alter table supr_010
add(lote_consignado number(1) default 0);
    
comment on column supr_010.lote_consignado
is 'Indica se o lote do fornecedor � consignado ou n�o (0-N�o � consignado, 1-� consignado)';

exec inter_pr_recompile;    