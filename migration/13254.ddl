create table OBRF_780
(numero_solicitacao     number(9)       default 0    not null,
 empresa_solicitacao    number(3)       default 0    not null,
 data_execucao          date,
 hora_execucao          date,
 usuario_execucao       varchar2(20)    default ' ',
 situacao               number(2)       default 0    not null,
 cod_processo           number(4)       default 0    not null,
 periodo_ini_op         number(4)       default 0,
 periodo_fim_op         number(4)       default 0,
 estagio_pendente       number(4)       default 0,
 serie_nota_fisc        varchar2(3)     default ' ',
 ult_seq_passo_exec     number(4)       default 0
);

alter table OBRF_780
add constraint pk_OBRF_780 primary key (numero_solicitacao);
                                        
comment on column OBRF_780.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_780.empresa_solicitacao  is 'Indica a empresa em que a solicita��o (execu��o do processo) foi criada e executada';
comment on column OBRF_780.data_execucao        is 'Data em que o processo foi executado';
comment on column OBRF_780.hora_execucao        is 'Hor�rio em que o processo foi executado';
comment on column OBRF_780.usuario_execucao     is 'Usu�rio que executou o processo';
comment on column OBRF_780.situacao             is 'Situa��o do processo (0-Em Aberto, 1-Iniciado, 2-Finalizado)';
comment on column OBRF_780.cod_processo         is 'C�digo do processo que ser� executado';
comment on column OBRF_780.periodo_ini_op       is 'Per�odo de Produ��o Inicial para busca das Ordens de Produ��o (quando h� algum passo cuja origem � Ordem de Produ��o)';
comment on column OBRF_780.periodo_fim_op       is 'Per�odo de Produ��o Final para busca das Ordens de Produ��o (quando h� algum passo cuja origem � Ordem de Produ��o)';
comment on column OBRF_780.estagio_pendente     is 'Est�gio pendente das Ordens de Produ��o para considerar na busca (quando h� algum passo cuja origem � Ordem de Produ��o)';
comment on column OBRF_780.serie_nota_fisc      is 'S�rie das Notas Fiscais de Sa�da para considerar na busca (quando h� algum passo cuja origem � Notas Fiscais de Sa�da)';
comment on column OBRF_780.ult_seq_passo_exec   is 'Sequ�ncia do �ltimo passo executado com sucesso neste processo';

comment on table OBRF_780                       is 'Tabela respons�vel por gravar as execu��es feitas para os processos que s�o compostos de passos.';

exec inter_pr_recompile;
