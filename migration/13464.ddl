alter table loja_200
add(TRANSP_REDESPACHO9    NUMBER(9)  default 0,
    TRANSP_REDESPACHO4    NUMBER(4)  default 0,
    TRANSP_REDESPACHO2    NUMBER(2)  default 0,
    TIPO_FRETE_REDES      NUMBER(1)  default 0);
/
exec inter_pr_recompile;
