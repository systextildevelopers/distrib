create global temporary table inter_tt_devol_consig
(CODIGO_EMPRESA    NUMBER(3),
FORNECEDOR2    NUMBER(2),
FORNECEDOR4    NUMBER(4),
FORNECEDOR9    NUMBER(9),
NIVEL_ESTRUTURA    VARCHAR2(1),
GRUPO_ESTRUTURA    VARCHAR2(5),
SUBGRUPO_ESTRUTURA VARCHAR2(3),
ITEM_ESTRUTURA VARCHAR2(6),
QTDE_VENDIDA    NUMBER,
VALOR_VENDIDO    NUMBER,
QTDE_A_DEVOLVER    NUMBER,
VALOR_A_DEVOLVER   NUMBER,
SALDO_CONSIGNADO   NUMBER,
VALOR_SALDO_CONSIGNADO NUMBER,
SALDO_APOS_OPERACAO    NUMBER,
VALOR_SALDO_APOS_OPERACAO  NUMBER,
PROCESSAR NUMBER(1) DEFAULT 0
)
on commit PRESERVE rows;

comment on table inter_tt_devol_consig is 'Tabela Temporaria para Painel de Consignacao';

comment on column INTER_TT_DEVOL_CONSIG.CODIGO_EMPRESA is 'Codigo da empresa da consigna��o';
comment on column INTER_TT_DEVOL_CONSIG.FORNECEDOR2 is 'CNPJ2 do fornecedor';
comment on column INTER_TT_DEVOL_CONSIG.FORNECEDOR4 is 'CNPJ4 do fornecedor';
comment on column INTER_TT_DEVOL_CONSIG.FORNECEDOR9 is 'CNPJ9 do fornecedor';
comment on column INTER_TT_DEVOL_CONSIG.GRUPO_ESTRUTURA is 'Codigo do grupo do produto';
comment on column INTER_TT_DEVOL_CONSIG.ITEM_ESTRUTURA is 'Codigo do item do produto';
comment on column INTER_TT_DEVOL_CONSIG.NIVEL_ESTRUTURA is 'codigo do nivel do produto';
comment on column INTER_TT_DEVOL_CONSIG.SUBGRUPO_ESTRUTURA is 'Codigo do subgrupo do produto';
comment on column INTER_TT_DEVOL_CONSIG.QTDE_VENDIDA is 'Quantidade vendida do produto';
comment on column INTER_TT_DEVOL_CONSIG.VALOR_VENDIDO is 'Valor vendido do produto';
comment on column INTER_TT_DEVOL_CONSIG.SALDO_CONSIGNADO is 'Quantidade que se encontra cosignado na conta corrente';
comment on column INTER_TT_DEVOL_CONSIG.VALOR_SALDO_CONSIGNADO is 'Valor do saldo consignado';
comment on column INTER_TT_DEVOL_CONSIG.QTDE_A_DEVOLVER is 'Quantidade a devolver';
comment on column INTER_TT_DEVOL_CONSIG.VALOR_A_DEVOLVER is 'valor a devolver durante operacao';
comment on column INTER_TT_DEVOL_CONSIG.SALDO_APOS_OPERACAO is 'Quantidade restante apos operacao' ;
comment on column INTER_TT_DEVOL_CONSIG.VALOR_SALDO_APOS_OPERACAO is 'valor do saldo ap�s ape';
comment on column INTER_TT_DEVOL_CONSIG.PROCESSAR is 'Valor que indica se o registro deve ser processador';


exec inter_pr_recompile;

