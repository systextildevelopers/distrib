alter table pedi_104 add codigo_empresa number(9) default 0;
comment on column pedi_104.codigo_empresa
  is 'Codigo da empresa que utiliza processo de disponibilidade.';

/
exec inter_pr_recompile;