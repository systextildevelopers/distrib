update rcnb_080 r 
set data_lancto = (select nvl(min(c.data_pagamento),  to_date( ('01-'|| to_char(r.mes,'99') || '-' ||to_char(r.ano,'9999')) , 'DD-MM-YYYY')) from cpag_015 c
                   where c.dupl_for_nrduppag             = r.num_titulo
                     and c.dupl_for_no_parc              = r.parcela
                     and c.dupl_for_for_cli9             = r.cgc9
                     and c.dupl_for_for_cli4             = r.cgc4
                     and c.dupl_for_for_cli2             = r.cgc2
                     and to_char(trunc(c.data_pagamento, 'MM'),'MM')     = r.mes
                     and to_char(trunc(c.data_pagamento, 'YYYY'),'YYYY') = r.ano
                     and c.data_pagamento is not null)
where to_char(r.data_lancto,'DD-MM-YYYY') = to_char(sysdate,'DD-MM-YYYY');

commit;