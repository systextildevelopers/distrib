create table crec_220_log (
cnpj9_cliente_old               number (9),
cnpj9_cliente_new               number (9),
cnpj4_cliente_old               number (4),
cnpj4_cliente_new               number (4),
cnpj2_cliente_old               number (2),
cnpj2_cliente_new               number (2),
pedido_venda_old                number (6),
pedido_venda_new                number (6),
cod_rep_cliente_old             number (5),
cod_rep_cliente_new             number (5),
seq_processo_old                number (2),
seq_processo_new                number (2),
perc_comissao_old               number(6,2),
perc_comissao_new               number(6,2),
perc_comissao_fatu_old          number(6,2),
perc_comissao_fatu_new          number(6,2),
operacao                        varchar2(1),
data_operacao                   date,
usuario_rede                    varchar2(20),
maquina_rede                    varchar2(40),
aplicativo                      varchar2(20),
nome_programa                   varchar2(20) );


comment on table crec_220_log is 'Tabela de Log da Comiss�o dos Representantes.';
comment on column crec_220_log.cnpj9_cliente_old is 'CNPJ9 do cliente';
comment on column crec_220_log.cnpj9_cliente_new is 'CNPJ9 do cliente';
comment on column crec_220_log.cnpj4_cliente_old is 'CNPJ4 do cliente';
comment on column crec_220_log.cnpj4_cliente_new is 'CNPJ4 do cliente';
comment on column crec_220_log.cnpj2_cliente_old is 'CNPJ2 do cliente';
comment on column crec_220_log.cnpj2_cliente_new is 'CNPJ2 do cliente';
comment on column crec_220_log.pedido_venda_old is 'N�mero do pedido de venda';
comment on column crec_220_log.pedido_venda_new is 'N�mero do pedido de venda';
comment on column crec_220_log.cod_rep_cliente_old is 'C�digo do representante do cliente';
comment on column crec_220_log.cod_rep_cliente_new is 'C�digo do representante do cliente';
comment on column crec_220_log.seq_processo_old is 'Sequencial do processo (sequ�ncia em que ser� dada a comiss�o ao representante)';
comment on column crec_220_log.seq_processo_new is 'Sequencial do processo (sequ�ncia em que ser� dada a comiss�o ao representante)';
comment on column crec_220_log.perc_comissao_old is 'Percentual de comiss�o a ser aplicado no processo de c�lculo da comiss�o';
comment on column crec_220_log.perc_comissao_new is 'Percentual de comiss�o a ser aplicado no processo de c�lculo da comiss�o';
comment on column crec_220_log.perc_comissao_fatu_old is 'Percentual de comiss�o a ser aplicado no processo de faturamento';
comment on column crec_220_log.perc_comissao_fatu_new is 'Percentual de comiss�o a ser aplicado no processo de faturamento';
comment on column crec_220_log.operacao is 'Tipo da opera��o (I-Insert, U-Update, D- Delete)';
comment on column crec_220_log.data_operacao is 'Data/hora que ocorreu a atualiza��o';
comment on column crec_220_log.usuario_rede is 'Usu�rio de rede que fez a atualiza��o';
comment on column crec_220_log.maquina_rede is 'M�quina de rede que fez a atualiza��o';
comment on column crec_220_log.aplicativo is 'Aplicativo que fez a atualiza��o';
comment on column crec_220_log.nome_programa is 'Nome do programa que fez a atualiza��o';

exec inter_pr_recompile;
