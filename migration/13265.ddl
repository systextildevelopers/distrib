create table OBRF_791
(numero_bcm             number(9)       default 0        not null,
 seq_item               number(9)       default 0        not null,
 nivel                  varchar2(1)     default ' '      not null,
 grupo                  varchar2(5)     default ' '      not null,
 subgrupo               varchar2(3)     default ' '      not null,
 item                   varchar2(6)     default ' '      not null,
 lote                   number(6)       default 0,
 valor_unitario         number(15,5)    default 0.00000  not null,
 qtde_movimentada       number(15,3)    default 0.000    not null,
 valor_total            number(15,2)    default 0.00     not null
);

alter table OBRF_791
add constraint pk_OBRF_791 primary key (numero_bcm, seq_item);
                                        
comment on column OBRF_791.numero_bcm           is 'N�mero do Boletim de Circula��o de Mercadorias (BCM)';
comment on column OBRF_791.seq_item             is 'Sequencial do item do Boletim de Circula��o de Mercadorias (BCM)';
comment on column OBRF_791.nivel                is 'N�vel do produto do Boletim de Circula��o de Mercadorias (BCM)';
comment on column OBRF_791.grupo                is 'Grupo do produto do Boletim de Circula��o de Mercadorias (BCM)';
comment on column OBRF_791.subgrupo             is 'Subgrupo do produto do Boletim de Circula��o de Mercadorias (BCM)';
comment on column OBRF_791.item                 is 'Item do produto do Boletim de Circula��o de Mercadorias (BCM)';
comment on column OBRF_791.lote                 is 'Lote do produto do Boletim de Circula��o de Mercadorias (BCM)';
comment on column OBRF_791.valor_unitario       is 'Valor unit�rio do item do Boletim de Circula��o de Mercadorias (BCM)';
comment on column OBRF_791.qtde_movimentada     is 'Quantidade movimentada do item do Boletim de Circula��o de Mercadorias (BCM)';
comment on column OBRF_791.valor_total          is 'Valor total do item do Boletim de Circula��o de Mercadorias (BCM)';

comment on table OBRF_791                       is 'Tabela respons�vel por registrar os Itens dos Boletins de Circula��o de Mercadorias (BCMs), que s�o documentos de transfer�ncia de mercadorias entre duas empresas do grupo';

exec inter_pr_recompile;