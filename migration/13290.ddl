insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f830', 'Consulta dos resultados da execu��o dos processos', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f830', 'menu_ad101', 1, 1, 'N', 'N', 'N', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Consulta dos resultados da execu��o dos processos'
 where hdoc_036.codigo_programa = 'obrf_f830'
   and hdoc_036.locale          = 'es_ES';
commit;
