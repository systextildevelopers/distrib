alter table pcpt_020 add(
STATUS_INTEGRACAO NUMBER(1) 
);

comment on column PCPT_020.STATUS_INTEGRACAO
  is 'Flag utilizada por softwares de terceiros para integração, valor livre conforme necessidade';

exec inter_pr_recompile;