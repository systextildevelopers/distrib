alter table fatu_504
add (rpt_comunicado_canc varchar2(20) default 'pe_canc001_aa');

comment on column fatu_504.rpt_comunicado_canc is
'Rpt para listar o comunicado dos pedidos cancelados';

commit;

exec inter_pr_recompile;