update gfv_hdoc_830 
set SQL = q'[select cod_cidade, rtrim(cidade) as cidade, estado, ddd
from basi_160
where not exists (
  select p.valor
  from gfv_parametro p, gfv_empresa e 
  where p.empresa = e.oid
    and p.chave = 'vendeForaDaSubregiao'
    and p.valor = '0'
    and e.codigo = ?cod_empresa?
)              
UNION 
select cod_cidade, rtrim(cidade) as cidade, estado, ddd
from basi_160
where exists (
  select p.valor
  from gfv_parametro p, gfv_empresa e 
  where p.empresa = e.oid
    and p.chave = 'vendeForaDaSubregiao'
    and p.valor = '0'
    and e.codigo = ?cod_empresa?
)
  and basi_160.cod_sub_regiao in (select pedi_023.sub_regiao from pedi_023 where pedi_023.cod_rep_cliente = ?cod_repre?)
   or basi_160.cod_cidade in (select pedi_027.cod_cidade from pedi_027 where pedi_027.representante = ?cod_repre?)         
order by estado, cidade]'
where ID = 2;

commit;