ALTER TABLE obrf_150 ADD (
      id_dest number(1) DEFAULT 0,
      ind_final number(1) DEFAULT 0,      
      ind_pres number(1) DEFAULT 0,
      id_estrangeiro varchar(20) DEFAULT '',
      ind_ie_dest number(1) DEFAULT 0,
      v_tot_trib number(13,2) DEFAULT 0,
      v_icms_deson number(13,2) DEFAULT 0,
      uf_saida_pais varchar(2) DEFAULT '',
      x_loc_exporta varchar(60) DEFAULT '',
      x_loc_despacho varchar(60) DEFAULT ''
 );
 exec inter_pr_recompile;