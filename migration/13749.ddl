alter table fatu_070
add (nr_adiantamento numeric(9) default 0);

comment on column fatu_070.nr_adiantamento
  is 'N�mero de adiantamento original do t�tulo';
       
exec inter_pr_recompile;
/