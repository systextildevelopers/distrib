create or replace view inter_vi_cons_consig_nfe as
select obrf_060.codigo_empresa,
    obrf_060.fornecedor9,
    obrf_060.fornecedor4,
    obrf_060.fornecedor2,
    obrf_060.nivel_estrutura,
    obrf_060.grupo_estrutura,
    obrf_060.subgrupo_estrutura,
    obrf_060.item_estrutura,
    obrf_060.documento,
    obrf_060.serie,
    obrf_060.data_entrada,
    supr_010.nome_fornecedor,
    obrf_060.qtde_nfe,
    obrf_060.qtde_retorno_devol,
    obrf_060.qtde_retorno_venda,
    obrf_060.saldo_fisico,
    obrf_060.saldo_fisico*obrf_060.valor_unitario valor_saldo_fisico
from obrf_060,supr_010
where supr_010.fornecedor2 = obrf_060.fornecedor2
and supr_010.fornecedor4 = obrf_060.fornecedor4
and supr_010.fornecedor9 = obrf_060.fornecedor9;

exec inter_pr_recompile;
