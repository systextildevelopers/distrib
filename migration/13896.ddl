CREATE TABLE rcnb_800 (
  mes_classe NUMBER(2) DEFAULT 0 NOT NULL,
  ano_classe NUMBER(4) DEFAULT 0 NOT NULL,
  seq_classe NUMBER(3) DEFAULT 0 NOT NULL,
  classe_valor NUMBER(4) DEFAULT 0 NOT NULL,
  valor_inicial NUMBER(10,2) DEFAULT 0.0,
  valor_final NUMBER(10,2) DEFAULT 0.0,
  situacao VARCHAR2(1) DEFAULT ''
);

COMMENT ON TABLE rcnb_800
  IS 'Tabela dos valores das classes de valor. Utilizado para gerar tabelas de pre�os de tecidos acabados e fios.';

COMMENT ON COLUMN rcnb_800.mes_classe
  is 'M�s da classe de valor.';
COMMENT ON COLUMN rcnb_800.ano_classe
  is 'Ano da classe de valor.';
COMMENT ON COLUMN rcnb_800.seq_classe
  is 'Sequ�ncia da classe de valor.';
COMMENT ON COLUMN rcnb_800.classe_valor
  is 'C�digo da classe de valor.';
COMMENT ON COLUMN rcnb_800.valor_inicial
  is 'Valor inicial da classe de valor.';
COMMENT ON COLUMN rcnb_800.valor_final
  is 'Valor final da classe de valor.';
COMMENT ON COLUMN rcnb_800.situacao
  is 'Situa��o da classe de valor. C - Cadastrada. A - Aprovada.';  

ALTER TABLE rcnb_800
  ADD CONSTRAINT PK_RCNB_800 PRIMARY KEY (mes_classe, ano_classe, seq_classe, classe_valor);
  
EXEC inter_pr_recompile;