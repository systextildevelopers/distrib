create or replace view inter_vi_pcpc_020_pcpc_040_cor
(periodo_producao, ordem_producao, ordem_principal, referencia_peca, pedido_venda, observacao, ultimo_estagio, ordem_origem, cod_cancelamento, codigo_estagio, seq_operacao, estagio_anterior, estagio_depende, codigo_familia, cor, codigo_empresa, qtde_pecas_prog, qtde_pecas_prod, qtde_pecas_2a, qtde_conserto, qtde_perdas)
as
select pcpc_020.periodo_producao, pcpc_020.ordem_producao,
       pcpc_020.ordem_principal,
       pcpc_020.referencia_peca,  pcpc_020.pedido_venda,
       pcpc_020.observacao,       pcpc_020.ultimo_estagio,
       pcpc_020.ordem_origem,     pcpc_020.cod_cancelamento,
       pcpc_040.codigo_estagio,   pcpc_040.seq_operacao,
       pcpc_040.estagio_anterior, pcpc_040.estagio_depende,
       pcpc_040.codigo_familia,   pcpc_040.proconf_item,
       pcpc_040.codigo_empresa,
       sum(pcpc_040.qtde_pecas_prog),
       sum(pcpc_040.qtde_pecas_prod),  sum(pcpc_040.qtde_pecas_2a),
       sum(pcpc_040.qtde_conserto),    sum(pcpc_040.qtde_perdas)
from pcpc_040, pcpc_020
where   pcpc_040.ordem_producao = pcpc_020.ordem_producao
group by pcpc_020.periodo_producao, pcpc_020.ordem_producao,
         pcpc_020.ordem_principal,
         pcpc_020.referencia_peca,  pcpc_020.pedido_venda,
         pcpc_020.observacao,       pcpc_020.ultimo_estagio,
         pcpc_020.ordem_origem,     pcpc_020.cod_cancelamento,
         pcpc_040.codigo_estagio,   pcpc_040.seq_operacao,
         pcpc_040.estagio_anterior, pcpc_040.estagio_depende,
         pcpc_040.codigo_familia,   pcpc_040.proconf_item,
         pcpc_040.codigo_empresa
order by pcpc_020.periodo_producao, pcpc_020.ordem_producao,
         pcpc_020.ordem_principal,
         pcpc_020.referencia_peca,  pcpc_020.pedido_venda,
         pcpc_020.observacao,       pcpc_020.ultimo_estagio,
         pcpc_020.ordem_origem,     pcpc_020.cod_cancelamento,
         pcpc_040.codigo_estagio,   pcpc_040.seq_operacao,
         pcpc_040.estagio_anterior, pcpc_040.estagio_depende,
         pcpc_040.codigo_familia,   pcpc_040.proconf_item,
         pcpc_040.codigo_empresa;

exec inter_pr_recompile;