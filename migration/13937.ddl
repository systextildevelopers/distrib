
  CREATE OR REPLACE FORCE VIEW MQOP_029_PCPT_020_021_035 (GRUPO_MAQUINA, SUBGRU_MAQUINA, NUMERO_MAQUINA, LOCAL_ALIMENTADOR, CODIGO_ROLO, NIVEL_PROD, GRUPO_PROD, SUBGRU_PROD, ITEM_PROD, CODIGO_DEPOSITO, ORDEM_PRODUCAO, LOTE_ACOMP, QTDE_QUILOS_ACAB, RESTRICAO, LOCAL_ALIMENTACAO, ENDERECO_ROLO, AREA_PRODUCAO) AS
  select pcpt_035.grupo_maquina,     pcpt_035.subgru_maquina,
       pcpt_035.numero_maquina,    pcpt_035.local_alimentador,
       pcpt_035.codigo_rolo,       pcpt_035.nivel_prod,
       pcpt_035.grupo_prod,        pcpt_035.subgru_prod,
       pcpt_035.item_prod,         pcpt_035.codigo_deposito,
       pcpt_035.ordem_producao,    pcpt_020.lote_acomp,
       pcpt_020.qtde_quilos_acab,  pcpt_021.restricao,
       mqop_029.local_alimentacao, pcpt_020.endereco_rolo,
       pcpt_035.area_producao
from pcpt_035, pcpt_020, pcpt_021, mqop_029
where  pcpt_035.codigo_rolo     =  pcpt_020.codigo_rolo
  and  pcpt_020.codigo_rolo     =  pcpt_021.codigo_rolo      (+)
  and  pcpt_035.grupo_maquina   =  mqop_029.grupo_maquina    (+)
  and  pcpt_035.subgru_maquina  =  mqop_029.subgrupo_maquina (+);

/
exec inter_pr_recompile;