alter table fatu_504 add cod_recurso_homem number(1) default 1;

comment on column fatu_504.cod_recurso_homem is 'Define codificacao de recursos para o Systextil Plan quando roteiro contem operacao homem. SS 65844/072.';
