create or replace view inter_vi_altera_cor
(periodoproducao, ordemproducao, produtonivel, produtogrupo, produtosubgrupo, produtoitem, produtosequencia, produtoalternativa, produtoroteiro, qtdeunidadeprog, qtdequilosprog, qtderolosprog, qtdequilosreal, qtderolosreal, ordemservico, seqordemservico, dataemissao, dataprevrecebimento, sitrecebimento, tabelaservico, tabelaservicomes, tabelaservicoseq, descrrecebimento)
as
select pcpb_010.periodo_producao, pcpb_010.ordem_producao,
       pcpb_020.pano_sbg_nivel99, pcpb_020.pano_sbg_grupo,
       pcpb_020.pano_sbg_subgrupo, pcpb_020.pano_sbg_item,
       pcpb_020.sequencia,
       pcpb_020.alternativa_item, pcpb_020.roteiro_opcional,
       pcpb_020.qtde_unidade_prog,
       pcpb_020.qtde_quilos_prog, pcpb_020.qtde_rolos_prog,
       pcpb_020.qtde_quilos_real, pcpb_020.qtde_rolos_real,
       pcpb_010.ordem_servico, pcpb_020.seq_ordem_servico,
       obrf_080.data_emissao, obrf_081.dt_prev_areceber,
       obrf_081.sit_areceber,
       obrf_080.cod_tabela_serv, obrf_080.cod_tabela_mes,
       obrf_080.cod_tabela_seq,
       CASE
         WHEN obrf_081.sit_areceber = 1
           THEN 'ABERTA'
         WHEN obrf_081.sit_areceber = 2
           THEN 'EM PROCESSO'
         WHEN obrf_081.sit_areceber = 3
           THEN 'BAIXA PARCIAL'
         WHEN obrf_081.sit_areceber = 4
           THEN 'BAIXA TOTAL'
       END descr_situacao
from pcpb_010, pcpb_020, obrf_080, obrf_081
where pcpb_010.ordem_producao = pcpb_020.ordem_producao
  and pcpb_010.ordem_servico = obrf_080.numero_ordem
  and obrf_080.numero_ordem = obrf_081.numero_ordem
  and obrf_081.sequencia = pcpb_020.seq_ordem_servico
  and obrf_081.sit_areceber < 3
  and pcpb_010.ordem_servico > 0
  and pcpb_020.qtde_quilos_prod = 0;
exec inter_pr_recompile;
/
