alter table basi_750 add checksum varchar2(32) default '';

comment on column basi_750.checksum is 'Campo usado para guardar o checksum da imagem. Pode ser usado para verificar quando uma imagem sofreu alteracoes';

exec inter_pr_recompile;