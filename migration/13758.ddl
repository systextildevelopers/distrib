declare
   nro_registro number;
   
   cursor opsEmpresa is              select pcpc_020.ordem_producao, pcpc_010.codigo_empresa
                                     from pcpc_020, pcpc_010
                                     where pcpc_020.cod_cancelamento = 0
                                     and   pcpc_010.area_periodo     = 1
                                     and   pcpc_010.periodo_producao = pcpc_020.periodo_producao;
begin
   nro_registro := 0;
   
   for reg_opsEmpresa in opsEmpresa
   loop
      begin
         update pcpc_040
         set   pcpc_040.codigo_empresa = reg_opsEmpresa.codigo_empresa
         where pcpc_040.ordem_producao = reg_opsEmpresa.ordem_producao
           and pcpc_040.codigo_empresa = 0;
      end;
      
      nro_registro := nro_registro + 1;
      
      if nro_registro > 1000
      then
         commit;
      end if;
   end loop;
   
   commit;
end;

/
exec inter_pr_recompile;  