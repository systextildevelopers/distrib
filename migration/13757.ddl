-- Add/modify columns 
alter table PCPC_040 add codigo_empresa NUMBER(3) default 0;
-- Add comments to the columns 
comment on column PCPC_040.codigo_empresa
  is 'C�digo da empresa que ir� produzir o est�gio';

exec inter_pr_recompile;
