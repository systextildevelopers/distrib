alter table obrf_761 add 
(motivo_devolucao             number(3) default 0);

comment on column obrf_761.motivo_devolucao is 'Este campo serve para se parametrizar o motivo de devolu��o a ser gravado no item da nota fiscal de sa�da gerada por este passo quando for uma devolu��o, isto �, quando a transa��o da natureza de opera��o do passo desta nota for do tipo "D"';

exec inter_pr_recompile;
