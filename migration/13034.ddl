alter table basi_010
add(origem_prod numeric(1));
    
comment on column basi_010.origem_prod
is 'Indica a origem do produto';

alter table basi_010
modify origem_prod default 3;

begin declare
cursor basi_010_c is
   select f.rowid from basi_010 f;

conta_reg number(9);

begin
   for reg_basi_010_c in basi_010_c
   loop
      conta_reg := conta_reg + 1;
       
      update basi_010
      set origem_prod = 3
      where origem_prod is null
        and basi_010.rowid = reg_basi_010_c.rowid;
      
      if conta_reg = 100
      then
         commit;
         conta_reg := 0;
      end if;
   end loop; 

   commit;
end;
end;
/

exec inter_pr_recompile;