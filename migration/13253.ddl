alter table crec_180
add(chq_resp number(1) default 0);
    
comment on column crec_180.chq_resp
is 'Relacionamento feito pelo cliente ou responsável do título.';

exec inter_pr_recompile;