-- Add/modify columns 
alter table HIST_010 add codigo_empresa NUMBER(3);
alter table HIST_010 add codigo_estagio NUMBER(2);
-- Add comments to the columns 
comment on column HIST_010.codigo_empresa
  is 'Empresa que ser� produzido';  
comment on column HIST_010.codigo_estagio
  is 'C�digo do est�gio';  

exec inter_pr_recompile;  
