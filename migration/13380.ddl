alter table fatu_503
add(tipo_geracao_ordem     number(1) default 0 null);
    
comment on column fatu_503.tipo_geracao_ordem
is 'Campo n�mero que diz qual tipo de geracao que sera usada.';

exec inter_pr_recompile;