insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('rcnb_f016', 'Cadastro temporário de Boletins de Circulação de Mercadorias', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'rcnb_f016', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Cadastro temporário de Boletins de Circulação de Mercadorias'
 where hdoc_036.codigo_programa = 'rcnb_f016'
   and hdoc_036.locale          = 'es_ES';
commit;
