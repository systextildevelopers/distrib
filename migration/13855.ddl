create table basi_572(
cod_empresa       number(3) default 0 not null,
terc_cnpj9        number(9) default 0 not null,
terc_cnpj4        number(4) default 0 not null,
terc_cnpj2        number(2) default 0 not null,
cod_dep           number(3) default 0);


comment on column basi_572.cod_empresa is 'Codigo da Empresa para controle de deposito de terceiro';
comment on column basi_572.terc_cnpj9 is 'cnpj do terceiro';
comment on column basi_572.terc_cnpj4 is 'cnpj do terceiro';
comment on column basi_572.terc_cnpj2 is 'cnpj do terceiro';
comment on column basi_572.cod_dep is 'codigo do deposito do terceiro';

alter table basi_572
  add constraint PK_basi_572 primary key (cod_empresa,terc_cnpj9, terc_cnpj4,terc_cnpj2 );
  
exec inter_pr_recompile;
/  