insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f668', 'Consulta de IPI por notas de Entrada/Sa�da ou por CFOP', 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f668', 'obrf_menu', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Consulta de IPI por notas de Entrada/Sa�da ou por CFOP'
 where hdoc_036.codigo_programa = 'obrf_f668'
   and hdoc_036.locale          = 'es_ES';

commit;

exec inter_pr_recompile;
