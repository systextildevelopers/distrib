alter table fatu_504 add
nr_linhas_layout_bcm number(3) default 27;

comment on column fatu_504.nr_linhas_layout_bcm is
'N�mero de linhas de itens do layout do Boletim de Circula��o de Mercadorias (BCM, tabela OBRF_790, que � gerado pelo processo autom�tico OBRF_F780). Este campo serve para o sistema fazer a quebra do BCM em um novo BCM quando atingir o limite de itens do layout.';

exec inter_pr_recompile;
