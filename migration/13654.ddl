-- Create table
create table BASI_061
(
  codigo_empresa      NUMBER(3)    default 0 not null,
  classific_fiscal    VARCHAR2(15) default ' ' not null,
  perc_icms           NUMBER(6,2)  default 0.00
);

alter table BASI_061
add constraint pk_BASI_061 primary key (codigo_empresa, classific_fiscal);

exec inter_pr_recompile;