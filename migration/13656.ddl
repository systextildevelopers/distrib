update hdoc_035
set hdoc_035.descricao = 'Consulta de Produ��o das Opera��es da Ordem de Confec��o'
where hdoc_035.codigo_programa = 'pcpc_f842';

update hdoc_036
set hdoc_036.descricao = 'Consulta de Produ��o das Opera��es da Ordem de Confec��o'
where hdoc_036.codigo_programa = 'pcpc_f842'
  and hdoc_036.locale = 'pt_BR';

update hdoc_036
set hdoc_036.descricao = 'Consulta de Producci�n de las Operaciones de la Orden de Confecci�n'
where hdoc_036.codigo_programa = 'pcpc_f842'
  and hdoc_036.locale = 'es_ES';

commit;

/

exec inter_pr_recompile;

