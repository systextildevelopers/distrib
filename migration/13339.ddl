insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpc_f178', 'Consulta de Itens faltantes do Pedido', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpc_f178', ' ', 1, 1, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpc_f178', ' ', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Consulta de Itens faltantes do Pedido'
 where hdoc_036.codigo_programa = 'pcpc_f178'
   and hdoc_036.locale          = 'es_ES';
commit;
