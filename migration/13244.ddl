insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('menu_ad101', 'Administração de Processos', 1,1);
commit;
insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'menu_ad101', 'menu_ge40', 1, 2, 'S', 'S', 'S', 'S');
insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'menu_ad101', 'menu_ge40', 1, 2, 'S', 'S', 'S', 'S');
update hdoc_036
set hdoc_036.descricao = 'Administración de Procesos'
where hdoc_036.codigo_programa = 'menu_ad101'
and hdoc_036.locale = 'es_ES';
commit;
exec inter_pr_recompile;