alter table basi_750 add (imagem_comercial number(1) default 0);
comment on column basi_750.imagem_comercial is 'Indica se a essa � a imagem comercial do produto, usada no for�a de vendas. 0 - N�o e 1 - Sim';

exec inter_pr_recompile;