alter table pedi_051
add(SACADOR_AVALISTA   VARCHAR2(40) default '');

comment on column pedi_051.sacador_avalista is 'Cnpj ou CPF mais o Nome do Sacador Avalista';

exec inter_pr_recompile;
/