create or replace view vi_cons_nfe as
select  entrada_saida
        ,serie
        ,numero
        ,data_emissao
        ,emit_cnpj9
        ,emit_cnpj4
        ,emit_cnpj2
        ,emit_nome
        ,nr_recibo
        ,nr_protocolo
        ,justificativa
        ,ano_inutilizacao
        ,nr_final_inut
        ,cod_status
        ,cod_status_sel
        ,numero_danf_nfe
        ,cod_solicitacao_nfe
        ,codigo_empresa
        ,nfe_contigencia
        ,cd_cce
from
    (select  'S' as entrada_saida -- saida
            ,f.serie_nota_fisc as serie
            ,f.num_nota_fiscal as numero
            ,f.data_emissao
            ,f.cgc_9 as emit_cnpj9
            ,f.cgc_4 as emit_cnpj4
            ,f.cgc_2 as emit_cnpj2
            ,decode(inter_fn_descricao_natureza(f.codigo_empresa,f.num_nota_fiscal,f.serie_nota_fisc),
                                                                 null,p.nome_cliente
                                                                     ,inter_fn_descricao_natureza(f.codigo_empresa,f.num_nota_fiscal,f.serie_nota_fisc)) as emit_nome
            ,f.nr_recibo
            ,f.nr_protocolo
            ,f.justificativa
            ,f.ano_inutilizacao
            ,f.nr_final_inut
            ,f.cod_status
            ,f.cod_status as cod_status_sel
            ,f.numero_danf_nfe
            ,f.cod_solicitacao_nfe
            ,f.codigo_empresa
            ,f.nfe_contigencia
            ,-1              as cd_cce
    from fatu_050 f,
         fatu_505 f_505,
         pedi_010 p
    where f_505.codigo_empresa  = f.codigo_empresa
      and f_505.serie_nota_fisc = f.serie_nota_fisc
      and f_505.codigo_empresa  = f.codigo_empresa
      and f.cgc_9               = p.cgc_9
      and f.cgc_4               = p.cgc_4
      and f.cgc_2               = p.cgc_2
      and f_505.serie_nfe       = 'S'
      and f.cod_status not in ('100','101','102','204')
      and (f.situacao_nfisc      = 1 or f.situacao_nfisc = 2 and f.cod_status = '990')
    )
union all
    (select 'E' -- ENTRADA
            ,o.serie
            ,o.documento
            ,o.data_emissao
            ,s_010.fornecedor9
            ,s_010.fornecedor4
            ,s_010.fornecedor2
            ,s_010.nome_fornecedor
            ,o.nr_recibo
            ,o.nr_protocolo
            ,o.justificativa
            ,o.ano_inutilizacao
            ,o.nr_final_inut
            ,o.cod_status
            ,o.cod_status as cod_status_sel
            ,o.numero_danf_nfe
            ,o.cod_solicitacao_nfe
            ,o.local_entrega
            ,o.nfe_contigencia
            ,-1              as cd_cce
    from obrf_010 o, fatu_505 f_505, supr_010 s_010
    where f_505.codigo_empresa  = o.local_entrega
      and f_505.serie_nota_fisc = o.serie
      and o.cgc_cli_for_9 = s_010.fornecedor9
      and o.cgc_cli_for_4 = s_010.fornecedor4
      and o.cgc_cli_for_2 = s_010.fornecedor2
      and o.cod_status not in ('100','101','102','204')
      and f_505.serie_nfe = 'S'
      and (o.situacao_entrada = 1 or o.situacao_entrada = 2 and o.cod_status = '990')
      union all
   (select obrf_122.cd_tipo_e_s                as entrada_saida
          ,obrf_122.cd_serie                   as serie
          ,obrf_122.cd_numero_nota             as numero
          ,to_date(null)                       as data_emissao
          ,obrf_122.cd_forn_9                  as emit_cnpj9
          ,obrf_122.cd_forn_4                  as emit_cnpj4
          ,obrf_122.cd_forn_2                  as emit_cnpj2
          ,decode (obrf_122.cd_tipo_e_s,'S',(select  pedi_010.nome_cliente from pedi_010
                                             where  pedi_010.cgc_9 = obrf_122.cd_forn_9
                                               and  pedi_010.cgc_4 = obrf_122.cd_forn_4
                                               and  pedi_010.cgc_2 = obrf_122.cd_forn_2),
                                            (select supr_010.nome_fornecedor from supr_010
                                             where supr_010.fornecedor9 =obrf_122.cd_forn_9
                                               and  supr_010.fornecedor4 =obrf_122.cd_forn_4
                                               and  supr_010.fornecedor2 =obrf_122.cd_forn_2))
                                               as emit_nome
          ,to_char(null)                       as nr_recibo
          ,to_char(null)                       as nr_protocolo
          ,to_char(null)                       as justificativa
          ,to_char(null)                       as ano_inutilizacao
          ,to_char(null)                       as nr_final_inut
          ,obrf_122.cd_status                  as cod_status
          ,obrf_122.cd_status                  as cod_status_sel
          ,to_char(null)                       as numero_danf_nfe
          ,0                                   as cod_solicitacao_nfe
          ,obrf_122.cd_empresa                 as codigo_empresa
          ,0                                   as nfe_contigencia
          ,obrf_122.cd_cce
          from obrf_122
          where obrf_122.st_enviado_s_n       = 'N'
            and (obrf_122.cd_status <> '135' )
          ));