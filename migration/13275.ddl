create table OBRF_786
(numero_solicitacao     number(9)       default 0        not null,
 seq_passo              number(4)       default 0        not null,
 cod_empresa            number(3)       default 0        not null,
 num_nota_fiscal        number(9)       default 0        not null,
 serie_nota_fisc        varchar2(3)     default ' '      not null,
 seq_item_nota          number(5)       default 0        not null,
 nivel                  varchar2(1)     default ' '      not null,
 grupo                  varchar2(5)     default ' '      not null,
 subgrupo               varchar2(3)     default ' '      not null,
 item                   varchar2(6)     default ' '      not null,
 lote_acomp             number(6)       default 0,
 cod_deposito           number(3)       default 0        not null,
 valor_unitario         number(15,5)    default 0.00000  not null,
 quantidade             number(15,3)    default 0.000    not null,
 flag_marcado           number(1)       default 0        not null
);

alter table OBRF_786
add constraint pk_OBRF_786 primary key (numero_solicitacao, seq_passo, cod_empresa, num_nota_fiscal, serie_nota_fisc, seq_item_nota);
                                        
comment on column OBRF_786.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_786.seq_passo            is 'Sequ�ncia do passo da execu��o do processo';

comment on column OBRF_786.cod_empresa          is 'C�digo da empresa da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';
comment on column OBRF_786.num_nota_fiscal      is 'N�mero da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';
comment on column OBRF_786.serie_nota_fisc      is 'S�rie da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';
comment on column OBRF_786.seq_item_nota        is 'Sequencial do item da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';
comment on column OBRF_786.nivel                is 'N�vel do produto da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';
comment on column OBRF_786.grupo                is 'Grupo do produto da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';
comment on column OBRF_786.subgrupo             is 'Subgrupo do produto da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';
comment on column OBRF_786.item                 is 'Item do produto da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';
comment on column OBRF_786.lote_acomp           is 'Lote do produto da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';
comment on column OBRF_786.cod_deposito         is 'Dep�sito do item da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';
comment on column OBRF_786.valor_unitario       is 'Valor unit�rio do item da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';
comment on column OBRF_786.quantidade           is 'Quantidade do item da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';
comment on column OBRF_786.flag_marcado         is 'Flag para indicar se o item foi considerado (1) ou n�o (0) na gera��o da nota fiscal de sa�da a ser gerada na execu��o do processo neste passo';

comment on table OBRF_786                       is 'Tabela respons�vel por gravar os �tens da nota fiscal de sa�da a ser gerada na execu��o do processo.';

exec inter_pr_recompile;
