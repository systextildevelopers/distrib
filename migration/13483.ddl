alter table basi_295 add (disponivel_net number(1) default 0);
comment on column basi_295.disponivel_net is 'Indica se o registro deve ser integrado com o forca de vendas. (1 - Sim, 0 - Nao)';

exec inter_pr_recompile;
