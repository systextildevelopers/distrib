create table OBRF_784
(numero_solicitacao     number(9)       default 0        not null,
 numero_bcm             number(9)       default 0        not null,
 numero_formulario      number(9)       default 0        not null,
 ordem_producao         number(9)       default 0        not null,
 data_emissao           date,
 cod_empresa_orig       number(3)       default 0        not null,
 cod_empresa_dest       number(3)       default 0        not null,
 quantidade             number(15,3)    default 0        not null,
 flag_marcado           number(1)       default 0        not null
);

alter table OBRF_784
add constraint pk_OBRF_784 primary key (numero_solicitacao, numero_bcm);
                                        
comment on column OBRF_784.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_784.numero_bcm           is 'N�mero do BCM encontrado na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_784.numero_formulario    is 'N�mero do Formul�rio (impress�o tipogr�fica) do BCM encontrado na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_784.ordem_producao       is 'Ordem de Produ��o do BCM encontrado na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_784.data_emissao         is 'Data de Emiss�o do BCM encontrado na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_784.cod_empresa_orig     is 'C�digo da Empresa de Origem do BCM encontrado na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_784.cod_empresa_dest     is 'C�digo da Empresa de Destino do BCM encontrado na consulta feita pelo usu�rio para esta execu��o do processo';

comment on column OBRF_784.quantidade           is 'Quantidade Total do BCM encontrado na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_784.flag_marcado         is 'Flag para indicar se o registro (BCM) foi marcado (1) ou n�o foi marcado (0) para considerar na execu��o do processo';

comment on table OBRF_784                       is 'Tabela respons�vel por gravar os Boletins de Circula��o de Mercadoria (BCMs) encontrados nas consultas feitas pelo usu�rio para cada execu��o do processo.';

exec inter_pr_recompile;
