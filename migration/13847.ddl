insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('cpag_e605', 'Relatório de Utilização de Adiantamentos', 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'cpag_e605', 'menu_ad19', 1 , 4, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Reporte del utilización del adelantos'
where hdoc_036.codigo_programa = 'cpag_e605'
  and hdoc_036.locale          = 'es_ES';

commit;

/

exec inter_pr_recompile;