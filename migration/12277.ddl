alter table inte_890 add (
    codigo_preposto number(5)
);

comment on column inte_890.codigo_preposto is 'C�digo do preposto (gfv_preposto) quando o mesmo for usado por um preposto';
/
exec inter_pr_recompile;