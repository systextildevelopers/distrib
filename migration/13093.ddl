alter table rcnb_060 
add (campo_numerico_01 numeric(9));

alter table rcnb_060
modify (campo_numerico_01 default 0);

alter table supr_511
add (tipo_requisicao number(1));

alter table supr_511
modify (tipo_requisicao default 1);

comment on column supr_511.tipo_requisicao
is 'tipo de requisi��o: 1 - Por Ordem ou 2 - Gen�rica';


prompt "ATEN��O! O comando abaixo devera demorar para executar, e n�o deve ser abortado."

begin
declare

cont number(9);

begin
   cont := 0;
   for reg_supr_511 in (select rowid from supr_511)
   loop 
      update supr_511
         set supr_511.tipo_requisicao = 1
      where supr_511.rowid           = reg_supr_511.rowid;
      
      cont := cont +1;

      if cont = 100
      then
         commit;
         cont := 0;
      end if;
   end loop;
   commit;
end;
end;
/

exec inter_pr_recompile;