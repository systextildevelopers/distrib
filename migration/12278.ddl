create table gfv_relay (
   codigo_representante number(5),
   codigo_preposto number(5),
   pedidos blob default empty_blob(),
   clientes blob default empty_blob()
);

comment on table gfv_relay is 'Servi�o de relay de represetantes/prepostos para o for�a de vendas';
comment on column gfv_relay.codigo_representante is 'Codigo do representante';
comment on column gfv_relay.codigo_preposto is 'Codigo do preposto';
comment on column gfv_relay.pedidos is 'Lista de pedidos serializados';
comment on column gfv_relay.clientes is 'Lista de clientes serializados';

/
exec inter_pr_recompile;