create table obrf_060 (
  codigo_empresa number(3) default 0 not null,
  fornecedor9 number(9) default 0 not null,
  fornecedor4 number(4) default 0 not null,
  fornecedor2 number(2) default 0 not null,
  documento number(9) default 0 not null,
  serie varchar2(3) default ' ' not null,
  sequencia_nota number(9) default 0 not null,
  nivel_estrutura varchar2(1) default ' ',
  grupo_estrutura varchar2(5) default ' ',   
  subgrupo_estrutura varchar2(3) default ' ', 
  item_estrutura varchar2(6) default ' ',
  qtde_nfe number(14,3) default 0.00,
  qtde_retorno_devol number(14,3) default 0.00,
  qtde_retorno_venda number(14,3) default 0.00,
  valor_unitario number(15,5) default 0.00,
  saldo_fisico number(14,3) default 0.00,
  data_entrada date);

comment on table obrf_060 is 'Tabela de Conta Corrente de produtos consignados';

comment on column obrf_060.codigo_empresa is 'Codigo da empresa';
comment on column obrf_060.fornecedor9 is 'CNPJ do fornecedor';
comment on column obrf_060.fornecedor4 is 'CNPJ do fornecedor';
comment on column obrf_060.fornecedor2 is 'CNPJ do fornecedor';
comment on column obrf_060.documento is 'Numero do documento da nota fiscal de entrada';
comment on column obrf_060.serie is 'Numero de serie do documento da nota fislca de entrada';
comment on column obrf_060.sequencia_nota is 'Sequencia do item da nota fiscal de entrada';
comment on column obrf_060.nivel_estrutura is 'Nivel do produto que entrou na nota fiscal de entrada';
comment on column obrf_060.grupo_estrutura is 'Grupo do produto que entrou na nota fiscal de entrada';
comment on column obrf_060.subgrupo_estrutura is 'Subgrupo do produto que entrou na nota fiscal de entrada';
comment on column obrf_060.item_estrutura is 'Item do produto que entrou na nota fiscal de entrada';
comment on column obrf_060.qtde_nfe is 'Quantidade do produto que entrou na nota fiscal de entrada';
comment on column obrf_060.qtde_retorno_devol is 'Quantidade devolvida para o fornecedor';
comment on column obrf_060.qtde_retorno_venda is 'Quantidade vendida do produto';
comment on column obrf_060.valor_unitario is 'Valor unitario do produto';
comment on column obrf_060.saldo_fisico is 'Saldo fisido do produto na empresa (qtde_nfe - qtde_retorno_devol - qtde_retorno_venda)';
comment on column obrf_060.data_entrada is 'Data da nota fiscal de entrada';


alter table obrf_060
add constraint pk_obrf_060 primary key (codigo_empresa, fornecedor9, fornecedor4, fornecedor2, documento, serie, sequencia_nota);

create synonym systextilrpt.obrf_060 for obrf_060;

exec inter_pr_recompile;

