create table pedi_125(
pedido_venda        number(6) default 0,
seq_distr           number(6) default 0,
cgc9cli             number (9) default 0,
cgc4cli             number (4) default 0,
cgc2cli             number (2) default 0,
niv_dest            varchar2(1) default ' ',
gru_dest            varchar2(5) default ' ',
sub_dest            varchar2(3) default ' ',
ite_dest            varchar2(6) default ' ',
qtde_dest           number(13,3) default 0.000,
seq_ped             number(3) default 0);



comment on column pedi_125.pedido_venda is 'o numero do pedido que estiver sendo distribu�do';
comment on column pedi_125.seq_distr is '�ltima sequencia + 1';
comment on column pedi_125.cgc9cli is 'cnpj de um cliente existente';
comment on column pedi_125.cgc4cli is 'cnpj de um cliente existente';
comment on column pedi_125.cgc2cli is 'cnpj de um cliente existente';
comment on column pedi_125.niv_dest is 'o c�digo de um produto';
comment on column pedi_125.gru_dest is 'o c�digo de um produto';
comment on column pedi_125.sub_dest is 'o c�digo de um produto';
comment on column pedi_125.ite_dest is 'o c�digo de um produto';
comment on column pedi_125.qtde_dest is 'a quantidade desejada';
comment on column pedi_125.seq_ped is 'novos itens no pedido';
