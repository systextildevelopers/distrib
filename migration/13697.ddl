create or replace trigger inter_tr_estq_310_trace
before insert or
       delete
       on estq_310
for each row

declare
   v_periodo_estoque    date;
   v_flag_fechamento    empr_001.flag_fechamento%type;
   v_dep_inativo        number;
   v_tran_inativa       number;
   v_data_lim_inativo   date;
   v_dtfim_fecha_estq   date;
   v_flag_fecha_estq    number;
   v_emp_ccusto         number;
   v_reg_hdoc_060       number;
   v_cta_estoque        number;
   v_bloq_hdoc_060      number;
   v_situacao_deposito  basi_205.situacao_deposito%type;
   v_empresa_deposito   basi_205.local_deposito%type;
   v_data_pre_fech      date;
   v_unidade_medida     basi_030.unidade_medida%type;
   v_nome_programa      hdoc_090.programa%type;
/*
   v_sid                number;
   v_osuser             hdoc_090.usuario_rede%type;
*/
   v_existe                  number;
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(20);
   ws_locale_usuario         varchar2(5);
begin

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

end inter_tr_estq_310_trace;
/

drop trigger inter_tr_estq_310_trace;
