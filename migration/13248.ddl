insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('tmrp_f013', 'Modificar Per�odo de Produ��o por Empresa', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, 
ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'tmrp_f013', 'NENHUM', 0, 0, 'N', 'N', 'N', 'N');

update hdoc_036
   set hdoc_036.descricao       = 'Modificar Per�odo de Produ��o por Empresa'
 where hdoc_036.codigo_programa = 'tmrp_f013'
   and hdoc_036.locale          = 'es_ES';
   
commit;

exec inter_pr_recompile;