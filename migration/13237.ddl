alter table obrf_158 add subtot_danfe_cst_icms VARCHAR2(1) default 'N';

comment on column obrf_158.subtot_danfe_cst_icms
  is 'define se a impressao da danfe ira mostrar totalizador por cst e aliquota de icms';
/
exec inter_pr_recompile;