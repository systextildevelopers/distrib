create or replace view pcpc_020_040_045
(periodo_producao, ordem_producao, ordem_principal, estagio_posicao, codigo_empresa, pedido_venda, nivel_estrutura, referencia_peca, programado, produzido, conserto, qtde_2a_qual, qtde_perdas, data_saida)
as
select
pcpc_020.periodo_producao,
pcpc_020.ordem_producao,
pcpc_020.ordem_principal,
pcpc_040.codigo_estagio,
pcpc_040.codigo_empresa,
pcpc_020.pedido_venda,
pcpc_040.proconf_nivel99,
pcpc_040.proconf_grupo,
pcpc_020.qtde_programada,
sum(pcpc_045.qtde_produzida),
sum(pcpc_045.qtde_conserto),
sum(pcpc_045.qtde_pecas_2a),
sum(pcpc_045.qtde_perdas),
max(pcpc_045.data_producao)
from pcpc_020,pcpc_040,pcpc_045
where pcpc_020.ordem_producao   = pcpc_040.ordem_producao
  and pcpc_020.ultimo_estagio   = pcpc_040.codigo_estagio
  and pcpc_040.periodo_producao = pcpc_045.pcpc040_perconf
  and pcpc_040.ordem_confeccao  = pcpc_045.pcpc040_ordconf
  and pcpc_040.codigo_estagio   = pcpc_045.pcpc040_estconf
group by pcpc_020.periodo_producao,
         pcpc_020.ordem_producao,
         pcpc_020.ordem_principal,
         pcpc_040.codigo_estagio,
         pcpc_040.codigo_empresa,
         pcpc_020.pedido_venda,
         pcpc_040.proconf_nivel99,
         pcpc_040.proconf_grupo,
         pcpc_020.qtde_programada;

exec inter_pr_recompile;