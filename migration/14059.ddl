alter table basi_240
modify cod_enquadramento_ipi default '999';


update basi_240
set cod_enquadramento_ipi = '999'
where trim(cod_enquadramento_ipi) is null;

commit;
/