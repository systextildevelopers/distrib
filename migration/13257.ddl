create table OBRF_783
(numero_solicitacao     number(9)       default 0        not null,
 cod_empresa_ent        number(3)       default 0        not null,
 numero_nota_ent        number(9)       default 0        not null,
 serie_nota_ent         varchar2(3)     default ' '      not null,
 cgc9_fornecedor        number(9)       default 0        not null,
 cgc4_fornecedor        number(4)       default 0        not null,
 cgc2_fornecedor        number(2)       default 0        not null,
 data_emissao           date,
 quantidade             number(15,3)    default 0        not null,
 flag_marcado           number(1)       default 0        not null
);

alter table OBRF_783
add constraint pk_OBRF_783 primary key (numero_solicitacao, numero_nota_ent, serie_nota_ent, cgc9_fornecedor, cgc4_fornecedor, cgc2_fornecedor);
                                        
comment on column OBRF_783.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_783.cod_empresa_ent      is 'C�digo da Empresa da Nota Fiscal de Entrada encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_783.numero_nota_ent      is 'N�mero da Nota Fiscal de Entrada encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_783.serie_nota_ent       is 'S�rie da Nota Fiscal de Entrada encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_783.cgc9_fornecedor      is 'CNPJ9 do Fornecedor da Nota Fiscal de Entrada encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_783.cgc4_fornecedor      is 'CNPJ9 do Fornecedor da Nota Fiscal de Entrada encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_783.cgc2_fornecedor      is 'CNPJ9 do Fornecedor da Nota Fiscal de Entrada encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_783.data_emissao         is 'Data de Emiss�o da Nota Fiscal de Entrada encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_783.quantidade           is 'Quantidade Total da Nota Fiscal de Entrada encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_783.flag_marcado         is 'Flag para indicar se o registro (Nota Fiscal de Entrada) foi marcado (1) ou n�o foi marcado (0) para considerar na execu��o do processo';

comment on table OBRF_783                       is 'Tabela respons�vel por gravar as Notas Fiscais de Entrada encontradas nas consultas feitas pelo usu�rio para cada execu��o do processo.';

exec inter_pr_recompile;
