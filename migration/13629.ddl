create table obrf_060_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(20) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(20) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  CODIGO_EMPRESA_OLD     NUMBER(3) default 0 ,  
  CODIGO_EMPRESA_NEW     NUMBER(3) default 0 , 
  FORNECEDOR9_OLD        NUMBER(9) default 0 ,  
  FORNECEDOR9_NEW        NUMBER(9) default 0 , 
  FORNECEDOR4_OLD        NUMBER(4) default 0 ,  
  FORNECEDOR4_NEW        NUMBER(4) default 0 , 
  FORNECEDOR2_OLD        NUMBER(2) default 0 ,  
  FORNECEDOR2_NEW        NUMBER(2) default 0 , 
  DOCUMENTO_OLD          NUMBER(9) default 0 ,  
  DOCUMENTO_NEW          NUMBER(9) default 0 , 
  SERIE_OLD              VARCHAR2(3) default ' ' ,  
  SERIE_NEW              VARCHAR2(3) default ' ' , 
  SEQUENCIA_NOTA_OLD     NUMBER(9) default 0 ,  
  SEQUENCIA_NOTA_NEW     NUMBER(9) default 0 , 
  NIVEL_ESTRUTURA_OLD    VARCHAR2(1) default ' ',  
  NIVEL_ESTRUTURA_NEW    VARCHAR2(1) default ' ', 
  GRUPO_ESTRUTURA_OLD    VARCHAR2(5) default ' ',  
  GRUPO_ESTRUTURA_NEW    VARCHAR2(5) default ' ', 
  SUBGRUPO_ESTRUTURA_OLD VARCHAR2(3) default ' ',  
  SUBGRUPO_ESTRUTURA_NEW VARCHAR2(3) default ' ', 
  ITEM_ESTRUTURA_OLD     VARCHAR2(6) default ' ',  
  ITEM_ESTRUTURA_NEW     VARCHAR2(6) default ' ', 
  QTDE_NFE_OLD           NUMBER(14,3) default 0.00,  
  QTDE_NFE_NEW           NUMBER(14,3) default 0.00, 
  QTDE_RETORNO_DEVOL_OLD NUMBER(14,3) default 0.00,  
  QTDE_RETORNO_DEVOL_NEW NUMBER(14,3) default 0.00, 
  QTDE_RETORNO_VENDA_OLD NUMBER(14,3) default 0.00,  
  QTDE_RETORNO_VENDA_NEW NUMBER(14,3) default 0.00, 
  VALOR_UNITARIO_OLD     NUMBER(15,5) default 0.00,  
  VALOR_UNITARIO_NEW     NUMBER(15,5) default 0.00, 
  SALDO_FISICO_OLD       NUMBER(14,3) default 0.00,  
  SALDO_FISICO_NEW       NUMBER(14,3) default 0.00, 
  DATA_ENTRADA_OLD       DATE, 
  DATA_ENTRADA_NEW       DATE 
) ;
