create or replace view inter_vi_cons_consig_it as
select codigo_empresa,
    fornecedor9,
    fornecedor4,
    fornecedor2,
    nivel_estrutura,
    grupo_estrutura,
    subgrupo_estrutura,
    item_estrutura,
    sum(saldo_fisico) saldo_consignado,
    sum(saldo_fisico*valor_unitario) valor_consignado
from obrf_060
group by
    codigo_empresa,
    fornecedor9,
    fornecedor4,
    fornecedor2,
    nivel_estrutura,
    grupo_estrutura,
    subgrupo_estrutura,
    item_estrutura;

exec inter_pr_recompile;
