create table OBRF_761
(codigo_passo        number(5)       default 0        not null,
 empresa             number(3)       default 0        not null,
 serie               varchar2(3)     default ' '      not null,
 especie             varchar2(5)     default ' '      not null,
 nat_oper            number(3)       default 0        not null,
 est_oper            varchar2(2)     default ' '      not null,
 cond_pagto          number(3)       default 0        not null,
 col_tabela          number(2)       default 0        not null,
 mes_tabela          number(2)       default 0        not null,
 seq_tabela          number(2)       default 0        not null,
 cgc9                number(9)       default 0        not null,
 cgc4                number(4)       default 0        not null,
 cgc2                number(2)       default 0        not null,
 seq_end_entr        number(3)       default 0        not null,
 cod_rep_cliente     number(5)       default 0        not null,
 tipo_comissao       number(1)       default 0        not null,
 perc_repres         number(6,2)     default 0.00     not null,
 seq_end_cobr        number(3)       default 0        not null,
 transpor_forne9     number(9)       default 0        not null,
 transpor_forne4     number(4)       default 0        not null,
 transpor_forne2     number(2)       default 0        not null,
 via_transporte      number(1)       default 0        not null,
 tipo_frete          number(1)       default 0        not null,
 marca_volumes       varchar2(15)    default ' ',
 especie_volume      varchar2(10)    default ' ',
 desconto1           number(6,2)     default 0.00     not null,
 vlr_desc_especial   number(15,2)    default 0.00     not null,
 encargos            number(9,5)     default 0.00000  not null,
 portador            number(3)       default 0        not null,
 historico_cont      number(4)       default 0        not null,
 deposito_saida      number(3)       default 0        not null,
 cod_obs1            number(6)       default 0        not null,
 desc_obs1           varchar2(55)    default ' ',
 cod_obs2            number(6)       default 0        not null,
 desc_obs2           varchar2(55)    default ' ',
 cod_obs3            number(6)       default 0        not null,
 desc_obs3           varchar2(55)    default ' ',
 cod_obs4            number(6)       default 0        not null,
 desc_obs4           varchar2(55)    default ' ' 
);
 
alter table OBRF_761
add constraint pk_OBRF_761 primary key (codigo_passo);


comment on column OBRF_761.codigo_passo        is 'C�digo do passo que estes par�metros para nota fiscal de sa�da est�o relacionados';
comment on column OBRF_761.empresa             is 'C�digo da empresa de sa�da da nota fiscal que ser� gerada por este passo';
comment on column OBRF_761.serie               is 'S�rie da nota fiscal de sa�da que ser� gerada por este passo';
comment on column OBRF_761.especie             is 'Esp�cie da nota fiscal de sa�da que ser� gerada por este passo';
comment on column OBRF_761.nat_oper            is 'Natureza de Opera��o da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.est_oper            is 'Estado da Natureza de Opera��o da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.cond_pagto          is 'Condi��o de pagamento da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.col_tabela          is 'C�digo (cole��o) da tabela de pre�os da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.mes_tabela          is 'C�digo (m�s) da tabela de pre�os da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.seq_tabela          is 'C�digo (sequ�ncia) da tabela de pre�os da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.cgc9                is 'CNPJ do cliente da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.cgc4                is 'CNPJ do cliente da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.cgc2                is 'CNPJ do cliente da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.seq_end_entr        is 'Sequ�ncia do endere�o de entrega para o cliente da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.cod_rep_cliente     is 'C�digo do representante da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.tipo_comissao       is 'Tipo da comiss�o para o representante da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.perc_repres         is 'Percentual de comiss�o para o representante da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.seq_end_cobr        is 'Sequ�ncia do endere�o de cobran�a para o cliente da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.transpor_forne9     is 'CNPJ da transportadora da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.transpor_forne4     is 'CNPJ da transportadora da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.transpor_forne2     is 'CNPJ da transportadora da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.via_transporte      is 'Via de transporte para o frete da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.tipo_frete          is 'Tipo  de frete da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.marca_volumes       is 'Marca dos volumes da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.especie_volume      is 'Esp�cie dos volumes da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.desconto1           is 'Percentual de desconto da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.vlr_desc_especial   is 'Valor do desconto especial dado para o cliente da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.encargos            is 'Percentual de encargos da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.portador            is 'C�digo do portador da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.historico_cont      is 'Hist�rico cont�bil da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.deposito_saida      is 'Dep�sito de sa�da da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.cod_obs1            is 'C�digo da mensagem para observa��es da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.desc_obs1           is 'Descri��o da mensagem para observa��es da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.cod_obs2            is 'C�digo da mensagem para observa��es da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.desc_obs2           is 'Descri��o da mensagem para observa��es da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.cod_obs3            is 'C�digo da mensagem para observa��es da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.desc_obs3           is 'Descri��o da mensagem para observa��es da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.cod_obs4            is 'C�digo da mensagem para observa��es da nota fiscal de sa�da que ser� gerada por este passo ';
comment on column OBRF_761.desc_obs4           is 'Descri��o da mensagem para observa��es da nota fiscal de sa�da que ser� gerada por este passo ';

comment on table OBRF_761                      is 'Tabela respons�vel por gravar os par�metros para gera��o de nota fiscal de sa�da de um passo.';

exec inter_pr_recompile;