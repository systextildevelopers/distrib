-- Add/modify columns 
alter table PCPC_100 add codigo_familia number(9) default 0;

-- Add comments to the columns 
comment on column PCPC_100.codigo_familia
  is 'C�digo da fam�lia que esta sendo executado a opera��o';

exec inter_pr_recompile;