create table PCPC_601
(
  codigo_familia      NUMBER(9)    default 0 not null,
  ano                 NUMBER(4)    default 0 not null,
  mes                 NUMBER(2)    default 0 not null,
  valor_minuto        NUMBER(17,2) default 0.00
);

alter table PCPC_601
  add constraint PK_PCPC_601 primary key (codigo_familia,ano,mes);

comment on table pcpc_601
  is 'Cadastro de valor por minuto por c�lula';

comment on column pcpc_601.codigo_familia
  is 'C�digo da familia';

comment on column pcpc_601.ano
  is 'Ano do valor minuto';

comment on column pcpc_601.mes
  is 'M�s do valor minuto';

comment on column pcpc_601.valor_minuto
  is 'Valor minuto para a familia celula';

create synonym systextilrpt.pcpc_601 for pcpc_601; 

exec inter_pr_recompile;