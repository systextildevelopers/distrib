create table OBRF_831
(numero_solicitacao     number(9)       default 0        not null,
 seq_passo              number(4)       default 0        not null,
 cod_empresa            number(3)       default 0        not null,
 num_nota_fiscal        number(9)       default 0        not null,
 serie_nota_fisc        varchar2(3)     default ' '      not null,
 data_emissao           date,
 quantidade_total       number(15,3)    default 0.000    not null,
 valor_total            number(15,2)    default 0.00     not null
);

alter table OBRF_831
add constraint pk_OBRF_831 primary key (numero_solicitacao, seq_passo, cod_empresa, num_nota_fiscal, serie_nota_fisc);
                                        
comment on column OBRF_831.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_831.seq_passo            is 'Sequ�ncia do passo da execu��o do processo';

comment on column OBRF_831.cod_empresa          is 'C�digo da empresa da nota fiscal de sa�da que foi gerada na execu��o do processo neste passo';
comment on column OBRF_831.num_nota_fiscal      is 'N�mero da nota fiscal de sa�da que foi gerada na execu��o do processo neste passo';
comment on column OBRF_831.serie_nota_fisc      is 'S�rie da nota fiscal de sa�da que foi gerada na execu��o do processo neste passo';
comment on column OBRF_831.data_emissao         is 'Data de emiss�o da nota fiscal de sa�da que foi gerada na execu��o do processo neste passo';
comment on column OBRF_831.quantidade_total     is 'Quantidade total da nota fiscal de sa�da que foi gerada na execu��o do processo neste passo';
comment on column OBRF_831.valor_total          is 'Valor total da nota fiscal de sa�da que foi gerada na execu��o do processo neste passo';

comment on table OBRF_831                       is 'Tabela respons�vel por gravar a nota fiscal de sa�da que foi gerada na execu��o do processo.';

exec inter_pr_recompile;
