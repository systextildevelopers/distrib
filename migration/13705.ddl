alter table FATU_050 add VALOR_TRIBUTOS NUMBER(15,2);

alter table FATU_050 modify VALOR_TRIBUTOS default 0;

prompt "ATEN��O! O comando abaixo devera demorar para executar, e n�o deve ser abortado."
declare
   v_contador  number (3) := 0;
begin
   for reg_fatu_050 in (select rowid from fatu_050 where fatu_050.valor_tributos is null)
   loop
      update fatu_050
      set    fatu_050.valor_tributos = 0
      where  fatu_050.rowid = reg_fatu_050.rowid;

      v_contador :=  v_contador + 1;

         if v_contador > 100 then
            commit;
            v_contador := 0;
         end if;
   end loop;
   commit;
end;

/
exec inter_pr_recompile;
