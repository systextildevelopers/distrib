CREATE OR REPLACE VIEW "INTER_VI_PEDIDOS_CLI" AS 
select pedi_100.* , pedi_010.nome_cliente, inter_fn_calc_desconto_triplo(pedi_100.valor_liq_itens, pedi_100.desconto1, pedi_100.desconto2, pedi_100.desconto3) as valor_liquido from pedi_100, pedi_010
where pedi_010.cgc_9 = pedi_100.cli_ped_cgc_cli9
and pedi_010.cgc_4 = pedi_100.cli_ped_cgc_cli4
and pedi_010.cgc_2 = pedi_100.cli_ped_cgc_cli2;
 


exec inter_pr_recompile;

