create or replace view estq_300_estq_310_dep as 
select
 estq_300.codigo_deposito,               estq_300.nivel_estrutura,           estq_300.grupo_estrutura,
 estq_300.subgrupo_estrutura,            estq_300.item_estrutura,            estq_300.data_movimento,
 estq_300.sequencia_ficha,               estq_300.sequencia_insercao,        estq_300.numero_lote,
 estq_300.numero_documento,              estq_300.serie_documento,           estq_300.cnpj_9,
 estq_300.cnpj_4,                        estq_300.cnpj_2,                    estq_300.sequencia_documento,
 estq_300.codigo_transacao,              estq_300.entrada_saida,             estq_300.centro_custo,
 estq_300.quantidade,                    estq_300.saldo_fisico,              estq_300.valor_movimento_unitario,
 estq_300.valor_movimento_unitario_proj, estq_300.valor_contabil_unitario,   estq_300.preco_medio_unitario,
 estq_300.preco_medio_unitario_proj,     estq_300.saldo_financeiro,          estq_300.saldo_financeiro_proj,
 estq_300.grupo_maquina,                 estq_300.subgru_maquina,            estq_300.numero_maquina,
 estq_300.ordem_servico,                 estq_300.contabilizado,             estq_300.usuario_systextil,
 estq_300.processo_systextil,            estq_300.data_insercao,             estq_300.usuario_rede,
 estq_300.maquina_rede,                  estq_300.aplicativo,                estq_300.tabela_origem,
 estq_300.valor_movto_unit_estimado,     estq_300.preco_medio_unit_estimado, estq_300.saldo_financeiro_estimado,
 estq_300.valor_total,                   estq_300.projeto,                   estq_300.subprojeto,
 estq_300.servico,                       basi_205.local_deposito,            basi_205.controla_ficha_cardex,
 estq_005.calcula_preco,
 case when estq_300.entrada_saida = 'E' and estq_005.calcula_preco = 1
 then estq_300.data_movimento
 else cast(Last_day(estq_300.data_movimento) as date)
 end as data_movimento_ult
from estq_300, basi_205, estq_005
where estq_300.codigo_deposito = basi_205.codigo_deposito
  and estq_300.codigo_transacao = estq_005.codigo_transacao
  and (estq_300.numero_lote = 0 or (estq_300.numero_lote <> 0 
                                    and not exists (select 1 from supr_010 
                                                    where supr_010.lote_fornecedor = estq_300.numero_lote 
                                                      and supr_010.lote_consignado = 1)))
UNION ALL select
 estq_310.codigo_deposito,               estq_310.nivel_estrutura,           estq_310.grupo_estrutura,
 estq_310.subgrupo_estrutura,            estq_310.item_estrutura,            estq_310.data_movimento,
 estq_310.sequencia_ficha,               estq_310.sequencia_insercao,        estq_310.numero_lote,
 estq_310.numero_documento,              estq_310.serie_documento,           estq_310.cnpj_9,
 estq_310.cnpj_4,                        estq_310.cnpj_2,                    estq_310.sequencia_documento,
 estq_310.codigo_transacao,              estq_310.entrada_saida,             estq_310.centro_custo,
 estq_310.quantidade,                    estq_310.saldo_fisico,              estq_310.valor_movimento_unitario,
 estq_310.valor_movimento_unitario_proj, estq_310.valor_contabil_unitario,   estq_310.preco_medio_unitario,
 estq_310.preco_medio_unitario_proj,     estq_310.saldo_financeiro,          estq_310.saldo_financeiro_proj,
 estq_310.grupo_maquina,                 estq_310.subgru_maquina,            estq_310.numero_maquina,
 estq_310.ordem_servico,                 estq_310.contabilizado,             estq_310.usuario_systextil,
 estq_310.processo_systextil,            estq_310.data_insercao,             estq_310.usuario_rede,
 estq_310.maquina_rede,                  estq_310.aplicativo,                estq_310.tabela_origem,
 estq_310.valor_movto_unit_estimado,     estq_310.preco_medio_unit_estimado, estq_310.saldo_financeiro_estimado,
 estq_310.valor_total,                   estq_310.projeto,                   estq_310.subprojeto,
 estq_310.servico,                       basi_205.local_deposito,            basi_205.controla_ficha_cardex,
 estq_005.calcula_preco,
 case when estq_310.entrada_saida = 'E' and estq_005.calcula_preco = 1
 then estq_310.data_movimento
 else cast(Last_day(estq_310.data_movimento) as date)
 end as data_movimento_ult
from estq_310, basi_205, estq_005
where estq_310.codigo_deposito = basi_205.codigo_deposito
  and estq_310.codigo_transacao = estq_005.codigo_transacao
  and (estq_310.numero_lote = 0 or (estq_310.numero_lote <> 0 
                                    and not exists (select 1 from supr_010 
                                                    where supr_010.lote_fornecedor = estq_310.numero_lote 
                                                      and supr_010.lote_consignado = 1)));

/

exec inter_pr_recompile;                                                         
