alter table fatu_504
add (dep_terc_sai_1 number (3) default 0,
     dep_terc_sai_2 number (3) default 0,
     dep_terc_sai_4 number (3) default 0,
     dep_terc_sai_7 number (3) default 0,
     dep_terc_sai_9 number (3) default 0,
     dep_terc_ent_1 number (3) default 0,
     dep_terc_ent_2 number (3) default 0,
     dep_terc_ent_4 number (3) default 0,
     dep_terc_ent_7 number (3) default 0,
     dep_terc_ent_9 number (3) default 0);
     
comment on column fatu_504.dep_terc_sai_1 is
'Armazena o dep�sito de n�vel 1 em poder de terceiros, onde ser� feita a sa�da dos itens nas notas de entrada.';

comment on column fatu_504.dep_terc_sai_2 is
'Armazena o dep�sito de n�vel 2 em poder de terceiros, onde ser� feita a sa�da dos itens nas notas de entrada.';

comment on column fatu_504.dep_terc_sai_4 is
'Armazena o dep�sito de n�vel 4 em poder de terceiros, onde ser� feita a sa�da dos itens nas notas de entrada.';

comment on column fatu_504.dep_terc_sai_7 is
'Armazena o dep�sito de n�vel 7 em poder de terceiros, onde ser� feita a sa�da dos itens nas notas de entrada.';

comment on column fatu_504.dep_terc_sai_9 is
'Armazena o dep�sito de n�vel 9 em poder de terceiros, onde ser� feita a sa�da dos itens nas notas de entrada.';

comment on column fatu_504.dep_terc_ent_1 is
'Armazena o dep�sito de n�vel 1 em poder de terceiros, onde ser� feita a entrada dos itens nas notas de sa�da.';

comment on column fatu_504.dep_terc_ent_2 is
'Armazena o dep�sito de n�vel 2 em poder de terceiros, onde ser� feita a entrada dos itens nas notas de sa�da.';

comment on column fatu_504.dep_terc_ent_4 is
'Armazena o dep�sito de n�vel 4 em poder de terceiros, onde ser� feita a entrada dos itens nas notas de sa�da.';

comment on column fatu_504.dep_terc_ent_7 is
'Armazena o dep�sito de n�vel 7 em poder de terceiros, onde ser� feita a entrada dos itens nas notas de sa�da.';

comment on column fatu_504.dep_terc_ent_9 is
'Armazena o dep�sito de n�vel 9 em poder de terceiros, onde ser� feita a entrada dos itens nas notas de sa�da.';

exec inter_pr_recompile;