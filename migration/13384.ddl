insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('loja_f651', 'Itens Venda Atacado', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'loja_f651', ' ', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'loja_f651', ' ', 0, 0, 'S', 'S', 'S', 'S');


update hdoc_036
   set hdoc_036.descricao       = 'Itens Venda Atacado'
 where hdoc_036.codigo_programa = 'loja_f651'
   and hdoc_036.locale          = 'es_ES';
commit;