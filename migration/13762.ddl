create or replace view inter_vi_pcpc_020_040_cons as
select pcpc_040.periodo_producao, pcpc_040.ordem_confeccao,
       pcpc_040.ordem_producao,   pcpc_020.ordem_principal,
       pcpc_040.proconf_grupo,    pcpc_040.proconf_subgrupo,
       pcpc_040.proconf_item,     pcpc_040.qtde_pecas_prog,
       pcpc_040.codigo_empresa
from   pcpc_020, pcpc_040
where pcpc_020.ordem_producao   = pcpc_040.ordem_producao
  and pcpc_040.estagio_anterior = 0
order by pcpc_040.periodo_producao, pcpc_040.ordem_confeccao,
         pcpc_040.ordem_producao,   pcpc_020.ordem_principal,
         pcpc_040.proconf_grupo,    pcpc_040.proconf_subgrupo,
         pcpc_040.proconf_item,     pcpc_040.qtde_pecas_prog;

exec inter_pr_recompile;