alter table empr_002
add (perc_toleran_prod_gener number(5,2) default 0);

comment on column empr_002.perc_toleran_prod_gener
is 'percentual de tolerancia usado na pesagem de produtos genericos';

execute inter_pr_recompile;
