insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('loja_f652', 'Fechamento da Venda', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'loja_f652', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Fechamento da Venda'
 where hdoc_036.codigo_programa = 'loja_f652'
   and hdoc_036.locale          = 'es_ES';
commit;
