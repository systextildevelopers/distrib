alter table pedi_080
add (permite_nf_mesmo_cnpj number(1) default 1);
    
comment on column pedi_080.permite_nf_mesmo_cnpj is 'Permite digitar nota para cpnj da empresa 1 - Sim ou 2 - N�o.';   

exec inter_pr_recompile;