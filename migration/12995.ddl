insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('crec_m120', 'Menu de Comiss�o em Cascata', 1,0);

commit;

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'crec_m120', 'menu_ad28', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Men� de Comisi�n en Cascata'
 where hdoc_036.codigo_programa = 'crec_m120'
   and hdoc_036.locale          = 'es_ES';

commit;

exec inter_pr_recompile;
