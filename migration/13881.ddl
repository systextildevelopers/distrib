alter table empr_050
add  ( faz_afericao number(1) default 0,
      frequencia_afericao number(3) default 0,
      peso_afericao number(10,6) default 0.0,
      variacao_afericao number(6,2) default 0.0,
      pesagens_afericao number(2) default 0);

/

exec inter_pr_recompile;