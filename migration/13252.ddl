insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f760', 'Cadastro de Passos de Processos', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f760', 'menu_ad101', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Registro de pasos de proceso'
 where hdoc_036.codigo_programa = 'obrf_f760'
   and hdoc_036.locale          = 'es_ES';
commit;
