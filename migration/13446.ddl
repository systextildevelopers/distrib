ALTER TABLE obrf_152 ADD (
      tp_via_transp number(2) DEFAULT 0,
      v_afrmm number(13,2) DEFAULT 0,      
      tp_inter_medio number(1) DEFAULT 0,
      cnpj number(14) DEFAULT 0,
      uf_terceiro varchar(2) DEFAULT '',
      n_draw number(11) DEFAULT 0
 );
 exec inter_pr_recompile;