alter table rcnb_080
add data_lancto date default sysdate not null;

comment on column rcnb_080.data_lancto
is 'Identifica data de lanšamento';

alter table rcnb_080 
drop constraint PK_RCNB_080;

alter table rcnb_080
add constraint PK_RCNB_080 primary key (codigo_empresa, mes, ano, centro_custo, cgc9, cgc4, cgc2, num_titulo, parcela, data_lancto);

execute inter_pr_recompile;