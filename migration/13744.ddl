insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_e095', 'Quantidades Vendidas de Pe�as por Data de Entrega / Emiss�o.', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_e095', 'menu_ad52', 1, 50, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Vendido cantidades de piezas de Entrega Fecha / Edici�n.'
 where hdoc_036.codigo_programa = 'pedi_e095'
   and hdoc_036.locale          = 'es_ES';

   insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_f821', 'Cadastro Tempor�rio Intervalo de Tamanhos.', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_f821', 'NENHUM', 1, 50, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Registro temporal gama Tama�os.'
 where hdoc_036.codigo_programa = 'pedi_f821'
   and hdoc_036.locale          = 'es_ES';

   insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_f822', 'Cadastro Tempor�rio Intervalo de Tamanhos.', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_f822', 'NENHUM', 1, 50, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Registro temporal gama Tama�os.'
 where hdoc_036.codigo_programa = 'pedi_f822'
   and hdoc_036.locale          = 'es_ES';
commit;
