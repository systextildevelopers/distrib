alter table pedi_051 add (diretorio_banco varchar(15) default '');
comment on column pedi_051.diretorio_banco is 'Nome do diretorio onde o arquivo deve ser salvo no banco. Para alguns bancos esse deve ser o nome do arquivo';

exec inter_pr_recompile;