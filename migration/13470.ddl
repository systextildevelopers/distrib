create table OBRF_834
(numero_solicitacao     number(9)       default 0    not null,
 seq_passo              number(4)       default 0    not null,
 cod_empresa            number(3)       default 0    not null,
 num_nota_fiscal        number(9)       default 0    not null,
 serie_nota_fisc        varchar2(3)     default '',
 seq_item_nfisc         number(5)       default 0,
 nivel                  varchar2(1)     default '',
 grupo                  varchar2(5)     default '',
 subgrupo               varchar2(3)     default '',
 item                   varchar2(6)     default '',
 lote_acomp             number(6)       default 0,
 numero_ordem           number(6)       default 0,
 seq_areceber           number(3)       default 0,
 sequencia              number(3)       default 0,
 qtde_emitida           number(14,3)    default 0.000
);

alter table OBRF_834
add constraint pk_OBRF_834 primary key (numero_solicitacao, seq_passo, cod_empresa, num_nota_fiscal, serie_nota_fisc, 
    seq_item_nfisc, nivel, grupo, subgrupo, item, lote_acomp, numero_ordem, seq_areceber, sequencia);
                                        
comment on column OBRF_834.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_834.qtde_emitida         is 'Indica o saldo do produto entre a quantidade da nota e da ordem';

comment on table OBRF_834                       is 'Controle de Saldo Itens Gerados X Ordens de servico';

exec inter_pr_recompile;
