update hdoc_035
set hdoc_035.descricao = 'Cadastro de Lote de Fibras/Fios'
where hdoc_035.codigo_programa = 'pcpf_f035';

update hdoc_036
set hdoc_036.descricao = 'Cadastro de Lote de Fibras/Fios'
where hdoc_036.codigo_programa = 'pcpf_f035'
and hdoc_036.locale = 'es_es';

commit work;