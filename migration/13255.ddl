create table OBRF_781
(numero_solicitacao     number(9)       default 0        not null,
 ordem_producao         number(9)       default 0        not null,
 referencia             varchar2(5)     default ' '      not null,
 quantidade             number(9)       default 0        not null,
 flag_marcado           number(1)       default 0        not null
);

alter table OBRF_781
add constraint pk_OBRF_781 primary key (numero_solicitacao, ordem_producao);
                                        
comment on column OBRF_781.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_781.ordem_producao       is 'N�mero da Ordem de Produ��o encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_781.referencia           is 'Refer�ncia da Ordem de Produ��o encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_781.quantidade           is 'Quantidade da Ordem de Produ��o encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_781.flag_marcado         is 'Flag para indicar se o registro (ordem de produ��o) foi marcado (1) ou n�o foi marcado (0) para considerar na execu��o do processo';

comment on table OBRF_781                       is 'Tabela respons�vel por gravar as Ordens de Produ��o encontradas nas consultas feitas pelo usu�rio para cada execu��o do processo.';

exec inter_pr_recompile;
