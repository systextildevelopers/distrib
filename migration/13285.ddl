create table OBRF_789
(numero_solicitacao     number(9)       default 0        not null,
 seq_passo              number(4)       default 0        not null,
 numero_bcm             number(9)       default 0        not null,
 numero_formulario      number(9)       default 0        not null,
 ordem_producao         number(9)       default 0        not null,
 cod_empresa_orig       number(3)       default 0        not null,
 cod_empresa_dest       number(3)       default 0        not null,
 data_emissao           date,
 seq_item_bcm           number(5)       default 0        not null,
 nivel                  varchar2(1)     default ' '      not null,
 grupo                  varchar2(5)     default ' '      not null,
 subgrupo               varchar2(3)     default ' '      not null,
 item                   varchar2(6)     default ' '      not null,
 lote_acomp             number(6)       default 0,
 valor_unitario         number(15,5)    default 0.00000  not null,
 quantidade             number(15,3)    default 0.000    not null,
 flag_marcado           number(1)       default 0        not null
);

alter table OBRF_789
add constraint pk_OBRF_789 primary key (numero_solicitacao, seq_passo, numero_bcm, seq_item_bcm);
                                        
comment on column OBRF_789.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_789.seq_passo            is 'Sequ�ncia do passo da execu��o do processo';

comment on column OBRF_789.numero_bcm           is 'N�mero (interno) do BCM da execu��o do processo neste passo';
comment on column OBRF_789.numero_formulario    is 'N�mero do formul�rio (tipogr�fico impresso) do BCM da execu��o do processo neste passo';
comment on column OBRF_789.ordem_producao       is 'Ordem de Produ��o do BCM da execu��o do processo neste passo';
comment on column OBRF_789.cod_empresa_orig     is 'C�digo da empresa de Origem do BCM da execu��o do processo neste passo';
comment on column OBRF_789.cod_empresa_dest     is 'C�digo da empresa de Destino do BCM da execu��o do processo neste passo';
comment on column OBRF_789.data_emissao         is 'Data de emiss�o do BCM a ser gerado na execu��o do processo neste passo';
comment on column OBRF_789.seq_item_bcm         is 'Sequencial do item do BCM da execu��o do processo neste passo';
comment on column OBRF_789.nivel                is 'N�vel do produto do BCM da execu��o do processo neste passo';
comment on column OBRF_789.grupo                is 'Grupo do produto do BCM da execu��o do processo neste passo';
comment on column OBRF_789.subgrupo             is 'Subgrupo do produto do BCM da execu��o do processo neste passo';
comment on column OBRF_789.item                 is 'Item do produto do BCM da execu��o do processo neste passo';
comment on column OBRF_789.lote_acomp           is 'Lote do produto do BCM da execu��o do processo neste passo';

comment on column OBRF_789.valor_unitario       is 'Valor unit�rio do item do BCM da execu��o do processo neste passo';
comment on column OBRF_789.quantidade           is 'Quantidade do item do BCM da execu��o do processo neste passo';
comment on column OBRF_789.flag_marcado         is 'Flag para indicar se o item foi considerado (1) ou n�o (0) na gera��o do BCM da execu��o do processo neste passo';

comment on table OBRF_789                       is 'Tabela respons�vel por gravar os �tens do BCM da execu��o do processo.';

exec inter_pr_recompile;
