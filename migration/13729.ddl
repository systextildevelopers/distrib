alter table crec_050
add (nr_linha number(6) default 0 );

comment on column crec_050.nr_linha is 'Identifica o nr da linha no arquivo para ordenar a execucao no retorno';

exec inter_pr_recompile;