alter table obrf_780 add
nr_formulario_bcm_ini number(9) default 0;

comment on column obrf_780.nr_formulario_bcm_ini is
'N�mero inicial do formul�rio do BCM que est� na impressora. O n�mero informado pelo usu�rio neste campo � o n�mero do formul�rio impresso (timbrado) no layout do BCM que est� posicionado na impressora e que vai ser o pr�ximo a ser impresso. O n�mero informado neste campo ser� gravado no BCM do Syst�xtil (OBRF_790) e ser� incrementado automaticamente para todos os passos gerados em uma execu��o de processo. A cada NOVA execu��o o usu�rio deve informar o n�mero do formulario que est� na impressora no momento.';

exec inter_pr_recompile;
