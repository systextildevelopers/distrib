create table OBRF_763
(codigo_passo        number(5)       default 0        not null,
 cod_emp_origem      number(3)       default 0        not null,
 cod_emp_destino     number(3)       default 0        not null,
 col_tabela          number(2)       default 0        not null,
 mes_tabela          number(2)       default 0        not null,
 seq_tabela          number(2)       default 0        not null
);
 
alter table OBRF_763
add constraint pk_OBRF_763 primary key (codigo_passo);

comment on column OBRF_763.codigo_passo        is 'C�digo do passo que estes par�metros para o BCM est�o relacionados';
comment on column OBRF_763.cod_emp_origem      is 'C�digo da empresa de origem do BCM que ser� gerado por este passo';
comment on column OBRF_763.cod_emp_destino     is 'C�digo da empresa de destino do BCM que ser� gerado por este passo';

comment on column OBRF_763.col_tabela          is 'Tabela de pre�os (cole��o) do BCM que ser� gerado por este passo';
comment on column OBRF_763.mes_tabela          is 'Tabela de pre�os (m�s) do BCM que ser� gerado por este passo';
comment on column OBRF_763.seq_tabela          is 'Tabela de pre�os (sequ�ncia) do BCM que ser� gerado por este passo';

comment on table OBRF_763                      is 'Tabela respons�vel por gravar os par�metros para gera��o do Boletim de Circula��o de Mercadorias (BCM) de um passo.';

exec inter_pr_recompile;