ALTER TABLE pcpc_300 ADD (opcao_leitura number(2) DEFAULT 0);
COMMENT ON COLUMN pcpc_300.opcao_leitura
   IS '0 - Todas as Op��es, 1 - Por Tag, 2 - Por Volume/Pack, 4 - Por Embalagem';
EXEC inter_pr_recompile;
/