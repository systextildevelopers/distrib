create table OBRF_770 
(codigo_processo number(4) default 0 not null, 
descricao varchar2(120) default ' ' not null, 
empresa_execucao number(3) default 0, 
ativo number(1) default 0 );

alter table OBRF_770 add 
constraint pk_OBRF_770 primary key (codigo_processo);

comment on column OBRF_770.codigo_processo is 
'C�digo do processo que possuir� os passos para execu��o posterior'; 

comment on column OBRF_770.descricao is 
'Descri��o do processo para identifica��o do que o processo dever� fazer'; 

comment on column OBRF_770.empresa_execucao is 
'Identifica a empresa que far� a execu��o do processo'; 

comment on column OBRF_770.ativo is 
'Identifica se o processo est� ativo (1) ou se est� inativo (0)';

exec inter_pr_recompile;