create table OBRF_787
(numero_solicitacao     number(9)       default 0        not null,
 seq_passo              number(4)       default 0        not null,
 ordem_producao         number(9)       default 0        not null,
 periodo_producao040    number(4)       default 0        not null,
 ordem_confeccao040     number(5)       default 0        not null,
 codigo_estagio040      number(2)       default 0        not null,
 data_emissao           date,
 seq_produto            number(5)       default 0        not null,
 nivel                  varchar2(1)     default ' '      not null,
 grupo                  varchar2(5)     default ' '      not null,
 subgrupo               varchar2(3)     default ' '      not null,
 item                   varchar2(6)     default ' '      not null,
 lote_acomp             number(6)       default 0,
 cod_deposito           number(3)       default 0        not null,
 valor_unitario         number(15,5)    default 0.00000  not null,
 quantidade             number(15,3)    default 0.000    not null,
 flag_marcado           number(1)       default 0        not null
);

alter table OBRF_787
add constraint pk_OBRF_787 primary key (numero_solicitacao, seq_passo, ordem_producao, periodo_producao040, ordem_confeccao040, codigo_estagio040, seq_produto);
                                        
comment on column OBRF_787.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_787.seq_passo            is 'Sequ�ncia do passo da execu��o do processo';

comment on column OBRF_787.ordem_producao       is 'Ordem de Produ��o da execu��o do processo neste passo';
comment on column OBRF_787.periodo_producao040  is 'Per�odo de Produ��o da execu��o do processo neste passo';
comment on column OBRF_787.ordem_confeccao040   is 'Ordem de Confec��o da execu��o do processo neste passo';
comment on column OBRF_787.codigo_estagio040    is 'C�digo do Est�gio da execu��o do processo neste passo';
comment on column OBRF_787.data_emissao         is 'Data de emiss�o da ordem de corte a ser gerada na execu��o do processo neste passo';
comment on column OBRF_787.seq_produto          is 'Sequencial do produto a ser gerado na execu��o do processo neste passo';
comment on column OBRF_787.nivel                is 'N�vel do produto a ser gerado na execu��o do processo neste passo';
comment on column OBRF_787.grupo                is 'Grupo do produto a ser gerado na execu��o do processo neste passo';
comment on column OBRF_787.subgrupo             is 'Subgrupo do produto a ser gerado na execu��o do processo neste passo';
comment on column OBRF_787.item                 is 'Item do produto a ser gerado na execu��o do processo neste passo';
comment on column OBRF_787.lote_acomp           is 'Lote do produto a ser gerado na execu��o do processo neste passo';
comment on column OBRF_787.cod_deposito         is 'Dep�sito do item a ser gerado na execu��o do processo neste passo';
comment on column OBRF_787.valor_unitario       is 'Valor unit�rio do item a ser gerado na execu��o do processo neste passo';
comment on column OBRF_787.quantidade           is 'Quantidade do item a ser gerado na execu��o do processo neste passo';
comment on column OBRF_787.flag_marcado         is 'Flag para indicar se o item foi considerado (1) ou n�o (0) a ser gerado na execu��o do processo neste passo';

comment on table OBRF_787                       is 'Tabela respons�vel por gravar os produtos das OPs a serem gerados na execu��o do processo.';

exec inter_pr_recompile;