alter table obrf_761 add 
(data_inicio_nfs_ent     date);

comment on column obrf_761.data_inicio_nfs_ent is 'Este campo serve para se parametrizar a data inicial de emiss�o para buscar as notas de entrada para fazer a devolu��o. Este campo s� ter� utilizada quando a transa��o da natureza de opera��o do par�metro do passo (OBRF_761) for do tipo DEVOLU��O (= "D")';

exec inter_pr_recompile;
