

alter table fatu_060 add nf_proc_consignacao number(9) ;
alter table fatu_060 add sr_proc_consignacao varchar2(3) ;
alter table fatu_060 add data_proc_consignacao date;


comment on column fatu_060.nf_proc_consignacao is   'Nr. da NF de devolu��o deste consignado gerada pelo processo obrf_f280.';
comment on column fatu_060.sr_proc_consignacao is   'S�rie da NF de devolu��o deste consignado gerada pelo processo obrf_f280.';
comment on column fatu_060.data_proc_consignacao is 'Data do processamento da NF de devolu��o deste produto consignado gerada pelo processo obrf_f280';


exec inter_pr_recompile;


