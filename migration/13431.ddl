insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('inte_f715', 'Integra��o de Ordens - Neoplan',0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'inte_f715', 'pcpc_menu',1 , 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Integraci�n de �rdenes - Neoplan'
where hdoc_036.codigo_programa = 'inte_f715'
  and hdoc_036.locale          = 'es_ES';
  
commit;

exec inter_pr_recompile;