alter table fatu_504
add (rpt_volume_previsto varchar2(15) default '');

comment on column fatu_504.rpt_volume_previsto
  is 'Layout utilizado na emiss�o de etiquetas de volumes previstos.';
/
exec inter_pr_recompile;