alter table obrf_761 add 
(obs_doc_origem             number(3) default 0);

comment on column obrf_761.obs_doc_origem is '';

alter table obrf_761 add 
(desc_obs_doc_origem        varchar2(55) default '');

comment on column obrf_761.desc_obs_doc_origem is '';

exec inter_pr_recompile;