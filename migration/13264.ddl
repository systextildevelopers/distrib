create table OBRF_790
(numero_bcm             number(9)       default 0        not null,
 numero_formulario      number(9)       default 0        not null,
 cod_empresa_orig       number(3)       default 0        not null,
 cod_empresa_dest       number(3)       default 0        not null,
 col_tab_preco          number(2)       default 0        not null,
 mes_tab_preco          number(2)       default 0        not null,
 seq_tab_preco          number(2)       default 0        not null,
 ordem_producao         number(9)       default 0        not null,
 data_emissao           date,
 data_saida             date,
 data_prev_ent          date,
 horario                date
);

alter table OBRF_790
add constraint pk_OBRF_790 primary key (numero_bcm);
                                        
comment on column OBRF_790.numero_bcm           is 'N�mero do Boletim de Circula��o de Mercadorias (BCM)';
comment on column OBRF_790.numero_formulario    is 'N�mero do Formul�rio do BCM';
comment on column OBRF_790.cod_empresa_orig     is 'C�digo da Empresa de Origem do BCM';
comment on column OBRF_790.cod_empresa_dest     is 'C�digo da Empresa de Destino do BCM';
comment on column OBRF_790.col_tab_preco        is 'C�digo da Tabela de Pre�os (cole��o) utilizada para a gera��o do BCM';
comment on column OBRF_790.mes_tab_preco        is 'C�digo da Tabela de Pre�os (m�s) utilizada para a gera��o do BCM';
comment on column OBRF_790.seq_tab_preco        is 'C�digo da Tabela de Pre�os (sequ�ncia) utilizada para a gera��o do BCM';
comment on column OBRF_790.ordem_producao       is 'Ordem de Produ��o utilizada para a gera��o do BCM';
comment on column OBRF_790.data_emissao         is 'Data de Emiss�o do BCM';
comment on column OBRF_790.data_saida           is 'Data de Sa�da do BCM';
comment on column OBRF_790.data_prev_ent        is 'Data de Previs�ode Entrega do BCM';
comment on column OBRF_790.horario              is 'Hor�rio do BCM';

comment on table OBRF_790                       is 'Tabela respons�vel por registrar os Boletins de Circula��o de Mercadorias (BCMs), que s�o documentos de transfer�ncia de mercadorias entre duas empresas do grupo';

exec inter_pr_recompile;
