ALTER TABLE FATU_504 ADD( 
IMPR_REG_C110 number(1) DEFAULT 0
); 

COMMENT ON COLUMN fatu_504.impr_reg_c110 IS 'Imprime bloco C110 no Sped para nota modelo 55. 0 - N�o imprime ou 1 - Imprime.';

exec inter_pr_recompile;
/