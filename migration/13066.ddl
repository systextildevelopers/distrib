alter table obrf_152
  add (numero_fci varchar2(40) default '');

comment on column obrf_152.numero_fci is
'N�mero da ficha de conte�do de importa��o (FCI).';

exec inter_pr_recompile;