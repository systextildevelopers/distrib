alter table pedi_110
add (cgc9cli       number(9),
     cgc4cli       number(4),
     cgc2cli       number(2),
     qtde_original number (13,3));
     
comment on column pedi_110.cgc9cli   is  'CNPJ cliente destinatário do item do pedido';
comment on column pedi_110.cgc4cli   is  'CNPJ cliente destinatário do item do pedido';
comment on column pedi_110.cgc2cli   is  'CNPJ cliente destinatário do item do pedido';
comment on column pedi_110.qtde_original is 'Quantidade pedido original para kit ou conjunto';

exec inter_pr_recompile;