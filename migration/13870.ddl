ALTER TABLE pcpt_022 
ADD (gram_atual_1	NUMBER(11,7) DEFAULT 0.00,
     gram_atual_2	NUMBER(11,7) DEFAULT 0.00,
     gram_atual_3	NUMBER(11,7) DEFAULT 0.00,
     larg_atual_1	NUMBER(7,3)	DEFAULT 0.00,
     larg_atual_2	NUMBER(7,3)	DEFAULT 0.00,
     larg_atual_3	NUMBER(7,3)	DEFAULT 0.00);

EXEC inter_pr_recompile;