alter table basi_750 add data_alteracao_imagem DATE;

create or replace TRIGGER "INTER_TR_BASI_750_UPDATE_IMAGE"
   after insert or update
   on basi_750
   for each row

declare
 A VARCHAR2(1);

begin

A:='TEXTO';

end inter_tr_basi_750_update_image;
/

drop trigger INTER_TR_BASI_750_UPDATE_IMAGE;
alter table basi_750 drop column data_alteracao_imagem;

exec inter_pr_recompile;