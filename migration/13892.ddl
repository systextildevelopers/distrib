insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('oper_f315', 'Tipo Produto SPED por Empresa',0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'oper_f315', 'obrf_m908' ,1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Tipo Produto SPED por Empresa'
where hdoc_036.codigo_programa = 'oper_f315'
  and hdoc_036.locale          = 'es_ES';

commit;