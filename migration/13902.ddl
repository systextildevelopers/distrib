CREATE TABLE rcnb_800_log (
  data_ocorrencia DATE NOT NULL,
  tipo_ocorrencia VARCHAR2(1) NOT NULL,
  usuario_rede VARCHAR2(20) NOT NULL,
  maquina_rede VARCHAR2(40) NOT NULL,
  aplicativo VARCHAR2(20) NOT NULL,
  empresa NUMBER(3) NOT NULL,
  usuario_systextil VARCHAR2(20) NOT NULL,
  nome_programa VARCHAR2(20) NOT NULL,
  mes_classe_OLD NUMBER(2) DEFAULT 0 NOT NULL,  
  mes_classe_NEW NUMBER(2) DEFAULT 0 NOT NULL, 
  ano_classe_OLD NUMBER(4) DEFAULT 0 NOT NULL,  
  ano_classe_NEW NUMBER(4) DEFAULT 0 NOT NULL, 
  seq_classe_OLD NUMBER(3) DEFAULT 0 NOT NULL,  
  seq_classe_NEW NUMBER(3) DEFAULT 0 NOT NULL, 
  classe_valor_OLD NUMBER(4) DEFAULT 0 NOT NULL,  
  classe_valor_NEW NUMBER(4) DEFAULT 0 NOT NULL, 
  valor_inicial_OLD NUMBER(10,2) DEFAULT 0.0,  
  valor_inicial_NEW NUMBER(10,2) DEFAULT 0.0, 
  valor_final_OLD NUMBER(10,2) DEFAULT 0.0,  
  valor_final_NEW NUMBER(10,2) DEFAULT 0.0, 
  situacao_OLD VARCHAR2(1) DEFAULT '', 
  situacao_NEW VARCHAR2(1) DEFAULT '' 
);

EXEC inter_pr_recompile;