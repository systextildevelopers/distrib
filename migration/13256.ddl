create table OBRF_782
(numero_solicitacao     number(9)       default 0        not null,
 cod_empresa            number(3)       default 0        not null,
 num_nota_fiscal        number(9)       default 0        not null,
 serie_nota_fisc        varchar2(3)     default ' '      not null,
 data_emissao           date,
 cgc9_cliente           number(9)       default 0        not null,
 cgc4_cliente           number(4)       default 0        not null,
 cgc2_cliente           number(2)       default 0        not null,
 quantidade             number(15,3)    default 0        not null,
 flag_marcado           number(1)       default 0        not null
);

alter table OBRF_782
add constraint pk_OBRF_782 primary key (numero_solicitacao, cod_empresa, num_nota_fiscal, serie_nota_fisc);
                                        
comment on column OBRF_782.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_782.cod_empresa          is 'C�digo da empresa da Nota Fiscal de Sa�da encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_782.num_nota_fiscal      is 'N�mero da Nota Fiscal de Sa�da encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_782.serie_nota_fisc      is 'S�rie da Nota Fiscal de Sa�da encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_782.data_emissao         is 'Data de Emiss�o da Nota Fiscal de Sa�da encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_782.cgc9_cliente         is 'CNPJ9 do Cliente da Nota Fiscal de Sa�da encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_782.cgc4_cliente         is 'CNPJ4 do Cliente da Nota Fiscal de Sa�da encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_782.cgc2_cliente         is 'CNPJ2 do Cliente da Nota Fiscal de Sa�da encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_782.quantidade           is 'Quantidade da Nota Fiscal de Sa�da encontrada na consulta feita pelo usu�rio para esta execu��o do processo';
comment on column OBRF_782.flag_marcado         is 'Flag para indicar se o registro (Nota Fiscal de Sa�da) foi marcado (1) ou n�o foi marcado (0) para considerar na execu��o do processo';

comment on table OBRF_782                       is 'Tabela respons�vel por gravar as Notas Fiscais de Sa�da encontradas nas consultas feitas pelo usu�rio para cada execu��o do processo.';

exec inter_pr_recompile;