update hdoc_033
  set hdoc_033.nome_menu = 'loja_TEMP'
where hdoc_033.usu_prg_cdusu = 'INTERSYS'
  and hdoc_033.nome_menu = 'loja_menu'
  and hdoc_033.item_menu = 1;
COMMIT;
update hdoc_033
  set hdoc_033.nome_menu = 'loja_menu'
where hdoc_033.usu_prg_cdusu = 'INTERSYS'
  and hdoc_033.nome_menu = 'loja_TEMP'
  and hdoc_033.programa in ('loja_f560', 'loja_f456',
                            'loja_f420', 'loja_f554',
                            'loja_f140', 'loja_f480',
                            'loja_f130', 'loja_f555',
                            'obrf_f999', 'loja_f630',
                            'loja_f460', 'loja_f470',
                            'loja_f105', 'loja_f230',
                            'loja_f999', 'loja_f650',
                            'loja_m030', 'loja_m500',
                            'loja_m040')
  and hdoc_033.item_menu = 1;
COMMIT;
update hdoc_033
  set hdoc_033.nome_menu = 'loja_T030'
where hdoc_033.usu_prg_cdusu = 'INTERSYS'
  and hdoc_033.nome_menu = 'loja_m030'
  and hdoc_033.item_menu = 1;
COMMIT;
update hdoc_033
  set hdoc_033.nome_menu = 'loja_m030'
where  hdoc_033.usu_prg_cdusu = 'INTERSYS'
  and (hdoc_033.nome_menu = 'loja_T030' or  hdoc_033.nome_menu = 'loja_TEMP')
  and  hdoc_033.programa  in ('loja_f010', 'loja_f250',
                              'loja_f251', 'loja_f570',
                              'loja_f510', 'loja_f557',
                              'loja_f253', 'loja_f558',
                              'loja_f254')
  and  hdoc_033.item_menu = 1;
COMMIT;
update hdoc_033
  set hdoc_033.nome_menu = 'loja_T500'
where hdoc_033.usu_prg_cdusu = 'INTERSYS'
  and hdoc_033.nome_menu = 'loja_m500'
  and hdoc_033.item_menu = 1;
COMMIT;
update hdoc_033
  set hdoc_033.nome_menu = 'loja_m500'
where  hdoc_033.usu_prg_cdusu = 'INTERSYS'
  and  (hdoc_033.nome_menu = 'loja_T500' or hdoc_033.nome_menu = 'loja_T030' or  hdoc_033.nome_menu = 'loja_TEMP')
  and  hdoc_033.programa  in ('loja_e290', 'loja_e070',
                              'loja_e215', 'loja_e470',
                              'crec_e385', 'loja_e280',
                              'loja_e001', 'crec_e039',
                              'loja_e205', 'loja_e210')
  and  hdoc_033.item_menu = 1;
COMMIT;

/

exec inter_pr_recompile;