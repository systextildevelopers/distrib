create table crec_220 (
  cnpj9_cliente number (9) not null,
  cnpj4_cliente number (4) not null,
  cnpj2_cliente number (2) not null,
  pedido_venda number (6) not null,
  cod_rep_cliente number (5) not null,
  seq_processo number (2) default 0,
  perc_comissao number(6,2) default 0.00,
  perc_comissao_fatu number(6,2) default 0.00,
  constraint crec_220 primary key (cnpj9_cliente, cnpj4_cliente, cnpj2_cliente, pedido_venda, cod_rep_cliente)
);

comment on table crec_220 is 'Tabela de Comiss�o dos Representantes.';
comment on column crec_220.cnpj9_cliente is 'CNPJ9 do cliente';
comment on column crec_220.cnpj4_cliente is 'CNPJ4 do cliente';
comment on column crec_220.cnpj2_cliente is 'CNPJ2 do cliente';
comment on column crec_220.pedido_venda is 'N�mero do pedido de venda';
comment on column crec_220.cod_rep_cliente is 'C�digo do representante do cliente';
comment on column crec_220.seq_processo is 'Sequencial do processo (sequ�ncia em que ser� dada a comiss�o ao representante)';
comment on column crec_220.perc_comissao is 'Percentual de comiss�o a ser aplicado no processo de c�lculo da comiss�o';
comment on column crec_220.perc_comissao_fatu is 'Percentual de comiss�o a ser aplicado no processo de faturamento';


create synonym systextilrpt.crec_220 for crec_220;

exec inter_pr_recompile;
