alter table obrf_763 add 
(divisao_prod_origem      number(4) default 0,
 divisao_prod_destino     number(4) default 0);

comment on column obrf_763.divisao_prod_origem  is 'Indica a Divis�o de Produ��o (C�lula/Fam�lia) de Origem das Pe�as (esta informa��o poder� ser utilizada na impress�o do BCM: Origem-Cod)';
comment on column obrf_763.divisao_prod_destino is 'Indica a Divis�o de Produ��o (C�lula/Fam�lia) de Destino das Pe�as (esta informa��o poder� ser utilizada na impress�o do BCM: Destino-Cod)';

exec inter_pr_recompile;
