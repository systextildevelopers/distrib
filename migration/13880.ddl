create table empr_055 (
    cod_balanca number(3) default 0 not null,
    data_afericao date not null,
    numero_afericao number(3) default 0 not null,
    peso_afericao number(10,3) default 0 not null,
    observacao varchar2(4000) default '',
    aferida number(1) not null,
    usuario number(15) default ''
);

alter table empr_055
add constraint pk_empr_055 primary key (cod_balanca, data_afericao, numero_afericao);

alter table empr_055
add constraint fk_empr_055_empr_050 foreign key (cod_balanca)
references empr_050(cod_balanca);

alter table empr_055
add constraint ck_aferida check (aferida in (0,1));

comment on table empr_055
is 'Tabela de Aferi��o de Balan�as;';
comment on column empr_055.cod_balanca
is 'Codigo da balan�a que foi aferida.';
comment on column empr_055.data_afericao
is 'Data em que a balan�a foi aferida, neste campo � gravado data e hora.';
comment on column empr_055.numero_afericao
is 'N�mero de pesagens efetuadas para aferir a balan�a.';
comment on column empr_055.peso_afericao
is 'Peso da balan�a para o n�mero aferi��o.';
comment on column empr_055.observacao
is 'Observa��o da pesagem, cada pesagem poder� ter uma observa��o diferente.';
comment on column empr_055.aferida
is 'Flag de confer�ncia da aferi��o. 0 - N�o Aferida ou 1 - Aferida. Esse campo n�o tem default e n�o aceita nulo.';
comment on column empr_055.usuario
is 'Usu�rio que efetuou a aferi��o, este campo deve ser atualizado juntamente com o campo AFERIDA.';

/

exec inter_pr_recompile;
