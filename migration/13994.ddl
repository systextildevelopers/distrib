update hdoc_035
set hdoc_035.descricao = 'Consulta de movimentação de estoques'
where hdoc_035.codigo_programa = 'estq_f090';

update hdoc_036
set hdoc_036.descricao = 'Consulta de movimentação de estoques'
where hdoc_036.codigo_programa = 'estq_f090'
and hdoc_036.locale = 'es_es';

commit work;