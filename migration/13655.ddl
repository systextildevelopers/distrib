alter table obrf_050
add (dca_op_prod_exp_017           number (15,2) default 0.00 null,
     dca_op_saida_is_018           number (15,2) default 0.00 null,
     dca_op_saida_df_019           number (15,2) default 0.00 null,
     dca_cretrans_exp_217          number (15,2) default 0.00 null,
     dca_cretrans_s_is_218         number (15,2) default 0.00 null,
     dca_cretrans_s_df_219         number (15,2) default 0.00 null,
     dca_sld_rel_exp_961           number (15,2) default 0.00 null,
     dca_sld_rel_saida_is_971      number (15,2) default 0.00 null,
     dca_sld_rel_saida_df_981      number (15,2) default 0.00 null,
     dca_ded_sld_receb_991         number (15,2) default 0.00 null,
     dca_ded_cred_receb_993        number (15,2) default 0.00 null,
     dca_ded_outras_997            number (15,2) default 0.00 null,
     dca_ded_total_999             number (15,2) default 0.00 null);
commit;