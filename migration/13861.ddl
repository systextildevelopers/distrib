declare
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pcpt_f022';

   v_usuario varchar2(15);
   
begin
   for reg_programa in programa
   loop
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'larg_atual_3';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'larg_atual_3',              0, 
            0,                           0
         );
         
         commit;
      end;
   end loop;
end;

/

exec inter_pr_recompile;