create or replace trigger inter_tr_cpag_015_custo
   before insert or
          delete or
          update of valor_pago
   on cpag_015
   for each row

declare
   v_sinal_titulo      cont_010.sinal_titulo%type;
   v_cod_hist          cont_010.codigo_historico%type;

   v_valor_pago        cpag_015.valor_pago%type;
   v_valor_pago_old    cpag_015.valor_pago%type;

   v_cnpj9 cpag_015.dupl_for_for_cli9%type;
   v_cnpj4 cpag_015.dupl_for_for_cli4%type;
   v_cnpj2 cpag_015.dupl_for_for_cli2%type;
   v_tipo_titulo       cpag_015.dupl_for_tipo_tit%type;
   v_num_titulo        cpag_015.dupl_for_nrduppag%type;
   v_parcela           cpag_015.dupl_for_no_parc%type;
   v_data_pagto        cpag_015.data_pagamento%type;

   v_valor_titulo      cpag_010.valor_parcela%type;
   v_centro_custo      cpag_010.codigo_depto%type;
   v_cod_empr          cpag_010.codigo_empresa%type;

   v_ano               number;
   v_mes               number;

   v_executa_trigger      number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementac?o executada para limpeza da base de dados do cliente
   -- e replicac?o dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      v_valor_pago     := 0.00;
      v_valor_pago_old := 0.00;

      if inserting or updating
      then
         v_cnpj9 := :new.dupl_for_for_cli9;
         v_cnpj4 := :new.dupl_for_for_cli4;
         v_cnpj2 := :new.dupl_for_for_cli2;
         v_tipo_titulo       := :new.dupl_for_tipo_tit;
         v_num_titulo        := :new.dupl_for_nrduppag;
         v_parcela           := :new.dupl_for_no_parc;
         v_cod_hist          := :new.codigo_historico;
         v_valor_pago        := :new.valor_pago;
         v_data_pagto         := :new.data_pagamento;

      else
         v_cnpj9 := :old.dupl_for_for_cli9;
         v_cnpj4 := :old.dupl_for_for_cli4;
         v_cnpj2 := :old.dupl_for_for_cli2;
         v_tipo_titulo       := :old.dupl_for_tipo_tit;
         v_num_titulo        := :old.dupl_for_nrduppag;
         v_parcela           := :old.dupl_for_no_parc;
         v_cod_hist          := :old.codigo_historico;
         v_valor_pago        := :old.valor_pago;
         v_data_pagto        := :old.data_pagamento;
      end if;
      /*Verifica sinal do titulo 1 - pagamento ou 2 - estorno*/
      select cont_010.sinal_titulo into v_sinal_titulo from cont_010
      where cont_010.codigo_historico = v_cod_hist;

      -- SINAL TITULO: 1) pagamento / 2) estorno

      if v_sinal_titulo = 2
      then
         v_valor_pago := v_valor_pago * (-1.0);
      else
         if v_sinal_titulo = 0
         then
            v_valor_pago := 0.00;
         end if;
      end if;

      /*Verifica o valor da parcela e do centro de custo*/
      begin
         select cpag_010.valor_parcela, cpag_010.codigo_depto,
                cpag_010.codigo_empresa
         into   v_valor_titulo,        v_centro_custo,
                v_cod_empr
         from cpag_010
         where cpag_010.cgc_9        = v_cnpj9
           and cpag_010.cgc_4        = v_cnpj4
           and cpag_010.cgc_2        = v_cnpj2
           and cpag_010.tipo_titulo  = v_tipo_titulo
           and cpag_010.nr_duplicata = v_num_titulo
           and cpag_010.parcela      = v_parcela;
      exception
         when no_data_found then
           v_valor_titulo := 0;
           v_centro_custo := 0;
           v_cod_empr     := 0;
      end;

      if v_centro_custo > 0 and v_valor_pago <> 0
      then

         v_mes := to_number(to_char(v_data_pagto,'MM'));
         v_ano := to_number(to_char(v_data_pagto,'YYYY'));

         begin
            insert into rcnb_080 (
               codigo_empresa, mes,
               ano,            centro_custo,
               cgc9,           cgc4,
               cgc2,           num_titulo,
               parcela,        valor_parcela,
               valor_despesa) VALUES (
               v_cod_empr,    v_mes,
               v_ano,         v_centro_custo,
               v_cnpj9,       v_cnpj4,
               v_cnpj2,       v_num_titulo,
               v_parcela,     v_valor_titulo,
               v_valor_pago);
            exception
            when others then
              begin
                 update rcnb_080
                 set   valor_despesa = rcnb_080.valor_despesa + v_valor_pago
                 where codigo_empresa = v_cod_empr
                   and mes            = v_mes
                   and ano            = v_ano
                   and centro_custo   = v_centro_custo
                   and cgc9           = v_cnpj9
                   and cgc4           = v_cnpj4
                   and cgc2           = v_cnpj2
                   and num_titulo     = v_num_titulo
                   and parcela        = v_parcela;
                exception
                when others then
                   v_centro_custo:=0;
              end;
           end;
      end if;
   end if;
end inter_tr_cpag_015_custo;
/
declare
   cursor cpag010 is
      select * from cpag_010
      where cpag_010.codigo_depto   > 0;
      
   v_data_pagamento date;
   v_valor_pago     number;
   v_ano            number;
   v_mes            number;
   
     v_registro       varchar2(100);   

begin
   dbms_output.enable(10000000);
  
   for reg_cpag010 in cpag010
   loop
      /*Elimina Registro*/ 
      begin
         delete  rcnb_080
         where codigo_empresa = reg_cpag010.codigo_empresa
           and centro_custo   = reg_cpag010.codigo_depto
           and cgc9           = reg_cpag010.cgc_9
           and cgc4           = reg_cpag010.cgc_4
           and cgc2           = reg_cpag010.cgc_2
           and num_titulo     = reg_cpag010.nr_duplicata
           and parcela        = reg_cpag010.parcela;
         exception
         when others then                  
            v_registro :=  ' N�o Eliminou ' || ' Tit ' ||
                                to_char(reg_cpag010.nr_duplicata,'000000')                 || '/' ||
                                reg_cpag010.parcela  ;
            dbms_output.put_line(v_registro);           
      end;
      
      /*Insere os novos*/
      for ii in ( 
      select cpag_015.data_pagamento,
              nvl(sum(decode(cont_010.sinal_titulo, 1,
                            (cpag_015.valor_pago),
                            2,
                            (cpag_015.valor_pago) * (-1.0),
                            0.00)),0) valor_pago
        from cpag_015, cont_010
        where cpag_015.dupl_for_for_cli9 = reg_cpag010.cgc_9
          and cpag_015.dupl_for_for_cli4 = reg_cpag010.cgc_4
          and cpag_015.dupl_for_for_cli2 = reg_cpag010.cgc_2
          and cpag_015.dupl_for_tipo_tit = reg_cpag010.tipo_titulo
          and cpag_015.dupl_for_nrduppag = reg_cpag010.nr_duplicata
          and cpag_015.dupl_for_no_parc  = reg_cpag010.parcela
          and cpag_015.codigo_historico  = cont_010.codigo_historico
        group by data_pagamento)
        loop 
          if ii.valor_pago <> 0
          then   

             v_mes := to_number(to_char(ii.data_pagamento,'MM'));
             v_ano := to_number(to_char(ii.data_pagamento,'YYYY'));

             begin
                insert into rcnb_080 (
                   codigo_empresa, mes,
                   ano,            centro_custo,
                   cgc9,           cgc4,
                   cgc2,           num_titulo,
                   parcela,        valor_parcela,
                   valor_despesa) VALUES (
                   reg_cpag010.codigo_empresa,    v_mes,
                   v_ano,                         reg_cpag010.codigo_depto,
                   reg_cpag010.cgc_9,             reg_cpag010.cgc_4,
                   reg_cpag010.cgc_2,             reg_cpag010.nr_duplicata,
                   reg_cpag010.parcela,           reg_cpag010.valor_parcela,
                   ii.valor_pago);
               exception
               when others then
               begin
                    update rcnb_080
                    set   valor_despesa = rcnb_080.valor_despesa + ii.valor_pago
                    where codigo_empresa = reg_cpag010.codigo_empresa
                      and mes            = v_mes
                      and ano            = v_ano
                      and centro_custo   = reg_cpag010.codigo_depto
                      and cgc9           = reg_cpag010.cgc_9
                      and cgc4           = reg_cpag010.cgc_4
                      and cgc2           = reg_cpag010.cgc_2
                      and num_titulo     = reg_cpag010.nr_duplicata
                      and parcela        = reg_cpag010.parcela;
                   exception
                   when others then
                  
                   v_registro :=  ' N�o encontrou Atualizou ' || ' Tit ' ||
                                to_char(reg_cpag010.nr_duplicata,'000000')                 || '/' ||
                                reg_cpag010.parcela  ;
                   dbms_output.put_line(v_registro);           
                   
              end;
           end;
           end if;   
           commit;
      end loop;
   end loop;
end;
/