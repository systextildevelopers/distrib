create or replace view vi_danfe_nf_mensagens as
select fatu_052.cod_empresa,      fatu_052.num_nota,
       fatu_052.cod_serie_nota,   -1 NATUR_OPERACAO,
       'XX' ESTADO_NATOPER,       fatu_052.cod_mensagem,
       fatu_052.ind_local,        fatu_052.seq_mensagem,
       fatu_052.des_mensag_1,     fatu_052.des_mensag_2,
       fatu_052.des_mensag_3,     fatu_052.des_mensag_4,
       fatu_052.des_mensag_5,     fatu_052.des_mensag_6,
       fatu_052.des_mensag_7,     fatu_052.des_mensag_8,
       fatu_052.des_mensag_9,     fatu_052.des_mensag_10,
       fatu_052.des_mensag_11,    fatu_052.des_mensag_12
from fatu_052
order by seq_mensagem;