insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpc_f760', 'Processo de Duplicação de Ordens de Corte', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpc_f760', 'pcpc_menu', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Proceso de Duplicación de Ordenes de Corte.'
 where hdoc_036.codigo_programa = 'pcpc_f760'
   and hdoc_036.locale          = 'es_ES';
commit;
