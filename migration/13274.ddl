create table OBRF_785
(numero_solicitacao     number(9)       default 0        not null,
 seq_passo              number(4)       default 0        not null,
 origem                 number(2)       default 0,
 geracao_de             number(2)       default 0,
 seq_origem_passo_ant   number(4)       default 0,
 empresa_destino        number(3)       default 0,
 itens_nivel_1          number(1)       default 0,
 itens_nivel_2          number(1)       default 0,
 itens_nivel_4          number(1)       default 0,
 itens_nivel_7          number(1)       default 0,
 itens_nivel_9          number(1)       default 0,
 situacao_passo         number(2)       default 0        not null
);

alter table OBRF_785
add constraint pk_OBRF_785 primary key (numero_solicitacao, seq_passo);
                                        
comment on column OBRF_785.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_785.seq_passo            is 'Sequ�ncia do passo da execu��o do processo';
comment on column OBRF_785.origem               is 'Origem do passo utilizado da execu��o do processo';
comment on column OBRF_785.geracao_de           is 'Indica o que ser� gerado por este passo da execu��o do processo';
comment on column OBRF_785.seq_origem_passo_ant is 'Indica qual a sequ�ncia do passo de origem quando � com base em um passo anterior';
comment on column OBRF_785.empresa_destino      is 'Empresa de destino para passos que s�o de troca de per�odo de OP';
comment on column OBRF_785.itens_nivel_1        is 'Indica se este passo ir� gerar �tens deste n�vel na execu��o do processo';
comment on column OBRF_785.itens_nivel_2        is 'Indica se este passo ir� gerar �tens deste n�vel na execu��o do processo';
comment on column OBRF_785.itens_nivel_4        is 'Indica se este passo ir� gerar �tens deste n�vel na execu��o do processo';
comment on column OBRF_785.itens_nivel_7        is 'Indica se este passo ir� gerar �tens deste n�vel na execu��o do processo';
comment on column OBRF_785.itens_nivel_9        is 'Indica se este passo ir� gerar �tens deste n�vel na execu��o do processo';
comment on column OBRF_785.situacao_passo       is 'Indica a situa��o do passo (1-Em aberto ou 2-Finalizado) na execu��o do processo';

comment on table OBRF_785                       is 'Tabela respons�vel por gravar os passos de cada execu��o do processo.';

exec inter_pr_recompile;
