alter table obrf_761 add 
(obs_ref_documento             number(3) default 0);

comment on column obrf_761.obs_ref_documento is '';

alter table obrf_761 add 
(desc_obs_ref_documento             varchar2(55) default '');

comment on column obrf_761.desc_obs_ref_documento is '';

exec inter_pr_recompile;