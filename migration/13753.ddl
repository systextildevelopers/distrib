alter table fatu_030
add (cgc9cli     number(9),
     cgc4cli     number(4),
     cgc2cli     number(2));
     
comment on column fatu_030.cgc9cli   is  'CNPJ cliente destinatário do item do pedido';
comment on column fatu_030.cgc4cli   is  'CNPJ cliente destinatário do item do pedido';
comment on column fatu_030.cgc2cli   is  'CNPJ cliente destinatário do item do pedido';

exec inter_pr_recompile;