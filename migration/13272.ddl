create table OBRF_762
(codigo_passo        number(5)       default 0        not null,
 cod_empresa_ent     number(3)       default 0        not null,
 nat_oper            number(3)       default 0        not null,
 est_oper            varchar2(2)     default ' '      not null,
 trazer_lote_saida   varchar2(2)     default ' '      not null,
 cod_centro_custo    number(6)       default 0        not null,
 cond_pagto          number(3)       default 0        not null,
 deposito_entrada    number(3)       default 0        not null,
 cod_portador        number(3)       default 0        not null
);
 
alter table OBRF_762
add constraint pk_OBRF_762 primary key (codigo_passo);

comment on column OBRF_762.codigo_passo        is 'C�digo do passo que estes par�metros para nota fiscal de entrada est�o relacionados';
comment on column OBRF_762.cod_empresa_ent     is 'C�digo da empresa de entrada da nota fiscal que ser� gerada por este passo';
comment on column OBRF_762.nat_oper            is 'Natureza de Opera��o da nota fiscal de entrada que ser� gerada por este passo ';
comment on column OBRF_762.est_oper            is 'Estado da Natureza de Opera��o da nota fiscal de entrada que ser� gerada por este passo ';
comment on column OBRF_762.trazer_lote_saida   is 'Trazer Lote de Sa�da da nota fiscal de entrada que ser� gerada por este passo ';
comment on column OBRF_762.cod_centro_custo    is 'C�digo do Centro de Custos da nota fiscal de entrada que ser� gerada por este passo ';
comment on column OBRF_762.cond_pagto          is 'Condi��o de pagamento da nota fiscal de entrada que ser� gerada por este passo ';
comment on column OBRF_762.deposito_entrada    is 'Dep�sito de entrada da nota fiscal de entrada que ser� gerada por este passo ';
comment on column OBRF_762.cod_portador        is 'C�digo do portador da nota fiscal de entrada que ser� gerada por este passo ';

comment on table OBRF_762                      is 'Tabela respons�vel por gravar os par�metros para gera��o de nota fiscal de entrada de um passo.';

exec inter_pr_recompile;