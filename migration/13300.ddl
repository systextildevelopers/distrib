insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f790', 'Consulta de Boletins de Circulação de Mercadorias (BCM)', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f790', 'menu_ad101', 1, 1, 'N', 'N', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Consulta de Boletins de Circulação de Mercadorias (BCM)'
 where hdoc_036.codigo_programa = 'obrf_f790'
   and hdoc_036.locale          = 'es_ES';
commit;
