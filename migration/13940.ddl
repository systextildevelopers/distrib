alter table exec_010 add (
qtde_prod_maquina number(15,3) default 0.0,
producao_relogio number(15,3) default 0.0 );

alter table mqop_030 modify (
passo_relogio number(15,6) default 0.0 );
/

exec inter_pr_recompile;