create or replace view vi_danfe_nf_mensagens as
select fatu_052.cod_empresa,
       fatu_052.num_nota,
       fatu_052.cod_serie_nota,
       -1 NATUR_OPERACAO,
       'XX' ESTADO_NATOPER,
       fatu_052.cod_mensagem,
       fatu_052.ind_local,
       fatu_052.seq_mensagem,
       fatu_052.des_mensag_1,
       fatu_052.des_mensag_2,
       fatu_052.des_mensag_3,
       fatu_052.des_mensag_4,
       fatu_052.des_mensag_5,
       fatu_052.des_mensag_6,
       fatu_052.des_mensag_7,
       fatu_052.des_mensag_8,
       fatu_052.des_mensag_9,
       fatu_052.des_mensag_10
from fatu_052
where fatu_052.ind_local = 'D'
union all
select -1    COD_EMPRESA,            -1 NUM_NOTA,
       'XXX' COD_SERIE_NOTA,         pedi_080.natur_operacao,
       pedi_080.estado_natoper,      obrf_874.cod_mensagem,
       obrf_874.ind_local,           0 SEQ_MENSAGEM,
       obrf_874.des_mensagem1,       obrf_874.des_mensagem2,
       obrf_874.des_mensagem3,       obrf_874.des_mensagem4,
       obrf_874.des_mensagem5,       obrf_874.des_mensagem6,
       obrf_874.des_mensagem7,       obrf_874.des_mensagem8,
       obrf_874.des_mensagem9,       obrf_874.des_mensagem10
from obrf_874, pedi_080
where obrf_874.cod_mensagem = pedi_080.cod_mensagem
  and obrf_874.ind_local = 'D'
order by seq_mensagem;
/