create table OBRF_788
(numero_solicitacao     number(9)       default 0        not null,
 seq_passo              number(4)       default 0        not null,
 cod_empresa_ent        number(3)       default 0        not null,
 numero_nota_ent        number(9)       default 0        not null,
 serie_nota_ent         varchar2(3)     default ' '      not null,
 cgc9_fornecedor        number(9)       default 0        not null,
 cgc4_fornecedor        number(4)       default 0        not null,
 cgc2_fornecedor        number(2)       default 0        not null,
 data_emissao           date,
 seq_item_nota          number(5)       default 0        not null,
 nivel                  varchar2(1)     default ' '      not null,
 grupo                  varchar2(5)     default ' '      not null,
 subgrupo               varchar2(3)     default ' '      not null,
 item                   varchar2(6)     default ' '      not null,
 lote_acomp             number(6)       default 0,
 cod_deposito           number(3)       default 0        not null,
 valor_unitario         number(15,5)    default 0.00000  not null,
 quantidade             number(15,3)    default 0.000    not null,
 flag_marcado           number(1)       default 0        not null
);

alter table OBRF_788
add constraint pk_OBRF_788 primary key (numero_solicitacao, seq_passo, cod_empresa_ent, numero_nota_ent, serie_nota_ent, cgc9_fornecedor, cgc4_fornecedor, cgc2_fornecedor, seq_item_nota);
                                        
comment on column OBRF_788.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_788.seq_passo            is 'Sequ�ncia do passo da execu��o do processo';

comment on column OBRF_788.cod_empresa_ent      is 'C�digo da empresa da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.numero_nota_ent      is 'N�mero da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.serie_nota_ent       is 'S�rie da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.cgc9_fornecedor      is 'CNPJ do Fornecedor da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.cgc4_fornecedor      is 'CNPJ do Fornecedor da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.cgc2_fornecedor      is 'CNPJ do Fornecedor da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.data_emissao         is 'Data de emiss�o da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.seq_item_nota        is 'Sequencial do item da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.nivel                is 'N�vel do produto da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.grupo                is 'Grupo do produto da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.subgrupo             is 'Subgrupo do produto da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.item                 is 'Item do produto da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.lote_acomp           is 'Lote do produto da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.cod_deposito         is 'Dep�sito do item da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';

comment on column OBRF_788.valor_unitario       is 'Valor unit�rio do item da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.quantidade           is 'Quantidade do item da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';
comment on column OBRF_788.flag_marcado         is 'Flag para indicar se o item foi considerado (1) ou n�o (0) na gera��o da nota fiscal de entrada a ser gerada na execu��o do processo neste passo';

comment on table OBRF_788                       is 'Tabela respons�vel por gravar os �tens da nota fiscal de entrada a ser gerada na execu��o do processo.';

exec inter_pr_recompile;
