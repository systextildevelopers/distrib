alter table obrf_762 add 
(empenhar             number(1) default 0,
 gera_parcelas_proib  number(1) default 0);

comment on column obrf_762.empenhar is 'Indica se dever� empenhar os pedidos para a ordem de produ��o (0: N�o empenha, 1: Empenha)';
comment on column obrf_762.gera_parcelas_proib is 'Indica se dever� gerar as parcelas dos t�tulos nas datas proibitivas mesmo que supere o limite m�ximo configurado';

exec inter_pr_recompile;
