create table obrf_801
 (cod_enq_ipi number(3) default 0,
    desc_enq    varchar2(1000) default ' ');
    
alter table obrf_801
add constraint pk_obrf_801 primary key (cod_enq_ipi);

exec inter_pr_recompile;