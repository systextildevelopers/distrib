alter table inte_890
add (versao_client varchar(10) default '0.00');

comment on column inte_890.versao_client
is 'Versao do client utilizada pelo representante';

exec inter_pr_recompile;