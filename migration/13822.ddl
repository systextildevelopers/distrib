alter table tmrp_330
add (qtde_programada numeric(6) default 0);    

comment on column tmrp_330.qtde_programada
is 'Quantidade programada';

exec inter_pr_recompile;