alter table HDOC_070
  drop constraint PK_HDOC_070;

prompt ATENCAO! Ao excluir este index se aparecer a mensagem "ORA-01418: specified index does not exist" favor ignorar.
drop index PK_HDOC_070;

alter table hdoc_070
add (requisitante varchar(20) default ' ');

comment on column HDOC_070.REQUISITANTE is 'Requisitante';

alter table HDOC_070
  add constraint PK_HDOC_070 primary key (CODIGO_PROCESSO, NOME_LIBERADOR, CENTRO_CUSTO, CONTA_ESTOQUE, COD_COMPRADOR, TIPO_TITULO, REQUISITANTE);

exec inter_pr_recompile;