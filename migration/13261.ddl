create table OBRF_760
(codigo_passo        number(5)       default 0    not null,
 descricao           varchar2(120)   default ' '  not null,  
 origem              number(2)       default 0    not null,
 geracao_de          number(2)       default 0    not null,
 itens_nivel_1       number(1)       default 0,
 itens_nivel_2       number(1)       default 0,
 itens_nivel_4       number(1)       default 0,
 itens_nivel_7       number(1)       default 0,
 itens_nivel_9       number(1)       default 0,
 empresa_destino     number(3)       default 0);
 
alter table OBRF_760
add constraint pk_OBRF_760 primary key (codigo_passo);
                                        
comment on column OBRF_760.codigo_passo  is 'C�digo do passo que ser� utilizado no processo';
comment on column OBRF_760.descricao     is 'Descri��o do passo para identifica��o do que o passo dever� fazer';
comment on column OBRF_760.origem        is 'Identifica a origem do passo, isto �, no que ser� baseado para fazer a gera��o (Origem 1: ORDEM DE CORTE ou 2: NOTA FISCAL DE SA�DA ou 3: NOTA FISCAL DE ENTRADA ou 4: COM BASE EM UM PASSO ANTERIOR ou 5: BOLETIM DE CIRCULA��O DE MERCADORIAS)';
comment on column OBRF_760.geracao_de    is 'Identifica o que ser� gerado neste passo (Gera��o de 1: NOTA FISCAL DE SA�DA ou 2: NOTA FISCAL DE ENTRADA/CONFIRMA��O ou 3: BOLETIM DE CIRCULA��O DE MERCADORIAS ou 4: TROCA DE PER�ODO DE OP)';
comment on column OBRF_760.itens_nivel_1 is 'Identifica se ser� considerado nos �tens do movimento que ser� gerado por este passo produtos com este n�vel';
comment on column OBRF_760.itens_nivel_2 is 'Identifica se ser� considerado nos �tens do movimento que ser� gerado por este passo produtos com este n�vel';
comment on column OBRF_760.itens_nivel_4 is 'Identifica se ser� considerado nos �tens do movimento que ser� gerado por este passo produtos com este n�vel';
comment on column OBRF_760.itens_nivel_7 is 'Identifica se ser� considerado nos �tens do movimento que ser� gerado por este passo produtos com este n�vel';
comment on column OBRF_760.itens_nivel_9 is 'Identifica se ser� considerado nos �tens do movimento que ser� gerado por este passo produtos com este n�vel';
comment on column OBRF_760.empresa_destino is 'Identifica a empresa de destino para troca de per�odo de OP, quando a ORIGEM DE for TROCA DE PER�ODO DE OP';

comment on table OBRF_760                is 'Tabela respons�vel por gravar os passos que ser�o utilizados no processo, sendo que cada passo ir� gerar um movimento.';

exec inter_pr_recompile;