create table pedi_126(
pedido_venda       number(6) default 0,
seq_distr          number(6) default 0,
seq_estr           number(3) default 0,
niv_estr           varchar2(1) default ' ',
gru_estr           varchar2(5) default ' ',
sub_estr           varchar2(3) default ' ',
ite_estr           varchar2(6) default ' ',
qtde_estr          number(13,3) default 0.000);


comment on column pedi_126.pedido_venda is 'numero do pedido que estiver sendo distribu�do';
comment on column pedi_126.seq_distr is 'sequencia da distribui��o';
comment on column pedi_126.seq_estr is 'sequencia da estrutura do produto';
comment on column pedi_126.niv_estr is 'n�vel da estrutura do produto';
comment on column pedi_126.gru_estr is 'grupo da estrutura do produto';
comment on column pedi_126.sub_estr is 'subgrupo da estrutura do produto';
comment on column pedi_126.ite_estr is 'item da estrutura do produto';
comment on column pedi_126.qtde_estr is 'quantidade da estrutura do produto';