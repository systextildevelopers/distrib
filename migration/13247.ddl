create table OBRF_771 (
codigo_processo      number(4) default 0 not null, 
seq_passo            number(4) default 0 not null, 
codigo_passo         number(5) default 0 not null, 
seq_origem_passo_ant number(4) default 0 );

alter table OBRF_771 add constraint pk_OBRF_771 primary key (codigo_processo, seq_passo);

comment on column OBRF_771.codigo_processo is 
'C�digo do processo que possuir� os passos para execu��o posterior'; 

comment on column OBRF_771.seq_passo is 
'Sequ�ncia/ordem do passo dentro do processo'; 

comment on column OBRF_771.codigo_passo is 
'C�digo do passo relacionado, passo que ser� executado no processo';

comment on column OBRF_771.seq_origem_passo_ant is 
'Identifica a sequ�ncia do passo de origem, quando este � um passo que se baseia em um passo anterior';

comment on table OBRF_771 is 
'Tabela respons�vel por gravar os passos que ser�o executados em cada processo';

exec inter_pr_recompile;