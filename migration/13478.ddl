ALTER TABLE loja_200
ADD (PESO_BRUTO    number(15,4) default 0,
     PESO_LIQUIDO  number(15,4) default 0);

exec inter_pr_recompile;
/