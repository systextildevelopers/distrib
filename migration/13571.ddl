drop table fatu_509;

drop synonym systextilrpt.fatu_509;

create table fatu_509
(processo varchar2(50)     default ''    null, 
 data_inicio date                        null, 
 usuario_sys varchar2(50)  default ''    null, 
 empresa_sys numeric(9)    default 0     null, 
 usuario_rede varchar2(50) default ''    null, 
 maquina_rede varchar2(50) default ''    null, 
 programa varchar2(50)   default ''    null, 
 sid_oracle numeric(9)     default 0     null);


comment on table fatu_509 is 
'Tabela de controle utilizada no calculo de faturamento.\nTabela vazia indica que não está sendo realizado nenhum calculo no momento.\nRegistro na tabela, mostra informações do usuário que está processando.';

comment on column fatu_509.processo
is 'Informacoes referente ao processo. Ex. FatuF194.';

comment on column fatu_509.data_inicio
is 'Data que o processo foi iniciado.';

comment on column fatu_509.usuario_sys
is 'Usuario do Systêxtil que iniciou o processo.';

comment on column fatu_509.usuario_rede
is 'Usuario de rede de quem iniciou o processo.';

comment on column fatu_509.maquina_rede
is 'Maquina de rede do usuário que iniciou o processo.';

comment on column fatu_509.programa
is 'Programa que iniciou o processamento.';

comment on column fatu_509.sid_oracle
is 'SID da sessão do oracle que iniciou o processo';

create synonym systextilrpt.fatu_509 for fatu_509;

exec inter_pr_recompile;