alter table inte_890
add (url_conexao varchar(100) default '');

comment on column inte_890.url_conexao
is 'URL utilizada para sincronizacao pelo representante';

exec inter_pr_recompile;