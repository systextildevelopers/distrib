insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_m150', 'Menu de Consigna��o', 1,0);

commit;

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_m150', 'obrf_menu', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Men� de Consignaci�n'
 where hdoc_036.codigo_programa = 'obrf_m150'
   and hdoc_036.locale          = 'es_ES';

commit;

exec inter_pr_recompile;