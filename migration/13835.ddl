create table SPED_CTB_0035
(
  cod_empresa NUMBER(3) default 0 not null,
  cod_scp     VARCHAR2(14) default ' ' not null,
  desc_scp    VARCHAR2(100) default ' ',
  inf_comp    VARCHAR2(254) default ' '
);

comment on column SPED_PC_0035.cod_empresa  is 'C�digo da empresa';
comment on column SPED_PC_0035.cod_scp  is 'C�digo de Sociedades em Conta de Participa��o';
comment on column SPED_PC_0035.desc_scp  is 'Descri��o de Sociedades em Conta de Participa��o';
comment on column SPED_PC_0035.inf_comp  is 'Informa��o complementar';

exec inter_pr_recompile;