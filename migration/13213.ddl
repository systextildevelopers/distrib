begin
declare
  cursor programa is select * from hdoc_033
  where hdoc_033.programa = 'tmrp_f070';

  v_usuario varchar2(15);
  begin
   for reg_programa in programa
   loop
     begin
        select oper_550.usuario into v_usuario 
        from oper_550 
        where oper_550.usuario       = reg_programa.usu_prg_cdusu
          and oper_550.empresa       = reg_programa.usu_prg_empr_usu
          and oper_550.nome_programa = reg_programa.programa
          and oper_550.nome_field    = 'btn_replicar_periodo';
         exception
         when no_data_found 
         then
            insert into oper_550
              (usuario, empresa, nome_programa, nome_subprograma, nome_field, requerido, acessivel)
            values(
               reg_programa.usu_prg_cdusu, reg_programa.usu_prg_empr_usu, reg_programa.programa, ' ', 'btn_replicar_periodo', 0, 0);
            commit;
      end;
   end loop;
end;

end;
/