insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpc_e012', 'Relat�rio de Hist�rico de Ordens de Produ��o', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpc_e012', 'pcpc_m500', 1, 20, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Reporte de Hist�rico de �rdenes de Producci�n'
 where hdoc_036.codigo_programa = 'pcpc_e012'
   and hdoc_036.locale          = 'es_ES';
commit;
