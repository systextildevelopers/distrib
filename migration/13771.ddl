create or replace view producao_por_estagio_conf
(ordem_producao, cod_estagio, codigo_empresa, seq_operacao, qtde_prog2, qtde_prog, qtde_a_produzir_pacote, qtde_prod, qtde_2a, qtde_cons, qtde_perd, qtde_pecas_prog, perc_prod)
as
select ops_selec.ordem_producao,
       ops_selec.codigo_estagio,
       ops_selec.codigo_empresa,
       ops_selec.seq_operacao,
       ops_selec.qtde_prog,
       ops_selec.em_prod,
       ops_selec.qtde_a_produzir_pacote,
       ops_selec.qtde_pecas_prod,
       ops_selec.qtde_pecas_2a,
       ops_selec.qtde_conserto,
       ops_selec.qtde_perdas,
       ops_selec.qtde_pecas_prog_op,
       ops_selec.perc_prod
from (
select pcpc_040.ordem_producao                            as ordem_producao,
       pcpc_040.codigo_estagio                            as codigo_estagio,
       nvl(max(pcpc_040.codigo_empresa),0)                as codigo_empresa,
       nvl(max(pcpc_040.seq_operacao),0)                  as seq_operacao,
       nvl(sum(pcpc_040.qtde_pecas_prog),0)               as qtde_prog,
       nvl(sum(pcpc_040.qtde_pecas_prog) -
           sum(pcpc_040.qtde_pecas_prod) -
           sum(pcpc_040.qtde_pecas_2a) -
           sum(pcpc_040.qtde_perdas),0)                   as em_prod,
       nvl(sum(pcpc_040.qtde_a_produzir_pacote),0)        as qtde_a_produzir_pacote,
       nvl(sum(pcpc_040.qtde_pecas_prod),0)               as qtde_pecas_prod,
       nvl(sum(pcpc_040.qtde_pecas_2a),0)                 as qtde_pecas_2a,
       nvl(sum(pcpc_040.qtde_conserto),0)                 as qtde_conserto,
       nvl(sum(pcpc_040.qtde_perdas),0)                   as qtde_perdas,
       nvl(sum(pcpc_040.qtde_pecas_prog),0)               as qtde_pecas_prog_op,
       nvl(((nvl(sum(pcpc_040.qtde_pecas_prog),0) -
             nvl(sum(pcpc_040.qtde_a_produzir_pacote),0))
       / decode(sum(pcpc_040.qtde_pecas_prog),0,1,
                sum(pcpc_040.qtde_pecas_prog))*100),0)    as perc_prod
from pcpc_040
where pcpc_040.codigo_estagio <> 0
group by pcpc_040.ordem_producao,
         pcpc_040.codigo_estagio) ops_selec
order by ops_selec.seq_operacao;

exec inter_pr_recompile;