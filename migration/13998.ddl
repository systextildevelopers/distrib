create table pcpf_017
(lote_fio    number(6) default 0,
 nivel       varchar(1) default '',
 grupo       varchar(5) default '',
 subgrupo    varchar(3) default '',
 item        varchar(6) default '',
 cor         varchar(4) default '',
 letra       varchar(5) default '',
 comprimento number(6,2) default 0.00,
 livre       varchar(1) default '',
 qtde_fardos number(4) default 0);

alter table pcpf_017
add constraint pk_pcpf_017 primary key(lote_fio);
/
exec inter_pr_recompile;