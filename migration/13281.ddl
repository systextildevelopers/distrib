create table OBRF_832
(numero_solicitacao     number(9)       default 0        not null,
 seq_passo              number(4)       default 0        not null,
 cod_empresa_ent        number(3)       default 0        not null,
 numero_nota_ent        number(9)       default 0        not null,
 serie_nota_ent         varchar2(3)     default ' '      not null,
 cgc9_fornecedor        number(9)       default 0        not null,
 cgc4_fornecedor        number(4)       default 0        not null,
 cgc2_fornecedor        number(2)       default 0        not null,
 data_emissao           date,
 quantidade_total       number(15,3)    default 0.000    not null,
 valor_total            number(15,2)    default 0.00     not null
);

alter table OBRF_832
add constraint pk_OBRF_832 primary key (numero_solicitacao, seq_passo, numero_nota_ent, serie_nota_ent, cgc9_fornecedor, cgc4_fornecedor, cgc2_fornecedor);
                                        
comment on column OBRF_832.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_832.seq_passo            is 'Sequ�ncia do passo da execu��o do processo';

comment on column OBRF_832.cod_empresa_ent      is 'C�digo da Empresa da Nota Fiscal de Entrada que foi gerada por este passo na execu��o do processo';
comment on column OBRF_832.numero_nota_ent      is 'N�mero da Nota Fiscal de Entrada que foi gerada por este passo na execu��o do processo';
comment on column OBRF_832.serie_nota_ent       is 'S�rie da Nota Fiscal de Entrada que foi gerada por este passo na execu��o do processo';
comment on column OBRF_832.cgc9_fornecedor      is 'CNPJ9 do Fornecedor da Nota Fiscal de Entrada que foi gerada por este passo na execu��o do processo';
comment on column OBRF_832.cgc4_fornecedor      is 'CNPJ9 do Fornecedor da Nota Fiscal de Entrada que foi gerada por este passo na execu��o do processo';
comment on column OBRF_832.cgc2_fornecedor      is 'CNPJ9 do Fornecedor da Nota Fiscal de Entrada que foi gerada por este passo na execu��o do processo';
comment on column OBRF_832.data_emissao         is 'Data de emiss�o da Nota Fiscal de Entrada que foi gerada por este passo na execu��o do processo';
comment on column OBRF_832.quantidade_total     is 'Quantidade total da Nota Fiscal de Entrada que foi gerada por este passo na execu��o do processo';
comment on column OBRF_832.valor_total          is 'Valor total da Nota Fiscal de Entrada que foi gerada por este passo na execu��o do processo';

comment on table OBRF_832                       is 'Tabela respons�vel por gravar as Notas Fiscais de Entrada que foram geradas por cada passo na execu��o do processo.';

exec inter_pr_recompile;
