insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpc_f603', 'Copia de Valor por Minuto por C�lula', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpc_f603', 'pcpc_menu', 1, 20, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Valor del Copia por minuto para Celular'
 where hdoc_036.codigo_programa = 'pcpc_f603'
   and hdoc_036.locale          = 'es_ES';

commit;

exec inter_pr_recompile;