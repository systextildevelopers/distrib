alter table cpag_200
add (valor_liquidacao numeric(13,2) default 0.00,
     data_liquidacao  date,
     banco_liquidacao numeric(3) default 0,
     conta_liquidacao numeric(9) default 0);

comment on column cpag_200.valor_liquidacao
  is 'Valor da liquidação do adiantamento.';
  
comment on column cpag_200.data_liquidacao
  is 'Data da liquidação do adiantamento.';
  
comment on column cpag_200.banco_liquidacao
  is 'Banco da liquidação do adiantamento.';
  
comment on column cpag_200.conta_liquidacao
  is 'Conta corrente da liquidação do adiantamento.';      
     
exec inter_pr_recompile;
/