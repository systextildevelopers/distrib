alter table basi_290 add OBS_ARTIGO varchar(120) default ' ';

comment on column basi_290.OBS_ARTIGO is
'Campo para se informar a observação do artigo do produto';

exec inter_pr_recompile;