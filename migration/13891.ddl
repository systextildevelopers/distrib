create table oper_315(
   cod_empresa number(3) NOT NULL,
   tipo_produto_sped number(2) default 0,
   conta_estoque number(2) NOT NULL,
   nivel varchar2(1) NOT NULL,
   grupo varchar2(5) NOT NULL,
   subgrupo varchar2(3) NOT NULL,
   item varchar2(6) NOT NULL
   );

alter table oper_315 add CONSTRAINT cod_empresa_fk PRIMARY KEY (cod_empresa, conta_estoque, nivel, grupo, subgrupo, item);

comment on table oper_315 is 'TIPO PRODUTO SPED POR EMPRESA.';