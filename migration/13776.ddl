-- Add/modify columns
alter table oper_325 add data_registro date;
alter table oper_325 modify data_registro default sysdate;

-- Add comments to the columns
comment on column oper_325.data_registro
  is 'Data da Inser��o / Ultima Alteracao no Registro';

/
exec inter_pr_recompile;