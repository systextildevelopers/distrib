CREATE TABLE OBRF_792 (
   NUMERO_SOLICITACAO  NUMBER(9)          DEFAULT 0 NOT NULL,
   ORDEM_SERVICO       NUMBER(6)          DEFAULT 0 NOT NULL,
   DATA_SERVICO        DATE, 
   CNPJ9               NUMBER(9)          DEFAULT 0 NOT NULL,
   CNPJ4               NUMBER(4)          DEFAULT 0 NOT NULL,
   CNPJ2               NUMBER(2)          DEFAULT 0 NOT NULL,
   QUANTIDADE          NUMBER(13,6)       DEFAULT 0.0 NOT NULL,
   VALOR               NUMBER(13,2)       DEFAULT 0.0 NOT NULL,
   FLAG_MARCADO        NUMBER(1)          DEFAULT 0 
);

comment on column OBRF_792.NUMERO_SOLICITACAO
  is 'N�mero de solicita��o da execu��o de processos - ordem de servi�o.';
comment on column OBRF_792.ORDEM_SERVICO
  is 'Ordem de servi�o da execu��o de processos - ordem de servi�o.';
comment on column OBRF_792.DATA_SERVICO
  is 'Data de servi�o da execu��o de processos - ordem de servi�o.';
comment on column OBRF_792.CNPJ9
  is 'CNPJ da execu��o de processos - ordem de servi�o.';
comment on column OBRF_792.CNPJ4
  is 'CNPJ da execu��o de processos - ordem de servi�o.';
comment on column OBRF_792.CNPJ2
  is 'CNPJ da execu��o de processos - ordem de servi�o.';
comment on column OBRF_792.QUANTIDADE
  is 'Quantidade de item da execu��o de processos - ordem de servi�o.';
comment on column OBRF_792.VALOR
  is 'Valor do item da execu��o de processos - ordem de servi�o.';
comment on column OBRF_792.FLAG_MARCADO
  is 'Flag da tela de execu��o de processos - ordem de servi�o.';

ALTER TABLE OBRF_792
   ADD CONSTRAINT PK_OBRF_792
   PRIMARY KEY (NUMERO_SOLICITACAO, ORDEM_SERVICO);

CREATE TABLE OBRF_793 (
   NUMERO_SOLICITACAO  NUMBER(9)          DEFAULT 0 NOT NULL,
   SEQ_PASSO           NUMBER(4)          DEFAULT 0 NOT NULL,
   ORDEM_SERVICO       NUMBER(6)          DEFAULT 0 NOT NULL,
   SEQUENCIA           NUMBER(3)          DEFAULT 0 NOT NULL,
   NIVEL               VARCHAR2(1)        DEFAULT '' NOT NULL,
   GRUPO               VARCHAR2(5)        DEFAULT '' NOT NULL,
   SUBGRUPO            VARCHAR2(3)        DEFAULT '' NOT NULL,
   ITEM                VARCHAR2(6)        DEFAULT '' NOT NULL,
   DEPOSITO            NUMBER(3)          DEFAULT 0 NOT NULL,
   QUANTIDADE          NUMBER(13,6)       DEFAULT 0.0 NOT NULL,
   VALOR               NUMBER(13,2)       DEFAULT 0.0 NOT NULL,
   FLAG_MARCADO        NUMBER(1)          DEFAULT 0
);

comment on column OBRF_793.NUMERO_SOLICITACAO
  is 'N�mero de solicita��o da execu��o de processos - ordem de servi�o.';
comment on column OBRF_793.SEQ_PASSO
  is 'Sequ�ncia de passos da execu��o de processos - ordem de servi�o.';
comment on column OBRF_793.ORDEM_SERVICO
  is 'Ordem de servi�o da execu��o de processos - ordem de servi�o.';
comment on column OBRF_793.SEQUENCIA
  is 'Sequ�ncia da execu��o de processos - ordem de servi�o.';
comment on column OBRF_793.NIVEL
  is 'Nivel do produto da execu��o de processos - ordem de servi�o.';
comment on column OBRF_793.GRUPO
  is 'Grupo do produto da execu��o de processos - ordem de servi�o.';
comment on column OBRF_793.SUBGRUPO
  is 'Subgrupo do produto da execu��o de processos - ordem de servi�o.';
comment on column OBRF_793.ITEM
  is 'Item do produto da execu��o de processos - ordem de servi�o.';
comment on column OBRF_793.DEPOSITO
  is 'Dep�sito da execu��o de processos - ordem de servi�o.';
comment on column OBRF_793.QUANTIDADE
  is 'Quantidade de item da execu��o de processos - ordem de servi�o.';
comment on column OBRF_793.VALOR
  is 'Valor do item da execu��o de processos - ordem de servi�o.';
comment on column OBRF_793.FLAG_MARCADO
  is 'Flag da tela de execu��o de processos - ordem de servi�o.';

ALTER TABLE OBRF_793
   ADD CONSTRAINT PK_OBRF_793
   PRIMARY KEY (NUMERO_SOLICITACAO, ORDEM_SERVICO);
   
exec inter_pr_recompile;