update HDOC_001
set HDOC_001.DESCRICAO21 = 'UN'
where HDOC_001.TIPO              = 85
  and trim(HDOC_001.DESCRICAO21) is null
  and HDOC_001.CAMPO_NUMERICO02  in (4,5,6);
commit work;