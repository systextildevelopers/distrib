insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('cpag_f940', 'Liquidação de Adiantamento',0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'cpag_f940', 'NENHUM',0 , 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Liquidación de adelanto'
where hdoc_036.codigo_programa = 'cpag_f940'
  and hdoc_036.locale          = 'es_ES';

commit;

/

exec inter_pr_recompile;