alter table empr_002 add
(
    preromane_lote_dif_pedido varchar2(1) default 'N'
);

comment on column empr_002.preromane_lote_dif_pedido is 'Indica se no pre-romaneio � permitido alocar produtos do estoque cujo lote seja diferente do lote do pedido';

exec inter_pr_recompile;
