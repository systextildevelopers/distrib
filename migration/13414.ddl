alter table obrf_010
  add (quarentena          number(1),
       data_lim_quarentena date);
  
comment on column obrf_010.quarentena is
'0- n�o est� na quarentena 1- est� na quarentena';

comment on column obrf_010.data_lim_quarentena is
'Data limite da quarentena.';

alter table obrf_010
   modify (quarentena    default 0,
           data_lim_quarentena);

prompt "ATEN��O! O comando abaixo dever� demorar para executar, e n�o deve ser abortado.";

begin
declare
   v_contador number(3) := 0;
begin
   for reg_obrf_010 in (select rowid from obrf_010 where obrf_010.quarentena is null and obrf_010.data_lim_quarentena is null)
   loop
      update obrf_010
      set    obrf_010.quarentena    = 0
      where  obrf_010.rowid = reg_obrf_010.rowid;
 
      v_contador :=  v_contador + 1;
      
         if v_contador > 100 then
            commit;
            v_contador := 0;
         end if;   
   end loop;   
   commit;
end;
end;

/

exec inter_pr_recompile;