create table OBRF_833
(numero_solicitacao     number(9)       default 0        not null,
 seq_passo              number(4)       default 0        not null,
 numero_bcm             number(9)       default 0        not null,
 numero_formulario      number(9)       default 0        not null,
 ordem_producao         number(9)       default 0        not null,
 cod_empresa_orig       number(3)       default 0        not null,
 cod_empresa_dest       number(3)       default 0        not null,
 data_emissao           date,
 quantidade_total       number(15,3)    default 0.000    not null,
 valor_total            number(15,2)    default 0.00     not null
);

alter table OBRF_833
add constraint pk_OBRF_833 primary key (numero_solicitacao, seq_passo, numero_bcm);
                                        
comment on column OBRF_833.numero_solicitacao   is 'N�mero de solicita��o da execu��o do processo';
comment on column OBRF_833.seq_passo            is 'Sequ�ncia do passo da execu��o do processo';

comment on column OBRF_833.numero_bcm           is 'N�mero do BCM que foi gerado neste passo para esta execu��o do processo';
comment on column OBRF_833.numero_formulario    is 'N�mero do Formul�rio (impress�o tipogr�fica) do BCM que foi gerado neste passo para esta execu��o do processo';
comment on column OBRF_833.ordem_producao       is 'Ordem de Produ��o do BCM que foi gerado neste passo para esta execu��o do processo';
comment on column OBRF_833.data_emissao         is 'Data de Emiss�o do BCM que foi gerado neste passo para esta execu��o do processo';
comment on column OBRF_833.cod_empresa_orig     is 'C�digo da Empresa de Origem do BCM que foi gerado neste passo para esta execu��o do processo';
comment on column OBRF_833.cod_empresa_dest     is 'C�digo da Empresa de Destino do BCM que foi gerado neste passo para esta execu��o do processo';

comment on column OBRF_833.data_emissao         is 'Data de emiss�o do BCM que foi gerado neste passo para esta execu��o do processo';
comment on column OBRF_833.quantidade_total     is 'Quantidade total do BCM que foi gerado neste passo para esta execu��o do processo';
comment on column OBRF_833.valor_total          is 'Valor total do BCM que foi gerado neste passo para esta execu��o do processo';

comment on table OBRF_833                       is 'Tabela respons�vel por gravar os Boletins de Circula��o de Mercadoria (BCMs) que foram gerados em cada passo nas execu��es dos processos.';

exec inter_pr_recompile;
