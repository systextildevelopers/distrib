create table obrf_250 (
  codigo_empresa            number(3)         default 0     not null,
  fornecedor9               number(9)         default 0     not null,
  fornecedor4               number(4)         default 0     not null,
  fornecedor2               number(2)         default 0     not null,
  nivel_prod_generico       varchar2(1)       default ' '   not null,
  grupo_prod_generico       varchar2(5)       default ' '   not null,   
  subgrupo_prod_generico    varchar2(3)       default ' '   not null,
  item_prod_generico        varchar2(6)       default ' '   not null,
  nivel_prod_final          varchar2(1)       default ' '   not null,
  grupo_prod_final          varchar2(5)       default ' '   not null,
  subgrupo_prod_final       varchar2(3)       default ' '   not null,
  item_prod_final           varchar2(6)       default ' '   not null);

comment on table obrf_250 is 'Tabela de Relacionamento de Produtos Genéricos com Produtos Finais';

comment on column obrf_250.codigo_empresa is 'Codigo da empresa';
comment on column obrf_250.fornecedor9 is 'CNPJ do fornecedor';
comment on column obrf_250.fornecedor4 is 'CNPJ do fornecedor';
comment on column obrf_250.fornecedor2 is 'CNPJ do fornecedor';
comment on column obrf_250.nivel_prod_generico is 'Nivel do produto genérico';
comment on column obrf_250.grupo_prod_generico is 'Grupo do produto genérico';
comment on column obrf_250.subgrupo_prod_generico is 'Subgrupo do produto genérico';
comment on column obrf_250.item_prod_generico is 'Item do produto genérico';
comment on column obrf_250.nivel_prod_final is 'Nivel do produto final';
comment on column obrf_250.grupo_prod_final is 'Grupo do produto final';
comment on column obrf_250.subgrupo_prod_final is 'Subgrupo do produto final';
comment on column obrf_250.item_prod_final is 'Item do produto final';


alter table obrf_250
add constraint pk_obrf_250 primary key (codigo_empresa, fornecedor9, fornecedor4, fornecedor2, nivel_prod_final, grupo_prod_final, subgrupo_prod_final, item_prod_final);

create synonym systextilrpt.obrf_250 for obrf_250;

exec inter_pr_recompile;