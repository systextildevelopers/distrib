create table obrf_290 (
  codigo_empresa            number(3)         not null,
  serie	                varchar2(3)       default ' ',
  natureza_operacao	    number(3)         default 0,
  condicao_pagto            number(3)         default 0,
  cod_msg_relaciona_nfs     number(6)         default 0);

comment on table obrf_290 is 'Tabela de Par�metros de NF de compra de consignados';

comment on column obrf_290.codigo_empresa is 'Codigo da empresa';
comment on column obrf_290.serie is 'Serie das notas de compra a serem geradas';
comment on column obrf_290.natureza_operacao is 'Natureza de opera��o que ser� usada nas notas';
comment on column obrf_290.condicao_pagto is 'Condi��o de pagamento';
comment on column obrf_290.cod_msg_relaciona_nfs is 'C�digo Mensagem Relacionamento NFs';



alter table obrf_290
add constraint pk_obrf_290 primary key (codigo_empresa);

create synonym systextilrpt.obrf_290 for obrf_290;

exec inter_pr_recompile;