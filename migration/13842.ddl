create global temporary table temp_001 (
name varchar2(20) not null,
int1 number(9),
int2 number(9),
int3 number(9),
int4 number(9),
int5 number(9),
int6 number(9),
str1 varchar2(20),
str2 varchar2(20),
str3 varchar2(20),
str4 varchar2(20),
str5 varchar2(20),
str6 varchar2(20)
) on commit delete rows;

comment on table temp_001 is 'Registros temporários para usar como filtros em consultas e relatórios';

create table temp_002 (
nome_form varchar2(12),
solicitante varchar2(20),
codigo_empresa number(3),
objeto blob,
constraint pk_temp002 primary key (nome_form, solicitante, codigo_empresa)
);

comment on table temp_002 is 'Parametros default do framework de registros temporários';

exec inter_pr_recompile;

