insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_e500', 'Romaneio de Retorno da Ordem de Servi�o de Confec��o',0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_e500', 'menu_ad67', 1 , 23, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Relaci�n de Retorno de la Orden de Servicio de Confecci�n'
where hdoc_036.codigo_programa = 'obrf_e500'
  and hdoc_036.locale          = 'es_ES';

commit;

/

exec inter_pr_recompile;