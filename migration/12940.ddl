insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f290', 'Parāmetros para NF de compra de consignados', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f290', 'obrf_m150', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Configuraciones para DF de compra de consignados
'
 where hdoc_036.codigo_programa = 'obrf_f290'
   and hdoc_036.locale          = 'es_ES';
commit;
