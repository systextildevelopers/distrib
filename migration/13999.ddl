insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpf_f037', 'Mistura do Lote', 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpf_f037', 'pcpf_menu' , 1, 0, 'N', 'S', 'N', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Mistura do Lote'
where hdoc_036.codigo_programa = 'pcpf_f037'
  and hdoc_036.locale          = 'es_es';

commit work;