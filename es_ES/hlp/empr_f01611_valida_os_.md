Solicitar servicio de reparación - envío
---
Elija una de las opciones:

** 0 ** * - * Antes de comenzar los procedimientos de envío, las partes deben ser liberados ya que la producción de reparación en la etapa de identificación de reparación.

** 1 ** * - * Antes de comenzar los procedimientos de envío, las piezas deben ser como en la reparación de la producción de la etapa de identificación.
