Tabela de Pre�o
-------------------
A tabela de pre�os informada neste campo deve obrigatoriamente existir na capa da tabela de pre�os. Portanto, caso o objetivo seja gerar uma tabela nova, � necess�rio acessar o programa **Tabela de Pre�o (pedi_f350)** (acess�vel neste campo pelo *Zoom*) e cadastrar a capa da tabela.

A tabela de pre�o dever� obrigatoriamente pertencer ao n�vel de produto informado no campo **PRODUTO**.
