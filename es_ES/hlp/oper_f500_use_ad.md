Autenticar usuarios de ERP con Active Directory
===============================================

Si los servidores donde est� instalado el ERP tienen acceso a un servidor de
Active Directory utilizado por la empresa para autenticar a sus usuarios en su
red local, ese mismo Active Directory puede ser utilizado para autenticar el
acceso al ERP.

Esta opci�n debe ser evaluada por los responsables de tecnolog�a de la empresa
usuaria del ERP.

Para habilitar esta opci�n, la direcci�n del servidor y el dominio de Active
Directory deben definirse en la configuraci�n del sistema de la empresa en el ERP.

Cuando se defina esta informaci�n, el registro de usuarios de esta empresa
ofrecer� la opci�n "Autenticar la contrase�a del usuario a trav�s de Active
Directory". Si esta opci�n est� activada para un determinado usuario, su clave
de acceso al ERP no quedar� registrada en la ficha de usuario del ERP, sino que
ser� validada por Active Directory.

Por lo tanto, primero se debe activar esta opci�n en el registro de la empresa,
y luego, para cada usuario de esa empresa, existir� la opci�n de autenticar su
acceso a trav�s del Active Directory configurado.
