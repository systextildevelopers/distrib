Relat�rio de Utiliza��o de Adiantamentos (cpag_e605)
-------------

Este programa emite um relat�rio informando os t�tulos que foram utilizados para realizar o pagamento dos adiantamentos. Adiantamentos que n�o tiverem t�tulos relacionados n�o ser�o exibidos no relat�rio.
