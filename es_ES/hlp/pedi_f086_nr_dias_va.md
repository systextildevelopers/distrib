NÚMERO DE DIAS 
==============

  Caso este parámetro esté registrado, va remplazar el número de días registrado en (empr_f001 - Parámetros de empresa / Ventas / Por empresa / aba ++), en el cálculo de la fecha final de vigencia del límite de crédito.
