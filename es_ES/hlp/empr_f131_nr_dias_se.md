NÚMERO DE DIAS PARA VALIDAD DEL LIM. DE CRÉDITO
=========================================

Ese parámetro sirve para calcular la fecha final de vigencia del límite de crédito en el programa pedi_f001 -  Registro de Informaciones Financieras de los Clientes.

Sirve para indicar el número máximo de días que es permitido dar al cliente de la validad del límite de crédito y es utilizado cuando no se tiene definido el número de días del límite de crédito por la conceptuación del cliente. 

- Si el número de días registrado en ese campo es menor que cero, no será permitido llenar la fecha de validad mayor o igual a la fecha corriente.
- Si el número de días registrado en ese campo es igual a cero, será permitido llenar cualquier fecha de validad sin hacer ninguna consistencia.
- Se el número de días registrado en ese campo es mayor que cero, será permitido llenar cualquier fecha de validad hasta la fecha corriente más el número registrado en ese campo.
