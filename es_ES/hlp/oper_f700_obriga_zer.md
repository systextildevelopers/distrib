ESPERE A CERO ANTES DE VOLVER A LEER
====================================

Esta opci�n est� disponible cuando la configuraci�n del puerto serie utiliza
_lectura continua con salida de campo autom�tica despu�s de la estabilizaci�n_.

Si est� habilitado, luego de salir del campo luego de una lectura exitosa, el
programa solo aceptar� una nueva lectura _despu�s de recibir un valor cero en el
puerto serie_.

Esta opci�n es �til cuando, por ejemplo, una b�scula presenta oscilaciones que
generan pesos estables de vez en cuando. En esta situaci�n, la salida autom�tica
del campo puede provocar que el mismo pesaje se registre varias veces con pesos
ligeramente diferentes. Para evitarlo, el programa se ve obligado a esperar a
recibir un peso cero antes de aceptar un nuevo pesaje.

Esta funcionalidad es importante en programas que registran registros
autom�ticamente, es decir, sin que el operador necesite acceder al formulario
entre pesajes. Esto asegura que entre uno y otro se reciba peso cero. Si el
programa no registra autom�ticamente registros sucesivos con salida autom�tica y
estabilizada, no se debe activar esta opci�n, ya que en este caso no es
necesaria y no hay certeza de que el programa reciba el peso cero a la espera de
la lectura desde el puerto serie.

Algunas b�sculas _no env�an peso cero_ por el puerto serie. En este caso no se
debe activar esta opci�n.
