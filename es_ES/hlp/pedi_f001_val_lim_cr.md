VALIDAD L�?MITE DE CRÉDITO
=========================

Informar la fecha de validad del límite de crédito. Irá hacer verificaciones con el campo:
1. "NR DE DIAS PARA VALIDAD DEL LIM. DE CRÉDITO" configurado en: "empr_f825 - parámetros de empresa / Ventas / Por empresa / aba ++";
2. "NÚMERO DE D�?AS" configurado en "pedi_f086 - Concepto del Cliente".


Ex da verificación:  
dt1 = Fecha actual: 01/01/2013  
nr1 = Parámetro (Configuración de Parámetros / Ventas): 10  
nr2 = Número de Días (Concepto del Cliente): 15  
dt2 = Validad Límite Crédito: 15/01/2013  

Fecha Máxima = dt1 + nr2 = 16/01/2013

1. si dt2 < Fecha Máxima, la fecha informada es válida.  
2. solamente si la primera situación es válida, irá hacer la siguiente validación. 
si dt2 < dt1 + nr1; 15/01/2013 < 11/01/2013. Como no es válida, habrá mensaje informando.
