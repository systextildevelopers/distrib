INSERTE EL ROLLO DE TEJIDO TERMINADO EN STOCK SIN CONTROL DE LOTE


Elija una de las opciones:

S - Sí
N - No

Este parámetro se aplicará en el proceso de ingreso de los rollos de tela procesada en stock, y considerará el lote como único (0) si se elige la opción "No" para este parámetro,
no influir en el lote de producción del rollo.
