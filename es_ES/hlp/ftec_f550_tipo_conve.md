Tipo Convers�o

**1** - *Largura cadastrada nas fichas t�cnicas em metros (Malharia).*
**2** - *Largura cadastrada nas fichas t�cnicas em cent�metros (Retil�neos, Tecidos planos, El�stico, etc.).*

**Unidade entrada = Unidade do produto**
Nesta op��o n�o faz nenhuma convers�o.

**Unidade entrada = Quilos**
Quando Unidade entrada (Quilos) for igual a unidade de medida do produto (KG ou Quilos) n�o faz nenhuma convers�o.

**Unidade entrada = Metros**
Quando Unidade entrada (Metros) for igual a unidade de medida do produto (M ou MT - Metros) n�o faz nenhuma convers�o.
