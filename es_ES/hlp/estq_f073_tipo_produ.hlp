INVENTARIO
-------------------------------------------------

El proceso de inventario consiste b�sicamente en tres procesos.

El primero es la CARGA DEL ARCHIVO DE INVENTARIO (estq_073) que puede ser hecho v�a carga o v�a colecta directa. Cuando el colector no estuviera conectado directamente al banco de datos, la colecta es hecha y el archivo generado es importado por el proceso 'IMPORTAR' del inventario. En ese momento es posible ingresar en la pantalla a qu� dep�sito pertenecen las informaciones colectadas. Si no fuera ingresado el c�digo del almac�n (dejado igual a cero), el proceso asumir� como almac�n del inventario, el almac�n donde se encuentra el volumen colectado. Cuando el colector estuviera conectado directamente al banco de datos, la colecta es hecha directamente en la tabla del inventario (estq_073) v�a un programa (inte_f980) que exige que sea ingresado el almac�n que est� siendo inventariado. En este �ltimo caso, nao es necesario ejecutar el proceso 'IMPORTAR'.

El segundo proceso es el REPORTE DEL INVENTARIO que comparar� las informaciones inventariadas  (estq_073) con las informaciones de los vol�menes en el Syst�xtil (pcpt_020, estq_060 ou pcpf_060). El reporte puede ser listado anal�ticamente (mostrando volumen a volumen) o sint�ticamente (acumulando por producto/lote/almac�n), con o sin quiebra por lote.

El tercer proceso (que est� disponibilizado s�lo para telas - rollos) es el que har� las ACTUALIZACIONES en el Syst�xtil con base en el archivo de inventario. Para ejecutar ese proceso es necesario ingresar las transacciones de salida, entrada, fecha en que deber�n ser hechos los lanzamientos en stock y el almac�n que est� siendo inventariado. En este momento, podr�n ser encontradas diversas situaciones que ser�n mostradas a seguir con el respectivo tratamiento que el sistema har�.

SITUACI�N 0: El rollo fue inventariado en el almac�n 'A' y en el Syst�xtil el rollo est� en el almac�n 'A' en stock (situaci�n 1-En stock, 3-Solicitado para facturaci�n � 5-Colecta confirmada).
TRATAMIENTO SITUACI�N 0: El proceso no har� nada, pues el rollo est� en stock y en el almac�n inventariado.

SITUACI�N 1: El rollo fue inventariado en el almac�n 'A' y en el Syst�xtil el rollo no existe.
TRATAMIENTO SITUACI�N 1: El proceso no har� nada. Solamente ser� listado en el reporte que el rollo debe ser repesado manualmente pues no hay informaci�n alguna sobre qu� producto, lote, peso que se refiere el rollo. De esta forma, el rollo recibir� una nueva etiqueta y tendr� un nuevo n�mero.

SITUACI�N 2: El rollo fue inventariado en el almac�n 'A' y en el Syst�xtil el rollo est� facturado (situaci�n 2-Facturado).
TRATAMIENTO SITUACI�N 2: Ese rollo ser� reativado, o sea, la situaci�n del mismo ser� alterada para 1-En stock y ser� dada entrada de su peso en el stock inventariado (estq_040). Ser� usada la transacci�n de entrada del rollo ingresada en la pantalla de solicitud del proceso.

SITUACI�N 3: El rollo fue inventariado en el almac�n 'A' y en el Syst�xtil el rollo est� facturado (situaci�n 2-Facturado) en el almac�n 'B'.
TRATAMIENTO SITUACI�N 3: Ese rollo ser� reactivado, o sea, la situaci�n del mismo ser� alterada para 1-En stock y ser� dada entrada de su peso en el stock inventariado (estq_040). Ser� usada la transacci�n de entrada del rollo ingresada en la pantalla de solicitud del proceso. Tambi�n ser� alterado el almac�n del rollo para el almac�n inventariado.

SITUACI�N 4: El rollo fue inventariado en el almac�n 'A' y en el Syst�xtil el rollo se encuentra en el stock (situaci�n 1-En stock o 4-En tr�nsito) en el almac�n 'B' que pertenece a la misma empresa del almac�n 'A'.
TRATAMIENTO SITUACI�N 4: Este rollo ser� transferido del almac�n 'B' para el almac�n 'A' usando la transacci�n de entrada de rollo ingresada en la pantalla de solicitud del proceso para dar entrada en el almac�n 'A' y la transacci�n de salida del rollo ingresada en la pantalla de solicitud del proceso para dar salida en el almac�n 'B'. Ser� dada baja en el stock del almac�n 'B' y entrada en el stock del almac�n 'A'.

SITUACAO 5: El rollo fue inventariado en el almac�n 'A' y en el Syst�xtil el rollo se encuentra en el stock (situaci�n 1-En stock o 4-En tr�nsito) en el almac�n 'C' que pertenece a otra empresa, diferente del almac�n 'A'.
TRATAMIENTO SITUACI�N 5: Ser� dada una salida del rollo en el dep�sito 'C' usando la transacci�n de salida ingresada en la pantalla de solicitud del proceso y ser� dada una entrada en el almac�n 'A' usando la transacci�n de entrada ingresada en la pantalla de solicitud del proceso. Ser� dada baja en el stock del almac�n 'C' y entrada en el stock del almac�n 'A'.

SITUACI�N 6: El rollo no fue inventariado en el almac�n 'A' y en el Syst�xtil el rollo s encuentra en el stock (situaci�n 1-En stock) en el almac�n 'A'.
TRATAMIENTO SITUACI�N 6: El rollo ser� bajado del almac�n 'A' usando la transacci�n de salida de rollo ingresada en la pantalla de solicitud del proceso. Ser� dada baja en el stock del almac�n 'A'.

SITUACI�N 7: El rollo fue inventariado en el almac�n 'A' y en el Syst�xtil el rollo se encuentra asociado a un pedido de venta (situaci�n 3-Solicitado para facturar � 5-Colecta confirmada) en el almac�n 'B'.
TRATAMIENTO SITUACI�N 7: El proceso no har� nada, solamente ser� listado en el reporte que el rollo est� asociado a un pedido de venta. Este rollo deber� ser tratado manualmente.

SITUACI�N 8: El rollo fue inventariado en el �lmac�n 'A' y en el Syst�xtil el rollo se encuentra asociado a un pedido de venta (situaci�n 3-Solicitado para facturar o 5-Colecta confirmada) en el almac�n 'A'.
TRATAMIENTO SITUACI�N 8: Si en la pantalla de solicitud del proceso, el campo EMB estuviera igual a 'N', el proceso desasociar� el rollo del pedido y colocar a su situaci�n como 1-En stock.

Cuando se inicia un inventario, se debe tomar el cuidado de saber cu�l es la fecha en que fue iniciado el mismo, porque los vol�menes que estuvieran en el almac�n y sean facturados despu�s del inicio del inventario, no ser�n considerados como facturados para efecto de la comparaci�n, as� como los vol�menes que dieran entrada en el almac�n despu�s del inicio del inventario tambi�n no ser�n considerados.

Al iniciar un inventario, todos los vol�menes que est�n en el almac�n deber�n ser inventariados. Si alg�n vol�men tuviera que ser facturado, este deber� ser inventariado primero. Todos los vol�menes que sean producidos despu�s del inicio del inventario no deber�n ser inventariados.

El campo 'ALMAC�N' en la pantalla del proceso de inventario dejando igual a cero, indica que est� siendo inventariada la empresa entera (todos los almacenes). Este procedimiento no es aconsejable. Normalmente se debe inventariar almac�n a almac�n para tener un mejor control. Eso no impide que m�s de un dep�sito sea inventariado al mismo tiempo. Basta que sean identificados los almacenes al momento de  la carga de los archivos colectados. Pero, independiente de eso, se debe ejecutar la importaci�n, un almac�n de cada vez. Para hacer la actualizaci�n del inventario, ser� obligatorio ingresar un c�digo de almac�n.

OBS: Despu�s de hacer la actualizaci�n del inventario del dep�sito ingresado, el proceso har� una barrida en los dem�s almacenes del mismo tipo de volume y hacer la correcci�n del stock de acuerdo con la cantidad existente en vol�menes (En el caso de telas, suma de las cantidades de los rollos en stock). Siempre que encuentra una cantidad de m�s en la tabla de stock, har� una salida usando la transacci�n de salida del rollo en el stock. Siempre que encuentre una cantidad de menos en la tabla de stock, har� una entrada usando la transacci�n de entrada del rollo en el stock.

