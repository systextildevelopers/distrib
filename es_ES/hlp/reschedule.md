Reprogramaci�n diaria o semanal
==============================

Le permite reprogramar el proceso actual eligiendo los d�as de la semana, la hora del d�a y la fecha de finalizaci�n.

El proceso programado se ejecutar� a la hora especificada y luego, si se configura la reprogramaci�n, se programar� una copia del proceso actual para el d�a y la hora definidos.

 - **Diario/Semanal**: los d�as de la semana y la hora a la que debe ejecutarse el proceso.
 - **�ltimo d�a**: el d�a m�ximo para ejecutar el proceso. No se programar�n procesos para _despu�s_ de esta fecha.
   Si no se informa el d�a m�ximo, el proceso se reprogramar� sin fecha de finalizaci�n.
 - En la parte inferior hay un campo que describe t�cnicamente la informaci�n que se registrar�.
   Es para uso experto.
 - A continuaci�n, una descripci�n de la frecuencia seleccionada.

Para grabar la reprogramaci�n, simplemente grabe el registro (F9) y regrese de Zoom.

Para eliminar una reprogramaci�n, simplemente haga clic en "Ninguno" y guarde.

Para descartar cambios de horario, simplemente cancele Zoom.

> _La informaci�n ser� efectiva cuando se guarde el proceso programado (F9)._
