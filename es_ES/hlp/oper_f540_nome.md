Credenciales de usuario para enviar correo electr�nico
==============================================

Estos datos son necesarios para que el usuario env�e correos electr�nicos a trav�s de Syst�xtil ERP en su nombre.

> Este dato no es necesario si la empresa del usuario dispone de las credenciales para el env�o de correos electr�nicos _**a nombre de la empresa**_ registrada en el ERP. Si las credenciales del usuario no existen, se utilizar�n las credenciales de la empresa.
>
> En este caso, si hay inter�s en que el nombre del usuario est� presente en el correo electr�nico enviado, entonces se debe completar el campo **Nombre**.
>
> Por esta regla, los campos no son obligatorios.

**Atenci�n:** Para registrarse o cambiar la contrase�a, introd�zcala en el campo respectivo. Si el campo est� vac�o, la contrase�a no se cambiar� (o eliminar�). Si no desea cambiar la contrase�a, deje el campo vac�o.
