OP��O RELAT�RIO

   Este campo definir� o tipo de filtro (por data ou por m�s) e como ser� a impress�o das informa��es:

   1 - POR DATA
   2 - POR M�S/ANO (CURVA ABC)


   Ao escolher a op��o 2 - POR M�S/ANO (CURVA ABC), ser�o habilitados campos para informar os percentuais a serem considerados em cada uma das colunas da op��o curva ABC.
