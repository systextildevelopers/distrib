Varia��o / Limite

**0 - Varia��o**
- Ao escolher a op��o Varia��o, o sistema ir� calcular o percentual de varia��o sobre o Peso previsto e o Peso real e ir� checar se a diferen�a est� entre o percentual aceit�vel. Nesta op��o, o sistema ir� verificar a diferen�a entre o percentual aceit�vel tanto para baixo quanto para cima.

**1 - Limite**
- Ao escolher a op��o Limite, o sistema ir� calcular o percentual de varia��o sobre o Peso previsto e o Peso real e ir� checar se a diferen�a est� entre o percentual aceit�vel. Nesta op��o, por�m, o sistema ir� aceitar que a diferen�a seja somente para baixo, n�o permitindo pesar mais. O Peso previsto � o limite m�ximo.
