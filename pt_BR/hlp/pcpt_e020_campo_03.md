OP��O RELAT�RIO


1. Por Data (Anal�tico)
2. Por Data (Sint�tico)
3. Por M�quina (Anal�tico)
4. Por Turno (Sint�tico)
5. Por Produto/M�quina (Anal�tico)     
6. Por Produto/Maquina (Sint�tico)
7. Por Operador (Anal�tico)            
8. Por Operador/M�quina (Sint�tico)
9. Por Unidade de Medida/Produto (Anal�tico)
10. Por Unidade de Medida/Produto (Sint�tico)
11. Por Dep�sito/Produto (Anal�tico)
12. Por Dep�sito/Produto (Sint�tico)
13. Por Produto (Sint�tico)
14. Por Tecel�o (Anal�tico)
15. Por M�quina/Tecel�o (Sint�tico)

**Observa��o:**
>Nas op��es acima, os totalizadores aparecer�o quando o detalhe possuir mais de um registro.
>Por exemplo, em um totalizador por data, s� ser� apresentado se tiver mais de um produto na mesma.

	
    DATA		PRODUTO	            QUANTIDADE
    10/01/05	4.TEC01.001.000001    100
                4.ATT82.001.000234    200
    TOTAL DA DATA....:                300
	
    11/01/15    4.ATT82.001.000001    50
    12/01/15    4.00001.001.000001    50
	            4.ATT82.002.79500S   100
	TOTAL DA DATA....:               150

Como pode ser observado no exemplo acima, s� ser� exibido o totalizador quando a data possuir **mais de um produto**.