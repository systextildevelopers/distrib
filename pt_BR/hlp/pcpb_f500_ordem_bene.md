CLASSIFICA��O DE NUANCE POR OB/OT
______________________________________________

Este programa tem como objetivo permitir informar o nuance por ordem de beneficiamento/tingimento que ser� utilizado na pesagem dos rolos.

Ao preencher o campo ORDEM BENEFICIAMENTO, o campo ORDEM TINGIMENTO ficar� desabilitado, e vice versa.

Se for identificado que existe um rolo pesado para a ordem de beneficiamento e o c�digo do nuance j� foi classificado por esta tela, o processo n�o ser� executado.

O bot�o CONFIRMAR ir� classificar o nuance informado nesta tela para a ordem parametrizada. Ao realizar a pesagem de rolos pelas telas 'Baixa da Guia de Beneficiamento (pcpb_f082)' e 'Baixa da Guia de Beneficiamento (pcpb_f182)', o sistema ir� verificar se existe um nuance classificado para a ordem. Caso exista, o mesmo ser� automaticamente inserido nestas telas, e n�o permitir� alterar essa informa��o. Caso contr�rio, o campo fica habilitado para preenchimento do usu�rio.
