PROCESSOS
----
 Caso estiver digitando em um dos processos abaixo (ex. Pedido de venda) e tiver registro de  relacionamento para a natureza de opera��o ir� substituir, caso contr�rio n�o.

**Informe:**
  **1** - DIGITA��O DE PEDIDO;
  **2** - MANUTEN��O DE PEDIDO;
  **3** - C�PIA DE PEDIDO;
  **4** - SUBSTITUI��O DE PEDIDO;
  **5** - IMPORTA��O DE PEDIDO;
  **6** - NOTAS DE SA�DA;
  **7** - LOJA;
  **8** - ASSOCIA��O DE KITS;
  **9** - NOTAS DE ENTRADA;
  **10** - INTEGRA��O COM LOJAS;
  **0** - A DEFINIR (n�o ir� alterar em nenhum processo).

*Observa��o: Os campos CLASSIFICA��O FISCAL E CNPJ CLIENTE somente ser�o considerados nos processos:*
***1*** *- DIGITA��O DE PEDIDO ou **6** - NOTAS DE SA�DA*

*A op��o de INTEGRA��O COM LOJAS (**10**) vai desconsiderar os campos de Classifica��o Fiscal, CNPJ do cliente, Pessoa, C�digo de atividade econ�mica, Inscri��o Estadual e c�digo dep�sito. Logo se esta for informada os campos receberam seus valores padr�es.*
