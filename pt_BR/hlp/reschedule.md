Reagendamento di�rio ou semanal
===============================

Permite reagendar o processo atual escolhendo os dias da semana, a hora do dia e a data final.

O processo agendado ser� executado na hora determinada e ent�o, se houver reagendamento configurado, ser� agendada uma c�pia do processo atual para o dia e a hora definidos.

 - **Di�rio/semanal**: os dias da semana e o hor�rio nos quais o processo deve ser executado.
 - **�ltimo dia**: o dia m�ximo para execu��o do processo. Nenhum processo ser� agendado para _depois_ dessa data.
   Se o dia m�ximo n�o for informado, o processo ser� reagendado sem data para acabar.
 - Na parte de baixo existe um campo que descreve tecnicamente as informa��es que ser�o gravadas.
   � para uso dos especialistas.
 - Logo abaixo, uma descri��o da periodicidade selecionada.

Para gravar o reagendamento, basta gravar o registro (F9) e retornar do Zoom.

Para remover um reagendamento, basta acionar "Nenhum" e gravar.

Para descartar as altera��es de agendamento, basta cancelar o Zoom.

> _As informa��es ser�o efetivadas quando o processo agendado for gravado (F9)._
