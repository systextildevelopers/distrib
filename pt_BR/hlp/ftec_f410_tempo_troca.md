Tempo troca
--------------------------------------------------------------------------------------------

O tempo de troca a ser informado neste campo corresponde ao tempo necess�rio para efetuar atividades como troca de ponto, transforma��o, troca de fio e regulagem da m�quina para produzir o produto nesta m�quina.

O tempo deve ser informado em minutos, pois ele ser� utilizado para c�lculo do campo Lote m�nimo de produ��o.
