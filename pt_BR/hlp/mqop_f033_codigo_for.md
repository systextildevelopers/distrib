C�DIGO DA F�RMULA

Este campo tem por finalidade indicar que f�rmula ser� utilizada pelo programa de� Apontamento de Produ��o (efic_f060) no c�lculo das quantidades produzidas.

Escolha a formula conforme a necessidade da maquina e produto:

Formulas para uso em diversos processos
---

**00 - Digita��o manual da produ��o.**
Ser� informada a produ��o diretamente no campo de quantidade produzida.

**01 - Calcula a produ��o usando um rel�gio que conta o tempo que a maquina ficou em opera��o.**
Esta formula, poder� ser usado para as maquinas em que a produ��o � calculada em fun��o do tempo que a mesma estiver em produ��o.

* Tempo = Leitura atual - Leitura anterior.

*  Passo�= De quanto em quanto o rel�gio conta.

*  Fator�= �ndice multiplicador para transformar o tempo em minutos.

*  Capacidade = Quanto esta cadastrado na ficha t�cnica que se consegue fazer do produto na maquina por minuto.

*  Produ��o = Tempo x passo x fator x capacidade.

Formulas para os processos de Fia��o
---

**02 - Calcula a produ��o, recebendo a quantidade de volumes produzidos.**
Vai calcular a produ��o usando a quantidade de volumes produzidos (Fardos, Latas, Rolos), multiplicando pelo quantidade m�dia contida neste volume.

* Peso Volume = Peso do volume padr�o produzido na maquina.

* Quantidade Volumes = Quantos volumes foram produzidos no turno.

* Produ��o = Quantidade volumes x Peso Volume.



**03 - Calcula a produ��o do rel�gio, usando o titulo padr�o da maquina.**
Usa-se esta formula para os casos em que o produto retirado da maquina n�o � o produto final, que ainda tem uma titulagem diferente� daquela informada na ficha t�cnica do produto.

* Produ��o rel�gio = Leitura atual - Leitura anterior.

* Passo�= De quanto em quanto o rel�gio conta.

* Fator = �ndice multiplicador que transforma a unidade do rel�gio na unidade de medida compat�vel com a unidade de medida do titulo.

* Titulo�= Qual o titulo m�dio que a maquina esta produzindo (informado no cadastro da maquina).

* Produ��o = Produ��o Rel�gio x Passo x Fator / T�tulo.


**04 - Calcula a produ��o do rel�gio, usando o t�tulo do produto.**
Usa-se esta formula para calcular a produ��o usando o t�tulo padr�o do produto.

* Produ��o rel�gio = Leitura atual - Leitura anterior.

* Passo = De quanto em quanto o rel�gio conta.

* Fator�= �ndice multiplicador que transforma a unidade do rel�gio na unidade de medida compat�vel com a unidade de medida do titulo.

* Titulo = Qual o titulo padr�o do produto� (informado na ficha t�cnica do produto).

* Produ��o = Produ��o Rel�gio x Passo x Fator / Titulo.

Formulas para os processos de Tecelagem Plana.
---

**05 - Calcula a produ��o do rel�gio, usando a quantidade de batidas por metro do produto.**
Usa-se esta formula para calcular a produ��o da tecelagem plana que normalmente tem seus produtos controlados em metros lineares.

*  Produ��o rel�gio = Leitura atual - Leitura anterior.

*  Passo = De quanto em quanto o rel�gio conta.

*  Fator = �ndice multiplicador que transforma a unidade do rel�gio quantidade de batidas.

*  Batidas por metro� = Quantas batidas por metros tem o produto� (informado na ficha t�cnica do produto).

*  Produ��o = Produ��o Rel�gio x Passo x Fator / Batidas por metro.
