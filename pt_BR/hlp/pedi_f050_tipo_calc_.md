TIPO C�LCULO DE SUBSTITUI��O TRIBUT�RIA


#F�rmula de c�lculo para o TIPO(1):

*Base de C�lculo de Substitui��o*
>*(Base de ICMS* * *((100.00 - % Substitui��o)/100.00)))*

*Valor do ICMS de Substitui��o*
>*(Base ICMS substitui��o* * *% ICMS)/100.00)*

#F�rmula de c�lculo para o TIPO(2):

Base de C�lculo de Substitui��o
>*((Base ICMS + % Substitui��o) - % redu��o substitui��o)*

Valor do ICMS de Substitui��o
>*((Base ICMS Substitui��o* * * % ICMS Subst. Interna) - Valor de ICMS)*

#F�rmula de c�lculo para o TIPO(3):

Base de C�lculo de Substitui��o
>*Igual a base ICMS*

Valor do ICMS de Substitui��o
>*Base ICMS substitui��o * *% Substitui��o)*

#F�rmula de c�lculo para o TIPO(4):

Base de C�lculo de Substitui��o:

>*((Valor Total Prod + Despesas - Descontos + Valor IPI) +**
>*((Valor Total Prod + Despesas - Descontos + Valor IPI) + IVA%) -** 
>*((Valor Total Prod + Despesas - Descontos + Valor IPI) + IVA%) - % Redu��o))*

Valor do ICMS de Substitui��o:
>*(Base Calculo ** % ICMS Substitui��o) - Vlr ICMS*

Obs.: Esta op��o 4, desconsidera o par�metro "IPI sobre Substitui��o".

#F�rmula de c�lculo para o TIPO(5):

Base de C�lculo de Substitui��o: 
>*Valor Total Prod + Despesas - Descontos + Valor IPI*

Valor do ICMS de Substitui��o:
>*(Base Calculo * ICMS Subst%)

#OBSERVA��O PARA O TIPO(9):

Informa manualmente os valores de substitui��o tribut�ria nos itens da nota fiscal,s� ter� influ�ncia na digita��o de notas fiscais manuais de sa�das (obrf_f020/obrf_f025) e de entrada com emiss�o (obrf_f050/obrf_f055).

#OBSERVA��O DO C�LCULO DA SUBSTITUI��O DO ICMS:
O valor total da nota ser� Valor Total dos Produtos + Valor do ICMS de Substitui��o.
