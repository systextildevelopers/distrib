ORDEM PRODU��O


   No processo de endere�amento de ordens � poss�vel a mesma ordem de beneficiamento estar utilizando um ou mais endere�os.
   
   Para que seja poss�vel a mesma ordem possuir mais endere�os, devemos cadastrar novamente a ordem de beneficiamento informando o outro endere�o que deseja associar a ordem de beneficiamento.
