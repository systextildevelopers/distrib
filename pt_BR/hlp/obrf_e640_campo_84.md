Marcar/Desmarcar Pagto.
-------------------------
Essa op��o permite escolher pela situa��o do fechamento quais entradas e ocorr�ncias devem ser listadas no relat�rio. As op��es s�o:

> - 0 � Pendente
> - 1 � Marcado para pagamento/desconto
> - 2 � Pago/Descontado
> - 9 � Todas situa��es (default)