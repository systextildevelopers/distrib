Lote m�nimo
---------------------------------------------------------------------------------------

A informa��o apresentada no campo Lote m�nimo, representa a quantidade m�nima que deve ser programada / produzida na m�quina sem que ocorra perda de produ��o.

O campo � calculado com base na f�rmula abaixo:

Lote m�nimo = (((((Tempo troca * 95) / 5) / Tempo trabalhado) * Produ��o di�ria) * Peso padr�o)

Onde:
95% � a toler�ncia do tempo de troca.
5% � o tempo utilizado para que a m�quina trabalhe sem perdas.
Tempo trabalhado, campo da tela, � a soma do tempo dispon�vel dos turnos.
Produ��o di�ria, campo da tela, � a produ��o calculada com base nas vari�veis.
Peso padr�o, campo da tela anterior, � o peso padr�o de cada rolo deste produto.
