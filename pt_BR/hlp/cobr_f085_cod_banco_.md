BANCO CORRESPONDENTE

#Existem casos onde o processo de cobran�a de um banco trabalha com mais de um banco. Para estes casos chamamos de "Banco Correspondente".

Este campo controla estes tipos de cobran�as, onde o banco faturado para o cliente remete a cobran�a para um banco terceiro. Nestes casos o c�digo deste banco dever� ser parametrizado neste campo. 

No momento do faturamento, quando o banco / portador utilizado no pedido estiver parametrizado para emitir o boleto automaticamente, 
o sistema ir� emitir o boleto banc�rio utilizando o layout do banco parametrizado neste atributo.

Este atributo trabalha em conjunto com o atributo "Envio dos arquivos de cobran�a pelo banco do atributo". 

Caso este atributo esteja desmarcado, o sistema ir� utilizar o layout do banco correspondente para criar o arquivo de remessa do cobran�a escritural. 

Caso o atributo estiver marcado o arquivo de remessa dever� ser feito com o layout do banco do atributo.

Este campo tem influencia apenas para o banco de atributo do BIC banco.
