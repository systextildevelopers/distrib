TIPO ENTRADA PADRÃO
--

Este tipo de entrada servirá para dar baixa de ordem de  serviço utilizando ordens de confecção.

Neste campo é informada a entrada padrão, que será utilizada no Sistema e que será pago ao terceiro. Isso é fundamental para que os relatórios de pagamentos de terceiros venham com valores corretos, os estoques sejam atualizados como configurado e as ordens sejam baixadas com correção.

Via zoom, é possível acessar a tela de cadastro de tipos de entradas, que também pode ser acessada através do Módulo de Terceiros.