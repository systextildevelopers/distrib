OPÇÃO DE C�?LCULO DO PREÇO DE VENDA A PRAZO E À VISTA
---

**Opção 1**:
Usando Tabela de Preços à Vista para se chegar ao preçoo a prazo. (acréscimo no preço à vista)

*Fórmula*:
PRECO_A_VISTA * (1 + % ACRESCIMO / 100)

**Opção 2**:
Usando Tabela de Preços a Prazo para se chegar ao preço a vista. (desconto no preço a prazo)

**Fórmula**:
PRECO_A_VISTA / (1 - (% ACRESCIMO / 100))

***PRAZO***

**Opção 3**:
Utilizando o %Acréscimo com parte do MARK-UP1

*Fórmula*:
PRECO_DE_CUSTO / ((MARK-UP1) * 100)
MARK-UP1 => (100 - %desp_diversas - %ICMS - %lucro - %desconto - %juros)

**Opção 4:**
Utilizando o %Acréscimo com parte do MARK-UP2

*Fórmula*:
PRECO_DE_CUSTO / ((MARK-UP2) * 100)
MARK-UP2 => (100 - %desp_diversas)    = PC1
             PC1 / (100 - %ICMS)      = PC2
             PC2 / (100 - %lucro)     = PC3
             PC3 / (100 - %desconto)  = PC4
             PC4 / (100 - %juros)     = MARK-UP2

***À VISTA***

**Opção 3**:
Utilizando o %Acréscimo com parte do MARK-UP1

*Fórmula*:
PRECO_DE_CUSTO / ((MARK-UP1) * 100)
MARK-UP1 => (100 - %desp_diversas - %ICMS - %lucro - %desconto)

**Opção 4**:
Utilizando o %Acréscimo com parte do MARK-UP2

*Fórmula*:
PRECO_DE_CUSTO / ((MARK-UP2) * 100)
MARK-UP2 => (100 - %desp_diversas)    = PC1
             PC1 / (100 - %ICMS)      = PC2
             PC2 / (100 - %lucro)     = PC3
             PC3 / (100 - %desconto)  = MARK-UP2
