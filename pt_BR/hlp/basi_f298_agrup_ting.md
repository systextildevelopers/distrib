AGRUP (AGRUPADOR TINGIMENTO)
-----

- Se o valor do campo for deixado com 0 (zero), significa que o tecido/fio/produto tingido não será agrupado com nenhum outro produto e poderá ser tinto separadamente.

- Se o valor do campo for maior que zero, será um produto que se deseja agrupar para o tingimento.

Neste caso, os produto que tiverem o mesmo código agrupador, serão os produtos que devem ser tingos juntos.

- Importante, na estrutura de produtos, não haverá validação se os produtos tem a mesma cor. Visto que os produtos poderam ter código de cores diferentes (PT0001 e PT0111) por características comerciais (venda de tecidos) mas que para o tingimento são a mesma cor (mesma cor de tingimento). Qualquer junção ou não dos produtos pela cor, ou pela cor de tingimento, serão tratadas nos processos que forem alterados ou implementados para fazer tais junções.

- Outra característica que somente será avaliada nos processos de tingimento serão tecidos com mesmo grupo e/ou processos de tingimento, cuja digitação ou geração de ordens de beneficiamento / tingimento irão consistir.
