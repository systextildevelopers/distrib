INSERE ROLO DE TECIDO ACABADO NO ESTOQUE SEM CONTROLE POR LOTE


Escolha uma das opções:

S - Sim
N - Não

Este parâmetro será aplicado no processo de entrada dos rolos de tecido beneficiado em estoque, sendo que irá considerar o lote como único (0) caso seja escolhida a opção "Não" para esse parâmetro,
não influenciando no lote de produção do rolo.
