QT PC.
-------------------------------------------------

A quantidade de peças seria quantas retilíneas são necessárias para gerar uma peça pronta.

No seguinte cenário temos:

- Uma GOLA produto 2.12008.P01.214284 consumo auxiliar 1 com quantidade de peças 2.

- Um PUNHO produto 2.12042.P01.214284 consumo auxiliar 4 com quantidade de peças 1.

- Um BOLSO produto 2.12009.P01.214284 consumo auxiliar 1 com quantidade de peças 4.

No exemplo da GOLA:
- Com uma peça de retilínea(CONS. AUX.) fazemos duas referências (nível 1)(QT PEÇAS).

No exemplo do PUNHO:
- Com quatro peças de retilínea (CONS. AUX.) fazemos uma referência(QT PEÇAS).

Para calcular o consumo, no caso de tiver quantidade de peças (QT PEÇAS), o calculo do consumo é para uma peça:

- CONSUMO = (((1 * CONS. AUX) / QTDE PEÇAS) / Rendimento)

Caso não tenho informado QT PEÇAS, fará o calculo do consumo conforme regras já existente.

- CONSUMO = CONS. AUX / RENDIMENTO CALCULADO

Quando o produto (tecido) for um retilíneo (Tipo de Produto = 3, 4 ou 6) e tiver "RENDIMENTO" e  quando o campo estiver liberado para ser preenchido pelo programa de "Alteração de tipos de Campos Requeridos" - oper_f006.
