VALIDA ESTQ. AUXILIARES NA EMISSÃO DA RECEITA
----
Parâmetro que define se deverá emitir receita de tingimento validando o saldo em estoque dos produtos auxiliares.

**Opções:**
   **(S)** - Será avaliado o saldo em estoque dos produtos auxiliares na emissão da receita e caso o saldo esteja zerado ou negativo, não será emitida a receita.

   **(N)** - Não será verificado o saldo em estoque dos produtos auxiliares na emissão da receita.

   Além do programa de emissão de receitas (pcpb_e116) a validação do saldo disponível em estoque para os produtos auxiliares será feita também nas telas de Edição da Receita (pcpb_f850) e Edição da Receita de Reprocesso (pcpb_f615).
