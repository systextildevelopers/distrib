REFER�NCIA

 
- Quando for usado o par�metro (1)-INCLUS�O, a consulta vai listar apenas as refer�ncias que forem informadas no cadastro tempor�rio.
   
- Quando for usado o parametro (2)-EXCE��O, a consulta vai ignorar as refer�ncias que forem informados no cadastro tempor�rio.
   
Para listar TODAS as refer�ncias, informe o par�metro (2)-EXCE��O e cadastre uma refer�ncia com XXXXXX.
