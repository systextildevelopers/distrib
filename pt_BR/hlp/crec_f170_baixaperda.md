BAIXA POR PERDAS

## As op��es s�o:
**0.** Baixa t�tulos normalmente (n�o baixa por perdas);

**1.** Baixa t�tulos por perda e gera o relat�rio listando os t�tulos baixados;

**2.** Somente gera o relat�rio listando os t�tulos que poder�o ser baixados;

> Este par�metro trabalha em consulto com os campos VALOR M�N. SALDO,  VALOR M�X. SALDO e DATA BAIXA POR PERDAS. 

> O valor da fatura deve estar entre os valores m�nimos e valores m�ximos e a �LTIMA parcela deve estar com data de vencimento (prorroga��o) menor a data informada no par�metro DATA BAIXA POR PERDAS.

# Ser�o baixadas todas as parcelas do t�tulo que se encaixarem nestas configura��es.
