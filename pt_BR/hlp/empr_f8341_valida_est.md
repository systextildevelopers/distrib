VALIDA ESTQ. CORANTES NA EMISSÃO DA RECEITA:
---
Parâmetro que define se deverá emitir receita de tingimento valindando o saldo em estoque de corantes.

**Opções:**

   **(S)**-  Será avaliado saldo dos corantes da receita em estoque, caso saldo esteja zerado ou negativo, não será emitido a receita.

   **(N)**- Não será verificado saldo dos corantes em estoque na emissão da receita.

   Além do programa de emissão de receitas (pcpb_e116) a validação do saldo disponível em estoque para os corantes será feita também nas telas de Edição da Receita (pcpb_f850) e Edição da Receita de Reprocesso (pcpb_f615).
