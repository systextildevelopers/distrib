CFOP - NATUREZA DE OPERA��O

   Quando for usado o par�metro (1) - INCLUS�O, o relat�rio vai listar APENAS as CFOPs solicitadas no cadastro tempor�rio.

   Quando for usado o par�metro (2) - EXCE��O, o relat�rio N�O IR� LISTAR as CFOPs solicitadas no cadastro tempor�rio.

   Para listar todas as CFOPs informe  (2) - EXCE��O e 'XXXXX' no cadastro tempor�rio.
