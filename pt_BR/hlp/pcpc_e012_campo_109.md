PEDIDO DE VENDAS


   - Quando for escolhida a op��o 1 - INCLUS�O - o relat�rio ir� considerar apenas os pedidos de venda que forem selecionadas no campo seguinte.

   - Quando for escolhida a op��o 2 - EXCE��O - o relat�rio n�o ir� listar os pedidos de venda que forem selecionadas no campo seguinte.

   Para considerar TODOS os pedidos de venda informe 2 - EXCE��O e deixe os campos seguintes com 999999.
