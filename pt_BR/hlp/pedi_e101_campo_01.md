PEDIDO DE VENDA


- Quando for usado o parâmetro ***1 - INCLUS�O***, o relat�rio vai listar apenas os pedidos que forem informados no cadastro tempor�rio.


- Quando for usado o  parâmetro ***2 - EXCE��O***, o relat�rio vai ignorar os pedidos que  forem informados no cadastro tempor�rio.


Para listar **TODOS** os pedidos, informe o parâmetro **2 - EXCE��O** e cadastre um pedidos com ***9999999***.
