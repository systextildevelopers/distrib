C�digo Especificador de Substitui��o Tribut�ria - CEST

*Este campo deve ser informado de acordo com a tabela do CEST X NCM (via zoom), onde o NCM do cadastro do produto � correspondente ao NCM que est� relacionado ao CEST.*

>**Exemplo 1:**

>CEST: 0109400

>NCM: 84137010 - Bomba de assist�ncia de dire��o hidr�ulica

>Caso o produto tiver a NCM acima, deve informar o c�digo do CEST 0109400.

>**Exemplo 2:**

>CEST: 0109400

>NCM: 8413 - Bombas

>Caso no produto tiver a NCM acima, deveria ser informado o c�digo do CEST correspondente � NCM, que inicia com 8413.

**IMPORTANTE: Para os casos em que houver mais de um CEST para o mesmo NCM, deve consultar a Contabilidade para indicar corretamente, pois este campo ir� para o arquivo do XML ao emitir uma nota fiscal.**
