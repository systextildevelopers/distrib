DATA FECHAMENTO
------------------------
Este campo � atualizado pelo relat�rio de **Rela��o de Servi�os Prestados (obrf_e640)**. Indica a data de fechamento das entradas da ordem de servi�o. Ser� gravada sempre a menor data de fechamento das entradas da ordem.