Valores das Classes de Valor:
-----------------------------------

Este programa � respons�vel por permitir o cadastro dos intervalos de valores das classes de valor. As classes de valor informadas no campo ***Classe de Valor*** devem estar com o seu c�digo e descri��o previamente cadastrados na tela **Cadastro de Classes de Valor (rcnb_f800)**.

As classes de valor cadastradas para um per�odo ser�o utilizadas no processo de **Gera��o de Tabela de Pre�o por Classe de Valor (rcnb_f805)** para agrupar os produtos que possu�rem o custo de tingimento nas mesmas faixas.

Esta tela possui as seguintes valida��es e funcionalidades quanto ao cadastro e atualiza��o de informa��es:

- Ser�o aceitos no campo *Situa��o* os valores **C** - Cadastrada e **A** - Aprovada. Ao inserir um novo registro, por padr�o o mesmo ser� preenchido com **C**. Ser� permitido possuir em um per�odo (m�s e ano) apenas uma sequ�ncia com a situa��o **A - Aprovada**, que ser� a sequ�ncia utilizada no processo de gera��o de tabela de pre�o.
-  O objetivo dos campos *Vlr. Inicial* e *Vlr. Final* � informar uma faixa onde o custo do tingimento de um produto ser� encaixado, para agrupar os produtos no processo de gera��o de tabela de pre�o. Portanto, n�o ser� poss�vel cadastrar valores com intercala��es entre as classes de um mesmo m�s, ano e sequ�ncia.