# Códigos de ajuste para o processo de apuração do DIFAL

* Código de ajuste de estorno ICMS: Configure este parâmetro para permitir o lançamento de estorno do valor de ICMS.

* Código de ajuste de estorno do valor de FCP: Configure este parâmetro para permitir o lançamento de estorno do valor de FCP.

Caso um dos campos acima, ou ambos, forem configurados, o processo de apuração de ICMS para vendas de consumidor final (DIFAL) irá utilizá-los para gerar um lançamento de estorno em notas fiscais de entrada por devolução, seja do valor de ICMS ou do valor de Fundo de Combate à Pobreza (FCP).
