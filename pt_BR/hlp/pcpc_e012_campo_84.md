ORDEM PRODU��O


   - Quando for escolhida a op��o 1 - INCLUS�O - o relat�rio ir� considerar apenas as ordens de produ��o que forem selecionadas no campo seguinte.

   - Quando for escolhida a op��o 2 - EXCE��O - o relat�rio n�o ir� listar as ordens de produ��o que forem selecionadas no campo seguinte.

   Para considerar TODAS as ordens de produ��o informe 2 - EXCE��O e deixe os campos seguintes com 999999999.
