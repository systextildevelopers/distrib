**OP��ES DE CONSULTA**
 
1 - Por item completo
2 - Por tamanho
3 - Por refer�ncia
4 - Por per�odo de produ��o
5 - Por produto/lote
6 - Por produto/dep�sito/lote

**Observa��es:**
  
   Para a op��o 4 - dever� ser informado o produto completo.
  
   Para as op��es 5 ou 6 - quando o produto consultado for utilizado na tecelagem atrav�s de   uma subprograma��o, o  sistema  ter�  condi��es  de mostrar a quantidade reservada  a  n�vel  de
produto, lote e dep�sito.

   Nos casos em que a empresa n�o trabalha  com o conceito de subprograma��o  de  ordens  de
tecelagem, o sistema ir� buscar a  quantidade reservada automaticamente, por�m neste  caso
n�o ser� poss�vel obter a reserva por lote e dep�sito.

   Tamb�m poder�o aparecer informa��es  para  o dep�sito zero. Estas informa��es dizem  respeito a produtos reservados na digita��o  da ordem, por�m sem  quantidade  dispon�vel  em estoque.
