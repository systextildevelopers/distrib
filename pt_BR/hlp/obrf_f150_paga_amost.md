PAGA % DE AMOSTRAGEM

Permite que para um tipo de servi�o seja pago um % adicional para ordens de amostra e para outros n�o.

- S: Efetua o pagamento considerando a % configurada no par�metro de empresa;

- N: N�o efetua o pagamento, considera 0%.
