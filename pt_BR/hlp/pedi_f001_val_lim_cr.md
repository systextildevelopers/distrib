VALIDADE LIMITE CRÉDITO
=======================

Informar a data de validade do limite de crédito. Irá fazer verificações com campo:
1. "NR DE DIAS PARA VALIDADE DO LIM. DE CRÉDITO" configurado em: "empr_f825 - parâmetros de empresa / Vendas / Por empresa / aba ++";
2. "NÚMERO DE DIAS" configurado em "pedi_f086 - Conceito Cliente".

Ex da verificação:  
> dt1 = Data atual: 01/01/2013  
> nr1 = Parametro (Configuração de Parâmetros / Vendas): 10  
> nr2 = Número de Dias (Conceito Cliente): 15  
> dt2 = Validade Limite Crédito: 15/01/2013  

Data Máxima = dt1 + nr2 = 16/01/2013

1. se dt2 < Data Máxima, a data informada é válida.
2. somente se a primeira situação foi válida, irá fazer a seguinte validação. 
se dt2 < dt1 + nr1; 15/01/2013 < 11/01/2013. Como não é válida, subirá mensagem informando.
