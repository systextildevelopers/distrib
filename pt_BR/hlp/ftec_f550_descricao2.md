Unidade de entrada:

As unidades de entrada permitidas s�o: 

- **UN** - Unidade do Produto (*Quando informado, a unidade esperada ser� a unidade de medida do produto*)
- **KG** - Quilos
- **MT** - Metros

Este par�metro ser� utilizado na entrada de produ��o de beneficiamento e tecelagem nos programas:

- **pcpb_f180** - Baixa das Ordens de Beneficiamento Via Balanca
- **pcpt_f080** - Baixa da Guia de Tecelagem
