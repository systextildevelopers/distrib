Par�metros para Percentual de Importados
-------------------------------------------------

   Neste programa s�o cadastrados os par�metros a serem utilizados para encontrar a "Origem do Produto" e o "Percentual de ICMS" que dever�o ser usados pelo c�lculo da Nota Fiscal de Sa�da.

   No faturamento e/ou na digita��o da Nota Fiscal de Sa�da, o sistema busca estes par�metros pelo C�digo da Empresa e pelo Produto, para encontrar a "Origem do Produto" e o "Percentual de ICMS".

   Caso n�o seja encontrado registro, o sistema busca estas informa��es da Classifica��o Fiscal do Produto (Origem) na Natureza de Opera��o (ICMS).

M�S/ANO (Per�odo): A busca por este campo ser� sempre utilizando a data de emiss�o da nota fiscal (m�s/ano), sendo que o sistema ir� buscar o registro do mais recente para o mais antigo.
