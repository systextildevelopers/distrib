Qtde maq.
-----------------------------------------------------------------------------------------

Quando for informada a m�quina completa o campo j� vir� preenchido com 1.

Quando for informado zero na m�quina, o campo vir� preenchido com as m�quinas ativas do grupo e subgrupo de m�quinas, permitindo ainda que o usu�rio altere para a quantidade de m�quinas que realmente podem produzir aquele produto.

Importante:
A soma do valor deste campo ser� apresentada no campo Total qtde maq., por�m o valor apresentado neste total ser� a soma da quantidade de m�quinas dos registros cujo subgrupo de m�quinas sejam diferentes.
