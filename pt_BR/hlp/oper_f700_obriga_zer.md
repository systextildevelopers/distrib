AGUARDAR ZERO ANTES DE NOVA LEITURA
===================================

Esta op��o fica dispon�vel quando a configura��o da porta serial usa _leitura
cont�nua com sa�da do campo autom�tica ap�s estabiliza��o_.

Se for ativada, ent�o, depois de sair do campo ap�s uma leitura bem sucedida,
o programa s� aceitar� uma nova leitura _depois de receber um valor zero na porta
serial_.

Esta op��o � �til quando, por exemplo, uma balan�a apresenta oscila��es que
geram pesos est�veis de tempos em tempos. Nessa situa��o a sa�da autom�tica do
campo pode fazer que a mesma pesagem seja gravada v�rias vezes com pesos
ligeiramente diferentes. Para evitar isso, obriga-se o programa a aguardar o
recebimento de um peso zero antes de aceitar uma nova pesagem.

Esta funcionalidade � importante em programas que gravam registros automaticamente,
isto �, sem que o operador precise fazer acesso ao formul�rio entre as pesagens.
Isso assegura que o peso zero seja recebido entre uma e outra. Se o programa n�o
gravar registros sucessivos automaticamente com sa�da autom�tica e estabilizada,
esta op��o n�o deve ser ativada, pois nesse caso ela n�o � necess�ria, e n�o
h� certeza de que o peso zero ser� recebido pelo programa ao aguardar leitura
da porta serial.

Algumas balan�as _n�o enviam o peso zero_ na porta serial. Nesse caso esta op��o
n�o deve ser ativada.
