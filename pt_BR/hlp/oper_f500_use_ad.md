Autenticar usu�rios do ERP com Active Directory
===============================================

Se os servidores onde o ERP estiver instalado tiverem acesso a um servidor de
Active Directory usado pela empresa para autenticar seus usu�rios na sua rede
local, esse mesmo Active Diretory pode ser usado para autenticar o acesso ao ERP.

Essa op��o deve ser avaliada pelos gestores de tecnologia da empresa usu�ria
do ERP.

Para ativar essa op��o, o endere�o do servidor e o dom�nio do Active Directory
devem ser definidos nas configura��es de sistema da empresa no ERP.

Quando essas informa��es estiverem definidas, o cadastro de usu�rios dessa
empresa oferecer� a op��o "Autenticar a senha do usu�rio pelo Active Directory".
Se essa op��o for ativada para determinado usu�rio, sua senha de acesso ao ERP
n�o ficar� no cadastro do usu�rio no ERP, e sim ser� validada pelo Active
Directory.

Portanto, essa op��o deve ser ativada primeiro no cadastro da empresa, e ent�o,
para cada usu�rio dessa empresa, haver� a op��o de autenticar seu acesso pelo
Active Directory configurado.

