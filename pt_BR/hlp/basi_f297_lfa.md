LFA � Comprimento do fio por volta do tear
--------------------------------------------------------------------------------------------------

Neste campo ser� informado o  Comprimento do fio utilizado por volta (LFA), que juntamente com o campo quantidade de fios, ser� utilizado para calcular o consumo do fio. 

Este � um exemplo do c�lculo para um tecido que utiliza 2 fios. Os fios utilizados, podem ter  t�tulos e unidade de medida do t�tulo diferente, informados na ficha t�cnica. A vari�vel fator contem o valor que ajusta o titulo para uma mesma unidade de medida a ser utilizado no calculo.

Consumo temp Fio 1 =  LFA * qtde Fios * 0.59 / (Titulo  * fator ) / 100
Consumo temp Fio 2 =  LFA * qtde Fios * 0.59 / (Titulo  * fator ) / 100

Consumo Fios = (Consumo temp Fio 1 + Consumo temp Fio 2)

Consumo Fio 1 = ( Consumo temp Fio 1 / Consumo Fios ) * ( 1 + ( perda / 100 ))
Consumo Fio 2 = ( Consumo temp Fio 2 / Consumo Fios ) * ( 1 + ( perda / 100 ))

