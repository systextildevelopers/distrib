ORIGEM CST
-------------------------------------------------

0 - Nacional, exceto as indicadas nos c�digos 3 a 5;
      
1 - Estrangeira - Importa��o direta, exceto a indicada no c�digo 6;
      
2 - Estrangeira - Adquirida no mercado interno, exceto a indicada no c�digo 7;
      
3 - Nacional, mercadoria ou bem com Conte�do de Importa��o superior a 40% e inferior ou igual a 70%;
      
4 - Nacional, cuja produ��o tenha sido feita em conformidade com os processos produtivos b�sicos de que tratam as legisla��es citadas nos Ajustes;
      
5 - Nacional, mercadoria ou bem com Conte�do de Importa��o inferior ou igual a 40%;
      
6 - Estrangeira - Importa��o direta, sem similar nacional, constante em lista da CAMEX;
      
7 - Estrangeira - Adquirida no mercado interno, sem similar nacional, constante em lista da CAMEX.

8 - Nacional, mercadoria ou bem com Conte�do de Importa��o superior a 70%;
