Tempo tecimento
-----------------------------------------------------------------------------

O tempo de tecimento � a quantidade de minutos necess�rios para produzir uma unidade do produto (1 Kg, 1 m, etc.).

O valor do campo tamb�m poder� ser calculado caso todos os campos da f�rmula forem preenchidos. A formula de c�lculo �:

Tempo tecimento = (((Voltas ou metros por rolo / Velocidade) / Peso padr�o) / Efici�ncia)

Importante:
A soma do valor deste campo ser� apresentada no campo Total qtde maq., por�m o valor apresentado neste total ser� a soma da quantidade de m�quinas dos registros cujo subgrupo de m�quinas sejam diferentes.
A m�dia ponderada do valor deste campo ser� apresentada no campo M�dia ponderada. O c�lculo ser� a m�dia ponderada do Tempo tecimento e Qtde maq. E a f�rmula de c�lculo �:

M�dia ponderada = ((soma Tempo tecimento * soma Qtde m�quina) / soma Qtde m�quina)
