Comprimento do ponto
--------------------------------

Ao informar os valores para os campos Polegadas e Pontos (n�mero de polegadas do ponto e o n�mero de pontos), o sistema ir� calcular o valor dos campos Pontos por polegada e Comprimento do ponto. Abaixo seguem as f�rmulas utilizadas para c�lculo:

Pontos por polegada = (Pontos / Polegadas)
Comprimento do ponto = (Polegadas / Pontos)

Ao informar os valores para os campos Pontos e Pontos por polegada (n�mero de pontos e n�mero de pontos por polegada), o sistema ir� calcular o valor dos campos Polegadas e Comprimento do ponto. Abaixo seguem as f�rmulas utilizadas para c�lculo:

Polegadas = (Pontos / Pontos por polegada)
Comprimento do ponto = (Polegadas / Pontos)

Ao informar os valores para os campos Polegadas ou Pontos e Comprimento do ponto (n�mero de polegadas do ponto ou n�mero de pontos e comprimento do ponto em polegadas), o sistema ir� calcular o valor dos campos Polegadas ou Pontos (aquele que n�o for informado) e Pontos por polegada. Abaixo seguem as f�rmulas utilizadas para c�lculo:

Polegadas = (Comprimento do ponto * Pontos)
Pontos = (Polegadas / Comprimento do ponto)
Pontos por polegada = (Pontos / Polegadas)
