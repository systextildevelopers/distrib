NR DE DIAS PARA VALIDADE DO LIM. DE CRÉDITO
===========================================

Esse parâmetro serve para calcular a data final da vigência do limite de crédito no programa pedi_f001 -  Cadastro de Informações Financeiras de Clientes.

Serve para indicar o numero máximo de dias que é permitido dar ao cliente de validade do limite de crédito e é utilizado quando não se tem definido o numero de dias do limite de crédito pela conceituação do cliente. 

- Se o número de dias informado nesse campo for menor que zero, não será permitido preencher data de validade maior ou igual à data corrente.
- Se o número de dias informado nesse campo for igual a zero, será permitido preencher qualquer data de validade sem fazer nenhuma consistencia.
- Se o número de dias informado nesse campo for maior que zero, será permitido preencher qualquer data de validade até a data corrente mais o número informado nesse campo.
