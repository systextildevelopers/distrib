Pedido Original
===============

O campo "Pedido Original" � utilizado para vincular um pedido atual (pedido filho) a um pedido principal (pedido pai), caso o pedido tenha sido desmembrado.

Funcionamento:

1. **V�nculo com Pedido Pai:** Preencha este campo com o n�mero do pedido pai quando o pedido atual for um pedido filho.
2. **Restri��es:**
    - Os itens do pedido filho devem estar no pedido pai.
    - A soma das quantidades dos pedidos filhos n�o pode exceder as quantidades do pedido pai.
3. **Valida��es:**
    - Pedidos pais n�o podem ser usados em notas fiscais de entrada.
    - Ap�s a entrega de itens, n�o ser� poss�vel alterar o v�nculo com o pedido pai.
