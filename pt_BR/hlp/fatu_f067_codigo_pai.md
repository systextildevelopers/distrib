### Instru��es para o C�digo do Pa�s (SISCOMEX)

O c�digo do pa�s a ser informado neste campo � sugerido pelo sistema conforme o cadastro do **"Cod fisc"** da tela **basi_f365** para cada pa�s, de acordo com a regra de valida��o da Receita no registro 1100.

O valor informado no campo dever� existir na tabela de Pa�ses do **SISCOMEX**, que corresponde ao segundo, terceiro e quarto d�gitos da tabela **BACEN**, a qual possui cinco caracteres.
