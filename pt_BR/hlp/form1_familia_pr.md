FAM�LIAS DE PRODUTO POR N�VEL

Par�metros para configura��o de fam�lia de produto para o Syst�xtil Plan.
A par�metriza��o efetuada nestes campos altera funcionalidades do programa inte_f440 - Exporta��o de Dados para o Syst�xtil Plan.

Quando a op��o parametrizada for 0 - Artigo de Produto, o Syst�xtil envia para a cadastro de fam�lias do Syst�xtil Plan o artigo de produto do Syst�xtil.
Quanto a op��o parametrizada for 1 - Divis�o de Fabrica��o, o Syst�xtil envia para o cadastro de fam�lias as divis�es de fabrica��o definidas na ficha t�cnica do produto confeccionado (ftec_f700).
A par�metriza��o deve ser efetuada para cada n�vel de produto.

Op��es dispon�veis por n�vel:

- N�VEL 1:
	(0) - Artigo de Produto
	(1) - Divis�o de Fabrica��o.

- N�VEL 2, 4, 7 Est� dispon�vel apenas a op��o:
	(0) - Artigo de Produto.