Ocorr�ncia do estorno

Informe neste campo a ocorr�ncia pela qual est� estornando o pacote da Ordem de Confec��o.

**Somente ser� permitido informar a ocorr�ncia do estorno atrav�s do programa pcpc_f046 (Estorno de Est�gio da Ordem de Produ��o (Pacote))**.
