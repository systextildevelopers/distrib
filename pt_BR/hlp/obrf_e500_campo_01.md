Romaneio de Retorno da Ordem de Servi�o de Confec��o
-------------------------
Este programa emite o relat�rio do retorno da ordem de servi�o. As informa��es deste programa ser�o obtidas das entradas da O.S. de confec��o e relacionamento da O.S. e O.P.

O layout desta tela deve ser informado no campo ***Rpt Romaneio Retorno O.S.:***, localizado na tela **Par�metros de Configura��o - Terceiros (empr_f016)**.