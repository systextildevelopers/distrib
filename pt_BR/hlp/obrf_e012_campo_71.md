MESES CONSIDERADOS NA LEITURA DAS ENTRADAS
-------------------------------------------------

Indica para o sistema o n�mero de meses a serem considerados no filtro para encontrar as notas fiscais de compra, sendo:

0- Somente o mesmo M�s que a data de emiss�o da Nota Fiscal de Sa�da;

1- At� um m�s anterior a data de emiss�o da nota fiscal de Sa�da;

2- At� dois meses anteriores a data de emiss�o da nota fiscal de Sa�da;

E assim por diante.

Ex:
Se o per�odo informado for de �05/2013�. 

Se for informado 0 meses, a data de transa��o da NFE dever� estar entre �01/05/2013� e �31/05/2013�.

Se for informado 1 m�s, a data de transa��o da NFE dever� estar entre �01/04/2013� e �31/05/2013�.

Se for informado 2 meses, a data de transa��o da NFE dever� estar entre �01/03/2013� e �31/05/2013�.
