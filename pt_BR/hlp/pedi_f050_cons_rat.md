CONSIDERA RATEIO NA BASE ICMS
=====

**As op��es s�o:**

- **1** - Recebe rateio de despesas/descontos.
- **2** - N�o recebe rateio de despesas/descontos.

Quando a natureza de opera��o do item da nota fiscal estiver parametrizado com 1, a base ICMS ser� considerado rateio, quando for 2 n�o ser� considerado.

Caso TODOS os itens da nota fiscal estiverem com natureza de opera��o com 2, o sistema ir� considerar o rateio na base ICMS dos itens.

Este par�metro n�o ter� efeito sobre notas de loja, notas de entrada e tamb�m sobre IPI para notas de entradas e sa�das.
