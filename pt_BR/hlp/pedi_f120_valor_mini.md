Valor m�nimo
============


> Armazena o **valor m�nimo** da condi��o de pagamento.


### Cat�logo

O valor especificado neste campo ir� ativar ou desativar a condi��o de pagamento no produto for�a de vendas cat�logo.
> **Ativar** - Caso o valor m�nimo seja **menor ou igual** o *valor total* do pedido, a condi��o estar� dispon�vel para sele��o.
> **Desativar** - Caso o valor m�nimo seja **maior** do que o valor total do pedido, a condi��o estar� indispon�vel para a sele��o.
