PROCESSO DE EMISS�O DE BOLETO COM REGISTRO
-------------------------------------------------
#Bancos suportados:

**001** - Brasil

**237** - Bradesco

**341** - Ita�

**041** - Banrisul

**356** - Real

**033** - Santander

**422** - Safra

**085** - CECRED

**756** - SICOOB

**748** - SICREDI

**104** - Caixa Econ�mica Federal

**399** - HSBC

#Pr�-requisitos para emiss�o do boleto com registro:

- **O titulo n�o pode estar com situa��o "2 - Cancelado";**

- **O tipo de titulo tem que estar configurado para envio de cobran�a;**

- **Al�m do banco suportado, o mesmo tem que estar com o atributo configurado para gera��o de boleto;**

- **Se o titulo estiver associado a uma NF-e, a mesma deve estar com situa��o "1 - Emitida" e status "100 - Autorizada" perante a receita;**

- **Caso o cadastro do cliente esteja habilitado a op��o de DDA(D�bito Direto Autorizado) n�o ser� gerado o boleto;**

#Para os t�tulos associados a notas fiscais n�o eletr�nicas, ser� gerado o boleto nas seguintes condi��es:

- **O titulo n�o pode estar com situa��o "2 - Cancelado";**

- **O tipo de titulo tem que estar configurado para envio de cobran�a;**
