Situa��o Fechamento
-------------------------
Este campo � atualizado pelo relat�rio de **Rela��o de Servi�os Prestados (obrf_e640)**. Indica a situa��o de fechamento da ocorr�ncia, que pode ser:
> - 0 - Pendente.
> - 1 - Marcado para pagamento / desconto.
> - 2 - Pago / descontado.