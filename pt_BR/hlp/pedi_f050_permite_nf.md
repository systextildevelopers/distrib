NATUREZA DE OPERACAO

###PERMITE NF MESMO CNPJ

Este campo indica se e permitido digitar notas fiscais para o mesmo cnpj do emitente.

- 1 - Permite.
- 2 - Nao permite.

> Obs: Este parametro somente e considerado nos seguintes programas:
  - ``obrf_f010`` - Entrada de Documentos Fiscais;
  - ``obrf_f020`` - Digitacao de Notas Fiscais de Saida;
  - ``obrf_f050`` - Digitacao de Notas Fiscais de Entrada com Emissao;
