PREPARA��O
------------------------------------------------

As op��es aceitas s�o:

**1.** As quantidades consumidas ser�o informadas nos est�gios de prepara��o.

**2.** As quantidades consumidas ser�o calculadas e retiradas do estoque no momento da entrada da produ��o da ordem de produ��o.

**3.** As quantidades consumidas ser�o calculadas e retiradas do estoque no momento da entrada da produ��o da ordem de produ��o. N�o faz a valida��o do lote de sa�da informado, ir� movimentar o consumo dos materiais com lote 000000.


OBS: As op��es 2 e 3, estar�o ativas apenas para as Ordens de Beneficiamento de Fios, Ordens de Fia��o e Ordens de Beneficiamento de Fibras, que n�o est�o associadas a uma ordem de servi�o que esteja relacionada a uma nota fiscal de remessa para industrializa��o 
------------------------------------------------
