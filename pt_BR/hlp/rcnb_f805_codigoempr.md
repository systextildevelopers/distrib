Gera��o de Tabela de Pre�o por Classe de Valor
--------------------------------------------------------
Este tela � respons�vel por gerar os itens de tabelas de pre�o a partir de informa��es do **'Painel de Simula��o de Ficha de Custos (rcnb_f300)'** e do **'Cadastro de Valores das Classes de Valor (rcnb_f801)'**.
As fichas de custos ser�o filtradas pelo c�digo de empresa, per�odo e produto informados. A classe de valor ser� filtrada apenas pelo per�odo informado.

Os produtos informados dever�o ser obrigatoriamente de n�vel  **2 - Tecidos Acabados** ou **7 - Fios**.

O primeiro campo do c�digo da tabela possui **AJUDA**, informando caracter�sticas espec�ficas da tabela a ser informada.

O campo **OP��O** ficar� habilitado apenas se for informado uma tabela que j� possua itens (Tabelas de Pre�os a N�vel de Grupo, Subgrupo, S�rie Cor e Item). Se for escolhida a op��o **1 -  Sobrepor os dados existentes** inicialmente todos os itens da tabela ser�o eliminados, e o processo ir� gerar os itens da tabela com as informa��es da ficha de custos e classe de valor. Caso for escolhida a op��o **2 - Complementar tabela** todos os itens j� existentes ser�o mantidos, e o processo ir� apenas incluir os novos registros.

Os itens gerados por esse processo ser�o armazenados na tabela de pre�o por c�digo de produto completo, ou seja, o pre�o ser� registrado na tela **'Tabela de Pre�os (a N�vel de Item) (pedi_f354)'**. Nesta tela, tamb�m ser� informado o c�digo da classe de valor utilizada para calcular o pre�o do item.

Na capa da **'Tabela de Pre�os (pedi_f350)'** ser� informado na parte inferior da tela qual o **m�s**, **ano** e **sequ�ncia** que foi utilizado para a gera��o dos itens da tabela de pre�o.