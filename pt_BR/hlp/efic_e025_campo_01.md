LISTAR PARADAS

Este par�metro tem a  finalidade de  filtrar as paradas que ser�o listadas da seguinte maneira:
   
   1 - Lista  paradas  cujo  os  motivos  est�o classificados para efici�ncia;
   2 - Lista paradas cujo os motivos n�o  est�o classificados para efici�ncia;
   9 - Lista todas as paradas.
