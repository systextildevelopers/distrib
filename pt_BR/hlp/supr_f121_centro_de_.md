
**Esta tela tem como fun��o definir, quando for recebido da nota fiscal deste pedido de compra, se seu item do pedido ter� rateio por centro de custo, conforme defini��o nesta tela quando do ato de cria��o do pedido de compra. Voc� pode ratear percentualmente a quantidade, ou informar a quantidade a ser rateada** - *um ou outro, nunca os dois.*

**Logo, ao dar entrada na nota fiscal, o sistema ir� ratear por centro de custo seguindo este crit�rio:**

- Onde informo o centro de custo e opto por informar percentual ou quantidade.

- Se informa um o outro trava e vice versa.

- Informando um o outro � calculado com base na qtde informada no item do pedido de compra.

- Ao tentar sair desta tela, o sistema tem que fechar 100% e o total de qtde distribu�do deve bater com a quantidade do item do pedido.

- Se o pedido n�o esta emitido posso excluir esta informa��o.
