Situa��o Fechamento
-------------------------
Este campo � atualizado pelo relat�rio de **Rela��o de Servi�os Prestados (obrf_e640)**. Indica a situa��o de fechamento das entradas da ordem de servi�o que pode ser.
> - 0 - Pendente.
> - 1 - Marcado para pagamento / desconto.
> - 2 - Pago / descontado.

Na execu��o do relat�rio **Rela��o de Servi�os Prestados (obrf_e640)**, ao solicitar para marcar as entradas e ocorr�ncias para serem pagas (confirma��o de pagamento), � atualizada a situa��o da ordem de servi�o para ***1 - Pago Parcial*** ou ***2 - Pago Total.*** Neste momento, caso a situa��o da Ordem de Servi�o seja recebimento parcial (3), atualiza a situa��o do fechamento para 1 (Pago Parcial), caso a situa��o da ordem seja recebimento total (4), atualiza a situa��o do fechamento para 2 (Pago Total).