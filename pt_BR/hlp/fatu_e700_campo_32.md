CFOP - NATUREZA DE OPERA��O DE SA�DA

 Quando for usado o par�metro (1) - INCLUS�O, o relat�rio vai listar APENAS as CFOP's de sa�da solicitadas no cadastro tempor�rio.

 Quando for usado o par�metro (2) - EXCE��O, o relat�rio N�O IR� LISTAR as CFOP's de sa�da solicitadas no cadastro tempor�rio.

 Para listar todas as CFOP's de sa�da informe (2) - EXCE��O e 'XXXXX' no cadastro tempor�rio.
