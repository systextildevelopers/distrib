Ordem de serviço de conserto - Envio
---
Escolha uma das opções:

**0** *–* Antes de iniciar os procedimentos de envio, as peças devem ser lançadas como produção de conserto no estágio de identificação do conserto.

**1** *–* Antes de iniciar os procedimentos de envio, as peças devem estar como em produção no estágio de identificação do conserto.
