VENDA DE ETIQUETAS
----------------------
Este par�metro identifica, se a empresa utilizar� para o cadastro de itens do pedido de venda, a tela de digita��o de Pedido de vendas para etiquetas.

0 - N�o utilizar� tela de digita��o de pedido de venda para etiquetas (n�o necessita de nenhum plugin).
1 - Utilizar� a tela de digita��o de pedido de venda para etiquetas padr�o 1 (necessita plugin do etiq_f123).
2 - Utilizar� a tela de digita��o de pedido de venda para etiquetas padr�o 2 (necessita plugin do etiq_f001).
