Permitir criar volumes na coleta de pe�as para faturamento
==========================================================

Define se os programas de coleta de pe�as para o faturamento podem criar novos
volumes se for informado o n�mero de volume zero.

Esta regra � usada no programa `inte_p010` - Coleta de pe�as referente a sugest�o.
