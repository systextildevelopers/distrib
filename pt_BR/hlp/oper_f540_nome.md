Credenciais do usu�rio para envio de e-mail
===========================================

Estes dados s�o necess�rios para que o usu�rio envie e-mail atrav�s do Syst�xtil ERP em seu nome.

> Estes dados n�o s�o necess�rios se a empresa do usu�rio possuir cadastradas no ERP as credenciais para envio de e-mail _**em nome da empresa**_. Se n�o houver as credenciais do usu�rio, ser�o usadas as credenciais da empresa.
>
> Neste caso, se houver interesse em que o nome do usu�rio esteja presente no e-mail enviado, ent�o o campo **Nome** deve ser preenchido.
>
> Por causa desta regra, os campos n�o s�o obrigat�rios.

**Aten��o:** Para registrar ou alterar a senha, informe-a no campo respectivo. Se o campo estiver vazio, a senha n�o ser� alterada (ou apagada). Se n�o quiser alterar a senha, deixe o campo vazio.
