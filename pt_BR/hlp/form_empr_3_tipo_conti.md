TIPO DE CONTING�NCIA PARA NF-e
==============================

Configure aqui o tipo de conting�ncia em vigor para o ERP.

Tipos 2 (FS) e 5 (FS-DA)
------------------------

Nestes modos, o ERP trabalha com tipo de envio normal, e s� usar� estes modos de conting�ncia
quando a nota fiscal for emitida em conting�ncia atrav�s da tela de manuten��o de nota fiscal
eletr�nica, depois de ter sido enviada sem sucesso pelo envio normal.

Para emitir a nota fiscal em conting�ncia � necess�rio ter preenchida a justificativa para a
entrada em conting�ncia. A data � preenchida automaticamente neste momento.

Tipo 3 (SVC)
------------

Neste modo, o ERP trabalha em modo de conting�ncia online, enviando as notas fiscais para a Sefaz
Virtual de Conting�ncia correspondente � UF do emitente. O tipo de envio � definido automaticamente
como **6** (SVC-AN) ou **7** (SVC-RS).

Neste modo, � obrigat�rio preencher a justificativa para a entrada em conting�ncia. A data �
preenchida automaticamente neste momento.

Quando terminar o modo de conting�ncia SVC � necess�rio configurar um dos outros tipos de
conting�ncia para retornar o ERP ao tipo de envio normal.

A configura��o mais comum para o tipo de conting�ncia � **5**.
