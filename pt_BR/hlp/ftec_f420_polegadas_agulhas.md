Polegadas
--------------

Ao informar os valores para os campos Polegadas e Agulhas 
(n�mero de polegadas da agulha e o n�mero de agulhas), o sistema 
ir� calcular o valor do campo Agulhas por polegada. Abaixo seguem 
as f�rmulas utilizadas para c�lculo:

Agulhas por polegada = (Agulhas / Polegadas)
