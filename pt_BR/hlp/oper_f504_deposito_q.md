Depósito de Quimicos por Usuário
----
O depósito informado neste campo será utilizado para consumo de produtos químicos no beneficiamento, logo, se for preenchido com um código diferente de zero será considerado ao invés do campo depósito de químicos dos parametros de empresa (Cadastro de Empresas / Configurações de Parâmetros > Comercial > Estoque > Global > Depósito de Químicos).
