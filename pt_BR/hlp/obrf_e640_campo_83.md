Marcar/Desmarcar Pagto.
-------------------------
Essa op��o ir� atualizar a situa��o do fechamento das entradas das ordens de servi�o e ocorr�ncias dos terceiros, permitindo realizar o pagamento/desconto das mesmas. As op��es s�o:

> - 0 � Somente listar (default)
> - 1 � Marcar para pagamento/desconto
> - 2 � Desmarcar pagamento/desconto
> - 3 � Efetuar pagamento/desconto