create or replace package body ST_PCK_CONTAS_A_PAGAR as

    function baixar_titulo(p_nr_duplicata     in number,     p_parcela          in varchar2, p_tipo_titulo     in number,
                           p_cgc_9            in number,     p_cgc_4            in number,   p_cgc_2           in number,
                           p_transacao        in out number, p_cod_portador     in number,   p_nr_conta_porta  in number,
                           p_cod_contab_deb   in out number, p_empresa          in number,   p_centro_custo    in number,
                           p_pago_adiantam    in number,     p_data_baixa       in date,     p_valor_pago      in number,
                           p_cod_historico    in number,     p_numero_documento in number,   p_data_pgmt       in date,
                           p_sequencia_pagto  in number,     p_prg_gerador      in varchar2, p_usuario         in varchar2) return varchar2
    as

    v_des_erro             varchar2(1000);
    v_origem               cont_520.origem%type;
    v_cod_contab_for_cre   supr_010.codigo_contabil%type;
    v_cod_contab_cre       cpag_020.codigo_contabil%type;
    v_lancamento_contabil  cont_600.numero_lanc%type;
    v_gera_contab_conta    cpag_020.gera_contabil%type;
    v_contabiliza_trans    estq_005.atualiza_contabi%type;
    v_tipo_contab_deb      cont_550.tipo_contabil%type;
    v_compl_historico      cont_600.compl_histor1%type;
    v_emitente_titulo      cpag_010.emitente_titulo%type;
    v_sinal_titulo         cont_010.sinal_titulo%type;

    begin
      v_lancamento_contabil := 0;
      v_origem := 6;--Fixo para baixa_titulo

      begin
          select supr_010.codigo_contabil
          into v_cod_contab_for_cre
          from supr_010
          where supr_010.fornecedor9 = p_cgc_9
          and   supr_010.fornecedor4 = p_cgc_4
          and   supr_010.fornecedor2 = p_cgc_2;
      exception
      when no_data_found then
          v_cod_contab_for_cre := 0;
      end;

      begin
          select atualiza_contabi
          INTO v_contabiliza_trans
          from estq_005
          where codigo_transacao = p_transacao;

          select  codigo_contabil, gera_contabil
          INTO v_cod_contab_cre, v_gera_contab_conta
          from cpag_020
          where portador = p_cod_portador
          and   conta_corrente = p_nr_conta_porta;
      EXCEPTION WHEN
      others then
          v_contabiliza_trans := 0;
          v_cod_contab_cre := 0;
          v_gera_contab_conta := 0;
      end;

      if v_contabiliza_trans <> 2 and v_gera_contab_conta = 1 then
          if v_contabiliza_trans = 3 then
              v_tipo_contab_deb := 4;
          else
              v_tipo_contab_deb := 2;
              p_cod_contab_deb := v_cod_contab_for_cre;
          end if;

          select cpag_010.emitente_titulo
          into v_emitente_titulo
          from cpag_010
          where cpag_010.nr_duplicata = p_nr_duplicata
            and cpag_010.parcela = p_parcela
            and cpag_010.cgc_9 = p_cgc_9
            and cpag_010.cgc_4 = p_cgc_4
            and cpag_010.cgc_2 = p_cgc_2
            and cpag_010.tipo_titulo = p_tipo_titulo;

          v_compl_historico := lpad(p_nr_duplicata, 9, 0) || '/' || p_parcela || ' - ' || v_emitente_titulo;


         BEGIN
             SELECT CASE
                      WHEN sinal_titulo IN (2, 3) THEN 3
                      ELSE 1
                    END
               INTO v_sinal_titulo
               FROM cont_010
              WHERE codigo_historico = p_cod_historico;
          EXCEPTION
             WHEN OTHERS THEN
                v_des_erro := 'Erro ao ler sinal_titulo';
          END;


          inter_pr_contabilizacao_prg_03(p_empresa,          p_centro_custo,     p_cgc_9,   p_cgc_4,    p_cgc_2,
                                  p_transacao,            p_cod_historico,    p_nr_conta_porta,      p_cod_contab_deb,
                                  v_cod_contab_cre,       p_nr_duplicata,     p_data_pgmt,           p_valor_pago,
                                  v_origem,               v_tipo_contab_deb,  20,                    p_prg_gerador,
                                  p_usuario,              v_compl_historico,  v_sinal_titulo,         p_tipo_titulo,
                                  v_lancamento_contabil, v_des_erro);

          if v_des_erro is not null then
              return v_des_erro;
          end if;
      end if;

      begin
          insert into cpag_015 (
              dupl_for_nrduppag,  dupl_for_no_parc,   dupl_for_for_cli9,
              dupl_for_for_cli4,  dupl_for_for_cli2,  dupl_for_tipo_tit,
              sequencia_pagto,    data_pagamento,     numero_documento,
              codigo_historico,   valor_pago,         cd_porta_cod_port,
              cd_porta_nr_conta,  codigo_transacao,   data_baixa,
              num_contabil,       pago_adiantam
          ) VALUES (
              p_nr_duplicata,     p_parcela,          p_cgc_9,
              p_cgc_4,            p_cgc_2,            p_tipo_titulo,
              p_sequencia_pagto,  p_data_pgmt,        p_numero_documento,
              p_cod_historico,    p_valor_pago,       p_cod_portador,
              p_nr_conta_porta,   p_transacao,        p_data_baixa,
              v_lancamento_contabil,  p_pago_adiantam
          );
      EXCEPTION WHEN
      others then
          v_des_erro := 'Erro ao baixar titulo no ERP!. Nr. Dupli.: ' || p_nr_duplicata || ' ERRO: ' || SQLERRM;
      end;

      return v_des_erro;

    end baixar_titulo;

    procedure criar_titulo(
        p_nr_duplicata IN number,          p_parcela IN varchar2,             p_cgc9 IN number,
        p_cgc4 IN number,                  p_cgc2 IN number,                  p_tipo_titulo IN number,
        p_codigo_empresa IN number,        p_cod_end_cobranca IN number,      p_emitente_titulo IN varchar2,
        p_documento IN number,             p_serie IN varchar2,               p_data_contrato IN date,
        p_data_vencimento IN date,         p_data_transacao IN date,          p_codigo_depto IN number,
        p_cod_portador IN number,          p_codigo_historico IN number,      p_codigo_transacao IN OUT number,
        p_posicao_titulo IN number,        p_codigo_contabil IN number,       p_tipo_pagamento IN number,
        p_moeda_titulo IN number,          p_origem_debito IN varchar2,       p_previsao IN number,
        p_valor_parcela IN number,         p_valor_parcela_moeda IN number,   p_base_irrf_moeda IN varchar2,
        --INICIO IMPOSTO
        p_base_irrf IN varchar2,           p_cod_ret_irrf IN varchar2,        p_aliq_irrf IN varchar2,
        p_valor_irrf_moeda IN number,      p_valor_irrf IN number,            p_base_iss IN number,
        p_cod_ret_iss IN number,           p_aliq_iss IN number,              p_valor_iss IN number,
        p_base_inss IN number,             p_cod_ret_inss IN number,          p_aliq_inss IN number,
        p_valor_inss_imp IN number,        p_base_pis IN number,              p_cod_ret_pis IN number,
        p_aliq_pis IN number,              p_valor_pis_imp IN number,         p_base_cofins IN number,
        p_cod_ret_cofins IN number,        p_aliq_cofins IN number,           p_valor_cofins_imp IN number,
        p_base_csl IN number,              p_cod_ret_csl IN number,           p_aliq_csl IN number,
        p_valor_csl_imp IN number,         p_base_csrf IN number,             p_cod_ret_csrf IN number,
        p_aliq_csrf IN number,             p_valor_csrf_imp IN number,
        --FIM IMPOSTO
        p_codigo_barras IN varchar2,       p_projeto IN number,               p_subprojeto IN number,
        p_servico IN number,               p_nr_processo_export IN number,    p_nr_processo_import IN number,
        p_cod_cancelamento IN number,      p_data_canc_tit IN varchar2,       p_num_lcmt IN OUT number,
        p_usuario_logado IN varchar2,      P_CGC9_FAVORECIDO IN NUMBER,       P_CGC4_FAVORECIDO IN NUMBER,
        P_CGC2_FAVORECIDO IN NUMBER,       P_BANCO_SISPAG    IN NUMBER,       P_AGENCIA_SISPAG  IN NUMBER,
        P_DIG_AGE_SISPAG  IN NUMBER,       P_CONTA_SISPAG    IN NUMBER,       P_DIG_CTA_SISPAG2 IN VARCHAR2,
        p_num_importacao IN varchar2,      p_prg_gerador     IN VARCHAR2,     p_usuario         IN VARCHAR2,
        p_success IN OUT varchar2,         p_des_erro        OUT varchar2)
    as

    v_cod_contab_for_cre    supr_010.codigo_contabil%type;
    v_origem                cont_520.origem%type;
    v_contabiliza_trans     estq_005.atualiza_contabi%type;
    v_tipo_contab_deb       cont_550.tipo_contabil%type;
    v_tipo_contab_cre       cont_550.tipo_contabil%type;
    p_compl_historico       cont_600.compl_histor1%type;
    v_mensagem_retorno      varchar2(4000);
    v_has_error             boolean;

    begin

      st_pck_contas_a_pagar.criar_titulo_juros(
            p_nr_duplicata,          p_parcela,             p_cgc9,
            p_cgc4,                  p_cgc2,                  p_tipo_titulo,
            p_codigo_empresa,        p_cod_end_cobranca,      p_emitente_titulo,
            p_documento,             p_serie,               p_data_contrato,
            p_data_vencimento,         p_data_transacao,          p_codigo_depto,
            p_cod_portador,          p_codigo_historico,      p_codigo_transacao,
            p_posicao_titulo,        p_codigo_contabil,       p_tipo_pagamento,
            p_moeda_titulo,          p_origem_debito,       p_previsao,
            p_valor_parcela,         p_valor_parcela_moeda,   p_base_irrf_moeda,
            --INICIO IMPOSTO
            p_base_irrf,           p_cod_ret_irrf,        p_aliq_irrf,
            p_valor_irrf_moeda,      p_valor_irrf,            p_base_iss,
            p_cod_ret_iss,           p_aliq_iss,              p_valor_iss,
            p_base_inss,             p_cod_ret_inss,          p_aliq_inss,
            p_valor_inss_imp,        p_base_pis,              p_cod_ret_pis,
            p_aliq_pis,              p_valor_pis_imp,         p_base_cofins,
            p_cod_ret_cofins,        p_aliq_cofins,           p_valor_cofins_imp,
            p_base_csl,              p_cod_ret_csl,           p_aliq_csl,
            p_valor_csl_imp,         p_base_csrf,             p_cod_ret_csrf,
            p_aliq_csrf,             p_valor_csrf_imp,
            -- FIM IMPOSTO
            p_codigo_barras,       p_projeto,               p_subprojeto,
            p_servico,               p_nr_processo_export,    p_nr_processo_import,
            p_cod_cancelamento,      p_data_canc_tit,       p_num_lcmt,
            p_usuario_logado,        P_CGC9_FAVORECIDO,       P_CGC4_FAVORECIDO,
            P_CGC2_FAVORECIDO,       P_BANCO_SISPAG,       P_AGENCIA_SISPAG,
            P_DIG_AGE_SISPAG ,       P_CONTA_SISPAG,       P_DIG_CTA_SISPAG2,
            p_num_importacao,        p_prg_gerador,     p_usuario,
            0,                       p_success,         p_des_erro);
    end criar_titulo;

   procedure criar_titulo_juros(
        p_nr_duplicata IN number,          p_parcela IN varchar2,             p_cgc9 IN number,
        p_cgc4 IN number,                  p_cgc2 IN number,                  p_tipo_titulo IN number,
        p_codigo_empresa IN number,        p_cod_end_cobranca IN number,      p_emitente_titulo IN varchar2,
        p_documento IN number,             p_serie IN varchar2,               p_data_contrato IN date,
        p_data_vencimento IN date,         p_data_transacao IN date,          p_codigo_depto IN number,
        p_cod_portador IN number,          p_codigo_historico IN number,      p_codigo_transacao IN OUT number,
        p_posicao_titulo IN number,        p_codigo_contabil IN number,       p_tipo_pagamento IN number,
        p_moeda_titulo IN number,          p_origem_debito IN varchar2,       p_previsao IN number,
        p_valor_parcela IN number,         p_valor_parcela_moeda IN number,   p_base_irrf_moeda IN varchar2,
        --INICIO IMPOSTO
        p_base_irrf IN varchar2,           p_cod_ret_irrf IN varchar2,        p_aliq_irrf IN varchar2,
        p_valor_irrf_moeda IN number,      p_valor_irrf IN number,            p_base_iss IN number,
        p_cod_ret_iss IN number,           p_aliq_iss IN number,              p_valor_iss IN number,
        p_base_inss IN number,             p_cod_ret_inss IN number,          p_aliq_inss IN number,
        p_valor_inss_imp IN number,        p_base_pis IN number,              p_cod_ret_pis IN number,
        p_aliq_pis IN number,              p_valor_pis_imp IN number,         p_base_cofins IN number,
        p_cod_ret_cofins IN number,        p_aliq_cofins IN number,           p_valor_cofins_imp IN number,
        p_base_csl IN number,              p_cod_ret_csl IN number,           p_aliq_csl IN number,
        p_valor_csl_imp IN number,         p_base_csrf IN number,             p_cod_ret_csrf IN number,
        p_aliq_csrf IN number,             p_valor_csrf_imp IN number,
        --FIM IMPOSTO
        p_codigo_barras IN varchar2,       p_projeto IN number,               p_subprojeto IN number,
        p_servico IN number,               p_nr_processo_export IN number,    p_nr_processo_import IN number,
        p_cod_cancelamento IN number,      p_data_canc_tit IN varchar2,       p_num_lcmt IN OUT number,
        p_usuario_logado IN varchar2,      P_CGC9_FAVORECIDO IN NUMBER,       P_CGC4_FAVORECIDO IN NUMBER,
        P_CGC2_FAVORECIDO IN NUMBER,       P_BANCO_SISPAG    IN NUMBER,       P_AGENCIA_SISPAG  IN NUMBER,
        P_DIG_AGE_SISPAG  IN NUMBER,       P_CONTA_SISPAG    IN NUMBER,       P_DIG_CTA_SISPAG2 IN VARCHAR2,
        p_num_importacao IN varchar2,      p_prg_gerador     IN VARCHAR2,     p_usuario         IN VARCHAR2,
        p_juros in number, p_success IN OUT varchar2,         p_des_erro        OUT varchar2)
    as

    v_cod_contab_for_cre    supr_010.codigo_contabil%type;
    v_origem                cont_520.origem%type;
    v_contabiliza_trans     estq_005.atualiza_contabi%type;
    v_tipo_contab_deb       cont_550.tipo_contabil%type;
    v_tipo_contab_cre       cont_550.tipo_contabil%type;
    p_compl_historico       cont_600.compl_histor1%type;
    v_mensagem_retorno      varchar2(4000);
    v_has_error             boolean;

    begin

      st_pck_contas_a_pagar.criar_titulo_cpag_010(
            p_nr_duplicata,          p_parcela,               p_cgc9,
            p_cgc4,                  p_cgc2,                  p_tipo_titulo,
            p_codigo_empresa,        p_cod_end_cobranca,      p_emitente_titulo,
            p_documento,             p_serie,                 p_data_contrato,
            p_data_vencimento,       p_data_transacao,        p_codigo_depto,
            p_cod_portador,          p_codigo_historico,      p_codigo_transacao,
            p_posicao_titulo,        p_codigo_contabil,       p_tipo_pagamento,
            p_moeda_titulo,          p_origem_debito,         p_previsao,
            p_valor_parcela,         p_valor_parcela_moeda,   p_base_irrf_moeda,
            --INICIO IMPOSTO
            p_base_irrf,             p_cod_ret_irrf,          p_aliq_irrf,
            p_valor_irrf_moeda,      p_valor_irrf,            p_base_iss,
            p_cod_ret_iss,           p_aliq_iss,              p_valor_iss,
            p_base_inss,             p_cod_ret_inss,          p_aliq_inss,
            p_valor_inss_imp,        p_base_pis,              p_cod_ret_pis,
            p_aliq_pis,              p_valor_pis_imp,         p_base_cofins,
            p_cod_ret_cofins,        p_aliq_cofins,           p_valor_cofins_imp,
            p_base_csl,              p_cod_ret_csl,           p_aliq_csl,
            p_valor_csl_imp,         p_base_csrf,             p_cod_ret_csrf,
            p_aliq_csrf,             p_valor_csrf_imp,
            -- FIM IMPOSTO
            p_codigo_barras,         p_projeto,               p_subprojeto,
            p_servico,               p_nr_processo_export,    p_nr_processo_import,
            p_cod_cancelamento,      p_data_canc_tit,         p_num_lcmt,
            p_usuario_logado,        P_CGC9_FAVORECIDO,       P_CGC4_FAVORECIDO,
            P_CGC2_FAVORECIDO,       P_BANCO_SISPAG,          P_AGENCIA_SISPAG,
            P_DIG_AGE_SISPAG ,       P_CONTA_SISPAG,          P_DIG_CTA_SISPAG2,
            p_num_importacao,        p_prg_gerador,           p_usuario,
            p_juros,                 0,                       p_success,
            p_des_erro);
    end criar_titulo_juros;



    procedure criar_titulo_cpag_010(
        p_nr_duplicata IN number,          p_parcela IN varchar2,             p_cgc9 IN number,
        p_cgc4 IN number,                  p_cgc2 IN number,                  p_tipo_titulo IN number,
        p_codigo_empresa IN number,        p_cod_end_cobranca IN number,      p_emitente_titulo IN varchar2,
        p_documento IN number,             p_serie IN varchar2,               p_data_contrato IN date,
        p_data_vencimento IN date,         p_data_transacao IN date,          p_codigo_depto IN number,
        p_cod_portador IN number,          p_codigo_historico IN number,      p_codigo_transacao IN OUT number,
        p_posicao_titulo IN number,        p_codigo_contabil IN number,       p_tipo_pagamento IN number,
        p_moeda_titulo IN number,          p_origem_debito IN varchar2,       p_previsao IN number,
        p_valor_parcela IN number,         p_valor_parcela_moeda IN number,   p_base_irrf_moeda IN varchar2,
        --INICIO IMPOSTO
        p_base_irrf IN varchar2,           p_cod_ret_irrf IN varchar2,        p_aliq_irrf IN varchar2,
        p_valor_irrf_moeda IN number,      p_valor_irrf IN number,            p_base_iss IN number,
        p_cod_ret_iss IN number,           p_aliq_iss IN number,              p_valor_iss IN number,
        p_base_inss IN number,             p_cod_ret_inss IN number,          p_aliq_inss IN number,
        p_valor_inss_imp IN number,        p_base_pis IN number,              p_cod_ret_pis IN number,
        p_aliq_pis IN number,              p_valor_pis_imp IN number,         p_base_cofins IN number,
        p_cod_ret_cofins IN number,        p_aliq_cofins IN number,           p_valor_cofins_imp IN number,
        p_base_csl IN number,              p_cod_ret_csl IN number,           p_aliq_csl IN number,
        p_valor_csl_imp IN number,         p_base_csrf IN number,             p_cod_ret_csrf IN number,
        p_aliq_csrf IN number,             p_valor_csrf_imp IN number,
        --FIM IMPOSTO
        p_codigo_barras IN varchar2,       p_projeto IN number,               p_subprojeto IN number,
        p_servico IN number,               p_nr_processo_export IN number,    p_nr_processo_import IN number,
        p_cod_cancelamento IN number,      p_data_canc_tit IN varchar2,       p_num_lcmt IN OUT number,
        p_usuario_logado IN varchar2,      P_CGC9_FAVORECIDO IN NUMBER,       P_CGC4_FAVORECIDO IN NUMBER,
        P_CGC2_FAVORECIDO IN NUMBER,       P_BANCO_SISPAG    IN NUMBER,       P_AGENCIA_SISPAG  IN NUMBER,
        P_DIG_AGE_SISPAG  IN NUMBER,       P_CONTA_SISPAG    IN NUMBER,       P_DIG_CTA_SISPAG2 IN VARCHAR2,
        p_num_importacao IN varchar2,      p_prg_gerador     IN VARCHAR2,     p_usuario         IN VARCHAR2,
        p_juros in number,                 p_id_titulo_cpag_narwal IN NUMBER, p_success IN OUT varchar2,
        p_des_erro        OUT varchar2)
    as

    v_cod_contab_for_cre    supr_010.codigo_contabil%type;
    v_origem                cont_520.origem%type;
    v_contabiliza_trans     estq_005.atualiza_contabi%type;
    v_tipo_contab_deb       cont_550.tipo_contabil%type;
    v_tipo_contab_cre       cont_550.tipo_contabil%type;
    p_compl_historico       cont_600.compl_histor1%type;
    v_mensagem_retorno      varchar2(4000);
    v_has_error             boolean;
    v_cod_transacao_jur     number;
    v_atualiza_contab       estq_005.atualiza_contabi%type;
    begin

      v_origem := 5;--Fixo para contas a pagar

      begin
          select supr_010.codigo_contabil
          into v_cod_contab_for_cre
          from supr_010
          where supr_010.fornecedor9 = p_cgc9
          and   supr_010.fornecedor4 = p_cgc4
          and   supr_010.fornecedor2 = p_cgc2;
      exception
      when no_data_found then
          v_cod_contab_for_cre := 0;
      end;

      begin
          select atualiza_contabi
          INTO v_contabiliza_trans
          from estq_005
          where codigo_transacao = p_codigo_transacao;
      EXCEPTION WHEN
      others then
          v_contabiliza_trans := 0;
      end;

      if v_contabiliza_trans <> 2 then
         v_tipo_contab_deb := 4;
         v_tipo_contab_cre := 2;

         p_compl_historico := lpad(p_nr_duplicata, 9, 0) || '/' || p_parcela || ' - ' || p_emitente_titulo;
         inter_pr_contabilizacao_prg_03(p_codigo_empresa,   p_codigo_depto,        p_cgc9,                p_cgc4,    p_cgc2,
                                 p_codigo_transacao,      p_codigo_historico,   0,                     p_codigo_contabil,
                                 v_cod_contab_for_cre,   p_nr_duplicata,        p_data_transacao,      p_valor_parcela-p_juros,
                                 v_origem,               v_tipo_contab_deb,     v_tipo_contab_cre,     p_prg_gerador,
                                 p_usuario,              p_compl_historico,     p_tipo_titulo, 1, p_num_lcmt,            v_mensagem_retorno);
         if v_mensagem_retorno is not null then
             p_des_erro := v_mensagem_retorno;
             p_success := 'N';
         end if;



        if p_juros > 0.00 then
            select transacao_jur into v_cod_transacao_jur
            from fndc_006
            where cod_empresa = p_codigo_empresa;


            select ATUALIZA_CONTABI into v_atualiza_contab
            from estq_005
            where codigo_transacao = v_cod_transacao_jur;

            if v_atualiza_contab = 1 then
                inter_pr_contabilizacao_prg_03(p_codigo_empresa,       p_codigo_depto,         p_cgc9,                 p_cgc4,    p_cgc2,
                                            v_cod_transacao_jur,     p_codigo_historico,     0,                     p_codigo_contabil,
                                            v_cod_contab_for_cre,   p_nr_duplicata,         p_data_transacao,       p_juros,
                                            v_origem,               v_tipo_contab_deb,      v_tipo_contab_cre,      p_prg_gerador,
                                            p_usuario,              p_compl_historico,      p_tipo_titulo,          1,    p_num_lcmt,             v_mensagem_retorno);
                if v_mensagem_retorno is not null then
                    p_des_erro := v_mensagem_retorno;
                    p_success := 'N';
                end if;
            end if;
        end if;

      end if;

    v_has_error := false;


      inter_pr_insere_cpag_010_recur(
              p_nr_duplicata,          p_parcela,                 p_cgc9,
              p_cgc4,                  p_cgc2,                    p_tipo_titulo,
              p_codigo_empresa,        p_cod_end_cobranca,        p_emitente_titulo,
              p_documento,             p_serie,                   p_data_contrato,
              p_data_vencimento,       p_data_transacao,          p_codigo_depto,
              p_cod_portador,          p_codigo_historico,        p_codigo_transacao,
              p_posicao_titulo,        p_codigo_contabil,         p_tipo_pagamento,
              p_moeda_titulo,          p_origem_debito,           p_previsao,
              p_valor_parcela,         p_valor_parcela_moeda,     p_base_irrf_moeda,
              --INICIO IMPOSTO
              p_base_irrf,             p_cod_ret_irrf,            p_aliq_irrf,
              p_valor_irrf_moeda,      p_valor_irrf,              p_base_iss,
              p_cod_ret_iss,           p_aliq_iss,                p_valor_iss,
              p_base_inss,             p_cod_ret_inss,            p_aliq_inss,
              p_valor_inss_imp,        p_base_pis,                p_cod_ret_pis,
              p_aliq_pis,              p_valor_pis_imp,           p_base_cofins,
              p_cod_ret_cofins,        p_aliq_cofins,             p_valor_cofins_imp,
              p_base_csl,              p_cod_ret_csl,             p_aliq_csl,
              p_valor_csl_imp,         p_base_csrf,               p_cod_ret_csrf,
              p_aliq_csrf,             p_valor_csrf_imp,
              --FIM IMPOSTO
              p_codigo_barras,         p_projeto,                 p_subprojeto,
              p_servico,               p_nr_processo_export,      p_nr_processo_import,
              p_cod_cancelamento,      p_data_canc_tit,           p_num_lcmt,
              p_usuario_logado,        P_CGC9_FAVORECIDO,         P_CGC4_FAVORECIDO,
              P_CGC2_FAVORECIDO,       P_BANCO_SISPAG,            P_AGENCIA_SISPAG,
              P_DIG_AGE_SISPAG,        P_CONTA_SISPAG,            P_DIG_CTA_SISPAG2,
              p_num_importacao,        p_juros,                   p_id_titulo_cpag_narwal,
              v_has_error,             v_mensagem_retorno
      );


      if v_mensagem_retorno is null
      then
         p_success := 'S';
      else
         p_des_erro := v_mensagem_retorno;
         p_success := 'N';
      end if;
    end criar_titulo_cpag_010;

FUNCTION valida_titulo(p_valida_titulo t_valida_titulo, p_codigo_empresa int, p_nr_duplicata int, p_seq_duplicata int, p_tipo_titulo int, p_cnpj9 int, p_cnpj4 int, p_cnpj2 int, p_cotacao int, p_valor int, p_codigo_moeda int, p_campo_erro_valida out varchar2) RETURN VARCHAR2 as
    v_mensagem_retorno VARCHAR2(4000);
    tmpInt number;
    v_existe_fornecedor boolean;
  BEGIN
    begin
        select 1 into tmpInt from cpag_010
        where  nr_duplicata = p_nr_duplicata
          and  PARCELA      = p_seq_duplicata
          and  cgc_9        = p_cnpj9
          and  cgc_4        = p_cnpj4
          and  cgc_2        = p_cnpj2
          and  tipo_titulo  = p_tipo_titulo;
    exception when others then
        if trim(tmpInt) is not null
        then
           v_mensagem_retorno := 'Duplicata ja existe: ' || p_nr_duplicata || '-' || p_seq_duplicata || '-' || p_tipo_titulo;
           return v_mensagem_retorno;
        end if;
    end;

    IF p_valida_titulo.validar_fornecedor THEN
        v_existe_fornecedor := ST_PCK_SUPR.exists_fornecedor(p_cnpj9, p_cnpj4, p_cnpj2);

        if v_existe_fornecedor then
            v_mensagem_retorno := 'Fornecedor nao cadastrado: ' || p_cnpj9 || '/' || p_cnpj4 || '-' || p_cnpj2;
            p_campo_erro_valida := 'Fornecedor';
            RETURN v_mensagem_retorno;
        end if;


    END IF;

    IF p_valida_titulo.validar_tipo_titulo THEN
      begin
        select 1 into tmpInt from cpag_040
        where tipo_titulo = p_tipo_titulo;
      exception when others then
        v_mensagem_retorno := 'Tipo Titulo nao encontrado: ' || p_tipo_titulo;
        p_campo_erro_valida := 'Tipo de Titulo';
        RETURN v_mensagem_retorno;
      END;
    END IF;



    IF p_valida_titulo.validar_cotacao THEN
      IF p_cotacao IS NULL OR p_cotacao <= 0 THEN
        v_mensagem_retorno := 'Cotacao nao informada ou invalida';
        p_campo_erro_valida := 'Cotacao';
        RETURN v_mensagem_retorno;
      END IF;
    END IF;

    IF p_valida_titulo.validar_codigo_moeda THEN
      IF p_codigo_moeda IS NULL THEN
        v_mensagem_retorno := 'Codigo da Moeda Nao cadastrado: ' || p_codigo_moeda;
        p_campo_erro_valida := 'Moeda';
        RETURN v_mensagem_retorno;
      END IF;
    END IF;

    IF p_valida_titulo.validar_valor THEN
      IF p_valor IS NULL OR p_valor <= 0 THEN
        v_mensagem_retorno := 'Valor nao informado ou invalido: ' || p_valor;
        p_campo_erro_valida := 'Valor';
        RETURN v_mensagem_retorno;
      END IF;
    END IF;
    return '';
  END valida_titulo;

   FUNCTION next_duplicata(p_cnpj9 IN NUMBER, p_cnpj4 IN NUMBER, p_cnpj2 IN NUMBER, p_tipo_titulo IN NUMBER) RETURN number IS
    v_nr_duplicata_new number;
    BEGIN
        select nvl(max(cpag_010.nr_duplicata),0) + 1
        into v_nr_duplicata_new
        from cpag_010
        where cpag_010.cgc_9       = p_cnpj9
          and cpag_010.cgc_4       = p_cnpj4
          and cpag_010.cgc_2       = p_cnpj2
          and cpag_010.tipo_titulo = p_tipo_titulo;
        RETURN v_nr_duplicata_new;
    end next_duplicata;
end;
/
