create or replace trigger inter_tr_PCPT_070_log 
after insert or delete or update 
on PCPT_070 
for each row 
declare 
   ws_usuario_rede           varchar2(250) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    -- begin 
    --    select hdoc_090.programa 
    --    into v_nome_programa 
    --    from hdoc_090 
    --    where hdoc_090.sid = ws_sid 
    --      and rownum       = 1 
    --      and hdoc_090.programa not like '%menu%' 
    --      and hdoc_090.programa not like '%_m%'; 
    --    exception 
    --      when no_data_found then            v_nome_programa := 'SQL'; 
    -- end; 
 
    v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 
 
 if inserting 
 then 
    begin 
 
        insert into PCPT_070_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           id_OLD,   /*8*/ 
           id_NEW,   /*9*/ 
           pedido_venda_OLD,   /*10*/ 
           pedido_venda_NEW,   /*11*/ 
           seq_item_pedido_OLD,   /*12*/ 
           seq_item_pedido_NEW,   /*13*/ 
           observacao_OLD,   /*14*/ 
           observacao_NEW,   /*15*/ 
           situacao_OLD,   /*16*/ 
           situacao_NEW,   /*17*/ 
           sugestao_escolhida_OLD,   /*18*/ 
           sugestao_escolhida_NEW,   /*19*/ 
           criado_por_OLD,   /*20*/ 
           criado_por_NEW,   /*21*/ 
           criado_em_OLD,   /*22*/ 
           criado_em_NEW,   /*23*/ 
           id_pedido_forca_vendas_OLD,   /*24*/ 
           id_pedido_forca_vendas_NEW    /*25*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.id, /*9*/   
           0,/*10*/
           :new.pedido_venda, /*11*/   
           0,/*12*/
           :new.seq_item_pedido, /*13*/   
           '',/*14*/
           :new.observacao, /*15*/   
           0,/*16*/
           :new.situacao, /*17*/   
           '',/*18*/
           :new.sugestao_escolhida, /*19*/   
           '',/*20*/
           :new.criado_por, /*21*/  
           '', 
           :new.criado_em, /*23*/   
           '',/*24*/
           :new.id_pedido_forca_vendas /*25*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into PCPT_070_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           id_OLD, /*8*/   
           id_NEW, /*9*/   
           pedido_venda_OLD, /*10*/   
           pedido_venda_NEW, /*11*/   
           seq_item_pedido_OLD, /*12*/   
           seq_item_pedido_NEW, /*13*/   
           observacao_OLD, /*14*/   
           observacao_NEW, /*15*/   
           situacao_OLD, /*16*/   
           situacao_NEW, /*17*/   
           sugestao_escolhida_OLD, /*18*/   
           sugestao_escolhida_NEW, /*19*/   
           criado_por_OLD, /*20*/   
           criado_por_NEW, /*21*/   
           criado_em_OLD, /*22*/   
           criado_em_NEW, /*23*/   
           id_pedido_forca_vendas_OLD, /*24*/   
           id_pedido_forca_vendas_NEW  /*25*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.id,  /*8*/  
           :new.id, /*9*/   
           :old.pedido_venda,  /*10*/  
           :new.pedido_venda, /*11*/   
           :old.seq_item_pedido,  /*12*/  
           :new.seq_item_pedido, /*13*/   
           :old.observacao,  /*14*/  
           :new.observacao, /*15*/   
           :old.situacao,  /*16*/  
           :new.situacao, /*17*/   
           :old.sugestao_escolhida,  /*18*/  
           :new.sugestao_escolhida, /*19*/   
           :old.criado_por,  /*20*/  
           :new.criado_por, /*21*/   
           :old.criado_em,  /*22*/  
           :new.criado_em, /*23*/   
           :old.id_pedido_forca_vendas,  /*24*/  
           :new.id_pedido_forca_vendas  /*25*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into PCPT_070_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           id_OLD, /*8*/   
           id_NEW, /*9*/   
           pedido_venda_OLD, /*10*/   
           pedido_venda_NEW, /*11*/   
           seq_item_pedido_OLD, /*12*/   
           seq_item_pedido_NEW, /*13*/   
           observacao_OLD, /*14*/   
           observacao_NEW, /*15*/   
           situacao_OLD, /*16*/   
           situacao_NEW, /*17*/   
           sugestao_escolhida_OLD, /*18*/   
           sugestao_escolhida_NEW, /*19*/   
           criado_por_OLD, /*20*/   
           criado_por_NEW, /*21*/   
           criado_em_OLD, /*22*/   
           criado_em_NEW, /*23*/   
           id_pedido_forca_vendas_OLD, /*24*/   
           id_pedido_forca_vendas_NEW /*25*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.id, /*8*/   
           0, /*9*/
           :old.pedido_venda, /*10*/   
           0, /*11*/
           :old.seq_item_pedido, /*12*/   
           0, /*13*/
           :old.observacao, /*14*/   
           '', /*15*/
           :old.situacao, /*16*/   
           0, /*17*/
           :old.sugestao_escolhida, /*18*/   
           '', /*19*/
           :old.criado_por, /*20*/   
           '', /*21*/
           :old.criado_em, /*22*/ 
           '' ,
           :old.id_pedido_forca_vendas, /*24*/   
           '' /*25*/
         );    
    end;    
 end if;    
end inter_tr_PCPT_070_log;

/
