alter table efic_020 add exige_rolo number(1);
comment on column efic_020.exige_rolo is 'Se deve informar o n�mero do rolo ao apontar parada de m�quina';
alter table efic_090 add codigo_rolo number(9);
comment on column efic_090.codigo_rolo is 'N�mero do rolo afetado com a parada da m�quina';
