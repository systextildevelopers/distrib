create table HAUS_007
(
  ID              NUMBER(9) not null,
  NUMERO_DANF_NFE VARCHAR2(44) not null,
  NUMERO_NOTA     NUMBER(15) not null,
  SERIE_NOTA      VARCHAR2(3) not null,
  SITUACAO        NUMBER(1),
  OBSERVACAO      VARCHAR2(4000)
);
  
create sequence ID_HAUS_007
minvalue 0
maxvalue 99999999999999999999
start with 1
increment by 1
cache 20;

create or replace trigger "INTER_TR_HAUS_007_ID"
before insert on HAUS_007
 for each row

declare
  v_nr_registro number;

begin
  select ID_HAUS_007.nextval into v_nr_registro from dual;

  :new.id := v_nr_registro;

end INTER_TR_HAUS_007_ID;
