create table BASI_062
(
  DATA_INICIAL DATE not null,
  DATA_FINAL   DATE not null,
  NCM      VARCHAR2(15) default ''
);
/

alter table BASI_062
  add constraint PK_BASI_062 primary key (NCM);
/

exec inter_pr_recompile;
/
