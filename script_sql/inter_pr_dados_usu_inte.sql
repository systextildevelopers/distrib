
  CREATE OR REPLACE PROCEDURE "INTER_PR_DADOS_USU_INTE" (
   ws_usuario_rede       out varchar2,
   ws_maquina_rede       out varchar2,
   ws_aplicativo         out varchar2,
   ws_sid                out number,
   ws_usuario_systextil  out varchar2,
   ws_empresa            out number,
   ws_locale_usuario     out varchar2
) is

begin
   -- DADOS DO LOGIN DO USUARIO
   begin
      select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20), sid
      into   ws_usuario_rede,     ws_maquina_rede,      ws_aplicativo,        ws_sid
      from sys.gv_$session
      where audsid  = userenv('SESSIONID')
        and inst_id = userenv('INSTANCE')
        and rownum < 2;
   exception when no_data_found then
      ws_usuario_rede := '';
      ws_maquina_rede := '';
      ws_aplicativo   := '';
      ws_sid          := 0;
   end;

    ws_usuario_systextil := '';
    ws_empresa           := 0;

    ws_locale_usuario := 'pt_BR';

end inter_pr_dados_usu_inte;

 

/

exec inter_pr_recompile;

