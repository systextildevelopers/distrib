create or replace PROCEDURE "INTER_PR_ORDEM_BENEFICIAMENTO"
    -- Recebe parametros para criacao dos estagios e operacoes.
   (v_nivel_tec       in varchar2,    v_grupo_tec        in varchar2,
    v_subgrupo_tec    in varchar2,    v_item_tec         in varchar2,
    v_alternativa_tec in number,      v_roteiro_tec      in number,
    v_ordem_producao  in number,      v_situacao_ordem   in number,
    v_sequencia_tec   in number,      v_qtde_quilos_prog in number,
    v_tipo_ordem      in number,      v_tipo_atu         in varchar2)
is

   -- Cria cursor para executar loop na mqop_050 buscando todas
   -- as operacoes / estagios para o produto.
   cursor mqop050 is
   select codigo_operacao,  seq_operacao,
          codigo_estagio,   minutos,
          subgru_estrutura, item_estrutura,
          cod_estagio_agrupador, seq_operacao_agrupador 
   from mqop_050
   where mqop_050.nivel_estrutura  = v_nivel_tec
     and mqop_050.grupo_estrutura  = v_grupo_tec
     and mqop_050.subgru_estrutura = v_subgrupo_tec
     and mqop_050.item_estrutura   = v_item_tec
     and mqop_050.numero_alternati = v_alternativa_tec
     and mqop_050.numero_roteiro   = v_roteiro_tec
   order by mqop_050.seq_operacao ASC;

   cursor pcpb018 is
   select pcpb_018.ordem_producao, pcpb_018.seq_tecido,
          pcpb_018.seq_operacao,   pcpb_018.codigo_operacao,
          pcpb_018.quantidade,     pcpb_018.minutos
   from pcpb_018
   where pcpb_018.ordem_producao = v_ordem_producao
   order by pcpb_018.seq_tecido,
            pcpb_018.seq_operacao;

   -- Declara as variaveis que serao utilizadas.
   v_minutos050           mqop_050.minutos%type;

   v_data_corrente        date;

   v_nome_operacao        mqop_040.nome_operacao%type;
   v_tipo_operacao        mqop_040.tipo_operacao%type;
   v_grupo_maquinas       mqop_040.grupo_maquinas%type := '####';
   v_sub_maquina          mqop_040.sub_maquina%type    := '###';

   v_rpt_pette            number; -- Variavel para controle especifico Pettenati.
   v_seq_tecido_princ     number; -- Variavel para controle especifico Pettenati.

   v_existe_015           number;
   v_existe_040           number;
   v_existe_028           number;

   v_primeiro             number;
   v_sequencia_upd        number;
   v_estagio_ante         number;
   v_seq_operacao_ante    number;

   v_classificacao        number;

   v_perc_efic_maq        number;
   v_perc_efic_maq_ad     number;

   v_estagio_aux          number;
   v_ultimo_estagio       number;

   v_periodo_producao     number;
   v_estagio_critico      number;
   v_gera_estagio_homem   empr_002.gera_estagio_operacao_homem%type;
   v_estagio_prepara      empr_001.estagio_prepara%type;

   v_registro_pcpb_018    number;
   v_quantidade_resto     number;
   v_encontrou_018        number;
   v_minutos_total_oper   number;
   v_existe_estagio_prep  number;

   v_existe_blocok        number;
   v_valida_arup_rot_ob   number;
   v_cod_emp_periodo      number;
begin
   -- Chamadas: inter_tr_pcpb_010
   --           inter_tr_pcpb_020
   --           inter_tr_pcpb_030
   v_estagio_aux    := 0;
   v_ultimo_estagio := 0;

   
   
   --233059 - Beneficiamento - Validar se roteiro da OB tem agrupador definido caso o parametro esteja configurado como 2-Alerta e Trava
   --Verificar se o BlocoK esta ativo
   v_cod_emp_periodo    := 0;
   v_valida_arup_rot_ob := 0;
   v_existe_blocok      := 0;
   begin
        select 1 
        into v_existe_blocok
        from empr_007 
        where param       = 'obrf.controleProcessoBlocoK' 
        and   default_int = 1
        and rownum <= 1;
   exception when others then
       v_existe_blocok := 0;
   end;

   if v_existe_blocok = 1 then  --BlocoK Ativo
      
      --Buscar o parametro da empresa se valida alerta e trava caso nao tenha agrupador no roteiro
      v_periodo_producao := 0;
      begin
         select pcpb_010.periodo_producao
         into   v_periodo_producao
         from pcpb_010
         where pcpb_010.ordem_producao = v_ordem_producao
         and rownum <= 1;
      exception when others then
         v_periodo_producao := 0;
      end;
      
      --Buscar empresa do periodo da OB
      v_cod_emp_periodo := 0;
      begin
         select pcpc_010.codigo_empresa
         into   v_cod_emp_periodo
         from pcpc_010
         where pcpc_010.periodo_producao = v_periodo_producao
           and pcpc_010.area_periodo     = v_nivel_tec    --Area/Nivel 2 ou 7 (pcpb_f010 ou pcpb_f410) de acordo com o nivel do produto da OB
           and rownum <= 1;
      exception when others then
         v_cod_emp_periodo := 0;
      end;

      --Buscar o parametro de validacao (0-Desativado, 1-Apenas alerta ou 2-Alerta e Trava) desta empresa 
      v_valida_arup_rot_ob := 0;
      begin
         select val_int
         into v_valida_arup_rot_ob
         from empr_008 
         where param        = 'obrf.validaEstAgrupDigitOB' 
         and codigo_empresa = v_cod_emp_periodo
         and rownum <= 1;
      exception when others then
         v_valida_arup_rot_ob := 0;
      end;

   end if;  --v_existe_blocok = 1



   -- Inicio do loop para insercao dos estagios e operacoes.
   for reg_mqop050 in mqop050
   loop
      
     
      --233059 - Beneficiamento - Validar se roteiro da OB tem agrupador definido caso o parametro esteja configurado como 2-Alerta e Trava
      if v_existe_blocok      = 1 and   --Parametro BlocoK Ativo
         v_valida_arup_rot_ob = 2       --Validacao 2-Alerta e Trava caso esteja sem agrupador no roteiro
      then


         if reg_mqop050.cod_estagio_agrupador  is null or reg_mqop050.cod_estagio_agrupador  = 0 or     --Sem agrupador
            reg_mqop050.seq_operacao_agrupador is null or reg_mqop050.seq_operacao_agrupador = 0
         then

            raise_application_error(-20000, 'ATENCAO! Roteiro do produto da OB possui estagio sem agrupador definido (mqop_050)! Nao e possivel continuar! Acesse o roteiro, gere os agrupadores e tente novamente. Estagio: '||reg_mqop050.codigo_estagio||'. Seq Operacao: '||reg_mqop050.seq_operacao);

         end if;


      end if;   --v_existe_blocok = 1 and v_valida_arup_rot_ob = 2      (BlocoK Ativo e Alerta e Trava OB com Roteiro sem Agrupador)



      if v_tipo_ordem <> 4 -- 4 = Aproveitamento sem processo (nao gera estagios nem operacoes.
      then
         begin
            select pcpb_010.periodo_producao
            into   v_periodo_producao
            from pcpb_010
            where pcpb_010.ordem_producao = v_ordem_producao;
            exception
            when others then
               v_periodo_producao := 0;
         end;

         begin
            select pcpc_010.estagio_critico
            into   v_estagio_critico
            from pcpc_010
            where pcpc_010.periodo_producao = v_periodo_producao
              and pcpc_010.area_periodo     = 2;
            exception
            when others then
               v_estagio_critico := 0;
         end;

         -- Carrega as variaveis com os valores do cursor.
         v_minutos050    := 0.0000;
         v_minutos050    := reg_mqop050.minutos;

         -- Carrega a data corrente para filtro.
         v_data_corrente := sysdate;

         begin
            v_tipo_operacao := 0;

            -- Busca a maquina que vai executar a operacao.
            select mqop_040.nome_operacao,  mqop_040.tipo_operacao,
                   mqop_040.grupo_maquinas, mqop_040.sub_maquina,
                   mqop_040.classificacao
            into   v_nome_operacao,         v_tipo_operacao,
                   v_grupo_maquinas,        v_sub_maquina,
                   v_classificacao
            from mqop_040
            where mqop_040.codigo_operacao = reg_mqop050.codigo_operacao;

            if v_tipo_operacao = 1
            then
               begin
                  if v_qtde_quilos_prog > 0.00
                  then
                     v_minutos050 := v_minutos050 * v_qtde_quilos_prog;
                  end if;
               end;
            end if;
            exception
            when others then
               v_nome_operacao  := ' ';
               v_tipo_operacao  := 0;
               v_grupo_maquinas := ' ';
               v_sub_maquina    := ' ';
               v_classificacao  := 0;
         end;

         v_perc_efic_maq    := 0.00;
         v_perc_efic_maq_ad := 0.00;

         begin
            -- Busca o percentual de eficiencia da maquina para calcular os minutos da operacao.
            select mqop_020.perc_eficiencia
            into   v_perc_efic_maq
            from mqop_020
            where mqop_020.grupo_maquina    = v_grupo_maquinas
              and mqop_020.subgrupo_maquina = v_sub_maquina;
            exception
            when others then
               v_perc_efic_maq := 0.00;
         end;

         -- Verifica o percentual adicional para maquina.
         v_perc_efic_maq_ad := v_perc_efic_maq - 100.00;

         -- Adiciona aos minutos o percetual de eficiencia da maquina
         -- quando o percentual de eficiencia for diferente de 100%.
         if v_minutos050 <> 0.00 and v_perc_efic_maq_ad <> 0.00
         then
            v_minutos050 := v_minutos050 + (v_minutos050 * (v_perc_efic_maq_ad / 100.00));
         end if;

         -- Busca parametro de empresa executando a seguinte consistencia:
         -- S - Insere estagio independente da classificacao
         -- N - Nao gera estagio classificacao 3 - Homem
         select empr_002.gera_estagio_operacao_homem
         into v_gera_estagio_homem
         from empr_002;

         if (v_classificacao <> 3 and v_gera_estagio_homem = 'N')
         or  v_gera_estagio_homem = 'S'
         then
            if v_tipo_atu = 'I' -- INCLUSAO.
            then
               if v_situacao_ordem = 0 -- Emissao
               then
                  -- Verifica se existe operacoes para a OB.
                  v_existe_015 := 0;
                  v_existe_028 := 0;

                  select count(*)
                  into v_existe_015
                  from pcpb_015
                  where pcpb_015.ordem_producao  = v_ordem_producao
                    and pcpb_015.seq_operacao    = reg_mqop050.seq_operacao
                    and pcpb_015.codigo_operacao = reg_mqop050.codigo_operacao;

                  -- CARREGA O ESTAGIO DE PREPARACAO PARA VERIFICAR SE
                  -- JA HOUVE PREPARACAO PARA OB'S DE FIO (PCPT_028)
                  select empr_001.estagio_prepara
                  into v_estagio_prepara
                  from empr_001;

                  -- SE ENCONTRAR PREPARACAO DA OB DE FIO, NAO TENTARA INCLUIR
                  -- NOVAMENTE O ESTAGIO DA PREPARACAO, SENAO PODERA DUPLICA-LO
                  -- E ISTO NAO E CORRETO.
                  if reg_mqop050.codigo_estagio = v_estagio_prepara
                  then
                     select nvl(count(*),0)
                     into v_existe_028
                     from pcpt_028
                     where pcpt_028.ordem_producao = v_ordem_producao;
                  else
                     v_existe_028 := 0;
                  end if;

                  -- Se nao existe operacoes e nao foi preparada insere operacao.
                  if v_existe_015 = 0 and v_existe_028 = 0
                  then
                     -- Insere pcpb_018
                     insert into pcpb_018
                        (pcpb_018.ordem_producao,     pcpb_018.seq_tecido,
                         pcpb_018.seq_operacao,       pcpb_018.codigo_operacao,
                         pcpb_018.quantidade,         pcpb_018.minutos
                        )
                     values
                        (v_ordem_producao,            v_sequencia_tec,
                         reg_mqop050.seq_operacao,    reg_mqop050.codigo_operacao,
                         v_qtde_quilos_prog,          reg_mqop050.minutos
                        );

                     -- Atualiza tabela de planejamento.
                     insert into pcpb_015
                        (pcpb_015.ordem_producao,      pcpb_015.seq_operacao,
                         pcpb_015.codigo_operacao,     pcpb_015.grupo_maquina,
                         pcpb_015.subgrupo_maquina,    pcpb_015.codigo_estagio,
                         pcpb_015.minutos_unitario,    
                         pcpb_015.cod_estagio_agrupador, pcpb_015.seq_operacao_agrupador) 
                     values
                        (v_ordem_producao,             reg_mqop050.seq_operacao,
                         reg_mqop050.codigo_operacao,  v_grupo_maquinas,
                         v_sub_maquina,                reg_mqop050.codigo_estagio,
                         v_minutos050,
                         reg_mqop050.cod_estagio_agrupador, reg_mqop050.seq_operacao_agrupador); 
                  else
                     -- Se nao foi preparado atualiza tempo.
                     if v_existe_028 = 0 and v_tipo_operacao = 1
                     then
                        v_registro_pcpb_018 := 0;

                        select count(*)
                        into v_registro_pcpb_018
                        from pcpb_018
                        where pcpb_018.ordem_producao  = v_ordem_producao
                          and pcpb_018.seq_tecido      = v_sequencia_tec
                          and pcpb_018.seq_operacao    = reg_mqop050.seq_operacao
                          and pcpb_018.codigo_operacao = reg_mqop050.codigo_operacao;

                        if v_registro_pcpb_018 > 0
                        then
                           update pcpb_018
                           set pcpb_018.quantidade = pcpb_018.quantidade + v_qtde_quilos_prog
                           where pcpb_018.ordem_producao  = v_ordem_producao
                             and pcpb_018.seq_tecido      = v_sequencia_tec
                             and pcpb_018.seq_operacao    = reg_mqop050.seq_operacao
                             and pcpb_018.codigo_operacao = reg_mqop050.codigo_operacao;
                        else
                           insert into pcpb_018
                             (pcpb_018.ordem_producao,     pcpb_018.seq_tecido,
                              pcpb_018.seq_operacao,       pcpb_018.codigo_operacao,
                              pcpb_018.quantidade,         pcpb_018.minutos
                             )
                           values
                             (v_ordem_producao,            v_sequencia_tec,
                              reg_mqop050.seq_operacao,    reg_mqop050.codigo_operacao,
                              v_qtde_quilos_prog,          reg_mqop050.minutos
                              );
                        end if;

                        v_minutos_total_oper := 0.000;

                        for reg_pcpb018 in pcpb018
                        loop
                           if reg_mqop050.seq_operacao = reg_pcpb018.seq_operacao and
                              reg_mqop050.codigo_operacao = reg_pcpb018.codigo_operacao
                           then
                              v_minutos_total_oper := v_minutos_total_oper + (reg_pcpb018.quantidade * reg_pcpb018.minutos);
                           end if;
                        end loop;

                        begin
                           update pcpb_015
                           set pcpb_015.minutos_unitario = (v_minutos_total_oper + (v_minutos_total_oper * (v_perc_efic_maq_ad / 100.00)))
                           where pcpb_015.ordem_producao  = v_ordem_producao
                             and pcpb_015.seq_operacao    = reg_mqop050.seq_operacao
                             and pcpb_015.codigo_operacao = reg_mqop050.codigo_operacao;
                        end;
                     else
                        if v_tipo_operacao = 2
                        then
                           select count(*)
                           into v_registro_pcpb_018
                           from pcpb_018
                           where pcpb_018.ordem_producao  = v_ordem_producao
                             and pcpb_018.seq_tecido      = v_sequencia_tec
                             and pcpb_018.seq_operacao    = reg_mqop050.seq_operacao
                             and pcpb_018.codigo_operacao = reg_mqop050.codigo_operacao;

                           if v_registro_pcpb_018 > 0
                           then
                              update pcpb_018
                              set pcpb_018.quantidade = pcpb_018.quantidade + v_qtde_quilos_prog
                              where pcpb_018.ordem_producao  = v_ordem_producao
                                and pcpb_018.seq_tecido      = v_sequencia_tec
                                and pcpb_018.seq_operacao    = reg_mqop050.seq_operacao
                                and pcpb_018.codigo_operacao = reg_mqop050.codigo_operacao;
                           else
                              insert into pcpb_018
                                (pcpb_018.ordem_producao,     pcpb_018.seq_tecido,
                                 pcpb_018.seq_operacao,       pcpb_018.codigo_operacao,
                                 pcpb_018.quantidade,         pcpb_018.minutos
                                )
                              values
                                (v_ordem_producao,            v_sequencia_tec,
                                 reg_mqop050.seq_operacao,    reg_mqop050.codigo_operacao,
                                 v_qtde_quilos_prog,          reg_mqop050.minutos
                                );
                           end if;
                        end if;
                     end if;
                  end if;

                  /* ++++++++++++++++  Carga da pcpb_040 ++++++++++++++++++++++++++++++++++*/

                  -- Verifica se o rpt que esta configurado e Pettenati.
                  -- Se for utiliza como regra para inserir estagios.
                  v_rpt_pette := 0;

                  select count(*)
                  into v_rpt_pette
                  from fatu_500
                  where fatu_500.rpt_ordem_benef = 'pcpb_opb002';

                  v_seq_tecido_princ := 1; /*Expecifico para Pettenati*/

                  -- Atualiza os estagios para OB.
                  if (v_rpt_pette > 0 and v_seq_tecido_princ = v_sequencia_tec)
                  or  v_rpt_pette = 0
                  then
                     if reg_mqop050.codigo_estagio <> v_estagio_aux
                     and reg_mqop050.codigo_estagio > 0
                     then
                        -- Verifica se ja existe estagio gerado para esta OB.
                        v_existe_040 := 0;
                        v_existe_028 := 0;

                        select empr_001.estagio_prepara
                        into v_estagio_prepara
                        from empr_001;

                        if reg_mqop050.codigo_estagio = v_estagio_prepara
                        then
                           select nvl(count(*),0)
                           into v_existe_028
                           from pcpt_028
                           where pcpt_028.ordem_producao = v_ordem_producao;
                        else
                           v_existe_028 := 0;
                        end if;

                        select count(*)
                        into v_existe_040
                        from pcpb_040
                        where pcpb_040.ordem_producao = v_ordem_producao
                          and pcpb_040.codigo_estagio = reg_mqop050.codigo_estagio
                          and pcpb_040.seq_operacao   = reg_mqop050.seq_operacao;

                        -- Se nao existir insere os estagios.
                        if  v_existe_040 = 0
                        and v_existe_028 = 0
                        then
                           insert into pcpb_040
                              (ordem_producao,           codigo_estagio,
                               seq_operacao)
                           values
                              (v_ordem_producao,         reg_mqop050.codigo_estagio,
                               reg_mqop050.seq_operacao);
                        end if;

                        v_estagio_aux := reg_mqop050.codigo_estagio;

                        v_ultimo_estagio := v_estagio_aux;

                     end if;
                  end if;
                  end if;
            end if;
         end if;

         if v_tipo_atu = 'I' -- INCLUSAO.
         then
            -- Executa calculo para atualizar a sequencia dos estagios para producao.
            v_primeiro      := 0;
            v_sequencia_upd := 0;

            for reg_pcpb_040 in (select pcpb_040.codigo_estagio,  pcpb_040.seq_operacao
                                 from pcpb_040
                                 where pcpb_040.ordem_producao = v_ordem_producao
                                 order by pcpb_040.codigo_estagio,
                                          pcpb_040.seq_operacao asc)
            loop
               if v_primeiro = 1
               then
                  if v_estagio_ante <> reg_pcpb_040.codigo_estagio
                  then
                     update pcpb_040
                     set seq_estagio = 9
                     where pcpb_040.codigo_estagio = v_estagio_ante
                       and pcpb_040.seq_operacao   = v_seq_operacao_ante
                       and pcpb_040.ordem_producao = v_ordem_producao;

                     v_sequencia_upd := 0;

                  else
                     update pcpb_040
                     set seq_estagio = v_sequencia_upd
                     where pcpb_040.codigo_estagio = v_estagio_ante
                       and pcpb_040.seq_operacao   = v_seq_operacao_ante
                       and pcpb_040.ordem_producao = v_ordem_producao;
                  end if;
               end if;

               v_estagio_ante      := reg_pcpb_040.codigo_estagio;
               v_seq_operacao_ante := reg_pcpb_040.seq_operacao;

               v_sequencia_upd := v_sequencia_upd + 1;
               v_primeiro      := 1;
            end loop;

            update pcpb_040
            set seq_estagio = 9
            where pcpb_040.codigo_estagio = v_estagio_ante
              and pcpb_040.seq_operacao   = v_seq_operacao_ante
              and pcpb_040.ordem_producao = v_ordem_producao;
         end if;
      end if;
   end loop;

   for reg3 in (select pcpb_018.ordem_producao,    pcpb_018.seq_operacao,
                       pcpb_018.codigo_operacao,   pcpb_018.seq_tecido,
                       pcpb_018.minutos,           pcpb_018.quantidade
                from pcpb_018
                where pcpb_018.ordem_producao = v_ordem_producao
                  and pcpb_018.seq_tecido     = v_sequencia_tec
                order by pcpb_018.seq_operacao asc
                )
   loop
      if v_tipo_atu = 'D' --DELECAO.
      then
         if v_situacao_ordem = 0 -- Emissao
         then
            v_existe_015 := 0;
            v_existe_028 := 0;

            select count(*)
            into v_existe_015
            from pcpb_015
            where pcpb_015.ordem_producao  = v_ordem_producao
              and pcpb_015.seq_operacao    = reg3.seq_operacao
              and pcpb_015.codigo_operacao = reg3.codigo_operacao;

            -- CARREGA O ESTAGIO DE PREPARACAO PARA VERIFICAR SE
            -- JA HOUVE PREPARACAO PARA OB'S DE FIO (PCPT_028)
            select empr_001.estagio_prepara
            into v_estagio_prepara
            from empr_001;

            begin
               v_tipo_operacao := 0;

               -- Busca a maquina que vai executar a operacao.
               select mqop_040.nome_operacao,  mqop_040.tipo_operacao,
                      mqop_040.grupo_maquinas, mqop_040.sub_maquina,
                      mqop_040.classificacao
               into   v_nome_operacao,         v_tipo_operacao,
                      v_grupo_maquinas,        v_sub_maquina,
                      v_classificacao
               from mqop_040
               where mqop_040.codigo_operacao = reg3.codigo_operacao;

               if v_tipo_operacao = 1
               then
                  begin
                     if v_qtde_quilos_prog > 0.00
                     then
                        v_minutos_total_oper := 0.000;
                        v_minutos_total_oper := v_qtde_quilos_prog * reg3.minutos;
                     end if;
                  end;
               end if;
               exception
               when others then
                  v_nome_operacao  := ' ';
                  v_tipo_operacao  := 0;
                  v_grupo_maquinas := ' ';
                  v_sub_maquina    := ' ';
                  v_classificacao  := 0;
            end;

            v_perc_efic_maq    := 0.00;
            v_perc_efic_maq_ad := 0.00;

            begin
               -- Busca o percentual de eficiencia da maquina para calcular os minutos da operacao.
               select mqop_020.perc_eficiencia
               into   v_perc_efic_maq
               from mqop_020
               where mqop_020.grupo_maquina    = v_grupo_maquinas
                 and mqop_020.subgrupo_maquina = v_sub_maquina;
               exception
               when others then
                  v_perc_efic_maq := 0.00;
            end;

            -- Verifica o percentual adicional para maquina.
            v_perc_efic_maq_ad := v_perc_efic_maq - 100.00;

            -- Adiciona aos minutos o percetual de eficiencia da maquina
            -- quando o percentual de eficiencia for diferente de 100%.
            if v_minutos_total_oper <> 0.00 and v_perc_efic_maq_ad <> 0.00
            then
               v_minutos_total_oper := v_minutos_total_oper + (v_minutos_total_oper * (v_perc_efic_maq_ad / 100.00));
            end if;

            -- SE ENCONTRAR PREPARACAO DA OB DE FIO, NAO TENTARA INCLUIR
            -- NOVAMENTE O ESTAGIO DA PREPARACAO, SENAO PODERA DUPLICA-LO
            -- E ISTO NAO E CORRETO.
            select count(*)
            into v_existe_estagio_prep
            from pcpb_015
            where pcpb_015.ordem_producao = v_ordem_producao
              and pcpb_015.codigo_estagio = v_estagio_prepara;

            if v_existe_estagio_prep > 0
            then
               select count(*)
               into v_existe_028
               from pcpt_028
               where pcpt_028.ordem_producao = v_ordem_producao;
            else
               v_existe_028 := 0;
            end if;

            if v_existe_028 = 0
            then
--               v_minutos_total_oper :=1;

               update pcpb_018
               set pcpb_018.quantidade = pcpb_018.quantidade - v_qtde_quilos_prog
               where pcpb_018.ordem_producao  = v_ordem_producao
                 and pcpb_018.seq_tecido      = v_sequencia_tec
                 and pcpb_018.seq_operacao    = reg3.seq_operacao
                 and pcpb_018.codigo_operacao = reg3.codigo_operacao;

               update pcpb_015
               set pcpb_015.minutos_unitario = pcpb_015.minutos_unitario - v_minutos_total_oper
               where pcpb_015.ordem_producao   = v_ordem_producao
                 and pcpb_015.seq_operacao     = reg3.seq_operacao
                 and pcpb_015.codigo_operacao  = reg3.codigo_operacao
                 and pcpb_015.minutos_unitario > 0.000;

               -- Quando nao restar peso para operacao na pcpb_018, deleta pcpb_018
               -- DELETA o registro do pcpb_015 e o seu respectivo no pcpb_040.
               select pcpb_018.quantidade
               into v_quantidade_resto
               from pcpb_018
               where pcpb_018.ordem_producao  = v_ordem_producao
                 and pcpb_018.seq_tecido      = v_sequencia_tec
                 and pcpb_018.seq_operacao    = reg3.seq_operacao
                 and pcpb_018.codigo_operacao = reg3.codigo_operacao;

               if v_quantidade_resto <= 0.000
               then
                  begin
                      delete pcpb_018
                      where pcpb_018.ordem_producao  = v_ordem_producao
                        and pcpb_018.seq_tecido      = v_sequencia_tec
                        and pcpb_018.seq_operacao    = reg3.seq_operacao
                        and pcpb_018.codigo_operacao = reg3.codigo_operacao;
                  end;
               end if;

               select count(*)
               into v_encontrou_018
               from pcpb_018
               where pcpb_018.ordem_producao  = v_ordem_producao
                 and pcpb_018.seq_operacao    = reg3.seq_operacao
                 and pcpb_018.codigo_operacao = reg3.codigo_operacao;

               if v_encontrou_018 = 0
               then
                  delete pcpb_015
                  where pcpb_015.ordem_producao  = v_ordem_producao
                    and pcpb_015.seq_operacao    = reg3.seq_operacao
                    and pcpb_015.codigo_operacao = reg3.codigo_operacao
                    and pcpb_015.data_inicio     is null;
               end if;

               select count(*)
               into v_encontrou_018
               from pcpb_018
               where pcpb_018.ordem_producao  = v_ordem_producao;

               if v_encontrou_018 = 0
               then
                  delete pcpb_015
                  where pcpb_015.ordem_producao = v_ordem_producao
                    and pcpb_015.data_inicio    is null;
               end if;
            end if;
         end if;
      end if;

      if v_tipo_atu = 'D' --DELECAO.
      then
         -- Verifica se eliminou a sequencia da operacao da OB.
         v_existe_015 := 0;
         v_existe_028 := 0;

         select count(*)
         into v_existe_015
         from pcpb_015
         where pcpb_015.ordem_producao  = v_ordem_producao
           and pcpb_015.seq_operacao    = reg3.seq_operacao
           and pcpb_015.codigo_operacao = reg3.codigo_operacao;

          -- CARREGA O ESTAGIO DE PREPARACAO PARA VERIFICAR SE
          -- JA HOUVE PREPARACAO PARA OB'S DE FIO (PCPT_028)
         select empr_001.estagio_prepara
         into v_estagio_prepara
         from empr_001;

         -- SE ENCONTRAR PREPARACAO DA OB DE FIO, NAO TENTARA INCLUIR
         -- NOVAMENTE O ESTAGIO DA PREPARACAO, SENAO PODERA DUPLICA-LO
         -- E ISTO NAO E CORRETO.
         select count(*)
         into v_existe_estagio_prep
         from pcpb_015
         where pcpb_015.ordem_producao = v_ordem_producao
           and pcpb_015.codigo_estagio = v_estagio_prepara;

         if v_existe_estagio_prep > 0
         then
            select count(*)
            into v_existe_028
            from pcpt_028
            where pcpt_028.ordem_producao = v_ordem_producao;
         else
            v_existe_028 := 0;
         end if;

         select count(*)
         into v_rpt_pette
         from fatu_500
         where fatu_500.rpt_ordem_benef = 'pcpb_opb002';

         -- Se nao existe a sequencia, e a situacao da OB for zero de OB digitada entao
         -- elimina o estagio desta ordem, mas somente se o estagio nao estiver baixado.
         if (v_rpt_pette = 0 and v_existe_015 = 0    and v_situacao_ordem = 0 and v_existe_028 = 0) or
            (v_rpt_pette > 0 and v_sequencia_tec = 1 and v_existe_015 = 0     and v_situacao_ordem = 0 and v_existe_028 = 0)
         then
            delete pcpb_040
            where pcpb_040.ordem_producao = v_ordem_producao
              and pcpb_040.seq_operacao   = reg3.seq_operacao
              and pcpb_040.data_inicio   is null
              and not exists (select 1 from pcpb_015
                              where pcpb_015.ordem_producao = pcpb_040.ordem_producao
                                and pcpb_015.codigo_estagio = pcpb_040.codigo_estagio);
         end if;
      end if;
   end loop;

   -- Atualiza o ultimo estagio na capa da OB conforme
   -- o ultimo estagio da OB na pcpb_040.
   if v_tipo_atu = 'I'
   then
      update pcpb_010
      set ultimo_estagio = v_ultimo_estagio
      where ordem_producao = v_ordem_producao
        and ultimo_estagio = 0;
   end if;
end INTER_PR_ORDEM_BENEFICIAMENTO;
/

exec inter_pr_recompile;

