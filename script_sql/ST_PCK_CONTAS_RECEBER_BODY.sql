create or replace PACKAGE BODY "ST_PCK_CONTAS_RECEBER" IS

    FUNCTION valida_titulo( 
        p_valida_titulo     t_valida_titulo, 
        p_codigo_empresa    INT, 
        p_nr_duplicata      INT, 
        p_seq_duplicata     int, 
        p_tipo_titulo       INT, 
        p_cnpj9             INT, 
        p_cnpj4             INT, 
        p_cnpj2             INT, 
        p_cotacao           INT, 
        p_valor             INT, 
        p_codigo_moeda      INT, 
        p_campo_erro_valida OUT VARCHAR2
    ) RETURN VARCHAR2 IS
    tmpInt NUMBER;
    v_mensagem_retorno VARCHAR2(4000);
    BEGIN

        -- Validac?o do cliente (antes era fornecedor)
        IF p_valida_titulo.validar_cliente THEN
            BEGIN
                SELECT 1 INTO tmpInt 
                FROM pedi_010
                WHERE CGC_9 = p_cnpj9  
                AND CGC_4 = p_cnpj4
                AND CGC_2 = p_cnpj2;
            EXCEPTION
                WHEN OTHERS THEN
                    v_mensagem_retorno := 'Cliente n?o cadastrado: ' || p_cnpj9 || '/' || p_cnpj4 || '-' || p_cnpj2;
                    p_campo_erro_valida := 'Cliente';
                    RETURN v_mensagem_retorno;
            END;
        END IF;

        -- Validac?o do tipo de titulo
        IF p_valida_titulo.validar_tipo_titulo THEN
            BEGIN
                SELECT 1 INTO tmpInt 
                FROM cpag_040
                WHERE tipo_titulo = p_tipo_titulo;
            EXCEPTION
                WHEN OTHERS THEN
                    v_mensagem_retorno := 'Tipo de Titulo n?o encontrado: ' || p_tipo_titulo;
                    p_campo_erro_valida := 'Tipo de Titulo';
                    RETURN v_mensagem_retorno;
            END;
        END IF;

        -- Validac?o da cotac?o
        IF p_valida_titulo.validar_cotacao THEN
            IF p_cotacao IS NULL OR p_cotacao <= 0 THEN
                v_mensagem_retorno := 'Cotac?o n?o informada ou invalida';
                p_campo_erro_valida := 'Cotac?o';
                RETURN v_mensagem_retorno;
            END IF;
        END IF;

        -- Validac?o do codigo de moeda
        IF p_valida_titulo.validar_codigo_moeda THEN
            IF p_codigo_moeda IS NULL THEN
                v_mensagem_retorno := 'Codigo da Moeda n?o cadastrado: ' || p_codigo_moeda;
                p_campo_erro_valida := 'Moeda';
                RETURN v_mensagem_retorno;
            END IF;
        END IF;

        -- Validac?o do valor
        IF p_valida_titulo.validar_valor THEN
            IF p_valor IS NULL OR p_valor <= 0 THEN
                v_mensagem_retorno := 'Valor n?o informado ou invalido: ' || p_valor;
                p_campo_erro_valida := 'Valor';
                RETURN v_mensagem_retorno;
            END IF;
        END IF;

        -- Verificac?o se o titulo ja existe
        BEGIN
            SELECT 1 INTO tmpInt 
            FROM fatu_070
            WHERE num_duplicata = p_nr_duplicata
            AND seq_duplicatas = p_seq_duplicata
            and tipo_titulo = p_tipo_titulo
            AND cli_dup_cgc_cli9 = p_cnpj9
            AND cli_dup_cgc_cli4 = p_cnpj4
            AND cli_dup_cgc_cli2 = p_cnpj2;

            RETURN 'Titulo ja existe';
        EXCEPTION
            WHEN OTHERS THEN
                NULL; -- Continua o fluxo, pois n?o encontrou o titulo
        END;

        RETURN '';
    END valida_titulo;


    PROCEDURE criar_titulo(
            p_codigo_empresa in fatu_070.codigo_empresa%type,
            p_cli_dup_cgc_cli9 in fatu_070.cli_dup_cgc_cli9%type,
            p_cli_dup_cgc_cli4 in fatu_070.cli_dup_cgc_cli4%type, 
            p_cli_dup_cgc_cli2 in fatu_070.cli_dup_cgc_cli9%type,
            p_tipo_titulo in fatu_070.tipo_titulo%type,
            p_num_duplicata in fatu_070.num_duplicata%type,
            p_seq_duplicatas in fatu_070.seq_duplicatas%type,
            p_data_venc_duplic in fatu_070.data_venc_duplic%type,
            p_valor_duplicata in fatu_070.valor_duplicata%type,
            p_cod_rep_cliente in fatu_070.cod_rep_cliente%type,
            p_data_emissao in fatu_070.data_emissao%type,
            p_data_transacao IN date,
            p_numero_titulo in fatu_070.numero_titulo%type,
            p_numero_sequencia in fatu_070.numero_sequencia%type,
            p_cod_historico in fatu_070.cod_historico%type,
            p_cod_local in fatu_070.cod_local%type,
            p_port_anterior in fatu_070.port_anterior%type,
            p_portador_duplic in fatu_070.portador_duplic%type,
            p_posicao_duplic in fatu_070.posicao_duplic%type,
            p_data_prorrogacao in fatu_070.data_prorrogacao%type,
            p_tecido_peca in fatu_070.tecido_peca%type,
            p_seq_end_cobranca in fatu_070.seq_end_cobranca%type,
            p_comissao_lancada in fatu_070.comissao_lancada%type,
            p_tipo_tit_origem in fatu_070.tipo_tit_origem%type,
            p_num_dup_origem in fatu_070.num_dup_origem%type,
            p_seq_dup_origem in fatu_070.seq_dup_origem%type,
            p_cli9resptit in fatu_070.cli9resptit%type,
            p_cli4resptit in fatu_070.cli4resptit%type,
            p_cli2resptit in fatu_070.cli2resptit%type,
            p_origem_pedido in fatu_070.origem_pedido%type,
            p_cod_transacao in out fatu_070.cod_transacao%type,
            p_codigo_contabil in fatu_070.codigo_contabil%type,
            p_valor_moeda in fatu_070.valor_moeda%type,
            p_nr_identificacao in fatu_070.nr_identificacao%type,
            p_conta_corrente in fatu_070.conta_corrente%type,
            p_num_contabil in fatu_070.num_contabil%type,
            p_duplic_emitida in fatu_070.duplic_emitida%type,
            p_pedido_venda in fatu_070.pedido_venda%type,
            p_cod_processo    in fatu_070.cod_processo%type,
            p_codigo_depto IN number,
            p_emitente_titulo IN varchar2,
            p_codigo_historico IN number,
            p_moeda_titulo in number,
            p_cotacao_moeda in number,
            p_prg_gerador IN varchar2,
            p_usuario in varchar2,
            p_num_lcmt IN OUT number,
            r_success out varchar2,
            r_des_erro out varchar2
    ) IS
    v_cod_contab_for_cre    supr_010.codigo_contabil%type; 
    v_origem                cont_520.origem%type; 
    v_contabiliza_trans     estq_005.atualiza_contabi%type; 
    v_tipo_contab_deb       cont_550.tipo_contabil%type; 
    v_tipo_contab_cre       cont_550.tipo_contabil%type; 
    p_compl_historico       cont_600.compl_histor1%type; 
    v_mensagem_retorno      varchar2(4000); 
    v_has_error             boolean; 
    BEGIN
         
      v_origem := 3;--Fixo para contas a receber
      begin 
          select pedi_010.codigo_contabil 
          into v_cod_contab_for_cre 
          from pedi_010  
          where pedi_010.cgc_9 = p_cli_dup_cgc_cli9 
          and   pedi_010.cgc_4 = p_cli_dup_cgc_cli4 
          and   pedi_010.cgc_2 = p_cli_dup_cgc_cli2; 
      exception 
      when no_data_found then 
          v_cod_contab_for_cre := 0; 
      end; 
 
      begin 
          select atualiza_contabi 
          INTO v_contabiliza_trans 
          from estq_005 
          where codigo_transacao = p_cod_transacao; 
      EXCEPTION WHEN  
      others then 
          v_contabiliza_trans := 0; 
      end; 
    
      if v_contabiliza_trans <> 2 then 
         v_tipo_contab_deb := 1; 
         v_tipo_contab_cre := 5; 
 
         p_compl_historico := lpad(p_num_duplicata, 9, 0) || '/' || p_seq_duplicatas || ' - ' || p_emitente_titulo; 
 
         inter_pr_contabilizacao_prg_02(p_codigo_empresa,   p_codigo_depto,     p_cli_dup_cgc_cli9,   p_cli_dup_cgc_cli4,    p_cli_dup_cgc_cli2,  
                                 p_cod_transacao,     p_codigo_historico,    0,      p_codigo_contabil,   
                                 v_cod_contab_for_cre,   p_num_duplicata,     p_data_transacao,           p_valor_duplicata, 
                                 v_origem,               v_tipo_contab_deb,  v_tipo_contab_cre,     p_prg_gerador, 
                                 p_usuario,              p_compl_historico, p_tipo_titulo, p_num_lcmt,  v_mensagem_retorno);  
 
         if v_mensagem_retorno is not null then 
             r_des_erro := v_mensagem_retorno; 
             r_success := 'N'; 
             return;
         end if; 
      end if;

        inter_pr_insere_fatu_070_new (
            p_codigo_empresa,
            p_cli_dup_cgc_cli9,
            p_cli_dup_cgc_cli4, 
            p_cli_dup_cgc_cli2,
            p_tipo_titulo,
            p_num_duplicata,
            p_seq_duplicatas,
            p_data_venc_duplic,
            p_valor_duplicata,
            p_cod_rep_cliente,
            p_data_emissao,
            p_numero_titulo,
            p_numero_sequencia,
            p_cod_historico,
            p_cod_local,
            p_port_anterior,
            p_portador_duplic,
            p_posicao_duplic,
            p_data_prorrogacao,
            p_tecido_peca,
            p_seq_end_cobranca,
            p_comissao_lancada,
            p_tipo_tit_origem,
            p_num_dup_origem,
            p_seq_dup_origem,
            p_cli9resptit,
            p_cli4resptit,
            p_cli2resptit,
            p_origem_pedido,
            p_cod_transacao,
            p_codigo_contabil,
            p_valor_moeda,
            p_nr_identificacao,
            p_conta_corrente,
            p_num_lcmt,
            p_duplic_emitida,
            p_pedido_venda,
            p_cod_processo,
            p_compl_historico,
            p_moeda_titulo,
            p_cotacao_moeda,
            r_des_erro         );
    END criar_titulo;

END "ST_PCK_CONTAS_RECEBER";
