create or replace trigger wms_tr_volumes
  before insert on inte_wms_volumes
  for each row
declare
  -- local variables here
  v_retorno_int              number;
  v_encontrou_reg            number; --0: N�o; 1: Sim

  v_seq_item_ped             number;
  v_cod_deposito             number;
  v_qtd_tot_vol              number;
  v_qtd_saldo_item           number;
  v_artigo_prod              number;
  v_linha_prod				       number;
  v_qtde_inserir_325         number;
  v_qtde_inserida_325        number;
  v_qtde_mont_vol            number;
  v_caracteristica_300       number;
  v_preco_medio_010          number;
  v_trans_saida_tipo_vol     number;
  v_transacao_ent            number;
  v_atualiza_estoque_kit_ent number;
  v_atualiza_estoque_kit_sai number;
  v_codigo_empresa           number;
  v_cod_deposito_saida_kit   number;
  v_cod_dep_entrada_kit      number;
  v_cod_deposito_saida_kg    number;
  v_cod_dep_entrada_kg       number;
  v_dep_kg_aux				       number;
  v_tipo_volume              number;
  v_peso_aux				         number;
  peso_real_item_aux         number;
  peso_liquido_total_aux	   number;
  peso_liquido_item_aux	     number;
  v_cor_estoque              varchar2(6);
  v_corTag                   varchar2(6);
  v_item                     varchar2(6);

  v_niv                      varchar2(1);
  v_gru                      varchar2(5);
  v_sub                      varchar2(3);
  v_ite                      varchar2(6);

  v_scdnivelpedido           varchar2(1);
  v_scdgrupopedido           varchar2(5);

  v_niv_311                  varchar2(1);
  v_gru_311                  varchar2(5);

  v_deposito_saida_aux       number;
  v_saldo_item               number;
  v_saldo_item_caixa         number;
  v_saldo_item_caixa_aux     number;
  v_qtde_atualiza            number;
  v_seq_atualiza             number;

  v_nivel_estoque            varchar2(1);
  v_grupo_estoque            varchar2(5);
  v_sub_estoque              varchar2(3);
  v_item_estoque             varchar2(6);

  v_niv_kg					         varchar2(1);
  v_gru_kg					         varchar2(5);
  v_tam_kg            		   varchar2(3);
  v_cor_kg 					         varchar2(6);
begin

  v_nivel_estoque            := '';
  v_grupo_estoque            := '';
  v_sub_estoque              := '';
  v_item_estoque             := '';

   --Buscar informa��es do tipo de volume
   begin
      select pcpc_300.caracteristica,   pcpc_300.transacao
      into  v_caracteristica_300,       v_trans_saida_tipo_vol
      from  pcpc_300
      where pcpc_300.cod_tipo_volume = :new.cod_tipo_vol_st;
   exception
   when no_data_found then
      v_caracteristica_300   := 0;
      v_trans_saida_tipo_vol := 0;
   end;

   begin
      select estq_005.atualiza_estoque,     estq_005.transac_entrada
      into v_atualiza_estoque_kit_sai,      v_transacao_ent
      from estq_005
      where estq_005.codigo_transacao = v_trans_saida_tipo_vol;
   exception
   when no_data_found then
      v_atualiza_estoque_kit_sai := 0;
      v_transacao_ent            := 0;
   end;

   begin
      select estq_005.atualiza_estoque
      into   v_atualiza_estoque_kit_ent
      from estq_005
      where estq_005.codigo_transacao = v_transacao_ent;
   exception
   when no_data_found then
      v_atualiza_estoque_kit_ent := 0;
   end;

   --Se possui pedido, buscar empresa do pedido
   if :new.nr_pedido <> 0
   then

      begin
         select pedi_100.codigo_empresa
         into   v_codigo_empresa
         from pedi_100
         where pedi_100.pedido_venda = :new.nr_pedido;
      exception
      when no_data_found then
         v_codigo_empresa := 0;
      end;

   else

      --Se n�o possui pedido, buscar empresa do dep�sito do volume
      begin
         select basi_205.local_deposito
         into   v_codigo_empresa
         from basi_205
         where basi_205.codigo_deposito = (select nvl(max(inte_wms_vol_itens.dep_item), 0)
                                           from inte_wms_vol_itens
                                           where inte_wms_vol_itens.nr_volume_st = :new.nr_volume_st);
      exception
      when no_data_found then
         v_codigo_empresa := 0;
      end;

   end if;

   --Buscar dep�sito de entrada e sa�da KIT
   begin
      select fatu_503.icdDepositoSaidaKit,    fatu_503.icdDepositoEntradaKit
      into   v_cod_deposito_saida_kit,        v_cod_dep_entrada_kit
      from fatu_503
      where fatu_503.codigo_empresa = v_codigo_empresa;
   exception
   when no_data_found then
      v_cod_deposito_saida_kit := 0;
      v_cod_dep_entrada_kit    := 0;
   end;

   --Buscar dep�sito de entrada e sa�da KG
   begin
      select fatu_501.dep_saida_kg_palm,      fatu_501.dep_entrada_kg_palm
      into   v_cod_deposito_saida_kg,         v_cod_dep_entrada_kg
      from fatu_501
      where fatu_501.codigo_empresa = v_codigo_empresa;
   exception
   when no_data_found then
      v_cod_deposito_saida_kg := 0;
      v_cod_dep_entrada_kg    := 0;
   end;

   begin

      /*gravar a capa da volume*/
      insert into pcpc_320
      ( pcpc_320.numero_volume,          pcpc_320.pedido_venda,
        pcpc_320.peso_volume,            pcpc_320.cod_tipo_volume,
        pcpc_320.cod_embalagem,          pcpc_320.montador,
        pcpc_320.turno,                  pcpc_320.cod_usuario,
        pcpc_320.data_montagem,          pcpc_320.hora_montagem,
        pcpc_320.situacao_volume,        pcpc_320.deposito_entrada,
        pcpc_320.transacao_entrada
      )
      values
      ( :new.nr_volume_st,               :new.nr_pedido,
        :new.peso_volume,                :new.cod_tipo_vol_st,
        :new.cod_embalagem_st,           :new.montador,
        :new.turno,                      :new.cod_usuario,
        trunc(:new.timestamp_fim_mont),  to_date('16/11/1989 '||to_char(:new.timestamp_fim_mont,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS'),
        0,                               0,
        0
      );
   exception
   when others then
      raise_application_error(-20000, 'N�o inseriu na tabela PCPC_320.' || Chr(10) || SQLERRM);
   end;

   for f_itens_vol in (
      select inte_wms_vol_itens.cod_barras_ean,
             inte_wms_vol_itens.nivel_sku,          inte_wms_vol_itens.grupo_sku,
             inte_wms_vol_itens.subgrupo_sku,       inte_wms_vol_itens.item_sku,
             inte_wms_vol_itens.dep_item,
             inte_wms_vol_itens.nivel_sku_kg,       inte_wms_vol_itens.grupo_sku_kg,
             inte_wms_vol_itens.subgrupo_sku_kg,    inte_wms_vol_itens.item_sku_kg,
             inte_wms_vol_itens.ref_original,
             sum(inte_wms_vol_itens.qtde_pecas) qtde_pecas,
             sum(inte_wms_vol_itens.qtde_quilos_estoque) qtde_quilos_estoque
      from  inte_wms_vol_itens
      where inte_wms_vol_itens.nr_volume_st       = :new.nr_volume_st
        and inte_wms_vol_itens.flag_integracao_st = 0
      group by inte_wms_vol_itens.cod_barras_ean,
               inte_wms_vol_itens.nivel_sku,          inte_wms_vol_itens.grupo_sku,
               inte_wms_vol_itens.subgrupo_sku,       inte_wms_vol_itens.item_sku,
               inte_wms_vol_itens.dep_item,
               inte_wms_vol_itens.nivel_sku_kg,       inte_wms_vol_itens.grupo_sku_kg,
               inte_wms_vol_itens.subgrupo_sku_kg,    inte_wms_vol_itens.item_sku_kg,
               inte_wms_vol_itens.ref_original
   )
   LOOP

      --Buscar o artigo
      begin
         select basi_030.artigo, basi_030.linha_produto
         into  v_artigo_prod,	   v_linha_prod
         from  basi_030
         where basi_030.nivel_estrutura = f_itens_vol.nivel_sku
           and basi_030.referencia      = f_itens_vol.grupo_sku;
      exception
      when no_data_found then
         v_artigo_prod := 0;
	       v_linha_prod  := 0;
      end;

      --Verificar se � Kg para pegar o campo
      if v_caracteristica_300 = 0
      then
      
         begin
            select pcpc_310.nivel,      pcpc_310.grupo,
                   pcpc_310.sub,        pcpc_310.item
            into   v_niv_kg,            v_gru_kg,
                   v_tam_kg,            v_cor_kg
            from   pcpc_310
            where  pcpc_310.cod_tipo_volume = :new.cod_tipo_vol_st
              and  pcpc_310.artigo_produto  = v_artigo_prod
              and  pcpc_310.linha_produto   = v_linha_prod
              and  pcpc_310.deposito        = f_itens_vol.dep_item;
         exception
         when no_data_found then
   
            begin
   	          
               select pcpc_310.nivel,      pcpc_310.grupo,
                      pcpc_310.sub,        pcpc_310.item
               into   v_niv_kg,            v_gru_kg,
                      v_tam_kg,            v_cor_kg
               from   pcpc_310
               where  pcpc_310.cod_tipo_volume = :new.cod_tipo_vol_st
                 and  pcpc_310.artigo_produto  = v_artigo_prod
                 and (pcpc_310.linha_produto   = v_linha_prod or
                      pcpc_310.linha_produto   = 0)
                 and  pcpc_310.deposito        = f_itens_vol.dep_item;
            exception
            when no_data_found then
               
               begin
                  
                  select pcpc_310.nivel,      pcpc_310.grupo,
                         pcpc_310.sub,        pcpc_310.item
                  into   v_niv_kg,            v_gru_kg,
                         v_tam_kg,            v_cor_kg
                  from   pcpc_310
                  where  pcpc_310.cod_tipo_volume = :new.cod_tipo_vol_st
                    and  pcpc_310.artigo_produto  = v_artigo_prod
                    and (pcpc_310.linha_produto   = v_linha_prod or
                         pcpc_310.linha_produto   = 0)
                    and (pcpc_310.deposito        = f_itens_vol.dep_item or
                         pcpc_310.deposito        = 0);
   	          exception
               when no_data_found then
   		           v_niv_kg := '';
   		           v_gru_kg := '';
   		           v_tam_kg := '';
   		           v_cor_kg := '';
               end;
   	       end;
         end;
      
      end if;  --fim do se caracteristica 0

      for f_tags_vol in (
         select inte_wms_tags.periodo_tag,      inte_wms_tags.ordem_prod_tag,
                inte_wms_tags.ordem_conf_tag,   inte_wms_tags.sequencia_tag,
                inte_wms_tags.nivel_sku,        inte_wms_tags.grupo_sku,
                inte_wms_tags.subgrupo_sku,     inte_wms_tags.item_sku,
                inte_wms_tags.nr_volume_st,     inte_wms_tags.nr_pedido,
                inte_wms_tags.flag_integracao_st
         from inte_wms_tags
         where inte_wms_tags.nr_volume_st       = :new.nr_volume_st
           and inte_wms_tags.nivel_sku          = f_itens_vol.nivel_sku
           and inte_wms_tags.grupo_sku          = f_itens_vol.grupo_sku
           and inte_wms_tags.subgrupo_sku       = f_itens_vol.subgrupo_sku
           and inte_wms_tags.item_sku           = f_itens_vol.item_sku
           and inte_wms_tags.flag_integracao_st = 0
      )
      LOOP

         --Descobrir o sequencial do item do pedido e o dep�sito
         if :new.nr_pedido = 0
         then
            v_seq_item_ped  := 0;
            v_cod_deposito  := f_itens_vol.dep_item;

            if v_caracteristica_300 = 0
            then
               v_cod_deposito  := v_cod_dep_entrada_kg;
            end if;
            
            if v_caracteristica_300 = 13
            then
               v_cod_deposito  := v_cod_dep_entrada_kit;
            end if;

            --Verificar se j� existe o registro
            v_encontrou_reg := 1;
            begin
               select 1
               into  v_encontrou_reg
               from  pcpc_325
               where pcpc_325.numero_volume         = :new.nr_volume_st
                 and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                 and pcpc_325.grupo                 = f_itens_vol.ref_original
                 and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                 and pcpc_325.item                  = f_itens_vol.item_sku
                 and pcpc_325.periodo_ordem         = f_tags_vol.periodo_tag
                 and pcpc_325.ordem_confeccao       = f_tags_vol.ordem_conf_tag
                 and pcpc_325.deposito_entrada_item = v_cod_deposito;
            exception
            when no_data_found then
               v_encontrou_reg := 0;
            end;

            if v_encontrou_reg = 0
            then

               --Verificar se � Kg para pegar o campo
               if v_caracteristica_300 = 0
               then
		 /*
                  v_nivel_estoque            := f_itens_vol.nivel_sku_kg;
                  v_grupo_estoque            := f_itens_vol.grupo_sku_kg;
                  v_sub_estoque              := f_itens_vol.subgrupo_sku_kg;
                  v_item_estoque             := f_itens_vol.item_sku_kg;*/

		              v_nivel_estoque            := v_niv_kg;
                  v_grupo_estoque            := v_gru_kg;
                  v_sub_estoque              := v_tam_kg;
                  v_item_estoque             := v_cor_kg;
                  v_dep_kg_aux	             := f_itens_vol.dep_item;
		              v_peso_aux		             := 0;

               else

                  v_nivel_estoque            := f_itens_vol.nivel_sku;
                  v_grupo_estoque            := f_itens_vol.ref_original;
                  v_sub_estoque              := f_itens_vol.subgrupo_sku;
                  v_item_estoque             := f_itens_vol.item_sku;
		              v_dep_kg_aux	             := 0;
		              v_peso_aux                 := f_itens_vol.qtde_quilos_estoque;

               end if;
               
               --Verificar se � Kit para pegar o campo
               if v_caracteristica_300 = 13
               then
                  v_dep_kg_aux := f_itens_vol.dep_item;
               end if;

               begin

                  /*gravar os �tens do volume*/
                  insert into pcpc_325
                  ( pcpc_325.numero_volume,           pcpc_325.nivel,
                    pcpc_325.grupo,                   pcpc_325.sub,
                    pcpc_325.item,                    pcpc_325.periodo_ordem,
                    pcpc_325.ordem_confeccao,         pcpc_325.qtde_pecas_real,
                    pcpc_325.seq_item_pedido,
                    pcpc_325.deposito_entrada_item,   pcpc_325.qtde_quilos_estoque,
                    pcpc_325.artigo_produto,
                    pcpc_325.nivel_estoque,           pcpc_325.grupo_estoque,
                    pcpc_325.sub_estoque,             pcpc_325.item_estoque,
                    pcpc_325.deposito_saida_item
                  )
                  values
                  ( :new.nr_volume_st,                f_itens_vol.nivel_sku,
                    f_itens_vol.ref_original,         f_itens_vol.subgrupo_sku,
                    f_itens_vol.item_sku,             f_tags_vol.periodo_tag,
                    f_tags_vol.ordem_conf_tag,        1,
                    v_seq_item_ped,
                    v_cod_deposito,                   v_peso_aux,
                    v_artigo_prod,
                    v_nivel_estoque,                  v_grupo_estoque,
                    v_sub_estoque,                    v_item_estoque,
                    v_dep_kg_aux
                  );
               exception
               when others then
                  raise_application_error(-20000, 'N�o inseriu na tabela PCPC_325.' || Chr(10) || SQLERRM);
               end;

            else

               begin

                  /*gravar os �tens do volume*/
                  update pcpc_325
                  set pcpc_325.qtde_pecas_real         = pcpc_325.qtde_pecas_real + 1,
                      pcpc_325.qtde_quilos_estoque     = pcpc_325.qtde_quilos_estoque + (v_peso_aux)
                  where pcpc_325.numero_volume         = :new.nr_volume_st
                    and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                    and pcpc_325.grupo                 = f_itens_vol.ref_original
                    and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                    and pcpc_325.item                  = f_itens_vol.item_sku
                    and pcpc_325.periodo_ordem         = f_tags_vol.periodo_tag
                    and pcpc_325.ordem_confeccao       = f_tags_vol.ordem_conf_tag
                    and pcpc_325.deposito_entrada_item = v_cod_deposito;
               exception
               when others then
                  raise_application_error(-20000, 'N�o atualizou a tabela PCPC_325.' || Chr(10) || SQLERRM);
               end;

            end if;

            --Atualizar a TAG do WMS como Integrada
            begin

               update inte_wms_tags
               set   inte_wms_tags.flag_integracao_st = 1
               where inte_wms_tags.nr_volume_st       = :new.nr_volume_st
                 and inte_wms_tags.nivel_sku          = f_itens_vol.nivel_sku
                 and inte_wms_tags.grupo_sku          = f_itens_vol.grupo_sku
                 and inte_wms_tags.subgrupo_sku       = f_itens_vol.subgrupo_sku
                 and inte_wms_tags.item_sku           = f_itens_vol.item_sku
                 and inte_wms_tags.flag_integracao_st = 0
                 and inte_wms_tags.periodo_tag        = f_tags_vol.periodo_tag
                 and inte_wms_tags.ordem_prod_tag     = f_tags_vol.ordem_prod_tag
                 and inte_wms_tags.ordem_conf_tag     = f_tags_vol.ordem_conf_tag
                 and inte_wms_tags.sequencia_tag      = f_tags_vol.sequencia_tag;

            exception
            when others then
               raise_application_error(-20000, 'N�o atualizou a tabela INTE_WMS_TAGS.' || Chr(10) || SQLERRM);
            end;

            begin
               select basi_010.preco_medio
               into v_preco_medio_010
               from basi_010
               where nivel_estrutura  = f_itens_vol.nivel_sku
                 and grupo_estrutura  = f_itens_vol.grupo_sku
                 and subgru_estrutura = f_itens_vol.subgrupo_sku
                 and item_estrutura   = f_itens_vol.item_sku;
            exception
            when no_data_found then
               v_preco_medio_010 := 0.00;
            end;

            --Atualizar a TAG do ST

            --SE FOR KIT OU KG
            if v_caracteristica_300 = 13 or v_caracteristica_300 = 0
            then

               begin
                  update pcpc_330
                  set  nr_volume             = :new.nr_volume_st,
                       pedido_venda          = :new.nr_pedido,
                       seq_item_pedido       = v_seq_item_ped,
                       usuario_exped         = :new.usuario_exped,
                       flag_controle         = 1,
                       estoque_tag           = 3,
                       nome_prog_050         = :new.nome_programa,
                       cod_caixa_rfid        = null,

                       transacao_cardex      = v_trans_saida_tipo_vol,
                       data_cardex           = trunc(:new.timestamp_fim_mont),
                       usuario_cardex        = :new.usuario_exped,
                       valor_movto_cardex    = v_preco_medio_010,
                       valor_contabil_cardex = v_preco_medio_010
                  where pcpc_330.periodo_producao  = f_tags_vol.periodo_tag
                    and (pcpc_330.ordem_producao   = f_tags_vol.ordem_prod_tag
                      or f_tags_vol.ordem_prod_tag = 0)
                    and pcpc_330.ordem_confeccao   = f_tags_vol.ordem_conf_tag
                    and pcpc_330.sequencia         = f_tags_vol.sequencia_tag;
               exception
               when others then
                  raise_application_error(-20000, 'N�o atualizou a tabela PCPC_330.' || Chr(10) || SQLERRM);
               end;

            else

               begin
                  update pcpc_330
                  set  nr_volume             = :new.nr_volume_st,
                       pedido_venda          = :new.nr_pedido,
                       seq_item_pedido       = v_seq_item_ped,
                       usuario_exped         = :new.usuario_exped,
                       flag_controle         = 1,
                       estoque_tag           = 3,
                       nome_prog_050         = :new.nome_programa,
                       cod_caixa_rfid        = null
                  where pcpc_330.periodo_producao  = f_tags_vol.periodo_tag
                    and (pcpc_330.ordem_producao   = f_tags_vol.ordem_prod_tag
                      or f_tags_vol.ordem_prod_tag = 0)
                    and pcpc_330.ordem_confeccao   = f_tags_vol.ordem_conf_tag
                    and pcpc_330.sequencia         = f_tags_vol.sequencia_tag;
               exception
               when others then
                  raise_application_error(-20000, 'N�o atualizou a tabela PCPC_330.' || Chr(10) || SQLERRM);
               end;

            end if;

         else

            --Verificar a quantidade de itens com mesmo produto
            begin
               select count(1)
               into  v_retorno_int
               from  pedi_110
               where pedi_110.pedido_venda      = :new.nr_pedido
                 and pedi_110.cd_it_pe_nivel99  = f_itens_vol.nivel_sku
                 and pedi_110.cd_it_pe_grupo    = f_itens_vol.ref_original
                 and pedi_110.cd_it_pe_subgrupo = f_itens_vol.subgrupo_sku
                 and pedi_110.cd_it_pe_item     = f_itens_vol.item_sku
                 and pedi_110.cod_cancelamento  = 0;
            exception
            when no_data_found then
               v_retorno_int := 0;
            end;

            --Existe apenas 1 item com o mesmo produto
            if v_retorno_int = 1
            then

               begin

                  --Buscar o sequencial do item e o deposito
                  select pedi_110.seq_item_pedido,      pedi_110.codigo_deposito
                  into  v_seq_item_ped,                 v_cod_deposito
                  from  pedi_110
                  where pedi_110.pedido_venda      = :new.nr_pedido
                    and pedi_110.cd_it_pe_nivel99  = f_itens_vol.nivel_sku
                    and pedi_110.cd_it_pe_grupo    = f_itens_vol.ref_original
                    and pedi_110.cd_it_pe_subgrupo = f_itens_vol.subgrupo_sku
                    and pedi_110.cd_it_pe_item     = f_itens_vol.item_sku
                    and pedi_110.cod_cancelamento  = 0;
               exception
               when no_data_found then
                  v_retorno_int  := 0;
                  v_seq_item_ped := 0;
                  v_cod_deposito := 0;
               end;

               --Verificar se j� existe o registro
               v_encontrou_reg := 1;
               begin
                  select 1
                  into  v_encontrou_reg
                  from  pcpc_325
                  where pcpc_325.numero_volume         = :new.nr_volume_st
                    and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                    and pcpc_325.grupo                 = f_itens_vol.ref_original
                    and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                    and pcpc_325.item                  = f_itens_vol.item_sku
                    and pcpc_325.periodo_ordem         = f_tags_vol.periodo_tag
                    and pcpc_325.ordem_confeccao       = f_tags_vol.ordem_conf_tag
                    and pcpc_325.deposito_entrada_item = v_cod_deposito;
               exception
               when no_data_found then
                  v_encontrou_reg := 0;
               end;

               if v_encontrou_reg = 0
               then

                  --Verificar se � Kg para pegar o campo
                  if v_caracteristica_300 = 0
                  then

                     /*
					           v_nivel_estoque            := f_itens_vol.nivel_sku_kg;
					           v_grupo_estoque            := f_itens_vol.grupo_sku_kg;
					           v_sub_estoque              := f_itens_vol.subgrupo_sku_kg;
					           v_item_estoque             := f_itens_vol.item_sku_kg;*/

					           v_nivel_estoque            := v_niv_kg;
					           v_grupo_estoque            := v_gru_kg;
					           v_sub_estoque              := v_tam_kg;
					           v_item_estoque             := v_cor_kg;

                  else

                     v_nivel_estoque            := f_itens_vol.nivel_sku;
                     v_grupo_estoque            := f_itens_vol.ref_original;
                     v_sub_estoque              := f_itens_vol.subgrupo_sku;
                     v_item_estoque             := f_itens_vol.item_sku;

                  end if;

                  begin

                     /*gravar os �tens do volume*/
                     insert into pcpc_325
                     ( pcpc_325.numero_volume,           pcpc_325.nivel,
                       pcpc_325.grupo,                   pcpc_325.sub,
                       pcpc_325.item,                    pcpc_325.periodo_ordem,
                       pcpc_325.ordem_confeccao,         pcpc_325.qtde_pecas_real,
                       pcpc_325.seq_item_pedido,
                       pcpc_325.deposito_entrada_item,   pcpc_325.qtde_quilos_estoque,
                       pcpc_325.artigo_produto,
                       pcpc_325.nivel_estoque,           pcpc_325.grupo_estoque,
                       pcpc_325.sub_estoque,             pcpc_325.item_estoque
                     )
                     values
                     ( :new.nr_volume_st,                f_itens_vol.nivel_sku,
                       f_itens_vol.ref_original,         f_itens_vol.subgrupo_sku,
                       f_itens_vol.item_sku,             f_tags_vol.periodo_tag,
                       f_tags_vol.ordem_conf_tag,        1,
                       v_seq_item_ped,
                       v_cod_deposito,                   f_itens_vol.qtde_quilos_estoque / f_itens_vol.qtde_pecas,
                       v_artigo_prod,
                       v_nivel_estoque,                  v_grupo_estoque,
                       v_sub_estoque,                    v_item_estoque
                     );
                  exception
                  when others then
                     raise_application_error(-20000, 'N�o inseriu na tabela PCPC_325.' || Chr(10) || SQLERRM);
                  end;

               else

                  begin

                     /*gravar os �tens do volume*/
                     update pcpc_325
                     set pcpc_325.qtde_pecas_real         = pcpc_325.qtde_pecas_real + 1,
                         pcpc_325.qtde_quilos_estoque     = pcpc_325.qtde_quilos_estoque + (f_itens_vol.qtde_quilos_estoque / f_itens_vol.qtde_pecas)
                     where pcpc_325.numero_volume         = :new.nr_volume_st
                       and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                       and pcpc_325.grupo                 = f_itens_vol.ref_original
                       and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                       and pcpc_325.item                  = f_itens_vol.item_sku
                       and pcpc_325.periodo_ordem         = f_tags_vol.periodo_tag
                       and pcpc_325.ordem_confeccao       = f_tags_vol.ordem_conf_tag
                       and pcpc_325.deposito_entrada_item = v_cod_deposito;
                  exception
                  when others then
                     raise_application_error(-20000, 'N�o atualizou a tabela PCPC_325.' || Chr(10) || SQLERRM);
                  end;

               end if;

               --Atualizar a TAG do WMS como Integrada
               begin

                  update inte_wms_tags
                  set   inte_wms_tags.flag_integracao_st = 1
                  where inte_wms_tags.nr_volume_st       = :new.nr_volume_st
                    and inte_wms_tags.nivel_sku          = f_itens_vol.nivel_sku
                    and inte_wms_tags.grupo_sku          = f_itens_vol.grupo_sku
                    and inte_wms_tags.subgrupo_sku       = f_itens_vol.subgrupo_sku
                    and inte_wms_tags.item_sku           = f_itens_vol.item_sku
                    and inte_wms_tags.flag_integracao_st = 0
                    and inte_wms_tags.periodo_tag        = f_tags_vol.periodo_tag
                    and inte_wms_tags.ordem_prod_tag     = f_tags_vol.ordem_prod_tag
                    and inte_wms_tags.ordem_conf_tag     = f_tags_vol.ordem_conf_tag
                    and inte_wms_tags.sequencia_tag      = f_tags_vol.sequencia_tag;

               exception
               when others then
                  raise_application_error(-20000, 'N�o atualizou a tabela INTE_WMS_TAGS.' || Chr(10) || SQLERRM);
               end;

               begin
                  select basi_010.preco_medio
                  into v_preco_medio_010
                  from basi_010
                  where nivel_estrutura  = f_itens_vol.nivel_sku
                    and grupo_estrutura  = f_itens_vol.grupo_sku
                    and subgru_estrutura = f_itens_vol.subgrupo_sku
                    and item_estrutura   = f_itens_vol.item_sku;
               exception
               when no_data_found then
                  v_preco_medio_010 := 0.00;
               end;

               --Atualizar a TAG do ST

               --SE FOR KIT OU KG
               if v_caracteristica_300 = 13 or v_caracteristica_300 = 0
               then

                  begin
                     update pcpc_330
                     set  nr_volume             = :new.nr_volume_st,
                          pedido_venda          = :new.nr_pedido,
                          seq_item_pedido       = v_seq_item_ped,
                          usuario_exped         = :new.usuario_exped,
                          flag_controle         = 1,
                          estoque_tag           = 3,
                          nome_prog_050         = :new.nome_programa,
                          cod_caixa_rfid        = null,

                          transacao_cardex      = v_trans_saida_tipo_vol,
                          data_cardex           = trunc(:new.timestamp_fim_mont),
                          usuario_cardex        = :new.usuario_exped,
                          valor_movto_cardex    = v_preco_medio_010,
                          valor_contabil_cardex = v_preco_medio_010
                     where pcpc_330.periodo_producao  = f_tags_vol.periodo_tag
                       and (pcpc_330.ordem_producao   = f_tags_vol.ordem_prod_tag
                         or f_tags_vol.ordem_prod_tag = 0)
                       and pcpc_330.ordem_confeccao   = f_tags_vol.ordem_conf_tag
                       and pcpc_330.sequencia         = f_tags_vol.sequencia_tag;
                  exception
                  when others then
                     raise_application_error(-20000, 'N�o atualizou a tabela PCPC_330.' || Chr(10) || SQLERRM);
                  end;

               else

                  begin
                     update pcpc_330
                     set  nr_volume       = :new.nr_volume_st,
                          pedido_venda    = :new.nr_pedido,
                          seq_item_pedido = v_seq_item_ped,
                          usuario_exped   = :new.usuario_exped,
                          flag_controle   = 1,
                          estoque_tag     = 3,
                          nome_prog_050   = :new.nome_programa,
                          cod_caixa_rfid  = null
                     where pcpc_330.periodo_producao  = f_tags_vol.periodo_tag
                       and (pcpc_330.ordem_producao   = f_tags_vol.ordem_prod_tag
                         or f_tags_vol.ordem_prod_tag = 0)
                       and pcpc_330.ordem_confeccao   = f_tags_vol.ordem_conf_tag
                       and pcpc_330.sequencia         = f_tags_vol.sequencia_tag;
                  exception
                  when others then
                     raise_application_error(-20000, 'N�o atualizou a tabela PCPC_330.' || Chr(10) || SQLERRM);
                  end;

               end if;

            else --Tem mais de 1 item para o mesmo produto, descobrir qual usar

               --Vari�vel com o saldo a inserir no item do volume
               v_qtde_inserir_325 := 1;

               --Fazer loop nos �tens do pedido com o mesmo produto, para cada item, verificar saldo a montar
               for f_itens_ped in (
                  select pedi_110.seq_item_pedido,        pedi_110.codigo_deposito,
                         pedi_110.qtde_pedida,            pedi_110.qtde_faturada,
                         pedi_110.qtde_afaturar
                  from  pedi_110
                  where pedi_110.pedido_venda        = :new.nr_pedido
                    and pedi_110.cd_it_pe_nivel99    = f_itens_vol.nivel_sku
                    and pedi_110.cd_it_pe_grupo      = f_itens_vol.ref_original
                    and pedi_110.cd_it_pe_subgrupo   = f_itens_vol.subgrupo_sku
                    and pedi_110.cd_it_pe_item       = f_itens_vol.item_sku
                    and pedi_110.cod_cancelamento    = 0
               )
               LOOP

                  --Buscar quantidade j� associada para este item deste pedido (volumes)
                  begin
                     select nvl(sum(pcpc_325.qtde_pecas_real), 0)
                     into   v_qtd_tot_vol
                     from  pcpc_325, pcpc_320
                     where pcpc_325.numero_volume     = pcpc_320.numero_volume
                       and pcpc_320.pedido_venda      = :new.nr_pedido
                       and pcpc_325.seq_item_pedido   = f_itens_ped.seq_item_pedido;
                  exception
                  when no_data_found then
                     v_qtd_tot_vol  := 0;
                  end;

                  --Calcular o saldo
                  v_qtd_saldo_item := f_itens_ped.qtde_pedida - v_qtd_tot_vol;

                  --Verificar o saldo
                  if v_qtd_saldo_item > 0
                  then

                     if v_qtd_saldo_item >= v_qtde_inserir_325
                     then

                        --Existe saldo suficiente nesta sequencia de pedido para o item do volume
                        --INSERIR ESTE ITEM COM A QUANTIDADE DO VOLUME DA INTEGRA��O

                        --Verificar se j� existe o registro
                        v_encontrou_reg := 1;
                        begin
                           select 1
                           into  v_encontrou_reg
                           from  pcpc_325
                           where pcpc_325.numero_volume         = :new.nr_volume_st
                             and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                             and pcpc_325.grupo                 = f_itens_vol.ref_original
                             and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                             and pcpc_325.item                  = f_itens_vol.item_sku
                             and pcpc_325.periodo_ordem         = f_tags_vol.periodo_tag
                             and pcpc_325.ordem_confeccao       = f_tags_vol.ordem_conf_tag
                             and pcpc_325.deposito_entrada_item = f_itens_ped.codigo_deposito;
                        exception
                        when no_data_found then
                           v_encontrou_reg := 0;
                        end;

                        if v_encontrou_reg = 0
                        then

                           --Verificar se � Kg para pegar o campo
                           if v_caracteristica_300 = 0
                           then
                           
                              /*
							                v_nivel_estoque            := f_itens_vol.nivel_sku_kg;
							                v_grupo_estoque            := f_itens_vol.grupo_sku_kg;
							                v_sub_estoque              := f_itens_vol.subgrupo_sku_kg;
							                v_item_estoque             := f_itens_vol.item_sku_kg;*/

							                v_nivel_estoque            := v_niv_kg;
							                v_grupo_estoque            := v_gru_kg;
							                v_sub_estoque              := v_tam_kg;
							                v_item_estoque             := v_cor_kg;
                              
                           else

                              v_nivel_estoque            := f_itens_vol.nivel_sku;
                              v_grupo_estoque            := f_itens_vol.ref_original;
                              v_sub_estoque              := f_itens_vol.subgrupo_sku;
                              v_item_estoque             := f_itens_vol.item_sku;

                           end if;

                           begin

                              /*gravar os �tens do volume*/
                              insert into pcpc_325
                              ( pcpc_325.numero_volume,           pcpc_325.nivel,
                                pcpc_325.grupo,                   pcpc_325.sub,
                                pcpc_325.item,                    pcpc_325.periodo_ordem,
                                pcpc_325.ordem_confeccao,         pcpc_325.qtde_pecas_real,
                                pcpc_325.seq_item_pedido,
                                pcpc_325.deposito_entrada_item,   pcpc_325.qtde_quilos_estoque,
                                pcpc_325.artigo_produto,
                                pcpc_325.nivel_estoque,           pcpc_325.grupo_estoque,
                                pcpc_325.sub_estoque,             pcpc_325.item_estoque
                              )
                              values
                              ( :new.nr_volume_st,                f_itens_vol.nivel_sku,
                                f_itens_vol.ref_original,         f_itens_vol.subgrupo_sku,
                                f_itens_vol.item_sku,             f_tags_vol.periodo_tag,
                                f_tags_vol.ordem_conf_tag,        v_qtde_inserir_325,
                                f_itens_ped.seq_item_pedido,
                                f_itens_ped.codigo_deposito,      (v_qtde_inserir_325 * f_itens_vol.qtde_quilos_estoque / f_itens_vol.qtde_pecas),
                                v_artigo_prod,
                                v_nivel_estoque,                  v_grupo_estoque,
                                v_sub_estoque,                    v_item_estoque
                              );
                           exception
                           when others then
                              raise_application_error(-20000, 'N�o inseriu na tabela PCPC_325.' || Chr(10) || SQLERRM);
                           end;

                        else

                           begin

                              /*gravar os �tens do volume*/
                              update pcpc_325
                              set pcpc_325.qtde_pecas_real         = pcpc_325.qtde_pecas_real + v_qtde_inserir_325,
                                  pcpc_325.qtde_quilos_estoque     = pcpc_325.qtde_quilos_estoque + (v_qtde_inserir_325 * f_itens_vol.qtde_quilos_estoque / f_itens_vol.qtde_pecas)
                              where pcpc_325.numero_volume         = :new.nr_volume_st
                                and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                                and pcpc_325.grupo                 = f_itens_vol.ref_original
                                and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                                and pcpc_325.item                  = f_itens_vol.item_sku
                                and pcpc_325.periodo_ordem         = f_tags_vol.periodo_tag
                                and pcpc_325.ordem_confeccao       = f_tags_vol.ordem_conf_tag
                                and pcpc_325.deposito_entrada_item = f_itens_ped.codigo_deposito;
                           exception
                           when others then
                              raise_application_error(-20000, 'N�o atualizou a tabela PCPC_325.' || Chr(10) || SQLERRM);
                           end;

                        end if;

                        --Atualizar a TAG do WMS como Integrada
                        begin

                           update inte_wms_tags
                           set   inte_wms_tags.flag_integracao_st = 1
                           where inte_wms_tags.nr_volume_st       = :new.nr_volume_st
                             and inte_wms_tags.nivel_sku          = f_itens_vol.nivel_sku
                             and inte_wms_tags.grupo_sku          = f_itens_vol.grupo_sku
                             and inte_wms_tags.subgrupo_sku       = f_itens_vol.subgrupo_sku
                             and inte_wms_tags.item_sku           = f_itens_vol.item_sku
                             and inte_wms_tags.flag_integracao_st = 0
                             and inte_wms_tags.periodo_tag        = f_tags_vol.periodo_tag
                             and inte_wms_tags.ordem_prod_tag     = f_tags_vol.ordem_prod_tag
                             and inte_wms_tags.ordem_conf_tag     = f_tags_vol.ordem_conf_tag
                             and inte_wms_tags.sequencia_tag      = f_tags_vol.sequencia_tag;

                        exception
                        when others then
                           raise_application_error(-20000, 'N�o atualizou a tabela INTE_WMS_TAGS.' || Chr(10) || SQLERRM);
                        end;

                        begin
                           select basi_010.preco_medio
                           into v_preco_medio_010
                           from basi_010
                           where nivel_estrutura  = f_itens_vol.nivel_sku
                             and grupo_estrutura  = f_itens_vol.grupo_sku
                             and subgru_estrutura = f_itens_vol.subgrupo_sku
                             and item_estrutura   = f_itens_vol.item_sku;
                        exception
                        when no_data_found then
                           v_preco_medio_010 := 0.00;
                        end;

                        --Atualizar a TAG do ST

                        --SE FOR KIT OU KG:
                        if v_caracteristica_300 = 13 or v_caracteristica_300 = 0
                        then

                           begin
                              update pcpc_330
                              set  nr_volume             = :new.nr_volume_st,
                                   pedido_venda          = :new.nr_pedido,
                                   seq_item_pedido       = f_itens_ped.seq_item_pedido,
                                   usuario_exped         = :new.usuario_exped,
                                   flag_controle         = 1,
                                   estoque_tag           = 3,
                                   nome_prog_050         = :new.nome_programa,
                                   cod_caixa_rfid        = null,

                                   transacao_cardex      = v_trans_saida_tipo_vol,
                                   data_cardex           = trunc(:new.timestamp_fim_mont),
                                   usuario_cardex        = :new.usuario_exped,
                                   valor_movto_cardex    = v_preco_medio_010,
                                   valor_contabil_cardex = v_preco_medio_010
                              where pcpc_330.periodo_producao  = f_tags_vol.periodo_tag
                                and (pcpc_330.ordem_producao   = f_tags_vol.ordem_prod_tag
                                  or f_tags_vol.ordem_prod_tag = 0)
                                and pcpc_330.ordem_confeccao   = f_tags_vol.ordem_conf_tag
                                and pcpc_330.sequencia         = f_tags_vol.sequencia_tag;
                           exception
                           when others then
                              raise_application_error(-20000, 'N�o atualizou a tabela PCPC_330.' || Chr(10) || SQLERRM);
                           end;

                        else

                           begin
                              update pcpc_330
                              set  nr_volume             = :new.nr_volume_st,
                                   pedido_venda          = :new.nr_pedido,
                                   seq_item_pedido       = f_itens_ped.seq_item_pedido,
                                   usuario_exped         = :new.usuario_exped,
                                   flag_controle         = 1,
                                   estoque_tag           = 3,
                                   nome_prog_050         = :new.nome_programa,
                                   cod_caixa_rfid        = null
                              where pcpc_330.periodo_producao  = f_tags_vol.periodo_tag
                                and (pcpc_330.ordem_producao   = f_tags_vol.ordem_prod_tag
                                  or f_tags_vol.ordem_prod_tag = 0)
                                and pcpc_330.ordem_confeccao   = f_tags_vol.ordem_conf_tag
                                and pcpc_330.sequencia         = f_tags_vol.sequencia_tag;
                           exception
                           when others then
                              raise_application_error(-20000, 'N�o atualizou a tabela PCPC_330.' || Chr(10) || SQLERRM);
                           end;

                        end if;

                        v_qtde_inserir_325 := v_qtde_inserir_325 - v_qtde_inserir_325;
                        EXIT WHEN 1 = 1;  --Sair do Loop

                     else

                        v_qtde_inserir_325 := v_qtde_inserir_325 - v_qtd_saldo_item;

                        --N�o existe saldo suficiente nesta sequencia de pedido para o item do volume
                        --INSERIR O SALDO DESTE ITEM E CONTINUAR BUSCANDO PR�XIMO ITEM COM SALDO


                        --Verificar se j� existe o registro
                        v_encontrou_reg := 1;
                        begin
                           select 1
                           into  v_encontrou_reg
                           from  pcpc_325
                           where pcpc_325.numero_volume         = :new.nr_volume_st
                             and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                             and pcpc_325.grupo                 = f_itens_vol.ref_original
                             and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                             and pcpc_325.item                  = f_itens_vol.item_sku
                             and pcpc_325.periodo_ordem         = f_tags_vol.periodo_tag
                             and pcpc_325.ordem_confeccao       = f_tags_vol.ordem_conf_tag
                             and pcpc_325.deposito_entrada_item = f_itens_ped.codigo_deposito;
                        exception
                        when no_data_found then
                           v_encontrou_reg := 0;
                        end;

                        if v_encontrou_reg = 0
                        then

                           --Verificar se � Kg para pegar o campo
                           if v_caracteristica_300 = 0
                           then

                              /*
							                v_nivel_estoque            := f_itens_vol.nivel_sku_kg;
							                v_grupo_estoque            := f_itens_vol.grupo_sku_kg;
							                v_sub_estoque              := f_itens_vol.subgrupo_sku_kg;
							                v_item_estoque             := f_itens_vol.item_sku_kg;*/

							                v_nivel_estoque            := v_niv_kg;
							                v_grupo_estoque            := v_gru_kg;
							                v_sub_estoque              := v_tam_kg;
							                v_item_estoque             := v_cor_kg;

                           else

                              v_nivel_estoque            := f_itens_vol.nivel_sku;
                              v_grupo_estoque            := f_itens_vol.ref_original;
                              v_sub_estoque              := f_itens_vol.subgrupo_sku;
                              v_item_estoque             := f_itens_vol.item_sku;

                           end if;

                           begin

                              /*gravar os �tens do volume*/
                              insert into pcpc_325
                              ( pcpc_325.numero_volume,           pcpc_325.nivel,
                                pcpc_325.grupo,                   pcpc_325.sub,
                                pcpc_325.item,                    pcpc_325.periodo_ordem,
                                pcpc_325.ordem_confeccao,         pcpc_325.qtde_pecas_real,
                                pcpc_325.seq_item_pedido,
                                pcpc_325.deposito_entrada_item,   pcpc_325.qtde_quilos_estoque,
                                pcpc_325.artigo_produto,
                                pcpc_325.nivel_estoque,           pcpc_325.grupo_estoque,
                                pcpc_325.sub_estoque,             pcpc_325.item_estoque
                              )
                              values
                              ( :new.nr_volume_st,                f_itens_vol.nivel_sku,
                                f_itens_vol.ref_original,         f_itens_vol.subgrupo_sku,
                                f_itens_vol.item_sku,             f_tags_vol.periodo_tag,
                                f_tags_vol.ordem_conf_tag,        v_qtd_saldo_item,
                                f_itens_ped.seq_item_pedido,
                                f_itens_ped.codigo_deposito,      (v_qtde_inserir_325 * f_itens_vol.qtde_quilos_estoque / f_itens_vol.qtde_pecas),
                                v_artigo_prod,
                                v_nivel_estoque,                  v_grupo_estoque,
                                v_sub_estoque,                    v_item_estoque
                              );
                           exception
                           when others then
                              raise_application_error(-20000, 'N�o inseriu na tabela PCPC_325.' || Chr(10) || SQLERRM);
                           end;

                        else

                           begin

                              /*gravar os �tens do volume*/
                              update pcpc_325
                              set pcpc_325.qtde_pecas_real         = pcpc_325.qtde_pecas_real + v_qtd_saldo_item,
                                  pcpc_325.qtde_quilos_estoque     = pcpc_325.qtde_quilos_estoque + (v_qtd_saldo_item * f_itens_vol.qtde_quilos_estoque / f_itens_vol.qtde_pecas)
                              where pcpc_325.numero_volume         = :new.nr_volume_st
                                and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                                and pcpc_325.grupo                 = f_itens_vol.ref_original
                                and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                                and pcpc_325.item                  = f_itens_vol.item_sku
                                and pcpc_325.periodo_ordem         = f_tags_vol.periodo_tag
                                and pcpc_325.ordem_confeccao       = f_tags_vol.ordem_conf_tag
                                and pcpc_325.deposito_entrada_item = f_itens_ped.codigo_deposito;
                           exception
                           when others then
                              raise_application_error(-20000, 'N�o atualizou a tabela PCPC_325.' || Chr(10) || SQLERRM);
                           end;

                        end if;

                        --Atualizar a TAG do WMS como Integrada
                        begin

                           update inte_wms_tags
                           set   inte_wms_tags.flag_integracao_st = 1
                           where inte_wms_tags.nr_volume_st       = :new.nr_volume_st
                             and inte_wms_tags.nivel_sku          = f_itens_vol.nivel_sku
                             and inte_wms_tags.grupo_sku          = f_itens_vol.grupo_sku
                             and inte_wms_tags.subgrupo_sku       = f_itens_vol.subgrupo_sku
                             and inte_wms_tags.item_sku           = f_itens_vol.item_sku
                             and inte_wms_tags.flag_integracao_st = 0
                             and inte_wms_tags.periodo_tag        = f_tags_vol.periodo_tag
                             and inte_wms_tags.ordem_prod_tag     = f_tags_vol.ordem_prod_tag
                             and inte_wms_tags.ordem_conf_tag     = f_tags_vol.ordem_conf_tag
                             and inte_wms_tags.sequencia_tag      = f_tags_vol.sequencia_tag;

                        exception
                        when others then
                           raise_application_error(-20000, 'N�o atualizou a tabela INTE_WMS_TAGS.' || Chr(10) || SQLERRM);
                        end;

                        begin
                           select basi_010.preco_medio
                           into v_preco_medio_010
                           from basi_010
                           where nivel_estrutura  = f_itens_vol.nivel_sku
                             and grupo_estrutura  = f_itens_vol.grupo_sku
                             and subgru_estrutura = f_itens_vol.subgrupo_sku
                             and item_estrutura   = f_itens_vol.item_sku;
                        exception
                        when no_data_found then
                           v_preco_medio_010 := 0.00;
                        end;

                        --Atualizar a TAG do ST

                        --SE FOR KIT OU KG
                        if v_caracteristica_300 = 13 or v_caracteristica_300 = 0
                        then

                           begin
                              update pcpc_330
                              set  nr_volume             = :new.nr_volume_st,
                                   pedido_venda          = :new.nr_pedido,
                                   seq_item_pedido       = f_itens_ped.seq_item_pedido,
                                   usuario_exped         = :new.usuario_exped,
                                   flag_controle         = 1,
                                   estoque_tag           = 3,
                                   nome_prog_050         = :new.nome_programa,
                                   cod_caixa_rfid        = null,

                                   transacao_cardex      = v_trans_saida_tipo_vol,
                                   data_cardex           = trunc(:new.timestamp_fim_mont),
                                   usuario_cardex        = :new.usuario_exped,
                                   valor_movto_cardex    = v_preco_medio_010,
                                   valor_contabil_cardex = v_preco_medio_010
                              where pcpc_330.periodo_producao  = f_tags_vol.periodo_tag
                                and (pcpc_330.ordem_producao   = f_tags_vol.ordem_prod_tag
                                  or f_tags_vol.ordem_prod_tag = 0)
                                and pcpc_330.ordem_confeccao   = f_tags_vol.ordem_conf_tag
                                and pcpc_330.sequencia         = f_tags_vol.sequencia_tag;
                           exception
                           when others then
                              raise_application_error(-20000, 'N�o atualizou a tabela PCPC_330.' || Chr(10) || SQLERRM);
                           end;

                        else

                           begin
                              update pcpc_330
                              set  nr_volume             = :new.nr_volume_st,
                                   pedido_venda          = :new.nr_pedido,
                                   seq_item_pedido       = f_itens_ped.seq_item_pedido,
                                   usuario_exped         = :new.usuario_exped,
                                   flag_controle         = 1,
                                   estoque_tag           = 3,
                                   nome_prog_050         = :new.nome_programa,
                                   cod_caixa_rfid        = null
                              where pcpc_330.periodo_producao  = f_tags_vol.periodo_tag
                                and (pcpc_330.ordem_producao   = f_tags_vol.ordem_prod_tag
                                  or f_tags_vol.ordem_prod_tag = 0)
                                and pcpc_330.ordem_confeccao   = f_tags_vol.ordem_conf_tag
                                and pcpc_330.sequencia         = f_tags_vol.sequencia_tag;
                           exception
                           when others then
                              raise_application_error(-20000, 'N�o atualizou a tabela PCPC_330.' || Chr(10) || SQLERRM);
                           end;

                        end if;

                     end if;

                  end if;

               END LOOP;  --fim do la�o dos itens do pedido

            end if;

         end if;

      END LOOP;  --fim do la�o das tags

      --Verificar se foi atendida toda a quantidade do item do volume para atualizar a flag importa��o ST

      --Buscar quantidade da 325 para este volume, item e deposito

      --Se n�o tiver pedido, busca o dep�sito da propria inte_wms_vol_itens
      if :new.nr_pedido = 0
      then
		     
         if v_caracteristica_300 = 0
         then
			      v_dep_kg_aux := v_cod_dep_entrada_kg;
         else
            v_dep_kg_aux := f_itens_vol.dep_item;
		     end if;
         
         if v_caracteristica_300 = 13
         then
            v_dep_kg_aux  := v_cod_dep_entrada_kit;
         end if;

		     begin
            select nvl(sum(pcpc_325.qtde_pecas_real), 0)
            into  v_qtde_inserida_325
            from  pcpc_325
            where pcpc_325.numero_volume         = :new.nr_volume_st
              and pcpc_325.nivel                 = f_itens_vol.nivel_sku
              and pcpc_325.grupo                 = f_itens_vol.ref_original
              and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
              and pcpc_325.item                  = f_itens_vol.item_sku
              and pcpc_325.deposito_entrada_item = v_dep_kg_aux;
         exception
         when no_data_found then
            v_qtde_inserida_325 := 0;
         end;

      else
         --Se tiver pedido, busca o dep�sito do item (pedi_110)

         begin
            select nvl(sum(pcpc_325.qtde_pecas_real), 0)
            into  v_qtde_inserida_325
            from  pcpc_325, pedi_110
            where pcpc_325.numero_volume         = :new.nr_volume_st
              and pcpc_325.nivel                 = f_itens_vol.nivel_sku
              and pcpc_325.grupo                 = f_itens_vol.ref_original
              and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
              and pcpc_325.item                  = f_itens_vol.item_sku
              and pedi_110.pedido_venda          = :new.nr_pedido
              and pedi_110.seq_item_pedido       = pcpc_325.seq_item_pedido
              and pcpc_325.deposito_entrada_item = pedi_110.codigo_deposito;
         exception
         when no_data_found then
            v_qtde_inserida_325 := 0;
         end;

      end if;

      --Verificar se a quantidade deste produto e dep�sito (inte_wms_vol_itens) � maior ou igual ao da 325.
      --Se for, marca como integrado
      --Se n�o for, nao faz nada
      if  v_qtde_inserida_325 >= f_itens_vol.qtde_pecas
      then

         --Atualizar a Item do Volume do WMS como Integrado
         begin
            update inte_wms_vol_itens
            set   inte_wms_vol_itens.flag_integracao_st = 1
            where inte_wms_vol_itens.nr_volume_st       = :new.nr_volume_st
              and inte_wms_vol_itens.nivel_sku          = f_itens_vol.nivel_sku
              and inte_wms_vol_itens.grupo_sku          = f_itens_vol.grupo_sku
              and inte_wms_vol_itens.subgrupo_sku       = f_itens_vol.subgrupo_sku
              and inte_wms_vol_itens.item_sku           = f_itens_vol.item_sku
              and inte_wms_vol_itens.flag_integracao_st = 0;
         exception
         when others then
            raise_application_error(-20000, 'N�o atualizou a tabela INTE_WMS_VOL_ITENS.' || Chr(10) || SQLERRM);
         end;

      end if;

   END LOOP;  --fim do la�o dos itens do volume

   --Fazer loop nos �tens n�o importados e ir realizando a oficializa��o
   for f_itens_vol in (
      select inte_wms_vol_itens.cod_barras_ean,
             inte_wms_vol_itens.nivel_sku,          inte_wms_vol_itens.grupo_sku,
             inte_wms_vol_itens.subgrupo_sku,       inte_wms_vol_itens.item_sku,
             inte_wms_vol_itens.dep_item,
             inte_wms_vol_itens.nivel_sku_kg,       inte_wms_vol_itens.grupo_sku_kg,
             inte_wms_vol_itens.subgrupo_sku_kg,    inte_wms_vol_itens.item_sku_kg,
             inte_wms_vol_itens.ref_original,
             sum(inte_wms_vol_itens.qtde_pecas) qtde_pecas,
             sum(inte_wms_vol_itens.qtde_quilos_estoque) qtde_quilos_estoque
      from  inte_wms_vol_itens
      where inte_wms_vol_itens.nr_volume_st       = :new.nr_volume_st
        and inte_wms_vol_itens.flag_integracao_st = 0
      group by inte_wms_vol_itens.cod_barras_ean,
               inte_wms_vol_itens.nivel_sku,          inte_wms_vol_itens.grupo_sku,
               inte_wms_vol_itens.subgrupo_sku,       inte_wms_vol_itens.item_sku,
               inte_wms_vol_itens.dep_item,
               inte_wms_vol_itens.nivel_sku_kg,       inte_wms_vol_itens.grupo_sku_kg,
               inte_wms_vol_itens.subgrupo_sku_kg,    inte_wms_vol_itens.item_sku_kg,
               inte_wms_vol_itens.ref_original
   )
   LOOP

      --Buscar o artigo
      begin
         select basi_030.artigo, basi_030.linha_produto
         into  v_artigo_prod,	   v_linha_prod
         from  basi_030
         where basi_030.nivel_estrutura = f_itens_vol.nivel_sku
           and basi_030.referencia      = f_itens_vol.grupo_sku;
      exception
      when no_data_found then
         v_artigo_prod := 0;
	       v_linha_prod  := 0;
      end;

      --Verificar se � Kg para pegar o campo
      if v_caracteristica_300 = 0
      then

         begin
            select pcpc_310.nivel,      pcpc_310.grupo,
                   pcpc_310.sub,        pcpc_310.item
            into 	 v_niv_kg,            v_gru_kg,
                   v_tam_kg,            v_cor_kg
            from   pcpc_310
            where  pcpc_310.cod_tipo_volume = :new.cod_tipo_vol_st
              and  pcpc_310.artigo_produto  = v_artigo_prod
              and  pcpc_310.linha_produto   = v_linha_prod
              and  pcpc_310.deposito        = f_itens_vol.dep_item;
         exception
         when no_data_found then
   
            begin
   
               select pcpc_310.nivel,      pcpc_310.grupo,
                      pcpc_310.sub,        pcpc_310.item
               into   v_niv_kg,            v_gru_kg,
                      v_tam_kg,            v_cor_kg
               from   pcpc_310
               where  pcpc_310.cod_tipo_volume = :new.cod_tipo_vol_st
                 and  pcpc_310.artigo_produto  = v_artigo_prod
                 and (pcpc_310.linha_produto   = v_linha_prod or
                      pcpc_310.linha_produto   = 0)
                 and  pcpc_310.deposito        = f_itens_vol.dep_item;
   
            exception
            when no_data_found then
   
               begin
   
                  select pcpc_310.nivel,      pcpc_310.grupo,
                         pcpc_310.sub,        pcpc_310.item
                  into   v_niv_kg,            v_gru_kg,
                         v_tam_kg,            v_cor_kg
                  from   pcpc_310
                  where  pcpc_310.cod_tipo_volume = :new.cod_tipo_vol_st
                    and  pcpc_310.artigo_produto  = v_artigo_prod
                    and (pcpc_310.linha_produto   = v_linha_prod or
                         pcpc_310.linha_produto   = 0)
                    and (pcpc_310.deposito        = f_itens_vol.dep_item or
                         pcpc_310.deposito        = 0);
   
               exception
               when no_data_found then
                  v_niv_kg := '';
                  v_gru_kg := '';
                  v_tam_kg := '';
                  v_cor_kg := '';
               end;
   
            end;
   
         end;
      
      end if;  --fim do se caracteristica 0

      --Descobrir o sequencial do item do pedido e o dep�sito
      if :new.nr_pedido = 0
      then
         v_seq_item_ped  := 0;
         v_cod_deposito  := f_itens_vol.dep_item;

	       if v_caracteristica_300 = 0
         then
	          v_cod_deposito  := v_cod_dep_entrada_kg;
	       end if;

         if v_caracteristica_300 = 13
         then
            v_cod_deposito  := v_cod_dep_entrada_kit;
         end if;

         --Verificar se j� existe o registro e buscar quanto j� foi montado para o item atual, para nao montar mais que o saldo
         begin
            select nvl(sum(pcpc_325.qtde_pecas_real), 0)
            into  v_qtde_mont_vol
            from  pcpc_325
            where pcpc_325.numero_volume         = :new.nr_volume_st
              and pcpc_325.nivel                 = f_itens_vol.nivel_sku
              and pcpc_325.grupo                 = f_itens_vol.ref_original
              and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
              and pcpc_325.item                  = f_itens_vol.item_sku
              and pcpc_325.deposito_entrada_item = v_cod_deposito;
         exception
         when no_data_found then
            v_qtde_mont_vol := 0;
         end;

         v_encontrou_reg := 1;
         begin
            select 1
            into  v_encontrou_reg
            from  pcpc_325
            where pcpc_325.numero_volume         = :new.nr_volume_st
              and pcpc_325.nivel                 = f_itens_vol.nivel_sku
              and pcpc_325.grupo                 = f_itens_vol.ref_original
              and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
              and pcpc_325.item                  = f_itens_vol.item_sku
              and pcpc_325.periodo_ordem         = 0
              and pcpc_325.ordem_confeccao       = 0
              and pcpc_325.deposito_entrada_item = v_cod_deposito;
         exception
         when no_data_found then
            v_encontrou_reg := 0;
         end;

         if f_itens_vol.qtde_pecas > v_qtde_mont_vol
         then

            if v_encontrou_reg = 0
            then

               --Verificar se � Kg para pegar o campo
               if v_caracteristica_300 = 0
               then

                  /*v_nivel_estoque          := f_itens_vol.nivel_sku_kg;
                  v_grupo_estoque            := f_itens_vol.grupo_sku_kg;
                  v_sub_estoque              := f_itens_vol.subgrupo_sku_kg;
                  v_item_estoque             := f_itens_vol.item_sku_kg;*/

		              v_nivel_estoque 			     := v_niv_kg ;
		              v_grupo_estoque 			     := v_gru_kg ;
		              v_sub_estoque   			     := v_tam_kg ;
		              v_item_estoque  			     := v_cor_kg ;
		              v_dep_kg_aux				       := f_itens_vol.dep_item;
		              v_peso_aux			           := 0;

               else

                  v_nivel_estoque            := f_itens_vol.nivel_sku;
                  v_grupo_estoque            := f_itens_vol.ref_original;
                  v_sub_estoque              := f_itens_vol.subgrupo_sku;
                  v_item_estoque             := f_itens_vol.item_sku;
		              v_dep_kg_aux				       := 0; 
		              v_peso_aux				         := f_itens_vol.qtde_quilos_estoque;

               end if;
               
               --Verificar se � Kit para pegar o campo
               if v_caracteristica_300 = 13
               then
                  v_dep_kg_aux := f_itens_vol.dep_item;
               end if;

               begin

                  /*gravar os �tens do volume*/
                  insert into pcpc_325
                  ( pcpc_325.numero_volume,           pcpc_325.nivel,
                    pcpc_325.grupo,                   pcpc_325.sub,
                    pcpc_325.item,                    pcpc_325.periodo_ordem,
                    pcpc_325.ordem_confeccao,         pcpc_325.qtde_pecas_real,
                    pcpc_325.seq_item_pedido,
                    pcpc_325.deposito_entrada_item,   pcpc_325.qtde_quilos_estoque,
                    pcpc_325.artigo_produto,
                    pcpc_325.nivel_estoque,           pcpc_325.grupo_estoque,
                    pcpc_325.sub_estoque,             pcpc_325.item_estoque,
                    pcpc_325.deposito_saida_item
                  )
                  values
                  ( :new.nr_volume_st,                f_itens_vol.nivel_sku,
                    f_itens_vol.ref_original,         f_itens_vol.subgrupo_sku,
                    f_itens_vol.item_sku,             0,
                    0,                                f_itens_vol.qtde_pecas - v_qtde_mont_vol,
                    v_seq_item_ped,
                    v_cod_deposito,                   ((f_itens_vol.qtde_pecas - v_qtde_mont_vol) * v_peso_aux / f_itens_vol.qtde_pecas),
                    v_artigo_prod,
                    v_nivel_estoque,                  v_grupo_estoque,
                    v_sub_estoque,                    v_item_estoque,
                    v_dep_kg_aux
                  );
               exception
               when others then
                  raise_application_error(-20000, 'N�o inseriu na tabela PCPC_325.' || Chr(10) || SQLERRM);
               end;

            else

               begin

                  /*gravar os �tens do volume*/
                  update pcpc_325
                  set pcpc_325.qtde_pecas_real         = pcpc_325.qtde_pecas_real + (f_itens_vol.qtde_pecas - v_qtde_mont_vol),
                      pcpc_325.qtde_quilos_estoque     = pcpc_325.qtde_quilos_estoque + ((f_itens_vol.qtde_pecas - v_qtde_mont_vol) * v_peso_aux / f_itens_vol.qtde_pecas)
                  where pcpc_325.numero_volume         = :new.nr_volume_st
                    and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                    and pcpc_325.grupo                 = f_itens_vol.ref_original
                    and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                    and pcpc_325.item                  = f_itens_vol.item_sku
                    and pcpc_325.periodo_ordem         = 0
                    and pcpc_325.ordem_confeccao       = 0
                    and pcpc_325.deposito_entrada_item = v_cod_deposito;
               exception
               when others then
                  raise_application_error(-20000, 'N�o atualizou a tabela PCPC_325.' || Chr(10) || SQLERRM);
               end;

            end if;

         end if;

         --Atualizar a Item do Volume do WMS como Integrado
         begin

            update inte_wms_vol_itens
            set   inte_wms_vol_itens.flag_integracao_st = 1
            where inte_wms_vol_itens.nr_volume_st       = :new.nr_volume_st
              and inte_wms_vol_itens.nivel_sku          = f_itens_vol.nivel_sku
              and inte_wms_vol_itens.grupo_sku          = f_itens_vol.grupo_sku
              and inte_wms_vol_itens.subgrupo_sku       = f_itens_vol.subgrupo_sku
              and inte_wms_vol_itens.item_sku           = f_itens_vol.item_sku
              and inte_wms_vol_itens.flag_integracao_st = 0;

         exception
         when others then
            raise_application_error(-20000, 'N�o atualizou a tabela INTE_WMS_VOL_ITENS.' || Chr(10) || SQLERRM);
         end;

      else  --tem pedido de venda

         --Verificar a quantidade de itens com mesmo produto
         begin
            select count(1)
            into  v_retorno_int
            from  pedi_110
            where pedi_110.pedido_venda      = :new.nr_pedido
              and pedi_110.cd_it_pe_nivel99  = f_itens_vol.nivel_sku
              and pedi_110.cd_it_pe_grupo    = f_itens_vol.ref_original
              and pedi_110.cd_it_pe_subgrupo = f_itens_vol.subgrupo_sku
              and pedi_110.cd_it_pe_item     = f_itens_vol.item_sku
              and pedi_110.cod_cancelamento  = 0;
         exception
         when no_data_found then
            v_retorno_int := 0;
         end;

         --Existe apenas 1 item com o mesmo produto
         if v_retorno_int = 1
         then

            begin

               --Buscar o sequencial do item e o deposito
               select pedi_110.seq_item_pedido,      pedi_110.codigo_deposito
               into  v_seq_item_ped,                 v_cod_deposito
               from  pedi_110
               where pedi_110.pedido_venda      = :new.nr_pedido
                 and pedi_110.cd_it_pe_nivel99  = f_itens_vol.nivel_sku
                 and pedi_110.cd_it_pe_grupo    = f_itens_vol.ref_original
                 and pedi_110.cd_it_pe_subgrupo = f_itens_vol.subgrupo_sku
                 and pedi_110.cd_it_pe_item     = f_itens_vol.item_sku
                 and pedi_110.cod_cancelamento  = 0;
            exception
            when no_data_found then
               v_retorno_int  := 0;
               v_seq_item_ped := 0;
               v_cod_deposito := 0;
            end;

            --Verificar se j� existe o registro e buscar quantidade j� montada
            begin
               select nvl(sum(pcpc_325.qtde_pecas_real), 0)
               into  v_qtde_mont_vol
               from  pcpc_325
               where pcpc_325.numero_volume         = :new.nr_volume_st
                 and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                 and pcpc_325.grupo                 = f_itens_vol.ref_original
                 and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                 and pcpc_325.item                  = f_itens_vol.item_sku
                 and pcpc_325.deposito_entrada_item = v_cod_deposito;
            exception
            when no_data_found then
               v_qtde_mont_vol := 0;
            end;

            v_encontrou_reg := 1;
            begin
               select 1
               into  v_encontrou_reg
               from  pcpc_325
               where pcpc_325.numero_volume         = :new.nr_volume_st
                 and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                 and pcpc_325.grupo                 = f_itens_vol.ref_original
                 and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                 and pcpc_325.item                  = f_itens_vol.item_sku
                 and pcpc_325.periodo_ordem         = 0
                 and pcpc_325.ordem_confeccao       = 0
                 and pcpc_325.deposito_entrada_item = v_cod_deposito;
            exception
            when no_data_found then
               v_encontrou_reg := 0;
            end;

            if f_itens_vol.qtde_pecas > v_qtde_mont_vol
            then

               if v_encontrou_reg = 0
               then

                  --Verificar se � Kg para pegar o campo
                  if v_caracteristica_300 = 0
                  then
                   /*v_nivel_estoque            := f_itens_vol.nivel_sku_kg;
                     v_grupo_estoque            := f_itens_vol.grupo_sku_kg;
                     v_sub_estoque              := f_itens_vol.subgrupo_sku_kg;
                     v_item_estoque             := f_itens_vol.item_sku_kg;*/

		                 v_nivel_estoque            := v_niv_kg ;
		                 v_grupo_estoque            := v_gru_kg ;
		                 v_sub_estoque              := v_tam_kg ;
		                 v_item_estoque             := v_cor_kg ;

                  else

                     v_nivel_estoque            := f_itens_vol.nivel_sku;
                     v_grupo_estoque            := f_itens_vol.ref_original;
                     v_sub_estoque              := f_itens_vol.subgrupo_sku;
                     v_item_estoque             := f_itens_vol.item_sku;

                  end if;

                  begin

                     /*gravar os �tens do volume*/
                     insert into pcpc_325
                     ( pcpc_325.numero_volume,           pcpc_325.nivel,
                       pcpc_325.grupo,                   pcpc_325.sub,
                       pcpc_325.item,                    pcpc_325.periodo_ordem,
                       pcpc_325.ordem_confeccao,         pcpc_325.qtde_pecas_real,
                       pcpc_325.seq_item_pedido,
                       pcpc_325.deposito_entrada_item,   pcpc_325.qtde_quilos_estoque,
                       pcpc_325.artigo_produto,
                       pcpc_325.nivel_estoque,           pcpc_325.grupo_estoque,
                       pcpc_325.sub_estoque,             pcpc_325.item_estoque
                     )
                     values
                     ( :new.nr_volume_st,                f_itens_vol.nivel_sku,
                       f_itens_vol.ref_original,         f_itens_vol.subgrupo_sku,
                       f_itens_vol.item_sku,             0,
                       0,                                f_itens_vol.qtde_pecas - v_qtde_mont_vol,
                       v_seq_item_ped,
                       v_cod_deposito,                   ((f_itens_vol.qtde_pecas - v_qtde_mont_vol) * f_itens_vol.qtde_quilos_estoque / f_itens_vol.qtde_pecas),
                       v_artigo_prod,
                       v_nivel_estoque,                  v_grupo_estoque,
                       v_sub_estoque,                    v_item_estoque
                     );
                  exception
                  when others then
                     raise_application_error(-20000, 'N�o inseriu na tabela PCPC_325.' || Chr(10) || SQLERRM);
                  end;

               else

                  begin

                     /*gravar os �tens do volume*/
                     update pcpc_325
                     set pcpc_325.qtde_pecas_real         = pcpc_325.qtde_pecas_real + (f_itens_vol.qtde_pecas - v_qtde_mont_vol),
                         pcpc_325.qtde_quilos_estoque     = ((f_itens_vol.qtde_pecas - v_qtde_mont_vol) * f_itens_vol.qtde_quilos_estoque / f_itens_vol.qtde_pecas)
                     where pcpc_325.numero_volume         = :new.nr_volume_st
                       and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                       and pcpc_325.grupo                 = f_itens_vol.ref_original
                       and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                       and pcpc_325.item                  = f_itens_vol.item_sku
                       and pcpc_325.periodo_ordem         = 0
                       and pcpc_325.ordem_confeccao       = 0
                       and pcpc_325.deposito_entrada_item = v_cod_deposito;
                  exception
                  when others then
                     raise_application_error(-20000, 'N�o atualizou a tabela PCPC_325.' || Chr(10) || SQLERRM);
                  end;

               end if;

            end if;

         else --Tem mais de 1 item para o mesmo produto, descobrir qual usar

            --Vari�vel com o saldo a inserir no item do volume
            v_qtde_inserir_325 := f_itens_vol.qtde_pecas;

            --Fazer loop nos �tens do pedido com o mesmo produto, para cada item, verificar saldo a montar
            for f_itens_ped in (
               select pedi_110.seq_item_pedido,        pedi_110.codigo_deposito,
                      pedi_110.qtde_pedida,            pedi_110.qtde_faturada,
                      pedi_110.qtde_afaturar
               from  pedi_110
               where pedi_110.pedido_venda        = :new.nr_pedido
                 and pedi_110.cd_it_pe_nivel99    = f_itens_vol.nivel_sku
                 and pedi_110.cd_it_pe_grupo      = f_itens_vol.ref_original
                 and pedi_110.cd_it_pe_subgrupo   = f_itens_vol.subgrupo_sku
                 and pedi_110.cd_it_pe_item       = f_itens_vol.item_sku
                 and pedi_110.cod_cancelamento    = 0
            )
            LOOP

               --Buscar quantidade j� associada para este item deste pedido (volumes)
               begin
                  select nvl(sum(pcpc_325.qtde_pecas_real), 0)
                  into   v_qtd_tot_vol
                  from  pcpc_325, pcpc_320
                  where pcpc_325.numero_volume     = pcpc_320.numero_volume
                    and pcpc_320.pedido_venda      = :new.nr_pedido
                    and pcpc_325.seq_item_pedido   = f_itens_ped.seq_item_pedido;
               exception
               when no_data_found then
                  v_qtd_tot_vol  := 0;
               end;

               --Calcular o saldo
               v_qtd_saldo_item := f_itens_ped.qtde_pedida - v_qtd_tot_vol;

               --Verificar o saldo
               if v_qtd_saldo_item > 0
               then

                  if v_qtd_saldo_item >= v_qtde_inserir_325
                  then

                     --Existe saldo suficiente nesta sequencia de pedido para o item do volume
                     --INSERIR ESTE ITEM COM A QUANTIDADE DO VOLUME DA INTEGRA��O

                     --Verificar se j� existe o registro
                     v_encontrou_reg := 1;
                     begin
                        select 1
                        into  v_encontrou_reg
                        from  pcpc_325
                        where pcpc_325.numero_volume         = :new.nr_volume_st
                          and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                          and pcpc_325.grupo                 = f_itens_vol.ref_original
                          and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                          and pcpc_325.item                  = f_itens_vol.item_sku
                          and pcpc_325.periodo_ordem         = 0
                          and pcpc_325.ordem_confeccao       = 0
                          and pcpc_325.deposito_entrada_item = f_itens_ped.codigo_deposito;
                     exception
                     when no_data_found then
                        v_encontrou_reg := 0;
                     end;

                     if v_encontrou_reg = 0
                     then

                        --Verificar se � Kg para pegar o campo
                        if v_caracteristica_300 = 0
                        then

                         /*v_nivel_estoque            := f_itens_vol.nivel_sku_kg;
                           v_grupo_estoque            := f_itens_vol.grupo_sku_kg;
                           v_sub_estoque              := f_itens_vol.subgrupo_sku_kg;
                           v_item_estoque             := f_itens_vol.item_sku_kg;*/

			                     v_nivel_estoque            := v_niv_kg ;
			                     v_grupo_estoque            := v_gru_kg ;
			                     v_sub_estoque              := v_tam_kg ;
			                     v_item_estoque             := v_cor_kg ;

                        else

                           v_nivel_estoque            := f_itens_vol.nivel_sku;
                           v_grupo_estoque            := f_itens_vol.ref_original;
                           v_sub_estoque              := f_itens_vol.subgrupo_sku;
                           v_item_estoque             := f_itens_vol.item_sku;

                        end if;

                        begin

                           /*gravar os �tens do volume*/
                           insert into pcpc_325
                           ( pcpc_325.numero_volume,           pcpc_325.nivel,
                             pcpc_325.grupo,                   pcpc_325.sub,
                             pcpc_325.item,                    pcpc_325.periodo_ordem,
                             pcpc_325.ordem_confeccao,         pcpc_325.qtde_pecas_real,
                             pcpc_325.seq_item_pedido,
                             pcpc_325.deposito_entrada_item,   pcpc_325.qtde_quilos_estoque,
                             pcpc_325.artigo_produto,
                             pcpc_325.nivel_estoque,           pcpc_325.grupo_estoque,
                             pcpc_325.sub_estoque,             pcpc_325.item_estoque
                           )
                           values
                           ( :new.nr_volume_st,                f_itens_vol.nivel_sku,
                             f_itens_vol.ref_original,         f_itens_vol.subgrupo_sku,
                             f_itens_vol.item_sku,             0,
                             0,                                v_qtde_inserir_325,
                             f_itens_ped.seq_item_pedido,
                             f_itens_ped.codigo_deposito,      (v_qtde_inserir_325 * f_itens_vol.qtde_quilos_estoque / f_itens_vol.qtde_pecas),
                             v_artigo_prod,
                             v_nivel_estoque,                  v_grupo_estoque,
                             v_sub_estoque,                    v_item_estoque
                           );
                        exception
                        when others then
                           raise_application_error(-20000, 'N�o inseriu na tabela PCPC_325.' || Chr(10) || SQLERRM);
                        end;

                     else

                        begin

                           /*gravar os �tens do volume*/
                           update pcpc_325
                           set pcpc_325.qtde_pecas_real         = pcpc_325.qtde_pecas_real + v_qtde_inserir_325,
                               pcpc_325.qtde_quilos_estoque     = pcpc_325.qtde_quilos_estoque + (v_qtde_inserir_325 * f_itens_vol.qtde_quilos_estoque / f_itens_vol.qtde_pecas)
                           where pcpc_325.numero_volume         = :new.nr_volume_st
                             and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                             and pcpc_325.grupo                 = f_itens_vol.ref_original
                             and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                             and pcpc_325.item                  = f_itens_vol.item_sku
                             and pcpc_325.periodo_ordem         = 0
                             and pcpc_325.ordem_confeccao       = 0
                             and pcpc_325.deposito_entrada_item = f_itens_ped.codigo_deposito;
                        exception
                        when others then
                           raise_application_error(-20000, 'N�o atualizou a tabela PCPC_325.' || Chr(10) || SQLERRM);
                        end;

                     end if;

                     v_qtde_inserir_325 := v_qtde_inserir_325 - v_qtde_inserir_325;
                     EXIT WHEN 1 = 1;  --Sair do Loop

                  else

                     v_qtde_inserir_325 := v_qtde_inserir_325 - v_qtd_saldo_item;

                     --N�o existe saldo suficiente nesta sequencia de pedido para o item do volume
                     --INSERIR O SALDO DESTE ITEM E CONTINUAR BUSCANDO PR�XIMO ITEM COM SALDO



                     --Verificar se j� existe o registro
                     v_encontrou_reg := 1;
                     begin
                        select 1
                        into  v_encontrou_reg
                        from  pcpc_325
                        where pcpc_325.numero_volume         = :new.nr_volume_st
                          and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                          and pcpc_325.grupo                 = f_itens_vol.ref_original
                          and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                          and pcpc_325.item                  = f_itens_vol.item_sku
                          and pcpc_325.periodo_ordem         = 0
                          and pcpc_325.ordem_confeccao       = 0
                          and pcpc_325.deposito_entrada_item = f_itens_ped.codigo_deposito;
                     exception
                     when no_data_found then
                        v_encontrou_reg := 0;
                     end;

                     if v_encontrou_reg = 0
                     then

                        --Verificar se � Kg para pegar o campo
                        if v_caracteristica_300 = 0
                        then

                         /*v_nivel_estoque            := f_itens_vol.nivel_sku_kg;
			                     v_grupo_estoque            := f_itens_vol.grupo_sku_kg;
			                     v_sub_estoque              := f_itens_vol.subgrupo_sku_kg;
			                     v_item_estoque             := f_itens_vol.item_sku_kg;*/

			                     v_nivel_estoque            := v_niv_kg ;
			                     v_grupo_estoque            := v_gru_kg ;
			                     v_sub_estoque              := v_tam_kg ;
			                     v_item_estoque             := v_cor_kg ;

                        else

                           v_nivel_estoque            := f_itens_vol.nivel_sku;
                           v_grupo_estoque            := f_itens_vol.ref_original;
                           v_sub_estoque              := f_itens_vol.subgrupo_sku;
                           v_item_estoque             := f_itens_vol.item_sku;

                        end if;

                        begin

                           /*gravar os �tens do volume*/
                           insert into pcpc_325
                           ( pcpc_325.numero_volume,           pcpc_325.nivel,
                             pcpc_325.grupo,                   pcpc_325.sub,
                             pcpc_325.item,                    pcpc_325.periodo_ordem,
                             pcpc_325.ordem_confeccao,         pcpc_325.qtde_pecas_real,
                             pcpc_325.seq_item_pedido,
                             pcpc_325.deposito_entrada_item,   pcpc_325.qtde_quilos_estoque,
                             pcpc_325.artigo_produto,
                             pcpc_325.nivel_estoque,           pcpc_325.grupo_estoque,
                             pcpc_325.sub_estoque,             pcpc_325.item_estoque
                           )
                           values
                           ( :new.nr_volume_st,                f_itens_vol.nivel_sku,
                             f_itens_vol.ref_original,         f_itens_vol.subgrupo_sku,
                             f_itens_vol.item_sku,             0,
                             0,                                v_qtd_saldo_item,
                             f_itens_ped.seq_item_pedido,
                             f_itens_ped.codigo_deposito,      (v_qtd_saldo_item * f_itens_vol.qtde_quilos_estoque / f_itens_vol.qtde_pecas),
                             v_artigo_prod,
                             v_nivel_estoque,                  v_grupo_estoque,
                             v_sub_estoque,                    v_item_estoque
                           );
                        exception
                        when others then
                           raise_application_error(-20000, 'N�o inseriu na tabela PCPC_325.' || Chr(10) || SQLERRM);
                        end;

                     else

                        begin

                           /*gravar os �tens do volume*/
                           update pcpc_325
                           set pcpc_325.qtde_pecas_real         = pcpc_325.qtde_pecas_real + v_qtd_saldo_item,
                               pcpc_325.qtde_quilos_estoque     = pcpc_325.qtde_quilos_estoque + (v_qtd_saldo_item * f_itens_vol.qtde_quilos_estoque / f_itens_vol.qtde_pecas)
                           where pcpc_325.numero_volume         = :new.nr_volume_st
                             and pcpc_325.nivel                 = f_itens_vol.nivel_sku
                             and pcpc_325.grupo                 = f_itens_vol.ref_original
                             and pcpc_325.sub                   = f_itens_vol.subgrupo_sku
                             and pcpc_325.item                  = f_itens_vol.item_sku
                             and pcpc_325.periodo_ordem         = 0
                             and pcpc_325.ordem_confeccao       = 0
                             and pcpc_325.deposito_entrada_item = f_itens_ped.codigo_deposito;
                        exception
                        when others then
                           raise_application_error(-20000, 'N�o atualizou a tabela PCPC_325.' || Chr(10) || SQLERRM);
                        end;

                     end if;

                  end if;

               end if;

            END LOOP;

         end if;

         --Atualizar a Item do Volume do WMS como Integrado
         begin

            update inte_wms_vol_itens
            set   inte_wms_vol_itens.flag_integracao_st = 1
            where inte_wms_vol_itens.nr_volume_st       = :new.nr_volume_st
              and inte_wms_vol_itens.nivel_sku          = f_itens_vol.nivel_sku
              and inte_wms_vol_itens.grupo_sku          = f_itens_vol.grupo_sku
              and inte_wms_vol_itens.subgrupo_sku       = f_itens_vol.subgrupo_sku
              and inte_wms_vol_itens.item_sku           = f_itens_vol.item_sku
              and inte_wms_vol_itens.flag_integracao_st = 0;

         exception
         when others then
            raise_application_error(-20000, 'N�o atualizou a tabela INTE_WMS_VOL_ITENS.' || Chr(10) || SQLERRM);
         end;

      end if;

   END LOOP;

   --ATUALIZAR PESO_EMBALAGEM NA CAPA DO VOLUME
   begin

      update pcpc_320
      set pcpc_320.peso_embalagem          = (select nvl(max(fatu_100.peso_embalagem), 0)
                                              from fatu_100
                                              where fatu_100.codigo_embalagem = :new.cod_embalagem_st)
      where pcpc_320.numero_volume         = :new.nr_volume_st;
   exception
   when others then
      raise_application_error(-20000, 'N�o atualizou a tabela PCPC_320.' || Chr(10) || SQLERRM);
   end;

   --MOVIMENTA��ES PARA KIT
   if v_caracteristica_300 = 13
   then

      if v_atualiza_estoque_kit_ent = 1
      then

         --Ler os �tens do volume
         for f_itens_volume_st in (
            select pcpc_325.nivel_estoque, pcpc_325.grupo_estoque,
                   pcpc_325.sub_estoque, pcpc_325.item_estoque,
                   sum(pcpc_325.qtde_pecas_real) qtde_pecas_real
            from pcpc_325
            where pcpc_325.numero_volume = :new.nr_volume_st
            group by  pcpc_325.nivel_estoque,
                      pcpc_325.grupo_estoque,
                      pcpc_325.sub_estoque,
                      pcpc_325.item_estoque
            order by  pcpc_325.nivel_estoque,
                      pcpc_325.grupo_estoque,
                      pcpc_325.sub_estoque,
                      pcpc_325.item_estoque)
         LOOP

            begin
               select basi_010.preco_medio
               into v_preco_medio_010
               from basi_010
               where nivel_estrutura  = f_itens_volume_st.nivel_estoque
                 and grupo_estrutura  = f_itens_volume_st.grupo_estoque
                 and subgru_estrutura = f_itens_volume_st.sub_estoque
                 and item_estrutura   = f_itens_volume_st.item_estoque;
            exception
            when no_data_found then
               v_preco_medio_010 := 0.00;
            end;

            begin

               insert into estq_300
                  (data_movimento,                            codigo_transacao,
                   nivel_estrutura,                           grupo_estrutura,
                   subgrupo_estrutura,                        item_estrutura,
                   quantidade,                                valor_movimento_unitario,
                   valor_contabil_unitario,                   codigo_deposito,
                   numero_lote,                               centro_custo,
                   usuario_systextil,                         processo_systextil,
                   tabela_origem,                             entrada_saida,
                   numero_documento)
               values
                  (trunc(:new.timestamp_fim_mont),            v_transacao_ent,
                   f_itens_volume_st.nivel_estoque,           f_itens_volume_st.grupo_estoque,
                   f_itens_volume_st.sub_estoque,             f_itens_volume_st.item_estoque,
                   f_itens_volume_st.qtde_pecas_real,         v_preco_medio_010,
                   v_preco_medio_010,                         v_cod_dep_entrada_kit,
                   0,                                         0,
                   :new.usuario_exped,                        :new.nome_programa,
                   'PCPC_320', /*WMS*/                        'E',
                   :new.nr_volume_st);

            exception
            when others then
               raise_application_error(-20000, 'N�o inseriu na tabela ESTQ_300.' || Chr(10) || SQLERRM);
            end;

         END LOOP;
         
      end if;

      --FAZER MOVIMENTO DE SAIDA
      if v_cod_deposito_saida_kit > 0 and v_atualiza_estoque_kit_sai = 1
      then

         begin

            select basi_205.tipo_volume
            into   v_tipo_volume
            from basi_205
            where basi_205.codigo_deposito = v_cod_deposito_saida_kit;
         exception
         when no_data_found then
            v_tipo_volume := 0;
         end;

         for f_itens_vol_st in (
            select pcpc_325.nivel,                pcpc_325.grupo,
                   pcpc_325.sub,                  pcpc_325.item,
                   pcpc_325.periodo_ordem,        pcpc_325.qtde_pecas_real,
                   pcpc_325.nivel_estoque,        pcpc_325.grupo_estoque,
                   pcpc_325.sub_estoque,          pcpc_325.item_estoque,
                   pcpc_325.ordem_confeccao,      pcpc_325.deposito_saida_item
            from pcpc_325
            where pcpc_325.numero_volume = :new.nr_volume_st)
         LOOP

            v_item := f_itens_vol_st.item;

            begin
               select basi_010.preco_medio
               into v_preco_medio_010
               from basi_010
               where nivel_estrutura  = f_itens_vol_st.nivel
                 and grupo_estrutura  = f_itens_vol_st.grupo
                 and subgru_estrutura = f_itens_vol_st.sub
                 and item_estrutura   = f_itens_vol_st.item;
            exception
            when no_data_found then
               v_preco_medio_010 := 0.00;
            end;

            begin
               select basi_030.cor_de_estoque
               into   v_cor_estoque
               from basi_030
               where basi_030.nivel_estrutura = f_itens_vol_st.nivel
                 and basi_030.referencia      = f_itens_vol_st.grupo;
            exception
            when no_data_found then
               v_cor_estoque := null;
            end;

            if v_cor_estoque is not null and
               v_cor_estoque <> '000000'
            then v_item := v_cor_estoque;
            end if;

            v_corTag := v_item;

            /*
               DEPOSITO SEM CONTROLE POR VOLUME
            */

            v_deposito_saida_aux := 0;

            if f_itens_vol_st.deposito_saida_item = 0
            then v_deposito_saida_aux := v_cod_deposito_saida_kit;
            else v_deposito_saida_aux := f_itens_vol_st.deposito_saida_item;

               begin
                  select basi_205.tipo_volume
                  into v_tipo_volume
                  from basi_205
                  where basi_205.codigo_deposito = v_deposito_saida_aux;
               exception
               when no_data_found then
                  v_tipo_volume := 0;
               end;

            end if;

            if v_tipo_volume = 0
            then

               begin

                  insert into estq_300
                     (data_movimento,                          codigo_transacao,
                      nivel_estrutura,                         grupo_estrutura,
                      subgrupo_estrutura,                      item_estrutura,
                      quantidade,                              valor_movimento_unitario,
                      valor_contabil_unitario,                 codigo_deposito,
                      numero_lote,                             centro_custo,
                      usuario_systextil,                       processo_systextil,
                      tabela_origem,                           entrada_saida,
                      numero_documento)
                  values
                     (trunc(:new.timestamp_fim_mont),          v_trans_saida_tipo_vol,
                      f_itens_vol_st.nivel,                    f_itens_vol_st.grupo,
                      f_itens_vol_st.sub,                      v_item,
                      f_itens_vol_st.qtde_pecas_real,          v_preco_medio_010,
                      v_preco_medio_010,                       v_deposito_saida_aux,
                      0,                                       0,
                      :new.usuario_exped,                      :new.nome_programa,
                      'PCPC_320', /*WMS*/                      'S',
                      :new.nr_volume_st);

               exception
               when others then
                  raise_application_error(-20000, 'N�o inseriu na tabela ESTQ_300.' || Chr(10) || SQLERRM);
               end;

            else
               /*
               DEPOSITO CONTROLADO POR VOLUME - TAG

               QUANDO FAZ LEITURA DO C�DIGO DE BARRAS � PASSADO O ESTOQUE TAG PARA 3.
               AQUI ABAIXO, QUANDO MUDA O ESTOQUE TAG PARA 4 A TRIGER FAZ A MOVIMENTA��O DO ESTOQUE (SAIDA)

               */
               begin

                  update pcpc_330
                  set nome_prog_050         = :new.nome_programa,
                      usuario_estq          = :new.usuario_exped,
                      estoque_tag           = 4,
                      deposito              = v_deposito_saida_aux,
                      transacao_cardex      = v_trans_saida_tipo_vol,
                      data_cardex           = trunc(:new.timestamp_fim_mont),
                      usuario_cardex        = :new.usuario_exped,
                      valor_movto_cardex    = v_preco_medio_010,
                      valor_contabil_cardex = v_preco_medio_010,
                      seq_saida             = 0
                    where pcpc_330.periodo_producao = f_itens_vol_st.periodo_ordem
                     and  pcpc_330.ordem_confeccao  = f_itens_vol_st.ordem_confeccao
                     and  pcpc_330.nr_volume        = :new.nr_volume_st
                     and  pcpc_330.nivel            = f_itens_vol_st.nivel
                     and  pcpc_330.grupo            = f_itens_vol_st.grupo
                     and  pcpc_330.subgrupo         = f_itens_vol_st.sub
                     and  pcpc_330.item             = v_corTag
                     and  pcpc_330.estoque_tag      in (2,3);  /*2 - COLETADA , 3 - COLETA CONFIRMADA*/
               exception
               when others then
                  raise_application_error(-20000, 'N�o atualizou a tabela PCPC_330.' || Chr(10) || SQLERRM);
               end;

            end if;

         END LOOP;

      end if;

   end if;

   --MOVIMENTA��ES PARA KG
   if v_caracteristica_300 = 0
   then

      if v_atualiza_estoque_kit_ent = 1
      then

	       select sum(basi_020.peso_liquido * pcpc_325.qtde_pecas_real)
	       into   peso_liquido_total_aux
	       from basi_020, pcpc_325
	       where basi_020.basi030_nivel030 = pcpc_325.nivel
 		       and basi_020.basi030_referenc = pcpc_325.grupo
		       and basi_020.tamanho_ref      = pcpc_325.sub
		       and pcpc_325.numero_volume    = :new.nr_volume_st
	       group by pcpc_325.numero_volume;

         for peso_liquido_volume in (select pcpc_325.nivel,          pcpc_325.grupo,
					                                  pcpc_325.sub,            pcpc_325.item,
 					                                  pcpc_325.periodo_ordem,  pcpc_325.ordem_confeccao,
					                                  pcpc_325.seq_item_pedido
                                     from pcpc_325
                                     where pcpc_325.numero_volume = :new.nr_volume_st
	       )loop

            begin

               select basi_020.peso_liquido * pcpc_325.qtde_pecas_real
               into   peso_liquido_item_aux
               from basi_020, pcpc_325
               where pcpc_325.numero_volume    = :new.nr_volume_st
                 and pcpc_325.nivel            = peso_liquido_volume.nivel
                 and pcpc_325.grupo            = peso_liquido_volume.grupo
                 and pcpc_325.sub              = peso_liquido_volume.sub
                 and pcpc_325.item             = peso_liquido_volume.item
                 and pcpc_325.periodo_ordem    = peso_liquido_volume.periodo_ordem
                 and pcpc_325.ordem_confeccao  = peso_liquido_volume.ordem_confeccao
                 and pcpc_325.seq_item_pedido  = peso_liquido_volume.seq_item_pedido
                 and basi_020.basi030_nivel030 = pcpc_325.nivel
                 and basi_020.basi030_referenc = pcpc_325.grupo
                 and basi_020.tamanho_ref      = pcpc_325.sub;
	          exception
            when no_data_found then
	             peso_liquido_item_aux := 0;
            end;

            peso_real_item_aux := 0;

            peso_real_item_aux := (peso_liquido_item_aux * (:new.peso_volume / peso_liquido_total_aux));

            begin
               update pcpc_325
               set pcpc_325.qtde_quilos_estoque = peso_real_item_aux
               where pcpc_325.numero_volume    = :new.nr_volume_st
                 and pcpc_325.nivel            = peso_liquido_volume.nivel
                 and pcpc_325.grupo            = peso_liquido_volume.grupo
                 and pcpc_325.sub              = peso_liquido_volume.sub
                 and pcpc_325.item             = peso_liquido_volume.item
                 and pcpc_325.periodo_ordem    = peso_liquido_volume.periodo_ordem
                 and pcpc_325.ordem_confeccao  = peso_liquido_volume.ordem_confeccao;
            exception
            when others then
		           raise_application_error(-20000, 'N�o inseriu na tabela PCPC_325.' || Chr(10) || SQLERRM);
            end;

	       end loop;

         /*
            TOTALIZA PESO DOS PRODUTOS DO QUILO PARA FAZER A ENTRADA NO ESTOQUE
         */
         for f_itens_volume_kg in (
            select nvl(sum(pcpc_325.qtde_quilos_estoque),0) qtde,  pcpc_325.nivel_estoque nivel_entrada,
                   pcpc_325.grupo_estoque grupo_entrada,           pcpc_325.sub_estoque sub_entrada,
                   pcpc_325.item_estoque item_entrada
            from pcpc_325
            where pcpc_325.numero_volume = :new.nr_volume_st
            group by  pcpc_325.nivel_estoque,
                      pcpc_325.grupo_estoque,
                      pcpc_325.sub_estoque,
                      pcpc_325.item_estoque
            order by  pcpc_325.nivel_estoque,
                      pcpc_325.grupo_estoque,
                      pcpc_325.sub_estoque,
                      pcpc_325.item_estoque)
         LOOP

            begin
                select basi_010.preco_medio
                into   v_preco_medio_010
                from basi_010
                where nivel_estrutura  = f_itens_volume_kg.nivel_entrada
                  and grupo_estrutura  = f_itens_volume_kg.grupo_entrada
                  and subgru_estrutura = f_itens_volume_kg.sub_entrada
                  and item_estrutura   = f_itens_volume_kg.item_entrada;
            exception
            when no_data_found then
               v_preco_medio_010 := 0.00;
            end;

            begin

               insert into estq_300
                    (data_movimento,                           codigo_transacao,
                     nivel_estrutura,                          grupo_estrutura,
                     subgrupo_estrutura,                       item_estrutura,
                     quantidade,                               valor_movimento_unitario,
                     valor_contabil_unitario,                  codigo_deposito,
                     numero_lote,                              centro_custo,
                     usuario_systextil,                        processo_systextil,
                     tabela_origem,                            entrada_saida,
                     numero_documento)
               values
                  (trunc(:new.timestamp_fim_mont),            v_transacao_ent,
                   f_itens_volume_kg.nivel_entrada,           f_itens_volume_kg.grupo_entrada,
                   f_itens_volume_kg.sub_entrada,             f_itens_volume_kg.item_entrada,
                   f_itens_volume_kg.qtde,                    v_preco_medio_010,
                   v_preco_medio_010,                         v_cod_dep_entrada_kg,
                   0,                                         0,
                   :new.usuario_exped,                        :new.nome_programa,
                   'PCPC_320', /*WMS*/                        'E',
                   :new.nr_volume_st);

            exception
            when others then
               raise_application_error(-20000, 'N�o inseriu na tabela ESTQ_300.' || Chr(10) || SQLERRM);
            end;

         END LOOP;

      end if;

      if v_cod_deposito_saida_kg > 0 and v_atualiza_estoque_kit_sai = 1
      then

         begin
            select basi_205.tipo_volume
            into   v_tipo_volume
            from basi_205
            where basi_205.codigo_deposito = v_cod_deposito_saida_kg;
         exception
         when no_data_found then
            v_tipo_volume := 0;
         end;

         for f_itens_volume_kg in (
            select pcpc_325.nivel,                                 pcpc_325.grupo,
                   pcpc_325.sub,                                   pcpc_325.item,
                   pcpc_325.periodo_ordem periodo_producao,        pcpc_325.qtde_pecas_real qtde,
                   pcpc_325.nivel_estoque nivel_entrada,           pcpc_325.grupo_estoque grupo_entrada,
                   pcpc_325.sub_estoque sub_entrada,               pcpc_325.item_estoque item_entrada,
                   pcpc_325.ordem_confeccao oc_entrada,            pcpc_325.deposito_saida_item dep_saida_item
            from pcpc_325
            where pcpc_325.numero_volume = :new.nr_volume_st)
         LOOP

            v_item := f_itens_volume_kg.item;

            begin
                select basi_010.preco_medio
                into   v_preco_medio_010
                from basi_010
                where nivel_estrutura  = f_itens_volume_kg.nivel
                  and grupo_estrutura  = f_itens_volume_kg.grupo
                  and subgru_estrutura = f_itens_volume_kg.sub
                  and item_estrutura   = f_itens_volume_kg.item;
            exception
            when no_data_found then
               v_preco_medio_010 := 0.00;
            end;

            v_corTag := f_itens_volume_kg.item;

            begin
               select basi_030.cor_de_estoque
               into   v_cor_estoque
               from basi_030
               where basi_030.nivel_estrutura = f_itens_volume_kg.nivel
                 and basi_030.referencia      = f_itens_volume_kg.grupo;
            exception
            when no_data_found then
               v_cor_estoque := null;
            end;

            if v_cor_estoque is not null and
               v_cor_estoque <> '000000'
            then v_item := v_cor_estoque;
            end if;


            /*
               DEPOSITO SEM CONTROLE POR VOLUME
            */

            v_deposito_saida_aux := 0;

            if f_itens_volume_kg.dep_saida_item = 0
            then v_deposito_saida_aux := v_cod_deposito_saida_kg;
            else v_deposito_saida_aux := f_itens_volume_kg.dep_saida_item;

               begin
                  select basi_205.tipo_volume
                  into v_tipo_volume
                  from basi_205
                  where basi_205.codigo_deposito = v_deposito_saida_aux;
               exception
               when no_data_found then
                  v_tipo_volume := 0;
               end;

            end if;

            if v_tipo_volume = 0
            then

               begin

                  insert into estq_300
                     (data_movimento,                          codigo_transacao,
                      nivel_estrutura,                         grupo_estrutura,
                      subgrupo_estrutura,                      item_estrutura,
                      quantidade,                              valor_movimento_unitario,
                      valor_contabil_unitario,                 codigo_deposito,
                      numero_lote,                             centro_custo,
                      usuario_systextil,                       processo_systextil,
                      tabela_origem,                           entrada_saida,
                      numero_documento)
                  values
                     (trunc(:new.timestamp_fim_mont),          v_trans_saida_tipo_vol,
                      f_itens_volume_kg.nivel,                 f_itens_volume_kg.grupo,
                      f_itens_volume_kg.sub,                   v_item,
                      f_itens_volume_kg.qtde,                  v_preco_medio_010,
                      v_preco_medio_010,                       v_deposito_saida_aux,
                      0,                                       0,
                      :new.usuario_exped,                      :new.nome_programa,
                      'PCPC_320', /*WMS*/                      'S',
                      :new.nr_volume_st);

               exception
               when others then
                  raise_application_error(-20000, 'N�o inseriu na tabela ESTQ_300.' || Chr(10) || SQLERRM);
               end;

            else
               /*
                  DEPOSITO CONTROLADO POR VOLUME - TAG

                  QUANDO FAZ LEITURA DO C�DIGO DE BARRAS � PASSADO O ESTOQUE TAG PARA 3.
                  AQUI ABAIXO, QUANDO MUDA O ESTOQUE TAG PARA 4 A TRIGER FAZ A MOVIMENTA��O DO ESTOQUE (SAIDA)

               */

               begin

                  update pcpc_330
                  set nome_prog_050         = :new.nome_programa,
                      usuario_estq          = :new.usuario_exped,
                      estoque_tag           = 4,
                      deposito              = v_deposito_saida_aux,
                      transacao_cardex      = v_trans_saida_tipo_vol,
                      data_cardex           = trunc(:new.timestamp_fim_mont),
                      usuario_cardex        = :new.usuario_exped,
                      valor_movto_cardex    = v_preco_medio_010,
                      valor_contabil_cardex = v_preco_medio_010,
                      seq_saida             = 0
                    where pcpc_330.periodo_producao = f_itens_volume_kg.periodo_producao
                     and  pcpc_330.ordem_confeccao  = f_itens_volume_kg.oc_entrada
                     and  pcpc_330.nr_volume        = :new.nr_volume_st
                     and  pcpc_330.nivel            = f_itens_volume_kg.nivel
                     and  pcpc_330.grupo            = f_itens_volume_kg.grupo
                     and  pcpc_330.subgrupo         = f_itens_volume_kg.sub
                     and  pcpc_330.item             = v_corTag
                     and  pcpc_330.estoque_tag      in (2,3);  /*2 - COLETADA , 3 - COLETA CONFIRMADA*/
               exception
               when others then
                  raise_application_error(-20000, 'N�o atualizou a tabela PCPC_330.' || Chr(10) || SQLERRM);
               end;
            end if;
         END LOOP;
      end if;

      for f_itens_volume_erp in (
         select nvl(sum(pcpc_325.qtde_quilos_estoque),0) qtde,      pcpc_325.nivel_estoque,
                pcpc_325.grupo_estoque,                             pcpc_325.sub_estoque,
                pcpc_325.item_estoque
         from pcpc_325
         where pcpc_325.numero_volume = :new.nr_volume_st
         group by  pcpc_325.nivel_estoque,
                   pcpc_325.grupo_estoque,
                   pcpc_325.sub_estoque,
                   pcpc_325.item_estoque
         order by  pcpc_325.nivel_estoque,
                   pcpc_325.grupo_estoque,
                   pcpc_325.sub_estoque,
                   pcpc_325.item_estoque)
      LOOP

         if f_itens_volume_erp.nivel_estoque <> '0'
         then
            v_niv := f_itens_volume_erp.nivel_estoque;
            v_gru := f_itens_volume_erp.grupo_estoque;
            v_sub := f_itens_volume_erp.sub_estoque;
            v_ite := f_itens_volume_erp.item_estoque;
         else
            v_niv := ' ';
            v_gru := ' ';
            v_sub := ' ';
            v_ite := ' ';
         end if;

         v_saldo_item_caixa := f_itens_volume_erp.qtde;

         if :new.nr_pedido > 0
         then

            /* SS 55550/001 - LUIZ */
            v_encontrou_reg := 1;
            begin
               select pcpc_311.scdnivelpedido,  pcpc_311.scdgrupopedido
               into   v_scdnivelpedido,         v_scdgrupopedido
               from pcpc_311
               where pcpc_311.scdnivelestoque = v_niv
                 and pcpc_311.scdgrupoestoque = v_gru;
            exception
            when no_data_found then
               v_scdnivelpedido := ' ';
               v_scdgrupopedido := ' ';
               v_encontrou_reg  := 0;
            end;

            if v_encontrou_reg = 1
            then

               v_niv_311 := v_scdnivelpedido;
               v_gru_311 := v_scdgrupopedido;

               v_saldo_item_caixa_aux := v_saldo_item_caixa;

               for f_itens_pedido_st in (
                  select pedi_110.pedido_venda icdPedidoVenda,    pedi_110.seq_item_pedido icdSeqPedido,
                         pedi_110.qtde_faturada,                  pedi_110.qtde_pedida,
                         pedi_110.qtde_afaturar
                  from pedi_110
                  where pedi_110.pedido_venda      = :new.nr_pedido
                    and pedi_110.cd_it_pe_nivel99  = v_niv
                    and pedi_110.cd_it_pe_grupo    = v_gru
                    and pedi_110.cd_it_pe_subgrupo = v_sub
                    and pedi_110.cd_it_pe_item     = v_ite
                    and pedi_110.codigo_deposito   = v_cod_dep_entrada_kg
                    and pedi_110.qtde_pedida - pedi_110.qtde_faturada - pedi_110.qtde_afaturar > 0.00)
               LOOP

                  v_saldo_item := (f_itens_pedido_st.qtde_pedida - f_itens_pedido_st.qtde_faturada - f_itens_pedido_st.qtde_afaturar);

                  if v_saldo_item_caixa_aux > 0.00 and v_saldo_item > 0
                  then

                     if v_saldo_item < v_saldo_item_caixa_aux
                     then v_qtde_atualiza := v_saldo_item;
                     else v_qtde_atualiza := v_saldo_item_caixa_aux;
                     end if;

                     v_saldo_item_caixa_aux := v_saldo_item_caixa_aux - v_qtde_atualiza;

                     wms_pr_atu_item_pedido(f_itens_pedido_st.icdPedidoVenda, f_itens_pedido_st.icdSeqPedido, v_niv_311, v_gru_311, v_qtde_atualiza);

                  end if;

               END LOOP;

            else
               v_niv_311 := v_niv;
               v_gru_311 := v_gru;
            end if;

            v_seq_atualiza := 0;

            for f_itens_ped_st in
                (select pedi_110.seq_item_pedido seq_atualiza,   pedi_110.qtde_faturada,
                        pedi_110.qtde_pedida,                    pedi_110.qtde_afaturar,
                        pedi_110.situacao_fatu_it
                 from pedi_110
                 where pedi_110.pedido_venda      = :new.nr_pedido
                   and pedi_110.cd_it_pe_nivel99  = v_niv_311
                   and pedi_110.cd_it_pe_grupo    = v_gru_311
                   and pedi_110.cd_it_pe_subgrupo = v_sub
                   and pedi_110.cd_it_pe_item     = v_ite
                   and pedi_110.codigo_deposito   = v_cod_dep_entrada_kg)
            LOOP

               v_seq_atualiza := f_itens_ped_st.seq_atualiza;

               v_saldo_item := (f_itens_ped_st.qtde_pedida - f_itens_ped_st.qtde_faturada - f_itens_ped_st.qtde_afaturar);

               if v_saldo_item_caixa > 0.00 and v_saldo_item > 0
               then

                  if v_saldo_item < v_saldo_item_caixa
                  then v_qtde_atualiza := v_saldo_item;
                  else v_qtde_atualiza := v_saldo_item_caixa;
                  end if;

                  v_saldo_item_caixa := v_saldo_item_caixa - v_qtde_atualiza;

                  /*ATUALIZA A QUANTIDADE A FATURAR NO PEDIDO E O NUMERO DA SOLICITACAO*/
                  begin

                     update pedi_110
                     set qtde_afaturar     = pedi_110.qtde_afaturar + v_qtde_atualiza,
                         nr_solicitacao    = 0,
                         situacao_fatu_it  = 2,
                         permite_atu_wms   = 1
                     where pedido_venda    = :new.nr_pedido
                       and seq_item_pedido = f_itens_ped_st.seq_atualiza;
                  exception
                  when others then
                     raise_application_error(-20000, 'N�o atualizou a tabela PEDI_110.' || Chr(10) || SQLERRM);
                  end;

                  begin

                     update pedi_110
                     set permite_atu_wms   = 0
                     where pedido_venda    = :new.nr_pedido
                       and seq_item_pedido = f_itens_ped_st.seq_atualiza;
                  exception
                  when others then
                     raise_application_error(-20000, 'N�o atualizou a tabela PEDI_110.' || Chr(10) || SQLERRM);
                  end;

               end if;

            END LOOP;

            if v_saldo_item_caixa > 0
            then

               begin

                  update pedi_110
                  set qtde_afaturar    = pedi_110.qtde_afaturar + v_saldo_item_caixa,
                      nr_solicitacao   = 0,
                      situacao_fatu_it = 2,
                      permite_atu_wms  = 1
                  where pedido_venda    = :new.nr_pedido
                    and seq_item_pedido = v_seq_atualiza;
               exception
               when others then
                  raise_application_error(-20000, 'N�o atualizou a tabela PEDI_110.' || Chr(10) || SQLERRM);
               end;

               begin

                  update pedi_110
                  set permite_atu_wms   = 0
                  where pedido_venda    = :new.nr_pedido
                    and seq_item_pedido = v_seq_atualiza;
               exception
               when others then
                  raise_application_error(-20000, 'N�o atualizou a tabela PEDI_110.' || Chr(10) || SQLERRM);
               end;

            end if;
         end if;
      END LOOP;
   end if;

   --ATUALIZAR SITUA��O DO VOLUME
   if :new.nr_pedido = 0
   then

      --Atualizar a situa��o do volume para 3-Fechado
      begin

         update pcpc_320
         set pcpc_320.situacao_volume         = 3,
             pcpc_320.nr_solicitacao          = 0
         where pcpc_320.numero_volume         = :new.nr_volume_st;
      exception
      when others then
         raise_application_error(-20000, 'N�o atualizou a tabela PCPC_320.' || Chr(10) || SQLERRM);
      end;
      
   else

      --Somente se for normal. Se for KG ou KIT, fez a rotina espec�fica anterior.
      if v_caracteristica_300 <> 13 and v_caracteristica_300 <> 0
      then

         --Atualizar a qtd pe�as a faturar da pedi_110
         for f_itens_volume_st in (
            select pcpc_325.qtde_pecas_real, pcpc_325.seq_item_pedido
            from pcpc_325, pcpc_320
            where pcpc_325.numero_volume = :new.nr_volume_st
              and pcpc_320.pedido_venda  = :new.nr_pedido
              and pcpc_320.numero_volume = pcpc_325.numero_volume)
         LOOP

            begin
               update pedi_110
               set   pedi_110.permite_atu_wms = 1,
                     pedi_110.qtde_afaturar   = pedi_110.qtde_afaturar + f_itens_volume_st.qtde_pecas_real,
                     pedi_110.nr_solicitacao  = 0
               where pedi_110.pedido_venda    = :new.nr_pedido
                 and pedi_110.seq_item_pedido = f_itens_volume_st.seq_item_pedido;
            exception
            when others then
               raise_application_error(-20000, 'N�o atualizou a tabela PEDI_110.' || Chr(10) || SQLERRM);
            end;

            begin
               update pedi_110
               set   pedi_110.permite_atu_wms = 0
               where pedi_110.pedido_venda    = :new.nr_pedido
                 and pedi_110.seq_item_pedido = f_itens_volume_st.seq_item_pedido;
            exception
            when others then
               raise_application_error(-20000, 'N�o atualizou a tabela PEDI_110.' || Chr(10) || SQLERRM);
            end;

            begin
               update fatu_780
               set   fatu_780.qtde_disponivel   = fatu_780.qtde_disponivel + f_itens_volume_st.qtde_pecas_real
               where fatu_780.pedido            = :new.nr_pedido
                 and fatu_780.sequencia         = f_itens_volume_st.seq_item_pedido
                 and fatu_780.flag_controle_wms = 1;  --Dentro do WMS
            exception
            when others then
               raise_application_error(-20000, 'N�o atualizou a tabela FATU_780.' || Chr(10) || SQLERRM);
            end;

         END LOOP;

         --Atualizar a situa��o do volume para 1-A faturar
         begin

            update pcpc_320
            set pcpc_320.situacao_volume         = 1,
                pcpc_320.nr_solicitacao          = 0
            where pcpc_320.numero_volume         = :new.nr_volume_st;
         exception
         when others then
            raise_application_error(-20000, 'N�o atualizou a tabela PCPC_320.' || Chr(10) || SQLERRM);
         end;

      end if;

   end if;

   --Passa o volume para integrado pelo Syst�xtil
   :new.flag_integracao_st := 1;

end wms_tr_volumes;
/

execute inter_pr_recompile;

/* versao: 6 */


 exit;


 exit;

 exit;


 exit;

 exit;


 exit;

 exit;


 exit;

 exit;


 exit;

 exit;
