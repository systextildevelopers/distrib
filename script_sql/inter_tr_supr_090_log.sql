
  CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_090_LOG" 
   after insert or
         delete or
         update of  dt_emis_ped_comp,      cond_pgto_compra,     forn_ped_forne9,       forn_ped_forne4,
                    forn_ped_forne2,       data_prev_entr,       tran_ped_forne9,       tran_ped_forne4,
                    tran_ped_forne2,       tipo_frete,           cod_end_entrega,       cod_end_cobranca,
                    codigo_comprador,      vendedor_contato,     cod_cancelamento,      situacao_pedido,
                    valor_frete,           valor_outras,         cod_portador,          codigo_empresa,
                    tab_preco,             codigo_transacao
   on supr_090
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   long_aux                  varchar2(2000);
   v_teve                    varchar2(1);
   v_executa_trigger      number;

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting
      then
         long_aux := '';

         long_aux := long_aux  ||
         inter_fn_buscar_tag('lb09880#PEDIDO DE COMPRA',ws_locale_usuario,ws_usuario_systextil) ||
               chr(10)         ||
               chr(10);

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb07207#EMPRESA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.codigo_empresa
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00142#DATA EMISSAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.dt_emis_ped_comp
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb02130#COND.PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.cond_pgto_compra
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00699#FORNECEDOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.forn_ped_forne9 || '/' ||
                                                   :new.forn_ped_forne4 || '-' ||
                                                   :new.forn_ped_forne2
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00617#DATA PREV. ENTREGA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.data_prev_entr
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb05956#TRANSPORTADORA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.tran_ped_forne9 || '/' ||
                                                   :new.tran_ped_forne4 || '-' ||
                                                   :new.tran_ped_forne2
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb07856#TIPO FRETE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.tipo_frete
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb03557#ENDERECO ENTREGA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.cod_end_entrega
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb03553#ENDERECO DE COBRANCA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.cod_end_cobranca
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb02587#CODIGO COMPRADOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.codigo_comprador
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb34988#CONTATO VENDEDOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.vendedor_contato
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb02582#CODIGO CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.cod_cancelamento
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb09662#SITUACAO PEDIDO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.situacao_pedido
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb08177#VALOR FRETE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.valor_frete
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb34265#VALOR OUTROS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.valor_outras
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb02661#CODIGO PORTADOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.cod_portador
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb16701#TABELA PRECO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.tab_preco
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb02672#CODIGO TRANSACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.codigo_transacao;

         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        aplicacao,
              usuario_rede,      maquina_rede,
              num01,             long01
              )
         VALUES
           ( 'SUPR_090',        'I',
              sysdate,           ws_aplicativo,
              ws_usuario_rede,   ws_maquina_rede,
              :new.pedido_compra,long_aux
                 );
      end if;

      if updating
      then
         long_aux := '';
         v_teve  := 'n';

         long_aux := long_aux  ||
         inter_fn_buscar_tag('lb09880#PEDIDO DE COMPRA',ws_locale_usuario,ws_usuario_systextil) ||
               chr(10)         ||
               chr(10);

         if :old.codigo_empresa <> :new.codigo_empresa
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb07207#EMPRESA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.codigo_empresa ||'->'||
                                                      :new.codigo_empresa
                                                   || chr(10);
         end if;

         if :old.dt_emis_ped_comp <> :new.dt_emis_ped_comp
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb00142#DATA EMISSAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.dt_emis_ped_comp ||'->'||
                                                      :new.dt_emis_ped_comp
                                                   || chr(10);
         end if;

         if :old.cond_pgto_compra <> :new.cond_pgto_compra
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb02130#COND.PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.cond_pgto_compra ||'->'||
                                                      :new.cond_pgto_compra
                                                   || chr(10);
         end if;

         if :old.forn_ped_forne9 <> :new.forn_ped_forne9 or
            :old.forn_ped_forne4 <> :new.forn_ped_forne4 or
            :old.forn_ped_forne2 <> :new.forn_ped_forne2
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb00699#FORNECEDOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.forn_ped_forne9 || '/' ||
                                                      :old.forn_ped_forne4 || '-' ||
                                                      :old.forn_ped_forne2 || '->'||
                                                      :new.forn_ped_forne9 || '/' ||
                                                      :new.forn_ped_forne4 || '-' ||
                                                      :new.forn_ped_forne2
                                                   || chr(10);
         end if;

         if :old.data_prev_entr <> :new.data_prev_entr
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb00617#DATA PREV. ENTREGA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.data_prev_entr ||'->'||
                                                      :new.data_prev_entr
                                                   || chr(10);
         end if;

         if :old.tran_ped_forne9 <> :new.tran_ped_forne9 or
            :old.tran_ped_forne4 <> :new.tran_ped_forne4 or
            :old.tran_ped_forne2 <> :new.tran_ped_forne2
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb05956#TRANSPORTADORA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.tran_ped_forne9 || '/' ||
                                                      :old.tran_ped_forne4 || '-' ||
                                                      :old.tran_ped_forne2 ||'->'||
                                                      :new.tran_ped_forne9 || '/' ||
                                                      :new.tran_ped_forne4 || '-' ||
                                                      :new.tran_ped_forne2
                                                   || chr(10);
         end if;

         if :old.tipo_frete <> :new.tipo_frete
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb07856#TIPO FRETE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.tipo_frete ||'->'||
                                                      :new.tipo_frete
                                                   || chr(10);
         end if;

         if :old.cod_end_entrega <> :new.cod_end_entrega
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb03557#ENDERECO ENTREGA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.cod_end_entrega ||'->'||
                                                      :new.cod_end_entrega
                                                   || chr(10);
         end if;

         if :old.cod_end_cobranca <> :new.cod_end_cobranca
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb03553#ENDERECO DE COBRANCA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.cod_end_cobranca ||'->'||
                                                      :new.cod_end_cobranca
                                                   || chr(10);
         end if;

         if :old.codigo_comprador <> :new.codigo_comprador
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb02587#CODIGO COMPRADOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.codigo_comprador ||'->'||
                                                      :new.codigo_comprador
                                                   || chr(10);
         end if;

         if :old.vendedor_contato <> :new.vendedor_contato
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb34988#CONTATO VENDEDOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.vendedor_contato ||'->'||
                                                      :new.vendedor_contato
                                                   || chr(10);
         end if;

         if :old.cod_cancelamento <> :new.cod_cancelamento
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb02582#CODIGO CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.cod_cancelamento ||'->'||
                                                      :new.cod_cancelamento
                                                   || chr(10);
         end if;

         if :old.situacao_pedido <> :new.situacao_pedido
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb09662#SITUACAO PEDIDO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.situacao_pedido ||'->'||
                                                      :new.situacao_pedido
                                                   || chr(10);
         end if;

         if :old.valor_frete <> :new.valor_frete
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb08177#VALOR FRETE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.valor_frete ||'->'||
                                                      :new.valor_frete
                                                   || chr(10);
         end if;

         if :old.valor_outras <> :new.valor_outras
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb34265#VALOR OUTROS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.valor_outras ||'->'||
                                                      :new.valor_outras
                                                   || chr(10);
         end if;

         if :old.cod_portador <> :new.cod_portador
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb02661#CODIGO PORTADOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.cod_portador ||'->'||
                                                      :new.cod_portador
                                                   || chr(10);
         end if;

         if :old.tab_preco <> :new.tab_preco
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb16701#TABELA PRECO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.tab_preco ||'->'||
                                                      :new.tab_preco
                                                   || chr(10);
         end if;

         if :old.codigo_transacao <> :new.codigo_transacao
         then
            v_teve  := 's';
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb02672#CODIGO TRANSACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.codigo_transacao ||'->'||
                                                      :new.codigo_transacao;
         end if;

         -- Veririca se foi alterada alguma informacao
         if v_teve  = 's'
         then

            INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        aplicacao,
              usuario_rede,      maquina_rede,
              num01,             long01
              )
            VALUES
            ( 'SUPR_090',        'A',
               sysdate,           ws_aplicativo,
               ws_usuario_rede,   ws_maquina_rede,
               :new.pedido_compra,long_aux
            );

         end if;
      end if;

      if deleting
      then
         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        aplicacao,
              usuario_rede,      maquina_rede,
              num01
              )
         VALUES
           ( 'SUPR_090',        'D',
              sysdate,           ws_aplicativo,
              ws_usuario_rede,   ws_maquina_rede,
              :new.pedido_compra
              );
      end if;
   end if;
end inter_tr_supr_090_log;

-- ALTER TRIGGER "INTER_TR_SUPR_090_LOG" ENABLE
 

/

exec inter_pr_recompile;

