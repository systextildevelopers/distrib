
  CREATE OR REPLACE PROCEDURE "INTER_PR_ENTR_DOC_FISCAL_ITEM" (
   p_codigo_empresa in number,
   p_nivel          in varchar2,
   p_grupo          in varchar2,
   p_subgrupo       in varchar2,
   p_item           in varchar2
)
IS
   v_perm_entr_doc    varchar2(1);
   v_item_ativo       number;
begin
   -- le a ENTRADA DE DOCUMENTOS FISCAIS PARA ITENS INATIVOS da FATU_503
   begin
      select fatu_503.perm_entr_doc_fiscal_item_inat
      into v_perm_entr_doc
      from  fatu_503
      where fatu_503.codigo_empresa = p_codigo_empresa ;
   exception when OTHERS then
      v_perm_entr_doc := 'S';
   end;

   -- verifica se a ENTRADA DE DOCUMENTOS FISCAIS PARA ITENS INATIVOS e valida, ou seja, igual a ZERO (ATIVO)
   if v_perm_entr_doc = 'N'
   then
      begin
         select nvl(basi_010.item_ativo,0)
         into v_item_ativo
         from basi_010
         where basi_010.nivel_estrutura  = p_nivel
           and basi_010.grupo_estrutura  = p_grupo
           and basi_010.subgru_estrutura = p_subgrupo
           and basi_010.item_estrutura   = p_item;
      exception when OTHERS then
         v_item_ativo := 0;
      end;

      if v_item_ativo = 1
      then
         raise_application_error (-20000, 'ATENCAO! Nao e permitido a entrada de itens inativos.');
      end if;
   end if;

end inter_pr_entr_doc_fiscal_item;

 

/

exec inter_pr_recompile;

