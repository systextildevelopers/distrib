ALTER TABLE HDOC_030 ADD ALTERA_FLUXO_SOLIC_MANU NUMBER(1);
COMMENT ON COLUMN HDOC_030.ALTERA_FLUXO_SOLIC_MANU IS '1 se tiver permissão para alterar o fluxo de solicitação de manutenção';
