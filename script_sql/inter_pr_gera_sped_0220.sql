create or replace procedure inter_pr_gera_sped_0220 (p_cod_empresa   IN NUMBER,
													 p_dat_final     IN DATE,
													 p_des_erro     out varchar2) is

begin
 
	BEGIN
		insert into sped_0220 (cod_empresa,cod_nivel,cod_grupo,cod_subgrupo,cod_item,cod_unid_cov,fator_conv)
		SELECT  		sped_c170.cod_empresa, 
								sped_c170.cod_nivel, 
								sped_c170.cod_grupo,
								nvl(sped_c170.cod_subgru, ' '),
								nvl(sped_c170.cod_item, ' '),
								sped_c170.unid_med_conver,
								sum(sped_c170.qtde_item_fatur)/sum(sped_c170.qtde_conver) fator_conv
		   FROM   sped_c170,sped_c100,fatu_504
		   WHERE    sped_c170.cod_empresa       = sped_c100.cod_empresa
		   and      fatu_504.codigo_empresa     = sped_c100.cod_empresa
		   and      sped_c170.num_nota_fiscal   = sped_c100.num_nota_fiscal
		   and      sped_c170.cod_serie_nota    = sped_c100.cod_serie_nota
		   and      sped_c170.num_cnpj_9        = sped_c100.num_cnpj_9
		   and      sped_c170.num_cnpj_4        = sped_c100.num_cnpj_4
		   and      sped_c170.num_cnpj_2        = sped_c100.num_cnpj_2
		   and      sped_c170.cod_empresa       = p_cod_empresa
		   and 		  sped_c170.qtde_conver					  > 0 
		   and 		  sped_c170.unid_med_conver 			<> sped_c170.cod_unid_medida
		   and      sped_c100.tip_entrada_saida = sped_c170.tip_entrada_saida
		   and      sped_c100.cod_modelo not in ('06','07','08','09','10','11','22','27','28','57','59')
		   and      sped_c100.ind_nf_consumo    = 'N'
		   and 		(sped_c100.sig_estado                <> 'EX' or (sped_c100.dat_averbacao         <= p_dat_final))
		   and not (fatu_504.tipo_sped = 0 
					and sped_c100.cod_modelo in ('55','65','59') 
					and sped_c100.sig_estado <> 'EX' 
					and (sped_c100.tip_entrada_saida = 'S' or sped_c100.tip_emissao_prop = 0))
		   and      sped_c100.cod_situacao_nota not in (2,3,5)
		   and 		(sped_c100.flag_exp = 'N' or sped_c100.sig_estado = 'EX')
		   group by sped_c170.cod_empresa, sped_c170.cod_nivel, sped_c170.cod_grupo, sped_c170.cod_subgru, sped_c170.cod_item, sped_c170.unid_med_conver;

    EXCEPTION
       WHEN dup_val_on_index THEN
       NULL;

                       
    END;

    BEGIN
		insert into sped_0220 (cod_empresa,cod_nivel,cod_grupo,cod_subgrupo,cod_item,cod_unid_cov,fator_conv)
		SELECT  distinct		sped_c170.cod_empresa, 
								sped_c170.cod_nivel, 
								sped_c170.cod_grupo,
								nvl(sped_c170.cod_subgru, ' '),
								nvl(sped_c170.cod_item, ' '),
								sped_c170.cod_unid_medida,
								1 fator_conv
		   FROM   sped_c170,sped_c100,fatu_504
		   WHERE    sped_c170.cod_empresa       = sped_c100.cod_empresa
		   and      fatu_504.codigo_empresa     = sped_c100.cod_empresa
		   and      sped_c170.num_nota_fiscal   = sped_c100.num_nota_fiscal
		   and      sped_c170.cod_serie_nota    = sped_c100.cod_serie_nota
		   and      sped_c170.num_cnpj_9        = sped_c100.num_cnpj_9
		   and      sped_c170.num_cnpj_4        = sped_c100.num_cnpj_4
		   and      sped_c170.num_cnpj_2        = sped_c100.num_cnpj_2
		   and      sped_c170.cod_empresa       = p_cod_empresa
     and     trim(sped_c170.cod_unid_medida) not in (select s.cod_unid_medida from sped_0200 s
                                                     where s.cod_empresa = sped_c170.cod_empresa 
                                                     and trim(s.cod_nivel) is null 
                                                     and trim(s.cod_item) is null 
                                                     and replace(s.num_classifi_fiscal,'.') = replace(sped_c170.cod_classific_fiscal,'.'))
     and trim(sped_c170.cod_nivel) is null
     and exists  (select 1 from sped_0200 s
                 where s.cod_empresa = sped_c170.cod_empresa 
                 and trim(s.cod_nivel) is null 
                 and trim(s.cod_item) is null 
                 and replace(s.num_classifi_fiscal,'.') = replace(sped_c170.cod_classific_fiscal,'.'))
		   and      sped_c100.tip_entrada_saida = sped_c170.tip_entrada_saida
		   and      sped_c100.cod_modelo not in ('06','07','08','09','10','11','22','27','28','57','59')
		   and      sped_c100.ind_nf_consumo    = 'N'
		   and 		(sped_c100.sig_estado                <> 'EX' or (sped_c100.dat_averbacao         <= p_dat_final))
		   and not (fatu_504.tipo_sped = 0 
					and sped_c100.cod_modelo in ('55','65','59') 
					and sped_c100.sig_estado <> 'EX' 
					and (sped_c100.tip_entrada_saida = 'S' or sped_c100.tip_emissao_prop = 0))
		   and      sped_c100.cod_situacao_nota not in (2,3,5)
		   and 		(sped_c100.flag_exp = 'N' or sped_c100.sig_estado = 'EX');

     EXCEPTION
       WHEN dup_val_on_index THEN
       NULL;

                 
     END;

   
END inter_pr_gera_sped_0220;

/
exec inter_pr_recompile;
