 create or replace view inter_vi_nf_saida_sped310 as
select c.codigo_empresa    cod_empresa,
       c.num_nota_fiscal   documento,
       c.serie_nota_fisc   serie,
       c.natop_nf_est_oper estado,
       c.data_emissao      data_emissao,  
       sum(i.base_icms + i.rateio_despesa) valor_operacao,
       i.perc_icms          perc_icms_est,    
       sum(i.valor_icms)   valor_icms,
       i.perc_icms_uf_dest perc_dest,  
        ABS(i.perc_icms_uf_dest - i.perc_icms)   perc_difal,
       sum(i.base_icms + i.rateio_despesa) *  ABS((i.perc_icms_uf_dest - i.perc_icms))/100 base_difal,
       sum(round(i.val_icms_uf_dest,2) ) valor_icms_dest,
       sum(round((i.val_icms_uf_dest + i.val_fcp_uf_dest),2)) total_difal,
       sum(round(i.val_icms_uf_remet,2)) valor_icms_remet,
       i.perc_fcp_uf_dest,
       sum(round(i.val_fcp_uf_dest,2)) valor_fcp
from fatu_050 c, fatu_060 i, pedi_080 p, pedi_010 d
where i.ch_it_nf_cd_empr  = c.codigo_empresa
  and i.ch_it_nf_num_nfis = c.num_nota_fiscal
  and i.ch_it_nf_ser_nfis = c.serie_nota_fisc
  and i.natopeno_nat_oper = p.natur_operacao
  and i.natopeno_est_oper = p.estado_natoper
  and c.cod_status         = '100' /*autorizada*/
  and c.situacao_nfisc    in (1,4) /*emitida*/
  and c.cgc_9              = d.cgc_9
  and c.cgc_4              = d.cgc_4                     
  and c.cgc_2              = d.cgc_2   
  and (d.insc_est_cliente     = 'ISENTO' or p.consumidor_final = 'S')  
  and (i.val_icms_uf_dest > 0.00 or i.val_fcp_uf_dest > 0.00)
group by c.codigo_empresa,      
         c.num_nota_fiscal,
         c.serie_nota_fisc,      
         c.natop_nf_est_oper,
         c.data_emissao,         
         i.perc_icms,    
         i.perc_icms_uf_dest,   
         ABS(i.perc_icms_uf_dest - i.perc_icms) ,
         i.perc_fcp_uf_dest;         
exec inter_pr_recompile;
