ALTER TABLE OBRF_180
ADD (COD_LOCAL_ORCAMENTO VARCHAR2(1) default ' ',
    COD_MSG_ORCAMENTO NUMBER(6) default 0,
    COD_SEQ_ORCAMENTO  NUMBER(2) default 0);

exec inter_pr_recompile;
