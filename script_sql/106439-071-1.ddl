create or replace view inter_vi_rcnb_030 as
select r.codigo_empresa,
       r.nivel_estrutura,
       r.grupo_estrutura,
       r.subgru_estrutura,
       r.item_estrutura,
       r.tipo_parametro,
       r.cnpj9,
       r.cnpj4,
       r.cnpj2
  from rcnb_030 r
 where (not exists (select 1 from rcnb_032 rr where rr.codigo_empresa = r.codigo_empresa)
        or  exists (select 1
                   from rcnb_032 rr
                  where rr.codigo_empresa = r.codigo_empresa
                    and rr.parametro_cd1 = '0'
                    and rr.parametro_cd2 = '0'
                    and rr.parametro_cd3 = '0'
                    and rr.parametro_cd4 = '0'
                    and rr.parametro_cd5 = '0'
                    and rr.parametro_cp1 = '0'
                    and rr.parametro_cp2 = '0'
                    and rr.parametro_cp3 = '0'
                    and rr.parametro_cp4 = '0'
                    and rr.parametro_cp5 = '0')
        or (r.tipo_parametro = 1 and exists
            (select 1
                from rcnb_032 rr
               where rr.codigo_empresa = r.codigo_empresa
                 and (rr.parametro_cp1 = r.nivel_estrutura or
                     rr.parametro_cp2 = r.nivel_estrutura or
                     rr.parametro_cp3 = r.nivel_estrutura or
                     rr.parametro_cp3 = r.nivel_estrutura or
                     rr.parametro_cp4 = r.nivel_estrutura or
                     rr.parametro_cp5 = r.nivel_estrutura)))
        or (r.tipo_parametro = 2 and exists
            (select 1
                from rcnb_032 rr
               where rr.codigo_empresa = r.codigo_empresa
                 and (rr.parametro_cd1 = r.nivel_estrutura or
                     rr.parametro_cd2 = r.nivel_estrutura or
                     rr.parametro_cd3 = r.nivel_estrutura or
                     rr.parametro_cd3 = r.nivel_estrutura or
                     rr.parametro_cd4 = r.nivel_estrutura or
                     rr.parametro_cd5 = r.nivel_estrutura))))
 group by r.codigo_empresa,
          r.nivel_estrutura,
          r.grupo_estrutura,
          r.subgru_estrutura,
          r.item_estrutura,
          r.tipo_parametro,
          r.cnpj9,
          r.cnpj4,
          r.cnpj2;
