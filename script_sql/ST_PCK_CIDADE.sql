create or replace package ST_PCK_CIDADE as

    TYPE t_dados_basi_160 IS TABLE OF basi_160%rowtype;

    function get_dados_cod_cidade(p_cod_cidade number, p_msg_erro in out varchar2) return t_dados_basi_160;

end;
