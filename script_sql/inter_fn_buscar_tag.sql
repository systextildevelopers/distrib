
  CREATE OR REPLACE FUNCTION "INTER_FN_BUSCAR_TAG" (
      par_tag_programa  in varchar2,
      codigo_idioma     in varchar2,
      usuario_systextil in varchar2)
   return varchar2
is
   -- Declara as variaveis para a extracao do tag;
   tag_extraida            varchar2(255);
   texto_tag               long;
   ultima_tag              varchar2(255);
   ultima_tag_texto        long;
   var_erro_pt_br          number;

begin

   ultima_tag       := ' ';
   ultima_tag_texto := '';

   -- extrai somente o codigo da tag
   tag_extraida := inter_fn_extrai_tag(par_tag_programa);
   --tag_extraida := par_tag_programa;

   texto_tag      := '';
   var_erro_pt_br := 0;

   -- Se a tag for de tabela alocada, entao mandamos a mensagem de tabela alocada
   -- do tag DS21069
   -- Ja estava estava assim na funcao BUSCAR TAG
   if tag_extraida = 'ds00036' or tag_extraida = 'ds00371' or tag_extraida = 'ds00759'
   then
      texto_tag := inter_fn_extrai_tag('ds21069');
   else
      if ultima_tag <> tag_extraida
      then
         if tag_extraida <> ' '
         then
            if codigo_idioma = 'pt_BR'
            then
               begin
                  select hdoc_870.descricao
                  into texto_tag
                  from hdoc_870
                  where hdoc_870.tag    = tag_extraida
                    and hdoc_870.locale = codigo_idioma;
                exception
                when no_data_found then
                   var_erro_pt_br := 1;

                   if usuario_systextil = 'CONVERSAO'
                   then
                      texto_tag        := 'Tag nao encontrado';
                      ultima_tag       := ' ';
                      ultima_tag_texto := ' ';
                   else
                      begin
                         select hdoc_870.descricao
                         into texto_tag
                         from  hdoc_870
                         where hdoc_870.tag    = tag_extraida
                           and hdoc_870.locale = 'pt_BR';
                      exception
                      when no_data_found then
                         var_erro_pt_br   := 2;
                         texto_tag        := ' ';
                         ultima_tag       := ' ';
                         ultima_tag_texto := ' ';
                      end;

                      if var_erro_pt_br = 1
                      then
                         ultima_tag       := tag_extraida;
                         ultima_tag_texto := texto_tag;
                      end if;
                   end if;
                end;

                if var_erro_pt_br = 0
                then
                   ultima_tag       := tag_extraida;
                   ultima_tag_texto := texto_tag;
                end if;
             else
                begin
                  select hdoc_870.descricao
                  into texto_tag
                  from  hdoc_870
                  where hdoc_870.tag    = tag_extraida
                    and hdoc_870.locale = 'es_ES';
                exception
                when no_data_found then
                   var_erro_pt_br := 1;

                   if usuario_systextil = 'CONVERSAO'
                   then
                      texto_tag        := 'Tag nao encontrado';
                      ultima_tag       := ' ';
                      ultima_tag_texto := ' ';
                   else
                      texto_tag        := ' ';
                      ultima_tag       := ' ';
                      ultima_tag_texto := ' ';
                   end if;
                end;

                if var_erro_pt_br = 0
                then
                   ultima_tag       := tag_extraida;
                   ultima_tag_texto := texto_tag;
                end if;
             end if;
         end if;
      else
         texto_tag := ultima_tag_texto;
      end if;
   end if;

   return (texto_tag);
end;


 

/

exec inter_pr_recompile;

