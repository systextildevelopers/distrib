alter table supp_010
  drop constraint PK_SUPP_010;

drop index PK_SUPP_010;

ALTER TABLE SUPP_010
add  constraint PK_SUPP_010
PRIMARY KEY (numero_nota, serie,cod_empresa,cgc9,cgc4,cgc2,data_criacao,requisicao,parcela_prorrogada);
/
exec inter_pr_recompile;
