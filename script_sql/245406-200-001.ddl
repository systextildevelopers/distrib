CREATE TABLE EXPE_006 (
  nota_fiscal             NUMBER(9) NOT NULL,
  serie                   VARCHAR2(3) NOT NULL,
  cod_empresa             NUMBER(3) NOT NULL,
  situacao                NUMBER(2) DEFAULT 0,
  nota_fiscal_dev         NUMBER(9) NOT NULL,
  serie_dev               VARCHAR2(3) NOT NULL,
  cgc9_dev                NUMBER(9) DEFAULT 0,
  cgc4_dev                NUMBER(4) DEFAULT 0,
  cgc2_dev                NUMBER(2) DEFAULT 0,
  observacao              VARCHAR2(4000) DEFAULT '',
  data_inicio             DATE,
  data_ult_atualizacao    DATE
);

ALTER TABLE EXPE_006 ADD CONSTRAINT PK_EXPE_006 PRIMARY KEY (nota_fiscal, serie, cod_empresa);

/

exec inter_pr_recompile;
