insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_f119', 'Seleção de Depositos para pagamentos a vista', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_f119', 'NENHUM', 0, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Seleção de Depositos para pagamentos a vista'
 where hdoc_036.codigo_programa = 'pedi_f119'
   and hdoc_036.locale          = 'es_ES';
commit;
/
