declare
cursor supr_010_c is
   select rowid from supr_010 where tipo_frete = 0;
   
contaRegistro number;

begin
   contaRegistro := 0;
   
   for reg_supr_010_c in supr_010_c
   loop
      update supr_010
      set tipo_frete = 9
      where tipo_frete = 0
        and supr_010.rowid = reg_supr_010_c.rowid;
      
      contaRegistro := contaRegistro + 1;
      
      if contaRegistro = 100
      then
         contaRegistro := 0;
         commit;
      end if;
   end loop; 

   commit;
end;
