create table inte_264 (
    CEP	  				number(8,0)  not null,
    COD_CIDADE			varchar2(60) not null,
    ENDERECO   			varchar2(160),
	COMPLEMENTO   		varchar2(160),
	STATUS_INTEGRACAO   number(1),
	DESTINO				number(5),
	SIGLA				varchar2(5),
	ROTA_ENTREGA		number(7),
    DESCRICAO_ERRO      varchar2(4000),
    CONSTRAINT inte_264_origem_pk PRIMARY KEY (CEP) USING INDEX ENABLE
);


/
exec inter_pr_recompile;
