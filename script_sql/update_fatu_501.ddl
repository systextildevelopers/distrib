update fatu_501 
set fatu_501.permite_custo_nfe = 0
where fatu_501.permite_custo_nfe = 2;

commit work;

update fatu_501
set fatu_501.permite_custo_nfe = 3
where fatu_501.permite_custo_nfe = 1;

commit work;
