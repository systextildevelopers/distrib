
  CREATE OR REPLACE TRIGGER "INTER_TR_ESTQ_005" 
  before update of entrada_saida, atualiza_estoque on estq_005 
  for each row








declare
   v_nr_registros number;

begin
   select count(*) into v_nr_registros from estq_020 e
   where transacao = :old.codigo_transacao;

   if v_nr_registros > 0
   then
      raise_application_error(-20000,'Nao se pode alterar transacoes ja utilizadas pelo estoque.');
   end if;
end inter_tr_estq_005;




-- ALTER TRIGGER "INTER_TR_ESTQ_005" ENABLE
 

/

exec inter_pr_recompile;

