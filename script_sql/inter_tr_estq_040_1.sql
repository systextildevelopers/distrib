
  CREATE OR REPLACE TRIGGER "INTER_TR_ESTQ_040_1" 
after insert or delete or update of qtde_empenhada, qtde_estoque_atu
on estq_040
for each row
begin
   if INSERTING
   then
      if (:new.cditem_nivel99 = '2' or :new.cditem_nivel99 = '4')
	    and inter_fn_dep_pronta_entrega(:new.deposito)= 1
      and :new.qtde_empenhada <= (:new.qtde_estoque_atu - :new.qtde_empenhada)
      then
         delete from pedi_845
         where pedi_845.nivel    = :new.cditem_nivel99
           and pedi_845.grupo    = :new.cditem_grupo
           and pedi_845.subgrupo = :new.cditem_subgrupo
           and pedi_845.item     = :new.cditem_item
           and pedi_845.deposito = :new.deposito
           and pedi_845.lote     = :new.lote_acomp;
      end if;
   end if;

   if UPDATING
   then

      if (:new.cditem_nivel99 = '2' or :new.cditem_nivel99 = '4')
	    and inter_fn_dep_pronta_entrega(:new.deposito)= 1
      and :new.qtde_estoque_atu > :new.qtde_empenhada
      then
         delete from pedi_845
         where pedi_845.nivel    = :new.cditem_nivel99
           and pedi_845.grupo    = :new.cditem_grupo
           and pedi_845.subgrupo = :new.cditem_subgrupo
           and pedi_845.item     = :new.cditem_item
           and pedi_845.deposito = :new.deposito
           and pedi_845.lote     = :new.lote_acomp;
      end if;
   end if;
   if DELETING
   then
      delete from pedi_845
      where pedi_845.nivel    = :old.cditem_nivel99
        and pedi_845.grupo    = :old.cditem_grupo
        and pedi_845.subgrupo = :old.cditem_subgrupo
        and pedi_845.item     = :old.cditem_item
        and pedi_845.deposito = :old.deposito
        and pedi_845.lote     = :old.lote_acomp;
   end if;

end inter_tr_estq_040_1;
-- ALTER TRIGGER "INTER_TR_ESTQ_040_1" ENABLE
 

/

exec inter_pr_recompile;

