CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_100_6"
BEFORE INSERT on pedi_100
for each row

declare
  w_nr_dias       number(6);
  v_executa_trigger number(1);

begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;


   if v_executa_trigger = 0
   then
      if :new.data_prev_receb is null
      then
        begin
          select pedi_181.nr_dias into w_nr_dias from pedi_181
          where pedi_181.cod_empresa = :new.codigo_empresa
            and pedi_181.cnpj_9      = :new.cli_ped_cgc_cli9
            and pedi_181.cnpj_4      = :new.cli_ped_cgc_cli4
            and pedi_181.cnpj_2      = :new.cli_ped_cgc_cli2;
        exception
           when no_data_found then
           w_nr_dias := 0;
        end;

        if w_nr_dias > 0
        then
           :new.data_prev_receb := :new.data_entr_venda + w_nr_dias;
        end if;
      end if;
   end if;
end inter_tr_pedi_100_6;
/

exec inter_pr_recompile;
