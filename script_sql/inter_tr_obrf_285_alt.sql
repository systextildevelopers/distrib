create or replace trigger inter_tr_obrf_285_alt
before update on obrf_285
for each row

begin
  if :old.enviado = 1
  then
    :new.ENVIADO  := 0;
    :new.indretif := 2;
  end if;
end;
