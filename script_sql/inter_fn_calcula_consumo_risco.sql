
  CREATE OR REPLACE FUNCTION "INTER_FN_CALCULA_CONSUMO_RISCO" 
  (p_grupo_prod          in  varchar2,
   p_subgrupo_prod       in  varchar2,
   p_item_prod           in  varchar2,
   p_nivel_tecido        in  varchar2,
   p_grupo_tecido        in  varchar2,
   p_subgru_tecido       in  varchar2,
   p_alternativa_prod    in  number  ,
   p_sequencia_estrutura in  number  ,
   p_codigo_risco        in  number  )
return number
is
   v_tipo_corte           basi_060.tipo_corte_peca%type;
   v_gramatura_tecido     basi_020.gramatura_1%type;
   v_multiplicador        number(4);
   v_soma_marc            number(4,1);
   v_larg_calc            pcpc_200.largura%type;
   v_comp_calc            pcpc_200.comprimento%type;
   v_perdas_calc          pcpc_200.percent_perdas%type;
   v_comprimento_debrum   basi_060.comprimento_debrum%type;
   v_largura_debrum       basi_060.largura_debrum%type;
   v_consumo_liq          number(13,7);
   v_constante1           pcpc_200.constante1%type;
   v_constante2           pcpc_200.constante1%type;
   v_constante3           pcpc_200.constante1%type;
   v_constante4           pcpc_200.constante1%type;
   v_tipo_calculo         pcpc_200.opcao_calculo%type;
   v_opcao_larg_calc      pcpc_200.opcao_larg_calc%type;
   v_tubular_aberto       basi_020.tubular_aberto%type;
   v_calc_cons            number := 0;
   /*
      SS 56535/001 - 28/07/2010 - Jeferson.h - Lunender
   */
   v_perdas_metros_calc   pcpc_200.perc_perdas_mts%type;
   comp_unid_med          varchar2(2);
   v_perc_perdas_util     pcpc_200.percent_perdas%type;

begin
   -- Obtem o tipo de corte da peca.
   -- Valores da variavel v_tipo_corte...
   /* 1 - Enfesto normal
      2 - Debrum
      3 - Cadarco
      4 - Gola, Punhos, Apliques
      5 - Termotine, Popeline, Manta
      6 - Tiras
      7 - Balancin
      8 - Laser
   */
   begin
      select basi_060.tipo_corte_peca,             basi_060.comprimento_debrum,
             basi_060.largura_debrum
      into   v_tipo_corte,                         v_comprimento_debrum,
             v_largura_debrum
      from basi_060
      where  basi_060.grupo_estrutura   = p_grupo_prod
        and (basi_060.subgru_estrutura  = '000'
         or  basi_060.subgru_estrutura  = p_subgrupo_prod)
        and (basi_060.item_estrutura    = '000000'
         or  basi_060.item_estrutura    = p_item_prod)
        and  basi_060.alternativa_item  = p_alternativa_prod
        and  basi_060.sequencia         = p_sequencia_estrutura;
      exception when others then
         v_tipo_corte := 1;
   end;

   -- Obtem a gramatura do tecido.
   begin
      select basi_020.gramatura_1, basi_020.tubular_aberto
      into   v_gramatura_tecido,   v_tubular_aberto
      from basi_020
      where basi_020.basi030_nivel030 = p_nivel_tecido
        and basi_020.basi030_referenc = p_grupo_tecido
        and basi_020.tamanho_ref      = p_subgru_tecido;
      exception when others then
         v_gramatura_tecido := 1.000;
   end;

   -- Define o multiplicador.
   if v_gramatura_tecido > 0.999
   then
      v_multiplicador := 1000;
   else
      v_multiplicador := 1;
   end if;

   v_gramatura_tecido := (v_gramatura_tecido / v_multiplicador);

   -- Obtem a qtde de marcacoes.
   begin
      select nvl(sum(pcpc_200.qtde_marcacoes),0)
      into v_soma_marc
      from pcpc_200
      where pcpc_200.codigo_risco     = p_codigo_risco
        and pcpc_200.grupo_estrutura  = p_grupo_prod
        and pcpc_200.alternativa_item = p_alternativa_prod
        and pcpc_200.ordem_estrutura  = p_sequencia_estrutura;
   end;

   -- Obtem o percentual de perdas do risco
   begin
      select    nvl(max(pcpc_200.largura),0),                 nvl(max(pcpc_200.comprimento),0),
                nvl(max(pcpc_200.percent_perdas),0),          nvl(max(pcpc_200.constante1),0),
                nvl(max(pcpc_200.perc_perdas_mts),0),
                nvl(max(pcpc_200.constante2),0),              nvl(max(pcpc_200.constante3),0),
                nvl(max(pcpc_200.constante4),0),              nvl(max(pcpc_200.opcao_calculo),0),
                nvl(max(pcpc_200.opcao_larg_calc),0)
      into      v_larg_calc,                                  v_comp_calc,
                v_perdas_calc,                                v_constante1,
                v_perdas_metros_calc,
                v_constante2,                                 v_constante3,
                v_constante4,                                 v_tipo_calculo,
                v_opcao_larg_calc
      from pcpc_200
      where pcpc_200.codigo_risco     = p_codigo_risco
        and pcpc_200.grupo_estrutura  = p_grupo_prod
        and pcpc_200.alternativa_item = p_alternativa_prod
        and pcpc_200.ordem_estrutura  = p_sequencia_estrutura;

   end;

   /*
        SS 56535/001 - 28/07/2010 - Jeferson.h - Lunender
   */
   begin
      select basi_030.unidade_medida
      into comp_unid_med
      from basi_030
      where basi_030.nivel_estrutura = p_nivel_tecido
         and basi_030.referencia     = p_grupo_tecido;
   end;

   if comp_unid_med = 'MT' or comp_unid_med = 'M'
   then
      if v_perdas_calc = 0.000
      then
         v_perc_perdas_util := v_perdas_metros_calc;
      else
         v_perc_perdas_util := v_perdas_calc;
      end if;
   else
      v_perc_perdas_util := v_perdas_calc;
   end if;

   -- Se for debrum subtitui variaveis do risco com o valor da estrutura.
   if v_tipo_corte = 2
   then
      if v_comprimento_debrum > 0
      then
         v_comp_calc := v_comprimento_debrum;
      end if;

      if v_largura_debrum > 0
      then
         v_larg_calc := v_largura_debrum;
      end if;
   end if;

   if v_tipo_calculo = 1
   then
      if v_soma_marc > 0
      then
         -- (v_tipo_corte) Se nao e debrum, executa calculo abaixo...
         if v_tipo_corte <> 2
         then
            -- (v_tubular_aberto) 1 - Aberto, 2 - Tubular...
            if  v_tubular_aberto   = 2
            and v_opcao_larg_calc  = 1    --  1 - Com abertura do tecido tubular na mesa de enfesto ou tecidos para cortes SEAMLESS.
            then
               v_consumo_liq  := ((((v_larg_calc * 2) * v_comp_calc) * v_gramatura_tecido) * (100 + v_perc_perdas_util)/100) / v_soma_marc;
            else
               v_consumo_liq  := (((v_larg_calc * v_comp_calc) * v_gramatura_tecido) * (100 + v_perc_perdas_util)/100) / v_soma_marc;
            end if;
         else

            -- Se for debrum, nao analisa se o tecido e tubular ou aberto.
            v_consumo_liq  := (((v_larg_calc * v_comp_calc) * v_gramatura_tecido) * (100 + v_perc_perdas_util)/100);
         end if;

         if v_soma_marc > 0
         then
            v_calc_cons := v_comp_calc / v_soma_marc;
         end if;
         if v_perc_perdas_util > 0.000
         then
            v_calc_cons := v_calc_cons + (v_calc_cons / 100) * v_perc_perdas_util;
         end if;
      else
         v_consumo_liq  := 0;
      end if;
   else
      /*
         SS 53796-001 - Conforme email passado pelo cleinte as constantes obrigatorias no calculo sao a 1 e a 2.
      */
      if  (v_constante1 is not null and v_constante1 > 0)
      and (v_constante2 is not null and v_constante2 > 0)
      and  v_soma_marc > 0
      and  v_comp_calc > 0
      then
         if v_tipo_corte = 2 --Debrum
         then

            if v_largura_debrum > 0 and v_comprimento_debrum > 0
            then
               if  v_tubular_aberto   = 2
               and v_opcao_larg_calc  = 1    --  1 - Com abertura do tecido tubular na mesa de enfesto ou tecidos para cortes SEAMLESS.
               then
                  v_consumo_liq := (v_comprimento_debrum * v_largura_debrum * v_gramatura_tecido) * 2;
               else
                  v_consumo_liq := v_comprimento_debrum * v_largura_debrum * v_gramatura_tecido;
               end if;
            end if;

            v_consumo_liq := (v_consumo_liq + (v_consumo_liq * (v_perc_perdas_util / 100.00))) * 1000.000;
         else
            v_calc_cons := (((v_comp_calc + v_constante1) / v_soma_marc) * (1 + (v_constante2 / 100)) * (1 + (v_constante4 / 100))) + v_constante3;

            if   v_tubular_aberto   = 2
            and (v_opcao_larg_calc  = 1    /*  1 - Com abertura do tecido tubular na mesa de enfesto ou tecidos para cortes SEAMLESS. */
            or   v_tipo_corte       = 2)   /* 2 - DEBRUN */
            then
               v_consumo_liq := ((v_calc_cons * v_gramatura_tecido * v_larg_calc) * 2) / 100;
            else
               v_consumo_liq := (v_calc_cons * v_gramatura_tecido * v_larg_calc) / 100;
            end if;

            v_consumo_liq := v_consumo_liq * 100;
         end if;
      end if;
   end if;

   if comp_unid_med = 'KG'
   then

      v_consumo_liq := inter_fn_consumo_quebra_corte(p_nivel_tecido,  p_grupo_tecido,
                                                     p_subgru_tecido, v_consumo_liq);
      return(v_consumo_liq);

   else

      v_calc_cons := inter_fn_consumo_quebra_corte(p_nivel_tecido,  p_grupo_tecido,
                                                   p_subgru_tecido, v_calc_cons);

      return(v_calc_cons);

   end if;

end inter_fn_calcula_consumo_risco;

 

/

exec inter_pr_recompile;

