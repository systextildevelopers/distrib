create table supr_014 (
  fornecedor9 number(9) not null,
  fornecedor4 number(4) not null,
  fornecedor2 number(2) not null,
  cpf_dep     varchar2(11) not null,
  rel_dep     number(2) not null,
  descr_dep   varchar2(30)
);
