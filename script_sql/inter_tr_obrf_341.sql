CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_341"
BEFORE INSERT ON OBRF_341 FOR EACH ROW
DECLARE
    nextValue number;
BEGIN
    select ID_OBRF_341.nextval into nextValue from dual;
    :new.ID := nextValue;
END INTER_TR_OBRF_341;

/

exec inter_pr_recompile;
