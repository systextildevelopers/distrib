
  CREATE OR REPLACE PROCEDURE "INTER_PR_RESERVA_DATA_VALIDADE" 
is
   /*numero_reserva number;*/
begin
   for reg_produtos in (select tmrp_640.numero_reserva
                        /*into numero_reserva*/
                        from tmrp_640
                        where tmrp_640.data_validade < sysdate)
   loop
      begin
         update tmrp_640
         set tmrp_640.status = 3
         where tmrp_640.numero_reserva = reg_produtos.numero_reserva;
      end;

      begin
         update tmrp_645
         set tmrp_645.minutos_reserva = 0
         where tmrp_645.numero_reserva = reg_produtos.numero_reserva;
      end;
   end loop;
   commit;
end inter_pr_reserva_data_validade;

 

/

exec inter_pr_recompile;

