create table obrf_342 (
    id                number(9)     not null,
    id_devolucao      number(9)     not null,
    sequencia         number(5)     not null,
    produto_nivel     varchar2(1)   not null,
    produto_grupo     varchar2(5)   not null,
    produto_subgrupo  varchar2(3)   not null,
    produto_item      varchar2(6)   not null,
    quantidade        number(15,3)  not null
);

alter table obrf_342 add constraint pk_obrf_342 primary key (id);
alter table obrf_342 add constraint fk_obrf_342_id_devolucao foreign key (id_devolucao) references obrf_341 (id) on delete cascade;

comment on table obrf_342 is 'Integra��o Devolu��o E-Commerce - Itens';

create sequence id_obrf_342;

exec inter_pr_recompile;
/
