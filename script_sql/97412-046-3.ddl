declare 
   nro_registro number;
   
begin
   nro_registro := 0;
   

   for exec010 in (select rowid
                   from exec_010
                   where exec_010.familia_maq is null)
   loop
      begin
         update exec_010
         set    exec_010.familia_maq = 0
         where  exec_010.rowid       = exec010.rowid;
      end;
      
      nro_registro := nro_registro + 1;
      
      if nro_registro > 1000
      then
         commit;
         nro_registro := 0;
      end if;
   end loop;
   
   commit;
end;


/
