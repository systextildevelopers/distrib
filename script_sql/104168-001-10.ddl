create table PEDI_392 ( 
  CODIGO_EMPRESA NUMBER(3)   default 0 not null,
  ESTADO         VARCHAR2(2) default '' not null,
  ORIG_PROD      NUMBER(1)   default 0 not null,
  TIPO_DESCONTO  NUMBER(3)   default 0,
  PERC_DESCONTO  NUMBER(6,2) default 0.00);

comment on COLUMN PEDI_392.CODIGO_EMPRESA  is 'Empresa onde sera analisada a excecao';
comment on COLUMN PEDI_392.ESTADO          is 'Estado destino para excecao';
comment on COLUMN PEDI_392.CODIGO_EMPRESA  is 'Tipo do desconto';
comment on COLUMN PEDI_392.PERC_DESCONTO   is 'PERCENTUAL DE DESCONTO';
comment on COLUMN PEDI_392.ORIG_PROD       is 'ORIGEM DO PRODUTO 0 - proprio, 1 - importado, 2 - comprado';


alter table PEDI_392
  add constraint PK_PEDI_392 primary key (CODIGO_EMPRESA, ESTADO, orig_prod);
  
/

exec inter_pr_recompile;
/
