alter table estq_180 add cod_agrupador_artigo_produto number(3,0) default 0;

update estq_180
    set cod_agrupador_artigo_produto = 0
where cod_agrupador_artigo_produto is null;

alter table estq_180
drop constraint "ESTQ_180_PK";

alter table estq_180
add constraint "ESTQ_180_PK" PRIMARY KEY (CODIGO_EMPRESA,PONTOS_MAIOR,LARG_MENOR,LARG_MAIOR,METROS_MAIOR,COD_AGRUPADOR_ARTIGO_PRODUTO) ENABLE;
