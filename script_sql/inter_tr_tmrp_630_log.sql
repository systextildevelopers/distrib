
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_630_LOG" 
after insert or
      update of
      ordem_planejamento, area_producao, ordem_prod_compra, nivel_produto,
      grupo_produto, subgrupo_produto, item_produto, qtde_conserto,
      qtde_2qualidade, qtde_perdas, qtde_produzida_comprada, pedido_venda,
      programado_comprado, alternativa_produto, qtde_atendida_estoque, erro_recalculo,
      pedido_reserva, qtde_programada_orig, codigo_risco
      or delete
on tmrp_630
for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_nome_programa          tmrp_630_log.programa%type;

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   
   ws_nome_programa := inter_fn_nome_programa(ws_sid);

   if inserting
   then
      begin
         insert into tmrp_630_log (
            data_ocorrencia,             usuario,
            operacao,                    usuario_rede,
            maquina_rede,                programa,

            ordem_planejamento_new,      pedido_venda_new,
            pedido_reserva_new,          area_producao_new,
            ordem_prod_compra_new,       nivel_produto_new,

            grupo_produto_new,           subgrupo_produto_new,
            item_produto_new,            alternativa_produto_new,
            programado_comprado_new
         ) values (
            sysdate,                     ws_usuario_systextil,
            0,                           ws_usuario_rede,
            ws_maquina_rede,             ws_nome_programa,

            :new.ordem_planejamento,     :new.pedido_venda,
            :new.pedido_reserva,         :new.area_producao,
            :new.ordem_prod_compra,      :new.nivel_produto,

            :new.grupo_produto,          :new.subgrupo_produto,
            :new.item_produto,           :new.alternativa_produto,
            :new.programado_comprado
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'TMRP_630_LOG(001-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   elsif updating
   then
      begin
         insert into tmrp_630_log (
            data_ocorrencia,             usuario,
            operacao,                    usuario_rede,
            maquina_rede,                programa,

            ordem_planejamento_new,      pedido_venda_new,
            pedido_reserva_new,          area_producao_new,
            ordem_prod_compra_new,       nivel_produto_new,

            grupo_produto_new,           subgrupo_produto_new,
            item_produto_new,            alternativa_produto_new,
            programado_comprado_new,

            ordem_planejamento_old,      pedido_venda_old,
            pedido_reserva_old,          area_producao_old,
            ordem_prod_compra_old,       nivel_produto_old,

            grupo_produto_old,           subgrupo_produto_old,
            item_produto_old,            alternativa_produto_old,
            programado_comprado_old
         ) values (
            sysdate,                     ws_usuario_systextil,
            1,                           ws_usuario_rede,
            ws_maquina_rede,             ws_nome_programa ,

            :new.ordem_planejamento,     :new.pedido_venda,
            :new.pedido_reserva,         :new.area_producao,
            :new.ordem_prod_compra,      :new.nivel_produto,

            :new.grupo_produto,          :new.subgrupo_produto,
            :new.item_produto,           :new.alternativa_produto,
            :new.programado_comprado,

            :old.ordem_planejamento,     :old.pedido_venda,
            :old.pedido_reserva,         :old.area_producao,
            :old.ordem_prod_compra,      :old.nivel_produto,

            :old.grupo_produto,          :old.subgrupo_produto,
            :old.item_produto,           :old.alternativa_produto,
            :old.programado_comprado);
      end;
   elsif deleting
   then
      insert into tmrp_630_log (
         data_ocorrencia,             usuario,
         operacao,                    usuario_rede,
         maquina_rede,                programa,

         ordem_planejamento_old,      pedido_venda_old,
         pedido_reserva_old,          area_producao_old,
         ordem_prod_compra_old,       nivel_produto_old,

         grupo_produto_old,           subgrupo_produto_old,
         item_produto_old,            alternativa_produto_old,
         programado_comprado_old
      ) values (
         sysdate,                     ws_usuario_systextil,
         2,                           ws_usuario_rede,
         ws_maquina_rede,             ws_nome_programa ,

         :old.ordem_planejamento,     :old.pedido_venda,
         :old.pedido_reserva,         :old.area_producao,
         :old.ordem_prod_compra,      :old.nivel_produto,

         :old.grupo_produto,          :old.subgrupo_produto,
         :old.item_produto,           :old.alternativa_produto,
         :old.programado_comprado);
   end if;

end inter_tr_tmrp_630_log;

-- ALTER TRIGGER "INTER_TR_TMRP_630_LOG" ENABLE
 

/

exec inter_pr_recompile;

