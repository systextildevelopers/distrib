
  CREATE OR REPLACE FUNCTION "INTER_FN_CALC_EM_PROD_ORDEM" (p_ordem_producao            in number,
                                                       p_codigo_estagio_consulta   in number,      p_empr_atu_estagios_seguintes    in number,
                                                       p_empr_deposito_conserto    in number,      p_ultimo_estagio                 in number)
RETURN number is
-- Variavel a ser retornada no calculo da funcao
   v_qtde_em_producao_pacote       pcpc_040.qtde_em_producao_pacote%type;

begin

      begin
         select nvl(sum(pcpc_040.qtde_em_producao_pacote),0)
         into    v_qtde_em_producao_pacote
         from pcpc_040
         where pcpc_040.ordem_producao   = p_ordem_producao
           and pcpc_040.codigo_estagio   = p_codigo_estagio_consulta;   ---- Codigo do estagio
--           and rownum                    = 1
--         group by pcpc_040.estagio_anterior,  pcpc_040.estagio_depende;
      exception when others then
         v_qtde_em_producao_pacote := 0.00000;
      end;

/*
      begin
         select pcpc_040.estagio_anterior,     pcpc_040.estagio_depende,
                sum(pcpc_040.qtde_pecas_prog), sum(pcpc_040.qtde_pecas_prod),
                sum(pcpc_040.qtde_pecas_2a),   sum(pcpc_040.qtde_conserto),
                sum(pcpc_040.qtde_perdas),     nvl(sum(pcpc_040.qtde_em_producao_pacote),0),
                nvl(sum(pcpc_040.qtde_a_produzir_pacote),0)
        into    v_estagio_anterior,            v_estagio_depende,
                V_qtde_pecas_prog,             v_qtde_pecas_prod ,
                v_qtde_pecas_2a,               v_qtde_conserto,
                v_qtde_perdas,                 v_qtde_em_producao_pacote,
                v_qtde_a_produzir_pacote
         from pcpc_040
         where pcpc_040.ordem_producao   = p_ordem_producao
           and pcpc_040.codigo_estagio   = p_codigo_estagio_consulta   ---- Codigo do estagio
           and rownum                    = 1
         group by pcpc_040.estagio_anterior,  pcpc_040.estagio_depende;
         exception
             when others then
                v_estagio_anterior  := 0.00000;
                v_estagio_depende   := 0.00000;
                V_qtde_pecas_prog   := 0.00000;
                v_qtde_pecas_prod   := 0.00000;
                v_qtde_pecas_2a     := 0.00000;
                v_qtde_conserto     := 0.00000;
                v_qtde_perdas       := 0.00000;
      end;

      if SQL%found   --- Quando encontrou registro
      then
         if v_estagio_anterior > 0     ---- Se existe estagio anterior ao que estamos analisando
         then
            if v_estagio_depende > 0   ---- Se o estagio que estamos analisando e simultaneo
            then

               begin
                  select sum(pcpc_040.qtde_pecas_prod)
                  into v_qtde_prod_ant
                  from pcpc_040
                  where pcpc_040.ordem_producao   = p_ordem_producao
                    and pcpc_040.codigo_estagio   = v_estagio_depende;
                  exception
                  when others then
                     v_qtde_prod_ant := 0.00000;
               end;

               if p_empr_atu_estagios_seguintes = 1
               then
                  begin
                     select sum(pcpc_040.qtde_pecas_2a),  sum(pcpc_040.qtde_perdas)
                     into   v_qtde_prod_2a_ant,           v_qtde_prod_perda_ant
                     from pcpc_040
                     where pcpc_040.ordem_producao   = p_ordem_producao
                       and pcpc_040.codigo_estagio   = v_estagio_depende;
                     exception
                     when others then
                        v_qtde_prod_2a_ant    := 0.00000;
                        v_qtde_prod_perda_ant := 0.00000;
                  end;

                  v_qtde_prod_ant := v_qtde_prod_ant + v_qtde_prod_2a_ant + v_qtde_prod_perda_ant;

               end if; -- if p_empresa_atu_estagios_seguintes

               v_qtde_em_producao := v_qtde_prod_ant;

               v_qtde_em_producao := v_qtde_em_producao - v_qtde_pecas_prod  - v_qtde_pecas_2a - v_qtde_perdas;

            else
               --- Nao tem estagios simultaneos para o estagio que estamos consultando

               begin
                  select pcpc_040.estagio_anterior
                  into v_est_ant
                  from pcpc_040
                  where pcpc_040.ordem_producao   = p_ordem_producao
                    and pcpc_040.codigo_estagio   = v_estagio_anterior
                    and rownum                    = 1;
                  exception
                  when others then
                     v_est_ant := 0.00000;

               end;

               -- Busca a quantidade produzida de todos os estagios cujo estagio anterior 63-pre-costura
               begin
                  select sum(pcpc_040.qtde_pecas_prod)
                  into v_qtde_prod_ant
                  from pcpc_040
                  where pcpc_040.ordem_producao   = p_ordem_producao
                    and pcpc_040.estagio_anterior = v_est_ant;

                  exception
                  when others then
                     v_qtde_prod_ant := 0.00000;

               end;

               -- Parametro que diz se e para avancar a qtdes de 2 e perda
               if p_empr_atu_estagios_seguintes = 1
               then
                  -- Busca a quantidade produzida de 2 qualidade e perda no estagio anterior  63 - costura

                  begin

                     select sum(pcpc_040.qtde_pecas_2a),  sum(pcpc_040.qtde_perdas)
                     into   v_qtde_prod_2a_ant,           v_qtde_prod_perda_ant
                     from pcpc_040
                     where pcpc_040.ordem_producao   = p_ordem_producao
                       and pcpc_040.estagio_anterior = v_est_ant;

                     exception
                     when others then
                        v_qtde_prod_2a_ant    := 0.00000;
                        v_qtde_prod_perda_ant := 0.00000;

                  end;


                  v_qtde_prod_ant := v_qtde_prod_ant + v_qtde_prod_2a_ant + v_qtde_prod_perda_ant;

               end if; --if p_empr_atu_estagios_seguintes

               v_qtde_em_producao := v_qtde_prod_ant;

               v_qtde_em_producao := v_qtde_em_producao - v_qtde_pecas_prod  - v_qtde_pecas_2a - v_qtde_perdas;

            end if;-- if v_estagio_depende

         else -- Caso seja o primeiro estagio fal o calculo simples

            v_qtde_em_producao := v_qtde_pecas_prog - v_qtde_pecas_prod  - v_qtde_pecas_2a - v_qtde_perdas;

         end if; -- if v_estagio_anterior

         -- Se for o ultimo estagio e tem deposito conserto abater a qtde de conserto

         if v_qtde_conserto              > 0
         and p_codigo_estagio_consulta   =  p_ultimo_estagio
         and p_empr_deposito_conserto    > 0
         and p_ultimo_estagio            > 0
         then
            v_qtde_em_producao := v_qtde_em_producao - v_qtde_conserto;
         end if;

      end if; -- if SQL%found

   return(v_qtde_em_producao);*/

   return(v_qtde_em_producao_pacote);

end INTER_FN_CALC_EM_PROD_ORDEM;


 

/

exec inter_pr_recompile;

