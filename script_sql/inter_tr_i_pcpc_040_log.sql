
  CREATE OR REPLACE TRIGGER "INTER_TR_I_PCPC_040_LOG" 
   after insert or delete or update
   on i_pcpc_040
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);

begin
      select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
      into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
      from sys.gv_$session
      where audsid  = userenv('SESSIONID')
        and inst_id = userenv('INSTANCE')
        and rownum < 2;

   if inserting
   then
      insert into i_pcpc_040_log
        (ordem_producao,          referencia,              lote_producao,
         situacao_ordem_ant,      situacao_ordem_atu,      codigo_cor,
         descricao_cor_ant,       descricao_cor_atu,       qtde_programada_ant,
         qtde_programada_atu,     tipo_ocorr,              data_ocorr,
         usuario_rede,            maquina_rede,            aplicacao)
      values
       (:new.ordem_producao,      :new.referencia,         :new.lote_producao,
        0,                        :new.situacao_ordem,     :new.codigo_cor,
        ' ',                      :new.descricao_cor,      0.00,
        :new.qtde_programada,     'I',                      sysdate,
        ws_usuario_rede,          ws_maquina_rede,          ws_aplicativo);
   end if;

   if updating
   then
      insert into i_pcpc_040_log
        (ordem_producao,          referencia,              lote_producao,
         situacao_ordem_ant,      situacao_ordem_atu,      codigo_cor,
         descricao_cor_ant,       descricao_cor_atu,       qtde_programada_ant,
         qtde_programada_atu,     tipo_ocorr,              data_ocorr,
         usuario_rede,            maquina_rede,            aplicacao)
      values
       (:new.ordem_producao,      :new.referencia,         :new.lote_producao,
        :old.situacao_ordem,      :new.situacao_ordem,     :new.codigo_cor,
        :old.descricao_cor,       :new.descricao_cor,      :old.qtde_programada,
        :new.qtde_programada,     'U',                      sysdate,
        ws_usuario_rede,          ws_maquina_rede,          ws_aplicativo);
   end if;

   if deleting
   then
      insert into i_pcpc_040_log
        (ordem_producao,          referencia,              lote_producao,
         situacao_ordem_ant,      situacao_ordem_atu,      codigo_cor,
         descricao_cor_ant,       descricao_cor_atu,       qtde_programada_ant,
         qtde_programada_atu,     tipo_ocorr,              data_ocorr,
         usuario_rede,            maquina_rede,            aplicacao)
      values
       (:old.ordem_producao,      :old.referencia,         :old.lote_producao,
        :old.situacao_ordem,      0,                       :old.codigo_cor,
        :old.descricao_cor,       ' ',                     :old.qtde_programada,
        0.00,                    'D',                      sysdate,
        ws_usuario_rede,          ws_maquina_rede,          ws_aplicativo);
   end if;
end inter_tr_i_pcpc_040_log;

-- ALTER TRIGGER "INTER_TR_I_PCPC_040_LOG" ENABLE
 

/

exec inter_pr_recompile;

