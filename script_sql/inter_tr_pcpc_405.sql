create or replace trigger inter_tr_pcpc_045
  before insert or update on pcpc_045  
  for each row
declare
  -- local variables here
begin
   if inserting and :new.hora_producao is not null
   then

      :new.hora_producao := to_date('16/11/1989 ' || to_CHAR(sysdate,'hh24:mi'),'dd/mm/yyyy hh24:mi');
   end if;
end inter_tr_pcpc_045;
/* versao: 2 */
