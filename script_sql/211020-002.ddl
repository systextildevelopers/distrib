declare tmpInt number;

cursor parametro_c is select codigo_empresa from fatu_500;

begin
  for reg_parametro in parametro_c
  loop
    select nvl(count(*),0) into tmpInt
    from fatu_500
    where fatu_500.codigo_empresa = reg_parametro.codigo_empresa
    and not exists (select 1 from empr_008
                    where codigo_empresa = fatu_500.codigo_empresa
		    and param = 'pedi.achaNat') ;

    if(tmpInt > 0)
    then

      
      begin
      insert into empr_008 ( codigo_empresa, param, val_int) 
            values (reg_parametro.codigo_empresa, 'pedi.achaNat', 0);
      end;
    end if;

  end loop;
  commit;
end;
