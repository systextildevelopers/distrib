alter table pcpb_015 add flag_controle_exp number(1);

comment on column pcpb_015.flag_controle_exp is 'Flag para identificar se foi exportado para o Infotint (inte_f665). Null: não foi exportado. 2: Foi exportado.';

exec inter_pr_recompile;
