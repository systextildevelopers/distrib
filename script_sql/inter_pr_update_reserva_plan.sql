
  CREATE OR REPLACE PROCEDURE "INTER_PR_UPDATE_RESERVA_PLAN" (
    p_ordem_planejamento  in number,
    p_pedido_venda        in number,
    p_pedido_reserva      in number,
    p_nivel_produto       in varchar2,
    p_grupo_produto       in varchar2,
    p_subgrupo_produto    in varchar2,
    p_item_produto        in varchar2,
    p_alternativa_produto in number,
    p_qtde_update         in number,
    p_seq_produto         in number,
    p_codigo_risco        in number)
is
   cursor tmrp625_reserva is
   select tmrp_625.nivel_produto,         tmrp_625.grupo_produto,
          tmrp_625.subgrupo_produto,      tmrp_625.item_produto,
          tmrp_625.consumo,               tmrp_625.alternativa_produto,
          tmrp_625.seq_produto,
          tmrp_625.relacao_banho,         tmrp_625.tipo_calculo,
          tmrp_625.cons_unid_med_generica
   from tmrp_625
   where  tmrp_625.ordem_planejamento           = p_ordem_planejamento
     and  tmrp_625.pedido_venda                 = p_pedido_venda
     and  tmrp_625.pedido_reserva               = p_pedido_reserva
     and  tmrp_625.nivel_produto_origem         = p_nivel_produto
     and  tmrp_625.grupo_produto_origem         = p_grupo_produto
     and  tmrp_625.subgrupo_produto_origem      = p_subgrupo_produto
     and  tmrp_625.item_produto_origem          = p_item_produto
     and (tmrp_625.alternativa_produto_origem   = p_alternativa_produto or p_alternativa_produto = 0)
     and  tmrp_625.seq_produto_origem           = p_seq_produto
   group by tmrp_625.nivel_produto,         tmrp_625.grupo_produto,
            tmrp_625.subgrupo_produto,      tmrp_625.item_produto,
            tmrp_625.consumo,               tmrp_625.alternativa_produto,
            tmrp_625.seq_produto,
            tmrp_625.relacao_banho,         tmrp_625.tipo_calculo,
            tmrp_625.cons_unid_med_generica;

   -- Declara as variaveis que serao utilizadas.
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_relacao_banho_emp       number(9,3);
   v_unidades_prog           number(9);
   v_tipo_produto            number(1);
   v_tipo_produto_origem     number(1);
   v_relacao_banho           number(9,3);
   v_qtde_reserva            number(15,5);
   v_consumo_risco           number(13,7);
   v_consumo_update          number(13,7);
begin
   v_qtde_reserva:= 0;

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- Obtem o volume da relacao de banho
   begin
      select empr_001.relacao_banho
      into v_relacao_banho_emp
      from empr_001;
      exception when no_data_found then
         v_relacao_banho_emp:= 0;
   end;

   -- Verifica se o produto passado  como  parametro e
   -- um tecido retilineo. Em caso positivo, os  teci-
   -- dos crus tambem devem atualizar as unidades.
   if p_nivel_produto = '2'
   then
      begin
         select basi_030.tipo_produto
         into   v_tipo_produto_origem
         from basi_030
         where basi_030.nivel_estrutura = p_nivel_produto
           and basi_030.referencia      = p_grupo_produto;
         exception when no_data_found then
            v_tipo_produto_origem := 0;
      end;
   end if;

   for reg_tmrp625 in tmrp625_reserva
   loop
      v_unidades_prog := 0;
      v_consumo_update :=  reg_tmrp625.consumo;

      if reg_tmrp625.nivel_produto = '2'
      then
         -- SS 49150-001 - TDV
         -- Leitura da basi_030 para saber se e retilineo ou normal
         -- Se for tecido retilineo nao calcula consumo pela funcao: inter_fn_calcula_consumo_risco
         -- O consumo que vale e o da estrutura.
         begin
            select basi_030.tipo_produto
            into   v_tipo_produto
            from basi_030
            where basi_030.nivel_estrutura = reg_tmrp625.nivel_produto
              and basi_030.referencia      = reg_tmrp625.grupo_produto;
         exception when no_data_found then
            v_tipo_produto:= 0;
         end;

         v_consumo_risco := 0;

         if p_codigo_risco > 0 and (v_tipo_produto = 1 or v_tipo_produto = 2 or v_tipo_produto = 5)    -- TECIDO NORMAL
         then
            v_consumo_risco := inter_fn_calcula_consumo_risco (p_grupo_produto,            p_subgrupo_produto,        p_item_produto,
                                                               reg_tmrp625.nivel_produto,  reg_tmrp625.grupo_produto, reg_tmrp625.subgrupo_produto,
                                                               p_alternativa_produto,      reg_tmrp625.seq_produto,   p_codigo_risco);
         end if;

         if v_consumo_risco > 0
         then
            v_consumo_update :=  v_consumo_risco;
         else
            v_consumo_update :=  reg_tmrp625.consumo;
         end if;

         if v_tipo_produto = 3 or v_tipo_produto = 4 or v_tipo_produto = 6   -- TECIDO RETILINEO
         then
            v_unidades_prog := round(p_qtde_update) * reg_tmrp625.cons_unid_med_generica;
         else
            v_unidades_prog := 0;
         end if;
      end if;

      if reg_tmrp625.nivel_produto = '4'
      then
         if v_tipo_produto_origem = 3 or v_tipo_produto_origem = 4 or v_tipo_produto_origem = 6
         then
            v_unidades_prog:= round(p_qtde_update) * reg_tmrp625.cons_unid_med_generica;
         else
            v_unidades_prog:= 0;
         end if;
      end if;

      if reg_tmrp625.nivel_produto = '5'
      then
         if reg_tmrp625.relacao_banho = 0
         then
            v_relacao_banho := v_relacao_banho_emp;
         else
            v_relacao_banho := reg_tmrp625.relacao_banho;
         end if;
      else
         v_relacao_banho := 0;
      end if;

      if reg_tmrp625.tipo_calculo = 2
      then
         -- TIPO CALCULO: Campo do cadastro de estrutura de receitas.
         -- "2 - Calculo de consumo com base no VOLUME DE BANHO do e-
         -- quipamento  onde  o produto sera produzido, identificando
         -- que se trata de produto  quimico de tinturaria."
         -- Fonte: Help do campo tipo_calculo, no programa basi_f395.
         v_qtde_reserva := v_consumo_update * (p_qtde_update * v_relacao_banho);
      else
         v_qtde_reserva := p_qtde_update * v_consumo_update;
      end if;

      begin
         update tmrp_625
         set    --tmrp_625.qtde_reserva_programada     = tmrp_625.qtde_reserva_programada     + v_qtde_reserva,
                tmrp_625.unidades_reserva_programada = tmrp_625.unidades_reserva_programada + v_unidades_prog,
/*                tmrp_625.qtde_reserva_programada     = decode(sign(tmrp_625.qtde_reserva_planejada  - (tmrp_625.qtde_reserva_programada + v_qtde_reserva)),-1,tmrp_625.qtde_reserva_planejada,
                                                                  (tmrp_625.qtde_reserva_programada + v_qtde_reserva))
*/

                tmrp_625.qtde_reserva_programada     = tmrp_625.qtde_reserva_programada + v_qtde_reserva
         where  tmrp_625.ordem_planejamento          = p_ordem_planejamento
           and  tmrp_625.pedido_venda                = p_pedido_venda
           and  tmrp_625.pedido_reserva              = p_pedido_reserva
           and  tmrp_625.nivel_produto_origem        = p_nivel_produto
           and  tmrp_625.grupo_produto_origem        = p_grupo_produto
           and  tmrp_625.subgrupo_produto_origem     = p_subgrupo_produto
           and  tmrp_625.item_produto_origem         = p_item_produto
           and (tmrp_625.alternativa_produto_origem  = p_alternativa_produto or p_alternativa_produto = 0)
           and  tmrp_625.seq_produto_origem          = p_seq_produto
           and  tmrp_625.nivel_produto               = reg_tmrp625.nivel_produto
           and  tmrp_625.grupo_produto               = reg_tmrp625.grupo_produto
           and  tmrp_625.subgrupo_produto            = reg_tmrp625.subgrupo_produto
           and  tmrp_625.item_produto                = reg_tmrp625.item_produto
           and  tmrp_625.alternativa_produto         = reg_tmrp625.alternativa_produto
           and  tmrp_625.seq_produto                 = reg_tmrp625.seq_produto;
         exception when others then
            -- ATENCAO! Nao atualizou {0}. Mensagem do banco de dados: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_625' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      if reg_tmrp625.nivel_produto = '3' or reg_tmrp625.nivel_produto = '5'
      then
         inter_pr_update_reserva_plan(p_ordem_planejamento,
                                      p_pedido_venda,
                                      p_pedido_reserva,
                                      reg_tmrp625.nivel_produto,
                                      reg_tmrp625.grupo_produto,
                                      reg_tmrp625.subgrupo_produto,
                                      reg_tmrp625.item_produto,
                                      reg_tmrp625.alternativa_produto,
                                      v_qtde_reserva,
                                      reg_tmrp625.seq_produto,
                                      0);
      end if;
   end loop;
end inter_pr_update_reserva_plan;



 

/

exec inter_pr_recompile;

