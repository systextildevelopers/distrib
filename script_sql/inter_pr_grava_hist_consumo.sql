
  CREATE OR REPLACE PROCEDURE "INTER_PR_GRAVA_HIST_CONSUMO" (p_programa         in varchar2,
                                                        p_codigo_risco     in number  ,
                                                        p_alternativa_item in number  ,
                                                        p_ordem_estrutura  in number  ,
                                                        p_grupo_estrutura  in varchar2) is

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_conteudo_seq            varchar2(2000);
   v_processo                varchar2(30);
   v_seq_hist                number(9);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_seq_hist := 0;

   if p_programa = 'pcpc_f220'
   then v_processo := inter_fn_buscar_tag('lb36496#DEFINIR COMO RISCO PADRAO',ws_locale_usuario,ws_usuario_systextil);
   else v_processo := inter_fn_buscar_tag('lb36497#ATUALIZAR CONSUMO DOS TAMANHOS',ws_locale_usuario,ws_usuario_systextil);
   end if;

   v_conteudo_seq := '                    ' || inter_fn_buscar_tag('lb36494#CADASTRO DE RISCO/ENCAIXE',ws_locale_usuario,ws_usuario_systextil) ||
                      chr(010) ||
                      chr(010) ||
                      inter_fn_buscar_tag('lb10822#NUMERO RISCO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(p_codigo_risco)     ||
                      chr(010) ||
                      inter_fn_buscar_tag('lb36642#REFERENCIA..:',ws_locale_usuario,ws_usuario_systextil) || ' ' || p_grupo_estrutura           ||
                      chr(010) ||
                      inter_fn_buscar_tag('lb31549#SEQ. ESTRUT.:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(p_ordem_estrutura)  ||
                      chr(010) ||
                      inter_fn_buscar_tag('lb36643#ALTERNATIVA.:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(p_alternativa_item) ||
                      chr(010) ||
                      chr(010) ||
                      inter_fn_buscar_tag('lb36644#ALTERADO O CONSUMO DA ESTRUTURA PELO BOTAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || v_processo;
   begin
      select max(hist_100.num04)
      into   v_seq_hist
      from hist_100
      where hist_100.tabela = 'PCPC_200'
        and hist_100.num01  = p_codigo_risco
        and hist_100.num02  = p_alternativa_item
        and hist_100.num03  = p_ordem_estrutura
        and hist_100.str01  = p_grupo_estrutura;
      exception
         when others then
            v_seq_hist := 0;
   end;

   v_seq_hist := v_seq_hist + 1;

   begin
      insert into hist_100
                 (hist_100.tabela,

                  hist_100.data_ocorr,             hist_100.aplicacao,
                  hist_100.usuario_rede,           hist_100.maquina_rede,
                  hist_100.usuario,

                  hist_100.num01,                  hist_100.num02,
                  hist_100.num03,                  hist_100.str01,
                  hist_100.num04,                  hist_100.long01)
             values
                 ('PCPC_200',

                  sysdate,                         p_programa,
                  ws_usuario_rede,                 ws_maquina_rede,
                  ws_usuario_systextil,

                  p_codigo_risco,                  p_alternativa_item,
                  p_ordem_estrutura,               p_grupo_estrutura,
                  v_seq_hist,                      v_conteudo_seq);
   end;
   commit;
end inter_pr_grava_hist_consumo;

 

/

exec inter_pr_recompile;

