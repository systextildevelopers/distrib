create table pedi_120_lnd (
	cod_empresa 		number(9) default 0,
	cod_condicao_pgto 	number(9) default 0,
	cod_bloqueio 		number(9) default 0
);

alter table pedi_120_lnd add 
	constraint PK_PEDI_120_LND primary key (cod_empresa,cod_condicao_pgto,cod_bloqueio);
	
EXEC inter_pr_recompile;
