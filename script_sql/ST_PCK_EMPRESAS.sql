create or replace package ST_PCK_EMPRESAS as
       
    type t_dados_fatu_500 is table of fatu_500%rowtype;

    type t_dados_fatu_503 is table of fatu_503%rowtype;
        
    function get_fatu_500(p_codigo_empresa number, p_msg_erro in out varchar2) return t_dados_fatu_500;

    function exists_by_cod_empresa(p_codigo_empresa number) return boolean;

    function get_fatu_503(p_codigo_empresa number, p_msg_erro in out varchar2) return t_dados_fatu_503;
   
end ST_PCK_EMPRESAS;
