declare
cursor parametro_c is select codigo_empresa from fatu_500;
tmpInt number;
formaAtu number;
begin

  select empr_002.forma_calculo_cardex into formaAtu from empr_002;
  for reg_parametro in parametro_c
  loop
  select count(*) into tmpInt
  from empr_008
  where empr_008.codigo_empresa = reg_parametro.codigo_empresa
  and empr_008.param = 'estq.formaCalcCardex' ;

  if(tmpInt = 0)
  then
    begin
       insert into empr_008 (
       codigo_empresa, param, val_int
       ) values (
       reg_parametro.codigo_empresa, 'estq.formaCalcCardex', formaAtu);
    end;
 end if;

commit;
 end loop;
end;

