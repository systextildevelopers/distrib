
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_331" 
   before insert
   on pcpc_331
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      :new.data_leitura    := sysdate;
      :new.hora_leitura    := sysdate;
      :new.usuario_leitura := ws_usuario_systextil;
      :new.processo        := ws_aplicativo;
      :new.maquina_rede    := ws_maquina_rede;
      :new.usuario_rede    := ws_usuario_rede;
   end if;

end inter_tr_pcpc_331;

-- ALTER TRIGGER "INTER_TR_PCPC_331" ENABLE
 

/

exec inter_pr_recompile;

