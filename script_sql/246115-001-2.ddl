alter table cpag_010 
add (
  VALOR_DESCONTO_MOEDA_AUX      NUMBER(15,2),
  VALOR_JUROS_MOEDA_AUX         NUMBER(15,2),
  VALOR_SALDO_MOEDA_AUX         NUMBER(15,2),
  VALOR_VAR_CAMBIAL				NUMBER(15,2)
);

alter table cpag_010 
modify VALOR_DESCONTO_MOEDA_AUX      NUMBER(15,2) default 0.00;

alter table cpag_010 
modify VALOR_JUROS_MOEDA_AUX         NUMBER(15,2) default 0.00;

alter table cpag_010 
modify VALOR_SALDO_MOEDA_AUX         NUMBER(15,2) default 0.00;

alter table cpag_010 
modify VALOR_VAR_CAMBIAL         NUMBER(15,2) default 0.00;

alter table fatu_070 
add (
  VALOR_DESCONTO_MOEDA_AUX      NUMBER(15,2),
  VALOR_JUROS_MOEDA_AUX         NUMBER(15,2),
  VALOR_SALDO_MOEDA_AUX         NUMBER(15,2),
  VALOR_VAR_CAMBIAL				NUMBER(15,2)
);

alter table fatu_070 
modify VALOR_DESCONTO_MOEDA_AUX      NUMBER(15,2) default 0.00;

alter table fatu_070 
modify VALOR_JUROS_MOEDA_AUX         NUMBER(15,2) default 0.00;

alter table fatu_070 
modify VALOR_SALDO_MOEDA_AUX         NUMBER(15,2) default 0.00;

alter table fatu_070 
modify VALOR_VAR_CAMBIAL         NUMBER(15,2) default 0.00;
