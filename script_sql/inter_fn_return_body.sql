create or replace FUNCTION "INTER_FN_RETURN_BODY" ( p_sql in varchar2, 
                                                    p_var in varchar2,
                                                    p_is_numeric boolean)
                                                        
return  varchar2 is
   listReturnBody  varchar2(4000);
begin
    if p_var is not null then
        if p_is_numeric = false then
            FOR SPLITAR_GENERIC IN (    SELECT REGEXP_SUBSTR (p_var,
                                                '[^:]+',
                                                1,
                                                LEVEL)
                                    TXT
                            FROM DUAL
                    CONNECT BY REGEXP_SUBSTR (p_var,
                                                '[^:]+',
                                                1,
                                                LEVEL)
                                    IS NOT NULL)
            LOOP
                if listReturnBody is not null then
                    listReturnBody := listReturnBody || ', ''' || SPLITAR_GENERIC.TXT || '''';
                else
                    listReturnBody :=  p_sql || SPLITAR_GENERIC.TXT || '''' ;
                end if;
            END LOOP;
        else
            FOR SPLITAR_GENERIC IN (    SELECT REGEXP_SUBSTR (p_var,
                                                '[^:]+',
                                                1,
                                                LEVEL)
                                    TXT
                            FROM DUAL
                    CONNECT BY REGEXP_SUBSTR (p_var,
                                                '[^:]+',
                                                1,
                                                LEVEL)
                                    IS NOT NULL)
            LOOP
                if listReturnBody is not null then
                    listReturnBody := listReturnBody || ', ' || SPLITAR_GENERIC.TXT;
                else
                    listReturnBody :=  p_sql || SPLITAR_GENERIC.TXT;
                end if;
            END LOOP;
        end if;

        listReturnBody := listReturnBody || ' ) ';
    else 
        listReturnBody := '';
    end if;

    return listReturnBody; 
end inter_fn_return_body;

/

exec inter_pr_recompile;

