create or replace package ST_PCK_RELAC_CONTABIL_CC as

    type t_dados_cont_555 is table of cont_555%rowtype;

    function get_dados_cont_555(p_cod_plano_conta number, p_cod_reduzido number, p_msg_erro in out varchar2) return t_dados_cont_555;

    function relac_cont_cc(p_cod_plano_conta number, p_msg_erro in out varchar2) return t_dados_cont_555; 

end;
