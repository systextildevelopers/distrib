create table empr_073
(
CNPJ9_FORNEC NUMBER(9) default 0,
CNPJ4_FORNEC NUMBER(4) default 0,
CNPJ2_FORNEC NUMBER(2) default 0,
CODIGO_CONTABIL  NUMBER(6)  default 0,
CODIGO_TRANSACAO NUMBER(3)  default 0,
CODIGO_HISTORICO_CONT NUMBER(4)  default 0,
TIPO_TITULO NUMBER(3) default 0
);

alter table empr_073
  add constraint PK_empre_073 primary key (cnpj9_fornec, cnpj4_fornec, cnpj2_fornec, tipo_titulo);

/
exec inter_pr_recompile;
