alter table pedi_104 add (
  tipo_registro         number(1)     default 0,
  total_pronta_entrega  number(13,3)  default 0
);

exec inter_pr_recompile;
