alter table obrf_016
add centro_custo number(9);

alter table obrf_016 modify centro_custo default 0;

exec inter_pr_recompile;
/
