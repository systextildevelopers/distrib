alter table obrf_825 add cod_ajuste varchar2(10) default ' ';

ALTER TABLE obrf_825
 DROP CONSTRAINT PK_OBRF_825;
 
drop index PK_OBRF_825;

ALTER TABLE obrf_825 ADD CONSTRAINT PK_OBRF_825 PRIMARY KEY (COD_EMPRESA, MES, ANO, ID, COD_MENSAGEM, cod_ajuste, DOC_ARRECADACAO);
 
exec inter_pr_recompile;
