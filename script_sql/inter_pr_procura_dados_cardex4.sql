CREATE OR REPLACE PROCEDURE "INTER_PR_PROCURA_DADOS_CARDEX4"
   (p_codigo_empresa        in     number,    p_nivel                 in     varchar2,
    p_grupo                 in     varchar2,  p_subgrupo              in     varchar2,
    p_item                  in     varchar2,  p_data_cardex           in     date,
    p_saldo_fisico          in out number,    p_saldo_financeiro      in out number,
    p_preco_medio_unit      in out number,    p_saldo_financeiro_proj in out number,
    p_preco_medio_unit_proj in out number,    p_saldo_financeiro_est  in out number,
    p_preco_medio_unit_est  in out number,    p_saldo_fisico_quilo    in out number)
is
BEGIN

   select estq_301.saldo_fisico,              estq_301.saldo_financeiro,
          estq_301.preco_medio_unitario,      estq_301.saldo_financeiro_proj,
          estq_301.preco_medio_unit_proj,     estq_301.saldo_financeiro_estimado,
          estq_301.preco_medio_unit_estimado, nvl(estq_301.saldo_fisico_quilo, 0)
   into   p_saldo_fisico,                     p_saldo_financeiro,
          p_preco_medio_unit,                 p_saldo_financeiro_proj,
          p_preco_medio_unit_proj,            p_saldo_financeiro_est,
          p_preco_medio_unit_est,             p_saldo_fisico_quilo
   from estq_301, (

      select max(estq_301.mes_ano_movimento) mes_ano_movimento
      from estq_301
      where estq_301.codigo_deposito    = 0
        and estq_301.nivel_estrutura    = p_nivel
        and estq_301.grupo_estrutura    = p_grupo
        and estq_301.subgrupo_estrutura = p_subgrupo
        and estq_301.item_estrutura     = p_item
        and estq_301.saldo_fisico is not null
        and estq_301.mes_ano_movimento  < p_data_cardex
        and estq_301.codigo_empresa     = p_codigo_empresa
      ORDER BY estq_301.mes_ano_movimento asc) estq301

   where estq_301.codigo_deposito    = 0
     and estq_301.nivel_estrutura    = p_nivel
     and estq_301.grupo_estrutura    = p_grupo
     and estq_301.subgrupo_estrutura = p_subgrupo
     and estq_301.item_estrutura     = p_item
     and estq_301.mes_ano_movimento  = estq301.mes_ano_movimento
     and estq_301.codigo_empresa     = p_codigo_empresa;

   exception
      when no_data_found 
      then begin
         select nvl(sum(estq_301.saldo_fisico),0),         
             nvl(sum(estq_301.saldo_financeiro),0),
             nvl(round(sum(estq_301.saldo_financeiro) / sum(estq_301.saldo_fisico),5),0),      
             nvl(sum(estq_301.saldo_financeiro_proj),0),
             nvl(round(sum(estq_301.saldo_financeiro_proj) / sum(estq_301.saldo_fisico),5),0),     
             nvl(sum(estq_301.saldo_financeiro_estimado),0),
             nvl(round(sum(estq_301.saldo_financeiro_estimado)  / sum(estq_301.saldo_fisico),5),0), 
             nvl(sum(nvl(estq_301.saldo_fisico_quilo, 0)),0)
         into   p_saldo_fisico,                     p_saldo_financeiro,
                p_preco_medio_unit,                 p_saldo_financeiro_proj,
                p_preco_medio_unit_proj,            p_saldo_financeiro_est,
                p_preco_medio_unit_est,             p_saldo_fisico_quilo
         from estq_301, (

            select max(estq_301.mes_ano_movimento) mes_ano_movimento
            from estq_301
            where estq_301.nivel_estrutura    = p_nivel
              and estq_301.grupo_estrutura    = p_grupo
              and estq_301.subgrupo_estrutura = p_subgrupo
              and estq_301.item_estrutura     = p_item
              and estq_301.saldo_fisico is not null
              and estq_301.mes_ano_movimento  < p_data_cardex
			  and estq_301.codigo_empresa     = 0
              and exists (select 1 from basi_205 
                          where basi_205.codigo_deposito = estq_301.codigo_deposito
                            and basi_205.local_deposito  = p_codigo_empresa
							and basi_205.codigo_deposito > 0)
            ORDER BY estq_301.mes_ano_movimento asc) estq301
         where estq_301.nivel_estrutura    = p_nivel
           and estq_301.grupo_estrutura    = p_grupo
           and estq_301.subgrupo_estrutura = p_subgrupo
           and estq_301.item_estrutura     = p_item
           and estq_301.mes_ano_movimento  = estq301.mes_ano_movimento
		   and estq_301.codigo_empresa     = 0
           and exists (select 1 from basi_205 
                       where basi_205.codigo_deposito = estq_301.codigo_deposito
                         and basi_205.local_deposito = p_codigo_empresa
						 and basi_205.codigo_deposito > 0);
         exception
         when others then
           p_saldo_fisico          := 0.000;
           p_saldo_financeiro      := 0.000;
           p_preco_medio_unit      := 0.000;
           p_saldo_financeiro_proj := 0.000;
           p_preco_medio_unit_proj := 0.000;
           p_saldo_financeiro_est  := 0.000;
           p_preco_medio_unit_est  := 0.000;
           p_saldo_fisico_quilo    := 0.000;
      end;
      when others then
         p_saldo_fisico          := 0.000;
         p_saldo_financeiro      := 0.000;
         p_preco_medio_unit      := 0.000;
         p_saldo_financeiro_proj := 0.000;
         p_preco_medio_unit_proj := 0.000;
         p_saldo_financeiro_est  := 0.000;
         p_preco_medio_unit_est  := 0.000;
         p_saldo_fisico_quilo    := 0.000;
end inter_pr_procura_dados_cardex4;

/

exec inter_pr_recompile;
/
