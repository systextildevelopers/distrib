DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'seq_principal';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'seq_principal',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'um_faturamento_um';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'um_faturamento_um',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'um_faturamento_qtde';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'um_faturamento_qtde',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'lote_empenhado';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'lote_empenhado',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'codigo_embalagem';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'codigo_embalagem',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'seq_ped_compra';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'seq_ped_compra',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'perc_mao_obra';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'perc_mao_obra',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'centro_custo';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'centro_custo',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cod_nat_op';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cod_nat_op',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'trans_natu_op';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'trans_natu_op',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'RENDIMENTO_PROD';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'RENDIMENTO_PROD',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'data_lanc_desat';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'data_lanc_desat',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f142';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cod_ped_cliente';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cod_ped_cliente',                 0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/
