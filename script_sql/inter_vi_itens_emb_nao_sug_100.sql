create or replace view inter_vi_itens_emb_nao_sug_100 as
select a.pedido_venda, 
       a.cd_it_pe_nivel99, 
       a.cd_it_pe_grupo, 
       a.cd_it_pe_subgrupo, 
       a.cd_it_pe_item,
       b.grupo_embarque,
       nvl(sum(a.qtde_sugerida),0) qtde_sugerida
from pedi_110 a, basi_590 b
where a.cod_cancelamento = 0
  and b.nivel = a.cd_it_pe_nivel99
  and b.grupo = a.cd_it_pe_grupo
  and b.subgrupo = a.cd_it_pe_subgrupo
  and b.item = a.cd_it_pe_item  
group by a.pedido_venda, a.cd_it_pe_nivel99, a.cd_it_pe_grupo, a.cd_it_pe_subgrupo, a.cd_it_pe_item, b.grupo_embarque
having sum(a.qtde_pedida - a.qtde_faturada) <> sum(a.qtde_sugerida)
and sum(a.qtde_pedida - a.qtde_faturada) > 0;
