
  CREATE OR REPLACE TRIGGER "INTER_TR_ESTQ_200" 
before insert
    or update of nivel, grupo, subgrupo, item, lote
on estq_200
for each row

begin
   select agrupador into :new.agrupador from estq_080
   where nivel_estrutura = :new.nivel
     and grupo_estrutura = :new.grupo
     and sub_estrutura   = :new.subgrupo
     and item_estrutura  = :new.item
     and lote_produto    = :new.lote;

   exception when others
   then :new.agrupador := 0;

end inter_tr_estq_080;
-- ALTER TRIGGER "INTER_TR_ESTQ_200" ENABLE
 

/

exec inter_pr_recompile;

