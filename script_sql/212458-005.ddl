create table pedi_411 
(
  cliente9         NUMBER(9) default 0 not null,
  cliente4         NUMBER(4) default 0 not null,
  cliente2         NUMBER(2) default 0 not null,
  status           NUMBER(1) default 0,
  data_atualizacao DATE,
  usuario varchar2(100) default '',
  homologacao      NUMBER(5) default 0
);

alter table PEDI_411
add constraint PK_PEDI_411 primary key (CLIENTE9, CLIENTE4, CLIENTE2);
