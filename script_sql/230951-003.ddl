CREATE table "MANU_011" (
    "ID"              NUMBER,
    "SOLICITACAO"     NUMBER(6,0),
    "COD_EMPRESA"     NUMBER(3,0),
    "TIPO_SERVICO"    NUMBER(5,0),
    "COD_FUNCIONARIO" NUMBER
);

alter table "MANU_011" add constraint  "MANU_011_PK" primary key ("ID","COD_EMPRESA");

CREATE sequence "MANU_011_SEQ";

alter table "MANU_011" add
constraint "MANU_011_UK" 
unique ("SOLICITACAO","COD_EMPRESA","TIPO_SERVICO","COD_FUNCIONARIO");  
