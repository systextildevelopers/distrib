create or replace procedure p_gera_NFe(p_cod_empresa    in  number,
                                       p_cod_id         in  number,
                                       p_str_entr_said  in  varchar2,  -- E ou S
                                       p_str_usuario    in  varchar2,
                                       p_msg_erro       out varchar2 ) is
-- Created on 6/2/2009 by ANDRE


  cursor c_NFeI is  -- ENTRADAS
      select obrf_158.nfe_ambiente as ambiente
             ,trim(to_char(estado_nfe.codigo_fiscal_uf,'00')) as cod_cidade
             ,substr(to_char(o.data_transacao,'DD/MM/YY'),7,2) as ano
             ,f500.cgc_9
             ,f500.cgc_4
             ,f500.cgc_2
             ,o.cgc_cli_for_9 as cgc_dest_9
             ,o.cgc_cli_for_4 as cgc_dest_4
             ,o.cgc_cli_for_2 as cgc_dest_2
             ,o.serie
             ,o.documento
             ,o.documento as documento_inut
             ,'INUTILIZACAO DE NOTA FISCAL ELETRONICA' as justificativa
             ,o.versao_systextilnfe
             ,pedi_080.estado_natoper as estado_dest
             ,s010.nome_fornecedor as dest_nome
      from obrf_010 o, basi_160 cidade_nfe, obrf_158, fatu_500 f500, basi_167 estado_nfe, pedi_080, supr_010 s010
      where f500.codigo_empresa     = o.local_entrega
        and cidade_nfe.cod_cidade   = f500.codigo_cidade
        and cidade_nfe.estado       = estado_nfe.estado
        and cidade_nfe.codigo_pais  = estado_nfe.codigo_pais
        and o.cod_status            = '997'
        and o.cod_solicitacao_nfe   = p_cod_id
        and obrf_158.cod_empresa    = p_cod_empresa
        and o.natoper_nat_oper      = pedi_080.natur_operacao
        and o.natoper_est_oper      = pedi_080.estado_natoper
        and s010.fornecedor9        = o.cgc_cli_for_9
        and s010.fornecedor4        = o.cgc_cli_for_4
        and s010.fornecedor2        = o.cgc_cli_for_2;

  cursor c_NFsI is  -- SAIDAS
      select obrf_158.nfe_ambiente as ambiente
             ,trim(to_char(estado_nfe.codigo_fiscal_uf,'00')) as cod_cidade
             ,substr(to_char(f.data_emissao,'DD/MM/YY'),7,2) as ano
             ,f500.cgc_9
             ,f500.cgc_4
             ,f500.cgc_2
             ,f.cgc_9 as cgc_dest_9
             ,f.cgc_4 as cgc_dest_4
             ,f.cgc_2 as cgc_dest_2
             ,f.serie_nota_fisc
             ,f.num_nota_fiscal
             ,f.num_nota_fiscal as documento_inut
             ,'INUTILIZACAO DE NOTA FISCAL ELETRONICA' as justificativa
             ,f.versao_systextilnfe
      from fatu_050 f, pedi_010 p010, basi_160 cidade_nfe, obrf_158, fatu_500 f500, basi_167 estado_nfe
      where p010.cgc_9              = f.cgc_9
        and p010.cgc_4              = f.cgc_4
        and p010.cgc_2              = f.cgc_2
        and f500.codigo_empresa     = f.codigo_empresa
        and cidade_nfe.cod_cidade   = f500.codigo_cidade
        and  cidade_nfe.estado      = estado_nfe.estado
        and  cidade_nfe.codigo_pais = estado_nfe.codigo_pais
        and f.cod_status            = '997'
        and f.cod_solicitacao_nfe   = p_cod_id
        and obrf_158.cod_empresa    = p_cod_empresa;


  -- CONSULTA DE STATUS DA NFE


  cursor c_NFestat is  -- ENTRADAS
      select obrf_158.nfe_ambiente as ambiente
             ,o.numero_danf_nfe
             ,4 as tipo_documento
             ,o.documento
             ,o.serie
             ,o.nr_recibo
             ,o.versao_systextilnfe
      from obrf_010 o, obrf_158
      where o.local_entrega       = p_cod_empresa
        and o.cod_status          = '995'
        and o.cod_solicitacao_nfe = p_cod_id
        and obrf_158.cod_empresa  = o.local_entrega;

  -- CONSULTA DE STATUS DA NFE

  cursor c_NFsstat is  -- SAIDAS
      select obrf_158.nfe_ambiente as ambiente
             ,f.numero_danf_nfe
             ,4 as tipo_documento
             ,f.num_nota_fiscal
             ,f.serie_nota_fisc
             ,f.nr_recibo
             ,f.versao_systextilnfe
      from fatu_050 f, obrf_158
      where f.codigo_empresa = p_cod_empresa
        and f.cod_status = '995'
        and f.cod_solicitacao_nfe = p_cod_id
        and obrf_158.cod_empresa  = f.codigo_empresa;


  -- NOTAS DE ENTRADAS
  cursor c_NFe is
      select o.numero_danf_nfe         /*1*/
      ,null                            /*2*/
      ,null                            /*3*/
      ,substr(Replace(p_080.cod_natureza || substr(to_char(p_080.divisao_natur,'00'),3,1),'.','') || ' ' ||p_080.descr_nat_oper,1,60) as natoper_nat_oper              /*4*/
      ,0                     /*5 Devera vir da supr_050 campo ainda nao existe*/
      ,'55'                            /*6*/
      ,o.serie                         /*7*/
      ,o.documento                     /*8*/
      ,o.data_emissao as o_data_emissao                  /*9*/
      ,to_date(null,'') as data_saida                 /*10*/   -- claudio
      ,'0' as entrada_saida            /*11*/
      ,to_number(trim(trim(to_char(estado_nfe.codigo_fiscal_uf,'00')) || trim(to_char(cidade_nfe.codigo_fiscal,'00000')))) as cod_cidade_ibge      /*12*/
      ,p.formato_danfe as formato_danfe                         /*13*/ -- rafaelsalvador
      ,1                               /*14*/ -- NORMAL
      ,p.nfe_ambiente                  /*15*/
      ,0                               /*16*/ -- EMITIDO PELO CONTRIBUINTE NO PROPRIO SISTEMA
      ,'5'                             /*17*/ -- Vers?o do Systextil
      ,1                               /*18*/ -- FINALIDADE
      ,f500.cgc_9 as f_cgc_9           /*19*/
      ,f500.cgc_4 as f_cgc_4           /*20*/
      ,f500.cgc_2 as f_cgc_2           /*21*/
      ,f500.nome_empresa               /*22*/
      ,f500.nome_empresa as f500_nome_fantasia                       /*23*/
      ,f500.endereco as f_endereco                                   /*24*/
      ,f500.numero as f_numero                                       /*25*/
      ,f500.complemento as complemento                               /*26*/
      ,f500.bairro as f_bairro                                       /*27*/
      ,to_number(trim(trim(to_char(estado_nfe.codigo_fiscal_uf,'00')) || trim(to_char(cidade_nfe.codigo_fiscal,'00000')))) as cidade_emit_cod_cidade_ibge  /*28*/
      ,inter_fn_retira_acentuacao(cidade_nfe.cidade) as cidade_emit_cidade                      /*29*/
      ,cidade_nfe.estado as cidade_emit_estado                      /*30*/
      ,trim(to_char(estado_nfe.codigo_fiscal_uf,'00'))               /*31*/
      ,f500.cep_empresa                                              /*32*/
      ,to_number(substr(trim(to_char(cidade_nfe.ddd,'0000')),3,2) || trim(to_char(f500.telefone,'00000000'))) as f500_telefone_emit /*33*/
      ,pais_nfe.codigo_fiscal cod_pais_fiscal_emit                   /*34*/
      ,pais_nfe.nome_pais  as pais_nfe_nome_pais_emit                /*35*/
      ,replace(substr(replace(f500.inscr_estadual,'.',''),1,14),'-','') as inscr_est_emit             /*36*/
      ,f012.ie_substituto                            /*37*/
      ,null             /*38*/
      ,null                            /*39*/
      ,s010.fornecedor9                /*40*/
      ,s010.fornecedor4                /*41*/
      ,s010.fornecedor2                /*42*/
      ,s010.nome_fornecedor            /*43*/
      ,s010.endereco_forne             /*44*/
      ,s010.numero_imovel as s010_numero_imovel_forn                      /*45*/
      ,s010.complemento as s010_complemento_forn                          /*46*/
      ,s010.bairro as s010_bairro_forn                                    /*47*/
      ,to_number(trim(trim(to_char(estado_dest.codigo_fiscal_uf,'00')) || trim(to_char(cidade_dest.codigo_fiscal,'00000')))) as cidade_cod_cidade_ibge_forn      /*48*/
      ,inter_fn_retira_acentuacao(decode(cidade_dest.estado,'EX',cidade_dest.cidade || ' - ' || pais_dest.nome_pais,cidade_dest.cidade)) as cidade_nfe_cidade_forn /*49*/
      ,cidade_dest.estado as cidade_nfe_estado_forn                        /*50*/
      ,pais_dest.nome_pais as pais_forn_nome_pais                         /*51*/
      ,to_number(substr(trim(to_char(cidade_dest.ddd,'0000')),3,2) || trim(to_char(s010.telefone_forne,'00000000'))) as f500_telefone_forn /*52*/
      ,decode(cidade_dest.estado,'EX',null,s010.cep_fornecedor) as cep_fornecedor                /*53*/
      ,pais_dest.codigo_fiscal as pais_forn_codigo_pais                   /*54*/
      /*leituras entradas*/
      ,replace(substr(replace(s010.inscr_est_forne,'.',''),1,14),'-','') as f_inscr_estadual_forn  /*55*/
      ,null                            /*56*/
      ,null                            /*57*/
      ,null                            /*58*/
      ,null                            /*59*/
      ,null                            /*60*/
      ,null                            /*61*/
      ,null                            /*62*/
      ,null                            /*63*/
      ,null                            /*64*/
      ,null                            /*65*/
      ,null                            /*66*/
      ,f500.cgc_9 cd_cli_cgc_cli9            /*67*/
      ,f500.cgc_4 cd_cli_cgc_cli4            /*68*/
      ,f500.cgc_2 cd_cli_cgc_cli2            /*69*/
      ,f500.endereco    as p150_end_entr_cobr                 /*70*/
      ,f500.numero      as p150_numero_imovel                  /*71*/
      ,f500.complemento as complemento_endereco                     /*72*/
      ,f500.bairro    as  bairro_entr_cobr                              /*73*/
      ,to_number(trim(trim(to_char(estado_entr.codigo_fiscal_uf,'00')) || trim(to_char(cidade_entr.codigo_fiscal,'00000')))) as cidade_entr_cod_cidade     /*74*/
      ,inter_fn_retira_acentuacao(cidade_entr.cidade) as cidade_entr_cidade                  /*75*/
      ,cidade_entr.estado as cidade_entr_estado                  /*76*/
      ,decode(o.valor_icms, 0.00, 0.00, o.base_icms) as base_icms                     /*77*/      -- claudio
      ,o.valor_icms                    /*78*/
      ,decode(o.valor_icms_sub,0.00,0.00,o.base_icms_sub) base_icms_sub               /*79*/
      ,o.valor_icms_sub                /*80*/
      ,o.valor_itens                   /*81*/
      ,o.valor_frete                   /*82*/
      ,o.valor_seguro                  /*83*/
      ,o.valor_desconto                /*84*/
      ,0.00                            /*85*/
      ,o.valor_total_ipi               /*86*/
      ,0.00                            /*87*/
      ,0.00                            /*88*/
      ,o.valor_despesas                /*89*/
      ,o.total_docto                   /*90*/
      ,null                            /*91*/
      ,null                            /*92*/
      ,null                            /*93*/
      ,null                            /*94*/
      ,null                            /*95*/
      ,null                            /*96*/
      ,null                            /*97*/
      ,null                            /*98*/
      ,null                            /*99*/
      ,null                            /*100*/
      ,null                            /*101*/
      ,null                            /*102*/
      ,decode(o.tipo_frete,1,0,2,1,3,2,9,9,0) as tipo_frete    /*103*/
      ,o.transpa_forne9 as trans_cgc9                  /*104*/
      ,o.transpa_forne4 as trans_cgc4                  /*105*/
      ,o.transpa_forne2 as trans_cgc2                  /*106*/
      ,transp.nome_fornecedor as trans_nome            /*107*/
      ,substr(replace(replace(transp.inscr_est_forne,'.',''),'-',''),1,14)  as trans_insest /*108*/
      ,transp.endereco_forne as trans_end              /*109*/
      ,inter_fn_retira_acentuacao(cidade_trans.cidade) as trans_cidade             /*110*/
      ,cidade_trans.estado as trans_estado             /*111*/
      ,null                            /*112*/
      ,null                            /*113*/
      ,null                            /*114*/
      ,null                            /*115*/
      ,null                            /*116*/
      ,null                            /*117*/
      ,null                            /*118*/
      ,null                            /*119*/
      ,null                            /*120*/
      ,null                            /*121*/
      ,null                            /*122*/
      ,null                            /*123*/
      ,o.qtde_volumes                  /*124*/
      ,o.especie_volumes               /*125*/
      ,o.marca_volumes                 /*126*/
      ,o.numero_volume                 /*127*/
      ,o.peso_liquido                  /*128*/
      ,o.peso_bruto                    /*129*/
      ,null                            /*130*/
      ,null                            /*131*/
      ,null                            /*132*/
      ,null                            /*133*/
      ,null                            /*134*/
      ,null                            /*135*/
      ,null                            /*136*/
      ,o.local_entrega as cod_empresa  /*137*/
      ,p_cod_id                        /*138*/
      ,p_str_usuario                   /*139*/
      ,o.nfe_contigencia               /*140*/
      ,s010.e_mail                     /*141*/
      ,s010.nfe_e_mail                 /*142*/
      ,h_030.user_id_e_mail            /*143*/
      ,h_030.e_mail as f_user_e_mail   /*144*/
      ,h_030.senha_e_mail              /*145*/
      ,o.natoper_est_oper               /*146*/
      ,o.nr_recibo
      ,f_505.tipo_titulo
      ,o.natoper_nat_oper NATUREZA_INTERNA
      ,o.tipo_nf_referenciada
      ,pais_nfe.codigo_pais
      ,pais_dest.codigo_pais codPaisDest
      ,o.versao_systextilnfe
      ,e005.tipo_transacao
      ,o.nota_estorno
      ,o.justificativa_cont
      ,o.data_hora_contigencia
      ,s010.codigo_fornecedor
      ,decode(s010.inscr_est_forne, 'ISENTO', null, s010.inscr_est_forne) as inscr_est_forne
      ,o.codigo_transacao
      ,o.val_fcp_uf_dest_nf
      ,o.val_icms_uf_dest_nf
      ,o.val_icms_uf_remet_nf
      ,p_080.consumidor_final
    ,p.resp_tec_cgc9
    ,p.resp_tec_cgc4
    ,p.resp_tec_cgc2
    ,p.resp_tec_nome
    ,p.resp_tec_email
    ,decode(replace(substr(to_char(resp_tec_telefone),1,9), ' ', ''),'','0000000000',replace(substr(to_char(resp_tec_telefone),1,9), ' ', '')) as resp_tec_telefone
    ,p.resp_tec_idCSRT
    ,p.resp_tec_hashCSRT
    ,s010.cod_cidade as codigo_cidade
    from obrf_010 o,
         fatu_500 f500,
         basi_160 cidade_nfe,
         basi_167 estado_nfe,
         basi_165 pais_nfe,
         basi_160 cidade_dest,
         basi_167 estado_dest,
         basi_165 pais_dest,
         obrf_158 p,
         supr_010 s010,
         basi_160 cidade_entr,
         basi_167 estado_entr,
         supr_010 transp,
         basi_160 cidade_trans,
         fatu_505 f_505,
         pedi_070 p070,
         pedi_080 p_080,
         hdoc_030 h_030,
         estq_005 e005,
    fatu_012 f012
    where f500.codigo_empresa      = o.local_entrega
      and o.local_entrega          = p_cod_empresa
      and p.cod_empresa            = f500.codigo_empresa
      and s010.fornecedor9         = o.cgc_cli_for_9
      and s010.fornecedor4         = o.cgc_cli_for_4
      and s010.fornecedor2         = o.cgc_cli_for_2
      and o.transpa_forne9         = transp.fornecedor9 (+)
      and o.transpa_forne4         = transp.fornecedor4 (+)
      and o.transpa_forne2         = transp.fornecedor2 (+)
      and cidade_dest.cod_cidade   = s010.cod_cidade
      and estado_dest.estado       = cidade_dest.estado
      and estado_dest.codigo_pais  = cidade_dest.codigo_pais
      and pais_dest.codigo_pais    = cidade_dest.codigo_pais
      and pais_nfe.codigo_pais     = cidade_nfe.codigo_pais
      and estado_nfe.estado        = cidade_nfe.estado
      and estado_nfe.codigo_pais   = cidade_nfe.codigo_pais
      and cidade_nfe.cod_cidade    = f500.codigo_cidade
      and cidade_trans.cod_cidade  = transp.cod_cidade
      and f_505.codigo_empresa     = o.local_entrega
      and f_505.serie_nota_fisc    = o.serie
      and f_505.serie_nfe          = 'S'
      and o.cod_status             in ('999')
      and cidade_entr.cod_cidade   = f500.codigo_cidade
      and estado_entr.estado       = cidade_entr.estado
      and estado_entr.codigo_pais  = cidade_entr.codigo_pais
      and o.cod_solicitacao_nfe    = p_cod_id
      and o.condicao_pagto         = p070.cond_pgt_cliente (+)
      and p_080.natur_operacao     = o.natoper_nat_oper
      and p_080.estado_natoper     = o.natoper_est_oper
      and h_030.empresa            = p_cod_empresa
      and h_030.usuario            = p_str_usuario
      and e005.codigo_transacao    = o.codigo_transacao
      and f012.codigo_empresa (+)  = o.local_entrega
      and f012.uf             (+)  = o.natoper_est_oper;

-- ITENS DE NF ENTRADA
cursor c_nfe_item (c_cod_empresa     number,
                   c_num_nota        number,
                   c_cod_serie_nota  varchar2,
                   c_cnj_fornecedor9 number,
                   c_cnj_fornecedor4 number,
                   c_cnj_fornecedor2 number,
                   v_controleNarrativaBlocoK number , 
                   v_controleDescSimulBlocoK varchar2) IS

      select decode(c_015.coditem_nivel99,'0',trim(to_char(c_015.sequencia,'00000'))
                                             ,decode(inter_fn_dados_produto_nf(0,5,c_cod_empresa,c_015.capa_ent_nrdoc,c_015.capa_ent_serie,c_015.coditem_nivel99,c_015.coditem_grupo,c_015.coditem_subgrupo,c_015.coditem_item,c_015.descricao_item),
                                                    '1',c_015.coditem_nivel99 ||c_015.coditem_grupo,
                                                    '2',c_015.coditem_nivel99 ||c_015.coditem_grupo || c_015.coditem_subgrupo,
                                                    inter_fn_cod_produto_sintegra(c_015.coditem_nivel99,c_015.coditem_grupo,c_015.coditem_subgrupo,c_015.coditem_item,c_cod_empresa,c_cod_serie_nota,c_010.data_emissao) || inter_fn_codigo_blk(c_015.cod_estagio_agrupador_insu,c_015.cod_estagio_simultaneo_insu,c_cod_empresa,c_010.data_emissao,c_015.seq_operacao_agrupador_insu))) as produto
             , case
        when INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'faturamento.codEanXml') = 'S' then
          max(substr(b_010.codigo_barras,1,15))
        else
          max(substr(b_010.codigo_barras,1,14))
        end as codigo_barras
           ,max(inter_fn_dados_produto_nf(0,5,c_cod_empresa,c_015.capa_ent_nrdoc,c_015.capa_ent_serie,c_015.coditem_nivel99,c_015.coditem_grupo,c_015.coditem_subgrupo,c_015.coditem_item,c_015.descricao_item)) as tipo_impressao
             ,decode(v_controleNarrativaBlocoK,2,--Valida se decri??o do est?gio vai antes ou depois da descri??o do produto.
                      max(inter_fn_descr_blk(c_015.cod_estagio_agrupador_insu, decode(v_controleDescSimulBlocoK,'N', 0, c_015.cod_estagio_simultaneo_insu))   || '  ' ||  inter_fn_dados_produto_nf(0,6,c_cod_empresa,c_015.capa_ent_nrdoc,c_015.capa_ent_serie,c_015.coditem_nivel99,c_015.coditem_grupo,c_015.coditem_subgrupo,c_015.coditem_item,c_015.descricao_item))
                      ,max(inter_fn_dados_produto_nf(0,6,c_cod_empresa,c_015.capa_ent_nrdoc,c_015.capa_ent_serie,c_015.coditem_nivel99,c_015.coditem_grupo,c_015.coditem_subgrupo,c_015.coditem_item,c_015.descricao_item)  || '!!' || inter_fn_descr_blk(c_015.cod_estagio_agrupador_insu, decode(v_controleDescSimulBlocoK,'N', 0, c_015.cod_estagio_simultaneo_insu)))
             )as descricao_item
             ,replace(p_080.cod_natureza || substr(to_char(p_080.divisao_natur,'00'),3,1),'.','') as cod_natureza
             ,c_015.unidade_medida as unidade_medida_aux -- unidade de medida original
             ,c_015.unidade_conv   as unidade_de_medida_conv -- unidade de medida convertida
             ,sum(c_015.quantidade) as quantidade_aux --quantidade original
             ,sum(c_015.quantidade_conv) as quantidade_conv --quantidade convertido
             ,c_015.valor_unitario as valor_unitario_aux --valor original
             ,c_015.valor_conv as valor_conv -- valor convertido
             ,sum(round(c_015.valor_total,2)) as valor_total
             ,case
        when INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'faturamento.codEanXml') = 'S' then
          max(substr(decode(inter_fn_dados_produto_nf(0,5,c_cod_empresa,c_015.capa_ent_nrdoc,c_015.capa_ent_serie,c_015.coditem_nivel99,c_015.coditem_grupo,c_015.coditem_subgrupo,c_015.coditem_item,c_015.descricao_item),
                                '3',b_010.codigo_barras
                                   ,'000000000000000'),1,15))
        else
          max(substr(decode(inter_fn_dados_produto_nf(0,5,c_cod_empresa,c_015.capa_ent_nrdoc,c_015.capa_ent_serie,c_015.coditem_nivel99,c_015.coditem_grupo,c_015.coditem_subgrupo,c_015.coditem_item,c_015.descricao_item),
                                '3',b_010.codigo_barras
                                   ,'000000000000000'),1,14))
        end as b_010_codigo_barras_trib
             ,c_015.unidade_medida as c_015_unidade_medida_trib_aux -- unidade original
             ,c_015.unidade_conv as c_015_unidade_conv -- unidade convertida
             ,sum(c_015.quantidade) as c_015_quantidade_trib_aux -- quantidade trib original
             ,sum(c_015.quantidade_conv) as c_015_quantidade_conv_trib -- quantidade trib convertida
             ,c_015.valor_unitario as c_015_valor_unitario_trib_aux -- valor unitario trib original
             ,c_015.valor_conv as c_015_valor_trib_conv -- valor trib convertido
             ,null as id
             ,max(c_015.natitem_est_oper) as natitem_est_oper
             ,max(c_015.cod_vlfiscal_icm) as cod_vlfiscal_icm
             ,max(c_015.cvf_ipi_entrada) as cvf_ipi_entrada
             ,max(c_015.cvf_pis) as cvf_pis
             ,max(c_015.cvf_cofins) as cvf_cofins
             ,sum(decode(c_015.valor_icms, 0.00, 0.00, c_015.base_calc_icm)) as base_calc_icm
             ,sum(c_015.base_ipi) as base_ipi
             ,sum(c_015.base_pis_cofins) as base_pis_cofins
             ,sum(c_015.valor_icms) as valor_icms
             ,sum(c_015.valor_ipi) as valor_ipi
             ,sum(decode(c_015.natitem_est_oper, 'EX', nvl(c_056.valor_pis_imp,0), c_015.valor_pis)) as valor_pis
             ,sum(decode(c_015.natitem_est_oper, 'EX', nvl(c_056.valor_cofins_imp,0), c_015.valor_cofins)) as valor_cofins
             ,avg(c_015.percentual_icm) as percentual_icm
             ,avg(c_015.percentual_ipi) as percentual_ipi
             ,avg(decode(c_015.natitem_est_oper, 'EX', nvl(c_056.perc_pis,0), c_015.perc_pis)) as perc_pis
             ,avg(decode(c_015.natitem_est_oper, 'EX', nvl(c_056.perc_cofins,0), c_015.perc_cofins)) as perc_cofins
             ,avg(c_015.perc_icms_subs) as perc_icms_subs
             ,max(decode(c_015.perc_redu_icm,null,decode(p_080.perc_reducao_icm, 0, null,p_080.perc_reducao_icm),c_015.perc_redu_icm)) perc_reducao_icm
             ,avg(decode(c_015.perc_redu_sub,null,p_080.perc_redu_sub,c_015.perc_redu_sub)) as perc_redu_sub
             ,sum(c_015.valor_subtituicao) as valor_icms_subs
             ,avg(decode(c_015.perc_substituicao,null,p_080.perc_substituica,c_015.perc_substituicao)) as perc_substituica
             ,sum(c_015.base_subtituicao) as base_icms_subs
             ,c_015.classific_fiscal
             ,c_015.procedencia
             ,sum(c_056.base_calculo_ii) as base_calculo_ii
             ,sum(c_056.valor_desp_adu) as valor_desp_adu
             ,sum(c_056.valor_ii) as valor_ii
             ,sum(c_056.valor_iof) as valor_iof
             ,c_056.numero_di
             ,c_056.data_declaracao
             ,c_056.local_desembaraco
             ,c_056.uf_desembaraco
             ,c_056.data_desembaraco
             ,c_056.codigo_exportador
             ,c_056.numero_adicao
             ,c_056.sequencia_adicao
             ,c_056.codigo_fabricante
             ,c_056.v_afrmm
             ,c_056.n_draw
             ,c_056.tp_via_transp
             ,sum(c_056.valor_desconto_item) as valor_desconto_item
             ,c_015.cod_csosn as cod_csosn
             ,sum(c_015.valor_frete) as valor_frete
             ,sum(c_015.valor_seguro) as valor_seguro
             ,sum(c_015.valor_outros) as valor_outros
             ,sum(c_015.valor_desc) as valor_desc
             ,(to_number(trim(to_char(c_015.procedencia, '0')) || trim(to_char(c_015.cod_vlfiscal_icm, '00')))) as cst
             ,(Replace(p_080.cod_natureza || substr(to_char(p_080.divisao_natur,'00'),3,1),'.','')) as cfop
              /*INF AD PROD*/
             ,max(substr(decode(trim(c_015.observacao),null,null,c_015.observacao),1,450))  as inf_ad_prod
              /*INF AD PROD*/
             ,sum(c_015.rateio_descontos_ipi) as rateio_descontos_ipi
                ,sum(c_015.rateio_despesas) as rateio_despesas
             ,c_015.num_nota_orig
             ,c_015.serie_nota_orig
             ,p_080.respeita_ipi_class_fiscal as p_080_respeita_ipi
             ,avg(c_015.perc_fcp_uf_dest) perc_fcp_uf_dest
             ,avg(c_015.perc_icms_uf_dest) perc_icms_uf_dest
             ,avg(c_015.perc_icms_partilha) perc_icms_partilha
             ,round(sum(c_015.val_fcp_uf_dest),2) val_fcp_uf_dest
             ,round(sum(c_015.val_icms_uf_dest),2) val_icms_uf_dest
             ,round(sum(c_015.val_icms_uf_remet),2) val_icms_uf_remet
             ,c_015.nr_cupom
             ,c_015.nr_caixa
             ,trim(c_015.cest) as cest
             ,c_015.pdif51          as pDif
             ,sum(c_015.vicmsop51 ) as vICMSOp
             ,sum(c_015.vicmsdif51) as vICMSDif
             ,max(p_080.ind_cfop_exportacao) ind_cfop_exportacao
             ,max(nvl(b_010.fator_umed_trib,0)) fator_umed_trib
             ,c_015.natitem_nat_oper
             ,p_080.ind_beneficio_icms
      from obrf_015 c_015, basi_010 b_010, pedi_080 p_080, obrf_056 c_056, obrf_010 c_010
      where b_010.nivel_estrutura      = c_015.coditem_nivel99
        and b_010.grupo_estrutura      = c_015.coditem_grupo
        and b_010.subgru_estrutura     = c_015.coditem_subgrupo
        and b_010.item_estrutura       = c_015.coditem_item
        and p_080.natur_operacao       = c_015.natitem_nat_oper
        and p_080.estado_natoper       = c_015.natitem_est_oper
        and c_015.capa_ent_nrdoc       = c_num_nota
        and c_015.capa_ent_serie       = c_cod_serie_nota
        and c_015.capa_ent_forcli9     = c_cnj_fornecedor9
        and c_015.capa_ent_forcli4     = c_cnj_fornecedor4
        and c_015.capa_ent_forcli2     = c_cnj_fornecedor2
        and c_015.capa_ent_nrdoc       = c_010.documento
        and c_015.capa_ent_serie       = c_010.serie
        and c_015.capa_ent_forcli9     = c_010.cgc_cli_for_9
        and c_015.capa_ent_forcli4     = c_010.cgc_cli_for_4
        and c_015.capa_ent_forcli2     = c_010.cgc_cli_for_2
        and c_056.capa_ent_nrdoc   (+) = c_015.capa_ent_nrdoc
        and c_056.capa_ent_serie   (+) = c_015.capa_ent_serie
        and c_056.capa_ent_forcli9 (+) = c_015.capa_ent_forcli9
        and c_056.capa_ent_forcli4 (+) = c_015.capa_ent_forcli4
        and c_056.capa_ent_forcli2 (+) = c_015.capa_ent_forcli2
        and c_056.sequencia        (+) = c_015.sequencia

      group by decode(c_015.coditem_nivel99,'0',trim(to_char(c_015.sequencia,'00000'))
                                               ,decode(inter_fn_dados_produto_nf(0,5,c_cod_empresa,c_015.capa_ent_nrdoc,c_015.capa_ent_serie,c_015.coditem_nivel99,c_015.coditem_grupo,c_015.coditem_subgrupo,c_015.coditem_item,c_015.descricao_item),
                                                       '1',c_015.coditem_nivel99 ||c_015.coditem_grupo,
                                                       '2',c_015.coditem_nivel99 ||c_015.coditem_grupo || c_015.coditem_subgrupo,
                                                       inter_fn_cod_produto_sintegra(c_015.coditem_nivel99,c_015.coditem_grupo,c_015.coditem_subgrupo,c_015.coditem_item,c_cod_empresa,c_cod_serie_nota,c_010.data_emissao) || inter_fn_codigo_blk(c_015.cod_estagio_agrupador_insu,c_015.cod_estagio_simultaneo_insu,c_cod_empresa,c_010.data_emissao,c_015.seq_operacao_agrupador_insu)))
               ,Replace(p_080.cod_natureza || substr(to_char(p_080.divisao_natur,'00'),3,1),'.','')
               ,to_number(trim(to_char(c_015.procedencia, '0')) || trim(to_char(c_015.cod_vlfiscal_icm, '00')))
               ,c_015.unidade_medida
               ,c_015.unidade_conv
               ,c_015.valor_unitario
               ,c_015.valor_conv
               ,c_015.classific_fiscal
               ,c_015.procedencia
               ,c_056.capa_ent_nrdoc
               ,c_056.capa_ent_serie
               ,c_056.capa_ent_forcli9
               ,c_056.capa_ent_forcli4
               ,c_056.capa_ent_forcli2
               ,c_056.tipo_declaracao
               ,c_056.numero_di
               ,c_056.data_declaracao
               ,c_056.local_desembaraco
               ,c_056.uf_desembaraco
               ,c_056.data_desembaraco
               ,c_056.codigo_exportador
               ,c_056.numero_adicao
               ,c_056.sequencia_adicao
               ,c_056.codigo_fabricante
               ,c_056.v_afrmm
               ,c_056.n_draw
               ,c_056.tp_via_transp
               ,c_015.cod_csosn
               ,c_015.num_nota_orig
               ,c_015.serie_nota_orig
               ,c_015.nr_cupom
               ,c_015.nr_caixa
               ,trim(c_015.cest)
               ,c_015.pdif51 /*CST 51*/
               ,p_080.respeita_ipi_class_fiscal
               ,c_015.perc_fcp_uf_ncm
               ,c_015.natitem_nat_oper
               ,p_080.ind_beneficio_icms
       order by c_015.num_nota_orig
               ,c_015.serie_nota_orig;

  -- NOTAS DE SAIDAS
  cursor c_NFs is

      select f.numero_danf_nfe         /*1*/
      ,null                            /*2*/
      ,null                            /*3*/
      ,substr(Replace(p_080.cod_natureza || substr(to_char(p_080.divisao_natur,'00'),3,1),'.','') || ' ' ||p_080.descr_nat_oper ,1,60) as natop_nf_nat_oper              /*4*/
      ,decode(p070.avista,1,0,1) as avista                           /*5*/
      ,'55'                            /*6*/
      ,f.serie_nota_fisc                         /*7*/
      ,f.num_nota_fiscal                     /*8*/
      ,f.data_emissao as o_data_emissao                  /*9*/
      ,f.data_saida as data_saida            /*10*/     -- Tiago.r
      ,'1' as entrada_saida            /*11*/
      ,DECODE(inter_fn_is_end_entrega(f.codigo_empresa,'vendas.enderecoDanfe',f.cgc_4),0,to_number(trim(trim(to_char(estado_nfe.codigo_fiscal_uf,'00')) || trim(to_char(cidade_nfe.codigo_fiscal,'00000'))))  /*12*/
                                                                                        ,to_number(trim(trim(to_char(estado_entr.codigo_fiscal_uf,'00')) || trim(to_char(cidade_entr.codigo_fiscal,'00000'))))) as cod_cidade_ibge
      ,p.formato_danfe as formato_danfe                             /*13*/ -- rafaelsalvador saida
      ,1                               /*14*/ -- NORMAL
      ,p.nfe_ambiente                  /*15*/
      ,0                               /*16*/ -- EMITIDO PELO CONTRIBUINTE NO PROPRIO SISTEMA
      ,'5'                             /*17*/ -- Vers?o do Systextil
      ,1                               /*18*/ -- FINALIDADE
      ,f500.cgc_9 as f_cgc_9           /*19*/
      ,f500.cgc_4 as f_cgc_4           /*20*/
      ,f500.cgc_2 as f_cgc_2           /*21*/
      ,f500.nome_empresa               /*22*/
      ,f500.nome_fantasia as f500_nome_fantasia             /*23*/
      ,f500.endereco as f_endereco     /*24*/
      ,f500.numero as f_numero         /*25*/
      ,f500.complemento as complemento /*26*/
      ,f500.bairro as f_bairro           /*27*/
      ,to_number(trim(trim(to_char(estado_dest.codigo_fiscal_uf,'00')) || trim(to_char(cidade_dest.codigo_fiscal,'00000')))) as cidade_dest_cod_cidade_ibge     /*28*/
      ,inter_fn_retira_acentuacao(cidade_dest.cidade) as cidade_dest_cidade              /*29*/
      ,cidade_dest.estado as cidade_dest_estado              /*30*/
      ,trim(to_char(estado_dest.codigo_fiscal_uf,'00')) as sb_cod_cidade_ibge    /*31*/
      ,f500.cep_empresa                /*32*/
      ,to_number(substr(trim(to_char(cidade_dest.ddd,'0000')),3,2) || trim(to_char(f500.telefone,'00000000'))) as f500_telefone                  /*33*/
      ,pais_dest.codigo_fiscal as pais_dest_codigo_pais           /*34*/
      ,pais_dest.nome_pais  as pais_dest_nome_pais /*35*/
      ,replace(substr(replace(f500.inscr_estadual,'.',''),1,14),'-','') as f_inscr_estadual             /*36*/
      ,f012.ie_substituto                            /*37*/
      ,null             /*38*/
      ,null                            /*39*/
      ,p010.cgc_9                /*40*/
      ,p010.cgc_4                /*41*/
      ,p010.cgc_2                /*42*/
      ,decode(inter_fn_descricao_natureza(f.codigo_empresa,f.num_nota_fiscal,f.serie_nota_fisc),
                                                                 null,p010.nome_cliente
                                                                     ,inter_fn_descricao_natureza(f.codigo_empresa,f.num_nota_fiscal,f.serie_nota_fisc)) as nome_cliente            /*43*/
      ,DECODE(inter_fn_is_end_entrega(f.codigo_empresa,'vendas.enderecoDanfe',f.cgc_4),0,p010.endereco_cliente, p150.end_entr_cobr) AS endereco_cliente        /*44*/
      ,DECODE(inter_fn_is_end_entrega(f.codigo_empresa,'vendas.enderecoDanfe',f.cgc_4),0,p010.numero_imovel, p150.numero_imovel) as s010_numero_imovel              /*45*/
      ,DECODE(inter_fn_is_end_entrega(f.codigo_empresa,'vendas.enderecoDanfe',f.cgc_4),0,p010.complemento, p150.complemento_endereco)  as s010_complemento               /*46*/
      ,DECODE(inter_fn_is_end_entrega(f.codigo_empresa,'vendas.enderecoDanfe',f.cgc_4),0,p010.bairro, p150.bairro_entr_cobr)  as s010_bairro                     /*47*/

      ,DECODE(inter_fn_is_end_entrega(f.codigo_empresa,'vendas.enderecoDanfe',f.cgc_4),0,to_number(trim(trim(to_char(estado_nfe.codigo_fiscal_uf,'00')) || trim(to_char(cidade_nfe.codigo_fiscal,'00000')))),   /*48*/
                                                                 to_number(trim(trim(to_char(estado_entr.codigo_fiscal_uf,'00')) || trim(to_char(cidade_entr.codigo_fiscal,'00000'))))) as cidade_nfe_cod_cidade_ibge

      ,DECODE(inter_fn_is_end_entrega(f.codigo_empresa,'vendas.enderecoDanfe',f.cgc_4),0,inter_fn_retira_acentuacao(decode(cidade_nfe.estado,'EX',cidade_nfe.cidade || ' - ' || pais_nfe.nome_pais,cidade_nfe.cidade))  /*49*/
                                                                                        ,inter_fn_retira_acentuacao(cidade_entr.cidade)) as cidade_nfe_cidade
      ,DECODE(inter_fn_is_end_entrega(f.codigo_empresa,'vendas.enderecoDanfe',f.cgc_4),0,cidade_nfe.estado, cidade_entr.estado) as cidade_nfe_estado               /*50*/
      ,pais_nfe.nome_pais as pais_nfe_nome_pais             /*51*/
      ,to_number(substr(decode(substr(trim(to_char(p010.telefone_cliente,'00000000')),0,10), 0, substr(trim(to_char(null,'0000')),3,2), null, substr(trim(to_char(null,'0000')),3,2), substr(trim(to_char(cidade_nfe.ddd,'0000')),3,2)) ||  trim(to_char(decode(p010.telefone_cliente, 0,trim(to_char(p010.telefone_cliente,'000000000')), null,trim(to_char(p010.celular_cliente,'000000000')),trim(to_char(p010.telefone_cliente,'00000000'))))), 0, 10)) as telefone_cliente /*52*/
      ,DECODE(inter_fn_is_end_entrega(f.codigo_empresa,'vendas.enderecoDanfe',f.cgc_4),0,p010.cep_cliente, p150.cep_entr_cobr) AS cep_cliente             /*53*/
      ,pais_nfe.codigo_fiscal   /*54*/
      /*leitura saidas*/
      ,trim(replace(replace(p010.insc_est_cliente,'.',''),'-','')) as inscr_est_forne          /*55*/
      ,null                            /*56*/
      ,null                            /*57*/
      ,null                            /*58*/
      ,null                            /*59*/
      ,null                            /*60*/
      ,null                            /*61*/
      ,null                            /*62*/
      ,null                            /*63*/
      ,null                            /*64*/
      ,null                            /*65*/
      ,null                            /*66*/
      ,p150.cgc_entr_cobr9            /*67*/
      ,p150.cgc_entr_cobr4            /*68*/
      ,p150.cgc_entr_cobr2            /*69*/
      ,p150.end_entr_cobr as p150_end_entr_cobr             /*70*/
      ,p150.numero_imovel as p150_numero_imovel              /*71*/
      ,p150.complemento_endereco       /*72*/
      ,p150.bairro_entr_cobr           /*73*/
      ,p150.cep_entr_cobr
      ,to_number(trim(trim(to_char(estado_entr.codigo_fiscal_uf,'00')) || trim(to_char(cidade_entr.codigo_fiscal,'00000')))) as cidade_entr_cod_cidade     /*74*/
      ,inter_fn_retira_acentuacao(cidade_entr.cidade) as cidade_entr_cidade              /*75*/
      ,cidade_entr.estado as cidade_entr_estado             /*76*/
      ,decode(f.valor_icms, 0.00, 0.00, f.base_icms) as base_icms   /*77*/    -- claudio
      ,f.valor_icms                    /*78*/
      ,f.base_icms_sub                 /*79*/
      ,f.valor_icms_sub                /*80*/
      ,f.valor_itens_nfis              /*81*/
      ,f.valor_frete_nfis              /*82*/
      ,f.valor_seguro                  /*83*/
      ,(f.vlr_desc_especial + f.valor_desc_nota + f.valor_suframa) as valor_desconto /*84*/
      ,0.00                            /*85*/
      ,f.valor_ipi                     /*86*/
      ,0.00                            /*87*/
      ,0.00                            /*88*/
      ,(f.valor_despesas +  f.valor_encar_nota) as valor_despesas               /*89*/
      ,(f.valor_itens_nfis + f.valor_ipi      + f.valor_encar_nota +
        f.valor_frete_nfis + f.valor_seguro   - f.valor_desc_nota +
        f.valor_despesas - f.vlr_desc_especial +
        f.valor_icms_sub - f.valor_suframa)
       as total_docto                   /*90*/
      ,null                            /*91*/
      ,null                            /*92*/
      ,null                            /*93*/
      ,null                            /*94*/
      ,null                            /*95*/
      ,null                            /*96*/
      ,null                            /*97*/
      ,null                            /*98*/
      ,null                            /*99*/
      ,null                            /*100*/
      ,null                            /*101*/
      ,null                            /*102*/
      ,decode(p.origem_tipofrete,0,decode(f.tipo_frete,1,0,2,1,3,2,9,9,6,3,7,4,0),1,decode(f.tipo_frete_redes,1,0,2,1,3,2,9,9,6,3,7,4,0)) as tipo_frete /*103*/
      ,f.transpor_forne9 as trans_cgc9                 /*104*/
      ,f.transpor_forne4 as trans_cgc4                 /*105*/
      ,f.transpor_forne2 as trans_cgc2                 /*106*/
      ,transp.nome_fornecedor as trans_nome            /*107*/
      ,substr(replace(replace(transp.inscr_est_forne,'.',''),'-',''),1,14) as trans_insest          /*108*/
      ,transp.endereco_forne as trans_end              /*109*/
      ,inter_fn_retira_acentuacao(cidade_trans.cidade) as trans_cidade             /*110*/
      ,cidade_trans.estado as trans_estado             /*111*/
      ,null                            /*112*/
      ,null                            /*113*/
      ,null                            /*114*/
      ,null                            /*115*/
      ,null                            /*116*/
      ,null                            /*117*/
      ,trim(f.placa_veiculo) as placa_veiculo /*118*/
      ,decode(trim(f.placa_veiculo),null,null,cidade_trans.estado) as uf_placa /*119*/
      ,null                            /*120*/
      ,null                            /*121*/
      ,null                            /*122*/
      ,null                            /*123*/
      ,f.qtde_embalagens               /*124*/
      ,f.especie_volume                /*125*/
      ,f.marca_volumes                 /*126*/
      ,f.nr_volume                     /*127*/
      ,f.peso_liquido                  /*128*/
      ,f.peso_bruto                    /*129*/
      ,null                            /*130*/
      ,null                            /*131*/
      ,null                            /*132*/
      ,null                            /*133*/
      ,null                            /*134*/
      ,null                            /*135*/
      ,null                            /*136*/
      ,f.codigo_empresa as cod_empresa  /*137*/
      ,p_cod_id                        /*138*/
      ,p_str_usuario                   /*139*/
      ,f.nfe_contigencia               /*140*/
      ,p010.e_mail                     /*141*/
      ,p010.nfe_e_mail                 /*142*/
      ,h_030.user_id_e_mail            /*143*/
      ,h_030.e_mail as f_user_e_mail   /*144*/
      ,h_030.senha_e_mail              /*145*/
      ,f.natop_nf_est_oper
      ,f_505.tipo_titulo
      ,f.natop_nf_nat_oper NATUREZA_INTERNA
      ,f.tipo_nf_referenciada
      ,pais_nfe.codigo_pais
      ,f.versao_systextilnfe
      ,to_char(trunc(sysdate) + (f.hora_saida - trunc(f.hora_saida)),'hh24:mi:ss') hora_saida
      ,decode(f.valor_suframa,0.00,'',p010.nr_suframa_cli) INSCR_SUFRAMA
      ,e005.tipo_transacao as tipo_transacao
      ,f.nota_fatura as nota_fatura
      ,f.nota_estorno as nota_estorno
      ,p010.tipo_cliente as tipo_cliente
      ,f.valor_suframa
      ,f.justificativa_cont
      ,f.data_hora_contigencia
      ,f.nr_recibo
      ,f.seq_end_entr
      ,f.seq_end_cobr
      ,p010.codigo_cliente
      ,decode(trim(p010.insc_est_cliente), 'ISENTO', null, p010.insc_est_cliente) as insc_est_cliente
      ,p_080.codigo_transacao
      ,f.val_fcp_uf_dest_nf
      ,f.val_icms_uf_dest_nf
      ,f.val_icms_uf_remet_nf
      ,p_080.consumidor_final
      ,p_080.ind_natur_cupom_ref
      ,decode(trim(f.nr_autorizacao_opera),null,' ',f.nr_autorizacao_opera) nr_autorizacao_opera
      ,f.origem_nota
      ,f.nr_solicitacao
      ,decode(f.cod_forma_pagto,null,0,f.cod_forma_pagto) cod_forma_pagto
    ,p.resp_tec_cgc9
    ,p.resp_tec_cgc4
    ,p.resp_tec_cgc2
    ,p.resp_tec_nome
    ,p.resp_tec_email
    ,decode(replace(substr(to_char(resp_tec_telefone),1,9), ' ', ''),'','0000000000',replace(substr(to_char(resp_tec_telefone),1,9), ' ', '')) as resp_tec_telefone
    ,p.resp_tec_idCSRT
    ,p.resp_tec_hashCSRT
    ,f.percentual_negociado
    ,p010.telefone_cliente as numero_fixo
    ,cidade_nfe.ddd as ddd_cidade
    ,p010.celular_cliente as numero_celular
    ,p010.cod_cidade as cidade_destino
    ,estado_dest.estado estado_empresa_deson
    from fatu_050 f,
         fatu_500 f500,
         basi_160 cidade_nfe,
         basi_167 estado_nfe,
         basi_165 pais_nfe,
         basi_160 cidade_dest,
         basi_167 estado_dest,
         basi_165 pais_dest,
         obrf_158 p,
         pedi_010 p010,
         pedi_150 p150,
         basi_160 cidade_entr,
         basi_167 estado_entr,
         supr_010 transp,
         basi_160 cidade_trans,
         fatu_505 f_505,
         pedi_070 p070,
         pedi_080 p_080,
         hdoc_030 h_030,
         estq_005 e005,
         fatu_012 f012
    where f500.codigo_empresa    = f.codigo_empresa
      and f.codigo_empresa       = p_cod_empresa
      and p.cod_empresa          = f500.codigo_empresa
      and p010.cgc_9             = f.cgc_9
      and p010.cgc_4             = f.cgc_4
      and p010.cgc_2             = f.cgc_2
      and cidade_nfe.cod_cidade  = p010.cod_cidade
      and estado_nfe.estado      = cidade_nfe.estado
      and estado_nfe.codigo_pais = cidade_nfe.codigo_pais
      and pais_nfe.codigo_pais   = cidade_nfe.codigo_pais
      and cidade_dest.cod_cidade = f500.codigo_cidade
      and estado_dest.estado     = cidade_dest.estado
      and estado_dest.codigo_pais= cidade_dest.codigo_pais
      and pais_dest.codigo_pais  = cidade_dest.codigo_pais
      and p150.cd_cli_cgc_cli9   = p010.cgc_9 (+)
      and p150.cd_cli_cgc_cli4   = p010.cgc_4 (+)
      and p150.cd_cli_cgc_cli2   = p010.cgc_2 (+)
      and p150.tipo_endereco     = 1
      and p150.seq_endereco      = f.seq_end_entr
      and cidade_entr.cod_cidade = p150.cid_entr_cobr (+)
      and estado_entr.estado     = cidade_entr.estado
      and estado_entr.codigo_pais= cidade_entr.codigo_pais
      and f_505.codigo_empresa   = f.codigo_empresa
      and f_505.serie_nota_fisc  = f.serie_nota_fisc
      and f.transpor_forne9      = transp.fornecedor9 (+)
      and f.transpor_forne4      = transp.fornecedor4 (+)
      and f.transpor_forne2      = transp.fornecedor2 (+)
      and cidade_trans.cod_cidade = transp.cod_cidade
      and f_505.serie_nfe        = 'S'
      and f.cod_status           in ('999')
      and f.cod_solicitacao_nfe  = p_cod_id
      and f.cond_pgto_venda      = p070.cond_pgt_cliente
      and p_080.natur_operacao   = f.natop_nf_nat_oper
      and p_080.estado_natoper   = f.natop_nf_est_oper
      and h_030.empresa          = p_cod_empresa
      and h_030.usuario          = p_str_usuario
      and e005.codigo_transacao  = p_080.codigo_transacao
      and f012.codigo_empresa (+) = f.codigo_empresa
      and f012.uf (+)             = f.natop_nf_est_oper;

-- ITENS DE NF SAIDA
    cursor c_nfs_item (c_cod_empresa    number,
                       c_num_nota       number,
                       c_cod_serie_nota varchar2,
                       v_controleNarrativaBlocoK  number,
                       v_controleDescSimulBlocoK varchar2
                       ) IS
      select decode(f_060.nivel_estrutura,'0',trim(to_char(f_060.seq_item_nfisc,'00000'))
                                             ,decode(inter_fn_dados_produto_nf(0,5,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '#$' || nvl(f_060.observacao,' ')),
                                                     '1',f_060.nivel_estrutura ||f_060.grupo_estrutura,
                                                     '2',f_060.nivel_estrutura ||f_060.grupo_estrutura || f_060.subgru_estrutura,
                                                     inter_fn_cod_produto_sintegra(f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,c_cod_empresa,c_cod_serie_nota,f_050.data_emissao)|| inter_fn_codigo_blk(f_060.cod_estagio_agrupador_insu,f_060.cod_estagio_simultaneo_insu,c_cod_empresa,f_050.data_emissao,f_060.seq_operacao_agrupador_insu))) as produto
             ,case
        when INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'faturamento.codEanXml') = 'S' then
          max(substr(b_010.codigo_barras,1,15))
        else
          max(substr(b_010.codigo_barras,1,14))
        end as codigo_barras
             ,max(inter_fn_dados_produto_nf(0,5,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item  || '#$' || nvl(f_060.observacao,' '))) as tipo_impressao

             ,decode((v_controleNarrativaBlocoK),2,--Valida se decri??o do est?gio vai antes ou depois da descri??o do produto.
                 trim(max(inter_fn_descr_blk(f_060.cod_estagio_agrupador_insu, decode(v_controleDescSimulBlocoK,'N', 0, f_060.cod_estagio_simultaneo_insu)) || ' ' || inter_fn_dados_produto_nf(0,6,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item  || '#$' || nvl(f_060.observacao,' ')))),
                 trim(max(inter_fn_dados_produto_nf(0,6,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '!!' || inter_fn_descr_blk(f_060.cod_estagio_agrupador_insu, decode(v_controleDescSimulBlocoK,'N', 0, f_060.cod_estagio_simultaneo_insu)) || '#$' || nvl(f_060.observacao,' '))))
             ) || ' ' ||

             max(/*GRAMATURA*/
                 substr(decode(o_180.cod_mensagem_gm2,null,null,0,null,inter_fn_dados_produto_nf(0,1,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '!!' || inter_fn_descr_blk(f_060.cod_estagio_agrupador_insu, f_060.cod_estagio_simultaneo_insu) || '#$' || nvl(f_060.observacao,' '))),1,120)  || ' ' ||
                 /*LARGURA*/
                 replace(substr(decode(o_180.cod_mensagem_largura,null,null,0,null,inter_fn_dados_produto_nf(0,2,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '!!' || inter_fn_descr_blk(f_060.cod_estagio_agrupador_insu, f_060.cod_estagio_simultaneo_insu) || '#$' || nvl(f_060.observacao,' '))),1,120),'.',',')  || ' ' ||
                 /*SIMBOLO CONSERVACAO*/
                 decode(trim(substr(decode(o_180.ind_conservacao,     null,null,0,null,inter_fn_dados_produto_nf(o_180.ind_conservacao,3,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '!!' || inter_fn_descr_blk(f_060.cod_estagio_agrupador_insu, f_060.cod_estagio_simultaneo_insu) || '#$' || nvl(f_060.observacao,' '))),1,120))
                        ,null,'',
                        'CONS. ' || substr(decode(o_180.ind_conservacao,     null,null,0,null,inter_fn_dados_produto_nf(o_180.ind_conservacao,3,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '!!' || inter_fn_descr_blk(f_060.cod_estagio_agrupador_insu, f_060.cod_estagio_simultaneo_insu) || '#$' || nvl(f_060.observacao,' '))),1,120)
                       )
             )as descricao_item


             /*INF AD PROD*/
             ,max(substr(inter_fn_dados_produto_nf(0,7,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '#$' || nvl(f_060.observacao,' '))||' '||
                         inter_fn_dados_produto_nf(0,8,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '#$' || nvl(f_060.observacao,' '))  ||' '||
                         inter_fn_dados_produto_nf(0,9,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '#$' || nvl(f_060.observacao,' '))
                  ,1,450)) || ' ' || max(decode(p_110.observacao_nfe, null,'',p_110.observacao_nfe))  as inf_ad_prod

             ,Replace(p_080.cod_natureza || substr(to_char(p_080.divisao_natur,'00'),3,1),'.','') as cod_natureza
             ,f_060.unidade_medida
             ,sum(f_060.qtde_item_fatur) as qtde_item_fatur
             ,f_060.valor_unitario
             ,sum(round(f_060.valor_faturado,2)) as valor_total
             , case
        when INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'faturamento.codEanXml') = 'S' then
          max(substr(decode(inter_fn_dados_produto_nf(0,5,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '#$' || f_060.observacao),
                                '3',b_010.codigo_barras
                                   ,'000000000000000'),1,15))
        else
          max(substr(decode(inter_fn_dados_produto_nf(0,5,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '#$' || f_060.observacao),
                                '3',b_010.codigo_barras
                                   ,'000000000000000'),1,14))
        end as b_010_codigo_barras_trib
             ,max(f_060.unidade_medida) as c_015_unidade_medida_trib
             ,sum(f_060.qtde_item_fatur) as c_015_quantidade_trib
             ,f_060.valor_unitario as c_015_valor_unitario_trib
             ,null as id
             ,max(f_060.natopeno_est_oper) as natopeno_est_oper
             ,max(f_060.cvf_icms) as cvf_icms
             ,max(f_060.cvf_ipi_saida) as cvf_ipi_saida
             ,max(f_060.cvf_pis) as cvf_pis
             ,max(f_060.cvf_cofins) as cvf_cofins
             ,sum(decode( f_060.perc_icms, 0.00, 0.00, (f_060.base_icms + f_060.rateio_despesa + f_060.rateio_desc_propaganda))) as base_icms
             ,sum(f_060.base_icms + f_060.rateio_despesa + f_060.rateio_desc_propaganda) as base_icms_desoner
             --,sum(decode( f_060.valor_icms, 0.00, 0.00, (f_060.base_icms + f_060.rateio_despesa + f_060.rateio_desc_propaganda + decode(p_080.ipi_sobre_icms, 1, f_060.valor_ipi, 0.00)))) as base_icms
             ,sum(f_060.base_icms) as base_icms_cbenef
             ,sum(f_060.base_ipi) as base_ipi
             ,sum(f_060.base_icms_difer) as base_icms_difer
             ,sum(f_060.basi_pis_cofins) as basi_pis_cofins
             ,sum(f_060.valor_icms) as valor_icms
             ,sum(f_060.valor_ipi) as valor_ipi
             ,sum(f_060.valor_pis) as valor_pis
             ,sum(f_060.valor_cofins) as valor_cofins
             ,avg(f_060.perc_icms) as perc_icms
             ,avg(f_060.perc_ipi) as perc_ipi
             ,avg(f_060.perc_pis) as perc_pis
             ,avg(f_060.perc_cofins) as perc_cofins
             ,avg(decode(f_060.perc_redu_icm,null,p_080.perc_reducao_icm,f_060.perc_redu_icm)) as perc_reducao_icm
             ,avg(decode(f_060.perc_redu_sub,null,p_080.perc_redu_sub,f_060.perc_redu_sub)) as perc_redu_sub
             ,avg(decode(f_060.perc_substituicao,null,p_080.perc_substituica,f_060.perc_substituicao)) as perc_substituica
             ,f_060.classific_fiscal
             ,f_060.procedencia
--           unidades de faturamento
             ,max(f_060.um_faturamento_um) as c_015_unidade_medida_com
             ,sum(f_060.um_faturamento_qtde) as c_015_quantidade_com
             ,f_060.um_faturamento_valor_unit as c_015_valor_unitario_com
             ,max(f_060.nota_ajuste) as nota_origem
             ,max(f_060.serie_ajuste) as serie_origem
             ,sum(f_060.valor_icms_difer) as valor_icms_difer
             ,f_060.cod_csosn as cod_csosn
             ,sum(f_060.valor_outros) as valor_outros
             ,sum(f_060.valor_seguro) as valor_seguro
             ,sum(f_060.valor_desc) as valor_desc
             ,sum(f_060.valor_frete) as valor_frete
             ,substr(inter_fn_dados_produto_nf(0,4,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '#$' || nvl(f_060.observacao,' ')),1,400) as descricao_prod
             ,(Replace(p_080.cod_natureza || substr(to_char(p_080.divisao_natur,'00'),3,1),'.','')) as cfop
             ,(to_number(trim(to_char(f_060.procedencia, '0')) || trim(to_char(f_060.cvf_icms, '00')))) as cst
             ,DECODE(DECODE(f_060.natopeno_est_oper,'EX',INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'fiscal.enviaCodClientePedidoXML'),'S'), 'N', '',
                      decode (trim(substr(p_110.cod_ped_cliente, 1, 15)), null,
                             trim(substr(p_100.cod_ped_cliente, 1, 15)), trim(substr(p_110.cod_ped_cliente, 1, 15)) )) as pedido_cliente
             ,DECODE(DECODE(f_060.natopeno_est_oper,'EX',INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'fiscal.enviaCodClientePedidoXML'),'S'), 'N', 0, p_110.seq_ped_compra) as seq_ped_compra
             ,p_080.natur_operacao as natur_operacao
             ,sum(f_060.rateio_descontos_ipi) as rateio_descontos_ipi
             ,f_060.num_nota_orig nf_origem_devol
             ,f_060.serie_nota_orig serie_nf_devol
             ,f_060.cgc9_origem     cnpj_9_devol
             ,f_060.cgc4_origem     cnpj_4_devol
             ,f_060.cgc2_origem     cnpj_2_devol
             ,sum(f_060.valor_suframa_icms) valor_suframa_icms
             ,sum(f_060.base_calc_icms_suframa) base_calc_icms_suframa
             ,avg(f_060.perc_fcp_uf_dest) perc_fcp_uf_dest
             ,avg(f_060.perc_icms_uf_dest) perc_icms_uf_dest
             ,f_060.perc_icms_partilha perc_icms_partilha
             ,sum(round(f_060.val_fcp_uf_dest,2)) val_fcp_uf_dest
             ,sum(round(f_060.val_icms_uf_dest,2)) val_icms_uf_dest
             ,sum(round(f_060.val_icms_uf_remet,2)) val_icms_uf_remet
             ,trim(f_060.cest) as cest
            /*CST 51*/
             ,p_080.respeita_ipi_class_fiscal as p_080_respeita_ipi
             ,f_060.pdif51          as pDif
             ,sum(f_060.vicmsop51 )  as vICMSOp
             ,sum(f_060.vicmsdif51) as vICMSDif
             ,f_060.nr_cupom
             ,f_060.nr_caixa
             ,max(p_080.ind_cfop_exportacao) ind_cfop_exportacao
             ,max(nvl(b_010.fator_umed_trib,0)) fator_umed_trib
             ,p_080.ind_beneficio_icms
             ,nvl(sum((select nvl(sum(pcpt_020.peso_bruto - pcpt_020.tara),0) from pcpt_020
                       where pcpt_020.cod_empresa_nota  = f_060.ch_it_nf_cd_empr
                         and pcpt_020.nota_fiscal       = f_060.ch_it_nf_num_nfis
                         and pcpt_020.serie_fiscal_sai  = f_060.ch_it_nf_ser_nfis
                         and pcpt_020.seq_nota_fiscal   = f_060.seq_item_nfisc
                         and pcpt_020.rolo_estoque      = 2)),0)  pesoDeRolos
       ,sum(f_060.valor_icms_substituto) valor_icms_substituto
       ,max(f_060.perc_cred_presumido) as perc_cred_presumido
       ,max(f_060.valor_cred_presumido) as valor_cred_presumido
       ,max(f_060.cod_cbnef) as cod_cbnef
      from fatu_060 f_060, basi_010 b_010, pedi_080 p_080, obrf_180 o_180, pedi_100 p_100, pedi_110 p_110, fatu_050 f_050
      where b_010.nivel_estrutura   = f_060.nivel_estrutura
        and b_010.grupo_estrutura   = f_060.grupo_estrutura
        and b_010.subgru_estrutura  = f_060.subgru_estrutura
        and b_010.item_estrutura    = f_060.item_estrutura
        and p_080.natur_operacao    = f_060.natopeno_nat_oper
        and p_080.estado_natoper    = f_060.natopeno_est_oper
        and f_060.ch_it_nf_cd_empr  = f_050.codigo_empresa
        and f_060.ch_it_nf_num_nfis = f_050.num_nota_fiscal
        and f_060.ch_it_nf_ser_nfis = f_050.serie_nota_fisc
        and f_060.ch_it_nf_cd_empr  = c_cod_empresa
        and f_060.ch_it_nf_num_nfis = c_num_nota
        and f_060.ch_it_nf_ser_nfis = c_cod_serie_nota
        and f_060.ch_it_nf_cd_empr  = o_180.cod_empresa (+)
        and f_060.pedido_venda      = p_100.pedido_venda (+)
        and f_060.pedido_venda      = p_110.pedido_venda (+)
        and f_060.seq_item_pedido   = p_110.seq_item_pedido (+)
      group by  f_060.ch_it_nf_cd_empr
               ,f_060.ch_it_nf_num_nfis
               ,f_060.ch_it_nf_ser_nfis
         ,decode(inter_fn_dados_produto_nf(0,5,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '#$' || nvl(f_060.observacao,' ')),
               4,f_060.seq_item_nfisc,0)
               ,decode(f_060.nivel_estrutura,'0',trim(to_char(f_060.seq_item_nfisc,'00000'))
                                                ,decode(inter_fn_dados_produto_nf(0,5,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '#$' || nvl(f_060.observacao,' ')),
                                                        '1',f_060.nivel_estrutura ||f_060.grupo_estrutura,
                                                        '2',f_060.nivel_estrutura ||f_060.grupo_estrutura || f_060.subgru_estrutura,
                                                        inter_fn_cod_produto_sintegra(f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,c_cod_empresa,c_cod_serie_nota,f_050.data_emissao) || inter_fn_codigo_blk(f_060.cod_estagio_agrupador_insu,f_060.cod_estagio_simultaneo_insu,c_cod_empresa,f_050.data_emissao,f_060.seq_operacao_agrupador_insu)))
               ,substr(inter_fn_dados_produto_nf(0,4,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '#$' || nvl(f_060.observacao,' ')),1,400)
               ,Replace(p_080.cod_natureza || substr(to_char(p_080.divisao_natur,'00'),3,1),'.','')
               ,to_number(trim(to_char(f_060.procedencia, '0')) || trim(to_char(f_060.cvf_icms, '00')))
               ,f_060.unidade_medida
               ,f_060.valor_unitario
               ,f_060.classific_fiscal
               ,f_060.procedencia
               ,f_060.um_faturamento_valor_unit
               ,f_060.cod_csosn
               ,DECODE(DECODE(f_060.natopeno_est_oper,'EX',INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'fiscal.enviaCodClientePedidoXML'),'S'), 'N', '',
                      decode (trim(substr(p_110.cod_ped_cliente, 1, 15)), null,
                             trim(substr(p_100.cod_ped_cliente, 1, 15)), trim(substr(p_110.cod_ped_cliente, 1, 15)) ))
               ,DECODE(DECODE(f_060.natopeno_est_oper,'EX',INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'fiscal.enviaCodClientePedidoXML'),'S'), 'N', 0, p_110.seq_ped_compra)
               ,p_080.natur_operacao
               ,f_060.num_nota_orig
               ,f_060.serie_nota_orig
               ,f_060.cgc9_origem
               ,f_060.cgc4_origem
               ,f_060.cgc2_origem
               ,f_060.perc_icms_partilha
               ,trim(f_060.cest)
               ,f_060.pdif51 /*CST 51*/
               ,p_080.respeita_ipi_class_fiscal
               ,f_060.nr_cupom
               ,f_060.nr_caixa
               ,p_080.ind_beneficio_icms
               ,f_060.nr_patrimonio_str
         order by decode(inter_fn_dados_produto_nf(0,5,f_060.ch_it_nf_cd_empr,f_060.ch_it_nf_num_nfis,f_060.ch_it_nf_ser_nfis,f_060.nivel_estrutura,f_060.grupo_estrutura,f_060.subgru_estrutura,f_060.item_estrutura,f_060.descricao_item || '#$' || nvl(f_060.observacao,' ')),
               4,f_060.seq_item_nfisc,0);



  cursor c_NFt (c_cod_empresa    number,
                c_num_nota       number,
                c_cod_serie_nota varchar2,
                c_cgc9           number,
                c_cgc4           number,
                c_cgc2           number,
                c_tip_titulo     number) IS

    select num_duplicata,    seq_duplicatas,
           data_venc_duplic, valor_duplicata
    from (
    select f.num_duplicata,    f.seq_duplicatas,
           f.data_prorrogacao data_venc_duplic, f.valor_duplicata
    from fatu_070 f
    where f.codigo_empresa   = c_cod_empresa
      and f.num_duplicata    = c_num_nota
      and f.cli_dup_cgc_cli9 = c_cgc9
      and f.cli_dup_cgc_cli4 = c_cgc4
      and f.cli_dup_cgc_cli2 = c_cgc2
      and f.tipo_titulo      = c_tip_titulo
      and f.serie_nota_fisc  = c_cod_serie_nota
    and (exists (select 1 from loja_010
                   where (loja_010.forma_pgto      = f.cod_forma_pagto)
                     and (loja_010.forma_pagto_nfe = 14 or loja_010.forma_pagto_nfe = 0)) or f.cod_forma_pagto = 0)
    UNION ALL
    select f.num_duplicata,    f.seq_duplicatas,
           f.data_prorrogacao data_venc_duplic, f.valor_duplicata
    from fatu_070 f
    where f.codigo_empresa   = c_cod_empresa
      and f.num_nota_fiscal    = c_num_nota
      and f.cli_dup_cgc_cli9 = c_cgc9
      and f.cli_dup_cgc_cli4 = c_cgc4
      and f.cli_dup_cgc_cli2 = c_cgc2
      and f.serie_nota_fisc  = c_cod_serie_nota
      and f.titulo_loja      = 1
      and f.loja_fat        <> 1
      and exists (select 1 from loja_010
                  where loja_010.forma_pgto = f.cod_forma_pagto
                  and loja_010.forma_pagto_nfe = 14)
    )
    order by seq_duplicatas;

 v_erro                      EXCEPTION;
 v_ambiente                  number;
 v_cod_cidade_ibge           number;
 v_obrf_150_id               number;
 v_obrf_151_id               number;
 v_tipo_contingencia         number;
 v_obrf_152_id               number;
 v_obrf_155_id               number;
 v_obrf_156_id               number;
 v_cobranca_id               number;
 v_cod_barras                varchar2(20);
 v_tipo_codigo_ean           number;
 w_ex_ipi                    basi_240.codigo_ex_ipi%type;
 w_cod_enq_ipi               basi_240.cod_enquadramento_ipi%type;
 w_cod_enq_ipi_140           basi_240.cod_enquadramento_ipi%type;
 v_valor_pis                 obrf_015.valor_pis%type;
 v_valor_cofins              obrf_015.valor_cofins%type;
 v_um_faturamento_um         fatu_060.um_faturamento_um%type;
 v_um_faturamento_qtde       fatu_060.um_faturamento_qtde%type;
 v_um_faturamento_valor_unit fatu_060.um_faturamento_valor_unit%type;
 v_cont_dupli                number;
 v_cont_dupli_aux            number;
 v_base_ipi_saida            fatu_060.base_ipi%type;
 v_tipo_nf_ref               number;
 v_nota_ref                  obrf_010.documento%type;
 v_serie_ref                 obrf_010.serie%type;
 v_numero_danfe_ref          obrf_010.numero_danf_nfe%type;
 v_cod_fiscal_uf             number;
 v_ncm                       varchar2(10);
 v_ncm_genero                varchar2(10);
 v_cnpj9_ref                 obrf_010.cnpj9_ref%type;
 v_cnpj4_ref                 obrf_010.cnpj4_ref%type;
 v_cnpj2_ref                 obrf_010.cnpj2_ref%type;
 v_ind_empr_simples          fatu_503.ind_empresa_simples%type;
 v_nivel                     basi_010.nivel_estrutura%type;
 v_grupo                     basi_010.grupo_estrutura%type;
 v_subgrupo                  basi_010.subgru_estrutura%type;
 v_item                      basi_010.item_estrutura%type;
 v_produto                   varchar2(30);
 v_nfe_manut                 number;
 v_tem_imp_ii                varchar2(1);
 v_versao_layout_nfe         obrf_158.versao_layout_nfe%type;
 v_Concatena_Cfop            obrf_158.concatena_cfop%type;
 v_destaca_ipi               obrf_158.destaca_ipi%type;
 v_imp_tam_danfe             obrf_158.imp_tam_danfe%type;
 v_quebra_niv                fatu_500.niv_quebra_peca%type;
 v_cont_pcas                 number(5);
 v_tam_conca                 varchar2(300);
 v_tam_aux                   basi_220.ordem_tamanho%type;
 v_cfop                      varchar2(60);
 v_cfopn                     varchar2(60);
 v_cont                      number(3);
 v_valor_outros              fatu_060.valor_outros%type;
 v_valor_desc                fatu_060.valor_desc%type;
 v_valor_desc_sem_suframa    fatu_060.valor_desc%type;
 v_valor_seguro              fatu_060.valor_seguro%type;
 v_valor_frete               fatu_060.valor_frete%type;
 v_valor_mercadoria          number(15,2);
 v_valor_outros_aux          fatu_060.valor_outros%type;
 v_valor_desc_aux            fatu_060.valor_desc%type;
 v_valor_seguro_aux          fatu_060.valor_seguro%type;
 v_valor_frete_aux           fatu_060.valor_frete%type;
 v_num_item_max              number(9);
 v_uf_embarque               obrf_175.local_uf%type;
 v_local_embarque            obrf_175.embarque_local%type;
 v_x_loc_despacho            obrf_175.x_loc_despacho%type;
 v_valor_total_import        number(15,2);
 v_tem_nf_aj_ipi             number;
 w_modelo_documento          fatu_050.especie_docto%type;
 v_vlr_unit_tributavel       obrf_152.vlr_unit_tributavel%type;
 v_valor_unit_comercial      obrf_152.valor_unit_comercial%type;
 v_perc_reducao_140          obrf_157.icms_predbc%type;
 v_codigo_suframa            varchar2(10);
 v_data_cont                 obrf_158.data_hora_contigencia%type;
 v_just_cont                 obrf_158.justificativa_cont%type;
 v_inf_ad_suframa            fatu_060.inf_ad_prod%type;
 v_inf_ad_cst51              obrf_152.inf_ad_prod%type;
 v_data_cont_150             obrf_158.data_hora_contigencia%type;
 v_just_cont_150             obrf_158.justificativa_cont%type;
 v_qtde_itens                number(9);
 v_perc_icms_imp             fatu_060.perc_icms%type;
 v_flag_icms_imp             varchar2(1);
 v_procedencia_imp           basi_240.procedencia%type;
 v_flag_proc_imp             varchar2(1);
 v_mensagem_imp              obrf_152.inf_ad_prod%type;
 v_flag_msg_imp              varchar2(1);
 v_obs_aux                   varchar2(2000);
 v_numero_fci                obrf_152.numero_fci%type;
 v_numero_fci_aux            obrf_152.numero_fci%type;
 v_flag_nfci                 varchar2(1);
 v_tipo_cod_barras           fatu_500.tipo_cod_barras%type;
 v_inf_unmed_forn            fatu_504.inf_unmed_forn%type;
 unidade_medida              obrf_015.unid_med_trib%type;
 w_unid_med_trib_ncm         obrf_015.unid_med_trib%type;
 quantidade                  obrf_015.quantidade%type;
 valor_unitario              obrf_015.valor_unitario%type;
 c_015_unidade_medida_trib   obrf_015.unid_med_trib%type;
 c_015_quantidade_trib       number(15,5);
 c_015_valor_unitario_trib   obrf_015.valor_unitario%type;
 v_id_dest                   number(1);
 v_ind_final                 varchar2(1);
 v_ind_pres                  number(1);
 v_id_estrangeiro            varchar2(15);
 v_ind_ie_dest               number(1);
 v_n_draw                    number(11);
 v_diferenca                 number(15,5);
 v_ultima_nota_ref           varchar2(44);
 w_trans_devol               estq_005.tipo_transacao%type;
 v_perc_suframa               number(3);
 v_valor_suframa_icms_aux    number(15,2);
 v_desconto_sem_suframa      number(15,2);
 v_suframa                   number(15,2);
 v_inf_ad_prod               varchar2(1000);
 v_seq_item_draw             fatu_060.seq_item_nfisc%type;
 v_ie_substituto             fatu_012.ie_substituto%type;
 v_considera_suframa         pedi_080.considera_suframa%type;
 v_termo_cst51               obrf_158.termos_cst51%type;
 w_conta_nfe_devol           number;
 v_alterar_ncm               boolean;
 v_difal                     boolean;
 consumidor_nat              boolean;

 v_cnpj9_operadora_cartao  loja_010.cnpj9_operadora_cartao%type;
 v_cnpj4_operadora_cartao  loja_010.cnpj4_operadora_cartao%type;
 v_cnpj2_operadora_cartao  loja_010.cnpj2_operadora_cartao%type;
 v_forma_pagto_nfe         loja_010.forma_pagto_nfe%type;
 v_desc_forma_pgto         obrf_164.desc_forma_pgto%type;
 v_band_operadora          loja_010.band_operadora%type;
 v_band_operadora_pedi     pedi_100.bandeira_cartao%type;
 v_indPgto                 number;
 v_val_fcp_uf_ncm          number(15,5);
 v_perc_fcp_uf_ncm         number(15,5);
 v_bc_fcp                  number(15,5);
 v_val_fcp_uf_ncm_nf       number(15,5);
 v_FCP_UF_Dest_nf          number(15,5);
 v_bc_fcp_uf_dest          number(15,5);
 v_fcp_uf_dest             number(15,5);
 v_bc_uf_dest              number(15,5);
 p_fcp_uf_dest             number(15,5);
 v_valor_dup               fatu_070.valor_duplicata%type;
 v_valor_dup_aux           fatu_070.valor_duplicata%type;
 v_val_sem_pagto           fatu_070.valor_duplicata%type;
 v_seq_duplicata           number;
 v_especie_docto           obrf_010.especie_docto%type;
 w_considera_calculo_beneficio varchar2(1);
 cod_beneficio_fiscal      varchar2(10);
 v_codBeneficioFiscal      varchar2(10);
 v_codBeneficioFiscalAd    varchar2(50);
 v_ipi_tributado           number(15,5);
 v_ipiDevolucao            number(15,5);
 v_percIpiDevol            number(15,5);
 v_valIpiDevol             number(15,5);
 v_quantidadeDevol         number(15,3);
 v_imprime_boleto_danfe    fatu_503.imprime_boleto_danfe%type;
 valor_parcelas_orcamento  number(15,2);
 diff_troco                number(15,2);
 nome_cliente_entr       pedi_010.nome_cliente%type;
 telefone_cliente_entr    varchar2(14);
 email_entr          pedi_010.e_mail%type;
 insc_est_cliente_entr      pedi_010.insc_est_cliente%type;
 v_valor_percela_orcamento  number(12,2);
 telefone_cliente_dest   varchar2(10);
 v_intermediador           number(1);
 v_cgc9_intermediador        number(9);
 v_cgc4_intermediador        number(4);
 v_cgc2_intermediador        number(2);
 v_id_cad_int_tran           varchar2(60);
 w_inf_ad_prod               varchar2(2000);
 v_danfe_simplificada      number(1);
 v_valor_icms_deson        number;
 v_motivo_deson            number;
 v_perc_icms_deson         number;
 v_controleNarrativaBlocoK NUMBER;
 v_controleDescSimulBlocoK varchar(2);
 v_cod_empresa_loja_010    loja_010.cod_empresa%type;
 v_ident_terminal          loja_010.ident_terminal%type;
 v_receb_cgc9              fatu_500.cgc_9%type;
 v_receb_cgc4              fatu_500.cgc_4%type;
 v_receb_cgc2              fatu_500.cgc_2%type;
 v_eh_antecipado           number(1);
 v_avista                  number(1);
 v_d_pag                   pedi_100.data_autorizacao%type;
 v_pedido_antec            fatu_050.pedido_venda%type;
 v_perc_cred_presumido     fatu_060.perc_cred_presumido%type;
 v_valor_cred_presumido    fatu_060.valor_cred_presumido%type;
 v_cod_cbnef               fatu_060.cod_cbnef%type;
 v_entrou_cadastro         number;
 v_marca                   basi_400.codigo_informacao%type;
 v_colecao                 basi_030.colecao%type;

begin

  -- EMPRESA DO SIMPLES NACIONAL
  begin
     select fatu_503.ind_empresa_simples, fatu_503.imprime_boleto_danfe
     into   v_ind_empr_simples,           v_imprime_boleto_danfe
     from fatu_503
     where fatu_503.codigo_empresa = p_cod_empresa;
  end;
  -- PARAMETRO QUE INDICA SE ULTILIZARA OU NAO AS UNIDADES CONVERTIDAS
  begin
    select fatu_504.inf_unmed_forn
    into v_inf_unmed_forn
    from fatu_504
    where fatu_504.codigo_empresa = p_cod_empresa;
  end;

  --VERSAO LAYOUT NFE
  begin
     select obrf_158.versao_layout_nfe,         obrf_158.imp_tam_danfe ,
          obrf_158.data_hora_contigencia,      obrf_158.justificativa_cont,
            trim(obrf_158.termos_cst51)
     into   v_versao_layout_nfe,                    v_imp_tam_danfe,
          v_data_cont,                            v_just_cont,
      v_termo_cst51
     from obrf_158
     where obrf_158.cod_empresa = p_cod_empresa;
  exception
  when no_data_found then
     v_versao_layout_nfe := '1.10';
     v_imp_tam_danfe     := 0;
     v_data_cont := ' ';
     v_just_cont := ' ';
     v_termo_cst51       := null;
  end;

  begin
     SELECT val_int INTO v_controleNarrativaBlocoK from empr_008 where param = 'obrf.descricaoProdutoEmElaboracaoNF' and codigo_empresa = p_cod_empresa;
  exception
     when no_data_found then
     v_controleNarrativaBlocoK := 1;
  end;
   
  begin
     SELECT val_str INTO v_controleDescSimulBlocoK from empr_008 where param = 'obrf.descricaoSimultaneoEmElaboracaoNF' and codigo_empresa = p_cod_empresa;
  exception
     when no_data_found then
     v_controleDescSimulBlocoK := 'S';
  end;

  --Modo de impress?o
  begin
     select fatu_500.niv_quebra_peca, fatu_500.tipo_cod_barras,
      fatu_500.perc_suframa
     into   v_quebra_niv      , v_tipo_cod_barras,
      v_perc_suframa
     from fatu_500
     where fatu_500.codigo_empresa = p_cod_empresa;
  exception
      when no_data_found then
         v_quebra_niv := 3;
     v_tipo_cod_barras := 4;
  end;

  select obrf_158.nfe_ambiente, obrf_158.tipo_contingencia
  into   v_ambiente, v_tipo_contingencia
  from obrf_158
  where obrf_158.cod_empresa = p_cod_empresa;

  select trim(to_char(basi_167.codigo_fiscal_uf,'00'))
  into   v_cod_cidade_ibge
  from fatu_500,
       basi_160,
       basi_167
   where fatu_500.codigo_empresa = p_cod_empresa
     and fatu_500.codigo_cidade  = basi_160.cod_cidade
     and basi_160.estado         = basi_167.estado
     and basi_160.codigo_pais    = basi_167.codigo_pais;


  BEGIN
    insert into obrf_150
    (cod_empresa
    ,cod_usuario
    ,tipo_ambiente
    ,emit_codigo_uf
    ,tipo_documento)
    values
    (p_cod_empresa
    ,p_cod_id
    ,v_ambiente
    ,v_cod_cidade_ibge
    ,5);
  EXCEPTION
     WHEN OTHERS THEN
        p_msg_erro := 'N?o inseriu dados na tabela obrf_150' || Chr(10) || SQLERRM;
  END;


  if p_str_entr_said = 'E'
  then
    -- NOTAS DE ENTRADAS

    for f_nfestat in c_NFestat
    LOOP
      BEGIN
        insert into obrf_150
        (cod_empresa
        ,cod_usuario
        ,obrf_150.numero
        ,serie
        ,tipo_ambiente
        ,chave_acesso
        ,tipo_documento
        ,nr_recibo
        ,versao_systextilnfe)
        values
        (p_cod_empresa
        ,p_cod_id
        ,f_nfestat.documento
        ,f_nfestat.serie
        ,f_nfestat.Ambiente
        ,f_nfestat.Numero_Danf_Nfe
        ,f_nfestat.tipo_documento
        ,f_nfestat.nr_recibo
        ,f_nfestat.versao_systextilnfe);
      EXCEPTION
        WHEN OTHERS THEN
          p_msg_erro := 'N?o inseriu dados na tabela obrf_150, consulta Status nota' || Chr(10) || SQLERRM;
      END;
    END LOOP;


    for f_nfeI in c_NFeI
    LOOP
      BEGIN
        insert into obrf_150
        (cod_empresa
        ,cod_usuario
        ,tipo_ambiente
        ,emit_codigo_uf
        ,ano_inutilizacao
        ,emit_cnpj9
        ,emit_cnpj4
        ,emit_cnpj2
        ,modelo_documento
        ,serie
        ,numero
        ,nr_final_inut
        ,justificativa
        ,tipo_documento
        ,tipo_emissao
        ,versao_systextilnfe
        ,dest_cnpj9
        ,dest_cnpj4
        ,dest_cnpj2
        ,dest_nome
        ,dest_sigla_uf)
        values
        (p_cod_empresa
        ,p_cod_id
        ,f_nfeI.Ambiente
        ,f_nfeI.Cod_Cidade
        ,f_nfeI.Ano
        ,f_nfeI.Cgc_9
        ,f_nfeI.Cgc_4
        ,f_nfeI.Cgc_2
        ,55
        ,f_nfeI.Serie
        ,f_nfeI.Documento
        ,f_nfeI.Documento_Inut
        ,f_nfeI.Justificativa
        ,3
        ,0
        ,f_nfeI.Versao_Systextilnfe
        ,f_nfeI.cgc_dest_9
        ,f_nfeI.cgc_dest_4
        ,f_nfeI.cgc_dest_2
        ,f_nfeI.Dest_Nome
        ,f_nfeI.Estado_Dest);
      EXCEPTION
         WHEN OTHERS THEN
            p_msg_erro := 'N?o inseriu dados na tabela obrf_150' || Chr(10) || SQLERRM;
      END;
    END LOOP;

    -- NOTAS DE ENTRADAS
    for f_nfe in c_NFe
    LOOP
       BEGIN

          --EMPRESA DO SIMPLES
          if v_ind_empr_simples = '1' and v_versao_layout_nfe = '1.10'
          then
             f_nfe.base_icms := 0.00;
             f_nfe.valor_icms := 0.00;
          end if;

          if v_tipo_contingencia <> 3
          then
               if f_nfe.nfe_contigencia = 0
               then
                    v_tipo_contingencia := 1;
               end if;
          end if;

          begin
             select nvl(sum(decode(f_nfe.natoper_est_oper, 'EX', nvl(obrf_056.valor_pis_imp,0), obrf_015.valor_pis)),0.00),
                    nvl(sum(decode(f_nfe.natoper_est_oper, 'EX', nvl(obrf_056.valor_cofins_imp,0), obrf_015.valor_cofins)),0.00),
                    nvl(sum(obrf_056.valor_ii),0.00)
             into   v_valor_pis,             v_valor_cofins, v_valor_total_import
             from obrf_015, obrf_056
             where obrf_015.capa_ent_nrdoc   = f_nfe.documento
               and obrf_015.capa_ent_serie   = f_nfe.serie
               and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
               and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
               and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2
               and obrf_056.capa_ent_nrdoc   (+) = obrf_015.capa_ent_nrdoc
               and obrf_056.capa_ent_serie   (+) = obrf_015.capa_ent_serie
               and obrf_056.capa_ent_forcli9 (+) = obrf_015.capa_ent_forcli9
               and obrf_056.capa_ent_forcli4 (+) = obrf_015.capa_ent_forcli4
               and obrf_056.capa_ent_forcli2 (+) = obrf_015.capa_ent_forcli2
               and obrf_056.sequencia        (+) = obrf_015.sequencia;
          exception
            when access_into_null then
              v_valor_pis    := 0.00;
              v_valor_cofins := 0.00;
              v_valor_total_import := 0.00;
          end;

          begin
            select obrf_158.concatena_cfop,   obrf_158.destaca_ipi
            into v_Concatena_Cfop,            v_destaca_ipi
            from obrf_158
            where obrf_158.cod_empresa = p_cod_empresa;
          exception
            when no_data_found then
              v_Concatena_Cfop := 'N';
              v_destaca_ipi := 0;
          end;

          v_cont := 0;

          if v_Concatena_Cfop = 'S'
          then
             v_cfop := '';
             BEGIN

               for f_nfe_item_c in (
                 select obrf_015.natitem_nat_oper, obrf_015.natitem_est_oper from obrf_015
                 where obrf_015.capa_ent_nrdoc   = f_nfe.documento
                   and obrf_015.capa_ent_serie   = f_nfe.serie
                   and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
                   and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
                   and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2
                 group by obrf_015.natitem_nat_oper, obrf_015.natitem_est_oper)
               LOOP

                 v_cont := v_cont + 1;
                 select substr(replace(p_080.cod_natureza || substr(to_char(p_080.divisao_natur,'00'),3,1),'.','') || ' ' ||p_080.descr_nat_oper,1,60)
                   into v_cfopn
                 from pedi_080 p_080
                 where p_080.natur_operacao = f_nfe_item_c.natitem_nat_oper
                   and p_080.estado_natoper = f_nfe_item_c.natitem_est_oper;

                 if v_cont = 1
                 then
                   v_cfop := v_cfopn;
                 else
                   v_cfop := substr(v_cfop ||' - ' ||v_cfopn,1,60);
                 end if;

               END LOOP;
             EXCEPTION
               WHEN OTHERS THEN
                 p_msg_erro := 'N?o juntou as CFOPs' || Chr(10) || SQLERRM;
             END;

          end if;

          if (v_quebra_niv = 1 and v_imp_tam_danfe = 1)
          then

             v_cont_pcas := 0;
             begin
                select count(obrf_015.coditem_nivel99)
                into v_cont_pcas
                from obrf_015
                where obrf_015.capa_ent_nrdoc   = f_nfe.documento
                  and obrf_015.capa_ent_serie   = f_nfe.serie
                  and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
                  and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
                  and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2
                  and obrf_015.coditem_nivel99  = '1';
             exception
                 when no_data_found then
                    v_cont_pcas := 0;
             end;

             if v_cont_pcas > 0
             then

                begin
                   delete from obrf_161
                   where obrf_161.codigo_empresa  = p_cod_empresa
                     and obrf_161.num_nota_fiscal = f_nfe.documento
                     and obrf_161.serie_nota_fisc = f_nfe.serie;
                exception
                    when others then
                       p_msg_erro := 'N?o removeu dados da tabela obrf_161' || Chr(10) || SQLERRM;
                end;

                for f_nfe_item_tam in (
                   select (obrf_015.coditem_nivel99||'.'||obrf_015.coditem_grupo) as grupo,
                          to_number(trim(to_char(obrf_015.procedencia, '0')) || trim(to_char(obrf_015.cod_vlfiscal_icm, '00'))) as CST,
                          replace(pedi_080.cod_natureza || substr(to_char(pedi_080.divisao_natur,'00'),3,3),'.','') as CFOP,
                          obrf_015.classific_fiscal as classificacao,
                          obrf_015.unidade_medida as unidade,
                          obrf_015.valor_unitario as valor_unitario
                   from obrf_015, pedi_080, obrf_010
                   where obrf_015.capa_ent_nrdoc   = f_nfe.documento
                     and obrf_015.capa_ent_serie   = f_nfe.serie
                     and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
                     and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
                     and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2
                     and obrf_015.capa_ent_nrdoc   = obrf_010.documento
                     and obrf_015.capa_ent_serie   = obrf_010.serie
                     and obrf_015.capa_ent_forcli9 = obrf_010.cgc_cli_for_9
                     and obrf_015.capa_ent_forcli4 = obrf_010.cgc_cli_for_4
                     and obrf_015.capa_ent_forcli2 = obrf_010.cgc_cli_for_2
                     and pedi_080.natur_operacao   = obrf_015.natitem_nat_oper
                     and pedi_080.estado_natoper   = obrf_015.natitem_est_oper
                     and obrf_015.coditem_nivel99  = '1'
                   group by obrf_015.capa_ent_nrdoc,
                            obrf_015.capa_ent_serie,
                            obrf_015.capa_ent_forcli9,
                            obrf_015.capa_ent_forcli4,
                            obrf_015.capa_ent_forcli2,
                            obrf_015.coditem_nivel99,
                            obrf_015.coditem_grupo,
                            (obrf_015.coditem_nivel99||'.'||obrf_015.coditem_grupo),
                            obrf_015.classific_fiscal,
                            to_number(trim(to_char(obrf_015.procedencia, '0')) || trim(to_char(obrf_015.cod_vlfiscal_icm, '00'))),
                            replace(pedi_080.cod_natureza || substr(to_char(pedi_080.divisao_natur,'00'),3,3),'.',''),
                            obrf_015.unidade_medida,
                            obrf_015.valor_unitario)
                LOOP

                   v_tam_conca := ' ';
                   v_tam_aux := 0;

                   for f_nfe_item_tam_grava in (
                      select trim(basi_220.descr_tamanho) as subgrupo
                            ,basi_220.ordem_tamanho    as ordem
                      from obrf_015, pedi_080, basi_220
                      where obrf_015.capa_ent_nrdoc           = f_nfe.documento
                        and obrf_015.capa_ent_serie           = f_nfe.serie
                        and obrf_015.capa_ent_forcli9         = f_nfe.fornecedor9
                        and obrf_015.capa_ent_forcli4         = f_nfe.fornecedor4
                        and obrf_015.capa_ent_forcli2         = f_nfe.fornecedor2
                        and obrf_015.coditem_nivel99          = '1'
                        and obrf_015.classific_fiscal         = f_nfe_item_tam.classificacao
                        and obrf_015.unidade_medida           = f_nfe_item_tam.unidade
                        and obrf_015.natitem_nat_oper         = pedi_080.natur_operacao
                        and obrf_015.natitem_est_oper         = pedi_080.estado_natoper
                        and obrf_015.valor_unitario           = f_nfe_item_tam.valor_unitario
                        and f_nfe_item_tam.grupo              = (obrf_015.coditem_nivel99||'.'||obrf_015.coditem_grupo)
                        and f_nfe_item_tam.cst                = to_number(trim(to_char(obrf_015.procedencia, '0')) || trim(to_char(obrf_015.cod_vlfiscal_icm, '00')))
                        and f_nfe_item_tam.cfop               = replace(pedi_080.cod_natureza || substr(to_char(pedi_080.divisao_natur,'00'),3,3),'.','')
                        and obrf_015.coditem_subgrupo         = basi_220.tamanho_ref
                      order by basi_220.ordem_tamanho)
                   LOOP

                      if f_nfe_item_tam_grava.ordem <> v_tam_aux
                      then
                         if v_tam_conca = ' '
                         then
                            v_tam_conca := ' - ' || trim(f_nfe_item_tam_grava.subgrupo);
                         else
                            v_tam_conca := v_tam_conca || ' - ' || trim(f_nfe_item_tam_grava.subgrupo);
                         end if;
                         v_tam_aux := f_nfe_item_tam_grava.ordem;
                      end if;

                   END LOOP;

                   begin
                      insert into obrf_161
                      (obrf_161.codigo_empresa
                      ,obrf_161.num_nota_fiscal
                      ,obrf_161.serie_nota_fisc
                      ,obrf_161.nivel_grupo
                      ,obrf_161.descricao_geral
                      ,obrf_161.cst
                      ,obrf_161.cfop
                      ,obrf_161.classific_fiscal
                      ,obrf_161.unidade_medida
                      ,obrf_161.valor_unitario
                      ,obrf_161.um_faturamento_um
                      ,obrf_161.tamanhos)
                      values
                      (p_cod_empresa
                      ,f_nfe.documento
                      ,f_nfe.serie
                      ,replace(f_nfe_item_tam.grupo, '.', '')
                      ,' '--f_nfe_item_tam.descricao_item
                      ,f_nfe_item_tam.cst
                      ,f_nfe_item_tam.cfop
                      ,f_nfe_item_tam.classificacao
                      ,f_nfe_item_tam.unidade
                      ,f_nfe_item_tam.valor_unitario
                      ,' '
                      ,v_tam_conca);
                   exception
                       when others then
                          p_msg_erro := 'N?o inseriu dados na tabela obrf_161' || Chr(10) || SQLERRM;
                   end;
                   commit;

                END LOOP;

             end if;
          end if;

          --PEGA OS VALORES PARA RATEIO NA CAPA DA NOTA
          v_valor_seguro := 0.00;
          v_valor_frete  := 0.00;
          v_valor_outros := 0.00;
          v_valor_desc   := 0.00;
          v_valor_mercadoria := 0.00;
          begin
            select  obrf_010.valor_seguro,    obrf_010.valor_frete,
                    obrf_010.valor_despesas,  obrf_010.valor_desconto,
                    obrf_010.valor_itens
            into    v_valor_seguro,           v_valor_frete,
                    v_valor_outros,           v_valor_desc,
                    v_valor_mercadoria
            from obrf_010
            where obrf_010.documento        = f_nfe.documento
              and obrf_010.serie            = f_nfe.serie
              and obrf_010.cgc_cli_for_9    = f_nfe.fornecedor9
              and obrf_010.cgc_cli_for_4    = f_nfe.fornecedor4
              and obrf_010.cgc_cli_for_2    = f_nfe.fornecedor2;
          exception
              when no_data_found then
                 v_valor_seguro := 0.00;
                 v_valor_frete  := 0.00;
                 v_valor_outros := 0.00;
                 v_valor_desc   := 0.00;
                 v_valor_mercadoria := 0.00;
          end;

          --SE TIVER AO MENOS UM VALOR DE SEGURO, FRETE, OUTROS OU DESCONTO EFETUA O LOOP DO RATEIO
          if ((v_valor_seguro > 0.00 or v_valor_frete > 0.00 or v_valor_outros > 0.00 or v_valor_desc > 0.00) and v_valor_mercadoria > 0.00) or ((f_nfe.tipo_nf_referenciada in (2,3))  and v_valor_mercadoria = 0.00)
          then

             BEGIN
                update obrf_015
                set obrf_015.valor_seguro   = 0.00,
                    obrf_015.valor_frete    = 0.00,
                    obrf_015.valor_outros   = 0.00,
                    obrf_015.valor_desc     = 0.00
                where obrf_015.capa_ent_nrdoc   = f_nfe.documento
                  and obrf_015.capa_ent_serie   = f_nfe.serie
                  and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
                  and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
                  and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2;
                commit;
             EXCEPTION
                 WHEN OTHERS THEN
                    p_msg_erro := 'N?o atualizou tabela obrf_015.' || Chr(10) || SQLERRM;
             END;

            if((f_nfe.tipo_nf_referenciada in (2,3,8)) and v_valor_mercadoria = 0.00) then
              begin
                select count(*)
                  into v_qtde_itens
                  from obrf_015
                 where obrf_015.capa_ent_nrdoc   = f_nfe.documento
                   and obrf_015.capa_ent_serie   = f_nfe.serie
                   and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
                   and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
                   and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2;
              exception
                when no_data_found then
                  v_qtde_itens := 0.00;
              end;
            end if;

            for f_nfe_item_rat in (
              select obrf_015.sequencia, obrf_015.valor_total from obrf_015
              where obrf_015.capa_ent_nrdoc   = f_nfe.documento
                and obrf_015.capa_ent_serie   = f_nfe.serie
                and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
                and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
                and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2
              order by obrf_015.sequencia)
            LOOP

              if((f_nfe.tipo_nf_referenciada in (2,3,8)) and v_valor_mercadoria = 0.00) then
                BEGIN

                  update obrf_015
                  set obrf_015.valor_seguro = trunc((v_valor_seguro / v_qtde_itens),2),
                      obrf_015.valor_frete  = trunc((v_valor_frete  / v_qtde_itens),2),
                      obrf_015.valor_outros = trunc((v_valor_outros / v_qtde_itens),2),
                      obrf_015.valor_desc   = trunc((v_valor_desc   / v_qtde_itens),2)
                  where obrf_015.capa_ent_nrdoc   = f_nfe.documento
                    and obrf_015.capa_ent_serie   = f_nfe.serie
                    and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
                    and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
                    and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2
                    and obrf_015.sequencia        = f_nfe_item_rat.sequencia;
                  commit;
                EXCEPTION
                  WHEN OTHERS THEN
                    p_msg_erro := 'N?o atualizou tabela obrf_015.' || Chr(10) || SQLERRM;
                END;
              else
                BEGIN
                  update obrf_015
                  set obrf_015.valor_seguro   = trunc(((f_nfe_item_rat.valor_total / v_valor_mercadoria) * v_valor_seguro),2),
                      obrf_015.valor_frete    = trunc(((f_nfe_item_rat.valor_total / v_valor_mercadoria) * v_valor_frete),2),
                      obrf_015.valor_outros   = trunc(((f_nfe_item_rat.valor_total / v_valor_mercadoria) * v_valor_outros),2),
                      obrf_015.valor_desc     = trunc(((f_nfe_item_rat.valor_total / v_valor_mercadoria) * v_valor_desc),2)
                  where obrf_015.capa_ent_nrdoc   = f_nfe.documento
                    and obrf_015.capa_ent_serie   = f_nfe.serie
                    and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
                    and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
                    and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2
                    and obrf_015.sequencia        = f_nfe_item_rat.sequencia;
                  commit;
                EXCEPTION
                  WHEN OTHERS THEN
                    p_msg_erro := 'N?o atualizou tabela obrf_015.' || Chr(10) || SQLERRM;
                END;
              end if;
            END LOOP;


            begin
               select sum(obrf_015.valor_frete),  sum(obrf_015.valor_seguro),
                      sum(obrf_015.valor_outros), sum(obrf_015.valor_desc)
               into   v_valor_frete_aux,          v_valor_seguro_aux,
                      v_valor_outros_aux,         v_valor_desc_aux
               from obrf_015
               where obrf_015.capa_ent_nrdoc   = f_nfe.documento
                 and obrf_015.capa_ent_serie   = f_nfe.serie
                 and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
                 and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
                 and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2;
            exception
                when no_data_found then
                  v_valor_frete_aux := 0.00;
                  v_valor_seguro_aux := 0.00;
                  v_valor_outros_aux := 0.00;
                  v_valor_desc_aux := 0.00;
            end;

            if (v_valor_frete_aux <> v_valor_frete or v_valor_seguro_aux <> v_valor_seguro or v_valor_outros_aux <> v_valor_outros or v_valor_desc_aux <> v_valor_desc)
            then
               begin
                  select obrf_015.sequencia
                  into v_num_item_max
                  from obrf_015
                  where obrf_015.capa_ent_nrdoc   = f_nfe.documento
                    and obrf_015.capa_ent_serie   = f_nfe.serie
                    and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
                    and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
                    and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2
                    and obrf_015.valor_total in (select max(f_015.valor_total) from obrf_015 f_015
                                                 where f_015.capa_ent_nrdoc   = obrf_015.capa_ent_nrdoc
                                                   and f_015.capa_ent_serie   = obrf_015.capa_ent_serie
                                                   and f_015.capa_ent_forcli9 = obrf_015.capa_ent_forcli9
                                                   and f_015.capa_ent_forcli4 = obrf_015.capa_ent_forcli4
                                                   and f_015.capa_ent_forcli2 = obrf_015.capa_ent_forcli2)
                    and rownum = 1;
               exception
                   when no_data_found then
                      v_num_item_max := 1;
               end;

               BEGIN
                 v_valor_frete_aux := (v_valor_frete - v_valor_frete_aux);
                 v_valor_seguro_aux := (v_valor_seguro - v_valor_seguro_aux);
                 v_valor_outros_aux := (v_valor_outros - v_valor_outros_aux);
                 v_valor_desc_aux := (v_valor_desc - v_valor_desc_aux);
                 update obrf_015
                 set obrf_015.valor_seguro   = (obrf_015.valor_seguro + v_valor_seguro_aux),
                     obrf_015.valor_frete    = (obrf_015.valor_frete + v_valor_frete_aux),
                     obrf_015.valor_outros   = (obrf_015.valor_outros + v_valor_outros_aux),
                     obrf_015.valor_desc     = (obrf_015.valor_desc + v_valor_desc_aux)
                 where obrf_015.capa_ent_nrdoc   = f_nfe.documento
                   and obrf_015.capa_ent_serie   = f_nfe.serie
                   and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
                   and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
                   and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2
                   and obrf_015.sequencia        = v_num_item_max;
                 commit;
               EXCEPTION
                   WHEN OTHERS THEN
                      p_msg_erro := 'N?o atualizou tabela obrf_015.' || Chr(10) || SQLERRM;
               END;
            end if;

          end if;

          begin
             select obrf_010.tipo_nf_referenciada,    obrf_010.nota_referenciada,
                    obrf_010.serie_referenciada,      obrf_010.cnpj9_ref,
                    obrf_010.cnpj4_ref,               obrf_010.cnpj2_ref
             into   v_tipo_nf_ref,                    v_nota_ref,
                    v_serie_ref,                      v_cnpj9_ref,
                    v_cnpj4_ref,                      v_cnpj2_ref
             from obrf_010
             where obrf_010.documento     = f_nfe.documento
               and obrf_010.serie         = f_nfe.serie
               and obrf_010.cgc_cli_for_9 = f_nfe.fornecedor9
               and obrf_010.cgc_cli_for_4 = f_nfe.fornecedor4
               and obrf_010.cgc_cli_for_2 = f_nfe.fornecedor2;
          exception
              when others then
                 v_tipo_nf_ref := 1;
                 v_nota_ref    := 0;
                 v_serie_ref   := '';
                 v_cnpj9_ref   := 0;
                 v_cnpj4_ref   := 0;
                 v_cnpj2_ref   := 0;
          end;

          if v_cnpj9_ref = 0 and v_cnpj4_ref = 0 and v_cnpj2_ref = 0 and (v_tipo_nf_ref = 2 or v_tipo_nf_ref = 8)
          then
             v_cnpj9_ref := f_nfe.fornecedor9;
             v_cnpj4_ref := f_nfe.fornecedor4;
             v_cnpj2_ref := f_nfe.fornecedor2;
          end if;

          if v_destaca_ipi = 1 and f_nfe.tipo_transacao = 'D'
          then
             f_nfe.valor_total_ipi := 0.00;
          end if;

          v_data_cont_150 := null;
          v_just_cont_150 := null;

          if v_tipo_contingencia = 3
          then
              v_data_cont_150 :=   v_data_cont;
              v_just_cont_150 :=   v_just_cont;
          end if;
          if v_tipo_contingencia = 5
          then
             v_data_cont_150 :=   f_nfe.data_hora_contigencia;
             v_just_cont_150 :=   f_nfe.justificativa_cont;
          end if;


          /*ENTRADAS */
            consumidor_nat  := false;

          if v_tipo_nf_ref = 0
          then
            v_ind_pres := 9;
          else
            v_ind_pres := 0;
          end if;

          v_ind_ie_dest := 1;
          v_ind_final   := 0;
          v_difal       := false;

          if f_nfe.inscr_est_forne is null and f_nfe.fornecedor4 = 0
          then
             v_ind_ie_dest := 2;
             v_ind_final   := 1;
             v_difal       := true;
          end if;

          if f_nfe.inscr_est_forne is null and f_nfe.fornecedor4 > 0 and f_nfe.consumidor_final = 'S'
          then
             v_ind_ie_dest   := 9;
             v_ind_final     := 1;
             v_ie_substituto := f_nfe.ie_substituto;
             v_difal         := true;

          end if;

          if f_nfe.consumidor_final = 'N' AND f_nfe.inscr_est_forne is null and f_nfe.fornecedor4 > 0
          then
             v_ind_ie_dest := 2;
             v_ind_final := 0;
          end if;

          if f_nfe.consumidor_final = 'N'
          then
          consumidor_nat := true;
          end if;

          if (f_nfe.consumidor_final = 'S') or
             (f_nfe.valor_icms > 0.00 and f_nfe.valor_icms_sub = 0.00 and
             (f_nfe.inscr_est_forne is null and f_nfe.fornecedor4 = 0) and
              f_nfe.natoper_est_oper in ('AM', 'BA', 'CE', 'GO', 'MG', 'MS', 'MT', 'PE', 'RN', 'SE' , 'SP'))
          then
             v_ind_ie_dest := 9;
             v_ind_final   := 1;
             v_ie_substituto := f_nfe.ie_substituto;
             v_difal       := true;
          end if;

          if f_nfe.natoper_est_oper = 'EX'
          then
            v_id_dest := 3;
            v_ind_ie_dest := 9;
            v_ind_final   := 0;
            v_id_estrangeiro := f_nfe.codigo_fornecedor;
          else
            if f_nfe.cidade_emit_estado = f_nfe.natoper_est_oper
            then


              v_id_dest := 1;
            else
              v_id_dest := 2;
            end if;
          end if;

          begin
            select estq_005.tipo_transacao
            into w_trans_devol
            from estq_005
            where estq_005.codigo_transacao = f_nfe.codigo_transacao;
            exception
                when no_data_found then
                  w_trans_devol := null;
          end;

          w_conta_nfe_devol := 0;

          if w_trans_devol = 'D' and (v_versao_layout_nfe = '3.10' or v_versao_layout_nfe = '4.00')
          then
             v_tipo_nf_ref := 4;
          end if;

          if w_trans_devol in ('R', 'D') and (v_versao_layout_nfe = '3.10' or v_versao_layout_nfe = '4.00')  /*cra */
          then

           select count(1)
                  into w_conta_nfe_devol
           from obrf_015 o
           where o.capa_ent_nrdoc   = f_nfe.documento
             and o.capa_ent_serie   = f_nfe.serie
             and o.capa_ent_forcli9 = f_nfe.fornecedor9
             and o.capa_ent_forcli4 = f_nfe.fornecedor4
             and o.capa_ent_forcli2 = f_nfe.fornecedor2
             and o.perc_icms_partilha > 0
             and exists (select 1 from fatu_050 f
                         where f.codigo_empresa  = p_cod_empresa
                           and f.num_nota_fiscal = o.num_nota_orig
                           and f.serie_nota_fisc = o.serie_nota_orig
                           and trunc(f.data_emissao)   < to_date('01-01-2019','dd-mm-yy'));

          end if;


          if w_conta_nfe_devol > 0 and trunc(f_nfe.o_data_emissao) < to_date('01-01-2020','dd-mm-yy')
          then
                f_nfe.val_fcp_uf_dest_nf   := 0;
                f_nfe.val_icms_uf_dest_nf  := 0;
                f_nfe.val_icms_uf_remet_nf := 0;
          end if;

          if f_nfe.val_fcp_uf_dest_nf is null
          then
              f_nfe.val_fcp_uf_dest_nf := 0;
          end if;
          if f_nfe.val_icms_uf_dest_nf is null
          then
              f_nfe.val_icms_uf_dest_nf := 0;
          end if;
          if f_nfe.val_icms_uf_remet_nf is null
          then
              f_nfe.val_icms_uf_remet_nf := 0;
          end if;

          if v_difal
          then
             v_val_fcp_uf_ncm_nf := 0;
             v_FCP_UF_Dest_nf    := f_nfe.val_fcp_uf_dest_nf;
          else
             v_val_fcp_uf_ncm_nf := f_nfe.val_fcp_uf_dest_nf;
             v_FCP_UF_Dest_nf    := 0;
          end if;

          /* ENTRADAS */
          insert into obrf_150
          (chave_acesso         /*1*/
          ,codigo_numerico      /*2*/
          ,digito_verificador   /*3*/
          ,natureza_operacao    /*4*/
          ,forma_pagamento      /*5*/
          ,modelo_documento     /*6*/
          ,serie                /*7*/
          ,numero               /*8*/
          ,data_emissao         /*9*/
          ,data_entrada_saida   /*10*/
          ,tipo_documento       /*11*/
          ,codigo_municipio     /*12*/
          ,tipo_impressao       /*13*/
          ,tipo_emissao         /*14*/
          ,tipo_ambiente        /*15*/
          ,processo_emissao     /*16*/
          ,versao_processo      /*17*/
          ,finalidade           /*18*/
          ,emit_cnpj9           /*19*/
          ,emit_cnpj4           /*20*/
          ,emit_cnpj2           /*21*/
          ,emit_nome            /*22*/
          ,emit_fantasia        /*23*/
          ,emit_logradouro      /*24*/
          ,emit_numero          /*25*/
          ,emit_complemento     /*26*/
          ,emit_bairro          /*27*/
          ,emit_cod_mun         /*28*/
          ,emit_nome_mun        /*29*/
          ,emit_sigla_uf        /*30*/
          ,emit_codigo_uf       /*31*/
          ,emit_cep             /*32*/
          ,emit_telefone        /*33*/
          ,emit_codigo_pais     /*34*/
          ,emit_nome_pais       /*35*/
          ,emit_ie              /*36*/
          ,emit_iest            /*37*/
          ,emit_im              /*38*/
          ,emit_cnae            /*39*/
          ,dest_cnpj9           /*40*/
          ,dest_cnpj4           /*41*/
          ,dest_cnpj2           /*42*/
          ,dest_nome            /*43*/
          ,dest_logradouro      /*44*/
          ,dest_numero          /*45*/
          ,dest_complemento     /*46*/
          ,dest_bairro          /*47*/
          ,dest_cod_mun         /*48*/
          ,dest_nome_mun        /*49*/
          ,dest_sigla_uf        /*50*/
          ,dest_nome_pais       /*51*/
          ,dest_telefone        /*52*/
          ,dest_cep             /*53*/
          ,dest_codigo_pais     /*54*/
          ,dest_ie              /*55*/
          ,dest_isuframa        /*56*/
          ,ret_cnpj9            /*57*/
          ,ret_cnpj4            /*58*/
          ,ret_cnpj2            /*59*/
          ,ret_logradouro       /*60*/
          ,ret_numero           /*61*/
          ,ret_complemento      /*62*/
          ,ret_bairro           /*63*/
          ,ret_codigo_mun       /*64*/
          ,ret_nome_mun         /*65*/
          ,ret_sigla_uf         /*66*/
          ,entr_cnpj9           /*67*/
          ,entr_cnpj4           /*68*/
          ,entr_cnpj2           /*69*/
          ,entr_logradouro      /*70*/
          ,entr_numero          /*71*/
          ,entr_complemento     /*72*/
          ,entr_bairro          /*73*/
          ,entr_codigo_mun      /*74*/
          ,entr_nome_mun        /*75*/
          ,entr_sigla_uf        /*76*/
          ,icmstot_vbc          /*77*/
          ,icmstot_vicms        /*78*/
          ,icmstot_vbcst        /*79*/
          ,icmstot_vst          /*80*/
          ,icmstot_vprod        /*81*/
          ,icmstot_vfrete       /*82*/
          ,icmstot_vseg         /*83*/
          ,icmstot_vdesc        /*84*/
          ,icmstot_vii          /*85*/
          ,icmstot_vipi         /*86*/
          ,icmstot_vpis         /*87*/
          ,icmstot_vcofins      /*88*/
          ,icmstot_voutro       /*89*/
          ,icmstot_vnf          /*90*/
          ,issqntot_vserv       /*91*/
          ,issqntot_vbc         /*92*/
          ,issqntot_viss        /*93*/
          ,issqntot_vpis        /*94*/
          ,issqntot_vcofins     /*95*/
          ,rettrib_vretpis      /*96*/
          ,rettrib_vretcofins   /*97*/
          ,rettrib_vretcsll     /*98*/
          ,rettrib_vbcirrf      /*99*/
          ,rettrib_virrf        /*100*/
          ,rettrib_vbcretprev   /*101*/
          ,rettrib_vretprev     /*102*/
          ,modfrete             /*103*/
          ,transp_cnpj9         /*104*/
          ,transp_cnpj4         /*105*/
          ,transp_cnpj2         /*106*/
          ,transp_nome          /*107*/
          ,transp_ie            /*108*/
          ,transp_endereco      /*109*/
          ,transp_nomemunicipio /*110*/
          ,transp_sigla_uf      /*111*/
          ,transp_vserv         /*112*/
          ,transp_vbcret        /*113*/
          ,transp_picmsret      /*114*/
          ,transp_vicmsret      /*115*/
          ,transp_cfop          /*116*/
          ,transp_cmunfg        /*117*/
          ,transp_placa         /*118*/
          ,transp_placa_uf      /*119*/
          ,transp_rntc          /*120*/
          ,transp_reb_placa     /*121*/
          ,transp_reb_uf        /*122*/
          ,transp_reb_rntc      /*123*/
          ,transp_qvol          /*124*/
          ,transp_esp           /*125*/
          ,transp_marca         /*126*/
          ,transp_nvol          /*127*/
          ,transp_pesol         /*128*/
          ,transp_pesob         /*129*/
          ,nr_recibo            /*130*/
          ,nr_protocolo         /*131*/
          ,justificativa        /*132*/
          ,ano_inutilizacao     /*133*/
          ,nr_final_inut        /*134*/
          ,cod_status           /*135*/
          ,msg_status           /*136*/
          ,cod_empresa          /*137*/
          ,cod_usuario      /*138*/
          ,usuario        /*139*/
          ,dest_e_mail          /*140*/
          ,dest_nfe_e_mail      /*141*/
          ,user_id_e_mail       /*142*/
          ,user_e_mail          /*143*/
          ,user_senha_e_mail    /*144*/
          ,infadic_fisco        /*145*/
          ,infadic_cpl          /*146*/
          ,cod_crt              /*147*/
          ,versao_systextilnfe  /*148*/
          ,hora_entrada_saida   /*149*/
          ,justificativa_cont   /*150*/
          ,data_hora_contigencia  /*151*/
          ,id_dest                /*152*/
          ,ind_final              /*153*/
          ,ind_pres               /*154*/
          ,id_estrangeiro         /*155*/
          ,ind_ie_dest            /*156*/
          ,v_icms_deson           /*157*/
          ,vFCPUFDest             /*158*/
          ,vICMSUFDest            /*159*/
          ,vICMSUFRemet           /*160*/
          ,vfcpstret               /*161*/
          ,vfcpst                   /*162*/
          ,vipidevol              /*163*/
          ,vfcp                  /*164*/
          ,ind_envia_boleto_email /*165*/
      ,resp_tec_cgc9    /*166*/
      ,resp_tec_cgc4    /*167*/
      ,resp_tec_cgc2    /*168*/
      ,resp_tec_nome    /*169*/
      ,resp_tec_email    /*170*/
      ,resp_tec_telefone  /*171*/
      ,resp_tec_idCSRT    /*172*/
      ,resp_tec_hashCSRT  /*173*/
      ,nfe_contigencia    /*174*/
         )
          values
          (f_nfe.numero_danf_nfe  /*1*/
          ,null                   /*2*/
          ,null                   /*3*/
          ,decode(f_nfe.nota_estorno,0,substr(inter_fn_retira_acentuacao(decode(v_Concatena_Cfop,'S',v_cfop,f_nfe.natoper_nat_oper)),1,60),'999 - Estorno de NF-e n?o cancelada no prazo legal')                       /*4*/
          ,0                                            /*5 Devera vir da supr_050 campo ainda nao existe*/
          ,'55'                                         /*6*/
          ,f_nfe.serie                                  /*7*/
          ,f_nfe.documento                              /*8*/
          ,f_nfe.o_data_emissao                         /*9*/
          ,f_nfe.data_saida                             /*10*/
          ,f_nfe.entrada_saida                          /*11*/
          ,f_nfe.cod_cidade_ibge                        /*12*/
          ,f_nfe.formato_danfe+1                     /*13*/ -- rafaelsalvador
          ,v_tipo_contingencia                          /*14*/ -- NORMAL
          ,f_nfe.nfe_ambiente                           /*15*/
          ,0                                            /*16*/ -- EMITIDO PELO CONTRIBUINTE NO PROPRIO SISTEMA
          ,'5'                                          /*17*/ -- Vers?o do Systextil
          ,decode(v_tipo_nf_ref,0,1,9,1,8,1,v_tipo_nf_ref)      /*18*/ -- FINALIDADE
          ,f_nfe.f_cgc_9                                             /*19*/
          ,f_nfe.f_cgc_4                                             /*20*/
          ,f_nfe.f_cgc_2                                             /*21*/
          ,inter_fn_retira_acentuacao(f_nfe.nome_empresa)            /*22*/
          ,inter_fn_retira_acentuacao(f_nfe.f500_nome_fantasia)      /*23*/
          ,inter_fn_retira_acentuacao(f_nfe.f_endereco)              /*24*/
          ,f_nfe.f_numero                                            /*25*/
          ,inter_fn_retira_acentuacao(f_nfe.complemento)             /*26*/
          ,inter_fn_retira_acentuacao(f_nfe.f_bairro)                /*27*/
          ,f_nfe.cidade_emit_cod_cidade_ibge                          /*28*/
          ,inter_fn_retira_acentuacao(f_nfe.cidade_emit_cidade)       /*29*/
          ,f_nfe.cidade_emit_estado                                  /*30*/
          ,substr(f_nfe.cod_cidade_ibge,1,2)                         /*31*/  --,substr(f_nfe.cod_cidade_ibge,1,2)
          ,trim(to_char(f_nfe.cep_empresa,'00000000'))                                         /*32*/
          ,f_nfe.f500_telefone_emit                                       /*33*/
          ,f_nfe.cod_pais_fiscal_emit                                 /*34*/
          ,f_nfe.pais_nfe_nome_pais_emit                              /*35*/
          ,substr(replace(replace(f_nfe.inscr_est_emit,'.',''),'-',''),1,14)                                      /*36*/
          ,decode(f_nfe.valor_icms_sub, 0,null,f_nfe.ie_substituto)                                                     /*37*/
          ,null             /*38*/
          ,null                            /*39*/
          ,decode(f_nfe.natoper_est_oper,'EX',null,f_nfe.fornecedor9)                                            /*40*/
          ,decode(f_nfe.natoper_est_oper,'EX',null,f_nfe.fornecedor4)                                            /*41*/
          ,decode(f_nfe.natoper_est_oper,'EX',null,f_nfe.fornecedor2)                                            /*42*/
          ,inter_fn_retira_acentuacao(f_nfe.nome_fornecedor)            /*43*/
          ,inter_fn_retira_acentuacao(f_nfe.endereco_forne)             /*44*/
          ,f_nfe.s010_numero_imovel_forn                                   /*45*/
          ,inter_fn_retira_acentuacao(f_nfe.s010_complemento_forn)           /*46*/
          ,inter_fn_retira_acentuacao(f_nfe.s010_bairro_forn)                /*47*/
          ,decode(f_nfe.natoper_est_oper,'EX','9999999', f_nfe.cidade_cod_cidade_ibge_forn)    /*48*/
          ,inter_fn_retira_acentuacao(decode(f_nfe.natoper_est_oper,'EX','EXTERIOR', f_nfe.cidade_nfe_cidade_forn))    /*49*/
          ,decode(f_nfe.natoper_est_oper,'EX','EX',f_nfe.cidade_nfe_estado_forn)                  /*50*/
          ,f_nfe.pais_forn_nome_pais                                 /*51*/
          ,f_nfe.f500_telefone_forn                                  /*52*/
          ,trim(to_char(f_nfe.cep_fornecedor,'00000000'))                      /*53*/
          ,f_nfe.pais_forn_codigo_pais                                  /*54*/
          ,substr(replace(replace(decode(f_nfe.f_inscr_estadual_forn,'ISENTO',null,f_nfe.f_inscr_estadual_forn),'.',''),'-',''),1,14)           /*55*/
          ,null                            /*56*/
          ,null                            /*57*/
          ,null                            /*58*/
          ,null                            /*59*/
          ,null                            /*60*/
          ,null                            /*61*/
          ,null                            /*62*/
          ,null                            /*63*/
          ,null                            /*64*/
          ,null                            /*65*/
          ,null                            /*66*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.cd_cli_cgc_cli9)            /*67*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.cd_cli_cgc_cli4)            /*68*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.cd_cli_cgc_cli2)            /*69*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.p150_end_entr_cobr))             /*70*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,f_nfe.p150_numero_imovel)              /*71*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.complemento_endereco))       /*72*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.bairro_entr_cobr))           /*73*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.cidade_entr_cod_cidade))     /*74*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.cidade_entr_cidade))              /*75*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.cidade_entr_estado))             /*76*/
          ,f_nfe.base_icms                     /*77*/
          ,f_nfe.valor_icms                    /*78*/
          ,f_nfe.base_icms_sub                 /*79*/
          ,f_nfe.valor_icms_sub                /*80*/
          ,f_nfe.valor_itens                   /*81*/
          ,f_nfe.valor_frete                   /*82*/
          ,f_nfe.valor_seguro                  /*83*/
          ,f_nfe.valor_desconto                /*84*/
          ,v_valor_total_import                /*85*/
          ,f_nfe.valor_total_ipi               /*86*/
          ,v_valor_pis                         /*87*/
          ,v_valor_cofins                      /*88*/
          ,f_nfe.valor_despesas                /*89*/
          ,f_nfe.total_docto                   /*90*/
          ,null                            /*91*/
          ,null                            /*92*/
          ,null                            /*93*/
          ,null                            /*94*/
          ,null                            /*95*/
          ,null                            /*96*/
          ,null                            /*97*/
          ,null                            /*98*/
          ,null                            /*99*/
          ,null                            /*100*/
          ,null                            /*101*/
          ,null                            /*102*/
          ,f_nfe.tipo_frete                /*103*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.trans_cgc9)                /*104*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.trans_cgc4)                /*105*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.trans_cgc2)                /*106*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.trans_nome))                /*107*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,f_nfe.trans_insest)              /*108*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.trans_end))                 /*109*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.trans_cidade))              /*110*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.trans_estado))              /*111*/
          ,null                            /*112*/
          ,null                            /*113*/
          ,null                            /*114*/
          ,null                            /*115*/
          ,null                            /*116*/
          ,null                            /*117*/
          ,null                            /*118*/
          ,null                            /*119*/
          ,null                            /*120*/
          ,null                            /*121*/
          ,null                            /*122*/
          ,null                            /*123*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.qtde_volumes)              /*124*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,f_nfe.especie_volumes)           /*125*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,f_nfe.marca_volumes)             /*126*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,f_nfe.numero_volume)             /*127*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.peso_liquido)              /*128*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.peso_bruto)                /*129*/
          ,f_nfe.nr_recibo                 /*130*/
          ,null                            /*131*/
          ,null                            /*132*/
          ,null                            /*133*/
          ,null                            /*134*/
          ,null                            /*135*/
          ,null                            /*136*/
          ,f_nfe.cod_empresa        /*137*/
          ,f_nfe.p_cod_id          /*138*/
          ,f_nfe.p_str_usuario             /*139*/
          ,''                              /*140*/
          ,f_nfe.nfe_e_mail                /*141*/
          ,f_nfe.user_id_e_mail            /*142*/
          ,f_nfe.f_user_e_mail             /*143*/
          ,f_nfe.senha_e_mail              /*144*/
          ,inter_fn_retira_acentuacao(substr(inter_fn_mensagens_nfe(f_nfe.cod_empresa,f_nfe.documento,f_nfe.serie,f_nfe.natureza_interna,f_nfe.natoper_est_oper,'F'),1,2000))    /*145*/
          ,inter_fn_retira_acentuacao(substr(inter_fn_mensagens_nfe(f_nfe.cod_empresa,f_nfe.documento,f_nfe.serie,f_nfe.natureza_interna,f_nfe.natoper_est_oper,'C'),1,4000))  /*146*/
          ,v_ind_empr_simples            /*147*/
          ,f_nfe.versao_systextilnfe     /*148*/
          ,null                          /*149*/
          ,v_just_cont_150               /*150*/
          ,v_data_cont_150               /*151*/
          ,v_id_dest                     /*152*/
          ,v_ind_final                   /*153*/
          ,v_ind_pres                    /*154*/
          ,v_id_estrangeiro              /*155*/
          ,v_ind_ie_dest                 /*156*/
          ,0                             /*157*/
          ,v_FCP_UF_Dest_nf              /*158*/
          ,f_nfe.val_icms_uf_dest_nf     /*159*/
          ,f_nfe.val_icms_uf_remet_nf    /*160*/
          ,0.00                          /*161 - vfcpstret*/
          ,0.00                          /*162 - vfcpst*/
          ,0.00                          /*163 - vipidevol*/
          ,decode(v_val_fcp_uf_ncm_nf,null,0,v_val_fcp_uf_ncm_nf)    /*164 - vfcp*/
          ,v_imprime_boleto_danfe        /*165*/
      ,f_nfe.resp_tec_cgc9   /*166*/
      ,f_nfe.resp_tec_cgc4   /*167*/
      ,f_nfe.resp_tec_cgc2   /*168*/
      ,f_nfe.resp_tec_nome   /*169*/
      ,f_nfe.resp_tec_email   /*170*/
      ,f_nfe.resp_tec_telefone   /*171*/
      ,f_nfe.resp_tec_idCSRT   /*172*/
      ,f_nfe.resp_tec_hashCSRT   /*173*/
      ,f_nfe.nfe_contigencia     /*174*/
       );
       EXCEPTION
          WHEN OTHERS THEN
             p_msg_erro := 'N?o inseriu dados na tabela obrf_150' || Chr(10) || SQLERRM;
       END;

       /* FORMAS DE PAGAMENTO */

       select obrf_150.id
       into   v_obrf_150_id
       from obrf_150
       where obrf_150.cod_empresa = p_cod_empresa
       and obrf_150.numero        = f_nfe.documento
       and obrf_150.serie         = f_nfe.serie
       and obrf_150.cod_usuario   = p_cod_id
       and rownum                 = 1
       order by obrf_150.id desc;

      /*ENTRADAS*/
      BEGIN
         insert into obrf_164
           (id_nfe
            ,tipo_pagamento
            ,valor_pagamento
            ,valor_troco

            ,bandeira_cartao
            ,cnpj9_card
            ,cnpj4_card
            ,cnpj2_card

            ,tipo_integracao
            ,num_autoriza_card)
        values
          (v_obrf_150_id
          ,'90'
          ,0 --f_nfe.total_docto
          ,0

          ,''
          ,0
          ,0
          ,0

          ,1
          ,'');

      EXCEPTION
      WHEN OTHERS THEN
         p_msg_erro := 'N?o inseriu dados na tabela obrf_164' || Chr(10) || SQLERRM;
      END;

       /*rafaels*/
       if v_tipo_nf_ref = 2 or (v_tipo_nf_ref = 3 and f_nfe.nota_estorno = 1)
       then

          if v_tipo_nf_ref = 2 or v_tipo_nf_ref = 8
          then
             begin
                select obrf_010.numero_danf_nfe into v_numero_danfe_ref
                from obrf_010
                where obrf_010.documento     = v_nota_ref
                  and obrf_010.serie         = v_serie_ref
                  and obrf_010.cgc_cli_for_9 = v_cnpj9_ref
                  and obrf_010.cgc_cli_for_4 = v_cnpj4_ref
                  and obrf_010.cgc_cli_for_2 = v_cnpj2_ref;
             exception
                when others then
                  v_numero_danfe_ref := '';
             end;
          end if;

          if(v_tipo_nf_ref = 3 and f_nfe.nota_estorno = 1)
          then
             begin
                select fatu_050.numero_danf_nfe into v_numero_danfe_ref
                from fatu_050
                where fatu_050.codigo_empresa = p_cod_empresa
                  and fatu_050.num_nota_fiscal = v_nota_ref
                  and fatu_050.serie_nota_fisc = v_serie_ref;
             exception
                when others then
                  v_numero_danfe_ref := '';
             end;
          end if;

          begin
             select basi_167.codigo_fiscal_uf into v_cod_fiscal_uf
             from basi_167
             where basi_167.estado = f_nfe.cidade_emit_estado
               and rownum = 1;
             exception
             when others then
               v_cod_fiscal_uf := 0;
          end;

          select obrf_150.id
          into   v_obrf_150_id
          from obrf_150
          where obrf_150.cod_empresa = p_cod_empresa
          and obrf_150.numero        = f_nfe.documento
          and obrf_150.serie         = f_nfe.serie
          and obrf_150.cod_usuario   = p_cod_id
          and rownum                 = 1
          order by obrf_150.id desc;

          /*ENTRADA*/
          BEGIN
             insert into obrf_151
               (id
               ,nfe_id
               ,chave_acesso
               ,codigo_uf
               ,ano_mes_emissao
               ,cnpj_emitente9
               ,cnpj_emitente4
               ,cnpj_emitente2
               ,modelo_documento
               ,serie
               ,numero
               ,cgc_cli_for_9
               ,cgc_cli_for_4
               ,cgc_cli_for_2)
            values
              (p_cod_id
              ,v_obrf_150_id
              ,v_numero_danfe_ref
              ,v_cod_fiscal_uf
              ,f_nfe.o_data_emissao
              ,f_nfe.f_cgc_9
              ,f_nfe.f_cgc_4
              ,f_nfe.f_cgc_2
              ,'01'
              ,to_number(v_serie_ref)
              ,v_nota_ref
              ,f_nfe.fornecedor9
              ,f_nfe.fornecedor4
              ,f_nfe.fornecedor2);
          EXCEPTION
          WHEN OTHERS THEN
             p_msg_erro := 'N?o inseriu dados na tabela obrf_151' || Chr(10) || SQLERRM;
          END;
       end if;

       --notas de produtor thiagoaqui
       if v_tipo_nf_ref = 9 -- alterado nf de produtor rural de tipo 4 para 9 devido nfe 3.1, 4 agora ? devolu??es
       then

          begin
             select basi_167.codigo_fiscal_uf into v_cod_fiscal_uf
             from basi_167
             where basi_167.estado = f_nfe.natoper_est_oper
               and rownum = 1;
             exception
                when others then
                    v_cod_fiscal_uf := 0;
          end;

          for f_nfe_nf_produtor in (
             select obrf_057.nota_produtor,    obrf_057.serie_produtor,
                    obrf_057.data_nf_produtor, obrf_057.modelo_nf_produtor
             from obrf_057
             where obrf_057.capa_ent_nrdoc   = f_nfe.documento
               and obrf_057.capa_ent_serie   = f_nfe.serie
               and obrf_057.capa_ent_forcli9 = f_nfe.fornecedor9
               and obrf_057.capa_ent_forcli4 = f_nfe.fornecedor4
               and obrf_057.capa_ent_forcli2 = f_nfe.fornecedor2
             order by obrf_057.nota_produtor,    obrf_057.serie_produtor,
                      obrf_057.data_nf_produtor, obrf_057.modelo_nf_produtor)
          LOOP
             select obrf_150.id
             into   v_obrf_150_id
             from obrf_150
             where obrf_150.cod_empresa = p_cod_empresa
             and obrf_150.numero        = f_nfe.documento
             and obrf_150.serie         = f_nfe.serie
             and obrf_150.cod_usuario   = p_cod_id
             and rownum                 = 1
             order by obrf_150.id desc;
             /*ENTRADA PRODUTOR*/
             BEGIN
                insert into obrf_151
                  (id
                  ,nfe_id
                  ,chave_acesso
                  ,codigo_uf
                  ,ano_mes_emissao
                  ,cnpj_emitente9
                  ,cnpj_emitente4
                  ,cnpj_emitente2
                  ,modelo_documento
                  ,serie
                  ,numero
                  ,cgc_cli_for_9
                  ,cgc_cli_for_4
                  ,cgc_cli_for_2)
               values
                 (p_cod_id
                 ,v_obrf_150_id
                 ,null
                 ,v_cod_fiscal_uf
                 ,f_nfe_nf_produtor.data_nf_produtor
                 ,f_nfe.f_cgc_9
                 ,f_nfe.f_cgc_4
                 ,f_nfe.f_cgc_2
                 ,f_nfe_nf_produtor.modelo_nf_produtor
                 ,f_nfe_nf_produtor.serie_produtor
                 ,f_nfe_nf_produtor.nota_produtor
                 ,f_nfe.fornecedor9
                 ,f_nfe.fornecedor4
                 ,f_nfe.fornecedor2);
             EXCEPTION
             WHEN OTHERS THEN
                p_msg_erro := 'N?o inseriu dados na tabela obrf_151' || Chr(10) || SQLERRM;
             END;
          END LOOP;
       end if;

       v_ultima_nota_ref := '999';

       -- NOTAS DE ENTRADAS
       for f_nfe_item in c_nfe_item(p_cod_empresa, f_nfe.documento, f_nfe.serie, f_nfe.fornecedor9,
                                    f_nfe.fornecedor4,f_nfe.fornecedor2, v_controleNarrativaBlocoK, v_controleDescSimulBlocoK)
       LOOP
          if v_tipo_nf_ref = 4
          then
             if f_nfe_item.nr_cupom is null
             then
                f_nfe_item.nr_cupom := 0;
             end if;

             if  f_nfe_item.nr_caixa is null
             then
                 f_nfe_item.nr_caixa := 0;
             end if;

             select obrf_150.id
             into   v_obrf_150_id
             from obrf_150
             where obrf_150.cod_empresa = p_cod_empresa
             and obrf_150.numero        = f_nfe.documento
             and obrf_150.serie         = f_nfe.serie
             and obrf_150.cod_usuario   = p_cod_id
             and rownum                 = 1
             order by obrf_150.id desc;

             if f_nfe_item.nr_cupom = 0
             then
                begin
                   select fatu_050.numero_danf_nfe into v_numero_danfe_ref
                   from fatu_050
                   where fatu_050.codigo_empresa = p_cod_empresa
                     and fatu_050.num_nota_fiscal = f_nfe_item.num_nota_orig
                     and fatu_050.serie_nota_fisc = f_nfe_item.serie_nota_orig;
                exception
                  when others then
                    v_numero_danfe_ref := '';
                end;

                if v_numero_danfe_ref = '' or v_numero_danfe_ref = ' ' or v_numero_danfe_ref is null
                then
                  begin
                   select obrf_990.chave_nfe
                   into v_numero_danfe_ref
                   from obrf_990
                   where obrf_990.tipo_nota   = 'E'
                     and obrf_990.cod_empresa = p_cod_empresa
                     and obrf_990.num_nota    = f_nfe.documento
                     and obrf_990.serie_nota  = f_nfe.serie
                     and obrf_990.cnpj9       = f_nfe.fornecedor9
                     and obrf_990.cnpj4       = f_nfe.fornecedor4
                     and obrf_990.cnpj2       = f_nfe.fornecedor2
           and rownum               = 1;
                   exception
                     when others then
                       v_numero_danfe_ref := '';
                   end;
                end if;

                if v_numero_danfe_ref <> v_ultima_nota_ref
                then
                   begin
                      select basi_167.codigo_fiscal_uf into v_cod_fiscal_uf
                      from basi_167
                      where basi_167.estado = f_nfe.cidade_emit_estado
                        and rownum = 1;
                      exception
                      when others then
                        v_cod_fiscal_uf := 0;
                   end;


                   begin
                       select obrf_151.id
                       into   v_obrf_151_id
                       from obrf_151
                       where obrf_151.nfe_id = v_obrf_150_id
                        and  obrf_151.chave_acesso = v_numero_danfe_ref;
                   exception
                   when no_data_found then

                      /* DEVOLUCAO DE VENDAS*/

                      BEGIN
                         insert into obrf_151
                           (id
                           ,nfe_id
                           ,chave_acesso
                           ,codigo_uf
                           ,ano_mes_emissao
                           ,cnpj_emitente9
                           ,cnpj_emitente4
                           ,cnpj_emitente2
                           ,modelo_documento
                           ,serie
                           ,numero
                           ,cgc_cli_for_9
                           ,cgc_cli_for_4
                           ,cgc_cli_for_2)
                        values
                          (p_cod_id
                          ,v_obrf_150_id
                          ,v_numero_danfe_ref
                          ,v_cod_fiscal_uf
                          ,f_nfe.o_data_emissao
                          ,f_nfe.f_cgc_9
                          ,f_nfe.f_cgc_4
                          ,f_nfe.f_cgc_2
                          ,'01'
                          ,f_nfe_item.serie_nota_orig
                          ,f_nfe_item.num_nota_orig
                          ,f_nfe.fornecedor9
                          ,f_nfe.fornecedor4
                          ,f_nfe.fornecedor2);
                      EXCEPTION
                      WHEN OTHERS THEN
                         p_msg_erro := 'N?o inseriu dados na tabela obrf_151' || Chr(10) || SQLERRM;
                      END;

                      v_ultima_nota_ref := v_numero_danfe_ref;
                   end;
                end if;
             else
                 -- DEVOLUCAO DE CUPOM FISCAL
                 begin
                    select obrf_200.modelo_documento into w_modelo_documento
                    from fatu_505, obrf_200
                    where fatu_505.codigo_especie = obrf_200.codigo_especie
                      and fatu_505.codigo_empresa = p_cod_empresa
                      and fatu_505.serie_nota_fisc = f_nfe_item.serie_nota_orig;

                    exception
                    when no_data_found then
                    w_modelo_documento := null;
                 end;

                 begin
                     select obrf_151.id
                     into   v_obrf_151_id
                     from obrf_151
                     where obrf_151.nfe_id = v_obrf_150_id
                       and obrf_151.n_coo  = f_nfe_item.nr_cupom
                       and obrf_151.n_ecf  = f_nfe_item.nr_caixa;
                 exception
                 when no_data_found then
                    BEGIN
                       insert into obrf_151
                         (id
                         ,nfe_id
                         ,chave_acesso
                         ,codigo_uf
                         ,ano_mes_emissao
                         ,cnpj_emitente9
                         ,cnpj_emitente4
                         ,cnpj_emitente2
                         ,modelo_documento
                         ,serie
                         ,numero
                         ,n_ecf
                         ,n_coo)
                      values
                        (p_cod_id
                        ,v_obrf_150_id
                        ,null
                        ,v_cod_fiscal_uf
                        ,f_nfe.o_data_emissao
                        ,f_nfe.f_cgc_9
                        ,f_nfe.f_cgc_4
                        ,f_nfe.f_cgc_2
                        ,w_modelo_documento
                        ,f_nfe_item.serie_nota_orig
                        ,f_nfe_item.num_nota_orig
                        ,f_nfe_item.nr_caixa
                        ,f_nfe_item.nr_cupom
                        );
                    EXCEPTION
                    WHEN OTHERS THEN
                       p_msg_erro := 'N?o inseriu dados na tabela obrf_151 (devolu??o de cupom fiscal)' || Chr(10) || SQLERRM;
                   END;
                end;
             end if; -- else v_numero_danfe_ref <> v_ultima_nota_ref
          end if; -- if v_tipo_nf_ref = 4

          if v_tipo_nf_ref = 8
          then

             select obrf_150.id
             into   v_obrf_150_id
             from obrf_150
             where obrf_150.cod_empresa = p_cod_empresa
             and obrf_150.numero        = f_nfe.documento
             and obrf_150.serie         = f_nfe.serie
             and obrf_150.cod_usuario   = p_cod_id
             and rownum                 = 1
             order by obrf_150.id desc;



             BEGIN
                 select obrf_010.numero_danf_nfe
                       into v_numero_danfe_ref
                 from obrf_010
                 where obrf_010.documento        = f_nfe_item.num_nota_orig
                   and obrf_010.serie            = f_nfe_item.serie_nota_orig
                   and obrf_010.cgc_cli_for_9    = f_nfe.fornecedor9
                   and obrf_010.cgc_cli_for_4    = f_nfe.fornecedor4
                   and obrf_010.cgc_cli_for_2    = f_nfe.fornecedor2;
              EXCEPTION
                 when others then
                   v_numero_danfe_ref := ' ';
              END;

             if v_numero_danfe_ref <> v_ultima_nota_ref
             then
               begin
                  select obrf_151.id
                  into   v_obrf_151_id
                  from obrf_151
                  where obrf_151.nfe_id = v_obrf_150_id
                    and  obrf_151.chave_acesso = v_numero_danfe_ref;
               exception
               when no_data_found then

                   --DEVOLUCAO DE VENDAS

                  BEGIN
                     insert into obrf_151
                       (id
                       ,nfe_id
                       ,chave_acesso
                       ,codigo_uf
                       ,ano_mes_emissao
                       ,cnpj_emitente9
                       ,cnpj_emitente4
                       ,cnpj_emitente2
                       ,modelo_documento
                       ,serie
                       ,numero
                       ,cgc_cli_for_9
                       ,cgc_cli_for_4
                       ,cgc_cli_for_2)
                    values
                      (p_cod_id
                      ,v_obrf_150_id
                      ,v_numero_danfe_ref
                      ,v_cod_fiscal_uf
                      ,f_nfe.o_data_emissao
                      ,f_nfe.f_cgc_9
                      ,f_nfe.f_cgc_4
                      ,f_nfe.f_cgc_2
                      ,'01'
                      ,f_nfe_item.serie_nota_orig
                      ,f_nfe_item.num_nota_orig
                      ,f_nfe.fornecedor9
                      ,f_nfe.fornecedor4
                      ,f_nfe.fornecedor2);
                  EXCEPTION
                  WHEN OTHERS THEN
                     p_msg_erro := 'N?o inseriu dados na tabela obrf_151' || Chr(10) || SQLERRM;
                  END;

                  v_ultima_nota_ref := v_numero_danfe_ref;
               end;
            end if;
          end if;

          -- troca os valores originais para os convertidos geovani
          if v_inf_unmed_forn = 'S' and
             trim(f_nfe_item.unidade_de_medida_conv) is not null and
             f_nfe_item.quantidade_conv is not null and
             trim(f_nfe_item.c_015_unidade_conv) is not null and
             f_nfe_item.c_015_quantidade_conv_trib is not null and
             f_nfe_item.c_015_valor_trib_conv is not null
          then
            unidade_medida            := f_nfe_item.unidade_de_medida_conv;
            quantidade                := f_nfe_item.quantidade_conv;
            valor_unitario            := f_nfe_item.valor_conv;
            c_015_unidade_medida_trib := f_nfe_item.c_015_unidade_conv;
            c_015_quantidade_trib     := f_nfe_item.c_015_quantidade_conv_trib;
            c_015_valor_unitario_trib := f_nfe_item.c_015_valor_trib_conv;
          else
                unidade_medida            := f_nfe_item.unidade_medida_aux;
                quantidade                := f_nfe_item.quantidade_aux;
                valor_unitario            := f_nfe_item.valor_unitario_aux;
                c_015_unidade_medida_trib := f_nfe_item.c_015_unidade_medida_trib_aux;
                c_015_quantidade_trib     := f_nfe_item.c_015_quantidade_trib_aux;
                c_015_valor_unitario_trib := f_nfe_item.c_015_valor_unitario_trib_aux;
          end if;
          -- fim da troca dos valores originais para os convertidos

          select obrf_150.id
          into   v_obrf_150_id
          from obrf_150
          where obrf_150.cod_empresa = p_cod_empresa
          and obrf_150.numero        = f_nfe.documento
          and obrf_150.serie         = f_nfe.serie
          and obrf_150.cod_usuario   = p_cod_id
          and rownum                 = 1
          order by obrf_150.id desc;

          --EMPRESA DO SIMPLES
          if v_ind_empr_simples = '1' and v_versao_layout_nfe = '1.10'
          then
             f_nfe_item.base_calc_icm  := 0.00;
             f_nfe_item.valor_icms     := 0.00;
             f_nfe_item.percentual_icm := 0.00;
          end if;

          if v_destaca_ipi = 1 and f_nfe.tipo_transacao = 'D'
          then
             f_nfe_item.valor_ipi := 0.00;
             f_nfe_item.base_ipi := 0.00;
             f_nfe_item.percentual_ipi := 0.00;
          end if;

          v_produto := f_nfe_item.produto;
          v_produto := REPLACE(v_produto,'.');

          begin
            select basi_030.tipo_codigo_ean
            into v_tipo_codigo_ean
            from basi_030
            where basi_030.nivel_estrutura = substr(v_produto,1,1)
               and basi_030.referencia = substr(v_produto,2,5);
          exception
              when no_data_found then
              v_tipo_codigo_ean := 0;
          end;

          if length(trim(f_nfe_item.codigo_barras)) in (13,14)
             and (v_tipo_cod_barras = 2
                    and ((substr(f_nfe_item.codigo_barras, 1, 3) in ('789','790') and length(trim(f_nfe_item.codigo_barras)) = 13)
                    or INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'faturamento.codEanXml') = 'S' )
                )
             and (f_nfe_item.tipo_impressao = '3' or f_nfe_item.tipo_impressao = '4' or f_nfe_item.tipo_impressao = '5' or
                 (f_nfe_item.tipo_impressao = '2' and v_tipo_codigo_ean = 2) or
                 (f_nfe_item.tipo_impressao = '2' and v_tipo_codigo_ean = 3) or
                 (f_nfe_item.tipo_impressao = '1' and v_tipo_codigo_ean = 3)) then
            v_cod_barras := f_nfe_item.codigo_barras;
          else
            v_cod_barras := '0000000000000';
          end if;

          if f_nfe_item.base_calculo_ii > 0.00 or f_nfe_item.valor_desp_adu > 0.00 or f_nfe_item.valor_ii > 0.00 or f_nfe_item.valor_iof > 0.00 or trim(f_nfe_item.numero_di) is not null
          then
             v_tem_imp_ii := 'S';
          else
             v_tem_imp_ii := 'N';
          end if;

          begin
             select basi_240.codigo_ex_ipi, basi_240.cod_enquadr_ipi_entr,
                    basi_240.unid_med_trib, basi_240.considera_calculo_beneficio
              into   w_ex_ipi,              w_cod_enq_ipi,
                    w_unid_med_trib_ncm,    w_considera_calculo_beneficio
             from basi_240
             where basi_240.classific_fiscal = f_nfe_item.classific_fiscal;
          exception
              when no_data_found then
                 w_ex_ipi                 := 0;
                 w_cod_enq_ipi            := '999';
                 w_unid_med_trib_ncm      := ' ';
                 w_considera_calculo_beneficio := ' ';
          end;

          if f_nfe_item.p_080_respeita_ipi = 2
          then w_cod_enq_ipi := '999';
          end if;

          if (length(v_produto) = 6)
           then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := '000';
             v_item     := '000000';
          elsif (length(v_produto) = 7) then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := substr(v_produto,7,1);
             v_item     := '000000';
          elsif (length(v_produto) = 8) then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := substr(v_produto,7,2);
             v_item     := '000000';
          elsif (length(v_produto) = 9) then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := substr(v_produto,7,3);
             v_item     := '000000';
          elsif (length(v_produto) = 13) then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := substr(v_produto,7,1);
             v_item     := substr(v_produto,8,6);
          elsif (length(v_produto) = 14) then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := substr(v_produto,7,2);
             v_item     := substr(v_produto,9,6);
          elsif (length(v_produto) = 15) then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := substr(v_produto,7,3);
             v_item     := substr(v_produto,10,6);
          end if;

          v_alterar_ncm := true;

          begin
             select basi_410.classific_fiscal
             into v_ncm
             from basi_410
             where basi_410.nivel    = v_nivel
               and basi_410.grupo    = v_grupo
               and basi_410.subgrupo = v_subgrupo
               and basi_410.item     = v_item
               and basi_410.pais     = f_nfe.codPaisDest;
          exception
              when no_data_found then
                 begin
                    select basi_410.classific_fiscal
                    into v_ncm
                    from basi_410
                    where basi_410.nivel    = v_nivel
                      and basi_410.grupo    = v_grupo
                      and basi_410.subgrupo = v_subgrupo
                      and basi_410.item     = '000000'
                      and basi_410.pais     = f_nfe.codPaisDest;
                 exception
                     when no_data_found then
                        begin
                           select basi_410.classific_fiscal
                           into v_ncm
                           from basi_410
                           where basi_410.nivel    = v_nivel
                             and basi_410.grupo    = v_grupo
                             and basi_410.subgrupo = '000'
                             and basi_410.item     = '000000'
                             and basi_410.pais     = f_nfe.codPaisDest;
                        exception
                            when others then
                               v_alterar_ncm := false;
                               v_ncm        := f_nfe_item.classific_fiscal;
                        end;
                 end;
          end;

          if v_alterar_ncm
          then
             update obrf_015 itens
             set itens.classific_fiscal = v_ncm
             where itens.capa_ent_nrdoc = f_nfe.documento
               and itens.capa_ent_serie = f_nfe.serie
               and itens.capa_ent_forcli9 = f_nfe.fornecedor9
               and itens.capa_ent_forcli4 = f_nfe.fornecedor4
               and itens.capa_ent_forcli2 = f_nfe.fornecedor2

               and itens.coditem_nivel99   = v_nivel
               and itens.coditem_grupo   = v_grupo
               and (itens.coditem_subgrupo  = v_subgrupo or v_subgrupo = '000')
               and (itens.coditem_item    = v_item     or v_item     = '000000');

          end if;

          v_ncm        := substr(REPLACE(v_ncm,'.'),1,8);
          v_ncm_genero := substr(REPLACE(v_ncm,'.'),1,2);

          -- QUANDO FOR OPERA??O EXTERIOR OU QUE INDIQUE FINS DE EXPORTA??O, UNIDADE DE MEDIDA DEVE SER PADR?O DA RFB - E N T R A D A S
          if (f_nfe_item.natitem_est_oper = 'EX' or f_nfe_item.ind_cfop_exportacao = 'S')
             and trim(w_unid_med_trib_ncm) is not null and
             c_015_unidade_medida_trib <> w_unid_med_trib_ncm and f_nfe_item.fator_umed_trib > 0.00000
          then

             c_015_unidade_medida_trib := w_unid_med_trib_ncm;

             if f_nfe_item.fator_umed_trib <> 1
             then
                c_015_quantidade_trib     := quantidade * f_nfe_item.fator_umed_trib;
                v_vlr_unit_tributavel     := round(f_nfe_item.valor_total / c_015_quantidade_trib,10);
                c_015_valor_unitario_trib := v_vlr_unit_tributavel;
             end if;

             update obrf_015
                 set obrf_015.unid_med_trib = w_unid_med_trib_ncm
                 where obrf_015.capa_ent_nrdoc   = f_nfe.documento
                   and obrf_015.capa_ent_serie   = f_nfe.serie
                   and obrf_015.capa_ent_forcli9 = f_nfe.fornecedor9
                   and obrf_015.capa_ent_forcli4 = f_nfe.fornecedor4
                   and obrf_015.capa_ent_forcli2 = f_nfe.fornecedor2
                   and obrf_015.classific_fiscal = f_nfe_item.classific_fiscal;
                 commit;
          end if;

          if (v_quebra_niv = 1 and v_imp_tam_danfe = 1)
          then
             v_tam_conca := ' ';

             if substr(f_nfe_item.produto, 1, 1) = '1'
             then
                begin
                   select obrf_161.tamanhos
                   into   v_tam_conca
                   from obrf_161
                   where obrf_161.codigo_empresa       = p_cod_empresa
                     and obrf_161.num_nota_fiscal      = f_nfe.documento
                     and obrf_161.serie_nota_fisc      = f_nfe.serie
                     and obrf_161.nivel_grupo          = substr(f_nfe_item.produto, 1, 6)
                     and obrf_161.descricao_geral      = ' ' --Notas de entrada de emiss?o pr?pria n?o fazem agrupamento por descri??o
                     and obrf_161.cst                  = f_nfe_item.cst
                     and obrf_161.cfop                 = f_nfe_item.cfop
                     and obrf_161.classific_fiscal     = f_nfe_item.classific_fiscal
                     and obrf_161.unidade_medida       = unidade_medida
                     and obrf_161.valor_unitario       = valor_unitario;
                exception
                    when no_data_found then
                       v_tam_conca := ' ';
                end;
             end if;
          end if;

          v_valor_unit_comercial := 0.0000000000;
          v_vlr_unit_tributavel  := 0.0000000000;


          -- se houver diferen?a no calculo dos valores, calcula o novo pre?o unitario comercial
          if (((round(valor_unitario * quantidade,10)) <> f_nfe_item.valor_total) and
           (f_nfe.tipo_nf_referenciada = 1 or f_nfe.tipo_nf_referenciada = 4))
          then
             if quantidade > 0
             then
                v_valor_unit_comercial := round(f_nfe_item.valor_total / quantidade,10);
             else
                v_valor_unit_comercial := valor_unitario;
             end if;
          else
             v_valor_unit_comercial := valor_unitario;
          end if;

          -- se houver diferen?a no calculo dos valores, calcula o novo pre?o unitario tributario
          if (((round(c_015_valor_unitario_trib * c_015_quantidade_trib,10)) <> f_nfe_item.valor_total) and
           (f_nfe.tipo_nf_referenciada = 1 or f_nfe.tipo_nf_referenciada = 4))
          then

             if c_015_quantidade_trib > 0
             then
                v_vlr_unit_tributavel := round(f_nfe_item.valor_total / c_015_quantidade_trib,10);
             else
                v_vlr_unit_tributavel := c_015_valor_unitario_trib;
             end if;
          else
             v_vlr_unit_tributavel := c_015_valor_unitario_trib;
          end if;
          ----

          /*CTS 51*/
          if f_nfe_item.pDif is null
          then f_nfe_item.pDif:= 0;
          end if;
          if f_nfe_item.vICMSOp is null
          then f_nfe_item.vICMSOp:= 0;
          end if;
          if f_nfe_item.vICMSDif is null
          then f_nfe_item.vICMSDif:= 0;
          end if;

          v_inf_ad_cst51 :=  '';
          v_obs_aux := '';

          if f_nfe_item.vICMSDif > 0 and  f_nfe_item.pDif < 100.00
          then
             /*Verifica se exite termo cadastrado para opera? com cst 51*/
             if v_termo_cst51 is null
             then
                if f_nfe.cidade_nfe_estado_forn = 'PR'
                then
                   v_termo_cst51 :=  ') nos termos do inciso I do art.96 do Decreto n?1.980/07 (RICMS/PR)';
                else
                   v_termo_cst51 :=  ') DIFERIMENTO PARCIAL ICMS CONF LIV III ART 1 -DO RICMS/97 -RS';
                end if;
             end if;

             v_inf_ad_cst51 :=  'Operacao com diferimento parcial do imposto de R$ ' || f_nfe_item.vICMSDif || ' (' || f_nfe_item.pDif || '% de ' || f_nfe_item.vICMSOp || ') ' || v_termo_cst51;
          end if;

          if trim(v_inf_ad_cst51) is not null
          then
             if trim(v_obs_aux) is not null
             then
                v_obs_aux := v_obs_aux||' - '|| v_inf_ad_cst51;
             else
                v_obs_aux := v_inf_ad_cst51;
             end if;
          end if;

          if trim(f_nfe_item.inf_ad_prod) is not null
          then
             if trim(v_obs_aux) is not null
             then
                v_obs_aux := v_obs_aux||' - '|| f_nfe_item.inf_ad_prod;
             else
                v_obs_aux := f_nfe_item.inf_ad_prod;
             end if;
          end if;

          if trim(v_tam_conca) is not null
          then
             if trim(v_obs_aux) is not null
             then
                v_obs_aux := v_obs_aux||'  '|| v_tam_conca;
             else
                v_obs_aux := v_tam_conca;
             end if;
          end if;



          if f_nfe_item.cest is null
          then
             f_nfe_item.cest := ' ';
          end if;

          begin
             select obrf_805.cod_beneficio_fiscal, obrf_805.cod_beneficio_fiscal_ad
             into v_codBeneficioFiscal, v_codBeneficioFiscalAd
             from obrf_805
             where obrf_805.uf_remetente         = f_nfe.cidade_emit_estado
               and obrf_805.cst_icms             = f_nfe_item.cod_vlfiscal_icm
               and obrf_805.classificacao_fiscal in (f_nfe_item.classific_fiscal, 'XXXXXXXXXXXXXXX')
               and obrf_805.natureza_operacao    in (f_nfe_item.natitem_nat_oper, 9999)
               and obrf_805.uf_destinatario      in (f_nfe.cidade_nfe_estado_forn,'XXX')
               and obrf_805.empresa_remetente    in (f_nfe.cod_empresa,9999)
               and obrf_805.cidade_destino       in (f_nfe.codigo_cidade,999999)
            order by obrf_805.natureza_operacao, obrf_805.classificacao_fiscal, obrf_805.uf_destinatario,
                     obrf_805.empresa_remetente, obrf_805.cidade_destino;
          exception
              when no_data_found then
                v_codBeneficioFiscalAd := '';
                if w_considera_calculo_beneficio = 'S' and f_nfe_item.ind_beneficio_icms = 'S'
                then
                   v_codBeneficioFiscal := INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'fiscal.codAjusteIcmsBeneficio');
                else
                   v_codBeneficioFiscal := '';
                end if;
          end;

          if trim(v_codBeneficioFiscalAd) is not null
          then
             if trim(v_obs_aux) is not null
             then
                v_obs_aux := v_obs_aux||'  '|| v_codBeneficioFiscalAd;
             else
                v_obs_aux := v_codBeneficioFiscalAd;
             end if;
          end if;

          select seq_obrf_152.nextval into v_obrf_152_id from dual;
          -- ENTRADA
          BEGIN
             insert into obrf_152
             (id
             ,nfe_id
             ,codigo
             ,cean
             ,descricao
             ,ncm
             ,extipi
             ,genero
             ,cfop
             ,unidade_comercial
             ,qtde_comercial
             ,valor_unit_comercial --12
             ,valor
             ,ceantrib
             ,unidade_tributavel
             ,qtde_tributavel
             ,vlr_unit_tributavel --17
             ,valor_frete
             ,valor_total_seguro
             ,valor_desconto
             ,valor_outros
             ,cod_empresa
             ,cod_usuario
             ,numero_di
             ,data_reg_decla
             ,local_desemb
             ,uf_desemb
             ,data_desemb
             ,codigo_expor
             ,numero_adi
             ,seq_adi
             ,cod_fab_adi
             ,vlr_desc_adi
             ,inf_ad_prod
             ,tp_inter_medio
             ,tp_via_transp
             ,v_afrmm
             ,n_draw
             ,cest
             ,ind_ordem_produto
             ,cod_beneficio_fiscal)
             values
             (v_obrf_152_id
             ,v_obrf_150_id
             ,f_nfe_item.produto
             ,v_cod_barras
             ,substr(inter_fn_retira_acentuacao(f_nfe_item.descricao_item),1,120)
             ,v_ncm
             ,decode(w_ex_ipi,0,null,w_ex_ipi)
             ,v_ncm_genero
             ,f_nfe_item.cod_natureza
             ,unidade_medida
             ,quantidade
             ,v_valor_unit_comercial --12
             ,f_nfe_item.valor_total
             ,v_cod_barras
             ,c_015_unidade_medida_trib
             ,c_015_quantidade_trib
             ,v_vlr_unit_tributavel --17
             ,decode(f_nfe_item.valor_frete, 0.00, null, f_nfe_item.valor_frete)
             ,decode(f_nfe_item.valor_seguro, 0.00, null, f_nfe_item.valor_seguro)
             ,decode(f_nfe_item.rateio_descontos_ipi, 0.00, null, f_nfe_item.rateio_descontos_ipi)
             ,decode(f_nfe_item.valor_outros, 0.00, null, f_nfe_item.valor_outros)
             ,p_cod_empresa
             ,p_cod_id
             ,decode(f_nfe_item.numero_di,'0',null,f_nfe_item.numero_di)
             ,f_nfe_item.data_declaracao
             ,f_nfe_item.local_desembaraco
             ,f_nfe_item.uf_desembaraco
             ,f_nfe_item.data_desembaraco
             ,f_nfe_item.codigo_exportador
             ,f_nfe_item.numero_adicao
             ,f_nfe_item.sequencia_adicao
             ,f_nfe_item.codigo_fabricante
             ,decode(f_nfe_item.valor_desconto_item, 0.00,null,f_nfe_item.valor_desconto_item)
             ,v_obs_aux
             ,1
             ,f_nfe_item.tp_via_transp
             ,f_nfe_item.v_afrmm
             ,f_nfe_item.n_draw
             ,f_nfe_item.cest
       ,f_nfe_item.tipo_impressao
             ,v_codBeneficioFiscal);

           if w_conta_nfe_devol > 0 and trunc(f_nfe.o_data_emissao) < to_date('01-01-2020','dd-mm-yy')
           then
              f_nfe_item.perc_fcp_uf_dest := 0;
              f_nfe_item.perc_icms_uf_dest := 0;
              f_nfe_item.perc_icms_partilha := 0;
              f_nfe_item.val_fcp_uf_dest := 0;
              f_nfe_item.val_icms_uf_dest := 0;
              f_nfe_item.val_icms_uf_remet := 0;
           end if;

           if f_nfe_item.perc_fcp_uf_dest is null
           then
              f_nfe_item.perc_fcp_uf_dest := 0;
           end if;
           if f_nfe_item.perc_icms_uf_dest is null
           then
              f_nfe_item.perc_icms_uf_dest := 0;
           end if;
           if f_nfe_item.perc_icms_partilha is null
           then
              f_nfe_item.perc_icms_partilha := 0;
           end if;
           if f_nfe_item.val_fcp_uf_dest is null
           then
              f_nfe_item.val_fcp_uf_dest := 0;
           end if;
           if f_nfe_item.val_icms_uf_dest is null
           then
             f_nfe_item.val_icms_uf_dest := 0;
           end if;
           if f_nfe_item.val_icms_uf_remet is null
           then
              f_nfe_item.val_icms_uf_remet := 0;
           end if;

             if f_nfe_item.pDif  is null
           then
              f_nfe_item.pDif := 0;
           end if;

           if f_nfe_item.vICMSOp is null
           then
              f_nfe_item.vICMSOp := 0;
           end if;

           if f_nfe_item.vICMSDif is null
           then
              f_nfe_item.vICMSDif := 0;
           end if;

           if consumidor_nat
           then
              f_nfe_item.perc_icms_partilha := null;
           end if;

           if not v_difal
           then
              v_val_fcp_uf_ncm  := f_nfe_item.val_fcp_uf_dest;
              v_perc_fcp_uf_ncm := f_nfe_item.perc_fcp_uf_dest;
              v_bc_fcp          := f_nfe_item.base_calc_icm;

              v_bc_fcp_uf_dest            := 0;
              v_fcp_uf_dest               := 0;
              v_bc_uf_dest                := 0;
              p_fcp_uf_dest               := 0;
           else
              v_bc_fcp_uf_dest  := f_nfe_item.base_calc_icm;
              v_fcp_uf_dest     := f_nfe_item.val_fcp_uf_dest;
              v_bc_uf_dest      := f_nfe_item.base_calc_icm;
              p_fcp_uf_dest     := f_nfe_item.perc_fcp_uf_dest;
              v_val_fcp_uf_ncm  := 0;
              v_perc_fcp_uf_ncm := 0;
              v_bc_fcp          := 0;
           end if;

                /*ENTRADA*/
             BEGIN
               insert into obrf_157
               (produto_id         /*1*/
               ,icms_orig          /*2*/
               ,icms_cst           /*3*/
               ,icms_modbc         /*4*/
               ,icms_predbc        /*5*/
               ,icms_vbc           /*6*/
               ,icms_picms         /*7*/
               ,icms_vicms         /*8*/
               ,icms_modbcst       /*9*/
               ,icms_pmvast        /*10*/
               ,icms_predbcst      /*11*/
               ,icms_vbcst         /*12*/
               ,icms_picmsst       /*13*/
               ,icms_vicmsst       /*14*/
               ,ipi_cienq          /*15*/
               ,ipi_cnpjprod       /*16*/
               ,ipi_cselo          /*17*/
               ,ipi_qselo          /*18*/
               ,ipi_cenq           /*19*/
               ,ipi_cst            /*20*/
               ,ipi_vbc            /*21*/
               ,ipi_qunid          /*22*/
               ,ipi_vunid          /*23*/
               ,ipi_pipi           /*24*/
               ,ipi_vipi           /*25*/
               ,ii_vbc             /*26*/
               ,ii_vdespadu        /*27*/
               ,ii_vii             /*28*/
               ,ii_viof            /*29*/
               ,pis_cst            /*30*/
               ,pis_vbc            /*31*/
               ,pis_ppis           /*32*/
               ,pis_vpis           /*33*/
               ,pis_qbcprod        /*34*/
               ,pis_valiqprod      /*35*/
               ,pisst_vbc          /*36*/
               ,pisst_ppis         /*37*/
               ,pisst_qbcprod      /*38*/
               ,pisst_valiqprod    /*39*/
               ,pisst_vpis         /*40*/
               ,cofins_cst         /*41*/
               ,cofins_vbc         /*42*/
               ,cofins_pcofins     /*43*/
               ,cofins_vcofins     /*44*/
               ,cofins_qbcprod     /*45*/
               ,cofins_valiqprod   /*46*/
               ,cofinsst_vbc       /*47*/
               ,cofinsst_pcofins   /*48*/
               ,cofinsst_qbcprod   /*49*/
               ,cofinsst_valiqprod /*50*/
               ,cofinsst_vcofins   /*51*/
               ,issqn_vbc          /*52*/
               ,issqn_valiq        /*53*/
               ,issqn_vissqn       /*54*/
               ,issqn_cod_mun      /*55*/
               ,issqn_clistserv    /*56*/
               ,cod_empresa        /*57*/
               ,cod_usuario        /*58*/
               ,cod_csosn          /*59*/
               ,v_icms_deson       /*60*/
               ,vbcufdest          /*61*/
               ,pfcpufdest         /*62*/
               ,picmsufdest        /*63*/
               ,picmsinter         /*64*/
               ,picmsinterpart     /*65*/
               ,vfcpufdest         /*66*/
               ,vicmsufdest        /*67*/
               ,vicmsufremet       /*68*/
               ,pdif51             /*69*/
               ,vicmsop51          /*70*/
               ,vicmsdif51         /*71*/
               ,vbcfcpufdest       /*72*/
               ,vfcpst             /*73*/
               ,vfcp               /*74*/
               ,vfcpstret          /*75*/
               ,pfcp               /*76*/
               ,vbcfcp             /*77*/
               )
               values
               (v_obrf_152_id                            /*1*/
               ,f_nfe_item.procedencia /*2*/
               ,f_nfe_item.cod_vlfiscal_icm                  /*3*/
               ,'3'                                         /*4*/
               ,decode(f_nfe_item.pDif,100.00,0,0,decode(f_nfe_item.perc_reducao_icm,0.00,null,f_nfe_item.perc_reducao_icm),null) /*5*/
               ,decode(f_nfe_item.pDif,100.00,0, f_nfe_item.base_calc_icm)                     /*6*/
               ,decode(f_nfe_item.pDif,100.00,0,f_nfe_item.percentual_icm)      /*7*/
               ,decode(f_nfe_item.pDif,100.00,0, f_nfe_item.valor_icms)              /*8*/
               ,0                                            /*9*/
               ,null                                         /*10*/
               ,null                                         /*11*/
               ,f_nfe_item.base_icms_subs                    /*12*/
               ,f_nfe_item.perc_substituica                  /*13*/
               ,f_nfe_item.valor_icms_subs                   /*14*/
               ,null                                         /*15*/
               ,null                                         /*16*/
               ,null                                         /*17*/
               ,null                                         /*18*/
               ,w_cod_enq_ipi                                /*19*/
               ,f_nfe_item.cvf_ipi_entrada                   /*20*/
               ,f_nfe_item.base_ipi                          /*21*/
               ,null                                         /*22*/
               ,null                                         /*23*/
               ,f_nfe_item.percentual_ipi                    /*24*/
               ,f_nfe_item.valor_ipi                         /*25*/
               ,decode(v_tem_imp_ii,'N',null,f_nfe_item.base_calculo_ii)                  /*26*/
               ,decode(v_tem_imp_ii,'N',null,f_nfe_item.valor_desp_adu)                    /*27*/
               ,decode(v_tem_imp_ii,'N',null,f_nfe_item.valor_ii)                                /*28*/
               ,decode(v_tem_imp_ii,'N',null,f_nfe_item.valor_iof)                              /*29*/
               ,f_nfe_item.cvf_pis                           /*30*/
               ,f_nfe_item.base_pis_cofins                   /*31*/
               ,f_nfe_item.perc_pis                          /*32*/
               ,f_nfe_item.valor_pis                         /*33*/
               ,null                        /*34*/
               ,null                         /*35*/
               ,null                   /*36*/
               ,null                          /*37*/
               ,null                                         /*38*/
               ,null                                         /*39*/
               ,null                         /*40*/
               ,f_nfe_item.cvf_cofins                        /*41*/
               ,f_nfe_item.base_pis_cofins                   /*42*/
               ,f_nfe_item.perc_cofins                       /*43*/
               ,f_nfe_item.valor_cofins                      /*44*/
               ,null                      /*45*/
               ,null                      /*46*/
               ,null                   /*47*/
               ,null                       /*48*/
               ,null                                         /*49*/
               ,null                                         /*50*/
               ,null                      /*51*/
               ,null                                         /*52*/
               ,null                                         /*53*/
               ,null                                         /*54*/
               ,null                                         /*55*/
               ,null                                         /*56*/
               ,p_cod_empresa                                /*57*/
               ,p_cod_id                                     /*58*/
               ,f_nfe_item.cod_csosn                         /*59*/
               ,null                                         /*60*/
               ,decode(f_nfe_item.valor_icms,0,null,v_bc_uf_dest)       /*61*/
               ,decode(f_nfe_item.valor_icms,0,null,p_fcp_uf_dest)    /*62*/
               ,decode(f_nfe_item.valor_icms,0,null,f_nfe_item.perc_icms_uf_dest)   /*63*/
               ,decode(f_nfe_item.valor_icms,0,null,f_nfe_item.percentual_icm)      /*64*/
               ,decode(f_nfe_item.valor_icms,0,null,f_nfe_item.perc_icms_partilha)  /*65*/
               ,decode(f_nfe_item.valor_icms,0,null,v_fcp_uf_dest)                  /*66*/
               ,decode(f_nfe_item.valor_icms,0,null,f_nfe_item.val_icms_uf_dest)    /*67*/
               ,decode(f_nfe_item.valor_icms,0,null,f_nfe_item.val_icms_uf_remet)   /*68*/
               ,decode(f_nfe_item.pDif,100.00,null,0.00,null,f_nfe_item.pDif)            /*69 CST51*/
               ,decode(f_nfe_item.pDif,100.00,null,0.00,null,f_nfe_item.vICMSOp)         /*70*/
               ,decode(f_nfe_item.pDif,100.00,null,0.00,null,f_nfe_item.vICMSDif)        /*71*/

               ,decode(f_nfe_item.valor_icms,0,null,v_bc_fcp_uf_dest)                         /*72 vbcfcpufdest*/

               ,null                                                                     /*73 vfcpst */
               ,decode(v_perc_fcp_uf_ncm,0,null,v_val_fcp_uf_ncm)                        /*74 vfcp */
               ,null                                                                     /*75 vfcpstret*/
               ,decode(v_perc_fcp_uf_ncm,0,null,v_perc_fcp_uf_ncm)                       /*76 pfcp */
               ,decode(v_perc_fcp_uf_ncm,0,null,v_bc_fcp)                                /*77 vbcfcp */
               );
            EXCEPTION
               WHEN OTHERS THEN
                  p_msg_erro := 'N?o inseriu dados na tabela obrf_157' || Chr(10) || SQLERRM;

            END;

          EXCEPTION
             WHEN OTHERS THEN
                p_msg_erro := 'N?o inseriu dados na tabela obrf_152' || Chr(10) || SQLERRM;

          END;

       END LOOP;

    END LOOP;
  end if;

  if p_str_entr_said = 'S'
  then

    for f_nfsstat in c_NFsstat
    LOOP
      BEGIN
        insert into obrf_150
        (cod_empresa
        ,cod_usuario
        ,numero
        ,serie
        ,tipo_ambiente
        ,chave_acesso
        ,tipo_documento
        ,nr_recibo
        ,versao_systextilnfe)
        values
        (p_cod_empresa
        ,p_cod_id
        ,f_nfsstat.num_nota_fiscal
        ,f_nfsstat.serie_nota_fisc
        ,f_nfsstat.Ambiente
        ,f_nfsstat.Numero_Danf_Nfe
        ,f_nfsstat.tipo_documento
        ,f_nfsstat.nr_recibo
        ,f_nfsstat.versao_systextilnfe);
      EXCEPTION
        WHEN OTHERS THEN
          p_msg_erro := 'N?o inseriu dados na tabela obrf_150, consulta Status nota' || Chr(10) || SQLERRM;
      END;
    END LOOP;

    for f_nfsI in c_NFsI
    LOOP
      BEGIN
        insert into obrf_150
        (cod_empresa
        ,cod_usuario
        ,tipo_ambiente
        ,emit_codigo_uf
        ,ano_inutilizacao
        ,emit_cnpj9
        ,emit_cnpj4
        ,emit_cnpj2
        ,modelo_documento
        ,serie
        ,numero
        ,nr_final_inut
        ,justificativa
        ,tipo_documento
        ,tipo_emissao
        ,versao_systextilnfe
        ,dest_cnpj9
        ,dest_cnpj4
        ,dest_cnpj2)
        values
        (p_cod_empresa
        ,p_cod_id
        ,f_nfsI.Ambiente
        ,f_nfsI.Cod_Cidade
        ,f_nfsI.Ano
        ,f_nfsI.Cgc_9
        ,f_nfsI.Cgc_4
        ,f_nfsI.Cgc_2
        ,55
        ,f_nfsI.Serie_Nota_Fisc
        ,f_nfsI.Num_Nota_Fiscal
        ,f_nfsI.Documento_Inut
        ,f_nfsI.Justificativa
        ,3
        ,1
        ,f_nfsI.Versao_Systextilnfe
        ,f_nfsI.Cgc_Dest_9
        ,f_nfsI.Cgc_Dest_4
        ,f_nfsI.Cgc_Dest_2);
      EXCEPTION
         WHEN OTHERS THEN
            p_msg_erro := 'N?o inseriu dados na tabela obrf_150' || Chr(10) || SQLERRM;

      END;
    END LOOP;

    -- NOTAS DE SAIDAS
    for f_nfe in c_NFs
    LOOP
       BEGIN

          if (v_quebra_niv = 1 and v_imp_tam_danfe = 1)
          then

             v_cont_pcas := 0;
             begin
                select count(fatu_060.nivel_estrutura)
                into v_cont_pcas
                from fatu_060
                where fatu_060.ch_it_nf_cd_empr  = p_cod_empresa
                  and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                  and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc
                  and fatu_060.nivel_estrutura   = '1';
             exception
                 when no_data_found then
                    v_cont_pcas := 0;
             end;

             if v_cont_pcas > 0
             then

                begin
                   delete from obrf_161
                   where obrf_161.codigo_empresa = p_cod_empresa
                     and obrf_161.num_nota_fiscal = f_nfe.num_nota_fiscal
                     and obrf_161.serie_nota_fisc = f_nfe.serie_nota_fisc;
                exception
                    when others then
                       p_msg_erro := 'N?o removeu dados da tabela obrf_161' || Chr(10) || SQLERRM;
                end;

                for f_nfe_item_tam in (
                   select    (fatu_060.nivel_estrutura ||'.'|| fatu_060.grupo_estrutura) as grupo,
                              max(substr(inter_fn_dados_produto_nf(0,4,fatu_060.ch_it_nf_cd_empr,fatu_060.ch_it_nf_num_nfis,fatu_060.ch_it_nf_ser_nfis,fatu_060.nivel_estrutura,fatu_060.grupo_estrutura,fatu_060.subgru_estrutura,fatu_060.item_estrutura,fatu_060.descricao_item || '#$' || nvl(fatu_060.observacao,' ')),1,400)) as descricao,
                              to_number(trim(to_char(fatu_060.procedencia, '0')) || trim(to_char(fatu_060.cvf_icms, '00'))) as CST,
                              replace(pedi_080.cod_natureza || substr(to_char(pedi_080.divisao_natur,'00'),3,3),'.','') as CFOP,
                              fatu_060.classific_fiscal as classificacao,
                              fatu_060.unidade_medida as unidade,
                              fatu_060.valor_unitario as valor_unitario,
                              decode(fatu_060.um_faturamento_um, null, ' ', fatu_060.um_faturamento_um) as um_faturamento
                   from fatu_060, pedi_080
                   where fatu_060.ch_it_nf_cd_empr   = p_cod_empresa
                     and fatu_060.ch_it_nf_num_nfis  = f_nfe.num_nota_fiscal
                     and fatu_060.ch_it_nf_ser_nfis  = f_nfe.serie_nota_fisc
                     and pedi_080.natur_operacao     = fatu_060.natopeno_nat_oper
                     and pedi_080.estado_natoper     = fatu_060.natopeno_est_oper
                     and fatu_060.nivel_estrutura    = '1'
                   group by fatu_060.ch_it_nf_cd_empr,
                            fatu_060.ch_it_nf_num_nfis,
                            fatu_060.ch_it_nf_ser_nfis,
                            (fatu_060.nivel_estrutura ||'.'|| fatu_060.grupo_estrutura),
                            substr(inter_fn_dados_produto_nf(0,4,fatu_060.ch_it_nf_cd_empr,fatu_060.ch_it_nf_num_nfis,fatu_060.ch_it_nf_ser_nfis,fatu_060.nivel_estrutura,fatu_060.grupo_estrutura,fatu_060.subgru_estrutura,fatu_060.item_estrutura,fatu_060.descricao_item || '#$' || nvl(fatu_060.observacao,' ')),1,400),
                            fatu_060.classific_fiscal,
                            to_number(trim(to_char(fatu_060.procedencia, '0')) || trim(to_char(fatu_060.cvf_icms, '00'))),
                            replace(pedi_080.cod_natureza || substr(to_char(pedi_080.divisao_natur,'00'),3,3),'.',''),
                            fatu_060.unidade_medida,
                            fatu_060.valor_unitario,
                            decode(fatu_060.um_faturamento_um, null, ' ', fatu_060.um_faturamento_um))
                LOOP

                   v_tam_conca := ' ';
                   v_tam_aux := 0;

                   for f_nfe_item_tam_grava in (
                      select trim(basi_220.descr_tamanho) as subgrupo
                            ,basi_220.ordem_tamanho as ordem
                      from fatu_060, pedi_080, basi_220
                      where fatu_060.ch_it_nf_cd_empr    = p_cod_empresa
                        and fatu_060.ch_it_nf_num_nfis   = f_nfe.num_nota_fiscal
                        and fatu_060.ch_it_nf_ser_nfis   = f_nfe.serie_nota_fisc
                        and pedi_080.natur_operacao      = fatu_060.natopeno_nat_oper
                        and pedi_080.estado_natoper      = fatu_060.natopeno_est_oper
                        and f_nfe_item_tam.grupo         = (fatu_060.nivel_estrutura ||'.'|| fatu_060.grupo_estrutura)
                        and f_nfe_item_tam.cst           = to_number(trim(to_char(fatu_060.procedencia, '0')) || trim(to_char(fatu_060.cvf_icms, '00')))
                        and f_nfe_item_tam.cfop          = replace(pedi_080.cod_natureza || substr(to_char(pedi_080.divisao_natur,'00'),3,3),'.','')
                        and f_nfe_item_tam.descricao     = substr(inter_fn_dados_produto_nf(0,4,fatu_060.ch_it_nf_cd_empr,fatu_060.ch_it_nf_num_nfis,fatu_060.ch_it_nf_ser_nfis,fatu_060.nivel_estrutura,fatu_060.grupo_estrutura,fatu_060.subgru_estrutura,fatu_060.item_estrutura,fatu_060.descricao_item || '#$' || nvl(fatu_060.observacao,' ')),1,200)
                        and f_nfe_item_tam.cst           = to_number(trim(to_char(fatu_060.procedencia, '0')) || trim(to_char(fatu_060.cvf_icms, '00')))
                        and f_nfe_item_tam.cfop          = replace(pedi_080.cod_natureza || substr(to_char(pedi_080.divisao_natur,'00'),3,3),'.','')
                        and fatu_060.classific_fiscal    = f_nfe_item_tam.classificacao
                        and fatu_060.unidade_medida      = f_nfe_item_tam.unidade
                        and fatu_060.valor_unitario      = f_nfe_item_tam.valor_unitario
                        and (fatu_060.um_faturamento_um  = f_nfe_item_tam.um_faturamento or trim(f_nfe_item_tam.um_faturamento) is null)
                        and fatu_060.nivel_estrutura     = '1'
                        and basi_220.tamanho_ref         = fatu_060.subgru_estrutura
                      order by basi_220.ordem_tamanho)
                   LOOP

                      if f_nfe_item_tam_grava.ordem <> v_tam_aux
                      then
                         if v_tam_conca = ' '
                         then
                            v_tam_conca := ' - ' || trim(f_nfe_item_tam_grava.subgrupo);
                         else
                            v_tam_conca := v_tam_conca || ' - ' || trim(f_nfe_item_tam_grava.subgrupo);
                         end if;
                         v_tam_aux := f_nfe_item_tam_grava.ordem;
                      end if;

                   END LOOP;

                   begin
                      insert into obrf_161
                      (obrf_161.codigo_empresa
                      ,obrf_161.num_nota_fiscal
                      ,obrf_161.serie_nota_fisc
                      ,obrf_161.nivel_grupo
                      ,obrf_161.descricao_geral
                      ,obrf_161.cst
                      ,obrf_161.cfop
                      ,obrf_161.classific_fiscal
                      ,obrf_161.unidade_medida
                      ,obrf_161.valor_unitario
                      ,obrf_161.um_faturamento_um
                      ,obrf_161.tamanhos)
                      values
                      (p_cod_empresa
                      ,f_nfe.num_nota_fiscal
                      ,f_nfe.serie_nota_fisc
                      ,replace(f_nfe_item_tam.grupo, '.', '')
                      ,f_nfe_item_tam.descricao
                      ,f_nfe_item_tam.cst
                      ,f_nfe_item_tam.cfop
                      ,f_nfe_item_tam.classificacao
                      ,f_nfe_item_tam.unidade
                      ,f_nfe_item_tam.valor_unitario
                      ,decode(f_nfe_item_tam.um_faturamento, null, ' ', f_nfe_item_tam.um_faturamento)
                      ,v_tam_conca);
                   exception
                       when others then
                          p_msg_erro := 'N?o inseriu dados na tabela obrf_161' || Chr(10) || SQLERRM;
                   end;
                   commit;

                END LOOP;
             end if;
          end if;

          --EMPRESA DO SIMPLES
          if v_ind_empr_simples = '1' and v_versao_layout_nfe = '1.10'
          then
             f_nfe.base_icms := 0.00;
             f_nfe.valor_icms := 0.00;
          end if;

          --PEGA OS VALORES PARA RATEIO NA CAPA DA NOTA
          v_valor_seguro := 0.00;
          v_valor_frete  := 0.00;
          v_valor_outros := 0.00;
          v_valor_desc   := 0.00;
          v_valor_mercadoria := 0.00;
          begin
            select  fatu_050.valor_seguro,                                fatu_050.valor_frete_nfis,
                   (fatu_050.valor_despesas + fatu_050.valor_encar_nota), (fatu_050.valor_desc_nota + fatu_050.vlr_desc_especial + fatu_050.valor_suframa),
                    fatu_050.valor_itens_nfis,
                    (fatu_050.valor_desc_nota + fatu_050.vlr_desc_especial),

                    ((fatu_050.valor_itens_nfis +
                    fatu_050.valor_seguro     +
                    fatu_050.valor_frete_nfis +
                    fatu_050.valor_despesas   -
                    fatu_050.valor_encar_nota -
                    fatu_050.valor_desc_nota  -
                    fatu_050.vlr_desc_especial) * v_perc_suframa) /100.00 valor_suframa

            into    v_valor_seguro,                                       v_valor_frete,
                    v_valor_outros,                                       v_valor_desc,
                    v_valor_mercadoria,
                    v_valor_desc_sem_suframa,
                    v_suframa
            from fatu_050
            where fatu_050.codigo_empresa  = p_cod_empresa
              and fatu_050.num_nota_fiscal = f_nfe.num_nota_fiscal
              and fatu_050.serie_nota_fisc = f_nfe.serie_nota_fisc;
          exception
              when no_data_found then
                 v_valor_seguro := 0.00;
                 v_valor_frete  := 0.00;
                 v_valor_outros := 0.00;
                 v_valor_desc   := 0.00;
                 v_valor_mercadoria := 0.00;
                 v_suframa := 0.00;
                 v_valor_desc_sem_suframa := 0.00;
          end;

          if f_nfe.valor_suframa = 0
          then
             v_suframa :=0;
          end if;

          --SE TIVER AO MENOS UM VALOR DE SEGURO, FRETE, OUTROS OU DESCONTO EFETUA O LOOP DO RATEIO
          if ((v_valor_seguro > 0.00 or v_valor_frete > 0.00 or v_valor_outros > 0.00 or v_valor_desc > 0.00) and v_valor_mercadoria > 0.00) or ((f_nfe.tipo_nf_referenciada in (2,3)) and v_valor_mercadoria = 0.00)
          then

            BEGIN
               update fatu_060
               set fatu_060.valor_seguro   = 0.00,
                   fatu_060.valor_frete    = 0.00,
                   fatu_060.valor_outros   = 0.00,
                   fatu_060.valor_desc     = 0.00--,
                --   fatu_060.valor_suframa_icms = 0.00,
                --   fatu_060.base_calc_icms_suframa = 0.00
                where fatu_060.ch_it_nf_cd_empr  = f_nfe.cod_empresa
                 and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                 and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc;
               commit;
            EXCEPTION
                WHEN OTHERS THEN
                   p_msg_erro := 'N?o atualizou tabela fatu_060.' || Chr(10) || SQLERRM;
            END;

            if((f_nfe.tipo_nf_referenciada in (2,3)) and v_valor_mercadoria = 0.00) then
              begin
                select count(*)
                  into v_qtde_itens
                  from fatu_060
                 where fatu_060.ch_it_nf_cd_empr  = f_nfe.cod_empresa
                   and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                   and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc;
              exception
                when no_data_found then
                  v_qtde_itens := 0.00;
              end;
            end if;

            for f_nfe_item_rat in (
              select fatu_060.seq_item_nfisc, fatu_060.valor_faturado,
                     fatu_060.natopeno_nat_oper,       fatu_060.natopeno_est_oper
              from fatu_060
              where fatu_060.ch_it_nf_cd_empr  = f_nfe.cod_empresa
                and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc
              order by fatu_060.seq_item_nfisc)
            LOOP

              if((f_nfe.tipo_nf_referenciada in (2,3)) and v_valor_mercadoria = 0.00) then
                BEGIN
                  update fatu_060
                  set fatu_060.valor_seguro = trunc((v_valor_seguro / v_qtde_itens),2),
                      fatu_060.valor_frete  = trunc((v_valor_frete  / v_qtde_itens),2),
                      fatu_060.valor_outros = trunc((v_valor_outros / v_qtde_itens),2),
                      fatu_060.valor_desc   = trunc((v_valor_desc   / v_qtde_itens),2)
                   where fatu_060.ch_it_nf_cd_empr = f_nfe.cod_empresa
                    and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                    and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc
                    and fatu_060.seq_item_nfisc    = f_nfe_item_rat.seq_item_nfisc;
                  commit;
                EXCEPTION
                  WHEN OTHERS THEN
                    p_msg_erro := 'N?o atualizou tabela fatu_060.' || Chr(10) || SQLERRM;
                END;
              else
                 BEGIN
                  update fatu_060
                  set fatu_060.valor_seguro = trunc(((f_nfe_item_rat.valor_faturado / v_valor_mercadoria) * v_valor_seguro),2),
                      fatu_060.valor_frete  = trunc(((f_nfe_item_rat.valor_faturado / v_valor_mercadoria) * v_valor_frete),2),
                      fatu_060.valor_outros = trunc(((f_nfe_item_rat.valor_faturado / v_valor_mercadoria) * v_valor_outros),2),
                      fatu_060.valor_desc   = trunc(((f_nfe_item_rat.valor_faturado / v_valor_mercadoria) * v_valor_desc_sem_suframa),2)
                   where fatu_060.ch_it_nf_cd_empr = f_nfe.cod_empresa
                    and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                    and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc
                    and fatu_060.seq_item_nfisc    = f_nfe_item_rat.seq_item_nfisc;
                EXCEPTION
                  WHEN OTHERS THEN
                    p_msg_erro := 'N?o atualizou tabela fatu_060.' || Chr(10) || SQLERRM;
                END;

                begin
                    select pedi_080.considera_suframa
                    into v_considera_suframa
                    from pedi_080
                    where pedi_080.natur_operacao    = f_nfe_item_rat.natopeno_nat_oper
                      and pedi_080.estado_natoper    = f_nfe_item_rat.natopeno_est_oper;
                exception
                when no_data_found then
                    v_considera_suframa := 'N';
                end;

                /*if f_nfe.valor_suframa > 0 and v_considera_suframa = 'S'
                then
                    BEGIN
                      update fatu_060
                      set fatu_060.valor_suframa_icms = round(((((fatu_060.valor_faturado + fatu_060.valor_seguro +
                                                                 fatu_060.valor_frete +    fatu_060.valor_outros - fatu_060.valor_desc)) * v_perc_suframa) / 100),2),
                          fatu_060.base_calc_icms_suframa = (fatu_060.valor_faturado + fatu_060.valor_seguro + fatu_060.valor_frete +    fatu_060.valor_outros - fatu_060.valor_desc)
                       where fatu_060.ch_it_nf_cd_empr = f_nfe.cod_empresa
                        and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                        and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc
                        and fatu_060.seq_item_nfisc    = f_nfe_item_rat.seq_item_nfisc
                        and fatu_060.cvf_icms in (40,41);

                    EXCEPTION
                      WHEN OTHERS THEN
                        p_msg_erro := 'N?o atualizou tabela fatu_060.' || Chr(10) || SQLERRM;
                    END;

                    BEGIN
                      update fatu_060
                      set fatu_060.valor_desc   = trunc(((f_nfe_item_rat.valor_faturado / v_valor_mercadoria) * v_valor_desc),2)
                       where fatu_060.ch_it_nf_cd_empr = f_nfe.cod_empresa
                        and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                        and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc
                        and fatu_060.seq_item_nfisc    = f_nfe_item_rat.seq_item_nfisc;
                    EXCEPTION
                      WHEN OTHERS THEN
                        p_msg_erro := 'N?o atualizou tabela fatu_060.' || Chr(10) || SQLERRM;
                    END;
                 end if;*/
              end if;

            END LOOP;

            begin
               select sum(fatu_060.valor_frete),  sum(fatu_060.valor_seguro),
                      sum(fatu_060.valor_outros), sum(fatu_060.valor_desc),
                      sum(fatu_060.valor_suframa_icms)
               into   v_valor_frete_aux,          v_valor_seguro_aux,
                      v_valor_outros_aux,         v_valor_desc_aux,
                      v_valor_suframa_icms_aux
               from fatu_060
               where fatu_060.ch_it_nf_cd_empr  = f_nfe.cod_empresa
                 and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                 and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc;
            exception
                when no_data_found then
                   v_valor_frete_aux := 0.00;
                   v_valor_seguro_aux := 0.00;
                   v_valor_outros_aux := 0.00;
                   v_valor_desc_aux := 0.00;
                   v_valor_suframa_icms_aux := 0.00;
            end;

            if (v_valor_frete_aux <> v_valor_frete or v_valor_seguro_aux <> v_valor_seguro or v_valor_outros_aux <> v_valor_outros or v_valor_desc_aux <> v_valor_desc or v_valor_suframa_icms_aux <> v_suframa)
            then
               begin
                  select fatu_060.seq_item_nfisc
                  into   v_num_item_max
                  from fatu_060, pedi_080
                  where fatu_060.ch_it_nf_cd_empr = f_nfe.cod_empresa
                    and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                    and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc
                    and pedi_080.natur_operacao    = fatu_060.natopeno_nat_oper
                    and pedi_080.estado_natoper    = fatu_060.natopeno_est_oper
                    and pedi_080.considera_suframa = 'S'
                    and fatu_060.valor_faturado in (select max(f_060.valor_faturado)
                                                    from fatu_060 f_060, pedi_080 p_080
                                                    where f_060.ch_it_nf_cd_empr = fatu_060.ch_it_nf_cd_empr
                                                      and f_060.ch_it_nf_num_nfis = fatu_060.ch_it_nf_num_nfis
                                                      and f_060.ch_it_nf_ser_nfis = fatu_060.ch_it_nf_ser_nfis
                  and p_080.natur_operacao    = f_060.natopeno_nat_oper
                                                      and p_080.estado_natoper    = f_060.natopeno_est_oper
                                                      and p_080.considera_suframa = 'S')
                    and rownum = 1;
               exception
                   when no_data_found then
                      v_num_item_max := 1;
               end;

               BEGIN

                  v_valor_frete_aux := (v_valor_frete - v_valor_frete_aux);
                  v_valor_seguro_aux := (v_valor_seguro - v_valor_seguro_aux);
                  v_valor_outros_aux := (v_valor_outros - v_valor_outros_aux);
                  v_valor_desc_aux := (v_valor_desc - v_valor_desc_aux);

                  if f_nfe.valor_suframa > 0
                  then
                     v_valor_suframa_icms_aux := (v_suframa - v_valor_suframa_icms_aux);
                  end if;

                  update fatu_060
                  set fatu_060.valor_seguro   = fatu_060.valor_seguro + v_valor_seguro_aux,
                      fatu_060.valor_frete    = fatu_060.valor_frete + v_valor_frete_aux,
                      fatu_060.valor_outros   = fatu_060.valor_outros + v_valor_outros_aux,
                      fatu_060.valor_desc     = fatu_060.valor_desc + v_valor_desc_aux
                      --fatu_060.valor_suframa_icms    = fatu_060.valor_suframa_icms + v_valor_suframa_icms_aux
                   where fatu_060.ch_it_nf_cd_empr  = f_nfe.cod_empresa
                    and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                    and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc
                    and fatu_060.seq_item_nfisc    = v_num_item_max;
                  commit;
               EXCEPTION
                   WHEN OTHERS THEN
                      p_msg_erro := 'N?o atualizou tabela fatu_060.' || Chr(10) || SQLERRM;
               END;
            end if;

          end if;

          /*
          SE A EMPRESA FOR DO PARANA, VERIFICA SE A NF-e GERADA EST?:1?:2 EM MANUTEN?:3?:4:5?:6?:7O,
          SE ESTIVER, VAI MANDAR A INSCRI?:8?:9:10?:11?:12O ESTADUAL EM BRACO, DEVIDO A UM PROBLEMA
          NO SISTEMA DA RECEITA FEDERAL. SS: 57762.
          */
          v_nfe_manut := 0;
          if f_nfe.cidade_dest_estado = 'PR'
          then
             if f_nfe.cidade_nfe_estado = 'GO'
             then
                begin
                   select count(*)
                   into v_nfe_manut
                   from obrf_150_log
                   where obrf_150_log.cod_empresa_new = p_cod_empresa
                     and obrf_150_log.numero_new = f_nfe.num_nota_fiscal
                     and obrf_150_log.serie_new  = f_nfe.serie_nota_fisc
                     and rownum = 1;
                exception
                   when no_data_found then
                   v_nfe_manut := 0;
                end;
             end if;
          end if;

          if v_tipo_contingencia <> 3
          then
             if f_nfe.nfe_contigencia = 0
             then
               v_tipo_contingencia := 1;
             end if;
          end if;

          begin
             select nvl(sum(fatu_060.valor_pis),0.00), nvl(sum(fatu_060.valor_cofins),0.00),
                    nvl(sum(decode(fatu_060.cvf_ipi_saida,50,fatu_060.valor_ipi,0)),0),
                    nvl(sum(decode(fatu_060.cvf_ipi_saida,99,fatu_060.valor_ipi,0)),0)
             into   v_valor_pis,             v_valor_cofins,
                    v_ipi_tributado,         v_ipiDevolucao
             from fatu_060
             where fatu_060.ch_it_nf_cd_empr  = f_nfe.cod_empresa
               and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
               and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc;
          exception
            when access_into_null then
              v_valor_pis    := 0.00;
              v_valor_cofins := 0.00;
              v_ipi_tributado:= 0.00;
              v_ipiDevolucao := 0.00;
          end;


          begin
            select obrf_158.concatena_cfop,   obrf_158.destaca_ipi
            into v_Concatena_Cfop,            v_destaca_ipi
            from obrf_158
            where obrf_158.cod_empresa = p_cod_empresa;
          exception
            when no_data_found then
              v_Concatena_Cfop := 'N';
              v_destaca_ipi := 0;
          end;

          v_cont := 0;
          if v_Concatena_Cfop = 'S'
          then
             v_cfop := '';
             BEGIN

               for f_nfe_item_c in (
                 select fatu_060.natopeno_nat_oper, fatu_060.natopeno_est_oper from fatu_060
                 where fatu_060.ch_it_nf_cd_empr  = f_nfe.cod_empresa
                   and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                   and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc
                 group by fatu_060.natopeno_nat_oper, fatu_060.natopeno_est_oper)
               LOOP

                 v_cont := v_cont + 1;
                 select substr(replace(p_080.cod_natureza || substr(to_char(p_080.divisao_natur,'00'),3,1),'.','') || ' ' ||p_080.descr_nat_oper,1,60)
                   into v_cfopn
                 from pedi_080 p_080
                 where p_080.natur_operacao = f_nfe_item_c.natopeno_nat_oper
                   and p_080.estado_natoper = f_nfe_item_c.natopeno_est_oper;

                 if v_cont = 1
                 then
                   v_cfop := v_cfopn;
                 else
                   v_cfop := substr(v_cfop||' - ' ||v_cfopn,1,60);
                 end if;

               END LOOP;
             EXCEPTION
                WHEN OTHERS THEN
                   p_msg_erro := 'N?o juntou as CFOPs' || Chr(10) || SQLERRM;
             END;


          end if;

          if v_destaca_ipi = 1 and f_nfe.nota_fatura > 0
          then
             f_nfe.valor_despesas := f_nfe.valor_despesas+f_nfe.valor_ipi;
             f_nfe.valor_ipi := 0.00;
          end if;

          if v_ipiDevolucao > 0 and f_nfe.tipo_transacao = 'D'
          then
             f_nfe.valor_despesas := f_nfe.valor_despesas /*+ v_ipiDevolucao*/;
             f_nfe.valor_ipi := v_ipi_tributado;
          end if;

          v_uf_embarque    := null;
          v_local_embarque := null;
          if f_nfe.natop_nf_est_oper = 'EX'
          then
             begin
                select obrf_175.local_uf, obrf_175.embarque_local, obrf_175.x_loc_despacho
                into   v_uf_embarque,     v_local_embarque,       v_x_loc_despacho
                from fatu_050, obrf_175
                where fatu_050.codigo_empresa  = p_cod_empresa
                  and fatu_050.num_nota_fiscal = f_nfe.num_nota_fiscal
                  and fatu_050.serie_nota_fisc = f_nfe.serie_nota_fisc
                  and obrf_175.cod_local       = fatu_050.cod_local;
             exception
                 when no_data_found then
                    v_uf_embarque    := null;
                    v_local_embarque := null;
             end;
          end if;

          select replace(ltrim(translate(f_nfe.inscr_suframa , translate(f_nfe.inscr_suframa, '1234567890', ' ') , ' ')), ' ', '') retorno_num into v_codigo_suframa from dual;

          if v_codigo_suframa is null or v_codigo_suframa = ''
          then
             v_codigo_suframa := f_nfe.inscr_suframa;
          end if;

          v_data_cont_150 := null;
          v_just_cont_150 := null;

          if v_tipo_contingencia = 3
          then
              v_data_cont_150 :=   v_data_cont;
              v_just_cont_150 :=   v_just_cont;
          end if;
          if v_tipo_contingencia = 5
          then
             v_data_cont_150 :=   f_nfe.data_hora_contigencia;
             v_just_cont_150 :=   f_nfe.justificativa_cont;
          end if;

          if f_nfe.natop_nf_est_oper = 'EX'
          then
            v_id_dest := 3;
          else
            if f_nfe.natop_nf_est_oper = f_nfe.cidade_dest_estado
            then
              v_id_dest := 1;
            else
              v_id_dest := 2;
            end if;
          end if;

          v_ind_final   := 0; --VENDA NORMAL
          v_ind_ie_dest := 1;
          v_difal       := false;

          select decode(f_nfe.valor_icms_sub, 0,null,f_nfe.ie_substituto) into v_ie_substituto from dual;

          if f_nfe.insc_est_cliente is null and f_nfe.f_cgc_4 = 0
          then
             v_ind_ie_dest := 2;
             v_ind_final   := 1; -- VENDA DESTINADA A OPERA??O FINAL
             v_ie_substituto := f_nfe.ie_substituto;
             v_difal         := true;
          end if;

          if f_nfe.insc_est_cliente is null and f_nfe.f_cgc_4 > 0 and f_nfe.consumidor_final = 'S'
          then
             v_ind_ie_dest := 9;
             v_ind_final := 1;
             v_ie_substituto := f_nfe.ie_substituto;
             v_difal         := true;
          end if;

          if f_nfe.consumidor_final = 'N' and f_nfe.insc_est_cliente is null and f_nfe.f_cgc_4 > 0
          then
             v_ind_ie_dest := 2;
             v_ind_final   := 0;
          end if;

          if (f_nfe.consumidor_final = 'S') or
             (f_nfe.valor_icms > 0.00 and f_nfe.valor_icms_sub = 0.00 and
             (f_nfe.insc_est_cliente is null and f_nfe.f_cgc_4 = 0) and
             f_nfe.natop_nf_est_oper in ('AM', 'BA', 'CE', 'GO', 'MG', 'MS', 'MT', 'PE', 'RN', 'SE' , 'SP'))
          then
             v_ind_ie_dest := 9;
             v_ind_final   := 1;
             v_ie_substituto := f_nfe.ie_substituto;
             v_difal         := true;
          end if;

          if f_nfe.natop_nf_est_oper = 'EX'
          then
            v_ind_ie_dest := 9;
            v_id_estrangeiro := f_nfe.codigo_cliente;
            v_ind_final   := 0;
          end if;

          /*NF DEVOLU??O DE ENTRADAS*/
          begin
          select estq_005.tipo_transacao into w_trans_devol from estq_005
          where estq_005.codigo_transacao = f_nfe.codigo_transacao;
          exception
              when no_data_found then
              w_trans_devol := 'N';
          end;

          if w_trans_devol = 'D' and (v_versao_layout_nfe = '3.10' or v_versao_layout_nfe = '4.00')
          then
              f_nfe.tipo_nf_referenciada := 4; --4 - Devolu??o de mercadoria
          end if;

          if  v_id_dest = 2
          then
           f_nfe.placa_veiculo := null;
          end if;

          if f_nfe.tipo_nf_referenciada = 1
          then
            v_ind_pres := 9;
          else
            v_ind_pres := 0;
          end if;
          begin
             select sum(fatu_060.valor_suframa_icms)
             into  v_valor_suframa_icms_aux
             from fatu_060
             where fatu_060.ch_it_nf_cd_empr  = f_nfe.cod_empresa
               and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
               and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc;
          exception
              when no_data_found then
                 v_valor_suframa_icms_aux := 0.00;
          end;

          if  v_id_dest = 2
            then
                f_nfe.placa_veiculo := null;
            end if;

          if f_nfe.val_fcp_uf_dest_nf is null
          then
              f_nfe.val_fcp_uf_dest_nf := 0;
          end if;
          if f_nfe.val_icms_uf_dest_nf is null
          then
              f_nfe.val_icms_uf_dest_nf := 0;
          end if;
          if f_nfe.val_icms_uf_remet_nf is null
          then
              f_nfe.val_icms_uf_remet_nf := 0;
          end if;

          if v_difal
          then
             v_val_fcp_uf_ncm_nf := 0;
             v_FCP_UF_Dest_nf    := f_nfe.val_fcp_uf_dest_nf;
          else
             v_val_fcp_uf_ncm_nf := f_nfe.val_fcp_uf_dest_nf;
             v_FCP_UF_Dest_nf    := 0;
          end if;

          if f_nfe.numero_fixo is null
          then
             f_nfe.numero_fixo := 0;
          end if;

          if f_nfe.numero_fixo = 0
          then
            f_nfe.ddd_cidade := null;
            telefone_cliente_dest := trim(to_char(f_nfe.numero_celular, '0000000000'));
          end if;

          if f_nfe.numero_fixo <> 0
          then
             telefone_cliente_dest := substr(substr(trim(to_char(f_nfe.ddd_cidade,'0000')),3,2) || trim(to_char(f_nfe.numero_fixo, '00000000')),0,10);
          end if;

          begin
            select nome_cliente,
      substr(substr(trim(to_char(ddd,'0000')),3,2) || trim(to_char(decode(telefone_cliente, 0,trim(to_char(celular_cliente,'000000000')) ,trim(to_char(telefone_cliente,'00000000'))))), 0, 14) as telefone_cliente,
      nfe_e_mail,
      insc_est_cliente
            into nome_cliente_entr, telefone_cliente_entr,
                email_entr, insc_est_cliente_entr
            from pedi_010, basi_160
            where cgc_9 = f_nfe.cgc_entr_cobr9
            and   cgc_4 = f_nfe.cgc_entr_cobr4
            and   cgc_2 = f_nfe.cgc_entr_cobr2
      and   basi_160.cod_cidade = pedi_010.cod_cidade;
            exception
              when no_data_found then
                nome_cliente_entr     := '';
                telefone_cliente_entr := '';
                email_entr            := '';
                insc_est_cliente_entr := '';
          end;

      if inter_fn_get_nt_em_vigor('NT_2020_006', v_ambiente) = 'S' and v_ind_pres in (2,3,4,9)
      then
      begin
          select pedi_115.tipo_intermediador, pedi_115.cgc9_intermediador,
           pedi_115.cgc4_intermediador, pedi_115.cgc2_intermediador
        into v_intermediador, v_cgc9_intermediador, v_cgc4_intermediador, v_cgc2_intermediador
        from pedi_115
        where pedi_115.origem = (select pedi_100.origem_pedido from pedi_100, fatu_050
                     where fatu_050.codigo_empresa  = f_nfe.cod_empresa
                     and fatu_050.num_nota_fiscal = f_nfe.num_nota_fiscal
                     and fatu_050.serie_nota_fisc = f_nfe.serie_nota_fisc
                     and fatu_050.pedido_venda = pedi_100.pedido_venda);
        exception
        when no_data_found
        then begin
          v_intermediador := 0;
          v_cgc9_intermediador := 0;
          v_cgc4_intermediador := 0;
          v_cgc2_intermediador := 0;
          v_id_cad_int_tran := '';
        end;
        end;

      if v_intermediador = 1
      then
        select supr_010.nome_fornecedor
        into v_id_cad_int_tran
        from supr_010
        where supr_010.fornecedor9 = v_cgc9_intermediador
          and supr_010.fornecedor4 = v_cgc4_intermediador
        and supr_010.fornecedor2 = v_cgc2_intermediador;
      end if;
      else
        v_intermediador := null;
      v_cgc9_intermediador := 0;
      v_cgc4_intermediador := 0;
      v_cgc2_intermediador := 0;
      v_id_cad_int_tran := '';
      end if;

       /* Guarda se a danfe ? simplificada */
       v_danfe_simplificada := inter_fn_danfe_simplificada(f_nfe.cod_empresa,f_nfe.num_nota_fiscal,f_nfe.serie_nota_fisc);

          v_desconto_sem_suframa := f_nfe.valor_desconto; -- DESCONTAR O VALOR DO SUFRAMA

          if v_valor_suframa_icms_aux is not null
          then
             if f_nfe.valor_desconto >= v_valor_suframa_icms_aux
             then
                v_desconto_sem_suframa := f_nfe.valor_desconto - v_valor_suframa_icms_aux;
             end if;
          end if;

          /* SAIDAS*/
          insert into obrf_150
          (chave_acesso         /*1*/
          ,codigo_numerico      /*2*/
          ,digito_verificador   /*3*/
          ,natureza_operacao    /*4*/
          ,forma_pagamento      /*5*/
          ,modelo_documento     /*6*/
          ,serie                /*7*/
          ,numero               /*8*/
          ,data_emissao         /*9*/
          ,data_entrada_saida   /*10*/
          ,tipo_documento       /*11*/
          ,codigo_municipio     /*12*/
          ,tipo_impressao       /*13*/
          ,tipo_emissao         /*14*/
          ,tipo_ambiente        /*15*/
          ,processo_emissao     /*16*/
          ,versao_processo      /*17*/
          ,finalidade           /*18*/
          ,emit_cnpj9           /*19*/
          ,emit_cnpj4           /*20*/
          ,emit_cnpj2           /*21*/
          ,emit_nome            /*22*/
          ,emit_fantasia        /*23*/
          ,emit_logradouro      /*24*/
          ,emit_numero          /*25*/
          ,emit_complemento     /*26*/
          ,emit_bairro          /*27*/
          ,emit_cod_mun         /*28*/
          ,emit_nome_mun        /*29*/
          ,emit_sigla_uf        /*30*/
          ,emit_codigo_uf       /*31*/
          ,emit_cep             /*32*/
          ,emit_telefone        /*33*/
          ,emit_codigo_pais     /*34*/
          ,emit_nome_pais       /*35*/
          ,emit_ie              /*36*/
          ,emit_iest            /*37*/
          ,emit_im              /*38*/
          ,emit_cnae            /*39*/
          ,dest_cnpj9           /*40*/
          ,dest_cnpj4           /*41*/
          ,dest_cnpj2           /*42*/
          ,dest_nome            /*43*/
          ,dest_logradouro      /*44*/
          ,dest_numero          /*45*/
          ,dest_complemento     /*46*/
          ,dest_bairro          /*47*/
          ,dest_cod_mun         /*48*/
          ,dest_nome_mun        /*49*/
          ,dest_sigla_uf        /*50*/
          ,dest_nome_pais       /*51*/
          ,dest_telefone        /*52*/
          ,dest_cep             /*53*/
          ,dest_codigo_pais     /*54*/
          ,dest_ie              /*55*/
          ,dest_isuframa        /*56*/
          ,ret_cnpj9            /*57*/
          ,ret_cnpj4            /*58*/
          ,ret_cnpj2            /*59*/
          ,ret_logradouro       /*60*/
          ,ret_numero           /*61*/
          ,ret_complemento      /*62*/
          ,ret_bairro           /*63*/
          ,ret_codigo_mun       /*64*/
          ,ret_nome_mun         /*65*/
          ,ret_sigla_uf         /*66*/
          ,entr_cnpj9           /*67*/
          ,entr_cnpj4           /*68*/
          ,entr_cnpj2           /*69*/
          ,entr_logradouro      /*70*/
          ,entr_numero          /*71*/
          ,entr_complemento     /*72*/
          ,entr_bairro          /*73*/
          ,entr_codigo_mun      /*74*/
          ,entr_nome_mun        /*75*/
          ,entr_sigla_uf        /*76*/
          ,icmstot_vbc          /*77*/
          ,icmstot_vicms        /*78*/
          ,icmstot_vbcst        /*79*/
          ,icmstot_vst          /*80*/
          ,icmstot_vprod        /*81*/
          ,icmstot_vfrete       /*82*/
          ,icmstot_vseg         /*83*/
          ,icmstot_vdesc        /*84*/
          ,icmstot_vii          /*85*/
          ,icmstot_vipi         /*86*/
          ,icmstot_vpis         /*87*/
          ,icmstot_vcofins      /*88*/
          ,icmstot_voutro       /*89*/
          ,icmstot_vnf          /*90*/
          ,issqntot_vserv       /*91*/
          ,issqntot_vbc         /*92*/
          ,issqntot_viss        /*93*/
          ,issqntot_vpis        /*94*/
          ,issqntot_vcofins     /*95*/
          ,rettrib_vretpis      /*96*/
          ,rettrib_vretcofins   /*97*/
          ,rettrib_vretcsll     /*98*/
          ,rettrib_vbcirrf      /*99*/
          ,rettrib_virrf        /*100*/
          ,rettrib_vbcretprev   /*101*/
          ,rettrib_vretprev     /*102*/
          ,modfrete             /*103*/
          ,transp_cnpj9         /*104*/
          ,transp_cnpj4         /*105*/
          ,transp_cnpj2         /*106*/
          ,transp_nome          /*107*/
          ,transp_ie            /*108*/
          ,transp_endereco      /*109*/
          ,transp_nomemunicipio /*110*/
          ,transp_sigla_uf      /*111*/
          ,transp_vserv         /*112*/
          ,transp_vbcret        /*113*/
          ,transp_picmsret      /*114*/
          ,transp_vicmsret      /*115*/
          ,transp_cfop          /*116*/
          ,transp_cmunfg        /*117*/
          ,transp_placa         /*118*/
          ,transp_placa_uf      /*119*/
          ,transp_rntc          /*120*/
          ,transp_reb_placa     /*121*/
          ,transp_reb_uf        /*122*/
          ,transp_reb_rntc      /*123*/
          ,transp_qvol          /*124*/
          ,transp_esp           /*125*/
          ,transp_marca         /*126*/
          ,transp_nvol          /*127*/
          ,transp_pesol         /*128*/
          ,transp_pesob         /*129*/
          ,nr_recibo            /*130*/
          ,nr_protocolo         /*131*/
          ,justificativa        /*132*/
          ,ano_inutilizacao     /*133*/
          ,nr_final_inut        /*134*/
          ,cod_status           /*135*/
          ,msg_status           /*136*/
          ,cod_empresa      /*137*/
          ,cod_usuario      /*138*/
          ,usuario              /*139*/
          ,dest_e_mail          /*140*/
          ,dest_nfe_e_mail      /*141*/
          ,user_id_e_mail       /*142*/
          ,user_e_mail          /*143*/
          ,user_senha_e_mail    /*144*/
          ,infadic_fisco        /*145*/
          ,infadic_cpl          /*146*/
          ,cod_crt              /*147*/
          ,versao_systextilnfe  /*148*/
          ,uf_embarque          /*149*/
          ,local_embarque       /*150*/
          ,hora_entrada_saida   /*151*/
          ,data_hora_contigencia  /*152*/
          ,justificativa_cont  /*153*/
          ,id_dest        /*154*/
          ,ind_final      /*155*/
          ,ind_pres        /*156*/
          ,id_estrangeiro    /*157*/
          ,ind_ie_dest      /*158*/
          ,v_icms_deson      /*159*/
          ,x_loc_despacho    /*160*/
          ,valor_suframa    /*161*/
          ,vFCPUFDest      /*162*/
          ,vICMSUFDest      /*163*/
          ,vICMSUFRemet      /*164*/
          ,vfcpstret            /*165*/
          ,vfcpst               /*166*/
          ,vipidevol            /*167*/
          ,vfcp                  /*168*/
          ,ind_envia_boleto_email /*169*/
      ,resp_tec_cgc9    /*170*/
      ,resp_tec_cgc4    /*171*/
      ,resp_tec_cgc2    /*172*/
      ,resp_tec_nome    /*173*/
      ,resp_tec_email    /*174*/
      ,resp_tec_telefone  /*175*/
      ,resp_tec_idCSRT    /*176*/
      ,resp_tec_hashCSRT  /*177*/
      ,entr_nome_receb    /*178*/
      ,entr_cod_cep      /*179*/
      ,entr_cod_pais    /*180*/
      ,entr_nome_pais    /*181*/
      ,entr_telefone    /*182*/
      ,entr_email_receb    /*183*/
      ,entr_inscr_estadual_receb  /*184*/
    ,intermediador /*185*/
    ,cgc9_intermediador /*186*/
    ,cgc4_intermediador /*187*/
    ,cgc2_intermediador /*188*/
    ,id_cad_int_tran /*189*/
    ,nfe_contigencia /*190*/
      )
          values
          (f_nfe.numero_danf_nfe         /*1*/
          ,null                            /*2*/
          ,null                            /*3*/
          ,decode(f_nfe.nota_estorno, 0, substr(inter_fn_retira_acentuacao(decode(v_Concatena_Cfop,'S',v_cfop,f_nfe.natop_nf_nat_oper)),1,60),'999 - Estorno de NF-e n?o cancelada no prazo legal')                       /*4*/
          ,f_nfe.avista                       /*5*/
          ,'55'                            /*6*/
          ,f_nfe.serie_nota_fisc                         /*7*/
          ,f_nfe.num_nota_fiscal                     /*8*/
          ,f_nfe.o_data_emissao                  /*9*/
          ,f_nfe.data_saida                  /*10*/
          ,f_nfe.entrada_saida            /*11*/
          ,f_nfe.cod_cidade_ibge      /*12*/
          ,decode(v_danfe_simplificada,0,f_nfe.formato_danfe+1,3)  /*13*/ -- rafaelsalvador
          ,v_tipo_contingencia                               /*14*/
          ,f_nfe.nfe_ambiente                  /*15*/
          ,0                               /*16*/ -- EMITIDO PELO CONTRIBUINTE NO PROPRIO SISTEMA
          ,'5'                             /*17*/ -- Vers?o do Systextil
          ,decode(f_nfe.tipo_nf_referenciada, 9, 1, 8, 1, f_nfe.tipo_nf_referenciada)                               /*18*/ -- FINALIDADE
          ,f_nfe.f_cgc_9                /*19*/
          ,f_nfe.f_cgc_4                /*20*/
          ,f_nfe.f_cgc_2                /*21*/
          ,inter_fn_retira_acentuacao(f_nfe.nome_empresa)            /*22*/
          ,inter_fn_retira_acentuacao(f_nfe.f500_nome_fantasia)             /*23*/
          ,inter_fn_retira_acentuacao(f_nfe.f_endereco)             /*24*/
          ,f_nfe.f_numero              /*25*/
          ,inter_fn_retira_acentuacao(f_nfe.complemento)               /*26*/
          ,inter_fn_retira_acentuacao(f_nfe.f_bairro)                     /*27*/
          ,f_nfe.cidade_dest_cod_cidade_ibge      /*28*/
          ,inter_fn_retira_acentuacao(f_nfe.cidade_dest_cidade)              /*29*/
          ,f_nfe.cidade_dest_estado               /*30*/
          ,f_nfe.sb_cod_cidade_ibge    /*31*/
          ,trim(to_char(f_nfe.cep_empresa,'00000000'))             /*32*/
          ,f_nfe.f500_telefone             /*33*/
          ,f_nfe.pais_dest_codigo_pais   /*34*/
          ,inter_fn_retira_acentuacao(f_nfe.pais_dest_nome_pais) /*35*/
          ,substr(replace(replace(f_nfe.f_inscr_estadual,'.',''),'-',''),1,14)            /*36*/
          ,v_ie_substituto                            /*37*/
          ,null             /*38*/
          ,null                            /*39*/
          ,decode(f_nfe.natop_nf_est_oper,'EX',null,f_nfe.cgc_9)           /*40*/
          ,decode(f_nfe.natop_nf_est_oper,'EX',null,f_nfe.cgc_4)           /*41*/
          ,decode(f_nfe.natop_nf_est_oper,'EX',null,f_nfe.cgc_2)           /*42*/
          ,inter_fn_retira_acentuacao(f_nfe.nome_cliente)               /*43*/
          ,inter_fn_retira_acentuacao(f_nfe.endereco_cliente)     /*44*/
          ,f_nfe.s010_numero_imovel         /*45*/
          ,inter_fn_retira_acentuacao(f_nfe.s010_complemento) /*46*/
          ,inter_fn_retira_acentuacao(f_nfe.s010_bairro)           /*47*/
          ,decode(f_nfe.natop_nf_est_oper,'EX','9999999', f_nfe.cidade_nfe_cod_cidade_ibge)     /*48*/
          ,inter_fn_retira_acentuacao(decode(f_nfe.natop_nf_est_oper,'EX','EXTERIOR', f_nfe.cidade_nfe_cidade))     /*49*/
          ,decode(f_nfe.natop_nf_est_oper,'EX','EX', f_nfe.cidade_nfe_estado)              /*50*/
          ,inter_fn_retira_acentuacao(f_nfe.pais_nfe_nome_pais)             /*51*/
          ,telefone_cliente_dest                 /*52*/
          ,trim(to_char(f_nfe.cep_cliente,'00000000'))                /*53*/
          ,f_nfe.codigo_fiscal           /*54*/
          ,decode(v_nfe_manut, 1, null,decode(f_nfe.inscr_est_forne,'ISENTO',null,f_nfe.inscr_est_forne))            /*55*/
          , substr(v_codigo_suframa,1,9)   /*56*/
          ,null                            /*57*/
          ,null                            /*58*/
          ,null                            /*59*/
          ,null                            /*60*/
          ,null                            /*61*/
          ,null                            /*62*/
          ,null                            /*63*/
          ,null                            /*64*/
          ,null                            /*65*/
          ,null                            /*66*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,inter_fn_trata_EX_num(f_nfe.natop_nf_est_oper,f_nfe.cgc_entr_cobr9))               /*67*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,inter_fn_trata_EX_num(f_nfe.natop_nf_est_oper,f_nfe.cgc_entr_cobr4))                /*68*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,inter_fn_trata_EX_num(f_nfe.natop_nf_est_oper,f_nfe.cgc_entr_cobr2))                /*69*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.p150_end_entr_cobr))                           /*70*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,f_nfe.p150_numero_imovel)                                                     /*71*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.complemento_endereco))                         /*72*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.bairro_entr_cobr))                             /*73*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,decode(f_nfe.natop_nf_est_oper,'EX','9999999', to_char(f_nfe.cidade_entr_cod_cidade,'9999999')))  /*74*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(decode(f_nfe.natop_nf_est_oper,'EX','EXTERIOR', f_nfe.cidade_entr_cidade)))       /*75*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,decode(f_nfe.natop_nf_est_oper,'EX','EX', f_nfe.cidade_entr_estado))             /*76*/
          ,f_nfe.base_icms                 /*77*/
          ,f_nfe.valor_icms                /*78*/
          ,f_nfe.base_icms_sub             /*79*/
          ,f_nfe.valor_icms_sub            /*80*/
          ,f_nfe.valor_itens_nfis          /*81*/
          ,f_nfe.valor_frete_nfis          /*82*/
          ,f_nfe.valor_seguro              /*83*/
          ,v_desconto_sem_suframa          /*84*/
          ,0.00                            /*85*/
          ,f_nfe.valor_ipi                 /*86*/
          ,v_valor_pis                     /*87*/
          ,v_valor_cofins                  /*88*/
          ,f_nfe.valor_despesas            /*89*/
          ,f_nfe.total_docto               /*90*/
          ,null                            /*91*/
          ,null                            /*92*/
          ,null                            /*93*/
          ,null                            /*94*/
          ,null                            /*95*/
          ,null                            /*96*/
          ,null                            /*97*/
          ,null                            /*98*/
          ,null                            /*99*/
          ,null                            /*100*/
          ,null                            /*101*/
          ,null                            /*102*/
          ,f_nfe.tipo_frete                /*103*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.trans_cgc9)                /*104*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.trans_cgc4)                /*105*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.trans_cgc2)                /*106*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.trans_nome))                /*107*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,f_nfe.trans_insest)             /*108*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.trans_end))                 /*109*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.trans_cidade))              /*110*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(f_nfe.trans_estado))              /*111*/
          ,null                            /*112*/
          ,null                            /*113*/
          ,null                            /*114*/
          ,null                            /*115*/
          ,null                            /*116*/
          ,null                            /*117*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,replace(f_nfe.placa_veiculo,' ',''))             /*118*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,f_nfe.uf_placa)                  /*119*/
          ,null                            /*120*/
          ,null                            /*121*/
          ,null                            /*122*/
          ,null                            /*123*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.qtde_embalagens)          /*124*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,f_nfe.especie_volume)            /*125*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,f_nfe.marca_volumes)             /*126*/
          ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,f_nfe.nr_volume)              /*127*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.peso_liquido)            /*128*/
          ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.peso_bruto)                /*129*/
          ,f_nfe.nr_recibo                            /*130*/
          ,null                            /*131*/
          ,null                            /*132*/
          ,null                            /*133*/
          ,null                            /*134*/
          ,null                            /*135*/
          ,null                            /*136*/
          ,f_nfe.cod_empresa               /*137*/
          ,f_nfe.p_cod_id                  /*138*/
          ,f_nfe.p_str_usuario             /*139*/
          ,f_nfe.e_mail                    /*140*/
          ,f_nfe.nfe_e_mail                /*141*/
          ,f_nfe.user_id_e_mail            /*142*/
          ,f_nfe.f_user_e_mail             /*143*/
          ,f_nfe.senha_e_mail              /*144*/
          ,inter_fn_retira_acentuacao(substr(inter_fn_mensagens_nfe(f_nfe.cod_empresa,f_nfe.num_nota_fiscal,f_nfe.serie_nota_fisc,f_nfe.natureza_interna,f_nfe.natop_nf_est_oper,'F'),1,2000))    /*145*/
          ,inter_fn_retira_acentuacao(substr(inter_fn_mensagens_nfe(f_nfe.cod_empresa,f_nfe.num_nota_fiscal,f_nfe.serie_nota_fisc,f_nfe.natureza_interna,f_nfe.natop_nf_est_oper,'C'),1,4000))  /*146*/
          ,v_ind_empr_simples /*147*/
          ,f_nfe.versao_systextilnfe /*148*/
          ,v_uf_embarque /*149*/
          ,v_local_embarque /*150*/
          ,f_nfe.hora_saida /*151*/
          ,v_data_cont_150 /*152*/
          ,v_just_cont_150 /*153*/
          ,v_id_dest /*154*/
          ,v_ind_final /*155*/
          ,v_ind_pres /*156*/
          ,v_id_estrangeiro /*157*/
          ,v_ind_ie_dest /*158*/
          ,0 /*159*/
          ,v_x_loc_despacho /*160*/
          ,v_valor_suframa_icms_aux /*161*/
          ,decode(v_FCP_UF_Dest_nf, null, 0, v_FCP_UF_Dest_nf) /*162*/
          ,decode(f_nfe.val_icms_uf_dest_nf, null, 0, f_nfe.val_icms_uf_dest_nf) /*163*/
          ,decode(f_nfe.val_icms_uf_remet_nf, null, 0, f_nfe.val_icms_uf_remet_nf) /*164*/
          ,0          /*165 - vfcpstret*/
          ,0          /*166 - vfcpst*/
          ,v_ipiDevolucao          /*167 - vipidevol*/
          ,decode(v_val_fcp_uf_ncm_nf, null, 0, v_val_fcp_uf_ncm_nf)   /*168 - vfcp*/
          ,v_imprime_boleto_danfe         /*169*/
      ,f_nfe.resp_tec_cgc9   /*170*/
      ,f_nfe.resp_tec_cgc4   /*171*/
      ,f_nfe.resp_tec_cgc2   /*172*/
      ,f_nfe.resp_tec_nome   /*173*/
      ,f_nfe.resp_tec_email   /*174*/
      ,f_nfe.resp_tec_telefone   /*175*/
      ,f_nfe.resp_tec_idCSRT   /*176*/
      ,f_nfe.resp_tec_hashCSRT   /*177*/
      ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,inter_fn_retira_acentuacao(nome_cliente_entr))   /*178*/
      ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,trim(to_char(f_nfe.cep_entr_cobr,'00000000'))) /*179*/
      ,inter_fn_trata_nulo_num(f_nfe.tipo_nf_referenciada,f_nfe.pais_dest_codigo_pais)   /*180*/
      ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,f_nfe.pais_dest_nome_pais)   /*181*/
      ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,telefone_cliente_entr)   /*182*/
      ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,substr(inter_fn_retira_acentuacao(email_entr),1,60))  /*183*/
      ,inter_fn_trata_nulo_str(f_nfe.tipo_nf_referenciada,decode(trim(insc_est_cliente_entr),'ISENTO','',substr(replace(replace(insc_est_cliente_entr,'.',''),'-',''),1,14)))   /*184*/
    ,v_intermediador /*185*/
    ,v_cgc9_intermediador /*186*/
    ,v_cgc4_intermediador /*187*/
    ,v_cgc2_intermediador /*188*/
    ,v_id_cad_int_tran /*189*/
    ,f_nfe.nfe_contigencia   /*190*/
      );

       EXCEPTION
          WHEN OTHERS THEN
             raise_application_error(-20000,'N?o inseriu dados na tabela obrf_150' || Chr(10) || SQLERRM);

       END;

        BEGIN
           update fatu_050
      set danfe_simplificada = v_danfe_simplificada
      where codigo_empresa = f_nfe.cod_empresa
        and num_nota_fiscal = f_nfe.num_nota_fiscal
        and serie_nota_fisc = f_nfe.serie_nota_fisc;
       EXCEPTION
          WHEN OTHERS THEN
        p_msg_erro := 'N?o atualizou danfe simplificada tabela fatu_050.' || Chr(10) || SQLERRM;
       END;

       v_ultima_nota_ref := 999;

       -- ITENS DE NOTAS FISCAIS DE SAIDA
       for f_nfe_item in c_nfs_item(p_cod_empresa, f_nfe.num_nota_fiscal, f_nfe.serie_nota_fisc,v_controleNarrativaBlocoK, v_controleDescSimulBlocoK)
       LOOP

          -- EMPRESA DO SIMPLES NACIONAL
          if v_ind_empr_simples = '1' and v_versao_layout_nfe = '1.10'
          then
             f_nfe_item.base_icms   := 0.00;
             f_nfe_item.perc_icms   := 0.00;
             f_nfe_item.valor_icms  := 0.00;
          end if;


          select obrf_150.id
          into   v_obrf_150_id
          from obrf_150
          where obrf_150.cod_empresa = p_cod_empresa
            and obrf_150.numero      = f_nfe.num_nota_fiscal
            and obrf_150.serie       = f_nfe.serie_nota_fisc
            and obrf_150.cod_usuario = p_cod_id
            and rownum               = 1
          order by obrf_150.id desc;

          v_produto := f_nfe_item.produto;
          v_produto := REPLACE(v_produto,'.');

          begin
            select basi_030.tipo_codigo_ean
            into v_tipo_codigo_ean
            from basi_030
            where basi_030.nivel_estrutura = substr(v_produto,1,1)
              and basi_030.referencia = substr(v_produto,2,5);
          exception
              when no_data_found then
              v_tipo_codigo_ean := 0;
          end;

          if length(trim(f_nfe_item.codigo_barras)) in (13,14)
             and (v_tipo_cod_barras = 2
                    and ((substr(f_nfe_item.codigo_barras, 1, 3) in ('789','790') and length(trim(f_nfe_item.codigo_barras)) = 13)
                    or INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'faturamento.codEanXml') = 'S' )
                )
             and (f_nfe_item.tipo_impressao = '3' or f_nfe_item.tipo_impressao = '4' or f_nfe_item.tipo_impressao = '5' or
                 (f_nfe_item.tipo_impressao = '2' and v_tipo_codigo_ean = 2) or
                 (f_nfe_item.tipo_impressao = '2' and v_tipo_codigo_ean = 3) or
                 (f_nfe_item.tipo_impressao = '1' and v_tipo_codigo_ean = 3)) then
            v_cod_barras := f_nfe_item.codigo_barras;
          else
            v_cod_barras := '0000000000000';
          end if;

          select seq_obrf_152.nextval into v_obrf_152_id from dual;

          begin
             select basi_240.codigo_ex_ipi, decode(f_nfe.natop_nf_est_oper,'EX',basi_240.cod_enquadr_ipi_exp,basi_240.cod_enquadramento_ipi),
                    basi_240.unid_med_trib, basi_240.considera_calculo_beneficio
             into   w_ex_ipi,               w_cod_enq_ipi,
                    w_unid_med_trib_ncm,    w_considera_calculo_beneficio
             from basi_240
             where basi_240.classific_fiscal = f_nfe_item.classific_fiscal;
          exception
              when no_data_found then
                 w_ex_ipi                 := 0;
                 w_unid_med_trib_ncm      := ' ';
                 w_cod_enq_ipi            := '999';
                     w_considera_calculo_beneficio := ' ';
          end;

          begin
            select decode(f_nfe.natop_nf_est_oper,'EX', obrf_140.cod_enquadramento_exportacao, obrf_140.cod_enquadramento_saida)
                into w_cod_enq_ipi_140
              from obrf_140
              where obrf_140.classific_fiscal = f_nfe_item.classific_fiscal
                  and obrf_140.natur_operacao   = f_nfe_item.natur_operacao
                  and obrf_140.estado_natoper   = f_nfe_item.natopeno_est_oper
                  and obrf_140.tipo             = 2
                  and (obrf_140.tipo_cliente    = f_nfe.tipo_cliente or obrf_140.tipo_cliente = 999);
              exception
                when no_data_found then
                  w_cod_enq_ipi_140 := null;
          end;

          if w_cod_enq_ipi_140 is not null then
             w_cod_enq_ipi   := w_cod_enq_ipi_140;
          end if;

          if f_nfe_item.p_080_respeita_ipi = 2
          then w_cod_enq_ipi := '999';
          end if;

          if f_nfe_item.c_015_unidade_medida_com <> f_nfe_item.c_015_unidade_medida_trib and
             f_nfe_item.c_015_unidade_medida_com <> ' ' and f_nfe_item.c_015_unidade_medida_com is not null
          then
            v_um_faturamento_qtde       := f_nfe_item.c_015_quantidade_com;
            v_um_faturamento_um         := f_nfe_item.c_015_unidade_medida_com;
            v_um_faturamento_valor_unit := f_nfe_item.c_015_valor_unitario_com;
          else
            v_um_faturamento_qtde       := f_nfe_item.c_015_quantidade_trib;
            v_um_faturamento_um         := f_nfe_item.c_015_unidade_medida_trib;
            v_um_faturamento_valor_unit := f_nfe_item.c_015_valor_unitario_trib;
          end if;

          if (length(v_produto) = 6)
          then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := '000';
             v_item     := '000000';
          elsif (length(v_produto) = 7)
          then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := substr(v_produto,7,1);
             v_item     := '000000';
          elsif (length(v_produto) = 8)
          then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := substr(v_produto,7,2);
             v_item     := '000000';
          elsif (length(v_produto) = 9)
          then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := substr(v_produto,7,3);
             v_item     := '000000';
          elsif (length(v_produto) = 13)
          then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := substr(v_produto,7,1);
             v_item     := substr(v_produto,8,6);
          elsif (length(v_produto) = 14)
          then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := substr(v_produto,7,2);
             v_item     := substr(v_produto,9,6);
          elsif (length(v_produto) = 15)
          then
             v_nivel    := substr(v_produto,1,1);
             v_grupo    := substr(v_produto,2,5);
             v_subgrupo := substr(v_produto,7,3);
             v_item     := substr(v_produto,10,6);
          end if;

          v_alterar_ncm := true;

          begin
              select basi_410.classific_fiscal
              into v_ncm
              from basi_410
             where basi_410.nivel    = v_nivel
               and basi_410.grupo    = v_grupo
               and basi_410.subgrupo = v_subgrupo
               and basi_410.item     = v_item
               and basi_410.pais     = f_nfe.codigo_pais;
          exception
              when no_data_found then
                 begin
                    select basi_410.classific_fiscal
                      into v_ncm
                    from basi_410
                    where basi_410.nivel    = v_nivel
                      and basi_410.grupo    = v_grupo
                      and basi_410.subgrupo = v_subgrupo
                      and basi_410.item     = '000000'
                      and basi_410.pais     = f_nfe.codigo_pais;
                 exception
                     when no_data_found then

                        begin
                           select basi_410.classific_fiscal
                           into v_ncm
                           from basi_410
                           where basi_410.nivel    = v_nivel
                             and basi_410.grupo    = v_grupo
                             and basi_410.subgrupo = '000'
                             and basi_410.item     = '000000'
                             and basi_410.pais     = f_nfe.codigo_pais;
                        exception
                            when others then
                               v_alterar_ncm := false;
                               v_ncm         := f_nfe_item.classific_fiscal;
                        end;
                 end;
          end;

          -- Indica se ? para alterar a NCM do fatu_060

          if v_alterar_ncm
          then
             update fatu_060 f
             set f.classific_fiscal = v_ncm
             where f.ch_it_nf_cd_empr  = p_cod_empresa
             and   f.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
             and   f.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc

             and   f.nivel_estrutura   = v_nivel
             and   f.grupo_estrutura   = v_grupo
             and  (f.subgru_estrutura  = v_subgrupo or v_subgrupo = '000')
             and  (f.item_estrutura    = v_item     or v_item     = '000000');

          end if;

          v_percIpiDevol := 0.00;
          v_valIpiDevol  := 0.00;

          if (v_destaca_ipi = 1 and f_nfe.nota_fatura > 0) or (f_nfe_item.cvf_ipi_saida = 99 and f_nfe.tipo_transacao = 'D')
          then
             if (f_nfe_item.cvf_ipi_saida = 99 and f_nfe.tipo_transacao = 'D')
             then
                select nvl(sum(obrf_015.quantidade),0)
                into v_quantidadeDevol
                from obrf_015
                where obrf_015.capa_ent_nrdoc   = f_nfe_item.nf_origem_devol
                  and obrf_015.capa_ent_serie   = f_nfe_item.serie_nf_devol
                  and obrf_015.capa_ent_forcli9 = f_nfe_item.cnpj_9_devol
                  and obrf_015.capa_ent_forcli4 = f_nfe_item.cnpj_4_devol
                  and obrf_015.capa_ent_forcli2 = f_nfe_item.cnpj_2_devol
                  and obrf_015.coditem_nivel99  = v_nivel
                  and obrf_015.coditem_grupo    = v_grupo
                  and (obrf_015.coditem_subgrupo = v_subgrupo or v_subgrupo = '000')
                  and (obrf_015.coditem_item     = v_item     or v_item     = '000000');

                if v_quantidadeDevol is null or v_quantidadeDevol = 0
                then
                    v_quantidadeDevol := v_um_faturamento_qtde;
                end if;

                if v_quantidadeDevol > 0
                then
                   v_percIpiDevol := (v_um_faturamento_qtde / v_quantidadeDevol) * 100.00000;
                else
                   v_percIpiDevol := 100.0;
                end if;

                if v_percIpiDevol > 100.00
                then
                   v_percIpiDevol := 100.00;
                end if;

                if f_nfe_item.valor_ipi = 0.00
                then
                   v_percIpiDevol := 0.00;
                end if;

                v_valIpiDevol  := f_nfe_item.valor_ipi;
             end if;

             f_nfe_item.valor_ipi := 0.00;
             f_nfe_item.base_ipi := 0.00;
             f_nfe_item.perc_ipi := 0.00;

          end if;

          v_ncm        := substr(REPLACE(v_ncm,'.'),1,8);
          v_ncm_genero := substr(REPLACE(v_ncm,'.'),1,2);

          if (v_quebra_niv = 1 and v_imp_tam_danfe = 1)
          then
             v_tam_conca := ' ';

             if substr(f_nfe_item.produto, 1, 1) = '1'
             then
                begin
                   select obrf_161.tamanhos
                   into   v_tam_conca
                   from obrf_161
                   where obrf_161.codigo_empresa       = p_cod_empresa
                     and obrf_161.num_nota_fiscal      = f_nfe.num_nota_fiscal
                     and obrf_161.serie_nota_fisc      = f_nfe.serie_nota_fisc
                     and obrf_161.nivel_grupo          = substr(f_nfe_item.produto, 1, 6)
                     and obrf_161.descricao_geral      = f_nfe_item.descricao_prod
                     and obrf_161.cst                  = f_nfe_item.cst
                     and obrf_161.cfop                 = f_nfe_item.cfop
                     and obrf_161.classific_fiscal     = f_nfe_item.classific_fiscal
                     and obrf_161.unidade_medida       = f_nfe_item.unidade_medida
                     and obrf_161.valor_unitario       = f_nfe_item.valor_unitario
                     and (obrf_161.um_faturamento_um   = f_nfe_item.c_015_unidade_medida_com or f_nfe_item.c_015_unidade_medida_com is null);
                exception
                    when no_data_found then
                       v_tam_conca := ' ';
                end;

                if v_tam_conca <> ' '
                then
                   f_nfe_item.inf_ad_prod := trim(f_nfe_item.inf_ad_prod) || ' ' || trim(v_tam_conca);
                end if;

             end if;
          end if;

          BEGIN
             select basi_167.codigo_fiscal_uf into v_cod_fiscal_uf
             from basi_167
             where basi_167.estado = f_nfe.cidade_nfe_estado
               and rownum         = 1;
          EXCEPTION
            when access_into_null then
               v_cod_fiscal_uf := 0;
          END;

          -- sa?das

          if f_nfe.tipo_nf_referenciada = 1 or f_nfe.tipo_nf_referenciada = 4 -- DEVOLU??ES
          then
             f_nfe_item.nota_origem := f_nfe_item.nf_origem_devol;
             f_nfe_item.serie_origem := f_nfe_item.serie_nf_devol;

             f_nfe.f_cgc_9 := f_nfe_item.cnpj_9_devol;
             f_nfe.f_cgc_4 := f_nfe_item.cnpj_4_devol;
             f_nfe.f_cgc_2 := f_nfe_item.cnpj_2_devol;


          end if;

          if  ((f_nfe.tipo_nf_referenciada in (2,4,8,9))
            or (f_nfe.tipo_nf_referenciada = 3 and f_nfe.nota_estorno = 1) 
            or (f_nfe_item.cod_natureza = '7501'))
          and f_nfe_item.nota_origem > 0
          and f_nfe_item.serie_origem is not null
          then
             -- DANFE DA NF REF.

             if (f_nfe.tipo_nf_referenciada in (2,9)) -- NF COMPLEMENTAR ou NFs RELACIONADAS
             then
                BEGIN
                   select fatu_050.numero_danf_nfe into v_numero_danfe_ref
                   from fatu_050
                   where fatu_050.codigo_empresa     = f_nfe.cod_empresa
                     and fatu_050.num_nota_fiscal    = f_nfe_item.nota_origem
                     and fatu_050.serie_nota_fisc    = f_nfe_item.serie_origem;
                EXCEPTION
                   when others then
                     v_numero_danfe_ref := ' ';
                END;
             end if;

             if (f_nfe.tipo_nf_referenciada = 3 and f_nfe.nota_estorno = 1)  --NF AJUSTES
             or f_nfe.tipo_nf_referenciada = 4 -- DEVOLUCOES
             or ((f_nfe.tipo_nf_referenciada = 9 or f_nfe_item.cod_natureza = '7501') -- exportacoes das mercadorias recebidas anteriormente com finalidade especifica de exportacao
             or (f_nfe.tipo_nf_referenciada = 8)
             and (trim(v_numero_danfe_ref) is null))
             then

                BEGIN
                   select obrf_010.numero_danf_nfe, obrf_010.especie_docto
                         into v_numero_danfe_ref,         v_especie_docto
                   from obrf_010
                   where obrf_010.documento        = f_nfe_item.nota_origem
                     and obrf_010.serie            = f_nfe_item.serie_origem
                     and obrf_010.cgc_cli_for_9    = f_nfe.cgc_9
                     and obrf_010.cgc_cli_for_4    = f_nfe.cgc_4
                     and obrf_010.cgc_cli_for_2    = f_nfe.cgc_2;
                EXCEPTION
                   when others then
                     v_numero_danfe_ref := ' ';
                     v_especie_docto := ' ';
                END;
             end if;

             if trim(v_numero_danfe_ref) is null
             then
               begin
                select obrf_990.chave_nfe, obrf_990.especie_nfe
                into v_numero_danfe_ref, v_especie_docto
                from obrf_990
                where obrf_990.tipo_nota   = 'S'
                  and obrf_990.cod_empresa = f_nfe.cod_empresa
                  and obrf_990.num_nota    = f_nfe.num_nota_fiscal
                  and obrf_990.serie_nota  = f_nfe.serie_nota_fisc
                  and obrf_990.cnpj9       = f_nfe.f_cgc_9
                  and obrf_990.cnpj4       = f_nfe.f_cgc_4
                  and obrf_990.cnpj2       = f_nfe.f_cgc_2
                  and rownum               = 1;
                exception
                  when others then
                    v_numero_danfe_ref := '';
                    v_especie_docto := '';
                end;
             end if;

             begin
                select obrf_151.id into v_obrf_151_id
                from obrf_151
                where obrf_151.numero           = f_nfe_item.nota_origem
                and   obrf_151.serie            = f_nfe_item.serie_origem
                and   obrf_151.cnpj_emitente9 = f_nfe.f_cgc_9
                and   obrf_151.cnpj_emitente4 = f_nfe.f_cgc_4
                and   obrf_151.cnpj_emitente2 = f_nfe.f_cgc_2
                and   obrf_151.nfe_id         = v_obrf_150_id
                and rownum                    = 1;
             exception
             when no_data_found then

                begin
                   select obrf_200.modelo_documento into w_modelo_documento
                   from obrf_200
                   where obrf_200.codigo_especie = v_especie_docto;
                   exception
                   when no_data_found then
                   w_modelo_documento := null;
                end;

                if (f_nfe.tipo_nf_referenciada <> 4) -- SE N?O FOR DEVOLU??O
                then
                   w_modelo_documento := '01';
                end if;

                /*SAIDA*/
                BEGIN
                   insert into obrf_151
                      (id
                      ,nfe_id
                      ,chave_acesso
                      ,codigo_uf
                      ,ano_mes_emissao
                      ,cnpj_emitente9
                      ,cnpj_emitente4
                      ,cnpj_emitente2
                      ,modelo_documento
                      ,serie
                      ,numero
                      ,cgc_cli_for_9
                      ,cgc_cli_for_4
                      ,cgc_cli_for_2)
                  values
                     (p_cod_id
                     ,v_obrf_150_id
                     ,v_numero_danfe_ref
                     ,v_cod_fiscal_uf
                     ,f_nfe.data_saida
                     ,f_nfe.f_cgc_9
                     ,f_nfe.f_cgc_4
                     ,f_nfe.f_cgc_2
                     ,w_modelo_documento
                     ,f_nfe_item.serie_origem
                     ,f_nfe_item.nota_origem
                     ,f_nfe.f_cgc_9
                     ,f_nfe.f_cgc_4
                     ,f_nfe.f_cgc_2);

                EXCEPTION
                   WHEN OTHERS THEN
                   p_msg_erro := 'N?o inseriu dados na tabela obrf_151' || Chr(10) || SQLERRM;
                END;
             end;
          end if;

          -- AJUSTE DE IPI
          if  f_nfe.tipo_nf_referenciada in (2,3) and f_nfe_item.valor_icms = 0.00 and f_nfe_item.valor_ipi > 0.00
          then
             for nf_aj_ipi in (
                select distinct fatu_060.ch_it_nf_num_nfis nota_aj_ipi, fatu_050.especie_docto,
                                fatu_050.nr_cupom, fatu_050.cod_impressora_fiscal
                from fatu_060, fatu_050
                where fatu_060.ch_it_nf_cd_empr  = p_cod_empresa
                  and fatu_060.num_nota_ecf_ipi  = f_nfe.num_nota_fiscal
                  and fatu_060.ch_it_nf_ser_nfis = 'CF'
                  and fatu_060.ch_it_nf_cd_empr  = fatu_050.codigo_empresa
                  and fatu_060.ch_it_nf_num_nfis = fatu_050.num_nota_fiscal
                  and fatu_060.ch_it_nf_ser_nfis = fatu_050.serie_nota_fisc)
             loop
                begin
                   select count(*) into v_tem_nf_aj_ipi from obrf_151
                   where obrf_151.nfe_id = v_obrf_150_id
                     and obrf_151.numero = nf_aj_ipi.nota_aj_ipi;
                end;

                begin
                   select obrf_200.modelo_documento into w_modelo_documento from obrf_200
                   where obrf_200.codigo_especie    = nf_aj_ipi.especie_docto;
                   exception
                   when no_data_found then
                   w_modelo_documento := null;
                end;

                -- insert 2 obrf_151
                if v_tem_nf_aj_ipi = 0 -- SE N?O ENCONTRAR NOTA DE AJUSTE INSERE
                then
                   BEGIN
                      insert into obrf_151
                        (id
                        ,nfe_id
                        ,chave_acesso
                        ,codigo_uf
                        ,ano_mes_emissao
                        ,cnpj_emitente9
                        ,cnpj_emitente4
                        ,cnpj_emitente2
                        ,modelo_documento
                        ,serie
                        ,numero
                        ,n_ecf
                        ,n_coo)
                     values
                       (p_cod_id
                       ,v_obrf_150_id
                       ,null
                       ,v_cod_fiscal_uf
                       ,f_nfe.data_saida
                       ,f_nfe.f_cgc_9
                       ,f_nfe.f_cgc_4
                       ,f_nfe.f_cgc_2
                       ,w_modelo_documento
                       ,f_nfe_item.serie_origem
                       ,nf_aj_ipi.nota_aj_ipi
                       ,nf_aj_ipi.cod_impressora_fiscal
                       ,nf_aj_ipi.nr_cupom);
                   EXCEPTION
                   WHEN OTHERS THEN
                      p_msg_erro := 'N?o inseriu dados na tabela obrf_151 (2)' || Chr(10) || SQLERRM;
                   END;
                end if;
             end loop;
          else
             if f_nfe.tipo_nf_referenciada = 1 and (f_nfe_item.nota_origem > 0 and f_nfe_item.nr_cupom > 0 and f_nfe.ind_natur_cupom_ref = 1)
             then
               -- CUPONS FISCAIS REFERENCIADOS NA NOTA FISCAL

                begin
                   select obrf_151.id
                   into   v_obrf_151_id
                   from obrf_151
                   where obrf_151.nfe_id = v_obrf_150_id
                     and obrf_151.n_coo  = f_nfe_item.nr_cupom
                     and obrf_151.n_ecf  = f_nfe_item.nr_caixa;
                exception
                when no_data_found then

                   begin
                       select obrf_200.modelo_documento into w_modelo_documento
                       from fatu_505, obrf_200
                       where fatu_505.codigo_especie  = obrf_200.codigo_especie
                         and fatu_505.codigo_empresa  = p_cod_empresa
                         and fatu_505.serie_nota_fisc = f_nfe_item.serie_origem;
                       exception
                       when no_data_found then
                       w_modelo_documento := null;
                    end;

                   BEGIN
                      insert into obrf_151
                        (id
                        ,nfe_id
                        ,chave_acesso
                        ,codigo_uf
                        ,ano_mes_emissao
                        ,cnpj_emitente9
                        ,cnpj_emitente4
                        ,cnpj_emitente2
                        ,modelo_documento
                        ,serie
                        ,numero
                        ,n_ecf
                        ,n_coo)
                     values
                       (p_cod_id
                       ,v_obrf_150_id
                       ,null
                       ,v_cod_fiscal_uf
                       ,f_nfe.data_saida
                       ,f_nfe.f_cgc_9
                       ,f_nfe.f_cgc_4
                       ,f_nfe.f_cgc_2
                       ,w_modelo_documento
                       ,f_nfe_item.serie_origem
                       ,f_nfe_item.nota_origem
                       ,f_nfe_item.nr_caixa
                       ,f_nfe_item.nr_cupom);
                   EXCEPTION
                   WHEN OTHERS THEN
                      p_msg_erro := 'N?o inseriu dados na tabela obrf_151 (2)' || Chr(10) || SQLERRM;
                   END;
                end;
             end if;
          end if;

          v_valor_unit_comercial := 0.0000000000;
          v_vlr_unit_tributavel  := 0.0000000000;

          if (((round(v_um_faturamento_qtde * v_um_faturamento_valor_unit, 2)) <> f_nfe_item.valor_total) and
               f_nfe.tipo_nf_referenciada = 1)
          then
             v_valor_unit_comercial := round(f_nfe_item.valor_total / v_um_faturamento_qtde,10);
             v_vlr_unit_tributavel := round(f_nfe_item.valor_total / f_nfe_item.c_015_quantidade_trib,10);
          else
             v_valor_unit_comercial := v_um_faturamento_valor_unit;
             v_vlr_unit_tributavel := f_nfe_item.c_015_valor_unitario_trib;
          end if;

          inter_pr_perc_importados(f_nfe.cgc_9,
                                   f_nfe.cgc_4,
                                   f_nfe.cgc_2,
                                   f_nfe_item.natur_operacao,
                                   f_nfe_item.natopeno_est_oper,
                                   f_nfe.o_data_emissao,
                                   f_nfe.cod_empresa,
                                   v_nivel,
                                   v_grupo,
                                   v_subgrupo,
                                   v_item,
                                   v_perc_icms_imp,
                                   v_flag_icms_imp,
                                   v_procedencia_imp,
                                   v_flag_proc_imp,
                                   v_mensagem_imp,
                                   v_flag_msg_imp,
                                   v_numero_fci,
                                   v_flag_nfci);

          v_obs_aux        := f_nfe_item.inf_ad_prod;
          v_numero_fci_aux := '';

          if v_flag_msg_imp = 's'
          then v_obs_aux := f_nfe_item.inf_ad_prod || ' ' || v_mensagem_imp;
          end if;

          if v_flag_nfci = 's' and trim(v_numero_fci) != ''
          then v_numero_fci_aux := v_numero_fci;
          end if;

          /* Identificar a sequencia do item na fatu_060*/
          begin
             select fatu_060.seq_item_nfisc
             into v_seq_item_draw
             from fatu_060
             where fatu_060.ch_it_nf_cd_empr  = p_cod_empresa
               and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
               and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc
               and fatu_060.nivel_estrutura =  v_nivel
               and fatu_060.grupo_estrutura = v_grupo
               and fatu_060.subgru_estrutura = v_subgrupo
               and fatu_060.item_estrutura = v_item
               and rownum = 1;
               exception
                 when no_data_found then
                   v_seq_item_draw    := 99999;
          end;

          begin
             select fatu_067.n_draw
             into v_n_draw
             from fatu_067
             where fatu_067.codigo_empresa = p_cod_empresa
               and fatu_067.num_nota_fiscal = f_nfe.num_nota_fiscal
               and fatu_067.serie_nota_fisc = f_nfe.serie_nota_fisc
               and (replace(fatu_067.ncm,'.') = v_ncm or
                    fatu_067.seq_nota_fisc = v_seq_item_draw);
          exception
              when no_data_found then
                v_n_draw                 := '';
          end;
          v_inf_ad_suframa :=  '';

          if f_nfe_item.valor_suframa_icms > 0
          then
              v_inf_ad_suframa :=  'Valor do ICMS abatido: R$ ' || f_nfe_item.valor_suframa_icms || ' (' || v_perc_suframa || '% sobre ' || f_nfe_item.base_calc_icms_suframa || ').';
          end if;

          if trim(v_inf_ad_suframa) is not null
          then
             if trim(v_obs_aux) is not null
             then
                v_obs_aux := v_obs_aux||' - '|| v_inf_ad_suframa;
             else
                v_obs_aux := v_inf_ad_suframa;
             end if;
          end if;

          /*CTS 51*/
          if f_nfe_item.pDif is null
          then f_nfe_item.pDif:= 0;
          end if;
           if f_nfe_item.vICMSOp is null
          then f_nfe_item.vICMSOp:= 0;
          end if;
           if f_nfe_item.vICMSDif is null
          then f_nfe_item.vICMSDif:= 0;
          end if;

          v_inf_ad_cst51 :=  '';

          if f_nfe_item.vICMSDif > 0 and f_nfe_item.pDif < 100.00
          then
             /*Verifica se exite termo cadastrado para opera? com cst 51*/
             if v_termo_cst51 is null
             then
                if f_nfe.natop_nf_est_oper = 'PR'
                then v_termo_cst51 :=  ') nos termos do inciso I do art.96 do Decreto n?1.980/07 (RICMS/PR)';
                else v_termo_cst51 :=  ') DIFERIMENTO PARCIAL ICMS CONF LIV III ART 1 -DO RICMS/97 -RS';
             end if;
          end if;
             v_inf_ad_cst51 :=  'Operacao com diferimento parcial do imposto de R$ ' || f_nfe_item.vICMSDif || ' (' || f_nfe_item.pDif || '% de ' || f_nfe_item.vICMSOp || ') ' || v_termo_cst51;
          end if;

          if trim(v_inf_ad_cst51) is not null
          then
             if trim(v_obs_aux) is not null
             then
                v_obs_aux := v_obs_aux||' - '|| v_inf_ad_cst51;
             else
                v_obs_aux := v_inf_ad_cst51;
             end if;
          end if;

            if not v_difal and f_nfe_item.val_fcp_uf_dest > 0
          then

              --//monta a mesnagem com o valores acima
              --(FCP): Base R$ 12.70 Perc.(2%) Vlr. R$ 0.25
              if trim(v_obs_aux) is not null
              then
                 v_obs_aux := v_obs_aux||'  '|| '(FCP): Base R$ '  || TRIM(to_char(f_nfe_item.base_icms,'9999999990.00'))
                                             || ' Perc. ('          || TRIM(to_char(f_nfe_item.perc_fcp_uf_dest,'9999999990.00'))
                                             || '%) Vlr. R$ '      || TRIM(to_char(f_nfe_item.val_fcp_uf_dest,'9999999990.00'));
              else
                 v_obs_aux := '(FCP): Base R$ '     || TRIM(to_char(f_nfe_item.base_icms,'9999999990.00'))
                           || ' Perc. ('             || TRIM(to_char(f_nfe_item.perc_fcp_uf_dest,'9999999990.00'))
                           || '%) Vlr. R$ '         || TRIM(to_char(f_nfe_item.val_fcp_uf_dest,'9999999990.00'));
              end if;

          end if;

          if f_nfe_item.cest is null
          then
             f_nfe_item.cest := ' ';
          end if;

          -- QUANDO FOR OPERA??O EXTERIOR, UNIDADE DE MEDIDA DEVE SER PADR?O DA RFB - S A I D A S
          if (f_nfe.natop_nf_est_oper = 'EX' or f_nfe_item.ind_cfop_exportacao = 'S') and trim(w_unid_med_trib_ncm) is not null and
             f_nfe_item.c_015_unidade_medida_trib <> w_unid_med_trib_ncm and (f_nfe_item.fator_umed_trib > 0.00000 or (f_nfe_item.pesoDeRolos > 0.000000 and w_unid_med_trib_ncm = 'KG')
             and v_um_faturamento_qtde > 0)
          then

             c_015_unidade_medida_trib := w_unid_med_trib_ncm;

                /* QUANDO SE TRABALHA COM ROLO E TEM PESO PARA OS MESMOS, PEGA O PESO DESTES */
             if f_nfe_item.pesoDeRolos > 0.000000 and w_unid_med_trib_ncm = 'KG'
             then
                c_015_quantidade_trib     := f_nfe_item.pesoDeRolos;
                v_vlr_unit_tributavel     := round(f_nfe_item.valor_total / c_015_quantidade_trib,10);
             else if f_nfe_item.fator_umed_trib <> 1
                then
                   c_015_quantidade_trib     := v_um_faturamento_qtde * f_nfe_item.fator_umed_trib;
                   v_vlr_unit_tributavel     := round(f_nfe_item.valor_total / c_015_quantidade_trib,10);
                else
                   c_015_quantidade_trib     := f_nfe_item.c_015_quantidade_trib;
                end if;
             end if;

             update fatu_060
               set fatu_060.unid_med_trib = w_unid_med_trib_ncm
                where fatu_060.ch_it_nf_cd_empr  = f_nfe.cod_empresa
                  and fatu_060.ch_it_nf_num_nfis = f_nfe.num_nota_fiscal
                  and fatu_060.ch_it_nf_ser_nfis = f_nfe.serie_nota_fisc
                  and fatu_060.classific_fiscal = f_nfe_item.classific_fiscal;
               commit;
          else
             c_015_unidade_medida_trib := f_nfe_item.c_015_unidade_medida_trib;
             c_015_quantidade_trib     := f_nfe_item.c_015_quantidade_trib;
          end if;

          v_codBeneficioFiscal := '';
          
          if f_nfe_item.cvf_icms > 0
          then
            begin
                SELECT COD_BENEFICIO_FISCAL,COD_BENEFICIO_FISCAL_AD
                into v_codBeneficioFiscal, v_codBeneficioFiscalAd
                FROM (
                    SELECT OBRF_805.COD_BENEFICIO_FISCAL, OBRF_805.COD_BENEFICIO_FISCAL_AD,
                          MAX( DECODE(OBRF_805.EMPRESA_REMETENTE     ,9999              ,0,1) +
                               DECODE(OBRF_805.UF_DESTINATARIO       ,'XXX'             ,0,2) +
                               DECODE(OBRF_805.NATUREZA_OPERACAO     ,9999              ,0,3) +
                               DECODE(OBRF_805.CIDADE_DESTINO        ,999999            ,0,4) +
                               DECODE(OBRF_805.CLASSIFICACAO_FISCAL  ,'XXXXXXXXXXXXXXX' ,0,5) ) ORDEM
                      FROM OBRF_805
                     WHERE OBRF_805.UF_REMETENTE         = F_NFE.CIDADE_DEST_ESTADO
                       AND OBRF_805.CST_ICMS             = F_NFE_ITEM.CVF_ICMS
                       AND OBRF_805.CLASSIFICACAO_FISCAL IN (F_NFE_ITEM.CLASSIFIC_FISCAL, 'XXXXXXXXXXXXXXX')
                       AND OBRF_805.NATUREZA_OPERACAO    IN (F_NFE_ITEM.NATUR_OPERACAO  , 9999)
                       AND OBRF_805.UF_DESTINATARIO      IN (F_NFE.CIDADE_NFE_ESTADO    , 'XXX')
                       AND OBRF_805.EMPRESA_REMETENTE    IN (F_NFE.COD_EMPRESA          , 9999)
                       AND OBRF_805.CIDADE_DESTINO       IN (F_NFE.CIDADE_DESTINO       , 999999)
                    GROUP BY OBRF_805.COD_BENEFICIO_FISCAL, OBRF_805.COD_BENEFICIO_FISCAL_AD,ROWNUM
                    ORDER BY 3 DESC)
                WHERE ROWNUM = 1;

            exception
                when no_data_found then
                  v_codBeneficioFiscalAd := '';
                  if w_considera_calculo_beneficio = 'S' and f_nfe_item.ind_beneficio_icms = 'S'
                  then
                     v_codBeneficioFiscal := INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'fiscal.codAjusteIcmsBeneficio');
                  else
                     v_codBeneficioFiscal := '';
                  end if;
            end;
          end if;

          v_valor_icms_deson := 0.00;
          v_motivo_deson     := 0;

          if v_codBeneficioFiscal is not null and f_nfe.estado_empresa_deson = 'SC' and f_nfe_item.cvf_icms in(20,40,41,50,51) and f_nfe_item.valor_suframa_icms = 0 
          then
          
             begin
               select perc_icms_destino
               into v_perc_icms_deson
               from basi_l369
               where uf_origem = f_nfe.estado_empresa_deson 
                 and uf_destino = f_nfe_item.natopeno_est_oper;
             exception
              when no_data_found then
                 if f_nfe.estado_empresa_deson = f_nfe_item.natopeno_est_oper
                 then
                    v_perc_icms_deson := 17;
                 else
                    v_perc_icms_deson := 12;
                 end if;
             end;
          

             case f_nfe_item.cvf_icms
                when 20 then
                  v_motivo_deson := 9;                  
                  v_valor_icms_deson := (f_nfe_item.valor_total-f_nfe_item.base_icms) * (f_nfe_item.perc_icms / 100);
                when 40 then
                  v_motivo_deson := 9;
                  v_valor_icms_deson := round(f_nfe_item.base_icms_cbenef * v_perc_icms_deson / 100, 2);
                when 41 then
                  v_motivo_deson := 0;
                  v_valor_icms_deson := 0.00;
                when 50 then
                  v_motivo_deson := 9;
                  v_valor_icms_deson := round(f_nfe_item.base_icms_cbenef * v_perc_icms_deson / 100, 2);
                when 51 then
                  if f_nfe_item.valor_icms > 0 
                     then            
                     f_nfe_item.pDif := f_nfe_item.perc_reducao_icm;
                     f_nfe_item.vICMSOp := f_nfe_item.valor_icms / (1 - (f_nfe_item.perc_reducao_icm / 100));
                     f_nfe_item.vICMSDif := f_nfe_item.vICMSOp - f_nfe_item.valor_icms;
                  end if;
                ELSE
                  v_motivo_deson := 0;
             end case;
    
             if f_nfe_item.valor_suframa_icms = 0 or f_nfe_item.valor_suframa_icms is null
             then
                update obrf_150 o
                set o.valor_suframa = nvl(o.valor_suframa,0) + v_valor_icms_deson
                where o.id = v_obrf_150_id;
                commit;
             else
                v_valor_icms_deson := f_nfe_item.valor_suframa_icms;
             end if;

          else

             if f_nfe_item.valor_suframa_icms > 0
             then
                v_valor_icms_deson := f_nfe_item.valor_suframa_icms;
                v_motivo_deson     := 7;
             end if;
          end if;

          if v_valor_icms_deson = 0.00
          then
                v_valor_icms_deson := null;
          end if;

          if INTER_FN_GET_PARAM_STRING(p_cod_empresa, 'fiscal.enviaDescricaoFtecF015') = 'S'
          then
          begin
             select 'Item CEA: ' || basi_010.codigo_cliente
             into w_inf_ad_prod
             from basi_010
             where basi_010.nivel_estrutura  = v_nivel
               and basi_010.grupo_estrutura  = v_grupo
               and basi_010.subgru_estrutura = v_subgrupo
               and basi_010.item_estrutura   = v_item;
          exception
             when no_data_found then
                w_inf_ad_prod := v_obs_aux;
          end;
         else
            w_inf_ad_prod := v_obs_aux;
         end if;

         if trim(v_codBeneficioFiscalAd) is not null
          then
             if trim(w_inf_ad_prod) is not null
             then
                w_inf_ad_prod := w_inf_ad_prod||'  '|| v_codBeneficioFiscalAd;
             else
                w_inf_ad_prod := v_codBeneficioFiscalAd;
             end if;
          end if;

         v_desconto_sem_suframa := f_nfe_item.rateio_descontos_ipi; -- DESCONTAR O VALOR DO SUFRAMA

         if f_nfe_item.valor_suframa_icms is not null
         then
            if f_nfe_item.rateio_descontos_ipi >= f_nfe_item.valor_suframa_icms
            then
               v_desconto_sem_suframa := f_nfe_item.rateio_descontos_ipi - f_nfe_item.valor_suframa_icms;
            end if;
         end if;
         
         begin
            select case when exists (select 1 from obrf_183) then 1 else 0 end 
            into v_entrou_cadastro 
            from dual;
            
            if v_entrou_cadastro > 0
            then
              begin
                select basi_030.colecao,
                case
                  when basi_400.codigo_informacao is not null then
                   basi_400.codigo_informacao
                  else
                   pedi_666.marca
                end as marca
                into v_colecao, v_marca
                from basi_030
                left join basi_400 basi_400 on basi_400.grupo = basi_030.referencia
                                             and basi_400.nivel = '1'
                                             and basi_400.tipo_informacao = 17
                left join basi_001 basi_001 on basi_001.grupo_produto =
                                                 basi_030.referencia
                                             and basi_001.nivel_produto = '1'
                left join pedi_666 pedi_666 on pedi_666.codigo_projeto =
                                                 basi_001.codigo_projeto
                where basi_030.nivel_estrutura = v_nivel
                and basi_030.referencia = v_grupo;
              exception
               when others then
                 v_marca   := 0;
                 v_colecao := 0;
             end;
             
              --Consulta se tem SQL cadastrado para ler descricao do item
               declare
                 cursor c_obrf_183 is
                   select obrf_183.codigo_sql, obrf_183.local_resultado
                     from obrf_183
                    where obrf_183.empresa = p_cod_empresa
                      and obrf_183.nivel = v_nivel
                      and (obrf_183.grupo = v_grupo or obrf_183.grupo is null)
                      and (obrf_183.subgrupo = v_subgrupo or obrf_183.subgrupo is null)
                      and (obrf_183.item = v_item or obrf_183.item is null)
                      and (obrf_183.colecao = v_colecao or obrf_183.colecao is null)
                      and (obrf_183.marca = v_marca or obrf_183.marca is null)
                    order by obrf_183.codigo_sql;

                 v_retorno_msg      varchar2(4000);

               begin
                 -- Loop para percorrer todos os registros encontrados na consulta
                
                 for rec in c_obrf_183 loop
                    select inter_fn_gera_msg_inf_prod(f_nfe.cod_empresa, f_nfe.num_nota_fiscal, f_nfe.serie_nota_fisc, v_nivel, v_grupo, v_subgrupo, v_item, v_marca, v_colecao, rec.codigo_sql)
                    into v_retorno_msg
                    from dual; 
                    

                    if rec.local_resultado = 0 then
                      w_inf_ad_prod := v_retorno_msg || ' ' || w_inf_ad_prod;
                    else
                      f_nfe_item.descricao_item := v_retorno_msg || ' ' || f_nfe_item.descricao_item;
                    end if;

                 end loop;

               exception
                 when others then
                   v_retorno_msg := '';
               end;
              
            end if;
         exception
           when others then
             v_marca   := 0;
             v_colecao := 0;
         end;
         
         -- SAIDA
          BEGIN
             insert into obrf_152
             (id
             ,nfe_id
             ,codigo
             ,cean
             ,descricao
             ,ncm
             ,extipi
             ,genero
             ,cfop
             ,unidade_comercial
             ,qtde_comercial
             ,valor_unit_comercial --12
             ,valor
             ,ceantrib
             ,unidade_tributavel
             ,qtde_tributavel
             ,vlr_unit_tributavel --17
             ,valor_frete
             ,valor_total_seguro
             ,valor_desconto
             ,valor_outros
             ,cod_empresa
             ,cod_usuario
             ,inf_ad_prod
             ,pedido_cliente
             ,seq_pedido_cliente
             ,numero_fci
             ,tp_inter_medio
             ,n_draw
             ,cest
             ,ind_ordem_produto
             ,cod_beneficio_fiscal
             ,perc_ipi_devol
             ,val_ipi_devol
             ,ch_nf_e
             ,perc_cred_presumido
             ,valor_cred_presumido
             ,cod_cbnef
             )
             values
             (v_obrf_152_id
             ,v_obrf_150_id
             ,f_nfe_item.produto
             ,v_cod_barras
             ,substr(inter_fn_retira_acentuacao(f_nfe_item.descricao_item),1,120)
             ,v_ncm
             ,decode(w_ex_ipi,0,null,w_ex_ipi)
             ,v_ncm_genero
             ,f_nfe_item.cod_natureza
             ,v_um_faturamento_um
             ,decode(f_nfe.tipo_nf_referenciada,3,0.0000,v_um_faturamento_qtde)
             ,decode(f_nfe.tipo_nf_referenciada,3,0.0000,v_valor_unit_comercial) --12
             ,f_nfe_item.valor_total
             ,v_cod_barras
             ,c_015_unidade_medida_trib
             ,decode(f_nfe.tipo_nf_referenciada,3,0.0000,c_015_quantidade_trib)
             ,decode(f_nfe.tipo_nf_referenciada,3,0.0000,v_vlr_unit_tributavel) --17
             ,decode(f_nfe_item.valor_frete, 0.00, null, f_nfe_item.valor_frete)
             ,decode(f_nfe_item.valor_seguro, 0.00, null, f_nfe_item.valor_seguro)
             ,decode(v_desconto_sem_suframa, 0.00, null, v_desconto_sem_suframa)
             ,decode(f_nfe_item.valor_outros, 0.00, null, f_nfe_item.valor_outros)
             ,p_cod_empresa
             ,p_cod_id
             ,w_inf_ad_prod
             ,substr(inter_fn_retira_acentuacao(f_nfe_item.pedido_cliente), 0, 14)
             ,f_nfe_item.seq_ped_compra
             ,v_numero_fci_aux
             ,1
             ,v_n_draw
             ,f_nfe_item.cest
             ,f_nfe_item.tipo_impressao
             ,v_codBeneficioFiscal
             ,v_percIpiDevol /* perc_Ipi_Devol*/
             ,v_valIpiDevol  /* val_Ipi_Devol*/
             ,decode(f_nfe_item.cod_natureza,'7501',v_numero_danfe_ref,'')
             ,f_nfe_item.perc_cred_presumido
             ,f_nfe_item.valor_cred_presumido
             ,decode(f_nfe_item.cvf_icms,0,f_nfe_item.cod_cbnef,null));

           if f_nfe_item.cvf_ipi_saida = 50 or f_nfe_item.cvf_ipi_saida = 51
           then v_base_ipi_saida := f_nfe_item.base_ipi;
           else v_base_ipi_saida := 0.00;
           end if;

           v_perc_reducao_140 := 0.00;

           begin
              select obrf_140.perc_reducao_icms into v_perc_reducao_140
              from obrf_140
              where obrf_140.classific_fiscal = f_nfe_item.classific_fiscal
                and obrf_140.natur_operacao   = f_nfe_item.natur_operacao
                and obrf_140.estado_natoper   = f_nfe_item.natopeno_est_oper
                and obrf_140.tipo             = 1
                and (obrf_140.tipo_cliente    = f_nfe.tipo_cliente or obrf_140.tipo_cliente = 999)
              order by obrf_140.tipo_cliente;
           exception
               when no_data_found then
                  v_perc_reducao_140 := 0.00;
           end;

           if v_perc_reducao_140 > 0.00
           then
              f_nfe_item.perc_reducao_icm := v_perc_reducao_140;
           end if;

           if f_nfe_item.valor_suframa_icms = 0
           then
              f_nfe_item.valor_suframa_icms := null;
           end if;

           if f_nfe_item.perc_fcp_uf_dest is null
           then
              f_nfe_item.perc_fcp_uf_dest := 0;
           end if;
           if f_nfe_item.perc_icms_uf_dest is null
           then
              f_nfe_item.perc_icms_uf_dest := 0;
           end if;
           if f_nfe_item.perc_icms is null
           then
              f_nfe_item.perc_icms := 0;
           end if;
           if f_nfe_item.perc_icms_partilha is null
           then
              f_nfe_item.perc_icms_partilha := 0;
           end if;
           if f_nfe_item.val_fcp_uf_dest is null
           then
              f_nfe_item.val_fcp_uf_dest := 0;
           end if;
           if f_nfe_item.val_icms_uf_dest is null
           then
             f_nfe_item.val_icms_uf_dest := 0;
           end if;
           if f_nfe_item.val_icms_uf_remet is null
           then
             f_nfe_item.val_icms_uf_remet := 0;
           end if;

           if f_nfe_item.pDif is null
           then
              f_nfe_item.pDif := 0;
           end if;

           if f_nfe_item.vICMSOp is null
           then
              f_nfe_item.vICMSOp := 0;
           end if;

           if f_nfe_item.vICMSDif is null
           then
              f_nfe_item.vICMSDif := 0;
           end if;

           if not v_difal
           then
              v_val_fcp_uf_ncm  := f_nfe_item.val_fcp_uf_dest;
              v_perc_fcp_uf_ncm := f_nfe_item.perc_fcp_uf_dest;
              v_bc_fcp          := f_nfe_item.base_icms;

              v_bc_fcp_uf_dest            := 0;
              v_fcp_uf_dest               := 0;
              v_bc_uf_dest                := 0;
              p_fcp_uf_dest               := 0;
           else
              v_bc_fcp_uf_dest  := f_nfe_item.base_icms;
              v_fcp_uf_dest     := f_nfe_item.val_fcp_uf_dest;
              v_bc_uf_dest      := f_nfe_item.base_icms;
              p_fcp_uf_dest     := f_nfe_item.perc_fcp_uf_dest;
              v_val_fcp_uf_ncm  := 0;
              v_perc_fcp_uf_ncm := 0;
              v_bc_fcp          := 0;

           end if;


           /* SAIDAS*/

           BEGIN
               insert into obrf_157
               (produto_id         /*1*/
               ,icms_orig          /*2*/
               ,icms_cst           /*3*/
               ,icms_modbc         /*4*/
               ,icms_predbc        /*5*/
               ,icms_vbc           /*6*/
               ,icms_picms         /*7*/
               ,icms_vicms         /*8*/
               ,icms_modbcst       /*9*/
               ,icms_pmvast        /*10*/
               ,icms_predbcst      /*11*/
               ,icms_vbcst         /*12*/
               ,icms_picmsst       /*13*/
               ,icms_vicmsst       /*14*/
               ,ipi_cienq          /*15*/
               ,ipi_cnpjprod       /*16*/
               ,ipi_cselo          /*17*/
               ,ipi_qselo          /*18*/
               ,ipi_cenq           /*19*/
               ,ipi_cst            /*20*/
               ,ipi_vbc            /*21*/
               ,ipi_qunid          /*22*/
               ,ipi_vunid          /*23*/
               ,ipi_pipi           /*24*/
               ,ipi_vipi           /*25*/
               ,ii_vbc             /*26*/
               ,ii_vdespadu        /*27*/
               ,ii_vii             /*28*/
               ,ii_viof            /*29*/
               ,pis_cst            /*30*/
               ,pis_vbc            /*31*/
               ,pis_ppis           /*32*/
               ,pis_vpis           /*33*/
               ,pis_qbcprod        /*34*/
               ,pis_valiqprod      /*35*/
               ,pisst_vbc          /*36*/
               ,pisst_ppis         /*37*/
               ,pisst_qbcprod      /*38*/
               ,pisst_valiqprod    /*39*/
               ,pisst_vpis         /*40*/
               ,cofins_cst         /*41*/
               ,cofins_vbc         /*42*/
               ,cofins_pcofins     /*43*/
               ,cofins_vcofins     /*44*/
               ,cofins_qbcprod     /*45*/
               ,cofins_valiqprod   /*46*/
               ,cofinsst_vbc       /*47*/
               ,cofinsst_pcofins   /*48*/
               ,cofinsst_qbcprod   /*49*/
               ,cofinsst_valiqprod /*50*/
               ,cofinsst_vcofins   /*51*/
               ,issqn_vbc          /*52*/
               ,issqn_valiq        /*53*/
               ,issqn_vissqn       /*54*/
               ,issqn_cod_mun      /*55*/
               ,issqn_clistserv    /*56*/
               ,cod_empresa        /*57*/
               ,cod_usuario        /*58*/
               ,cod_csosn          /*59*/
               ,v_icms_deson       /*60*/
               ,mot_des_icms       /*61*/
               ,vbcufdest          /*62*/
               ,pfcpufdest         /*63*/
               ,picmsufdest        /*64*/
               ,picmsinter         /*65*/
               ,picmsinterpart     /*66*/
               ,vfcpufdest         /*67*/
               ,vicmsufdest        /*68*/
               ,vicmsufremet       /*69*/
               ,pdif51             /*70*/
               ,vicmsop51          /*71*/
               ,vicmsdif51         /*72*/
               ,vbcfcpufdest       /*73*/
               ,vfcpst             /*74*/
               ,vfcp               /*75*/
               ,vfcpstret          /*76*/
               ,pfcp               /*77*/
               ,vbcfcp             /*78*/
               ,valor_icms_substituto   /*79*/
               )
               values
               (v_obrf_152_id                               /*1*/
               ,f_nfe_item.procedencia                      /*2*/
               ,f_nfe_item.cvf_icms                         /*3*/
               ,'3'                                         /*4*/
               ,decode(f_nfe_item.pDif,100.00,null,0,decode(f_nfe_item.perc_reducao_icm,0.00,null,f_nfe_item.perc_reducao_icm), null)  /*5*/
               ,decode(f_nfe_item.pDif,100.00,null,f_nfe_item.base_icms)    /*6*/
               ,decode(f_nfe_item.pDif,100.00,null,f_nfe_item.perc_icms)    /*7*/
               ,decode(f_nfe_item.pDif,100.00,null,f_nfe_item.valor_icms)   /*8*/
               ,0                                            /*9*/
               ,null                                         /*10*/
               ,null                                         /*11*/
--               ,f_nfe_item.valor_icms_difer                  /*12*/
               ,f_nfe_item.base_icms_difer                   /*12*/
               ,f_nfe_item.perc_substituica                  /*13*/
--               ,f_nfe_item.valor_icms                        /*14*/
--               ,((f_nfe_item.base_icms_difer * f_nfe_item.perc_substituica)/100)                  /*14 (SS:65083.001 210-rafaels) */
               ,f_nfe_item.valor_icms_difer                  /*14*/
               ,null                                         /*15*/
               ,null                                         /*16*/
               ,null                                         /*17*/
               ,null                                         /*18*/
               ,decode(f_nfe.tipo_nf_referenciada,3,null,w_cod_enq_ipi)                                /*19*/
               ,decode(f_nfe.tipo_nf_referenciada,3,null,f_nfe_item.cvf_ipi_saida)                     /*20*/
               ,decode(f_nfe.tipo_nf_referenciada,3,null,v_base_ipi_saida)                            /*21*/
               ,null                                         /*22*/
               ,null                                         /*23*/
               ,decode(f_nfe.tipo_nf_referenciada,3,null,f_nfe_item.perc_ipi)                          /*24*/
               ,decode(f_nfe.tipo_nf_referenciada,3,null,f_nfe_item.valor_ipi)                         /*25*/
               ,null                                         /*26*/
               ,null                                         /*27*/
               ,null                                         /*28*/
               ,null                                         /*29*/
               ,decode(f_nfe_item.cvf_pis,0,99,f_nfe_item.cvf_pis)                           /*30*/
               ,f_nfe_item.basi_pis_cofins                   /*31*/
               ,f_nfe_item.perc_pis                          /*32*/
               ,f_nfe_item.valor_pis                         /*33*/
               ,null                        /*34*/
               ,null                         /*35*/
               ,null                   /*36*/
               ,null                          /*37*/
               ,null                                         /*38*/
               ,null                                         /*39*/
               ,null                         /*40*/
               ,decode(f_nfe_item.cvf_cofins,0,99,f_nfe_item.cvf_cofins)           /*41*/
               ,f_nfe_item.basi_pis_cofins                   /*42*/
               ,f_nfe_item.perc_cofins                       /*43*/
               ,f_nfe_item.valor_cofins                      /*44*/
               ,null                      /*45*/
               ,null                      /*46*/
               ,null                   /*47*/
               ,null                       /*48*/
               ,null                                         /*49*/
               ,null                                         /*50*/
               ,null                      /*51*/
               ,null                                         /*52*/
               ,null                                         /*53*/
               ,null                                         /*54*/
               ,null                                         /*55*/
               ,null                                         /*56*/
               ,p_cod_empresa                                /*57*/
               ,p_cod_id                                     /*58*/
               ,f_nfe_item.cod_csosn                         /*59*/
               ,v_valor_icms_deson                                                 /*60*/
               ,v_motivo_deson                                                     /*61*/
               ,decode(f_nfe_item.valor_icms, 0,null,v_bc_uf_dest)                 /*62 vbcufdest*/
               ,decode(f_nfe_item.valor_icms, 0,null,p_fcp_uf_dest)                /*63 pfcpufdest */
               ,decode(f_nfe_item.valor_icms, 0,null,f_nfe_item.perc_icms_uf_dest) /*64 picmsufdest*/
               ,decode(f_nfe_item.valor_icms, 0,null,f_nfe_item.perc_icms)         /*65 picmsinter*/
               ,decode(f_nfe_item.valor_icms, 0,null,f_nfe_item.perc_icms_partilha)/*66 picmsinterpart*/
               ,decode(f_nfe_item.valor_icms, 0,null,v_fcp_uf_dest)                /*67 vfcpufdest*/
               ,decode(f_nfe_item.valor_icms, 0,null,f_nfe_item.val_icms_uf_dest)  /*68 vicmsufdest*/
               ,decode(f_nfe_item.valor_icms, 0,null,f_nfe_item.val_icms_uf_remet) /*69 vicmsufremet*/
               ,decode(f_nfe_item.pDif,100.00,null,0.00,null,f_nfe_item.pDif)      /*70 pdif51*/
               ,decode(f_nfe_item.pDif,100.00,null,0.00,null,f_nfe_item.vICMSOp)   /*71 vicmsop51*/
               ,decode(f_nfe_item.pDif,100.00,null,0.00,null,f_nfe_item.vICMSDif)  /*72 vicmsdif51*/
               ,decode(f_nfe_item.valor_icms, 0,null,v_bc_fcp_uf_dest)             /*73 vbcfcpufdest*/
               ,null                                                                 /*74 vfcpst */
               ,decode(v_perc_fcp_uf_ncm,0,null,v_val_fcp_uf_ncm)                    /*75 vfcp */
               ,null                                                                 /*76 vfcpstret*/
               ,decode(v_perc_fcp_uf_ncm,0,null,v_perc_fcp_uf_ncm)                   /*77 pfcp */
               ,decode(v_perc_fcp_uf_ncm,0,null,v_bc_fcp)                            /*78 pBcFcp */
         ,decode(f_nfe_item.valor_icms_substituto,null,0.00,f_nfe_item.valor_icms_substituto)  /*79*/
               );

            EXCEPTION
               WHEN OTHERS THEN
                  p_msg_erro := 'N?o inseriu dados na tabela obrf_157' || Chr(10) || SQLERRM;

            END;

          EXCEPTION
             WHEN OTHERS THEN
                p_msg_erro := 'N?o inseriu dados na tabela obrf_152' || Chr(10) || SQLERRM;

          END;

       END LOOP;

       begin
           select count(*), sum(f.valor_duplicata) into v_cont_dupli, v_valor_dup from fatu_070 f
           where f.codigo_empresa   = p_cod_empresa
             and f.num_duplicata    = f_nfe.num_nota_fiscal
             and f.cli_dup_cgc_cli9 = f_nfe.cgc_9
             and f.cli_dup_cgc_cli4 = f_nfe.cgc_4
             and f.cli_dup_cgc_cli2 = f_nfe.cgc_2
             and f.tipo_titulo      = f_nfe.tipo_titulo
             and f.serie_nota_fisc  = f_nfe.serie_nota_fisc
             and (exists (select 1 from loja_010
                where (loja_010.forma_pgto      = f.cod_forma_pagto)
                and (loja_010.forma_pagto_nfe = 14 or loja_010.forma_pagto_nfe = 0)) or f.cod_forma_pagto = 0);
           exception
           when no_data_found then
              v_cont_dupli := 0;
       end;

       if v_valor_dup is null
       then
          v_valor_dup := 0;
       end if;

       begin
          select count(*), sum(f.valor_duplicata)   into v_cont_dupli_aux, v_valor_dup_aux from fatu_070 f
          where f.codigo_empresa   = p_cod_empresa
            and f.num_nota_fiscal  = f_nfe.num_nota_fiscal
            and f.cli_dup_cgc_cli9 = f_nfe.cgc_9
            and f.cli_dup_cgc_cli4 = f_nfe.cgc_4
            and f.cli_dup_cgc_cli2 = f_nfe.cgc_2
            and f.serie_nota_fisc  = f_nfe.serie_nota_fisc
            and f.titulo_loja      = 1
            and f.loja_fat        <> 1
            and (exists (select 1 from loja_010
                         where (loja_010.forma_pgto      = f.cod_forma_pagto)
                           and (loja_010.forma_pagto_nfe = 14 or loja_010.forma_pagto_nfe = 0)) or f.cod_forma_pagto = 0)
            and not exists (select 1 from fatu_070 f
                            where f.codigo_empresa   = p_cod_empresa
                              and f.num_duplicata    = f_nfe.num_nota_fiscal
                              and f.cli_dup_cgc_cli9 = f_nfe.cgc_9
                              and f.cli_dup_cgc_cli4 = f_nfe.cgc_4
                              and f.cli_dup_cgc_cli2 = f_nfe.cgc_2
                              and f.tipo_titulo      = f_nfe.tipo_titulo
                              and f.serie_nota_fisc  = f_nfe.serie_nota_fisc
                              and (exists (select 1 from loja_010
                       where (loja_010.forma_pgto      = f.cod_forma_pagto)
                         and (loja_010.forma_pagto_nfe = 14 or loja_010.forma_pagto_nfe = 0)) or f.cod_forma_pagto = 0));
       exception
           when no_data_found then
              v_cont_dupli_aux := 0;
       end;

       if v_valor_dup_aux is null
       then
          v_valor_dup_aux  := 0;
       end if;

       v_cont_dupli := v_cont_dupli + v_cont_dupli_aux;

       if f_nfe.origem_nota <> 3 and v_valor_dup > 0
       then
          v_val_sem_pagto := f_nfe.total_docto - v_valor_dup;
       else
          v_val_sem_pagto := 0;
       end if;

       if v_cont_dupli > 0
       then

          BEGIN
             select seq_obrf_156.nextval into v_obrf_156_id from dual;
             insert into obrf_156
            (id
            ,cod_empresa
            ,cod_usuario
            ,nfe_id
            ,nfat
            ,vorig
            ,vdesc
            ,vliq)
            values
            (v_obrf_156_id
            ,p_cod_empresa
            ,p_cod_id
            ,v_obrf_150_id
            ,f_nfe.num_nota_fiscal
            ,v_valor_dup + f_nfe.valor_desconto --f_nfe.total_docto + f_nfe.valor_desconto - v_val_sem_pagto
            ,decode(v_versao_layout_nfe, '4.00',f_nfe.valor_desconto,decode(f_nfe.valor_desconto,0,null,f_nfe.valor_desconto))
            ,v_valor_dup --f_nfe.total_docto - v_val_sem_pagto
            );
          EXCEPTION
             WHEN OTHERS THEN
                p_msg_erro := 'N?o inseriu dados na tabela obrf_156' || Chr(10) || SQLERRM;

          END;

       end if;

       /* FORMAS DE PAGAMENTO SA?DA*/

       select obrf_150.id
       into   v_obrf_150_id
       from obrf_150
       where obrf_150.cod_empresa = p_cod_empresa
         and obrf_150.numero      = f_nfe.num_nota_fiscal
         and obrf_150.serie       = f_nfe.serie_nota_fisc
         and obrf_150.cod_usuario = p_cod_id
         and rownum               = 1
       order by obrf_150.id desc;

       if f_nfe.origem_nota <> 3 and v_valor_dup = 0
       then
          -- PROCURA VALOR TOTAL DAS DUPLICATAS
          select sum(f.valor_duplicata) into v_valor_dup from fatu_070 f
           where f.codigo_empresa   = p_cod_empresa
             and f.num_duplicata    = f_nfe.num_nota_fiscal
             and f.cli_dup_cgc_cli9 = f_nfe.cgc_9
             and f.cli_dup_cgc_cli4 = f_nfe.cgc_4
             and f.cli_dup_cgc_cli2 = f_nfe.cgc_2
             and f.tipo_titulo      = f_nfe.tipo_titulo
             and f.serie_nota_fisc  = f_nfe.serie_nota_fisc;
          if v_valor_dup is null
          then
             v_valor_dup := 0;
          end if;

          if v_valor_dup > 0
          then
             -- ACHA DIFEREN?A ENTRE O VALOR TOTAL DAS DUPLICATAS E TOTAL DA NOTA
             v_val_sem_pagto := f_nfe.total_docto  - v_valor_dup;
          else
             v_val_sem_pagto := 0;
          end if;
       end if;

       --INICIO DA ALTERACAO PARA CORRIGIR VALOR DAS PARCELAS DO LOJA.
       if f_nfe.origem_nota = 3
       then
         if f_nfe.percentual_negociado = 0 or f_nfe.percentual_negociado = 100
         then
            BEGIN
               update loja_075
               set loja_075.valor_parc_nota_xml = loja_075.valor_parc
               where loja_075.cod_empresa       = p_cod_empresa
                 and loja_075.documento         = f_nfe.nr_solicitacao;
            EXCEPTION
            WHEN OTHERS THEN
               p_msg_erro := 'Nao atualizou valor_parc_nota_xml na tabela loja_075. ' || Chr(10) || SQLERRM;
            END;
         else if f_nfe.percentual_negociado <> 0 and f_nfe.percentual_negociado <> 100
            then
              for reg_parc_orc in (
                select loja_075.sequencia, loja_075.valor_parc,
                       loja_075.condicao_pagto
                from loja_075
                where loja_075.cod_empresa = p_cod_empresa
                  and loja_075.documento   = f_nfe.nr_solicitacao)
              loop
                 v_valor_percela_orcamento := reg_parc_orc.valor_parc * (f_nfe.percentual_negociado / 100);
                 BEGIN
                   update loja_075
                   set loja_075.valor_parc_nota_xml = v_valor_percela_orcamento
                   where loja_075.cod_empresa       = p_cod_empresa
                     and loja_075.documento         = f_nfe.nr_solicitacao
                     and loja_075.sequencia         = reg_parc_orc.sequencia
                     and loja_075.condicao_pagto    = reg_parc_orc.condicao_pagto;
                 EXCEPTION
                 WHEN OTHERS THEN
                   p_msg_erro := 'Nao atualizou valor_parc_nota_xml na tabela loja_075. ' || Chr(10) || SQLERRM;
                 END;
              end loop;
            end if;
         end if;

         select sum(loja_075.valor_parc_nota_xml)
         into valor_parcelas_orcamento
         from loja_075
         where loja_075.cod_empresa  = p_cod_empresa
           and loja_075.documento    = f_nfe.nr_solicitacao;
         if valor_parcelas_orcamento is null
         then
            valor_parcelas_orcamento := 0;
         end if;

         if f_nfe.total_docto <> valor_parcelas_orcamento
         then
            diff_troco := valor_parcelas_orcamento - f_nfe.total_docto;
            BEGIN
              UPDATE LOJA_075
              SET valor_parc_nota_xml = valor_parc_nota_xml - diff_troco
              where loja_075.cod_empresa  = p_cod_empresa
                and loja_075.documento    = f_nfe.nr_solicitacao
             and loja_075.sequencia    = (select min(sequencia) from loja_075
                                          where loja_075.cod_empresa  = p_cod_empresa
                                            and loja_075.documento    = f_nfe.nr_solicitacao);
            EXCEPTION
            WHEN OTHERS THEN
               p_msg_erro := 'Nao atualizou dados na tabela loja_075. ' || Chr(10) || SQLERRM;
            END;
         end if;
       end if;
       --FIM DA ALTERACAO PARA CORRIGIR VALOR DAS PARCELAS DO LOJA.
       
       --GERA DADOS PARA O TAG autXML
       for aut_xml in (select lpad(cgc_9, 8, '0') || lpad(cgc_4, 4, '0') || lpad(cgc_2, 2, '0') as cgc
                       from pedi_232 
                       where cgc_9_relacionado = f_nfe.f_cgc_9
                         and cgc_4_relacionado = f_nfe.f_cgc_4
                         and cgc_2_relacionado = f_nfe.f_cgc_2
                         and tipo = 1
                       union 
                       select lpad(cgc_9, 8, '0') || lpad(cgc_4, 4, '0') || lpad(cgc_2, 2, '0') as cgc
                       from pedi_232 
                       where empresa = f_nfe.cod_empresa
                         and tipo = 2)
       loop
            insert into obrf_166 (nfe_id, cod_empresa, cod_usuario, cgc)
            values(v_obrf_150_id, p_cod_empresa, p_cod_id, aut_xml.cgc);
       end loop;

       v_eh_antecipado := 0;
       
       begin
         select 1
         into v_eh_antecipado
         from fatu_070
         where fatu_070.codigo_empresa   = p_cod_empresa
           and fatu_070.cli_dup_cgc_cli9 = f_nfe.cgc_9
           and fatu_070.cli_dup_cgc_cli4 = f_nfe.cgc_4
           and fatu_070.cli_dup_cgc_cli2 = f_nfe.cgc_2
           and fatu_070.num_duplicata    = f_nfe.num_nota_fiscal
           and fatu_070.tipo_titulo      = f_nfe.tipo_titulo
           and fatu_070.data_venc_duplic > f_nfe.data_saida
           and rownum = 1 ;
       exception 
       when no_data_found 
         then v_eh_antecipado := 0;
       end;
       
       begin
         select pedido_venda
         into v_pedido_antec 
         from fatu_050 
         where fatu_050.codigo_empresa  = p_cod_empresa
           and fatu_050.num_nota_fiscal = f_nfe.num_nota_fiscal
           and fatu_050.serie_nota_fisc = f_nfe.serie_nota_fisc
           and rownum = 1 ;
       exception 
       when no_data_found 
         then v_pedido_antec := 0;  
       end;
    
       for reg_forma_pgto in (
          select cod_forma_pagto,           sum(valor_parc) valor_parc,
                 nr_autorizacao_opera
          from (
             select loja_075.forma_pgto cod_forma_pagto, loja_075.valor_parc_nota_xml valor_parc,
                    loja_075.nr_autorizacao_opera
             from loja_075
             where loja_075.cod_empresa  = p_cod_empresa
               and loja_075.documento    = f_nfe.nr_solicitacao
               and f_nfe.origem_nota     = 3 /*NOTA DE LOJA*/
             UNION ALL
             -- LER DIRETO PARA QUE SE NAO GEROU DUPLICATA, PEGA O VALOR DA NOTA
             select f_nfe.cod_forma_pagto cod_forma_pagto, f_nfe.total_docto - v_val_sem_pagto valor_parc,
                    f_nfe.nr_autorizacao_opera nr_autorizacao_opera
             from dual
             where f_nfe.origem_nota    <> 3 /*NOTA DE LOJA*/
             UNION ALL
             select 9999 cod_forma_pagto,  v_val_sem_pagto valor_parc,
                    '' nr_autorizacao_opera
             from dual
             where f_nfe.origem_nota    <> 3 /*NOTA DE LOJA*/
               and v_val_sem_pagto       > 0)
          group by cod_forma_pagto, nr_autorizacao_opera
          order by cod_forma_pagto)
       loop
          begin
             select loja_010.cnpj9_operadora_cartao, loja_010.cnpj4_operadora_cartao, loja_010.cnpj2_operadora_cartao,
                    loja_010.forma_pagto_nfe,        loja_010.band_operadora,         loja_010.descricao,
                    loja_010.cod_empresa,            loja_010.ident_terminal
             into   v_cnpj9_operadora_cartao,        v_cnpj4_operadora_cartao,        v_cnpj2_operadora_cartao,
                    v_forma_pagto_nfe,               v_band_operadora,                v_desc_forma_pgto,
                    v_cod_empresa_loja_010,          v_ident_terminal
             from loja_010
             where loja_010.forma_pgto = reg_forma_pgto.cod_forma_pagto
               and reg_forma_pgto.valor_parc > 0;
          exception
             when no_data_found then
             v_cnpj9_operadora_cartao := 0;
             v_cnpj4_operadora_cartao := 0;
             v_cnpj2_operadora_cartao := 0;
             v_forma_pagto_nfe        := 0;
             v_band_operadora         := 0;
             v_cod_empresa_loja_010   := 0;
             v_ident_terminal         := '';
          end;

          if reg_forma_pgto.cod_forma_pagto = 9999 and reg_forma_pgto.valor_parc > 0
          then
             v_forma_pagto_nfe     := 90;
          end if;

          if v_forma_pagto_nfe = 0
          then
             begin
                select 14 into v_forma_pagto_nfe from fatu_070
                where fatu_070.num_duplicata    = f_nfe.num_nota_fiscal
                  and fatu_070.tipo_titulo      = (select fatu_505.tipo_titulo from fatu_505 where fatu_505.codigo_empresa = p_cod_empresa and fatu_505.serie_nota_fisc = f_nfe.serie_nota_fisc)
                  and fatu_070.cli_dup_cgc_cli9 = f_nfe.cgc_9
                  and fatu_070.cli_dup_cgc_cli4 = f_nfe.cgc_4
                  and fatu_070.cli_dup_cgc_cli2 = f_nfe.cgc_2
                  and fatu_070.codigo_empresa   = p_cod_empresa
                  and f_nfe.tipo_nf_referenciada <> 4
                  and rownum = 1;
             exception
                 when no_data_found then
                 v_forma_pagto_nfe := 90;
             end;
          end if;

          begin
             select count(*) into v_indPgto from obrf_164
             where obrf_164.id_nfe         = v_obrf_150_id
               and obrf_164.tipo_pagamento = v_forma_pagto_nfe;
          end;

          if v_forma_pagto_nfe = 90
          then
             reg_forma_pgto.valor_parc := 0;
          end if;

          if v_indPgto = 0
          then
             begin
               select fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2
               into v_receb_cgc9, v_receb_cgc4, v_receb_cgc2
               from fatu_500
               where fatu_500.codigo_empresa = v_cod_empresa_loja_010;
             exception when no_data_found
             then
               v_receb_cgc9 := 0;
               v_receb_cgc4 := 0;
               v_receb_cgc2 := 0;
             end;

             v_avista := 1;
             
             if v_forma_pagto_nfe = 3 or v_forma_pagto_nfe = 4
               or (v_eh_antecipado = 1 and (v_forma_pagto_nfe = 1 
                or v_forma_pagto_nfe = 15 or v_forma_pagto_nfe = 17))
             then
               v_avista := 0;
             end if;

             v_d_pag := null;
             
             if v_forma_pagto_nfe = 3 or v_forma_pagto_nfe = 4
               or v_forma_pagto_nfe = 17 
               or (v_eh_antecipado = 1 or v_forma_pagto_nfe = 15)
             then
               if v_pedido_antec > 0
               then
                 begin
                   select data_autorizacao, BANDEIRA_CARTAO
                   into v_d_pag, v_band_operadora_pedi
                   from pedi_100
                   where pedi_100.pedido_venda = v_pedido_antec;
                 exception
                 when no_data_found
                   then v_d_pag := null;
                 end;

                 if v_forma_pagto_nfe = 3 and trim(v_band_operadora_pedi) is not null
                 then
                     v_band_operadora := CASE v_band_operadora_pedi
                         WHEN '1' THEN 2   -- Master
                         WHEN '2' THEN 5   -- Dinners
                         WHEN '3' THEN 1   -- Visa
                         WHEN '4' THEN 9   -- Cabal
                         WHEN '5' THEN 7   -- Hypercard
                         WHEN '6' THEN 4   -- Sorocred
                         WHEN '7' THEN 99  -- CUP
                         WHEN '8' THEN 99  -- Credsystem
                         WHEN '9' THEN 99  -- Sicredi
                         WHEN 'A' THEN 99  -- AVISTA
                         WHEN 'E' THEN 6   -- ELO
                         WHEN 'B' THEN 11  -- BANESCARD
                         WHEN 'J' THEN 18  -- JCB
                         WHEN 'X' THEN 3   -- AMEX
                         WHEN 'Z' THEN 13  -- CREDZ
                         ELSE 99           -- Valor padri??o se ni??o corresponder a nenhum dos valores acima
                   END;
                 end if;
               end if;
             end if;

             /*SAIDA*/
             BEGIN
                insert into obrf_164
                  (id_nfe
                   ,tipo_pagamento
                   ,valor_pagamento
                   ,valor_troco

                   ,bandeira_cartao
                   ,cnpj9_card
                   ,cnpj4_card
                   ,cnpj2_card

                   ,tipo_integracao
                   ,num_autoriza_card
                   ,a_vista_a_prazo
                   ,desc_forma_pgto
                   ,receb_cgc9
                   ,receb_cgc4
                   ,receb_cgc2
                   ,ident_terminal
                   ,d_pag)
               values
                 (v_obrf_150_id
                 ,v_forma_pagto_nfe
                 ,reg_forma_pgto.valor_parc
                 ,0

                 ,v_band_operadora
                 ,v_cnpj9_operadora_cartao
                 ,v_cnpj4_operadora_cartao
                 ,v_cnpj2_operadora_cartao

                 ,2 /*pagamento nao integrado com o sistema*/
                 ,reg_forma_pgto.nr_autorizacao_opera
                 ,v_avista
                 ,v_desc_forma_pgto
                 ,v_receb_cgc9
                 ,v_receb_cgc4
                 ,v_receb_cgc2
                 ,v_ident_terminal
                 ,v_d_pag);

             EXCEPTION
             WHEN OTHERS THEN
                p_msg_erro := 'N?o inseriu dados na tabela obrf_164' || Chr(10) || SQLERRM;
             END;
          else
             begin
                update obrf_164
                set obrf_164.valor_pagamento = obrf_164.valor_pagamento + reg_forma_pgto.valor_parc
                where obrf_164.id_nfe         = v_obrf_150_id
                  and obrf_164.tipo_pagamento = v_forma_pagto_nfe;
             end;
          end if;
       end loop; /*fim for reg_forma_pgto*/

       v_seq_duplicata := 0;

       for f_nfe_dup in c_NFt(p_cod_empresa, f_nfe.num_nota_fiscal, f_nfe.serie_nota_fisc,f_nfe.cgc_9,f_nfe.cgc_4,f_nfe.cgc_2,f_nfe.tipo_titulo)
       LOOP
         BEGIN

           v_seq_duplicata := v_seq_duplicata + 1;

           select seq_obrf_155.nextval into v_obrf_155_id from dual;

           insert into obrf_155
           (id
           ,cod_empresa
           ,cod_usuario
           ,cobranca_id
           ,ndup
           ,dvenc
           ,vdup)
           values
           (v_obrf_155_id
           ,p_cod_empresa
           ,p_cod_id
           ,v_obrf_156_id
           ,to_char(v_seq_duplicata,'000') --to_char(f_nfe_dup.num_duplicata,'000000') || '-' || to_char(f_nfe_dup.seq_duplicatas,'00')
           ,f_nfe_dup.data_venc_duplic
           ,f_nfe_dup.valor_duplicata);
         EXCEPTION
            WHEN OTHERS THEN
               p_msg_erro := 'N?o inseriu dados na tabela obrf_155' || Chr(10) || SQLERRM;

         END;

       END LOOP;

    END LOOP;

  end if;

  commit;

end p_gera_NFe;
/
