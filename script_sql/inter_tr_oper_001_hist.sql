
  CREATE OR REPLACE TRIGGER "INTER_TR_OPER_001_HIST" 
-- TABELA DE HISTORICO DE PARAMETROS DE RELATORIO: OPER_001

BEFORE DELETE OR INSERT
or update of tp_reg_relatorio, nome_usuario, codigo_usuario, locale,
	classe, data_execucao, data_fim_execucao, status_processo,
	status_leitura, msg_exception, campo_146, campo_147,
	campo_148, campo_149, campo_150, campo_39,
	campo_42, campo_45, campo_48, campo_51,
	campo_54, campo_57, campo_60, campo_63,
	campo_66, campo_69, campo_72, campo_75,
	campo_78, campo_82, campo_85, campo_88,
	campo_91, campo_94, campo_97, campo_100,
	campo_103, campo_106, campo_110, campo_113,
	campo_116, campo_119, campo_123, campo_126,
	campo_129, campo_132, campo_135, campo_139,
	campo_13, campo_14, campo_15, campo_16,
	campo_17, campo_18, campo_19, campo_20,
	campo_21, campo_22, campo_23, campo_24,
	campo_25, campo_26, campo_27, campo_28,
	campo_29, campo_30, campo_31, campo_32,
	campo_33, campo_34, campo_35, campo_36,
	campo_37, campo_38, campo_40, campo_41,
	campo_43, campo_44, campo_46, campo_47,
	campo_49, campo_50, campo_52, campo_53,
	campo_55, campo_56, campo_58, campo_59,
	campo_61, campo_62, campo_64, campo_65,
	campo_67, campo_68, campo_70, campo_71,
	campo_73, campo_74, campo_76, campo_77,
	campo_79, campo_80, campo_81, campo_83,
	campo_84, campo_86, campo_87, campo_89,
	campo_90, campo_92, campo_93, campo_95,
	campo_96, campo_98, campo_99, campo_101,
	campo_102, campo_104, campo_105, campo_107,
	campo_108, campo_109, campo_111, campo_112,
	campo_114, campo_115, campo_117, campo_118,
	campo_120, campo_121, campo_122, campo_124,
	campo_125, campo_127, campo_128, campo_130,
	campo_131, campo_133, campo_134, campo_136,
	campo_137, campo_138, campo_140, campo_141,
	campo_142, campo_143, campo_144, campo_145,
	nr_solicitacao, codigo_relatorio, solicitante, data_solicitacao,
	codigo_empresa, observacoes, situacao, campo_01,
	campo_02, campo_03, campo_04, campo_05,
	campo_06, campo_07, campo_08, campo_09,
	campo_10, campo_11, campo_12
on oper_001
for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_area_producao          number(1);
   ws_ordem_producao         number(9);
   ws_periodo_producao       number(4);
   ws_ordem_confeccao        number(2);
   ws_tipo_historico         number(1);

   ws_descricao_historico    varchar2(4000);
   ws_tipo_ocorr             varchar2(1);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if INSERTING then
      ws_area_producao         := 0;
      ws_ordem_producao        := :new.nr_solicitacao;
      ws_periodo_producao      := :new.codigo_empresa;
      ws_ordem_confeccao       := 0;
      ws_tipo_historico        := 0;
      ws_descricao_historico   := inter_fn_buscar_tag('lb34855#INCLUSAO PARAMETRO (OPER_001)',ws_locale_usuario,ws_usuario_systextil) || '  ' || chr(10)
                                ||inter_fn_buscar_tag('lb00247#NR. SOLICITACAO:',ws_locale_usuario,ws_usuario_systextil) || '  ' || :new.nr_solicitacao || chr(10)
                                ||inter_fn_buscar_tag('lb12088#SOLICITANTE:',ws_locale_usuario,ws_usuario_systextil) || '  ' || :new.solicitante || chr(10)
                                ||inter_fn_buscar_tag('lb34850#DATA SOLICITACAO:',ws_locale_usuario,ws_usuario_systextil) || '  ' || :new.data_solicitacao || chr(10)
                                ||inter_fn_buscar_tag('lb00127#CODIGO EMPRESA:',ws_locale_usuario,ws_usuario_systextil) || '  ' || :new.codigo_empresa || chr(10)
                                ||inter_fn_buscar_tag('lb03958#OBSERVACAO:',ws_locale_usuario,ws_usuario_systextil) || '  ' || :new.observacoes || chr(10);
      ws_tipo_ocorr            := 'I';
   elsif DELETING then
         ws_area_producao         := 0;
         ws_ordem_producao        := :old.nr_solicitacao;
         ws_periodo_producao      := :old.codigo_empresa;
         ws_ordem_confeccao       := 0;
         ws_tipo_historico        := 0;
         ws_descricao_historico   := inter_fn_buscar_tag('lb34857#EXCUSAO PARAMETRO (OPER_001)',ws_locale_usuario,ws_usuario_systextil) || '  ' || chr(10)
                                   ||inter_fn_buscar_tag('lb00247#NR. SOLICITACAO:',ws_locale_usuario,ws_usuario_systextil) || '  ' || :old.nr_solicitacao || chr(10)
                                   ||inter_fn_buscar_tag('lb12088#SOLICITANTE:',ws_locale_usuario,ws_usuario_systextil) || '  ' || :old.solicitante || chr(10)
                                   ||inter_fn_buscar_tag('lb34850#DATA SOLICITACAO:',ws_locale_usuario,ws_usuario_systextil) || '  ' || :old.data_solicitacao || chr(10)
                                   ||inter_fn_buscar_tag('lb00127#CODIGO EMPRESA:',ws_locale_usuario,ws_usuario_systextil) || '  ' || :old.codigo_empresa || chr(10)
                                   ||inter_fn_buscar_tag('lb03958#OBSERVACAO:',ws_locale_usuario,ws_usuario_systextil) || '  ' || :old.observacoes || chr(10);
         ws_tipo_ocorr            := 'D';
   end if;

   INSERT INTO hist_010
     (area_producao,           ordem_producao,          periodo_producao,
      ordem_confeccao,         tipo_historico,          descricao_historico,
      tipo_ocorr,              data_ocorr,              hora_ocorr,
      usuario_rede,            maquina_rede,            aplicacao,
      usuario_sistema)
   VALUES
     (ws_area_producao,        ws_ordem_producao,       ws_periodo_producao,
      ws_ordem_confeccao,      ws_tipo_historico,       ws_descricao_historico,
      ws_tipo_ocorr,           sysdate,                 sysdate,
      ws_usuario_rede,         ws_maquina_rede,         ws_aplicativo,
      ws_usuario_systextil);

end inter_tr_oper_001_hist;

-- ALTER TRIGGER "INTER_TR_OPER_001_HIST" DISABLE
 

/

exec inter_pr_recompile;

