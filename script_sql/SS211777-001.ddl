alter table inte_100 add (cod_forma_pagto  number(2));

comment on COLUMN inte_100.cod_forma_pagto is 'Indica a forma de pagamento';

alter table inte_100
modify cod_forma_pagto default 0;

/

exec inter_pr_recompile;
