alter table SPED_CTB_I355
  drop constraint PK_SPED_CTB_I355;
prompt ATENCAO! Ao excluir este index se aparecer a mensagem "ORA-01418: specified index does not exist" favor ignorar.
drop index PK_SPED_CTB_I355;

alter table SPED_CTB_I355
  add constraint PK_SPED_CTB_I355 primary key (COD_EMPRESA, EXERCICIO, COD_CONTA, IND_SALDO, TRIMESTRE, CENTRO_CUSTO) novalidate;
