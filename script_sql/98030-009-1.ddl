create table supr_121(
   pedido number(6) not null,
   sequencia number(2) not null,
   centro_de_custo number(6) not null,
   perc_dist number(10,2) default 0.00,
   qtde_dist number(10,2) default 0.00
);

alter table supr_121
add constraint PK_SUPR_121 primary key(pedido, sequencia, centro_de_custo);

comment on column supr_121.pedido is 'Pedido de compra';
comment on column supr_121.sequencia is 'Sequência do item do pedido de compra';
comment on column supr_121.centro_de_custo is '';
comment on column supr_121.perc_dist is 'Percentual de distribuição';
comment on column supr_121.qtde_dist is 'Quantidade de distribuição';

/
exec inter_pr_recompile;
/
