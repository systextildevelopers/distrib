
  CREATE OR REPLACE FUNCTION "INTER_FN_CALC_TEMPO_OT" (p_tipo_ordem in number,
                                                   p_ordem_tingimento in number,
                                                   p_grupo_maquina in varchar2,
                                                   p_subgrupo_maquina in varchar2)
RETURN number is
-- Variavel a ser retornada no calculo da funcao
   v_tempo_total_ot       number(15,4);
begin

   v_tempo_total_ot := 0.0000;

   v_tempo_total_ot := inter_fn_get_tempo_grafico_ot(p_ordem_tingimento,
                                                     p_tipo_ordem,
                                                     p_grupo_maquina,
                                                     p_subgrupo_maquina);

   if v_tempo_total_ot  = 0
   then
      v_tempo_total_ot := inter_fn_get_tempo_roteiro_ot (p_ordem_tingimento,
                                                         p_tipo_ordem,
                                                         2); -- Nao depende de quantidade.
   end if;

   if v_tempo_total_ot  = 0
   then
      v_tempo_total_ot := inter_fn_get_tempo_roteiro_ot (p_ordem_tingimento,
                                                         p_tipo_ordem,
                                                         1); -- Depende de quantidade.
   end if;

   return(v_tempo_total_ot);
end inter_fn_calc_tempo_ot;
 

/

exec inter_pr_recompile;

