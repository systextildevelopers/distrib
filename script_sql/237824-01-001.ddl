create table  supr_027_aux(
    cep          number(9,0) default 0 not null enable
	,rota         number(9,0)
	,filial_sigla varchar2(4)
	,id_filial    number(4,0)
	,fornecedor9  number(9,0)
	,cep_final    number(9,0)
	,row_num      number
	,constraint pk_supr_027_aux primary key (fornecedor9, cep) using index  enable
);

alter table supr_027_aux 
    add constraint fk_supr_027_029_aux foreign key (fornecedor9) references  supr_029 (fornecedor9) enable;
