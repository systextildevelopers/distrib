
  CREATE OR REPLACE TRIGGER "INTER_TR_CREC_170_2" 
  before insert or delete or update of valor_cheque on crec_170
  for each row
begin
   if inserting and :new.lote_cheque > 0
   then
      begin
        update crec_165
        set crec_165.valor_lote    = crec_165.valor_lote + :new.valor_cheque,
            crec_165.saldo_lote    = crec_165.saldo_lote + :new.valor_cheque
        where crec_165.empresa     = :new.empresa
          and crec_165.lote_cheque = :new.lote_cheque;
      exception when others
        then raise_application_error(-20000, sqlerrm);
      end;
   end if;

   if deleting and :old.lote_cheque > 0
   then
      begin
        update crec_165
        set crec_165.valor_lote    = crec_165.valor_lote - :old.valor_cheque,
            crec_165.saldo_lote    = crec_165.saldo_lote - :old.valor_cheque
        where crec_165.empresa     = :old.empresa
          and crec_165.lote_cheque = :old.lote_cheque;
      exception when others
        then raise_application_error(-20000, sqlerrm);
      end;
   end if;

   if updating and :new.lote_cheque > 0
   then
      begin
        update crec_165
        set crec_165.valor_lote    = crec_165.valor_lote - :old.valor_cheque + :new.valor_cheque,
            crec_165.saldo_lote    = crec_165.saldo_lote - :old.valor_cheque + :new.valor_cheque
        where crec_165.empresa     = :new.empresa
          and crec_165.lote_cheque = :new.lote_cheque;
      exception when others
        then raise_application_error(-20000, sqlerrm);
      end;
   end if;
end;

-- ALTER TRIGGER "INTER_TR_CREC_170_2" ENABLE
 

/

exec inter_pr_recompile;

