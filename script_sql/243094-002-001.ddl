alter table i_obrf_015
add (
CEST                           VARCHAR2(10),
PERC_FCP_UF_DEST               NUMBER(8,4),
PERC_ICMS_PARTILHA             NUMBER(8,4),
PERC_ICMS_UF_DEST              NUMBER(8,4),
QUANTIDADE_CONV                NUMBER(14,3),
UNIDADE_CONV                   VARCHAR2(3),
VALOR_AFRMM                    NUMBER(15,6),
VALOR_ANTI_DUMP                NUMBER(15,6),
VALOR_CONV                     NUMBER(15,6),
VALOR_DESP_ACESS               NUMBER(15,6),
VALOR_DESP_NTRIB               NUMBER(15,6),
VAL_FCP_UF_DEST                NUMBER(17,5),
VAL_ICMS_UF_DEST               NUMBER(17,5),
VAL_ICMS_UF_REMET              NUMBER(17,5)
);
