
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_021" 
   after insert
      or delete
      or update of subgru_comp,       item_comp,
                   nivel_item,        grupo_item,
                   subgru_item,       item_item,
                   alternativa_produto
on basi_021 -- Equivalente a Basi_040, so que e a estrutura do produto que sera aprovado para o projeto.
for each row

declare
   cliente9                  basi_001.cnpj_cliente9%type;
   cliente4                  basi_001.cnpj_cliente4%type;
   cliente2                  basi_001.cnpj_cliente2%type;
   v_nivel_comp_n            basi_013.nivel_comp%type;
   v_grupo_comp_n            basi_013.grupo_comp%type;
   v_subgru_comp_n           basi_013.subgru_comp%type;
   v_seq_subprj_n            number(4);
   v_seq_cor                 basi_013.seq_cor%type;
   v_codigo_desenho          basi_013.codigo_desenho%type;
   v_gera_inf_aprova_reprova empr_002.gera_inf_aprova_reprova%type;

begin

   -- Le parametro global para gerar ou nao as informacoes
   -- de aprovacao do projeto.
   -- Parametro criado para agilizar o processo de atualizacao de estutura do projeto,
   -- porque esta rotina e a mais demorada e nao esta em uso por nenhum cliente.
   -- Habilitando a geracao destas informacoes, o tempo de processamento de atualizacao
   -- de estutura de projeto, aumenta em 90%
   begin
      select empr_002.gera_inf_aprova_reprova
      into   v_gera_inf_aprova_reprova
      from empr_002;
      exception
      when OTHERS
         then v_gera_inf_aprova_reprova := 'N';
   end;

   if v_gera_inf_aprova_reprova = 'S'
   then
      if inserting or updating
      then
         -- Como nao temos o nivel e grupo na tabela de ralacionamento de cor e tamanho da estrutura
         -- lemos o componente da basi_013.
         begin
            select basi_013.nivel_comp,                   basi_013.grupo_comp,
                   basi_013.subgru_comp,
                   basi_013.sequencia_subprojeto,         basi_013.seq_cor,
                   basi_013.codigo_desenho
            into   v_nivel_comp_n,                        v_grupo_comp_n,
                   v_subgru_comp_n,
                   v_seq_subprj_n,                        v_seq_cor,
                   v_codigo_desenho
            from basi_013
            where basi_013.codigo_projeto      = :new.codigo_projeto
              and basi_013.sequencia_projeto   = :new.sequencia_projeto
              and basi_013.alternativa_produto = :new.alternativa_produto
              and basi_013.sequencia_estrutura = :new.sequencia_estrutura
              and basi_013.sequencia_variacao  = :new.sequencia_variacao
              and basi_013.nivel_item          = :new.nivel_item
              and basi_013.grupo_item          = :new.grupo_item
              and (basi_013.subgru_item        = :new.subgru_item
               or basi_013.subgru_item         = '000')
              and (basi_013.item_item          = :new.item_item
               or basi_013.item_item           = '000000');
         exception when others then
            v_nivel_comp_n   := '';
            v_grupo_comp_n   := '';
            v_subgru_comp_n  := '';
            v_seq_subprj_n   := 0;
            v_seq_cor        := ' ';
            v_codigo_desenho := ' ';
         end;

         begin
            -- Seleciona o cliente da capa do projeto.
            select nvl(basi_001.cnpj_cliente9,0),   nvl(basi_001.cnpj_cliente4,0),
                   nvl(basi_001.cnpj_cliente2,0)
            into   cliente9,                        cliente4,
                   cliente2
            from basi_001
            where basi_001.codigo_projeto = :new.codigo_projeto;
         exception when OTHERS then
            raise_application_error (-20000, 'Projeto nao cadastrado');
         end;

         inter_pr_limpar_aprov_comp(:new.codigo_projeto,         v_seq_subprj_n,
                                    :new.nivel_item,             :new.grupo_item,
                                    :new.subgru_item,            :new.item_item,
                                    :new.alternativa_produto,
                                    cliente9,                    cliente4,
                                    cliente2);

         if v_seq_cor        <> ' '
         or v_codigo_desenho <> ' '
         then
            if v_seq_cor <> ' '
            then
               inter_pr_exp_seq_cor(:new.codigo_projeto,         v_seq_subprj_n,
                                    :new.nivel_item,             :new.grupo_item,
                                    :new.subgru_item,            :new.item_item,
                                    :new.alternativa_produto,
                                    v_nivel_comp_n,              v_grupo_comp_n,
                                    :new.subgru_comp,            :new.item_comp,
                                    cliente9,                    cliente4,
                                    cliente2,
                                    v_seq_cor);
            end if;

            if v_codigo_desenho <> ' '
            then
               inter_pr_exp_desenho(:new.codigo_projeto,         v_seq_subprj_n,
                                    :new.nivel_item,             :new.grupo_item,
                                    :new.subgru_item,            :new.item_item,
                                    :new.alternativa_produto,
                                    v_nivel_comp_n,              v_grupo_comp_n,
                                    :new.subgru_comp,            :new.item_comp,
                                    cliente9,                    cliente4,
                                    cliente2,
                                    v_codigo_desenho,
                                    :new.sequencia_estrutura,    :new.sequencia_variacao);
            end if;
         else
            if :new.subgru_comp is not null
            and :new.subgru_comp <> '000'
            then
               v_subgru_comp_n := :new.subgru_comp;
            end if;

            if  v_nivel_comp_n   is not null and v_nivel_comp_n   <> '0'
            and v_grupo_comp_n   is not null and v_grupo_comp_n   <> '00000'
            and v_subgru_comp_n  is not null and v_subgru_comp_n  <> '000'
            and :new.item_comp   is not null and :new.item_comp   <> '000000'
            then
               inter_pr_inserir_basi_027_028 (v_nivel_comp_n,
                                              v_grupo_comp_n,
                                              v_subgru_comp_n,
                                              :new.item_comp,
                                              :new.codigo_projeto,
                                              v_seq_subprj_n,
                                              :new.nivel_item,
                                              :new.grupo_item,
                                              :new.subgru_item,
                                              :new.item_item,
                                              :new.alternativa_produto,
                                              cliente9,
                                              cliente4,
                                              cliente2);
            end if;

            -- Para item igual 000000...
            if (v_subgru_comp_n <> '000'  and :new.item_comp = '000000')
            then
               for reg_basi_010 in (select basi_010.item_estrutura
                                    from basi_010
                                    where basi_010.nivel_estrutura   = v_nivel_comp_n
                                      and basi_010.grupo_estrutura   = v_grupo_comp_n
                                      and basi_010.subgru_estrutura  = v_subgru_comp_n)
               loop
                  inter_pr_inserir_basi_027_028 (v_nivel_comp_n,
                                                 v_grupo_comp_n,
                                                 v_subgru_comp_n,
                                                 reg_basi_010.item_estrutura,
                                                 :new.codigo_projeto,
                                                 v_seq_subprj_n,
                                                 :new.nivel_item,
                                                 :new.grupo_item,
                                                 :new.subgru_item,
                                                 :new.item_item,
                                                 :new.alternativa_produto,
                                                 cliente9,
                                                 cliente4,
                                                 cliente2);
               end loop;
            end if;
         end if;
      end if;
   end if;
end inter_tr_basi_021;

-- ALTER TRIGGER "INTER_TR_BASI_021" ENABLE
 

/

exec inter_pr_recompile;

