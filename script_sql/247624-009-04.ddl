create table ESTQ_420_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(250) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(250) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  id_OLD                 NUMBER ,  
  id_NEW                 NUMBER , 
  nome_arquivo_OLD       VARCHAR2(400) ,  
  nome_arquivo_NEW       VARCHAR2(400) , 
  mime_type_OLD          VARCHAR2(255) ,  
  mime_type_NEW          VARCHAR2(255) , 
  conteudo_OLD           BLOB,  
  conteudo_NEW           BLOB, 
  data_importacao_OLD    TIMESTAMP(6) default current_timestamp ,  
  data_importacao_NEW    TIMESTAMP(6) default current_timestamp , 
  usuario_importacao_OLD VARCHAR2(250),  
  usuario_importacao_NEW VARCHAR2(250), 
  status_OLD             NUMBER(2), 
  status_NEW             NUMBER(2) 
) ;

-- execute
