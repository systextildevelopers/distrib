create table PROG_050
(
    NIVEL_ESTRUTURA         VARCHAR2(1)    default '' not null,
    GRUPO_ESTRUTURA         VARCHAR2(5)    default '' not null,
    SUBGRU_ESTRUTURA        VARCHAR2(3)    default '' not null,
    ITEM_ESTRUTURA          VARCHAR2(6)    default '' not null,
    NUMERO_ALTERNATI        NUMBER(2)      default 0  not null,
    NUMERO_ROTEIRO          NUMBER(2)      default 0  not null,
    SEQ_OPERACAO            NUMBER(4)      default 0  not null,
    CODIGO_OPERACAO         NUMBER(5)      default 0,
    MINUTOS                 NUMBER(9, 4)   default 0.0,
    CODIGO_ESTAGIO          NUMBER(2)      default 0,
    CENTRO_CUSTO            NUMBER(6)      default 0,
    SEQUENCIA_ESTAGIO       NUMBER(4)      default 0,
    ESTAGIO_ANTERIOR        NUMBER(2)      default 0,
    ESTAGIO_DEPENDE         NUMBER(2)      default 0,
    SEPARA_OPERACAO         VARCHAR2(1)    default '',
    MINUTOS_HOMEM           NUMBER(9, 4)   default 0.00,
    CCUSTO_HOMEM            NUMBER(6)      default 0,
    NUMERO_CORDAS           NUMBER(3)      default 0,
    NUMERO_ROLOS            NUMBER(5)      default 0,
    VELOCIDADE              NUMBER(8, 2)   default 0.00,
    CODIGO_FAMILIA          NUMBER(6)      default 0,
    OBSERVACAO              VARCHAR2(4000) default '',
    TIPO_PROCESSO           NUMBER(1)      default 0,
    PECAS_1_HORA            NUMBER(9)      default 0,
    PECAS_8_HORAS           NUMBER(9)      default 0,
    CUSTO_MINUTO            NUMBER(6, 3)   default 0.00,
    PERC_EFICIENCIA         NUMBER(6, 3)   default 0.00,
    TEMPERATURA             NUMBER(7, 3)   default 0.00,
    TEMPO_LOTE_PRODUCAO     NUMBER(4)      default 0,
    PECAS_LOTE_PRODUCAO     NUMBER(10, 2)  default 0.00,
    TIME_CELULA             NUMBER(4)      default 0,
    CODIGO_APARELHO         VARCHAR2(4)    default '',
    PERC_EFIC_ROT           NUMBER(6, 3)   default 0.000,
    NUMERO_OPERADORAS       NUMBER(3)      default 0,
    CONSIDERA_EFIC          NUMBER(1)      default 0,
    SEQ_OPERACAO_AGRUPADORA NUMBER(4)      default 0,
    CODIGO_PARTE_PECA       VARCHAR2(5)    default '',
    SEQ_JUNCAO_PARTE_PECA   NUMBER(4)      default 0,
    SITUACAO                NUMBER(1)      default 0,
    PERC_PERDAS             NUMBER(5, 2)   default 0.00,
    PERC_CUSTOS             NUMBER(5, 2)   default 0.00,
    IND_ESTAGIO_GARGALO     NUMBER(1)      default 0,
    NR_AGULHAS              NUMBER(3)      default 0
);
/

create table PROG_055
(
    CMQOP055_NIVMQ050 VARCHAR2(1) default '' not null,
    CMQOP055_GRUMQ050 VARCHAR2(5) default '' not null,
    CMQOP055_SUBMQ050 VARCHAR2(3) default '' not null,
    CMQOP055_ITEMQ050 VARCHAR2(6) default '' not null,
    CMQOP055_NR_ALT   NUMBER(2)   default 0  not null,
    CMQOP055_NR_ROT   NUMBER(2)   default 0  not null,
    CMQOP055_SQ_OPER  NUMBER(4)   default 0  not null,
    SEQ_ESTRUTURA     NUMBER(3)   default 0  not null
);
/

create table PROG_056
(
    NIVEL_ESTRUTURA  VARCHAR2(1)  default '' not null,
    GRUPO_ESTRUTURA  VARCHAR2(5)  default '' not null,
    SUBGRU_ESTRUTURA VARCHAR2(3)  default '' not null,
    ITEM_ESTRUTURA   VARCHAR2(6)  default '' not null,
    NUMERO_ALTERNATI NUMBER(2)    default 0  not null,
    NUMERO_ROTEIRO   NUMBER(2)    default 0  not null,
    SEQ_OPERACAO     NUMBER(4)    default 0  not null,
    CODIGO_OPERADOR  NUMBER(3)    default 0  not null,
    TEMPO_OPERADOR   NUMBER(6, 2) default 0.00,
    QTDE_PC_OPERADOR NUMBER(8, 2) default 0,
    CODIGO_FAMILIA   NUMBER(4)    default 0
);
/

create table PROG_057
(
    NIVEL_ESTRUTURA    VARCHAR2(1)  default ' ' not null,
    GRUPO_ESTRUTURA    VARCHAR2(5)  default ' ' not null,
    SUBGRUPO_ESTRUTURA VARCHAR2(3)  default ' ' not null,
    ITEM_ESTRUTURA     VARCHAR2(6)  default ' ' not null,
    NUMERO_ALTERNATIVA NUMBER(2)    default 0   not null,
    NUMERO_ROTEIRO     NUMBER(2)    default 0   not null,
    CODIGO_ESTAGIO     NUMBER(2)    default 0   not null,
    DESCR_PARTE_PECA   VARCHAR2(30) default ' ' not null,
    CONTROLE_POR_PARTE NUMBER(1)    default 0,
    TIPO_IMPRESSAO     VARCHAR2(1)  default '1'
);
/
create table PROG_059
(
    NIVEL_ROTEIRO    VARCHAR2(1)   default '0'      not null,
    GRUPO_ROTEIRO    VARCHAR2(5)   default '00000'  not null,
    SUBGRU_ROTEIRO   VARCHAR2(3)   default '000'    not null,
    ITEM_ROTEIRO     VARCHAR2(6)   default '000000' not null,
    ALTERNATIVA      NUMBER(2)     default 0        not null,
    ROTEIRO          NUMBER(2)     default 0        not null,
    OPERACAO_ROTEIRO NUMBER(5)     default 0        not null,
    SEQ_OPERACAO     NUMBER(4)     default 0        not null,
    NIVEL_LINHA      VARCHAR2(1)   default '0'      not null,
    GRUPO_LINHA      VARCHAR2(5)   default '00000'  not null,
    SUBGRU_LINHA     VARCHAR2(3)   default '000'    not null,
    ITEM_LINHA       VARCHAR2(6)   default '000000' not null,
    CONSUMO_LINHA    NUMBER(6, 2)  default 0.00,
    PESO_METRO       NUMBER(10, 6) default 0.000000,
    COL_TABELA_PRECO NUMBER(2)     default 0,
    MES_TABELA_PRECO NUMBER(2)     default 0,
    SEQ_TABELA_PRECO NUMBER(2)     default 0,
    CUSTO            NUMBER(10, 6) default 0.000000,
    ULTIMO_CUSTO     NUMBER(10, 6) default 0.000000
);
/
create table PROG_090
(
    CODIGO_VARIAVEL    NUMBER(4)    default 0  not null,
    LIMITE_MAXIMO      NUMBER(8, 3) default 0.0,
    LIMITE_MINIMO      NUMBER(8, 3) default 0.0,
    MQOP050_GRUMQ050   VARCHAR2(5)  default '' not null,
    MQOP050_ITEMQ050   VARCHAR2(6)  default '' not null,
    MQOP050_NIVMQ050   VARCHAR2(1)  default '' not null,
    MQOP050_NR_ALT     NUMBER(2)    default 0  not null,
    MQOP050_NR_ROT     NUMBER(2)    default 0  not null,
    MQOP050_SQ_OPER    NUMBER(4)    default 0  not null,
    MQOP050_SUBMQ050   VARCHAR2(3)  default '' not null,
    LIMITE_PADRAO      NUMBER(8, 3) default 0.0,
    OBSERVACAO         VARCHAR2(30) default '',
    NIVEL_VARIAVEL_OP  VARCHAR2(1)  default '' not null,
    GRUPO_VARIAVEL_OP  VARCHAR2(5)  default '' not null,
    SUBGRU_VARIAVEL_OP VARCHAR2(3)  default '' not null,
    ITEM_VARIAVEL_OP   VARCHAR2(6)  default '' not null,
    GRUPO_MAQUINA      VARCHAR2(4)  default '' not null,
    SUB_MAQUINA        VARCHAR2(3)  default '' not null,
    NUMERO_MAQUINA     NUMBER       default 0  not null
);
/

exec inter_pr_recompile;
