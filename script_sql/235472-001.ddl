create table obrf_181
(
  EMPRESA                 NUMBER(3) default 0 not null,
  COD_MSG                 NUMBER(6) default 0 not null,
  CODIGO_SQL              NUMBER(5) default 0,
  INICIO_VIGENCIA         DATE default '01-jan-1990',
  FIM_VIGENCIA            DATE,
  ATIVO_INATIVO           NUMBER(1) default 1
);
 
create unique index PK_OBRF_181 on obrf_181 (EMPRESA, COD_MSG);
 
insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f232', 'Cadastro de relacionamento de SQL de mensagem de nota.', 0,1);
 
insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f232', 'obrf_menu', 1, 20, 'S', 'S', 'S', 'S');

create table obrf_182
(
  CODIGO     NUMBER(5) default 0 not null,
  NOME       VARCHAR2(45) default '' not null,
  DESCRICAO  VARCHAR2(4000) default '',
  SCRIPT_SQL VARCHAR2(4000) default '',
  TIPO_NOTA  NUMBER(2) 
);
 
alter table obrf_182
  add constraint PK_OBRF_182 primary key (CODIGO, NOME);
  
  
insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f233', 'Cadastro de SQL para mensagem de nota.', 0,1);
 
insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f233', 'obrf_menu', 1, 20, 'S', 'S', 'S', 'S');
