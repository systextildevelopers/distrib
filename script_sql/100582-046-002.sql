CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_880"
BEFORE INSERT ON PCPC_880 FOR EACH ROW

DECLARE
   proximo_valor	number;
BEGIN
    select PCPC_880_ID_ESTAGIO_CONSERTO.nextval into proximo_valor from dual;
    :new.id_estagio_conserto := proximo_valor;
END inter_tr_pcpc_880;
/
