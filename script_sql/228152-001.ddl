alter table BASI_165 add ftid_code varchar2(15) default '';

comment on column BASI_165.ftid_code
  is 'Siglas de nomenclaturas de identifica��o fiscal ou n�mero de identifica��o do contribuinte do pa�s';
