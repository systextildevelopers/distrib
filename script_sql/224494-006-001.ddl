create table FNDC_005
(
  cod_empresa NUMBER(3) not null,
  sequencial  NUMBER(4),
  hst_pag     NUMBER(4),
  hst_estorno NUMBER(4),
  nr_dias     NUMBER(2),
  perc_juros  FLOAT(4)
);

alter table FNDC_005
  add constraint PK_FNDC_005 primary key (COD_EMPRESA);
  
 
  /
 
 exec inter_pr_recompile;
