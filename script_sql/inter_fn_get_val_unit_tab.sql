CREATE OR REPLACE FUNCTION inter_fn_get_val_unit_tab(
    p_colecao   in number,
    p_mes       in number,
    p_sequencia in number,
    p_nivel     in varchar2,
    p_grupo     in varchar2,
    p_sub_grupo in varchar2,
    p_item      in varchar2
)
    return number
    is
    valor_unitario       number(15, 6);
    tipo_produto         number;
    tipo_produto_030     number;
    tipo_produto_030_aux number;
    serie_cor_item       number;

begin
    begin
        select pedi_095.val_tabela_preco
        into   valor_unitario
        from pedi_095
        where pedi_095.tab_col_tab        = p_colecao
          and pedi_095.tab_mes_tab        = p_mes
          and pedi_095.tab_seq_tab        = p_sequencia
          and pedi_095.nivel_estrutura    = p_nivel
          and pedi_095.grupo_estrutura    = p_grupo
          and pedi_095.subgru_estrutura   = p_sub_grupo
          and pedi_095.item_estrutura     = p_item
          and pedi_095.nivel_preco        = 4;
    exception when no_data_found then
        valor_unitario := 0;
    end;

    if valor_unitario > 0 then
        return(valor_unitario);
    end if;
    begin
        select basi_020.tipo_produto
        into tipo_produto
        from basi_020
        where basi030_nivel030  = p_nivel
          and basi030_referenc = p_grupo
          and tamanho_ref      = p_sub_grupo;
    exception when no_data_found then
        tipo_produto := 0;
    end;

    begin
        select basi_030.tipo_produto
        into tipo_produto_030
        from basi_030
        where nivel_estrutura = p_nivel
          and referencia      = p_grupo;
    end;

    if tipo_produto_030 = 1
    then tipo_produto_030_aux := tipo_produto;
    else tipo_produto_030_aux := 1;
    end if;

    /* PROCURA.SERIE_DE_COR.DO.PRODUTO.PARA.PROCURAR.PRECO.POR.SERIE */
    begin
        select basi_100.serie_cor
        into serie_cor_item
        from basi_100
        where basi_100.cor_sortimento = p_item
          and basi_100.tipo_cor       = tipo_produto_030_aux;
    exception when no_data_found then
        serie_cor_item := 0;
    end;
    if serie_cor_item > 0
    then
        begin
            select pedi_095.val_tabela_preco
            into valor_unitario
            from pedi_095
            where pedi_095.tab_col_tab      = p_colecao
              and pedi_095.tab_mes_tab      = p_mes
              and pedi_095.tab_seq_tab      = p_sequencia
              and pedi_095.nivel_estrutura   = p_nivel
              and pedi_095.grupo_estrutura  = p_grupo
              and pedi_095.subgru_estrutura = p_sub_grupo
              and pedi_095.item_estrutura   = serie_cor_item
              and pedi_095.nivel_preco      = 3;
        exception when no_data_found then
            valor_unitario := 0;
        end;
        if valor_unitario > 0 then
            return(valor_unitario);
        end if;

    end if;
    begin
        select pedi_095.val_tabela_preco
        into valor_unitario
        from pedi_095
        where pedi_095.tab_col_tab       = p_colecao
          and pedi_095.tab_mes_tab       = p_mes
          and pedi_095.tab_seq_tab      = p_sequencia
          and pedi_095.nivel_estrutura  = p_nivel
          and pedi_095.grupo_estrutura  = p_grupo
          and pedi_095.subgru_estrutura = p_sub_grupo
          and pedi_095.nivel_preco      = 2;
    exception when no_data_found then
        valor_unitario := 0;
    end;

    if valor_unitario > 0 then
        return(valor_unitario);
    end if;

    begin
        select pedi_095.val_tabela_preco
        into valor_unitario
        from pedi_095
        where pedi_095.tab_col_tab     = p_colecao
          and pedi_095.tab_mes_tab     = p_mes
          and pedi_095.tab_seq_tab     = p_sequencia
          and pedi_095.nivel_estrutura = p_nivel
          and pedi_095.grupo_estrutura = p_grupo
          and pedi_095.nivel_preco     = 1;
    exception when no_data_found then
        valor_unitario := 0;
    end;

    if valor_unitario > 0 then
        return(valor_unitario);
    end if;

    return(valor_unitario);
end inter_fn_get_val_unit_tab;

/

exec inter_pr_recompile;

