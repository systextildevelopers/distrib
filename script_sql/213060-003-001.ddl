-- Tipos de Não Conformidades

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('cobr_f915', 'Painel de Movimento Arquivos Fidic', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'cobr_f915', 'efic_menu', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'cobr_f915', 'efic_menu', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Painel de Movimento Arquivos Fidic'
 where hdoc_036.codigo_programa = 'cobr_f915'
   and hdoc_036.locale          = 'es_ES';
commit;

/
