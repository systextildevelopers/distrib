CREATE OR REPLACE PROCEDURE inter_pr_numer_auto_areceber(p_cod_empresa  IN number,
                                                         p_tipo_titulo  IN number,
                                                         v_num_titulo   OUT number) AS
BEGIN
   v_num_titulo := 0;
   
   
   select nvl(max(fatu_070.num_duplicata),0) + 1 into v_num_titulo from fatu_070
   where fatu_070.codigo_empresa = p_cod_empresa
     and fatu_070.tipo_titulo    = p_tipo_titulo; 
   if v_num_titulo >= 999999999
   then
      begin
         select registros_vazios
         into v_num_titulo
         from(
             select ROWNUM as registros_vazios
             from dual
             CONNECT BY LEVEL <= 999999
             MINUS
             select fatu_070.num_duplicata
             from fatu_070
             where fatu_070.codigo_empresa = p_cod_empresa
               and fatu_070.tipo_titulo    = p_tipo_titulo
         )
         where ROWNUM = 1;
      EXCEPTION WHEN
      others then
         v_num_titulo := 0;
      end;
   end if;
END inter_pr_numer_auto_areceber;
