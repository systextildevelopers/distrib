create or replace trigger inter_tr_cdst_fa01_novos_usu
before insert or delete on hdoc_033
for each row
    -- 06/2024 - Caio Carvalho
    -- Trigger criada para auxiliar na atualização dos usuarios na oper_550 quando um usuario for adicionado ou excluído da hdoc_033
declare

  v_qtde_reg  number;

begin

    if inserting and :new.programa = 'cdst_fa01' then
        v_qtde_reg :=0;

        begin

            select
                nvl(count(1),0)
            into v_qtde_reg
            from oper_550 a
            where   a.usuario       = :new.usu_prg_cdusu
                and a.nome_programa = 'cdst_fa01'
                and a.empresa       = :new.usu_prg_empr_usu;

        exception
            when others then
                raise_application_error (-20000, 'Erro no select da trigger inter_tr_cdst_fa01_novos_usu - cdst_fa01 ');
        end;

        if v_qtde_reg = 0 then

            begin
                
                for x in (
                    select CODIGO from vi_carga_tabelas
                )loop
                    insert into oper_550 ( usuario, empresa, nome_programa, nome_subprograma, nome_field, requerido, acessivel, inicia_campo)
                    values (:new.usu_prg_cdusu, :new.usu_prg_empr_usu, 'cdst_fa01', 'LISTA_TABELAS', x.CODIGO, 0, 1, 0);
                end loop;
                
            exception when others then

                raise_application_error (-20000, 'Erro no insert da trigger inter_tr_cdst_fa01_novos_usu - cdst_fa01');

            end;

        end if;

    end if;

    if deleting and :old.programa = 'cdst_fa01' and :old.nome_menu <> 'favoritos' then
        v_qtde_reg :=0;

        begin

            select
                nvl(count(1),0)
            into v_qtde_reg
            from oper_550 a
            where   a.usuario       = :old.usu_prg_cdusu
                and a.nome_programa = 'cdst_fa01'
                and a.empresa       = :old.usu_prg_empr_usu;

        exception
            when others then
                raise_application_error (-20000, 'Erro no select da trigger inter_tr_cdst_fa01_novos_usu - cdst_fa01 ');
        end;

        if v_qtde_reg > 0 then

            begin
                delete
                from oper_550 a
                where a.usuario         = :old.usu_prg_cdusu
                    and a.nome_programa = 'cdst_fa01'
                    and a.empresa       = :old.usu_prg_empr_usu;

            exception when others then
                raise_application_error (-20000, 'Erro no delete da trigger inter_tr_cdst_fa01_novos_usu - cdst_fa01 ');
            end;

        end if;
        
    end if;

end inter_tr_cdst_fa01_novos_usu;
