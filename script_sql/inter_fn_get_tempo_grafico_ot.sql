create or replace function inter_fn_get_tempo_grafico_ot (p_ordem_tingimento in number,
                                                          p_tipo_ordem in varchar2,
                                                          p_grupo_maquina in varchar2,
                                                          p_subgrupo_maquina in varchar2)
return number is
   v_tempo_grafico         number(15,4);
   v_nivel_tecido          varchar2(1);
   v_grupo_tecido          varchar2(5);
   v_subgrupo_tecido       varchar2(3);
   v_item_tecido           varchar2(6);
   v_alternativa_tecido    number(2);

   v_subgrupo_comp         varchar2(3);
   v_item_comp             varchar2(6);
   v_numero_grafico        number;
begin
   v_tempo_grafico := 0.0000;

   for reg_pcpb_100 in (select pcpb_020.pano_sbg_nivel99,  pcpb_020.pano_sbg_grupo,
                               pcpb_020.pano_sbg_subgrupo, pcpb_020.pano_sbg_item,
                               pcpb_020.alternativa_item,  pcpb_020.roteiro_opcional,
                               pcpb_020.qtde_quilos_prog,  pcpb_100.ordem_producao,
                               pcpb_010.grupo_maquina,     pcpb_010.subgrupo_maquina
                          from pcpb_100, pcpb_020, pcpb_010
                         where pcpb_100.ordem_agrupamento = p_ordem_tingimento
                           and pcpb_100.tipo_ordem        = p_tipo_ordem
                           and pcpb_100.ordem_producao    = pcpb_020.ordem_producao
                           and pcpb_020.ordem_producao    = pcpb_010.ordem_producao
                         order by pcpb_020.qtde_quilos_prog desc)
   loop
      v_nivel_tecido       := reg_pcpb_100.pano_sbg_nivel99;
      v_grupo_tecido       := reg_pcpb_100.pano_sbg_grupo;
      v_subgrupo_tecido    := reg_pcpb_100.pano_sbg_subgrupo;
      v_item_tecido        := reg_pcpb_100.pano_sbg_item;
      v_alternativa_tecido := reg_pcpb_100.alternativa_item;

      for reg_basi_050 in (select basi_050.nivel_comp,      basi_050.grupo_comp,
                                  basi_050.sub_comp,        basi_050.item_comp,
                                  basi_050.sequencia,       basi_050.sub_item,
                                  basi_050.item_item,       basi_050.numero_grafico,
                                  basi_050.alternativa_comp
                            from  basi_050
                           where  basi_050.nivel_item       = v_nivel_tecido
                             and  basi_050.grupo_item       = v_grupo_tecido
                             and (basi_050.sub_item         = v_subgrupo_tecido or basi_050.sub_item  = '000')
                             and (basi_050.item_item        = v_item_tecido     or basi_050.item_item = '000000')
                             and  basi_050.alternativa_item = v_alternativa_tecido
                             and  basi_050.nivel_comp       = '5'
                           order by  basi_050.sequencia, basi_050.numero_grafico)
      loop
         if reg_basi_050.sub_comp = '000'
         then
            begin
               select basi_040.sub_comp
                 into v_subgrupo_comp
                 from basi_040
                where basi_040.nivel_item       = v_nivel_tecido
                  and basi_040.grupo_item       = v_grupo_tecido
                  and basi_040.sub_item         = v_subgrupo_tecido
                  and basi_040.item_item        = reg_basi_050.item_item
                  and basi_040.sequencia        = reg_basi_050.sequencia
                  and basi_040.alternativa_item = v_alternativa_tecido;
               exception when no_data_found
               then v_subgrupo_comp := '000';
            end;
         else
            v_subgrupo_comp := reg_basi_050.sub_comp;
         end if;

         if reg_basi_050.item_comp = '000000'
         then
            begin
               select basi_040.item_comp
                 into v_item_comp
                 from basi_040
                where basi_040.nivel_item       = v_nivel_tecido
                  and basi_040.grupo_item       = v_grupo_tecido
                  and basi_040.sub_item         = reg_basi_050.sub_item
                  and basi_040.item_item        = v_item_tecido
                  and basi_040.sequencia        = reg_basi_050.sequencia
                  and basi_040.alternativa_item = v_alternativa_tecido;
               exception when no_data_found
               then v_item_comp := '000000';
            end;
         else
            v_item_comp := reg_basi_050.item_comp;
         end if;
         
         if reg_basi_050.numero_grafico = 0
         then
            begin
               select basi_010.numero_grafico
               into   v_numero_grafico
               from basi_010
               where basi_010.nivel_estrutura  = reg_basi_050.nivel_comp
                 and basi_010.grupo_estrutura  = reg_basi_050.grupo_comp
                 and basi_010.subgru_estrutura = v_subgrupo_comp
                 and basi_010.item_estrutura   = v_item_comp; 
               exception when others
               then v_numero_grafico := 0;
            end;
         else
            v_numero_grafico := reg_basi_050.numero_grafico;
         end if;         

         if v_numero_grafico > 0
         then
            v_tempo_grafico := inter_fn_get_tempo_mqop_150 (v_numero_grafico,
                                                            p_grupo_maquina,
                                                            p_subgrupo_maquina);
         end if;

         for reg_basi_050_5 in (select  basi_050.nivel_comp, basi_050.grupo_comp,
                                        basi_050.sub_comp,   basi_050.item_comp,
                                        basi_050.sequencia,  basi_050.sub_item,
                                        basi_050.item_item,  basi_050.numero_grafico
                                  from  basi_050
                                 where  basi_050.nivel_item       = reg_basi_050.nivel_comp
                                   and  basi_050.grupo_item       = reg_basi_050.grupo_comp
                                   and (basi_050.sub_item         = v_subgrupo_comp or basi_050.sub_item  = '000')
                                   and (basi_050.item_item        = v_item_comp     or basi_050.item_item = '000000')
                                   and  basi_050.alternativa_item = reg_basi_050.alternativa_comp
                                   and  basi_050.nivel_comp       = '5'
                                 order by basi_050.sequencia, basi_050.numero_grafico)
         loop
            if reg_basi_050_5.numero_grafico > 0
            then
               v_tempo_grafico := v_tempo_grafico + inter_fn_get_tempo_mqop_150 (reg_basi_050_5.numero_grafico,
                                                                                 p_grupo_maquina,
                                                                                 p_subgrupo_maquina);
            end if;
         end loop;

         exit when v_tempo_grafico > 0;
      end loop;

      exit when v_tempo_grafico > 0;
   end loop;

   return(v_tempo_grafico);
end inter_fn_get_tempo_grafico_ot;
/

/* versao: 3 */

exec inter_pr_recompile;
