create or replace trigger inter_tr_PCPT_021_log 
after insert or delete or update 
on PCPT_021 
for each row 
declare 
   ws_usuario_rede           varchar2(250) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    -- begin 
    --    select hdoc_090.programa 
    --    into v_nome_programa 
    --    from hdoc_090 
    --    where hdoc_090.sid = ws_sid 
    --      and rownum       = 1 
    --      and hdoc_090.programa not like '%menu%' 
    --      and hdoc_090.programa not like '%_m%'; 
    --    exception 
    --      when no_data_found then            v_nome_programa := 'SQL'; 
    -- end; 
 
    v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 
 if inserting 
 then 
    begin 
 
        insert into PCPT_021_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           codigo_rolo_OLD,   /*8*/ 
           codigo_rolo_NEW,   /*9*/ 
           arriada_OLD,   /*10*/ 
           arriada_NEW,   /*11*/ 
           pontuacao_qualidade_OLD,   /*12*/ 
           pontuacao_qualidade_NEW,   /*13*/ 
           ordem_engomagem_OLD,   /*14*/ 
           ordem_engomagem_NEW,   /*15*/ 
           restricao_OLD,   /*16*/ 
           restricao_NEW,   /*17*/ 
           agrupador_producao_OLD,   /*18*/ 
           agrupador_producao_NEW,   /*19*/ 
           cod_nuance_OLD,   /*20*/ 
           cod_nuance_NEW,   /*21*/ 
           emenda_OLD,   /*22*/ 
           emenda_NEW,   /*23*/ 
           sequencia_OLD,   /*24*/ 
           sequencia_NEW,   /*25*/ 
           usuario_conf_OLD,   /*26*/ 
           usuario_conf_NEW,   /*27*/ 
           data_conf_OLD,   /*28*/ 
           data_conf_NEW,   /*29*/ 
           hora_conf_OLD,   /*30*/ 
           hora_conf_NEW,   /*31*/ 
           transacao_ent_orig_OLD,   /*32*/ 
           transacao_ent_orig_NEW,   /*33*/ 
           deposito_original_OLD,   /*34*/ 
           deposito_original_NEW,   /*35*/ 
           sequencia_corte_OLD,   /*36*/ 
           sequencia_corte_NEW,   /*37*/ 
           numero_lacre_OLD,   /*38*/ 
           numero_lacre_NEW,   /*39*/ 
           grupo_atendimento_OLD,   /*40*/ 
           grupo_atendimento_NEW,   /*41*/ 
           procedencia_OLD,   /*42*/ 
           procedencia_NEW,   /*43*/ 
           quantidade_abono_OLD,   /*44*/ 
           quantidade_abono_NEW,   /*45*/ 
           qtde_original_prevista_OLD,   /*46*/ 
           qtde_original_prevista_NEW,   /*47*/ 
           seq_rolo_prev_romaneio_OLD,   /*48*/ 
           seq_rolo_prev_romaneio_NEW,   /*49*/ 
           nome_fornecedor_OLD,   /*50*/ 
           nome_fornecedor_NEW,   /*51*/ 
           peso_previsto_OLD,   /*52*/ 
           peso_previsto_NEW,   /*53*/ 
           peso_rolo_real_OLD,   /*54*/ 
           peso_rolo_real_NEW,   /*55*/ 
           numero_op_OLD,   /*56*/ 
           numero_op_NEW,   /*57*/ 
           estagio_op_OLD,   /*58*/ 
           estagio_op_NEW,   /*59*/ 
           sequencia_tingimento_OLD,   /*60*/ 
           sequencia_tingimento_NEW,   /*61*/ 
           sequencia_acabamento_OLD,   /*62*/ 
           sequencia_acabamento_NEW,   /*63*/ 
           rolada_OLD,   /*64*/ 
           rolada_NEW,   /*65*/ 
           seq_rolada_OLD,   /*66*/ 
           seq_rolada_NEW,   /*67*/ 
           nome_programa_OLD,   /*68*/ 
           nome_programa_NEW,   /*69*/ 
           pontos_qualidade_m2_OLD,   /*70*/ 
           pontos_qualidade_m2_NEW,   /*71*/ 
           reposicao_OLD,   /*72*/ 
           reposicao_NEW    /*73*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.codigo_rolo, /*9*/   
           '',/*10*/
           :new.arriada, /*11*/   
           0,/*12*/
           :new.pontuacao_qualidade, /*13*/   
           0,/*14*/
           :new.ordem_engomagem, /*15*/   
           '',/*16*/
           :new.restricao, /*17*/   
           0,/*18*/
           :new.agrupador_producao, /*19*/   
           '',/*20*/
           :new.cod_nuance, /*21*/   
           0,/*22*/
           :new.emenda, /*23*/   
           0,/*24*/
           :new.sequencia, /*25*/   
           '',/*26*/
           :new.usuario_conf, /*27*/   
           null,/*28*/
           :new.data_conf, /*29*/   
           null,/*30*/
           :new.hora_conf, /*31*/   
           0,/*32*/
           :new.transacao_ent_orig, /*33*/   
           0,/*34*/
           :new.deposito_original, /*35*/   
           0,/*36*/
           :new.sequencia_corte, /*37*/   
           0,/*38*/
           :new.numero_lacre, /*39*/   
           0,/*40*/
           :new.grupo_atendimento, /*41*/   
           0,/*42*/
           :new.procedencia, /*43*/   
           0,/*44*/
           :new.quantidade_abono, /*45*/   
           0,/*46*/
           :new.qtde_original_prevista, /*47*/   
           0,/*48*/
           :new.seq_rolo_prev_romaneio, /*49*/   
           '',/*50*/
           :new.nome_fornecedor, /*51*/   
           0,/*52*/
           :new.peso_previsto, /*53*/   
           0,/*54*/
           :new.peso_rolo_real, /*55*/   
           0,/*56*/
           :new.numero_op, /*57*/   
           0,/*58*/
           :new.estagio_op, /*59*/   
           0,/*60*/
           :new.sequencia_tingimento, /*61*/   
           0,/*62*/
           :new.sequencia_acabamento, /*63*/   
           0,/*64*/
           :new.rolada, /*65*/   
           0,/*66*/
           :new.seq_rolada, /*67*/   
           '',/*68*/
           :new.nome_programa, /*69*/   
           0,/*70*/
           :new.pontos_qualidade_m2, /*71*/   
           0,/*72*/
           :new.reposicao /*73*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into PCPT_021_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           codigo_rolo_OLD, /*8*/   
           codigo_rolo_NEW, /*9*/   
           arriada_OLD, /*10*/   
           arriada_NEW, /*11*/   
           pontuacao_qualidade_OLD, /*12*/   
           pontuacao_qualidade_NEW, /*13*/   
           ordem_engomagem_OLD, /*14*/   
           ordem_engomagem_NEW, /*15*/   
           restricao_OLD, /*16*/   
           restricao_NEW, /*17*/   
           agrupador_producao_OLD, /*18*/   
           agrupador_producao_NEW, /*19*/   
           cod_nuance_OLD, /*20*/   
           cod_nuance_NEW, /*21*/   
           emenda_OLD, /*22*/   
           emenda_NEW, /*23*/   
           sequencia_OLD, /*24*/   
           sequencia_NEW, /*25*/   
           usuario_conf_OLD, /*26*/   
           usuario_conf_NEW, /*27*/   
           data_conf_OLD, /*28*/   
           data_conf_NEW, /*29*/   
           hora_conf_OLD, /*30*/   
           hora_conf_NEW, /*31*/   
           transacao_ent_orig_OLD, /*32*/   
           transacao_ent_orig_NEW, /*33*/   
           deposito_original_OLD, /*34*/   
           deposito_original_NEW, /*35*/   
           sequencia_corte_OLD, /*36*/   
           sequencia_corte_NEW, /*37*/   
           numero_lacre_OLD, /*38*/   
           numero_lacre_NEW, /*39*/   
           grupo_atendimento_OLD, /*40*/   
           grupo_atendimento_NEW, /*41*/   
           procedencia_OLD, /*42*/   
           procedencia_NEW, /*43*/   
           quantidade_abono_OLD, /*44*/   
           quantidade_abono_NEW, /*45*/   
           qtde_original_prevista_OLD, /*46*/   
           qtde_original_prevista_NEW, /*47*/   
           seq_rolo_prev_romaneio_OLD, /*48*/   
           seq_rolo_prev_romaneio_NEW, /*49*/   
           nome_fornecedor_OLD, /*50*/   
           nome_fornecedor_NEW, /*51*/   
           peso_previsto_OLD, /*52*/   
           peso_previsto_NEW, /*53*/   
           peso_rolo_real_OLD, /*54*/   
           peso_rolo_real_NEW, /*55*/   
           numero_op_OLD, /*56*/   
           numero_op_NEW, /*57*/   
           estagio_op_OLD, /*58*/   
           estagio_op_NEW, /*59*/   
           sequencia_tingimento_OLD, /*60*/   
           sequencia_tingimento_NEW, /*61*/   
           sequencia_acabamento_OLD, /*62*/   
           sequencia_acabamento_NEW, /*63*/   
           rolada_OLD, /*64*/   
           rolada_NEW, /*65*/   
           seq_rolada_OLD, /*66*/   
           seq_rolada_NEW, /*67*/   
           nome_programa_OLD, /*68*/   
           nome_programa_NEW, /*69*/   
           pontos_qualidade_m2_OLD, /*70*/   
           pontos_qualidade_m2_NEW, /*71*/   
           reposicao_OLD, /*72*/   
           reposicao_NEW  /*73*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.codigo_rolo,  /*8*/  
           :new.codigo_rolo, /*9*/   
           :old.arriada,  /*10*/  
           :new.arriada, /*11*/   
           :old.pontuacao_qualidade,  /*12*/  
           :new.pontuacao_qualidade, /*13*/   
           :old.ordem_engomagem,  /*14*/  
           :new.ordem_engomagem, /*15*/   
           :old.restricao,  /*16*/  
           :new.restricao, /*17*/   
           :old.agrupador_producao,  /*18*/  
           :new.agrupador_producao, /*19*/   
           :old.cod_nuance,  /*20*/  
           :new.cod_nuance, /*21*/   
           :old.emenda,  /*22*/  
           :new.emenda, /*23*/   
           :old.sequencia,  /*24*/  
           :new.sequencia, /*25*/   
           :old.usuario_conf,  /*26*/  
           :new.usuario_conf, /*27*/   
           :old.data_conf,  /*28*/  
           :new.data_conf, /*29*/   
           :old.hora_conf,  /*30*/  
           :new.hora_conf, /*31*/   
           :old.transacao_ent_orig,  /*32*/  
           :new.transacao_ent_orig, /*33*/   
           :old.deposito_original,  /*34*/  
           :new.deposito_original, /*35*/   
           :old.sequencia_corte,  /*36*/  
           :new.sequencia_corte, /*37*/   
           :old.numero_lacre,  /*38*/  
           :new.numero_lacre, /*39*/   
           :old.grupo_atendimento,  /*40*/  
           :new.grupo_atendimento, /*41*/   
           :old.procedencia,  /*42*/  
           :new.procedencia, /*43*/   
           :old.quantidade_abono,  /*44*/  
           :new.quantidade_abono, /*45*/   
           :old.qtde_original_prevista,  /*46*/  
           :new.qtde_original_prevista, /*47*/   
           :old.seq_rolo_prev_romaneio,  /*48*/  
           :new.seq_rolo_prev_romaneio, /*49*/   
           :old.nome_fornecedor,  /*50*/  
           :new.nome_fornecedor, /*51*/   
           :old.peso_previsto,  /*52*/  
           :new.peso_previsto, /*53*/   
           :old.peso_rolo_real,  /*54*/  
           :new.peso_rolo_real, /*55*/   
           :old.numero_op,  /*56*/  
           :new.numero_op, /*57*/   
           :old.estagio_op,  /*58*/  
           :new.estagio_op, /*59*/   
           :old.sequencia_tingimento,  /*60*/  
           :new.sequencia_tingimento, /*61*/   
           :old.sequencia_acabamento,  /*62*/  
           :new.sequencia_acabamento, /*63*/   
           :old.rolada,  /*64*/  
           :new.rolada, /*65*/   
           :old.seq_rolada,  /*66*/  
           :new.seq_rolada, /*67*/   
           :old.nome_programa,  /*68*/  
           :new.nome_programa, /*69*/   
           :old.pontos_qualidade_m2,  /*70*/  
           :new.pontos_qualidade_m2, /*71*/   
           :old.reposicao,  /*72*/  
           :new.reposicao  /*73*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into PCPT_021_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           codigo_rolo_OLD, /*8*/   
           codigo_rolo_NEW, /*9*/   
           arriada_OLD, /*10*/   
           arriada_NEW, /*11*/   
           pontuacao_qualidade_OLD, /*12*/   
           pontuacao_qualidade_NEW, /*13*/   
           ordem_engomagem_OLD, /*14*/   
           ordem_engomagem_NEW, /*15*/   
           restricao_OLD, /*16*/   
           restricao_NEW, /*17*/   
           agrupador_producao_OLD, /*18*/   
           agrupador_producao_NEW, /*19*/   
           cod_nuance_OLD, /*20*/   
           cod_nuance_NEW, /*21*/   
           emenda_OLD, /*22*/   
           emenda_NEW, /*23*/   
           sequencia_OLD, /*24*/   
           sequencia_NEW, /*25*/   
           usuario_conf_OLD, /*26*/   
           usuario_conf_NEW, /*27*/   
           data_conf_OLD, /*28*/   
           data_conf_NEW, /*29*/   
           hora_conf_OLD, /*30*/   
           hora_conf_NEW, /*31*/   
           transacao_ent_orig_OLD, /*32*/   
           transacao_ent_orig_NEW, /*33*/   
           deposito_original_OLD, /*34*/   
           deposito_original_NEW, /*35*/   
           sequencia_corte_OLD, /*36*/   
           sequencia_corte_NEW, /*37*/   
           numero_lacre_OLD, /*38*/   
           numero_lacre_NEW, /*39*/   
           grupo_atendimento_OLD, /*40*/   
           grupo_atendimento_NEW, /*41*/   
           procedencia_OLD, /*42*/   
           procedencia_NEW, /*43*/   
           quantidade_abono_OLD, /*44*/   
           quantidade_abono_NEW, /*45*/   
           qtde_original_prevista_OLD, /*46*/   
           qtde_original_prevista_NEW, /*47*/   
           seq_rolo_prev_romaneio_OLD, /*48*/   
           seq_rolo_prev_romaneio_NEW, /*49*/   
           nome_fornecedor_OLD, /*50*/   
           nome_fornecedor_NEW, /*51*/   
           peso_previsto_OLD, /*52*/   
           peso_previsto_NEW, /*53*/   
           peso_rolo_real_OLD, /*54*/   
           peso_rolo_real_NEW, /*55*/   
           numero_op_OLD, /*56*/   
           numero_op_NEW, /*57*/   
           estagio_op_OLD, /*58*/   
           estagio_op_NEW, /*59*/   
           sequencia_tingimento_OLD, /*60*/   
           sequencia_tingimento_NEW, /*61*/   
           sequencia_acabamento_OLD, /*62*/   
           sequencia_acabamento_NEW, /*63*/   
           rolada_OLD, /*64*/   
           rolada_NEW, /*65*/   
           seq_rolada_OLD, /*66*/   
           seq_rolada_NEW, /*67*/   
           nome_programa_OLD, /*68*/   
           nome_programa_NEW, /*69*/   
           pontos_qualidade_m2_OLD, /*70*/   
           pontos_qualidade_m2_NEW, /*71*/   
           reposicao_OLD, /*72*/   
           reposicao_NEW /*73*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.codigo_rolo, /*8*/   
           0, /*9*/
           :old.arriada, /*10*/   
           '', /*11*/
           :old.pontuacao_qualidade, /*12*/   
           0, /*13*/
           :old.ordem_engomagem, /*14*/   
           0, /*15*/
           :old.restricao, /*16*/   
           '', /*17*/
           :old.agrupador_producao, /*18*/   
           0, /*19*/
           :old.cod_nuance, /*20*/   
           '', /*21*/
           :old.emenda, /*22*/   
           0, /*23*/
           :old.sequencia, /*24*/   
           0, /*25*/
           :old.usuario_conf, /*26*/   
           '', /*27*/
           :old.data_conf, /*28*/   
           null, /*29*/
           :old.hora_conf, /*30*/   
           null, /*31*/
           :old.transacao_ent_orig, /*32*/   
           0, /*33*/
           :old.deposito_original, /*34*/   
           0, /*35*/
           :old.sequencia_corte, /*36*/   
           0, /*37*/
           :old.numero_lacre, /*38*/   
           0, /*39*/
           :old.grupo_atendimento, /*40*/   
           0, /*41*/
           :old.procedencia, /*42*/   
           0, /*43*/
           :old.quantidade_abono, /*44*/   
           0, /*45*/
           :old.qtde_original_prevista, /*46*/   
           0, /*47*/
           :old.seq_rolo_prev_romaneio, /*48*/   
           0, /*49*/
           :old.nome_fornecedor, /*50*/   
           '', /*51*/
           :old.peso_previsto, /*52*/   
           0, /*53*/
           :old.peso_rolo_real, /*54*/   
           0, /*55*/
           :old.numero_op, /*56*/   
           0, /*57*/
           :old.estagio_op, /*58*/   
           0, /*59*/
           :old.sequencia_tingimento, /*60*/   
           0, /*61*/
           :old.sequencia_acabamento, /*62*/   
           0, /*63*/
           :old.rolada, /*64*/   
           0, /*65*/
           :old.seq_rolada, /*66*/   
           0, /*67*/
           :old.nome_programa, /*68*/   
           '', /*69*/
           :old.pontos_qualidade_m2, /*70*/   
           0, /*71*/
           :old.reposicao, /*72*/   
           0 /*73*/
         );    
    end;    
 end if;    
end inter_tr_PCPT_021_log;

/
