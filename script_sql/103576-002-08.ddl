create table pedi_106_log(
    CODIGO_EMPRESA         NUMBER(3)      DEFAULT 0
    );

drop table pedi_106_log;

create table pedi_106_log(
    CODIGO_EMPRESA         NUMBER(3)      DEFAULT 0,
    CODIGO_REPRES          NUMBER(6)      DEFAULT 0,
    COL_TABELA_PRECO       NUMBER(3)      DEFAULT 0,
    MES_TABELA_PRECO       NUMBER(3)      DEFAULT 0,
    SEQ_TABELA_PRECO       NUMBER(3)      DEFAULT 0,
    ID_PLANO_PGTO          NUMBER(9)      DEFAULT 0,
    PERC_RETORNO_FUC       NUMBER(5,2)    DEFAULT 0.0,
    PERC_GRAV_PEDIDO       NUMBER(5,2)    DEFAULT 0.0,
    PARAN_EMPRESA          NUMBER(1)      DEFAULT 0,
    PEDIDO_VENDA           NUMBER(9)      DEFAULT 0,
    REPRES_CONS_PLANO_COMISS VARCHAR2(1)   DEFAULT ' ',
    TP_FILTRO_REL_PLANO    VARCHAR2(20)   DEFAULT ' ',
    USUARIO_SISTEMA        VARCHAR2(20)   DEFAULT ' ',
    NOME_PROGRAMA          VARCHAR2(20)   DEFAULT ' ',
    DATA_OCORRENCIA        DATE,
    USUARIO_REDE           VARCHAR2(20)   DEFAULT ' ',               
    MAQUINA_REDE           VARCHAR2(40)   DEFAULT ' ',               
    APLICACAO              VARCHAR2(20)   DEFAULT ' ',
    TIPO_REPRESENTANTE     VARCHAR2(14)   DEFAULT ' '
    );
     
  COMMENT ON COLUMN PEDI_106_log.CODIGO_EMPRESA is 'Codigo de empresa para relacionamento de plano de pagamento de comiss�o X Empresa X representante X tabela de Preco';  
  COMMENT ON COLUMN PEDI_106_log.CODIGO_REPRES is 'Codigo do representante para relacionamento de plano de pagamento de comiss�o X Empresa X representante X tabela de Preco';  
  COMMENT ON COLUMN PEDI_106_log.COL_TABELA_PRECO is 'Colecao da tabela de preco';  
  COMMENT ON COLUMN PEDI_106_log.MES_TABELA_PRECO is 'mes da tabela de preco';  
  COMMENT ON COLUMN PEDI_106_log.SEQ_TABELA_PRECO is 'sequencia da tabela de preco';  
  COMMENT ON COLUMN PEDI_106_log.ID_PLANO_PGTO is 'ID do plano de pagamento cadastrado na pedi_423';  
  COMMENT ON COLUMN PEDI_106_log.PERC_RETORNO_FUC is 'Percentual que a fun��o encontrou e retornou';  
  COMMENT ON COLUMN PEDI_106_log.PERC_GRAV_PEDIDO is 'Percentual que ficou gravado no pedido de venda'; 
  COMMENT ON COLUMN PEDI_106_log.PARAN_EMPRESA is 'Parametro de empresa utilizado no momento da grava��o do registro';
  COMMENT ON COLUMN PEDI_106_log.PEDIDO_VENDA is 'Pedido de venda relacionado ao percentual';
  COMMENT ON COLUMN PEDI_106_log.USUARIO_SISTEMA is 'Usuario do sistema que utilizou o processo';
  COMMENT ON COLUMN PEDI_106_log.DATA_OCORRENCIA is 'Data que ocorreu a grava��o do registro nesta tabela';
  COMMENT ON COLUMN PEDI_106_log.USUARIO_REDE is 'Usu�rio na rede que gerou a grava��o do registro nesta tabela';
  COMMENT ON COLUMN PEDI_106_log.MAQUINA_REDE is 'Maquina na rede que gerou a grava��o do registro nesta tabela';
  COMMENT ON COLUMN PEDI_106_log.APLICACAO is 'Aplica��o que gerou a grava��o do registro nesta tabela';
  COMMENT ON COLUMN PEDI_106_log.REPRES_CONS_PLANO_COMISS is 'Registra o valor do parametro da tabela de representante (S ou N)';
  COMMENT ON COLUMN PEDI_106_log.TP_FILTRO_REL_PLANO      is 'Registra a forma que encotrou o registro na tabela pedi_106 (GERAL, TABELA PRECO, REPRESENTANTE, COMPLETO ou REPRES = N) ';
  
  exec inter_pr_recompile;
