create or replace trigger TRIGGER_PEDI_110_HIST
before insert or update of 
    liquida_saldo_aprogramar
    ,permite_atu_wms
    ,grade_item
    ,gramatura
    ,numero_reserva
    ,executa_trigger
    ,cod_ped_cliente
    ,seq_original
    ,qtde_volumes
    ,nr_alternativa
    ,nr_roteiro
    ,cor_formulario_fal
    ,tipo_servico
    ,grupo_maquina
    ,subgrupo_maquina
    ,numero_maquina
    ,motivo_reprocesso
    ,destino_projeto
    ,produto_integracao
    ,prod_grade_integracao
    ,qtde_pecas_atend
    ,seq_item_reserva
    ,seq_ped_compra
    ,seq_item_sub
    ,seq_principal
    ,observacao1
    ,kanban_ped_ob
    ,kanban_pedi_ob
    ,item_ativo
    ,empenho_automatico
    ,caract
    ,caract_esp
    ,codigo_embalagem
    ,pedido_venda
    ,seq_item_pedido
    ,cd_it_pe_nivel99
    ,cd_it_pe_grupo
    ,cd_it_pe_subgrupo
    ,cd_it_pe_item
    ,codigo_deposito
    ,lote_empenhado
    ,qtde_distribuida
    ,qtde_pedida
    ,valor_unitario
    ,percentual_desc
    ,qtde_faturada
    ,nr_solicitacao
    ,qtde_afaturar
    ,situacao_fatu_it
    ,cod_cancelamento
    ,dt_cancelamento
    ,dt_inclusao
    ,qtde_rolos
    ,nr_pedido_sub
    ,deposito_sub
    ,lote_sub
    ,qtde_pedida_loja
    ,qtde_sugerida
    ,observacao2
    ,caixa_ped_ob
    ,montador_ped_ob
    ,area_ped_ob
    ,data_ent_prog
    ,caixa_pedi_ob
    ,codigo_acomp
    ,data_empenho
    ,data_solicitacao
    ,acrescimo
    ,expedidor
    ,data_leitura_coletor
    ,ja_atualizado
    ,corredor
    ,box
    ,data_sugestao
    ,perc_mao_obra
    ,caract_ing
    ,nr_sugestao
    ,agrupador_producao
    ,cod_nat_op,est_nat_op
    ,um_faturamento_um
    ,um_faturamento_qtde
    ,um_faturamento_valor
    ,largura
    ,centro_custo
or delete on pedi_110

for each row

declare
    ws_pedido               pedi_110.pedido_venda%type;
    ws_seq_item_pedido      number(3);
    ws_cd_it_pe_nivel99     varchar2(1);
    ws_cd_it_pe_grupo       varchar2(5);
    ws_cd_it_pe_subgrupo    varchar2(3);
    ws_cd_it_pe_item        varchar2(6);
    ws_tipo_ocorr           varchar2(1);
    ws_usuario_rede         varchar2(20);
    ws_maquina_rede         varchar2(40);
    ws_aplicacao            varchar2(20);
    ws_nome_programa        varchar2(20);
    ws_aplicativo           varchar2(20);
    ws_sid                  number(9);
    ws_empresa              number(3);
    ws_usuario_systextil    varchar2(250);
    ws_locale_usuario       varchar2(5);
    ws_percentual_desc      number(6,2);
    ws_qtde_sugerida_old    number(13,3);
    ws_qtde_sugerida_new    number(13,3);
    ws_qtde_pedida_old      number(13,3);
    ws_qtde_pedida_new      number(13,3);
    ws_qtde_faturada_old    number(13,3);
    ws_qtde_faturada_new    number(13,3);
    ws_qtde_afaturar_old    number(13,3);
    ws_qtde_afaturar_new    number(13,3);
    ws_seq_principal_old    number(3);
    ws_seq_principal_new    number(3);
    ws_situacao_fatu_it_old number(1);
    ws_situacao_fatu_it_new number(1);
    ws_lote_empenhado_old   number(6);
    ws_lote_empenhado_new   number(6);
    ws_codigo_acomp_old     number(6);
    ws_codigo_acomp_new     number(6);
    ws_deposito_old         number(3);
    ws_deposito_new         number(3);
    ws_qtde_distribuida_old number(15,3);
    ws_qtde_distribuida_new number(15,3);
    v_executa_trigger       number(1);
    ws_centro_custo_old     number(9);
    ws_centro_custo_new     number(9);
    ws_cod_ped_cliente_old  varchar2(30);
    ws_cod_ped_cliente_new  varchar2(30);

begin

    if inserting then

        if :new.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;

    end if;

    if updating then

        if :old.executa_trigger = 1 or :new.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;
        
    end if;

    if deleting then

        if :old.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;

    end if;

    if v_executa_trigger = 0 then

        if inserting then
        
            ws_pedido               := :new.pedido_venda;
            ws_seq_item_pedido      := :new.seq_item_pedido;
            ws_cd_it_pe_nivel99     := :new.cd_it_pe_nivel99;
            ws_cd_it_pe_grupo       := :new.cd_it_pe_grupo;
            ws_cd_it_pe_subgrupo    := :new.cd_it_pe_subgrupo;
            ws_cd_it_pe_item        := :new.cd_it_pe_item;
            ws_tipo_ocorr           := 'I';
            ws_percentual_desc      := :new.percentual_desc;
            ws_qtde_faturada_old    := 0.0;
            ws_qtde_faturada_new    := :new.qtde_faturada;
            ws_qtde_afaturar_old    := 0.0;
            ws_qtde_afaturar_new    := :new.qtde_afaturar;
            ws_qtde_sugerida_old    := 0.0;
            ws_qtde_sugerida_new    := :new.qtde_sugerida;
            ws_seq_principal_old    := 0;
            ws_seq_principal_new    := :new.seq_principal;
            ws_qtde_pedida_new      := :new.qtde_pedida;
            ws_qtde_pedida_old      := 0.00;
            ws_situacao_fatu_it_old := 0;
            ws_situacao_fatu_it_new := :new.situacao_fatu_it;
            ws_lote_empenhado_old   := 0;
            ws_lote_empenhado_new   := :new.lote_empenhado;
            ws_codigo_acomp_old     := 0;
            ws_codigo_acomp_new     := :new.codigo_acomp;
            ws_deposito_old         := 0;
            ws_deposito_new         := :new.codigo_deposito;
            ws_qtde_distribuida_old := 0.0;
            ws_qtde_distribuida_new := :new.qtde_distribuida;
            ws_centro_custo_old     := 0;
            ws_centro_custo_new     := :new.centro_custo;
            ws_cod_ped_cliente_old  := 0;
            ws_cod_ped_cliente_new  := :new.cod_ped_cliente;

        elsif updating then

            ws_pedido               := :new.pedido_venda;
            ws_seq_item_pedido      := :new.seq_item_pedido;
            ws_cd_it_pe_nivel99     := :new.cd_it_pe_nivel99;
            ws_cd_it_pe_grupo       := :new.cd_it_pe_grupo;
            ws_cd_it_pe_subgrupo    := :new.cd_it_pe_subgrupo;
            ws_cd_it_pe_item        := :new.cd_it_pe_item;
            ws_tipo_ocorr           := 'A';
            ws_percentual_desc      := :new.percentual_desc;
            ws_qtde_faturada_old    := :old.qtde_faturada;
            ws_qtde_faturada_new    := :new.qtde_faturada;
            ws_qtde_afaturar_old    := :old.qtde_afaturar;
            ws_qtde_afaturar_new    := :new.qtde_afaturar;
            ws_qtde_sugerida_old    := :old.qtde_sugerida;
            ws_qtde_sugerida_new    := :new.qtde_sugerida;
            ws_seq_principal_old    := :old.seq_principal;
            ws_seq_principal_new    := :new.seq_principal;
            ws_qtde_pedida_new      := :new.qtde_pedida;
            ws_qtde_pedida_old      := :old.qtde_pedida;
            ws_situacao_fatu_it_old := :old.situacao_fatu_it;
            ws_situacao_fatu_it_new := :new.situacao_fatu_it;
            ws_lote_empenhado_old   := :old.lote_empenhado;
            ws_lote_empenhado_new   := :new.lote_empenhado;
            ws_codigo_acomp_old     := :old.codigo_acomp;
            ws_codigo_acomp_new     := :new.codigo_acomp;
            ws_deposito_old         := :old.codigo_deposito;
            ws_deposito_new         := :new.codigo_deposito;
            ws_qtde_distribuida_old := :old.qtde_distribuida;
            ws_qtde_distribuida_new := :new.qtde_distribuida;
            ws_centro_custo_old     := :old.centro_custo;
            ws_centro_custo_new     := :new.centro_custo;
            ws_cod_ped_cliente_old  := :old.cod_ped_cliente;
            ws_cod_ped_cliente_new  := :new.cod_ped_cliente;

        elsif deleting then

            ws_pedido               := :old.pedido_venda;
            ws_seq_item_pedido      := :old.seq_item_pedido;
            ws_cd_it_pe_nivel99     := :old.cd_it_pe_nivel99;
            ws_cd_it_pe_grupo       := :old.cd_it_pe_grupo;
            ws_cd_it_pe_subgrupo    := :old.cd_it_pe_subgrupo;
            ws_cd_it_pe_item        := :old.cd_it_pe_item;
            ws_tipo_ocorr           := 'D';
            ws_percentual_desc      := :old.percentual_desc;
            ws_qtde_faturada_old    := :old.qtde_faturada;
            ws_qtde_faturada_new    := 0.0;
            ws_qtde_afaturar_old    := :old.qtde_afaturar;
            ws_qtde_afaturar_new    := 0.0;
            ws_qtde_sugerida_old    := :old.qtde_sugerida;
            ws_qtde_sugerida_new    := 0.0;
            ws_seq_principal_old    := :old.seq_principal;
            ws_seq_principal_new    := 0;
            ws_qtde_pedida_new      := 0.00;
            ws_qtde_pedida_old      := :old.qtde_pedida;
            ws_situacao_fatu_it_old := :old.situacao_fatu_it;
            ws_situacao_fatu_it_new := 0;
            ws_lote_empenhado_old   := :old.lote_empenhado;
            ws_lote_empenhado_new   := 0;
            ws_codigo_acomp_old     := :old.codigo_acomp;
            ws_codigo_acomp_new     := 0;
            ws_deposito_old         := :old.codigo_deposito;
            ws_deposito_new         := 0;
            ws_qtde_distribuida_old := :old.qtde_distribuida;
            ws_qtde_distribuida_new := 0.0;
            ws_centro_custo_old     := :old.centro_custo;
            ws_centro_custo_new     := 0;
            ws_cod_ped_cliente_old  := :old.cod_ped_cliente;
            ws_cod_ped_cliente_new  := 0;
        end if;

        -- dados do usuï¿¿rio logado
        inter_pr_dados_usuario(
            ws_usuario_rede
            ,ws_maquina_rede
            ,ws_aplicacao
            ,ws_sid
            ,ws_usuario_systextil
            ,ws_empresa
            ,ws_locale_usuario
        );

        ws_nome_programa := inter_fn_nome_programa(ws_sid);                               

        insert into pedi_110_hist(
            numero_pedido        
            ,tipo_ocorr          
            ,data_ocorr
            ,usuario_rede            
            ,maquina_rede         
            ,aplicacao
            ,percentual_desc         
            ,seq_item_pedido     
            ,qtde_sugerida_old
            ,qtde_sugerida_new      
            ,seq_principal_old    
            ,seq_principal_new
            ,qtde_pedida_new         
            ,qtde_pedida_old       
            ,situacao_fatu_it_old
            ,situacao_fatu_it_new  
            ,lote_empenhado_old
            ,lote_empenhado_new
            ,codigo_acomp_old       
            ,codigo_acomp_new     
            ,cd_it_pe_nivel99
            ,cd_it_pe_grupo
            ,cd_it_pe_subgrupo  
            ,cd_it_pe_item
            ,deposito_old    
            ,deposito_new        
            ,qtde_faturada_old
            ,qtde_faturada_new    
            ,qtde_afaturar_old    
            ,qtde_afaturar_new
            ,qtde_distribuida_old   
            ,qtde_distribuida_new
            ,nome_programa
            ,centro_custo_old        
            ,centro_custo_new     
            ,cod_ped_cliente_old
            ,cod_ped_cliente_new
        )values (
            ws_pedido               
            ,ws_tipo_ocorr         
            ,sysdate
            ,ws_usuario_rede       
            ,ws_maquina_rede     
            ,ws_aplicacao
            ,ws_percentual_desc     
            ,ws_seq_item_pedido  
            ,ws_qtde_sugerida_old
            ,ws_qtde_sugerida_new 
            ,ws_seq_principal_old
            ,ws_seq_principal_new
            ,ws_qtde_pedida_new  
            ,ws_qtde_pedida_old
            ,ws_situacao_fatu_it_old
            ,ws_situacao_fatu_it_new
            ,ws_lote_empenhado_old
            ,ws_lote_empenhado_new
            ,ws_codigo_acomp_old   
            ,ws_codigo_acomp_new
            ,ws_cd_it_pe_nivel99
            ,ws_cd_it_pe_grupo     
            ,ws_cd_it_pe_subgrupo
            ,ws_cd_it_pe_item
            ,ws_deposito_old     
            ,ws_deposito_new        
            ,ws_qtde_faturada_old
            ,ws_qtde_faturada_new
            ,ws_qtde_afaturar_old 
            ,ws_qtde_afaturar_new
            ,ws_qtde_distribuida_old 
            ,ws_qtde_distribuida_new
            ,ws_nome_programa
            ,ws_centro_custo_old 
            ,ws_centro_custo_new
            ,ws_cod_ped_cliente_old
            ,ws_cod_ped_cliente_new
        );

    end if;

end trigger_pedi_110_hist;

-- alter trigger "trigger_pedi_110_hist" enable;
