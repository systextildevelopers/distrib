
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_013_1" 
   before insert or
          delete or
          update of seq_cor, codigo_desenho, comb_desenho

on basi_013 -- Estrutura do produto de Projeto
for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_nro_reg                  number;
   v_nro_reg_844              number;
   v_nro_reg_848              number;
   v_nro_reg_855              number;

   cliente9                    basi_001.cnpj_cliente9%type;
   cliente4                    basi_001.cnpj_cliente4%type;
   cliente2                    basi_001.cnpj_cliente2%type;
   v_total_28                  number;

   v_comb_dest                 basi_847.comb_desenho%type;
   v_selec_reg                 number;
   v_gerar_combinacao          varchar2(1);
   v_alternativa_padrao_503    fatu_503.alternativa_padrao%type;
begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nro_reg := 0;

   if :new.alternativa_produto = 0
   then
      raise_application_error(-20000,'Alternativa n�o pode ser 0, deve ser informado uma alterantiva.');
   end if;

   if (updating)
   and :old.nivel_item = '1'
   then
      if   :old.codigo_desenho <> :new.codigo_desenho
      then
         begin
            select nvl(count(*),0)
            into   v_nro_reg
            from basi_039
            where basi_039.codigo_projeto      = :old.codigo_projeto
              and basi_039.alternativa_produto = :old.alternativa_produto
              and basi_039.sequencia_estrutura = :old.sequencia_estrutura
              and basi_039.sequencia_variacao  = :old.sequencia_variacao;
         end;

         if v_nro_reg > 0
         then
            begin
               delete from basi_039
               where basi_039.codigo_projeto      = :old.codigo_projeto
                 and basi_039.alternativa_produto = :old.alternativa_produto
                 and basi_039.sequencia_estrutura = :old.sequencia_estrutura
                 and basi_039.sequencia_variacao  = :old.sequencia_variacao;
            end;
         end if;
      end if;
   end if;

   if (inserting or updating)
   and :new.nivel_item = '1'
   then
      if :new.seq_cor is not null
      and :new.seq_cor <> ' '
      then
         --
         --  BUSCAR TODAS AS COMBINA��ES DO PROJETO
         --
         for reg_basi_847 in (select inter_vi_basi_843_alt.combinacao_item,
                                     inter_vi_basi_843_alt.alternativa_produto
                              from inter_vi_basi_843_alt
                              where  inter_vi_basi_843_alt.codigo_projeto      = :new.codigo_projeto
                                and (inter_vi_basi_843_alt.alternativa_produto = :new.alternativa_produto
                                or   inter_vi_basi_843_alt.alternativa_produto = 0)
                                and (inter_vi_basi_843_alt.combinacao_item     = :new.combinacao_item
                                or   :new.combinacao_item                      = '000000'))
         loop
            --
            -- VERIFICAR SE DESTINO ESTA ATIVO PARA A COMBINA��O
            --
            v_nro_reg_855 := 1;

            if :new.codigo_destino <> '00'
            then
               v_nro_reg_855 := 0;

               begin
                  select count(*)
                  into v_nro_reg_855
                  from basi_855
                  where basi_855.codigo_projeto   = :new.codigo_projeto
                    and basi_855.combinacao_item  = reg_basi_847.combinacao_item
                    and basi_855.codigo_destino   = :new.codigo_destino
                    and basi_855.alternativa_prod = reg_basi_847.alternativa_produto
                    and basi_855.utiliza_destino  = 1;
               end;
            end if;

            --
            -- VERIFICAR SE EXISTE A SEQ DE COR PARA A COMBINA��O
            --
            begin
               select count(1)
               into v_nro_reg
               from basi_847
               where basi_847.codigo_projeto   = :new.codigo_projeto
                 and basi_847.combinacao_item  = reg_basi_847.combinacao_item
                 and basi_847.alternativa_prod = reg_basi_847.alternativa_produto
                 and basi_847.seq_cor          = :new.seq_cor;
            end;

            if  v_nro_reg     = 0
            and v_nro_reg_855 > 0
            then
               begin
                  insert into basi_847 (
                     codigo_projeto,                    combinacao_item,
                     alternativa_prod,

                     seq_cor,                           codigo_desenho,
                     comb_desenho
                  ) values (
                     :new.codigo_projeto,               reg_basi_847.combinacao_item,
                     reg_basi_847.alternativa_produto,

                     :new.seq_cor,                      ' ',
                     ' '
                  );
               end;
            end if;
         end loop;
      end if;

      if  :new.codigo_desenho is not null
      and :new.codigo_desenho <> ' '
      then
         --
         --  BUSCAR TODAS AS COMBINA��ES DO PROJETO
         --
         for reg_basi_847 in (select inter_vi_basi_843_alt.combinacao_item,
                                     inter_vi_basi_843_alt.alternativa_produto
                              from inter_vi_basi_843_alt
                              where inter_vi_basi_843_alt.codigo_projeto       = :new.codigo_projeto
                                and (inter_vi_basi_843_alt.alternativa_produto = :new.alternativa_produto
                                or   inter_vi_basi_843_alt.alternativa_produto = 0)
                                and (inter_vi_basi_843_alt.combinacao_item     = :new.combinacao_item
                                or   :new.combinacao_item                      = '000000'))
         loop
            --
            -- VERIFICAR SE DESTINO ESTA ATIVO PARA A COMBINA��O
            --
            v_nro_reg_855 := 1;

            if :new.codigo_destino <> '00'
            then
               v_nro_reg_855 := 0;

               begin
                  select count(*)
                  into v_nro_reg_855
                  from basi_855
                  where basi_855.codigo_projeto   = :new.codigo_projeto
                    and basi_855.combinacao_item  = reg_basi_847.combinacao_item
                    and basi_855.codigo_destino   = :new.codigo_destino
                    and basi_855.alternativa_prod = reg_basi_847.alternativa_produto
                    and basi_855.utiliza_destino  = 1;
               end;
            end if;

            if :new.comb_desenho <> '00'
            then
               v_comb_dest := :new.comb_desenho;
            else
               begin
                  select basi_841.gerar_combinacao
                  into v_gerar_combinacao
                  from basi_841
                  where basi_841.codigo_desenho = :new.codigo_desenho;
               end;

               if v_gerar_combinacao = 'S'
               then
                  v_comb_dest := substr(reg_basi_847.combinacao_item,5,2);
               else
                  v_comb_dest := '00';
               end if;
            end if;

            --
            -- VERIFICAR SE EXISTE O DESENHO PARA A COMBINA��O
            --
            begin
               select count(1)
               into v_nro_reg
               from basi_847
               where basi_847.codigo_projeto   = :new.codigo_projeto
                 and basi_847.combinacao_item  = reg_basi_847.combinacao_item
                 and basi_847.alternativa_prod = reg_basi_847.alternativa_produto
                 and basi_847.codigo_desenho   = :new.codigo_desenho;
            end;

            if v_nro_reg = 0
            then
               begin
                  insert into basi_847 (
                     codigo_projeto,                     combinacao_item,
                     alternativa_prod,

                     seq_cor,                            cor,
                     codigo_desenho,                     comb_desenho
                  ) values (
                     :new.codigo_projeto,                reg_basi_847.combinacao_item,
                     reg_basi_847.alternativa_produto,

                     ' ',                                ' ',
                     :new.codigo_desenho,                v_comb_dest
                  );
               end;
            end if;
            --
            -- VERIFICAR SE EXISTE O DESENHO PARA A COMBINA��O
            --
            begin
               select count(1)
               into v_nro_reg
               from basi_039
               where basi_039.codigo_projeto      = :new.codigo_projeto
                 and basi_039.combinacao_item     = reg_basi_847.combinacao_item
                 and basi_039.alternativa_prod    = reg_basi_847.alternativa_produto
                 and basi_039.codigo_desenho      = :new.codigo_desenho
                 and basi_039.comb_desenho        = v_comb_dest
                 and basi_039.alternativa_produto = :new.alternativa_produto
                 and basi_039.sequencia_estrutura = :new.sequencia_estrutura
                 and basi_039.sequencia_variacao  = :new.sequencia_variacao;
            end;

            if v_comb_dest is null
            then
               v_comb_dest := ' ';
            end if;

            if v_nro_reg = 0
            then
               /*
                  Regras para ver se pode ou n�o selecionar o componente
               */
               v_selec_reg := 0;

               begin
                  select nvl(count(*),0)
                  into   v_nro_reg
                  from basi_039
                  where basi_039.codigo_projeto      = :new.codigo_projeto
                    and basi_039.combinacao_item     = reg_basi_847.combinacao_item
                    and basi_039.alternativa_prod    = reg_basi_847.alternativa_produto
                    and basi_039.alternativa_produto = :new.alternativa_produto
                    and basi_039.sequencia_estrutura = :new.sequencia_estrutura
                    and basi_039.sequencia_variacao  = :new.sequencia_variacao
                    and basi_039.selecionado          = 1;
               end;

               if v_nro_reg > 0
               then
                  v_selec_reg := 0;
               else
                  v_selec_reg := 1;
               end if;

               begin
                  select fatu_503.alternativa_padrao
                  into   v_alternativa_padrao_503
                  from fatu_503
                  where fatu_503.codigo_empresa = ws_empresa;
               exception when others then
                  v_alternativa_padrao_503 := 'N';
               end;

               if  v_alternativa_padrao_503         = 'N'
               and reg_basi_847.alternativa_produto = 0
               then
                  v_selec_reg := 0;
               end if;

               if v_comb_dest = '00'
               then
                  v_selec_reg := 0;
               end if;

               begin
                  insert into basi_039 (
                     codigo_projeto,                       combinacao_item,
                     alternativa_prod,                     codigo_desenho,
                     comb_desenho,                         alternativa_produto,
                     sequencia_estrutura,                  sequencia_variacao,
                     selecionado
                  ) values (
                     :new.codigo_projeto,                  reg_basi_847.combinacao_item,
                     reg_basi_847.alternativa_produto,     :new.codigo_desenho,
                     v_comb_dest,                          :new.alternativa_produto,
                     :new.sequencia_estrutura,             :new.sequencia_variacao,
                     v_selec_reg
                  );
               end;
            end if;
         end loop;

         --
         -- INSERIR AS COMBINA��ES DOS DESENHOS
         --
         for reg_basi_847 in (select basi_847.combinacao_item,
                                     basi_847.codigo_desenho,
                                     basi_847.comb_desenho
                              from basi_847
                              where basi_847.codigo_projeto  = :new.codigo_projeto
                                and basi_847.codigo_desenho  <> ' '
                                and basi_847.comb_desenho    <> '00'
                              group by basi_847.combinacao_item,
                                       basi_847.codigo_desenho,
                                       basi_847.comb_desenho)
         loop
            begin
               select count(1)
               into v_nro_reg_844
               from basi_844
               where basi_844.codigo_desenho = reg_basi_847.codigo_desenho
                 and basi_844.comb_desenho   = reg_basi_847.comb_desenho;
            end;

            if v_nro_reg_844 = 0
            then
               begin
                  insert into basi_844 (
                     codigo_desenho,                 comb_desenho,
                     descr_combinacao
                  ) values (
                     reg_basi_847.codigo_desenho,  reg_basi_847.comb_desenho,
                     '.'
                  );
               end;

               for reg_basi_8471 in (select basi_842.seq_cor
                                     from basi_847, basi_842
                                     where basi_847.codigo_projeto  = :new.codigo_projeto
                                       and basi_847.combinacao_item = reg_basi_847.combinacao_item
                                       and basi_842.codigo_desenho  = basi_847.codigo_desenho
                                     group by basi_842.seq_cor)
               loop
                  begin
                     select count(1)
                     into v_nro_reg_848
                     from basi_848
                     where basi_848.codigo_desenho = reg_basi_847.codigo_desenho
                       and basi_848.comb_desenho   = reg_basi_847.comb_desenho
                       and basi_848.seq_cor        = reg_basi_8471.seq_cor;
                  end;

                  if v_nro_reg_848 = 0
                  then
                     begin
                        insert into basi_848 (
                           codigo_desenho,                 comb_desenho,
                           seq_cor,                        cor
                        ) values (
                           reg_basi_847.codigo_desenho,    reg_basi_847.comb_desenho,
                           reg_basi_8471.seq_cor,          ' '
                        );
                     end;
                  end if;
               end loop;
            end if;
         end loop;
      end if;
   end if;

   if (inserting or updating)
   then
      begin
         -- Seleciona o cliente da capa do projeto
         select nvl(basi_001.cnpj_cliente9,0),   nvl(basi_001.cnpj_cliente4,0),
                nvl(basi_001.cnpj_cliente2,0)
         into   cliente9,                        cliente4,
                cliente2
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when OTHERS then
         raise_application_error (-20000, 'Projeto n�o cadastrado');
      end;

      if :new.nivel_comp = '2'
      then
         begin
            select nvl(count(1),0)
            into   v_total_28
            from basi_028
            where basi_028.nivel_componente    = :new.nivel_comp
              and basi_028.grupo_componente    = :new.grupo_comp
              and basi_028.subgrupo_componente = :new.subgru_comp
              and basi_028.item_componente     = :new.item_comp
              and basi_028.cnpj_cliente9       = cliente9
              and basi_028.cnpj_cliente4       = cliente4
              and basi_028.cnpj_cliente2       = cliente2
              and basi_028.tipo_aprovacao      = 1
              and basi_028.situacao_componente in (1,2);
         exception when OTHERS then
            v_total_28 := 0;
         end;
      else
         begin
            select nvl(count(1),0)
            into   v_total_28
            from basi_028
            where basi_028.nivel_componente    = :new.nivel_comp
              and basi_028.grupo_componente    = :new.grupo_comp
              and basi_028.subgrupo_componente = :new.subgru_comp
              and basi_028.item_componente     = :new.item_comp
              and basi_028.codigo_projeto      = :new.codigo_projeto
              and basi_028.sequencia_projeto   = :new.sequencia_projeto
              and basi_028.tipo_aprovacao      = 1
              and basi_028.situacao_componente in (1,2);
         exception when OTHERS then
            v_total_28 := 0;
         end;
      end if;

      if v_total_28 > 0
      then
         :new.data_alteracao_comp := trunc(sysdate,'DD');
         :new.hora_alteracao_comp := to_char(sysdate,'hh24:mi');
      end if;
   end if;

   if (deleting)
   then
      begin
         select nvl(count(*),0)
         into   v_nro_reg
         from basi_039
         where basi_039.codigo_projeto      = :old.codigo_projeto
           and basi_039.alternativa_produto = :old.alternativa_produto
           and basi_039.sequencia_estrutura = :old.sequencia_estrutura
           and basi_039.sequencia_variacao  = :old.sequencia_variacao;
      end;

      if v_nro_reg > 0
      then
         begin
            delete from basi_039
            where basi_039.codigo_projeto      = :old.codigo_projeto
              and basi_039.alternativa_produto = :old.alternativa_produto
              and basi_039.sequencia_estrutura = :old.sequencia_estrutura
              and basi_039.sequencia_variacao  = :old.sequencia_variacao;
         end;
      end if;
   end if;

end inter_tr_basi_013_1;

-- ALTER TRIGGER "INTER_TR_BASI_013_1" ENABLE
 

/

exec inter_pr_recompile;

