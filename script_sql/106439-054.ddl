alter table efic_100
add tipo_apont number(1);

comment on column efic_100.tipo_apont is 'Tipo de qualidade apontada na rejeição. 1(2ª Qualidade), 2(Perdas), 3(Conserto), 4(Reprocesso).'; 
