insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('ftec_f701', 'ATUALIZAÇÃO DO TIPO DE ESFESTO DO PRODUTO',0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'ftec_f701', 'basi_m030' ,1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'ATUALIZAÇÃO DO TIPO DE ESFESTO DO PRODUTO'
where hdoc_036.codigo_programa = 'ftec_f701'
  and hdoc_036.locale          = 'es_ES';

commit;
/


insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('ftec_f702', 'Cadastro Temporário das sequências dos tecidos',0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'ftec_f702', 'nenhum' ,0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Cadastro Temporário das sequências dos tecidos'
where hdoc_036.codigo_programa = 'ftec_f702'
  and hdoc_036.locale          = 'es_ES';

commit;
/

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('ftec_f703', 'Cadastro Temporário de ordens de agrupamento',0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'ftec_f703', 'nenhum' ,0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Cadastro Temporário de ordens de agrupamento'
where hdoc_036.codigo_programa = 'ftec_f703'
  and hdoc_036.locale          = 'es_ES';

commit;
/
