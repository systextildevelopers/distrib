create or replace trigger inter_tr_obrf_060_log 
after insert or delete or update 
on obrf_060 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu?rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 


   v_nome_programa := inter_fn_nome_programa(ws_sid);   
 
 if inserting 
 then 
    begin 
 
        insert into obrf_060_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           CODIGO_EMPRESA_OLD,   /*8*/ 
           CODIGO_EMPRESA_NEW,   /*9*/ 
           FORNECEDOR9_OLD,   /*10*/ 
           FORNECEDOR9_NEW,   /*11*/ 
           FORNECEDOR4_OLD,   /*12*/ 
           FORNECEDOR4_NEW,   /*13*/ 
           FORNECEDOR2_OLD,   /*14*/ 
           FORNECEDOR2_NEW,   /*15*/ 
           DOCUMENTO_OLD,   /*16*/ 
           DOCUMENTO_NEW,   /*17*/ 
           SERIE_OLD,   /*18*/ 
           SERIE_NEW,   /*19*/ 
           SEQUENCIA_NOTA_OLD,   /*20*/ 
           SEQUENCIA_NOTA_NEW,   /*21*/ 
           NIVEL_ESTRUTURA_OLD,   /*22*/ 
           NIVEL_ESTRUTURA_NEW,   /*23*/ 
           GRUPO_ESTRUTURA_OLD,   /*24*/ 
           GRUPO_ESTRUTURA_NEW,   /*25*/ 
           SUBGRUPO_ESTRUTURA_OLD,   /*26*/ 
           SUBGRUPO_ESTRUTURA_NEW,   /*27*/ 
           ITEM_ESTRUTURA_OLD,   /*28*/ 
           ITEM_ESTRUTURA_NEW,   /*29*/ 
           QTDE_NFE_OLD,   /*30*/ 
           QTDE_NFE_NEW,   /*31*/ 
           QTDE_RETORNO_DEVOL_OLD,   /*32*/ 
           QTDE_RETORNO_DEVOL_NEW,   /*33*/ 
           QTDE_RETORNO_VENDA_OLD,   /*34*/ 
           QTDE_RETORNO_VENDA_NEW,   /*35*/ 
           VALOR_UNITARIO_OLD,   /*36*/ 
           VALOR_UNITARIO_NEW,   /*37*/ 
           SALDO_FISICO_OLD,   /*38*/ 
           SALDO_FISICO_NEW,   /*39*/ 
           DATA_ENTRADA_OLD,   /*40*/ 
           DATA_ENTRADA_NEW    /*41*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.CODIGO_EMPRESA, /*9*/   
           0,/*10*/
           :new.FORNECEDOR9, /*11*/   
           0,/*12*/
           :new.FORNECEDOR4, /*13*/   
           0,/*14*/
           :new.FORNECEDOR2, /*15*/   
           0,/*16*/
           :new.DOCUMENTO, /*17*/   
           '',/*18*/
           :new.SERIE, /*19*/   
           0,/*20*/
           :new.SEQUENCIA_NOTA, /*21*/   
           '',/*22*/
           :new.NIVEL_ESTRUTURA, /*23*/   
           '',/*24*/
           :new.GRUPO_ESTRUTURA, /*25*/   
           '',/*26*/
           :new.SUBGRUPO_ESTRUTURA, /*27*/   
           '',/*28*/
           :new.ITEM_ESTRUTURA, /*29*/   
           0,/*30*/
           :new.QTDE_NFE, /*31*/   
           0,/*32*/
           :new.QTDE_RETORNO_DEVOL, /*33*/   
           0,/*34*/
           :new.QTDE_RETORNO_VENDA, /*35*/   
           0,/*36*/
           :new.VALOR_UNITARIO, /*37*/   
           0,/*38*/
           :new.SALDO_FISICO, /*39*/   
           null,/*40*/
           :new.DATA_ENTRADA /*41*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into obrf_060_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           CODIGO_EMPRESA_OLD, /*8*/   
           CODIGO_EMPRESA_NEW, /*9*/   
           FORNECEDOR9_OLD, /*10*/   
           FORNECEDOR9_NEW, /*11*/   
           FORNECEDOR4_OLD, /*12*/   
           FORNECEDOR4_NEW, /*13*/   
           FORNECEDOR2_OLD, /*14*/   
           FORNECEDOR2_NEW, /*15*/   
           DOCUMENTO_OLD, /*16*/   
           DOCUMENTO_NEW, /*17*/   
           SERIE_OLD, /*18*/   
           SERIE_NEW, /*19*/   
           SEQUENCIA_NOTA_OLD, /*20*/   
           SEQUENCIA_NOTA_NEW, /*21*/   
           NIVEL_ESTRUTURA_OLD, /*22*/   
           NIVEL_ESTRUTURA_NEW, /*23*/   
           GRUPO_ESTRUTURA_OLD, /*24*/   
           GRUPO_ESTRUTURA_NEW, /*25*/   
           SUBGRUPO_ESTRUTURA_OLD, /*26*/   
           SUBGRUPO_ESTRUTURA_NEW, /*27*/   
           ITEM_ESTRUTURA_OLD, /*28*/   
           ITEM_ESTRUTURA_NEW, /*29*/   
           QTDE_NFE_OLD, /*30*/   
           QTDE_NFE_NEW, /*31*/   
           QTDE_RETORNO_DEVOL_OLD, /*32*/   
           QTDE_RETORNO_DEVOL_NEW, /*33*/   
           QTDE_RETORNO_VENDA_OLD, /*34*/   
           QTDE_RETORNO_VENDA_NEW, /*35*/   
           VALOR_UNITARIO_OLD, /*36*/   
           VALOR_UNITARIO_NEW, /*37*/   
           SALDO_FISICO_OLD, /*38*/   
           SALDO_FISICO_NEW, /*39*/   
           DATA_ENTRADA_OLD, /*40*/   
           DATA_ENTRADA_NEW  /*41*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.CODIGO_EMPRESA,  /*8*/  
           :new.CODIGO_EMPRESA, /*9*/   
           :old.FORNECEDOR9,  /*10*/  
           :new.FORNECEDOR9, /*11*/   
           :old.FORNECEDOR4,  /*12*/  
           :new.FORNECEDOR4, /*13*/   
           :old.FORNECEDOR2,  /*14*/  
           :new.FORNECEDOR2, /*15*/   
           :old.DOCUMENTO,  /*16*/  
           :new.DOCUMENTO, /*17*/   
           :old.SERIE,  /*18*/  
           :new.SERIE, /*19*/   
           :old.SEQUENCIA_NOTA,  /*20*/  
           :new.SEQUENCIA_NOTA, /*21*/   
           :old.NIVEL_ESTRUTURA,  /*22*/  
           :new.NIVEL_ESTRUTURA, /*23*/   
           :old.GRUPO_ESTRUTURA,  /*24*/  
           :new.GRUPO_ESTRUTURA, /*25*/   
           :old.SUBGRUPO_ESTRUTURA,  /*26*/  
           :new.SUBGRUPO_ESTRUTURA, /*27*/   
           :old.ITEM_ESTRUTURA,  /*28*/  
           :new.ITEM_ESTRUTURA, /*29*/   
           :old.QTDE_NFE,  /*30*/  
           :new.QTDE_NFE, /*31*/   
           :old.QTDE_RETORNO_DEVOL,  /*32*/  
           :new.QTDE_RETORNO_DEVOL, /*33*/   
           :old.QTDE_RETORNO_VENDA,  /*34*/  
           :new.QTDE_RETORNO_VENDA, /*35*/   
           :old.VALOR_UNITARIO,  /*36*/  
           :new.VALOR_UNITARIO, /*37*/   
           :old.SALDO_FISICO,  /*38*/  
           :new.SALDO_FISICO, /*39*/   
           :old.DATA_ENTRADA,  /*40*/  
           :new.DATA_ENTRADA  /*41*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into obrf_060_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           CODIGO_EMPRESA_OLD, /*8*/   
           CODIGO_EMPRESA_NEW, /*9*/   
           FORNECEDOR9_OLD, /*10*/   
           FORNECEDOR9_NEW, /*11*/   
           FORNECEDOR4_OLD, /*12*/   
           FORNECEDOR4_NEW, /*13*/   
           FORNECEDOR2_OLD, /*14*/   
           FORNECEDOR2_NEW, /*15*/   
           DOCUMENTO_OLD, /*16*/   
           DOCUMENTO_NEW, /*17*/   
           SERIE_OLD, /*18*/   
           SERIE_NEW, /*19*/   
           SEQUENCIA_NOTA_OLD, /*20*/   
           SEQUENCIA_NOTA_NEW, /*21*/   
           NIVEL_ESTRUTURA_OLD, /*22*/   
           NIVEL_ESTRUTURA_NEW, /*23*/   
           GRUPO_ESTRUTURA_OLD, /*24*/   
           GRUPO_ESTRUTURA_NEW, /*25*/   
           SUBGRUPO_ESTRUTURA_OLD, /*26*/   
           SUBGRUPO_ESTRUTURA_NEW, /*27*/   
           ITEM_ESTRUTURA_OLD, /*28*/   
           ITEM_ESTRUTURA_NEW, /*29*/   
           QTDE_NFE_OLD, /*30*/   
           QTDE_NFE_NEW, /*31*/   
           QTDE_RETORNO_DEVOL_OLD, /*32*/   
           QTDE_RETORNO_DEVOL_NEW, /*33*/   
           QTDE_RETORNO_VENDA_OLD, /*34*/   
           QTDE_RETORNO_VENDA_NEW, /*35*/   
           VALOR_UNITARIO_OLD, /*36*/   
           VALOR_UNITARIO_NEW, /*37*/   
           SALDO_FISICO_OLD, /*38*/   
           SALDO_FISICO_NEW, /*39*/   
           DATA_ENTRADA_OLD, /*40*/   
           DATA_ENTRADA_NEW /*41*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.CODIGO_EMPRESA, /*8*/   
           0, /*9*/
           :old.FORNECEDOR9, /*10*/   
           0, /*11*/
           :old.FORNECEDOR4, /*12*/   
           0, /*13*/
           :old.FORNECEDOR2, /*14*/   
           0, /*15*/
           :old.DOCUMENTO, /*16*/   
           0, /*17*/
           :old.SERIE, /*18*/   
           '', /*19*/
           :old.SEQUENCIA_NOTA, /*20*/   
           0, /*21*/
           :old.NIVEL_ESTRUTURA, /*22*/   
           '', /*23*/
           :old.GRUPO_ESTRUTURA, /*24*/   
           '', /*25*/
           :old.SUBGRUPO_ESTRUTURA, /*26*/   
           '', /*27*/
           :old.ITEM_ESTRUTURA, /*28*/   
           '', /*29*/
           :old.QTDE_NFE, /*30*/   
           0, /*31*/
           :old.QTDE_RETORNO_DEVOL, /*32*/   
           0, /*33*/
           :old.QTDE_RETORNO_VENDA, /*34*/   
           0, /*35*/
           :old.VALOR_UNITARIO, /*36*/   
           0, /*37*/
           :old.SALDO_FISICO, /*38*/   
           0, /*39*/
           :old.DATA_ENTRADA, /*40*/   
           null /*41*/
         );    
    end;    
 end if;    
end inter_tr_obrf_060_log;

/
exec inter_pr_recompile;
/
