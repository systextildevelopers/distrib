create or replace view inter_vi_grupo_emb_ped_sug_100 as
select a.pedido_venda, sum(a.qtde_sugerida) qtde_sugerida, count(*) qtde_embarque
  from inter_vi_grupo_emb_cod_sug_100 a
group by a.pedido_venda; 
