
  CREATE OR REPLACE TRIGGER "INTER_TR_ESTQ_560_SEQ" 
   before insert on estq_560
   for each row
declare
   v_seq number;
begin
   select seq_cod_chave_estq_futuro.nextval into v_seq from dual;
   :new.cod_chave := v_seq;
end inter_tr_estq_560_seq;

-- ALTER TRIGGER "INTER_TR_ESTQ_560_SEQ" ENABLE
 

/

exec inter_pr_recompile;

