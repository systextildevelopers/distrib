alter table basi_240
add (CONSIDERA_TTD number(1) default 0);

COMMENT ON COLUMN basi_240.CONSIDERA_TTD IS 'Indica se o NCM participa das regras do TTD para produtos';

exec inter_pr_recompile;
/
