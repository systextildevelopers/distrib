create table pedi_156(
     CGC_9                          NUMBER(9,0) NOT NULL
    ,CGC_4                          NUMBER(4,0) NOT NULL
    ,CGC_2                          NUMBER(2,0) NOT NULL
    ,METRAGEM_MIN                   NUMBER(9,3) NOT NULL
    ,PEDIDOS_ACIMA_DE               NUMBER(9,3) NOT NULL
    ,PCT_PECA_MEDIA                 NUMBER(9,3) NOT NULL
    ,PCT_MENOR_COMP_ROLO            NUMBER(9,3) NOT NULL
    ,PCT_MAIOR_COMP_ROLO            NUMBER(9,3) NOT NULL
    ,PCT_MENOR_TOT_LOTE             NUMBER(9,3) NOT NULL
    ,PCT_MAIOR_TOT_LOTE             NUMBER(9,3) NOT NULL
    ,METRAGEM_MIN_SEGUNDA_QUALIDADE NUMBER(9,3) NOT NULL
    ,PCT_MENOR_PESO                 NUMBER(9,3) NOT NULL
    ,PCT_MAIOR_PESO                 NUMBER(9,3) NOT NULL
);

ALTER TABLE PEDI_156 ADD CONSTRAINT PK_PEDI_156 PRIMARY KEY (CGC_9, CGC_4, CGC_2);
