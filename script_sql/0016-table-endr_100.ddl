create table endr_100(
  cod_empresa number(3) not null,
  tipo_volume number(6) default 0
);

alter table endr_100 add constraint pk_endr_100 primary key (cod_empresa);

comment on table endr_100 is 'Cadastro de parametros para criar volumes endr_f007';
comment on column endr_100.cod_empresa is 'Empresa cadastrada com parametro diferente de criacao de volume';
comment on column endr_100.tipo_volume is 'Tipo do volume cadastrado no volume';
