alter table i_obrf_010
add nr_tentativas_consulta number(1);

alter table i_obrf_010 modify nr_tentativas_consulta default 0;

alter table i_obrf_010
add data_hora_consulta date;

exec inter_pr_recompile;
