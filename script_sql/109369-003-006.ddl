insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpb_f089', 'Valida��o de Largura de Rolos Antes de Pesar', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpb_f089', 'pcpb_menu', 1, 20, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Validaci�n de Anchura de Rodillos Antes de Pesar'
 where hdoc_036.codigo_programa = 'pcpb_f089'
   and hdoc_036.locale          = 'es_ES';
commit;

