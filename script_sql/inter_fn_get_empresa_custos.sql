CREATE OR REPLACE FUNCTION inter_fn_get_empresa_custos
   (p_empresa1 in number,
    p_mes in number,
    p_ano in number,
    p_nivel in varchar2,
    p_grupo in varchar2,
    p_subgrupo in varchar2,
    p_item in varchar2)
return number is

   v_empresa number;
   v_forma_calculo_cardex number;
   v_empresa_matriz number;
   
begin
   
   v_forma_calculo_cardex := INTER_FN_GET_PARAM_INT(p_empresa1, 'estq.formaCalcCardex');
   
   v_empresa := p_empresa1;

   if (v_forma_calculo_cardex = 3)
   then
     
     select fatu_500.codigo_matriz into v_empresa_matriz from fatu_500
     where fatu_500.codigo_empresa = p_empresa1;
     
     begin
        select tab_aux.codigo_empresa
        into   v_empresa
        from (select basi_350.codigo_empresa
              from   basi_350
              where basi_350.codigo_empresa in (select fatu_500.codigo_empresa from fatu_500
                                where fatu_500.codigo_matriz = v_empresa_matriz)
                and to_date('01/'||basi_350.mes||'/'||basi_350.ano,'dd/mm/yyyy') <= to_date('01/'||p_mes||'/'||p_ano, 'dd/mm/yyyy')
                and basi_350.nivel_estrutura = p_nivel
                and basi_350.grupo_estrutura = p_grupo
                and basi_350.subgru_estrutura = p_subgrupo
                and basi_350.item_estrutura = p_item
              order by basi_350.ano desc, basi_350.mes desc, basi_350.codigo_empresa asc) tab_aux
        where rownum < 2;
     exception
        when others
        then v_empresa := p_empresa1;
     end;
   end if;

   return(v_empresa);

end inter_fn_get_empresa_custos;
/
