alter table pcpb_665 add(motivo_aprovacao number (2) default 0);
comment on column pcpb_665.motivo_aprovacao   is 'C�digo do motivo de aprova��o';

alter table pcpb_665 add(obs_motivo_aprovacao varchar2(120) default 0);
comment on column pcpb_665.obs_motivo_aprovacao   is 'Observa��o do motivo de aprova��o';

exec inter_pr_recompile;
