
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_080" 
before update of situacao_ordem on obrf_080 for each row

declare
   v_executa_trigger number;

begin
   -- INICIO - L�gica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementa��o executada para limpeza da base de dados do cliente
   -- e replica��o dos dados para base hist�rico. (SS.38405)
   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - L�gica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger = 0
   then
      if updating
      then
         if :new.situacao_ordem = 2 -- Em processo.
         then
            -- Se existir algum item da OS com situa��o menor que 2
            -- a trigger deve atualizar estes itens para 2, eliminando problema
            -- de inconsist�ncia de capa com situa��o 2 - EM PROCESSO e item
            -- com situa��o 1 - ABERTA
            update obrf_081
            set obrf_081.sit_areceber = 2
            where obrf_081.numero_ordem = :new.numero_ordem
              and obrf_081.sit_areceber < 2;
         end if;

         if :old.situacao_ordem = 4 and :new.situacao_ordem = 3
         then
            :new.sit_fechamento := 1;
         end if;

      end if;
   end if;
end inter_tr_obrf_080;

-- ALTER TRIGGER "INTER_TR_OBRF_080" ENABLE
 

/

exec inter_pr_recompile;

