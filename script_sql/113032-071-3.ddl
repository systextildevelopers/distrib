CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_278"
BEFORE INSERT ON OBRF_278 FOR EACH ROW
DECLARE
    proximo_valor number;
BEGIN
    select ID_OBRF_278.nextval into proximo_valor from dual;
    :new.ID_LOTE_MULT := proximo_valor;
END INTER_TR_OBRF_278;

/

exec inter_pr_recompile;
