
  CREATE OR REPLACE PROCEDURE "INTER_PR_INSERE_ERRO_SPED" (p_tip_contabil_fiscal IN VARCHAR2,
                                                       p_cod_empresa  NUMBER,
                                                       p_des_erro     IN varchar2) is
--
-- Finalidade: Gravar registros na tabela de ocorrencias da geracao do sped
-- Autor.....: Cesar Anton
-- Data......: 19/11/08
--
-- Historicos
--
-- Data    Autor    Observacoes
--
w_seq_erro          NUMBER(10);

BEGIN
   BEGIN
      SELECT Max(seq_erro) + 1
      INTO   w_seq_erro
      FROM   sped_0010
      WHERE sped_0010.cod_empresa         = p_cod_empresa
        and sped_0010.tip_contabil_fiscal = p_tip_contabil_fiscal;
   EXCEPTION
      WHEN OTHERS THEN
         w_seq_erro := 1;
   END;
   IF  w_seq_erro IS NULL THEN
       w_seq_erro := 1;
   END IF;
   BEGIN
      INSERT INTO sped_0010
         (tip_contabil_fiscal
         ,cod_empresa
         ,seq_erro
         ,des_erro
         ) VALUES
         (p_tip_contabil_fiscal
         ,p_cod_empresa
         ,w_seq_erro
         ,p_des_erro);
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;
END inter_pr_insere_erro_sped;
 

/

exec inter_pr_recompile;

