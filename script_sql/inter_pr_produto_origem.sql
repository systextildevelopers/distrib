
  CREATE OR REPLACE PROCEDURE "INTER_PR_PRODUTO_ORIGEM" (
p_nivel               in  varchar2,
p_grupo               in  varchar2,
p_subgrupo            in  varchar2,
p_item                in  varchar2,
p_alternativa         in  number,
p_seq_produto         in  number,
p_ordem_planejamento  in  number,
p_pedido              in  number,
ws_nivel_origem       out varchar2,
ws_grupo_origem       out varchar2,
ws_subgrupo_origem    out varchar2,
ws_item_origem        out varchar2,
ws_alternativa_origem out number,
ws_seq_produto_origem out number)

-- Finalidade: Obter o item de determinado componente, baseado na estrutura da ordem de planejamento
-- Autor.....: Junior
-- Data......: 26/07/10
--
-- Historicos de alteracoes na procedure
--
-- Data    Autor    Observacoes

is

begin
   begin
      select tmrp_625.nivel_produto_origem,       tmrp_625.grupo_produto_origem,
             tmrp_625.subgrupo_produto_origem,    tmrp_625.item_produto_origem,
             tmrp_625.alternativa_produto_origem, tmrp_625.seq_produto_origem
      into   ws_nivel_origem,                     ws_grupo_origem,
             ws_subgrupo_origem,                  ws_item_origem,
             ws_alternativa_origem,               ws_seq_produto_origem
      from tmrp_625
      where tmrp_625.ordem_planejamento = p_ordem_planejamento
        and decode(tmrp_625.pedido_reserva,0,tmrp_625.pedido_venda,tmrp_625.pedido_reserva) = p_pedido
        and tmrp_625.nivel_produto = p_nivel
        and tmrp_625.grupo_produto = p_grupo
        and tmrp_625.subgrupo_produto = p_subgrupo
        and tmrp_625.item_produto = p_item
        and tmrp_625.alternativa_produto = p_alternativa
        and tmrp_625.seq_produto = p_seq_produto
        and rownum = 1;
      exception when others
      then
         ws_nivel_origem       := 'A';
         ws_grupo_origem       := 'AAAAA';
         ws_subgrupo_origem    := 'AAA';
         ws_item_origem        := 'AAAAAA';
         ws_alternativa_origem := 0;
         ws_seq_produto_origem := 0;
   end;
end inter_pr_produto_origem;
 

/

exec inter_pr_recompile;

