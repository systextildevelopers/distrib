CREATE OR REPLACE FUNCTION inter_fn_valida_fabricado (
    p_codigo_empresa IN basi_015.codigo_empresa%TYPE,
    p_nivel_estrutura IN basi_015.nivel_estrutura%TYPE,
    p_grupo_estrutura IN basi_015.grupo_estrutura%TYPE,
    p_subgru_estrutura IN basi_015.subgru_estrutura%TYPE,
    p_item_estrutura IN basi_015.item_estrutura%TYPE
)
RETURN BOOLEAN
IS
    v_comprado_fabric basi_015.comprado_fabric%TYPE;
BEGIN
    SELECT basi_015.comprado_fabric
    INTO v_comprado_fabric
    FROM basi_015
    WHERE basi_015.codigo_empresa = p_codigo_empresa
    AND basi_015.nivel_estrutura = p_nivel_estrutura
    AND basi_015.grupo_estrutura = p_grupo_estrutura
    AND basi_015.subgru_estrutura = p_subgru_estrutura
    AND basi_015.item_estrutura = p_item_estrutura
    AND basi_015.comprado_fabric = 2
    AND ROWNUM = 1;

    RETURN TRUE;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN FALSE;
    WHEN OTHERS THEN
        RETURN FALSE;
END inter_fn_valida_fabricado;
/
