create or replace procedure inter_pr_gera_ctb_sped_j100(p_cod_empresa   IN NUMBER,
                                                        p_exercicio     IN NUMBER,
                                                        p_cod_plano_cta IN NUMBER,
                                                        p_periodo_tri   IN NUMBER,
                                                        p_periodo_anual IN NUMBER,
                                                        p_form_apur     IN VARCHAR2,
                                                        p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar a tabela sped_cta_j100 - Balanco Patrominial
   --
   -- Historicos
   --
   -- Data    Autor    Observac?es
   --

   cursor cont610(p_cod_empresa NUMBER, p_exercicio NUMBER, p_cod_plano_cta NUMBER,p_periodo NUMBER, p_form_apur VARCHAR2) IS
      select cont_610.periodo,             cont_610.conta_reduzida,
             cont_535.conta_contabil,      cont_535.tipo_conta,
             cont_535.nivel,               cont_535.descricao,
             cont_610.saldo_periodo,       decode(cont_535.indic_natu_sped, 1, 1, 2, 2, 3, 2, 5, 1, 6, 2, 8, 1, 2) indic_natu_sped,
             nvl((select sum(c610.saldo_periodo) from cont_610 c610
              where c610.cod_empresa    = cont_610.cod_empresa
                and c610.exercicio      = cont_610.exercicio
                and c610.conta_reduzida = cont_610.conta_reduzida
                and c610.periodo        = decode(p_form_apur,'T',decode(p_periodo, 3,0, 6,3, 9,6,12,9,0),0)
        and c610.filial         = 0),0) saldo_inicial,
        decode(cont_535.nivel,5,'D','T') ind_cod_agl,
        cont_535.conta_mae cod_agl_sup
      from   cont_610, cont_535
      where  cont_610.cod_empresa    = p_cod_empresa
      and    cont_610.exercicio      = p_exercicio
      and    cont_535.cod_plano_cta  = p_cod_plano_cta
      and    cont_535.cod_reduzido   = cont_610.conta_reduzida
      and    (   cont_610.saldo_periodo  <> 0.00
              or
                 nvl((select sum(c610.saldo_periodo) from cont_610 c610
                  where c610.cod_empresa    = cont_610.cod_empresa
                    and c610.exercicio      = cont_610.exercicio
                    and c610.conta_reduzida = cont_610.conta_reduzida
                    and c610.periodo 		= decode(p_form_apur,'T',decode(p_periodo, 3,0, 6,3, 9,6,12,9,0),0)
                    and c610.filial         = 0),0) <> 0.00
             )
      and    cont_610.filial         = 0
      and    cont_535.patr_result    = 1
      and    cont_535.exige_subconta = 2
      and    cont_535.tipo_conta     = 2  -- envia apenas as contas sinteticas
      and    cont_610.periodo        = p_periodo
      order  by cont_610.conta_reduzida;

   v_erro                  EXCEPTION;

   v_ind_saldo_aglutinacao varchar2(1);
   v_valor_saldo           number(15,2);
   
   v_ind_saldo_inicial     varchar2(1);
   v_saldo_inicial         number(15,2);
   w_conta_mae             cont_535.conta_contabil%type;
   w_controle_tipo_conta   varchar2(1);
   
begin
   p_des_erro       := NULL;

   for reg_cont610 in cont610(p_cod_empresa, p_exercicio, p_cod_plano_cta,p_periodo_tri,p_form_apur)
   loop

      if reg_cont610.saldo_periodo < 0.00
      then
         v_ind_saldo_aglutinacao := 'C';
         v_valor_saldo           := reg_cont610.saldo_periodo * (-1.0);
      else
         v_ind_saldo_aglutinacao := 'D';
         v_valor_saldo           := reg_cont610.saldo_periodo;
      end if;

      if reg_cont610.saldo_inicial < 0.00
      then
         v_ind_saldo_inicial := 'C';
         v_saldo_inicial     := reg_cont610.saldo_inicial * (-1.0);
      else
         v_ind_saldo_inicial := 'D';
         v_saldo_inicial     := reg_cont610.saldo_inicial;
      end if;

      begin
        select cont_535.conta_contabil into w_conta_mae
        from cont_535
        where cont_535.cod_plano_cta = p_cod_plano_cta
        and cont_535.cod_reduzido    = reg_cont610.cod_agl_sup;
        exception
            when no_data_found then
              w_conta_mae := '';
      end;

      begin

         select distinct cont_535.tipo_conta into w_controle_tipo_conta
         from cont_535
         where cont_535.cod_plano_cta     = p_cod_plano_cta
           and cont_535.conta_contabil like reg_cont610.conta_contabil || '%' --'1.01.01.01.01%'
           and cont_535.nivel              = reg_cont610.nivel + 1;

       exception
         when others then
            w_controle_tipo_conta := '1';
      end;
     
      if w_controle_tipo_conta = 1
      then 
         reg_cont610.ind_cod_agl := 'D';
      else
         reg_cont610.ind_cod_agl := 'T';
      end if;
      
      begin

         insert into sped_ctb_j100
           (cod_empresa,                           exercicio,
            tipo_demonstracao,
            cod_aglutinacao,                       nivel_aglutinacao,
            descricao_aglutinacao,                 saldo_final,
            ind_nat_agrutinacao,                   ind_saldo_aglutinacao,
            saldo_inicial,                         ind_saldo_inicial,
            periodo,                               ind_cod_agl,
            cod_agl_sup)

      values (
            p_cod_empresa,                         p_exercicio,
            1,
            reg_cont610.conta_contabil,            reg_cont610.nivel,
            reg_cont610.descricao,                 v_valor_saldo,
            reg_cont610.indic_natu_sped,           v_ind_saldo_aglutinacao,
            v_saldo_inicial,                       v_ind_saldo_inicial,
            p_periodo_anual,                       reg_cont610.ind_cod_agl,
            w_conta_mae);

      exception
         when others then
            p_des_erro := 'Erro na inclus?o da tabela sped_ctb_j100 ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;
   end loop;

   commit;

exception
   when V_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_gera_ctb_sped_j100 ' || Chr(10) || p_des_erro;

   when others then
      p_des_erro := 'Outros erros na procedure inter_pr_gera_ctb_sped_j100 ' || Chr(10) || SQLERRM;

end inter_pr_gera_ctb_sped_j100;
