INSERT INTO EMPR_007 (
PARAM,TIPO,LABEL,FYI_MESSAGE,DEFAULT_STR,DEFAULT_INT,DEFAULT_DBL,DEFAULT_DAT
) VALUES (
'fichaTecnica.ReplicarComponentes', 0,'lb48898','fy46024','N',null,null,null);

COMMIT;

/

declare 
cursor parametro_c is select codigo_empresa from fatu_500;
tmpInt number;

begin
  for reg_parametro in parametro_c
  loop
	select count(*) into tmpInt
    from empr_008
    where empr_008.codigo_empresa = reg_parametro.codigo_empresa
      and empr_008.param = 'fichaTecnica.ReplicarComponentes' ;
    if(tmpInt = 0)
    then
      begin
        insert into empr_008 (
          codigo_empresa, param, val_str
        ) values (
          reg_parametro.codigo_empresa, 'fichaTecnica.ReplicarComponentes', 'N');
      end;
     end if; 
   end loop;
  commit;
end;
/

exec inter_pr_recompile;
