update obrf_150
set entr_cod_cep = null;
commit;

alter table obrf_150
modify entr_cod_cep varchar2(8);

exec inter_pr_recompile;
