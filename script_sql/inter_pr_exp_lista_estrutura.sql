
  CREATE OR REPLACE PROCEDURE "INTER_PR_EXP_LISTA_ESTRUTURA" 
    -- Recebe parametros para explosao da estrutura.
   (p_nivel_explode       in varchar2,
    p_grupo_explode       in varchar2,
    p_subgrupo_explode    in varchar2,
    p_item_explode        in varchar2,
    p_alternativa_explode in number,
    p_cod_empresa         in number,
    p_cod_usuario         in number,
    p_nr_solicitacao      in number,
    p_nivel_explosao      in number)
is
   -- Cria cursor para executar loop na basi_050 buscando todos
   -- os produtos da estrutura.
   cursor basi050 is
   select nivel_comp,       grupo_comp,
          sub_comp,         item_comp,
          sequencia,        consumo,
          sub_item,         item_item,
          estagio,          tipo_calculo,
          alternativa_comp, perc_cons_calc
   from basi_050
   where  basi_050.nivel_item       = p_nivel_explode
     and  basi_050.grupo_item       = p_grupo_explode
     and (basi_050.sub_item         = p_subgrupo_explode or basi_050.sub_item  = '000')
     and (basi_050.item_item        = p_item_explode     or basi_050.item_item = '000000')
     and  basi_050.alternativa_item = p_alternativa_explode
   order by basi_050.sequencia;

   -- Declara as variaveis que serao utilizadas.
   p_nivel_comp050       basi_050.nivel_comp%type := '#';
   p_grupo_comp050       basi_050.grupo_comp%type := '#####';
   p_subgru_comp050      basi_050.sub_comp%type   := '###';
   p_item_comp050        basi_050.item_comp%type  := '######';
   p_sequencia050        basi_050.sequencia%type;
   p_consumo050          basi_050.consumo%type;
   p_sub_item050         basi_050.sub_item%type   := '###';
   p_item_item050        basi_050.item_item%type  := '######';
   p_alternativa_comp_050 basi_050.alternativa_comp%type;
   p_alternativa_comp_040 basi_050.alternativa_comp%type;
   p_seq_reg             number(4) := 0;
   p_nivel_explosao_a    number(2) := 0;
   p_preco_ult_compra    basi_010.preco_ult_compra%type;
   p_alt_estrut_planej   empr_002.alternativa_estrutura_plane%type;
   p_comp_unid_med       basi_030.unidade_medida%type;
   p_narrativa           basi_010.narrativa%type;
   p_estagio             basi_050.estagio%type;


begin
   p_seq_reg             := 0;
   p_nivel_explosao_a    := 0;
   -- Inicio do loop da explosao da estrutura.
   for reg_basi050 in basi050
   loop
      -- Carrega as variaveis com os valores do cursor.
      p_nivel_comp050    := reg_basi050.nivel_comp;
      p_grupo_comp050    := reg_basi050.grupo_comp;
      p_subgru_comp050   := reg_basi050.sub_comp;
      p_item_comp050     := reg_basi050.item_comp;
      p_sequencia050     := reg_basi050.sequencia;
      p_consumo050       := reg_basi050.consumo;
      p_sub_item050      := reg_basi050.sub_item;
      p_item_item050     := reg_basi050.item_item;
      p_alternativa_comp_050  := reg_basi050.alternativa_comp;
      p_estagio               := reg_basi050.estagio;

      if p_subgru_comp050 = '000' or p_consumo050 = 0.0000000
      then
         begin
            select basi_040.sub_comp, basi_040.consumo
            into   p_subgru_comp050,  p_consumo050
            from basi_040
            where basi_040.nivel_item       = p_nivel_explode
              and basi_040.grupo_item       = p_grupo_explode
              and basi_040.sub_item         = p_subgrupo_explode
              and basi_040.item_item        = p_item_item050
              and basi_040.alternativa_item = p_alternativa_explode
              and basi_040.sequencia        = p_sequencia050;
            exception
            when others then
               p_subgru_comp050 := '000';
               p_consumo050     := 0.0000000;
         end;
      end if;

      if p_item_comp050 = '000000'
      then
         begin
            select basi_040.item_comp, basi_040.alternativa_comp
            into   p_item_comp050,     p_alternativa_comp_040
            from basi_040
            where basi_040.nivel_item       = p_nivel_explode
              and basi_040.grupo_item       = p_grupo_explode
              and basi_040.sub_item         = p_sub_item050
              and basi_040.item_item        = p_item_explode
              and basi_040.alternativa_item = p_alternativa_explode
              and basi_040.sequencia        = p_sequencia050;
            exception
            when others then
               p_item_comp050     := '000000';
               p_alternativa_comp_040 := 0;
         end;
      end if;

      -- Busca parametro de empresa executando a seguinte consistencia:
      -- 0 - Reserva Produto Principal ou 1 - Reserva Produto Alternativo
      select empr_002.alternativa_estrutura_plane
      into   p_alt_estrut_planej
      from empr_002;

      if p_alt_estrut_planej <> 0
      then
         if p_alternativa_comp_040 <> 0
         then
            p_alternativa_comp_050 := p_alternativa_comp_040;
         end if;
      end if;

      BEGIN
         select basi_010.preco_ult_compra,  basi_010.narrativa
         into p_preco_ult_compra,           p_narrativa
         from basi_010
         where basi_010.nivel_estrutura  = p_nivel_comp050
           and basi_010.grupo_estrutura  = p_grupo_comp050
           and basi_010.subgru_estrutura = p_subgru_comp050
           and basi_010.item_estrutura   = p_item_comp050;
      END;

      BEGIN
         select basi_030.unidade_medida into p_comp_unid_med
         from basi_030
         where basi_030.nivel_estrutura = p_nivel_comp050
           and basi_030.referencia      = p_grupo_comp050;
      END;

      BEGIN
         select nvl(max(rcnb_060.subgru_estrutura),0) + 1
         into p_seq_reg
         from rcnb_060
         where tipo_registro = 858
           and nr_solicitacao = p_nr_solicitacao
           and nivel_estrutura = p_cod_usuario
           and grupo_estrutura = p_cod_empresa;
      END;

      if reg_basi050.perc_cons_calc > 0.00
      then p_narrativa := p_narrativa || ' - ' || trim(to_char(reg_basi050.perc_cons_calc,'990.00') || '%');
      end if;

      -- grava na tabela temporaria
      BEGIN
         INSERT into rcnb_060
            (tipo_registro,                   nr_solicitacao,
             nivel_estrutura,                 grupo_estrutura,
             subgru_estrutura,

             nivel_estrutura_str,
             grupo_estrutura_str,             subgru_estrutura_str,
             item_estrutura_str,              campo_float_01,
             campo_numerico,                  campo_numerico2,

             campo_float_02,                  relatorio_rel,
             campo_str_01,                    campo_int_01
            )
         VALUES
            (858,                             p_nr_solicitacao,
             p_cod_usuario,                   p_cod_empresa,
             p_seq_reg,

             p_nivel_comp050,
             p_grupo_comp050,                 p_subgru_comp050,
             p_item_comp050,                  p_consumo050,
             p_alternativa_comp_050,          p_nivel_explosao,

             p_preco_ult_compra,              substr(p_narrativa,1,65),
             p_comp_unid_med,                 p_estagio

            );
    EXCEPTION
       WHEN OTHERS THEN
       raise_application_error(-20000,'Nao atualizaou tabela RCNB_060' ||
       Chr(10) || SQLERRM);
    END;

    -- Chama procedure novamente para explosao da estrutura dos componentes

    p_nivel_explosao_a := p_nivel_explosao + 1;

    inter_pr_exp_lista_estrutura(p_nivel_comp050,
                                 p_grupo_comp050,
                                 p_subgru_comp050,
                                 p_item_comp050,
                                 p_alternativa_comp_050,
                                 p_cod_empresa,
                                 p_cod_usuario,
                                 p_nr_solicitacao,
                                 p_nivel_explosao_a);

   end loop;
end INTER_PR_EXP_LISTA_ESTRUTURA;

 

/

exec inter_pr_recompile;

