create table PCPT_025_MOVTO
(
  area_ordem         NUMBER(2) default 0 not null,
  codigo_rolo        NUMBER(9) default 0 not null,
  ordem_producao     NUMBER(9) default 0 not null,
  data_movimento     DATE not null,
  qtde_movimento     NUMBER(10,3) default 0.000,
  tipo_movimento     VARCHAR2(1) default '' not null,
  usuario_systextil  VARCHAR2(250) default '',
  processo_systextil VARCHAR2(20) default '',
  data_insercao      DATE,
  usuario_rede       VARCHAR2(20) default '',
  maquina_rede       VARCHAR2(40) default '',
  aplicativo         VARCHAR2(20) default ''
);
/
exec inter_pr_recompile;
