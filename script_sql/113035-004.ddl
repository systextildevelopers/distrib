alter table crec_030 
add centro_custo number(9);

alter table crec_030 
modify centro_custo default 0;

comment on column crec_030.centro_custo IS 'Centro de Custo de Bonificacao';

declare
nro_registro number;
begin
  nro_registro := 0;

  for reg in (select rowid
              from crec_030
              where centro_custo is null)
  loop
    update crec_030
      set centro_custo = 0
    where rowid = reg.rowid;

    nro_registro := nro_registro + 1;

    if nro_registro > 1000
    then
       nro_registro := 0;
       commit;
    end if;
  end loop;

  commit;

end;

/

exec inter_pr_recompile;










