ALTER TABLE BASI_020 ADD (
PERC_ESTAMPAR NUMBER(6, 2)
);
COMMENT ON COLUMN BASI_020.PERC_ESTAMPAR IS 'Em rela��o � quantidade preparada/programada deve-se estampar a mais ou a menos';
