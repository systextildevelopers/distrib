-- Create table
create table PEDI_994
(
  NR_SOLICITACAO       NUMBER(9) default 0,
  TIPO_REGISTRO        NUMBER(3) default 0,
  PEDIDO_VENDA         NUMBER(9) default 0,
  SEQUENCIA            NUMBER(3) default 0,
  CGC_CLIENTE9         NUMBER(9) default 0,
  CGC_CLIENTE4         NUMBER(4) default 0,
  CGC_CLIENTE2         NUMBER(2) default 0,
  NOME_CLIENTE         VARCHAR2(60) default '',
  NIVEL                VARCHAR2(1) default '',
  GRUPO                VARCHAR2(5) default '',
  SUBGRUPO             VARCHAR2(3) default '',
  ITEM                 VARCHAR2(6) default '',
  ITEM_PROD            VARCHAR2(6) default '',
  DATA_TROCA           DATE,
  QTDE_PEDIDO_SEM      NUMBER(9) default 0,
  QTDE_PEDIDO_SEM_ANT  NUMBER(9) default 0,
  QTDE_ESTOQUE_040     NUMBER(9) default 0,
  QTDE_ESTOQUE_CALC    NUMBER(9) default 0,
  QTDE_RECEBER         NUMBER(9) default 0,
  QTDE_RECEBER_CONS    NUMBER(9) default 0,
  QTDE_SALDO           NUMBER(9) default 0,
  QTDE_RESERVA NUMBER(9) default 0
)

/

exec inter_pr_recompile;
