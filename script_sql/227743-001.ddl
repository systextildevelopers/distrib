drop table estq_074;

create table estq_074(
     NUMERO_CAIXA     NUMBER(9,0)   NOT NULL
    ,ENDERECO         VARCHAR2(10)  NOT NULL
    ,LOTE             NUMBER(6,0)   NOT NULL
    ,DATA_HORA_CONF   DATE          NOT NULL
    ,USUARIO          VARCHAR2(240) NOT NULL
);
