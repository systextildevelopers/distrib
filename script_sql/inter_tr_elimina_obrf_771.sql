create or replace trigger inter_tr_elimina_obrf_771 
before delete on obrf_770
for each row
begin
  begin 
    delete from obrf_771 
    where obrf_771.codigo_processo = :old.codigo_processo;
   exception when others
      then raise_application_error(-20000, sqlerrm);
   end;
end;
/
exec inter_pr_recompile;
