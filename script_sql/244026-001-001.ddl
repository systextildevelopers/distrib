ALTER TABLE pedi_053
DROP CONSTRAINT PK_PEDI_053;

DROP INDEX PK_PEDI_053;

ALTER TABLE PEDI_053
ADD CONSTRAINT PK_PEDI_053 PRIMARY KEY
(CODIGO_BANCO, TIPO_OPERACAO, NUMERO_CONTRATO, COD_COMUNICACAO);
