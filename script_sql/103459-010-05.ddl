alter table rcnb_755 
add ( PRECO_MARG_ICMS01      NUMBER(17,5),
      PRECO_MARG_ICMS02      NUMBER(17,5) ,
      PRECO_MARG_ICMS03      NUMBER(17,5) ,
      PRECO_MARG_ICMS04      NUMBER(17,5) ,
      PRECO_MARG_ICMS05      NUMBER(17,5) ,
      PRECO_MARG_ICMS06      NUMBER(17,5) ,
      PRECO_MARG_ICMS07      NUMBER(17,5) ,
      PRECO_MARG_ICMS08      NUMBER(17,5) ,
      PRECO_MARG_ICMS09      NUMBER(17,5) ,
      PRECO_MARG_ICMS10      NUMBER(17,5) ,
      PRECO_MARG_ICMS11      NUMBER(17,5) ,
      PRECO_MARG_ICMS12      NUMBER(17,5) ,
      PRECO_MARG_ICMS13      NUMBER(17,5) ,
      PRECO_AGRUP_ICMS       NUMBER(17,5) ,
      MC_AGRUP_ICMS          NUMBER(17,5) );

alter table rcnb_755 
modify ( PRECO_MARG_ICMS01       default 0.00,
         PRECO_MARG_ICMS02       default 0.00,
         PRECO_MARG_ICMS03       default 0.00,
         PRECO_MARG_ICMS04       default 0.00,
         PRECO_MARG_ICMS05       default 0.00,
         PRECO_MARG_ICMS06       default 0.00,
         PRECO_MARG_ICMS07       default 0.00,
         PRECO_MARG_ICMS08       default 0.00,
         PRECO_MARG_ICMS09       default 0.00,
         PRECO_MARG_ICMS10       default 0.00,
         PRECO_MARG_ICMS11       default 0.00,
         PRECO_MARG_ICMS12       default 0.00,
         PRECO_MARG_ICMS13       default 0.00,
         PRECO_AGRUP_ICMS        default 0.00,
         MC_AGRUP_ICMS           default 0.00);   
      
exec inter_pr_recompile;
/
