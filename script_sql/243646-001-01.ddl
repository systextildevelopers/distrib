create table estq_426(
    codigo_empresa        number(3)
    ,deposito_entrada     number(3)
    ,transacao_entrada    number(3)
    ,realizar_conferencia number(1)
);

alter table estq_426
add constraint pk_estq_426 primary key (codigo_empresa);

alter table estq_426
add constraint fk_estq_426 foreign key (codigo_empresa) references fatu_500 (codigo_empresa);
