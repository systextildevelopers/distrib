create or replace trigger trigger_pedi_117_hist
after update or delete or insert on pedi_117
referencing old as old new as new for each row
declare
   v_cgc9                     number(9);
   v_cgc4                     number(4);
   v_cgc2                     number(2);
   v_sequencia_atu            number(4);
   v_sequencia_old            number(4);
   v_valor_deposito_atu       number(13,2);
   v_valor_deposito_old       number(13,2);
   v_data_deposito_atu        date;
   v_data_deposito_old        date;
   v_saldo_final_atu          number(13,2);
   v_saldo_final_old          number(13,2);
   v_situacao_deposito_atu    number(1);
   v_situacao_deposito_old    number(1);
   v_tipo_ocorr               varchar2(1);
   v_data_ocorr               date;
   v_usuario_rede             varchar2(20);
   v_maquina_rede             varchar2(40);
   v_aplicacao                varchar2(20);

begin
   if inserting then
      v_cgc9                  := :new.cgc9;
      v_cgc4                  := :new.cgc4;
      v_cgc2                  := :new.cgc2;
      v_sequencia_atu         := :new.sequencia;
      v_sequencia_old         := null;
      v_valor_deposito_atu    := :new.valor_deposito;
      v_valor_deposito_old    := null;
      v_data_deposito_atu     := :new.data_deposito;
      v_data_deposito_old     := null;
      v_saldo_final_atu       := :new.saldo_final;
      v_saldo_final_old       := null;
      v_situacao_deposito_atu := :new.situacao_deposito;
      v_situacao_deposito_old := null;
      v_tipo_ocorr            := 'I';
   else if updating then
           v_cgc9                  := :new.cgc9;
           v_cgc4                  := :new.cgc4;
           v_cgc2                  := :new.cgc2;
           v_sequencia_atu         := :new.sequencia;
           v_sequencia_old         := :old.sequencia;
           v_valor_deposito_atu    := :new.valor_deposito;
           v_valor_deposito_old    := :old.valor_deposito;
           v_data_deposito_atu     := :new.data_deposito;
           v_data_deposito_old     := :old.data_deposito;
           v_saldo_final_atu       := :new.saldo_final;
           v_saldo_final_old       := :old.saldo_final;
           v_situacao_deposito_atu := :new.situacao_deposito;
           v_situacao_deposito_old := :old.situacao_deposito;
           v_tipo_ocorr            := 'A';
        else
           v_cgc9                  := :old.cgc9;
           v_cgc4                  := :old.cgc4;
           v_cgc2                  := :old.cgc2;
           v_sequencia_atu         := :new.sequencia;
           v_sequencia_old         := :old.sequencia;
           v_valor_deposito_atu    := :new.valor_deposito;
           v_valor_deposito_old    := :old.valor_deposito;
           v_data_deposito_atu     := :new.data_deposito;
           v_data_deposito_old     := :old.data_deposito;
           v_saldo_final_atu       := :new.saldo_final;
           v_saldo_final_old       := :old.saldo_final;
           v_situacao_deposito_atu := :new.situacao_deposito;
           v_situacao_deposito_old := :old.situacao_deposito;
           v_tipo_ocorr            := 'D';
        end if;
   end if;

   select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
   into   v_usuario_rede,      v_maquina_rede,       v_aplicacao
   from sys.v_$session
   where audsid = userenv('SESSIONID');

   insert into pedi_117_hist
   (cgc9,
    cgc4,
    cgc2,
    sequencia_atu,
    sequencia_old,
    valor_deposito_atu,
    valor_deposito_old,
    data_deposito_atu,
    data_deposito_old,
    saldo_final_atu,
    saldo_final_old,
    situacao_deposito_atu,
    situacao_deposito_old,
    tipo_ocorr,
    data_ocorr,
    usuario_rede,
    maquina_rede,
    aplicacao)
   VALUES
   (v_cgc9,
    v_cgc4,
    v_cgc2,
    v_sequencia_atu,
    v_sequencia_old,
    v_valor_deposito_atu,
    v_valor_deposito_old,
    v_data_deposito_atu,
    v_data_deposito_old,
    v_saldo_final_atu,
    v_saldo_final_old,
    v_situacao_deposito_atu,
    v_situacao_deposito_old,
    v_tipo_ocorr,
    sysdate,
    v_usuario_rede,
    v_maquina_rede,
    v_aplicacao);

end trigger_hdoc_117_hist;
/
/* versao: 1 */
