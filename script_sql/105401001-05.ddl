alter table loja_010
add (cnpj9_operadora_cartao number(9) default 0,
     cnpj4_operadora_cartao number(4) default 0,
     cnpj2_operadora_cartao number(2) default 0,
     forma_pagto_nfe        number(2) default 0,
     band_operadora         number(2) default 0);     
comment on column loja_010.cnpj9_operadora_cartao    is 'CNPJ DA OPERADORA';
comment on column loja_010.cnpj4_operadora_cartao    is 'CNPJ DA OPERADORA';
comment on column loja_010.cnpj2_operadora_cartao    is 'CNPJ DA OPERADORA';
comment on column loja_010.forma_pagto_nfe           is '01-dinheiro,02-cheque,03-cartao cred,04-cartao deb,05-cred loja,10-vale alimentacao,11-vale refeicao,12-vale presente,13-vale combustivel,14-duplicata mercantil,90-sem pagto,99-outros';
comment on column loja_010.band_operadora            is '01-Visa, 02-Mastrcard,03-American Express, 04-Sorocred, 05-Diners Club, 06-Elo, 07-Hipercard, 08-Aura, 09-Cabal, 99-Outros';

/
exec inter_pr_recompile;
