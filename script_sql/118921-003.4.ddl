create table PROG_057
(
    NIVEL_ESTRUTURA    VARCHAR2(1)  default ' ' not null,
    GRUPO_ESTRUTURA    VARCHAR2(5)  default ' ' not null,
    SUBGRUPO_ESTRUTURA VARCHAR2(3)  default ' ' not null,
    ITEM_ESTRUTURA     VARCHAR2(6)  default ' ' not null,
    NUMERO_ALTERNATIVA NUMBER(2)    default 0   not null,
    NUMERO_ROTEIRO     NUMBER(2)    default 0   not null,
    CODIGO_ESTAGIO     NUMBER(2)    default 0   not null,
    DESCR_PARTE_PECA   VARCHAR2(30) default ' ' not null,
    CONTROLE_POR_PARTE NUMBER(1)    default 0,
    TIPO_IMPRESSAO     VARCHAR2(1)  default '1'
);
/
exec inter_pr_recompile;
