CREATE SEQUENCE SEQ_COD_PARTE_MQOP_015 INCREMENT BY 1 START WITH 1 MAXVALUE 9999 MINVALUE 1 CACHE 2;


CREATE OR REPLACE TRIGGER inter_tr_mqop_015_cod_parte
BEFORE INSERT ON mqop_015 FOR EACH ROW
DECLARE
    next_value number;
BEGIN
    if :new.cod_parte is null then
        select SEQ_COD_PARTE_MQOP_015.nextval
        into next_value from dual;
        :new.cod_parte := next_value;
    end if;
END inter_tr_mqop_015_cod_parte;
