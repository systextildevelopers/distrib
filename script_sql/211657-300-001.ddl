alter table pcpt_021 add numero_op 	number(9);
alter table pcpt_021 add estagio_op number(2);

comment on column pcpt_021.numero_op 	is 'Numero da OP utilizada no pcpc_f215 para atualizar na estq_300';
comment on column pcpt_021.estagio_op 	is 'Estagio da OP utilizada no pcpc_f215 para atualizar na estq_300';

exec inter_pr_recompile;
