CREATE OR REPLACE VIEW "MANU_001_MANU_002" ("SOLICITACAO", "COD_EMPRESA", "GRUPO_MAQUINA", "SUBGRU_MAQUINA", "NUMERO_MAQUINA", "DATA_SOLICITACAO", "TIPO_SERVICO", "CAUSA_PROBLEMA", "HORA_SOLICITACAO", "SIT_MAQUINA", "SIT_SOLICITACAO", "COD_PARTE", "COD_COMP") AS
select
manu_001.solicitacao,
manu_001.cod_empresa,
manu_001.grupo_maquina,
manu_001.subgru_maquina,
manu_001.numero_maquina,
manu_001.data_solicitacao,
manu_002.tipo_servico,
manu_001.causa_problema,
manu_001.hora_solicitacao,
manu_001.sit_maquina,
manu_001.sit_solicitacao,
manu_001.cod_parte,
manu_001.cod_comp
from manu_001, manu_002
where manu_001.solicitacao = manu_002.solicitacao
and manu_001.cod_empresa = manu_002.cod_empresa;
