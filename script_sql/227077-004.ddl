alter table obrf_180 add cod_msg_ajuste_adicional number(6) default 0;
alter table obrf_180 add seq_msg_ajuste_adicional number(2) default 0;
alter table obrf_180 add loc_msg_ajuste_adicional varchar2(1) default ' ';

comment on column obrf_180.cod_msg_ajuste_adicional is 'C�digo da mensagem referente aos codigos cbnef adicionais';
comment on column obrf_180.seq_msg_ajuste_adicional is 'Sequencia da mensagem referente aos codigos cbnef adicionais';
comment on column obrf_180.loc_msg_ajuste_adicional is 'Local da mensagem referente aos codigos cbnef adicionais';
