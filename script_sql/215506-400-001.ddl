alter table fatu_050 add flag_processo_retorno number(1);

comment on column fatu_050.flag_processo_retorno is 'Indica se a nota fiscal já foi processada no processo de retorno simbólico de insumos industrializados (Apex) - null: Não foi processada ainda, 1: Já foi processada';

exec inter_pr_recompile;
