declare

    v_tag    varchar2(10);
    v_column varchar2(100);

begin

    for x in(

        SELECT 
            REGEXP_SUBSTR(
                'lb66107@#atraso_pela_renegociacao:
                ;lb66108@#bandeira_cartao:
                ;lb66109@#base_calc_comis:
                ;lb66110@#bolepix:
                ;lb66111@#calc_vl_presente:
                ;lb66112@#cartao:
                ;lb66113@#cd_centro_custo:
                ;lb66114@#chavepix:
                ;lb66115@#cli_dup_cgc_cli_o:
                ;lb66116@#cli_dup_cgc_cli_r:
                ;lb66117@#cmc7_cheque:
                ;lb66118@#codigo_contabil:
                ;lb66119@#codigo_fatura_loja:
                ;lb66120@#codigo_request:
                ;lb66121@#cod_barras:
                ;lb66122@#cod_categoria:
                ;lb66123@#cod_emp_adt:
                ;lb66124@#cod_forma_pagto:
                ;lb66125@#cod_historico:
                ;lb66126@#cod_processo:
                ;lb66127@#cod_usuario:
                ;lb66128@#comissao_lancada:
                ;lb66129@#compl_historico:
                ;lb66130@#cond_pagto_vendor:
                ;lb66131@#controle_cheque:
                ;lb66132@#cotacao_moeda:
                ;lb66133@#data_aceite_banco:
                ;lb66134@#data_alteracao_boleto:
                ;lb66135@#data_atualizacao_api:
                ;lb66136@#data_autorizacao:
                ;lb66137@#data_emissao:
                ;lb66138@#data_prorrogacao_ant:
                ;lb66139@#data_transf_tit:
                ;lb66140@#data_ult_movim_credito:
                ;lb66141@#data_ult_movim_pagto:
                ;lb66142@#data_vl_presente:
                ;lb66143@#duplic_impressa:
                ;lb66144@#estabelecimento_opera:
                ;lb66145@#estab_centralizador:
                ;lb66146@#executa_trigger:
                ;lb66147@#flag_alteracao_boleto:
                ;lb66148@#flag_marca_cheque:
                ;lb66149@#fnd_dt_remessa:
                ;lb66150@#fnd_nr_remessa:
                ;lb66151@#fnd_situacao:
                ;lb66152@#fnd_tem_espelho:
                ;lb66153@#id_conc_pagto:
                ;lb66154@#id_conc_venda:
                ;lb66155@#indice_diario:
                ;lb66156@#indice_mensal:
                ;lb66157@#linha_digitavel:
                ;lb66158@#loja_fat:
                ;lb66159@#lote_recebimento:
                ;lb66160@#mensagem_boleto:
                ;lb66161@#nr_adiantamento:
                ;lb66162@#nr_autorizacao_opera:
                ;lb66163@#nr_cupom:',
                '[^;]+', 
                1, 
                LEVEL
            ) AS column_value
        FROM 
            DUAL
        CONNECT BY 
            LEVEL <= REGEXP_COUNT(
                'lb66107@#atraso_pela_renegociacao:
                ;lb66108@#bandeira_cartao:
                ;lb66109@#base_calc_comis:
                ;lb66110@#bolepix:
                ;lb66111@#calc_vl_presente:
                ;lb66112@#cartao:
                ;lb66113@#cd_centro_custo:
                ;lb66114@#chavepix:
                ;lb66115@#cli_dup_cgc_cli_o:
                ;lb66116@#cli_dup_cgc_cli_r:
                ;lb66117@#cmc7_cheque:
                ;lb66118@#codigo_contabil:
                ;lb66119@#codigo_fatura_loja:
                ;lb66120@#codigo_request:
                ;lb66121@#cod_barras:
                ;lb66122@#cod_categoria:
                ;lb66123@#cod_emp_adt:
                ;lb66124@#cod_forma_pagto:
                ;lb66125@#cod_historico:
                ;lb66126@#cod_processo:
                ;lb66127@#cod_usuario:
                ;lb66128@#comissao_lancada:
                ;lb66129@#compl_historico:
                ;lb66130@#cond_pagto_vendor:
                ;lb66131@#controle_cheque:
                ;lb66132@#cotacao_moeda:
                ;lb66133@#data_aceite_banco:
                ;lb66134@#data_alteracao_boleto:
                ;lb66135@#data_atualizacao_api:
                ;lb66136@#data_autorizacao:
                ;lb66137@#data_emissao:
                ;lb66138@#data_prorrogacao_ant:
                ;lb66139@#data_transf_tit:
                ;lb66140@#data_ult_movim_credito:
                ;lb66141@#data_ult_movim_pagto:
                ;lb66142@#data_vl_presente:
                ;lb66143@#duplic_impressa:
                ;lb66144@#estabelecimento_opera:
                ;lb66145@#estab_centralizador:
                ;lb66146@#executa_trigger:
                ;lb66147@#flag_alteracao_boleto:
                ;lb66148@#flag_marca_cheque:
                ;lb66149@#fnd_dt_remessa:
                ;lb66150@#fnd_nr_remessa:
                ;lb66151@#fnd_situacao:
                ;lb66152@#fnd_tem_espelho:
                ;lb66153@#id_conc_pagto:
                ;lb66154@#id_conc_venda:
                ;lb66155@#indice_diario:
                ;lb66156@#indice_mensal:
                ;lb66157@#linha_digitavel:
                ;lb66158@#loja_fat:
                ;lb66159@#lote_recebimento:
                ;lb66160@#mensagem_boleto:
                ;lb66161@#nr_adiantamento:
                ;lb66162@#nr_autorizacao_opera:
                ;lb66163@#nr_cupom:', 
                ';'
            ) + 1
    )loop

        v_tag    := substr(x.column_value,1,7);
        v_column := replace(upper(substr(x.column_value,10)),'_',' ');

        -- htp.p(v_tag ||' - '|| v_column);

        begin

            insert into HDOC_870 (TAG, LOCALE, DESCRICAO, DATA_CADASTRO, FLAG_ATUALIZADO, FLAG_REPASSADO, USUARIO_TRADUCAO)
            values(v_tag, 'pt_BR', v_column, SYSDATE, 0, 4, 'CAIO.B@SYSTEXTIL.COM.BR');

            insert into HDOC_870 (TAG, LOCALE, DESCRICAO, DATA_CADASTRO, FLAG_ATUALIZADO, FLAG_REPASSADO, USUARIO_TRADUCAO)
            values(v_tag, 'es_ES', v_column, SYSDATE, 0, 4, 'CAIO.B@SYSTEXTIL.COM.BR');

        exception
            when dup_val_on_index then
                null;
        end;

    end loop;

end; --;
