create table empr_090
(
  empresa               NUMBER(3) default 0 not null,
  cnpj9                 NUMBER(9),                                                                                                                                                                            
  cnpj4                 NUMBER(4),                                                                                                                                                                           
  cnpj2                 NUMBER(2),                  
  tipo_titulo           NUMBER(2) default 0 not null,
  cod_contab            NUMBER(6),
  historico             NUMBER(4),
  origem_deb		    VARCHAR2(400) default '');

ALTER TABLE empr_090 ADD CONSTRAINT empr_090 PRIMARY KEY(empresa, tipo_titulo);
