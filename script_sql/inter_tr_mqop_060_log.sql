
  CREATE OR REPLACE TRIGGER "INTER_TR_MQOP_060_LOG" 
   after insert or delete or update
       of nivel_estrutura, grupo_estrutura, subgru_estrutura,    item_estrutura,
          tipo_processo,   seq_operacao,    qtde_maquinas,       velocidade,
          qtde_rolos,      qtde_minima,     capac_producao,      qtde_quilos,
          metros_por_lote, tempo_producao,  tempo_disponivel,    relacao_banho,
          absorcao,        rpm,             velocidade_molinete, temperatura,
          giros_cordas,    seq_programacao, tipo_tingimento,     lote_mult_rolos,
          lote_mult_peso

   on mqop_060
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   long_aux                  varchar2(2000);
   produto                   varchar2(20);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      produto := :new.nivel_estrutura   || '.' ||
                 :new.grupo_estrutura   || '.' ||
                 :new.subgru_estrutura  || '.' ||
                 :new.item_estrutura;

      INSERT INTO hist_100
         ( tabela,                 operacao,
           data_ocorr,             aplicacao,
           usuario_rede,           maquina_rede,
           str02,                  str03,
           str04,                  long01
         )
      VALUES
         ( 'MQOP_060',             'I',
           sysdate,                ws_aplicativo,
           ws_usuario_rede,        ws_maquina_rede,
           :new.maq_sbgr_grupo_mq, :new.maq_sbgr_sbgr_maq,
           produto,
           '                              '||
           inter_fn_buscar_tag('lb34868#CAPACIDADE DE PRODUCAO',ws_locale_usuario,ws_usuario_systextil) ||
                                    chr(10)               ||
                                    chr(10)               ||
           inter_fn_buscar_tag('lb16624#CODIGO PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.nivel_estrutura   || '.' ||
                                   :new.grupo_estrutura   || '.' ||
                                   :new.subgru_estrutura  || '.' ||
                                   :new.item_estrutura    ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb05953#TP:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.tipo_processo     ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb06610#SEQ.OPERACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.seq_operacao      ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb10847#QUANTIDADE DE MAQ.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.qtde_maquinas     ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb08656#VELOCIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.velocidade        ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb34869#ROLOS MAX.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.qtde_rolos        ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb34870#LOTE MIN.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.qtde_minima       ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb34871#QUILOS MAX.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.capac_producao    ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb34870#LOTE MIN.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.qtde_quilos       ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb34872#METROS LOTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.metros_por_lote   ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb34873#TEMPO PRODUCAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.tempo_producao    ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb34874#TEMPO  TRAB.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.tempo_disponivel  ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb00926#REL. BANHO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.relacao_banho     ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb01607#ABSORCAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.absorcao          ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb10845#R.P.M.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.rpm               ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb10448#VELOC. MOLINETE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.velocidade_molinete ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb09695#TEMPERATURA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.temperatura       ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb06969#GIROS /CORDAS:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.giros_cordas      ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb08392#SEQ. PROG.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.seq_programacao   ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb01023#TIPO TINGIMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.tipo_tingimento   ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb12154#LOTE MULT. ROLOS:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.lote_mult_rolos   ||
                                   chr(10)                ||
           inter_fn_buscar_tag('lb12143#LOTE MULT. QUILOS:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                   :new.lote_mult_peso);
   end if;

   if updating and
      (:old.nivel_estrutura       <>     :new.nivel_estrutura     or
       :old.grupo_estrutura     <>     :new.grupo_estrutura     or
       :old.subgru_estrutura      <>     :new.subgru_estrutura    or
       :old.item_estrutura      <>     :new.item_estrutura      or
       :old.tipo_processo         <>     :new.tipo_processo       or
       :old.seq_operacao        <>     :new.seq_operacao        or
       :old.qtde_maquinas         <>     :new.qtde_maquinas       or
       :old.velocidade            <>     :new.velocidade          or
       :old.qtde_rolos            <>     :new.qtde_rolos          or
       :old.qtde_minima           <>     :new.qtde_minima         or
       :old.capac_producao        <>     :new.capac_producao      or
       :old.qtde_quilos           <>     :new.qtde_quilos         or
       :old.metros_por_lote     <>     :new.metros_por_lote     or
       :old.tempo_producao      <>     :new.tempo_producao      or
       :old.tempo_disponivel    <>     :new.tempo_disponivel    or
       :old.relacao_banho         <>     :new.relacao_banho       or
       :old.absorcao                <>     :new.absorcao            or
       :old.rpm                     <>     :new.rpm                 or
       :old.velocidade_molinete <>     :new.velocidade_molinete or
       :old.temperatura           <>     :new.temperatura         or
       :old.giros_cordas          <>     :new.giros_cordas        or
       :old.seq_programacao     <>     :new.seq_programacao     or
       :old.tipo_tingimento     <>     :new.tipo_tingimento     or
       :old.lote_mult_rolos     <>     :new.lote_mult_rolos     or
       :old.lote_mult_peso      <>     :new.lote_mult_peso
      )
   then
      produto := :new.nivel_estrutura   || '.' ||
                 :new.grupo_estrutura   || '.' ||
                 :new.subgru_estrutura  || '.' ||
                 :new.item_estrutura;

      long_aux := long_aux ||
                 '                              '||
                 inter_fn_buscar_tag('lb34868#CAPACIDADE DE PRODUCAO',ws_locale_usuario,ws_usuario_systextil) ||
                  chr(10)                    ||
                  chr(10);
      if :old.nivel_estrutura   <>     :new.nivel_estrutura     or
         :old.grupo_estrutura     <>     :new.grupo_estrutura     or
         :old.subgru_estrutura  <>     :new.subgru_estrutura
      then
         long_aux := long_aux ||
            inter_fn_buscar_tag('lb16624#CODIGO PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                              :old.nivel_estrutura   || '.' ||
                                              :old.grupo_estrutura   || '.' ||
                                              :old.subgru_estrutura  || '.' ||
                                              :old.item_estrutura    || ' -> ' ||
                                              :new.nivel_estrutura   || '.' ||
                                              :new.grupo_estrutura   || '.' ||
                                              :new.subgru_estrutura  || '.' ||
                                              :new.item_estrutura    ||
                                              chr(10);
      end if;

      if :old.tipo_processo <> :new.tipo_processo
      then
          long_aux := long_aux ||
             inter_fn_buscar_tag('lb05953#TP:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                              :old.tipo_processo  || ' -> ' ||
                                              :new.tipo_processo  ||
                                              chr(10);
       end if;

       if :old.seq_operacao <> :new.seq_operacao
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb06610#SEQ.OPERACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.seq_operacao || ' -> ' ||
                                               :new.seq_operacao ||
                                                chr(10);
       end if;

       if :old.qtde_maquinas <> :new.qtde_maquinas
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb10847#QUANTIDADE DE MAQ.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.qtde_maquinas || ' -> ' ||
                                               :new.qtde_maquinas ||
                                               chr(10);
       end if;

       if :old.velocidade <> :new.velocidade
       then
            long_aux := long_aux ||
               inter_fn_buscar_tag('lb08656#VELOCIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                                :old.velocidade || ' -> ' ||
                                                :new.velocidade ||
                                                chr(10);
       end if;

       if :old.qtde_rolos <> :new.qtde_rolos
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb34869#ROLOS MAX.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.qtde_rolos || ' -> ' ||
                                               :new.qtde_rolos ||
                                               chr(10);
       end if;

       if :old.qtde_minima <> :new.qtde_minima
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb34870#LOTE MIN.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.qtde_minima || ' -> ' ||
                                               :new.qtde_minima ||
                                               chr(10);
       end if;

       if :old.capac_producao <> :new.capac_producao
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb34871#QUILOS MAX.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.capac_producao || ' -> ' ||
                                               :new.capac_producao ||
                                               chr(10);
       end if;

       if :old.qtde_quilos <> :new.qtde_quilos
       then
           long_aux := long_aux ||
                       'LOTE MIN........: ' || :old.qtde_quilos || ' -> ' ||
                                               :new.qtde_quilos ||
                                               chr(10);
       end if;

       if :old.metros_por_lote <> :new.metros_por_lote
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb34872#METROS LOTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.metros_por_lote || ' -> ' ||
                                               :new.metros_por_lote ||
                                               chr(10);
       end if;

       if :old.tempo_producao <> :new.tempo_producao
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb34873#TEMPO PRODUCAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.tempo_producao || ' -> ' ||
                                               :new.tempo_producao ||
                                               chr(10);
       end if;

       if :old.tempo_disponivel <> :new.tempo_disponivel
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb34874#TEMPO  TRAB.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.tempo_disponivel || ' -> ' ||
                                               :new.tempo_disponivel ||
                                               chr(10);
       end if;

       if :old.relacao_banho <> :new.relacao_banho
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb00926#REL. BANHO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.relacao_banho || ' -> ' ||
                                               :new.relacao_banho ||
                                               chr(10);
       end if;

       if :old.absorcao <> :new.absorcao
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb01607#ABSORCAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.absorcao || ' -> ' ||
                                               :new.absorcao ||
                                               chr(10);
       end if;

       if :old.rpm <> :new.rpm
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb10845#R.P.M.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.rpm || ' -> ' ||
                                               :new.rpm ||
                                               chr(10);
       end if;

       if :old.velocidade_molinete <> :new.velocidade_molinete
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb10448#VELOC. MOLINETE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.velocidade_molinete || ' -> ' ||
                                               :new.velocidade_molinete ||
                                               chr(10);
       end if;

       if :old.temperatura <> :new.temperatura
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb09695#TEMPERATURA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                               :old.temperatura || ' -> ' ||
                                               :new.temperatura ||
                                               chr(10);
       end if;

       if :old.giros_cordas <> :new.giros_cordas
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb06969#GIROS /CORDAS:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                                :old.giros_cordas || ' -> ' ||
                                                :new.giros_cordas ||
                                                chr(10);
       end if;

       if :old.seq_programacao <> :new.seq_programacao
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb08392#SEQ. PROG.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                                 :old.seq_programacao || ' -> ' ||
                                                 :new.seq_programacao ||
                                                 chr(10);
       end if;

       if :old.tipo_tingimento <> :new.tipo_tingimento
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb01023#TIPO TINGIMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                                :old.tipo_tingimento || ' -> ' ||
                                                :new.tipo_tingimento ||
                                                chr(10);
       end if;

       if :old.lote_mult_rolos <> :new.lote_mult_rolos
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb12154#LOTE MULT. ROLOS:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                                :old.lote_mult_rolos || ' -> ' ||
                                                :new.lote_mult_rolos ||
                                                chr(10);

       end if;

       if :old.lote_mult_peso <> :new.lote_mult_peso
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb12143#LOTE MULT. QUILOS:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                                :old.lote_mult_peso || ' -> ' ||
                                                :new.lote_mult_peso ||
                                                chr(10);
       end if;

      INSERT INTO hist_100
         ( tabela,                 operacao,
           data_ocorr,             aplicacao,
           usuario_rede,           maquina_rede,
           str02,                  str03,
           str04,                  long01
         )
      VALUES
         ( 'MQOP_060',             'A',
           sysdate,                ws_aplicativo,
           ws_usuario_rede,        ws_maquina_rede,
           :new.maq_sbgr_grupo_mq, :new.maq_sbgr_sbgr_maq,
           produto,                long_aux
        );
   end if;

   if deleting
   then
      produto := :old.nivel_estrutura   || '.' ||
                 :old.grupo_estrutura   || '.' ||
                 :old.subgru_estrutura  || '.' ||
                 :old.item_estrutura;


      INSERT INTO hist_100
         ( tabela,                 operacao,
           data_ocorr,             aplicacao,
           usuario_rede,           maquina_rede,
           str02,                  str03,
           str04
         )
      VALUES
         ( 'MQOP_060',             'D',
           sysdate,                ws_aplicativo,
           ws_usuario_rede,        ws_maquina_rede,
           :old.maq_sbgr_grupo_mq, :old.maq_sbgr_sbgr_maq,
           produto
        );
   end if;

end inter_tr_mqop_060_log;

-- ALTER TRIGGER "INTER_TR_MQOP_060_LOG" ENABLE
 

/

exec inter_pr_recompile;

