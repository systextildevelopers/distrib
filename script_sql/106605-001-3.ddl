alter table pedi_080
add ind_beneficio_icms varchar2(1) default 'N';

comment on column pedi_080.ind_beneficio_icms is 'S - Indica que a nota fiscal com esta natureza de operação entrará no ajuste de apuração de icms ou N - para não';
/
