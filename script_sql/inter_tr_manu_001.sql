
  CREATE OR REPLACE TRIGGER "INTER_TR_MANU_001" 
  before insert or
  update of solicitacao, cod_empresa, grupo_maquina, subgru_maquina,
	numero_maquina, tipo_problema1, tipo_problema2, tipo_problema3,
	tipo_problema4, tipo_problema5, observacao, sit_maquina,
	causa_problema, sit_solicitacao, solicitante, responsavel,
	divisao_producao, data_solicitacao, hora_solicitacao, usuario,
	senha, data, obs_reabrir, descricao,
	projeto, subprojeto, servico, c_custo_resp,
	motivo_manu, obs_manutencao, tipo_solicitacao, forma_controle,
	data_exec_ori, hora_exec_ori, executa_trigger
  on manu_001
  for each row

begin
    -- Author  : Antony
    -- Created : 06/02/09

   -- Logica para gravar a hora temino e hora inicio com uma data valida.
   -- A data definida '16/11/1989', foi definida com base da data de gravacao padrao do Vision.
   if (updating  and :new.hora_exec_ori is not null)
   or (inserting and :new.hora_exec_ori is not null)
   then
      :new.hora_exec_ori := to_date('16/11/1989 '||to_char(:new.hora_exec_ori,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

end inter_tr_manu_001;
-- ALTER TRIGGER "INTER_TR_MANU_001" ENABLE
 

/

exec inter_pr_recompile;

