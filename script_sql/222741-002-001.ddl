insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('crec_f426', 'Históricos de abatimento', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'crec_f426', 'crec_menu', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Históricos de abatimento'
 where hdoc_036.codigo_programa = 'crec_f426'
   and hdoc_036.locale          = 'es_ES';

commit;
