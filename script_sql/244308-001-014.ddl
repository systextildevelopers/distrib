CREATE TABLE  "PROJ_090" 
   (	"SEQ_IMPORTACAO_PLANO" NUMBER(30,0) NOT NULL ENABLE, 
	"CODIGO_EMPRESA" NUMBER(3,0) NOT NULL ENABLE, 
	"ANO" NUMBER(4,0) NOT NULL ENABLE, 
	"NIVEL" VARCHAR2(1) NOT NULL ENABLE, 
	"GRUPO" VARCHAR2(5) NOT NULL ENABLE, 
	"SUBGRUPO" VARCHAR2(3) NOT NULL ENABLE, 
	"ITEM" VARCHAR2(6) NOT NULL ENABLE, 
	"JAN_QUINZENA_1" NUMBER(30,0) NOT NULL ENABLE, 
	"JAN_QUINZENA_2" NUMBER(30,0) NOT NULL ENABLE, 
	"FEV_QUINZENA_1" NUMBER(30,0) NOT NULL ENABLE, 
	"FEV_QUINZENA_2" NUMBER(30,0) NOT NULL ENABLE, 
	"MAR_QUINZENA_1" NUMBER(30,0) NOT NULL ENABLE, 
	"MAR_QUINZENA_2" NUMBER(30,0) NOT NULL ENABLE, 
	"ABR_QUINZENA_1" NUMBER(30,0) NOT NULL ENABLE, 
	"ABR_QUINZENA_2" NUMBER(30,0) NOT NULL ENABLE, 
	"MAI_QUINZENA_1" NUMBER(30,0) NOT NULL ENABLE, 
	"MAI_QUINZENA_2" NUMBER(30,0) NOT NULL ENABLE, 
	"JUN_QUINZENA_1" NUMBER(30,0) NOT NULL ENABLE, 
	"JUN_QUINZENA_2" NUMBER(30,0) NOT NULL ENABLE, 
	"JUL_QUINZENA_1" NUMBER(30,0) NOT NULL ENABLE, 
	"JUL_QUINZENA_2" NUMBER(30,0) NOT NULL ENABLE, 
	"AGO_QUINZENA_1" NUMBER(30,0) NOT NULL ENABLE, 
	"AGO_QUINZENA_2" NUMBER(30,0) NOT NULL ENABLE, 
	"SET_QUINZENA_1" NUMBER(30,0) NOT NULL ENABLE, 
	"SET_QUINZENA_2" NUMBER(30,0) NOT NULL ENABLE, 
	"OUT_QUINZENA_1" NUMBER(30,0) NOT NULL ENABLE, 
	"OUT_QUINZENA_2" NUMBER(30,0) NOT NULL ENABLE, 
	"NOV_QUINZENA_1" NUMBER(30,0) NOT NULL ENABLE, 
	"NOV_QUINZENA_2" NUMBER(30,0) NOT NULL ENABLE, 
	"DEZ_QUINZENA_1" NUMBER(30,0) NOT NULL ENABLE, 
	"DEZ_QUINZENA_2" NUMBER(30,0) NOT NULL ENABLE, 
	"USUARIO_ATUALIZACAO" VARCHAR2(30) NOT NULL ENABLE, 
	"DATA_ATUALIZACAO" DATE NOT NULL ENABLE, 
	 CONSTRAINT "PK_PROJ_090" PRIMARY KEY ("SEQ_IMPORTACAO_PLANO")
  USING INDEX  ENABLE
   )
/
ALTER TABLE  "PROJ_090" ADD CONSTRAINT "REF_PROJ_090_BASI_010" FOREIGN KEY ("NIVEL", "GRUPO", "SUBGRUPO", "ITEM")
	  REFERENCES  "BASI_010" ("NIVEL_ESTRUTURA", "GRUPO_ESTRUTURA", "SUBGRU_ESTRUTURA", "ITEM_ESTRUTURA") ENABLE
/

CREATE OR REPLACE EDITIONABLE TRIGGER  "INTER_TR_PROJ_090" 
before insert or update on PROJ_090 for each row
begin
 if  inserting then 
     if  :new.seq_importacao_plano is null then 
         :new.seq_importacao_plano:= seq_proj_090.nextval;
     end if;
 end if;
 :new.usuario_atualizacao := nvl(sys_context('APEX$SESSION','APP_USER'),user);
 :new.data_atualizacao    := sysdate;   
end INTER_TR_PROJ_090;

/
ALTER TRIGGER  "INTER_TR_PROJ_090" ENABLE
/

CREATE OR REPLACE EDITIONABLE TRIGGER  "INTER_TR_PROJ_090_LOG" 
after insert or delete or update 
on PROJ_090 
for each row 
declare 
 ws_usuario_rede      varchar2 (2000); 
 ws_maquina_rede      varchar2 (2000); 
 ws_aplicativo        varchar2 (2000); 
 ws_sid               number; 
 ws_empresa           number; 
 ws_usuario_systextil varchar2 (2000); 
 ws_locale_usuario    varchar2 (2000); 
 ws_nome_programa     varchar2 (2000); 
 ws_tipo_ocorr        char     (1);

begin
-- dados do usuario logado
 inter_pr_dados_usuario (ws_usuario_rede,ws_maquina_rede,ws_aplicativo,ws_sid,ws_usuario_systextil,ws_empresa,ws_locale_usuario); 
 ws_nome_programa := inter_fn_nome_programa(ws_sid);

 if    inserting then
       ws_tipo_ocorr := 'I';
 elsif updating then
       ws_tipo_ocorr := 'A';
 else
       ws_tipo_ocorr := 'D';
 end if;

 insert into PROJ_090_log 
            (tipo_ocorr
           , data_ocorr
           , hora_ocorr
           , usuario_rede
           , maquina_rede
           , aplicacao
           , usuario_sistema
           , nome_programa
           , seq_importacao_plano_new
           , codigo_empresa_new
           , ano_new
           , nivel_new
           , grupo_new
           , subgrupo_new
           , item_new
           , jan_quinzena_1_new
           , jan_quinzena_2_new
           , fev_quinzena_1_new
           , fev_quinzena_2_new
           , mar_quinzena_1_new
           , mar_quinzena_2_new
           , abr_quinzena_1_new
           , abr_quinzena_2_new
           , mai_quinzena_1_new
           , mai_quinzena_2_new
           , jun_quinzena_1_new
           , jun_quinzena_2_new
           , jul_quinzena_1_new
           , jul_quinzena_2_new
           , ago_quinzena_1_new
           , ago_quinzena_2_new
           , set_quinzena_1_new
           , set_quinzena_2_new
           , out_quinzena_1_new
           , out_quinzena_2_new
           , nov_quinzena_1_new
           , nov_quinzena_2_new
           , dez_quinzena_1_new
           , dez_quinzena_2_new
           , seq_importacao_plano_old
           , codigo_empresa_old
           , ano_old
           , nivel_old
           , grupo_old
           , subgrupo_old
           , item_old
           , jan_quinzena_1_old
           , jan_quinzena_2_old
           , fev_quinzena_1_old
           , fev_quinzena_2_old
           , mar_quinzena_1_old
           , mar_quinzena_2_old
           , abr_quinzena_1_old
           , abr_quinzena_2_old
           , mai_quinzena_1_old
           , mai_quinzena_2_old
           , jun_quinzena_1_old
           , jun_quinzena_2_old
           , jul_quinzena_1_old
           , jul_quinzena_2_old
           , ago_quinzena_1_old
           , ago_quinzena_2_old
           , set_quinzena_1_old
           , set_quinzena_2_old
           , out_quinzena_1_old
           , out_quinzena_2_old
           , nov_quinzena_1_old
           , nov_quinzena_2_old
           , dez_quinzena_1_old
           , dez_quinzena_2_old
           , usuario_atualizacao
           , data_atualizacao)
    values ( ws_tipo_ocorr                                                                    --tipo_ocorr
           , sysdate                                                                          --data_ocorr
           , sysdate                                                                          --hora_ocorr
           , ws_usuario_rede                                                                  --usuario_rede
           , ws_maquina_rede                                                                  --maquina_rede
           , ws_aplicativo                                                                    --aplicacao
           , ws_usuario_systextil                                                             --usuario_sistema
           , ws_nome_programa                                                                 --nome_programa
           , case when ws_tipo_ocorr in ('I','A') then :new.seq_importacao_plano else null end--seq_importacao_plano_new
           , case when ws_tipo_ocorr in ('I','A') then :new.codigo_empresa       else null end--codigo_empresa_new
           , case when ws_tipo_ocorr in ('I','A') then :new.ano                  else null end--ano_new
           , case when ws_tipo_ocorr in ('I','A') then :new.nivel                else null end--nivel_new
           , case when ws_tipo_ocorr in ('I','A') then :new.grupo                else null end--grupo_new
           , case when ws_tipo_ocorr in ('I','A') then :new.subgrupo             else null end--subgrupo_new
           , case when ws_tipo_ocorr in ('I','A') then :new.item                 else null end--item_new
           , case when ws_tipo_ocorr in ('I','A') then :new.jan_quinzena_1       else null end--jan_quinzena_1_new
           , case when ws_tipo_ocorr in ('I','A') then :new.jan_quinzena_2       else null end--jan_quinzena_2_new
           , case when ws_tipo_ocorr in ('I','A') then :new.fev_quinzena_1       else null end--fev_quinzena_1_new
           , case when ws_tipo_ocorr in ('I','A') then :new.fev_quinzena_2       else null end--fev_quinzena_2_new
           , case when ws_tipo_ocorr in ('I','A') then :new.mar_quinzena_1       else null end--mar_quinzena_1_new
           , case when ws_tipo_ocorr in ('I','A') then :new.mar_quinzena_2       else null end--mar_quinzena_2_new
           , case when ws_tipo_ocorr in ('I','A') then :new.abr_quinzena_1       else null end--abr_quinzena_1_new
           , case when ws_tipo_ocorr in ('I','A') then :new.abr_quinzena_2       else null end--abr_quinzena_2_new
           , case when ws_tipo_ocorr in ('I','A') then :new.mai_quinzena_1       else null end--mai_quinzena_1_new
           , case when ws_tipo_ocorr in ('I','A') then :new.mai_quinzena_2       else null end--mai_quinzena_2_new
           , case when ws_tipo_ocorr in ('I','A') then :new.jun_quinzena_1       else null end--jun_quinzena_1_new
           , case when ws_tipo_ocorr in ('I','A') then :new.jun_quinzena_2       else null end--jun_quinzena_2_new
           , case when ws_tipo_ocorr in ('I','A') then :new.jul_quinzena_1       else null end--jul_quinzena_1_new
           , case when ws_tipo_ocorr in ('I','A') then :new.jul_quinzena_2       else null end--jul_quinzena_2_new
           , case when ws_tipo_ocorr in ('I','A') then :new.ago_quinzena_1       else null end--ago_quinzena_1_new
           , case when ws_tipo_ocorr in ('I','A') then :new.ago_quinzena_2       else null end--ago_quinzena_2_new
           , case when ws_tipo_ocorr in ('I','A') then :new.set_quinzena_1       else null end--set_quinzena_1_new
           , case when ws_tipo_ocorr in ('I','A') then :new.set_quinzena_2       else null end--set_quinzena_2_new
           , case when ws_tipo_ocorr in ('I','A') then :new.out_quinzena_1       else null end--out_quinzena_1_new
           , case when ws_tipo_ocorr in ('I','A') then :new.out_quinzena_2       else null end--out_quinzena_2_new
           , case when ws_tipo_ocorr in ('I','A') then :new.nov_quinzena_1       else null end--nov_quinzena_1_new
           , case when ws_tipo_ocorr in ('I','A') then :new.nov_quinzena_2       else null end--nov_quinzena_2_new
           , case when ws_tipo_ocorr in ('I','A') then :new.dez_quinzena_1       else null end--dez_quinzena_1_new
           , case when ws_tipo_ocorr in ('I','A') then :new.dez_quinzena_2       else null end--dez_quinzena_2_new
           , case when ws_tipo_ocorr in ('A','D') then :old.seq_importacao_plano else null end--seq_importacao_plano_old
           , case when ws_tipo_ocorr in ('A','D') then :old.codigo_empresa       else null end--codigo_empresa_old
           , case when ws_tipo_ocorr in ('A','D') then :old.ano                  else null end--ano_old
           , case when ws_tipo_ocorr in ('A','D') then :old.nivel                else null end--nivel_old
           , case when ws_tipo_ocorr in ('A','D') then :old.grupo                else null end--grupo_old
           , case when ws_tipo_ocorr in ('A','D') then :old.subgrupo             else null end--subgrupo_old
           , case when ws_tipo_ocorr in ('A','D') then :old.item                 else null end--item_old
           , case when ws_tipo_ocorr in ('A','D') then :old.jan_quinzena_1       else null end--jan_quinzena_1_old
           , case when ws_tipo_ocorr in ('A','D') then :old.jan_quinzena_2       else null end--jan_quinzena_2_old
           , case when ws_tipo_ocorr in ('A','D') then :old.fev_quinzena_1       else null end--fev_quinzena_1_old
           , case when ws_tipo_ocorr in ('A','D') then :old.fev_quinzena_2       else null end--fev_quinzena_2_old
           , case when ws_tipo_ocorr in ('A','D') then :old.mar_quinzena_1       else null end--mar_quinzena_1_old
           , case when ws_tipo_ocorr in ('A','D') then :old.mar_quinzena_2       else null end--mar_quinzena_2_old
           , case when ws_tipo_ocorr in ('A','D') then :old.abr_quinzena_1       else null end--abr_quinzena_1_old
           , case when ws_tipo_ocorr in ('A','D') then :old.abr_quinzena_2       else null end--abr_quinzena_2_old
           , case when ws_tipo_ocorr in ('A','D') then :old.mai_quinzena_1       else null end--mai_quinzena_1_old
           , case when ws_tipo_ocorr in ('A','D') then :old.mai_quinzena_2       else null end--mai_quinzena_2_old
           , case when ws_tipo_ocorr in ('A','D') then :old.jun_quinzena_1       else null end--jun_quinzena_1_old
           , case when ws_tipo_ocorr in ('A','D') then :old.jun_quinzena_2       else null end--jun_quinzena_2_old
           , case when ws_tipo_ocorr in ('A','D') then :old.jul_quinzena_1       else null end--jul_quinzena_1_old
           , case when ws_tipo_ocorr in ('A','D') then :old.jul_quinzena_2       else null end--jul_quinzena_2_old
           , case when ws_tipo_ocorr in ('A','D') then :old.ago_quinzena_1       else null end--ago_quinzena_1_old
           , case when ws_tipo_ocorr in ('A','D') then :old.ago_quinzena_2       else null end--ago_quinzena_2_old
           , case when ws_tipo_ocorr in ('A','D') then :old.set_quinzena_1       else null end--set_quinzena_1_old
           , case when ws_tipo_ocorr in ('A','D') then :old.set_quinzena_2       else null end--set_quinzena_2_old
           , case when ws_tipo_ocorr in ('A','D') then :old.out_quinzena_1       else null end--out_quinzena_1_old
           , case when ws_tipo_ocorr in ('A','D') then :old.out_quinzena_2       else null end--out_quinzena_2_old
           , case when ws_tipo_ocorr in ('A','D') then :old.nov_quinzena_1       else null end--nov_quinzena_1_old
           , case when ws_tipo_ocorr in ('A','D') then :old.nov_quinzena_2       else null end--nov_quinzena_2_old
           , case when ws_tipo_ocorr in ('A','D') then :old.dez_quinzena_1       else null end--dez_quinzena_1_old
           , case when ws_tipo_ocorr in ('A','D') then :old.dez_quinzena_2       else null end--dez_quinzena_2_old
           , nvl(:new.usuario_atualizacao, :old.usuario_atualizacao)                          --usuario_atualizacao
           , nvl(:new.data_atualizacao,    :old.data_atualizacao));                           --data_atualizacao

end inter_tr_PROJ_090_log;

/
ALTER TRIGGER  "INTER_TR_PROJ_090_LOG" ENABLE
/
