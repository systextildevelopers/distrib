declare
    id_grid    number;
    id_profile number;
    id_column  number;

    procedure insert_column(p_id number,
                            p_id_grid number,
                            p_key varchar2,
                            p_name varchar2,
                            p_type varchar2,
                            p_formatter_index number,
                            p_scale number) is
    begin
        insert into conf_grid_column (id, id_grid, key, name, type, formatter_index, scale)
        values (p_id, p_id_grid, p_key, p_name, p_type, p_formatter_index, p_scale);
    end;

    procedure insert_column_profile(p_id_grid_perfil number,
                                    p_id_grid_column number,
                                    p_position number) is
    begin
        insert into conf_grid_perfil_column (id_grid_perfil, id_grid_column, position)
        values (p_id_grid_perfil, p_id_grid_column, p_position);
    end;
begin
    id_grid := id_conf_grid.nextval;

    insert into conf_grid (id, nome) values (id_grid, 'NaoConformidadeProdutiva');

    id_profile := id_conf_grid_perfil.nextval;

    insert into conf_grid_perfil (id, id_grid, usuario_gerenciador,
                                  empresa_usuario_gerenciador, nome,
                                  publico, padrao, padrao_usuario)
    values (id_profile, id_grid, 'INTERSYS', 1, 'Padrão', 1, 1, 1);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'id', 'Nr. NC',
                  'NUMBER', 0, 0);
    insert_column_profile(id_profile, id_column, 1);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'data', 'Data Registro',
                  'DATE', 2, 0);
    insert_column_profile(id_profile, id_column, 2);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'usuario', 'Usuário',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 3);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'cod_setor', 'Cód. Setor',
                  'NUMBER', 0, 0);
    insert_column_profile(id_profile, id_column, 4);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'desc_setor', 'Descrição Setor',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 5);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'ordem_producao', 'Ordem Produção',
                  'NUMBER', 0, 0);
    insert_column_profile(id_profile, id_column, 6);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'referencia_peca', 'Referência',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 7);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'descr_referencia', 'Descrição Referência',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 8);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'cod_estagio', 'Cód. Estágio',
                  'NUMBER', 0, 0);
    insert_column_profile(id_profile, id_column, 9);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'desc_estagio', 'Descrição Estágio',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 10);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'qtde_pecas', 'Qtde Peças',
                  'NUMBER', 0, 0);
    insert_column_profile(id_profile, id_column, 11);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'tempo_improd', 'Tempo Improdutivo',
                  'NUMBER', 0, 0);
    insert_column_profile(id_profile, id_column, 12);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'cod_setor_ocorr', 'Cód Setor Ocasionou',
                  'NUMBER', 0, 0);
    insert_column_profile(id_profile, id_column, 13);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'desc_setor_ocorr', 'Desc. Setor Ocasionou',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 14);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'cod_tipo', 'Cód. Tipo',
                  'NUMBER', 0, 0);
    insert_column_profile(id_profile, id_column, 15);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'desc_tipo', 'Descrição Tipo',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 16);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'detalhes', 'Detalhes',
                  'LONG', 0, 0);
    insert_column_profile(id_profile, id_column, 17);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'cod_setor_impacto', 'Cód. Setor Impactou',
                  'NUMBER', 0, 0);
    insert_column_profile(id_profile, id_column, 18);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'desc_setor_impacto', 'Desc. Setor Impactou',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 19);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'fase', 'Fase',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 20);

end;

/
