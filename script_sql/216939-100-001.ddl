alter table hdoc_030 add permissao_adiantamento number(1) default 0;

comment on column hdoc_030.permissao_adiantamento is '0-Tem acesso aos adiantamentos de clientes e fornecedores; 1-Tem acesso aos adiantamentos de fornecedores; 2-Tem acesso aos adiantamentos de clientes';

exec inter_pr_recompile;
