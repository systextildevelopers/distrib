CREATE OR REPLACE FORCE VIEW "VW_NOTAS_FICAIS_3NIVEIS" ("NF_FORNECEDOR", "SERIE_FORNECEDOR", "FORNECEDOR", "NF_TRANSFERENCIA", "SERIE_TRANSFERENCIA", "NF_CONFIRMACAO", "SERIE_CONFIRMACAO", "ID_ARQUIVO") AS 
  select obrf_010.documento             nf_fornecedor,
       obrf_010.serie                 serie_fornecedor,
       lpad(supr_010.fornecedor9, 9, '0')  || '/' || lpad(supr_010.fornecedor4, 4, '0')  ||'-' || lpad(supr_010.fornecedor2, 2, '0')  || ' - ' | | supr_010.NOME_FORNECEDOR AS FORNECEDOR,
       fatu.nf_transferencia,
       fatu.serie_transferencia,
       conf.documento                 nf_confirmacao,
       conf.serie                     serie_confirmacao,
    estq.id_arquivo as ID_ARQUIVO
from obrf_010, obrf_010 conf, supr_010, (select nota_fiscal, serie_fiscal_sai, cnpj_cpf_9, cnpj_cpf_4, cnpj_cpf_2, id_arquivo
                                          from estq_425
                                          group by nota_fiscal, serie_fiscal_sai, cnpj_cpf_9, cnpj_cpf_4, cnpj_cpf_2, id_arquivo)
                                                estq,

              (select
           fatu_050.codigo_empresa      empresa,
           fatu_050.cgc_9,
           fatu_050.cgc_4,
           fatu_050.cgc_2,
           fatu_060.ch_it_nf_num_nfis     nf_transferencia,
           fatu_060.ch_it_nf_ser_nfis     serie_transferencia,
           fatu_060.num_nota_orig,
           fatu_060.serie_nota_orig,
           fatu_060.cgc9_origem,
           fatu_060.cgc4_origem,
           fatu_060.cgc2_origem
      from fatu_050,
         fatu_060
      where fatu_050.codigo_empresa  = fatu_060.ch_it_nf_cd_empr
        and fatu_050.num_nota_fiscal = fatu_060.ch_it_nf_num_nfis
        and fatu_050.serie_nota_fisc = fatu_060.ch_it_nf_ser_nfis
        and fatu_050.cod_status = '100'
        and fatu_060.num_nota_orig is not null
      group by   fatu_050.codigo_empresa,
             fatu_050.cgc_9,
             fatu_050.cgc_4,
             fatu_050.cgc_2,
             fatu_060.ch_it_nf_num_nfis,
             fatu_060.ch_it_nf_ser_nfis,
             fatu_060.num_nota_orig,
             fatu_060.serie_nota_orig,
             fatu_060.cgc9_origem,
             fatu_060.cgc4_origem,
             fatu_060.cgc2_origem) fatu
where obrf_010.documento     = fatu.num_nota_orig (+)
  and obrf_010.serie         = fatu.serie_nota_orig (+)
  and obrf_010.cgc_cli_for_9 = fatu.cgc9_origem (+)
  and obrf_010.cgc_cli_for_4 = fatu.cgc4_origem (+)
  and obrf_010.cgc_cli_for_2 = fatu.cgc2_origem (+)

  and fatu.cgc_9             = conf.cgc_cli_for_9 (+)
  and fatu.nf_transferencia  = conf.documento     (+)
  and fatu.serie_transferencia = conf.serie       (+)

  and obrf_010.cgc_cli_for_9     = estq.cnpj_cpf_9
  and obrf_010.cgc_cli_for_4     = estq.cnpj_cpf_4
  and obrf_010.cgc_cli_for_2     = estq.cnpj_cpf_2

  and obrf_010.documento       = estq.nota_fiscal
  and obrf_010.serie           = estq.serie_fiscal_sai

  and obrf_010.cgc_cli_for_9 = supr_010.fornecedor9
  and obrf_010.cgc_cli_for_4 = supr_010.fornecedor4
  and obrf_010.cgc_cli_for_2 = supr_010.fornecedor2
