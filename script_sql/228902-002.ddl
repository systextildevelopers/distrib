-- Add/modify columns 
alter table ESTQ_300 add cod_estagio_agrupador number(2);

-- Add comments to the columns 
comment on column ESTQ_300.cod_estagio_agrupador
  is 'estq_300.COD_ESTAGIO_AGRUPADOR faz referencia ao campo pcpb_015.CODIGO_ESTAGIO';

-- Add/modify columns 
alter table ESTQ_300 add seq_operacao_agrupador number(4);

-- Add comments to the columns 
comment on column ESTQ_300.seq_operacao_agrupador
  is 'estq_300.SEQ_OPERACAO_AGRUPADOR faz referÍncia ao campo pcpb_015.SEQ_OPERACAO_AGRUPADOR';
