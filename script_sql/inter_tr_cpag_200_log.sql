create or replace trigger inter_tr_CPAG_200_log 
after insert or delete or update 
on CPAG_200 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    begin 
       select hdoc_090.programa 
       into v_nome_programa 
       from hdoc_090 
       where hdoc_090.sid = ws_sid 
         and rownum       = 1 
         and hdoc_090.programa not like '%menu%' 
         and hdoc_090.programa not like '%_m%'; 
       exception 
         when no_data_found then            v_nome_programa := 'SQL'; 
    end; 
 
 
 
 if inserting 
 then 
    begin 
 
        insert into CPAG_200_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           tipo_adiantam_OLD,   /*8*/ 
           tipo_adiantam_NEW,   /*9*/ 
           numero_adiantam_OLD,   /*10*/ 
           numero_adiantam_NEW,   /*11*/ 
           cgc_9_OLD,   /*12*/ 
           cgc_9_NEW,   /*13*/ 
           cgc_4_OLD,   /*14*/ 
           cgc_4_NEW,   /*15*/ 
           cgc_2_OLD,   /*16*/ 
           cgc_2_NEW,   /*17*/ 
           data_digitacao_OLD,   /*18*/ 
           data_digitacao_NEW,   /*19*/ 
           valor_adiant_OLD,   /*20*/ 
           valor_adiant_NEW,   /*21*/ 
           valor_saldo_OLD,   /*22*/ 
           valor_saldo_NEW,   /*23*/ 
           situacao_OLD,   /*24*/ 
           situacao_NEW,   /*25*/ 
           data_remessa_OLD,   /*26*/ 
           data_remessa_NEW,   /*27*/ 
           cod_cancelamento_OLD,   /*28*/ 
           cod_cancelamento_NEW,   /*29*/ 
           data_cancelamento_OLD,   /*30*/ 
           data_cancelamento_NEW,   /*31*/ 
           codigo_empresa_OLD,   /*32*/ 
           codigo_empresa_NEW,   /*33*/ 
           data_prev_pgto_OLD,   /*34*/ 
           data_prev_pgto_NEW,   /*35*/ 
           origem_adiantam_OLD,   /*36*/ 
           origem_adiantam_NEW,   /*37*/ 
           nr_adiantam_geral_OLD,   /*38*/ 
           nr_adiantam_geral_NEW,   /*39*/ 
           pedido_compra_OLD,   /*40*/ 
           pedido_compra_NEW,   /*41*/ 
           percentual_OLD,   /*42*/ 
           percentual_NEW,   /*43*/ 
           cod_moeda_OLD,   /*44*/ 
           cod_moeda_NEW,   /*45*/ 
           cod_portador_OLD,   /*46*/ 
           cod_portador_NEW,   /*47*/ 
           data_moeda_OLD,   /*48*/ 
           data_moeda_NEW,   /*49*/ 
           num_importacao_OLD,   /*50*/ 
           num_importacao_NEW,   /*51*/ 
           valor_liquidacao_OLD,   /*52*/ 
           valor_liquidacao_NEW,   /*53*/ 
           data_liquidacao_OLD,   /*54*/ 
           data_liquidacao_NEW,   /*55*/ 
           banco_liquidacao_OLD,   /*56*/ 
           banco_liquidacao_NEW,   /*57*/ 
           conta_liquidacao_OLD,   /*58*/ 
           conta_liquidacao_NEW,   /*59*/ 
           cotacao_moeda_OLD,   /*60*/ 
           cotacao_moeda_NEW    /*61*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.tipo_adiantam, /*9*/   
           0,/*10*/
           :new.numero_adiantam, /*11*/   
           0,/*12*/
           :new.cgc_9, /*13*/   
           0,/*14*/
           :new.cgc_4, /*15*/   
           0,/*16*/
           :new.cgc_2, /*17*/   
           null,/*18*/
           :new.data_digitacao, /*19*/   
           0,/*20*/
           :new.valor_adiant, /*21*/   
           0,/*22*/
           :new.valor_saldo, /*23*/   
           0,/*24*/
           :new.situacao, /*25*/   
           null,/*26*/
           :new.data_remessa, /*27*/   
           0,/*28*/
           :new.cod_cancelamento, /*29*/   
           null,/*30*/
           :new.data_cancelamento, /*31*/   
           0,/*32*/
           :new.codigo_empresa, /*33*/   
           null,/*34*/
           :new.data_prev_pgto, /*35*/   
           '',/*36*/
           :new.origem_adiantam, /*37*/   
           0,/*38*/
           :new.nr_adiantam_geral, /*39*/   
           0,/*40*/
           :new.pedido_compra, /*41*/   
           0,/*42*/
           :new.percentual, /*43*/   
           0,/*44*/
           :new.cod_moeda, /*45*/   
           0,/*46*/
           :new.cod_portador, /*47*/   
           null,/*48*/
           :new.data_moeda, /*49*/   
           '',/*50*/
           :new.num_importacao, /*51*/   
           0,/*52*/
           :new.valor_liquidacao, /*53*/   
           null,/*54*/
           :new.data_liquidacao, /*55*/   
           0,/*56*/
           :new.banco_liquidacao, /*57*/   
           0,/*58*/
           :new.conta_liquidacao, /*59*/   
           0,/*60*/
           :new.cotacao_moeda /*61*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into CPAG_200_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           tipo_adiantam_OLD, /*8*/   
           tipo_adiantam_NEW, /*9*/   
           numero_adiantam_OLD, /*10*/   
           numero_adiantam_NEW, /*11*/   
           cgc_9_OLD, /*12*/   
           cgc_9_NEW, /*13*/   
           cgc_4_OLD, /*14*/   
           cgc_4_NEW, /*15*/   
           cgc_2_OLD, /*16*/   
           cgc_2_NEW, /*17*/   
           data_digitacao_OLD, /*18*/   
           data_digitacao_NEW, /*19*/   
           valor_adiant_OLD, /*20*/   
           valor_adiant_NEW, /*21*/   
           valor_saldo_OLD, /*22*/   
           valor_saldo_NEW, /*23*/   
           situacao_OLD, /*24*/   
           situacao_NEW, /*25*/   
           data_remessa_OLD, /*26*/   
           data_remessa_NEW, /*27*/   
           cod_cancelamento_OLD, /*28*/   
           cod_cancelamento_NEW, /*29*/   
           data_cancelamento_OLD, /*30*/   
           data_cancelamento_NEW, /*31*/   
           codigo_empresa_OLD, /*32*/   
           codigo_empresa_NEW, /*33*/   
           data_prev_pgto_OLD, /*34*/   
           data_prev_pgto_NEW, /*35*/   
           origem_adiantam_OLD, /*36*/   
           origem_adiantam_NEW, /*37*/   
           nr_adiantam_geral_OLD, /*38*/   
           nr_adiantam_geral_NEW, /*39*/   
           pedido_compra_OLD, /*40*/   
           pedido_compra_NEW, /*41*/   
           percentual_OLD, /*42*/   
           percentual_NEW, /*43*/   
           cod_moeda_OLD, /*44*/   
           cod_moeda_NEW, /*45*/   
           cod_portador_OLD, /*46*/   
           cod_portador_NEW, /*47*/   
           data_moeda_OLD, /*48*/   
           data_moeda_NEW, /*49*/   
           num_importacao_OLD, /*50*/   
           num_importacao_NEW, /*51*/   
           valor_liquidacao_OLD, /*52*/   
           valor_liquidacao_NEW, /*53*/   
           data_liquidacao_OLD, /*54*/   
           data_liquidacao_NEW, /*55*/   
           banco_liquidacao_OLD, /*56*/   
           banco_liquidacao_NEW, /*57*/   
           conta_liquidacao_OLD, /*58*/   
           conta_liquidacao_NEW, /*59*/   
           cotacao_moeda_OLD, /*60*/   
           cotacao_moeda_NEW  /*61*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.tipo_adiantam,  /*8*/  
           :new.tipo_adiantam, /*9*/   
           :old.numero_adiantam,  /*10*/  
           :new.numero_adiantam, /*11*/   
           :old.cgc_9,  /*12*/  
           :new.cgc_9, /*13*/   
           :old.cgc_4,  /*14*/  
           :new.cgc_4, /*15*/   
           :old.cgc_2,  /*16*/  
           :new.cgc_2, /*17*/   
           :old.data_digitacao,  /*18*/  
           :new.data_digitacao, /*19*/   
           :old.valor_adiant,  /*20*/  
           :new.valor_adiant, /*21*/   
           :old.valor_saldo,  /*22*/  
           :new.valor_saldo, /*23*/   
           :old.situacao,  /*24*/  
           :new.situacao, /*25*/   
           :old.data_remessa,  /*26*/  
           :new.data_remessa, /*27*/   
           :old.cod_cancelamento,  /*28*/  
           :new.cod_cancelamento, /*29*/   
           :old.data_cancelamento,  /*30*/  
           :new.data_cancelamento, /*31*/   
           :old.codigo_empresa,  /*32*/  
           :new.codigo_empresa, /*33*/   
           :old.data_prev_pgto,  /*34*/  
           :new.data_prev_pgto, /*35*/   
           :old.origem_adiantam,  /*36*/  
           :new.origem_adiantam, /*37*/   
           :old.nr_adiantam_geral,  /*38*/  
           :new.nr_adiantam_geral, /*39*/   
           :old.pedido_compra,  /*40*/  
           :new.pedido_compra, /*41*/   
           :old.percentual,  /*42*/  
           :new.percentual, /*43*/   
           :old.cod_moeda,  /*44*/  
           :new.cod_moeda, /*45*/   
           :old.cod_portador,  /*46*/  
           :new.cod_portador, /*47*/   
           :old.data_moeda,  /*48*/  
           :new.data_moeda, /*49*/   
           :old.num_importacao,  /*50*/  
           :new.num_importacao, /*51*/   
           :old.valor_liquidacao,  /*52*/  
           :new.valor_liquidacao, /*53*/   
           :old.data_liquidacao,  /*54*/  
           :new.data_liquidacao, /*55*/   
           :old.banco_liquidacao,  /*56*/  
           :new.banco_liquidacao, /*57*/   
           :old.conta_liquidacao,  /*58*/  
           :new.conta_liquidacao, /*59*/   
           :old.cotacao_moeda,  /*60*/  
           :new.cotacao_moeda  /*61*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into CPAG_200_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           tipo_adiantam_OLD, /*8*/   
           tipo_adiantam_NEW, /*9*/   
           numero_adiantam_OLD, /*10*/   
           numero_adiantam_NEW, /*11*/   
           cgc_9_OLD, /*12*/   
           cgc_9_NEW, /*13*/   
           cgc_4_OLD, /*14*/   
           cgc_4_NEW, /*15*/   
           cgc_2_OLD, /*16*/   
           cgc_2_NEW, /*17*/   
           data_digitacao_OLD, /*18*/   
           data_digitacao_NEW, /*19*/   
           valor_adiant_OLD, /*20*/   
           valor_adiant_NEW, /*21*/   
           valor_saldo_OLD, /*22*/   
           valor_saldo_NEW, /*23*/   
           situacao_OLD, /*24*/   
           situacao_NEW, /*25*/   
           data_remessa_OLD, /*26*/   
           data_remessa_NEW, /*27*/   
           cod_cancelamento_OLD, /*28*/   
           cod_cancelamento_NEW, /*29*/   
           data_cancelamento_OLD, /*30*/   
           data_cancelamento_NEW, /*31*/   
           codigo_empresa_OLD, /*32*/   
           codigo_empresa_NEW, /*33*/   
           data_prev_pgto_OLD, /*34*/   
           data_prev_pgto_NEW, /*35*/   
           origem_adiantam_OLD, /*36*/   
           origem_adiantam_NEW, /*37*/   
           nr_adiantam_geral_OLD, /*38*/   
           nr_adiantam_geral_NEW, /*39*/   
           pedido_compra_OLD, /*40*/   
           pedido_compra_NEW, /*41*/   
           percentual_OLD, /*42*/   
           percentual_NEW, /*43*/   
           cod_moeda_OLD, /*44*/   
           cod_moeda_NEW, /*45*/   
           cod_portador_OLD, /*46*/   
           cod_portador_NEW, /*47*/   
           data_moeda_OLD, /*48*/   
           data_moeda_NEW, /*49*/   
           num_importacao_OLD, /*50*/   
           num_importacao_NEW, /*51*/   
           valor_liquidacao_OLD, /*52*/   
           valor_liquidacao_NEW, /*53*/   
           data_liquidacao_OLD, /*54*/   
           data_liquidacao_NEW, /*55*/   
           banco_liquidacao_OLD, /*56*/   
           banco_liquidacao_NEW, /*57*/   
           conta_liquidacao_OLD, /*58*/   
           conta_liquidacao_NEW, /*59*/   
           cotacao_moeda_OLD, /*60*/   
           cotacao_moeda_NEW /*61*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.tipo_adiantam, /*8*/   
           0, /*9*/
           :old.numero_adiantam, /*10*/   
           0, /*11*/
           :old.cgc_9, /*12*/   
           0, /*13*/
           :old.cgc_4, /*14*/   
           0, /*15*/
           :old.cgc_2, /*16*/   
           0, /*17*/
           :old.data_digitacao, /*18*/   
           null, /*19*/
           :old.valor_adiant, /*20*/   
           0, /*21*/
           :old.valor_saldo, /*22*/   
           0, /*23*/
           :old.situacao, /*24*/   
           0, /*25*/
           :old.data_remessa, /*26*/   
           null, /*27*/
           :old.cod_cancelamento, /*28*/   
           0, /*29*/
           :old.data_cancelamento, /*30*/   
           null, /*31*/
           :old.codigo_empresa, /*32*/   
           0, /*33*/
           :old.data_prev_pgto, /*34*/   
           null, /*35*/
           :old.origem_adiantam, /*36*/   
           '', /*37*/
           :old.nr_adiantam_geral, /*38*/   
           0, /*39*/
           :old.pedido_compra, /*40*/   
           0, /*41*/
           :old.percentual, /*42*/   
           0, /*43*/
           :old.cod_moeda, /*44*/   
           0, /*45*/
           :old.cod_portador, /*46*/   
           0, /*47*/
           :old.data_moeda, /*48*/   
           null, /*49*/
           :old.num_importacao, /*50*/   
           '', /*51*/
           :old.valor_liquidacao, /*52*/   
           0, /*53*/
           :old.data_liquidacao, /*54*/   
           null, /*55*/
           :old.banco_liquidacao, /*56*/   
           0, /*57*/
           :old.conta_liquidacao, /*58*/   
           0, /*59*/
           :old.cotacao_moeda, /*60*/   
           0 /*61*/
         );    
    end;    
 end if;    
end inter_tr_CPAG_200_log;
