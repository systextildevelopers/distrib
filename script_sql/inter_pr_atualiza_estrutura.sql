
  CREATE OR REPLACE PROCEDURE "INTER_PR_ATUALIZA_ESTRUTURA" (p_ordem_planejamento  in number,
                                                         p_pedido_venda        in number,
                                                         p_pedido_reserva      in number,
                                                         p_nivel_origem        in varchar2,
                                                         p_grupo_origem        in varchar2,
                                                         p_subgrupo_origem     in varchar2,
                                                         p_item_origem         in varchar2,
                                                         p_alternativa_origem  in number,
                                                         p_qtde_atualizar      in number)
is
   cursor tmrp625 is
      select tmrp_625.nivel_produto,          tmrp_625.grupo_produto,
             tmrp_625.subgrupo_produto,       tmrp_625.item_produto,
             tmrp_625.alternativa_produto,    tmrp_625.consumo
      from tmrp_625
      where tmrp_625.ordem_planejamento         = p_ordem_planejamento
        and tmrp_625.pedido_venda               = p_pedido_venda
        and tmrp_625.pedido_reserva             = p_pedido_reserva
        and tmrp_625.nivel_produto_origem       = p_nivel_origem
        and tmrp_625.grupo_produto_origem       = p_grupo_origem
        and tmrp_625.subgrupo_produto_origem    = p_subgrupo_origem
        and tmrp_625.item_produto_origem        = p_item_origem
        and tmrp_625.alternativa_produto_origem = p_alternativa_origem;
begin
      for reg_625 in tmrp625
      loop
         begin
            update tmrp_625
               set tmrp_625.qtde_reserva_planejada     = (tmrp_625.qtde_reserva_planejada - (p_qtde_atualizar * reg_625.consumo))
             where tmrp_625.ordem_planejamento         = p_ordem_planejamento
               and tmrp_625.pedido_venda               = p_pedido_venda
               and tmrp_625.pedido_reserva             = p_pedido_reserva
               and tmrp_625.nivel_produto_origem       = p_nivel_origem
               and tmrp_625.grupo_produto_origem       = p_grupo_origem
               and tmrp_625.subgrupo_produto_origem    = p_subgrupo_origem
               and tmrp_625.item_produto_origem        = p_item_origem
               and tmrp_625.alternativa_produto_origem = p_alternativa_origem
               and tmrp_625.nivel_produto              = reg_625.nivel_produto
               and tmrp_625.grupo_produto              = reg_625.grupo_produto
               and tmrp_625.subgrupo_produto           = reg_625.subgrupo_produto
               and tmrp_625.item_produto               = reg_625.item_produto
               and tmrp_625.alternativa_produto        = reg_625.alternativa_produto;

            inter_pr_atualiza_estrutura (p_ordem_planejamento,
                                         p_pedido_venda,
                                         p_pedido_reserva,
                                         reg_625.nivel_produto,
                                         reg_625.grupo_produto,
                                         reg_625.subgrupo_produto,
                                         reg_625.item_produto,
                                         reg_625.alternativa_produto,
                                         p_qtde_atualizar);
         end;
      end loop;
end inter_pr_atualiza_estrutura;

 

/

exec inter_pr_recompile;

