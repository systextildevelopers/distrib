CREATE OR REPLACE TRIGGER INTER_TR_OBRF_150_LOG
before insert on obrf_150_log
  for each row

declare
   v_nome_programa   obrf_150_log.nome_programa%type;
   v_sid             number;
   v_osuser          varchar2(20);
   v_nome_usuario    obrf_150_log.usuario_sistema%type;
   v_id_registro     number;
   ws_empresa        number;
   ws_maquina_rede   varchar2(60);
   ws_locale_usuario varchar2(60);
   ws_aplicativo     varchar2(60);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (v_osuser,         ws_maquina_rede,   ws_aplicativo,     v_sid,
                           v_nome_usuario,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(v_sid);

   :new.nome_programa    := v_nome_programa;
   :new.usuario_sistema  := v_nome_usuario;

   begin
      select NVL(seq_obrf_150_log.nextval,0)
      into v_id_registro
      from dual;
   end;

   :new.id_registro := v_id_registro;

end inter_tr_obrf_150_log;

-- ALTER TRIGGER INTER_TR_OBRF_150_LOG ENABLE

/
