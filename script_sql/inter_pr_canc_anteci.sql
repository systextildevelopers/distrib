create or replace PROCEDURE "INTER_PR_CANC_ANTECI"
is
    v_cod_canc number;
    tmpInt number;
    v_seq_dup_origem number;
    v_situacao_sph       varchar2(20);

begin


   FOR reg_pedido in (

        select  PEDIDO_VENDA,     CGC9,
                CGC4,             CGC2,
                CODIGO_EMPRESA,   COD_REP_CLIENTE,
                QTDE_PEDIDA,      NR_TITULO,
                NUMERO_REFERENCIA,SITUACAO_ANTEC
        from (
            select distinct
                    pedi_100.PEDIDO_VENDA
                    ,pedi_100.CODIGO_EMPRESA
                    ,pedi_100.CLI_PED_CGC_CLI9 AS CGC9
                    ,pedi_100.CLI_PED_CGC_CLI4 AS CGC4
                    ,pedi_100.CLI_PED_CGC_CLI2 AS CGC2
                    ,pedi_100.COD_REP_CLIENTE
                    ,pedi_121.NR_TITULO
                    ,case 
                        when pedi_100.situacao_venda = 9 then pedi_100.QTDE_SALDO_PEDI
                        else pedi_100.QTDE_TOTAL_PEDI end AS QTDE_PEDIDA
                    ,case
                        when pedi_117.sit_boleto != 2 and (select count(1) as data_venc from FATU_070 fatu, FATU_504
                                                                            where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA
                                                                            and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9
                                                                            and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4
                                                                            and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2
                                                                            and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO
                                                                            and fatu.PEDIDO_VENDA = pedi_100.pedido_venda
                                                                            and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO
                                                                            and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA
                                                                            and fatu.DATA_PRORROGACAO >= to_date(sysdate)
                                                                            and fatu.SALDO_DUPLICATA > 0 ) >= 1 then 'AGUARDANDO PAGAMENTO'
                        when pedi_117.sit_boleto != 2 and (select count(1) as data_venc from FATU_070 fatu, FATU_504
                                                                            where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA
                                                                            and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9
                                                                            and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4
                                                                            and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2
                                                                            and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO
                                                                            and fatu.PEDIDO_VENDA = pedi_100.pedido_venda
                                                                            and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO
                                                                            and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA
                                                                            and fatu.DATA_PRORROGACAO < to_date(sysdate)
                                                                            and fatu.duplic_emitida <> 3
                                                                            and fatu.SALDO_DUPLICATA > 0) >= 1 then 'ANTECIPA��O EM ATRASO'
                        when pedi_117.sit_boleto != 2 and (select count(1) as data_venc from FATU_070 fatu, FATU_504
                                                                            where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA
                                                                            and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9
                                                                            and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4
                                                                            and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2
                                                                            and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO
                                                                            and fatu.PEDIDO_VENDA = pedi_100.pedido_venda
                                                                            and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO
                                                                            and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA
                                                                            and fatu.SALDO_DUPLICATA = 0) >= 1 then 'ANTECIPA��O RECEBIDA'
                        when pedi_117.sit_boleto = 2 then 'ANTECIPA��O CANCELADA'
                        when pedi_117.sit_boleto in (0,1) and pedi_100.pedido_venda = pedi_118.pedido_venda   then 'PROCESSO DE ANTECIPA��O REALIZADO'
                        --when ((select nvl(sum(QTDE_SUGERIDA),0) from pedi_110 where pedido_venda = pedi_100.pedido_venda) <= 0)  and not exists (select fatu.pedido_venda from fatu_070 fatu where fatu.pedido_venda = pedi_100.pedido_venda) then 'PEDIDO DIGITADO' --and pedi_100.pedido_venda != pedi_118.pedido_venda
                        when ((select nvl(sum(QTDE_SUGERIDA),0) from pedi_110 where pedido_venda = pedi_100.pedido_venda) > 0) and pedi_118.pedido_venda is null then 'PEDIDO SUGERIDO'
                        else 'PEDIDO DIGITADO'
                    end as situacao_antec
                    ,pedi_010.cod_cidade
                    ,pedi_100.numero_referencia --Inserido p/ cancelar Transa��o de Pagamento SPH
                from  pedi_100
                    ,pedi_110
                    ,pedi_070 --condi��o de pagamento
                    ,pedi_010 --cliente
                    ,pedi_075
                    ,pedi_118
                    ,pedi_117
                    ,pedi_121
                    ,loja_010 --adicionado SPH
                    where
                        pedi_100.COND_PGTO_VENDA =  pedi_070.COND_PGT_CLIENTE
                        and pedi_100.cod_forma_pagto = loja_010.forma_pgto(+) -- Adicionado SPH
                        and pedi_100.pedido_venda = pedi_110.pedido_venda
                        and pedi_100.CLI_PED_CGC_CLI9 = pedi_010.CGC_9 (+)
                        and pedi_100.CLI_PED_CGC_CLI4 = pedi_010.CGC_4 (+)
                        and pedi_100.CLI_PED_CGC_CLI2 = pedi_010.CGC_2 (+)
                        and pedi_100.cond_pgto_venda = pedi_075.condicao_pagto
                        and pedi_100.CLI_PED_CGC_CLI9 = pedi_118.CGC9 (+)
                        and pedi_100.CLI_PED_CGC_CLI4 = pedi_118.CGC4 (+)
                        and pedi_100.CLI_PED_CGC_CLI2 = pedi_118.CGC2 (+)
                        and pedi_100.pedido_venda = pedi_118.pedido_venda  (+)
                        and pedi_118.CGC9 = pedi_117.CGC9 (+)
                        and pedi_118.CGC4 = pedi_117.CGC4 (+)
                        and pedi_118.CGC2 = pedi_117.CGC2 (+)
                        and pedi_118.SEQUENCIA = pedi_117.SEQUENCIA (+)
                        and pedi_118.CGC9 = pedi_121.CGC9 (+)
                        and pedi_118.CGC4 = pedi_121.CGC4 (+)
                        and pedi_118.CGC2 = pedi_121.CGC2 (+)
                        and pedi_118.SEQUENCIA = pedi_121.seq_deposito (+)
                        and nvl(pedi_117.sit_boleto,0) != 5
                        and (pedi_075.vencimento = 0 or (loja_010.codigo_servico_sph > 0)) -- condiserar venda prazo quando for SPH
                        and pedi_100.cod_cancelamento = 0
                        and pedi_110.situacao_fatu_it <> 1
                        and pedi_110.cod_cancelamento  = 0
                        and pedi_100.SITUACAO_VENDA != 10
                        and (pedi_110.QTDE_SUGERIDA > 0
                        or (((select count(1) from pedi_118 pedi118
                                where pedi118.pedido_venda = pedi_100.pedido_venda) > 0)
                                and pedi_100.situacao_venda <> 9))
            )
                    WHERE SITUACAO_ANTEC = 'ANTECIPA��O EM ATRASO'
                    group by
                    PEDIDO_VENDA,   CGC9,
                    CGC4,           CGC2,
                    CODIGO_EMPRESA, COD_REP_CLIENTE,
                    QTDE_PEDIDA,    NR_TITULO,
                    NUMERO_REFERENCIA, SITUACAO_ANTEC 
                    order by pedido_venda
    )
    LOOP
    
        --incluido a valida��o da situa��o do SPH para evitar de cancelar pedido que possui situa��o liquidada
        begin
            select SITUACAO_SPH
            into   v_situacao_sph
            from   FINA_204
            where  numero_referencia = reg_pedido.numero_referencia;
        exception when no_data_found then
            insert into fina_211(ID,DATA_MOVIMENTO,NOME_PROCESSO,TABELA,TIPO_PROCESSO,USUARIO,EMPRESA,DADOS) 
            values(FINA_211_SEQ.nextval, SYSDATE,'CANCELAMENTO PEDIDO', 'inter_pr_canc_anteci', 'AVISO',null, null,'O numero_referencia n�o est� cadastrado!');
            v_situacao_sph := 'PADRAO';
        end;

        if v_situacao_sph not in ('CANCELADO', 'LIQUIDADO', 'ESTORNADO') then --Inclus�o da condi��o para n�o cancelar titulos que est�o liquidados, cancelados ou estornados no SPH

            begin
                inter_pr_canc_pedido(2,reg_pedido.PEDIDO_VENDA);
            exception when others then
                continue;
            end;

            commit;
        end if;
    end loop;
end inter_pr_canc_anteci;
/
