CREATE TABLE SUPR_027
(
  cep   			NUMBER(9) default 0 not null ,
  rota         		number(9),
  filial_sigla      varchar2(4),
  id_filial         number(4)
);

comment on table SUPR_027 
  is 'Siglas e rotas das transportadoras por CEP.';
-- Add comments to the columns 
comment on column SUPR_027.cep  is 'cep.';

-- Create/Recreate primary, unique and foreign key constraints 
alter table SUPR_027 
  add constraint PK_SUPR_027 primary key (cep);
