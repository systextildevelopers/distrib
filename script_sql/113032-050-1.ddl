alter table i_supr_010 add CODIGO_CONTABIL number(6);

alter table i_fatu_050 add ( valor_pgto_dinheiro number(15,2));
alter table fatu_050 add ( valor_pgto_dinheiro number(15,2));

alter table i_fatu_070 add (codigo_fatura_loja number(9));
alter table fatu_070 add (codigo_fatura_loja number(9));

exec inter_pr_recompile;
