insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('crec_f968', 'Seleção de Cliente para Depósito Identificado', 1,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'crec_f968', 'crec_m200', 1, 1, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'crec_f968', 'crec_m200', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Seleção de Cliente para Depósito Identificado'
 where hdoc_036.codigo_programa = 'crec_f968'
   and hdoc_036.locale          = 'es_ES';
commit;

/
exec inter_pr_recompile;
