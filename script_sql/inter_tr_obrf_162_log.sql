
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_162_LOG" 
after insert or delete or update
on OBRF_162
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   
   v_nome_programa := inter_fn_nome_programa(ws_sid); 

 if inserting
 then
    begin

        insert into OBRF_162_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           COD_EMPRESA_OLD,   /*8*/
           COD_EMPRESA_NEW,   /*9*/
           NUM_NOTA_FISCAL_OLD,   /*10*/
           NUM_NOTA_FISCAL_NEW,   /*11*/
           SERIE_NOTA_FISCAL_OLD,   /*12*/
           SERIE_NOTA_FISCAL_NEW,   /*13*/
           CNPJ9_OLD,   /*14*/
           CNPJ9_NEW,   /*15*/
           CNPJ4_OLD,   /*16*/
           CNPJ4_NEW,   /*17*/
           CNPJ2_OLD,   /*18*/
           CNPJ2_NEW,   /*19*/
           NUMERO_DANFE_NFE_OLD,   /*20*/
           NUMERO_DANFE_NFE_NEW,   /*21*/
           DATA_EMISSAO_OLD,   /*22*/
           DATA_EMISSAO_NEW,   /*23*/
           TOTAL_NOTA_OLD,   /*24*/
           TOTAL_NOTA_NEW,   /*25*/
           TOTAL_IPI_OLD,   /*26*/
           TOTAL_IPI_NEW,   /*27*/
           TOTAL_FRETE_OLD,   /*28*/
           TOTAL_FRETE_NEW,   /*29*/
           TOTAL_SEGURO_OLD,   /*30*/
           TOTAL_SEGURO_NEW,   /*31*/
           TOTAL_DESPESAS_OLD,   /*32*/
           TOTAL_DESPESAS_NEW,   /*33*/
           TOTAL_DESCONTO_OLD,   /*34*/
           TOTAL_DESCONTO_NEW,   /*35*/
           TOTAL_MERCADORIA_OLD,   /*36*/
           TOTAL_MERCADORIA_NEW,   /*37*/
           TOTAL_BASE_ICMS_OLD,   /*38*/
           TOTAL_BASE_ICMS_NEW,   /*39*/
           TOTAL_ICMS_OLD,   /*40*/
           TOTAL_ICMS_NEW,   /*41*/
           TOTAL_ICMS_ST_OLD,   /*42*/
           TOTAL_ICMS_ST_NEW,   /*43*/
           TOTAL_BASE_ICMS_ST_OLD,   /*44*/
           TOTAL_BASE_ICMS_ST_NEW,   /*45*/
           PESO_LIQUIDO_OLD,   /*46*/
           PESO_LIQUIDO_NEW,   /*47*/
           PESO_BRUTO_OLD,   /*48*/
           PESO_BRUTO_NEW,   /*49*/
           TIPO_FRETE_OLD,   /*50*/
           TIPO_FRETE_NEW,   /*51*/
           CTE_OLD,   /*52*/
           CTE_NEW   /*53*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.COD_EMPRESA, /*9*/
           0,/*10*/
           :new.NUM_NOTA_FISCAL, /*11*/
           '',/*12*/
           :new.SERIE_NOTA_FISCAL, /*13*/
           0,/*14*/
           :new.CNPJ9, /*15*/
           0,/*16*/
           :new.CNPJ4, /*17*/
           0,/*18*/
           :new.CNPJ2, /*19*/
           '',/*20*/
           :new.NUMERO_DANFE_NFE, /*21*/
           null,/*22*/
           :new.DATA_EMISSAO, /*23*/
           0,/*24*/
           :new.TOTAL_NOTA, /*25*/
           0,/*26*/
           :new.TOTAL_IPI, /*27*/
           0,/*28*/
           :new.TOTAL_FRETE, /*29*/
           0,/*30*/
           :new.TOTAL_SEGURO, /*31*/
           0,/*32*/
           :new.TOTAL_DESPESAS, /*33*/
           0,/*34*/
           :new.TOTAL_DESCONTO, /*35*/
           0,/*36*/
           :new.TOTAL_MERCADORIA, /*37*/
           0,/*38*/
           :new.TOTAL_BASE_ICMS, /*39*/
           0,/*40*/
           :new.TOTAL_ICMS, /*41*/
           0,/*42*/
           :new.TOTAL_ICMS_ST, /*43*/
           0,/*44*/
           :new.TOTAL_BASE_ICMS_ST, /*45*/
           0,/*46*/
           :new.PESO_LIQUIDO, /*47*/
           0,/*48*/
           :new.PESO_BRUTO, /*49*/
           0,/*50*/
           :new.TIPO_FRETE, /*51*/
           '',/*52*/
           :new.CTE /*53*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into OBRF_162_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_EMPRESA_OLD, /*8*/
           COD_EMPRESA_NEW, /*9*/
           NUM_NOTA_FISCAL_OLD, /*10*/
           NUM_NOTA_FISCAL_NEW, /*11*/
           SERIE_NOTA_FISCAL_OLD, /*12*/
           SERIE_NOTA_FISCAL_NEW, /*13*/
           CNPJ9_OLD, /*14*/
           CNPJ9_NEW, /*15*/
           CNPJ4_OLD, /*16*/
           CNPJ4_NEW, /*17*/
           CNPJ2_OLD, /*18*/
           CNPJ2_NEW, /*19*/
           NUMERO_DANFE_NFE_OLD, /*20*/
           NUMERO_DANFE_NFE_NEW, /*21*/
           DATA_EMISSAO_OLD, /*22*/
           DATA_EMISSAO_NEW, /*23*/
           TOTAL_NOTA_OLD, /*24*/
           TOTAL_NOTA_NEW, /*25*/
           TOTAL_IPI_OLD, /*26*/
           TOTAL_IPI_NEW, /*27*/
           TOTAL_FRETE_OLD, /*28*/
           TOTAL_FRETE_NEW, /*29*/
           TOTAL_SEGURO_OLD, /*30*/
           TOTAL_SEGURO_NEW, /*31*/
           TOTAL_DESPESAS_OLD, /*32*/
           TOTAL_DESPESAS_NEW, /*33*/
           TOTAL_DESCONTO_OLD, /*34*/
           TOTAL_DESCONTO_NEW, /*35*/
           TOTAL_MERCADORIA_OLD, /*36*/
           TOTAL_MERCADORIA_NEW, /*37*/
           TOTAL_BASE_ICMS_OLD, /*38*/
           TOTAL_BASE_ICMS_NEW, /*39*/
           TOTAL_ICMS_OLD, /*40*/
           TOTAL_ICMS_NEW, /*41*/
           TOTAL_ICMS_ST_OLD, /*42*/
           TOTAL_ICMS_ST_NEW, /*43*/
           TOTAL_BASE_ICMS_ST_OLD, /*44*/
           TOTAL_BASE_ICMS_ST_NEW, /*45*/
           PESO_LIQUIDO_OLD, /*46*/
           PESO_LIQUIDO_NEW, /*47*/
           PESO_BRUTO_OLD, /*48*/
           PESO_BRUTO_NEW, /*49*/
           TIPO_FRETE_OLD, /*50*/
           TIPO_FRETE_NEW, /*51*/
           CTE_OLD, /*52*/
           CTE_NEW /*53*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.COD_EMPRESA,  /*8*/
           :new.COD_EMPRESA, /*9*/
           :old.NUM_NOTA_FISCAL,  /*10*/
           :new.NUM_NOTA_FISCAL, /*11*/
           :old.SERIE_NOTA_FISCAL,  /*12*/
           :new.SERIE_NOTA_FISCAL, /*13*/
           :old.CNPJ9,  /*14*/
           :new.CNPJ9, /*15*/
           :old.CNPJ4,  /*16*/
           :new.CNPJ4, /*17*/
           :old.CNPJ2,  /*18*/
           :new.CNPJ2, /*19*/
           :old.NUMERO_DANFE_NFE,  /*20*/
           :new.NUMERO_DANFE_NFE, /*21*/
           :old.DATA_EMISSAO,  /*22*/
           :new.DATA_EMISSAO, /*23*/
           :old.TOTAL_NOTA,  /*24*/
           :new.TOTAL_NOTA, /*25*/
           :old.TOTAL_IPI,  /*26*/
           :new.TOTAL_IPI, /*27*/
           :old.TOTAL_FRETE,  /*28*/
           :new.TOTAL_FRETE, /*29*/
           :old.TOTAL_SEGURO,  /*30*/
           :new.TOTAL_SEGURO, /*31*/
           :old.TOTAL_DESPESAS,  /*32*/
           :new.TOTAL_DESPESAS, /*33*/
           :old.TOTAL_DESCONTO,  /*34*/
           :new.TOTAL_DESCONTO, /*35*/
           :old.TOTAL_MERCADORIA,  /*36*/
           :new.TOTAL_MERCADORIA, /*37*/
           :old.TOTAL_BASE_ICMS,  /*38*/
           :new.TOTAL_BASE_ICMS, /*39*/
           :old.TOTAL_ICMS,  /*40*/
           :new.TOTAL_ICMS, /*41*/
           :old.TOTAL_ICMS_ST,  /*42*/
           :new.TOTAL_ICMS_ST, /*43*/
           :old.TOTAL_BASE_ICMS_ST,  /*44*/
           :new.TOTAL_BASE_ICMS_ST, /*45*/
           :old.PESO_LIQUIDO,  /*46*/
           :new.PESO_LIQUIDO, /*47*/
           :old.PESO_BRUTO,  /*48*/
           :new.PESO_BRUTO, /*49*/
           :old.TIPO_FRETE,  /*50*/
           :new.TIPO_FRETE, /*51*/
           :old.CTE,  /*52*/
           :new.CTE  /*53*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into OBRF_162_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_EMPRESA_OLD, /*8*/
           COD_EMPRESA_NEW, /*9*/
           NUM_NOTA_FISCAL_OLD, /*10*/
           NUM_NOTA_FISCAL_NEW, /*11*/
           SERIE_NOTA_FISCAL_OLD, /*12*/
           SERIE_NOTA_FISCAL_NEW, /*13*/
           CNPJ9_OLD, /*14*/
           CNPJ9_NEW, /*15*/
           CNPJ4_OLD, /*16*/
           CNPJ4_NEW, /*17*/
           CNPJ2_OLD, /*18*/
           CNPJ2_NEW, /*19*/
           NUMERO_DANFE_NFE_OLD, /*20*/
           NUMERO_DANFE_NFE_NEW, /*21*/
           DATA_EMISSAO_OLD, /*22*/
           DATA_EMISSAO_NEW, /*23*/
           TOTAL_NOTA_OLD, /*24*/
           TOTAL_NOTA_NEW, /*25*/
           TOTAL_IPI_OLD, /*26*/
           TOTAL_IPI_NEW, /*27*/
           TOTAL_FRETE_OLD, /*28*/
           TOTAL_FRETE_NEW, /*29*/
           TOTAL_SEGURO_OLD, /*30*/
           TOTAL_SEGURO_NEW, /*31*/
           TOTAL_DESPESAS_OLD, /*32*/
           TOTAL_DESPESAS_NEW, /*33*/
           TOTAL_DESCONTO_OLD, /*34*/
           TOTAL_DESCONTO_NEW, /*35*/
           TOTAL_MERCADORIA_OLD, /*36*/
           TOTAL_MERCADORIA_NEW, /*37*/
           TOTAL_BASE_ICMS_OLD, /*38*/
           TOTAL_BASE_ICMS_NEW, /*39*/
           TOTAL_ICMS_OLD, /*40*/
           TOTAL_ICMS_NEW, /*41*/
           TOTAL_ICMS_ST_OLD, /*42*/
           TOTAL_ICMS_ST_NEW, /*43*/
           TOTAL_BASE_ICMS_ST_OLD, /*44*/
           TOTAL_BASE_ICMS_ST_NEW, /*45*/
           PESO_LIQUIDO_OLD, /*46*/
           PESO_LIQUIDO_NEW, /*47*/
           PESO_BRUTO_OLD, /*48*/
           PESO_BRUTO_NEW, /*49*/
           TIPO_FRETE_OLD, /*50*/
           TIPO_FRETE_NEW, /*51*/
           CTE_OLD, /*52*/
           CTE_NEW /*53*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.COD_EMPRESA, /*8*/
           0, /*9*/
           :old.NUM_NOTA_FISCAL, /*10*/
           0, /*11*/
           :old.SERIE_NOTA_FISCAL, /*12*/
           '', /*13*/
           :old.CNPJ9, /*14*/
           0, /*15*/
           :old.CNPJ4, /*16*/
           0, /*17*/
           :old.CNPJ2, /*18*/
           0, /*19*/
           :old.NUMERO_DANFE_NFE, /*20*/
           '', /*21*/
           :old.DATA_EMISSAO, /*22*/
           null, /*23*/
           :old.TOTAL_NOTA, /*24*/
           0, /*25*/
           :old.TOTAL_IPI, /*26*/
           0, /*27*/
           :old.TOTAL_FRETE, /*28*/
           0, /*29*/
           :old.TOTAL_SEGURO, /*30*/
           0, /*31*/
           :old.TOTAL_DESPESAS, /*32*/
           0, /*33*/
           :old.TOTAL_DESCONTO, /*34*/
           0, /*35*/
           :old.TOTAL_MERCADORIA, /*36*/
           0, /*37*/
           :old.TOTAL_BASE_ICMS, /*38*/
           0, /*39*/
           :old.TOTAL_ICMS, /*40*/
           0, /*41*/
           :old.TOTAL_ICMS_ST, /*42*/
           0, /*43*/
           :old.TOTAL_BASE_ICMS_ST, /*44*/
           0, /*45*/
           :old.PESO_LIQUIDO, /*46*/
           0, /*47*/
           :old.PESO_BRUTO, /*48*/
           0, /*49*/
           :old.TIPO_FRETE, /*50*/
           0, /*51*/
           :old.CTE, /*52*/
           '' /*53*/
         );
    end;
 end if;
end inter_tr_OBRF_162_log;

-- ALTER TRIGGER "INTER_TR_OBRF_162_LOG" ENABLE
 

/

exec inter_pr_recompile;

