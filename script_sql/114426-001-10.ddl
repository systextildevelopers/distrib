UPDATE OBRF_780 
SET OBRF_780.ESTAGIO_DOCUMENTO = 0;

commit work;

alter table obrf_780 modify (estagio_documento default 0);

exec inter_pr_recompile;
/

