create or replace package body "ST_PCK_PAIS" is

FUNCTION get_dados_codigo_pais(p_codigo_pais number, p_msg_erro in out varchar2) return t_dados_basi_165
AS 
    v_dados_basi_165 t_dados_basi_165;
BEGIN
    begin
        SELECT *  
        BULK COLLECT 
        INTO v_dados_basi_165
        FROM basi_165
        WHERE codigo_pais = p_codigo_pais;
    exception when others then
        p_msg_erro := 'Não foi possível buscar dados para o código do país!. p_codigo_pais:' || p_codigo_pais;
        return null;
    end;

    return v_dados_basi_165;

END;

end "ST_PCK_PAIS";
