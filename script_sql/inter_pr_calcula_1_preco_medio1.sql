CREATE OR REPLACE PROCEDURE "INTER_PR_CALCULA_PRECO_MEDIO1" (p_nivel              in varchar2,
                                                         p_grupo              in varchar2,
                                                         p_subgrupo           in varchar2,
                                                         p_item               in varchar2,
                                                         p_data_inicial_ficha in date,
                                                         p_data_final_ficha   in date,
                                                         p_inc_exc            in number,
                                                         p_empresa1           in number,
                                                         p_estq_300_estq_310  in varchar2,
                                                         p_preco_medio_unit      in out number,
                                                         p_preco_medio_unit_proj in out number,
                                                         p_preco_medio_unit_est  in out number) is

   v_mes_movimento number;
   v_ano_movimento number;

   p_saldo_fisico              number(18,5);
   p_saldo_financeiro          number(18,5);
   p_saldo_financeiro_proj     number(18,5);
   p_saldo_financeiro_est      number(18,5);
   p_preco_medio_unit_ant      number(18,5);
   p_preco_medio_unit_proj_ant number(18,5);
   p_preco_medio_unit_est_ant  number(18,5);

   p_saldo_fisico_ent          number(18,5);
   p_saldo_financeiro_ent      number(18,5);
   p_saldo_financeiro_proj_ent number(18,5);
   p_saldo_financeiro_est_ent  number(18,5);

   p_saldo_fisico_sai          number(18,5);
   p_saldo_financeiro_sai      number(18,5);
   p_saldo_financeiro_proj_sai number(18,5);
   p_saldo_financeiro_est_sai  number(18,5);

   v_total_movimento_pcpc_320  number(18,5);
   v_total_movto_pcpc_320_proj number(18,5);

   v_data_vigencia date;

   v_novo_valor_movimento_unit  estq_300.valor_contabil_unitario%type;
   v_novo_valor_movto_unit_proj obrf_950.valor_entrada%type;

   v_comprado_fabric            basi_030.comprado_fabric%type;
   v_comprado_fabric_015        basi_030.comprado_fabric%type;
   v_tipo_empresa               fatu_500.tipo_empresa%type;
   v_apuracao_ir                fatu_503.apuracao_ir%type;

   v_valorizacao_cardex         empr_002.valorizacao_cardex%type;
   v_valor_subproduto           basi_305.valor_subproduto%type;

   v_origem_preco_custo         rcnb_395.origem_preco_custo%type;

   v_novo_valor_custo_unit      number(15,5);

   v_novo_ano number;
   v_novo_mes number;

BEGIN

   -- busca o modo de calculo da valorizacao da cardex (1- Custo integrado ou 2- Custo arbitrado)
   begin
      select fatu_503.apuracao_ir
      into   v_apuracao_ir
      from fatu_503
      where fatu_503.codigo_empresa = p_empresa1;

   exception
      when others then
         v_apuracao_ir := 1;
   end;

   -- encontra se o produto e comprado (1) ou fabricado (2)
   begin
      select comprado_fabric
      into   v_comprado_fabric
      from   basi_030
      where  nivel_estrutura = p_nivel
      and    referencia      = p_grupo;

   exception
      when others then
         v_comprado_fabric := 1;
   end;

   --encontra se o produto é comprado (1) ou fabricado (2) ou (0) vale o basi_030  - pelo cadastro de parametro de compras

    begin
       select comprado_fabric
       into   v_comprado_fabric_015
       from basi_015
       where basi_015.codigo_empresa   = p_empresa1
       and   basi_015.nivel_estrutura  = p_nivel
       and   basi_015.grupo_estrutura  = p_grupo
       and   basi_015.subgru_estrutura = p_subgrupo
       and   basi_015.item_estrutura   = p_item;

    exception
       when others then
          v_comprado_fabric_015 := 0;
    end;

    if v_comprado_fabric_015 <> 0
    then
       v_comprado_fabric := v_comprado_fabric_015;
    end if;

   -- verifica se o produto se encontra no cadastro de origem do valor a ser buscado.
   begin
      -- tenta buscar a parametrizacao pelo produto completo
      select origem_preco_custo
      into   v_origem_preco_custo
      from rcnb_395
      where  nivel    = p_nivel
      and    grupo    = p_grupo
      and    subgrupo = p_subgrupo
      and    item     = p_item;

   exception
      when no_data_found then
         begin
            -- tenta buscar a parametrizacao pelo produto ate o subgrupo
            select origem_preco_custo
            into   v_origem_preco_custo
            from rcnb_395
            where  nivel    = p_nivel
            and    grupo    = p_grupo
            and    subgrupo = p_subgrupo
            and    item     = '000000';

         exception
            when no_data_found then
               begin
                  -- tenta buscar a parametrizacao pelo produto ate o grupo
                  select origem_preco_custo
                  into   v_origem_preco_custo
                  from rcnb_395
                  where  nivel    = p_nivel
                  and    grupo    = p_grupo
                  and    subgrupo = '000'
                  and    item     = '000000';

               exception
                  when no_data_found then
                     v_origem_preco_custo := 0;  -- se nao encontrar, seta o parametro para o default (busca pela nota)
               end;  -- por grupo
         end;        -- por subgrupo
   end;              -- por item completo

   -- le parametro global para identificar se a cardex e calulada por deposito ou por empresa / produto
   -- 1 = por deposito ou 2 = por empresa / produto
   begin
      select empr_002.valorizacao_cardex
      into   v_valorizacao_cardex
      from   empr_002;

   exception
      when OTHERS then
         v_valorizacao_cardex := 1;
   end;

   -- Le:
   --  * Saldo Fisico
   --  * Saldo Financeiro
   --  * Saldo Financeiro Projetado
   --  * Saldo Financeiro Estimado
   --  * Preco Medio
   --  * Preco Medio Projetado
   --  * Preco Medio Estimado
   -- Filtrando por:
   --  * Produto
   --  * Periodo anterior ao calculado
   --  * Deposito de uma das empresas parametrizadas e informado no cad. temporario (ou 9999)

   begin
      select nvl(sum(estq_301.saldo_fisico), 0),
             nvl(sum(estq_301.saldo_financeiro), 0),
             nvl(sum(estq_301.saldo_financeiro_proj), 0),
             nvl(sum(estq_301.saldo_financeiro_estimado), 0),
             nvl(avg(estq_301.preco_medio_unitario), 0),
             nvl(avg(estq_301.preco_medio_unit_proj), 0),
             nvl(avg(estq_301.preco_medio_unit_estimado), 0)
      into   p_saldo_fisico,
             p_saldo_financeiro,
             p_saldo_financeiro_proj,
             p_saldo_financeiro_est,
             p_preco_medio_unit_ant,
             p_preco_medio_unit_proj_ant,
             p_preco_medio_unit_est_ant
      from   estq_301
      where  estq_301.nivel_estrutura = p_nivel
      and    estq_301.grupo_estrutura = p_grupo
      and    estq_301.subgrupo_estrutura = p_subgrupo
      and    estq_301.item_estrutura = p_item
      and    estq_301.mes_ano_movimento in (select max(a.mes_ano_movimento)
                                            from   estq_301 a
                                            where  a.nivel_estrutura = p_nivel
                                            and    a.grupo_estrutura = p_grupo
                                            and    a.subgrupo_estrutura = p_subgrupo
                                            and    a.item_estrutura = p_item
                                            and    a.saldo_fisico is not null
                                            and    a.mes_ano_movimento < p_data_inicial_ficha
                                            and    a.codigo_deposito = estq_301.codigo_deposito)
      and    estq_301.codigo_deposito   in (select basi_205.codigo_deposito
                                            from   basi_205
                                            where  basi_205.controla_ficha_cardex = 1
                                            and   (basi_205.tipo_valorizacao      = 1 or v_apuracao_ir = 1)
                                            and    basi_205.local_deposito        = p_empresa1);
   exception
      when others then
         p_saldo_fisico              := 0.000;
         p_saldo_financeiro          := 0.000;
         p_saldo_financeiro_proj     := 0.000;
         p_saldo_financeiro_est      := 0.000;
         p_preco_medio_unit_ant      := 0.000;
         p_preco_medio_unit_proj_ant := 0.000;
         p_preco_medio_unit_est_ant  := 0.000;
   end;

   p_saldo_fisico_ent          := 0.000;
   p_saldo_financeiro_ent      := 0.000;
   p_saldo_financeiro_est_ent  := 0.000;
   p_saldo_financeiro_proj_ent := 0.000;

   p_saldo_fisico_sai          := 0.000;
   p_saldo_financeiro_sai      := 0.000;
   p_saldo_financeiro_est_sai  := 0.000;
   p_saldo_financeiro_proj_sai := 0.000;

   -- Le movimentos de entrada, e os acumula conforme as regras abaixo.
   for reg_estq300 in (select estq_300.codigo_deposito,     estq_300.nivel_estrutura,
                              estq_300.grupo_estrutura,     estq_300.subgrupo_estrutura,
                              estq_300.item_estrutura,      estq_300.quantidade,
                              estq_300.tabela_origem,       estq_300.valor_total,
                              estq_300.data_movimento,      estq_300.valor_movimento_unitario,
                              estq_300.numero_documento,    estq_300.codigo_transacao,
                              estq_005.altera_custo,        estq_300.entrada_saida tipo_e_s,
                              estq_300.sequencia_insercao,  estq_300.valor_movimento_unitario_proj,
                              estq_005.istajustefinanceiro
                       from   estq_300, estq_005
                       where  estq_300.codigo_transacao   = estq_005.codigo_transacao
                       and    estq_300.nivel_estrutura    = p_nivel
                       and    estq_300.grupo_estrutura    = p_grupo
                       and    estq_300.subgrupo_estrutura = p_subgrupo
                       and    estq_300.item_estrutura     = p_item
                       and    estq_300.data_movimento     between p_data_inicial_ficha and p_data_final_ficha
    --                   and    estq_300.entrada_saida      = 'E'
                       and    estq_005.calcula_preco      = 1
                       and    estq_300.codigo_deposito in (select basi_205.codigo_deposito from   basi_205
                                                           where  basi_205.controla_ficha_cardex = 1
                                                           and   (basi_205.tipo_valorizacao      = 1 or v_apuracao_ir = 1)
                                                           and    basi_205.local_deposito = p_empresa1)
                       and    lower(p_estq_300_estq_310) = 'estq_300'

                       and (estq_300.numero_lote = 0 or (estq_300.numero_lote <> 0 and
                            not exists (select 1 from supr_010
                                        where supr_010.lote_fornecedor = estq_300.numero_lote
                                        and supr_010.lote_consignado = 1)))
                       UNION ALL
                       select estq_310.codigo_deposito,     estq_310.nivel_estrutura,
                              estq_310.grupo_estrutura,     estq_310.subgrupo_estrutura,
                              estq_310.item_estrutura,      estq_310.quantidade,
                              estq_310.tabela_origem,       estq_310.valor_total,
                              estq_310.data_movimento,      estq_310.valor_movimento_unitario,
                              estq_310.numero_documento,    estq_310.codigo_transacao,
                              estq_005.altera_custo,        estq_310.entrada_saida tipo_e_s,
                              estq_310.sequencia_insercao,  estq_310.valor_movimento_unitario_proj,
                              estq_005.istajustefinanceiro
                       from   estq_310, estq_005
                       where  estq_310.codigo_transacao   = estq_005.codigo_transacao
                       and    estq_310.nivel_estrutura    = p_nivel
                       and    estq_310.grupo_estrutura    = p_grupo
                       and    estq_310.subgrupo_estrutura = p_subgrupo
                       and    estq_310.item_estrutura     = p_item
                       and    estq_310.data_movimento     between p_data_inicial_ficha and p_data_final_ficha
--                       and    estq_310.entrada_saida      = 'E'
                       and    estq_005.calcula_preco      = 1
                       and    estq_310.codigo_deposito in (select basi_205.codigo_deposito from   basi_205
                                                           where  basi_205.controla_ficha_cardex = 1
                                                           and   (basi_205.tipo_valorizacao      = 1 or v_apuracao_ir = 1)
                                                           and    basi_205.local_deposito = p_empresa1)
                       and    lower(p_estq_300_estq_310) = 'estq_310'

                       and (estq_310.numero_lote = 0 or (estq_310.numero_lote <> 0 and
                            not exists (select 1 from supr_010
                                        where supr_010.lote_fornecedor = estq_310.numero_lote
                                        and supr_010.lote_consignado = 1)))

                       order  by codigo_deposito,
                                 nivel_estrutura,
                                 grupo_estrutura,
                                 subgrupo_estrutura,
                                 item_estrutura,
                                 data_movimento,
                                 tipo_e_s,
                                 sequencia_insercao)
   loop

      -- le a empresa para pegar o seu tipo
      begin
         select tipo_empresa
         into   v_tipo_empresa
         from   fatu_500
         where  codigo_empresa = reg_estq300.codigo_deposito;
      exception
         when others then
            v_tipo_empresa := 1;
      end;

      if reg_estq300.istajustefinanceiro = 1
      then
         if reg_estq300.tipo_e_s = 'E'
         then
            p_saldo_fisico_ent := round(p_saldo_fisico_ent + reg_estq300.quantidade, 5);
         else
            p_saldo_fisico_sai := round(p_saldo_fisico_sai + reg_estq300.quantidade, 5);
         end if;

         if reg_estq300.quantidade <> 0.00
         then

            if reg_estq300.tipo_e_s = 'E'
            then
               p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                   (reg_estq300.valor_movimento_unitario * reg_estq300.quantidade), 5);
               p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                (reg_estq300.valor_movimento_unitario * reg_estq300.quantidade), 5);
               p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                   (reg_estq300.valor_movimento_unitario * reg_estq300.quantidade), 5);
            else
               p_saldo_financeiro_sai      := round(p_saldo_financeiro_sai +
                                                   (reg_estq300.valor_movimento_unitario * reg_estq300.quantidade), 5);
               p_saldo_financeiro_est_sai  := round(p_saldo_financeiro_est_sai +
                                                (reg_estq300.valor_movimento_unitario * reg_estq300.quantidade), 5);
               p_saldo_financeiro_proj_sai := round(p_saldo_financeiro_proj_sai +
                                                   (reg_estq300.valor_movimento_unitario * reg_estq300.quantidade), 5);
            end if;

         else

            if reg_estq300.tipo_e_s = 'E'
            then
               p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                   (reg_estq300.valor_movimento_unitario), 5);
               p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                   (reg_estq300.valor_movimento_unitario), 5);
               p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                   (reg_estq300.valor_movimento_unitario), 5);

            else
               p_saldo_financeiro_sai      := round(p_saldo_financeiro_sai +
                                                   (reg_estq300.valor_movimento_unitario), 5);
               p_saldo_financeiro_est_sai  := round(p_saldo_financeiro_est_sai +
                                                   (reg_estq300.valor_movimento_unitario), 5);
               p_saldo_financeiro_proj_sai := round(p_saldo_financeiro_proj_sai +
                                                   (reg_estq300.valor_movimento_unitario), 5);
            end if;
         end if;
      else
         -- se nivel = 9 (comprado) ou se a empresa for loja e a origem do movimento
         -- for nota fiscal de entrada (OBRF_015), entao o valor do movimento unitario
         -- (preco_custo) sera o proprio movimento
--         if  reg_estq300.nivel_estrutura = '9' or  
         if v_comprado_fabric = 1
         or (v_tipo_empresa              = 2   and reg_estq300.tabela_origem = 'OBRF_015')
         or (reg_estq300.nivel_estrutura in ('1', '2', '4', '7','9') and reg_estq300.altera_custo  = 1 and reg_estq300.tabela_origem = 'OBRF_015')
         or (reg_estq300.quantidade = 0.000 and reg_estq300.tabela_origem = 'ESTQ_300')
         then

            if reg_estq300.tipo_e_s = 'E'
            then
               p_saldo_fisico_ent := round(p_saldo_fisico_ent +
                                           reg_estq300.quantidade, 5);

            else
               p_saldo_fisico_sai := round(p_saldo_fisico_sai +
                                           reg_estq300.quantidade, 5);
            end if;
            -- se a referencia estiver paramerizada para a busca do valor no basi_010,
            -- entao despreza o valor do movimento e le o preco_custo da basi_010
            if v_origem_preco_custo = 1
            then

               select round(preco_custo, 5)
               into   v_novo_valor_custo_unit
               from   basi_010
               where  nivel_estrutura  = p_nivel
               and    grupo_estrutura  = p_grupo
               and    subgru_estrutura = p_subgrupo
               and    item_estrutura   = p_item;

               if reg_estq300.quantidade <> 0.00
               then

                  if reg_estq300.tipo_e_s = 'E'
                  then
                     p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                         (v_novo_valor_custo_unit * reg_estq300.quantidade), 5);
                     p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                         (v_novo_valor_custo_unit * reg_estq300.quantidade), 5);
                     p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                         (v_novo_valor_custo_unit * reg_estq300.quantidade), 5);

                  else
                     p_saldo_financeiro_sai      := round(p_saldo_financeiro_sai +
                                                         (v_novo_valor_custo_unit * reg_estq300.quantidade), 5);
                     p_saldo_financeiro_est_sai  := round(p_saldo_financeiro_est_sai +
                                                         (v_novo_valor_custo_unit * reg_estq300.quantidade), 5);
                     p_saldo_financeiro_proj_sai := round(p_saldo_financeiro_proj_sai +
                                                         (v_novo_valor_custo_unit * reg_estq300.quantidade), 5);
                  end if;
               else

                  if reg_estq300.tipo_e_s = 'E'
                  then
                     p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                         (v_novo_valor_custo_unit), 5);
                     p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                         (v_novo_valor_custo_unit), 5);
                     p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                         (v_novo_valor_custo_unit), 5);
                  else
                     p_saldo_financeiro_sai      := round(p_saldo_financeiro_sai +
                                                         (v_novo_valor_custo_unit), 5);
                     p_saldo_financeiro_est_sai  := round(p_saldo_financeiro_est_sai +
                                                         (v_novo_valor_custo_unit), 5);
                     p_saldo_financeiro_proj_sai := round(p_saldo_financeiro_proj_sai +
                                                         (v_novo_valor_custo_unit), 5);
                  end if;
               end if;
            else

               if reg_estq300.tabela_origem = 'OBRF_015'
               then

                  if reg_estq300.tipo_e_s = 'E'
                  then
                     p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                         (reg_estq300.valor_total), 5);
                     p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                         (reg_estq300.valor_total), 5);
                     p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                         (reg_estq300.valor_total), 5);

                  else
                     p_saldo_financeiro_sai      := round(p_saldo_financeiro_sai +
                                                         (reg_estq300.valor_total), 5);
                     p_saldo_financeiro_est_sai  := round(p_saldo_financeiro_est_sai +
                                                         (reg_estq300.valor_total), 5);
                     p_saldo_financeiro_proj_sai := round(p_saldo_financeiro_proj_sai +
                                                         (reg_estq300.valor_total), 5);
                  end if;

               else

                  if reg_estq300.quantidade <> 0.00
                  then

                     if reg_estq300.tipo_e_s = 'E'
                     then
                        p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                            (reg_estq300.valor_movimento_unitario * reg_estq300.quantidade), 5);
                        p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                            (reg_estq300.valor_movimento_unitario * reg_estq300.quantidade), 5);
                        p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                            (reg_estq300.valor_movimento_unitario * reg_estq300.quantidade), 5);

                     else
                        p_saldo_financeiro_sai      := round(p_saldo_financeiro_sai +
                                                            (reg_estq300.valor_movimento_unitario * reg_estq300.quantidade), 5);
                        p_saldo_financeiro_est_sai  := round(p_saldo_financeiro_est_sai +
                                                            (reg_estq300.valor_movimento_unitario * reg_estq300.quantidade), 5);
                        p_saldo_financeiro_proj_sai := round(p_saldo_financeiro_proj_sai +
                                                            (reg_estq300.valor_movimento_unitario * reg_estq300.quantidade), 5);

                     end if;

                  else

                     if reg_estq300.tipo_e_s = 'E'
                     then
                        p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                            (reg_estq300.valor_movimento_unitario), 5);
                        p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                            (reg_estq300.valor_movimento_unitario), 5);
                        p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                            (reg_estq300.valor_movimento_unitario), 5);
                     else
                        p_saldo_financeiro_sai      := round(p_saldo_financeiro_sai +
                                                            (reg_estq300.valor_movimento_unitario), 5);
                        p_saldo_financeiro_est_sai  := round(p_saldo_financeiro_est_sai +
                                                            (reg_estq300.valor_movimento_unitario), 5);
                        p_saldo_financeiro_proj_sai := round(p_saldo_financeiro_proj_sai +
                                                            (reg_estq300.valor_movimento_unitario), 5);
                     end if;
                  end if;
               end if;
            end if;
         else

            -- encontra preco do movimento com base na ficha de custos
            v_mes_movimento := to_number(to_char(reg_estq300.data_movimento, 'MM'));
            v_ano_movimento := to_number(to_char(reg_estq300.data_movimento, 'YYYY'));

            begin
               select max(basi_350.ano)
               into   v_novo_ano
               from   basi_350
               where  basi_350.codigo_empresa = p_empresa1
                 and  basi_350.nivel_estrutura = reg_estq300.nivel_estrutura
                 and  basi_350.grupo_estrutura = reg_estq300.grupo_estrutura
                 and  basi_350.subgru_estrutura = reg_estq300.subgrupo_estrutura
                 and  basi_350.item_estrutura = reg_estq300.item_estrutura
                 and  basi_350.ano <= v_ano_movimento
                 and  basi_350.custo_fabricacao > 0.000;

            exception
               when others then
                  v_novo_ano := 0;
            end;

            if v_novo_ano < v_ano_movimento
            then v_mes_movimento := 12;
            end if;

            begin
               select max(basi_350.mes)
               into   v_novo_mes
               from basi_350
               where basi_350.codigo_empresa = p_empresa1
                 and basi_350.ano = v_novo_ano
                 and basi_350.mes <= v_mes_movimento
                 and basi_350.nivel_estrutura = reg_estq300.nivel_estrutura
                 and basi_350.grupo_estrutura = reg_estq300.grupo_estrutura
                 and basi_350.subgru_estrutura = reg_estq300.subgrupo_estrutura
                 and basi_350.item_estrutura = reg_estq300.item_estrutura
                 and basi_350.custo_fabricacao > 0.000;

            exception
               when others then
                  v_novo_mes := 0;
            end;

            begin
               select nvl(sum(basi_350.custo_fabricacao), 0.000)
               into   v_novo_valor_custo_unit
               from basi_350
               where basi_350.mes = v_novo_mes
                 and basi_350.ano = v_novo_ano
                 and basi_350.codigo_empresa = p_empresa1
                 and basi_350.nivel_estrutura = reg_estq300.nivel_estrutura
                 and basi_350.grupo_estrutura = reg_estq300.grupo_estrutura
                 and basi_350.subgru_estrutura = reg_estq300.subgrupo_estrutura
                 and basi_350.item_estrutura = reg_estq300.item_estrutura;

            end;

            if v_novo_valor_custo_unit = 0.00
            then

               select round(preco_custo, 5)
               into   v_novo_valor_custo_unit
               from   basi_010
               where  nivel_estrutura = reg_estq300.nivel_estrutura
               and    grupo_estrutura = reg_estq300.grupo_estrutura
               and    subgru_estrutura = reg_estq300.subgrupo_estrutura
               and    item_estrutura = reg_estq300.item_estrutura;

            end if;

            if reg_estq300.quantidade <> 0.00
            then

               if reg_estq300.tipo_e_s = 'E'
               then
                  p_saldo_fisico_ent          := round(p_saldo_fisico_ent +
                                                       reg_estq300.quantidade, 5);
                  p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                      (v_novo_valor_custo_unit * reg_estq300.quantidade), 5);
                  p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                      (v_novo_valor_custo_unit * reg_estq300.quantidade), 5);
                  p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                      (v_novo_valor_custo_unit * reg_estq300.quantidade), 5);
               else
                  p_saldo_fisico_sai          := round(p_saldo_fisico_sai +
                                                       reg_estq300.quantidade, 5);
                  p_saldo_financeiro_sai      := round(p_saldo_financeiro_sai +
                                                      (v_novo_valor_custo_unit * reg_estq300.quantidade), 5);
                  p_saldo_financeiro_est_sai  := round(p_saldo_financeiro_est_sai +
                                                      (v_novo_valor_custo_unit * reg_estq300.quantidade), 5);
                  p_saldo_financeiro_proj_sai := round(p_saldo_financeiro_proj_sai +
                                                      (v_novo_valor_custo_unit * reg_estq300.quantidade), 5);
               end if;

            else

               if reg_estq300.tipo_e_s = 'E'
               then
                  p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent, 5) +
                                                      (v_novo_valor_custo_unit);
                  p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent, 5) +
                                                      (v_novo_valor_custo_unit);
                  p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent, 5) +
                                                      (v_novo_valor_custo_unit);
               else
                  p_saldo_financeiro_sai      := round(p_saldo_financeiro_sai, 5) +
                                                      (v_novo_valor_custo_unit);
                  p_saldo_financeiro_est_sai  := round(p_saldo_financeiro_est_sai, 5) +
                                                      (v_novo_valor_custo_unit);
                  p_saldo_financeiro_proj_sai := round(p_saldo_financeiro_proj_sai, 5) +
                                                      (v_novo_valor_custo_unit);
               end if;
            end if;

            -- se a tabela de origem for 'PCPC_320' or 'PCPC_330', quer dizer que o movimento foi gerado
            -- por uma transferencia de pecas para quilos, assim a valorizacao do movimento
            -- e' com base nos movimentos dos produtos que originaram o movimento em quilo.
            -- Por isso, e'  lido todos os movimentos da cardex que originou a transferncia
            -- para quilo, sendo sua soma o valor total da movimentacao em quilo, e este
            -- deve ser dividido pela quantidade (peso total) para se achar o valor unitario
            -- do movimento
            if reg_estq300.tabela_origem = 'PCPC_320' or
               reg_estq300.tabela_origem = 'PCPC_330'
            then

               -- encontra o valor do movimento em quilo, tomando como base a soma de
               -- todos os movimentos em pecas que originaram o movimento em quilo
               select nvl(sum(estq_300.valor_movimento_unitario *
                              estq_300.quantidade), 0),
                      nvl(sum(estq_300.valor_movimento_unitario_proj *
                              estq_300.quantidade), 0)
               into   v_total_movimento_pcpc_320, v_total_movto_pcpc_320_proj
               from   estq_300, pcpc_320, pcpc_300
               where  estq_300.numero_documento = reg_estq300.numero_documento
               and    estq_300.tabela_origem in ('PCPC_320', 'PCPC_330')
               and    estq_300.entrada_saida = 'S'
               and    estq_300.numero_documento = pcpc_320.numero_volume
               and    pcpc_320.cod_tipo_volume = pcpc_300.cod_tipo_volume
               and    pcpc_300.caracteristica = 0;

               if v_total_movimento_pcpc_320 > 0.0
               then

                  if reg_estq300.quantidade <> 0.00
                  then

                     p_saldo_fisico_ent          := round(p_saldo_fisico_ent +
                                                          reg_estq300.quantidade, 5);
                     p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                          v_total_movimento_pcpc_320, 5);
                     p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                          v_total_movimento_pcpc_320, 5);
                     p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                          v_total_movto_pcpc_320_proj, 5);

                  else

                     p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                          v_total_movimento_pcpc_320, 5);
                     p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                          v_total_movimento_pcpc_320, 5);
                     p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                          v_total_movto_pcpc_320_proj, 5);

                  end if;
               else
                  p_saldo_fisico_ent          := round(p_saldo_fisico_ent +
                                                       reg_estq300.quantidade, 5);
                  p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                       (reg_estq300.valor_movimento_unitario *
                                                       reg_estq300.quantidade), 5);
                  p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                       (reg_estq300.valor_movimento_unitario *
                                                       reg_estq300.quantidade), 5);
                  p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                       (reg_estq300.valor_movimento_unitario_proj *
                                                       reg_estq300.quantidade), 5);

               end if;
            else
               -- se a tabela de origem for 'OBRF_015', quer dizer que o movimento foi gerado
               -- por uma Nota Fiscal de Entrada, assim a valorizacao do movimento sera com
               -- base no cadastro de valores de entradas OBRF_950. Caso nao tenha esse cadastro
               -- o movimento sera valorizado pelo proprio valor de custo
               if reg_estq300.tabela_origem = 'ESTQ_060' and
                  reg_estq300.nivel_estrutura = '7'
               then
                  select nvl(max(obrf_950.data_vigencia), null)
                  into   v_data_vigencia
                  from   obrf_950
                  where  obrf_950.nivel_produto = reg_estq300.nivel_estrutura
                  and    obrf_950.grupo_produto = reg_estq300.grupo_estrutura
                  and    obrf_950.subgru_produto = reg_estq300.subgrupo_estrutura
                  and    obrf_950.item_produto = reg_estq300.item_estrutura
                  and    obrf_950.codigo_transacao = reg_estq300.codigo_transacao
                  and    obrf_950.data_vigencia <= reg_estq300.data_movimento;

                  if v_data_vigencia is not null
                  then
                     select obrf_950.valor_entrada, obrf_950.valor_entrada
                     into   v_novo_valor_movimento_unit,
                            v_novo_valor_movto_unit_proj
                     from   obrf_950
                     where  obrf_950.nivel_produto    = reg_estq300.nivel_estrutura
                     and    obrf_950.grupo_produto    = reg_estq300.grupo_estrutura
                     and    obrf_950.subgru_produto   = reg_estq300.subgrupo_estrutura
                     and    obrf_950.item_produto     = reg_estq300.item_estrutura
                     and    obrf_950.codigo_transacao = reg_estq300.codigo_transacao
                     and    obrf_950.data_vigencia    = v_data_vigencia;

                     p_saldo_fisico_ent          := round(p_saldo_fisico_ent +
                                                          reg_estq300.quantidade, 5);
                     p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                         (v_novo_valor_movimento_unit * reg_estq300.quantidade), 5);
                     p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                         (v_novo_valor_movimento_unit * reg_estq300.quantidade), 5);
                     p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                         (v_novo_valor_movto_unit_proj * reg_estq300.quantidade), 5);

                  end if;
               end if;
            end if;
         end if;

         -- encontra preco do movimento com base na ficha de custos
         v_mes_movimento := to_number(to_char(reg_estq300.data_movimento, 'MM'));
         v_ano_movimento := to_number(to_char(reg_estq300.data_movimento, 'YYYY'));

         -- Le o valor cadastrado para o Sub-Produto na tela
         -- Atualizacao de Valores dos Sub-Produtos (basi_f432)
         begin
            select basi_305.valor_subproduto
            into   v_valor_subproduto
            from   basi_305
            where  basi_305.codigo_empresa = p_empresa1
            and    basi_305.mes = v_mes_movimento
            and    basi_305.ano = v_ano_movimento
            and    basi_305.nivel_subproduto = reg_estq300.nivel_estrutura
            and    basi_305.grupo_subproduto = reg_estq300.grupo_estrutura
            and    basi_305.subgru_subproduto = reg_estq300.subgrupo_estrutura
            and    basi_305.item_subproduto = reg_estq300.item_estrutura
            and   exists (select * from   basi_300
                          where basi_300.nivel_subproduto  = basi_305.nivel_subproduto
                            and basi_300.grupo_subproduto  = basi_305.grupo_subproduto
                            and basi_300.subgru_subproduto = basi_305.subgru_subproduto
                            and basi_300.item_subproduto   = basi_305.item_subproduto);
         exception
            when OTHERS then
               v_valor_subproduto := 0.00;
         end;

         -- Verifica se a valorizacao das entradas dos Sub-Produtos
         -- deve considerar o (1) Preco Medio do movimento anterior ou
         -- o (2) Valor informado na tela de Sub-Produtos (basi_f432).
         -- Este e um Parametro global de Estoques (aba '++').

         if v_valorizacao_cardex = 2 and v_valor_subproduto > 0
         then
            p_saldo_fisico_ent          := round(p_saldo_fisico_ent +
                                                 reg_estq300.quantidade, 5);
            p_saldo_financeiro_ent      := round(p_saldo_financeiro_ent +
                                                (v_valor_subproduto * reg_estq300.quantidade), 5);
            p_saldo_financeiro_est_ent  := round(p_saldo_financeiro_est_ent +
                                                (v_valor_subproduto * reg_estq300.quantidade), 5);
            p_saldo_financeiro_proj_ent := round(p_saldo_financeiro_proj_ent +
                                                (v_valor_subproduto * reg_estq300.quantidade), 5);
         end if;
      end if;
   end loop;

   begin

      -- Le:
      --  * Saldo Fisico
      --  * Saldo Financeiro
      --  * Saldo Financeiro Projetado
      --  * Saldo Financeiro Estimado
      -- Filtrando por:
      --  * Produto
      --  * Periodo calculado
      --  * Deposito de uma das empresas parametrizadas e informado no cad. temporario (ou 9999)
      --  * Movimentos de saida
      --  * Atualizam preco medio
      --  * Tenham quantidade = 0 (zero)
if 1 = 2
then
      if lower(p_estq_300_estq_310) = 'estq_300'
      then
         select sum(estq_300.quantidade),
                sum(decode(estq_300.quantidade,0,estq_300.valor_movimento_unitario,estq_300.quantidade * estq_300.valor_movimento_unitario)),
                sum(decode(estq_300.quantidade,0,estq_300.valor_movimento_unitario,estq_300.quantidade * estq_300.valor_movimento_unitario_proj)),
                sum(decode(estq_300.quantidade,0,estq_300.valor_movimento_unitario,estq_300.quantidade * estq_300.valor_movto_unit_estimado))
         into   p_saldo_fisico_sai,
                p_saldo_financeiro_sai,
                p_saldo_financeiro_proj_sai,
                p_saldo_financeiro_est_sai
         from   estq_300, estq_005
         where  estq_300.nivel_estrutura    = p_nivel
         and    estq_300.grupo_estrutura    = p_grupo
         and    estq_300.subgrupo_estrutura = p_subgrupo
         and    estq_300.item_estrutura     = p_item
         and    estq_300.data_movimento     between p_data_inicial_ficha and p_data_final_ficha
         and    estq_300.entrada_saida      = 'X' -- 'S'
         and    estq_300.quantidade         = 0
         and    estq_005.calcula_preco      = 1
         and    estq_300.codigo_deposito    in (select basi_205.codigo_deposito from basi_205
                                                where  basi_205.controla_ficha_cardex = 1
                                                and   (basi_205.tipo_valorizacao      = 1 or v_apuracao_ir = 1)
                                                and    basi_205.local_deposito        = p_empresa1)
         and    estq_300.codigo_transacao  = estq_005.codigo_transacao

         and (estq_300.numero_lote = 0 or (estq_300.numero_lote <> 0 and
              not exists (select 1 from supr_010
                          where supr_010.lote_fornecedor = estq_300.numero_lote
                          and supr_010.lote_consignado = 1)));
      else
         select sum(estq_310.quantidade),
                sum(decode(estq_310.quantidade,0,estq_310.valor_movimento_unitario,estq_310.quantidade * estq_310.valor_movimento_unitario)),
                sum(decode(estq_310.quantidade,0,estq_310.valor_movimento_unitario,estq_310.quantidade * estq_310.valor_movimento_unitario_proj)),
                sum(decode(estq_310.quantidade,0,estq_310.valor_movimento_unitario,estq_310.quantidade * estq_310.valor_movto_unit_estimado))
         into   p_saldo_fisico_sai,
                p_saldo_financeiro_sai,
                p_saldo_financeiro_proj_sai,
                p_saldo_financeiro_est_sai
         from   estq_310, estq_005
         where  estq_310.nivel_estrutura    = p_nivel
         and    estq_310.grupo_estrutura    = p_grupo
         and    estq_310.subgrupo_estrutura = p_subgrupo
         and    estq_310.item_estrutura     = p_item
         and    estq_310.data_movimento     between p_data_inicial_ficha and p_data_final_ficha
         and    estq_310.entrada_saida      = 'X' --'S'
         and    estq_310.quantidade         = 0
         and    estq_005.calcula_preco      = 1
         and    estq_310.codigo_deposito    in (select basi_205.codigo_deposito from basi_205
                                                where  basi_205.controla_ficha_cardex = 1
                                                and   (basi_205.tipo_valorizacao      = 1 or v_apuracao_ir = 1)
                                                and    basi_205.local_deposito        = p_empresa1)
         and    estq_310.codigo_transacao  = estq_005.codigo_transacao

         and (estq_310.numero_lote = 0 or (estq_310.numero_lote <> 0 and
              not exists (select 1 from supr_010
                          where supr_010.lote_fornecedor = estq_310.numero_lote
                          and supr_010.lote_consignado = 1)));

      end if;

      if p_saldo_fisico_sai is null
      then
         p_saldo_fisico_sai := 0.000;
      end if;

      if p_saldo_financeiro_sai is null
      then
         p_saldo_financeiro_sai := 0.000;
      end if;

      if p_saldo_financeiro_proj_sai is null
      then
         p_saldo_financeiro_proj_sai := 0.000;
      end if;

      if p_saldo_financeiro_est_sai is null
      then
         p_saldo_financeiro_est_sai := 0.000;
      end if;
end if;

   end;

   if p_saldo_financeiro > 0 and p_saldo_fisico > 0
   then p_preco_medio_unit_ant := p_saldo_financeiro / p_saldo_fisico;
   end if;

   if p_saldo_financeiro_proj > 0 and p_saldo_fisico > 0
   then p_preco_medio_unit_proj_ant := p_saldo_financeiro_proj / p_saldo_fisico;
   end if;

   if p_saldo_financeiro_est > 0 and p_saldo_fisico > 0
   then p_preco_medio_unit_est_ant := p_saldo_financeiro_est / p_saldo_fisico;
   end if;

  dbms_output.put_line(to_char(p_saldo_fisico, '999,999,990.000'));
  dbms_output.put_line(to_char(p_saldo_fisico_ent, '999,999,990.000'));
  dbms_output.put_line(to_char(p_saldo_fisico_sai, '999,999,990.000'));


  dbms_output.put_line(to_char(p_saldo_financeiro, '999,999,990.000'));
  dbms_output.put_line(to_char(p_saldo_financeiro_ent, '999,999,990.000'));
  dbms_output.put_line(to_char(p_saldo_financeiro_sai, '999,999,990.000'));


   p_saldo_fisico          := p_saldo_fisico +
                              p_saldo_fisico_ent -
                              p_saldo_fisico_sai;
   p_saldo_financeiro      := p_saldo_financeiro +
                              p_saldo_financeiro_ent -
                              p_saldo_financeiro_sai;
   p_saldo_financeiro_proj := p_saldo_financeiro_proj +
                              p_saldo_financeiro_proj_ent -
                              p_saldo_financeiro_proj_sai;
   p_saldo_financeiro_est  := p_saldo_financeiro_est +
                              p_saldo_financeiro_est_ent -
                              p_saldo_financeiro_est_sai;

   if p_saldo_fisico > 0.000 and p_saldo_financeiro > 0.000
   then
      p_preco_medio_unit      := p_saldo_financeiro / p_saldo_fisico;
      p_preco_medio_unit_proj := p_saldo_financeiro_proj / p_saldo_fisico;
      p_preco_medio_unit_est  := p_saldo_financeiro_est / p_saldo_fisico;
   else
      p_preco_medio_unit      := p_preco_medio_unit_ant;
      p_preco_medio_unit_proj := p_preco_medio_unit_proj_ant;
      p_preco_medio_unit_est  := p_preco_medio_unit_est_ant;
   end if;

end inter_pr_calcula_preco_medio1;
/
exec inter_pr_recompile;
/
