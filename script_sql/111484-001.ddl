create table obrf_014 (
  nota_fiscal number(9),
  serie varchar(3),
  cnpj9 number(9),
  cnpj4 number(4),
  cnpj2 number(2),
  nivel varchar(1),
  grupo varchar(5),
  subgrupo varchar(3),
  item varchar(6),
  qtde_lida number(14,3),
  qtde_nota number(14,3),
  situacao number(1),
  
  constraint pk_obrf_014 primary key (nota_fiscal, serie, cnpj9, cnpj4, cnpj2, nivel,
                                      grupo, subgrupo, item)
);

alter table obrf_014
add constraint fk_obrf_014_nota_fiscal
    foreign key (nota_fiscal, serie, cnpj9, cnpj4, cnpj2)
    references OBRF_010(documento, serie, cgc_cli_for_9, cgc_cli_for_4, cgc_cli_for_2) on delete cascade;

alter table obrf_014
add constraint fk_obrf_014_PRODUTO
    foreign key (nivel, grupo, subgrupo, item)
    references BASI_010(nivel_estrutura, grupo_estrutura, subgru_estrutura, item_estrutura) on delete cascade;


create synonym systextilrpt.obrf_014 for obrf_014; 

comment on table obrf_014 is 'Conferência da quantidade e de itens da nota fiscal';

begin
  -- Call the procedure
  inter_pr_recompile;
end;

--exec inter_pr_recompile;
