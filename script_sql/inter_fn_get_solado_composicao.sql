create or replace FUNCTION inter_fn_get_solado_composicao(p_faixa_etaria IN hdoc_001.codigo%TYPE, p_tipo_descr NUMBER)
RETURN VARCHAR2
IS
    v_descr VARCHAR2(100);
    v_descr_ing VARCHAR2(100);
    v_descr_esp VARCHAR2(100);
    v_solado_comp VARCHAR2(100);
BEGIN
    BEGIN
        SELECT h.descricao, h.descr_ingles, h.descr_espanhol
        INTO v_descr, v_descr_ing, v_descr_esp
        FROM hdoc_001 h
        WHERE h.tipo = 516
        AND h.codigo = p_faixa_etaria; -- basi_220.faixa_etaria

    EXCEPTION
        WHEN no_data_found THEN
            v_descr := NULL;
            v_descr_ing := NULL;
            v_descr_esp := NULL;
    END;

    CASE p_tipo_descr
        WHEN 1 THEN
            v_solado_comp := v_descr;
        WHEN 2 THEN
            v_solado_comp := v_descr_ing;
        WHEN 3 THEN
            v_solado_comp := v_descr_esp;
        ELSE
            v_solado_comp := NULL;
    END CASE;

    RETURN v_solado_comp;
END;
