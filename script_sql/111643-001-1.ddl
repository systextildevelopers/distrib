CREATE TABLE tmrp_652 (
 OP_MP number(9) NOT NULL,
 nivel VARCHAR(1) DEFAULT '',
 grupo VARCHAR(6) DEFAULT '',
 subgrupo VARCHAR(4) DEFAULT '',
 item VARCHAR (7) DEFAULT '',
 OP_DESTINO number(9) NOT NULL,
 qtde number (10, 3) DEFAULT 0.000 NOT NULL,
 constraint pk_tmrp_652 primary key (nivel, grupo, subgrupo, item, OP_MP, OP_DESTINO)
);
