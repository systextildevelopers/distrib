create or replace PACKAGE ST_PCK_BAIXA_TITULO AS

    function exists_titulo( p_nr_duplicata IN varchar2,   p_parcela IN varchar2,  p_cgc9 IN number, 
                            p_cgc4 IN number,             p_cgc2 IN number,       p_tipo_titulo IN number) return boolean;
    
    function new_nr_titulo( p_cod_empresa IN number,   p_tipo_titulo IN number,
                            p_cgc9 IN number,          p_cgc4 IN number,
                            p_cgc2 IN number,          p_parcela IN varchar2,
                            p_des_erro OUT varchar2,   p_has_error OUT boolean) return number;

    function exists_integracao( p_tipos_titulo IN varchar2,     p_tipo_adiantamento IN number,
                                p_id_integracao OUT varchar2,   p_nr_adiantam OUT number,
                                p_retorno_ids OUT varchar2) return boolean;
                                
    function baixa_titulo(  p_nr_duplicata IN varchar2,  p_parcela IN varchar2,     p_cgc9 IN number, 
                            p_cgc4 IN number,            p_cgc2 IN number,          p_tipo_titulo IN number,
                            p_sequencia_pagto IN number, p_data_pgmt IN varchar2,   p_numero_documento IN number,
                            p_cod_historico IN number,   p_valor_pago IN number,    p_cod_portador IN number,
                            p_nr_conta_porta IN number,  p_transacao IN OUT number,     p_data_baixa IN varchar2,
                            p_num_contabil IN varchar2,  p_empresa IN number,       p_centro_custo IN number,
                            p_cod_contab_deb IN OUT number,  p_pago_adiantam IN number) return varchar2;

    function sequencia_baixa(  p_nr_duplicata IN varchar2,  p_parcela IN varchar2,     p_cgc9 IN number, 
                                p_cgc4 IN number,            p_cgc2 IN number,          p_tipo_titulo IN number) return number;

    function pagamento_titulo(  p_cgc9              IN number,      p_cgc4              IN number,      p_cgc2             IN number,      
                                p_tipo_titulo       IN number,      p_codigo_transacao  IN OUT number,      p_empresa          IN number,      
                                p_centro_custo      IN number,      p_emitente_titulo   IN varchar2,    p_valor_parcela    IN number,      
                                p_data_contrato     IN date,        p_data_vencimento   IN date,        p_cod_contab_deb   IN number,      
                                p_codigo_historico  IN number,      p_cod_portador      IN number,      p_num_adiantam     IN number,      
                                p_conta             IN number,      p_origem_debito     IN varchar2,    p_usuario_logado   IN varchar2,
                                p_parcela           IN varchar2,    p_baixar_titulo     IN boolean,     p_referencia       IN varchar2,
                                p_valor_estorno IN number,
                                p_lancamento_contabil IN OUT number
                            ) return varchar2;

    function contabiliza_estorno(  p_cgc9       IN number,      p_cgc4             IN number,      p_cgc2             IN number,      
                                p_tipo_titulo   IN number,      p_codigo_transacao IN OUT number,  p_empresa          IN number,      
                                p_centro_custo  IN number,      p_emitente_titulo  IN varchar2,    p_valor_parcela    IN number,      
                                p_data_contrato IN date,        p_cod_contab_deb   IN number,      p_codigo_historico IN number,      
                                p_cod_portador  IN number,      p_num_adiantam     IN number,
                                p_lancamento_contabil IN OUT number
                            ) return varchar2;

    function insere_rateio( x IN number,                            p_tipo_titulo IN number,
                            p_nr_duplicata IN number,               p_fornecedor_rateio IN varchar2, 
                            p_cod_transacao_rateio IN OUT number,   p_cod_contabil_rateio IN number,
                            p_centro_custo_rateio IN number,        p_valor_rateio IN number,
                            p_cod_empresa_rateio IN number,         p_emitente_titulo_rateio IN varchar2,
                            p_cod_portador_rateio IN number,        p_date_contrato_rateio IN varchar2,   
                            p_codigo_historico_rateio IN number,    p_retorno_nr_lanc IN OUT number,
                            p_cgc9_out IN OUT number,               p_cgc4_out IN OUT number,
                            p_cgc2_out IN OUT number) return varchar2;

    function atualiza_rateio( p_nr_lanc IN number,        p_nr_duplicata IN number,
                            p_tipo_titulo IN number,    p_parcela IN varchar2, 
                            p_cgc9_new IN number,       p_cgc4_new IN number,
                            p_cgc2_new IN number) return varchar2;              
END st_pck_baixa_titulo;
