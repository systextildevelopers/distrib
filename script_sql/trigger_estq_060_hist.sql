CREATE OR REPLACE TRIGGER "TRIGGER_ESTQ_060_HIST" 
BEFORE UPDATE of serie_nota, transacao_ent, seq_nota_fiscal, data_entrada,
		 codigo_embalagem, proforma, valor_entrada, valor_despesas,
		 qtde_pecas_emb, data_transacao, torcao, nota_fisc_ent,
		 seri_fisc_ent, sequ_fisc_ent, nome_prog, periodo_producao,
		 qualidade_fio, pre_romaneio, grupo_maquina, sub_maquina,
		 numero_maquina, cnpj_fornecedor9, cnpj_fornecedor4, cnpj_fornecedor2,
		 data_insercao, data_cardex, transacao_cardex, usuario_cardex,
		 valor_movto_cardex, valor_contabil_cardex, tabela_origem_cardex, ordem_agrupamento,
		 caixa_original, turno_original, codigo_pesador, divisao_producao,
		 ordem_producao, peso_adicional, arreada, restricao,
		 deposito_producao, transacao_producao, sequencia_plano, cod_empresa_sai,
		 sigla_inmetro, centro_custo_cardex, numero_caixa, turno,
		 codigo_deposito, lote, prodcai_nivel99, prodcai_grupo,
		 prodcai_subgrupo, prodcai_item, data_producao, peso_liquido,
		 peso_bruto, peso_embalagem, preco_medio, endereco_caixa,
		 numero_pedido, sequencia_pedido, nr_solicitacao, status_caixa,
		 nota_fiscal
or DELETE
or INSERT
on estq_060
for each row
declare
   ws_numero_caixa         estq_060.numero_caixa%type;
   ws_turno                estq_060.turno%type;
   ws_nivel                estq_060.prodcai_nivel99%type;
   ws_grupo                estq_060.prodcai_grupo%type;
   ws_sub                  estq_060.prodcai_subgrupo%type;
   ws_item                 estq_060.prodcai_item%type;
   ws_lote                 estq_060.lote%type;
   ws_deposito_old         estq_060.codigo_deposito%type;
   ws_deposito_atu         estq_060.codigo_deposito%type;
   ws_status_old           estq_060.status_caixa%type;
   ws_status_atu           estq_060.status_caixa%type;
   ws_peso_old             estq_060.peso_liquido%type;
   ws_peso_atu             estq_060.peso_liquido%type;
   ws_nota_old             number;
   ws_nota_atu             number;
   ws_pedido_old           number;
   ws_pedido_atu           number;
   ws_transacao_old        estq_060.transacao_ent%type;
   ws_transacao_atu        estq_060.transacao_ent%type;
   ws_atualiza_estq        estq_005.atualiza_estoque%type;
   ws_nota_ent_old         estq_060.nota_fisc_ent%type;
   ws_nota_ent_atu         estq_060.nota_fisc_ent%type;
   ws_tipo_ocorr           estq_060_hist.tipo_ocorr%type;
   ws_nome_programa        estq_060.nome_prog%type;
   ws_usuario_rede         estq_060_hist.usuario_rede%type;
   ws_maquina_rede         estq_060_hist.maquina_rede%type;
   ws_aplicacao            estq_060_hist.aplicacao%type;
   ws_aplicativo           varchar2(20);
   ws_sid                  number(9);
   ws_empresa              number(3);
   ws_usuario_systextil    varchar2(250);
   ws_locale_usuario       varchar2(5);
begin
   if UPDATING then
      ws_numero_caixa     := :old.numero_caixa;
      ws_turno            := :old.turno;
      ws_nivel            := :old.prodcai_nivel99;
      ws_grupo            := :old.prodcai_grupo;
      ws_sub              := :old.prodcai_subgrupo;
      ws_item             := :old.prodcai_item;
      ws_lote             := :old.lote;
      ws_deposito_atu     := :new.codigo_deposito;
      ws_deposito_old     := :old.codigo_deposito;
      ws_status_atu       := :new.status_caixa;
      ws_status_old       := :old.status_caixa;
      ws_peso_atu         := :new.peso_liquido;
      ws_peso_old         := :old.peso_liquido;
      ws_nota_atu         := :new.nota_fiscal;
      ws_nota_old         := :old.nota_fiscal;
      ws_pedido_atu       := :new.numero_pedido;
      ws_pedido_old       := :old.numero_pedido;
      ws_transacao_atu    := :new.transacao_ent;
      ws_transacao_old    := :old.transacao_ent;
      ws_nota_ent_atu     := :new.nota_fisc_ent;
      ws_nota_ent_old     := :old.nota_fisc_ent;
      ws_tipo_ocorr       := 'A';
      ws_nome_programa    := :new.nome_prog;
      select atualiza_estoque
      into   ws_atualiza_estq
      from estq_005 where codigo_transacao = ws_transacao_atu;
   elsif DELETING then
         ws_numero_caixa     := :old.numero_caixa;
         ws_turno            := :old.turno;
         ws_nivel            := :old.prodcai_nivel99;
         ws_grupo            := :old.prodcai_grupo;
         ws_sub              := :old.prodcai_subgrupo;
         ws_item             := :old.prodcai_item;
         ws_lote             := :old.lote;
         ws_deposito_atu     := 0;
         ws_deposito_old     := :old.codigo_deposito;
         ws_status_atu       := 0;
         ws_status_old       := :old.status_caixa;
         ws_peso_atu         := 0.0;
         ws_peso_old         := :old.peso_liquido;
         ws_nota_atu         := 0.0;
         ws_nota_old         := :old.nota_fiscal;
         ws_pedido_atu       := 0;
         ws_pedido_old       := :old.numero_pedido;
         ws_transacao_atu    := 0;
         ws_transacao_old    := :old.transacao_ent;
         ws_nota_ent_atu     := 0;
         ws_nota_ent_old     := :old.nota_fisc_ent;
         ws_tipo_ocorr       := 'D';
         ws_nome_programa    := :old.nome_prog;
         select atualiza_estoque
         into   ws_atualiza_estq
         from estq_005 where codigo_transacao = ws_transacao_old;
      elsif INSERTING then
            ws_numero_caixa     := :new.numero_caixa;
            ws_turno            := :new.turno;
            ws_nivel            := :new.prodcai_nivel99;
            ws_grupo            := :new.prodcai_grupo;
            ws_sub              := :new.prodcai_subgrupo;
            ws_item             := :new.prodcai_item;
            ws_lote             := :new.lote;
            ws_deposito_atu     := :new.codigo_deposito;
            ws_deposito_old     := 0;
            ws_status_atu       := :new.status_caixa;
            ws_status_old       := 0;
            ws_peso_atu         := :new.peso_liquido;
            ws_peso_old         := 0;
            ws_nota_atu         := :new.nota_fiscal;
            ws_nota_old         := 0;
            ws_pedido_atu       := :new.numero_pedido;
            ws_pedido_old       := 0;
            ws_transacao_atu    := :new.transacao_ent;
            ws_transacao_old    := 0;
            ws_nota_ent_atu     := :new.nota_fisc_ent;
            ws_nota_ent_old     := 0;
            ws_tipo_ocorr       := 'I';
            ws_nome_programa    := :new.nome_prog;
            select atualiza_estoque
            into   ws_atualiza_estq
            from estq_005 where codigo_transacao = ws_transacao_atu;
   end if;

      inter_pr_dados_usuario(ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                             ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   
      ws_nome_programa := inter_fn_nome_programa(ws_sid); 

      INSERT INTO estq_060_hist
         (numero_caixa,     turno,            nivel,
          grupo,            sub,              item,
          lote,             deposito_old,     deposito_atu,
          status_old,       status_atu,       peso_old,
          peso_atu,         nota_old,         nota_atu,
          pedido_old,       pedido_atu,       transacao_old,
          transacao_atu,    atualiza_estq,    nota_ent_old,
          nota_ent_atu,     data_ocorr,       tipo_ocorr,
          nome_programa,    usuario_rede,     maquina_rede,
          aplicacao)
      VALUES
         (ws_numero_caixa,  ws_turno,         ws_nivel,
          ws_grupo,         ws_sub,           ws_item,
          ws_lote,          ws_deposito_old,  ws_deposito_atu,
          ws_status_old,    ws_status_atu,    ws_peso_old,
          ws_peso_atu,      ws_nota_old,      ws_nota_atu,
          ws_pedido_old,    ws_pedido_atu,    ws_transacao_old,
          ws_transacao_atu, ws_atualiza_estq, ws_nota_ent_old,
          ws_nota_ent_atu,  sysdate,          ws_tipo_ocorr,
          ws_nome_programa, ws_usuario_rede,  ws_maquina_rede,
          ws_aplicacao);

   -- Comentado na SS 52209, pois n�o estava sendo
   -- possivel utilizar o controle do programa que estava inserindo.
   /*if INSERTING or UPDATING then
      :new.nome_prog := '';
   end if;*/

end trigger_estq_060_hist;
-- ALTER TRIGGER "TRIGGER_ESTQ_060_HIST" ENABLE
 

/

exec inter_pr_recompile;

