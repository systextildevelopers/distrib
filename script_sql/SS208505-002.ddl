create table estq_376
(
    cod_empresa          NUMBER(4) default 0 not null,
    nivel                VARCHAR2(1) default ' ',
    grupo                VARCHAR2(5) default ' ',
    subgrupo             VARCHAR2(3) default ' ',
    item                 VARCHAR2(6) default ' ',
    email                VARCHAR(4000) default ' '
);
