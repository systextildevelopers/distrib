
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_185_PCPC_341" 
before update
       of custo_minuto_previsto
on basi_185
for each row

declare

   TYPE nivelEstrutura IS TABLE OF mqop_050.nivel_estrutura%TYPE;
   v_nivel_estrutura nivelEstrutura;

   TYPE grupoEstrutura IS TABLE OF mqop_050.grupo_estrutura%TYPE;
   v_grupo_estrutura grupoEstrutura;

   TYPE subgruEstrutura IS TABLE OF mqop_050.subgru_estrutura%TYPE;
   v_subgru_estrutura subgruEstrutura;

   TYPE itemEstrutura IS TABLE OF mqop_050.item_estrutura%TYPE;
   v_item_estrutura itemEstrutura;

   TYPE numeroAlternati IS TABLE OF mqop_050.numero_alternati%TYPE;
   v_numero_alternati numeroAlternati;

   TYPE numeroRoteiro IS TABLE OF mqop_050.numero_roteiro%TYPE;
   v_numero_roteiro numeroRoteiro;

   TYPE seqOperacao IS TABLE OF mqop_050.seq_operacao%TYPE;
   v_seq_operacao seqOperacao;
   
   v_sid           number(9);
   v_osuser        varchar2(20);
   v_nome_usuario  varchar2(20);

   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_empresa                number(3);
   ws_locale_usuario         varchar2(5);
   v_origem_valor_operacao   number(1);
   
begin
	inter_pr_dados_usuario (v_osuser,        ws_maquina_rede,   ws_aplicativo,     v_sid,
							v_nome_usuario,  ws_empresa,        ws_locale_usuario);
	begin
		select origem_valor_operacao
		INTO v_origem_valor_operacao
		from fatu_501
		where codigo_empresa = ws_empresa;
	exception when others then
		v_origem_valor_operacao := 0;
	end;

   if :old.custo_minuto_previsto <> :new.custo_minuto_previsto
   then

      begin
         select mqop_050.nivel_estrutura,  mqop_050.grupo_estrutura,
                mqop_050.subgru_estrutura, mqop_050.item_estrutura,
                mqop_050.numero_alternati, mqop_050.numero_roteiro,
                mqop_050.seq_operacao
         BULK COLLECT INTO
                v_nivel_estrutura,         v_grupo_estrutura,
                v_subgru_estrutura,        v_item_estrutura,
                v_numero_alternati,        v_numero_roteiro,
                v_seq_operacao
         from mqop_050
         where mqop_050.centro_custo = :old.centro_custo;
      end;

      if sql%found and v_origem_valor_operacao = 0
      then
		
         FOR i IN v_nivel_estrutura.FIRST .. v_nivel_estrutura.LAST
         LOOP

            update pcpc_341
               set pcpc_341.valor_minuto = :new.custo_minuto_previsto
             where pcpc_341.nivel_rot      = v_nivel_estrutura(i)
               and pcpc_341.referencia_rot = v_grupo_estrutura(i)
               and pcpc_341.subgrupo_rot   = v_subgru_estrutura(i)
               and pcpc_341.item_rot       = v_item_estrutura(i)
               and pcpc_341.alt_rot        = v_numero_alternati(i)
               and pcpc_341.rot_rot        = v_numero_roteiro(i)
               and pcpc_341.seq_operacao   = v_seq_operacao(i);

         END LOOP;
      end if;
   end if;

end inter_tr_basi_185_pcpc_341;

-- ALTER TRIGGER "INTER_TR_BASI_185_PCPC_341" ENABLE
 

/

exec inter_pr_recompile;

