
  CREATE OR REPLACE PROCEDURE "INTER_PR_GERA_CTB_SPED_0150" (p_cod_empresa    IN NUMBER,
                                                        p_exercicio      IN NUMBER,
                                                        p_tipo_exporta   IN NUMBER,
                                                        p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar a tabela sped_cta_O150 - Participantes
   --
   -- Historicos
   --
   -- Data    Autor    Observacoes
   --

   cursor ctbi250 IS
      select DISTINCT
             sped_ctb_i250.cod_participante9, sped_ctb_i250.cod_participante4,
             sped_ctb_i250.cod_participante2, sped_ctb_i250.cliente_fornecedor_part
      from   sped_ctb_i250
      where sped_ctb_i250.cod_participante9 > 0;

   v_erro                  EXCEPTION;

   v_cod_relacion_sped_ctb pedi_010.cod_relacion_sped_ctb%type;
   v_cod_relacion_sped_tmp pedi_010.cod_relacion_sped_ctb%type;
   v_nome_participante     pedi_010.nome_cliente%type;
   v_inscr_estadual        pedi_010.insc_est_cliente%type;
   v_cod_cidade_syst       pedi_010.cod_cidade%type;
   v_num_suframa           pedi_010.nr_suframa_cli%type;
   v_inscr_municipal       supr_010.insc_municipal%type;
   v_periodo_ini_rel_ctb   pedi_010.data_cad_cliente%type;
   v_periodo_fim_rel_ctb   pedi_010.data_exc_cliente%type;

   v_sig_estado            basi_160.estado%type;
   v_nome_cidade           basi_160.cidade%type;
   v_codigo_cidade_ibge    number;
   v_codigo_fiscal_uf      basi_167.codigo_fiscal_uf%type;
   v_cod_pais              basi_165.codigo_fiscal%type;

   v_nr_registro           number;
   w_cont_participante     number;

begin

   p_des_erro             := NULL;

   for reg_ctbi250 in ctbi250
   loop

      if reg_ctbi250.cliente_fornecedor_part = 1   -- clientes
      then
          v_inscr_municipal := 0;

         begin
            select cod_relacion_sped_ctb,     nome_cliente,
                   insc_est_cliente,          cod_cidade,
                   nr_suframa_cli,            data_cad_cliente,
                   data_exc_cliente
            into   v_cod_relacion_sped_tmp,   v_nome_participante,
                   v_inscr_estadual,          v_cod_cidade_syst,
                   v_num_suframa,             v_periodo_ini_rel_ctb,
                   v_periodo_fim_rel_ctb
            from pedi_010 p
            where cgc_9 = reg_ctbi250.cod_participante9
              and cgc_4 = reg_ctbi250.cod_participante4
              and cgc_2 = reg_ctbi250.cod_participante2;

            begin
               select cod_relacion_sped_ctb
               into   v_cod_relacion_sped_ctb
               from cont_013
               where cliente_fornecedor = 1
                 and cnpj9              = reg_ctbi250.cod_participante9
                 and cnpj4              = reg_ctbi250.cod_participante4
                 and cnpj2              = reg_ctbi250.cod_participante2
                 and codigo_empresa     = p_cod_empresa;

            exception
               when others then

                  select count(*)
                  into   v_nr_registro
                  from cont_013
                  where cliente_fornecedor = 1
                    and cnpj9              = reg_ctbi250.cod_participante9
                    and cnpj4              = reg_ctbi250.cod_participante4
                    and cnpj2              = reg_ctbi250.cod_participante2;

                  if v_nr_registro > 0
                  then
                     v_cod_relacion_sped_ctb := 0;
                  else
                     v_cod_relacion_sped_ctb := v_cod_relacion_sped_tmp;
                  end if;
            end;

         exception
            when others then
               v_cod_relacion_sped_ctb := null;

               inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura do cliente  ' ||
                                         to_char(reg_ctbi250.cod_participante9, '900000000') || '/' ||
                                         to_char(reg_ctbi250.cod_participante4, '0000')      || '-' ||
                                         to_char(reg_ctbi250.cod_participante2, '00'));

               commit;
         end;
      else   -- fornecedores

         v_num_suframa := 0;

         begin
            select cod_relacion_sped_ctb,     nome_fornecedor,
                   inscr_est_forne,           cod_cidade,
                   insc_municipal,            data_cadastro,
                   data_cancelamento
            into   v_cod_relacion_sped_tmp,   v_nome_participante,
                   v_inscr_estadual,          v_cod_cidade_syst,
                   v_inscr_municipal,         v_periodo_ini_rel_ctb,
                   v_periodo_fim_rel_ctb
            from supr_010
            where fornecedor9 = reg_ctbi250.cod_participante9
              and fornecedor4 = reg_ctbi250.cod_participante4
              and fornecedor2 = reg_ctbi250.cod_participante2;

            begin
               select cod_relacion_sped_ctb
               into   v_cod_relacion_sped_ctb
               from cont_013
               where cliente_fornecedor = 2
                 and cnpj9              = reg_ctbi250.cod_participante9
                 and cnpj4              = reg_ctbi250.cod_participante4
                 and cnpj2              = reg_ctbi250.cod_participante2
                 and codigo_empresa     = p_cod_empresa;

            exception
               when others then

                  select count(*)
                  into   v_nr_registro
                  from cont_013
                  where cliente_fornecedor = 2
                    and cnpj9              = reg_ctbi250.cod_participante9
                    and cnpj4              = reg_ctbi250.cod_participante4
                    and cnpj2              = reg_ctbi250.cod_participante2;

                  if v_nr_registro > 0
                  then
                     v_cod_relacion_sped_ctb := 0;
                  else
                     v_cod_relacion_sped_ctb := v_cod_relacion_sped_tmp;
                  end if;
            end;


         exception
            when others then
               v_cod_relacion_sped_ctb := null;

               inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura do cliente  ' ||
                                         to_char(reg_ctbi250.cod_participante9, '900000000') || '/' ||
                                         to_char(reg_ctbi250.cod_participante4, '0000')      || '-' ||
                                         to_char(reg_ctbi250.cod_participante2, '00'));

               commit;
         end;
      end if;

      /* incluir somente um registro de cada participante */
      begin
         select count(*)
         into w_cont_participante
         from sped_0150
         where cod_empresa = p_cod_empresa
           and sped_0150.tip_contabil_fiscal = 'C'
           and num_cnpj9 = reg_ctbi250.cod_participante9
           and num_cnpj4 = reg_ctbi250.cod_participante4
           and num_cnpj2 = reg_ctbi250.cod_participante2;
      end;

      if p_tipo_exporta = 1 and v_cod_relacion_sped_ctb > 0 and w_cont_participante = 0
      then

         -- le a cidade da empresa informada pelo usuario
         begin
            select basi_160.estado,          basi_160.cidade,
                   basi_160.cod_cidade,      basi_160.codigo_fiscal,
                   basi_165.codigo_fiscal,   basi_167.codigo_fiscal_uf
            into   v_sig_estado,             v_nome_cidade,
                   v_cod_cidade_syst,        v_codigo_cidade_ibge,
                   v_cod_pais,               v_codigo_fiscal_uf
            from   basi_160, basi_167, basi_165
            where  basi_160.cod_cidade  = v_cod_cidade_syst
            and    basi_160.estado      = basi_167.estado
            and    basi_160.codigo_pais = basi_167.codigo_pais
            and    basi_160.codigo_pais = basi_165.codigo_pais;


         exception
            when others then
               v_sig_estado         := NULL;
               v_nome_cidade        := NULL;
               v_cod_cidade_syst    := NULL;
               v_codigo_cidade_ibge := NULL;
               v_cod_pais           := NULL;
               v_codigo_fiscal_uf   := NULL;

               inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura da cidade da empresa ' || p_cod_empresa || Chr(10) ||
                                          '   Cidade: ' || v_cod_cidade_syst);

            commit;
         end;

         begin

            v_codigo_cidade_ibge :=  to_number(ltrim(to_char(v_codigo_fiscal_uf, '00')) || ltrim(to_char(v_codigo_cidade_ibge, '00000')));

            insert into sped_0150
              (tip_contabil_fiscal,
               cod_empresa,                     exercicio,
               num_cnpj9,                       num_cnpj4,
               num_cnpj2,                       tip_participante,
               nom_participante,                cod_pais_particip,
               sig_estado_particip,             num_inscri_estadual,
               num_inscri_municipal,            cod_cidade_ibge,
               cod_cidade_syst,                 nom_cidade_syst,
               num_suframa,                     cod_relacion_sped_ctb,
               periodo_ini_rel_ctb,             periodo_fim_rel_ctb)
            values (
               'C',
               p_cod_empresa,                   p_exercicio,
               reg_ctbi250.cod_participante9,   reg_ctbi250.cod_participante4,
               reg_ctbi250.cod_participante2,   reg_ctbi250.cliente_fornecedor_part,
               v_nome_participante,             v_cod_pais,
               v_sig_estado,                    v_inscr_estadual,
               v_inscr_municipal,               v_codigo_cidade_ibge,
               v_cod_cidade_syst,               v_nome_cidade,
               v_num_suframa,                   ltrim(to_char(v_cod_relacion_sped_ctb, '00')),
               v_periodo_ini_rel_ctb,           v_periodo_fim_rel_ctb);

         exception
            when others then
               p_des_erro := 'Erro na inclusao da tabela sped_ctb_0150 ' || Chr(10) || SQLERRM;
               RAISE V_ERRO;
         end;
      end if;
   end loop;
   commit;

exception
   when V_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_gera_ctb_sped_O150 ' || Chr(10) || p_des_erro;

   when others then
      p_des_erro := 'Outros erros na procedure inter_pr_gera_ctb_sped_0150 ' || Chr(10) || SQLERRM;
end inter_pr_gera_ctb_sped_0150;
 

/

exec inter_pr_recompile;

