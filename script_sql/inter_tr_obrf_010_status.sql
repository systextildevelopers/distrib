
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_010_STATUS" 
before update of cod_status, nr_protocolo, numero_danf_nfe

on obrf_010
for each row
begin

   if :old.cod_status = '100' and :new.cod_status = '999'
   then
      :new.cod_status      := '100';
      :new.nr_protocolo    := :old.nr_protocolo;
      :new.numero_danf_nfe := :old.numero_danf_nfe;

   end if;
   
   if :old.cod_status = '501' and :new.cod_status = '999'
   then
      :new.cod_status      := '100';
      :new.nr_protocolo    := :old.nr_protocolo;
      :new.numero_danf_nfe := :old.numero_danf_nfe;

   end if;
   
   if :old.cod_status = '218' and :new.cod_status = '999'
   then
      :new.cod_status      := '101';
      :new.nr_protocolo    := :old.nr_protocolo;
      :new.numero_danf_nfe := :old.numero_danf_nfe;
      :new.situacao_entrada := 2;
      :new.msg_status := 'Cancelamento de NF-e homologado';
   end if;

end inter_tr_obrf_010_status;

-- ALTER TRIGGER "INTER_TR_OBRF_010_STATUS" ENABLE
 

/

exec inter_pr_recompile;

