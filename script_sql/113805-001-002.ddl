INSERT INTO hdoc_035
( codigo_programa, programa_menu, 
 item_menu_def,   descricao)
VALUES
( 'obrf_f274',     0,
 1,        		   'XMLs Importados');

INSERT INTO hdoc_033
( usu_prg_cdusu, usu_prg_empr_usu, 
 programa,      nome_menu, 
 item_menu,     ordem_menu, 
 incluir,       modificar, 
 excluir,       procurar)
VALUES
( 'INTERSYS',    1, 
 'obrf_f274',   'obrf_menu', 
 1,             1, 
 'S',           'S', 
 'S',           'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'XMLs Importados'
 WHERE hdoc_036.codigo_programa = 'obrf_f274'
   AND hdoc_036.locale          = 'es_ES';

commit;
