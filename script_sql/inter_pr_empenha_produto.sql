  CREATE OR REPLACE PROCEDURE "INTER_PR_EMPENHA_PRODUTO" 
   (p_nivel_estrutura    in number,   p_grupo_estrutura in varchar2,
    p_subgrupo_estrutura in varchar2, p_item_estrutura  in varchar2,
    p_lote_empenhado     in number,   p_codigo_deposito in number,
    p_qtde_empenhada     in number)
is

begin
   update estq_040
   set qtde_empenhada = greatest(0.0, qtde_empenhada + p_qtde_empenhada)
   where cditem_nivel99  = p_nivel_estrutura
   and   cditem_grupo    = p_grupo_estrutura
   and   cditem_subgrupo = p_subgrupo_estrutura
   and   cditem_item     = p_item_estrutura
   and   lote_acomp      = p_lote_empenhado
   and   deposito        = p_codigo_deposito;
            
   if SQL%notfound
      and p_qtde_empenhada > 0.0
   then

      insert into estq_040
        (cditem_nivel99,       cditem_grupo, 
         cditem_subgrupo,      cditem_item, 
         lote_acomp,           deposito,     
         qtde_empenhada)
      values
        (p_nivel_estrutura,    p_grupo_estrutura,
         p_subgrupo_estrutura, p_item_estrutura,
         p_lote_empenhado,     p_codigo_deposito, 
         p_qtde_empenhada);
   end if;
end inter_pr_empenha_produto;

/

exec inter_pr_recompile;

