alter table pedi_085 add prioridade_sugestao number(1) default 9;
comment on column pedi_085.prioridade_sugestao is 'Prioridade de sugestão a ser analisada pela fatu_e780 - Sugestão de Faturamento de Peças';

exec inter_pr_recompile;
