alter table rcnb_940
add (faixa_etaria_custos number(3),
     qtde_marcacoes number(4,1));

comment on column rcnb_940.faixa_etaria_custos is 'Faixa estaria de tamanhos para custos';
comment on column rcnb_940.qtde_marcacoes is 'quantidade de marcações do tamanho do produto';

execute inter_pr_recompile; 
