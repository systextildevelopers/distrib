alter table estq_060 add lote_old number(6);

comment on column estq_060.lote_old is 'Numero do lote que estava no rolo antes da alteracao';

exec inter_pr_recompile;
