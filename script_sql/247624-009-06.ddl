create table ESTQ_426_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(250) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(250) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  codigo_empresa_OLD       NUMBER(3) ,  
  codigo_empresa_NEW       NUMBER(3) , 
  deposito_entrada_OLD     NUMBER(3),  
  deposito_entrada_NEW     NUMBER(3), 
  transacao_entrada_OLD    NUMBER(3),  
  transacao_entrada_NEW    NUMBER(3), 
  realizar_conferencia_OLD NUMBER(1),  
  realizar_conferencia_NEW NUMBER(1), 
  preview_OLD              NUMBER(1) default 0, 
  preview_NEW              NUMBER(1) default 0 
) ;

-- execute
