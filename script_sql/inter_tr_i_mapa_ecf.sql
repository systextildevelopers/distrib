
  CREATE OR REPLACE TRIGGER "INTER_TR_I_MAPA_ECF" 
before insert on i_mapa_ecf
for each row

declare
  v_id_registro number;
begin
   select SEQ_IMPORTACAO.nextval into v_id_registro from dual;
   :new.id_registro     := v_id_registro;
   :new.data_exportacao := sysdate;
end;
-- ALTER TRIGGER "INTER_TR_I_MAPA_ECF" ENABLE
 

/

exec inter_pr_recompile;

