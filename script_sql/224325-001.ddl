alter table fatu_504 add mov_acert_comp_estagios_ant varchar2(1) default 'S';

comment on column fatu_504.mov_acert_comp_estagios_ant is 'Parâmetro de empresa que indique se devemos ou não fazer a movimentação de acertos de componentes dos estágios anteriores ao corte no momento da baixa da ordem por enfesto.';
