create or replace view inter_vi_pcpc_040_mqop_f051 (
    nivel_estrutura,
    grupo_estrutura,
    subgru_estrutura,
    item_estrutura,
    numero_alternati,
    numero_roteiro,
    seq_operacao,
    codigo_operacao,
    codigo_estagio,
    centro_custo,
    sequencia_estagio,
    estagio_anterior,
    estagio_depende,
    ccusto_homem,
    cod_estagio_agrupador,
	
	ordem_producao,
	periodo_producao,
	ordem_confeccao,
	estagios_anteriores
) as
    select 
            pcpc_040.proconf_nivel99        as nivel_estrutura,
            pcpc_040.proconf_grupo          as grupo_estrutura,
            pcpc_040.proconf_subgrupo       as subgru_estrutura,
            pcpc_040.proconf_item           as item_estrutura,
            pcpc_020.alternativa_peca       as numero_alternati,
            pcpc_020.roteiro_peca           as numero_roteiro,
            pcpc_040.seq_operacao,
            cast(null as number(5))         as codigo_operacao,
            pcpc_040.codigo_estagio,
            cast(null as number(6))         as centro_custo,
            pcpc_040.sequencia_estagio,
            pcpc_040.estagio_anterior,
            pcpc_040.estagio_depende,
            cast(null as number(6))         as ccusto_homem,
            pcpc_040.cod_estagio_agrupador,

            pcpc_040.ordem_producao,
            pcpc_040.periodo_producao,
            pcpc_040.ordem_confeccao,
			
			cast(null as varchar2(20))      as estagios_anteriores

            --##############################################################################################################################################################
            --####                                                              ATENCAO!!!!                                                                             #### 
            --####                                                                                                                                                      #### 
            --####  SE INCLUIR OU ALTERAR OU REMOVER ALGUM CAMPO NESTA VIEW, DEVE FAZER O MESMO NA VIEW inter_vi_mqop_050_mqop_f051 PARA AMBAS TEREM OS MESMOS CAMPOS   ####
            --##############################################################################################################################################################

    from pcpc_040, pcpc_020
    where pcpc_020.ordem_producao   = pcpc_040.ordem_producao

;
