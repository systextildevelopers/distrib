CREATE TABLE BASI_760 (
          id_carac number(3),
          desc_carac varchar2(50),
          constraint basi_760_pk PRIMARY KEY(id_carac));

CREATE TABLE BASI_770 (
          nivel varchar2(1),
          grupo varchar2(5),
		  sub_grupo varchar2(3),
		  item varchar2(6),
		  id_carac number(3),
          constraint basi_770_pk PRIMARY KEY(nivel, grupo, sub_grupo, item, id_carac),
		  constraint fk_basi_770_basi_760 foreign key (id_carac) references basi_760 (id_carac));

exec inter_pr_recompile;

/
