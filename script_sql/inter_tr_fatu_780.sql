
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_780" 
   before insert or delete or
   update of pedido, sequencia, nivel, grupo,
	subgrupo, item, deposito, lote,
	nr_solicitacao, tipo_registro, qtde_pedida, qtde_sugerida,
	qtde_disponivel, qtde_afaturar, nr_sugestao, nome_programa,
	ref_original, executa_trigger
   on fatu_780
   for each row
declare
   v_saldo_disponivel        estq_040.qtde_estoque_atu  %type;
   v_qtde_estoque_atual      estq_040.qtde_estoque_atu  %type;
   v_qtde_empenhada          estq_040.qtde_empenhada    %type;
   v_qtde_sugerida_estq_040  estq_040.qtde_sugerida     %type;
   v_pronta_entrega          basi_205.pronta_entrega    %type;
   v_saldo_sugerido_item     pedi_110.qtde_sugerida     %type;
   v_qtde_sugerida_pedi_110  pedi_110.qtde_sugerida     %type;
   v_qtde_sugerida_fatu_780  fatu_780.qtde_sugerida     %type;
   v_pedido_venda            fatu_780.pedido            %type;
   v_seq_item_pedido         fatu_780.sequencia         %type;
   v_nivel_produto           fatu_780.nivel             %type;
   v_grupo_produto           fatu_780.grupo             %type;
   v_subgrupo_produto        fatu_780.subgrupo          %type;
   v_item_produto            fatu_780.item              %type;
   v_deposito                fatu_780.deposito          %type;
   v_num_lote                fatu_780.lote              %type;
   v_nr_sugestao             fatu_780.nr_sugestao       %type;
   v_nome_programa           fatu_780.nome_programa     %type;
   v_ref_original            fatu_780.grupo             %type;
   v_grupo_leitura           fatu_780.grupo             %type;
   v_executa_trigger         number(1);

begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      if inserting or updating
      then
        -- N�o podemos ter quantidade coletada negativa
         if :new.qtde_disponivel < 0.000
         then
            raise_application_error(-20000,'ATEN��O! Qtde coletada n�o pode ficar negativa');
         end if;

         -- N�o podemos coletar mais do que foi sugerido
         if :new.qtde_disponivel > :new.qtde_sugerida
         then
            raise_application_error(-20000,'ATEN��O! Qtde coletada n�o pode ser maior que qtde sugerida');
         end if;

         -- N�o podemos sugerir mais do que foi pedido
         if :new.qtde_sugerida > :new.qtde_pedida
         then
            raise_application_error(-20000,'ATEN��O! Qtde sugerida n�o pode ser maior que qtde pedida');
         end if;
      end if; -- if inserting or updating

      -- Quando estamos adicionando registro ou atualizando
      -- qtde sugerida na tabela de sugest�o
      if inserting or (updating and :new.qtde_sugerida > :old.qtde_sugerida)
      then
         -- Quando estamos adicionado devemos atualiza a nova qtde sugerida
         -- Sen�o atualizamos a qtde sugerida calculando qtde nova - qtde velha.
         if inserting
         then v_qtde_sugerida_fatu_780 := :new.qtde_sugerida;
         else v_qtde_sugerida_fatu_780 := :new.qtde_sugerida - :old.qtde_sugerida;
         end if;

           -- Verifica se a qtde sugerida foi atualizada
         if v_qtde_sugerida_fatu_780 > 0.000
         then
            v_saldo_disponivel := 0.000;

            -- ***** le referencia original ******
            select basi_030.ref_original
            into   v_ref_original
            from basi_030
            where basi_030.nivel_estrutura = :new.nivel
              and basi_030.referencia      = :new.grupo;

            if v_ref_original is not null and v_ref_original <> '00000'
               then
                  v_grupo_leitura := v_ref_original;
               else
                  v_grupo_leitura := :new.grupo;
            end if;

            -- Lendo a quantidade disponivel em estoque para
            -- saber se a qtde sugerida pode ser atendida
            begin
               select qtde_estoque_atu,       qtde_empenhada,
                      nvl(qtde_sugerida, 0)
               into   v_qtde_estoque_atual,   v_qtde_empenhada,
                      v_qtde_sugerida_estq_040
               from estq_040
               where cditem_nivel99   = :new.nivel
                 and cditem_grupo     = v_grupo_leitura
                 and cditem_subgrupo  = :new.subgrupo
                 and cditem_item      = :new.item
                 and lote_acomp       = :new.lote
                 and deposito         = :new.deposito;
               exception
                  when NO_DATA_FOUND
                  then
                     raise_application_error(-20000,'ATEN��O! N�o existe registro de estoques para o produto/deposito');
            end;

            -- Verificando o tipo de deposito Pronta entrega ou Programado
            select pronta_entrega into v_pronta_entrega from basi_205
            where codigo_deposito = :new.deposito;

            if v_pronta_entrega = 1 -- Pronta entrega
            then v_saldo_disponivel := v_qtde_estoque_atual ;
            else v_saldo_disponivel := v_qtde_estoque_atual - v_qtde_sugerida_estq_040;
            end if; -- v_pronta_entrega = 1 -- Pronta entrega

            -- Verifica se o saldo atende a qtde a ser sugerida
            if v_saldo_disponivel < v_qtde_sugerida_fatu_780
            then
               raise_application_error(-20000,'ATEN��O! N�o existe estoque para fazer sugest�o de faturamento. Prod.: ' || :new.nivel || '.' || :new.grupo || '.' || :new.subgrupo  || '.' || :new.item || ' Dep.' || :new.deposito  );
            end if;
			
            if v_saldo_disponivel >= v_qtde_sugerida_fatu_780
            then
               -- Mandando as informa��es para a procedure que atualiza a qtde sugerida
               -- do pedido de venda e estoques
               inter_pr_sugestao_fatur (:new.pedido,              :new.sequencia,
                                        :new.nivel,               v_grupo_leitura,
                                        :new.subgrupo,            :new.item,
                                        :new.deposito,            :new.lote,
                                        v_qtde_sugerida_fatu_780, :new.nr_sugestao,
                                        :new.nome_programa);

            end if; -- if v_saldo_disponivel >= v_qtde_sugerida_fatu_780
         end if;    -- if v_qtde_sugerida_fatu_780 > 0.000
      end if;       -- if inserting or (updating and :new.qtde_sugerida > :old.qtde_sugerida)

      -- Quando estamos diminuindo a qtde sugerida ou estamos eliminando
      -- registro de sugest�o de faturamento
      if deleting or (updating and :new.qtde_sugerida < :old.qtde_sugerida and :new.nome_programa <> 'inte_p011' )-- Troca de itens
      then
         -- N�o deixa executar opera��o se j� existir itens j� coletados
         if :old.qtde_disponivel > 0.000 and (deleting or :new.flag_controle_wms = 0)   --S� se for fora do WMS
         then
            raise_application_error(-20000,'ATEN��O! Item do pedido j� coletado. N�o pode eliminar sugest�o');
         end if;

         v_saldo_sugerido_item     := 0.000;
         v_qtde_sugerida_pedi_110  := 0.000;

         -- Quando estamos eliminado devemos atualiza a qtde sugerida
         -- com base nas informa��es existentes na qtde antiga
         -- Sen�o devemos calcular o saldo para desfazer a sugest�o
         -- Tamb�m s�o atualizados outras informa��es para atualiza��o
         if deleting
         then
            v_qtde_sugerida_fatu_780  := :old.qtde_sugerida;
            v_pedido_venda            := :old.pedido;
            v_seq_item_pedido         := :old.sequencia;
            v_nivel_produto           := :old.nivel;
            v_grupo_produto           := :old.grupo;
            v_subgrupo_produto        := :old.subgrupo;
            v_item_produto            := :old.item;
            v_deposito                := :old.deposito;
            v_num_lote                := :old.lote;
            v_nr_sugestao             := 0;
            v_nome_programa           := 'EXCLUSAO';
         else
            v_qtde_sugerida_fatu_780  := :old.qtde_sugerida  - :new.qtde_sugerida;
            v_pedido_venda            := :new.pedido;
            v_seq_item_pedido         := :new.sequencia;
            v_nivel_produto           := :new.nivel;
            v_grupo_produto           := :new.grupo;
            v_subgrupo_produto        := :new.subgrupo;
            v_item_produto            := :new.item;
            v_deposito                := :new.deposito;
            v_num_lote                := :new.lote;
            v_nr_sugestao             := :new.nr_sugestao;
            v_nome_programa           := :new.nome_programa;
         end if;

         -- Verifica se a qtde sugerida do pedido vai ficar zerada depois
         -- da atualiza��o
         begin
            select qtde_sugerida
            into   v_qtde_sugerida_pedi_110
            from pedi_110
            where pedido_venda    = v_pedido_venda
              and seq_item_pedido = v_seq_item_pedido;
            exception
               when NO_DATA_FOUND
               then
                  raise_application_error(-20000,'ATEN��O! N�o existe registro de itens de pedido de venda');
         end;

         -- Calculando o saldo para n�o deixar ficar uma qtde sugerida
         -- negativa depois de desfazer a sugest�o
         v_saldo_sugerido_item := v_qtde_sugerida_pedi_110 - v_qtde_sugerida_fatu_780;

         -- Verifica se estamos tentando cancelar mais quantidade do que foi
         -- sugerida para o item do pedido
         if v_saldo_sugerido_item < 0.000
         then
            raise_application_error(-20000,'ATEN��O! Qtde sugerida do item do pedido ficar� negativa. N�o ser� feito a atualizacao');
         end if;

         -- Quando ficar saldo depois da atualiza��o ou saldo = 0.00
         if v_saldo_sugerido_item >= 0.000
         then
            -- Mandando as informa��es para a procedure que atualiza a qtde sugerida
            -- do pedido de venda e estoques
            -- Mandando a qtde negativa para abater do valor

            v_qtde_sugerida_fatu_780  := v_qtde_sugerida_fatu_780 * -1;

            -- ***** le referencia original ******
            select basi_030.ref_original
            into   v_ref_original
            from basi_030
            where basi_030.nivel_estrutura = v_nivel_produto
              and basi_030.referencia      = v_grupo_produto;

            if v_ref_original is not null and v_ref_original <> '00000'
               then
                  v_grupo_leitura := v_ref_original;
               else
                  v_grupo_leitura := v_grupo_produto;
            end if;


            inter_pr_sugestao_fatur (v_pedido_venda,             v_seq_item_pedido,
                                     v_nivel_produto,            v_grupo_leitura,
                                     v_subgrupo_produto,         v_item_produto,
                                     v_deposito,                 v_num_lote,
                                     v_qtde_sugerida_fatu_780,   v_nr_sugestao,
                                     v_nome_programa);
         end if; -- if v_saldo_sugerido_item >= 0.000

      end if;    -- if deletind or (updating and :new.qtde_sugerida < :old.qtde_sugerida)

   end if;

end inter_tr_fatu_780;
-- ALTER TRIGGER "INTER_TR_FATU_780" ENABLE
 

/

exec inter_pr_recompile;

