alter table obrf_010
add ind_mudanca_processo number(1);

-- criado sem default proposital.
-- 1 - executa nova funcionalidade do substituicao, demais não.

comment on column obrf_010. ind_mudanca_processo is 'Indica se o docto usa nova funcionalidade para substituicao';

exec inter_pr_recompile;
/
exit;
