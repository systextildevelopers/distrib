
  CREATE OR REPLACE PROCEDURE "INTER_PR_ALIMENTA_EXEC_015" (p_data_rejeicao   date,     p_motivo_rejeicao numeric,
                                                        p_tipo_rejeicao   numeric,  p_nivel           varchar2,
                                                        p_grupo           varchar2, p_subgrupo        varchar2,
                                                        p_estagio         numeric,  p_turno           numeric,
                                                        p_qtde_rejeicao   numeric,  p_qtde_produzida  numeric)
is
   contador number;
begin

   begin

      -- Conta registros da exec_015 com a chave.
      select count(*) into contador from exec_015
      where data_rejeicao   = p_data_rejeicao
        and motivo_rejeicao = p_motivo_rejeicao
        and tipo_rejeicao   = p_tipo_rejeicao
        and nivel           = p_nivel
        and grupo           = p_grupo
        and subgrupo        = p_subgrupo
        and estagio         = p_estagio
        and turno           = p_turno;


      if contador > 0
      then

         begin

            -- Atualiza registro existente somando (ou subtraindo) novas quantidades.
            update exec_015
            set qtde_rejeicao  = qtde_rejeicao  + p_qtde_rejeicao,
                qtde_produzida = qtde_produzida + p_qtde_produzida
            where data_rejeicao   = p_data_rejeicao
              and motivo_rejeicao = p_motivo_rejeicao
              and tipo_rejeicao   = p_tipo_rejeicao
              and nivel           = p_nivel
              and grupo           = p_grupo
              and subgrupo        = p_subgrupo
              and estagio         = p_estagio
              and turno           = p_turno;
            exception
            when others then
               raise_application_error(-20000,'Erro ao atualizar tabela EXEC_015.');
         end;

      else

         -- Somente insere numeros positivos.
         if p_qtde_rejeicao >= 0.000 and  p_qtde_produzida >= 0.000
         then

            begin

               -- Insere novo registro.
               insert into exec_015
                        (data_rejeicao,   motivo_rejeicao,
                         tipo_rejeicao,   nivel,
                         grupo,           subgrupo,
                         estagio,         turno,
                         qtde_rejeicao,   qtde_produzida)
                  values
                        (p_data_rejeicao, p_motivo_rejeicao,
                         p_tipo_rejeicao, p_nivel,
                         p_grupo,         p_subgrupo,
                         p_estagio,       p_turno,
                         p_qtde_rejeicao, p_qtde_produzida);
                exception
                when others then
                   raise_application_error(-20000,'Erro ao inserir tabela EXEC_015.');
            end;

         end if;

      end if;

   end;


end inter_pr_alimenta_exec_015;


 

/

exec inter_pr_recompile;

