create or replace procedure inter_pr_gera_sped_0400 (p_cod_empresa  IN  NUMBER,
                                                     p_cnpj9         IN NUMBER,
                                                     p_cnpj4         IN NUMBER,
                                                     p_cnpj2         IN NUMBER,
                                                     p_junta_empresa IN NUMBER,
                                                     p_des_erro     OUT varchar2) is
--
-- Finalidade: Gerar a tabela sped_0400 - Naturezas de Operacao
-- Autor.....: Cesar Anton
-- Data......: 20/11/08
--
-- Historicos
--
-- Data    Autor    Observacoes
--

CURSOR u_sped_c170 (p_cod_empresa    NUMBER,
                    p_cnpj9          NUMBER,
                    p_cnpj4          NUMBER,
                    p_cnpj2          NUMBER,
                    p_junta_empresa  NUMBER) IS
   SELECT DISTINCT cod_nat_oper, sig_estado_oper
   from   sped_c170, sped_c100, fatu_504
   where sped_c170.cod_empresa       = sped_c100.cod_empresa
     and fatu_504.codigo_empresa     = sped_c100.cod_empresa
     and sped_c170.num_nota_fiscal   = sped_c100.num_nota_fiscal
     and sped_c170.cod_serie_nota    = sped_c100.cod_serie_nota
     and sped_c170.num_cnpj_9        = sped_C100.Num_Cnpj_9
     and sped_c170.num_cnpj_4        = sped_C100.Num_Cnpj_4
     and sped_c170.num_cnpj_2        = sped_C100.Num_Cnpj_2
     and sped_c100.tip_entrada_saida = sped_c170.tip_entrada_saida
     and sped_c100.cod_modelo not in ('00','02','06','07','08','09','10','11','21','22','27','28','57','67','29','6')
     and not (fatu_504.tipo_sped = 0 and sped_c100.cod_modelo in ('55','65','59') and (sped_c100.tip_entrada_saida = 'S' or sped_c100.tip_emissao_prop = 0))
     and sped_c100.cod_situacao_nota not in (2,3,5)
     and sped_c100.cod_serie_nota    <> 'ECF'
     and sped_c100.cod_serie_nota    <> 'CF'
     and sped_c100.flag_exp          = 'N'
     and sped_c100.ind_nf_consumo    = 'N'
     and  ((sped_c170.cod_empresa in (select fatu_500.codigo_empresa
                                         from   fatu_500
                                         where  fatu_500.cgc_9 = p_cnpj9
                                           and  fatu_500.cgc_4 = p_cnpj4
                                           and  fatu_500.cgc_2 = p_cnpj2
                                           and  p_junta_empresa = 1)
               or  (sped_c170.cod_empresa = p_cod_empresa
                    and  p_junta_empresa = 0)));

CURSOR u_pedi_080 (p_cod_nature_opera  pedi_080.natur_operacao%TYPE,
                   p_sig_estado        pedi_080.estado_natoper%type) IS
   SELECT *
   from   pedi_080
   where  pedi_080.natur_operacao = p_cod_nature_opera
   AND    pedi_080.estado_natoper = p_sig_estado;

w_erro                 EXCEPTION;
w_ind_achou            VARCHAR2(1);
w_tip_mensagem         sped_0450.tip_mensagem%type;
w_des_mensagem         sped_0450.des_mensagem%type;
w_tem_msg              number(2);

BEGIN
   p_des_erro          := NULL;
   FOR sped_c170 IN u_sped_c170 (p_cod_empresa, p_cnpj9,p_cnpj4,p_cnpj2,
                                 p_junta_empresa) LOOP
       w_ind_achou := 'N';
       FOR pedi_080 IN u_pedi_080 (sped_c170.cod_nat_oper, sped_c170.sig_estado_oper) LOOP
           w_ind_achou := 'S';
           BEGIN
               INSERT INTO sped_0400
                 (cod_empresa
                 ,cod_natureza
                 ,des_natureza
                 ,cfop
                 ) VALUES
                 (p_cod_empresa
                 ,pedi_080.natur_operacao || pedi_080.estado_natoper
                 ,pedi_080.descr_nat_oper
                 ,REPLACE(pedi_080.cod_natureza,'.') || substr(to_char(pedi_080.divisao_natur,'00'),2,1));
           EXCEPTION
               WHEN Dup_Val_On_Index THEN
                  NULL;
               WHEN OTHERS THEN
                 p_des_erro := 'Erro na inclusao da tabela sped_0400 ' || Chr(10) || SQLERRM;
                 RAISE W_ERRO;
           END;

       if pedi_080.cod_mensagem > 0
       then
          begin
             select obrf_874.tip_mensagem,
                    trim(obrf_874.des_mensagem1 ||  obrf_874.des_mensagem2 ||
                    obrf_874.des_mensagem3 ||  obrf_874.des_mensagem4 ||
                    obrf_874.des_mensagem5)
             into   w_tip_mensagem,  w_des_mensagem
             from obrf_874
             where obrf_874.cod_mensagem = pedi_080.cod_mensagem;
          exception
             when no_data_found then
             w_tip_mensagem  := ' ';
             w_des_mensagem  := ' ';
          end;

          begin
             select count(*)
             into w_tem_msg
             from sped_0450
             where sped_0450.cod_empresa  = p_cod_empresa
               and sped_0450.cod_mensagem = pedi_080.cod_mensagem
               and sped_0450.tip_mensagem = w_tip_mensagem;
          exception
             when no_data_found then
             w_tem_msg := 0;
          end;

          if w_tem_msg = 0
          then
              BEGIN
                 INSERT INTO sped_0450
                    (cod_empresa
                    ,cod_mensagem
                    ,des_mensagem
                    ,tip_mensagem
                    ) values
                    (p_cod_empresa                 -- cod_empresa
                    ,pedi_080.cod_mensagem         -- cod_mensagem
                    ,w_des_mensagem                -- des_mesagem
                    ,w_tip_mensagem                -- tipo_mensagem
                    );
              EXCEPTION
                 WHEN OTHERS THEN
                    p_des_erro := 'Erro na inclusao da tabela sped_0450 ' || Chr(10) ||
                                  ' MENSAGEM: ' || pedi_080.cod_mensagem ||
                                  Chr(10) || SQLERRM;
                    RAISE W_ERRO;
              END;
          end if;
       end if;

       END LOOP;
       IF  w_ind_achou = 'N' THEN
           inter_pr_insere_erro_sped ('F',p_cod_empresa,
                               'Nao achou a natureza de operacao nos cadastros do sistema ' || Chr(10) ||
                               '   Natureza: ' || sped_c170.cod_nat_oper || '-' || sped_c170.sig_estado_oper);
       END IF;

       COMMIT;

   END LOOP;

   COMMIT;

EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_0400 ' || Chr(10) || p_des_erro;
      ROLLBACK;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure p_gera_sped_0400 ' || Chr(10) || SQLERRM;
      ROLLBACK;
END inter_pr_gera_sped_0400;

 

/

exec inter_pr_recompile;

