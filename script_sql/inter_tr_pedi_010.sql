create or replace TRIGGER "INTER_TR_PEDI_010" 
before insert or update of nome_cliente, fantasia_cliente, endereco_cliente,
                 bairro,       cep_cliente,      telefone_cliente,
                 fax_cliente,  e_mail,           cdrepres_cliente,
                 cod_cidade,   cod_sit_credito,  situacao_cliente  on pedi_010
for each row

declare

  v_cidade_cliente basi_160.cidade%type;
  v_estado_cliente basi_160.estado%type;

begin


    if inserting
    then
       if :new.situacao_cliente = 0
       then
          :new.situacao_cliente := 1;
       end if;
    end if;


   if UPDATING
   then

      select basi_160.cidade,   basi_160.estado
      into   v_cidade_cliente,  v_estado_cliente
      from basi_160
      where basi_160.cod_cidade = :new.cod_cidade;

      update hdoc_050
         set hdoc_050.nome_cliente    = :new.nome_cliente,
             hdoc_050.nome_fantasia   = :new.fantasia_cliente,
             hdoc_050.endereco        = :new.endereco_cliente,
             hdoc_050.bairro          = :new.bairro,
             hdoc_050.cep             = :new.cep_cliente,
             hdoc_050.telefone        = :new.telefone_cliente,
             hdoc_050.fax             = :new.fax_cliente,
             hdoc_050.e_mail          = :new.e_mail,
             hdoc_050.representante   = :new.cdrepres_cliente,
             hdoc_050.cod_sit_credito = :new.cod_sit_credito,
             hdoc_050.cidade          = v_cidade_cliente,
             hdoc_050.uf              = v_estado_cliente
      where hdoc_050.cgc9 = :new.cgc_9
        and hdoc_050.cgc4 = :new.cgc_4
        and hdoc_050.cgc2 = :new.cgc_2;

        -- Alteracao na trigger INTER_TR_PEDI_010 (22/07/2024) ticket: 241042
        if :old.situacao_cliente = 1 and :new.situacao_cliente = 2 then

            update pedi_728
            set situacao       = 'I'
                ,data_situacao = trunc(sysdate)
            where   pedi_728.cnpj9_cliente = :new.cgc_9
                and pedi_728.cnpj4_cliente = :new.cgc_4
                and pedi_728.cnpj2_cliente = :new.cgc_2;
            
    end if;
        
   end if;

end inter_tr_pedi_010;


-- ALTER TRIGGER "INTER_TR_PEDI_010" ENABLE
