alter table basi_220
add (faixa_etaria_custos number(3) default 0);

comment on column basi_220.faixa_etaria
is 'Faixa etária de custo que o tamanho atende.';

exec inter_pr_recompile;
