alter table basi_010
add descr_ing varchar2(60);

alter table basi_010
add descr_esp varchar2(60);

alter table basi_122
add descr_ingles varchar2(60);

alter table basi_122
add descr_espanhol varchar2(60);

alter table hdoc_001
add descr_ingles varchar2(60);

alter table hdoc_001
add descr_espanhol varchar2(60);

alter table basi_542
add descr_ingles varchar2(60);

alter table basi_542
add descr_espanhol varchar2(60);

exec inter_pr_recompile;
/
