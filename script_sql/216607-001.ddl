alter table fatu_504 add atualizacao_preco_individual number(1) default 0 ;

comment on column fatu_504.atualizacao_preco_individual is 'Replica preços na tela de Atualização de Preços Individual. 1- SIM 0- NÃO';
