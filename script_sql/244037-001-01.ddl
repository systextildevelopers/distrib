create table finalidade_ob(
    codigo_finalidade  number(3)
    ,descricao         varchar2(30) not null
);

alter table finalidade_ob
add constraint pk_finalidade_ob primary key (codigo_finalidade);
