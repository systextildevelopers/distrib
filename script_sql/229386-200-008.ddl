create table blocok_290 (
	COD_EMPRESA             number(3),
	ANO_PERIODO_APUR        number(4),
	MES_PERIODO_APUR        number(2),

	ORIGEM                  varchar2(20),
	ORDEM_PRODUCAO          number(9),
	PERIODO                 number(4),
	PACOTE                  number(5),
	ESTAGIO_AGRUPADOR       number(2),

	COD_DOC_OP              varchar2(30),
	DT_INI_OP               varchar2(8),
	DT_FIN_OP               varchar2(8),
	
	data_hora_ins           date            default sysdate
);
