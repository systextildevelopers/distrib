create or replace procedure inter_pr_estrutura_itens_fci
   (p_cod_empresa      in number,
    p_descricao_item   in varchar2,
    p_nivel            in varchar2,
    p_grupo            in varchar2,
    p_subgrupo         in varchar2,
    p_item             in varchar2,
    p_nivel_nota       in varchar2,
    p_grupo_nota       in varchar2,
    p_subgrupo_nota    in varchar2,
    p_item_nota        in varchar2,    
    p_qtde_item        in number,
    p_nivel_material   in varchar2,
    p_perc_icms_venda  in number,
    p_perc_icms_compra in number,
    p_periodo_ini      in date,
    p_periodo_fim      in date,
    p_periodo_ini_ent  in date,    
    p_codigo_usuario   in number,
    p_usuario          in varchar2,
    p_nr_solicitacao   in number,
    p_nome_programa    in varchar2,
    p_visualiza_mp     in number,
    p_total_vlr_unit   in number,
    p_alternativa_custos in number,
    p_deb_icms        in number,
    p_contador_est    in number,
    p_msg_erro        out varchar2
) is

   TYPE nivelComp IS TABLE OF basi_030.nivel_estrutura%TYPE;
   p_nivel_comp nivelComp;

   TYPE grupoComp IS TABLE OF basi_030.referencia%TYPE;
   p_grupo_comp grupoComp;

   TYPE subgrupoComp IS TABLE OF basi_020.tamanho_ref%TYPE;
   p_sub_comp subgrupoComp;

   TYPE itemComp IS TABLE OF basi_010.item_estrutura%TYPE;
   p_item_comp itemComp;

   TYPE consumo IS TABLE OF basi_050.consumo%TYPE;
   p_consumo consumo;

   TYPE alternativaComp IS TABLE OF basi_050.alternativa_item%TYPE;
   p_alternativa_comp alternativaComp;

   TYPE sequenciaComp IS TABLE OF basi_050.sequencia%TYPE;
   p_sequencia sequenciaComp;

   TYPE subgrupoItem IS TABLE OF basi_020.tamanho_ref%TYPE;
   p_sub_item subgrupoItem;

   TYPE itemItem IS TABLE OF basi_010.item_estrutura%TYPE;
   p_item_item itemItem;

 --  p_alternativa_aux basi_050.alternativa_item%TYPE;
   comprado_fabric   integer;
   nr_documento      integer;
   qtde_itnota       number;       
   vlr_unitario      number;
   p_seq_temporaria  integer; 
   p_cons_total      number;
   p_base_icms       number;
   p_cred_icms       number;
   p_encontrou_item  number;
   p_conta_est       number;

procedure insere_estrutura(p_erro boolean) is
   w_descricao  varchar2(60);  
   w_status     number;
   w_deb_icms   number;
   w_cred_icms  number;
begin

   p_encontrou_item := 0;
   begin
      select oper_tmp.nr_solicitacao, flo_04,      flo_05
        into p_encontrou_item,        w_deb_icms,  w_cred_icms
        from oper_tmp
       where oper_tmp.nr_solicitacao = p_nr_solicitacao
         and oper_tmp.nome_relatorio = p_nome_programa
         and oper_tmp.str_01 = p_nivel_nota
         and oper_tmp.str_02 = p_grupo_nota
         and oper_tmp.str_03 = p_subgrupo_nota
         and oper_tmp.str_04 = p_item_nota;
   exception when others then
      p_encontrou_item := 0;
      w_deb_icms       := 0;
      w_cred_icms      := 0;
   end;

   begin
      select count(*) + 1
        into p_seq_temporaria
        from oper_tmp
       where oper_tmp.nr_solicitacao = p_nr_solicitacao
         and oper_tmp.nome_relatorio = p_nome_programa;
   exception when others then
      p_seq_temporaria := 1;
   end;

   w_descricao := '';
   w_status    := 0;
   w_cred_icms := w_cred_icms + p_cred_icms;
   w_deb_icms  := p_deb_icms;
   if p_erro then
      w_descricao := 'ESTRUTURA DO PRODUTO COM PROBLEMA';
      w_status    := 1; 
      w_cred_icms := 0;
      w_deb_icms  := 0;
   end if; 
   
   if p_encontrou_item = 0
   then
      insert into oper_tmp (
                  nr_solicitacao,    nome_relatorio,
                  sequencia,         int_01,
                  int_03,

                  str_01,             str_02,
                  str_03,             str_04,
                  str_60,             int_02,
                  flo_03,             flo_04,
                  flo_05,             str_61
               ) values (
                  p_nr_solicitacao, p_nome_programa,
                  p_seq_temporaria, p_cod_empresa,
                  p_codigo_usuario,

                  p_nivel_nota,     p_grupo_nota,
                  p_subgrupo_nota,  p_item_nota,
                  p_descricao_item, w_status,
                  p_cons_total,     w_deb_icms,
                  w_cred_icms,      w_descricao
               );
   else
      update oper_tmp set
             flo_05 = w_cred_icms,
             flo_04 = w_deb_icms, 
             flo_03 = flo_03 + p_cons_total,
             str_61 = w_descricao,
             int_02 = w_status
       where oper_tmp.nr_solicitacao = p_nr_solicitacao
         and oper_tmp.nome_relatorio = p_nome_programa
         and oper_tmp.str_01 = p_nivel_nota
         and oper_tmp.str_02 = p_grupo_nota
         and oper_tmp.str_03 = p_subgrupo_nota
         and oper_tmp.str_04 = p_item_nota;                         
   end if;
   
   if p_erro then
      commit work;
   end if;   
end; 


BEGIN
   
   p_msg_erro := ' ';     
   
   begin
     select
            basi_050.nivel_comp,       basi_050.grupo_comp,
            basi_050.sub_comp,         basi_050.item_comp,
            basi_050.alternativa_comp, basi_050.sequencia,
            basi_050.sub_item,         basi_050.item_item,
            basi_050.consumo
        BULK COLLECT INTO
            p_nivel_comp,              p_grupo_comp,
            p_sub_comp,                p_item_comp,
            p_alternativa_comp,        p_sequencia,
            p_sub_item,                p_item_item,
            p_consumo
       from basi_050
      where  basi_050.nivel_item = p_nivel
        and  basi_050.grupo_item = p_grupo
        and (basi_050.sub_item  = p_subgrupo or basi_050.sub_item = '000')
        and (basi_050.item_item = p_item or basi_050.item_item = '000000')
        and  basi_050.alternativa_item = p_alternativa_custos;
    end;

   if sql%found
   then

     FOR i IN p_sequencia.FIRST .. p_sequencia.LAST
     LOOP
         if p_sub_comp(i) = '000' or p_consumo(i) = 0.0000 then
         begin
           select basi_040.sub_comp, basi_040.consumo
             into p_sub_comp(i),     p_consumo(i)
             from basi_040
            where basi_040.nivel_item       = p_nivel
              and basi_040.grupo_item       = p_grupo
              and basi_040.sub_item         = p_subgrupo
              and basi_040.item_item        = p_item_item(i)
              and basi_040.sequencia        = p_sequencia(i)
              and basi_040.alternativa_item = p_alternativa_comp(i);
           exception
             when others then
               p_sub_comp(i) := '000';
               p_consumo(i)  := 0.0000;
           end;
         end if;

         if p_item_comp(i) = '000000' then
         begin
           select basi_040.item_comp
             into p_item_comp(i)
             from basi_040
            where basi_040.nivel_item       = p_nivel
              and basi_040.grupo_item       = p_grupo
              and basi_040.sub_item         = p_sub_item(i)
              and basi_040.item_item        = p_item
              and basi_040.sequencia        = p_sequencia(i)
              and basi_040.alternativa_item = p_alternativa_comp(i);
           exception 
             when others then
               p_item_comp(i) := '000000';
           end;
         end if;
         
         if ((p_contador_est >= 1000) or ((p_nivel = p_nivel_comp(i)) and (p_grupo = p_grupo_comp(i)) and (p_subgrupo = p_sub_comp(i)) and (p_item = p_item_comp(i)) ))
         then 
            insere_estrutura(true);
     
            raise_application_error(-20000, 'Produto: ' || p_nivel_nota||'.'||p_grupo_nota||'.'||p_subgrupo_nota||'.'||p_item_nota );
         end if;         
         
         
         comprado_fabric := 0;
         nr_documento     := 0;
         p_seq_temporaria := p_seq_temporaria + 1;

         begin
            select 1
              into comprado_fabric
              from basi_030
             where basi_030.nivel_estrutura = p_nivel_comp(i)
               and basi_030.referencia      = p_grupo_comp(i)
               and basi_030.comprado_fabric = 1;
            exception when others then
               comprado_fabric := 0;
         end;
         
         p_cons_total := p_qtde_item  * p_consumo(i);   
         if comprado_fabric = 1
         then
           
            if (p_nivel_comp(i) = p_nivel_material) or (p_nivel_material = 'X')
            then

               begin
                  select dados_entrada.documento,  dados_entrada.quantidade,
                         dados_entrada.vl_unitario
                    into nr_documento,             qtde_itnota,
                         vlr_unitario                  
                    from (select obrf_010.documento documento,     obrf_015.quantidade quantidade,
                                (obrf_015.base_calc_icm / obrf_015.quantidade) as vl_unitario
                            from obrf_010, obrf_015
                           where obrf_010.data_emissao between p_periodo_ini_ent and p_periodo_fim
                             and obrf_010.local_entrega    = p_cod_empresa
                             and obrf_010.documento        = obrf_015.capa_ent_nrdoc
                             and obrf_010.serie            = obrf_015.capa_ent_serie
                             and obrf_010.cgc_cli_for_9    = obrf_015.capa_ent_forcli9
                             and obrf_010.cgc_cli_for_4    = obrf_015.capa_ent_forcli4
                             and obrf_010.cgc_cli_for_2    = obrf_015.capa_ent_forcli2
                             and obrf_015.percentual_icm   = p_perc_icms_compra
                             and obrf_015.coditem_nivel99  = p_nivel_comp(i)
                             and obrf_015.coditem_grupo    = p_grupo_comp(i)
                             and obrf_015.coditem_subgrupo = p_sub_comp(i)
                             and obrf_015.coditem_item     = p_item_comp(i) 
                          order by obrf_010.data_emissao desc) dados_entrada
                  where rownum = 1;
               exception when others then
                  nr_documento := 0;
               end;               

               if nr_documento > 0
               then 
                  p_base_icms  := p_cons_total * vlr_unitario;
                  p_cred_icms := (p_base_icms * p_perc_icms_compra)/100;

                  insere_estrutura(false);

               end if;
            end if;
         end if;

         p_conta_est := p_contador_est + 1;

         inter_pr_estrutura_itens_fci(p_cod_empresa,
                                      p_descricao_item,
                                      p_nivel_comp(i),
                                      p_grupo_comp(i),
                                      p_sub_comp(i),
                                      p_item_comp(i),
                                      p_nivel_nota,
                                      p_grupo_nota,
                                      p_subgrupo_nota,
                                      p_item_nota,
                                      p_cons_total,
                                      p_nivel_material,
                                      p_perc_icms_venda,
                                      p_perc_icms_compra,
                                      p_periodo_ini,
                                      p_periodo_fim,
                                      p_periodo_ini_ent,
                                      p_codigo_usuario,
                                      p_usuario,
                                      p_nr_solicitacao,
                                      p_nome_programa,
                                      p_visualiza_mp,
                                      p_total_vlr_unit,
                                      p_alternativa_comp(i),
                                      p_deb_icms,
                                      p_conta_est,
                                      p_msg_erro);


     END LOOP;

  end if;
  
end inter_pr_estrutura_itens_fci;

/

exec inter_pr_recompile;
