
  CREATE OR REPLACE TRIGGER "TRIGGER_HDOC_100_HIST" 
BEFORE UPDATE OR DELETE OR INSERT on hdoc_100
for each row







declare
   ws_area_bloqueio             number(2);
   ws_sit_bloqueio              number(2);
   ws_usubloq_cdusu             varchar2(250);
   ws_usubloq_empr_usu          number(3);
   ws_nivel_bloqueio_old        number(1);
   ws_nivel_bloqueio_atu        number(1);
   ws_valor_inicial_old         number(13,2);
   ws_valor_inicial_atu         number(13,2);
   ws_valor_final_old           number(13,2);
   ws_valor_final_atu           number(13,2);
   ws_lib_orcament_old          number(1);
   ws_lib_orcament_atu          number(1);
   ws_local_bloqueio_old        number(1);
   ws_local_bloqueio_atu        number(1);
   ws_tipo_ocorr                varchar2(1);
   ws_usuario_rede              varchar2(20);
   ws_maquina_rede              varchar2(40);
   ws_aplicacao                 varchar2(20);
begin
   if INSERTING then
      ws_area_bloqueio         := :new.area_bloqueio;
      ws_sit_bloqueio          := :new.sit_bloqueio;
      ws_usubloq_cdusu         := :new.usubloq_cdusu;
      ws_usubloq_empr_usu      := :new.usubloq_empr_usu;
      ws_nivel_bloqueio_old    := 0;
      ws_nivel_bloqueio_atu    := :new.nivel_bloqueio;
      ws_valor_inicial_old     := 0.0;
      ws_valor_inicial_atu     := :new.valor_inicial;
      ws_valor_final_old       := 0.0;
      ws_valor_final_atu       := :new.valor_final;
      ws_lib_orcament_old      := 0;
      ws_lib_orcament_atu      := :new.lib_orcament;
      ws_local_bloqueio_old    := 0;
      ws_local_bloqueio_atu    := :new.local_bloqueio;
      ws_tipo_ocorr            := 'I';
   elsif UPDATING then
         ws_area_bloqueio         := :new.area_bloqueio;
         ws_sit_bloqueio          := :new.sit_bloqueio;
         ws_usubloq_cdusu         := :new.usubloq_cdusu;
         ws_usubloq_empr_usu      := :new.usubloq_empr_usu;
         ws_nivel_bloqueio_old    := :old.nivel_bloqueio;
         ws_nivel_bloqueio_atu    := :new.nivel_bloqueio;
         ws_valor_inicial_old     := :old.valor_inicial;
         ws_valor_inicial_atu     := :new.valor_inicial;
         ws_valor_final_old       := :old.valor_final;
         ws_valor_final_atu       := :new.valor_final;
         ws_lib_orcament_old      := :old.lib_orcament;
         ws_lib_orcament_atu      := :new.lib_orcament;
         ws_local_bloqueio_old    := :old.local_bloqueio;
         ws_local_bloqueio_atu    := :new.local_bloqueio;
         ws_tipo_ocorr            := 'A';
      elsif DELETING then
            ws_area_bloqueio         := :old.area_bloqueio;
            ws_sit_bloqueio          := :old.sit_bloqueio;
            ws_usubloq_cdusu         := :old.usubloq_cdusu;
            ws_usubloq_empr_usu      := :old.usubloq_empr_usu;
            ws_nivel_bloqueio_old    := :old.nivel_bloqueio;
            ws_nivel_bloqueio_atu    := 0;
            ws_valor_inicial_old     := :old.valor_inicial;
            ws_valor_inicial_atu     := 0.0;
            ws_valor_final_old       := :old.valor_final;
            ws_valor_final_atu       := 0.0;
            ws_lib_orcament_old      := :old.lib_orcament;
            ws_lib_orcament_atu      := 0;
            ws_local_bloqueio_old    := :old.local_bloqueio;
            ws_local_bloqueio_atu    := 0;
            ws_tipo_ocorr            := 'D';
   end if;
   select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
   into   ws_usuario_rede,     ws_maquina_rede,      ws_aplicacao
   from sys.v_$session
   where audsid = userenv('SESSIONID');
   INSERT INTO hdoc_100_hist
     (area_bloqueio,         sit_bloqueio,          usubloq_cdusu,
      usubloq_empr_usu,      nivel_bloqueio_old,    nivel_bloqueio_atu,
      valor_inicial_old,     valor_inicial_atu,     valor_final_old,
      valor_final_atu,       lib_orcament_old,      lib_orcament_atu,
      local_bloqueio_old,    local_bloqueio_atu,    tipo_ocorr,
      data_ocorr,            usuario_rede,          maquina_rede,
      aplicacao)
   VALUES
     (ws_area_bloqueio,      ws_sit_bloqueio,       ws_usubloq_cdusu,
      ws_usubloq_empr_usu,   ws_nivel_bloqueio_old, ws_nivel_bloqueio_atu,
      ws_valor_inicial_old,  ws_valor_inicial_atu,  ws_valor_final_old,
      ws_valor_final_atu,    ws_lib_orcament_old,   ws_lib_orcament_atu,
      ws_local_bloqueio_old, ws_local_bloqueio_atu, ws_tipo_ocorr,
      sysdate,               ws_usuario_rede,       ws_maquina_rede,
      ws_aplicacao);
end trigger_hdoc_100_hist;


-- ALTER TRIGGER "TRIGGER_HDOC_100_HIST" ENABLE
 

/

exec inter_pr_recompile;

