create table empr_070
(
  codigo_empresa               NUMBER(3) default 0 not null,
  tipo_titulo                  NUMBER(2) default 0 not null,
  cod_local                    NUMBER(3),
  seq_end_cobranca             NUMBER(3),
  portador_duplic              NUMBER(3),
  nr_identificacao             NUMBER(3),
  moeda_titulo                 NUMBER(2),
  responsavel_receb            NUMBER(3),
  data_atual                   VARCHAR2(1),
  cod_transacao                NUMBER(3),
  codigo_contabil              NUMBER(6),
  cod_historico                NUMBER(4),
  tecido_peca                  VARCHAR2(1) default '');

ALTER TABLE empr_070 ADD CONSTRAINT empr_070 PRIMARY KEY(codigo_empresa, tipo_titulo);
