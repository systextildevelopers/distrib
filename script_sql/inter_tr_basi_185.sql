
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_185" 
before insert or
update of situacao, centro_custo, descricao, tipo_mao_obra,
	tipo_ccusto, local_entrega, custo_minuto, divisao_producao,
	tempo_turno1, tempo_turno2, tempo_turno3, tempo_turno4,
	codigo_criterio, codigo_contabil, horaini_t1, horafim_t1,
	horaini_t2, horafim_t2, horaini_t3, horafim_t3,
	horaini_t4, horafim_t4, cc_agrupador, intervaloini_t1,
	intervalofim_t1, aplicacao_intervalo_t1, intervaloini_t2, intervalofim_t2,
	aplicacao_intervalo_t2, intervaloini_t3, intervalofim_t3, aplicacao_intervalo_t3,
	intervaloini_t4, intervalofim_t4, aplicacao_intervalo_t4, custo_minuto_previsto,
	centro_custo_pai, custo_minuto_estimado, data_cadastro, custo_despesa,
	fixo_variavel, data_alteracao
on basi_185
for each row

begin
   -- Logica para gravar a hora temino e hora inicio com uma data valida.
   -- A data definida '16/11/1989', foi definida com base da data de gravacao padrao do Vision.
   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horaini_t1 := to_date('16/11/1989 '||to_char(:new.horaini_t1,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horafim_t1 is not null)
   or (inserting and :new.horafim_t1 is not null)
   then
      :new.horafim_t1 := to_date('16/11/1989 '||to_char(:new.horafim_t1,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horaini_t2 := to_date('16/11/1989 '||to_char(:new.horaini_t2,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horafim_t2 := to_date('16/11/1989 '||to_char(:new.horafim_t2,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horaini_t3 := to_date('16/11/1989 '||to_char(:new.horaini_t3,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horafim_t3 := to_date('16/11/1989 '||to_char(:new.horafim_t3,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horaini_t4 := to_date('16/11/1989 '||to_char(:new.horaini_t4,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horafim_t4 := to_date('16/11/1989 '||to_char(:new.horafim_t4,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;



   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervaloini_t1 := to_date('16/11/1989 '||to_char(:new.intervaloini_t1,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervalofim_t1 := to_date('16/11/1989 '||to_char(:new.intervalofim_t1,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervaloini_t2 := to_date('16/11/1989 '||to_char(:new.intervaloini_t2,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervalofim_t2 := to_date('16/11/1989 '||to_char(:new.intervalofim_t2,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervaloini_t3 := to_date('16/11/1989 '||to_char(:new.intervaloini_t3,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervalofim_t3 := to_date('16/11/1989 '||to_char(:new.intervalofim_t3,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervaloini_t4 := to_date('16/11/1989 '||to_char(:new.intervaloini_t4,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervalofim_t4 := to_date('16/11/1989 '||to_char(:new.intervalofim_t4,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :old.descricao <> :new.descricao)
   or (inserting)
   then
      :new.data_alteracao := sysdate;
   end if;

end inter_tr_basi_185;
-- ALTER TRIGGER "INTER_TR_BASI_185" ENABLE
 

/

exec inter_pr_recompile;

