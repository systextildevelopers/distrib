ALTER TABLE CONT_032_HIST ADD CODIGO2 VARCHAR2(7);

update cont_032_hist set codigo2=codigo;

alter table cont_032_hist drop column codigo;
alter table cont_032_hist rename column codigo2 to codigo;
/
exec inter_pr_recompile;
