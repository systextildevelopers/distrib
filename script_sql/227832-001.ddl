begin

--================
--== Inventario ==  
--================

  begin
    execute immediate '
      CREATE TABLE  "ESTQ_192" (	
        CODIGO_INVENTARIO           NUMBER(9, 0) NOT NULL ENABLE,
        SITUACAO                    NUMBER(1, 0) DEFAULT 0 NOT NULL ENABLE,
        QTDE_LEITURAS               NUMBER(1, 0) NOT NULL ENABLE,
        TIPO_INVENTARIO             VARCHAR2(1)  NOT NULL ENABLE,
        DEPOSITO_AGRUPADOR          VARCHAR2(1)  DEFAULT ''D'' NOT NULL ENABLE,
        CODIGO_DEPOSITO             NUMBER(9,0)  NOT NULL ENABLE,
        PRODUTOS                    CLOB,
        CONTA_ESTOQUE               NUMBER(2,0),
        ARTIGO                      NUMBER(4,0),
        ENDERECOS                   CLOB,
        USUARIO_SYSTEXTIL_CRIACAO   VARCHAR2(2000),
        USUARIO_REDE_CRIACAO        VARCHAR2(2000),
        MAQUINA_REDE_CRIACAO        VARCHAR2(2000),
        DATA_CRIACAO                TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        USUARIO_SYSTEXTIL_INI_CONT1 VARCHAR2(2000),
        USUARIO_REDE_INI_CONT1      VARCHAR2(2000),
        MAQUINA_REDE_INI_CONT1      VARCHAR2(2000),
        DATA_INI_CONT1              TIMESTAMP,
        USUARIO_SYSTEXTIL_FIM_CONT1 VARCHAR2(2000), 
        USUARIO_REDE_FIM_CONT1      VARCHAR2(2000),
        MAQUINA_REDE_FIM_CONT1      VARCHAR2(2000),
        DATA_FIM_CONT1              TIMESTAMP,
        USUARIO_SYSTEXTIL_INI_CONT2 VARCHAR2(2000),
        USUARIO_REDE_INI_CONT2      VARCHAR2(2000),
        MAQUINA_REDE_INI_CONT2      VARCHAR2(2000),
        DATA_INI_CONT2              TIMESTAMP,
        USUARIO_SYSTEXTIL_FIM_CONT2 VARCHAR2(2000), 
        USUARIO_REDE_FIM_CONT2      VARCHAR2(2000),
        MAQUINA_REDE_FIM_CONT2      VARCHAR2(2000),
        DATA_FIM_CONT2              TIMESTAMP,
        USUARIO_SYSTEXTIL_INI_CONT3 VARCHAR2(2000),
        USUARIO_REDE_INI_CONT3      VARCHAR2(2000),
        MAQUINA_REDE_INI_CONT3      VARCHAR2(2000),
        DATA_INI_CONT3              TIMESTAMP,
        USUARIO_SYSTEXTIL_FIM_CONT3 VARCHAR2(2000), 
        USUARIO_REDE_FIM_CONT3      VARCHAR2(2000),
        MAQUINA_REDE_FIM_CONT3      VARCHAR2(2000),
        DATA_FIM_CONT3              TIMESTAMP
      )';
    exception when others then
      if sqlcode != -955 then
        raise;
      end if;
  end;

  begin
    execute immediate '
      CREATE UNIQUE INDEX "PK_ESTQ_192" ON "ESTQ_192" ("CODIGO_INVENTARIO")
    ';
    exception when others then
      null;
  end;

  begin
    execute immediate '
      ALTER TABLE  "ESTQ_192" ADD CONSTRAINT "PK_ESTQ_192" PRIMARY KEY ("CODIGO_INVENTARIO") USING INDEX  "PK_ESTQ_192"  ENABLE
    ';
    exception when others then
      null;
  end;

  begin
    execute immediate '
      ALTER TABLE  "ESTQ_192" ADD CONSTRAINT "CHECK_ESTQ_192_TIPO_INVENTARIO" CHECK( "TIPO_INVENTARIO" IN (''I'', ''C'') ) ENABLE
    ';
    exception when others then
      null;
  end;

  begin
    execute immediate '
      ALTER TABLE  "ESTQ_192" ADD CONSTRAINT "CHECK_ESTQ_192_DEPOSITO_AGRUPADOR" CHECK( "DEPOSITO_AGRUPADOR" IN (''D'', ''A'') ) ENABLE
    ';
    exception when others then
      null;
  end;

  begin
    execute immediate '
      CREATE SEQUENCE "ESTQ_192_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1
    ';
    exception when others then
      if sqlcode != -955 then
        raise;
      end if;
  end;

end;
