
  CREATE OR REPLACE TRIGGER "INTER_TR_LOJA_090" 
before insert or delete or update of sinal_e_s, valor on loja_090
for each row






declare
   v_valor_movimenta fatu_501.saldo_caixa_loja%type;
begin
   if UPDATING
   then
      if :new.valor <> :old.valor
      then
         raise_application_error(-20000,'Nao e permitido alterar o valor da movimentacao.');
      end if;

      if :new.sinal_e_s <> :old.sinal_e_s
      then

         if :new.sinal_e_s = 'E'
         then
            v_valor_movimenta := :old.valor;
         else
            v_valor_movimenta := (:old.valor * -1);
         end if;

      end if;
   end if;

   if INSERTING
   then
      if :new.sinal_e_s = 'E'
      then
         v_valor_movimenta := :new.valor;
      else
         v_valor_movimenta := (:new.valor * -1);
      end if;
   end if;

   if DELETING
   then
      if :old.sinal_e_s = 'E'
      then
         v_valor_movimenta := (:old.valor * -1);
      else
         v_valor_movimenta := :old.valor;
      end if;
   end if;

   update fatu_501
   set saldo_caixa_loja = saldo_caixa_loja + v_valor_movimenta;

end inter_tr_loja_090;

-- ALTER TRIGGER "INTER_TR_LOJA_090" ENABLE
 

/

exec inter_pr_recompile;

