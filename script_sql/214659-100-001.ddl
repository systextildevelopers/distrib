alter table pedi_100 add antecipacao_a_partir_de date;
alter table pedi_100 add conceder_data_base_fatur number(1);

alter table pedi_100 modify conceder_data_base_fatur default 1;

exec inter_pr_recompile;
