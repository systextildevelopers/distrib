ALTER TABLE BASI_180 ADD REQ_MAT_ALMOXARIFADO NUMBER(1);

COMMENT ON COLUMN BASI_180.REQ_MAT_ALMOXARIFADO IS 'Permite digitação requisição de Material (0 - Não / 1 - Sim)';

exec inter_pr_recompile;
/
