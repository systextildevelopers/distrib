alter table blocok_210 add (
COD_ITEM_NIVEL                   VARCHAR2(1),
COD_ITEM_GRUPO                   VARCHAR2(5),
COD_ITEM_SUBGRUPO                VARCHAR2(3),
COD_ITEM_ITEM                    VARCHAR2(6),
COD_ITEM_EST_AGRUP_INSU          NUMBER(2),
COD_ITEM_EST_SIMULT_INSU         NUMBER(2));

exec inter_pr_recompile;
