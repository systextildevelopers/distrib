create or replace view VI_HDOC_001_517 as
select *
from HDOC_001
where TIPO = 517;
   
create or replace synonym systextilrpt.vi_hdoc_001_517 for vi_hdoc_001_517;

/
exec inter_pr_recompile;
exit;
