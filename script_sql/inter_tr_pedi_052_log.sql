CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_052_LOG" 
after insert or delete or update
on PEDI_052
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);  
       

 if inserting
 then
    begin

        insert into PEDI_052_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           CODIGO_BANCO_OLD,   /*8*/
           CODIGO_BANCO_NEW,   /*9*/
           TIPO_OPERACAO_OLD,   /*10*/
           TIPO_OPERACAO_NEW,   /*11*/
           RPT_RETORNO_OLD,   /*12*/
           RPT_RETORNO_NEW,   /*13*/
           RPT_REMESSA_OLD,   /*14*/
           RPT_REMESSA_NEW,   /*15*/
           QUEBRA_ARQUIVO_OLD,   /*16*/
           QUEBRA_ARQUIVO_NEW    /*17*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.CODIGO_BANCO, /*9*/
           0,/*10*/
           :new.TIPO_OPERACAO, /*11*/
           '',/*12*/
           :new.RPT_RETORNO, /*13*/
           '',/*14*/
           :new.RPT_REMESSA, /*15*/
           '',/*16*/
           :new.QUEBRA_ARQUIVO /*17*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into PEDI_052_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           CODIGO_BANCO_OLD, /*8*/
           CODIGO_BANCO_NEW, /*9*/
           TIPO_OPERACAO_OLD, /*10*/
           TIPO_OPERACAO_NEW, /*11*/
           RPT_RETORNO_OLD, /*12*/
           RPT_RETORNO_NEW, /*13*/
           RPT_REMESSA_OLD, /*14*/
           RPT_REMESSA_NEW, /*15*/
           QUEBRA_ARQUIVO_OLD, /*16*/
           QUEBRA_ARQUIVO_NEW  /*17*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.CODIGO_BANCO,  /*8*/
           :new.CODIGO_BANCO, /*9*/
           :old.TIPO_OPERACAO,  /*10*/
           :new.TIPO_OPERACAO, /*11*/
           :old.RPT_RETORNO,  /*12*/
           :new.RPT_RETORNO, /*13*/
           :old.RPT_REMESSA,  /*14*/
           :new.RPT_REMESSA, /*15*/
           :old.QUEBRA_ARQUIVO,  /*16*/
           :new.QUEBRA_ARQUIVO  /*17*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into PEDI_052_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           CODIGO_BANCO_OLD, /*8*/
           CODIGO_BANCO_NEW, /*9*/
           TIPO_OPERACAO_OLD, /*10*/
           TIPO_OPERACAO_NEW, /*11*/
           RPT_RETORNO_OLD, /*12*/
           RPT_RETORNO_NEW, /*13*/
           RPT_REMESSA_OLD, /*14*/
           RPT_REMESSA_NEW, /*15*/
           QUEBRA_ARQUIVO_OLD, /*16*/
           QUEBRA_ARQUIVO_NEW /*17*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.CODIGO_BANCO, /*8*/
           0, /*9*/
           :old.TIPO_OPERACAO, /*10*/
           0, /*11*/
           :old.RPT_RETORNO, /*12*/
           '', /*13*/
           :old.RPT_REMESSA, /*14*/
           '', /*15*/
           :old.QUEBRA_ARQUIVO, /*16*/
           '' /*17*/
         );
    end;
 end if;
end inter_tr_PEDI_052_log;

-- ALTER TRIGGER "INTER_TR_PEDI_052_LOG" ENABLE
 

/

exec inter_pr_recompile;

