
  CREATE OR REPLACE FUNCTION "INTER_FN_PERC_DEF" 
(motivo   in efic_100.codigo_motivo%type,
 nivel    in efic_100.prod_rej_nivel99%type,
 data_rej_ini in efic_100.data_rejeicao%type,
 data_rej_fim in efic_100.data_rejeicao%type)
return number 
is
  perc_defeitos number(15,2);
begin
  
  select 
  (nvl(efic_100.qtde_defeitos,0)/
  sum(nvl(efic_100.qtde_defeitos,0))*100) into perc_defeitos
  from  efic_100
  where efic_100.codigo_motivo  = motivo
  and efic_100.prod_rej_nivel99 = nivel
  and efic_100.data_rejeicao    >= data_rej_ini
  and efic_100.data_rejeicao    <= data_rej_fim; 
  
return(perc_defeitos);
end inter_fn_perc_def;

 

/

exec inter_pr_recompile;

