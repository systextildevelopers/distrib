alter table fatu_510
add ind_grava_custo_indus number(1) default 0;

comment on column fatu_510.ind_grava_custo_indus is '1 - Indica que a empresa grava custo de industrialização (mapão), 0-Para não gravar';

exec inter_pr_recompile;
/
