create or replace trigger inter_tr_OBRF_160_log
after insert or delete or update
on OBRF_160
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usu?rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);


 if inserting
 then
    begin

        insert into OBRF_160_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           CODIGO_EMPRESA_OLD,   /*8*/
           CODIGO_EMPRESA_NEW,   /*9*/
           NUM_NOTA_FISCAL_OLD,   /*10*/
           NUM_NOTA_FISCAL_NEW,   /*11*/
           SERIE_NOTA_FISCAL_OLD,   /*12*/
           SERIE_NOTA_FISCAL_NEW,   /*13*/
           CNPJ9_OLD,   /*14*/
           CNPJ9_NEW,   /*15*/
           CNPJ4_OLD,   /*16*/
           CNPJ4_NEW,   /*17*/
           CNPJ2_OLD,   /*18*/
           CNPJ2_NEW,   /*19*/
           NOTA_ENTRADA_SAIDA_OLD,   /*20*/
           NOTA_ENTRADA_SAIDA_NEW,   /*21*/
           ARQUIVO_XML_OLD,   /*22*/
           ARQUIVO_XML_NEW,   /*23*/
           NUMERO_DANFE_NFE_OLD,   /*24*/
           NUMERO_DANFE_NFE_NEW,   /*25*/
           TIPO_OLD,   /*26*/
           TIPO_NEW    /*27*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.CODIGO_EMPRESA, /*9*/
           0,/*10*/
           :new.NUM_NOTA_FISCAL, /*11*/
           '',/*12*/
           :new.SERIE_NOTA_FISCAL, /*13*/
           0,/*14*/
           :new.CNPJ9, /*15*/
           0,/*16*/
           :new.CNPJ4, /*17*/
           0,/*18*/
           :new.CNPJ2, /*19*/
           '',/*20*/
           :new.NOTA_ENTRADA_SAIDA, /*21*/
           '',
           :new.ARQUIVO_XML, /*23*/
           '',/*24*/
           :new.NUMERO_DANFE_NFE, /*25*/
           '',
           :new.TIPO /*27*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into OBRF_160_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           CODIGO_EMPRESA_OLD, /*8*/
           CODIGO_EMPRESA_NEW, /*9*/
           NUM_NOTA_FISCAL_OLD, /*10*/
           NUM_NOTA_FISCAL_NEW, /*11*/
           SERIE_NOTA_FISCAL_OLD, /*12*/
           SERIE_NOTA_FISCAL_NEW, /*13*/
           CNPJ9_OLD, /*14*/
           CNPJ9_NEW, /*15*/
           CNPJ4_OLD, /*16*/
           CNPJ4_NEW, /*17*/
           CNPJ2_OLD, /*18*/
           CNPJ2_NEW, /*19*/
           NOTA_ENTRADA_SAIDA_OLD, /*20*/
           NOTA_ENTRADA_SAIDA_NEW, /*21*/
           ARQUIVO_XML_OLD, /*22*/
           ARQUIVO_XML_NEW, /*23*/
           NUMERO_DANFE_NFE_OLD, /*24*/
           NUMERO_DANFE_NFE_NEW, /*25*/
           TIPO_OLD, /*26*/
           TIPO_NEW  /*27*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.CODIGO_EMPRESA,  /*8*/
           :new.CODIGO_EMPRESA, /*9*/
           :old.NUM_NOTA_FISCAL,  /*10*/
           :new.NUM_NOTA_FISCAL, /*11*/
           :old.SERIE_NOTA_FISCAL,  /*12*/
           :new.SERIE_NOTA_FISCAL, /*13*/
           :old.CNPJ9,  /*14*/
           :new.CNPJ9, /*15*/
           :old.CNPJ4,  /*16*/
           :new.CNPJ4, /*17*/
           :old.CNPJ2,  /*18*/
           :new.CNPJ2, /*19*/
           :old.NOTA_ENTRADA_SAIDA,  /*20*/
           :new.NOTA_ENTRADA_SAIDA, /*21*/
           :old.ARQUIVO_XML,  /*22*/
           :new.ARQUIVO_XML, /*23*/
           :old.NUMERO_DANFE_NFE,  /*24*/
           :new.NUMERO_DANFE_NFE, /*25*/
           :old.TIPO,  /*26*/
           :new.TIPO  /*27*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into OBRF_160_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           CODIGO_EMPRESA_OLD, /*8*/
           CODIGO_EMPRESA_NEW, /*9*/
           NUM_NOTA_FISCAL_OLD, /*10*/
           NUM_NOTA_FISCAL_NEW, /*11*/
           SERIE_NOTA_FISCAL_OLD, /*12*/
           SERIE_NOTA_FISCAL_NEW, /*13*/
           CNPJ9_OLD, /*14*/
           CNPJ9_NEW, /*15*/
           CNPJ4_OLD, /*16*/
           CNPJ4_NEW, /*17*/
           CNPJ2_OLD, /*18*/
           CNPJ2_NEW, /*19*/
           NOTA_ENTRADA_SAIDA_OLD, /*20*/
           NOTA_ENTRADA_SAIDA_NEW, /*21*/
           ARQUIVO_XML_OLD, /*22*/
           ARQUIVO_XML_NEW, /*23*/
           NUMERO_DANFE_NFE_OLD, /*24*/
           NUMERO_DANFE_NFE_NEW, /*25*/
           TIPO_OLD, /*26*/
           TIPO_NEW /*27*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.CODIGO_EMPRESA, /*8*/
           0, /*9*/
           :old.NUM_NOTA_FISCAL, /*10*/
           0, /*11*/
           :old.SERIE_NOTA_FISCAL, /*12*/
           '', /*13*/
           :old.CNPJ9, /*14*/
           0, /*15*/
           :old.CNPJ4, /*16*/
           0, /*17*/
           :old.CNPJ2, /*18*/
           0, /*19*/
           :old.NOTA_ENTRADA_SAIDA, /*20*/
           '', /*21*/
           :old.ARQUIVO_XML, /*22*/
           '',
           :old.NUMERO_DANFE_NFE, /*24*/
           '', /*25*/
           :old.TIPO, /*26*/
           ''
         );
    end;
 end if;
end inter_tr_OBRF_160_log;
