alter table fatu_060
	add (val_fcp_uf_ncm 		number(15,2),
      perc_fcp_uf_ncm  number(15,2)
	);
 
 alter table fatu_060
	modify (val_fcp_uf_ncm 		default 0.0,
      perc_fcp_uf_ncm  default 0.0
	);
comment on column fatu_060.val_fcp_uf_ncm 		is 'Valor FCP por Uf X NCM';
comment on column fatu_060.perc_fcp_uf_ncm 		is 'Percentual FCP por Uf X NCM';

alter table fatu_050
	add (val_fcp_uf_ncm_nf 		number(15,2)
	);
 
 alter table fatu_050
	modify (val_fcp_uf_ncm_nf 		default 0.0
	);
comment on column fatu_050.val_fcp_uf_ncm_nf 		is 'Valor FCP por Uf X NCM total por NF';

alter table obrf_015
	add (val_fcp_uf_ncm 		number(15,2),
      perc_fcp_uf_ncm  number(15,2)
	);
 
alter table obrf_015
	modify (val_fcp_uf_ncm 		default 0.0,
      perc_fcp_uf_ncm  default 0.0
	);
comment on column obrf_015.val_fcp_uf_ncm 		is 'Valor FCP por Uf X NCM';
comment on column obrf_015.perc_fcp_uf_ncm 		is 'Percentual FCP por Uf X NCM';

alter table obrf_010
	add (val_fcp_uf_ncm_nf 		number(15,2)
	);
 
 alter table obrf_010
	modify (val_fcp_uf_ncm_nf default 0.0
	);
comment on column obrf_010.val_fcp_uf_ncm_nf 		is 'Valor FCP por Uf X NCM total por NF';

/
exec inter_pr_recompile;
