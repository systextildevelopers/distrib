
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_050_EVITACANC" 
before update of situacao_nfisc

on fatu_050
for each row
begin

   if :old.situacao_nfisc = 2 and :new.situacao_nfisc = 1
   then
      raise_application_error(-20000,'O Sistema esta tentando alterar a situacao de uma nota ja cancelada. Favor entrar em contato com a equipe de Atendimento da Intersys.');
   end if;

end inter_tr_fatu_050_evitacanc;

-- ALTER TRIGGER "INTER_TR_FATU_050_EVITACANC" ENABLE
 

/

exec inter_pr_recompile;

