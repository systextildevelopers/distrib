
-- Aproveitamento de Encaixe

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpc_f480', 'Aproveitamento de Encaixe', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpc_f480', 'menu_mp08', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpc_f480', 'menu_mp08', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Aproveitamento de Encaixe'
 where hdoc_036.codigo_programa = 'pcpc_f480'
   and hdoc_036.locale          = 'es_ES';
commit;

-- Cadastro de Ocorrências de Encaixe

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpc_f481', 'Cadastro de Ocorrências de Encaixe', 0,1);


insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpc_f481', 'menu_mp08', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpc_f481', 'menu_mp08', 1, 0, 'S', 'S', 'S', 'S');


update hdoc_036
   set hdoc_036.descricao       = 'Cadastro de Ocorrências de Encaixe'
 where hdoc_036.codigo_programa = 'pcpc_f481'
   and hdoc_036.locale          = 'es_ES';
commit;

-- Cadastro de Ações para Ocorrências de Encaixe

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpc_f482', 'Cadastro de Ações para Ocorrências de Encaixe', 0,1);


insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpc_f482', 'menu_mp08', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpc_f482', 'menu_mp08', 1, 0, 'S', 'S', 'S', 'S');


update hdoc_036
   set hdoc_036.descricao       = 'Cadastro de Ações para Ocorrências de Encaixe'
 where hdoc_036.codigo_programa = 'pcpc_f482'
   and hdoc_036.locale          = 'es_ES';
commit;


