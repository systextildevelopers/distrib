CREATE OR REPLACE TRIGGER INTER_TR_OBRF_081_LOG
   before insert or
          delete or
          update of numero_ordem, sequencia, prodord_nivel99, prodord_grupo,
		prodord_subgrupo, prodord_item, lote, dt_prev_areceber,
		valor_servico, qtde_areceber, sit_areceber, valor_produto,
		prodbase_nivel99, prodbase_grupo, prodbase_subgrupo, prodbase_item,
		nota_saida, serie_saida, seq_nota_saida, qtde_real_ret,
		largura, mlin_kg, mt_quadra, unid_trab,
		ref_forne, preparacao_iniciada, perc_imposto, executa_trigger,
		qtde_pagamento
          on obrf_081
for each row

declare
  v_prodord_nivel99_old   obrf_081.prodord_nivel99%type;
  v_prodord_grupo_old     obrf_081.prodord_grupo%type;
  v_prodord_subgrupo_old  obrf_081.prodord_subgrupo%type;
  v_prodord_item_old      obrf_081.prodord_item%type;
  v_lote_old              obrf_081.lote%type;
  v_dt_prev_areceber_old  obrf_081.dt_prev_areceber%type;
  v_valor_servico_old     obrf_081.valor_servico%type;
  v_qtde_areceber_old     obrf_081.qtde_areceber%type;
  v_sit_areceber_old      obrf_081.sit_areceber%type;
  v_valor_produto_old     obrf_081.valor_produto%type;
  v_nota_saida_old        obrf_081.nota_saida%type;
  v_serie_saida_old       obrf_081.serie_saida%type;
  v_seq_nota_saida_old    obrf_081.seq_nota_saida%type;
  v_qtde_real_ret_old     obrf_081.qtde_real_ret%type;

  v_prodord_nivel99_new   obrf_081.prodord_nivel99%type;
  v_prodord_grupo_new     obrf_081.prodord_grupo%type;
  v_prodord_subgrupo_new  obrf_081.prodord_subgrupo%type;
  v_prodord_item_new      obrf_081.prodord_item%type;
  v_lote_new              obrf_081.lote%type;
  v_dt_prev_areceber_new  obrf_081.dt_prev_areceber%type;
  v_valor_servico_new     obrf_081.valor_servico%type;
  v_qtde_areceber_new     obrf_081.qtde_areceber%type;
  v_sit_areceber_new      obrf_081.sit_areceber%type;
  v_valor_produto_new     obrf_081.valor_produto%type;
  v_nota_saida_new        obrf_081.nota_saida%type;
  v_serie_saida_new       obrf_081.serie_saida%type;
  v_seq_nota_saida_new    obrf_081.seq_nota_saida%type;
  v_qtde_real_ret_new     obrf_081.qtde_real_ret%type;

  v_numero_ordem          obrf_081.numero_ordem%type;
  v_sequencia             obrf_081.sequencia%type;

  v_operacao              varchar(1);
  v_data_operacao         date;
  v_usuario_rede          varchar(20);
  v_maquina_rede          varchar(40);
  v_aplicativo            varchar(20);
  v_nome_programa         hdoc_090.programa%type;
  v_sid                   hdoc_090.sid%type;

  v_executa_trigger      number;

  ws_empresa                    number(3);
  ws_usuario_systextil          varchar2(250);
  ws_locale_usuario             varchar2(5);

begin
   -- INICIO - L¿¿¿gica implementada para controle de gatilho das triggers do banco
   -- para atender a implementa¿¿¿¿¿¿o executada para limpeza da base de dados do cliente
   -- e replica¿¿¿¿¿¿o dos dados para base hist¿¿¿rico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - L¿¿¿gica implementada para controle de gatilho das triggers do banco. (SS.38405)

   if v_executa_trigger = 0
   then
      begin
      -- Grava a data/hora da inser¿¿¿¿¿¿o do registro (log)
      v_data_operacao := sysdate();

      inter_pr_dados_usuario (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                              ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

      v_nome_programa := inter_fn_nome_programa(v_sid);

      --alimenta as vari¿¿¿veis new caso seja insert ou update
      if inserting or updating
      then
         if inserting
         then v_operacao := 'I';
         else v_operacao := 'U';
         end if;

         v_numero_ordem          := :new.numero_ordem;
         v_sequencia             := :new.sequencia;
         v_prodord_nivel99_new   := :new.prodord_nivel99;
         v_prodord_grupo_new     := :new.prodord_grupo;
         v_prodord_subgrupo_new  := :new.prodord_subgrupo;
         v_prodord_item_new      := :new.prodord_item;
         v_lote_new              := :new.lote;
         v_dt_prev_areceber_new  := :new.dt_prev_areceber;
         v_valor_servico_new     := :new.valor_servico;
         v_qtde_areceber_new     := :new.qtde_areceber;
         v_sit_areceber_new      := :new.sit_areceber;
         v_valor_produto_new     := :new.valor_produto;
         v_nota_saida_new        := :new.nota_saida;
         v_serie_saida_new       := :new.serie_saida;
         v_seq_nota_saida_new    := :new.seq_nota_saida;
         v_qtde_real_ret_new     := :new.qtde_real_ret;

      end if; --fim do if inserting or updating

      --alimenta as vari¿¿¿veis old caso seja insert ou update
      if deleting or updating
      then
         if deleting
         then v_operacao := 'D';
         else v_operacao := 'U';
         end if;

         v_numero_ordem          := :old.numero_ordem;
         v_sequencia             := :old.sequencia;
         v_prodord_nivel99_old   := :old.prodord_nivel99;
         v_prodord_grupo_old     := :old.prodord_grupo;
         v_prodord_subgrupo_old  := :old.prodord_subgrupo;
         v_prodord_item_old      := :old.prodord_item;
         v_lote_old              := :old.lote;
         v_dt_prev_areceber_old  := :old.dt_prev_areceber;
         v_valor_servico_old     := :old.valor_servico;
         v_qtde_areceber_old     := :old.qtde_areceber;
         v_sit_areceber_old      := :old.sit_areceber;
         v_valor_produto_old     := :old.valor_produto;
         v_nota_saida_old        := :old.nota_saida;
         v_serie_saida_old       := :old.serie_saida;
         v_seq_nota_saida_old    := :old.seq_nota_saida;
         v_qtde_real_ret_old     := :old.qtde_real_ret;

      end if; --fim do if inserting or updating

      --insere na obrf_081_log o registro

      insert into obrf_081_log
        (numero_ordem,           sequencia,
         operacao,               data_operacao,
         usuario_rede,           maquina_rede,
         aplicativo,             prodord_nivel99_old,
         prodord_grupo_old,      prodord_subgrupo_old,
         prodord_item_old,       lote_old,
         dt_prev_areceber_old,   valor_servico_old,
         qtde_areceber_old,      sit_areceber_old,
         valor_produto_old,      nota_saida_old,
         serie_saida_old,        seq_nota_saida_old,
         qtde_real_ret_old,      prodord_nivel99_new,
         prodord_grupo_new,      prodord_subgrupo_new,
         prodord_item_new,       lote_new,
         dt_prev_areceber_new,   valor_servico_new,
         qtde_areceber_new,      sit_areceber_new,
         valor_produto_new,      nota_saida_new,
         serie_saida_new,        seq_nota_saida_new,
         qtde_real_ret_new,      processo_systextil)
      values
        (v_numero_ordem,          v_sequencia,
         v_operacao,              v_data_operacao,
         v_usuario_rede,          v_maquina_rede,
         v_aplicativo,            v_prodord_nivel99_old,
         v_prodord_grupo_old,     v_prodord_subgrupo_old,
         v_prodord_item_old,      v_lote_old,
         v_dt_prev_areceber_old,  v_valor_servico_old,
         v_qtde_areceber_old,     v_sit_areceber_old,
         v_valor_produto_old,     v_nota_saida_old,
         v_serie_saida_old,       v_seq_nota_saida_old,
         v_qtde_real_ret_old,     v_prodord_nivel99_new,
         v_prodord_grupo_new,     v_prodord_subgrupo_new,
         v_prodord_item_new,      v_lote_new,
         v_dt_prev_areceber_new,  v_valor_servico_new,
         v_qtde_areceber_new,     v_sit_areceber_new,
         v_valor_produto_new,     v_nota_saida_new,
         v_serie_saida_new,       v_seq_nota_saida_new,
         v_qtde_real_ret_new,     v_nome_programa);

      exception
         when OTHERS
         then raise_application_error (-20000, 'N¿¿¿o atualizou a tabela de log da obrf_081.');
      end;
   end if;
end inter_tr_obrf_081_log;

-- ALTER TRIGGER INTER_TR_OBRF_081_LOG ENABLE


/
