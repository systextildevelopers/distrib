--------------------------------------------
-- Erros da Sugestão de tecidos tem peças --
--------------------------------------------
create table pcpt_083 (
	id number(9,0) not null,
  pedido_venda number(9,0) not null,
  artigo varchar2(5) not null,
  observacao varchar2(3000),
  criado_por varchar2(250),
  criado_em timestamp (6) default current_timestamp
);
/

alter table pcpt_083 add constraint pcpt_083_pk primary key (id);
/

create sequence pcpt_083_seq;
/

create or replace trigger  "inter_tr_pcpt_083_pk" 
before insert on pcpt_083
for each row
begin
  :new.id := pcpt_083_seq.nextval;
end;
/

