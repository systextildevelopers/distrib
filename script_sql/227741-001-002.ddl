declare
cursor parametro_c is select codigo_empresa from fatu_500;
tmpInt number;

begin
   for reg_parametro in parametro_c
   loop
   select count(*) into tmpInt
      from fatu_500
      where fatu_500.codigo_empresa = reg_parametro.codigo_empresa;
      
      if(tmpInt = 0)
      then
         begin
            insert into empr_008 (
               codigo_empresa, param, val_str
            ) values (
               reg_parametro.codigo_empresa, 'areceber.comisprevisao', 'N');
         end;
      end if;
   end loop;
   
  commit;
end;
/

exec inter_pr_recompile;
