create table blocok_210 (
	COD_EMPRESA             number(3),
	ANO_PERIODO_APUR        number(4),
	MES_PERIODO_APUR        number(2),
	
	SEQ_ID_O                number(15),
	DATA_MOVIMENTO          date,

	DT_INI_OS               varchar2(8),
	DT_FIN_OS               varchar2(8),
	COD_DOC_OS              varchar2(30),
	COD_ITEM_ORI            varchar2(60),
	QTD_ORI                 number(15,6),
	
	data_hora_ins           date            default sysdate
);
