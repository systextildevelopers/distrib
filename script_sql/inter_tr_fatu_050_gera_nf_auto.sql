CREATE OR REPLACE TRIGGER INTER_TR_FATU_050_GERA_NF_AUTO
BEFORE UPDATE OF cod_status
ON fatu_050
FOR EACH ROW

DECLARE
   ws_usuario_rede               varchar2(20);
   ws_maquina_rede              varchar2(40);
   ws_aplicativo                     varchar2(20);
   ws_sid                                number(9);
   ws_empresa                       number(3);
   ws_usuario_systextil          varchar2(250);
   ws_locale_usuario              varchar2(5);
   
   v_codigo_empresa              fatu_500.codigo_empresa%TYPE;
   v_codigo_matriz                   fatu_500.codigo_matriz%TYPE;
   v_cod_cidade                      fatu_500.codigo_cidade%TYPE;
   v_cla_cont_tran_e               fatu_500.cla_cont_tran_e%TYPE;   
   v_cgc_9                              fatu_500.cgc_9%TYPE;
   v_cgc_4                              fatu_500.cgc_4%TYPE;
   v_cgc_2                              fatu_500.cgc_2%TYPE;
   v_cfop_entrada                   obrf_260.cfop_entrada%TYPE;
   v_confirma_automatico       obrf_260.confirma_automatico%TYPE;
   v_atualiza_estoque             estq_005.atualiza_estoque%TYPE;
   v_estado                             basi_160.estado%TYPE;
   v_deposito                          fatu_060.deposito%TYPE;
   v_dep_destino                     basi_208.deposito_destino%TYPE;
   v_ind_altera_situacao_vol   fatu_504.ind_altera_situacao_vol%TYPE;
   v_nr_solicitacao                  NUMBER;
   v_tp_reg_relatorio               NUMBER;
   v_cod_relatorio                   VARCHAR2(10);
   v_usuario                            VARCHAR2(250);
   v_codigo_transacao            pedi_080.codigo_transacao%TYPE;
   v_hist_contabil                     pedi_080.hist_contabil%TYPE;
   v_cvf_cofins                        pedi_080.cvf_cofins%TYPE;
   v_perc_icms                        pedi_080.perc_icms%TYPE;
   v_cvf_pis                             pedi_080.cvf_pis%TYPE;
   v_perc_diferenc                   pedi_080.perc_diferenc%TYPE;
   v_exercicio_doc                  NUMBER;


BEGIN

   -- Quando uma nota fiscal for autorizada na receita gerar automaticamente a nota de entrada na loja atraves do programa obrf_f200.

   IF :OLD.cod_status <> '100' AND :NEW.cod_status = '100'
   THEN

      BEGIN
         SELECT codigo_empresa,    codigo_cidade,
                       codigo_matriz,         cla_cont_tran_e
         INTO v_codigo_empresa,      v_cod_cidade, 
                  v_codigo_matriz,          v_cla_cont_tran_e
         FROM fatu_500
         WHERE fatu_500.cgc_9 = :new.cgc_9
               AND fatu_500.cgc_4 = :new.cgc_4
               AND fatu_500.cgc_2 = :new.cgc_2
			   AND ROWNUM         = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN  v_codigo_empresa := 0;
      END;

      IF v_codigo_empresa > 0
      THEN

         /*SELECT estado
         INTO v_estado
         FROM basi_160
         WHERE cod_cidade = v_cod_cidade;*/
         
         select basi_160.estado
         INTO v_estado
         from fatu_500
         inner join basi_160
         on basi_160.cod_cidade = fatu_500.codigo_cidade
         where fatu_500.codigo_empresa = :new.codigo_empresa;

         BEGIN
            SELECT cfop_entrada, confirma_automatico
            INTO v_cfop_entrada, v_confirma_automatico
            FROM obrf_260
            WHERE cod_empresa                  = v_codigo_empresa
                  AND cfop_saida               = :new.natop_nf_nat_oper
                  AND estado_natoper_saida     = v_estado
                  AND estado_natoper_entrada   = :new.natop_nf_est_oper
                  AND confirma_automatico      = 'S';
         EXCEPTION
            WHEN NO_DATA_FOUND THEN v_confirma_automatico := 'N';
         END;

         IF v_confirma_automatico = 'S'
         THEN
            BEGIN
               SELECT atualiza_estoque
               INTO v_atualiza_estoque
               FROM estq_005, pedi_080
               WHERE estq_005.codigo_transacao  = pedi_080.codigo_transacao
                     AND pedi_080.natur_operacao      = v_cfop_entrada
                     AND pedi_080.estado_natoper      = :new.natop_nf_est_oper;

               -- Sempre buscar o deposito do primeiro item?
               begin
                 SELECT deposito
                 INTO v_deposito
                 FROM fatu_060
                 WHERE ch_it_nf_cd_empr  = :new.codigo_empresa
                       AND ch_it_nf_num_nfis  = :new.num_nota_fiscal
                       AND ch_it_nf_ser_nfis   = :new.serie_nota_fisc
                       AND ROWNUM              = 1;
               exception 
               when no_data_found then v_deposito := 0;
               end;

               begin
                 SELECT deposito_destino
                 INTO v_dep_destino
                 FROM basi_208
                 WHERE empresa_destino = v_codigo_empresa
                       AND deposito_origem  = v_deposito;
               exception 
               when no_data_found then v_dep_destino := 0;
               end;
                  
             -- RAISE_APPLICATION_ERROR (-20201, 'teste trigger ' || v_deposito || ' - ' || v_dep_destino );

               IF (v_atualiza_estoque = 1 AND v_deposito > 0 AND v_dep_destino > 0) OR (v_deposito = 0 AND v_atualiza_estoque = 2)
               THEN

                  SELECT ind_altera_situacao_vol
                  INTO v_ind_altera_situacao_vol
                  FROM fatu_504
                  WHERE codigo_empresa = :new.codigo_empresa;

                  SELECT seq_relatorio.nextval
                  INTO v_nr_solicitacao
                  FROM dual;

                  SELECT codigo_transacao, hist_contabil, cvf_cofins,
                                 perc_icms,             cvf_pis,         perc_diferenc
                  INTO v_codigo_transacao, v_hist_contabil, v_cvf_cofins,
                           v_perc_icms,             v_cvf_pis,         v_perc_diferenc
                  FROM pedi_080
                  WHERE pedi_080.natur_operacao = v_cfop_entrada
                        AND pedi_080.estado_natoper = :new.natop_nf_est_oper;
                           
                 SELECT cgc_9, cgc_4, cgc_2
			           INTO v_cgc_9, v_cgc_4, v_cgc_2
			           FROM fatu_500
			           WHERE codigo_empresa = :new.codigo_empresa;
                
                  -- Dados do usuario logado
                  inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,            ws_sid,
                                                         ws_usuario_systextil,  ws_empresa,            ws_locale_usuario);
                           
                  v_exercicio_doc := inter_fn_checa_data(v_codigo_empresa, sysdate, 0);
					 
			   -- Na variavel v_tp_reg_relatorio deve receber INTER_FN_GET_BATCH_CODE(), mas como ha uma indefinic?o sobre a liberac?o desta func?o, foi deixado fixo -8 ate ser resolvido.
                  v_tp_reg_relatorio := -8;
                  v_usuario := ws_usuario_systextil;
                  v_cod_relatorio := substr(v_usuario, 1, 3) || '' || v_nr_solicitacao;
                     
                  --RAISE_APPLICATION_ERROR (-20201, 'teste trigger');
                     
                  INSERT INTO oper_001 (
                               codigo_empresa,                                   tp_reg_relatorio,
                               classe,                                                  locale,
                               solicitante,                                            status_processo,
                               data_solicitacao,                                  data_execucao,
                               nr_solicitacao,                                      codigo_relatorio,
                               campo_01,                                           campo_02,
                               campo_03,                                           campo_04,
                               campo_132,                                         campo_109,
                               campo_110,                                         campo_111,
                               campo_112,                                         campo_113,
                               campo_114,                                         campo_115,
                               campo_116,                                         campo_117,
                               campo_118,                                         campo_133,
                               campo_10,                                           campo_119,
                               campo_134,                                         campo_135,
                               campo_05,                                           campo_120,
                               campo_136,                                         campo_101,
                               campo_102,                                         campo_137,
                               campo_103,                                         campo_121,
                               campo_104,                                         campo_105,
                               campo_106,                                         campo_107,
                               campo_108,                                         campo_122,
                               campo_123,                                         campo_124,
                               campo_90,                                           campo_85,
                               campo_99,                                           campo_86
                  ) VALUES (
                               v_codigo_empresa,                                                v_tp_reg_relatorio,
                               'br.com.intersys.systextil.batch.obrf.obrf_f200',      'pt_BR',
                               v_usuario,                                                              0,
                               sysdate,                                                                 sysdate,
                               v_nr_solicitacao,                                                    v_cod_relatorio,
                               v_dep_destino,                                                      9999,
                               :new.codigo_empresa,                                           v_codigo_empresa,
                               :new.serie_nota_fisc,                                             :new.num_nota_fiscal,
                               0,                                                                           0,
                               0,                                                                           0,
                               0,                                                                           0,
                               0,                                                                           0,
                               0,                                                                           1,
                               sysdate,                                                                 0,
                               v_estado,                                            :new.natop_nf_est_oper,
                               0,                                                                           0,
                               v_ind_altera_situacao_vol,                                    0,
                               0,                                                                           'N',
                               v_exercicio_doc,                                                   0,
                               v_cgc_9,                                                               v_cgc_4,
                               v_cgc_2,                                                               v_codigo_matriz,
                               v_codigo_transacao,                                             v_hist_contabil,
                               v_cvf_cofins,                                                         v_cla_cont_tran_e,
                               v_perc_icms,                                                         v_cvf_pis,
                               'N',                                                                         v_perc_diferenc
                  );
               END IF;
            END;
         END IF;
      END IF;
   END IF;
END INTER_TR_FATU_050_GERA_NF_AUTO;
