create table obrf_341 (
    id                 number(9)    not null,
    codigo_empresa     number(3)    not null,
    nota_fiscal        number(9)    not null,
    serie              varchar2(3)  not null,
    informa_frete      varchar2(1)  default 0 not null,
    status             number(1)    default 0 not null,
    nr_doc_entrada     number(9),
    serie_doc_entrada  varchar2(3),
    cgc9_doc_entrada   number(9),
    cgc4_doc_entrada   number(4),
    cgc2_doc_entrada   number(2),
    status_devolucao   varchar2(4000),
    data_integracao    date         default sysdate not null,
    data_processamento date
);

alter table obrf_341 add constraint pk_obrf_341 primary key (id);

comment on table obrf_341 is 'Integra��o Devolu��o E-Commerce - Capa';
comment on column obrf_341.status is '0 - Pendente / 1 - Processada';

create sequence id_obrf_341;

exec inter_pr_recompile;
/
