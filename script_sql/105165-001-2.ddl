create table obrf_059 (
    codigo_empresa                number(3) default 0,
    mes_referencia                number(2) default 0,
    ano_referencia                number(4) default 0,
    ii_tipo_declaracao            number(1) default 1,
    cod_tipo_declaracao           number(1) default 0,   
    val_base_calc_saida_cp        numeric(13,2) default 0.0,
    deb_relat_saida_cp            numeric(13,2) default 0.0,
    cp_util_subs_cred_entradas    numeric(13,2) default 0.0,
    sald_dev_app_cp_mes           numeric(13,2) default 0.0,
    dep_app_extemp_cp             numeric(13,2) default 0.0,
    sc_ant_mseg_apm_ant           numeric(13,2) default 0.0,
    cdpa_icms_dsaida_subimp       numeric(13,2) default 0.0,
    tot_antecipacoes              numeric(13,2) default 0.0,
    imp_rec_util_cp               numeric(13,2) default 0.0,
    saldo_cd_ant_mseg             numeric(13,2) default 0.0
);

alter table obrf_059 add constraint pk_obrf_059 primary key (
    codigo_empresa, 
    ano_referencia, 
    mes_referencia, 
    ii_tipo_declaracao, 
    cod_tipo_declaracao
);

comment on column  obrf_059.codigo_empresa is 'c�digo da empresa';
comment on column  obrf_059.mes_referencia is 'mes de refer�ncia';
comment on column  obrf_059.ano_referencia is 'ano de refer�ncia';
comment on column  obrf_059.val_base_calc_saida_cp is 'valor da base de c�lculo das sa�das com cr�dito presumido';
comment on column  obrf_059.deb_relat_saida_cp is '(+) d�bitos relativos �s sa�das com cr�dito presumido em substitui��o aos cr�ditos pelas entradas';
comment on column  obrf_059.cp_util_subs_cred_entradas is '(-) cr�dito presumido�utilizado em substitui��o aos cr�ditos pelas entradas';
comment on column  obrf_059.sald_dev_app_cp_mes is '(=) saldo devedor apurado pela apropria��o do cr�dito presumido no m�s';
comment on column  obrf_059.dep_app_extemp_cp is '(=) d�bito apurado pela apropria��o extempor�nea do cr�dito presumido';
comment on column  obrf_059.sc_ant_mseg_apm_ant is '(+) saldo credor das antecipa��es para o m�s seguinte apurado no m�s anterior';
comment on column  obrf_059.cdpa_icms_dsaida_subimp is '(+) cr�dito decorrente do pagamento antecipado do icms devido na sa�da subsequente � importa��o, com utiliza��o de cr�dito presumido cdpa_icms_dsaida_subimp';
comment on column  obrf_059.tot_antecipacoes is '(=) total das antecipa��es';
comment on column  obrf_059.imp_rec_util_cp is '(=) imposto a recolher pela�utiliza��o do cr�dito presumido';
comment on column  obrf_059.saldo_cd_ant_mseg is '(=)�saldo credor das antecipa��es para o m�s seguinte';

exec inter_pr_recompile;
/
