alter table rcnb_750
  drop constraint pk_rcnb_750;

drop index pk_rcnb_750;

alter table rcnb_750 drop column id_execucao;
alter table rcnb_755 drop column id_execucao;

alter table rcnb_750 add id_execucao varchar2(20) default 0;
alter table rcnb_755 add id_execucao varchar2(20) default 0;

exec inter_pr_recompile;
