create or replace PACKAGE BODY ST_PCK_BASI AS
    
    function exists_produto(p_nivel_estrutura varchar2, p_grupo_estrutura varchar2, p_subgru_estrutura varchar2, p_item_estrutura varchar2) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM basi_010 
        WHERE   nivel_estrutura = p_nivel_estrutura
        AND     grupo_estrutura = p_grupo_estrutura
        AND     subgru_estrutura = p_subgru_estrutura
        AND     item_estrutura = p_item_estrutura;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_produto;
    
    function get_produto_by_integracao(p_produto_integracao varchar2) return t_produto 
    AS
        v_produto t_produto;
    BEGIN
        BEGIN
            SELECT
                nivel_estrutura,
                grupo_estrutura,
                subgru_estrutura,
                item_estrutura
            INTO
                v_produto.nivel_estrutura,
                v_produto.grupo_estrutura,
                v_produto.subgru_estrutura,
                v_produto.item_estrutura
            FROM basi_010
            WHERE produto_integracao = p_produto_integracao; 
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_produto.nivel_estrutura := null;
                    v_produto.grupo_estrutura := null;
                    v_produto.subgru_estrutura := null;
                    v_produto.item_estrutura := null;
        END;    
        RETURN v_produto;
    END get_produto_by_integracao;

    function exists_centro_custo(p_centro_custo number, p_codigo_empresa number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM basi_185 
        WHERE centro_custo = p_centro_custo
        AND   local_entrega = p_codigo_empresa;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_centro_custo;
    
	function is_centro_custo_atv(p_centro_custo number, p_codigo_empresa number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT 1
        INTO v_exists
        FROM basi_185 
        WHERE centro_custo = p_centro_custo
		AND   situacao <> 1
        AND   local_entrega = p_codigo_empresa;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END is_centro_custo_atv;
	
    function exists_deposito(p_codigo_deposito number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM basi_205 
        WHERE codigo_deposito = p_codigo_deposito;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_deposito;
    
    function get_empresa_by_deposito(p_codigo_deposito number) return number 
    AS
        v_local_deposito number;
    BEGIN
        SELECT
            local_deposito
        INTO
            v_local_deposito
        FROM basi_205
        WHERE codigo_deposito = p_codigo_deposito;
        
        RETURN v_local_deposito;
        EXCEPTION
            WHEN OTHERS THEN
                return 0;      
    END get_empresa_by_deposito;
    
    function exists_moeda_by_cod(p_codigo number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM basi_265 
        WHERE codigo_moeda = p_codigo;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_moeda_by_cod;

    function get_descr_moeda(p_codigo number) return varchar2
    AS
        v_descr_moeda varchar2(20);
    begin  
        select 
            basi_265.descricao
        INTO 
            v_descr_moeda
        from basi_265
        where 
        basi_265.codigo_moeda = p_codigo;
        return v_descr_moeda;
    end get_descr_moeda;    

    function get_simbolo_moeda(p_codigo number) return varchar2
        AS
            v_simbolo_moeda varchar2(5);
    begin  
        select 
            basi_265.simbolo_moeda
        INTO 
            v_simbolo_moeda
        from basi_265
        where 
        basi_265.codigo_moeda = p_codigo;
        return v_simbolo_moeda;
    end get_simbolo_moeda;
END ST_PCK_BASI;
