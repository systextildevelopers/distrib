create table oper_282 (
	cod_empresa    number(3)          default 0 not null,
	cfop           varchar2(5 byte)   default ' ' not null
);

alter table oper_282 add constraint pk_oper_282 primary key(cod_empresa, cfop);

exec inter_pr_recompile;
