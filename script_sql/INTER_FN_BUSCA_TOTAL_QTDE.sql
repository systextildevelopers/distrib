create or replace function "INTER_FN_BUSCA_TOTAL_QTDE"
                        (p_ped_compra in NUMBER)
return 
    NUMBER
is

    v_return  number;
begin

    select  NVL(SUM(qtde_pedida_item),0) AS QTDE_TOTAL_PED
    into v_return
    from supr_100
    where supr_100.num_ped_compra = p_ped_compra;

    return v_return;
end;


/

exec inter_pr_recompile;
