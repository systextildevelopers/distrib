
  CREATE OR REPLACE TRIGGER "INTER_TR_MQOP_010_HIST" 
AFTER INSERT OR
      DELETE OR
      UPDATE of grupo_maquina, nome_grupo_maq, un_med_capacid, man_automatica,
	tem_eficiencia, maquina_critica, tipo_carga, qtde_maq_simul,
	tipo_operacao, permite_ordem_varias_maquinas, permite_ignorar_apontamentos, programacao_automatica,
	tipo_maquina, prioridade, perc_variacao, codigo_estagio
      on mqop_010
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_area_producao          number(1);
   ws_ordem_producao         number(9);
   ws_periodo_producao       number(4);
   ws_ordem_confeccao        number(2);
   ws_tipo_historico         number(1);

   ws_descricao_historico    varchar2(4000);
   ws_tipo_ocorr             varchar2(1);

   ws_grupo_maquina          varchar2(5);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- HISTORICO DE BENEFICIAMENTO ATIVO
   if INSERTING then
      ws_area_producao         := 0;
      ws_ordem_producao        := 0;
      ws_periodo_producao      := 0;
      ws_ordem_confeccao       := 0;
      ws_tipo_historico        := 1;
      ws_grupo_maquina         := :new.grupo_maquina;


      ws_descricao_historico   := inter_fn_buscar_tag('lb36668#INCLUSAO MAQUINAS (MQOP_010)',ws_locale_usuario,ws_usuario_systextil)
                                ||chr(10)
                                ||inter_fn_buscar_tag('lb06026#GRUPO MAQUINA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.grupo_maquina
                                ||chr(10)
                                ||inter_fn_buscar_tag('lb00640#DESCRICAO MAQUINA:',ws_locale_usuario,ws_usuario_systextil)  || ' ' || :new.nome_grupo_maq
                                ||chr(10)
                                ||inter_fn_buscar_tag('lb36660#UNIDADE MEDIDA CAPAC.:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.un_med_capacid
                                ||chr(10)
                                ||inter_fn_buscar_tag('lb36661#MANUTENCAO AUTOMATICA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.man_automatica
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36662#TEM EFICIENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tem_eficiencia
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36663#MAQUINA CRITICA: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.maquina_critica
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36664#TIPO CARGA: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tipo_carga
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36665#QTDE MAQUINA SIMUL: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.qtde_maq_simul
                                ||chr(10)
                                || inter_fn_buscar_tag('lb06660#TIPO OPERACAO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tipo_operacao
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36666#PERMITE ORDEM VARIAS MAQUINAS: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.permite_ordem_varias_maquinas
                                ||chr(10)
                                || inter_fn_buscar_tag('lb10518#PERMITE IGNORAR APONTAMENTO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.permite_ignorar_apontamentos
                                ||chr(10)
                                || inter_fn_buscar_tag('lb10111#PROGRAMACAO AUTOMATICA: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.programacao_automatica
                                ||chr(10)
                                || inter_fn_buscar_tag('lb11400#TIPO MAQUINA: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tipo_maquina
                                ||chr(10)
                                || inter_fn_buscar_tag('lb00876#PRIORIDADE: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.prioridade
                                ;
      ws_tipo_ocorr            := 'I';
    elsif DELETING then
         ws_area_producao         := 0;
         ws_ordem_producao        := 0;
         ws_periodo_producao      := 0;
         ws_ordem_confeccao       := 0;
         ws_tipo_historico        := 1;
         ws_grupo_maquina         := :old.grupo_maquina;


         ws_descricao_historico   := inter_fn_buscar_tag('lb36669#EXCLUSAO MAQUINAS (MQOP_010)',ws_locale_usuario,ws_usuario_systextil)
                                ||chr(10)
                                ||inter_fn_buscar_tag('lb06026#GRUPO MAQUINA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.grupo_maquina
                                ||chr(10)
                                ||inter_fn_buscar_tag('lb00640#DESCRICAO MAQUINA:',ws_locale_usuario,ws_usuario_systextil)  || ' ' || :old.nome_grupo_maq
                                ||chr(10)
                                ||inter_fn_buscar_tag('lb36660#UNIDADE MEDIDA CAPAC.:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.un_med_capacid
                                ||chr(10)
                                ||inter_fn_buscar_tag('lb36661#MANUTENCAO AUTOMATICA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.man_automatica
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36662#TEM EFICIENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.tem_eficiencia
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36663#MAQUINA CRITICA: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.maquina_critica
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36664#TIPO CARGA: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.tipo_carga
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36665#QTDE MAQUINA SIMUL: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.qtde_maq_simul
                                ||chr(10)
                                || inter_fn_buscar_tag('lb06660#TIPO OPERACAO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.tipo_operacao
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36666#PERMITE ORDEM VARIAS MAQUINAS: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.permite_ordem_varias_maquinas
                                ||chr(10)
                                || inter_fn_buscar_tag('lb10518#PERMITE IGNORAR APONTAMENTO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.permite_ignorar_apontamentos
                                ||chr(10)
                                || inter_fn_buscar_tag('lb10111#PROGRAMACAO AUTOMATICA: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.programacao_automatica
                                ||chr(10)
                                || inter_fn_buscar_tag('lb11400#TIPO MAQUINA: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.tipo_maquina
                                ||chr(10)
                                || inter_fn_buscar_tag('lb00876#PRIORIDADE: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.prioridade;
         ws_tipo_ocorr            := 'D';
    elsif UPDATING then
         ws_area_producao         := 0;
         ws_ordem_producao        := 0;
         ws_periodo_producao      := 0;
         ws_ordem_confeccao       := 0;
         ws_tipo_historico        := 1;
         ws_grupo_maquina         := :new.grupo_maquina;


         ws_descricao_historico   := inter_fn_buscar_tag('lb36670#ATUALIZACAO MAQUINA (MQOP_010)',ws_locale_usuario,ws_usuario_systextil)
                                ||chr(10)
                                ||inter_fn_buscar_tag('lb06026#GRUPO MAQUINA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.grupo_maquina
                                                                                                                         || '->' || :new.grupo_maquina
                                ||chr(10)
                                ||inter_fn_buscar_tag('lb00640#DESCRICAO MAQUINA:',ws_locale_usuario,ws_usuario_systextil)  || ' ' || :old.nome_grupo_maq
                                                                                                                            || '->' || :new.nome_grupo_maq
                                ||chr(10)
                                ||inter_fn_buscar_tag('lb36660#UNIDADE MEDIDA CAPAC.:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.un_med_capacid
                                                                                                                               || '->' || :new.un_med_capacid
                                ||chr(10)
                                ||inter_fn_buscar_tag('lb36661#MANUTENCAO AUTOMATICA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.man_automatica
                                                                                                                               || '->' || :new.man_automatica
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36662#TEM EFICIENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.tem_eficiencia
                                                                                                                         || '->' || :new.tem_eficiencia
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36663#MAQUINA CRITICA: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.maquina_critica
                                                                                                                           || '->' || :new.maquina_critica
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36664#TIPO CARGA: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.tipo_carga
                                                                                                                      || '->' || :new.tipo_carga
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36665#QTDE MAQUINA SIMUL: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.qtde_maq_simul
                                                                                                                              || '->' || :new.qtde_maq_simul
                                ||chr(10)
                                || inter_fn_buscar_tag('lb06660#TIPO OPERACAO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.tipo_operacao
                                                                                                                         || '->' || :new.tipo_operacao
                                ||chr(10)
                                || inter_fn_buscar_tag('lb36666#PERMITE ORDEM VARIAS MAQUINAS: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.permite_ordem_varias_maquinas
                                                                                                                                         || '->' || :new.permite_ordem_varias_maquinas
                                ||chr(10)
                                || inter_fn_buscar_tag('lb10518#PERMITE IGNORAR APONTAMENTO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.permite_ignorar_apontamentos
                                                                                                                                       || '->' || :new.permite_ignorar_apontamentos
                                ||chr(10)
                                || inter_fn_buscar_tag('lb10111#PROGRAMACAO AUTOMATICA: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.programacao_automatica
                                                                                                                                  || '->' || :new.programacao_automatica
                                ||chr(10)
                                || inter_fn_buscar_tag('lb11400#TIPO MAQUINA: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.tipo_maquina
                                                                                                                        || '->' || :new.tipo_maquina
                                ||chr(10)
                                || inter_fn_buscar_tag('lb00876#PRIORIDADE: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.prioridade
                                                                                                                      || '->' || :new.prioridade;
         ws_tipo_ocorr            := 'A';
   end if;
    INSERT INTO hist_010
     (area_producao,           ordem_producao,          periodo_producao,
      ordem_confeccao,         tipo_historico,          descricao_historico,
      tipo_ocorr,              data_ocorr,              hora_ocorr,
      usuario_rede,            maquina_rede,            aplicacao,
      usuario_sistema,         grupo_maquina)
   VALUES
     (ws_area_producao,        ws_ordem_producao,       ws_periodo_producao,
      ws_ordem_confeccao,      ws_tipo_historico,       ws_descricao_historico,
      ws_tipo_ocorr,           sysdate,                 sysdate,
      ws_usuario_rede,         ws_maquina_rede,         ws_aplicativo,
      ws_usuario_systextil,    ws_grupo_maquina);
end inter_tr_pcpb_015_hist;

-- ALTER TRIGGER "INTER_TR_MQOP_010_HIST" ENABLE
 

/

exec inter_pr_recompile;

