create table efic_300(
    id number(4),
    constraint efic_300_pk primary key (id),
    descricao varchar2(120),
	classificacao varchar2(1));

comment on column efic_300.id is 'Identificador do tipo de não conformidade';
comment on column efic_300.descricao is 'Descrição do tipo de não conformidade';
comment on column efic_300.classificacao is 'Classificação do tipo de não conformidade';
comment on table efic_300 is 'Tabela de cadastro de tipos de não conformidades';

/
