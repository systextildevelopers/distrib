create or replace trigger wms_tr_inte_wms_rfid_tags
before delete on inte_wms_rfid_tags
for each row
begin

   -- Se flag liberar_tag_st = 0 deve gera uma mensagem
   if :old.liberar_tag_st = 0
   then Raise_application_error(-20000, 'Esta TAG n�o poder� ser eliminada, aguarde at� que o WMS altere esta situa��o. ');
   end if;

   -- Se flag liberar_tag_st = 2 deve limpar o c�digo da caixa Rfid da TAG
   if :old.liberar_tag_st = 2
   then
      update pcpc_330
      set pcpc_330.cod_caixa_rfid = null
      where pcpc_330.periodo_producao = :old.periodo_tag
        and pcpc_330.ordem_producao   = :old.ordem_prod_tag
        and pcpc_330.ordem_confeccao  = :old.ordem_conf_tag
        and pcpc_330.sequencia        = :old.sequencia_tag;
   end if;

   delete from inte_wms_rfid_tags_espelho
   where inte_wms_rfid_tags_espelho.rfid_caixa = :old.rfid_caixa
     and inte_wms_rfid_tags_espelho.periodo_tag = :old.periodo_tag
     and inte_wms_rfid_tags_espelho.ordem_prod_tag = :old.ordem_prod_tag
     and inte_wms_rfid_tags_espelho.ordem_conf_tag = :old.ordem_conf_tag
     and inte_wms_rfid_tags_espelho.sequencia_tag = :old.sequencia_tag;

   insert into inte_wms_rfid_tags_espelho
     (rfid_caixa,
      periodo_tag,
      ordem_prod_tag,
      ordem_conf_tag,
      sequencia_tag,
      liberar_tag_st,
      codigo_unico_tag)
   values
     (:old.rfid_caixa,
      :old.periodo_tag,
      :old.ordem_prod_tag,
      :old.ordem_conf_tag,
      :old.sequencia_tag,
      :old.liberar_tag_st,
      :old.codigo_unico_tag);    

end wms_tr_inte_wms_rfid_tags;

/

exec inter_pr_recompile;
/* versao: 2 */


 exit;
