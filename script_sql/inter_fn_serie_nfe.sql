
  CREATE OR REPLACE FUNCTION "INTER_FN_SERIE_NFE" (p_cod_empresa in number, p_serie in varchar2) return number is
  v_serie_nfe int;
begin

  v_serie_nfe := 0;

  begin
     select 0
     into   v_serie_nfe
     from fatu_505
     where fatu_505.codigo_empresa  = p_cod_empresa
       and fatu_505.serie_nota_fisc = p_serie
       and fatu_505.serie_nfe       = 'S';
  exception
      when no_data_found then
         v_serie_nfe := 1;
  end;

  return(v_serie_nfe);
end INTER_FN_SERIE_NFE;

 

/

exec inter_pr_recompile;

