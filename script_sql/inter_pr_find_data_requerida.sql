
  CREATE OR REPLACE PROCEDURE "INTER_PR_FIND_DATA_REQUERIDA" (f_ordem_producao number,
                                                         f_estagio number,
                                                         f_tipo_alocacao number,
                                                         f_tipo_recurso number,
                                                         data_requerida out date)
is
   v_existe_reg  integer;
   v_existe_reg2 integer;
   v_existe_reg3 integer;
   v_existe_reg4 integer;
begin

   data_requerida := null;

   begin
      select  min(tmrp_650.data_inicio)
      into data_requerida
      from tmrp_650
      where tmrp_650.tipo_alocacao   = f_tipo_alocacao /* 1-Confeccao, 2-Beneficiamento, 4-Tecelagem */
        and tmrp_650.tipo_recurso    = f_tipo_recurso /* 1 - Maquina, 5 - Celula */
        and tmrp_650.ordem_trabalho  = f_ordem_producao
        and tmrp_650.codigo_estagio  = f_estagio
        and tmrp_650.data_inicio is not null;
   exception when others then
      data_requerida := null;
   end;

end inter_pr_find_data_requerida;

 

/

exec inter_pr_recompile;

