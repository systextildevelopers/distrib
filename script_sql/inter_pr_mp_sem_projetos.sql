
  CREATE OR REPLACE PROCEDURE "INTER_PR_MP_SEM_PROJETOS" 
   (p_nr_solicitacao  in number,
    p_codigo_usuario  in number,
    p_usuario         in varchar2,
    p_empresa_logada  in number,
    p_empresa_filtro  in number,
    p_nome_programa   in varchar2,
    p_visualiza_mp    in number,
    p_inc_exc_dep     in number,
    p_cons_transito   in number,
    p_nivel_material  in varchar2,
    p_tipo_material   in number,
    p_inc_exc_produto in number)
 is

   TYPE nivelEstq IS TABLE OF basi_030.nivel_estrutura%TYPE;
   p_nivel_estq nivelEstq;

   TYPE grupoEstq IS TABLE OF basi_030.referencia%TYPE;
   p_grupo_estq grupoEstq;

   TYPE subgrupoEstq IS TABLE OF basi_020.tamanho_ref%TYPE;
   p_sub_estq subgrupoEstq;

   TYPE itemEstq IS TABLE OF basi_010.item_estrutura%TYPE;
   p_item_estq itemEstq;

/*   TYPE qtdeEstqAtu IS TABLE OF estq_040.qtde_estoque_atu%TYPE;
   p_qtde_estoque_atu qtdeEstqAtu;
*/

   p_qtde_estoque_atu number;
   p_qtde_reservada   tmrp_041.qtde_reservada%TYPE;
   p_qtde_programado  tmrp_041.qtde_areceber%TYPE;
   p_qtde_disponivel  estq_040.qtde_estoque_atu%TYPE;
   p_qtde_transito    pcpt_020.qtde_quilos_acab%TYPE;

BEGIN

   /*Busca itens que nao foram gravados na TMRP_615*/
   begin
      select  /*parallel(estq_040, basi_030, 20)*/
             estq_040.cditem_nivel99,  estq_040.cditem_grupo,
             estq_040.cditem_subgrupo, estq_040.cditem_item
      BULK COLLECT INTO
             p_nivel_estq,             p_grupo_estq,
             p_sub_estq,               p_item_estq
        from estq_040, basi_030
       where estq_040.cditem_nivel99 = basi_030.nivel_estrutura
         and estq_040.cditem_grupo   = basi_030.referencia
         and estq_040.deposito in (select basi_205.codigo_deposito
                                     from basi_205
                                    where basi_205.local_deposito = p_empresa_filtro)
         and not exists (select 1 from tmrp_615
                          where tmrp_615.nome_programa  = 'tmrp_f025'
                            and tmrp_615.nr_solicitacao = p_nr_solicitacao
                            and tmrp_615.codigo_usuario = p_codigo_usuario
                            and tmrp_615.tipo_registro  = 255
                            and tmrp_615.usuario        = p_usuario
                            and tmrp_615.codigo_empresa = p_empresa_logada
                            and tmrp_615.tipo_natureza  = 1
                            and tmrp_615.nivel          = estq_040.cditem_nivel99
                            and tmrp_615.grupo          = estq_040.cditem_grupo
                            and tmrp_615.subgrupo       = estq_040.cditem_subgrupo
                            and tmrp_615.item           = estq_040.cditem_item)
            and (estq_040.cditem_nivel99  = p_nivel_material
             or ((estq_040.cditem_nivel99 = '2' or estq_040.cditem_nivel99 = '9') and p_nivel_material = '0'))
         and (basi_030.comprado_fabric = p_tipo_material or p_tipo_material = 3)

              /* PRODUTO */
              and ((p_inc_exc_produto = 1
              and   exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060       /* INCLUSAO */
                            where rcnb_060.tipo_registro        = 599
                              and rcnb_060.nr_solicitacao       = 25
                              and rcnb_060.subgru_estrutura     = p_codigo_usuario
                              and rcnb_060.usuario_rel          = p_usuario
                              and rcnb_060.empresa_rel          = p_empresa_logada
                              and rcnb_060.relatorio_rel        = 'tmrp_f025'
                              and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                              and rcnb_060.nivel_estrutura_str  in (estq_040.cditem_nivel99, 'X')
                              and rcnb_060.grupo_estrutura_str  in (estq_040.cditem_grupo, 'XXXXXX')))
              or  (p_inc_exc_produto = 2
              and  not exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060   /* EXCLUSAO */
                               where rcnb_060.tipo_registro        = 599
                                 and rcnb_060.nr_solicitacao       = 25
                                 and rcnb_060.subgru_estrutura     = p_codigo_usuario
                                 and rcnb_060.usuario_rel          = p_usuario
                                 and rcnb_060.empresa_rel          = p_empresa_logada
                                 and rcnb_060.relatorio_rel        = 'tmrp_f025'
                                 and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                                 and rcnb_060.nivel_estrutura_str  in (estq_040.cditem_nivel99,'X')
                                 and (rcnb_060.grupo_estrutura_str = estq_040.cditem_grupo
                                 or   rcnb_060.grupo_estrutura_str = 'XXXXXX'))
                                  or exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060
                         where rcnb_060.tipo_registro        = 599
                           and rcnb_060.nr_solicitacao       = 25
                           and rcnb_060.subgru_estrutura     = p_codigo_usuario
                           and rcnb_060.usuario_rel          = p_usuario
                           and rcnb_060.empresa_rel          = p_empresa_logada
                           and rcnb_060.relatorio_rel        = 'tmrp_f025'
                           and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                           and rcnb_060.nivel_estrutura_str  in (estq_040.cditem_nivel99,'X')
                           and rcnb_060.grupo_estrutura_str  = 'XXXXXX')))
      group by estq_040.cditem_nivel99,  estq_040.cditem_grupo,
               estq_040.cditem_subgrupo, estq_040.cditem_item;

      if sql%found
      then

         FOR i IN p_nivel_estq.FIRST .. p_nivel_estq.LAST
         LOOP
            begin
               select  /*parallel(estq_040, basi_030, 20)*/
                      nvl(sum(estq_040.qtde_estoque_atu),0)
               INTO   p_qtde_estoque_atu
               from estq_040
               where estq_040.cditem_nivel99   = p_nivel_estq(i)
                 and estq_040.cditem_grupo     = p_grupo_estq(i)
                 and estq_040.cditem_subgrupo  = p_sub_estq(i)
                 and estq_040.cditem_item      = p_item_estq(i)
                 and ((p_inc_exc_dep = 1
                 and   estq_040.deposito in (select /*parallel(rcnb_060, 20)*/
                                                    rcnb_060.grupo_estrutura
                                               from rcnb_060
                                              where rcnb_060.tipo_registro    = 37
                                                and rcnb_060.nr_solicitacao   = p_nr_solicitacao
                                                and rcnb_060.subgru_estrutura = p_codigo_usuario
                                                and rcnb_060.usuario_rel      = p_usuario
                                                and rcnb_060.empresa_rel      = p_empresa_logada
                                                and rcnb_060.relatorio_rel    = 'tmrp_f025'
                                                and rcnb_060.tp_reg_relatorio = 0))
                  or  (p_inc_exc_dep = 2
                 and   estq_040.deposito not in (select /*parallel(rcnb_060, 20)*/
                                                        rcnb_060.grupo_estrutura
                                                           from rcnb_060
                                                  where rcnb_060.tipo_registro    = 37
                                                    and rcnb_060.nr_solicitacao   = p_nr_solicitacao
                                                    and rcnb_060.subgru_estrutura = p_codigo_usuario
                                                    and rcnb_060.usuario_rel      = p_usuario
                                                    and rcnb_060.empresa_rel      = p_empresa_logada
                                                    and rcnb_060.relatorio_rel    = 'tmrp_f025'
                                                    and rcnb_060.tp_reg_relatorio = 0)))
                 and estq_040.deposito in (select basi_205.codigo_deposito
                                             from basi_205
                                            where basi_205.local_deposito = p_empresa_filtro);
            exception when others then
               p_qtde_estoque_atu := 0;
            end;

            /*Busca quantidade em transito*/
            begin
               select /*parallel(pcpt_020, 20)*/
                      nvl(sum(pcpt_020.qtde_quilos_acab),0)
                 into p_qtde_transito
                 from pcpt_020
                where pcpt_020.panoacab_nivel99  = p_nivel_estq(i)
                  and pcpt_020.panoacab_grupo    = p_grupo_estq(i)
                  and pcpt_020.panoacab_subgrupo = p_sub_estq(i)
                  and pcpt_020.panoacab_item     = p_item_estq(i)
                  and pcpt_020.rolo_estoque      = 4
                  and ((p_inc_exc_dep = 1
                  and   pcpt_020.codigo_deposito in (select /*parallel(rcnb_060, 20)*/
                                                            rcnb_060.grupo_estrutura
                                                       from rcnb_060
                                                      where rcnb_060.tipo_registro    = 37
                                                        and rcnb_060.nr_solicitacao   = p_nr_solicitacao
                                                        and rcnb_060.subgru_estrutura = p_codigo_usuario
                                                        and rcnb_060.usuario_rel      = p_usuario
                                                        and rcnb_060.empresa_rel      = p_empresa_logada
                                                        and rcnb_060.relatorio_rel    = 'tmrp_f025'
                                                        and rcnb_060.tp_reg_relatorio = 0))
                   or  (p_inc_exc_dep = 2
                  and   pcpt_020.codigo_deposito not in (select /*parallel(rcnb_060, 20)*/
                                                                rcnb_060.grupo_estrutura
                                                           from rcnb_060
                                                         where rcnb_060.tipo_registro    = 37
                                                           and rcnb_060.nr_solicitacao   = p_nr_solicitacao
                                                           and rcnb_060.subgru_estrutura = p_codigo_usuario
                                                           and rcnb_060.usuario_rel      = p_usuario
                                                           and rcnb_060.empresa_rel      = p_empresa_logada
                                                           and rcnb_060.relatorio_rel    = 'tmrp_f025'
                                                           and rcnb_060.tp_reg_relatorio = 0)))
                  and pcpt_020.codigo_deposito in (select basi_205.codigo_deposito
                                                     from basi_205
                                                    where basi_205.local_deposito = p_empresa_filtro)
               group by pcpt_020.panoacab_nivel99,  pcpt_020.panoacab_grupo,
                        pcpt_020.panoacab_subgrupo, pcpt_020.panoacab_item;
               exception
                  when others then
                     p_qtde_transito := 0.0000;
            end;

            if sql%notfound
            then
               p_qtde_transito := 0.0000;
            end if;
            /*Fim - Quantidade em transito*/

            /*Desconsidera quantidade em transito*/
            if p_cons_transito = 0
            then
               p_qtde_estoque_atu := p_qtde_estoque_atu - p_qtde_transito;
            end if;

            /*Busca quantidade reservada para o item*/
            begin
               select /*parallel(tmrp_040, 20)*/
                      nvl(sum(tmrp_040.qtde_reservada),0)
                 into p_qtde_reservada
                 from tmrp_040
                where tmrp_040.co_reser_nivel99  = p_nivel_estq(i)
                  and tmrp_040.co_reser_grupo    = p_grupo_estq(i)
                  and tmrp_040.co_reser_subgrupo = p_sub_estq(i)
                  and tmrp_040.co_reser_item     = p_item_estq(i)
                  and tmrp_040.periodo_producao  <> 0
               group by tmrp_040.co_reser_nivel99,  tmrp_040.co_reser_grupo,
                        tmrp_040.co_reser_subgrupo, tmrp_040.co_reser_item;
               exception
                  when others then
                     p_qtde_reservada := 0.0000;
            end;

            if sql%notfound
            then
               p_qtde_reservada := 0.0000;
            end if;
            /*Fim - Reservada*/

            /*Busca quantidade programada para o item*/
            begin
               select /*parallel(tmrp_041, 20)*/
                      nvl(sum(tmrp_041.qtde_areceber),0)
                 into p_qtde_programado
                 from tmrp_041
                where tmrp_041.area_producao    = 2
                  and tmrp_041.nivel_estrutura  = p_nivel_estq(i)
                  and tmrp_041.grupo_estrutura  = p_grupo_estq(i)
                  and tmrp_041.subgru_estrutura = p_sub_estq(i)
                  and tmrp_041.item_estrutura   = p_item_estq(i)
               group by tmrp_041.nivel_estrutura,  tmrp_041.grupo_estrutura,
                         tmrp_041.subgru_estrutura, tmrp_041.item_estrutura;
               exception
                  when others then
                     p_qtde_programado := 0.0000;
            end;

            if sql%notfound
            then
               p_qtde_programado := 0.0000;
            end if;
            /*Fim - Programada*/

            /*Calcula a quantidade disponivel. O A PROGRAMAR nao e usado no calculo, pois sempre sera zero,
              visto que quando nao tem projetos e nao esta em pedido, naoo tem quantidade a programar.*/
            p_qtde_disponivel := p_qtde_estoque_atu - p_qtde_reservada + p_qtde_programado;

            if p_qtde_disponivel > 0 or p_qtde_programado > 0
            then
               inter_pr_grava_tmrp_615(p_empresa_logada,
                                       p_nivel_estq(i),
                                       p_grupo_estq(i),
                                       p_sub_estq(i),
                                       p_item_estq(i),
                                       0,
                                       p_codigo_usuario,
                                       p_usuario,
                                       p_nr_solicitacao,
                                       p_nome_programa,
                                       255,
                                       5,
                                       p_visualiza_mp);

            end if;
         END LOOP;
      end if;
   end;
end inter_pr_mp_sem_projetos;

 

/

exec inter_pr_recompile;

