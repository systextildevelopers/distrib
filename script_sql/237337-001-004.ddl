create table blocok_empr_indust(
    id                      number(9)
    ,empresa_contratante     number(3)
    ,cnpj9_industrializadora number(9)
    ,cnpj4_industrializadora number(9)
    ,cnpj2_industrializadora number(2)
);

alter table blocok_empr_indust
    add constraint pk_blocok_empr_indust primary key (id);
