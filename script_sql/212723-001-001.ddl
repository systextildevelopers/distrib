insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('inte_p216', 'Definir impressora padr�o de etiquetas do usu�rio', 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'inte_p216', 'menu_p001', 1, 0, 'N', 'S', 'N', 'S');

update hdoc_036
set hdoc_036.descricao         = 'Establecer impresora predeterminada de etiquetas de usuario'
where hdoc_036.codigo_programa = 'inte_p216'
  and hdoc_036.locale          = 'es_ES';
  
commit work;
