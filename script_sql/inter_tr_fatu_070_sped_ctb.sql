
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_070_SPED_CTB" 
   before insert or update of tipo_titulo_original, duplic_impressa, cod_transacao, codigo_contabil,
	tit_renegociado, seq_end_cobranca, num_contabil, comissao_lancada,
	referente_nf, tipo_tit_origem, num_dup_origem, seq_dup_origem,
	observacao, cli9resptit, cli4resptit, cli2resptit,
	origem_pedido, cgc9_endosso, cgc4_endosso, cgc2_endosso,
	controle_cheque, tipo_comissao, codigo_administr, comissao_administr,
	perc_comis_crec_adm, tit_baixar, posicao_ant, mensagem_boleto,
	atraso_pela_renegociacao, data_ult_movim_pagto, data_ult_movim_credito, saldo_duplicata,
	cod_forma_pagto, nr_cupom, nr_mtv_prorrogacao, nr_remessa_inadimplencia,
	nr_solicitacao_inadimplencia, situacao_inadimplencia, valor_desp_cobr, cond_pagto_vendor,
	executa_trigger, valor_desconto_aux, valor_juros_aux, valor_saldo_aux,
	nr_identificacao, cod_carteira, status_serasa_pefin, motivo_bxa_serasa_pefin,
	valor_avp, indice_mensal, indice_diario, selecionado_credito_reneg,
	num_renegociacao, seq_renegociacao, cmc7_cheque, responsavel_receb,
	cd_centro_custo, valor_remessa, moeda_titulo, perc_comis_crec,
	codigo_empresa, cli_dup_cgc_cli9, cli_dup_cgc_cli4, cli_dup_cgc_cli2,
	tipo_titulo, num_duplicata, seq_duplicatas, data_venc_duplic,
	valor_duplicata, situacao_duplic, cod_canc_duplic, data_canc_duplic,
	perc_juro_duplic, perc_desc_duplic, portador_duplic, serie_nota_fisc,
	numero_bordero, quantidade, tecido_peca, percentual_comis,
	valor_comis, base_calc_comis, pedido_venda, nr_titulo_banco,
	cod_rep_cliente, posicao_duplic, port_anterior, vencto_anterior,
	duplic_emitida, nr_solicitacao, data_emissao, numero_titulo,
	data_transf_tit, numero_sequencia, cod_historico, compl_historico,
	cod_local, previsao, valor_moeda, data_prorrogacao,
	conta_corrente, numero_remessa
   on fatu_070
   for each row

declare
   v_cliente_fornec  cont_600.cliente_fornecedor_part%type;
   v_cnpj_9          cont_600.cnpj9_participante%type;
   v_cnpj_4          cont_600.cnpj4_participante%type;
   v_cnpj_2          cont_600.cnpj2_participante%type;
   v_sid             cont_601.sid%type;
   v_instancia       cont_601.instancia%type;

begin

   if inserting
   then
      v_cnpj_9 := :new.cli_dup_cgc_cli9;
      v_cnpj_4 := :new.cli_dup_cgc_cli4;
      v_cnpj_2 := :new.cli_dup_cgc_cli2;
   else
      v_cnpj_9 := :old.cli_dup_cgc_cli9;
      v_cnpj_4 := :old.cli_dup_cgc_cli4;
      v_cnpj_2 := :old.cli_dup_cgc_cli2;
   end if;

   v_cliente_fornec := 1; -- cliente

   select sid,   inst_id
   into   v_sid, v_instancia
   from   v_lista_sessao_banco;

   insert into cont_601
      (sid,
       instancia,
       data_insercao,
       tabela_origem,
       cnpj_9,
       cnpj_4,
       cnpj_2,
       cliente_fornec)
   values
      (v_sid,
       v_instancia,
       sysdate,
       'FATU_070',
       v_cnpj_9,
       v_cnpj_4,
       v_cnpj_2,
       v_cliente_fornec);

   exception
      when others then
         null;
end inter_tr_fatu_070_sped_ctb;
-- ALTER TRIGGER "INTER_TR_FATU_070_SPED_CTB" ENABLE
 

/

exec inter_pr_recompile;

