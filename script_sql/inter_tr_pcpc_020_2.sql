create or replace trigger inter_tr_pcpc_020_2
   before delete or
         update of cod_cancelamento
   on pcpc_020
   for each row

declare
   v_executa_trigger  number;
   v_qtde_prod_pedido number;
begin
   -- L�gica de controle para execu��o ou n�o da trigger, referente ao
   -- processo de limpeza de dados.
   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      -- Esta trigger far� executar� com base no pedido de compra a atualiza��o
      -- das quantidades programadas na ordem de planejamento (tmrp_630).
      -- Estas atualiza��es ir�o acontecer quando cancelado um pedido ou excluido
      -- por este motivo n�o temos INSERT.
      if updating and :new.cod_cancelamento <> 0 and :new.cod_cancelamento <> :old.cod_cancelamento
      then
         -- Quando o produto origem for igual '0', significa que o produto � o
         -- produto principal da ordem, assim j� teremos a quantidade a receber
         -- do produto da ordem.
         for reg_tmrp in (select tmrp_630.ordem_planejamento,            tmrp_630.pedido_venda,
                                 tmrp_630.nivel_produto,                 tmrp_630.grupo_produto,
                                 tmrp_630.subgrupo_produto,              tmrp_630.item_produto,
                                 tmrp_625.seq_produto_origem,            tmrp_625.nivel_produto_origem,
                                 tmrp_625.grupo_produto_origem,          tmrp_625.subgrupo_produto_origem,
                                 tmrp_625.item_produto_origem,           tmrp_625.alternativa_produto_origem,
                                 tmrp_625.seq_produto,                   tmrp_625.qtde_areceber_programada,
                                 tmrp_625.qtde_reserva_programada
                          from tmrp_630, tmrp_625
                          where tmrp_630.ordem_planejamento      = tmrp_625.ordem_planejamento
                            and tmrp_630.pedido_venda            = tmrp_625.pedido_venda
                            and tmrp_630.nivel_produto           = tmrp_625.nivel_produto
                            and tmrp_630.grupo_produto           = tmrp_625.grupo_produto
                            and tmrp_630.subgrupo_produto        = tmrp_625.subgrupo_produto
                            and tmrp_630.item_produto            = tmrp_625.item_produto
                            and tmrp_625.nivel_produto_origem    = '0'
                            and tmrp_625.grupo_produto_origem    = '00000'
                            and tmrp_625.subgrupo_produto_origem = '000'
                            and tmrp_625.item_produto_origem     = '000000'
                            and tmrp_630.nivel_produto           = '1'
                            and tmrp_630.grupo_produto           = :new.referencia_peca
                            and tmrp_630.area_producao           = 1 --Confec�ao
                            and tmrp_630.ordem_prod_compra       = :new.ordem_producao)
         loop
            -- A quantidade a receber do produto principal
            begin
               select sum(tmrp_041_data.qtde_areceber)
               into v_qtde_prod_pedido
               from tmrp_041_data
               where tmrp_041_data.area_producao   = 1
                 and tmrp_041_data.nr_pedido_ordem = :new.ordem_producao
                 and tmrp_041_data.nivel_prod      = reg_tmrp.nivel_produto
                 and tmrp_041_data.grupo_prod      = reg_tmrp.grupo_produto
                 and tmrp_041_data.subgrupo_prod   = reg_tmrp.subgrupo_produto
                 and tmrp_041_data.item_prod       = reg_tmrp.item_produto
               group by tmrp_041_data.nivel_prod, tmrp_041_data.grupo_prod,
                        tmrp_041_data.subgrupo_prod, tmrp_041_data.item_prod;
            exception
            when OTHERS then
               v_qtde_prod_pedido := 0.00;
            end;

            inter_pr_atu_planej_reves( reg_tmrp.ordem_planejamento,
                                       reg_tmrp.pedido_venda,            reg_tmrp.nivel_produto,
                                       reg_tmrp.grupo_produto,           reg_tmrp.subgrupo_produto,
                                       reg_tmrp.item_produto,            v_qtde_prod_pedido,
                                       reg_tmrp.qtde_reserva_programada,
                                       reg_tmrp.nivel_produto_origem,    reg_tmrp.grupo_produto_origem,
                                       reg_tmrp.subgrupo_produto_origem, reg_tmrp.item_produto_origem,
                                       reg_tmrp.seq_produto_origem,      reg_tmrp.alternativa_produto_origem,
                                       reg_tmrp.seq_produto,             'pcpc_020',
                                       :new.ordem_producao);

            -- Ap�s atualiza��o de todos os componentes do produto ir� deletar o link entre
            -- o Systextil e as ordens de planejamento (tmrp_630)
            begin
               delete from tmrp_630
               where tmrp_630.ordem_planejamento = reg_tmrp.ordem_planejamento
                 and tmrp_630.pedido_venda       = reg_tmrp.pedido_venda
                 and tmrp_630.area_producao      = 1
                 and tmrp_630.ordem_prod_compra  = :new.ordem_producao
                 and tmrp_630.nivel_produto      = reg_tmrp.nivel_produto
                 and tmrp_630.grupo_produto      = reg_tmrp.grupo_produto
                 and tmrp_630.subgrupo_produto   = reg_tmrp.subgrupo_produto
                 and tmrp_630.item_produto       = reg_tmrp.item_produto;
            exception
            when OTHERS then
               raise_application_error(-20000,'N�o excluiu tmpr_630');
            end;
         end loop;
      end if;

      if deleting
      then
         for reg_tmrp in (select tmrp_630.ordem_planejamento,            tmrp_630.pedido_venda,
                                 tmrp_630.nivel_produto,                 tmrp_630.grupo_produto,
                                 tmrp_630.subgrupo_produto,              tmrp_630.item_produto,
                                 tmrp_625.seq_produto_origem,            tmrp_625.nivel_produto_origem,
                                 tmrp_625.grupo_produto_origem,          tmrp_625.subgrupo_produto_origem,
                                 tmrp_625.item_produto_origem,           tmrp_625.alternativa_produto_origem,
                                 tmrp_625.seq_produto,                   tmrp_625.qtde_areceber_programada,
                                 tmrp_625.qtde_reserva_programada
                          from tmrp_630, tmrp_625
                          where tmrp_630.ordem_planejamento      = tmrp_625.ordem_planejamento
                            and tmrp_630.pedido_venda            = tmrp_625.pedido_venda
                            and tmrp_630.nivel_produto           = tmrp_625.nivel_produto
                            and tmrp_630.grupo_produto           = tmrp_625.grupo_produto
                            and tmrp_630.subgrupo_produto        = tmrp_625.subgrupo_produto
                            and tmrp_630.item_produto            = tmrp_625.item_produto
                            and tmrp_625.nivel_produto_origem    = '0'
                            and tmrp_625.grupo_produto_origem    = '00000'
                            and tmrp_625.subgrupo_produto_origem = '000'
                            and tmrp_625.item_produto_origem     = '000000'
                            and tmrp_630.nivel_produto           = '1'
                            and tmrp_630.grupo_produto           = :old.referencia_peca
                            and tmrp_630.area_producao           = 1 --Confec�ao
                            and tmrp_630.ordem_prod_compra       = :old.ordem_producao)
         loop
            -- pega a quantidade do produto para fazer a atualiza��o da tabela tmrp_625.
            begin
               select sum(tmrp_041_data.qtde_areceber)
               into v_qtde_prod_pedido
               from tmrp_041_data
               where tmrp_041_data.area_producao   = 1
                 and tmrp_041_data.nr_pedido_ordem = :old.ordem_producao
                 and tmrp_041_data.nivel_prod      = reg_tmrp.nivel_produto
                 and tmrp_041_data.grupo_prod      = reg_tmrp.grupo_produto
                 and tmrp_041_data.subgrupo_prod   = reg_tmrp.subgrupo_produto
                 and tmrp_041_data.item_prod       = reg_tmrp.item_produto
               group by tmrp_041_data.nivel_prod, tmrp_041_data.grupo_prod,
                        tmrp_041_data.subgrupo_prod, tmrp_041_data.item_prod;
            exception
            when OTHERS then
               v_qtde_prod_pedido := 0.00;
            end;


            inter_pr_atu_planej_reves( reg_tmrp.ordem_planejamento,
                                       reg_tmrp.pedido_venda,            reg_tmrp.nivel_produto,
                                       reg_tmrp.grupo_produto,           reg_tmrp.subgrupo_produto,
                                       reg_tmrp.item_produto,            v_qtde_prod_pedido,
                                       0.00,
                                       reg_tmrp.nivel_produto_origem,    reg_tmrp.grupo_produto_origem,
                                       reg_tmrp.subgrupo_produto_origem, reg_tmrp.item_produto_origem,
                                       reg_tmrp.seq_produto_origem,      reg_tmrp.alternativa_produto_origem,
                                       reg_tmrp.seq_produto,             'pcpc_020',
                                       :old.ordem_producao);

            -- Ap�s atualiza��o de todos os componentes do produto ir� deletar o link entre
            -- o Systextil e as ordens de planejamento (tmrp_630)
            begin
               delete from tmrp_630
               where tmrp_630.ordem_planejamento = reg_tmrp.ordem_planejamento
                 and tmrp_630.pedido_venda       = reg_tmrp.pedido_venda
                 and tmrp_630.area_producao      = 1
                 and tmrp_630.ordem_prod_compra  = :old.ordem_producao
                 and tmrp_630.nivel_produto      = reg_tmrp.nivel_produto
                 and tmrp_630.grupo_produto      = reg_tmrp.grupo_produto
                 and tmrp_630.subgrupo_produto   = reg_tmrp.subgrupo_produto
                 and tmrp_630.item_produto       = reg_tmrp.item_produto;
            exception
            when OTHERS then
               raise_application_error(-20000,'N�o excluiu tmpr_630');
            end;
         end loop;
      end if;
   end if;
end;

/

execute inter_pr_recompile;
/* versao: 3 */
