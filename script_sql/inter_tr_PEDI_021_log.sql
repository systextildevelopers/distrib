create or replace trigger inter_tr_PEDI_021_log 
after insert or delete or update 
on PEDI_021 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    begin 
       select hdoc_090.programa 
       into v_nome_programa 
       from hdoc_090 
       where hdoc_090.sid = ws_sid 
         and rownum       = 1 
         and hdoc_090.programa not like '%menu%' 
         and hdoc_090.programa not like '%_m%'; 
       exception 
         when no_data_found then            v_nome_programa := 'SQL'; 
    end; 
 
 
 
 if inserting 
 then 
    begin 
 
        insert into PEDI_021_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           cod_repres_OLD,   /*8*/ 
           cod_repres_NEW,   /*9*/ 
           tipo_comissao_OLD,   /*10*/ 
           tipo_comissao_NEW,   /*11*/ 
           perc_comissao_OLD,   /*12*/ 
           perc_comissao_NEW    /*13*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.cod_repres, /*9*/   
           0,/*10*/
           :new.tipo_comissao, /*11*/   
           0,/*12*/
           :new.perc_comissao /*13*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into PEDI_021_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           cod_repres_OLD, /*8*/   
           cod_repres_NEW, /*9*/   
           tipo_comissao_OLD, /*10*/   
           tipo_comissao_NEW, /*11*/   
           perc_comissao_OLD, /*12*/   
           perc_comissao_NEW  /*13*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.cod_repres,  /*8*/  
           :new.cod_repres, /*9*/   
           :old.tipo_comissao,  /*10*/  
           :new.tipo_comissao, /*11*/   
           :old.perc_comissao,  /*12*/  
           :new.perc_comissao  /*13*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into PEDI_021_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           cod_repres_OLD, /*8*/   
           cod_repres_NEW, /*9*/   
           tipo_comissao_OLD, /*10*/   
           tipo_comissao_NEW, /*11*/   
           perc_comissao_OLD, /*12*/   
           perc_comissao_NEW /*13*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.cod_repres, /*8*/   
           0, /*9*/
           :old.tipo_comissao, /*10*/   
           0, /*11*/
           :old.perc_comissao, /*12*/   
           0 /*13*/
         );    
    end;    
 end if;    
end inter_tr_PEDI_021_log;
