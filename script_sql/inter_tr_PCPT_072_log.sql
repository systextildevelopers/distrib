create or replace trigger inter_tr_PCPT_072_log 
after insert or delete or update 
on PCPT_072 
for each row 
declare 
   ws_usuario_rede           varchar2(250) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    -- begin 
    --    select hdoc_090.programa 
    --    into v_nome_programa 
    --    from hdoc_090 
    --    where hdoc_090.sid = ws_sid 
    --      and rownum       = 1 
    --      and hdoc_090.programa not like '%menu%' 
    --      and hdoc_090.programa not like '%_m%'; 
    --    exception 
    --      when no_data_found then            v_nome_programa := 'SQL'; 
    -- end; 
 
    v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 
 if inserting 
 then 
    begin 
 
        insert into PCPT_072_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           id_OLD,   /*8*/ 
           id_NEW,   /*9*/ 
           definicao_OLD,   /*10*/ 
           definicao_NEW,   /*11*/ 
           qtde_metros_de_OLD,   /*12*/ 
           qtde_metros_de_NEW,   /*13*/ 
           qtde_metros_ate_OLD,   /*14*/ 
           qtde_metros_ate_NEW,   /*15*/ 
           unidade_medida_embalagem_OLD,   /*16*/ 
           unidade_medida_embalagem_NEW,   /*17*/ 
           descricao_da_embalagem_OLD,   /*18*/ 
           descricao_da_embalagem_NEW    /*19*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.id, /*9*/   
           '',/*10*/
           :new.definicao, /*11*/   
           0,/*12*/
           :new.qtde_metros_de, /*13*/   
           0,/*14*/
           :new.qtde_metros_ate, /*15*/   
           '',/*16*/
           :new.unidade_medida_embalagem, /*17*/   
           '',/*18*/
           :new.descricao_da_embalagem /*19*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into PCPT_072_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           id_OLD, /*8*/   
           id_NEW, /*9*/   
           definicao_OLD, /*10*/   
           definicao_NEW, /*11*/   
           qtde_metros_de_OLD, /*12*/   
           qtde_metros_de_NEW, /*13*/   
           qtde_metros_ate_OLD, /*14*/   
           qtde_metros_ate_NEW, /*15*/   
           unidade_medida_embalagem_OLD, /*16*/   
           unidade_medida_embalagem_NEW, /*17*/   
           descricao_da_embalagem_OLD, /*18*/   
           descricao_da_embalagem_NEW  /*19*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.id,  /*8*/  
           :new.id, /*9*/   
           :old.definicao,  /*10*/  
           :new.definicao, /*11*/   
           :old.qtde_metros_de,  /*12*/  
           :new.qtde_metros_de, /*13*/   
           :old.qtde_metros_ate,  /*14*/  
           :new.qtde_metros_ate, /*15*/   
           :old.unidade_medida_embalagem,  /*16*/  
           :new.unidade_medida_embalagem, /*17*/   
           :old.descricao_da_embalagem,  /*18*/  
           :new.descricao_da_embalagem  /*19*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into PCPT_072_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           id_OLD, /*8*/   
           id_NEW, /*9*/   
           definicao_OLD, /*10*/   
           definicao_NEW, /*11*/   
           qtde_metros_de_OLD, /*12*/   
           qtde_metros_de_NEW, /*13*/   
           qtde_metros_ate_OLD, /*14*/   
           qtde_metros_ate_NEW, /*15*/   
           unidade_medida_embalagem_OLD, /*16*/   
           unidade_medida_embalagem_NEW, /*17*/   
           descricao_da_embalagem_OLD, /*18*/   
           descricao_da_embalagem_NEW /*19*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.id, /*8*/   
           0, /*9*/
           :old.definicao, /*10*/   
           '', /*11*/
           :old.qtde_metros_de, /*12*/   
           0, /*13*/
           :old.qtde_metros_ate, /*14*/   
           0, /*15*/
           :old.unidade_medida_embalagem, /*16*/   
           '', /*17*/
           :old.descricao_da_embalagem, /*18*/   
           '' /*19*/
         );    
    end;    
 end if;    
end inter_tr_PCPT_072_log;
