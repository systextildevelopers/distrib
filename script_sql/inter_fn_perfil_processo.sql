create or replace function inter_fn_perfil_processo (
    p_ordem           number   default null
    ,p_nivel          number   default null
    ,p_grupo          varchar2 default null
    ,p_subgrupo       varchar2 default null
    ,p_item           varchar2 default null
    ,p_codigo_empresa number
) return varchar2 

is

    v_ordem    number;
    v_nivel    number;
    v_grupo    varchar2(200);
    v_subgrupo varchar2(200);
    v_item     varchar2(200);

    v_tipo_estampa     number;
    v_codigo_estagio   number;
    v_numero_alternati number;
    v_numero_roteiro   number;
    v_codigo_operacao  number;
    v_perfil_processo  varchar2(200);

begin

    v_ordem    := p_ordem;
    v_nivel    := p_nivel;
    v_grupo    := p_grupo;
    v_subgrupo := p_subgrupo;
    v_item     := p_item;

    if v_nivel is null then
        select 
            m.nivel_estrutura,
            m.grupo_estrutura,
            m.subgru_estrutura,
            m.item_estrutura
        into
            v_nivel,
            v_grupo,
            v_subgrupo,
            v_item
        from pcpb_020 p, mqop_050 m
        where   p.ordem_producao     = p_ordem
            and p.pano_sbg_nivel99   = m.nivel_estrutura
            and p.pano_sbg_grupo     = m.grupo_estrutura
            and (p.pano_sbg_subgrupo = m.subgru_estrutura or m.subgru_estrutura = '000')
            and (p.pano_sbg_item     = m.item_estrutura   or m.item_estrutura   = '000000')
            and p.alternativa_item   = m.numero_alternati
            and p.roteiro_opcional   = m.numero_roteiro
            and rownum = 1;
    end if;

    select 
        basi_122.tipo_estampa
    into v_tipo_estampa
    from   basi_010, basi_122
    where basi_010.subgru_estrutura = basi_122.codigo_processo
     and  basi_010.nivel_estrutura  = v_nivel
     and  basi_010.grupo_estrutura  = v_grupo
     and (v_subgrupo                = basi_010.subgru_estrutura or v_subgrupo = '000')
     and (v_item                    = basi_010.item_estrutura   or v_item     = '000000')
     and  rownum = 1
     and  basi_122.tipo_estampa is not null;

    --// 1 - digital
    --// 2 - ROTATIVA
    --// 3 - transfer
    
    if v_tipo_estampa not in (1, 2, 3) then
        return null;
    end if;
--raise_application_error(-20000,'v_tipo_estampa 1' || ' -- ' || v_tipo_estampa);
    if v_tipo_estampa = 1 or v_tipo_estampa = 3 
    then
      --raise_application_error(-20000,'v_tipo_estampa 1' || ' -- ' || v_tipo_estampa);
      select
        decode(
          v_tipo_estampa
          ,1,fatu_504.estagio_estampa_digital
          ,3,fatu_504.estagio_estampa_transfer
        )
      into v_codigo_estagio
      from fatu_504
      where fatu_504.codigo_empresa = p_codigo_empresa;

      if p_ordem is not null then
        select 
          m.codigo_operacao
        into v_codigo_operacao
        from pcpb_020 p, mqop_050 m
        where p.ordem_producao       = v_ordem
          and p.pano_sbg_nivel99   = m.nivel_estrutura
          and p.pano_sbg_grupo     = m.grupo_estrutura
          and (p.pano_sbg_subgrupo = m.subgru_estrutura or m.subgru_estrutura = '000')
          and (p.pano_sbg_item     = m.item_estrutura   or m.item_estrutura = '000000')
          and p.alternativa_item   = m.numero_alternati
          and p.roteiro_opcional   = m.numero_roteiro
          and m.codigo_estagio     = v_codigo_estagio
          and rownum = 1;
      else
        select 
          m.codigo_operacao
        into v_codigo_operacao
        from mqop_050 m, basi_010 b, mqop_040 o
        where
          b.nivel_estrutura       = m.nivel_estrutura
          and b.grupo_estrutura   = m.grupo_estrutura
          and b.subgru_estrutura  = m.subgru_estrutura
          and b.item_estrutura    = m.item_estrutura
          and p_nivel             = m.nivel_estrutura
          and p_grupo             = m.grupo_estrutura
          and (p_subgrupo         = m.subgru_estrutura or m.subgru_estrutura = m.subgru_estrutura)
          and (p_item             = m.item_estrutura   or m.item_estrutura   = m.item_estrutura  )
          and m.codigo_estagio    = v_codigo_estagio
          and m.codigo_operacao   = o.codigo_operacao
          and o.classificacao in (1,2)
          and rownum = 1;
      end if;

      select distinct 
        ftec_208.descricao
      into v_perfil_processo
      from ftec_208

      inner join ftec_207 on
          ftec_208.codigo_perfil      = ftec_207.codigo_perfil
        and ftec_207.nivel_estrutura  = v_nivel
        and ftec_207.grupo_estrutura  = v_grupo
        and ftec_207.subgru_estrutura = v_subgrupo
        and ftec_207.item_estrutura   = v_item

      inner join mqop_040 on
          mqop_040.grupo_maquinas   = ftec_207.grupo_maquina  
        and mqop_040.sub_maquina    = ftec_207.subgrupo_maquina

      where mqop_040.codigo_operacao = v_codigo_operacao;

      return v_perfil_processo;
    else
      select 
        basi_122.descricao
      into v_perfil_processo
      from  basi_010, basi_122
      where basi_010.subgru_estrutura = basi_122.codigo_processo
       and  basi_010.nivel_estrutura  = v_nivel
       and  basi_010.grupo_estrutura  = v_grupo
       and (v_subgrupo                = basi_010.subgru_estrutura or v_subgrupo = '000')
       and (v_item                    = basi_010.item_estrutura   or v_item     = '000000')
       and  rownum = 1
       and  basi_122.tipo_estampa is not null;
      
      if v_perfil_processo is null
      then
         select 
           basi_020.descr_tam_refer
         into v_perfil_processo
         from  basi_020
         where basi_020.basi030_nivel030 = v_nivel
         and   basi_020.basi030_referenc = v_grupo
         and  (v_subgrupo                = basi_020.tamanho_ref or v_subgrupo = '000');
      end if;   
      
      return v_perfil_processo;
    end if;

    exception
        when no_data_found then
            return null;
            
end inter_fn_perfil_processo;
