alter table pedi_187 add classificacao_pedido number(5) default 0 not null;
alter table pedi_187 add suframa number(1) default 0 not null;

ALTER TABLE pedi_187  DROP PRIMARY KEY;
-- � preciso tamb�m remover o �ndice
DROP INDEX pk_pedi_187;

alter table pedi_187 add constraint pk_pedi_187 primary key (NIVEL, GRUPO, SUBGRUPO, ITEM, TIPO_PESSOA, ESTADO, PROCESSO, CODIGO_EMPRESA, CLASSIFIC_FISC, CNPJ_CLI9, CNPJ_CLI4, CNPJ_CLI2, NAT_OP_ORIGEM, ORIGEM_PROD, COD_ATIVIDADE_ECONOMICA, INSC_EST,classificacao_pedido,suframa,COMPRADO_FABRICADO);

exec inter_pr_recompile;
