CREATE OR REPLACE VIEW INTER_VI_PCPC_888_ORD_ORIG
(
  NUMERO_ORDEM,
  CODIGO_SERVICO,
  COD_TABELA_SERV,
  COD_TABELA_MES,
  COD_TABELA_SEQ,
  NUMERO_SOLICITACAO
)
AS 
SELECT obrf_080.numero_ordem,
       obrf_080.codigo_servico,
       obrf_080.cod_tabela_serv,
       obrf_080.cod_tabela_mes,
       obrf_080.cod_tabela_seq,
       pcpc_880.numero_solicitacao
FROM pcpc_888,
     obrf_089_aberta,
     pcpc_040,
     obrf_080,
     pcpc_880
WHERE pcpc_888.numero_solicitacao = obrf_089_aberta.numero_solicitacao
AND   pcpc_888.numero_ordem = obrf_089_aberta.ordem_servico
AND   pcpc_880.numero_solicitacao = pcpc_888.numero_solicitacao
AND   pcpc_040.periodo_producao = obrf_089_aberta.periodo_producao
AND   pcpc_040.ordem_confeccao = obrf_089_aberta.ordem_confeccao
AND   pcpc_040.ordem_producao = obrf_089_aberta.ordem_producao
AND   pcpc_040.codigo_estagio = pcpc_880.estagio_conserto
AND   pcpc_040.numero_ordem = obrf_080.numero_ordem
AND   pcpc_888.codigo_cancelamento = 0
AND   pcpc_888.numero_solicitacao > 0
GROUP BY obrf_080.numero_ordem,
         obrf_080.codigo_servico,
         obrf_080.cod_tabela_serv,
         obrf_080.cod_tabela_mes,
         obrf_080.cod_tabela_seq,
         pcpc_880.numero_solicitacao
UNION
SELECT obrf_080.numero_ordem,
       obrf_080.codigo_servico,
       obrf_080.cod_tabela_serv,
       obrf_080.cod_tabela_mes,
       obrf_080.cod_tabela_seq,
       pcpc_888.numero_solicitacao
FROM pcpc_888,
     obrf_089_aberta,
     pcpc_040,
     obrf_080
WHERE pcpc_888.numero_solicitacao = obrf_089_aberta.numero_solicitacao
AND   pcpc_888.numero_ordem = obrf_089_aberta.ordem_servico
AND   pcpc_040.periodo_producao = obrf_089_aberta.periodo_producao
AND   pcpc_040.ordem_confeccao = obrf_089_aberta.ordem_confeccao
AND   pcpc_040.ordem_producao = obrf_089_aberta.ordem_producao
AND   pcpc_040.codigo_estagio = pcpc_888.cod_est_cons
AND   pcpc_040.numero_ordem = obrf_080.numero_ordem
AND   pcpc_888.codigo_cancelamento = 0
AND   pcpc_888.numero_solicitacao > 0
GROUP BY obrf_080.numero_ordem,
         obrf_080.codigo_servico,
         obrf_080.cod_tabela_serv,
         obrf_080.cod_tabela_mes,
         obrf_080.cod_tabela_seq,
         pcpc_888.numero_solicitacao;



exec inter_pr_recompile;
