create table obrf_971_origem (
    codigo_empresa_nota_orig            number(3)       not null,
    serie_nota_orig                     varchar2(3)     not null,
    num_nota_orig                       number(9)       not null,
    numero_identificacao                number(9)       not null,
    data_processo                       date            default sysdate,

    CONSTRAINT obrf_971_origem_pk PRIMARY KEY (codigo_empresa_nota_orig, serie_nota_orig, num_nota_orig, numero_identificacao) USING INDEX  ENABLE
);

comment on table obrf_971_origem is 'Notas de saída de origem do romaneio (obrf_971.numero_identificacao) gerado no processo obrf_fa02';
