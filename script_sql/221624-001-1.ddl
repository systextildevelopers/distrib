alter table cont_560 
add (
    cta_var_camb_ativ_cpag  number(5) default 0,
    cta_var_camb_pass_cpag  number(5) default 0,
    hst_var_camb_cpag       number(4) default 0,
    ccusto_var_camb_cpag    number(6) default 0,
    cta_var_camb_ativ_crec  number(5) default 0,
    cta_var_camb_pass_crec  number(5) default 0,
    hst_var_camb_crec       number(4) default 0,
    ccusto_var_camb_crec    number(6) default 0
);
