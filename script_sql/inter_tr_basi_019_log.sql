
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_019_LOG" 
   after insert
      or delete
      or update
      of  PARTE_CONJUNTO,               QTDE_PARTE_PECA,
          TIPO_CORTE_PECA,              TIPO_ENFESTO_1,
          TIPO_ENFESTO_2,               COMPRIMENTO_DEBRUM,
          LARGURA_DEBRUM,               NUMERO_BALANCIN_LASER,
          INF_CORTE_SENTIDO,            INF_CORTE_CASADO,
          INF_CORTE_DESENHO,            OBSERVACOES
   on basi_019
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_cod_tipo_produto_n     varchar2(60);
   ws_tipo_produto_n         varchar2(60);
   ws_desc_projeto_n         varchar2(60);

   ws_desc_prod_n            varchar2(120);
   ws_desc_comp_n            varchar2(120);

   ws_sequencia_subprojeto   number;
   ws_nivel_comp             varchar2(1);
   ws_grupo_comp             varchar2(5);
   ws_subgru_comp            varchar2(3);
   ws_item_comp              varchar2(6);

   ws_descricao_parte_peca   varchar2(30);
   ws_descricao_alternativa  basi_070.descricao%type;

   long_aux                  varchar2(2000);
   ws_teve                   varchar2(1);
   ws_narrativa              basi_010.narrativa%type;
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      -- DESCRICAO DO PROJETO
      begin
         select basi_001.descricao_projeto
         into ws_desc_projeto_n
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto
           and rownum = 1;
      exception when no_data_found then
         ws_desc_projeto_n := '';
      end;

      inter_pr_desc_produto(:new.nivel_item,
                            :new.grupo_item,
                            :new.subgru_item,
                            :new.item_item,
                            ws_desc_prod_n,
                            ws_narrativa);

      -- COMPONENTE QUE VAI EM CIMA DO TECIDO
      begin
         select basi_013.sequencia_subprojeto,  basi_013.nivel_comp,
                basi_013.grupo_comp,            basi_013.subgru_comp,
                basi_013.item_comp
         into   ws_sequencia_subprojeto,        ws_nivel_comp,
                ws_grupo_comp,                  ws_subgru_comp,
                ws_item_comp
         from basi_013
         where basi_013.codigo_projeto       = :new.codigo_projeto
           and basi_013.sequencia_projeto    = :new.sequencia_projeto
           and basi_013.nivel_item           = :new.nivel_item
           and basi_013.grupo_item           = :new.grupo_item
           and basi_013.subgru_item          = :new.subgru_item
           and basi_013.item_item            = :new.item_item
           and basi_013.alternativa_produto  = :new.alternativa_produto
           and basi_013.sequencia_estrutura  = :new.sequencia_estrutura
           and basi_013.sequencia_variacao   = :new.sequencia_variacao
           and rownum = 1;
      exception when no_data_found then
         ws_sequencia_subprojeto   := 0;
         ws_nivel_comp             := '0';
         ws_grupo_comp             := '00000';
         ws_subgru_comp            := '000';
         ws_item_comp              := '000000';
      end;

      -- ENCONTRA O TIPO_PRODUTO DA SEQUENCIA
      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = ws_sequencia_subprojeto
           and rownum = 1;
      exception when no_data_found then
         ws_cod_tipo_produto_n := '';
      end;

      -- DESCRICAO DO TIPO DE PRODUTO
      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n
           and rownum = 1;
      exception when no_data_found then
         ws_tipo_produto_n := '';
      end;

      -- DESCRICAO DO NOVO COMPONENTE
      inter_pr_desc_produto(ws_nivel_comp,
                            ws_grupo_comp,
                            ws_subgru_comp,
                            ws_item_comp,
                            ws_desc_comp_n,
                            ws_narrativa);

      -- DESCRICAO PARTE DA PECA
      begin
         select basi_018.descricao_parte_peca
         into   ws_descricao_parte_peca
         from basi_018
         where basi_018.codigo_parte_peca = :new.codigo_parte_peca
           and rownum = 1;
      exception when no_data_found then
         ws_descricao_parte_peca := '';
      end;

      -- DESCRICAO PARTE DA ALTERNATIVA
      begin
         select basi_070.descricao
         into   ws_descricao_alternativa
         from basi_070
         where  basi_070.nivel       = :new.nivel_item
           and (basi_070.grupo       = :new.grupo_item
           or   basi_070.grupo       = '00000')
           and (basi_070.subgrupo    = :new.subgru_item
           or   basi_070.subgrupo    = '000')
           and (basi_070.item        = :new.item_item
           or   basi_070.item        = '000000')
           and  basi_070.alternativa = :new.alternativa_produto
           and  basi_070.roteiro     = 0
           and  rownum               = 1
         order by basi_070.nivel DESC, basi_070.grupo DESC, basi_070.subgrupo DESC, basi_070.item DESC;
      exception when no_data_found then
         ws_descricao_alternativa := '';
      end;

      begin
         INSERT INTO hist_100 (
            tabela,                   operacao,
            data_ocorr,               aplicacao,
            usuario_rede,             maquina_rede,

            str01,                    num05,
            str07,                    str08,
            str09,                    str10,
            num06,                    num07,
            num04,

            long01
         ) VALUES (
            'BASI_019',               'I',
            sysdate,                  ws_aplicativo,
            ws_usuario_rede,          ws_maquina_rede,

            :new.codigo_projeto,      :new.sequencia_projeto,
            :new.nivel_item,          :new.grupo_item,
            :new.subgru_item,         :new.item_item,
            :new.alternativa_produto, :new.sequencia_estrutura,
            :new.sequencia_variacao,
            '                            ' ||
            inter_fn_buscar_tag('lb30101#PARTES DA PECA',ws_locale_usuario,ws_usuario_systextil) || '(019)' ||
            chr(10)                                               ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.codigo_projeto    ||
            ' - '                       || ws_desc_projeto_n      ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.nivel_item        ||
            '.'                         || :new.grupo_item        ||
            '.'                         || :new.subgru_item       ||
            '.'                         || :new.item_item         ||
            ' - '                       || ws_desc_prod_n         ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb00442#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || to_char(:new.alternativa_produto,'00')   ||
            ' - '                       || ws_descricao_alternativa  ||
            chr(10)                                               ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb08486#TIPO DE PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || ws_cod_tipo_produto_n  ||
            ' - '                       || ws_tipo_produto_n      ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb00523#COMPONENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :new.sequencia_estrutura      ||
            '.'                         || :new.sequencia_variacao       ||
            ' - '                       || ws_nivel_comp                 ||
            '.'                         || ws_grupo_comp                 ||
            '.'                         || ws_subgru_comp                ||
            '.'                         || ws_item_comp                  ||
            ' - '                       || ws_desc_comp_n                ||
            chr(10)                                                      ||
            inter_fn_buscar_tag('lb19574#PARTE DA PECA:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.codigo_parte_peca   ||
            ' - '                       || ws_descricao_parte_peca  ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb08190#PARTE CONJUNTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.parte_conjunto      ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb37028#QTDE. PARTES:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.qtde_parte_peca     ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb37029#TIPO CORTE:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.tipo_corte_peca     ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb29724#TIPO DE ENFESTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || '(1) -'                    || ' '
                                        || :new.tipo_enfesto_1      || '  '
                                        || '(2) -'                    || ' '
                                        || :new.tipo_enfesto_2      ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb37030#INFORMACOES DO CORTE:',ws_locale_usuario,ws_usuario_systextil)  ||
            chr(10)                                                 ||
            '      ' || inter_fn_buscar_tag('lb37031#SENTIDO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.inf_corte_sentido   ||
            chr(10)                                                 ||
            '      ' || inter_fn_buscar_tag('lb37032#CASADO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.inf_corte_casado    ||
            chr(10)                                                 ||
            '      ' || inter_fn_buscar_tag('lb31575#DESENHO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.inf_corte_desenho   ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb37033#COMP DEBRUM::',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.comprimento_debrum   ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb37034#LARGURA DEBRUM:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.largura_debrum      ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb37035#NUMERO  NAVALHA/LASER:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.numero_balancin_laser  ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb29116#OBSERVACAO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.observacoes         ||
            chr(10)
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   if updating
   then
      -- DESCRICAO DO PROJETO
      begin
         select basi_001.descricao_projeto
         into ws_desc_projeto_n
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto
           and rownum = 1;
      exception when no_data_found then
         ws_desc_projeto_n := '';
      end;

      inter_pr_desc_produto(:new.nivel_item,
                            :new.grupo_item,
                            :new.subgru_item,
                            :new.item_item,
                            ws_desc_prod_n,
                            ws_narrativa);

      -- COMPONENTE QUE VAI EM CIMA DO TECIDO
      begin
         select basi_013.sequencia_subprojeto,  basi_013.nivel_comp,
                basi_013.grupo_comp,            basi_013.subgru_comp,
                basi_013.item_comp
         into   ws_sequencia_subprojeto,        ws_nivel_comp,
                ws_grupo_comp,                  ws_subgru_comp,
                ws_item_comp
         from basi_013
         where basi_013.codigo_projeto       = :new.codigo_projeto
           and basi_013.sequencia_projeto    = :new.sequencia_projeto
           and basi_013.nivel_item           = :new.nivel_item
           and basi_013.grupo_item           = :new.grupo_item
           and basi_013.subgru_item          = :new.subgru_item
           and basi_013.item_item            = :new.item_item
           and basi_013.alternativa_produto  = :new.alternativa_produto
           and basi_013.sequencia_estrutura  = :new.sequencia_estrutura
           and basi_013.sequencia_variacao   = :new.sequencia_variacao
           and rownum                        = 1;
      exception when no_data_found then
         ws_sequencia_subprojeto   := 0;
         ws_nivel_comp             := '0';
         ws_grupo_comp             := '00000';
         ws_subgru_comp            := '000';
         ws_item_comp              := '000000';
      end;

      -- ENCONTRA O TIPO_PRODUTO DA SEQUENCIA
      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = ws_sequencia_subprojeto
           and rownum                     = 1;
      exception when no_data_found then
         ws_cod_tipo_produto_n := '';
      end;

      -- DESCRICAO DO TIPO DE PRODUTO
      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n
           and rownum                       = 1;
      exception when no_data_found then
         ws_tipo_produto_n := '';
      end;

      -- DESCRICAO DO NOVO COMPONENTE
      inter_pr_desc_produto(ws_nivel_comp,
                            ws_grupo_comp,
                            ws_subgru_comp,
                            ws_item_comp,
                            ws_desc_comp_n,
                            ws_narrativa);

      -- DESCRICAO PARTE DA PECA
      begin
         select basi_018.descricao_parte_peca
         into   ws_descricao_parte_peca
         from basi_018
         where basi_018.codigo_parte_peca = :new.codigo_parte_peca
           and rownum                     = 1;
      exception when no_data_found then
         ws_descricao_parte_peca := '';
      end;

      -- DESCRICAO PARTE DA ALTERNATIVA
      begin
         select basi_070.descricao
         into   ws_descricao_alternativa
         from basi_070
         where  basi_070.nivel       = :new.nivel_item
           and (basi_070.grupo       = :new.grupo_item
           or   basi_070.grupo       = '00000')
           and (basi_070.subgrupo    = :new.subgru_item
           or   basi_070.subgrupo    = '000')
           and (basi_070.item        = :new.item_item
           or   basi_070.item        = '000000')
           and  basi_070.alternativa = :new.alternativa_produto
           and  basi_070.roteiro     = 0
           and  rownum               = 1
         order by basi_070.nivel DESC, basi_070.grupo DESC, basi_070.subgrupo DESC, basi_070.item DESC;
      exception when no_data_found then
         ws_descricao_alternativa := '';
      end;

      long_aux :=
            '                            ' ||
            inter_fn_buscar_tag('lb30101#PARTES DA PECA',ws_locale_usuario,ws_usuario_systextil) || '(019)' ||
            chr(10)                                               ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.codigo_projeto    ||
            ' - '                       || ws_desc_projeto_n      ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.nivel_item         ||
            '.'                         || :new.grupo_item         ||
            '.'                         || :new.subgru_item        ||
            '.'                         || :new.item_item          ||
            ' - '                       || ws_desc_prod_n          ||
            chr(10)                                                ||
            inter_fn_buscar_tag('lb00442#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || to_char(:new.alternativa_produto,'00')   ||
            ' - '                       || ws_descricao_alternativa  ||
            chr(10)                                               ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb08486#TIPO DE PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || ws_cod_tipo_produto_n  ||
            ' - '                       || ws_tipo_produto_n      ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb00523#COMPONENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :new.sequencia_estrutura      ||
            '.'                         || :new.sequencia_variacao       ||
            ' - '                       || ws_nivel_comp                 ||
            '.'                         || ws_grupo_comp                 ||
            '.'                         || ws_subgru_comp                ||
            '.'                         || ws_item_comp                  ||
            ' - '                       || ws_desc_comp_n                ||
            chr(10)                                                      ||
            inter_fn_buscar_tag('lb19574#PARTE DA PECA:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :new.codigo_parte_peca   ||
            ' - '                       || ws_descricao_parte_peca  ||
            chr(10);


      if :new.parte_conjunto <> :old.parte_conjunto
      then
         ws_teve := 's';

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb08190#PARTE CONJUNTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.parte_conjunto      || ' -> '
                                        || :new.parte_conjunto      ||
            chr(10);
      end if;

      if :new.qtde_parte_peca <> :old.qtde_parte_peca
      then
         ws_teve := 's';

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb37028#QTDE. PARTES:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.qtde_parte_peca     || ' -> '
                                        || :new.qtde_parte_peca     ||
            chr(10);
      end if;

      if :new.tipo_corte_peca <> :old.tipo_corte_peca
      then
         ws_teve := 's';

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb37029#TIPO CORTE:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.tipo_corte_peca     || ' -> '
                                        || :new.tipo_corte_peca     ||
            chr(10);
      end if;

      if :new.tipo_enfesto_1 <> :old.tipo_enfesto_1
      or :new.tipo_enfesto_2 <> :old.tipo_enfesto_2
      then
         ws_teve := 's';

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb29724#TIPO DE ENFESTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || '(1) -'                    || ' '
                                        || :old.tipo_enfesto_1        || '  '
                                        || '(2) -'                    || ' '
                                        || :old.tipo_enfesto_2        || ' -> '

                                        || '(1) -'                    || ' '
                                        || :new.tipo_enfesto_1        || '  '
                                        || '(2) -'                    || ' '
                                        || :new.tipo_enfesto_2        ||
            chr(10);
      end if;

      if :new.inf_corte_sentido <> :old.inf_corte_sentido
      or :new.inf_corte_casado  <> :old.inf_corte_casado
      or :new.inf_corte_desenho <> :old.inf_corte_desenho
      then
         ws_teve := 's';

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb37030#INFORMACOES DO CORTE:',ws_locale_usuario,ws_usuario_systextil)  ||
            chr(10)                                                 ||
            '      ' || inter_fn_buscar_tag('lb37031#SENTIDO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.inf_corte_sentido   || ' -> '
                                        || :new.inf_corte_sentido   ||
            chr(10)                                                 ||
            '      ' || inter_fn_buscar_tag('lb37032#CASADO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.inf_corte_casado    || ' -> '
                                        || :new.inf_corte_casado    ||
            chr(10)                                                 ||
            '      ' || inter_fn_buscar_tag('lb31575#DESENHO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.inf_corte_desenho   || ' -> '
                                        || :new.inf_corte_desenho   ||
            chr(10);
      end if;

      if :new.comprimento_debrum <> :old.comprimento_debrum
      then
         ws_teve := 's';

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb37033#COMP DEBRUM::',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.comprimento_debrum   || ' -> '
                                        || :new.comprimento_debrum   ||
            chr(10);
      end if;

      if :new.largura_debrum <> :old.largura_debrum
      then
         ws_teve := 's';

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb37034#LARGURA DEBRUM:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.largura_debrum      || ' -> '
                                        || :new.largura_debrum      ||
            chr(10);
      end if;

      if :new.numero_balancin_laser <> :old.numero_balancin_laser
      then
         ws_teve := 's';

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb37035#NUMERO  NAVALHA/LASER:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.numero_balancin_laser  || ' -> '
                                        || :new.numero_balancin_laser  ||
            chr(10);
      end if;

      if :new.observacoes <> :old.observacoes
      then
         ws_teve := 's';

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb29116#OBSERVACAO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.observacoes         || ' -> '
                                        || :new.observacoes         ||
            chr(10);
      end if;

      if ws_teve = 's'
      then
         begin
            INSERT INTO hist_100 (
               tabela,                   operacao,
               data_ocorr,               aplicacao,
               usuario_rede,             maquina_rede,

               str01,                    num05,
               str07,                    str08,
               str09,                    str10,
               num06,                    num07,
               num04,
               long01
            ) VALUES (
               'BASI_019',               'A',
               sysdate,                  ws_aplicativo,
               ws_usuario_rede,          ws_maquina_rede,

               :new.codigo_projeto,      :new.sequencia_projeto,
               :new.nivel_item,          :new.grupo_item,
               :new.subgru_item,         :new.item_item,
               :new.alternativa_produto, :new.sequencia_estrutura,
               :new.sequencia_variacao,
               long_aux
            );
         exception when OTHERS then
            raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;

   if deleting
   then
      -- DESCRICAO DO PROJETO
      begin
         select basi_001.descricao_projeto
         into ws_desc_projeto_n
         from basi_001
         where basi_001.codigo_projeto = :old.codigo_projeto
           and rownum                  = 1;
      exception when no_data_found then
         ws_desc_projeto_n := '';
      end;

      inter_pr_desc_produto(:old.nivel_item,
                            :old.grupo_item,
                            :old.subgru_item,
                            :old.item_item,
                            ws_desc_prod_n,
                            ws_narrativa);

      -- COMPONENTE QUE VAI EM CIMA DO TECIDO
      begin
         select basi_013.sequencia_subprojeto,  basi_013.nivel_comp,
                basi_013.grupo_comp,            basi_013.subgru_comp,
                basi_013.item_comp
         into   ws_sequencia_subprojeto,        ws_nivel_comp,
                ws_grupo_comp,                  ws_subgru_comp,
                ws_item_comp
         from basi_013
         where basi_013.codigo_projeto       = :old.codigo_projeto
           and basi_013.sequencia_projeto    = :old.sequencia_projeto
           and basi_013.nivel_item           = :old.nivel_item
           and basi_013.grupo_item           = :old.grupo_item
           and basi_013.subgru_item          = :old.subgru_item
           and basi_013.item_item            = :old.item_item
           and basi_013.alternativa_produto  = :old.alternativa_produto
           and basi_013.sequencia_estrutura  = :old.sequencia_estrutura
           and basi_013.sequencia_variacao   = :new.sequencia_variacao
           and rownum                        = 1;
      exception when no_data_found then
         ws_sequencia_subprojeto   := 0;
         ws_nivel_comp             := '0';
         ws_grupo_comp             := '00000';
         ws_subgru_comp            := '000';
         ws_item_comp              := '000000';
      end;

      -- ENCONTRA O TIPO_PRODUTO DA SEQUENCIA
      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :old.codigo_projeto
           and basi_101.sequencia_projeto = ws_sequencia_subprojeto
           and rownum                     = 1;
      exception when no_data_found then
         ws_cod_tipo_produto_n := '';
      end;

      -- DESCRICAO DO TIPO DE PRODUTO
      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n
           and rownum                       = 1;
      exception when no_data_found then
         ws_tipo_produto_n := '';
      end;

      -- DESCRICAO DO NOVO COMPONENTE
      inter_pr_desc_produto(ws_nivel_comp,
                            ws_grupo_comp,
                            ws_subgru_comp,
                            ws_item_comp,
                            ws_desc_comp_n,
                            ws_narrativa);

      -- DESCRICAO PARTE DA PECA
      begin
         select basi_018.descricao_parte_peca
         into   ws_descricao_parte_peca
         from basi_018
         where basi_018.codigo_parte_peca = :old.codigo_parte_peca
           and rownum                     = 1;
      exception when no_data_found then
         ws_descricao_parte_peca := '';
      end;

      -- DESCRICAO PARTE DA ALTERNATIVA
      begin
         select basi_070.descricao
         into   ws_descricao_alternativa
         from basi_070
         where  basi_070.nivel       = :old.nivel_item
           and (basi_070.grupo       = :old.grupo_item
           or   basi_070.grupo       = '00000')
           and (basi_070.subgrupo    = :old.subgru_item
           or   basi_070.subgrupo    = '000')
           and (basi_070.item        = :old.item_item
           or   basi_070.item        = '000000')
           and  basi_070.alternativa = :old.alternativa_produto
           and  basi_070.roteiro     = 0
           and  rownum               = 1
         order by basi_070.nivel DESC, basi_070.grupo DESC, basi_070.subgrupo DESC, basi_070.item DESC;
      exception when no_data_found then
         ws_descricao_alternativa := '';
      end;

      begin
         INSERT INTO hist_100 (
            tabela,                   operacao,
            data_ocorr,               aplicacao,
            usuario_rede,             maquina_rede,

            str01,                    num05,
            str07,                    str08,
            str09,                    str10,
            num06,                    num07,
            num04,

            long01
         ) VALUES (
            'BASI_019',               'I',
            sysdate,                  ws_aplicativo,
            ws_usuario_rede,          ws_maquina_rede,

            :old.codigo_projeto,      :old.sequencia_projeto,
            :old.nivel_item,          :old.grupo_item,
            :old.subgru_item,         :old.item_item,
            :old.alternativa_produto, :old.sequencia_estrutura,
            :old.sequencia_variacao,
            '                            ' ||
            inter_fn_buscar_tag('lb30101#PARTES DA PECA',ws_locale_usuario,ws_usuario_systextil) || '(019)' ||
            chr(10)                                               ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.codigo_projeto    ||
            ' - '                       || ws_desc_projeto_n      ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.nivel_item        ||
            '.'                         || :old.grupo_item        ||
            '.'                         || :old.subgru_item       ||
            '.'                         || :old.item_item         ||
            ' - '                       || ws_desc_prod_n         ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb00442#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || to_char(:old.alternativa_produto,'00')   ||
            ' - '                       || ws_descricao_alternativa  ||
            chr(10)                                               ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb08486#TIPO DE PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || ws_cod_tipo_produto_n  ||
            ' - '                       || ws_tipo_produto_n      ||
            chr(10)                                               ||
            inter_fn_buscar_tag('lb00523#COMPONENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.sequencia_estrutura      ||
            '.'                         || :old.sequencia_variacao       ||
            ' - '                       || ws_nivel_comp                 ||
            '.'                         || ws_grupo_comp                 ||
            '.'                         || ws_subgru_comp                ||
            '.'                         || ws_item_comp                  ||
            ' - '                       || ws_desc_comp_n                ||
            chr(10)                                                      ||
            inter_fn_buscar_tag('lb19574#PARTE DA PECA:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.codigo_parte_peca   ||
            ' - '                       || ws_descricao_parte_peca  ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb08190#PARTE CONJUNTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.parte_conjunto      ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb37028#QTDE. PARTES:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.qtde_parte_peca     ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb37029#TIPO CORTE:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.tipo_corte_peca     ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb29724#TIPO DE ENFESTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || '(1) -'                    || ' '
                                        || :old.tipo_enfesto_1      || '  '
                                        || '(2) -'                    || ' '
                                        || :old.tipo_enfesto_2      ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb37030#INFORMACOES DO CORTE:',ws_locale_usuario,ws_usuario_systextil)  ||
            chr(10)                                                 ||
            '      ' || inter_fn_buscar_tag('lb37031#SENTIDO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.inf_corte_sentido   ||
            chr(10)                                                 ||
            '      ' || inter_fn_buscar_tag('lb37032#CASADO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.inf_corte_casado    ||
            chr(10)                                                 ||
            '      ' || inter_fn_buscar_tag('lb31575#DESENHO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.inf_corte_desenho   ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb37033#COMP DEBRUM::',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.comprimento_debrum   ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb37034#LARGURA DEBRUM:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.largura_debrum      ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb37035#NUMERO  NAVALHA/LASER:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.numero_balancin_laser  ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb29116#OBSERVACAO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                        || :old.observacoes         ||
            chr(10)
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100-D)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

end inter_tr_basi_019_log;

-- ALTER TRIGGER "INTER_TR_BASI_019_LOG" ENABLE
 

/

exec inter_pr_recompile;

