CREATE OR REPLACE TRIGGER INTER_TR_FATU_050_MENSAGENS
AFTER  INSERT or 
	   UPDATE of situacao_nfisc

on fatu_050

for each row

declare


  v_orig_pedido    pedi_100.pedido_venda%type;
  v_imprimeMsgInd varchar(1);
  v_insereMensagens varchar(1);


begin

  begin

  select empr_008.val_str
    into v_imprimeMsgInd
  from empr_008
  where empr_008.param = 'obrf.imprimeMsgInd'
    and empr_008.codigo_empresa = :new.codigo_empresa;
  exception
  when OTHERS then
    v_imprimeMsgInd := 'N';
  end;
  
  v_insereMensagens := 'S';
  
  if updating
  then 
    if :old.situacao_nfisc = 3 and :new.situacao_nfisc = 1
	then
	  v_insereMensagens := 'S';
	else
	  v_insereMensagens := 'N';
	end if;
  end if;

  if v_insereMensagens = 'S' and (:new.pedido_venda <> 0 or :new.origem_nota = 3)
  then

    v_orig_pedido := :new.pedido_venda;
    if :new.origem_nota = 3 then
       v_orig_pedido := :new.nr_solicitacao;
    end if;

    for msg_pedido in(

       select num_pedido,
              cod_mensagem,
			  seq_mensagem,
              des_mensagem1,
              des_mensagem2,
              des_mensagem3,
              des_mensagem4,
              des_mensagem5,
              des_mensagem6,
              des_mensagem7,
              des_mensagem8,
              des_mensagem9,
              des_mensagem10,
              ind_local
       from pedi_101
       where num_pedido = v_orig_pedido
         and origem_mensagem = decode(:new.origem_criacao,0,0,99)
         and (v_imprimeMsgInd = 'S' or
                (v_imprimeMsgInd = 'N' and exists (select 1 from pedi_080 
                     where pedi_080.natur_operacao = :new.natop_nf_nat_oper
                     and   pedi_080.estado_natoper = :new.natop_nf_est_oper
                     and   pedi_080.cod_natureza || pedi_080.divisao_natur not in ('5.925', '6.925', '5.902', '6.902', '5.903', '6.903'))
                )
             )
                     --CFOP's de retorno de industrialização.
    )

    loop
         begin
           insert into fatu_052
           (cod_empresa,
            num_nota,
            cod_serie_nota,
            cod_mensagem,
			seq_mensagem,
            cnpj9,
            cnpj4,
            cnpj2,
            ind_entr_saida,
            ind_local,
            des_mensag_1,
            des_mensag_2,
            des_mensag_3,
            des_mensag_4,
            des_mensag_5,
            des_mensag_6,
            des_mensag_7,
            des_mensag_8,
            des_mensag_9,
            des_mensag_10)
           values
           (:new.codigo_empresa,
            :new.num_nota_fiscal,
            :new.serie_nota_fisc,
            msg_pedido.cod_mensagem,
			msg_pedido.seq_mensagem,
            :new.cgc_9,
            :new.cgc_4,
            :new.cgc_2,
            'S',
            msg_pedido.ind_local,
            msg_pedido.des_mensagem1,
            msg_pedido.des_mensagem2,
            msg_pedido.des_mensagem3,
            msg_pedido.des_mensagem4,
            msg_pedido.des_mensagem5,
            msg_pedido.des_mensagem6,
            msg_pedido.des_mensagem7,
            msg_pedido.des_mensagem8,
            msg_pedido.des_mensagem9,
            msg_pedido.des_mensagem10);
          exception
             WHEN dup_val_on_index THEN
               NULL;
             when others then
               raise_application_error(-20000,'Erro ao inserir Mensagem do Pedido da nota');
          end;

    end loop;

  end if;

end INTER_TR_FATU_050_MENSAGENS;

-- ALTER TRIGGER "INTER_TR_FATU_050_MENSAGENS" ENABLE
 

/

exec inter_pr_recompile;

/* versao: 33 */


 exit;


 exit;


 exit;


 exit;
