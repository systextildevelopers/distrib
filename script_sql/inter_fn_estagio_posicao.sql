
  CREATE OR REPLACE FUNCTION "INTER_FN_ESTAGIO_POSICAO" (p_ordem_producao in number,p_ordem_confeccao in number)
return number
is


   qtde_prod_ant_aux number;
   estagio_posicao   number:=0;
BEGIN
   /********************************************************************************************************
      ENCONTRAR O ESTAGIO POSICAO DA ORDEM DE CORTE
    *********************************************************************************************************/
   for reg_pcpc040 in (select pcpc_040.periodo_producao,
             pcpc_040.codigo_estagio,          pcpc_040.qtde_pecas_prog,
             pcpc_040.qtde_pecas_prod,         pcpc_040.qtde_conserto,
             pcpc_040.qtde_pecas_2a,           pcpc_040.qtde_perdas,
             pcpc_040.estagio_anterior
   from pcpc_040
   where  pcpc_040.ordem_producao  = p_ordem_producao
     and (pcpc_040.ordem_confeccao = p_ordem_confeccao
      or  p_ordem_confeccao       = 0)
   order by pcpc_040.seq_operacao      ASC,
            pcpc_040.sequencia_estagio ASC,
            pcpc_040.codigo_estagio    ASC)
   loop
      estagio_posicao := reg_pcpc040.codigo_estagio;

      /* INICIO - Verifica em producao no estagio */
      begin
         select nvl(min(pcpc_040.qtde_pecas_prod),0)
         into qtde_prod_ant_aux
         from pcpc_040
         where pcpc_040.periodo_producao = reg_pcpc040.periodo_producao
         and   pcpc_040.ordem_confeccao  = p_ordem_confeccao
         and   pcpc_040.codigo_estagio   = reg_pcpc040.estagio_anterior;
         exception when others
         then qtde_prod_ant_aux := 0;
      end;

      if qtde_prod_ant_aux is null
      then
         qtde_prod_ant_aux := reg_pcpc040.qtde_pecas_prog;
      end if;

      if (qtde_prod_ant_aux - (reg_pcpc040.qtde_pecas_prod +
                               reg_pcpc040.qtde_pecas_2a   +
                               reg_pcpc040.qtde_perdas     +
                               reg_pcpc040.qtde_conserto) > 0)
      then
         exit;
      end if;
      /* FIM - Verifica em producao no estagio */
   end loop;

   if estagio_posicao is null
   or estagio_posicao = 0
   then
      estagio_posicao := 999;
   end if;

   return ( estagio_posicao );

END inter_fn_estagio_posicao;
 

/

exec inter_pr_recompile;

