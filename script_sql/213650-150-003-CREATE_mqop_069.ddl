CREATE TABLE mqop_069 (
        grupo_maq   VARCHAR2(4) NOT NULL ENABLE,
        sub_maq     VARCHAR2(3) NOT NULL ENABLE,
        cod_tipo    NUMBER(3) NOT NULL ENABLE,
        tipo        VARCHAR2(1) NOT NULL ENABLE,
    CONSTRAINT "mqop_069_grupo_maq_PK" PRIMARY KEY (grupo_maq,sub_maq,cod_tipo) ENABLE
);

COMMENT ON COLUMN mqop_069.grupo_maq IS 'Grupo de maquina mqop_020.grupo_maquina(CHAVE)';

COMMENT ON COLUMN mqop_069.sub_maq IS 'SubGrupo maquina mqop_020.subgrupo_maquina(CHAVE)';

COMMENT ON COLUMN mqop_069.cod_tipo IS 'Familia carretel endr_011.cod_tipo(CHAVE)';

COMMENT ON COLUMN mqop_069.tipo IS '(S ou E) Se é de entrada ou saida';

ALTER TABLE mqop_069 ADD CONSTRAINT "REF_MQOP_069_MQOP_020" FOREIGN KEY (grupo_maq,sub_maq)
REFERENCES mqop_020 (grupo_maquina,subgrupo_maquina) ENABLE;

CREATE INDEX mqop_069_IDX_grupo_maq ON mqop_069 (grupo_maq);

CREATE INDEX mqop_069_IDX_sub_maq ON mqop_069 (sub_maq);

CREATE INDEX mqop_069_IDX_cod_tipo ON mqop_069 (cod_tipo);
