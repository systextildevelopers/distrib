create or replace TRIGGER "INTER_TR_PCPT_020_VAL_ROMANEIO" 
/*
   30/09/2024     MMB - ID_6873 - 243500 - 148359 - Criar validação de pré romaneio
                  Mário André Torriani zanchi
   18/10/2024     MMB 001 - Alterado por solicitação por Mario Andre Torriani zanchi               
*/
   before --insert or
          --delete or
          update of rolo_estoque, qtde_quilos_acab, mt_lineares_prod, codigo_deposito, pre_romaneio
   on PCPT_020
   for each row
declare   
   z_erro  varchar2 (200) := 'Atenção! Rolo está relacionado com um pré-romaneio bloqueado, não pode fazer alteração. Solicite o desbloqueio para continuar o processo!';
   z_situacao     number (1);
   z_libera_alt_pcpt_020     number (1);
   z_empresa                 number (5);
begin
    for A in (select situacao
                from ESTQ_090
               where pre_romaneio = :new.pre_romaneio)
    loop
       z_situacao := A.situacao;
    end loop;
    --
    for A in (select local_deposito 
                from BASI_205 
               where codigo_deposito = :new.codigo_deposito)
    loop
       z_empresa := A.local_deposito;
    end loop;
    --
    for A in (select libera_alt_pcpt_020
                from FATU_503
               where CODIGO_EMPRESA = z_empresa)
    loop
       z_libera_alt_pcpt_020 := A.libera_alt_pcpt_020;
    end loop;
    z_libera_alt_pcpt_020 := nvl(z_libera_alt_pcpt_020,0);
    --
    --dbms_output.put_line('z_situacao=' || z_situacao || '   z_libera_alt_pcpt_020=' || z_libera_alt_pcpt_020 || '   :old.cod_empresa_nota=' || :old.cod_empresa_nota);

    if updating and z_situacao = 1 and z_libera_alt_pcpt_020 = 1 then  -- Para liberar alteração na PCPT_020    0 = Não valida 1 = valida
       if nvl(:old.rolo_estoque,-1) <> nvl(:new.rolo_estoque,-1) then
          RAISE_APPLICATION_ERROR (-20201, z_erro);
       end  if; 
       --
       if nvl(:old.qtde_quilos_acab,-1) <> nvl(:new.qtde_quilos_acab,-1) then
          RAISE_APPLICATION_ERROR (-20201, z_erro);
       end  if; 
       --
       if nvl(:old.mt_lineares_prod,-1) <> nvl(:new.mt_lineares_prod,-1) then
          RAISE_APPLICATION_ERROR (-20201, z_erro);
       end  if; 
       --
       if nvl(:old.codigo_deposito,-1) <> nvl(:new.codigo_deposito,-1) then
          RAISE_APPLICATION_ERROR (-20201, z_erro);
       end  if; 
       --
       -- 001 - Incluido por solicitação em 18/10/2024 por Mário Andre Torriani zanchi
       if nvl(:old.pre_romaneio,-1) <> nvl(:new.pre_romaneio,-1) then
          RAISE_APPLICATION_ERROR (-20201, z_erro);
       end  if; 

    end if;   
end;
