alter table pedi_155 add id number(9);
alter table pedi_155 add qtde_permitida_nuance_minima number(9) default 1;
alter table pedi_155 add qtde_minima_metros_nuance number(12,3) default 500;
alter table pedi_155 add qtde_tolerancia_metros_nuance number(9,2) default 10;

create sequence pedi_155_seq;

update pedi_155 set id = pedi_155_seq.nextval where id is null;
