alter table cont_k200
add (IND_CTA_aux    varchar2(1) default ' ',
     IND_VAL_CS_aux varchar2(1) default ' ',
     IND_VAL_AG_aux varchar2(1) default ' ',
     IND_VAL_EL_aux varchar2(1) default ' ');
     
update cont_k200
set IND_CTA_aux    = IND_CTA,
    IND_VAL_CS_aux = IND_VAL_CS,
    IND_VAL_AG_aux = IND_VAL_AG,
    IND_VAL_EL_aux = IND_VAL_EL;
COMMIT;

alter table cont_k200
drop (IND_CTA,
      IND_VAL_CS,
      IND_VAL_AG,
      IND_VAL_EL);
     
alter table cont_k200
add (IND_CTA    varchar2(1) default ' ',
     IND_VAL_CS varchar2(1) default ' ',
     IND_VAL_AG varchar2(1) default ' ',
     IND_VAL_EL varchar2(1) default ' ');

alter table cont_k200     
add constraint ck_IND_CTA CHECK (IND_CTA IN ('S','A', ' '));

alter table cont_k200     
add constraint ck_IND_VAL_CS CHECK (IND_VAL_CS IN ('D','C',' '));

alter table cont_k200     
add constraint ck_IND_VAL_AG CHECK (IND_VAL_AG IN ('D','C',' '));

alter table cont_k200     
add constraint ck_IND_VAL_EL CHECK (IND_VAL_EL IN ('D','C',' '));

update cont_k200
set IND_CTA    = IND_CTA_aux,
    IND_VAL_CS = IND_VAL_CS_aux,
    IND_VAL_AG = IND_VAL_AG_aux,
    IND_VAL_EL = IND_VAL_EL_aux;
COMMIT;

alter table cont_k200
drop (IND_CTA_aux,
      IND_VAL_CS_aux,
      IND_VAL_AG_aux,
      IND_VAL_EL_aux);
      
