create or replace package ST_PCK_RELAC_CONTABIL as
    
	TYPE t_dados_cont_550 IS TABLE OF cont_550%rowtype;
	
	function get(	p_cod_plano_cta NUMBER, 	p_tipo_contabil NUMBER,
					p_codigo_contabil NUMBER,	p_transacao NUMBER,
                    p_msg_erro in out varchar2) return t_dados_cont_550;

	function lista_relacionamentos(p_cod_plano_cta number, p_msg_erro in out varchar2) return t_dados_cont_550;
		
    procedure insere_relacionamento (	p_cod_plano_cta NUMBER, 	p_tipo_contabil NUMBER,
                                        p_codigo_contabil NUMBER,	p_transacao NUMBER,
                                        p_conta_contabil NUMBER);
end ST_PCK_RELAC_CONTABIL;
