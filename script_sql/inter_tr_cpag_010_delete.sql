create or replace trigger "INTER_TR_CPAG_010_DELETE"
after delete
on cpag_010
for each row
declare

v_existe number(1);

begin

   select count(1)
   into v_existe
   from hdoc_110
   where hdoc_110.empresa = :old.codigo_empresa
     and hdoc_110.documento = :old.nr_duplicata
     and hdoc_110.serie_parcela = :old.parcela
     and hdoc_110.tipo_titulo = :old.tipo_titulo
     and hdoc_110.cgc9 = :old.cgc_9
     and hdoc_110.cgc4 = :old.cgc_4
     and hdoc_110.cgc2 = :old.cgc_2
     and hdoc_110.codigo_processo = 4;

   if v_existe > 0
   then
      delete
      from hdoc_110
      where hdoc_110.empresa = :old.codigo_empresa
        and hdoc_110.documento = :old.nr_duplicata
        and hdoc_110.serie_parcela = :old.parcela
        and hdoc_110.tipo_titulo = :old.tipo_titulo
        and hdoc_110.cgc9 = :old.cgc_9
        and hdoc_110.cgc4 = :old.cgc_4
        and hdoc_110.cgc2 = :old.cgc_2
        and hdoc_110.codigo_processo = 4;
   end if;

end INTER_TR_CPAG_010_DELETE;
