alter table fatu_070
add( 
  FND_SITUACAO        NUMBER(1),
  FND_NR_REMESSA      NUMBER(9),
  FND_DT_REMESSA      DATE
);

alter table fatu_070
modify FND_SITUACAO default 0;

alter table fatu_070
modify FND_NR_REMESSA default 0;

comment on column fatu_070.FND_NR_REMESSA   is 'N�mero da remessa para Fundo';

comment on column fatu_070.FND_SITUACAO  is 'Situacao do titulo junto ao Fundo, 0 - N�o faz parte do fundo , 1 - Selecionado para Remessa, 3 - Aprovado pelo Fundo remessa ou 5 - Gerada Remessa';

comment on column fatu_070.FND_DT_REMESSA   is 'Data da remessa';

exec inter_pr_recompile;
/
