create table SUPR_011
(
  data_inicial     DATE not null,
  data_final       DATE not null,
  cnpj_fornecedor9 NUMBER(9) default 0 not null,
  cnpj_fornecedor4 NUMBER(4) default 0 not null,
  cnpj_fornecedor2 NUMBER(2) default 0 not null
);
/
 
alter table SUPR_011
  add constraint PK_SUPR_011 primary key (CNPJ_FORNECEDOR9, CNPJ_FORNECEDOR4, CNPJ_FORNECEDOR2);
/

exec inter_pr_recompile;
/
