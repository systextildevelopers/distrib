create or replace procedure inter_pr_elimina_tmrp_615_108 is
begin

  delete tmrp_615_108
  where tmrp_615_108.data_criacao < trunc(sysdate,'DD') - 2;
  commit;

end inter_pr_elimina_tmrp_615_108;

/

exec inter_pr_recompile;

/* versao: 1 */

