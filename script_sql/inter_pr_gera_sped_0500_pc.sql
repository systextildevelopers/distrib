create or replace procedure inter_pr_gera_sped_0500_pc(p_cod_empresa   IN  NUMBER,
                                                       p_cod_matriz    IN  NUMBER,
                                                       p_cnpj9_empresa IN  NUMBER,
                                                       p_cnpj4_empresa IN  NUMBER,
                                                       p_cnpj2_empresa IN  NUMBER,
                                                       p_dat_inicial   IN DATE,
                                                       p_des_erro      OUT varchar2) is

w_erro                EXCEPTION;

CURSOR u_pc_0500 (p_cod_matriz      NUMBER,
                  p_dat_inicial     IN DATE) IS
   select distinct data_alter_cc,       decode(cod_nat_cc,6,5,cod_nat_cc) cod_nat_cc,
                   ind_cta,             nivel_cc,
                   num_conta_contabil,  nvl(cod_cta_ref, ' ') cod_cta_ref,
                   nome_cta
   from (
         (select distinct cont_535.data_alteracao data_alter_cc,      cont_535.indic_natu_sped cod_nat_cc,
                          trim(to_char(cont_535.tipo_conta,'0')) ind_cta,                cont_535.nivel nivel_cc,
                          to_char(cont_535.cod_reduzido) num_conta_contabil, cont_535.conta_contabil_ref cod_cta_ref,
                          cont_535.descricao nome_cta
          from cont_535, obrf_708
          where cont_535.cod_plano_cta   = obrf_708.cod_plano_cta
            and cont_535.cod_reduzido    = obrf_708.conta_contabil
            and obrf_708.conta_contabil <> '0'
            and obrf_708.conta_contabil is not null
            and obrf_708.cod_matriz      = p_cod_matriz
            and obrf_708.mes_apur        = to_char(p_dat_inicial, 'MM')
            and obrf_708.ano_apur        = to_char(p_dat_inicial, 'YYYY'))
   union all
         (select distinct cont_535.data_cadastro as data_alter_cc,               cont_535.indic_natu_sped as cod_nat_cc,
                          trim(to_char(cont_535.tipo_conta)) as ind_cta,               cont_535.nivel as nivel_cc,
                          to_char(cont_535.cod_reduzido) as num_conta_contabil,  cont_535.conta_contabil_ref as cod_cta_ref,
                          cont_535.descricao as nome_cta
          from sped_pc_1900, fatu_500, cont_535, cont_500
          where fatu_500.codigo_empresa            = sped_pc_1900.cod_empresa
            and fatu_500.codigo_matriz             = p_cod_matriz
            and to_char(sped_pc_1900.dat_apuracao,'MM') = to_char(p_dat_inicial, 'MM')
            and to_char(sped_pc_1900.dat_apuracao,'YYYY')= to_char(p_dat_inicial, 'YYYY')
            and cont_500.cod_empresa               = fatu_500.codigo_matriz
            and cont_500.exercicio                 = inter_fn_checa_data(fatu_500.codigo_matriz, p_dat_inicial, 0)
            and cont_535.cod_plano_cta             = cont_500.cod_plano_cta
            and to_char(cont_535.cod_reduzido)     = sped_pc_1900.conta_contabil)
   union all
         (select distinct cont_535.data_cadastro as data_alter_cc,               cont_535.indic_natu_sped as cod_nat_cc,
                          trim(to_char(cont_535.tipo_conta)) as ind_cta,               cont_535.nivel as nivel_cc,
                          to_char(cont_535.cod_reduzido) as num_conta_contabil,  cont_535.conta_contabil_ref as cod_cta_ref,
                          cont_535.descricao as nome_cta
          from obrf_711, fatu_500, cont_535, cont_500
          where fatu_500.codigo_empresa            = obrf_711.cod_empresa
            and fatu_500.codigo_matriz             = p_cod_matriz
            and obrf_711.per_apu_cred_mes          = to_char(p_dat_inicial, 'MM')
            and obrf_711.per_apu_cred_ano          = to_char(p_dat_inicial, 'YYYY')
            and cont_500.cod_empresa               = fatu_500.codigo_matriz
            and cont_500.exercicio                 = inter_fn_checa_data(fatu_500.codigo_matriz, p_dat_inicial, 0)
            and cont_535.cod_plano_cta             = cont_500.cod_plano_cta
            and to_char(cont_535.cod_reduzido)     = obrf_711.cod_cta)
   union all
         (select distinct cont_535.data_cadastro as data_alter_cc,               cont_535.indic_natu_sped as cod_nat_cc,
                          trim(to_char(cont_535.tipo_conta)) as ind_cta,               cont_535.nivel as nivel_cc,
                          to_char(cont_535.cod_reduzido) as num_conta_contabil,  cont_535.conta_contabil_ref as cod_cta_ref,
                          cont_535.descricao as nome_cta
          from obrf_302, fatu_500, cont_535, cont_500
          where fatu_500.codigo_empresa            = obrf_302.cod_empresa
            and fatu_500.codigo_matriz             = p_cod_matriz
            and obrf_302.mes_apur                  = to_char(p_dat_inicial, 'MM')
            and obrf_302.ano_apur                  = to_char(p_dat_inicial, 'YYYY')
            and cont_500.cod_empresa               = fatu_500.codigo_matriz
            and cont_500.exercicio                 = inter_fn_checa_data(fatu_500.codigo_matriz, p_dat_inicial, 0)
            and cont_535.cod_plano_cta             = cont_500.cod_plano_cta
            and to_char(cont_535.cod_reduzido)     = trim(obrf_302.cod_cta))
   union all
         (select distinct cont_535.data_cadastro as data_alter_cc,               cont_535.indic_natu_sped as cod_nat_cc,
                          trim(to_char(cont_535.tipo_conta)) as ind_cta,               cont_535.nivel as nivel_cc,
                          to_char(cont_535.cod_reduzido) as num_conta_contabil,  cont_535.conta_contabil_ref as cod_cta_ref,
                          cont_535.descricao as nome_cta
          from obrf_700, fatu_500, cont_535, cont_500
          where fatu_500.codigo_empresa            = obrf_700.cod_empresa
            and fatu_500.codigo_matriz             = p_cod_matriz
            and obrf_700.mes_apur                  = to_char(p_dat_inicial, 'MM')
            and obrf_700.ano_apur                  = to_char(p_dat_inicial, 'YYYY')
            and cont_500.cod_empresa               = fatu_500.codigo_matriz
            and cont_500.exercicio                 = inter_fn_checa_data(fatu_500.codigo_matriz, p_dat_inicial, 0)
            and cont_535.cod_plano_cta             = cont_500.cod_plano_cta
            and to_char(cont_535.cod_reduzido)     = trim(obrf_700.cod_cta)
            and obrf_700.ind_orig_cred             < 3)
   union all
         (select distinct cont_535.data_cadastro as data_alter_cc,               cont_535.indic_natu_sped as cod_nat_cc,
                          trim(to_char(cont_535.tipo_conta)) as ind_cta,               cont_535.nivel as nivel_cc,
                          to_char(cont_535.cod_reduzido) as num_conta_contabil,  cont_535.conta_contabil_ref as cod_cta_ref,
                          cont_535.descricao as nome_cta
          from obrf_707, fatu_500, cont_535, cont_500
          where fatu_500.codigo_empresa            = obrf_707.cod_empresa
            and fatu_500.codigo_matriz             = p_cod_matriz
            and obrf_707.mes_apur                  = to_char(p_dat_inicial, 'MM')
            and obrf_707.ano_apur                  = to_char(p_dat_inicial, 'YYYY')
            and cont_500.cod_empresa               = fatu_500.codigo_matriz
            and cont_500.exercicio                 = inter_fn_checa_data(fatu_500.codigo_matriz, p_dat_inicial, 0)
            and cont_535.cod_plano_cta             = cont_500.cod_plano_cta
            and to_char(cont_535.cod_reduzido)     = trim(obrf_707.cod_cta))
   union all
         (select distinct cont_535.data_cadastro as data_alter_cc,               cont_535.indic_natu_sped as cod_nat_cc,
                          trim(to_char(cont_535.tipo_conta)) as ind_cta,               cont_535.nivel as nivel_cc,
                          to_char(cont_535.cod_reduzido) as num_conta_contabil,  cont_535.conta_contabil_ref as cod_cta_ref,
                          cont_535.descricao as nome_cta
               from sped_pc_p100, fatu_500, cont_535, cont_500
               where fatu_500.codigo_empresa            = sped_pc_p100.cod_empresa
                 and fatu_500.codigo_matriz             = p_cod_matriz
                 and cont_500.cod_empresa               = fatu_500.codigo_matriz
                 and cont_500.exercicio                 = inter_fn_checa_data(fatu_500.codigo_matriz, p_dat_inicial, 0)
                 and cont_535.cod_plano_cta             = cont_500.cod_plano_cta
                 and to_char(cont_535.cod_reduzido)     = sped_pc_p100.cod_cta
                 and sped_pc_p100.cod_cta is not null
                 and sped_pc_p100.cod_cta <> '0')
   union all
         (select distinct cont_535.data_cadastro as data_alter_cc,               cont_535.indic_natu_sped as cod_nat_cc,
                          trim(to_char(cont_535.tipo_conta)) as ind_cta,               cont_535.nivel as nivel_cc,
                          to_char(cont_535.cod_reduzido) as num_conta_contabil,  cont_535.conta_contabil_ref as cod_cta_ref,
                          cont_535.descricao as nome_cta
          from obrf_721, fatu_500, cont_535, cont_500
          where fatu_500.codigo_empresa            = obrf_721.cod_empresa
            and fatu_500.codigo_matriz             = p_cod_matriz
            and obrf_721.mes_apur                  = to_char(p_dat_inicial, 'MM')
            and obrf_721.ano_apur                  = to_char(p_dat_inicial, 'YYYY')
            and cont_500.cod_empresa               = fatu_500.codigo_matriz
            and cont_500.exercicio                 = inter_fn_checa_data(fatu_500.codigo_matriz, p_dat_inicial, 0)
            and cont_535.cod_plano_cta             = cont_500.cod_plano_cta
            and to_char(cont_535.cod_reduzido)     = trim(obrf_721.cod_cta)))
   where num_conta_contabil > 0
   group by data_alter_cc,       cod_nat_cc,
            ind_cta,             nivel_cc,
            num_conta_contabil,  cod_cta_ref,
            nome_cta;
BEGIN


   p_des_erro := NULL;

   FOR pc_0500 IN u_pc_0500 (p_cod_matriz, p_dat_inicial) LOOP
      begin
         insert into sped_pc_0500
            (cod_matriz
             ,cod_empresa
             ,num_cnpj_empr9
             ,num_cnpj_empr4
             ,num_cnpj_empr2
             ,data_alter_cc
             ,cod_nat_cc
             ,ind_cta
             ,nivel_cc
             ,num_conta_contabil
             ,cod_cta_ref
             ,nome_cta)
         values
            (p_cod_matriz
            ,p_cod_empresa
            ,p_cnpj9_empresa
            ,p_cnpj4_empresa
            ,p_cnpj2_empresa
            ,pc_0500.data_alter_cc
            ,pc_0500.cod_nat_cc
            ,pc_0500.ind_cta
            ,pc_0500.nivel_cc
            ,pc_0500.num_conta_contabil
            ,pc_0500.cod_cta_ref
            ,pc_0500.nome_cta);
      exception
         when others then
             p_des_erro := 'N�o inseriu dados na tabela sped_pc_0500' || Chr(10) || SQLERRM;
      end;

   END LOOP;

END inter_pr_gera_sped_0500_pc;
/
exec inter_pr_recompile;
