create or replace package body "ST_PCK_RELAC_CONTABIL_CC" is

    function get_dados_cont_555(p_cod_plano_conta number, p_cod_reduzido number, p_msg_erro in out varchar2) 
    return t_dados_cont_555 
    as
        v_dados_cont_555 t_dados_cont_555;
    begin
        begin
            select *
            bulk collect into v_dados_cont_555
            from cont_555
            where cod_plano_conta = p_cod_plano_conta
            and cod_reduzido = p_cod_reduzido;
        exception when others then
            p_msg_erro := 'Não foi possível buscar o plano de conta!.   cod_plano_cta:' || p_cod_plano_conta
                                                                        ||' cod_reduzido: '|| p_cod_reduzido;
        end;
        
        return v_dados_cont_555;
    end get_dados_cont_555;

    function relac_cont_cc(p_cod_plano_conta number, p_msg_erro in out varchar2) 
    return t_dados_cont_555 
    as
        v_dados_cont_555 t_dados_cont_555;
    begin
        begin
            select *
            bulk collect into v_dados_cont_555
            from cont_555
            where cod_plano_conta = p_cod_plano_conta;
        exception when others then
            p_msg_erro := 'Não foi possível buscar o plano de conta!. cod_plano_cta:' || p_cod_plano_conta;
        end;

        return v_dados_cont_555;
    end relac_cont_cc;

end "ST_PCK_RELAC_CONTABIL_CC";
