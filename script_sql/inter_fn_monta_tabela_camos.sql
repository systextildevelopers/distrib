
  CREATE OR REPLACE FUNCTION "INTER_FN_MONTA_TABELA_CAMOS" ( pdiscos1 in varchar2,
                                                        pdiscos2 in varchar2,
                                                        pdiscos3 in varchar2,
                                                        pdiscos4 in varchar2,
                                                        pdiscos5 in varchar2,
                                                        pcilindros1 in varchar2,
                                                        pcilindros2 in varchar2,
                                                        pcilindros3 in varchar2,
                                                        pcilindros4 in varchar2,
                                                        pcilindros5 in varchar2 )
return
  array_camos
is
  v_table array_camos;
  tamanho_tabela number(3);
  v_count number(3);

  discos1 mqop_060.camos_disco1%type;
  discos2 mqop_060.camos_disco2%type;
  discos3 mqop_060.camos_disco3%type;
  discos4 mqop_060.camos_disco4%type;
  discos5 mqop_060.camos_disco5%type;

  cilindros1 mqop_060.camos_cilindro1%type;
  cilindros2 mqop_060.camos_cilindro2%type;
  cilindros3 mqop_060.camos_cilindro3%type;
  cilindros4 mqop_060.camos_cilindro4%type;
  cilindros5 mqop_060.camos_cilindro5%type;

begin
  v_table := array_camos();

  discos1 := replace(pdiscos1, ' ', '');
  discos2 := replace(pdiscos2, ' ', '');
  discos3 := replace(pdiscos3, ' ', '');
  discos4 := replace(pdiscos4, ' ', '');
  discos5 := replace(pdiscos5, ' ', '');

  cilindros1 := replace(pcilindros1, ' ', '');
  cilindros2 := replace(pcilindros2, ' ', '');
  cilindros3 := replace(pcilindros3, ' ', '');
  cilindros4 := replace(pcilindros4, ' ', '');
  cilindros5 := replace(pcilindros5, ' ', '');

  select nvl(max(tamanho), 0)
  into tamanho_tabela
  from (
  select length(discos1)     tamanho from dual
  UNION
  select length(discos2)    tamanho from dual
  UNION
  select length(discos3)    tamanho from dual
  UNION
  select length(discos4)    tamanho from dual
  UNION
  select length(discos5)    tamanho from dual
  UNION
  select length(cilindros1) tamanho from dual
  UNION
  select length(cilindros2) tamanho from dual
  UNION
  select length(cilindros3) tamanho from dual
  UNION
  select length(cilindros4) tamanho from dual
  UNION
  select length(cilindros5) tamanho from dual
  );

  if tamanho_tabela > 0
  then
     v_table.extend(tamanho_tabela);
     v_count := 0;
     WHILE v_count < tamanho_tabela
     LOOP
        v_count := v_count + 1;
        v_table(v_count) := camos(v_count,
                                  substr(discos1, v_count, 1),
                                  substr(discos2, v_count, 1),
                                  substr(discos3, v_count, 1),
                                  substr(discos4, v_count, 1),
                                  substr(discos5, v_count, 1),
                                  substr(cilindros1, v_count, 1),
                                  substr(cilindros2, v_count, 1),
                                  substr(cilindros3, v_count, 1),
                                  substr(cilindros4, v_count, 1),
                                  substr(cilindros5, v_count, 1)
                                  );
     END LOOP;
  else
     v_table.extend(1);
     v_table(1) := camos(1, '', '', '', '', '', '', '', '', '', '');
  end if;
  return v_table;
end;

 

/

exec inter_pr_recompile;

