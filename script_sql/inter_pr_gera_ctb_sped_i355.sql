create or replace procedure inter_pr_gera_ctb_sped_i355(p_cod_empresa   IN NUMBER,
                                                        p_exercicio     IN NUMBER,
                                                        p_plano_cta     IN NUMBER,
                                                        p_form_apur     IN VARCHAR2,
                                                        p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar a tabela sped_cta_i355 - Saldos das Contas de Resultado antes do zeramento
   --
   -- Historicos
   --
   -- Data    Autor    Observacoes
   --

   cursor cont600(p_cod_empresa NUMBER, p_exercicio NUMBER, p_plano_cta NUMBER, p_form_apur VARCHAR, p_ind_considera_c_custos VARCHAR) IS
      select cont_600.conta_contabil,      cont_600.conta_reduzida, decode(p_ind_considera_c_custos, 'N', 0, cont_600.centro_custo) centro_custo,
             decode(p_form_apur,'T',decode(cont_600.periodo,1,'1',
             decode(cont_600.periodo,2,'1',
             decode(cont_600.periodo,3,'1',
             decode(cont_600.periodo,4,'2',
             decode(cont_600.periodo,5,'2',
             decode(cont_600.periodo,6,'2',
             decode(cont_600.periodo,7,'3',
             decode(cont_600.periodo,8,'3',
             decode(cont_600.periodo,9,'3',
             decode(cont_600.periodo,10,'4',
             decode(cont_600.periodo,11,'4',
             decode(cont_600.periodo,12,'4')))))))))))),'12')   trimestre,
             sum(decode(cont_600.debito_credito, 'D', cont_600.valor_lancto, cont_600.valor_lancto * (-1.0))) TOTAL_VALOR
      from cont_600,cont_535
      where cont_600.cod_empresa    = p_cod_empresa
        and cont_600.exercicio      = p_exercicio
        and cont_600.conta_contabil = cont_535.conta_contabil
        and cont_535.cod_plano_cta  = p_plano_cta
        and cont_535.patr_result    = 2
        and cont_600.origem       <> 20
      group by cont_600.conta_contabil, cont_600.conta_reduzida,decode(p_ind_considera_c_custos, 'N', 0, cont_600.centro_custo),
               decode(p_form_apur,'T',decode(cont_600.periodo,1,'1',
               decode(cont_600.periodo,2,'1',
               decode(cont_600.periodo,3,'1',
               decode(cont_600.periodo,4,'2',
               decode(cont_600.periodo,5,'2',
               decode(cont_600.periodo,6,'2',
               decode(cont_600.periodo,7,'3',
               decode(cont_600.periodo,8,'3',
               decode(cont_600.periodo,9,'3',
               decode(cont_600.periodo,10,'4',
               decode(cont_600.periodo,11,'4',
               decode(cont_600.periodo,12,'4')))))))))))),'12')
      order by cont_600.conta_contabil;

   v_erro           EXCEPTION;

   v_valor_saldo             number(15,2);
   v_ind_saldo               varchar2(1);
   v_ind_considera_c_custos  varchar2(1);

begin
   p_des_erro       := NULL;
   select ind_considera_c_custos into v_ind_considera_c_custos from fatu_504
   where  fatu_504.codigo_empresa = p_cod_empresa;

   for reg_cont600 in cont600(p_cod_empresa, p_exercicio, p_plano_cta, p_form_apur, v_ind_considera_c_custos)
   loop

      if reg_cont600.total_valor > 0.00
      then
         v_valor_saldo := reg_cont600.total_valor;
         v_ind_saldo   := 'D';

      else
         v_valor_saldo := reg_cont600.total_valor * (-1.0);
         v_ind_saldo   := 'C';

      end if;

      begin

         insert into sped_ctb_i355
           (cod_empresa,                exercicio,
            cod_conta,                  codigo_reduzido,
            saldo_final,                ind_saldo,
            trimestre,                  centro_custo)
         values (
            p_cod_empresa,              p_exercicio,
            reg_cont600.conta_contabil, reg_cont600.conta_reduzida,
            v_valor_saldo,              v_ind_saldo,
            reg_cont600.trimestre,       reg_cont600.centro_custo);

      exception
         when others then
            p_des_erro := 'Erro na inclusao da tabela sped_ctb_i355 ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;
   end loop;

   commit;

exception
   when V_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_gera_ctb_sped_i355 ' || Chr(10) || p_des_erro;

   when others then
      p_des_erro := 'Outros erros na procedure inter_pr_gera_ctb_sped_i355 ' || Chr(10) || SQLERRM;
end inter_pr_gera_ctb_sped_i355;




/

exec inter_pr_recompile;
