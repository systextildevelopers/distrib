alter table i_obrf_015 add conferencia_almox number(1);

alter table i_obrf_015 modify conferencia_almox default 0;

exec inter_pr_recompile;
