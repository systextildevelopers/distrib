
  CREATE OR REPLACE PROCEDURE "INTER_PR_EMPENHO_ALMOXARIFADO" is
   cursor supr520 is
      select num_requisicao, sequencia,  nivel,    grupo,
             subgrupo,       item,       deposito, situacao,
             cod_transacao,
             decode(flag_faccionista,1,decode(greatest(supr_520.qtde_requisitada - supr_520.qtde_entregue_aux ,0),0, supr_520.qtde_entregue_aux,supr_520.qtde_requisitada) - qtde_emp_faccao_nfs,qtde_requisitada - qtde_entregue) QTDE_SALDO
      from supr_520
      where situacao not in (7)                              -- cancelada
        and (situacao not in (6) or flag_faccionista = 1)    -- atendida ou ent�o � de faccionista
        and deposito      > 0
        and cod_transacao > 0
        and decode(flag_faccionista,1,qtde_requisitada - qtde_emp_faccao_nfs,qtde_requisitada - qtde_entregue) > 0.00
        and exists (select 1 from basi_205
                    where basi_205.codigo_deposito      = supr_520.deposito
                      and basi_205.empenha_almoxarifado = 1)
      order by nivel, grupo, subgrupo, item, deposito, num_requisicao;

   nivel_old            estq_040.cditem_nivel99%type  := '#';
   grupo_old            estq_040.cditem_grupo%type    := '#####';
   subgrupo_old         estq_040.cditem_subgrupo%type := '###';
   item_old             estq_040.cditem_item%type     := '######';
   deposito_old         estq_040.deposito%type        := 0;
   v_qtde_empenhar      estq_040.qtde_empenhada%type;
   v_qtde_disponivel    estq_040.qtde_estoque_atu%type;
   v_qtde_nao_empenhada supr_520.qtde_nao_emp%type;
   v_nova_situacao      supr_520.situacao%type;
   v_entrada_saida      estq_005.entrada_saida%type;
   v_rpt_pette          number;
   v_status_ev   number := 0;
   v_qtde_ev   number := 0;

begin

   for reg in (select rowid conta
               from estq_040
               where estq_040.deposito in (select basi_205.codigo_deposito from basi_205
                                           where basi_205.aceita_req_almox     = 1
                                           and   basi_205.empenha_almoxarifado = 1)
               and   estq_040.lote_acomp      = 0
               and   estq_040.qtde_empenhada <> 0.00

               and   not exists (select 1 from supr_520, estq_005
                                 where supr_520.nivel         = estq_040.cditem_nivel99
                                 and   supr_520.grupo         = estq_040.cditem_grupo
                                 and   supr_520.subgrupo      = estq_040.cditem_subgrupo
                                 and   supr_520.item          = estq_040.cditem_item
                                 and   supr_520.deposito      = estq_040.deposito

                                 and   supr_520.cod_transacao = estq_005.codigo_transacao
                                 and   estq_005.entrada_saida <> 'E'

                                 and   supr_520.situacao not in (7)                                       -- cancelada
                                 and  (supr_520.situacao not in (6) or supr_520.flag_faccionista = 1)    -- atendida ou ent�o � de faccionista
                                 and   supr_520.deposito      > 0
                                 and   supr_520.cod_transacao > 0
                                 and   decode(supr_520.flag_faccionista,1,supr_520.qtde_requisitada - supr_520.qtde_emp_faccao_nfs,supr_520.qtde_requisitada - supr_520.qtde_entregue) > 0.00
                                 and   exists (select 1 from basi_205
                                               where basi_205.codigo_deposito      = supr_520.deposito
                                               and   basi_205.empenha_almoxarifado = 1)))
   loop

      begin
         update estq_040
         set qtde_empenhada = 0.0,
             nome_prog_040  = 'INTER_PR_EMPENHO_ALMOXARIFADO'
         where rowid = reg.conta;

      exception when others
      then raise_application_error(-20000,'Problema ao atualizar dados na tabela estq_040');
      end;
      commit;
   end loop;

   for reg_supr520 in supr520
   loop

      -- se houver quebra do produto ou depssito, atualiza a quantidade_empenhada
      -- do estq_040 com o total acumulado de empneho do ultimo produto/deposito
      -- processado (desde que nco seja a primeira vez), le a quantidade em estoque

     -- disponivel no estq_040 (qtde_estoque_atu)
      if reg_supr520.nivel    <> nivel_old    or reg_supr520.grupo <> grupo_old
      or reg_supr520.subgrupo <> subgrupo_old or reg_supr520.item  <> item_old
      or reg_supr520.deposito <> deposito_old
      then

         -- se nivel_old <> '#', entao i o primeiro produto/deposito a ser processado
         -- assim nao se pode atualizar o estq_040 com a quantiudade empenhada anterior
         if nivel_old <> '#'
         then
            begin
               update estq_040
               set qtde_empenhada = v_qtde_empenhar,
                   nome_prog_040  = 'INTER_PR_EMPENHO_ALMOXARIFADO'
               where cditem_nivel99  = nivel_old
                 and cditem_grupo    = grupo_old
                 and cditem_subgrupo = subgrupo_old
                 and cditem_item     = item_old
                 and lote_acomp      = 0
                 and deposito        = deposito_old;
               exception when others
               then raise_application_error(-20000,'Problema ao atualizar dados na tabela estq_040');
            end;
         end if;

         -- atualiza variaveis de controle de quebra com o produto/deposito que se esta
         -- processando no momento
         nivel_old    := reg_supr520.nivel;
         grupo_old    := reg_supr520.grupo;
         subgrupo_old := reg_supr520.subgrupo;
         item_old     := reg_supr520.item;
         deposito_old := reg_supr520.deposito;

         -- zera variavel acumuladora do empenho, esta i usada para atualizar o campo
         -- estq_040.qtde_empenhada
         v_qtde_empenhar := 0.00;

         -- le o saldo do produto/deposito do lote zero, que i o lote padrco do controle
         -- de requisicao do almoxarifado
         begin
            select qtde_estoque_atu into v_qtde_disponivel
            from estq_040
            where cditem_nivel99  = reg_supr520.nivel
              and cditem_grupo    = reg_supr520.grupo
              and cditem_subgrupo = reg_supr520.subgrupo
              and cditem_item     = reg_supr520.item
              and lote_acomp      = 0
              and deposito        = reg_supr520.deposito;

            exception
               when others then
                  v_qtde_disponivel := 0.00;
         end;

         -- se o saldo for negativo, entao zera a quantidade, pois a disponibilidade i zero.
         if v_qtde_disponivel < 0.00
         then
            v_qtde_disponivel := 0.00;
         end if;
      end if;

      -- le a transacao de estoque
      begin
         select entrada_saida into v_entrada_saida
         from estq_005
         where codigo_transacao = reg_supr520.cod_transacao;
         exception
            when others then
               v_entrada_saida := 'E';
      end;

      -- se a transacao nao for de saida ou transferencia, nao faz nada
      if v_entrada_saida = 'S' or v_entrada_saida = 'T'
      then
         -- se o saldo do item da requisicao for menor ou igual ao saldo disponivel no estoque
         -- entao se pode empenhar toda a quantidade de saldo da requisicao
         -- senao ss se pode empenhar a quantidade disponivel
         if reg_supr520.qtde_saldo <= v_qtde_disponivel
         then
            -- quantidade que nao foi empenhada da requisicao
            v_qtde_nao_empenhada := 0.00;
            -- acumulador do total a empenhar no estq_040.qtde_empenhada
            v_qtde_empenhar      := v_qtde_empenhar   + reg_supr520.qtde_saldo;
            -- decrementador do total da quantidade disponivel no estoque
            v_qtde_disponivel    := v_qtde_disponivel - reg_supr520.qtde_saldo;

            -- troca a situacao da requisicao para empenhado total, quando a mesma tiver
            -- a situacao 2 (nao empenhada) ou 4 (liberada para compra). Se a requisicao
            -- nao estiver com a situacao 2 e 4, entao nao altera a situacao.
            if reg_supr520.situacao = 2 or reg_supr520.situacao = 4
            then
               v_nova_situacao := 1;   -- empenhado total
            else
               v_nova_situacao := reg_supr520.situacao;
            end if;
         else
            -- quantidade que nao foi empenhada da requisicao
            v_qtde_nao_empenhada := reg_supr520.qtde_saldo - v_qtde_disponivel;
            -- acumulador do total a empenhar no estq_040.qtde_empenhada
            v_qtde_empenhar      := v_qtde_empenhar        + v_qtde_disponivel;
            -- decrementador do total da quantidade disponivel no estoque
            v_qtde_disponivel    := 0.00;

            -- troca a situacao da requisicao para nao empenhado total, quando a mesma tiver
            -- a situacao 1 (empenhada total) ou 4 (liberada para compra). Se a requisicao
            -- nao estiver com a situacao 1 e 4, entao nao altera a situacao.
            if reg_supr520.situacao = 1 or reg_supr520.situacao = 4
            then
               v_nova_situacao := 2;   -- nao empenhado total
            else
               v_nova_situacao := reg_supr520.situacao;
            end if;
         end if;

         /* Para a Pettenati, este programa deve fazer um tratamento diferente,
            eles nunca v�o atualizar para transfer�ncia, somente para sa�da. */
         v_rpt_pette := 0;

         select count(*)
         into v_rpt_pette
         from fatu_500
         where fatu_500.rpt_ordem_benef = 'pcpb_opb002';

         v_status_ev := 0;
         v_qtde_ev := 0;

         begin
            select decode(estq_220.stats,5,0,estq_220.stats), to_number(decode(estq_220.qtd_exec,null,'0',estq_220.qtd_exec))
            into v_status_ev, v_qtde_ev
            from estq_220
            where estq_220.requisicao_almoxarifado = reg_supr520.num_requisicao
            and   estq_220.seq_requisicao          = reg_supr520.sequencia
            and   rownum = 1;
            exception when others
            then v_status_ev := 0;
                 v_qtde_ev := 0;
         end;

         -- atualiza o iem da requisicao com a quantidade que nao conseguiu empenhar e
         -- a nova situacao
         if v_entrada_saida = 'S'
         then
            if v_status_ev > 0
            then
               begin
                  update supr_520
                  set qtde_nao_emp  = v_qtde_nao_empenhada,
                      nome_programa = 'INTER_PR_EMPENHO_ALMOXARIFADO',
                      controla_log  = 'PR_E_AL' -- N�O ATUALIZA LOG DA SUPR_520
                  where num_requisicao = reg_supr520.num_requisicao
                    and sequencia      = reg_supr520.sequencia;
                  exception when others
                  then raise_application_error(-20000,'Problema ao atualizar dados na tabela supr_520 (1)');
               end;
            else
               begin
                  update supr_520
                  set qtde_nao_emp  = v_qtde_nao_empenhada,
                      situacao      = v_nova_situacao,
                      nome_programa = 'INTER_PR_EMPENHO_ALMOXARIFADO',
                      controla_log  = 'PR_E_AL'
                  where num_requisicao = reg_supr520.num_requisicao
                    and sequencia      = reg_supr520.sequencia;
                  exception when others
                  then raise_application_error(-20000,'Problema ao atualizar dados na tabela supr_520 (1)');
               end;
            end if;
         else
            begin
               /* Se � transfer�ncia e n�o � Pettenati, ent�o atualiza somente a situa��o. */
               if v_rpt_pette = 0
               then
                  begin
                     update supr_520
                     set situacao      = v_nova_situacao,
                         nome_programa = 'INTER_PR_EMPENHO_ALMOXARIFADO',
                         controla_log  = 'PR_E_AL'
                     where num_requisicao = reg_supr520.num_requisicao
                       and sequencia      = reg_supr520.sequencia;
                     exception when others
                     then raise_application_error(-20000,'Problema ao atualizar dados na tabela supr_520 (2)');
                  end;
               end if;
            end;
         end if;

         commit;
      end if;
   end loop;

   -- atualiza o estq_040.qtde_empenhada com o total que se conseguiu empenhar do
   -- �ltimo produto/deposito processado e que nco entrou na quebra de produto/deposito
   begin
      update estq_040
      set qtde_empenhada = v_qtde_empenhar,
          nome_prog_040  = 'INTER_PR_EMPENHO_ALMOXARIFADO'
      where cditem_nivel99  = nivel_old
        and cditem_grupo    = grupo_old
        and cditem_subgrupo = subgrupo_old
        and cditem_item     = item_old
        and lote_acomp      = 0
        and deposito        = deposito_old;
      exception when others
      then raise_application_error(-20000,'Problema ao atualizar dados na tabela estq_040');
   end;
   commit;

end inter_pr_empenho_almoxarifado;
 

/

exec inter_pr_recompile;

