create or replace PACKAGE ST_PCK_LANCAMENTO_CONTAB AS
    
    type v_dados_cont_600 is table of cont_600%rowtype index by binary_integer;

    FUNCTION last_seq( p_cod_empresa NUMBER, p_exercicio NUMBER,  p_numero_lanc NUMBER) RETURN NUMBER;
    
    FUNCTION list( p_cod_empresa NUMBER, p_exercicio NUMBER, p_numero_lanc NUMBER) RETURN v_dados_cont_600;

    function update_proximo_lancamento(p_codigo_empresa number, p_codigo_exercicio number) return number;

END ST_PCK_LANCAMENTO_CONTAB;
