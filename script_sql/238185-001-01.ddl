begin
    EXECUTE IMMEDIATE 'alter table mqop_005            modify (cod_estagio_agrupador          number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/


begin
    EXECUTE IMMEDIATE 'alter table mqop_005            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_005            modify (estagio_base_fila              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_005            modify (estagio_final                  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_005            modify (est_agrup_est                  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_005_log        modify (cod_estagio_agrupador_new      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_005_log        modify (cod_estagio_agrupador_old      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_005_log        modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/
