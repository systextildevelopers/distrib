create table supr_690
(
  id           number(9) not null,
  fornecedor9  number(9) not null,
  fornecedor4  number(4) not null,
  fornecedor2  number(2) not null,
  tipo_prod    number(3) not null
);

-- Create primary key
ALTER TABLE SUPR_690 ADD CONSTRAINT PK_SUPR_690 PRIMARY KEY (ID);
ALTER TABLE SUPR_690 ADD CONSTRAINT UNIQ_PK_SUPR_690 UNIQUE(FORNECEDOR9, FORNECEDOR4, FORNECEDOR2, TIPO_PROD);

-- Create FOREIGN key
ALTER TABLE SUPR_690
  ADD CONSTRAINT FK_RELAC_TIPO_PROD_FORNECEDOR 
  FOREIGN KEY (FORNECEDOR9, FORNECEDOR4, FORNECEDOR2)
  REFERENCES SUPR_010(fornecedor9, fornecedor4, fornecedor2);

--cria sequence para id
create sequence ID_SUPR_690
minvalue 0
maxvalue 99999999999999999999
start with 1
increment by 1;

--cria trigger para buscar proximo id
create or replace trigger "INTER_TR_SUPR_690_ID" 
before insert on SUPR_690
  for each row
declare
   v_nr_registro number;
begin
   select ID_SUPR_690.nextval 
   into v_nr_registro from dual;
   :new.id := v_nr_registro;
end INTER_TR_SUPR_690_ID;
/
exec inter_pr_recompile;
