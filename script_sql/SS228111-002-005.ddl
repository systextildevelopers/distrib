create table HAUS_005
(
  ID                NUMBER(9) not null,
  ID_HAUS_003       NUMBER(3),
  SEQUENCIA_PARCELA NUMBER(9),
  DATA_VENCIMENTO   DATE,
  VALOR_PARCELA     NUMBER(20,5)
);

alter table HAUS_005
  add constraint FK_HAUS_005_ID_HAUS_003 foreign key (ID_HAUS_003)
  references HAUS_003 (ID);

create sequence ID_HAUS_005
minvalue 0
maxvalue 99999999999999999999
start with 1
increment by 1
cache 20;  
