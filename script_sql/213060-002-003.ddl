create table fndc_003
( FND_EMPRESA                NUMBER(3)    default 0 not null,
 FND_TIPO	                 NUMBER(2)    default 0, 
 FND_HISTORICO	             NUMBER(4)    default 0,                  
 FND_TRANS                   NUMBER(3)    default 0,                  
 FND_POSICAO                 NUMBER(2)    default 0,
 FND_VL_LIMITE               NUMBER(15,2)    default 0,
 FND_VL_USU_LIM              NUMBER(15,2)    default 0
);

comment on table fndc_003 is 'Tabela de par�metros para cria��o dos t�tulos de controle do Fundo (t�tulo espelho)';

comment on column fndc_003.FND_EMPRESA    is 'C�digo da empresa no sistema';

comment on column fndc_003.FND_TIPO   is 'Tipo para t�tulo de acompanhamento de cr�dito';

comment on column fndc_003.FND_HISTORICO      is 'Hist�rico para t�tulo de acompanhamento de cr�dito';

comment on column fndc_003.FND_TRANS      is 'C�digo da transa��o';

comment on column fndc_003.FND_POSICAO      is 'Posi��o do t�tulo';

comment on column fndc_003.FND_VL_LIMITE      is 'Valor do limite';

comment on column fndc_003.FND_VL_USU_LIM      is 'Valor usado do limite';

ALTER TABLE fndc_003 ADD CONSTRAINT pk_fndc_003 PRIMARY KEY (FND_EMPRESA);
