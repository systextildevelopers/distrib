alter table obrf_080 add numero_ordem_origem          NUMBER(6);
alter table obrf_080 add sequencia_origem             NUMBER(3);
alter table obrf_080 add situacao_impressao           NUMBER(1) default 0;
alter table obrf_080_log add situacao_impressao_new        NUMBER(1) default 0;
