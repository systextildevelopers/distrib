create or replace TRIGGER INTER_TR_SUPR_110_LOG
  before insert or
         delete or
         update of ch_100_num_ped, ch_100_seq_ped, nr_nota_fiscal, data_entrega,
		   qtde_entregue, preco_entrega, percentual_ipi, valor_ipi,
		   cond_pagamento, tipo_frete, valor_frete, observacao,
		   tem_entrega, executa_trigger, cod_deposito, cod_transacao
	 on supr_110
  for each row

declare
    v_ch_100_num_ped_old      supr_110.ch_100_num_ped%type;
    v_ch_100_num_ped_new      supr_110.ch_100_num_ped%type;
    v_ch_100_seq_ped_old      supr_110.ch_100_seq_ped%type;
    v_ch_100_seq_ped_new      supr_110.ch_100_seq_ped%type;
    v_nr_nota_fiscal_old      supr_110.nr_nota_fiscal%type;
    v_nr_nota_fiscal_new      supr_110.nr_nota_fiscal%type;
    v_data_entrega_old        supr_110.data_entrega%type;
    v_data_entrega_new        supr_110.data_entrega%type;
    v_qtde_entregue_old       supr_110.qtde_entregue%type;
    v_qtde_entregue_new       supr_110.qtde_entregue%type;
    v_cond_pagamento_old      supr_110.cond_pagamento%type;
    v_cond_pagamento_new      supr_110.cond_pagamento%type;
    v_valor_frete_old         supr_110.valor_frete%type;
    v_valor_frete_new         supr_110.valor_frete%type;

    v_operacao              varchar(1);
    v_data_operacao         date;
    v_usuario_rede          varchar(20);
    v_maquina_rede          varchar(40);
    v_aplicativo            varchar(20);
    ws_usuario_rede           varchar2(20);
    ws_maquina_rede           varchar2(40);
    ws_aplicativo             varchar2(20);
    ws_sid                    number(9);
    ws_empresa                number(3);
    ws_usuario_systextil      varchar2(250);
    ws_locale_usuario         varchar2(5);
    ws_nome_programa          varchar(20);
    v_executa_trigger       number;

begin

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

       -- Grava a data/hora da insercao do registro (log)
      v_data_operacao := sysdate();

      ws_nome_programa := inter_fn_nome_programa(ws_sid);   

      --alimenta as variaveis new caso seja insert ou update
      if inserting
      then
         v_ch_100_num_ped_new      := :new.ch_100_num_ped;
         v_ch_100_seq_ped_new      := :new.ch_100_seq_ped;
         v_nr_nota_fiscal_new      := :new.nr_nota_fiscal;
         v_data_entrega_new        := :new.data_entrega;
         v_qtde_entregue_new       := :new.qtde_entregue;
         v_cond_pagamento_new      := :new.cond_pagamento;
         v_valor_frete_new         := :new.valor_frete;
           
        begin
            insert into supr_110_log
              (ch_100_num_ped_old      ,      ch_100_num_ped_new      ,
               ch_100_seq_ped_old      ,      ch_100_seq_ped_new      ,
               nr_nota_fiscal_old      ,      nr_nota_fiscal_new      ,
               data_entrega_old        ,      data_entrega_new        ,
               qtde_entregue_old       ,      qtde_entregue_new       ,
               cond_pagamento_old      ,      cond_pagamento_new      ,
               valor_frete_old         ,      valor_frete_new         ,
               operacao                ,      data_operacao           ,
               usuario_rede            ,      maquina_rede            ,
               aplicativo              ,      programa)
            values
              (0      ,    v_ch_100_num_ped_new      ,
               0      ,    v_ch_100_seq_ped_new      ,
               0      ,    v_nr_nota_fiscal_new      ,
               ''     ,    v_data_entrega_new        ,
               0      ,    v_qtde_entregue_new       ,
               0      ,    v_cond_pagamento_new      ,
               0      ,    v_valor_frete_new         ,
               'I'                        ,    v_data_operacao,
               ws_usuario_rede            ,    ws_maquina_rede,
               ws_aplicativo              ,    ws_nome_programa);
        exception
            when OTHERS
            then raise_application_error (-20000, 'Nao atualizou a tabela de log da supr_110.');
        end;

      end if; --fim do if inserting or updating

      --alimenta as variaveis old caso seja delete ou update
      if deleting
      then

         v_ch_100_num_ped_old      := :old.ch_100_num_ped;
         v_ch_100_seq_ped_old      := :old.ch_100_seq_ped;
         v_nr_nota_fiscal_old      := :old.nr_nota_fiscal;
         v_data_entrega_old        := :old.data_entrega;
         v_qtde_entregue_old       := :old.qtde_entregue;
         v_cond_pagamento_old      := :old.cond_pagamento;
         v_valor_frete_old         := :old.valor_frete;

        begin
          insert into supr_110_log
            (ch_100_num_ped_old      ,      ch_100_num_ped_new      ,
             ch_100_seq_ped_old      ,      ch_100_seq_ped_new      ,
             nr_nota_fiscal_old      ,      nr_nota_fiscal_new      ,
             data_entrega_old        ,      data_entrega_new        ,
             qtde_entregue_old       ,      qtde_entregue_new       ,
             cond_pagamento_old      ,      cond_pagamento_new      ,
             valor_frete_old         ,      valor_frete_new         ,
             operacao                ,      data_operacao           ,
             usuario_rede            ,      maquina_rede            ,
             aplicativo              ,      programa)
          values
            (v_ch_100_num_ped_old      ,    0      ,
             v_ch_100_seq_ped_old      ,    0      ,
             v_nr_nota_fiscal_old      ,    0      ,
             v_data_entrega_old     ,       ''      ,
             v_qtde_entregue_old      ,     0       ,
             v_cond_pagamento_old      ,    0      ,
             v_cond_pagamento_old      ,    0         ,
             'D'                        ,    v_data_operacao,
             ws_usuario_rede            ,    ws_maquina_rede,
             ws_aplicativo              ,    ws_nome_programa);
        exception
           when OTHERS
           then raise_application_error (-20000, 'Nao atualizou a tabela de log da supr_110.');
        end;

      end if; --fim do if deleting or updating

      if updating
      then 

         v_ch_100_num_ped_old      := :old.ch_100_num_ped;
         v_ch_100_seq_ped_old      := :old.ch_100_seq_ped;
         v_nr_nota_fiscal_old      := :old.nr_nota_fiscal;
         v_data_entrega_old        := :old.data_entrega;
         v_qtde_entregue_old       := :old.qtde_entregue;
         v_cond_pagamento_old      := :old.cond_pagamento;
         v_valor_frete_old         := :old.valor_frete;

         v_ch_100_num_ped_new      := :new.ch_100_num_ped;
         v_ch_100_seq_ped_new      := :new.ch_100_seq_ped;
         v_nr_nota_fiscal_new      := :new.nr_nota_fiscal;
         v_data_entrega_new        := :new.data_entrega;
         v_qtde_entregue_new       := :new.qtde_entregue;
         v_cond_pagamento_new      := :new.cond_pagamento;
         v_valor_frete_new         := :new.valor_frete;
           
        begin
            insert into supr_110_log
              (ch_100_num_ped_old      ,      ch_100_num_ped_new      ,
               ch_100_seq_ped_old      ,      ch_100_seq_ped_new      ,
               nr_nota_fiscal_old      ,      nr_nota_fiscal_new      ,
               data_entrega_old        ,      data_entrega_new        ,
               qtde_entregue_old       ,      qtde_entregue_new       ,
               cond_pagamento_old      ,      cond_pagamento_new      ,
               valor_frete_old         ,      valor_frete_new         ,
               operacao                ,      data_operacao           ,
               usuario_rede            ,      maquina_rede            ,
               aplicativo              ,      programa)
            values
              (v_ch_100_num_ped_old      ,    v_ch_100_num_ped_new      ,
               v_ch_100_seq_ped_old      ,    v_ch_100_seq_ped_new      ,
               v_nr_nota_fiscal_old      ,    v_nr_nota_fiscal_new      ,
               v_data_entrega_old     ,    v_data_entrega_new        ,
               v_qtde_entregue_old      ,    v_qtde_entregue_new       ,
               v_cond_pagamento_old      ,    v_cond_pagamento_new      ,
               v_valor_frete_old      ,    v_valor_frete_new         ,
               'U'                        ,    v_data_operacao,
               ws_usuario_rede            ,    ws_maquina_rede,
               ws_aplicativo              ,    ws_nome_programa);
        exception
            when OTHERS
            then raise_application_error (-20000, 'Nao atualizou a tabela de log da supr_110.');
        end;

      end if;

   end if;

end inter_tr_supr_110_log;

/

exec inter_pr_recompile;

