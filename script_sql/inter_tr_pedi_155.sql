create or replace trigger inter_tr_pedi_155
before insert on pedi_155
for each row
begin
  if :new.id is null then
    :new.id := pedi_155_seq.nextval;
  end if;
end;
