CREATE OR REPLACE TRIGGER INTER_TR_PCPC_320_LOG
  before insert or delete or
  update of qtde_volumes_fisicos, conferente, nr_orcamento_loja, usuario_alocado,
	flag_alocado, data_hora_alocado, numero_volume, numero_kit,
	nivel_kit, grupo_kit, sub_kit, item_kit,
	montador, data_montagem, turno, hora_montagem,
	situacao_kit, situacao_volume, peso_volume, pedido_venda,
	nr_solicitacao, cod_usuario, cod_tipo_volume, deposito_entrada,
	transacao_entrada, cod_empresa, nota_fiscal, serie_nota,
	cod_cancelamento, data_cancelamento, deposito_saida, transacao_saida,
	peso_embalagem, numero_volume_exp, local_caixa, empresa_nota_entrada,
	nota_fiscal_entrada, serie_nota_entrada, cnpj9_entrada, cnpj4_entrada,
	cnpj2_entrada, numero_coleta_faturamento, sit_anterior, tipo_endereco,
	qtde_pecas_sug, cod_embalagem, executa_trigger, pre_romaneio,
	empresa_transf, nota_transf, serie_transf, numero_pre_pack,
	endereco_caixa_peca, endereco, caixa_conferida, usuario,
	data_hora_conf, grade_integracao, exec_processo, flag_complemento_tag
  on pcpc_320
  for each row
declare
  -- local variables here
 v_numero_volume					pcpc_320.numero_volume 		%type;
 v_numero_kit						pcpc_320.numero_kit		%type;
 v_nivel_kit						pcpc_320.nivel_kit		%type;
 v_grupo_kit						pcpc_320.grupo_kit		%type;
 v_sub_kit						pcpc_320.sub_kit		%type;
 v_item_kit						pcpc_320.item_kit		%type;
 v_montador						pcpc_320.montador		%type;
 v_data_montagem					pcpc_320.data_montagem		%type;
 v_turno						pcpc_320.turno			%type;
 v_hora_montagem					pcpc_320.hora_montagem		%type;
 v_cod_usuario						pcpc_320.cod_usuario		%type;
 v_cod_tipo_volume					pcpc_320.cod_tipo_volume	%type;
 v_deposito_entrada					pcpc_320.deposito_entrada	%type;
 v_transacao_entrada					pcpc_320.transacao_entrada	%type;
 v_cod_empresa						pcpc_320.cod_empresa		%type;
 v_cod_cancelamento					pcpc_320.cod_cancelamento	%type;
 v_data_cancelamento					pcpc_320.data_cancelamento	%type;
 v_numero_volume_exp					pcpc_320.numero_volume_exp	%type;
 v_local_caixa						pcpc_320.local_caixa		%type;
 v_peso_volume_old					pcpc_320.peso_volume 		%type;
 v_situacao_volume_old		 			pcpc_320.situacao_volume	%type;
 v_situacao_kit_old	 				pcpc_320.situacao_kit      	%type;
 v_pedido_venda_old					pcpc_320.pedido_venda		%type;
 v_nr_solicitacao_old					pcpc_320.nr_solicitacao		%type;
 v_nota_fiscal_old					pcpc_320.nota_fiscal		%type;
 v_serie_nota_old					pcpc_320.serie_nota		%type;
 v_deposito_saida_old					pcpc_320.deposito_saida		%type;
 v_transacao_saida_old					pcpc_320.transacao_saida	%type;
 v_peso_embalagem_old					pcpc_320.peso_embalagem		%type;
 v_peso_volume_new					pcpc_320.peso_volume 		%type;
 v_pedido_venda_new					pcpc_320.pedido_venda		%type;
 v_nr_solicitacao_new					pcpc_320.nr_solicitacao		%type;
 v_nota_fiscal_new					pcpc_320.nota_fiscal		%type;
 v_deposito_saida_new					pcpc_320.deposito_saida		%type;
 v_transacao_saida_new					pcpc_320.transacao_saida	%type;
 v_peso_embalagem_new					  pcpc_320.peso_embalagem		%type;
 v_serie_nota_new					      pcpc_320.serie_nota		%type;
 v_situacao_kit_new	 				    pcpc_320.situacao_kit        	%type;
 v_situacao_volume_new		 			pcpc_320.situacao_volume	%type;
 v_usuario_alocado_new		 			pcpc_320.usuario_alocado 	%type;
 v_usuario_alocado_old          pcpc_320.usuario_alocado 	%type;
 v_flag_alocado_new		 			    pcpc_320.flag_alocado 	%type;
 v_flag_alocado_old             pcpc_320.flag_alocado 	%type;
 v_nr_orcamento_loja_old        pcpc_320.nr_orcamento_loja     %type;
 v_nr_orcamento_loja_new        pcpc_320.nr_orcamento_loja     %type;
 v_qtde_volumes_fisicos_old     pcpc_320.qtde_volumes_fisicos  %type;
 v_qtde_volumes_fisicos_new     pcpc_320.qtde_volumes_fisicos  %type;
 v_conferente_old               pcpc_320.conferente        %type;
 v_conferente_new               pcpc_320.conferente        %type;
 v_endereco_caixa_old           pcpc_320.endereco_caixa_peca   %type;
 v_endereco_caixa_new           pcpc_320.endereco_caixa_peca   %type;


 v_sid                            number(9);
 v_empresa                        number(3);
 v_usuario_systextil              varchar2(20);
 v_locale_usuario                 varchar2(5);
 v_nome_programa                  varchar2(20);

 v_operacao              varchar(1);
 v_data_operacao         date;
 v_usuario_rede          varchar(20);
 v_maquina_rede          varchar(40);
 v_aplicativo            varchar(20);
 v_executa_trigger       number(1);
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      -- grava a data/hora da insercao do registro (log)
      v_data_operacao := sysdate();

      -- encontra dados do usuario da rede, maquina e aplicativo que esta atualizando a ficha
      select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
      into v_usuario_rede, v_maquina_rede, v_aplicativo
      from sys.gv_$session
      where audsid  = userenv('SESSIONID')
        and inst_id = userenv('INSTANCE')
        and rownum < 2;

      --alimenta as variaveis new caso seja insert ou update
      if inserting or updating
      then
         if inserting
         then v_operacao := 'i';
         else v_operacao := 'u';
         end if;

         v_numero_volume			:= :new.numero_volume;
         v_numero_kit			:= :new.numero_kit;
         v_nivel_kit			:= :new.nivel_kit;
         v_grupo_kit			:= :new.grupo_kit;
         v_sub_kit				:= :new.sub_kit;
         v_item_kit			:= :new.item_kit;
         v_montador			:= :new.montador;
         v_data_montagem			:= :new.data_montagem;
         v_turno				:= :new.turno;
         v_hora_montagem			:= :new.hora_montagem;
         v_cod_usuario			:= :new.cod_usuario;
         v_cod_tipo_volume			:= :new.cod_tipo_volume;
         v_deposito_entrada		:= :new.deposito_entrada;
         v_transacao_entrada		:= :new.transacao_entrada;
         v_cod_empresa			:= :new.cod_empresa;
         v_cod_cancelamento		:= :new.cod_cancelamento;
         v_data_cancelamento		:= :new.data_cancelamento;
         v_numero_volume_exp		:= :new.numero_volume_exp;
         v_local_caixa		  	:= :new.local_caixa;
         v_peso_volume_new			:= :new.peso_volume;
         v_pedido_venda_new		:= :new.pedido_venda;
         v_nr_solicitacao_new		:= :new.nr_solicitacao;
         v_nota_fiscal_new			:= :new.nota_fiscal;
         v_deposito_saida_new		:= :new.deposito_saida;
         v_transacao_saida_new		:= :new.transacao_saida;
         v_peso_embalagem_new		:= :new.peso_embalagem;
         v_serie_nota_new			:= :new.serie_nota;
         v_situacao_kit_new	 	   := :new.situacao_kit;
         v_situacao_volume_new	 := :new.situacao_volume;
         v_usuario_alocado_new   := :new.usuario_alocado;
         v_flag_alocado_new      := :new.flag_alocado;
         v_nr_orcamento_loja_new     := :new.nr_orcamento_loja;
         v_qtde_volumes_fisicos_new  := :new.qtde_volumes_fisicos;
         v_conferente_new            := :new.conferente ;
         v_endereco_caixa_new        := :new.endereco_caixa_peca;

      end if; --fim do if inserting or updating

      --alimenta as variaveis old caso seja insert ou update
      if deleting or updating
      then
         if deleting
         then
            v_operacao      := 'd';
         else
            v_operacao      := 'u';
         end if;

         v_numero_volume			:= :old.numero_volume;
         v_numero_kit			:= :old.numero_kit;
         v_nivel_kit			:= :old.nivel_kit;
         v_grupo_kit			:= :old.grupo_kit;
         v_sub_kit				:= :old.sub_kit;
         v_item_kit			:= :old.item_kit;
         v_montador			:= :old.montador;
         v_data_montagem			:= :old.data_montagem;
         v_turno				:= :old.turno;
         v_hora_montagem			:= :old.hora_montagem;
         v_cod_usuario			:= :old.cod_usuario;
         v_cod_tipo_volume			:= :old.cod_tipo_volume;
         v_deposito_entrada		:= :old.deposito_entrada;
         v_transacao_entrada		:= :old.transacao_entrada;
         v_cod_empresa			:= :old.cod_empresa;
         v_cod_cancelamento		:= :old.cod_cancelamento;
         v_data_cancelamento		:= :old.data_cancelamento;
         v_numero_volume_exp		:= :old.numero_volume_exp;
         v_local_caixa		  	:= :old.local_caixa;
         v_peso_volume_old 		:= :old.peso_volume;
         v_pedido_venda_old		:= :old.pedido_venda;
         v_nr_solicitacao_old		:= :old.nr_solicitacao;
         v_nota_fiscal_old			:= :old.nota_fiscal;
         v_deposito_saida_old		:= :old.deposito_saida;
         v_transacao_saida_old		:= :old.transacao_saida;
         v_peso_embalagem_old		:= :old.peso_embalagem;
         v_serie_nota_old			:= :old.serie_nota;
         v_situacao_kit_old	 	:= :old.situacao_kit;
         v_situacao_volume_old		:= :old.situacao_volume;
         v_usuario_alocado_old   := :old.usuario_alocado;
         v_flag_alocado_old      := :old.flag_alocado;
         v_nr_orcamento_loja_old     := :old.nr_orcamento_loja;
         v_qtde_volumes_fisicos_old  := :old.qtde_volumes_fisicos;
         v_conferente_old            := :old.conferente ;
         v_endereco_caixa_old        := :old.endereco_caixa_peca;


      end if; --fim do if deleting or updating


      -- Dados do usu¿¿¿¿¿¿rio logado
      inter_pr_dados_usuario (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                              v_usuario_systextil,   v_empresa,        v_locale_usuario);

      v_nome_programa := inter_fn_nome_programa(v_sid);



      --insere na pcpc_320_log o registro.

      insert into pcpc_320_log (
         numero_volume,		numero_kit,
         nivel_kit,	               grupo_kit,
         sub_kit,	              	 item_kit,
         montador,	               data_montagem,
         turno,	              		 hora_montagem,
         situacao_kit_old,         situacao_kit_new,
         situacao_volume_old,	     situacao_volume_new,
         peso_volume_old,          peso_volume_new,
         pedido_venda_old,         pedido_venda_new,
         nr_solicitacao_old,	     nr_solicitacao_new,
         cod_usuario,	      	     cod_tipo_volume,
         deposito_entrada,         transacao_entrada ,
         cod_empresa,		           nota_fiscal_old,
         nota_fiscal_new,	         serie_nota_old,
         serie_nota_new,	         cod_cancelamento,
         data_cancelamento,        deposito_saida_old,
         deposito_saida_new,	     transacao_saida_old,
         transacao_saida_new,	     peso_embalagem_old,
         peso_embalagem_new,	     numero_volume_exp,
         local_caixa,	        	   operacao,
         data_operacao,          	 usuario_rede,
         maquina_rede,           	 aplicativo,
         nome_programa,            usuario_alocado_old,
         usuario_alocado_new,      flag_alocado_old,
         flag_alocado_new,         nr_orcamento_loja_old,
         nr_orcamento_loja_new,    qtde_volumes_fisicos_old,
         qtde_volumes_fisicos_new, conferente_old,
         conferente_new,           endereco_caixa_peca_old,
         endereco_caixa_peca_new
         )
      values (
         v_numero_volume,		      v_numero_kit,
         v_nivel_kit,	            v_grupo_kit,
         v_sub_kit,              	v_item_kit,
         v_montador,	          	v_data_montagem,
         v_turno,	      		      v_hora_montagem,
         v_situacao_kit_old,      v_situacao_kit_new,
         v_situacao_volume_old,	  v_situacao_volume_new,
         v_peso_volume_old,       v_peso_volume_new,
         v_pedido_venda_old,      v_pedido_venda_new,
         v_nr_solicitacao_old,	  v_nr_solicitacao_new,
         v_cod_usuario,	        	v_cod_tipo_volume,
         v_deposito_entrada,      v_transacao_entrada ,
         v_cod_empresa,		        v_nota_fiscal_old,
         v_nota_fiscal_new,	      v_serie_nota_old,
         v_serie_nota_new,	      v_cod_cancelamento,
         v_data_cancelamento,     v_deposito_saida_old,
         v_deposito_saida_new,	  v_transacao_saida_old,
         v_transacao_saida_new,	     v_peso_embalagem_old,
         v_peso_embalagem_new,	     v_numero_volume_exp,
         v_local_caixa,	             v_operacao,
         v_data_operacao,            v_usuario_rede,
         v_maquina_rede,             v_aplicativo,
         v_nome_programa,            v_usuario_alocado_old,
         v_usuario_alocado_new,      v_flag_alocado_old,
         v_flag_alocado_new,         v_nr_orcamento_loja_old,
         v_nr_orcamento_loja_new,    v_qtde_volumes_fisicos_old,
         v_qtde_volumes_fisicos_new, v_conferente_old,
         v_conferente_new,            v_endereco_caixa_old,
         v_endereco_caixa_new
);
   end if;
end inter_tr_pcpc_320_log;

-- ALTER TRIGGER INTER_TR_PCPC_320_LOG ENABLE


/
