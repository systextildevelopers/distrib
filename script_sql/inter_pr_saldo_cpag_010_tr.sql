create or replace procedure inter_pr_saldo_cpag_010_tr (
   p_cgc_9        in cpag_010.cgc_9%type,
   p_cgc_4        in cpag_010.cgc_4%type,
   p_cgc_2        in cpag_010.cgc_2%type,
   p_tipo_titulo  in cpag_010.tipo_titulo%type,
   p_nr_duplicata in cpag_010.nr_duplicata%type,
   p_parcela      in cpag_010.parcela%type)

   -- Author  : Neylor Zimmermann de Carvalho
   -- Created : 24/10/05

is
   v_data_pagamento date;

begin
   select max(data_pagamento)
   into v_data_pagamento
   from cpag_015
   where dupl_for_nrduppag = p_nr_duplicata
     and dupl_for_no_parc  = p_parcela
     and dupl_for_for_cli9 = p_cgc_9
     and dupl_for_for_cli4 = p_cgc_4
     and dupl_for_for_cli2 = p_cgc_2
     and dupl_for_tipo_tit = p_tipo_titulo;

   update cpag_010
   set data_ult_movim_pagto = v_data_pagamento
   where nr_duplicata = p_nr_duplicata
     and parcela      = p_parcela
     and cgc_9        = p_cgc_9
     and cgc_4        = p_cgc_4
     and cgc_2        = p_cgc_2
     and tipo_titulo  = p_tipo_titulo;

end inter_pr_saldo_cpag_010_tr;

/
/* versao: 1 */
