
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_040_3" 
     before insert
         or delete
         or update of qtde_pecas_prog,          qtde_pecas_prod,
                      qtde_conserto,            qtde_pecas_2a,
                      qtde_perdas
--                      qtde_em_producao_pacote,  qtde_a_produzir_pacote
     on pcpc_040
     for each row
declare
   v_executa_trigger number;
   v_tem_reg_400     number;
   v_periodo_op        pcpc_020.periodo_producao%type;
   
begin
   -- INICIO - L�gica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementa��o executada para limpeza da base de dados do cliente
   -- e replica��o dos dados para base hist�rico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :new.executa_trigger = 3
         then
             v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1
      or :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :new.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
             v_executa_trigger := 0;
         end if;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :old.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;
   -- FINAL - L�gica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger in (0,3)
   then
      if inserting
      or updating
      then
         inter_pr_verif_reg_em_producao (2,:new.executa_trigger);
		 
		 begin
            -- Encontra o periodo de produ��o da Capa da Ordem de Produ��o.
            select pcpc_020.periodo_producao
            into   v_periodo_op
            from pcpc_020
            where pcpc_020.ordem_producao = :new.ordem_producao;
            exception
            when others then
               v_periodo_op := 0;
         end;
		 
		 if v_periodo_op = 0
		 then
			v_periodo_op := :new.periodo_producao;
		 end if;

         -- Valida��o do per�odo aberto ou fechado deve ser feito do per�odo da Capa da Ordem e n�o do Pacote.
         --inter_pr_verif_per_fechado(:new.periodo_producao,1);
         inter_pr_verif_per_fechado(v_periodo_op,1);
      end if;

      if deleting
      then
         inter_pr_verif_reg_em_producao (2,:old.executa_trigger);

         begin
            -- Encontra o periodo de produ��o da Capa da Ordem de Produ��o.
            select pcpc_020.periodo_producao
            into   v_periodo_op
            from pcpc_020
            where pcpc_020.ordem_producao = :old.ordem_producao;
            exception
            when others then
               v_periodo_op := 0;
         end;
		 
		 if v_periodo_op = 0
		 then
			v_periodo_op := :old.periodo_producao;
		 end if;
         
         -- Valida��o do per�odo aberto ou fechado deve ser feito do per�odo da Capa da Ordem e n�o do Pacote.
         --inter_pr_verif_per_fechado(:old.periodo_producao,1);
         inter_pr_verif_per_fechado(v_periodo_op,1);
      end if;
   end if;

   v_tem_reg_400 := inter_fn_executou_recalculo();

   if inserting
   and v_tem_reg_400 <> 0
   then
      :new.situacao_em_prod_a_prod := 1;
   end if;

   if (inserting
   or  updating)
   and v_executa_trigger = 3
   then
      if  inserting
      and v_executa_trigger = 3
      and v_tem_reg_400     <> 0
      then
        :new.situacao_em_prod_a_prod := 1;
      end if;

      if  v_tem_reg_400                <> 0
      and (:new.situacao_em_prod_a_prod is null
      or   :new.situacao_em_prod_a_prod = 0)
      then
         raise_application_error(-20000,'ATEN��O! Para o per�odo de produ��o desta ordem n�o foi executado o processo de calculo do em produ��o, informe ao PCP. ' || to_char(:new.situacao_em_prod_a_prod));
      end if;

      if  inserting
      and v_tem_reg_400                <> 0
      and :new.situacao_em_prod_a_prod = 0
      then
         :new.situacao_em_prod_a_prod := 1;  -- Se tem o recalculo rodado a ordem deve nascer como OK
      end if;
   end if;

end inter_tr_pcpc_040_3;

-- ALTER TRIGGER "INTER_TR_PCPC_040_3" ENABLE
 

/

exec inter_pr_recompile;

