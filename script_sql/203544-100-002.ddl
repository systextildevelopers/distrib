CREATE TABLE BASI_182
   (DIVISAO_PRODUCAO                NUMBER(4)           DEFAULT 0       NOT NULL,
	INFORME_TIPO_INF                NUMBER(2)           DEFAULT 1       NOT NULL, 
	INFORME_DATAINFO                DATE                                NOT NULL, 
	INFORME_INFORMTE                VARCHAR2(25 BYTE)   DEFAULT ''      NOT NULL, 
	SEQUENCIA                       NUMBER(6)           DEFAULT 0       NOT NULL, 
	INFORMACAO                      VARCHAR2(60 BYTE)   DEFAULT '', 
	INFORME_HISTORICO_INF           NUMBER(2)           DEFAULT 0       NOT NULL, 
	COMPLEMENTO                     VARCHAR2(4000 BYTE));
    
ALTER TABLE BASI_182 ADD
	 CONSTRAINT PK_BASI_182 PRIMARY KEY (DIVISAO_PRODUCAO, INFORME_TIPO_INF, INFORME_DATAINFO, INFORME_INFORMTE, INFORME_HISTORICO_INF, SEQUENCIA);
     
CREATE INDEX BASI_182_01 ON BASI_182 (DIVISAO_PRODUCAO, INFORME_TIPO_INF, INFORME_DATAINFO, INFORME_INFORMTE);

exec inter_pr_recompile;
