  Create table obrf_743 (
    COD_EMPRESA      number(03)    default 0    not null ,
    MES              number(02)    default 0    not null ,
    ANO              number(04)    default 0    not null ,
    UF               varchar2(2)   default ' '  not null,
    COD_AJ_APUR      varchar2(08)  default ' '  not null,
    IND_ENTR_SAID    number(01)    default 0    not null,
    NUM_DOC          number(9)     default 0    not null,
    SER              varchar2(3)   default ' '  not null,
    CNPJ9            number(9)     default 0    not null,
    CNPJ4            number(4)     default 0    not null,
    CNPJ2            number(2)     default 0    not null,
    NIVEL            varchar2(1)   default ' '  not null,
    GRUPO            varchar2(5)   default ' '  not null,
    SUB              varchar2(3)   default ' '  not null,
    ITEM             varchar2(6)   default ' '  not null,
    COD_MOD          varchar2(2)   default ' ',
    CHV_DOC          varchar2(44)  default ' ',
    DT_DOC           date,                   
    VL_AJ_APUR       number(13,2)  default 0.00);
    
  alter table obrf_743
  add constraint PK_OBRF_743 primary key (COD_EMPRESA, MES, ANO,UF, COD_AJ_APUR, IND_ENTR_SAID,
                                          NUM_DOC,SER,CNPJ9,CNPJ4,CNPJ2,NIVEL,GRUPO,SUB,ITEM );
  
  comment on column obrf_743.COD_EMPRESA   is 'Codigo de empresa para registro E313 filho 311';
  comment on column obrf_743.MES           is 'Mes para registro E313 filho 311';  
  comment on column obrf_743.ANO           is 'ano para registro E313 filho 311';  
  comment on column obrf_743.UF            is 'Estado para registro E313 filho 311';  
  comment on column obrf_743.COD_AJ_APUR   is 'Codigo de registro E313 filho 311';  
  comment on column obrf_743.IND_ENTR_SAID is 'Indica se � nota fiscal de entrada ou saida, 0 - Entrada, 1 - Saida';
  comment on column obrf_743.NUM_DOC is 'Numero do documento entrada ou saida';
  comment on column obrf_743.SER is 'serie do documento de entrada ou saida';
  comment on column obrf_743.COD_MOD   is 'Indica o codigo do modelo do documento entrada ou saida';
  comment on column obrf_743.CHV_DOC  is 'chav do documento de entrada ou saida';
  comment on column obrf_743.DT_DOC is 'Data de emissao do documento de entrada ou saida';
  comment on column obrf_743.VL_AJ_APUR is 'valor do ajuste do documento de entrada ou saida';
  
  exec inter_pr_recompile ;
