INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'pedi_l120',      0,
	0,			     'Cadastro de Exceções de Bloqueios por condição de pagamento');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'pedi_l120',   'NENHUM', 
	0,             1, 
	'S',           'S', 
	'S',           'S');
	
INSERT INTO hdoc_033
(	usu_prg_cdusu,  usu_prg_empr_usu, 
	programa,       nome_menu, 
	item_menu,      ordem_menu, 
	incluir,        modificar, 
	excluir,        procurar)
VALUES
(	'TREINAMENTO',  1, 
	'pedi_l120',    'NENHUM', 
	0,              1, 
	'S',            'S', 
	'S',            'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Cadastro de Exceções de Bloqueios por condição de pagamento'
 WHERE hdoc_036.codigo_programa = 'pedi_l120'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;

EXEC inter_pr_recompile;
