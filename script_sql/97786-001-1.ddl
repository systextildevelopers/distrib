-- 14009 SQL PARA MOTIVO DE REJEI��O - SUFRAMA

alter table obrf_157 add (mot_des_icms number(1));

comment on column obrf_157.mot_des_icms
is 'Identifica o motivo da desonera��o de suframa.';


-- 14010
alter table obrf_150 add (valor_suframa number(15,2));

comment on column obrf_150.valor_suframa
is 'Valor do total do suframa sem pis e cofins.';


--14011 E 14012
alter table fatu_060 add (valor_suframa_icms number(15,2), inf_ad_prod varchar2(1000));

comment on column fatu_060.valor_suframa_icms
is 'Valor do total do suframa sem pis e cofins.';


comment on column fatu_060.inf_ad_prod
is 'Descri��o do descnto de suframa.';


-- 14055 - ENQUADRAMENTO

alter table basi_240
add (cod_enquadr_ipi_entr varchar2(3) default '999',
     cod_enquadr_ipi_exp  varchar2(3) default '999');


--14056
create table obrf_801
 (cod_enq_ipi number(3) default 0,
    desc_enq    varchar2(1000) default ' ');
    

alter table basi_240
modify cod_enquadramento_ipi default '999';


--14048
alter table fatu_067
  drop constraint pk_fatu_067;

drop index pk_fatu_067;

alter table fatu_067
add (seq_nota_fisc number(9) default 0 not null );

comment on column fatu_067.seq_nota_fisc is 'Sequencia da nota';

alter table fatu_067
  add constraint pk_fatu_067 primary key (codigo_empresa, num_nota_fiscal, serie_nota_fisc, seq_nota_fisc, ncm);

exec inter_pr_recompile;
/


-- ****************************************
update basi_240
set cod_enquadramento_ipi = '999'
where trim(cod_enquadramento_ipi) is null;

commit;
/

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f801', 'Enquadramento Legal de IPI',0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f801', 'obrf_menu' ,1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Enquadramento Legal de IPI'
where hdoc_036.codigo_programa = 'obrf_f801'
  and hdoc_036.locale          = 'es_ES';

commit;
/


Insert into HDOC_870 (TAG,LOCALE,DESCRICAO,DATA_CADASTRO,DATA_ALTERACAO,FLAG_ATUALIZADO,FLAG_REPASSADO,DATA_TRADUCAO,USUARIO_TRADUCAO,USUARIO_CADASTRO,COMENTARIO,BLOQUEADO) values ('fy28658','pt_BR','Informe o c�digo de enquadramento legal de IPI.',to_date('29/10/15','DD/MM/RR'),null,'0','4',null,null,'matheus.e',null,'0');
Insert into HDOC_870 (TAG,LOCALE,DESCRICAO,DATA_CADASTRO,DATA_ALTERACAO,FLAG_ATUALIZADO,FLAG_REPASSADO,DATA_TRADUCAO,USUARIO_TRADUCAO,USUARIO_CADASTRO,COMENTARIO,BLOQUEADO) values ('fy28658','es_ES','Informe o c�digo de enquadramento legal de IPI.',to_date('29/10/15','DD/MM/RR'),null,'0','4',null,null,'matheus.e',null,'0');
Insert into HDOC_870 (TAG,LOCALE,DESCRICAO,DATA_CADASTRO,DATA_ALTERACAO,FLAG_ATUALIZADO,FLAG_REPASSADO,DATA_TRADUCAO,USUARIO_TRADUCAO,USUARIO_CADASTRO,COMENTARIO,BLOQUEADO) values ('fy28659','pt_BR','Informe a descri��o do enquadramento legal de IPI.',to_date('29/10/15','DD/MM/RR'),null,'0','4',null,null,'matheus.e',null,'0');
Insert into HDOC_870 (TAG,LOCALE,DESCRICAO,DATA_CADASTRO,DATA_ALTERACAO,FLAG_ATUALIZADO,FLAG_REPASSADO,DATA_TRADUCAO,USUARIO_TRADUCAO,USUARIO_CADASTRO,COMENTARIO,BLOQUEADO) values ('fy28659','es_ES','Informe a descri��o do enquadramento legal de IPI.',to_date('29/10/15','DD/MM/RR'),null,'0','4',null,null,'matheus.e',null,'0');
Insert into HDOC_870 (TAG,LOCALE,DESCRICAO,DATA_CADASTRO,DATA_ALTERACAO,FLAG_ATUALIZADO,FLAG_REPASSADO,DATA_TRADUCAO,USUARIO_TRADUCAO,USUARIO_CADASTRO,COMENTARIO,BLOQUEADO) values ('fy28652','pt_BR','Informe o c�digo de enquadramento legal de IPI de entrada',to_date('29/10/15','DD/MM/RR'),null,'0','4',null,null,'matheus.e',null,'0');
Insert into HDOC_870 (TAG,LOCALE,DESCRICAO,DATA_CADASTRO,DATA_ALTERACAO,FLAG_ATUALIZADO,FLAG_REPASSADO,DATA_TRADUCAO,USUARIO_TRADUCAO,USUARIO_CADASTRO,COMENTARIO,BLOQUEADO) values ('fy28652','es_ES','Informe o c�digo de enquadramento legal de IPI de entrada',to_date('29/10/15','DD/MM/RR'),null,'0','4',null,null,'matheus.e',null,'0');
Insert into HDOC_870 (TAG,LOCALE,DESCRICAO,DATA_CADASTRO,DATA_ALTERACAO,FLAG_ATUALIZADO,FLAG_REPASSADO,DATA_TRADUCAO,USUARIO_TRADUCAO,USUARIO_CADASTRO,COMENTARIO,BLOQUEADO) values ('fy28653','pt_BR','Informe o c�digo de enquadramento legal de IPI de sa�da',to_date('29/10/15','DD/MM/RR'),null,'0','4',null,null,'matheus.e',null,'0');
Insert into HDOC_870 (TAG,LOCALE,DESCRICAO,DATA_CADASTRO,DATA_ALTERACAO,FLAG_ATUALIZADO,FLAG_REPASSADO,DATA_TRADUCAO,USUARIO_TRADUCAO,USUARIO_CADASTRO,COMENTARIO,BLOQUEADO) values ('fy28653','es_ES','Informe o c�digo de enquadramento legal de IPI de sa�da',to_date('29/10/15','DD/MM/RR'),null,'0','4',null,null,'matheus.e',null,'0');
Insert into HDOC_870 (TAG,LOCALE,DESCRICAO,DATA_CADASTRO,DATA_ALTERACAO,FLAG_ATUALIZADO,FLAG_REPASSADO,DATA_TRADUCAO,USUARIO_TRADUCAO,USUARIO_CADASTRO,COMENTARIO,BLOQUEADO) values ('fy28654','pt_BR','Informe o c�digo de enquadramento legal de IPI de sa�da para exporta��o',to_date('29/10/15','DD/MM/RR'),null,'0','4',null,null,'matheus.e',null,'0');
Insert into HDOC_870 (TAG,LOCALE,DESCRICAO,DATA_CADASTRO,DATA_ALTERACAO,FLAG_ATUALIZADO,FLAG_REPASSADO,DATA_TRADUCAO,USUARIO_TRADUCAO,USUARIO_CADASTRO,COMENTARIO,BLOQUEADO) values ('fy28654','es_ES','Informe o c�digo de enquadramento legal de IPI de sa�da para exporta��o',to_date('29/10/15','DD/MM/RR'),null,'0','4',null,null,'matheus.e',null,'0');

commit;
/