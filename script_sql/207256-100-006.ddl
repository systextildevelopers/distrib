alter table ftec_202 add    perfil          varchar2(60);
alter table ftec_202 add    modo_perfil     varchar2(60);
alter table ftec_202 add    modo_estampado  varchar2(10);
alter table ftec_202 add    encolhimento    number(2);

alter table ftec_202 add    salto           varchar2(10);
alter table ftec_202 add    temp_forno      number(3);
alter table ftec_202 add    veloc           number(3);


comment on column ftec_202.perfil           is 'Perfil';
comment on column ftec_202.modo_perfil      is 'Modo do perfil';
comment on column ftec_202.modo_estampado   is 'Modo do estampado';
comment on column ftec_202.encolhimento     is 'Encolhimento';
comment on column ftec_202.salto            is 'Salto';
comment on column ftec_202.temp_forno       is 'Temperatura forno';
comment on column ftec_202.veloc            is 'Velocidade';

exec inter_pr_recompile;
