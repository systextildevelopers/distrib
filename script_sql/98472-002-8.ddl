alter table obrf_157 
add (vICMSOp51   number(15,2),
     pDIF51      number(9,2),
     vICMSDif51  number(15,2));

comment on column obrf_157.vICMSOp51 
 is 'CST 51 - Valor como se nao tivesse diferimento';
comment on column obrf_157.pDIF51
is 'CST 51 - Percentual diferimento. No caso de diferimento total, informar 100 no percentual de diferimento';

comment on column obrf_157.vICMSDif51
is 'Valor do Icms Diferido';

alter table obrf_157 
modify ( vICMSOp51   default 0.00,
         pDIF51      default 0.00,
         vICMSDif51  default 0.00);

/

exec inter_pr_recompile;
