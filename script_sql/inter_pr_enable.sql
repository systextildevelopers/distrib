
  CREATE OR REPLACE PROCEDURE "INTER_PR_ENABLE" (p_nome_trigger varchar2)
is
   v_tipo_objeto user_objects.object_name%type;
begin
   begin
      select lower(user_objects.object_type)
      into v_tipo_objeto
      from user_objects
      where user_objects.object_name = upper(p_nome_trigger);
   exception when others then
         v_tipo_objeto := '';
   end;

   if v_tipo_objeto = 'trigger'
   then
      begin
         execute immediate 'alter trigger ' || p_nome_trigger || ' enable';
      end;
   end if;

end inter_pr_enable;
 

/

exec inter_pr_recompile;

