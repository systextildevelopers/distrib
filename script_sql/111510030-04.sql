CREATE OR REPLACE TRIGGER "INTER_TR_I_OBRF_016" 
before insert on i_obrf_016
for each row

declare
  v_id_registro number;
begin
   select SEQ_IMPORTACAO.nextval into v_id_registro from dual;
   :new.id_registro     := v_id_registro;
   :new.data_exportacao := sysdate;
end;

/
exec inter_pr_recompile;
