alter table fatu_110 modify (
  cod_rua number(9),
  cod_box number(9)
);

alter table fatu_115 modify (
  cod_rua number(9),
  cod_box number(9)
);

alter table fatu_115_log modify (
  cod_rua_old number(9),
  cod_rua_new number(9),
  cod_box_old number(9),
  cod_box_new number(9)
);

alter table estq_150 modify (
  corredor number(9),
  box number(9)
);
