alter table PCPT_015 add QUANTIDADE2 number(13,7);

update pcpt_015 set QUANTIDADE2=QUANTIDADE;
commit;

alter table PCPT_015 drop column QUANTIDADE;
alter table PCPT_015 rename column QUANTIDADE2 to QUANTIDADE;

exec inter_pr_recompile;
