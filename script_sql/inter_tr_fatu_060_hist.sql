create or replace TRIGGER INTER_TR_FATU_060_HIST
   after insert or
         delete or
         update of  natopeno_nat_oper,
                    classific_fiscal,
                    classif_contabil,
                    centro_custo,
                    codigo_contabil,
                    qtde_item_fatur,
                    valor_unitario,
                    valor_faturado,
                    rateio_despesa,
                    perc_ipi,
                    cvf_ipi_saida,
                    base_ipi,
                    valor_ipi,
                    perc_icms,
                    cvf_icms,
                    base_icms,
                    valor_icms,
                    cvf_pis,
                    cvf_cofins,
                    basi_pis_cofins,
                    valor_pis,
                    valor_cofins,
                    motivo_devolucao,
                    descricao_item,
                    unidade_medida,
                    valor_contabil,
                    desconto_item,
                    natopeno_est_oper,
                    valor_icms_difer,
                    peso_liquido,
                    cod_cancelamento,
                    deposito_transf,
                    cen_custo_transf,
                    qtde_fatu_empe,
                    rateio_desc_propaganda,
                    base_icms_difer,
                    valor_negociacao,
                    perc_icms_diferido,
                    valor_icms_diferido,
                    data_canc_nfisc,
                    rateio_despesas_ipi,
                    rateio_descontos_ipi,
                    processo_contabil_estoque,
                    valor_iss,
                    um_faturamento_um,
                    um_faturamento_qtde,
                    um_faturamento_valor_unit,
                    perc_iss,
                    perc_pis,
                    perc_cofins,
                    cod_csosn,
                    valor_frete,
                    valor_desc,
                    valor_outros,
                    valor_seguro,
                    perc_fcp_uf_dest,
                    perc_icms_uf_dest,
                    perc_icms_partilha,
                    val_fcp_uf_dest,
                    val_icms_uf_dest,
                    val_icms_uf_remet

   on fatu_060
   for each row

-- Finalidade: Registrar altera��es feitas na tabela fatu_060 - itens das notas fiscais de sa�da
-- Autor.....: Aline Blanski
-- Data......: 07/06/10
--
-- Hist�ricos de altera��es na trigger
--
-- Data      Autor           SS          Observa��es
-- 12/07/10  Aline Blanski   58329/001   Adicionado campos na verifica��o da trigger.
-- 29/07/10  Aline Blanski   58331/001   Adicionado campos na verifica��o da trigger.
-- 30/05/11  Vanderlei       59924/001   Realizado altera��es conforme doc da SS.

declare
  ws_tipo_ocorr                     varchar2 (1);
  ws_usuario_rede                   varchar2(20);
  ws_maquina_rede                   varchar2(40);
  ws_aplicativo                     varchar2(20);
  ws_sid                            number(9);
  ws_empresa                        number(3);
  ws_usuario_systextil              varchar2(250);
  ws_locale_usuario                 varchar2(5);
  ws_nome_programa                  varchar2(20);
  ws_seq_item_nfisc_new             NUMBER(5)    default 0;
  ws_num_ordem_serv_new             NUMBER(6)    default 0;
  ws_seq_item_ordem_new             NUMBER(3)    default 0;
  ws_requisicao_new                 NUMBER(6)    default 0;
  ws_seq_requisicao_new             NUMBER(4)    default 0;
  ws_nivel_estrutura_new            VARCHAR2(1)  default '';
  ws_grupo_estrutura_new            VARCHAR2(5)  default '';
  ws_subgru_estrutura_new           VARCHAR2(3)  default '';
  ws_item_estrutura_new             VARCHAR2(6)  default '';
  ws_natopeno_nat_oper_old          NUMBER(3)    default 0;
  ws_natopeno_nat_oper_new          NUMBER(3)    default 0;
  ws_transacao_new                  NUMBER(3)    default 0;
  ws_classific_fiscal_old           VARCHAR2(15) default '';
  ws_classific_fiscal_new           VARCHAR2(15) default '';
  ws_classif_contabil_old           NUMBER(6)    default 0;
  ws_classif_contabil_new           NUMBER(6)    default 0;
  ws_lote_acomp_new                 NUMBER(6)    default 0;
  ws_deposito_new                   NUMBER(3)    default 0;
  ws_centro_custo_old               NUMBER(6)    default 0;
  ws_centro_custo_new               NUMBER(6)    default 0;
  ws_codigo_contabil_old            NUMBER(6)    default 0;
  ws_codigo_contabil_new            NUMBER(6)    default 0;
  ws_qtde_item_fatur_old            NUMBER(15,3) default 0.0;
  ws_qtde_item_fatur_new            NUMBER(15,3) default 0.0;
  ws_valor_unitario_old             NUMBER(15,5) default 0.0;
  ws_valor_unitario_new             NUMBER(15,5) default 0.0;
  ws_valor_faturado_old             NUMBER(15,2) default 0.0;
  ws_valor_faturado_new             NUMBER(15,2) default 0.0;
  ws_rateio_despesa_old             NUMBER(15,2) default 0.0;
  ws_rateio_despesa_new             NUMBER(15,2) default 0.0;
  ws_perc_ipi_old                   NUMBER(6,2)  default 0.0;
  ws_perc_ipi_new                   NUMBER(6,2)  default 0.0;
  ws_cvf_ipi_saida_old              NUMBER(2)    default 0  ;
  ws_cvf_ipi_saida_new              NUMBER(2)    default 0  ;
  ws_base_ipi_old                   NUMBER(15,2) default 0.0;
  ws_base_ipi_new                   NUMBER(15,2) default 0.0;
  ws_valor_ipi_old                  NUMBER(15,2) default 0.0;
  ws_valor_ipi_new                  NUMBER(15,2) default 0.0;
  ws_perc_icms_old                  NUMBER(6,2)  default 0.0;
  ws_perc_icms_new                  NUMBER(6,2)  default 0.0;
  ws_cvf_icms_old                   NUMBER(2)    default 0;
  ws_cvf_icms_new                   NUMBER(2)    default 0;
  ws_base_icms_old                  NUMBER(15,2) default 0.0;
  ws_base_icms_new                  NUMBER(15,2) default 0.0;
  ws_valor_icms_old                 NUMBER(15,2) default 0.0;
  ws_valor_icms_new                 NUMBER(15,2) default 0.0;
  ws_cvf_pis_old                    NUMBER(2)    default 0;
  ws_cvf_pis_new                    NUMBER(2)    default 0;
  ws_cvf_cofins_old                 NUMBER(2)    default 0;
  ws_cvf_cofins_new                 NUMBER(2)    default 0;
  ws_basi_pis_cofins_old            NUMBER(15,2) default 0.00;
  ws_basi_pis_cofins_new            NUMBER(15,2) default 0.00;
  ws_valor_pis_old                  NUMBER(15,2) default 0.00;
  ws_valor_pis_new                  NUMBER(15,2) default 0.00;
  ws_valor_cofins_old               NUMBER(15,2) default 0.00;
  ws_valor_cofins_new               NUMBER(15,2) default 0.00;
  ws_num_nota_orig_new              NUMBER(9)    default 0;
  ws_serie_nota_orig_new            VARCHAR2(3)  default '';
  ws_seq_nota_orig_new              NUMBER(5)    default 0;
  ws_cgc9_origem_new                NUMBER(9)    default 0;
  ws_cgc4_origem_new                NUMBER(4)    default 0;
  ws_cgc2_origem_new                NUMBER(2)    default 0;
  ws_motivo_devolucao_old           NUMBER(3)    default 0;
  ws_motivo_devolucao_new           NUMBER(3)    default 0;
  ws_nota_ajuste_new                NUMBER(9)    default 0;
  ws_serie_ajuste_new               VARCHAR2(3)  default '';
  ws_descricao_item_old             VARCHAR2(120) default '';
  ws_descricao_item_new             VARCHAR2(120) default '';
  ws_unidade_medida_old             VARCHAR2(2)  default '';
  ws_unidade_medida_new             VARCHAR2(2)  default '';
  ws_ch_it_nf_cd_empr_new           number(3)    default 0;
  ws_ch_it_nf_num_nfis_new          number(9)    default 0;
  ws_ch_it_nf_ser_nfis_new          varchar2(3)  default '';
  ws_valor_contabil_old             NUMBER(15,2) default 0.00;
  ws_valor_contabil_new             NUMBER(15,2) default 0.00;
  ws_desconto_item_old              NUMBER(6,2)  default 0.00;
  ws_desconto_item_new              NUMBER(6,2)  default 0.00;
  ws_natopeno_est_oper_old          VARCHAR2(2)  default '';
  ws_natopeno_est_oper_new          VARCHAR2(2)  default '';
  ws_valor_icms_difer_old           NUMBER(11,2) default 0.00;
  ws_valor_icms_difer_new           NUMBER(11,2) default 0.00;
  ws_peso_liquido_old               NUMBER(13,4) default 0.0000;
  ws_peso_liquido_new               NUMBER(13,4) default 0.0000;
  ws_cod_cancelamento_old           NUMBER(2)    default 0;
  ws_cod_cancelamento_new           NUMBER(2)    default 0;
  ws_deposito_transf_old            NUMBER(3)    default 0;
  ws_deposito_transf_new            NUMBER(3)    default 0;
  ws_cen_custo_transf_old           NUMBER(6)    default 0;
  ws_cen_custo_transf_new           NUMBER(6)    default 0;
  ws_qtde_fatu_empe_old             NUMBER(14,3) default 0.000;
  ws_qtde_fatu_empe_new             NUMBER(14,3) default 0.000;
  ws_rateio_desc_propag_old         NUMBER(15,2) default 0.00;
  ws_rateio_desc_propag_new         NUMBER(15,2) default 0.00;
  ws_base_icms_difer_old            NUMBER(15,2) default 0.00;
  ws_base_icms_difer_new            NUMBER(15,2) default 0.00;
  ws_valor_negociacao_old           NUMBER(15,2) default 0.00;
  ws_valor_negociacao_new           NUMBER(15,2) default 0.00;
  ws_perc_icms_diferido_old         NUMBER(6,2)  default 0.00;
  ws_perc_icms_diferido_new         NUMBER(6,2)  default 0.00;
  ws_valor_icms_diferido_old        NUMBER(15,2) default 0.00;
  ws_valor_icms_diferido_new        NUMBER(15,2) default 0.00;
  ws_data_canc_nfisc_old            DATE;
  ws_data_canc_nfisc_new            DATE;
  ws_rateio_despesas_ipi_old        NUMBER(15,2) default 0.00;
  ws_rateio_despesas_ipi_new        NUMBER(15,2) default 0.00;
  ws_rateio_descontos_ipi_old       NUMBER(15,2) default 0.00;
  ws_rateio_descontos_ipi_new       NUMBER(15,2) default 0.00;
  ws_processo_cont_estoque_old      NUMBER(1)    default 0;
  ws_processo_cont_estoque_new      NUMBER(1)    default 0;
  ws_pedido_venda_new               fatu_060.pedido_venda%type    default 0;
  ws_valor_iss_old                  NUMBER(15,2) default 0.00;
  ws_valor_iss_new                  NUMBER(15,2) default 0.00;
  ws_um_faturamento_um_old          VARCHAR2(3)  default '';
  ws_um_faturamento_um_new          VARCHAR2(3)  default '';
  ws_um_faturamento_qtde_old        NUMBER(15,3) default 0.000;
  ws_um_faturamento_qtde_new        NUMBER(15,3) default 0.000;
  ws_um_fatu_valor_unit_old         NUMBER(15,5) default 0.00000;
  ws_um_fatu_valor_unit_new         NUMBER(15,5) default 0.00000;
  ws_perc_iss_old                   NUMBER(6,2)  default 0.00;
  ws_perc_iss_new                   NUMBER(6,2)  default 0.00;
  ws_perc_pis_old                   NUMBER(6,2)  default 0.00;
  ws_perc_pis_new                   NUMBER(6,2)  default 0.00;
  ws_perc_cofins_old                NUMBER(6,2)  default 0.00;
  ws_perc_cofins_new                NUMBER(6,2)  default 0.00;
  ws_cod_csosn_old                  NUMBER(3)    default 0;
  ws_cod_csosn_new                  NUMBER(3)    default 0;
  ws_valor_frete_old                NUMBER(15,2) default 0.00;
  ws_valor_frete_new                NUMBER(15,2) default 0.00;
  ws_valor_desc_old                 NUMBER(15,2) default 0.00;
  ws_valor_desc_new                 NUMBER(15,2) default 0.00;
  ws_valor_outros_old               NUMBER(15,2) default 0.00;
  ws_valor_outros_new               NUMBER(15,2) default 0.00;
  ws_valor_seguro_old               NUMBER(15,2) default 0.00;
  ws_valor_seguro_new               NUMBER(15,2) default 0.00;
  
  ws_perc_fcp_uf_dest_old           NUMBER(15,2) default 0.00;
  ws_perc_fcp_uf_dest_new           NUMBER(15,2) default 0.00;
  ws_perc_icms_uf_dest_old          NUMBER(15,2) default 0.00;
  ws_perc_icms_uf_dest_new          NUMBER(15,2) default 0.00;
  ws_perc_icms_partilha_old         NUMBER(15,2) default 0.00;
  ws_perc_icms_partilha_new         NUMBER(15,2) default 0.00;
  ws_val_fcp_uf_dest_old            NUMBER(15,2) default 0.00;
  ws_val_fcp_uf_dest_new            NUMBER(15,2) default 0.00;
  ws_val_icms_uf_dest_old           NUMBER(15,2) default 0.00;
  ws_val_icms_uf_dest_new           NUMBER(15,2) default 0.00;
  ws_val_icms_uf_remet_old          NUMBER(15,2) default 0.00;
  ws_val_icms_uf_remet_new          NUMBER(15,2) default 0.00;


begin
   if INSERTING then
      ws_tipo_ocorr                     := 'I';
      ws_seq_item_nfisc_new             := :new.seq_item_nfisc;
      ws_num_ordem_serv_new             := :new.num_ordem_serv;
      ws_seq_item_ordem_new             := :new.seq_item_ordem;
      ws_requisicao_new                 := :new.requisicao;
      ws_seq_requisicao_new             := :new.seq_requisicao;
      ws_nivel_estrutura_new            := :new.nivel_estrutura;
      ws_grupo_estrutura_new            := :new.grupo_estrutura;
      ws_subgru_estrutura_new           := :new.subgru_estrutura;
      ws_item_estrutura_new             := :new.item_estrutura;
      ws_natopeno_nat_oper_old          := 0;
      ws_natopeno_nat_oper_new          := :new.natopeno_nat_oper;
      ws_transacao_new                  := :new.transacao;
      ws_classific_fiscal_old           := null;
      ws_classific_fiscal_new           := :new.classific_fiscal;
      ws_classif_contabil_old           := 0;
      ws_classif_contabil_new           := :new.classif_contabil;
      ws_lote_acomp_new                 := :new.lote_acomp;
      ws_deposito_new                   := :new.deposito;
      ws_centro_custo_old               := 0;
      ws_centro_custo_new               := :new.centro_custo;
      ws_codigo_contabil_old            := 0;
      ws_codigo_contabil_new            := :new.codigo_contabil;
      ws_qtde_item_fatur_old            := 0;
      ws_qtde_item_fatur_new            := :new.qtde_item_fatur;
      ws_valor_unitario_old             := 0;
      ws_valor_unitario_new             := :new.valor_unitario;
      ws_valor_faturado_old             := 0;
      ws_valor_faturado_new             := :new.valor_faturado;
      ws_rateio_despesa_old             := 0;
      ws_rateio_despesa_new             := :new.rateio_despesa;
      ws_perc_ipi_old                   := 0;
      ws_perc_ipi_new                   := :new.perc_ipi ;
      ws_cvf_ipi_saida_old              := 0;
      ws_cvf_ipi_saida_new              := :new.cvf_ipi_saida;
      ws_base_ipi_old                   := 0;
      ws_base_ipi_new                   := :new.base_ipi ;
      ws_valor_ipi_old                  := 0;
      ws_valor_ipi_new                  := :new.valor_ipi ;
      ws_perc_icms_old                  := 0;
      ws_perc_icms_new                  := :new.perc_icms;
      ws_cvf_icms_old                   := 0;
      ws_cvf_icms_new                   := :new.cvf_icms ;
      ws_base_icms_old                  := 0;
      ws_base_icms_new                  := :new.base_icms;
      ws_valor_icms_old                 := 0;
      ws_valor_icms_new                 := :new.valor_icms;
      ws_cvf_pis_old                    := 0;
      ws_cvf_pis_new                    := :new.cvf_pis;
      ws_cvf_cofins_old                 := 0;
      ws_cvf_cofins_new                 := :new.cvf_cofins;
      ws_basi_pis_cofins_old            := 0;
      ws_basi_pis_cofins_new            := :new.basi_pis_cofins;
      ws_valor_pis_old                  := 0;
      ws_valor_pis_new                  := :new.valor_pis;
      ws_valor_cofins_old               := 0;
      ws_valor_cofins_new               := :new.valor_cofins;
      ws_num_nota_orig_new              := :new.num_nota_orig;
      ws_serie_nota_orig_new            := :new.serie_nota_orig;
      ws_seq_nota_orig_new              := :new.seq_nota_orig;
      ws_cgc9_origem_new                := :new.cgc9_origem;
      ws_cgc4_origem_new                := :new.cgc4_origem;
      ws_cgc2_origem_new                := :new.cgc2_origem;
      ws_motivo_devolucao_old           := 0;
      ws_motivo_devolucao_new           := :new.motivo_devolucao;
      ws_nota_ajuste_new                := :new.nota_ajuste;
      ws_serie_ajuste_new               := :new.serie_ajuste;
      ws_descricao_item_old             := null;
      ws_descricao_item_new             := :new.descricao_item;
      ws_unidade_medida_old             := null;
      ws_unidade_medida_new             := :new.unidade_medida;
      ws_ch_it_nf_cd_empr_new           := :new.ch_it_nf_cd_empr;
      ws_ch_it_nf_num_nfis_new          := :new.ch_it_nf_num_nfis;
      ws_ch_it_nf_ser_nfis_new          := :new.ch_it_nf_ser_nfis;
      ws_valor_contabil_old             := null;
      ws_valor_contabil_new             := :new.valor_contabil;
      ws_desconto_item_old              := null;
      ws_desconto_item_new              := :new.desconto_item;
      ws_natopeno_est_oper_old          := null;
      ws_natopeno_est_oper_new          := :new.natopeno_est_oper;
      ws_valor_icms_difer_old           := null;
      ws_valor_icms_difer_new           := :new.valor_icms_difer;
      ws_peso_liquido_old               := null;
      ws_peso_liquido_new               := :new.peso_liquido;
      ws_cod_cancelamento_old           := null;
      ws_cod_cancelamento_new           := :new.cod_cancelamento;
      ws_deposito_transf_old            := null;
      ws_deposito_transf_new            := :new.deposito_transf;
      ws_cen_custo_transf_old           := null;
      ws_cen_custo_transf_new           := :new.cen_custo_transf;
      ws_qtde_fatu_empe_old             := null;
      ws_qtde_fatu_empe_new             := :new.qtde_fatu_empe;
      ws_rateio_desc_propag_old         := null;
      ws_rateio_desc_propag_new         := :new.rateio_desc_propaganda;
      ws_base_icms_difer_old            := null;
      ws_base_icms_difer_new            := :new.base_icms_difer;
      ws_valor_negociacao_old           := null;
      ws_valor_negociacao_new           := :new.valor_negociacao;
      ws_perc_icms_diferido_old         := null;
      ws_perc_icms_diferido_new         := :new.perc_icms_diferido;
      ws_valor_icms_diferido_old        := null;
      ws_valor_icms_diferido_new        := :new.valor_icms_diferido;
      ws_data_canc_nfisc_old            := null;
      ws_data_canc_nfisc_new            := :new.data_canc_nfisc;
      ws_rateio_despesas_ipi_old        := null;
      ws_rateio_despesas_ipi_new        := :new.rateio_despesas_ipi;
      ws_rateio_descontos_ipi_old       := null;
      ws_rateio_descontos_ipi_new       := :new.rateio_descontos_ipi;
      ws_processo_cont_estoque_old      := null;
      ws_processo_cont_estoque_new      := :new.processo_contabil_estoque;
      ws_pedido_venda_new               := :new.pedido_venda;
      ws_valor_iss_old                  := null;
      ws_valor_iss_new                  := :new.valor_iss;
      ws_um_faturamento_um_old          := null;
      ws_um_faturamento_um_new          := :new.um_faturamento_um;
      ws_um_faturamento_qtde_old        := null;
      ws_um_faturamento_qtde_new        := :new.um_faturamento_qtde;
      ws_um_fatu_valor_unit_old         := null;
      ws_um_fatu_valor_unit_new         := :new.um_faturamento_valor_unit;
      ws_perc_iss_old                   := null;
      ws_perc_iss_new                   := :new.perc_iss;
      ws_perc_pis_old                   := null;
      ws_perc_pis_new                   := :new.perc_pis;
      ws_perc_cofins_old                := null;
      ws_perc_cofins_new                := :new.perc_cofins;
      ws_cod_csosn_old                  := null;
      ws_cod_csosn_new                  := :new.cod_csosn;
      ws_valor_frete_old                := null;
      ws_valor_frete_new                := :new.valor_frete;
      ws_valor_desc_old                 := null;
      ws_valor_desc_new                 := :new.valor_desc;
      ws_valor_outros_old               := null;
      ws_valor_outros_new               := :new.valor_outros;
      ws_valor_seguro_old               := null;
      ws_valor_seguro_new               := :new.valor_seguro;
      
      ws_perc_fcp_uf_dest_old           := 0.00;
      ws_perc_fcp_uf_dest_new           := :new.perc_fcp_uf_dest;
      ws_perc_icms_uf_dest_old          := 0.00;
      ws_perc_icms_uf_dest_new          := :new.perc_icms_uf_dest;
      ws_perc_icms_partilha_old         := 0.00;
      ws_perc_icms_partilha_new         := :new.perc_icms_partilha;
      ws_val_fcp_uf_dest_old            := 0.00;
      ws_val_fcp_uf_dest_new            := :new.val_fcp_uf_dest;
      ws_val_icms_uf_dest_old           := 0.00;
      ws_val_icms_uf_dest_new           := :new.val_icms_uf_dest;
      ws_val_icms_uf_remet_old          := 0.00;
      ws_val_icms_uf_remet_new          := :new.val_icms_uf_remet;

   elsif UPDATING then

      ws_tipo_ocorr                     := 'A';
      ws_seq_item_nfisc_new             := :new.seq_item_nfisc;
      ws_num_ordem_serv_new             := :new.num_ordem_serv;
      ws_seq_item_ordem_new             := :new.seq_item_ordem;
      ws_requisicao_new                 := :new.requisicao;
      ws_seq_requisicao_new             := :new.seq_requisicao;
      ws_nivel_estrutura_new            := :new.nivel_estrutura;
      ws_grupo_estrutura_new            := :new.grupo_estrutura;
      ws_subgru_estrutura_new           := :new.subgru_estrutura;
      ws_item_estrutura_new             := :new.item_estrutura;
      ws_natopeno_nat_oper_old          := :old.natopeno_nat_oper;
      ws_natopeno_nat_oper_new          := :new.natopeno_nat_oper;
      ws_transacao_new                  := :new.transacao;
      ws_classific_fiscal_old           := :old.classific_fiscal;
      ws_classific_fiscal_new           := :new.classific_fiscal;
      ws_classif_contabil_old           := :old.classif_contabil;
      ws_classif_contabil_new           := :new.classif_contabil;
      ws_lote_acomp_new                 := :new.lote_acomp;
      ws_deposito_new                   := :new.deposito;
      ws_centro_custo_old               := :old.centro_custo;
      ws_centro_custo_new               := :new.centro_custo;
      ws_codigo_contabil_old            := :old.codigo_contabil;
      ws_codigo_contabil_new            := :new.codigo_contabil;
      ws_qtde_item_fatur_old            := :old.qtde_item_fatur;
      ws_qtde_item_fatur_new            := :new.qtde_item_fatur;
      ws_valor_unitario_old             := :old.valor_unitario;
      ws_valor_unitario_new             := :new.valor_unitario;
      ws_valor_faturado_old             := :old.valor_faturado;
      ws_valor_faturado_new             := :new.valor_faturado;
      ws_rateio_despesa_old             := :old.rateio_despesa;
      ws_rateio_despesa_new             := :new.rateio_despesa;
      ws_perc_ipi_old                   := :old.perc_ipi ;
      ws_perc_ipi_new                   := :new.perc_ipi ;
      ws_cvf_ipi_saida_old              := :old.cvf_ipi_saida;
      ws_cvf_ipi_saida_new              := :new.cvf_ipi_saida;
      ws_base_ipi_old                   := :old.base_ipi ;
      ws_base_ipi_new                   := :new.base_ipi ;
      ws_valor_ipi_old                  := :old.valor_ipi ;
      ws_valor_ipi_new                  := :new.valor_ipi ;
      ws_perc_icms_old                  := :old.perc_icms;
      ws_perc_icms_new                  := :new.perc_icms;
      ws_cvf_icms_old                   := :old.cvf_icms;
      ws_cvf_icms_new                   := :new.cvf_icms ;
      ws_base_icms_old                  := :old.base_icms;
      ws_base_icms_new                  := :new.base_icms;
      ws_valor_icms_old                 := :old.valor_icms;
      ws_valor_icms_new                 := :new.valor_icms;
      ws_cvf_pis_old                    := :old.cvf_pis;
      ws_cvf_pis_new                    := :new.cvf_pis;
      ws_cvf_cofins_old                 := :old.cvf_cofins;
      ws_cvf_cofins_new                 := :new.cvf_cofins;
      ws_basi_pis_cofins_old            := :old.basi_pis_cofins;
      ws_basi_pis_cofins_new            := :new.basi_pis_cofins;
      ws_valor_pis_old                  := :old.valor_pis;
      ws_valor_pis_new                  := :new.valor_pis;
      ws_valor_cofins_old               := :old.valor_cofins;
      ws_valor_cofins_new               := :new.valor_cofins;
      ws_num_nota_orig_new              := :new.num_nota_orig;
      ws_serie_nota_orig_new            := :new.serie_nota_orig;
      ws_seq_nota_orig_new              := :new.seq_nota_orig;
      ws_cgc9_origem_new                := :new.cgc9_origem;
      ws_cgc4_origem_new                := :new.cgc4_origem;
      ws_cgc2_origem_new                := :new.cgc2_origem;
      ws_motivo_devolucao_old           := :old.motivo_devolucao;
      ws_motivo_devolucao_new           := :new.motivo_devolucao;
      ws_nota_ajuste_new                := :new.nota_ajuste;
      ws_serie_ajuste_new               := :new.serie_ajuste;
      ws_descricao_item_old             := :old.descricao_item;
      ws_descricao_item_new             := :new.descricao_item;
      ws_unidade_medida_old             := :old.unidade_medida;
      ws_unidade_medida_new             := :new.unidade_medida;
      ws_ch_it_nf_cd_empr_new           := :new.ch_it_nf_cd_empr;
      ws_ch_it_nf_num_nfis_new          := :new.ch_it_nf_num_nfis;
      ws_ch_it_nf_ser_nfis_new          := :new.ch_it_nf_ser_nfis;
      ws_valor_contabil_old             := :old.valor_contabil;
      ws_valor_contabil_new             := :new.valor_contabil;
      ws_desconto_item_old              := :old.desconto_item;
      ws_desconto_item_new              := :new.desconto_item;
      ws_natopeno_est_oper_old          := :old.natopeno_est_oper;
      ws_natopeno_est_oper_new          := :new.natopeno_est_oper;
      ws_valor_icms_difer_old           := :old.valor_icms_difer;
      ws_valor_icms_difer_new           := :new.valor_icms_difer;
      ws_peso_liquido_old               := :old.peso_liquido;
      ws_peso_liquido_new               := :new.peso_liquido;
      ws_cod_cancelamento_old           := :old.cod_cancelamento;
      ws_cod_cancelamento_new           := :new.cod_cancelamento;
      ws_deposito_transf_old            := :old.deposito_transf;
      ws_deposito_transf_new            := :new.deposito_transf;
      ws_cen_custo_transf_old           := :old.cen_custo_transf;
      ws_cen_custo_transf_new           := :new.cen_custo_transf;
      ws_qtde_fatu_empe_old             := :old.qtde_fatu_empe;
      ws_qtde_fatu_empe_new             := :new.qtde_fatu_empe;
      ws_rateio_desc_propag_old         := :old.rateio_desc_propaganda;
      ws_rateio_desc_propag_new         := :new.rateio_desc_propaganda;
      ws_base_icms_difer_old            := :old.base_icms_difer;
      ws_base_icms_difer_new            := :new.base_icms_difer;
      ws_valor_negociacao_old           := :old.valor_negociacao;
      ws_valor_negociacao_new           := :new.valor_negociacao;
      ws_perc_icms_diferido_old         := :old.perc_icms_diferido;
      ws_perc_icms_diferido_new         := :new.perc_icms_diferido;
      ws_valor_icms_diferido_old        := :old.valor_icms_diferido;
      ws_valor_icms_diferido_new        := :new.valor_icms_diferido;
      ws_data_canc_nfisc_old            := :old.data_canc_nfisc;
      ws_data_canc_nfisc_new            := :new.data_canc_nfisc;
      ws_rateio_despesas_ipi_old        := :old.rateio_despesas_ipi;
      ws_rateio_despesas_ipi_new        := :new.rateio_despesas_ipi;
      ws_rateio_descontos_ipi_old       := :old.rateio_descontos_ipi;
      ws_rateio_descontos_ipi_new       := :new.rateio_descontos_ipi;
      ws_processo_cont_estoque_old      := :old.processo_contabil_estoque;
      ws_processo_cont_estoque_new      := :new.processo_contabil_estoque;
      ws_pedido_venda_new               := :new.pedido_venda;
      ws_valor_iss_old                  := :old.valor_iss;
      ws_valor_iss_new                  := :new.valor_iss;
      ws_um_faturamento_um_old          := :old.um_faturamento_um;
      ws_um_faturamento_um_new          := :new.um_faturamento_um;
      ws_um_faturamento_qtde_old        := :old.um_faturamento_qtde;
      ws_um_faturamento_qtde_new        := :new.um_faturamento_qtde;
      ws_um_fatu_valor_unit_old         := :old.um_faturamento_valor_unit;
      ws_um_fatu_valor_unit_new         := :new.um_faturamento_valor_unit;
      ws_perc_iss_old                   := :old.perc_iss;
      ws_perc_iss_new                   := :new.perc_iss;
      ws_perc_pis_old                   := :old.perc_pis;
      ws_perc_pis_new                   := :new.perc_pis;
      ws_perc_cofins_old                := :old.perc_cofins;
      ws_perc_cofins_new                := :new.perc_cofins;
      ws_cod_csosn_old                  := :old.cod_csosn;
      ws_cod_csosn_new                  := :new.cod_csosn;
      ws_valor_frete_old                := :old.valor_frete;
      ws_valor_frete_new                := :new.valor_frete;
      ws_valor_desc_old                 := :old.valor_desc;
      ws_valor_desc_new                 := :new.valor_desc;
      ws_valor_outros_old               := :old.valor_outros;
      ws_valor_outros_new               := :new.valor_outros;
      ws_valor_seguro_old               := :old.valor_seguro;
      ws_valor_seguro_new               := :new.valor_seguro;

      ws_perc_fcp_uf_dest_old           := :old.perc_fcp_uf_dest;
      ws_perc_fcp_uf_dest_new           := :new.perc_fcp_uf_dest;
      ws_perc_icms_uf_dest_old          := :old.perc_icms_uf_dest;
      ws_perc_icms_uf_dest_new          := :new.perc_icms_uf_dest;
      ws_perc_icms_partilha_old         := :old.perc_icms_partilha;
      ws_perc_icms_partilha_new         := :new.perc_icms_partilha;
      ws_val_fcp_uf_dest_old            := :old.val_fcp_uf_dest;
      ws_val_fcp_uf_dest_new            := :new.val_fcp_uf_dest;
      ws_val_icms_uf_dest_old           := :old.val_icms_uf_dest;
      ws_val_icms_uf_dest_new           := :new.val_icms_uf_dest;
      ws_val_icms_uf_remet_old          := :old.val_icms_uf_remet;
      ws_val_icms_uf_remet_new          := :new.val_icms_uf_remet;

   elsif DELETING then

      ws_tipo_ocorr                     := 'D';
      ws_seq_item_nfisc_new             := 0;
      ws_num_ordem_serv_new             := 0;
      ws_seq_item_ordem_new             := 0;
      ws_requisicao_new                 := 0;
      ws_seq_requisicao_new             := 0;
      ws_nivel_estrutura_new            := null;
      ws_grupo_estrutura_new            := null;
      ws_subgru_estrutura_new           := null;
      ws_item_estrutura_new             := null;
      ws_natopeno_nat_oper_old          := :old.natopeno_nat_oper;
      ws_natopeno_nat_oper_new          := 0;
      ws_transacao_new                  := 0;
      ws_classific_fiscal_old           := :old.classific_fiscal;
      ws_classific_fiscal_new           := 0;
      ws_classif_contabil_old           := :old.classif_contabil;
      ws_classif_contabil_new           := 0;
      ws_lote_acomp_new                 := 0;
      ws_deposito_new                   := 0;
      ws_centro_custo_old               := :old.centro_custo;
      ws_centro_custo_new               := 0;
      ws_codigo_contabil_old            := :old.codigo_contabil;
      ws_codigo_contabil_new            := 0;
      ws_qtde_item_fatur_old            := :old.qtde_item_fatur;
      ws_qtde_item_fatur_new            := 0;
      ws_valor_unitario_old             := :old.valor_unitario;
      ws_valor_unitario_new             := 0;
      ws_valor_faturado_old             := :old.valor_faturado;
      ws_valor_faturado_new             := 0;
      ws_rateio_despesa_old             := :old.rateio_despesa;
      ws_rateio_despesa_new             := 0;
      ws_perc_ipi_old                   := :old.perc_ipi ;
      ws_perc_ipi_new                   := 0;
      ws_cvf_ipi_saida_old              := :old.cvf_ipi_saida;
      ws_cvf_ipi_saida_new              := 0;
      ws_base_ipi_old                   := :old.base_ipi ;
      ws_base_ipi_new                   := 0;
      ws_valor_ipi_old                  := :old.valor_ipi ;
      ws_valor_ipi_new                  := 0;
      ws_perc_icms_old                  := :old.perc_icms;
      ws_perc_icms_new                  := 0;
      ws_cvf_icms_old                   := :old.cvf_icms;
      ws_cvf_icms_new                   := 0;
      ws_base_icms_old                  := :old.base_icms;
      ws_base_icms_new                  := 0;
      ws_valor_icms_old                 := :old.valor_icms;
      ws_valor_icms_new                 := 0;
      ws_cvf_pis_old                    := :old.cvf_pis;
      ws_cvf_pis_new                    := 0;
      ws_cvf_cofins_old                 := :old.cvf_cofins;
      ws_cvf_cofins_new                 := 0;
      ws_basi_pis_cofins_old            := :old.basi_pis_cofins;
      ws_basi_pis_cofins_new            := 0;
      ws_valor_pis_old                  := :old.valor_pis;
      ws_valor_pis_new                  := 0;
      ws_valor_cofins_old               := :old.valor_cofins;
      ws_valor_cofins_new               := 0;
      ws_num_nota_orig_new              := 0;
      ws_serie_nota_orig_new            := 0;
      ws_seq_nota_orig_new              := null;
      ws_cgc9_origem_new                := 0;
      ws_cgc4_origem_new                := 0;
      ws_cgc2_origem_new                := 0;
      ws_motivo_devolucao_old           := :old.motivo_devolucao;
      ws_motivo_devolucao_new           := 0;
      ws_nota_ajuste_new                := 0;
      ws_serie_ajuste_new               := null;
      ws_descricao_item_old             := :old.descricao_item;
      ws_descricao_item_new             := null;
      ws_unidade_medida_old             := :old.unidade_medida;
      ws_unidade_medida_new             := null;
      ws_ch_it_nf_cd_empr_new           := :old.ch_it_nf_cd_empr;
      ws_ch_it_nf_num_nfis_new          := :old.ch_it_nf_num_nfis;
      ws_ch_it_nf_ser_nfis_new          := :old.ch_it_nf_ser_nfis;
      ws_valor_contabil_old             := :old.valor_contabil;
      ws_valor_contabil_new             := 0;
      ws_desconto_item_old              := :old.desconto_item;
      ws_desconto_item_new              := 0;
      ws_natopeno_est_oper_old          := :old.natopeno_est_oper;
      ws_natopeno_est_oper_new          := null;
      ws_valor_icms_difer_old           := :old.valor_icms_difer;
      ws_valor_icms_difer_new           := 0;
      ws_peso_liquido_old               := :old.peso_liquido;
      ws_peso_liquido_new               := 0;
      ws_cod_cancelamento_old           := :old.cod_cancelamento;
      ws_cod_cancelamento_new           := 0;
      ws_deposito_transf_old            := :old.deposito_transf;
      ws_deposito_transf_new            := 0;
      ws_cen_custo_transf_old           := :old.cen_custo_transf;
      ws_cen_custo_transf_new           := 0;
      ws_qtde_fatu_empe_old             := :old.qtde_fatu_empe;
      ws_qtde_fatu_empe_new             := 0;
      ws_rateio_desc_propag_old         := :old.rateio_desc_propaganda;
      ws_rateio_desc_propag_new         := 0;
      ws_base_icms_difer_old            := :old.base_icms_difer;
      ws_base_icms_difer_new            := 0;
      ws_valor_negociacao_old           := :old.valor_negociacao;
      ws_valor_negociacao_new           := 0;
      ws_perc_icms_diferido_old         := :old.perc_icms_diferido;
      ws_perc_icms_diferido_new         := 0;
      ws_valor_icms_diferido_old        := :old.valor_icms_diferido;
      ws_valor_icms_diferido_new        := 0;
      ws_data_canc_nfisc_old            := :old.data_canc_nfisc;
      ws_data_canc_nfisc_new            := null;
      ws_rateio_despesas_ipi_old        := :old.rateio_despesas_ipi;
      ws_rateio_despesas_ipi_new        := 0;
      ws_rateio_descontos_ipi_old       := :old.rateio_descontos_ipi;
      ws_rateio_descontos_ipi_new       := 0;
      ws_processo_cont_estoque_old      := :old.processo_contabil_estoque;
      ws_processo_cont_estoque_new      := 0;
      ws_pedido_venda_new               := 0;
      ws_valor_iss_old                  := :old.valor_iss;
      ws_valor_iss_new                  := 0;
      ws_um_faturamento_um_old          := :old.um_faturamento_um;
      ws_um_faturamento_um_new          := null;
      ws_um_faturamento_qtde_old        := :old.um_faturamento_qtde;
      ws_um_faturamento_qtde_new        := 0;
      ws_um_fatu_valor_unit_old         := :old.um_faturamento_valor_unit;
      ws_um_fatu_valor_unit_new         := 0;
      ws_perc_iss_old                   := :old.perc_iss;
      ws_perc_iss_new                   := 0;
      ws_perc_pis_old                   := :old.perc_pis;
      ws_perc_pis_new                   := 0;
      ws_perc_cofins_old                := :old.perc_cofins;
      ws_perc_cofins_new                := 0;
      ws_cod_csosn_old                  := :old.cod_csosn;
      ws_cod_csosn_new                  := 0;
      ws_valor_frete_old                := :old.valor_frete;
      ws_valor_frete_new                := 0;
      ws_valor_desc_old                 := :old.valor_desc;
      ws_valor_desc_new                 := 0;
      ws_valor_outros_old               := :old.valor_outros;
      ws_valor_outros_new               := 0;
      ws_valor_seguro_old               := :old.valor_seguro;
      ws_valor_seguro_new               := 0;

      ws_perc_fcp_uf_dest_old           := :old.perc_fcp_uf_dest;
      ws_perc_fcp_uf_dest_new           := 0.00;
      ws_perc_icms_uf_dest_old          := :old.perc_icms_uf_dest;
      ws_perc_icms_uf_dest_new          := 0.00;
      ws_perc_icms_partilha_old         := :old.perc_icms_partilha;
      ws_perc_icms_partilha_new         := 0.00;
      ws_val_fcp_uf_dest_old            := :old.val_fcp_uf_dest;
      ws_val_fcp_uf_dest_new            := 0.00;
      ws_val_icms_uf_dest_old           := :old.val_icms_uf_dest;
      ws_val_icms_uf_dest_new           := 0.00;
      ws_val_icms_uf_remet_old          := :old.val_icms_uf_remet;
      ws_val_icms_uf_remet_new          := 0.00;

   end if;

   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   ws_nome_programa := inter_fn_nome_programa(ws_sid);  
 

   INSERT INTO fatu_060_hist
      ( aplicacao,                        tipo_ocorr,
        data_ocorr,
        usuario_rede,                     maquina_rede,
        nome_programa,                    usuario_systextil,
        seq_item_nfisc_new,
        num_ordem_serv_new,
        seq_item_ordem_new,
        requisicao_new,
        seq_requisicao_new,
        nivel_estrutura_new,
        grupo_estrutura_new,
        subgru_estrutura_new,
        item_estrutura_new,
        natopeno_nat_oper_old,            natopeno_nat_oper_new,
        transacao_new,
        classific_fiscal_old,             classific_fiscal_new,
        classif_contabil_old,             classif_contabil_new,
        lote_acomp_new,
        deposito_new,
        centro_custo_old,                 centro_custo_new,
        codigo_contabil_old,              codigo_contabil_new,
        qtde_item_fatur_old,              qtde_item_fatur_new,
        valor_unitario_old,               valor_unitario_new,
        valor_faturado_old,               valor_faturado_new,
        rateio_despesa_old,               rateio_despesa_new,
        perc_ipi_old,                     perc_ipi_new,
        cvf_ipi_saida_old,                cvf_ipi_saida_new,
        base_ipi_old,                     base_ipi_new,
        valor_ipi_old,                    valor_ipi_new,
        perc_icms_old,                    perc_icms_new,
        cvf_icms_old,                     cvf_icms_new,
        base_icms_old,                    base_icms_new,
        valor_icms_old,                   valor_icms_new,
        cvf_pis_old,                      cvf_pis_new,
        cvf_cofins_old,                   cvf_cofins_new,
        basi_pis_cofins_old,              basi_pis_cofins_new,
        valor_pis_old,                    valor_pis_new,
        valor_cofins_old,                 valor_cofins_new,
        num_nota_orig_new,
        serie_nota_orig_new,
        seq_nota_orig_new,
        cgc9_origem_new,
        cgc4_origem_new,
        cgc2_origem_new,
        motivo_devolucao_old,             motivo_devolucao_new,
        nota_ajuste_new,
        serie_ajuste_new,
        descricao_item_old,               descricao_item_new,
        unidade_medida_old,               unidade_medida_new,
        ch_it_nf_cd_empr_new,
        ch_it_nf_num_nfis_new,
        ch_it_nf_ser_nfis_new,
        valor_contabil_old,               valor_contabil_new,
        desconto_item_old,                desconto_item_new,
        natopeno_est_oper_old,            natopeno_est_oper_new,
        valor_icms_difer_old,             valor_icms_difer_new,
        peso_liquido_old,                 peso_liquido_new,
        cod_cancelamento_old,             cod_cancelamento_new,
        deposito_transf_old,              deposito_transf_new,
        cen_custo_transf_old,             cen_custo_transf_new,
        qtde_fatu_empe_old,               qtde_fatu_empe_new,
        rateio_desc_propaganda_old,       rateio_desc_propaganda_new,
        base_icms_difer_old,              base_icms_difer_new,
        valor_negociacao_old,             valor_negociacao_new,
        perc_icms_diferido_old,           perc_icms_diferido_new,
        valor_icms_diferido_old,          valor_icms_diferido_new,
        data_canc_nfisc_old,              data_canc_nfisc_new,
        rateio_despesas_ipi_old,          rateio_despesas_ipi_new,
        rateio_descontos_ipi_old,         rateio_descontos_ipi_new,
        processo_contabil_estoque_old,    processo_contabil_estoque_new,
        pedido_venda_new,
        valor_iss_old,                    valor_iss_new,
        um_faturamento_um_old,            um_faturamento_um_new,
        um_faturamento_qtde_old,          um_faturamento_qtde_new,
        um_faturamento_valor_unit_old,    um_faturamento_valor_unit_new,
        perc_iss_old,                     perc_iss_new,
        perc_pis_old,                     perc_pis_new,
        perc_cofins_old,                  perc_cofins_new,
        cod_csosn_old,                    cod_csosn_new,
        valor_frete_old,                  valor_frete_new,
        valor_desc_old,                   valor_desc_new,
        valor_outros_old,                 valor_outros_new,
        valor_seguro_old,                 valor_seguro_new,
        
        perc_fcp_uf_dest_old,
        perc_fcp_uf_dest_new,
        perc_icms_uf_dest_old,
        perc_icms_uf_dest_new,
        perc_icms_partilha_old,
        perc_icms_partilha_new,
        val_fcp_uf_dest_old ,
        val_fcp_uf_dest_new ,
        val_icms_uf_dest_old ,
        val_icms_uf_dest_new ,
        val_icms_uf_remet_old,
        val_icms_uf_remet_new
        
   )
   VALUES
      ( ws_aplicativo,                    ws_tipo_ocorr,
        sysdate,
        ws_usuario_rede,                  ws_maquina_rede,
        ws_nome_programa,                 ws_usuario_systextil,
        ws_seq_item_nfisc_new,
        ws_num_ordem_serv_new,
        ws_seq_item_ordem_new,
        ws_requisicao_new,
        ws_seq_requisicao_new,
        ws_nivel_estrutura_new,
        ws_grupo_estrutura_new,
        ws_subgru_estrutura_new,
        ws_item_estrutura_new,
        ws_natopeno_nat_oper_old,         ws_natopeno_nat_oper_new,
        ws_transacao_new,
        ws_classific_fiscal_old,          ws_classific_fiscal_new,
        ws_classif_contabil_old,          ws_classif_contabil_new,
        ws_lote_acomp_new,
        ws_deposito_new,
        ws_centro_custo_old,              ws_centro_custo_new,
        ws_codigo_contabil_old,           ws_codigo_contabil_new,
        ws_qtde_item_fatur_old,           ws_qtde_item_fatur_new,
        ws_valor_unitario_old,            ws_valor_unitario_new,
        ws_valor_faturado_old,            ws_valor_faturado_new,
        ws_rateio_despesa_old,            ws_rateio_despesa_new,
        ws_perc_ipi_old,                  ws_perc_ipi_new,
        ws_cvf_ipi_saida_old,             ws_cvf_ipi_saida_new,
        ws_base_ipi_old,                  ws_base_ipi_new,
        ws_valor_ipi_old,                 ws_valor_ipi_new,
        ws_perc_icms_old,                 ws_perc_icms_new,
        ws_cvf_icms_old,                  ws_cvf_icms_new,
        ws_base_icms_old,                 ws_base_icms_new,
        ws_valor_icms_old,                ws_valor_icms_new,
        ws_cvf_pis_old,                   ws_cvf_pis_new,
        ws_cvf_cofins_old,                ws_cvf_cofins_new,
        ws_basi_pis_cofins_old,           ws_basi_pis_cofins_new,
        ws_valor_pis_old,                 ws_valor_pis_new,
        ws_valor_cofins_old,              ws_valor_cofins_new,
        ws_num_nota_orig_new,
        ws_serie_nota_orig_new,
        ws_seq_nota_orig_new,
        ws_cgc9_origem_new,
        ws_cgc4_origem_new,
        ws_cgc2_origem_new,
        ws_motivo_devolucao_old,          ws_motivo_devolucao_new,
        ws_nota_ajuste_new,
        ws_serie_ajuste_new,
        ws_descricao_item_old,            ws_descricao_item_new,
        ws_unidade_medida_old,            ws_unidade_medida_new,
        ws_ch_it_nf_cd_empr_new,
        ws_ch_it_nf_num_nfis_new,
        ws_ch_it_nf_ser_nfis_new,
        ws_valor_contabil_old,            ws_valor_contabil_new,
        ws_desconto_item_old,             ws_desconto_item_new,
        ws_natopeno_est_oper_old,         ws_natopeno_est_oper_new,
        ws_valor_icms_difer_old,          ws_valor_icms_difer_new,
        ws_peso_liquido_old,              ws_peso_liquido_new,
        ws_cod_cancelamento_old,          ws_cod_cancelamento_new,
        ws_deposito_transf_old,           ws_deposito_transf_new,
        ws_cen_custo_transf_old,          ws_cen_custo_transf_new,
        ws_qtde_fatu_empe_old,            ws_qtde_fatu_empe_new,
        ws_rateio_desc_propag_old,        ws_rateio_desc_propag_new,
        ws_base_icms_difer_old,           ws_base_icms_difer_new,
        ws_valor_negociacao_old,          ws_valor_negociacao_new,
        ws_perc_icms_diferido_old,        ws_perc_icms_diferido_new,
        ws_valor_icms_diferido_old,       ws_valor_icms_diferido_new,
        ws_data_canc_nfisc_old,           ws_data_canc_nfisc_new,
        ws_rateio_despesas_ipi_old,       ws_rateio_despesas_ipi_new,
        ws_rateio_descontos_ipi_old,      ws_rateio_descontos_ipi_new,
        ws_processo_cont_estoque_old,     ws_processo_cont_estoque_new,
        ws_pedido_venda_new,
        ws_valor_iss_old,                 ws_valor_iss_new,
        ws_um_faturamento_um_old,         ws_um_faturamento_um_new,
        ws_um_faturamento_qtde_old,       ws_um_faturamento_qtde_new,
        ws_um_fatu_valor_unit_old,        ws_um_fatu_valor_unit_new,
        ws_perc_iss_old,                  ws_perc_iss_new,
        ws_perc_pis_old,                  ws_perc_pis_new,
        ws_perc_cofins_old,               ws_perc_cofins_new,
        ws_cod_csosn_old,                 ws_cod_csosn_new,
        ws_valor_frete_old,               ws_valor_frete_new,
        ws_valor_desc_old,                ws_valor_desc_new,
        ws_valor_outros_old,              ws_valor_outros_new,
        ws_valor_seguro_old,              ws_valor_seguro_new,
        
        ws_perc_fcp_uf_dest_old           ,
        ws_perc_fcp_uf_dest_new           ,
        ws_perc_icms_uf_dest_old          ,
        ws_perc_icms_uf_dest_new          ,
        ws_perc_icms_partilha_old         ,
        ws_perc_icms_partilha_new         ,
        ws_val_fcp_uf_dest_old            ,
        ws_val_fcp_uf_dest_new            ,
        ws_val_icms_uf_dest_old           ,
        ws_val_icms_uf_dest_new           ,
        ws_val_icms_uf_remet_old          ,
        ws_val_icms_uf_remet_new 
       );

end inter_tr_fatu_060_hist;

-- ALTER TRIGGER "INTER_TR_FATU_060_HIST" ENABLE/

/
