CREATE OR REPLACE FUNCTION INTER_FN_CUSTO_MINUTO(fac9 number, fac4 number, fac2 number, linha number) RETURN number AS
    custo number;
begin
    begin
      SELECT valor_minuto INTO custo FROM
      (select valor_minuto
      from pcpc_601 pc601
      where codigo_familia = (select divisao_producao 
                              from basi_180
                              where faccionista9 = fac9
                              and   faccionista4 = fac4 
                              and   faccionista2 = fac2
                              and rownum < 2)
      and linha_produto in (0, linha)
      and 12 * ano + mes = (select MAX(12 * ano + mes) from pcpc_601 where codigo_familia = pc601.codigo_familia)
      order by linha_produto desc)
      WHERE ROWNUM = 1;
    exception when no_data_found then
      custo := 0;
    end;
    return custo;
end;
/
