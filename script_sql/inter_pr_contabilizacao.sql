create or replace procedure "INTER_PR_CONTABILIZACAO"(p_cod_empresa   	 IN NUMBER,
                                                      p_c_custo			 IN NUMBER,
                                                      p_cnpj9            IN NUMBER,
                                                      p_cnpj4            IN NUMBER,
                                                      p_cnpj2            IN NUMBER,
                                                      p_transacao        IN OUT NUMBER,
                                                      p_codigo_historico IN NUMBER,
                                                      p_conta            IN NUMBER,
                                                      p_cod_contab_deb   IN NUMBER,
                                                      p_cod_contab_cre   IN NUMBER,
                                                      p_documento        IN NUMBER,
                                                      p_data_transacao	 IN DATE,
                                                      p_valor            IN NUMBER,
                                                      p_origem           IN NUMBER,
                                                      p_tp_cta_deb       IN NUMBER,
                                                      p_tp_cta_cre       IN NUMBER,
                                                      p_num_lcto         IN OUT number,
                                                      p_des_erro      	 OUT varchar2) AS
                                                      
    v_empresa_matriz number;
    v_exercicio number(4);
    v_codigo_historico number;
    v_exercicio_doc number(4);
    cta_debito number;
    cta_credito number;
    v_cod_contabil_forn number;
	v_compl_histor1 varchar2(100);
    v_valor_deb  number;
    v_valor_cred number;
BEGIN
    
    BEGIN
       v_codigo_historico := p_codigo_historico;
       
       begin
          select fatu_500.codigo_matriz
          into v_empresa_matriz
          from fatu_500
          where fatu_500.codigo_empresa = p_cod_empresa;
      exception
          when no_data_found then
          v_empresa_matriz := 0;
      end;
      
       IF v_empresa_matriz > 0
       THEN
           
           v_exercicio_doc := inter_fn_checa_data(v_empresa_matriz, p_data_transacao, 0);
           v_exercicio := inter_fn_checa_data(v_empresa_matriz, p_data_transacao,0);

           if v_exercicio_doc < 0
           then
               v_exercicio_doc := v_exercicio;
           end if;

           if v_exercicio <= 0
           then
               p_des_erro := 'Exercício e/ou período contábil não encontrado ou fechado. 
                             Não será permitido realizar movimentações nesta data. Contate o contador. ' 
                             ||' Data transação: '|| p_data_transacao
                             ||' Exercicio: '|| v_exercicio
                             ||' Exercicio doc: '|| v_exercicio_doc;
               return;
           end if;
          
           IF v_exercicio > 0 THEN
                
               cta_debito := INTER_FN_ENCONTRA_CONTA(p_cod_empresa,p_tp_cta_deb,
                                                       p_cod_contab_deb,
                                                       p_transacao,
                                                       p_c_custo,
                                                       v_exercicio,
                                                       v_exercicio_doc);
               if cta_debito < 0
               THEN
                   p_des_erro := 'Conta contabil de débito não encontrada. ' || 
                               'Esta transação não será confirmada. Contate o contador. ' ||
                               'Tipo contábil: ' || p_tp_cta_deb || ' , Transação: ' || p_transacao || ' ' ||
                               'Código contábil: ' || p_cod_contab_deb;
                   return;
               END IF;
               
               v_valor_deb   := p_valor;
               v_valor_cred  := p_valor;
                                        
               --Se for a origem 6 a transação vai ser 0.
               --Passar o campo p_tp_cta_cre 20
               if p_tp_cta_cre = 20 then
                    p_transacao := 0;
               end if;
               
               -- para estorno adiantamento

               cta_credito := INTER_FN_ENCONTRA_CONTA(p_cod_empresa,p_tp_cta_cre,
                                                       p_cod_contab_cre,
                                                       p_transacao,
                                                       p_c_custo,
                                                       v_exercicio,
                                                       v_exercicio_doc);
               
               if cta_credito < 0
               THEN
                   p_des_erro := 'Conta contabil de crédito não encontrada. ' || 
                               'Esta transação não será confirmada. Contate o contador. ' ||
                               'Tipo contábil: '|| p_tp_cta_cre || ' , Transação: ' || p_transacao || ' ' ||
                               'Código contábil: ' || p_cod_contab_cre;
                   return;
               END IF;
               
               if cta_debito > 0 and cta_credito > 0 --(cta_credito > 0 or v_valor_cred = 0)
               then
                v_compl_histor1 := p_documento;

                inter_pr_gera_lanc_cont(p_cod_empresa,
                                        p_origem,
                                        p_num_lcto,
                                        p_c_custo,
                                        p_data_transacao,
                                        v_codigo_historico,
                                        v_compl_histor1,
                                        1, 
                                        p_transacao,
                                        cta_debito,
                                        v_valor_deb, 
                                        cta_credito,
                                        v_valor_cred, 
                                        0,
                                        p_conta,
                                        p_data_transacao,
                                        p_documento, 
                                        p_cnpj9, 
                                        p_cnpj4, 
                                        p_cnpj2, 
                                        2, 
                                        p_documento, 
                                        '1',
                                        1, 
                                        1,
                                        0,
                                        0, 
                                        0, 
                                        0, 
                                        p_des_erro);
               end if;
           END IF;
       END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
           raise_application_error(-20001, 'Erro ao processar inter_pr_contabilização ' ||
                                           ' p_cod_empresa ' || p_cod_empresa ||
                                           ' p_origem ' || 0 ||
                                           ' p_num_lcto ' || p_num_lcto ||
                                           ' p_c_custo ' || p_c_custo ||
                                           ' v_codigo_historico ' || v_codigo_historico ||
                                           ' v_compl_histor1 ' || v_compl_histor1 ||
                                           ' p_transacao ' || p_transacao ||
                                           ' cta_debito ' || cta_debito ||
                                           ' cta_credito ' || cta_credito ||
                                           ' p_valor ' || p_valor ||
                                           ' p_conta ' || p_conta ||
                                           ' p_data_transacao ' || p_data_transacao ||
                                           ' p_documento ' || p_documento ||
                                           ' p_cnpj9 ' || p_cnpj9 ||
                                           ' p_cnpj4 ' || p_cnpj4 ||
                                           ' p_cnpj2 ' || p_cnpj2 
           || SQLERRM);
    END;
END "INTER_PR_CONTABILIZACAO";

/

exec inter_pr_recompile;
