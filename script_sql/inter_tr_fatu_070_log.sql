create or replace TRIGGER "INTER_TR_FATU_070_LOG" 
   after insert or delete or update of
    codigo_empresa    
    ,cli_dup_cgc_cli9     
    ,cli_dup_cgc_cli4
    ,cli_dup_cgc_cli2 
    ,tipo_titulo    
    ,num_duplicata
    ,seq_duplicatas    
    ,data_venc_duplic 
    ,valor_duplicata
    ,situacao_duplic  
    ,cod_canc_duplic  
    ,data_canc_duplic
    ,perc_juro_duplic 
    ,perc_desc_duplic 
    ,portador_duplic
    ,percentual_comis   
    ,cod_rep_cliente
    ,duplic_emitida     
    ,cod_local         
    ,moeda_titulo
    ,data_prorrogacao  
    ,perc_comis_crec  
    ,cli9resptit
    ,cli4resptit       
    ,cli2resptit      
    ,cgc9_endosso
    ,cgc4_endosso      
    ,cgc2_endosso     
    ,tipo_comissao
    ,codigo_administr   
    ,comissao_administr 
    ,perc_comis_crec_adm
    ,saldo_duplicata    
    ,seq_end_cobranca   
    ,nr_identificacao
    ,cod_carteira       
    ,status_serasa_pefin
    ,motivo_bxa_serasa_pefin 
    ,nr_titulo_banco  
    ,conta_corrente  
    ,cod_transacao

    ,atraso_pela_renegociacao
    ,bandeira_cartao
    ,base_calc_comis
    ,bolepix
    ,calc_vl_presente
    ,cartao
    ,cd_centro_custo
    ,chavepix
    ,cli_dup_cgc_cli_o
    ,cli_dup_cgc_cli_r
    ,cmc7_cheque
    ,codigo_contabil
    ,codigo_fatura_loja
    ,codigo_request
    ,cod_barras
    ,cod_categoria
    ,cod_emp_adt
    ,cod_forma_pagto
    ,cod_historico
    ,cod_processo
    ,cod_usuario
    ,comissao_lancada
    ,compl_historico
    ,cond_pagto_vendor
    ,controle_cheque
    ,cotacao_moeda
    ,data_aceite_banco
    ,data_alteracao_boleto
    ,data_atualizacao_api
    ,data_autorizacao
    ,data_emissao
    ,data_prorrogacao_ant
    ,data_transf_tit
    ,data_ult_movim_credito
    ,data_ult_movim_pagto
    ,data_vl_presente
    ,duplic_impressa
    ,estabelecimento_opera
    ,estab_centralizador
    ,executa_trigger
    ,flag_alteracao_boleto
    ,flag_marca_cheque
    ,fnd_dt_remessa
    ,fnd_nr_remessa
    ,fnd_situacao
    ,fnd_tem_espelho
    ,id_conc_pagto
    ,id_conc_venda
    ,indice_diario
    ,indice_mensal
    ,linha_digitavel
    ,loja_fat
    ,lote_recebimento
    ,mensagem_boleto
    ,nr_adiantamento
    ,nr_autorizacao_opera
    ,nr_cupom
    ,nr_mtv_prorrogacao
    ,nr_remessa_inadimplencia
    ,nr_solicitacao
    ,nr_solicitacao_inadimplencia
    ,nr_titulo_banco_corresp
    ,numero_bordero
    ,numero_caixa
    ,numero_nsu
    ,numero_processo
    ,numero_referencia
    ,numero_remessa
    ,numero_sequencia
    ,numero_titulo
    ,num_contabil
    ,num_dup_origem
    ,num_nota_fiscal
    ,num_renegociacao
    ,ordem_credito_opera
    ,origem_pedido
    ,pedido_operadora
    ,pedido_venda
    ,port_anterior
    ,posicao_ant
    ,posicao_duplic
    ,previsao
    ,quantidade
    ,referente_nf
    ,ren_num_renegocia
    ,ren_sit_renegocia
    ,responsavel_receb
    ,resumo_venda
    ,scpc_nr_remessa
    ,scpc_situacao
    ,selecionado_credito_reneg
    ,seq_dup_origem
    ,seq_renegociacao
    ,serie_nota_fisc
    ,situacao_inadimplencia
    ,status_credito_opera
    ,status_nsu
    ,tecido_peca
    ,tid
    ,tipopix
    ,tipo_titulo_original
    ,tipo_tit_origem
    ,tipo_transacao_opera
    ,titulo_loja
    ,tit_baixar
    ,tit_renegociado
    ,valor_avp
    ,valor_comis
    ,valor_desconto_aux
    ,valor_desp_cobr
    ,valor_juros_aux
    ,valor_moeda
    ,valor_remessa
    ,valor_saldo_aux
    ,variacao_cambial
    ,vencto_anterior
    ,vlr_presente
    ,vl_liquido_cartao

   on fatu_070
   for each row

declare
    ws_usuario_rede           varchar2(250);
    ws_maquina_rede           varchar2(40);
    ws_aplicativo             varchar2(20);
    ws_sid                    number(9);
    ws_empresa                number(3);
    ws_usuario_systextil      varchar2(250);
    ws_locale_usuario         varchar2(5);

    long_aux                  clob;
    v_old_titulo_banco 		 varchar2(300);
    v_new_titulo_banco 		 varchar2(300);

    v_executa_trigger         number;
    ws_nome_programa          hdoc_090.programa%type;

begin
    -- Dados do usuï¿¿rio logado
    inter_pr_dados_usuario (
        ws_usuario_rede
        ,ws_maquina_rede
        ,ws_aplicativo
        ,ws_sid
        ,ws_usuario_systextil
        ,ws_empresa
        ,ws_locale_usuario
    );

   -- INICIO - Lï¿¿gica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementaï¿¿ï¿¿o executada para limpeza da base de dados do cliente
   -- e replicaï¿¿ï¿¿o dos dados para base histï¿¿rico. (SS.38405)

   ws_nome_programa := inter_fn_nome_programa(ws_sid);  
 
    if inserting then

        if :new.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;

    end if;

    if updating then

        if :old.executa_trigger = 1 or :new.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;

    end if;

    if deleting then

        if :old.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;

    end if;

    if v_executa_trigger = 0 then

        if inserting then

            INSERT INTO hist_100(
                 programa         
                ,tabela
                ,operacao     
                ,data_ocorr
                ,usuario_rede    
                ,maquina_rede
                ,aplicacao         
                ,num01
                ,num02             
                ,num03
                ,num04             
                ,num05
                ,str01             
                ,num06
                ,long01
            )values(
                ws_nome_programa     
                ,'FATU_070'
                ,'I'                 
                ,sysdate
                ,ws_usuario_rede      
                ,ws_maquina_rede
                ,ws_aplicativo        
                ,:new.num_duplicata
                ,:new.cli_dup_cgc_cli9
                ,:new.cli_dup_cgc_cli4
                ,:new.cli_dup_cgc_cli2 
                ,:new.tipo_titulo
                ,:new.seq_duplicatas 
                ,:new.codigo_empresa
                ,SUBSTR('                                                  '
                ||inter_fn_buscar_tag('lb34836#Tï¿¿TULOS A RECEBER'                    ,ws_locale_usuario,ws_usuario_systextil) ||chr(10)
                ||chr(10)|| inter_fn_buscar_tag('lb07207#EMPRESA:'                     ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.codigo_empresa
                -- ||chr(10)|| inter_fn_buscar_tag('lb31254#CLIENTE:'                     ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cli_dup_cgc_cli9 || '/' || :new.cli_dup_cgc_cli4 || '-' || :new.cli_dup_cgc_cli2
                ||chr(10)|| inter_fn_buscar_tag('lb00149#DATA VENCTO:'                 ,ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:new.data_venc_duplic,'dd/mm/yy')
                ||chr(10)|| inter_fn_buscar_tag('lb03009#DATA PRORROGAï¿¿ï¿¿O:'        ,ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:new.data_prorrogacao,'dd/mm/yy')
                -- ||chr(10)|| inter_fn_buscar_tag('lb02861#DATA CANCELAMENTO:'           ,ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:new.data_canc_duplic,'dd/mm/yy')
                ||chr(10)|| inter_fn_buscar_tag('lb05237#VALOR DUPLICATA:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_duplicata
                ||chr(10)|| inter_fn_buscar_tag('lb05873#SALDO DUPLICATA:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.saldo_duplicata
                ||chr(10)|| inter_fn_buscar_tag('lb08122#SITUAï¿¿ï¿¿O :'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.situacao_duplic
                -- ||chr(10)|| inter_fn_buscar_tag('lb31282#COD. CANCELAMENTO:'           ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cod_canc_duplic
                ||chr(10)|| inter_fn_buscar_tag('lb34837#% JUROS:'                     ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.perc_juro_duplic
                ||chr(10)|| inter_fn_buscar_tag('lb03863#% DESC. DUPLIC:'              ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.perc_desc_duplic
                ||chr(10)|| inter_fn_buscar_tag('lb16134#PORTADOR DUPLICATAS:'         ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.portador_duplic
                ||chr(10)|| inter_fn_buscar_tag('lb05911#TIPO COMISSï¿¿O:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tipo_comissao
                ||chr(10)|| inter_fn_buscar_tag('lb00294#REPRESENTANTE:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cod_rep_cliente
                ||chr(10)|| inter_fn_buscar_tag('lb34838#% COMIS. REPRES.:'            ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.percentual_comis
                ||chr(10)|| inter_fn_buscar_tag('lb34839#% FATU/BAIXA:'                ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.perc_comis_crec
                ||chr(10)|| inter_fn_buscar_tag('lb01633#ADMINISTRADOR:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.codigo_administr
                ||chr(10)|| inter_fn_buscar_tag('lb03857#% COMISSï¿¿O (ADMIN):'        ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.comissao_administr
                ||chr(10)|| inter_fn_buscar_tag('lb34839#% FATU/BAIXA:'                ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.perc_comis_crec_adm
                ||chr(10)|| inter_fn_buscar_tag('lb34840#DUPLIC. EMITIDA:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.duplic_emitida
                ||chr(10)|| inter_fn_buscar_tag('lb34841#COD. LOCAL:'                  ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cod_local
                ||chr(10)|| inter_fn_buscar_tag('lb13146#MOEDA:'                       ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.moeda_titulo
                ||chr(10)|| inter_fn_buscar_tag('lb37232#NR IDENTIFICAï¿¿ï¿¿O:'        ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.nr_identificacao
                ||chr(10)|| inter_fn_buscar_tag('lb37233#Cï¿¿D. CARTEIRA:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cod_carteira
                ||chr(10)|| inter_fn_buscar_tag('lb05866#RESPONSï¿¿VEL:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cli9resptit || '/' || :new.cli4resptit || '-' || :new.cli2resptit
                ||chr(10)|| inter_fn_buscar_tag('lb03561#ENDOSSO:'                     ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cgc9_endosso || '/' || :new.cgc4_endosso || '-' || :new.cgc2_endosso
                -- ||chr(10)|| inter_fn_buscar_tag('lb09567#SEQUï¿¿NCIA END. COBRANï¿¿A:' ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.seq_end_cobranca || ' '
                -- ||chr(10)|| inter_fn_buscar_tag('lb41619#NR. TITULO NO BANCO:'         ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.nr_titulo_banco
                -- ||chr(10)|| inter_fn_buscar_tag('lb66107#ATRASO PELA RENEGOCIACAO:'    ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.atraso_pela_renegociacao       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66108#BANDEIRA CARTAO:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.bandeira_cartao       
                ||chr(10)|| inter_fn_buscar_tag('lb66109#BASE CALC COMIS:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.base_calc_comis       
                ||chr(10)|| inter_fn_buscar_tag('lb66110#BOLEPIX:'                     ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.bolepix       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66111#CALC VL PRESENTE:'            ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.calc_vl_presente       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66112#CARTAO:'                      ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cartao       
                ||chr(10)|| inter_fn_buscar_tag('lb66113#CD CENTRO CUSTO:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cd_centro_custo       
                ||chr(10)|| inter_fn_buscar_tag('lb66114#CHAVEPIX:'                    ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.chavepix       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66115#CLI DUP CGC CLI O:'           ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cli_dup_cgc_cli_o       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66116#CLI DUP CGC CLI R:'           ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cli_dup_cgc_cli_r       
                ||chr(10)|| inter_fn_buscar_tag('lb66117#CMC7 CHEQUE:'                 ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cmc7_cheque       
                ||chr(10)|| inter_fn_buscar_tag('lb66118#CODIGO CONTABIL:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.codigo_contabil       
                ||chr(10)|| inter_fn_buscar_tag('lb66119#CODIGO FATURA LOJA:'          ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.codigo_fatura_loja       
                ||chr(10)|| inter_fn_buscar_tag('lb66120#CODIGO REQUEST:'              ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.codigo_request       
                ||chr(10)|| inter_fn_buscar_tag('lb66121#COD BARRAS:'                  ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cod_barras       
                ||chr(10)|| inter_fn_buscar_tag('lb66122#COD CATEGORIA:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cod_categoria       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66123#COD EMP ADT:'                 ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cod_emp_adt       
                ||chr(10)|| inter_fn_buscar_tag('lb66124#COD FORMA PAGTO:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cod_forma_pagto       
                ||chr(10)|| inter_fn_buscar_tag('lb66125#COD HISTORICO:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cod_historico       
                ||chr(10)|| inter_fn_buscar_tag('lb66126#COD PROCESSO:'                ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cod_processo       
                ||chr(10)|| inter_fn_buscar_tag('lb66127#COD USUARIO:'                 ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cod_usuario       
                ||chr(10)|| inter_fn_buscar_tag('lb66128#COMISSAO LANCADA:'            ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.comissao_lancada       
                ||chr(10)|| inter_fn_buscar_tag('lb66129#COMPL HISTORICO:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.compl_historico       
                ||chr(10)|| inter_fn_buscar_tag('lb66130#COND PAGTO VENDOR:'           ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cond_pagto_vendor       
                ||chr(10)|| inter_fn_buscar_tag('lb66131#CONTROLE CHEQUE:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.controle_cheque       
                ||chr(10)|| inter_fn_buscar_tag('lb66132#COTACAO MOEDA:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cotacao_moeda       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66133#DATA ACEITE BANCO:'           ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_aceite_banco       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66134#DATA ALTERACAO BOLETO:'       ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_alteracao_boleto       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66135#DATA ATUALIZACAO API:'        ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_atualizacao_api       
                ||chr(10)|| inter_fn_buscar_tag('lb66136#DATA AUTORIZACAO:'            ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_autorizacao       
                ||chr(10)|| inter_fn_buscar_tag('lb66137#DATA EMISSAO:'                ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_emissao       
                ||chr(10)|| inter_fn_buscar_tag('lb66138#DATA PRORROGACAO ANT:'        ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_prorrogacao_ant       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66139#DATA TRANSF TIT:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_transf_tit       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66140#DATA ULT MOVIM CREDITO:'      ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_ult_movim_credito       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66141#DATA ULT MOVIM PAGTO:'        ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_ult_movim_pagto       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66142#DATA VL PRESENTE:'            ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_vl_presente       
                ||chr(10)|| inter_fn_buscar_tag('lb66143#DUPLIC IMPRESSA:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.duplic_impressa       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66144#ESTABELECIMENTO OPERA:'       ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.estabelecimento_opera       
                ||chr(10)|| inter_fn_buscar_tag('lb66145#ESTAB CENTRALIZADOR:'         ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.estab_centralizador       
                ||chr(10)|| inter_fn_buscar_tag('lb66146#EXECUTA TRIGGER:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.executa_trigger       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66147#FLAG ALTERACAO BOLETO:'       ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.flag_alteracao_boleto       
                ||chr(10)|| inter_fn_buscar_tag('lb66148#FLAG MARCA CHEQUE:'           ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.flag_marca_cheque       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66149#FND DT REMESSA:'              ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.fnd_dt_remessa       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66150#FND NR REMESSA:'              ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.fnd_nr_remessa       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66151#FND SITUACAO:'                ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.fnd_situacao       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66152#FND TEM ESPELHO:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.fnd_tem_espelho       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66153#ID CONC PAGTO:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.id_conc_pagto       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66154#ID CONC VENDA:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.id_conc_venda       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66155#INDICE DIARIO:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.indice_diario       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66156#INDICE MENSAL:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.indice_mensal       
                ||chr(10)|| inter_fn_buscar_tag('lb66157#LINHA DIGITAVEL:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.linha_digitavel       
                ||chr(10)|| inter_fn_buscar_tag('lb66158#LOJA FAT:'                    ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.loja_fat       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66159#LOTE RECEBIMENTO:'            ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.lote_recebimento       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66160#MENSAGEM BOLETO:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.mensagem_boleto       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66161#NR ADIANTAMENTO:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.nr_adiantamento       
                ||chr(10)|| inter_fn_buscar_tag('lb66162#NR AUTORIZACAO OPERA:'        ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.nr_autorizacao_opera       
                ||chr(10)|| inter_fn_buscar_tag('lb66163#NR CUPOM:'                    ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.nr_cupom       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66164#NR MTV PRORROGACAO:'          ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.nr_mtv_prorrogacao       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66165#NR REMESSA INADIMPLENCIA:'    ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.nr_remessa_inadimplencia       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66166#NR SOLICITACAO:'              ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.nr_solicitacao       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66167#NR SOLICITACAO INADIMPLENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.nr_solicitacao_inadimplencia       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66168#NR TITULO BANCO CORRESP:'     ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.nr_titulo_banco_corresp       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66169#NUMERO BORDERO:'              ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.numero_bordero
                ||chr(10)|| inter_fn_buscar_tag('lb66170#NUMERO CAIXA:'                ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.numero_caixa
                ||chr(10)|| inter_fn_buscar_tag('lb66171#NUMERO NSU:'                  ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.numero_nsu      
                ||chr(10)|| inter_fn_buscar_tag('lb66172#NUMERO PROCESSO:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.numero_processo      
                ||chr(10)|| inter_fn_buscar_tag('lb66173#NUMERO REFERENCIA:'           ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.numero_referencia      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66174#NUMERO REMESSA:'              ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.numero_remessa      
                ||chr(10)|| inter_fn_buscar_tag('lb66175#NUMERO SEQUENCIA:'            ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.numero_sequencia      
                ||chr(10)|| inter_fn_buscar_tag('lb66176#NUMERO TITULO:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.numero_titulo      
                ||chr(10)|| inter_fn_buscar_tag('lb66177#NUM CONTABIL:'                ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.num_contabil      
                ||chr(10)|| inter_fn_buscar_tag('lb66178#NUM DUP ORIGEM:'              ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.num_dup_origem      
                ||chr(10)|| inter_fn_buscar_tag('lb66179#NUM NOTA FISCAL:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.num_nota_fiscal     
                ||chr(10)|| inter_fn_buscar_tag('lb66180#NUM RENEGOCIACAO:'            ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.num_renegociacao       
                -- ||chr(10)|| inter_fn_buscar_tag('lb66182#ORDEM CREDITO OPERA:'         ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.ordem_credito_opera      
                ||chr(10)|| inter_fn_buscar_tag('lb66183#ORIGEM PEDIDO:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.origem_pedido      
                ||chr(10)|| inter_fn_buscar_tag('lb66184#PEDIDO OPERADORA:'            ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.pedido_operadora      
                ||chr(10)|| inter_fn_buscar_tag('lb66185#PEDIDO VENDA:'                ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.pedido_venda      
                ||chr(10)|| inter_fn_buscar_tag('lb66186#PORT ANTERIOR:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.port_anterior      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66187#POSICAO ANT:'                 ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.posicao_ant      
                ||chr(10)|| inter_fn_buscar_tag('lb66188#POSICAO DUPLIC:'              ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.posicao_duplic      
                ||chr(10)|| inter_fn_buscar_tag('lb66189#PREVISAO:'                    ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.previsao      
                ||chr(10)|| inter_fn_buscar_tag('lb66190#QUANTIDADE:'                  ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.quantidade      
                ||chr(10)|| inter_fn_buscar_tag('lb66191#REFERENTE NF:'                ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.referente_nf      
                ||chr(10)|| inter_fn_buscar_tag('lb66192#REN NUM RENEGOCIA:'           ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.ren_num_renegocia      
                ||chr(10)|| inter_fn_buscar_tag('lb66193#REN SIT RENEGOCIA:'           ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.ren_sit_renegocia      
                ||chr(10)|| inter_fn_buscar_tag('lb66194#RESPONSAVEL RECEB:'           ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.responsavel_receb      
                ||chr(10)|| inter_fn_buscar_tag('lb66195#RESUMO VENDA:'                ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.resumo_venda      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66196#SCPC NR REMESSA:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.scpc_nr_remessa      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66197#SCPC SITUACAO:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.scpc_situacao      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66198#SELECIONADO CREDITO RENEG:'   ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.selecionado_credito_reneg      
                ||chr(10)|| inter_fn_buscar_tag('lb66199#SEQ DUP ORIGEM:'              ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.seq_dup_origem      
                ||chr(10)|| inter_fn_buscar_tag('lb66200#SEQ RENEGOCIACAO:'            ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.seq_renegociacao      
                ||chr(10)|| inter_fn_buscar_tag('lb66201#SERIE NOTA FISC:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.serie_nota_fisc      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66202#SITUACAO INADIMPLENCIA:'      ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.situacao_inadimplencia      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66203#STATUS CREDITO OPERA:'        ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.status_credito_opera      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66204#STATUS NSU:'                  ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.status_nsu      
                ||chr(10)|| inter_fn_buscar_tag('lb66205#TECIDO PECA:'                 ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tecido_peca      
                ||chr(10)|| inter_fn_buscar_tag('lb66206#TID:'                         ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tid      
                ||chr(10)|| inter_fn_buscar_tag('lb66207#TIPOPIX:'                     ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tipopix      
                ||chr(10)|| inter_fn_buscar_tag('lb66208#TIPO TITULO ORIGINAL:'        ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tipo_titulo_original      
                ||chr(10)|| inter_fn_buscar_tag('lb66209#TIPO TIT ORIGEM:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tipo_tit_origem      
                ||chr(10)|| inter_fn_buscar_tag('lb66210#TIPO TRANSACAO OPERA:'        ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tipo_transacao_opera      
                ||chr(10)|| inter_fn_buscar_tag('lb66211#TITULO LOJA:'                 ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.titulo_loja      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66212#TIT BAIXAR:'                  ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tit_baixar      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66213#TIT RENEGOCIADO:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.tit_renegociado      
                ||chr(10)|| inter_fn_buscar_tag('lb66214#VALOR AVP:'                   ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_avp      
                ||chr(10)|| inter_fn_buscar_tag('lb66215#VALOR COMIS:'                 ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_comis      
                ||chr(10)|| inter_fn_buscar_tag('lb66216#VALOR DESCONTO AUX:'          ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_desconto_aux      
                ||chr(10)|| inter_fn_buscar_tag('lb66217#VALOR DESP COBR:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_desp_cobr      
                ||chr(10)|| inter_fn_buscar_tag('lb66218#VALOR JUROS AUX:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_juros_aux      
                ||chr(10)|| inter_fn_buscar_tag('lb66219#VALOR MOEDA:'                 ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_moeda      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66220#VALOR REMESSA:'               ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_remessa      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66221#VALOR SALDO AUX:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_saldo_aux      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66222#VARIACAO CAMBIAL:'            ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.variacao_cambial      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66223#VENCTO ANTERIOR:'             ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.vencto_anterior      
                ||chr(10)|| inter_fn_buscar_tag('lb66224#VLR PRESENTE:'                ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.vlr_presente      
                -- ||chr(10)|| inter_fn_buscar_tag('lb66225#VL LIQUIDO CARTAO:'           ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.vl_liquido_cartao
                , 4000)
            );
        end if;

        v_old_titulo_banco := :old.nr_titulo_banco;
        v_new_titulo_banco := :new.nr_titulo_banco;
        
        if :old.nr_titulo_banco is null then
            v_old_titulo_banco := ' ';
        end if;
        
        if :new.nr_titulo_banco is null then
            v_new_titulo_banco := ' ';
        end if;
	  
        if updating   and
            (:old.codigo_empresa               <> :new.codigo_empresa               or
            :old.cli_dup_cgc_cli9             <> :new.cli_dup_cgc_cli9             or
            :old.cli_dup_cgc_cli4             <> :new.cli_dup_cgc_cli4             or
            :old.cli_dup_cgc_cli2             <> :new.cli_dup_cgc_cli2             or
            :old.data_venc_duplic             <> :new.data_venc_duplic             or
            :old.data_prorrogacao             <> :new.data_prorrogacao             or
            :old.data_canc_duplic             <> :new.data_canc_duplic             or
            :old.valor_duplicata              <> :new.valor_duplicata              or
            :old.saldo_duplicata              <> :new.saldo_duplicata              or
            :old.situacao_duplic              <> :new.situacao_duplic              or
            :old.cod_canc_duplic              <> :new.cod_canc_duplic              or
            :old.perc_juro_duplic             <> :new.perc_juro_duplic             or
            :old.perc_desc_duplic             <> :new.perc_desc_duplic             or
            :old.portador_duplic              <> :new.portador_duplic              or
            :old.tipo_comissao                <> :new.tipo_comissao                or
            :old.cod_rep_cliente              <> :new.cod_rep_cliente              or
            :old.percentual_comis             <> :new.percentual_comis             or
            :old.perc_comis_crec              <> :new.perc_comis_crec              or
            :old.codigo_administr             <> :new.codigo_administr             or
            :old.comissao_administr           <> :new.comissao_administr           or
            :old.perc_comis_crec_adm          <> :new.perc_comis_crec_adm          or
            :old.duplic_emitida               <> :new.duplic_emitida               or
            :old.cod_local                    <> :new.cod_local                    or
            :old.moeda_titulo                 <> :new.moeda_titulo                 or
            :old.nr_identificacao             <> :new.nr_identificacao             or
            :old.cod_carteira                 <> :new.cod_carteira                 or
            :old.cli9resptit                  <> :new.cli9resptit                  or
            :old.cli4resptit                  <> :new.cli4resptit                  or
            :old.cli2resptit                  <> :new.cli2resptit                  or
            :old.cgc9_endosso                 <> :new.cgc9_endosso                 or
            :old.cgc4_endosso                 <> :new.cgc4_endosso                 or
            :old.cgc2_endosso                 <> :new.cgc2_endosso                 or
            :old.seq_end_cobranca             <> :new.seq_end_cobranca             or
            :old.status_serasa_pefin          <> :new.status_serasa_pefin          or
            :old.motivo_bxa_serasa_pefin      <> :new.motivo_bxa_serasa_pefin      or
            v_old_titulo_banco                <> v_new_titulo_banco                or
            :old.conta_corrente               <> :new.conta_corrente               or
            :old.cod_transacao                <> :new.cod_transacao                or
            :old.atraso_pela_renegociacao     <> :new.atraso_pela_renegociacao     or
            :old.bandeira_cartao              <> :new.bandeira_cartao              or
            :old.base_calc_comis              <> :new.base_calc_comis              or
            :old.bolepix                      <> :new.bolepix                      or
            :old.calc_vl_presente             <> :new.calc_vl_presente             or
            :old.cartao                       <> :new.cartao                       or
            :old.cd_centro_custo              <> :new.cd_centro_custo              or
            :old.chavepix                     <> :new.chavepix                     or
            :old.cli_dup_cgc_cli_o            <> :new.cli_dup_cgc_cli_o            or
            :old.cli_dup_cgc_cli_r            <> :new.cli_dup_cgc_cli_r            or
            :old.cmc7_cheque                  <> :new.cmc7_cheque                  or
            :old.codigo_contabil              <> :new.codigo_contabil              or
            :old.codigo_fatura_loja           <> :new.codigo_fatura_loja           or
            :old.codigo_request               <> :new.codigo_request               or
            :old.cod_barras                   <> :new.cod_barras                   or
            :old.cod_categoria                <> :new.cod_categoria                or
            :old.cod_emp_adt                  <> :new.cod_emp_adt                  or
            :old.cod_forma_pagto              <> :new.cod_forma_pagto              or
            :old.cod_historico                <> :new.cod_historico                or
            :old.cod_processo                 <> :new.cod_processo                 or
            :old.cod_usuario                  <> :new.cod_usuario                  or
            :old.comissao_lancada             <> :new.comissao_lancada             or
            :old.compl_historico              <> :new.compl_historico              or
            :old.cond_pagto_vendor            <> :new.cond_pagto_vendor            or
            :old.controle_cheque              <> :new.controle_cheque              or
            :old.cotacao_moeda                <> :new.cotacao_moeda                or
            :old.data_aceite_banco            <> :new.data_aceite_banco            or
            :old.data_alteracao_boleto        <> :new.data_alteracao_boleto        or
            :old.data_atualizacao_api         <> :new.data_atualizacao_api         or
            :old.data_autorizacao             <> :new.data_autorizacao             or
            :old.data_emissao                 <> :new.data_emissao                 or
            :old.data_prorrogacao_ant         <> :new.data_prorrogacao_ant         or
            :old.data_transf_tit              <> :new.data_transf_tit              or
            :old.data_ult_movim_credito       <> :new.data_ult_movim_credito       or
            :old.data_ult_movim_pagto         <> :new.data_ult_movim_pagto         or
            :old.data_vl_presente             <> :new.data_vl_presente             or
            :old.duplic_impressa              <> :new.duplic_impressa              or
            :old.estabelecimento_opera        <> :new.estabelecimento_opera        or
            :old.estab_centralizador          <> :new.estab_centralizador          or
            :old.executa_trigger              <> :new.executa_trigger              or
            :old.flag_alteracao_boleto        <> :new.flag_alteracao_boleto        or
            :old.flag_marca_cheque            <> :new.flag_marca_cheque            or
            :old.fnd_dt_remessa               <> :new.fnd_dt_remessa               or
            :old.fnd_nr_remessa               <> :new.fnd_nr_remessa               or
            :old.fnd_situacao                 <> :new.fnd_situacao                 or
            :old.fnd_tem_espelho              <> :new.fnd_tem_espelho              or
            :old.id_conc_pagto                <> :new.id_conc_pagto                or
            :old.id_conc_venda                <> :new.id_conc_venda                or
            :old.indice_diario                <> :new.indice_diario                or
            :old.indice_mensal                <> :new.indice_mensal                or
            :old.linha_digitavel              <> :new.linha_digitavel              or
            :old.loja_fat                     <> :new.loja_fat                     or
            :old.lote_recebimento             <> :new.lote_recebimento             or
            :old.mensagem_boleto              <> :new.mensagem_boleto              or
            :old.nr_adiantamento              <> :new.nr_adiantamento              or
            :old.nr_autorizacao_opera         <> :new.nr_autorizacao_opera         or
            :old.nr_cupom                     <> :new.nr_cupom                     or
            :old.nr_mtv_prorrogacao           <> :new.nr_mtv_prorrogacao           or
            :old.nr_remessa_inadimplencia     <> :new.nr_remessa_inadimplencia     or
            :old.nr_solicitacao               <> :new.nr_solicitacao               or
            :old.nr_solicitacao_inadimplencia <> :new.nr_solicitacao_inadimplencia or
            :old.nr_titulo_banco_corresp      <> :new.nr_titulo_banco_corresp      or
            :old.numero_bordero               <> :new.numero_bordero               or
            :old.numero_caixa                 <> :new.numero_caixa                 or
            :old.numero_nsu                   <> :new.numero_nsu                   or
            :old.numero_processo              <> :new.numero_processo              or
            :old.numero_referencia            <> :new.numero_referencia            or
            :old.numero_remessa               <> :new.numero_remessa               or
            :old.numero_sequencia             <> :new.numero_sequencia             or
            :old.numero_titulo                <> :new.numero_titulo                or
            :old.num_contabil                 <> :new.num_contabil                 or
            :old.num_dup_origem               <> :new.num_dup_origem               or
            :old.num_nota_fiscal              <> :new.num_nota_fiscal              or        
            :old.num_renegociacao             <> :new.num_renegociacao             or
            :old.ordem_credito_opera          <> :new.ordem_credito_opera          or
            :old.origem_pedido                <> :new.origem_pedido                or
            :old.pedido_operadora             <> :new.pedido_operadora             or
            :old.pedido_venda                 <> :new.pedido_venda                 or
            :old.port_anterior                <> :new.port_anterior                or
            :old.posicao_ant                  <> :new.posicao_ant                  or
            :old.posicao_duplic               <> :new.posicao_duplic               or
            :old.previsao                     <> :new.previsao                     or
            :old.quantidade                   <> :new.quantidade                   or
            :old.referente_nf                 <> :new.referente_nf                 or
            :old.ren_num_renegocia            <> :new.ren_num_renegocia            or
            :old.ren_sit_renegocia            <> :new.ren_sit_renegocia            or
            :old.responsavel_receb            <> :new.responsavel_receb            or
            :old.resumo_venda                 <> :new.resumo_venda                 or
            :old.scpc_nr_remessa              <> :new.scpc_nr_remessa              or
            :old.scpc_situacao                <> :new.scpc_situacao                or
            :old.selecionado_credito_reneg    <> :new.selecionado_credito_reneg    or
            :old.seq_dup_origem               <> :new.seq_dup_origem               or
            :old.seq_renegociacao             <> :new.seq_renegociacao             or
            :old.serie_nota_fisc              <> :new.serie_nota_fisc              or
            :old.situacao_inadimplencia       <> :new.situacao_inadimplencia       or
            :old.status_credito_opera         <> :new.status_credito_opera         or
            :old.status_nsu                   <> :new.status_nsu                   or
            :old.tecido_peca                  <> :new.tecido_peca                  or
            :old.tid                          <> :new.tid                          or
            :old.tipopix                      <> :new.tipopix                      or
            :old.tipo_titulo_original         <> :new.tipo_titulo_original         or
            :old.tipo_tit_origem              <> :new.tipo_tit_origem              or
            :old.tipo_transacao_opera         <> :new.tipo_transacao_opera         or
            :old.titulo_loja                  <> :new.titulo_loja                  or
            :old.tit_baixar                   <> :new.tit_baixar                   or
            :old.tit_renegociado              <> :new.tit_renegociado              or
            :old.valor_avp                    <> :new.valor_avp                    or
            :old.valor_comis                  <> :new.valor_comis                  or
            :old.valor_desconto_aux           <> :new.valor_desconto_aux           or
            :old.valor_desp_cobr              <> :new.valor_desp_cobr              or
            :old.valor_juros_aux              <> :new.valor_juros_aux              or
            :old.valor_moeda                  <> :new.valor_moeda                  or
            :old.valor_remessa                <> :new.valor_remessa                or
            :old.valor_saldo_aux              <> :new.valor_saldo_aux              or
            :old.variacao_cambial             <> :new.variacao_cambial             or
            :old.vencto_anterior              <> :new.vencto_anterior              or
            :old.vlr_presente                 <> :new.vlr_presente                 or
            :old.vl_liquido_cartao            <> :new.vl_liquido_cartao            
            )
        then
            long_aux := long_aux ||
                '                                                  ' ||
                inter_fn_buscar_tag('lb34836#Tï¿¿TULOS A RECEBER',ws_locale_usuario,ws_usuario_systextil) ||
                chr(10)                                     ||
                chr(10);
            if :old.codigo_empresa      <> :new.codigo_empresa
            then
                long_aux := long_aux  ||
                    inter_fn_buscar_tag('lb07207#EMPRESA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                    || :old.codigo_empresa || ' -> '
                                    || :new.codigo_empresa ||
                                    chr(10);
            end if;

            if :old.cli_dup_cgc_cli9    <> :new.cli_dup_cgc_cli9    or
                :old.cli_dup_cgc_cli4    <> :new.cli_dup_cgc_cli4    or
                :old.cli_dup_cgc_cli2    <> :new.cli_dup_cgc_cli2
            then
                long_aux := long_aux  ||
                    inter_fn_buscar_tag('lb31254#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.cli_dup_cgc_cli9     || '/' ||
                                        :old.cli_dup_cgc_cli4     || '-' ||
                                        :old.cli_dup_cgc_cli2     || ' -> '
                                        || :new.cli_dup_cgc_cli9     || '/' ||
                                        :new.cli_dup_cgc_cli4     || '-' ||
                                        :new.cli_dup_cgc_cli2     ||
                                        chr(10);
            end if;

            if :old.data_venc_duplic    <> :new.data_venc_duplic
            then
                long_aux := long_aux  ||
                    inter_fn_buscar_tag('lb00149#DATA VENCTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || to_char(:old.data_venc_duplic,'DD/MM/YY')     || ' -> '
                                        || to_char(:new.data_venc_duplic,'DD/MM/YY')     ||
                                        chr(10);
            end if;

            if :old.data_prorrogacao    <> :new.data_prorrogacao
            then
                long_aux := long_aux   ||
                    inter_fn_buscar_tag('lb03009#DATA PRORROGAï¿¿ï¿¿O:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || to_char(:old.data_prorrogacao,'DD/MM/YY')     || ' -> '
                                        || to_char(:new.data_prorrogacao,'DD/MM/YY')     ||
                                            chr(10);
            end if;

            if :old.data_canc_duplic    <> :new.data_canc_duplic
            then
                long_aux := long_aux   ||
                    inter_fn_buscar_tag('lb02861#DATA CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || to_char(:old.data_canc_duplic,'DD/MM/YY')     || ' -> '
                                        || to_char(:new.data_canc_duplic,'DD/MM/YY')     ||
                                            chr(10);
            end if;

            if :old.valor_duplicata     <> :new.valor_duplicata
            then
                long_aux := long_aux   ||
                    inter_fn_buscar_tag('lb05237#VALOR DUPLICATA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.valor_duplicata      || ' -> '
                                        || :new.valor_duplicata      ||
                                        chr(10);
            end if;

            if :old.saldo_duplicata     <> :new.saldo_duplicata
            then
                long_aux := long_aux   ||
                    inter_fn_buscar_tag('lb05873#SALDO DUPLICATA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.saldo_duplicata      || ' -> '
                                        || :new.saldo_duplicata      ||
                                        chr(10);
            end if;

            if :old.situacao_duplic     <> :new.situacao_duplic
            then
                long_aux := long_aux   ||
                inter_fn_buscar_tag('lb08122#SITUAï¿¿ï¿¿O :',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.situacao_duplic      || ' -> '
                                        || :new.situacao_duplic      ||
                                        chr(10);
            end if;

            if :old.cod_canc_duplic     <> :new.cod_canc_duplic
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb31282#COD. CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.cod_canc_duplic      || ' -> '
                                            || :new.cod_canc_duplic      ||
                                                chr(10);
            end if;

            if :old.perc_juro_duplic    <> :new.perc_juro_duplic
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb34837#% JUROS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.perc_juro_duplic     || ' -> '
                                            || :new.perc_juro_duplic     ||
                                            chr(10);
            end if;

            if :old.perc_desc_duplic    <> :new.perc_desc_duplic
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb03863#% DESC. DUPLIC:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.perc_desc_duplic     || ' -> '
                                            || :new.perc_desc_duplic     ||
                                            chr(10);
            end if;

            if :old.portador_duplic     <> :new.portador_duplic
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb16134#PORTADOR DUPLICATAS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.portador_duplic      || ' -> '
                                            || :new.portador_duplic      ||
                                            chr(10);
            end if;

            if :old.tipo_comissao       <> :new.tipo_comissao
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb05911#TIPO COMISSï¿¿O:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.tipo_comissao        || ' -> '
                                            || :new.tipo_comissao        ||
                                            chr(10);
            end if;

            if :old.cod_rep_cliente     <> :new.cod_rep_cliente
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb00294#REPRESENTANTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.cod_rep_cliente      || ' -> '
                                            || :new.cod_rep_cliente      ||
                                            chr(10);
            end if;

            if :old.percentual_comis    <> :new.percentual_comis
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb34838#% COMIS. REPRES.:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.percentual_comis     || ' -> '
                                            || :new.percentual_comis     ||
                                            chr(10);
            end if;

            if :old.perc_comis_crec     <> :new.perc_comis_crec
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb34839#% FATU/BAIXA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.perc_comis_crec      || ' -> '
                                            || :new.perc_comis_crec      ||
                                            chr(10);
            end if;

            if :old.codigo_administr    <> :new.codigo_administr
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb01633#ADMINISTRADOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.codigo_administr     || ' -> '
                                            || :new.codigo_administr     ||
                                            chr(10);
            end if;

            if :old.comissao_administr  <> :new.comissao_administr
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb03857#% COMISSï¿¿O (ADMIN):',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.comissao_administr   || ' -> '
                                            || :new.comissao_administr   ||
                                            chr(10);
            end if;

            if :old.perc_comis_crec_adm <> :new.perc_comis_crec_adm
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb34839#% FATU/BAIXA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.perc_comis_crec_adm  || ' -> '
                                            || :new.perc_comis_crec_adm  ||
                                            chr(10);
            end if;

            if :old.duplic_emitida      <> :new.duplic_emitida
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb34840#DUPLIC. EMITIDA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.duplic_emitida       || ' -> '
                                            || :new.duplic_emitida       ||
                                            chr(10);
            end if;

            if :old.cod_local           <> :new.cod_local
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb34841#COD. LOCAL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.cod_local            || ' -> '
                                            || :new.cod_local            ||
                                            chr(10);
            end if;

            if :old.moeda_titulo        <> :new.moeda_titulo
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb13146#MOEDA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.moeda_titulo         || ' -> '
                                            || :new.moeda_titulo         ||
                                            chr(10);
            end if;

            if :old.nr_identificacao       <> :new.nr_identificacao
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb37232#NR IDENTIFICAï¿¿ï¿¿O:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.nr_identificacao         || ' -> '
                                            || :new.nr_identificacao         ||
                                            chr(10);
            end if;

            if :old.cod_carteira        <> :new.cod_carteira
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb37233#Cï¿¿D. CARTEIRA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.cod_carteira         || ' -> '
                                            || :new.cod_carteira         ||
                                            chr(10);
            end if;

            if :old.cli9resptit         <> :new.cli9resptit or
                :old.cli4resptit         <> :new.cli4resptit or
                :old.cli2resptit         <> :new.cli2resptit
            then
                long_aux := long_aux       ||
                inter_fn_buscar_tag('lb05866#RESPONSï¿¿VEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.cli9resptit          || '/' ||
                                            :old.cli4resptit          || '-' ||
                                            :old.cli2resptit          || ' -> '
                                            || :new.cli9resptit          || '/' ||
                                            :new.cli4resptit          || '-' ||
                                            :new.cli2resptit          ||
                                            chr(10);
            end if;

            if :old.cgc9_endosso        <> :new.cgc9_endosso        or
                :old.cgc4_endosso        <> :new.cgc4_endosso        or
                :old.cgc2_endosso        <> :new.cgc2_endosso
            then
                long_aux := long_aux       ||
                    inter_fn_buscar_tag('lb03561#ENDOSSO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.cgc9_endosso         || '/' ||
                                                :old.cgc4_endosso         || '-' ||
                                                :old.cgc2_endosso         || ' -> '
                                            || :new.cgc9_endosso         || '/' ||
                                                :new.cgc4_endosso         || '-' ||
                                                :new.cgc2_endosso         ||
                                            chr(10);
            end if;

            if :old.seq_end_cobranca    <> :new.seq_end_cobranca
            then
                long_aux := long_aux       ||
                    inter_fn_buscar_tag('lb09567#SEQUï¿¿NCIA END. COBRANï¿¿A:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.seq_end_cobranca ||  ' -> '
                                            || :new.seq_end_cobranca;
            end if;

            if :old.status_serasa_pefin <> :new.status_serasa_pefin
            then
                long_aux := long_aux       ||
                    inter_fn_buscar_tag('lb37679#Status SERASA PEFIN:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.status_serasa_pefin ||  ' -> '
                                            || :new.status_serasa_pefin ||
                                            chr(10);
            end if;

            if :old.motivo_bxa_serasa_pefin <> :new.motivo_bxa_serasa_pefin
            then
                long_aux := long_aux       ||
                    inter_fn_buscar_tag('lb37680#MOTIVO BAIXA SERASA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :old.motivo_bxa_serasa_pefin ||  ' -> '
                                            || :new.motivo_bxa_serasa_pefin ||
                                            chr(10);
            end if;

            if v_old_titulo_banco <> v_new_titulo_banco
            then
                long_aux := long_aux       || 'NR. TITULO NO BANCO:' || v_old_titulo_banco ||  ' -> '
                                            || v_new_titulo_banco ||
                                            chr(10);
            end if;

            if :old.conta_corrente <> :new.conta_corrente then
                long_aux := long_aux || inter_fn_buscar_tag('lb02239#CONTA CORRENTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.conta_corrente ||  ' -> ' || :new.conta_corrente || chr(10);
            end if;

            if :old.BOLEPIX <> :new.BOLEPIX then
                long_aux := long_aux || inter_fn_buscar_tag('lb66110#BOLEPIX:'       ,ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.BOLEPIX         || ' -> ' || :new.BOLEPIX ||chr(10);
            end if;

            if :old.cod_transacao <> :new.cod_transacao
            then
                long_aux := long_aux       ||
                    inter_fn_buscar_tag('lb02570#CÓD.TRANSAÇÃO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.cod_transacao ||  ' -> ' || :new.conta_corrente || chr(10);
            end if;

            if :old.ATRASO_PELA_RENEGOCIACAO <> :new.ATRASO_PELA_RENEGOCIACAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66107#ATRASO PELA RENEGOCIACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.ATRASO_PELA_RENEGOCIACAO || ' -> ' || :new.ATRASO_PELA_RENEGOCIACAO ||chr(10);
            end if;
                    
            if :old.BANDEIRA_CARTAO <> :new.BANDEIRA_CARTAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66108#BANDEIRA CARTAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.BANDEIRA_CARTAO || ' -> ' || :new.BANDEIRA_CARTAO ||chr(10);
            end if;
                    
            if :old.BASE_CALC_COMIS <> :new.BASE_CALC_COMIS then
                long_aux := long_aux || inter_fn_buscar_tag('lb66109#BASE CALC COMIS:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.BASE_CALC_COMIS || ' -> ' || :new.BASE_CALC_COMIS ||chr(10);
            end if;
                    
            if :old.CALC_VL_PRESENTE <> :new.CALC_VL_PRESENTE then
                long_aux := long_aux || inter_fn_buscar_tag('lb66111#CALC VL PRESENTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.CALC_VL_PRESENTE || ' -> ' || :new.CALC_VL_PRESENTE ||chr(10);
            end if;
                    
            if :old.CARTAO <> :new.CARTAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66112#CARTAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.CARTAO || ' -> ' || :new.CARTAO ||chr(10);
            end if;
                    
            if :old.CD_CENTRO_CUSTO <> :new.CD_CENTRO_CUSTO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66113#CD CENTRO CUSTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.CD_CENTRO_CUSTO || ' -> ' || :new.CD_CENTRO_CUSTO ||chr(10);
            end if;
                    
            if :old.CHAVEPIX <> :new.CHAVEPIX then
                long_aux := long_aux || inter_fn_buscar_tag('lb66114#CHAVEPIX:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.CHAVEPIX || ' -> ' || :new.CHAVEPIX ||chr(10);
            end if;
                    
            if :old.CLI_DUP_CGC_CLI_O <> :new.CLI_DUP_CGC_CLI_O then
                long_aux := long_aux || inter_fn_buscar_tag('lb66115#CLI DUP CGC CLI O:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.CLI_DUP_CGC_CLI_O || ' -> ' || :new.CLI_DUP_CGC_CLI_O ||chr(10);
            end if;
                    
            if :old.CLI_DUP_CGC_CLI_R <> :new.CLI_DUP_CGC_CLI_R then
                long_aux := long_aux || inter_fn_buscar_tag('lb66116#CLI DUP CGC CLI R:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.CLI_DUP_CGC_CLI_R || ' -> ' || :new.CLI_DUP_CGC_CLI_R ||chr(10);
            end if;
                    
            if :old.CMC7_CHEQUE <> :new.CMC7_CHEQUE then
                long_aux := long_aux || inter_fn_buscar_tag('lb66117#CMC7 CHEQUE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.CMC7_CHEQUE || ' -> ' || :new.CMC7_CHEQUE ||chr(10);
            end if;
                    
            if :old.CODIGO_CONTABIL <> :new.CODIGO_CONTABIL then
                long_aux := long_aux || inter_fn_buscar_tag('lb66118#CODIGO CONTABIL:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.CODIGO_CONTABIL || ' -> ' || :new.CODIGO_CONTABIL ||chr(10);
            end if;
                    
            if :old.CODIGO_FATURA_LOJA <> :new.CODIGO_FATURA_LOJA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66119#CODIGO FATURA LOJA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.CODIGO_FATURA_LOJA || ' -> ' || :new.CODIGO_FATURA_LOJA ||chr(10);
            end if;
                    
            if :old.CODIGO_REQUEST <> :new.CODIGO_REQUEST then
                long_aux := long_aux || inter_fn_buscar_tag('lb66120#CODIGO REQUEST:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.CODIGO_REQUEST || ' -> ' || :new.CODIGO_REQUEST ||chr(10);
            end if;
                    
            if :old.COD_BARRAS <> :new.COD_BARRAS then
                long_aux := long_aux || inter_fn_buscar_tag('lb66121#COD BARRAS:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.COD_BARRAS || ' -> ' || :new.COD_BARRAS ||chr(10);
            end if;
            
            if :old.COD_CATEGORIA <> :new.COD_CATEGORIA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66122#COD CATEGORIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.COD_CATEGORIA || ' -> ' || :new.COD_CATEGORIA ||chr(10);
            end if;
                    
            if :old.COD_EMP_ADT <> :new.COD_EMP_ADT then
                long_aux := long_aux || inter_fn_buscar_tag('lb66123#COD EMP ADT:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.COD_EMP_ADT || ' -> ' || :new.COD_EMP_ADT ||chr(10);
            end if;
                    
            if :old.COD_FORMA_PAGTO <> :new.COD_FORMA_PAGTO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66124#COD FORMA PAGTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.COD_FORMA_PAGTO || ' -> ' || :new.COD_FORMA_PAGTO ||chr(10);
            end if;
                    
            if :old.COD_HISTORICO <> :new.COD_HISTORICO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66125#COD HISTORICO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.COD_HISTORICO || ' -> ' || :new.COD_HISTORICO ||chr(10);
            end if;
                    
            if :old.COD_PROCESSO <> :new.COD_PROCESSO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66126#COD PROCESSO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.COD_PROCESSO || ' -> ' || :new.COD_PROCESSO ||chr(10);
            end if;
            
            if :old.COD_USUARIO <> :new.COD_USUARIO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66127#COD USUARIO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.COD_USUARIO || ' -> ' || :new.COD_USUARIO ||chr(10);
            end if;
                    
            if :old.COMISSAO_LANCADA <> :new.COMISSAO_LANCADA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66128#COMISSAO LANCADA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.COMISSAO_LANCADA || ' -> ' || :new.COMISSAO_LANCADA ||chr(10);
            end if;
            
            if :old.COMPL_HISTORICO <> :new.COMPL_HISTORICO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66129#COMPL HISTORICO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.COMPL_HISTORICO || ' -> ' || :new.COMPL_HISTORICO ||chr(10);
            end if;
            
            if :old.COND_PAGTO_VENDOR <> :new.COND_PAGTO_VENDOR then
                long_aux := long_aux || inter_fn_buscar_tag('lb66130#COND PAGTO VENDOR:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.COND_PAGTO_VENDOR || ' -> ' || :new.COND_PAGTO_VENDOR ||chr(10);
            end if;
                    
            if :old.CONTROLE_CHEQUE <> :new.CONTROLE_CHEQUE then
                long_aux := long_aux || inter_fn_buscar_tag('lb66131#CONTROLE CHEQUE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.CONTROLE_CHEQUE || ' -> ' || :new.CONTROLE_CHEQUE ||chr(10);
            end if;
            
            if :old.COTACAO_MOEDA <> :new.COTACAO_MOEDA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66132#COTACAO MOEDA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.COTACAO_MOEDA || ' -> ' || :new.COTACAO_MOEDA ||chr(10);
            end if;
                    
            if :old.DATA_ACEITE_BANCO <> :new.DATA_ACEITE_BANCO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66133#DATA ACEITE BANCO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.DATA_ACEITE_BANCO || ' -> ' || :new.DATA_ACEITE_BANCO ||chr(10);
            end if;
                
            if :old.DATA_ALTERACAO_BOLETO <> :new.DATA_ALTERACAO_BOLETO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66134#DATA ALTERACAO BOLETO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.DATA_ALTERACAO_BOLETO || ' -> ' || :new.DATA_ALTERACAO_BOLETO ||chr(10);
            end if;
                    
            if :old.DATA_ATUALIZACAO_API <> :new.DATA_ATUALIZACAO_API then
                long_aux := long_aux || inter_fn_buscar_tag('lb66135#DATA ATUALIZACAO API:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.DATA_ATUALIZACAO_API || ' -> ' || :new.DATA_ATUALIZACAO_API ||chr(10);
            end if;
                    
            if :old.DATA_AUTORIZACAO <> :new.DATA_AUTORIZACAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66136#DATA AUTORIZACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.DATA_AUTORIZACAO || ' -> ' || :new.DATA_AUTORIZACAO ||chr(10);
            end if;
                    
            if :old.DATA_EMISSAO <> :new.DATA_EMISSAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66137#DATA EMISSAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.DATA_EMISSAO || ' -> ' || :new.DATA_EMISSAO ||chr(10);
            end if;
                    
            if :old.DATA_PRORROGACAO_ANT <> :new.DATA_PRORROGACAO_ANT then
                long_aux := long_aux || inter_fn_buscar_tag('lb66138#DATA PRORROGACAO ANT:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.DATA_PRORROGACAO_ANT || ' -> ' || :new.DATA_PRORROGACAO_ANT ||chr(10);
            end if;
                    
            if :old.DATA_TRANSF_TIT <> :new.DATA_TRANSF_TIT then
                long_aux := long_aux || inter_fn_buscar_tag('lb66139#DATA TRANSF TIT:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.DATA_TRANSF_TIT || ' -> ' || :new.DATA_TRANSF_TIT ||chr(10);
            end if;
            
            if :old.DATA_ULT_MOVIM_CREDITO <> :new.DATA_ULT_MOVIM_CREDITO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66140#DATA ULT MOVIM CREDITO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.DATA_ULT_MOVIM_CREDITO || ' -> ' || :new.DATA_ULT_MOVIM_CREDITO ||chr(10);
            end if;
                    
            if :old.DATA_ULT_MOVIM_PAGTO <> :new.DATA_ULT_MOVIM_PAGTO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66141#DATA ULT MOVIM PAGTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.DATA_ULT_MOVIM_PAGTO || ' -> ' || :new.DATA_ULT_MOVIM_PAGTO ||chr(10);
            end if;
                    
            if :old.DATA_VL_PRESENTE <> :new.DATA_VL_PRESENTE then
                long_aux := long_aux || inter_fn_buscar_tag('lb66142#DATA VL PRESENTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.DATA_VL_PRESENTE || ' -> ' || :new.DATA_VL_PRESENTE ||chr(10);
            end if;
                    
            if :old.DUPLIC_IMPRESSA <> :new.DUPLIC_IMPRESSA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66143#DUPLIC IMPRESSA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.DUPLIC_IMPRESSA || ' -> ' || :new.DUPLIC_IMPRESSA ||chr(10);
            end if;
                    
            if :old.ESTABELECIMENTO_OPERA <> :new.ESTABELECIMENTO_OPERA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66144#ESTABELECIMENTO OPERA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.ESTABELECIMENTO_OPERA || ' -> ' || :new.ESTABELECIMENTO_OPERA ||chr(10);
            end if;
                    
            if :old.ESTAB_CENTRALIZADOR <> :new.ESTAB_CENTRALIZADOR then
                long_aux := long_aux || inter_fn_buscar_tag('lb66145#ESTAB CENTRALIZADOR:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.ESTAB_CENTRALIZADOR || ' -> ' || :new.ESTAB_CENTRALIZADOR ||chr(10);
            end if;
                    
            if :old.EXECUTA_TRIGGER <> :new.EXECUTA_TRIGGER then
                long_aux := long_aux || inter_fn_buscar_tag('lb66146#EXECUTA TRIGGER:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.EXECUTA_TRIGGER || ' -> ' || :new.EXECUTA_TRIGGER ||chr(10);
            end if;
                    
            if :old.FLAG_ALTERACAO_BOLETO <> :new.FLAG_ALTERACAO_BOLETO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66147#FLAG ALTERACAO BOLETO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.FLAG_ALTERACAO_BOLETO || ' -> ' || :new.FLAG_ALTERACAO_BOLETO ||chr(10);
            end if;
                    
            if :old.FLAG_MARCA_CHEQUE <> :new.FLAG_MARCA_CHEQUE then
                long_aux := long_aux || inter_fn_buscar_tag('lb66148#FLAG MARCA CHEQUE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.FLAG_MARCA_CHEQUE || ' -> ' || :new.FLAG_MARCA_CHEQUE ||chr(10);
            end if;
                
            if :old.FND_DT_REMESSA <> :new.FND_DT_REMESSA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66149#FND DT REMESSA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.FND_DT_REMESSA || ' -> ' || :new.FND_DT_REMESSA ||chr(10);
            end if;
                    
            if :old.FND_NR_REMESSA <> :new.FND_NR_REMESSA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66150#FND NR REMESSA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.FND_NR_REMESSA || ' -> ' || :new.FND_NR_REMESSA ||chr(10);
            end if;
                    
            if :old.FND_SITUACAO <> :new.FND_SITUACAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66151#FND SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.FND_SITUACAO || ' -> ' || :new.FND_SITUACAO ||chr(10);
            end if;
                    
            if :old.FND_TEM_ESPELHO <> :new.FND_TEM_ESPELHO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66152#FND TEM ESPELHO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.FND_TEM_ESPELHO || ' -> ' || :new.FND_TEM_ESPELHO ||chr(10);
            end if;
                    
            if :old.ID_CONC_PAGTO <> :new.ID_CONC_PAGTO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66153#ID CONC PAGTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.ID_CONC_PAGTO || ' -> ' || :new.ID_CONC_PAGTO ||chr(10);
            end if;
                    
            if :old.ID_CONC_VENDA <> :new.ID_CONC_VENDA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66154#ID CONC VENDA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.ID_CONC_VENDA || ' -> ' || :new.ID_CONC_VENDA ||chr(10);
            end if;
                    
            if :old.INDICE_DIARIO <> :new.INDICE_DIARIO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66155#INDICE DIARIO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.INDICE_DIARIO || ' -> ' || :new.INDICE_DIARIO ||chr(10);
            end if;
                    
            if :old.INDICE_MENSAL <> :new.INDICE_MENSAL then
                long_aux := long_aux || inter_fn_buscar_tag('lb66156#INDICE MENSAL:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.INDICE_MENSAL || ' -> ' || :new.INDICE_MENSAL ||chr(10);
            end if;
                    
            if :old.LINHA_DIGITAVEL <> :new.LINHA_DIGITAVEL then
                long_aux := long_aux || inter_fn_buscar_tag('lb66157#LINHA DIGITAVEL:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.LINHA_DIGITAVEL || ' -> ' || :new.LINHA_DIGITAVEL ||chr(10);
            end if;
                    
            if :old.LOJA_FAT <> :new.LOJA_FAT then
                long_aux := long_aux || inter_fn_buscar_tag('lb66158#LOJA FAT:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.LOJA_FAT || ' -> ' || :new.LOJA_FAT ||chr(10);
            end if;
            
            if :old.LOTE_RECEBIMENTO <> :new.LOTE_RECEBIMENTO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66159#LOTE RECEBIMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.LOTE_RECEBIMENTO || ' -> ' || :new.LOTE_RECEBIMENTO ||chr(10);
            end if;
                    
            if :old.MENSAGEM_BOLETO <> :new.MENSAGEM_BOLETO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66160#MENSAGEM BOLETO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.MENSAGEM_BOLETO || ' -> ' || :new.MENSAGEM_BOLETO ||chr(10);
            end if;
                    
            if :old.NR_ADIANTAMENTO <> :new.NR_ADIANTAMENTO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66161#NR ADIANTAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NR_ADIANTAMENTO || ' -> ' || :new.NR_ADIANTAMENTO ||chr(10);
            end if;
                    
            if :old.NR_AUTORIZACAO_OPERA <> :new.NR_AUTORIZACAO_OPERA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66162#NR AUTORIZACAO OPERA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NR_AUTORIZACAO_OPERA || ' -> ' || :new.NR_AUTORIZACAO_OPERA ||chr(10);
            end if;
                    
            if :old.NR_CUPOM <> :new.NR_CUPOM then
                long_aux := long_aux || inter_fn_buscar_tag('lb66163#NR CUPOM:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NR_CUPOM || ' -> ' || :new.NR_CUPOM ||chr(10);
            end if;
                
            if :old.NR_MTV_PRORROGACAO <> :new.NR_MTV_PRORROGACAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66164#NR MTV PRORROGACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NR_MTV_PRORROGACAO || ' -> ' || :new.NR_MTV_PRORROGACAO ||chr(10);
            end if;
                    
            if :old.NR_REMESSA_INADIMPLENCIA <> :new.NR_REMESSA_INADIMPLENCIA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66165#NR REMESSA INADIMPLENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NR_REMESSA_INADIMPLENCIA || ' -> ' || :new.NR_REMESSA_INADIMPLENCIA ||chr(10);
            end if;

            if :old.NR_SOLICITACAO <> :new.NR_SOLICITACAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66166#NR SOLICITACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NR_SOLICITACAO || ' -> ' || :new.NR_SOLICITACAO ||chr(10);
            end if;
                    
            if :old.NR_SOLICITACAO_INADIMPLENCIA <> :new.NR_SOLICITACAO_INADIMPLENCIA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66167#NR SOLICITACAO INADIMPLENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NR_SOLICITACAO_INADIMPLENCIA || ' -> ' || :new.NR_SOLICITACAO_INADIMPLENCIA ||chr(10);
            end if;
                    
            if :old.NR_TITULO_BANCO_CORRESP <> :new.NR_TITULO_BANCO_CORRESP then
                long_aux := long_aux || inter_fn_buscar_tag('lb66168#NR TITULO BANCO CORRESP:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NR_TITULO_BANCO_CORRESP || ' -> ' || :new.NR_TITULO_BANCO_CORRESP ||chr(10);
            end if;
                    
            if :old.NUMERO_BORDERO <> :new.NUMERO_BORDERO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66169#NUMERO BORDERO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NUMERO_BORDERO || ' -> ' || :new.NUMERO_BORDERO ||chr(10);
            end if;

            if :old.NUMERO_CAIXA <> :new.NUMERO_CAIXA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66170#NUMERO CAIXA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NUMERO_CAIXA || ' -> ' || :new.NUMERO_CAIXA ||chr(10);
            end if;
                    
            if :old.NUMERO_NSU <> :new.NUMERO_NSU then
                long_aux := long_aux || inter_fn_buscar_tag('lb66171#NUMERO NSU:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NUMERO_NSU || ' -> ' || :new.NUMERO_NSU||chr(10);
            end if;
            
            if :old.NUMERO_PROCESSO <> :new.NUMERO_PROCESSO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66172#NUMERO PROCESSO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NUMERO_PROCESSO || ' -> ' || :new.NUMERO_PROCESSO||chr(10);
            end if;
                    
            if :old.NUMERO_REFERENCIA <> :new.NUMERO_REFERENCIA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66173#NUMERO REFERENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NUMERO_REFERENCIA || ' -> ' || :new.NUMERO_REFERENCIA||chr(10);
            end if;
                    
            if :old.NUMERO_REMESSA <> :new.NUMERO_REMESSA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66174#NUMERO REMESSA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NUMERO_REMESSA || ' -> ' || :new.NUMERO_REMESSA||chr(10);
            end if;
                    
            if :old.NUMERO_SEQUENCIA <> :new.NUMERO_SEQUENCIA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66175#NUMERO SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NUMERO_SEQUENCIA || ' -> ' || :new.NUMERO_SEQUENCIA||chr(10);
            end if;
                    
            if :old.NUMERO_TITULO <> :new.NUMERO_TITULO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66176#NUMERO TITULO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NUMERO_TITULO || ' -> ' || :new.NUMERO_TITULO||chr(10);
            end if;
                    
            if :old.NUM_CONTABIL <> :new.NUM_CONTABIL then
                long_aux := long_aux || inter_fn_buscar_tag('lb66177#NUM CONTABIL:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NUM_CONTABIL || ' -> ' || :new.NUM_CONTABIL||chr(10);
            end if;
                    
            if :old.NUM_DUP_ORIGEM <> :new.NUM_DUP_ORIGEM then
                long_aux := long_aux || inter_fn_buscar_tag('lb66178#NUM DUP ORIGEM:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NUM_DUP_ORIGEM || ' -> ' || :new.NUM_DUP_ORIGEM||chr(10);
            end if;
                    
            if :old.NUM_NOTA_FISCAL <> :new.NUM_NOTA_FISCAL then
                long_aux := long_aux || inter_fn_buscar_tag('lb66179#NUM NOTA FISCAL:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NUM_NOTA_FISCAL || ' -> ' || :new.NUM_NOTA_FISCAL||chr(10);
            end if;

            if :old.NUM_RENEGOCIACAO <> :new.NUM_RENEGOCIACAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66180#NUM RENEGOCIACAO :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.NUM_RENEGOCIACAO || ' -> ' || :new.NUM_RENEGOCIACAO ||chr(10);
            end if;

            -- if :old.OBSERVACAO <> :new.OBSERVACAO then
            --     long_aux := long_aux || inter_fn_buscar_tag('lb66181#OBSERVACAO :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.OBSERVACAO || ' -> ' || :new.OBSERVACAO ||chr(10);
            -- end if;

            if :old.ORDEM_CREDITO_OPERA <> :new.ORDEM_CREDITO_OPERA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66182#ORDEM CREDITO OPERA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.ORDEM_CREDITO_OPERA || ' -> ' || :new.ORDEM_CREDITO_OPERA ||chr(10);
            end if;
                    
            if :old.ORIGEM_PEDIDO <> :new.ORIGEM_PEDIDO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66183#ORIGEM PEDIDO :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.ORIGEM_PEDIDO || ' -> ' || :new.ORIGEM_PEDIDO ||chr(10);
            end if;
                    
            if :old.PEDIDO_OPERADORA <> :new.PEDIDO_OPERADORA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66184#PEDIDO OPERADORA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.PEDIDO_OPERADORA || ' -> ' || :new.PEDIDO_OPERADORA ||chr(10);
            end if;
        
            if :old.PEDIDO_VENDA <> :new.PEDIDO_VENDA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66185#PEDIDO VENDA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.PEDIDO_VENDA || ' -> ' || :new.PEDIDO_VENDA ||chr(10);
            end if;
                    
            if :old.PORT_ANTERIOR <> :new.PORT_ANTERIOR then
                long_aux := long_aux || inter_fn_buscar_tag('lb66186#PORT ANTERIOR :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.PORT_ANTERIOR || ' -> ' || :new.PORT_ANTERIOR ||chr(10);
            end if;
                    
            if :old.POSICAO_ANT <> :new.POSICAO_ANT then
                long_aux := long_aux || inter_fn_buscar_tag('lb66187#POSICAO ANT :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.POSICAO_ANT || ' -> ' || :new.POSICAO_ANT ||chr(10);
            end if;
                    
            if :old.POSICAO_DUPLIC <> :new.POSICAO_DUPLIC then
                long_aux := long_aux || inter_fn_buscar_tag('lb66188#POSICAO DUPLIC :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.POSICAO_DUPLIC || ' -> ' || :new.POSICAO_DUPLIC ||chr(10);
            end if;
                    
            if :old.PREVISAO <> :new.PREVISAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66189#PREVISAO :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.PREVISAO || ' -> ' || :new.PREVISAO ||chr(10);
            end if;
                    
            if :old.QUANTIDADE <> :new.QUANTIDADE then
                long_aux := long_aux || inter_fn_buscar_tag('lb66190#QUANTIDADE :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.QUANTIDADE || ' -> ' || :new.QUANTIDADE ||chr(10);
            end if;
                    
            if :old.REFERENTE_NF <> :new.REFERENTE_NF then
                long_aux := long_aux || inter_fn_buscar_tag('lb66191#REFERENTE NF :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.REFERENTE_NF || ' -> ' || :new.REFERENTE_NF ||chr(10);
            end if;
                    
            if :old.REN_NUM_RENEGOCIA <> :new.REN_NUM_RENEGOCIA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66192#REN NUM RENEGOCIA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.REN_NUM_RENEGOCIA || ' -> ' || :new.REN_NUM_RENEGOCIA ||chr(10);
            end if;
                    
            if :old.REN_SIT_RENEGOCIA <> :new.REN_SIT_RENEGOCIA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66193#REN SIT RENEGOCIA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.REN_SIT_RENEGOCIA || ' -> ' || :new.REN_SIT_RENEGOCIA ||chr(10);
            end if;
            
            if :old.RESPONSAVEL_RECEB <> :new.RESPONSAVEL_RECEB then
                long_aux := long_aux || inter_fn_buscar_tag('lb66194#RESPONSAVEL RECEB :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.RESPONSAVEL_RECEB || ' -> ' || :new.RESPONSAVEL_RECEB ||chr(10);
            end if;
                    
            if :old.RESUMO_VENDA <> :new.RESUMO_VENDA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66195#RESUMO VENDA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.RESUMO_VENDA || ' -> ' || :new.RESUMO_VENDA ||chr(10);
            end if;
                    
            if :old.SCPC_NR_REMESSA <> :new.SCPC_NR_REMESSA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66196#SCPC NR REMESSA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.SCPC_NR_REMESSA || ' -> ' || :new.SCPC_NR_REMESSA ||chr(10);
            end if;
                    
            if :old.SCPC_SITUACAO <> :new.SCPC_SITUACAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66197#SCPC SITUACAO :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.SCPC_SITUACAO || ' -> ' || :new.SCPC_SITUACAO ||chr(10);
            end if;
                    
            if :old.SELECIONADO_CREDITO_RENEG <> :new.SELECIONADO_CREDITO_RENEG then
                long_aux := long_aux || inter_fn_buscar_tag('lb66198#SELECIONADO CREDITO RENEG :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.SELECIONADO_CREDITO_RENEG || ' -> ' || :new.SELECIONADO_CREDITO_RENEG ||chr(10);
            end if;
                    
            if :old.SEQ_DUP_ORIGEM <> :new.SEQ_DUP_ORIGEM then
                long_aux := long_aux || inter_fn_buscar_tag('lb66199#SEQ DUP ORIGEM :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.SEQ_DUP_ORIGEM || ' -> ' || :new.SEQ_DUP_ORIGEM ||chr(10);
            end if;
                    
            if :old.SEQ_RENEGOCIACAO <> :new.SEQ_RENEGOCIACAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66200#SEQ RENEGOCIACAO :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.SEQ_RENEGOCIACAO || ' -> ' || :new.SEQ_RENEGOCIACAO ||chr(10);
            end if;
            
            if :old.SERIE_NOTA_FISC <> :new.SERIE_NOTA_FISC then
                long_aux := long_aux || inter_fn_buscar_tag('lb66201#SERIE NOTA FISC :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.SERIE_NOTA_FISC || ' -> ' || :new.SERIE_NOTA_FISC ||chr(10);
            end if;
                    
            if :old.SITUACAO_INADIMPLENCIA <> :new.SITUACAO_INADIMPLENCIA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66202#SITUACAO INADIMPLENCIA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.SITUACAO_INADIMPLENCIA || ' -> ' || :new.SITUACAO_INADIMPLENCIA ||chr(10);
            end if;
                    
            if :old.STATUS_CREDITO_OPERA <> :new.STATUS_CREDITO_OPERA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66203#STATUS CREDITO OPERA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.STATUS_CREDITO_OPERA || ' -> ' || :new.STATUS_CREDITO_OPERA ||chr(10);
            end if;
                    
            if :old.STATUS_NSU <> :new.STATUS_NSU then
                long_aux := long_aux || inter_fn_buscar_tag('lb66204#STATUS NSU :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.STATUS_NSU || ' -> ' || :new.STATUS_NSU ||chr(10);
            end if;
                    
            if :old.TECIDO_PECA <> :new.TECIDO_PECA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66205#TECIDO PECA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.TECIDO_PECA || ' -> ' || :new.TECIDO_PECA ||chr(10);
            end if;
                    
            if :old.TID <> :new.TID then
                long_aux := long_aux || inter_fn_buscar_tag('lb66206#TID :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.TID || ' -> ' || :new.TID ||chr(10);
            end if;
                    
            if :old.TIPOPIX <> :new.TIPOPIX then
                long_aux := long_aux || inter_fn_buscar_tag('lb66207#TIPOPIX :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.TIPOPIX || ' -> ' || :new.TIPOPIX ||chr(10);
            end if;
                    
            if :old.TIPO_TITULO_ORIGINAL <> :new.TIPO_TITULO_ORIGINAL then
                long_aux := long_aux || inter_fn_buscar_tag('lb66208#TIPO TITULO ORIGINAL :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.TIPO_TITULO_ORIGINAL || ' -> ' || :new.TIPO_TITULO_ORIGINAL ||chr(10);
            end if;
                    
            if :old.TIPO_TIT_ORIGEM <> :new.TIPO_TIT_ORIGEM then
                long_aux := long_aux || inter_fn_buscar_tag('lb66209#TIPO TIT ORIGEM :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.TIPO_TIT_ORIGEM || ' -> ' || :new.TIPO_TIT_ORIGEM ||chr(10);
            end if;
                
            if :old.TIPO_TRANSACAO_OPERA <> :new.TIPO_TRANSACAO_OPERA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66210#TIPO TRANSACAO OPERA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.TIPO_TRANSACAO_OPERA || ' -> ' || :new.TIPO_TRANSACAO_OPERA ||chr(10);
            end if;
                    
            if :old.TITULO_LOJA <> :new.TITULO_LOJA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66211#TITULO LOJA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.TITULO_LOJA || ' -> ' || :new.TITULO_LOJA ||chr(10);
            end if;
                    
            if :old.TIT_BAIXAR <> :new.TIT_BAIXAR then
                long_aux := long_aux || inter_fn_buscar_tag('lb66212#TIT BAIXAR :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.TIT_BAIXAR || ' -> ' || :new.TIT_BAIXAR ||chr(10);
            end if;
                    
            if :old.TIT_RENEGOCIADO <> :new.TIT_RENEGOCIADO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66213#TIT RENEGOCIADO :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.TIT_RENEGOCIADO || ' -> ' || :new.TIT_RENEGOCIADO ||chr(10);
            end if;
                    
            if :old.VALOR_AVP <> :new.VALOR_AVP then
                long_aux := long_aux || inter_fn_buscar_tag('lb66214#VALOR AVP :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.VALOR_AVP || ' -> ' || :new.VALOR_AVP ||chr(10);
            end if;
                    
            if :old.VALOR_COMIS <> :new.VALOR_COMIS then
                long_aux := long_aux || inter_fn_buscar_tag('lb66215#VALOR COMIS :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.VALOR_COMIS || ' -> ' || :new.VALOR_COMIS ||chr(10);
            end if;
                    
            if :old.VALOR_DESCONTO_AUX <> :new.VALOR_DESCONTO_AUX then
                long_aux := long_aux || inter_fn_buscar_tag('lb66216#VALOR DESCONTO AUX :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.VALOR_DESCONTO_AUX || ' -> ' || :new.VALOR_DESCONTO_AUX ||chr(10);
            end if;
                    
            if :old.VALOR_DESP_COBR <> :new.VALOR_DESP_COBR then
                long_aux := long_aux || inter_fn_buscar_tag('lb66217#VALOR DESP COBR :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.VALOR_DESP_COBR || ' -> ' || :new.VALOR_DESP_COBR ||chr(10);
            end if;
                
            if :old.VALOR_JUROS_AUX <> :new.VALOR_JUROS_AUX then
                long_aux := long_aux || inter_fn_buscar_tag('lb66218#VALOR JUROS AUX :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.VALOR_JUROS_AUX || ' -> ' || :new.VALOR_JUROS_AUX ||chr(10);
            end if;
                    
            if :old.VALOR_MOEDA <> :new.VALOR_MOEDA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66219#VALOR MOEDA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.VALOR_MOEDA || ' -> ' || :new.VALOR_MOEDA ||chr(10);
            end if;
                    
            if :old.VALOR_REMESSA <> :new.VALOR_REMESSA then
                long_aux := long_aux || inter_fn_buscar_tag('lb66220#VALOR REMESSA :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.VALOR_REMESSA || ' -> ' || :new.VALOR_REMESSA ||chr(10);
            end if;
            
            if :old.VALOR_SALDO_AUX <> :new.VALOR_SALDO_AUX then
                long_aux := long_aux || inter_fn_buscar_tag('lb66221#VALOR SALDO AUX :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.VALOR_SALDO_AUX || ' -> ' || :new.VALOR_SALDO_AUX ||chr(10);
            end if;
                    
            if :old.VARIACAO_CAMBIAL <> :new.VARIACAO_CAMBIAL then
                long_aux := long_aux || inter_fn_buscar_tag('lb66222#VARIACAO CAMBIAL :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.VARIACAO_CAMBIAL || ' -> ' || :new.VARIACAO_CAMBIAL ||chr(10);
            end if;
                    
            if :old.VENCTO_ANTERIOR <> :new.VENCTO_ANTERIOR then
                long_aux := long_aux || inter_fn_buscar_tag('lb66223#VENCTO ANTERIOR :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.VENCTO_ANTERIOR || ' -> ' || :new.VENCTO_ANTERIOR ||chr(10);
            end if;
                    
            if :old.VLR_PRESENTE <> :new.VLR_PRESENTE then
                long_aux := long_aux || inter_fn_buscar_tag('lb66224#VLR PRESENTE :',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.VLR_PRESENTE || ' -> ' || :new.VLR_PRESENTE ||chr(10);
            end if;
                    
            if :old.VL_LIQUIDO_CARTAO <> :new.VL_LIQUIDO_CARTAO then
                long_aux := long_aux || inter_fn_buscar_tag('lb66225#VL LIQUIDO CARTAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.VL_LIQUIDO_CARTAO || ' -> ' || :new.VL_LIQUIDO_CARTAO ||chr(10);
            end if;

            insert into hist_100(
                programa        
                ,tabela
                ,operacao       
                ,data_ocorr
                ,usuario_rede     
                ,maquina_rede
                ,aplicacao      
                ,num01
                ,num02          
                ,num03
                ,num04           
                ,num05
                ,str01           
                ,num06
                ,long01
            )VALUES(
                ws_nome_programa    
                ,'FATU_070'
                ,'A'              
                ,sysdate
                ,ws_usuario_rede    
                ,ws_maquina_rede
                ,ws_aplicativo        
                ,:new.num_duplicata
                ,:new.cli_dup_cgc_cli9 
                ,:new.cli_dup_cgc_cli4
                ,:new.cli_dup_cgc_cli2
                ,:new.tipo_titulo
                ,:new.seq_duplicatas  
                ,:new.codigo_empresa
                ,long_aux
            );

        end if;

        if deleting then
            INSERT INTO hist_100(
                programa          
                ,tabela
                ,operacao        
                ,data_ocorr
                ,usuario_rede    
                ,maquina_rede
                ,aplicacao       
                ,num01
                ,num02          
                ,num03
                ,num04            
                ,num05
                ,str01            
                ,num06
                ,long01
            )VALUES(
                ws_nome_programa   
                ,'FATU_070'
                ,'D'                   
                ,sysdate
                ,ws_usuario_rede      
                ,ws_maquina_rede
                ,ws_aplicativo       
                ,:old.num_duplicata
                ,:old.cli_dup_cgc_cli9
                ,:old.cli_dup_cgc_cli4
                ,:old.cli_dup_cgc_cli2
                ,:old.tipo_titulo
                ,:old.seq_duplicatas  
                ,:old.codigo_empresa
                ,'                                                  ' || inter_fn_buscar_tag('lb34836#Tï¿¿TULOS A RECEBER',ws_locale_usuario,ws_usuario_systextil) || chr(10)
            );
        end if;

   end if;

end inter_tr_fatu_070_log;
