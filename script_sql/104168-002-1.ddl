CREATE TABLE PEDI_013 (
CGC9 NUMBER(9) NOT NULL ,
CGC4 NUMBER(4) NOT NULL ,
CGC2 NUMBER(2) NOT NULL ,
TOTAL_LIMITE NUMBER(14,2) DEFAULT 0.0,
SALDO_LIMITE NUMBER(14,2) DEFAULT 0.0,
DIAS_ATRASO NUMBER(5) DEFAULT 0,
STATUS_CARTAO VARCHAR2(1) DEFAULT '',
STATUS_CLIENTE VARCHAR2(1) DEFAULT '',
DATA_RETORNO DATE,
DATA_REJEICAO DATE,
MOTIVO_REJEICAO VARCHAR2(100) DEFAULT ''
);
ALTER TABLE PEDI_013
ADD CONSTRAINT PK_PEDI_013 PRIMARY KEY (CGC9, CGC4, CGC2);
COMMENT ON COLUMN PEDI_013.TOTAL_LIMITE IS 'Total do limite de cr�dito do cliente na suppliercard';
COMMENT ON COLUMN PEDI_013.SALDO_LIMITE IS 'Saldo do limite de cr�dito do cliente na suppliercard';
COMMENT ON COLUMN PEDI_013.DIAS_ATRASO IS 'Dias atraso do cliente na suppliercard';
COMMENT ON COLUMN PEDI_013.STATUS_CARTAO IS '1-Atendida, 2-Encaminhada, 3-Rejeitada';
COMMENT ON COLUMN PEDI_013.STATUS_CLIENTE IS '0-Ativo, 1-Bloqueado por Cr�dito, 2-Bloqueado por Atraso, 3-Cancelado';
COMMENT ON COLUMN PEDI_013.DATA_RETORNO IS 'Data de retorno da suppliercard';
COMMENT ON COLUMN PEDI_013.DATA_REJEICAO IS 'Data da rejei��o pela suppliercard';
COMMENT ON COLUMN PEDI_013.MOTIVO_REJEICAO IS 'Motivo da rejei��o pela suppliercard';
