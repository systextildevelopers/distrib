create or replace package body ST_PCK_PLANO_CONTAS is

    FUNCTION get( p_cod_plano_cta NUMBER, p_msg_erro in out varchar2) return t_cont_530 
    as 
        r_bulk t_cont_530;
	BEGIN
        begin
            select * 
            BULK COLLECT INTO r_bulk
            from cont_530 
            where cod_plano_cta = p_cod_plano_cta;
        exception when others then
            p_msg_erro := 'Não foi possível buscar o plano de conta!. cod_plano_cta:' || p_cod_plano_cta;
        end;

		RETURN r_bulk;
	END;

    function exists_plano_conta(p_cod_plano_cta number) return boolean 
    as 
        tmpInt number;
	begin
        
		select NVL(MIN(1),0)
        into tmpInt
        from cont_530
        where cod_plano_cta = p_cod_plano_cta
        and rownum = 1;

        return tmpInt = 1;
    end;

    procedure insere_plano_contas (	p_cod_plano_cta number, p_descricao varchar2, p_mascara varchar2, p_msg_erro in out varchar2) AS
    begin
        
        begin
			insert into cont_530 (cod_plano_cta, descricao, mascara) 
            values ( p_cod_plano_cta, p_descricao, p_mascara);
        exception when others then
            p_msg_erro := 'Ocorreu um erro ao inserir plano de contas:' || SQLERRM;
        end;
        
	end;

end ST_PCK_PLANO_CONTAS;
