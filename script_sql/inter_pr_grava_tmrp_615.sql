
  CREATE OR REPLACE PROCEDURE "INTER_PR_GRAVA_TMRP_615" 
   (p_empresa_logada in number,
    p_nivel          in varchar2,
    p_grupo          in varchar2,
    p_subgrupo       in varchar2,
    p_item           in varchar2,
    p_aprogramar     in number,
    p_codigo_usuario in number,
    p_usuario        in varchar2,
    p_nr_solicitacao in number,
    p_nome_programa  in varchar2,
    p_tipo_registro  in number,
    p_nro_insert     in number,
    p_visualiza_mp   in number
) is

begin

declare

   p_count             number(10);
   p_mensagem          varchar2(200);
   p_usuario_rede      varchar2(20);
   p_maquina_rede      varchar2(40);
   p_aplicativo        varchar2(20);
   p_sid               number(9);
   p_usuario_systextil varchar2(250);
   p_locale_usuario    varchar2(5);
   p_narrativa         varchar2(100);
   p_empresa_usu       number(3);

BEGIN
   inter_pr_dados_usuario (p_usuario_rede,
                           p_maquina_rede,
                           p_aplicativo,
                           p_sid,
                           p_usuario_systextil,
                           p_empresa_usu,
                           p_locale_usuario);
   begin
      select count(*)
      into p_count
      from tmrp_615
      where tmrp_615.nome_programa  = p_nome_programa
        and tmrp_615.nivel          = p_nivel
        and tmrp_615.grupo          = p_grupo
        and tmrp_615.subgrupo       = p_subgrupo
        and tmrp_615.item           = p_item
        and tmrp_615.nr_solicitacao = p_nr_solicitacao
        and tmrp_615.codigo_usuario = p_codigo_usuario
        and tmrp_615.tipo_registro  = p_tipo_registro
        and tmrp_615.usuario        = p_usuario
        and tmrp_615.codigo_empresa = p_empresa_logada;
      exception
         when no_data_found then
            p_count := 0;
   end;

   if p_count = 0
   then

      begin
         select basi_010.narrativa
           into p_narrativa
           from basi_010
          where basi_010.nivel_estrutura  = p_nivel
            and basi_010.grupo_estrutura  = p_grupo
            and basi_010.subgru_estrutura = p_subgrupo
            and basi_010.item_estrutura   = p_item;
         exception
            when no_data_found then
               p_narrativa := null;
      end;



if p_nro_insert = 0
or p_nro_insert is null
then
               raise_application_error(-20000, 'gecaaasa');
end if;

      begin
         insert into tmrp_615 (
             nome_programa,    nr_solicitacao,
             codigo_usuario,   tipo_registro,
             usuario,          codigo_empresa,
             nivel,            grupo,
             subgrupo,         item,
             qtde_necessaria,  narrativa,
             roteiro_prod,     tipo_natureza
         ) values (
             p_nome_programa,  p_nr_solicitacao,
             p_codigo_usuario, p_tipo_registro,
             p_usuario,        p_empresa_logada,
             p_nivel,          p_grupo,
             p_subgrupo,       p_item,
             p_aprogramar,     p_narrativa,
             p_nro_insert,     p_visualiza_mp
         );
      exception when others then
               --ATENCAO! Nao inseriu TMRP_615 {0}. Status: {1}
               p_mensagem := inter_fn_buscar_tag_composta('ds21459',
                                                          'TMRP_615',
                                                          sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',
                                                          p_locale_usuario,
                                                          p_usuario_systextil);

               raise_application_error(-20000, p_mensagem);
      end;

   else

      begin
         update tmrp_615
            set tmrp_615.qtde_necessaria = tmrp_615.qtde_necessaria + p_aprogramar
          where tmrp_615.nome_programa  = p_nome_programa
            and tmrp_615.nivel          = p_nivel
            and tmrp_615.grupo          = p_grupo
            and tmrp_615.subgrupo       = p_subgrupo
            and tmrp_615.item           = p_item
            and tmrp_615.nr_solicitacao = p_nr_solicitacao
            and tmrp_615.codigo_usuario = p_codigo_usuario
            and tmrp_615.tipo_registro  = p_tipo_registro
            and tmrp_615.usuario        = p_usuario
            and tmrp_615.codigo_empresa = p_empresa_logada;
         exception
            when others then
               --ATENCAO! Nao atualizou TMRP_615 {0}. Status = {1}
               p_mensagem := inter_fn_buscar_tag_composta('ds21501',
                                                          'TMRP_615',
                                                          sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',
                                                          p_locale_usuario,
                                                          p_usuario_systextil);

               raise_application_error(-20000, p_mensagem);
      end;
   end if;
END;

end inter_pr_grava_tmrp_615;

 

/

exec inter_pr_recompile;

