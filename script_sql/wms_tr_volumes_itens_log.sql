create or replace trigger wms_tr_volumes_itens_log
before insert
or delete
or update of nivel_sku_kg, grupo_sku_kg,       subgrupo_sku_kg, item_sku_kg,
	     nr_volume_st, cod_barras_ean,     nivel_sku,       grupo_sku,
	     subgrupo_sku, item_sku,           qtde_pecas,      qtde_quilos_estoque,
	     dep_item,     flag_integracao_st, ref_original,    sku_original
on inte_wms_vol_itens
for each row
declare

   v_nr_volume_st               inte_wms_vol_itens.nr_volume_st          %type;
   v_cod_barras_ean             inte_wms_vol_itens.cod_barras_ean      %type; 
   v_nivel_sku_new              inte_wms_vol_itens.nivel_sku           %type;
   v_nivel_sku_old              inte_wms_vol_itens.nivel_sku           %type;
   v_grupo_sku_new              inte_wms_vol_itens.grupo_sku           %type;
   v_grupo_sku_old              inte_wms_vol_itens.grupo_sku           %type;
   v_subgrupo_sku_new           inte_wms_vol_itens.subgrupo_sku        %type;
   v_subgrupo_sku_old           inte_wms_vol_itens.subgrupo_sku        %type;
   v_item_sku_new               inte_wms_vol_itens.item_sku            %type;
   v_item_sku_old               inte_wms_vol_itens.item_sku            %type;
   v_qtde_pecas_new             inte_wms_vol_itens.qtde_pecas          %type;
   v_qtde_pecas_old             inte_wms_vol_itens.qtde_pecas          %type;
   v_qtde_quilos_estoque_new    inte_wms_vol_itens.qtde_quilos_estoque %type;
   v_qtde_quilos_estoque_old    inte_wms_vol_itens.qtde_quilos_estoque %type;
   v_dep_item_new               inte_wms_vol_itens.dep_item            %type;
   v_dep_item_old               inte_wms_vol_itens.dep_item            %type;
   v_flag_integracao_st_new     inte_wms_vol_itens.flag_integracao_st  %type;
   v_flag_integracao_st_old     inte_wms_vol_itens.flag_integracao_st  %type;
   v_nivel_sku_kg_new           inte_wms_vol_itens.nivel_sku_kg        %type;
   v_nivel_sku_kg_old           inte_wms_vol_itens.nivel_sku_kg        %type;
   v_grupo_sku_kg_new           inte_wms_vol_itens.grupo_sku_kg        %type;
   v_grupo_sku_kg_old           inte_wms_vol_itens.grupo_sku_kg        %type;
   v_subgrupo_sku_kg_new        inte_wms_vol_itens.subgrupo_sku_kg     %type;
   v_subgrupo_sku_kg_old        inte_wms_vol_itens.subgrupo_sku_kg     %type;
   v_item_sku_kg_new            inte_wms_vol_itens.item_sku_kg         %type;
   v_item_sku_kg_old            inte_wms_vol_itens.item_sku_kg         %type;
   v_ref_original_new           inte_wms_vol_itens.ref_original        %type;
   v_ref_original_old           inte_wms_vol_itens.ref_original        %type;
   v_sku_original_new           inte_wms_vol_itens.sku_original        %type;
   v_sku_original_old           inte_wms_vol_itens.sku_original        %type;

   v_sid                        number(9);                
   v_empresa                    number(3);                
   v_usuario_systextil          varchar2(250);             
   v_locale_usuario             varchar2(5);           
   v_nome_programa              varchar2(20);             
   v_operacao                   varchar(1);
   v_data_operacao              date;
   v_usuario_rede               varchar(20);
   v_maquina_rede               varchar(40);
   v_aplicativo                 varchar(20);
begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();
   
   --alimenta as variaveis new caso seja insert ou update
   if inserting or updating
   then
      if inserting
      then v_operacao := 'i';
      else v_operacao := 'u';
      end if;
      
      v_nr_volume_st              := :new.nr_volume_st;
      v_cod_barras_ean            := :new.cod_barras_ean;
      v_nivel_sku_new             := :new.nivel_sku;
      v_grupo_sku_new             := :new.grupo_sku;
      v_subgrupo_sku_new          := :new.subgrupo_sku;
      v_item_sku_new              := :new.item_sku;
      v_qtde_pecas_new            := :new.qtde_pecas;
      v_qtde_quilos_estoque_new   := :new.qtde_quilos_estoque;
      v_dep_item_new              := :new.dep_item;
      v_flag_integracao_st_new    := :new.flag_integracao_st; 
      v_nivel_sku_kg_new          := :new.nivel_sku_kg;
      v_grupo_sku_kg_new          := :new.grupo_sku_kg;
      v_subgrupo_sku_kg_new       := :new.subgrupo_sku_kg;
      v_item_sku_kg_new           := :new.item_sku_kg;
      v_ref_original_new          := :new.ref_original;
      v_sku_original_new          := :new.sku_original;

   end if; --fim do if inserting or updating
   
   --alimenta as variaveis old caso seja insert ou update
   if deleting or updating
   then
      if deleting
      then
         v_operacao      := 'd';
      else
         v_operacao      := 'u';
      end if;
      
      v_nr_volume_st              := :old.nr_volume_st;
      v_cod_barras_ean            := :old.cod_barras_ean;
      v_nivel_sku_old             := :old.nivel_sku;
      v_grupo_sku_old             := :old.grupo_sku;
      v_subgrupo_sku_old          := :old.subgrupo_sku;
      v_item_sku_old              := :old.item_sku;
      v_qtde_pecas_old            := :old.qtde_pecas;
      v_qtde_quilos_estoque_old   := :old.qtde_quilos_estoque;
      v_dep_item_old              := :old.dep_item;
      v_flag_integracao_st_old    := :old.flag_integracao_st;
      v_nivel_sku_kg_old          := :old.nivel_sku_kg;
      v_grupo_sku_kg_old          := :old.grupo_sku_kg;
      v_subgrupo_sku_kg_old       := :old.subgrupo_sku_kg;
      v_item_sku_kg_old           := :old.item_sku_kg;
      v_ref_original_old          := :old.ref_original;
      v_sku_original_old          := :old.sku_original;
      
   end if; --fim do if deleting or updating
   
   
   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);
   
   
    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente
    
    -- insere as informa��es na tabela de log
    insert into inte_wms_vol_itens_log (
       nr_volume_st,                   cod_barras_ean,          
       nivel_sku_new,                  nivel_sku_old,
       grupo_sku_new,                  grupo_sku_old,          
       subgrupo_sku_new,               subgrupo_sku_old,       
       item_sku_new,                   item_sku_old,           
       qtde_pecas_new,                 qtde_pecas_old,
       qtde_quilos_estoque_new,        qtde_quilos_estoque_old,
       dep_item_new,                   dep_item_old,           
       flag_integracao_st_new,         flag_integracao_st_old, 
       operacao,                       data_operacao,             
       usuario_rede,                   maquina_rede,             
       aplicativo,                     nome_programa,
       nivel_sku_kg_new,               nivel_sku_kg_old,
       grupo_sku_kg_new,               grupo_sku_kg_old,
       subgrupo_sku_kg_new,            subgrupo_sku_kg_old,
       item_sku_kg_new,                item_sku_kg_old,
       ref_original_new,               ref_original_old,
       sku_original_new,               sku_original_old)
    values(
       v_nr_volume_st,                 v_cod_barras_ean,            
       v_nivel_sku_new,                v_nivel_sku_old,             
       v_grupo_sku_new,                v_grupo_sku_old,             
       v_subgrupo_sku_new,             v_subgrupo_sku_old,          
       v_item_sku_new,                 v_item_sku_old,              
       v_qtde_pecas_new,               v_qtde_pecas_old,            
       v_dep_item_new,                 v_dep_item_old,
       v_qtde_quilos_estoque_new,      v_qtde_quilos_estoque_old,
       v_flag_integracao_st_new,       v_flag_integracao_st_old,      
       v_operacao,                     v_data_operacao,             
       v_usuario_rede,                 v_maquina_rede,             
       v_aplicativo,                   v_nome_programa,
       v_nivel_sku_kg_new,             v_nivel_sku_kg_old,
       v_grupo_sku_kg_new,             v_grupo_sku_kg_old,
       v_subgrupo_sku_kg_new,          v_subgrupo_sku_kg_old,
       v_item_sku_kg_new,              v_item_sku_kg_old,
       v_ref_original_new,             v_ref_original_old,
       v_sku_original_new,             v_sku_original_old
    );
       
end wms_tr_volumes_itens_log;

/

execute inter_pr_recompile;

/* versao: 4 */


 exit;


 exit;


 exit;

 exit;
