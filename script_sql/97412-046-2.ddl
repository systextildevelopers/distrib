ALTER TABLE mqop_030 
ADD ultima_leitura2 NUMBER (10,2) DEFAULT 0.00;

ALTER TABLE mqop_030 
ADD ultima_leitura3 NUMBER (10,2) DEFAULT 0.00;

ALTER TABLE mqop_030 
ADD ultima_leitura4 NUMBER (10,2) DEFAULT 0.00;


UPDATE mqop_030
   SET ultima_leitura2 = -1,
       ultima_leitura3 = -1,
       ultima_leitura4 = -1;
  
commit work;

/
exec inter_pr_recompile;

