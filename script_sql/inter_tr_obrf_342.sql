CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_342"
BEFORE INSERT ON OBRF_342 FOR EACH ROW
DECLARE
    nextValue number;
BEGIN
    select ID_OBRF_342.nextval into nextValue from dual;
    :new.ID := nextValue;
END INTER_TR_OBRF_342;

/

exec inter_pr_recompile;
