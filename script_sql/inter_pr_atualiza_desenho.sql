create or replace procedure inter_pr_atualiza_desenho
( p_codigo_empresa              number
, p_desenho                     varchar2
, p_alternativa                 number
, p_grupo_tecido                varchar2
, p_subgrupo_tecido             varchar2
) is

v_grupo_tecido                  varchar2(5);
v_subgrupo_tecido               varchar2(3);
v_nr_solicitacao                number(9);
v_desfazer_transacao            boolean;
v_alternativa                   number(2);

--------------------------------------------------------------------------------------------------------------------------------------------------------------
--busca_codigo_reduzido
function busca_codigo_reduzido(p_usa_cod_reduzido number) return boolean is

v_ult_cod_reduzido              number(9);
v_codigo_reduzido               number(9);
v_achou_basi_f130               varchar(1);
v_codigo_pesquisa               number(9);
v_desfazer_transacao            boolean;
v_tmp_int                       number(1);

begin
  v_desfazer_transacao := false;

  if p_usa_cod_reduzido = 1 or p_usa_cod_reduzido = 2
  then
    begin
      update fatu_501
      set ult_cod_reduzido = fatu_501.ult_cod_reduzido;
      exception
      when OTHERS then
        v_ult_cod_reduzido := 0;
        begin
          select fatu_501.ult_cod_reduzido
          into v_ult_cod_reduzido
          from fatu_501;
          exception when OTHERS then
            if p_usa_cod_reduzido = 1
            then
              v_codigo_reduzido := v_ult_cod_reduzido + 1;
            end if;

            if p_usa_cod_reduzido = 2
            then
              v_achou_basi_f130 := 's';
              v_codigo_pesquisa := v_ult_cod_reduzido;

              loop
                select 1
                into v_tmp_int
                from basi_010
                where basi_010.codigo_barras   = TO_CHAR(v_codigo_pesquisa)
                  and basi_010.nivel_estrutura = '2';
                if v_tmp_int <> 0
                then
                   select 1
                   into v_tmp_int
                   from basi_010
                   where basi_010.codigo_barras   = TO_CHAR(v_codigo_pesquisa)
                   and basi_010.nivel_estrutura = '2';

                   if v_tmp_int <> 0
                   then
                     v_achou_basi_f130 := 'n';
                   else
                     v_codigo_pesquisa := v_codigo_pesquisa + 1;
                   end if;
                else
                  v_codigo_pesquisa := v_codigo_pesquisa + 1;
                end if;
                exit when v_achou_basi_f130 = 'n';
               end loop;

              v_codigo_reduzido := v_codigo_pesquisa;
            end if;
            begin
              update fatu_501
              set    fatu_501.ult_cod_reduzido = v_codigo_reduzido;
              exception when OTHERS then
                RAISE_APPLICATION_ERROR (-20205, sqlerrm || 'ATEN??O! Tente gravar novamente, se n?o conseguir avise o CPD.');
                v_desfazer_transacao := true;
            end;
          end;
        RAISE_APPLICATION_ERROR (-20205, sqlerrm || 'ATEN??O! N?o foi poss?vel encontrar novo n?mero do c?digo reduzido.');
        v_desfazer_transacao := true;
    end;
  end if;
  return(v_desfazer_transacao);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------------------
--f_painel_estrutura_062
function f_painel_estrutura_062(p_codigo_projeto varchar2) return boolean is

v_r_erro                        boolean;

begin
    v_r_erro := false;

    begin
      update basi_050
      set   basi_050.sequencia_projeto = -999
      where basi_050.codigo_projeto    = p_codigo_projeto;
      exception
      when OTHERS then
        v_r_erro := true;
    end;

    begin
      update basi_040
      set   basi_040.sequencia_projeto = -999
      where basi_040.codigo_projeto    = p_codigo_projeto;
      exception
      when OTHERS then
        v_r_erro := true;
    end;
    return(v_r_erro);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------------------
--fcalcula_cons_por_litro
procedure fcalcula_cons_por_litro
(
  p_niv_tec                    varchar2
, p_gru_tec                    varchar2
, p_sub_tec                    varchar2
, p_item_tec                   varchar2
, p_alt_tec                    number
, p_nivel_rec                  varchar2
, p_grupo_rec                  varchar2
, p_sub_rec                    varchar2
, p_item_rec                   varchar2
, p_alternativa                number
, p_recalcula_cons             varchar2
) is

v_desfazer_transacao           boolean;
v_tp_calc_grama_litro          number(2);
v_relacao_unidade              number(8,3);
v_total_cons_un_rec            number(13,7);

begin
    v_desfazer_transacao := false;
    v_tp_calc_grama_litro := 0;

    begin
      select empr_002.tipo_calculo_grama_litro
      into v_tp_calc_grama_litro
      from empr_002;
      exception when OTHERS then
        v_tp_calc_grama_litro := 0;

        if v_tp_calc_grama_litro is not null
        then
          begin
            select basi_420.relacao_uni
            into v_relacao_unidade
            from basi_420
            where basi_420.codigo = v_tp_calc_grama_litro;
            exception when OTHERS then
              v_relacao_unidade := 1000;
          end;
        end if;
    end;

    if v_tp_calc_grama_litro is not null or v_tp_calc_grama_litro <> 0
    then
      v_total_cons_un_rec := 0.000000;

      begin
        select sum(basi_050.cons_un_rec)
        into v_total_cons_un_rec
        from basi_050
        where basi_050.nivel_item       = p_nivel_rec
          and basi_050.grupo_item       = p_grupo_rec
          and basi_050.sub_item         = p_sub_rec
          and basi_050.item_item        = p_item_rec
          and basi_050.alternativa_item = p_alternativa
          and basi_050.tipo_calculo     = v_tp_calc_grama_litro;
        exception when OTHERS then
          v_total_cons_un_rec := 0.000000;
      end;

      if v_total_cons_un_rec <> 0.000000 and p_recalcula_cons = 'S'
      then
        begin
          update basi_050
          set basi_050.consumo = basi_050.cons_un_rec / v_total_cons_un_rec
          where basi_050.nivel_item       = p_nivel_rec
            and basi_050.grupo_item       = p_grupo_rec
            and basi_050.sub_item         = p_sub_rec
            and basi_050.item_item        = p_item_rec
            and basi_050.alternativa_item = p_alternativa
            and basi_050.tipo_calculo     = v_tp_calc_grama_litro;
          exception when OTHERS then
            v_desfazer_transacao := true;
        end;

        for dados_050 in (
        select basi_050.nivel_item,       basi_050.grupo_item,
                basi_050.sub_item,         basi_050.item_item,
                basi_050.alternativa_item, basi_050.sequencia,
                basi_050.nivel_comp,       basi_050.grupo_comp,
                basi_050.sub_comp,         basi_050.item_comp,
                basi_050.alternativa_comp, basi_050.consumo,
                (select nvl(sum(basi_050a.cons_un_rec),0) from basi_050 basi_050a
                 where basi_050a.nivel_item       = basi_050.nivel_comp
                   and basi_050a.grupo_item       = basi_050.grupo_comp
                   and basi_050a.sub_item         = basi_050.sub_comp
                   and basi_050a.item_item        = basi_050.item_comp
                   and basi_050a.alternativa_item = basi_050.alternativa_comp
                   and basi_050a.tipo_calculo     = v_tp_calc_grama_litro) / basi_550.quantidade_por_litro / v_relacao_unidade as recalculo_cons_receita
        from basi_050, basi_550
        where basi_550.nivel_tecido         = basi_050.nivel_item
          and basi_550.grupo_tecido         = basi_050.grupo_item
          and basi_550.nivel_receita        = basi_050.nivel_comp
          and basi_550.grupo_receita        = basi_050.grupo_comp
          and (basi_050.nivel_item          = p_niv_tec     or p_niv_tec     = '0')
          and (basi_050.grupo_item          = p_gru_tec     or p_gru_tec     = '00000')
          and (basi_050.sub_item            = p_sub_tec     or p_sub_tec     = '000')
          and (basi_050.item_item           = p_item_tec    or p_item_tec    = '000000')
          and (basi_050.alternativa_item    = p_alt_tec     or p_alt_tec     = 0)
          and basi_050.nivel_comp           = p_nivel_rec
          and basi_050.grupo_comp           = p_grupo_rec
          and (basi_050.sub_comp            = p_sub_rec     or p_sub_rec     = '000')
          and (basi_050.item_comp           = p_item_rec    or p_item_rec    = '000000')
          and (basi_050.alternativa_comp    = p_alternativa or p_alternativa = 0)
          and basi_550.quantidade_por_litro > 0
        ) loop
          begin
            update basi_050
            set basi_050.consumo            = dados_050.recalculo_cons_receita
            where basi_050.nivel_item       = dados_050.nivel_item
              and basi_050.grupo_item       = dados_050.grupo_item
              and basi_050.sub_item         = dados_050.sub_item
              and basi_050.item_item        = dados_050.item_item
              and basi_050.alternativa_item = dados_050.alternativa_item
              and basi_050.sequencia        = dados_050.sequencia;
            exception when OTHERS then
              v_desfazer_transacao := true;
          end;

          if v_desfazer_transacao = true
          then
            rollback;
          end if;
        end loop;
      end if;
    end if;

    if v_desfazer_transacao = false
    then
      commit;
    end if;
end;

--------------------------------------------------------------------------------------------------------------------------------------------------------------
--f_painel_estrutura_065
function f_painel_estrutura_065
(
  p_codigo_desenho              varchar
, p_grupo_item                  varchar2
, p_subgrupo_item               varchar2
, p_item_item                   varchar2
, p_alternativa_item            number
, p_chave                       varchar2
, p_codigo_empresa              number
) return boolean is

v_tem_erro                      boolean;
v_informar_listrado             varchar2(1);
v_grupo_item4                   varchar2(5);
v_subgrupo_item4                varchar2(3);
v_tp_calc_grama_litro           number(1);
v_cor_fundo_basi_856            varchar2(6);
v_item_comp_basi_852            varchar2(6);
v_alternativa_basi_852          number(1);
v_numero_grafico_basi_852       number(6);

begin
  v_tem_erro := false;

  v_informar_listrado := 'N';

  begin
    select fatu_503.informar_listrado
    into v_informar_listrado
    from fatu_503
    where fatu_503.codigo_empresa = p_codigo_empresa;
    exception when OTHERS then
      v_informar_listrado := 'N';
  end;
v_informar_listrado := 'S';
  if v_informar_listrado = 'S'
  then
    begin
      select min(basi_845.grupo_produto), min(basi_845.subgrupo_produto)
      into v_grupo_item4, v_subgrupo_item4
      from basi_845
      where basi_845.nivel_tecido    = '2'
        and basi_845.grupo_tecido    = p_grupo_item
        and basi_845.subgrupo_tecido = p_subgrupo_item
        and basi_845.codigo_desenho  = p_codigo_desenho;
      exception when OTHERS then
        v_grupo_item4    := '00000';
        v_subgrupo_item4 := '000';
    end;
  else
    v_grupo_item4    := p_grupo_item;
    v_subgrupo_item4 := p_subgrupo_item;
  end if;

  begin
    select empr_002.tipo_calculo_grama_litro
    into v_tp_calc_grama_litro
    from empr_002;
    exception when OTHERS then
      v_tp_calc_grama_litro := 0;
  end;

  for dados_846 in (
  select basi_846.sequencia,
         basi_846.nivel_comp,       basi_846.grupo_comp,
         basi_846.sub_comp,         basi_846.item_comp,
         basi_846.alternativa_comp, basi_846.consumo,
         basi_846.percent_perdas,   basi_846.estagio,
         basi_846.centro_custo,     basi_846.numero_grafico
  from basi_846
  where basi_846.codigo_desenho    = p_codigo_desenho
    and basi_846.tipo_registro     = 0
    and basi_846.nivel_produto     = '4'
    and basi_846.grupo_produto     = v_grupo_item4
    and (basi_846.subgrupo_produto = v_subgrupo_item4
    or   basi_846.subgrupo_produto = '000')
    and basi_846.grupo_tecido      = p_grupo_item
    and basi_846.subgrupo_tecido   = p_subgrupo_item
    and basi_846.consumo           <> 0
    and basi_846.cor_fundo         ='000000'
  ) loop
    if  v_tp_calc_grama_litro > 0 and dados_846.nivel_comp = '5'
    then
      fcalcula_cons_por_litro ('2',                        p_grupo_item,
                               p_subgrupo_item,            p_item_item,
                               p_alternativa_item,
                               dados_846.nivel_comp,       dados_846.grupo_comp,
                               dados_846.sub_comp,         dados_846.item_comp,
                               dados_846.alternativa_comp, 'N');
    end if;

    v_cor_fundo_basi_856 := '';

    if dados_846.item_comp = '000000'
    then
      begin
        select basi_856.cor_fundo
        into v_cor_fundo_basi_856
        from basi_856
        where basi_856.codigo_desenho = p_codigo_desenho
          and basi_856.comb_cor_fundo = substr(p_item_item, 5, 6);
        exception
        when OTHERS then
          v_cor_fundo_basi_856 := '';
      end;

      if v_cor_fundo_basi_856 is not null
      then
        begin
          select basi_852.item_comp,     basi_852.alternativa,
                 basi_852.numero_grafico
          into   v_item_comp_basi_852,   v_alternativa_basi_852,
                 v_numero_grafico_basi_852
          from basi_852
          where basi_852.codigo_desenho     = p_codigo_desenho
            and basi_852.tipo_registro      = 0
            and basi_852.seq_cor            = ' '
            and basi_852.nivel_produto      = '4'
            and basi_852.grupo_produto      = v_grupo_item4
            and basi_852.subgrupo_produto   = v_subgrupo_item4
            and basi_852.sequencia          = dados_846.sequencia
            and basi_852.nivel_tecido       = '2'
            and basi_852.grupo_tecido       = p_grupo_item
            and basi_852.subgrupo_tecido    = p_subgrupo_item
            and basi_852.cor_fundo          = v_cor_fundo_basi_856
            and basi_852.utiliza_componente = 1;
          exception when OTHERS then
            v_item_comp_basi_852 := '000000';
            v_alternativa_basi_852 := 0;
            v_numero_grafico_basi_852 := 0;
        end;

        if v_item_comp_basi_852 <> '000000'
        then
           dados_846.item_comp := v_item_comp_basi_852;
        end if;

        if v_alternativa_basi_852 <> 0
        then
           dados_846.alternativa_comp := v_alternativa_basi_852;
        end if;

        if v_numero_grafico_basi_852 <> 0
        then
           dados_846.numero_grafico := v_numero_grafico_basi_852;
        end if;
      end if;
    end if;

    begin
      insert into basi_050 (
        nivel_item,                                  grupo_item,
        sub_item,                                    item_item,
        alternativa_item,                            sequencia,
        nivel_comp,                                  grupo_comp,
        sub_comp,                                    item_comp,
        alternativa_comp,                            consumo,
        cons_unid_med_generica,                      estagio,
        codigo_projeto,                              sequencia_projeto,
        centro_custo,                                percent_perdas,
        numero_grafico
      ) values (
        '2',                                         p_grupo_item,
        p_subgrupo_item,                             p_item_item,
        p_alternativa_item,                          dados_846.sequencia,
        dados_846.nivel_comp,                        dados_846.grupo_comp,
        dados_846.sub_comp,                          dados_846.item_comp,
        dados_846.alternativa_comp,                  dados_846.consumo,
        0,                                           dados_846.estagio,
        p_chave,                                     999,
        dados_846.centro_custo,                      dados_846.percent_perdas,
        dados_846.numero_grafico
      );
      exception when dup_val_on_index
      then
        begin
          UPDATE basi_050
          set   basi_050.nivel_comp             = dados_846.nivel_comp,
                basi_050.grupo_comp             = dados_846.grupo_comp,
                basi_050.sub_comp               = dados_846.sub_comp,
                basi_050.item_comp              = dados_846.item_comp,
                basi_050.alternativa_comp       = dados_846.alternativa_comp,
                basi_050.consumo                = dados_846.consumo,
                basi_050.centro_custo           = dados_846.centro_custo,
                basi_050.estagio                = dados_846.estagio,
                basi_050.codigo_projeto         = p_chave,
                basi_050.sequencia_projeto      = 999,
                basi_050.percent_perdas         = dados_846.percent_perdas,
                basi_050.numero_grafico         = dados_846.numero_grafico
           where basi_050.nivel_item             = '2'
            and basi_050.grupo_item             = p_grupo_item
            and basi_050.sub_item               = p_subgrupo_item
            and basi_050.item_item              = p_item_item
            and basi_050.alternativa_item       = p_alternativa_item
            and basi_050.sequencia              = dados_846.sequencia;
          exception when OTHERS then
             v_tem_erro := true;
        end;

        begin
          update basi_040
          set   basi_040.sequencia_projeto = 999
          where basi_040.nivel_item        = '2'
            and basi_040.grupo_item        = p_grupo_item
            and basi_040.alternativa_item  = p_alternativa_item
            and basi_040.sequencia         = dados_846.sequencia
            and basi_040.codigo_projeto    = p_chave
            and basi_040.sequencia_projeto = -999;
          exception when OTHERS then
            v_tem_erro := true;
        end;
    end;
  end loop;
  return(v_tem_erro);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------------------
--f_painel_estrutura_061
function f_painel_estrutura_061
(
  p_codigo_projeto              varchar2
, p_combinacao_item             varchar2
, p_alternativa                 number
, p_desenho                     varchar
, p_grupo_tecido                varchar2
, p_subgrupo_tecido             varchar2
, p_nome_programa               varchar2
) return boolean is

v_forma_narrativa_n1            number(1);
v_usa_cod_reduzido              number(1);
v_alternativa_padrao_503        varchar2(1);
v_tipo_desenho_841              number(1);
v_descr_combinacao_847          varchar2(20);
v_codigo_barra_010              varchar2(15);
v_niv_quebra_peca               number(1);
v_narrativa_aux                 varchar2(71);
v_data_lancamento               date;
v_controla_ativacao             number(1);
v_tamanho_ref_847               varchar2(3);
v_cor_847                       varchar2(6);
v_tipo_codigo_barra             number(1);
v_narrativa_010                 varchar2(71);
v_cod_contabil_030              number(6);
v_classific_fiscal              varchar2(15);
v_seq_tamanho                   number(3);
v_erro_processo                 boolean;

begin
  begin
    select fatu_501.narra_nivel1, fatu_501.usa_cod_reduzido
    into v_forma_narrativa_n1, v_usa_cod_reduzido
    from  fatu_501
    where fatu_501.codigo_empresa = p_codigo_empresa;
    exception
    when OTHERS then
      v_forma_narrativa_n1      := 0;
      v_usa_cod_reduzido     := 0;
  end;

  begin
    select fatu_503.alternativa_padrao
    into v_alternativa_padrao_503
    from fatu_503
    where fatu_503.codigo_empresa = p_codigo_empresa;
    exception
    when OTHERS then
      v_alternativa_padrao_503 := 'S';
  end;

  v_tipo_desenho_841 := 0;

  begin
    select basi_841.tipo_desenho
    into v_tipo_desenho_841
    from basi_841
    where basi_841.codigo_desenho = p_desenho;
    exception
    when OTHERS then
      v_tipo_desenho_841 := 0;
  end;

  for dados_desenho_847 in (
  select basi_845.nivel_componete,      basi_845.grupo_componente,
         basi_845.subgrupo_componente,  basi_848.cor,
         basi_030.descr_referencia,     basi_020.descr_tam_refer,
         basi_020.desc_tam_ficha
  from basi_845, basi_848, basi_030, basi_020, basi_847
  where basi_020.basi030_nivel030  = basi_845.nivel_componete
    and basi_020.basi030_referenc  = basi_845.grupo_componente
    and basi_020.tamanho_ref       = basi_845.subgrupo_componente
    and basi_030.nivel_estrutura   = basi_845.nivel_componete
    and basi_030.referencia        = basi_845.grupo_componente
    and basi_845.codigo_desenho    = basi_848.codigo_desenho
    and basi_845.seq_cor           = basi_848.seq_cor
    and basi_848.codigo_desenho    = basi_847.codigo_desenho
    and basi_848.comb_desenho      = basi_847.comb_desenho
    and basi_847.codigo_projeto    = p_codigo_projeto
    and basi_847.combinacao_item   = p_combinacao_item
    and v_alternativa_padrao_503   = 'S'
 --   and v_tipo_desenho_841         = 1
  group by basi_845.nivel_componete,              basi_845.grupo_componente,
           basi_845.subgrupo_componente,          basi_848.cor,
           basi_030.descr_referencia,             basi_020.descr_tam_refer,
           basi_020.desc_tam_ficha
  UNION ALL
     select basi_845.nivel_componete,      basi_845.grupo_componente,
            basi_845.subgrupo_componente,  basi_848.cor,
            basi_030.descr_referencia,     basi_020.descr_tam_refer,
            basi_020.desc_tam_ficha
  from basi_845, basi_848, basi_030, basi_020, basi_847
  where basi_020.basi030_nivel030  = basi_845.nivel_componete
    and basi_020.basi030_referenc  = basi_845.grupo_componente
    and basi_020.tamanho_ref       = basi_845.subgrupo_componente
    and basi_030.nivel_estrutura   = basi_845.nivel_componete
    and basi_030.referencia        = basi_845.grupo_componente
    and basi_845.codigo_desenho    = basi_848.codigo_desenho
    and basi_845.seq_cor           = basi_848.seq_cor
    and basi_848.codigo_desenho    = basi_847.codigo_desenho
    and basi_848.comb_desenho      = basi_847.comb_desenho
    and basi_847.codigo_projeto    = p_codigo_projeto
    and basi_847.combinacao_item   = p_combinacao_item
    and v_alternativa_padrao_503   = 'N'
    and basi_847.alternativa_prod  = p_alternativa
 --   and v_tipo_desenho_841         = 1
  group by basi_845.nivel_componete,              basi_845.grupo_componente,
           basi_845.subgrupo_componente,          basi_848.cor,
           basi_030.descr_referencia,             basi_020.descr_tam_refer,
           basi_020.desc_tam_ficha
  UNION ALL
  select basi_845.nivel_componete,      basi_845.grupo_componente,
         basi_845.subgrupo_componente,  basi_848.cor,
         basi_030.descr_referencia,     basi_020.descr_tam_refer,
         basi_020.desc_tam_ficha
  from basi_845, basi_848, basi_030, basi_020
  where basi_020.basi030_nivel030  = basi_845.nivel_componete
    and basi_020.basi030_referenc  = basi_845.grupo_componente
    and basi_020.tamanho_ref       = basi_845.subgrupo_componente
    and basi_030.nivel_estrutura   = basi_845.nivel_componete
    and basi_030.referencia        = basi_845.grupo_componente
    and basi_848.codigo_desenho    = basi_845.codigo_desenho
    and basi_848.seq_cor           = basi_845.seq_cor
    and basi_845.codigo_desenho    = p_desenho
    and basi_845.nivel_tecido      = '2'
    and basi_845.grupo_tecido      = p_grupo_tecido
    and basi_845.subgrupo_tecido   = p_subgrupo_tecido
  --  and v_tipo_desenho_841         = 2
  group by basi_845.nivel_componete,              basi_845.grupo_componente,
           basi_845.subgrupo_componente,          basi_848.cor,
           basi_030.descr_referencia,             basi_020.descr_tam_refer,
           basi_020.desc_tam_ficha
  ) loop
    begin
      select basi_100.descricao
      into v_descr_combinacao_847
      from basi_100
      where basi_100.cor_sortimento = dados_desenho_847.cor;
      exception
      when OTHERS then
        v_descr_combinacao_847 := '';
    end;

    if dados_desenho_847.desc_tam_ficha is null or dados_desenho_847.nivel_componete <> '9'
    then dados_desenho_847.desc_tam_ficha := '';
    end if;

    v_codigo_barra_010 := '';

    if dados_desenho_847.nivel_componete is null
    then dados_desenho_847.nivel_componete := '';
    end if;

    if dados_desenho_847.grupo_componente is null
    then dados_desenho_847.grupo_componente := '';
    end if;

    if v_tamanho_ref_847 is null
    then
      v_tamanho_ref_847 := '';
    end if;

    if v_cor_847 is null
    then
      v_cor_847 := '';
    end if;

    begin
      select fatu_500.tipo_cod_barras
      into v_tipo_codigo_barra
      from fatu_500
      where fatu_500.codigo_empresa = p_codigo_empresa;
      exception
      when OTHERS then
        v_tipo_codigo_barra := 0;
    end;

    if v_tipo_codigo_barra = 1
    then
      v_codigo_barra_010 := dados_desenho_847.nivel_componete || dados_desenho_847.grupo_componente || v_tamanho_ref_847 + v_cor_847;
    end if;

    if dados_desenho_847.nivel_componete = '2'
    then
      if busca_codigo_reduzido(v_usa_cod_reduzido)  = true
      then
        v_erro_processo := true;
      end if;
    end if;

    begin
      select fatu_500.niv_quebra_peca
      into v_niv_quebra_peca
      from fatu_500
      where fatu_500.codigo_empresa = p_codigo_empresa;
      exception
      when OTHERS then
        v_niv_quebra_peca := 0;
    end;

    if v_forma_narrativa_n1 = 0
    then
      if v_niv_quebra_peca = 1
      then
         v_narrativa_010 := dados_desenho_847.descr_referencia;
      end if;

      if v_niv_quebra_peca = 2
      then
        v_narrativa_010 := dados_desenho_847.descr_referencia  || dados_desenho_847.descr_tam_refer || dados_desenho_847.desc_tam_ficha;
      end if;

      if v_niv_quebra_peca = 3
      then
        v_narrativa_010 := dados_desenho_847.descr_referencia || dados_desenho_847.descr_tam_refer || dados_desenho_847.desc_tam_ficha || v_descr_combinacao_847;
      end if;
    else
      if dados_desenho_847.nivel_componete = '2'
      then
        begin
          select basi_010.narrativa
          into v_narrativa_aux
          from  basi_010
          where basi_010.nivel_estrutura = dados_desenho_847.nivel_componete
            and basi_010.grupo_estrutura = dados_desenho_847.grupo_componente
            and basi_010.narrativa       is not null;
          exception when OTHERS then
             v_narrativa_aux := ' ';
        end;

        v_narrativa_010 := v_narrativa_aux;
      else
        v_narrativa_010 := dados_desenho_847.descr_referencia || dados_desenho_847.descr_tam_refer || dados_desenho_847.desc_tam_ficha || v_descr_combinacao_847;
      end if;
    end if;

    v_cod_contabil_030 := 0;
    v_classific_fiscal := '';

    begin
      select basi_030.codigo_contabil, basi_030.classific_fiscal
      into v_cod_contabil_030, v_classific_fiscal
      from basi_030
      where basi_030.nivel_estrutura = dados_desenho_847.nivel_componete
        and basi_030.referencia      = dados_desenho_847.grupo_componente;
      exception when OTHERS then
        v_cod_contabil_030 := 0;
        v_classific_fiscal := '';
    end;

    v_seq_tamanho := 0;

    begin
      select basi_220.ordem_tamanho
      into v_seq_tamanho
      from basi_220
      where basi_220.tamanho_ref = v_tamanho_ref_847;
      exception when OTHERS then
        v_seq_tamanho := 0;
    end;

    v_data_lancamento := to_date(null);

    begin
      select empr_001.controla_ativacao
      into v_controla_ativacao
      from empr_001;
      exception
      when OTHERS then
        v_controla_ativacao := 0;
    end;

    if v_controla_ativacao = 0
    then v_data_lancamento := current_date;
    end if;

    if v_narrativa_010 is null
    then v_narrativa_010 := ' ';
    end if;

    begin
      insert into basi_010 (
        nivel_estrutura,                       grupo_estrutura,
        subgru_estrutura,                      item_estrutura,
        descricao_15,                          numero_roteiro,
        numero_alternati,                      narrativa,
        codigo_barras,                         data_cadastro,
        sequencia_tamanho,                     codigo_contabil,
        item_ativo,                            data_lancamento,
        classific_fiscal,                      complemento,
        alternativa_custos,                    roteiro_custos
      ) values (
        dados_desenho_847.nivel_componete,     dados_desenho_847.grupo_componente,
        dados_desenho_847.subgrupo_componente, dados_desenho_847.cor,
        substr(v_descr_combinacao_847,1,15),   1,
        1,                                     substr(v_narrativa_010,1,65),
        substr(v_codigo_barra_010,1,15),       current_date,
        v_seq_tamanho,                         v_cod_contabil_030,
        v_controla_ativacao,                   v_data_lancamento,
        v_classific_fiscal,                    p_nome_programa || ' - 03',
        1,                                     1
      );
      exception
      when dup_val_on_index
      then
        update basi_010
        set basi_010.descricao_15       = substr(v_descr_combinacao_847,1,15),
            basi_010.numero_roteiro     = 1,
            basi_010.numero_alternati   = 1,
            basi_010.narrativa          = substr(v_narrativa_010,1,65),
            basi_010.codigo_barras      = substr(v_codigo_barra_010,1,15),
            basi_010.data_cadastro      = current_date,
            basi_010.sequencia_tamanho  = v_seq_tamanho,
            basi_010.codigo_contabil    = v_cod_contabil_030,
            basi_010.item_ativo         = v_controla_ativacao,
            basi_010.data_lancamento    = v_data_lancamento,
            basi_010.classific_fiscal   = v_classific_fiscal,
            basi_010.complemento        = p_nome_programa || ' - 08',
            basi_010.alternativa_custos = 1,
            basi_010.roteiro_custos     = 1
        where basi_010.nivel_estrutura  = dados_desenho_847.nivel_componete
        and   basi_010.grupo_estrutura  = dados_desenho_847.grupo_componente
        and   basi_010.subgru_estrutura = dados_desenho_847.subgrupo_componente
        and   basi_010.item_estrutura   = dados_desenho_847.cor;
   end;

    begin
      insert into basi_100 (
        cor_sortimento,                      tipo_cor,
        descricao,                           cor_de_fundo
      ) values (
        dados_desenho_847.cor,               1,
        substr(v_descr_combinacao_847,1,20), '000000'
      );
      exception
      when dup_val_on_index
      then
        update basi_100
        set   basi_100.descricao      = substr(v_descr_combinacao_847,1,20)
        where basi_100.cor_sortimento = dados_desenho_847.cor
          and basi_100.tipo_cor       = 1;
    end;
  end loop;
  return(v_erro_processo);
end;
--------------------------------------------------------------------------------------------------------------------------------------------------------------
--f_painel_estrutura_060
function f_painel_estrutura_060
(
  p_codigo_projeto              varchar2
, p_combinacao_item             varchar2
, p_alternativa_produto         number
, p_nome_programa               varchar2
, p_nr_solicitacao              number
, p_desenho                     varchar
, p_grupo_tecido                varchar2
, p_subgrupo_tecido             varchar2
) return boolean is

v_informar_listrado             varchar2(1);
v_forma_narrativa               number(1);
v_usa_cod_reduzido              number(1);
v_tipo_desenho_841              number(1);
v_cor_fundo_nivel_2             varchar2(6);
v_erro_processo                 boolean;
v_codigo_barra_010              varchar2(15);
v_cor_847_nivel_2               varchar2(6);
v_cor_847_nivel_4               varchar2(6);
v_tipo_codigo_barra             number(1);
v_niv_quebra_peca               number(1);
v_narrativa_010                 varchar2(71);
v_narrativa_aux                 varchar2(71);
v_cod_contabil_030              number(6);
v_classific_fiscal              varchar2(15);
v_seq_tamanho                   number(3);
v_data_lancamento               date;
v_controla_ativacao             number(1);
v_qtde_necessaria               number(15,5);
v_serie_cor                     number(4);
v_data_02                       date;
v_grupo_tecido4                 varchar2(5);
v_subgrupo_tecido4              varchar2(3);
v_codigo_usuario                number(9);
v_XXX                           number(1);

begin

  v_informar_listrado := 'N';
  v_codigo_usuario := 10000;

  begin
    select fatu_503.informar_listrado
    into v_informar_listrado
    from fatu_503
    where fatu_503.codigo_empresa = p_codigo_empresa;
    exception
    when OTHERS then
      v_informar_listrado := 'N';
  end;
v_informar_listrado := 'S';
  begin
    select fatu_501.narra_nivel1, fatu_501.usa_cod_reduzido
    into v_forma_narrativa, v_usa_cod_reduzido
    from  fatu_501
    where fatu_501.codigo_empresa = p_codigo_empresa;
    exception
    when OTHERS then
      v_usa_cod_reduzido     := 0;
      v_forma_narrativa      := 0;
  end;

  v_tipo_desenho_841 := 0;

  begin
    select basi_841.tipo_desenho
    into v_tipo_desenho_841
    from basi_841
    where basi_841.codigo_desenho = p_desenho;
    exception
    when OTHERS then
      v_tipo_desenho_841 := 0;
  end;

  for dados_desenho_847 in (
  select    vbasi_desenhos.codigo_desenho,       vbasi_desenhos.comb_desenho_nivel_2,
            vbasi_desenhos.nivel_comp,           vbasi_desenhos.grupo_comp,
            vbasi_desenhos.subgrupo_comp,        vbasi_desenhos.item_comp,
            vbasi_desenhos.descr_referencia,     vbasi_desenhos.descr_tam_refer,
            vbasi_desenhos.descr_combinacao,     vbasi_desenhos.desc_tam_ficha,
            vbasi_desenhos.comb_desenho_nivel_4, vbasi_desenhos.descr_combinacao_nivel_4,
            vbasi_desenhos.cor_fundo_nivel_2
     from (select basi_013.codigo_desenho        as codigo_desenho,
                  basi_847.comb_desenho          as comb_desenho_nivel_2,
                  basi_013.nivel_comp            as nivel_comp,
                  basi_013.grupo_comp            as grupo_comp,
                  basi_020.tamanho_ref           as subgrupo_comp,
                  basi_013.item_comp             as item_comp,
                  basi_030.descr_referencia      as descr_referencia,
                  basi_020.descr_tam_refer       as descr_tam_refer,
                  basi_844.descr_combinacao      as descr_combinacao,
                  basi_020.desc_tam_ficha        as desc_tam_ficha,
                  basi_847.comb_desenho          as comb_desenho_nivel_4,
                  basi_844.descr_combinacao      as descr_combinacao_nivel_4,
                  '000000'                       as cor_fundo_nivel_2
           from basi_013, basi_847, basi_030, basi_020, basi_844
           where basi_847.codigo_projeto      = basi_013.codigo_projeto
             and basi_847.codigo_desenho      = basi_013.codigo_desenho
             and basi_013.alternativa_produto = p_alternativa_produto
             and basi_013.codigo_desenho      <> ' '
             and basi_013.utiliza_componente  = 1

             and basi_020.basi030_nivel030    = basi_013.nivel_comp
             and basi_020.basi030_referenc    = basi_013.grupo_comp
             and basi_020.tamanho_ref         = basi_013.subgru_comp

             and basi_030.nivel_estrutura     = basi_013.nivel_comp
             and basi_030.referencia          = basi_013.grupo_comp

             and basi_844.codigo_desenho      = basi_847.codigo_desenho
             and basi_844.comb_desenho        = basi_847.comb_desenho

             and basi_847.codigo_projeto      = p_codigo_projeto
             and basi_847.combinacao_item     = p_combinacao_item
             -- and v_tipo_desenho_841           = 1
           UNION ALL
           select basi_013.codigo_desenho        as codigo_desenho,
                  basi_847.comb_desenho          as comb_desenho_nivel_2,
                  basi_013.nivel_comp            as nivel_comp,
                  basi_013.grupo_comp            as grupo_comp,
                  basi_020.tamanho_ref           as subgrupo_comp,
                  basi_013.item_comp             as item_comp,
                  basi_030.descr_referencia      as descr_referencia,
                  basi_020.descr_tam_refer       as descr_tam_refer,
                  basi_844.descr_combinacao      as descr_combinacao,
                  basi_020.desc_tam_ficha        as desc_tam_ficha,
                  basi_847.comb_desenho          as comb_desenho_nivel_4,
                  basi_844.descr_combinacao      as descr_combinacao_nivel_4,
                  '000000'                       as cor_fundo_nivel_2
           from basi_013, basi_847, basi_030, basi_020, basi_844, basi_021
           where basi_847.codigo_projeto      = basi_013.codigo_projeto
             and basi_847.codigo_desenho      = basi_013.codigo_desenho
             and basi_013.alternativa_produto = p_alternativa_produto
             and basi_013.codigo_desenho      <> ' '
             and basi_013.utiliza_componente  = 1

             and basi_020.basi030_nivel030    = basi_013.nivel_comp
             and basi_020.basi030_referenc    = basi_013.grupo_comp
             and basi_013.subgru_comp         = '000'

             and basi_030.nivel_estrutura     = basi_013.nivel_comp
             and basi_030.referencia          = basi_013.grupo_comp

             and basi_844.codigo_desenho      = basi_847.codigo_desenho
             and basi_844.comb_desenho        = basi_847.comb_desenho

             and basi_847.codigo_projeto      = p_codigo_projeto
             and basi_847.combinacao_item     = p_combinacao_item

             and basi_021.codigo_projeto      = basi_013.codigo_projeto
             and basi_021.sequencia_projeto   = basi_013.sequencia_projeto
             and basi_021.nivel_item          = basi_013.nivel_item
             and basi_021.grupo_item          = basi_013.grupo_item
             and basi_021.item_item           = basi_013.item_item
             and basi_021.alternativa_produto = basi_013.alternativa_produto
             and basi_021.sequencia_estrutura = basi_013.sequencia_estrutura
             and basi_021.sequencia_variacao  = basi_013.sequencia_variacao
             and basi_021.subgru_comp         = basi_020.tamanho_ref
             and basi_021.utiliza_componente  = 1
         --    and v_tipo_desenho_841           = 1
           UNION ALL
           select basi844.codigo_desenho           as codigo_desenho,
                  basi_856.comb_cor_fundo          as comb_desenho_nivel_2,
                  basi_845.nivel_tecido            as nivel_comp,
                  basi_845.grupo_tecido            as grupo_comp,
                  basi_845.subgrupo_tecido         as subgrupo_comp,
                 '000000'                          as item_comp,
                  basi_030.descr_referencia        as descr_referencia,
                  basi_020.descr_tam_refer         as descr_tam_refer,
                  basi_100.descricao               as descr_combinacao,
                  basi_020.desc_tam_ficha          as desc_tam_ficha,
                  basi_856.comb_desenho            as comb_desenho_nivel_4,
                  basi844.descr_combinacao         as descr_combinacao_nivel_4,
                  basi_856.cor_fundo               as cor_fundo_nivel_2
           from basi_845, basi_856, basi_030, basi_020, basi844, basi_100
           where basi_845.codigo_desenho      = p_desenho
             and basi_845.nivel_tecido        = '2'
             and basi_845.grupo_tecido        = p_grupo_tecido
             and basi_845.subgrupo_tecido     = p_subgrupo_tecido

             and basi844.codigo_desenho       = basi_845.codigo_desenho
             and basi844.tem_cor_fundo        = 1

             and basi_856.codigo_desenho      = basi844.codigo_desenho
             and basi_856.comb_desenho        = basi844.comb_desenho
             and basi_856.item_ativo          = 1

             and basi_100.cor_sortimento      = basi_856.cor_fundo
             and basi_100.tipo_cor            = 1

             and basi_030.nivel_estrutura     = basi_845.nivel_tecido
             and basi_030.referencia          = basi_845.grupo_tecido

             and basi_020.basi030_nivel030    = basi_845.nivel_tecido
             and basi_020.basi030_referenc    = basi_845.grupo_tecido
             and basi_020.tamanho_ref         = basi_845.subgrupo_tecido
          --   and v_tipo_desenho_841           = 2
           UNION ALL
           select basi844.codigo_desenho         as codigo_desenho,
                  basi_856.comb_cor_fundo        as comb_desenho_nivel_2,
                  basi_845.nivel_tecido          as nivel_comp,
                  basi_845.grupo_tecido          as grupo_comp,
                  basi_020.tamanho_ref           as subgrupo_comp,
                  '000000'                       as item_comp,
                  basi_030.descr_referencia      as descr_referencia,
                  basi_020.descr_tam_refer       as descr_tam_refer,
                  basi_100.descricao             as descr_combinacao,
                  basi_020.desc_tam_ficha        as desc_tam_ficha,
                  basi_856.comb_desenho          as comb_desenho_nivel_4,
                  basi844.descr_combinacao       as descr_combinacao_nivel_4,
                  basi_856.cor_fundo             as cor_fundo_nivel_2
           from basi_845, basi_856, basi_030, basi_020, basi844, basi_100
           where basi_845.codigo_desenho      = p_desenho
             and basi_845.nivel_tecido        = '2'
             and basi_845.grupo_tecido        = p_grupo_tecido
             and basi_845.subgrupo_tecido     = p_subgrupo_tecido
             and p_subgrupo_tecido            = '000'

             and basi844.codigo_desenho       = basi_845.codigo_desenho
             and basi844.tem_cor_fundo        = 1

             and basi_856.codigo_desenho      = basi844.codigo_desenho
             and basi_856.comb_desenho        = basi844.comb_desenho
             and basi_856.item_ativo          = 1

             and basi_100.cor_sortimento      = basi_856.cor_fundo
             and basi_100.tipo_cor            = 1

             and basi_030.nivel_estrutura     = basi_845.nivel_tecido
             and basi_030.referencia          = basi_845.grupo_tecido

             and basi_020.basi030_nivel030    = basi_845.nivel_tecido
             and basi_020.basi030_referenc    = basi_845.grupo_tecido
             and basi_020.tipo_produto        in (3,4)

           --  and v_tipo_desenho_841           = 2
           UNION ALL
           select basi844.codigo_desenho        as codigo_desenho,
                  basi844.comb_desenho          as comb_desenho_nivel_2,
                  basi_845.nivel_tecido         as nivel_comp,
                  basi_845.grupo_tecido         as grupo_comp,
                  basi_845.subgrupo_tecido      as subgrupo_comp,
                  '000000'                      as item_comp,
                  basi_030.descr_referencia     as descr_referencia,
                  basi_020.descr_tam_refer      as descr_tam_refer,
                  basi844.descr_combinacao      as descr_combinacao,
                  basi_020.desc_tam_ficha       as desc_tam_ficha,
                  basi844.comb_desenho          as comb_desenho_nivel_4,
                  basi844.descr_combinacao      as descr_combinacao_nivel_4,
                  '000000'                      as cor_fundo_nivel_2
           from basi_845, basi_030, basi_020, basi844
           where basi_845.codigo_desenho      = p_desenho
             and basi_845.nivel_tecido        = '2'
             and basi_845.grupo_tecido        = p_grupo_tecido
             and basi_845.subgrupo_tecido     = p_subgrupo_tecido

             and basi844.codigo_desenho       = basi_845.codigo_desenho
             and basi844.tem_cor_fundo        = 0

             and basi_030.nivel_estrutura     = basi_845.nivel_tecido
             and basi_030.referencia          = basi_845.grupo_tecido

             and basi_020.basi030_nivel030    = basi_845.nivel_tecido
             and basi_020.basi030_referenc    = basi_845.grupo_tecido
             and basi_020.tamanho_ref         = basi_845.subgrupo_tecido
           --  and v_tipo_desenho_841           = 2
           UNION ALL
           select basi844.codigo_desenho        as codigo_desenho,
                  basi844.comb_desenho          as comb_desenho_nivel_2,
                  basi_845.nivel_tecido         as nivel_comp,
                  basi_845.grupo_tecido         as grupo_comp,
                  basi_020.tamanho_ref          as subgrupo_comp,
                  '000000'                      as item_comp,
                  basi_030.descr_referencia     as descr_referencia,
                  basi_020.descr_tam_refer      as descr_tam_refer,
                  basi844.descr_combinacao      as descr_combinacao,
                  basi_020.desc_tam_ficha       as desc_tam_ficha,
                  basi844.comb_desenho          as comb_desenho_nivel_4,
                  basi844.descr_combinacao      as descr_combinacao_nivel_4,
                  '000000'                      as cor_fundo_nivel_2
           from basi_845, basi_030, basi_020, basi844
           where basi_845.codigo_desenho      = p_desenho
             and basi_845.nivel_tecido        = '2'
             and basi_845.grupo_tecido        = p_grupo_tecido
             and basi_845.subgrupo_tecido     = p_subgrupo_tecido
             and p_subgrupo_tecido            = '000'

             and basi844.codigo_desenho       = basi_845.codigo_desenho
             and basi844.tem_cor_fundo        = 0

             and basi_030.nivel_estrutura     = basi_845.nivel_tecido
             and basi_030.referencia          = basi_845.grupo_tecido

             and basi_020.basi030_nivel030    = basi_845.nivel_tecido
             and basi_020.basi030_referenc    = basi_845.grupo_tecido
             and basi_020.tipo_produto        in (3,4)

          --   and v_tipo_desenho_841           = 2
             ) vbasi_desenhos
     group by vbasi_desenhos.codigo_desenho,       vbasi_desenhos.comb_desenho_nivel_2,
              vbasi_desenhos.nivel_comp,           vbasi_desenhos.grupo_comp,
              vbasi_desenhos.subgrupo_comp,        vbasi_desenhos.item_comp,
              vbasi_desenhos.descr_referencia,     vbasi_desenhos.descr_tam_refer,
              vbasi_desenhos.descr_combinacao,     vbasi_desenhos.desc_tam_ficha,
              vbasi_desenhos.comb_desenho_nivel_4,
              vbasi_desenhos.descr_combinacao_nivel_4,
              vbasi_desenhos.cor_fundo_nivel_2
  ) loop
    if dados_desenho_847.comb_desenho_nivel_2 is null
    then
       RAISE_APPLICATION_ERROR (-20205, sqlerrm || 'ATEN??O! Cor n?o definida para uma seq??ncia de cor.');
       v_erro_processo := true;
    end if;

    if dados_desenho_847.descr_combinacao is null
    then
       dados_desenho_847.descr_combinacao := '';
    end if;

    if dados_desenho_847.desc_tam_ficha is null or dados_desenho_847.desc_tam_ficha <> '9'
    then
       dados_desenho_847.desc_tam_ficha := '';
    end if;

    v_codigo_barra_010 := ' ';

    v_cor_847_nivel_2 := dados_desenho_847.codigo_desenho || dados_desenho_847.comb_desenho_nivel_2;

    begin
      select fatu_500.tipo_cod_barras
      into v_tipo_codigo_barra
      from fatu_500
      where fatu_500.codigo_empresa = p_codigo_empresa;
      exception
      when OTHERS then
        v_tipo_codigo_barra := 0;
    end;

    if v_tipo_codigo_barra = 1
    then
       v_codigo_barra_010 := dados_desenho_847.nivel_comp || dados_desenho_847.grupo_comp || dados_desenho_847.subgrupo_comp || v_cor_847_nivel_2;
    end if;

    if dados_desenho_847.nivel_comp = '2'
    then
     if busca_codigo_reduzido(v_usa_cod_reduzido)  = true
     then
       v_erro_processo := true;
     end if;
    end if;

    begin
      select fatu_500.niv_quebra_peca
      into v_niv_quebra_peca
      from fatu_500
      where fatu_500.codigo_empresa = p_codigo_empresa;
      exception
      when OTHERS then
        v_niv_quebra_peca := 0;
    end;

    if v_forma_narrativa = 0
    then
       if v_niv_quebra_peca = 1
       then
          v_narrativa_010 := dados_desenho_847.descr_referencia;
       end if;

       if v_niv_quebra_peca = 2
       then
          v_narrativa_010 := dados_desenho_847.descr_referencia || ' ' ||
                             dados_desenho_847.descr_tam_refer  || ' ' ||
                             dados_desenho_847.desc_tam_ficha;
       end if;

       if v_niv_quebra_peca = 3
       then
          v_narrativa_010 := dados_desenho_847.descr_referencia  || ' ' ||
                             dados_desenho_847.descr_tam_refer   || ' ' ||
                             dados_desenho_847.desc_tam_ficha    || ' ' ||
                             dados_desenho_847.descr_combinacao;
       end if;
    else
      if dados_desenho_847.nivel_comp = '2'
      then
         begin
           select basi_010.narrativa
           into v_narrativa_aux
           from  basi_010
           where basi_010.nivel_estrutura = dados_desenho_847.nivel_comp
             and basi_010.grupo_estrutura = dados_desenho_847.grupo_comp
             and basi_010.narrativa       is not null;
           exception when OTHERS then
              v_narrativa_aux := ' ';
         end;

         v_narrativa_010 := v_narrativa_aux;
      else
         v_narrativa_010 := dados_desenho_847.descr_referencia  || ' ' ||
                            dados_desenho_847.descr_tam_refer   || ' ' ||
                            dados_desenho_847.desc_tam_ficha    || ' ' ||
                            dados_desenho_847.descr_combinacao;
      end if;
    end if;

    if v_cor_fundo_nivel_2 is null
    then v_cor_fundo_nivel_2 := '000000';
    end if;

    v_cod_contabil_030 := 0;
    v_classific_fiscal := '';

    begin
      select basi_030.codigo_contabil, basi_030.classific_fiscal
      into v_cod_contabil_030, v_classific_fiscal
      from basi_030
      where basi_030.nivel_estrutura = dados_desenho_847.nivel_comp
        and basi_030.referencia      = dados_desenho_847.grupo_comp;
      exception when OTHERS then
         v_cod_contabil_030 := 0;
         v_classific_fiscal := '';
    end;

    v_seq_tamanho := 0;

    if dados_desenho_847.subgrupo_comp is not null
    then
       begin
         select basi_220.ordem_tamanho
         into v_seq_tamanho
         from basi_220
         where basi_220.tamanho_ref = dados_desenho_847.subgrupo_comp;
         exception when OTHERS then
           v_seq_tamanho := 0;
       end;
    end if;

    v_data_lancamento := to_date(null);

    begin
      select empr_001.controla_ativacao
      into v_controla_ativacao
      from empr_001;
      exception
      when OTHERS then
        v_controla_ativacao := 0;
    end;

    if v_controla_ativacao = 0
    then v_data_lancamento := current_date;
    end if;

    if v_narrativa_010 is null
    then v_narrativa_010 := ' ';
    end if;
    begin
      insert into basi_010
      (
         nivel_estrutura,                                 grupo_estrutura,
         subgru_estrutura,                                item_estrutura,
         descricao_15,                                    numero_roteiro,
         numero_alternati,                                narrativa,
         codigo_barras,                                   data_cadastro,
         sequencia_tamanho,                               codigo_contabil,
         item_ativo,                                      data_lancamento,
         classific_fiscal,                                complemento,
         alternativa_custos,                              roteiro_custos
      ) values (
         dados_desenho_847.nivel_comp,                    dados_desenho_847.grupo_comp,
         dados_desenho_847.subgrupo_comp,                 v_cor_847_nivel_2,
         substr(dados_desenho_847.descr_combinacao,1,15), 1,
         1,                                               substr(v_narrativa_010,1,65),
         substr(v_codigo_barra_010,1,15),                 current_date,
         v_seq_tamanho,                                   v_cod_contabil_030,
         v_controla_ativacao,                             v_data_lancamento,
         v_classific_fiscal,                              p_nome_programa || ' - 07',
         1,                                               1
      );
      exception
      when dup_val_on_index
      then
        update basi_010
        set basi_010.descricao_15       = substr(dados_desenho_847.descr_combinacao,1,15),
            basi_010.numero_roteiro     = 1,
            basi_010.numero_alternati   = 1,
            basi_010.narrativa          = substr(v_narrativa_010,1,65),
            basi_010.codigo_barras      = substr(v_codigo_barra_010,1,15),
            basi_010.data_cadastro      = current_date,
            basi_010.sequencia_tamanho  = v_seq_tamanho,
            basi_010.codigo_contabil    = v_cod_contabil_030,
            basi_010.item_ativo         = v_controla_ativacao,
            basi_010.data_lancamento    = v_data_lancamento,
            basi_010.classific_fiscal   = v_classific_fiscal,
            basi_010.complemento        = p_nome_programa || ' - 07',
            basi_010.alternativa_custos = 1,
            basi_010.roteiro_custos     = 1
        where basi_010.nivel_estrutura  = dados_desenho_847.nivel_comp
        and   basi_010.grupo_estrutura  = dados_desenho_847.grupo_comp
        and   basi_010.subgru_estrutura = dados_desenho_847.subgrupo_comp
        and   basi_010.item_estrutura   = v_cor_847_nivel_2;
    end;

    v_qtde_necessaria := 0.00;
    v_serie_cor := 0;

    begin
      select tmrp_615.cgc_cliente4,    nvl(sum(tmrp_615.qtde_necessaria),0)
      into v_serie_cor, v_qtde_necessaria
      from tmrp_615
      where tmrp_615.nome_programa  = p_nome_programa
        and tmrp_615.codigo_usuario = v_codigo_usuario
        and tmrp_615.nr_solicitacao = p_nr_solicitacao
        and tmrp_615.tipo_registro  = 941
       and tmrp_615.codigo_projeto  = dados_desenho_847.codigo_desenho
        and tmrp_615.nome_cliente   = dados_desenho_847.comb_desenho_nivel_4
      group by tmrp_615.cgc_cliente4
      order by nvl(sum(tmrp_615.qtde_necessaria),0) DESC, tmrp_615.cgc_cliente4 DESC;
      exception when OTHERS then
        v_qtde_necessaria := 0.00;
        v_serie_cor := 0;
    end;

    begin
      insert into basi_100 (
          cor_sortimento,                                  tipo_cor,
          cor_pantone,                                     sortim_estampa,
          descricao,                                       serie_cor,
          cor_de_fundo
      ) values (
          v_cor_847_nivel_2,                               3,
          dados_desenho_847.codigo_desenho,                dados_desenho_847.comb_desenho_nivel_2,
          substr(dados_desenho_847.descr_combinacao,1,20), v_serie_cor,
          dados_desenho_847.cor_fundo_nivel_2
      );
      exception
       when dup_val_on_index
       then
          update basi_100
          set   basi_100.descricao      = substr(dados_desenho_847.descr_combinacao,1,20),
                basi_100.serie_cor      = v_serie_cor
          where basi_100.cor_sortimento = v_cor_847_nivel_2
            and basi_100.tipo_cor       = 3;
    end;

    for desenho in (
    select basi_857.colecao,                  basi_857.situacao,
               basi_857.bandeira
     from  basi_857
     where basi_857.codigo_desenho = dados_desenho_847.codigo_desenho
       and basi_857.comb_desenho   = dados_desenho_847.comb_desenho_nivel_4
       and basi_857.comb_cor_fundo = dados_desenho_847.comb_desenho_nivel_2
    ) loop
       v_data_02 := to_date(null);

       if desenho.situacao = 2
       then
          v_data_02 := current_date();
       end if;

       begin
         insert into basi_400 (
             tipo_informacao,
             nivel,                                        grupo,
             subgrupo,                                     item,
             codigo_informacao,                            situacao,
             classificacao,                                data_01,
            data_02
         ) values (
             12,
             dados_desenho_847.nivel_comp,                 dados_desenho_847.grupo_comp,
             dados_desenho_847.subgrupo_comp,              v_cor_847_nivel_2,
             desenho.colecao,                              desenho.situacao,
             desenho.bandeira,                             sysdate,
             v_data_02
         );
         exception
       when dup_val_on_index
       then
          v_XXX := 0;
       end;
    end loop;

    if dados_desenho_847.nivel_comp = '2'
    then
      v_cor_847_nivel_4 := dados_desenho_847.codigo_desenho || dados_desenho_847.comb_desenho_nivel_4;

      if busca_codigo_reduzido(v_usa_cod_reduzido)  = true
      then
        v_erro_processo := true;
      end if;
    end if;

    if dados_desenho_847.nivel_comp = '2'
    then
      begin
       select fatu_500.niv_quebra_peca
       into v_niv_quebra_peca
       from fatu_500
       where fatu_500.codigo_empresa = p_codigo_empresa;
       exception
       when OTHERS then
         v_niv_quebra_peca := 0;
      end;

      if v_forma_narrativa = 0
      then
         if v_niv_quebra_peca = 1
         then
            v_narrativa_010 := dados_desenho_847.descr_referencia;
         end if;

         if v_niv_quebra_peca = 2
         then
            v_narrativa_010 := dados_desenho_847.descr_referencia  || ' ' || dados_desenho_847.descr_tam_refer  || ' ' || dados_desenho_847.desc_tam_ficha;
         end if;

         if v_niv_quebra_peca = 3
         then
           v_narrativa_010 := dados_desenho_847.descr_referencia  || ' ' || dados_desenho_847.descr_tam_refer || ' ' || dados_desenho_847.desc_tam_ficha || ' ' || dados_desenho_847.descr_combinacao_nivel_4;
         end if;
      else
        if dados_desenho_847.nivel_comp = '2'
        then
          begin
            select basi_010.narrativa
            into v_narrativa_aux
            from  basi_010
            where basi_010.nivel_estrutura = dados_desenho_847.nivel_comp
              and basi_010.grupo_estrutura = dados_desenho_847.grupo_comp
              and basi_010.narrativa       is not null;
            exception when OTHERS then
               v_narrativa_aux := ' ';
          end;

          v_narrativa_010 := v_narrativa_aux;
       else
          v_narrativa_010 := dados_desenho_847.descr_referencia  || ' ' || dados_desenho_847.descr_tam_refer || ' ' || dados_desenho_847.desc_tam_ficha || ' ' || dados_desenho_847.descr_combinacao_nivel_4;
       end if;
     end if;
   end if;

   -- RAISE_APPLICATION_ERROR (-20205, sqlerrm || dados_desenho_847.grupo_comp || ' - ' || dados_desenho_847.subgrupo_comp || ': ' || v_informar_listrado);

   if v_informar_listrado = 'S'
   then
      begin
       select min(basi_845.grupo_produto), min(basi_845.subgrupo_produto)
       into v_grupo_tecido4, v_subgrupo_tecido4
       from basi_845
       where basi_845.nivel_tecido    = dados_desenho_847.nivel_comp
         and basi_845.grupo_tecido    = dados_desenho_847.grupo_comp
         and basi_845.subgrupo_tecido = dados_desenho_847.subgrupo_comp
         and basi_845.codigo_desenho  = dados_desenho_847.codigo_desenho;
       exception when OTHERS then
         v_grupo_tecido4    := '00000';
         v_subgrupo_tecido4 := '000' ;
      end;
   else
     v_grupo_tecido4    := dados_desenho_847.grupo_comp;
     v_subgrupo_tecido4 := dados_desenho_847.subgrupo_comp;
   end if;

   v_data_lancamento := to_date(null);
    if v_controla_ativacao = 0
   then v_data_lancamento := current_date();
   end if;

   v_cod_contabil_030 := 0;
   v_classific_fiscal := '';

   begin
     select basi_030.codigo_contabil, basi_030.classific_fiscal
     into v_cod_contabil_030, v_classific_fiscal
     from basi_030
     where basi_030.nivel_estrutura = '4'
       and basi_030.referencia      = dados_desenho_847.grupo_comp;
     exception when OTHERS then
       v_cod_contabil_030 := 0;
       v_classific_fiscal := '';
   end;

   v_seq_tamanho := 0;

   begin
     select basi_220.ordem_tamanho
     into v_seq_tamanho
     from basi_220
     where basi_220.tamanho_ref = v_subgrupo_tecido4;
     exception when OTHERS then
       v_seq_tamanho := 0;
   end;

   if v_narrativa_010  is null
   then v_narrativa_010 := ' ';
   end if;

   begin
     insert into basi_010 (
        nivel_estrutura,                                          grupo_estrutura,
        subgru_estrutura,                                         item_estrutura,
        descricao_15,                                             numero_roteiro,
        numero_alternati,                                         narrativa,
        codigo_barras,                                            data_cadastro,
        sequencia_tamanho,                                        codigo_contabil,
        item_ativo,                                               data_lancamento,
        complemento,
        alternativa_custos,                                       roteiro_custos
     ) values (
        '4',                                                      v_grupo_tecido4,
        v_subgrupo_tecido4,                                       v_cor_847_nivel_4,
        substr(dados_desenho_847.descr_combinacao_nivel_4,1,20),  1,
        1,                                                        substr(v_narrativa_010,1,65),
        substr(v_codigo_barra_010,1,15),                          current_date,
        v_seq_tamanho,                                            v_cod_contabil_030,
        v_controla_ativacao,                                      v_data_lancamento,
        p_nome_programa || ' - 08',
        1,                                                        1
     );
     exception
      when dup_val_on_index
      then
        update basi_010
        set basi_010.descricao_15       = substr(dados_desenho_847.descr_combinacao_nivel_4,1,20),
            basi_010.numero_roteiro     = 1,
            basi_010.numero_alternati   = 1,
            basi_010.narrativa          = substr(v_narrativa_010,1,65),
            basi_010.codigo_barras      = substr(v_codigo_barra_010,1,15),
            basi_010.data_cadastro      = current_date,
            basi_010.sequencia_tamanho  = v_seq_tamanho,
            basi_010.codigo_contabil    = v_cod_contabil_030,
            basi_010.item_ativo         = v_controla_ativacao,
            basi_010.data_lancamento    = v_data_lancamento,
            basi_010.complemento        = p_nome_programa || ' - 08',
            basi_010.alternativa_custos = 1,
            basi_010.roteiro_custos     = 1
        where basi_010.nivel_estrutura  = '4'
        and   basi_010.grupo_estrutura  = v_grupo_tecido4
        and   basi_010.subgru_estrutura = v_subgrupo_tecido4
        and   basi_010.item_estrutura   = v_cor_847_nivel_4;
   end;

   begin
     insert into basi_100 (
         cor_sortimento,                                          tipo_cor,
         cor_pantone,                                             sortim_estampa,
         descricao,                                               serie_cor,
         cor_de_fundo
     ) values (
         v_cor_847_nivel_4,                               3,
         dados_desenho_847.codigo_desenho,                dados_desenho_847.comb_desenho_nivel_4,
         substr(dados_desenho_847.descr_combinacao_nivel_4,1,20), v_serie_cor,
         '000000'
     );
     exception
      when dup_val_on_index
      then
         update basi_100
         set   basi_100.descricao      = substr(dados_desenho_847.comb_desenho_nivel_4,1,20),
               basi_100.serie_cor      = v_serie_cor
         where basi_100.cor_sortimento = v_cor_847_nivel_4
           and basi_100.tipo_cor       = 3;
   end;
  end loop;
  return(v_erro_processo);
end;
--------------------------------------------------------------------------------------------------------------------------------------------------------------
--f_painel_estrutura_068
function f_painel_estrutura_068
(
  p_codigo_projeto              varchar2
, p_combinacao_item             varchar2
, p_alternativa                 number
, p_desenho                     varchar
, p_grupo_tecido                varchar2
, p_subgrupo_tecido             varchar2
, p_codigo_empresa              number
) return boolean is

v_centro_custo_845              number(6);
v_consumo_845                   number(13,7);
v_chave_050                     varchar2(25);
v_estagio_preparacao            number(5);
v_erro_processo                 boolean;
v_tipo_desenho_841              number(1);
v_informar_listrado             varchar(1);
v_grupo_tecido4                 varchar2(5);
v_subgrupo_tecido4              varchar2(3);
v_cor_847_nivel_2               varchar2(6);
v_cor_847_nivel_4               varchar2(6);
v_seq_tamanho                   number(1);
v_sequencia                     number(3);
v_intTmp                        number(1);
v_serie_cor_845                 varchar2(6);
v_percentual_perdas_841         number(6,2);
v_percent_perdas                number(6,2);
v_perc_consumo22                number(13,7);
v_centro_custo_013              number(6);
v_estagio_tecelagem             number(5);

begin
  v_erro_processo := false;
  v_tipo_desenho_841 := 0;
  v_estagio_tecelagem := 0;
  v_estagio_preparacao := 0;

  begin
    select empr_001.estagio_tecelagem, empr_001.estagio_prepara
    into v_estagio_tecelagem, v_estagio_preparacao
    from empr_001;
    exception when OTHERS then
      v_estagio_tecelagem := 0;
      v_estagio_preparacao := 0;
  end;

  begin
    select basi_841.tipo_desenho
    into v_tipo_desenho_841
    from basi_841
    where basi_841.codigo_desenho = p_desenho;
    exception
    when OTHERS then
      v_tipo_desenho_841 := 0;
  end;

  v_informar_listrado := 'N';

  begin
    select fatu_503.informar_listrado
    into v_informar_listrado
    from fatu_503
    where fatu_503.codigo_empresa = p_codigo_empresa;
    exception
    when OTHERS then
      v_informar_listrado := 'N';
  end;
v_informar_listrado := 'S';
  for dados_desenho_847_2 in (
  select vbasi_desenhos.nivel_item,            vbasi_desenhos.grupo_item,
         vbasi_desenhos.subgru_item,           vbasi_desenhos.combinacao_item,
         vbasi_desenhos.nivel_comp,            vbasi_desenhos.grupo_comp,
         vbasi_desenhos.tamanho_ref,           vbasi_desenhos.cor,
         vbasi_desenhos.consumo_componente,    vbasi_desenhos.consumo_auxiliar,
         vbasi_desenhos.codigo_desenho,        vbasi_desenhos.comb_desenho,
         vbasi_desenhos.sequencia_estrutura,   vbasi_desenhos.sequencia_variacao,
         vbasi_desenhos.alternativa_comp,      vbasi_desenhos.centro_custo,
         vbasi_desenhos.comb_cor_fundo
  from (select basi_013.nivel_item,            basi_013.grupo_item,
               basi_013.subgru_item,           basi_847.combinacao_item,
               basi_013.nivel_comp,            basi_013.grupo_comp,
               basi_020.tamanho_ref,           basi_847.cor,
               basi_013.consumo_componente,    basi_013.consumo_auxiliar,
               basi_847.codigo_desenho,        basi_847.comb_desenho,
               basi_013.sequencia_estrutura,   basi_013.sequencia_variacao,
               basi_013.alternativa_comp,      basi_013.centro_custo,
               basi_847.comb_desenho  as comb_cor_fundo
        from basi_013, basi_847, basi_020, basi_039
        where basi_013.codigo_projeto      = basi_847.codigo_projeto
          and basi_013.codigo_desenho      = basi_847.codigo_desenho
          and basi_013.alternativa_produto = basi_039.alternativa_produto
          and basi_013.sequencia_estrutura = basi_039.sequencia_estrutura
          and basi_013.sequencia_variacao  = basi_039.sequencia_variacao
          and basi_013.codigo_desenho      <> ' '
          and basi_013.utiliza_componente  = 1
          and basi_013.nivel_comp          = '2'
          and basi_020.basi030_nivel030    = basi_013.nivel_comp
          and basi_020.basi030_referenc    = basi_013.grupo_comp
          and basi_020.tamanho_ref         = basi_013.subgru_comp
          and basi_039.codigo_projeto      = basi_847.codigo_projeto
          and basi_039.combinacao_item     = basi_847.combinacao_item
          and basi_039.alternativa_prod    = basi_847.alternativa_prod
          and basi_039.codigo_desenho      = basi_847.codigo_desenho
          and basi_039.comb_desenho        = basi_847.comb_desenho
          and basi_039.alternativa_produto = p_alternativa
          and basi_039.selecionado         = 1
          and basi_847.codigo_projeto      = p_codigo_projeto
          and basi_847.combinacao_item     = p_combinacao_item
        --  and v_tipo_desenho_841           = 1
        UNION ALL
        select basi_013.nivel_item,            basi_013.grupo_item,
               basi_013.subgru_item,           basi_847.combinacao_item,
               basi_013.nivel_comp,            basi_013.grupo_comp,
               basi_020.tamanho_ref,           basi_847.cor,
               basi_013.consumo_componente,    basi_013.consumo_auxiliar,
               basi_847.codigo_desenho,        basi_847.comb_desenho,
               basi_013.sequencia_estrutura,   basi_013.sequencia_variacao,
               basi_013.alternativa_comp,      basi_013.centro_custo,
               basi_847.comb_desenho  as comb_cor_fundo
        from basi_013, basi_847, basi_020, basi_021, basi_039
        where basi_013.codigo_projeto      = basi_847.codigo_projeto
          and basi_013.codigo_desenho      = basi_847.codigo_desenho
          and basi_013.codigo_desenho      <> ' '
          and basi_013.utiliza_componente  = 1
          and basi_013.nivel_comp          = '2'
          and basi_013.subgru_comp         = '000'
          and basi_013.alternativa_produto = basi_039.alternativa_produto
          and basi_013.sequencia_estrutura = basi_039.sequencia_estrutura
          and basi_013.sequencia_variacao  = basi_039.sequencia_variacao
          and basi_020.basi030_nivel030    = basi_013.nivel_comp
          and basi_020.basi030_referenc    = basi_013.grupo_comp
          and basi_020.tamanho_ref         = basi_021.subgru_comp
          and basi_039.codigo_projeto      = basi_847.codigo_projeto
          and basi_039.combinacao_item     = basi_847.combinacao_item
          and basi_039.alternativa_prod    = basi_847.alternativa_prod
          and basi_039.codigo_desenho      = basi_847.codigo_desenho
          and basi_039.comb_desenho        = basi_847.comb_desenho
          and basi_039.alternativa_produto = p_alternativa
          and basi_039.selecionado         = 1
          and basi_847.codigo_projeto      = p_codigo_projeto
          and basi_847.combinacao_item     = p_combinacao_item
          and basi_021.codigo_projeto      = basi_013.codigo_projeto
          and basi_021.sequencia_projeto   = basi_013.sequencia_projeto
          and basi_021.nivel_item          = basi_013.nivel_item
          and basi_021.grupo_item          = basi_013.grupo_item
          and basi_021.item_item           = basi_013.item_item
          and basi_021.alternativa_produto = basi_013.alternativa_produto
          and basi_021.sequencia_estrutura = basi_013.sequencia_estrutura
          and basi_021.sequencia_variacao  = basi_013.sequencia_variacao
          and basi_021.utiliza_componente  = 1
       --   and v_tipo_desenho_841           = 1
        UNION ALL
        select '0',                           '00000',
               '000',                         '000000',
               basi_845.nivel_tecido,         basi_845.grupo_tecido,
               basi_845.subgrupo_tecido,      '000000',
               1,                             0,
               basi_845.codigo_desenho,       basi844.comb_desenho,
               5,                             5,
               basi_845.alternativa_tecido,   basi_845.centro_custo,
               basi_856.comb_cor_fundo
        from basi_845, basi_856, basi844
        where basi_856.codigo_desenho    = basi844.codigo_desenho
          and basi_856.comb_desenho      = basi844.comb_desenho
          and basi_856.item_ativo        = 1
          and basi844.codigo_desenho    = basi_845.codigo_desenho
          and basi844.tem_cor_fundo     = 1
          and basi_845.codigo_desenho    = p_desenho
          and basi_845.nivel_tecido      = '2'
          and basi_845.grupo_tecido      = p_grupo_tecido
          and basi_845.subgrupo_tecido   = p_subgrupo_tecido
          and basi_845.subgrupo_tecido   <> '000'
       --   and v_tipo_desenho_841          = 2
        group by '0',                           '00000',
                 '000',                         '000000',
                  basi_845.nivel_tecido,         basi_845.grupo_tecido,
                  basi_845.subgrupo_tecido,      '000000',
                  1,                             0,
                  basi_845.codigo_desenho,       basi844.comb_desenho,
                  5,                             5,
                  basi_845.alternativa_tecido,   basi_845.centro_custo,
                  basi_856.comb_cor_fundo
        UNION ALL
           select '0',                           '00000',
                  '000',                         '000000',
                  basi_845.nivel_tecido,         basi_845.grupo_tecido,
                  basi_020.tamanho_ref,          '000000',
                  1,                             0,
                  basi_845.codigo_desenho,       basi844.comb_desenho,
                  5,                             5,
                  basi_845.alternativa_tecido,   basi_845.centro_custo,
                  basi_856.comb_cor_fundo
        from basi_845, basi_856, basi_020, basi844
        where basi_856.codigo_desenho    = basi844.codigo_desenho
          and basi_856.comb_desenho      = basi844.comb_desenho
          and basi_856.item_ativo        = 1
          and basi844.codigo_desenho     = basi_845.codigo_desenho
          and basi844.tem_cor_fundo      = 1
          and basi_845.codigo_desenho    = p_desenho
          and basi_845.nivel_tecido      = '2'
          and basi_845.grupo_tecido      = p_grupo_tecido
          and basi_845.subgrupo_tecido   = p_subgrupo_tecido
          and basi_845.subgrupo_tecido   <> '000'
          and basi_020.basi030_nivel030  = basi_845.nivel_componete
          and basi_020.basi030_referenc  = basi_845.grupo_componente
          and basi_020.tipo_produto      in (3,4)
       --   and v_tipo_desenho_841         = 2
        group by '0',                            '00000',
                 '000',                          '000000',
                  basi_845.nivel_tecido,         basi_845.grupo_tecido,
                  basi_020.tamanho_ref,          '000000',
                  1,                             0,
                  basi_845.codigo_desenho,       basi844.comb_desenho,
                  5,                             5,
                  basi_845.alternativa_tecido,   basi_845.centro_custo,
                  basi_856.comb_cor_fundo
        UNION ALL
        select '0',                           '00000',
               '000',                         '000000',
               basi_845.nivel_tecido,         basi_845.grupo_tecido,
               basi_845.subgrupo_tecido,      '000000',
               1,                             0,
               basi_845.codigo_desenho,       basi844.comb_desenho,
               5,                             5,
               basi_845.alternativa_tecido,   basi_845.centro_custo,
               basi844.comb_desenho
        from basi_845, basi844
        where basi844.codigo_desenho    = basi_845.codigo_desenho
          and basi844.tem_cor_fundo     = 0
          and basi_845.codigo_desenho    = p_desenho
          and basi_845.nivel_tecido      = '2'
          and basi_845.grupo_tecido      = p_grupo_tecido
          and basi_845.subgrupo_tecido   = p_subgrupo_tecido
          and basi_845.subgrupo_tecido   <> '000'
       --  and v_tipo_desenho_841         = 2
        group by '0',                           '00000',
                  '000',                         '000000',
                  basi_845.nivel_tecido,         basi_845.grupo_tecido,
                  basi_845.subgrupo_tecido,      '000000',
                  1,                             0,
                  basi_845.codigo_desenho,       basi844.comb_desenho,
                  5,                             5,
                  basi_845.alternativa_tecido,   basi_845.centro_custo,
                  basi844.comb_desenho
        UNION ALL
        select '0',                           '00000',
               '000',                         '000000',
               basi_845.nivel_tecido,         basi_845.grupo_tecido,
               basi_020.tamanho_ref,          '000000',
               1,                             0,
               basi_845.codigo_desenho,       basi844.comb_desenho,
               5,                             5,
               basi_845.alternativa_tecido,   basi_845.centro_custo,
               basi844.comb_desenho
        from basi_845, basi_020, basi844
        where basi844.codigo_desenho     = basi_845.codigo_desenho
          and basi844.tem_cor_fundo      = 0
          and basi_845.codigo_desenho    = p_desenho
          and basi_845.nivel_tecido      = '2'
          and basi_845.grupo_tecido      = p_grupo_tecido
          and basi_845.subgrupo_tecido   = p_subgrupo_tecido
          and basi_845.subgrupo_tecido   <> '000'
          and basi_020.basi030_nivel030  = basi_845.nivel_componete
          and basi_020.basi030_referenc  = basi_845.grupo_componente
          and basi_020.tipo_produto      in (3,4)
       --   and v_tipo_desenho_841        = 2
        group by '0',                            '00000',
                 '000',                          '000000',
                  basi_845.nivel_tecido,         basi_845.grupo_tecido,
                  basi_020.tamanho_ref,          '000000',
                  1,                             0,
                  basi_845.codigo_desenho,       basi844.comb_desenho,
                  5,                             5,
                  basi_845.alternativa_tecido,   basi_845.centro_custo,
                  basi844.comb_desenho
  ) vbasi_desenhos
  group by vbasi_desenhos.nivel_item,            vbasi_desenhos.grupo_item,
           vbasi_desenhos.subgru_item,           vbasi_desenhos.combinacao_item,
           vbasi_desenhos.nivel_comp,            vbasi_desenhos.grupo_comp,
           vbasi_desenhos.tamanho_ref,           vbasi_desenhos.cor,
           vbasi_desenhos.consumo_componente,    vbasi_desenhos.consumo_auxiliar,
           vbasi_desenhos.codigo_desenho,        vbasi_desenhos.comb_desenho,
           vbasi_desenhos.sequencia_estrutura,   vbasi_desenhos.sequencia_variacao,
           vbasi_desenhos.alternativa_comp,      vbasi_desenhos.centro_custo,
           vbasi_desenhos.comb_cor_fundo
  ) loop
    if v_informar_listrado = 'S'
    then
      begin
      select min(basi_845.grupo_produto), min(basi_845.subgrupo_produto)
      into v_grupo_tecido4, v_subgrupo_tecido4
      from basi_845
      where basi_845.nivel_tecido    = dados_desenho_847_2.nivel_comp
        and basi_845.grupo_tecido    = dados_desenho_847_2.grupo_comp
        and basi_845.subgrupo_tecido = dados_desenho_847_2.tamanho_ref
        and basi_845.codigo_desenho  = dados_desenho_847_2.codigo_desenho;
      exception when OTHERS then
        v_grupo_tecido4    := '00000';
        v_subgrupo_tecido4 := '000' ;
      end;
    else
      v_grupo_tecido4    := dados_desenho_847_2.grupo_comp;
      v_subgrupo_tecido4 := dados_desenho_847_2.tamanho_ref;
    end if;

    if v_tipo_desenho_841 = 1
    then
      v_cor_847_nivel_2 := dados_desenho_847_2.codigo_desenho || dados_desenho_847_2.comb_desenho;
      v_cor_847_nivel_4 := dados_desenho_847_2.codigo_desenho || dados_desenho_847_2.comb_desenho;
    else
      v_cor_847_nivel_2 := dados_desenho_847_2.codigo_desenho || dados_desenho_847_2.comb_cor_fundo;
      v_cor_847_nivel_4 := dados_desenho_847_2.codigo_desenho || dados_desenho_847_2.comb_desenho;
    end if;

    begin
      select min(basi_845.centro_custo), min(basi_845.consumo_produto)
      into v_centro_custo_845, v_consumo_845
      from basi_845
      where  basi_845.codigo_desenho   = dados_desenho_847_2.codigo_desenho
        and  basi_845.nivel_produto    = '4'
        and  basi_845.grupo_produto    = v_grupo_tecido4
        and (basi_845.subgrupo_produto = v_subgrupo_tecido4
        or   basi_845.subgrupo_produto = '000');
      exception when OTHERS then
        v_centro_custo_845 := 0;
        v_consumo_845 := 0 ;
    end;

    v_chave_050 := dados_desenho_847_2.codigo_desenho || '-' || dados_desenho_847_2.nivel_comp || '.' || dados_desenho_847_2.grupo_comp || '.' ||
                   dados_desenho_847_2.tamanho_ref  || '.' || v_cor_847_nivel_2 || '-' || dados_desenho_847_2.alternativa_comp;

    if f_painel_estrutura_062(v_chave_050) = true
    then
      v_erro_processo := true;
    end if;

    begin
      insert into basi_050 (
        nivel_item,                                  grupo_item,
        sub_item,                                    item_item,
        alternativa_item,                            sequencia,
        nivel_comp,                                  grupo_comp,
        sub_comp,                                    item_comp,
        alternativa_comp,                            consumo,
        cons_unid_med_generica,                      estagio,
        codigo_projeto,                              sequencia_projeto,
        centro_custo
      ) values (
        dados_desenho_847_2.nivel_comp,              dados_desenho_847_2.grupo_comp,
        dados_desenho_847_2.tamanho_ref,             v_cor_847_nivel_2,
        dados_desenho_847_2.alternativa_comp,        1,
        '4',                                         v_grupo_tecido4,
        v_subgrupo_tecido4,                          v_cor_847_nivel_4,
        dados_desenho_847_2.alternativa_comp,        v_consumo_845,
        1,                                           v_estagio_preparacao,
        v_chave_050,                                 999,
        v_centro_custo_845
      );
      exception
        when dup_val_on_index
          then
            UPDATE basi_050
            set   basi_050.nivel_comp             = '4',
                  basi_050.grupo_comp             = v_grupo_tecido4,
                  basi_050.sub_comp               = v_subgrupo_tecido4,
                  basi_050.item_comp              = v_cor_847_nivel_4,
                  basi_050.alternativa_comp       = dados_desenho_847_2.alternativa_comp,
                  basi_050.consumo                = v_consumo_845,
                  basi_050.centro_custo           = v_centro_custo_845,
                  basi_050.estagio                = v_estagio_preparacao,
                  basi_050.codigo_projeto         = v_chave_050,
                  basi_050.sequencia_projeto      = 999
            where basi_050.nivel_item             = dados_desenho_847_2.nivel_comp
              and basi_050.grupo_item             = dados_desenho_847_2.grupo_comp
              and basi_050.sub_item               = dados_desenho_847_2.tamanho_ref
              and basi_050.item_item              = v_cor_847_nivel_2
              and basi_050.alternativa_item       = dados_desenho_847_2.alternativa_comp
              and basi_050.sequencia              = 1;

            begin
            UPDATE basi_040
            set basi_040.sequencia_projeto   = 999
            where basi_040.nivel_item        = dados_desenho_847_2.nivel_comp
              and basi_040.grupo_item        = dados_desenho_847_2.grupo_comp
              and basi_040.alternativa_item  = dados_desenho_847_2.alternativa_comp
              and basi_040.sequencia         = 1
              and basi_040.codigo_projeto    = v_chave_050
              and basi_040.sequencia_projeto = -999;
            exception when OTHERS then
              v_erro_processo := true;
          end;
    end;

    if f_painel_estrutura_065(dados_desenho_847_2.codigo_desenho,    dados_desenho_847_2.grupo_comp,
                              dados_desenho_847_2.tamanho_ref,       v_cor_847_nivel_2,
                              dados_desenho_847_2.alternativa_comp,  v_chave_050,
                              p_codigo_empresa) = true
    then
      v_erro_processo := true;
    end if;

    if v_tipo_desenho_841 = 1
    then
    begin
      select 1
      into v_intTmp
      from basi_851
      where basi_851.codigo_projeto = p_codigo_projeto;
      exception when OTHERS then
        v_seq_tamanho := 0;

        begin
          select basi_220.ordem_tamanho
          into v_seq_tamanho
          from basi_220
          where basi_220.tamanho_ref = dados_desenho_847_2.tamanho_ref;
          exception when OTHERS then
            v_seq_tamanho := 0;
        end;

        begin
          insert into basi_021 (
             codigo_projeto,                     sequencia_projeto,
             alternativa_produto,                nivel_item,
             grupo_item,                         subgru_item,
             item_item,                          sequencia_estrutura,
             sequencia_variacao,
             consumo_componente,                 item_comp,
             subgru_comp,                        ordem_tamanho
          ) VALUES (
             p_codigo_projeto,                      1,
             dados_desenho_847_2.alternativa_comp,  dados_desenho_847_2.nivel_item,
             dados_desenho_847_2.grupo_item,        dados_desenho_847_2.subgru_item,
             dados_desenho_847_2.combinacao_item,   dados_desenho_847_2.sequencia_estrutura,
             dados_desenho_847_2.sequencia_variacao,
             1,                                     v_cor_847_nivel_2,
             dados_desenho_847_2.tamanho_ref,       v_seq_tamanho
          );
          exception when OTHERS then
            v_erro_processo := true;
        end;
      end;
    else
      for dados_desenho3 in (
      select basi_013.nivel_item,                basi_013.grupo_item,
             basi_013.subgru_item,               basi_013.item_item,
             basi_013.sequencia_estrutura,       basi_013.sequencia_variacao,
             basi_013.alternativa_produto,
             basi_851.codigo_destino,            basi_013.subgru_comp,
             basi_013.utiliza_componente,        basi_013.codigo_desenho,
             basi_847.comb_desenho
      from basi_013, basi_847, basi_020, basi_851
      where basi_847.codigo_projeto      = basi_013.codigo_projeto
        and basi_847.codigo_desenho      = basi_013.codigo_desenho
        and basi_013.codigo_desenho      <> ' '
        and basi_013.alternativa_produto = p_alternativa
        and basi_013.nivel_comp          = '2'
        and basi_013.utiliza_componente  = 1
        and basi_020.basi030_nivel030    = basi_013.nivel_comp
        and basi_020.basi030_referenc    = basi_013.grupo_comp
        and basi_847.codigo_projeto      = p_codigo_projeto
        and basi_847.combinacao_item     = p_combinacao_item
        and basi_851.codigo_projeto(+)   = basi_847.codigo_projeto
      ) loop
        dados_desenho3.item_item := dados_desenho3.codigo_destino || substr(p_combinacao_item,3,6);
        v_seq_tamanho := 0;

        begin
          select basi_220.ordem_tamanho
          into v_seq_tamanho
          from basi_220
          where basi_220.tamanho_ref = dados_desenho3.subgru_comp;
          exception when OTHERS then
            v_seq_tamanho := 0;
        end;

        begin
          insert into basi_021 (
            codigo_projeto,                     sequencia_projeto,
            alternativa_produto,                nivel_item,
            grupo_item,                         subgru_item,
            item_item,                          sequencia_estrutura,
            sequencia_variacao,
            item_comp,                          subgru_comp,
            utiliza_componente,                 ordem_tamanho
          ) VALUES (
            p_codigo_projeto,                                                           1,
            dados_desenho3.alternativa_produto,                                         dados_desenho3.nivel_item,
            dados_desenho3.grupo_item,                                                  dados_desenho3.subgru_item,
            dados_desenho3.item_item,                                                   dados_desenho3.sequencia_estrutura,
            dados_desenho3.sequencia_variacao,
            dados_desenho3.codigo_desenho || dados_desenho3.comb_desenho,               dados_desenho3.subgru_comp,
            dados_desenho3.utiliza_componente,                                          v_seq_tamanho
          );
          exception when OTHERS then
            v_erro_processo := true;
        end;
      end loop;
    end if;

    v_sequencia := 0;

    for dados_desenho4 in (
    select basi_845.nivel_componete,        basi_845.grupo_componente,
           basi_845.subgrupo_componente,    basi_848.cor,
           basi_848.seq_cor,                basi_845.consumo_fio
    from basi_845, basi_848
    where  basi_845.codigo_desenho   = basi_848.codigo_desenho
      and  basi_848.comb_desenho     = dados_desenho_847_2.comb_desenho
      and  basi_845.nivel_produto    = '4'
      and  basi_845.grupo_produto    = v_grupo_tecido4
      and (basi_845.subgrupo_produto = v_subgrupo_tecido4  or basi_845.subgrupo_produto = '000')
      and  basi_845.nivel_tecido     = '2'
      and  basi_845.grupo_tecido     = dados_desenho_847_2.grupo_comp
      and  basi_845.subgrupo_tecido  = dados_desenho_847_2.tamanho_ref
      and  basi_845.codigo_desenho   = dados_desenho_847_2.codigo_desenho
      and  basi_845.seq_cor          = basi_848.seq_cor
      --and  basi_845.comb_desenho     = '00'
    order by basi_848.seq_cor
    ) loop
      v_sequencia := v_sequencia + 5;

      begin
        select basi_100.serie_cor
        into v_serie_cor_845
        from basi_100
        where basi_100.cor_sortimento   = dados_desenho4.cor
          and basi_100.tipo_cor         = 1;
        exception when OTHERS then
            v_serie_cor_845 := 0;
      end;

      v_percentual_perdas_841 := 0.00;

      begin
        select basi_841.percentual_perdas
        into v_percentual_perdas_841
        from basi_841
        where basi_841.codigo_desenho = dados_desenho_847_2.codigo_desenho;
        exception when OTHERS then
            v_percentual_perdas_841 := 0.00;
      end;

      v_percent_perdas := v_percentual_perdas_841;

      v_percentual_perdas_841 := (1 + (v_percentual_perdas_841 / 100));

      v_perc_consumo22 := 0.00;

      v_perc_consumo22 := dados_desenho4.consumo_fio * 100;

      dados_desenho4.consumo_fio := dados_desenho4.consumo_fio * v_percentual_perdas_841;

      if dados_desenho_847_2.centro_custo is null
      then
         v_centro_custo_013 := 0;
      end if;

      begin
        insert into basi_050 (
          nivel_item,                      grupo_item,
          sub_item,                        item_item,
          alternativa_item,                sequencia,
          nivel_comp,                      grupo_comp,
          sub_comp,                        item_comp,
          alternativa_comp,                consumo,
          cons_unid_med_generica,          estagio,
          percent_perdas,                  codigo_projeto,
          sequencia_projeto,               perc_cons_calc,
          centro_custo
        ) values (
          '4',                                    v_grupo_tecido4,
          v_subgrupo_tecido4,                     v_cor_847_nivel_4,
          dados_desenho_847_2.alternativa_comp,   v_sequencia,
          dados_desenho4.nivel_componete,         dados_desenho4.grupo_componente,
          dados_desenho4.subgrupo_componente,     dados_desenho4.cor,
          dados_desenho_847_2.alternativa_comp,   dados_desenho4.consumo_fio,
          dados_desenho4.consumo_fio,             v_estagio_tecelagem,
          v_percent_perdas,                       v_chave_050,
          999,                                    v_perc_consumo22,
          v_centro_custo_013
        );
        exception
        when dup_val_on_index
        then
          UPDATE basi_050
          set   basi_050.nivel_comp             = dados_desenho4.nivel_componete,
                basi_050.grupo_comp             = dados_desenho4.grupo_componente,
                basi_050.sub_comp               = dados_desenho4.subgrupo_componente,
                basi_050.item_comp              = dados_desenho4.cor,
                basi_050.alternativa_comp       = dados_desenho_847_2.alternativa_comp,
                basi_050.consumo                = dados_desenho4.consumo_fio,
                basi_050.centro_custo           = v_centro_custo_013,
                basi_050.estagio                = v_estagio_tecelagem,
                basi_050.codigo_projeto         = v_chave_050,
                basi_050.sequencia_projeto      = 999,
                basi_050.perc_cons_calc         = v_perc_consumo22,
                basi_050.percent_perdas         = v_percent_perdas
          where basi_050.nivel_item             = '4'
            and basi_050.grupo_item             = v_grupo_tecido4
            and basi_050.sub_item               = v_subgrupo_tecido4
            and basi_050.item_item              = v_cor_847_nivel_4
            and basi_050.alternativa_item       = dados_desenho_847_2.alternativa_comp
            and basi_050.sequencia              = v_sequencia;

          begin
            UPDATE basi_040
            set basi_040.sequencia_projeto   = 999
            where basi_040.nivel_item        = '4'
              and basi_040.grupo_item        = v_grupo_tecido4
              and basi_040.alternativa_item  = dados_desenho_847_2.alternativa_comp
              and basi_040.sequencia         = v_sequencia
              and basi_040.codigo_projeto    = v_chave_050
              and basi_040.sequencia_projeto = -999;
             exception when OTHERS then
               v_erro_processo := true;
          end;
      end;
    end loop;

    ----------------------------------------

    v_sequencia := 0;

    for dados_desenho7 in (
    select    basi_846.nivel_produto,           basi_846.grupo_produto,
              basi_846.subgrupo_produto,        basi_848.cor,           basi_846.alternativa_comp, 
              basi_846.nivel_comp,              basi_846.grupo_comp,
              basi_846.sub_comp,                basi_846.item_comp,
              basi_848.seq_cor,                 basi_846.consumo,
              basi_846.estagio,                 basi_846.centro_custo       
        from basi_846, basi_848
        where  basi_846.codigo_desenho   = basi_848.codigo_desenho
          and  basi_848.comb_desenho     = dados_desenho_847_2.comb_desenho
          and  basi_846.nivel_produto    = '7'
          and  basi_846.nivel_comp       = '7'
          and  basi_846.codigo_desenho   = dados_desenho_847_2.codigo_desenho
          and  basi_846.seq_cor          = basi_848.seq_cor
        order by basi_848.seq_cor    
    ) loop
      v_sequencia := v_sequencia + 5;

      begin
        insert into basi_050 (
          nivel_item,                      grupo_item,
          sub_item,                        item_item,
          alternativa_item,                sequencia,
          nivel_comp,                      grupo_comp,
          sub_comp,                        item_comp,
          alternativa_comp,                consumo,
          cons_unid_med_generica,          estagio,
          percent_perdas,                  codigo_projeto,
          sequencia_projeto,               perc_cons_calc,
          centro_custo
        ) values (
          '7',                                    dados_desenho7.grupo_produto,
          dados_desenho7.subgrupo_produto,        dados_desenho7.cor,
          v_alternativa,                          v_sequencia,
          dados_desenho7.nivel_comp,              dados_desenho7.grupo_comp,
          dados_desenho7.sub_comp,                dados_desenho7.item_comp,
          dados_desenho7.alternativa_comp,        dados_desenho7.consumo,
          dados_desenho7.consumo,                 dados_desenho7.estagio,
          0,                                      0,
          999,                                    0,
          dados_desenho7.centro_custo
        );
        exception
        when dup_val_on_index
        then
          UPDATE basi_050
          set   basi_050.nivel_comp             = dados_desenho7.nivel_comp,
                basi_050.grupo_comp             = dados_desenho7.grupo_comp,
                basi_050.sub_comp               = dados_desenho7.sub_comp,
                basi_050.item_comp              = dados_desenho7.item_comp,
                basi_050.alternativa_comp       = dados_desenho7.alternativa_comp,
                basi_050.consumo                = dados_desenho7.consumo,
                basi_050.centro_custo           = dados_desenho7.centro_custo,
                basi_050.estagio                = dados_desenho7.estagio,
                basi_050.codigo_projeto         = 0,
                basi_050.sequencia_projeto      = 999,
                basi_050.perc_cons_calc         = 0,
                basi_050.percent_perdas         = 0
          where basi_050.nivel_item             = '7'
            and basi_050.grupo_item             = dados_desenho7.grupo_produto
            and basi_050.sub_item               = dados_desenho7.subgrupo_produto
            and basi_050.item_item              = dados_desenho7.cor
            and basi_050.alternativa_item       = v_alternativa
            and basi_050.sequencia              = v_sequencia;

          begin
            UPDATE basi_040
            set basi_040.sequencia_projeto   = 999
            where basi_040.nivel_item        = '7'
              and basi_040.grupo_item        = dados_desenho7.grupo_produto
              and basi_040.alternativa_item  = v_alternativa
              and basi_040.sequencia         = v_sequencia
              and basi_040.codigo_projeto    = 0
              and basi_040.sequencia_projeto = -999;
             exception when OTHERS then
               v_erro_processo := true;
          end;
      end;
    end loop;

    --------------------------------------------

  end loop;
  return(v_erro_processo);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------------------
--principal
begin
  v_alternativa := p_alternativa;
  
  if p_alternativa is null
  then
    v_alternativa := 1;
  end if;
  
  begin
      update basi_845 
      set alternativa_tecido = 1
      where codigo_desenho = p_desenho
      and   alternativa_tecido is null;
  end;
  
  v_grupo_tecido    := p_grupo_tecido;
  v_subgrupo_tecido := p_subgrupo_tecido;

  begin
    select dbms_utility.get_hash_value(to_char(dbms_utility.get_time),100,9999)
    into v_nr_solicitacao
    from dual;
    exception when others then
      v_nr_solicitacao := 0;
  end;

  -- 1
  v_desfazer_transacao := f_painel_estrutura_060('00000',          '000000',
                                                 v_alternativa,    'basi_f845',
                                                 v_nr_solicitacao, p_desenho,
                                                 v_grupo_tecido,   v_subgrupo_tecido);                                                 
  -- 2
  v_desfazer_transacao := f_painel_estrutura_061('00000',          '000000',
                                                 v_alternativa,    p_desenho,
                                                 v_grupo_tecido,   v_subgrupo_tecido,
                                                 'basi_f845');
  -- 3
  v_desfazer_transacao := f_painel_estrutura_068('000000',
                                                 '000000',                    v_alternativa,
                                                 p_desenho,                   v_grupo_tecido,
                                                 v_subgrupo_tecido,           p_codigo_empresa);

  commit;
end inter_pr_atualiza_desenho;
