create or replace TRIGGER INTER_TR_PEDI_100_7
    before update on pedi_100               
    for each row  
declare
    v_cod_catalogo pedi_090.COD_CATALOGO%TYPE;
begin   
  
  if :new.colecao_tabela > 0
  then
    begin
       select pedi_090.cod_catalogo
       into v_cod_catalogo
       from pedi_090
       where col_tabela_preco = :new.colecao_tabela
       and   mes_tabela_preco = :new.mes_tabela
       and   seq_tabela_preco = :new.sequencia_tabela;
    exception
    when OTHERS then
       v_cod_catalogo := 0;
    end;
  
    :new.cod_catalogo := v_cod_catalogo;
  end if;
  
end;
