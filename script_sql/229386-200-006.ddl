create table blocok_250 (
	COD_EMPRESA             number(3),
	ANO_PERIODO_APUR        number(4),
	MES_PERIODO_APUR        number(2),
	
	CENARIO                 varchar2(5),
	ORIGEM                  varchar2(20),
	ID_DOC_ORIG             varchar2(200),
	COD_EMP_DOC_ORIG        number(3),
	SERIE_DOC_ORIG          varchar2(3),
	NUM_DOC_ORIG            number(9),
	CGC9_DOC_ORIG           number(9),
	CGC4_DOC_ORIG           number(4),
	CGC2_DOC_ORIG           number(2),
	SEQ_ITE_DOC_ORIG        number(9),
	
	COD_ITEM_NIVEL          varchar2(1),
	COD_ITEM_GRUPO          varchar2(5),
	COD_ITEM_SUBGRUPO       varchar2(3),
	COD_ITEM_ITEM           varchar2(6),
	COD_ITEM_EST_AGRUP      number(2),
	ALTERNATIVA_OP          number(2),

    ordem_producao          number(9),
    periodo                 number(4),
    pacote                  number(5),
    codigo_estagio          number(2),

	DT_PROD                 varchar2(8),
	COD_ITEM                varchar2(60),
	QTD                     number(15,6),
	
	data_hora_ins           date            default sysdate
);
