INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'obrf_f058',     0,
	1,			     'Demonstrativo dos valores devidos aos fundos de benefício fiscal');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'obrf_f058',   'nenhum', 
	0,             1, 
	'S',           'S', 
	'S',           'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Demonstrativo dos valores devidos aos fundos de benefício fiscal'
 WHERE hdoc_036.codigo_programa = 'obrf_f058'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;



INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'obrf_f063',     0,
	1,			     'Demonstrativo da apuração de valores devidos ou saldo credor de fundos');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'obrf_f063',   'nenhum', 
	0,             1, 
	'S',           'S', 
	'S',           'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Demonstrativo da apuração de valores devidos ou saldo credor de fundos'
 WHERE hdoc_036.codigo_programa = 'obrf_f063'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;




INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'obrf_f064',     0,
	1,			     'Discriminação das contribuições ao fia e fei devidas no exercício anterior');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'obrf_f064',   'nenhum', 
	0,             1, 
	'S',           'S', 
	'S',           'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Discriminação das contribuições ao fia e fei devidas no exercício anterior'
 WHERE hdoc_036.codigo_programa = 'obrf_f064'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;
