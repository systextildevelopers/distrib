create or replace PACKAGE ST_PCK_TRANSACAO AS

    type t_dados_estq_005 is table of estq_005%rowtype;

    function get(p_codigo_transacao number, p_msg_erro in out varchar2) return t_dados_estq_005;

    FUNCTION transacao_gera_contabilizacao(p_transacao IN NUMBER, p_validar_por_contab_cardex IN BOOLEAN, p_nome_form IN VARCHAR2) RETURN BOOLEAN;

END ST_PCK_TRANSACAO;
