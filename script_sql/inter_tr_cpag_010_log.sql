
  CREATE OR REPLACE TRIGGER "INTER_TR_CPAG_010_LOG" 
   after insert or
         delete or
         update of nr_duplicata,          parcela,              cgc_9,
                   cgc_4,                 cgc_2,                tipo_titulo,
                   codigo_empresa,        cgc9_favorecido,      cgc4_favorecido,
                   cgc2_favorecido,       cgc9_subs,            cgc4_subs,
                   cgc2_subs,             cod_cancelamento,     cod_portador,
                   codigo_depto,          codigo_historico,     codigo_transacao,
                   data_canc_tit,         data_contrato,        data_digitacao,
                   data_real_pagto,       data_transacao,       data_vencimento,
                   data_vencto_original,  moeda_titulo,         num_tit_subs,
                   numero_adiantam,       numero_cheque,        saldo_titulo,
                   sit_bloqueio,          situacao,             situacao_sispag,
                   tipo_tit_subs,         usuario_digitacao,    valor_abatimento,
                   valor_cheque,          valor_desc,           valor_desconto_cheque,
                   valor_juros,           valor_juros_cheque,   valor_moeda,
                   valor_parcela,         codigo_contabil,      lote,
                   data_lote,             executa_trigger,      codigo_barras,
                   observacao_libercaco,  status_monkey
   on cpag_010
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   long_aux                  varchar2(4000);
   old_observacao_libercaco  varchar2(4000);


begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

      if inserting
      then
         INSERT INTO hist_100
            ( tabela,                         operacao,
              data_ocorr,                     usuario_rede,
              maquina_rede,                   aplicacao,
              num01,                          num02,
              num03,                          num04,
              num05,                          str01,
              long01
            )
         VALUES
            ( 'CPAG_010',                     'I',
              sysdate,                        ws_usuario_rede,
              ws_maquina_rede,                ws_aplicativo,
              :new.nr_duplicata,              :new.cgc_9,
              :new.cgc_4,                     :new.cgc_2,
              :new.tipo_titulo,               :new.parcela,
              '                                                  ' ||
              inter_fn_buscar_tag('ds26571#TITULOS A PAGAR',ws_locale_usuario,ws_usuario_systextil) ||
              chr(10)                                                             ||
              chr(10)                                                             ||
              inter_fn_buscar_tag('lb31254#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                          :new.cgc_9        || '/' ||
                                          :new.cgc_4        || '-' ||
                                          :new.cgc_2        ||
                                          chr(10) ||
              inter_fn_buscar_tag('ds23004#EMPRESA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                             :new.codigo_empresa ||
                                             chr(10) ||
              inter_fn_buscar_tag('lb03747#FAVORECIDO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.cgc9_favorecido || '/' ||
                                            :new.cgc4_favorecido || '-' ||
                                            :new.cgc2_favorecido ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb03500#EMITENTE SUBSTITUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.cgc9_subs || '/' ||
                                            :new.cgc4_subs || '-' ||
                                            :new.cgc2_subs ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb31282#COD. CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.cod_cancelamento ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb05818#PORTADOR DO TITULO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.cod_portador ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb00038#CENTRO CUSTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.codigo_depto ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb34828#HISTORICO TITULO',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.codigo_historico ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb34829#TRANSACAO DO TITULO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.codigo_transacao ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb31283#DATA CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.data_canc_tit ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb02872#DATA CONTRATO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.data_contrato ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb02908#DATA DIGITACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.data_digitacao ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb02981#DATA PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.data_real_pagto ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb03029#DATA TRANSACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.data_transacao ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb17668#DATA VENCIMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.data_vencto_original ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb22852#DATA PRORROGACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.data_vencimento ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb13146#MOEDA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.moeda_titulo ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb05518#TITULO SUBSTITUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.num_tit_subs ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb05494#TIPO SUBSTITUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.tipo_tit_subs ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb05344#NUMERO DO ADIANTAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.numero_adiantam ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb13245#NR.CHEQUE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.numero_cheque ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb34830#VALOR SALDO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.saldo_titulo ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb05458#SIT. BLOQUEIO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.sit_bloqueio ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.situacao ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb00348#USUARIO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.usuario_digitacao ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb01604#ABATIMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.valor_abatimento ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb13246#VALOR CHEQUE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.valor_cheque ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb07929#VALOR DESCONTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.valor_desc ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb34831#VALOR DESCONTO CHEQUE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.valor_desconto_cheque ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb31928#VALOR JUROS:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.valor_juros ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb34832#VALOR JUROS CHEQUE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.valor_juros_cheque ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb05544#VALOR MOEDA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.valor_moeda ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb05547#VALOR PARCELA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.valor_parcela ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb36826#SITUACAO DO PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.situacao_sispag ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb00585#CODIGO CONTABIL:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.codigo_contabil ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb00236#LOTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.lote ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb42094#DATA LOTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.data_lote ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb02581#C?DIGO BARRAS:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.codigo_barras ||
                                            chr(10) ||
              'EXECUTA TRIGGER' || ' ' ||
                                            :new.executa_trigger || '' ||
                                            'STATUS MONKEY:  ' || :new.status_monkey

           );
      end if;

      if updating  and
         (:old.nr_duplicata           <>  :new.nr_duplicata           or
          :old.parcela                <>  :new.parcela                or
          :old.cgc_9                  <>  :new.cgc_9                  or
          :old.cgc_4                  <>  :new.cgc_4                  or
          :old.cgc_2                  <>  :new.cgc_2                  or
          :old.tipo_titulo            <>  :new.tipo_titulo             or
          (:new.codigo_empresa is null  or :old.codigo_empresa is null 
           or :old.codigo_empresa        <>  :new.codigo_empresa)         or
          :old.cgc9_favorecido        <>  :new.cgc9_favorecido        or
          :old.cgc4_favorecido        <>  :new.cgc4_favorecido        or
          :old.cgc2_favorecido        <>  :new.cgc2_favorecido        or
          :old.cgc9_subs              <>  :new.cgc9_subs              or
          :old.cgc4_subs              <>  :new.cgc4_subs              or
          :old.cgc2_subs              <>  :new.cgc2_subs              or
          :old.cod_cancelamento       <>  :new.cod_cancelamento       or
          :old.cod_portador           <>  :new.cod_portador           or
          :old.codigo_depto           <>  :new.codigo_depto           or
          :old.codigo_historico       <>  :new.codigo_historico       or
          :old.codigo_transacao       <>  :new.codigo_transacao       or
          :old.data_canc_tit          <>  :new.data_canc_tit          or
          :old.data_contrato          <>  :new.data_contrato          or
          :old.data_digitacao         <>  :new.data_digitacao         or
          :old.data_real_pagto        <>  :new.data_real_pagto        or
          :old.data_transacao         <>  :new.data_transacao         or
          :old.data_vencimento        <>  :new.data_vencimento        or
          :old.data_vencto_original   <>  :new.data_vencto_original   or
          :old.moeda_titulo           <>  :new.moeda_titulo           or
          :old.num_tit_subs           <>  :new.num_tit_subs           or
          :old.numero_adiantam        <>  :new.numero_adiantam        or
          :old.numero_cheque          <>  :new.numero_cheque          or
          :old.saldo_titulo           <>  :new.saldo_titulo           or
          :old.sit_bloqueio           <>  :new.sit_bloqueio           or
          :old.situacao               <>  :new.situacao               or
          :old.situacao_sispag        <>  :new.situacao_sispag        or
          :old.tipo_tit_subs          <>  :new.tipo_tit_subs          or
          :old.usuario_digitacao      <>  :new.usuario_digitacao      or
          :old.valor_abatimento       <>  :new.valor_abatimento       or
          :old.valor_cheque           <>  :new.valor_cheque           or
          :old.valor_desc             <>  :new.valor_desc             or
          :old.valor_desconto_cheque  <>  :new.valor_desconto_cheque  or
          :old.valor_juros            <>  :new.valor_juros            or
          :old.valor_juros_cheque     <>  :new.valor_juros_cheque     or
          :old.valor_moeda            <>  :new.valor_moeda            or
          :old.valor_parcela          <>  :new.valor_parcela          or
          :old.codigo_contabil        <>  :new.codigo_contabil        or
          :old.lote                   <>  :new.lote                   or
          :old.data_lote              <>  :new.data_lote              or
          :old.executa_trigger        <>  :new.executa_trigger        or
          :old.codigo_barras          <>  :new.codigo_barras          or
          :new.valor_parcela is null                                  or
          :new.valor_moeda is null                                    or
          :old.valor_parcela is null                                  or
          :old.valor_moeda is null                                   or 
          :old.status_monkey      <>   :new.status_monkey)
      then
         long_aux := long_aux ||
             '                                                  ' ||
             inter_fn_buscar_tag('ds26571#TITULOS A PAGAR',ws_locale_usuario,ws_usuario_systextil) ||
             chr(10)                                        ||
             chr(10);
         if :old.cgc_9 <> :new.cgc_9 or
            :old.cgc_4 <> :new.cgc_4 or
            :old.cgc_2 <> :new.cgc_2
         then
             long_aux := long_aux ||
                inter_fn_buscar_tag('lb31254#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                          :old.cgc_9        || '/' ||
                                          :old.cgc_4        || '-' ||
                                          :old.cgc_2        || ' -> ' ||
                                          :new.cgc_9        || '/' ||
                                          :new.cgc_4        || '-' ||
                                          :new.cgc_2        ||
                                          chr(10);
         end if;

         if (:new.codigo_empresa is null or :old.codigo_empresa is null or :old.codigo_empresa        <>  :new.codigo_empresa)
         then
             long_aux := long_aux ||
                inter_fn_buscar_tag('ds23004#EMPRESA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.codigo_empresa || ' -> ' ||
                                            :new.codigo_empresa ||
                                             chr(10);
         end if;

         if :old.cgc9_favorecido <> :new.cgc9_favorecido or
            :old.cgc4_favorecido <> :new.cgc4_favorecido or
            :old.cgc2_favorecido <> :new.cgc2_favorecido
         then
             long_aux := long_aux        ||
              'FAVORECIDO: '             || :old.cgc9_favorecido || '/' ||
                                            :old.cgc4_favorecido || '-' ||
                                            :old.cgc2_favorecido || ' -> ' ||
                                            :new.cgc9_favorecido || '/' ||
           :new.cgc4_favorecido || '-' ||
                                            :new.cgc2_favorecido ||
                                            chr(10);
         end if;

         if :old.cgc9_subs <> :new.cgc9_subs or
            :old.cgc4_subs <> :new.cgc4_subs or
            :old.cgc2_subs <> :new.cgc2_subs
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb03500#EMITENTE SUBSTITUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.cgc9_subs || '/' ||
                                            :old.cgc4_subs || '-' ||
                                            :old.cgc2_subs || ' -> ' ||
                                            :new.cgc9_subs || '/' ||
           :new.cgc4_subs || '-' ||
                                            :new.cgc2_subs ||
                                            chr(10);
         end if;

         if :old.cod_cancelamento <> :new.cod_cancelamento
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb31282#COD. CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.cod_cancelamento || ' -> ' ||
                                            :new.cod_cancelamento ||
                                            chr(10);
         end if;

         if :old.cod_portador <> :new.cod_portador
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb05818#PORTADOR DO TITULO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.cod_portador || ' -> ' ||
                                            :new.cod_portador ||
                                            chr(10);
         end if;

         if :old.codigo_depto <> :new.codigo_depto
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb00038#CENTRO CUSTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.codigo_depto || ' -> ' ||
                                            :new.codigo_depto ||
                                            chr(10);
         end if;

         if :old.codigo_historico <> :new.codigo_historico
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb34828#HISTORICO TITULO',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.codigo_historico || ' -> ' ||
                                            :new.codigo_historico ||
                                            chr(10);
         end if;

         if :old.codigo_transacao <> :new.codigo_transacao
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb34829#TRANSACAO DO TITULO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.codigo_transacao || ' -> ' ||
                                            :new.codigo_transacao ||
                                            chr(10);
         end if;

         if :old.data_canc_tit <> :new.data_canc_tit
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb31283#DATA CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.data_canc_tit || ' -> ' ||
                                            :new.data_canc_tit ||
                                            chr(10);
         end if;

         if :old.data_contrato <> :new.data_contrato
         then
             long_aux := long_aux        ||
              inter_fn_buscar_tag('lb02872#DATA CONTRATO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.data_contrato || ' -> ' ||
                                            :new.data_contrato ||
                                            chr(10);
         end if;

         if :old.data_digitacao <> :new.data_digitacao
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb02908#DATA DIGITACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.data_digitacao || ' -> ' ||
                                            :new.data_digitacao ||
                                            chr(10);
         end if;

         if :old.data_real_pagto <> :new.data_real_pagto
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb02981#DATA PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.data_real_pagto || ' -> ' ||
                                            :new.data_real_pagto ||
                                            chr(10);
         end if;

         if :old.data_transacao <> :new.data_transacao
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb03029#DATA TRANSACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.data_transacao || ' -> ' ||
                                            :new.data_transacao ||
                                            chr(10);
         end if;

         if :old.data_vencto_original <> :new.data_vencto_original
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb17668#DATA VENCIMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.data_vencto_original || ' -> ' ||
                                            :new.data_vencto_original ||
                                            chr(10);
         end if;

         if :old.data_vencimento <> :new.data_vencimento
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb22852#DATA PRORROGACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.data_vencimento || ' -> ' ||
                                            :new.data_vencimento ||
                                            chr(10);
         end if;

         if :old.moeda_titulo <> :new.moeda_titulo
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb13146#MOEDA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.moeda_titulo || ' -> ' ||
                                            :new.moeda_titulo ||
                                            chr(10);
         end if;

         if :old.num_tit_subs <> :new.num_tit_subs
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb05518#TITULO SUBSTITUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.num_tit_subs || ' -> ' ||
                                            :new.num_tit_subs ||
                                            chr(10);
         end if;

         if :old.tipo_tit_subs <> :new.tipo_tit_subs
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb05494#TIPO SUBSTITUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.tipo_tit_subs || ' -> ' ||
                                            :new.tipo_tit_subs ||
                                            chr(10);
         end if;

         if :old.numero_adiantam <> :new.numero_adiantam
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb05344#NUMERO DO ADIANTAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.numero_adiantam || ' -> ' ||
                                            :new.numero_adiantam ||
                                            chr(10);
         end if;

         if :old.numero_cheque <> :new.numero_cheque
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb13245#NR.CHEQUE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.numero_cheque || ' -> ' ||
                                            :new.numero_cheque ||
                                            chr(10);
         end if;

         if :old.saldo_titulo <> :new.saldo_titulo
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb34830#VALOR SALDO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.saldo_titulo || ' -> ' ||
                                            :new.saldo_titulo ||
                                            chr(10);
         end if;

         if :old.sit_bloqueio <> :new.sit_bloqueio
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb05458#SIT. BLOQUEIO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.sit_bloqueio || ' -> ' ||
                                            :new.sit_bloqueio ||
                                            chr(10);
         end if;

         if :old.situacao <> :new.situacao
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.situacao || ' -> ' ||
                                            :new.situacao ||
                                            chr(10);
         end if;

         if :old.usuario_digitacao <> :new.usuario_digitacao
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb00348#USUARIO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.usuario_digitacao || ' -> ' ||
                                            :new.usuario_digitacao ||
                                            chr(10);
         end if;

         if :old.valor_abatimento <> :new.valor_abatimento
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb01604#ABATIMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.valor_abatimento || ' -> ' ||
                                            :new.valor_abatimento ||
                                            chr(10);
         end if;

         if :old.valor_cheque <> :new.valor_cheque
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb13246#VALOR CHEQUE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.valor_cheque || ' -> ' ||
                                            :new.valor_cheque ||
                                            chr(10);
         end if;

         if :old.valor_desc <> :new.valor_desc
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb07929#VALOR DESCONTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.valor_desc || ' -> ' ||
                                            :new.valor_desc ||
                                            chr(10);
         end if;

         if :old.valor_desconto_cheque <> :new.valor_desconto_cheque
         then
             long_aux := long_aux        ||
              inter_fn_buscar_tag('lb34831#VALOR DESCONTO CHEQUE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.valor_desconto_cheque || ' -> ' ||
                                            :new.valor_desconto_cheque ||
                                            chr(10);
         end if;

         if :old.valor_juros <> :new.valor_juros
         then
             long_aux := long_aux        ||
              inter_fn_buscar_tag('lb31928#VALOR JUROS:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.valor_juros || ' -> ' ||
                                            :new.valor_juros ||
                                            chr(10);
         end if;

         if :old.valor_juros_cheque <> :new.valor_juros_cheque
         then
             long_aux := long_aux        ||
              inter_fn_buscar_tag('lb34832#VALOR JUROS CHEQUE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.valor_juros_cheque || ' -> ' ||
                                            :new.valor_juros_cheque ||
                                            chr(10);
         end if;

         if :old.valor_moeda <> :new.valor_moeda or :new.valor_moeda is null or :old.valor_moeda is null
         then
             long_aux := long_aux        ||
              inter_fn_buscar_tag('lb05544#VALOR MOEDA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.valor_moeda || ' -> ' ||
                                            :new.valor_moeda ||
                                            chr(10);
         end if;

         if :old.valor_parcela <> :new.valor_parcela or :new.valor_parcela is null or :old.valor_parcela is null
         then
             long_aux := long_aux        ||
              inter_fn_buscar_tag('lb05547#VALOR PARCELA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.valor_parcela || ' -> ' ||
                                            :new.valor_parcela ||
                                            chr(10);
         end if;


         if :old.situacao_sispag <> :new.situacao_sispag
         then
             long_aux := long_aux        ||
             inter_fn_buscar_tag('lb36826#SITUACAO DO PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.situacao_sispag || ' -> ' ||
                                            :new.situacao_sispag ||
                                            chr(10);
         end if;

         if :old.codigo_contabil <> :new.codigo_contabil
         then
             long_aux := long_aux        ||
              inter_fn_buscar_tag('lb00585#CODIGO CONTABIL: :',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.codigo_contabil || ' -> ' ||
                                            :new.codigo_contabil ||
                                            chr(10);
         end if;

         if :old.lote <> :new.lote
         then
             long_aux := long_aux        ||

              inter_fn_buscar_tag('lb00236#LOTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.lote || ' -> ' ||
                                            :new.lote ||
                                            chr(10);
         end if;

         if :old.data_lote <> :new.data_lote
         then
             long_aux := long_aux        ||

             inter_fn_buscar_tag('lb42094#DATA LOTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :old.data_lote || ' -> ' ||
                                           :new.data_lote||
                                            chr(10);
         end if;

         if :old.codigo_barras <> :new.codigo_barras
         then
             long_aux := long_aux        ||
              inter_fn_buscar_tag('lb02581#C?DIGO BARRAS:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.codigo_barras || ' -> ' ||
                                            :new.codigo_barras||
                                            chr(10);
         end if;
         
         if trim(:old.observacao_libercaco) is null
         then
             old_observacao_libercaco := ' ';
         end if;
             
         if old_observacao_libercaco <> :new.observacao_libercaco
         then
             
             
             long_aux := long_aux        ||
              inter_fn_buscar_tag('lb00259#OBSERVA??O:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            old_observacao_libercaco || ' -> ' ||
                                            :new.observacao_libercaco||
                                            chr(10);
         end if;

         if :old.status_monkey  <> :new.status_monkey then
            long_aux := long_aux        ||
              'Status Monkey: ' || ' ' ||
                                            :old.status_monkey || ' -> ' ||
                                            :new.status_monkey;
         end if;

         if :old.executa_trigger <> :new.executa_trigger
         then
             long_aux := long_aux        ||
              'EXECUTA TRIGGER' || ' ' ||
                                            :old.executa_trigger || ' -> ' ||
                                            :new.executa_trigger;
         end if;

         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        usuario_rede,
              maquina_rede,      aplicacao,
              num01,             num02,
              num03,             num04,
              num05,             str01,
              long01
            )
         VALUES
            ( 'CPAG_010',            'A',
              sysdate,               ws_usuario_rede,
              ws_maquina_rede,       ws_aplicativo,
              :new.nr_duplicata,     :new.cgc_9,
              :new.cgc_4,            :new.cgc_2,
              :new.tipo_titulo,      :new.parcela,
              long_aux
           );

      end if;

      if deleting
      then
         INSERT INTO hist_100
            ( tabela,                         operacao,
              data_ocorr,                     usuario_rede,
              maquina_rede,                   aplicacao,
              num01,                          num02,
              num03,                          num04,
              num05,                          str01,
              long01
            )
         VALUES
            ( 'CPAG_010',                     'D',
              sysdate,                        ws_usuario_rede,
              ws_maquina_rede,                ws_aplicativo,
              :old.nr_duplicata,              :old.cgc_9,
              :old.cgc_4,                     :old.cgc_2,
              :old.tipo_titulo,               :old.parcela,
              '                                                  ' ||
              inter_fn_buscar_tag('ds26571#TITULOS A PAGAR',ws_locale_usuario,ws_usuario_systextil)
           );
      end if;
end inter_tr_cpag_010_log;

/

exec inter_pr_recompile;

