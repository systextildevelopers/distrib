create table pcpc_151
(
	cod_deposito     number(3) 		default 0 	not null,
	tipo_deposito    varchar2(30) 	default '' 	not null,
	cod_rgb          varchar2(15) 	default '' 	not null,
	qualidade        number(1) 		default 0 	not null
);

alter table pcpc_151 add constraint pk_pcpc_151 primary key (cod_deposito);

comment on table pcpc_151 is 'Depósitos para Apontamento de Produção';
comment on column pcpc_151.tipo_deposito    is 'Descrição do Tipo de depósito';
comment on column pcpc_151.cod_deposito     is 'Código do depósito da basi_205';
comment on column pcpc_151.cod_rgb     		is 'Código da cor no formato RGB.';
comment on column pcpc_151.qualidade     	is 'Código do Tipo de Qualidade.';
