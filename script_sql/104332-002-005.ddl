create table OBRF_825
( COD_EMPRESA          NUMBER(3)    default 0 not null,
  MES                  NUMBER(2)    default 0 not null,
  ANO                  NUMBER(4)    default 0 not null,
  ID                   NUMBER(9)    default 0 not null,
  COD_MENSAGEM         NUMBER(6)    default 0 not null,
  DOC_ARRECADACAO      VARCHAR2(40) default ' ' not null,
  ORIGEM_PROCESSO      VARCHAR2(1)  default ' ',
  NR_DOC_PROCESSO      VARCHAR2(40) default ' ',
  DESCR_EMBASAMENTO    VARCHAR2(250) default ' ',
  DESCR_COMPLEMENTAR   VARCHAR2(250) default ' ');
  
alter table OBRF_825 add constraint PK_OBRF_825 primary key (COD_EMPRESA,MES,ANO,ID,COD_MENSAGEM,DOC_ARRECADACAO);
 
create synonym systextilrpt.OBRF_825 for OBRF_825; 
