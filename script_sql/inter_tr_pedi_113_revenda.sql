CREATE OR REPLACE TRIGGER inter_tr_pedi_113_revenda
BEFORE INSERT OR DELETE ON pedi_113
FOR EACH ROW
BEGIN
    IF :OLD.item_revenda = 1 or :NEW.item_revenda = 1 THEN
         IF INSERTING THEN
             UPDATE estq_308
             SET qtde_empenhada = qtde_empenhada + :NEW.quantidade
             WHERE NIVEL_ESTRUTURA = :NEW.NIVEL
             AND GRUPO_ESTRUTURA = :NEW.GRUPO
             AND SUBGRU_ESTRUTURA = :NEW.SUBGRUPO
             AND ITEM_ESTRUTURA = :NEW.ITEM
             AND COD_EMPRESA = :NEW.COD_EMPRESA;
         ELSIF DELETING THEN
             UPDATE estq_308
             SET qtde_empenhada = qtde_empenhada - :OLD.quantidade
             WHERE NIVEL_ESTRUTURA = :OLD.NIVEL
             AND GRUPO_ESTRUTURA = :OLD.GRUPO
             AND SUBGRU_ESTRUTURA = :OLD.SUBGRUPO
             AND ITEM_ESTRUTURA = :OLD.ITEM
             AND COD_EMPRESA = :OLD.COD_EMPRESA;
        END IF;
    END IF;
END;
