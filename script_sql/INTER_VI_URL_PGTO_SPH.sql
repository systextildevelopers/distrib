--------------------------------------------------------------------
-- View que Lista a URL da transacao de Pagamento gerada pelo SPH --
--------------------------------------------------------------------
CREATE OR REPLACE VIEW  "INTER_VI_URL_PGTO_SPH" AS 
  SELECT 
    numero_referencia,
    codigo_empresa,
    CD_CLI_CGC_CLI9,
    CD_CLI_CGC_CLI4,
    CD_CLI_CGC_CLI2,
    pedido_venda,
    documento_origem,
    tipo_pagamento_sph,
    gateway_pagamento_sph,
    valor, 
    data_vencimento as data_expiracao,
    CASE 
        WHEN link_pagamento_sph IS NOT NULL THEN link_pagamento_sph
        ELSE
            CASE 
                WHEN url_pagamento_sph IS NOT NULL THEN url_pagamento_sph
                ELSE emv_sph
            END 
    END AS url_pgto
FROM 
    fina_204;
/
