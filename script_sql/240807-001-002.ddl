-------------------------------------------
-- Item da Sugestão de tecidos tem peças --
-------------------------------------------
create table pcpt_081 (
	id number(9,0) not null,
  id_sugestao number(9,0) not null,
  seq_item_pedido number(9,0) not null,
	qtde_pedida number(9,0) not null,
	qtde_sugerida number(9,0) not null,
  criado_por varchar2(250),
  criado_em timestamp (6) default current_timestamp,
  atualizado_por varchar2(250),
  atualizado_em timestamp (6)
);
/

alter table pcpt_081 add foreign key (id_sugestao) references pcpt_080 (id);
/

alter table pcpt_081 add constraint pcpt_081_pk primary key (id);
/

create sequence pcpt_081_seq;
/

create or replace trigger  "inter_tr_pcpt_081_pk" 
before insert on pcpt_081
for each row
begin
  :new.id := pcpt_081_seq.nextval;
end;
/

