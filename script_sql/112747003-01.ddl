alter table basi_632 add (
	numero_alternativa  number(2) default 0,
	numero_roteiro		number(2) default 0
);

comment on column basi_632.numero_alternativa is 'N�mero da alternativa para a composi��o';
comment on column basi_632.numero_roteiro is 'N�mero do roteiro para a composi��o';

exec inter_pr_recompile;
