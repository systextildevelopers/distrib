alter table sped_k200_h010
add SEQUENCIA_OPERACAO_AGRUPADOR NUMBER(4) default 0;

alter table sped_h010
add SEQUENCIA_OPERACAO_AGRUPADOR NUMBER(4) default 0;

alter table sped_k200
add SEQUENCIA_OPERACAO_AGRUPADOR NUMBER(4) default 0;

alter table sped_0200
add SEQUENCIA_OPERACAO_AGRUPADOR NUMBER(4) default 0;

alter table SPED_0200
drop constraint PK_SPED_0200;
 
drop index PK_SPED_0200;
 
alter table SPED_0200
add constraint PK_SPED_0200 primary key (COD_EMPRESA, COD_NIVEL, COD_GRUPO, COD_SUBGRUPO, COD_ITEM, ESTAGIO_AGRUPADOR, ESTAGIO_AGRUPADOR_SIMULTANEO, SEQUENCIA_OPERACAO_AGRUPADOR);

-- campo abaixo já existe na apex-blocok, mas é necessário uso no systextil-sql
alter table blocok_230 add sequencia_operacao_agrupador number(4,0);
alter table blocok_235 add sequencia_operacao_agrupador number(4,0);



/


exec inter_pr_recompile;
