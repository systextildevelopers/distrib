CREATE TABLE PCPC_012 (
	FORNECEDOR_CGC9 NUMBER(9) NOT NULL,
	FORNECEDOR_CGC4 NUMBER(4) NOT NULL,
	FORNECEDOR_CGC2 NUMBER(2) NOT NULL,
	CLIENTE_CGC9 NUMBER(9) NOT NULL,
	CLIENTE_CGC4 NUMBER(4) NOT NULL,
	CLIENTE_CGC2 NUMBER(2) NOT NULL,
	CONSTRAINT PK_RELAC_FORN_CLI PRIMARY KEY (FORNECEDOR_CGC9, FORNECEDOR_CGC4, FORNECEDOR_CGC2, CLIENTE_CGC9, CLIENTE_CGC4, CLIENTE_CGC2),
	CONSTRAINT FK_RELAC_FORN_CLI_FORNECEDOR 
		FOREIGN KEY (FORNECEDOR_CGC9, FORNECEDOR_CGC4, FORNECEDOR_CGC2)
		REFERENCES SUPR_010(fornecedor9, fornecedor4, fornecedor2),
	CONSTRAINT FK_RELAC_FORN_CLI_CLIENTE
		FOREIGN KEY (CLIENTE_CGC9, CLIENTE_CGC4, CLIENTE_CGC2)
		REFERENCES PEDI_010(CGC_9, CGC_4, CGC_2)
);
COMMENT ON TABLE PCPC_012 IS 'Relacionamento entre fornecedores e clientes para indicar fornecedores bloquados para prestar serviços à determinados clientes';
