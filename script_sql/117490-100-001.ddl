alter table fatu_504 add cria_rolo_como_previsto number(1) default 0;

comment on column fatu_504.cria_rolo_como_previsto is '0: Nao cria o rolo como previsto (situacao 0). 1: Rolo sera criado no pcpb_f180 como previsto (situacao 0).';

exec inter_pr_recompile;
