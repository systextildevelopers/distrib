create table obrf_340 (
    id                number(9)      not null,
    descricao         varchar2(255)  not null,
    motivo_devolucao  number(3),
    especie           varchar2(5),
    serie             varchar2(3),
    tipo_frete        number(1),
    via_transporte    number(1),
    especie_volume    varchar2(10),
    marca_volume      varchar2(15),
    cond_pagamento    number(3),
    deposito          number(3),
    zerar_frete       number(1),
    tipo_fornecedor   number(2),
    codigo_contabil   number(6),
    padrao            number(1)      default 0
);

alter table obrf_340 add constraint pk_obrf_340 primary key (id);

create sequence id_obrf_340;

exec inter_pr_recompile;
/
