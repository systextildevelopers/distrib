alter table supr_090
  ADD (
    e_mail varchar(4000) default '' null,
    assunto_email varchar(100) default '' null,
    texto_email varchar(4000) default '' null
  );
  
  comment on column supr_090.e_mail is 'Endereco e-mail para envio';
  comment on column supr_090.assunto_email is 'Assunto do e-mail';
  comment on column supr_090.texto_email is 'Texto e-mail';
  
exec inter_pr_recompile;
