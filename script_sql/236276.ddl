INSERT INTO EMPR_007 (
PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT) 
VALUES ('estq.atlzSaldoPeriodico', 1, 'lb50891', 'fy46622', null, 0, null, null);

COMMIT;


declare
cursor parametro_c is select codigo_empresa from fatu_500;
tmpInt number;
formaAtu number;
begin

  for reg_parametro in parametro_c
  loop
  select count(*) into tmpInt
  from empr_008
  where empr_008.codigo_empresa = reg_parametro.codigo_empresa
  and empr_008.param = 'estq.atlzSaldoPeriodico' ;

  if(tmpInt = 0)
  then
    begin
       insert into empr_008 (
       codigo_empresa, param, val_int
       ) values (
       reg_parametro.codigo_empresa, 'estq.atlzSaldoPeriodico', 0);
    end;
 end if;

commit;
 end loop;
end;
