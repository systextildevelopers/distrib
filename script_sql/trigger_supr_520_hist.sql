
  CREATE OR REPLACE TRIGGER "TRIGGER_SUPR_520_HIST" 
   BEFORE INSERT OR
          DELETE OR
          UPDATE of situacao, qtde_requisitada, qtde_entregue, data_entregue,
                    usuario_receptor, almoxarife, cod_transacao, qtde_nao_emp, deposito
                    on supr_520
   for each row
declare
   ws_num_requisicao       number(6);
   ws_sequencia            number(4);
   ws_nivel                varchar2(1);
   ws_grupo                varchar2(5);
   ws_subgrupo             varchar2(3);
   ws_item                 varchar2(6);
   ws_situacao_old         number(1);
   ws_situacao_new         number(1);
   ws_qtde_requisitada_old number(13,2);
   ws_qtde_requisitada_new number(13,2);
   ws_qtde_entregue_old    number(13,2);
   ws_qtde_entregue_new    number(13,2);
   ws_data_entregue_old    date;
   ws_data_entregue_new    date;
   ws_usuario_receptor_old varchar2(30);
   ws_usuario_receptor_new varchar2(30);
   ws_almoxarife_old       varchar2(20);
   ws_almoxarife_new       varchar2(20);
   ws_cod_transacao_old    number(3);
   ws_cod_transacao_new    number(3);
   ws_deposito_old         number(3);
   ws_deposito_new         number(3);
   ws_qtde_nao_emp_old     number(13,2);
   ws_qtde_nao_emp_new     number(13,2);
   ws_tipo_ocorr           varchar2(1);
   ws_usuario_rede         varchar2(20);
   ws_maquina_rede         varchar2(40);
   ws_aplicacao            varchar2(20);
   v_consumo               basi_050.consumo%type;
   v_ref                   pcpc_020.referencia_peca%type;
   v_alt                   pcpc_020.alternativa_peca%type;
   v_ult                   pcpc_020.ultimo_estagio%type;
   v_sub                   basi_050.sub_item%type;
   v_ite                   basi_050.item_item%type;
   v_qtde                  pcpc_040.qtde_pecas_prog%type;
   v_executa_trigger      number;

   v_nome_programa          supr_520.nome_programa%type;
   v_usuario_rede           varchar2(20);
   v_maquina_rede           varchar2(40);
   v_aplicativo             varchar2(20);
   v_sid                    number(9);
   v_empresa                number(3);
   v_usuario_systextil      varchar2(250);
   v_locale_usuario         varchar2(5);



begin

   -- Dados do usu�rio logado
   inter_pr_dados_usuario (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                           v_usuario_systextil,   v_empresa,        v_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(v_sid); 
   
   -- INICIO - L�gica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementa��o executada para limpeza da base de dados do cliente
   -- e replica��o dos dados para base hist�rico. (SS.38405)
   -- quando disparado pela procedure INTER_PR_EMPENHO_ALMOXARIFADO n�o atualiza log

   if :new.controla_log is null or :new.controla_log <> 'PR_E_AL'
   then
      if inserting
      then
         ws_tipo_ocorr:='I';
         if :new.executa_trigger = 1
         then v_executa_trigger := 1;
         else v_executa_trigger := 0;
         end if;
      end if;

      if updating
      then
         ws_tipo_ocorr:= 'U';
         if :old.executa_trigger = 1 or :new.executa_trigger = 1
         then v_executa_trigger := 1;
         else v_executa_trigger := 0;
         end if;
      end if;

      if deleting
      then
         if :old.executa_trigger = 1
         then v_executa_trigger := 1;
         else v_executa_trigger := 0;
         end if;
      end if;

      if v_executa_trigger = 0
      then

         if INSERTING then
            ws_num_requisicao       := :new.num_requisicao;
            ws_sequencia            := :new.sequencia;
            ws_nivel                := :new.nivel;
            ws_grupo                := :new.grupo;
            ws_subgrupo             := :new.subgrupo;
            ws_item                 := :new.item;
            ws_situacao_old         := 0;
            ws_situacao_new         := :new.situacao;
            ws_qtde_requisitada_old := 0.0;
            ws_qtde_requisitada_new := :new.qtde_requisitada;
            ws_qtde_entregue_old    := 0.0;
            ws_qtde_entregue_new    := :new.qtde_entregue;
            ws_data_entregue_old    := null;
            ws_data_entregue_new    := :new.data_entregue;
            ws_usuario_receptor_old := '';
            ws_usuario_receptor_new := :new.usuario_receptor;
            ws_almoxarife_old       := '';
            ws_almoxarife_new       := :new.almoxarife;
            ws_cod_transacao_old    := 0;
            ws_cod_transacao_new    := :new.cod_transacao;
            ws_qtde_nao_emp_old     := 0.0;
            ws_qtde_nao_emp_new     := :new.qtde_nao_emp;
            ws_deposito_old         := 0;
            ws_deposito_new         := :new.deposito;
            ws_tipo_ocorr           := 'I';
         elsif UPDATING then
               ws_num_requisicao       := :new.num_requisicao;
               ws_sequencia            := :new.sequencia;
               ws_nivel                := :new.nivel;
               ws_grupo                := :new.grupo;
               ws_subgrupo             := :new.subgrupo;
               ws_item                 := :new.item;
               ws_situacao_old         := :old.situacao;
               ws_situacao_new         := :new.situacao;
               ws_qtde_requisitada_old := :old.qtde_requisitada;
               ws_qtde_requisitada_new := :new.qtde_requisitada;
               ws_qtde_entregue_old    := :old.qtde_entregue;
               ws_qtde_entregue_new    := :new.qtde_entregue;
               ws_data_entregue_old    := :old.data_entregue;
               ws_data_entregue_new    := :new.data_entregue;
               ws_usuario_receptor_old := :old.usuario_receptor;
               ws_usuario_receptor_new := :new.usuario_receptor;
               ws_almoxarife_old       := :old.almoxarife;
               ws_almoxarife_new       := :new.almoxarife;
               ws_cod_transacao_old    := :old.cod_transacao;
               ws_cod_transacao_new    := :new.cod_transacao;
               ws_qtde_nao_emp_old     := :old.qtde_nao_emp;
               ws_qtde_nao_emp_new     := :new.qtde_nao_emp;
               ws_deposito_old         := :old.deposito;
               ws_deposito_new         := :new.deposito;
               ws_tipo_ocorr           := 'A';
            elsif DELETING then
                  ws_num_requisicao       := :old.num_requisicao;
                  ws_sequencia            := :old.sequencia;
                  ws_nivel                := :old.nivel;
                  ws_grupo                := :old.grupo;
                  ws_subgrupo             := :old.subgrupo;
                  ws_item                 := :old.item;
                  ws_situacao_old         := :old.situacao;
                  ws_situacao_new         := 0;
                  ws_qtde_requisitada_old := :old.qtde_requisitada;
                  ws_qtde_requisitada_new := 0.0;
                  ws_qtde_entregue_old    := :old.qtde_entregue;
                  ws_qtde_entregue_new    := 0.0;
                  ws_data_entregue_old    := :old.data_entregue;
                  ws_data_entregue_new    := null;
                  ws_usuario_receptor_old := :old.usuario_receptor;
                  ws_usuario_receptor_new := '';
                  ws_almoxarife_old       := :old.almoxarife;
                  ws_almoxarife_new       := '';
                  ws_cod_transacao_old    := :old.cod_transacao;
                  ws_cod_transacao_new    := 0;
                  ws_qtde_nao_emp_old     := :old.qtde_nao_emp;
                  ws_qtde_nao_emp_new     := 0.0;
                  ws_deposito_old         := :old.deposito;
                  ws_deposito_new         := 0;
                  ws_tipo_ocorr           := 'D';
         end if;
         select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
         into ws_usuario_rede, ws_maquina_rede, ws_aplicacao
         from sys.gv_$session
         where audsid  = userenv('SESSIONID')
           and inst_id = userenv('INSTANCE')
           and rownum < 2;

         INSERT INTO supr_520_hist
           (num_requisicao,          sequencia,            nivel,
            grupo,                   subgrupo,             item,
            situacao_old,            situacao_new,         qtde_requisitada_old,
            qtde_requisitada_new,    qtde_entregue_old,    qtde_entregue_new,
            data_entregue_old,       data_entregue_new,    usuario_receptor_old,
            usuario_receptor_new,    almoxarife_old,       almoxarife_new,
            cod_transacao_old,       cod_transacao_new,    deposito_old,
            deposito_new,            qtde_nao_emp_old,     qtde_nao_emp_new,
            tipo_ocorr,              usuario_rede,         maquina_rede,
            aplicacao,               data_ocorr,           nome_programa,       usuario)
         VALUES
           (ws_num_requisicao,       ws_sequencia,         ws_nivel,
            ws_grupo,                ws_subgrupo,          ws_item,
            ws_situacao_old,         ws_situacao_new,      ws_qtde_requisitada_old,
            ws_qtde_requisitada_new, ws_qtde_entregue_old, ws_qtde_entregue_new,
            ws_data_entregue_old,    ws_data_entregue_new, ws_usuario_receptor_old,
            ws_usuario_receptor_new, ws_almoxarife_old,    ws_almoxarife_new,
            ws_cod_transacao_old,    ws_cod_transacao_new, ws_deposito_old,
            ws_deposito_new,         ws_qtde_nao_emp_old,  ws_qtde_nao_emp_new,
            ws_tipo_ocorr,           ws_usuario_rede,      ws_maquina_rede,
            ws_aplicacao,            sysdate,              v_nome_programa,     v_usuario_systextil);
      end if;
   end if;

end trigger_supr_520_hist;
-- ALTER TRIGGER "TRIGGER_SUPR_520_HIST" ENABLE
 

/

exec inter_pr_recompile;

