create or replace trigger inter_tr_pedi_095_log 
after insert or delete or update 
on PEDI_095 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(20) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu?rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 
  
   v_nome_programa := inter_fn_nome_programa(ws_sid); 
 
 if inserting 
 then 
    begin 
 
        insert into PEDI_095_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           TAB_COL_TAB_OLD,   /*8*/ 
           TAB_COL_TAB_NEW,   /*9*/ 
           TAB_MES_TAB_OLD,   /*10*/ 
           TAB_MES_TAB_NEW,   /*11*/ 
           TAB_SEQ_TAB_OLD,   /*12*/ 
           TAB_SEQ_TAB_NEW,   /*13*/ 
           NIVEL_PRECO_OLD,   /*14*/ 
           NIVEL_PRECO_NEW,   /*15*/ 
           NIVEL_ESTRUTURA_OLD,   /*16*/ 
           NIVEL_ESTRUTURA_NEW,   /*17*/ 
           GRUPO_ESTRUTURA_OLD,   /*18*/ 
           GRUPO_ESTRUTURA_NEW,   /*19*/ 
           SUBGRU_ESTRUTURA_OLD,   /*20*/ 
           SUBGRU_ESTRUTURA_NEW,   /*21*/ 
           ITEM_ESTRUTURA_OLD,   /*22*/ 
           ITEM_ESTRUTURA_NEW,   /*23*/ 
           SERIE_COR_OLD,   /*24*/ 
           SERIE_COR_NEW,   /*25*/ 
           VAL_TABELA_PRECO_OLD,   /*26*/ 
           VAL_TABELA_PRECO_NEW,   /*27*/ 
           DESCONTO_MAXIMO_OLD,   /*28*/ 
           DESCONTO_MAXIMO_NEW,   /*29*/ 
           LARGURA_OLD,   /*30*/ 
           LARGURA_NEW,   /*31*/ 
           DATA_FORM_PRECO_OLD,   /*32*/ 
           DATA_FORM_PRECO_NEW,   /*33*/ 
           TIPO_VALOR_OLD,   /*34*/ 
           TIPO_VALOR_NEW,   /*35*/ 
           COR_PANTONE_OLD,   /*36*/ 
           COR_PANTONE_NEW,   /*37*/ 
           FLAG_EXPORTACAO_LOJA_OLD,   /*38*/ 
           FLAG_EXPORTACAO_LOJA_NEW,   /*39*/ 
           FLAG_AGRUPADOR_OLD,   /*40*/ 
           FLAG_AGRUPADOR_NEW    /*41*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.TAB_COL_TAB, /*9*/   
           0,/*10*/
           :new.TAB_MES_TAB, /*11*/   
           0,/*12*/
           :new.TAB_SEQ_TAB, /*13*/   
           0,/*14*/
           :new.NIVEL_PRECO, /*15*/   
           '',/*16*/
           :new.NIVEL_ESTRUTURA, /*17*/   
           '',/*18*/
           :new.GRUPO_ESTRUTURA, /*19*/   
           '',/*20*/
           :new.SUBGRU_ESTRUTURA, /*21*/   
           '',/*22*/
           :new.ITEM_ESTRUTURA, /*23*/   
           0,/*24*/
           :new.SERIE_COR, /*25*/   
           0,/*26*/
           :new.VAL_TABELA_PRECO, /*27*/   
           0,/*28*/
           :new.DESCONTO_MAXIMO, /*29*/   
           0,/*30*/
           :new.LARGURA, /*31*/   
           null,/*32*/
           :new.DATA_FORM_PRECO, /*33*/   
           0,/*34*/
           :new.TIPO_VALOR, /*35*/   
           '',/*36*/
           :new.COR_PANTONE, /*37*/   
           0,/*38*/
           :new.FLAG_EXPORTACAO_LOJA, /*39*/   
           0,/*40*/
           :new.FLAG_AGRUPADOR /*41*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into PEDI_095_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           TAB_COL_TAB_OLD, /*8*/   
           TAB_COL_TAB_NEW, /*9*/   
           TAB_MES_TAB_OLD, /*10*/   
           TAB_MES_TAB_NEW, /*11*/   
           TAB_SEQ_TAB_OLD, /*12*/   
           TAB_SEQ_TAB_NEW, /*13*/   
           NIVEL_PRECO_OLD, /*14*/   
           NIVEL_PRECO_NEW, /*15*/   
           NIVEL_ESTRUTURA_OLD, /*16*/   
           NIVEL_ESTRUTURA_NEW, /*17*/   
           GRUPO_ESTRUTURA_OLD, /*18*/   
           GRUPO_ESTRUTURA_NEW, /*19*/   
           SUBGRU_ESTRUTURA_OLD, /*20*/   
           SUBGRU_ESTRUTURA_NEW, /*21*/   
           ITEM_ESTRUTURA_OLD, /*22*/   
           ITEM_ESTRUTURA_NEW, /*23*/   
           SERIE_COR_OLD, /*24*/   
           SERIE_COR_NEW, /*25*/   
           VAL_TABELA_PRECO_OLD, /*26*/   
           VAL_TABELA_PRECO_NEW, /*27*/   
           DESCONTO_MAXIMO_OLD, /*28*/   
           DESCONTO_MAXIMO_NEW, /*29*/   
           LARGURA_OLD, /*30*/   
           LARGURA_NEW, /*31*/   
           DATA_FORM_PRECO_OLD, /*32*/   
           DATA_FORM_PRECO_NEW, /*33*/   
           TIPO_VALOR_OLD, /*34*/   
           TIPO_VALOR_NEW, /*35*/   
           COR_PANTONE_OLD, /*36*/   
           COR_PANTONE_NEW, /*37*/   
           FLAG_EXPORTACAO_LOJA_OLD, /*38*/   
           FLAG_EXPORTACAO_LOJA_NEW, /*39*/   
           FLAG_AGRUPADOR_OLD, /*40*/   
           FLAG_AGRUPADOR_NEW  /*41*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.TAB_COL_TAB,  /*8*/  
           :new.TAB_COL_TAB, /*9*/   
           :old.TAB_MES_TAB,  /*10*/  
           :new.TAB_MES_TAB, /*11*/   
           :old.TAB_SEQ_TAB,  /*12*/  
           :new.TAB_SEQ_TAB, /*13*/   
           :old.NIVEL_PRECO,  /*14*/  
           :new.NIVEL_PRECO, /*15*/   
           :old.NIVEL_ESTRUTURA,  /*16*/  
           :new.NIVEL_ESTRUTURA, /*17*/   
           :old.GRUPO_ESTRUTURA,  /*18*/  
           :new.GRUPO_ESTRUTURA, /*19*/   
           :old.SUBGRU_ESTRUTURA,  /*20*/  
           :new.SUBGRU_ESTRUTURA, /*21*/   
           :old.ITEM_ESTRUTURA,  /*22*/  
           :new.ITEM_ESTRUTURA, /*23*/   
           :old.SERIE_COR,  /*24*/  
           :new.SERIE_COR, /*25*/   
           :old.VAL_TABELA_PRECO,  /*26*/  
           :new.VAL_TABELA_PRECO, /*27*/   
           :old.DESCONTO_MAXIMO,  /*28*/  
           :new.DESCONTO_MAXIMO, /*29*/   
           :old.LARGURA,  /*30*/  
           :new.LARGURA, /*31*/   
           :old.DATA_FORM_PRECO,  /*32*/  
           :new.DATA_FORM_PRECO, /*33*/   
           :old.TIPO_VALOR,  /*34*/  
           :new.TIPO_VALOR, /*35*/   
           :old.COR_PANTONE,  /*36*/  
           :new.COR_PANTONE, /*37*/   
           :old.FLAG_EXPORTACAO_LOJA,  /*38*/  
           :new.FLAG_EXPORTACAO_LOJA, /*39*/   
           :old.FLAG_AGRUPADOR,  /*40*/  
           :new.FLAG_AGRUPADOR  /*41*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into PEDI_095_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           TAB_COL_TAB_OLD, /*8*/   
           TAB_COL_TAB_NEW, /*9*/   
           TAB_MES_TAB_OLD, /*10*/   
           TAB_MES_TAB_NEW, /*11*/   
           TAB_SEQ_TAB_OLD, /*12*/   
           TAB_SEQ_TAB_NEW, /*13*/   
           NIVEL_PRECO_OLD, /*14*/   
           NIVEL_PRECO_NEW, /*15*/   
           NIVEL_ESTRUTURA_OLD, /*16*/   
           NIVEL_ESTRUTURA_NEW, /*17*/   
           GRUPO_ESTRUTURA_OLD, /*18*/   
           GRUPO_ESTRUTURA_NEW, /*19*/   
           SUBGRU_ESTRUTURA_OLD, /*20*/   
           SUBGRU_ESTRUTURA_NEW, /*21*/   
           ITEM_ESTRUTURA_OLD, /*22*/   
           ITEM_ESTRUTURA_NEW, /*23*/   
           SERIE_COR_OLD, /*24*/   
           SERIE_COR_NEW, /*25*/   
           VAL_TABELA_PRECO_OLD, /*26*/   
           VAL_TABELA_PRECO_NEW, /*27*/   
           DESCONTO_MAXIMO_OLD, /*28*/   
           DESCONTO_MAXIMO_NEW, /*29*/   
           LARGURA_OLD, /*30*/   
           LARGURA_NEW, /*31*/   
           DATA_FORM_PRECO_OLD, /*32*/   
           DATA_FORM_PRECO_NEW, /*33*/   
           TIPO_VALOR_OLD, /*34*/   
           TIPO_VALOR_NEW, /*35*/   
           COR_PANTONE_OLD, /*36*/   
           COR_PANTONE_NEW, /*37*/   
           FLAG_EXPORTACAO_LOJA_OLD, /*38*/   
           FLAG_EXPORTACAO_LOJA_NEW, /*39*/   
           FLAG_AGRUPADOR_OLD, /*40*/   
           FLAG_AGRUPADOR_NEW /*41*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.TAB_COL_TAB, /*8*/   
           0, /*9*/
           :old.TAB_MES_TAB, /*10*/   
           0, /*11*/
           :old.TAB_SEQ_TAB, /*12*/   
           0, /*13*/
           :old.NIVEL_PRECO, /*14*/   
           0, /*15*/
           :old.NIVEL_ESTRUTURA, /*16*/   
           '', /*17*/
           :old.GRUPO_ESTRUTURA, /*18*/   
           '', /*19*/
           :old.SUBGRU_ESTRUTURA, /*20*/   
           '', /*21*/
           :old.ITEM_ESTRUTURA, /*22*/   
           '', /*23*/
           :old.SERIE_COR, /*24*/   
           0, /*25*/
           :old.VAL_TABELA_PRECO, /*26*/   
           0, /*27*/
           :old.DESCONTO_MAXIMO, /*28*/   
           0, /*29*/
           :old.LARGURA, /*30*/   
           0, /*31*/
           :old.DATA_FORM_PRECO, /*32*/   
           null, /*33*/
           :old.TIPO_VALOR, /*34*/   
           0, /*35*/
           :old.COR_PANTONE, /*36*/   
           '', /*37*/
           :old.FLAG_EXPORTACAO_LOJA, /*38*/   
           0, /*39*/
           :old.FLAG_AGRUPADOR, /*40*/   
           0 /*41*/
         );    
    end;    
 end if;    
end inter_tr_PEDI_095_log;
/
exec inter_pr_recompile;
/* versao: 1 */


 exit;
