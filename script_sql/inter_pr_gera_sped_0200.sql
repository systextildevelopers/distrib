create or replace procedure inter_pr_gera_sped_0200(p_cod_empresa  NUMBER,
                                                     p_dat_inicial  IN  DATE,
                                                     p_dat_final    IN  DATE,
                                                     p_gera_sped_k200 IN varchar2,
                                                     p_des_erro     out varchar2) is
--
-- Finalidade: gerar a tabela sped_0200 - Produtos
--                            sped_0190 - Unidade de Medida
-- Autor.....: Cesar Anton
-- Data......: 21/11/08
--
-- Historicos
--
-- Data    Autor    Observacoes
--
w_erro                EXCEPTION;
w_ind_achou           VARCHAR2(1);


w_ex_ipi                 sped_0200.cod_export_ipi%type;
w_ex_ipi_aux             sped_0200.cod_export_ipi%type;
w_cod_ncm                basi_240.classific_fiscal%type;
ws_aliquota_icms_sped    basi_240.aliquota_icms_sped%type;
ws_icms_sped             basi_240.aliquota_icms_sped%type;
ws_cod_barras            basi_010.codigo_barras%type;
ws_cod_enquadramento_ipi basi_240.cod_enquadramento_ipi%type;
ws_aliquota_ipi          basi_240.aliquota_ipi%type;
ws_aliquota_ipi_aux      basi_240.aliquota_ipi%type;
ws_descr_class_fisc      basi_240.descr_class_fisc%type;
ws_cod_unid_medida       sped_c170.cod_unid_medida%type;
w_perfil_empresa         fatu_502.perfil_sped%type;
w_max_item               varchar(100);
w_tem_ex_ipi             number(1);
w_cod_servico_lst        basi_010.cod_servico_lst%type;
w_tipo_produto_sped      number(2);
temApuracaoNova          number(2);
v_narrativa              varchar(400);
v_dtIniblok              date;
v_controleProcessoBlocoK NUMBER;
v_controleNarrativaBlocoK NUMBER;
v_controleDescSimulBlocoK varchar(2);
v_estagio_agrupador_simultaneo NUMBER;

CURSOR u_sped_c170 (p_cod_empresa     NUMBER) IS
  select cod_empresa, cod_nivel, cod_grupo, cod_subgru, cod_item, estagio_estrutura, estagio_agrupador_simultaneo, sum(qtde), seq_agrupador
    from(
    (SELECT DISTINCT  sped_c170.cod_empresa, sped_c170.cod_nivel, sped_c170.cod_grupo, sped_c170.cod_subgru, sped_c170.cod_item, sped_c170.estagio_agrupador estagio_estrutura, sped_c170.estagio_agrupador_simultaneo as estagio_agrupador_simultaneo, 1 qtde, sped_c170.seq_operacao_agrupador_insu as seq_agrupador
    FROM   sped_c170,
           sped_c100,
           fatu_504
    WHERE    sped_c170.cod_empresa       = sped_c100.cod_empresa
    and      fatu_504.codigo_empresa     = sped_c100.cod_empresa
    and      sped_c170.num_nota_fiscal   = sped_c100.num_nota_fiscal
    and      sped_c170.cod_serie_nota    = sped_c100.cod_serie_nota
    and      sped_c170.num_cnpj_9        = sped_c100.num_cnpj_9
    and      sped_c170.num_cnpj_4        = sped_c100.num_cnpj_4
    and      sped_c170.num_cnpj_2        = sped_c100.num_cnpj_2
    and      sped_c170.cod_empresa       = p_cod_empresa
    and      sped_c100.tip_entrada_saida = sped_c170.tip_entrada_saida
    and      sped_c100.cod_modelo not in ('06','07','08','09','10','11','22','27','28','57','59', '67', '29', '6')
    and      sped_c100.ind_nf_consumo    = 'N'
    and (sped_c100.cod_situacao_nota <> 2 and sped_c100.cod_situacao_nota <> 3
         and sped_c100.cod_situacao_nota <> 5 and sped_c100.cod_situacao_nota <> 6
         and ((sped_c100.cod_modelo <> '55' or sped_c100.tip_emissao_prop = 1
             or (sped_c100.tip_emissao_prop = 0 and (sped_c100.tip_entrada_saida = 'S' and sped_c100.sig_estado = 'EX' and (sped_c100.dat_averbacao is not null or sped_c100.tip_doc_importacao = 1)))) or fatu_504.tipo_sped = 1))
    and (sped_c100.sig_estado                <> 'EX'
         or
         sped_c100.tip_entrada_saida = 'E'
         or
           ((select count(1) from fatu_067
               where fatu_067.codigo_empresa = sped_c100.cod_empresa
                 and fatu_067.num_nota_fiscal = sped_c100.num_nota_fiscal
                 and fatu_067.serie_nota_fisc = sped_c100.cod_serie_nota) > 0
             and sped_c100.dat_averbacao  <= p_dat_final)
           )
    and not (fatu_504.tipo_sped = 0 and sped_c100.cod_modelo in ('55','65','59') and sped_c100.sig_estado <> 'EX' and (sped_c100.tip_entrada_saida = 'S' or sped_c100.tip_emissao_prop = 0))
    and      sped_c100.cod_situacao_nota not in (2,3,5)
    and (sped_c100.flag_exp = 'N' or sped_c100.sig_estado = 'EX')
    )
    union all
       (select distinct sped_h010.cod_empresa, sped_h010.nivel, sped_h010.grupo, sped_h010.subgrupo, sped_h010.item, sped_h010.estagio_agrupador estagio_estrutura, sped_h010.estagio_agrupador_simultaneo, 1 qtde, nvl(sped_h010.sequencia_operacao_agrupador, 0) as seq_agrupador
       from sped_h010
       where sped_h010.cod_empresa = p_cod_empresa
       )

    union all

      (select distinct
              obrf_297.cod_empresa,       obrf_297.coditem_nivel99,   obrf_297.coditem_grupo,

              decode(decode(obrf_297.coditem_nivel99,'1',niv_quebra_peca,
                     decode(obrf_297.coditem_nivel99,'2',niv_quebra_tecid,
                     decode(obrf_297.coditem_nivel99,'4',niv_quebra_pano,
                     decode(obrf_297.coditem_nivel99,'7',niv_quebra_fio,
                     decode(obrf_297.coditem_nivel99,'9',3,
                     decode(obrf_297.coditem_nivel99,'0',3)))))),1,null,obrf_297.coditem_subgrupo)     AS CODITEM_SUBGRUPO,

              decode(decode(obrf_297.coditem_nivel99,'1',niv_quebra_peca,
                     decode(obrf_297.coditem_nivel99,'2',niv_quebra_tecid,
                     decode(obrf_297.coditem_nivel99,'4',niv_quebra_pano,
                     decode(obrf_297.coditem_nivel99,'7',niv_quebra_fio,
                     decode(obrf_297.coditem_nivel99,'9',3,
                     decode(obrf_297.coditem_nivel99,'0',3)))))), 1,null,

              decode(decode(obrf_297.coditem_nivel99,'1',niv_quebra_peca,
                     decode(obrf_297.coditem_nivel99,'2',niv_quebra_tecid,
                     decode(obrf_297.coditem_nivel99,'4',niv_quebra_pano,
                     decode(obrf_297.coditem_nivel99,'7',niv_quebra_fio,
                     decode(obrf_297.coditem_nivel99,'9',3,
                     decode(obrf_297.coditem_nivel99,'0',3)))))), 2,null,obrf_297.coditem_item)) AS CODITEM_ITEM,

               0 AS estagio_estrutura,
               0 AS estagio_agrupador_simultaneo,
               1 qtde,
               0 as seq_agrupador
        from obrf_297, fatu_500
        where obrf_297.cod_empresa = fatu_500.codigo_empresa

          and  obrf_297.cod_empresa            = p_cod_empresa
          and  obrf_297.mes                    = to_number(to_char(p_dat_final, 'MM'))
          and  obrf_297.ano                    = to_number(to_char(p_dat_final, 'YYYY'))
          and  obrf_297.icms_ipi               = 1 /* ICMS */

          and (obrf_297.coditem_nivel99       is not null
          and  trim(obrf_297.coditem_nivel99) <> ' ')
          and temApuracaoNova = 0
          )

    union all
         (select distinct obrf_823.cod_empresa, obrf_823.nivel, obrf_823.grupo, obrf_823.subgrupo, obrf_823.item, 0 estagio_estrutura, 0 estagio_agrupador_simultaneo, 1 qtde, 0 as seq_agrupador
        from obrf_823
        where obrf_823.cod_empresa = p_cod_empresa
          and obrf_823.ano           = to_number(to_char(p_dat_final, 'YYYY'))
          and obrf_823.mes           = to_number(to_char(p_dat_final, 'MM'))
        and (obrf_823.id           in (3,7,12) or obrf_823.reg_c197 = 1)
        and obrf_823.sped_painel  = 1
        and obrf_823.icms_valor > 0
        and temApuracaoNova       = 1
        and trim(obrf_823.nivel) is not null
        and obrf_823.nivel <> '0'
        )

    union all

       (select distinct obrf_141.cod_empresa, obrf_141.nivel_prod, obrf_141.grupo_prod, obrf_141.subgrupo_prod, obrf_141.item_prod, 0 estagio_estrutura, 0 estagio_agrupador_simultaneo, 1 qtde, 0 as seq_agrupador
       from obrf_141
       where obrf_141.cod_empresa = p_cod_empresa
         and obrf_141.periodo_ini >= p_dat_final
         and obrf_141.periodo_fim <= p_dat_final
         and exists ( select 1
                   from basi_010
                   where basi_010.nivel_estrutura = obrf_141.nivel_prod
                   and basi_010.grupo_estrutura = obrf_141.grupo_prod )
         )
    union all
        (select distinct obrf_743.cod_empresa, obrf_743.nivel,obrf_743.grupo, obrf_743.sub, obrf_743.item, 0 estagio_estrutura, 0 estagio_agrupador_simultaneo, 1 qtde, 0 as seq_agrupador
         from obrf_743
         where obrf_743.mes         = to_number(to_char(p_dat_final, 'MM'))
           and obrf_743.ano         = to_number(to_char(p_dat_final, 'YYYY'))
           and  obrf_743.cod_empresa  = p_cod_empresa

           and (obrf_743.nivel       is not null
           and  trim(obrf_743.nivel) <> ' ')
           )
    union all

               
         (select sped_k100.cod_empresa, 
                 sped_k200.cod_nivel, 
                 sped_k200.cod_grupo, 
                 sped_k200.cod_subgrupo,
                 sped_k200.cod_item,      
                 nvl(sped_k200.estagio_agrupador,0) estagio_estrutura, 
                 nvl(sped_k200.estagio_agrupador_simultaneo,0) estagio_agrupador_simultaneo,
                 sum(sped_k200.qtd) qtde,
                 nvl(sped_k200.sequencia_operacao_agrupador, 0) as seq_agrupador
          from sped_k100, sped_k200
          where sped_k100.cod_empresa = p_cod_empresa
                and sped_k100.dt_ini  = p_dat_inicial
          and sped_k100.id = sped_k200.id_k100
          and p_gera_sped_k200 <> 'N' --S gerar bloco K.
          group by sped_k100.cod_empresa, 
                 sped_k200.cod_nivel, 
                 sped_k200.cod_grupo, 
                 sped_k200.cod_subgrupo,
                 sped_k200.cod_item,      
                 nvl(sped_k200.estagio_agrupador,0), 
                 nvl(sped_k200.estagio_agrupador_simultaneo,0),
                 sped_k200.sequencia_operacao_agrupador
          )

    union all

      ---------------------- BLOCOK_210 ----------------------------
       (select blocok_210.cod_empresa empresa,
               blocok_210.cod_item_nivel nivel_estrutura,
               blocok_210.cod_item_grupo grupo_estrutura,
               blocok_210.cod_item_subgrupo subgru_estrutura,
               blocok_210.cod_item_item item_estrutura,
               nvl(blocok_210.cod_item_est_agrup_insu, 0) as estagio_agrupador,
               nvl(blocok_210.cod_item_est_simult_insu, 0) as estagio_agrupador_simultaneo,
               sum(blocok_210.qtd_ori) qtde,
               0 as seq_agrupador
          from blocok_210
         where blocok_210.cod_empresa = p_cod_empresa
           and blocok_210.ano_periodo_apur =
               extract(year from to_date(p_dat_final))
           and blocok_210.mes_periodo_apur =
               extract(month from to_date(p_dat_final))
           and v_controleProcessoBlocoK > 0
           and v_dtIniblok <= p_dat_inicial
           and p_gera_sped_k200 = '1' -- COMPLETO
           group by blocok_210.cod_empresa ,
               blocok_210.cod_item_nivel ,
               blocok_210.cod_item_grupo ,
               blocok_210.cod_item_subgrupo ,
               blocok_210.cod_item_item ,
               nvl(blocok_210.cod_item_est_agrup_insu, 0) ,
               nvl(blocok_210.cod_item_est_simult_insu, 0) 
           )

    union all

      ---------------------- BLOCOK_215 ----------------------------
       (select blocok_215.cod_empresa empresa,
               blocok_215.cod_item_nivel nivel_estrutura,
               blocok_215.cod_item_grupo grupo_estrutura,
               blocok_215.cod_item_subgrupo subgru_estrutura,
               blocok_215.cod_item_item item_estrutura,
               nvl(blocok_215.cod_item_est_agrup_insu, 0) as estagio_agrupador,
               nvl(blocok_215.cod_item_est_simult_insu, 0) as estagio_agrupador_simultaneo,
               sum(blocok_215.qtd_des) qtde,
               0 as seq_agrupador
          from blocok_215
         where blocok_215.cod_empresa = p_cod_empresa
           and blocok_215.ano_periodo_apur =
               extract(year from to_date(p_dat_final))
           and blocok_215.mes_periodo_apur =
               extract(month from to_date(p_dat_final))
           and v_controleProcessoBlocoK > 0
           and v_dtIniblok <= p_dat_inicial
           and p_gera_sped_k200 = '1' -- COMPLETO
           group by blocok_215.cod_empresa ,
               blocok_215.cod_item_nivel ,
               blocok_215.cod_item_grupo ,
               blocok_215.cod_item_subgrupo ,
               blocok_215.cod_item_item ,
               nvl(blocok_215.cod_item_est_agrup_insu, 0) ,
               nvl(blocok_215.cod_item_est_simult_insu, 0) 
           )

    union all

      ---------------------- BLOCOK_220 prod orig----------------------------
       (select blocok_220.cod_empresa empresa,
               blocok_220.cod_item_nivel_ori nivel_estrutura,
               blocok_220.cod_item_grupo_ori grupo_estrutura,
               blocok_220.cod_item_subgrupo_ori subgru_estrutura,
               blocok_220.cod_item_item_ori item_estrutura,
               nvl(blocok_220.cod_item_est_agrup_insu_ori, 0) as estagio_agrupador,
               nvl(blocok_220.cod_item_est_simult_insu_ori, 0) as estagio_agrupador_simultaneo,
               sum(blocok_220.qtd_ori) qtde,
               nvl(blocok_220.COD_ITEM_SEQ_OPER_AGRUP_ORI, 0) as seq_agrupador
          from blocok_220
         where blocok_220.cod_empresa = p_cod_empresa
           and blocok_220.ano_periodo_apur =
               extract(year from to_date(p_dat_final))
           and blocok_220.mes_periodo_apur =
               extract(month from to_date(p_dat_final))
           and v_controleProcessoBlocoK > 0
           and v_dtIniblok <= p_dat_inicial
           and p_gera_sped_k200 <> 'N' --S gerar bloco K.
           group by blocok_220.cod_empresa ,
               blocok_220.cod_item_nivel_ori ,
               blocok_220.cod_item_grupo_ori ,
               blocok_220.cod_item_subgrupo_ori ,
               blocok_220.cod_item_item_ori ,
               nvl(blocok_220.cod_item_est_agrup_insu_ori, 0) ,
               nvl(blocok_220.cod_item_est_simult_insu_ori, 0),  
               nvl(blocok_220.COD_ITEM_SEQ_OPER_AGRUP_ORI, 0)
           )

    union all

      ---------------------- BLOCOK_220 prod dest----------------------------
       (select blocok_220.cod_empresa empresa,
               blocok_220.cod_item_nivel_dest nivel_estrutura,
               blocok_220.cod_item_grupo_dest grupo_estrutura,
               blocok_220.cod_item_subgrupo_dest subgru_estrutura,
               blocok_220.cod_item_item_dest item_estrutura,
               nvl(blocok_220.cod_item_est_agrup_insu_dest, 0) as estagio_agrupador,
               nvl(blocok_220.cod_item_est_simult_insu_dest, 0) as estagio_agrupador_simultaneo,
               sum(blocok_220.qtd_dest) qtde,
               nvl(blocok_220.COD_ITEM_SEQ_OPER_AGRUP_DEST, 0) as seq_agrupador
          from blocok_220
         where blocok_220.cod_empresa = p_cod_empresa
           and blocok_220.ano_periodo_apur =
               extract(year from to_date(p_dat_final))
           and blocok_220.mes_periodo_apur =
               extract(month from to_date(p_dat_final))
           and v_controleProcessoBlocoK > 0
           and v_dtIniblok <= p_dat_inicial
           and p_gera_sped_k200 <> 'N' --S gerar bloco K.
          group by blocok_220.cod_empresa ,
               blocok_220.cod_item_nivel_dest ,
               blocok_220.cod_item_grupo_dest ,
               blocok_220.cod_item_subgrupo_dest ,
               blocok_220.cod_item_item_dest ,
               nvl(blocok_220.cod_item_est_agrup_insu_dest, 0),
               nvl(blocok_220.cod_item_est_simult_insu_dest, 0),                
               nvl(blocok_220.COD_ITEM_SEQ_OPER_AGRUP_DEST, 0)
           )

    union all
      ---------------------- BLOCOK_230 ----------------------------
       (select blocok_230.cod_empresa empresa,
               blocok_230.cod_item_nivel nivel_estrutura,
               blocok_230.cod_item_grupo grupo_estrutura,
               blocok_230.cod_item_subgrupo subgru_estrutura,
               blocok_230.cod_item_item item_estrutura,
               nvl(blocok_230.cod_item_est_agrup, 0) as estagio_agrupador,
               0 as estagio_agrupador_simultaneo,
               sum(decode(blocok_230.qtd_enc,0,1, blocok_230.qtd_enc)) qtde,
               nvl(blocok_230.sequencia_operacao_agrupador, 0) as seq_agrupador
          from blocok_230
         where blocok_230.cod_empresa = p_cod_empresa
           and blocok_230.ano_periodo_apur =
               extract(year from to_date(p_dat_final))
           and blocok_230.mes_periodo_apur =
               extract(month from to_date(p_dat_final))
           and v_controleProcessoBlocoK > 0
           and v_dtIniblok <= p_dat_inicial
           and p_gera_sped_k200 <> 'N' --S gerar bloco K.

--           and ((blocok_230.qtd_enc > 0)
--           or trim(blocok_230.dt_fin_op) is null and blocok_230.qtd_enc = 0)
           
--           and ((trim(blocok_230.dt_fin_op) is not null  and blocok_230.qtd_enc > 0)
--              or (trim(blocok_230.dt_fin_op) is null and blocok_230.qtd_enc = 0))
              
          group by blocok_230.cod_empresa ,
               blocok_230.cod_item_nivel ,
               blocok_230.cod_item_grupo ,
               blocok_230.cod_item_subgrupo ,
               blocok_230.cod_item_item,
                nvl(blocok_230.cod_item_est_agrup, 0),
                blocok_230.sequencia_operacao_agrupador 
           )

    union all
      ---------------------- BLOCOK_250 ----------------------------
       (select blocok_250.cod_empresa empresa,
               blocok_250.cod_item_nivel nivel_estrutura,
               blocok_250.cod_item_grupo grupo_estrutura,
               blocok_250.cod_item_subgrupo subgru_estrutura,
               blocok_250.cod_item_item item_estrutura,
               nvl(blocok_250.cod_item_est_agrup, 0) as estagio_agrupador,
               0 as estagio_agrupador_simultaneo,
               sum(blocok_250.qtd) qtde,
               0 as seq_agrupador
          from blocok_250
         where blocok_250.cod_empresa = p_cod_empresa
           and blocok_250.ano_periodo_apur =
               extract(year from to_date(p_dat_final))
           and blocok_250.mes_periodo_apur =
               extract(month from to_date(p_dat_final))
           and v_controleProcessoBlocoK > 0
           and v_dtIniblok <= p_dat_inicial
           and p_gera_sped_k200 <> 'N' --S gerar bloco K.
          group by blocok_250.cod_empresa ,
               blocok_250.cod_item_nivel ,
               blocok_250.cod_item_grupo ,
               blocok_250.cod_item_subgrupo ,
               blocok_250.cod_item_item ,
                nvl(blocok_250.cod_item_est_agrup, 0)
           )

   union all
      ---------------------- BLOCOK_291 ----------------------------
       (select blocok_291.cod_empresa empresa,
               blocok_291.cod_item_nivel nivel_estrutura,
               blocok_291.cod_item_grupo grupo_estrutura,
               blocok_291.cod_item_subgrupo subgru_estrutura,
               blocok_291.cod_item_item item_estrutura,
               nvl(blocok_291.cod_item_est_agrup, 0) as estagio_agrupador,
               nvl(blocok_291.cod_item_est_agrup_simult,0) as estagio_agrupador_simultaneo,
               sum(blocok_291.qtd) qtde,
               0 as seq_agrupador
          from blocok_291
         where blocok_291.cod_empresa = p_cod_empresa
           and blocok_291.ano_periodo_apur =
               extract(year from to_date(p_dat_final))
           and blocok_291.mes_periodo_apur =
               extract(month from to_date(p_dat_final))
           and v_controleProcessoBlocoK > 0
           and v_dtIniblok <= p_dat_inicial
           and p_gera_sped_k200 <> 'N' --S gerar bloco K.
          group by  blocok_291.cod_empresa ,
               blocok_291.cod_item_nivel ,
               blocok_291.cod_item_grupo ,
               blocok_291.cod_item_subgrupo ,
               blocok_291.cod_item_item ,
               nvl(blocok_291.cod_item_est_agrup, 0),
               nvl(blocok_291.cod_item_est_agrup_simult,0)           )

    union all
      ---------------------- BLOCOK_235 ----------------------------
       (select blocok_235.cod_empresa empresa,
               blocok_235.cod_item_nivel nivel_estrutura,
               blocok_235.cod_item_grupo grupo_estrutura,
               blocok_235.cod_item_subgrupo subgru_estrutura,
               blocok_235.cod_item_item item_estrutura,
               nvl(blocok_235.cod_item_est_agrup_insu, 0) as estagio_agrupador,
               nvl(blocok_235.cod_item_est_simult_insu, 0) as estagio_agrupador_simultaneo,
               sum(blocok_235.qtd) qtde,
               nvl(blocok_235.sequencia_operacao_agrupador, 0) as seq_agrupador
          from blocok_235, blocok_230
         where blocok_235.cod_empresa = p_cod_empresa
           and blocok_235.ano_periodo_apur =
               extract(year from to_date(p_dat_final))
           and blocok_235.mes_periodo_apur =
               extract(month from to_date(p_dat_final))
           and v_controleProcessoBlocoK > 0
           and v_dtIniblok <= p_dat_inicial
           and p_gera_sped_k200 = '1' -- COMPLETO
           
           and  blocok_230.cod_empresa = blocok_235.cod_empresa
           and blocok_230.ano_periodo_apur = blocok_235.ano_periodo_apur
           and blocok_230.mes_periodo_apur = blocok_235.mes_periodo_apur
           and ((blocok_230.qtd_enc > 0) or trim(blocok_230.dt_fin_op) is null and blocok_230.qtd_enc = 0)
           and blocok_230.cod_doc_op = blocok_235.cod_doc_op_orig
           and blocok_230.cod_item = blocok_235.cod_item_orig 
           
          group by blocok_235.cod_empresa ,
               blocok_235.cod_item_nivel ,
               blocok_235.cod_item_grupo ,
               blocok_235.cod_item_subgrupo ,
               blocok_235.cod_item_item ,
               nvl(blocok_235.cod_item_est_agrup_insu, 0) ,
               nvl(blocok_235.cod_item_est_simult_insu, 0),
               blocok_235.sequencia_operacao_agrupador
           )

    union all
      ---------------------- BLOCOK_255 ----------------------------
       (select blocok_255.cod_empresa empresa,
               blocok_255.cod_item_nivel nivel_estrutura,
               blocok_255.cod_item_grupo grupo_estrutura,
               blocok_255.cod_item_subgrupo subgru_estrutura,
               blocok_255.cod_item_item item_estrutura,
               nvl(blocok_255.cod_item_est_agrup_insu, 0) as estagio_agrupador,
               nvl(blocok_255.cod_item_est_simult_insu, 0) as estagio_agrupador_simultaneo,
               sum(blocok_255.qtd) qtde,
               0 as seq_agrupador
          from blocok_255
         where blocok_255.cod_empresa = p_cod_empresa
           and blocok_255.ano_periodo_apur =
               extract(year from to_date(p_dat_final))
           and blocok_255.mes_periodo_apur =
               extract(month from to_date(p_dat_final))
           and v_controleProcessoBlocoK > 0
           and v_dtIniblok <= p_dat_inicial
           and p_gera_sped_k200 = '1' -- COMPLETO
          group by blocok_255.cod_empresa ,
               blocok_255.cod_item_nivel ,
               blocok_255.cod_item_grupo ,
               blocok_255.cod_item_subgrupo ,
               blocok_255.cod_item_item ,
               nvl(blocok_255.cod_item_est_agrup_insu, 0) ,
               nvl(blocok_255.cod_item_est_simult_insu, 0)
           )

    union all

         ---------------------- BLOCOK_260 ----------------------------
       (select distinct blocok_260.cod_empresa empresa,
               blocok_260.cod_item_nivel nivel_estrutura,
               blocok_260.cod_item_grupo grupo_estrutura,
               blocok_260.cod_item_subgrupo subgru_estrutura,
               blocok_260.cod_item_item item_estrutura,
               0 as estagio_agrupador,
               0 as estagio_agrupador_simultaneo,
               1 qtde,
               0 as seq_agrupador
          from blocok_260
         where blocok_260.cod_empresa = p_cod_empresa
           and blocok_260.ano_periodo_apur =
               extract(year from to_date(p_dat_final))
           and blocok_260.mes_periodo_apur =
               extract(month from to_date(p_dat_final))
           and v_controleProcessoBlocoK > 0
           and v_dtIniblok <= p_dat_inicial
           and p_gera_sped_k200 = '1' -- COMPLETO
           )

    union all

      ---------------------- BLOCOK_265 ----------------------------
       (select distinct blocok_265.cod_empresa empresa,
               blocok_265.cod_item_nivel nivel_estrutura,
               blocok_265.cod_item_grupo grupo_estrutura,
               blocok_265.cod_item_subgrupo subgru_estrutura,
               blocok_265.cod_item_item item_estrutura,
               0 as estagio_agrupador,
               0 as estagio_agrupador_simultaneo,
               1 qtde,
               0 as seq_agrupador
          from blocok_265
         where blocok_265.cod_empresa = p_cod_empresa
           and blocok_265.ano_periodo_apur =
               extract(year from to_date(p_dat_final))
           and blocok_265.mes_periodo_apur =
               extract(month from to_date(p_dat_final))
           and v_controleProcessoBlocoK > 0
           and v_dtIniblok <= p_dat_inicial
           and p_gera_sped_k200 = '1' -- COMPLETO
           )

    union all

      ---------------------- BLOCOK_292 ----------------------------
       (select blocok_292.cod_empresa empresa,
               blocok_292.cod_item_nivel nivel_estrutura,
               blocok_292.cod_item_grupo grupo_estrutura,
               blocok_292.cod_item_subgrupo subgru_estrutura,
               blocok_292.cod_item_item item_estrutura,
               nvl(blocok_292.cod_item_est_agrup_insu, 0) as estagio_agrupador,
               nvl(blocok_292.cod_item_est_simult_insu, 0) as estagio_agrupador_simultaneo,
               sum(blocok_292.qtd) qtde,
               0 as seq_agrupador
          from blocok_292
         where blocok_292.cod_empresa = p_cod_empresa
           and blocok_292.ano_periodo_apur =
               extract(year from to_date(p_dat_final))
           and blocok_292.mes_periodo_apur =
               extract(month from to_date(p_dat_final))
           and v_controleProcessoBlocoK > 0
           and v_dtIniblok <= p_dat_inicial
           and p_gera_sped_k200 <> 'N' --S gerar bloco K.
           and p_gera_sped_k200 = '1' -- COMPLETO
          group by blocok_292.cod_empresa ,
               blocok_292.cod_item_nivel ,
               blocok_292.cod_item_grupo ,
               blocok_292.cod_item_subgrupo ,
               blocok_292.cod_item_item ,
               nvl(blocok_292.cod_item_est_agrup_insu, 0) ,
               nvl(blocok_292.cod_item_est_simult_insu, 0) 
           )
     )
     
  
     group by cod_empresa, cod_nivel, cod_grupo, cod_subgru, cod_item, estagio_estrutura, estagio_agrupador_simultaneo, seq_agrupador
     having sum(qtde) <> 0
;



CURSOR u_basi_010 (p_cod_nivel          VARCHAR2,
                   p_cod_grupo          VARCHAR2,
                   p_cod_subgru         VARCHAR2,
                   p_cod_item           VARCHAR2) IS
   select basi_010.nivel_estrutura,     basi_010.grupo_estrutura,        decode(p_cod_subgru,null,' ',basi_010.subgru_estrutura) as subgru_estrutura,
         decode(p_cod_item,null,' ',basi_010.item_estrutura) as item_estrutura,      basi_010.narrativa,              basi_010.codigo_barras,
          basi_010.classific_fiscal,    basi_030.unidade_medida,         basi_240.codigo_ex_ipi,
          basi_240.aliquota_icms_sped,  basi_240.cod_enquadramento_ipi,
          basi_150.tipo_produto_sped,   basi_240.aliquota_ipi,           basi_010.cod_servico_lst, basi_150.conta_estoque,
          basi_010.cest
   from   basi_010,
          basi_240,
          basi_150,
          basi_030
   where  basi_010.classific_fiscal   = basi_240.classific_fiscal
   and    basi_010.nivel_estrutura    = basi_030.nivel_estrutura
   and    basi_010.grupo_estrutura    = basi_030.referencia
   and    basi_030.conta_estoque      = basi_150.conta_estoque
   and    basi_010.nivel_estrutura    = p_cod_nivel
   and    basi_010.grupo_estrutura    = p_cod_grupo
   and   (basi_010.subgru_estrutura   = p_cod_subgru or p_cod_subgru is null)
   and   (basi_010.item_estrutura     = p_cod_item   or p_cod_item   is null)
   and    rownum                      = 1
   group by basi_010.nivel_estrutura,     basi_010.grupo_estrutura,
            decode(p_cod_subgru,null,' ',basi_010.subgru_estrutura),
            decode(p_cod_item,null,' ',basi_010.item_estrutura),
            basi_010.narrativa,           basi_010.codigo_barras,
            basi_010.classific_fiscal,    basi_030.unidade_medida,         basi_240.codigo_ex_ipi,
            basi_240.aliquota_icms_sped,  basi_240.cod_enquadramento_ipi,
            basi_150.tipo_produto_sped,   basi_240.aliquota_ipi,           basi_010.cod_servico_lst,
            basi_150.conta_estoque, basi_010.cest;



CURSOR u_sped_0200_G (p_cod_empresa     NUMBER) IS
select distinct descricao_bem,nivel_prod, grupo_prod,subgrupo_prod, item_prod from (
 select max(obrf_301.descricao_bem) as descricao_bem, obrf_141.nivel_prod, obrf_141.grupo_prod,obrf_141.subgrupo_prod, decode(obrf_141.item_prod, null, '000000', obrf_141.item_prod) item_prod
  from obrf_141, obrf_301
  where obrf_141.codigo_bem  = obrf_301.codigo_bem
  and   obrf_141.cod_empresa = obrf_301.cod_empresa
  and   obrf_141.cod_empresa = p_cod_empresa
  and   obrf_141.periodo_ini >= p_dat_inicial
  and   obrf_141.periodo_fim <= (p_dat_final)
  and not exists ( select 1
                   from basi_010
                   where basi_010.nivel_estrutura = obrf_141.nivel_prod
                   and basi_010.grupo_estrutura = obrf_141.grupo_prod )
  group by obrf_141.nivel_prod,    obrf_141.grupo_prod,
           obrf_141.subgrupo_prod, obrf_141.item_prod);






-- UNIDADE DE MEDIDA 0190

CURSOR u_sped_0200 (p_cod_empresa     NUMBER) IS
   SELECT DISTINCT  cod_empresa, cod_unid_medida
   from(
   (SELECT DISTINCT  sped_c170.cod_empresa,
    decode(sped_c170.Qtde_Conver, 0, sped_c170.Cod_Unid_Medida, sped_c170.Unid_Med_Conver) cod_unid_medida
   FROM   sped_c170,
          sped_c100,
          fatu_504
   WHERE    sped_c170.cod_empresa     = sped_c100.cod_empresa
   and      fatu_504.codigo_empresa   = sped_c100.cod_empresa
   and      sped_c170.num_nota_fiscal = sped_c100.num_nota_fiscal
   and      sped_c170.cod_serie_nota  = sped_c100.cod_serie_nota
   and      sped_c170.num_cnpj_9      = sped_c100.num_cnpj_9
   and      sped_c170.num_cnpj_4      = sped_c100.num_cnpj_4
   and      sped_c170.num_cnpj_2      = sped_c100.num_cnpj_2
   and      sped_c170.cod_empresa     = p_cod_empresa
    and (sped_c100.flag_exp                  = 'N'
     or sped_c100.dat_averbacao             <= p_dat_final
       and sped_c100.dat_exp                <= p_dat_final
       and sped_c100.dat_registro_exp       <= p_dat_final
       and sped_c100.dat_conhecimento_emb   <= p_dat_final)
   and      sped_c100.cod_modelo not in ('06','07','08','09','10','11','22','27','28','57', '67')
   and (not ((fatu_504.tipo_sped = 0 and sped_c100.cod_modelo in ('55','65','59') and (sped_c100.tip_entrada_saida = 'S' or sped_c100.tip_emissao_prop = 0)) or (sped_c100.tip_entrada_saida = 'S' and sped_c100.sig_estado = 'EX' and (sped_c100.dat_averbacao     is not null or sped_c100.tip_doc_importacao = 1))))
   and      sped_c100.cod_situacao_nota not in (2,3,5))
union all
   (select distinct sped_h010.cod_empresa, sped_h010.cod_unid_medida
   from sped_h010
   where sped_h010.cod_empresa = p_cod_empresa)
union all
   (select distinct sped_0200.cod_empresa, sped_0200.cod_unid_medida
   from sped_0200
   where sped_0200.cod_empresa = p_cod_empresa)
union all
  (select distinct sped_0220.cod_empresa, sped_0220.cod_unid_cov
   from sped_0220
   where sped_0220.cod_empresa = p_cod_empresa));




CURSOR u_basi_200 (p_cod_unid_medida      basi_200.unidade_medida%TYPE) IS
   SELECT DISTINCT  descr_unidade, unidade_medida
   from(
   (select distinct basi_200.descr_unidade,  basi_200.unidade_medida
   from   basi_200
   where  basi_200.unidade_medida = p_cod_unid_medida)
  union all
    (select distinct basi_201.DESCRICAO_UN_MED_FORN,  basi_201.UNIDADE_MEDIDA_FORN
    from   basi_201
    where  basi_201.UNIDADE_MEDIDA_FORN = p_cod_unid_medida));


BEGIN


   select fatu_502.perfil_sped
   into   w_perfil_empresa
   from fatu_502
   where fatu_502.codigo_empresa = p_cod_empresa;

   select count(*) into temApuracaoNova from obrf_820
   where obrf_820.cod_empresa = p_cod_empresa
   and obrf_820.mes = EXTRACT(MONTH FROM p_dat_inicial)
   and obrf_820.ano = EXTRACT(YEAR FROM p_dat_inicial);

   BEGIN
     select val_dat into v_dtIniblok from empr_008
     where empr_008.codigo_empresa = p_cod_empresa
       and empr_008.param          = 'obrf.dataIniBlocoK';
     exception
             when no_data_found then
             v_dtIniblok := sysdate + 3365;
   END;

   SELECT COUNT(*) INTO v_controleProcessoBlocoK from empr_007 where param = 'obrf.controleProcessoBlocoK' and default_int = 1;
   
   BEGIN
      SELECT val_int 
      INTO v_controleNarrativaBlocoK 
      from empr_008 
      where param = 'obrf.descricaoProdutoEmElaboracaoNF' 
      and codigo_empresa = p_cod_empresa;
      exception
          when no_data_found then
          v_controleNarrativaBlocoK := 0;
    END;
    
    BEGIN
      SELECT val_str 
      INTO v_controleDescSimulBlocoK 
      from empr_008 
      where param = 'obrf.descricaoSimultaneoEmElaboracaoNF' 
      and codigo_empresa = p_cod_empresa;
      exception
          when no_data_found then
          v_controleDescSimulBlocoK := 'S';
    END;

   -- le a tabela de itens dos documentos para buscar os produtos
   FOR sped_c170 IN u_sped_c170 (p_cod_empresa)
   LOOP

      w_ind_achou := 'N';

      if trim(sped_c170.cod_nivel) is null and trim(sped_c170.cod_item) is null
      then
         ws_cod_unid_medida := '';

         begin
            SELECT i.cod_unid_medida into ws_cod_unid_medida
            FROM   sped_c170 i,
                   sped_c100 c
            WHERE  i.cod_empresa     = c.cod_empresa
            and    i.num_nota_fiscal = c.num_nota_fiscal
            and    i.cod_serie_nota  = c.cod_serie_nota
            and    i.num_cnpj_9      = c.num_cnpj_9
            and    i.num_cnpj_4      = c.num_cnpj_4
            and    i.num_cnpj_2      = c.num_cnpj_2
            and    i.cod_empresa     = p_cod_empresa
            and    c.cod_modelo not in ('06','07','08','09','10','11','22','27','28','57','65','59')
            and    c.cod_situacao_nota not in (2,3,5)
            and    i.cod_nivel              = sped_c170.cod_nivel
            and    i.cod_grupo              = sped_c170.cod_grupo
            and    i.cod_subgru             = sped_c170.cod_subgru
            and    i.cod_item               = sped_c170.cod_item
            and    rownum                   = 1;
         exception
             when no_data_found then
              ws_cod_unid_medida := '';
         end;

         begin
            select basi_240.codigo_ex_ipi,          basi_240.aliquota_icms_sped,
                   basi_240.cod_enquadramento_ipi,  basi_240.aliquota_ipi,
                   REPLACE(basi_240.classific_fiscal,'.'),       trim(basi_240.descr_class_fisc)
            into   w_ex_ipi,                        ws_aliquota_icms_sped,
                   ws_cod_enquadramento_ipi,        ws_aliquota_ipi,
                   w_cod_ncm,                       ws_descr_class_fisc
            from basi_240
            where REPLACE(basi_240.classific_fiscal,'.') = trim(sped_c170.cod_grupo) || trim(sped_c170.cod_subgru)
            and    rownum                   = 1;
         exception
             when no_data_found then
                w_ex_ipi                 := 0;
                ws_aliquota_icms_sped    := 0.00;
                ws_cod_enquadramento_ipi := '';
                ws_aliquota_ipi          := 0.00;
                w_cod_ncm                := '';
                ws_descr_class_fisc      := '';
         end;

         begin
            select obrf_245.codigo_ex_ipi,   obrf_245.percentual_ipi,
                   1
            into   w_ex_ipi_aux,             ws_aliquota_ipi_aux,
                   w_tem_ex_ipi
            from obrf_245
            where obrf_245.nivel    = sped_c170.cod_nivel
              and obrf_245.grupo    = sped_c170.cod_grupo
              and obrf_245.subgrupo = sped_c170.cod_subgru
              and obrf_245.item     = sped_c170.cod_item;
         exception
             when no_data_found then
                w_ex_ipi_aux := 0;
                ws_aliquota_ipi_aux := 0.00;
                w_tem_ex_ipi := 0;
         end;

         if w_tem_ex_ipi = 1
         then
            w_ex_ipi := w_ex_ipi_aux;
            ws_aliquota_ipi := ws_aliquota_ipi;
         end if;

         begin
            select basi_061.perc_icms
            into ws_icms_sped
            from basi_061
            where basi_061.codigo_empresa = p_cod_empresa
              and REPLACE(basi_061.classific_fiscal,'.') = w_cod_ncm;
         exception
            when no_data_found then
            ws_icms_sped := ws_aliquota_icms_sped;
         end;

         begin
            select basi_010.codigo_barras
            into ws_cod_barras
            from basi_010
            where basi_010.nivel_estrutura = sped_c170.cod_nivel
              and basi_010.grupo_estrutura = sped_c170.cod_grupo
              and basi_010.subgru_estrutura = sped_c170.cod_subgru
              and basi_010.item_estrutura = sped_c170.cod_item
              and (basi_010.codigo_barras like '789%' or basi_010.codigo_barras like '790%');
         exception
            when no_data_found then
            ws_cod_barras := '';
         end;

         begin
            insert into sped_0200
               (cod_nivel,                        cod_grupo,                       cod_subgrupo,
                cod_item,                         des_narrativa,                   cod_barra,
                cod_unid_medida,                  tip_destinacao_item,             num_classifi_fiscal,
                cod_export_ipi,                   cod_genero_item,                 cod_enquadramento_ipi,
                per_icms,                         per_ipi,                         cod_empresa,
                cod_lst,                          cest,                            estagio_agrupador,
                estagio_agrupador_simultaneo,     sequencia_operacao_agrupador)
            values
               (sped_c170.cod_nivel,              sped_c170.cod_grupo,             sped_c170.cod_subgru,
                sped_c170.cod_item,               ws_descr_class_fisc,             ws_cod_barras,
                ws_cod_unid_medida,               7,                               w_cod_ncm,
                w_ex_ipi,                         null,                            ws_cod_enquadramento_ipi,
                ws_icms_sped,                     ws_aliquota_ipi,                 p_cod_empresa,
                null,                             null,                            0,
                0,                                0);

         EXCEPTION
            WHEN OTHERS THEN
               p_des_erro := 'Erro na inclusao da tabela sped_0200 (1)' || Chr(10) ||
                             'Produto: ' || sped_c170.cod_nivel || '-' ||
                                            sped_c170.cod_grupo  || '-' ||
                                            sped_c170.cod_subgru || '-' ||
                                            sped_c170.cod_item || Chr(10) || SQLERRM;
               RAISE W_ERRO;
         END;
      else
         -- le a tabela de itens do Systextil
         /*

            Mudar para leitura simples e ler somente se n?o for virtual, ou seja
            se o codigo estagio agrupador e sumultaneo n?o tiver informado
            Hoje o loop j? ? rownum igual a 1 - leiuta simples

         */
         FOR basi_010 IN u_basi_010 (sped_c170.cod_nivel, sped_c170.cod_grupo, sped_c170.cod_subgru, sped_c170.cod_item)
         LOOP

            w_ind_achou := 'S';


            begin
            select obrf_245.codigo_ex_ipi,   obrf_245.percentual_ipi,
                   1
            into   w_ex_ipi_aux,             ws_aliquota_ipi_aux,
                   w_tem_ex_ipi
            from obrf_245
            where obrf_245.nivel    = sped_c170.cod_nivel
              and obrf_245.grupo    = sped_c170.cod_grupo
              and obrf_245.subgrupo = sped_c170.cod_subgru
              and obrf_245.item     = sped_c170.cod_item;
            exception
                when no_data_found then
                   w_ex_ipi_aux := 0;
                   ws_aliquota_ipi_aux := 0.00;
                   w_tem_ex_ipi := 0;
            end;

            if w_tem_ex_ipi = 1
            then
               basi_010.codigo_ex_ipi := w_ex_ipi_aux;
               basi_010.aliquota_ipi  := ws_aliquota_ipi_aux;
            end if;

            ws_aliquota_icms_sped := basi_010.aliquota_icms_sped;

            -- APARTIR DE 2015, O LST TEM QUE TER O PONTO.
            if to_number(to_char(p_dat_final,'YYYY')) >= 2015
            then
               w_cod_servico_lst := basi_010.cod_servico_lst;
            else
               w_cod_servico_lst := REPLACE(basi_010.cod_servico_lst,'.');
            end if;
            
            w_tipo_produto_sped := basi_010.tipo_produto_sped;
            
            if(sped_c170.estagio_estrutura + sped_c170.estagio_agrupador_simultaneo) > 0
            then
               w_tipo_produto_sped := 3;
            end if;
            

            w_tipo_produto_sped := inter_fn_tp_prod_sped_recur(p_cod_empresa, basi_010.conta_estoque, sped_c170.cod_nivel, sped_c170.cod_grupo, sped_c170.cod_subgru, sped_c170.cod_item, 
                        w_tipo_produto_sped, sped_c170.estagio_estrutura);

            v_narrativa := basi_010.narrativa;

            if(sped_c170.estagio_estrutura + sped_c170.estagio_agrupador_simultaneo) > 0
            then

               w_cod_servico_lst := '';
               basi_010.codigo_barras := '';

               --Verifica se ir??? atribuir estagio agrupador a narrativa do produto.
               case v_controleDescSimulBlocoK
                 when 'N' then
                 v_estagio_agrupador_simultaneo := 0;
                 ELSE
                 v_estagio_agrupador_simultaneo := sped_c170.estagio_agrupador_simultaneo;
               end case;
               --Cria narrativa conforme parametro por empresa, coloca estagios na frente ou atr???s.
               case v_controleNarrativaBlocoK
                 when 2 then
                 v_narrativa := inter_fn_descr_blk(sped_c170.estagio_estrutura, v_estagio_agrupador_simultaneo) || ' ' || basi_010.narrativa ;
                 ELSE
                 v_narrativa := basi_010.narrativa || ' ' || inter_fn_descr_blk(sped_c170.estagio_estrutura, v_estagio_agrupador_simultaneo);
               end case;

               --basi_010.unidade_medida basi_030
               basi_010.classific_fiscal := inter_fn_get_classific(sped_c170.cod_nivel, sped_c170.cod_grupo, sped_c170.cod_subgru, sped_c170.cod_item, sped_c170.estagio_estrutura, sped_c170.estagio_agrupador_simultaneo);
               basi_010.cod_enquadramento_ipi :=  999;
               basi_010.codigo_ex_ipi :=  0;
               basi_010.cest  := '';
            end if;

            -- verifica se o tamanho da classificacao fiscal esta correta, caso nao esteja, gera mensagem de erro
            IF  Nvl(Length(REPLACE(basi_010.classific_fiscal,'.')),0) > 8
            THEN

               inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                   'Procedure: p_gera_sped_0200 ' || Chr(10) ||
                                   'Tamanho do campo classificacao fiscal e maior que o permitido ' || Chr(10) ||
                                   '   Produto: ' || sped_c170.cod_nivel || '-' ||
                                                     sped_c170.cod_grupo  || '-' ||
                                                     sped_c170.cod_subgru || '-' ||
                                                     sped_c170.cod_item   || Chr(10) ||
                                   '   Classif. Fiscal ' || basi_010.classific_fiscal);
                basi_010.classific_fiscal := substr(trim(REPLACE(basi_010.classific_fiscal,'.')),1,8);
            END IF;


            begin
               select basi_061.perc_icms
               into ws_icms_sped
               from basi_061
               where basi_061.codigo_empresa = p_cod_empresa
                 and substr(REPLACE(basi_061.classific_fiscal,'.'),1,8) = substr(REPLACE(basi_010.classific_fiscal,'.'),1,8);
            exception
               when no_data_found then
               ws_icms_sped := ws_aliquota_icms_sped;
            end;


            -- insere o registro do produto na base de dados
            begin
               insert into sped_0200
                  (cod_nivel,                        cod_grupo,                       cod_subgrupo,
                   cod_item,                         des_narrativa,                   cod_barra,
                   cod_unid_medida,                  tip_destinacao_item,             num_classifi_fiscal,
                   cod_export_ipi,                   cod_genero_item,                 cod_enquadramento_ipi,
                   per_icms,                         per_ipi,                         cod_empresa,
                   cod_lst,                          cest,                            estagio_agrupador,
                   estagio_agrupador_simultaneo,     sequencia_operacao_agrupador)
               values
                  (basi_010.nivel_estrutura,         basi_010.grupo_estrutura,        basi_010.subgru_estrutura,
                   basi_010.item_estrutura,          trim(v_narrativa),               basi_010.codigo_barras,
                   basi_010.unidade_medida,          w_tipo_produto_sped,             substr(REPLACE(basi_010.classific_fiscal,'.'),1,8),
                   basi_010.codigo_ex_ipi,           null,                            trim(basi_010.cod_enquadramento_ipi),
                   ws_icms_sped,                     basi_010.aliquota_ipi,           p_cod_empresa,
                   w_cod_servico_lst,                basi_010.cest,                   sped_c170.estagio_estrutura,
                   sped_c170.estagio_agrupador_simultaneo, nvl(sped_c170.seq_agrupador,0));

            EXCEPTION
               WHEN OTHERS THEN
                  p_des_erro := 'Erro na inclusao da tabela sped_0200 (2)' || Chr(10) ||
                                'Produto: ' || sped_c170.cod_nivel || '-' ||
                                               sped_c170.cod_grupo  || '-' ||
                                               sped_c170.cod_subgru || '-' ||
                                               sped_c170.cod_item || Chr(10) || SQLERRM;
                  RAISE W_ERRO;
            END;
         END LOOP;
      end if;

      IF  w_ind_achou = 'N'
      THEN
         inter_pr_insere_erro_sped ('F',p_cod_empresa,
                             'Nao achou o produto nos cadastros de produto do sistema ' || Chr(10) ||
                             '   Produto: ' || sped_c170.cod_nivel || '-' ||
                                               sped_c170.cod_grupo  || '-' ||
                                               sped_c170.cod_subgru || '-' ||
                                               sped_c170.cod_item);
      END IF;
   END LOOP;


   --
   -- EXPORTAR ITENS DO BLOCO G ( IMPORTACAO )
   --

   FOR sped_0200_G in u_sped_0200_G(p_cod_empresa)
   LOOP


     select ltrim(to_char(decode(max(sped_0200.cod_item), ' ','000000', null,'000000', (max(sped_0200.cod_item)+1)  ),'000000'),' ') into w_max_item from sped_0200
     where sped_0200.cod_empresa = p_cod_empresa
       and sped_0200.cod_nivel = 'X'
       and sped_0200.cod_grupo = sped_0200_G.Grupo_Prod
       and sped_0200.cod_subgrupo = sped_0200_G.Subgrupo_Prod;

         begin
          insert into sped_0200
                  (cod_empresa,                     cod_nivel,            cod_grupo,
                   cod_subgrupo,                    cod_item,             des_narrativa,
                   cod_unid_medida,                 tip_destinacao_item
                   )
               values
                  (p_cod_empresa,sped_0200_g.nivel_prod,sped_0200_g.grupo_prod,sped_0200_g.subgrupo_prod ,sped_0200_G.Item_Prod,sped_0200_G.descricao_bem,'UN','08');

            EXCEPTION
               WHEN OTHERS THEN
                  p_des_erro := 'Erro na inclusao da tabela sped_0200 (2)' || Chr(10) ||
                                                                        Chr(10) || SQLERRM;
                  RAISE W_ERRO;
                  END;
    END LOOP;

   --
   -- Ler todas as unidades de medida gravadas nos produtos
   --

   FOR sped_0200 IN u_sped_0200 (p_cod_empresa)
   LOOP

      w_ind_achou := 'N';

      -- le os dados dos cadastro de unidade de medida
      FOR basi_200 IN u_basi_200 (sped_0200.cod_unid_medida)
      LOOP

         w_ind_achou := 'S';

         begin
            insert into sped_0190
               (cod_empresa,
                cod_unid_medida,
                des_unid_medida)
            VALUES
               (p_cod_empresa,
                basi_200.unidade_medida,
                trim(basi_200.descr_unidade));

         EXCEPTION
               WHEN Dup_Val_On_Index THEN
                  NULL;
               WHEN OTHERS THEN
               p_des_erro := 'Erro na inclusao da tabela sped_0190 ' || Chr(10) ||
                             'Unidade de Medida: ' || basi_200.unidade_medida || Chr(10) || SQLERRM;
               RAISE W_ERRO;
         END;
      END LOOP;

      IF  w_ind_achou = 'N'
      THEN
         inter_pr_insere_erro_sped ('F',p_cod_empresa,
                             'Nao achou a unidade de medida nos cadastros do sistema ' || Chr(10) ||
                             '   Unidade de Medida: ' || sped_0200.cod_unid_medida);
      END IF;
   END LOOP;

   COMMIT;

EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_0200 ' || Chr(10) || p_des_erro;

   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure p_gera_sped_0200 ' || Chr(10) || SQLERRM;
END inter_pr_gera_sped_0200;

/
