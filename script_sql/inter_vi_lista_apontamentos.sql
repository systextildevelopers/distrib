create or replace view inter_vi_lista_apontamentos as
select  pcpc_045.codigo_usuario,
        pcpc_045.usuario_systextil,
        pcpc_045.processo_systextil as programa,
        pcpc_045.data_producao,
        pcpc_045.hora_producao,
        pcpc_045.pcpc040_estconf as codigo_estagio,
        mqop_005.descricao as desc_estagio,
        pcpc_045.codigo_familia familia,
        pcpc_045.turno_producao,
        pcpc_045.ordem_producao,
        pcpc_045.pcpc040_perconf as periodo_producao,
        pcpc_045.pcpc040_ordconf as ordem_confeccao,
        (select count(1) from pcpc_040 p
         where p.ordem_producao = pcpc_045.ordem_producao
           and p.codigo_estagio = pcpc_045.pcpc040_estconf
           and p.qtde_pecas_prog <= (p.qtde_pecas_prod + p.qtde_pecas_2a + p.qtde_conserto + p.qtde_perdas)) as pacotes_produzidos_atu,
      --   inter_fn_qtde_ocs_prod(pcpc_045.ordem_producao, pcpc_045.pcpc040_estconf, pcpc_045.data_insercao) as pacotes_produzidos_atu,
        (select count(1)  from pcpc_040 p
         where p.ordem_producao = pcpc_045.ordem_producao
           and p.codigo_estagio = pcpc_045.pcpc040_estconf) as pacotes_total,
        pcpc_040.qtde_pecas_prog as quantidade_ordem_corte,
        pcpc_045.qtde_produzida,
        pcpc_045.qtde_pecas_2a,
        pcpc_045.qtde_conserto,
        pcpc_045.qtde_perdas,
        pcpc_040.proconf_nivel99 as produto_niv,
        pcpc_040.proconf_grupo as produto_gru,
        pcpc_040.proconf_subgrupo as produto_sub,
        pcpc_040.proconf_item as produto_ite,
        basi_010.narrativa as narrativa,
        pcpc_045.numero_ordem as numero_os
from pcpc_045, pcpc_040, basi_010, mqop_005
where pcpc_045.pcpc040_estconf = mqop_005.codigo_estagio
  and pcpc_045.pcpc040_perconf = pcpc_040.periodo_producao
  and pcpc_045.pcpc040_ordconf = pcpc_040.ordem_confeccao
  and pcpc_045.pcpc040_estconf = pcpc_040.codigo_estagio
  and pcpc_040.proconf_nivel99 = basi_010.nivel_estrutura
  and pcpc_040.proconf_grupo   = basi_010.grupo_estrutura
  and pcpc_040.proconf_subgrupo = basi_010.subgru_estrutura
  and pcpc_040.proconf_item     = basi_010.item_estrutura
  and pcpc_045.data_insercao > sysdate - 1
order by
    pcpc_045.data_producao desc,
    pcpc_045.hora_producao desc;

/

exec inter_pr_recompile;
