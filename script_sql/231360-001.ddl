alter table fatu_070
add(
  variacao_cambial NUMBER(1)
);

alter table fatu_070
modify variacao_cambial default 0;


comment on column fatu_070.variacao_cambial   is 'Indica se j� sofreu calculo de varia��o cambial.';

