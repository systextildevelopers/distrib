
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_035" 
  before insert or delete on pcpb_035
  for each row
declare
   v_entrada_saida        estq_005.entrada_saida%type;
   v_atualiza_estoque     estq_005.atualiza_estoque%type;
   v_tran_est_producao    estq_005.codigo_transacao%type;
   v_tipo_prod_deposito   basi_205.tipo_prod_deposito%type;
   v_tabela_origem_cardex estq_300.tabela_origem%type := 'PCPB_035';
   v_quantidade_movto     number;

begin
   if inserting
   then
      -- Verificando se a transacao para movimentacao foi atualizada
      if :new.transacao_cardex = 0
      then
         raise_application_error(-20000,'Deve-se informar a transacao de movimentacao para entrada de tecidos acabados das ordens de beneficiamento.');
      end if;

      -- Verificando o tipo de transacao e se a mesma atualiza estoque
      select entrada_saida,    atualiza_estoque
      into   v_entrada_saida,  v_atualiza_estoque
      from estq_005
      where codigo_transacao = :new.transacao_cardex;

      if v_entrada_saida <> 'E'
      then
         raise_application_error(-20000,'Transacao de baixa da ordem de beneficiamento deve ser de entrada.');
      end if;

      if v_atualiza_estoque = 2
      then
         raise_application_error(-20000,'Utilize uma transacao que atualize estoque.');
      end if;

      -- Verificando se foi informada a data de movimento
      if :new.data_cardex is null
      then
         raise_application_error(-20000,'Deve-se informar a data de movimentacao de baixa da ordem de beneficiamento.');
      end if;

      -- Verificando se foi informado o deposito para o movimento
      if :new.codigo_deposito = 0
      then
         raise_application_error(-20000,'Informe o deposito da movimentacao de baixa da ordem de beneficiamento.');
      else
         -- Verificando o tipo de produto do deposito
         select tipo_prod_deposito into v_tipo_prod_deposito
         from basi_205
         where basi_205.codigo_deposito = :new.codigo_deposito;

           if :new.pb35pb20_pano_sbg_nivel99 = '9' and :new.qtde_unidade <> 0.000 -- materiais beneficiados
           then
              v_quantidade_movto := :new.qtde_unidade; -- atualiza com a quantidade de unidade
         else
           if v_tipo_prod_deposito = 2 -- Segunda qualidade
           then
               v_quantidade_movto := :new.qtde_quilos_2q;
           else
               v_quantidade_movto := :new.qtde_quilos_prod;
           end if;
         end if;

         -- Enviando informacoes para atualizar o Cardex
         inter_pr_insere_estq_300 (:new.codigo_deposito,            :new.pb35pb20_pano_sbg_nivel99,   :new.pb35pb20_pano_sbg_grupo,
                                   :new.pb35pb20_pano_sbg_subgrupo, :new.pb35pb20_pano_sbg_item,      :new.data_cardex,
                                   :new.lote_acomp,                 :new.pb35pb20_ordpro20 ,          ' ',
                                   0,                               0,                                0,
                                   :new.sequencia,
                                   :new.transacao_cardex,           'E',                              0,
                                   v_quantidade_movto,              :new.preco_unitario_cardex,       :new.preco_unitario_cardex,
                                   :new.usuario_cardex,             v_tabela_origem_cardex,           :new.nome_programa,
                                   0.00);
      end if;  -- if :new.codigo_deposito = 0
   end if;     -- if inserting

   if deleting
   then
      -- Verificando a transacao de cancelamento da producao
      select tran_est_produca into v_tran_est_producao from empr_001;

      -- Verificando o tipo de transacao e se a mesma atualiza estoque
      select entrada_saida,    atualiza_estoque
      into   v_entrada_saida,  v_atualiza_estoque
      from estq_005
      where codigo_transacao = v_tran_est_producao;

      if v_entrada_saida <> 'S'
      then
         raise_application_error(-20000,'Transacao para estorno de producao deve ser de saida.');
      end if;

      if v_atualiza_estoque = 2
      then
         raise_application_error(-20000,'Utilize uma transacao que atualize estoque.');
      else
         -- Verificando o tipo de produto do deposito
         select tipo_prod_deposito into v_tipo_prod_deposito
         from basi_205
         where basi_205.codigo_deposito = :old.codigo_deposito;

           if :old.pb35pb20_pano_sbg_nivel99 = '9' and :old.qtde_unidade <> 0.000 -- materiais beneficiados
           then
              v_quantidade_movto := :old.qtde_unidade; -- atualiza com a quantidade de unidade
           else
            if v_tipo_prod_deposito = 2 -- Segunda qualidade
            then
               v_quantidade_movto := :old.qtde_quilos_2q;
            else
               v_quantidade_movto := :old.qtde_quilos_prod;
            end if;
         end if;

         -- Enviando informacoes para atualizar o Cardex
         inter_pr_insere_estq_300 (:old.codigo_deposito,            :old.pb35pb20_pano_sbg_nivel99,   :old.pb35pb20_pano_sbg_grupo,
                                   :old.pb35pb20_pano_sbg_subgrupo, :old.pb35pb20_pano_sbg_item,      sysdate,
                                   :old.lote_acomp,                 :old.pb35pb20_ordpro20 ,          ' ',
                                   0,                               0,                                0,
                                   :old.sequencia,
                                   v_tran_est_producao,             'S',                              0,
                                   v_quantidade_movto,              :old.preco_unitario_cardex,       :old.preco_unitario_cardex,
                                   'DELECAO',                       v_tabela_origem_cardex,           ' ',
                                   0.00);

      end if;  -- if v_atualiza_estoque = 2
   end if;     -- if deleting

   -- zera campos de controle da ficha cardex, assim nao ha perigo da trigger atualizar
   -- o estoque com dados antigos, caso o usuario nao passar estes parametros
   if inserting
   then
      :new.transacao_cardex     := 0;
      :new.usuario_cardex       := ' ';
      :new.nome_programa        := ' ';
      :new.data_cardex          := null;
   end if;
end inter_tr_pcpb_035;

-- ALTER TRIGGER "INTER_TR_PCPB_035" ENABLE
 

/

exec inter_pr_recompile;

