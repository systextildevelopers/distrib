create table pcpb_814
( ID 					NUMBER(9)      	DEFAULT 0 NOT NULL,
  TIPO_ORDEM            VARCHAR2(1)		DEFAULT ' ',
  LISTA_OB				VARCHAR2(255)	DEFAULT ' ',
  OBSERVACAO			VARCHAR2(255)	DEFAULT ' ',
  DATA_INSERCAO			DATE			DEFAULT sysdate,
  OT_GERADA				NUMBER(9)		DEFAULT 0);
  
alter table pcpb_814
   ADD CONSTRAINT PK_PCPB_814 PRIMARY KEY (ID);

create synonym systextilrpt.pcpb_814 for pcpb_814;

comment on table pcpb_814 is 'Cadastro de Ordens de Tingimento';

comment on column pcpb_814.TIPO_ORDEM is 'Tingimento ou Lavação';
comment on column pcpb_814.LISTA_OB is 'Lista com várias OB para fazer Tingimento ou Lavação';
/
