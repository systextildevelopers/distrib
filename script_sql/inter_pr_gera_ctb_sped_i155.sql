create or replace procedure inter_pr_gera_ctb_sped_i155(p_cod_empresa   IN NUMBER,
                                                        p_exercicio     IN NUMBER,
                                                        p_cod_plano_cta IN NUMBER,
                                                        p_per_inicial   IN  NUMBER,
                                                        p_per_final     IN  NUMBER,
                                                        p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar a tabela sped_cta_i155 - Saldos por periodo
   --
   -- Historicos
   --
   -- Data    Autor    Observacoes
   --
--SS225197-011
     cursor cont610(p_cod_empresa NUMBER, p_exercicio NUMBER, p_cod_reduzido NUMBER) IS
        select cont_610.periodo,             cont_610.conta_reduzida,
               cont_610.total_debito,        cont_610.total_credito,
               cont_610.saldo_periodo
        from   cont_610
        where  cont_610.cod_empresa   = p_cod_empresa
        and    cont_610.exercicio     = p_exercicio
        and    cont_610.periodo       between p_per_inicial - 1 and p_per_final
        and    cont_610.conta_reduzida = p_cod_reduzido
        and    cont_610.filial        = 0
        order  by cont_610.conta_reduzida, cont_610.periodo;
     
     cursor cont535(p_cod_plano_cta NUMBER) is
        select distinct cont_535.cod_reduzido,       cont_535.conta_contabil,
               cont_535.subconta,           cont_535.tipo_conta,
               cont_535.patr_result,        cont_535.exige_subconta
        from   cont_610, cont_535
        where  cont_610.cod_empresa   = p_cod_empresa
        and    cont_610.exercicio     = p_exercicio
        and    cont_610.periodo       between p_per_inicial - 1 and p_per_final
        and    cont_535.cod_plano_cta = p_cod_plano_cta
        and    cont_535.cod_reduzido  = cont_610.conta_reduzida
        and    cont_610.filial        = 0
        and    cont_535.tipo_conta    = 1
        order  by cont_535.cod_reduzido;
          
     cursor cont600(p_cod_empresa NUMBER, p_exercicio NUMBER, p_cod_reduzido NUMBER) IS
        select cont_600.periodo,            cont_600.conta_reduzida, cont_600.centro_custo,
               sum(decode(cont_600.debito_credito, 'D', cont_600.valor_lancto, 0)) total_debito,
               sum(decode(cont_600.debito_credito, 'C', cont_600.valor_lancto,0)) total_credito,               
               sum(decode(cont_600.debito_credito, 'D', cont_600.valor_lancto, cont_600.valor_lancto * (-1.0))) saldo_periodo
        from cont_600
        where cont_600.cod_empresa         = p_cod_empresa
          and cont_600.exercicio           = p_exercicio
          and cont_600.conta_reduzida      = p_cod_reduzido
          and cont_600.periodo             between p_per_inicial and p_per_final
        group by cont_600.conta_reduzida,cont_600.centro_custo, cont_600.periodo
        order by cont_600.centro_custo, cont_600.conta_reduzida, cont_600.periodo;
     
        
   
    v_erro           EXCEPTION;

    v_per_inicial    cont_510.per_inicial%type;
    v_per_final      cont_510.per_final%type;
    v_conta_anterior varchar2(30);
    v_conta_contabil varchar2(30);
    v_ind_saldo_ini  varchar2(1);
    v_ind_saldo_fim  varchar2(1);

    v_saldo_anterior_ins cont_610.saldo_periodo%type;
    v_saldo_anterior     cont_610.saldo_periodo%type;
    v_saldo_periodo      cont_610.saldo_periodo%type;
    v_ind_considera_c_custos varchar2(1);
    v_total_debito           number;
    v_total_credito          number;
    v_centro_custo_ant       number;
    v_periodo_ant            number;
    v_primeiro_registro      boolean;
    v_cod_cta_reduzido       number;
    v_saldo_ini              number;
    v_saldo_fim              number;
    v_saldo_ant              number;

begin
   
    p_des_erro       := NULL;
    
    select ind_considera_c_custos into v_ind_considera_c_custos from fatu_504
    where  fatu_504.codigo_empresa = p_cod_empresa;

    v_conta_anterior := '##########';
    v_cod_cta_reduzido := 0;
    
    for reg_cont535 in cont535(p_cod_plano_cta)
    loop
      
       if v_ind_considera_c_custos = 'N' or reg_cont535.patr_result = 1
       then
         for reg_cont610 in cont610(p_cod_empresa, p_exercicio, reg_cont535.cod_reduzido)
         loop

             -- monta a conta contabil acrescendo a subconta, quando necessario
             -- se for uma conta de resultado ou se a conta NAO exige subconta ou se a conta for sintetica
             -- a conta sera a propria conta. Caso contrario, concatenea o codigo da conta e o codigo da
             -- subconta

             if reg_cont535.patr_result = 2 or reg_cont535.exige_subconta = 2
             or reg_cont535.tipo_conta  = 2
             then
                v_conta_contabil := reg_cont535.conta_contabil;
             else
                v_conta_contabil := rtrim(reg_cont535.conta_contabil) ||
                                    ltrim(to_char(reg_cont535.subconta, '0000'));
             end if;

             -- encontra o saldo anterior se a conta contabil
             -- apenas para a primeira vez de cada conta
             if v_conta_contabil <> v_conta_anterior
             then
                v_conta_anterior := v_conta_contabil;

                -- se o periodo for zero, e porque o saldo anterior e valor do registro lido
                if reg_cont610.periodo < p_per_inicial
                then

                   v_saldo_anterior := reg_cont610.saldo_periodo;
                else

                   -- caso o primeiro registro nao for o periodo zero, o seta o saldo anterior para zero.
                   v_saldo_anterior := 0.00;

                end if;
             end if;

             -- insere registo do saldo do periodo na tabela de saldos do periodo
             -- se o periodo for zero, nao insere, pois e o saldo anterior
             if reg_cont610.periodo >= p_per_inicial
             then

                begin
                   select per_inicial,   per_final
                   into   v_per_inicial, v_per_final
                   from   cont_510
                   where  cod_empresa = p_cod_empresa
                   and    exercicio   = p_exercicio
                   and    periodo     = reg_cont610.periodo;
                exception
                   when others then
                      v_per_inicial := NULL;
                      v_per_final   := NULL;

                      inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura do periodo  ' || reg_cont610.periodo);

                      commit;

                      RAISE V_ERRO;
                end;

                -- seta o indicador do saldo anterior Devedor ou Credor
                if v_saldo_anterior >= 0.00
                then
                   v_saldo_anterior_ins := v_saldo_anterior;
                   v_ind_saldo_ini      := 'D';
                else
                   v_saldo_anterior_ins := v_saldo_anterior * (-1.00);
                   v_ind_saldo_ini  := 'C';
                end if;

                -- seta o indicador do saldo Devedor ou Credor
                if reg_cont610.saldo_periodo >= 0.00
                then
                   v_ind_saldo_fim := 'D';
                   v_saldo_periodo := reg_cont610.saldo_periodo;
                else
                   v_ind_saldo_fim := 'C';
                   v_saldo_periodo := reg_cont610.saldo_periodo * (-1.0);
                end if;

                if reg_cont610.total_debito  <> 0.00
                or reg_cont610.total_credito <> 0.00 or v_saldo_periodo          <> 0.00
                then
                                  
                   -- insere o registro
                   begin
                      insert into sped_ctb_i155
                         (cod_empresa,                  exercicio,
                          cod_periodo,                  periodo_inicial,
                          periodo_final,                cod_conta,
                          saldo_inicial,                ind_saldo_ini,
                          total_debitos,                total_credito,
                          saldo_final,                  ind_saldo_fim,
                          codigo_reduzido,              centro_custo)
                      values
                         (p_cod_empresa,                p_exercicio,
                          reg_cont610.periodo,          v_per_inicial,
                          v_per_final,                  v_conta_contabil,
                          v_saldo_anterior_ins,         v_ind_saldo_ini,
                          reg_cont610.total_debito,     reg_cont610.total_credito,
                          v_saldo_periodo,              v_ind_saldo_fim,
                          reg_cont610.conta_reduzida,   0 );

                   exception
                      when others then
                         p_des_erro := 'Erro na inclusao da tabela sped_ctb_i155 (1) ' || reg_cont610.conta_reduzida || ' ' || Chr(10) || SQLERRM;
                         RAISE V_ERRO;
                   end;
                end if;

                v_saldo_anterior := reg_cont610.saldo_periodo;
                v_ind_saldo_ini  := v_ind_saldo_fim;
             end if;

          end loop;
       end if;
       
       if v_ind_considera_c_custos = 'S' and reg_cont535.patr_result = 2
       then
          v_saldo_periodo      := 0;
          v_conta_contabil     := '';
          v_saldo_anterior     := 0.00;
          v_saldo_anterior_ins := 0;
          v_centro_custo_ant   := -1;
          v_periodo_ant        := 0;
          v_primeiro_registro  := false;
         
          for reg_cont600 in cont600(p_cod_empresa, p_exercicio,reg_cont535.cod_reduzido)
          loop
            -- monta a conta contabil acrescendo a subconta, quando necessario
             -- se for uma conta de resultado ou se a conta NAO exige subconta ou se a conta for sintetica
             -- a conta sera a propria conta. Caso contrario, concatenea o codigo da conta e o codigo da
             -- subconta

             if reg_cont535.patr_result = 2 or reg_cont535.exige_subconta = 2
             or reg_cont535.tipo_conta  = 2
             then
                v_conta_contabil := reg_cont535.conta_contabil;
             else
                v_conta_contabil := rtrim(reg_cont535.conta_contabil) ||
                                    ltrim(to_char(reg_cont535.subconta, '0000'));
             end if;

             -- encontra o saldo anterior se a conta contabil
             -- apenas para a primeira vez de cada conta
             if reg_cont600.centro_custo <> v_centro_custo_ant or 
                v_conta_contabil <> v_conta_anterior or
                reg_cont600.periodo <> v_periodo_ant
             then
                v_periodo_ant    := reg_cont600.periodo;                
                v_conta_anterior := v_conta_contabil;
                
                -- quando mudar o centro de cutos é o primeiro registro do periodo
                if reg_cont600.centro_custo <> v_centro_custo_ant
                then
                   v_primeiro_registro := true;
                   v_centro_custo_ant := reg_cont600.centro_custo;   
                else 
                   v_primeiro_registro := false;
                end if;
                
             end if;

             -- insere registo do saldo do periodo na tabela de saldos do periodo
             -- se o periodo for zero, nao insere, pois e o saldo anterior
             if reg_cont600.periodo >= p_per_inicial
             then

                
                   -- se for primeiro e unico, só faz o insert
                if v_primeiro_registro
                then                
                   -- se for o primeiro do centro custo, tem que inserir para todos os período
                   -- para os demais so fazer o update depois
                   for per in (select p.periodo from cont_510 p        
                       where p.cod_empresa = p_cod_empresa
                         and p.exercicio = p_exercicio
                         and p.periodo   >= reg_cont600.periodo)
                   loop
                      if reg_cont600.periodo = per.periodo
                      then
                         v_total_debito  := reg_cont600.total_debito;
                         v_total_credito := reg_cont600.total_credito;
                      else
                         v_total_debito  := 0;
                         v_total_credito := 0;
                      end if;
                      
                      begin
                         select per_inicial,   per_final
                         into   v_per_inicial, v_per_final
                         from   cont_510
                         where  cod_empresa = p_cod_empresa
                         and    exercicio   = p_exercicio
                         and    periodo     = per.periodo;
                     exception
                        when others then
                           v_per_inicial := NULL;
                           v_per_final   := NULL;

                          inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura do periodo  ' || reg_cont600.periodo);

                          commit;

                          RAISE V_ERRO;
                      end;
                      -- LOGICA PARA INSERIR OS REGISTROS PARA TODOS OS PERIODOS PARA DEPOIS SER ATUALIZADO 
                      --     CONFORME SALDOS E LANCAMENTOS DO CONT_600
                      begin
                         insert into sped_ctb_i155
                            (cod_empresa,                  exercicio,
                             cod_periodo,                  periodo_inicial,
                             periodo_final,                cod_conta,
                             saldo_inicial,                ind_saldo_ini,
                             total_debitos,                total_credito,
                             saldo_final,                  ind_saldo_fim,
                             codigo_reduzido,              centro_custo)
                         values
                            (p_cod_empresa,                p_exercicio,
                             per.periodo,                  v_per_inicial,
                             v_per_final,                  v_conta_contabil,
                             0,                            '',
                             v_total_debito,               v_total_credito,
                             0,                            '',
                             reg_cont600.conta_reduzida,   reg_cont600.centro_custo );

                      exception
                         when others then
                            p_des_erro := 'Erro na inclusao da tabela sped_ctb_i155 (2) ' || reg_cont600.conta_reduzida || ' ' || Chr(10) || SQLERRM;
                            RAISE V_ERRO;
                      end;
                   end loop;
                else
                   BEGIN
                   update sped_ctb_i155
                      set sped_ctb_i155.total_debitos = reg_cont600.total_debito,
                          sped_ctb_i155.total_credito = reg_cont600.total_credito
                   where sped_ctb_i155.cod_empresa     = p_cod_empresa
                     and sped_ctb_i155.exercicio       = p_exercicio
                     and sped_ctb_i155.cod_periodo     = reg_cont600.periodo
                     and sped_ctb_i155.codigo_reduzido = reg_cont600.conta_reduzida
                     and sped_ctb_i155.centro_custo    = reg_cont600.centro_custo;
                   exception
                      when others then
                         p_des_erro := 'Erro ao atualizar a tabela sped_ctb_i155 (2) ' || reg_cont600.conta_reduzida || ' ' ||  
                                                                                          reg_cont600.periodo || ' ' ||
                                                                                          reg_cont600.centro_custo || ' ' || Chr(10) || SQLERRM;
                         RAISE V_ERRO;
                   end;
                      
                end if;
                
             end if;
            
            
          end loop;
          
          v_centro_custo_ant   := -1;
          v_periodo_ant        := 0;
          v_primeiro_registro := false;
          
          -- LOGICA PARA ATUALIZAR OS SALDOS INICIAIS E FINAIS
          FOR AjusteSld in (
             select s.cod_periodo, s.codigo_reduzido, s.centro_custo, s.total_debitos, s.total_credito
             from sped_ctb_i155 s
             where s.cod_empresa     = p_cod_empresa 
               and s.exercicio       = p_exercicio
               and s.codigo_reduzido = reg_cont535.cod_reduzido
             order by s.centro_custo,s.codigo_reduzido,s.cod_periodo)
          LOOP
          
             -- encontra o saldo anterior se a conta contabil
             -- apenas para a primeira vez de cada conta
             if AjusteSld.centro_custo <> v_centro_custo_ant or 
                v_cod_cta_reduzido     <> reg_cont535.cod_reduzido or
                AjusteSld.cod_periodo      <> v_periodo_ant
             then
                
                v_cod_cta_reduzido := reg_cont535.cod_reduzido;
                v_periodo_ant    := AjusteSld.cod_periodo;
                
                -- quando mudar o centro de cutos é o primeiro registro do periodo
                if AjusteSld.centro_custo <> v_centro_custo_ant
                then
                   v_primeiro_registro := true;
                   v_centro_custo_ant := AjusteSld.centro_custo;
                else 
                   v_primeiro_registro := false;
                end if;
                
             end if;
             
             if v_primeiro_registro
             then
                v_saldo_ini := 0;
                v_saldo_fim := AjusteSld.total_debitos - AjusteSld.total_credito;
                v_saldo_ant := AjusteSld.total_debitos - AjusteSld.total_credito;
             else
                v_saldo_ini := v_saldo_ant;
                v_saldo_fim := v_saldo_ant + AjusteSld.total_debitos - AjusteSld.total_credito;
                v_saldo_ant := v_saldo_fim;
             end if;
                          
             if v_saldo_ini >= 0
             then
                v_ind_saldo_ini := 'D';
             else
                v_saldo_ini := v_saldo_ini * (-1);
                v_ind_saldo_ini := 'C';
             end if;
             
             if v_saldo_fim >= 0
             then
                v_ind_saldo_fim := 'D';
             else
                v_saldo_fim := v_saldo_fim * (-1);
                v_ind_saldo_fim := 'C';
             end if;
             
             BEGIN
                update sped_ctb_i155
                   set sped_ctb_i155.saldo_inicial = v_saldo_ini,
                       sped_ctb_i155.ind_saldo_ini = v_ind_saldo_ini,
                       sped_ctb_i155.saldo_final   = v_saldo_fim,
                       sped_ctb_i155.ind_saldo_fim = v_ind_saldo_fim
                where sped_ctb_i155.cod_empresa     = p_cod_empresa
                  and sped_ctb_i155.exercicio       = p_exercicio
                  and sped_ctb_i155.cod_periodo     = AjusteSld.cod_periodo
                  and sped_ctb_i155.codigo_reduzido = reg_cont535.cod_reduzido
                  and sped_ctb_i155.centro_custo    = AjusteSld.centro_custo;
             exception
                when others then
                   p_des_erro := 'Erro ao atualizar a tabela sped_ctb_i155 (3) ' || reg_cont535.cod_reduzido || ' ' ||  
                                                                                    AjusteSld.cod_periodo || ' ' ||
                                                                                    AjusteSld.centro_custo || ' ' || Chr(10) || SQLERRM;
                   RAISE V_ERRO;
             end;
          END LOOP;
          
          -- LOGICA PARA LIMPAR REGISTROS QUE FICARE ZERADOS
          BEGIN
             delete from sped_ctb_i155
             where sped_ctb_i155.cod_empresa     = p_cod_empresa
               and sped_ctb_i155.exercicio       = p_exercicio
               and sped_ctb_i155.codigo_reduzido = reg_cont535.cod_reduzido
               and sped_ctb_i155.saldo_inicial = 0
               and sped_ctb_i155.total_debitos = 0
               and sped_ctb_i155.total_credito = 0
               and sped_ctb_i155.saldo_final   = 0;
          exception
             when others then
                p_des_erro := 'Erro ao eliminar a tabela sped_ctb_i155 ' || reg_cont535.cod_reduzido || ' ' ||Chr(10) || SQLERRM;
             RAISE V_ERRO;
          end;
       end if;
      
    end loop

    commit;

    exception
    when V_ERRO then
       p_des_erro := 'Erro na procedure inter_pr_gera_ctb_sped_i155 ' || Chr(10) || p_des_erro;

    when others then
       p_des_erro := 'Outros erros na procedure inter_pr_gera_ctb_sped_i155 ' || Chr(10) || SQLERRM;
end inter_pr_gera_ctb_sped_i155;
