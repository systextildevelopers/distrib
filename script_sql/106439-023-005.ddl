declare
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pcpc_f140';

   v_usuario varchar2(15);
   
begin
   for reg_programa in programa
   loop
      begin
         
         update oper_550 
         set acessivel  = 1
         where usuario  = reg_programa.usu_prg_cdusu
         and empresa    = reg_programa.usu_prg_empr_usu
         and nome_programa = reg_programa.programa
         and nome_subprograma = ' '
         and nome_field =  'bt_dep_segunda'
         and acessivel = 0;
         
         commit;
      end;
   end loop;
end;

/

exec inter_pr_recompile;
