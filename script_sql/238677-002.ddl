alter table pedi_051
modify dias_apos_venci default null;

update pedi_051
set dias_apos_venci = null;

alter table pedi_051
modify dias_apos_venci varchar2(255);
