create or replace TRIGGER INTER_TR_PEDI_095_LOG
   after insert
   or delete
   or update of
	   tab_col_tab, tab_mes_tab, tab_seq_tab, nivel_preco,
	   nivel_estrutura, grupo_estrutura, subgru_estrutura, item_estrutura,
	   serie_cor, val_tabela_preco, desconto_maximo, largura,
	   data_form_preco, tipo_valor, cor_pantone, flag_exportacao_loja,
	   flag_agrupador, classe_valor
   on pedi_095
for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   long_aux                  varchar2(2000);
begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      long_aux := '';

      long_aux := long_aux ||
          inter_fn_buscar_tag('lb34888#ITENS DA TABELA DE PRE�O',ws_locale_usuario,ws_usuario_systextil) ||
          chr(10)                  ||
          chr(10);

      long_aux := long_aux                  ||
          inter_fn_buscar_tag('lb34889#N�VEL PRE�O:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :new.nivel_preco ||
                                                chr(10);

      long_aux := long_aux                  ||
          inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :new.nivel_estrutura   || '.'
                                            || :new.grupo_estrutura   || '.'
                                            || :new.subgru_estrutura  || '.'
                                            || :new.item_estrutura    ||
                                                chr(10);

      long_aux := long_aux                  ||
          inter_fn_buscar_tag('lb31673#S�RIE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :new.serie_cor         ||
                                                chr(10);

      long_aux := long_aux                  ||
          inter_fn_buscar_tag('lb06516#PRE�O:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                              || :new.val_tabela_preco  ||
                                                chr(10);



      long_aux := long_aux                  ||
          inter_fn_buscar_tag('lb03107#DESCONTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                              || :new.desconto_maximo   ||
                                                chr(10);

      long_aux := long_aux                  ||
          inter_fn_buscar_tag('lb06048#LARGURA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :new.largura           ||
                                                chr(10);

      long_aux := long_aux                  ||
          inter_fn_buscar_tag('lb00004#DATA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :new.data_form_preco   ||
                                                chr(10);
                                                
      long_aux := long_aux                  ||
          inter_fn_buscar_tag('lb44735#CLASSE DE VALOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                            || :new.classe_valor   ||
                                                chr(10);
      
      INSERT INTO hist_100
         ( tabela,            operacao,
           data_ocorr,        aplicacao,
           usuario_rede,      maquina_rede,
           num02,             num03,
           num04,             long01
           )
      VALUES
        ( 'PEDI_095',        'I',
           sysdate,           ws_aplicativo,
           ws_usuario_rede,   ws_maquina_rede,
           :new.tab_col_tab, :new.tab_mes_tab,
           :new.tab_seq_tab,
           long_aux
              );
   end if;

   if updating
   then
      long_aux := '';

      long_aux := long_aux ||
            inter_fn_buscar_tag('lb34888#ITENS DA TABELA DE PRE�O',ws_locale_usuario,ws_usuario_systextil) ||
            chr(10)                             ||
            chr(10);

      if :old.nivel_preco <> :new.nivel_preco
      then
         long_aux := long_aux                  ||
             inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :new.nivel_estrutura   || '.'
                                               || :new.grupo_estrutura   || '.'
                                               || :new.subgru_estrutura  || '.'
                                               || :new.item_estrutura    ||
                                                   chr(10)               ||
             inter_fn_buscar_tag('lb34889#N�VEL PRE�O:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :old.nivel_preco || '->'
                                               || :new.nivel_preco ||
                                                   chr(10);
      end if;

      if :old.nivel_estrutura  <> :new.nivel_estrutura    or
         :old.grupo_estrutura  <> :new.grupo_estrutura    or
         :old.subgru_estrutura <> :new.subgru_estrutura   or
         :old.item_estrutura   <> :new.item_estrutura
      then
         long_aux := long_aux                  ||
             inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :old.nivel_estrutura   || '.'
                                               || :old.grupo_estrutura   || '.'
                                               || :old.subgru_estrutura  || '.'
                                               || :old.item_estrutura    || '->'
                                               || :new.nivel_estrutura   || '.'
                                               || :new.grupo_estrutura   || '.'
                                               || :new.subgru_estrutura  || '.'
                                               || :new.item_estrutura    ||
                                                   chr(10);
      end if;

      if :old.serie_cor <> :new.serie_cor
      then
         long_aux := long_aux                  ||
             inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :new.nivel_estrutura   || '.'
                                               || :new.grupo_estrutura   || '.'
                                               || :new.subgru_estrutura  || '.'
                                               || :new.item_estrutura    ||
                                                   chr(10)               ||
             inter_fn_buscar_tag('lb31673#S�RIE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :old.serie_cor || '->' ||
                                                  :new.serie_cor         ||
                                                   chr(10);
      end if;

      if :old.val_tabela_preco <> :new.val_tabela_preco
      then
         long_aux := long_aux                  ||
             inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :new.nivel_estrutura   || '.'
                                               || :new.grupo_estrutura   || '.'
                                               || :new.subgru_estrutura  || '.'
                                               || :new.item_estrutura    ||
                                                   chr(10)               ||
             inter_fn_buscar_tag('lb06516#PRE�O:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :old.val_tabela_preco  || '->'
                                               || :new.val_tabela_preco  ||
                                                chr(10);
      end if;

      if :old.desconto_maximo  <> :new.desconto_maximo
      then
         long_aux := long_aux                  ||
             inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :new.nivel_estrutura   || '.'
                                               || :new.grupo_estrutura   || '.'
                                               || :new.subgru_estrutura  || '.'
                                               || :new.item_estrutura    ||
                                                   chr(10)               ||
             inter_fn_buscar_tag('lb03107#DESCONTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :old.desconto_maximo   || '->'
                                                || :new.desconto_maximo   ||
                                                   chr(10);
      end if;

      if :old.largura <> :new.largura
      then
         long_aux := long_aux                  ||
             inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :new.nivel_estrutura   || '.'
                                               || :new.grupo_estrutura   || '.'
                                               || :new.subgru_estrutura  || '.'
                                               || :new.item_estrutura    ||
                                                   chr(10)               ||
             inter_fn_buscar_tag('lb06048#LARGURA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :old.largura           || '->'
                                               || :new.largura           ||
                                                   chr(10);
      end if;

      if :old.data_form_preco  <> :new.data_form_preco
      then
         long_aux := long_aux                  ||
             inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :new.nivel_estrutura   || '.'
                                               || :new.grupo_estrutura   || '.'
                                               || :new.subgru_estrutura  || '.'
                                               || :new.item_estrutura    ||
                                                   chr(10)               ||
             inter_fn_buscar_tag('lb00004#DATA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :old.data_form_preco   || '->'
                                               || :new.data_form_preco   ||
                                                   chr(10);
      end if;
      
      if :old.classe_valor  <> :new.classe_valor
      then
         long_aux := long_aux                  ||
             inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :new.nivel_estrutura   || '.'
                                               || :new.grupo_estrutura   || '.'
                                               || :new.subgru_estrutura  || '.'
                                               || :new.item_estrutura    ||
                                                   chr(10)               ||
             inter_fn_buscar_tag('lb44735#CLASSE DE VALOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                               || :old.classe_valor      || '->'
                                               || :new.classe_valor       ||
                                                   chr(10);
      end if;
      
      INSERT INTO hist_100
      ( tabela,            operacao,
        data_ocorr,        aplicacao,
        usuario_rede,      maquina_rede,
        num02,             num03,
        num04,             long01
        )
      VALUES
      ( 'PEDI_095',        'A',
         sysdate,           ws_aplicativo,
         ws_usuario_rede,   ws_maquina_rede,
         :new.tab_col_tab,  :new.tab_mes_tab,
         :new.tab_seq_tab,
         long_aux
      );
   end if;

   if deleting
   then
       INSERT INTO hist_100
         ( tabela,            operacao,
           data_ocorr,        aplicacao,
           usuario_rede,      maquina_rede,
           num02,             num03,
           num04
           )
      VALUES
        ( 'PEDI_095',        'D',
           sysdate,           ws_aplicativo,
           ws_usuario_rede,   ws_maquina_rede,
           :new.tab_col_tab,  :new.tab_mes_tab,
           :new.tab_seq_tab
              );
   end if;


end inter_tr_pedi_095_log;

-- ALTER TRIGGER "INTER_TR_PEDI_095_LOG" ENABLE

/

exec inter_pr_recompile;

