
  CREATE OR REPLACE PROCEDURE "INTER_PR_TRANSPORTADORA" (p_cod_empresa       in  number,
                                                    p_cnpj9_cliente     in  number,
                                                    p_cnpj4_cliente     in  number,
                                                    p_cnpj2_cliente     in  number,
                                                    p_cod_representante in  number,
                                                    p_estado_atuacao    in  varchar2,
                                                    p_cod_cidade        in  number,
                                                    p_marca             in  number,
                                                    p_cnpj_transp9      out number,
                                                    p_cnpj_transp4      out number,
                                                    p_cnpj_transp2      out number) is

   TYPE cnpjTransp9 IS TABLE OF supr_010.fornecedor9%TYPE;
   cnpj_transp9 cnpjTransp9;

   TYPE cnpjTransp4 IS TABLE OF supr_010.fornecedor4%TYPE;
   cnpj_transp4 cnpjTransp4;

   TYPE cnpjTransp2 IS TABLE OF supr_010.fornecedor2%TYPE;
   cnpj_transp2 cnpjTransp2;

begin

   begin
      select supr_132.cnpj_transp9,
             supr_132.cnpj_transp4,
             supr_132.cnpj_transp2
      BULK COLLECT INTO
             cnpj_transp9,
             cnpj_transp4,
             cnpj_transp2
      from supr_132
      where (supr_132.empresa           = p_cod_empresa       or supr_132.empresa           = 0)
        and (supr_132.cnpj9_cliente     = p_cnpj9_cliente     or supr_132.cnpj9_cliente     = 0)
        and (supr_132.cnpj4_cliente     = p_cnpj4_cliente     or supr_132.cnpj4_cliente     = 0)
        and (supr_132.cnpj2_cliente     = p_cnpj2_cliente     or supr_132.cnpj2_cliente     = 0)
        and (supr_132.cod_representante = p_cod_representante or supr_132.cod_representante = 0)
        and (supr_132.estado_atuacao    = p_estado_atuacao    or supr_132.estado_atuacao    = 'XX')
        and (supr_132.cod_cidade        = p_cod_cidade        or supr_132.cod_cidade        = 0)
        and (supr_132.marca             = p_marca             or supr_132.marca             = 0)
      order by supr_132.empresa desc,           supr_132.cnpj9_cliente desc,
               supr_132.cnpj4_cliente desc,     supr_132.cnpj4_cliente desc,
               supr_132.cod_representante desc, supr_132.cod_cidade desc,
               supr_132.estado_atuacao,         supr_132.marca desc;
      exception
         when others then
            p_cnpj_transp9 := 0;
            p_cnpj_transp4 := 0;
            p_cnpj_transp2 := 0;
   end;

   if sql%found
   then
      FOR i IN cnpj_transp9.FIRST .. cnpj_transp9.LAST
      LOOP

         p_cnpj_transp9 := cnpj_transp9(i);
         p_cnpj_transp4 := cnpj_transp4(i);
         p_cnpj_transp2 := cnpj_transp2(i);

         exit;

      END LOOP;
   else
      p_cnpj_transp9 := 0;
      p_cnpj_transp4 := 0;
      p_cnpj_transp2 := 0;
   end if;

end inter_pr_transportadora;

 

/

exec inter_pr_recompile;

