create  table cont_k210 (
COD_CONTROLADORA    	NUMBER(9) 		DEFAULT 0,
EXERCICIO           	NUMBER(9) 		DEFAULT 0,
COD_CTA 				VARCHAR2(20) 	DEFAULT ' ',
COD_EMP      			NUMBER(4) 		DEFAULT 0,
COD_CTA_EMP  			VARCHAR2(20) 	DEFAULT ''
);

alter table cont_k210
       add constraint pk_cont_k210 primary key (COD_CONTROLADORA,EXERCICIO,COD_CTA,COD_EMP,COD_CTA_EMP);
       
alter table cont_k210 
      add constraint fk_cont_k210_k200 FOREIGN KEY(COD_CONTROLADORA,EXERCICIO,COD_CTA) REFERENCES cont_k200 (COD_CONTROLADORA,EXERCICIO,COD_CTA);

comment on table cont_k210              		is 'MAPEAMENTO PARA PLANOS DE CONTAS DAS EMPRESAS CONSOLIDADAS ';
comment on column cont_k210.COD_CONTROLADORA 	is 'código da Empresa Controladora';
comment on column cont_k210.EXERCICIO 			is 'Exercicio a que se refere as informações da empresa no período';
comment on column cont_k210.COD_CTA 			is 'Código da Conta';
comment on column cont_k210.COD_EMP     		is 'Código de identificação da empresa participante';
comment on column cont_k210.COD_CTA_EMP 		is 'Código da conta da empresa participante';
