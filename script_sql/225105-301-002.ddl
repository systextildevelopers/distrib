CREATE TABLE  ESTQ_800 
   (	SEQ_ID_O NUMBER(15,0), 
	TIPO_LEITURA NUMBER(2,0), 
	DEP_S NUMBER(3,0) NOT NULL ENABLE, 
	DEP_E NUMBER(3,0) NOT NULL ENABLE, 
	TRANS_S NUMBER(3,0) NOT NULL ENABLE, 
	TRANS_E NUMBER(3,0) NOT NULL ENABLE, 
	DATA_MOV DATE, 
	NIVEL_O VARCHAR2(2), 
	GRUPO_O VARCHAR2(5), 
	SUBGRUPO_O VARCHAR2(3), 
	ITEM_O VARCHAR2(6), 
	QTDE_O NUMBER(9,0), 
	QTDE_T_D NUMBER(9,0), 
	PERIODO NUMBER(4,0), 
	ORDEM NUMBER(9,0), 
	PACOTE NUMBER(5,0), 
	SEQ NUMBER(4,0), 
	ESTAGIO NUMBER(2,0), 
	 CONSTRAINT ESTQ_800_PK PRIMARY KEY (SEQ_ID_O)
  USING INDEX  ENABLE
   );
