DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'data_entr_venda';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'data_entr_venda',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'fatura_comercial';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'fatura_comercial',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'bloqueio_rolo';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'bloqueio_rolo',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'criterio_pedido';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'criterio_pedido',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'data_base_fatur';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'data_base_fatur',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cli_ped_cgc_cli9';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cli_ped_cgc_cli9',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cli_ped_cgc_cli4';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cli_ped_cgc_cli4',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cli_ped_cgc_cli2';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cli_ped_cgc_cli2',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cod_forma_pagto';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cod_forma_pagto',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'tipo_comissao';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'tipo_comissao',             0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'perc_comis_venda';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'perc_comis_venda',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cod_ped_cliente';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cod_ped_cliente',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'classificacao_pedido';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'classificacao_pedido',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cond_pgto_venda';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cond_pgto_venda',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'encargos';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'encargos',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'data_prev_receb';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'data_prev_receb',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'colecao_tabela';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'colecao_tabela',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'mes_tabela';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'mes_tabela',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'sequencia_tabela';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'sequencia_tabela',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'desconto1';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'desconto1',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'desconto2';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'desconto2',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'desconto3';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'desconto3',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'tipo_prod_pedido';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'tipo_prod_pedido',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'desconto_item1';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'desconto_item1',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'desconto_item2';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'desconto_item2',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'desconto_item3';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'desconto_item3',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'seq_end_entrega';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'seq_end_entrega',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'seq_end_cobranca';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'seq_end_cobranca',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'tipo_frete';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'tipo_frete',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'trans_pv_forne9';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'trans_pv_forne9',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'trans_pv_forne4';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'trans_pv_forne4',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'trans_pv_forne2';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'trans_pv_forne2',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'natop_pv_nat_oper';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'natop_pv_nat_oper',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'natop_pv_est_oper';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'natop_pv_est_oper',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cod_banco';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cod_banco',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f220';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'usuario_cadastro';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'usuario_cadastro',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/
