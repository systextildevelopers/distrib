alter table mqop_030 add letra_agrup varchar2(5);

comment on column mqop_030.letra_agrup is 'Letra agrupadora da máquinas: uma letra/código para agrupar um conjunto de máquinas (utilizado inicialmente no covo_e310)';

exec inter_pr_recompile;
