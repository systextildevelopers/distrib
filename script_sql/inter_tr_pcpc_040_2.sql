
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_040_2" 
   before insert
       or delete
       or update of qtde_pecas_prog,
                    qtde_pecas_prod,
                    qtde_pecas_2a,
                    qtde_perdas,
                    qtde_conserto,
                    qtde_em_producao_pacote,
                    qtde_a_produzir_pacote
   on pcpc_040
   for each row
declare
   ws_usuario_rede                       varchar2(20);
   ws_maquina_rede                       varchar2(40);
   ws_aplicativo                         varchar2(20);
   ws_sid                                number(9);
   ws_empresa                            number(3);
   ws_usuario_systextil                  varchar2(250);
   ws_locale_usuario                     varchar2(5);

   v_executa_trigger                     number;
   v_tem_registro                        number;
   v_deposito_conserto                   number;

   v_estagio_corte                       basi_030.estagio_altera_programado%type;
   v_tem_reg_400                         number;
begin
   -- INICIO - L�gica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementa��o executada para limpeza da base de dados do cliente
   -- e replica��o dos dados para base hist�rico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :new.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1
      or :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :new.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :old.executa_trigger = 3
         then
            v_executa_trigger := 1;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;
   -- FINAL - L�gica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_tem_reg_400 := inter_fn_executou_recalculo();

   if  inserting
   and v_tem_reg_400 <> 0
   then
      if :new.qtde_em_producao_pacote is null
      or :new.qtde_em_producao_pacote  = 0
      then
         :new.qtde_em_producao_pacote := 0;
         :new.qtde_disponivel_baixa   := 0;

         if :new.estagio_anterior = 0
         then
            :new.qtde_em_producao_pacote := :new.qtde_pecas_prog;
            :new.qtde_disponivel_baixa   := :new.qtde_pecas_prog;
         end if;
      end if;

      :new.qtde_a_produzir_pacote  := :new.qtde_pecas_prog;
      :new.situacao_em_prod_a_prod := 1;
   end if;

   if  updating
   and v_tem_reg_400 <> 0
   then
      if :new.qtde_pecas_prog < :old.qtde_pecas_prog
      then
         :new.qtde_a_produzir_pacote := :new.qtde_a_produzir_pacote +
                                        (:new.qtde_pecas_prog - :old.qtde_pecas_prog);

         if :new.estagio_anterior = 0
         then
            :new.qtde_em_producao_pacote := :new.qtde_em_producao_pacote +
                                            (:new.qtde_pecas_prog - :old.qtde_pecas_prog);
            :new.qtde_disponivel_baixa  := :new.qtde_disponivel_baixa +
                                            (:new.qtde_pecas_prog - :old.qtde_pecas_prog);
         end if;
      end if;

      if :new.qtde_pecas_prog > :old.qtde_pecas_prog
      then
         if  :new.qtde_pecas_prod + :new.qtde_pecas_2a + :new.qtde_perdas + :new.qtde_conserto = 0
         and :old.qtde_a_produzir_pacote = :old.qtde_pecas_prog
         then
            :new.qtde_a_produzir_pacote := :new.qtde_pecas_prog;
         else
            :new.qtde_a_produzir_pacote := :new.qtde_a_produzir_pacote +
                                           (:new.qtde_pecas_prog - :old.qtde_pecas_prog);
         end if;

         begin
            select nvl(basi_030.estagio_altera_programado,0)
            into   v_estagio_corte
            from basi_030, pcpc_020
            where pcpc_020.ordem_producao = :new.ordem_producao
              and basi_030.nivel_estrutura = '1'
              and basi_030.referencia      = pcpc_020.referencia_peca;
         exception when OTHERS then
            v_estagio_corte := 0;
         end;

         if v_estagio_corte is null
         or v_estagio_corte = 0
         then
            begin
               select nvl(empr_001.estagio_corte,0)
               into   v_estagio_corte
               from empr_001;
            exception when OTHERS then
               v_estagio_corte := 0;
            end;
         end if;

         -- Como alterou o programado o primeiro est�gio vai ter que come�ar a produzir a nova qtde
         if  :new.estagio_anterior   = 0
         and :new.qtde_pecas_prod +
             :new.qtde_pecas_2a   +
             :new.qtde_perdas     +
             :new.qtde_conserto      <> 0
         or  (:new.estagio_anterior  = 0
         and   v_estagio_corte       = :new.codigo_estagio)
         then
            :new.qtde_em_producao_pacote := :new.qtde_em_producao_pacote +
                                            (:new.qtde_pecas_prog - :old.qtde_pecas_prog);
            :new.qtde_disponivel_baixa   := :new.qtde_disponivel_baixa +
                                            (:new.qtde_pecas_prog - :old.qtde_pecas_prog);
         end if;

         if  :new.estagio_anterior   = 0
         and (:new.qtde_pecas_prod +
              :new.qtde_pecas_2a   +
              :new.qtde_perdas     +
              :new.qtde_conserto)    = 0
         then
            :new.qtde_em_producao_pacote := :new.qtde_pecas_prog;
            :new.qtde_disponivel_baixa   := :new.qtde_pecas_prog;
         end if;
      end if;
   end if;

   if  updating
   and v_executa_trigger <> 3
   and v_executa_trigger <> 1
   and v_tem_reg_400     <> 0
   then
      :new.qtde_pecas_prod := :old.qtde_pecas_prod;
      :new.qtde_pecas_2a   := :old.qtde_pecas_2a;
      :new.qtde_conserto   := :old.qtde_conserto;
      :new.qtde_perdas     := :old.qtde_perdas;
   end if;

   if inserting
   or updating
   then
      :new.executa_trigger := 0;
   end if;

end inter_tr_pcpc_040_2;

-- ALTER TRIGGER "INTER_TR_PCPC_040_2" ENABLE
 

/

exec inter_pr_recompile;

