insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_f015', 'Concede desconto de FCI',0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_f015', ' ' ,1, 0, 'N', 'N', 'N', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pedi_f015', ' ' ,1, 0, 'N', 'N', 'N', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Otorga descuento FCI'
where hdoc_036.codigo_programa = 'pedi_f015'
  and hdoc_036.locale          = 'es_ES';
  
commit;
