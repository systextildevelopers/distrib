create or replace trigger INTER_TR_SUPR_520
   before insert or
          delete or
          update of nivel,            grupo,         subgrupo,
                    item,             situacao,      cod_transacao,
                    qtde_requisitada, qtde_entregue, deposito,
                    nome_programa, deposito_entrega
   on supr_520
   for each row
declare
   v_codigo_empresa            supr_510.codigo_empresa%type;
   v_tipo_produto             basi_015.tipo_produto%type;
   v_entrada_saida            estq_005.entrada_saida%type;
   v_atualiza_estoque         estq_005.atualiza_estoque%type;
   v_data_atual               date     := trunc(sysdate,'DD');
   v_qtde_entregue            number;
   v_entrada_saida_orig       estq_005.entrada_saida%type;
   v_transac_entrada          estq_005.transac_entrada%type;
   v_deposito_estagio         mqop_005.codigo_deposito%type;
   v_destino_req_almoxarifado fatu_502.destino_req_almoxarifado%type;
   v_nr_documento             estq_300.numero_documento%type;
   v_preco_medio              number;
   v_estoque_atu              number;
   v_perm_alter_qtde_req_mat  number;
   v_situacao_novo_item       number;
   v_op                       number;

   v_executa_trigger          number;
   v_nome_programa            supr_520.nome_programa%type;

   v_usuario_rede             varchar2(20);
   v_maquina_rede             varchar2(40);
   v_aplicativo               varchar2(20);
   tmpDep                     number;
   v_sid                      number(9);
   v_empresa                  number(3);
   v_usuario_systextil        varchar2(20);
   v_locale_usuario           varchar2(5);

   v_executa_transferencia    number(1);
   v_transacao_transf_almox   number(3);
   v_deposito_transf_almox    number(3);

   max_sequencia              number;
   qtde_req                   number;
   param_lote                 number;
   v_ordemProd           number;
   v_codEstagio         number;
   v_inserePorAlmoxarife     number;
   v_rpt_pette number(1);

   procedure transfere_produto(num_requisicao NUMBER,
      nivel VARCHAR2, grupo VARCHAR2, subgrupo VARCHAR2,
      item VARCHAR2, deposito NUMBER, qtde_new NUMBER, qtde_old NUMBER)
   is begin
      begin
         select basi_010.preco_medio
         into   v_preco_medio
         from basi_010
         where basi_010.nivel_estrutura  = :new.nivel
           and basi_010.grupo_estrutura  = :new.grupo
           and basi_010.subgru_estrutura = :new.subgrupo
           and basi_010.item_estrutura   = :new.item;
      exception
      when others then
         v_preco_medio := 0;
      end;

      v_qtde_entregue := qtde_new - nvl(qtde_old,0);
      inter_pr_transf_almox(nivel,
                            grupo,
                            subgrupo,
                            item,
                            deposito,
                            1,
                            num_requisicao,
                            v_preco_medio,
                            v_qtde_entregue,
              '');
   end;

   procedure novo_item(p_num_requisicao number,
     p_nivel varchar2, p_grupo varchar2, p_subgrupo varchar2, p_item varchar2,
     p_numero_os varchar2, p_obs_requerente varchar2, p_qtde_requisitada number,
     p_qtde_entregue number, p_almoxarife varchar2, p_obs_almoxarife varchar2,
     p_usuario_canc varchar2, p_data_canc date, p_cod_transacao number,
     p_qtde_entregue_aux number, p_qtde_nao_emp number, p_requis_compra number, p_deposito number,
     p_data_requis date, p_ccusto_destino number, p_narrativa_prod varchar2, p_solicitacao number,
     p_emp_solic number, p_usuario_cardex varchar2, p_nome_programa varchar2, p_req_almox_origem number,
     p_seq_almox_origem number, p_projeto number, p_subprojeto number, p_servico number, p_ordem_producao number,
     p_cod_estagio number, p_area_producao number, p_lote_estoque number, p_deposito_entrega number,
     p_hora_requis date, p_qtde_emp_faccao_nfs number, p_flag_faccionista number,
     p_serv_solicitacao number, p_numero_ordem number, p_seq_ordem_serv number, p_flag_separacao number,
     p_controla_log varchar2)
   as pragma autonomous_transaction;
   begin
      begin
         select max(supr_520.sequencia)
         into max_sequencia
         from supr_520
         where supr_520.num_requisicao = p_num_requisicao;
      exception
      when others then
        max_sequencia := 0;
      end;

      begin
         select estq_040.qtde_estoque_atu
         into v_estoque_atu
         from estq_040
         where estq_040.cditem_nivel99 = p_nivel
           and estq_040.cditem_grupo = p_grupo
           and estq_040.cditem_subgrupo = p_subgrupo
           and estq_040.cditem_item = p_item
           and estq_040.deposito = p_deposito;
      exception
      when others then
         v_estoque_atu := 0;
      end;

      v_situacao_novo_item := 1;
      if v_estoque_atu < qtde_req
      then
         v_situacao_novo_item := 2;
      end if;

      max_sequencia := max_sequencia + 1;
      qtde_req := p_qtde_requisitada - p_qtde_entregue;

      begin
         insert into supr_520
           (
            num_requisicao, sequencia, nivel, grupo, subgrupo, item,
            situacao, numero_os, obs_requerente, qtde_requisitada, qtde_entregue,
            data_entregue, usuario_receptor, almoxarife, obs_almoxarife,
            usuario_canc, data_canc, cod_transacao, marca_atendimento,
            qtde_entregue_aux, qtde_nao_emp, requis_compra, deposito,
            data_requis, ccusto_destino, narrativa_prod, solicitacao,
            emp_solic, usuario_cardex, nome_programa, req_almox_origem,
            seq_almox_origem, projeto, subprojeto, servico, ordem_producao,
            cod_estagio, area_producao, lote_estoque, deposito_entrega,
            executa_trigger, hora_requis, qtde_emp_faccao_nfs, flag_faccionista,
            serv_solicitacao, numero_ordem, seq_ordem_serv, flag_separacao,
            controla_log, sit_transf
           )
         values
           (
            p_num_requisicao, max_sequencia, p_nivel, p_grupo, p_subgrupo, p_item,
            v_situacao_novo_item, p_numero_os, p_obs_requerente, qtde_req, 0,
            null, '', p_almoxarife, p_obs_almoxarife,
            p_usuario_canc, p_data_canc, p_cod_transacao, 0,
            p_qtde_entregue_aux, p_qtde_nao_emp, p_requis_compra, p_deposito,
            p_data_requis, p_ccusto_destino, p_narrativa_prod, p_solicitacao,
            p_emp_solic, p_usuario_cardex, p_nome_programa, p_req_almox_origem,
            p_seq_almox_origem, p_projeto, p_subprojeto, p_servico, p_ordem_producao,
            p_cod_estagio, p_area_producao, p_lote_estoque, p_deposito_entrega,
            0, p_hora_requis, p_qtde_emp_faccao_nfs, 1,
            p_serv_solicitacao, p_numero_ordem, p_seq_ordem_serv, p_flag_separacao,
            p_controla_log, 0
           );
      exception
      when others then
         raise_application_error(-20000, sqlerrm);
      end;

      commit;
   end;
begin
   -- INICIO - L�gica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementa��o executada para limpeza da base de dados do cliente
   -- e replica��o dos dados para base hist�rico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   -- Dados do usu�rio logado
   inter_pr_dados_usuario (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                           v_usuario_systextil,   v_empresa,        v_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(v_sid);
--   begin
--      select hdoc_090.programa
--      into v_nome_programa
--      from hdoc_090
--      where hdoc_090.sid = v_sid
--        and hdoc_090.instancia = userenv('INSTANCE')
--        and rownum       = 1
--        and hdoc_090.programa not like '%menu%'
--        and hdoc_090.programa not like '%!_m%'escape'!';
--      exception
--      when no_data_found then
--         v_nome_programa := 'SQL';
--   end;

   BEGIN
      select fatu_504.transacao_transf_almox, fatu_504.deposito_transf_almox
      into v_transacao_transf_almox, v_deposito_transf_almox
      from fatu_504
      where fatu_504.codigo_empresa = v_empresa;
   EXCEPTION
   WHEN OTHERS THEN
      v_transacao_transf_almox := 0;
      v_deposito_transf_almox := 0;
   END;

   v_executa_transferencia := 0;

   if v_transacao_transf_almox <> 0 or v_deposito_transf_almox <> 0
   then
      v_executa_transferencia := 1;
   end if;
   --raise_application_error(-20000,to_char(:new.deposito));

   if v_executa_trigger = 0
   then
      if inserting and v_nome_programa <> 'inte_p875'
      then
         begin
            select 1
            into tmpDep
            from basi_205
            where basi_205.codigo_deposito = :new.deposito_entrega
              and basi_205.tipo_volume = 0;
            exception
            when no_data_found then
               raise_application_error(-20000,'ATEN��O! Somente � permitido informar dep�sito de entrega sem volume. Deposito: ' || :new.deposito_entrega);
         end;

         -- nao permite que se insira uma requisicao com a quantidade entregue j� informada
         if :new.qtde_entregue <> 0.00
         then
            raise_application_error(-20000,'N�o se pode incluir um item de requisi��o com quantidade entrega j� informada.');
         end if;

         -- nao permite que se insira uma requisicao com situacao cancelada
         if :new.situacao = 7
         then
            raise_application_error(-20000,'N�o se pode incluir um item com situa��o de cancelada');
         end if;

         -- nao permite que se insira uma requisicao com situacao de entregue total ou parcial
         if :new.situacao in (5,6)
         then
            raise_application_error(-20000,'N�o se pode incluir um item com situa��o de atendida.');
         end if;

         -- se for informada o codigo da transacao
         if :new.cod_transacao > 0
         then

            -- le a transacao informada
            begin
               select entrada_saida,   atualiza_estoque
               into   v_entrada_saida, v_atualiza_estoque
               from estq_005
               where codigo_transacao = :new.cod_transacao;
               exception
                  when no_data_found then
                     raise_application_error(-20000,'Transa��o n�o cadastrada');
            end;

            v_entrada_saida_orig := v_entrada_saida;

            -- se a transacao nao atualizar o estoque, o deposito nao deve ser informado
            if  v_atualiza_estoque <> 1 and :new.deposito > 0 -- transacao nao atualiza estoque
            and :new.flag_faccionista <> 1                    -- Quando � uma divisao de faccionista deixa informar:
                                                              -- transacao NAO ATU ESTQ + DEPOSITO > 0
            then
               raise_application_error(-20000,'N�o se deve informar o dep�sito se a Transa��o n�o atualizar o estoque.');
            end if;

            -- se a transacao for de transferencia, nao deixa passar, pois nao se pode
            -- fazer uma transferencia usando a requisicao de almoxarifado
            if v_entrada_saida not in ('S','E','T')
            then
               raise_application_error(-20000,'Transa��o deve ser de entrada ou sa�da.');
            end if;

            -- se for informado um deposito
            if  :new.deposito > 0
            then
               -- le o deposito informado, buscando a empresa a que pertence
               begin
                  select local_deposito into v_codigo_empresa  from basi_205
                  where codigo_deposito = :new.deposito;
                  exception
                      when no_data_found then
                           raise_application_error(-20000,'Dep�sito n�o cadastrado');
               end;

               -- procura se o produto � de estoque ou de compra direta
               begin
                  select tipo_produto into v_tipo_produto
                  from basi_015
                  where codigo_empresa   = v_codigo_empresa
                  and   nivel_estrutura  = :new.nivel
                  and   grupo_estrutura  = :new.grupo
                  and   subgru_estrutura = :new.subgrupo
                  and   item_estrutura   = :new.item
                  and   codigo_deposito  = :new.deposito;

                  exception when others
                  then

                     begin
                        select tipo_produto into v_tipo_produto
                        from basi_015
                        where codigo_empresa   = v_codigo_empresa
                        and   nivel_estrutura  = :new.nivel
                        and   grupo_estrutura  = :new.grupo
                        and   subgru_estrutura = :new.subgrupo
                        and   item_estrutura   = :new.item
                        and   codigo_deposito  = 0;

                        exception
                           when others then
                              v_tipo_produto := 2;   -- compra direta
                     end;
               end;

               -- se o produto for de estoque, a transacao deve atualizar o estoque
               if  v_tipo_produto = 1 and v_atualiza_estoque <> 1
               and :new.flag_faccionista <> 1                    -- Quando � uma divisao de faccionista deixa informar:
                                                                 -- transacao NAO ATU ESTQ + PRODUTO DE ESTOQUE
               then
                  raise_application_error(-20000, 'Transacao deve atualizar o estoque quando o produto for de estoque.');
               end if;           -- if :new.deposito > 0 and :new.nivel_estrutura <> '0'

            end if;
         end if;              -- if :new.situacao not in (6,7)
      end if;                 -- if inserting


      if inserting and v_nome_programa = 'inte_p875'
      then
         begin
            select basi_010.preco_medio
            into   v_preco_medio
            from basi_010
            where basi_010.nivel_estrutura  = :new.nivel
              and basi_010.grupo_estrutura  = :new.grupo
              and basi_010.subgru_estrutura = :new.subgrupo
              and basi_010.item_estrutura   = :new.item;
         exception
               when others
                 then v_preco_medio := 0;
         end;

         -- le o deposito informado e busca a empresa a que pertence
         begin
            select local_deposito into v_codigo_empresa  from basi_205
            where codigo_deposito = :new.deposito;
         exception
            when others then
               raise_application_error(-20000,'Dep�sito n�o cadastrado');
         end;
         -- procura se o produto � de estoque ou de compra direta
         begin
            select tipo_produto into v_tipo_produto
            from basi_015
            where codigo_empresa     = v_codigo_empresa
              and   nivel_estrutura  = :new.nivel
              and   grupo_estrutura  = :new.grupo
              and   subgru_estrutura = :new.subgrupo
              and   item_estrutura   = :new.item
              and   codigo_deposito  = :new.deposito;
         exception when others
         then
            begin
               select tipo_produto into v_tipo_produto
               from basi_015
               where codigo_empresa   = v_codigo_empresa
                 and nivel_estrutura  = :new.nivel
                 and grupo_estrutura  = :new.grupo
                 and subgru_estrutura = :new.subgrupo
                 and item_estrutura   = :new.item
                 and codigo_deposito  = 0;
            exception when others
            then
               v_tipo_produto := 2;   -- compra direta
            end;
         end;

         -- se o produto for de estoque, obriga que seja informada a transacao
         if :new.cod_transacao = 0
         then
            raise_application_error(-20000,'� obrigat�rio informar uma transa��o v�lida para produtos de estoque.');
         end if;
         -- le a transacao para verificar se ela � de entrada ou saida e se atualiza o estoque
         begin
            select entrada_saida,   atualiza_estoque,   estq_005.transac_entrada
            into   v_entrada_saida, v_atualiza_estoque, v_transac_entrada
            from estq_005
            where codigo_transacao = :new.cod_transacao;

         exception
            when no_data_found then
              raise_application_error(-20000,'Transa��o n�o cadastrada');
         end;

         v_entrada_saida_orig := v_entrada_saida;

         -- se a transacao for de transferencia, nao deixa passar, pois nao se pode
         -- fazer uma transferencia usando a requisicao de almoxarifado
         if v_entrada_saida not in ('S','E','T')
         then
            raise_application_error(-20000,'Transa��o deve ser de entrada ou sa�da.');
         end if;

         -- exige que a transacao atualize o estoque
         if v_atualiza_estoque <> 1  -- transacao nao atualiza estoque
         then
            raise_application_error(-20000,'Transa��o n�o atualiza estoque.');
         end if;

         -- a quantidade atendida sera a diferenca do que ja tinha sido entregue (old) com
         -- a qntidade que se esta alterando o registro (new)
         v_qtde_entregue := :new.qtde_entregue - nvl(:old.qtde_entregue,0);

         if v_entrada_saida_orig = 'T'
         then
            v_entrada_saida := 'S';
         end if;

         v_nr_documento := :new.num_requisicao;

         if :new.ordem_producao <> 0 and :new.area_producao = 4
         then
           v_nr_documento := :new.ordem_producao;
         end if;

         if :new.solicitacao > 0
         then
            v_nr_documento := :new.solicitacao;
         end if;


          if v_inserePorAlmoxarife = 1 then
              inter_pr_estq_300_almoxa(:new.deposito,
                                     :new.nivel,
                                     :new.grupo,
                                     :new.subgrupo,
                                     :new.item,
                                     v_data_atual,
                                     :new.lote_estoque,
                                     v_nr_documento,
                                     ' ',
                                     0,
                                     0,
                                     0,
                                     0,
                                     :new.cod_transacao,
                                     v_entrada_saida,
                                     :new.ccusto_destino,
                                     v_qtde_entregue,
                                     v_preco_medio,
                                     v_preco_medio,
                                     :new.usuario_cardex,
                                     'SUPR_520',
                                     v_nome_programa,
                                     0.00,
                                     v_ordemProd,
                                     v_codEstagio);
          else
             inter_pr_insere_estq_300(:new.deposito,
                                   :new.nivel,
                                   :new.grupo,
                                   :new.subgrupo,
                                   :new.item,
                                   v_data_atual,
                                   :new.lote_estoque,
                                   v_nr_documento,
                                   ' ',
                                     0,
                                     0,
                                     0,
                                     0,
                                   :new.cod_transacao,
                                   v_entrada_saida,
                                   :new.ccusto_destino,
                                   v_qtde_entregue,
                                   v_preco_medio,
                                   v_preco_medio,
                                   :new.usuario_cardex,
                                   'SUPR_520',
                                   v_nome_programa,
                                   0.00);
           end if;

      end if; /*Fim If inserting and v_programa = inte_p875*/

      if updating
      then
         -- N�o permite alterar o produto
         if :old.nivel    <> :new.nivel
         or :old.grupo    <> :new.grupo
         or :old.subgrupo <> :new.subgrupo
         or :old.item     <> :new.item
         then
            raise_application_error(-20000, 'N�o se pode alterar o produto de uma requisi��o.');
         end if;

         begin
            select 1
            into tmpDep
            from basi_205
            where basi_205.codigo_deposito = :new.deposito_entrega
              and basi_205.tipo_volume = 0;
            exception
              when no_data_found then
               raise_application_error(-20000,'ATEN��O! Somente � permitido informar dep�sito de entrega sem volume.');
         end;

		 v_rpt_pette := 0;
		 begin
			 select count(*)
			 into v_rpt_pette
			 from fatu_500
			 where fatu_500.rpt_ordem_benef = 'pcpb_opb002';
		 end;

         -- N�o permite alterar a quantidade requisitada
         if :old.qtde_requisitada <> :new.qtde_requisitada and v_nome_programa <> 'inte_p875' and v_rpt_pette = 0
         then
            raise_application_error(-20000, 'N�o se pode alterar a quantidade requisitada.');
         end if;

         /*67222/001 - Somente valida quantidade entregue quando processo n�o for de separa��o de aviamentos*/
         if :old.flag_separacao = 0
         then
            -- N�o permite diminuir a quantidade entregue
            if :old.qtde_entregue > :new.qtde_entregue  -- qtde_entregue anterior esta maior e a atual
            then                                        -- quer dizer que a quantidade entregue foi subtraida
               raise_application_error(-20000, 'N�o se pode diminuir a quantidade entregue.');
            end if;
         end if;

         -- Nao permite descancelar uma requisicao
         if :old.situacao = 7 and :new.situacao <> 7
         then
            raise_application_error(-20000, 'N�o se pode descancelar uma requisi��o.');
         end if;

         if :old.situacao = 6 and :new.situacao = 7
         then
            raise_application_error(-20000, 'N�o se pode cancelar uma requisi��o atendida.');
         end if;

         -- altera a situacao da requisicao.
         -- se a quantidade entregue for menor que a requisitada, � uma entrega parcial
         -- senao e' uma entrega total
         if :new.situacao <> 6 and :new.situacao <> 7 and :new.qtde_entregue > 0 and v_nome_programa <> 'inte_p875'
         then
            if :new.flag_faccionista = 1 and  v_executa_transferencia = 1
            then
               :new.sit_transf := 1;
               transfere_produto(:new.num_requisicao,
                                 :new.nivel, :new.grupo , :new.subgrupo,
                                 :new.item , :new.deposito, :new.qtde_entregue, :old.qtde_entregue);

            end if;

            if :new.qtde_entregue < :new.qtde_requisitada
            then
               if :new.flag_faccionista = 0 or v_executa_transferencia = 0
               then
                  :new.situacao := 5;     -- entrega parcial
               else
                  :new.situacao := 6;     -- entrega total
                  :new.sit_transf := 1;
                  novo_item(:new.num_requisicao, :new.nivel, :new.grupo, :new.subgrupo, :new.item,
                            :new.numero_os, :new.obs_requerente, :new.qtde_requisitada, :new.qtde_entregue,
                             :new.almoxarife, :new.obs_almoxarife,
                             :new.usuario_canc, :new.data_canc, :new.cod_transacao,
                             :new.qtde_entregue_aux, :new.qtde_nao_emp, :new.requis_compra, :new.deposito,
                             :new.data_requis, :new.ccusto_destino, :new.narrativa_prod, :new.solicitacao,
                             :new.emp_solic, :new.usuario_cardex, v_nome_programa, :new.req_almox_origem,
                             :new.seq_almox_origem, :new.projeto, :new.subprojeto, :new.servico, :new.ordem_producao,
                             :new.cod_estagio, :new.area_producao, :new.lote_estoque, :new.deposito_entrega,
                             :new.hora_requis, :new.qtde_emp_faccao_nfs, :new.flag_faccionista,
                             :new.serv_solicitacao, :new.numero_ordem, :new.seq_ordem_serv, :new.flag_separacao,
                             :new.controla_log);
               end if;
            else
               :new.situacao := 6;     -- entrega total
            end if;
         end if;

         -- se o produto da requisicao for valido e se nao estiver sendo cancelada
         -- e houver sido informado um deposito, processo entra no processamento
         if  :new.nivel <> '0' and :new.situacao <> 7 and :new.deposito > 0
         then

            begin
               select basi_010.preco_medio
               into   v_preco_medio
               from basi_010
               where basi_010.nivel_estrutura  = :new.nivel
                 and basi_010.grupo_estrutura  = :new.grupo
                 and basi_010.subgru_estrutura = :new.subgrupo
                 and basi_010.item_estrutura   = :new.item;
               exception
                  when others
                  then v_preco_medio := 0;
            end;
            -- se a quantidade estiver sendo altera � porque esta sendo processado uma
            -- entrega ou uma devolucao
            if  nvl(:old.qtde_entregue,0) <> :new.qtde_entregue
            and :new.flag_faccionista <> 1                   -- Quando � uma divisao de faccionista n�o atualiza estoque
            and :new.sit_transf = 0
            then

              -- le o deposito informado e busca a empresa a que pertence
               begin
                  select local_deposito into v_codigo_empresa  from basi_205
                  where codigo_deposito = :new.deposito;

                  exception
                     when others then
                        raise_application_error(-20000,'Dep�sito n�o cadastrado');
               end;

               -- procura se o produto � de estoque ou de compra direta
               begin
                  select tipo_produto into v_tipo_produto
                  from basi_015
                  where codigo_empresa   = v_codigo_empresa
                  and   nivel_estrutura  = :new.nivel
                  and   grupo_estrutura  = :new.grupo
                  and   subgru_estrutura = :new.subgrupo
                  and   item_estrutura   = :new.item
                  and   codigo_deposito  = :new.deposito;

                  exception when others
                  then
                     begin
                        select tipo_produto into v_tipo_produto
                        from basi_015
                        where codigo_empresa   = v_codigo_empresa
                        and   nivel_estrutura  = :new.nivel
                        and   grupo_estrutura  = :new.grupo
                        and   subgru_estrutura = :new.subgrupo
                        and   item_estrutura   = :new.item
                        and   codigo_deposito  = 0;

                        exception when others
                        then
                           v_tipo_produto := 2;   -- compra direta
                     end;
               end;
               

                -- se o produto for de estoque, obriga que seja informada a transacao
                if :new.cod_transacao = 0
                then
                   raise_application_error(-20000,'� obrigat�rio informar uma transa��o v�lida para produtos de estoque.');
                end if;

                -- le a transacao para verificar se ela � de entrada ou saida e se atualiza o estoque
                begin
                   select entrada_saida,   atualiza_estoque,   estq_005.transac_entrada
                   into   v_entrada_saida, v_atualiza_estoque, v_transac_entrada
                   from estq_005
                   where codigo_transacao = :new.cod_transacao;

                   exception
                      when no_data_found then
                         raise_application_error(-20000,'Transa��o n�o cadastrada');
                end;

                v_entrada_saida_orig := v_entrada_saida;

                -- se a transacao for de transferencia, nao deixa passar, pois nao se pode
                -- fazer uma transferencia usando a requisicao de almoxarifado
                if v_entrada_saida not in ('S','E','T')
                then
                  raise_application_error(-20000,'Transa��o deve ser de entrada ou sa�da.');
                end if;

                -- exige que a transacao atualize o estoque
                if v_atualiza_estoque <> 1  -- transacao nao atualiza estoque
                then
                   raise_application_error(-20000,'Transa��o n�o atualiza estoque.');
                end if;

                -- a quantidade atendida sera a diferenca do que ja tinha sido entregue (old) com
                -- a qntidade que se esta alterando o registro (new)
                v_qtde_entregue := :new.qtde_entregue - nvl(:old.qtde_entregue,0);

                if v_entrada_saida_orig = 'T'
                then
                   v_entrada_saida := 'S';
                end if;

                v_op := 0;

                v_ordemProd := 0;
                v_codEstagio := 0;

                if  v_entrada_saida = 'S' 
                and v_entrada_saida_orig != 'T'
                and v_nome_programa = 'supr_f542' then
                      v_ordemProd := :new.ordem_producao;
                      v_codEstagio := :new.cod_estagio;
                      v_inserePorAlmoxarife := 1;
                else
                      v_inserePorAlmoxarife := 0;
                end if;


                if :new.ordem_producao <> 0 and :new.area_producao = 4
                then
                   v_nr_documento := :new.ordem_producao;
                   v_op           := :new.ordem_producao;
                else
                   v_nr_documento := :new.num_requisicao;
                   v_op           := 0;
                end if;

                begin
                   select empr_008.val_int
                   into param_lote
                   from empr_008
                   where empr_008.codigo_empresa = v_empresa
                   and   empr_008.param          = 'compras.lote_entrada';
                exception
                when others then
                   param_lote := :new.lote_estoque;
                end;

                if param_lote = 1
                then param_lote := :new.lote_estoque;
                else param_lote := 0;
                end if;

                if :new.solicitacao > 0
                then
                   v_nr_documento := :new.solicitacao;
                end if;

                /*66925/001 - Somente gera movimenta��o se a requisi��o n�o foi gerada pela separa��o de aviamentos.*/
                if :old.flag_separacao = 0
                then
                    if v_inserePorAlmoxarife = 1 then
                        inter_pr_estq_300_almoxa(:new.deposito,
                                               :new.nivel,
                                               :new.grupo,
                                               :new.subgrupo,
                                               :new.item,
                                               v_data_atual,
                                               :new.lote_estoque,
                                               v_nr_documento,
                                               ' ',
                                               0,
                                               0,
                                               0,
                                               0,
                                               :new.cod_transacao,
                                               v_entrada_saida,
                                               :new.ccusto_destino,
                                               v_qtde_entregue,
                                               v_preco_medio,
                                               v_preco_medio,
                                               :new.usuario_cardex,
                                               'SUPR_520',
                                               v_nome_programa,
                                               0.00,
                                               v_ordemProd,
                                               v_codEstagio);
                    else
                        -- gera o movimento de estoque
                        inter_pr_insere_estq_300_recur(:new.deposito,
                                                  :new.nivel,
                                                  :new.grupo,
                                                  :new.subgrupo,
                                                  :new.item,
                                                  v_data_atual,
                                                  :new.lote_estoque,
                                                  v_nr_documento,
                                                  ' ',
                                                  0,
                                                  0,
                                                  0,
                                                  0,
                                                  :new.cod_transacao,
                                                  v_entrada_saida,
                                                  :new.ccusto_destino,
                                                  v_qtde_entregue,
                                                  v_preco_medio,
                                                  v_preco_medio,
                                                  :new.usuario_cardex,
                                                  'SUPR_520',
                                                  v_nome_programa,
                                                  0.00,
                                                  v_op,
                                                  0,
                                                  0,
                                                  :new.area_producao,
                                                  :new.cod_estagio);
                    end if;
                end if;

                if v_entrada_saida_orig = 'T'
                then
                   v_entrada_saida := 'E';

                   begin


                      select fatu_502.destino_req_almoxarifado
                       into   v_destino_req_almoxarifado
                       from fatu_502
                      where fatu_502.codigo_empresa = v_codigo_empresa;
                   exception
                    when no_data_found then
                         raise_application_error(-20000,'Parametro destino requisicao almoxarifado n�o cadastrado');
                   end;

                   if v_destino_req_almoxarifado = 0 or (v_destino_req_almoxarifado = 2 and :new.ordem_producao > 0)
                   then
                      begin
                         select mqop_008.codigo_deposito
                         into   v_deposito_estagio
                         from mqop_008
                         where mqop_008.codigo_estagio = :new.cod_estagio
                           and mqop_008.codigo_empresa = v_empresa;
                      exception when no_data_found then
                         v_deposito_estagio := 0;
                      end;

                      if v_deposito_estagio = 0
                      then
                         begin
                            select mqop_005.codigo_deposito
                            into   v_deposito_estagio
                            from mqop_005
                            where mqop_005.codigo_estagio = :new.cod_estagio;
                         exception when no_data_found then
                            raise_application_error(-20000,'Est�gio n�o cadastrado');
                         end;
                      end if;
                   end if;

                   if v_destino_req_almoxarifado = 1 or (v_destino_req_almoxarifado = 2 and :new.ordem_producao = 0)
                   then
                      v_deposito_estagio := :new.deposito_entrega;
                   end if;

                   v_op := 0;

                   if :new.ordem_producao <> 0 and :new.area_producao = 4
                   then
                      v_nr_documento := :new.ordem_producao;
                      v_op           := :new.ordem_producao;
                   else
                      v_nr_documento := :new.num_requisicao;
                      v_op           := 0;
                   end if;



                   if :new.solicitacao > 0
                   then
                      v_nr_documento := :new.solicitacao;
                   end if;

                   /*66925/001 - Somente gera movimenta��o se a requisi��o n�o foi gerada pela separa��o de aviamentos.*/
                   if :old.flag_separacao = 0
                   then

                     -- gera o movimento de estoque
                     if v_inserePorAlmoxarife = 1 then
                        inter_pr_estq_300_almoxa(:new.deposito,
                                               :new.nivel,
                                               :new.grupo,
                                               :new.subgrupo,
                                               :new.item,
                                               v_data_atual,
                                               :new.lote_estoque,
                                               v_nr_documento,
                                               ' ',
                                               0,
                                               0,
                                               0,
                                               0,
                                               :new.cod_transacao,
                                               v_entrada_saida,
                                               :new.ccusto_destino,
                                               v_qtde_entregue,
                                               v_preco_medio,
                                               v_preco_medio,
                                               :new.usuario_cardex,
                                               'SUPR_520',
                                               v_nome_programa,
                                               0.00,
                                               v_ordemProd,
                                               v_codEstagio);
                    else
                        inter_pr_insere_estq_300_recur(v_deposito_estagio,
                                              :new.nivel,
                                              :new.grupo,
                                              :new.subgrupo,
                                              :new.item,
                                              v_data_atual,
                                              param_lote,
                                              v_nr_documento,
                                              ' ',
                                              0,
                                              0,
                                              0,
                                              0,
                                              v_transac_entrada,
                                              v_entrada_saida,
                                              :new.ccusto_destino,
                                              v_qtde_entregue,
                                              v_preco_medio,
                                              v_preco_medio,
                                              :new.usuario_cardex,
                                              'SUPR_520',
                                              v_nome_programa,
                                                 0.00,
                                                 v_op,
                                                 0,
                                                 0,
                                                 :new.area_producao,
                                                 :new.cod_estagio);
                    end if;
                  end if;
               end if;
               if :new.situacao = 6
               then
                  :new.sit_transf := 1;
               end if;

            end if;           -- if v_tipo_produto = 1
         end if;              -- if :old.nivel <> '0'
      end if;                 -- if updating
   end if;
end inter_tr_supr_520;
/
