
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_075_1" 
   after insert or
         update of data_pagamento, data_credito
   on fatu_075
   for each row

declare
   v_data_ult_movim_pagto   fatu_070.data_ult_movim_pagto%type;
   v_data_ult_movim_credito fatu_070.data_ult_movim_credito%type;

   v_executa_trigger number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      begin
         select data_ult_movim_pagto,   data_ult_movim_credito
         into   v_data_ult_movim_pagto, v_data_ult_movim_credito
         from fatu_070
         where fatu_070.codigo_empresa   = :new.nr_titul_codempr
           and fatu_070.cli_dup_cgc_cli9 = :new.nr_titul_cli_dup_cgc_cli9
           and fatu_070.cli_dup_cgc_cli4 = :new.nr_titul_cli_dup_cgc_cli4
           and fatu_070.cli_dup_cgc_cli2 = :new.nr_titul_cli_dup_cgc_cli2
           and fatu_070.tipo_titulo      = :new.nr_titul_cod_tit
           and fatu_070.num_duplicata    = :new.nr_titul_num_dup
           and fatu_070.seq_duplicatas   = :new.nr_titul_seq_dup;
      end;

      if :new.data_pagamento > v_data_ult_movim_pagto
      then v_data_ult_movim_pagto := :new.data_pagamento;
      end if;

      if :new.data_credito > v_data_ult_movim_credito
      then v_data_ult_movim_credito := :new.data_credito;
      end if;

      begin
         update fatu_070
         set data_ult_movim_pagto   = v_data_ult_movim_pagto,
             data_ult_movim_credito = v_data_ult_movim_credito
         where fatu_070.codigo_empresa   = :new.nr_titul_codempr
           and fatu_070.cli_dup_cgc_cli9 = :new.nr_titul_cli_dup_cgc_cli9
           and fatu_070.cli_dup_cgc_cli4 = :new.nr_titul_cli_dup_cgc_cli4
           and fatu_070.cli_dup_cgc_cli2 = :new.nr_titul_cli_dup_cgc_cli2
           and fatu_070.tipo_titulo      = :new.nr_titul_cod_tit
           and fatu_070.num_duplicata    = :new.nr_titul_num_dup
           and fatu_070.seq_duplicatas   = :new.nr_titul_seq_dup;
      end;
   end if;
end inter_tr_fatu_075_1;

-- ALTER TRIGGER "INTER_TR_FATU_075_1" ENABLE
 

/

exec inter_pr_recompile;

