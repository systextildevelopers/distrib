
CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_100_PLAN" 
   before insert or
          delete or
          update of num_requisicao,seq_item_req, cod_cancelamento on supr_100
   for each row
declare
   v_pedido_venda       pcpc_020.pedido_venda%type;

   v_ordem_compra            supr_100.num_ped_compra%type;
   v_ordem_planej            tmrp_620.ordem_planejamento%type;
   v_ordem_pedido            supr_068.num_relacionamento%type;
   v_tipo_destino            supr_068.tipo_relacionamento%type;
   v_nivel_produto           supr_100.item_100_nivel99%type;
   v_grupo_produto           supr_100.item_100_grupo%type;
   v_subgrupo_produto        supr_100.item_100_subgrupo%type;
   v_item_produto            supr_100.item_100_item%type;
   v_qtde_reserva            tmrp_625.qtde_reserva_planejada%type;
   v_quantidade_update       pcpb_030.qtde_quilos_prog%type;
   v_saldo                   pcpb_030.qtde_quilos_prog%type;
   v_count_reg               number;
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

  if (updating and :old.cod_cancelamento = 0 and :old.cod_cancelamento <> :new.cod_cancelamento) or deleting
   then
      begin
         delete supr_071
         where supr_071.pedido_compra       = :old.num_ped_compra
           and supr_071.seq_item_pedido     = :old.seq_item_pedido
           and supr_071.tipo_relacionamento = 4;
      exception
      when OTHERS then
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'SUPR_071' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   begin
      insert into tmrp_615
        (nome_programa,
         nr_solicitacao,                           tipo_registro,
         cgc_cliente9)
      values
        ('trigger_planejamento',
         888,                                      888,
         ws_sid);
      exception when others
      then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   if (inserting or updating) and :new.num_requisicao > 0
   then
      v_nivel_produto    := :new.item_100_nivel99;
      v_grupo_produto    := :new.item_100_grupo;
      v_subgrupo_produto := :new.item_100_subgrupo;
      v_item_produto     := :new.item_100_item;
      v_ordem_compra     := :new.num_ped_compra;

      -- Se for uma atualizacao e o destino ja era para o corte,
      -- a quantidade a atualizar sera a diferenca entre o peso
      -- programado anteriormente e o novo.
      if updating and (:old.num_requisicao = :new.num_requisicao)
      then v_quantidade_update := :new.qtde_pedida_item - :old.qtde_pedida_item;
      else v_quantidade_update := :new.qtde_pedida_item;
      end if;

      if :new.seq_item_req > 0
      then
         begin
            select supr_068.tipo_relacionamento, supr_068.num_relacionamento
            into   v_tipo_destino,               v_ordem_pedido
            from supr_068
            where supr_068.num_requisicao = :new.num_requisicao
              and supr_068.seq_item_req   = :new.seq_item_req
          and rownum                  = 1;
            exception when others
            then v_tipo_destino := 0;
                 v_ordem_pedido := 0;
         end;
      end if;
   elsif deleting
   then
      v_nivel_produto     := :old.item_100_nivel99;
      v_grupo_produto     := :old.item_100_grupo;
      v_subgrupo_produto  := :old.item_100_subgrupo;
      v_item_produto      := :old.item_100_item;
      v_ordem_compra      := :old.num_ped_compra;

      v_quantidade_update := :old.qtde_pedida_item * (-1);

      if :old.num_requisicao > 0
      then
         begin
            select supr_068.tipo_relacionamento, supr_068.num_relacionamento
            into   v_tipo_destino,               v_ordem_pedido
            from supr_068
            where supr_068.num_requisicao = :old.num_requisicao
              and supr_068.seq_item_req   = :old.seq_item_req
          and rownum                  = 1;
            exception when others
            then v_tipo_destino  := 0;
         end;
      end if;
   end if;

   v_saldo := v_quantidade_update;

   if v_tipo_destino = 4 -- Ordem Planejamento
   then
      v_ordem_planej := v_ordem_pedido;

      if v_ordem_planej > 0
      then
         for reg_tmrp_625 in (select tmrp_625.nivel_produto_origem,    tmrp_625.grupo_produto_origem,
                                     tmrp_625.subgrupo_produto_origem, tmrp_625.item_produto_origem,
                                     tmrp_625.seq_produto_origem,      tmrp_625.seq_produto,
                                     tmrp_625.pedido_venda,            tmrp_625.pedido_reserva,
                                     tmrp_625.data_requerida,
                                     ceil((decode(sum(tmrp_625.qtde_reserva_planejada) - sum(tmrp_625.qtde_reserva_programada) - abs(sum(tmrp_625.qtde_reserva_planejada) - sum(tmrp_625.qtde_reserva_programada)),
                                            0,
                                            sum(tmrp_625.qtde_reserva_planejada),
                                            sum(tmrp_625.qtde_reserva_programada)) - sum(tmrp_625.qtde_areceber_programada)) * 1000) / 1000 qtde_necessaria
                              from tmrp_625
                              where tmrp_625.nivel_produto      = v_nivel_produto
                                and tmrp_625.grupo_produto      = v_grupo_produto
                                and tmrp_625.subgrupo_produto   = v_subgrupo_produto
                                and tmrp_625.item_produto       = v_item_produto
                                and tmrp_625.ordem_planejamento = v_ordem_planej
                              group by tmrp_625.nivel_produto_origem,    tmrp_625.grupo_produto_origem,
                                       tmrp_625.subgrupo_produto_origem, tmrp_625.item_produto_origem,
                                       tmrp_625.seq_produto_origem,      tmrp_625.seq_produto,
                                       tmrp_625.pedido_venda,            tmrp_625.pedido_reserva,
                                       tmrp_625.data_requerida
                              order by tmrp_625.data_requerida DESC)
         loop
            if v_saldo > reg_tmrp_625.qtde_necessaria
            then
               v_qtde_reserva := reg_tmrp_625.qtde_necessaria;
               v_saldo        := v_saldo - reg_tmrp_625.qtde_necessaria;
            else
               v_qtde_reserva := v_saldo;
               v_saldo        := 0;
            end if;

            begin
               select count(1)
               into   v_count_reg
               from tmrp_630
               where tmrp_630.ordem_planejamento   = v_ordem_planej
                 and tmrp_630.pedido_venda         = reg_tmrp_625.pedido_venda
                 and tmrp_630.ordem_prod_compra    = v_ordem_compra
                 and tmrp_630.area_producao        = 9
                 and tmrp_630.nivel_produto        = v_nivel_produto
                 and tmrp_630.grupo_produto        = v_grupo_produto
                 and tmrp_630.subgrupo_produto     = v_subgrupo_produto
                 and tmrp_630.item_produto         = v_item_produto
                 and tmrp_630.alternativa_produto  = 0;
               exception when others
               then v_count_reg := 0;
            end;

            if v_count_reg > 0
            then
               begin
                  update tmrp_630
                     set tmrp_630.programado_comprado = ceil((tmrp_630.programado_comprado + v_qtde_reserva) * 1000) / 1000
                  where tmrp_630.ordem_planejamento   = v_ordem_planej
                    and tmrp_630.pedido_venda         = reg_tmrp_625.pedido_venda
                    and tmrp_630.ordem_prod_compra    = v_ordem_compra
                    and tmrp_630.area_producao        = 9
                    and tmrp_630.nivel_produto        = v_nivel_produto
                    and tmrp_630.grupo_produto        = v_grupo_produto
                    and tmrp_630.subgrupo_produto     = v_subgrupo_produto
                    and tmrp_630.item_produto         = v_item_produto
                    and tmrp_630.alternativa_produto  = 0;
                  exception
                  when others
                     then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            else
               begin
                  insert into tmrp_630
                    (ordem_planejamento,  area_producao,             ordem_prod_compra,
                     nivel_produto,       grupo_produto,             subgrupo_produto,
                     item_produto,        pedido_venda,              programado_comprado,
                     alternativa_produto, pedido_reserva)
                  values
                    (v_ordem_planej,      9,                         v_ordem_compra,
                     v_nivel_produto,     v_grupo_produto,           v_subgrupo_produto,
                     v_item_produto,      reg_tmrp_625.pedido_venda, v_qtde_reserva,
                     0,                   reg_tmrp_625.pedido_reserva);
                  exception
                  when others
                     then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            end if;
         end loop;
      end if;
   elsif v_tipo_destino = 5 -- Pedido Venda
   then
      v_pedido_venda := v_ordem_pedido;

      if v_pedido_venda > 0
      then
         for reg_tmrp_625 in (select tmrp_625.nivel_produto_origem,    tmrp_625.grupo_produto_origem,
                                     tmrp_625.subgrupo_produto_origem, tmrp_625.item_produto_origem,
                                     tmrp_625.seq_produto_origem,      tmrp_625.seq_produto,
                                     tmrp_625.ordem_planejamento,      tmrp_625.data_requerida,
                                     ceil((decode(sum(tmrp_625.qtde_reserva_planejada) - sum(tmrp_625.qtde_reserva_programada) - abs(sum(tmrp_625.qtde_reserva_planejada) - sum(tmrp_625.qtde_reserva_programada)),
                                                  0,
                                                  sum(tmrp_625.qtde_reserva_planejada),
                                                  sum(tmrp_625.qtde_reserva_programada)) - sum(tmrp_625.qtde_areceber_programada)) * 1000) / 1000 qtde_necessaria
                              from tmrp_625
                              where tmrp_625.nivel_produto      = v_nivel_produto
                                and tmrp_625.grupo_produto      = v_grupo_produto
                                and tmrp_625.subgrupo_produto   = v_subgrupo_produto
                                and tmrp_625.item_produto       = v_item_produto
                                and tmrp_625.pedido_venda       = v_pedido_venda
                              group by tmrp_625.nivel_produto_origem,    tmrp_625.grupo_produto_origem,
                                       tmrp_625.subgrupo_produto_origem, tmrp_625.item_produto_origem,
                                       tmrp_625.seq_produto_origem,      tmrp_625.seq_produto,
                                       tmrp_625.ordem_planejamento,      tmrp_625.data_requerida
                              order by tmrp_625.data_requerida DESC)
         loop
            if v_saldo > reg_tmrp_625.qtde_necessaria
            then
               v_qtde_reserva := reg_tmrp_625.qtde_necessaria;
               v_saldo        := v_saldo - reg_tmrp_625.qtde_necessaria;
            else
               v_qtde_reserva := v_saldo;
               v_saldo        := 0;
            end if;

            begin
               select count(1)
               into   v_count_reg
               from tmrp_630
               where tmrp_630.ordem_planejamento   = reg_tmrp_625.ordem_planejamento
                 and tmrp_630.pedido_venda         = v_pedido_venda
                 and tmrp_630.ordem_prod_compra    = v_ordem_compra
                 and tmrp_630.area_producao        = 9
                 and tmrp_630.nivel_produto        = v_nivel_produto
                 and tmrp_630.grupo_produto        = v_grupo_produto
                 and tmrp_630.subgrupo_produto     = v_subgrupo_produto
                 and tmrp_630.item_produto         = v_item_produto
                 and tmrp_630.alternativa_produto  = 0;
               exception when others
               then v_count_reg := 0;
            end;

            if v_count_reg > 0
            then
               begin
                  update tmrp_630
                     set tmrp_630.programado_comprado = ceil((tmrp_630.programado_comprado + v_qtde_reserva) * 1000) / 1000
                  where tmrp_630.ordem_planejamento   = reg_tmrp_625.ordem_planejamento
                    and tmrp_630.pedido_venda         = v_pedido_venda
                    and tmrp_630.ordem_prod_compra    = v_ordem_compra
                    and tmrp_630.area_producao        = 9
                    and tmrp_630.nivel_produto        = v_nivel_produto
                    and tmrp_630.grupo_produto        = v_grupo_produto
                    and tmrp_630.subgrupo_produto     = v_subgrupo_produto
                    and tmrp_630.item_produto         = v_item_produto
                    and tmrp_630.alternativa_produto  = 0;
                  exception when others
          then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            else
               begin
                  insert into tmrp_630
                    (ordem_planejamento,              area_producao,     ordem_prod_compra,
                     nivel_produto,                   grupo_produto,     subgrupo_produto,
                     item_produto,                    pedido_venda,      programado_comprado,
                     alternativa_produto)
                  values
                    (reg_tmrp_625.ordem_planejamento, 9,                 v_ordem_compra,
                     v_nivel_produto,                 v_grupo_produto,   v_subgrupo_produto,
                     v_item_produto,                  v_pedido_venda,    v_qtde_reserva,
                     0);
                  exception when others
                      then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            end if;
         end loop;
      end if;
   end if;

end inter_tr_supr_100_plan;

-- ALTER TRIGGER "INTER_TR_SUPR_100_PLAN" ENABLE
 

/

exec inter_pr_recompile;

