
  CREATE OR REPLACE PROCEDURE "INTER_PR_ALIMENTA_OPER_020" 
   (tabela    in varchar2,   lista_programas in varchar2)
is
begin

declare
   v_programas      varchar2(4000);
   v_posicao        number(3);
   v_count          number(3);
   v_programa       varchar2(100);


   cursor gatilhos is
      select trigger_name from all_triggers
       where table_name = tabela
         and (trigger_name like '%INTER_%' or trigger_name like '%TRIGGER_%');

   cursor objeto_procedures is
     SELECT p.object_name
       FROM all_objects p
     where p.object_type in ('PROCEDURE','TRIGGER','FUNCTION')
      and (p.object_name like '%INTER_%' or p.object_name like '%P_GERA%' or p.object_name like '%TRIGGER_%' or p.object_name = 'VERIFICA_ESTRUTURA');


begin


   for reg_trig in gatilhos
   loop

      v_programas := lista_programas||',';

      loop
         select instr(v_programas,',',1,1) into v_posicao  from dual;
         select substr(v_programas,1,(v_posicao-1)) into v_programa from dual;
         select replace(v_programas,''||v_programa||',','') into v_programas from dual;

         begin
            select 1
            into v_count
            from oper_020
            where nome_objeto = reg_trig.trigger_name
              and nome_formul = v_programa;
          exception when no_data_found then
            insert into oper_020
               (nome_objeto, nome_formul)
            values
               (reg_trig.trigger_name, v_programa);
          end;


         for reg_obj in objeto_procedures
         loop
            begin
               select count(*)
               into v_count
               from user_source a
               where a.name = reg_trig.trigger_name
                 and upper(a.text) like '%'||upper(reg_obj.object_name)||'%'
                 and reg_trig.trigger_name <> reg_obj.object_name;
            exception when no_data_found then
               v_count := 0;
            end;

            if v_count > 0
            then
                begin
                   select 1
                   into v_count
                   from oper_020
                   where nome_objeto = reg_obj.object_name
                     and nome_formul = v_programa;
                exception when no_data_found then
                  insert into oper_020
                    (nome_objeto, nome_formul)
                  values
                    (reg_obj.object_name, v_programa);
                end;
            end if;
         end loop;

         exit when v_programas is null;
      end loop;
   end loop;

   commit work;
end;
end inter_pr_alimenta_oper_020;

 

/

exec inter_pr_recompile;

