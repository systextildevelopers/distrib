
  CREATE OR REPLACE PROCEDURE "INTER_PR_ATUALIZA_HOMOLOGACAO" 
    -- Recebe parametros para atualizacao das etiquetas.
   (p_nivel_tec    in varchar2,    p_grupo_tec         in varchar2,
    p_subgrupo_tec in varchar2,    p_item_tec          in varchar2,
    p_pedido_tec   in number,      p_controle_etiqueta in varchar2,
    p_peso_do_rolo in number)
is

   -- Cria cursor para executar loop na basi_400 buscando todas
   -- os homologacoes cadastradas para o produto.
   cursor basi400 is
   select basi_400.codigo_informacao,   basi_400.valor
   from basi_400
   where  basi_400.nivel    = p_nivel_tec
     and  basi_400.grupo    = p_grupo_tec
     and (basi_400.subgrupo = p_subgrupo_tec or basi_400.subgrupo = '000')
     and (basi_400.item     = p_item_tec     or basi_400.item     = '000000')
     and  basi_400.tipo_informacao = 1; /* HOMOLOGACAO */

   -- Declara as variaveis que serao utilizadas.
   p_tipo_homologacao     basi_400.codigo_informacao%type;
   p_qtde_etiquetas       basi_400.valor%type;

   p_etiquetas_previstas  pedi_425.etiquetas_previstas%type;
   p_etiquetas_enviadas   pedi_425.etiquetas_enviadas%type;
   p_sequencia            pedi_425.sequencia%type;
   p_ult_sequencia        pedi_425.sequencia%type;

   p_cgc9                 pedi_100.cli_ped_cgc_cli9%type;
   p_cgc4                 pedi_100.cli_ped_cgc_cli4%type;
   p_cgc2                 pedi_100.cli_ped_cgc_cli2%type;

   p_fator_correcao       pedi_420.fator_correcao%type;

   p_existe_001 number;
   p_existe_425 number;
   p_existe_420 number;

begin

   -- Inicio do loop da explosao de etiquetas para o produto.
   for reg_basi400 in basi400
   loop

      -- Carrega as variaveis com os valores do cursor.
      p_tipo_homologacao := reg_basi400.codigo_informacao;
      p_qtde_etiquetas   := reg_basi400.valor;

      -- Busca o cliente do pedido de venda para buscar o fator de
      -- correcao que sera aplicado na quantidade de etiquetas.
      begin
         select pedi_100.cli_ped_cgc_cli9, pedi_100.cli_ped_cgc_cli4,
                pedi_100.cli_ped_cgc_cli2
         into   p_cgc9,                    p_cgc4,
                p_cgc2
         from pedi_100
         where pedi_100.pedido_venda = p_pedido_tec;
         exception
         when others then
            p_cgc9 := 0;
            p_cgc4 := 0;
            p_cgc2 := 0;
            raise_application_error(-20000,'Pedido de venda nao encontrado.(PEDI_100)');
      end;

      -- LOGICA PARA REMOVER ETIQUETAS, desaloca o rolo do pedido de venda.
      if p_controle_etiqueta = 'R'
      then
         begin
            -- A Trigger nao testa "status", para contornar isso, utilizaremos no
            -- decorrer da procedure contadores, para verificar se o registro
            -- existe ou nao.

            -- Verifica se o tipo de Homologacao e valida.
            p_existe_001 := 0;

            begin
               select count(*)
               into p_existe_001
               from hdoc_001
               where hdoc_001.tipo   = 1 /* HOMOLOGACAO */
                 and hdoc_001.codigo = p_tipo_homologacao;
               exception
               when others then
                  p_existe_001 := 0;
                  raise_application_error(-20000,'Homologacao nao encontrada.(HDOC_001 1)');
            end;

            if p_existe_001 > 0
            then
               -- Verifica se existe etiquetas geradas para o pedido
               -- para abater as etiquetas.
               p_existe_425 := 0;

               begin
                  select count(*)
                  into p_existe_425
                  from pedi_425
                  where pedi_425.pedido_venda        = p_pedido_tec
                    and pedi_425.tipo_homologacao    = p_tipo_homologacao
                    and pedi_425.etiquetas_enviadas  = 0 -- So abate de nao foi enviada nenhuma etiqueta.
                    and pedi_425.etiquetas_previstas > 0;
                  exception
                  when others then
                     p_existe_425 := 0;
                     raise_application_error(-20000,'Nao foi encontrado etiquetas para o pedido.(PEDI_425 1)');
               end;

               -- Se existe quantidade de etiquetas para o pedido de venda
               -- carrega as informacoes das quantidades e sequencia que
               -- deve ser atualizada.
               if p_existe_425 > 0
               then
                  if p_existe_425 > 1
                  then
                     select etiquetas_previstas,
                            etiquetas_enviadas,
                            sequencia
                     into   p_etiquetas_previstas,
                            p_etiquetas_enviadas,
                            p_sequencia
                     from (select pedi_425.etiquetas_previstas,
                                  pedi_425.etiquetas_enviadas,
                                  pedi_425.sequencia
                           from pedi_425
                           where pedi_425.pedido_venda        = p_pedido_tec
                             and pedi_425.tipo_homologacao    = p_tipo_homologacao
                             and pedi_425.etiquetas_enviadas  = 0
                             and pedi_425.etiquetas_previstas > 0)
                     where rownum = 1;
                  end if;

                  if p_existe_425 = 1
                  then
                     select pedi_425.etiquetas_previstas,
                            pedi_425.etiquetas_enviadas,
                            pedi_425.sequencia
                     into   p_etiquetas_previstas,
                            p_etiquetas_enviadas,
                            p_sequencia
                     from pedi_425
                     where pedi_425.pedido_venda        = p_pedido_tec
                       and pedi_425.tipo_homologacao    = p_tipo_homologacao
                       and pedi_425.etiquetas_enviadas  = 0
                       and pedi_425.etiquetas_previstas > 0;
                  end if;

                  -- Tem que buscar o fator de correcao para envio de etiquetas
                  -- por cliente.
                  p_existe_420 := 0;
                  begin
                     select count(*)
                     into p_existe_420
                     from pedi_420
                     where  pedi_420.cliente9         = p_cgc9
                       and  pedi_420.cliente4         = p_cgc4
                       and  pedi_420.cliente2         = p_cgc2
                       and  pedi_420.nivel            = p_nivel_tec
                       and  pedi_420.grupo            = p_grupo_tec
                       and (pedi_420.subgrupo         = p_subgrupo_tec
                        or  pedi_420.subgrupo         = '000')
                       and (pedi_420.item             = p_item_tec
                        or  pedi_420.item             = '000000')
                       and  pedi_420.tipo_homologacao = p_tipo_homologacao;
                  end;

                  if p_existe_420 > 0
                  then
                     begin
                        select pedi_420.fator_correcao
                        into   p_fator_correcao
                        from pedi_420
                        where  pedi_420.cliente9         = p_cgc9
                          and  pedi_420.cliente4         = p_cgc4
                          and  pedi_420.cliente2         = p_cgc2
                          and  pedi_420.nivel            = p_nivel_tec
                          and  pedi_420.grupo            = p_grupo_tec
                          and (pedi_420.subgrupo         = p_subgrupo_tec
                           or  pedi_420.subgrupo         = '000')
                          and (pedi_420.item             = p_item_tec
                           or  pedi_420.item             = '000000')
                          and  pedi_420.tipo_homologacao = p_tipo_homologacao
                          and rownum                     = 1;
                     end;
                  end if;

                  if p_fator_correcao is null
                  or p_fator_correcao = 0.00
                  then
                     p_fator_correcao := 1.00;
                  end if;

                  -- Gera a quantidade de etiquetas que deve ser diminuida.
                  p_qtde_etiquetas := p_peso_do_rolo   *
                                      p_qtde_etiquetas *
                                      p_fator_correcao;

                  update pedi_425
                  set pedi_425.etiquetas_faturadas  =
                      pedi_425.etiquetas_faturadas - p_qtde_etiquetas
                  where pedi_425.pedido_venda     = p_pedido_tec
                    and pedi_425.tipo_homologacao = p_tipo_homologacao
                    and pedi_425.sequencia        = p_sequencia
                    and pedi_425.etiquetas_previstas > 0;
               end if;
            end if;
         end;
      end if;

      -- LOGICA PARA ADICIONAR ETIQUETAS
      if p_controle_etiqueta = 'A'
      then
         begin
            -- A Trigger nao testa "status", para contornar isso, utilizaremos no
            -- decorrer da procedure contadores, para verificar se o registro
            -- existe ou nao.

            -- Verifica se o tipo de Homologacao e valida.
            p_existe_001 := 0;

            begin
               select count(*)
               into p_existe_001
               from hdoc_001
               where hdoc_001.tipo   = 1 /* HOMOLOGACAO */
                 and hdoc_001.codigo = p_tipo_homologacao;
               exception
               when others then
                  p_existe_001 := 0;
                  raise_application_error(-20000,'Homologacao nao encontrada.(HDOC_001 2)');
            end;

            if p_existe_001 > 0
            then
               -- Verifica se existe etiquetas geradas para o pedido
               -- para adicionar as etiquetas.
               p_existe_425 := 0;

               begin
                  select count(*)
                  into p_existe_425
                  from pedi_425
                  where pedi_425.pedido_venda        = p_pedido_tec
                    and pedi_425.tipo_homologacao    = p_tipo_homologacao
                    and pedi_425.etiquetas_previstas > 0;
                  exception
                  when others then
                     p_existe_425 := 0;
                     raise_application_error(-20000,'Nao foi encontrado etiquetas para o pedido.(PEDI_425 2)');
               end;

               -- Se existe quantidade de etiquetas para o pedido de venda
               -- carrega as informacoes da quantidades e tipo de homologacao
               -- sequencia que deve ser atualizada.
               if p_existe_425 > 0
               then
                  begin
                     if p_existe_425 > 1
                     then
                        select etiquetas_previstas,
                               etiquetas_enviadas,
                               sequencia
                        into   p_etiquetas_previstas,
                               p_etiquetas_enviadas,
                               p_sequencia
                        from (select pedi_425.etiquetas_previstas,
                                     pedi_425.etiquetas_enviadas,
                                     pedi_425.sequencia
                              from pedi_425
                              where pedi_425.pedido_venda        = p_pedido_tec
                                and pedi_425.tipo_homologacao    = p_tipo_homologacao
                                and pedi_425.etiquetas_previstas > 0
                              order by pedi_425.etiquetas_enviadas asc)
                        where rownum = 1;
                     end if;

                     if p_existe_425 = 1
                     then
                        begin
                           select pedi_425.etiquetas_previstas,
                                  pedi_425.etiquetas_enviadas,
                                  pedi_425.sequencia
                           into   p_etiquetas_previstas,
                                  p_etiquetas_enviadas,
                                  p_sequencia
                           from pedi_425
                           where pedi_425.pedido_venda        = p_pedido_tec
                             and pedi_425.tipo_homologacao    = p_tipo_homologacao
                             and pedi_425.etiquetas_previstas > 0;
                        end;
                     end if;
                  end;

                  -- **************************************************
                  -- ****   Se nao foi enviada nenhuma etiqueta.   ****
                  -- **************************************************
                  if p_etiquetas_enviadas = 0
                  then
                     p_existe_420 := 0;
                     begin
                        select count(*)
                        into p_existe_420
                        from pedi_420
                        where  pedi_420.cliente9         = p_cgc9
                          and  pedi_420.cliente4         = p_cgc4
                          and  pedi_420.cliente2         = p_cgc2
                          and  pedi_420.nivel            = p_nivel_tec
                          and  pedi_420.grupo            = p_grupo_tec
                          and (pedi_420.subgrupo         = p_subgrupo_tec
                           or  pedi_420.subgrupo         = '000')
                          and (pedi_420.item             = p_item_tec
                           or  pedi_420.item             = '000000')
                          and  pedi_420.tipo_homologacao = p_tipo_homologacao;
                     end;

                     if p_existe_420 > 0
                     then
                        begin
                           select pedi_420.fator_correcao
                           into   p_fator_correcao
                           from pedi_420
                           where  pedi_420.cliente9         = p_cgc9
                             and  pedi_420.cliente4         = p_cgc4
                             and  pedi_420.cliente2         = p_cgc2
                             and  pedi_420.nivel            = p_nivel_tec
                             and  pedi_420.grupo            = p_grupo_tec
                             and (pedi_420.subgrupo         = p_subgrupo_tec
                              or  pedi_420.subgrupo         = '000')
                             and (pedi_420.item             = p_item_tec
                              or  pedi_420.item             = '000000')
                             and  pedi_420.tipo_homologacao = p_tipo_homologacao
                             and rownum                     = 1;
                        end;
                     end if;

                     if p_fator_correcao is null
                     or p_fator_correcao = 0.00
                     then
                        p_fator_correcao := 1.00;
                     end if;

                     p_qtde_etiquetas := p_peso_do_rolo   *
                                         p_qtde_etiquetas *
                                         p_fator_correcao;

                     update pedi_425
                     set pedi_425.etiquetas_faturadas  =
                         pedi_425.etiquetas_faturadas + p_qtde_etiquetas
                      where pedi_425.pedido_venda        = p_pedido_tec
                        and pedi_425.tipo_homologacao    = p_tipo_homologacao
                        and pedi_425.sequencia           = p_sequencia
                        and pedi_425.etiquetas_previstas > 0;
                  end if;

                  -- **************************************************
                  -- ****   Se ja teve etiquetas enviadas.         ****
                  -- **************************************************
                  if p_etiquetas_enviadas > 0
                  then
                     p_existe_425 := 0;
                     begin
                        select count(*)
                        into p_existe_425
                        from pedi_425
                        where pedi_425.pedido_venda        = p_pedido_tec
                          and pedi_425.tipo_homologacao    = p_tipo_homologacao
                          and pedi_425.etiquetas_enviadas  = 0
                          and pedi_425.etiquetas_previstas > 0;
                        exception
                        when others then
                           p_existe_425 := 0;
                           raise_application_error(-20000,'Nao foi encontrado etiquetas para o pedido.(PEDI_425 3)');
                     end;

                     -- Se nao existe quantidade de etiquetas para o pedido de venda
                     -- carrega as informacoes da quantidades e tipo de homologacao
                     -- sequencia que deve ser incluida / atualizada.
                     if p_existe_425 = 0
                     then
                        -- Busca a ultima sequencia da homologacao, para incluir uma
                        -- nova sequencia.
                        p_ult_sequencia := 0;
                        begin
                           select max(pedi_425.sequencia)
                           into   p_ult_sequencia
                           from pedi_425
                           where pedi_425.pedido_venda        = p_pedido_tec
                             and pedi_425.tipo_homologacao    = p_tipo_homologacao
                             and pedi_425.etiquetas_previstas > 0;
                        end;
                        p_sequencia := p_ult_sequencia + 1;

                        p_existe_420 := 0;
                        begin
                           select count(*)
                           into p_existe_420
                           from pedi_420
                           where  pedi_420.cliente9         = p_cgc9
                             and  pedi_420.cliente4         = p_cgc4
                             and  pedi_420.cliente2         = p_cgc2
                             and  pedi_420.nivel            = p_nivel_tec
                             and  pedi_420.grupo            = p_grupo_tec
                             and (pedi_420.subgrupo         = p_subgrupo_tec
                              or  pedi_420.subgrupo         = '000')
                             and (pedi_420.item             = p_item_tec
                              or  pedi_420.item             = '000000')
                             and  pedi_420.tipo_homologacao = p_tipo_homologacao;
                        end;

                        if p_existe_420 > 0
                        then
                           begin
                              select pedi_420.fator_correcao
                              into   p_fator_correcao
                              from pedi_420
                              where  pedi_420.cliente9         = p_cgc9
                                and  pedi_420.cliente4         = p_cgc4
                                and  pedi_420.cliente2         = p_cgc2
                                and  pedi_420.nivel            = p_nivel_tec
                                and  pedi_420.grupo            = p_grupo_tec
                                and (pedi_420.subgrupo         = p_subgrupo_tec
                                 or  pedi_420.subgrupo         = '000')
                                and (pedi_420.item             = p_item_tec
                                 or  pedi_420.item             = '000000')
                                and  pedi_420.tipo_homologacao = p_tipo_homologacao
                                and rownum                     = 1;
                           end;
                        end if;

                        if p_fator_correcao is null
                        or p_fator_correcao = 0.00
                        then
                           p_fator_correcao := 1.00;
                        end if;

                        p_qtde_etiquetas := p_peso_do_rolo   *
                                            p_qtde_etiquetas *
                                            p_fator_correcao;

                        -- Verifica se o registro existe, para executar insert ou
                        -- update da tabela de etiquetas.
                        p_existe_425 := 0;
                        begin
                           select count(*)
                           into p_existe_425
                           from pedi_425
                           where pedi_425.pedido_venda        = p_pedido_tec
                             and pedi_425.tipo_homologacao    = p_tipo_homologacao
                             and pedi_425.sequencia           = p_sequencia;
                           exception
                           when others then
                              p_existe_425 := 0;
                              raise_application_error(-20000,'Nao foi encontrado etiquetas para o pedido.(PEDI_425 4)');
                        end;

                        if p_existe_425 = 0
                        then
                           insert into pedi_425
                             (pedido_venda,           tipo_homologacao,
                              sequencia,              etiquetas_previstas,
                              etiquetas_faturadas)
                           values
                             (p_pedido_tec,           p_tipo_homologacao,
                              p_sequencia,            p_etiquetas_previstas,
                              p_qtde_etiquetas);
                        else
                           update pedi_425
                           set pedi_425.etiquetas_faturadas  =
                               pedi_425.etiquetas_faturadas + p_qtde_etiquetas
                           where pedi_425.pedido_venda        = p_pedido_tec
                             and pedi_425.tipo_homologacao    = p_tipo_homologacao
                             and pedi_425.sequencia           = p_sequencia
                             and pedi_425.etiquetas_previstas > 0;
                        end if;
                     end if;
                  end if;
               end if;
            end if;
         end;
      end if;
   end loop;
end inter_pr_atualiza_homologacao;

 

/

exec inter_pr_recompile;

