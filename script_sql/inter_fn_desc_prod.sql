create or replace FUNCTION           INTER_FN_DESC_PROD (
			p_nivel 		varchar2,
			p_grupo 		varchar2,
			p_subgrupo 		varchar2,
			p_item 			varchar2)
return VARCHAR2 is
   descr_produto varchar2(300);
   desc_030 basi_030.DESCR_REFERENCIA%type;
   desc_020 basi_020.DESCR_TAM_REFER%type;
   desc_010 basi_010.DESCRICAO_15%type;
  
begin
	if p_grupo = '00000'
    then
        return '';
    else 
        begin
            select DESCR_REFERENCIA
            into desc_030
            from basi_030
            where NIVEL_ESTRUTURA 	= p_nivel
            and   REFERENCIA 	    = p_grupo;
        exception when others then
            desc_030 := '';
        end;
    end if;

    if p_subgrupo = '000'
    then
        desc_020 := '';
    else 
        begin
           select DESCR_TAM_REFER
           into desc_020
           from basi_020
           where BASI030_NIVEL030 	= p_nivel
           and   BASI030_REFERENC 	= p_grupo
           and   TAMANHO_REF        = p_subgrupo;
        exception when others then
           desc_020 := '';
        end;
    end if;

    if p_item = '000000'
    then
        desc_010 := '';
    else 
        begin
           select DESCRICAO_15
           into desc_010
           from basi_010
           where NIVEL_ESTRUTURA 	= p_nivel
           and   GRUPO_ESTRUTURA 	= p_grupo
           and   p_subgrupo     in (SUBGRU_ESTRUTURA,'000')
           and   ITEM_ESTRUTURA 	= p_item;
        exception when others then
           desc_010 := '';
        end;    
    end if;
    
    descr_produto := trim(desc_030) || ' ' || trim(desc_020 ) || ' ' || trim(desc_010);
    return descr_produto;
end;

/

exec inter_pr_recompile;

