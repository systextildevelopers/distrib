CREATE OR REPLACE FUNCTION INTER_FN_ENCONTRA_CONTA_DATA (p_cod_empresa          in number,
                                                           p_tipo_contab          in number,
                                                           p_codigo_contab        in number,
                                                           p_transacao            in number,
                                                           p_c_custo              in number,
                                                           p_data_movimento       in date)
return number   is v_conta_contab_ret  number;
 
v_empresa_matriz             fatu_500.codigo_matriz%type;
v_exercicio                  cont_500.exercicio%type;
 
begin
   v_conta_contab_ret := 0;
 
   begin
      select fatu_500.codigo_matriz
      into v_empresa_matriz
      from fatu_500
      where fatu_500.codigo_empresa = p_cod_empresa;
 
      select cont_500.exercicio
      into v_exercicio
      from cont_500
      where cont_500.cod_empresa = v_empresa_matriz
      and   p_data_movimento between cont_500.per_inicial and cont_500.per_final;
 
      v_conta_contab_ret := inter_fn_encontra_conta(p_cod_empresa, p_tipo_contab, p_codigo_contab, p_transacao, p_c_custo, v_exercicio, v_exercicio);
 
   exception
      when no_data_found then
         v_conta_contab_ret := -99;
   end;
 
   return(v_conta_contab_ret);
end inter_fn_encontra_conta_data;
/
