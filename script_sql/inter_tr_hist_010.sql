
CREATE OR REPLACE TRIGGER "INTER_TR_HIST_010" 
before insert on hist_010
  for each row

declare
   v_nome_programa varchar2(60);
   v_sid           number(9);
   v_osuser        varchar2(20);
   v_nome_usuario  varchar2(250);

   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_empresa                number(3);
   ws_locale_usuario         varchar2(5);

begin
      inter_pr_dados_usuario (v_osuser,        ws_maquina_rede,   ws_aplicativo,     v_sid,
                              v_nome_usuario,  ws_empresa,        ws_locale_usuario);

      v_nome_programa := inter_fn_nome_programa(v_sid); 

   :new.nome_programa    := substr(v_nome_programa, 1, 20);
   :new.usuario_sistema  := substr(v_nome_usuario, 1, 250);
end inter_tr_hist_010;


-- ALTER TRIGGER "INTER_TR_HIST_010" ENABLE
 

/

exec inter_pr_recompile;

