
  CREATE OR REPLACE PROCEDURE "INTER_PR_RELATORIO_RECALCULO" (p_string_mensagem varchar2,
                                                         p_integer_tipo_erro number)
is
   v_sequencia number(9);
begin
   begin
      select nvl(max(oper_tmp.sequencia),0) + 1
      into v_sequencia
      from oper_tmp
      where oper_tmp.nr_solicitacao = 77777
        and oper_tmp.nome_relatorio = 'recalculo';
   exception
   when OTHERS then
      v_sequencia := 1;
   end;

   begin
      insert into oper_tmp(
         nr_solicitacao,    nome_relatorio,
         sequencia,

         str_73,            int_01
      )values(
         77777,            'recalculo',
         v_sequencia,

         p_string_mensagem,  p_integer_tipo_erro
      );
   exception
   when OTHERS then
      v_sequencia := 0;
      raise_application_error(-20000,sqlerrm);
   end;
end inter_pr_relatorio_recalculo;

 

/

exec inter_pr_recompile;

