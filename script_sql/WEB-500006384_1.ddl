alter table fatu_504
add(valida_cor_representativa number(1) default 0);

comment on column fatu_504.valida_cor_representativa is 'Este campo serve consistir o formato quando é efetuado o cadastro da cor representativa onde 0 - valida RGB e 1 - consiste formato RGB e HEXA ';
