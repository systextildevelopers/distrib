
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_010_INFOTINT" 
before insert
on basi_010
for each row

begin
   if INSERTING and (:new.nivel_estrutura = '9' or :new.nivel_estrutura = '2')
   then
      :new.ind_envia_infotint := 'S';
   end if;
end;

-- ALTER TRIGGER "INTER_TR_BASI_010_INFOTINT" ENABLE
 

/

exec inter_pr_recompile;

