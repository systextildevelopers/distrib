create table obrf_058 (
cod_empresa              number(03)    default 0    not null ,
mes                      number(02)    default 0    not null ,
ano                      number(04)    default 0    not null ,
vl_debito_difal          number(13,2)  default 0.00,
vl_out_deb_difal         number(13,2)  default 0.00,
tot_debitos              number(13,2)  default 0.00,
vl_sld_cred_ant_difal    number(13,2)  default 0.00,
vl_creditos_difal        number(13,2)  default 0.00,
vl_out_cred_difal        number(13,2)  default 0.00,
tot_creditos             number(13,2)  default 0.00,
vl_pagto_antecipados     number(13,2)  default 0.00,
vl_saldo_devedor         number(13,2)  default 0.00,
vl_saldo_devedor_grafica number(13,2)  default 0.00,
vl_saldo_credor          number(13,2)  default 0.00,
vl_saldo_credor_mes_segt number(13,2)  default 0.00
);

alter table obrf_058
add constraint obrf_058 primary key (cod_empresa, mes, ano );

comment on column obrf_058.cod_empresa              is 'Codigo de empresa para quadro 13';  
comment on column obrf_058.mes                      is 'Mes para registro quadro 13';  
comment on column obrf_058.ano                      is 'Ano para registro quadro 13';  

comment on column obrf_058.vl_debito_difal          is '010 - Debito da diferenca de aliquota devido ao estado: lancar o somatorio dos valores do ICMS referente a parcela da diferenca de aliquota devida ao estado constante nas notas fiscais de saidas';
comment on column obrf_058.vl_out_deb_difal         is '020 - Outros debitos: informar os valores correspondentes a outros debitos de diferenca de aliquota que nao se enquadre no item anterio';

comment on column obrf_058.tot_debitos              is 'Total dos debitos';

comment on column obrf_058.vl_sld_cred_ant_difal    is '050 - 100 - Pagamentos Antecipados - informar o montante dos valores correspondente e parcela da diferenca de aliquota devido ao estado em decorrencia de operacoes ou prestacoes interestaduais destinadas a consumidor final, que tenham sido recolhidos antecipadamente em cada operacao ou prestacao';
comment on column obrf_058.vl_creditos_difal        is '060 - Devolucao de mercadorias e anulacoes de venda';
comment on column obrf_058.vl_out_cred_difal        is '070 - Outros creditos';

comment on column obrf_058.tot_creditos             is 'Total de creditos';
comment on column obrf_058.vl_pagto_antecipados     is '100 -valor total de saldo devedor icms diferencial de aliquota da uf de origem/destino antes das deducoes';

comment on column obrf_058.vl_saldo_devedor         is '120 - Total de Creditos: demonstrativo dos creditos, que sera informado sempre que o somatorio dos itens 90 (Total de creditos) e 110 (Total de Pagamentos Antecipados) for superior ao item 040 (Total de debito)';
comment on column obrf_058.vl_saldo_devedor_grafica is 'Saldo devedor em conta grafica';
comment on column obrf_058.vl_saldo_credor          is '150 - Saldo Credor: preencher com o valor da diferenca entre o somatorio dos itens 90 (Total de creditos) e 110 (Total Pagamentos Antecipados) e o item 40 (Total de debitos) se o somatorio for maior que o total de debitos.';

comment on column obrf_058.vl_saldo_credor_mes_segt is 'Saldo credor para o mes seguinte';

exec inter_pr_recompile ;
/
