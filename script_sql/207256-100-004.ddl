alter table ftec_202 add    data_alteracao  date;
alter table ftec_202 add    resp_alteracao  varchar2(20);
alter table ftec_202 add    observacao      varchar2(500);

comment on column ftec_202.data_alteracao is 'Data da última alteração do registro';
comment on column ftec_202.resp_alteracao is 'Responsável pela última alteração do registro';
comment on column ftec_202.observacao     is 'Observação do registro';

exec inter_pr_recompile;
