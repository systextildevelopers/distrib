create or replace trigger wms_tr_ondas_log
  before insert
  or delete
  or update of cod_deposito, onda, timestamp_aimportar, tipo_tag_ean
  on inte_wms_ondas
  for each row
declare
  -- local variables here
  v_cod_deposito        inte_wms_ondas.cod_deposito        %type;
  v_onda                inte_wms_ondas.onda                %type;
  v_timestamp_aimportar inte_wms_ondas.timestamp_aimportar %type;
  v_tipo_tag_ean        inte_wms_ondas.tipo_tag_ean        %type;

  v_sid                 number(9);
  v_empresa             number(3);
  v_usuario_systextil   varchar2(250);
  v_locale_usuario      varchar2(5);
  v_nome_programa       varchar2(20);

  v_operacao            varchar(1);
  v_data_operacao       date;
  v_usuario_rede        varchar(20);
  v_maquina_rede        varchar(40);
  v_aplicativo          varchar(20);

begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();

   --alimenta as variaveis new caso seja insert
   if inserting
   then
      v_operacao := 'i';

      v_cod_deposito        := :new.cod_deposito;
      v_onda                := :new.onda;
      v_timestamp_aimportar := :new.timestamp_aimportar;
      v_tipo_tag_ean        := :new.tipo_tag_ean;

   end if; --fim do if inserting or updating

   --alimenta as variaveis old caso seja insert ou update
   if deleting
   then
      v_operacao      := 'd';

      v_cod_deposito        := :old.cod_deposito;
      v_onda                := :old.onda;
      v_timestamp_aimportar := :old.timestamp_aimportar;
      v_tipo_tag_ean        := :old.tipo_tag_ean;

   end if; --fim do if deleting or updating


   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);


    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente

   --insere na inte_wms_ondas_log o registro.
   insert into inte_wms_ondas_log (
      cod_deposito,        onda,
      timestamp_aimportar, tipo_tag_ean,
      operacao,            data_operacao,       
      usuario_rede,        maquina_rede,        
      aplicativo,          nome_programa
   )
   values (
      v_cod_deposito,        v_onda,
      v_timestamp_aimportar, v_tipo_tag_ean,
      v_operacao,            v_data_operacao,       
      v_usuario_rede,        v_maquina_rede,        
      v_aplicativo,          v_nome_programa
   );

end wms_tr_ondas_log;

/

exec inter_pr_recompile;
/* versao: 3 */


 exit;


 exit;
