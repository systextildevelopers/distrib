create or replace trigger "TR_PCPB_040_PCPB_012" 
   before delete or
          update of data_termino, hora_termino
   on pcpb_040
   for each row

declare
    v_estagio_prepara number;
    v_atualiza_qtd number;
    v_qtd_prod number;
    v_has_pcpb_012 number;
begin
    begin
        SELECT DEFAULT_INT 
            into v_atualiza_qtd
        FROM EMPR_007 
        WHERE PARAM = 'pcpb.atualizaQtdProdEstg';

    exception when others then
        v_atualiza_qtd := 0;
    end;

    if v_atualiza_qtd = 1 then

        if updating then
            select ESTAGIO_PREPARA
                into v_estagio_prepara
            from EMPR_001;

            if :new.CODIGO_ESTAGIO <> v_estagio_prepara then
                if (:NEW.data_termino is null and :OLD.data_termino is not null) THEN
                    begin
                        delete from pcpb_012
                        where CODIGO_ESTAGIO = :new.CODIGO_ESTAGIO
                        and SEQ_OPERACAO = :new.SEQ_OPERACAO
                        and ORDEM_PRODUCAO = :new.ORDEM_PRODUCAO
                        ;
                    exception when others then
                        null;
                    end;
                end if;

                if (:NEW.data_termino is not null and :OLD.data_termino is null) or (:NEW.data_termino <> :OLD.data_termino) then
                
                    select SUM(QTDE_QUILOS_REAL)
                        into v_qtd_prod
                    from PCPB_020
                    where ORDEM_PRODUCAO = :new.ORDEM_PRODUCAO;

                    select count(*)
                        into v_has_pcpb_012
                    from pcpb_012
                    where CODIGO_ESTAGIO = :new.CODIGO_ESTAGIO
                    and SEQ_OPERACAO = :new.SEQ_OPERACAO
                    and ORDEM_PRODUCAO = :new.ORDEM_PRODUCAO
                    ;

                    if v_has_pcpb_012 > 0 then
                        begin
                            update pcpb_012
                            set QTDE_PRODUZIDA = v_qtd_prod
                            where CODIGO_ESTAGIO = :new.CODIGO_ESTAGIO
                            and SEQ_OPERACAO = :new.SEQ_OPERACAO
                            and ORDEM_PRODUCAO = :new.ORDEM_PRODUCAO
                            ;
                        exception when others then
                            null;
                        end;
                    else
                        begin
                            insert into pcpb_012 (
                                TIPO_EFIC_PREP        , ORDEM_PRODUCAO
                                , CODIGO_ESTAGIO      , OPERACAO
                                , PANO_SBG_NIVEL99    , PANO_SBG_GRUPO
                                , PANO_SBG_SUBGRUPO   , PANO_SBG_ITEM
                                , CODIGO_ACOMP        , LOTE_ACOMP
                                , SEQ_OPERACAO        , PB12PB10_PC05PB10
                                , PB12PB10_NR_GBENE   , GRUPO_MAQUINA
                                , SUB_MAQUINA         , NUMERO_MAQUINA
                                , TURNO               , DATA_PRODUCAO
                                , PRIORIDADE          , QTDE_PRODUZIDA
                                , CODIGO_OPERADOR
                            ) 
                            select 2
                            , :new.ORDEM_PRODUCAO
                            , :new.CODIGO_ESTAGIO
                            , PCPB_015.CODIGO_OPERACAO 
                            , PCPB_020.PANO_SBG_NIVEL99
                            , PCPB_020.PANO_SBG_GRUPO
                            , PCPB_020.PANO_SBG_SUBGRUPO
                            , PCPB_020.PANO_SBG_ITEM
                            , 0
                            , 0
                            , :new.SEQ_OPERACAO
                            , 0
                            , 0
                            , null
                            , null
                            , null
                            , :new.TURNO_PRODUCAO
                            , :new.DATA_TERMINO
                            , 0
                            , v_qtd_prod
                            , :new.CODIGO_USUARIO
                            from PCPB_020
                            inner join PCPB_015 on PCPB_015.ORDEM_PRODUCAO = PCPB_020.ORDEM_PRODUCAO
                                                and PCPB_015.SEQ_OPERACAO = :new.SEQ_OPERACAO
                            where PCPB_020.ORDEM_PRODUCAO = :new.ORDEM_PRODUCAO
                            ;
                        exception when others then
                            null;
                        end;
                    end if;
                end if;

            end if;
        elsif deleting then
            begin
                delete from pcpb_012
                where CODIGO_ESTAGIO = :new.CODIGO_ESTAGIO
                and SEQ_OPERACAO = :new.SEQ_OPERACAO
                and ORDEM_PRODUCAO = :new.ORDEM_PRODUCAO
                ;
            exception when others then
                null;
            end;
        end if;
    end if;
end;
