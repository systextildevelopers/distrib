ALTER TABLE obrf_700
DROP CONSTRAINT PK_OBRF_700;

drop index PK_OBRF_700;

ALTER TABLE obrf_700
ADD CONSTRAINT PK_OBRF_700 PRIMARY KEY
(mes_apur, ano_apur, cod_empresa, num_nota, ser_nota, cod_cred, reg, cod_part9, cod_part4, cod_part2, 
cod_nivel, cod_grupo, cod_subgru, cod_item, nat_bc_cred, ind_orig_cred, cod_cta);

exec inter_pr_recompile;
