create table blocok_215 (
	COD_EMPRESA             number(3),
	ANO_PERIODO_APUR        number(4),
	MES_PERIODO_APUR        number(2),
	
	SEQ_ID_O                number(15),
	DATA_MOVIMENTO          date,

	COD_ITEM_DES            varchar2(60),
	QTD_DES                 number(15,6),
	
	data_hora_ins           date            default sysdate
);
