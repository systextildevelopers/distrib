alter table pedi_080 add red_icms_base_pis_cof number(1) default 0;

comment on column pedi_080.red_icms_base_pis_cof is 'Identifica se deve reduzir o valor do ICMS da base de Pis/Cofins (0: Não, 1: Sim)';

exec inter_pr_recompile;
