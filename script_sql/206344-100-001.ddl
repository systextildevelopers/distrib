alter table orcm_001 add flag_orcamento     number(1)   default 1;
alter table orcm_001 add flag_manutencao    number(1)   default 1;

comment on column orcm_001.flag_orcamento  is '1: Indica se a permissao deste centro de custo e valida para o modulo de orcamento OU 0: para nao';
comment on column orcm_001.flag_manutencao is '1: Indica se a permissao deste centro de custo e valida para o modulo de manutencao OU 0: para nao';

exec inter_pr_recompile;
