create or replace PROCEDURE "INTER_PR_LINHA_PROD" (
    p_cgc9               IN  number,
    p_cgc4               IN  number,
    p_cgc2               IN  number,
    p_new_publico        OUT varchar2,
    p_new_segmento       OUT varchar2,
    p_new_ramo_atividade OUT varchar2
)
is
    --Publico
    v_feminino number;     
    v_masculino number;
    v_juvenil number;
    v_bebe number; 
    v_senhora number;
    v_outros_publico number;
    
    --Segmento
    v_sportwear number;
    v_casualwear number;
    v_streetwear number;
    v_surfwear number;
    v_activewear number;
    v_intima number;
    v_praia number;
    v_fashion number;
    v_outros_segmento number; 

    --Ramo atividade
    v_confeccao number;
    v_atac_conf number;
    v_var_conf number;
    v_atac_teci number;
    v_var_teci number;
    v_outros_rm_atvd number;
begin
    select ALVO_FEMININO,   ALVO_MASCULINO, ALVO_JUVENIL,
            ALVO_BEBE,       ALVO_SENHORA,   ALVO_OUTROS, --Publico
            SEG_SPORTWEAR,   SEG_CASUALWEAR, SEG_STREETWEAR,
            SEG_SURFWEAR,    SEG_ACTIVEWEAR, SEG_INTIMA,
            SEG_PRAIA,       FASHION,        SEG_OUTROS, --SEGMENTO
            ATIV_CONFECCAO,  ATIV_ATAC_CONF, ATIV_VAR_CONF,
            ATIV_ATAC_TECI,  ATIV_VAR_TECI,  ATIV_OUTROS --RAMO ATIVIDADE
    into    v_feminino,      v_masculino,    v_juvenil,
            v_bebe,          v_senhora,      v_outros_publico, --Publico
            v_sportwear,     v_casualwear,   v_streetwear,
            v_surfwear,      v_activewear,   v_intima,
            v_praia,         v_fashion,      v_outros_segmento, --Segmento
            v_confeccao,     v_atac_conf,    v_var_conf,
            v_atac_teci,     v_var_teci,     v_outros_rm_atvd --Ramo atividade
    from pedi_010
    where CGC_9 = p_cgc9
    and   CGC_4 = p_cgc4
    and   CGC_2 = p_cgc2;
    
    -- INICIO PUBLICO --
    if v_feminino = 1 then
        p_new_publico := ' FEMININO ';
    end if;

    if v_masculino = 1 then
        p_new_publico := p_new_publico || ' MASCULINO ';
    end if;

    if v_juvenil = 1 then
        p_new_publico := p_new_publico || ' JUVENIL ';
    end if;

    if v_bebe = 1 then
        p_new_publico := p_new_publico || ' BEB� ';
    end if;

    if v_senhora = 1 then
        p_new_publico := p_new_publico || ' SENHORA ';
    end if;

    if v_outros_publico = 1 then
        p_new_publico := p_new_publico || ' OUTROS ';
    end if;

    if p_new_publico is null then
        p_new_publico := 'NENHUM';
    end if;
    -- FIM PUBLICO --

    -- INICIO SEGMENTO --
    if v_sportwear = 1 then
        p_new_segmento := ' SPORTWEAR ';
    end if;

    if v_casualwear = 1 then
        p_new_segmento := p_new_segmento || ' CASUALWEAR ';
    end if;

    if v_streetwear = 1 then
        p_new_segmento := p_new_segmento || ' STREETWEAR ';
    end if;
    
    if v_surfwear = 1 then
        p_new_segmento := p_new_segmento || ' SURFWEAR ';
    end if;

    if v_activewear = 1 then
        p_new_segmento := p_new_segmento || ' ACTIVEWEAR ';
    end if;

    if v_intima = 1 then
        p_new_segmento := p_new_segmento || ' LINHA INTIMA ';
    end if;

    if v_praia = 1 then
        p_new_segmento := p_new_segmento || ' MODA PRAIA ';
    end if;

    if v_fashion = 1 then
        p_new_segmento := p_new_segmento || ' FASHION ';
    end if;

    if v_outros_segmento = 1 then
        p_new_segmento := p_new_segmento || ' OUTROS ';
    end if;

    if p_new_segmento is null then
        p_new_segmento := 'NENHUM';
    end if;
    -- FIM SEGMENTO --

    -- INICIO RAMO ATIVIDADE --
    if v_confeccao = 1 then
        p_new_ramo_atividade := ' CONFEC��O ';
    end if;

    if v_atac_conf = 1 then
        p_new_ramo_atividade := p_new_ramo_atividade || ' ATACADO DE CONFEC��O ';
    end if;

    if v_var_conf = 1 then
        p_new_ramo_atividade := p_new_ramo_atividade || ' VAREJO DE CONFEC��O ';
    end if;

    if v_atac_teci = 1 then
        p_new_ramo_atividade := p_new_ramo_atividade || ' ATACADO DE TECIDOS ';
    end if;

    if v_var_teci = 1 then
        p_new_ramo_atividade := p_new_ramo_atividade || ' VAREJO DE TECIDOS ';
    end if;

    if v_outros_rm_atvd = 1 then
        p_new_ramo_atividade := p_new_ramo_atividade || ' OUTROS ';
    end if;

    if p_new_ramo_atividade is null then
        p_new_ramo_atividade := 'NENHUM';
    end if;
    -- FIM RAMO ATIVIDADE --
end inter_pr_linha_prod;

/

exec inter_pr_recompile;
