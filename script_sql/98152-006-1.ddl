alter table basi_180 add (
min_cost number(13,2) default 0 null);

comment on column basi_180.min_cost 
is 'Minutos costura.';

exec inter_pr_recompile;
