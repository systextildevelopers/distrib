alter table obrf_140 add (cod_enquadramento_saida varchar2(3) default '999');

alter table obrf_140 add (cod_enquadramento_entrada varchar2(3) default '999');

alter table obrf_140 add (cod_enquadramento_exportacao varchar2(3) default '999');

comment on column obrf_140.cod_enquadramento_saida is 'C�digo de enquadramento de IPI para sa�das.';

comment on column obrf_140.cod_enquadramento_entrada is 'C�digo de enquadramento de IPI para entradas.';

comment on column obrf_140.cod_enquadramento_exportacao is 'C�digo de enquadramento de IPI para importa��o.';

/
exec inter_pr_recompile;
/
