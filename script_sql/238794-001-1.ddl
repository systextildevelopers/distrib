create table SPED_CTB_J100_AUX
(
  COD_EMPRESA           NUMBER(3) default 0,
  EXERCICIO             NUMBER(4) default 0,
  TIPO_DEMONSTRACAO     NUMBER(1) default 0,
  COD_AGLUTINACAO       VARCHAR2(20) default 0,
  NIVEL_AGLUTINACAO     NUMBER(1),
  DESCRICAO_AGLUTINACAO VARCHAR2(40),
  SALDO_FINAL           NUMBER(15,2),
  IND_NAT_AGRUTINACAO   VARCHAR2(1),
  IND_SALDO_AGLUTINACAO VARCHAR2(1),
  SEQUENCIA_DRE         NUMBER(3) default 0,
  NIVEL_DRE             NUMBER(1) default 0,
  TIPO_DRE              NUMBER(1) default 1,
  PERIODO               NUMBER(2) default 1 not null,
  ACUMULADO             NUMBER(1),
  COD_FILIAL            NUMBER(3) default 0,
  SALDO_INICIAL         NUMBER(15,2) default 0.00,
  IND_SALDO_INICIAL     VARCHAR2(1) default '',
  SALDO_PER_ANT         NUMBER(15,2),
  IND_SALDO_PER_ANT     VARCHAR2(1),
  IND_COD_AGL           VARCHAR2(1) default ' ',
  COD_AGL_SUP           VARCHAR2(20) default ' ',
  IND_DC_CTA            VARCHAR2(1) default ' ',
  IND_GRP_DRE           VARCHAR2(1) default ' ',
  CENTRO_CUSTO          NUMBER default 0
);

alter table SPED_CTB_J100_AUX
  add constraint PK_SPED_CTB_J100_AUX primary key (COD_EMPRESA, EXERCICIO, TIPO_DEMONSTRACAO, COD_AGLUTINACAO, PERIODO, CENTRO_CUSTO);
