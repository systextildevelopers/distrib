
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_030" 
   before insert or
          delete or
          update of pedido_corte,
                    qtde_quilos_prog,
                    qtde_quilos_produzido,
                    alternativa,
                    codigo_embalagem
   on pcpb_030
   for each row

declare
   v_periodo_producao    pcpb_010.periodo_producao%type;
   v_situacao_ordem      pcpb_010.situacao_ordem%type;
   v_tipo_ordem          pcpb_010.tipo_ordem%type;

   v_sub_ler             mqop_050.subgru_estrutura%type := '###';
   v_item_ler            mqop_050.item_estrutura%type   := '######';
   v_nr_registro         number;

   v_seq_destino         inte_088.seq_destino%type;
   v_des_destino         inte_088.des_destino%type;
   v_inte88_aux          number;
   v_tipo_prod_deposito  inte_088.seq_destino%type;
   v_tipo_prod           number;
   v_criterio_pedi       number;
   v_modular             number;

   v_data_embarque       date;
   v_data_embarque_velha date;
   v_data_embarque_nova  date;

   v_registro            number;
   v_nr_registro_031     number;

   v_ordem_tingimento    pcpb_010.ordem_tingimento%type;

   v_alternativa_020     number;
   v_roteiro_020         number;

   v_saldo_qtde_quilos   pcpb_030.qtde_quilos_prog%type;

   v_ordem_producao      pcpb_030.ordem_producao%type;
   v_sequencia           pcpb_030.sequencia%type;
   v_pano_nivel99        pcpb_030.pano_nivel99%type;
   v_pano_grupo          pcpb_030.pano_grupo%type;
   v_pano_subgrupo       pcpb_030.pano_subgrupo%type;
   v_pano_item           pcpb_030.pano_item%type;
   v_alternativa           pcpb_030.alternativa%type;
   v_roteiro             pcpb_030.roteiro%type;
   
   v_vip1                number;
begin
   v_vip1 := 0;
      
   if inserting
   then
      if :new.pedido_corte = 3
      then
         begin
            select nvl(pedi_010.vip,0) vip
            into v_vip1
            from  pedi_102, pedi_100, pedi_010
            where pedi_102.pedido_destino   = :new.nr_pedido_ordem
            and   pedi_100.pedido_venda     = pedi_102.pedido_original
            and   pedi_100.cli_ped_cgc_cli9 = pedi_010.cgc_9
            and   pedi_100.cli_ped_cgc_cli4 = pedi_010.cgc_4
            and   pedi_100.cli_ped_cgc_cli2 = pedi_010.cgc_2;
         exception
         when others then
            v_vip1 := 0;
         end;
          
         if v_vip1 <= 0
         then
            begin
               select pedi_010.vip 
               into v_vip1
               from pedi_100, pedi_010
               where pedi_100.cli_ped_cgc_cli9 = pedi_010.cgc_9
                 and pedi_100.cli_ped_cgc_cli4 = pedi_010.cgc_4
                 and pedi_100.cli_ped_cgc_cli2 = pedi_010.cgc_2
                 and pedi_100.pedido_venda     = :new.nr_pedido_ordem;
               exception
               when others then
                  v_vip1 := 0;
            end;
         end if;
      end if;         
      
      if :new.pedido_corte = 5
      then
         begin
            select pedi_010.vip into v_vip1
            from pedi_010
            where pedi_010.cgc_9 = :new.nr_pedido_ordem
              and pedi_010.cgc_4 = :new.sequenci_periodo
              and pedi_010.cgc_2 = :new.codigo_deposito;
            exception
            when others then
               v_vip1 := 0;
         end;
      end if;
   end if;
   
   begin
      if inserting
      then
         v_ordem_producao := :new.ordem_producao;
         v_sequencia      := :new.sequencia;
         v_pano_nivel99   := :new.pano_nivel99;
         v_pano_grupo     := :new.pano_grupo;
         v_pano_subgrupo  := :new.pano_subgrupo;
         v_pano_item      := :new.pano_item;
         v_alternativa    := :new.alternativa;
         v_roteiro        := :new.roteiro;
      else
         v_ordem_producao := :old.ordem_producao;
         v_sequencia      := :old.sequencia;
         v_pano_nivel99   := :old.pano_nivel99;
         v_pano_grupo     := :old.pano_grupo;
         v_pano_subgrupo  := :old.pano_subgrupo;
         v_pano_item      := :old.pano_item;
         v_alternativa    := 0;                     -- Conforme logica anterior
         v_roteiro        := 0;
      end if;

      -- Busca a situacao da OB.
      select pcpb_010.situacao_ordem,   pcpb_010.tipo_ordem,
             pcpb_010.ordem_tingimento, pcpb_010.periodo_producao
      into   v_situacao_ordem,          v_tipo_ordem,
             v_ordem_tingimento,        v_periodo_producao
      from pcpb_010
      where pcpb_010.ordem_producao   = v_ordem_producao
        and pcpb_010.cod_cancelamento = 0;
      exception
      when others then
         v_periodo_producao := 0;
         v_situacao_ordem   := 0;
         v_tipo_ordem       := 0;
         v_ordem_tingimento := 0;
   end;

   begin
      select pcpb_020.alternativa_item,     pcpb_020.roteiro_opcional
      into   v_alternativa_020,             v_roteiro_020
      from pcpb_020
      where pcpb_020.ordem_producao    = v_ordem_producao
        and pcpb_020.sequencia         = v_sequencia
        and pcpb_020.pano_sbg_nivel99  = v_pano_nivel99
        and pcpb_020.pano_sbg_grupo    = v_pano_grupo
        and pcpb_020.pano_sbg_subgrupo = v_pano_subgrupo
        and pcpb_020.pano_sbg_item     = v_pano_item;
      exception
      when others then
         v_alternativa_020 := v_alternativa;
         v_roteiro_020     := v_roteiro;
   end;


   if inserting
   then
      -- Sempre antes de chamar a procedure para atualizar estagios e operacoes
      -- a logica abaixo deve ser executada para definicao do produto.
      v_sub_ler     := :new.pano_subgrupo;
      v_item_ler    := :new.pano_item;
      v_nr_registro := 0;

      begin
         select count(*)
         into v_nr_registro
         from mqop_050
         where mqop_050.nivel_estrutura  = :new.pano_nivel99
           and mqop_050.grupo_estrutura  = :new.pano_grupo
           and mqop_050.subgru_estrutura = v_sub_ler
           and mqop_050.item_estrutura   = v_item_ler
           and mqop_050.numero_alternati = v_alternativa_020
           and mqop_050.numero_roteiro   = v_roteiro_020;
         if v_nr_registro = 0
         then
            v_item_ler := '000000';
            begin
               select count(*)
               into v_nr_registro
               from mqop_050
               where mqop_050.nivel_estrutura  = :new.pano_nivel99
                 and mqop_050.grupo_estrutura  = :new.pano_grupo
                 and mqop_050.subgru_estrutura = v_sub_ler
                 and mqop_050.item_estrutura   = v_item_ler
                 and mqop_050.numero_alternati = v_alternativa_020
                 and mqop_050.numero_roteiro   = v_roteiro_020;
               if v_nr_registro = 0
               then
                  v_sub_ler  := '000';
                  v_item_ler := :new.pano_item;
                  begin
                     select count(*)
                     into v_nr_registro
                     from mqop_050
                     where mqop_050.nivel_estrutura  = :new.pano_nivel99
                       and mqop_050.grupo_estrutura  = :new.pano_grupo
                       and mqop_050.subgru_estrutura = v_sub_ler
                       and mqop_050.item_estrutura   = v_item_ler
                       and mqop_050.numero_alternati = v_alternativa_020
                       and mqop_050.numero_roteiro   = v_roteiro_020;
                     if v_nr_registro = 0
                     then
                        v_item_ler := '000000';
                     end if;
                  end;
               end if;
            end;
         end if;
      end;

      -- Chama procedure para geracao dos estagios e operacoes para OB.
      inter_pr_ordem_beneficiamento(:new.pano_nivel99,
                                    :new.pano_grupo,
                                    v_sub_ler,
                                    v_item_ler,
                                    v_alternativa_020,
                                    v_roteiro_020,
                                    :new.ordem_producao,
                                    v_situacao_ordem,
                                    :new.sequencia,
                                    :new.qtde_quilos_prog,
                                    v_tipo_ordem,
                                    'I');
      
      inter_pr_manu_estagios(:new.ordem_producao,
                             :new.sequencia,
                             :new.qtde_quilos_prog,
                             v_vip1, v_tipo_ordem);

      -- Busca a data de embarque/entrega do pedido de venda destinado a OB
      -- para calcular com o centro de custo os dias para producao.
      v_data_embarque       := null;
      v_data_embarque_nova  := null;
      v_data_embarque_velha := null;

      begin
         if :new.pedido_corte = 3
         then
            select pedi_100.data_entr_venda
            into v_data_embarque_nova
            from pedi_100
            where pedi_100.pedido_venda = :new.nr_pedido_ordem;
         end if;
      end;

      begin
         v_registro := 0;

         select count(*)
         into v_registro
         from pcpb_030
         where pcpb_030.ordem_producao = :new.ordem_producao;

         if v_registro > 0
         then
            select min(pedi_100.data_entr_venda)
            into   v_data_embarque_velha
            from pedi_100, pcpb_030
            where pedi_100.pedido_venda   = pcpb_030.nr_pedido_ordem
              and pcpb_030.ordem_producao = :new.ordem_producao
              and pcpb_030.pedido_corte   = 3;
         end if;

         if v_data_embarque_nova  is not null
         or v_data_embarque_velha is not null
         then
            if v_data_embarque_nova > v_data_embarque_velha
            then
               v_data_embarque := v_data_embarque_velha;
            else
               v_data_embarque := v_data_embarque_nova;
            end if;
         end if;
      end;

      v_inte88_aux := 0;
      v_seq_destino := 0;

      begin
        select count(*) into v_inte88_aux
        from inte_088;
      exception when NO_DATA_FOUND
      then
         v_inte88_aux := 0;
      end;

      if v_inte88_aux > 0
      then
        if :new.pedido_corte = 3
        then
           begin
              select pedi_100.tipo_prod_pedido, pedi_100.criterio_pedido,
                     pedi_110.modular
              into   v_tipo_prod,               v_criterio_pedi,
                     v_modular
              from pedi_100, pedi_110
              where pedi_100.pedido_venda = pedi_110.pedido_venda
                and pedi_100.pedido_venda = :new.nr_pedido_ordem
                and pedi_100.tecido_peca  = '2'
                and pedi_110.seq_item_pedido = :new.sequenci_periodo;
           exception
           when OTHERS then
              v_tipo_prod := 0;
           end;
           if v_modular = 1
           then
              select inte_088.seq_destino
              into   v_seq_destino
              from   inte_088
              where  inte_088.cod_destino = 'MD';

              :new.seq_destino := v_des_destino;
           else
              if v_tipo_prod = 5
              then
                 begin
                    select inte_088.seq_destino
                    into   v_seq_destino
                    from   inte_088
                    where  inte_088.cod_destino = 'EX';
                 exception
                 when OTHERS then
                    v_seq_destino := 0;
                 end;
              else
                 if v_criterio_pedi = 0
                 then
                    begin
                       select inte_088.seq_destino
                       into   v_seq_destino
                       from   inte_088
                       where  inte_088.cod_destino = 'PN';
                    exception
                    when OTHERS then
                       v_seq_destino := 0;
                    end;
                 else
                    begin
                       select inte_088.seq_destino
                       into   v_seq_destino
                       from   inte_088
                       where  inte_088.cod_destino = 'CL';
                    exception
                    when OTHERS then
                       v_seq_destino := 0;
                    end;
                 end if;
              end if;
           end if;
        else
           if :new.pedido_corte = 2 --Previs�o
           then
              begin
                 select basi_205.tipo_prod_deposito
                 into v_tipo_prod_deposito
                 from basi_205
                 where basi_205.codigo_deposito = :new.codigo_deposito;
              exception
              when OTHERS then
                 v_tipo_prod_deposito := 0;
              end;

              if v_tipo_prod_deposito = 2
              then
                  begin
                     select inte_088.seq_destino
                     into   v_seq_destino
                     from   inte_088
                     where  inte_088.cod_destino = 'SQ';
                  exception
                  when OTHERS then
                     v_seq_destino := 0;
                  end;
              end if;
           else
              if v_tipo_prod = 6 --Corte
              then
                 begin
                     select inte_088.seq_destino
                     into   v_seq_destino
                     from   inte_088
                     where  inte_088.cod_destino = 'CT';
                  exception
                  when OTHERS then
                     v_seq_destino := 0;
                  end;
              end if;
           end if;
        end if;

        :new.seq_destino := v_seq_destino;
      end if;
      -- Chama procedure para calcular a prioridade das operacoes.
      inter_pr_calcula_seq_ob(:new.ordem_producao,
                              v_ordem_tingimento,
                              v_data_embarque,
                              :new.sequencia,
                              'PCPB_030',
                              :new.pano_nivel99,
                              :new.pano_grupo,
                              v_sub_ler,
                              v_item_ler,
                              v_alternativa_020,
                              v_roteiro_020);

      -- Chama a procedure que explode a estrutura gerando planejamento
      -- a partir dos destinos criados para a ordem.
      ----
      -- O planejamento do tecido principal e feito na trigger da tabela
      -- PCPB_020.
      for reg_pcpb_040 in (select pcpb_040.codigo_estagio
                           from pcpb_040
                           where pcpb_040.ordem_producao = :new.ordem_producao
                             and pcpb_040.data_termino  is null)
      loop
         inter_pr_explode_estrutura(:new.pano_nivel99,
                                    :new.pano_grupo,
                                    :new.pano_subgrupo,
                                    :new.pano_item,
                                    v_alternativa_020,
                                    v_periodo_producao,
                                    :new.qtde_quilos_prog,
                                    :new.ordem_producao,
                                    0,
                                    2,
                                    :new.sequencia,
                                    reg_pcpb_040.codigo_estagio,
                                    reg_pcpb_040.codigo_estagio,
                                    0.00,
                                    :new.codigo_embalagem,
                                    'pcpb',1);
      end loop;
   end if;


   if updating
   then
      if :old.pedido_corte <> 3 and :new.pedido_corte = 3
      then
         -- Busca a data de embarque/entrega do pedido de venda destinado a OB
         -- para calcular com o centro de custo os dias para producao.
         v_data_embarque       := null;
         v_data_embarque_nova  := null;
         v_data_embarque_velha := null;

         begin
            select pedi_100.data_entr_venda
            into   v_data_embarque_nova
            from pedi_100
            where pedi_100.pedido_venda = :new.nr_pedido_ordem;
            exception
            when others then
               v_data_embarque_nova := null;
         end;

         begin
            select min(pedi_100.data_entr_venda)
            into   v_data_embarque_velha
            from pedi_100, pcpb_030
            where pedi_100.pedido_venda   = pcpb_030.nr_pedido_ordem
              and pcpb_030.ordem_producao = :new.ordem_producao
              and pcpb_030.pedido_corte   = 3;
            exception
            when others then
               v_data_embarque_velha := null;
         end;

         if v_data_embarque_nova  is not null
         or v_data_embarque_velha is not null
         then
            if v_data_embarque_nova > v_data_embarque_velha
            then
               v_data_embarque := v_data_embarque_velha;
            else
               v_data_embarque := v_data_embarque_nova;
            end if;
         end if;

         -- Chama procedure para calcular a prioridade das operacoes.
         inter_pr_calcula_seq_ob(:new.ordem_producao,
                                 v_ordem_tingimento,
                                 v_data_embarque,
                                 :new.sequencia,
                                 'PCPB_030',
                                 :new.pano_nivel99,
                                 :new.pano_grupo,
                                 v_sub_ler,
                                 v_item_ler,
                                 v_alternativa_020,
                                 v_roteiro_020);
      end if;


      -- Se alterar a quantidade, chama a procedure que explode a estrutura
      -- com a diferenca do valor antigo para o novo.
      ----
      -- O planejamento do tecido principal e feito na trigger da tabela
      -- PCPB_020.
      if :old.qtde_quilos_prog       <> :new.qtde_quilos_prog      and
         :old.codigo_embalagem       =  :new.codigo_embalagem      and
         :old.alternativa            =  :new.alternativa
      then
         begin
            v_saldo_qtde_quilos := 0.000;

            if :old.qtde_quilos_prog <> :new.qtde_quilos_prog
            then
               v_saldo_qtde_quilos := v_saldo_qtde_quilos + :new.qtde_quilos_prog - :old.qtde_quilos_prog;
            end if;

            begin
               for reg_pcpb_040 in (select pcpb_040.codigo_estagio
                                    from pcpb_040
                                    where pcpb_040.ordem_producao = :old.ordem_producao
                                      and pcpb_040.data_termino  is null)
               loop
                  inter_pr_explode_estrutura(:old.pano_nivel99,
                                             :old.pano_grupo,
                                             :old.pano_subgrupo,
                                             :old.pano_item,
                                             v_alternativa_020,
                                             v_periodo_producao,
                                             v_saldo_qtde_quilos,
                                             :old.ordem_producao,
                                             0,
                                             2,
                                             :old.sequencia,
                                             reg_pcpb_040.codigo_estagio,
                                             reg_pcpb_040.codigo_estagio,
                                             0.00,
                                             :old.codigo_embalagem,
                                             'pcpb', 1);
               end loop;
            end;
         end;
      end if;

      -- Explode negativo pra eliminar os registros com alternativa
      -- antiga e depois explode positivo pra cadastrar com a nova alternativa.
      ----
      -- Se alterar a embalagem, entao, estorna a qtdade antiga e
      -- lanca a quantidade novamente.
      ---
      -- OBS: a tela de destinos pcpb_f018 nao deixa alterar o
      -- codigo de embalagem se tiver quantidade produzida.
      -- Julio autorizou esta trava.
      ----
      -- A alternativa e alterada somente pela trigger da pcpb_020.
      ----
      -- O planejamento do tecido principal e feito na trigger da tabela
      -- PCPB_020.
      if :old.alternativa      <> :new.alternativa or
         :old.codigo_embalagem <> :new.codigo_embalagem
      then
         begin

            v_saldo_qtde_quilos := :old.qtde_quilos_prog * (-1);

            for reg_pcpb_040 in (select pcpb_040.codigo_estagio
                                 from pcpb_040
                                 where pcpb_040.ordem_producao = :old.ordem_producao
                                 and pcpb_040.data_termino  is null)
            loop

               -- Somente devera explodir a estrutura estornando
               -- a necessidade quando alterar o codigo da embalagem,
               -- pois, no quando se altera alternativa o sistema
               -- elimina as necessidades atraves da trigger
               -- inter_tr_pcpb_020.
               if :old.codigo_embalagem <> :new.codigo_embalagem
               then
                  begin
                     inter_pr_explode_estrutura(:old.pano_nivel99,
                                                :old.pano_grupo,
                                                :old.pano_subgrupo,
                                                :old.pano_item,
                                                :old.alternativa,
                                                v_periodo_producao,
                                                v_saldo_qtde_quilos,
                                                :old.ordem_producao,
                                                0,
                                                2,
                                                :old.sequencia,
                                                reg_pcpb_040.codigo_estagio,
                                                reg_pcpb_040.codigo_estagio,
                                                0.00,
                                                :old.codigo_embalagem,
                                                'pcpb', 1);
                  end;
               end if;

               inter_pr_explode_estrutura(:old.pano_nivel99,
                                          :old.pano_grupo,
                                          :old.pano_subgrupo,
                                          :old.pano_item,
                                          :new.alternativa,
                                          v_periodo_producao,
                                          :new.qtde_quilos_prog,
                                          :old.ordem_producao,
                                          0,
                                          2,
                                          :old.sequencia,
                                          reg_pcpb_040.codigo_estagio,
                                          reg_pcpb_040.codigo_estagio,
                                          0.00,
                                          :new.codigo_embalagem,
                                          'pcpb',1);
            end loop;
         end;
      end if;
   end if;

   if ((updating and :new.qtde_quilos_prog <> :old.qtde_quilos_prog)
   and :new.executa_trigger = 0)
   or  deleting
   then
      select count(1)
      into v_nr_registro_031
      from pcpb_031
      where pcpb_031.ordem_producao = :old.ordem_producao;
      if v_nr_registro_031 > 0
      then
         begin
            delete pcpb_031
            where pcpb_031.ordem_producao = :old.ordem_producao;
         end;
      end if;
   end if;

   if deleting
   then
      -- Sempre antes de chamar a procedure para atualizar estagios e operacoes
      -- a logica abaixo deve ser executada para definicao do produto.
      v_sub_ler     := :old.pano_subgrupo;
      v_item_ler    := :old.pano_item;
      v_nr_registro := 0;

      begin
         select count(*)
         into v_nr_registro
         from mqop_050
         where mqop_050.nivel_estrutura  = :old.pano_nivel99
           and mqop_050.grupo_estrutura  = :old.pano_grupo
           and mqop_050.subgru_estrutura = v_sub_ler
           and mqop_050.item_estrutura   = v_item_ler
           and mqop_050.numero_alternati = v_alternativa_020
           and mqop_050.numero_roteiro   = v_roteiro_020;
         if v_nr_registro = 0
         then
            v_item_ler := '000000';
            begin
               select count(*)
               into v_nr_registro
               from mqop_050
               where mqop_050.nivel_estrutura  = :old.pano_nivel99
                 and mqop_050.grupo_estrutura  = :old.pano_grupo
                 and mqop_050.subgru_estrutura = v_sub_ler
                 and mqop_050.item_estrutura   = v_item_ler
                 and mqop_050.numero_alternati = v_alternativa_020
                 and mqop_050.numero_roteiro   = v_roteiro_020;
               if v_nr_registro = 0
               then
                  v_sub_ler  := '000';
                  v_item_ler := :old.pano_item;
                  begin
                     select count(*)
                     into v_nr_registro
                     from mqop_050
                     where mqop_050.nivel_estrutura  = :old.pano_nivel99
                       and mqop_050.grupo_estrutura  = :old.pano_grupo
                       and mqop_050.subgru_estrutura = v_sub_ler
                       and mqop_050.item_estrutura   = v_item_ler
                       and mqop_050.numero_alternati = v_alternativa_020
                       and mqop_050.numero_roteiro   = v_roteiro_020;
                     if v_nr_registro = 0
                     then
                        v_item_ler := '000000';
                     end if;
                  end;
               end if;
            end;
         end if;
      end;

      -- Chama procedure para atualizacao dos estagios e operacoes para OB.
      inter_pr_ordem_beneficiamento(:old.pano_nivel99,
                                    :old.pano_grupo,
                                    v_sub_ler,
                                    v_item_ler,
                                    v_alternativa_020,
                                    v_roteiro_020,
                                    :old.ordem_producao,
                                    v_situacao_ordem,
                                    :old.sequencia,
                                    :old.qtde_quilos_prog,
                                    v_tipo_ordem,
                                    'D');

      -- Se deletar destino, entao, explode a estrutura com valor negativo.
      ----
      -- O planejamento do tecido principal e feito na trigger da tabela
      -- PCPB_020.
      begin
         v_saldo_qtde_quilos := :old.qtde_quilos_prog * (-1);
         inter_pr_explode_estrutura(:old.pano_nivel99,
                                    :old.pano_grupo,
                                    :old.pano_subgrupo,
                                    :old.pano_item,
                                    v_alternativa_020,
                                    v_periodo_producao,
                                    v_saldo_qtde_quilos,
                                    :old.ordem_producao,
                                    0,
                                    2,
                                    :old.sequencia,
                                    0,
                                    0,
                                    0.00,
                                    :old.codigo_embalagem,
                                    'pcpb',1);
      end;

   end if;
end inter_tr_pcpb_030;
-- ALTER TRIGGER "INTER_TR_PCPB_030" ENABLE
 

/

exec inter_pr_recompile;

