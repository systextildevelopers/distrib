
  CREATE OR REPLACE PROCEDURE "LOJA_PR_VALIDA_DAV" (pCODFIL IN varchar2,
                                                 pNUMPED IN varchar2,
                                                 pTIPPED IN varchar2,
                                                 p_msg_erro out varchar2) is
begin
  loja_pr_valida_dav_inte(pCODFIL,
                         pNUMPED,
                         pTIPPED,
                         p_msg_erro);
end loja_pr_valida_dav;
 

/

exec inter_pr_recompile;

