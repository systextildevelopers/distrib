update hdoc_035
   set hdoc_035.descricao = 'Motivos de Exclusão/Inativação de Clientes'
where codigo_programa = 'pedi_f060';

update hdoc_036
   set hdoc_036.descricao       = 'Motivos de Exclusión/Inactivación de Clientes'
 where hdoc_036.codigo_programa = 'pedi_f060'
   and hdoc_036.locale          = 'es_ES';
   
commit;
