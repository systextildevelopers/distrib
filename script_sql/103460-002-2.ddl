insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_f181', 'Cadastro de configuração de preparação de pedido por cliente', 1,0);


insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu,
 incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_f181', 'nenhum', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu,
 incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pedi_f181', 'nenhum', 0, 0, 'S', 'S', 'S', 'S');


UPDATE hdoc_036
SET hdoc_036.descricao = 'Cadastro de configuração de preparação de pedido por cliente'
WHERE hdoc_036.codigo_programa = 'pedi_f181'
      AND hdoc_036.locale = 'es_ES';

commit;
/
