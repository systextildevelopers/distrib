
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_020_PMS" 
before delete or
       update of cod_cancelamento
   on pcpc_020
   for each row

declare
   v_qtde_programada         pcpc_040.qtde_pecas_prog%type;
   v_qtde_produzida          pcpc_040.qtde_pecas_prod%type;
   v_lote_producao           i_pcpc_040.lote_producao%type;
   v_descricao_cor           basi_010.descricao_15%type;
   v_proconf_subgrupo        pcpc_040.proconf_subgrupo%type;

   v_executa_trigger         number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      if updating and :new.cod_cancelamento > 0
      then
         v_lote_producao := 0;
         for exporta_canc in (
            select sum(pcpc_040.qtde_pecas_prog) v_qtde_programada,
                   sum(pcpc_040.qtde_pecas_prod +  pcpc_040.qtde_conserto +
                       pcpc_040.qtde_pecas_2a   +  pcpc_040.qtde_perdas) v_qtde_produzida,
                   pcpc_040.proconf_grupo,         pcpc_040.proconf_item,
                   pcpc_040.codigo_estagio
            from pcpc_040, basi_030
            where pcpc_040.ordem_producao  = :new.ordem_producao
              and pcpc_040.periodo_producao= :new.periodo_producao
              and pcpc_040.proconf_nivel99 = basi_030.nivel_estrutura
              and pcpc_040.proconf_grupo   = basi_030.referencia
              and pcpc_040.codigo_estagio  = basi_030.estagio_altera_programado
            group by pcpc_040.ordem_producao,  pcpc_040.proconf_grupo,
                     pcpc_040.proconf_item,    pcpc_040.codigo_estagio
           )
         loop
         if exporta_canc.v_qtde_programada > 0.00 and
            exporta_canc.v_qtde_produzida  > 0.00 and
            exporta_canc.v_qtde_programada = exporta_canc.v_qtde_produzida
         then

            v_lote_producao := v_lote_producao + 1;

            select min(pcpc_040.proconf_subgrupo)
            into v_proconf_subgrupo
            from pcpc_040, basi_030
            where pcpc_040.ordem_producao    = :new.ordem_producao
              and pcpc_040.periodo_producao  = :new.periodo_producao
              and pcpc_040.proconf_nivel99   = '1'
              and pcpc_040.proconf_grupo     = exporta_canc.proconf_grupo
              and pcpc_040.proconf_item      = exporta_canc.proconf_item
              and pcpc_040.codigo_estagio    = exporta_canc.codigo_estagio;

            begin
               select basi_010.descricao_15
               into v_descricao_cor
               from basi_010
               where basi_010.nivel_estrutura  = '1'
                 and basi_010.grupo_estrutura  = exporta_canc.proconf_grupo
                 and basi_010.subgru_estrutura = v_proconf_subgrupo
                 and basi_010.item_estrutura   = exporta_canc.proconf_item;
            exception
               when no_data_found then
                  v_descricao_cor          := ' ';
            end;

            insert into i_pcpc_040
              (ordem_producao, referencia,
               lote_producao,  situacao_ordem,
               codigo_cor,     descricao_cor,
               qtde_programada)
             values
              (:new.ordem_producao,exporta_canc.proconf_grupo,
               v_lote_producao,    2,
               exporta_canc.proconf_item,     v_descricao_cor,
               exporta_canc.v_qtde_programada);
         end if;
         end loop;
      end if;
   end if;

end inter_tr_pcpc_020_pms;

-- ALTER TRIGGER "INTER_TR_PCPC_020_PMS" ENABLE
 

/

exec inter_pr_recompile;

