insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_f102', 'Cadastro de Relacionamento de Plano de Pagamento', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_f102', 'pedi_menu', 1, 10, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pedi_f102', 'pedi_menu' , 1, 10, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Cadastro de Ralacionamento de Plano de Pagamento'
 where hdoc_036.codigo_programa = 'pedi_f102'
   and hdoc_036.locale          = 'es_ES';
commit;
