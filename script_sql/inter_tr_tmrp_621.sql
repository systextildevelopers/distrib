
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_621" 
  before insert or
         delete on tmrp_621
  for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_qtde_vendida           number;
   v_codigo_projeto         varchar2(6);
   v_total_625              number;

   sequencia_acomp          number(9);
   v_nro_reg_tmrp_625       number(9);

begin

   if :new.alternativa_produto_old <> -1
   then
      begin
          -- Dados do usuario logado
         inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                                 ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

         v_codigo_projeto         := '000000';

         if inserting and :new.nivel_produto_origem = '0'
         then
            begin
               select basi_001.codigo_projeto
               into v_codigo_projeto
               from basi_001
               where basi_001.nivel_produto =  :new.nivel_produto
                 and basi_001.grupo_produto =  :new.grupo_produto;
            exception
            when OTHERS then
               v_codigo_projeto := '000000';
            end;
         end if;

         -- Quando insere um registro na tabela de produtos com troca de alternativa
         -- na programacao...
         if inserting
         then
            begin
               select tmrp_625.estagio_producao,            tmrp_625.tipo_calculo,
                      tmrp_625.consumo,
                      tmrp_625.situacao,
                      tmrp_625.qtde_adicional,
                      tmrp_625.qtde_reposicao,              tmrp_625.data_requerida,
                      tmrp_625.qtde_produzida_comprada,     tmrp_625.qtde_conserto,
                      tmrp_625.qtde_2qualidade,             tmrp_625.qtde_perdas,
                      tmrp_625.qtde_atendida_adicional,     tmrp_625.qtde_atendida_devolvida,
                      tmrp_625.data_requerida_prev,         tmrp_625.data_requerida_real,
                      tmrp_625.data_acertada_compra,        tmrp_625.data_acertada_desenv,
                      tmrp_625.observacao,                  tmrp_625.setor,
                      tmrp_625.relacao_banho,               tmrp_625.qtde_atendida_estoque,
                      tmrp_625.cons_unid_med_generica,
                      tmrp_625.tecido_principal,            tmrp_625.liquida_saldo
               into   :new.estagio_producao,                :new.tipo_calculo,
                      :new.consumo,
                      :new.situacao,
                      :new.qtde_adicional,
                      :new.qtde_reposicao,                  :new.data_requerida,
                      :new.qtde_produzida_comprada,         :new.qtde_conserto,
                      :new.qtde_2qualidade,                 :new.qtde_perdas,
                      :new.qtde_atendida_adicional,         :new.qtde_atendida_devolvida,
                      :new.data_requerida_prev,             :new.data_requerida_real,
                      :new.data_acertada_compra,            :new.data_acertada_desenv,
                      :new.observacao,                      :new.setor,
                      :new.relacao_banho,                   :new.qtde_atendida_estoque,
                      :new.cons_unid_med_generica,
                      :new.tecido_principal,                :new.liquida_saldo
               from tmrp_625
               where tmrp_625.ordem_planejamento         = :new.ordem_planejamento
                 and tmrp_625.pedido_venda               = :new.pedido_venda
                 and tmrp_625.pedido_reserva             = :new.pedido_reserva
                 and tmrp_625.seq_produto_origem         = :new.seq_produto_origem
                 and tmrp_625.nivel_produto_origem       = :new.nivel_produto_origem
                 and tmrp_625.grupo_produto_origem       = :new.grupo_produto_origem
                 and tmrp_625.subgrupo_produto_origem    = :new.subgrupo_produto_origem
                 and tmrp_625.item_produto_origem        = :new.item_produto_origem
                 and tmrp_625.alternativa_produto_origem = :new.alternativa_produto_origem
                 and tmrp_625.seq_produto                = :new.seq_produto
                 and tmrp_625.nivel_produto              = :new.nivel_produto
                 and tmrp_625.grupo_produto              = :new.grupo_produto
                 and tmrp_625.subgrupo_produto           = :new.subgrupo_produto
                 and tmrp_625.item_produto               = :new.item_produto
                 and tmrp_625.alternativa_produto        = :new.alternativa_produto_old;
            end;

            v_qtde_vendida := 0;

            -- A nova quantidade planejada, deve ser a quantidade a reeber programada
            -- do produto na nova alternativa.
            :new.qtde_reserva_planejada     := :new.qtde_areceber_programada;
            :new.unidades_reserva_planejada := :new.unidades_areceber_programada;

            -- Quilos...
            if :new.nivel_produto_origem = '0'
            then
               :new.qtde_vendida := :new.qtde_areceber_programada;
               v_qtde_vendida    := :new.qtde_areceber_programada;
            end if;

            -- Unidados...
            if :new.nivel_produto_origem <> '0'
            then
               :new.unidades_reserva_programada  := :new.unidades_areceber_programada;
            end if;

            -- Atualiza a tabela de produtos da ordem de planejamento, abatendo a quantidade que foi programada
            -- na alternativa antiga, ficando o saldo para programacao.
            begin
               update tmrp_625
               set   tmrp_625.qtde_reserva_planejada        = tmrp_625.qtde_reserva_planejada - :new.qtde_areceber_programada,
                     tmrp_625.qtde_vendida                  = decode(:new.nivel_produto_origem,'0',tmrp_625.qtde_vendida-:new.qtde_areceber_programada,tmrp_625.qtde_vendida),
                     tmrp_625.qtde_reserva_programada       = decode(:new.nivel_produto_origem,'0',tmrp_625.qtde_reserva_programada,tmrp_625.qtde_reserva_programada-:new.qtde_areceber_programada),
                     tmrp_625.unidades_areceber_programada  = tmrp_625.unidades_areceber_programada - :new.unidades_areceber_programada
               where tmrp_625.ordem_planejamento            = :new.ordem_planejamento
                 and tmrp_625.pedido_venda                  = :new.pedido_venda
                 and tmrp_625.pedido_reserva                = :new.pedido_reserva
                 and tmrp_625.seq_produto_origem            = :new.seq_produto_origem
                 and tmrp_625.nivel_produto_origem          = :new.nivel_produto_origem
                 and tmrp_625.grupo_produto_origem          = :new.grupo_produto_origem
                 and tmrp_625.subgrupo_produto_origem       = :new.subgrupo_produto_origem
                 and tmrp_625.item_produto_origem           = :new.item_produto_origem
                 and tmrp_625.alternativa_produto_origem    = :new.alternativa_produto_origem
                 and tmrp_625.seq_produto                   = :new.seq_produto
                 and tmrp_625.nivel_produto                 = :new.nivel_produto
                 and tmrp_625.grupo_produto                 = :new.grupo_produto
                 and tmrp_625.subgrupo_produto              = :new.subgrupo_produto
                 and tmrp_625.item_produto                  = :new.item_produto
                 and tmrp_625.alternativa_produto           = :new.alternativa_produto_old;
               exception  when others then
                  -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_625(inter_tr_tmrp_621(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;

            -- Update na coluna tmrp_625.qtde_reserva_planejada dos niveis abaixo do nivel que esta sendo programado
            inter_pr_up_qtde_reserva_plan (:new.ordem_planejamento,
                                           :new.pedido_venda,
                                           :new.pedido_reserva,
                                           :new.seq_produto,
                                           :new.nivel_produto,
                                           :new.grupo_produto,
                                           :new.subgrupo_produto,
                                           :new.item_produto,
                                           :new.alternativa_produto_old,
                                           :new.qtde_areceber_programada);

            if :new.nivel_produto <> '1'
            then
               select nvl(count(1),0)
               into v_total_625
               from tmrp_625
               where tmrp_625.ordem_planejamento         = :new.ordem_planejamento
                 and tmrp_625.pedido_venda               = :new.pedido_venda
                 and tmrp_625.pedido_reserva             = 0
                 and tmrp_625.seq_produto_origem         = :new.seq_produto_origem
                 and tmrp_625.nivel_produto_origem       = :new.nivel_produto_origem
                 and tmrp_625.grupo_produto_origem       = :new.grupo_produto_origem
                 and tmrp_625.subgrupo_produto_origem    = :new.subgrupo_produto_origem
                 and tmrp_625.item_produto_origem        = :new.item_produto_origem
                 and tmrp_625.alternativa_produto_origem = :new.alternativa_produto_origem
                 and tmrp_625.seq_produto                = :new.seq_produto
                 and tmrp_625.nivel_produto              = :new.nivel_produto
                 and tmrp_625.grupo_produto              = :new.grupo_produto
                 and tmrp_625.subgrupo_produto           = :new.subgrupo_produto
                 and tmrp_625.item_produto               = :new.item_produto
                 and tmrp_625.alternativa_produto        = :new.alternativa_produto;

               if v_total_625 = 0
               then
                  begin
                     insert into tmrp_625 (
                         ordem_planejamento,
                         pedido_venda,                             pedido_reserva,

                         nivel_produto_origem,                     grupo_produto_origem,
                         subgrupo_produto_origem,                  item_produto_origem,
                         alternativa_produto_origem,               seq_produto_origem,

                         nivel_produto,                            grupo_produto,
                         subgrupo_produto,                         item_produto,
                         alternativa_produto,                      seq_produto,

                         estagio_producao,                         consumo,
                         qtde_reserva_planejada,                   situacao,

                         codigo_projeto,                           seq_projeto_origem,
                         tipo_produto_origem,                      data_requerida,
                         relacao_banho,                            tipo_calculo,
                         cons_unid_med_generica,
                         unidades_reserva_planejada,               tecido_principal
                     ) values (
                         :new.ordem_planejamento,
                         :new.pedido_venda,                        0,

                         :new.nivel_produto_origem,                :new.grupo_produto_origem,
                         :new.subgrupo_produto_origem,             :new.item_produto_origem,
                         :new.alternativa_produto_origem,          :new.seq_produto_origem,

                         :new.nivel_produto,                       :new.grupo_produto,
                         :new.subgrupo_produto,                    :new.item_produto,
                         :new.alternativa_produto,                 :new.seq_produto,

                         :new.estagio_producao,                    :new.consumo,
                         :new.qtde_areceber_programada,            :new.situacao,

                         :new.codigo_projeto,                      :new.seq_projeto_origem,
                         :new.tipo_produto_origem,                 :new.data_requerida,
                         :new.relacao_banho,                       :new.tipo_calculo,
                         :new.cons_unid_med_generica,
                         :new.unidades_reserva_planejada,          :new.tecido_principal
                     );
                  exception when others then
                    -- ATENCAO! Nao inseriu {0}. Status = {1}.
                     raise_application_error(-20000,inter_fn_buscar_tag_composta('ds21797', 'tmrp_625(21)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                   end;
               end if;

               if v_total_625 > 0
               then
                  begin
                     update tmrp_625
                        set tmrp_625.qtde_reserva_planejada      = tmrp_625.qtde_reserva_planejada     + :new.qtde_areceber_programada,
                            tmrp_625.unidades_reserva_planejada  = tmrp_625.unidades_reserva_planejada + :new.unidades_reserva_planejada,
                            tmrp_625.relacao_banho               = :new.relacao_banho,
                            tmrp_625.tipo_calculo                = :new.tipo_calculo,
                            tmrp_625.estagio_producao            = :new.estagio_producao,
                            tmrp_625.consumo                     = :new.consumo,
                            tmrp_625.qtde_reserva_programada     = tmrp_625.qtde_reserva_programada     + :new.qtde_areceber_programada,
                            tmrp_625.unidades_reserva_programada = tmrp_625.unidades_reserva_programada + :new.unidades_reserva_programada,
                            tmrp_625.cons_unid_med_generica      = :new.cons_unid_med_generica,
                            tmrp_625.tecido_principal            = :new.tecido_principal
                     where tmrp_625.ordem_planejamento         = :new.ordem_planejamento
                       and tmrp_625.pedido_venda               = :new.pedido_venda
                       and tmrp_625.pedido_reserva             = :new.pedido_reserva
                       and tmrp_625.seq_produto_origem         = :new.seq_produto_origem
                       and tmrp_625.nivel_produto_origem       = :new.nivel_produto_origem
                       and tmrp_625.grupo_produto_origem       = :new.grupo_produto_origem
                       and tmrp_625.subgrupo_produto_origem    = :new.subgrupo_produto_origem
                       and tmrp_625.item_produto_origem        = :new.item_produto_origem
                       and tmrp_625.alternativa_produto_origem = :new.alternativa_produto_origem
                       and tmrp_625.seq_produto                = :new.seq_produto
                       and tmrp_625.nivel_produto              = :new.nivel_produto
                       and tmrp_625.grupo_produto              = :new.grupo_produto
                       and tmrp_625.subgrupo_produto           = :new.subgrupo_produto
                       and tmrp_625.item_produto               = :new.item_produto
                       and tmrp_625.alternativa_produto        = :new.alternativa_produto;
                     exception when others then
                        -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
                        raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_625(inter_tr_tmrp_621(1))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               end if;
            end if;

            --raise_application_error(-20000,'Entrando na procedure... ' || :new.qtde_areceber_programada);

            if :new.nivel_produto = '1'
            then
               begin
                  select nvl(count(1),0)
                  into v_nro_reg_tmrp_625
                  from tmrp_625
                  where  tmrp_625.ordem_planejamento         = :new.ordem_planejamento
                    and  tmrp_625.pedido_venda               = :new.pedido_venda
                    and  tmrp_625.pedido_reserva             = :new.pedido_reserva
                    and  tmrp_625.seq_produto_origem         = 0
                    and  tmrp_625.nivel_produto_origem       = :new.nivel_produto
                    and  tmrp_625.grupo_produto_origem       = :new.grupo_produto
                    and  tmrp_625.subgrupo_produto_origem    = :new.subgrupo_produto
                    and  tmrp_625.item_produto_origem        = :new.item_produto
                    and  tmrp_625.alternativa_produto_origem = :new.alternativa_produto
                    and tmrp_625.nivel_produto               = '2';
               end;

               if v_nro_reg_tmrp_625 > 0
               then
                  begin
                     insert into tmrp_615 (
                        nome_programa,                            cgc_cliente9,
                        nr_solicitacao,                           tipo_registro,

                        ordem_planejamento,                       pedido_venda,
                        pedido_reserva,
                        ordem_prod,                               area_producao,
                        nivel,                                    grupo,
                        subgrupo,                                 item,
                        alternativa
                     ) values (
                        'trigger_planejamento',                   ws_sid,
                        888,                                      888,

                        :new.ordem_planejamento,                  :new.pedido_venda,
                        :new.pedido_reserva,
                        0,                                        1,
                        :new.nivel_produto,                       :new.grupo_produto,
                        :new.subgrupo_produto,                    :new.item_produto,
                        :new.alternativa_produto
                     );
                  exception when others then
                     raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               end if;
            end if;

            INTER_PR_NEC_ORDEM_PLANEJ(:new.ordem_planejamento,
                                      :new.pedido_venda,
                                      0,

                                      v_codigo_projeto,
                                      :new.seq_projeto,

                                      :new.nivel_produto,
                                      :new.grupo_produto,
                                      :new.subgrupo_produto,
                                      :new.item_produto,
                                      :new.alternativa_produto,
                                      :new.seq_produto,

                                      :new.qtde_areceber_programada,
                                      :new.qtde_adicional,
                                      v_qtde_vendida,
                                      :new.unidades_reserva_planejada);

            -- Acerto final do calculo para quantidade programada,
            -- executa a quantidade planejada * o consumo do componente.
            begin
               update tmrp_625
                  set tmrp_625.qtde_reserva_programada = 0
               where tmrp_625.ordem_planejamento         = :new.ordem_planejamento
                 and tmrp_625.pedido_venda               = :new.pedido_venda
                 and tmrp_625.pedido_reserva             = :new.pedido_reserva;
               exception when others then
                  -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_625(inter_tr_tmrp_621(3))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;

            -- Acerto final do calculo para quantidade programada,
            -- executa a quantidade planejada * o consumo do componente.
            begin
               update tmrp_625
                  set tmrp_625.qtde_reserva_programada = 0
               where tmrp_625.ordem_planejamento       = :new.ordem_planejamento
                 and tmrp_625.pedido_venda             = :new.pedido_venda
                 and tmrp_625.pedido_reserva           = :new.pedido_reserva;
               exception when others then
                  -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_625(inter_tr_tmrp_621(4))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;

            for reg_tmrp625 in (select tmrp_625.nivel_produto,         tmrp_625.grupo_produto,
                                       tmrp_625.subgrupo_produto,      tmrp_625.item_produto,
                                       tmrp_625.seq_produto,           tmrp_625.alternativa_produto,
                                       tmrp_625.consumo,               tmrp_625.qtde_areceber_programada
                                from tmrp_625
                                where tmrp_625.ordem_planejamento       = :new.ordem_planejamento
                                  and tmrp_625.pedido_venda             = :new.pedido_venda
                                  and tmrp_625.pedido_reserva           = :new.pedido_reserva
                                  and tmrp_625.qtde_areceber_programada > 0)
            loop
      /*         if :new.area_producao = 1
               then
                  begin
                     select pcpc_020.codigo_risco
                     into p_codigo_risco
                     from pcpc_020
                     where pcpc_020.ordem_producao = :new.ordem_prod_compra;
                     exception when no_data_found
                     then p_codigo_risco := 0;
                  end;
               end if;*/

               inter_pr_update_reserva_plan(:new.ordem_planejamento,
                                            :new.pedido_venda,
                                            :new.pedido_reserva,
                                            reg_tmrp625.nivel_produto,
                                            reg_tmrp625.grupo_produto,
                                            reg_tmrp625.subgrupo_produto,
                                            reg_tmrp625.item_produto,
                                            reg_tmrp625.alternativa_produto,
                                            reg_tmrp625.qtde_areceber_programada,
                                            reg_tmrp625.seq_produto,
                                            0);
            end loop;
         end if;
      end;
   end if;
end inter_tr_tmrp_621;

-- ALTER TRIGGER "INTER_TR_TMRP_621" ENABLE
 

/

exec inter_pr_recompile;

