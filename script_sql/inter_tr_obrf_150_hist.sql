
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_150_HIST" 
   after insert or delete or update
       of  id,                       cod_empresa,
           cod_usuario,              chave_acesso,
           modelo_documento,         serie,
           dest_cnpj9,               dest_cnpj4,
           dest_cnpj2,               numero,
           nr_recibo,                nr_protocolo,
           justificativa,            nr_final_inut,
           cod_status,               msg_status,
           usuario,                  versao_systextilnfe
   on obrf_150
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      begin
         insert into obrf_150_log (
            tipo_ocorr,                            data_ocorr,
            hora_ocorr,                            usuario_rede,
            maquina_rede,                          aplicacao,
            usuario_sistema,

            id_old,                                id_new,
            cod_empresa_old,                       cod_empresa_new,
            cod_usuario_old,                       cod_usuario_new,
            chave_acesso_old,                      chave_acesso_new,
            modelo_documento_old,                  modelo_documento_new,
            serie_old,                             serie_new,
            dest_cnpj9_old,                        dest_cnpj9_new,
            dest_cnpj4_old,                        dest_cnpj4_new,
            dest_cnpj2_old,                        dest_cnpj2_new,
            numero_old,                            numero_new,
            nr_recibo_old,                         nr_recibo_new,
            nr_protocolo_old,                      nr_protocolo_new,
            justificativa_old,                     justificativa_new,
            nr_final_inut_old,                     nr_final_inut_new,
            cod_status_old,                        cod_status_new,
            msg_status_old,                        msg_status_new,
            usuario_old,                           usuario_new,
            versao_systextilnfe_old,               versao_systextilnfe_new
         ) values (
            'I',                                   sysdate,
            sysdate,                               ws_usuario_rede,
            ws_maquina_rede,                       ws_aplicativo,
            ws_usuario_systextil,

            0,                                     :new.id,
            0,                                     :new.cod_empresa,
            0,                                     :new.cod_usuario,
            '',                                    :new.chave_acesso,
            '',                                    :new.modelo_documento,
            0,                                     :new.serie,
            0,                                     :new.dest_cnpj9,
            0,                                     :new.dest_cnpj4,
            0,                                     :new.dest_cnpj2,
            0,                                     :new.numero,
            '',                                    :new.nr_recibo,
            '',                                    :new.nr_protocolo,
            '',                                    :new.justificativa,
            '',                                    :new.nr_final_inut,
            '',                                    :new.cod_status,
            '',                                    :new.msg_status,
            '',                                    :new.usuario,
            '',                                    :new.versao_systextilnfe
         );
      end;
   end if;

   if updating
   then
      begin
         insert into obrf_150_log (
            tipo_ocorr,                            data_ocorr,
            hora_ocorr,                            usuario_rede,
            maquina_rede,                          aplicacao,
            usuario_sistema,

            id_old,                                id_new,
            cod_empresa_old,                       cod_empresa_new,
            cod_usuario_old,                       cod_usuario_new,
            chave_acesso_old,                      chave_acesso_new,
            modelo_documento_old,                  modelo_documento_new,
            serie_old,                             serie_new,
            dest_cnpj9_old,                        dest_cnpj9_new,
            dest_cnpj4_old,                        dest_cnpj4_new,
            dest_cnpj2_old,                        dest_cnpj2_new,
            numero_old,                            numero_new,
            nr_recibo_old,                         nr_recibo_new,
            nr_protocolo_old,                      nr_protocolo_new,
            justificativa_old,                     justificativa_new,
            nr_final_inut_old,                     nr_final_inut_new,
            cod_status_old,                        cod_status_new,
            msg_status_old,                        msg_status_new,
            usuario_old,                           usuario_new,
            versao_systextilnfe_old,               versao_systextilnfe_new
         ) values (
            'U',                                   sysdate,
            sysdate,                               ws_usuario_rede,
            ws_maquina_rede,                       ws_aplicativo,
            ws_usuario_systextil,

            :old.id,                               :new.id,
            :old.cod_empresa,                      :new.cod_empresa,
            :old.cod_usuario,                      :new.cod_usuario,
            :old.chave_acesso,                     :new.chave_acesso,
            :old.modelo_documento,                 :new.modelo_documento,
            :old.serie,                            :new.serie,
            :old.dest_cnpj9,                       :new.dest_cnpj9,
            :old.dest_cnpj4,                       :new.dest_cnpj4,
            :old.dest_cnpj2,                       :new.dest_cnpj2,
            :old.numero,                           :new.numero,
            :old.nr_recibo,                        :new.nr_recibo,
            :old.nr_protocolo,                     :new.nr_protocolo,
            :old.justificativa,                    :new.justificativa,
            :old.nr_final_inut,                    :new.nr_final_inut,
            :old.cod_status,                       :new.cod_status,
            :old.msg_status,                       :new.msg_status,
            :old.usuario,                          :new.usuario,
            :old.versao_systextilnfe,              :new.versao_systextilnfe
         );
      end;
   end if;

   if deleting
   then
      begin
         insert into obrf_150_log (
            tipo_ocorr,                            data_ocorr,
            hora_ocorr,                            usuario_rede,
            maquina_rede,                          aplicacao,
            usuario_sistema,

            id_old,                                id_new,
            cod_empresa_old,                       cod_empresa_new,
            cod_usuario_old,                       cod_usuario_new,
            chave_acesso_old,                      chave_acesso_new,
            modelo_documento_old,                  modelo_documento_new,
            serie_old,                             serie_new,
            dest_cnpj9_old,                        dest_cnpj9_new,
            dest_cnpj4_old,                        dest_cnpj4_new,
            dest_cnpj2_old,                        dest_cnpj2_new,
            numero_old,                            numero_new,
            nr_recibo_old,                         nr_recibo_new,
            nr_protocolo_old,                      nr_protocolo_new,
            justificativa_old,                     justificativa_new,
            nr_final_inut_old,                     nr_final_inut_new,
            cod_status_old,                        cod_status_new,
            msg_status_old,                        msg_status_new,
            usuario_old,                           usuario_new,
            versao_systextilnfe_old,               versao_systextilnfe_new
         ) values (
            'D',                                   sysdate,
            sysdate,                               ws_usuario_rede,
            ws_maquina_rede,                       ws_aplicativo,
            ws_usuario_systextil,

            :old.id,                               0,
            :old.cod_empresa,                      0,
            :old.cod_usuario,                      0,
            :old.chave_acesso,                     '',
            :old.modelo_documento,                 '',
            :old.serie,                            0,
            :old.dest_cnpj9,                       0,
            :old.dest_cnpj4,                       0,
            :old.dest_cnpj2,                       0,
            :old.numero,                           0,
            :old.nr_recibo,                        '',
            :old.nr_protocolo,                     '',
            :old.justificativa,                    '',
            :old.nr_final_inut,                    '',
            :old.cod_status,                       '',
            :old.msg_status,                       '',
            :old.usuario,                          '',
            :old.versao_systextilnfe,              0
         );
      end;
   end if;

end inter_tr_obrf_150_hist;

-- ALTER TRIGGER "INTER_TR_OBRF_150_HIST" ENABLE
 

/

exec inter_pr_recompile;

