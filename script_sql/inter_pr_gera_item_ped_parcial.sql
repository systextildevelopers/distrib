create or replace procedure "INTER_PR_GERA_ITEM_PED_PARCIAL"
                            (p_cod_empresa IN NUMBER,
                            p_pedido_compra IN NUMBER,
                            p_seq_item_pedido IN NUMBER,
                            p_item_nivel IN VARCHAR2,
                            p_item_grupo IN VARCHAR2,
                            p_item_subgru IN VARCHAR2,
                            p_item_item IN VARCHAR2,
                            p_qtde_requisitada IN NUMBER,
                            p_preco_item IN NUMBER,
                            p_perc_ipi IN NUMBER,
                            p_data_prev_entr IN DATE,
                            p_cod_contabil IN NUMBER,
                            p_cod_transacao IN NUMBER,
                            p_num_requis IN NUMBER,
                            p_seq_item_req IN NUMBER,
                            p_qtde_pedida IN NUMBER,
                            p_cgc9 IN NUMBER,
                            p_cgc4 IN NUMBER,
                            p_cgc2 IN NUMBER,
                            p_ccusto IN NUMBER,
                            p_valor_conv IN NUMBER)
is

v_seq_item_pedido number;
v_tmp_int number;
v_un_medida varchar2(2);
v_un_medida_aux varchar2(2);
v_projeto number;
v_subprojeto number;
v_servico number;
v_data_prev_entr date;
v_observacao_item_req varchar(2000);
v_conta_previsao number;
v_periodo_compras number;
v_perc_iva number;
v_perc_igv number;
v_class_fiscal varchar2(15);
v_consiste_empresa_compras varchar2(1);
v_unidade_conv varchar2(3);
v_fator_conv number;
v_cod_aplic number;
v_seq_ped number;
v_desc_item varchar2(1000);
v_cod_deposito number;
v_tem_critica number;

begin

    v_tmp_int := 0;
    v_un_medida := '';
    v_un_medida_aux := '';
    v_projeto := 0;
    v_subprojeto := 0;
    v_servico := 0;
    v_observacao_item_req := '';
    v_conta_previsao := 0;
    v_periodo_compras := 0;
    v_perc_iva := 0;
    v_perc_igv := 0;
    v_class_fiscal := '';
    v_unidade_conv := '';
    v_fator_conv := 0;
    v_cod_aplic := 0;
    v_cod_deposito := 0;
    v_tem_critica := 0;

    select flag_insert_item
    into v_tem_critica
    from supr_002
    where num_requisicao = p_num_requis
      and seq_item_req = p_seq_item_req;

    if v_tem_critica = 0
    then
        begin
            select count(1) 
            into v_tmp_int
            from supr_100 
            where supr_100.num_ped_compra = p_pedido_compra
              and supr_100.num_requisicao = p_num_requis
              and supr_100.seq_item_req = p_seq_item_req;
        exception when no_data_found then
            v_tmp_int := 0;
        end;
        
        if v_tmp_int = 0
        then
            begin
                select basi_030.unidade_medida, basi_030.conta_estoque
                into v_un_medida, v_conta_previsao
                from basi_030
                where basi_030.nivel_estrutura = p_item_nivel
                    and basi_030.referencia = p_item_grupo;
            exception when no_data_found then
                v_un_medida := '';
                v_conta_previsao := 0;
            end;

            begin
                select supr_067.data_prev_entr, supr_067.projeto,
                    supr_067.subprojeto, supr_067.servico
                into v_data_prev_entr, v_projeto,
                    v_subprojeto, v_servico
                from supr_067
                where supr_067.num_requisicao = p_num_requis
                    and supr_067.seq_item_req = p_seq_item_req
                    and supr_067.data_prev_entr > sysdate - 1;
            exception when no_data_found then
                v_data_prev_entr := p_data_prev_entr;
            end;
            
            begin
                select supr_067.unidade_medida
                into v_un_medida_aux
                from supr_067
                where supr_067.num_requisicao = p_num_requis
                and supr_067.seq_item_req = p_seq_item_req;
            exception when no_data_found then
                v_un_medida_aux := '';
            end;

            if v_un_medida is null or v_un_medida = ''
            then
                v_un_medida := v_un_medida_aux;
            end if;

            begin
                select supr_067.observacao_item, supr_067.descricao_item
                into v_observacao_item_req, v_desc_item
                from supr_067 
                where supr_067.num_requisicao = p_num_requis
                    and supr_067.seq_item_req = p_seq_item_req;
            exception when no_data_found then
                v_observacao_item_req := '';
                v_desc_item := '';
            end;
            
            if v_observacao_item_req is null
            then
                v_observacao_item_req := '';
            end if;
            
            begin
                select fatu_503.consiste_empresa_compras
                into v_consiste_empresa_compras
                from fatu_503 
                where fatu_503.codigo_empresa = p_cod_empresa;
            exception when no_data_found then
                v_consiste_empresa_compras := 'N';
            end; 
            select max(pcpc_010.periodo_producao) 
            into v_periodo_compras
            from pcpc_010
            where (pcpc_010.codigo_empresa = p_cod_empresa or v_consiste_empresa_compras = 'N')
                and pcpc_010.area_periodo = 9
                and pcpc_010.situacao_periodo < 3
                and pcpc_010.data_ini_periodo <= v_data_prev_entr
                and pcpc_010.data_fim_periodo >= v_data_prev_entr;
            
            if v_periodo_compras is null
            then v_periodo_compras := 0;
            end if;

            begin
                select basi_010.classific_fiscal
                into v_class_fiscal
                from basi_010
                where basi_010.nivel_estrutura = p_item_nivel
                    and basi_010.grupo_estrutura = p_item_grupo
                    and basi_010.subgru_estrutura = p_item_subgru
                    and basi_010.item_estrutura = p_item_item;
            exception when no_data_found then
                v_class_fiscal := '';
            end;
            begin
                select basi_240.perc_igv 
                into v_perc_igv
                from basi_240
                where basi_240.classific_fiscal = v_class_fiscal;
            exception when no_data_found then
                v_perc_iva := 0;
            end;

            if v_perc_igv > 0
            then
                v_perc_iva := v_perc_igv;
            else v_perc_iva := 0;
            end if;

            begin
                select supr_065.codigo_deposito
                into v_cod_deposito
                from supr_065
                where supr_065.num_requisicao = p_num_requis;
            exception when no_data_found then
                v_cod_deposito := 0;
            end;
            
            begin
                select basi_015.aplicacao
                into v_cod_aplic
                from basi_015 
                where basi_015.codigo_empresa = p_cod_empresa
                    and basi_015.nivel_estrutura = p_item_nivel
                    and basi_015.grupo_estrutura = p_item_grupo
                    and basi_015.subgru_estrutura = p_item_subgru
                    and basi_015.item_estrutura = p_item_item
                    and basi_015.codigo_deposito = v_cod_deposito;
            exception when no_data_found then
                v_cod_aplic := 0;
            end;
            
            begin
                select supr_060.unid_conv
                into v_unidade_conv
                from supr_060
                where supr_060.item_060_nivel99 = p_item_nivel
                  and supr_060.item_060_grupo = p_item_grupo
                  and supr_060.item_060_subgrupo = p_item_subgru
                  and supr_060.item_060_item = p_item_item
                  and supr_060.forn_060_forne9 = p_cgc9
                  and supr_060.forn_060_forne4 = p_cgc4
                  and supr_060.forn_060_forne2 = p_cgc2
                  and supr_060.fator_conv > 0.000000;
            exception when no_data_found then
                v_unidade_conv := '';
            end;
            
            begin
                insert into supr_100
                (num_ped_compra, seq_item_pedido,
                item_100_nivel99, item_100_grupo,
                item_100_subgrupo, item_100_item,
                descricao_item, unidade_medida,
                qtde_pedida_item, qtde_saldo_item,
                preco_item_comp, percentual_desc,
                percentual_ipi, outras_despesas,
                centro_custo, data_prev_entr,
                num_requisicao, seq_item_req,
                situacao_item, codigo_deposito,
                codigo_contabil, projeto,
                subprojeto, servico,
                periodo_compras, observacao_item,
                perc_iva, unidade_conv, fator_conv, valor_conv, cod_aplicacao, 
                codigo_transacao)
                VALUES
                (p_pedido_compra, p_seq_item_pedido,
                p_item_nivel, p_item_grupo,
                p_item_subgru, p_item_item,
                v_desc_item, v_un_medida,
                p_qtde_pedida, p_qtde_pedida,
                p_preco_item, 0,
                p_perc_ipi, 0,
                p_ccusto, v_data_prev_entr,
                p_num_requis, p_seq_item_req,
                1, v_cod_deposito,
                p_cod_contabil, v_projeto,
                v_subprojeto, v_servico,
                v_periodo_compras, v_observacao_item_req,
                v_perc_iva, v_unidade_conv, v_fator_conv, p_valor_conv, v_cod_aplic,
                p_cod_transacao );
            exception when others then
                raise_application_error(-20001, SQLERRM);
            end;

            commit;
        end if;
    end if;
end INTER_PR_GERA_ITEM_PED_PARCIAL;
