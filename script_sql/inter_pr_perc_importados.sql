CREATE OR REPLACE PROCEDURE "INTER_PR_PERC_IMPORTADOS" (p_cnpj9        in number,
                                                     p_cnpj4        in number,
                                                     p_cnpj2        in number,
                                                     p_nat_oper     in number,
                                                     p_estado       in varchar2,
                                                     p_data_emissao in date,
                                                     p_cod_empresa  in number,
                                                     p_nivel        in varchar2,
                                                     p_grupo        in varchar2,
                                                     p_subgrupo     in varchar2,
                                                     p_item         in varchar2,
                                                     p_perc_icms    out number,
                                                     p_flag_icms    out varchar2,
                                                     p_procedencia  out number,
                                                     p_flag_proc    out varchar2,
                                                     p_mensagem     out varchar2,
                                                     p_flag_msg     out varchar2,
                                                     p_numero_fci   out varchar2,
                                                     p_flag_nfci    out varchar2) is

   v_isento        pedi_010.insc_est_cliente%TYPE;
   v_perc_icms     pedi_080.perc_icms%TYPE;
   v_estado_emp    basi_160.estado%TYPE;
   v_origem        obrf_650.origem%TYPE;
   v_numero_fci    obrf_650.numero_fci%TYPE;
   v_icms_interno  obrf_650.icms_interno%TYPE;
   v_icms_intesta  obrf_650.icms_intesta%TYPE;
   v_vlr_med_vend  obrf_650.vlr_med_vend%TYPE;
   v_valor_par_imp obrf_650.valor_parcela_imp%TYPE;
   v_participacao  obrf_650.participacao%TYPE;
   v_min_part      obrf_650.min_part%TYPE;
   v_imprime_msg   obrf_158.imprime_msg_importados%TYPE;
   v_troca_icms    varchar2(1);
   v_not_found     varchar2(1);
   v_periodo_max   date;
   v_mostra_fci    varchar2(1);
   v_beneficioEstadual pedi_016.conteudo%type;

/*PROCEDURE INTERNA PARA RETORNAR OS DADOS DA OBRF_650*/
procedure p_busca_dados_obrf_650(p_data_emissao  in date,
                                 p_cod_emp_int   in number,
                                 p_nivel         in varchar2,
                                 p_grupo         in varchar2,
                                 p_subgrupo      in varchar2,
                                 p_item          in varchar2,
                                 p_origem        in out number,
                                 p_numero_fci    in out varchar2,
                                 p_icms_interno  in out number,
                                 p_icms_intesta  in out number,
                                 p_vlr_med_vend  in out number,
                                 P_valor_par_imp in out number,
                                 p_participacao  in out number,
                                 p_min_part      in out number,
                                 p_not_found     in out varchar2
) is

begin

   begin
      select obrf_650.origem,       nvl(upper(obrf_650.numero_fci), ' '),
             obrf_650.icms_interno, obrf_650.icms_intesta,
             obrf_650.vlr_med_vend, obrf_650.participacao,
             obrf_650.min_part,     obrf_650.valor_parcela_imp
      into   p_origem,              p_numero_fci,
             p_icms_interno,        p_icms_intesta,
             p_vlr_med_vend,        p_participacao,
             p_min_part,            P_valor_par_imp
      from obrf_650
      where to_date('01'||trim(to_char(obrf_650.mes,'00'))||trim(to_char(obrf_650.ano,'0000')), 'DDMMYYYY') = p_data_emissao
        and obrf_650.cod_empresa   = p_cod_emp_int
        and obrf_650.nivel         = p_nivel
        and obrf_650.grupo         = p_grupo
        and obrf_650.subgrupo      = p_subgrupo
        and obrf_650.item          = p_item;
      exception
         when others then
            p_not_found     := 'n';
            p_origem        := 0;
            p_numero_fci    := '';
            p_icms_interno  := 0.00;
            p_icms_intesta  := 0.00;
            p_vlr_med_vend  := 0.00;
            P_valor_par_imp := 0.00;
            p_participacao  := 0.00;
            p_min_part      := 0.00;

   end;

   if sql%found
   then p_not_found := 's';
   else p_not_found := 'n';
   end if;

end;
/*FIM- Procedure interna*/

begin

   v_not_found  := 'n';
   v_troca_icms := 's';
   p_flag_icms  := 'n';
   p_flag_proc  := 'n';
   p_flag_msg   := 'n';
   p_flag_nfci  := 'n';
   p_mensagem   := '';

   begin
      select basi_160.estado
      into v_estado_emp
      from basi_160, fatu_500
      where basi_160.cod_cidade     = fatu_500.codigo_cidade
        and fatu_500.codigo_empresa = p_cod_empresa;
      exception
         when others then
            v_estado_emp := '';
   end;

   begin
      select pedi_010.insc_est_cliente
      into   v_isento
      from pedi_010
      where pedi_010.cgc_9 = p_cnpj9
        and pedi_010.cgc_4 = p_cnpj4
        and pedi_010.cgc_2 = p_cnpj2;
      exception
         when others then
            v_troca_icms := 'n';
   end;

   begin
      select lower(conteudo) into v_beneficioEstadual from pedi_016 p
      where p.cnpj9 = p_cnpj9
        and p.cnpj4 = p_cnpj4
        and p.cnpj2 = p_cnpj2
        and p.atributo = 'FISCAL.beneficioEstadual';
    exception
         when others then
            v_beneficioEstadual := 'n';
   end;

   if v_estado_emp = 'EX'
   then v_troca_icms := 'n';
   end if;

   if v_troca_icms = 's'
   then

      begin
         select pedi_080.perc_icms
         into v_perc_icms
         from pedi_080
         where pedi_080.natur_operacao = p_nat_oper
           and pedi_080.estado_natoper = p_estado;
         exception
            when others then
               v_troca_icms := 'n';
      end;
      if v_perc_icms = 0.00
      then v_troca_icms := 'n';
      end if;
   end if;

   begin
      select max(to_date('01'||trim(to_char(obrf_650.mes,'00'))||trim(to_char(obrf_650.ano,'0000')), 'DDMMYYYY'))
      into   v_periodo_max
      from obrf_650
      where to_date('01'||trim(to_char(obrf_650.mes,'00'))||trim(to_char(obrf_650.ano,'0000')), 'DDMMYYYY') <= p_data_emissao
        and obrf_650.cod_empresa = p_cod_empresa
        and obrf_650.nivel       = p_nivel
        and obrf_650.grupo       = p_grupo
        and (obrf_650.subgrupo   = p_subgrupo or obrf_650.subgrupo = 'XXX')
        and (obrf_650.item       = p_item     or obrf_650.item     = 'XXXXXX')
        and (trim(obrf_650.numero_fci) is not null or (obrf_650.considera_calculo_fci is not null and obrf_650.considera_calculo_fci = 1));
      exception
         when others then
            v_periodo_max := to_date(null, '');

   end;

   if v_periodo_max is not null
   then

      p_busca_dados_obrf_650(v_periodo_max,
                             p_cod_empresa,
                             p_nivel,
                             p_grupo,
                             'XXX',
                             'XXXXXX',
                             v_origem,
                             v_numero_fci,
                             v_icms_interno,
                             v_icms_intesta,
                             v_vlr_med_vend,
                             v_valor_par_imp,
                             v_participacao,
                             v_min_part,
                             v_not_found);

      if v_not_found = 'n'
      then
         p_busca_dados_obrf_650(v_periodo_max,
                                p_cod_empresa,
                                p_nivel,
                                p_grupo,
                                'XXX',
                                p_item,
                                v_origem,
                                v_numero_fci,
                                v_icms_interno,
                                v_icms_intesta,
                                v_vlr_med_vend,
                                v_valor_par_imp,
                                v_participacao,
                                v_min_part,
                                v_not_found);

         if v_not_found = 'n'
         then
            p_busca_dados_obrf_650(v_periodo_max,
                                   p_cod_empresa,
                                   p_nivel,
                                   p_grupo,
                                   p_subgrupo,
                                   'XXXXXX',
                                   v_origem,
                                   v_numero_fci,
                                   v_icms_interno,
                                   v_icms_intesta,
                                   v_vlr_med_vend,
                                   v_valor_par_imp,
                                   v_participacao,
                                   v_min_part,
                                   v_not_found);

            if v_not_found = 'n'
            then
               p_busca_dados_obrf_650(v_periodo_max,
                                      p_cod_empresa,
                                      p_nivel,
                                      p_grupo,
                                      p_subgrupo,
                                      p_item,
                                      v_origem,
                                      v_numero_fci,
                                      v_icms_interno,
                                      v_icms_intesta,
                                      v_vlr_med_vend,
                                      v_valor_par_imp,
                                      v_participacao,
                                      v_min_part,
                                      v_not_found);
            end if;
         end if;
      end if;

   else

      begin
         select max(to_date('01'||trim(to_char(obrf_650.mes,'00'))||trim(to_char(obrf_650.ano,'0000')), 'DDMMYYYY'))
         into   v_periodo_max
         from obrf_650
         where to_date('01'||trim(to_char(obrf_650.mes,'00'))||trim(to_char(obrf_650.ano,'0000')), 'DDMMYYYY') <= p_data_emissao
           and obrf_650.cod_empresa = 0
           and obrf_650.nivel       = p_nivel
           and obrf_650.grupo       = p_grupo
           and (obrf_650.subgrupo   = p_subgrupo or obrf_650.subgrupo = 'XXX')
           and (obrf_650.item       = p_item     or obrf_650.item     = 'XXXXXX')
           and (trim(obrf_650.numero_fci) is not null or (obrf_650.considera_calculo_fci is not null and obrf_650.considera_calculo_fci = 1));
         exception
            when others then
               v_periodo_max := to_date(null, '');
      end;

      if v_periodo_max is not null
      then

         p_busca_dados_obrf_650(v_periodo_max,
                                0,
                                p_nivel,
                                p_grupo,
                                'XXX',
                                'XXXXXX',
                                v_origem,
                                v_numero_fci,
                                v_icms_interno,
                                v_icms_intesta,
                                v_vlr_med_vend,
                                v_valor_par_imp,
                                v_participacao,
                                v_min_part,
                                v_not_found);

         if v_not_found = 'n'
         then
            p_busca_dados_obrf_650(v_periodo_max,
                                   0,
                                   p_nivel,
                                   p_grupo,
                                   'XXX',
                                   p_item,
                                   v_origem,
                                   v_numero_fci,
                                   v_icms_interno,
                                   v_icms_intesta,
                                   v_vlr_med_vend,
                                   v_valor_par_imp,
                                   v_participacao,
                                   v_min_part,
                                   v_not_found);

            if v_not_found = 'n'
            then
               p_busca_dados_obrf_650(v_periodo_max,
                                      0,
                                      p_nivel,
                                      p_grupo,
                                      p_subgrupo,
                                      'XXXXXX',
                                      v_origem,
                                      v_numero_fci,
                                      v_icms_interno,
                                      v_icms_intesta,
                                      v_vlr_med_vend,
                                      v_valor_par_imp,
                                      v_participacao,
                                      v_min_part,
                                      v_not_found);

               if v_not_found = 'n'
               then
                  p_busca_dados_obrf_650(v_periodo_max,
                                         0,
                                         p_nivel,
                                         p_grupo,
                                         p_subgrupo,
                                         p_item,
                                         v_origem,
                                         v_numero_fci,
                                         v_icms_interno,
                                         v_icms_intesta,
                                         v_vlr_med_vend,
                                         v_valor_par_imp,
                                         v_participacao,
                                         v_min_part,
                                         v_not_found);
               end if;
            end if;
         end if;
      end if;
   end if;

   if v_not_found = 's'
   then

      if v_participacao >= v_min_part
      then

         if v_troca_icms = 's'
         then

            p_flag_icms := 's';

            if p_estado = v_estado_emp
            then
               if v_beneficioEstadual = 's'
               then
                  p_perc_icms := v_perc_icms;
               else
                   p_perc_icms := v_icms_interno;
               end if;
            else 
               if v_origem = 5 then
                  p_perc_icms := v_perc_icms;
               else
                  p_perc_icms := v_icms_intesta;
               end if;
            end if;

         end if;
      end if;

      if v_participacao > 0.00
      then

         if p_estado <> 'EX'
         then
            p_procedencia := v_origem;
         else
            p_procedencia := 0;
         end if;

         p_flag_proc   := 's';
         v_imprime_msg := 'S';

         p_numero_fci  := v_numero_fci;
         

         begin
            select obrf_158.imprime_msg_importados
            into   v_imprime_msg
            from obrf_158
            where obrf_158.cod_empresa = p_cod_empresa;
            exception
               when others then
                  v_imprime_msg := 'S';
         end;

         v_mostra_fci := 'N';

         if  v_imprime_msg = 'S'
         then
            v_mostra_fci := 'S';
         end if;
         
         if p_numero_fci is not null then 
         
           if  v_imprime_msg = 'I' and (p_estado <> v_estado_emp and p_estado <> 'EX')
           then
              v_mostra_fci := 'S';
           end if;

           if  v_imprime_msg = 'M' and p_estado <> 'EX'
           then
              v_mostra_fci := 'S';
           end if;
         
         end if;
         
         if  v_mostra_fci = 'S'
         then
            p_flag_msg  := 's';
            p_flag_nfci := 's';

            p_mensagem   := 'Resolucao do Senado Federal 13/12, FCI N? ' || v_numero_fci;
         end if;
      end if;
   end if;

end inter_pr_perc_importados;
/

exec inter_pr_recompile;
/
