
create or replace trigger "INTER_TR_SUPR_100_LOG"
   after insert or delete or update
   of num_ped_compra,      seq_item_pedido,      item_100_nivel99,      item_100_grupo,
      item_100_subgrupo,   item_100_item,        descricao_item,        unidade_medida,
      qtde_pedida_item,    qtde_saldo_item,      preco_item_comp,       percentual_desc,
      percentual_ipi,      outras_despesas,      centro_custo,          codigo_deposito,
      data_prev_entr,      num_requisicao,       seq_item_req,          cod_cancelamento,
      dt_cancelamento,     situacao_item,        numero_coleta,         codigo_contabil,
      perc_enc_finan,      projeto,              subprojeto,            servico,
      perc_iva,            periodo_compras,      observacao_item,       unidade_conv,
      fator_conv,          valor_conv,           percentual_subs
   on supr_100
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   long_aux                  varchar2(2000);
   v_executa_trigger         number;

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting
      then
         long_aux := '';

         long_aux := long_aux           ||
         inter_fn_buscar_tag('lb34990#ITENS DO PEDIDO DE COMPRA',ws_locale_usuario,ws_usuario_systextil) ||
               chr(10)                  ||
               chr(10);

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb05455#SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.seq_item_pedido
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.item_100_nivel99  ||'.'||
                                                   :new.item_100_grupo    ||'.'||
                                                   :new.item_100_subgrupo ||'.'||
                                                   :new.item_100_item
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb30896#DESCRICAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.descricao_item
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.situacao_item
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb09007#UNIDADE MEDIDA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.unidade_medida
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb15127#QTDE PEDIDA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.qtde_pedida_item
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb05451#SALDO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.qtde_saldo_item
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb11173#VALOR UNITARIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.preco_item_comp
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb01253#% DESCONTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.percentual_desc
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb03929#%IPI:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.percentual_ipi
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00271#OUTRAS DESPESAS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.outras_despesas
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb28752#CENTRO CUSTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.centro_custo
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb02613#CODIGO DEPOSITO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.codigo_deposito
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00617#DATA PREV. ENTREGA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.data_prev_entr
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb34991#REQUISICAO COMPRA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.num_requisicao ||'/'||
                                                   :new.seq_item_req
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb02582#CODIGO CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.cod_cancelamento
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb31283#DATA CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.dt_cancelamento
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb09674#NR. COLETA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.numero_coleta
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb34724#COD. CONTABIL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.codigo_contabil
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb03875#% ENCARGOS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.perc_enc_finan
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb11019#PROJETO / SUBPROJETO / SERVICO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.projeto    ||'/'||
                                                   :new.subprojeto ||'/'||
                                                   :new.servico
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb34992#PERCENTUAL IVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.perc_iva
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb11068#PERIODO DE COMPRAS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.periodo_compras
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb28640#UNIDADE DE CONVERSAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.unidade_conv
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb03730#FATOR CONVERSAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.fator_conv
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb28882#VALOR CONVERSAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.valor_conv
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00259#OBSERVACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.observacao_item
                                                || chr(10);
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb03919#% SUBSTIT. TRIBUTÁRIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :new.percentual_subs;

         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        aplicacao,
              usuario_rede,      maquina_rede,
              num01,             long01
              )
         VALUES
           ( 'SUPR_100',        'I',
              sysdate,           ws_aplicativo,
              ws_usuario_rede,   ws_maquina_rede,
              :new.num_ped_compra,long_aux
                 );
      end if;

      if updating
      then
         long_aux := '';

         long_aux := long_aux           ||
         inter_fn_buscar_tag('lb34990#ITENS DO PEDIDO DE COMPRA',ws_locale_usuario,ws_usuario_systextil) ||
               chr(10)                  ||
               chr(10);


		long_aux := long_aux ||
		inter_fn_buscar_tag('lb05455#SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
											   || :old.seq_item_pedido ||'->'||
												  :new.seq_item_pedido
											   || chr(10);

         if :old.item_100_nivel99  <> :new.item_100_nivel99  or
            :old.item_100_grupo    <> :new.item_100_grupo    or
            :old.item_100_subgrupo <> :new.item_100_subgrupo or
            :old.item_100_item     <> :new.item_100_item
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)
                                                || :old.item_100_nivel99  ||'.'||
                                                   :old.item_100_grupo    ||'.'||
                                                   :old.item_100_subgrupo ||'.'||
                                                   :old.item_100_item
                                                ||'->'||
                                                   :new.item_100_nivel99  ||'.'||
                                                   :new.item_100_grupo    ||'.'||
                                                   :new.item_100_subgrupo ||'.'||
                                                   :new.item_100_item
                                                || chr(10);

         end if;

         if :old.descricao_item <> :new.descricao_item
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb30896#DESCRICAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.descricao_item ||'->'||
                                                      :new.descricao_item
                                                   || chr(10);
         end if;

         if :old.situacao_item <> :new.situacao_item
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.situacao_item ||'->'||
                                                      :new.situacao_item
                                                   || chr(10);
         end if;

         if :old.unidade_medida <> :new.unidade_medida
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb09007#UNIDADE MEDIDA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.unidade_medida ||'->'||
                                                      :new.unidade_medida
                                                   || chr(10);
         end if;

         if :old.qtde_pedida_item <> :new.qtde_pedida_item
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb15127#QTDE PEDIDA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.qtde_pedida_item ||'->'||
                                                      :new.qtde_pedida_item
                                                   || chr(10);
         end if;

         if :old.qtde_saldo_item <> :new.qtde_saldo_item
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb05451#SALDO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.qtde_saldo_item ||'->'||
                                                      :new.qtde_saldo_item
                                                   || chr(10);
         end if;

         if :old.preco_item_comp <> :new.preco_item_comp
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb11173#VALOR UNITARIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.preco_item_comp ||'->'||
                                                      :new.preco_item_comp
                                                   || chr(10);
         end if;

         if :old.percentual_desc <> :new.percentual_desc
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb01253#% DESCONTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.percentual_desc ||'->'||
                                                      :new.percentual_desc
                                                   || chr(10);
         end if;

         if :old.percentual_ipi <> :new.percentual_ipi
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb03929#%IPI:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.percentual_ipi ||'->'||
                                                      :new.percentual_ipi
                                                   || chr(10);
         end if;

         if :old.outras_despesas <> :new.outras_despesas
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb00271#OUTRAS DESPESAS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.outras_despesas ||'->'||
                                                      :new.outras_despesas
                                                   || chr(10);
         end if;

         if :old.centro_custo <> :new.centro_custo
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb28752#CENTRO CUSTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.centro_custo ||'->'||
                                                      :new.centro_custo
                                                   || chr(10);
         end if;

         if :old.codigo_deposito <> :new.codigo_deposito
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb02613#CODIGO DEPOSITO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.codigo_deposito ||'->'||
                                                      :new.codigo_deposito
                                                   || chr(10);
         end if;

         if :old.data_prev_entr <> :new.data_prev_entr
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb00617#DATA PREV. ENTREGA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.data_prev_entr ||'->'||
                                                      :new.data_prev_entr
                                                   || chr(10);
         end if;

         if :old.num_requisicao <> :new.num_requisicao or
            :old.seq_item_req   <> :new.seq_item_req
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb34991#REQUISICAO COMPRA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.num_requisicao ||'/'||
                                                      :old.seq_item_req   ||'->'||
                                                      :new.num_requisicao ||'/'||
                                                      :new.seq_item_req
                                                   || chr(10);
         end if;

         if :old.cod_cancelamento <> :new.cod_cancelamento
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb02582#CODIGO CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.cod_cancelamento ||'->'||
                                                      :new.cod_cancelamento
                                                   || chr(10);
         end if;

         if :old.dt_cancelamento <> :new.dt_cancelamento
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb31283#DATA CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.dt_cancelamento ||'->'||
                                                      :new.dt_cancelamento
                                                   || chr(10);
         end if;

         if :old.numero_coleta <> :new.numero_coleta
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb09674#NR. COLETA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.numero_coleta ||'->'||
                                                      :new.numero_coleta
                                                   || chr(10);
         end if;

         if :old.codigo_contabil <> :new.codigo_contabil
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb34724#COD. CONTABIL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.codigo_contabil ||'->'||
                                                      :new.codigo_contabil
                                                   || chr(10);
         end if;

         if :old.perc_enc_finan <> :new.perc_enc_finan
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb03875#% ENCARGOS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.perc_enc_finan ||'->'||
                                                      :new.perc_enc_finan
                                                   || chr(10);
         end if;

         if :old.projeto    <> :new.projeto    or
            :old.subprojeto <> :new.subprojeto or
            :old.servico    <> :new.servico
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb11019#PROJETO / SUBPROJETO / SERVICO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.projeto    ||'/'||
                                                      :old.subprojeto ||'/'||
                                                      :old.servico    ||'->'||
                                                      :new.projeto    ||'/'||
                                                      :new.subprojeto ||'/'||
                                                      :new.servico
                                                   || chr(10);
         end if;

         if :old.perc_iva <> :new.perc_iva
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb34992#PERCENTUAL IVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.perc_iva ||'->'||
                                                      :new.perc_iva
                                                   || chr(10);
         end if;

         if :old.periodo_compras <> :new.periodo_compras
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb11068#PERIODO DE COMPRAS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.periodo_compras ||'->'||
                                                      :new.periodo_compras
                                                   || chr(10);
         end if;

         if :old.unidade_conv  <> :new.unidade_conv
         or :old.fator_conv    <> :new.fator_conv
         or :old.valor_conv    <> :new.valor_conv
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb28640#UNIDADE DE CONVERSAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.unidade_conv  ||'->'||
                                                      :new.unidade_conv
                                                   || chr(10);
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb03730#FATOR CONVERSAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.fator_conv  ||'->'||
                                                      :new.fator_conv
                                                   || chr(10);
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb28882#VALOR CONVERSAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.valor_conv  ||'->'||
                                                      :new.valor_conv
                                                   || chr(10);
         end if;

         if :old.observacao_item <> :new.observacao_item
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb00259#OBSERVACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.observacao_item ||'->'||
                                                      :new.observacao_item
                                                   || chr(10);
         end if;

         if :old.percentual_subs <> :new.percentual_subs
         then
            long_aux := long_aux ||
            inter_fn_buscar_tag('lb03919#% SUBSTIT. TRIBUTÁRIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                   || :old.percentual_subs ||'->'||
                                                      :new.percentual_subs;
         end if;

         -- Verifica se foi alterada alguma informacao
         if long_aux <> 'ITENS DO PEDIDO DE COMPRA'|| chr(10) || chr(10)
         then

            INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        aplicacao,
              usuario_rede,      maquina_rede,
              num01,             long01
              )
            VALUES
            ( 'SUPR_100',        'A',
               sysdate,           ws_aplicativo,
               ws_usuario_rede,   ws_maquina_rede,
               :new.num_ped_compra,long_aux
            );
         end if;
      end if;

      if deleting
      then
         long_aux := inter_fn_buscar_tag('lb05455#SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                     || :old.seq_item_pedido;

         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        aplicacao,
              usuario_rede,      maquina_rede,
              num01,             long01
              )
         VALUES
           ( 'SUPR_100',        'D',
              sysdate,           ws_aplicativo,
              ws_usuario_rede,   ws_maquina_rede,
              :old.num_ped_compra,long_aux
              );
      end if;
   end if;
end inter_tr_supr_100_log;

-- ALTER TRIGGER "INTER_TR_SUPR_100_LOG" ENABLE
 

/

exec inter_pr_recompile;

