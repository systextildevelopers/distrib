
  CREATE OR REPLACE PROCEDURE "INTER_PR_CONSISTE_ESTOQUE" 
is

   cursor estq040 is
      select DISTINCT estq_040.cditem_nivel99,    estq_040.cditem_grupo,
                      estq_040.cditem_subgrupo,   estq_040.cditem_item,
                      estq_040.deposito,          basi_205.tipo_volume,
                      basi_205.controla_ficha_cardex
      from estq_040, basi_205
      where estq_040.deposito              = basi_205.codigo_deposito
      and  (basi_205.tipo_volume           > 0
      or    basi_205.controla_ficha_cardex = 1);

   -- campo retornados pela procedure INTER_PR_PROCURA_DADOS_CARDEX, que
   -- retorna os saldos dos produtos no periodo fechado.
   --- O unico campo utilizado por esta rotina e' o campo v_saldo_fisico
   v_saldo_fisico           estq_300.saldo_fisico%type;
   v_saldo_financeiro       estq_300.saldo_financeiro%type;
   v_preco_medio_unit       estq_300.preco_medio_unitario%type;
   v_saldo_financeiro_proj  estq_300.saldo_financeiro_proj%type;
   v_preco_medio_unit_proj  estq_300.preco_medio_unitario_proj%type;
   v_saldo_financeiro_est   estq_300.saldo_financeiro_estimado%type;
   v_preco_medio_unit_est   estq_300.preco_medio_unit_estimado%type;

   v_saldo_fisico_quilo     estq_300.saldo_fisico_quilo%type;

   -- saldo das tabelas de cardex (estq_300/estq_310)
   v_saldo_cardex           estq_300.quantidade%type;

   -- saldo das tabelas de saldos (estq_040)
   v_saldo_estq_040         estq_040.qtde_estoque_atu%type;

   -- saldo das tabelas de volumes
   v_saldo_volumes          number;

   -- controla as 3 tentativas para verificar se realmente ha diferenca
   -- ou se apenas algum usuario estava atualizando o estoque no momento
   v_passagem               number;

   v_periodo_estoque        date;

begin

   -- le a data inicial
   select periodo_estoque
   into   v_periodo_estoque
   from   empr_001;

   -- elimina registros antigos
   begin
      delete estq_320;
   end;

   --???????????????????????
   --???????????????????????
   --???????????????????????
   --???????????????????????
   --???????????????????????
   commit;

   for reg_estq040 in estq040
   loop
      -- declara que e' a primeira passagem do produto/deposito
      v_passagem := 1;

      while (v_passagem <= 3)
      loop
         -- le o saldo do produto/deposito (estq_040)
         begin
            select nvl(sum(estq_040.qtde_estoque_atu), 0)
            into   v_saldo_estq_040
            from estq_040
            where estq_040.cditem_nivel99  = reg_estq040.cditem_nivel99
              and estq_040.cditem_grupo    = reg_estq040.cditem_grupo
              and estq_040.cditem_subgrupo = reg_estq040.cditem_subgrupo
              and estq_040.cditem_item     = reg_estq040.cditem_item
              and estq_040.deposito        = reg_estq040.deposito;
         end;

         -- seta que o saldo dos volumes e' igual ao saldo do estq_040,
         -- pois se nao houver volumes a calcular, o processo nao
         -- encontrar diferenca. Se houver o sistema verificara qual
         -- o saldo dos volumes
         v_saldo_volumes := v_saldo_estq_040;

         -- verifica saldo dos volumes de tecidos
         if reg_estq040.tipo_volume in (2,4)
         then
            begin
               select nvl(sum(pcpt_020.qtde_quilos_acab), 0)
               into   v_saldo_volumes
               from pcpt_020
               where pcpt_020.panoacab_nivel99  = reg_estq040.cditem_nivel99
                 and pcpt_020.panoacab_grupo    = reg_estq040.cditem_grupo
                 and pcpt_020.panoacab_subgrupo = reg_estq040.cditem_subgrupo
                 and pcpt_020.panoacab_item     = reg_estq040.cditem_item
                 and pcpt_020.codigo_deposito   = reg_estq040.deposito
                 and pcpt_020.rolo_estoque      not in (0,2);
            end;
         end if;

         -- verifica saldo dos volumes de fios
         if reg_estq040.tipo_volume = 7
         then
            begin
               select nvl(sum(estq_060.peso_liquido), 0)
               into   v_saldo_volumes
               from estq_060
               where estq_060.prodcai_nivel99  = reg_estq040.cditem_nivel99
                 and estq_060.prodcai_grupo    = reg_estq040.cditem_grupo
                 and estq_060.prodcai_subgrupo = reg_estq040.cditem_subgrupo
                 and estq_060.prodcai_item     = reg_estq040.cditem_item
                 and estq_060.codigo_deposito  = reg_estq040.deposito
                 and estq_060.status_caixa not in (4,8,9);
            end;
         end if;

         -- verifica saldo dos volumes de algodao
         if reg_estq040.tipo_volume = 9
         then
            begin
               select nvl(sum(decode(pcpf_060.peso_real, 0, pcpf_060.peso_medio, pcpf_060.peso_real)), 0)
               into   v_saldo_volumes
               from pcpf_060
               where pcpf_060.fardo_nivel99  = reg_estq040.cditem_nivel99
                 and pcpf_060.fardo_grupo    = reg_estq040.cditem_grupo
                 and pcpf_060.fardo_subgrupo = reg_estq040.cditem_subgrupo
                 and pcpf_060.fardo_item     = reg_estq040.cditem_item
                 and pcpf_060.deposito       = reg_estq040.deposito
                 and pcpf_060.status_fardo   not in (2,5);
            end;
         end if;

         -- se o deposito controlar ficha cardex, o sistema procurara o
         -- saldo do mesmo
         if reg_estq040.controla_ficha_cardex = 1
         then

            inter_pr_procura_dados_cardex(reg_estq040.deposito,
                                          reg_estq040.cditem_nivel99,
                                          reg_estq040.cditem_grupo,
                                          reg_estq040.cditem_subgrupo,
                                          reg_estq040.cditem_item,
                                          v_periodo_estoque,
                                          v_saldo_fisico,
                                          v_saldo_financeiro,
                                          v_preco_medio_unit,
                                          v_saldo_financeiro_proj,
                                          v_preco_medio_unit_proj,
                                          v_saldo_financeiro_est,
                                          v_preco_medio_unit_est,
                                          v_saldo_fisico_quilo);
            begin
               select nvl(sum(decode(estq_300.entrada_saida, 'E', estq_300.quantidade, estq_300.quantidade * (-1.0))), 0)
               into   v_saldo_cardex
               from estq_300
               where estq_300.codigo_deposito    = reg_estq040.deposito
                 and estq_300.nivel_estrutura    = reg_estq040.cditem_nivel99
                 and estq_300.grupo_estrutura    = reg_estq040.cditem_grupo
                 and estq_300.subgrupo_estrutura = reg_estq040.cditem_subgrupo
                 and estq_300.item_estrutura     = reg_estq040.cditem_item;
            end;

            v_saldo_cardex := v_saldo_cardex + v_saldo_fisico;

         else

            -- caso nao o deposito nao controle a ficha cardex, o saldo da cardex
            -- sera a mesma do saldo de estq_040, para nao haver diferenca
            v_saldo_cardex := v_saldo_estq_040;
         end if;

         -- se houver diferenca
         if v_saldo_estq_040 <> v_saldo_volumes
         or v_saldo_estq_040 <> v_saldo_cardex
         or v_saldo_volumes  <> v_saldo_cardex
         then

            -- se ja for a 3o. passagem, e' porque realmente ha diferenca de saldos
            if v_passagem = 3
            then
               -- insere diferenca na tabela de diferencas de estoque
               begin
                  insert into estq_320 (
                     data_movto,                  hora_movto,
                     nivel,                       grupo,
                     subgrupo,                    item,
                     deposito,                    qtde_estoque,
                     qtde_volumes,                qtde_cardex
                  ) values (
                     trunc(sysdate, 'DD'),        sysdate,
                     reg_estq040.cditem_nivel99,  reg_estq040.cditem_grupo,
                     reg_estq040.cditem_subgrupo, reg_estq040.cditem_item,
                     reg_estq040.deposito,        v_saldo_estq_040,
                     v_saldo_volumes,             v_saldo_cardex
                  );
               end;

   --???????????????????????
   --???????????????????????
   --???????????????????????
   --???????????????????????
   --???????????????????????

               commit;
            end if; -- if passagem = 3

            v_passagem := v_passagem + 1;

         else

            -- se nao houver diferenca, sai do laco
            v_passagem := 99;
         end if;    -- se houver diferenca
      end loop;     -- fim do loop das 3 tentativas
   end loop;        -- fim do loop da leitura do estq_040

end inter_pr_consiste_estoque;

 

/

exec inter_pr_recompile;

