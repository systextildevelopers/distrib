create or replace PROCEDURE INTER_PR_GERA_LANC_AUTO(
   p_empresa in number,             p_titulo in number,         p_seq_duplicata number,
   p_parcela in varchar2,           p_tipo_titulo in number,    p_cnpj9 in number,
   p_cnpj4 in number,               p_cnpj2 in number,          p_data_cred in date,
   p_des_erro in out varchar2,      p_tp_cta_deb in number,     p_cod_contab_deb in number,
   p_transacao in out number,       p_c_custo in number,        p_valor_adiantamento in number,
   p_tp_cta_cre in number,          p_cod_contab_cre in number, p_documento in number,
   p_historico in  number,          p_conta_corrente in number, p_portador_dupl in number,
   p_data_ctb_crec in date,         p_data_credito in date,     p_data_emissao in date,
   p_transacao_credito in number,   p_num_lcto_ctb in out number, p_usuario in varchar2,
   p_programa in varchar2,          p_compl_histor1 in varchar2, p_tipo_movimentacao in varchar2)
is
    v_exercicio                    cont_500.exercicio%type;
    v_cod_plano_cta                cont_500.cod_plano_cta%type;
    v_compl_histor1                cont_600.compl_histor1%type;
    v_gera_contabil                fatu_500.gera_contabil%type;

    v_des_erro                     varchar2(1000);
    v_executou_processo            boolean;
    v_teve_cpag_019                boolean;
    v_ativa_passiva                number;
    v_empresa_matriz number;
    v_exercicio_doc number;
    v_cta_debito number;
    v_valor_cred number;
    v_cta_credito number;
    v_valor_deb number;
    v_compl_cliente varchar2(4000);
    v_estorno number;

    v_agrupa_lanc_cont_chave varchar2(1);
    v_nome_cli varchar2(4000);
    v_cta_juros number;
    v_cta_deb number;
    v_hst_juros varchar2(4000);
    v_hist_desp_cobr varchar2(4000);
    v_cta_desp_cobr number;
    v_cta_cobr_custas number;
    v_seq_pag number;
    v_valor_desp_cobr number;
    v_ind_cred_deb varchar2(4000);
    v_valida_conta number;
    v_historico number;

    v_cta_debito_old number;
    v_cta_credito_old number;
    
begin
    begin
        select  fatu_500.gera_contabil,     fatu_500.codigo_matriz
        into    v_gera_contabil, v_empresa_matriz
        from fatu_500
        where fatu_500.codigo_empresa = p_empresa;
    exception when no_data_found then
        v_empresa_matriz := 0;
        
    end;

    v_executou_processo := FALSE;

    begin
        v_exercicio_doc := inter_fn_checa_data(v_empresa_matriz, trunc(p_data_cred), 0); --data_transacao
        v_exercicio := inter_fn_checa_data(v_empresa_matriz, trunc(p_data_cred),0); --data_transacao

        if v_exercicio_doc < 0
        then
            v_exercicio_doc := v_exercicio;
        end if;
        
        if v_exercicio <= 0
        then
            p_des_erro := 'Exercício e/ou período contábil não encontrado ou fechado.
                            Não será permitido realizar movimentações nesta data. Contate o contador. '
                            ||' transação: '|| p_transacao
                            ||' Exercicio: '|| v_exercicio
                            ||' Exercicio doc: '|| v_exercicio_doc;
            return;
        end if;

        v_cta_debito_old := INTER_FN_ENCONTRA_CONTA(    p_empresa,
                                                    p_tp_cta_deb,
                                                    p_cod_contab_deb,
                                                    p_transacao,
                                                    p_c_custo,
                                                    v_exercicio,
                                                    v_exercicio_doc);

        if v_cta_debito < 0 then
            p_des_erro := 'Conta contabil de débito não encontrada. ' ||
                        'Esta transação não será confirmada. Contate o contador. ' ||
                        'Tipo contábil: ' || p_tp_cta_deb 
                        || ' , Transação: ' || p_transacao || ' ' ||
                        'Código contábil: ' || p_cod_contab_deb;
            return;
        end if;

        v_cta_credito_old := INTER_FN_ENCONTRA_CONTA(   p_empresa,
                                                    p_tp_cta_cre,
                                                    p_cod_contab_cre,
                                                    p_transacao_credito,
                                                    0,
                                                    v_exercicio,
                                                    v_exercicio_doc);
        if v_cta_credito < 0 then
            p_des_erro := 'Conta contabil de crédito não encontrada. ' ||
                        'Esta transação não será confirmada. Contate o contador. ' ||
                        'Tipo contábil: ' || p_tp_cta_cre || ' , Transação: ' || p_transacao_credito || ' ' ||
                        'Código contábil: ' || p_cod_contab_cre;
            return;
        end if;

        if p_tipo_movimentacao = 'D' then
            v_cta_debito := v_cta_debito_old;
            v_cta_credito := v_cta_credito_old;
        else
            v_cta_debito := v_cta_credito_old;
            v_cta_credito := v_cta_debito_old;
        end if;

        if v_gera_contabil = 1 
        and v_cta_debito > 0 
        and v_cta_credito > 0 then
            begin
                select cod_plano_cta
                into v_cod_plano_cta
                from cont_500
                where exercicio = v_exercicio
                and cod_empresa = v_empresa_matriz;
            exception
                when others then v_cod_plano_cta := 0;
            end;

            v_compl_histor1 := p_documento;

            --centro custo -> passa 0
            --documento -> passa 0
            --seq_pagamento -> passa 0
            --cod_imposto -> passa 0
            --projeto -> passa 0
            --subprojeto -> passa 0
            --servico -> passa 0
            v_compl_cliente := '';

            --v_cta_credito invertido com a debito porque a capri utiliza assim.
            v_historico := 0;
            v_historico := p_historico;
                       
            INTER_PR_GERA_LANC_CONT_PRG(p_empresa,              --p_cod_empresa IN NUMBER,
                                        9 ,                     --p_origem IN NUMBER,
                                        p_num_lcto_ctb ,        --p_num_lanc IN OUT NUMBER,
                                        p_c_custo ,             --p_centro_custo  IN NUMBER,
                                        p_data_ctb_crec ,       --p_data_lancto IN DATE,
                                        v_historico ,           --p_hist_contabil IN OUT NUMBER,
                                        p_compl_histor1 ,       --p_compl_histor1 IN VARCHAR2,
                                        1 ,                     --p_estorno IN NUMBER,
                                        p_transacao ,           --p_transacao IN NUMBER,
                                        v_cta_debito ,         --p_conta_debito IN NUMBER,
                                        p_valor_adiantamento ,  --p_valor_debito IN NUMBER,
                                        v_cta_credito ,          --p_conta_credito IN NUMBER,
                                        p_valor_adiantamento ,  --p_valor_credito IN NUMBER,
                                        p_portador_dupl ,       --p_banco_func IN NUMBER,
                                        p_conta_corrente ,      --p_conta_func IN NUMBER,
                                        p_data_ctb_crec ,       --p_data_func IN DATE,
                                        p_titulo,               --p_docto_func IN NUMBER,
                                        p_cnpj9 ,               --p_cnpj9 IN NUMBER,
                                        p_cnpj4,                --p_cnpj4 IN NUMBER,
                                        p_cnpj2 ,               --p_cnpj2 IN NUMBER,
                                        0,                      --p_tipo_cnpj IN NUMBER,
                                        p_titulo,               --p_num_documento IN NUMBER,
                                        p_parcela,              --p_parcela_serie IN VARCHAR2,
                                        p_tipo_titulo,          --p_tipo_titulo IN NUMBER,
                                        0,                      --p_seq_pagamento IN NUMBER,
                                        0 ,                     --p_cod_imposto IN NUMBER,
                                        0 ,                     --p_projeto IN NUMBER,
                                        0 ,                     --p_subprojeto IN NUMBER,
                                        0 ,                     --p_servico IN NUMBER,
                                        p_programa,
                                        p_usuario,
                                        v_des_erro);            --Retorna Mensagem de erro

            if v_des_erro is not null then
                raise_application_error(-20001, v_des_erro);
            end if;
        end if;

        commit;
    EXCEPTION WHEN OTHERS THEN
        rollback;
        p_des_erro := 'Ocorreu um durante o processo de gerar contabilização: '||SQLERRM;
    END;
EXCEPTION WHEN OTHERS
then
    rollback;
    p_des_erro := 'Ocorreu um durante o processo de gerar contabilização: '||SQLERRM;
end inter_pr_gera_lanc_auto;
