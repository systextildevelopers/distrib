insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('ftec_e051', 'Ficha Técnica de Retilíneas', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'ftec_e051', 'ftec_menu', 1, 1, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'ftec_e051', 'ftec_menu', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Ficha Técnica de Retilíneas'
 where hdoc_036.codigo_programa = 'ftec_e051'
   and hdoc_036.locale          = 'es_ES';
commit;
