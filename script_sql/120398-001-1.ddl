declare

aux number(1);

begin
  for empresa in (select * from fatu_500)
  loop
    begin
      insert into fatu_510 (codigo_empresa) values (empresa.codigo_empresa);
    exception
    when DUP_VAL_ON_INDEX then     
         aux := 1;
    end;
  end loop;
  commit;
end;
