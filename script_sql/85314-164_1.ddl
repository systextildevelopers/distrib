-- Substitui LONG RAW por BLOB, pois � muito mais eficiente.
DROP TABLE OPER_005;
CREATE TABLE OPER_005 (
PRINTER_NAME VARCHAR2(35) NOT NULL,
CONTENT BLOB
);
COMMENT ON TABLE OPER_005 IS 'Fila para impress�o de etiquetas a partir da nuvem';
