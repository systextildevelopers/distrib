
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_843" 
   after insert
      or delete
      or update of codigo_projeto, combinacao_item
on basi_843
for each row

declare
   v_total_28    number(9);
   v_total_27    number(9);
   v_total_29    number(9);

   v_nivel_produto_n   basi_001.nivel_produto%type;
   v_grupo_produto_n    basi_001.grupo_produto%type;

   v_nivel_produto_o   basi_001.nivel_produto%type;
   v_grupo_produto_o   basi_001.grupo_produto%type;
   v_ins_perda         number;
begin
   --Quando for iserido um novo componente na estrutura do desenvolvimento de produtos
   --sera validado o componente e sera inserido na tabela de situacao do componente*/
   if inserting
   then
      begin
         select basi_001.nivel_produto,    basi_001.grupo_produto
         into v_nivel_produto_n,           v_grupo_produto_n
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when OTHERS then
         raise_application_error (-20000, 'Projeto nao cadastrado. (inter_tr_basi_843)');
      end;

      begin
         INSERT INTO basi_027 (
            nivel_item,                   grupo_item,
            subgrupo_item,                item_item,
            alt_item,

            nivel_componente,             grupo_componente,
            subgrupo_componente,          item_componente,

            codigo_projeto,               sequencia_projeto,
            cnpj_cliente9,                cnpj_cliente4,
            cnpj_cliente2
         ) VALUES (
            v_nivel_produto_n,            v_grupo_produto_n,
            '000',                        :new.combinacao_item,
            0,

            '0',                          '00000',
            '000',                        '000000',

            :new.codigo_projeto,          1,
            0,                            0,
            0);
      exception when OTHERS then
         raise_application_error (-20000, 'Nao incluiu na tabela (BASI_027-inter_tr_basi_843)');
      end;

      select count(1)
      into v_total_28
      from basi_028
      where basi_028.nivel_componente  = v_nivel_produto_n
        and basi_028.grupo_componente  = v_grupo_produto_n
        and basi_028.item_componente   = :new.combinacao_item
        and basi_028.tipo_aprovacao    = 2;   --APROVACAO POR COMBINACAO;

      if v_total_28 = 0
      then
         begin
            INSERT INTO basi_028(
              nivel_componente,                        grupo_componente,
              subgrupo_componente,                     item_componente,

              situacao_componente,
              codigo_projeto,                          sequencia_projeto,
              tipo_aprovacao
            ) VALUES (
               v_nivel_produto_n,                      v_grupo_produto_n,
               '000',                                  :new.combinacao_item,

               0,
               :new.codigo_projeto,                    1,
               2
            );
         exception when OTHERS then
            raise_application_error (-20000, 'Nao incluiu na tabela (BASI_028-inter_tr_basi_843)');
         end;
      end if;

      /*
         Primeiro procura e insere perdas para o cliente especifico.
      */
      v_ins_perda := 0;

      for reg_basi_041 in  (select basi_041.seq_limite,
                                   basi_041.limite_inferior,
                                   basi_041.limite_superior,
                                   basi_041.qtde_perda,
                                   basi_041.perc_perda
                            from basi_041, basi_001
                            where basi_001.codigo_projeto = :new.codigo_projeto
                              and basi_001.cnpj_cliente9  = basi_041.cnpj_cliente9
                              and basi_001.cnpj_cliente4  = basi_041.cnpj_cliente4
                              and basi_001.cnpj_cliente2  = basi_041.cnpj_cliente2)
      loop
         v_ins_perda := 1;

         begin
            insert into basi_029 (
               codigo_projeto,                item_produto,
               seq_limite,                    limite_inferior,
               limite_superior,               qtde_perda,
               perc_perda
	          ) values (
               :new.codigo_projeto,           :new.combinacao_item,
               reg_basi_041.seq_limite,       reg_basi_041.limite_inferior,
               reg_basi_041.limite_superior,  reg_basi_041.qtde_perda,
               reg_basi_041.perc_perda
	          );
         exception when OTHERS then
            v_ins_perda := 99;
            raise_application_error (-20000, 'Nao incluiu na tabela (BASI_029-inter_tr_basi_843)');
         end;
      end loop;

      /*
         Depois procura para o cliente todos
      */
      if v_ins_perda = 0
      then
         for reg_basi_041 in  (select basi_041.seq_limite,
                                      basi_041.limite_inferior,
                                      basi_041.limite_superior,
                                      basi_041.qtde_perda,
                                      basi_041.perc_perda
                               from basi_041
                               where basi_041.cnpj_cliente9 = 999999999
                                 and basi_041.cnpj_cliente4 = 9999
                                 and basi_041.cnpj_cliente2 = 99)
         loop
            v_ins_perda := 1;

            select nvl(count(*),0)
            into v_total_29
            from basi_029
            where basi_029.codigo_projeto = :new.codigo_projeto
              and basi_029.item_produto   = :new.combinacao_item
              and basi_029.seq_limite     = reg_basi_041.seq_limite;

            if v_total_29 = 0
            then
               begin
                  insert into basi_029 (
                     codigo_projeto,                item_produto,
                     seq_limite,                    limite_inferior,
                     limite_superior,               qtde_perda,
                     perc_perda
      	          ) values (
                     :new.codigo_projeto,           :new.combinacao_item,
                     reg_basi_041.seq_limite,       reg_basi_041.limite_inferior,
                     reg_basi_041.limite_superior,  reg_basi_041.qtde_perda,
                     reg_basi_041.perc_perda
      	          );
               exception when OTHERS then
                  v_ins_perda := 99;
                  raise_application_error (-20000, 'Nao incluiu na tabela (BASI_029-inter_tr_basi_843)');
               end;
            end if;
         end loop;
      end if;

      if v_ins_perda = 0
      then
         begin
            insert into basi_029 (
	                  codigo_projeto,                item_produto,
	                  seq_limite,                    limite_inferior,
	                  limite_superior,               qtde_perda,
	                  perc_perda
	          ) values (
	                  :new.codigo_projeto,           :new.combinacao_item,
	                  1,                             1,
	                  1,                             0,
	                  0
	          );
         exception when OTHERS then
            v_ins_perda := 99;
            raise_application_error (-20000, 'Nao incluiu na tabela (BASI_029-inter_tr_basi_843)');
         end;
      end if;
   end if;

   if deleting
   then
      begin
         select basi_001.nivel_produto,    basi_001.grupo_produto
         into v_nivel_produto_o,           v_grupo_produto_o
         from basi_001
         where basi_001.codigo_projeto = :old.codigo_projeto;
      exception when OTHERS then
         raise_application_error (-20000, 'Projeto nao cadstrado. (inter_tr_basi_843)');
      end;

      select count(1)
      into v_total_27
      from basi_027
      where basi_027.nivel_item          = v_nivel_produto_o
        and basi_027.grupo_item          = v_grupo_produto_o
        and basi_027.item_item           = :old.combinacao_item
        and basi_027.codigo_projeto      = :old.codigo_projeto
        and basi_027.sequencia_projeto   = 1;

      if v_total_27 > 0
      then
         begin
            delete from basi_027
            where basi_027.nivel_item          = v_nivel_produto_o
              and basi_027.grupo_item          = v_grupo_produto_o
              and basi_027.item_item           = :old.combinacao_item
              and basi_027.codigo_projeto      = :old.codigo_projeto
              and basi_027.sequencia_projeto   = 1;
         exception when OTHERS then
            raise_application_error (-20000, 'Nao excluiu da tabela (BASI_027-inter_tr_basi_843)');
         end;
      end if;

      select count(1)
      into v_total_27
      from basi_027
      where basi_027.nivel_item          = v_nivel_produto_o
        and basi_027.grupo_item          = v_grupo_produto_o
        and basi_027.item_item           = :old.combinacao_item
        and basi_027.sequencia_projeto   = 1;

      select count(1)
      into v_total_28
      from basi_028
      where basi_028.nivel_componente = v_nivel_produto_o
        and basi_028.grupo_componente = v_grupo_produto_o
        and basi_028.item_componente  = :old.combinacao_item
        and basi_028.tipo_aprovacao   = 2;   --APROVACAO POR COMBINACAO;

      if v_total_28 > 0 and v_total_27 = 0
      then
         begin
            delete from basi_028
            where basi_028.nivel_componente = v_nivel_produto_o
              and basi_028.grupo_componente = v_grupo_produto_o
              and basi_028.item_componente  = :old.combinacao_item
              and basi_028.tipo_aprovacao   = 2;   --APROVACAO POR COMBINACAO;
         exception when OTHERS then
            raise_application_error (-20000, 'Nao excluiu da tabela (BASI_028-inter_tr_basi_843)');
         end;
      end if;
   end if;
end inter_tr_basi_843;

-- ALTER TRIGGER "INTER_TR_BASI_843" ENABLE
 

/

exec inter_pr_recompile;

