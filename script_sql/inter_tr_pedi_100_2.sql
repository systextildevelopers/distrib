
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_100_2" 
before INSERT or DELETE on pedi_100
for each row

declare
  v_qtde_25   number(01);
  v_qtde_26   number(01);
  v_qtde_40   number(01);
  v_executa_trigger  number(1);
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting
      then
         :new.situacao_venda := 5;
      else
         select count(*)
         into v_qtde_25
         from pedi_135
         where pedido_venda = :old.pedido_venda
            and codigo_situacao = 25;
         if v_qtde_25 > 0
         then
            DELETE  pedi_135
            where pedido_venda = :old.pedido_venda
            and codigo_situacao = 25;
         end if;
         select count(*)
         into v_qtde_26
         from pedi_135
         where pedido_venda = :old.pedido_venda
            and codigo_situacao = 26;
         if v_qtde_26 > 0
         then
            DELETE  pedi_135
            where pedido_venda = :old.pedido_venda
            and codigo_situacao = 26;
         end if;
         select count(*)
         into v_qtde_40
         from pedi_340
         where pedido_venda = :old.pedido_venda;
         if v_qtde_40 > 0
         then
            DELETE  pedi_340
            where pedido_venda = :old.pedido_venda;
         end if;

      end if;

   end if;

end inter_tr_pedi_100_2;
-- ALTER TRIGGER "INTER_TR_PEDI_100_2" ENABLE
 

/

exec inter_pr_recompile;

