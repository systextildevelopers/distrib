alter table cont_016 add (IND_REC_RECEITA number(1) default 1);
comment on column cont_016.IND_REC_RECEITA is 'Criterio de reconhecimento de receitas para empresas tributadas pelo Lucro Presumido: 1.Regime de caixa ou 2.Regime de competÍncia';
exec inter_pr_recompile;
