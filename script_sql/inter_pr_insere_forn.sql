create or replace procedure "INTER_PR_INSERE_FORN" (p_cnpj_for9 IN number, 	        p_cnpj_for4 IN number,           p_cnpj_for2 IN number, --3	   
                                                    p_nome IN varchar2,             p_telefone IN varchar2,          p_celular_forne IN varchar2, --6 
                                                    p_email IN varchar2, 	        p_nfe_email IN varchar2,         p_cep IN varchar2, --9
                                                    p_endereco IN varchar2, 	    p_numero_imovel IN varchar2,     p_complemento IN varchar2, --12 
                                                    p_bairro IN varchar2, 	        p_cidade IN varchar2, 	         p_empresa IN varchar2, --15
                                                    p_cod_fornecedor IN varchar2,   p_tipo_fornecedor IN number,     p_codigo_contabil IN number, --18 
                                                    p_inscr_est_forn IN varchar2,   p_exists_fornecedor OUT boolean, p_mensagem_retorno OUT varchar2 --21
                                                )
is
    exists_fornecedor boolean;
    p_des_erro varchar2(1000);
begin
    p_mensagem_retorno := '';

    declare
        v_exists numeric;
    begin
        select 1
        into v_exists
        from supr_010 
        where fornecedor9 = p_cnpj_for9
        and   fornecedor4 = p_cnpj_for4
        and   fornecedor2 = p_cnpj_for2;
            
        if v_exists = 1 then
            exists_fornecedor := true;
        else
            exists_fornecedor := false;
        END IF;
    EXCEPTION when 
    others then    
        exists_fornecedor := false;
    end;

    p_exists_fornecedor := exists_fornecedor;
    if exists_fornecedor = true then
        p_mensagem_retorno := '';
        return;
    end if;

    begin
        INSERT INTO SUPR_010 (FORNECEDOR9, FORNECEDOR4, FORNECEDOR2,
                            NOME_FORNECEDOR, NOME_FANTASIA, SIT_FORNECEDOR,
                            TELEFONE_FORNE, CEP_FORNECEDOR, ENDERECO_FORNE,
                            INSCR_EST_FORNE,  BAIRRO, COD_CIDADE,
                            DATA_CADASTRO, COD_EMPRESA, TIPO_FORNECEDOR,
                            E_MAIL, CODIGO_CONTABIL, CODIGO_FORNECEDOR,
                            DATA_ATUALIZACAO, CELULAR_FORNECEDOR, NUMERO_IMOVEL,
                            COMPLEMENTO, NFE_E_MAIL)

        VALUES (
            p_cnpj_for9, p_cnpj_for4, p_cnpj_for2,
            p_nome, p_nome, 1,
            p_telefone, p_cep, p_endereco,
            p_inscr_est_forn, p_bairro, p_cidade,
            sysdate, to_number(p_empresa), p_tipo_fornecedor,
            p_email, p_codigo_contabil, p_cod_fornecedor,
            sysdate, p_celular_forne, p_numero_imovel,
            p_complemento, p_email);
    EXCEPTION when
    others then
        p_mensagem_retorno := 'Erro ao inserir fornecedor. CNPJ: ' || p_cnpj_for9 || '/' || p_cnpj_for4 || '-' || p_cnpj_for2 || ' ERRO: ' || SQLERRM;
    end;

end;

/

exec inter_pr_recompile;
