  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_075_LOG" 
   after insert or
         delete or
         update of nr_titul_codempr, nr_titul_cli_dup_cgc_cli9, nr_titul_cli_dup_cgc_cli4, nr_titul_cli_dup_cgc_cli2,
    nr_titul_cod_tit, nr_titul_num_dup, nr_titul_seq_dup, seq_pagamento,
    data_pagamento, valor_pago, historico_pgto, numero_documento,
    valor_juros, valor_descontos, portador, conta_corrente,
    data_credito, docto_pagto, num_contabil, comissao_lancada,
    alinea, pago_adiantamento, nr_contrato_acc, processo_export_acc,
    valor_pago_moeda, executa_trigger, vlr_desconto_moeda, vlr_juros_moeda,
    vlr_var_cambial
   on fatu_075
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   long_aux                  varchar2(2000);

   v_executa_trigger      number;

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      if inserting
      then
         INSERT INTO hist_100
            ( tabela,                         operacao,
              data_ocorr,                     usuario_rede,
              maquina_rede,                   aplicacao,
              num01,                          num02,
              num03,                          num04,
              num05,                          str01,
              num06,                          long01
            )
         VALUES
            ( 'FATU_075',                     'I',
              sysdate,                        ws_usuario_rede,
              ws_maquina_rede,                ws_aplicativo,
              :new.nr_titul_num_dup,          :new.nr_titul_cli_dup_cgc_cli9,
              :new.nr_titul_cli_dup_cgc_cli4, :new.nr_titul_cli_dup_cgc_cli2,
              :new.nr_titul_cod_tit,          :new.nr_titul_seq_dup,
              :new.nr_titul_codempr,
              '                                             ' ||
              inter_fn_buscar_tag('lb34843#BAIXA DE TiTULOS A RECEBER',ws_locale_usuario,ws_usuario_systextil) ||
              chr(10)                                                               ||
              chr(10)                                                               ||
              inter_fn_buscar_tag('lb07207#EMPRESA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.nr_titul_codempr ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb31254#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                               :new.nr_titul_cli_dup_cgc_cli9 || '/' ||
                               :new.nr_titul_cli_dup_cgc_cli4 || '-' ||
                               :new.nr_titul_cli_dup_cgc_cli2 ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb34842#SEQ. DE PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.seq_pagamento ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb02981#DATA PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_pagamento ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb05241#VALOR PAGO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_pago ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb05272#HISTORICO  PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.historico_pgto ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb31928#VALOR JUROS:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_juros ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb07929#VALOR DESCONTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_descontos ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb05205#PORTADOR:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.portador ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb02239#CONTA CORRENTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.conta_corrente ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb02874#DATA CRED:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_credito ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb05332#NR.DOCTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.docto_pagto ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb05343#NUM.CONTABIL:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.num_contabil ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb09108#TIPO COMIS:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.comissao_lancada  ||
              chr(10)                                         ||
              inter_fn_buscar_tag('lb01632#ADIANTAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.pago_adiantamento  ||
              chr(10)                                         ||
              'VALOR VARIACAO CAMBIAL: ' || ' ' || :new.vlr_var_cambial
           );
      end if;

      if updating   and
         (:old.nr_titul_codempr  <> :new.nr_titul_codempr                  or
          :old.nr_titul_cli_dup_cgc_cli9 <> :new.nr_titul_cli_dup_cgc_cli9 or
          :old.nr_titul_cli_dup_cgc_cli4 <> :new.nr_titul_cli_dup_cgc_cli4 or
          :old.nr_titul_cli_dup_cgc_cli2 <> :new.nr_titul_cli_dup_cgc_cli2 or
          :old.seq_pagamento <> :new.seq_pagamento                         or
          :old.data_pagamento <> :new.data_pagamento                       or
          :old.valor_pago <> :new.valor_pago                               or
          :old.historico_pgto <> :new.historico_pgto                       or
          :old.valor_juros <> :new.valor_juros                             or
          :old.valor_descontos <> :new.valor_descontos                     or
          :old.portador <> :new.portador                                   or
          :old.conta_corrente <> :new.conta_corrente                       or
          :old.data_credito <> :new.data_credito                           or
          :old.docto_pagto <> :new.docto_pagto                             or
          :old.num_contabil <> :new.num_contabil                           or
          :old.comissao_lancada <> :new.comissao_lancada                   or
          :old.pago_adiantamento <> :new.pago_adiantamento                 or
          :old.vlr_var_cambial <> :new.vlr_var_cambial)
      then
         long_aux := long_aux ||
             '                                             ' ||
             inter_fn_buscar_tag('lb34843#BAIXA DE TiTULOS A RECEBER',ws_locale_usuario,ws_usuario_systextil) ||
             chr(10)                                     ||
             chr(10);
          if :old.nr_titul_codempr  <> :new.nr_titul_codempr
          then
             long_aux := long_aux  ||
                inter_fn_buscar_tag('lb07207#EMPRESA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                              || :new.nr_titul_codempr          || ' -> '
                              || :new.nr_titul_codempr          ||
                              chr(10);
          end if;

          if :old.nr_titul_cli_dup_cgc_cli9 <> :new.nr_titul_cli_dup_cgc_cli9 or
             :old.nr_titul_cli_dup_cgc_cli4 <> :new.nr_titul_cli_dup_cgc_cli4 or
             :old.nr_titul_cli_dup_cgc_cli2 <> :new.nr_titul_cli_dup_cgc_cli2
          then
              long_aux := long_aux  ||
              inter_fn_buscar_tag('lb31254#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                               || :old.nr_titul_cli_dup_cgc_cli9 || '/' ||
                                  :old.nr_titul_cli_dup_cgc_cli4 || '-' ||
                                  :old.nr_titul_cli_dup_cgc_cli2 || ' -> '
                               || :new.nr_titul_cli_dup_cgc_cli9 || '/' ||
                                  :new.nr_titul_cli_dup_cgc_cli4 || '-' ||
                                  :new.nr_titul_cli_dup_cgc_cli2 ||
                               chr(10);
          end if;

          if :old.seq_pagamento <> :new.seq_pagamento
          then
              long_aux := long_aux    ||
              inter_fn_buscar_tag('lb34842#SEQ. DE PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                      || :old.seq_pagamento      || ' -> '
                                      || :new.seq_pagamento      ||
                                      chr(10);
          end if;

          if :old.data_pagamento <> :new.data_pagamento
          then
              long_aux := long_aux    ||
              inter_fn_buscar_tag('lb02981#DATA PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                      || :old.data_pagamento     || ' -> '
                                      || :new.data_pagamento     ||
                                      chr(10);
          end if;

          if :old.valor_pago <> :new.valor_pago
          then
              long_aux := long_aux    ||
              inter_fn_buscar_tag('lb05241#VALOR PAGO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                      || :old.valor_pago         || ' -> '
                                      || :new.valor_pago         ||
                                      chr(10);
          end if;

          if :old.historico_pgto <> :new.historico_pgto
          then
              long_aux := long_aux    ||
              inter_fn_buscar_tag('lb05272#HISTORICO  PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                      || :old.historico_pgto     || ' -> '
                                      || :new.historico_pgto     ||
                                      chr(10);
          end if;

          if :old.valor_juros <> :new.valor_juros
          then
              long_aux := long_aux    ||
              inter_fn_buscar_tag('lb31928#VALOR JUROS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                      || :old.valor_juros        || ' -> '
                                      || :new.valor_juros        ||
                                      chr(10);
          end if;

          if :old.valor_descontos <> :new.valor_descontos
          then
              long_aux := long_aux    ||
              inter_fn_buscar_tag('lb07929#VALOR DESCONTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                      || :old.valor_descontos    || ' -> '
                                      || :new.valor_descontos    ||
                                      chr(10);
          end if;

          if :old.portador <> :new.portador
          then
              long_aux := long_aux    ||
              inter_fn_buscar_tag('lb05205#PORTADOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                      || :old.portador           || ' -> '
                                      || :new.portador           ||
                                      chr(10);
          end if;

          if :old.conta_corrente <> :new.conta_corrente
          then
              long_aux := long_aux    ||
              inter_fn_buscar_tag('lb02239#CONTA CORRENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                      || :old.conta_corrente     || ' -> '
                                      || :new.conta_corrente     ||
                                      chr(10);
          end if;

          if :old.data_credito <> :new.data_credito
          then
              long_aux := long_aux    ||
              inter_fn_buscar_tag('lb02874#DATA CRED:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                      || :old.data_credito       || ' -> '
                                      || :new.data_credito       ||
                                      chr(10);
          end if;

          if :old.docto_pagto <> :new.docto_pagto
          then
              long_aux := long_aux    ||
              inter_fn_buscar_tag('lb05332#NR.DOCTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                      || :old.docto_pagto        || ' -> '
                                      || :new.docto_pagto        ||
                                      chr(10);
          end if;

          if :old.num_contabil <> :new.num_contabil
          then
              long_aux := long_aux    ||
              inter_fn_buscar_tag('lb05343#NUM.CONTABIL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                      || :old.num_contabil       || ' -> '
                                      || :new.num_contabil       ||
                                      chr(10);
          end if;

          if :old.comissao_lancada <> :new.comissao_lancada
          then
              long_aux := long_aux     ||
              inter_fn_buscar_tag('lb09108#TIPO COMIS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.comissao_lancada  || ' -> '
                                       || :new.comissao_lancada  ||
                                       chr(10);
          end if;

          if :old.pago_adiantamento <> :new.pago_adiantamento
          then
              long_aux := long_aux     ||
              inter_fn_buscar_tag('lb01632#ADIANTAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.pago_adiantamento || ' -> '
                                       || :new.pago_adiantamento ||
                                       chr(10);
          end if;
          
          if :old.vlr_var_cambial <> :new.vlr_var_cambial
          then
              long_aux := long_aux     ||
              'VALOR VARIACAO CAMBIAL: '
                                       || :old.vlr_var_cambial || ' -> '
                                       || :new.vlr_var_cambial;
          end if;

         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        usuario_rede,
              maquina_rede,      aplicacao,
              num01,             num02,
              num03,             num04,
              num05,             str01,
              num06,             long01
            )
         VALUES
            ( 'FATU_075',            'A',
              sysdate,               ws_usuario_rede,
              ws_maquina_rede,       ws_aplicativo,
              :new.nr_titul_num_dup,          :new.nr_titul_cli_dup_cgc_cli9,
              :new.nr_titul_cli_dup_cgc_cli4, :new.nr_titul_cli_dup_cgc_cli2,
              :new.nr_titul_cod_tit,          :new.nr_titul_seq_dup,
              :new.nr_titul_codempr,          long_aux
           );

      end if;

      if deleting
      then
         INSERT INTO hist_100
            ( tabela,                         operacao,
              data_ocorr,                     usuario_rede,
              maquina_rede,                   aplicacao,
              num01,                          num02,
              num03,                          num04,
              num05,                          str01,
              num06,                          long01
            )
         VALUES
            ( 'FATU_075',                     'D',
              sysdate,                        ws_usuario_rede,
              ws_maquina_rede,                ws_aplicativo,
              :old.nr_titul_num_dup,          :old.nr_titul_cli_dup_cgc_cli9,
              :old.nr_titul_cli_dup_cgc_cli4, :old.nr_titul_cli_dup_cgc_cli2,
              :old.nr_titul_cod_tit,          :old.nr_titul_seq_dup,
              :old.nr_titul_codempr,
              '                                             ' ||
              inter_fn_buscar_tag('lb34843#BAIXA DE TiTULOS A RECEBER',ws_locale_usuario,ws_usuario_systextil)
           );
      end if;
   end if;
end inter_tr_fatu_075_log;

-- ALTER TRIGGER "INTER_TR_FATU_075_LOG" ENABLE
 
/

exec inter_pr_recompile;

