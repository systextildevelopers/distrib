create table basi_241 (cest varchar2(10) default ' ', ncm  varchar2(10) default ' ', descr_cest varchar2(2000));

alter table basi_241 add constraint pk_basi_241 primary key (cest, ncm);
comment on table basi_241 is 'Cadastro CEST/NCM';
comment on column basi_241.cest is 'Código Especificador da Substituição Tributária';
comment on column basi_241.ncm is ' Nomenclatura Comum do Mercosul (classificação fiscal)';
comment on column basi_241.descr_cest is 'Descrição do CEST';
