create or replace trigger pette01_tr_pedi_010
  after update on pedi_010
  for each row

declare
   -- declarando vari�veis para ser utilizadas em filtro/atualizacao
   v_endereco_entrega_old  pedi_150.end_entr_cobr%type;
   v_endereco_cobranca_old pedi_150.end_entr_cobr%type;

   v_cidade_entrega_old  pedi_150.cid_entr_cobr%type;
   v_cidade_cobranca_old pedi_150.cid_entr_cobr%type;

   v_cep_entrega_old  pedi_150.cep_entr_cobr%type;
   v_cep_cobranca_old pedi_150.cep_entr_cobr%type;

   v_bairro_entrega_old  pedi_150.bairro_entr_cobr%type;
   v_bairro_cobranca_old pedi_150.bairro_entr_cobr%type;

   v_fone_entrega_old  pedi_150.fone_entr_cobr%type;
   v_fone_cobranca_old pedi_150.fone_entr_cobr%type;
   
   v_numero_imovel_entrega_old pedi_150.numero_imovel%type;   
   v_numero_imovel_cobranca_old pedi_150.numero_imovel%type;   

   v_atualiza_cobr     boolean := true;
   v_atualiza_entr     boolean := true;
   
begin
   -- verificando se o endere�o do cliente foi alterado
   if :old.endereco_cliente <> :new.endereco_cliente
   or :old.cod_cidade       <> :new.cod_cidade
   or :old.cep_cliente      <> :new.cep_cliente
   or :old.bairro           <> :new.bairro
   or :old.telefone_cliente <> :new.telefone_cliente
   or :old.numero_imovel    <> :new.numero_imovel
   then

     -- selecionando as informa��es de endere�o de entrega antigo
      begin

         select end_entr_cobr,           cid_entr_cobr,
                cep_entr_cobr,           bairro_entr_cobr,
                fone_entr_cobr,          numero_imovel
         into   v_endereco_entrega_old,  v_cidade_entrega_old,
                v_cep_entrega_old,       v_bairro_entrega_old,
                v_fone_entrega_old,      v_numero_imovel_entrega_old
         from pedi_150
         where cd_cli_cgc_cli9 = :new.cgc_9
           and cd_cli_cgc_cli4 = :new.cgc_4
           and cd_cli_cgc_cli2 = :new.cgc_2
           and seq_endereco    = 1
           and tipo_endereco   = 1;

      exception
         when no_data_found then
            v_atualiza_entr := false;
      end;
                  
      -- selecionando as informa��es de endere�o de cobran�a antigo
      begin
         select end_entr_cobr,            cid_entr_cobr,
                cep_entr_cobr,            bairro_entr_cobr,
                fone_entr_cobr,           numero_imovel
         into   v_endereco_cobranca_old,  v_cidade_cobranca_old,
                v_cep_cobranca_old,       v_bairro_cobranca_old,
                v_fone_cobranca_old,      v_numero_imovel_cobranca_old
         from pedi_150
         where cd_cli_cgc_cli9 = :new.cgc_9
           and cd_cli_cgc_cli4 = :new.cgc_4
           and cd_cli_cgc_cli2 = :new.cgc_2
           and seq_endereco    = 2
           and tipo_endereco   = 2;
      exception
         when no_data_found then
            v_atualiza_cobr := false;
      end;

      -- se o endereco de entrega antido for igual ao endereco antigo do cliente
      -- atualiza com os novos dados de entrega
      if  v_atualiza_entr        = true
      and v_endereco_entrega_old = :old.endereco_cliente
      and v_cidade_entrega_old   = :old.cod_cidade
      and v_cep_entrega_old      = :old.cep_cliente
      and v_bairro_entrega_old   = :old.bairro
      and v_fone_entrega_old     = :old.telefone_cliente
    and v_numero_imovel_entrega_old = :old.numero_imovel
      then
         update pedi_150
         set end_entr_cobr     = :new.endereco_cliente,
             cid_entr_cobr     = :new.cod_cidade,
             cep_entr_cobr     = :new.cep_cliente,
             bairro_entr_cobr  = :new.bairro,
             fone_entr_cobr    = :new.telefone_cliente,
       numero_imovel     = :new.numero_imovel
         where cd_cli_cgc_cli9 = :new.cgc_9
           and cd_cli_cgc_cli4 = :new.cgc_4
           and cd_cli_cgc_cli2 = :new.cgc_2
           and seq_endereco    = 1
           and tipo_endereco   = 1;

      end if;

      -- se o endereco de cobranca antido for igual ao endereco antigo do cliente
      -- atualiza com o novo endereco de cobranca
      if  v_atualiza_cobr         = true
      and v_endereco_cobranca_old = :old.endereco_cliente
      and v_cidade_cobranca_old   = :old.cod_cidade
      and v_cep_cobranca_old      = :old.cep_cliente
      and v_bairro_cobranca_old   = :old.bairro
      and v_fone_cobranca_old     = :old.telefone_cliente
    and v_numero_imovel_cobranca_old = :old.numero_imovel
      then
         update pedi_150
         set end_entr_cobr     = :new.endereco_cliente,
             cid_entr_cobr     = :new.cod_cidade,
             cep_entr_cobr     = :new.cep_cliente,
             bairro_entr_cobr  = :new.bairro,
             fone_entr_cobr    = :new.telefone_cliente,
       numero_imovel     = :new.numero_imovel
         where cd_cli_cgc_cli9 = :new.cgc_9
           and cd_cli_cgc_cli4 = :new.cgc_4
           and cd_cli_cgc_cli2 = :new.cgc_2
           and seq_endereco    = 2
           and tipo_endereco   = 2;

      end if;

   end if;

end pette01_tr_pedi_010;

/

exec inter_pr_recompile;
