create table obrf_partilha(
    ano_base number(5) default 0,
    perc_partilha number(5,2) default 0
);

exec inter_pr_recompile;

INSERT INTO OBRF_PARTILHA (ANO_BASE, PERC_PARTILHA) VALUES (2016, 40);
INSERT INTO OBRF_PARTILHA (ANO_BASE, PERC_PARTILHA) VALUES (2017, 60);
INSERT INTO OBRF_PARTILHA (ANO_BASE, PERC_PARTILHA) VALUES (2018, 80);
INSERT INTO OBRF_PARTILHA (ANO_BASE, PERC_PARTILHA) VALUES (2019, 100);

commit;

exec inter_pr_recompile;

/