INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'pedi_f129',      0,
	0,			     'Cadastro de Exce��es de Bloqueios por condi��o de pagamento');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'pedi_f129',   'NENHUM', 
	0,             1, 
	'S',           'S', 
	'S',           'S');
	
INSERT INTO hdoc_033
(	usu_prg_cdusu,  usu_prg_empr_usu, 
	programa,       nome_menu, 
	item_menu,      ordem_menu, 
	incluir,        modificar, 
	excluir,        procurar)
VALUES
(	'TREINAMENTO',  1, 
	'pedi_f129',    'NENHUM', 
	0,              1, 
	'S',            'S', 
	'S',            'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Cadastro de Exce��es de Bloqueios por condi��o de pagamento'
 WHERE hdoc_036.codigo_programa = 'pedi_f129'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;

commit work;
