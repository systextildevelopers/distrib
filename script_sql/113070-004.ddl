CREATE TABLE OBRF_442 (
	CODIGO            NUMBER(5)   	DEFAULT 0   NOT NULL,
	NOME              VARCHAR2(45)   	DEFAULT ''   NOT NULL,
	DESCRICAO         VARCHAR2(250)   	DEFAULT '',
	script_sql        VARCHAR2(4000)     DEFAULT ''
);

ALTER TABLE OBRF_442 
	ADD CONSTRAINT PK_OBRF_442 PRIMARY KEY (CODIGO, NOME);
	
COMMENT ON TABLE OBRF_442
	IS 'Cadastro de SQL para parametros de apuracao dos impostos ICMS';
   
exec inter_pr_recompile;
