ALTER TABLE PCPC_032
ADD OBSERVACAO_APROV_CORTE VARCHAR2(4000) DEFAULT '';

COMMENT ON COLUMN PCPC_032.OBSERVACAO_APROV_CORTE IS 'Observação informativa utilizada no aproveitamento de corte (pcpc_f216)';

exec inter_pr_recompile;
/
