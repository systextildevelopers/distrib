CREATE OR REPLACE PROCEDURE inter_pr_insere_cpag_010 (
        p_nr_duplicata IN number,          p_parcela IN varchar2,
        p_cgc9 IN number,                  p_cgc4 IN number,
        p_cgc2 IN number,                  p_tipo_titulo IN number,
        p_codigo_empresa IN number,        p_cod_end_cobranca IN number,
        p_emitente_titulo IN varchar2,     p_documento IN number,
        p_serie IN varchar2,               p_data_contrato IN date,
        p_data_vencimento IN date,         p_data_transacao IN date,
        p_codigo_depto IN number,          p_cod_portador IN number,
        p_codigo_historico IN number,      p_codigo_transacao IN number,
        p_posicao_titulo IN number,        p_codigo_contabil IN number,
        p_tipo_pagamento IN number,        p_moeda_titulo IN number,
        p_origem_debito IN varchar2,       p_previsao IN number,
        p_valor_parcela IN number,         p_valor_parcela_moeda IN number, 
        p_base_irrf_moeda IN varchar2,     p_base_irrf IN varchar2,--INICIO IMPOSTO
        p_cod_ret_irrf IN varchar2,        p_aliq_irrf IN varchar2,
        p_valor_irrf_moeda IN number,      p_valor_irrf IN number,
        p_base_iss IN number,              p_cod_ret_iss IN number,
        p_aliq_iss IN number,              p_valor_iss IN number,
        p_base_inss IN number,             p_cod_ret_inss IN number,
        p_aliq_inss IN number,             p_valor_inss_imp IN number,
        p_base_pis IN number,              p_cod_ret_pis IN number,
        p_aliq_pis IN number,              p_valor_pis_imp IN number,
        p_base_cofins IN number,           p_cod_ret_cofins IN number,
        p_aliq_cofins IN number,           p_valor_cofins_imp IN number,
        p_base_csl IN number,              p_cod_ret_csl IN number,
        p_aliq_csl IN number,              p_valor_csl_imp IN number,
        p_base_csrf IN number,             p_cod_ret_csrf IN number,
        p_aliq_csrf IN number,             p_valor_csrf_imp IN number,--FIM IMPOSTO
        p_codigo_barras IN varchar2,       p_projeto IN number,
        p_subprojeto IN number,            p_servico IN number,
        p_nr_processo_export IN number,    p_nr_processo_import IN number,
        p_cod_cancelamento IN number,      p_data_canc_tit IN varchar2,
        p_num_lcmt IN number,              p_usuario_logado IN varchar2,
        
        
        P_CGC9_FAVORECIDO IN NUMBER,
        P_CGC4_FAVORECIDO IN NUMBER,                                                                                                       
        P_CGC2_FAVORECIDO IN NUMBER,                                                                                                       

        P_BANCO_SISPAG    IN NUMBER,                                                                                                       
        P_AGENCIA_SISPAG  IN NUMBER,                                                                                                       
        P_DIG_AGE_SISPAG  IN NUMBER,                                                                                                       
        P_CONTA_SISPAG    IN NUMBER,
        P_DIG_CTA_SISPAG2 IN VARCHAR2,
        
        p_num_importacao IN varchar2,
        p_has_error IN OUT boolean,           
        p_mensagem_retorno OUT varchar2)
is
    v_mensagem_erro varchar2(4000);
    v_des_erro varchar2(4000);
    v_tmp_codigo_transacao  number;
begin
    
    if p_has_error = false then
        INSERT INTO cpag_010 (
            nr_duplicata,       parcela,                cgc_9,              cgc_4,              cgc_2, 
            tipo_titulo,        codigo_empresa,         data_contrato,      data_vencimento,    valor_parcela,
            data_digitacao,     codigo_historico,       codigo_transacao,   emitente_titulo,    data_transacao, 
            origem_debito,      cod_portador,           posicao_titulo,     valor_moeda,        codigo_depto,
            sit_bloqueio,       numero_adiantam,        codigo_contabil,    num_contabil,       usuario_digitacao,
            tipo_pagamento,     data_vencto_original,   moeda_titulo,       cod_end_cobranca,   num_importacao,
            CGC9_FAVORECIDO,    CGC4_FAVORECIDO,        CGC2_FAVORECIDO,    BANCO_SISPAG,                                                                                                       
            AGENCIA_SISPAG,     DIG_AGE_SISPAG_S,         CONTA_SISPAG,       DIG_CTA_SISPAG2
        ) VALUES (
            p_nr_duplicata,     p_parcela,              p_cgc9,             p_cgc4,             p_cgc2, 
            p_tipo_titulo,      p_codigo_empresa,       p_data_contrato,    p_data_vencimento,  p_valor_parcela, 
            p_data_contrato,    p_codigo_historico,     p_codigo_transacao, substr(p_emitente_titulo,1,40),  p_data_contrato, 
            p_origem_debito,    p_cod_portador,         0,                  0,                  p_codigo_depto, 
            0,                  p_nr_duplicata,         p_codigo_contabil,  p_num_lcmt,         p_usuario_logado, 
            0,                  p_data_vencimento,      0,                  0,                  p_num_importacao,
            P_CGC9_FAVORECIDO,  P_CGC4_FAVORECIDO,      P_CGC2_FAVORECIDO,  P_BANCO_SISPAG,                                                                                                       
            P_AGENCIA_SISPAG,   P_DIG_AGE_SISPAG,       P_CONTA_SISPAG,     P_DIG_CTA_SISPAG2
        );
    else
        p_mensagem_retorno := 'Ocorreu um erro ao inserir o titulo(1). ERRO: ' || SQLERRM || p_mensagem_retorno || ' p_nr_duplicata => ' || p_nr_duplicata
                                                                                        || ' p_cgc9 => ' || p_cgc9
                                                                                        || ' p_cgc4 =>' || p_cgc4
                                                                                        || ' p_cgc2 =>' || p_cgc2
                                                                                        || ' p_codigo_empresa =>' || p_codigo_empresa
                                                                                        || ' p_tipo_titulo =>' || p_tipo_titulo
                                                                                        || ' p_data_contrato =>' || p_data_contrato
                                                                                        || ' p_data_vencimento =>' || p_data_vencimento
                                                                                        || ' p_valor_parcela =>' || p_valor_parcela
                                                                                        || ' p_codigo_historico =>' || p_codigo_historico
                                                                                        || ' p_codigo_transacao =>' || p_codigo_transacao
                                                                                        || ' p_emitente_titulo =>' || p_emitente_titulo 
                                                                                        || ' p_data_contrato =>' || p_data_contrato 
                                                                                        || ' p_cod_portador =>' || p_cod_portador 
                                                                                        || ' p_codigo_depto =>' || p_codigo_depto 
                                                                                        || ' p_nr_duplicata =>' || p_nr_duplicata 
                                                                                        || ' p_codigo_contabil =>' || p_codigo_contabil
                                                                                        || ' v_num_lcto =>' || p_num_lcmt
                                                                                        || ' pp_parcela =>' || p_parcela
                                                                                        || ' p_num_importacao =>' || p_num_importacao;
                                                                                    
    end if;
EXCEPTION
when others THEN
    p_mensagem_retorno := 'Ocorreu um inserir o titulo(2). ERRO: ' || SQLERRM || ' p_nr_duplicata => ' || p_nr_duplicata
                                                                        || ' p_cgc9 => ' || p_cgc9
                                                                        || ' p_cgc4 =>' || p_cgc4
                                                                        || ' p_cgc2 =>' || p_cgc2
                                                                        || ' p_codigo_empresa =>' || p_codigo_empresa
                                                                        || ' p_tipo_titulo =>' || p_tipo_titulo
                                                                        || ' p_data_contrato =>' || p_data_contrato
                                                                        || ' p_data_vencimento =>' || p_data_vencimento
                                                                        || ' p_valor_parcela =>' || p_valor_parcela
                                                                        || ' p_codigo_historico =>' || p_codigo_historico
                                                                        || ' p_codigo_transacao =>' || p_codigo_transacao
                                                                        || ' p_emitente_titulo =>' || p_emitente_titulo 
                                                                        || ' p_data_contrato =>' || p_data_contrato 
                                                                        || ' p_cod_portador =>' || p_cod_portador 
                                                                        || ' p_codigo_depto =>' || p_codigo_depto 
                                                                        || ' p_nr_duplicata =>' || p_nr_duplicata 
                                                                        || ' p_codigo_contabil =>' || p_codigo_contabil
                                                                        || ' v_num_lcto =>' || p_num_lcmt
                                                                        || ' xp_parcela =>' || p_parcela
                                                                        || ' p_num_importacao =>' || p_num_importacao;
end inter_pr_insere_cpag_010;
/
