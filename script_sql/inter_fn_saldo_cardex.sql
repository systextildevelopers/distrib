
  CREATE OR REPLACE FUNCTION "INTER_FN_SALDO_CARDEX" 
   (p_deposito         in     number,    p_nivel            in     varchar2,
    p_grupo            in     varchar2,  p_subgrupo         in     varchar2,
    p_item             in     varchar2,  p_data_base        in     date)
return number 
is

   v_saldo_fisico     number := 0.00;
   v_ultima_sequencia number;
   v_saldo_movimento  number;
   v_periodo_fechado  date;
   v_data_busca       date;

BEGIN
   select periodo_estoque into v_periodo_fechado from empr_001;

   begin
      select max(estq_310.data_movimento) into v_data_busca from estq_310
      where estq_310.codigo_deposito    = p_deposito
      and   estq_310.nivel_estrutura    = p_nivel
      and   estq_310.grupo_estrutura    = p_grupo
      and   estq_310.subgrupo_estrutura = p_subgrupo
      and   estq_310.item_estrutura     = p_item
      and   estq_310.data_movimento    <= p_data_base;

      if v_data_busca is not null
      then
         select max(estq_310.sequencia_ficha) into v_ultima_sequencia from estq_310
         where estq_310.codigo_deposito    = p_deposito
         and   estq_310.nivel_estrutura    = p_nivel
         and   estq_310.grupo_estrutura    = p_grupo
         and   estq_310.subgrupo_estrutura = p_subgrupo
         and   estq_310.item_estrutura     = p_item
         and   estq_310.data_movimento     = v_data_busca;

         select estq_310.saldo_fisico
         into   v_saldo_fisico
         from estq_310
         where estq_310.codigo_deposito    = p_deposito
         and   estq_310.nivel_estrutura    = p_nivel
         and   estq_310.grupo_estrutura    = p_grupo
         and   estq_310.subgrupo_estrutura = p_subgrupo
         and   estq_310.item_estrutura     = p_item
         and   estq_310.data_movimento     = v_data_busca
         and   estq_310.sequencia_ficha    = v_ultima_sequencia;
      end if;

      exception
         when others then
            v_saldo_fisico := 0.000;
   end;

   if v_periodo_fechado < p_data_base
   then
      select nvl(sum(decode(entrada_saida, 'E', quantidade, quantidade * (-1.0))),0)
      into v_saldo_movimento
      from estq_300
      where codigo_deposito    = p_deposito
      and   nivel_estrutura    = p_nivel
      and   grupo_estrutura    = p_grupo
      and   subgrupo_estrutura = p_subgrupo
      and   item_estrutura     = p_item
      and   data_movimento    <= p_data_base;

      v_saldo_fisico := v_saldo_fisico + v_saldo_movimento;
   end if;

   return (v_saldo_fisico);
end inter_fn_saldo_cardex;

 

/

exec inter_pr_recompile;

