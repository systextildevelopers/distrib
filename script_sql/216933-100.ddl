alter table fatu_501 add gera_numeracao_aut  number(1) default 0;

comment on column fatu_501.gera_numeracao_aut is 'Gerar numeração automática na digitação manual de títulos a receber';
