alter table cpag_250 add(
ctrlRetorno varchar2(60) default ' ' );

comment on column cpag_250.ctrlRetorno
  is 'Controle bradesco para nao importar o mesmo arquivo';

exec inter_pr_recompile;
