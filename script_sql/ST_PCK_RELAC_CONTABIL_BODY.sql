create or replace package body "ST_PCK_RELAC_CONTABIL" is

	FUNCTION get(	p_cod_plano_cta NUMBER, 	p_tipo_contabil NUMBER,
					p_codigo_contabil NUMBER,	p_transacao NUMBER,
                    p_msg_erro in out varchar2) 
    return t_dados_cont_550 
    as 
        r_bulk t_dados_cont_550;
	BEGIN

        begin
            select * 
            BULK COLLECT INTO r_bulk
            from cont_550 
            where cod_plano_cta = p_cod_plano_cta
            and tipo_contabil = p_tipo_contabil 
            and codigo_contabil = p_codigo_contabil 
            and transacao = p_transacao;
        exception when others then
            p_msg_erro := 'Não foi possível buscar relacionamento contabil!. cod_plano_cta:' || p_cod_plano_cta
                                                                        || ' tipo_contabil' || p_tipo_contabil
                                                                        || ' codigo_contabil' || p_codigo_contabil
                                                                        || ' transacao' || p_transacao;
        end;

		RETURN r_bulk;
	END ;

    function lista_relacionamentos(p_cod_plano_cta number, p_msg_erro in out varchar2) 
    return t_dados_cont_550 
    as 
        r_bulk t_dados_cont_550;
	begin

        begin
            select * 
            BULK COLLECT INTO r_bulk
            from cont_550 
            where cod_plano_cta = p_cod_plano_cta;
        exception when others then
            p_msg_erro := 'Não foi possível buscar relacionamento contabil!. cod_plano_cta:' || p_cod_plano_cta;
        end;
		
		RETURN r_bulk;
    end;

    procedure insere_relacionamento(	p_cod_plano_cta NUMBER, 	p_tipo_contabil NUMBER,
									    p_codigo_contabil NUMBER,	p_transacao NUMBER,
									    p_conta_contabil NUMBER) AS
    begin
        begin
			insert into cont_550 ( cod_plano_cta, tipo_contabil, codigo_contabil, transacao, conta_contabil) 
			values (p_cod_plano_cta, p_tipo_contabil, p_codigo_contabil, p_transacao, p_conta_contabil);
		exception when others then
			raise_application_error(-20001,'Ocorreu um erro ao inserir o relacionamento contabil' || SQLERRM);
		end;
	end;

end "ST_PCK_RELAC_CONTABIL";
