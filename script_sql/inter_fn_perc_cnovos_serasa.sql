
  CREATE OR REPLACE FUNCTION "INTER_FN_PERC_CNOVOS_SERASA" 
  (p_pedido_venda pedi_100.pedido_venda%type)
return number
is

   v_total_anual     number(15,2);
   v_total_3_meses   number(15,2);
   v_perc_cli_novos  number(5,2);
   v_cod_rep_cliente pedi_100.cod_rep_cliente%type;

begin

   begin
      select pedi_100.cod_rep_cliente
      into   v_cod_rep_cliente
      from pedi_100
      where pedi_100.pedido_venda = p_pedido_venda;
      exception when others then
         v_cod_rep_cliente := 0;
   end;

   begin
      select nvl(sum(pedi_110.qtde_faturada), 0)
	    into v_total_anual
      from pedi_110
      where pedi_110.cod_cancelamento = 0
        and pedi_110.pedido_venda in (select pedi_100.pedido_venda
                  										from pedi_100
                                      where pedi_100.cod_cancelamento = 0
                  										  and pedi_100.data_emis_venda  >= current_date - 365
                  										  and pedi_100.cod_rep_cliente  = v_cod_rep_cliente);
      exception when others then
         v_total_anual := 1.00;
   end;

   if sql%notfound
   then
      v_total_anual := 1.00;
   end if;

   begin
      select nvl(sum(pedi_110.qtde_faturada), 0.00)
      into v_total_3_meses
  		from pedi_110
  		where pedi_110.cod_cancelamento = 0
    		and pedi_110.pedido_venda in (select pedi_100.pedido_venda
                  										from pedi_100, pedi_010
                  									  where pedi_100.cli_ped_cgc_cli9 =  pedi_010.cgc_9
                  										  and pedi_100.cli_ped_cgc_cli4 =  pedi_010.cgc_4
                  										  and pedi_100.cli_ped_cgc_cli2 =  pedi_010.cgc_2
                                        and pedi_100.cod_rep_cliente  =  v_cod_rep_cliente
                                        and pedi_100.data_emis_venda  >= current_date - 365
                  										  and pedi_100.cod_cancelamento =  0
                                        and pedi_010.data_cad_cliente >= current_date - 90);
      exception when others then
         v_total_3_meses := 0.00;
   end;

   if sql%notfound
   then
      v_total_3_meses := 0.00;
   end if;

   if v_total_anual = 0
   then v_perc_cli_novos :=  0;
   else v_perc_cli_novos := (v_total_3_meses * 100) / v_total_anual;
   end if;

   return(v_perc_cli_novos);

end inter_fn_perc_cnovos_serasa;
 

/

exec inter_pr_recompile;

