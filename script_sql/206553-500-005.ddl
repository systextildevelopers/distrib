alter table pcpb_015 add nr_reprocesso number(10);
comment on column pcpb_015.nr_reprocesso is 'Número do reprocesso utilizado na manutenção de estágios que adicionou este registro';

exec inter_pr_recompile;
