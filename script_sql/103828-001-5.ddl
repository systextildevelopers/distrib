begin
   for basi_270_cur in (select basi_270.valor_moeda, basi_270.codigo_moeda, basi_270.data_moeda
                        from basi_270, cpag_200
                        where basi_270.codigo_moeda = cpag_200.cod_moeda
                         and basi_270.data_moeda = cpag_200.data_moeda 
                        group by basi_270.codigo_moeda, basi_270.data_moeda, basi_270.valor_moeda)
   loop
      update cpag_200
         set cpag_200.cotacao_moeda = basi_270_cur.valor_moeda
      where cpag_200.cod_moeda  = basi_270_cur.codigo_moeda
        and cpag_200.data_moeda  = basi_270_cur.data_moeda;
   end loop;
   commit;

   for basi_270_cur2 in (select basi_270.valor_moeda, basi_270.codigo_moeda, basi_270.data_moeda
                        from basi_270, cpag_010
                        where basi_270.codigo_moeda = cpag_010.moeda_titulo
                         and basi_270.data_moeda = cpag_010.data_contrato
                        group by basi_270.codigo_moeda, basi_270.data_moeda, basi_270.valor_moeda)
   loop
      update cpag_010
         set cpag_010.cotacao_moeda = basi_270_cur2.valor_moeda
      where cpag_010.moeda_titulo  = basi_270_cur2.codigo_moeda
        and cpag_010.data_contrato  = basi_270_cur2.data_moeda;
   end loop;
   commit;
end;

/
exec inter_pr_recompile;
