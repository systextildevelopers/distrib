CREATE OR REPLACE FORCE VIEW INTER_VI_ESTRUTURA ("NIVEL_ITEM", "GRUPO_ITEM", "SUB_ITEM", "ITEM_ITEM", "ALTERNATIVA_ITEM", "SEQUENCIA", "NIVEL_COMP", "GRUPO_COMP", "SUB_COMP", "ITEM_COMP", "ALTERNATIVA_COMP", "CONSUMO", "ESTAGIO", "TIPO_CALCULO", "LETRA_GRAFICO", "QTDE_CAMADAS", "PERCENT_PERDAS", "NUMERO_GRAFICO", "QTDE_INICIAL", "QTDE_FINAL", "TENSAO", "LFA", "LOTE", "FORNECEDOR", "CONS_UN_REC", "SEQ_PRINCIPAL", "CENTRO_CUSTO", "CONS_UNID_MED_GENERICA", "PERC_CONS_CALC", "CALCULA_COMPOSICAO", "RELACAO_BANHO", "QTDE_PECAS_ESTAMPADAS", "TECIDO_PRINCIPAL", "QTDE_PECAS", "AGRUP_TINGIMENTO", "TIPO_VARETA", "VARETA", "PRESSAO", "SEQ_EXECUCAO", "CONSUMO_UNIDADE") AS 
select
-- Estruturas cadastradas sem variacao de componentes
basi_050.nivel_item,
basi_050.grupo_item,
basi_050.sub_item,
basi_050.item_item,
basi_050.alternativa_item,
basi_050.sequencia,
basi_050.nivel_comp,
basi_050.grupo_comp,
basi_050.sub_comp,
basi_050.item_comp,
basi_050.alternativa_comp,
basi_050.consumo,
basi_050.estagio,
basi_050.tipo_calculo,
basi_050.letra_grafico,
basi_050.qtde_camadas,
basi_050.percent_perdas,
basi_050.numero_grafico,
basi_050.qtde_inicial,
basi_050.qtde_final,
basi_050.tensao,
basi_050.lfa,
basi_050.lote,
basi_050.fornecedor,
basi_050.cons_un_rec,
basi_050.seq_principal,
basi_050.centro_custo,
basi_050.cons_unid_med_generica,
basi_050.perc_cons_calc,
basi_050.calcula_composicao,
basi_050.relacao_banho,
basi_050.qtde_pecas_estampadas,
basi_050.tecido_principal,
basi_050.QTDE_PECAS,
basi_050.AGRUP_TINGIMENTO,
BASI_050.TIPO_VARETA,
BASI_050.VARETA,
BASI_050.PRESSAO,
BASI_050.SEQ_EXECUCAO,
BASI_050.consumo_unidade
from basi_050
where sub_item <> '000'
and item_item <> '000000'
union all select
-- Estruturas com possibilidade de variacao para subgrupos e items
-- nao variando os componentes
basi_050.nivel_item,
basi_050.grupo_item,
basi_010.subgru_estrutura sub_item,
basi_010.item_estrutura item_item,
basi_050.alternativa_item,
basi_050.sequencia,
basi_050.nivel_comp,
basi_050.grupo_comp,
basi_050.sub_comp,
basi_050.item_comp,
basi_050.alternativa_comp,
basi_050.consumo,
basi_050.estagio,
basi_050.tipo_calculo,
basi_050.letra_grafico,
basi_050.qtde_camadas,
basi_050.percent_perdas,
basi_050.numero_grafico,
basi_050.qtde_inicial,
basi_050.qtde_final,
basi_050.tensao,
basi_050.lfa,
basi_050.lote,
basi_050.fornecedor,
basi_050.cons_un_rec,
basi_050.seq_principal,
basi_050.centro_custo,
basi_050.cons_unid_med_generica,
basi_050.perc_cons_calc,
basi_050.calcula_composicao,
basi_050.relacao_banho,
basi_050.qtde_pecas_estampadas,
basi_050.tecido_principal,
basi_050.QTDE_PECAS,
basi_050.AGRUP_TINGIMENTO,
BASI_050.TIPO_VARETA,
BASI_050.VARETA,
BASI_050.PRESSAO,
BASI_050.SEQ_EXECUCAO,
BASI_050.consumo_unidade
from basi_050, basi_010
where basi_050.nivel_item = basi_010.nivel_estrutura
and basi_050.grupo_item = basi_010.grupo_estrutura
and basi_050.sub_item = '000'
and basi_050.item_item = '000000'
and basi_050.sub_comp <> '000'
and basi_050.consumo <> 0.00
and basi_050.item_comp <> '000000'
union all
select
-- Estruturas com possibilidade de variacao para subgrupos
-- nao variando os componentes
basi_050.nivel_item,
basi_050.grupo_item,
basi_020.tamanho_ref sub_item,
basi_050.item_item,
basi_050.alternativa_item,
basi_050.sequencia,
basi_050.nivel_comp,
basi_050.grupo_comp,
basi_050.sub_comp,
basi_050.item_comp,
basi_050.alternativa_comp,
basi_050.consumo,
basi_050.estagio,
basi_050.tipo_calculo,
basi_050.letra_grafico,
basi_050.qtde_camadas,
basi_050.percent_perdas,
basi_050.numero_grafico,
basi_050.qtde_inicial,
basi_050.qtde_final,
basi_050.tensao,
basi_050.lfa,
basi_050.lote,
basi_050.fornecedor,
basi_050.cons_un_rec,
basi_050.seq_principal,
basi_050.centro_custo,
basi_050.cons_unid_med_generica,
basi_050.perc_cons_calc,
basi_050.calcula_composicao,
basi_050.relacao_banho,
basi_050.qtde_pecas_estampadas,
basi_050.tecido_principal,
basi_050.QTDE_PECAS,
basi_050.AGRUP_TINGIMENTO,
BASI_050.TIPO_VARETA,
BASI_050.VARETA,
BASI_050.PRESSAO,
BASI_050.SEQ_EXECUCAO,
BASI_050.consumo_unidade
from basi_050, basi_020
where basi_050.nivel_item = basi_020.basi030_nivel030
and basi_050.grupo_item = basi_020.basi030_referenc
and basi_050.sub_item = '000'
and basi_050.item_item <> '000000'
and basi_050.sub_comp <> '000'
and basi_050.consumo <> 0.00
union all
select
-- Estruturas com possibilidade de variacao para itens
-- nao variando os componentes
basi_050.nivel_item,
basi_050.grupo_item,
basi_050.sub_item,
basi010.item_estrutura item_item,
basi_050.alternativa_item,
basi_050.sequencia,
basi_050.nivel_comp,
basi_050.grupo_comp,
basi_050.sub_comp,
basi_050.item_comp,
basi_050.alternativa_comp,
basi_050.consumo,
basi_050.estagio,
basi_050.tipo_calculo,
basi_050.letra_grafico,
basi_050.qtde_camadas,
basi_050.percent_perdas,
basi_050.numero_grafico,
basi_050.qtde_inicial,
basi_050.qtde_final,
basi_050.tensao,
basi_050.lfa,
basi_050.lote,
basi_050.fornecedor,
basi_050.cons_un_rec,
basi_050.seq_principal,
basi_050.centro_custo,
basi_050.cons_unid_med_generica,
basi_050.perc_cons_calc,
basi_050.calcula_composicao,
basi_050.relacao_banho,
basi_050.qtde_pecas_estampadas,
basi_050.tecido_principal,
basi_050.QTDE_PECAS,
basi_050.AGRUP_TINGIMENTO,
BASI_050.TIPO_VARETA,
BASI_050.VARETA,
BASI_050.PRESSAO,
BASI_050.SEQ_EXECUCAO,
BASI_050.consumo_unidade
from basi_050,
(select nivel_estrutura,grupo_estrutura,item_estrutura from basi_010
group by nivel_estrutura,grupo_estrutura,item_estrutura) basi010
where basi_050.nivel_item = basi010.nivel_estrutura
and basi_050.grupo_item = basi010.grupo_estrutura
and basi_050.sub_item <> '000'
and basi_050.item_item = '000000'
and basi_050.item_comp <> '000000'
union all
select
-- Estruturas com possibilidade de variacao para subgrupos e items
-- variando os componentes pela cor
basi_050.nivel_item,
basi_050.grupo_item,
basi_010.subgru_estrutura sub_item,
basi_010.item_estrutura item_item,
basi_050.alternativa_item,
basi_050.sequencia,
basi_050.nivel_comp,
basi_050.grupo_comp,
basi_050.sub_comp,
basi_040.item_comp,
basi_050.alternativa_comp,
basi_050.consumo,
basi_050.estagio,
basi_050.tipo_calculo,
basi_050.letra_grafico,
basi_050.qtde_camadas,
basi_050.percent_perdas,
basi_050.numero_grafico,
basi_050.qtde_inicial,
basi_050.qtde_final,
basi_050.tensao,
basi_050.lfa,
basi_050.lote,
basi_050.fornecedor,
basi_050.cons_un_rec,
basi_050.seq_principal,
basi_050.centro_custo,
basi_050.cons_unid_med_generica,
basi_050.perc_cons_calc,
basi_050.calcula_composicao,
basi_050.relacao_banho,
basi_050.qtde_pecas_estampadas,
basi_050.tecido_principal,
basi_050.QTDE_PECAS,
basi_050.AGRUP_TINGIMENTO,
BASI_050.TIPO_VARETA,
BASI_050.VARETA,
BASI_050.PRESSAO,
BASI_050.SEQ_EXECUCAO,
BASI_050.consumo_unidade
from basi_050,basi_040, basi_010
where basi_050.nivel_item = basi_010.nivel_estrutura
and basi_050.grupo_item = basi_010.grupo_estrutura
and basi_050.nivel_item = basi_040.nivel_item
and basi_050.grupo_item = basi_040.grupo_item
and basi_050.sub_item = basi_040.sub_item
and basi_010.item_estrutura = basi_040.item_item
and basi_050.alternativa_item = basi_040.alternativa_item
and basi_050.sequencia = basi_040.sequencia
and basi_050.sub_item = '000'
and basi_050.item_item = '000000'
and basi_050.sub_comp <> '000'
and basi_050.consumo <> 0.00
and basi_050.item_comp = '000000'
union all
select
-- Estruturas com possibilidade de variacao para subgrupos e items
-- variando os componentes pelo tamanho ou consumo
basi_050.nivel_item,
basi_050.grupo_item,
basi_010.subgru_estrutura sub_item,
basi_010.item_estrutura item_item,
basi_050.alternativa_item,
basi_050.sequencia,
basi_050.nivel_comp,
basi_050.grupo_comp,
basi_040.sub_comp,
basi_050.item_comp,
basi_050.alternativa_comp,
basi_040.consumo,
basi_050.estagio,
basi_050.tipo_calculo,
basi_050.letra_grafico,
basi_050.qtde_camadas,
basi_050.percent_perdas,
basi_050.numero_grafico,
basi_050.qtde_inicial,
basi_050.qtde_final,
basi_050.tensao,
basi_050.lfa,
basi_050.lote,
basi_050.fornecedor,
basi_050.cons_un_rec,
basi_050.seq_principal,
basi_050.centro_custo,
basi_040.cons_unid_med_generica,
basi_050.perc_cons_calc,
basi_050.calcula_composicao,
basi_050.relacao_banho,
basi_050.qtde_pecas_estampadas,
basi_050.tecido_principal,
basi_040.QTDE_PECAS,
basi_050.AGRUP_TINGIMENTO,
BASI_050.TIPO_VARETA,
BASI_050.VARETA,
BASI_050.PRESSAO,
BASI_050.SEQ_EXECUCAO,
BASI_050.consumo_unidade
from basi_050, basi_040, basi_010
where basi_050.nivel_item = basi_010.nivel_estrutura
and basi_050.grupo_item = basi_010.grupo_estrutura
and basi_050.nivel_item = basi_040.nivel_item
and basi_050.grupo_item = basi_040.grupo_item
and basi_010.subgru_estrutura = basi_040.sub_item
and basi_050.item_item = basi_040.item_item
and basi_050.alternativa_item = basi_040.alternativa_item
and basi_050.sequencia = basi_040.sequencia
and basi_050.sub_item = '000'
and basi_050.item_item = '000000'
and (basi_050.sub_comp = '000' or basi_050.consumo = 0.00)
and basi_050.item_comp <> '000000'
union all
select
-- Estruturas com possibilidade de variacao para subgrupos e items
-- variando os componentes pelo tamanho ou consumo e cor
basi_050.nivel_item,
basi_050.grupo_item,
basi_010.subgru_estrutura sub_item,
basi_010.item_estrutura item_item,
basi_050.alternativa_item,
basi_050.sequencia,
basi_050.nivel_comp,
basi_050.grupo_comp,
basi0040.sub_comp,
basi0040.item_comp,
basi_050.alternativa_comp,
basi0040.consumo,
basi_050.estagio,
basi_050.tipo_calculo,
basi_050.letra_grafico,
basi_050.qtde_camadas,
basi_050.percent_perdas,
basi_050.numero_grafico,
basi_050.qtde_inicial,
basi_050.qtde_final,
basi_050.tensao,
basi_050.lfa,
basi_050.lote,
basi_050.fornecedor,
basi_050.cons_un_rec,
basi_050.seq_principal,
basi_050.centro_custo,
basi_050.cons_unid_med_generica,
basi_050.perc_cons_calc,
basi_050.calcula_composicao,
basi_050.relacao_banho,
basi_050.qtde_pecas_estampadas,
basi_050.tecido_principal,
basi0040.QTDE_PECAS,
basi_050.AGRUP_TINGIMENTO,
BASI_050.TIPO_VARETA,
BASI_050.VARETA,
BASI_050.PRESSAO,
BASI_050.SEQ_EXECUCAO,
BASI_050.consumo_unidade
from basi_050, basi_010,
(select basi_040.nivel_item,
basi_040.grupo_item,
decode(basi_040.sub_item,'000' ,basi040.sub_item ,basi_040.sub_item) sub_item,
decode(basi_040.item_item,'000000',basi040.item_item,basi_040.item_item) item_item,
basi_040.alternativa_item,
basi_040.sequencia,
decode(basi_040.sub_comp,'000' ,basi040.sub_comp ,basi_040.sub_comp) sub_comp,
decode(basi_040.item_comp,'000000',basi040.item_comp,basi_040.item_comp) item_comp,
basi_040.consumo,
basi_040.QTDE_PECAS
from basi_040,
(select nivel_item,grupo_item,sub_item,item_item,alternativa_item,sequencia,sub_comp,item_comp,consumo
from basi_040) basi040
where basi_040.nivel_item = basi040.nivel_item
and basi_040.grupo_item = basi040.grupo_item
and basi040.sub_item = '000'
and basi_040.item_item = '000000'
and basi_040.alternativa_item = basi040.alternativa_item
and basi_040.sequencia = basi040.sequencia
) basi0040
where basi_050.nivel_item = basi_010.nivel_estrutura
and basi_050.grupo_item = basi_010.grupo_estrutura
and basi_050.nivel_item = basi0040.nivel_item
and basi_050.grupo_item = basi0040.grupo_item
and basi_010.subgru_estrutura = basi0040.sub_item
and basi_010.item_estrutura = basi0040.item_item
and basi_050.alternativa_item = basi0040.alternativa_item
and basi_050.sequencia = basi0040.sequencia
and basi_050.sub_item = '000'
and basi_050.item_item = '000000'
and (basi_050.sub_comp = '000' or basi_050.consumo = 0.00)
and basi_050.item_comp = '000000'
union all
select
-- Estruturas com possibilidade de variacao para subgrupos dos items
-- variando os componentes pelo subgrupo
basi_050.nivel_item,
basi_050.grupo_item,
basi_040.sub_item,
basi_050.item_item,
basi_050.alternativa_item,
basi_050.sequencia,
basi_050.nivel_comp,
basi_050.grupo_comp,
basi_040.sub_comp,
basi_050.item_comp,
basi_050.alternativa_comp,
basi_040.consumo,
basi_050.estagio,
basi_050.tipo_calculo,
basi_050.letra_grafico,
basi_050.qtde_camadas,
basi_050.percent_perdas,
basi_050.numero_grafico,
basi_050.qtde_inicial,
basi_050.qtde_final,
basi_050.tensao,
basi_050.lfa,
basi_050.lote,
basi_050.fornecedor,
basi_050.cons_un_rec,
basi_050.seq_principal,
basi_050.centro_custo,
basi_050.cons_unid_med_generica,
basi_050.perc_cons_calc,
basi_050.calcula_composicao,
basi_050.relacao_banho,
basi_050.qtde_pecas_estampadas,
basi_050.tecido_principal,
basi_040.QTDE_PECAS,
basi_050.AGRUP_TINGIMENTO,
BASI_050.TIPO_VARETA,
BASI_050.VARETA,
BASI_050.PRESSAO,
BASI_050.SEQ_EXECUCAO,
BASI_050.consumo_unidade
from basi_050,basi_040
where basi_050.nivel_item = basi_040.nivel_item
and basi_050.grupo_item = basi_040.grupo_item
and basi_050.item_item = basi_040.item_item
and basi_050.alternativa_item = basi_040.alternativa_item
and basi_050.sequencia = basi_040.sequencia
and basi_050.sub_item = '000'
and basi_050.item_item <> '000000'
and (basi_050.sub_comp = '000' or basi_050.consumo = 0.00)
union all
select
-- Estruturas com possibilidade de variacao para cores dos items
-- variando os componentes pelo cores
basi_050.nivel_item,
basi_050.grupo_item,
basi_040.sub_item,
basi_040.item_item,
basi_050.alternativa_item,
basi_050.sequencia,
basi_050.nivel_comp,
basi_050.grupo_comp,
basi_050.sub_comp,
basi_040.item_comp,
basi_050.alternativa_comp,
basi_050.consumo,
basi_050.estagio,
basi_050.tipo_calculo,
basi_050.letra_grafico,
basi_050.qtde_camadas,
basi_050.percent_perdas,
basi_050.numero_grafico,
basi_050.qtde_inicial,
basi_050.qtde_final,
basi_050.tensao,
basi_050.lfa,
basi_050.lote,
basi_050.fornecedor,
basi_050.cons_un_rec,
basi_050.seq_principal,
basi_050.centro_custo,
basi_050.cons_unid_med_generica,
basi_050.perc_cons_calc,
basi_050.calcula_composicao,
basi_050.relacao_banho,
basi_050.qtde_pecas_estampadas,
basi_050.tecido_principal,
basi_050.QTDE_PECAS,
basi_050.AGRUP_TINGIMENTO,
BASI_050.TIPO_VARETA,
BASI_050.VARETA,
BASI_050.PRESSAO,
BASI_050.SEQ_EXECUCAO,
BASI_050.consumo_unidade
from basi_050,basi_040
where basi_050.nivel_item = basi_040.nivel_item
and basi_050.grupo_item = basi_040.grupo_item
and basi_050.sub_item = basi_040.sub_item
and basi_050.alternativa_item = basi_040.alternativa_item
and basi_050.sequencia = basi_040.sequencia
and basi_050.sub_item <> '000'
and basi_050.item_item = '000000'
and basi_050.item_comp = '000000'
;

/

exec inter_pr_recompile;

