create table oper_277 (
	id 					number(9),
	data_hora_consulta 	date,
	chave_nf 			varchar2(44),
	url 				varchar2(120),
	certificado 		varchar2(70),
	status_old 			varchar2(5),
	status_new 			varchar2(5),
	nfe_cte 			varchar2(5),
	cgc9_emp			number(9),
	cgc4_emp 			number(4),
	cgc2_emp 			number(2)
);

alter table oper_277 add constraint oper_277_pk unique (id);

alter table oper_277 add constraint oper_277_ck check (nfe_cte in ('NFe','CTe'));
