CREATE OR REPLACE PROCEDURE "INTER_PR_EXPLODE_ESTRUTURA"
    -- Recebe parametros para explosao da estrutura.
   (p_nivel_explode       in varchar2,
    p_grupo_explode       in varchar2,
    p_subgrupo_explode    in varchar2,
    p_item_explode        in varchar2,
    p_alternativa_explode in number,
    p_periodo_producao    in number,
    p_qtde_quilos_prog    in number,
    p_pedido_ordem        in number,
    p_seq_ordem           in number,
    p_area_producao       in number,
    p_sequencia_item      in number,
    p_estagio             in number,
    p_estagio_baixa       in number,
    p_relacao_banho_aux   in number,
    p_codigo_embalagem    in number,
    p_origem_chamada      in varchar2,
    p_tipo_msg            in number)
is
   -- Cria cursor para executar loop na basi_050 buscando todos
   -- os produtos da estrutura.
   cursor basi050 is
   select nivel_comp,       grupo_comp,
          sub_comp,         item_comp,
          sequencia,        consumo,
          sub_item,         item_item,
          estagio,          tipo_calculo,
          alternativa_comp, relacao_banho
   from basi_050
   where  basi_050.nivel_item       = p_nivel_explode
     and  basi_050.grupo_item       = p_grupo_explode
     and (basi_050.sub_item         = p_subgrupo_explode or basi_050.sub_item  = '000')
     and (basi_050.item_item        = p_item_explode     or basi_050.item_item = '000000')
     and  basi_050.alternativa_item = p_alternativa_explode
     -- SS 53234-001 - retirado pois a estrutura que esta sendo explodida esta com o nivel 0 ou o estagio do nivel anterior esta 0.
     and (basi_050.estagio          = p_estagio
     or   basi_050.estagio          = 0
     or   (p_estagio                = 0
     and   p_area_producao          = 4)
     or   (p_estagio                = 0
     and   p_area_producao          = 2) -- Regra definida engenharia (Giba,Marcos)
     or   (p_nivel_explode          = '5' ))
     --  and (basi_050.estagio          = p_estagio or basi_050.estagio = 0)
   order by basi_050.sequencia;

   -- Declara as variaveis que serao utilizadas.
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   v_existe_registro         integer;
   rpt_pette                 varchar2(1);
   percent_perdas            basi_020.perc_perdas%type;

   p_nivel_comp050       basi_050.nivel_comp%type := '#';
   p_grupo_comp050       basi_050.grupo_comp%type := '#####';
   p_subgru_comp050      basi_050.sub_comp%type   := '###';
   p_item_comp050        basi_050.item_comp%type  := '######';
   p_sequencia050        basi_050.sequencia%type;
   p_consumo050          basi_050.consumo%type;
   p_sub_item050         basi_050.sub_item%type   := '###';
   p_item_item050        basi_050.item_item%type  := '######';
   p_estagio050          basi_050.estagio%type;
   p_tpcalculo050        basi_050.tipo_calculo%type;
   p_alternativa_comp    basi_050.alternativa_comp%type;
   p_relacao_banho       basi_050.relacao_banho%type;

   p_est_baixa           basi_050.estagio%type;

   p_fio_niv             basi_050.nivel_comp%type := '#';
   p_fio_gru             basi_050.grupo_comp%type := '#####';
   p_fio_sub             basi_050.sub_comp%type   := '###';
   p_fio_ite             basi_050.item_comp%type  := '######';

   p_percent_qtde_alter  basi_230.qtde_alternativa%type;

   p_qtde_reservada      tmrp_041.qtde_reservada%type;
   p_consumo_final       basi_050.consumo%type;

   p_reserva_planej_mat_prima4 empr_002.reserva_planej_mat_prima4%type;
   p_relacao_banho_emp         empr_001.relacao_banho%type;

   v_existe_041           number(9);
   v_existe_010           number(9);
   v_existe_040_data      number;
   v_existe_040_semdata   number;

   v_codigo_risco         pcpc_200.codigo_risco%type;
   v_consumo_liq          number(13,7);
   v_tipo_ordem           number(1);

   p_data_corrente        date;
   p_alternativa_comp_040 number(2);
   p_alt_estrut_planej    number(2);

   v_tipo_produto_030     basi_030.tipo_produto%type;
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- Busca parametro de empresa para calculo das quantidades que
   -- forem atraves do volume de banho.
   begin
      select empr_001.relacao_banho
      into p_relacao_banho_emp
      from empr_001;
   end;

   begin
      select nvl(count(1),0)
      into v_existe_registro
      from fatu_500
      where fatu_500.rpt_ordem_benef = 'pcpb_opb002';
   exception when others then
      v_existe_registro := 0;
   end;

   if v_existe_registro > 0
   then
      rpt_pette := 's'; /* PETTENATI */
   else
       rpt_pette := 'n'; /* TODOS OS CLIENTES */
   end if;

   -- Inicio do loop da explosao da estrutura.
   for reg_basi050 in basi050
   loop
      -- Carrega as variaveis com os valores do cursor.
      p_nivel_comp050    := reg_basi050.nivel_comp;
      p_grupo_comp050    := reg_basi050.grupo_comp;
      p_subgru_comp050   := reg_basi050.sub_comp;
      p_item_comp050     := reg_basi050.item_comp;
      p_sequencia050     := reg_basi050.sequencia;
      p_consumo050       := reg_basi050.consumo;
      p_sub_item050      := reg_basi050.sub_item;
      p_item_item050     := reg_basi050.item_item;
      p_estagio050       := reg_basi050.estagio;
      p_tpcalculo050     := reg_basi050.tipo_calculo;
      p_alternativa_comp := reg_basi050.alternativa_comp;

      p_est_baixa        := p_estagio_baixa;

      -- Pega alternativa da embalagem setada no cadastro de embalagem
      -- quando o codigo_embalagem for maior que zero e nivel comp
      -- for 3.
      if p_nivel_comp050 = '3' and p_codigo_embalagem > 0 and p_origem_chamada <> 'pcpc'  --marcio e giba revisar
      then
         begin
            select fatu_100.alternativa
            into p_alternativa_comp
            from fatu_100
            where fatu_100.codigo_embalagem = p_codigo_embalagem
              and fatu_100.alternativa      > 0
              and exists (select 1 from basi_030
                          where basi_030.nivel_estrutura  = '3'
                            and basi_030.referencia       = p_grupo_comp050
                            and basi_030.tipo_produto     = 6);
            exception
            when others then
               p_alternativa_comp := reg_basi050.alternativa_comp;
         end;
      end if;

      if p_origem_chamada = 'pcpt'
      then
         p_est_baixa := p_estagio050;
      end if;

      -- SS 53234-001 - Incluido consistencia abaixo pois a funcao de recalculo na leva em consideracao a relacao de banho
      -- para confeccao
      if p_origem_chamada    <> 'pcpc'
      then
         if p_nivel_explode      = '5'
         and p_relacao_banho_aux = 0.00
         then
            if reg_basi050.relacao_banho = 0.00
            then
               p_relacao_banho := p_relacao_banho_emp;
            else
               p_relacao_banho := reg_basi050.relacao_banho;
            end if;
         else
            p_relacao_banho := p_relacao_banho_aux;
         end if;
      else
         p_relacao_banho := 0.00;
      end if;

      if p_nivel_explode = '2'
      then
         p_relacao_banho := 0.00;
      end if;

      -- Carrega a data corrente para filtro.
      p_data_corrente    := sysdate;

      if p_subgru_comp050 = '000' or p_consumo050 = 0.0000000
      then
         begin
            select basi_040.sub_comp, basi_040.consumo
            into   p_subgru_comp050,  p_consumo050
            from basi_040
            where basi_040.nivel_item       = p_nivel_explode
              and basi_040.grupo_item       = p_grupo_explode
              and basi_040.sub_item         = p_subgrupo_explode
              and basi_040.item_item        = p_item_item050
              and basi_040.alternativa_item = p_alternativa_explode
              and basi_040.sequencia        = p_sequencia050;
            exception
            when others then
               p_subgru_comp050 := '000';
               p_consumo050     := 0.0000000;
         end;
      end if;

      p_alternativa_comp_040 := 0;

      if p_item_comp050 = '000000'
      then
         begin
            select basi_040.item_comp, basi_040.alternativa_comp
            into   p_item_comp050,     p_alternativa_comp_040
            from basi_040
            where basi_040.nivel_item       = p_nivel_explode
              and basi_040.grupo_item       = p_grupo_explode
              and basi_040.sub_item         = p_sub_item050
              and basi_040.item_item        = p_item_explode
              and basi_040.alternativa_item = p_alternativa_explode
              and basi_040.sequencia        = p_sequencia050;
            exception
            when others then
               p_item_comp050 := '000000';
               p_alternativa_comp_040 := 0;
         end;
      end if;

      -- Busca parametro de empresa executando a seguinte consistencia:
      -- 0 - Reserva Produto Principal ou 1 - Reserva Produto Alternativo
      select empr_002.reserva_planej_mat_prima4, empr_002.alternativa_estrutura_plane
      into   p_reserva_planej_mat_prima4,        p_alt_estrut_planej
      from empr_002;

      if p_alt_estrut_planej <> 0 --marcio e giba revisar
      then
         if p_alternativa_comp_040 <> 0
         then
            p_alternativa_comp := p_alternativa_comp_040;
         end if;
      end if;

      if p_nivel_explode = '4'
      then
         if p_reserva_planej_mat_prima4 = 0
         then
            p_fio_niv := p_nivel_comp050;
            p_fio_gru := p_grupo_comp050;
            p_fio_sub := p_subgru_comp050;
            p_fio_ite := p_item_comp050;
         else
            begin
               select basi_230.item_alt_nivel99,   basi_230.item_alt_grupo,
                      basi_230.item_alt_subgrupo,  basi_230.item_alt_item,
                      basi_230.qtde_alternativa
               into   p_fio_niv,                   p_fio_gru,
                      p_fio_sub,                   p_fio_ite,
                      p_percent_qtde_alter
               from basi_230
               where basi_230.item_pri_nivel99  = p_nivel_comp050
                 and basi_230.item_pri_grupo    = p_grupo_comp050
                 and basi_230.item_pri_subgrupo = p_subgru_comp050
                 and basi_230.item_pri_item     = p_item_comp050
                 and basi_230.data_inicial     <= p_data_corrente
                 and basi_230.data_final       >= p_data_corrente;

               p_consumo050 := p_consumo050 * p_percent_qtde_alter;

               exception
               when others then
                  p_fio_niv    := p_nivel_comp050;
                  p_fio_gru    := p_grupo_comp050;
                  p_fio_sub    := p_subgru_comp050;
                  p_fio_ite    := p_item_comp050;
                  p_consumo050 := p_consumo050;
            end;
         end if;
      else
         p_fio_niv := p_nivel_comp050;
         p_fio_gru := p_grupo_comp050;
         p_fio_sub := p_subgru_comp050;
         p_fio_ite := p_item_comp050;
      end if;

/*
      if  p_origem_chamada = 'pcpc'
      and p_fio_niv        <> '3'
      and p_fio_niv        <> '5'
      then*/
         -- SS 49150-001 - TDV
         -- Leitura da basi_030 para saber se e tecido retilineo ou normal
         -- Se for tecido retilineo nao calcula consumo pela funcao: inter_fn_calcula_consumo_risco, mas sim o consumo deve ser da estrutura.
         -- (1) - Tecido Normal (liso e estampado)
         -- (2) - Tecido Listrado (com fio tinto)
         -- (5) - Ribanas
         begin
            select basi_030.tipo_produto
            into    v_tipo_produto_030
            from basi_030
            where basi_030.nivel_estrutura = p_nivel_comp050
              and basi_030.referencia      = p_grupo_comp050;
         exception when others then
            v_tipo_produto_030 := 1;
         end;

         -- SS 39468/018.
         -- Quando houver risco informado para a ordem de corte
         -- O parametro p_seq_ordem, na confeccao, representa o codigo de risco
         v_codigo_risco := p_seq_ordem;
         v_consumo_liq  := 0.000000;

         if v_codigo_risco  = 0 or p_nivel_comp050 <> '2'
         then
            -- SS 53234-001
            if p_relacao_banho_aux > 0.000000
            and p_nivel_explode    = '5'
            then
               p_consumo_final := (p_qtde_reservada / p_relacao_banho_aux);
            else
               p_consumo_final := p_consumo050;
            end if;
         else
            if v_codigo_risco > 0 and p_nivel_comp050 = '2' and (v_tipo_produto_030 = 1 or v_tipo_produto_030 = 2 or v_tipo_produto_030 = 5)
            then
               begin
                  v_consumo_liq := inter_fn_calcula_consumo_risco
                    (p_grupo_explode,
                     p_subgrupo_explode,
                     p_item_explode,
                     p_nivel_comp050,
                     p_grupo_comp050,
                     p_subgru_comp050,
                     p_alternativa_explode,
                     p_sequencia050,
                     v_codigo_risco);
                  exception when others then
                     v_consumo_liq := 0.000000;
               end;
            end if;

            if p_relacao_banho_aux > 0.000000
            then
               p_consumo_final := v_consumo_liq / p_relacao_banho_aux;
            else
               if v_consumo_liq > 0
               then
                  p_consumo_final := v_consumo_liq;
                  p_consumo050    := v_consumo_liq;
               else
                  p_consumo_final := p_consumo050;
               end if;
            end if;
         end if;

      -- (DIGITACAO) - Se a origem da chamada for a confeccao, atualiza o planejamento da producao.
      if p_origem_chamada = 'pcpc'
      then
         if (p_nivel_explode = '5' or p_nivel_explode = '3')
         and (p_fio_niv = '9' or p_fio_niv = '2') -- Pettenati, Neylor
         then
             p_estagio050 := p_est_baixa;
         end if;
         p_qtde_reservada := p_consumo050 * p_codigo_embalagem;
         if p_pedido_ordem = 235032 and p_fio_niv = '9'  and p_fio_gru = '01160' and p_fio_sub = '901'
         then
            dbms_output.put_line('p_nivel_comp050: '||p_nivel_comp050||chr(10)||
                                'p_grupo_comp050:'||p_grupo_comp050||chr(10)||
                                'p_consumo050: '||p_consumo050||chr(10)||
                                'p_sequencia_item: '||p_sequencia_item||chr(10)||
                                'p_codigo_embalagem: '||p_codigo_embalagem||chr(10)||
                                'p_qtde_reservada: '||p_consumo050 * p_codigo_embalagem);
         end if;
      end if;

--         p_qtde_reservada := p_consumo_final * p_qtde_quilos_prog;
/*
         -- Verifica se o componente do produto esta cadastrado
         -- confere_estrutura
         select count(*)
         into v_existe_010
         from basi_010
         where basi_010.nivel_estrutura  = p_fio_niv
           and basi_010.grupo_estrutura  = p_fio_gru
           and basi_010.subgru_estrutura = p_fio_sub
           and basi_010.item_estrutura   = p_fio_ite;
         if v_existe_010 = 0
         then
            if p_tipo_msg = 0
            then
               inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26445', p_fio_niv , p_fio_gru, p_fio_sub , p_fio_ite , p_nivel_explode , p_grupo_explode  , p_sub_item050 , p_item_item050 , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#',
                                            1);
            else
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26445', p_fio_niv , p_fio_gru, p_fio_sub , p_fio_ite , p_nivel_explode , p_grupo_explode  , p_sub_item050 , p_item_item050 , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
            end if;
         end if;
*/
         
         -- Verifica se o componente do produto esta cadastrado confere_estrutura
         begin
            select 1
            into v_existe_010
            from basi_010
            where basi_010.nivel_estrutura  = p_fio_niv
              and basi_010.grupo_estrutura  = p_fio_gru
              and basi_010.subgru_estrutura = p_fio_sub
              and basi_010.item_estrutura   = p_fio_ite;
         exception
            when NO_DATA_FOUND then
               if p_tipo_msg = 0
               then
                  inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26445', p_fio_niv , p_fio_gru, p_fio_sub , p_fio_ite , p_nivel_explode , p_grupo_explode  , p_sub_item050 , p_item_item050 , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#',
                                               1);
               else
                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26445', p_fio_niv , p_fio_gru, p_fio_sub , p_fio_ite , p_nivel_explode , p_grupo_explode  , p_sub_item050 , p_item_item050 , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
               end if;
         end;


             if p_qtde_quilos_prog > 0
             then p_consumo_final := (p_qtde_reservada / p_qtde_quilos_prog);
             else p_consumo_final := p_consumo050;
             end if;
             --  p_qtde_reservada := p_consumo_final * p_codigo_embalagem;

             -- O campo relacao banho para confeccao e a quantidade prograda da OP.
             -- ** ajustar na intersys para facilitar futura interpretacao.
             -- Atualiza tabela de planejamento.

           if p_pedido_ordem = 235032 and p_fio_niv = '9'  and p_fio_gru = '01160' and p_fio_sub = '901'
            then
                  dbms_output.put_line('periodo_producao: '||p_periodo_producao||chr(10)||
                                       ' area_producao: '||p_pedido_ordem||chr(10)||
                                       ' p_fio_niv: '||p_fio_niv||chr(10)||
                                       ' p_fio_gru: '||p_fio_gru||chr(10)||
                                       ' p_fio_sub: '||p_fio_sub||chr(10)||
                                       ' p_fio_ite: '||p_fio_ite||chr(10)||
                                       ' qtde_reservada: '||p_qtde_reservada||chr(10)||
                                       ' p_qtde_quilos_prog:' ||p_qtde_quilos_prog||chr(10)||
                                       ' p_consumo_final: '||p_consumo_final||chr(10)||
                                       ' p_sequencia050: '||p_sequencia050||chr(10)||
                                       ' p_estagio: '||p_estagio050||chr(10)||
                                       ' p_codigo_embalagem: '||p_codigo_embalagem||chr(10)||
                                       ' v_consumo_liq: '||v_consumo_liq||chr(10));
              end if;

      if  p_origem_chamada = 'pcpc'
      and p_fio_niv        <> '3'
      and p_fio_niv        <> '5'
      then
/*
               -- Verifica se o registro ja existe, se nao existir insere novo registro
         -- senao atualiza a quantidade a receber do produto

          select count(*)
          into v_existe_041
          from tmrp_041
          where tmrp_041.periodo_producao    = p_periodo_producao
            and tmrp_041.area_producao       = 1
            and tmrp_041.nr_pedido_ordem     = p_pedido_ordem
            and tmrp_041.nivel_estrutura     = p_fio_niv
            and tmrp_041.grupo_estrutura     = p_fio_gru
            and tmrp_041.subgru_estrutura    = p_fio_sub
            and tmrp_041.item_estrutura      = p_fio_ite
            and tmrp_041.sequencia_estrutura = p_sequencia050
            and tmrp_041.codigo_estagio      = p_estagio050
            and tmrp_041.seq_pedido_ordem    = p_sequencia_item;

          if v_existe_041 = 0
          then

             begin
                insert into tmrp_041 (
                        periodo_producao,   area_producao,
                        nr_pedido_ordem,    nivel_estrutura,
                        grupo_estrutura,    subgru_estrutura,
                        item_estrutura,     qtde_reservada,
                        consumo,            sequencia_estrutura,
                        codigo_estagio,     seq_pedido_ordem,
                        tipo_calculo
                ) values (
                        p_periodo_producao, 1,
                        p_pedido_ordem,     p_fio_niv,
                        p_fio_gru,          p_fio_sub,
                        p_fio_ite,          p_qtde_reservada,
                        p_consumo_final,    p_sequencia050,
                        p_estagio050,       p_sequencia_item,
                        p_tpcalculo050
                );
             exception when others then
                if p_tipo_msg = 0
                then
                   inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#',
                                                0);
                else
                   raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
                end if;
             end;
          else
             begin
                update tmrp_041
                set tmrp_041.qtde_reservada = tmrp_041.qtde_reservada  + p_qtde_reservada
                where tmrp_041.periodo_producao    = p_periodo_producao
                  and tmrp_041.area_producao       = 1
                  and tmrp_041.nr_pedido_ordem     = p_pedido_ordem
                  and tmrp_041.nivel_estrutura     = p_fio_niv
                  and tmrp_041.grupo_estrutura     = p_fio_gru
                  and tmrp_041.subgru_estrutura    = p_fio_sub
                  and tmrp_041.item_estrutura      = p_fio_ite
                  and tmrp_041.sequencia_estrutura = p_sequencia050
                  and tmrp_041.codigo_estagio      = p_estagio050
                  and tmrp_041.seq_pedido_ordem    = p_sequencia_item;
             exception
             when NO_DATA_FOUND then
                if p_tipo_msg = 0
                   then
                      inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds24385', 'TMRP_041(inter_pr_explode_estrutura(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil  )||'#',
                                                1);
                   else
                      -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
                      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_041(inter_pr_explode_estrutura(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                end if;
             end;
          end if;
*/


          begin
             insert into tmrp_041 (
                     periodo_producao,   area_producao,
                     nr_pedido_ordem,    nivel_estrutura,
                     grupo_estrutura,    subgru_estrutura,
                     item_estrutura,     qtde_reservada,
                     consumo,            sequencia_estrutura,
                     codigo_estagio,     seq_pedido_ordem,
                     tipo_calculo
             ) values (
                     p_periodo_producao, 1,
                     p_pedido_ordem,     p_fio_niv,
                     p_fio_gru,          p_fio_sub,
                     p_fio_ite,          p_qtde_reservada,
                     p_consumo_final,    p_sequencia050,
                     p_estagio050,       p_sequencia_item,
                     p_tpcalculo050
             );
          exception
             -- Se for registro duplicado, tenta fazer update
             when DUP_VAL_ON_INDEX then
                begin
                   update tmrp_041
                   set tmrp_041.qtde_reservada = tmrp_041.qtde_reservada  + p_qtde_reservada
                   where tmrp_041.periodo_producao    = p_periodo_producao
                     and tmrp_041.area_producao       = 1
                     and tmrp_041.nr_pedido_ordem     = p_pedido_ordem
                     and tmrp_041.nivel_estrutura     = p_fio_niv
                     and tmrp_041.grupo_estrutura     = p_fio_gru
                     and tmrp_041.subgru_estrutura    = p_fio_sub
                     and tmrp_041.item_estrutura      = p_fio_ite
                     and tmrp_041.sequencia_estrutura = p_sequencia050
                     and tmrp_041.codigo_estagio      = p_estagio050
                     and tmrp_041.seq_pedido_ordem    = p_sequencia_item;
                exception
                   when NO_DATA_FOUND then
                      if p_tipo_msg = 0
                      then
                         inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds24385', 'TMRP_041(inter_pr_explode_estrutura(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil  )||'#',
                                                   1);
                      else
                         -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
                         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_041(inter_pr_explode_estrutura(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                      end if;
                   
                   when others then
                      if p_tipo_msg = 0
                      then
                         inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds24385', 'TMRP_041(inter_pr_explode_estrutura(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil  )||'#',
                                                   1);
                      else
                         -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
                         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_041(inter_pr_explode_estrutura(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                      end if;
                end;
                
             -- Se n?o for registro duplicado, registra a mensagem de erro do insert acima
             when others then
                if p_tipo_msg = 0
                then
                   inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#',
                                                0);
                else
                   raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
                end if;
          end;
      end if;

      v_tipo_ordem := 0;
      -- Se a origem da chamada for o beneficiamento, atualiza o planejamento da producao.
      if p_origem_chamada = 'pcpb'
      or p_origem_chamada = 'pcpt'
      then
         -- Quando o calculo for pelo volume de banho.
         if p_tpcalculo050 = 2
         then
            p_qtde_reservada := p_consumo050 * (p_qtde_quilos_prog * p_relacao_banho);
         else
            p_qtde_reservada := p_consumo050 *  p_qtde_quilos_prog;
         end if;

         if p_origem_chamada = 'pcpb'
         then
            p_estagio050 := p_est_baixa;
         end if;

         -- Verifica se o estagio da Ordem de Producao (Beneficiamento) esta baixado.
         -- Se estiver, joga a quantidade reservada para 0.00 (zero), para nao reservar
         -- quantidade de insumo de um estagio que ja foi baixado.
         begin
            v_existe_040_data := 0;

            select count(1)
            into v_existe_040_data
            from pcpb_040
            where pcpb_040.ordem_producao = p_pedido_ordem
              and pcpb_040.codigo_estagio = p_estagio050
              and pcpb_040.data_termino is not null;

/*            if  v_existe_040_data > 0
            and p_origem_chamada = 'pcpb'
            then
               p_qtde_reservada := 0.00;
            end if;
*/         end;

         begin
            v_existe_040_semdata := 0;

            select count(1)
            into v_existe_040_semdata
            from pcpb_040
            where pcpb_040.ordem_producao = p_pedido_ordem
              and pcpb_040.codigo_estagio = p_estagio050
              and pcpb_040.data_termino is null;

            if   v_existe_040_data    > 0
            and  v_existe_040_semdata = 0
            and p_origem_chamada      = 'pcpb'
            then
               p_qtde_reservada := 0.00;
            end if;
         end;

         -- Consistencia para atender a SS 44026/001
         -- Quando o tipo da ordem for
         -- 1 - ORDEM REPROCESSO (IDENTIFICACAO INTERNA)
         -- 2 - ORDEM REPROCESSO (IDENTIFICACAO EXTERNA)
         -- 4 - ORDEM APROVEITAMENTO (SEM PROCESSO)
         -- nao movimentara as necessidades dos tecidos acabados e crus.
         begin
            select pcpb_010.tipo_ordem
            into v_tipo_ordem
            from pcpb_010
            where pcpb_010.ordem_producao = p_pedido_ordem;
         exception
         when OTHERS then
            v_tipo_ordem := 0;
         end;

         if (p_fio_niv = '2' or p_fio_niv = '4') and rpt_pette = 's' and p_origem_chamada = 'pcpb'
         then
            begin
               select basi_020.perc_perdas
               into percent_perdas
               from  basi_020
               where basi_020.basi030_nivel030 = p_nivel_explode
                 and basi_020.basi030_referenc = p_grupo_explode
                 and basi_020.tamanho_ref      = p_subgrupo_explode;
            exception
            when OTHERS then
               percent_perdas := 0.00;
            end;
            if percent_perdas > 0.000
            then
               p_qtde_reservada := p_qtde_reservada * (1 + (percent_perdas / 100.00));
            end if;
         end if;

         if  (v_tipo_ordem <> 1 and v_tipo_ordem <> 2 and v_tipo_ordem <> 4)
         or  (p_fio_niv    <> '2' and p_fio_niv    <> '4')  -- VER COM ENGENHARIA
         then
/*
            -- Verifica se o componente do produto esta cadastrado
            -- confere_estrutura
            select count(*)
            into v_existe_010
            from basi_010
            where basi_010.nivel_estrutura  = p_fio_niv
              and basi_010.grupo_estrutura  = p_fio_gru
              and basi_010.subgru_estrutura = p_fio_sub
              and basi_010.item_estrutura   = p_fio_ite;
            if v_existe_010 = 0
            then
               if p_tipo_msg = 0
               then
                  inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26445', p_fio_niv , p_fio_gru, p_fio_sub , p_fio_ite , p_nivel_explode , p_grupo_explode  , p_sub_item050 , p_item_item050 , '' , '',ws_locale_usuario,ws_usuario_systextil ),
                                               1);
               else
                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26445', p_fio_niv , p_fio_gru, p_fio_sub , p_fio_ite , p_nivel_explode , p_grupo_explode  , p_sub_item050 , p_item_item050 , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
               end if;
            end if;
*/
            
            -- Verifica se o componente do produto esta cadastrado confere_estrutura
            begin
               select 1
               into v_existe_010
               from basi_010
               where basi_010.nivel_estrutura  = p_fio_niv
                 and basi_010.grupo_estrutura  = p_fio_gru
                 and basi_010.subgru_estrutura = p_fio_sub
                 and basi_010.item_estrutura   = p_fio_ite;
            exception
               when NO_DATA_FOUND then
                  if p_tipo_msg = 0
                  then
                     inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26445', p_fio_niv , p_fio_gru, p_fio_sub , p_fio_ite , p_nivel_explode , p_grupo_explode  , p_sub_item050 , p_item_item050 , '' , '',ws_locale_usuario,ws_usuario_systextil ),
                                                  1);
                  else
                     raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26445', p_fio_niv , p_fio_gru, p_fio_sub , p_fio_ite , p_nivel_explode , p_grupo_explode  , p_sub_item050 , p_item_item050 , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
                  end if;
            end;

/*
            -- Verifica se o registro ja existe, se nao existir insere novo registro
            -- senao atualiza a quantidade a receber do produto.
            select count(*)
            into v_existe_041
            from tmrp_041
            where tmrp_041.periodo_producao    = p_periodo_producao
              and tmrp_041.area_producao       = p_area_producao
              and tmrp_041.nr_pedido_ordem     = p_pedido_ordem
              and tmrp_041.nivel_estrutura     = p_fio_niv
              and tmrp_041.grupo_estrutura     = p_fio_gru
              and tmrp_041.subgru_estrutura    = p_fio_sub
              and tmrp_041.item_estrutura      = p_fio_ite
              and tmrp_041.sequencia_estrutura = p_sequencia050
              and tmrp_041.codigo_estagio      = p_estagio050
              and tmrp_041.seq_pedido_ordem    = p_sequencia_item;

            if v_existe_041 = 0
            then
               if(p_origem_chamada = 'pcpb'
               and p_fio_niv <> '3'
               and p_fio_niv <> '5')
               or(p_origem_chamada = 'pcpt'
               and p_qtde_reservada > 0.000
               and p_fio_niv <> '3'
               and p_fio_niv <> '5')
               then
                  begin
                     -- Atualiza tabela de planejamento.
                     insert into tmrp_041
                            (periodo_producao,   area_producao,
                             nr_pedido_ordem,    nivel_estrutura,
                             grupo_estrutura,    subgru_estrutura,
                             item_estrutura,     qtde_reservada,
                             consumo,            sequencia_estrutura,
                             codigo_estagio,     seq_pedido_ordem,
                             tipo_calculo)
                     values (p_periodo_producao, p_area_producao,
                             p_pedido_ordem,     p_fio_niv,
                             p_fio_gru,          p_fio_sub,
                             p_fio_ite,          p_qtde_reservada,
                             p_consumo050,       p_sequencia050,
                             p_estagio050,       p_sequencia_item,
                             p_tpcalculo050);
                  exception when others then
                      if p_tipo_msg = 0
                      then
                         inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#',
                                                      0);
                      else
                         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                      end if;
                  end;
               end if;
            else
               begin
                  update tmrp_041
                  set tmrp_041.qtde_reservada = tmrp_041.qtde_reservada  + p_qtde_reservada
                  where tmrp_041.periodo_producao    = p_periodo_producao
                    and tmrp_041.area_producao       = p_area_producao
                    and tmrp_041.nr_pedido_ordem     = p_pedido_ordem
                    and tmrp_041.nivel_estrutura     = p_fio_niv
                    and tmrp_041.grupo_estrutura     = p_fio_gru
                    and tmrp_041.subgru_estrutura    = p_fio_sub
                    and tmrp_041.item_estrutura      = p_fio_ite
                    and tmrp_041.sequencia_estrutura = p_sequencia050
                    and tmrp_041.codigo_estagio      = p_estagio050
                    and tmrp_041.seq_pedido_ordem    = p_sequencia_item;
               exception
               when NO_DATA_FOUND then
                   if p_tipo_msg = 0
                   then
                      inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds24385', 'TMRP_041(inter_pr_explode_estrutura(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil  )||'#',
                                                   1);
                   else
                      -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
                      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_041(inter_pr_explode_estrutura(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                   end if;
               end;
            end if;

*/

            if (p_origem_chamada = 'pcpb' and p_fio_niv <> '3' and p_fio_niv <> '5')
            or (p_origem_chamada = 'pcpt' and p_qtde_reservada > 0.000 and p_fio_niv <> '3' and p_fio_niv <> '5')
            then
               begin
                  -- Atualiza tabela de planejamento.
                  insert into tmrp_041
                         (periodo_producao,   area_producao,
                          nr_pedido_ordem,    nivel_estrutura,
                          grupo_estrutura,    subgru_estrutura,
                          item_estrutura,     qtde_reservada,
                          consumo,            sequencia_estrutura,
                          codigo_estagio,     seq_pedido_ordem,
                          tipo_calculo)
                  values (p_periodo_producao, p_area_producao,
                          p_pedido_ordem,     p_fio_niv,
                          p_fio_gru,          p_fio_sub,
                          p_fio_ite,          p_qtde_reservada,
                          p_consumo050,       p_sequencia050,
                          p_estagio050,       p_sequencia_item,
                          p_tpcalculo050);
               exception
                  -- Se for registro duplicado, tenta fazer update
                  when DUP_VAL_ON_INDEX then
                     begin
                        update tmrp_041
                        set tmrp_041.qtde_reservada = tmrp_041.qtde_reservada  + p_qtde_reservada
                        where tmrp_041.periodo_producao    = p_periodo_producao
                          and tmrp_041.area_producao       = p_area_producao
                          and tmrp_041.nr_pedido_ordem     = p_pedido_ordem
                          and tmrp_041.nivel_estrutura     = p_fio_niv
                          and tmrp_041.grupo_estrutura     = p_fio_gru
                          and tmrp_041.subgru_estrutura    = p_fio_sub
                          and tmrp_041.item_estrutura      = p_fio_ite
                          and tmrp_041.sequencia_estrutura = p_sequencia050
                          and tmrp_041.codigo_estagio      = p_estagio050
                          and tmrp_041.seq_pedido_ordem    = p_sequencia_item;
                     exception
                        when NO_DATA_FOUND then
                           if p_tipo_msg = 0
                           then
                              inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds24385', 'TMRP_041(inter_pr_explode_estrutura(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil  )||'#',
                                                           1);
                           else
                              -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
                              raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_041(inter_pr_explode_estrutura(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                           end if;
                        
                        when others then
                           if p_tipo_msg = 0
                           then
                              inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds24385', 'TMRP_041(inter_pr_explode_estrutura(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil  )||'#',
                                                           1);
                           else
                              -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
                              raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_041(inter_pr_explode_estrutura(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                           end if;
                     end;
                  
                  -- Se n?o for registro duplicado, registra a mensagem de erro do insert acima
                  when others then
                     if p_tipo_msg = 0
                     then
                        inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#',
                                                     0);
                     else
                        raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                     end if;
               end;
            end if;
         end if;
      end if;
      
      -- Se a origem chamada for obrf, atualiza a quantidade a enviar da ordem de servico.
      if p_origem_chamada = 'obrf'
      then
         if p_fio_niv = '2' or p_fio_niv = '4'
         then
            p_qtde_reservada := p_relacao_banho_aux;
         else
            p_qtde_reservada := p_consumo050 * p_relacao_banho_aux;
         end if;

         begin
            update obrf_082
            set obrf_082.qtde_estrutura     = p_qtde_reservada,
                obrf_082.qtde_enviada       = p_qtde_reservada
            where obrf_082.numero_ordem     = p_pedido_ordem
              and obrf_082.seq_areceber     = p_seq_ordem
              and obrf_082.prodsai_nivel99  = p_fio_niv
              and obrf_082.prodsai_grupo    = p_fio_gru
              and obrf_082.prodsai_subgrupo = p_fio_sub
              and obrf_082.prodsai_item     = p_fio_ite;
         exception         when NO_DATA_FOUND then
            if p_tipo_msg = 0
            then
                   inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds24385', 'TMRP_082(inter_pr_explode_estrutura(4))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil  )||'#',
                                                1);
            else
               -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_082(inter_pr_explode_estrutura(4))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end if   ;
         end;
      end if;

      -- Explode a estrutura de estampas(3) e receitas(5) de tingimento
      if  p_fio_niv = '3'
      or  p_fio_niv = '5'
      or (p_fio_niv = '7' and p_nivel_explode = '2')
      then
         if p_origem_chamada = 'pcpb'
         or p_origem_chamada = 'pcpt'
         then
            -- Chama procedure novamente para explosao da estrutura dos componentes
            -- da receita.
            inter_pr_explode_estrutura(p_fio_niv,
                                       p_fio_gru,
                                       p_fio_sub,
                                       p_fio_ite,
                                       p_alternativa_comp,
                                       p_periodo_producao,
                                       p_qtde_reservada,
                                       p_pedido_ordem,
                                       p_seq_ordem,
                                       2,
                                       p_sequencia_item,
                                       0,
                                       p_estagio050,
                                       p_relacao_banho,
                                       p_codigo_embalagem,
                                       p_origem_chamada, p_tipo_msg);
         end if;

         if p_origem_chamada = 'obrf'
         then
            -- Chama procedure novamente para explosao da estrutura dos componentes
            -- da receita.
            inter_pr_explode_estrutura(p_fio_niv,
                                       p_fio_gru,
                                       p_fio_sub,
                                       p_fio_ite,
                                       p_alternativa_comp,
                                       p_periodo_producao,
                                       p_qtde_reservada,
                                       p_pedido_ordem,
                                       p_seq_ordem,
                                       2,
                                       p_sequencia_item,
                                       p_estagio050,
                                       0,
                                       p_relacao_banho_aux,
                                       p_codigo_embalagem,
                                       'obrf', p_tipo_msg);
         end if;
      end if;

      if  p_fio_niv = '3'
      or  p_fio_niv = '5'
      then
         if p_origem_chamada = 'pcpc'
         then
            -- Chama procedure novamente para explosao da estrutura dos componentes
            -- da receita.
            -- SS 53234-001 - Incluido consistencia abaixo pois a funcao de recalculo na leva em consideracao a relacao de banho
            -- para confeccao
            inter_pr_explode_estrutura(p_fio_niv,
                                       p_fio_gru,
                                       p_fio_sub,
                                       p_fio_ite,
                                       p_alternativa_comp,
                                       p_periodo_producao,
                                       p_qtde_quilos_prog,
                                       p_pedido_ordem,
                                       p_seq_ordem,
                                       1,
                                       p_sequencia_item,
                                       p_estagio050,
                                       p_est_baixa,
--                                       p_qtde_quilos_prog,
                                       0.00,
                                       p_qtde_reservada,
                                       'pcpc', p_tipo_msg);
         end if;
      end if;
   end loop;
end INTER_PR_EXPLODE_ESTRUTURA;

 

/

exec inter_pr_recompile;

