create or replace trigger inter_tr_supr_100_1
after insert or
      delete or
      update of cod_cancelamento, periodo_compras, data_prev_entr, qtde_saldo_item, situacao_item
   on supr_100
   for each row
  
declare
   var1                   number(1);
   v_executa_trigger      number;
   deletarRegistro        number;
   saldo_ped              supr_100.qtde_saldo_item%type;
   v_codigo_empresa       supr_090.codigo_empresa%type;
   v_ordem_servico        supr_090.ordem_servico%type;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   
   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   
   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   
   deletarRegistro := 0;

   if v_executa_trigger = 0
   then
      if inserting
      or updating
      then
         begin
            select supr_090.codigo_empresa, 
                   supr_090.ordem_servico
            into   v_codigo_empresa, 
                   v_ordem_servico
            from supr_090
            where supr_090.pedido_compra = :new.num_ped_compra;
         end;
         
         begin
            inter_pr_entr_doc_fiscal_item (v_codigo_empresa,
                                           :new.item_100_nivel99,
                                           :new.item_100_grupo,
                                           :new.item_100_subgrupo,
                                           :new.item_100_item);
         end;
      end if;
      
      if inserting
      then
         if  :new.item_100_nivel99 <> '0' -- Nivel valido.
         and :new.situacao_item    <> 4   -- Conforme recalculo planejamento.
         and :new.cod_cancelamento  = 0   -- O sistema permite cadastrar pedido de compra cancelado.
         and v_ordem_servico        = 0
         then begin
            insert into tmrp_041
                   (periodo_producao,              area_producao,
                    nr_pedido_ordem,               seq_pedido_ordem,
                    codigo_estagio,                nivel_estrutura,
                    grupo_estrutura,               subgru_estrutura,
                    item_estrutura,                qtde_reservada,
                    qtde_areceber,                 consumo,
                    data_requirida)
            values (:new.periodo_compras,          9,
                    :new.num_ped_compra,           :new.seq_item_pedido,
                    0,                             :new.item_100_nivel99,
                    :new.item_100_grupo,           :new.item_100_subgrupo,
                    :new.item_100_item,            0.000,
                    :new.qtde_saldo_item,          0.000,
                    :new.data_prev_entr);
            EXCEPTION
               WHEN Dup_Val_On_Index THEN
                  NULL;
               WHEN OTHERS
                 THEN var1 := 0;
            end;
         end if;
      end if; -- if inserting
      
      if updating
      then
         -- Se alterar a data de entrega do item do pedido de compras.
         if  :new.cod_cancelamento = 0
         and :new.data_prev_entr  <> :old.data_prev_entr
         then
            begin
               update tmrp_041
               set tmrp_041.data_requirida     = :new.data_prev_entr
               where tmrp_041.area_producao    = 9
                 and tmrp_041.nr_pedido_ordem  = :new.num_ped_compra
                 and tmrp_041.seq_pedido_ordem = :new.seq_item_pedido;
              exception
                 when no_data_found
                 then var1 := 0; -- Raise_application_error(-20100, 'NAO ENCONTREI O TMRP_041');
            end;
         end if;
         
         -- Se alterar o saldo do item do pedido de compras.
         if  :new.cod_cancelamento = 0
         and :new.qtde_saldo_item <> :old.qtde_saldo_item
         then
            if :new.qtde_saldo_item < 0
            then saldo_ped := 0.00;
            else saldo_ped := :new.qtde_saldo_item;
            end if;
            
            if v_ordem_servico = 0
            then
               begin
               update tmrp_041
               set tmrp_041.qtde_areceber = saldo_ped
               where tmrp_041.area_producao    = 9
                 and tmrp_041.nr_pedido_ordem  = :new.num_ped_compra
                 and tmrp_041.seq_pedido_ordem = :new.seq_item_pedido;
               if SQL%ROWCOUNT = 0 then
                  insert into tmrp_041
                         (periodo_producao,              area_producao,
                          nr_pedido_ordem,               seq_pedido_ordem,
                          codigo_estagio,                nivel_estrutura,
                          grupo_estrutura,               subgru_estrutura,
                          item_estrutura,                qtde_reservada,
                          qtde_areceber,                 consumo,
                          data_requirida)
                  values (:new.periodo_compras,          9,
                          :new.num_ped_compra,           :new.seq_item_pedido,
                          0,                             :new.item_100_nivel99,
                          :new.item_100_grupo,           :new.item_100_subgrupo,
                          :new.item_100_item,            0.000,
                          saldo_ped,                     0.000,
                          :new.data_prev_entr);
               end if;
               
               exception
                  when Dup_Val_On_Index then
                     null;
                  when others then
                     var1 := 0;
            end;

            end if;
         end if;
         
         -- Se alterar o periodo de producao do item do pedido de compras.
         if  :new.cod_cancelamento = 0
         and :new.periodo_compras <> :old.periodo_compras
         then
            begin
               update tmrp_041
               set tmrp_041.periodo_producao   = :new.periodo_compras
               where tmrp_041.area_producao    = 9
                 and tmrp_041.nr_pedido_ordem  = :new.num_ped_compra
                 and tmrp_041.seq_pedido_ordem = :new.seq_item_pedido;
              exception
                 when no_data_found
                 then var1 := 0; -- Raise_application_error(-20100, 'NAO ENCONTREI O TMRP_041');
                 WHEN Dup_Val_On_Index THEN
                  NULL;
            end;
         end if;
         
         -- Cancelamento do item do pedido de compra.
         if :new.cod_cancelamento <> 0
         then
            delete from tmrp_041
            where tmrp_041.area_producao    = 9
              and tmrp_041.nr_pedido_ordem  = :new.num_ped_compra
              and tmrp_041.seq_pedido_ordem = :new.seq_item_pedido;
         end if;
         
         -- Se atender total o item do pedido de compra.
         if :new.situacao_item = 3
         then
            delete from tmrp_041
            where tmrp_041.area_producao    = 9
              and tmrp_041.nr_pedido_ordem  = :new.num_ped_compra
              and tmrp_041.seq_pedido_ordem = :new.seq_item_pedido;
         end if;
         
         select count(1)
         into deletarRegistro
         from tmrp_041
         where tmrp_041.area_producao    = 9
           and tmrp_041.nr_pedido_ordem  = :new.num_ped_compra
           and tmrp_041.seq_pedido_ordem = :new.seq_item_pedido
           and tmrp_041.qtde_areceber   <= 0;
         
         if deletarRegistro > 0
         then
            begin
               delete from tmrp_041
               where tmrp_041.area_producao    = 9
                 and tmrp_041.nr_pedido_ordem  = :new.num_ped_compra
                 and tmrp_041.seq_pedido_ordem = :new.seq_item_pedido
                 and tmrp_041.qtde_areceber   <= 0;
            end;
         end if;
         
         -- CHAMA PROCEDURE QUE INSERE NA TABELA TEMPORARIA DO RECALCULO
         inter_pr_marca_referencia_op('0', '00000',
                                      '000', '00000',
                                      :new.num_ped_compra, 9);
      end if; -- if updating
      
      if deleting
      then
         delete from tmrp_041
         where tmrp_041.area_producao    = 9
           and tmrp_041.nr_pedido_ordem  = :old.num_ped_compra
           and tmrp_041.seq_pedido_ordem = :old.seq_item_pedido;
      end if; -- if deleting
   end if;

end inter_tr_supr_100_1;
