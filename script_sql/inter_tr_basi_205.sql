create or replace trigger "INTER_TR_BASI_205"
before update
of tipo_volume, endereca_caixa
on BASI_205
for each row
declare
   vTmp number;


begin

   if ((:new.tipo_volume = 1) and (:new.endereca_caixa = 1))
   then
      

      BEGIN
         select nvl(max(1),0)
         into vTmp
         from pcpc_330
         where pcpc_330.deposito = :new.codigo_deposito
           and pcpc_330.estoque_tag = 1
           and (pcpc_330.endereco is null or not exists (select 1 from estq_110
                                                        where estq_110.deposito = pcpc_330.deposito
                                                          and estq_110.endereco = pcpc_330.endereco
                                                          and (estq_110.nivel    = pcpc_330.nivel or estq_110.nivel = '0') 
                                                          and (estq_110.grupo    = pcpc_330.grupo or estq_110.grupo = '00000')
                                                          and (estq_110.subgrupo = pcpc_330.subgrupo or estq_110.subgrupo = '000')
                                                          and (estq_110.item     =  decode((select basi_030.cor_de_estoque
                                                                                            from basi_030
                                                                                            where basi_030.nivel_estrutura  = pcpc_330.nivel
                                                                                              and basi_030.referencia       = pcpc_330.grupo),'009999','009999',pcpc_330.item) 
                                                                                  or estq_110.item     = '000000')));
                                                          
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            vTmp:=0;
      END;

      if (vTmp>0)
      then
         raise_application_error(-20000,'ATENCAO! Para habilitar o enderecamento no deposito nao pode haver TAGs em estoque sem endereco.');
      end if;
   end if;
end INTER_TR_BASI_205;


/

exec inter_pr_recompile;
