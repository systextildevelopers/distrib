create table PEDI_307
( CNPJ9           NUMBER(9) default 0 not null,
  CNPJ4           NUMBER(4) default 0 not null,
  CNPJ2           NUMBER(2) default 0 not null,
  PESO_NOTA       NUMBER(1) default 1);

Comment on column PEDI_307.PESO_NOTA is '1-Peso te�rico - 2-Peso real.';

alter table PEDI_307 add constraint PK_PEDI_307 primary key (CNPJ9, CNPJ4, CNPJ2);

/
exec inter_pr_recompile;
