create or replace view inter_vi_pedidos_itens as
select pedi_100.tecido_peca,                   pedi_100.tipo_pedido,
       pedi_100.codigo_empresa,                pedi_100.tipo_prod_pedido,
       pedi_100.cod_rep_cliente,               pedi_100.data_emis_venda,
       pedi_100.data_digit_venda,              pedi_100.data_entr_venda,
       pedi_100.cli_ped_cgc_cli9,              pedi_100.cli_ped_cgc_cli4,
       pedi_100.cli_ped_cgc_cli2,              pedi_100.classificacao_pedido,
       pedi_100.tipo_comissao,                 pedi_100.pedido_venda,
       pedi_100.perc_comis_venda,              pedi_100.codigo_administr,
       pedi_100.comissao_administr,            pedi_100.colecao_tabela,
       pedi_100.mes_tabela,                    pedi_100.sequencia_tabela,
       pedi_100.codigo_moeda,                  pedi_100.cond_pgto_venda,
       pedi_100.cod_banco,                     pedi_100.tipo_desconto,
       pedi_100.desconto1,                     pedi_100.desconto2,
       pedi_100.desconto3,                     pedi_100.desconto_item1,
       pedi_100.desconto_item2,                pedi_100.desconto_item3,
       pedi_100.seq_end_entrega,               pedi_100.seq_end_cobranca,
       pedi_100.tipo_frete,                    pedi_100.cod_via_transp,
       pedi_100.trans_pv_forne9,               pedi_100.trans_pv_forne4,
       pedi_100.trans_pv_forne2,               pedi_100.trans_re_forne9,
       pedi_100.trans_re_forne4,               pedi_100.trans_re_forne2,
       pedi_100.data_canc_venda,               pedi_100.cod_cancelamento,
       pedi_100.cidade_cif,                    pedi_100.natop_pv_nat_oper,
       pedi_100.natop_pv_est_oper,             pedi_100.status_pedido,
       pedi_100.status_expedicao,              pedi_100.status_comercial,
       pedi_100.situacao_venda,                pedi_100.qtde_total_pedi,
       pedi_100.valor_total_pedi,              pedi_100.valor_liq_itens,
       pedi_100.qtde_saldo_pedi,               pedi_100.valor_saldo_pedi,
       pedi_100.cod_ped_cliente,               pedi_100.criterio_qualidade,
       pedi_110.seq_item_pedido,               pedi_110.cd_it_pe_nivel99,
       pedi_110.cd_it_pe_grupo,                pedi_110.cd_it_pe_subgrupo,
       pedi_110.cd_it_pe_item,                 pedi_110.qtde_pedida,
       pedi_110.qtde_faturada,                 pedi_110.qtde_afaturar,
       pedi_110.valor_unitario,                pedi_110.percentual_desc,
       pedi_110.situacao_fatu_it,              pedi_110.cod_cancelamento cod_canc_item,
       pedi_110.lote_empenhado,                pedi_110.codigo_deposito,
       pedi_100.observacao,                    pedi_100.prazo_extra,
       pedi_100.desconto_extra,                pedi_100.nr_solicitacao,
       pedi_100.cod_catalogo,                  pedi_100.aceita_antecipacao,
       pedi_100.permite_parcial,               pedi_010.grupo_economico,
       pedi_110.codigo_acomp,                  pedi_100.cod_proforma,
       pedi_100.cod_processo,				   pedi_100.cod_cargo_repre,
	   pedi_100.descr_cli_final
from pedi_110, pedi_100, pedi_010
where pedi_110.pedido_venda  = pedi_100.pedido_venda
  and pedi_100.cli_ped_cgc_cli9 = pedi_010.cgc_9
  and pedi_100.cli_ped_cgc_cli4 = pedi_010.cgc_4
  and pedi_100.cli_ped_cgc_cli2 = pedi_010.cgc_2;
