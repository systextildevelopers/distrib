
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_853_LOG" 
   after insert or
         delete or
         update of nivel_comp,       grupo_comp,
                   subgrupo_comp,    item_comp,
                   codigo_destino,   utiliza_componente
   on basi_853
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_cod_tipo_produto_n     varchar2(60);

   ws_tipo_produto_n         varchar2(60);

   ws_desc_projeto_n         varchar2(60);

   ws_desc_prod_n            varchar2(120);
   ws_desc_comp_o            varchar2(120);
   ws_desc_comp_n            varchar2(120);
   ws_desc_grupo_n           varchar2(30);
   ws_desc_subgrupo_n        varchar2(30);
   ws_desc_item_n            varchar2(30);

   ws_cnpj_cliente9          basi_001.cnpj_cliente9%type;
   ws_cnpj_cliente4          basi_001.cnpj_cliente4%type;
   ws_cnpj_cliente2          basi_001.cnpj_cliente2%type;
   ws_situacao_componente    basi_028.situacao_componente%type;
   ws_teve                   varchar2(1);

   long_aux                  varchar2(2000);
begin

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      -- ENCONTRA O TIPO_PRODUTO DA SEQUENCIA
      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = :new.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_n := '';
      end;

      -- DESCRICAO DO TIPO DE PRODUTO
      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n;
      exception when no_data_found then
         ws_tipo_produto_n := '';
      end;

     -- DESCRICAO DO PROJETO
      begin
         select basi_001.descricao_projeto,  basi_001.cnpj_cliente9,
                basi_001.cnpj_cliente4,      basi_001.cnpj_cliente2
         into ws_desc_projeto_n,             ws_cnpj_cliente9,
              ws_cnpj_cliente4,              ws_cnpj_cliente2
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_n := '';
         ws_cnpj_cliente9  := 0;
         ws_cnpj_cliente4  := 0;
         ws_cnpj_cliente2  := 0;
      end;

      -- DESCRICAO DO NOVO PRODUTO
      begin
         select nvl(basi_030.descr_referencia,' ')
         into ws_desc_grupo_n
         from basi_030
         where basi_030.nivel_estrutura = :new.nivel_item
           and basi_030.referencia      = :new.grupo_item;
      exception when no_data_found then
            ws_desc_grupo_n := ' ';
      end;

      begin
         select nvl(basi_020.descr_tam_refer,' ')
         into ws_desc_subgrupo_n
         from basi_020
         where basi_020.basi030_nivel030 = :new.nivel_item
           and basi_020.basi030_referenc = :new.grupo_item
           and basi_020.tamanho_ref      = :new.subgru_item;
      exception when no_data_found then
         ws_desc_subgrupo_n := ' ';
      end;

      begin
         select nvl(basi_010.descricao_15,' ')
         into ws_desc_item_n
         from basi_010
         where basi_010.nivel_estrutura  = :new.nivel_item
           and basi_010.grupo_estrutura  = :new.grupo_item
           and basi_010.subgru_estrutura = :new.subgru_item
           and basi_010.item_estrutura   = :new.item_item;
      exception when no_data_found then
         ws_desc_item_n := ' ';
      end;

      ws_desc_prod_n := ws_desc_grupo_n || ' ' || ws_desc_subgrupo_n  || ' ' || ws_desc_item_n;

      -- DESCRICAO DO NOVO COMPONENTE
      begin
         select nvl(basi_030.descr_referencia,' ')
         into ws_desc_grupo_n
         from basi_030
         where basi_030.nivel_estrutura = :new.nivel_comp
           and basi_030.referencia      = :new.grupo_comp;
      exception when no_data_found then
         ws_desc_grupo_n := ' ';
      end;

      begin
         select nvl(basi_020.descr_tam_refer,' ')
         into ws_desc_subgrupo_n
         from basi_020
         where basi_020.basi030_nivel030 = :new.nivel_comp
           and basi_020.basi030_referenc = :new.grupo_comp
           and basi_020.tamanho_ref      = :new.subgrupo_comp;
      exception when no_data_found then
         ws_desc_subgrupo_n := ' ';
      end;

      begin
         select nvl(basi_010.descricao_15,' ')
         into ws_desc_item_n
         from basi_010
         where basi_010.nivel_estrutura  = :new.nivel_comp
           and basi_010.grupo_estrutura  = :new.grupo_comp
           and basi_010.subgru_estrutura = :new.subgrupo_comp
           and basi_010.item_estrutura   = :new.item_comp;
      exception when no_data_found then
         ws_desc_item_n := ' ';
      end;

      ws_desc_comp_n := ws_desc_grupo_n || ' ' || ws_desc_subgrupo_n  || ' ' || ws_desc_item_n;

      -- DESCRICAO DO NOVO COMPONENTE - PRODUTO NIVEL 1
      begin
         select nvl(basi_030.descr_referencia,' ')
         into ws_desc_grupo_n
         from basi_030
         where basi_030.nivel_estrutura =  :new.nivel_item
           and basi_030.referencia      =  :new.grupo_item;
      exception when no_data_found then
         ws_desc_grupo_n := ' ';
      end;

      begin
         select nvl(basi_020.descr_tam_refer,' ')
         into ws_desc_subgrupo_n
         from basi_020
         where basi_020.basi030_nivel030 = :new.nivel_item
           and basi_020.basi030_referenc = :new.grupo_item
           and basi_020.tamanho_ref      = :new.subgru_item;
      exception when no_data_found then
         ws_desc_subgrupo_n := ' ';
      end;

      begin
         select nvl(basi_010.descricao_15,' ')
         into ws_desc_item_n
         from basi_010
         where basi_010.nivel_estrutura  = :new.nivel_item
           and basi_010.grupo_estrutura  = :new.grupo_item
           and basi_010.subgru_estrutura = :new.subgru_item
           and basi_010.item_estrutura   = :new.item_item;
      exception when no_data_found then
         ws_desc_item_n := ' ';
      end;

      ws_desc_prod_n := ws_desc_grupo_n || ' ' || ws_desc_subgrupo_n  || ' ' || ws_desc_item_n;

      -- Busca qual situacao do componente
        if :new.nivel_comp = '2'
      then
         begin
            select basi_028.situacao_componente
            into ws_situacao_componente
            from basi_028
            where basi_028.nivel_componente     = :new.nivel_comp
              and basi_028.grupo_componente     = :new.grupo_comp
              and basi_028.subgrupo_componente  = :new.subgrupo_comp
              and basi_028.item_componente      = :new.item_comp
              and basi_028.cnpj_cliente9        = ws_cnpj_cliente9
              and basi_028.cnpj_cliente4        = ws_cnpj_cliente4
              and basi_028.cnpj_cliente2        = ws_cnpj_cliente2
              and basi_028.tipo_aprovacao       = 1;   --APROVACAO POR COMPONENTE
         exception when no_data_found then
              ws_situacao_componente := 0;   -- 0 - indefinido 1 - definido 2 - aprovado 3 - reprovado
         end;
      else
         begin
            select basi_028.situacao_componente
            into ws_situacao_componente
            from basi_028
            where basi_028.nivel_componente     = :new.nivel_comp
              and basi_028.grupo_componente     = :new.grupo_comp
              and basi_028.subgrupo_componente  = :new.subgrupo_comp
              and basi_028.item_componente      = :new.item_comp
              and basi_028.codigo_projeto       = :new.codigo_projeto
              and basi_028.sequencia_projeto    = :new.sequencia_subprojeto
              and basi_028.tipo_aprovacao       = 1;   --APROVACAO POR COMPONENTE
         exception when no_data_found then
              ws_situacao_componente := 0;   -- 0 - indefinido 1 - definido 2 - aprovado 3 - reprovado
         end;
      end if;

      begin
         INSERT INTO hist_100 (
              tabela,             operacao,
              data_ocorr,         aplicacao,
              usuario_rede,       maquina_rede,
              str01,              num05,
              num07,              num08,
              num09,
              str07,              str08,
              str09,              str10,
              num06,
              long01
         ) VALUES (
              'BASI_013',         'I',
              sysdate,            ws_aplicativo,
              ws_usuario_rede,    ws_maquina_rede,
              :new.codigo_projeto, :new.sequencia_projeto,
              ws_cnpj_cliente9,
              ws_cnpj_cliente4,   ws_cnpj_cliente2,
              :new.nivel_comp,     :new.grupo_comp,
              :new.subgrupo_comp,    :new.item_comp,
              ws_situacao_componente,
              inter_fn_buscar_tag('lb34894#ESTRUTURA DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) || '(853)' ||
              chr(10)                                               ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || :new.codigo_projeto    ||
              ' - '                       || ws_desc_projeto_n      ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb08486#TIPO DE PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || :new.sequencia_projeto ||
              ' - '                       || ws_cod_tipo_produto_n  ||
              ' - '                       || ws_tipo_produto_n      ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || :new.nivel_item        ||
              '.'                         || :new.grupo_item        ||
              '.'                         || :new.subgru_item       ||
              '.'                         || :new.item_item         ||
              ' - '                       || ws_desc_prod_n         ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb04571#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || to_char(:new.alternativa_produto,'00')   ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb00523#COMPONENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || :new.nivel_comp        ||
              '.'                         || :new.grupo_comp        ||
              '.'                         || :new.subgrupo_comp       ||
              '.'                         || :new.item_comp         ||
              ' - '                       || ws_desc_comp_n         ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb34895#UTILIZA COMPONENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || :new.utiliza_componente ||
              chr(10)
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   if updating
   then
      -- ENCONTRA O TIPO_PRODUTO DA SEQUENCIA
      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = :new.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_n := '';
      end;

      -- DESCRICAO DO TIPO DE PRODUTO
      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n;
      exception when no_data_found then
         ws_tipo_produto_n := '';
      end;

     -- DESCRICAO DO PROJETO
      begin
         select basi_001.descricao_projeto,  basi_001.cnpj_cliente9,
                basi_001.cnpj_cliente4,      basi_001.cnpj_cliente2
         into ws_desc_projeto_n,             ws_cnpj_cliente9,
              ws_cnpj_cliente4,              ws_cnpj_cliente2
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_n := '';
         ws_cnpj_cliente9  := 0;
         ws_cnpj_cliente4  := 0;
         ws_cnpj_cliente2  := 0;
      end;

      -- DESCRICAO DO NOVO COMPONENTE - OLD
      begin
         select nvl(basi_030.descr_referencia,' ')
         into ws_desc_grupo_n
         from basi_030
         where basi_030.nivel_estrutura = :old.nivel_comp
           and basi_030.referencia      = :old.grupo_comp;
      exception when no_data_found then
         ws_desc_grupo_n := ' ';
      end;

      begin
         select nvl(basi_020.descr_tam_refer,' ')
         into ws_desc_subgrupo_n
         from basi_020
         where basi_020.basi030_nivel030 = :old.nivel_comp
           and basi_020.basi030_referenc = :old.grupo_comp
           and basi_020.tamanho_ref      = :old.subgrupo_comp;
      exception when no_data_found then
         ws_desc_subgrupo_n := ' ';
      end;

      begin
         select nvl(basi_010.descricao_15,' ')
         into ws_desc_item_n
         from basi_010
         where basi_010.nivel_estrutura  = :old.nivel_comp
           and basi_010.grupo_estrutura  = :old.grupo_comp
           and basi_010.subgru_estrutura = :old.subgrupo_comp
           and basi_010.item_estrutura   = :old.item_comp;
      exception when no_data_found then
         ws_desc_item_n := ' ';
      end;

      ws_desc_comp_o := ws_desc_grupo_n || ' ' || ws_desc_subgrupo_n  || ' ' || ws_desc_item_n;

      -- DESCRICAO DO NOVO COMPONENTE - NOVO
      begin
         select nvl(basi_030.descr_referencia,' ')
         into ws_desc_grupo_n
         from basi_030
         where basi_030.nivel_estrutura = :new.nivel_comp
           and basi_030.referencia      = :new.grupo_comp;
      exception when no_data_found then
         ws_desc_grupo_n := ' ';
      end;

      begin
         select nvl(basi_020.descr_tam_refer,' ')
         into ws_desc_subgrupo_n
         from basi_020
         where basi_020.basi030_nivel030 = :new.nivel_comp
           and basi_020.basi030_referenc = :new.grupo_comp
           and basi_020.tamanho_ref      = :new.subgrupo_comp;
      exception when no_data_found then
         ws_desc_subgrupo_n := ' ';
      end;

      begin
         select nvl(basi_010.descricao_15,' ')
         into ws_desc_item_n
         from basi_010
         where basi_010.nivel_estrutura  = :new.nivel_comp
           and basi_010.grupo_estrutura  = :new.grupo_comp
           and basi_010.subgru_estrutura = :new.subgrupo_comp
           and basi_010.item_estrutura   = :new.item_comp;
      exception when no_data_found then
         ws_desc_item_n := ' ';
      end;

      ws_desc_comp_n := ws_desc_grupo_n || ' ' || ws_desc_subgrupo_n  || ' ' || ws_desc_item_n;

      -- DESCRICAO DO NOVO COMPONENTE - PRODUTO NIVEL 1
      begin
         select nvl(basi_030.descr_referencia,' ')
         into ws_desc_grupo_n
         from basi_030
         where basi_030.nivel_estrutura =  :new.nivel_item
           and basi_030.referencia      =  :new.grupo_item;
      exception when no_data_found then
         ws_desc_grupo_n := ' ';
      end;

      begin
         select nvl(basi_020.descr_tam_refer,' ')
         into ws_desc_subgrupo_n
         from basi_020
         where basi_020.basi030_nivel030 = :new.nivel_item
           and basi_020.basi030_referenc = :new.grupo_item
           and basi_020.tamanho_ref      = :new.subgru_item;
      exception when no_data_found then
         ws_desc_subgrupo_n := ' ';
      end;

      begin
         select nvl(basi_010.descricao_15,' ')
         into ws_desc_item_n
         from basi_010
         where basi_010.nivel_estrutura  = :new.nivel_item
           and basi_010.grupo_estrutura  = :new.grupo_item
           and basi_010.subgru_estrutura = :new.subgru_item
           and basi_010.item_estrutura   = :new.item_item;
      exception when no_data_found then
         ws_desc_item_n := ' ';
      end;

      ws_desc_prod_n := ws_desc_grupo_n || ' ' || ws_desc_subgrupo_n  || ' ' || ws_desc_item_n;

      ws_teve := 'n';

      ws_teve := 'n';

      long_aux := long_aux ||
           inter_fn_buscar_tag('lb34894#ESTRUTURA DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) || '(853)' ||
           chr(10)                                               ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.codigo_projeto    ||
           ' - '                       || ws_desc_projeto_n      ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb08486#TIPO DE PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_projeto ||
           ' - '                       || ws_cod_tipo_produto_n  ||
           ' - '                       || ws_tipo_produto_n      ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.nivel_item        ||
           '.'                         || :new.grupo_item        ||
           '.'                         || :new.subgru_item       ||
           '.'                         || :new.item_item         ||
           ' - '                       || ws_desc_prod_n         ||
           chr(10);

      if (:old.nivel_comp  <> :new.nivel_comp)  or
         (:old.grupo_comp  <> :new.grupo_comp)  or
         (:old.subgrupo_comp <> :new.subgrupo_comp) or
         (:old.item_comp   <> :new.item_comp)
      then
         ws_teve := 's';
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00523#COMPONENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                     || :old.nivel_comp        ||
         '.'                         || :old.grupo_comp        ||
         '.'                         || :old.subgrupo_comp       ||
         '.'                         || :old.item_comp         ||
         ' - '                       || ws_desc_comp_o         ||
         '-> '                       || :new.nivel_comp        ||
         '.'                         || :new.grupo_comp        ||
         '.'                         || :new.subgrupo_comp       ||
         '.'                         || :new.item_comp         ||
         ' - '                       || ws_desc_comp_n         ||
         chr(10);
      end if;

      if :old.utiliza_componente <> :new.utiliza_componente
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb34895#UTILIZA COMPONENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                     || :old.utiliza_componente  ||
         ' -> '                      || :new.utiliza_componente  ||
         chr(10);
      end if;

      if ws_teve = 's'
      then
         begin
            INSERT INTO hist_100
               ( tabela,              operacao,
                 data_ocorr,          aplicacao,
                 usuario_rede,        maquina_rede,
                 str01,               num05,
                 num07,               num08,
                 num09,
                 str07,               str08,
                 str09,               str10,
                 num06,
                 long01
               )
            VALUES
               ( 'BASI_013',          'A',
                 sysdate,             ws_aplicativo,
                 ws_usuario_rede,     ws_maquina_rede,
                 :new.codigo_projeto, :new.sequencia_projeto,
                 ws_cnpj_cliente9,    ws_cnpj_cliente4,
                 ws_cnpj_cliente2,
                 :new.nivel_comp,     :new.grupo_comp,
                 :new.subgrupo_comp,    :new.item_comp,
                 ws_situacao_componente,
                 long_aux
            );
         exception when OTHERS then
            raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(2)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;

   if deleting
   then
      begin
         INSERT INTO hist_100
            ( tabela,              operacao,
              data_ocorr,          aplicacao,
              usuario_rede,        maquina_rede,
              str01,               num05,
              num07,               num08,
              num09,
              str07,               str08,
              str09,               str10
            )
         VALUES
            ( 'BASI_013',          'D',
              sysdate,             ws_aplicativo,
              ws_usuario_rede,     ws_maquina_rede,
              :old.codigo_projeto, :old.sequencia_projeto,
              ws_cnpj_cliente9,      ws_cnpj_cliente4,
              ws_cnpj_cliente2,
              :old.nivel_comp,       :old.grupo_comp,
              :old.subgrupo_comp,    :old.item_comp
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(3)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;
end inter_tr_basi_853_log;

-- ALTER TRIGGER "INTER_TR_BASI_853_LOG" ENABLE
 

/

exec inter_pr_recompile;

