create or replace trigger inter_tr_PCPC_090_log 
after insert or delete or update 
on PCPC_090 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    begin 
       select hdoc_090.programa 
       into v_nome_programa 
       from hdoc_090 
       where hdoc_090.sid = ws_sid 
         and rownum       = 1 
         and hdoc_090.programa not like '%menu%' 
         and hdoc_090.programa not like '%_m%'; 
       exception 
         when no_data_found then            v_nome_programa := 'SQL'; 
    end; 
 
 
 
 if inserting 
 then 
    begin 
 
        insert into PCPC_090_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           ordem_producao_OLD,   /*8*/ 
           ordem_producao_NEW,   /*9*/ 
           sequencia_enfesto_OLD,   /*10*/ 
           sequencia_enfesto_NEW,   /*11*/ 
           ordem_confeccao_OLD,   /*12*/ 
           ordem_confeccao_NEW,   /*13*/ 
           sequencia_parte_OLD,   /*14*/ 
           sequencia_parte_NEW,   /*15*/ 
           codigo_estagio_OLD,   /*16*/ 
           codigo_estagio_NEW,   /*17*/ 
           descr_parte_peca_OLD,   /*18*/ 
           descr_parte_peca_NEW,   /*19*/ 
           etiqueta_impressa_OLD,   /*20*/ 
           etiqueta_impressa_NEW,   /*21*/ 
           controle_na_entrada_saida_OLD,   /*22*/ 
           controle_na_entrada_saida_NEW,   /*23*/ 
           data_conferencia_parte_OLD,   /*24*/ 
           data_conferencia_parte_NEW,   /*25*/ 
           hora_conferencia_parte_OLD,   /*26*/ 
           hora_conferencia_parte_NEW,   /*27*/ 
           usuario_conferencia_parte_OLD,   /*28*/ 
           usuario_conferencia_parte_NEW,   /*29*/ 
           data_entrada_estagio_OLD,   /*30*/ 
           data_entrada_estagio_NEW,   /*31*/ 
           hora_entrada_estagio_OLD,   /*32*/ 
           hora_entrada_estagio_NEW,   /*33*/ 
           usuario_entrada_estagio_OLD,   /*34*/ 
           usuario_entrada_estagio_NEW,   /*35*/ 
           data_saida_estagio_OLD,   /*36*/ 
           data_saida_estagio_NEW,   /*37*/ 
           hora_saida_estagio_OLD,   /*38*/ 
           hora_saida_estagio_NEW,   /*39*/ 
           usuario_saida_estagio_OLD,   /*40*/ 
           usuario_saida_estagio_NEW    /*41*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.ordem_producao, /*9*/   
           0,/*10*/
           :new.sequencia_enfesto, /*11*/   
           0,/*12*/
           :new.ordem_confeccao, /*13*/   
           0,/*14*/
           :new.sequencia_parte, /*15*/   
           0,/*16*/
           :new.codigo_estagio, /*17*/   
           '',/*18*/
           :new.descr_parte_peca, /*19*/   
           0,/*20*/
           :new.etiqueta_impressa, /*21*/   
           0,/*22*/
           :new.controle_na_entrada_saida, /*23*/   
           null,/*24*/
           :new.data_conferencia_parte, /*25*/   
           null,/*26*/
           :new.hora_conferencia_parte, /*27*/   
           '',/*28*/
           :new.usuario_conferencia_parte, /*29*/   
           null,/*30*/
           :new.data_entrada_estagio, /*31*/   
           null,/*32*/
           :new.hora_entrada_estagio, /*33*/   
           '',/*34*/
           :new.usuario_entrada_estagio, /*35*/   
           null,/*36*/
           :new.data_saida_estagio, /*37*/   
           null,/*38*/
           :new.hora_saida_estagio, /*39*/   
           '',/*40*/
           :new.usuario_saida_estagio /*41*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into PCPC_090_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           ordem_producao_OLD, /*8*/   
           ordem_producao_NEW, /*9*/   
           sequencia_enfesto_OLD, /*10*/   
           sequencia_enfesto_NEW, /*11*/   
           ordem_confeccao_OLD, /*12*/   
           ordem_confeccao_NEW, /*13*/   
           sequencia_parte_OLD, /*14*/   
           sequencia_parte_NEW, /*15*/   
           codigo_estagio_OLD, /*16*/   
           codigo_estagio_NEW, /*17*/   
           descr_parte_peca_OLD, /*18*/   
           descr_parte_peca_NEW, /*19*/   
           etiqueta_impressa_OLD, /*20*/   
           etiqueta_impressa_NEW, /*21*/   
           controle_na_entrada_saida_OLD, /*22*/   
           controle_na_entrada_saida_NEW, /*23*/   
           data_conferencia_parte_OLD, /*24*/   
           data_conferencia_parte_NEW, /*25*/   
           hora_conferencia_parte_OLD, /*26*/   
           hora_conferencia_parte_NEW, /*27*/   
           usuario_conferencia_parte_OLD, /*28*/   
           usuario_conferencia_parte_NEW, /*29*/   
           data_entrada_estagio_OLD, /*30*/   
           data_entrada_estagio_NEW, /*31*/   
           hora_entrada_estagio_OLD, /*32*/   
           hora_entrada_estagio_NEW, /*33*/   
           usuario_entrada_estagio_OLD, /*34*/   
           usuario_entrada_estagio_NEW, /*35*/   
           data_saida_estagio_OLD, /*36*/   
           data_saida_estagio_NEW, /*37*/   
           hora_saida_estagio_OLD, /*38*/   
           hora_saida_estagio_NEW, /*39*/   
           usuario_saida_estagio_OLD, /*40*/   
           usuario_saida_estagio_NEW  /*41*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.ordem_producao,  /*8*/  
           :new.ordem_producao, /*9*/   
           :old.sequencia_enfesto,  /*10*/  
           :new.sequencia_enfesto, /*11*/   
           :old.ordem_confeccao,  /*12*/  
           :new.ordem_confeccao, /*13*/   
           :old.sequencia_parte,  /*14*/  
           :new.sequencia_parte, /*15*/   
           :old.codigo_estagio,  /*16*/  
           :new.codigo_estagio, /*17*/   
           :old.descr_parte_peca,  /*18*/  
           :new.descr_parte_peca, /*19*/   
           :old.etiqueta_impressa,  /*20*/  
           :new.etiqueta_impressa, /*21*/   
           :old.controle_na_entrada_saida,  /*22*/  
           :new.controle_na_entrada_saida, /*23*/   
           :old.data_conferencia_parte,  /*24*/  
           :new.data_conferencia_parte, /*25*/   
           :old.hora_conferencia_parte,  /*26*/  
           :new.hora_conferencia_parte, /*27*/   
           :old.usuario_conferencia_parte,  /*28*/  
           :new.usuario_conferencia_parte, /*29*/   
           :old.data_entrada_estagio,  /*30*/  
           :new.data_entrada_estagio, /*31*/   
           :old.hora_entrada_estagio,  /*32*/  
           :new.hora_entrada_estagio, /*33*/   
           :old.usuario_entrada_estagio,  /*34*/  
           :new.usuario_entrada_estagio, /*35*/   
           :old.data_saida_estagio,  /*36*/  
           :new.data_saida_estagio, /*37*/   
           :old.hora_saida_estagio,  /*38*/  
           :new.hora_saida_estagio, /*39*/   
           :old.usuario_saida_estagio,  /*40*/  
           :new.usuario_saida_estagio  /*41*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into PCPC_090_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           ordem_producao_OLD, /*8*/   
           ordem_producao_NEW, /*9*/   
           sequencia_enfesto_OLD, /*10*/   
           sequencia_enfesto_NEW, /*11*/   
           ordem_confeccao_OLD, /*12*/   
           ordem_confeccao_NEW, /*13*/   
           sequencia_parte_OLD, /*14*/   
           sequencia_parte_NEW, /*15*/   
           codigo_estagio_OLD, /*16*/   
           codigo_estagio_NEW, /*17*/   
           descr_parte_peca_OLD, /*18*/   
           descr_parte_peca_NEW, /*19*/   
           etiqueta_impressa_OLD, /*20*/   
           etiqueta_impressa_NEW, /*21*/   
           controle_na_entrada_saida_OLD, /*22*/   
           controle_na_entrada_saida_NEW, /*23*/   
           data_conferencia_parte_OLD, /*24*/   
           data_conferencia_parte_NEW, /*25*/   
           hora_conferencia_parte_OLD, /*26*/   
           hora_conferencia_parte_NEW, /*27*/   
           usuario_conferencia_parte_OLD, /*28*/   
           usuario_conferencia_parte_NEW, /*29*/   
           data_entrada_estagio_OLD, /*30*/   
           data_entrada_estagio_NEW, /*31*/   
           hora_entrada_estagio_OLD, /*32*/   
           hora_entrada_estagio_NEW, /*33*/   
           usuario_entrada_estagio_OLD, /*34*/   
           usuario_entrada_estagio_NEW, /*35*/   
           data_saida_estagio_OLD, /*36*/   
           data_saida_estagio_NEW, /*37*/   
           hora_saida_estagio_OLD, /*38*/   
           hora_saida_estagio_NEW, /*39*/   
           usuario_saida_estagio_OLD, /*40*/   
           usuario_saida_estagio_NEW /*41*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.ordem_producao, /*8*/   
           0, /*9*/
           :old.sequencia_enfesto, /*10*/   
           0, /*11*/
           :old.ordem_confeccao, /*12*/   
           0, /*13*/
           :old.sequencia_parte, /*14*/   
           0, /*15*/
           :old.codigo_estagio, /*16*/   
           0, /*17*/
           :old.descr_parte_peca, /*18*/   
           '', /*19*/
           :old.etiqueta_impressa, /*20*/   
           0, /*21*/
           :old.controle_na_entrada_saida, /*22*/   
           0, /*23*/
           :old.data_conferencia_parte, /*24*/   
           null, /*25*/
           :old.hora_conferencia_parte, /*26*/   
           null, /*27*/
           :old.usuario_conferencia_parte, /*28*/   
           '', /*29*/
           :old.data_entrada_estagio, /*30*/   
           null, /*31*/
           :old.hora_entrada_estagio, /*32*/   
           null, /*33*/
           :old.usuario_entrada_estagio, /*34*/   
           '', /*35*/
           :old.data_saida_estagio, /*36*/   
           null, /*37*/
           :old.hora_saida_estagio, /*38*/   
           null, /*39*/
           :old.usuario_saida_estagio, /*40*/   
           '' /*41*/
         );    
    end;    
 end if;    
end inter_tr_PCPC_090_log;
