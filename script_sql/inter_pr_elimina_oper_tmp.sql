
CREATE OR REPLACE PROCEDURE "INTER_PR_ELIMINA_OPER_TMP" is

nro_registro number;

begin
   nro_registro := 0;
   for reg in (select * from oper_tmp
               where data_criacao < trunc(sysdate,'DD') - 2
                 and nome_relatorio not in ('estq_e120','estq_f445'))
   loop
     begin
       delete oper_tmp 
       where oper_tmp.nr_solicitacao = reg.nr_solicitacao
         and oper_tmp.nome_relatorio = reg.nome_relatorio
         and oper_tmp.sequencia      = reg.sequencia;
     end;
     
     nro_registro := nro_registro + 1;
     
     if nro_registro > 1000
     then 
       commit;
       nro_registro := 0;
     end if;
   end loop;
   
   commit;
   
   nro_registro := 0;
   
   for reg2 in (select nr_solicitacao,   codigo_usuario, nome_programa,
                       sequencia,        nivel_produto,  grupo_produto,
                       subgrupo_produto, item_produto  
				from oper_bin
                where data_insercao < trunc(sysdate,'DD') - 2)
   loop
     begin
       delete oper_bin
       where oper_bin.nr_solicitacao   = reg2.nr_solicitacao
         and oper_bin.codigo_usuario   = reg2.codigo_usuario
         and oper_bin.nome_programa    = reg2.nome_programa
         and oper_bin.sequencia        = reg2.sequencia
         and oper_bin.nivel_produto    = reg2.nivel_produto
         and oper_bin.grupo_produto    = reg2.grupo_produto
         and oper_bin.subgrupo_produto = reg2.subgrupo_produto
         and oper_bin.item_produto     = reg2.item_produto;
     end;
     
     nro_registro := nro_registro + 1;
     
     if nro_registro > 1000
     then 
       commit;
       nro_registro := 0;
     end if;
     
   end loop;

   commit;

end inter_pr_elimina_oper_tmp;

 

/

exec inter_pr_recompile;

