create or replace package ST_PCK_MQOP as

    function fnc_set_Parada_Maquina  (v_codigoEmpresa number
                                        ,v_codigoParada number
                                        ,v_apontamento varchar2
                                        ,v_maqGru varchar2
                                        ,v_maqSub varchar2
                                        ,v_maqNum number
                                        ,v_numeroFusos number
                                        ,v_codigoOperador number
                                        ,v_areaResponsavel number
                                        ,v_velocidade number
                                        ) return varchar2;

end ST_PCK_MQOP;
