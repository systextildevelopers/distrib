alter table pcpt_021_log
modify estagio_op_new number(5,0);

alter table pcpt_021_log
modify estagio_op_old number(5,0);

alter table pcpt_021_log
modify seq_rolada_new number(3,0);

alter table pcpt_021_log
modify seq_rolada_old number(3,0);
