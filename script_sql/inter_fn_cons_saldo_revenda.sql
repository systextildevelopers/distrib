CREATE OR REPLACE FUNCTION inter_fn_cons_saldo_revenda(p_cod_empresa number,
                                                       p_nivel varchar2,
                                                       p_grupo varchar2,
                                                       p_subgrupo varchar2,
                                                       p_item varchar2) RETURN number IS
    v_sldo_disp NUMBER;
BEGIN
    SELECT sum(nvl((e.saldo_qtde - e.qtde_empenhada),0))
    into v_sldo_disp
    FROM estq_308 e
    WHERE e.cod_empresa      = p_cod_empresa
      and e.nivel_estrutura  = p_nivel
      and e.grupo_estrutura  = p_grupo
      and e.subgru_estrutura = p_subgrupo
      and e.item_estrutura   = p_item;

      RETURN v_sldo_disp;

END;
