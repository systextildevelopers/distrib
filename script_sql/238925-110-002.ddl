drop type t_array_estagios;
drop type t_estagio_rot_pacote;

CREATE TYPE t_estagio_rot_pacote is object(
    codigo_estagio              number(5),      --Codigo do estagio (PK no array)

    --Informacoes do roteiro
    sequencia_estagio           number(4),
    estagio_anterior            number(5),
    estagio_depende             number(5),
    seq_operacao                number(4),
    codigo_operacao             number(5),
    centro_custo                number(6),
    ccusto_homem                number(6),

    empresa_centro_custo        number(3),      --Do centro custo maquina ou do centro de custo homem

    --Informacoes de qual roteiro
    nivel                       varchar2(1),
    grupo                       varchar2(5),
    subgrupo                    varchar2(3),
    item                        varchar2(6),
    alternativa                 number(2),
    roteiro                     number(2),

    --Informacoes de qual pacote
    ordem_producao              number(9),
    periodo_producao            number(4),
    ordem_confeccao             number(5),



    --Informacoes para as regras
    estagio_agrupador_ant       number(5),      --Estagio agrupador que estava na tabela do banco de dados antes da execucao atual
    estagio_agrupador_definido  number(5),      --Estagio agrupador que foi definido durante a execucao atual
    regra_est_agrup_definido    number(2),      --Qual numero da regra utilizada para definir o estagio agrupador na execucao atual

    tem_consumo_regra           number(1),      --O estagio tem consumo? (cfm regra de consumo definida)
    tem_consumo_falso           number(1),      --O estagio tem consumo falso na estrutura (Regra: Componente nivel 2 para o estagio que nao eh estagio do corte)

    seq_est_simult_sem_depende  number(1),      --Tem mais de 1 estagio na sequencia de estagio dele e nao possui estagio depende (regras 02, 12 e 13)
    seq_est_simult_com_depende  number(1),      --Tem mais de 1 estagio na sequencia de estagio dele mas possui estagio depende (regras 01, 18, 19, 20, 02, 03, 04, 05)
                                            
                                                --Se seq_est_simult_sem_depende e seq_est_simult_com_depende FOREM FALSOS, entao nao esta em um simultaneo (regras 02, 06, 07, 08, 09, 10, 11)
    
    estagio_eh_entregue_seq     number(1)       --O estagio eh entregue pela sequencia de estagio simultanea que tem depende?
);
/

CREATE TYPE t_array_estagios AS TABLE OF t_estagio_rot_pacote;
/

exec inter_pr_recompile;
/
