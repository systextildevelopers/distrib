create or replace package body ST_PCK_CIDADE is

FUNCTION get_dados_cod_cidade(p_cod_cidade number, p_msg_erro in out varchar2) return t_dados_basi_160
AS 
    v_dados_basi_160 t_dados_basi_160;
BEGIN
    begin
        SELECT *  
        BULK COLLECT 
        INTO v_dados_basi_160
        FROM basi_160
        WHERE cod_cidade = p_cod_cidade;
    exception when others then
        p_msg_erro := 'Não foi possível buscar dados para o código da cidade!. p_cod_cidade:' || p_cod_cidade;
        return null;
    end;

    return v_dados_basi_160;

END;

end ST_PCK_CIDADE;
