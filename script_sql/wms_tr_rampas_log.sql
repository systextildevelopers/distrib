create or replace trigger wms_tr_rampas_log
  before insert or delete on inte_wms_rampas
  for each row
declare
  -- local variables here
  v_cgc9_transp                    inte_wms_rampas.cgc9_transp                   %type;
  v_cgc4_transp                    inte_wms_rampas.cgc4_transp                   %type;
  v_cgc2_transp                    inte_wms_rampas.cgc2_transp                   %type;
  v_nr_rampa                       inte_wms_rampas.nr_rampa                      %type;
  v_timestamp_aimportar            inte_wms_rampas.timestamp_aimportar           %type;

  v_sid                            number(9);
  v_empresa                        number(3);
  v_usuario_systextil              varchar2(250);
  v_locale_usuario                 varchar2(5);
  v_nome_programa                  varchar2(20);
 

  v_operacao                       varchar(1);
  v_data_operacao                  date;
  v_usuario_rede                   varchar(20);
  v_maquina_rede                   varchar(40);
  v_aplicativo                     varchar(20);
begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();

   --alimenta as variaveis new caso seja insert
   if inserting
   then
      v_operacao := 'i';

      v_cgc9_transp          := :new.cgc9_transp;
      v_cgc4_transp          := :new.cgc4_transp;
      v_cgc2_transp          := :new.cgc2_transp;
      v_nr_rampa             := :new.nr_rampa;
      v_timestamp_aimportar  := :new.timestamp_aimportar;

   end if; --fim do if inserting or updating

   --alimenta as variaveis old caso seja insert ou update
   if deleting
   then
      v_operacao      := 'd';

      v_cgc9_transp          := :old.cgc9_transp;
      v_cgc4_transp          := :old.cgc4_transp;
      v_cgc2_transp          := :old.cgc2_transp;
      v_nr_rampa             := :old.nr_rampa;
      v_timestamp_aimportar  := :old.timestamp_aimportar;

   end if; --fim do if deleting or updating


   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);


    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente

   --insere na inte_wms_rfid_log o registro.
   insert into inte_wms_rampas_log (
      cgc9_transp,             cgc4_transp,
      cgc2_transp,             nr_rampa,
      timestamp_aimportar,
      operacao,
      data_operacao,           usuario_rede,
      maquina_rede,            aplicativo,
      nome_programa
   )
   values (
      v_cgc9_transp,           v_cgc4_transp,
      v_cgc2_transp,           v_nr_rampa,
      v_timestamp_aimportar,
      v_operacao,
      v_data_operacao,         v_usuario_rede,
      v_maquina_rede,          v_aplicativo,
      v_nome_programa
   );

end wms_tr_rampas_log;
/

execute inter_pr_recompile;

/* versao: 1 */


 exit;
