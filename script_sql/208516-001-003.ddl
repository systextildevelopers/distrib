create table MQOP_012
(
  maq_sbgr_grupo_mq         VARCHAR2(4) default ' ' not null,
  maq_sbgr_sbgr_maq         VARCHAR2(3) default ' ' not null,
  numero_maquina            NUMBER(5),
  pista_disco1              VARCHAR2(60),
  pista_disco2              VARCHAR2(60),
  pista_cilindro1           VARCHAR2(60),
  pista_cilindro2           VARCHAR2(60),
  camos_disco1              VARCHAR2(120),
  camos_disco2              VARCHAR2(120),
  camos_cilindro1           VARCHAR2(120),
  camos_cilindro2           VARCHAR2(120),
  camos_cilindro3           VARCHAR2(120),
  camos_cilindro4           VARCHAR2(120),
  camos_cilindro5           VARCHAR2(120),
  status_agulheiro          NUMBER(5)
);

alter table mqop_012 add constraint pk_mqop_012 primary key (maq_sbgr_grupo_mq, maq_sbgr_sbgr_maq, numero_maquina);

/
exec inter_pr_recompile;
