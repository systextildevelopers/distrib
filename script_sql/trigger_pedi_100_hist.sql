create or replace TRIGGER "TRIGGER_PEDI_100_HIST" 
before update of
    cod_preposto
    ,aceita_antecipacao
    ,permite_parcial
    ,usuario_cadastro
    ,cli9resptit
    ,cli4resptit
    ,cli2resptit
    ,desconto_item1
    ,desconto_item2
    ,desconto_item3
    ,observacao
    ,calculo_comissao
    ,coleta_gerada
    ,tipo_pedido
    ,criterio_pedido
    ,status_expedicao
    ,obs_nota_fiscal
    ,tipo_comissao
    ,encargos
    ,codigo_administr
    ,comissao_administr
    ,expedidor
    ,data_sugestao
    ,tipo_prod_pedido
    ,promocao
    ,status_homologacao
    ,bloqueio_rolo
    ,status_pedido
    ,com_criterio_empenhado
    ,volumes_tecido
    ,volumes_acomp
    ,obs_producao
    ,nome_coletor
    ,classificacao_pedido
    ,tp_frete_redesp
    ,status_comercial
    ,valor_despesas_pedido
    ,valor_frete_pedido
    ,valor_seguro_pedido
    ,nr_sugestao
    ,controle
    ,fatura_comercial
    ,liquida_saldo_pedido
    ,sugestao_impressa
    ,coleta_caixa_aberta
    ,data_liber_exped
    ,hora_liber_exped
    ,pedido_origem_desdobr
    ,exigencia_requisito_ambiental
    ,especificar_requisito
    ,criterio_qualidade
    ,data_entr_plan
    ,data_entr_interna
    ,executa_trigger
    ,data_prev_receb
    ,id_pedido_forca_vendas
    ,cod_funcionario
    ,desconto_especial
    ,incoterm
    ,cod_negociacao
    ,prazo_extra
    ,desconto_extra
    ,data_original_entrega
    ,colecao
    ,pedido_operativo
    ,flag_marcado
    ,nr_solicitacao
    ,cod_local
    ,prioridade
    ,liberado_coletar
    ,liberado_faturar
    ,data_hora_distribuicao
    ,coletor
    ,cod_catalogo
    ,natop_pv_nat_oper
    ,cod_cancelamento
    ,valor_saldo_pedi
    ,seq_recebimento
    ,numero_semana
    ,bonus_comissao
    ,pedido_venda
    ,tecido_peca
    ,data_emis_venda
    ,data_entr_venda
    ,cli_ped_cgc_cli9
    ,cli_ped_cgc_cli4
    ,cli_ped_cgc_cli2
    ,cod_rep_cliente
    ,perc_comis_venda
    ,cod_ped_cliente
    ,colecao_tabela
    ,mes_tabela
    ,sequencia_tabela
    ,codigo_moeda
    ,cond_pgto_venda
    ,desconto1
    ,desconto2
    ,desconto3
    ,seq_end_entrega
    ,seq_end_cobranca
    ,trans_pv_forne9
    ,trans_pv_forne4
    ,trans_pv_forne2
    ,trans_re_forne9
    ,trans_re_forne4
    ,trans_re_forne2
    ,cod_via_transp
    ,frete_venda
    ,cidade_cif
    ,cod_banco
    ,natop_pv_est_oper
    ,numero_controle
    ,num_periodo_prod
    ,codigo_empresa
    ,data_canc_venda
    ,situacao_venda
    ,data_digit_venda
    ,data_liberacao
    ,data_base_fatur
    ,qtde_total_pedi
	,valor_total_pedi
    ,qtde_saldo_pedi
    ,pedido_sujerido
    ,sit_aloc_pedi
	,valor_liq_itens
    ,perc_desc_duplic
    ,codigo_vendedor
    ,perc_comis_vendedor
    ,tipo_desconto
    ,tipo_frete
    ,origem_pedido
    ,sit_coletor
    ,numero_carga
    ,situacao_coleta
    ,obs_coleta
    ,pedido_impresso
    ,data_faturar_parcial
    ,data_faturar_total
    
or delete or insert on pedi_100

for each row

declare

    v_executa_trigger    number(1);
    ws_usuario_rede      varchar2(20);
    ws_maquina_rede      varchar2(40);
    ws_aplicacao         varchar2(20);
    ws_sid               number(9);
    ws_empresa           number(3);
    ws_usuario_systextil varchar2(250);
    ws_locale_usuario    varchar2(5);

begin

    if inserting then

        if :new.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;

    end if;

    if updating then

        if :old.executa_trigger = 1 or :new.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;

    end if;

    if deleting then

        if :old.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;

    end if;

   if v_executa_trigger = 0 then

    -- dados do usuário logado
    inter_pr_dados_usuario(
        ws_usuario_rede
        ,ws_maquina_rede
        ,ws_aplicacao
        ,ws_sid
        ,ws_usuario_systextil
        ,ws_empresa
        ,ws_locale_usuario
    );

    if inserting then

        insert into pedi_100_hist(
            pedido
            ,situacao_venda_old
            ,situacao_venda_atu
            ,observacao_old
            ,observacao_atu
            ,data_ocorr
            ,tipo_ocorr
            ,usuario_rede
            ,maquina_rede
            ,aplicacao
            ,cod_cancelamento_old
            ,cod_cancelamento_new
            ,data_canc_venda_old
            ,data_canc_venda_new
            ,qtde_saldo_pedi_new
            ,qtde_saldo_pedi_old
            ,valor_saldo_pedi_new
            ,valor_saldo_pedi_old
            ,data_entr_venda_new
            ,data_entr_venda_old
            ,origem_pedido_new
            ,origem_pedido_old
            ,cod_ped_cliente_old
            ,cod_ped_cliente_new
            ,data_faturar_total_old
            ,data_faturar_total_new
            ,data_faturar_parcial_old
            ,data_faturar_parcial_new

        )values (
            :new.pedido_venda
            ,null           
            ,:new.situacao_venda
            ,null
            ,null
            ,sysdate
            ,'I'
            ,ws_usuario_rede
            ,ws_maquina_rede
            ,ws_aplicacao
            ,:new.cod_cancelamento
            ,:new.cod_cancelamento
            ,:new.data_canc_venda
            ,:new.data_canc_venda
            ,:new.qtde_saldo_pedi
            ,:new.qtde_saldo_pedi
            ,:new.valor_saldo_pedi
            ,:new.valor_saldo_pedi
            ,:new.data_entr_venda
            ,:new.data_entr_venda
            ,:new.origem_pedido
            ,:new.origem_pedido
            ,'0'
            ,:new.cod_ped_cliente
            ,:new.data_faturar_total 
            ,:new.data_faturar_total 
            ,:new.data_faturar_parcial 
            ,:new.data_faturar_parcial 
        );

    elsif updating then

        insert into pedi_100_hist(
            pedido
            ,situacao_venda_old
            ,situacao_venda_atu
            ,observacao_old
            ,observacao_atu
            ,data_ocorr
            ,tipo_ocorr
            ,usuario_rede
            ,maquina_rede
            ,aplicacao
            ,cod_cancelamento_old
            ,cod_cancelamento_new
            ,data_canc_venda_old
            ,data_canc_venda_new
            ,qtde_saldo_pedi_new
            ,qtde_saldo_pedi_old
            ,valor_saldo_pedi_new
            ,valor_saldo_pedi_old
            ,data_entr_venda_new
            ,data_entr_venda_old
            ,origem_pedido_new
            ,origem_pedido_old
            ,incoterm_new
            ,incoterm_old
            ,cod_ped_cliente_old
            ,cod_ped_cliente_new
            ,data_faturar_total_old
            ,data_faturar_total_new
            ,data_faturar_parcial_old
            ,data_faturar_parcial_new
        )values (
            :new.pedido_venda
            ,:old.situacao_venda
            ,:new.situacao_venda
            ,null
            , null
            ,sysdate
            ,'A'
            ,ws_usuario_rede
            ,ws_maquina_rede
            ,ws_aplicacao
            ,:old.cod_cancelamento
            ,:new.cod_cancelamento
            ,:old.data_canc_venda
            ,:new.data_canc_venda
            ,:new.qtde_saldo_pedi
            ,:old.qtde_saldo_pedi
            ,:new.valor_saldo_pedi
            ,:old.valor_saldo_pedi
            ,:new.data_entr_venda
            ,:old.data_entr_venda
            ,:new.origem_pedido
            ,:old.origem_pedido
            ,:new.incoterm
            ,:old.incoterm
            ,:old.cod_ped_cliente
            ,:new.cod_ped_cliente
            ,:old.data_faturar_total 
            ,:new.data_faturar_total 
            ,:old.data_faturar_parcial 
            ,:new.data_faturar_parcial 
        );

    elsif deleting then

        insert into pedi_100_hist(
            pedido
            ,situacao_venda_old
            ,situacao_venda_atu
            ,observacao_old
            ,observacao_atu
            ,data_ocorr
            ,tipo_ocorr
            ,usuario_rede
            ,maquina_rede
            ,aplicacao
            ,cod_ped_cliente_old
            ,cod_ped_cliente_new
        )values (
            :old.pedido_venda
            ,:old.situacao_venda
            ,null
            ,null
            ,null
            ,sysdate
            ,'D'
            ,ws_usuario_rede
            ,ws_maquina_rede
            ,ws_aplicacao
            ,:old.cod_ped_cliente
            ,:old.cod_ped_cliente
        );

      end if;

   end if;

end trigger_pedi_100_hist;
/

-- alter trigger "trigger_pedi_100_hist" enable


