create or replace PACKAGE BODY ST_PCK_MQOP AS

function fnc_set_Parada_Maquina  (v_codigoEmpresa number
                                    ,v_codigoParada number
                                    ,v_apontamento varchar2
                                    ,v_maqGru varchar2
                                    ,v_maqSub varchar2
                                    ,v_maqNum number
                                    ,v_numeroFusos number
                                    ,v_codigoOperador number
                                    ,v_areaResponsavel number
                                    ,v_velocidade number
                                    ) return varchar2 is
    v_retorno varchar2(4000);
    v_maq number;
    v_centrocusto number;
    v_divisaodeproducao number;
    v_areaproducao number;
    v_paradaValida number;
    v_validaApontInicio number;
    v_validaOperador  number;
    v_validaLancaParada number;
    v_operadorTurno number;
    v_validaApontFim number;
    v_minutos number;

begin
    --Pega o centro de custo da maquina
    select count(*)
            ,centro_custo
            into
            v_maq
            ,v_centrocusto
        from mqop_030
        where mqop_030.maq_sub_grupo_mq = v_maqGru
        and mqop_030.maq_sub_sbgr_maq = v_maqSub
        and mqop_030.numero_maquina = v_maqNum
        group by centro_custo;

        --Pega a area de produção da maquina
        v_areaproducao := 2;
        select divisao_producao into v_divisaodeproducao from basi_185 where centro_custo = v_centrocusto;
        if v_areaproducao is not null then
        select area_producao into v_areaproducao from basi_180
        where divisao_producao = v_divisaodeproducao;
        end if;
        select count(*) into v_paradaValida from efic_020
        where efic_020.area_operacao = v_areaproducao and efic_020.codigo_parada = v_codigoParada;

        --Valida se a maquina existe
        if v_maq = 0 then
            v_retorno := 'Máquina não cadastrada.';
        --Valida motivo de parada
        elsif v_paradaValida = 0 then
            v_retorno := 'Motivo = ' ||  v_codigoParada || 'de parada não cadastrado.';

        end if;

        --Caso seja apontamento de inicio de parada
        if v_apontamento = 'INICIO' then
        --Valida operador
            select count(*), lanca_parada,turno into v_validaOperador,v_validaLancaParada,v_operadorTurno
            from efic_050
            where efic_050.cod_empresa = v_codigoEmpresa
            and efic_050.cod_funcionario = v_codigoOperador
            group by lanca_parada,turno;

            if v_validaOperador = 0 then
            v_retorno := 'Operador ' || v_codigoOperador || ' não cadastrado.';
            elsif v_validaLancaParada = 0 then
            v_retorno := 'Operador ' || v_codigoOperador || ' não tem permissão.';
            end if;

        select count(*) into v_validaApontInicio
					from efic_090
					where efic_090.maquina_maq_sub_grupo_mq = v_maqGru
					and   efic_090.maquina_maq_sub_sbgr_maq = v_maqSub
					and   efic_090.maquina_nr_maq = v_maqNum
					and   efic_090.data_parada 	is not null
					and   efic_090.data_final 	is null
					and   efic_090.numero_fusos 	= 0.00
					and   efic_090.velocidade 	= 0.00;
        -- Verifica se a maquina já está parada
        if v_validaApontInicio > 0 then
        v_retorno := 'Máquina = ' || v_maqGru || '.' || v_maqSub || '.' ||  v_maqNum || 'já está parada (1).';
        -- Verifica se a possui paradas de fusos
        elsif v_numeroFusos != 0 then
        v_validaApontInicio := null;
        select count(*) into v_validaApontInicio
					from efic_090
					where efic_090.maquina_maq_sub_grupo_mq = v_maqGru
					and   efic_090.maquina_maq_sub_sbgr_maq = v_maqSub
					and   efic_090.maquina_nr_maq = v_maqNum
					and   efic_090.data_parada 	is not null
					and   efic_090.data_final 	is null
					and   efic_090.numero_fusos > 0.00 ;
        if v_validaApontInicio > 0 then
        v_retorno := 'Máquina = ' || v_maqGru || '.' || v_maqSub || '.' ||  v_maqNum || ' finalize parada de fusos anterior.';
        end if;
        -- Verifica se a possui paradas de velocidade
        elsif v_velocidade != 0 then
        v_validaApontInicio := null;
        select count(*) into v_validaApontInicio
					from efic_090
					where efic_090.maquina_maq_sub_grupo_mq = v_maqGru
					and   efic_090.maquina_maq_sub_sbgr_maq = v_maqSub
					and   efic_090.maquina_nr_maq = v_maqNum
					and   efic_090.data_parada 	is not null
					and   efic_090.data_final 	is null
					and   efic_090.velocidade 	> 0.00;
        if v_validaApontInicio > 0 then
        v_retorno := 'Máquina = ' || v_maqGru || '.' || v_maqSub || '.' ||  v_maqNum || ' finalize parada de velocidade anterior.';
        end if;
        end if;
        --se não ocorrer nenhum erro de validação, faz o insert na efic_090
        if v_retorno is null then
        insert into efic_090 (
            data_parada
            ,maquina_maq_sub_grupo_mq
            ,maquina_maq_sub_sbgr_maq
            ,maquina_nr_maq
            ,parada_cd_area
            ,parada_cd_parad
            ,hora_inicio
            ,operador_parada
            ,turno
            ,numero_fusos
            ,velocidade
            ,area_resp
        ) values (
            to_char(sysdate,'dd/mm/yyyy')
            ,v_maqGru
            ,v_maqSub
            ,v_maqNum
            ,v_areaproducao
            ,v_codigoParada
            ,SYSTIMESTAMP
            ,v_codigoOperador
            ,v_operadorTurno
            ,v_numeroFusos
            ,v_velocidade
            ,v_areaResponsavel
        );
        end if;
        end if;

        --Caso seja apontamento de fim de parada
        if v_apontamento = 'FIM' then
        select count(*)
            into v_validaApontFim
					from efic_090
					where efic_090.parada_cd_area = v_areaproducao
					and   efic_090.parada_cd_parad = v_codigoParada
					and   efic_090.maquina_maq_sub_grupo_mq = v_maqGru
					and   efic_090.maquina_maq_sub_sbgr_maq = v_maqSub
					and   efic_090.maquina_nr_maq = v_maqNum
					and   efic_090.data_parada is not null
					and   efic_090.data_final  is null;
        --Verifica se tem parada iniciada para fazer a parada de fim
        if v_validaApontFim = 0 then
        v_retorno := 'Parada = ' ||  v_codigoParada || ' ainda não foi iniciada.';
        else
        --se não ocorrer nenhum erro de validação, faz o update na efic_090
        --faz calculo de minutos entre a data de inicio da parada e o fim(sysdate)
        v_minutos := 0;
       select
            --round(to_number(to_date(to_char(trunc(to_date(sysdate,'mm/dd/yyyy'),'DD')) || ' ' || to_char(sysdate,'HH24:MI'),'mm/dd/yyyy HH24:MI') -
            --to_date(to_char(trunc(to_date(efic_090.data_parada,'mm/dd/yyyy'),'DD'))  || ' ' || to_char(efic_090.hora_inicio,'HH24:MI'),'mm/dd/yyyy HH24:MI')) * 1440)
            round(to_number(to_date(to_char(trunc(to_date(sysdate,'dd/mm/rr'),'DD')) || ' ' || to_char(sysdate,'HH24:MI'),'dd/mm/rr HH24:MI') -
            to_date(to_char(trunc(to_date(efic_090.data_parada,'dd/mm/rr'),'DD'))  || ' ' || to_char(efic_090.hora_inicio,'HH24:MI'),'dd/mm/rr HH24:MI')) * 1440)            into v_minutos
        from EFIC_090
            where efic_090.parada_cd_area = v_areaproducao
				and   efic_090.parada_cd_parad = v_codigoParada
				and   efic_090.maquina_maq_sub_grupo_mq = v_maqGru
				and   efic_090.maquina_maq_sub_sbgr_maq = v_maqSub
				and   efic_090.maquina_nr_maq = v_maqNum
				and   efic_090.data_parada is not null
				and   efic_090.data_final  is null;

            update efic_090
                    set   efic_090.data_final 		= sysdate
                          ,efic_090.hora_final 		= SYSTIMESTAMP
                          ,efic_090.minutos_parada 	= v_minutos
                          ,efic_090.parada_cd_parad 	= v_codigoParada
                    where efic_090.parada_cd_area 	= v_areaproducao
                    and   efic_090.parada_cd_parad   		= v_codigoParada
                    and   efic_090.maquina_maq_sub_grupo_mq 	= v_maqGru
                    and   efic_090.maquina_maq_sub_sbgr_maq 	= v_maqSub
                    and   efic_090.maquina_nr_maq 			= v_maqNum
                    and   efic_090.data_parada 				is not null
                    and   efic_090.data_final 				is null;
        end if;



        end if;

    return v_retorno;
end fnc_set_Parada_Maquina;

END ST_PCK_MQOP;
