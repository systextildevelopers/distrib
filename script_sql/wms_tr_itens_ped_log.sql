create or replace trigger wms_tr_itens_ped_log
  before insert
  or delete
  or update of numero_pedido, seq_item,       nivel_sku,           grupo_sku, 
	       subgrupo_sku,  item_sku,       qtd_pedida,          qtd_faturada, 
	       qtd_afaturar,  qtd_saldo,      val_unit_liq,        ean, 
	       cod_deposito,  respeita_grade, referencia_completa,
               ref_original,  sku_original 
  on inte_wms_itens_ped
  for each row
declare
  -- local variables here
  v_numero_pedido           inte_wms_itens_ped.numero_pedido       %type;
  v_seq_item                inte_wms_itens_ped.seq_item            %type;
  v_nivel_sku_old           inte_wms_itens_ped.nivel_sku           %type;
  v_nivel_sku_new           inte_wms_itens_ped.nivel_sku           %type;
  v_grupo_sku_old           inte_wms_itens_ped.grupo_sku           %type;
  v_grupo_sku_new           inte_wms_itens_ped.grupo_sku           %type;
  v_subgrupo_sku_old        inte_wms_itens_ped.subgrupo_sku        %type;
  v_subgrupo_sku_new        inte_wms_itens_ped.subgrupo_sku        %type;
  v_item_sku_old            inte_wms_itens_ped.item_sku            %type;
  v_item_sku_new            inte_wms_itens_ped.item_sku            %type;
  v_qtd_pedida_old          inte_wms_itens_ped.qtd_pedida          %type;
  v_qtd_pedida_new          inte_wms_itens_ped.qtd_pedida          %type;
  v_qtd_faturada_old        inte_wms_itens_ped.qtd_faturada        %type;
  v_qtd_faturada_new        inte_wms_itens_ped.qtd_faturada        %type;
  v_qtd_afaturar_old        inte_wms_itens_ped.qtd_afaturar        %type;
  v_qtd_afaturar_new        inte_wms_itens_ped.qtd_afaturar        %type;
  v_qtd_saldo_old           inte_wms_itens_ped.qtd_saldo           %type;
  v_qtd_saldo_new           inte_wms_itens_ped.qtd_saldo           %type;
  v_val_unit_liq_old        inte_wms_itens_ped.val_unit_liq        %type;
  v_val_unit_liq_new        inte_wms_itens_ped.val_unit_liq        %type;
  v_ean_old                 inte_wms_itens_ped.ean                 %type;
  v_ean_new                 inte_wms_itens_ped.ean                 %type;
  v_cod_deposito_old        inte_wms_itens_ped.cod_deposito        %type;
  v_cod_deposito_new        inte_wms_itens_ped.cod_deposito        %type;
  v_respeita_grade_old      inte_wms_itens_ped.respeita_grade      %type;
  v_respeita_grade_new      inte_wms_itens_ped.respeita_grade      %type;
  v_referencia_completa_old inte_wms_itens_ped.referencia_completa %type;
  v_referencia_completa_new inte_wms_itens_ped.referencia_completa %type;
  v_ref_original_old        inte_wms_itens_ped.ref_original        %type;
  v_ref_original_new        inte_wms_itens_ped.ref_original        %type;
  v_sku_original_old        inte_wms_itens_ped.sku_original        %type;
  v_sku_original_new        inte_wms_itens_ped.sku_original        %type;

  v_sid                            number(9);
  v_empresa                        number(3);
  v_usuario_systextil              varchar2(250);
  v_locale_usuario                 varchar2(5);
  v_nome_programa                  varchar2(20);


  v_operacao                       varchar(1);
  v_data_operacao                  date;
  v_usuario_rede                   varchar(20);
  v_maquina_rede                   varchar(40);
  v_aplicativo                     varchar(20);
begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();

   --alimenta as variaveis new caso seja insert ou update
   if inserting or updating
   then
      if inserting
      then v_operacao := 'i';
      else v_operacao := 'u';
      end if;

      v_numero_pedido           := :new.numero_pedido;
      v_seq_item                := :new.seq_item;
      v_nivel_sku_new           := :new.nivel_sku;
      v_grupo_sku_new           := :new.grupo_sku;
      v_subgrupo_sku_new        := :new.subgrupo_sku;
      v_item_sku_new            := :new.item_sku;
      v_qtd_pedida_new          := :new.qtd_pedida;
      v_qtd_faturada_new        := :new.qtd_faturada;
      v_qtd_afaturar_new        := :new.qtd_afaturar;
      v_qtd_saldo_new           := :new.qtd_saldo;
      v_val_unit_liq_new        := :new.val_unit_liq;
      v_ean_new                 := :new.ean;
      v_cod_deposito_new        := :new.cod_deposito;
      v_respeita_grade_new      := :new.respeita_grade;
      v_referencia_completa_new := :new.referencia_completa;
      v_ref_original_new        := :new.ref_original;
      v_sku_original_new        := :new.sku_original;

   end if; --fim do if inserting or updating

   --alimenta as variaveis old caso seja delete ou update
   if deleting or updating
   then
      if deleting
      then
         v_operacao      := 'd';
      else
         v_operacao      := 'u';
      end if;

      v_numero_pedido           := :old.numero_pedido;
      v_seq_item                := :old.seq_item;
      v_nivel_sku_old           := :old.nivel_sku;
      v_grupo_sku_old           := :old.grupo_sku;
      v_subgrupo_sku_old        := :old.subgrupo_sku;
      v_item_sku_old            := :old.item_sku;
      v_qtd_pedida_old          := :old.qtd_pedida;
      v_qtd_faturada_old        := :old.qtd_faturada;
      v_qtd_afaturar_old        := :old.qtd_afaturar;
      v_qtd_saldo_old           := :old.qtd_saldo;
      v_val_unit_liq_old        := :old.val_unit_liq;
      v_ean_old                 := :old.ean;
      v_cod_deposito_old        := :old.cod_deposito;
      v_respeita_grade_old      := :old.respeita_grade;
      v_referencia_completa_old := :old.referencia_completa;
      v_ref_original_old        := :old.ref_original;
      v_sku_original_old        := :old.sku_original;

   end if; --fim do if deleting or updating


   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);


    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente

   --insere na wms_tr_itens_ped_log o registro.
   insert into inte_wms_itens_ped_log (
      numero_pedido,             seq_item,
      nivel_sku_old,             nivel_sku_new,
      grupo_sku_old,             grupo_sku_new,
      subgrupo_sku_old,          subgrupo_sku_new,
      item_sku_old,              item_sku_new,
      qtd_pedida_old,            qtd_pedida_new,
      qtd_faturada_old,          qtd_faturada_new,
      qtd_afaturar_old,          qtd_afaturar_new,
      qtd_saldo_old,             qtd_saldo_new,
      val_unit_liq_old,          val_unit_liq_new,
      ean_old,                   ean_new,
      cod_deposito_old,          cod_deposito_new,
      respeita_grade_old,        respeita_grade_new,
      referencia_completa_old,   referencia_completa_new,
      operacao,                  data_operacao,
      usuario_rede,              maquina_rede,
      aplicativo,                nome_programa,
      ref_original_old,          ref_original_new,
      sku_original_old,          sku_original_new
   )
   values (
      v_numero_pedido,           v_seq_item,
      v_nivel_sku_old,           v_nivel_sku_new,
      v_grupo_sku_old,           v_grupo_sku_new,
      v_subgrupo_sku_old,        v_subgrupo_sku_new,
      v_item_sku_old,            v_item_sku_new,
      v_qtd_pedida_old,          v_qtd_pedida_new,
      v_qtd_faturada_old,        v_qtd_faturada_new,
      v_qtd_afaturar_old,        v_qtd_afaturar_new,
      v_qtd_saldo_old,           v_qtd_saldo_new,
      v_val_unit_liq_old,        v_val_unit_liq_new,
      v_ean_old,                 v_ean_new,
      v_cod_deposito_old,        v_cod_deposito_new,
      v_respeita_grade_old,      v_respeita_grade_new,
      v_referencia_completa_old, v_referencia_completa_new,
      v_operacao,                v_data_operacao,
      v_usuario_rede,            v_maquina_rede,
      v_aplicativo,              v_nome_programa,
      v_ref_original_old,        v_ref_original_new,
      v_sku_original_old,        v_sku_original_new
   );

end wms_tr_itens_ped_log;

/

exec inter_pr_recompile;
/* versao: 3 */


 exit;


 exit;

 exit;
