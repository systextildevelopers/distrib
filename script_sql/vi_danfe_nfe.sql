create or replace view vi_danfe_nfe as
select
      numero_danf_nfe
      ,natoper_nat_oper
      ,avista
      ,tipo_doc
      ,serie
      ,documento
      ,o_data_emissao
      ,data_emissao
      ,entrada_saida
      ,cod_cidade_ibge
      ,fornecedor9
      ,fornecedor4
      ,fornecedor2
      ,nome_fornecedor
      ,nome_fantasia
      ,endereco_forne
      ,s010_numero_imovel
      ,s010_complemento
      ,s010_bairro
      ,cidade_nfe_cod_cidade_ibge
      ,cidade_nfe_cidade
      ,cidade_nfe_estado
      ,codigo_estado
      ,cep_fornecedor
      ,telefone_forne
      ,codigo_internacional
      ,pais_nfe_nome_pais
      ,inscr_est_forne
      ,insc_municipal
      ,f_cgc_9 as emit_f_cgc_9
      ,f_cgc_4 as emit_f_cgc_4
      ,f_cgc_2 as emit_f_cgc_2
      ,nome_empresa as emit_nome_empresa
      ,endereco as emit_endereco
      ,f_numero as emit_f_numero
      ,complemento as emit_complemento
      ,f_bairro as emit_f_bairro
      ,cidade_dest_cod_cidade_ibge as emit_cod_cidade_ibge
      ,cidade_dest_cidade as emit_cidade_dest_cidade
      ,cidade_dest_estado as emit_cidade_dest_estado
      ,pais_dest_nome_pais as pais_dest_nome_pais
      ,f500_telefone as f500_telefone
      ,cep_empresa as emit_cep_empresa
      ,pais_dest_codigo_pais  as emit_pais_dest_codigo_pais
      ,f_inscr_estadual as emit_f_inscr_estadual
      ,cd_cli_cgc_cli9
      ,cd_cli_cgc_cli4
      ,cd_cli_cgc_cli2
      ,p150_end_entr_cobr
      ,p150_numero_imovel
      ,complemento_endereco
      ,bairro_entr_cobr
      ,cidade_entr_cod_cidade
      ,cidade_entr_cidade
      ,cidade_entr_estado
      ,base_icms
      ,valor_icms
      ,base_icms_sub
      ,valor_icms_sub
      ,valor_itens
      ,valor_frete
      ,valor_seguro
      ,valor_desconto
      ,valor_total_ipi
      ,valor_despesas
      ,total_docto
      ,tipo_frete
      ,cod_empresa
      ,cod_solicitacao_nfe
from
    ( select
       o.numero_danf_nfe         /*1*/
      ,o.natoper_nat_oper              /*4*/
      ,p070.avista                     /*5*/
      ,'55' as tipo_doc                            /*6*/
      ,o.serie                         /*7*/
      ,o.documento                     /*8*/
      ,o.data_emissao as o_data_emissao                  /*9*/
      ,o.data_emissao                  /*10*/
      ,'0' as entrada_saida            /*11*/
      ,cidade_nfe.cod_cidade_ibge      /*12*/
      ,s010.fornecedor9                /*19*/
      ,s010.fornecedor4                /*20*/
      ,s010.fornecedor2                /*21*/
      ,s010.nome_fornecedor            /*22*/
      ,s010.nome_fantasia as nome_fantasia             /*23*/
      ,s010.endereco_forne             /*24*/
      ,s010.numero_imovel as s010_numero_imovel              /*25*/
      ,s010.complemento as s010_complemento               /*26*/
      ,s010.bairro as s010_bairro                     /*27*/
      ,cidade_nfe.cod_cidade_ibge as cidade_nfe_cod_cidade_ibge      /*28*/
      ,cidade_nfe.cidade as cidade_nfe_cidade              /*29*/
      ,cidade_nfe.estado as cidade_nfe_estado               /*30*/
      ,substr(cidade_nfe.cod_cidade_ibge,1,2) as codigo_estado    /*31*/
      ,s010.cep_fornecedor             /*32*/
      ,s010.telefone_forne             /*33*/
      ,pais_nfe.codigo_internacional   /*34*/
      ,pais_nfe.nome_pais  as pais_nfe_nome_pais /*35*/
      ,s010.inscr_est_forne            /*36*/
      ,s010.insc_municipal             /*38*/
      ,f500.cgc_9 as f_cgc_9           /*40*/
      ,f500.cgc_4 as f_cgc_4           /*41*/
      ,f500.cgc_2 as f_cgc_2           /*42*/
      ,f500.nome_empresa               /*43*/
      ,f500.endereco as endereco     /*44*/
      ,f500.numero as f_numero         /*45*/
      ,f500.complemento as complemento /*46*/
      ,f500.bairro as f_bairro           /*47*/
      ,cidade_dest.cod_cidade_ibge as cidade_dest_cod_cidade_ibge     /*48*/
      ,cidade_dest.cidade as cidade_dest_cidade              /*49*/
      ,cidade_dest.estado as cidade_dest_estado              /*50*/
      ,pais_dest.nome_pais as pais_dest_nome_pais             /*51*/
      ,f500.telefone as f500_telefone                  /*52*/
      ,f500.cep_empresa                /*53*/
      ,pais_dest.codigo_internacional as pais_dest_codigo_pais           /*54*/
      ,cast(f500.inscr_estadual as varchar2(14)) as f_inscr_estadual             /*55*/
      ,p150.cd_cli_cgc_cli9            /*67*/
      ,p150.cd_cli_cgc_cli4            /*68*/
      ,p150.cd_cli_cgc_cli2            /*69*/
      ,p150.end_entr_cobr as p150_end_entr_cobr             /*70*/
      ,p150.numero_imovel as p150_numero_imovel              /*71*/
      ,p150.complemento_endereco       /*72*/
      ,p150.bairro_entr_cobr           /*73*/
      ,cidade_entr.cod_cidade_ibge as cidade_entr_cod_cidade     /*74*/
      ,cidade_entr.cidade as cidade_entr_cidade              /*75*/
      ,cidade_entr.estado as cidade_entr_estado             /*76*/
      ,o.base_icms                     /*77*/
      ,o.valor_icms                    /*78*/
      ,o.base_icms_sub                 /*79*/
      ,o.valor_icms_sub                /*80*/
      ,o.valor_itens                   /*81*/
      ,o.valor_frete                   /*82*/
      ,o.valor_seguro                  /*83*/
      ,o.valor_desconto                /*84*/
      ,o.valor_total_ipi               /*86*/
      ,o.valor_despesas                /*89*/
      ,o.total_docto                   /*90*/
      ,decode(o.tipo_frete,1,0,2,1,0) as tipo_frete    /*103*/
      ,o.local_entrega as cod_empresa  /*137*/
      ,o.cod_solicitacao_nfe
    from obrf_010 o, fatu_500 f500, basi_160 cidade_nfe,
         basi_160 cidade_dest, basi_165 pais_nfe, basi_165 pais_dest,
         obrf_158 p, supr_010 s010, pedi_150 p150, basi_160 cidade_entr,
         fatu_505 f_505, pedi_070 p070
    where f500.codigo_empresa    = o.local_entrega
      and o.local_entrega        = 1
      and p.cod_empresa          = f500.codigo_empresa
      and s010.fornecedor9       = o.cgc_cli_for_9
      and s010.fornecedor4       = o.cgc_cli_for_4
      and s010.fornecedor2       = o.cgc_cli_for_2
      and cidade_nfe.cod_cidade  = s010.cod_cidade
      and pais_nfe.codigo_pais   = cidade_nfe.codigo_pais
      and cidade_dest.cod_cidade = f500.codigo_cidade
      and pais_dest.codigo_pais  = cidade_dest.codigo_pais
      and p150.cd_cli_cgc_cli9   = s010.fornecedor9 (+)
      and p150.cd_cli_cgc_cli4   = s010.fornecedor4 (+)
      and p150.cd_cli_cgc_cli2   = s010.fornecedor2 (+)
      and p150.tipo_endereco     = 1
      and p150.seq_endereco      = 1
      and f_505.codigo_empresa  = o.local_entrega
      and f_505.serie_nota_fisc = o.serie
      and f_505.serie_nfe = 'S'
      and o.cod_status in ('100')
      and cidade_entr.cod_cidade = p150.cid_entr_cobr (+)
      and o.condicao_pagto = p070.cond_pgt_cliente)
union all
    (select f.numero_danf_nfe         /*1*/
      ,f.natop_nf_nat_oper              /*4*/
      ,p070.avista                            /*5*/
      ,'55' as tipo_doc                            /*6*/
      ,f.serie_nota_fisc                         /*7*/
      ,f.num_nota_fiscal                     /*8*/
      ,f.data_emissao as o_data_emissao                  /*9*/
      ,f.data_emissao                  /*10*/
      ,'1' as entrada_saida            /*11*/
      ,cidade_nfe.cod_cidade_ibge      /*12*/
      ,p010.cgc_9                /*19*/
      ,p010.cgc_4                /*20*/
      ,p010.cgc_2                /*21*/
      ,p010.nome_cliente            /*22*/
      ,p010.fantasia_cliente as nome_fantasia             /*23*/
      ,p010.endereco_cliente             /*24*/
      ,p010.numero_imovel as s010_numero_imovel              /*25*/
      ,p010.complemento as s010_complemento               /*26*/
      ,p010.bairro as s010_bairro                     /*27*/
      ,cidade_nfe.cod_cidade_ibge as cidade_nfe_cod_cidade_ibge      /*28*/
      ,cidade_nfe.cidade as cidade_nfe_cidade              /*29*/
      ,cidade_nfe.estado as cidade_nfe_estado               /*30*/
      ,substr(cidade_nfe.cod_cidade_ibge,1,2) as codigo_estado    /*31*/
      ,p010.cep_cliente             /*32*/
      ,p010.telefone_cliente             /*33*/
      ,pais_nfe.codigo_internacional   /*34*/
      ,pais_nfe.nome_pais  as pais_nfe_nome_pais /*35*/
      ,replace(p010.insc_est_cliente,'.','') as inscr_est_forne          /*36*/
      ,null             /*38*/
      ,f500.cgc_9 as f_cgc_9           /*40*/
      ,f500.cgc_4 as f_cgc_4           /*41*/
      ,f500.cgc_2 as f_cgc_2           /*42*/
      ,f500.nome_empresa               /*43*/
      ,f500.endereco as f_endereco     /*44*/
      ,f500.numero as f_numero         /*45*/
      ,f500.complemento as complemento /*46*/
      ,f500.bairro as f_bairro           /*47*/
      ,cidade_dest.cod_cidade_ibge as cidade_dest_cod_cidade_ibge     /*48*/
      ,cidade_dest.cidade as cidade_dest_cidade              /*49*/
      ,cidade_dest.estado as cidade_dest_estado              /*50*/
      ,pais_dest.nome_pais as pais_dest_nome_pais             /*51*/
      ,f500.telefone as f500_telefone                  /*52*/
      ,f500.cep_empresa                /*53*/
      ,pais_dest.codigo_internacional as pais_dest_codigo_pais           /*54*/
      ,cast(replace(f500.inscr_estadual,'.','') as varchar2(14)) as f_inscr_estadual             /*55*/
      ,p150.cd_cli_cgc_cli9            /*67*/
      ,p150.cd_cli_cgc_cli4            /*68*/
      ,p150.cd_cli_cgc_cli2            /*69*/
      ,p150.end_entr_cobr as p150_end_entr_cobr             /*70*/
      ,p150.numero_imovel as p150_numero_imovel              /*71*/
      ,p150.complemento_endereco       /*72*/
      ,p150.bairro_entr_cobr           /*73*/
      ,cidade_entr.cod_cidade_ibge as cidade_entr_cod_cidade     /*74*/
      ,cidade_entr.cidade as cidade_entr_cidade              /*75*/
      ,cidade_entr.estado as cidade_entr_estado             /*76*/
      ,f.base_icms                     /*77*/
      ,f.valor_icms                    /*78*/
      ,f.base_icms_sub                 /*79*/
      ,f.valor_icms_sub                /*80*/
      ,f.valor_itens_nfis                  /*81*/
      ,f.valor_frete_nfis                   /*82*/
      ,f.valor_seguro                  /*83*/
      ,f.valor_desconto                /*84*/
      ,f.valor_ipi               /*86*/
      ,f.valor_despesas                /*89*/
      ,(f.valor_itens_nfis + f.valor_ipi      + f.valor_encar_nota +
        f.valor_frete_nfis + f.valor_seguro   - f.valor_desc_nota -
        f.valor_suframa    + f.valor_despesas - f.vlr_desc_especial +
        f.valor_icms_sub)
       as total_docto                   /*90*/
      ,decode(f.tipo_frete,1,0,2,1,0) as tipo_frete    /*103*/
      ,f.codigo_empresa as cod_empresa  /*137*/
      ,f.cod_solicitacao_nfe
    from fatu_050 f, fatu_500 f500, basi_160 cidade_nfe,
         basi_160 cidade_dest, basi_165 pais_nfe, basi_165 pais_dest,
         obrf_158 p, pedi_010 p010, pedi_150 p150, basi_160 cidade_entr,
         fatu_505 f_505, pedi_070 p070
    where f500.codigo_empresa    = f.codigo_empresa
      and f.codigo_empresa        = 1
      and p.cod_empresa          = f500.codigo_empresa
      and p010.cgc_9       = f.cgc_9
      and p010.cgc_4       = f.cgc_4
      and p010.cgc_2       = f.cgc_2
      and cidade_nfe.cod_cidade  = p010.cod_cidade
      and pais_nfe.codigo_pais   = cidade_nfe.codigo_pais
      and cidade_dest.cod_cidade = f500.codigo_cidade
      and pais_dest.codigo_pais  = cidade_dest.codigo_pais
      and p150.cd_cli_cgc_cli9   = p010.cgc_9 (+)
      and p150.cd_cli_cgc_cli4   = p010.cgc_4 (+)
      and p150.cd_cli_cgc_cli2   = p010.cgc_2 (+)
      and p150.tipo_endereco     = 1
      and p150.seq_endereco      = 1
      and cidade_entr.cod_cidade = p150.cid_entr_cobr (+)
      and f_505.codigo_empresa  = f.codigo_empresa
      and f_505.serie_nota_fisc = f.serie_nota_fisc
      and f_505.serie_nfe = 'S'
      and f.cod_status in ('100')
      and f.cond_pgto_venda = p070.cond_pgt_cliente);

