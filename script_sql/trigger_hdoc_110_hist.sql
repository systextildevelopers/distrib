
  CREATE OR REPLACE TRIGGER "TRIGGER_HDOC_110_HIST" 
BEFORE UPDATE of empresa, documento, sequencia, serie_parcela,
		 tipo_titulo, cgc9, cgc4, cgc2,
		 usuario, codigo_processo, data_liberacao, data_lote,
		 hora_liberacao
OR DELETE
OR INSERT
on hdoc_110
for each row

declare
   ws_empresa                   hdoc_110.empresa%type;
   ws_documento                 hdoc_110.documento%type;
   ws_sequencia                 hdoc_110.sequencia%type;
   ws_serie_parcela             hdoc_110.serie_parcela%type;
   ws_tipo_titulo               hdoc_110.tipo_titulo%type;
   ws_cgc9                      hdoc_110.cgc9%type;
   ws_cgc4                      hdoc_110.cgc4%type;
   ws_cgc2                      hdoc_110.cgc2%type;
   ws_usuario                   hdoc_110.usuario%type;
   ws_codigo_processo           hdoc_110.codigo_processo%type;
   ws_data_liberacao_old        date;
   ws_data_liberacao_atu        date;
   ws_tipo_ocorr                varchar2(1);
   ws_usuario_rede              varchar2(20);
   ws_maquina_rede              varchar2(40);
   ws_aplicacao                 varchar2(20);
begin
   if INSERTING then
      ws_empresa                := :new.empresa;
      ws_documento        	:= :new.documento;
      ws_sequencia       	:= :new.sequencia;
      ws_serie_parcela   	:= :new.serie_parcela;
      ws_tipo_titulo    	:= :new.tipo_titulo;
      ws_cgc9           	:= :new.cgc9;
      ws_cgc4           	:= :new.cgc4;
      ws_cgc2           	:= :new.cgc2;
      ws_usuario        	:= :new.usuario;
      ws_codigo_processo     	:= :new.codigo_processo;
      ws_data_liberacao_old	:= '';
      ws_data_liberacao_atu	:= :new.data_liberacao;
      ws_tipo_ocorr          	:= 'I';
   elsif UPDATING then
         ws_empresa             := :new.empresa;
         ws_documento           := :new.documento;
         ws_sequencia           := :new.sequencia;
         ws_serie_parcela       := :new.serie_parcela;
         ws_tipo_titulo         := :new.tipo_titulo;
         ws_cgc9                := :new.cgc9;
         ws_cgc4                := :new.cgc4;
         ws_cgc2                := :new.cgc2;
         ws_usuario             := :new.usuario;
         ws_codigo_processo     := :new.codigo_processo;
         ws_data_liberacao_old  := :old.data_liberacao;
         ws_data_liberacao_atu  := :new.data_liberacao;
         ws_tipo_ocorr          := 'A';
      elsif DELETING then
            ws_empresa             := :old.empresa;
            ws_documento           := :old.documento;
            ws_sequencia       	   := :old.sequencia;
            ws_serie_parcela   	   := :old.serie_parcela;
            ws_tipo_titulo    	   := :old.tipo_titulo;
            ws_cgc9           	   := :old.cgc9;
            ws_cgc4           	   := :old.cgc4;
            ws_cgc2            	   := :old.cgc2;
            ws_usuario             := :old.usuario;
            ws_codigo_processo     := :old.codigo_processo;
            ws_data_liberacao_old  := :old.data_liberacao;
            ws_data_liberacao_atu  := null;
            ws_tipo_ocorr          := 'D';
   end if;

   -- DADOS DO LOGIN DO USUARIO
   begin
      select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
      into   ws_usuario_rede,     ws_maquina_rede,      ws_aplicacao
      from sys.gv_$session
      where audsid  = userenv('SESSIONID')
        and inst_id = userenv('INSTANCE')
        and rownum < 2;
   exception when no_data_found then
      ws_usuario_rede := '';
      ws_maquina_rede := '';
      ws_aplicacao    := '';
   end;

   INSERT INTO hdoc_110_hist
     (empresa,            documento,             sequencia,
      serie_parcela,      tipo_titulo,           cgc9,
      cgc4,               cgc2,                  usuario,
      codigo_processo,    data_liberacao_old,    data_liberacao_atu,
      tipo_ocorr,         data_ocorr,            usuario_rede,
      maquina_rede,       aplicacao)
   VALUES
     (ws_empresa,         ws_documento,          ws_sequencia,
      ws_serie_parcela,   ws_tipo_titulo,        ws_cgc9,
      ws_cgc4,            ws_cgc2,               ws_usuario,
      ws_codigo_processo, ws_data_liberacao_old, ws_data_liberacao_atu,
      ws_tipo_ocorr,      sysdate,               ws_usuario_rede,
      ws_maquina_rede,    ws_aplicacao);
end trigger_hdoc_110_hist;

-- ALTER TRIGGER "TRIGGER_HDOC_110_HIST" ENABLE
 

/

exec inter_pr_recompile;

