alter table LOJA_010 add DATA_ATUALIZACAO_API DATE;
comment on column LOJA_010.DATA_ATUALIZACAO_API is 'Este campo so sera alimentado se o cliente estiver utilizando as APIs';
create index LOJA_010_DTAPI on LOJA_010 (DATA_ATUALIZACAO_API);
create index CREC_085_REPRE ON CREC_085 (REPRES);
create index FATU_070_REPRE ON FATU_070 (COD_REP_CLIENTE);
create index FATX_070_REPRE ON FATX_070 (COD_REP_CLIENTE);
create index PEDI_100_REPRE ON PEDI_100 (COD_REP_CLIENTE);
create index PEDI_250_REPRE ON PEDI_250 (CODIGO_REPR);
