create or replace package body ST_PCK_PLANO_CONTAS_ITEM is

	FUNCTION get_by_cod_plano(p_cod_plano_cta number, p_msg_erro in out varchar2) 
    return t_dados_cont_535 as r_bulk t_dados_cont_535;
	BEGIN
        begin
            select * 
            BULK COLLECT INTO r_bulk
            from cont_535 
            where cod_plano_cta = p_cod_plano_cta;
        exception when others then
            p_msg_erro := 'Não foi possível buscar o plano de contas item!. cod_plano_cta:' || p_cod_plano_cta;
        end;

		RETURN r_bulk;
	END;

    function get_by_cod_reduzido(p_cod_plano_cta number, p_cod_reduzido number, p_msg_erro in out varchar2) 
    return t_dados_cont_535 as r_bulk t_dados_cont_535;
	begin

        begin
            select * 
            BULK COLLECT INTO r_bulk
            from cont_535 
            where cod_plano_cta = p_cod_plano_cta
            and   cod_reduzido = p_cod_reduzido;
        exception when others then
            p_msg_erro := 'Não foi possível buscar o plano de contas item pelo codigo reduzido!. cod_reduzido:' || p_cod_reduzido;
        end;

		RETURN r_bulk;
    end;

    function get_by_nivel_patr(p_cod_plano_cta number, p_nivel number, p_patr number, p_msg_erro in out varchar2) 
    return t_dados_cont_535 as r_bulk t_dados_cont_535;
	begin
        begin
            select * 
            BULK COLLECT INTO r_bulk
            from cont_535 
            where cod_plano_cta = p_cod_plano_cta
            and   nivel = p_nivel
            and   patr_result = p_patr;
        exception when others then
            p_msg_erro := 'Não foi possível buscar o plano de contas item pelo patrimonio!. p_patr:' || p_patr;
        end;

		RETURN r_bulk;
    end;

    procedure insere_item(  p_cod_plano number, p_conta_contabil varchar2,
                            p_sub_conta number, p_cod_reduzido number, 
                            p_conta_mae number, p_descricao varchar2, 
                            p_descr_sub_conta varchar2, p_exige_sub_conta number,
                            p_nivel number, p_patr_result number,
                            p_tipo_conta number, p_gera_custos number, 
                            p_tipo_orcamento number, p_familia_orcamento number, 
                            p_debito_credito varchar2, p_indicador_natu number, 
                            p_permite_lancamento_manual number, p_quebra_filial number,
                            p_conta_contabil_ref varchar2, p_indic_natu_sped number,
                            p_tp_livro_aux_sped varchar2, p_data_alteracao date, 
                            p_tipo_natureza number, p_msg_erro in out varchar2) AS
    begin
        
        begin
			insert into cont_535 ( 
                cod_plano_cta,     conta_contabil,   
                subconta,          cod_reduzido, 
                conta_mae,         descricao, 
                descr_subconta,    exige_subconta, 
                nivel,             patr_result, 
                tipo_conta,        gera_custos, 
                tipo_orcamento, 
                familia_orcamento, debito_credito, 
                indicador_natu,    permite_lancto_manual,  
                quebra_filial,     conta_contabil_ref,     
                indic_natu_sped,   tp_livro_aux_sped,      
                data_alteracao,    tipo_natureza
            ) values ( 
                p_cod_plano, p_conta_contabil,  
                p_sub_conta, p_cod_reduzido, 
                p_conta_mae, p_descricao, 
                p_descr_sub_conta, p_exige_sub_conta,                                                               
                p_nivel, p_patr_result, 
                p_tipo_conta, p_gera_custos,   
                p_tipo_orcamento,                              
                p_familia_orcamento, p_debito_credito, 
                p_indicador_natu, p_permite_lancamento_manual,  
                p_quebra_filial, p_conta_contabil_ref, 
                p_indic_natu_sped, p_tp_livro_aux_sped, 
                p_data_alteracao, p_tipo_natureza
            );
		exception when others then
            p_msg_erro := 'Ocorreu um erro ao inserir plano de contas item!.' || SQLERRM;
		end;
        
	end;

end ST_PCK_PLANO_CONTAS_ITEM;
