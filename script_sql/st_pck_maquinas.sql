create or replace package st_pck_maquinas as

  ----------------------------------
  -- Verifica se a máquina existe --
  ----------------------------------
  function maquina_existe( 
    p_maquina varchar2
  ) return boolean;

  function maquina_existe( 
    p_grupo varchar2,
    p_subgrupo varchar2,
    p_numero number
  ) return boolean;

end st_pck_maquinas;
/
create or replace package body st_pck_maquinas as

  ----------------------------------
  -- Verifica se a máquina existe --
  ----------------------------------

  function maquina_existe( 
    p_maquina varchar2
  ) return boolean
  is
    v_tam_str number := length(p_maquina);
    v_grupo varchar2(10);
    v_subgrupo varchar2(10);
    v_numero number;
  begin

    if v_tam_str = 8 then
        v_grupo    := substr(p_maquina,1,2);
        v_subgrupo := substr(p_maquina,3,1);
        v_numero   := substr(p_maquina,4,4);
    elsif v_tam_str = 10 then
        v_grupo    := substr(p_maquina,1,4);
        v_subgrupo := substr(p_maquina,5,1);
        v_numero   := substr(p_maquina,6,4);
    elsif v_tam_str = 11 then
        v_grupo    := substr(p_maquina,1,3);
        v_subgrupo := substr(p_maquina,4,2);
        v_numero   := substr(p_maquina,7,4);
    elsif v_tam_str = 12 then
        v_grupo    := substr(replace(p_maquina, '.', ''),1,4);
        v_subgrupo := substr(replace(p_maquina, '.', ''),5,2);
        v_numero   := substr(replace(p_maquina, '.', ''),8,4); 
    elsif v_tam_str = 14 then
        v_grupo    := substr(p_maquina,1,4);
        v_subgrupo := substr(p_maquina,6,3);
        v_numero   := substr(p_maquina,10,5);
    end if;

    return maquina_existe(v_grupo, v_subgrupo, v_numero);

  end maquina_existe;


  function maquina_existe( 
    p_grupo varchar2,
    p_subgrupo varchar2,
    p_numero number
  ) return boolean
  is
    v_existe number;
  begin

    select count(1) into v_existe from mqop_030 
      where mqop_030.maq_sub_grupo_mq = p_grupo
      and mqop_030.maq_sub_sbgr_maq = p_subgrupo
      and mqop_030.numero_maquina = p_numero;
    
    return v_existe = 1;

  end maquina_existe;

end st_pck_maquinas;
/
