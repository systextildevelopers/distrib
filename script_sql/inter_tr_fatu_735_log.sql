
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_735_LOG" 
   after insert or delete or update of cod_empresa, num_solicitacao,cnpj9,cnpj4,cnpj2,situacao_sol,data_embarque,tipo_faturamento
   on fatu_735
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   v_nome_programa           varchar2(20);
begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid); 
 
   if inserting
   then
      begin

         insert into fatu_735_log (
            tipo_ocorr,                   data_ocorr,
            hora_ocorr,                   usuario_rede,
            maquina_rede,                 aplicacao,
            usuario_sistema,

            cod_empresa_old,                  cod_empresa_new,
            num_solicitacao_old,              num_solicitacao_new,
            cnpj9_old,                        cnpj9_new,
            cnpj4_old,                        cnpj4_new,
            cnpj2_old,                        cnpj2_new,
            situacao_sol_old,                 situacao_sol_new,
            data_embarque_old,                data_embarque_new,
            tipo_faturamento_old,             tipo_faturamento_new,
            nome_programa
         ) values (
            'I',                              sysdate,
            sysdate,                          ws_usuario_rede,
            ws_maquina_rede,                  ws_aplicativo,
            ws_usuario_systextil,

            0,                                :new.cod_empresa,
            0,                                :new.num_solicitacao,
            0,                                :new.cnpj9,
            0,                                :new.cnpj4,
            0,                                :new.cnpj2,
            0,                                :new.situacao_sol,
            null,                             :new.data_embarque,
            0,                                :new.tipo_faturamento,
            v_nome_programa
         );
      end;
   end if;

   if updating
   then
      begin
         insert into fatu_735_log (
            tipo_ocorr,                      data_ocorr,
            hora_ocorr,                      usuario_rede,
            maquina_rede,                    aplicacao,
            usuario_sistema,

            cod_empresa_old,                 cod_empresa_new,
            num_solicitacao_old,             num_solicitacao_new,
            cnpj9_old,                       cnpj9_new,
            cnpj4_old,                       cnpj4_new,
            cnpj2_old,                       cnpj2_new,
            situacao_sol_old,                situacao_sol_new,
            data_embarque_old,               data_embarque_new,
            tipo_faturamento_old,            tipo_faturamento_new,
            nome_programa
         ) values (
            'U',                             sysdate,
            sysdate,                         ws_usuario_rede,
            ws_maquina_rede,                 ws_aplicativo,
            ws_usuario_systextil,

            :old.cod_empresa,                :new.cod_empresa,
            :old.num_solicitacao,            :new.num_solicitacao,
            :old.cnpj9,                      :new.cnpj9,
            :old.cnpj4,                      :new.cnpj4,
            :old.cnpj2,                      :new.cnpj2,
            :old.situacao_sol,               :new.situacao_sol,
            :old.data_embarque,              :new.data_embarque,
            :old.tipo_faturamento,           :new.tipo_faturamento,
            v_nome_programa
         );
      end;
   end if;

   if deleting
   then
      begin
         insert into fatu_735_log (
            tipo_ocorr,                      data_ocorr,
            hora_ocorr,                      usuario_rede,
            maquina_rede,                    aplicacao,
            usuario_sistema,

            cod_empresa_old,                 cod_empresa_new,
            num_solicitacao_old,             num_solicitacao_new,
            cnpj9_old,                       cnpj9_new,
            cnpj4_old,                       cnpj4_new,
            cnpj2_old,                       cnpj2_new,
            situacao_sol_old,                situacao_sol_new,
            data_embarque_old,               data_embarque_new,
            tipo_faturamento_old,            tipo_faturamento_new,
            nome_programa

         ) values (
            'D',                                   sysdate,
            sysdate,                               ws_usuario_rede,
            ws_maquina_rede,                       ws_aplicativo,
            ws_usuario_systextil,

            :old.cod_empresa,                0,
            :old.num_solicitacao,            0,
            :old.cnpj9,                      0,
            :old.cnpj4,                      0,
            :old.cnpj2,                      0,
            :old.situacao_sol,               0,
            :old.data_embarque,              null,
            :old.tipo_faturamento,           0,
            v_nome_programa
         );
      end;
   end if;

end inter_tr_fatu_735_log;


-- ALTER TRIGGER "INTER_TR_FATU_735_LOG" ENABLE
 

/

exec inter_pr_recompile;

