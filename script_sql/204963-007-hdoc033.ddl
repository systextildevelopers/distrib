-- Tipos de Não Conformidades

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('efic_f350', 'Cadastro de Tipos de Não Conformidades', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'efic_f350', 'efic_menu', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'efic_f350', 'efic_menu', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Cadastro de Tipos de Não Conformidades'
 where hdoc_036.codigo_programa = 'efic_f350'
   and hdoc_036.locale          = 'es_ES';
commit;

-- Ações Para Não Conformidades

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('efic_f351', 'Cadastro de Ações para Não Conformidades', 0,1);


insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'efic_f351', 'efic_menu', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'efic_f351', 'efic_menu', 1, 0, 'S', 'S', 'S', 'S');


update hdoc_036
   set hdoc_036.descricao       = 'Cadastro de Ações para Não Conformidades'
 where hdoc_036.codigo_programa = 'efic_f351'
   and hdoc_036.locale          = 'es_ES';
commit;

-- Relacionamento de Setores X Tipos de Não Conformidades

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('efic_f352', 'Relacionamento de Setores X Tipos de Não Conformidades', 0,1);


insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'efic_f352', 'efic_menu', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'efic_f352', 'efic_menu', 1, 0, 'S', 'S', 'S', 'S');


update hdoc_036
   set hdoc_036.descricao       = 'Relacionamento de Setores X Tipos de Não Conformidades'
 where hdoc_036.codigo_programa = 'efic_f352'
   and hdoc_036.locale          = 'es_ES';
commit;

-- Cadastro de Não Conformidades Produtivas

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('efic_f355', 'Cadastro de Não Conformidades Produtivas', 0,1);


insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'efic_f355', 'efic_menu', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'efic_f355', 'efic_menu', 1, 0, 'S', 'S', 'S', 'S');


update hdoc_036
   set hdoc_036.descricao       = 'Cadastro de Não Conformidades Produtivas'
 where hdoc_036.codigo_programa = 'efic_f355'
   and hdoc_036.locale          = 'es_ES';
commit;

