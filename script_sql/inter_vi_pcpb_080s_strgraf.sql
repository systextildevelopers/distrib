create or replace view inter_vi_pcpb_080s_strgraf as
select pcpb80.menor_sequencia,              pcpb_080."ORDEM_PRODUCAO",        pcpb_080."NIVEL_COMP",
       pcpb_080."GRUPO_COMP",               pcpb_080."SUBGRUPO_COMP",         pcpb_080."ITEM_COMP",
       pcpb_080."GRUPO_RECEITA",            pcpb_080."SUBGRUPO_RECEITA",      pcpb_080."ITEM_RECEITA",
       pcpb_080."PESO_PREVISTO",            pcpb_080."PESO_REAL",             pcpb_080."PESO_PROGRAMADO",
       pcpb_080."PESO_ENFESTADO",           pcpb_080."OPERADOR",              pcpb_080."DATA_EMISSAO",
       pcpb_080."HORA_EMISSAO",             pcpb_080."DATA_PESAGEM",          pcpb_080."HORA_PESAGEM",
       pcpb_080."LETRA",                    pcpb_080."PESAR_PRODUTO",         pcpb_080."SEQ_OPERACAO",
       pcpb_080."CONSUMO",                  pcpb_080."TIPO_CALCULO",          pcpb_080."CODIGO_OPERACAO",
       pcpb_080."SEQUENCIA_ESTRUTURA",      pcpb_080."SEQUENCIA_NIVEL5",      pcpb_080."GRUPO_RECEITA_PRINCIPAL",
       pcpb_080."SUBGRU_RECEITA_PRINCIPAL", pcpb_080."ITEM_RECEITA_PRINCIPAL",pcpb_080."NIVEL_RECEITA_PRINCIPAL",
       pcpb_080."ALTERNATIVA_PRINCIPAL",    pcpb_080."ALTERNATIVA_RECEITA",   pcpb_080."NUMERO_GRAFICO",
       pcpb_080."CONSUMO_RECEITA",          pcpb_080."ORDEM_REPROCESSO",      pcpb_080."CONS_UN_REC",
       pcpb_080."CENTRO_CUSTO",             pcpb_080."MOTIVO_REPESAGEM",      pcpb_080."NOME_PROGRAMA",
       pcpb_080."RESPONSAVEL_REPESAGEM",    pcpb_080."VOLUME_BANHO",          pcpb_080."TIPO_ORDEM", 
	   pcpb_080."CALL_OFF",          		pcpb_080."COUNTER"
from pcpb_080, (select min(pcpb_080.sequencia_nivel5) menor_sequencia,
                       pcpb_080.numero_grafico num_grafico,
                       pcpb_080.ordem_producao op,
                       pcpb_080.ordem_reprocesso op_repro,
                       pcpb_080.tipo_ordem tp_ord
                from pcpb_080
                group by pcpb_080.numero_grafico,
                         pcpb_080.ordem_producao,
                         pcpb_080.ordem_reprocesso,
                         pcpb_080.tipo_ordem
                order by min(pcpb_080.sequencia_nivel5)) pcpb80
where  pcpb80.num_grafico = pcpb_080.numero_grafico
  and ((pcpb80.tp_ord <> 4 
  and pcpb80.op       = pcpb_080.ordem_producao   
  and pcpb80.tp_ord   = pcpb_080.tipo_ordem)
   or (pcpb80.tp_ord  = 4 
  and pcpb80.op_repro = pcpb_080.ordem_reprocesso 
  and pcpb80.op_repro > 0 
  and pcpb80.tp_ord   = pcpb_080.tipo_ordem))
order by pcpb80.menor_sequencia,
         pcpb_080.sequencia_nivel5;
