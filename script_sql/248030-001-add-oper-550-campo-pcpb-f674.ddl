DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pcpb_f674';
   v_usuario varchar2(4000);

begin
   for reg_programa in programa
   loop
      begin
         select oper_550.usuario
         into   v_usuario
         from oper_550
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'seq_operacao';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa,
            nome_programa,               nome_subprograma,
            nome_field,                  requerido,
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu,
            reg_programa.programa,       ' ',
            'seq_operacao',           0,
            0,                           0
         );
         commit;
      end;
	  begin
         select oper_550.usuario
         into   v_usuario
         from oper_550
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'codigo_operacao';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa,
            nome_programa,               nome_subprograma,
            nome_field,                  requerido,
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu,
            reg_programa.programa,       ' ',
            'codigo_operacao',           0,
            0,                           0
         );
         commit;
      end;
	  begin
         select oper_550.usuario
         into   v_usuario
         from oper_550
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'maquina';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa,
            nome_programa,               nome_subprograma,
            nome_field,                  requerido,
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu,
            reg_programa.programa,       ' ',
            'maquina',           0,
            0,                           0
         );
         commit;
      end;	 
	  begin
         select oper_550.usuario
         into   v_usuario
         from oper_550
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'codigo_estagio';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa,
            nome_programa,               nome_subprograma,
            nome_field,                  requerido,
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu,
            reg_programa.programa,       ' ',
            'codigo_estagio',           0,
            0,                           0
         );
         commit;
      end;	
	  begin
         select oper_550.usuario
         into   v_usuario
         from oper_550
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'data_inicio';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa,
            nome_programa,               nome_subprograma,
            nome_field,                  requerido,
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu,
            reg_programa.programa,       ' ',
            'data_inicio',           0,
            0,                           0
         );
         commit;
      end;	
	  begin
         select oper_550.usuario
         into   v_usuario
         from oper_550
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'hora_inicio';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa,
            nome_programa,               nome_subprograma,
            nome_field,                  requerido,
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu,
            reg_programa.programa,       ' ',
            'hora_inicio',           0,
            0,                           0
         );
         commit;
      end;	 	    
	  begin
         select oper_550.usuario
         into   v_usuario
         from oper_550
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'data_termino';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa,
            nome_programa,               nome_subprograma,
            nome_field,                  requerido,
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu,
            reg_programa.programa,       ' ',
            'data_termino',           0,
            0,                           0
         );
         commit;
      end;	  
	  begin
         select oper_550.usuario
         into   v_usuario
         from oper_550
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'hora_termino';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa,
            nome_programa,               nome_subprograma,
            nome_field,                  requerido,
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu,
            reg_programa.programa,       ' ',
            'hora_termino',           0,
            0,                           0
         );
         commit;
      end;		  
   end loop;
end;

/

exec inter_pr_recompile;
