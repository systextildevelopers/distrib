create table obrf_109
(
       num_nota       NUMBER(9) default 0 not null, 
       serie          VARCHAR2(3) default '' not null, 
       empresa        NUMBER(3) default 0 not null, 
       num_pedido     NUMBER(9) default 0, 
       num_danfe      VARCHAR2(44) default '', 
       qtde_lida      NUMBER(9) default 0, 
       usuario_logado VARCHAR2(15) default '',
       data_leitura   DATE,
       numero_volume  VARCHAR2(18) default 0 not null,
       situacao_volume       NUMBER(1) default 0,
       volume_nao_encontrado VARCHAR2(18) default '',

       CONSTRAINT PK_OBRF_109 PRIMARY KEY (empresa, num_nota, serie, numero_volume)
);

/

 exec inter_pr_recompile;
