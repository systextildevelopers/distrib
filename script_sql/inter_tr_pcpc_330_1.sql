
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_330_1"
  before insert or
         delete or
         update of deposito, estoque_tag, pedido_venda, seq_item_pedido, tabela_origem_cardex on pcpc_330
  for each row

declare
   v_entrada_saida        estq_005.entrada_saida%type;
   v_transac_entrada      estq_005.transac_entrada%type;
   v_transacao_cancel     estq_005.codigo_transacao%type;
   v_tipo_volume          basi_205.tipo_volume%type;

   v_numero_documento     estq_300.numero_documento%type;
   v_serie_documento      estq_300.serie_documento%type;
   v_sequencia_documento  estq_300.sequencia_documento%type;
   v_cnpj_9               estq_300.cnpj_9%type;
   v_cnpj_4               estq_300.cnpj_4%type;
   v_cnpj_2               estq_300.cnpj_2%type;
   v_tabela_origem_cardex estq_300.tabela_origem%type := 'PCPC_330';

   v_cor_estoque          estq_300.item_estrutura%type;
   v_cor_movimento        estq_300.item_estrutura%type;

   v_data_cancelamento    date;

   v_lote_pedi_110        number(9) := 0;

   v_executa_trigger      number;
   v_endereca             number;

begin
   -- INICIO - L�gica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementa��o executada para limpeza da base de dados do cliente
   -- e replica��o dos dados para base hist�rico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - L�gica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if inserting or updating
   then
      v_endereca := 0;
      BEGIN
         select basi_205.endereca_caixa
         into v_endereca
         from basi_205
         where basi_205.codigo_deposito = :new.deposito;
      EXCEPTION
         when others then
           v_endereca := 0;
      END;

      if (v_endereca=0)
      then
         :new.endereco := '';
      end if;

      if (v_endereca=1)
      then
         BEGIN
            select basi_030.cor_de_estoque
            into v_cor_estoque
            from basi_030
            where basi_030.nivel_estrutura = :new.nivel
              and basi_030.referencia = :new.grupo;
         EXCEPTION
              when no_data_found then
                  v_cor_estoque := '000000';
         END;

         BEGIN
            select max(1)
            into v_endereca
            from estq_110
            where estq_110.deposito  = :new.deposito
              and estq_110.endereco  = :new.endereco
              and (estq_110.nivel    = :new.nivel or estq_110.nivel = '0')
              and (estq_110.grupo    = :new.grupo or estq_110.grupo = '00000')
              and (estq_110.subgrupo = :new.subgrupo or estq_110.subgrupo = '000')
              and ((estq_110.item     = :new.item     or estq_110.item     = '000000') or (v_cor_estoque='009999' and estq_110.item = v_cor_estoque)) ;
         EXCEPTION
           when no_data_found then
              raise_application_error(-20000,'ATEN��O! Dep�sito configurado para endere�amento de TAG. � necess�rio informar uma endere�o v�lido.');
         END;

      end if;
   end if;

   if v_executa_trigger = 0
   then
      -- Inicializa.NÚMEMRO_DOCUMENTO.
      v_numero_documento := 0;  
	  
	  if inserting or updating
      then
         -- se n�o for informada a tabela origem, assume que a tabela � a pr�pria PCPC_330
         if :new.tabela_origem_cardex is null or :new.tabela_origem_cardex = ' '
         then
            :new.tabela_origem_cardex := 'PCPC_330';
         end if;
      end if;

      if (:new.tabela_origem_cardex not in ('FATU_060', 'OBRF_015') or deleting)
      then

         if inserting or updating
         then
            -- verifica se esta associando ou transferindo a tag a um pedido com lotes
            -- diferente do lote do item do pedido.
            if inserting
            then
               if :new.pedido_venda > 0 and :new.seq_item_pedido > 0
               then
                  v_lote_pedi_110 := 0;
                  begin
                     select lote_empenhado into v_lote_pedi_110
                     from pedi_110
                     where pedi_110.pedido_venda    = :new.pedido_venda
                       and pedi_110.seq_item_pedido = :new.seq_item_pedido;

                     exception
                        when no_data_found then
                           v_lote_pedi_110 := 0;
                  end;

                  if v_lote_pedi_110 <> :new.lote and ( v_lote_pedi_110 > 0 or  :new.lote > 0)
                  then
                     raise_application_error(-20000,'A tag: '||to_char(:new.ordem_producao, '000000000')||
                                                    ' est� no lote: '||to_char(:new.lote, '000000')||
                                                    ' e a seq��ncia do item est� com o lote: '||to_char(v_lote_pedi_110,'000000'));
                  end if;
               end if;
            else
               if :new.pedido_venda > 0 and :new.seq_item_pedido > 0 and
                 (:new.pedido_venda <> :old.pedido_venda or :new.seq_item_pedido <> :old.seq_item_pedido)
               then
                  v_lote_pedi_110 := 0;
                  begin
                     select lote_empenhado into v_lote_pedi_110
                     from pedi_110
                     where pedi_110.pedido_venda    = :new.pedido_venda
                       and pedi_110.seq_item_pedido = :new.seq_item_pedido;

                     exception
                        when no_data_found then
                          v_lote_pedi_110 := 0;
                  end;

                  if v_lote_pedi_110 <> :new.lote  and ( v_lote_pedi_110 > 0 or :new.lote > 0)
                  then
                     raise_application_error(-20000,'A tag : '||to_char(:new.ordem_producao, '000000000')||
                                                    ' est� no lote: '||to_char(:new.lote, '000000')||
                                                    ' e a seq��ncia do item est� com o lote: '||to_char(v_lote_pedi_110,'000000'));
                  end if;
               end if;
            end if;

            -- verifica se � necess�rio fazer alguma conferencia
            if  (updating
            and (
                   (:old.estoque_tag not in (0,4,9) and :new.estoque_tag     in (0,4,9)) -- tag estava em estoque e nao esta mais
               or  (:old.estoque_tag     in (0,4,9) and :new.estoque_tag not in (0,4,9)) -- tag nao estava em estoque e agora esta
               or  (:old.deposito  <> :new.deposito and :new.estoque_tag not in (0,4,9)) -- deposito alterado EM ESTOQUE
                 ))
            then

               if :new.transacao_cardex = 0 and (:new.estoque_tag <> 0 or updating)
               then
                  raise_application_error(-20000,'Deve-se informar a transa��o de movimenta��o.');
               else
                  begin
                     select entrada_saida, transac_entrada into v_entrada_saida, v_transac_entrada
                     from estq_005
                     where codigo_transacao = :new.transacao_cardex;

                     exception
                        when no_data_found then
                           raise_application_error(-20000,'Transa��o informada n�o cadastrada.');
                  end;

                  if updating                                       -- Alterando tag
                  and :old.deposito  <> :new.deposito               -- Se foi alterado o deposito
                  and :old.estoque_tag not in (0,4,9)                -- TAG n�o estava em estoque
                  and :new.estoque_tag not in (0,4,9)                -- TAG n�o esta em estoque
                  and  v_entrada_saida      <> 'T'                  -- Consiste transacao transferencia
                  then
                     raise_application_error(-20000,'Dep�sito alterado, deve-se usar uma transa��o de transfer�ncia.');
                  end if;
               end if;

               if :new.data_cardex is null and (:new.estoque_tag <> 0 or updating)
               then
                  raise_application_error(-20000,'Deve-se informar a data de movimenta��o.');
               end if;
            end if;
         end if;

         if inserting and :new.estoque_tag <> 0
         then
            -- le se o dep�sito controla por volume
            select tipo_volume into v_tipo_volume from basi_205
            where codigo_deposito = :new.deposito;

            -- estoque_tag.: 0 - Em produ��o / 4 - Faturado
            -- v_tipo_volume: 1 - Controle por TAG.
            if :new.estoque_tag not in (0,4,9) or (v_tipo_volume <> 1 and :new.deposito > 0)
            then
               if v_entrada_saida <> 'E'
               then
                  raise_application_error(-20000,'Na inser��o de uma nova tag, a transa��o deve ser de entrada.');
               end if;

               -- se o dep�sito N�O for por tag, ent�o a tag entra como faturado
               if v_tipo_volume <> 1
               then
                  :new.estoque_tag := 4;
               end if;

               -- verifica qual o documento � que deve ser enviado para a estq_300,
               -- se � a nota fiscal ou se � a tag
               if   :new.nota_entr  is not null  and :new.nota_entr  > 0
               and (:new.serie_entr is not null  and :new.serie_entr <> ' '
               or   :new.serie_entr is not null  and :new.serie_entr <> '')
               and  :new.seq_entr   is not null  and :new.seq_entr  > 0
               then
                  v_numero_documento    := :new.nota_entr;
                  v_serie_documento     := :new.serie_entr;
                  v_sequencia_documento := :new.seq_entr;
                  v_cnpj_9              := :new.forn9_entr;
                  v_cnpj_4              := :new.forn4_entr;
                  v_cnpj_2              := :new.forn2_entr;
               else
                  v_numero_documento    := :new.nr_volume;
                  v_serie_documento     := ' ';
                  v_sequencia_documento := 0;
                  v_cnpj_9              := 0;
                  v_cnpj_4              := 0;
                  v_cnpj_2              := 0;
               end if;

               -- atualiza a data de inser��o com a data corrente (sysdate)
               :new.data_inclusao := trunc(sysdate,'dd');

               select basi_030.cor_de_estoque into v_cor_estoque from basi_030
               where basi_030.nivel_estrutura = :new.nivel
                 and basi_030.referencia      = :new.grupo;

               if v_cor_estoque <> '000000' and :new.item <> v_cor_estoque
               then  v_cor_movimento  := v_cor_estoque;
               else  v_cor_movimento  := :new.item;
               end if;

               -- Seta.NÚMEMRO_DOCUMENTO.da.Ficha_KARDEX, com.o.NÚMERO_ORDEM_DE_PRODUÇÃO.
               if v_numero_documento = 0
               then v_numero_documento := :new.ordem_producao;
               end if;

               inter_pr_insere_estq_300 (:new.deposito,          :new.nivel,             :new.grupo,
                                         :new.subgrupo,          v_cor_movimento,        :new.data_cardex,
                                         :new.lote,              v_numero_documento,      v_serie_documento,
                                         v_cnpj_9,               v_cnpj_4,                v_cnpj_2,
                                         v_sequencia_documento,
                                         :new.transacao_cardex,  'E',                     0,
                                         1,                      :new.valor_movto_cardex, :new.valor_contabil_cardex,
                                         :new.usuario_cardex,    v_tabela_origem_cardex,  :new.nome_prog_050,
                                         0.00);

            end if;
         end if;     -- if inserting

         if deleting
         then

            if :old.estoque_tag not in (0,4,9)
            then
               -- encontra a transa��o de cancelamento de produ��o
               select tran_est_produca into v_transacao_cancel from empr_001;

               -- encontra a data de lan�amento do movimento no estq_300,
               -- se a data do cancelamento (sysdate) for igual a data de inser��o
               -- do tag, ent�o a data de cancelamento ser� a data de entrada
               -- sen�o ser� a data de cancelamento (sysdate)
               if :old.data_inclusao is null
               or :old.data_inclusao <> trunc(sysdate,'dd')
               then
                  v_data_cancelamento := trunc(sysdate,'dd');
               else
                  v_data_cancelamento := :old.data_entrada;
               end if;

               select basi_030.cor_de_estoque into v_cor_estoque from basi_030
               where basi_030.nivel_estrutura = :old.nivel
                 and basi_030.referencia      = :old.grupo;

               if v_cor_estoque <> '000000' and :old.item <> v_cor_estoque
               then  v_cor_movimento  := v_cor_estoque;
               else  v_cor_movimento  := :old.item;
               end if;

               inter_pr_insere_estq_300 (:old.deposito,       :old.nivel,              :old.grupo,
                                         :old.subgrupo,       v_cor_movimento,         v_data_cancelamento,
                                         :old.lote,           :old.ordem_producao,     '',
                                         0,                   0,                       0,
                                         0,
                                         v_transacao_cancel,  'S',                     0,
                                         1,                   :old.valor_movto_cardex, :old.valor_contabil_cardex,
                                         'DELECAO',            v_tabela_origem_cardex, ' ',
                                         0.00);
            end if;
         end if;     -- if deleting


         if updating
         then

            -- se o tag j� estava em estoque, ent�o n�o deixa alterar a
            -- transacao e a data de entrada
            if  :old.estoque_tag not in (0,4,9)
            and :new.estoque_tag not in (0,4,9)
            then
               :new.transacao_ent := :old.transacao_ent;
               :new.data_entrada  := :old.data_entrada;
            end if;

            -- movimento de saida
            if   :old.estoque_tag not in (0,4,9)                   -- tag estava em estoque
            and (:old.deposito  <> :new.deposito                 -- deposito alterado
            or   :new.estoque_tag     in (0,4,9))                  -- tag tirada do estoque
            then

               if v_entrada_saida <> 'S' and v_entrada_saida <> 'T'
               then
                  raise_application_error(-20000,'N�o se pode realizar uma movimenta��o de sa�da com uma transacao de entrada.');
               end if;

               if  :new.nota_inclusao      is not null and :new.nota_inclusao       > 0
               and :new.serie_nota is not null and :new.serie_nota <> ' '
               and :new.seq_saida  is not null and :new.seq_saida   > 0
               and :new.nome_prog_050 <> 'inte_p021'
               then
                  v_numero_documento    := :new.nota_inclusao;
                  v_serie_documento     := :new.serie_nota;
                  v_sequencia_documento := :new.seq_saida;
                  v_cnpj_9              := 0;
                  v_cnpj_4              := 0;
                  v_cnpj_2              := 0;
               else
                  v_numero_documento    := :new.nr_volume;
                  v_serie_documento     := ' ';
                  v_sequencia_documento := 0;
                  v_cnpj_9              := 0;
                  v_cnpj_4              := 0;
                  v_cnpj_2              := 0;
               end if;

               select basi_030.cor_de_estoque into v_cor_estoque from basi_030
               where basi_030.nivel_estrutura = :old.nivel
                 and basi_030.referencia      = :old.grupo;

               if v_cor_estoque <> '000000' and :old.item <> v_cor_estoque
               then  v_cor_movimento  := v_cor_estoque;
               else  v_cor_movimento  := :old.item;
               end if;

               -- Seta.NÚMEMRO_DOCUMENTO.da.Ficha_KARDEX, com.o.NÚMERO_ORDEM_DE_PRODUÇÃO.
               if v_numero_documento = 0
               then v_numero_documento := :new.ordem_producao;
               end if;

               inter_pr_insere_estq_300 (:old.deposito,   :old.nivel,             :old.grupo,
                                         :old.subgrupo,   v_cor_movimento,        :new.data_cardex,
                                         :old.lote,       v_numero_documento,     v_serie_documento,
                                         v_cnpj_9,        v_cnpj_4,               v_cnpj_2,
                                         v_sequencia_documento,
                                         :new.transacao_cardex,  'S',                     0,
                                         1,               :new.valor_movto_cardex, :new.valor_contabil_cardex,
                                         :new.usuario_cardex,    v_tabela_origem_cardex,  :new.nome_prog_050,
                                         0.00);
            end if;


            -- movimento de entrada
            if   :new.estoque_tag not in (0,4,9)                   -- tag esta em estoque
            and (:old.deposito  <> :new.deposito                 -- deposito alterado
            or   :old.estoque_tag     in (0,4,9))                  -- tag nao estava em estoque
            then

               if v_entrada_saida <> 'T'
               then
                  if v_entrada_saida <> 'E'
                  then
                     raise_application_error(-20000,'N�o se pode realizar uma movimenta��o de entrada com uma transacao de sa�da.');
                  end if;

                  v_transac_entrada := :new.transacao_cardex;
               end if;

               if  (:new.nota_entr is not null and :new.nota_entr  > 0
               and :new.serie_entr is not null and :new.serie_entr <> ' '
               and :new.seq_entr is not null and :new.seq_entr  > 0
               and :new.nome_prog_050 <> 'pcpc_f190')
               or :new.nome_prog_050 = 'fatu_f330'
               then
                  v_numero_documento    := :new.nota_entr;
                  v_serie_documento     := :new.serie_entr;
                  v_sequencia_documento := :new.seq_entr;
                  v_cnpj_9              := :new.forn9_entr;
                  v_cnpj_4              := :new.forn4_entr;
                  v_cnpj_2              := :new.forn2_entr;
               else
                  if :new.nome_prog_050 = 'pcpc_f190'
                  then
                    v_numero_documento    := :old.nr_volume;
                  else
                    v_numero_documento    := :new.nr_volume;
                  end if;

                  v_serie_documento     := ' ';
                  v_sequencia_documento := 0;
                  v_cnpj_9              := 0;
                  v_cnpj_4              := 0;
                  v_cnpj_2              := 0;
               end if;

               select basi_030.cor_de_estoque into v_cor_estoque from basi_030
               where basi_030.nivel_estrutura = :new.nivel
                 and basi_030.referencia      = :new.grupo;

               if v_cor_estoque <> '000000' and :new.item <> v_cor_estoque
               then  v_cor_movimento  := v_cor_estoque;
               else  v_cor_movimento  := :new.item;
               end if;

               -- Seta.NÚMEMRO_DOCUMENTO.da.Ficha_KARDEX, com.o.NÚMERO_ORDEM_DE_PRODUÇÃO.
               if v_numero_documento = 0
               then v_numero_documento := :new.ordem_producao;
               end if;

               inter_pr_insere_estq_300 (:new.deposito,          :new.nivel,         :new.grupo,
                                         :new.subgrupo,          v_cor_movimento,    :new.data_cardex,
                                         :new.lote,              v_numero_documento,  v_serie_documento,
                                         v_cnpj_9,               v_cnpj_4,            v_cnpj_2,
                                         v_sequencia_documento,
                                         v_transac_entrada,      'E',                     0,
                                         1,                      :new.valor_movto_cardex, :new.valor_contabil_cardex,
                                         :new.usuario_cardex,    v_tabela_origem_cardex,  :new.nome_prog_050,
                                         0.00);
             end if;
         end if;     -- if updating
      end if;        -- if (:new.tabela_origem_cardex not in ('FATU_060', 'OBRF_015') or deleting)

      -- zera campos de controle da ficha cardex, assim n�o h� perigo da trigger atualizar
      -- o estoque com dados antigos, caso o usuario n�o passar estes parametros
      if inserting or updating
      then
         :new.transacao_cardex     := 0;
         :new.usuario_cardex       := ' ';
         :new.tabela_origem_cardex := ' ';
         :new.nome_prog_050        := ' ';
         :new.data_cardex          := null;
      end if;

      if inserting
      then
        :new.data_ult_movimento := :new.data_inclusao;
      end if;

      if updating
      then
        if :new.deposito <> :old.deposito
        then
            :new.data_ult_movimento := sysdate;
        end if;
      end if;
    end if;
end inter_tr_pcpc_330_1;


-- ALTER TRIGGER "INTER_TR_PCPC_330_1" ENABLE


/

exec inter_pr_recompile;
