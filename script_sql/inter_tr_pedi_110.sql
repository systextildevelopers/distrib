
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_110"
  before insert or delete or update of cd_it_pe_nivel99,       cd_it_pe_grupo,
                                       cd_it_pe_subgrupo,      cd_it_pe_item,
                                       codigo_deposito,        lote_empenhado,
                                       qtde_pedida,            qtde_afaturar,
                                       qtde_faturada,          cod_cancelamento
  on pedi_110
  for each row
declare
   v_qtde_a_empenhar         pedi_110.qtde_pedida%type;
   v_qtde_afaturar           pedi_110.qtde_afaturar%type;
   t_periodo_producao        pcpb_010.periodo_producao%type;
   v_unidade_medida          basi_030.unidade_medida%type;
   v_nat_vendas              fatu_501.nat_operacao_pedido%type;
   v_permitir_faturar_a_mais fatu_503.permitir_faturar_a_mais%type;
   v_codigo_empresa          pedi_100.codigo_empresa%type;
   v_executa_trigger         number(1);
   v_cont_serv               number(9);
   v_cod_nat_op              pedi_110.cod_nat_op%type;
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting
      then
         begin
            if v_cod_nat_op = null
            then
               raise_application_error(-20000,'ATEN��O! Item sem natureza de opera��o! N�o ser� permitido gravar o item.');
            end if;
         end;
		 
		 begin
			if :new.seq_item_pedido = 0
			then
				raise_application_error(-20000,'ATEN��O! Sequ�ncia do item do pedido de venda n�o pode ser zero.');
			end if;
		 end;

         begin
            select pedi_100.num_periodo_prod
            into   t_periodo_producao
            from pedi_100
            where pedi_100.pedido_venda = :new.pedido_venda;
            exception
            when others then
               t_periodo_producao := 0;
         end;

         if  :new.cod_cancelamento = 0                               -- item nao cancelado
         and inter_fn_dep_pronta_entrega(:new.codigo_deposito)  = 1  -- deposito for pronta entrega
         then
            -- empenha a quantidade maior entre a qtde_pedida e a qtde_afaturar
            if :new.qtde_pedida >= :new.qtde_afaturar
            then
               v_qtde_a_empenhar := :new.qtde_pedida   - :new.qtde_faturada;
            else
               v_qtde_a_empenhar := :new.qtde_afaturar - :new.qtde_faturada;
            end if;

            if v_qtde_a_empenhar > 0.00  -- se houver qtde a empenhar
            then
               -- empenha o item
               inter_pr_empenha_produto(:new.cd_it_pe_nivel99,  :new.cd_it_pe_grupo,
                                        :new.cd_it_pe_subgrupo, :new.cd_it_pe_item,
                                        :new.lote_empenhado,    :new.codigo_deposito,
                                        v_qtde_a_empenhar);
              -- procedure de controle da tabela de excessos (pedi_845)
              inter_pr_pedi_845(:new.cd_it_pe_nivel99,  :new.cd_it_pe_grupo,
                                :new.cd_it_pe_subgrupo, :new.cd_it_pe_item,
                                :new.lote_empenhado,    :new.codigo_deposito,
                                :new.pedido_venda,      :new.seq_item_pedido,
                                :new.qtde_pedida,       :new.qtde_afaturar,
                                :new.qtde_faturada);

            end if; -- if v_qtde_a_empenhar > 0.00
         end if;    -- if :new.cod_cancelamento = 0

         -- Atualiza tabela de planejamento com a quantidade planejada.
         insert into tmrp_041
                (periodo_producao,     area_producao,
                 nr_pedido_ordem,      nivel_estrutura,
                 grupo_estrutura,      subgru_estrutura,
                 item_estrutura,       qtde_planejada,
                 seq_pedido_ordem)
         values (t_periodo_producao,   6,
                 :new.pedido_venda,    :new.cd_it_pe_nivel99,
                 :new.cd_it_pe_grupo,  :new.cd_it_pe_subgrupo,
                 :new.cd_it_pe_item,   :new.qtde_pedida,
                 :new.seq_item_pedido);

      end if; -- if inserting

      if deleting
      then

         if :old.qtde_sugerida > 0
         then
            raise_application_error(-20000,'ATEN��O! Item do pedido possui sugest�o e n�o pode ser eliminado.');
         end if;

         begin
            select pedi_100.num_periodo_prod
            into   t_periodo_producao
            from pedi_100
            where pedi_100.pedido_venda = :old.pedido_venda;
            exception
            when others then
               t_periodo_producao := 0;
         end;

         if  :old.cod_cancelamento = 0                                     -- item nao cancelado
         and inter_fn_dep_pronta_entrega(:old.codigo_deposito)  = 1      -- deposito for pronta entrega
         then
            -- desempenha a quantidade maior entre a (qtde_pedida - qtde_faturada) e a qtde_afaturar
            if (:old.qtde_pedida - :old.qtde_faturada) >= :old.qtde_afaturar
            then
               v_qtde_a_empenhar := :old.qtde_pedida - :old.qtde_faturada;
            else
               v_qtde_a_empenhar := :old.qtde_afaturar;
            end if;
            if v_qtde_a_empenhar > 0.00  -- se houver qtde a desempenhar
            then
               -- desempenha o item
               inter_pr_empenha_produto(:old.cd_it_pe_nivel99,  :old.cd_it_pe_grupo,
                                        :old.cd_it_pe_subgrupo, :old.cd_it_pe_item,
                                        :old.lote_empenhado,    :old.codigo_deposito,
                                        v_qtde_a_empenhar * (-1.0));
               -- procedure de controle da tabela de excessos (pedi_845)
               inter_pr_pedi_845(:old.cd_it_pe_nivel99,  :old.cd_it_pe_grupo,
                                 :old.cd_it_pe_subgrupo, :old.cd_it_pe_item,
                                 :old.lote_empenhado,    :old.codigo_deposito,
                                 :old.pedido_venda,      :old.seq_item_pedido,
			      :old.qtde_pedida,       :old.qtde_afaturar,
			      :old.qtde_faturada);

           end if; -- if v_qtde_a_empenhar > 0.00
         end if;   -- if :new.cod_cancelamento = 0

         delete from tmrp_041
         where tmrp_041.periodo_producao = t_periodo_producao
           and tmrp_041.area_producao    = 6
           and tmrp_041.nr_pedido_ordem  = :old.pedido_venda
           and tmrp_041.nivel_estrutura  = :old.cd_it_pe_nivel99
           and tmrp_041.grupo_estrutura  = :old.cd_it_pe_grupo
           and tmrp_041.subgru_estrutura = :old.cd_it_pe_subgrupo
           and tmrp_041.item_estrutura   = :old.cd_it_pe_item
           and tmrp_041.seq_pedido_ordem = :old.seq_item_pedido;

         -- Eliminar o relacionamento de itens a serem industrializados (pedi_112)
         begin
            delete from pedi_112
            where pedi_112.ped_facc_pe_ve_it = :old.pedido_venda
              and pedi_112.ped_facc_sq_it_pe = :old.seq_item_pedido ;
            exception
            when others then
               null;
         end;
		 
		 -- Eliminar o relacionamento de itens (pedi_266)
         begin
            delete from pedi_266
            where pedi_266.pedido    = :old.pedido_venda
              and pedi_266.sequencia = :old.seq_item_pedido ;
            exception
            when others then
               null;
         end;

      end if;      -- if deleting

      if updating
      then
         -- Consist�ncia para vericar quantidade coletada para o pedido,
         -- se a quantidade a faturar for maior que a quantidade pedida,
         -- para pedidos de pe�as confecionadas, bloqueia para faturamento.
         -- n�o pode se aplicar para pedidos com unidade de medida em KG.
         begin
            select basi_030.unidade_medida
            into   v_unidade_medida
            from basi_030
            where basi_030.nivel_estrutura = :new.cd_it_pe_nivel99
              and basi_030.referencia      = :new.cd_it_pe_grupo;
            exception
            when others then
               v_unidade_medida := '';
         end;

         begin
            select pedi_100.num_periodo_prod, pedi_100.codigo_empresa
            into   t_periodo_producao,        v_codigo_empresa
            from pedi_100
            where pedi_100.pedido_venda = :new.pedido_venda;
            exception
            when others then
               t_periodo_producao := 0;
         end;

         begin
            select fatu_503.permitir_faturar_a_mais
            into   v_permitir_faturar_a_mais
            from fatu_503
            where fatu_503.codigo_empresa = v_codigo_empresa;
            exception
            when others then
               v_permitir_faturar_a_mais := 0;
         end;

         if :new.qtde_afaturar < 0.00
         then
            raise_application_error(-20000,'ATEN��O! Quantidade faturada n�o pode ser negativa. Entre em contato com o CPD.');
         end if;

         if :new.qtde_afaturar < 0.00
         then
            raise_application_error(-20000,'ATEN��O! Quantidade a faturar n�o pode ser negativa. Entre em contato com o CPD.');
         end if;

/*         if  :new.cd_it_pe_nivel99 = '1'
         and :new.qtde_afaturar    > :old.qtde_pedida
         and v_unidade_medida     <> 'KG'
         then

            if v_permitir_faturar_a_mais = 0
            then raise_application_error(-20000,'ATEN��O! Quantidade a faturar n�o pode ser maior que a quantidade pedida. Entre em contato com o CPD.');
            end if;

         end if;
*/
         -- Se for presta��o de servi�o ser� poss�vel alterar o produto
         begin
            select fatu_501.nat_operacao_pedido into v_nat_vendas
            from fatu_501
            where fatu_501.codigo_empresa = v_codigo_empresa;
            exception
            when others then
               v_nat_vendas := 0;
         end;

         select count(*) into v_cont_serv
	       from pedi_080
         where pedi_080.natur_operacao = v_nat_vendas
           and pedi_080.tipo_natureza  > 0;

         --*************************************************************************************************
         --Se for presta��o de servi�o ser� permitido alterar o c�digo do produto.
         --*************************************************************************************************

         if (:old.cd_it_pe_nivel99  <> :new.cd_it_pe_nivel99
          or :old.cd_it_pe_grupo    <> :new.cd_it_pe_grupo
          or :old.cd_it_pe_subgrupo <> :new.cd_it_pe_subgrupo
          or :old.cd_it_pe_item     <> :new.cd_it_pe_item) and (v_cont_serv > 0)
         then

            --**********************************************************************************************
            -- Na altera��o de produto, dever� retirar as quantidades atualizadas na inser��o do produto ...
            --**********************************************************************************************

            begin
               select pedi_100.num_periodo_prod
               into   t_periodo_producao
               from pedi_100
               where pedi_100.pedido_venda = :old.pedido_venda;
               exception
                  when others then
                     t_periodo_producao := 0;
            end;

            if  :old.cod_cancelamento = 0                                   -- item nao cancelado
            and inter_fn_dep_pronta_entrega(:old.codigo_deposito)  = 1      -- deposito for pronta entrega
            then
               -- desempenha a quantidade maior entre a (qtde_pedida - qtde_faturada) e a qtde_afaturar
               if (:old.qtde_pedida - :old.qtde_faturada) >= :old.qtde_afaturar
               then
                  v_qtde_a_empenhar := :old.qtde_pedida - :old.qtde_faturada;
               else
                   v_qtde_a_empenhar := :old.qtde_afaturar;
               end if;
               if v_qtde_a_empenhar > 0.00  -- se houver qtde a desempenhar
               then
                  -- desempenha o item
                  inter_pr_empenha_produto(:old.cd_it_pe_nivel99,  :old.cd_it_pe_grupo,
                                           :old.cd_it_pe_subgrupo, :old.cd_it_pe_item,
                                           :old.lote_empenhado,    :old.codigo_deposito,
                                           v_qtde_a_empenhar * (-1.0));
                   -- procedure de controle da tabela de excessos (pedi_845)
                  inter_pr_pedi_845(:old.cd_it_pe_nivel99,  :old.cd_it_pe_grupo,
                                    :old.cd_it_pe_subgrupo, :old.cd_it_pe_item,
                                    :old.lote_empenhado,    :old.codigo_deposito,
                                    :old.pedido_venda,      :old.seq_item_pedido,
                                    :old.qtde_pedida,       :old.qtde_afaturar,
                                    :old.qtde_faturada);

              end if; -- if v_qtde_a_empenhar > 0.00
            end if;   -- if :new.cod_cancelamento = 0

            delete from tmrp_041
            where tmrp_041.periodo_producao = t_periodo_producao
              and tmrp_041.area_producao    = 6
              and tmrp_041.nr_pedido_ordem  = :old.pedido_venda
              and tmrp_041.nivel_estrutura  = :old.cd_it_pe_nivel99
              and tmrp_041.grupo_estrutura  = :old.cd_it_pe_grupo
              and tmrp_041.subgru_estrutura = :old.cd_it_pe_subgrupo
              and tmrp_041.item_estrutura   = :old.cd_it_pe_item
              and tmrp_041.seq_pedido_ordem = :old.seq_item_pedido;

            --*****************************************************************************************************
            -- ... e logo ap�s dever� ser acumulado novamente para o novo produto, com base na quantidade anterior.
            --*****************************************************************************************************

            if  :old.cod_cancelamento = 0                               -- item nao cancelado
            and inter_fn_dep_pronta_entrega(:old.codigo_deposito)  = 1  -- deposito for pronta entrega
            then
               -- empenha a quantidade maior entre a qtde_pedida e a qtde_afaturar
               if :old.qtde_pedida >= :old.qtde_afaturar
               then
                  v_qtde_a_empenhar := :old.qtde_pedida   - :old.qtde_faturada;
               else
                  v_qtde_a_empenhar := :old.qtde_afaturar - :old.qtde_faturada;
               end if;

               if v_qtde_a_empenhar > 0.00  -- se houver qtde a empenhar
               then
                  -- empenha o item
                  inter_pr_empenha_produto(:new.cd_it_pe_nivel99,  :new.cd_it_pe_grupo,
                                           :new.cd_it_pe_subgrupo, :new.cd_it_pe_item,
                                           :old.lote_empenhado,    :old.codigo_deposito,
                                           v_qtde_a_empenhar);
                  -- procedure de controle da tabela de excessos (pedi_845)
                  inter_pr_pedi_845(:new.cd_it_pe_nivel99,  :new.cd_it_pe_grupo,
                                    :new.cd_it_pe_subgrupo, :new.cd_it_pe_item,
                                    :old.lote_empenhado,    :old.codigo_deposito,
                                    :old.pedido_venda,      :old.seq_item_pedido,
                                    :old.qtde_pedida,       :old.qtde_afaturar,
                                    :old.qtde_faturada);

               end if; -- if v_qtde_a_empenhar > 0.00
            end if;    -- if :new.cod_cancelamento = 0

            -- Atualiza tabela de planejamento com a quantidade planejada.
            insert into tmrp_041
                   (periodo_producao,     area_producao,
                    nr_pedido_ordem,      nivel_estrutura,
                    grupo_estrutura,      subgru_estrutura,
                    item_estrutura,       qtde_planejada,
                    seq_pedido_ordem)
            values (t_periodo_producao,   6,
                    :old.pedido_venda,    :new.cd_it_pe_nivel99,
                    :new.cd_it_pe_grupo,  :new.cd_it_pe_subgrupo,
                    :new.cd_it_pe_item,   :old.qtde_pedida,
                    :old.seq_item_pedido);

         end if;

         --********************************************************************************************************

         -- Nao permite alterar o produto.
         if (:old.cd_it_pe_nivel99  <> :new.cd_it_pe_nivel99
          or :old.cd_it_pe_grupo    <> :new.cd_it_pe_grupo
          or :old.cd_it_pe_subgrupo <> :new.cd_it_pe_subgrupo
          or :old.cd_it_pe_item     <> :new.cd_it_pe_item) and (v_cont_serv = 0)
         then
            raise_application_error(-20000,'Produto nao pode se alterado.');
         end if;

         -- Nao permite alterar o lote.
         -- if :old.lote_empenhado <> :new.lote_empenhado
         -- then
         --    raise_application_error(-20000,'Lote do produto nao pode se alterado.');
         -- end if;

               -- CHAMA PROCEDURE QUE INSERE NA TABELA TEMPORARIA DO RECALCULO
         inter_pr_marca_referencia_op('0', '00000',
                                      '000', '00000',
                                      :new.pedido_venda, 6);


         -- So executa o empenho/desempenho se as condicoes abaixo forem respeitadas.
         if :new.cod_cancelamento  > 0                                     -- item cancelado
         or :old.codigo_deposito  <> :new.codigo_deposito                  -- alterado o deposito
         or :old.qtde_pedida      <> :new.qtde_pedida                      -- qtde_pedida alterada
         or :old.qtde_faturada    <> :new.qtde_faturada                    -- qtde_faturada alterada
         or :old.qtde_afaturar    <> :new.qtde_afaturar                    -- qtde_afaturar alterada
         then
            -- se quentidade pedida for alterada
            if :old.qtde_pedida <> :new.qtde_pedida
            then
               update tmrp_041
               set qtde_planejada = :new.qtde_pedida
               where tmrp_041.periodo_producao = t_periodo_producao
                 and tmrp_041.area_producao    = 6
                 and tmrp_041.nr_pedido_ordem  = :new.pedido_venda
                 and tmrp_041.nivel_estrutura  = :new.cd_it_pe_nivel99
                 and tmrp_041.grupo_estrutura  = :new.cd_it_pe_grupo
                 and tmrp_041.subgru_estrutura = :new.cd_it_pe_subgrupo
                 and tmrp_041.item_estrutura   = :new.cd_it_pe_item
                 and tmrp_041.seq_pedido_ordem = :new.seq_item_pedido;
            end if;  -- if :old.qtde_pedida <> :new.qtde_pedida

            -- ITEM CANCELADO
            if :new.cod_cancelamento  > 0                                  -- item cancelado
            then
               if inter_fn_dep_pronta_entrega(:old.codigo_deposito) = 1  -- deposito for pronta entrega
               then
                  -- desempenha a quantidade maior entre a (qtde_pedida - qtde_faturada) e a qtde_afaturar
                  if (:old.qtde_pedida - :old.qtde_faturada) >= :old.qtde_afaturar
                  then
                     v_qtde_a_empenhar := :old.qtde_pedida - :old.qtde_faturada;
                  else
                     v_qtde_a_empenhar := :old.qtde_afaturar;
                  end if;
                  if v_qtde_a_empenhar > 0.00                              -- se houver qtde a desempenhar
                  then
                     -- desempenha o item
                     inter_pr_empenha_produto(:old.cd_it_pe_nivel99,  :old.cd_it_pe_grupo,
                                              :old.cd_it_pe_subgrupo, :old.cd_it_pe_item,
                                              :old.lote_empenhado,    :old.codigo_deposito,
                                              v_qtde_a_empenhar * (-1.0));

                     -- procedure de controle da tabela de excessos (pedi_845)
                     inter_pr_pedi_845(:old.cd_it_pe_nivel99,  :old.cd_it_pe_grupo,
                                       :old.cd_it_pe_subgrupo, :old.cd_it_pe_item,
                                       :old.lote_empenhado,    :old.codigo_deposito,
                                       :old.pedido_venda,      :old.seq_item_pedido,
                                       :old.qtde_pedida,       :old.qtde_afaturar,
                                       :old.qtde_faturada);

                  end if; -- if v_qtde_a_empenhar > 0.00
               end if;    -- if inter_fn_dep_pronta_entrega(:old.codigo_deposito) = 1

               delete from tmrp_041
               where tmrp_041.periodo_producao = t_periodo_producao
                 and tmrp_041.area_producao    = 6
                 and tmrp_041.nr_pedido_ordem  = :old.pedido_venda
                 and tmrp_041.nivel_estrutura  = :old.cd_it_pe_nivel99
                 and tmrp_041.grupo_estrutura  = :old.cd_it_pe_grupo
                 and tmrp_041.subgru_estrutura = :old.cd_it_pe_subgrupo
                 and tmrp_041.item_estrutura   = :old.cd_it_pe_item
                 and tmrp_041.seq_pedido_ordem = :old.seq_item_pedido;

               -- Eliminar o relacionamento de itens a serem industrializados (pedi_112)
               begin
                  delete from pedi_112
                  where pedi_112.ped_facc_pe_ve_it = :old.pedido_venda
                    and pedi_112.ped_facc_sq_it_pe = :old.seq_item_pedido ;
                  exception
                     when others then
                        null;
               end;

            else  -- se nao foi cancelado
               -- se trocou o deposito, entao desempenha qtde do deposito antigo
               -- e empenha a qtde no deposito novo
               if :old.codigo_deposito <> :new.codigo_deposito
               then
                  -- desempenha, se preciso, a quantidade no deposito antigo
                  if inter_fn_dep_pronta_entrega(:old.codigo_deposito) = 1 -- deposito pronta entrega
                  then
                     -- desempenha a quantidade maior entre a (qtde_pedida - qtde_faturada) e a qtde_afaturar
                     if (:old.qtde_pedida - :old.qtde_faturada) >= :old.qtde_afaturar
                     then
                        v_qtde_a_empenhar := :old.qtde_pedida - :old.qtde_faturada;
                     else
                       v_qtde_a_empenhar := :old.qtde_afaturar;
                     end if;
                     if v_qtde_a_empenhar > 0.00  -- se houver qtde a desempenhar
                     then
                        -- desempenha o item
                        inter_pr_empenha_produto(:old.cd_it_pe_nivel99,  :old.cd_it_pe_grupo,
                                                 :old.cd_it_pe_subgrupo, :old.cd_it_pe_item,
                                                 :old.lote_empenhado,    :old.codigo_deposito,
                                                 v_qtde_a_empenhar * (-1.0));

                        -- procedure de controle da tabela de excessos (pedi_845)
                        inter_pr_pedi_845(:old.cd_it_pe_nivel99,  :old.cd_it_pe_grupo,
                                          :old.cd_it_pe_subgrupo, :old.cd_it_pe_item,
                                          :old.lote_empenhado,    :old.codigo_deposito,
                                          :old.pedido_venda,      :old.seq_item_pedido,
				       :old.qtde_pedida,       :old.qtde_afaturar,
				       :old.qtde_faturada);

                     end if; -- if new.qtde_pedida > :new.qtde_faturada
                  end if;
                  -- empenha, se preciso, a quantidade no deposito novo
                  if inter_fn_dep_pronta_entrega(:new.codigo_deposito) = 1 -- deposito pronta entrega
                  then
                     -- empenha a quantidade maior entre a (qtde_pedida - qtde_faturada) e a qtde_afaturar
                     if (:new.qtde_pedida - :new.qtde_faturada) >= :new.qtde_afaturar
                     then
                        v_qtde_a_empenhar := :new.qtde_pedida - :new.qtde_faturada;
                     else
                        v_qtde_a_empenhar := :new.qtde_afaturar;
                     end if;
                     if v_qtde_a_empenhar > 0.00  -- se houver qtde a desempenhar
                     then
                        -- desempenha o item
                        inter_pr_empenha_produto(:new.cd_it_pe_nivel99,  :new.cd_it_pe_grupo,
                                                 :new.cd_it_pe_subgrupo, :new.cd_it_pe_item,
                                                 :new.lote_empenhado,    :new.codigo_deposito,
                                                 v_qtde_a_empenhar);

                       -- procedure de controle da tabela de excessos (pedi_845)
                       inter_pr_pedi_845(:new.cd_it_pe_nivel99,  :new.cd_it_pe_grupo,
                                         :new.cd_it_pe_subgrupo, :new.cd_it_pe_item,
                                         :new.lote_empenhado,    :new.codigo_deposito,
                                         :new.pedido_venda,      :new.seq_item_pedido,
                                         :new.qtde_pedida,       :new.qtde_afaturar,
                                         :new.qtde_faturada);

                     end if; -- if new.qtde_pedida > :new.qtde_faturada
                  end if;    -- if inter_fn_dep_pronta_entrega(:new.codigo_deposito) = 1
               else -- if :old.codigo_deposito <> :new.codigo_deposito
                  -- se o deposito for de pronta entrega, entao empenha/desempenha
                  if inter_fn_dep_pronta_entrega(:new.codigo_deposito) = 1  -- deposito for pronta entrega
                  then
                     -- se quentidade pedida for alterada
                     if :old.qtde_pedida <> :new.qtde_pedida
                     then
                        if :old.qtde_pedida > :old.qtde_afaturar
                        then
                           v_qtde_a_empenhar := :new.qtde_pedida - :old.qtde_pedida;
                        else
                           v_qtde_a_empenhar := :new.qtde_pedida - :old.qtde_afaturar;
                        end if;

                        -- empenha/desempenha o item
                        inter_pr_empenha_produto(:new.cd_it_pe_nivel99,  :new.cd_it_pe_grupo,
                                                 :new.cd_it_pe_subgrupo, :new.cd_it_pe_item,
                                                 :new.lote_empenhado,    :new.codigo_deposito,
                                                 v_qtde_a_empenhar);

                        -- procedure de controle da tabela de excessos (pedi_845)
                        inter_pr_pedi_845(:new.cd_it_pe_nivel99,  :new.cd_it_pe_grupo,
                                          :new.cd_it_pe_subgrupo, :new.cd_it_pe_item,
                                          :new.lote_empenhado,    :new.codigo_deposito,
                                          :new.pedido_venda,      :new.seq_item_pedido,
                                          :new.qtde_pedida,       :new.qtde_afaturar,
                                          :new.qtde_faturada);

                     end if;  -- if :old.qtde_pedida <> :new.qtde_pedida
                     -- se a qtde_afaturar for alterada e a qtde_faturada nao for alteda
                     -- empenha ou desempenha
                     if  :old.qtde_afaturar <> :new.qtde_afaturar
                     and :old.qtde_faturada  = :new.qtde_faturada
                     then
                        v_qtde_a_empenhar := 0.00;
                        v_qtde_afaturar   := :new.qtde_afaturar - :old.qtde_afaturar;
                        -- empenha item, se a qtde_afaturar for maior que saldo do item
                        if v_qtde_afaturar > 0 and (:new.qtde_afaturar) > (:new.qtde_pedida - :new.qtde_faturada)
                        then
                           -- se a qtde_afaturar anterior for menor que
                           if (:old.qtde_afaturar) <= (:new.qtde_pedida - :new.qtde_faturada)
                           then
                              v_qtde_a_empenhar := :new.qtde_afaturar - (:new.qtde_pedida - :new.qtde_faturada);
                           else
                              v_qtde_a_empenhar := v_qtde_afaturar;
                           end if;  -- if (:old.qtde_afaturar) <= (:new.qtde_pedida - :new.qtde_faturada)
                        end if;     -- if v_qtde_afaturar > 0 and (:new.qtde_afaturar) > (:new.qtde_pedida - :new.qtde_faturada)

                        -- Desempenha item, se a qtde_afaturar era maior que saldo do item
                        if v_qtde_afaturar < 0 and (:old.qtde_afaturar) > (:new.qtde_pedida - :new.qtde_faturada)
                        then
                           -- se a qtde_afaturar anterior for menor que.
                           if (:new.qtde_afaturar) <= (:new.qtde_pedida - :new.qtde_faturada)
                           then
                              v_qtde_a_empenhar := (:new.qtde_pedida - :new.qtde_faturada) - :old.qtde_afaturar;
                           else
                              v_qtde_a_empenhar := v_qtde_afaturar;
                           end if;  -- if (:new.qtde_afaturar) <= (:new.qtde_pedida - :new.qtde_faturada)
                        end if;     -- if v_qtde_afaturar < 0 and (:old.qtde_afaturar) > (:new.qtde_pedida - :new.qtde_faturada)

                        if v_qtde_a_empenhar <> 0.00
                        then
                           -- empenha/desempenha o item
                           inter_pr_empenha_produto(:new.cd_it_pe_nivel99,  :new.cd_it_pe_grupo,
                                                    :new.cd_it_pe_subgrupo, :new.cd_it_pe_item,
                                                    :new.lote_empenhado,    :new.codigo_deposito,
                                                    v_qtde_a_empenhar);

                           if (:new.qtde_afaturar) > (:new.qtde_pedida - :new.qtde_faturada)
                           then
                              -- procedure de controle da tabela de excessos (pedi_845)
                              inter_pr_pedi_845(:new.cd_it_pe_nivel99,  :new.cd_it_pe_grupo,
                                                :new.cd_it_pe_subgrupo, :new.cd_it_pe_item,
                                                :new.lote_empenhado,    :new.codigo_deposito,
                                                :new.pedido_venda,  	  :new.seq_item_pedido,
                                                :new.qtde_pedida,       :new.qtde_afaturar,
				                                        :new.qtde_faturada);
                           end if;
                        end if;  -- if v_qtde_a_empenhar <> 0.00
                     end if;     -- if :old.qtde_afaturar <> :new.qtde_afaturar

                     -- Se a qtde_faturada foi alterar, empenha/desempenha o item.
                     if :old.qtde_faturada <> :new.qtde_faturada
                     then
                        -- se a nova qtde_faturada for maior que a antiga, quer dizer que se esta faturando
                        -- assim deve desempenhar a quantidade
                        if :new.qtde_faturada > :old.qtde_faturada
                        then
                           -- se a situacao for de faturada total, desempenha o saldo
                           -- senao desempenha apenas q quantidade faturada
                           if :new.situacao_fatu_it = 1 and :new.qtde_faturada <= :new.qtde_pedida
                           then
                              if :new.qtde_afaturar > 0
                              then v_qtde_a_empenhar := :old.qtde_faturada - :new.qtde_faturada ;
                              else v_qtde_a_empenhar := :old.qtde_faturada - :new.qtde_pedida ;
                              end if;

                           else
                              v_qtde_a_empenhar := :old.qtde_faturada - :new.qtde_faturada;
                           end if;
                        else -- Cancelamento de faturamento
                           -- Se a situacao estava como faturada total, empenha o saldo
                           -- senao empenha apenas o ultimo faturamento.
                           if :old.situacao_fatu_it = 1
                           then
                              v_qtde_a_empenhar := :new.qtde_pedida   - :new.qtde_faturada;
                           else
                              v_qtde_a_empenhar := :old.qtde_faturada - :new.qtde_faturada;
                           end if;
                        end if;

                        -- Empenha/desempenha o item.
                        inter_pr_empenha_produto(:new.cd_it_pe_nivel99,  :new.cd_it_pe_grupo,
                                                 :new.cd_it_pe_subgrupo, :new.cd_it_pe_item,
                                                 :new.lote_empenhado,    :new.codigo_deposito,
                                                 v_qtde_a_empenhar);

                        -- Procedure de controle da tabela de excessos (pedi_845).
                        inter_pr_pedi_845(:new.cd_it_pe_nivel99,  :new.cd_it_pe_grupo,
                                          :new.cd_it_pe_subgrupo, :new.cd_it_pe_item,
                                          :new.lote_empenhado,    :new.codigo_deposito,
                                          :new.pedido_venda,      :new.seq_item_pedido,
                                          :new.qtde_pedida,       :new.qtde_afaturar,
                                          :new.qtde_faturada);

                     end if;  -- if :old.qtde_faturada  = :new.qtde_faturada
                  end if;     -- if inter_fn_dep_pronta_entrega(:new.codigo_deposito) = 1
               end if;        -- if :old.codigo_deposito <> :new.codigo_deposito
            end if;           -- if :new.cod_cancelamento  > 0
         end if;              -- se foi alterado cod_cancelamento, deposito, qtde_pedida, a_faturar, faturada
      end if;                 -- if updating
   end if;
end inter_tr_pedi_110;

-- ALTER TRIGGER "INTER_TR_PEDI_110" ENABLE


/

exec inter_pr_recompile;
