DECLARE
      CURSOR cad_usuario IS
      SELECT usuario, empresa
      FROM hdoc_030;
BEGIN
            
      FOR cad_usuario_cr IN cad_usuario
      LOOP
         begin
            insert into oper_550(
                  usuario,
                  empresa,
                  nome_programa,
                  nome_subprograma,
                  nome_field,
                  requerido,
                  acessivel,
                  inicia_campo) 
            values(
                  cad_usuario_cr.usuario, 
                  cad_usuario_cr.empresa, 
                  'pcpb_f182', 
                  ' ',
                  'cod_nuance', 
                  0, 
                  0, 
                  0
            );
         exception when others then null;
         end;
      END LOOP;
      COMMIT;
END;
/
commit;
