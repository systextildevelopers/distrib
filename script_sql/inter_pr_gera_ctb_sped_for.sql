create or replace procedure inter_pr_gera_ctb_sped_for(p_cod_empresa   IN NUMBER,
                                                       p_exercicio     IN NUMBER,
                                                       p_per_inicial   IN DATE,
                                                       p_per_final     IN DATE,
                                                       p_nome_programa IN VARCHAR2,
                                                       p_inclu_exce    IN NUMBER,
                                                       p_cod_usuario   IN NUMBER,
                                                       p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar as tabelas sped_cta_i550 - Razao/diario de fornecedores
   --
   -- Historicos
   --
   -- Data    Autor    Observacoes
   --


   -- busca fornecedores para saldos anteriores
   cursor fornecedores is
      select supr_010.fornecedor9, supr_010.fornecedor4,
             supr_010.fornecedor2, supr_010.codigo_contabil,
             supr_010.nome_fornecedor
      from supr_010
      where ((exists (select r.grupo_estrutura
                  from rcnb_060 r,
                       cont_002 c
                  where r.tipo_registro       = 546
                   and r.nr_solicitacao       = 2
                   and r.item_estrutura       = p_cod_empresa  --cod_empresa
                   and r.item_estrutura_str   = 'F'
                   and r.campo_numerico       = 1 -- inclusao
                   and r.grupo_estrutura      = supr_010.codigo_contabil
                   and c.codigo_empresa       = r.item_estrutura
                   and c.inc_exc              = r.campo_numerico
                   and r.item_estrutura_str   = c.tipo_cli_for
                   and c.ind_opcao_parametro  = 1)
           or (not exists (select r.grupo_estrutura
                  from rcnb_060 r,
                       cont_002 c
                  where r.tipo_registro       = 546
                   and r.nr_solicitacao       = 2
                   and r.item_estrutura       = p_cod_empresa  --cod_empresa
                   and r.item_estrutura_str   = 'F'
                   and r.campo_numerico       = 2  -- exclu
                   and r.grupo_estrutura      = supr_010.codigo_contabil
                   and c.codigo_empresa       = r.item_estrutura
                   and c.inc_exc              = r.campo_numerico
                   and r.item_estrutura_str   = c.tipo_cli_for
                   and c.ind_opcao_parametro  = 1)
                 and exists (select 1  from cont_002
                             where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                               and cont_002.tipo_cli_for        = 'F'
                               and cont_002.ind_opcao_parametro = 1
                               and cont_002.inc_exc             = 2) )
           )
       or not exists (select 1  from cont_002
           where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
             and cont_002.tipo_cli_for        = 'F'
             and cont_002.ind_opcao_parametro = 1
             and cont_002.inc_exc            in (1,2)))
       -- FILTRA FORNECEDOR
       and (((p_inclu_exce = 1
       and exists (select 1 from rcnb_060
                   where rcnb_060.tipo_registro    = 141
                     and rcnb_060.nr_solicitacao   = 455
                     and rcnb_060.subgru_estrutura = p_cod_usuario
                     and rcnb_060.grupo_estrutura  = supr_010.fornecedor9
                     and rcnb_060.item_estrutura   = supr_010.fornecedor4
                     and rcnb_060.nivel_estrutura  = supr_010.fornecedor2))
         or  (p_inclu_exce = 2
       and not exists (select 1 from rcnb_060
                        where rcnb_060.tipo_registro    = 141
                          and rcnb_060.nr_solicitacao   = 455
                          and rcnb_060.subgru_estrutura = p_cod_usuario
                          and rcnb_060.grupo_estrutura  = supr_010.fornecedor9
                          and rcnb_060.item_estrutura   = supr_010.fornecedor4
                          and rcnb_060.nivel_estrutura  = supr_010.fornecedor2)))
           or p_nome_programa = 'cont_f001');


   -- busca saldos anteriores
   cursor saldos(p_cod_empresa NUMBER, p_data_ini DATE, p_fornecedor9 NUMBER, p_fornecedor4 NUMBER, p_fornecedor2 NUMBER) IS
      select cpag_010.codigo_empresa,      cpag_010.nr_duplicata,
             cpag_010.parcela,             cpag_010.cgc_9,
             cpag_010.cgc_4,               cpag_010.cgc_2,
             cpag_010.tipo_titulo,         cpag_010.data_transacao,
             cpag_010.codigo_historico,    cont_010.historico_contab,
             (cpag_010.valor_parcela - cpag_010.valor_inss - cpag_010.valor_iss -
             decode(fatu_502.contab_irrf,1,
                    decode(cpag_040.controle_irrf, 0, cpag_010.valor_irrf, cpag_010.valor_irrf * (-1.0)), 0.00)) SALDO_PARCELA
      from cpag_010,
           cont_010,
           cpag_040,
           fatu_502
      where cpag_010.codigo_empresa in (select codigo_empresa from fatu_500
                                        where fatu_500.codigo_matriz = p_cod_empresa)
       and   (((exists (select r.subgru_estrutura
                                       from rcnb_060 r,
                                            cont_002 c
                                       where r.grupo_estrutura     = 815
                                        and r.item_estrutura       = p_cod_empresa -- cod_empresa
                                        and r.item_estrutura_str   = 'F'
                                        and r.nivel_estrutura      = 1 --inclu
                                        and cpag_010.tipo_titulo   = r.subgru_estrutura
                                        and c.codigo_empresa       = r.item_estrutura
                                        and c.inc_exc              = r.nivel_estrutura
                                        and r.item_estrutura_str   = c.tipo_cli_for
                                        and c.ind_opcao_parametro  = 0))


       or   (not exists (select r.subgru_estrutura
                                       from rcnb_060 r,
                                            cont_002 c
                                       where r.grupo_estrutura     = 815
                                        and r.item_estrutura       = p_cod_empresa -- cod_empresa
                                        and r.item_estrutura_str   = 'F'
                                        and r.nivel_estrutura      = 2 -- exclu
                                        and r.subgru_estrutura    = cpag_010.tipo_titulo
                                        and c.codigo_empresa       = r.item_estrutura
                                        and c.inc_exc              = r.nivel_estrutura
                                        and r.item_estrutura_str   = c.tipo_cli_for
                           and c.ind_opcao_parametro  = 0)
                           and exists (select 1  from cont_002
                                       where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                                         and cont_002.tipo_cli_for        = 'F'
                                         and cont_002.ind_opcao_parametro = 0
                                         and cont_002.inc_exc             = 2))
       )
      or not exists (select 1  from cont_002
                  where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                    and cont_002.tipo_cli_for        = 'F'
                    and cont_002.ind_opcao_parametro = 0)) -- ou sem cadastro
       -- FILTRA FORNECEDOR
       and (((p_inclu_exce = 1
       and exists (select 1 from rcnb_060
                   where rcnb_060.tipo_registro    = 141
                     and rcnb_060.nr_solicitacao   = 455
                     and rcnb_060.subgru_estrutura = p_cod_usuario
                     and rcnb_060.grupo_estrutura  = cpag_010.cgc_9
                     and rcnb_060.item_estrutura   = cpag_010.cgc_4
                     and rcnb_060.nivel_estrutura  = cpag_010.cgc_2))
         or  (p_inclu_exce = 2
       and not exists (select 1 from rcnb_060
                        where rcnb_060.tipo_registro    = 141
                          and rcnb_060.nr_solicitacao   = 455
                          and rcnb_060.subgru_estrutura = p_cod_usuario
                          and rcnb_060.grupo_estrutura  = cpag_010.cgc_9
                          and rcnb_060.item_estrutura   = cpag_010.cgc_4
                          and rcnb_060.nivel_estrutura  = cpag_010.cgc_2)))
           or p_nome_programa = 'cont_f001')

      and   cpag_010.data_transacao   <= p_data_ini
      and   cpag_010.previsao          = 0
      and   cpag_010.cod_cancelamento  = 0
      and   cont_010.codigo_historico  = cpag_010.codigo_historico
      and   cpag_010.codigo_empresa    = fatu_502.codigo_empresa
      and   cpag_010.tipo_titulo       = cpag_040.tipo_titulo
      and   cpag_010.cgc_9             = p_fornecedor9
      and   cpag_010.cgc_4             = p_fornecedor4
      and   cpag_010.cgc_2             = p_fornecedor2
      and   cont_010.tipo_historico   <> 'NC'
      and   cont_010.diario            = 1;


  -- SUBSTITUICAO DE TITULOS
  cursor cpag010_sub(p_cod_empresa NUMBER, p_data_ini DATE, p_data_fim DATE, p_fornecedor9 NUMBER, p_fornecedor4 NUMBER, p_fornecedor2 NUMBER) IS
      select distinct cpag_010.codigo_empresa,      cpag_010_sub.nr_duplicata,
                      cpag_010_sub.parcela,         cpag_010_sub.cgc_9,
                      cpag_010_sub.cgc_4,           cpag_010_sub.cgc_2,
                      cpag_010_sub.tipo_titulo,     cpag_010.data_transacao,
                      cpag_010_sub.codigo_historico,cont_010.historico_contab,
                     (cpag_010_sub.valor_parcela - cpag_010_sub.valor_inss - cpag_010_sub.valor_iss -
                      decode(fatu_502.contab_irrf,1,
                             decode(cpag_040.controle_irrf, 0, cpag_010_sub.valor_irrf, cpag_010_sub.valor_irrf * (-1.0)), 0.00)) SALDO_PARCELA
      from cpag_010,
           cpag_010 cpag_010_sub,
           cont_010,
           cpag_040,
           fatu_502
      where cpag_010.codigo_empresa in (select codigo_empresa from fatu_500
                                        where fatu_500.codigo_matriz = p_cod_empresa)
             -- FILTRA O TIPO TITULO
       and   (((exists (select r.subgru_estrutura
                        from rcnb_060 r,
                             cont_002 c
                        where r.grupo_estrutura     = 815
                          and r.item_estrutura       = p_cod_empresa -- cod_empresa
                          and r.item_estrutura_str   = 'F'
                          and r.nivel_estrutura      = 1 --inclu
                          and cpag_010_sub.tipo_titulo   = r.subgru_estrutura
                          and c.codigo_empresa       = r.item_estrutura
                          and c.inc_exc              = r.nivel_estrutura
                          and r.item_estrutura_str   = c.tipo_cli_for
                          and c.ind_opcao_parametro  = 0))


       or   (not exists (select r.subgru_estrutura
                         from rcnb_060 r,
                              cont_002 c
                         where r.grupo_estrutura     = 815
                           and r.item_estrutura       = p_cod_empresa -- cod_empresa
                           and r.item_estrutura_str   = 'F'
                           and r.nivel_estrutura      = 2 -- exclu
                           and r.subgru_estrutura    = cpag_010_sub.tipo_titulo
                           and c.codigo_empresa       = r.item_estrutura
                           and c.inc_exc              = r.nivel_estrutura
                           and r.item_estrutura_str   = c.tipo_cli_for
                           and c.ind_opcao_parametro  = 0)
                           and exists (select 1  from cont_002
                                       where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                                         and cont_002.tipo_cli_for        = 'F'
                                         and cont_002.ind_opcao_parametro = 0
                                         and cont_002.inc_exc             = 2))
       )
      or not exists (select 1  from cont_002
                  where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                    and cont_002.tipo_cli_for        = 'F'
                    and cont_002.ind_opcao_parametro = 0)) -- ou sem cadastro

      and   cpag_010.data_transacao   >= p_data_ini
      and   cpag_010.data_transacao   <= p_data_fim
      and   cpag_010.situacao         in (0,3)
      and   cpag_010.previsao          = 0
      and   cpag_010.cod_cancelamento  = 0
      and   cpag_010.codigo_empresa    = fatu_502.codigo_empresa
      and   cont_010.codigo_historico  = cpag_010.codigo_historico
      and   cont_010.tipo_historico   <> 'NC'
      and   cont_010.diario            = 1
      and   cpag_010.tipo_titulo       = cpag_040.tipo_titulo

      and   cpag_010_sub.situacao      = 1 -- substituido
      and   cpag_010_sub.num_tit_subs  > 0
      and   cpag_010.nr_duplicata      = cpag_010_sub.num_tit_subs
      and   cpag_010.tipo_titulo       = cpag_010_sub.tipo_tit_subs
      and   cpag_010.cgc_9             = cpag_010_sub.cgc9_subs
      and   cpag_010.cgc_4             = cpag_010_sub.cgc4_subs
      and   cpag_010.cgc_2             = cpag_010_sub.cgc2_subs
      and   cpag_010.cgc_9             = p_fornecedor9
      and   cpag_010.cgc_4             = p_fornecedor4
      and   cpag_010.cgc_2             = p_fornecedor2;


   -- entrada de titulos
   cursor cpag010(p_cod_empresa NUMBER, p_data_ini DATE, p_data_fim DATE, p_fornecedor9 NUMBER, p_fornecedor4 NUMBER, p_fornecedor2 NUMBER) IS
      select cpag_010.codigo_empresa,      cpag_010.nr_duplicata,
             cpag_010.parcela,             cpag_010.cgc_9,
             cpag_010.cgc_4,               cpag_010.cgc_2,
             cpag_010.tipo_titulo,         cpag_010.data_transacao,
             cpag_010.codigo_historico,    cont_010.historico_contab,
            (cpag_010.valor_parcela - cpag_010.valor_inss - cpag_010.valor_iss -
             decode(fatu_502.contab_irrf,1,
                    decode(cpag_040.controle_irrf, 0, cpag_010.valor_irrf, cpag_010.valor_irrf * (-1.0)), 0.00)) SALDO_PARCELA
      from cpag_010,
           cont_010,
           cpag_040,
           fatu_502
      where cpag_010.codigo_empresa in (select codigo_empresa from fatu_500
                                        where fatu_500.codigo_matriz = p_cod_empresa)
             -- FILTRA O TIPO TITULO
       and   (((exists (select r.subgru_estrutura
                        from rcnb_060 r,
                             cont_002 c
                        where r.grupo_estrutura     = 815
                          and r.item_estrutura       = p_cod_empresa -- cod_empresa
                          and r.item_estrutura_str   = 'F'
                          and r.nivel_estrutura      = 1 --inclu
                          and cpag_010.tipo_titulo   = r.subgru_estrutura
                          and c.codigo_empresa       = r.item_estrutura
                          and c.inc_exc              = r.nivel_estrutura
                          and r.item_estrutura_str   = c.tipo_cli_for
                          and c.ind_opcao_parametro  = 0))


       or   (not exists (select r.subgru_estrutura
                         from rcnb_060 r,
                              cont_002 c
                         where r.grupo_estrutura     = 815
                           and r.item_estrutura       = p_cod_empresa -- cod_empresa
                           and r.item_estrutura_str   = 'F'
                           and r.nivel_estrutura      = 2 -- exclu
                           and r.subgru_estrutura    = cpag_010.tipo_titulo
                           and c.codigo_empresa       = r.item_estrutura
                           and c.inc_exc              = r.nivel_estrutura
                           and r.item_estrutura_str   = c.tipo_cli_for
                           and c.ind_opcao_parametro  = 0)
                           and exists (select 1  from cont_002
                                       where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                                         and cont_002.tipo_cli_for        = 'F'
                                         and cont_002.ind_opcao_parametro = 0
                                         and cont_002.inc_exc             = 2))
       )
      or not exists (select 1  from cont_002
                  where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                    and cont_002.tipo_cli_for        = 'F'
                    and cont_002.ind_opcao_parametro = 0)) -- ou sem cadastro

      and   cpag_010.data_transacao   >= p_data_ini
      and   cpag_010.data_transacao   <= p_data_fim
      and   cpag_010.situacao         in (0,1,3)
      and   cpag_010.previsao          = 0
      and   cpag_010.cod_cancelamento  = 0
      and   cpag_010.codigo_empresa    = fatu_502.codigo_empresa
      and   cont_010.codigo_historico  = cpag_010.codigo_historico
      and   cont_010.tipo_historico   <> 'NC'
      and   cont_010.diario            = 1
      and   cpag_010.tipo_titulo       = cpag_040.tipo_titulo
      and   cpag_010.cgc_9             = p_fornecedor9
      and   cpag_010.cgc_4             = p_fornecedor4
      and   cpag_010.cgc_2             = p_fornecedor2;


   -- baixa de titulos
   cursor cpag015(p_cod_empresa NUMBER, p_data_ini DATE, p_data_fim DATE, p_fornecedor9 NUMBER, p_fornecedor4 NUMBER, p_fornecedor2 NUMBER) IS
      select cpag_010.codigo_empresa,      cpag_010.nr_duplicata,
             cpag_010.parcela,             cpag_010.cgc_9,
             cpag_010.cgc_4,               cpag_010.cgc_2,
             cpag_010.tipo_titulo,         cpag_015.sequencia_pagto,
             cpag_015.data_pagamento,
             cpag_015.valor_pago,          cpag_015.valor_juros,
             cpag_015.valor_descontos,     cpag_015.codigo_historico,
             cpag_015.vlr_var_cambial,

             cont_010.historico_contab,    cont_010.sinal_titulo

      from cpag_015,
           cpag_010,
           cont_010,
           cont_010 cont_010_c
      where cpag_015.data_pagamento   >= p_data_ini
      and   cpag_015.data_pagamento   <= p_data_fim
      and   cpag_010.codigo_empresa   in (select codigo_empresa from fatu_500
                                          where fatu_500.codigo_matriz = p_cod_empresa)

       and   (((exists (select r.subgru_estrutura
                                       from rcnb_060 r,
                                            cont_002 c
                                       where r.grupo_estrutura     = 815
                                        and r.item_estrutura       = p_cod_empresa -- cod_empresa
                                        and r.item_estrutura_str   = 'F'
                                        and r.nivel_estrutura      = 1 --inclu
                                        and cpag_010.tipo_titulo   = r.subgru_estrutura
                                        and c.codigo_empresa       = r.item_estrutura
                                        and c.inc_exc              = r.nivel_estrutura
                                        and r.item_estrutura_str   = c.tipo_cli_for
                                        and c.ind_opcao_parametro  = 0))


       or   (not exists (select r.subgru_estrutura
                         from rcnb_060 r,
                              cont_002 c
                         where r.grupo_estrutura     = 815
                           and r.item_estrutura       = p_cod_empresa -- cod_empresa
                           and r.item_estrutura_str   = 'F'
                           and r.nivel_estrutura      = 2 -- exclu
                           and r.subgru_estrutura    = cpag_010.tipo_titulo
                           and c.codigo_empresa       = r.item_estrutura
                           and c.inc_exc              = r.nivel_estrutura
                           and r.item_estrutura_str   = c.tipo_cli_for
                           and c.ind_opcao_parametro  = 0)
              and exists (select 1  from cont_002
                          where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                            and cont_002.tipo_cli_for        = 'F'
                            and cont_002.ind_opcao_parametro = 0
                            and cont_002.inc_exc             = 2))
       )
      or not exists (select 1  from cont_002
                  where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                    and cont_002.tipo_cli_for        = 'F'
                    and cont_002.ind_opcao_parametro = 0)) -- ou sem cadastro

      and   cpag_010.nr_duplicata      = cpag_015.dupl_for_nrduppag
      and   cpag_010.parcela           = cpag_015.dupl_for_no_parc
      and   cpag_010.cgc_9             = cpag_015.dupl_for_for_cli9
      and   cpag_010.cgc_4             = cpag_015.dupl_for_for_cli4
      and   cpag_010.cgc_2             = cpag_015.dupl_for_for_cli2
      and   cpag_010.tipo_titulo       = cpag_015.dupl_for_tipo_tit
      and   cont_010.codigo_historico   = cpag_015.codigo_historico
      and   cont_010_c.codigo_historico = cpag_010.codigo_historico
      and   cont_010.tipo_historico    <> 'NC'
      and   cont_010_c.diario           = 1
      and   cont_010.sinal_titulo     in (1,2)
      and   cpag_010.cgc_9             = p_fornecedor9
      and   cpag_010.cgc_4             = p_fornecedor4
      and   cpag_010.cgc_2             = p_fornecedor2;


   v_erro           EXCEPTION;

   v_registro_livro_razao     sped_ctb_i550.registro_livro_razao%type;
   v_per_anterior             cont_500.per_final%type;
   v_valor_movimento          cpag_015.valor_pago%type;
   v_valor_movimento_deb      cpag_015.valor_pago%type;
   v_valor_movimento_cre      cpag_015.valor_pago%type;

   v_gerou_capa_livro_fornec  number(4);
   v_saldo_inicial_fornec     number(13,2);
   v_saldo_inicial_fornec_livro number(13,2);
   v_total_pago               number(13,2);
   encontrou_movimento        boolean;

begin
   p_des_erro       := NULL;

   select count(*)
   into   v_gerou_capa_livro_fornec
   from sped_ctb_i012
   where cod_empresa    = p_cod_empresa
   and   exercicio      = p_exercicio
   and   tipo_livro_aux = 'F';

   -- quando executado atravez do programa cont_e455 deve ser eliminado so registro para serem carregados novamente
   if p_nome_programa = 'cont_e455'
   then
      delete from sped_ctb_i550 s
      where s.cod_empresa    = p_cod_empresa
        and s.tipo_livro_aux = 'F'
        and s.nome_programa  = p_nome_programa;
   end if;

   if v_gerou_capa_livro_fornec > 0 or p_nome_programa = 'cont_e455'
   then



      if p_nome_programa = 'cont_f001'
      then
         -- Gera as Informacoes da Definicao do layout do Razao/diario de clientes (SPED_I500)
         inter_pr_gera_ctb_sped_i500 (p_cod_empresa, p_exercicio, 'F', p_des_erro);
         IF  p_des_erro IS NOT NULL
         THEN
            RAISE v_erro;
         END IF;
      end if;



      ---------------------- INICIO ROTINA DE CALCULO DOS OS SALDOS INICIAIS ---------------------

      -- seta a data do final do exercicio anterior
      v_per_anterior := p_per_inicial - 1;

      -- inicializa saldos anteriores

      v_saldo_inicial_fornec := 0.00;

      for reg_fornecedores in fornecedores
      loop

         encontrou_movimento := false;

         -- entrada de titulos
         for reg_cpag010 in cpag010(p_cod_empresa, p_per_inicial, p_per_final,reg_fornecedores.fornecedor9,reg_fornecedores.fornecedor4,reg_fornecedores.fornecedor2)
         loop
            encontrou_movimento := true;

            -- monta a linha que vai ser utilizada na exportacao dos dados
            begin
               v_valor_movimento_deb := 0.00;
               v_valor_movimento_cre := reg_cpag010.saldo_parcela;

               v_registro_livro_razao := ltrim(to_char(reg_cpag010.cgc_9, '900000000'))             || '/' ||
                                         ltrim(to_char(reg_cpag010.cgc_4, '0000'))                  || '-' ||
                                         ltrim(to_char(reg_cpag010.cgc_2, '00'))                    || '|' ||
                                         ltrim(reg_fornecedores.nome_fornecedor)                         || '|' ||
                                         ltrim(to_char(reg_cpag010.data_transacao, 'DD/MM/YY'))     || '|' ||
                                         ltrim(reg_cpag010.historico_contab)                        || '|' ||
                                         ltrim(to_char(reg_cpag010.nr_duplicata, '000000000'))      || '/' ||
                                         ltrim(reg_cpag010.parcela)                                 || '|' ||
                                         ltrim(replace(replace(replace(to_char(v_valor_movimento_deb, '999999990.00'), '.', ';'), ',', '.'), ';', ',')) || '|' ||
                                         ltrim(replace(replace(replace(to_char(v_valor_movimento_cre, '999999990.00'), '.', ';'), ',', '.'), ';', ','));


            exception
               when others then
                  p_des_erro := 'Erro na montagem do registro do Razao de Fornecedores (1) ' || Chr(10) || SQLERRM;
                  RAISE V_ERRO;
            end;

            begin
               insert into sped_ctb_i550
                 (cod_empresa,                   exercicio,
                  tipo_livro_aux,                codigo_empresa_titulo,
                  numero_titulo,                 parcela_titulo,
                  cgc_9_titulo,                  cgc_4_titulo,
                  cgc_2_titulo,                  tipo_titulo,
                  seq_pagamento,                 nome_cliente_fornec,
                  cod_historico,                 desc_historico,
                  data_movimento,                valor_movimento_deb,
                  valor_movimento_cre,           tabela_origem,
                  registro_livro_razao,          nome_programa,
                  codigo_contabil)
               values
                 (p_cod_empresa,                 p_exercicio,
                  'F',                           reg_cpag010.codigo_empresa,
                  reg_cpag010.nr_duplicata,      reg_cpag010.parcela,
                  reg_cpag010.cgc_9,             reg_cpag010.cgc_4,
                  reg_cpag010.cgc_2,             reg_cpag010.tipo_titulo,
                  -1,                             reg_fornecedores.nome_fornecedor,
                  reg_cpag010.codigo_historico,  reg_cpag010.historico_contab,
                  reg_cpag010.data_transacao,    v_valor_movimento_deb,
                  v_valor_movimento_cre,         'CPAG_010',
                  v_registro_livro_razao,        p_nome_programa,
                  reg_fornecedores.codigo_contabil);

            exception
               when others then
                  p_des_erro := 'Erro na inclusao da tabela sped_ctb_i550 - Fornecedores (1) ' || Chr(10) || SQLERRM;
                  RAISE V_ERRO;
            end;
            commit;

         end loop;

         ---------------------- FIM DA ROTINA DE CALCULO DAS ENTADAS DOS TITULOS (COLUNA CREDITO ) ---

         ---------------------- INICIO ROTINA DE CALCULO DAS ENTADAS DOS TITULOS (COLUNA DEDITO) ---

         -- entrada de titulos
         for reg_cpag010_sub in cpag010_sub(p_cod_empresa, p_per_inicial, p_per_final,reg_fornecedores.fornecedor9,reg_fornecedores.fornecedor4,reg_fornecedores.fornecedor2)
         loop
            encontrou_movimento := true;

            -- monta a linha que vai ser utilizada na exportacao dos dados
            begin
               v_valor_movimento_deb := reg_cpag010_sub.saldo_parcela;
               v_valor_movimento_cre := 0.00;

               v_registro_livro_razao := ltrim(to_char(reg_cpag010_sub.cgc_9, '900000000'))             || '/' ||
                                         ltrim(to_char(reg_cpag010_sub.cgc_4, '0000'))                  || '-' ||
                                         ltrim(to_char(reg_cpag010_sub.cgc_2, '00'))                    || '|' ||
                                         ltrim(reg_fornecedores.nome_fornecedor)                         || '|' ||
                                         ltrim(to_char(reg_cpag010_sub.data_transacao, 'DD/MM/YY'))     || '|' ||
                                         ltrim(reg_cpag010_sub.historico_contab)                        || '|' ||
                                         ltrim(to_char(reg_cpag010_sub.nr_duplicata, '000000000'))      || '/' ||
                                         ltrim(reg_cpag010_sub.parcela)                                 || '|' ||
                                         ltrim(replace(replace(replace(to_char(v_valor_movimento_deb, '999999990.00'), '.', ';'), ',', '.'), ';', ',')) || '|' ||
                                         ltrim(replace(replace(replace(to_char(v_valor_movimento_cre, '999999990.00'), '.', ';'), ',', '.'), ';', ','));


            exception
               when others then
                  p_des_erro := 'Erro na montagem do registro do Razao de Fornecedores (1) ' || Chr(10) || SQLERRM;
                  RAISE V_ERRO;
            end;

            begin
               insert into sped_ctb_i550
                 (cod_empresa,                   exercicio,
                  tipo_livro_aux,                codigo_empresa_titulo,
                  numero_titulo,                 parcela_titulo,
                  cgc_9_titulo,                  cgc_4_titulo,
                  cgc_2_titulo,                  tipo_titulo,
                  seq_pagamento,                 nome_cliente_fornec,
                  cod_historico,                 desc_historico,
                  data_movimento,                valor_movimento_deb,
                  valor_movimento_cre,           tabela_origem,
                  registro_livro_razao,          nome_programa,
                  codigo_contabil)
               values
                 (p_cod_empresa,                     p_exercicio,
                  'F',                               reg_cpag010_sub.codigo_empresa,
                  reg_cpag010_sub.nr_duplicata,      reg_cpag010_sub.parcela,
                  reg_cpag010_sub.cgc_9,             reg_cpag010_sub.cgc_4,
                  reg_cpag010_sub.cgc_2,             reg_cpag010_sub.tipo_titulo,
                  -2,                                reg_fornecedores.nome_fornecedor,
                  reg_cpag010_sub.codigo_historico,  reg_cpag010_sub.historico_contab,
                  reg_cpag010_sub.data_transacao,    v_valor_movimento_deb,
                  v_valor_movimento_cre,             'CPAG_010',
                  v_registro_livro_razao,            p_nome_programa,
                  reg_fornecedores.codigo_contabil);

            exception
               when others then
                  p_des_erro := 'Erro na inclusao da tabela sped_ctb_i550 - Fornecedores (1) ' || Chr(10) || SQLERRM;
                  RAISE V_ERRO;
            end;
            commit;

         end loop;

         ---------------------- FIM DA ROTINA DE CALCULO DAS ENTADAS DOS TITULOS (COLUNA DEDITO ) ---

         ---------------------- INICIO ROTINA DE CALCULO DOS PAGAMENTOS DOS TITULOS (COLUNAS DEBITO E CREDITO) ----

         -- baixa de titulos
         for reg_cpag015 in cpag015(p_cod_empresa, p_per_inicial, p_per_final,reg_fornecedores.fornecedor9,reg_fornecedores.fornecedor4,reg_fornecedores.fornecedor2)
         loop
            encontrou_movimento := true;

            -- encontra o valor liquido do movimento (valor contabil do movimento)
            v_valor_movimento := reg_cpag015.valor_pago - reg_cpag015.valor_juros + reg_cpag015.valor_descontos - reg_cpag015.vlr_var_cambial;

            -- se o valor for zero, nao exporta o lancamento
            if v_valor_movimento <> 0.00
            then

               -- seta o valor para as colunas corretas (debito/credito), sendo:
               -- se for baixa de titulo e o valor for positivo e porque se esta baixando um titulo e tem valor pago
               -- ou se for estorno, mas o valor e negativo, quer dizer que e um estorno de provisao de juros.
               -- nestes dois casos, esta se subtraindo o valor da conta de clientes, ou seja, se esta gerando um
               -- credito.
               -- Caso contrario esta se aumentando o valor da conta de clientes, ou seja, esta gerando um debito.

               if (reg_cpag015.sinal_titulo = 1 and v_valor_movimento > 0.00)
               or (reg_cpag015.sinal_titulo = 2 and v_valor_movimento < 0.00)
               then
                  v_valor_movimento_deb := v_valor_movimento;
                  v_valor_movimento_cre := 0.00;
               else
                  v_valor_movimento_deb := 0.00;
                  v_valor_movimento_cre := v_valor_movimento;
               end if;

               -- transforma o valor em positivo, no caso de o valor de juros for maior que o do valor pago
               if v_valor_movimento_deb < 0.00
               then
                  v_valor_movimento_deb := v_valor_movimento_deb * (-1.00);
               end if;

               -- transforma o valor em positivo, no caso de o valor de juros for maior que o do valor pago
               if v_valor_movimento_cre < 0.00
               then
                  v_valor_movimento_cre := v_valor_movimento_cre * (-1.00);
               end if;

               -- monta a linha que vai ser utilizada na exportacao dos dados
               begin
                  v_registro_livro_razao := ltrim(to_char(reg_cpag015.cgc_9, '900000000'))             || '/' ||
                                            ltrim(to_char(reg_cpag015.cgc_4, '0000'))                  || '-' ||
                                            ltrim(to_char(reg_cpag015.cgc_2, '00'))                    || '|' ||
                                            ltrim(reg_fornecedores.nome_fornecedor)                         || '|' ||
                                            ltrim(to_char(reg_cpag015.data_pagamento, 'DD/MM/YY'))     || '|' ||
                                            ltrim(reg_cpag015.historico_contab)                        || '|' ||
                                            ltrim(to_char(reg_cpag015.nr_duplicata, '000000000'))      || '/' ||
                                            ltrim(reg_cpag015.parcela)                                 || '|' ||
                                            ltrim(replace(replace(replace(to_char(v_valor_movimento_deb, '999999990.00'), '.', ';'), ',', '.'), ';', ',')) || '|' ||
                                            ltrim(replace(replace(replace(to_char(v_valor_movimento_cre, '999999990.00'), '.', ';'), ',', '.'), ';', ','));

               exception
                  when others then
                     p_des_erro := 'Erro na montagem do registro do Razao de Fornecedores (2) ' || Chr(10) || SQLERRM;
                     RAISE V_ERRO;
               end;

               begin
                  insert into sped_ctb_i550
                    (cod_empresa,                   exercicio,
                     tipo_livro_aux,                codigo_empresa_titulo,
                     numero_titulo,                 parcela_titulo,
                     cgc_9_titulo,                  cgc_4_titulo,
                     cgc_2_titulo,                  tipo_titulo,
                     seq_pagamento,                 nome_cliente_fornec,
                     cod_historico,                 desc_historico,
                     data_movimento,                valor_movimento_deb,
                     valor_movimento_cre,           tabela_origem,
                     registro_livro_razao,          nome_programa,
                     codigo_contabil)
                  values
                    (p_cod_empresa,                 p_exercicio,
                     'F',                           reg_cpag015.codigo_empresa,
                     reg_cpag015.nr_duplicata,      reg_cpag015.parcela,
                     reg_cpag015.cgc_9,             reg_cpag015.cgc_4,
                     reg_cpag015.cgc_2,             reg_cpag015.tipo_titulo,
                     reg_cpag015.sequencia_pagto,   reg_fornecedores.nome_fornecedor,
                     reg_cpag015.codigo_historico,  reg_cpag015.historico_contab,
                     reg_cpag015.data_pagamento,    v_valor_movimento_deb,
                     v_valor_movimento_cre,         'CPAG_015',
                     v_registro_livro_razao,        p_nome_programa,
                     reg_fornecedores.codigo_contabil);

               exception
                  when others then
                     p_des_erro := 'Erro na inclusao da tabela sped_ctb_i550 - Fornecedores (2) ' || Chr(10) || SQLERRM;
                     RAISE V_ERRO;
               end;
               commit;
            end if;
         end loop;

         ---------------------- FIM DA ROTINA DE CALCULO DOS PAGAMENTOS DOS TITULOS (COLUNAS DEBITO E CREDITO) ----

         if p_nome_programa = 'cont_e455'
         then
            v_saldo_inicial_fornec_livro := 0.00;
         end if;

         for reg_saldos in saldos(p_cod_empresa, v_per_anterior, reg_fornecedores.fornecedor9,reg_fornecedores.fornecedor4,reg_fornecedores.fornecedor2)
         loop
            -- le a tabela de pagamentos para encontrar o saldos da duplicata
            begin
               select nvl(sum(decode(cont_010.sinal_titulo, 1, (cpag_015.valor_pago - cpag_015.valor_juros + cpag_015.valor_descontos - cpag_015.vlr_var_cambial),
                                                           (cpag_015.valor_pago - cpag_015.valor_juros + cpag_015.valor_descontos - cpag_015.vlr_var_cambial) * (-1.0))),0.00)
               into   v_total_pago
               from cpag_015, cont_010
               where cpag_015.dupl_for_nrduppag         = reg_saldos.nr_duplicata
               and   cpag_015.dupl_for_no_parc          = reg_saldos.parcela
               and   cpag_015.dupl_for_for_cli9         = reg_saldos.cgc_9
               and   cpag_015.dupl_for_for_cli4         = reg_saldos.cgc_4
               and   cpag_015.dupl_for_for_cli2         = reg_saldos.cgc_2
               and   cpag_015.dupl_for_tipo_tit         = reg_saldos.tipo_titulo
               and   cpag_015.data_pagamento            < p_per_inicial
               and   cont_010.codigo_historico          = cpag_015.codigo_historico
               and   cont_010.sinal_titulo             in (1,2);

            exception
               when no_data_found then
                  v_total_pago := 0.00;
            end;

            -- acumula valores do fornecedor
            v_saldo_inicial_fornec := v_saldo_inicial_fornec + (reg_saldos.saldo_parcela - v_total_pago);
            v_saldo_inicial_fornec_livro := v_saldo_inicial_fornec_livro + (reg_saldos.saldo_parcela - v_total_pago);

         end loop; -- fim saldos

         if p_nome_programa = 'cont_e455' and (encontrou_movimento or v_saldo_inicial_fornec_livro <> 0.00)
         then

            if v_saldo_inicial_fornec_livro > 0.00
            then
               v_valor_movimento_deb := 0.00;
               v_valor_movimento_cre := v_saldo_inicial_fornec_livro;
            else
               v_valor_movimento_deb := v_saldo_inicial_fornec_livro * (-1.0);
               v_valor_movimento_cre := 0.00;
            end if;

            -- grava os dados do saldo inicial do fornecedor
            begin
               insert into sped_ctb_i550
                 (cod_empresa,                   exercicio,
                  tipo_livro_aux,                codigo_empresa_titulo,
                  numero_titulo,                 parcela_titulo,
                  cgc_9_titulo,                  cgc_4_titulo,
                  cgc_2_titulo,                  tipo_titulo,
                  seq_pagamento,                 nome_cliente_fornec,
                  cod_historico,                 desc_historico,
                  data_movimento,                valor_movimento_deb,
                  valor_movimento_cre,           tabela_origem,
                  registro_livro_razao,          codigo_contabil,
                  nome_programa)
               values
                 (p_cod_empresa,                 p_exercicio,
                  'F',                           0,
                  0,                             ' ',
                  reg_fornecedores.fornecedor9,      reg_fornecedores.fornecedor4,
                  reg_fornecedores.fornecedor2,      0,
                  -1,                            reg_fornecedores.nome_fornecedor,
                  0,                             'SALDO INICIAL',
                  v_per_anterior,                 v_valor_movimento_deb,
                  v_valor_movimento_cre,         'SALDO',
                  v_registro_livro_razao,        reg_fornecedores.codigo_contabil,
                  p_nome_programa);

            exception
               when others then
                  p_des_erro := 'Erro na insercao do registro dos Saldos Iniciais do Razao de Fornecedores (1) ' || Chr(10) || SQLERRM;
                  RAISE V_ERRO;
            end;
            commit;
         end if;
      end loop; -- fim fornecedores

      -- grava os dados do ultimo fornecedor lido
      -- monta a linha que vai ser utilizada na exportacao dos dados
      begin

         if v_saldo_inicial_fornec > 0.00
         then
            v_valor_movimento_deb := 0.00;
            v_valor_movimento_cre := v_saldo_inicial_fornec;
         else
            v_valor_movimento_deb := v_saldo_inicial_fornec * (-1.0);
            v_valor_movimento_cre := 0.00;
         end if;

         v_registro_livro_razao := ' ' ;

         if p_nome_programa = 'cont_f001'
         then
            v_registro_livro_razao := 'SALDO INICIAL:'                         || '|' ||  -- CNPJ
                                      ' '                                      || '|' ||  -- NOME DO FORNECEDOR
                                      to_char(v_per_anterior, 'DD/MM/YY')      || '|' ||  -- DATA DO MOVIMENTO
                                      ' '                                      || '|' ||  -- HISTORICO
                                      ' '                                      || '|' ||  -- NUMERO_DOCUMENTO
                                      ltrim(replace(replace(replace(to_char(v_valor_movimento_deb, '999999990.00'), '.', ';'), ',', '.'), ';', ',')) || '|' ||
                                      ltrim(replace(replace(replace(to_char(v_valor_movimento_cre, '999999990.00'), '.', ';'), ',', '.'), ';', ','));
         end if;
      exception
         when others then
            p_des_erro := 'Erro na montagem do registro de saldos inciciais de Fornecedores (2) ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;

      -- grava os dados do saldo inicial do fornecedor
      begin
         insert into sped_ctb_i550
           (cod_empresa,                   exercicio,
            tipo_livro_aux,                codigo_empresa_titulo,
            numero_titulo,                 parcela_titulo,
            cgc_9_titulo,                  cgc_4_titulo,
            cgc_2_titulo,                  tipo_titulo,
            seq_pagamento,                 nome_cliente_fornec,
            cod_historico,                 desc_historico,
            data_movimento,                valor_movimento_deb,
            valor_movimento_cre,           tabela_origem,
            registro_livro_razao,          nome_programa)
         values
           (p_cod_empresa,                 p_exercicio,
            'F',                           0,
            0,                             ' ',
            0,                             0,
            0,                             0,
            0,                             'SALDO INICIAL',
            0,                             'SALDO INICIAL',
            v_per_anterior,                 v_valor_movimento_deb,
            v_valor_movimento_cre,         'SALDO',
            v_registro_livro_razao,        p_nome_programa);

      exception
         when others then
            p_des_erro := 'Erro na insercao do registro dos Saldos Iniciais do Razao de Fornecedores (2) ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;
      commit;


      ---------------------- FIM DA ROTINA DE CALCULO DOS OS SALDOS INICIAIS ---------------------

      ---------------------- INICIO ROTINA DE CALCULO DAS ENTADAS DOS TITULOS (COLUNA CREDITO) ---

   end if;

   commit;

exception
   when V_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_for ' || Chr(10) || p_des_erro;

   when others then
      p_des_erro := 'Outros erros na procedure p_gera_sped_for ' || Chr(10) || SQLERRM;

end inter_pr_gera_ctb_sped_for;
 

/

exec inter_pr_recompile;

