
  CREATE OR REPLACE FUNCTION "INTER_FN_FIOS_REJEICAO" 
        (fniv_rejeita in efic_100.prod_rej_nivel99%type,
         fgru_rejeita in efic_100.prod_rej_grupo%type,
         fsub_rejeita in efic_100.prod_rej_subgrupo%type,
         fite_rejeita in efic_100.prod_rej_item%type,
         fdat_rejeita in efic_100.data_rejeicao%type)
RETURN number
IS
   ftot_qtde number(15,4);
begin
   select nvl(sum(efic_100.quantidade),0) into ftot_qtde from efic_100
   where efic_100.prod_rej_nivel99  = fniv_rejeita
     and efic_100.prod_rej_grupo    = fgru_rejeita
     and efic_100.prod_rej_subgrupo = fsub_rejeita
     and efic_100.prod_rej_item     = fite_rejeita
     and efic_100.data_rejeicao     = fdat_rejeita;

   return(ftot_qtde);
end inter_fn_fios_rejeicao;



 

/

exec inter_pr_recompile;

