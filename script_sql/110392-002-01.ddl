create table cont_k100 (
	COD_CONTROLADORA    NUMBER(9) default 0,
	EXERCICIO           NUMBER(9) DEFAULT 0,
	EMP_COD             NUMBER(9) DEFAULT 0,
	COD_PAIS            NUMBER(9) DEFAULT 0,
	CNPJ_9              NUMBER(9) DEFAULT 0,
	CNPJ_4              NUMBER(4) DEFAULT 0,
	CNPJ_2              NUMBER(2) DEFAULT 0,
	NOME                VARCHAR2(200) DEFAULT ' ',
	PER_PART            NUMBER(8,4) DEFAULT 0.0,
	EVENTO              VARCHAR2(1) CHECK(EVENTO IN ('S','N')) NOT NULL,
	PER_CONS            NUMBER(8,4) DEFAULT 0.0,
	DATA_INI_EMP        DATE NOT NULL,
	DATA_FIN_EMP        DATE NOT NULL
);

alter table cont_k100
  add constraint PK_cont_k100 primary key (COD_CONTROLADORA,EXERCICIO,EMP_COD);
  
comment on table cont_k100 						is 'RELAÇÃO DAS EMPRESAS CONSOLIDADAS';
comment on column cont_k100.COD_CONTROLADORA 	is 'CÓDIGO DA EMPRESA CONTROLADORA';
comment on column cont_k100.EXERCICIO           is 'Exercicio a que se refere as informações da empresa no período';
comment on column cont_k100.EMP_COD             is 'Código do país da empresa, conforme tabela do Banco Central do Brasil.';
comment on column cont_k100.COD_PAIS            is 'Código de identificação da empresa participante.';
comment on column cont_k100.CNPJ_9              is 'CNPJ (somente os 8 primeiros dígitos).';
comment on column cont_k100.NOME                is 'Nome empresarial.';
comment on column cont_k100.PER_PART            is 'Percentual de participação total do conglomerado na empresa no final do período consolidado: Informar a participação acionária.';
comment on column cont_k100.EVENTO              is 'Evento societário ocorrido no período: S - Sim N – Não';
comment on column cont_k100.PER_CONS            is 'Percentual de consolidação da empresa no final do período consolidado: Informar o percentual do resultado da empresa que foi para a consolidação.';
comment on column cont_k100.DATA_INI_EMP        is 'Data inicial do período da escrituração contábil da empresa que foi consolidada.';
comment on column cont_k100.DATA_FIN_EMP        is 'Data final do período da escrituração contábil da empresa que foi consolidada';
