create or replace procedure wms_pr_insere_pcs_rfid (p_cod_rfid         in  varchar2,
                                                    p_periodo_tag      in  number,
                                                    p_ordem_prod_tag   in  number,
                                                    p_ordem_conf_tag   in  number,
                                                    p_sequencia_tag    in  number,
                                                    nivel_sku          in  varchar2,
                                                    grupo_sku          in  varchar2,
                                                    subgrupo_sku       in  varchar2,
                                                    item_sku           in  varchar2,
                                                    cod_deposito       in  number,
                                                    p_quantidade       in  number) is
  v_resultado_int     number;
  v_codigo_unico_tag  inte_wms_rfid_tags.codigo_unico_tag%type;
  v_codigo_unico_sku  inte_wms_rfid.codigo_unico_sku%type;
begin

   if p_periodo_tag <> 0 and p_ordem_prod_tag <> 0 and p_ordem_conf_tag <> 0 and p_sequencia_tag <> 0 and 
      p_quantidade   > 1
   then
      --N�o pode informar Tag e Quantidade maior que 1 (tem que ser Zero ou Um)
      raise_application_error(-20000, 'ATEN��O! OPERA��O INV�LIDA! N�o � poss�vel informar uma TAG espec�fica a ser inserida na Caixa RFID com o Par�metro Quantidade maior que 1. J� que a Tag � unit�ria, o par�metro Quantidade deve ser Zerado ou 1 quando se informar uma Tag.');
   end if;
   
   --Verificar se � para inserir uma Tag ou 1 Pe�a (EAN)
   if p_periodo_tag <> 0 and p_ordem_prod_tag <> 0 and p_ordem_conf_tag <> 0 and p_sequencia_tag <> 0
   then
      --monta o codigo_unico_tag
      select to_char(p_periodo_tag,'0000') || 
             to_char(p_ordem_prod_tag,'000000000') || 
             to_char(p_ordem_conf_tag,'00000') || 
             to_char(p_sequencia_tag,'0000') 
      into v_codigo_unico_tag
      from dual ;
     
      --Inserir uma Tag
         
      --Inserir a Tag na Caixa (inte_wms_rfid_tags)
      begin
         insert into inte_wms_rfid_tags
            (rfid_caixa,
             periodo_tag,
             ordem_prod_tag,
             ordem_conf_tag,
             sequencia_tag,
             liberar_tag_st,
             codigo_unico_tag)
         values
            (p_cod_rfid,
             p_periodo_tag,
             p_ordem_prod_tag,
             p_ordem_conf_tag,
             p_sequencia_tag,
             0,
             v_codigo_unico_tag);
      exception
         when others then
            raise_application_error(-20000, 'N�o inseriu na tabela INTE_WMS_RFID_TAGS.' || Chr(10) || SQLERRM);
      end;

      --Verificar se a caixa existe para atualizar ou inserir 
      begin
         select 1
         into   v_resultado_int
         from inte_wms_rfid
         where inte_wms_rfid.rfid_caixa = p_cod_rfid
           and rownum                  <= 1;
      exception
         when no_data_found then
            v_resultado_int := 0;
      end;
      
      if v_resultado_int = 1
      then
         
         --A caixa existe. Atualizar
         begin
            update inte_wms_rfid
            set    inte_wms_rfid.qtd_pecas   = inte_wms_rfid.qtd_pecas + 1
            where  inte_wms_rfid.rfid_caixa  = p_cod_rfid;
         exception
            when others then
               raise_application_error(-20000, 'N�o atualizou tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);
         end;     
         
      else
         -- monta codigo_unico_sku
         select nivel_sku || '.' || 
                grupo_sku || '.' || 
                subgrupo_sku || '.' || 
                item_sku 
         into v_codigo_unico_sku
         from dual;                
         
         --A caixa n�o existe. Inserir.
         begin
            insert into inte_wms_rfid
               (rfid_caixa,
                nivel_sku,
                grupo_sku,
                subgrupo_sku,
                item_sku,
                cod_deposito,
                qtd_pecas,
                flag,
                timestamp_liberacao,
                timestamp_armazenagem,
                mensagem,
                codigo_unico_sku)
            values
               (p_cod_rfid,
                nivel_sku,
                grupo_sku,
                subgrupo_sku,
                item_sku,
                cod_deposito,
                1,
                0,
                null,
                null,
                ' ',
                v_codigo_unico_sku);
         exception
            when others then
               raise_application_error(-20000, 'N�o inseriu na tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);
         end;   
      
      end if;

   else
         
      --Inserir uma Pe�a (EAN)
      --Verificar se a caixa existe para atualizar ou inserir 
      begin
         select 1
         into   v_resultado_int
         from inte_wms_rfid
         where inte_wms_rfid.rfid_caixa = p_cod_rfid
           and rownum                  <= 1;
      exception
         when no_data_found then
            v_resultado_int := 0;
      end;
      
      if v_resultado_int = 1
      then
         
         --A caixa existe. Atualizar
         begin
            update inte_wms_rfid
            set    inte_wms_rfid.qtd_pecas   = inte_wms_rfid.qtd_pecas + p_quantidade
            where  inte_wms_rfid.rfid_caixa  = p_cod_rfid;
         exception
            when others then
               raise_application_error(-20000, 'N�o atualizou tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);
         end;     
         
      else
         -- monta codigo_unico_sku
         select nivel_sku || '.' || 
           grupo_sku || '.' || 
           subgrupo_sku || '.' || 
           item_sku 
    into v_codigo_unico_sku
         from dual;
         
         --A caixa n�o existe. Inserir.
         begin
            insert into inte_wms_rfid
               (rfid_caixa,
                nivel_sku,
                grupo_sku,
                subgrupo_sku,
                item_sku,
                cod_deposito,
                qtd_pecas,
                flag,
                timestamp_liberacao,
                timestamp_armazenagem,
                mensagem,
                codigo_unico_sku)
            values
               (p_cod_rfid,
                nivel_sku,
                grupo_sku,
                subgrupo_sku,
                item_sku,
                cod_deposito,
                p_quantidade,
                0,
                null,
                null,
                ' ',
                v_codigo_unico_sku);
         exception
            when others then
               raise_application_error(-20000, 'N�o inseriu na tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);
         end;   
      
      end if;
         
   end if;
      
   commit;
  
end wms_pr_insere_pcs_rfid;

/

exec inter_pr_recompile;

/* versao: 2 */


 exit;


 exit;
