
  CREATE OR REPLACE PROCEDURE "INTER_PR_EXPL_ESTR_TMRP_710" 
    -- Recebe parametros para explosao da estrutura.
   (p_nivel_explode       in varchar2,
    p_grupo_explode       in varchar2,
    p_subgrupo_explode    in varchar2,
    p_item_explode        in varchar2,
    p_alternativa_explode in number,
    p_periodo_producao    in number,
    p_qtde_quilos_prog    in number,
    p_pedido_ordem        in number,
    p_seq_ordem           in number,
    p_area_producao       in number,
    p_sequencia_item      in number,
    p_estagio             in number,
    p_relacao_banho_aux   in number,
    p_codigo_embalagem    in number,
    p_origem_chamada      in varchar2)
is
   -- Cria cursor para executar loop na basi_050 buscando todos
   -- os produtos da estrutura.
   cursor basi050 is
   select nivel_comp,       grupo_comp,
          sub_comp,         item_comp,
          sequencia,        consumo,
          sub_item,         item_item,
          estagio,          tipo_calculo,
          alternativa_comp, relacao_banho
   from basi_050
   where  basi_050.nivel_item       = p_nivel_explode
     and  basi_050.grupo_item       = p_grupo_explode
     and (basi_050.sub_item         = p_subgrupo_explode or basi_050.sub_item  = '000')
     and (basi_050.item_item        = p_item_explode     or basi_050.item_item = '000000')
     and  basi_050.alternativa_item = p_alternativa_explode
     and (basi_050.estagio          = p_estagio or p_estagio = 0 or basi_050.estagio = 0)
   order by basi_050.sequencia;

   -- Declara as variaveis que serao utilizadas.
   p_nivel_comp050       basi_050.nivel_comp%type := '#';
   p_grupo_comp050       basi_050.grupo_comp%type := '#####';
   p_subgru_comp050      basi_050.sub_comp%type   := '###';
   p_item_comp050        basi_050.item_comp%type  := '######';
   p_sequencia050        basi_050.sequencia%type;
   p_consumo050          basi_050.consumo%type;
   p_sub_item050         basi_050.sub_item%type   := '###';
   p_item_item050        basi_050.item_item%type  := '######';
   p_estagio050          basi_050.estagio%type;
   p_tpcalculo050        basi_050.tipo_calculo%type;
   p_alternativa_comp    basi_050.alternativa_comp%type;
   p_relacao_banho       basi_050.relacao_banho%type;

   p_fio_niv             basi_050.nivel_comp%type := '#';
   p_fio_gru             basi_050.grupo_comp%type := '#####';
   p_fio_sub             basi_050.sub_comp%type   := '###';
   p_fio_ite             basi_050.item_comp%type  := '######';

   p_percent_qtde_alter  basi_230.qtde_alternativa%type;

   p_qtde_reservada      tmrp_710.qtde_reservada%type;
   p_consumo_final       basi_050.consumo%type;

   p_reserva_planej_mat_prima4 empr_002.reserva_planej_mat_prima4%type;
   p_relacao_banho_emp         empr_001.relacao_banho%type;

   v_existe_710           number(9);
   v_existe_040           number;
   v_codigo_risco         pcpc_200.codigo_risco%type;
   v_consumo_liq          number(13,7);
   v_tipo_ordem           number(1);

   p_data_corrente        date;
   p_alternativa_comp_040 number(2);
   p_alt_estrut_planej    number(2);

begin
   -- Busca parametro de empresa para calculo das quantidades que
   -- forem atraves do volume de banho.
   select empr_001.relacao_banho
   into p_relacao_banho_emp
   from empr_001;

   -- Inicio do loop da explosao da estrutura.
   for reg_basi050 in basi050
   loop

      -- Carrega as variaveis com os valores do cursor.
      p_nivel_comp050    := reg_basi050.nivel_comp;
      p_grupo_comp050    := reg_basi050.grupo_comp;
      p_subgru_comp050   := reg_basi050.sub_comp;
      p_item_comp050     := reg_basi050.item_comp;
      p_sequencia050     := reg_basi050.sequencia;
      p_consumo050       := reg_basi050.consumo;
      p_sub_item050      := reg_basi050.sub_item;
      p_item_item050     := reg_basi050.item_item;
      p_estagio050       := reg_basi050.estagio;
      p_tpcalculo050     := reg_basi050.tipo_calculo;
      p_alternativa_comp := reg_basi050.alternativa_comp;

      -- Pega alternativa da embalagem setada no cadastro de embalagem
      -- quando o codigo_embalagem for maior que zero e nivel comp
      -- for 3.
      if p_nivel_comp050 = '3' and p_codigo_embalagem > 0 and p_origem_chamada <> 'pcpc'
      then
         begin
            select fatu_100.alternativa
            into p_alternativa_comp
            from fatu_100
            where fatu_100.codigo_embalagem = p_codigo_embalagem
              and fatu_100.alternativa      > 0
              and exists (select * from basi_030
                          where basi_030.nivel_estrutura  = '3'
                            and basi_030.referencia       = p_grupo_comp050
                            and basi_030.tipo_produto     = 6);
            exception
            when others then
               p_alternativa_comp := reg_basi050.alternativa_comp;
         end;
      end if;

      if p_nivel_explode = '5' and p_relacao_banho_aux = 0.00
      then
         if reg_basi050.relacao_banho = 0.00
         then
            p_relacao_banho := p_relacao_banho_emp;
         else
            p_relacao_banho := reg_basi050.relacao_banho;
         end if;
      else p_relacao_banho := p_relacao_banho_aux;
      end if;

      if p_nivel_explode = '2'
      then
         p_relacao_banho := 0.00;
      end if;

      -- Carrega a data corrente para filtro.
      p_data_corrente    := sysdate;

      if p_subgru_comp050 = '000' or p_consumo050 = 0.0000000
      then
         begin
            select basi_040.sub_comp, basi_040.consumo
            into   p_subgru_comp050,  p_consumo050
            from basi_040
            where basi_040.nivel_item       = p_nivel_explode
              and basi_040.grupo_item       = p_grupo_explode
              and basi_040.sub_item         = p_subgrupo_explode
              and basi_040.item_item        = p_item_item050
              and basi_040.alternativa_item = p_alternativa_explode
              and basi_040.sequencia        = p_sequencia050;
            exception
            when others then
               p_subgru_comp050 := '000';
               p_consumo050     := 0.0000000;
         end;
      end if;

      p_alternativa_comp_040 := 0;

      if p_item_comp050 = '000000'
      then
         begin
            select basi_040.item_comp, basi_040.alternativa_comp
            into   p_item_comp050,     p_alternativa_comp_040
            from basi_040
            where basi_040.nivel_item       = p_nivel_explode
              and basi_040.grupo_item       = p_grupo_explode
              and basi_040.sub_item         = p_sub_item050
              and basi_040.item_item        = p_item_explode
              and basi_040.alternativa_item = p_alternativa_explode
              and basi_040.sequencia        = p_sequencia050;
            exception
            when others then
               p_item_comp050 := '000000';
               p_alternativa_comp_040 := 0;
         end;
      end if;

      -- Busca parametro de empresa executando a seguinte consistencia:
      -- 0 - Reserva Produto Principal ou 1 - Reserva Produto Alternativo
      select empr_002.reserva_planej_mat_prima4, empr_002.alternativa_estrutura_plane
      into   p_reserva_planej_mat_prima4,        p_alt_estrut_planej
      from empr_002;

      if p_alt_estrut_planej <> 0
      then
         if p_alternativa_comp_040 <> 0
         then
            p_alternativa_comp := p_alternativa_comp_040;
         end if;
      end if;

      if p_nivel_explode = '4'
      then
         if p_reserva_planej_mat_prima4 = 0
         then
            p_fio_niv := p_nivel_comp050;
            p_fio_gru := p_grupo_comp050;
            p_fio_sub := p_subgru_comp050;
            p_fio_ite := p_item_comp050;
         else
            begin
               select basi_230.item_alt_nivel99,   basi_230.item_alt_grupo,
                      basi_230.item_alt_subgrupo,  basi_230.item_alt_item,
                      basi_230.qtde_alternativa
               into   p_fio_niv,                   p_fio_gru,
                      p_fio_sub,                   p_fio_ite,
                      p_percent_qtde_alter
               from basi_230
               where basi_230.item_pri_nivel99  = p_nivel_comp050
                 and basi_230.item_pri_grupo    = p_grupo_comp050
                 and basi_230.item_pri_subgrupo = p_subgru_comp050
                 and basi_230.item_pri_item     = p_item_comp050
                 and basi_230.data_inicial     <= p_data_corrente
                 and basi_230.data_final       >= p_data_corrente;

               p_consumo050 := p_consumo050 * p_percent_qtde_alter;

               exception
               when others then
                  p_fio_niv    := p_nivel_comp050;
                  p_fio_gru    := p_grupo_comp050;
                  p_fio_sub    := p_subgru_comp050;
                  p_fio_ite    := p_item_comp050;
                  p_consumo050 := p_consumo050;
            end;
         end if;
      else
         p_fio_niv := p_nivel_comp050;
         p_fio_gru := p_grupo_comp050;
         p_fio_sub := p_subgru_comp050;
         p_fio_ite := p_item_comp050;
      end if;

      v_codigo_risco := 0;
      v_consumo_liq := 0.00;

      -- SS 39468/018.
      -- Quando houver risco informado para a ordem de corte
      -- O parametro p_seq_ordem, na confeccao, representa o codigo de risco

      -- (DIGITACAO) - Se a origem da chamada for a confeccao, e for um tecido -> busca o consumo do risco
      -- (DIGITACAO) - Se a origem da chamada for a confeccao, atualiza o planejamento da producao.

      v_tipo_ordem := 0;
      -- Se a origem da chamada for o beneficiamento, atualiza o planejamento da producao.
      if p_origem_chamada = 'pcpb' and p_nivel_explode = '2' or p_nivel_explode = '4'
      then

         -- Quando o calculo for pelo volume de banho.
         if p_tpcalculo050 = 2
         then
            p_qtde_reservada := p_consumo050 * (p_qtde_quilos_prog * p_relacao_banho);
         else
            p_qtde_reservada := p_consumo050 *  p_qtde_quilos_prog;
         end if;

         if p_estagio050 = 0
         then
            p_estagio050 := p_estagio;
         end if;


         -- Verifica se o registro ja existe, se nao existir insere novo registro
         -- senao atualiza a quantidade a receber do produto.


         select count(*)
         into v_existe_710
         from tmrp_710
         where tmrp_710.periodo_producao    = p_periodo_producao
           and tmrp_710.area_producao       = p_area_producao
           and tmrp_710.nivel_estrutura     = p_fio_niv
           and tmrp_710.grupo_estrutura     = p_fio_gru
           and tmrp_710.subgru_estrutura    = p_fio_sub
           and tmrp_710.item_estrutura      = p_fio_ite
           and tmrp_710.sequencia_estrutura = p_sequencia050;
         if v_existe_710 = 0
         then
            -- Atualiza tabela de planejamento.
            insert into tmrp_710
                   (periodo_producao,    area_producao,
                    nivel_estrutura,     grupo_estrutura,
                    subgru_estrutura,    item_estrutura,
                    qtde_reservada,      consumo,
                    sequencia_estrutura, tipo_calculo)
            values (p_periodo_producao, p_area_producao,
                    p_fio_niv,          p_fio_gru,
                    p_fio_sub,          p_fio_ite,
                    p_qtde_reservada,   p_consumo050,
                    p_sequencia050,     p_tpcalculo050);
         else
            update tmrp_710
            set tmrp_710.qtde_reservada = tmrp_710.qtde_reservada  + p_qtde_reservada
            where tmrp_710.periodo_producao    = p_periodo_producao
              and tmrp_710.area_producao       = p_area_producao
              and tmrp_710.nivel_estrutura     = p_fio_niv
              and tmrp_710.grupo_estrutura     = p_fio_gru
              and tmrp_710.subgru_estrutura    = p_fio_sub
              and tmrp_710.item_estrutura      = p_fio_ite
              and tmrp_710.sequencia_estrutura = p_sequencia050;
         end if;
      end if;

      -- Explode a estrutura de estampas(3) e receitas(5) de tingimento
      if  (p_fio_niv = '4' and p_nivel_explode = '2')
      then
         inter_pr_expl_estr_tmrp_710(p_fio_niv,
                                     p_fio_gru,
                                     p_fio_sub,
                                     p_fio_ite,
                                     p_alternativa_comp,
                                     p_periodo_producao,
                                     p_qtde_reservada,
                                     0,
                                     0,
                                     2,
                                     0,
                                     0,
                                     0.00,
                                     0,
                                    'pcpb');
      end if;
   end loop;
end inter_pr_expl_estr_tmrp_710;

 

/

exec inter_pr_recompile;

