alter table fatu_050 add (data_vencto_gnre date);
alter table fatu_050 add (cod_receita_fcp varchar2(20));
alter table fatu_050 add (cod_receita_dest varchar2(20));
alter table fatu_050 add (ind_gerou_gnre number(1));

comment on COLUMN fatu_050.data_vencto_gnre is 'Data de vencimento da gnre';
comment on COLUMN fatu_050.cod_receita_fcp is 'codigo de receita do valor de icms fcp';
comment on COLUMN fatu_050.cod_receita_dest is 'codigo de receita do valor de icms destinatario';

alter table fatu_050 modify (cod_receita_fcp default ' ', cod_receita_dest default ' ', ind_gerou_gnre default 0);

exec inter_pr_recompile;
