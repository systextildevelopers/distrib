
CREATE OR REPLACE PROCEDURE "P_GERA_CCE" (p_cod_empresa    in  number,
                                       p_cod_id         in  number,
                                       p_cod_cce        in  number,
                                       p_str_usuario    in  varchar2,
                                       p_msg_erro       out varchar2
                                       ) is

 v_erro             EXCEPTION;
 v_ambiente         obrf_158.nfe_ambiente%type;
 v_emit_cnpj9       fatu_500.cgc_9%type;
 v_emit_cnpj4       fatu_500.cgc_4%type;
 v_emit_cnpj2       fatu_500.cgc_2%type;
 v_emit_codigo_uf   basi_167.codigo_fiscal_uf%type;
 v_nfe_e_mail       supr_010.nfe_e_mail%type;
 v_rep_e_mail           pedi_020.e_mail%type;

begin

  /*Alimenta tipo ambiente*/
  select obrf_158.nfe_ambiente
  into   v_ambiente
  from obrf_158
  where obrf_158.cod_empresa = p_cod_empresa;
  /*Alimenta emit_cnpj e emit_codigo_uf*/
  select fatu_500.cgc_9,     fatu_500.cgc_4,
         fatu_500.cgc_2,     trim(to_char(basi_167.codigo_fiscal_uf,'00')) as emit_codigo_uf
  into   v_emit_cnpj9,       v_emit_cnpj4,
         v_emit_cnpj2,       v_emit_codigo_uf
  from   fatu_500            ,basi_160 ,
         basi_167
  where  fatu_500.codigo_empresa   = p_cod_empresa
    and  basi_160.cod_cidade       = fatu_500.codigo_cidade
    and  basi_160.estado           = basi_167.estado;


/*Select de notas de entrada e saida*/


  for f_CCe in
  (select numero_danf_nfe
         ,numero
         ,serie
         ,cd_cce
         ,ds_cce
         ,cd_tp_evento
         ,cd_seq_cce
         ,data_emissao
         ,dest_nfe_email
         ,dest_nome
         ,cd_transp_cnpj_9
         ,cd_transp_cnpj_4
         ,cd_transp_cnpj_2
         ,cod_rep_cliente
         ,user_id_e_mail
         ,f_user_e_mail
         ,senha_e_mail
         ,entrada_saida
    from (select obr.numero_danf_nfe
                ,obr.documento        as numero
                ,obr.serie            as serie
                ,cce.cd_cce
                ,replace(cce.ds_cce,chr(10),' ') as ds_cce
                ,cce.cd_tp_evento
                ,cce.cd_seq_cce
                ,obr.data_emissao
                ,s010.nfe_e_mail      as dest_nfe_email
                ,s010.nome_fornecedor as dest_nome
                ,cce.cd_transp_cnpj_9
                ,cce.cd_transp_cnpj_4
                ,cce.cd_transp_cnpj_2
                ,0 as cod_rep_cliente
                ,h_030.user_id_e_mail
                ,h_030.e_mail as f_user_e_mail
                ,h_030.senha_e_mail
                ,'E' as entrada_saida
          from obrf_010 obr
              ,obrf_122 cce
              ,supr_010 s010
              ,hdoc_030 h_030
              ,fatu_500 f500
          where cce.cd_cce                = p_cod_cce
            and cce.st_enviado_s_n        = 'N'
            and obr.cgc_cli_for_9         = cce.cd_forn_9
            and obr.cgc_cli_for_4         = cce.cd_forn_4
            and obr.cgc_cli_for_2         = cce.cd_forn_2
            and obr.documento             = cce.cd_numero_nota
            and obr.serie                 = cce.cd_serie
            and s010.fornecedor9          = obr.cgc_cli_for_9
            and s010.fornecedor4          = obr.cgc_cli_for_4
            and s010.fornecedor2          = obr.cgc_cli_for_2
            and h_030.empresa             = p_cod_empresa
            and h_030.usuario             = p_str_usuario
            and f500.codigo_empresa       = h_030.empresa
            )

    union all
    (select fatu.numero_danf_nfe
           ,fatu.num_nota_fiscal as numero
           ,fatu.serie_nota_fisc as serie
           ,cce2.cd_cce
           ,replace(cce2.ds_cce,chr(10),' ') as ds_cce
           ,cce2.cd_tp_evento
           ,cce2.cd_seq_cce
           ,fatu.data_emissao
           ,p010.nfe_e_mail      as dest_nfe_email
           ,p010.nome_cliente    as dest_nome
           ,cce2.cd_transp_cnpj_9
           ,cce2.cd_transp_cnpj_4
           ,cce2.cd_transp_cnpj_2
           ,fatu.cod_rep_cliente as cod_rep_cliente
           ,h_030.user_id_e_mail
           ,h_030.e_mail as f_user_e_mail
           ,h_030.senha_e_mail
           ,'S' as entrada_saida
    from fatu_050 fatu
        ,obrf_122 cce2
        ,pedi_010 p010
        ,hdoc_030 h_030
        ,fatu_500 f500
    where cce2.cd_cce               = p_cod_cce
      and cce2.st_enviado_s_n       = 'N'
      and fatu.num_nota_fiscal      = cce2.cd_numero_nota
      and fatu.serie_nota_fisc      = cce2.cd_serie
      and fatu.codigo_empresa       = p_cod_empresa
      and p010.cgc_9                = fatu.cgc_9
      and p010.cgc_4                = fatu.cgc_4
      and p010.cgc_2                = fatu.cgc_2
      and h_030.empresa             = p_cod_empresa
      and h_030.usuario             = p_str_usuario
      and f500.codigo_empresa       = h_030.empresa
))
  LOOP
    BEGIN
       
    v_nfe_e_mail := '';
    v_rep_e_mail := '';
    
    if f_CCe.entrada_saida = 'S'
    then
       begin
         select nfe_e_mail 
         into v_nfe_e_mail
         from supr_010
         where supr_010.FORNECEDOR9 = f_CCe.cd_transp_cnpj_9
         and   supr_010.FORNECEDOR4 = f_CCe.cd_transp_cnpj_4
         and   supr_010.FORNECEDOR2 = f_CCe.cd_transp_cnpj_2;
       exception when no_data_found then
            v_nfe_e_mail := '';
      end;
      
      begin
         select e_mail 
         into v_rep_e_mail
         from pedi_020
         where pedi_020.cod_rep_cliente = f_CCe.Cod_Rep_Cliente
         and   pedi_020.envia_email in (1,3);
      exception when no_data_found then
            v_rep_e_mail := '';
      end;
    end if; 
      
  insert into obrf_150
      (obrf_150.cod_empresa
      ,obrf_150.cod_usuario
      ,obrf_150.tipo_ambiente
      ,obrf_150.chave_acesso
      ,obrf_150.numero
      ,obrf_150.serie
      ,obrf_150.id_lote
      ,obrf_150.x_correcao
      ,obrf_150.tp_evento
      ,obrf_150.n_seq_evento
      ,obrf_150.id_cce
      ,obrf_150.tipo_documento
      ,obrf_150.emit_cnpj9
      ,obrf_150.emit_cnpj4
      ,obrf_150.emit_cnpj2
      ,obrf_150.emit_codigo_uf
      ,obrf_150.data_emissao
      ,obrf_150.dest_nfe_e_mail
      ,obrf_150.dest_nome
      ,obrf_150.transp_cnpj9
      ,obrf_150.transp_cnpj4
      ,obrf_150.transp_cnpj2
      ,obrf_150.user_id_e_mail
      ,obrf_150.user_e_mail
      ,obrf_150.user_senha_e_mail
      ,obrf_150.transp_email
      ,obrf_150.repre_email
     )
      values
      (p_cod_empresa
      ,p_cod_id
      ,v_ambiente
      ,f_CCe.numero_danf_nfe
      ,f_CCe.numero
      ,f_CCe.serie
      ,f_CCe.cd_cce
      ,f_CCe.ds_cce
      ,f_CCe.cd_tp_evento
      ,f_CCe.cd_seq_cce
      ,'ID' || to_char(f_CCe.cd_tp_evento) || f_CCe.numero_danf_nfe || to_char(f_CCe.cd_seq_cce) /*Monta a chave do id_cce*/
      ,6
      ,v_emit_cnpj9
      ,v_emit_cnpj4
      ,v_emit_cnpj2
      ,v_emit_codigo_uf
      ,f_CCe.data_emissao
      ,f_CCe.dest_nfe_email
      ,f_CCe.dest_nome
      ,f_CCe.cd_transp_cnpj_9
      ,f_CCe.cd_transp_cnpj_4
      ,f_CCe.cd_transp_cnpj_2
      ,f_CCe.user_id_e_mail
      ,f_CCe.f_user_e_mail
      ,f_CCe.senha_e_mail
      ,v_nfe_e_mail
      ,v_rep_e_mail);
    EXCEPTION
       WHEN OTHERS THEN

          p_msg_erro := 'Nao inseriu dados na tabela obrf_150' || Chr(10) || SQLERRM;
        RAISE v_ERRO;
    END;

  END LOOP;

end p_gera_CCe;

 

/

exec inter_pr_recompile;

