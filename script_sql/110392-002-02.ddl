create table cont_k110 (
	COD_CONTROLADORA    NUMBER(9) DEFAULT 0,
	EXERCICIO           NUMBER(9) DEFAULT 0,
	EMP_COD             NUMBER(9) DEFAULT 0,
	EVENTO 				NUMBER(1) CHECK(EVENTO IN (1,2,3,4,5,6,7,8)) NOT NULL,
	DATA_EVENTO 		DATE NOT NULL
);

alter table cont_k110
	ADD CONSTRAINT cont_k110_pk PRIMARY KEY (COD_CONTROLADORA,EXERCICIO,EMP_COD,EVENTO);

alter table cont_k110
	ADD CONSTRAINT cont_k110_fk FOREIGN KEY(COD_CONTROLADORA,EXERCICIO,EMP_COD) REFERENCES cont_k100 (COD_CONTROLADORA,EXERCICIO,EMP_COD);

comment on table cont_k110 						is 'Relação dos eventos societários';
comment on column cont_k110.COD_CONTROLADORA 	is 'CÓDIGO DA EMPRESA CONTROLADORA';
comment on column cont_k110.EXERCICIO           is 'Exercicio a que se refere as informações da empresa no período';
comment on column cont_k110.EMP_COD             is 'Código do país da empresa, conforme tabela do Banco Central do Brasil.';	
comment on column cont_k110.EVENTO 				is 'Evento societário ocorrido no período';
comment on column cont_k110.DATA_EVENTO 		is 'Data do evento societário';
