
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_030" 
after delete
on fatu_030
for each row
begin
   begin
      delete fatu_380
      where fatu_380.nr_solicitacao = :old.nr_solicitacao
        and fatu_380.pedido_venda   = :old.pedido_venda;
   exception when others
      then null;
   end;

   begin
      update fatu_050
         set fatu_050.cod_solicitacao_nfe = 0
      where fatu_050.pedido_venda   = :old.pedido_venda
        and fatu_050.nr_solicitacao = :old.nr_solicitacao;
   exception when others
      then null;
   end;

   begin
      update pcpc_320
      set pcpc_320.nr_solicitacao  = 0
      where pcpc_320.nr_solicitacao = :old.nr_solicitacao
        and pcpc_320.pedido_venda   = :old.pedido_venda
        and pcpc_320.nota_fiscal    = 0;
   exception when others
      then null;
   end;

end inter_tr_fatu_030;

-- ALTER TRIGGER "INTER_TR_FATU_030" ENABLE
 

/

exec inter_pr_recompile;

