CREATE OR REPLACE PROCEDURE "INTER_PR_CALC_CARDEX_CONSIG"( p_inc_exc           in number,
                                                           p_empresa1          in number,
                                                           p_empresa2          in number,
                                                           p_empresa3          in number,
                                                           p_empresa4          in number,
                                                           p_empresa5          in number,
                                                           p_nivel_prod        in varchar2,
                                                           p_grupo_prod        in varchar2,
                                                           p_nivel_prod1       in varchar2,
                                                           p_grupo_prod1       in varchar2,
                                                           p_nivel_prod2       in varchar2,
                                                           p_grupo_prod2       in varchar2,
                                                           p_nivel_prod3       in varchar2,
                                                           p_grupo_prod3       in varchar2,
                                                           p_dt_ini_atualizacao in date,
                                                           p_dt_fim_atualizacao  in date,
                                                           p_estq_300_estq_310 in varchar2)
is
  cursor registros is
  select
    a.codigo_deposito as dep, a.nivel_estrutura as nivel, a.grupo_estrutura as grupo, a.subgrupo_estrutura as sub,
    a.item_estrutura as item, count(1) as qtde_reg, b.local_deposito as empr_dep
    from estq_300 a, basi_205 b, cons_004 c
    where a.codigo_deposito = b.codigo_deposito
      and a.data_movimento between p_dt_ini_atualizacao and p_dt_fim_atualizacao
      and a.nivel_estrutura  = '1'
      and ((a.nivel_estrutura    = p_nivel_prod  and (a.grupo_estrutura = p_grupo_prod  or p_grupo_prod  = 'XXXXXX'))
      or   (a.nivel_estrutura    = p_nivel_prod1 and (a.grupo_estrutura = p_grupo_prod1 or p_grupo_prod1 = 'XXXXXX'))
      or   (a.nivel_estrutura    = p_nivel_prod2 and (a.grupo_estrutura = p_grupo_prod2 or p_grupo_prod2 = 'XXXXXX'))
      or   (a.nivel_estrutura    = p_nivel_prod3 and (a.grupo_estrutura = p_grupo_prod3 or p_grupo_prod3 = 'XXXXXX')))
      and b.controla_ficha_cardex = 1
    and b.local_deposito   in (p_empresa1,p_empresa2,p_empresa3,p_empresa4,p_empresa5)
      and c.empresa = b.local_deposito
      and c.deposito = b.codigo_deposito
    and    lower(p_estq_300_estq_310)  = 'estq_300'
    group by a.codigo_deposito, a.nivel_estrutura, a.grupo_estrutura, a.subgrupo_estrutura, a.item_estrutura, b.local_deposito
    order by a.codigo_deposito, a.nivel_estrutura, a.grupo_estrutura, a.subgrupo_estrutura, a.item_estrutura;


    valor_movimento_unitario       estq_300.valor_movimento_unitario%type;
    valor_contabil_unitario        estq_300.valor_contabil_unitario%type;
    preco_medio_unitario           estq_300.preco_medio_unitario%type;
    saldo_financeiro               estq_300.saldo_financeiro%type;
    valor_movimento_unitario_proj  estq_300.valor_movimento_unitario_proj%type;
    valor_contabil_unitario_proj   estq_300.valor_contabil_unitario_proj%type;
    preco_medio_unitario_proj      estq_300.preco_medio_unitario_proj%type;
    saldo_financeiro_proj          estq_300.saldo_financeiro_proj%type;

    valor_movto_unit_estimado      estq_300.valor_movto_unit_estimado%type;
    preco_medio_unit_estimado      estq_300.preco_medio_unit_estimado%type;
    saldo_financeiro_estimado      estq_300.saldo_financeiro_estimado%type;
    valor_total                    estq_300.valor_total%type;

    ini_saldo_financeiro           estq_301.saldo_financeiro%type;
    ini_preco_medio_unitario       estq_301.preco_medio_unitario%type;
    ini_preco_custo_unitario       estq_301.preco_custo_unitario%type;
    ini_saldo_financeiro_estimado   estq_301.saldo_financeiro_estimado%type;
    ini_preco_medio_unit_estimado   estq_301.preco_medio_unit_estimado%type;
    ini_preco_custo_unit_estimado   estq_301.preco_custo_unit_estimado%type;
    ini_saldo_financeiro_proj       estq_301.saldo_financeiro_proj%type;
    ini_preco_medio_unit_proj       estq_301.preco_medio_unit_proj%type;
    ini_preco_custo_unit_proj       estq_301.preco_custo_unit_proj%type;

    ant_nivel                      estq_300.nivel_estrutura%type;
    ant_grupo                      estq_300.grupo_estrutura%type;
    ant_subgrupo                   estq_300.subgrupo_estrutura%type;
    ant_item                       estq_300.item_estrutura%type;

    ant_saldo_financeiro           estq_300.saldo_financeiro%type;
    ant_preco_medio_unitario       estq_300.preco_medio_unitario%type;
    ant_preco_custo_unitario       estq_301.preco_custo_unitario%type;

    ant_saldo_financeiro_proj      estq_300.saldo_financeiro_proj%type;
    ant_preco_medio_unitario_proj  estq_300.preco_medio_unitario_proj%type;
    ant_preco_custo_unit_proj      estq_301.preco_custo_unit_proj%type;

    ant_saldo_financeiro_estimado  estq_300.saldo_financeiro_estimado%type;
    ant_preco_medio_unit_estimado  estq_300.preco_medio_unit_estimado%type;
    ant_preco_custo_unit_estimado  estq_301.preco_custo_unit_estimado%type;

    mov_preco_medio_unitario       estq_300.preco_medio_unitario%type;
    mov_preco_medio_unitario_proj  estq_300.preco_medio_unitario_proj%type;
    mov_preco_medio_unit_estimado  estq_300.preco_medio_unit_estimado%type;

    preco_medio_calc               estq_300.preco_medio_unitario%type;
    preco_medio_calc_proj          estq_300.preco_medio_unitario_proj%type;
    preco_medio_calc_estimado      estq_300.preco_medio_unit_estimado%type;

    registro                       varchar2(400);
    contador                       number;
    ajuste_sub                     varchar2(2);
    qtde_vl_total                  estq_300.quantidade%type;
    atualiza                       varchar2(1);

begin
   --dbms_output.enable(null);
   atualiza := 'S';

  for reg in registros loop
    contador := 0;
    begin
      select f.saldo_financeiro,                    f.preco_medio_unitario,                   f.preco_custo_unitario,
             f.saldo_financeiro_estimado,           f.preco_medio_unit_estimado,              f.preco_custo_unit_estimado,
             f.saldo_financeiro_proj,               f.preco_medio_unit_proj,                  f.preco_custo_unit_proj
        into ini_saldo_financeiro,                  ini_preco_medio_unitario,                 ini_preco_custo_unitario,
             ini_saldo_financeiro_estimado,         ini_preco_medio_unit_estimado,            ini_preco_custo_unit_estimado,
             ini_saldo_financeiro_proj,              ini_preco_medio_unit_proj,                ini_preco_custo_unit_proj
      from estq_301 f
        where f.codigo_deposito    = reg.dep
          and f.nivel_estrutura    = reg.nivel
          and f.grupo_estrutura    = reg.grupo
          and f.subgrupo_estrutura = reg.sub
          and f.item_estrutura     = reg.item
          and f.mes_ano_movimento < p_dt_ini_atualizacao
          and f.mes_ano_movimento  = trunc((to_date(p_dt_ini_atualizacao,'dd/mm/yyyy') - 1),'MM');
    exception
      when no_data_found then
      begin
        select nvl(g.saldo_financeiro,0),             nvl(g.preco_medio_unitario,0),            nvl(g.preco_custo_unitario,0),
               nvl(g.saldo_financeiro_estimado,0),    nvl(g.preco_medio_unit_estimado,0),       nvl(g.preco_custo_unit_estimado,0),
               nvl(g.saldo_financeiro_proj,0),        nvl(g.preco_medio_unit_proj,0),           nvl(g.preco_custo_unit_proj,0)
          into ini_saldo_financeiro,                  ini_preco_medio_unitario,                 ini_preco_custo_unitario,
               ini_saldo_financeiro_estimado,         ini_preco_medio_unit_estimado,            ini_preco_custo_unit_estimado,
               ini_saldo_financeiro_proj,              ini_preco_medio_unit_proj,                ini_preco_custo_unit_proj
        from estq_301 g
        where g.codigo_deposito    = reg.dep
          and g.nivel_estrutura    = reg.nivel
          and g.grupo_estrutura    = reg.grupo
          and g.subgrupo_estrutura = reg.sub
          and g.item_estrutura     = reg.item
          and g.mes_ano_movimento <  p_dt_ini_atualizacao
          and g.mes_ano_movimento  = (select max(h.mes_ano_movimento)
                                      from estq_301 h
                                      where h.codigo_deposito    = g.codigo_deposito
                                        and h.nivel_estrutura    = g.nivel_estrutura
                                        and h.grupo_estrutura    = g.grupo_estrutura
                                        and h.subgrupo_estrutura = g.subgrupo_estrutura
                                        and h.item_estrutura     = g.item_estrutura
                                        and h.mes_ano_movimento < p_dt_ini_atualizacao);
      exception
        when no_data_found then
        begin
           ini_saldo_financeiro         :=0;
           ini_preco_medio_unitario     :=0;
           ini_preco_custo_unitario     :=0;
           ini_saldo_financeiro_estimado:=0;
           ini_preco_medio_unit_estimado:=0;
           ini_preco_custo_unit_estimado:=0;
           ini_saldo_financeiro_proj    :=0;
           ini_preco_medio_unit_proj    :=0;
           ini_preco_custo_unit_proj    :=0;
        end;
      end;
    end;
      declare
      cursor skus is
        select
        a.codigo_deposito as dep2, a.nivel_estrutura as nivel2, a.grupo_estrutura as grupo2, a.subgrupo_estrutura as sub2, a.item_estrutura as item2,
        a.data_movimento, a.sequencia_ficha, a.sequencia_insercao, a.numero_documento, a.serie_documento,
        a.cnpj_9, a.cnpj_4, a.cnpj_2, a.sequencia_documento, a.entrada_saida,
        decode(a.entrada_saida,'S', (a.quantidade * -1),a.quantidade) as qtde,
        a.saldo_fisico
        from estq_300 a
        where a.codigo_deposito    = reg.dep
          and a.nivel_estrutura    = reg.nivel
          and a.grupo_estrutura    = reg.grupo
          and a.subgrupo_estrutura = reg.sub
          and a.item_estrutura     = reg.item
          and a.data_movimento between p_dt_ini_atualizacao and p_dt_fim_atualizacao
        order by a.codigo_deposito, a.nivel_estrutura, a.grupo_estrutura, a.subgrupo_estrutura, a.item_estrutura,
                 a.data_movimento, a.sequencia_ficha;

      begin
        registro:= '                                                                                                                                        ';
        --dbms_output.put_line(registro);
        registro:= 'CONT    QT T  DEP PRODUTO            DATA MOV    SF  SI   NUMDOC S CNPJ              SDOC    Q MOV  SALDO    DATA INI ATU   DATA FIM ATU';
        --dbms_output.put_line(registro);

        ant_saldo_financeiro           := ini_saldo_financeiro;
        ant_preco_medio_unitario       := ini_preco_medio_unitario;
        ant_preco_custo_unitario       := ini_preco_custo_unitario;

        ant_saldo_financeiro_proj      := ini_saldo_financeiro_proj;
        ant_preco_medio_unitario_proj  := ini_preco_medio_unit_proj;
        ant_preco_custo_unit_proj      := ini_preco_custo_unit_proj;

        ant_saldo_financeiro_estimado  := ini_saldo_financeiro_estimado;
        ant_preco_medio_unit_estimado  := ini_preco_medio_unit_estimado;
        ant_preco_custo_unit_estimado  := ini_preco_custo_unit_estimado;

        for reg2 in skus loop

            if reg2.entrada_saida = 'E' then
              begin
                select nvl(aa.preco_medio_unitario,0), nvl(aa.preco_medio_unitario_proj,0), nvl(aa.preco_medio_unit_estimado,0)
                  into mov_preco_medio_unitario,       mov_preco_medio_unitario_proj,       mov_preco_medio_unit_estimado
                from estq_300 aa, basi_205 bb
                where aa.codigo_deposito     = bb.codigo_deposito
                  and aa.codigo_deposito    <> reg2.dep2
                  and aa.nivel_estrutura     = reg2.nivel2
                  and aa.grupo_estrutura     = reg2.grupo2
                  and aa.subgrupo_estrutura  = reg2.sub2
                  and aa.item_estrutura      = reg2.item2
                  and aa.data_movimento      = reg2.data_movimento
                  and aa.numero_documento    = reg2.numero_documento
                  and aa.serie_documento     = reg2.serie_documento
                  and aa.cnpj_9              = reg2.cnpj_9
                  and aa.cnpj_4              = reg2.cnpj_4
                  and aa.cnpj_2              = reg2.cnpj_2
                  and aa.sequencia_documento = reg2.sequencia_documento
                  and aa.quantidade          = reg2.qtde
                  and bb.local_deposito      = reg.empr_dep
                  and aa.entrada_saida       = 'S';                  
                exception when others then
                  registro:= ' Erro busca custo: '||
                             reg2.dep2||' '||
                             reg2.nivel2||'.'||reg2.grupo2||'.'||reg2.sub2||ajuste_sub||'.'||reg2.item2 ||' '||
                             reg2.data_movimento||'   '||
                             reg2.sequencia_ficha||'   '||
                             reg2.sequencia_insercao||'   '||
                             reg2.numero_documento||' '||
                             reg2.serie_documento||' '||
                             lpad(reg2.cnpj_9,9,0)||'/'||lpad(reg2.cnpj_4,4,0)||'-'||lpad(reg2.cnpj_2,2,0)||' '||
                             lpad(reg2.sequencia_documento,5,0)||' '||
                             reg2.entrada_saida||' '||
                             lpad(reg2.qtde,6,0)||' '||
                             lpad(reg2.saldo_fisico,6,0)||'  -  '||
                             trunc((to_date(p_dt_ini_atualizacao,'dd/mm/yyyy')),'MM')||'  -  '||
                             (to_date(p_dt_fim_atualizacao,'dd/mm/yyyy'));
                          --dbms_output.put_line(registro);
                end;
              else
              mov_preco_medio_unitario      := ant_preco_medio_unitario;
              mov_preco_medio_unitario_proj := ant_preco_medio_unitario_proj;
              mov_preco_medio_unit_estimado := ant_preco_medio_unit_estimado;
            end if;

            if reg2.saldo_fisico > 0 then
              preco_medio_calc          := (ant_saldo_financeiro + (reg2.qtde * mov_preco_medio_unitario)) / reg2.saldo_fisico;
              preco_medio_calc_proj     := (ant_saldo_financeiro_proj + (reg2.qtde * mov_preco_medio_unitario_proj)) / reg2.saldo_fisico;
              preco_medio_calc_estimado := (ant_saldo_financeiro_estimado + (reg2.qtde * mov_preco_medio_unit_estimado)) / reg2.saldo_fisico;
            else
              preco_medio_calc          := ant_preco_medio_unitario;
              preco_medio_calc_proj     := ant_preco_medio_unitario_proj;
              preco_medio_calc_estimado := ant_preco_medio_unit_estimado;
            end if;

            if reg2.qtde < 0 then
              qtde_vl_total := reg2.qtde * -1;
            else
              qtde_vl_total := reg2.qtde;
            end if;

            if atualiza = 'S' then
              update estq_300 ee
              set ee.valor_contabil_unitario       = mov_preco_medio_unitario,

                  ee.valor_movimento_unitario      = mov_preco_medio_unitario,
                  ee.saldo_financeiro              = ant_saldo_financeiro + (reg2.qtde * mov_preco_medio_unitario),
                  ee.preco_medio_unitario          = preco_medio_calc,

                  ee.valor_movimento_unitario_proj = mov_preco_medio_unitario_proj,
                  ee.saldo_financeiro_proj         = ant_saldo_financeiro_proj + (reg2.qtde * mov_preco_medio_unitario_proj),
                  ee.preco_medio_unitario_proj     = preco_medio_calc_proj,

                  ee.valor_movto_unit_estimado     = mov_preco_medio_unit_estimado,
                  ee.saldo_financeiro_estimado     = ant_saldo_financeiro_estimado + (reg2.qtde * mov_preco_medio_unit_estimado),
                  ee.preco_medio_unit_estimado     = preco_medio_calc_estimado,

                  ee.valor_total                   = qtde_vl_total * mov_preco_medio_unitario

              where ee.codigo_deposito    = reg2.dep2
                and ee.nivel_estrutura    = reg2.nivel2
                and ee.grupo_estrutura    = reg2.grupo2
                and ee.subgrupo_estrutura = reg2.sub2
                and ee.item_estrutura     = reg2.item2
                and ee.data_movimento     = reg2.data_movimento
                and ee.numero_documento   = reg2.numero_documento
                and ee.serie_documento    = reg2.serie_documento
                and ee.cnpj_9             = reg2.cnpj_9
                and ee.cnpj_4             = reg2.cnpj_4
                and ee.cnpj_2             = reg2.cnpj_2
                and ee.sequencia_documento= reg2.sequencia_documento;

              update estq_301 bb
              set
                  bb.preco_custo_unitario          = mov_preco_medio_unitario,
                  bb.saldo_financeiro              = ant_saldo_financeiro + (reg2.qtde * mov_preco_medio_unitario),
                  bb.preco_medio_unitario          = preco_medio_calc,

                  bb.preco_custo_unit_proj         = mov_preco_medio_unitario_proj,
                  bb.saldo_financeiro_proj         = ant_saldo_financeiro_proj + (reg2.qtde * mov_preco_medio_unitario_proj),
                  bb.preco_medio_unit_proj         = preco_medio_calc_proj,

                  bb.preco_custo_unit_estimado     = mov_preco_medio_unit_estimado,
                  bb.saldo_financeiro_estimado     = ant_saldo_financeiro_estimado + (reg2.qtde * mov_preco_medio_unit_estimado),
                  bb.preco_medio_unit_estimado     = preco_medio_calc_estimado

              where bb.codigo_deposito    = reg2.dep2
                and bb.nivel_estrutura    = reg2.nivel2
                and bb.grupo_estrutura    = reg2.grupo2
                and bb.subgrupo_estrutura = reg2.sub2
                and bb.item_estrutura     = reg2.item2
          --      and bb.mes_ano_movimento  < p_dt_ini_atualizacao
                and bb.mes_ano_movimento   = trunc((to_date(p_dt_ini_atualizacao,'dd/mm/yyyy')),'MM');
          end if;

          ant_preco_medio_unitario      := mov_preco_medio_unitario;
          ant_preco_medio_unitario_proj := mov_preco_medio_unitario_proj;
          ant_preco_medio_unit_estimado := mov_preco_medio_unit_estimado;

          ant_saldo_financeiro          := ant_saldo_financeiro + (reg2.qtde * mov_preco_medio_unitario);
          ant_saldo_financeiro_proj     := ant_saldo_financeiro_proj + (reg2.qtde * mov_preco_medio_unitario_proj);
          ant_saldo_financeiro_estimado := ant_saldo_financeiro_estimado + (reg2.qtde * mov_preco_medio_unit_estimado);


          contador := contador + 1;
          ajuste_sub := null;
          if length(reg2.sub2) = 1 then
             ajuste_sub := '  ';
          end if;

          if length(reg2.sub2) = 2 then
             ajuste_sub := ' ';
          end if;

          registro:= lpad(contador,5,0)||' - '||lpad(reg.qtde_reg,5,0)||' '||
                     reg2.dep2||' '||
                     reg2.nivel2||'.'||reg2.grupo2||'.'||reg2.sub2||'.'||reg2.item2||ajuste_sub||' '||
                     reg2.data_movimento||'   '||
                     reg2.sequencia_ficha||'   '||
                     reg2.sequencia_insercao||'   '||
                     reg2.numero_documento||' '||
                     reg2.serie_documento||' '||
                     lpad(reg2.cnpj_9,9,0)||'/'||lpad(reg2.cnpj_4,4,0)||'-'||lpad(reg2.cnpj_2,2,0)||' '||
                     lpad(reg2.sequencia_documento,5,0)||' '||
                     reg2.entrada_saida||' '||
                     lpad(reg2.qtde,6,0)||' '||
                     lpad(reg2.saldo_fisico,6,0)||'  -  '||
                     trunc((to_date(p_dt_ini_atualizacao,'dd/mm/yyyy')),'MM')||'  -  '||
                     (to_date(p_dt_fim_atualizacao,'dd/mm/yyyy'));
          --dbms_output.put_line(registro);

        end loop;
      end;
      commit;
 --   end;
  end loop;
end INTER_PR_CALC_CARDEX_CONSIG;
/
exec inter_pr_recompile;
/
