CREATE OR REPLACE FUNCTION inter_fn_codigo_blk
        (COD_ESTAGIO_AGRUPADOR_INSU  NUMBER,
         COD_ESTAGIO_SIMULTANEO_INSU NUMBER,
         pcod_empresa      obrf_158.cod_empresa%type,
         pdata_emissao     date,
         SEQ_OPERACAO_AGRUPADOR_INSU NUMBER)

RETURN VARCHAR2

IS

tp_impressao_cod_produto obrf_158.tp_impressao_cod_prod%type;
cod_estagio              VARCHAR2(100);
v_dtiniblok              date;

BEGIN
  cod_estagio := '';
  if (COD_ESTAGIO_AGRUPADOR_INSU + COD_ESTAGIO_SIMULTANEO_INSU) > 0
  then
    begin
       select obrf_158.tp_impressao_cod_prod
       into tp_impressao_cod_produto
       from obrf_158
       where obrf_158.cod_empresa = pcod_empresa
       and rownum = 1;
    end;
    
    begin    
       select val_dat
       into v_dtiniblok
       from empr_008
       where empr_008.codigo_empresa = pcod_empresa
       and empr_008.param = 'obrf.dataIniBlocoK';
    exception
    when no_data_found then
       v_dtiniblok := sysdate + 3365;
    end;

    if tp_impressao_cod_produto = 1 or v_dtiniblok <= pdata_emissao 
    then
       if  COD_ESTAGIO_SIMULTANEO_INSU > 0
       then
           cod_estagio := '.' || trim(to_char(COD_ESTAGIO_AGRUPADOR_INSU, '99900')) ||
                          '.' || trim(to_char(COD_ESTAGIO_SIMULTANEO_INSU, '99900'));
       else
           if SEQ_OPERACAO_AGRUPADOR_INSU > 0
           then
               cod_estagio := '.' || trim(to_char(COD_ESTAGIO_AGRUPADOR_INSU, '99900')) ||
                              '.' || trim(to_char(SEQ_OPERACAO_AGRUPADOR_INSU, '99900'));
           else
              cod_estagio := '.' || trim(to_char(COD_ESTAGIO_AGRUPADOR_INSU, '99900'));
           end if;
       end if;
    else
       cod_estagio := trim(to_char(COD_ESTAGIO_AGRUPADOR_INSU, '99900')) || 
                      trim(to_char(COD_ESTAGIO_SIMULTANEO_INSU, '99900'));

       if  COD_ESTAGIO_SIMULTANEO_INSU > 0
       then
           cod_estagio := trim(to_char(COD_ESTAGIO_AGRUPADOR_INSU, '99900')) || 
                          trim(to_char(COD_ESTAGIO_SIMULTANEO_INSU, '99900'));
     else
        if SEQ_OPERACAO_AGRUPADOR_INSU > 0
        then
           cod_estagio := trim(to_char(COD_ESTAGIO_AGRUPADOR_INSU, '99900')) ||
                          trim(to_char(SEQ_OPERACAO_AGRUPADOR_INSU, '99900'));
        else
           cod_estagio := trim(to_char(COD_ESTAGIO_AGRUPADOR_INSU, '99900'));
        end if;
       end if;
    end if;    
  end if;
  return cod_estagio;

end inter_fn_codigo_blk;
