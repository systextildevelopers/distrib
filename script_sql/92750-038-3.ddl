create table pedi_184
(
cod_empresa numeric(3) default 0,
nivel varchar2(1) default ' ',
grupo varchar2(5) default ' ',
subgrupo varchar2(3) default ' ',
item varchar2(6) default ' ',
valor_compra numeric(10,2) default 0.0,
outros_custos numeric(10,2) default 0.0,
custo_total numeric(10,2) default 0.0,
icms numeric(10,2) default 0.0,
pis numeric(10,2) default 0.0,
cofins numeric(10,2) default 0.0,
comissao numeric(10,2) default 0.0,
lucro numeric(10,2) default 0.0,
acrescimo numeric(10,2) default 0.0,
sugerido numeric(10,2) default 0.0,
valor_final numeric(10,2) default 0.0
);

alter table pedi_184 add constraint PK_PEDI_184 primary key (cod_empresa, nivel, grupo, subgrupo, item);
