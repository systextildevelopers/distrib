
CREATE OR REPLACE FUNCTION "SALDO_DUPLIC_CREC_ESTORNO"
        (emp_duplic in fatu_070.codigo_empresa%type,
         num_duplic in fatu_070.num_duplicata%type,
         seq_duplic in fatu_070.seq_duplicatas%type,
         tip_duplic in fatu_070.tipo_titulo%type,
         cg9_duplic in fatu_070.cli_dup_cgc_cli9%type,
         cg4_duplic in fatu_070.cli_dup_cgc_cli4%type,
         cg2_duplic in fatu_070.cli_dup_cgc_cli2%type)
RETURN number
IS
   v_saldo_estorno    number(15,7);
begin
   SELECT sum(FATU_075.VALOR_PAGO - FATU_075.VALOR_JUROS + VALOR_DESCONTOS - FATU_075.VLR_VAR_CAMBIAL) into v_saldo_estorno
   FROM FATU_070, FATU_075, CONT_010
   WHERE CONT_010.CODIGO_HISTORICO          = FATU_075.HISTORICO_PGTO
     AND CONT_010.SINAL_TITULO              = 2
     AND FATU_075.NR_TITUL_CODEMPR          = FATU_070.CODIGO_EMPRESA
     AND FATU_075.NR_TITUL_CLI_DUP_CGC_CLI9 = FATU_070.CLI_DUP_CGC_CLI9
     AND FATU_075.NR_TITUL_CLI_DUP_CGC_CLI4 = FATU_070.CLI_DUP_CGC_CLI4
     AND FATU_075.NR_TITUL_CLI_DUP_CGC_CLI2 = FATU_070.CLI_DUP_CGC_CLI2
     AND FATU_075.NR_TITUL_COD_TIT          = FATU_070.TIPO_TITULO
     AND FATU_075.NR_TITUL_NUM_DUP          = FATU_070.NUM_DUPLICATA
     AND FATU_075.NR_TITUL_SEQ_DUP          = FATU_070.SEQ_DUPLICATAS
     and fatu_070.codigo_empresa            = emp_duplic
     and fatu_070.num_duplicata             = num_duplic
     and fatu_070.seq_duplicatas            = seq_duplic
     and fatu_070.tipo_titulo               = tip_duplic
     and fatu_070.cli_dup_cgc_cli9          = cg9_duplic
     and fatu_070.cli_dup_cgc_cli4          = cg4_duplic
     and fatu_070.cli_dup_cgc_cli2          = cg2_duplic
   group by FATU_070.NUM_DUPLICATA,
            FATU_070.SEQ_DUPLICATAS,
            FATU_070.TIPO_TITULO,
            FATU_070.CODIGO_EMPRESA,
            FATU_070.CLI_DUP_CGC_CLI9,
            FATU_070.CLI_DUP_CGC_CLI4,
            FATU_070.CLI_DUP_CGC_CLI2,
            FATU_070.VALOR_DUPLICATA;

   return(v_saldo_estorno);

end saldo_duplic_crec_estorno;

 

/

exec inter_pr_recompile;

