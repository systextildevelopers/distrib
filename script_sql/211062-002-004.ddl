insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('cpag_f666', 'Detalhes das informações de baixa', 1, 0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'cpag_f666', 'nenhum', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao         = 'Detalhes das informações de baixa'
where hdoc_036.codigo_programa = 'cpag_f666'
  and hdoc_036.locale          = 'es_ES';
  
commit work;
