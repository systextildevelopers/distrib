alter table obrf_010 add (ind_rateio_substituicao number(1));

comment on column obrf_010.ind_rateio_substituicao is 'Indica qual a situa��o do rateio da substitui��o: 0 - n�o selecionado 1 - selecionado  2 - processado';

exec inter_pr_recompile;
/
