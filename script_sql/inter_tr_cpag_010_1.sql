
  CREATE OR REPLACE TRIGGER "INTER_TR_CPAG_010_1" 
   before insert or
          update of sit_bloqueio
   on cpag_010
   for each row
declare
   v_executa_trigger number;
   v_tem_registro    number;
begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if  updating
      or  inserting
      and (:new.sit_bloqueio  <> :old.sit_bloqueio
      and  :new.sit_bloqueio  = 1)
      then
         select nvl(count(1),0)
         into v_tem_registro
         from hdoc_110
         where hdoc_110.empresa         = :new.codigo_empresa
           and hdoc_110.documento       = :new.nr_duplicata
           and hdoc_110.sequencia       = 0
           and hdoc_110.serie_parcela   = :new.parcela
           and hdoc_110.tipo_titulo     = :new.tipo_titulo
           and hdoc_110.cgc9            = :new.cgc_9
           and hdoc_110.cgc4            = :new.cgc_4
           and hdoc_110.cgc2            = :new.cgc_2
           and hdoc_110.codigo_processo = 4;

         if v_tem_registro > 0
         then
            begin
               delete from hdoc_110
               where hdoc_110.empresa         = :new.codigo_empresa
                 and hdoc_110.documento       = :new.nr_duplicata
                 and hdoc_110.sequencia       = 0
                 and hdoc_110.serie_parcela   = :new.parcela
                 and hdoc_110.tipo_titulo     = :new.tipo_titulo
                 and hdoc_110.cgc9            = :new.cgc_9
                 and hdoc_110.cgc4            = :new.cgc_4
                 and hdoc_110.cgc2            = :new.cgc_2
                 and hdoc_110.codigo_processo = 4;
            end;
         end if;
      end if;
   end if;
end inter_tr_cpag_010_1;

-- ALTER TRIGGER "INTER_TR_CPAG_010_1" ENABLE
 

/

exec inter_pr_recompile;

