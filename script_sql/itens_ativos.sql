create or replace function itens_ativos(p_pedido_venda number)

return number 

is

    v_count number;
begin
    select count(1)
    into v_count
    from pedi_110 
    where pedido_venda = p_pedido_venda
        and COD_CANCELAMENTO = 0;
    
    return v_count;
end;
