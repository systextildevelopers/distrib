create table oper_021
(
CODIGO_CONTABIL  NUMBER(6)  default 0,
CODIGO_TRANSACAO NUMBER(3)  default 0,
CODIGO_HISTORICO_CONT NUMBER(4)  default 0,
POSICAO_TITULO NUMBER(9) default 0,
COD_PORTADOR     NUMBER(3)  default 0,
CODIGO_HISTORICO_PGTO NUMBER(4)  default 0,
CONTA_CORRENTE   NUMBER(9)   default 0
);

exec inter_pr_recompile;
