DECLARE 
 col_count  integer;
BEGIN 
 SELECT count(*)
   into col_count
 FROM user_tab_columns
 WHERE table_name = 'pcpb_665'
 AND column_name = 'status_impressao';

 IF col_count = 0 THEN 
    EXECUTE IMMEDIATE 'ALTER TABLE pcpb_665 add status_impressao number(1) default ''0'' not null';
    COMMIT;
 END IF;
 exception when others then
 if sqlcode = -01430 || -06512 then
   null;
 end if;
END;
