declare 
   nro_registro number;
   
begin
   nro_registro := 0;
   

   for pedi100 in (select rowid
                   from pedi_100
                   where tipo_desconto not in (1,2))
   loop
  	begin
	     update pedi_100
	     set tipo_desconto = 1
	     where rowid  = pedi100.rowid
	     and situacao_venda not in (9,10)
	     and cod_cancelamento = 0;
  	end;
      
      nro_registro := nro_registro + 1;
      
      if nro_registro > 100
      then
         commit;
         nro_registro := 0;
      end if;
   end loop;
   
   commit;
end;

/

exec inter_pr_recompile;

/
