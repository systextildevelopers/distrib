alter table i_pedi_010
add estado varchar2(2);

alter table i_pedi_010
modify estado default '';

alter table i_pedi_010
add cidade varchar2(100);

alter table i_pedi_010
modify cidade default '';

alter table i_pedi_010
add pais varchar2(100);

alter table i_pedi_010
modify pais default '';

/
exec inter_pr_recompile;
