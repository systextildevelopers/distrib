create or replace procedure "INTER_PR_GERA_LANC_CONT" (p_cod_empresa IN NUMBER,
                                                      p_origem IN NUMBER,
                                                      p_num_lanc IN OUT NUMBER,
                                                      p_centro_custo  IN NUMBER,
                                                      p_data_lancto IN DATE,
                                                      p_hist_contabil IN OUT NUMBER,
                                                      p_compl_histor1 IN VARCHAR2,
                                                      p_estorno IN NUMBER,
                                                      p_transacao IN NUMBER,
                                                      p_conta_debito IN NUMBER,
                                                      p_valor_debito IN NUMBER, --REVISAR
                                                      p_conta_credito IN NUMBER,
                                                      p_valor_credito IN NUMBER, --REVISAR
                                                      p_banco_func IN NUMBER,
                                                      p_conta_func IN NUMBER,
                                                      p_data_func IN DATE,
                                                      p_docto_func IN NUMBER,
                                                      p_cnpj9 IN NUMBER,
                                                      p_cnpj4 IN NUMBER,
                                                      p_cnpj2 IN NUMBER,
                                                      p_tipo_cnpj IN NUMBER,
                                                      p_num_documento IN NUMBER,
                                                      p_parcela_serie IN VARCHAR2,
                                                      p_tipo_titulo IN NUMBER,
                                                      p_seq_pagamento IN NUMBER,
                                                      p_cod_imposto IN NUMBER,
                                                      p_projeto IN NUMBER,
                                                      p_subprojeto IN NUMBER,
                                                      p_servico IN NUMBER,
                                                      p_des_erro OUT varchar2) IS

/****** ATEN��O ATEN��O ATEN��O *********

 EST� FUN��O POSSUI CONTROLE DE TRANSA��O AO ADICONAR UMA NOVA FUN��O A ELA N�O PERDER ESSA INTEGRIDADE

 ****** ATEN��O ATEN��O ATEN��O *********/

cursor seq_lanc (p_empresa_matriz number, p_exercicio number, p_num_lanc number) is
   select cont_600.numero_lanc numero_lanc_reg,                            cont_600.seq_lanc seq_lanc_reg,
         cont_600.origem origem_reg,                                   cont_600.lote lote_reg,
         cont_600.conta_contabil conta_contabil_reg,                   cont_600.conta_reduzida conta_reduzida_reg,
         cont_600.subconta subconta_reg,                               cont_600.centro_custo centro_custo_reg,
         cont_600.debito_credito debito_credito_reg,                   cont_600.compl_histor1 compl_histor1_reg,
         cont_600.compl_histor2 compl_histor2_reg,                     cont_600.valor_lancto valor_lancto_reg,
         cont_600.banco banco_reg,                                     cont_600.conta_corrente conta_corrente_reg,
         cont_600.data_controle data_controle_reg,                     cont_600.documento documento_reg,
         cont_600.filial_lancto filial_lancto_reg,                     cont_600.cnpj9_participante cnpj9_participante_reg,
         cont_600.cnpj4_participante cnpj4_participante_reg,           cont_600.cnpj2_participante cnpj2_participante_reg,
         cont_600.num_documento num_documento_reg,                     cont_600.parcela_serie parcela_serie_reg,
         cont_600.tipo_titulo tipo_titulo_reg,                         cont_600.seq_pagamento seq_pagamento_reg,
         cont_600.cliente_fornecedor_part cliente_fornecedor_part_reg, cont_600.cod_imposto cod_imposto_reg
   from cont_600
   where cont_600.cod_empresa = p_empresa_matriz
     and cont_600.exercicio   = p_exercicio
     and cont_600.numero_lanc = p_num_lanc;

   teste                      varchar2(10);
   v_prg_gerador              varchar2(100);
   usuario_rede               varchar(20);
   maquina_rede               varchar(40);
   aplicativo                 varchar(20);
   sid                        sys.gv_$session.sid%type;
   empresa                    fatu_500.codigo_empresa%TYPE;
   locale_usuario             hdoc_030.locale%TYPE;

   gera_contabil_func         fatu_500.gera_contabil%type;
   v_empresa_matriz           fatu_500.codigo_matriz%type;
   v_exercicio                cont_500.exercicio%type;
   plano_exercicio            cont_500.cod_plano_cta%type;
   v_periodo                  cont_510.periodo%type;
   v_seq_cont                 cont_600.seq_lanc%type;
   sq_lancam                  cont_600.seq_lanc%type;
   v_centro_custo_lanc        cont_600.centro_custo%type;
   sit_lote_lanc              cont_520.situacao%type;
   v_debito_credito           varchar2(1);
   v_deb_cred                 varchar2(1);
   v_lote                     number;

   f_consiste_tipo_natureza   fatu_503.consiste_tipo_natureza%type;
   f_tipo_natureza            cont_535.tipo_natureza%type;
   f_custo_despesa            basi_185.custo_despesa%type;
   contabiliza_trans_f        estq_005.atualiza_contabi%type;
   contab_cardex              estq_005.contab_cardex%type;

   guarda_filial              number;
   num_lancto                 number;
   v_conta_reduzida           cont_600.conta_reduzida%type;
   v_valor_contabil             cont_600.valor_lancto%type;

   conta_reduzida_trans_deb   number;
   conta_reduzida_trans_cre   number;
   debito_credito_trans       varchar2(1);
   filial_transitoria         number;

   v_conta_contabil           cont_535.conta_contabil%type;
   v_subconta                 cont_535.subconta%type;
   patr_result                cont_535.patr_result%type;
   exige_subconta             cont_535.exige_subconta%type;
   ult_num_lanc               cont_500.ultimo_lanc%type;
   tipo_origem_f              cont_050.tipo%type;
   tem_lancto_f               varchar2(1);
BEGIN

   BEGIN
     select fatu_500.gera_contabil, fatu_500.codigo_matriz
     into gera_contabil_func,     v_empresa_matriz
     from fatu_500
     where fatu_500.codigo_empresa = p_cod_empresa;
     EXCEPTION
      WHEN NO_DATA_FOUND
        THEN
          gera_contabil_func := 0;
          v_empresa_matriz := 0;
   END;

   BEGIN
   select fatu_503.consiste_tipo_natureza
   into f_consiste_tipo_natureza
   from fatu_503
   where fatu_503.codigo_empresa = p_cod_empresa;
   EXCEPTION
   WHEN NO_DATA_FOUND
     THEN
      f_consiste_tipo_natureza := 0;
   END;

   if gera_contabil_func = 1   /* empresa gera contabiliza��o */
   then
      v_prg_gerador := 'EXPENSEON';

      /* acha o exercicio e o periodo do lancamento */
      select cont_500.exercicio, cont_500.cod_plano_cta
      into v_exercicio, plano_exercicio
      from cont_500
      where cont_500.cod_empresa  = v_empresa_matriz
      and   cont_500.per_inicial <= to_date(p_data_lancto)
      and   cont_500.per_final   >= to_date(p_data_lancto);

      select cont_510.periodo
      into v_periodo
      from cont_510
      where cont_510.cod_empresa  = v_empresa_matriz
      and   cont_510.exercicio    = v_exercicio
      and   cont_510.per_inicial <= to_date(p_data_lancto)
      and   cont_510.per_final   >= to_date(p_data_lancto);

      if p_estorno = 3  /* cancelamento de documentos */
      THEN

         BEGIN
            select max(nvl(cont_600.seq_lanc,0))
            into v_seq_cont
            from cont_600
            where cont_600.cod_empresa = v_empresa_matriz
            and   cont_600.exercicio   = v_exercicio
            and   cont_600.numero_lanc = p_num_lanc;
            EXCEPTION
            WHEN NO_DATA_FOUND
               THEN
                 v_seq_cont := 0;
         END;

         for reg_seq_lanc in seq_lanc (v_empresa_matriz, v_exercicio, p_num_lanc)
         LOOP

            BEGIN
            select cont_520.situacao
            into sit_lote_lanc
            from cont_520
            where cont_520.cod_empresa = v_empresa_matriz
            and   cont_520.exercicio   = v_exercicio
            and   cont_520.origem      = reg_seq_lanc.origem_reg
            and   cont_520.lote        = reg_seq_lanc.lote_reg;
            EXCEPTION
               WHEN NO_DATA_FOUND
                 THEN
                   sit_lote_lanc := -1;
            END;

            if sit_lote_lanc = 0
            THEN
               BEGIN
                 delete from cont_600
                 where cont_600.cod_empresa = v_empresa_matriz
                 and   cont_600.exercicio   = v_exercicio
                 and   cont_600.numero_lanc = p_num_lanc
                 and   cont_600.seq_lanc    = reg_seq_lanc.seq_lanc_reg;

              EXCEPTION
              WHEN OTHERS THEN
                p_des_erro := inter_fn_buscar_tag_composta('ds22948', to_char(SQLCODE), '' , '' , '' , '' , '' , '' , '' , '' , '', 'pt_BR', 'INTERSYS');
              END;
            ELSE
               if p_hist_contabil is null
               then
                  select cont_560.hst_canc_ent
                  into p_hist_contabil
                  from cont_560
                  where cont_560.cod_plano_cta = plano_exercicio;
               end if;

               if v_debito_credito = 'D'
                  then v_debito_credito := 'C';
                  else v_debito_credito := 'D';
               end if;

               v_lote := inter_fn_acha_lote(v_empresa_matriz,v_exercicio,p_origem,v_lote,p_data_lancto);

               if v_lote <> 0
               THEN
                  v_seq_cont := v_seq_cont + 1;

                  /***** BUSCA O TIPO DA NATUREZA DA CONTA CONTABIL ****/
                  BEGIN
                    select cont_535.tipo_natureza
                    into f_tipo_natureza
                    from cont_535
                    where cont_535.cod_plano_cta = plano_exercicio
                    and cont_535.cod_reduzido  = v_conta_reduzida;
                  EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN f_tipo_natureza := 0;
                  END;

                  BEGIN
                    select basi_185.custo_despesa
                    into f_custo_despesa
                    from basi_185
                    where basi_185.centro_custo = centro_custo;
                  EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN f_custo_despesa := 0;
                  END;

                  if f_consiste_tipo_natureza = 0                   or /* CONFIG PARA NAO CONSISTIR */
                  (f_tipo_natureza  = 1 and f_custo_despesa  = 1) or /* NATUREZA DA CONTA E C.CUSTO SAO DE CUSTOS   */
                  (f_tipo_natureza  = 2 and f_custo_despesa  = 2) or /* NATUREZA DA CONTA E C.CUSTO SAO DE DESPESAS */
                  (f_tipo_natureza <> 1 and f_tipo_natureza <> 2)    /* NATUREZA DA CONTA E C.CUSTO NAO SAO DE DESPESAS NEM CUSTOS */
                  THEN

                     insert into cont_600 (
                        cod_empresa,           exercicio,
                        numero_lanc,           seq_lanc,
                        origem,                lote,
                        periodo,               conta_contabil,
                        conta_reduzida,        subconta,
                        centro_custo,          debito_credito,
                        hist_contabil,         compl_histor1,
                        compl_histor2,         data_lancto,
                        valor_lancto,          prg_gerador,
                        usuario,               filial_lancto,
                        banco,                 conta_corrente,
                        data_controle,         documento,
                        cnpj9_participante,
                        cnpj4_participante,    cnpj2_participante,
                        cliente_fornecedor_part,
                        num_documento,         parcela_serie,
                        tipo_titulo,           seq_pagamento,
                        cod_imposto
                     ) VALUES (
                        v_empresa_matriz,                       v_exercicio,
                        p_num_lanc,                             v_seq_cont,
                        reg_seq_lanc.origem_reg,                  reg_seq_lanc.lote_reg,
                        v_periodo,                                reg_seq_lanc.conta_contabil_reg,
                        reg_seq_lanc.conta_reduzida_reg,          reg_seq_lanc.subconta_reg,
                        reg_seq_lanc.centro_custo_reg,            reg_seq_lanc.debito_credito_reg,
                        p_hist_contabil,                          reg_seq_lanc.compl_histor1_reg,
                        reg_seq_lanc.compl_histor2_reg,           p_data_lancto,
                        reg_seq_lanc.valor_lancto_reg,            v_prg_gerador,
                        'INTERSYS',                      reg_seq_lanc.filial_lancto_reg,
                        reg_seq_lanc.banco_reg,                   reg_seq_lanc.conta_corrente_reg,
                        reg_seq_lanc.data_controle_reg,           reg_seq_lanc.documento_reg,
                        reg_seq_lanc.cnpj9_participante_reg,
                        reg_seq_lanc.cnpj4_participante_reg,      reg_seq_lanc.cnpj2_participante_reg,
                        p_tipo_cnpj,
                        reg_seq_lanc.num_documento_reg,           reg_seq_lanc.parcela_serie_reg,
                        reg_seq_lanc.tipo_titulo_reg,             reg_seq_lanc.seq_pagamento_reg,
                        reg_seq_lanc.cod_imposto_reg
                     );
                  ELSE
                     if substr(v_prg_gerador, 1,5) <> 'inte_'
                     THEN
                        p_des_erro := inter_fn_buscar_tag('ds30441','pt_BR','INTERSYS');
                     END IF;
                  END IF;
               END IF;
            END IF;
         END LOOP;
      ELSE

         if p_transacao > 0
         THEN
            BEGIN
               select estq_005.atualiza_contabi, estq_005.contab_cardex
               into contabiliza_trans_f,       contab_cardex
               from estq_005
               where estq_005.codigo_transacao = p_transacao;
            EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               contabiliza_trans_f := 2;
               contab_cardex := 2;
            END;
         ELSE
            contabiliza_trans_f := 1;
            contab_cardex := 1;
         END IF;

         if ((contabiliza_trans_f = 1 or contabiliza_trans_f = 3) and v_prg_gerador <> 'cont_f820') or
                (contab_cardex       = 1                             and v_prg_gerador  = 'cont_f820')
            THEN
            num_lancto := 1;

            /* guarda a empresa da contabilizacao, pois dependendo da partida contabil,
                  o empresa do lancamento da partida ira da empresa do documento para a empresa da conta corrente */

            guarda_filial := p_cod_empresa;

            while num_lancto <= 2
            loop
               /* verifica se e' lancamento de debito (1) ou de credito (2) */
               if num_lancto = 1
               then
                  v_conta_reduzida := p_conta_debito;
                  v_valor_contabil := p_valor_debito;

                  if p_estorno = 1
                  then v_deb_cred := 'D';
                  else v_deb_cred := 'C';
                  end if;
               else
                  v_conta_reduzida := p_conta_credito;
                  v_valor_contabil := p_valor_credito;

                  if p_estorno = 1
                  then v_deb_cred := 'C';
                  else v_deb_cred := 'D';
                  end if;
               end if;

               conta_reduzida_trans_deb := 0;
               conta_reduzida_trans_cre := 0;
               debito_credito_trans := 'X';
               filial_transitoria := p_cod_empresa;

               /* se o cadastro lido estiver paramatrizado com a mesma natureza (D/C) que o lancamento
                 da partida contabil, a empresa do lancamento da partida contabil sera a empresa indicada no cadastro.
                 caso contrario, a empresa da partida contabil sera a empresa do lancamento contabil
                 (do documento ou da conta corrente) */

               /* faz o lancamento contabil apenas se o valor for maior que
                 zero, desta forma, pode-se realizar um lancamento sem a
                 contrapartida */

               if v_valor_contabil <> 0.00 and v_conta_reduzida > 0
               then

                  BEGIN
                     /*****   ATUALIZA LANCAMENTO CONTABIL ****/
                     select   cont_535.conta_contabil, cont_535.subconta,
                          cont_535.patr_result,    cont_535.exige_subconta
                     into     v_conta_contabil,        v_subconta,
                          patr_result,                exige_subconta
                     from cont_535
                     where cont_535.cod_plano_cta = plano_exercicio
                     and   cont_535.cod_reduzido  = v_conta_reduzida;
                     EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        v_conta_contabil := '';
                        v_subconta       := 0;
                        patr_result    := 0;
                        exige_subconta := 0;
                  END;

                  if patr_result = 1 or (patr_result = 2 and exige_subconta = 2)
                  then
                     v_centro_custo_lanc := 0;
                  else
                     v_centro_custo_lanc := p_centro_custo;
                  end if;

                  /* se nao existe o numero de lancamento, entao acha
                    qual e' proximo */
                  if p_num_lanc = 0
                  then
                     BEGIN
                        update cont_500
                        set ultimo_lanc = ultimo_lanc
                        where cod_empresa = v_empresa_matriz
                        and   exercicio   = v_exercicio;
                     EXCEPTION
                     WHEN OTHERS THEN
                        if substr(v_prg_gerador, 1,5) <> 'inte_'
                        then
                           p_des_erro := inter_fn_buscar_tag_composta('ds22952', to_char(SQLCODE) , '' , '' , '' , '' , '' , '' , '' , '' , '' , 'pt_BR', 'INTERSYS');
                        end if;
                     END;

                     select cont_500.ultimo_lanc
                     into ult_num_lanc
                     from cont_500
                     where cont_500.cod_empresa  = v_empresa_matriz
                     and   cont_500.exercicio    = v_exercicio;

                     p_num_lanc := ult_num_lanc + 1;

                     BEGIN
                        update cont_500
                        set ultimo_lanc = p_num_lanc
                        where cod_empresa = v_empresa_matriz
                        and   exercicio   = v_exercicio;
                     EXCEPTION
                     WHEN OTHERS THEN
                        if substr(v_prg_gerador, 1,5) <> 'inte_'
                        then
                           p_des_erro := inter_fn_buscar_tag_composta('ds22952', to_char(SQLCODE) , '' , '' , '' , '' , '' , '' , '' , '' , '' , 'pt_BR', 'INTERSYS');
                        end if;
                     END;
                  end if;

                  v_lote := inter_fn_acha_lote(v_empresa_matriz,v_exercicio,p_origem,0,p_data_lancto);

                  BEGIN
                     select cont_050.tipo
                     into tipo_origem_f
                     from cont_050
                     where cont_050.origem = p_origem;
                  EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     tipo_origem_f := 1;
                  END;

                  tem_lancto_f := 's';

                  if tipo_origem_f <> 3
                  then
                     BEGIN
                        select cont_600.seq_lanc
                        into v_seq_cont
                        from cont_600
                        where cont_600.cod_empresa        = v_empresa_matriz
                        and   cont_600.exercicio          = v_exercicio
                        and   cont_600.numero_lanc        = p_num_lanc
                        and   cont_600.debito_credito     = v_deb_cred
                        and   cont_600.conta_reduzida     = v_conta_reduzida
                        and   cont_600.centro_custo       = v_centro_custo_lanc
                        and   cont_600.lote               = v_lote
                        and   cont_600.filial_lancto      = p_cod_empresa
                        and   cont_600.hist_contabil      = p_hist_contabil
                        and   cont_600.compl_histor1      = p_compl_histor1;
                     EXCEPTION
                     WHEN OTHERS
                     THEN
                        tem_lancto_f := 'n';
                     END;
                  else
                     tem_lancto_f := 'n';
                  end if;

                  if tem_lancto_f = 's'
                  then
                     BEGIN
                        update cont_600
                        set valor_lancto = cont_600.valor_lancto + v_valor_contabil
                        where cont_600.cod_empresa    = v_empresa_matriz
                        and   cont_600.exercicio      = v_exercicio
                        and   cont_600.numero_lanc    = p_num_lanc
                        and   cont_600.seq_lanc       = v_seq_cont;
                     EXCEPTION
                     WHEN OTHERS THEN
                        if substr(v_prg_gerador, 1,5) <> 'inte_'
                        then
                           p_des_erro := inter_fn_buscar_tag_composta('ds22953', to_char(SQLCODE) , '' , '' , '' , '' , '' , '' , '' , '' , '' , 'pt_BR', 'INTERSYS');
                        end if;
                     END;
                  else

                     /***** BUSCA O TIPO DA NATUREZA DA CONTA CONTABIL ****/
                     BEGIN
                        select cont_535.tipo_natureza
                        into f_tipo_natureza
                        from cont_535
                        where cont_535.cod_plano_cta = plano_exercicio
                        and cont_535.cod_reduzido  = v_conta_reduzida;
                     EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        f_tipo_natureza := 0;
                     END;

                     BEGIN
                        select basi_185.custo_despesa
                        into f_custo_despesa
                        from basi_185
                        where basi_185.centro_custo = v_centro_custo_lanc;
                     EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        f_custo_despesa := 0;
                     END;

                     if f_consiste_tipo_natureza = 0                 or /* CONFIG PARA NAO CONSISTIR */
                     (f_tipo_natureza  = 1 and f_custo_despesa  = 1) or /* NATUREZA DA CONTA E C.CUSTO SAO DE CUSTOS   */
                     (f_tipo_natureza  = 2 and f_custo_despesa  = 2) or /* NATUREZA DA CONTA E C.CUSTO SAO DE DESPESAS */
                     (f_tipo_natureza <> 1 and f_tipo_natureza <> 2)    /* NATUREZA DA CONTA E C.CUSTO NAO SAO DE DESPESAS NEM CUSTOS */
                     then

                        sq_lancam := 0;

                        BEGIN

                           select nvl(max(cont_600.seq_lanc),0)
                           into sq_lancam
                           from cont_600
                           where cont_600.cod_empresa    = v_empresa_matriz
                           and   cont_600.exercicio      = v_exercicio
                           and   cont_600.numero_lanc    = p_num_lanc;

                        EXCEPTION
                        WHEN NO_DATA_FOUND
                           THEN
                             sq_lancam := 0;
                        END;

                         sq_lancam := sq_lancam + 1;

                        insert into cont_600 (
                           cod_empresa,        exercicio,
                           numero_lanc,        seq_lanc,
                           origem,             lote,
                           periodo,            conta_contabil,
                           conta_reduzida,     subconta,
                           centro_custo,       debito_credito,
                           hist_contabil,      compl_histor1,
                           data_lancto,        valor_lancto,
                           prg_gerador,        usuario,
                           filial_lancto,      banco,
                           conta_corrente,     data_controle,
                           documento,          cnpj9_participante,
                           cnpj4_participante, cnpj2_participante,
                           cliente_fornecedor_part,
                           num_documento,      parcela_serie,
                           tipo_titulo,        seq_pagamento,
                           cod_imposto
                        ) VALUES (
                           v_empresa_matriz,   v_exercicio,
                           p_num_lanc,         sq_lancam,
                           p_origem,           v_lote,
                           v_periodo,          v_conta_contabil,
                           v_conta_reduzida,   v_subconta,
                           v_centro_custo_lanc,  v_deb_cred,
                           p_hist_contabil,    p_compl_histor1,
                           p_data_lancto,      v_valor_contabil,
                           v_prg_gerador,      'INTERSYS',
                           p_cod_empresa,       p_banco_func,
                           p_conta_func,       p_data_func,
                           p_docto_func,       p_cnpj9,
                           p_cnpj4,            p_cnpj2,
                           p_tipo_cnpj,
                           p_num_documento,    p_parcela_serie,
                           p_tipo_titulo,      p_seq_pagamento,
                           p_cod_imposto
                        );
                     else
                        if substr(v_prg_gerador, 1,5) <> 'inte_'
                        then
                           p_des_erro := inter_fn_buscar_tag('ds30441', 'pt_BR', 'INTERSYS');
                        end if;
                     end if;
                  end if;
               end if;
               num_lancto := num_lancto + 1;
            end loop;
         end if;
      end if;
   end if;
END "INTER_PR_GERA_LANC_CONT";

/

exec inter_pr_recompile;

