CREATE OR REPLACE FUNCTION inter_fn_calc_ficha_cardex_rec
   (p_inc_exc           in number,   p_empresa1           in number,
    p_empresa2          in number,   p_empresa3           in number,
    p_empresa4          in number,   p_empresa5           in number,
    p_nivel_prod        in varchar2, p_grupo_prod         in varchar2,
    p_nivel_prod1       in varchar2, p_grupo_prod1        in varchar2,
    p_nivel_prod2       in varchar2, p_grupo_prod2        in varchar2,
    p_nivel_prod3       in varchar2, p_grupo_prod3        in varchar2,
    p_conta_estoque     in number,   p_data_inicial_ficha in date,
    p_data_final_ficha   in date,    p_origem_custo_fabr  in number,
    p_situacao_depositos in number,  p_valorizacao        in number,
     p_subgrupo_prod     in varchar2, p_item_prod         in varchar2)
return varchar2
is
   erro_mensagem          varchar2(500) := ' ';
   v_periodo_estoque      date;
   v_forma_calculo_cardex empr_002.forma_calculo_cardex%type;
   v_estq_300_estq_310    varchar2(10);

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

begin

   -- Dados do usu?rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


   -- Verifica se esta executando o calculo para um periodo fechado ou aberto
   begin
      select periodo_estoque
      into   v_periodo_estoque
      from empr_001;
      exception
         when others then
            erro_mensagem := SQLERRM;
   end;

   if v_periodo_estoque > p_data_inicial_ficha
   then v_estq_300_estq_310 := 'estq_310';
   else v_estq_300_estq_310 := 'estq_300';
   end if;

   for v_empresa in (select fatu_500.codigo_empresa
     from fatu_500
     where (fatu_500.codigo_empresa in (p_empresa1, p_empresa2, p_empresa3, p_empresa4, p_empresa5)
       or p_empresa1 = 0))
   loop

     -- Verifica a forma de calculo da Cardex:
       -- 0 - Opcao Antiga, recalcula preco medio por movimento, e e produto / deposito
       -- 1 - Opcao Nova, define o preco medio mensal, e e somente por produto
       v_forma_calculo_cardex := INTER_FN_GET_PARAM_INT(v_empresa.codigo_empresa, 'estq.formaCalcCardex');

     if v_forma_calculo_cardex = 0
     then
      begin
       inter_pr_calcula_ficha_cardex(p_inc_exc,         v_empresa.codigo_empresa,
                       0,              0,
                       0,              0,
                       p_nivel_prod,      p_grupo_prod,
                       p_nivel_prod1,     p_grupo_prod1,
                       p_nivel_prod2,     p_grupo_prod2,
                       p_nivel_prod3,     p_grupo_prod3,
                       p_conta_estoque,   p_data_inicial_ficha,
                       p_data_final_ficha,p_origem_custo_fabr,
                       v_estq_300_estq_310);

       exception
        when others then
           erro_mensagem := SQLERRM;
      end;
     end if;

     if (v_forma_calculo_cardex = 1 or v_forma_calculo_cardex = 3)
     then
      begin
       inter_pr_calcula_ficha_cardex2(p_inc_exc,           v_empresa.codigo_empresa,
                      0,               0,
                      0,               0,
                      p_nivel_prod,        p_grupo_prod,
                      p_nivel_prod1,       p_grupo_prod1,
                      p_nivel_prod2,       p_grupo_prod2,
                      p_nivel_prod3,       p_grupo_prod3,
                      p_conta_estoque,     p_data_inicial_ficha,
                      p_data_final_ficha,  p_origem_custo_fabr,
                      p_valorizacao,       v_estq_300_estq_310,
                            p_subgrupo_prod,      p_item_prod);

       exception
        when others then
           erro_mensagem := SQLERRM;
      end;
     end if;
     
    if v_forma_calculo_cardex = 2
    then
      begin
       inter_pr_calcula_ficha_cardex3(p_inc_exc,           v_empresa.codigo_empresa,
                      p_nivel_prod,        p_grupo_prod,                                       
                      p_nivel_prod1,       p_grupo_prod1,
                      p_nivel_prod2,       p_grupo_prod2,
                      p_nivel_prod3,       p_grupo_prod3,
                      p_conta_estoque,     p_data_inicial_ficha,
                      p_data_final_ficha,  p_origem_custo_fabr,
                      p_valorizacao,       v_estq_300_estq_310,
                            p_subgrupo_prod,      p_item_prod);

       exception
        when others then
           erro_mensagem := SQLERRM;
      end;
     end if;
     
     if v_forma_calculo_cardex = 4
     then
      begin
       inter_pr_calcula_ficha_cardex4(p_inc_exc,        v_empresa.codigo_empresa,
                       0,              0,
                       0,              0,
                       p_nivel_prod,      p_grupo_prod,
                       p_nivel_prod1,     p_grupo_prod1,
                       p_nivel_prod2,     p_grupo_prod2,
                       p_nivel_prod3,     p_grupo_prod3,
                       p_conta_estoque,   p_data_inicial_ficha,
                       p_data_final_ficha,p_origem_custo_fabr,
                       v_estq_300_estq_310);

       exception
        when others then
           erro_mensagem := SQLERRM;
      end;
     end if;

     if p_situacao_depositos = 1
     then

      begin
       update basi_205
       set basi_205.situacao_deposito = 1
       where  (basi_205.local_deposito in (v_empresa.codigo_empresa,0,0,0,0) 
        or   v_empresa.codigo_empresa = 0
        or   basi_205.local_deposito in (select fatu_500.codigo_empresa
                         from fatu_500
                         where fatu_500.codigo_matriz  = v_empresa.codigo_empresa
                           and v_forma_calculo_cardex = 3))
         and   basi_205.controla_ficha_cardex = 1
         and   not exists (select 1 from estq_300
                 where estq_300.codigo_deposito   = basi_205.codigo_deposito
                   and estq_300.data_movimento    between trunc(v_periodo_estoque,'MM') and last_day(v_periodo_estoque)
                   and estq_300.sequencia_ficha   = 0
                   and lower(v_estq_300_estq_310) = 'estq_300'
                 UNION ALL
                 select 1 from estq_310
                 where estq_310.codigo_deposito   = basi_205.codigo_deposito
                   and estq_310.data_movimento    between trunc(v_periodo_estoque,'MM') and last_day(v_periodo_estoque)
                   and estq_310.sequencia_ficha   = 0
                   and lower(v_estq_300_estq_310) = 'estq_310')
         and ((p_inc_exc = 1 /*INCLUSAO*/
         and   exists (select 1 from rcnb_060
               where rcnb_060.tipo_registro   = 37
                 and rcnb_060.nr_solicitacao  = 077
                 and rcnb_060.empresa_rel     = ws_empresa
                 and rcnb_060.usuario_rel     = ws_usuario_systextil
                 and rcnb_060.grupo_estrutura = basi_205.codigo_deposito))
        or  (p_inc_exc = 2 /*EXCECAO*/
         and   not exists (select 1 from rcnb_060
                 where rcnb_060.tipo_registro   = 37
                   and rcnb_060.nr_solicitacao  = 077
                   and rcnb_060.empresa_rel     = ws_empresa
                   and rcnb_060.usuario_rel     = ws_usuario_systextil
                   and rcnb_060.grupo_estrutura = basi_205.codigo_deposito)));
       exception
        when others then
           erro_mensagem := SQLERRM;
      end;
     end if;
   end loop;

   return(erro_mensagem);
end inter_fn_calc_ficha_cardex_rec;


/

exec inter_pr_recompile;
/
