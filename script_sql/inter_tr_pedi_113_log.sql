
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_113_LOG" 
  before insert
  or delete
  or update of
	  pedido_venda, seq_item_pedido, seq_numero_nota, seq_item_nr_nota,
	  sequencia_113, nivel, grupo, subgrupo,
	  item, valor_unitario, quantidade, valor_unit_negociacao,
	  tipo_registro, nota_fiscal_entrada, serie_nf_entrada, fornecedor_9,
	  fornecedor_4, fornecedor_2, seq_nota_entrada, peso_liquido,
	  peso_bruto, qtde_embalagens, agrupador, marca_reatualizar,
	  numero_volume, marca_volume, um_faturamento_um, um_faturamento_qtde,
	  um_faturamento_valor_unit, lote, executa_trigger
  on pedi_113
  for each row
declare
  -- local variables here
    v_pedido_venda_old          		    pedi_113.pedido_venda%type;
    v_seq_item_pedido_old       		    pedi_113.seq_item_pedido%type;
    v_seq_numero_nota_old       		    pedi_113.seq_numero_nota%type;
    v_seq_item_nr_nota_old      		    pedi_113.seq_item_nr_nota%type;
    v_sequencia_113_old         		    pedi_113.sequencia_113%type;
    v_nivel_old                 		    pedi_113.nivel%type;
    v_grupo_old                 		    pedi_113.grupo%type;
    v_subgrupo_old              		    pedi_113.subgrupo%type;
    v_item_old                  		    pedi_113.item%type;
    v_quantidade_old            		    pedi_113.quantidade%type;
    v_qtde_embalagens_old       		    pedi_113.qtde_embalagens%type;
    v_pedido_venda_new           	      pedi_113.pedido_venda%type;
    v_seq_item_pedido_new       		    pedi_113.seq_item_pedido%type;
    v_seq_numero_nota_new       		    pedi_113.seq_numero_nota%type;
    v_seq_item_nr_nota_new      		    pedi_113.seq_item_nr_nota%type;
    v_sequencia_113_new         		    pedi_113.sequencia_113%type;
    v_nivel_new                 		    pedi_113.nivel%type;
    v_grupo_new                 		    pedi_113.grupo%type;
    v_subgrupo_new              		    pedi_113.subgrupo%type;
    v_item_new                  		    pedi_113.item%type;
    v_quantidade_new            		    pedi_113.quantidade%type;
    v_qtde_embalagens_new       		    pedi_113.qtde_embalagens%type;

    v_operacao              varchar(1);
    v_data_operacao         date;
    v_usuario_rede          varchar(20);
    v_maquina_rede          varchar(40);
    v_aplicativo            varchar(20);
    v_executa_trigger       number(1);
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      begin
      -- Grava a data/hora da insercao do registro (log)
      v_data_operacao := sysdate();

      -- Encontra dados do usuario da rede, maquina e aplicativo que esta atualizando a ficha
      select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
      into v_usuario_rede, v_maquina_rede, v_aplicativo
      from sys.gv_$session
      where audsid  = userenv('SESSIONID')
        and inst_id = userenv('INSTANCE')
        and rownum < 2;

      --alimenta as variaveis new caso seja insert ou update
      if inserting or updating
      then
         if inserting
         then v_operacao := 'I';
         else v_operacao := 'U';
         end if;


         v_pedido_venda_new        := :new.pedido_venda;
         v_seq_item_pedido_new     := :new.seq_item_pedido;
         v_seq_numero_nota_new     := :new.seq_numero_nota;
         v_seq_item_nr_nota_new    := :new.seq_item_nr_nota;
         v_sequencia_113_new       := :new.sequencia_113;
         v_nivel_new               := :new.nivel;
         v_grupo_new               := :new.grupo;
         v_subgrupo_new            := :new.subgrupo;
         v_item_new                := :new.item;
         v_quantidade_new          := :new.quantidade;
         v_qtde_embalagens_new     := :new.qtde_embalagens;

      end if; --fim do if inserting or updating

      --alimenta as variaveis old caso seja insert ou update
      if deleting or updating
      then
         if deleting
         then v_operacao := 'D';
         else v_operacao := 'U';
         end if;

         v_pedido_venda_old        := :old.pedido_venda;
         v_seq_item_pedido_old     := :old.seq_item_pedido;
         v_seq_numero_nota_old     := :old.seq_numero_nota;
         v_seq_item_nr_nota_old    := :old.seq_item_nr_nota;
         v_sequencia_113_old       := :old.sequencia_113;
         v_nivel_old               := :old.nivel;
         v_grupo_old               := :old.grupo;
         v_subgrupo_old            := :old.subgrupo;
         v_item_old                := :old.item;
         v_quantidade_old          := :old.quantidade;
         v_qtde_embalagens_old     := :old.qtde_embalagens;

      end if; --fim do if inserting or updating

      --insere na pedi_113_log o registro.

      insert into pedi_113_log (
         pedido_venda_old,        seq_item_pedido_old,
         seq_numero_nota_old,     seq_item_nr_nota_old,
         sequencia_113_old,       nivel_old,
         grupo_old,               subgrupo_old,
         item_old,                quantidade_old,
         qtde_embalagens_old,     pedido_venda_new,
         seq_item_pedido_new,     seq_numero_nota_new,
         seq_item_nr_nota_new,    sequencia_113_new,
         nivel_new,               grupo_new,
         subgrupo_new,            item_new,
         quantidade_new,          qtde_embalagens_new,
         operacao,                data_operacao,
         usuario_rede,            maquina_rede,
         aplicativo)
      values (
         v_pedido_venda_new       , v_seq_item_pedido_new,
         v_seq_numero_nota_new    , v_seq_item_nr_nota_new,
         v_sequencia_113_new      , v_nivel_new,
         v_grupo_new              , v_subgrupo_new,
         v_item_new               , v_quantidade_new,
         v_qtde_embalagens_new    , v_pedido_venda_old,
         v_seq_item_pedido_old    , v_seq_numero_nota_old,
         v_seq_item_nr_nota_old   , v_sequencia_113_old,
         v_nivel_old              , v_grupo_old,
         v_subgrupo_old           , v_item_old,
         v_quantidade_old         , v_qtde_embalagens_old,
         v_operacao               , v_data_operacao,
         v_usuario_rede           , v_maquina_rede,
         v_aplicativo);

      exception
      when others
         then raise_application_error (-20000, 'Nao atualizou a tabela de log da pedi_113.');

      end;
   end if;

end inter_tr_pedi_113_log;

-- ALTER TRIGGER "INTER_TR_PEDI_113_LOG" ENABLE
 

/

exec inter_pr_recompile;

