CREATE OR REPLACE TRIGGER "INTER_TR_BASI_030_UMCML"
BEFORE UPDATE OR INSERT ON basi_030 FOR EACH ROW

BEGIN
	if :new.unidade_medida_comercial is null then
	  :new.unidade_medida_comercial := :new.unidade_medida;
	end if;
END inter_tr_basi_030_umcml;

/

exec inter_pr_recompile;
