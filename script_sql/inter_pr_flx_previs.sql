
  CREATE OR REPLACE PROCEDURE "INTER_PR_FLX_PREVIS" (p_cod_usuario          IN NUMBER,
                                                p_nom_usuario          IN VARCHAR2,
                                                p_letra_registro       IN CHAR,
                                                p_des_erro             OUT varchar2) is

-- Finalidade: Calcular saldo de titulos previstos para o fluxo de caixa
-- Autor.....: Tiago H
-- Data......: 08/11/10

w_erro                EXCEPTION;
w_saldo_tit_previs    cpag_010.saldo_titulo%type;
w_valor_rateio        cpag_010.saldo_titulo%type;
w_cont                number(5);
w_saldo_titulo        cpag_010.saldo_titulo%type;

BEGIN

   FOR previs in (
      select oper_flx.int_04,
             oper_flx.dat_04
      from oper_flx
      where oper_flx.str_63          = p_letra_registro
         and oper_flx.int_01         = p_cod_usuario
         and oper_flx.nr_solicitacao = p_cod_usuario
         and oper_flx.nome_relatorio = 'cpag_f820'
         and oper_flx.str_61         = p_nom_usuario
         and oper_flx.int_02         = 0
      group by oper_flx.int_04,oper_flx.dat_04
    )

    LOOP

      BEGIN
         select sum(oper_flx.flo_02) into w_saldo_titulo
         from oper_flx
         where oper_flx.str_63         = p_letra_registro
           and oper_flx.int_01         = p_cod_usuario
           and oper_flx.nr_solicitacao = p_cod_usuario
           and oper_flx.nome_relatorio = 'cpag_f820'
           and oper_flx.str_61         = p_nom_usuario
           and oper_flx.int_02         = 0
           and oper_flx.dat_04         = previs.dat_04
           and oper_flx.int_04         = previs.int_04;
      EXCEPTION
         WHEN no_data_found then
           w_saldo_titulo := 0;
      END;

      if w_saldo_titulo is null
      then
         w_saldo_titulo := 0;
      end if;
      if w_saldo_titulo > 0
      then

        BEGIN
           select sum(oper_flx.flo_02) into w_saldo_tit_previs
           from oper_flx
           where oper_flx.str_63         = p_letra_registro
             and oper_flx.int_01         = p_cod_usuario
             and oper_flx.nr_solicitacao = p_cod_usuario
             and oper_flx.nome_relatorio = 'cpag_f820'
             and oper_flx.str_61         = p_nom_usuario
             and oper_flx.int_02         = 1
             and oper_flx.dat_04         = previs.dat_04
             and oper_flx.int_04         = previs.int_04;
        EXCEPTION
           WHEN no_data_found then
             w_saldo_tit_previs := 0;

          if w_saldo_tit_previs is null
          then
             w_saldo_tit_previs := 0;
          end if;
        END;

        if w_saldo_tit_previs > 0
        then

          BEGIN
            select count(*) into w_cont
             from oper_flx
             where oper_flx.str_63         = p_letra_registro
               and oper_flx.int_01         = p_cod_usuario
               and oper_flx.nr_solicitacao = p_cod_usuario
               and oper_flx.nome_relatorio = 'cpag_f820'
               and oper_flx.str_61         = p_nom_usuario
               and oper_flx.int_02         = 1
               and oper_flx.dat_04         = previs.dat_04
               and oper_flx.int_04         = previs.int_04;
          EXCEPTION
             WHEN no_data_found then
               w_cont := 1;
          END;

          if w_cont is null
          then
             w_cont := 1;
          end if;

          w_valor_rateio := w_saldo_tit_previs - w_saldo_titulo;

          if w_valor_rateio <= 0
          then
             w_valor_rateio := 0;
          else
             w_valor_rateio := w_valor_rateio / w_cont;
          end if;

          BEGIN
             update oper_flx
             set oper_flx.flo_02 = w_valor_rateio
             where oper_flx.str_63         = p_letra_registro
               and oper_flx.int_01         = p_cod_usuario
               and oper_flx.nr_solicitacao = p_cod_usuario
               and oper_flx.nome_relatorio = 'cpag_f820'
               and oper_flx.str_61         = p_nom_usuario
               and oper_flx.int_02         = 1
               and oper_flx.dat_04         = previs.dat_04
               and oper_flx.int_04         = previs.int_04;
          EXCEPTION
             WHEN OTHERS THEN
                p_des_erro := 'Erro na atualizacao do saldo dos titulos previstos' || Chr(10) || SQLERRM;
                RAISE W_ERRO;
          END;

          commit;
        end if;
      end if;
    END LOOP;

EXCEPTION
   WHEN w_erro THEN
      p_des_erro := 'Erro na procedure inter_pr_flx_previs ' || Chr(10) || p_des_erro;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure inter_pr_flx_previs ' || Chr(10) || SQLERRM;

END INTER_PR_FLX_PREVIS;
 

/

exec inter_pr_recompile;

