DECLARE
    CURSOR programa_cursor IS 
        SELECT DISTINCT * FROM hdoc_033
        WHERE hdoc_033.programa = 'cdst_fa01';
    
    v_usuario VARCHAR2(150);
BEGIN
    FOR Y IN (SELECT DISTINCT CODIGO FROM vi_carga_tabelas) LOOP
        FOR reg_programa IN programa_cursor LOOP
            BEGIN
                -- Verifica se existe um registro na tabela oper_550 com as condições especificadas
                SELECT DISTINCT oper_550.usuario 
                INTO v_usuario 
                FROM oper_550 
                WHERE oper_550.usuario           = reg_programa.usu_prg_cdusu
                AND oper_550.empresa             = reg_programa.usu_prg_empr_usu
                AND oper_550.nome_programa       = reg_programa.programa
                AND oper_550.nome_subprograma    = 'LISTA_TABELAS'
                AND oper_550.nome_field          = Y.CODIGO;
                
            EXCEPTION 
                WHEN NO_DATA_FOUND THEN

                -- Caso não encontre registro, insere dados na tabela oper_550
                    INSERT INTO oper_550 (
                        usuario,                     empresa, 
                        nome_programa,               nome_subprograma, 
                        nome_field,                  requerido, 
                        acessivel,                   inicia_campo
                    ) VALUES (
                        reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
                        reg_programa.programa,       'LISTA_TABELAS', 
                        Y.codigo,                    0, 
                        1,                           0
                    );
                COMMIT;

            END;
        END LOOP;
    END LOOP;
END;

/
