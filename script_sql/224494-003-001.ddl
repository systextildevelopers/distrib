CREATE TABLE FNDC_007
   (         NR_SOLICITACAO NUMBER(9,0) DEFAULT 0 NOT NULL ENABLE, 
             DATA_PROCESSO DATE , 
             EMPRESA number(3) NOT NULL ENABLE,
             CGC9 NUMBER(9,0) NOT NULL ENABLE, 
             CGC4 NUMBER(4,0) NOT NULL ENABLE, 
             CGC2 NUMBER(2,0) NOT NULL ENABLE, 
            TITULO number(9) NOT NULL ENABLE,
            PARCELA number(2) NOT NULL ENABLE,
            TIPO_TITULO number(2) NOT NULL ENABLE,
            DATA_PRORROGACAO date ,
            SALDO number(15,2) ,
            VALOR_JUROS number(15,2) ,
            FLAG number(1),
            NR_REMESSA number(9),
	 CONSTRAINT FNDC_007_PK PRIMARY KEY (NR_SOLICITACAO, EMPRESA, CGC9, CGC4, CGC2, TITULO, PARCELA, TIPO_TITULO)
    );
