CREATE OR REPLACE TRIGGER INTER_TR_PCPB_010 
   before update of periodo_producao, situacao_ordem,
                    cod_cancelamento, ordem_tingimento,
                    previsao_termino
   on pcpb_010
   for each row
declare
  t_existe_015 number;
  t_existe_040 number;
  t_existe_100 number;

  v_sub_ler     mqop_050.subgru_estrutura%type := '###';
  v_item_ler    mqop_050.item_estrutura%type := '######';
  v_nr_registro number;

  v_data_embarque date;

  v_dias_disp_final_ob number;
  v_dias_disp_final_ot number;

  v_qtde_quilos_prog number;

  v_alternativa_020 number;
  v_roteiro_020     number;

begin
  if updating and :new.tipo_ordem <> 4
  then
    -- Se esta alterando a ordem de tingimento altera na pcpb_201.
    if :old.ordem_tingimento <> :new.ordem_tingimento
    then
      update pcpb_201
         set pcpb_201.ordem_tingimento = :new.ordem_tingimento
       where pcpb_201.ordem_producao = :old.ordem_producao;
    end if;

    -- Se esta alterando a ordem de tingimento altera na pcpb_201.
    if :old.previsao_termino <> :new.previsao_termino
    then
       if :new.ordem_tingimento <> 0
       then
          t_existe_100 := 0;

          select count(*)
          into t_existe_100
          from pcpb_100
          where pcpb_100.ordem_agrupamento = :new.ordem_tingimento
            and pcpb_100.ordem_producao    = :new.ordem_producao
            and pcpb_100.tipo_ordem        = '1';

          if t_existe_100 > 0
          then
             update pcpb_100
             set pcpb_100.data_prev_termino = :new.previsao_termino
             where pcpb_100.ordem_agrupamento = :new.ordem_tingimento
               and pcpb_100.tipo_ordem        = '1';
          end if;
       end if;
    end if;

    -- Se esta cancelando a OB ira eliminar os registros da pcpb_201.
    if :old.cod_cancelamento = 0 and :new.cod_cancelamento > 0 then
      delete pcpb_201 where pcpb_201.ordem_producao = :old.ordem_producao;
    end if;

    -- Se esta cancelando a OB ira eliminar os registros de planejamento
    if  :old.cod_cancelamento = 0 
    and :new.cod_cancelamento > 0
    then
       delete from tmrp_041
       where  tmrp_041.AREA_PRODUCAO = 2 
       and    tmrp_041.NR_PEDIDO_ORDEM = :old.ordem_producao;
    end if;

    -- Se alterar o periodo de producao da OB.
    if :old.periodo_producao <> :new.periodo_producao and
       :old.tipo_ordem <> 4 then
      -- Altera periodo de producao do planejamento, se alterar o periodo de producao da OB
      update tmrp_041
         set tmrp_041.periodo_producao = :new.periodo_producao
       where tmrp_041.periodo_producao = :old.periodo_producao
         and tmrp_041.area_producao = 2
         and tmrp_041.nr_pedido_ordem = :old.ordem_producao;
    end if;

    -- Quando a situacao da Ordem passar de 0 (A emitir) para Emitida (> 0)
    -- reatualiza os estagios e operacoes.
    if :old.situacao_ordem = 0 and :new.situacao_ordem > 0 then

      -- Verifica se existe estagios e operacoes para a OB.
      t_existe_015 := 0;
      t_existe_040 := 0;

      select count(*)
        into t_existe_015
        from pcpb_015
       where pcpb_015.ordem_producao = :old.ordem_producao;

      select count(*)
        into t_existe_040
        from pcpb_040
       where pcpb_040.ordem_producao = :old.ordem_producao;

      -- Se nao existir estagios e operacoes executa atualizacao.
      if t_existe_015 = 0 and t_existe_040 = 0 then

        -- Inicio do loop para reatualizacao dos estagios e operacoes da OB.
        for reg_pcpb_030 in (select pcpb_030.pano_nivel99,
                                    pcpb_030.pano_grupo,
                                    pcpb_030.pano_subgrupo,
                                    pcpb_030.pano_item,
                                    pcpb_030.alternativa,
                                    pcpb_030.roteiro,
                                    pcpb_030.sequencia,
                                    pcpb_030.nr_pedido_ordem
                               from pcpb_030
                              where pcpb_030.ordem_producao =
                                    :old.ordem_producao) loop

          v_qtde_quilos_prog := 0.000;

          begin
            select pcpb_020.qtde_quilos_prog,
                   pcpb_020.alternativa_item,
                   pcpb_020.roteiro_opcional
              into v_qtde_quilos_prog, v_alternativa_020, v_roteiro_020
              from pcpb_020
             where pcpb_020.ordem_producao = :old.ordem_producao
               and pcpb_020.sequencia = reg_pcpb_030.sequencia
               and pcpb_020.pano_sbg_nivel99 = reg_pcpb_030.pano_nivel99
               and pcpb_020.pano_sbg_grupo = reg_pcpb_030.pano_grupo
               and pcpb_020.pano_sbg_subgrupo = reg_pcpb_030.pano_subgrupo
               and pcpb_020.pano_sbg_item = reg_pcpb_030.pano_item;
          exception
            when others then
              v_qtde_quilos_prog := 0.00;
              v_alternativa_020  := 0;
              v_roteiro_020      := 0;
          end;

          -- Sempre antes de chamar a procedure para atualizar estagios e operacoes
          -- a logica abaixo deve ser executada para definicao do produto.
          v_sub_ler     := reg_pcpb_030.pano_subgrupo;
          v_item_ler    := reg_pcpb_030.pano_item;
          v_nr_registro := 0;

          begin
            select count(*)
              into v_nr_registro
              from mqop_050
             where mqop_050.nivel_estrutura = reg_pcpb_030.pano_nivel99
               and mqop_050.grupo_estrutura = reg_pcpb_030.pano_grupo
               and mqop_050.subgru_estrutura = v_sub_ler
               and mqop_050.item_estrutura = v_item_ler
               and mqop_050.numero_alternati = v_alternativa_020
               and mqop_050.numero_roteiro = v_roteiro_020;
            if v_nr_registro = 0 then
              v_item_ler := '000000';
              begin
                select count(*)
                  into v_nr_registro
                  from mqop_050
                 where mqop_050.nivel_estrutura = reg_pcpb_030.pano_nivel99
                   and mqop_050.grupo_estrutura = reg_pcpb_030.pano_grupo
                   and mqop_050.subgru_estrutura = v_sub_ler
                   and mqop_050.item_estrutura = v_item_ler
                   and mqop_050.numero_alternati = v_alternativa_020
                   and mqop_050.numero_roteiro = v_roteiro_020;
                if v_nr_registro = 0 then
                  v_sub_ler  := '000';
                  v_item_ler := reg_pcpb_030.pano_item;
                  begin
                    select count(*)
                      into v_nr_registro
                      from mqop_050
                     where mqop_050.nivel_estrutura =
                           reg_pcpb_030.pano_nivel99
                       and mqop_050.grupo_estrutura =
                           reg_pcpb_030.pano_grupo
                       and mqop_050.subgru_estrutura = v_sub_ler
                       and mqop_050.item_estrutura = v_item_ler
                       and mqop_050.numero_alternati = v_alternativa_020
                       and mqop_050.numero_roteiro = v_roteiro_020;
                    if v_nr_registro = 0 then
                      v_item_ler := '000000';

                    end if;
                  end;
                end if;
              end;
            end if;
          end;

          inter_pr_ordem_beneficiamento(reg_pcpb_030.pano_nivel99,
                                        reg_pcpb_030.pano_grupo,
                                        v_sub_ler,
                                        v_item_ler,
                                        v_alternativa_020,
                                        v_roteiro_020,
                                        :old.ordem_producao,
                                        :old.situacao_ordem,
                                        reg_pcpb_030.sequencia,
                                        v_qtde_quilos_prog,
                                        :old.tipo_ordem,
                                        'I');

          begin
            -- Busca a data de embarque/entrega do pedido de venda destinado a OB
            -- para calcular com o centro de custo os dias para producao.
            v_data_embarque := null;

            begin
              select min(pedi_100.data_entr_venda)
                into v_data_embarque
                from pedi_100, pcpb_030
               where pedi_100.pedido_venda = pcpb_030.nr_pedido_ordem
                 and pcpb_030.ordem_producao = :old.ordem_producao
                 and pcpb_030.pedido_corte = 3;
            exception
              when others then
                v_data_embarque := null;
            end;

            -- Chama procedure para calcular a prioridade das operacoes.
            inter_pr_calcula_seq_ob(:old.ordem_producao,
                                    :old.ordem_tingimento,
                                    v_data_embarque,
                                    reg_pcpb_030.sequencia,
                                    'PCPB_010',
                                    reg_pcpb_030.pano_nivel99,
                                    reg_pcpb_030.pano_grupo,
                                    v_sub_ler,
                                    v_item_ler,
                                    v_alternativa_020,
                                    v_roteiro_020);
          end;
        end loop;

        -- Atualiza a prioridade de producao na capa da OB e da OT (Agrupamento).
        begin
          select min(pcpb_015.prioridade_producao)
            into v_dias_disp_final_ob
            from pcpb_015
           where pcpb_015.ordem_producao = :old.ordem_producao;
        end;

        if v_dias_disp_final_ob is null then
          v_dias_disp_final_ob := 0;
        end if;

        :new.ordem_preparacao := v_dias_disp_final_ob;

        if :old.ordem_tingimento > 0 then
          begin
            select min(pcpb_010.ordem_preparacao)
              into v_dias_disp_final_ot
              from pcpb_010
             where pcpb_010.ordem_tingimento = :old.ordem_tingimento;
          exception
            when others then
              v_dias_disp_final_ot := 0;
          end;

          update pcpb_100
             set prioridade_producao = v_dias_disp_final_ot
           where pcpb_100.ordem_agrupamento = :old.ordem_tingimento
             and pcpb_100.tipo_ordem = '1';

          update pcpb_110
             set prioridade_producao = v_dias_disp_final_ot
           where pcpb_110.ordem_agrupamento = :old.ordem_tingimento
             and pcpb_110.tipo_ordem = '1';
        end if;
      end if;
    end if;
  end if;
end inter_tr_pcpb_010;

-- ALTER TRIGGER "INTER_TR_PCPB_010" ENABLE
 

/

exec inter_pr_recompile;

