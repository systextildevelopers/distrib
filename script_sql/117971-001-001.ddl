alter table pedi_050 add pago_qq_bco number(1) default 0;

comment on column pedi_050.pago_qq_bco is 'Identifica se os boletos emitos pelo banco terão msg de pagavel em qualquer banco)';

exec inter_pr_recompile;
