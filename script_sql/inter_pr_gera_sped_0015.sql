
  CREATE OR REPLACE PROCEDURE "INTER_PR_GERA_SPED_0015" (p_cod_empresa  IN NUMBER,
                                              p_des_erro     OUT varchar2) is
--
-- Finalidade: Gerar a tabela p_gera_sped_0015 - Dados do substituto
-- Autor.....: Cesar Anton
-- Data......: 19/11/08
--
-- Historicos
--
-- Data    Autor    Observacoes
--
CURSOR u_fatu_012 (p_cod_empresa    NUMBER) IS
   SELECT *
   from   fatu_012
   where  fatu_012.codigo_empresa = p_cod_empresa;

w_erro                 EXCEPTION;

BEGIN
   p_des_erro          := NULL;
   FOR fatu_012 IN u_fatu_012 (p_cod_empresa) LOOP
       begin
          insert into sped_0015
            (cod_empresa,
             sig_estado,
             num_inscri_estadual
            ) values
            (p_cod_empresa,
             fatu_012.uf,
             fatu_012.ie_substituto);
       EXCEPTION
          WHEN OTHERS THEN
             p_des_erro := 'Erro na inclusao da tabela sped_0015 ' || Chr(10) || SQLERRM;
             RAISE W_ERRO;
       END;
   END LOOP;
   COMMIT;
EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_0015 ' || Chr(10) || p_des_erro;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure p_gera_sped_0015 ' || Chr(10) || SQLERRM;
END inter_pr_gera_sped_0015;
/
exec inter_pr_recompile;

