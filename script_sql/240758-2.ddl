CREATE TABLE  "WEBHOOK_SUBSCRIPTION_AUTH_CONEXAO_NFE" 
   (	"ID_SUBSCRIPTION" VARCHAR2(32) NOT NULL, 
	"ID_INTEGRACAO" VARCHAR2(1000) NOT NULL, 
	"AUTHENTICATION_URL" VARCHAR2(2000) NOT NULL, 
	"TOKEN" VARCHAR2(4000), 
	"EXPIRES" DATE
   );
