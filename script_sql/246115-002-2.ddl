declare 

v_count number(3);

begin

   v_count := 0;

   for titulo in (
       select * from fatu_070 
       where fatu_070.data_venc_duplic > '31-dec-2023'
   ) loop
      v_count := v_count + 1;
   
      update fatu_070
      set VALOR_DESCONTO_MOEDA_AUX = 0.00,
          VALOR_JUROS_MOEDA_AUX = 0.00,
          VALOR_SALDO_MOEDA_AUX = 0.00,
          VALOR_VAR_CAMBIAL = 0.00
      where fatu_070.codigo_empresa   = titulo.codigo_empresa
        and fatu_070.cli_dup_cgc_cli9 = titulo.cli_dup_cgc_cli9
        and fatu_070.cli_dup_cgc_cli4 = titulo.cli_dup_cgc_cli4
        and fatu_070.cli_dup_cgc_cli2 = titulo.cli_dup_cgc_cli2
        and fatu_070.num_duplicata    = titulo.num_duplicata
        and fatu_070.tipo_titulo      = titulo.tipo_titulo
        and fatu_070.seq_duplicatas   = titulo.seq_duplicatas;
     
      if v_count = 100
      then
         commit;
         v_count := 0;
      end if;
   
   end loop;
   
   commit;
   
end;
