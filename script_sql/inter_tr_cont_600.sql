create or replace trigger "INTER_TR_CONT_600"
   before insert or
          delete or
          update of cod_empresa, exercicio, conta_reduzida, debito_credito, data_lancto,
                    centro_custo, valor_lancto
   on cont_600
   for each row

declare
   v_tipo_orcamento       fatu_500.controla_orcam%type;
   v_cod_empresa          cont_500.cod_empresa%type;
   v_exercicio            cont_500.exercicio%type;
   v_situacao_orcm        cont_500.situacao_orcm%type;
   v_cod_plano_cta        cont_500.cod_plano_cta%type;
   v_tipo_orcamento_plano cont_535.tipo_orcamento%type;
   v_debito_credito       cont_535.debito_credito%type;
   v_valor_lancto         cont_600.valor_lancto%type;
   v_controle_orcm        cont_500.controle_orcamento%type;
   v_controla_origem      cont_050.lancamentos%type;
   v_origem               cont_050.origem%type; 

   v_mes                  number;
   erro_sem_conta         exception;
   v_data                 date;
   v_executa_trigger      number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger = 0
   then
      -- seta variaveis, conforme situacao: insert/uodate ou delete
      if inserting or updating
      then
         v_cod_empresa := :new.cod_empresa;
         v_exercicio   := :new.exercicio;
         v_origem      := :new.origem; 

         begin
            select sysdate into v_data from dual;
         end;
         :new.datainsercao := v_data;

      else
         v_cod_empresa := :old.cod_empresa;
         v_exercicio   := :old.exercicio;
         v_origem      := :old.origem; 
      end if;

      -- verifica se a empresa utiliza o modulo orcamentario
      select controla_orcam into v_tipo_orcamento from fatu_500
      where codigo_empresa = v_cod_empresa;

      if v_tipo_orcamento = 1
      then
         -- le o exercicio contabil para verificar a situacao e pegar o plano de contas
         
         select cont_500.situacao_orcm,   cont_500.cod_plano_cta,
                cont_500.controle_orcamento
         into   v_situacao_orcm, v_cod_plano_cta,
                v_controle_orcm
         from cont_500
		 where cont_500.cod_empresa = v_cod_empresa
		 and cont_500.exercicio = v_exercicio;
         
         begin
            select cont_050.lancamentos
            into   v_controla_origem
            from cont_050
            where cont_050.origem = v_origem;

            exception
            when others
            then v_controla_origem := 0;
         end; 

         -- controla orcamento por lancamentos contabeis
         if v_controle_orcm in (2,3) and v_controla_origem = 1
         then

            -- se a situacao nao poder aceitar lancamentos contabeis, gera erro
            if v_situacao_orcm not in (1,2,4)  -- utilizado, revisao, bloqueado pela revisao
            then
               Raise_application_error(-20100, 'Situacao do orcamento no exercicio nao permite manutencao.');
            end if;

            if inserting or updating
            then
               -- le a conta contabil, se nao encontrar gera erro
               begin
                  select tipo_orcamento,         debito_credito
                  into   v_tipo_orcamento_plano, v_debito_credito
                  from cont_535
                  where cod_plano_cta = v_cod_plano_cta
                  and   cod_reduzido  = :new.conta_reduzida;

                  exception
                     when no_data_found then
                        raise erro_sem_conta;
               end;

               -- se a conta controlar orcamento, entao atualiza saldo orcamentario
               if v_tipo_orcamento_plano in (1,2)
               then
                  -- encontra o mes do lancamento
                  begin
                    select cont_510.periodo into v_mes from cont_510         
                    where cont_510.cod_empresa = v_cod_empresa
                      and cont_510.exercicio   = v_exercicio
                      and :new.data_lancto between cont_510.per_inicial and cont_510.per_final;
                    exception
                     when no_data_found then
                        v_mes := to_number(to_char(:new.data_lancto,'MM'));
                  end;

                  -- verifica se o lancamento contabil e do mesmo tipo da conta contabil
                  -- (debito/credito). Se for, o lancamento deve ser somado, senao, o
                  -- lancamento deve ser subtraido
                  if v_debito_credito = :new.debito_credito
                  then
                     v_valor_lancto := :new.valor_lancto;
                  else
                     v_valor_lancto := :new.valor_lancto * (-1.0);
                  end if;

                  -- atualiza valor realizado
                  update orcm_010
                  set valor_realizado  = valor_realizado + v_valor_lancto
                  where cod_empresa    = :new.cod_empresa
                  and   exercicio      = :new.exercicio
                  and   mes            = v_mes
                  and   centro_custo   = :new.centro_custo
                  and   conta_reduzida = :new.conta_reduzida;

                  -- se nao encontrar, insere im registro no orcamento
                  if SQL%notfound
                  then
                     insert into orcm_010
                       (cod_empresa,         exercicio,
                        mes,                 centro_custo,
                        conta_reduzida,      valor_realizado)
                     values
                       (:new.cod_empresa,    :new.exercicio,
                        v_mes,               :new.centro_custo,
                        :new.conta_reduzida, v_valor_lancto);
                  end if;
               end if;
            end if;

            if deleting or updating
            then
               -- le a conta contabil, se nao encontrar gera erro
               begin
                  select tipo_orcamento,         debito_credito
                  into   v_tipo_orcamento_plano, v_debito_credito
                  from cont_535
                  where cod_plano_cta = v_cod_plano_cta
                  and   cod_reduzido  = :old.conta_reduzida;

                  exception
                     when no_data_found then
                        raise erro_sem_conta;
               end;

               -- se a conta controlar orcamento, entao atualiza saldo orcamentario
               if v_tipo_orcamento_plano in (1,2)
               then
                  -- encontra o mes do lancamento
                  begin
                    select cont_510.periodo into v_mes from cont_510         
                    where cont_510.cod_empresa = v_cod_empresa
                      and cont_510.exercicio   = v_exercicio
                      and :old.data_lancto between cont_510.per_inicial and cont_510.per_final;
                    exception
                     when no_data_found then
                        v_mes := to_number(to_char(:old.data_lancto,'MM'));
                  end;

                  -- verifica se o lancamento contabil e do mesmo tipo da conta contabil
                  -- (debito/credito). Se for, o lancamento deve ser somado, senao, o
                  -- lancamento deve ser subtraido
                  if v_debito_credito = :old.debito_credito
                  then
                     v_valor_lancto := :old.valor_lancto;
                  else
                     v_valor_lancto := :old.valor_lancto * (-1.0);
                  end if;

                  -- atualiza valor realizado
                  update orcm_010
                  set valor_realizado  = valor_realizado - v_valor_lancto
                  where cod_empresa    = :old.cod_empresa
                  and   exercicio      = :old.exercicio
                  and   mes            = v_mes
                  and   centro_custo   = :old.centro_custo
                  and   conta_reduzida = :old.conta_reduzida;

                  -- se nao encontrar, insere im registro no orcamento
                  if SQL%notfound
                  then
                     insert into orcm_010
                       (cod_empresa,         exercicio,
                        mes,                 centro_custo,
                        conta_reduzida,      valor_realizado)
                     values
                       (:old.cod_empresa,    :old.exercicio,
                        v_mes,               :old.centro_custo,
                        :old.conta_reduzida, v_valor_lancto);
                  end if;
               end if;
            end if;
         end if;
      end if;
   end if;

   exception
      when erro_sem_conta then
         raise_application_error(-20101,'Conta Contabil nao cadastrada no plano de contas.');

end inter_tr_cont_600;

-- ALTER TRIGGER "INTER_TR_CONT_600" ENABLE
 

/

exec inter_pr_recompile;

