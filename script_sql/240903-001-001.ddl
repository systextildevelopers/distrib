alter table pedi_100 add(
	cod_cargo_repre number(4,0),
	descr_cli_final varchar2(255)
);

COMMENT ON COLUMN pedi_100.cod_cargo_repre IS 'Campo no Systextil relacionado a CoordinatorId__c que é o codigo do cargo do representante como coordenador';
COMMENT ON COLUMN pedi_100.descr_cli_final IS 'Campo no Systextil relacionado a FinalCostumerId__c que e descricao do cliente final';

exec inter_pr_recompile;
