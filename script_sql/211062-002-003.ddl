alter table cpag_015
add (vlr_multa         number(15,2) default 0.00,
     vlr_juros         number(15,2) default 0.00,
     vlr_desp_cartorio number(15,2) default 0.00,
     vlr_outras_desp   number(15,2) default 0.00);

/
exec inter_pr_recompile;
