create or replace trigger PK_blocok_cfop_saida
    before insert on blocok_cfop_saida   
    for each row  
begin   
    select
        nvl(max(id),0) + 1
    into :NEW.ID
    from blocok_cfop_saida ;
end;
