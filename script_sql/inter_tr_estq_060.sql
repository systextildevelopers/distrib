create or replace trigger inter_tr_estq_060
   before insert
       or delete
       or update of peso_liquido, codigo_deposito, status_caixa on estq_060
   for each row

declare
   v_entrada_saida           estq_005.entrada_saida%type;
   v_transac_entrada         estq_005.transac_entrada%type;
   v_transacao_cancel        estq_005.codigo_transacao%type;

   v_peso_caixa              estq_060.peso_liquido%type;
   v_preco_custo             estq_060.valor_movto_cardex%type;
   v_tipo_volume             basi_205.tipo_volume%type;
   v_ender_caixa             basi_205.endereca_caixa%type;

   v_numero_documento        estq_300.numero_documento%type;
   v_serie_documento         estq_300.serie_documento%type;
   v_sequencia_documento     estq_300.sequencia_documento%type;
   v_cnpj_9                  estq_300.cnpj_9%type;
   v_cnpj_4                  estq_300.cnpj_4%type;
   v_cnpj_2                  estq_300.cnpj_2%type;
   v_tabela_origem_cardex    estq_300.tabela_origem%type := 'ESTQ_060';

   v_data_cancelamento       date;
   v_lote_estoque            basi_030.lote_estoque%type;
   v_ccusto_cardex           number(6);

   v_tipo_prod_deposito      basi_205.tipo_prod_deposito%type;
   v_tipo_prod_deposito_new  basi_205.tipo_prod_deposito%type;
   v_tipo_prod_deposito_old  basi_205.tipo_prod_deposito%type;
   v_periodo_estoque         empr_001.periodo_estoque%type;

begin
   /**************************
    Status:
    0 - Pesada/embalada;
    1 - Enderecada;
    2 - Coletada;
    3 - Coleta confirmada;
    4 - Faturada;
    5 - Em maturacao;
    6 - Prevista;
    9 - Eliminada.
   **************************/
   if inserting and :new.status_caixa not in (8, 6)
   then
      begin
         -- le se o deposito controla por volume
         select basi_205.endereca_caixa
         into   v_ender_caixa
         from basi_205
         where basi_205.codigo_deposito = :new.codigo_deposito;

         if v_ender_caixa = 1
         then
           :new.status_caixa := 1;
         else
           :new.status_caixa := 0;
         end if;
      end;
   end if;

   if inserting or updating
   then
      -- se nao for informada a tabela origem, assume que a tabela e a propria ESTQ_060
      if :new.tabela_origem_cardex is null or :new.tabela_origem_cardex = ' '
      then
         :new.tabela_origem_cardex := 'ESTQ_060';
      end if;
   end if;

   v_ccusto_cardex := 0;

   if inserting or updating
   then
      -- carrega o centro de custo
      if :new.centro_custo_cardex is null
      then
         v_ccusto_cardex := 0;
      else
         v_ccusto_cardex := :new.centro_custo_cardex;
      end if;
   end if;

   if (:new.tabela_origem_cardex not in ('FATU_060', 'OBRF_015') or deleting)
   then

      if inserting or updating
      then
         -- verifica se e necessario fazer alguma conferencia
         if  (updating
         and (:old.status_caixa not in (4,8,9)   and :new.status_caixa     in (4,8,9)) -- Caixa estava em estoque e nao esta mais
         or  (:old.status_caixa     in (4,6,8,9) and :new.status_caixa not in (4,8,9)) -- Caixa nao estava em estoque e agora esta
         or ((:old.codigo_deposito  <> :new.codigo_deposito                            -- deposito alterado
         or   :old.peso_liquido     <> :new.peso_liquido)                              -- peso da caixa alterado
         and  :old.status_caixa not in (4,8,9)))                                       -- Caixa estava em estoque

         or (inserting and :new.status_caixa not in (6)) -- nao considera caixas previstas
         then

            if (:new.transacao_cardex = 0) and (:new.status_caixa not in (8,6) or updating)
            then
               raise_application_error(-20000,'Deve-se informar a transacao de movimentacao.');
            else
               select entrada_saida, transac_entrada into v_entrada_saida, v_transac_entrada
               from estq_005
               where codigo_transacao = :new.transacao_cardex;

               if updating
               and :old.codigo_deposito <> :new.codigo_deposito
               and :old.status_caixa not in (4,6,8,9)   -- Se caixa nao estava em estoque deixa usar uma transacao que nao seja transferencia e ainda alterar o deposito
               and  v_entrada_saida     <> 'T'
               then
                  raise_application_error(-20000,'Deposito alterado, deve-se usar uma transacao de transferencia.');
               end if;
            end if;

            if :new.data_cardex is null and (:new.status_caixa <> 8 or updating)
            then
               raise_application_error(-20000,'Deve-se informar a data de movimentacao.');
            end if;
         end if;
      end if;


      if inserting and  :new.status_caixa not in (6)  -- nao considera caixas previstas
      then
         begin
            -- le se o deposito controla por volume
            select basi_205.tipo_volume
            into   v_tipo_volume
            from basi_205
            where basi_205.codigo_deposito = :new.codigo_deposito;
         end;

         if :new.status_caixa not in (4,8,9) or v_tipo_volume not in (7)
         then
            -- buscar um campo lote_estoque da referencia para saber se precisamos atualizar a ordem de producao da
            -- caixa com o lote, isso quando ela nao e atualizada pelo programa
            select lote_estoque
            into    v_lote_estoque
            from basi_030
            where basi_030.nivel_estrutura = :new.prodcai_nivel99
              and basi_030.referencia      = :new.prodcai_grupo;

            if  :new.ordem_producao = 0
            and :new.lote > 0
            and v_lote_estoque = 1  --  lote estoque e a ordem de producao
            and (:new.nome_prog = 'estq_f019' or :new.nome_prog = 'pcpb_f735' or :new.nome_prog = 'estq_e605')
            then
               :new.ordem_producao := :new.lote;
            end if;

            if v_entrada_saida <> 'E'
            then
               raise_application_error(-20000,'Na insercao de uma caixa nova, a transacao deve ser de entrada.');
            end if;

            -- se o deposito for por quilo, entao a caixa entra como faturada
            if v_tipo_volume not in (7)
            then
               :new.status_caixa := 4;
            end if;

            -- verifica qual o documento e que deve ser enviado para a estq_300,
            -- se e a nota fiscal ou se e o numero da caixa
            if  :new.nota_fisc_ent is not null and :new.nota_fisc_ent  > 0
            and :new.seri_fisc_ent is not null and :new.seri_fisc_ent <> ' '
            and :new.sequ_fisc_ent is not null and :new.sequ_fisc_ent  > 0
            then
               v_numero_documento    := :new.nota_fisc_ent;
               v_serie_documento     := :new.seri_fisc_ent;
               v_sequencia_documento := :new.sequ_fisc_ent;
               v_cnpj_9              := :new.cnpj_fornecedor9;
               v_cnpj_4              := :new.cnpj_fornecedor4;
               v_cnpj_2              := :new.cnpj_fornecedor2;
               v_tabela_origem_cardex:= 'OBRF_015';
               v_preco_custo         := :new.peso_liquido * :new.valor_movto_cardex;

               -- Ticket 225065 - Altera tabela origem para n�o gerar problemas de saldo de estoque ao deletar nota(OBRF_010)
               if :new.nome_prog = 'estq_f900'
               then
                  :new.tabela_origem_cardex := 'ESTQ_060';
               end if;
            else
               v_numero_documento    := :new.numero_caixa;
               v_serie_documento     := ' ';
               v_sequencia_documento := 0;
               v_cnpj_9              := 0;
               v_cnpj_4              := 0;
               v_cnpj_2              := 0;
               v_preco_custo         := :new.valor_movto_cardex;
            end if;

            -- atualiza a data de insercao com a data corrente (sysdate)
            :new.data_insercao := trunc(sysdate,'dd');

            --Atualiza os campos deposito_producao e transacao_producao
            :new.deposito_producao  := :new.codigo_deposito;
            :new.transacao_producao := :new.transacao_ent;

            inter_pr_insere_estq_300 (:new.codigo_deposito,  :new.prodcai_nivel99,    :new.prodcai_grupo,
                                      :new.prodcai_subgrupo, :new.prodcai_item,       :new.data_cardex,
                                      :new.lote,             v_numero_documento,      v_serie_documento,               v_cnpj_9,                  v_cnpj_4,                 v_cnpj_2,
                                      v_sequencia_documento,
                                      :new.transacao_cardex, 'E',                     v_ccusto_cardex,
                                      :new.peso_liquido,     v_preco_custo,           :new.valor_contabil_cardex,
                                      :new.usuario_cardex,   v_tabela_origem_cardex,  :new.nome_prog,
                                      0.00);
         end if;
      end if; -- if inserting


      if deleting
      then
         if :old.status_caixa not in (4,6,8,9)
         then
            -- encontra a transacao de cancelamento de producao
            select tran_est_produca, periodo_estoque into v_transacao_cancel, v_periodo_estoque from empr_001;
            
            if :old.data_entrada < v_periodo_estoque
            then
                raise_application_error(-20000,'Caixa com data de entrada menor que a data do período de estoque. Esta caixa deve ser cancelada, não pode ser excluída. Caixa: ' || 
                :old.numero_caixa ||  ' - Data entrada Caixa: '  ||  :old.data_entrada || ' - Data período estoque: ' || v_periodo_estoque);
            end if;

            inter_pr_insere_estq_300 (:old.codigo_deposito,  :old.prodcai_nivel99,    :old.prodcai_grupo,
                                      :old.prodcai_subgrupo, :old.prodcai_item,       :old.data_entrada,
                                      :old.lote,             :old.numero_caixa,       ' ',
                                      0,                     0,                       0,
                                      0,
                                      v_transacao_cancel,    'S',                     v_ccusto_cardex,
                                      :old.peso_liquido,     :old.valor_movto_cardex, :old.valor_contabil_cardex,
                                      'DELECAO',             v_tabela_origem_cardex,  ' ',
                                      0.00);
         end if;
      end if;     -- if deleting


      if updating
      then
         if :old.status_caixa <> 6 -- Caixa Prevista
         then
            :new.transacao_ent := :old.transacao_ent;
         end if;

         if :new.peso_liquido > :old.peso_liquido and :old.status_caixa not in (6)
         then
            raise_application_error(-20000,'Nao se pode aumentar o peso de caixa.');
         end if;

         -- movimento de saida
         if   :old.status_caixa not in (4,6,8,9)              -- caixa estava em estoque
         and (:old.codigo_deposito  <> :new.codigo_deposito   -- deposito alterado
         or   :old.peso_liquido     <> :new.peso_liquido      -- peso da caixa alterado
         or   :new.status_caixa     in (4,8,9))               -- caixa tirada do estoque
         then
            if v_entrada_saida <> 'S' and v_entrada_saida <> 'T'
            then
               raise_application_error(-20000,'Nao se pode realizar uma movimentacao de saida com uma transacao de entrada.');
            end if;

            -- se foi o volume continua em estoque no mesmo deposito,
            -- retira do estoque apenas a diferenca entre os pesos, senao
            -- retira do estoque o peso total do volume
            if  :old.codigo_deposito   = :new.codigo_deposito -- mesmo deposito
            and :new.status_caixa not in (4,8,9)              -- caixa continua no estoque
            then
               v_peso_caixa := :old.peso_liquido - :new.peso_liquido;
            else
               v_peso_caixa := :new.peso_liquido;
            end if;

            -- verifica qual o documento e que deve ser enviado para a estq_300,
            -- se e a nota fiscal ou se e o numero da caixa
            if  :new.nota_fiscal     is not null and :new.nota_fiscal      > 0
            and :new.serie_nota      is not null and :new.serie_nota      <> ' '
            and :new.seq_nota_fiscal is not null and :new.seq_nota_fiscal  > 0
            then
               v_numero_documento    := :new.nota_fiscal;
               v_serie_documento     := :new.serie_nota;
               v_sequencia_documento := :new.seq_nota_fiscal;
               v_cnpj_9              := 0;
               v_cnpj_4              := 0;
               v_cnpj_2              := 0;
            else
               v_numero_documento    := :new.numero_caixa;
               v_serie_documento     := ' ';
               v_sequencia_documento := 0;
               v_cnpj_9              := 0;
               v_cnpj_4              := 0;
               v_cnpj_2              := 0;
            end if;

            if :new.nome_prog = 'estq_f183'
            then
               v_numero_documento    := :new.nota_fisc_ent;
               v_serie_documento     := :new.seri_fisc_ent;
               v_sequencia_documento := :new.sequ_fisc_ent;
               v_cnpj_9              := :new.cnpj_fornecedor9;
               v_cnpj_4              := :new.cnpj_fornecedor4;
               v_cnpj_2              := :new.cnpj_fornecedor2;
            end if;

            inter_pr_insere_estq_300 (:old.codigo_deposito,  :old.prodcai_nivel99,    :old.prodcai_grupo,
                                      :old.prodcai_subgrupo, :old.prodcai_item,       :new.data_cardex,
                                      :old.lote,             v_numero_documento,      v_serie_documento,
                                      v_cnpj_9,              v_cnpj_4,                v_cnpj_2,
                                      v_sequencia_documento,
                                      :new.transacao_cardex, 'S',                     v_ccusto_cardex,
                                      v_peso_caixa,          :new.valor_movto_cardex, :new.valor_contabil_cardex,
                                      :new.usuario_cardex,   v_tabela_origem_cardex,  :new.nome_prog,
                                      0.00);
         end if;


         -- movimento de entrada
         if   :new.status_caixa not in (4,8,9)                -- caixa esta em estoque
         and (:old.codigo_deposito  <> :new.codigo_deposito   -- deposito alterado
         or   :old.status_caixa     in (4,6,8,9))             -- caixa nao estava em estoque
         then

            -- se for entrada, atualiza a data_entrada com a data do movimento
            :new.data_entrada := :new.data_cardex;

            if v_entrada_saida <> 'T'
            then
               if v_entrada_saida <> 'E'
               then
                  raise_application_error(-20000,'Nao se pode realizar uma movimentacao de entrada com uma transacao de saida.');
               end if;

               v_transac_entrada := :new.transacao_cardex;
            end if;

            if  :new.nota_fisc_ent is not null and :new.nota_fisc_ent > 0
            and :new.seri_fisc_ent is not null and :new.seri_fisc_ent <> ' '
            and :new.sequ_fisc_ent is not null and :new.sequ_fisc_ent  > 0
            then
               v_numero_documento    := :new.nota_fisc_ent;
               v_serie_documento     := :new.seri_fisc_ent;
               v_sequencia_documento := :new.sequ_fisc_ent;
               v_cnpj_9              := :new.cnpj_fornecedor9;
               v_cnpj_4              := :new.cnpj_fornecedor4;
               v_cnpj_2              := :new.cnpj_fornecedor2;
               v_tabela_origem_cardex:= 'OBRF_015';
               v_preco_custo         := :new.peso_liquido * :new.valor_movto_cardex;
            else
               v_numero_documento    := :new.numero_caixa;
               v_serie_documento     := ' ';
               v_sequencia_documento := 0;
               v_cnpj_9              := 0;
               v_cnpj_4              := 0;
               v_cnpj_2              := 0;
               v_preco_custo         := :new.valor_movto_cardex;
            end if;

            inter_pr_insere_estq_300 (:new.codigo_deposito,  :new.prodcai_nivel99,    :new.prodcai_grupo,
                                      :new.prodcai_subgrupo, :new.prodcai_item,       :new.data_cardex,
                                      :new.lote,             v_numero_documento,      v_serie_documento,
                                      v_cnpj_9,              v_cnpj_4,                v_cnpj_2,
                                      v_sequencia_documento,
                                      v_transac_entrada,     'E',                     v_ccusto_cardex,
                                      :new.peso_liquido,     v_preco_custo,           :new.valor_contabil_cardex,
                                      :new.usuario_cardex,   v_tabela_origem_cardex,  :new.nome_prog,
                                      0.00);
          end if;
      end if;  -- if updating
   end if;     -- if (:new.tabela_origem_cardex not in ('FATU_060', 'OBRF_015') or deleting)

   -- INICIO - Atualizacao da quantidade produzida na ordem de planejamento.
   if inserting or updating or deleting
   then
      if inserting and :new.ordem_producao <> 0
      then
         begin
            -- Le o tipo do deposito, se e de primeira ou segunda qualidade.
            select  b.tipo_prod_deposito
            into    v_tipo_prod_deposito
            from basi_205 b
            where b.codigo_deposito = :new.codigo_deposito;
            exception
            when others then
               v_tipo_prod_deposito := 0;
         end;

         -- Atualiza a quantidade produzida de primeira qualidade.
         if v_tipo_prod_deposito = 1
         then
            inter_pr_atu_prod_ordem_planej(2,
                                           :new.ordem_producao,
                                           0,
                                           :new.prodcai_nivel99,
                                           :new.prodcai_grupo,
                                           :new.prodcai_subgrupo,
                                           :new.prodcai_item,
                                           0,
                                           0,
                                           0,
                                           :new.peso_liquido,
                                           'P');
         end if;

         -- Atualiza a quantidade produzida de segunda qualidade.
         if v_tipo_prod_deposito = 2
         then
            inter_pr_atu_prod_ordem_planej(2,
                                           :new.ordem_producao,
                                           0,
                                           :new.prodcai_nivel99,
                                           :new.prodcai_grupo,
                                           :new.prodcai_subgrupo,
                                           :new.prodcai_item,
                                           0,
                                           :new.peso_liquido,
                                           0,
                                           0,
                                           'P');
         end if;
      end if;

      if updating and :new.ordem_producao <> 0
      then
         -- Atualizando o estorno quando a caixa for cancelada.
         if :old.status_caixa <> 9 and :new.status_caixa = 9
         then
            begin
              -- Le o tipo do deposito, se e de primeira ou segunda qualidade.
               select  b.tipo_prod_deposito
               into    v_tipo_prod_deposito
               from basi_205 b
               where b.codigo_deposito = :old.codigo_deposito;
               exception
               when others then
                  v_tipo_prod_deposito := 0;
            end;

            -- Atualiza a quantidade produzida de primeira qualidade.
            if v_tipo_prod_deposito = 1
            then
               inter_pr_atu_prod_ordem_planej(2,
                                              :old.ordem_producao,
                                              0,
                                              :old.prodcai_nivel99,
                                              :old.prodcai_grupo,
                                              :old.prodcai_subgrupo,
                                              :old.prodcai_item,
                                              0,
                                              0,
                                              0,
                                              :old.peso_liquido,
                                              'E');
            end if;

            -- Atualiza a quantidade produzida de segunda qualidade.
            if v_tipo_prod_deposito = 2
            then
               inter_pr_atu_prod_ordem_planej(2,
                                              :old.ordem_producao,
                                              0,
                                              :old.prodcai_nivel99,
                                              :old.prodcai_grupo,
                                              :old.prodcai_subgrupo,
                                              :old.prodcai_item,
                                              0,
                                              :old.peso_liquido,
                                              0,
                                              0,
                                              'E');
            end if;
         end if;

         -- Atualizando o estorno quando for alterado o peso da caixa.
         if :old.peso_liquido <> :new.peso_liquido
         then
            begin
              -- Le o tipo do deposito, se e de primeira ou segunda qualidade.
               select  b.tipo_prod_deposito
               into    v_tipo_prod_deposito
               from basi_205 b
               where b.codigo_deposito = :old.codigo_deposito;
               exception
               when others then
                  v_tipo_prod_deposito := 0;
            end;

            -- Atualiza a quantidade produzida de primeira qualidade.
            if v_tipo_prod_deposito = 1
            then
               inter_pr_atu_prod_ordem_planej(2,
                                              :old.ordem_producao,
                                              0,
                                              :old.prodcai_nivel99,
                                              :old.prodcai_grupo,
                                              :old.prodcai_subgrupo,
                                              :old.prodcai_item,
                                              0,
                                              0,
                                              0,
                                              (:old.peso_liquido - :new.peso_liquido),
                                              'E');
            end if;

            -- Atualiza a quantidade produzida de segunda qualidade.
            if v_tipo_prod_deposito = 2
            then
               inter_pr_atu_prod_ordem_planej(2,
                                              :old.ordem_producao,
                                              0,
                                              :old.prodcai_nivel99,
                                              :old.prodcai_grupo,
                                              :old.prodcai_subgrupo,
                                              :old.prodcai_item,
                                              0,
                                              (:old.peso_liquido - :new.peso_liquido),
                                              0,
                                              0,
                                              'E');
            end if;
         end if;

         -- Atualizando a quantidade quando houver troca de qualidade, no caso da alteracao do deposito.
         if :old.codigo_deposito <> :new.codigo_deposito
         then
            begin
               -- Le o tipo do novo deposito, se e de primeira ou segunda qualidade.
               select  b.tipo_prod_deposito
               into    v_tipo_prod_deposito_new
               from basi_205 b
               where b.codigo_deposito = :new.codigo_deposito;
               exception
               when others then
                  v_tipo_prod_deposito_new := 0;
            end;

            begin
               -- Le o tipo do antigo deposito, se e de primeira ou segunda qualidade.
               select  b.tipo_prod_deposito
               into    v_tipo_prod_deposito_old
               from basi_205 b
               where b.codigo_deposito = :old.codigo_deposito;
               exception
               when others then
                  v_tipo_prod_deposito_old := 0;
            end;

            -- Quando houver a troca de deposito, reatualiza a quantidade conforme a qualidade do novo deposito.
            if v_tipo_prod_deposito_old = 1 and v_tipo_prod_deposito_new = 2
            then
               inter_pr_atu_prod_ordem_planej(2,
                                              :new.ordem_producao,
                                              0,
                                              :new.prodcai_nivel99,
                                              :new.prodcai_grupo,
                                              :new.prodcai_subgrupo,
                                              :new.prodcai_item,
                                              0,
                                              0,
                                              0,
                                              :new.peso_liquido,
                                              'E');

               inter_pr_atu_prod_ordem_planej(2,
                                              :new.ordem_producao,
                                              0,
                                              :new.prodcai_nivel99,
                                              :new.prodcai_grupo,
                                              :new.prodcai_subgrupo,
                                              :new.prodcai_item,
                                              0,
                                              :new.peso_liquido,
                                              0,
                                              0,
                                              'P');
            end if;

            -- Quando houver a troca de deposito, reatualiza a quantidade conforme a qualidade do novo deposito.
            if v_tipo_prod_deposito_old = 2 and v_tipo_prod_deposito_new = 1
            then
               inter_pr_atu_prod_ordem_planej(2,
                                              :new.ordem_producao,
                                              0,
                                              :new.prodcai_nivel99,
                                              :new.prodcai_grupo,
                                              :new.prodcai_subgrupo,
                                              :new.prodcai_item,
                                              0,
                                              :new.peso_liquido,
                                              0,
                                              0,
                                              'E');

               inter_pr_atu_prod_ordem_planej(2,
                                              :new.ordem_producao,
                                              0,
                                              :new.prodcai_nivel99,
                                              :new.prodcai_grupo,
                                              :new.prodcai_subgrupo,
                                              :new.prodcai_item,
                                              0,
                                              0,
                                              0,
                                              :new.peso_liquido,
                                              'P');
            end if;
         end if;
      end if;

      if deleting and :old.ordem_producao <> 0
      then
         begin
            -- Le o tipo do deposito, se e de primeira ou segunda qualidade.
            select  b.tipo_prod_deposito
            into    v_tipo_prod_deposito
            from basi_205 b
            where b.codigo_deposito = :old.codigo_deposito;
            exception
            when others then
               v_tipo_prod_deposito := 0;
         end;

         -- Atualiza a quantidade produzida de primeira qualidade.
         if v_tipo_prod_deposito = 1
         then
            inter_pr_atu_prod_ordem_planej(2,
                                           :old.ordem_producao,
                                           0,
                                           :old.prodcai_nivel99,
                                           :old.prodcai_grupo,
                                           :old.prodcai_subgrupo,
                                           :old.prodcai_item,
                                           0,
                                           0,
                                           0,
                                           :old.peso_liquido,
                                           'E');
         end if;

         -- Atualiza a quantidade produzida de segunda qualidade.
         if v_tipo_prod_deposito = 2
         then
            inter_pr_atu_prod_ordem_planej(2,
                                           :old.ordem_producao,
                                           0,
                                           :old.prodcai_nivel99,
                                           :old.prodcai_grupo,
                                           :old.prodcai_subgrupo,
                                           :old.prodcai_item,
                                           0,
                                           :old.peso_liquido,
                                           0,
                                           0,
                                           'E');
         end if;
      end if;
   end if;
   -- FIM - Atualizacao da quantidade produzida na ordem de planejamento.

   -- zera campos de controle da ficha cardex, assim nao ha perigo da trigger atualizar
   -- o estoque com dados antigos, caso o usuario nao passar estes parametros
   if inserting or updating
   then
      :new.transacao_cardex     := 0;
      :new.centro_custo_cardex  := 0;
      :new.usuario_cardex       := ' ';
      :new.tabela_origem_cardex := ' ';
      :new.nome_prog            := ' ';
      :new.data_cardex          := null;
   end if;
end;

/

exec inter_pr_recompile;
