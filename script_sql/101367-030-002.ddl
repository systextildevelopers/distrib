ALTER TABLE TMRP_041
ADD SOLICITACAO_ALMOXARIFADO NUMBER(6);

ALTER TABLE TMRP_041
ADD SEQUENCIA_ALMOXARIFADO NUMBER(4);

ALTER TABLE TMRP_041
MODIFY SOLICITACAO_ALMOXARIFADO DEFAULT 0;

ALTER TABLE TMRP_041
MODIFY SEQUENCIA_ALMOXARIFADO DEFAULT 0;

declare
nro_registro number;

begin
  nro_registro := 0;

  for reg in (select rowid
              from tmrp_041
              where sequencia_almoxarifado is null
                and solicitacao_almoxarifado is null)
  loop
    update tmrp_041
      set sequencia_almoxarifado = 0,
          solicitacao_almoxarifado = 0
    where rowid = reg.rowid;

    nro_registro := nro_registro + 1;

    if nro_registro > 1000
    then
       nro_registro := 0;
       commit;
    end if;
  end loop;

  commit;

end;

/

ALTER TABLE tmrp_041
DROP CONSTRAINT PK_TMRP_041;

CREATE INDEX PK_TMRP_041
  ON TMRP_041(PERIODO_PRODUCAO);

DROP INDEX PK_TMRP_041;

ALTER TABLE TMRP_041
   ADD CONSTRAINT PK_TMRP_041
   PRIMARY KEY (PERIODO_PRODUCAO, AREA_PRODUCAO, NR_PEDIDO_ORDEM, SEQ_PEDIDO_ORDEM, CODIGO_ESTAGIO, NIVEL_ESTRUTURA, GRUPO_ESTRUTURA, SUBGRU_ESTRUTURA, ITEM_ESTRUTURA, SEQUENCIA_ESTRUTURA, SOLICITACAO_ALMOXARIFADO, SEQUENCIA_ALMOXARIFADO);

exec inter_pr_recompile;
