DECLARE tmpInt NUMBER;

BEGIN 
  SELECT NVL(COUNT(*),0) INTO tmpInt
  FROM hdoc_036
  WHERE hdoc_036.codigo_programa = 'estq_e680'
  AND   hdoc_036.locale = 'es_ES';

  IF (tmpInt = 0) 
  THEN 
    BEGIN 
      INSERT INTO hdoc_036
      (
        codigo_programa,
        locale,
        descricao
      ) VALUES (
        'estq_e680',
        'es_ES',
        'Emisi�n de C�digo de Barras de Pr�-Relaciones'
      );
    END;
  END IF;

  SELECT NVL(COUNT(*),0) INTO tmpInt
  FROM hdoc_036
  WHERE hdoc_036.codigo_programa = 'estq_e680'
  AND   hdoc_036.locale = 'pt_BR';

  IF (tmpInt = 0) 
  THEN 
    BEGIN 
      INSERT INTO hdoc_036
      (
        codigo_programa,
        locale,
        descricao
      ) VALUES (
        'estq_e680',
        'pt_BR',
        'Emiss�o de C�digo de Barras de Pr�-Romaneios'
      );
    END;
  END IF;
END;
/

commit work;

EXEC inter_pr_recompile;
