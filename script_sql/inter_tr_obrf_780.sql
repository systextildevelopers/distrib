
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_780" 
  before insert on obrf_780
  for each row

declare
  v_id_registro number;
begin
   select seq_obrf_780.nextval into v_id_registro from dual;
   :new.numero_solicitacao := v_id_registro;

end inter_tr_obrf_780;
-- ALTER TRIGGER "INTER_TR_OBRF_780" ENABLE
 

/

exec inter_pr_recompile;

