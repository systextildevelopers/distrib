create or replace procedure inter_pr_calcula_valor_venda (
  p_nivel_produto      in varchar2, p_grupo_produto    in varchar2,
  p_subgrupo_produto   in varchar2, p_item_produto     in varchar2,
  p_empresa            in number,   p_mes              in number,
  p_ano                in number,   p_casas_decimais   in number,
  p_usuario            in varchar2, p_empresa_mdi      in number,
  p_per_max_saida      in number,   p_busca_valor_venda in number,
  v_valor_venda      out number
) is
  v_total_valor      number;
  v_total_qtde       number;
  v_val_tabela_preco pedi_095.val_tabela_preco%type;
  v_tab_col_tab      rcnb_060.nivel_estrutura%type;
  v_tab_mes_tab      rcnb_060.grupo_estrutura%type;
  v_tab_seq_tab      rcnb_060.item_estrutura%type;
  v_tipo_produto     basi_020.tipo_produto%type;
  v_tipo_produto_020 basi_020.tipo_produto%type;
  v_tipo_produto_030 basi_030.tipo_produto%type;
  v_serie_cor        basi_100.serie_cor%type;
  v_contador         number;
  v_data_notas_saida date;
  v_mes              number;
  v_ano              number;
begin

  v_valor_venda := 0.0000;
  v_contador := 0;

  begin
    select obrf_650.vlr_med_vend
    into   v_valor_venda
    from obrf_650
    where obrf_650.cod_empresa = p_empresa
      and obrf_650.mes         = p_mes
      and obrf_650.ano         = p_ano
      and obrf_650.nivel       = p_nivel_produto
      and obrf_650.grupo       = p_grupo_produto
      and obrf_650.subgrupo    = p_subgrupo_produto
      and obrf_650.item        = p_item_produto;
    exception when others then

    if p_busca_valor_venda = 1
    then
        while p_per_max_saida > v_contador
        loop

          v_contador := v_contador + 1;
          v_valor_venda := 0.000000;
          v_total_valor := 0.000000;
          v_total_qtde  := 0.000;

          if p_mes <= v_contador
          then
            v_mes := p_mes + 12;
            v_ano := p_ano - 1;
          end if;

          if p_mes > v_contador
          then
            v_mes := p_mes;
            v_ano := p_ano;
          end if;

          v_data_notas_saida := to_date('01-'||(v_mes-v_contador)||'-'||v_ano,'dd-mm-yyyy');

          --Busca as notas fiscais interestaduais do per?odo imediatamente anterior ao per?odo da apura??o;

          for nf_saida in (select fatu_050.num_nota_fiscal,     fatu_050.serie_nota_fisc,
                                  sum(fatu_060.valor_contabil)  as valor_contabil,
                                  sum(fatu_060.valor_icms)      as valor_icms,
                                  sum(fatu_060.valor_ipi)       as valor_ipi,
                                  sum(fatu_060.qtde_item_fatur) as qtde_item_fatur,
                                  sum(fatu_060.valor_faturado)  as valor_faturado,
                                  basi_010.alternativa_custos   as alternativa_custos
                           from fatu_060, fatu_050, pedi_080, basi_010
                           where fatu_060.nivel_estrutura   = to_char(p_nivel_produto)
                             and fatu_060.grupo_estrutura   = to_char(p_grupo_produto)
                             and fatu_060.subgru_estrutura  = to_char(p_subgrupo_produto)
                             and fatu_060.item_estrutura    = to_char(p_item_produto)
                             and fatu_050.codigo_empresa    = p_empresa
                             and fatu_060.ch_it_nf_cd_empr  = fatu_050.codigo_empresa
                             and fatu_060.ch_it_nf_num_nfis = fatu_050.num_nota_fiscal
                             and fatu_060.ch_it_nf_ser_nfis = fatu_050.serie_nota_fisc
                             and fatu_050.data_saida between v_data_notas_saida and to_date('01-'||p_mes||'-'||p_ano,'dd-mm-yyyy')-1
                             and fatu_060.natopeno_nat_oper = pedi_080.natur_operacao
                             and fatu_060.natopeno_est_oper = pedi_080.estado_natoper
                             and substr(pedi_080.cod_natureza,0,1) = 6
                             and fatu_060.nivel_estrutura   = basi_010.nivel_estrutura
                             and fatu_060.grupo_estrutura   = basi_010.grupo_estrutura
                             and fatu_060.subgru_estrutura  = basi_010.subgru_estrutura
                             and fatu_060.item_estrutura    = basi_010.item_estrutura
                             and fatu_050.situacao_nfisc    <> 2
                           group by fatu_050.num_nota_fiscal, fatu_050.serie_nota_fisc, basi_010.alternativa_custos) -- Operac?o Interestadual
           loop

             v_total_valor := v_total_valor + (nf_saida.valor_contabil - nf_saida.valor_icms - nf_saida.valor_ipi);
             v_total_qtde  := v_total_qtde  + nf_saida.qtde_item_fatur;

             inter_pr_insere_obrf_665(p_empresa,
                                      p_ano,
                                      p_mes,
                                      'S',
                                      p_nivel_produto,
                                      p_grupo_produto,
                                      p_subgrupo_produto,
                                      p_item_produto,
                                      nf_saida.num_nota_fiscal,
                                      nf_saida.serie_nota_fisc,
                                      0,
                                      0,
                                      0,
                                      nf_saida.qtde_item_fatur,
                                      nf_saida.valor_faturado,
                                      nf_saida.valor_contabil - nf_saida.valor_icms - nf_saida.valor_ipi,
                                      0,
                                      0.00,
                                      0.00000,
                                      nf_saida.alternativa_custos,
                                      'I');

           end loop;

           --Busca as notas fiscais estaduais no per?odo imediatamente anterior ao per?odo da apura??o;
           if v_total_valor = 0.0000
           then
             for nf_saida_2 in (select fatu_050.num_nota_fiscal, fatu_050.serie_nota_fisc,
                                       sum(fatu_060.valor_contabil)  as valor_contabil,
                                       sum(fatu_060.valor_icms)      as valor_icms,
                                       sum(fatu_060.valor_ipi)       as valor_ipi,
                                       sum(fatu_060.qtde_item_fatur) as qtde_item_fatur,
                                       sum(fatu_060.valor_faturado)  as valor_faturado,
                                       basi_010.alternativa_custos   as alternativa_custos
                                from fatu_060, fatu_050, pedi_080, basi_010
                                where fatu_060.nivel_estrutura   = to_char(p_nivel_produto)
                                  and fatu_060.grupo_estrutura   = to_char(p_grupo_produto)
                                  and fatu_060.subgru_estrutura  = to_char(p_subgrupo_produto)
                                  and fatu_060.item_estrutura    = to_char(p_item_produto)
                                  and fatu_050.codigo_empresa    = p_empresa
                                  and fatu_060.ch_it_nf_cd_empr  = fatu_050.codigo_empresa
                                  and fatu_060.ch_it_nf_num_nfis = fatu_050.num_nota_fiscal
                                  and fatu_060.ch_it_nf_ser_nfis = fatu_050.serie_nota_fisc
                                  and fatu_050.data_saida between v_data_notas_saida and to_date('01-'||p_mes||'-'||p_ano,'dd-mm-yyyy')-1
                                  and fatu_060.natopeno_nat_oper = pedi_080.natur_operacao
                                  and fatu_060.natopeno_est_oper = pedi_080.estado_natoper
                                  and substr(pedi_080.cod_natureza ,0,1) = 5
                                  and fatu_060.nivel_estrutura   = basi_010.nivel_estrutura
                                  and fatu_060.grupo_estrutura   = basi_010.grupo_estrutura
                                  and fatu_060.subgru_estrutura  = basi_010.subgru_estrutura
                                  and fatu_060.item_estrutura    = basi_010.item_estrutura
                                  and fatu_050.situacao_nfisc    <> 2
                                group by fatu_050.num_nota_fiscal, fatu_050.serie_nota_fisc, basi_010.alternativa_custos) -- Operac?o Estadual
              loop
                v_total_valor := v_total_valor + (nf_saida_2.valor_contabil - nf_saida_2.valor_icms - nf_saida_2.valor_ipi);
                v_total_qtde  := v_total_qtde  + nf_saida_2.qtde_item_fatur;

                inter_pr_insere_obrf_665(p_empresa,
                                         p_ano,
                                         p_mes,
                                         'S',
                                         p_nivel_produto,
                                         p_grupo_produto,
                                         p_subgrupo_produto,
                                         p_item_produto,
                                         nf_saida_2.num_nota_fiscal,
                                         nf_saida_2.serie_nota_fisc,
                                         0,
                                         0,
                                         0,
                                         nf_saida_2.qtde_item_fatur,
                                         nf_saida_2.valor_faturado,
                                         nf_saida_2.valor_contabil - nf_saida_2.valor_icms - nf_saida_2.valor_ipi,
                                         0,
                                         0.00,
                                         0.00000,
                                         nf_saida_2.alternativa_custos,
                                         'E');

              end loop;
            end if;

            if v_total_valor > 0.00000
            then exit;
            end if;

          end loop;

        else
          while p_per_max_saida > v_contador
          loop

          v_mes := v_mes -2;

          if v_contador = 2
          then exit;
          end if;

          v_valor_venda := 0.000000;
          v_total_valor := 0.000000;
          v_total_qtde  := 0.000;

          v_data_notas_saida := to_date('01-'||(v_mes+v_contador)||'-'||v_ano,'dd-mm-yyyy');
          v_contador := v_contador + 1;

          --Busca as notas fiscais interestaduais do pen?ltimo per?odo anterior ao per?odo da apura??o;
          --Busca as notas fiscais interestaduais do per?odo imediatamente anterior ao per?odo da apura??o;

          for nf_saida in (select fatu_050.num_nota_fiscal,     fatu_050.serie_nota_fisc,
                                  sum(fatu_060.valor_contabil)  as valor_contabil,
                                  sum(fatu_060.valor_icms)      as valor_icms,
                                  sum(fatu_060.valor_ipi)       as valor_ipi,
                                  sum(fatu_060.qtde_item_fatur) as qtde_item_fatur,
                                  sum(fatu_060.valor_faturado)  as valor_faturado,
                                  basi_010.alternativa_custos   as alternativa_custos
                           from fatu_060, fatu_050, pedi_080, basi_010
                           where fatu_060.nivel_estrutura   = to_char(p_nivel_produto)
                             and fatu_060.grupo_estrutura   = to_char(p_grupo_produto)
                             and fatu_060.subgru_estrutura  = to_char(p_subgrupo_produto)
                             and fatu_060.item_estrutura    = to_char(p_item_produto)
                             and fatu_050.codigo_empresa    = p_empresa
                             and fatu_060.ch_it_nf_cd_empr  = fatu_050.codigo_empresa
                             and fatu_060.ch_it_nf_num_nfis = fatu_050.num_nota_fiscal
                             and fatu_060.ch_it_nf_ser_nfis = fatu_050.serie_nota_fisc
                             and fatu_050.data_saida between v_data_notas_saida and to_date('01-'||p_mes||'-'||p_ano,'dd-mm-yyyy')-1
                             and fatu_060.natopeno_nat_oper = pedi_080.natur_operacao
                             and fatu_060.natopeno_est_oper = pedi_080.estado_natoper
                             and substr(pedi_080.cod_natureza,0,1) = 6
                             and fatu_060.nivel_estrutura   = basi_010.nivel_estrutura
                             and fatu_060.grupo_estrutura   = basi_010.grupo_estrutura
                             and fatu_060.subgru_estrutura  = basi_010.subgru_estrutura
                             and fatu_060.item_estrutura    = basi_010.item_estrutura
                             and fatu_050.situacao_nfisc    <> 2
                           group by fatu_050.num_nota_fiscal, fatu_050.serie_nota_fisc, basi_010.alternativa_custos) -- Operac?o Interestadual
           loop

             v_total_valor := v_total_valor + (nf_saida.valor_contabil - nf_saida.valor_icms - nf_saida.valor_ipi);
             v_total_qtde  := v_total_qtde  + nf_saida.qtde_item_fatur;

             inter_pr_insere_obrf_665(p_empresa,
                                      p_ano,
                                      p_mes,
                                      'S',
                                      p_nivel_produto,
                                      p_grupo_produto,
                                      p_subgrupo_produto,
                                      p_item_produto,
                                      nf_saida.num_nota_fiscal,
                                      nf_saida.serie_nota_fisc,
                                      0,
                                      0,
                                      0,
                                      nf_saida.qtde_item_fatur,
                                      nf_saida.valor_faturado,
                                      nf_saida.valor_contabil - nf_saida.valor_icms - nf_saida.valor_ipi,
                                      0,
                                      0.00,
                                      0.00000,
                                      nf_saida.alternativa_custos,
                                      'I');

           end loop;

           --Busca as notas fiscais estaduais do pen?ltimo per?odo anterior ao per?odo da apura??o;
           --Busca as notas fiscais estaduais no per?odo imediatamente anterior ao per?odo da apura??o;
           if v_total_valor = 0.0000
           then
             for nf_saida_2 in (select fatu_050.num_nota_fiscal, fatu_050.serie_nota_fisc,
                                       sum(fatu_060.valor_contabil)  as valor_contabil,
                                       sum(fatu_060.valor_icms)      as valor_icms,
                                       sum(fatu_060.valor_ipi)       as valor_ipi,
                                       sum(fatu_060.qtde_item_fatur) as qtde_item_fatur,
                                       sum(fatu_060.valor_faturado)  as valor_faturado,
                                       basi_010.alternativa_custos   as alternativa_custos
                                from fatu_060, fatu_050, pedi_080, basi_010
                                where fatu_060.nivel_estrutura   = to_char(p_nivel_produto)
                                  and fatu_060.grupo_estrutura   = to_char(p_grupo_produto)
                                  and fatu_060.subgru_estrutura  = to_char(p_subgrupo_produto)
                                  and fatu_060.item_estrutura    = to_char(p_item_produto)
                                  and fatu_050.codigo_empresa    = p_empresa
                                  and fatu_060.ch_it_nf_cd_empr  = fatu_050.codigo_empresa
                                  and fatu_060.ch_it_nf_num_nfis = fatu_050.num_nota_fiscal
                                  and fatu_060.ch_it_nf_ser_nfis = fatu_050.serie_nota_fisc
                                  and fatu_050.data_saida between v_data_notas_saida and to_date('01-'||p_mes||'-'||p_ano,'dd-mm-yyyy')-1
                                  and fatu_060.natopeno_nat_oper = pedi_080.natur_operacao
                                  and fatu_060.natopeno_est_oper = pedi_080.estado_natoper
                                  and substr(pedi_080.cod_natureza ,0,1) = 5
                                  and fatu_060.nivel_estrutura   = basi_010.nivel_estrutura
                                  and fatu_060.grupo_estrutura   = basi_010.grupo_estrutura
                                  and fatu_060.subgru_estrutura  = basi_010.subgru_estrutura
                                  and fatu_060.item_estrutura    = basi_010.item_estrutura
                                  and fatu_050.situacao_nfisc    <> 2
                                group by fatu_050.num_nota_fiscal, fatu_050.serie_nota_fisc, basi_010.alternativa_custos) -- Operac?o Estadual
              loop
                v_total_valor := v_total_valor + (nf_saida_2.valor_contabil - nf_saida_2.valor_icms - nf_saida_2.valor_ipi);
                v_total_qtde  := v_total_qtde  + nf_saida_2.qtde_item_fatur;

                inter_pr_insere_obrf_665(p_empresa,
                                         p_ano,
                                         p_mes,
                                         'S',
                                         p_nivel_produto,
                                         p_grupo_produto,
                                         p_subgrupo_produto,
                                         p_item_produto,
                                         nf_saida_2.num_nota_fiscal,
                                         nf_saida_2.serie_nota_fisc,
                                         0,
                                         0,
                                         0,
                                         nf_saida_2.qtde_item_fatur,
                                         nf_saida_2.valor_faturado,
                                         nf_saida_2.valor_contabil - nf_saida_2.valor_icms - nf_saida_2.valor_ipi,
                                         0,
                                         0.00,
                                         0.00000,
                                         nf_saida_2.alternativa_custos,
                                         'E');

              end loop;
            end if;

            if v_total_valor > 0.00000
            then exit;
            end if;

          end loop;
        end if;

        if v_total_valor = 0.00000
        then
          begin
            v_tab_col_tab := 0;
            v_tab_mes_tab := 0;
            v_tab_seq_tab := 0;

            for temp1 in (select rcnb_060.nivel_estrutura, rcnb_060.grupo_estrutura, rcnb_060.item_estrutura
                          into   v_tab_col_tab,            v_tab_mes_tab,            v_tab_seq_tab
                          from rcnb_060, pedi_090
                          where rcnb_060.tipo_registro   = 547
                            and rcnb_060.nr_solicitacao  = 630
                            and rcnb_060.usuario_rel     = p_usuario
                            and rcnb_060.empresa_rel     = p_empresa_mdi
                            and rcnb_060.nivel_estrutura = pedi_090.col_tabela_preco
                            and rcnb_060.grupo_estrutura = pedi_090.mes_tabela_preco
                            and rcnb_060.item_estrutura  = pedi_090.seq_tabela_preco
                          order by pedi_090.data_fim_tabela desc)
            loop
              begin
                select pedi_095.val_tabela_preco
                into   v_val_tabela_preco
                from pedi_095
                where pedi_095.tab_col_tab      = temp1.nivel_estrutura
                  and pedi_095.tab_mes_tab      = temp1.grupo_estrutura
                  and pedi_095.tab_seq_tab      = temp1.item_estrutura
                  and pedi_095.nivel_estrutura  = p_nivel_produto
                  and pedi_095.grupo_estrutura  = p_grupo_produto
                  and pedi_095.subgru_estrutura = p_subgrupo_produto
                  and pedi_095.item_estrutura   = p_item_produto
                  and pedi_095.nivel_preco      = 4;
                exception when others then
                  begin
                    select basi_020.tipo_produto
                    into   v_tipo_produto_020
                    from basi_020
                    where basi_020.basi030_nivel030 = p_nivel_produto
                      and basi_020.basi030_referenc = p_grupo_produto
                      and basi_020.tamanho_ref      = p_subgrupo_produto;
                    exception when others then
                      v_tipo_produto_020 := 0;
                  end;

                  begin
                    select basi_030.tipo_produto
                    into   v_tipo_produto_030
                    from basi_030
                    where basi_030.nivel_estrutura = p_nivel_produto
                      and basi_030.referencia      = p_grupo_produto;
                    exception when others then
                      v_tipo_produto_030 := 0;
                  end;

                  if v_tipo_produto_030 = 1
                  then v_tipo_produto := v_tipo_produto_020;
                  else v_tipo_produto := v_tipo_produto_030;
                  end if;

                  -- Procura a serie de cor do produto para producurar o preco por serie
                  begin
                    select basi_100.serie_cor
                    into   v_serie_cor
                    from basi_100
                    where basi_100.cor_sortimento = p_item_produto
                      and basi_100.tipo_cor       = v_tipo_produto;
                    exception when others then
                      v_serie_cor := '';
                  end;

                  begin
                    select pedi_095.val_tabela_preco
                    into   v_val_tabela_preco
                    from pedi_095
                    where pedi_095.tab_col_tab      = temp1.nivel_estrutura
                      and pedi_095.tab_mes_tab      = temp1.grupo_estrutura
                      and pedi_095.tab_seq_tab      = temp1.item_estrutura
                      and pedi_095.nivel_estrutura  = p_nivel_produto
                      and pedi_095.grupo_estrutura  = p_grupo_produto
                      and pedi_095.subgru_estrutura = p_subgrupo_produto
                      and pedi_095.item_estrutura   = '000000'
                      and pedi_095.serie_cor        = v_serie_cor
                      and pedi_095.nivel_preco      = 3;
                    exception when others then
                      begin
                        select pedi_095.val_tabela_preco
                        into   v_val_tabela_preco
                        from pedi_095
                        where pedi_095.tab_col_tab      = temp1.nivel_estrutura
                          and pedi_095.tab_mes_tab      = temp1.grupo_estrutura
                          and pedi_095.tab_seq_tab      = temp1.item_estrutura
                          and pedi_095.nivel_estrutura  = p_nivel_produto
                          and pedi_095.grupo_estrutura  = p_grupo_produto
                          and pedi_095.subgru_estrutura = p_subgrupo_produto
                          and pedi_095.item_estrutura   = '000000'
                          and pedi_095.nivel_preco      = 2;
                        exception when others then
                          begin
                            select pedi_095.val_tabela_preco
                            into   v_val_tabela_preco
                            from pedi_095
                            where pedi_095.tab_col_tab      = temp1.nivel_estrutura
                              and pedi_095.tab_mes_tab      = temp1.grupo_estrutura
                              and pedi_095.tab_seq_tab      = temp1.item_estrutura
                              and pedi_095.nivel_estrutura  = p_nivel_produto
                              and pedi_095.grupo_estrutura  = p_grupo_produto
                              and pedi_095.subgru_estrutura = '000'
                              and pedi_095.item_estrutura   = '000000'
                              and pedi_095.nivel_preco      = 1;
                            exception when others then
                              v_val_tabela_preco := 0.0000;
                          end;
                      end;
                  end;
                  if v_val_tabela_preco > 0.0000 then
                    begin
                      v_tab_col_tab := temp1.nivel_estrutura;
                      v_tab_mes_tab := temp1.grupo_estrutura;
                      v_tab_seq_tab := temp1.item_estrutura;
                      exit;
                    end;
                  end if;
              end;
            end loop;

            inter_pr_insere_obrf_665(p_empresa,
                                     p_ano,
                                     p_mes,
                                     'T',
                                     p_nivel_produto,
                                     p_grupo_produto,
                                     p_subgrupo_produto,
                                     p_item_produto,
                                     0,
                                     ' ',
                                     v_tab_col_tab,
                                     v_tab_mes_tab,
                                     v_tab_seq_tab,
                                     0.00000,
                                     v_val_tabela_preco,
                                     0.00000,
                                     0,
                                     0.00,
                                     0.00000,
                                     0,
                                     'T');
          end;
        end if;

        if v_total_valor > 0.0000
          then begin
            v_valor_venda := v_total_valor / v_total_qtde;
              if p_casas_decimais > 0
              then
                select round(v_valor_venda, p_casas_decimais)
                into   v_valor_venda
                from dual;
              end if;

          end;
        end if;

        if v_val_tabela_preco > 0.0000
        then v_valor_venda := v_val_tabela_preco;
        end if;

        inter_pr_insere_obrf_650(p_mes,
                                 p_ano,
                                 p_empresa,
                                 p_nivel_produto,
                                 p_grupo_produto,
                                 p_subgrupo_produto,
                                 p_item_produto,
                                 0.0000,
                                 0.0000,
                                 0.0000,
                                 0,
                                 0.00,
                                 v_valor_venda,
                                 '',
                                 0.0000);

  end;
end inter_pr_calcula_valor_venda;
