
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_050_CCE" 
before update of especie_docto,      qtde_embalagens,
                 transpor_forne9,    transpor_forne4,
                 transpor_forne2,    natop_nf_nat_oper,
                 natop_nf_est_oper,  placa_veiculo,
                 peso_liquido,       peso_bruto,
                 tipo_frete

on fatu_050
for each row
begin

   if :old.situacao_nfisc = 1 and (:old.cod_status = '100' or :old.cod_status = '990')
   then :new.st_flag_cce := 1;
   end if;

end inter_tr_fatu_050_cce;
-- ALTER TRIGGER "INTER_TR_FATU_050_CCE" ENABLE
 

/

exec inter_pr_recompile;

