create table pcpb_001(
    codigo_empresa number(3)
    ,tipo_produto  number(9)
    ,tipo_ordem    number(2)
);

alter table pcpb_001
add constraint pk_pcpb_001 primary key(codigo_empresa,tipo_produto,tipo_ordem);

alter table pcpb_001
add constraint fk_fatu_500_pcpb_001 foreign key(codigo_empresa) references fatu_500(CODIGO_EMPRESA);
