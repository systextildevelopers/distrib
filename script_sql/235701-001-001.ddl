alter table pedi_100 
add disp_capa_pedido varchar2(1);

alter table pedi_100 modify 
disp_capa_pedido default 'N';

update pedi_100 
set disp_capa_pedido = 'N';

commit;
