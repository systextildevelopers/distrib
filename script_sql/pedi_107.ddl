CREATE TABLE pedi_107
(
  empresa_origem   			number(3) 		default 0 not null,
  pedido_origem				number(9) 		default 0 not null,
  seq_pedido				number(9) 		default 0 not null,
  empresa_destino			number(3) 		default 0 not null,
  pedido_destino			number(9) 		default 0 not null,
  desc_dblink               varchar2(100) 	default ''
);

alter table pedi_107 add constraint PK_pedi_107  primary key (empresa_origem, pedido_origem, seq_pedido, empresa_destino, pedido_destino);
  
comment on table pedi_107 is 'Relacão das sequências de pedido copiadas para cada empresa destino';

comment on column pedi_107.empresa_origem   is 'Empresa origem do pedido de venda copiado';
comment on column pedi_107.pedido_origem    is 'Número do pedido de venda origem copiado';
comment on column pedi_107.seq_pedido    	is 'Sequência do item do pedido de venda copiado';
comment on column pedi_107.empresa_destino  is 'Empresa destino do pedido de venda copiado';
comment on column pedi_107.pedido_destino   is 'Número do pedido de venda novo (destino) copiado';
comment on column pedi_107.desc_dblink    	is 'Descrição do DBLINK';
