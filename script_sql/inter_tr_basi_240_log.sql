
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_240_LOG" 
after insert or delete or update
on basi_240
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
  
   v_nome_programa := inter_fn_nome_programa(ws_sid);

 if inserting
 then
    begin

        insert into basi_240_log (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           classific_fiscal_old,   /*8*/
           classific_fiscal_new,   /*9*/
           descr_class_fisc_old,   /*10*/
           descr_class_fisc_new,   /*11*/
           aliquota_ipi_old,   /*12*/
           aliquota_ipi_new,   /*13*/
           cod_vlfiscal_ipi_old,   /*14*/
           cod_vlfiscal_ipi_new,   /*15*/
           procedencia_old,   /*16*/
           procedencia_new,   /*17*/
           cod_vlfiscal_ipi_exp_old,   /*18*/
           cod_vlfiscal_ipi_exp_new,   /*19*/
           perc_igv_old,   /*20*/
           perc_igv_new,   /*21*/
           codigo_ex_ipi_old,   /*22*/
           codigo_ex_ipi_new,   /*23*/
           aliquota_icms_sped_old,   /*24*/
           aliquota_icms_sped_new,   /*25*/
           cod_enquadramento_ipi_old,   /*26*/
           cod_enquadramento_ipi_new,   /*27*/
           cvf_ipi_saida_old,   /*28*/
           cvf_ipi_saida_new,   /*29*/
           cvf_ipi_entrada_old,   /*30*/
           cvf_ipi_entrada_new,   /*31*/
           cvf_ipi_saida_exp_old,   /*32*/
           cvf_ipi_saida_exp_new,   /*33*/
           desc_detalha_class_fisc_old,   /*34*/
           desc_detalha_class_fisc_new,   /*35*/
           exporta_classif_sped_old,   /*36*/
           exporta_classif_sped_new,   /*37*/
           situacao_old,   /*38*/
           situacao_new    /*39*/
        ) values (
            'i', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           '',/*8*/
           :new.classific_fiscal, /*9*/
           '',/*10*/
           :new.descr_class_fisc, /*11*/
           0,/*12*/
           :new.aliquota_ipi, /*13*/
           0,/*14*/
           :new.cod_vlfiscal_ipi, /*15*/
           0,/*16*/
           :new.procedencia, /*17*/
           0,/*18*/
           :new.cod_vlfiscal_ipi_exp, /*19*/
           0,/*20*/
           :new.perc_igv, /*21*/
           0,/*22*/
           :new.codigo_ex_ipi, /*23*/
           0,/*24*/
           :new.aliquota_icms_sped, /*25*/
           '',/*26*/
           :new.cod_enquadramento_ipi, /*27*/
           0,/*28*/
           :new.cvf_ipi_saida, /*29*/
           0,/*30*/
           :new.cvf_ipi_entrada, /*31*/
           0,/*32*/
           :new.cvf_ipi_saida_exp, /*33*/
           '',/*34*/
           :new.desc_detalha_class_fisc, /*35*/
           '',/*36*/
           :new.exporta_classif_sped, /*37*/
           0,/*38*/
           :new.situacao /*39*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into basi_240_log (
           tipo_ocorr, /*0*/
           data_ocorr, /*1*/
           hora_ocorr, /*2*/
           usuario_rede, /*3*/
           maquina_rede, /*4*/
           aplicacao, /*5*/
           usuario_sistema, /*6*/
           nome_programa, /*7*/
           classific_fiscal_old, /*8*/
           classific_fiscal_new, /*9*/
           descr_class_fisc_old, /*10*/
           descr_class_fisc_new, /*11*/
           aliquota_ipi_old, /*12*/
           aliquota_ipi_new, /*13*/
           cod_vlfiscal_ipi_old, /*14*/
           cod_vlfiscal_ipi_new, /*15*/
           procedencia_old, /*16*/
           procedencia_new, /*17*/
           cod_vlfiscal_ipi_exp_old, /*18*/
           cod_vlfiscal_ipi_exp_new, /*19*/
           perc_igv_old, /*20*/
           perc_igv_new, /*21*/
           codigo_ex_ipi_old, /*22*/
           codigo_ex_ipi_new, /*23*/
           aliquota_icms_sped_old, /*24*/
           aliquota_icms_sped_new, /*25*/
           cod_enquadramento_ipi_old, /*26*/
           cod_enquadramento_ipi_new, /*27*/
           cvf_ipi_saida_old, /*28*/
           cvf_ipi_saida_new, /*29*/
           cvf_ipi_entrada_old, /*30*/
           cvf_ipi_entrada_new, /*31*/
           cvf_ipi_saida_exp_old, /*32*/
           cvf_ipi_saida_exp_new, /*33*/
           desc_detalha_class_fisc_old, /*34*/
           desc_detalha_class_fisc_new, /*35*/
           exporta_classif_sped_old, /*36*/
           exporta_classif_sped_new, /*37*/
           situacao_old, /*38*/
           situacao_new  /*39*/
        ) values (
            'a', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.classific_fiscal,  /*8*/
           :new.classific_fiscal, /*9*/
           :old.descr_class_fisc,  /*10*/
           :new.descr_class_fisc, /*11*/
           :old.aliquota_ipi,  /*12*/
           :new.aliquota_ipi, /*13*/
           :old.cod_vlfiscal_ipi,  /*14*/
           :new.cod_vlfiscal_ipi, /*15*/
           :old.procedencia,  /*16*/
           :new.procedencia, /*17*/
           :old.cod_vlfiscal_ipi_exp,  /*18*/
           :new.cod_vlfiscal_ipi_exp, /*19*/
           :old.perc_igv,  /*20*/
           :new.perc_igv, /*21*/
           :old.codigo_ex_ipi,  /*22*/
           :new.codigo_ex_ipi, /*23*/
           :old.aliquota_icms_sped,  /*24*/
           :new.aliquota_icms_sped, /*25*/
           :old.cod_enquadramento_ipi,  /*26*/
           :new.cod_enquadramento_ipi, /*27*/
           :old.cvf_ipi_saida,  /*28*/
           :new.cvf_ipi_saida, /*29*/
           :old.cvf_ipi_entrada,  /*30*/
           :new.cvf_ipi_entrada, /*31*/
           :old.cvf_ipi_saida_exp,  /*32*/
           :new.cvf_ipi_saida_exp, /*33*/
           :old.desc_detalha_class_fisc,  /*34*/
           :new.desc_detalha_class_fisc, /*35*/
           :old.exporta_classif_sped,  /*36*/
           :new.exporta_classif_sped, /*37*/
           :old.situacao,  /*38*/
           :new.situacao  /*39*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into basi_240_log (
           tipo_ocorr, /*0*/
           data_ocorr, /*1*/
           hora_ocorr, /*2*/
           usuario_rede, /*3*/
           maquina_rede, /*4*/
           aplicacao, /*5*/
           usuario_sistema, /*6*/
           nome_programa, /*7*/
           classific_fiscal_old, /*8*/
           classific_fiscal_new, /*9*/
           descr_class_fisc_old, /*10*/
           descr_class_fisc_new, /*11*/
           aliquota_ipi_old, /*12*/
           aliquota_ipi_new, /*13*/
           cod_vlfiscal_ipi_old, /*14*/
           cod_vlfiscal_ipi_new, /*15*/
           procedencia_old, /*16*/
           procedencia_new, /*17*/
           cod_vlfiscal_ipi_exp_old, /*18*/
           cod_vlfiscal_ipi_exp_new, /*19*/
           perc_igv_old, /*20*/
           perc_igv_new, /*21*/
           codigo_ex_ipi_old, /*22*/
           codigo_ex_ipi_new, /*23*/
           aliquota_icms_sped_old, /*24*/
           aliquota_icms_sped_new, /*25*/
           cod_enquadramento_ipi_old, /*26*/
           cod_enquadramento_ipi_new, /*27*/
           cvf_ipi_saida_old, /*28*/
           cvf_ipi_saida_new, /*29*/
           cvf_ipi_entrada_old, /*30*/
           cvf_ipi_entrada_new, /*31*/
           cvf_ipi_saida_exp_old, /*32*/
           cvf_ipi_saida_exp_new, /*33*/
           desc_detalha_class_fisc_old, /*34*/
           desc_detalha_class_fisc_new, /*35*/
           exporta_classif_sped_old, /*36*/
           exporta_classif_sped_new, /*37*/
           situacao_old, /*38*/
           situacao_new /*39*/
        ) values (
            'd', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.classific_fiscal, /*8*/
           '', /*9*/
           :old.descr_class_fisc, /*10*/
           '', /*11*/
           :old.aliquota_ipi, /*12*/
           0, /*13*/
           :old.cod_vlfiscal_ipi, /*14*/
           0, /*15*/
           :old.procedencia, /*16*/
           0, /*17*/
           :old.cod_vlfiscal_ipi_exp, /*18*/
           0, /*19*/
           :old.perc_igv, /*20*/
           0, /*21*/
           :old.codigo_ex_ipi, /*22*/
           0, /*23*/
           :old.aliquota_icms_sped, /*24*/
           0, /*25*/
           :old.cod_enquadramento_ipi, /*26*/
           '', /*27*/
           :old.cvf_ipi_saida, /*28*/
           0, /*29*/
           :old.cvf_ipi_entrada, /*30*/
           0, /*31*/
           :old.cvf_ipi_saida_exp, /*32*/
           0, /*33*/
           :old.desc_detalha_class_fisc, /*34*/
           '', /*35*/
           :old.exporta_classif_sped, /*36*/
           '', /*37*/
           :old.situacao, /*38*/
           0 /*39*/
         );
    end;
 end if;
end inter_tr_basi_240_log;
-- ALTER TRIGGER "INTER_TR_BASI_240_LOG" ENABLE
 

/

exec inter_pr_recompile;

