
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_090_LOG" 
   after insert or delete or update
       of data_ini_tabela,  data_fim_tabela,    codigo_moeda,      nivel_estrutura,
          descricao,        observacao,         tipo_preco,        fator_conversao,
          desconto_maximo,  usuario_cadastro,   codigo_politica,   vlr_min_pedido,
          observacao_nota,  cod_deposito,       situacao,          segmento_mercado,
          cd_agrupador,		desconto_aplicado
   on pedi_090
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   long_aux                  varchar2(2000);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      long_aux := '';

      long_aux := long_aux ||
          inter_fn_buscar_tag('lb09776#TABELA DE PRECO',ws_locale_usuario,ws_usuario_systextil) ||
          chr(10)        ||
          chr(10);

      long_aux := long_aux ||
          inter_fn_buscar_tag('lb34884#VALIDADE INICIAL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.data_ini_tabela
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb34885#VALIDADE FINAL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.data_fim_tabela
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb13146#MOEDA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.codigo_moeda
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb06391#NIVEL PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.nivel_estrutura
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb30896#DESCRICAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.descricao
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb00259#OBSERVACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.observacao
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb11519#TIPO PRECO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.tipo_preco
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb03730#FATOR CONVERSAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.fator_conversao
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb34886#DESCONTO MAXIMO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.desconto_maximo
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb31285#USUARIO CADASTRO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.usuario_cadastro
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb10297#POLITICA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.codigo_politica
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb34887#VALOR MIN.PEDIDO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.vlr_min_pedido
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb11373#OBSERVACAO NOTA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.observacao_nota
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb08048#DEPOSITO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.cod_deposito
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.situacao
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb10051#SEGMENTO MERCADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.segmento_mercado
                                             || chr(10);
      long_aux := long_aux ||
          inter_fn_buscar_tag('lb38330#CODIGO AGRUPADOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.cd_agrupador
                                             || chr(10);
	  long_aux := long_aux ||
          inter_fn_buscar_tag('lb49306#DESCONTO APLICADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :new.desconto_aplicado
                                             || chr(10);

      INSERT INTO hist_100
         ( tabela,            operacao,
           data_ocorr,        aplicacao,
           usuario_rede,      maquina_rede,
           num02,             num03,
           num04,             long01
           )
      VALUES
        ( 'PEDI_090',        'I',
           sysdate,           ws_aplicativo,
           ws_usuario_rede,   ws_maquina_rede,
           :new.col_tabela_preco, :new.mes_tabela_preco,
           :new.seq_tabela_preco,
           long_aux
              );
   end if;

   if updating
   then
      long_aux := '';

      long_aux := long_aux ||
          inter_fn_buscar_tag('lb09776#TABELA DE PRECO',ws_locale_usuario,ws_usuario_systextil) ||
            chr(10)        ||
            chr(10);

      if :old.data_ini_tabela <> :new.data_ini_tabela
      then
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb34884#VALIDADE INICIAL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :old.data_ini_tabela   ||'->'
                                                || :new.data_ini_tabela
                                                || chr(10);
      end if;

      if :old.data_fim_tabela <> :new.data_fim_tabela
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb34885#VALIDADE FINAL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.data_fim_tabela   ||'->'
                                                 || :new.data_fim_tabela
                                                 || chr(10);
      end if;

      if :old.codigo_moeda <> :new.codigo_moeda
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb13146#MOEDA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :old.codigo_moeda       ||'->'
                                                || :new.codigo_moeda
                                                || chr(10);
      end if;

      if :old.nivel_estrutura <> :new.nivel_estrutura
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb06391#NIVEL PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :old.nivel_estrutura     ||'->'
                                                || :new.nivel_estrutura
                                                || chr(10);
      end if;

      if :old.descricao <> :new.descricao
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb30896#DESCRICAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                || :old.descricao           ||'->'
                                                || :new.descricao
                                                || chr(10);
      end if;

      if :old.observacao <> :new.observacao
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb00259#OBSERVACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.observacao         ||'->'
                                                 || :new.observacao
                                                 || chr(10);
      end if;

      if :old.tipo_preco <> :new.tipo_preco
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb11519#TIPO PRECO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.tipo_preco         ||'->'
                                                 || :new.tipo_preco
                                                 || chr(10);
      end if;

      if :old.fator_conversao <> :new.fator_conversao
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb03730#FATOR CONVERSAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.fator_conversao     ||'->'
                                                 || :new.fator_conversao
                                                 || chr(10);
      end if;

      if :old.desconto_maximo <> :new.desconto_maximo
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb34886#DESCONTO MAXIMO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.desconto_maximo     ||'->'
                                                 || :new.desconto_maximo
                                                 || chr(10);
      end if;

      if :old.usuario_cadastro <> :new.usuario_cadastro
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb31285#USUARIO CADASTRO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.usuario_cadastro    ||'->'
                                                 || :new.usuario_cadastro
                                                 || chr(10);
      end if;

      if :old.codigo_politica <> :new.codigo_politica
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb10297#POLITICA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.codigo_politica     ||'->'
                                                 || :new.codigo_politica
                                                 || chr(10);
      end if;

      if :old.vlr_min_pedido <> :new.vlr_min_pedido
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb34887#VALOR MIN.PEDIDO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.vlr_min_pedido      ||'->'
                                                 || :new.vlr_min_pedido
                                                 || chr(10);
      end if;

      if :old.observacao_nota <> :new.observacao_nota
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb11373#OBSERVACAO NOTA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.observacao_nota     ||'->'
                                                 || :new.observacao_nota
                                                 || chr(10);
      end if;

      if :old.cod_deposito <> :new.cod_deposito
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb08048#DEPOSITO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.cod_deposito       ||'->'
                                                 || :new.cod_deposito
                                                 || chr(10);
      end if;

      if :old.situacao <> :new.situacao
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.situacao           ||'->'
                                                 || :new.situacao
                                                 || chr(10);
      end if;

      if :old.segmento_mercado <> :new.segmento_mercado
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb10051#SEGMENTO MERCADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.segmento_mercado   ||'->'
                                                 || :new.segmento_mercado
                                                 || chr(10);
      end if;

      if :old.cd_agrupador <> :new.cd_agrupador
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb38330#CODIGO AGRUPADOR:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                 || :old.cd_agrupador   ||'->'
                                                 || :new.cd_agrupador
                                                 || chr(10);
      end if;
      
	  if :old.desconto_aplicado <> :new.desconto_aplicado
      then
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb49306#DESCONTO APLICADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                             || :old.desconto_aplicado  ||'->'
                                             || :new.desconto_aplicado
                                             || chr(10);
      end if;

      INSERT INTO hist_100
      ( tabela,            operacao,
        data_ocorr,        aplicacao,
        usuario_rede,      maquina_rede,
        num02,             num03,
        num04,             long01
        )
      VALUES
      ( 'PEDI_090',        'A',
         sysdate,           ws_aplicativo,
         ws_usuario_rede,   ws_maquina_rede,
         :new.col_tabela_preco, :new.mes_tabela_preco,
         :new.seq_tabela_preco,
         long_aux
      );

   end if;

   if deleting
   then
      INSERT INTO hist_100
         ( tabela,            operacao,
           data_ocorr,        aplicacao,
           usuario_rede,      maquina_rede,
           num02,             num03,
           num04
           )
      VALUES
        ( 'PEDI_090',        'D',
           sysdate,           ws_aplicativo,
           ws_usuario_rede,   ws_maquina_rede,
           :new.col_tabela_preco, :new.mes_tabela_preco,
           :new.seq_tabela_preco
        );
   end if;

end inter_tr_pedi_090_log;
-- ALTER TRIGGER "INTER_TR_PEDI_090_LOG" ENABLE
 

/

exec inter_pr_recompile;

