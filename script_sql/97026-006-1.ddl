create table fatu_014 (
uf                 	varchar2(2)  default ' ',
cod_receita        	varchar2(20) default ' ',
cod_doc_origem     	varchar2(10) default ' ',
cod_extra          	varchar2(10) default ' ',
tip_extra          	varchar2(10) default ' ',
opcao_campo_extra 	number(1) 	 default 0,
detalhe_destin 		varchar2(1)  default 'S',
detalhe_receita 	varchar2(10) default ' ',
inf_periodo 		varchar2(1)  default 'S',
cod_receita_fcp 	varchar2(20) default ' ',
detalhe_receita_fcp varchar2(10) default ' ',
produto_rec     	varchar2(20) default ' ',
valor_total 		varchar2(1)  default ' ');

alter table fatu_014 add (uf varchar2(2)  default ' ');
alter table fatu_014 add (cod_receita varchar2(20) default ' ');
alter table fatu_014 add (cod_doc_origem varchar2(10) default ' ');
alter table fatu_014 add (cod_extra varchar2(10) default ' ');
alter table fatu_014 add (tip_extra varchar2(10) default ' ');
alter table fatu_014 add (opcao_campo_extra number(1) 	 default 0);
alter table fatu_014 add (detalhe_destin varchar2(1)  default 'S');
alter table fatu_014 add (detalhe_receita varchar2(10) default ' ');
alter table fatu_014 add (inf_periodo varchar2(1)  default 'S');
alter table fatu_014 add (cod_receita_fcp varchar2(20) default ' ');
alter table fatu_014 add (detalhe_receita_fcp varchar2(10) default ' ');
alter table fatu_014 add (produto_rec varchar2(20) default ' ');
alter table fatu_014 add (valor_total varchar2(1)  default ' ');

comment on column fatu_014.opcao_campo_extra is '0 - para danfe ou 1 - numero da nota fiscal';
comment on column fatu_014.detalhe_destin is 'S - Gera detalhamento de destinatario, N - N�o gera';
comment on column fatu_014.detalhe_receita is 'C�digo da receita ';
comment on column fatu_014.inf_periodo is 'S - Gera periodo, N - N�o gera';
comment on column fatu_014.cod_receita_fcp is 'C�digo de receita fcp';
comment on column fatu_014.produto_rec is 'C�digo de produto receita';
comment on column fatu_014.detalhe_receita_fcp is 'C�digo da receita FCP';
comment on column fatu_014.valor_total is 'S - Gera o campo valor total, N - N�o gera';

exec inter_pr_recompile;
/
