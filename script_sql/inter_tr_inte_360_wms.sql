create or replace trigger inter_tr_inte_360_wms
before insert or
       update of cgc9_transp,
                 cgc4_transp,
                 cgc2_transp,
                 nr_rampa
on inte_360
for each row
  
begin
      
   /* Inserir na tabela de integra��o */
   begin
      insert into inte_wms_rampas
         (cgc9_transp,
          cgc4_transp,
          cgc2_transp,
          nr_rampa
         )
      values
         (:new.cgc9_transp,
          :new.cgc4_transp,
          :new.cgc2_transp,
          :new.nr_rampa
         );
   exception
      when others then
         raise_application_error(-20000, 'N�o inseriu na tabela INTE_WMS_RAMPAS.' || Chr(10) || SQLERRM);
   end;

end inter_tr_inte_360_wms;

/

execute inter_pr_recompile;
/* versao: 1 */


 exit;
