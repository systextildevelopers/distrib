DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f135';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'aceita_antecipacao';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'aceita_antecipacao',        0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f135';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'permite_parcial';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'permite_parcial',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f135';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'antecipacao_a_partir_de';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'antecipacao_a_partir_de',   0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f135';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'conceder_data_base_fatur';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'conceder_data_base_fatur',  0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f135';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'observacao';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'observacao',                0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f135';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'exigencia_requisito_ambiental';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,      reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,           ' ', 
            'exigencia_requisito_ambiental', 0, 
            1,                               0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f135';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'especificar_requisito';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,      reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,           ' ', 
            'especificar_requisito',         0, 
            1,                               0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f135';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'obs_lista_conteudo_1';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,      reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,           ' ', 
            'obs_lista_conteudo_1',          0, 
            1,                               0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f135';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'obs_lista_conteudo_2';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,      reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,           ' ', 
            'obs_lista_conteudo_2',          0, 
            1,                               0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f135';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'obs_lista_conteudo_3';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,      reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,           ' ', 
            'obs_lista_conteudo_3',          0, 
            1,                               0
         );
         
         commit;
      end;
    
  end loop;
end;

/
