
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_010_SPED" 
BEFORE  UPDATE

of narrativa

on basi_010
for each row

begin

  if :new.narrativa <> :old.narrativa
  then
    insert into hist_100(
      tabela,
      str10,
      operacao,
      data_ocorr,
      str01,
      str02,
      str03,
      str04,
      str05)
    values (
      'basi_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      :new.nivel_estrutura,
      :new.grupo_estrutura,
      :new.subgru_estrutura,
      :new.item_estrutura,
      :old.narrativa);
  end if;


end;
-- ALTER TRIGGER "INTER_TR_BASI_010_SPED" ENABLE
 

/

exec inter_pr_recompile;

