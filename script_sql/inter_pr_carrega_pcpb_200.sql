
  CREATE OR REPLACE PROCEDURE "INTER_PR_CARREGA_PCPB_200" 
is
   cursor cr_grafico is
      select * from oper_tmp
      where oper_tmp.nome_relatorio = 'pcpb_e440'
      order by oper_tmp.str_01,                oper_tmp.str_02,
               oper_tmp.str_03,                oper_tmp.int_05,
               oper_tmp.int_07,                oper_tmp.int_08;

   cursor cr_menor_pcpb200(p_data_zero date) is
      select data_base, data_situacao, situacao
      from pcpb_200
      where pcpb_200.data_base = p_data_zero --parametro para data de geracao do relatorio
        and situacao < 0
      group by situacao, data_situacao, data_base
      order by situacao desc;

   cursor cr_maior_pcpb200(p_data_zero date) is
      select data_base, data_situacao, situacao
      from pcpb_200
      where pcpb_200.data_base = p_data_zero --parametro para data de geracao do relatorio
        and situacao >= 0
      group by situacao, data_situacao, data_base
      order by situacao;

   --declaracao de variaveis
   v_descr_estagio  varchar2(20) := ' ';
   v_cor_ot         varchar2(6)  := ' ';
   v_nome_cliente   varchar2(40) := ' ';
   v_cor_estagio    varchar2(6)  := ' ';
   v_pedido         number(9)    := 0;
   v_max_sequencia  number(9);
   v_dias_nao_uteis number(9)    := 0;
   v_continua       number(9);
   v_count_dias     number(9);
   v_data_situacao  date;
   v_data_entrega   date;
   v_data_zero      date;

begin
   v_max_sequencia := 0;

   --executa o loop para todas as maquinas
   delete pcpb_200
   where pcpb_200.data_base IN
     (select data_criacao
      from oper_tmp
      where nome_relatorio = 'pcpb_e440'
      group by data_criacao);

   for reg_grafico in cr_grafico
   loop
      v_data_entrega := null;
      v_nome_cliente := ' ';
      v_data_zero    := reg_grafico.data_criacao;

      begin
         --encontra a descricao do estagio
         select mqop_005.descricao into v_descr_estagio
         from  mqop_005
         where mqop_005.codigo_estagio = reg_grafico.int_08
         group by descricao;

         exception
            when others then
               raise_application_error(-20000,'Estagio: ' || to_char(reg_grafico.int_08) || ' nao cadastrado.');
      end;

      --pega a data correspondente a situacao
      if reg_grafico.int_05 <> 0
      then v_data_situacao := reg_grafico.data_criacao + reg_grafico.int_05;
      else v_data_situacao := reg_grafico.data_criacao;
      end if;

      begin
         --busca a cor da OB
         select pcpb_020.pano_sbg_item into v_cor_ot
         from  pcpb_020
         where pcpb_020.ordem_producao = reg_grafico.int_07
         group by pano_sbg_item;

         -- Se a ordem de beneficiamento por algum motivo for eliminada no decorrer da geracao
         -- do relatorio (SS 25256), esta ordem nao sera carregada.
         exception
            when others then
               v_cor_ot := 'N_CAD';
               --raise_application_error(-20000,'Cor da OB: ' || to_char(reg_grafico.int_07) || ' nao encontrada.');
      end;

      -- Se encontrou OB entra, senao nao.
      if v_cor_ot <> 'N_CAD'
      then
         begin
            --busca o pedido da OB
            select pcpb_030.nr_pedido_ordem into v_pedido
            from  pcpb_030
            where pcpb_030.ordem_producao = reg_grafico.int_07
            and ROWNUM <= 1;

            exception
               when others then
                 v_pedido := 0;
                 -- raise_application_error(-20000,'Pedido da OB: ' || to_char(reg_grafico.int_07) || ' nao encontrado.');
         end;

         --busca o nome do cliente da OB e data entrega do pedido
         if v_pedido <> 0
         then
            begin
               select pedi_010.nome_cliente into v_nome_cliente
               from pedi_100, pedi_010
               where pedi_100.cli_ped_cgc_cli9 = pedi_010.cgc_9
                 and pedi_100.cli_ped_cgc_cli4 = pedi_010.cgc_4
                 and pedi_100.cli_ped_cgc_cli2 = pedi_010.cgc_2
                 and pedi_100.pedido_venda     = v_pedido;

               exception
                  when others then
                     v_nome_cliente := ' ';
                     --raise_application_error(-20000,'Pedido para o cliente nao encontrado. (Nome Cliente)');
            end;

            begin
               select pedi_100.data_entr_venda into v_data_entrega
               from pedi_100, pedi_010
               where pedi_100.cli_ped_cgc_cli9 = pedi_010.cgc_9
                 and pedi_100.cli_ped_cgc_cli4 = pedi_010.cgc_4
                 and pedi_100.cli_ped_cgc_cli2 = pedi_010.cgc_2
                 and pedi_100.pedido_venda     = v_pedido;

               exception
                  when others then
                     v_data_entrega := '01-jan-80';
                     --raise_application_error(-20000,'Pedido para o cliente nao encontrado. (Data Entrega)');
            end;
         end if;

         begin
            --busca a cor representativa para o estagio
            select cor_representativa into v_cor_estagio
            from  mqop_005
            where mqop_005.codigo_estagio = reg_grafico.int_08
            group by cor_representativa;

            exception
               when others then
                  raise_application_error(-20000,'Cor representativa para o estagio: ' || to_char(reg_grafico.int_08) || ' nao encontrada.');
         end;

         v_max_sequencia := v_max_sequencia + 1;

         insert into pcpb_200 (
            grupo_maq,                    subgrupo_maq,
            numero_maq,                   sequencia,
            data_base,
            data_situacao,                situacao,
            estagio_atual,                numero_ot,
            numero_ob,                    cor_ot,
            cor_estagio,                  tempo,
            pedido,                       cliente,
            data_entrega)
         values (
            reg_grafico.str_01,           reg_grafico.str_02,
            to_number(reg_grafico.str_03),v_max_sequencia + 1,
            reg_grafico.data_criacao,
            v_data_situacao,              reg_grafico.int_05,
            reg_grafico.int_08,           reg_grafico.int_02,
            reg_grafico.int_07,           v_cor_ot,
            v_cor_estagio,                reg_grafico.int_06,
            v_pedido,                     v_nome_cliente,
            v_data_entrega);
         commit;
      end if;
   end loop;

   --calcula a data util para as situacoes menores que a data base.
   v_dias_nao_uteis := 0;
   for reg_menor_pcpb200 in cr_menor_pcpb200(v_data_zero)
   loop
      v_continua := 1;
      while v_continua = 1
      loop
         select count(*) into v_count_dias
         from basi_260
         where data_calendario = reg_menor_pcpb200.data_base + reg_menor_pcpb200.situacao - v_dias_nao_uteis
           and dia_util        = 0;

         if v_count_dias > 0 then
            v_continua := 0;
         else
            v_dias_nao_uteis := v_dias_nao_uteis + 1;
         end if;
      end loop;

      begin
         update pcpb_200
            set data_situacao = reg_menor_pcpb200.data_base + reg_menor_pcpb200.situacao - v_dias_nao_uteis
         where pcpb_200.data_base = reg_menor_pcpb200.data_base
           and pcpb_200.situacao  = reg_menor_pcpb200.situacao;

         exception
            when others then
               raise_application_error(-20000,'Nao atualizou data_situacao. (pcpb_200) - 1');
      end;
      commit;
   end loop;

   --calcula a data util para as situacoes maiores ou iguais que a data base.
   v_dias_nao_uteis := 0;
   for reg_maior_pcpb200 in cr_maior_pcpb200(v_data_zero)
   loop
      v_continua := 1;
      while v_continua = 1
      loop
         select count(*) into v_count_dias
         from basi_260
         where data_calendario = reg_maior_pcpb200.data_base + reg_maior_pcpb200.situacao + v_dias_nao_uteis
           and dia_util        = 0;

         if v_count_dias > 0 then
            v_continua := 0;
         else
            v_dias_nao_uteis := v_dias_nao_uteis + 1;
         end if;
      end loop;

      begin
         update pcpb_200
            set data_situacao = reg_maior_pcpb200.data_base + reg_maior_pcpb200.situacao + v_dias_nao_uteis
         where pcpb_200.data_base = reg_maior_pcpb200.data_base
           and pcpb_200.situacao  = reg_maior_pcpb200.situacao;

         exception
            when others then
               raise_application_error(-20000,'Nao atualizou data_situacao. (pcpb_200) - 2');
      end;
      commit;
   end loop;
end inter_pr_carrega_pcpb_200;

 

/

exec inter_pr_recompile;

