CREATE OR REPLACE TRIGGER PK_blocok_cfop 
    before insert on blocok_cfop    
    for each row  
begin   
    select
        nvl(max(id),0) + 1
    into :NEW.ID
    from blocok_cfop ;
end;
