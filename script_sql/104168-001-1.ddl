alter table estq_040
add (qtde_estoque_syst_imagem number(13,3));

alter table estq_040 add data_imagem date;

alter table estq_040
modify ( data_imagem default to_date(to_char(sysdate,'dd-mon-yyyy')));

comment on column estq_040.qtde_estoque_syst_imagem
 is 'Este campo serve para guardar a quantidade do estoque congelado do Systextil para executar a sugest�o de faturamento.';

/

exec inter_pr_recompile;
