
  CREATE OR REPLACE TRIGGER "INTER_TR_CREC_165_2" 
  before update on crec_165
  for each row

begin
   /*Se n�o tiver saldo e o saldo do juros maior que zero, a situa��o ser� 2 - Saldos transf. em juros*/
   if      :new.saldo_lote <= 0 and :new.saldo_juros > 0
   then :new.situacao_lote := 2;
   /*Se n�o tiver saldo e o saldo do adiantamento maior que zero, a situa��o ser� 3 - Saldos transf. em adiantamento*/
   elsif :new.saldo_lote <= 0 and :new.saldo_adiantamento > 0
   then :new.situacao_lote := 3;
   /*Se n�o tiver saldo, a situa��o ser� 0 - Saldo zero*/
   elsif :new.saldo_lote <= 0
   then :new.situacao_lote := 0;
   /*Se tiver saldo, a situa��o ser� 1 - Saldo pendente*/
   else :new.situacao_lote := 1;
   end if;
end;

-- ALTER TRIGGER "INTER_TR_CREC_165_2" ENABLE
 

/

exec inter_pr_recompile;

