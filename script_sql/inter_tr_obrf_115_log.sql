CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_115_LOG" 
after insert or delete or update
on obrf_115
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
                           
   v_nome_programa := inter_fn_nome_programa(ws_sid); 

 if inserting
 then
    begin

        insert into obrf_115_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           DOCUMENTO_OLD,   /*8*/
           DOCUMENTO_NEW,   /*9*/
           SERIE_OLD,   /*10*/
           SERIE_NEW,   /*11*/
           FORNECEDOR9_OLD,   /*12*/
           FORNECEDOR9_NEW,   /*13*/
           FORNECEDOR4_OLD,   /*14*/
           FORNECEDOR4_NEW,   /*15*/
           FORNECEDOR2_OLD,   /*16*/
           FORNECEDOR2_NEW,   /*17*/
           DIVISAO_OLD,   /*18*/
           DIVISAO_NEW,   /*19*/
           COD_PROBLEMA_OLD,   /*20*/
           COD_PROBLEMA_NEW,   /*21*/
           DATA_PROBLEMA_OLD,   /*22*/
           DATA_PROBLEMA_NEW,   /*23*/
           SEQUENCIA_OLD,   /*24*/
           SEQUENCIA_NEW,   /*25*/
           NIVEL_OLD,   /*26*/
           NIVEL_NEW,   /*27*/
           GRUPO_OLD,   /*28*/
           GRUPO_NEW,   /*29*/
           SUBGRUPO_OLD,   /*30*/
           SUBGRUPO_NEW,   /*31*/
           ITEM_OLD,   /*32*/
           ITEM_NEW,   /*33*/
           COMPLEMENTO_OLD,   /*34*/
           COMPLEMENTO_NEW,   /*35*/
           STATUS_OLD,   /*36*/
           STATUS_NEW    /*37*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.DOCUMENTO, /*9*/
           '',/*10*/
           :new.SERIE, /*11*/
           0,/*12*/
           :new.FORNECEDOR9, /*13*/
           0,/*14*/
           :new.FORNECEDOR4, /*15*/
           0,/*16*/
           :new.FORNECEDOR2, /*17*/
           '',/*18*/
           :new.DIVISAO, /*19*/
           0,/*20*/
           :new.COD_PROBLEMA, /*21*/
           null,/*22*/
           :new.DATA_PROBLEMA, /*23*/
           0,/*24*/
           :new.SEQUENCIA, /*25*/
           '',/*26*/
           :new.NIVEL, /*27*/
           '',/*28*/
           :new.GRUPO, /*29*/
           '',/*30*/
           :new.SUBGRUPO, /*31*/
           '',/*32*/
           :new.ITEM, /*33*/
           '',/*34*/
           :new.COMPLEMENTO, /*35*/
           0,/*36*/
           :new.STATUS /*37*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into obrf_115_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           DOCUMENTO_OLD, /*8*/
           DOCUMENTO_NEW, /*9*/
           SERIE_OLD, /*10*/
           SERIE_NEW, /*11*/
           FORNECEDOR9_OLD, /*12*/
           FORNECEDOR9_NEW, /*13*/
           FORNECEDOR4_OLD, /*14*/
           FORNECEDOR4_NEW, /*15*/
           FORNECEDOR2_OLD, /*16*/
           FORNECEDOR2_NEW, /*17*/
           DIVISAO_OLD, /*18*/
           DIVISAO_NEW, /*19*/
           COD_PROBLEMA_OLD, /*20*/
           COD_PROBLEMA_NEW, /*21*/
           DATA_PROBLEMA_OLD, /*22*/
           DATA_PROBLEMA_NEW, /*23*/
           SEQUENCIA_OLD, /*24*/
           SEQUENCIA_NEW, /*25*/
           NIVEL_OLD, /*26*/
           NIVEL_NEW, /*27*/
           GRUPO_OLD, /*28*/
           GRUPO_NEW, /*29*/
           SUBGRUPO_OLD, /*30*/
           SUBGRUPO_NEW, /*31*/
           ITEM_OLD, /*32*/
           ITEM_NEW, /*33*/
           COMPLEMENTO_OLD, /*34*/
           COMPLEMENTO_NEW, /*35*/
           STATUS_OLD, /*36*/
           STATUS_NEW  /*37*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.DOCUMENTO,  /*8*/
           :new.DOCUMENTO, /*9*/
           :old.SERIE,  /*10*/
           :new.SERIE, /*11*/
           :old.FORNECEDOR9,  /*12*/
           :new.FORNECEDOR9, /*13*/
           :old.FORNECEDOR4,  /*14*/
           :new.FORNECEDOR4, /*15*/
           :old.FORNECEDOR2,  /*16*/
           :new.FORNECEDOR2, /*17*/
           :old.DIVISAO,  /*18*/
           :new.DIVISAO, /*19*/
           :old.COD_PROBLEMA,  /*20*/
           :new.COD_PROBLEMA, /*21*/
           :old.DATA_PROBLEMA,  /*22*/
           :new.DATA_PROBLEMA, /*23*/
           :old.SEQUENCIA,  /*24*/
           :new.SEQUENCIA, /*25*/
           :old.NIVEL,  /*26*/
           :new.NIVEL, /*27*/
           :old.GRUPO,  /*28*/
           :new.GRUPO, /*29*/
           :old.SUBGRUPO,  /*30*/
           :new.SUBGRUPO, /*31*/
           :old.ITEM,  /*32*/
           :new.ITEM, /*33*/
           :old.COMPLEMENTO,  /*34*/
           :new.COMPLEMENTO, /*35*/
           :old.STATUS,  /*36*/
           :new.STATUS  /*37*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into obrf_115_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           DOCUMENTO_OLD, /*8*/
           DOCUMENTO_NEW, /*9*/
           SERIE_OLD, /*10*/
           SERIE_NEW, /*11*/
           FORNECEDOR9_OLD, /*12*/
           FORNECEDOR9_NEW, /*13*/
           FORNECEDOR4_OLD, /*14*/
           FORNECEDOR4_NEW, /*15*/
           FORNECEDOR2_OLD, /*16*/
           FORNECEDOR2_NEW, /*17*/
           DIVISAO_OLD, /*18*/
           DIVISAO_NEW, /*19*/
           COD_PROBLEMA_OLD, /*20*/
           COD_PROBLEMA_NEW, /*21*/
           DATA_PROBLEMA_OLD, /*22*/
           DATA_PROBLEMA_NEW, /*23*/
           SEQUENCIA_OLD, /*24*/
           SEQUENCIA_NEW, /*25*/
           NIVEL_OLD, /*26*/
           NIVEL_NEW, /*27*/
           GRUPO_OLD, /*28*/
           GRUPO_NEW, /*29*/
           SUBGRUPO_OLD, /*30*/
           SUBGRUPO_NEW, /*31*/
           ITEM_OLD, /*32*/
           ITEM_NEW, /*33*/
           COMPLEMENTO_OLD, /*34*/
           COMPLEMENTO_NEW, /*35*/
           STATUS_OLD, /*36*/
           STATUS_NEW /*37*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.DOCUMENTO, /*8*/
           0, /*9*/
           :old.SERIE, /*10*/
           '', /*11*/
           :old.FORNECEDOR9, /*12*/
           0, /*13*/
           :old.FORNECEDOR4, /*14*/
           0, /*15*/
           :old.FORNECEDOR2, /*16*/
           0, /*17*/
           :old.DIVISAO, /*18*/
           '', /*19*/
           :old.COD_PROBLEMA, /*20*/
           0, /*21*/
           :old.DATA_PROBLEMA, /*22*/
           null, /*23*/
           :old.SEQUENCIA, /*24*/
           0, /*25*/
           :old.NIVEL, /*26*/
           '', /*27*/
           :old.GRUPO, /*28*/
           '', /*29*/
           :old.SUBGRUPO, /*30*/
           '', /*31*/
           :old.ITEM, /*32*/
           '', /*33*/
           :old.COMPLEMENTO, /*34*/
           '', /*35*/
           :old.STATUS, /*36*/
           0 /*37*/
         );
    end;
 end if;
end inter_tr_obrf_115_log;

-- ALTER TRIGGER "INTER_TR_OBRF_115_LOG" ENABLE
 

/

exec inter_pr_recompile;

