create or replace trigger wms_tr_tipos_volumes_log
  before insert or delete on inte_wms_tipos_volumes
  for each row
declare
  -- local variables here
  v_cod_tipo_volume_st  inte_wms_tipos_volumes.cod_tipo_volume_st  %type;
  v_cod_integracao_wms  inte_wms_tipos_volumes.cod_integracao_wms  %type;
  v_desc_tipo_vol_st    inte_wms_tipos_volumes.desc_tipo_vol_st    %type;
  v_uso_tipo_vol        inte_wms_tipos_volumes.uso_tipo_vol        %type;
  v_timestamp_aimportar inte_wms_tipos_volumes.timestamp_aimportar %type;
  
  v_sid                 number(9);
  v_empresa             number(3);
  v_usuario_systextil   varchar2(250);
  v_locale_usuario      varchar2(5);
  v_nome_programa       varchar2(20);

  v_operacao            varchar(1);
  v_data_operacao       date;
  v_usuario_rede        varchar(20);
  v_maquina_rede        varchar(40);
  v_aplicativo          varchar(20);
  
begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();

   --alimenta as variaveis new caso seja insert
   if inserting
   then
      v_operacao := 'i';

      v_cod_tipo_volume_st  := :new.cod_tipo_volume_st;
      v_cod_integracao_wms  := :new.cod_integracao_wms;
      v_desc_tipo_vol_st    := :new.desc_tipo_vol_st;
      v_uso_tipo_vol        := :new.uso_tipo_vol;
      v_timestamp_aimportar := :new.timestamp_aimportar;

   end if; --fim do if inserting or updating

   --alimenta as variaveis old caso seja insert ou update
   if deleting
   then
      v_operacao      := 'd';

      v_cod_tipo_volume_st  := :old.cod_tipo_volume_st;
      v_cod_integracao_wms  := :old.cod_integracao_wms;
      v_desc_tipo_vol_st    := :old.desc_tipo_vol_st;
      v_uso_tipo_vol        := :old.uso_tipo_vol;
      v_timestamp_aimportar := :old.timestamp_aimportar;
      
   end if; --fim do if deleting or updating


   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);


    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente

   --insere na inte_wms_tipos_volumes_log o registro.
   insert into inte_wms_tipos_volumes_log (
      cod_tipo_volume_st,  cod_integracao_wms,
      desc_tipo_vol_st,    uso_tipo_vol,
      timestamp_aimportar, operacao,
      data_operacao,       usuario_rede,
      maquina_rede,        aplicativo,
      nome_programa
   )
   values (
      v_cod_tipo_volume_st,  v_cod_integracao_wms,
      v_desc_tipo_vol_st,    v_uso_tipo_vol,
      v_timestamp_aimportar, v_operacao,
      v_data_operacao,       v_usuario_rede,
      v_maquina_rede,        v_aplicativo,
      v_nome_programa
   );

end inte_wms_tipos_volumes;

/

exec inter_pr_recompile;
/* versao: 1 */


 exit;
