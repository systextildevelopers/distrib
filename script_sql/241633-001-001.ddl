create table CONT_520_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(20) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(20) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  cod_empresa_OLD 			NUMBER(3) default 0 ,  
  cod_empresa_NEW 			NUMBER(3) default 0 , 
  exercicio_OLD   			NUMBER(4) default 0 ,  
  exercicio_NEW   			NUMBER(4) default 0 , 
  origem_OLD      			NUMBER(2) default 0 ,  
  origem_NEW      			NUMBER(2) default 0 , 
  lote_OLD        			NUMBER(5) default 0 ,  
  lote_NEW        			NUMBER(5) default 0 , 
  data_lote_OLD   			DATE,  
  data_lote_NEW   			DATE, 
  situacao_OLD    			NUMBER(1) default 0 , 
  situacao_NEW    			NUMBER(1) default 0 
) ;

/

exec inter_pr_recompile;
