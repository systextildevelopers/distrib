
  CREATE OR REPLACE TRIGGER "INTER_TR_LOJA_060_LOG" 
after insert or delete or update
on LOJA_060
for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   v_nome_programa           varchar2(20);
begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

 v_nome_programa := inter_fn_nome_programa(ws_sid);

 if inserting
 then
    begin

        insert into LOJA_060_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           DOCUMENTO_OLD,   /*8*/
           DOCUMENTO_NEW,   /*9*/
           CNPJ9_OLD,   /*10*/
           CNPJ9_NEW,   /*11*/
           CNPJ4_OLD,   /*12*/
           CNPJ4_NEW,   /*13*/
           CNPJ2_OLD,   /*14*/
           CNPJ2_NEW,   /*15*/
           DATA_EMISSAO_OLD,   /*16*/
           DATA_EMISSAO_NEW,   /*17*/
           SITUACAO_OLD,   /*18*/
           SITUACAO_NEW,   /*19*/
           COL_TABELA_OLD,   /*20*/
           COL_TABELA_NEW,   /*21*/
           MES_TABELA_OLD,   /*22*/
           MES_TABELA_NEW,   /*23*/
           SEQ_TABELA_OLD,   /*24*/
           SEQ_TABELA_NEW,   /*25*/
           CODIGO_EMPRESA_OLD,   /*26*/
           CODIGO_EMPRESA_NEW,   /*27*/
           CONSIGNACAO_DEVOLUCAO_OLD,   /*28*/
           CONSIGNACAO_DEVOLUCAO_NEW    /*29*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.DOCUMENTO, /*9*/
           0,/*10*/
           :new.CNPJ9, /*11*/
           0,/*12*/
           :new.CNPJ4, /*13*/
           0,/*14*/
           :new.CNPJ2, /*15*/
           null,/*16*/
           :new.DATA_EMISSAO, /*17*/
           0,/*18*/
           :new.SITUACAO, /*19*/
           0,/*20*/
           :new.COL_TABELA, /*21*/
           0,/*22*/
           :new.MES_TABELA, /*23*/
           0,/*24*/
           :new.SEQ_TABELA, /*25*/
           0,/*26*/
           :new.CODIGO_EMPRESA, /*27*/
           0,/*28*/
           :new.CONSIGNACAO_DEVOLUCAO /*29*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into LOJA_060_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           DOCUMENTO_OLD, /*8*/
           DOCUMENTO_NEW, /*9*/
           CNPJ9_OLD, /*10*/
           CNPJ9_NEW, /*11*/
           CNPJ4_OLD, /*12*/
           CNPJ4_NEW, /*13*/
           CNPJ2_OLD, /*14*/
           CNPJ2_NEW, /*15*/
           DATA_EMISSAO_OLD, /*16*/
           DATA_EMISSAO_NEW, /*17*/
           SITUACAO_OLD, /*18*/
           SITUACAO_NEW, /*19*/
           COL_TABELA_OLD, /*20*/
           COL_TABELA_NEW, /*21*/
           MES_TABELA_OLD, /*22*/
           MES_TABELA_NEW, /*23*/
           SEQ_TABELA_OLD, /*24*/
           SEQ_TABELA_NEW, /*25*/
           CODIGO_EMPRESA_OLD, /*26*/
           CODIGO_EMPRESA_NEW, /*27*/
           CONSIGNACAO_DEVOLUCAO_OLD, /*28*/
           CONSIGNACAO_DEVOLUCAO_NEW  /*29*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.DOCUMENTO,  /*8*/
           :new.DOCUMENTO, /*9*/
           :old.CNPJ9,  /*10*/
           :new.CNPJ9, /*11*/
           :old.CNPJ4,  /*12*/
           :new.CNPJ4, /*13*/
           :old.CNPJ2,  /*14*/
           :new.CNPJ2, /*15*/
           :old.DATA_EMISSAO,  /*16*/
           :new.DATA_EMISSAO, /*17*/
           :old.SITUACAO,  /*18*/
           :new.SITUACAO, /*19*/
           :old.COL_TABELA,  /*20*/
           :new.COL_TABELA, /*21*/
           :old.MES_TABELA,  /*22*/
           :new.MES_TABELA, /*23*/
           :old.SEQ_TABELA,  /*24*/
           :new.SEQ_TABELA, /*25*/
           :old.CODIGO_EMPRESA,  /*26*/
           :new.CODIGO_EMPRESA, /*27*/
           :old.CONSIGNACAO_DEVOLUCAO,  /*28*/
           :new.CONSIGNACAO_DEVOLUCAO  /*29*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into LOJA_060_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           DOCUMENTO_OLD, /*8*/
           DOCUMENTO_NEW, /*9*/
           CNPJ9_OLD, /*10*/
           CNPJ9_NEW, /*11*/
           CNPJ4_OLD, /*12*/
           CNPJ4_NEW, /*13*/
           CNPJ2_OLD, /*14*/
           CNPJ2_NEW, /*15*/
           DATA_EMISSAO_OLD, /*16*/
           DATA_EMISSAO_NEW, /*17*/
           SITUACAO_OLD, /*18*/
           SITUACAO_NEW, /*19*/
           COL_TABELA_OLD, /*20*/
           COL_TABELA_NEW, /*21*/
           MES_TABELA_OLD, /*22*/
           MES_TABELA_NEW, /*23*/
           SEQ_TABELA_OLD, /*24*/
           SEQ_TABELA_NEW, /*25*/
           CODIGO_EMPRESA_OLD, /*26*/
           CODIGO_EMPRESA_NEW, /*27*/
           CONSIGNACAO_DEVOLUCAO_OLD, /*28*/
           CONSIGNACAO_DEVOLUCAO_NEW /*29*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.DOCUMENTO, /*8*/
           0, /*9*/
           :old.CNPJ9, /*10*/
           0, /*11*/
           :old.CNPJ4, /*12*/
           0, /*13*/
           :old.CNPJ2, /*14*/
           0, /*15*/
           :old.DATA_EMISSAO, /*16*/
           null, /*17*/
           :old.SITUACAO, /*18*/
           0, /*19*/
           :old.COL_TABELA, /*20*/
           0, /*21*/
           :old.MES_TABELA, /*22*/
           0, /*23*/
           :old.SEQ_TABELA, /*24*/
           0, /*25*/
           :old.CODIGO_EMPRESA, /*26*/
           0, /*27*/
           :old.CONSIGNACAO_DEVOLUCAO, /*28*/
           0 /*29*/
         );
    end;
 end if;
end inter_tr_LOJA_060_log;

-- ALTER TRIGGER "INTER_TR_LOJA_060_LOG" ENABLE
 

/

exec inter_pr_recompile;

