
  CREATE OR REPLACE PROCEDURE "INTER_PR_BAIXA_PARADA_MAQUINA" (grupo_maquina in varchar2, subgrupo_maquina in varchar2, numero_maquina in number,
                                                           data_baixa in date, hora_baixa in date) is
   ws_usuario_rede                       varchar2(20);
   ws_maquina_rede                       varchar2(40);
   ws_aplicativo                         varchar2(20);
   ws_sid                                number(9);
   ws_empresa                            number(3);
   ws_usuario_systextil                  varchar2(250);
   ws_locale_usuario                     varchar2(5);

   finaliza_parada_auto                  number(3);
   v_existe_reg                          number(3);
   v_data_parada                         date;
   v_hora_parada                         date;
   v_minutos                             number(9);
begin

   begin
      select empr_002.finaliza_parada_auto
      into finaliza_parada_auto
      from empr_002;
   exception when others then
      finaliza_parada_auto := 0;
   end;

   if finaliza_parada_auto = 1
   then
   --   raise_application_error(-20000,to_char(hora_baixa,'hh24:mi'));
      begin
         select 1,           efic_090.data_parada, efic_090.hora_inicio
         into v_existe_reg,  v_data_parada,        v_hora_parada
         from efic_090
         where efic_090.maquina_maq_sub_grupo_mq = grupo_maquina
           and efic_090.maquina_maq_sub_sbgr_maq = subgrupo_maquina
           and efic_090.maquina_nr_maq           = numero_maquina
           and efic_090.data_final               is null
           and (efic_090.data_parada              < data_baixa
            or  to_char(efic_090.hora_inicio,'hh24:mi') < to_char(hora_baixa,'hh24:mi'));
      exception when others then
         v_existe_reg := 0;
      end;
--              raise_application_error(-20000,v_existe_reg);
      if (v_existe_reg > 0)
      then
         v_minutos := inter_fn_calcula_minutos(data_baixa, hora_baixa, v_data_parada, v_hora_parada);
         BEGIN
            update efic_090
            set efic_090.data_final = data_baixa,
                efic_090.hora_final = hora_baixa,
                efic_090.minutos_parada = v_minutos
            where efic_090.maquina_maq_sub_grupo_mq = grupo_maquina
              and efic_090.maquina_maq_sub_sbgr_maq = subgrupo_maquina
              and efic_090.maquina_nr_maq           = numero_maquina
              and efic_090.data_final               is null
              and (efic_090.data_parada              < data_baixa
               or  to_char(efic_090.hora_inicio,'hh24:mi') < to_char(hora_baixa,'hh24:mi'));
         exception when others then
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'EFIC_090' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
         end;
      end if;
   end if;
end;

 

/

exec inter_pr_recompile;

