
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_015" 
  before insert or
  update of ordem_producao, seq_operacao, codigo_operacao, motivo_rejeicao,
	data_digitacao, hora_digitacao, grupo_maquina, subgrupo_maquina,
	numero_maquina, codigo_estagio, operador_inicio, data_inicio,
	hora_inicio, operador_termino, data_termino, hora_termino,
	minutos_unitario, ordem_reprocesso, prioridade_producao, observacao
  or delete on pcpb_015
  for each row

declare
--   Pragma Autonomous_Transaction;

   -- declaracao de variaveis
   v_gru_maq_menor_seq   varchar2(4);
   v_sub_maq_menor_seq   varchar2(3);
   v_cor_maq_vez         varchar2(15);
   v_menor_seq           number(9);
   v_ordem_producao      number(9);
   v_seq_atual           number(9);
   v_ordem_tingimento    number(9);
   v_pedido_venda        pcpb_030.nr_pedido_ordem%type;
   v_registros_pcpb_018  number;
   v_registros_pcpb_040  number;
   data_parada date;
   hora_parada date;
   v_controle number;

begin
   if (updating and (:old.data_inicio  is not null and :new.data_inicio  is null)
                 or (:old.hora_inicio  is not null and :new.hora_inicio  is null)
                 or (:old.data_termino is not null and :new.data_termino is null)
                 or (:old.hora_termino is not null and :new.hora_termino is null))
   then
      begin
         v_registros_pcpb_040 := 0;

         select count(*)
         into v_registros_pcpb_040
         from pcpb_040
         where pcpb_040.ordem_producao  = :new.ordem_producao
           and pcpb_040.seq_operacao    > :new.seq_operacao
           and pcpb_040.data_termino is not null;

         if v_registros_pcpb_040 > 0
         then
            raise_application_error(-20000,chr(10)||chr(10)||'ATENCAO! Nao pode voltar a situacao da operacao quando estagios/operacoes posteriores estiverem produzidas. '||chr(10)||'Consulte os estagios apontados atraves do programa de Baixa dos Estagios de Beneficiamento(pcpb_f070). '||chr(10)||chr(10));
         end if;
      end;
   end if; -- fim do if principal

   -- ira fazer algo se for
   if ((updating and :new.data_inicio  is null and :old.data_inicio is not null) or
       (updating and :new.data_termino is null and :old.data_termino is not null))
   then
      -- acerta data/hora da tabela dos estagios da ordem de producao pcpb_040.
      update pcpb_040
         set pcpb_040.data_inicio  = :new.data_inicio,
             pcpb_040.hora_inicio  = :new.hora_inicio,
             pcpb_040.data_termino = :new.data_termino,
             pcpb_040.hora_termino = :new.hora_termino
      where  pcpb_040.ordem_producao = :new.ordem_producao
        and  pcpb_040.seq_operacao   = :new.seq_operacao;
   end if; -- fim do if principal
   -- FINAL - CONTROLE DE APONTAMENTOS DE PRODUCAO (DATA_INICIO, DATA_TERMINO)



   -- ira fazer algo se for
   if ((updating and :new.data_termino is not null and :old.data_termino is null)     or
       (updating and :new.data_termino is null     and :old.data_termino is not null) or
        inserting or deleting)
   then
      -- inicializa as variaveis
      v_gru_maq_menor_seq := ' ';
      v_sub_maq_menor_seq := ' ';
      v_cor_maq_vez       := '0';
      v_menor_seq         := 999999999;
      v_ordem_producao    := 0;
      v_seq_atual         := 0;

      -- if para a insercao de um registro da pcpb_015 e para um update aonde a data_termino
      -- nao era nula e agora sera, indicando que a operacao sera reaberta.
      if ((inserting and :new.data_termino is null) or (updating and :new.data_termino is null and :old.data_termino is not null))
      then
         v_ordem_producao    := :new.ordem_producao;
         v_seq_atual         := :new.seq_operacao;

         -- seleciona a menor sequencia da ob em questao com a data_termino nula
         select min(pcpb_201.seq_operacao) into v_menor_seq
         from pcpb_201
         where pcpb_201.ordem_producao    = v_ordem_producao
           and not (pcpb_201.seq_operacao = v_seq_atual and pcpb_201.codigo_operacao = :new.codigo_operacao);

         -- se nao encontrar registro inicializara com 99999999
         if v_menor_seq is null
         then
            v_menor_seq := 999999999;
         end if;

         -- se a sequenica que esta sendo inserida e menor que todas que estao na pcpb_201
         -- entao a cor da maquina vai ser a cor da propria maquina
         if v_seq_atual < v_menor_seq
         then
            v_menor_seq         := v_seq_atual;
            v_gru_maq_menor_seq := :new.grupo_maquina;
            v_sub_maq_menor_seq := :new.subgrupo_maquina;
            -- encontra a cor representativa da maquina que esta sendo inserindo
            begin
               select mqop_020.cor_representativa
               into   v_cor_maq_vez
               from  mqop_020
               where mqop_020.grupo_maquina    = :new.grupo_maquina
                 and mqop_020.subgrupo_maquina = :new.subgrupo_maquina;
            exception
               when no_data_found then
                  v_cor_maq_vez       := '0';
            end;

            -- atualiza todas as maquinas com a nova cor representativa
            update pcpb_201
               set pcpb_201.cor_maq_menor_seq = v_cor_maq_vez,
                   pcpb_201.gru_maq_menor_seq = v_gru_maq_menor_seq,
                   pcpb_201.sub_maq_menor_seq = v_sub_maq_menor_seq
            where  pcpb_201.ordem_producao    = v_ordem_producao;
         else
            -- busca a cor representativa da maquina da menor sequencia
            begin
               v_cor_maq_vez       := '0';
               v_gru_maq_menor_seq := ' ';
               v_sub_maq_menor_seq := ' ';

               for reg1 in (select mqop_020.cor_representativa, mqop_020.grupo_maquina,
                                   mqop_020.subgrupo_maquina
                            from  pcpb_201, mqop_020
                            where pcpb_201.ordem_producao   = v_ordem_producao
                              and pcpb_201.seq_operacao     = v_menor_seq
                              and pcpb_201.grupo_maquina    = mqop_020.grupo_maquina
                              and pcpb_201.subgrupo_maquina = mqop_020.subgrupo_maquina
                            order by pcpb_201.ordem_producao, pcpb_201.seq_operacao asc,
                                     mqop_020.cor_representativa desc)
               loop
                  v_cor_maq_vez       := reg1.cor_representativa;
                  v_gru_maq_menor_seq := reg1.grupo_maquina;
                  v_sub_maq_menor_seq := reg1.subgrupo_maquina;
                  exit;
               end loop;
            end;
         end if;

         -- encontra o pedido e a ordem de timgimento da ob
         begin
            select c.ordem_tingimento, a.nr_pedido_ordem
	          into   v_ordem_tingimento, v_pedido_venda
	          from pcpb_030 a, pedi_100 b, (select min(pedi_100.data_entr_venda) menor_data
	                                        from pedi_100, pcpb_030
	                                        where pedi_100.pedido_venda   = pcpb_030.nr_pedido_ordem
	                                          and pcpb_030.ordem_producao = :new.ordem_producao
	                                          and pcpb_030.pedido_corte   = 3) pedi_menor_data, pcpb_010 c
       	    where a.ordem_producao  = c.ordem_producao
	            and a.ordem_producao  = :new.ordem_producao
	            and a.pedido_corte    = 3
	            and a.nr_pedido_ordem = b.pedido_venda
	            and b.data_entr_venda = pedi_menor_data.menor_data
	            and rownum            = 1;
	       exception
	          when no_data_found then
	             v_ordem_tingimento := 0;
	             v_pedido_venda     := 0;
         end;

         begin
           -- insere os dados na tabela pcpb_201
           insert into pcpb_201
             (ordem_producao,                seq_operacao,
              codigo_operacao,               grupo_maquina,
              subgrupo_maquina,              numero_maquina,
              codigo_estagio,                operador_inicio,
              minutos_unitario,              prioridade_producao,
              gru_maq_menor_seq,             sub_maq_menor_seq,
              cor_maq_menor_seq,             pedido_venda,
              ordem_tingimento)
           values
             (:new.ordem_producao,           :new.seq_operacao,
              :new.codigo_operacao,          :new.grupo_maquina,
              :new.subgrupo_maquina,         :new.numero_maquina,
              :new.codigo_estagio,           :new.operador_inicio,
              :new.minutos_unitario,         :new.prioridade_producao,
              v_gru_maq_menor_seq,           v_sub_maq_menor_seq,
              v_cor_maq_vez,                 v_pedido_venda,
              v_ordem_tingimento);
         exception       
            WHEN DUP_VAL_ON_INDEX THEN
            -- variavel sem uso, apenas para controle
            v_controle := 1;
         end;

      end if; --fim do if de inserting

      -- #######################################################################

      -- if para a delecao de um registro da pcpb_015 e para um update aonde a data_termino
      -- era nula e agora nao sera, indicando que a operacao sera finalizada.
      if (deleting or (updating and :new.data_termino is not null and :old.data_termino is null))
      then
         v_ordem_producao    := :old.ordem_producao;
         v_seq_atual         := :old.seq_operacao;

         -- seleciona a menor sequencia da ob em questao com a data_termino nula
         select min(pcpb_201.seq_operacao) into v_menor_seq
         from pcpb_201
         where pcpb_201.ordem_producao    = v_ordem_producao
           and not (pcpb_201.seq_operacao = v_seq_atual and pcpb_201.codigo_operacao = :old.codigo_operacao);
         -- caso nao encontre nenhuma sera inicializada com 999999999
         if v_menor_seq is null
         then
            v_menor_seq := 999999999;
         end if;
         -- se a sequenica que esta sendo deletada e menor que menor sequencia que esta
         -- na pcpb_201 entao a cor da maquina vai ser a cor da menor sequencia diferente
         -- da sequencia que esta sendo eliminada.
         if v_seq_atual < v_menor_seq
         then
            begin
               v_cor_maq_vez       := '0';
               v_gru_maq_menor_seq := ' ';
               v_sub_maq_menor_seq := ' ';

               for reg1 in (select mqop_020.cor_representativa, mqop_020.grupo_maquina,
                                   mqop_020.subgrupo_maquina
                            from  pcpb_201, mqop_020
                            where pcpb_201.ordem_producao   = v_ordem_producao
                              and pcpb_201.seq_operacao     = v_menor_seq
                              and pcpb_201.grupo_maquina    = mqop_020.grupo_maquina
                              and pcpb_201.subgrupo_maquina = mqop_020.subgrupo_maquina
                            order by pcpb_201.ordem_producao, pcpb_201.seq_operacao asc,
                                     mqop_020.cor_representativa desc)
               loop
                  v_cor_maq_vez       := reg1.cor_representativa;
                  v_gru_maq_menor_seq := reg1.grupo_maquina;
                  v_sub_maq_menor_seq := reg1.subgrupo_maquina;
                  exit;
               end loop;
            end;
         end if;

--raise_application_error(-20000,chr(10)||chr(10)||'ACOMPANHANDO  '||chr(10)||chr(10));

         -- esta eliminando a operacao da pcpb_201 pois a operacao esta sendo eliminada na pcpb_015
         delete pcpb_201
         where pcpb_201.ordem_producao = v_ordem_producao
           and pcpb_201.seq_operacao   = :old.seq_operacao
           and pcpb_201.codigo_operacao= :old.codigo_operacao;

         if v_seq_atual < v_menor_seq
         then
            -- set for update ou delete atualiza a cor representativa e a maquina paa a ordem de produc?.
            update pcpb_201
               set pcpb_201.cor_maq_menor_seq = v_cor_maq_vez,
                   pcpb_201.gru_maq_menor_seq = v_gru_maq_menor_seq,
                   pcpb_201.sub_maq_menor_seq = v_sub_maq_menor_seq
            where  pcpb_201.ordem_producao    = v_ordem_producao;
         end if;
      end if; -- fim do if deleting
   end if; -- fim do if principal

   if (updating and :new.prioridade_producao <> :old.prioridade_producao)
   then
      -- acerta a prioridade de producao da pcpb_201.
      update pcpb_201
         set pcpb_201.prioridade_producao = :new.prioridade_producao
      where  pcpb_201.ordem_producao      = :new.ordem_producao
        and  pcpb_201.seq_operacao        = :new.seq_operacao
        and  pcpb_201.codigo_operacao     = :new.codigo_operacao;
   end if;

   if updating and (:new.grupo_maquina    <> :old.grupo_maquina or
                    :new.subgrupo_maquina <> :old.subgrupo_maquina)
   then
      -- acerta a maquina na pcpb_201.
      update pcpb_201
         set pcpb_201.grupo_maquina    = :new.grupo_maquina,
             pcpb_201.subgrupo_maquina = :new.subgrupo_maquina
      where  pcpb_201.ordem_producao   = :new.ordem_producao
        and  pcpb_201.seq_operacao     = :new.seq_operacao
        and  pcpb_201.codigo_operacao  = :new.codigo_operacao;
   end if;

   -- Logica para gravar a hora temino e hora inicio com uma data valida.
   -- A data definida '16/11/1989', foi definida com base da data de gravacao padrao do Vision.
   if (updating  and :new.hora_inicio is not null)
   or (inserting and :new.hora_inicio is not null)
   then
      :new.hora_inicio := to_date('16/11/1989 '||to_char(:new.hora_inicio,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.hora_termino is not null)
   or (inserting and :new.hora_termino is not null)
   then
      :new.hora_termino := to_date('16/11/1989 '||to_char(:new.hora_termino,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and (:new.data_inicio  is not null or :new.data_termino is not null))
   or (inserting and (:new.data_inicio  is not null or :new.data_termino is not null))
   then
      if (:new.data_inicio  is not null and :old.data_inicio is null)
      then
         data_parada := :new.data_inicio;
         select to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || to_char(:new.hora_inicio,'hh24:mi'),'dd/mm/yyyy hh24:mi')
         into hora_parada
         from dual;

      else
         data_parada := :new.data_termino;
         select to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || to_char(:new.hora_termino,'hh24:mi'),'dd/mm/yyyy hh24:mi')
         into hora_parada
         from dual;

      end if;

      inter_pr_baixa_parada_maquina(:new.grupo_maquina, :new.subgrupo_maquina, :new.numero_maquina, data_parada, hora_parada);
   end if;
   -- Elimina pcpb_018
   if deleting
   then
      begin
         delete from pcpb_018
         where pcpb_018.ordem_producao  = :old.ordem_producao
           and pcpb_018.seq_operacao    = :old.seq_operacao
           and pcpb_018.codigo_operacao = :old.codigo_operacao;
      end;
   end if;

   if inserting
   then
      begin

         v_registros_pcpb_018 := 0;

         select count(*)
         into v_registros_pcpb_018
         from pcpb_018
         where pcpb_018.ordem_producao  = v_ordem_producao
           and pcpb_018.seq_operacao    = :new.seq_operacao
           and pcpb_018.codigo_operacao = :new.codigo_operacao;

        if v_registros_pcpb_018 = 0
        then
           for reg2 in (select pcpb_020.sequencia, pcpb_020.qtde_quilos_prog
                        from pcpb_020
                        where pcpb_020.ordem_producao = v_ordem_producao)
           loop

              v_registros_pcpb_018 := 0;

              select count(*)
              into v_registros_pcpb_018
              from pcpb_018
              where pcpb_018.ordem_producao  = v_ordem_producao
                and pcpb_018.seq_operacao    = :new.seq_operacao
                and pcpb_018.codigo_operacao = :new.codigo_operacao
                and pcpb_018.seq_tecido      = reg2.sequencia;

              begin
                 if v_registros_pcpb_018 = 0
                 then
                    insert into pcpb_018
                       (pcpb_018.ordem_producao, pcpb_018.seq_tecido,
                        pcpb_018.seq_operacao,   pcpb_018.codigo_operacao,
                        pcpb_018.quantidade,     pcpb_018.minutos
                       )
                    values
                       (v_ordem_producao,        reg2.sequencia,
                        :new.seq_operacao,       :new.codigo_operacao,
                        reg2.qtde_quilos_prog,   0.000
                       );
                 end if;
              end;
           end loop;
        end if;
      end;
   end if;

end inter_tr_pcpb_015;

-- ALTER TRIGGER "INTER_TR_PCPB_015" ENABLE
 

/

exec inter_pr_recompile;

