CREATE OR REPLACE VIEW INTER_VI_PCPC_020_040
(
  PERIODO_PRODUCAO,
  ORDEM_PRODUCAO,
  REFERENCIA_PECA,
  SIT_REQ_TECIDOS,
  PRIORIDADE_PRODU,
  ORDEM_PRINCIPAL,
  QTDE_APRODUZIR
)
AS 
select pcpc_020.periodo_producao,
       pcpc_020.ordem_producao,
       pcpc_020.referencia_peca,
       pcpc_020.sit_req_tecidos,
       pcpc_020.prioridade_produ,
       pcpc_020.ordem_principal,
       sum(pcpc_040.qtde_a_produzir_pacote) qtde_aproduzir
  from pcpc_020, pcpc_040, fatu_503, pcpc_010
 where pcpc_040.ordem_producao   = pcpc_020.ordem_producao
   and pcpc_040.codigo_estagio in (fatu_503.estagio_preparacao_ordem)
   and fatu_503.codigo_empresa = pcpc_010.codigo_empresa
   and pcpc_010.area_periodo = 1
   and pcpc_010.periodo_producao = pcpc_040.periodo_producao
   and pcpc_040.qtde_a_produzir_pacote > 0
   and not exists (select 1 from pcpc_040 p
                   where p.ordem_producao = pcpc_040.ordem_producao
                     and p.codigo_estagio = pcpc_040.estagio_anterior
                     and p.qtde_a_produzir_pacote > 0
                     and rownum = 1)
   and pcpc_020.cod_cancelamento = 0
group by pcpc_020.periodo_producao,
       pcpc_020.ordem_producao,        pcpc_020.referencia_peca,
       pcpc_020.sit_req_tecidos,       pcpc_020.prioridade_produ,
       pcpc_020.ordem_principal;
