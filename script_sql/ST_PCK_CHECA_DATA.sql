create or replace package ST_PCK_CHECA_DATA as

   -- Definição do Tipo
    TYPE t_regras IS record (
        validarPorContabCardex number,
        validarExercicio number,
        validarPeriodo number
    );

    -- Tabela Associativa para Armazenar as Exceções
    TYPE excecoes_table IS TABLE OF t_regras INDEX BY VARCHAR2(50);

    -- Função para Obter Regras
    FUNCTION get_regras(p_nome_form VARCHAR2) RETURN t_regras;

    -- Procedimento para Inicializar Exceções
    PROCEDURE inicializa_excecoes;

    -- Variável para Regras Padrão
    REGRAS_PADRAO t_regras;

    -- Variável para Exceções
    EXCECOES excecoes_table;
   
    FUNCTION executar(p_nome_form VARCHAR2, p_cod_empresa NUMBER, p_data_lac DATE, p_transacao NUMBER) RETURN number;

end "ST_PCK_CHECA_DATA";
