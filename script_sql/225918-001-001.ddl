declare
    v_id_grid    number;
    v_id_column  number;
	
    cursor profiles (p_id_grid number) is
        select id, (select max(position) + 1 from conf_grid_perfil_column
                    where id_grid_perfil = conf_grid_perfil.id) max_position_column from conf_grid_perfil
        where id_grid = p_id_grid;

	profile_row profiles%rowtype;
	
    procedure insert_column(p_id number,
                            p_id_grid number,
                            p_key varchar2,
                            p_name varchar2,
                            p_type varchar2,
                            p_formatter_index number) is
    begin
        insert into conf_grid_column (id, id_grid, key, name, type, formatter_index)
        values (p_id, p_id_grid, p_key, p_name, p_type, p_formatter_index);
    end;

    procedure insert_column_profile(p_id_grid_perfil number,
                                    p_id_grid_column number,
                                    p_position number) is
    begin
        insert into conf_grid_perfil_column (id_grid_perfil, id_grid_column, position)
        values (p_id_grid_perfil, p_id_grid_column, p_position);
    end;
begin
    select id
    into v_id_grid from conf_grid
    where nome = 'PedidosDeOrigem';
	
    v_id_column := id_conf_grid_column.nextval;
    insert_column(v_id_column, v_id_grid, 'perc_comis_venda', 'Perc comiss',
                  'NUMBER', 1);
				  
    OPEN profiles(v_id_grid); LOOP
		FETCH profiles into profile_row;
		EXIT WHEN profiles%NOTFOUND;
		insert_column_profile(profile_row.ID, v_id_column, profile_row.max_position_column);
	END LOOP;
	CLOSE profiles;
end;
