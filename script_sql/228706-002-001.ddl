create table SPED_K200_H010
(
  EMPRESA             NUMBER(3) not null,
  MES                 NUMBER(2) not null,
  ANO                 NUMBER(4) not null,
  CNPJ9_PARTICIPANTE  NUMBER(9) not null,
  CNPJ4_PARTICIPANTE  NUMBER(4) not null,
  CNPJ2_PARTICIPANTE  NUMBER(2) not null,
  NIVEL               VARCHAR2(1) not null,
  GRUPO               VARCHAR2(5) not null,
  SUBGRUPO            VARCHAR2(3) not null,
  ITEM                VARCHAR2(6) not null,
  ESTAGIO_AGRUPADOR   NUMBER(3) not null,
  ESTAGIO_AGRUPADOR_SIMULTANEO NUMBER(3) not null,
  QUANTIDADE          NUMBER(17,3) not null,
  VALOR_UNITARIO      NUMBER(17,5) not null,
  VALOR_TOTAL         NUMBER(17,2) not null,
  TIPO_PROPRIEDADE    NUMBER(1) not null,
  CODIGO_CONTABIL     NUMBER(6) not null,
  UNIDADE_MEDIDA      VARCHAR2(6) not null,
  VALOR_IR            NUMBER(17,5) not null,
  CODIGO_DEPOSITO     NUMBER(3) not null,
  TIPO_PRODUTO_SPED   NUMBER(2) not null
);

alter table SPED_K200_H010
  add constraint SPED_K200_H010_PK primary key (
    EMPRESA,
    MES,
    ANO,
    CNPJ9_PARTICIPANTE,
    CNPJ4_PARTICIPANTE,
    CNPJ2_PARTICIPANTE,
    NIVEL,
    GRUPO,
    SUBGRUPO,
    ITEM,
    ESTAGIO_AGRUPADOR,
    ESTAGIO_AGRUPADOR_SIMULTANEO,
    CODIGO_CONTABIL,
    CODIGO_DEPOSITO);

