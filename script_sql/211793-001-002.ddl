-- pcpb_f420

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpb_f420', 'Entrada de rolos via balan�a - pl�stico', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpb_f420', 'pcpb_menu', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpb_f420', 'pcpb_menu', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Entrada de rolos via balan�a - pl�stico'
 where hdoc_036.codigo_programa = 'pcpb_f420'
   and hdoc_036.locale          = 'es_ES';
commit;

-- pcpb_f421

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpb_f421', 'Par�metros de configura��es para o processo de pl�stico', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpb_f421', 'pcpb_menu', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpb_f421', 'pcpb_menu', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Par�metros de configura��es para o processo de pl�stico'
 where hdoc_036.codigo_programa = 'pcpb_f421'
   and hdoc_036.locale          = 'es_ES';
commit;
