create or replace trigger inter_tr_supr_100_2
   before delete or
          update of cod_cancelamento, qtde_pedida_item
   on supr_100
   for each row

declare
   Pragma Autonomous_Transaction;
   
   v_qtde_prod_pedido   number;
   v_qtde_areceber_prog number;
   v_executa_trigger    number;
   v_ordem_planej_old   number;
   v_qtde_630           number;

begin
   -- L�gica de controle para execu��o ou n�o da trigger, referente ao
   -- processo de limpeza de dados.
   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      -- Esta trigger far� executar� com base no pedido de compra a atualiza��o
      -- das quantidades programadas na ordem de planejamento (tmrp_630).
      -- Estas atualiza��es ir�o acontecer quando cancelado um pedido ou excluido
      -- por este motivo n�o temos INSERT.
      if updating and ((:new.cod_cancelamento <> 0 and :new.cod_cancelamento <> :old.cod_cancelamento) or
                       :new.qtde_pedida_item <> :old.qtde_pedida_item)
      then
         v_qtde_prod_pedido := 0.00;
         v_ordem_planej_old := 0;

         for reg_tmrp in (select tmrp_630.ordem_planejamento,            tmrp_630.pedido_venda,
                                 tmrp_630.nivel_produto,                 tmrp_630.grupo_produto,
                                 tmrp_630.subgrupo_produto,              tmrp_630.item_produto,
                                 tmrp_625.seq_produto_origem,            tmrp_625.nivel_produto_origem,
                                 tmrp_625.grupo_produto_origem,          tmrp_625.subgrupo_produto_origem,
                                 tmrp_625.item_produto_origem,           tmrp_625.alternativa_produto_origem,
                                 tmrp_625.seq_produto,                   tmrp_625.qtde_areceber_programada
                          from tmrp_630, tmrp_625
                          where tmrp_625.ordem_planejamento = tmrp_630.ordem_planejamento
                            and tmrp_625.pedido_venda       = tmrp_630.pedido_venda
                            and tmrp_625.nivel_produto      = tmrp_630.nivel_produto
                            and tmrp_625.grupo_produto      = tmrp_630.grupo_produto
                            and tmrp_625.subgrupo_produto   = tmrp_630.subgrupo_produto
                            and tmrp_625.item_produto       = tmrp_630.item_produto
                            and tmrp_630.area_producao      = 9 --Compras
                            and tmrp_630.ordem_prod_compra  = :new.num_ped_compra)
         loop
            begin
               select sum(supr_100.qtde_pedida_item)
               into   v_qtde_prod_pedido
               from supr_100
               where supr_100.num_ped_compra    = :new.num_ped_compra
                 and supr_100.item_100_nivel99  = reg_tmrp.nivel_produto
                 and supr_100.item_100_grupo    = reg_tmrp.grupo_produto
                 and supr_100.item_100_subgrupo = reg_tmrp.subgrupo_produto
                 and supr_100.item_100_item     = reg_tmrp.item_produto;
              exception
              when OTHERS then
               v_qtde_prod_pedido := 0.0;
            end;

            v_qtde_areceber_prog := reg_tmrp.qtde_areceber_programada - v_qtde_prod_pedido;

            if v_qtde_areceber_prog < 0
            then
               v_qtde_prod_pedido := reg_tmrp.qtde_areceber_programada;
            end if;

            inter_pr_atu_planej_reves( reg_tmrp.ordem_planejamento,
                                       reg_tmrp.pedido_venda,            reg_tmrp.nivel_produto,
                                       reg_tmrp.grupo_produto,           reg_tmrp.subgrupo_produto,
                                       reg_tmrp.item_produto,            v_qtde_prod_pedido,
                                       0.00,
                                       reg_tmrp.nivel_produto_origem,    reg_tmrp.grupo_produto_origem,
                                       reg_tmrp.subgrupo_produto_origem, reg_tmrp.item_produto_origem,
                                       reg_tmrp.seq_produto_origem,      reg_tmrp.alternativa_produto_origem,
                                       reg_tmrp.seq_produto,             'supr_100',
                                       0);
                                       
            if v_ordem_planej_old <> reg_tmrp.ordem_planejamento
            then
               v_ordem_planej_old := reg_tmrp.ordem_planejamento;
   
               begin
                  select tmrp_630.programado_comprado
                  into v_qtde_630
                  from tmrp_630
                  where tmrp_630.ordem_planejamento = reg_tmrp.ordem_planejamento
                    and tmrp_630.area_producao      = 9
                    and tmrp_630.ordem_prod_compra  = :new.num_ped_compra
                    and tmrp_630.nivel_produto      = :new.item_100_nivel99
                    and tmrp_630.grupo_produto      = :new.item_100_grupo
                    and tmrp_630.subgrupo_produto   = :new.item_100_subgrupo
                    and tmrp_630.item_produto       = :new.item_100_item;
               exception
               when OTHERS then
                  v_qtde_630 := 0;
               end;
               
               if (v_qtde_630 - :new.qtde_pedida_item) <= 0.000000
               then
                  begin
                     delete tmrp_630
                     where tmrp_630.ordem_planejamento = reg_tmrp.ordem_planejamento
                       and tmrp_630.area_producao      = 9
                       and tmrp_630.ordem_prod_compra  = :new.num_ped_compra
                       and tmrp_630.nivel_produto      = :new.item_100_nivel99
                       and tmrp_630.grupo_produto      = :new.item_100_grupo
                       and tmrp_630.subgrupo_produto   = :new.item_100_subgrupo
                       and tmrp_630.item_produto       = :new.item_100_item;
                  exception
                  when OTHERS then
                     raise_application_error(-20000,'ATEN��O! N�o eliminou tmrp_630 (updating).');
                  end;
               else
                  begin
                     update tmrp_630
                        set tmrp_630.programado_comprado = tmrp_630.programado_comprado - :new.qtde_pedida_item
                     where tmrp_630.ordem_planejamento = reg_tmrp.ordem_planejamento
                       and tmrp_630.area_producao      = 9
                       and tmrp_630.ordem_prod_compra  = :new.num_ped_compra
                       and tmrp_630.nivel_produto      = :new.item_100_nivel99
                       and tmrp_630.grupo_produto      = :new.item_100_grupo
                       and tmrp_630.subgrupo_produto   = :new.item_100_subgrupo
                       and tmrp_630.item_produto       = :new.item_100_item;
                  exception
                  when OTHERS then
                     raise_application_error(-20000,'ATEN��O! N�o atualizou tmrp_630 (updating).');
                  end; 
               end if;
            end if;
         end loop;
      end if;

      if deleting
      then
         v_qtde_prod_pedido := 0.00;
         v_ordem_planej_old := 0;
         v_qtde_areceber_prog := 0.00;
         
         for reg_tmrp in (select tmrp_630.ordem_planejamento,            tmrp_630.pedido_venda,
                                 tmrp_630.nivel_produto,                 tmrp_630.grupo_produto,
                                 tmrp_630.subgrupo_produto,              tmrp_630.item_produto,
                                 tmrp_625.seq_produto_origem,            tmrp_625.nivel_produto_origem,
                                 tmrp_625.grupo_produto_origem,          tmrp_625.subgrupo_produto_origem,
                                 tmrp_625.item_produto_origem,           tmrp_625.alternativa_produto_origem,
                                 tmrp_625.seq_produto,                   tmrp_625.qtde_areceber_programada
                          from tmrp_630, tmrp_625
                          where tmrp_625.ordem_planejamento = tmrp_630.ordem_planejamento
                            and tmrp_625.pedido_venda       = tmrp_630.pedido_venda
                            and tmrp_625.nivel_produto      = tmrp_630.nivel_produto
                            and tmrp_625.grupo_produto      = tmrp_630.grupo_produto
                            and tmrp_625.subgrupo_produto   = tmrp_630.subgrupo_produto
                            and tmrp_625.item_produto       = tmrp_630.item_produto
                            and tmrp_630.area_producao      = 9 --Compras
                            and tmrp_630.ordem_prod_compra  = :old.num_ped_compra)
         loop
            begin
               select nvl(sum(supr_100.qtde_pedida_item),0)
               into   v_qtde_prod_pedido
               from supr_100
               where supr_100.num_ped_compra    = :old.num_ped_compra
                 and supr_100.seq_item_pedido   = :old.seq_item_pedido
                 and supr_100.item_100_nivel99  = reg_tmrp.nivel_produto
                 and supr_100.item_100_grupo    = reg_tmrp.grupo_produto
                 and supr_100.item_100_subgrupo = reg_tmrp.subgrupo_produto
                 and supr_100.item_100_item     = reg_tmrp.item_produto;
            exception
               when OTHERS then
               v_qtde_prod_pedido := 0.000;
               
            end;
            
            begin
               select (reg_tmrp.qtde_areceber_programada - v_qtde_prod_pedido)
               into v_qtde_areceber_prog
               from dual;

               if v_qtde_areceber_prog < 0
               then
                  v_qtde_prod_pedido := reg_tmrp.qtde_areceber_programada;
               end if;
            end;
            
            inter_pr_atu_planej_reves( reg_tmrp.ordem_planejamento,
                                       reg_tmrp.pedido_venda,            reg_tmrp.nivel_produto,
                                       reg_tmrp.grupo_produto,           reg_tmrp.subgrupo_produto,
                                       reg_tmrp.item_produto,            v_qtde_prod_pedido,
                                       0.00,
                                       reg_tmrp.nivel_produto_origem,    reg_tmrp.grupo_produto_origem,
                                       reg_tmrp.subgrupo_produto_origem, reg_tmrp.item_produto_origem,
                                       reg_tmrp.seq_produto_origem,      reg_tmrp.alternativa_produto_origem,
                                       reg_tmrp.seq_produto,             'supr_100',
                                       0);
                                       
            if v_ordem_planej_old <> reg_tmrp.ordem_planejamento
            then
               v_ordem_planej_old := reg_tmrp.ordem_planejamento;
               begin
                  select tmrp_630.programado_comprado
                  into v_qtde_630
                  from tmrp_630
                  where tmrp_630.ordem_planejamento = reg_tmrp.ordem_planejamento
                    and tmrp_630.area_producao      = 9
                    and tmrp_630.ordem_prod_compra  = :old.num_ped_compra
                    and tmrp_630.nivel_produto      = :old.item_100_nivel99
                    and tmrp_630.grupo_produto      = :old.item_100_grupo
                    and tmrp_630.subgrupo_produto   = :old.item_100_subgrupo
                    and tmrp_630.item_produto       = :old.item_100_item;
               exception
               when OTHERS then
                  v_qtde_630 := 0;
               end;
               
               if (v_qtde_630 - :old.qtde_pedida_item) <= 0.000000
               then
                  begin
                     delete tmrp_630
                     where tmrp_630.ordem_planejamento = reg_tmrp.ordem_planejamento
                       and tmrp_630.area_producao      = 9
                       and tmrp_630.ordem_prod_compra  = :old.num_ped_compra
                       and tmrp_630.nivel_produto      = :old.item_100_nivel99
                       and tmrp_630.grupo_produto      = :old.item_100_grupo
                       and tmrp_630.subgrupo_produto   = :old.item_100_subgrupo
                       and tmrp_630.item_produto       = :old.item_100_item;
                  exception
                  when OTHERS then
                     raise_application_error(-20000,'ATEN��O! N�o deletou tmrp_630 (1)');
                  end;
               else
                  begin
                     update tmrp_630
                        set tmrp_630.programado_comprado = tmrp_630.programado_comprado - :old.qtde_pedida_item
                     where tmrp_630.ordem_planejamento = reg_tmrp.ordem_planejamento
                       and tmrp_630.area_producao      = 9
                       and tmrp_630.ordem_prod_compra  = :old.num_ped_compra
                       and tmrp_630.nivel_produto      = :old.item_100_nivel99
                       and tmrp_630.grupo_produto      = :old.item_100_grupo
                       and tmrp_630.subgrupo_produto   = :old.item_100_subgrupo
                       and tmrp_630.item_produto       = :old.item_100_item;
                  exception
                  when OTHERS then
                     raise_application_error(-20000,'ATEN��O! N�o atualizou tmrp_630 (1)');
                  end; 
               end if;
            end if;
         end loop;
      end if;
   end if;
   commit work;
end; -- Final da trigger da supr_090 x Planejamento de Produtos

/

execute inter_pr_recompile;
/* versao: 4 */
