CREATE OR REPLACE TRIGGER  BI_PCPB_260 
    before insert on PCPB_260               
    for each row  
begin   
    if :NEW.ID is null then 
        select PCPB_260_SEQ.nextval into :NEW.ID from sys.dual; 
    end if; 
end; 

/

ALTER TRIGGER  BI_PCPB_260 ENABLE;
