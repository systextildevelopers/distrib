
  CREATE OR REPLACE FUNCTION "INTER_FN_QTDE_SERV" (p_nr_rolo_origem   in number,
                                              p_qtde_quilos_acab in number)
return number is
    Pragma autonomous_transaction;

    v_qtde_quilos_acab      number;
    v_proporcao_rolo_orig   number;
begin
    begin
        select p_qtde_quilos_acab / sum(pcpt0202.qtde_quilos_acab)
        into   v_proporcao_rolo_orig
        from pcpt_020 pcpt0202
        where pcpt0202.nr_rolo_origem   = p_nr_rolo_origem
          and pcpt0202.qtde_quilos_acab > 0
        group by v_proporcao_rolo_orig;

        if v_proporcao_rolo_orig is null
        then v_proporcao_rolo_orig := 0.00;
        end if;

        select pcpt020.qtde_quilos_acab * v_proporcao_rolo_orig
        into v_qtde_quilos_acab
        from pcpt_020 pcpt020
        where pcpt020.codigo_rolo = p_nr_rolo_origem;
    end;

    if v_qtde_quilos_acab is null
    then v_qtde_quilos_acab := 0.00;
    end if;

    return v_qtde_quilos_acab;

end inter_fn_qtde_serv;

 

/

exec inter_pr_recompile;

