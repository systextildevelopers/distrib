CREATE OR REPLACE PROCEDURE INTER_PR_GERA_REEMBOLSO(p_numero_solicitacao  in number, p_empresa in number, p_programa in varchar2, p_usuario in varchar2
) is

   v_codigo_contabil number;
   v_codigo_transacao number;
   v_codigo_historico_cont number;
   v_posicao_titulo number;
   v_cod_portador number;
   v_codigo_historico_pgto number;
   v_conta_corrente number;
   v_tipo_tit_adiant number;
   
   v_cod_hist_pgto_oper_023 number;
   v_conta_corrente_oper_023 number;
   v_cod_port_oper_023 number;
   
   v_tipo_tit_oper_022 number; 
   v_origem_tit_oper_022 varchar2(100);
   v_cod_cont_oper_022 number;
   v_cod_tran_oper_022 number;
   v_cod_hist_cont_oper_022 number;
   v_pos_tit_oper_022 number;
   v_cod_port_oper_022 number;
   
   v_cod_port_oper_024 number;
   v_cod_cont_oper_024 number;
   v_cod_tran_oper_024 number;
   v_cod_hist_cont_oper_024 number;
   v_pos_tit_oper_024 number;
   v_nr_dias_ut_oper_024 number;
   v_tipo_tit_oper_024 number;
   
   v_codigo_contabil_cc number; 
   v_gera_contabil_cc number;
   
   v_codigo_contabil_cc_023 number; 
   v_gera_contabil_cc_023 number;

   v_atualiza_contabi_oper_021 number;
   v_atualiza_contabi_oper_022 number;
   v_atualiza_contabi_oper_024 number;

   r_num_lcto number;
   r_des_erro varchar2(4000);
   v_sequencia_insert number;
   v_adiantamento number;
   ok BOOLEAN;
   r_has_error BOOLEAN;
   

BEGIN

   -- ler tabela de parametros oper_021 - Transforma??o dos Adiantamentos em t?tulos
   BEGIN 
      select codigo_contabil, codigo_transacao, codigo_historico_cont, posicao_titulo, cod_portador, codigo_historico_pgto, conta_corrente
        into v_codigo_contabil, v_codigo_transacao, v_codigo_historico_cont, v_posicao_titulo, v_cod_portador, v_codigo_historico_pgto, v_conta_corrente
        from oper_021
        where cod_empresa = p_empresa
        and rownum = 1;
        
      EXCEPTION
          WHEN no_data_found THEN
            v_codigo_contabil := 0;
            v_codigo_transacao := 0;
            v_codigo_historico_cont := 0;
            v_posicao_titulo := 0;
            v_cod_portador := 0;
            v_codigo_historico_pgto := 0;
            v_conta_corrente := 0;
   END;

   v_atualiza_contabi_oper_021 := 1;
   
   -- Verifica se a contabilização está ativa para a transação da tabela oper_021
   BEGIN 
      select estq_005.atualiza_contabi into v_atualiza_contabi_oper_021 from estq_005 where estq_005.codigo_transacao = v_codigo_transacao;
   END;
   
    BEGIN     
         select f.tp_titul_adiantamento into v_tipo_tit_adiant from fatu_502 f
         where f.codigo_empresa = p_empresa
         and rownum = 1;
    EXCEPTION
        WHEN no_data_found THEN
        v_tipo_tit_adiant :=0;
    END;
        
    BEGIN
      select c.codigo_contabil, c.gera_contabil
        into v_codigo_contabil_cc, v_gera_contabil_cc
        from cpag_020 c
        where c.portador = v_cod_portador
        and c.conta_corrente = v_conta_corrente
        and rownum = 1;

      EXCEPTION
          WHEN no_data_found THEN
              v_codigo_contabil_cc := 0;
              v_gera_contabil_cc := 0;
   END;

   FOR adto in (
      select a.empresa,           a.numero_solicitacao, a.tipo_solicitacao, 
             a.situacao,          a.data_entrada,       a.data_processo, 
             a.cnpj9_cliente,     a.cnpj4_cliente,      a.cnpj2_cliente, 
             a.cod_repr_cliente, rep_nota_fiscal,    a.cod_centro_custo, 
             a.cod_banco,         a.agencia,            a.dig_agencia, 
             a.conta,             a.dig_conta,          a.cnpj9_favorecido, 
             a.cnpj4_favorecido,  a.cnpj2_favorecido,   a.num_nota_fiscal,
             a.serie_nota_fiscal, a.numero_adiantamento,a.valor,
             a.observacao,        a.motivo_rejeicao,    c.nome_cliente, 
             c.codigo_contabil    cod_contabil_cli,     c.cdrepres_cliente
      from cpag_787 a, pedi_010 c
      where a.numero_solicitacao = p_numero_solicitacao
      and a.empresa = p_empresa
      and a.cnpj9_cliente = c.cgc_9
      and a.cnpj4_cliente = c.cgc_4
      and a.cnpj2_cliente = c.cgc_2
      and a.situacao = 0
   )
   LOOP
       v_sequencia_insert := 1;
       
        r_has_error := false;
        r_num_lcto := 0;
        r_des_erro := NULL;
        
       if adto.tipo_solicitacao = 1 then
          -- gera adiantamento
         r_des_erro := ST_PCK_ADIANTAMENTO.cria_adiantamento_cf(
           adto.cnpj9_cliente,
           adto.cnpj4_cliente,
           adto.cnpj2_cliente,       
           adto.empresa, 
           adto.cod_centro_custo, 
           p_programa,
           adto.valor,      
           to_date(sysdate),   
           to_date(sysdate),   
           v_cod_portador,
           '0', --p_moeda 
           ' ',--p_num_importacao,  
           'SOLICITACAO DE REEMBOLSO', --P_origem_adiantam,
           v_adiantamento,
           v_tipo_tit_adiant,
           2 --p_tipo_adiantam
         );
   
        


        --contabiliza titulo a receber gerado pela transforma??o do adiantamento

        if v_atualiza_contabi_oper_021 = 1 then
            inter_pr_contabilizacao_prg_02(adto.empresa,
                                 adto.cod_centro_custo,
                                 adto.cnpj9_cliente,
                                 adto.cnpj4_cliente,
                                 adto.cnpj2_cliente,
                                 v_codigo_transacao,
                                 v_codigo_historico_cont,
                                 0,--v_conta_corrente,
                                 adto.cod_contabil_cli,
                                 v_codigo_contabil,
                                 v_adiantamento,
                                 to_date(sysdate),
                                 adto.valor,
                                 3, -- p_origem      titulo a receber
                                 1, -- p_tp_cta_deb       IN NUMBER
                                 5, -- p_tp_cta_cre       IN NUMBER
                                 p_programa,
                                 p_usuario,
                                 v_adiantamento || ' - ' || adto.nome_cliente,
                                 v_tipo_tit_adiant,
                                 r_num_lcto,
                                 r_des_erro);
        end if;
       
        if r_des_erro is not null 
        then
           rollback;
           raise_application_error(-20001, ' titulo a receber transformacao do adiantamento - ' || r_des_erro || Chr(10) || SQLERRM);
        end if;
       
        ok := false;

        while ok = false
        LOOP
              -- gera titulo a receber pela transformacao do adiantamento
            begin
                inter_pr_insere_fatu_070 (
                  adto.empresa,
                  adto.cnpj9_cliente,
                  adto.cnpj4_cliente,
                  adto.cnpj2_cliente,
                  v_tipo_tit_adiant,
                  v_adiantamento,
                  v_sequencia_insert,
                  to_date(sysdate),
                  adto.valor,
                  adto.cdrepres_cliente,
                  to_date(sysdate),
                  0, --p_numero_titulo
                  0, --v_sequencia_titulo
                  v_codigo_historico_cont,
                  adto.empresa,
                  0, --in fatu_070.port_anterior%type
                  v_cod_portador,
                  v_posicao_titulo,
                  to_date(sysdate),
                  '0', --in fatu_070.tecido_peca%type,
                  2,   --fatu_070.seq_end_cobranca%type
                  0,   --in fatu_070.comissao_lancada%type,
                  0,   --in fatu_070.tipo_tit_origem%type,
                  0,   --in fatu_070.num_dup_origem%type,
                  0,   --in fatu_070.seq_dup_origem%type,
                  adto.cnpj9_cliente,
                  adto.cnpj4_cliente,
                  adto.cnpj2_cliente,
                  0,   --in fatu_070.origem_pedido%type
                  v_codigo_transacao,
                  v_codigo_contabil,
                  adto.valor,
                  0, --in fatu_070.nr_identificacao%type,
                  v_conta_corrente,
                  r_num_lcto,
                  0, -- in fatu_070.duplic_emitida%type,
                  0,  -- in fatu_070.pedido_venda%type
                  'SOLICITACAO DE REEMBOLSO',
                  r_des_erro
                );
                ok := true;
           exception
                when others then
                   ok := false;
                   v_sequencia_insert := v_sequencia_insert + 1;
           end;
        end loop;
        
        if r_des_erro is not null then
           rollback;
           raise_application_error(-20001, r_des_erro || Chr(10) || SQLERRM);                      
        end if;  


        r_num_lcto := 0;
        

        -- contabiliza baixa do titulo a receber gerado pela transformacaoo do adiantamento
        if v_gera_contabil_cc = 1 and v_atualiza_contabi_oper_021 <> 2 -- consiste se a conta corrente de baixa gera contabilizacao e se a contabilizacao esta ativa
        then
          inter_pr_contabilizacao_prg_02(adto.empresa,
                             adto.cod_centro_custo,
                             adto.cnpj9_cliente,
                             adto.cnpj4_cliente,
                             adto.cnpj2_cliente,
                             v_codigo_transacao,
                             v_codigo_historico_cont,
                             v_conta_corrente,
                             v_codigo_contabil_cc,
                             adto.cod_contabil_cli,
                             v_adiantamento,
                             to_date(sysdate),
                             adto.valor,
                             4, --p_origem    baixa titulos,
                             20, -- p_tp_cta_deb       IN NUMBER,
                             1, --p_tp_cta_cre       IN NUMBER,
                             p_programa,
                             p_usuario,
                             v_adiantamento || ' - ' || adto.nome_cliente,
                             v_tipo_tit_adiant,
                             r_num_lcto,
                             r_des_erro);

             if r_des_erro is not null then
                      rollback;
                      raise_application_error(-20001, ' baixa do titulo a receber transformacao do adiantamento - ' || r_des_erro || Chr(10) || SQLERRM);

             end if;
         end if;
         
         --atualiza conta corrente
        BEGIN

        INTER_PR_INSERE_CPAG_026
        ( v_conta_corrente, v_cod_portador,  to_date(sysdate),
        'C', v_codigo_historico_pgto,  adto.valor,
        2,  1,  'BAIXA OUTRAS RECEITAS', to_date(sysdate), r_num_lcto);

        EXCEPTION WHEN OTHERS THEN
          rollback;
          raise_application_error(-20001, r_des_erro || ' Erro ao inserir conta corrente - cpag_026' || Chr(10) || SQLERRM);
        END;


        --gera a baixa do titulo pela transformacao do adiantamento

        BEGIN
        inter_pr_gera_fatu_075(
          adto.cnpj9_cliente,
          adto.cnpj4_cliente,
          adto.cnpj2_cliente,
          v_tipo_tit_adiant,
          v_adiantamento,
          adto.empresa,
          v_sequencia_insert,
          to_date(sysdate),
          v_codigo_historico_pgto,
          v_cod_portador,
          v_conta_corrente,
          to_date(sysdate),
          0, --p_pago_adiantamento
          0, --v_adiantamento
          r_num_lcto,
          adto.valor,
          0, --p_tipo_lanc_comissao
          r_des_erro);

         EXCEPTION WHEN OTHERS THEN 
           rollback;
           raise_application_error(-20001, r_des_erro || ' Erro ao inserir baixa de titulo - fatu_075' || Chr(10) || SQLERRM);
        END;
        
     end if; 
     
     
     if adto.tipo_solicitacao = 2 then
     
        v_adiantamento := adto.numero_adiantamento;
     
     end if;
                                  
    
     BEGIN
      update cpag_200
      set cpag_200.situacao = 1,
          cpag_200.valor_saldo = 0
      where tipo_adiantam = 2
      and numero_adiantam = v_adiantamento;
     EXCEPTION WHEN OTHERS THEN
         rollback;
        raise_application_error(-20001, r_des_erro || ' Erro ao atualizar adiantamento - cpag_200' || Chr(10) || SQLERRM);
     END;
     
     BEGIN
        update cpag_787
        set nr_titulo_transf = v_adiantamento ,
            seq_titulo_trans = v_sequencia_insert,
            situacao         = 1 -- atualiza para processado
        where cpag_787.empresa = p_empresa
          and cpag_787.numero_solicitacao = p_numero_solicitacao;
     EXCEPTION WHEN OTHERS THEN
         rollback;
        raise_application_error(-20001, r_des_erro || ' Erro ao atualizar reembolso - cpag_787' || Chr(10) || SQLERRM);
     END;
     
     -- ler tabela de parametros oper_022
     BEGIN
      select tipo_titulo, origem_titulo, codigo_contabil, codigo_transacao, codigo_historico_cont, posicao_titulo, cod_portador
        into v_tipo_tit_oper_022, v_origem_tit_oper_022, v_cod_cont_oper_022, v_cod_tran_oper_022, v_cod_hist_cont_oper_022, v_pos_tit_oper_022, v_cod_port_oper_022
        from oper_022
        where rownum = 1;
        
        
     EXCEPTION
       WHEN no_data_found THEN
            v_tipo_tit_oper_022 :=0;
            v_origem_tit_oper_022 :=0;
            v_cod_cont_oper_022 :=0;
            v_cod_tran_oper_022 :=0;
            v_cod_hist_cont_oper_022 :=0;
            v_pos_tit_oper_022 :=0;
            v_cod_port_oper_022 :=0;      
     END;

     v_atualiza_contabi_oper_022 := 1;
   
     -- Verifica se a contabilização está ativa para a transação
     BEGIN 
        select estq_005.atualiza_contabi into v_atualiza_contabi_oper_022 from estq_005 where estq_005.codigo_transacao = v_cod_tran_oper_022;
     END;
     
     -- ler tabela de parametros oper_023 - Baixa do t?tulo a receber de reembolso
     BEGIN
     
       select codigo_historico_pgto, conta_corrente, cod_portador
         into v_cod_hist_pgto_oper_023, v_conta_corrente_oper_023, v_cod_port_oper_023
         from oper_023
         where cod_empresa = adto.empresa
         and rownum = 1;
         
        EXCEPTION
            WHEN no_data_found THEN
             v_cod_hist_pgto_oper_023 :=0;
             v_conta_corrente_oper_023:= 0;
             v_cod_port_oper_023:= 0;
     END;
     
     BEGIN
      select c.codigo_contabil, c.gera_contabil
        into v_codigo_contabil_cc_023, v_gera_contabil_cc_023
        from cpag_020 c
        where c.portador     = v_cod_port_oper_023
        and c.conta_corrente = v_conta_corrente_oper_023
        and rownum = 1;

      EXCEPTION
          WHEN no_data_found THEN
              v_codigo_contabil_cc_023 := 0;
              v_gera_contabil_cc_023 := 0;
   END;
     
     r_num_lcto := 0;
     
     --contabiliza titulo a receber gerado de reembolso
     if v_atualiza_contabi_oper_022 = 1 then
         inter_pr_contabilizacao_prg_02(adto.empresa,
                              adto.cod_centro_custo,
                              adto.cnpj9_cliente,
                              adto.cnpj4_cliente,
                              adto.cnpj2_cliente,
                              v_cod_tran_oper_022,--aqui 
                              v_cod_hist_cont_oper_022,
                              0,--v_conta_corrente,
                              adto.cod_contabil_cli,
                              v_cod_cont_oper_022,
                              v_adiantamento,
                              to_date(sysdate),
                              adto.valor,
                              3, -- p_origem      titulo a receber
                              1, -- p_tp_cta_deb       IN NUMBER
                              5, -- p_tp_cta_cre       IN NUMBER
                              p_programa,
                              p_usuario,
                              v_adiantamento || ' - ' || adto.nome_cliente,
                              v_tipo_tit_oper_022,
                              r_num_lcto,
                              r_des_erro);
     end if;

     if r_des_erro is not null then
             rollback;
             raise_application_error(-20001, ' contabiliza titulo a receber reembolso - ' || r_des_erro || Chr(10) || SQLERRM);
     end if;
     
     ok := false;
     
      while ok = false
        LOOP
              -- gera titulo a receber de reembolso
            begin
                inter_pr_insere_fatu_070 (
                  adto.empresa,
                  adto.cnpj9_cliente,
                  adto.cnpj4_cliente,
                  adto.cnpj2_cliente,
                  v_tipo_tit_oper_022,
                  v_adiantamento,
                  v_sequencia_insert,
                  to_date(sysdate),
                  adto.valor,
                  adto.cdrepres_cliente,
                  to_date(sysdate),
                  adto.numero_adiantamento,
                  v_sequencia_insert,
                  v_cod_hist_cont_oper_022,
                  adto.empresa,
                  0, --in fatu_070.port_anterior%type
                  v_cod_port_oper_022,
                  v_pos_tit_oper_022,
                  to_date(sysdate),
                  '0', --in fatu_070.tecido_peca%type,
                  2,   --fatu_070.seq_end_cobranca%type
                  0,   --in fatu_070.comissao_lancada%type,
                  0,   --in fatu_070.tipo_tit_origem%type,
                  0,   --in fatu_070.num_dup_origem%type,
                  0,   --in fatu_070.seq_dup_origem%type,
                  adto.cnpj9_cliente,
                  adto.cnpj4_cliente,
                  adto.cnpj2_cliente,
                  0,   --in fatu_070.origem_pedido%type
                  v_cod_tran_oper_022,
                  v_cod_cont_oper_022,
                  adto.valor,
                  0, --in fatu_070.nr_identificacao%type,
                  v_conta_corrente,
                  r_num_lcto,
                  0, -- in fatu_070.duplic_emitida%type,
                  0,  -- in fatu_070.pedido_venda%type
                  'SOLICITACAO DE REEMBOLSO',
                  r_des_erro
                );
                ok := true;
           exception
                when others then
                   ok := false;
                   v_sequencia_insert := v_sequencia_insert + 1;
           end;
        end loop;
        
        r_num_lcto := 0;
        
        if v_gera_contabil_cc_023 = 1 and v_atualiza_contabi_oper_022 <> 2-- consiste se a conta corrente de baixa gera contabilizacao 
        then
            --contabiliza baixa titulo a receber de reembolso
            inter_pr_contabilizacao_prg_02(adto.empresa,
                                adto.cod_centro_custo,
                                adto.cnpj9_cliente,
                                adto.cnpj4_cliente,
                                adto.cnpj2_cliente,
                                v_cod_tran_oper_022,
                                v_cod_hist_pgto_oper_023,
                                v_conta_corrente_oper_023,

                                v_codigo_contabil_cc_023,
                                adto.cod_contabil_cli,
                                v_adiantamento,
                                to_date(sysdate),
                                adto.valor,
                                4, --p_origem    baixa titulos,
                                20, -- p_tp_cta_deb       IN NUMBER,
                                1, --p_tp_cta_cre       IN NUMBER,
                                p_programa,
                                p_usuario,
                                v_adiantamento || ' - ' || adto.nome_cliente,
                                v_tipo_tit_oper_022,
                                r_num_lcto,
                                r_des_erro);
                                
            if r_des_erro is not null then
                          rollback;
                          raise_application_error(-20001, ' contabiliza baixa de titulo a receber reembolso - ' || r_des_erro || Chr(10) || SQLERRM);

            end if;
        end if;
      
       -- baixa titulo a receber de reembolso                    
         
         inter_pr_gera_fatu_075(
           adto.cnpj9_cliente,
           adto.cnpj4_cliente,
           adto.cnpj2_cliente,
           v_tipo_tit_oper_022,
           v_adiantamento,
           adto.empresa,
           v_sequencia_insert,
           to_date(sysdate),
           v_cod_hist_pgto_oper_023,
           v_cod_port_oper_023,
           v_conta_corrente_oper_023,
           to_date(sysdate),
           1, --p_pago_adiantamento
           v_adiantamento,
           r_num_lcto,
           adto.valor,
           0, --p_tipo_lanc_comissao
           r_des_erro);
           
           if r_des_erro is not null then
              rollback;
              raise_application_error(-20001, ' contabiliza titulo a receber reembolso - ' || r_des_erro || Chr(10) || SQLERRM);
           end if;
        
        BEGIN
          select cod_portador, codigo_contabil, codigo_transacao, codigo_historico_cont, posicao_titulo, nr_dias_uteis, tipo_titulo
          into v_cod_port_oper_024, v_cod_cont_oper_024, v_cod_tran_oper_024, v_cod_hist_cont_oper_024, v_pos_tit_oper_024, v_nr_dias_ut_oper_024, v_tipo_tit_oper_024
          from oper_024
          where rownum = 1;
        EXCEPTION
            WHEN no_data_found THEN
              v_cod_port_oper_024 := 0;
              v_cod_cont_oper_024 := 0;
              v_cod_tran_oper_024 := 0;
              v_cod_hist_cont_oper_024 := 0;
              v_pos_tit_oper_024 := 0;
              v_nr_dias_ut_oper_024 := 0;
        END;
        
        r_num_lcto := 0;

        v_atualiza_contabi_oper_024 := 1;
   
        -- Verifica se a contabilização está ativa para a transação da tabela oper_021
        BEGIN 
           select estq_005.atualiza_contabi into v_atualiza_contabi_oper_024 from estq_005 where estq_005.codigo_transacao = v_cod_tran_oper_024;
        END;
        
        -- contabiliza o titulo a pagar de reembolso
        if v_atualiza_contabi_oper_024 = 1 then
            inter_pr_contabilizacao_prg_02(adto.empresa,
                                 adto.cod_centro_custo,
                                 adto.cnpj9_cliente,
                                 adto.cnpj4_cliente,
                                 adto.cnpj2_cliente,
                                 v_cod_tran_oper_024,
                                 v_cod_hist_cont_oper_024,
                                 0, -- p_conta
                                 v_cod_cont_oper_024, -- cod contabil de debito
                                 adto.cod_contabil_cli, -- cod contabil de credito
                                 v_adiantamento,
                                 to_date(sysdate),
                                 adto.valor,
                                 5, -- p_origem      titulo a pagar
                                 4, -- p_tp_cta_deb       IN NUMBER
                                 2, -- p_tp_cta_cre       IN NUMBER
                                 p_programa,
                                 p_usuario,
                                 v_adiantamento || ' - ' || adto.nome_cliente,
                                 v_tipo_tit_oper_024,
                                 r_num_lcto,
                                 r_des_erro);
        end if;
        
        if r_des_erro is not null then
           rollback;
           raise_application_error(-20001, ' contabiliza o titulo a pagar de reembolso - ' || r_des_erro || Chr(10) || SQLERRM);
        end if;
        
        BEGIN
            --titulo a pagar de reembolso - cpag_010
            inter_pr_insere_cpag_010(v_adiantamento,
                                     v_sequencia_insert,
                                     adto.cnpj9_cliente,
                                     adto.cnpj4_cliente,
                                     adto.cnpj2_cliente,
                                     v_tipo_tit_oper_024,
                                     adto.empresa,
                                     adto.empresa,  
                                     adto.nome_cliente,
                                     0, --p_documento
                                     '',--p_serie
                                     to_date(sysdate), -- p_data_contrato
                                     inter_fn_calendarioDiaUtil(sysdate + v_nr_dias_ut_oper_024 + 1 , 1), -- p_data_vencimento
                                     to_date(sysdate), -- p_data_transacao
                                     adto.cod_centro_custo, --dentro de custo
                                     v_cod_port_oper_024,
                                     v_cod_hist_cont_oper_024,
                                     v_cod_tran_oper_024,
                                     v_pos_tit_oper_024,
                                     v_cod_cont_oper_024,
                                     0, --p_tipo_pagamento
                                     0, --p_moeda_titulo
                                     'SOLICITACAO DE REEMBOLSO',
                                     0, --p_previsao
                                     adto.valor,
                                     adto.valor,
                                     0, --p_base_irrf_moeda  
                                     0, --p_base_irrf        
                                     0, --p_cod_ret_irrf     
                                     0, --p_aliq_irrf        
                                     0, --p_valor_irrf_moeda 
                                     0, --p_valor_irrf       
                                     0, --p_base_iss         
                                     0, --p_cod_ret_iss      
                                     0, --p_aliq_iss         
                                     0, --p_valor_iss        
                                     0, --p_base_inss        
                                     0, --p_cod_ret_inss     
                                     0, --p_aliq_inss        
                                     0, --p_valor_inss_imp   
                                     0, --p_base_pis         
                                     0, --p_cod_ret_pis      
                                     0, --p_aliq_pis         
                                     0, --p_valor_pis_imp    
                                     0, --p_base_cofins      
                                     0, --p_cod_ret_cofins   
                                     0, --p_aliq_cofins      
                                     0, --p_valor_cofins_imp 
                                     0, --p_base_csl         
                                     0, --p_cod_ret_csl      
                                     0, --p_aliq_csl         
                                     0, --p_valor_csl_imp    
                                     0, --p_base_csrf        
                                     0, --p_cod_ret_csrf     
                                     0, --p_aliq_csrf        
                                     0, --p_valor_csrf_imp    
                                     '', --p_codigo_barras,
                                     0, --p_projeto
                                     0, --p_subprojeto
                                     0, --p_servico
                                     0, --p_nr_processo_export
                                     0, --p_nr_processo_import
                                     0, --p_cod_cancelamento
                                     '', --p_data_canc_tit
                                     r_num_lcto,
                                     p_usuario,
                                     
                                     adto.cnpj9_favorecido,
                                     adto.cnpj4_favorecido,                                                                                                       
                                     adto.cnpj2_favorecido,                                                                                                      

                                     adto.cod_banco,                                                                                                        
                                     adto.agencia,                                                                                                          
                                     adto.dig_agencia,                                                                                                        
                                     adto.conta, 
                                     adto.dig_conta, 
                                     
                                     0,--p_num_importacao                                   
                                     
                                     r_has_error,
                                     r_des_erro);

        EXCEPTION WHEN OTHERS THEN 
           rollback;
           raise_application_error(-20001, r_des_erro || ' Erro ao inserir titulo reembolso - cpag_010' || Chr(10) || SQLERRM);
        END; 
        
          
   END LOOP;

   if r_des_erro is not null then
      rollback;
      raise_application_error(-20001, r_des_erro || Chr(10) || SQLERRM);
   else
      commit;
   end if;
   

END INTER_PR_GERA_REEMBOLSO;
