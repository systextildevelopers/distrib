
  CREATE OR REPLACE PROCEDURE "INTER_PR_GRAVAR_MAIOR_VENDA" (p_empresa    in number,
                                                         p_nivel      in varchar2,
                                                         p_grupo      in varchar2,
                                                         p_subgrupo   in varchar2,
                                                         p_item       in varchar2,
                                                         p_tp_produto in number,
                                                         p_mes        in number,
                                                         p_ano        in number,
                                                         p_nota       in number,
                                                         p_serie      in varchar2,
                                                         p_data       in date,
                                                         p_valor      in number,
                                                         p_valor_nota in number)
is
begin
   begin
      insert into rcnb_380 (
                  cod_empresa,    mes_apuracao,    ano_apuracao,
                  nivel_prod,  	  grupo_prod,      sub_prod,
                  item_prod,      tipo_produto,    nota_fiscal,
                  serie_nota,     data_nota,       valor,
                  valor_nota)
             values (
                  p_empresa,      p_mes,           p_ano,
                  p_nivel,        p_grupo,         p_subgrupo,
                  p_item,         p_tp_produto,    p_nota,
                  p_serie,        p_data,          p_valor,
                  p_valor_nota);
      exception
         when others then
            update rcnb_380
               set rcnb_380.nota_fiscal  = p_nota ,
                   rcnb_380.serie_nota   = p_serie,
                   rcnb_380.data_nota    = p_data ,
                   rcnb_380.valor        = p_valor,
                   rcnb_380.tipo_produto = p_tp_produto,
                   rcnb_380.valor_nota   = p_valor_nota
             where rcnb_380.cod_empresa  = p_empresa
               and rcnb_380.mes_apuracao = p_mes
               and rcnb_380.ano_apuracao = p_ano
               and rcnb_380.nivel_prod   = p_nivel
               and rcnb_380.grupo_prod   = p_grupo
               and rcnb_380.sub_prod     = p_subgrupo
               and rcnb_380.item_prod    = p_item;
   end;
end inter_pr_gravar_maior_venda;

 

/

exec inter_pr_recompile;

