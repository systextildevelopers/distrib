
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_040_HIST" 
   after update
   of nivel_item,
      grupo_item,
      sub_item,
      item_item,
      alternativa_item,
      sequencia,
      sub_comp,
      item_comp,
      consumo,
      cons_unid_med_generica,
      sequencia_tamanho,
      alternativa_comp
on basi_040
for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   teve_alter             number(1);

   v_nivel_comp           varchar2(1);
   v_grupo_comp           varchar2(5);

   sequencia_hist         number(9);
   long_aux               long;

   linha                  varchar(100);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if updating and (:old.nivel_item <> :new.nivel_item or
                    :old.grupo_item <> :new.grupo_item or
                    :old.sub_item <> :new.sub_item or
                    :old.item_item <> :new.item_item or
                    :old.alternativa_item <> :new.alternativa_item or
                    :old.sequencia <> :new.sequencia or
                    :old.sub_comp <> :new.sub_comp or
                    :old.item_comp <> :new.item_comp or
                    :old.consumo <> :new.consumo or
                    :old.cons_unid_med_generica <> :new.cons_unid_med_generica or
                    :old.sequencia_tamanho <> :new.sequencia_tamanho or
                    :old.alternativa_comp <> :new.alternativa_comp)
   then
     begin
        select basi_050.nivel_comp, basi_050.grupo_comp
        into   v_nivel_comp,        v_grupo_comp
        from basi_050
        where basi_050.nivel_item       = :old.nivel_item
          and basi_050.grupo_item       = :old.grupo_item
          and basi_050.alternativa_item = :old.alternativa_item
          and basi_050.sequencia        = :old.sequencia
          and rownum                    = 1;
     exception when others then
         v_nivel_comp := '';
         v_grupo_comp := '';
     end;

      begin
         select nvl(max(basi_095.sequencia),0)
         into sequencia_hist
         from basi_095
         where basi_095.tipo_comentario = 2 -- Historico.
           and basi_095.nivel_estrutura = :new.nivel_item
           and basi_095.grupo_estrutura = :new.grupo_item
           and basi_095.data_historico  = sysdate;
      exception when others then
         sequencia_hist := 0;
      end;

      sequencia_hist := sequencia_hist + 1;

      linha := ' ';

      long_aux       := inter_fn_buscar_tag('lb34909#A ESTRUTURA(040):',ws_locale_usuario,ws_usuario_systextil) ||
                        :new.nivel_item || '.' || :new.grupo_item || '.' || :new.sub_item   || '.' || :new.item_item  || ' ' ||
                        inter_fn_buscar_tag('lb34910#NA ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                        to_char(:new.alternativa_item, '00') || chr(10) ||
                        inter_fn_buscar_tag('lb34318#TEVE AS SEGUINTES MODIFICACOES:',ws_locale_usuario,ws_usuario_systextil);

      teve_alter     := 0;

      if :old.sub_comp <> :new.sub_comp
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:new.sequencia,'000') || chr(10) ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              v_nivel_comp || '.' || v_grupo_comp || '.' || :old.sub_comp   || '.' || :old.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34953#TEVE O TAMANHO DO COMPONENTE ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.sub_comp ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.sub_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34951#FICANDO DA SEGUINTE FORMA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              v_nivel_comp  || '.' || v_grupo_comp  || '.' || :new.sub_comp || '.' || :new.item_comp;
      end if;

      if :old.item_comp <> :new.item_comp
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:new.sequencia,'000') || chr(10) ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              v_nivel_comp || '.' || v_grupo_comp || '.' || :old.sub_comp   || '.' || :old.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34954#TEVE A COR DO COMPONENTE ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.item_comp ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34951#FICANDO DA SEGUINTE FORMA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              v_nivel_comp  || '.' || v_grupo_comp  || '.' || :new.sub_comp || '.' || :new.item_comp;
      end if;

      if :old.consumo <> :new.consumo
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:new.sequencia,'000') || chr(10) ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              v_nivel_comp || '.' || v_grupo_comp || '.' || :new.sub_comp   || '.' || :old.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34916#TEVE O CONSUMO ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.consumo ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.consumo;
      end if;

      if :old.cons_unid_med_generica <> :new.cons_unid_med_generica
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:new.sequencia,'000') || chr(10) ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              v_nivel_comp || '.' || v_grupo_comp || '.' || :new.sub_comp   || '.' || :old.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34940#TEVE O CONSUMO AUXILIAR ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.cons_unid_med_generica ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.cons_unid_med_generica;
      end if;

      if :old.sequencia_tamanho <> :new.sequencia_tamanho
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:new.sequencia,'000') || chr(10) ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              v_nivel_comp || '.' || v_grupo_comp || '.' || :new.sub_comp   || '.' || :old.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34955#TEVE A SEQUENCIA TAMANHO ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.sequencia_tamanho ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.sequencia_tamanho;
      end if;

      if :old.alternativa_comp <> :new.alternativa_comp
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:new.sequencia,'000') || chr(10) ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              v_nivel_comp || '.' || v_grupo_comp || '.' || :new.sub_comp   || '.' || :old.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34920#TEVE A ALTERNATIVA ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.alternativa_comp ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.alternativa_comp;
      end if;

      if teve_alter = 1
      then
         begin
            INSERT INTO basi_095
               (nivel_estrutura,        grupo_estrutura,
                subgru_estrutura,       data_historico,
                tipo_comentario,        sequencia,
                descricao,              codigo_usuario,
                hora_historico)
            VALUES
               (:new.nivel_item,        :new.grupo_item,
                :new.sub_item,          sysdate,
                2,                      sequencia_hist,
                long_aux,               ws_usuario_systextil,
                sysdate);
         exception when OTHERS then
            raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'BASI_095(050-3)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;
end inter_tr_basi_040_hist;

-- ALTER TRIGGER "INTER_TR_BASI_040_HIST" ENABLE
 

/

exec inter_pr_recompile;

