create table OPER_026
(
  tipo_titulo           NUMBER(3) default 0 not null,
  origem_titulo         VARCHAR2(100) default ' ',
  codigo_contabil       NUMBER(6) default 0 not null,
  codigo_transacao      NUMBER(3) default 0 not null,
  codigo_historico_cont NUMBER(4) default 0 not null,
  posicao_titulo        NUMBER(9) default 0 not null,
  cod_portador          NUMBER(3) default 0 not null
);

alter table OPER_026
  add constraint PK_OPER_026 primary key (TIPO_TITULO, CODIGO_CONTABIL, CODIGO_TRANSACAO, CODIGO_HISTORICO_CONT, POSICAO_TITULO, COD_PORTADOR);

/
exec inter_pr_recompile;
 
