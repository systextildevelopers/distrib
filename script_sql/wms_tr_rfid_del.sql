create or replace trigger WMS_TR_RFID_DEL
  after delete on inte_wms_rfid
  for each row
declare
  Pragma Autonomous_Transaction;
  -- local variables here
  v_rfid_caixa                 inte_wms_rfid.rfid_caixa              %type;
  v_nivel_sku_old              inte_wms_rfid.nivel_sku               %type;
  v_grupo_sku_old              inte_wms_rfid.grupo_sku               %type;
  v_subgrupo_sku_old           inte_wms_rfid.subgrupo_sku            %type;
  v_item_sku_old               inte_wms_rfid.item_sku                %type;
  v_cod_deposito_old           inte_wms_rfid.cod_deposito            %type;
  v_qtd_pecas_old              inte_wms_rfid.qtd_pecas               %type;
  v_flag_old                   inte_wms_rfid.flag                    %type;
  v_timestamp_lib_old          inte_wms_rfid.timestamp_liberacao     %type;
  v_timestamp_arm_old          inte_wms_rfid.timestamp_armazenagem   %type;
  v_mensagem_old               inte_wms_rfid.mensagem                %type;
  v_codigo_unico_sku_old       inte_wms_rfid.codigo_unico_sku        %type;
begin

   --alimenta as variaveis old caso seja delete
   if deleting
   then

      v_rfid_caixa            := :old.rfid_caixa;
      v_nivel_sku_old         := :old.nivel_sku;
      v_grupo_sku_old         := :old.grupo_sku;
      v_subgrupo_sku_old      := :old.subgrupo_sku;
      v_item_sku_old          := :old.item_sku;
      v_cod_deposito_old      := :old.cod_deposito;
      v_qtd_pecas_old         := :old.qtd_pecas;
      v_flag_old              := :old.flag;
      v_timestamp_lib_old     := :old.timestamp_liberacao;
      v_timestamp_arm_old     := :old.timestamp_armazenagem;
      v_mensagem_old          := :old.mensagem;
      v_codigo_unico_sku_old  := :old.codigo_unico_sku;

      if :old.flag = 9 --Rejeitada
      then         

         --insere na inte_wms_rfid o registro.
         insert into inte_wms_rfid (
            inte_wms_rfid.rfid_caixa,
            inte_wms_rfid.nivel_sku,               inte_wms_rfid.grupo_sku,
            inte_wms_rfid.subgrupo_sku,            inte_wms_rfid.item_sku,
            inte_wms_rfid.cod_deposito,            inte_wms_rfid.qtd_pecas,
            inte_wms_rfid.flag,                    inte_wms_rfid.timestamp_liberacao,
            inte_wms_rfid.timestamp_armazenagem,   inte_wms_rfid.mensagem,
            inte_wms_rfid.codigo_unico_sku
         )
         values (
            'D'||v_rfid_caixa,
            v_nivel_sku_old,                       v_grupo_sku_old,
            v_subgrupo_sku_old,                    v_item_sku_old,
            v_cod_deposito_old,                    v_qtd_pecas_old,
            99,                                    null,
            null,                                  '',
            v_nivel_sku_old||'.'||v_grupo_sku_old||'.'||v_subgrupo_sku_old||'.'||v_item_sku_old
         );
         COMMIT;

      end if; --fim do if rejeitada

      /*Remove item da tabela espelho*/
      delete from inte_wms_rfid_espelho
      where inte_wms_rfid_espelho.rfid_caixa = v_rfid_caixa;
            
      /*Adiciona item a tabela espelho*/
      insert into inte_wms_rfid_espelho
        (rfid_caixa,
         nivel_sku,
         grupo_sku,
         subgrupo_sku,
         item_sku,
         cod_deposito,
         qtd_pecas,
         flag,
         timestamp_liberacao,
         timestamp_armazenagem,
         mensagem,
         codigo_unico_sku)
      values
        (v_rfid_caixa,
         v_nivel_sku_old,
         v_grupo_sku_old,
         v_subgrupo_sku_old,
         v_item_sku_old,
         v_cod_deposito_old,
         v_qtd_pecas_old,
         0,
         sysdate,
         sysdate,
         v_mensagem_old,
         v_codigo_unico_sku_old);

      /*Remove itens da tabela espelho de 2 dias atr�s*/
      delete from inte_wms_rfid_espelho
      where to_date(to_char(inte_wms_rfid_espelho.timestamp_liberacao, 'dd/mm/yyyy')) <= to_date(to_char(sysdate -2, 'dd/mm/yyyy'));
      
      commit;

   end if; --fim do if deleting

end WMS_TR_RFID_DEL;

/

execute inter_pr_recompile;

/* versao: 2 */


 exit;
