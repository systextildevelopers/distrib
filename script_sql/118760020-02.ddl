INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'supr_f094',     0,
	1,			     'Seleção de Requisições');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'supr_f094',   'rcnb_menu', 
	0,             0, 
	'S',           'S', 
	'S',           'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Seleção de Requisições'
 WHERE hdoc_036.codigo_programa = 'supr_f094'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;
