
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_615_SID" 
   before insert
   on tmrp_615
   for each row
begin
   begin
      select sys.gv_$session.sid,  sys.gv_$session.inst_id
      into   :new.sid_usuario,     :new.instancia
      from sys.gv_$session
      where audsid  = userenv('SESSIONID')
        and inst_id = userenv('INSTANCE')
        and rownum < 2;
   end;
end inter_tr_tmrp_615_sid;

-- ALTER TRIGGER "INTER_TR_TMRP_615_SID" ENABLE
 

/

exec inter_pr_recompile;

