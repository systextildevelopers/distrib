CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_343"
BEFORE INSERT ON OBRF_343 FOR EACH ROW
DECLARE
    nextValue number;
BEGIN
    select ID_OBRF_343.nextval into nextValue from dual;
    :new.ID := nextValue;
END INTER_TR_OBRF_343;

/

exec inter_pr_recompile;
