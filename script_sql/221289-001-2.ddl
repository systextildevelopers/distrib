declare tmpInt number;

cursor parametro_c is select codigo_empresa from fatu_500;

begin
  for reg_parametro in parametro_c
  loop
    select nvl(count(*),0) into tmpInt
    from fatu_500
    where fatu_500.codigo_empresa = reg_parametro.codigo_empresa;

    if(tmpInt > 0)
    then
      begin
        insert into empr_008 ( codigo_empresa, param, val_int)
            values (reg_parametro.codigo_empresa, 'notasDevolvidas.agenciaBaixa', 0);
      end;
	  begin
        insert into empr_008 ( codigo_empresa, param, val_int)
            values (reg_parametro.codigo_empresa, 'notasDevolvidas.codContabilGeracao', 0);
      end;
	  begin
        insert into empr_008 ( codigo_empresa, param, val_int)
            values (reg_parametro.codigo_empresa, 'notasDevolvidas.codContabilMovtoConta', 0);
      end;
	  begin
        insert into empr_008 ( codigo_empresa, param, val_int)
            values (reg_parametro.codigo_empresa, 'notasDevolvidas.codHistoricoBaixa', 0);
      end;
	  begin
        insert into empr_008 ( codigo_empresa, param, val_int)
            values (reg_parametro.codigo_empresa, 'notasDevolvidas.codHistoricoGeracao', 0);
      end;
	  begin
        insert into empr_008 ( codigo_empresa, param, val_int)
            values (reg_parametro.codigo_empresa, 'notasDevolvidas.codHistoricoMovtoConta', 0);
      end;
	  begin
        insert into empr_008 ( codigo_empresa, param, val_int)
            values (reg_parametro.codigo_empresa, 'notasDevolvidas.transacaoGeracao', 0);
      end;
	  begin
        insert into empr_008 ( codigo_empresa, param, val_int)
            values (reg_parametro.codigo_empresa, 'notasDevolvidas.codTransacaoMovtoConta', 0);
      end;
	  begin
        insert into empr_008 ( codigo_empresa, param, val_int)
            values (reg_parametro.codigo_empresa, 'notasDevolvidas.contaCorrenteBaixa', 0);
      end;
	  begin
        insert into empr_008 ( codigo_empresa, param, val_int)
            values (reg_parametro.codigo_empresa, 'notasDevolvidas.portadorBaixa', 0);
      end;
	  begin
        insert into empr_008 ( codigo_empresa, param, val_int)
            values (reg_parametro.codigo_empresa, 'notasDevolvidas.tipoTituloGeracao', 0);
      end;
    end if;

  end loop;
  commit;
end;
/
