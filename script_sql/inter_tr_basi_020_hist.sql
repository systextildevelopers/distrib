create or replace trigger INTER_TR_BASI_020_HIST
 after insert or delete or update of
   basi030_nivel030, basi030_referenc,
   tamanho_ref, descr_tam_refer,
   rendimento
 on basi_020
 for each row
 
declare
  ws_tipo_ocorr             varchar2(1);
  ws_usuario_rede           varchar2(20);
  ws_maquina_rede           varchar2(40);
  ws_aplicativo             varchar2(20);
  ws_sid                    number(9);
  ws_empresa                number(3);
  ws_usuario_systextil      varchar2(250);
  ws_locale_usuario         varchar2(5);
  ws_nome_programa          varchar2(20);
  ws_basi030_nivel030_old   varchar2(1);
  ws_basi030_nivel030_new   varchar2(1);
  ws_basi030_referenc_old   varchar2(5);
  ws_basi030_referenc_new   varchar2(5);
  ws_tamanho_ref_old        varchar2(3);
  ws_tamanho_ref_new        varchar2(3);
  ws_descr_tam_refer_old    varchar2(11);
  ws_descr_tam_refer_new    varchar2(11);
  ws_rendimento_old         number(8,2);
  ws_rendimento_new         number(8,2);

BEGIN
  ws_tipo_ocorr := ' ';
  ws_usuario_rede := ' ';
  ws_maquina_rede := ' ';
  ws_aplicativo := ' ';
  ws_sid := 0;
  ws_empresa := 0;
  ws_usuario_systextil := ' ';
  ws_locale_usuario := ' ';
  ws_nome_programa := ' ';
  ws_basi030_nivel030_old := ' ';
  ws_basi030_nivel030_new := ' ';
  ws_basi030_referenc_old := ' ';
  ws_basi030_referenc_new := ' ';
  ws_tamanho_ref_old := ' ';
  ws_tamanho_ref_new := ' ';
  ws_descr_tam_refer_old := ' ';
  ws_descr_tam_refer_new := ' ';
  ws_rendimento_old := 0.0;
  ws_rendimento_new := 0.0;

  inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                          ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

  begin
    select hdoc_090.programa
    into ws_nome_programa
    from hdoc_090
    where hdoc_090.sid = ws_sid
      and rownum       = 1
      and hdoc_090.programa not like '%menu%'
      and hdoc_090.programa not like '%_m%';
    exception
      when no_data_found then ws_nome_programa := 'SQL';
  end;

  if INSERTING then
    ws_tipo_ocorr                := 'I';
  elsif UPDATING then
    ws_tipo_ocorr                := 'A';
  elsif DELETING then
    ws_tipo_ocorr                := 'D';
  end if;

  if INSERTING or UPDATING then
     ws_basi030_nivel030_new := :new.basi030_nivel030;
     ws_basi030_referenc_new := :new.basi030_referenc;
     ws_tamanho_ref_new := :new.tamanho_ref;
     ws_descr_tam_refer_new := :new.descr_tam_refer;
     ws_rendimento_new := :new.rendimento;
  end if;
  if DELETING or UPDATING then
     ws_basi030_nivel030_old := :old.basi030_nivel030;
     ws_basi030_referenc_old := :old.basi030_referenc;
     ws_tamanho_ref_old := :old.tamanho_ref;
     ws_descr_tam_refer_old := :old.descr_tam_refer;
     ws_rendimento_old := :old.rendimento;
  end if;

  INSERT INTO BASI_020_LOG
  (
    TIPO_OCORR,
    DATA_OCORR,
    USUARIO_REDE,
    MAQUINA_REDE,
    APLICACAO,
    USUARIO_SISTEMA,
    NOME_PROGRAMA,
    BASI030_NIVEL030_OLD,
    BASI030_NIVEL030_NEW,
    BASI030_REFERENC_OLD,
    BASI030_REFERENC_NEW,
    TAMANHO_REF_OLD,
    TAMANHO_REF_NEW,
    DESCR_TAM_REFER_OLD,
    DESCR_TAM_REFER_NEW,
    RENDIMENTO_OLD,
    RENDIMENTO_NEW
  )
  VALUES
  (
    ws_tipo_ocorr,
    sysdate,
    ws_usuario_rede,
    ws_maquina_rede,
    ws_aplicativo,
    ws_usuario_systextil,
    ws_nome_programa,
    ws_BASI030_NIVEL030_OLD,
    ws_BASI030_NIVEL030_NEW,
    ws_BASI030_REFERENC_OLD,
    ws_BASI030_REFERENC_NEW,
    ws_TAMANHO_REF_OLD,
    ws_TAMANHO_REF_NEW,
    ws_DESCR_TAM_REFER_OLD,
    ws_DESCR_TAM_REFER_NEW,
    ws_RENDIMENTO_OLD,
    ws_RENDIMENTO_NEW
  );



END INTER_TR_BASI_020_HIST;


/

exec inter_pr_recompile;
