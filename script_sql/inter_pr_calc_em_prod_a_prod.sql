
  CREATE OR REPLACE PROCEDURE "INTER_PR_CALC_EM_PROD_A_PROD" 
is
   ws_usuario_rede                 varchar2(20);
   ws_maquina_rede                 varchar2(40);
   ws_aplicativo                   varchar2(20);
   ws_sid                          number(9);
   ws_empresa                      number(3);
   ws_usuario_systextil            varchar2(250);
   ws_locale_usuario               varchar2(5);

   v_qtde_pecas_prod_recalc        number;
   v_qtde_conserto_recalc          number;
   v_qtde_pecas_2a_recalc          number;
   v_qtde_perdas_recalc            number;
   v_qtde_pecas_prod_atz           number;
   v_qtde_pecas_2a_atz             number;
   v_qtde_conserto_atz             number;
   v_qtde_perdas_atz               number;

   ordem_producao_com_erro         boolean;
   v_tem_registro                  number;
   v_tem_registro_045              number;
   v_estagio_corte                 number;
   v_seq_operacao_alt_programado   number;
   v_sequencia_estagio_maior       number;
   v_seq_operacao_prim_est         number;
   v_seq_operacao_menor            number;
   v_sequencia_estagio_prim_est    number;
   v_sequencia_estagio_menor       number;
   v_nr_solicitacao                number;
   v_sequencia_045                 number;
   v_turno_producao                number;
   v_codigo_usuario                number;
   v_menor_seq_operacao            number;
   v_sqlerrm                       varchar2(2000);

PROCEDURE f_desabilita_trigger
IS
BEGIN
   execute immediate 'alter trigger inter_tr_pcpc_040 disable';
   execute immediate 'alter trigger inter_tr_pcpc_040_2 disable';
   execute immediate 'alter trigger inter_tr_pcpc_040_3 disable';
   execute immediate 'alter trigger inter_tr_pcpc_045_3 disable';
   execute immediate 'alter trigger inter_tr_pcpc_040_hist disable';
   execute immediate 'alter trigger inter_tr_pcpc_040_prod disable';
end f_desabilita_trigger;

PROCEDURE f_habilita_trigger
IS
BEGIN
   execute immediate 'alter trigger inter_tr_pcpc_040 enable';
   execute immediate 'alter trigger inter_tr_pcpc_040_2 enable';
   execute immediate 'alter trigger inter_tr_pcpc_040_3 enable';
   execute immediate 'alter trigger inter_tr_pcpc_045_3 enable';
   execute immediate 'alter trigger inter_tr_pcpc_040_hist enable';
   execute immediate 'alter trigger inter_tr_pcpc_040_prod enable';
end f_habilita_trigger;

begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


   v_tem_registro := 0;

   f_desabilita_trigger();

   begin
      select nvl(count(1),0)
      into v_tem_registro
      from tmrp_615
      where tmrp_615.nome_programa    = 'recalculo_plan'
        and tmrp_615.tipo_registro    = 555;     /* INICIO DO PLANEJAMENTO */
   exception when others then
      v_tem_registro := 0;
   end;

   if v_tem_registro <> 0
   then
      f_habilita_trigger();
      raise_application_error(-20000,'ATEN��O! O recalculo das necessidades de produ��o(tmrp_f010) esta sendo executado, aguarde o termino para executar este processo.');
   end if;

   begin
      select nvl(max(pcpc_144.nr_solicitacao),0) + 1
      into v_nr_solicitacao
      from pcpc_144;
   end;

   /*
      delete registro que indica que foi executado o calculo do em produ��o e a produzir
   */
   begin
      delete from basi_400
      where basi_400.nivel              = '0'
        and basi_400.grupo              = '00000'
        and basi_400.subgrupo           = '000'
        and basi_400.item               = '000000'
        and basi_400.tipo_informacao    = 144
        and basi_400.codigo_informacao  = 144;
   exception when others then
      v_sqlerrm :=  substr (sqlerrm, 1, 2000);
      f_habilita_trigger();
      -- ATEN��O! N�o atualizou registro na tabela {0}. Status: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'basi_400(7)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   commit;

   -- Inserir um registro indicando que o recalculo do em produ��o e a produzir foi executado
   begin
      insert into basi_400 (
         nivel,            grupo,
         subgrupo,         item,
         tipo_informacao,  codigo_informacao
      ) values (
         '0',              '00000',
         '000',            '000000',
         144,              144
      );
   exception when others then
      v_sqlerrm :=  substr (sqlerrm, 1, 2000);
      f_habilita_trigger();

      -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'BASI_400' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   commit;

   -- Grava um registro para indicar que esta sendo executado o recalculo do em produ��o e a produzir
   begin
      insert into tmrp_615 (
         nr_solicitacao,                   tipo_registro,
         nome_programa
      ) values (
         144,                              144,
         'recalc_em_prod_a_prod'
      );
   exception when others then
      v_sqlerrm :=  substr (sqlerrm, 1, 2000);
      f_habilita_trigger();
      -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   commit;

   -- Inicia o calculo do em produ��o e a produzir
   for reg_pcpc_020 in (select pcpc_020.ordem_producao,
                               pcpc_020.periodo_producao
                        from pcpc_020, (select pcpc_040.ordem_producao
                                        from pcpc_040
                                        where pcpc_040.situacao_em_prod_a_prod is null
                                           or pcpc_040.situacao_em_prod_a_prod in (0,2,3)
                                           or pcpc_040.qtde_em_producao_pacote < 0
                                           or pcpc_040.qtde_a_produzir_pacote < 0
                                        group by pcpc_040.ordem_producao) pcpc040
                        where pcpc_020.cod_cancelamento = 0
                          and pcpc_020.periodo_producao in  (select pcpc_010.periodo_producao
                                                             from pcpc_010
                                                             where pcpc_010.area_periodo     = 1
                                                               and pcpc_010.situacao_periodo < 3)
                          and pcpc_020.ordem_producao = pcpc040.ordem_producao
                        order by pcpc_020.ordem_producao,
                                 pcpc_020.periodo_producao)
   loop
      ordem_producao_com_erro := false;

      if inter_fn_ordem_fechada(reg_pcpc_020.ordem_producao) <> 0
      then
         -- Fazer um backup das quantidades peroduzidas para ter uma base se precisar fazer um PL/SQL
         begin
            select nvl(sum(pcpc_040.qtde_pecas_prod_recalc),0),         nvl(sum(pcpc_040.qtde_conserto_recalc),0),
                   nvl(sum(pcpc_040.qtde_pecas_2a_recalc),0),           nvl(sum(pcpc_040.qtde_perdas_recalc),0)
            into   v_qtde_pecas_prod_recalc,                            v_qtde_conserto_recalc,
                   v_qtde_pecas_2a_recalc,                              v_qtde_perdas_recalc
            from pcpc_040
            where pcpc_040.ordem_producao          = reg_pcpc_020.ordem_producao;
         end;

         if  v_qtde_pecas_prod_recalc = 0
         and v_qtde_conserto_recalc   = 0
         and v_qtde_pecas_2a_recalc   = 0
         and v_qtde_perdas_recalc     = 0
         then
            begin
               update pcpc_040
               set   pcpc_040.executa_trigger         = 1,
                     pcpc_040.qtde_pecas_prod_recalc  = pcpc_040.qtde_pecas_prod,
                     pcpc_040.qtde_conserto_recalc    = pcpc_040.qtde_conserto,
                     pcpc_040.qtde_pecas_2a_recalc    = pcpc_040.qtde_pecas_2a,
                     pcpc_040.qtde_perdas_recalc      = pcpc_040.qtde_perdas
               where pcpc_040.ordem_producao          = reg_pcpc_020.ordem_producao;
            exception when others then
               v_sqlerrm :=  substr (sqlerrm, 1, 2000);
               f_habilita_trigger();
               -- ATEN��O! N�o atualizou registro na tabela {0}. Status: {1}
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(8)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end if;

         -- Equalizar os apontamentos da pcpc_040 com a pcpc_045
         for reg_pcpc_040 in (select pcpc_040.periodo_producao,
                                     pcpc_040.ordem_confeccao,
                                     pcpc_040.codigo_estagio
                              from pcpc_040
                              where pcpc_040.ordem_producao   = reg_pcpc_020.ordem_producao
                              group by pcpc_040.periodo_producao,
                                       pcpc_040.ordem_confeccao,
                                       pcpc_040.codigo_estagio
                              order by pcpc_040.periodo_producao,
                                       pcpc_040.ordem_confeccao,
                                       pcpc_040.codigo_estagio)
         loop
            begin
               select pcpc_040.qtde_pecas_prod - pcpc_045a.rqtde_produzida,
                      pcpc_040.qtde_pecas_2a   - pcpc_045a.rqtde_pecas_2a,
                      pcpc_040.qtde_conserto   - pcpc_045a.rqtde_conserto,
                      pcpc_040.qtde_perdas     - pcpc_045a.rqtde_perdas,
                      pcpc_045a.rturno_producao
               into   v_qtde_pecas_prod_atz,
                      v_qtde_pecas_2a_atz,
                      v_qtde_conserto_atz,
                      v_qtde_perdas_atz,
                      v_turno_producao
               from pcpc_040, (select pcpc_045.ordem_producao,
                                      pcpc_045.pcpc040_perconf,
                                      pcpc_045.pcpc040_ordconf,
                                      pcpc_045.pcpc040_estconf,
                                      nvl(sum(pcpc_045.qtde_produzida),0) as rqtde_produzida,
                                      nvl(sum(pcpc_045.qtde_pecas_2a),0)  as rqtde_pecas_2a,
                                      nvl(sum(pcpc_045.qtde_conserto),0)  as rqtde_conserto,
                                      nvl(sum(pcpc_045.qtde_perdas),0)    as rqtde_perdas,
                                      nvl(min(pcpc_045.turno_producao),0) as rturno_producao
                               from pcpc_045
                               where pcpc_045.ordem_producao   = reg_pcpc_020.ordem_producao
                                 and pcpc_045.pcpc040_perconf  = reg_pcpc_040.periodo_producao
                                 and pcpc_045.pcpc040_ordconf  = reg_pcpc_040.ordem_confeccao
                                 and pcpc_045.pcpc040_estconf  = reg_pcpc_040.codigo_estagio
                               group by pcpc_045.ordem_producao,
                                      pcpc_045.pcpc040_perconf,
                                      pcpc_045.pcpc040_ordconf,
                                      pcpc_045.pcpc040_estconf) pcpc_045a
               where pcpc_045a.ordem_producao   = pcpc_040.ordem_producao
                 and pcpc_045a.pcpc040_perconf  = pcpc_040.periodo_producao
                 and pcpc_045a.pcpc040_ordconf  = pcpc_040.ordem_confeccao
                 and pcpc_045a.pcpc040_estconf  = pcpc_040.codigo_estagio
                 and pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                 and pcpc_040.codigo_estagio   = reg_pcpc_040.codigo_estagio
                 and pcpc_040.ordem_producao   = reg_pcpc_020.ordem_producao;
            exception when others then
               v_qtde_pecas_prod_atz := 0;
               v_qtde_pecas_2a_atz   := 0;
               v_qtde_conserto_atz   := 0;
               v_qtde_perdas_atz     := 0;
               v_turno_producao      := 0;
            end;

            begin
               select hdoc_030.codigo_usuario
               into   v_codigo_usuario
               from hdoc_030, pcpc_010
               where hdoc_030.usuario          = 'INTERSYS'
                 and hdoc_030.empresa          = pcpc_010.codigo_empresa
                 and pcpc_010.area_periodo     = 1
                 and pcpc_010.periodo_producao = reg_pcpc_040.periodo_producao;
            exception when others then
               v_codigo_usuario := 144;
            end;

            if v_qtde_pecas_prod_atz  <> 0
            or v_qtde_pecas_2a_atz    <> 0
            or v_qtde_conserto_atz    <> 0
            or v_qtde_perdas_atz      <> 0
            then
               begin
                  select nvl(max(pcpc_045.sequencia),0) + 1
                  into   v_sequencia_045
                  from pcpc_045
                  where pcpc_045.pcpc040_perconf = reg_pcpc_040.periodo_producao
                    and pcpc_045.pcpc040_ordconf = reg_pcpc_040.ordem_confeccao
                    and pcpc_045.pcpc040_estconf = reg_pcpc_040.codigo_estagio;
               exception when others then
                  v_sequencia_045 := 144;
               end;

               if v_sequencia_045 < 144
               then
                  v_sequencia_045 := 144;
               end if;

               begin
                  insert into pcpc_045 (
                     pcpc040_ordconf,                   pcpc040_perconf,
                     pcpc040_estconf,                   sequencia,
                     ordem_producao,

                     data_producao,
                     hora_producao,

                     turno_producao,                    codigo_usuario,
                     numero_documento,

                     qtde_produzida,                    qtde_pecas_2a,
                     qtde_conserto,                     qtde_perdas,

                     executa_trigger
                  ) values (
                     reg_pcpc_040.ordem_confeccao,       reg_pcpc_040.periodo_producao,
                     reg_pcpc_040.codigo_estagio,        v_sequencia_045,
                     reg_pcpc_020.ordem_producao,

                     to_date('01/01/2012 ' || to_CHAR(sysdate,'hh24:mi'),'dd/mm/yyyy hh24:mi'),
                     to_date('16/11/1989 ' || to_CHAR(sysdate,'hh24:mi'),'dd/mm/yyyy hh24:mi'),

                     v_turno_producao,                   v_codigo_usuario,
                     144144,

                     v_qtde_pecas_prod_atz,              v_qtde_pecas_2a_atz,
                     v_qtde_conserto_atz,                v_qtde_perdas_atz,

                     1
                  );
               exception when others then
                  v_sqlerrm :=  substr (sqlerrm, 1, 2000);
                  f_habilita_trigger();
                  raise_application_error(-20000,v_sqlerrm);
               end;
            end if;
         end loop;

         commit;

         -- Inicializa os valores do em produ��o e aproduzir da ordem de produ��o
         begin
            update pcpc_040
            set   pcpc_040.executa_trigger         = 1,
                  pcpc_040.qtde_em_producao_pacote = 0,
                  pcpc_040.qtde_disponivel_baixa   = 0,
                  pcpc_040.qtde_a_produzir_pacote  = pcpc_040.qtde_pecas_prog,
                  pcpc_040.qtde_pecas_prod         = 0,
                  pcpc_040.qtde_conserto           = 0,
                  pcpc_040.qtde_pecas_2a           = 0,
                  pcpc_040.qtde_perdas             = 0
            where pcpc_040.ordem_producao          = reg_pcpc_020.ordem_producao;
         exception when others then
            v_sqlerrm :=  substr (sqlerrm, 1, 2000);
            f_habilita_trigger();
            -- ATEN��O! N�o atualizou registro na tabela {0}. Status: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(7)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;

         -- Inicializa os valores do em produ��o e aproduzir da ordem de produ��o
         begin
            update pcpc_040
            set   pcpc_040.qtde_em_producao_pacote = pcpc_040.qtde_pecas_prog,
                  pcpc_040.qtde_disponivel_baixa   = pcpc_040.qtde_pecas_prog,
                  pcpc_040.executa_trigger         = 1
            where pcpc_040.ordem_producao          = reg_pcpc_020.ordem_producao
              and pcpc_040.estagio_anterior        = 0;
         exception when others then
            v_sqlerrm :=  substr (sqlerrm, 1, 2000);
            f_habilita_trigger();
            -- ATEN��O! N�o atualizou registro na tabela {0}. Status: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(7)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;

         v_tem_registro_045 := 0;

         begin
            select nvl(count(*),0)
            into v_tem_registro_045
            from pcpc_045
            where pcpc_045.ordem_producao          = reg_pcpc_020.ordem_producao
              and pcpc_045.executa_trigger         = 1;
         exception when others then
            v_tem_registro_045 := 0;
         end;

         if v_tem_registro_045 > 0
         then
            begin
               update pcpc_045
               set   pcpc_045.executa_trigger         = 0
               where pcpc_045.ordem_producao          = reg_pcpc_020.ordem_producao
                 and pcpc_045.executa_trigger         = 1;
            exception when others then
               v_sqlerrm :=  substr (sqlerrm, 1, 2000);
               f_habilita_trigger();
               -- ATEN��O! N�o atualizou registro na tabela {0}. Status: {1}
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_045(1)', v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end if;

         for reg_pcpc_040 in (select pcpc_040.periodo_producao,
                                     pcpc_040.ordem_confeccao
                              from pcpc_040
                              where pcpc_040.ordem_producao   = reg_pcpc_020.ordem_producao
                              group by pcpc_040.periodo_producao,
                                       pcpc_040.ordem_confeccao)
         loop
            -- Verifica se o pacote n�o tem o primeiro est�gio e aponta a ordem com erro
            v_tem_registro := 0;

            begin
               select nvl(count(*),0)
               into v_tem_registro
               from pcpc_040
               where pcpc_040.ordem_producao   = reg_pcpc_020.ordem_producao
                 and pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                 and pcpc_040.estagio_anterior = 0;
            exception when others then
               v_tem_registro := 0;
            end;

            if v_tem_registro = 0
            then
               begin
                  select nvl(min(pcpc_040.seq_operacao),0)
                  into v_menor_seq_operacao
                  from pcpc_040
                  where pcpc_040.ordem_producao   = reg_pcpc_020.ordem_producao
                    and pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                    and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                    and pcpc_040.seq_operacao     > 0;
               exception when others then
                  v_menor_seq_operacao := 0;
               end;

               if v_menor_seq_operacao <> 0
               then
                  begin
                     update pcpc_040
                     set   pcpc_040.estagio_anterior = 0
                     where pcpc_040.ordem_producao   = reg_pcpc_020.ordem_producao
                       and pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                       and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                       and pcpc_040.seq_operacao     = v_menor_seq_operacao;
                  end;
               end if;
            end if;
         end loop;

         commit;

         -- Processo de calculo do emprodu��o e a produzir e disponivel para baixa
         for reg_pcpc_040 in (select pcpc_040.periodo_producao,
                                     pcpc_040.ordem_confeccao,
                                     pcpc_040.codigo_estagio,
                                     pcpc_045.sequencia
                              from pcpc_040, pcpc_045
                              where pcpc_040.ordem_producao   = reg_pcpc_020.ordem_producao
                                and pcpc_045.ordem_producao   = pcpc_040.ordem_producao
                                and pcpc_045.pcpc040_perconf  = pcpc_040.periodo_producao
                                and pcpc_045.pcpc040_ordconf  = pcpc_040.ordem_confeccao
                                and pcpc_045.pcpc040_estconf  = pcpc_040.codigo_estagio
                              order by pcpc_040.ordem_producao,
                                       pcpc_040.ordem_confeccao,
                                       pcpc_040.seq_operacao,
                                       pcpc_040.sequencia_estagio,
                                       pcpc_045.sequencia)
         loop
            begin
               update pcpc_045
               set    pcpc_045.qtde_produzida     = pcpc_045.qtde_produzida,
                      pcpc_045.qtde_pecas_2a      = pcpc_045.qtde_pecas_2a,
                      pcpc_045.qtde_conserto      = pcpc_045.qtde_conserto,
                      pcpc_045.qtde_perdas        = pcpc_045.qtde_perdas,
                      pcpc_045.atz_em_prod        = 0,
                      pcpc_045.atz_a_prod         = 0,
                      pcpc_045.atz_pode_produzir  = 0,
                      pcpc_045.executa_trigger    = 3
               where pcpc_045.ordem_producao      = reg_pcpc_020.ordem_producao
                 and pcpc_045.pcpc040_perconf     = reg_pcpc_040.periodo_producao
                 and pcpc_045.pcpc040_ordconf     = reg_pcpc_040.ordem_confeccao
                 and pcpc_045.pcpc040_estconf     = reg_pcpc_040.codigo_estagio
                 and pcpc_045.sequencia           = reg_pcpc_040.sequencia;
            exception when others then
               v_sqlerrm :=  substr (sqlerrm, 1, 2000);
               f_habilita_trigger();
               -- ATENC?O! N?o atualizou registro na tabela {0}. Status: {1}
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_045(1)', v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end loop;

         -- Qual est�gio altera o programado
         begin
            select basi_030.estagio_altera_programado
            into   v_estagio_corte
            from basi_030, pcpc_020
            where pcpc_020.ordem_producao = reg_pcpc_020.ordem_producao
              and basi_030.nivel_estrutura = '1'
              and basi_030.referencia      = pcpc_020.referencia_peca;
         end;

         if v_estagio_corte is null
         or v_estagio_corte = 0
         then
            begin
               select empr_001.estagio_corte
               into   v_estagio_corte
               from empr_001;
            end;
         end if;

         ordem_producao_com_erro := false;

         for reg_pcpc_040 in (select pcpc_040.periodo_producao,
                                     pcpc_040.ordem_confeccao
                              from pcpc_040
                              where pcpc_040.ordem_producao   = reg_pcpc_020.ordem_producao
                              group by pcpc_040.periodo_producao,
                                       pcpc_040.ordem_confeccao)
         loop
            -- Verifica se o pacote n�o tem o primeiro est�gio e aponta a ordem com erro
            v_tem_registro := 0;

            begin
               select nvl(count(*),0)
               into v_tem_registro
               from pcpc_040
               where pcpc_040.ordem_producao   = reg_pcpc_020.ordem_producao
                 and pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                 and pcpc_040.estagio_anterior = 0;
            exception when others then
               v_tem_registro := 0;
            end;

            if v_tem_registro = 0
            then
               ordem_producao_com_erro := true;

               begin
                  insert into pcpc_144 (
                     nr_solicitacao,                  codigo_erro,
                     ordem_producao,                  ordem_confeccao,
                     periodo_producao
                  ) values (
                     v_nr_solicitacao,                 1, --N�o tem o primeiro est�gio
                     reg_pcpc_020.ordem_producao,      reg_pcpc_040.ordem_confeccao,
                     reg_pcpc_040.periodo_producao
                   );
               exception when others then
                  v_sqlerrm :=  substr (sqlerrm, 1, 2000);
                  f_habilita_trigger();
                  -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPC_144(1)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            end if;

            -- Verifica se o pacote n�o tem o primeiro est�gio e aponta a ordem com erro
            v_tem_registro := 0;

            begin
               select nvl(count(*),0)
               into v_tem_registro
               from pcpc_040
               where pcpc_040.ordem_producao           = reg_pcpc_020.ordem_producao
                 and pcpc_040.periodo_producao         = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao          = reg_pcpc_040.ordem_confeccao
                 and (pcpc_040.qtde_em_producao_pacote < 0
                 or   pcpc_040.qtde_a_produzir_pacote  < 0);
            exception when others then
               v_tem_registro := 0;
            end;

            if v_tem_registro > 0
            then
               ordem_producao_com_erro := true;

               begin
                  insert into pcpc_144 (
                     nr_solicitacao,                  codigo_erro,
                     ordem_producao,                  ordem_confeccao,
                     periodo_producao
                  ) values (
                     v_nr_solicitacao,                 2, --O em produ��o ou a produzir negativo
                     reg_pcpc_020.ordem_producao,      reg_pcpc_040.ordem_confeccao,
                     reg_pcpc_040.periodo_producao
                   );
               exception when others then
                  v_sqlerrm :=  substr (sqlerrm, 1, 2000);
                  f_habilita_trigger();
                  -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPC_144(2)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            end if;

            -- Verifica se o pacote tem seq. de est�gio 0 e seq. de estagio <> 0
            v_tem_registro := 0;

            begin
               select nvl(count(*),0)
               into v_tem_registro
               from pcpc_040
               where pcpc_040.ordem_producao    = reg_pcpc_020.ordem_producao
                 and pcpc_040.periodo_producao  = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao   = reg_pcpc_040.ordem_confeccao
                 and pcpc_040.sequencia_estagio = 0;
            exception when others then
               v_tem_registro := 0;
            end;

            if v_tem_registro != 0
            then
               v_tem_registro := 0;

               begin
                  select nvl(count(*),0)
                  into v_tem_registro
                  from pcpc_040
                  where pcpc_040.ordem_producao    = reg_pcpc_020.ordem_producao
                    and pcpc_040.periodo_producao  = reg_pcpc_040.periodo_producao
                    and pcpc_040.ordem_confeccao   = reg_pcpc_040.ordem_confeccao
                    and pcpc_040.sequencia_estagio > 0;
               exception when others then
                  v_tem_registro := 0;
               end;

               if v_tem_registro != 0
               then
                  ordem_producao_com_erro := true;

                  begin
                     insert into pcpc_144 (
                        nr_solicitacao,                  codigo_erro,
                        ordem_producao,                  ordem_confeccao,
                        periodo_producao
                     ) values (
                        v_nr_solicitacao,                 3, --Tem seq. de est�gio igual a 0 e seq. de est�gio <> 0
                        reg_pcpc_020.ordem_producao,      reg_pcpc_040.ordem_confeccao,
                        reg_pcpc_040.periodo_producao
                      );
                  exception when others then
                     v_sqlerrm :=  substr (sqlerrm, 1, 2000);
                     f_habilita_trigger();
                     -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
                     raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPC_144(3)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               end if;
            end if;

            -- Verifica se foi apontado perda ou segunda antes do estagio altera programado
            v_seq_operacao_alt_programado := 0;

            begin
               select nvl(min(pcpc_040.seq_operacao),0)
               into  v_seq_operacao_alt_programado
               from pcpc_040
               where pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                 and pcpc_040.codigo_estagio   = v_estagio_corte;
            exception when OTHERS then
               v_seq_operacao_alt_programado := 0;
            end;

            v_tem_registro := 0;

            begin
               select nvl(count(*),0)
               into  v_tem_registro
               from pcpc_040
               where pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                 and pcpc_040.seq_operacao     < v_seq_operacao_alt_programado
                 and (pcpc_040.qtde_pecas_2a   <> 0
                 or   pcpc_040.qtde_perdas     <> 0);
            exception when OTHERS then
               v_tem_registro := 0;
            end;

            if v_tem_registro > 0
            then
               ordem_producao_com_erro := true;

               begin
                  insert into pcpc_144 (
                     nr_solicitacao,                   codigo_erro,
                     ordem_producao,                   ordem_confeccao,
                     periodo_producao
                  ) values (
                     v_nr_solicitacao,                 4, --Tem apontamento de perda ou segunda antes do estagio altera programado
                     reg_pcpc_020.ordem_producao,      reg_pcpc_040.ordem_confeccao,
                     reg_pcpc_040.periodo_producao
                   );
               exception when others then
                  v_sqlerrm :=  substr (sqlerrm, 1, 2000);
                  f_habilita_trigger();
                  -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPC_144(4)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            end if;

            -- Verifica se tem uma seq. de estagio baguncada
            for reg_pcpc_0401 in (select pcpc_040.seq_operacao,
                                         pcpc_040.sequencia_estagio
                                  from pcpc_040
                                  where pcpc_040.ordem_producao   = reg_pcpc_020.ordem_producao
                                    and pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                                    and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                                   order by pcpc_040.seq_operacao,
                                           pcpc_040.sequencia_estagio)
            loop
               v_sequencia_estagio_maior := 0;

               begin
                  select nvl(max(pcpc_040.sequencia_estagio),0)
                  into v_sequencia_estagio_maior
                  from pcpc_040
                  where pcpc_040.ordem_producao   = reg_pcpc_020.ordem_producao
                    and pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                    and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                    and pcpc_040.seq_operacao     < reg_pcpc_0401.seq_operacao;
               exception when others then
                  v_tem_registro := 0;
               end;

               if v_sequencia_estagio_maior > reg_pcpc_0401.sequencia_estagio
               then
                  ordem_producao_com_erro := true;

                  begin
                     insert into pcpc_144 (
                        nr_solicitacao,                   codigo_erro,
                        ordem_producao,                   ordem_confeccao,
                        periodo_producao
                     ) values (
                        v_nr_solicitacao,                 5, --Sequencia bagun�ada
                        reg_pcpc_020.ordem_producao,      reg_pcpc_040.ordem_confeccao,
                        reg_pcpc_040.periodo_producao
                     );
                  exception when others then
                     v_sqlerrm :=  substr (sqlerrm, 1, 2000);
                     f_habilita_trigger();
                     -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
                     raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPC_144(5)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               end if;
            end loop;

            -- Verifica se no pacote o primeiro est�gio � o que tem a menor seq. de opera��o
            v_seq_operacao_prim_est := 0;

            begin
               select nvl(max(pcpc_040.seq_operacao),0)
               into v_seq_operacao_prim_est
               from pcpc_040
               where pcpc_040.ordem_producao    = reg_pcpc_020.ordem_producao
                 and pcpc_040.periodo_producao  = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao   = reg_pcpc_040.ordem_confeccao
                 and pcpc_040.estagio_anterior  = 0;
            exception when others then
               v_seq_operacao_prim_est := 0;
            end;

            v_sequencia_estagio_prim_est := 0;

            begin
               select pcpc_040.sequencia_estagio
               into v_sequencia_estagio_prim_est
               from pcpc_040
               where pcpc_040.ordem_producao    = reg_pcpc_020.ordem_producao
                 and pcpc_040.periodo_producao  = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao   = reg_pcpc_040.ordem_confeccao
                 and pcpc_040.seq_operacao      = v_seq_operacao_prim_est;
            exception when others then
               v_sequencia_estagio_prim_est := 0;
            end;

            v_seq_operacao_menor := 0;

            begin
               select nvl(min(pcpc_040.seq_operacao),0)
               into v_seq_operacao_menor
               from pcpc_040
               where pcpc_040.ordem_producao    = reg_pcpc_020.ordem_producao
                 and pcpc_040.periodo_producao  = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao   = reg_pcpc_040.ordem_confeccao;
            exception when others then
               v_seq_operacao_menor := 0;
            end;

            v_sequencia_estagio_menor := 0;

            begin
               select pcpc_040.sequencia_estagio
               into v_sequencia_estagio_menor
               from pcpc_040
               where pcpc_040.ordem_producao    = reg_pcpc_020.ordem_producao
                 and pcpc_040.periodo_producao  = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao   = reg_pcpc_040.ordem_confeccao
                 and pcpc_040.seq_operacao      = v_seq_operacao_menor;
            exception when others then
               v_sequencia_estagio_menor := 0;
            end;

            if v_seq_operacao_menor          < v_seq_operacao_prim_est
            and v_sequencia_estagio_prim_est <> v_sequencia_estagio_menor
            then
               ordem_producao_com_erro := true;

               begin
                  insert into pcpc_144 (
                     nr_solicitacao,                  codigo_erro,
                     ordem_producao,                  ordem_confeccao,
                     periodo_producao
                  ) values (
                     v_nr_solicitacao,                 6, --No pacote a primeira seq de opera��o n�o tem a menor seq. de opera��o
                     reg_pcpc_020.ordem_producao,      reg_pcpc_040.ordem_confeccao,
                     reg_pcpc_040.periodo_producao
                   );
               exception when others then
                  v_sqlerrm :=  substr (sqlerrm, 1, 2000);
                  f_habilita_trigger();
                  -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPC_144(6)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            end if;

            -- Verifica se no pacote tem apontamento para um estagio no final da ordem mas os anteriores nao foram apontados
            v_sequencia_estagio_maior := 0;

            begin
               select nvl(max(pcpc_040.sequencia_estagio),0)
               into v_sequencia_estagio_maior
               from pcpc_040, pcpc_045
               where pcpc_040.ordem_producao    = reg_pcpc_020.ordem_producao
                 and pcpc_040.periodo_producao  = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao   = reg_pcpc_040.ordem_confeccao
                 and pcpc_045.pcpc040_perconf   = pcpc_040.periodo_producao
                 and pcpc_045.pcpc040_ordconf   = pcpc_040.ordem_confeccao
                 and pcpc_045.pcpc040_estconf   = pcpc_040.codigo_estagio
                 and pcpc_045.ordem_producao    = pcpc_040.ordem_producao;
            exception when others then
                v_sequencia_estagio_maior := 0;
            end;

            v_tem_registro := 0;

            begin
               select nvl(count(*),0)
               into v_tem_registro
               from pcpc_040
               where pcpc_040.ordem_producao    = reg_pcpc_020.ordem_producao
                 and pcpc_040.periodo_producao  = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao   = reg_pcpc_040.ordem_confeccao
                 and pcpc_040.sequencia_estagio < v_sequencia_estagio_maior
                 and not exists                 (select 1
                                                 from pcpc_045
                                                 where pcpc_045.pcpc040_perconf  = pcpc_040.periodo_producao
                                                   and pcpc_045.pcpc040_ordconf  = pcpc_040.ordem_confeccao
                                                   and pcpc_045.pcpc040_estconf  = pcpc_040.codigo_estagio);
            exception when others then
               v_tem_registro := 0;
            end;

            if v_tem_registro <> 0
            then
               ordem_producao_com_erro := true;

               begin
                  insert into pcpc_144 (
                     nr_solicitacao,                  codigo_erro,
                     ordem_producao,                  ordem_confeccao,
                     periodo_producao
                  ) values (
                     v_nr_solicitacao,                 7, --No pacote tem apontamento para um estagio no final da ordem mas os anteriores nao foram apontados
                     reg_pcpc_020.ordem_producao,      reg_pcpc_040.ordem_confeccao,
                     reg_pcpc_040.periodo_producao
                   );
               exception when others then
                  v_sqlerrm :=  substr (sqlerrm, 1, 2000);
                  f_habilita_trigger();
                  -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPC_144(7)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            end if;
         end loop;
      end if;

      /*
         situacao_em_prod_a_prod
            0 - N�o calculada
            1 - Calculada
            2 - Calculada com erro
      */
      if ordem_producao_com_erro
      then
         v_tem_registro := 0;

         begin
            select nvl(count(*),0)
            into v_tem_registro
            from pcpc_040
            where pcpc_040.ordem_producao           = reg_pcpc_020.ordem_producao
              and pcpc_040.situacao_em_prod_a_prod  = 3;
         exception when others then
            v_tem_registro := 0;
         end;

         if v_tem_registro = 0
         then
            begin
               update pcpc_040
               set   pcpc_040.situacao_em_prod_a_prod  = 2,
                     pcpc_040.qtde_em_producao_pacote  = 0,
                     pcpc_040.qtde_a_produzir_pacote   = 0,
                     pcpc_040.qtde_disponivel_baixa    = 0,
                     pcpc_040.executa_trigger          = 1
               where pcpc_040.ordem_producao           = reg_pcpc_020.ordem_producao;
            exception when others then
               v_sqlerrm :=  substr (sqlerrm, 1, 2000);
               f_habilita_trigger();
               -- ATEN��O! N�o atualizou registro na tabela {0}. Status: {1}
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(9)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end if;
      else
         begin
            update pcpc_040
            set   pcpc_040.situacao_em_prod_a_prod  = 1,
                  pcpc_040.executa_trigger          = 1
            where pcpc_040.ordem_producao           = reg_pcpc_020.ordem_producao;
         exception when others then
            v_sqlerrm :=  substr (sqlerrm, 1, 2000);
            f_habilita_trigger();
            -- ATEN��O! N�o atualizou registro na tabela {0}. Status: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(9)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;

         begin
            insert into pcpc_144 (
               nr_solicitacao,                  codigo_erro,
                                                         ordem_producao,                  ordem_confeccao,
               periodo_producao
            ) values (
               v_nr_solicitacao,                 0, --Deu certo o recalculo
               reg_pcpc_020.ordem_producao,      0,
               0
            );
         exception when others then
            v_sqlerrm :=  substr (sqlerrm, 1, 2000);
            f_habilita_trigger();
            -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;

      begin
         update pcpc_040
         set   pcpc_040.executa_trigger = 0
         where pcpc_040.ordem_producao  = reg_pcpc_020.ordem_producao;
      exception when others then
         v_sqlerrm :=  substr (sqlerrm, 1, 2000);
         f_habilita_trigger();
         -- ATEN��O! N�o atualizou registro na tabela {0}. Status: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(7)' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      commit;

   end loop;

   commit;

   -- Excluir o registro que indica que esta sendo executado o recalculo
   begin
      delete from tmrp_615
      where tmrp_615.nr_solicitacao = 144
        and tmrp_615.tipo_registro  = 144
        and tmrp_615.nome_programa  = 'recalc_em_prod_a_prod';
   exception when others then
      v_sqlerrm :=  substr (sqlerrm, 1, 2000);
      f_habilita_trigger();
      -- ATEN��O! N�o excluiu {0}. Status: {1}.
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24352', 'TMRP_615' , v_sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   f_habilita_trigger();

   commit;

   --dbms_output.put_line(' OP ' || to_char(reg_pcpc_020.ordem_producao) );
   --raise_application_error(-20000,'ERRO');

end inter_pr_calc_em_prod_a_prod;

 

/

exec inter_pr_recompile;

