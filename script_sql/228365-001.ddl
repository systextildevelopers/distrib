create table supr_200(
    cgc_9           number(9,0)
    ,cgc_4          number(4,0)
    ,cgc_2          number(2,0)
    ,sequencia      number(3)
    ,informante     varchar2(240)
    ,observacao     varchar2(240)
    ,data_hora_info date
);

alter table supr_200 add constraint pk_supr_200 primary key (cgc_9, cgc_4, cgc_2, sequencia);
