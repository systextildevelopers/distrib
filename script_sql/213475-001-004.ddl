create table SUPR_526
(
  id              NUMBER(9)    not null,
  ordem_producao  NUMBER(9)    default 0,
  pacote          NUMBER(6)    default 0,
  num_requisicao  NUMBER(6)    default 0 not null,
  seq_requisicao  NUMBER(4)    default 0 not null,
  nivel_prod      VARCHAR2(1)  default '',
  grupo_prod      VARCHAR2(5)  default '',
  subgrupo_prod   VARCHAR2(3)  default '',
  item_prod       VARCHAR2(6)  default '',
  sequencia       NUMBER(3)    default 0,
  nivel_comp      VARCHAR2(1)  default '',
  grupo_comp      VARCHAR2(5)  default '',
  subgrupo_comp   VARCHAR2(3)  default '',
  item_comp       VARCHAR2(6)  default '',
  consumo         NUMBER(17,6) default 0.000000,
  estagio         NUMBER(2)    default 0,
  qtde_solicitado NUMBER(17,6) default 0.000000,
  qtde_saldo      NUMBER(17,6) default 0.000000,
  data_baixa      DATE,
  hora_baixa      DATE,
  usuario_baixa   VARCHAR2(15) default '',
  qtde_baixa      NUMBER(17,6) default 0.000000,
  baixa_estorno   VARCHAR2(1)  default '' 
);

-- Create/Recreate primary, unique and foreign key constraints 
alter table SUPR_526
  add constraint PK_SUPR_526 primary key (num_requisicao, seq_requisicao, ordem_producao, pacote, sequencia, id);
  
/

exec inter_pr_recompile;
