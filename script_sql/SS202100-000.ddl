CREATE OR REPLACE VIEW rcnb_033_param_custo AS
SELECT DISTINCT
         nivel_estrutura,
         grupo_estrutura,
         subgru_estrutura,
         item_estrutura,
         nr_solicitacao,
         cod_empresa,
         nome_usuario,
         codigo_usuario,
         codigo_programa
    FROM rcnb_033;
    COMMIT;
