CREATE TABLE  PROJ_060 
   (	SEQ_IMPORTACAO_URDIMENTO NUMBER(30,0) NOT NULL ENABLE, 
	CODIGO_EMPRESA NUMBER(3,0) NOT NULL ENABLE, 
	DATA_IMPORTACAO DATE NOT NULL ENABLE, 
	CODIGO_MAQUINA NUMBER(5,0) NOT NULL ENABLE, 
	FAMILIA_URD VARCHAR2(150) NOT NULL ENABLE, 
	FIO_URDIDO VARCHAR2(150) NOT NULL ENABLE, 
	QTDE_NEC_URDIMENTO NUMBER(12,0), 
	PERC_OCUPACAO NUMBER(5,2), 
	USUARIO_ATUALIZACAO VARCHAR2(30) NOT NULL ENABLE, 
	DATA_ATUALIZACAO DATE NOT NULL ENABLE, 
	QTDE_ESTOQUE_PROJETADO NUMBER(12,0), 
	 CONSTRAINT PK_PROJ_060 PRIMARY KEY (SEQ_IMPORTACAO_URDIMENTO)
  USING INDEX  ENABLE, 
	 CONSTRAINT UK_PROJ_060 UNIQUE (CODIGO_EMPRESA, DATA_IMPORTACAO, CODIGO_MAQUINA, FAMILIA_URD, FIO_URDIDO)
  USING INDEX  ENABLE
   );
   
/

CREATE OR REPLACE EDITIONABLE TRIGGER  INTER_TR_PROJ_060 
BEFORE INSERT OR UPDATE ON PROJ_060 FOR EACH ROW 
DECLARE 
BEGIN 
   IF inserting then  
      IF :new.seq_importacao_urdimento is null THEN  
         :new.seq_importacao_urdimento:= seq_proj_060.nextval; 
      END IF; 
   END IF; 
   :NEW.USUARIO_ATUALIZACAO:= nvl(sys_context('APEX$SESSION','APP_USER'),user); 
   :NEW.DATA_ATUALIZACAO:= sysdate;    
END INTER_TR_PROJ_060; 

/

ALTER TRIGGER  INTER_TR_PROJ_060 ENABLE;

/

CREATE OR REPLACE EDITIONABLE TRIGGER  INTER_TR_PROJ_060_LOG 
after insert or delete or update 
on proj_060 
for each row 
declare 
   ws_usuario_rede           varchar2(2000) ; 
   ws_maquina_rede           varchar2(2000) ; 
   ws_aplicativo             varchar2(2000) ; 
   ws_sid                    number; 
   ws_empresa                number; 
   ws_usuario_systextil      varchar2(2000) ; 
   ws_locale_usuario         varchar2(2000) ; 
   ws_nome_programa          varchar2(2000) ; 

begin
-- dados do usuario logado
 inter_pr_dados_usuario (ws_usuario_rede,ws_maquina_rede,ws_aplicativo,ws_sid,ws_usuario_systextil,ws_empresa,ws_locale_usuario); 
 ws_nome_programa := inter_fn_nome_programa(ws_sid);

 if inserting 
 then 
    begin 
        insert into proj_060_log (
           tipo_ocorr,   /*0*/ 
           data_ocorr,   /*1*/ 
           hora_ocorr,   /*2*/ 
           usuario_rede,   /*3*/ 
           maquina_rede,   /*4*/ 
           aplicacao,   /*5*/ 
           usuario_sistema,   /*6*/ 
           nome_programa,   /*7*/ 
           seq_importacao_urdimento, /*7.1*/ 
           codigo_empresa,   /*8*/ 
           data_importacao, /*9*/ 
           codigo_maquina, /*10*/ 
           familia_urd, /*11*/ 
           fio_urdido, /*12*/
           qtde_nec_urdimento_new,  /*13*/ 
           qtde_nec_urdimento_old, /*14*/
           perc_ocupacao_new,/*15*/ 
           perc_ocupacao_old, /*16*/
           qtde_estoque_projetado_new, /*17*/ 
           qtde_estoque_projetado_old  /*18*/
        ) values (    
           'i', /*o*/
           sysdate, /*1*/
           sysdate,/*2*/ 
           ws_usuario_rede,/*3*/ 
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/ 
           ws_nome_programa, /*7*/
           null, /*7.1*/ 
           :new.codigo_empresa,/*8*/
           :new.data_importacao, /*9*/   
           :new.codigo_maquina, /*10*/ 
           :new.familia_urd, /*11*/ 
           :new.fio_urdido, /*12*/
           :new.qtde_nec_urdimento,  /*13*/ 
           null, /*14*/
           :new.perc_ocupacao,/*15*/ 
           null, /*16*/       
           :new.qtde_estoque_projetado, /*17*/ 
           null /*18*/           
      );    
    end;    
 end if;    
  
 if updating
 then 
    begin 
        insert into proj_060_log (
           tipo_ocorr,   /*0*/ 
           data_ocorr,   /*1*/ 
           hora_ocorr,   /*2*/ 
           usuario_rede,   /*3*/ 
           maquina_rede,   /*4*/ 
           aplicacao,   /*5*/ 
           usuario_sistema,   /*6*/ 
           nome_programa,   /*7*/ 
           seq_importacao_urdimento, /*7.1*/ 
           codigo_empresa,   /*8*/ 
           data_importacao, /*9*/ 
           codigo_maquina, /*10*/ 
           familia_urd, /*11*/ 
           fio_urdido, /*12*/
           qtde_nec_urdimento_new,  /*13*/ 
           qtde_nec_urdimento_old, /*14*/
           perc_ocupacao_new,/*15*/ 
           perc_ocupacao_old, /*16*/
           qtde_estoque_projetado_new, /*17*/ 
           qtde_estoque_projetado_old  /*18*/
        ) values (    
           'a', /*o*/
           sysdate, /*1*/
           sysdate,/*2*/ 
           ws_usuario_rede,/*3*/ 
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/ 
           ws_nome_programa, /*7*/
           :new.seq_importacao_urdimento, /*7.1*/ 
           :new.codigo_empresa,/*8*/
           :new.data_importacao, /*9*/   
           :new.codigo_maquina, /*10*/ 
           :new.familia_urd, /*11*/ 
           :new.fio_urdido, /*12*/
           :new.qtde_nec_urdimento,  /*13*/ 
           :old.qtde_nec_urdimento, /*14*/
           :new.perc_ocupacao,/*15*/ 
           :old.perc_ocupacao, /*16*/ 
           :new.qtde_estoque_projetado, /*17*/ 
           :old.qtde_estoque_projetado  /*18*/      
         );    
    end;    
 end if;    
  
 if deleting 
 then 
    begin 
        insert into proj_060_log (
           tipo_ocorr,   /*0*/ 
           data_ocorr,   /*1*/ 
           hora_ocorr,   /*2*/ 
           usuario_rede,   /*3*/ 
           maquina_rede,   /*4*/ 
           aplicacao,   /*5*/ 
           usuario_sistema,   /*6*/ 
           nome_programa,   /*7*/
           seq_importacao_urdimento, /*7.1*/ 
           codigo_empresa,   /*8*/ 
           data_importacao, /*9*/ 
           codigo_maquina, /*10*/ 
           familia_urd, /*11*/ 
           fio_urdido, /*12*/
           qtde_nec_urdimento_new,  /*13*/ 
           qtde_nec_urdimento_old, /*14*/
           perc_ocupacao_new,/*15*/ 
           perc_ocupacao_old, /*16*/
           qtde_estoque_projetado_new, /*17*/ 
           qtde_estoque_projetado_old  /*18*/
       ) values (    
           'd', /*0*/
           sysdate, /*1*/
           sysdate,/*2*/ 
           ws_usuario_rede,/*3*/ 
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/ 
           ws_nome_programa, /*7*/
           :old.seq_importacao_urdimento, /*7.1*/ 
           :old.codigo_empresa,/*8*/
           :old.data_importacao, /*9*/   
           :old.codigo_maquina, /*10*/ 
           :old.familia_urd, /*11*/ 
           :old.fio_urdido, /*12*/
           null,  /*13*/ 
           :old.qtde_nec_urdimento, /*14*/
           null,/*15*/ 
           :old.perc_ocupacao, /*16*/  
           null, /*17*/ 
           :old.qtde_estoque_projetado  /*18*/                 
         );
    end;    
 end if;
end inter_tr_proj_060_log;

/

ALTER TRIGGER  INTER_TR_PROJ_060_LOG ENABLE;

/
