alter table pedi_170 
add (un_medida varchar2(2),
   cliente9  number(9)   default 0,
   cliente4  number(4)   default 0,
   cliente2  number(2)   default 0);

alter table pedi_170 drop constraint PK_PEDI_170; 

drop index PK_PEDI_170;

alter table pedi_170
  add constraint PK_PEDI_170 primary key (periodo_cotas, codigo_regiao, codigo_repr, artigo_cotas, cliente9, cliente4, cliente2);
  
exec inter_pr_recompile;
