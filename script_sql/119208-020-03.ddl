alter table obrf_874 add pertence_reg_1921 varchar2(1) default 'N';

comment on column obrf_874.pertence_reg_1921 IS 'Identifica que a mensagens que serão utilizadas
para gerar o bloco 1921 do Sped Fiscal';

exec inter_pr_recompile;
