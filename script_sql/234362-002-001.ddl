INSERT INTO EMPR_007 (PARAM,             TIPO,   LABEL,     FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES              ('obrf.dataIniFormtProd',  3,   'null', 	  ' ',         null,        null,         null,       (SELECT COALESCE((SELECT DEFAULT_DAT FROM empr_007 WHERE param = 'obrf.dataIniBlocoK'), SYSDATE+3365) FROM DUAL));

COMMIT;

declare tmpInt number;

cursor parametro_c is select codigo_empresa from fatu_500;

begin
   for reg_parametro in parametro_c
   loop
      select count(*)
      into tmpInt
      from empr_008
      where empr_008.codigo_empresa = reg_parametro.codigo_empresa
      and empr_008.param = 'obrf.dataIniFormtProd';
      
      if(tmpInt = 0)
      then
         begin
            insert into empr_008 ( codigo_empresa, param, val_dat)
            values (reg_parametro.codigo_empresa, 'obrf.dataIniFormtProd', (SELECT COALESCE((SELECT val_dat FROM empr_008 WHERE codigo_empresa = reg_parametro.codigo_empresa AND param = 'obrf.dataIniBlocoK'), SYSDATE+3365)FROM DUAL));
         end;
      end if;

   end loop;
   
   commit;
end;
