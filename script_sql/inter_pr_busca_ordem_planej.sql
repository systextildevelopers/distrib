
  CREATE OR REPLACE PROCEDURE "INTER_PR_BUSCA_ORDEM_PLANEJ" (
p_nivel     in varchar,
p_grupo     in varchar,
p_subgrupo  in varchar,
p_item      in varchar,
p_alternativa in number,
curSQL      out sys_refcursor
)
is
begin

open curSQL for
   select tmrp_virtual.ordem_planejamento,
          tmrp_virtual.pedido_venda,
          tmrp_virtual.pedido_reserva,
          tmrp_virtual.nivel_produto_origem,
          tmrp_virtual.grupo_produto_origem
   from (
   select tmrp_610.ordem_planejamento,
          tmrp_625.pedido_venda,
          tmrp_625.pedido_reserva,
          '0'     as nivel_produto_origem,
          '00000' as grupo_produto_origem
   from tmrp_625, tmrp_620, tmrp_610
   where   tmrp_620.ordem_planejamento       = tmrp_625.ordem_planejamento
     and   tmrp_620.pedido_venda             = tmrp_625.pedido_venda
     and   tmrp_620.pedido_reserva           = tmrp_625.pedido_reserva
     and   tmrp_620.nivel_produto            = tmrp_625.nivel_produto
     and   tmrp_620.grupo_produto            = tmrp_625.grupo_produto
     and   tmrp_620.subgrupo_produto         = tmrp_625.subgrupo_produto
     and   tmrp_620.item_produto             = tmrp_625.item_produto
     and   tmrp_620.alternativa_produto      = p_alternativa
     and   tmrp_625.ordem_planejamento       = tmrp_610.ordem_planejamento
     and   tmrp_610.situacao_ordem           < 9
     and   tmrp_625.nivel_produto_origem     = '0'
     and   tmrp_625.grupo_produto_origem     = '00000'
     and   tmrp_625.subgrupo_produto_origem  = '000'
     and   tmrp_625.item_produto_origem      = '000000'
     and   tmrp_625.nivel_produto            = p_nivel
     and   tmrp_625.grupo_produto            = p_grupo
     and  (tmrp_625.subgrupo_produto         = p_subgrupo or p_subgrupo = '000')
     and  (tmrp_625.item_produto             = p_item     or p_item     = '000000')
     and ((tmrp_625.qtde_areceber_programada > 0
     and  exists (select 1 from tmrp_041, pcpc_020, tmrp_630
                  where tmrp_041.periodo_producao   = pcpc_020.periodo_producao
                    and tmrp_041.area_producao      = 1
                    and tmrp_041.nr_pedido_ordem    = tmrp_630.ordem_prod_compra
                    and tmrp_041.codigo_estagio     = pcpc_020.ultimo_estagio
                    and tmrp_041.nivel_estrutura    = tmrp_625.nivel_produto
                    and tmrp_041.grupo_estrutura    = tmrp_625.grupo_produto
                    and tmrp_041.subgru_estrutura   = tmrp_625.subgrupo_produto
                    and tmrp_041.item_estrutura     = tmrp_625.item_produto
                    and tmrp_041.qtde_areceber      > 0
                    and pcpc_020.ordem_producao     = tmrp_630.ordem_prod_compra
                    and pcpc_020.cod_cancelamento   = 0
                    and tmrp_630.ordem_planejamento = tmrp_610.ordem_planejamento
                   and tmrp_630.area_producao      = 1))
      or tmrp_625.qtde_areceber_programada = 0)
   group by tmrp_610.ordem_planejamento , tmrp_625.pedido_venda, tmrp_625.pedido_reserva
   UNION ALL
   select tmrp_610.ordem_planejamento,
          tmrp_625.pedido_venda,
          tmrp_625.pedido_reserva,
          tmrp_625.nivel_produto_origem,
          tmrp_625.grupo_produto_origem
   from tmrp_625, tmrp_620, tmrp_610
   where   tmrp_620.ordem_planejamento       = tmrp_625.ordem_planejamento
     and   tmrp_620.pedido_venda             = tmrp_625.pedido_venda
     and   tmrp_620.pedido_reserva           = tmrp_625.pedido_reserva
     and   tmrp_620.nivel_produto            = tmrp_625.nivel_produto_origem
     and   tmrp_620.grupo_produto            = tmrp_625.grupo_produto_origem
     and   tmrp_620.subgrupo_produto         = tmrp_625.subgrupo_produto_origem
     and   tmrp_620.item_produto             = tmrp_625.item_produto_origem
     and   tmrp_620.alternativa_produto      = p_alternativa
     and   tmrp_625.ordem_planejamento       = tmrp_610.ordem_planejamento
     and   tmrp_610.situacao_ordem           < 9
     and   tmrp_625.nivel_produto_origem     = '1'
     and   tmrp_625.nivel_produto            = p_nivel
     and   tmrp_625.grupo_produto            = p_grupo
     and  (tmrp_625.subgrupo_produto         = p_subgrupo or p_subgrupo = '000')
     and  (tmrp_625.item_produto             = p_item     or p_item     = '000000')
     and ((tmrp_625.qtde_areceber_programada > 0
     and  exists (select 1 from tmrp_041, pcpc_020, tmrp_630
                  where tmrp_041.periodo_producao   = pcpc_020.periodo_producao
                    and tmrp_041.area_producao      = 1
                    and tmrp_041.nr_pedido_ordem    = tmrp_630.ordem_prod_compra
                    and tmrp_041.codigo_estagio     = pcpc_020.ultimo_estagio
                    and tmrp_041.nivel_estrutura    = tmrp_625.nivel_produto
                    and tmrp_041.grupo_estrutura    = tmrp_625.grupo_produto
                    and tmrp_041.subgru_estrutura   = tmrp_625.subgrupo_produto
                    and tmrp_041.item_estrutura     = tmrp_625.item_produto
                    and tmrp_041.qtde_areceber      > 0
                    and pcpc_020.ordem_producao     = tmrp_630.ordem_prod_compra
                    and pcpc_020.cod_cancelamento   = 0
                    and tmrp_630.ordem_planejamento = tmrp_610.ordem_planejamento
                   and tmrp_630.area_producao      = 1))
      or tmrp_625.qtde_areceber_programada = 0)
   group by tmrp_610.ordem_planejamento,
            tmrp_625.pedido_venda,
            tmrp_625.pedido_reserva,
            tmrp_625.nivel_produto_origem,
            tmrp_625.GRUPO_produto_origem) tmrp_virtual
   group by tmrp_virtual.ordem_planejamento,
            tmrp_virtual.pedido_venda,
            tmrp_virtual.pedido_reserva,
            tmrp_virtual.nivel_produto_origem,
            tmrp_virtual.grupo_produto_origem;

End inter_pr_busca_ordem_planej;



 

/

exec inter_pr_recompile;

