
alter table basi_265 add codigo_iso varchar2(3);

comment on column basi_265.codigo_iso is 'Conforme o ISO 4217, informe o código de moeda composto por três letras.';
