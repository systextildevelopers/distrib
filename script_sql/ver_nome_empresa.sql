
  CREATE OR REPLACE FUNCTION "VER_NOME_EMPRESA" 
        (codigo_emp in fatu_500.codigo_empresa%type)
RETURN varchar2
IS
   v_nome_empresa varchar2(50);
begin
   select nvl(nome_empresa,'Nao cadastrada') into v_nome_empresa from fatu_500
   where fatu_500.codigo_empresa            = codigo_emp;
   if sql%notfound
   then
      v_nome_empresa:='Nao Cadastrada';
   end if;
   return(v_nome_empresa);
end ver_nome_empresa;

 

/

exec inter_pr_recompile;

