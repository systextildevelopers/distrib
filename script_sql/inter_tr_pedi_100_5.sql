
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_100_5" 
AFTER INSERT or DELETE on pedi_100
for each row

declare
  v_reg_pedi_620   number(03);
  v_reg_pedi_625   number(01);
  v_executa_trigger   number(1);
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting
      then
         inter_pr_insere_posicao_ped(:new.pedido_venda,:new.data_digit_venda);
      else
         select count(*)
         into v_reg_pedi_620
         from pedi_620
         where pedido = :old.pedido_venda;
         if v_reg_pedi_620 > 0
         then
            DELETE pedi_620
            where pedido = :old.pedido_venda;
         end if;

         select count(*)
         into v_reg_pedi_625
         from pedi_625
         where pedido = :old.pedido_venda;
         if v_reg_pedi_625 > 0
         then
            DELETE pedi_625
            where pedido = :old.pedido_venda;
         end if;
      end if;
   end if;
end inter_tr_pedi_100_5;
-- ALTER TRIGGER "INTER_TR_PEDI_100_5" ENABLE
 

/

exec inter_pr_recompile;

