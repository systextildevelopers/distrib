
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_010_PROD" 
   before update of qtde_quilos_prod
   on pcpt_010
   for each row
begin
   begin
      update tmrp_650
      set tmrp_650.quantidade_produzida = :new.qtde_quilos_prod
      where tmrp_650.tipo_alocacao  = 4
        and tmrp_650.tipo_recurso   = 1
        and tmrp_650.ordem_trabalho = :new.ordem_tecelagem;
   exception when others then
      raise_application_error(-20000, 'Nao atualizou TMRP_650. ' || sqlerrm);
   end;
end inter_tr_pcpt_010_prod;
-- ALTER TRIGGER "INTER_TR_PCPT_010_PROD" ENABLE
 

/

exec inter_pr_recompile;

