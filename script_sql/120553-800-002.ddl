alter table estq_080 add descr_fabricante varchar2(30);

comment on column estq_080.descr_fabricante is 'Descrição do fabricante do lote/produto';

exec inter_pr_recompile;
