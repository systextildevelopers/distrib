create or replace TRIGGER "INTER_TR_TMRP_650_HIST" 
    before insert or
            delete or
            update 
            on tmrp_650
    for each row
    declare
        ws_usuario_rede           varchar2(20);
        ws_maquina_rede           varchar2(40);
        ws_aplicativo             varchar2(20);
        ws_sid                    number(9);
        ws_empresa                number(3);
        ws_usuario_systextil      varchar2(250);
        ws_locale_usuario         varchar2(5);

        --   long_aux                  varchar2(4000);
begin
    -- Dados do usuario logado
    inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
               ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

    if ws_aplicativo is null then
        ws_aplicativo := 'SQL';
    end if;

    if inserting then
        begin
            insert into tmrp_650_hist(
                ordem_trabalho,      tipo_alocacao,
                codigo_recurso,      pedido_venda,
                data_inicio,         data_termino,
                hora_inicio,         hora_termino,
                processo_systextil,  usuario_systextil,
                usuario_rede,        data_hora,
                acao,                numero_id
            )values(
                :new.ordem_trabalho, :new.tipo_alocacao,
                :new.codigo_recurso, :new.pedido_venda,
                :new.data_inicio,    :new.data_termino,
                :new.hora_inicio,    :new.hora_termino,
                ws_aplicativo,       ws_usuario_systextil,
                ws_usuario_rede,     sysdate,
                'I',                 -1
            );
            /*     exception
            when OTHERS then
            raise_application_error(-20000,sqlerrm || ' Erro ao atualizar historico do tmrp_650.');*/
        end;
    end if;


    if deleting then
        /*long_aux := 'Ordem Trabalho: ' || :old.ordem_trabalho || chr(10) ||
          ' Tipo Alocacao: ' || :old.tipo_alocacao || chr(10) ||
          ' Codigo Recurso: ' || :old.codigo_recurso || chr(10) ||
          ' Pedido Venda: ' || :old.pedido_venda || chr(10) ||
          ' Data Inicio: ' || :old.data_inicio || chr(10) ||
          ' Hora Inicio: ' || :old.hora_inicio || chr(10) ||
          ' Data Termino: ' || :old.data_termino || chr(10) ||
          ' Hora Inicio: ' || :old.hora_inicio || chr(10) ||
          ' Processo Systextil: ' || ws_aplicativo || chr(10) ||
          ' Usuario Systextil: ' || ws_usuario_systextil || chr(10) ||
          ' Usuario Rede: ' || ws_usuario_rede || chr(10) ||
          ' Data / Hora: ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS') || chr(10) ||
          ' Acao: D';*/

        begin
            insert into tmrp_650_hist(
                ordem_trabalho,      tipo_alocacao,
                codigo_recurso,      pedido_venda,
                data_inicio,         data_termino,
                hora_inicio,         hora_termino,
                processo_systextil,  usuario_systextil,
                usuario_rede,        data_hora,
                acao,                numero_id
            )values(
                :old.ordem_trabalho, :old.tipo_alocacao,
                :old.codigo_recurso, :old.pedido_venda,
                :old.data_inicio,    :old.data_termino,
                :old.hora_inicio,    :old.hora_termino,
                ws_aplicativo,       ws_usuario_systextil,
                ws_usuario_rede,     sysdate,
                'D',                 -1
            );
            /*     exception
            when OTHERS then
            raise_application_error(-20000,sqlerrm || ' Erro ao atualizar historico do tmrp_650.');*/
        end;
    end if;

    if updating then
        begin
            insert into tmrp_650_hist(
                ordem_trabalho,     old_ordem_trabalho,      
                tipo_alocacao,      old_tipo_alocacao,
                codigo_recurso,     old_codigo_recurso,      
                pedido_venda,       old_pedido_venda,
                data_inicio,        old_data_inicio,         
                data_termino,       old_data_termino,
                hora_inicio,        old_hora_inicio,         
                hora_termino,       old_hora_termino,
                processo_systextil, usuario_systextil,
                usuario_rede,       data_hora,
                acao,               numero_id
            )values(
                :new.ordem_trabalho,    :old.ordem_trabalho,
                :new.tipo_alocacao,     :old.tipo_alocacao,
                :new.codigo_recurso,    :old.codigo_recurso,
                :new.pedido_venda,      :old.pedido_venda,
                :new.data_inicio,       :old.data_inicio,
                :new.data_termino,      :old.data_termino,
                :new.hora_inicio,       :old.hora_inicio,
                :new.hora_termino,      :old.hora_termino,
                ws_aplicativo,       ws_usuario_systextil,
                ws_usuario_rede,     sysdate,
                'A',                 -1
            );
            /*     exception
            when OTHERS then
            raise_application_error(-20000,sqlerrm || ' Erro ao atualizar historico do tmrp_650.');*/
        end;
    end if;
end inter_tr_tmrp_650_hist;
-- ALTER TRIGGER "INTER_TR_TMRP_650_HIST" ENABLE

/

exec inter_pr_recompile;
