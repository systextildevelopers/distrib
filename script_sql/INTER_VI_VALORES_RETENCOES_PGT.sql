CREATE OR REPLACE FORCE VIEW INTER_VI_VALORES_RETENCOES_PGT (PERIODO_APURACAO, CODIGO_EMPRESA, NR_DUPLICATA, PARCELA, TIPO_TITULO, CNPJ9_FORNECEDOR, CNPJ4_FORNECEDOR, CNPJ2_FORNECEDOR, CNPJ_CPF_FORNECEDOR, NOME_FORNECEDOR, CIDADE_FORNECEDOR, UF_FORNECEDOR, SERIE_NF, DATA_EMISSAO, DATA_VENCIMENTO, DATA_PAGAMENTO, VALOR_PARCELA, COD_RETENCAO_PIS, BASE_PIS, ALIQUOTA_PIS, VALOR_PIS, DATA_VENCTO_PIS, COD_RETENCAO_COFINS, BASE_COFINS, ALIQUOTA_COFINS, VALOR_COFINS, DATA_VENCTO_COFINS, COD_RETENCAO_CSL, BASE_CSL, ALIQUOTA_CSL, VALOR_CSL, DATA_VENCTO_CSL) AS 
  select trunc(data_pagamento, 'DD')                                    as PERIODO_APURACAO,       
       codigo_empresa                                                 as CODIGO_EMPRESA,
       nr_duplicata                                                   as NR_DUPLICATA,
       parcela                                                        as PARCELA,
       tipo_titulo                                                    as TIPO_TITULO,
       cnpj9_fornecedor                                               as CNPJ9_FORNECEDOR,
       cnpj4_fornecedor                                               as CNPJ4_FORNECEDOR,
       cnpj2_fornecedor                                               as CNPJ2_FORNECEDOR,
       cnpj_cpf_fornecedor                                            as CNPJ_CPF_FORNECEDOR,
       nome_fornecedor                                                as NOME_FORNECEDOR,
       cidade_fornecedor                                              as CIDADE_FORNECEDOR,
       uf_fornecedor                                                  as UF_FORNECEDOR,
       serie_nf                                                       as SERIE_NF,
       data_emissao                                                   as DATA_EMISSAO,
       data_vencimento                                                as DATA_VENCIMENTO,
       data_pagamento                                                 as DATA_PAGAMENTO,
       valor_parcela                                                  as VALOR_PARCELA,
       cod_retencao_pis                                               as COD_RETENCAO_PIS,
       base_pis                                                       as BASE_PIS,
       aliquota_pis                                                   as ALIQUOTA_PIS,
       valor_pis                                                      as VALOR_PIS,
       inter_fn_data_vencto_retencoes(data_pagamento, 'PIS')          as DATA_VENCTO_PIS,
       cod_retencao_cofins                                            as COD_RETENCAO_COFINS,
       base_cofins                                                    as BASE_COFINS,
       aliquota_cofins                                                as ALIQUOTA_COFINS,
       valor_cofins                                                   as VALOR_COFINS,
       inter_fn_data_vencto_retencoes(data_pagamento, 'COFINS')       as DATA_VENCTO_COFINS,
       cod_retencao_csl                                               as COD_RETENCAO_CSL,
       base_csl                                                       as BASE_CSL,
       aliquota_csl                                                   as ALIQUOTA_CSL,
       valor_csl                                                      as VALOR_CSL,
       inter_fn_data_vencto_retencoes(data_pagamento, 'CSL')          as DATA_VENCTO_CSL
from (
   select (select min(cpag_015.data_pagamento) from cpag_015    
           where cpag_010.nr_duplicata = cpag_015.dupl_for_nrduppag
           and   cpag_010.parcela      = cpag_015.dupl_for_no_parc
           and   cpag_010.cgc_9        = cpag_015.dupl_for_for_cli9
           and   cpag_010.cgc_4        = cpag_015.dupl_for_for_cli4
           and   cpag_010.cgc_2        = cpag_015.dupl_for_for_cli2
           and   cpag_010.tipo_titulo  = cpag_015.dupl_for_tipo_tit
           and   cpag_015.valor_pago   > 0)                                       as DATA_PAGAMENTO,       
          cpag_010.codigo_empresa                                                 as CODIGO_EMPRESA,
          cpag_010.nr_duplicata                                                   as NR_DUPLICATA,
          cpag_010.parcela                                                        as PARCELA,
          cpag_010.tipo_titulo                                                    as TIPO_TITULO,
          cpag_010.cgc_9                                                          as CNPJ9_FORNECEDOR,
          cpag_010.cgc_4                                                          as CNPJ4_FORNECEDOR,
          cpag_010.cgc_2                                                          as CNPJ2_FORNECEDOR,
          case
             when cpag_010.cgc_4 = 0 then
                trim(to_char(cpag_010.cgc_9, '000000000')) || '-' ||
                trim(to_char(cpag_010.cgc_2, '00'))
             else
                trim(to_char(cpag_010.cgc_9, '00000000'))  || '/' ||
                trim(to_char(cpag_010.cgc_4, '0000'))      || '-' ||
                trim(to_char(cpag_010.cgc_2, '00'))
          end                                                                     as CNPJ_CPF_FORNECEDOR,
          supr_010.nome_fornecedor                                                as NOME_FORNECEDOR,
         (select basi_160.cidade from basi_160
          where basi_160.cod_cidade = supr_010.cod_cidade)                        as CIDADE_FORNECEDOR,
         (select basi_160.estado from basi_160
          where basi_160.cod_cidade = supr_010.cod_cidade)                        as UF_FORNECEDOR,
          cpag_010.serie                                                          as SERIE_NF,
          cpag_010.data_contrato                                                  as DATA_EMISSAO,
          cpag_010.data_vencimento                                                as DATA_VENCIMENTO,
          cpag_010.valor_parcela                                                  as VALOR_PARCELA,
          cpag_010.cod_ret_pis                                                    as COD_RETENCAO_PIS,
          cpag_010.base_pis                                                       as BASE_PIS,
          cpag_010.aliq_pis                                                       as ALIQUOTA_PIS,
          cpag_010.valor_pis_imp                                                  as VALOR_PIS,
          cpag_010.cod_ret_cofins                                                 as COD_RETENCAO_COFINS,
          cpag_010.base_cofins                                                    as BASE_COFINS,
          cpag_010.aliq_cofins                                                    as ALIQUOTA_COFINS,
          cpag_010.valor_cofins_imp                                               as VALOR_COFINS,
          cpag_010.cod_ret_csl                                                    as COD_RETENCAO_CSL,
          cpag_010.base_csl                                                       as BASE_CSL,
          cpag_010.aliq_csl                                                       as ALIQUOTA_CSL,
          cpag_010.valor_csl_imp                                                  as VALOR_CSL
   from cpag_010, supr_010
   where cpag_010.cgc_9            = supr_010.fornecedor9
   and   cpag_010.cgc_4            = supr_010.fornecedor4
   and   cpag_010.cgc_2            = supr_010.fornecedor2
   and  (cpag_010.valor_pis_imp    > 0.00
   or    cpag_010.valor_cofins_imp > 0.00
   or    cpag_010.valor_csl_imp    > 0.00)
   and  exists (select 1 from cpag_015
                where cpag_010.nr_duplicata = cpag_015.dupl_for_nrduppag
                and   cpag_010.parcela      = cpag_015.dupl_for_no_parc
                and   cpag_010.cgc_9        = cpag_015.dupl_for_for_cli9
                and   cpag_010.cgc_4        = cpag_015.dupl_for_for_cli4
                and   cpag_010.cgc_2        = cpag_015.dupl_for_for_cli2
                and   cpag_010.tipo_titulo  = cpag_015.dupl_for_tipo_tit
                and   cpag_015.valor_pago   > 0));
                
/
