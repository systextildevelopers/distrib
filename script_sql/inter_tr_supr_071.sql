
  CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_071" 

   before insert or
          delete or
          update of pedido_compra, seq_item_pedido, nivel_item_ped, grupo_item_ped,
		    subgrupo_item_ped, item_item_ped, tipo_relacionamento, num_relacionamento,
		    qtde_item
   on supr_071
   for each row

declare

begin
   if inserting
   then
      if :new.tipo_relacionamento = 4
      then
         -- Incluir ligacao com a ordem de planejamento
         inter_pr_destinos_planejamento(:new.num_relacionamento,
                                        :new.pedido_compra,
                                        :new.qtde_item,
                                        :new.nivel_item_ped,
                                        :new.grupo_item_ped,
                                        :new.subgrupo_item_ped,
                                        :new.item_item_ped,
                                        1,
                                        'I');
      end if;
   end if;

   if deleting
   then
      if :old.tipo_relacionamento = 4
      then
         -- Procedimento que vai executar a explosao da estrutura
         -- do produto e atualizar a quantidade programada na ordem de planejamento
         -- Para retirar a quantidade da ordem antiga
         /*inter_pr_ordem_planejamento('pcpt',
                                     :old.num_relacionamento,
                                     p_cd_pano_nivel99,
                                     p_cd_pano_grupo,
                                     p_cd_pano_subgrupo,
                                     p_cd_pano_item,
                                     p_alternativa_item,
                                     :old.quantidade * -1);*/

         -- Excluir ligacao com a ordem de planejamento
         inter_pr_destinos_planejamento(:old.num_relacionamento,
                                        :old.pedido_compra,
                                        :old.qtde_item * (-1),
                                        :old.nivel_item_ped,
                                        :old.grupo_item_ped,
                                        :old.subgrupo_item_ped,
                                        :old.item_item_ped,
                                        1,
                                        'D');
      end if;
   end if;

   if updating
   then
      if :new.tipo_relacionamento = 4 or :old.tipo_relacionamento = 4
      then
         if :old.tipo_relacionamento = 4 and :new.tipo_relacionamento <> 4
         then
            -- Procedimento que vai executar a explosao da estrutura
            -- do produto e atualizar a quantidade programada na ordem de planejamento
            -- Para retirar a quantidade da ordem antiga
            /*inter_pr_ordem_planejamento('pcpt',
                                        :old.num_relacionamento,
                                        p_cd_pano_nivel99,
                                        p_cd_pano_grupo,
                                        p_cd_pano_subgrupo,
                                        p_cd_pano_item,
                                        p_alternativa_item,
                                        :old.quantidade * -1);*/

            -- Excluir ligacao com a ordem de planejamento
            inter_pr_destinos_planejamento(:old.num_relacionamento,
                                           :old.pedido_compra,
                                           :old.qtde_item,
                                           :old.nivel_item_ped,
                                           :old.grupo_item_ped,
                                           :old.subgrupo_item_ped,
                                           :old.item_item_ped,
                                           1,
                                           'D');
         else
            -- Se alterar a Ordem de Planejamento da Ordem de Tecelagem
            if :old.tipo_relacionamento = :new.tipo_relacionamento
            then
               -- Procedimento que vai executar a explosao da estrutura
               -- do produto e atualizar a quantidade programada na ordem de planejamento
               -- Para retirar a quantidade da ordem antiga
               /*inter_pr_ordem_planejamento('pcpt',
                                           :old.num_relacionamento,
                                           p_cd_pano_nivel99,
                                           p_cd_pano_grupo,
                                           p_cd_pano_subgrupo,
                                           p_cd_pano_item,
                                           p_alternativa_item,
                                           :old.quantidade * -1);*/

               -- Excluir ligacao com a ordem de planejamento
               /*delete from tmrp_630
               where tmrp_630.ordem_planejamento    = :old.num_relacionamento
                 and tmrp_630.area_producao         = 4
                 and tmrp_630.ordem_prod_compra     = :old.pedido_compra
                 and tmrp_630.nivel_produto         = p_cd_pano_nivel99
                 and tmrp_630.grupo_produto         = p_cd_pano_grupo
                 and tmrp_630.subgrupo_produto      = p_cd_pano_subgrupo
                 and tmrp_630.item_produto          = p_cd_pano_item;
               */
               -- Procedimento que vai executar a explosao da estrutura
               -- do produto e atualizar a quantidade programada na ordem de planejamento
               -- Para acrescentar a quantidade da ordem nova
               inter_pr_destinos_planejamento(:old.num_relacionamento,
                                              :old.pedido_compra,
                                              :new.qtde_item - :old.qtde_item,
                                              :old.nivel_item_ped,
                                              :old.grupo_item_ped,
                                              :old.subgrupo_item_ped,
                                              :old.item_item_ped,
                                              1,
                                              'U');
            end if;
         end if;
      end if;
   end if;
end inter_tr_supr_071;

-- ALTER TRIGGER "INTER_TR_SUPR_071" ENABLE
 

/

exec inter_pr_recompile;

