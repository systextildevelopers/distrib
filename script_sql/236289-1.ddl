alter table tmrp_695 add situacao_bloqueio number(1) default 0;
comment on column tmrp_695.situacao_bloqueio is 'Situa��o de bloqueio informada no quadro Distribui��o Ordens da Proje��o (tmrp_f690), que ser� gravada na Ordem de Produ��o (pcpc_020.situacao_bloqueio).';
