declare

    v_tag    varchar2(10);
    v_column varchar2(100);

begin

    for x in(

        SELECT 
            REGEXP_SUBSTR(
                'lb66164@#nr_mtv_prorrogacao:
                ;lb66165@#nr_remessa_inadimplencia:
                ;lb66166@#nr_solicitacao:
                ;lb66167@#nr_solicitacao_inadimplencia:
                ;lb66168@#nr_titulo_banco_corresp:
                ;lb66169@#numero_bordero:
                ;lb66170@#numero_caixa:
                ;lb66171@#numero_nsu:
                ;lb66172@#numero_processo:
                ;lb66173@#numero_referencia:
                ;lb66174@#numero_remessa:
                ;lb66175@#numero_sequencia:
                ;lb66176@#numero_titulo:
                ;lb66177@#num_contabil:
                ;lb66178@#num_dup_origem:
                ;lb66179@#num_nota_fiscal:
                ;lb66180@#num_renegociacao:
                ;lb66182@#ordem_credito_opera:
                ;lb66183@#origem_pedido:
                ;lb66184@#pedido_operadora:
                ;lb66185@#pedido_venda:
                ;lb66186@#port_anterior:
                ;lb66187@#posicao_ant:
                ;lb66188@#posicao_duplic:
                ;lb66189@#previsao:
                ;lb66190@#quantidade:
                ;lb66191@#referente_nf:
                ;lb66192@#ren_num_renegocia:
                ;lb66193@#ren_sit_renegocia:
                ;lb66194@#responsavel_receb:
                ;lb66195@#resumo_venda:
                ;lb66196@#scpc_nr_remessa:
                ;lb66197@#scpc_situacao:
                ;lb66198@#selecionado_credito_reneg:
                ;lb66199@#seq_dup_origem:
                ;lb66200@#seq_renegociacao:
                ;lb66201@#serie_nota_fisc:
                ;lb66202@#situacao_inadimplencia:
                ;lb66203@#status_credito_opera:
                ;lb66204@#status_nsu:
                ;lb66205@#tecido_peca:
                ;lb66206@#tid:
                ;lb66207@#tipopix:
                ;lb66208@#tipo_titulo_original:
                ;lb66209@#tipo_tit_origem:
                ;lb66210@#tipo_transacao_opera:
                ;lb66211@#titulo_loja:
                ;lb66212@#tit_baixar:
                ;lb66213@#tit_renegociado:
                ;lb66214@#valor_avp:
                ;lb66215@#valor_comis:
                ;lb66216@#valor_desconto_aux:
                ;lb66217@#valor_desp_cobr:
                ;lb66218@#valor_juros_aux:
                ;lb66219@#valor_moeda:
                ;lb66220@#valor_remessa:
                ;lb66221@#valor_saldo_aux:
                ;lb66222@#variacao_cambial:
                ;lb66223@#vencto_anterior:
                ;lb66224@#vlr_presente:
                ;lb66225@#vl_liquido_cartao:',
                '[^;]+', 
                1, 
                LEVEL
            ) AS column_value
        FROM 
            DUAL
        CONNECT BY 
            LEVEL <= REGEXP_COUNT(
                'lb66164@#nr_mtv_prorrogacao:
                ;lb66165@#nr_remessa_inadimplencia:
                ;lb66166@#nr_solicitacao:
                ;lb66167@#nr_solicitacao_inadimplencia:
                ;lb66168@#nr_titulo_banco_corresp:
                ;lb66169@#numero_bordero:
                ;lb66170@#numero_caixa:
                ;lb66171@#numero_nsu:
                ;lb66172@#numero_processo:
                ;lb66173@#numero_referencia:
                ;lb66174@#numero_remessa:
                ;lb66175@#numero_sequencia:
                ;lb66176@#numero_titulo:
                ;lb66177@#num_contabil:
                ;lb66178@#num_dup_origem:
                ;lb66179@#num_nota_fiscal:
                ;lb66180@#num_renegociacao:
                ;lb66182@#ordem_credito_opera:
                ;lb66183@#origem_pedido:
                ;lb66184@#pedido_operadora:
                ;lb66185@#pedido_venda:
                ;lb66186@#port_anterior:
                ;lb66187@#posicao_ant:
                ;lb66188@#posicao_duplic:
                ;lb66189@#previsao:
                ;lb66190@#quantidade:
                ;lb66191@#referente_nf:
                ;lb66192@#ren_num_renegocia:
                ;lb66193@#ren_sit_renegocia:
                ;lb66194@#responsavel_receb:
                ;lb66195@#resumo_venda:
                ;lb66196@#scpc_nr_remessa:
                ;lb66197@#scpc_situacao:
                ;lb66198@#selecionado_credito_reneg:
                ;lb66199@#seq_dup_origem:
                ;lb66200@#seq_renegociacao:
                ;lb66201@#serie_nota_fisc:
                ;lb66202@#situacao_inadimplencia:
                ;lb66203@#status_credito_opera:
                ;lb66204@#status_nsu:
                ;lb66205@#tecido_peca:
                ;lb66206@#tid:
                ;lb66207@#tipopix:
                ;lb66208@#tipo_titulo_original:
                ;lb66209@#tipo_tit_origem:
                ;lb66210@#tipo_transacao_opera:
                ;lb66211@#titulo_loja:
                ;lb66212@#tit_baixar:
                ;lb66213@#tit_renegociado:
                ;lb66214@#valor_avp:
                ;lb66215@#valor_comis:
                ;lb66216@#valor_desconto_aux:
                ;lb66217@#valor_desp_cobr:
                ;lb66218@#valor_juros_aux:
                ;lb66219@#valor_moeda:
                ;lb66220@#valor_remessa:
                ;lb66221@#valor_saldo_aux:
                ;lb66222@#variacao_cambial:
                ;lb66223@#vencto_anterior:
                ;lb66224@#vlr_presente:
                ;lb66225@#vl_liquido_cartao:', 
                ';'
            ) + 1
    )loop

        v_tag    := substr(x.column_value,1,7);
        v_column := replace(upper(substr(x.column_value,10)),'_',' ');

        -- htp.p(v_tag ||' - '|| v_column);

        begin

            insert into HDOC_870 (TAG, LOCALE, DESCRICAO, DATA_CADASTRO, FLAG_ATUALIZADO, FLAG_REPASSADO, USUARIO_TRADUCAO)
            values(v_tag, 'pt_BR', v_column, SYSDATE, 0, 4, 'CAIO.B@SYSTEXTIL.COM.BR');

            insert into HDOC_870 (TAG, LOCALE, DESCRICAO, DATA_CADASTRO, FLAG_ATUALIZADO, FLAG_REPASSADO, USUARIO_TRADUCAO)
            values(v_tag, 'es_ES', v_column, SYSDATE, 0, 4, 'CAIO.B@SYSTEXTIL.COM.BR');

        exception
            when dup_val_on_index then
                null;
        end;

    end loop;

end; --;
