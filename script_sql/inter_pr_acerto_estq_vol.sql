
  CREATE OR REPLACE PROCEDURE "INTER_PR_ACERTO_ESTQ_VOL" 
is

   -- Criando o loop principal de deposito
   cursor basi205 is
      select basi_205.codigo_deposito,          basi_205.tipo_volume,
             basi_206.transacao_acerto_entrada, basi_206.transacao_acerto_saida
      from basi_205, basi_206
      where basi_206.deposito_acerto = basi_205.codigo_deposito
        and basi_205.tipo_volume    in (2,4,7,9)
      order by basi_206.deposito_acerto;

   -- Criando o loop de tecidos acabados e tecidos crus
   cursor pcpt020 (p_deposito_lido number) is
      select panoacab_nivel99 nivel,    panoacab_grupo  grupo,      panoacab_subgrupo subgrupo,
             panoacab_item    item,     codigo_deposito deposito,   lote_acomp lote,
             sum(qtde_quilos_acab) estoque_rolos
      from pcpt_020
      where rolo_estoque     not in (0,2)
        and panoacab_nivel99 is not null
        and codigo_deposito  = p_deposito_lido
      group by panoacab_nivel99, panoacab_grupo, panoacab_subgrupo, panoacab_item,
               codigo_deposito,  lote_acomp;

   -- Criando o loop de caixas
   cursor estq060 (p_deposito_lido number) is
      select prodcai_nivel99 nivel,      prodcai_grupo grupo,        prodcai_subgrupo subgrupo,
             prodcai_item    item,       codigo_deposito deposito,   lote lote,
             sum(peso_liquido) estoque_caixas
      from estq_060
      where status_caixa   not in (4,8,9)
        and codigo_deposito = p_deposito_lido
        and prodcai_nivel99 is not null
      group by prodcai_nivel99 ,   prodcai_grupo ,     prodcai_subgrupo ,
               prodcai_item    ,   codigo_deposito  ,  lote;

   -- Criando o loop de fardos
   cursor pcpf060 (p_deposito_lido number) is
      select fardo_nivel99 nivel,       fardo_grupo grupo,         fardo_subgrupo subgrupo,
             fardo_item    item,        deposito deposito,         lote_mat_prima lote,
             nvl(sum(decode(peso_real, 0, peso_medio, peso_real)), 0) estoque_fardos
      from pcpf_060
      where status_fardo   not in (2,5)
        and deposito      = p_deposito_lido
        and fardo_nivel99 is not null
      group by fardo_nivel99,     fardo_grupo,     fardo_subgrupo,
               fardo_item,        deposito,        lote_mat_prima;

   -- Criando loop do estoque - SALDOS
   cursor estq040 (p_deposito_lido number) is
      select e.cditem_nivel99 nivel, e.cditem_grupo grupo, e.cditem_subgrupo subgrupo,
             e.cditem_item item,     e.deposito deposito,  e.lote_acomp lote,
             sum(e.qtde_estoque_atu) qtde_estoque
      from estq_040 e
      where e.deposito = p_deposito_lido
        and e.cditem_nivel99 is not null
      group by e.cditem_nivel99, e.cditem_grupo, e.cditem_subgrupo, e.cditem_item, e.deposito, e.lote_acomp;

   v_diferenca     number;
   v_transacao     number;
   v_entrada_saida varchar2(1);
   v_qtde_movto    number;
   saldo_final     number;
   v_qtdevolumes   number;


begin
   -- le depositos escolhidos em paramentros
   for reg_basi205 in basi205
   loop

      if reg_basi205.tipo_volume = 2 or
         reg_basi205.tipo_volume = 4     --- Rolos
      then
         -- PCPT_020
         for reg_pcpt020 in pcpt020 (reg_basi205.codigo_deposito)
         loop

            select nvl(sum(qtde_estoque_atu),0) into saldo_final from estq_040 e
            where e.cditem_nivel99  = reg_pcpt020.nivel
            and   e.cditem_grupo    = reg_pcpt020.grupo
            and   e.cditem_subgrupo = reg_pcpt020.subgrupo
            and   e.cditem_item     = reg_pcpt020.item
            and   e.deposito        = reg_pcpt020.deposito
            and   e.lote_acomp      = reg_pcpt020.lote;

            if reg_pcpt020.estoque_rolos <> saldo_final
            then
               v_diferenca := reg_pcpt020.estoque_rolos - saldo_final;

               if v_diferenca > 0.000
               then
                  v_transacao     := reg_basi205.transacao_acerto_entrada;
                  v_entrada_saida := 'E';
                  v_qtde_movto    := v_diferenca;
               else
                  v_transacao     := reg_basi205.transacao_acerto_saida;
                  v_entrada_saida := 'S';
                  v_qtde_movto    := v_diferenca *-1;
               end if;

               insert into estq_300
                 (codigo_deposito,           nivel_estrutura,            grupo_estrutura,
                  subgrupo_estrutura,        item_estrutura,             data_movimento,
                  numero_lote,               numero_documento,           serie_documento,
                  cnpj_9,                    cnpj_4,                     cnpj_2,
                  sequencia_documento,
                  codigo_transacao,          entrada_saida,              centro_custo,
                  quantidade,                valor_movimento_unitario,   valor_contabil_unitario,
                  usuario_systextil,         tabela_origem,              processo_systextil)
               values
                 (reg_pcpt020.deposito,     reg_pcpt020.nivel,          reg_pcpt020.grupo,
                  reg_pcpt020.subgrupo,     reg_pcpt020.item,           trunc(sysdate),
                  reg_pcpt020.lote,         0,                     '',
                  0,                        0,                          0,
                  0,
                  v_transacao,              v_entrada_saida,            0,
                  v_qtde_movto,             1.00,                       1.00,
                 'INTERSYS',               'ESTQ_300',                 'acerto_estq_vol');

            end if;
         end loop; -- pcpt_020
      end if; -- tipo volume rolos

      if reg_basi205.tipo_volume = 7     --- Caixas
      then
         -- ESTQ_060
         for reg_estq060 in estq060 (reg_basi205.codigo_deposito)
         loop

            select sum(qtde_estoque_atu) into saldo_final from estq_040 e
            where e.cditem_nivel99  = reg_estq060.nivel
            and   e.cditem_grupo    = reg_estq060.grupo
            and   e.cditem_subgrupo = reg_estq060.subgrupo
            and   e.cditem_item     = reg_estq060.item
            and   e.deposito        = reg_estq060.deposito
            and   e.lote_acomp      = reg_estq060.lote;

            if reg_estq060.estoque_caixas <> saldo_final
            then

               v_diferenca := reg_estq060.estoque_caixas - saldo_final;

               if v_diferenca > 0.000
               then
                  v_transacao     := reg_basi205.transacao_acerto_entrada;
                  v_entrada_saida := 'E';
                  v_qtde_movto    := v_diferenca;

               else
                  v_transacao     := reg_basi205.transacao_acerto_saida;
                  v_entrada_saida := 'S';
                  v_qtde_movto    := v_diferenca *-1;

               end if;

               insert into estq_300
                 (codigo_deposito,           nivel_estrutura,            grupo_estrutura,
                  subgrupo_estrutura,        item_estrutura,             data_movimento,
                  numero_lote,               numero_documento,           serie_documento,
                  cnpj_9,                    cnpj_4,                     cnpj_2,
                  sequencia_documento,
                  codigo_transacao,          entrada_saida,              centro_custo,
                  quantidade,                valor_movimento_unitario,   valor_contabil_unitario,
                  usuario_systextil,         tabela_origem,              processo_systextil)
               values
                 (reg_estq060.deposito,     reg_estq060.nivel,          reg_estq060.grupo,
                  reg_estq060.subgrupo,     reg_estq060.item,           trunc(sysdate),
                  reg_estq060.lote,         0,                     '',
                  0,                        0,                          0,
                  0,
                  v_transacao,              v_entrada_saida,            0,
                  v_qtde_movto,             1.00,                       1.00,
                 'INTERSYS',               'ESTQ_300',                 'acerto_estq_vol');

            end if;
         end loop; -- estq_060
      end if; -- tipo volume caixas

      if reg_basi205.tipo_volume = 9     --- Fardos
      then
         -- PCPF_060
         for reg_pcpf060 in pcpf060  (reg_basi205.codigo_deposito)
         loop

            select sum(qtde_estoque_atu) into saldo_final from estq_040 e
            where e.cditem_nivel99  = reg_pcpf060.nivel
            and   e.cditem_grupo    = reg_pcpf060.grupo
            and   e.cditem_subgrupo = reg_pcpf060.subgrupo
            and   e.cditem_item     = reg_pcpf060.item
            and   e.deposito        = reg_pcpf060.deposito
            and   e.lote_acomp      = reg_pcpf060.lote;

            if reg_pcpf060.estoque_fardos <> saldo_final
            then
               v_diferenca := reg_pcpf060.estoque_fardos - saldo_final;

               if v_diferenca > 0.000
               then
                  v_transacao     := reg_basi205.transacao_acerto_entrada;
                  v_entrada_saida := 'E';
                  v_qtde_movto    := v_diferenca;
               else
                  v_transacao     := reg_basi205.transacao_acerto_saida;
                  v_entrada_saida := 'S';
                  v_qtde_movto    := v_diferenca *-1;
               end if;

               insert into estq_300
                 (codigo_deposito,           nivel_estrutura,            grupo_estrutura,
                  subgrupo_estrutura,        item_estrutura,             data_movimento,
                  numero_lote,               numero_documento,           serie_documento,
                  cnpj_9,                    cnpj_4,                     cnpj_2,
                  sequencia_documento,
                  codigo_transacao,          entrada_saida,              centro_custo,
                  quantidade,                valor_movimento_unitario,   valor_contabil_unitario,
                  usuario_systextil,         tabela_origem,              processo_systextil)
               values
                 (reg_pcpf060.deposito,     reg_pcpf060.nivel,          reg_pcpf060.grupo,
                  reg_pcpf060.subgrupo,     reg_pcpf060.item,           trunc(sysdate),
                  reg_pcpf060.lote,         0,                     '',
                  0,                        0,                          0,
                  0,
                  v_transacao,              v_entrada_saida,            0,
                  v_qtde_movto,             1.00,                       1.00,
                 'INTERSYS',               'ESTQ_300',                 'acerto_estq_vol');

           end if;
         end loop; -- pcpf_060
      end if; -- tipo volume fardos






      -- ACERTO DOS ITENS QUE POSSUEM SALDO POREM ESTAO COM QTDE ROLOS ZERADOS.
      for reg_estq040 in estq040 (reg_basi205.codigo_deposito)
      loop

         v_qtdevolumes := 0;

         if reg_basi205.tipo_volume = 2 or
            reg_basi205.tipo_volume = 4     --- Rolos
         then
            select nvl(sum(p.qtde_quilos_acab),0) into v_qtdevolumes from pcpt_020 p
            where p.codigo_deposito   = reg_estq040.deposito
            and   p.panoacab_nivel99  = reg_estq040.nivel
            and   p.panoacab_grupo    = reg_estq040.grupo
            and   p.panoacab_subgrupo = reg_estq040.subgrupo
            and   p.panoacab_item     = reg_estq040.item
            and   p.lote_acomp        = reg_estq040.lote
            and   p.rolo_estoque not in (0,2);
         end if;

         if reg_basi205.tipo_volume = 7     --- Caixas
         then
            select  nvl(sum(peso_liquido), 0)     into v_qtdevolumes from estq_060 e
            where e.codigo_deposito  = reg_estq040.deposito
              and e.prodcai_nivel99  = reg_estq040.nivel
              and e.prodcai_grupo    = reg_estq040.grupo
              and e.prodcai_subgrupo = reg_estq040.subgrupo
              and e.prodcai_item     = reg_estq040.item
              and e.lote             = reg_estq040.lote
              and e.status_caixa     not in (4,8,9);
         end if;

         if reg_basi205.tipo_volume = 9     --- Fardos
         then
            select nvl(sum(decode(peso_real, 0, peso_medio, peso_real)), 0) into v_qtdevolumes from pcpf_060 f
            where f.deposito        = reg_estq040.deposito
              and f.fardo_nivel99   = reg_estq040.nivel
              and f.fardo_grupo     = reg_estq040.grupo
              and f.fardo_subgrupo  = reg_estq040.subgrupo
              and f.fardo_item      = reg_estq040.item
              and f.lote_mat_prima  = reg_estq040.lote
              and f.status_fardo   not in (2,5);
         end if;

         if reg_estq040.qtde_estoque <> v_qtdevolumes
         then
            v_diferenca := v_qtdevolumes - reg_estq040.qtde_estoque;

            if v_diferenca > 0.000
            then
               v_transacao     := reg_basi205.transacao_acerto_entrada;
               v_entrada_saida := 'E';
               v_qtde_movto    := v_diferenca;
            else
               v_transacao     := reg_basi205.transacao_acerto_saida;
               v_entrada_saida := 'S';
               v_qtde_movto    := v_diferenca *-1;
            end if;

            insert into estq_300
              (codigo_deposito,           nivel_estrutura,            grupo_estrutura,
               subgrupo_estrutura,        item_estrutura,             data_movimento,
               numero_lote,               numero_documento,           serie_documento,
               cnpj_9,                    cnpj_4,                     cnpj_2,
               sequencia_documento,
               codigo_transacao,          entrada_saida,              centro_custo,
               quantidade,                valor_movimento_unitario,   valor_contabil_unitario,
               usuario_systextil,         tabela_origem,              processo_systextil)
            values
              (reg_estq040.deposito,     reg_estq040.nivel,          reg_estq040.grupo,
               reg_estq040.subgrupo,     reg_estq040.item,           trunc(sysdate),
               reg_estq040.lote,         0,                     '',
               0,                        0,                          0,
               0,
               v_transacao,              v_entrada_saida,            0,
               v_qtde_movto,             1.00,                       1.00,
              'INTERSYS',               'ESTQ_300',                 'acerto_estq_vol');

         end if;

      end loop; -- estq_040

   end loop; -- basi_205

end inter_pr_acerto_estq_vol;
 

/

exec inter_pr_recompile;

