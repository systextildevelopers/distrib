create or replace package ST_PCK_HISTORICO_CONTABIL as

    TYPE t_dados_cont_010 IS TABLE OF cont_010%rowtype;

    function busca_hist_contabil(p_codigo_historico NUMBER, p_msg_erro in out varchar2) return t_dados_cont_010;

    function exists_hist_contab(p_codigo_historico number) return boolean;

    function exists_by_sinal(p_codigo_historico number, p_sinal_titulo number) return boolean;

end ST_PCK_HISTORICO_CONTABIL;
