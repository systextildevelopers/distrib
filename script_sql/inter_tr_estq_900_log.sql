
  CREATE OR REPLACE TRIGGER "INTER_TR_ESTQ_900_LOG" 
  before insert or
         delete or
         update of carro, ordem_producao, estagio, endereco,
		operador, data_movto, flag_carro, executa_trigger,
		qtde_pecas
         on estq_900
  for each row

declare
  v_carro                estq_900.carro              %type;
  v_ordem_producao       estq_900.ordem_producao     %type;
  v_estagio              estq_900.estagio            %type;
  v_endereco             estq_900.endereco           %type;
  v_operador             estq_900.operador           %type;
  v_data_movto           estq_900.data_movto         %type;
  v_flag_carro           estq_900.flag_carro         %type;

  v_tipo_ocorr            varchar(1);
  v_data_ocorr            date;
  v_usuario_rede          varchar(20);
  v_maquina_rede          varchar(40);
  v_aplicacao             varchar(20);

  v_executa_trigger       number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger = 0
   then
      -- grava a data/hora da insercao do registro (log)
      v_data_ocorr := sysdate();

      -- encontra dados do usuario da rede, maquina e aplicativo que esta atualizando a ficha
      select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
      into v_usuario_rede, v_maquina_rede, v_aplicacao
      from sys.gv_$session
      where audsid  = userenv('SESSIONID')
        and inst_id = userenv('INSTANCE')
        and rownum < 2;

      --alimenta as variaveis new caso seja insert ou update
      if inserting or updating
      then
         if inserting
         then
            v_tipo_ocorr := 'I';
         else
            v_tipo_ocorr := 'A';
         end if;

         v_carro                := :new.carro;
         v_ordem_producao       := :new.ordem_producao;
         v_estagio              := :new.estagio;
         v_endereco             := :new.endereco;
         v_operador             := :new.operador;
         v_data_movto           := :new.data_movto;
         v_flag_carro           := :new.flag_carro;

      end if; --fim do if inserting or updating

      --alimenta as variaveis old caso seja delete ou update
      if deleting
      then
         v_tipo_ocorr := 'E';
         v_carro                := :old.carro;
         v_ordem_producao       := :old.ordem_producao;
         v_estagio              := :old.estagio;
         v_endereco             := :old.endereco;
         v_operador             := :old.operador;
         v_data_movto           := :old.data_movto;
         v_flag_carro           := :old.flag_carro;
      end if; --fim do if inserting or updating

      --insere na pcpc_330_log o registro.

      insert into estq_901
        (carro,               ordem_producao,
         estagio,             endereco,
         operador,            data_movto,
         flag_carro,          tipo_ocorr,
         data_ocorr,          usuario_rede,
         maquina_rede,        aplicacao)
      values
        (v_carro,             v_ordem_producao,
         v_estagio,           v_endereco,
         v_operador,          v_data_movto,
         v_flag_carro,        v_tipo_ocorr,
         v_data_ocorr,        v_usuario_rede,
         v_maquina_rede,      v_aplicacao);

    end if;
end inter_tr_estq_900_log;

-- ALTER TRIGGER "INTER_TR_ESTQ_900_LOG" ENABLE
 

/

exec inter_pr_recompile;

