
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_040_PROD" 
   before update of qtde_pecas_prod, qtde_pecas_2a, qtde_perdas
   on pcpc_040
   for each row
begin
   begin
      update tmrp_650
      set tmrp_650.quantidade_produzida = tmrp_650.quantidade_produzida + (:new.qtde_pecas_prod - :old.qtde_pecas_prod)
                                                                        + (:new.qtde_pecas_2a   - :old.qtde_pecas_2a)
                                                                        + (:new.qtde_perdas     - :old.qtde_perdas)
      where tmrp_650.tipo_alocacao  = 1
        and tmrp_650.tipo_recurso   = 5
        and tmrp_650.ordem_trabalho = :new.ordem_producao
        and tmrp_650.codigo_estagio = :new.codigo_estagio
        and tmrp_650.numero_id      = :new.numero_id_tmrp_650;
   exception when others then
      raise_application_error(-20000,'Nao atualizou TMRP_650. ' || sqlerrm);
   end;
end inter_tr_pcpc_040_prod;
-- ALTER TRIGGER "INTER_TR_PCPC_040_PROD" ENABLE
 

/

exec inter_pr_recompile;

