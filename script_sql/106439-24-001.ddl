alter table pcpc_020
add (ORDEM_AGRUP_PROD  NUMBER(9) default 0);       
comment on column pcpc_020.ordem_agrup_prod is 'Ordem de agrupamento de OP para produ��o independente do planejamento do corte (pcpc_f055), gravado no agrupamento de ordens pela tela Agrupamento de Ordens de Produ��o.';     

/
exec inter_pr_recompile;
