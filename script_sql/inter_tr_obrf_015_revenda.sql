create or replace TRIGGER inter_tr_obrf_015_revenda
BEFORE INSERT OR DELETE  OR UPDATE of transacao_canc_nfisc ON obrf_015
FOR EACH ROW
DECLARE
    v_NIVEL_ESTRUTURA ESTQ_307.NIVEL_ESTRUTURA%TYPE;
    v_GRUPO_ESTRUTURA ESTQ_307.GRUPO_ESTRUTURA%TYPE;
    v_SUBGRU_ESTRUTURA ESTQ_307.SUBGRU_ESTRUTURA%TYPE;
    v_ITEM_ESTRUTURA ESTQ_307.ITEM_ESTRUTURA%TYPE;
    v_TIPO_MOVIMENTO ESTQ_307.TIPO_MOVIMENTO%TYPE;
    v_DOCUMENTO ESTQ_307.DOCUMENTO%TYPE;
    v_SERIE ESTQ_307.SERIE%TYPE;
    
    v_QUANTIDADE ESTQ_307.QUANTIDADE%TYPE;
    v_USUARIO_SYSTEXTIL ESTQ_307.USUARIO_SYSTEXTIL%TYPE;
    v_DATA_HORA_MOV ESTQ_307.DATA_HORA_MOV%TYPE;
    v_PROG_GEROU_MOV ESTQ_307.PROG_GEROU_MOV%TYPE;
    v_SEQ_INSERCAO ESTQ_307.SEQ_INSERCAO%TYPE;
    v_QTDE_SALDO ESTQ_307.QTDE_SALDO%TYPE;

    v_NATITEM_NAT_OPER OBRF_015.NATITEM_NAT_OPER%TYPE;
    V_NATITEM_EST_OPER OBRF_015.NATITEM_EST_OPER%type;

    v_TRANSACAO obrf_015.CODIGO_TRANSACAO%TYPE;
         
    v_cod_estagio_agrupador_insu obrf_015.cod_estagio_agrupador_insu%TYPE;
    v_cod_estagio_simultaneo_insu obrf_015.cod_estagio_simultaneo_insu%TYPE;

    v_origem OBRF_010.ORIGEM%TYPE;
    v_cfop varchar2(20);
    v_cod_empresa OBRF_010.LOCAL_ENTREGA%TYPE;

    v_CPNJ9 number(9);
    v_CPNJ4 number(4);
    v_CPNJ2 number(2);
    
BEGIN
     
     IF INSERTING or UPDATING THEN
         v_NIVEL_ESTRUTURA := :NEW.CODITEM_NIVEL99;
         v_GRUPO_ESTRUTURA := :NEW.CODITEM_GRUPO;
         v_SUBGRU_ESTRUTURA := :NEW.CODITEM_SUBGRUPO;
         v_ITEM_ESTRUTURA := :NEW.CODITEM_ITEM;
         v_TIPO_MOVIMENTO := 'E';
         v_DOCUMENTO := :NEW.CAPA_ENT_NRDOC;
         v_SERIE := :NEW.CAPA_ENT_SERIE;
         v_CPNJ9 := :NEW.CAPA_ENT_FORCLI9;
         v_CPNJ4 := :NEW.CAPA_ENT_FORCLI4;
         v_CPNJ2 := :NEW.CAPA_ENT_FORCLI2;

         v_QUANTIDADE := :NEW.QUANTIDADE;
         v_USUARIO_SYSTEXTIL := :NEW.USUARIO_CARDEX;
         v_DATA_HORA_MOV := SYSDATE;
         v_PROG_GEROU_MOV := :NEW.NOME_PROGRAMA;
         v_SEQ_INSERCAO := ESTQ_307_SEQ_INSERCAO_SEQ.nextval;
         v_QTDE_SALDO := 0;
         
         v_TRANSACAO := :NEW.CODIGO_TRANSACAO;
         
         v_cod_estagio_agrupador_insu := :NEW.cod_estagio_agrupador_insu;
         v_cod_estagio_simultaneo_insu := :NEW.cod_estagio_simultaneo_insu;
         
         v_NATITEM_NAT_OPER := :NEW.NATITEM_NAT_OPER;
         V_NATITEM_EST_OPER := :NEW.NATITEM_EST_OPER;
        
    ELSIF DELETING THEN
        v_NIVEL_ESTRUTURA := :OLD.CODITEM_NIVEL99;
         v_GRUPO_ESTRUTURA := :OLD.CODITEM_GRUPO;
         v_SUBGRU_ESTRUTURA := :OLD.CODITEM_SUBGRUPO;
         v_ITEM_ESTRUTURA := :OLD.CODITEM_ITEM;
         v_TIPO_MOVIMENTO := 'S'; 
         v_DOCUMENTO := :OLD.CAPA_ENT_NRDOC;
         v_SERIE := :OLD.CAPA_ENT_SERIE;
         v_CPNJ9 := :OLD.CAPA_ENT_FORCLI9;
         v_CPNJ4 := :OLD.CAPA_ENT_FORCLI4;
         v_CPNJ2 := :OLD.CAPA_ENT_FORCLI2;

         v_QUANTIDADE := :OLD.QUANTIDADE;
         v_USUARIO_SYSTEXTIL := :OLD.USUARIO_CARDEX;
         v_DATA_HORA_MOV := SYSDATE;
         v_PROG_GEROU_MOV := :OLD.NOME_PROGRAMA;
         v_SEQ_INSERCAO := ESTQ_307_SEQ_INSERCAO_SEQ.nextval;
         v_QTDE_SALDO := 0;
         
         v_TRANSACAO := :OLD.CODIGO_TRANSACAO;
         
         v_cod_estagio_agrupador_insu := :OLD.cod_estagio_agrupador_insu;
         v_cod_estagio_simultaneo_insu := :OLD.cod_estagio_simultaneo_insu;
         
         v_NATITEM_NAT_OPER := :OLD.NATITEM_NAT_OPER;
         V_NATITEM_EST_OPER := :OLD.NATITEM_EST_OPER;
            
    END IF;
    
    IF UPDATING and :OLD.transacao_canc_nfisc = 0 and :NEW.transacao_canc_nfisc > 0 THEN
       v_TIPO_MOVIMENTO := 'S'; 
    END IF; 
     
    v_cfop := inter_fn_busca_cfop(v_NATITEM_NAT_OPER, V_NATITEM_EST_OPER);
    
    begin
          SELECT LOCAL_ENTREGA
          INTO  v_cod_empresa
          FROM OBRF_010
          WHERE DOCUMENTO = v_DOCUMENTO
          AND SERIE = v_SERIE
          and cgc_cli_for_9 = v_CPNJ9
          and cgc_cli_for_4 = v_CPNJ4
          and cgc_cli_for_2 = v_CPNJ2;
      EXCEPTION WHEN NO_DATA_FOUND THEN
                v_cod_empresa := 0; 
      END;    
      
    IF  inter_fn_cc_revenda(v_cod_empresa)
        AND inter_fn_valida_tipo_transacao(v_TRANSACAO)
        AND inter_fn_valida_cfop(v_cfop)
        AND inter_fn_valida_prod_virtual(v_cod_estagio_agrupador_insu, v_cod_estagio_simultaneo_insu)
    THEN
        IF INSERTING OR DELETING OR (UPDATING and :OLD.transacao_canc_nfisc = 0 and :NEW.transacao_canc_nfisc > 0) THEN
             INSERT INTO ESTQ_307 (
                 NIVEL_ESTRUTURA,
                 GRUPO_ESTRUTURA,
                 SUBGRU_ESTRUTURA,
                 ITEM_ESTRUTURA,
                 TIPO_MOVIMENTO,
                 DOCUMENTO,
                 SERIE,
                 ORIGEM_MOVIMENTO,
                 QUANTIDADE,
                 USUARIO_SYSTEXTIL,
                 DATA_HORA_MOV,
                 PROG_GEROU_MOV,
                 SEQ_INSERCAO,
                 QTDE_SALDO,
                 COD_EMPRESA,
                 DATA_INSERCAO
             ) VALUES (
                 v_NIVEL_ESTRUTURA,
                 v_GRUPO_ESTRUTURA,
                 v_SUBGRU_ESTRUTURA,
                 v_ITEM_ESTRUTURA,
                 v_TIPO_MOVIMENTO,
                 v_DOCUMENTO,
                 v_SERIE,
                 v_cfop,
                 v_QUANTIDADE,
                 v_USUARIO_SYSTEXTIL,
                 v_DATA_HORA_MOV,
                 v_PROG_GEROU_MOV,
                 v_SEQ_INSERCAO,
                 v_QTDE_SALDO,
                 v_cod_empresa,
                 sysdate
             );
        
        END IF;
    END IF;
END;

/
