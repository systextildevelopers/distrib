create or replace PACKAGE BODY ST_PCK_SUPR AS

    function exists_fornecedor(p_fornecedor9 number, p_fornecedor4 number, p_fornecedor2 number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM supr_010 
        WHERE fornecedor9 = p_fornecedor9
          AND fornecedor4 = p_fornecedor4
          AND fornecedor2 = p_fornecedor2;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_fornecedor;

    function cria_fornecedor(p_cnpj_for9 	    number, 	p_cnpj_for4 		number, 	p_cnpj_for2 	number, 	p_nome 				varchar2, 
							p_telefone  		varchar2, 	p_celular_forne 	varchar2, 	p_email 		varchar2, 	p_nfe_email 		varchar2, 
							p_cep 				varchar2, 	p_endereco 			varchar2, 	p_numero_imovel varchar2, 	p_complemento 		varchar2, 
							p_bairro 			varchar2, 	p_cidade 			varchar2, 	p_empresa 		varchar2, 	p_cod_fornecedor 	varchar2, 
							p_tipo_fornecedor 	number, 	p_codigo_contabil 	number) return varchar2
    AS
        p_des_erro varchar2(1000);
    BEGIN

        BEGIN
            INSERT INTO SUPR_010 (FORNECEDOR9, FORNECEDOR4, FORNECEDOR2,
                                NOME_FORNECEDOR, NOME_FANTASIA, SIT_FORNECEDOR,
                                TELEFONE_FORNE, CEP_FORNECEDOR, ENDERECO_FORNE,
                                INSCR_EST_FORNE,  BAIRRO, COD_CIDADE,
                                DATA_CADASTRO, COD_EMPRESA, TIPO_FORNECEDOR,
                                E_MAIL, CODIGO_CONTABIL, CODIGO_FORNECEDOR,
                                DATA_ATUALIZACAO, CELULAR_FORNECEDOR, NUMERO_IMOVEL,
                                COMPLEMENTO, NFE_E_MAIL)

            VALUES (
                p_cnpj_for9, p_cnpj_for4, p_cnpj_for2,
                p_nome, p_nome, 1,
                p_telefone, p_cep, p_endereco,
                'ISENTO', p_bairro, p_cidade,
                sysdate, to_number(p_empresa), p_tipo_fornecedor,
                p_email, p_codigo_contabil, p_cod_fornecedor,
                sysdate, p_celular_forne, p_numero_imovel,
                p_complemento, p_email);
        EXCEPTION
        WHEN OTHERS
            THEN
                p_des_erro := 'Erro ao inserir fornecedor. CNPJ: ' || p_cnpj_for9 || '/' || p_cnpj_for4 || '-' || p_cnpj_for2 || ' ERRO: ' || SQLERRM;
        END;

        return p_des_erro;
    END cria_fornecedor;
    
END ST_PCK_SUPR;
