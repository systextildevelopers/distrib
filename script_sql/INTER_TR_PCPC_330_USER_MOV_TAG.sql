create or replace trigger INTER_TR_PCPC_330_USER_MOV_TAG
before insert or update of
estoque_tag 
on pcpc_330
for each row

declare 

  v_usuario_rede              varchar(20);
  v_maquina_rede              varchar(40);
  v_aplicativo                varchar(20);

  v_sid                 number;
  v_usuario_systextil   varchar(250);
  v_empresa             number;
  v_locale_usuario      varchar(20);
  v_ult_usuario         varchar(250);
  
begin
  inter_pr_dados_usuario(v_usuario_rede, v_maquina_rede, v_aplicativo, v_sid,
                         v_usuario_systextil, v_empresa, v_locale_usuario);

  if v_usuario_systextil is not null then
    v_ult_usuario := v_usuario_systextil;
  else
    v_ult_usuario := v_usuario_rede; 
  end if;  

  :new.ultimo_usuario_log := v_ult_usuario;
  
end;
