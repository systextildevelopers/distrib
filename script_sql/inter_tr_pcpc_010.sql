create or replace trigger inter_tr_pcpc_010
before update
  of  SITUACAO_PERIODO
   on pcpc_010
   for each row
 
declare
 
 
begin
   if UPDATING
   then
      if :new.situacao_periodo = 3
      then
         :new.data_fechamento_periodo := trunc(sysdate, 'DD');
      else
         :new.data_fechamento_periodo := null;
      end if;
   end if;
end;

/
