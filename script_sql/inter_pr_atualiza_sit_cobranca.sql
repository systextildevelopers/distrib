
  CREATE OR REPLACE PROCEDURE "INTER_PR_ATUALIZA_SIT_COBRANCA" (p_empresa  in number, p_nova_sit_cobranca in number)
is
   cursor fatu070 is
      select fatu_070.data_prorrogacao,  fatu_070.codigo_empresa,
             fatu_070.num_duplicata,     fatu_070.seq_duplicatas,
             fatu_070.tipo_titulo,       fatu_070.valor_duplicata,
             fatu_070.situacao_duplic,   fatu_070.cli_dup_cgc_cli9,
             fatu_070.cli_dup_cgc_cli4,  fatu_070.cli_dup_cgc_cli2
      from pedi_010, fatu_070
      where fatu_070.cli_dup_cgc_cli9  = pedi_010.cgc_9
        and fatu_070.cli_dup_cgc_cli4  = pedi_010.cgc_4
        and fatu_070.cli_dup_cgc_cli2  = pedi_010.cgc_2
        and fatu_070.data_prorrogacao  < trunc(sysdate,'DD')
        and fatu_070.situacao_duplic  <> 2
        and fatu_070.saldo_duplicata <> 0.00
        and exists (select hdoc_001.classificacao1 from hdoc_001
                    where hdoc_001.tipo           = 6
                      and hdoc_001.codigo         = pedi_010.cod_sit_cobranca
                      and hdoc_001.classificacao1 = 1)
      order by fatu_070.cli_dup_cgc_cli9,
               fatu_070.cli_dup_cgc_cli4,  fatu_070.cli_dup_cgc_cli2;

   cursor fatu070_2 is
      select fatu_070.data_prorrogacao,  fatu_070.codigo_empresa,
             fatu_070.num_duplicata,     fatu_070.seq_duplicatas,
             fatu_070.tipo_titulo,       fatu_070.valor_duplicata,
             fatu_070.situacao_duplic,   fatu_070.cli_dup_cgc_cli9,
             fatu_070.cli_dup_cgc_cli4,  fatu_070.cli_dup_cgc_cli2,
             pedi_010.cod_sit_cobranca
      from pedi_010, fatu_070
      where fatu_070.cli_dup_cgc_cli9  = pedi_010.cgc_9
        and fatu_070.cli_dup_cgc_cli4  = pedi_010.cgc_4
        and fatu_070.cli_dup_cgc_cli2  = pedi_010.cgc_2
        and fatu_070.situacao_duplic  <> 2
        and pedi_010.cod_sit_cobranca > 0
       order by fatu_070.cli_dup_cgc_cli9,
       fatu_070.cli_dup_cgc_cli4,  fatu_070.cli_dup_cgc_cli2;

   v_data_hoje        date := trunc(sysdate,'DD');
   v_data_prorrogacao date;
   v_data_util        date;

   v_trocar_situacao  boolean := false;
   v_tem_atraso       boolean;

   v_cgc9_old         number := 0;
   v_cgc4_old         number := 0;
   v_cgc2_old         number := 0;
   v_max_dias_atr_tit number;
   v_valor_pago       number;

begin

   begin
      select max_dias_atraso into v_max_dias_atr_tit from fatu_500
      where codigo_empresa = p_empresa;

      exception
         when others then
            v_max_dias_atr_tit := 0;
   end;

   update pedi_010
   set pedi_010.cod_sit_cobranca   = 0,
       pedi_010.data_sit_cobranca  = null,
       pedi_010.data_prox_cobranca = null
   where exists (select hdoc_001.classificacao1 from hdoc_001
                 where hdoc_001.tipo           = 6
                   and hdoc_001.codigo         = pedi_010.cod_sit_cobranca
                   and hdoc_001.classificacao1 = 1);

   commit;

   for reg_fatu070 in fatu070
   loop

      if (v_cgc9_old <> reg_fatu070.cli_dup_cgc_cli9
      or  v_cgc4_old <> reg_fatu070.cli_dup_cgc_cli4
      or  v_cgc2_old <> reg_fatu070.cli_dup_cgc_cli2)
      then

         if v_trocar_situacao = true
         then
            update pedi_010
            set cod_sit_cobranca  = p_nova_sit_cobranca,
                data_sit_cobranca = v_data_hoje,
                dig_cliente = 9
            where pedi_010.cgc_9 = v_cgc9_old
              and pedi_010.cgc_4 = v_cgc4_old
              and pedi_010.cgc_2 = v_cgc2_old;

            commit;
         end if;

         if v_trocar_situacao = false
         then
            update pedi_010
            set dig_cliente = 8
            where pedi_010.cgc_9 = v_cgc9_old
              and pedi_010.cgc_4 = v_cgc4_old
              and pedi_010.cgc_2 = v_cgc2_old;

            commit;
         end if;


         v_cgc9_old := reg_fatu070.cli_dup_cgc_cli9;
         v_cgc4_old := reg_fatu070.cli_dup_cgc_cli4;
         v_cgc2_old := reg_fatu070.cli_dup_cgc_cli2;

         v_trocar_situacao := false;
      end if;

      v_data_prorrogacao := reg_fatu070.data_prorrogacao;
      v_data_util        := reg_fatu070.data_prorrogacao;

      select min(data_calendario) into v_data_util from basi_260
      where data_calendario >= v_data_prorrogacao
        and dia_util_finan   = 0;

      if v_data_util is null
      then
         v_data_util := v_data_prorrogacao;
      end if;

      if v_data_hoje <= v_data_util
      then
         v_data_prorrogacao := v_data_util;
      end if;

      if (v_data_hoje - v_data_prorrogacao) > v_max_dias_atr_tit
      then
         v_tem_atraso := true;
      else
         v_tem_atraso := false;
      end if;

      if v_tem_atraso = true
      then

         select nvl(sum(decode(cont_010.sinal_titulo,1, (fatu_075.valor_pago - fatu_075.valor_juros + fatu_075.valor_descontos - fatu_075.vlr_var_cambial),
                                                     2, (fatu_075.valor_pago - fatu_075.valor_juros + fatu_075.valor_descontos - fatu_075.vlr_var_cambial) * (-1.0),
                                                     0.00)),0)
         into v_valor_pago
         from fatu_075, cont_010
         where fatu_075.nr_titul_cli_dup_cgc_cli9 = reg_fatu070.cli_dup_cgc_cli9
           and fatu_075.nr_titul_cli_dup_cgc_cli4 = reg_fatu070.cli_dup_cgc_cli4
           and fatu_075.nr_titul_cli_dup_cgc_cli2 = reg_fatu070.cli_dup_cgc_cli2
           and fatu_075.nr_titul_cod_tit          = reg_fatu070.tipo_titulo
           and fatu_075.nr_titul_num_dup          = reg_fatu070.num_duplicata
           and fatu_075.nr_titul_seq_dup          = reg_fatu070.seq_duplicatas
           and fatu_075.nr_titul_codempr          = reg_fatu070.codigo_empresa
           and fatu_075.historico_pgto            = cont_010.codigo_historico;


         if (reg_fatu070.valor_duplicata - v_valor_pago) > 0.00
         then
            v_trocar_situacao := true;
         end if;
      end if;
   end loop;

   if v_trocar_situacao = true
   then
      update pedi_010
      set cod_sit_cobranca  = p_nova_sit_cobranca,
          data_sit_cobranca = v_data_hoje,
          dig_cliente = 9
      where pedi_010.cgc_9 = v_cgc9_old
        and pedi_010.cgc_4 = v_cgc4_old
        and pedi_010.cgc_2 = v_cgc2_old;

      commit;
   end if;


-- AVALIA OS QUE ESTAO MARCADOS E QUE NAO POSSUEM MAIS ATRASOS PARA LIBERA-LOS

   v_cgc9_old         := 0;
   v_cgc4_old         := 0;
   v_cgc2_old         := 0;
   v_trocar_situacao  := false;

   for reg_fatu070_2 in fatu070_2
   loop

      if (v_cgc9_old <> reg_fatu070_2.cli_dup_cgc_cli9
      or  v_cgc4_old <> reg_fatu070_2.cli_dup_cgc_cli4
      or  v_cgc2_old <> reg_fatu070_2.cli_dup_cgc_cli2)
      then

         if v_trocar_situacao = false
         then
            update pedi_010
            set pedi_010.cod_sit_cobranca   = 0,
                pedi_010.data_sit_cobranca  = null,
                pedi_010.data_prox_cobranca = null,
                dig_cliente                 = 8
            where pedi_010.cgc_9 = v_cgc9_old
              and pedi_010.cgc_4 = v_cgc4_old
              and pedi_010.cgc_2 = v_cgc2_old;

            commit;
         end if;


         v_cgc9_old := reg_fatu070_2.cli_dup_cgc_cli9;
         v_cgc4_old := reg_fatu070_2.cli_dup_cgc_cli4;
         v_cgc2_old := reg_fatu070_2.cli_dup_cgc_cli2;

         v_trocar_situacao := false;
      end if;

      v_data_prorrogacao := reg_fatu070_2.data_prorrogacao;
      v_data_util        := reg_fatu070_2.data_prorrogacao;

      select min(data_calendario) into v_data_util from basi_260
      where data_calendario >= v_data_prorrogacao
        and dia_util_finan   = 0;

      if v_data_util is null
      then
         v_data_util := v_data_prorrogacao;
      end if;

      if v_data_hoje <= v_data_util
      then
         v_data_prorrogacao := v_data_util;
      end if;

      if (v_data_hoje - v_data_prorrogacao) > v_max_dias_atr_tit
      then
         v_tem_atraso := true;
      else
         v_tem_atraso := false;
      end if;

      if v_tem_atraso = true
      then

         select nvl(sum(decode(cont_010.sinal_titulo,1, (fatu_075.valor_pago - fatu_075.valor_juros + fatu_075.valor_descontos - fatu_075.vlr_var_cambial),
                                                     2, (fatu_075.valor_pago - fatu_075.valor_juros + fatu_075.valor_descontos - fatu_075.vlr_var_cambial) * (-1.0),
                                                     0.00)),0)
         into v_valor_pago
         from fatu_075, cont_010
         where fatu_075.nr_titul_cli_dup_cgc_cli9 = reg_fatu070_2.cli_dup_cgc_cli9
           and fatu_075.nr_titul_cli_dup_cgc_cli4 = reg_fatu070_2.cli_dup_cgc_cli4
           and fatu_075.nr_titul_cli_dup_cgc_cli2 = reg_fatu070_2.cli_dup_cgc_cli2
           and fatu_075.nr_titul_cod_tit          = reg_fatu070_2.tipo_titulo
           and fatu_075.nr_titul_num_dup          = reg_fatu070_2.num_duplicata
           and fatu_075.nr_titul_seq_dup          = reg_fatu070_2.seq_duplicatas
           and fatu_075.nr_titul_codempr          = reg_fatu070_2.codigo_empresa
           and fatu_075.historico_pgto            = cont_010.codigo_historico;


         if (reg_fatu070_2.valor_duplicata - v_valor_pago) > 0.00
         then
            v_trocar_situacao := true;
         end if;
      end if;
   end loop;

   if v_trocar_situacao = false
   then
      update pedi_010
      set pedi_010.cod_sit_cobranca   = 0,
          pedi_010.data_sit_cobranca  = null,
          pedi_010.data_prox_cobranca = null,
          dig_cliente                 = 8
      where pedi_010.cgc_9 = v_cgc9_old
        and pedi_010.cgc_4 = v_cgc4_old
        and pedi_010.cgc_2 = v_cgc2_old;

      commit;
   end if;
end inter_pr_atualiza_sit_cobranca;

 

/

exec inter_pr_recompile;

