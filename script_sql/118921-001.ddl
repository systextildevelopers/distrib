alter table MQOP_050 add NR_AGULHAS NUMBER(3);
alter table MQOP_050 modify NR_AGULHAS default 0; 
comment on column MQOP_050.NR_AGULHAS is 'Número de Agulhas, uma variante do aparelho.';

exec inter_pr_recompile;

/
