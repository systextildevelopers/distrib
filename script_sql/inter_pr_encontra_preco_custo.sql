
  CREATE OR REPLACE PROCEDURE "INTER_PR_ENCONTRA_PRECO_CUSTO" (p_origem_preco_custo in varchar2,
                                                          p_empresa            in number,
                                                          p_nivel              in varchar2,
                                                          p_grupo              in varchar2,
                                                          p_subgrupo           in varchar2,
                                                          p_item               in varchar2,
                                                          p_deposito           in number,
                                                          p_data_movimento     in date,
                                                          r_valor_custo        out number,
                                                          r_valor_custo_proj   out number,
                                                          r_valor_custo_est    out number) is

   v_local_deposito    basi_205.local_deposito%type;
   v_deposito_terceiro basi_205.deposito_terceiro%type;
   v_ref_original      basi_030.ref_original%type;

   v_novo_valor_custo_unit      estq_300.preco_medio_unitario%type;
   v_novo_valor_custo_unit_proj estq_300.preco_medio_unitario%type;
   v_novo_valor_custo_unit_est  estq_300.preco_medio_unitario%type;
   v_custo_fab_prev             estq_300.preco_medio_unitario%type;
   v_custo_fab_proj             estq_300.preco_medio_unitario%type;
   v_custo_fab_est              estq_300.preco_medio_unitario%type;

   v_mes_movimento  number;
   v_ano_movimento  number;
   v_mes_movimento2 number;
   v_ano_movimento2 number;

   v_ultimo_calculo date;

begin

   -- busca dados do deposito do movimento
   if p_deposito = 0
   then
      v_local_deposito    := p_empresa;
      v_deposito_terceiro := 0;
   else

      begin
         select local_deposito, deposito_terceiro
         into   v_local_deposito, v_deposito_terceiro
         from   basi_205
         where  codigo_deposito = p_deposito;

      exception
         when others then
            v_local_deposito    := p_empresa;
            v_deposito_terceiro := 0;
      end;
   end if;

   -- busca a referencia original (se houver)
   begin
      select ref_original
      into   v_ref_original
      from   basi_030
      where  nivel_estrutura = p_nivel
      and    referencia = p_grupo;

   exception
      when others then
         v_ref_original := 0;
   end;

   -- separa os o mes e o ano da data do movimento
   v_mes_movimento := to_number(to_char(p_data_movimento, 'MM'));
   v_ano_movimento := to_number(to_char(p_data_movimento, 'YYYY'));

   -- encontra os valores do preco de custo, conforme custo calculado
   if p_origem_preco_custo = '1' -- custo PADRAO (gerado pela FICHA DE CUSTOS - BASI_350)
   then

      if v_deposito_terceiro = 1 -- e deposito de terceiro
      then

         -- le.custo.de.fabricacao.nao.incluindo.o.valor.da.materia.prima.de.terceiro
         select round(nvl(sum(valor_mo + valor_cp + valor_cd), 0.000), 5),
                round(nvl(sum(valor_mo + valor_cp + valor_cd), 0.000), 5)
         into   v_novo_valor_custo_unit, v_novo_valor_custo_unit_proj
         from   basi_350
         where  codigo_empresa = v_local_deposito
         and    mes = v_mes_movimento
         and    ano = v_ano_movimento
         and    nivel_estrutura = p_nivel
         and    grupo_estrutura = v_ref_original
         and    subgru_estrutura = p_subgrupo
         and    item_estrutura = p_item;

         select round(nvl(sum(valor_mo + valor_cp + valor_cd), 0.000), 5)
         into   v_novo_valor_custo_unit_est
         from   basi_350
         where  codigo_empresa = 999
         and    mes = v_mes_movimento
         and    ano = v_ano_movimento
         and    nivel_estrutura = p_nivel
         and    grupo_estrutura = v_ref_original
         and    subgru_estrutura = p_subgrupo
         and    item_estrutura = p_item;

         -- se nao encontrar registros nas tabelas de custo, ou se o valor encontrado
         -- for zero, procura o preco de custo do ultimo periodo calculado na ficha de custo
         if v_novo_valor_custo_unit = 0.000
         then

            -- encontra a ultima ficha de custo calculada
            select max(to_date('01/' || mes || '/' || ano, 'dd/mm/yyyy'))
            into   v_ultimo_calculo
            from   basi_350
            where  codigo_empresa = v_local_deposito
            and    to_date('01/' || mes || '/' || ano, 'dd/mm/yyyy') <
                   to_date('01/' || v_mes_movimento || '/' || v_ano_movimento, 'dd/mm/yyyy')
            and    nivel_estrutura = p_nivel
            and    grupo_estrutura = v_ref_original
            and    subgru_estrutura = p_subgrupo
            and    item_estrutura = p_item;

            -- se encontrar, pega o preco de custo do periodo encontrado
            if v_ultimo_calculo is not null
            then
               v_mes_movimento2 := to_number(to_char(v_ultimo_calculo, 'MM'));
               v_ano_movimento2 := to_number(to_char(v_ultimo_calculo, 'YYYY'));

               select round(nvl(sum(valor_mo + valor_cp + valor_cd), 0.000), 5)
               into   v_novo_valor_custo_unit
               from   basi_350
               where  codigo_empresa = v_local_deposito
               and    mes = v_mes_movimento2
               and    ano = v_ano_movimento2
               and    nivel_estrutura = p_nivel
               and    grupo_estrutura = v_ref_original
               and    subgru_estrutura = p_subgrupo
               and    item_estrutura = p_item;

               select round(nvl(sum(valor_mo + valor_cp + valor_cd), 0.000), 5)
               into   v_novo_valor_custo_unit_est
               from   basi_350
               where  codigo_empresa = 999
               and    mes = v_mes_movimento2
               and    ano = v_ano_movimento2
               and    nivel_estrutura = p_nivel
               and    grupo_estrutura = v_ref_original
               and    subgru_estrutura = p_subgrupo
               and    item_estrutura = p_item;
            end if;
         end if;

      else
         -- Deposito nao e de terceiro

         -- le.custo.de.fabricacao.incluindo.o.valor.da.materia.prima
         select nvl(sum(basi_350.custo_fabricacao), 0.000),
                nvl(sum(basi_350.custo_fabricacao_proj), 0.000)
         into   v_custo_fab_prev, v_custo_fab_proj
         from   basi_350
         where  codigo_empresa = v_local_deposito
         and    mes = v_mes_movimento
         and    ano = v_ano_movimento
         and    nivel_estrutura = p_nivel
         and    grupo_estrutura = v_ref_original
         and    subgru_estrutura = p_subgrupo
         and    item_estrutura = p_item;

         v_novo_valor_custo_unit      := round(v_custo_fab_prev, 5);
         v_novo_valor_custo_unit_proj := round(v_custo_fab_proj, 5);

         -- le.custo.de.fabricacao.incluindo.o.valor.da.materia.prima para EMPRESA CONSOLIDACAO (gerencial)
         select nvl(sum(basi_350.custo_fabricacao), 0.000)
         into   v_custo_fab_est
         from   basi_350
         where  codigo_empresa = 999
         and    mes = v_mes_movimento
         and    ano = v_ano_movimento
         and    nivel_estrutura = p_nivel
         and    grupo_estrutura = v_ref_original
         and    subgru_estrutura = p_subgrupo
         and    item_estrutura = p_item;

         v_novo_valor_custo_unit_est := round(v_custo_fab_est, 5);

         -- se nao encontrar registros nas tabelas de custo, ou se o valor encontrado
         -- for zero, procura o preco de custo do ultimo periodo calculado na ficha de custo
         if v_novo_valor_custo_unit = 0.000
         then

            -- encontra a ultima ficha de custo calculada
            select max(to_date('01/' || mes || '/' || ano, 'dd/mm/yyyy'))
            into   v_ultimo_calculo
            from   basi_350
            where  codigo_empresa = v_local_deposito
            and    to_date('01/' || mes || '/' || ano, 'dd/mm/yyyy') <
                   to_date('01/' || v_mes_movimento || '/' || v_ano_movimento, 'dd/mm/yyyy')
            and    nivel_estrutura = p_nivel
            and    grupo_estrutura = v_ref_original
            and    subgru_estrutura = p_subgrupo
            and    item_estrutura = p_item;

            -- se encontrar, pega o preco de custo do periodo encontrado
            if v_ultimo_calculo is not null
            then

               v_mes_movimento2 := to_number(to_char(v_ultimo_calculo, 'MM'));
               v_ano_movimento2 := to_number(to_char(v_ultimo_calculo, 'YYYY'));

               select nvl(sum(basi_350.custo_fabricacao), 0.000),
                      nvl(sum(basi_350.custo_fabricacao_proj), 0.000)
               into   v_custo_fab_prev, v_custo_fab_proj
               from   basi_350
               where  codigo_empresa = v_local_deposito
               and    mes = v_mes_movimento2
               and    ano = v_ano_movimento2
               and    nivel_estrutura = p_nivel
               and    grupo_estrutura = v_ref_original
               and    subgru_estrutura = p_subgrupo
               and    item_estrutura = p_item;

               v_novo_valor_custo_unit      := round(v_custo_fab_prev, 5);
               v_novo_valor_custo_unit_proj := round(v_custo_fab_proj, 5);

               select nvl(sum(basi_350.custo_fabricacao), 0.000)
               into   v_custo_fab_est
               from   basi_350
               where  codigo_empresa = 999
               and    mes = v_mes_movimento2
               and    ano = v_ano_movimento2
               and    nivel_estrutura = p_nivel
               and    grupo_estrutura = v_ref_original
               and    subgru_estrutura = p_subgrupo
               and    item_estrutura = p_item;

               v_novo_valor_custo_unit_est := round(v_custo_fab_est, 5);
            end if;
         end if;
      end if;

   else
      -- custo REAL (gerado pela APURACAO DO CUSTO REAL - RCNB_350)

      -- le.custo.de.fabricacao.incluindo.o.valor.da.materia.prima
      select nvl(sum(rcnb_350.custo_real_unit_fabric), 0.000),
             nvl(sum(rcnb_350.custo_real_unit_fabric), 0.000)
      into   v_custo_fab_prev, v_custo_fab_proj
      from   rcnb_350
      where  codigo_empresa = v_local_deposito
      and    mes = v_mes_movimento
      and    ano = v_ano_movimento
      and    nivel_produto = p_nivel
      and    grupo_produto = v_ref_original
      and    sub_produto = p_subgrupo
      and    item_produto = p_item;

      v_novo_valor_custo_unit      := round(v_custo_fab_prev, 5);
      v_novo_valor_custo_unit_proj := round(v_custo_fab_proj, 5);

      -- se nao encontrar registros na tabela de custo REAL, ou se o valor encontrado
      -- for zero, procura o preco de custo do ultimo periodo calculado na Apuracao do Custo Real
      if v_novo_valor_custo_unit = 0.000
      then

         -- encontra a ultima ficha de custo calculada
         select max(to_date('01/' || mes || '/' || ano, 'dd/mm/yyyy'))
         into   v_ultimo_calculo
         from   rcnb_350
         where  codigo_empresa = v_local_deposito
         and    to_date('01/' || mes || '/' || ano, 'dd/mm/yyyy') <
                to_date('01/' || v_mes_movimento || '/' || v_ano_movimento, 'dd/mm/yyyy')
         and    nivel_produto = p_nivel
         and    grupo_produto = v_ref_original
         and    sub_produto = p_subgrupo
         and    item_produto = p_item;

         -- se encontrar, pega o preco de custo do periodo encontrado
         if v_ultimo_calculo is not null
         then

            v_mes_movimento2 := to_number(to_char(v_ultimo_calculo, 'MM'));
            v_ano_movimento2 := to_number(to_char(v_ultimo_calculo, 'YYYY'));

            select nvl(sum(rcnb_350.custo_real_unit_fabric), 0.000),
                   nvl(sum(rcnb_350.custo_real_unit_fabric), 0.000)
            into   v_custo_fab_prev, v_custo_fab_proj
            from   rcnb_350
            where  codigo_empresa = v_local_deposito
            and    mes = v_mes_movimento2
            and    ano = v_ano_movimento2
            and    nivel_produto = p_nivel
            and    grupo_produto = v_ref_original
            and    sub_produto = p_subgrupo
            and    item_produto = p_item;

            v_novo_valor_custo_unit      := round(v_custo_fab_prev, 5);
            v_novo_valor_custo_unit_proj := round(v_custo_fab_proj, 5);
         end if;
      end if;
   end if;

   if v_novo_valor_custo_unit = 0.000
   then
      select round(preco_custo, 5)
      into   v_novo_valor_custo_unit
      from   basi_010
      where  nivel_estrutura = p_nivel
      and    grupo_estrutura = p_grupo
      and    subgru_estrutura = p_subgrupo
      and    item_estrutura = p_item;
   end if;

   if v_novo_valor_custo_unit_proj = 0.000
   then
      select round(preco_custo, 5)
      into   v_novo_valor_custo_unit_proj
      from   basi_010
      where  nivel_estrutura = p_nivel
      and    grupo_estrutura = p_grupo
      and    subgru_estrutura = p_subgrupo
      and    item_estrutura = p_item;
   end if;

   r_valor_custo      := v_novo_valor_custo_unit;
   r_valor_custo_proj := v_novo_valor_custo_unit_proj;
   r_valor_custo_est  := v_novo_valor_custo_unit_est;

exception
   when others then
      r_valor_custo      := 0.00;
      r_valor_custo_proj := 0.00;
      r_valor_custo_est  := 0.00;
end inter_pr_encontra_preco_custo;
 

/

exec inter_pr_recompile;

