CREATE OR REPLACE TRIGGER INTER_TR_PCPB_665_1
   before insert or
          update of LARGURA_MEDIA, LARGURA_MINIMA 
   on pcpb_665
   for each row

declare
   grupo                         pcpb_020.pano_sbg_grupo%type;
   subgrupo                      pcpb_020.pano_sbg_subgrupo%type;
   item                          pcpb_020.pano_sbg_item%type;
   largura                       basi_020.largura_1%type;
   v_periodo_producao            pcpb_010.periodo_producao%type;
   cod_empresa                   pcpc_010.CODIGO_EMPRESA%type;
   VARIACAO_LARGURA_INFERIOR     basi_020.largura_1%type;
   VARIACAO_LARGURA_SUPERIOR     basi_020.largura_1%type;
   largura_minima                basi_020.largura_1%type;
   largura_maxima                basi_020.largura_1%type;
   parametro_definicao_largura   basi_023.LARGURA_RAMA%type;
   largura_calc                  pcpb_665.largura_media%type;
   largura_padrao                pcpb_665.largura_media%type;
   largura_rama                  number;
begin
   begin
      select pcpb_010.periodo_producao
      into v_periodo_producao
      from pcpb_010
      where pcpb_010.ordem_producao = :new.ordem_beneficiamento;
   end;
   
   begin   
      select pcpc_010.codigo_empresa
      into cod_empresa
      from pcpc_010
      where pcpc_010.area_periodo  = 2
        and pcpc_010.periodo_producao = v_periodo_producao;
   end;
   
   begin
      select pcpb_020.pano_sbg_grupo,
             pcpb_020.pano_sbg_subgrupo,
             pcpb_020.PANO_SBG_ITEM
      into   grupo,
             subgrupo,
             item
      from pcpb_020 
      where pcpb_020.ORDEM_PRODUCAO = :new.ordem_beneficiamento;
   end;
   
   begin
      select basi_020.largura_1
      into   largura_padrao
      from basi_020
      where basi_020.basi030_nivel030 = '2'
        and basi_020.basi030_referenc = grupo
        and basi_020.tamanho_ref      = subgrupo ;
   end;
   
   begin
     select empr_008.val_dbl / 100
     into variacao_largura_inferior
     from empr_008 
     where empr_008.codigo_empresa = cod_empresa
     and   empr_008.param          = 'pcpb.var.lar.inf' ;
   end;
   
   begin
     select empr_008.val_dbl / 100
     into variacao_largura_superior
     from empr_008 
     where empr_008.codigo_empresa = cod_empresa
     and   empr_008.param          = 'pcpb.var.lar.sup' ;
   end;

   begin
      select basi_023.largura_rama
      into   largura_rama
      from basi_023
      where basi_023.basi030_nivel030 = '2'
        and basi_023.basi030_referenc = grupo
        and basi_023.tamanho_ref      = subgrupo ;
   end;
   
   if (largura_rama =  0)
   then
      largura_calc := :new.LARGURA_MEDIA;
   else 
      largura_calc := :new.LARGURA_MINIMA;
   end if;

   :new.situacao := 0;
   if (largura_calc < (largura_padrao - variacao_largura_inferior))
   or (largura_calc > (largura_padrao + variacao_largura_superior))
   then
      :new.situacao := 1;
   end if;

end INTER_TR_PCPB_665_1;
/

exec inter_pr_recompile;
