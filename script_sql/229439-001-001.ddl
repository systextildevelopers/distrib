ALTER TABLE pedi_080
MODIFY perc_pis NUMBER(10, 4);

ALTER TABLE pedi_080
MODIFY perc_cofins NUMBER(10, 4);

ALTER TABLE pedi_081
MODIFY perc_pis NUMBER(10, 4);

ALTER TABLE pedi_081
MODIFY perc_cofins NUMBER(10, 4);

ALTER TABLE obrf_015
MODIFY perc_pis NUMBER(10, 4);

ALTER TABLE obrf_015
MODIFY perc_cofins NUMBER(10, 4);

ALTER TABLE i_obrf_015
MODIFY perc_pis_xml NUMBER(10, 4);

ALTER TABLE i_obrf_015
MODIFY perc_cofins_xml NUMBER(10, 4);

ALTER TABLE i_obrf_015_orig_xml
MODIFY perc_pis_xml NUMBER(10, 4);

ALTER TABLE i_obrf_015_orig_xml
MODIFY perc_cofins_xml NUMBER(10, 4);

ALTER TABLE sped_pc_c170
MODIFY perc_pis NUMBER(10, 4);

ALTER TABLE sped_pc_c170
MODIFY perc_cofins NUMBER(10, 4);

/
EXEC INTER_PR_RECOMPILE;
