alter table hdoc_030 add(compra_emergencial varchar2(1) default 'N');

alter table supr_090 add(tipo_pedido numeric(1) default 0);

comment on column supr_090.tipo_pedido
  is 'Tipo pedido: 0-Normal , 1-Contrato e 2-Emergencial';
  
alter table supr_090 add(envia_email_emergencial numeric(1) default 0);
  
/
