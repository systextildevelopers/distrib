INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'pedi_f428',     0,
	0,			     'Lista de Pedidos Bloqueados');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'pedi_f428',   'pedi_menu', 
	0,             1, 
	'S',           'S', 
	'S',           'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Lista de Pedidos Bloqueados'
 WHERE hdoc_036.codigo_programa = 'pedi_l425'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;

/
EXEC inter_pr_recompile;
