CREATE TABLE EXPE_008 (
  nota_fiscal             NUMBER(9) NOT NULL,
  serie                   VARCHAR2(3) NOT NULL,
  cod_empresa             NUMBER(3) NOT NULL,
  nr_solicitacao          NUMBER(9) NOT NULL,
  codigo_relatorio        VARCHAR2(9) NOT NULL,
  solicitante             VARCHAR2(250) NOT NULL,
  cod_empresa_rel         NUMBER(3) NOT NULL
);

ALTER TABLE EXPE_008 ADD CONSTRAINT PK_EXPE_008 PRIMARY KEY (nota_fiscal, serie, cod_empresa, nr_solicitacao, codigo_relatorio, solicitante, cod_empresa_rel);

ALTER TABLE EXPE_008
ADD CONSTRAINT REF_EXPE_008_EXPE_006 FOREIGN KEY (nota_fiscal, serie, cod_empresa) REFERENCES EXPE_006 (nota_fiscal, serie, cod_empresa);

/

exec inter_pr_recompile;
