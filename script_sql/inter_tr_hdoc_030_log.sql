create or replace trigger inter_tr_hdoc_030_log
   after update of codigo_usuario, supervisor, data_alteracao_senha

   on hdoc_030
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   long_aux                  varchar2(2000);
   w_supervisor_old          varchar2(20);
   w_supervisor_new          varchar2(20);

begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario_hdoc (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


   if updating   and
      (:old.codigo_usuario        <> :new.codigo_usuario          or
       :old.supervisor            <> :new.supervisor              or
       :old.data_alteracao_senha  <> :new.data_alteracao_senha
      )
   then
      long_aux := long_aux ||
          '                                                  ' ||
          'CADASTRO DE USU�RIOS'                               ||
          chr(10);
          
       

       if :old.codigo_usuario    <> :new.codigo_usuario
       then
           long_aux := long_aux  || 'C�DIGO USU�RIO:' || ' '
                                 || :old.codigo_usuario     || ' -> '
                                 || :new.codigo_usuario     || 
                                    chr(10);
       end if;

       if :old.data_alteracao_senha    <> :new.data_alteracao_senha
       then
           long_aux := long_aux  || 'DATA ALTERA��O SENHA:' || ' '
                                 || to_char(:old.data_alteracao_senha,'DD/MM/YY')     || ' -> '
                                 || to_char(:new.data_alteracao_senha,'DD/MM/YY')     ||
                                    chr(10);
       end if;
       
       if :old.supervisor    <> :new.supervisor
       then
           if :old.supervisor = 0 
           then w_supervisor_old := ' - N�o Supervisor';
           else w_supervisor_old := ' - Supervisor';
           end if;
           
           if :new.supervisor = 0 
           then w_supervisor_new := ' - N�o Supervisor';
           else w_supervisor_new := ' - Supervisor';
           end if;
           
           long_aux := long_aux  || 'SUPERVISOR:' || ' '
                                 || to_char(:old.supervisor) || w_supervisor_old     || ' -> '
                                 || to_char(:new.supervisor)|| w_supervisor_new      ||
                                    chr(10);
       end if;
       
       INSERT INTO hist_100
         ( tabela,            operacao,
           data_ocorr,        aplicacao,
           usuario_rede,      maquina_rede,
           num01,             str04,
           long01
         )
      VALUES
         ( 'HDOC_030',        'A',
           sysdate,           ws_aplicativo,
           ws_usuario_rede,   ws_maquina_rede,
           :new.empresa,      :new.usuario,
           long_aux
      );

   end if;

end inter_tr_hdoc_030_log;
/
exec inter_pr_recompile;
