
  CREATE OR REPLACE FUNCTION "INTER_FN_CALCULA_MINUTOS" 
(p_data_inicio  in date,
 p_hora_inicio  in date,
 p_data_termino in date,
 p_hora_termino in date)
return number is
   v_minutos number;
   v_data_loop date;
   v_hora_loop date;
begin
   v_minutos := 0;

   if trunc(p_data_inicio,'DD') = trunc(p_data_termino,'DD') then
      --v_minutos := p_hora_termino - p_hora_inicio;
      --esta logica para as horas e por que o vision coloca a data 16-nov-89, quando o campo e hora
      --por isso com esta logica a data da hora e igual a data de inicio e data de termino

      v_minutos := to_date(to_char(trunc(p_data_inicio,'DD'))  || ' ' || to_char(p_hora_inicio,'HH24:MI'),'DD-MON-YY HH24:MI') -
                   to_date(to_char(trunc(p_data_termino,'DD')) || ' ' || to_char(p_hora_termino,'HH24:MI'),'DD-MON-YY HH24:MI');
   else
      v_data_loop := p_data_inicio;
      v_hora_loop := to_date(to_char(trunc(p_data_inicio,'DD'))  || ' ' || to_char(p_hora_inicio,'HH24:MI'),'DD-MON-YY HH24:MI');

      while trunc(v_data_loop,'DD') <= trunc(p_data_termino,'DD')
      loop
         if trunc(v_data_loop,'DD') <> trunc(p_data_termino,'DD') then
            v_minutos := v_minutos + ((to_date(to_char(trunc(v_data_loop,'DD')) || ' ' || '23:59','DD-MON-YY HH24:MI') - v_hora_loop) + 0.0001);
            v_data_loop := v_data_loop + 1;
            v_hora_loop := to_date(to_char(trunc(v_data_loop,'DD')) || ' ' || '00:00','DD-MON-YY HH24:MI');
         else
            v_minutos := v_minutos + (to_date(to_char(trunc(p_data_termino,'DD')) || ' ' || to_char(p_hora_termino,'HH24:MI'),'DD-MON-YY HH24:MI') - v_hora_loop);
            v_data_loop := v_data_loop + 1;
         end if;
      end loop;
   end if;

   v_minutos := v_minutos * 1440;

   return(v_minutos);

end inter_fn_calcula_minutos;

 

/

exec inter_pr_recompile;

