create or replace procedure "INTER_PR_GERA_CLASSIFIC_LOTE" (
    p_tipo_classificacao in number
    ,p_nr_lote_adps      in number
    ,p_situacao_lote     in number

)

is

    v_volumes_pedido_total number;
    v_situacao_volume      number;
    v_seq_leitura          number;

begin
              
    v_seq_leitura := 1;
  
    delete
    from adps_001
    where nr_lote_adps = p_nr_lote_adps;

    delete
    from adps_003
    where nr_lote_adps = p_nr_lote_adps;

    if p_tipo_classificacao = 1 then

        for endereco in (
            select
                volume.endereco_caixa_peca
                ,nota.codigo_empresa
            from fatu_050 nota

            inner join pcpc_320 volume on
                    volume.nota_fiscal = nota.num_nota_fiscal
                and volume.serie_nota  = nota.serie_nota_fisc
                and volume.cod_empresa = nota.codigo_empresa

            where nvl(nota.nr_lote_adps,0) = p_nr_lote_adps

            group by
                volume.endereco_caixa_peca
                ,nota.codigo_empresa

            order by 1
        )loop

            for endereco_pedido in (
                select distinct
                    volume.endereco_caixa_peca
                    ,volume.pedido_venda /*nota.pedido_venda*/
                    ,nota.num_nota_fiscal
                    ,nota.serie_nota_fisc
                    ,nota.codigo_empresa
                    ,(
                        select
                                count(1) 
                        from pcpc_320
                        where  pcpc_320.nota_fiscal                   = nota.num_nota_fiscal
                            and pcpc_320.serie_nota                   = nota.serie_nota_fisc
                            and pcpc_320.cod_empresa                  = nota.codigo_empresa
                            and nvl(pcpc_320.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')
                    ) as qtde
                from fatu_050 nota

                inner join pcpc_320 volume on
                        volume.nota_fiscal = nota.num_nota_fiscal
                    and volume.serie_nota  = nota.serie_nota_fisc
                    and volume.cod_empresa = nota.codigo_empresa

                where   nota.nr_lote_adps                   = p_nr_lote_adps
                    and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')

                group by
                    volume.endereco_caixa_peca
                    ,volume.pedido_venda /*nota.pedido_venda*/
                    ,nota.num_nota_fiscal
                    ,nota.serie_nota_fisc
                    ,nota.codigo_empresa
            )loop

                select
                    count(1)
                into
                    v_volumes_pedido_total
                from pcpc_320

                where   pcpc_320.nota_fiscal = endereco_pedido.num_nota_fiscal
                    and pcpc_320.serie_nota  = endereco_pedido.serie_nota_fisc
                    and pcpc_320.cod_empresa = endereco_pedido.codigo_empresa
                    and pcpc_320.pedido_venda = endereco_pedido.pedido_venda;
                        
                begin
                    insert into adps_001(
                        nr_lote_adps
                        ,seq_leitura
                        ,endereco
                        ,pedido_venda
                        ,volumes_pedido
                        ,volumes_saldo
                        ,volumes_lidos
                        ,numero_nota
                        ,serie_nota
                        ,situacao
                    )values(
                        p_nr_lote_adps
                        ,v_seq_leitura
                        ,endereco.endereco_caixa_peca
                        ,endereco_pedido.pedido_venda
                        ,endereco_pedido.qtde
                        ,v_volumes_pedido_total
                        ,0
                        ,endereco_pedido.num_nota_fiscal
                        ,endereco_pedido.serie_nota_fisc
                        ,p_situacao_lote
                    );
                end;

            end loop;
                    
            for volumes in (
                select
                    nota.pedido_venda
                    ,volume.numero_volume
                    ,volume.nr_lote_adps
                from fatu_050 nota

                inner join pcpc_320 volume on
                        volume.nota_fiscal = nota.num_nota_fiscal
                    and volume.serie_nota  = nota.serie_nota_fisc
                    and volume.cod_empresa = nota.codigo_empresa

                where   nota.nr_lote_adps                   = p_nr_lote_adps
                    and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')

                group by
                    nota.pedido_venda
                    ,volume.numero_volume
                    ,nota.codigo_empresa
                    ,volume.nr_lote_adps
            )loop

                if volumes.nr_lote_adps > 0 then
                    v_situacao_volume := 1;
                else 
                    v_situacao_volume := 0;
                end if;

                insert into adps_003(
                    nr_lote_adps
                    ,pedido_venda
                    ,numero_volume
                    ,situacao
                )values (
                    p_nr_lote_adps
                    ,volumes.pedido_venda
                    ,volumes.numero_volume
                    ,v_situacao_volume
                );

            end loop;

            v_seq_leitura := v_seq_leitura + 1;

        end loop;

    elsif p_tipo_classificacao = 2 then

        for endereco in (
            select
                volume.endereco_caixa_peca
            from fatu_050 nota

            inner join pcpc_320 volume on
                    volume.nota_fiscal = nota.num_nota_fiscal
                and volume.serie_nota  = nota.serie_nota_fisc
                and volume.cod_empresa = nota.codigo_empresa

            where nvl(nota.nr_lote_adps,0) = p_nr_lote_adps

            group by
                volume.endereco_caixa_peca
                ,nota.codigo_empresa

            order by 1 desc
        )loop

            for endereco_pedido in (
                select distinct
                    volume.endereco_caixa_peca
                    ,volume.pedido_venda /*nota.pedido_venda*/
                    ,nota.num_nota_fiscal
                    ,nota.serie_nota_fisc
                    ,nota.codigo_empresa
                    ,(
                        select
                            count(1) 
                        from pcpc_320
                        where   pcpc_320.nota_fiscal                  = nota.num_nota_fiscal
                            and pcpc_320.serie_nota                   = nota.serie_nota_fisc
                            and nvl(pcpc_320.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')
                    ) as qtde
                from fatu_050 nota

                inner join pcpc_320 volume on
                        volume.nota_fiscal = nota.num_nota_fiscal
                    and volume.serie_nota  = nota.serie_nota_fisc
                    and volume.cod_empresa = nota.codigo_empresa

                where   nota.nr_lote_adps                   = p_nr_lote_adps
                    and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')

                group by
                    volume.endereco_caixa_peca
                    ,volume.pedido_venda /*nota.pedido_venda*/ 
                    ,nota.num_nota_fiscal
                    ,nota.serie_nota_fisc
                    ,nota.codigo_empresa
            )loop

                select
                    count(1)
                into
                    v_volumes_pedido_total
                from pcpc_320
                where   pcpc_320.nota_fiscal = endereco_pedido.num_nota_fiscal
                    and pcpc_320.serie_nota  = endereco_pedido.serie_nota_fisc
                    and pcpc_320.cod_empresa = endereco_pedido.codigo_empresa
                    and pcpc_320.pedido_venda = endereco_pedido.pedido_venda;
                        
                begin
                    insert into adps_001(
                        nr_lote_adps
                        ,seq_leitura
                        ,endereco
                        ,pedido_venda
                        ,volumes_pedido
                        ,volumes_saldo
                        ,volumes_lidos
                        ,numero_nota
                        ,serie_nota
                        ,situacao
                    )values (
                        p_nr_lote_adps
                        ,v_seq_leitura
                        ,endereco.endereco_caixa_peca
                        ,endereco_pedido.pedido_venda
                        ,endereco_pedido.qtde
                        ,v_volumes_pedido_total
                        ,0
                        ,endereco_pedido.num_nota_fiscal
                        ,endereco_pedido.serie_nota_fisc
                        ,p_situacao_lote
                    );
                end;

            end loop;
                    
            for volumes in (
                select 
                    nota.pedido_venda
                    ,volume.numero_volume
                    ,volume.nr_lote_adps
                from fatu_050 nota

                inner join pcpc_320 volume on
                        volume.nota_fiscal = nota.num_nota_fiscal
                    and volume.serie_nota  = nota.serie_nota_fisc
                    and volume.cod_empresa = nota.codigo_empresa

                where   nota.nr_lote_adps                   = p_nr_lote_adps
                    and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')

                group by
                    nota.pedido_venda
                    ,volume.numero_volume
                    ,nota.codigo_empresa
                    ,volume.nr_lote_adps
            )loop

                if volumes.nr_lote_adps > 0 then
                    v_situacao_volume := 1;
                else 
                    v_situacao_volume := 0;
                end if;

                insert into adps_003(
                    nr_lote_adps
                    ,pedido_venda
                    ,numero_volume
                    ,situacao
                )values (
                    p_nr_lote_adps
                    ,volumes.pedido_venda
                    ,volumes.numero_volume
                    ,v_situacao_volume
                );

            end loop;

            v_seq_leitura := v_seq_leitura + 1;

        end loop;

    elsif p_tipo_classificacao = 3 then

        for endereco in (
            select
                count(1) as qtde
                ,volume.endereco_caixa_peca
            from fatu_050 nota

            inner join pcpc_320 volume on
                    volume.nota_fiscal = nota.num_nota_fiscal
                and volume.serie_nota  = nota.serie_nota_fisc
                and volume.cod_empresa = nota.codigo_empresa

            where nvl(nota.nr_lote_adps,0) = p_nr_lote_adps

            group by
                volume.endereco_caixa_peca
                ,nota.codigo_empresa

            order by 1 desc
        )loop

            for endereco_pedido in (
                select distinct
                    volume.endereco_caixa_peca
                    ,volume.pedido_venda /*nota.pedido_venda*/
                    ,nota.num_nota_fiscal, nota.serie_nota_fisc
                    ,nota.codigo_empresa
                    ,(
                        select
                            count(1) 
                        from pcpc_320
                        where   pcpc_320.nota_fiscal              = nota.num_nota_fiscal
                            and pcpc_320.serie_nota               = nota.serie_nota_fisc
                            and pcpc_320.cod_empresa              = nota.codigo_empresa
                        and nvl(pcpc_320.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')
                    ) as qtde
                from fatu_050 nota

                inner join pcpc_320 volume on
                        volume.nota_fiscal = nota.num_nota_fiscal
                    and volume.serie_nota  = nota.serie_nota_fisc
                    and volume.cod_empresa = nota.codigo_empresa

                where   nota.nr_lote_adps                   = p_nr_lote_adps
                    and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')

                group by
                    volume.endereco_caixa_peca
                    ,volume.pedido_venda /*nota.pedido_venda*/
                    ,nota.num_nota_fiscal
                    ,nota.serie_nota_fisc
                    ,nota.codigo_empresa
            )loop

                select
                    count(1)
                into
                    v_volumes_pedido_total
                from pcpc_320
                where   pcpc_320.nota_fiscal = endereco_pedido.num_nota_fiscal
                    and pcpc_320.serie_nota  = endereco_pedido.serie_nota_fisc
                    and pcpc_320.cod_empresa = endereco_pedido.codigo_empresa
                    and pcpc_320.pedido_venda = endereco_pedido.pedido_venda;
                        
                begin
                    insert into adps_001(
                        nr_lote_adps
                        ,seq_leitura
                        ,endereco
                        ,pedido_venda
                        ,volumes_pedido
                        ,volumes_saldo
                        ,volumes_lidos
                        ,numero_nota
                        ,serie_nota
                        ,situacao
                    )values (
                        p_nr_lote_adps
                        ,v_seq_leitura
                        ,endereco.endereco_caixa_peca
                        ,endereco_pedido.pedido_venda
                        ,endereco_pedido.qtde
                        ,v_volumes_pedido_total
                        ,0
                        ,endereco_pedido.num_nota_fiscal
                        ,endereco_pedido.serie_nota_fisc
                        ,p_situacao_lote
                    );
                end;

            end loop;

            for volumes in (
                select
                    nota.pedido_venda
                    ,volume.numero_volume
                    ,volume.nr_lote_adps
                from fatu_050 nota

                inner join pcpc_320 volume on
                        volume.nota_fiscal = nota.num_nota_fiscal
                    and volume.serie_nota  = nota.serie_nota_fisc
                    and volume.cod_empresa = nota.codigo_empresa

                where   nota.nr_lote_adps                   = p_nr_lote_adps
                    and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')

                group by
                    nota.pedido_venda
                    ,volume.numero_volume
                    ,nota.codigo_empresa
                    ,volume.nr_lote_adps

            )loop

                if volumes.nr_lote_adps > 0 then
                    v_situacao_volume := 1;
                else 
                    v_situacao_volume := 0;
                end if;

                insert into adps_003(
                    nr_lote_adps
                    ,pedido_venda
                    ,numero_volume
                    ,situacao
                )values (
                    p_nr_lote_adps
                    ,volumes.pedido_venda
                    ,volumes.numero_volume
                    ,v_situacao_volume
                );

            end loop;

            v_seq_leitura := v_seq_leitura + 1;

        end loop;

    elsif p_tipo_classificacao = 4 then

        for endereco in (
            select
                count(1) as qtde
                ,volume.endereco_caixa_peca
            from fatu_050 nota

            inner join pcpc_320 volume on
                    volume.nota_fiscal = nota.num_nota_fiscal
                and volume.serie_nota  = nota.serie_nota_fisc
                and volume.cod_empresa = nota.codigo_empresa

            where nvl(nota.nr_lote_adps,0) = p_nr_lote_adps

            group by
                volume.endereco_caixa_peca
                ,nota.codigo_empresa

            order by 1
        )loop

            for endereco_pedido in (
                select distinct
                    volume.endereco_caixa_peca
                    ,volume.pedido_venda /*nota.pedido_venda*/
                    ,nota.num_nota_fiscal
                    ,nota.serie_nota_fisc
                    ,nota.codigo_empresa
                    ,(
                        select
                            count(1) 
                        from pcpc_320
                        where pcpc_320.nota_fiscal                    = nota.num_nota_fiscal
                            and pcpc_320.serie_nota                   = nota.serie_nota_fisc
                            and pcpc_320.cod_empresa                  = nota.codigo_empresa
                            and nvl(pcpc_320.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')
                    ) as qtde
                from fatu_050 nota

                inner join pcpc_320 volume on
                        volume.nota_fiscal = nota.num_nota_fiscal
                    and volume.serie_nota  = nota.serie_nota_fisc
                    and volume.cod_empresa = nota.codigo_empresa
                    
                where   nota.nr_lote_adps                   = p_nr_lote_adps
                    and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')

                group by
                    volume.endereco_caixa_peca
                    ,volume.pedido_venda /*nota.pedido_venda*/
                    ,nota.num_nota_fiscal
                    ,nota.serie_nota_fisc
                    ,nota.codigo_empresa
            )loop

                select
                    count(1)
                into
                    v_volumes_pedido_total
                from pcpc_320
                where   pcpc_320.nota_fiscal = endereco_pedido.num_nota_fiscal
                    and pcpc_320.serie_nota  = endereco_pedido.serie_nota_fisc
                    and pcpc_320.cod_empresa = endereco_pedido.codigo_empresa
                    and pcpc_320.pedido_venda = endereco_pedido.pedido_venda;
                        
                begin
                    insert into adps_001(
                        nr_lote_adps
                        ,seq_leitura
                        ,endereco
                        ,pedido_venda
                        ,volumes_pedido
                        ,volumes_saldo
                        ,volumes_lidos
                        ,numero_nota
                        ,serie_nota
                        ,situacao
                    )values (
                        p_nr_lote_adps
                        ,v_seq_leitura
                        ,endereco.endereco_caixa_peca
                        ,endereco_pedido.pedido_venda
                        ,endereco_pedido.qtde
                        ,v_volumes_pedido_total
                        ,0
                        ,endereco_pedido.num_nota_fiscal
                        ,endereco_pedido.serie_nota_fisc
                        ,p_situacao_lote
                    );
                end;

            end loop;

            for volumes in (
                select
                    nota.pedido_venda
                    ,volume.numero_volume
                    ,volume.nr_lote_adps
                from fatu_050 nota

                inner join pcpc_320 volume on
                        volume.nota_fiscal = nota.num_nota_fiscal
                    and volume.serie_nota  = nota.serie_nota_fisc
                    and volume.cod_empresa = nota.codigo_empresa

                where   nota.nr_lote_adps                   = p_nr_lote_adps
                    and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')

                group by
                    nota.pedido_venda
                    ,volume.numero_volume
                    ,nota.codigo_empresa
                    ,volume.nr_lote_adps
            )loop

                if volumes.nr_lote_adps > 0 then
                    v_situacao_volume := 1;
                else 
                    v_situacao_volume := 0;
                end if;

                insert into adps_003(
                    nr_lote_adps
                    ,pedido_venda
                    ,numero_volume
                    ,situacao
                )values (
                    p_nr_lote_adps
                    ,volumes.pedido_venda
                    ,volumes.numero_volume
                    ,v_situacao_volume
                );

            end loop;
                    
            v_seq_leitura := v_seq_leitura + 1;

        end loop;

    elsif p_tipo_classificacao = 5 then

        for endereco in (
            select
                volume.endereco_caixa_peca
                ,(
                    select
                        count(1) 
                    from pcpc_320 
                    where   pcpc_320.endereco_caixa_peca = volume.endereco_caixa_peca
                        and nvl(pcpc_320.nr_lote_adps,0) = 0
                        and pcpc_320.situacao_volume in (1,2)
                ) - count(1) as qtde
                from fatu_050 nota

                inner join pcpc_320 volume on
                        volume.nota_fiscal = nota.num_nota_fiscal
                    and volume.serie_nota  = nota.serie_nota_fisc
                    and volume.cod_empresa = nota.codigo_empresa

                where nvl(nota.nr_lote_adps,0) = p_nr_lote_adps

                group by
                    volume.endereco_caixa_peca
                    ,nota.codigo_empresa

                order by 2
        )loop

            for endereco_pedido in (
                select distinct
                    volume.endereco_caixa_peca
                    ,volume.pedido_venda /*nota.pedido_venda*/
                    ,nota.num_nota_fiscal
                    ,nota.serie_nota_fisc
                    ,nota.codigo_empresa
                    ,(
                        select
                            count(1) 
                        from pcpc_320
                        where  pcpc_320.nota_fiscal                   = nota.num_nota_fiscal
                            and pcpc_320.serie_nota                   = nota.serie_nota_fisc
                            and pcpc_320.cod_empresa                  = nota.codigo_empresa
                            and nvl(pcpc_320.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')
                    ) as qtde
            from fatu_050 nota

            inner join pcpc_320 volume on
                    volume.nota_fiscal = nota.num_nota_fiscal
                and volume.serie_nota  = nota.serie_nota_fisc
                and volume.cod_empresa = nota.codigo_empresa

            where   nota.nr_lote_adps                   = p_nr_lote_adps
                and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')

            group by
                volume.endereco_caixa_peca
                ,volume.pedido_venda /*nota.pedido_venda*/
                ,nota.num_nota_fiscal
                ,nota.serie_nota_fisc
                ,nota.codigo_empresa
            )loop

                select
                    count(1)
                into v_volumes_pedido_total
                from pcpc_320
                where   pcpc_320.nota_fiscal = endereco_pedido.num_nota_fiscal
                    and pcpc_320.serie_nota  = endereco_pedido.serie_nota_fisc
                    and pcpc_320.cod_empresa = endereco_pedido.codigo_empresa
                    and pcpc_320.pedido_venda = endereco_pedido.pedido_venda;
                        
                begin
                    insert into adps_001(
                        nr_lote_adps
                        ,seq_leitura
                        ,endereco
                        ,pedido_venda
                        ,volumes_pedido
                        ,volumes_saldo
                        ,volumes_lidos
                        ,numero_nota
                        ,serie_nota
                        ,situacao
                    )values (
                        p_nr_lote_adps
                        ,v_seq_leitura
                        ,endereco.endereco_caixa_peca
                        ,endereco_pedido.pedido_venda
                        ,endereco_pedido.qtde
                        ,v_volumes_pedido_total
                        ,0
                        ,endereco_pedido.num_nota_fiscal
                        ,endereco_pedido.serie_nota_fisc
                        ,p_situacao_lote
                    );
                end;

            end loop;

            for volumes in (
                select
                    nota.pedido_venda
                    ,volume.numero_volume
                    ,volume.nr_lote_adps
                from fatu_050 nota
                
                inner join pcpc_320 volume on
                        volume.nota_fiscal = nota.num_nota_fiscal
                    and volume.serie_nota  = nota.serie_nota_fisc
                    and volume.cod_empresa = nota.codigo_empresa

                where   nota.nr_lote_adps                   = p_nr_lote_adps
                    and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')

                group by
                    nota.pedido_venda
                    ,volume.numero_volume
                    ,nota.codigo_empresa
                    ,volume.nr_lote_adps
            )loop

                if volumes.nr_lote_adps > 0 then
                    v_situacao_volume := 1;
                else 
                    v_situacao_volume := 0;
                end if;

                insert into adps_003(
                    nr_lote_adps
                    ,pedido_venda
                    ,numero_volume
                    ,situacao
                )values (
                    p_nr_lote_adps
                    ,volumes.pedido_venda
                    ,volumes.numero_volume
                    ,v_situacao_volume
                );

            end loop;
                    
            v_seq_leitura := v_seq_leitura + 1;

        end loop;

    elsif p_tipo_classificacao = 6 then

        for endereco in (
            select
                volume.endereco_caixa_peca
                ,(
                    select
                        count(1) 
                    from pcpc_320 
                    where   pcpc_320.endereco_caixa_peca = volume.endereco_caixa_peca
                        and nvl(pcpc_320.nr_lote_adps,0) = 0
                        and pcpc_320.situacao_volume in (1,2)
                    
                ) - count(1) as qtde
            from fatu_050 nota

            inner join pcpc_320 volume on
                    volume.nota_fiscal = nota.num_nota_fiscal
                and volume.serie_nota  = nota.serie_nota_fisc
                and volume.cod_empresa = nota.codigo_empresa

            where nvl(nota.nr_lote_adps,0) = p_nr_lote_adps

            group by
                volume.endereco_caixa_peca
                ,nota.codigo_empresa

            order by 2 desc
        )loop

            for endereco_pedido in (
                select distinct
                    volume.endereco_caixa_peca
                    ,volume.pedido_venda /*nota.pedido_venda*/
                    ,nota.num_nota_fiscal
                    ,nota.serie_nota_fisc
                    ,nota.codigo_empresa
                    ,(
                        select
                            count(1) 
                        from pcpc_320
                        where   pcpc_320.nota_fiscal                = nota.num_nota_fiscal
                            and pcpc_320.serie_nota                 = nota.serie_nota_fisc
                            and pcpc_320.cod_empresa                = nota.codigo_empresa
                            and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')
                    ) as qtde
                from fatu_050 nota

                inner join pcpc_320 volume on
                        volume.nota_fiscal = nota.num_nota_fiscal
                    and volume.serie_nota  = nota.serie_nota_fisc
                    and volume.cod_empresa = nota.codigo_empresa

                where   nota.nr_lote_adps                   = p_nr_lote_adps
                    and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')

                group by
                    volume.endereco_caixa_peca
                    ,volume.pedido_venda /*nota.pedido_venda*/ 
                    ,nota.num_nota_fiscal
                    ,nota.serie_nota_fisc
                    ,nota.codigo_empresa
            )loop

                select
                    count(1)
                into
                    v_volumes_pedido_total
                from pcpc_320
                where   pcpc_320.nota_fiscal = endereco_pedido.num_nota_fiscal
                    and pcpc_320.serie_nota  = endereco_pedido.serie_nota_fisc
                    and pcpc_320.cod_empresa = endereco_pedido.codigo_empresa
                    and pcpc_320.pedido_venda = endereco_pedido.pedido_venda;
                        
                begin
                    insert into adps_001(
                        nr_lote_adps
                        ,seq_leitura
                        ,endereco
                        ,pedido_venda
                        ,volumes_pedido
                        ,volumes_saldo
                        ,volumes_lidos
                        ,numero_nota
                        ,serie_nota
                        , situacao
                    )values (
                        p_nr_lote_adps
                        ,v_seq_leitura
                        ,endereco.endereco_caixa_peca
                        ,endereco_pedido.pedido_venda
                        ,endereco_pedido.qtde
                        ,v_volumes_pedido_total
                        ,0
                        ,endereco_pedido.num_nota_fiscal
                        ,endereco_pedido.serie_nota_fisc
                        ,p_situacao_lote
                    );
                end;

            end loop;
                    
            for volumes in (
                select
                    nota.pedido_venda
                    ,volume.numero_volume
                    ,volume.nr_lote_adps
                from fatu_050 nota

                inner join pcpc_320 volume on
                        volume.nota_fiscal = nota.num_nota_fiscal
                    and volume.serie_nota  = nota.serie_nota_fisc
                    and volume.cod_empresa = nota.codigo_empresa

                where   nota.nr_lote_adps                   = p_nr_lote_adps
                    and nvl(volume.endereco_caixa_peca,' ') = nvl(endereco.endereco_caixa_peca,' ')

                group by
                    nota.pedido_venda
                    ,volume.numero_volume
                    ,nota.codigo_empresa
                    ,volume.nr_lote_adps
            )loop

                if volumes.nr_lote_adps > 0 then
                    v_situacao_volume := 1;
                else 
                    v_situacao_volume := 0;
                end if;

                insert into adps_003(
                    nr_lote_adps
                    ,pedido_venda
                    ,numero_volume
                    ,situacao
                )values (
                    p_nr_lote_adps
                    ,volumes.pedido_venda
                    ,volumes.numero_volume
                    ,v_situacao_volume
                );

            end loop;
                    
            v_seq_leitura := v_seq_leitura + 1;

        end loop;

    end if;
       
end;
/
