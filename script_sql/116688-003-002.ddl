alter table sped_ctb_j100 add ind_dc_cta varchar2(1);
alter table sped_ctb_j100 modify ind_dc_cta default ' ';

alter table sped_ctb_j100 add ind_grp_dre varchar2(1);
alter table sped_ctb_j100 modify ind_grp_dre default ' ';

exec inter_pr_recompile;
