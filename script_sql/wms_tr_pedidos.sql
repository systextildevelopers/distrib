create or replace trigger wms_tr_pedidos
  before update of flag_importacao_wms on inte_wms_pedidos
  for each row
declare
  -- local variables here
   v_qtde_pecas_325             number;
   v_qtde_saldo_sug_wms         number;
begin

   if :new.flag_importacao_wms = 4 /*Expedido*/
   or :new.flag_importacao_wms = 5 /*Coleta cancelada*/
   then

      if :new.flag_importacao_wms = 4 and :new.tipo_pedido = 0 /*Expedido e pedido normal, desfazer o saldo da sugest�o em rela��o ao que est� no WMS*/
      then

         --Buscar quantidade enviada para o WMS
         for f_itens_pedido in (
            select inte_wms_itens_ped.seq_item, inte_wms_itens_ped.qtd_pedida
            from  inte_wms_itens_ped
            where inte_wms_itens_ped.numero_pedido = :new.numero_pedido
            order by inte_wms_itens_ped.seq_item
         )
         LOOP

            --Buscar quanto foi montado para descontar s� o saldo
            begin
               select nvl(sum(pcpc_325.qtde_pecas_real), 0)
               into   v_qtde_pecas_325
               from pcpc_325, pcpc_320
               where pcpc_325.numero_volume   = pcpc_320.numero_volume
                 and pcpc_320.pedido_venda    = :new.numero_pedido
                 and pcpc_325.seq_item_pedido = f_itens_pedido.seq_item;
            exception
            when no_data_found then
               v_qtde_pecas_325 := 0;
            end;

            v_qtde_saldo_sug_wms := f_itens_pedido.qtd_pedida - v_qtde_pecas_325;

            --Se teve saldo em rela��o ao que foi enviado...
            if v_qtde_saldo_sug_wms > 0
            then

               begin

                  /*desfazer o saldo da sugest�o*/
                  update fatu_780
                  set fatu_780.qtde_sugerida       = fatu_780.qtde_sugerida - v_qtde_saldo_sug_wms
                  where fatu_780.pedido            = :new.numero_pedido
                    and fatu_780.sequencia         = f_itens_pedido.seq_item
                    and fatu_780.flag_controle_wms = 1;  --Dentro do WMS
               exception
               when others then
                  raise_application_error(-20000, 'N�o atualizou a tabela PCPC_325.' || Chr(10) || SQLERRM);
               end;


            end if;

         END LOOP;  --fim do la�o dos itens do pedido do wms

      end if;

      :new.flag_importacao_st := 1 ;

   end if;

end wms_tr_pedidos;
/

exec inter_pr_recompile;

/* versao: 2 */


 exit;


 exit;


 exit;

 exit;
