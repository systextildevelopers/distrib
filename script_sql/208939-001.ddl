insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('rcnb_f553', 'Parāmetros de Produtos', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'rcnb_f553', 'menu_mp00', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'rcnb_f553', 'menu_mp00', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Parāmetros de Produtos'
 where hdoc_036.codigo_programa = 'rcnb_f553'
   and hdoc_036.locale          = 'es_ES';
commit;
