
  CREATE OR REPLACE TRIGGER INTER_TR_ESTQ_300
before insert or
       delete
       on estq_300
for each row

declare
   v_periodo_estoque    date;
   v_flag_fechamento    empr_001.flag_fechamento%type;
   v_dep_inativo        number;
   v_tran_inativa       number;
   v_data_lim_inativo   date;
   v_dtfim_fecha_estq   date;
   v_flag_fecha_estq    number;
   v_emp_ccusto         number;
   v_reg_hdoc_060       number;
   v_cta_estoque        number;
   v_bloq_hdoc_060      number;
   v_tipo_volume        number;
   v_situacao_deposito  basi_205.situacao_deposito%type;
   v_empresa_deposito   basi_205.local_deposito%type;
   v_data_pre_fech      date;
   v_unidade_medida     basi_030.unidade_medida%type;
   v_nome_programa      hdoc_090.programa%type;
/*
   v_sid                number;
   v_osuser             hdoc_090.usuario_rede%type;
*/
   v_existe                  number;
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
begin

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
	  :new.tabela_origem := upper(:new.tabela_origem);
      begin
         select flag_fechamento,   periodo_estoque
         into   v_flag_fechamento, v_periodo_estoque
         from empr_001;
      end;

      begin
         select dtfim_fecha_estq,   flag_fecha_estq
         into   v_dtfim_fecha_estq, v_flag_fecha_estq
         from empr_002;
      end;

/*
      begin
         select sid, osuser
         into v_sid, v_osuser
         from V_LISTA_SESSAO_BANCO
         where rownum = 1;
      end;
*/

      -- Para a nova fórmula de calculo se faz necessário calcular
      -- o valor do movimento unitário, para que mesmo após calcular a cardez
      -- ser possível saber qual o valor original do movimento
      :new.valor_contabil_unitario := :new.valor_movimento_unitario;

      begin
         select tran_inativa
         into v_tran_inativa
         from estq_005
         where estq_005.codigo_transacao = :new.codigo_transacao;
      exception
         when no_data_found then
         raise_application_error(-20000,'Transação não cadastrada.');
      end;

      begin
         select dep_inativo,        data_lim_inativo,
                local_deposito,     situacao_deposito,
                tipo_volume
         into   v_dep_inativo,      v_data_lim_inativo,
                v_empresa_deposito, v_situacao_deposito,
                v_tipo_volume
         from basi_205
         where basi_205.codigo_deposito = :new.codigo_deposito;
      exception
         when no_data_found then
         raise_application_error(-20000,'Depósito ' || to_char(:new.codigo_deposito) || ' que é utilizado para movimentação não está cadastrado.');
      end;

      begin
         select data_pre_fechamento
         into   v_data_pre_fech from fatu_502
         where fatu_502.codigo_empresa = v_empresa_deposito;
      exception
         when no_data_found then
         raise_application_error(-20000,'Empresa do Depósito inv�lida.');
      end;

      -- Verifica se nivel informado for igual a '0'
      if :new.nivel_estrutura = 0
      then
         raise_application_error(-20000,'O nivel informado inv�lido.');
      end if;

      -- Nao permite inserir registro sem a informacao de entrada_saida
      if :new.entrada_saida not in ('E', 'S') or :new.entrada_saida is null
      then
        raise_application_error(-20000, 'Aten��o! Movimenta��o sem indica��o se � entrada ou saida.');
      end if;

      -- Verifica se produto existe na tabela basi_010
      begin
      select 1 into v_existe
      from basi_010
      where basi_010.nivel_estrutura = :new.nivel_estrutura
        and basi_010.grupo_estrutura = :new.grupo_estrutura
        and basi_010.subgru_estrutura = :new.subgrupo_estrutura
        and basi_010.item_estrutura = :new.item_estrutura;
      exception
         when no_data_found then
         raise_application_error(-20000,'Produto ' || :new.nivel_estrutura || '.' || :new.grupo_estrutura || '.' || :new.subgrupo_estrutura || '.' || :new.item_estrutura || ' que é utilizado para movimentação não está cadastrado.');
      end;

      -- Quando a transacao for ENTRADA, ira consistir o nivel do produto conforme o tipo de volume do deposito que esta sendo movimentado
      if :new.entrada_saida = 'E' and v_tipo_volume <> 0
      then
         if v_tipo_volume <> 2 and v_tipo_volume <> 4 and (:new.nivel_estrutura = 2 or :new.nivel_estrutura = 4) and v_tipo_volume <> :new.nivel_estrutura
         then
            raise_application_error(-20000,'O deposito ' || to_char(:new.codigo_deposito) || ' possui um tipo de volume diferente do nivel do produto a ser movimentado. O processo sera abortado.');
         end if;
      end if;

      -- Verifica se a transacao esta inativa
      if v_tran_inativa = 1
      then
         raise_application_error(-20000,'Esta transação esta inativa para efetuar movimentações.');
      end if;

      -- Verifica se o deposito esta inativo
      if v_dep_inativo = 1 and (v_data_lim_inativo is null or :new.data_movimento < v_data_lim_inativo) and :new.PROCESSO_SYSTEXTIL <> 'EQUALIZACAO'
      then
         raise_application_error(-20000,'Este deposito: ' || to_char(:new.codigo_deposito) || ' esta inativo para efetuar movimentações neste período.');
      end if;

      if v_situacao_deposito = 1 and
         :new.data_movimento >= trunc(v_periodo_estoque,'MM') and
         :new.data_movimento <= last_day(v_periodo_estoque)
      then
         raise_application_error(-20000,'Este deposito: ' || to_char(:new.codigo_deposito) || '  está fechado, não poderá efetuar movimentações neste período.');
      end if;

      if :new.quantidade < 0.00
      then
         raise_application_error(-20000,'Não é permitido quantidade negativa no movimento.');
      end if;

      if :new.data_movimento > sysdate and :new.PROCESSO_SYSTEXTIL <> 'EQUALIZACAO'
      then
         raise_application_error(-20000,'Data de movimento n�o pode ser maior do que a data atual.');
      end if;

      -- Verifica se não est� sendo executado o fechamento de estoques
      if v_flag_fechamento = 1
      then
         raise_application_error(-20000,'Está sendo executado o fechamento dos estoques. Não podemos realizar movimentos enquanto processo não estiver encerrado.');
      end if;

      -- Verifica se o lancamento não é menor que a data de fechamento de estoque
      if v_periodo_estoque is null or :new.data_movimento < v_periodo_estoque
      then
         raise_application_error(-20000,'Data do movimento não pode ser menor que periodo de estoque.');
      end if;

      -- Verifica se o lancamento não é de data que esta sendo fechado o estoque
      if (v_dtfim_fecha_estq is null or :new.data_movimento <= v_dtfim_fecha_estq) and v_flag_fecha_estq > 0
      then
         raise_application_error(-20000,'Fechamento de estoque ocorrendo. Data do movimento não pode ser do periodo em fechamento.');
      end if;

      -- Verifica se o lancamento não é menor que a data de pré-fechamento do estoque
      if (v_data_pre_fech is null or :new.data_movimento <= v_data_pre_fech)
      then
         raise_application_error(-20000,'Data do movimento não pode ser menor que o periodo de pré-fechamento do estoque.');
      end if;

      -- Verifica se a empresa trabalha com regras de movimentação C.Custo X Conta Estoque
      if :new.tabela_origem = 'ESTQ_300' and :new.centro_custo > 0
      then

         begin
            select basi_185.local_entrega into v_emp_ccusto
            from basi_185
            where basi_185.centro_custo = :new.centro_custo;
         exception
            when no_data_found then
            raise_application_error(-20000,'não existe um local de entraga para este centro de custo.');
         end;

         select count(*) into v_reg_hdoc_060
         from hdoc_060
         where hdoc_060.codigo_empresa = v_emp_ccusto
           and hdoc_060.tipo_operacao  = 9;

         if v_reg_hdoc_060 > 0
         then
            begin
               select basi_030.conta_estoque into v_cta_estoque
               from basi_030
               where basi_030.nivel_estrutura = :new.nivel_estrutura
                 and basi_030.referencia      = :new.grupo_estrutura;
            exception
               when no_data_found then
               raise_application_error(-20000,'Conta de estoque não cadastrada.');
            end;

            select count(*) into v_bloq_hdoc_060
            from hdoc_060
            where  hdoc_060.codigo_empresa = v_emp_ccusto
              and  hdoc_060.tipo_operacao  = 9
              and (hdoc_060.centro_custo   = :new.centro_custo
               or  hdoc_060.centro_custo   = 9999999)
              and (hdoc_060.conta_estoque  = v_cta_estoque
               or  hdoc_060.conta_estoque  = 999);

            if v_bloq_hdoc_060 = 0
            then
               raise_application_error(-20000,'Centro de custo e conta de estoque do produto não estão relacionados nas regras de movimentação.');
            end if;
         end if;
      end if;

      -- Verifica se a empresa trabalha com regras de movimentação Depósito X Unidade Medida
      if :new.tabela_origem = 'ESTQ_300' and :new.codigo_deposito > 0
      then
         select count(*) into v_reg_hdoc_060
         from hdoc_060
         where hdoc_060.codigo_empresa = v_empresa_deposito
           and hdoc_060.tipo_operacao  = 16
           and trim(hdoc_060.unidade_medida) is not null;

         if v_reg_hdoc_060 > 0
         then
            begin
               select basi_030.unidade_medida into v_unidade_medida
               from basi_030
               where basi_030.nivel_estrutura = :new.nivel_estrutura
                 and basi_030.referencia      = :new.grupo_estrutura;
            exception
               when no_data_found then
               raise_application_error(-20000,'Unidade de medida não cadastrada.');
            end;

            select count(*) into v_bloq_hdoc_060
            from hdoc_060
            where  hdoc_060.codigo_empresa  = v_empresa_deposito
              and  hdoc_060.tipo_operacao   = 16
              and (hdoc_060.codigo_deposito = :new.codigo_deposito
               or  hdoc_060.codigo_deposito = 9999)
              and (hdoc_060.unidade_medida  = v_unidade_medida
               or  hdoc_060.unidade_medida  = 'XXX');

            if v_bloq_hdoc_060 = 0
            then
               raise_application_error(-20000,'O Depósito e a unidade de medida do produto não estão relacionados nas regras de movimentação.');
            end if;
         end if;
      end if;

      -- Trunca a data de movimento para que seja considerada apenas a data, sem a hora
      :new.data_movimento := trunc(:new.data_movimento,'dd');

      -- Grava a data/hora da inserção do registro (log)
      :new.data_insercao  := sysdate;

      -- Grava valor total da movimentação
      if :new.tabela_origem = 'OBRF_015'
      then
         if :new.quantidade > 0.00
         then
            :new.valor_total              := :new.valor_movimento_unitario;
            :new.valor_movimento_unitario := :new.valor_movimento_unitario / :new.quantidade;
         else
            :new.valor_total := :new.valor_movimento_unitario;
         end if;
      else
         if :new.quantidade > 0.00
         then
            :new.valor_total := :new.valor_movimento_unitario * :new.quantidade;
         else
            :new.valor_total := :new.valor_movimento_unitario;
         end if;
      end if;

/*    begin
    -- Encontra dados do usu�rio da rede, máquina e aplicativo que esta atualizando a ficha
         select substr(osuser,1,20),  substr(machine,1,40),
                substr(program,1,20)
         into   :new.usuario_rede,    :new.maquina_rede,
                :new.aplicativo
         from sys.gv_$session
         where audsid  = userenv('SESSIONID')
           and inst_id = userenv('INSTANCE')
           and rownum < 2;
      end;
*/
      if (:new.PROCESSO_SYSTEXTIL = 'pcpb_fa01') then
           begin
               select pcpb_015.cod_estagio_agrupador,    pcpb_015.seq_operacao_agrupador,
                      pcpb_015.codigo_estagio
               into   :new.cod_estagio_agrupador,        :new.seq_operacao_agrupador,
                      :new.estagio_op
               from  pcpb_015
               where pcpb_015.ordem_producao  = :new.numero_op
               and  (pcpb_015.data_inicio     is null
               or    pcpb_015.hora_inicio     is null
               or    pcpb_015.data_termino    is null
               or    pcpb_015.hora_termino    is null)
               and   rownum                   = 1
               order by pcpb_015.seq_operacao;
           exception when no_data_found then
               null;
               
               begin
                   select pcpb_015.cod_estagio_agrupador,    pcpb_015.seq_operacao_agrupador,
                          pcpb_015.codigo_estagio
                   into   :new.cod_estagio_agrupador,        :new.seq_operacao_agrupador,
                          :new.estagio_op
                   from  pcpb_015
                   where pcpb_015.ordem_producao  = :new.numero_op
                   and   pcpb_015.seq_operacao in (select max(pcpb_015M.seq_operacao)
                                                   from pcpb_015 pcpb_015M
                                                   where pcpb_015M.ordem_producao = :new.numero_op);
               exception when no_data_found then
                   null;
               end;
           end;
           
           :new.tipo_ordem         := 2;
           :new.ordem_agrupamento  := :new.numero_documento;
           :new.numero_op          := 0;
      end if;
      
      :new.usuario_rede  := ws_usuario_rede;
      :new.maquina_rede  := ws_maquina_rede;
      :new.aplicativo    := ws_aplicativo;

      if (:new.processo_systextil not in ('EQUALIZACAO', 'EQUALIZACAO_TAB', 'ESTQ_F890')
      or  :new.processo_systextil is null or (:new.processo_systextil = 'ESTQ_F890' and :new.sequencia_ficha <> 99999 and :new.codigo_transacao <> 0))
      then

         inter_pr_estoque(:new.nivel_estrutura, :new.grupo_estrutura,  :new.subgrupo_estrutura,
                          :new.item_estrutura,  :new.codigo_transacao, :new.codigo_deposito,
                          :new.numero_lote,     :new.entrada_saida,    :new.data_movimento,
                          :new.quantidade,      :new.valor_movimento_unitario,
                          :new.flag_elimina);
      end if;

      begin
         -- Acha a pr�xima sequencia
         select nvl(max(sequencia_insercao),0) + 1
         into :new.sequencia_insercao
         from estq_300
         where estq_300.codigo_deposito    = :new.codigo_deposito
           and estq_300.nivel_estrutura    = :new.nivel_estrutura
           and estq_300.grupo_estrutura    = :new.grupo_estrutura
           and estq_300.subgrupo_estrutura = :new.subgrupo_estrutura
           and estq_300.item_estrutura     = :new.item_estrutura
           and estq_300.data_movimento     = :new.data_movimento;
      end;

      if :new.processo_systextil is null
      then
      
         v_nome_programa := substr(inter_fn_nome_programa(ws_sid), 1,20);

/*
         begin
            select hdoc_090.programa
            into   v_nome_programa
            from hdoc_090
            where hdoc_090.sid       = v_sid
              and hdoc_090.instancia = userenv('INSTANCE')
              and rownum             = 1
              and hdoc_090.programa not like '%menu%'
              and hdoc_090.programa not like '%!_m%'escape'!'
              and hdoc_090.usuario_rede = v_osuser;
         exception
            when no_data_found then
               v_nome_programa := 'SQL';
         end;
*/

         if v_nome_programa is not null
         then
            :new.processo_systextil := v_nome_programa;
         end if;
      end if;
   end if;

   if deleting
   then
      if :old.flag_elimina = 1
      then

         begin
            select flag_fechamento,   periodo_estoque
            into   v_flag_fechamento, v_periodo_estoque
            from empr_001;
         end;

         begin
            select dtfim_fecha_estq,   flag_fecha_estq
            into   v_dtfim_fecha_estq, v_flag_fecha_estq
            from empr_002;
         end;

         -- Verifica se o lancamento não é de data que esta sendo fechado o estoque
         if (v_dtfim_fecha_estq is null or :new.data_movimento <= v_dtfim_fecha_estq) and v_flag_fecha_estq > 0
         then
            raise_application_error(-20000,'Fechamento de estoque ocorrendo. Data do movimento não pode ser do periodo em fechamento.');
         end if;

         begin
            select tran_inativa
            into v_tran_inativa
            from estq_005
            where estq_005.codigo_transacao = :old.codigo_transacao;
         exception
            when no_data_found then
            raise_application_error(-20000,'Transação não cadastrada.');
         end;

         begin
            select dep_inativo,     data_lim_inativo,
                   local_deposito
            into   v_dep_inativo,   v_data_lim_inativo,
                   v_empresa_deposito
            from basi_205
            where basi_205.codigo_deposito = :old.codigo_deposito;
         exception
            when no_data_found then
            raise_application_error(-20000,'Depósito da empresa não cadastrado.');
         end;

         begin
            select data_pre_fechamento
            into v_data_pre_fech
            from fatu_502
            where fatu_502.codigo_empresa = v_empresa_deposito;
         end;

         -- Verifica se o lancamento não é menor que a data de pré-fechamento do estoque
         if (v_data_pre_fech is null or :new.data_movimento <= v_data_pre_fech)
         then
            raise_application_error(-20000,'Data do movimento não pode ser menor que o periodo de pré-fechamento do estoque.');
         end if;

         -- Verifica se a transacao esta inativa
         if v_tran_inativa = 1
         then
            raise_application_error(-20000,'Esta Transação esta inativa para efetuar movimentações.');
         end if;

         -- Verifica se o deposito esta inativo
         if v_dep_inativo = 1 and (v_data_lim_inativo is null or :old.data_movimento < v_data_lim_inativo)
         then
            raise_application_error(-20000,'Este deposito: ' || to_char(:new.codigo_deposito) || ' esta inativo para efetuar movimentações neste período.');
         end if;

         if (:old.processo_systextil not in ('EQUALIZACAO', 'EQUALIZACAO_TAB', 'ESTQ_F890')
         or  :old.processo_systextil is null or (:old.processo_systextil = 'ESTQ_F890' and :old.sequencia_ficha <> 99999 and :old.codigo_transacao <> 0))
         then
            inter_pr_estoque(:old.nivel_estrutura,     :old.grupo_estrutura,  :old.subgrupo_estrutura,
                             :old.item_estrutura,      :old.codigo_transacao, :old.codigo_deposito,
                             :old.numero_lote,         :old.entrada_saida,    :old.data_movimento,
                             :old.quantidade,          :old.valor_movimento_unitario,
                             :old.flag_elimina);
         end if;
      end if;
   end if;
end;

-- ALTER TRIGGER INTER_TR_ESTQ_300 ENABLE


/

exec inter_pr_recompile;
