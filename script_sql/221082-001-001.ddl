insert into hdoc_035
      (codigo_programa, descricao, programa_menu, item_menu_def)
select 'inte_p137', descricao, programa_menu, 0 
from hdoc_035
where codigo_programa = 'inte_p037';

insert into hdoc_033
      (usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
select usu_prg_cdusu, usu_prg_empr_usu, 'inte_p137', nome_menu, 0, ordem_menu, incluir, modificar, excluir, procurar 
from hdoc_033
where programa = 'inte_p037';

update hdoc_036
set hdoc_036.descricao       = 'Conferencia de Volumes'
where hdoc_036.codigo_programa = 'inte_p137'
and hdoc_036.locale          = 'es_ES';

commit;
