
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_021_LOG" 
   after insert or delete or update
       of  consumo_componente
   on basi_021
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_cod_tipo_produto_n     varchar2(60);

   ws_tipo_produto_n         varchar2(60);

   ws_desc_projeto_n         varchar2(60);

   ws_nivel                  basi_013.nivel_comp%type;
   ws_grupo                  basi_013.grupo_comp%type;
   ws_subgru                 basi_013.subgru_comp%type;
   ws_item                   basi_013.item_comp%type;

   ws_desc_prod_n            varchar2(120);
   ws_desc_comp_o            varchar2(120);
   ws_desc_comp_n            varchar2(120);
   ws_desc_grupo_n           varchar2(30);
   ws_desc_subgrupo_n        varchar2(30);
   ws_desc_item_n            varchar2(30);

   ws_cnpj_cliente9          basi_001.cnpj_cliente9%type;
   ws_cnpj_cliente4          basi_001.cnpj_cliente4%type;
   ws_cnpj_cliente2          basi_001.cnpj_cliente2%type;

   ws_teve                   varchar2(1);

   long_aux                  varchar2(2000);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if updating
   then
      -- ENCONTRA O TIPO_PRODUTO DA SEQUENCIA
      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = :new.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_n := '';
      end;

      -- DESCRICAO DO TIPO DE PRODUTO
      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n;
      exception when no_data_found then
         ws_tipo_produto_n := '';
      end;

     -- DESCRICAO DO PROJETO
      begin
         select basi_001.descricao_projeto,  basi_001.cnpj_cliente9,
                basi_001.cnpj_cliente4,      basi_001.cnpj_cliente2
         into ws_desc_projeto_n,             ws_cnpj_cliente9,
              ws_cnpj_cliente4,              ws_cnpj_cliente2
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_n := '';
         ws_cnpj_cliente9  := 0;
         ws_cnpj_cliente4  := 0;
         ws_cnpj_cliente2  := 0;
      end;

      -- DESCRICAO DO NOVO COMPONENTE - OLD
      begin
         select basi_013.nivel_comp,  basi_013.grupo_comp,
                basi_013.subgru_comp, basi_013.item_comp
         into   ws_nivel,             ws_grupo,
                ws_subgru,            ws_item
         from basi_013
         where basi_013.codigo_projeto       = :old.codigo_projeto
           and basi_013.sequencia_projeto    = :old.sequencia_projeto
           and basi_013.alternativa_produto  = :old.alternativa_produto
           and basi_013.nivel_item           = :old.nivel_item
           and basi_013.grupo_item           = :old.grupo_item
           and (basi_013.subgru_item         = :old.subgru_item
            or basi_013.subgru_item          = '000')
           and (basi_013.item_item           = :old.item_item
            or  basi_013.item_item           = '000000')
           and basi_013.sequencia_estrutura  = :old.sequencia_estrutura
           and basi_013.sequencia_variacao   = :old.sequencia_variacao;
      exception when no_data_found then
         ws_nivel    := 'N';
         ws_grupo    := 'N';
         ws_subgru   := ' ';
         ws_item     := ' ';
      end;

      begin
         select nvl(basi_030.descr_referencia,' ')
         into ws_desc_grupo_n
         from basi_030
         where basi_030.nivel_estrutura = ws_nivel
           and basi_030.referencia      = ws_grupo;
         exception when no_data_found then
            ws_desc_grupo_n := ' ';
      end;

      begin
         select nvl(basi_020.descr_tam_refer,' ')
         into ws_desc_subgrupo_n
         from basi_020
         where basi_020.basi030_nivel030 =  ws_nivel
           and basi_020.basi030_referenc =  ws_grupo
           and basi_020.tamanho_ref      =  ws_subgru;
         exception when no_data_found then
            ws_desc_subgrupo_n := ' ';
      end;

      begin
         select nvl(basi_010.descricao_15,' ')
         into ws_desc_item_n
         from basi_010
         where basi_010.nivel_estrutura  =  ws_nivel
           and basi_010.grupo_estrutura  =  ws_grupo
           and basi_010.subgru_estrutura =  ws_subgru
           and basi_010.item_estrutura   =  ws_item;
         exception when no_data_found then
            ws_desc_item_n := ' ';
      end;

      ws_desc_comp_o := ws_desc_grupo_n || ' ' || ws_desc_subgrupo_n  || ' ' || ws_desc_item_n;

      -- DESCRICAO DO NOVO COMPONENTE - NOVO
      begin
         select basi_013.nivel_comp,  basi_013.grupo_comp,
                basi_013.subgru_comp, basi_013.item_comp
         into   ws_nivel,             ws_grupo,
                ws_subgru,            ws_item
         from basi_013
         where basi_013.codigo_projeto       = :new.codigo_projeto
           and basi_013.sequencia_projeto    = :new.sequencia_projeto
           and basi_013.alternativa_produto  = :new.alternativa_produto
           and basi_013.nivel_item           = :new.nivel_item
           and basi_013.grupo_item           = :new.grupo_item
           and (basi_013.subgru_item         = :new.subgru_item
            or basi_013.subgru_item          = '000')
           and (basi_013.item_item           = :new.item_item
            or  basi_013.item_item           = '000000')
           and basi_013.sequencia_estrutura  = :new.sequencia_estrutura
           and basi_013.sequencia_variacao   = :new.sequencia_variacao;
         exception when no_data_found then
            ws_nivel    := ' ';
            ws_grupo    := ' ';
            ws_subgru   := ' ';
            ws_item     := ' ';
      end;

      begin
         select nvl(basi_030.descr_referencia,' ')
         into ws_desc_grupo_n
         from basi_030
         where basi_030.nivel_estrutura =  ws_nivel
           and basi_030.referencia      =  ws_grupo;
         exception when no_data_found then
            ws_desc_grupo_n := ' ';
      end;

      begin
         select nvl(basi_020.descr_tam_refer,' ')
         into ws_desc_subgrupo_n
         from basi_020
         where basi_020.basi030_nivel030 = ws_nivel
           and basi_020.basi030_referenc = ws_grupo
           and basi_020.tamanho_ref      = ws_subgru;
         exception when no_data_found then
            ws_desc_subgrupo_n := ' ';
      end;

      begin
         select nvl(basi_010.descricao_15,' ')
         into ws_desc_item_n
         from basi_010
         where basi_010.nivel_estrutura  = ws_nivel
           and basi_010.grupo_estrutura  = ws_grupo
           and basi_010.subgru_estrutura = ws_subgru
           and basi_010.item_estrutura   = ws_item;
         exception when no_data_found then
            ws_desc_item_n := ' ';
      end;

      ws_desc_comp_n := ws_desc_grupo_n || ' ' || ws_desc_subgrupo_n  || ' ' || ws_desc_item_n;

      -- DESCRICAO DO NOVO COMPONENTE - PRODUTO NIVEL 1
      begin
         select nvl(basi_030.descr_referencia,' ')
         into ws_desc_grupo_n
         from basi_030
         where basi_030.nivel_estrutura =  :new.nivel_item
           and basi_030.referencia      =  :new.grupo_item;
         exception when no_data_found then
            ws_desc_grupo_n := ' ';
      end;

      begin
         select nvl(basi_020.descr_tam_refer,' ')
         into ws_desc_subgrupo_n
         from basi_020
         where basi_020.basi030_nivel030 = :new.nivel_item
           and basi_020.basi030_referenc = :new.grupo_item
           and basi_020.tamanho_ref      = :new.subgru_item;
         exception when no_data_found then
            ws_desc_subgrupo_n := ' ';
      end;

      begin
         select nvl(basi_010.descricao_15,' ')
         into ws_desc_item_n
         from basi_010
         where basi_010.nivel_estrutura  = :new.nivel_item
           and basi_010.grupo_estrutura  = :new.grupo_item
           and basi_010.subgru_estrutura = :new.subgru_item
           and basi_010.item_estrutura   = :new.item_item;
         exception when no_data_found then
            ws_desc_item_n := ' ';
      end;

      ws_desc_prod_n := ws_desc_grupo_n || ' ' || ws_desc_subgrupo_n  || ' ' || ws_desc_item_n;

      ws_teve := 'n';

      long_aux := long_aux ||
           inter_fn_buscar_tag('lb34894#ESTRUTURA DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) || '(021)' ||
           chr(10)                                               ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.codigo_projeto    ||
           ' - '                       || ws_desc_projeto_n      ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb08486#TIPO DE PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_projeto ||
           ' - '                       || ws_cod_tipo_produto_n  ||
           ' - '                       || ws_tipo_produto_n      ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.nivel_item        ||
           '.'                         || :new.grupo_item        ||
           '.'                         || :new.subgru_item       ||
           '.'                         || :new.item_item         ||
           ' - '                       || ws_desc_prod_n         ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb04571#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.alternativa_produto,'00')   ||
           '-> '                       || to_char(:new.alternativa_produto,'00')   ||
           chr(10)                                               ||
           chr(10);

      if (:old.subgru_comp <> :new.subgru_comp) or
         (:old.item_comp   <> :new.item_comp)
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00523#COMPONENTE::',ws_locale_usuario,ws_usuario_systextil) || ' '
                                     || ws_nivel               ||
         '.'                         || ws_grupo               ||
         '.'                         || :old.subgru_comp       ||
         '.'                         || :old.item_comp         ||
         ' - '                       || ws_desc_comp_o         ||
         '-> '                       || ws_nivel               ||
         '.'                         || ws_grupo               ||
         '.'                         || :new.subgru_comp       ||
         '.'                         || :new.item_comp         ||
         ' - '                       || ws_desc_comp_n         ||
         chr(10);
      end if;

      if :old.consumo_componente <> :new.consumo_componente
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00064#CONSUMO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                     || :old.consumo_componente  ||
         ' -> '                      || :new.consumo_componente  ||
         chr(10);
      end if;

      if :old.metros_componente <> :new.metros_componente
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb34962#METROS CONSUMO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                     || :old.metros_componente ||
         ' -> '                      || :new.metros_componente ||
         chr(10);
      end if;

      if ws_teve = 's'
      then
         INSERT INTO hist_100
            ( tabela,              operacao,
              data_ocorr,          aplicacao,
              usuario_rede,        maquina_rede,
              str01,               num05,
              str07,               str08,
              str09,               str10,
              num07,               num08,
              num09,               long01
            )
         VALUES
            ( 'BASI_013',          'A',
              sysdate,             ws_aplicativo,
              ws_usuario_rede,     ws_maquina_rede,
              :new.codigo_projeto, :new.sequencia_projeto,
              ws_nivel,            ws_grupo,
              :new.subgru_comp,    :new.item_comp,
              ws_cnpj_cliente9,    ws_cnpj_cliente4,
              ws_cnpj_cliente2,    long_aux
           );
      end if;
   end if;

   if deleting
   then
        -- DESCRICAO DO PROJETO
      begin
         select basi_001.cnpj_cliente9,
                basi_001.cnpj_cliente4,      basi_001.cnpj_cliente2
         into ws_cnpj_cliente9,
              ws_cnpj_cliente4,              ws_cnpj_cliente2
         from basi_001
         where basi_001.codigo_projeto = :old.codigo_projeto;
      exception when no_data_found then
         ws_cnpj_cliente9  := 0;
         ws_cnpj_cliente4  := 0;
         ws_cnpj_cliente2  := 0;
      end;

      -- DESCRICAO DO NOVO COMPONENTE - OLD
      begin
         select basi_013.nivel_comp,  basi_013.grupo_comp,
                basi_013.subgru_comp, basi_013.item_comp
         into   ws_nivel,             ws_grupo,
                ws_subgru,            ws_item
         from basi_013
         where basi_013.codigo_projeto       = :old.codigo_projeto
           and basi_013.sequencia_projeto    = :old.sequencia_projeto
           and basi_013.alternativa_produto  = :old.alternativa_produto
           and basi_013.nivel_item           = :old.nivel_item
           and basi_013.grupo_item           = :old.grupo_item
           and basi_013.subgru_item          = :old.subgru_item
           and basi_013.item_item            = :old.item_item
           and basi_013.sequencia_estrutura  = :old.sequencia_estrutura
           and basi_013.sequencia_variacao   = :old.sequencia_variacao;
         exception when no_data_found then
            ws_nivel    := ' ';
            ws_grupo    := ' ';
      end;


      INSERT INTO hist_100
         ( tabela,              operacao,
           data_ocorr,          aplicacao,
           usuario_rede,        maquina_rede,
           str01,               num05,
           str07,               str08,
           str09,               str10,
           num07,               num08,
           num09
         )
      VALUES
         ( 'BASI_013',          'D',
           sysdate,             ws_aplicativo,
           ws_usuario_rede,     ws_maquina_rede,
           :old.codigo_projeto, :old.sequencia_projeto,
           ws_nivel,            ws_grupo,
           :old.subgru_comp,    :old.item_comp,
           ws_cnpj_cliente9,    ws_cnpj_cliente4,
           ws_cnpj_cliente2
         );
   end if;
end inter_tr_basi_021_log;

-- ALTER TRIGGER "INTER_TR_BASI_021_LOG" ENABLE
 

/

exec inter_pr_recompile;

