
  CREATE OR REPLACE TRIGGER "INTER_TR_LOJA_210" 

  before insert or
         delete or
         update of flag_cancelamento, nivel, grupo, sub, item, lote, deposito, qtde, numero_volume on loja_210
  for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_dep_kg_rolo          number(1);
   v_deposito             basi_205.codigo_deposito%type;
   v_entrada_saida        estq_005.entrada_saida%type;
   v_rolo                 loja_210.numero_volume%type;
   v_saldo                estq_330.peso_saldo%type;
   v_transacao            estq_005.codigo_transacao%type;
   v_obser                estq_330.observacao%type;
   v_atualiza_estoque     estq_005.atualiza_estoque%type;
   v_ult_seq              estq_330.seq_mov%type;
   v_rolo_ativo           varchar2(1);
   v_vendedor             loja_200.vendedor%type;
   v_peso_orig            estq_330.peso_orig%type;
   v_peso_mov             estq_330.peso_mov%type;
   v_menor_seq            estq_330.seq_mov%type;
   v_ativo                estq_330.ativo%type;
   v_empresa              estq_330.empresa%type;
   v_docto                estq_330.documento%type;
   v_seq_doc              estq_330.seq_doc%type;
   v_existe               number(1);
   v_seq_mov              estq_330.seq_mov%type;
   v_pronta_entrega       basi_205.pronta_entrega%type;
   v_empenha              estq_040.qtde_empenhada%type;
   v_niv                  estq_040.cditem_nivel99%type;
   v_gru                  estq_040.cditem_grupo%type;
   v_sub                  estq_040.cditem_subgrupo%type;
   v_ite                  estq_040.cditem_item%type;
   v_lot                  loja_210.lote%type;
   v_transacaoi           estq_005.codigo_transacao%type;
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ------------------------------------------------------
   --             alimenta as variaveis                --
   ------------------------------------------------------
   if inserting or updating
   then
      if :new.qtde = 0
      then
         raise_application_error(-20000,inter_fn_buscar_tag('ds26573#ATENCAO! Deve-se obrigatoriamente informar uma quantidade.',ws_locale_usuario,ws_usuario_systextil));
      end if;

      v_empresa              := :new.cod_empresa;
      v_docto                := :new.documento;
      v_seq_doc              := :new.sequencia;
      v_deposito             := :new.deposito;
      v_rolo                 := :new.numero_volume;
      v_transacao            := :new.transacao;
      v_peso_mov             := :new.qtde;
      v_niv                  := :new.nivel;
      v_gru                  := :new.grupo;
      v_sub                  := :new.sub;
      v_ite                  := :new.item;
      v_lot                  := :new.lote;

      if :new.venda_troca = 'T'
      then
         begin
            select fatu_502.transacao_ent_troca, estq_005.entrada_saida
            into   v_transacao,                  v_entrada_saida
            from fatu_502, estq_005
            where fatu_502.transacao_ent_troca  = estq_005.codigo_transacao
              and fatu_502.codigo_empresa       = v_empresa
              and fatu_502.transacao_ent_troca <> 0;

            exception when no_data_found
            then raise_application_error(-20000,inter_fn_buscar_tag('ds26574#ATENCAO! A Transacao de troca nao esta configurada nos parametros de loja da empresa deste orcamento.',ws_locale_usuario,ws_usuario_systextil));

            if v_entrada_saida <> 'E'
            then
               raise_application_error(-20000,inter_fn_buscar_tag('ds26575#ATENCAO! A Transacao de troca desta empresa nao esta parametrizada como uma entrada.',ws_locale_usuario,ws_usuario_systextil));
            end if;
         end;

         if :old.flag_cancelamento = 2 and :new.flag_cancelamento = 0
         then
            begin
               v_transacaoi := v_transacao;

               select estq_005.trans_cancelamento
               into   v_transacao
               from estq_005
               where codigo_transacao = v_transacaoi;

               exception when no_data_found
               then raise_application_error(-20000,inter_fn_buscar_tag('ds26576#ATENCAO! Transacao de cancelamento nao cadastrada para a transacao:',ws_locale_usuario,ws_usuario_systextil)|| to_char(v_transacaoi,'999'));
            end;
         end if;

      end if;
   end if;

   if deleting or (:old.flag_cancelamento = 0 and :new.flag_cancelamento = 1) -- Deletando ou cancelando o orcamento
   then
      v_empresa              := :old.cod_empresa;
      v_docto                := :old.documento;
      v_seq_doc              := :old.sequencia;
      v_deposito             := :old.deposito;
      v_rolo                 := :old.numero_volume;
      v_peso_mov             := :old.qtde;
      v_niv                  := :old.nivel;
      v_gru                  := :old.grupo;
      v_sub                  := :old.sub;
      v_ite                  := :old.item;
      v_lot                  := :old.lote;

--      begin
--         select estq_005.trans_cancelamento
--         into   v_transacao
--         from estq_005
--         where codigo_transacao = v_transacaoi;
--
--         exception when no_data_found
--         then raise_application_error(-20000,'Transacao de cancelamento nao cadastrada para a transacao: ' || to_char(v_transacaoi,'999'));
--      end;

   end if;

   -------------------------------------------------------------
   -- Verifica se o deposito e do tipo KG controlado por ROLO --
   -------------------------------------------------------------
   select nvl(basi_205.dep_kg_rolo,0), nvl(basi_205.pronta_entrega,0)
     into v_dep_kg_rolo,               v_pronta_entrega
   from basi_205
   where basi_205.codigo_deposito = v_deposito;

   if   v_dep_kg_rolo = 1
   and (v_niv = '2' or v_niv = '4')
   then
      if deleting or (:old.flag_cancelamento = 0 and :new.flag_cancelamento = 1) -- Deletando ou cancelando o orcamento
      then
         begin
            select min(estq_330.seq_mov) into v_menor_seq from estq_330
            where estq_330.rolo      = v_rolo
              and estq_330.empresa   = v_empresa
              and estq_330.e_s       = 'E';

            select estq_330.transacao into v_transacao from estq_330
            where estq_330.rolo      = v_rolo
              and estq_330.seq_mov   = v_menor_seq;

            exception when no_data_found
            then raise_application_error(-20000,inter_fn_buscar_tag('ds26577#ATENCAO! Transacao de inclusao do rolo neste orcamento nao foi encontrada.',ws_locale_usuario,ws_usuario_systextil));
         end;
      end if;

      begin
         select max(estq_330.seq_mov) into v_ult_seq from estq_330
         where estq_330.rolo    = v_rolo;

         select estq_330.ativo,     estq_330.peso_saldo,
                estq_330.peso_orig, estq_330.ativo
         into   v_rolo_ativo,       v_saldo,
                v_peso_orig,        v_ativo
         from estq_330
         where estq_330.rolo     = v_rolo
           and estq_330.empresa  = v_empresa
           and estq_330.deposito = v_deposito
           and estq_330.seq_mov  = v_ult_seq;

         exception when no_data_found
         then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26578', to_char(v_rolo,'999999999') , to_char(v_empresa,'999') , to_char(v_deposito,'999') , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil));
      end;

      if v_transacao = 0
      then
         raise_application_error(-20000,inter_fn_buscar_tag('ds26579#ATENCAO! Quando o deposito for de KG, deve ser informada uma transacao de estoque.',ws_locale_usuario,ws_usuario_systextil));
      end if;

      if  v_rolo_ativo     <> 'S' -- Rolo Inativo
      and :new.venda_troca <> 'T' -- E nao e uma troca, pois na troca pode-se reativar um rolo.
      and (inserting or (updating and :old.flag_cancelamento = 0 and :new.flag_cancelamento = 0))
      then
         raise_application_error(-20000,inter_fn_buscar_tag('ds26580#ATENCAO! Este rolo nao esta mais ativo.',ws_locale_usuario,ws_usuario_systextil));
      end if;

      begin
         select 1 into v_existe from pcpt_020
         where pcpt_020.codigo_rolo  = v_rolo
           and pcpt_020.rolo_estoque = 2;

         exception when no_data_found
         then raise_application_error(-20000,inter_fn_buscar_tag('ds26581#ATENCAO! O rolo informado nao passou por um deposito de volumes.' || to_char(v_rolo,'999999999'),ws_locale_usuario,ws_usuario_systextil));
      end;

      ------------------------------------------------------------------------------------
      -- Campos que nao devem ser alterados em depositos de quilo controlado por volume --
      ------------------------------------------------------------------------------------
      if updating
      then
           if :old.deposito <> :new.deposito
         then
            raise_application_error(-20000,inter_fn_buscar_tag('ds26582#ATENCAO! O deposito nao pode ser alterado no orcamento. Este e um deposito de quilo controlado por volume.',ws_locale_usuario,ws_usuario_systextil));
         end if;

           if :old.numero_volume <> :new.numero_volume
         then
            raise_application_error(-20000,inter_fn_buscar_tag('ds26583#ATENCAO! O numero do volume nao pode ser alterado no orcamento. Este e um deposito de quilo controlado por volume.',ws_locale_usuario,ws_usuario_systextil));
         end if;

           if :old.lote <> :new.lote
         then
            raise_application_error(-20000,inter_fn_buscar_tag('ds26584#ATENCAO! O lote nao pode ser alterado. Este e um deposito de quilo controlado por volume.',ws_locale_usuario,ws_usuario_systextil));
         end if;

         if :old.nivel <> :new.nivel
         or :old.grupo <> :new.grupo
         or :old.sub   <> :new.sub
         or :old.item  <> :new.item
         then
            raise_application_error(-20000,inter_fn_buscar_tag('ds26585#ATENCAO! O produto nao pode ser alterado. Este e um deposito de quilo controlado por volume.',ws_locale_usuario,ws_usuario_systextil));
         end if;

      end if;

      -- verifica se a transacao atualiza estoque
      begin
         select atualiza_estoque,   entrada_saida
         into   v_atualiza_estoque, v_entrada_saida
         from estq_005
         where codigo_transacao = v_transacao;

         exception when no_data_found
         then raise_application_error(-20000,inter_fn_buscar_tag('ds08939#ATENCAO! Transacao nao cadastrada.',ws_locale_usuario,ws_usuario_systextil));

      end;

      if v_atualiza_estoque = 1
      then

         if (:old.flag_cancelamento = 2 and :new.flag_cancelamento = 0 and :new.venda_troca <> 'T') -- Re-empenha
         or (:old.flag_cancelamento = 0 and :new.flag_cancelamento = 2 and :new.venda_troca <> 'T') -- Tira empenho pq esta faturando
         then
            if v_pronta_entrega = 1
            then
               begin

                  v_empenha := v_peso_mov;

                  if (:old.flag_cancelamento = 0 and :new.flag_cancelamento = 2)
                  then
                     v_empenha       := v_peso_mov * -1;
                  end if;

-- v_mostra := v_niv || '.' || v_gru || '.' || v_sub || '.' || v_ite || ' Lote: ' || to_char(v_lot) || ' Dep: ' || to_char(v_deposito) || ' Empenha: ' || to_char(v_empenha);
-- raise_application_error(-20000, v_mostra);
                  update estq_040
                  set qtde_empenhada = qtde_empenhada + v_empenha
                  where cditem_nivel99  = v_niv
                  and   cditem_grupo    = v_gru
                  and   cditem_subgrupo = v_sub
                  and   cditem_item     = v_ite
                  and   lote_acomp      = v_lot
                  and   deposito        = v_deposito;

                  if SQL%notfound
                  then
                     insert into estq_040
                       (cditem_nivel99,       cditem_grupo,
                        cditem_subgrupo,      cditem_item,
                        lote_acomp,           deposito,
                        qtde_empenhada)
                     values
                       (v_niv,                v_gru,
                        v_sub,                v_ite,
                        v_lot,                v_deposito,
                        v_empenha);
                  end if;

                  exception when others
                  then raise_application_error(-20000, SQLERRM);

               end;
            end if;
         else
            if  :new.venda_troca = 'T'
            and updating
            and ((:old.flag_cancelamento = 0 and :new.flag_cancelamento = 2) or (:old.flag_cancelamento = 2 and :new.flag_cancelamento = 0))
            then
               if updating
               and (:new.qtde          <> :old.qtde
               or   :new.nivel         <> :old.nivel
               or   :new.grupo         <> :old.grupo
               or   :new.sub           <> :old.sub
               or   :new.item          <> :old.item
               or   :new.deposito      <> :old.deposito
               or   :new.numero_volume <> :old.numero_volume
               or   :new.documento     <> :old.documento
               or   :new.cod_empresa   <> :old.cod_empresa)
               then
                  raise_application_error(-20000,inter_fn_buscar_tag('ds26586#ATENCAO! Este item do orcamento e de um deposito de KG?. Voce nao pode alterar informacoes de estoque.',ws_locale_usuario,ws_usuario_systextil));
               end if;

               begin
                  select loja_200.vendedor into v_vendedor from loja_200
                  where loja_200.cod_empresa = v_empresa
                    and loja_200.documento   = v_docto;

                  exception when no_data_found
                  then raise_application_error(-20000,inter_fn_buscar_tag('ds06106#ATENCAO! Este orcamento nao existe.'||to_char(v_empresa,'999')||'-'||to_char(v_docto,'999999'),ws_locale_usuario,ws_usuario_systextil));
               end;

               -----------------------------------------------------------------------------------------
               --                 Busca a maior sequencia da empresa/orcamento/rolo                   --
               -----------------------------------------------------------------------------------------
               begin
                  select nvl(max(estq_330.seq_mov),0) into v_seq_mov from estq_330
                  where estq_330.rolo = v_rolo;

                  v_seq_mov := v_seq_mov + 1;
               end;

               -- FATURAMENTO --
               if  updating
               and :old.flag_cancelamento = 0
               and :new.flag_cancelamento = 2
               and v_entrada_saida        = 'E'
               then
                  v_entrada_saida := 'E';

                  if v_rolo_ativo = 'N'
                  then
                     v_saldo := v_peso_mov;
                  else
                     v_saldo := v_saldo + v_peso_mov;
                  end if;

                  v_ativo         := 'S';
                  v_obser         := inter_fn_buscar_tag('lb34844#ENTRADA POR TROCA/DEVOLUCAO',ws_locale_usuario,ws_usuario_systextil);
               end if;

               -- CANCELAMENTO DA NF --
               if  updating
               and :old.flag_cancelamento = 2
               and :new.flag_cancelamento = 0
               and v_entrada_saida        = 'S'
               then
                  v_entrada_saida := 'S';
                  v_saldo         := v_saldo - v_peso_mov;
                  v_ativo         := 'S';
                  v_obser         := inter_fn_buscar_tag('ds26587#CANCELAMENTO DA TROCA/DEVOLUCAO',ws_locale_usuario,ws_usuario_systextil);
               end if;

               begin
                  insert into estq_330
                     (rolo,                seq_mov,
                      empresa,             documento,
                      seq_doc,             vendedor,
                      peso_orig,           deposito,
                      transacao,           e_s,
                      peso_mov,            peso_saldo,
                      data_movto,          observacao,
                      usuario,             data_ocorr,
                      maquina,             programa,
                      executavel,          ativo)
                     VALUES
                     (v_rolo,              v_seq_mov,
                      v_empresa,           v_docto,
                      v_seq_doc,           v_vendedor,
                      v_peso_orig,         v_deposito,
                      v_transacao,         v_entrada_saida,
                      v_peso_mov,          v_saldo,
                      trunc(sysdate),      v_obser,
                      ws_usuario_rede,      sysdate,
                      ws_maquina_rede,      'ORCAMENTO',
                      ws_aplicativo,         v_ativo);

                  exception when others
                  then raise_application_error(-20000, SQLERRM);
               end;
            else
               if :new.venda_troca = 'V'
               or (deleting and :old.venda_troca = 'V')
               then
                  if  (inserting or updating)
                  and (:old.flag_cancelamento <> 0 or :new.flag_cancelamento <> 1)
                  and v_entrada_saida <> 'S'
                  then
                     raise_application_error(-20000,inter_fn_buscar_tag('ds26588#ATENCAO! Somente pode utilizar transacoes de saida em uma venda.', ws_locale_usuario,ws_usuario_systextil) || v_entrada_saida);
                  end if;

                  -----------------------------------------------------------------------------------------
                  -- Busca a maior sequencia da empresa/orcamento/rolo para saber o saldo, se esta ativo --
                  -----------------------------------------------------------------------------------------
                  begin
                     select nvl(max(estq_330.seq_mov),0) into v_seq_mov from estq_330
                     where estq_330.rolo = v_rolo;

                     v_seq_mov := v_seq_mov + 1;
                  end;

                  if v_entrada_saida = 'T' or v_entrada_saida = 'S'
                  then
                     v_entrada_saida := 'S';
                     v_saldo         := v_saldo - v_peso_mov;
                  else
                     v_entrada_saida := 'E';
                     v_saldo         := v_saldo + v_peso_mov;
                  end if;

                  if updating
                  and (:new.qtde          <> :old.qtde
                   or  :new.nivel         <> :old.nivel
                   or  :new.grupo         <> :old.grupo
                   or  :new.sub           <> :old.sub
                   or  :new.item          <> :old.item
                   or  :new.deposito      <> :old.deposito
                   or  :new.numero_volume <> :old.numero_volume
                   or  :new.documento     <> :old.documento
                   or  :new.cod_empresa   <> :old.cod_empresa)
                  then
                     raise_application_error(-20000,inter_fn_buscar_tag('ds26586#ATENCAO! Este item do orcamento e de um deposito de KG?. Voce nao pode alterar informacoes de estoque.',ws_locale_usuario,ws_usuario_systextil));
                  end if;

                  if inserting or deleting
                  or (:old.flag_cancelamento = 0 and :new.flag_cancelamento = 1)
                  or (:old.flag_cancelamento = 1 and :new.flag_cancelamento = 0)
                  then
                     begin
                        select loja_200.vendedor into v_vendedor from loja_200
                        where loja_200.cod_empresa = v_empresa
                          and loja_200.documento   = v_docto;

                        exception when no_data_found
                        then raise_application_error(-20000,inter_fn_buscar_tag('ds06106#ATENCAO! Este orcamento nao existe.'||to_char(v_empresa,'999')||'-'||to_char(v_docto,'999999'),ws_locale_usuario,ws_usuario_systextil));

                     end;

                     v_obser   := inter_fn_buscar_tag('lb11175#VENDA', ws_locale_usuario,ws_usuario_systextil);
                     v_empenha := v_peso_mov;

                     if (:old.flag_cancelamento = 1 and :new.flag_cancelamento = 0)
                     then
                        v_obser         := inter_fn_buscar_tag('lb34845#REATIV.ORC.', ws_locale_usuario,ws_usuario_systextil);
                        v_ativo         := 'S';
                     end if;

                     if (:old.flag_cancelamento = 0 and :new.flag_cancelamento = 1)
                     then
                        v_obser         := inter_fn_buscar_tag('lb34846#CANC. ORCAM.', ws_locale_usuario,ws_usuario_systextil);
                        v_ativo         := 'S';
                        v_entrada_saida := 'E';
 --                     v_transacao     := 0;
                        v_empenha       := v_peso_mov * -1;
                     end if;

                     if deleting
                     then
                        v_obser         := inter_fn_buscar_tag('lb34847#DELETE ITEM ORC.', ws_locale_usuario,ws_usuario_systextil);
                        v_empenha       := v_peso_mov * -1;
                     end if;

                     begin
                        insert into estq_330
                        (rolo,                seq_mov,
                         empresa,             documento,
                         seq_doc,             vendedor,
                         peso_orig,           deposito,
                         transacao,           e_s,
                         peso_mov,            peso_saldo,
                         data_movto,          observacao,
                         usuario,             data_ocorr,
                         maquina,             programa,
                         executavel,          ativo)
                        VALUES
                        (v_rolo,              v_seq_mov,
                         v_empresa,           v_docto,
                         v_seq_doc,           v_vendedor,
                         v_peso_orig,         v_deposito,
                         v_transacao,         v_entrada_saida,
                         v_peso_mov,          v_saldo,
                         trunc(sysdate),      v_obser,
                         ws_usuario_rede,     sysdate,
                         ws_maquina_rede,     'ORCAMENTO',
                         ws_aplicativo,       v_ativo);

                        exception when others
                        then raise_application_error(-20000, SQLERRM);
                     end;

                     if v_pronta_entrega = 1
                     then

                        begin
                           update estq_040
                           set qtde_empenhada = qtde_empenhada + v_empenha
                           where cditem_nivel99  = v_niv
                           and   cditem_grupo    = v_gru
                           and   cditem_subgrupo = v_sub
                           and   cditem_item     = v_ite
                           and   lote_acomp      = v_lot
                           and   deposito        = v_deposito;

                           if SQL%notfound
                           then
                              insert into estq_040
                                (cditem_nivel99,       cditem_grupo,
                                 cditem_subgrupo,      cditem_item,
                                 lote_acomp,           deposito,
                                 qtde_empenhada)
                              values
                                (v_niv,                v_gru,
                                 v_sub,                v_ite,
                                 v_lot,                v_deposito,
                                 v_empenha);
                           end if;

                           exception when others
                           then raise_application_error(-20000, SQLERRM);

                        end;
                     end if;
                  end if;
               end if;
            end if;
         end if;
      end if;
   end if;
end;

-- ALTER TRIGGER "INTER_TR_LOJA_210" ENABLE
 

/

exec inter_pr_recompile;

