CREATE OR REPLACE TRIGGER "INTER_TR_LOJA_008_LOG" 
after insert or delete or update
on loja_008
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


   v_nome_programa := inter_fn_nome_programa(ws_sid);  


 if inserting
 then
    begin

        insert into loja_008_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           empresa_OLD,   /*8*/
           empresa_NEW,   /*9*/
           pedido_loja_OLD,   /*10*/
           pedido_loja_NEW,   /*11*/
           nr_fardo_OLD,   /*12*/
           nr_fardo_NEW,   /*13*/
           situacao_OLD,   /*14*/
           situacao_NEW,   /*15*/
           data_montagem_OLD,   /*16*/
           data_montagem_NEW    /*17*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.empresa, /*9*/
           0,/*10*/
           :new.pedido_loja, /*11*/
           '',/*12*/
           :new.nr_fardo, /*13*/
           0,/*14*/
           :new.situacao, /*15*/
           null,/*16*/
           :new.data_montagem /*17*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into loja_008_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           empresa_OLD, /*8*/
           empresa_NEW, /*9*/
           pedido_loja_OLD, /*10*/
           pedido_loja_NEW, /*11*/
           nr_fardo_OLD, /*12*/
           nr_fardo_NEW, /*13*/
           situacao_OLD, /*14*/
           situacao_NEW, /*15*/
           data_montagem_OLD, /*16*/
           data_montagem_NEW  /*17*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.empresa,  /*8*/
           :new.empresa, /*9*/
           :old.pedido_loja,  /*10*/
           :new.pedido_loja, /*11*/
           :old.nr_fardo,  /*12*/
           :new.nr_fardo, /*13*/
           :old.situacao,  /*14*/
           :new.situacao, /*15*/
           :old.data_montagem,  /*16*/
           :new.data_montagem  /*17*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into loja_008_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           empresa_OLD, /*8*/
           empresa_NEW, /*9*/
           pedido_loja_OLD, /*10*/
           pedido_loja_NEW, /*11*/
           nr_fardo_OLD, /*12*/
           nr_fardo_NEW, /*13*/
           situacao_OLD, /*14*/
           situacao_NEW, /*15*/
           data_montagem_OLD, /*16*/
           data_montagem_NEW /*17*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.empresa, /*8*/
           0, /*9*/
           :old.pedido_loja, /*10*/
           0, /*11*/
           :old.nr_fardo, /*12*/
           '', /*13*/
           :old.situacao, /*14*/
           0, /*15*/
           :old.data_montagem, /*16*/
           null /*17*/
         );
    end;
 end if;
end inter_tr_loja_008_log;
-- ALTER TRIGGER "INTER_TR_LOJA_008_LOG" ENABLE
 

/

exec inter_pr_recompile;

