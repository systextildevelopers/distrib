create or replace view  VW_CREC_TITULOS_LIQUIDADOS as
    select
        fatu_070.codigo_empresa
        ,fatu_070.cli_dup_cgc_cli9
        ,fatu_070.cli_dup_cgc_cli4
        ,fatu_070.cli_dup_cgc_cli2
        ,pedi_010.nome_cliente
        ,pedi_010.cod_cliente
        ,fatu_070.cod_transacao
        ,estq_005.descricao as desc_transacao
        ,cpag_040.tipo_titulo ---cpag_010.tipo_titulo
        ,cpag_040.descricao as nome_tipo
        ,fatu_070.num_duplicata
        ,fatu_070.seq_duplicatas
        ,fatu_070.data_emissao
        ,fatu_070.data_prorrogacao as data_venc
        ,fatu_070.data_prorrogacao as data_venc_real
        ,fatu_070.data_venc_duplic as data_venc_original
        ,fatu_070.data_prorrogacao as data_prorrogacao
        ,fatu_070.moeda_titulo
        ,basi_270.valor_moeda
        ,fatu_070.valor_duplicata

        ,(
            select
                sum(decode(cont_010.sinal_titulo, 1, fatu_075.valor_pago, 2, fatu_075.valor_pago * (-1), 0))
            from fatu_075, cont_010
            where   fatu_075.nr_titul_codempr          = fatu_070.codigo_empresa
                and fatu_075.nr_titul_cli_dup_cgc_cli9 = fatu_070.cli_dup_cgc_cli9
                and fatu_075.nr_titul_cli_dup_cgc_cli4 = fatu_070.cli_dup_cgc_cli4
                and fatu_075.nr_titul_cli_dup_cgc_cli2 = fatu_070.cli_dup_cgc_cli2
                and fatu_075.nr_titul_cod_tit          = fatu_070.tipo_titulo
                and fatu_075.nr_titul_num_dup          = fatu_070.num_duplicata
                and fatu_075.nr_titul_seq_dup          = fatu_070.seq_duplicatas
                and fatu_075.historico_pgto            = cont_010.codigo_historico
                and fatu_075.pago_adiantamento         = 1
        ) as valor_compensado

        ,nvl((
            select
                sum(fatu_075.valor_pago)
            from fatu_075
            where   fatu_075.nr_titul_codempr          = fatu_070.codigo_empresa
                and fatu_075.nr_titul_cli_dup_cgc_cli9 = fatu_070.cli_dup_cgc_cli9
                and fatu_075.nr_titul_cli_dup_cgc_cli4 = fatu_070.cli_dup_cgc_cli4
                and fatu_075.nr_titul_cli_dup_cgc_cli2 = fatu_070.cli_dup_cgc_cli2
                and fatu_075.nr_titul_cod_tit          = fatu_070.tipo_titulo
                and fatu_075.nr_titul_num_dup          = fatu_070.num_duplicata
                and fatu_075.nr_titul_seq_dup          = fatu_070.seq_duplicatas)
        ,0) as valor_baixa

        ,fatu_070.posicao_duplic
        ,crec_010.descricao
        ,fatu_070.nr_titulo_banco
        ,fatu_070.num_contabil
        ,fatu_070.numero_bordero
        ,fatu_070.pedido_venda
        ,fatu_070.data_emissao as data_transacao
        ,fatu_070.codigo_contabil
        ,fatu_070.cod_historico
        ,case
            when fatu_070.previsao = 1 then
                'SIM'
            else
                'NÃO'
        end as previsto
        ,fatu_070.cod_canc_duplic
        ,basi_160.cidade
        ,basi_160.estado
        ,pedi_020.nome_rep_cliente
        ,fatu_070.cod_rep_cliente

    from
        fatu_070
        ,pedi_010
        ,estq_005
        --,cpag_010
        ,cpag_040
        ,basi_270
        ,crec_010
        ,basi_160
        --,cpag_015
        ,pedi_020

    where
            fatu_070.cli_dup_cgc_cli9     = pedi_010.cgc_9
        and fatu_070.cli_dup_cgc_cli4     = pedi_010.cgc_4
        and fatu_070.cli_dup_cgc_cli2     = pedi_010.cgc_2
        and fatu_070.cod_transacao        = estq_005.codigo_transacao
        --  and fatu_070.cli_dup_cgc_cli9 = cpag_010.cgc_9
        --  and fatu_070.cli_dup_cgc_cli4 = cpag_010.cgc_4
        --  and fatu_070.cli_dup_cgc_cli2 = cpag_010.cgc_2
        --  and fatu_070.tipo_titulo      = cpag_010.tipo_titulo
        and fatu_070.tipo_titulo          = cpag_040.tipo_titulo
        and fatu_070.moeda_titulo         = basi_270.codigo_moeda  (+)
        and fatu_070.data_emissao         = basi_270.data_moeda (+)
        and fatu_070.posicao_duplic       = crec_010.posicao_duplic
        and pedi_010.cod_cidade           = basi_160.cod_cidade (+)
        --  and cpag_010.nr_duplicata     = cpag_015.dupl_for_nrduppag
        --  and cpag_010.parcela          = cpag_015.dupl_for_no_parc
        --  and cpag_010.cgc_9            = cpag_015.dupl_for_for_cli9
        --  and cpag_010.cgc_4            = cpag_015.dupl_for_for_cli4
        --  and cpag_010.cgc_2            = cpag_015.dupl_for_for_cli2
        --  and cpag_010.tipo_titulo      = cpag_015.dupl_for_tipo_tit
        and pedi_010.cdrepres_cliente     = pedi_020.cod_rep_cliente
        and fatu_070.saldo_duplicata      <= 0
/
