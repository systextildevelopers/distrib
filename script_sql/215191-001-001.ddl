insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpt_l002', 'Cadastro de Rolos Cortados em Processo', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpt_l002', 'pcpt_menu',1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpt_l002', 'pcpt_menu', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Cadastro de Rolos Cortados em Processo'
 where hdoc_036.codigo_programa = 'pcpt_l002'
   and hdoc_036.locale          = 'es_ES';
commit;
