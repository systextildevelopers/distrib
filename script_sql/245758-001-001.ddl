ALTER TABLE supr_010 ADD inserido_no_obc NUMBER(1) DEFAULT 0;

COMMENT ON COLUMN supr_010.inserido_no_obc IS '0 = N�o inserido, 1 = Inserido';
