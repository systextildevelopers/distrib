-- Create table 
drop table PCPT_070_LOG;
create table PCPT_070_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(20) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(20) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  id_OLD                     NUMBER(9) ,  
  id_NEW                     NUMBER(9) , 
  pedido_venda_OLD           NUMBER(9) ,  
  pedido_venda_NEW           NUMBER(9) , 
  seq_item_pedido_OLD        NUMBER(3) ,  
  seq_item_pedido_NEW        NUMBER(3) , 
  observacao_OLD             VARCHAR2(300),  
  observacao_NEW             VARCHAR2(300), 
  situacao_OLD               NUMBER(1) default 0 ,  
  situacao_NEW               NUMBER(1) default 0 , 
  sugestao_escolhida_OLD     VARCHAR2(30) default 'Nenhuma' ,  
  sugestao_escolhida_NEW     VARCHAR2(30) default 'Nenhuma' , 
  criado_por_OLD             VARCHAR2(250),  
  criado_por_NEW             VARCHAR2(250), 
  criado_em_OLD              TIMESTAMP(6) default current_timestamp,  
  criado_em_NEW              TIMESTAMP(6) default current_timestamp, 
  id_pedido_forca_vendas_OLD VARCHAR2(128), 
  id_pedido_forca_vendas_NEW VARCHAR2(128) 
) ;

/
