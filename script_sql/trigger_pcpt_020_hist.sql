create or replace trigger "TRIGGER_PCPT_020_HIST" 
before update of tipo_reg_prod, data_insercao, valor_movto_cardex, valor_contabil_cardex,
	tabela_origem_cardex, certificacao_qualidade, pedido_loja, gatilho_trg,
	executa_trigger, posicao_rolo, codigo_analista_qualidade, data_inspecao_qualidade,
	nr_pedido_ordem_dest, sequenci_periodo_dest, codigo_deposito_dest, pedido_corte,
	pos_rolo_processo, rolo_externo, seq_rolo_externo, centro_custo_cardex,
	montador, produto_cliente, panoacab_subgrupo, panoacab_item,
	qtde_quilos_prod, qtde_quilos_acab, qtde_quilos_decl, qtde_rolos_real,
	qtde_rolos_decla, data_inicio_prod, data_prod_tecel, hora_inicio,
	hora_termino, grupo_maquina, sub_maquina, numero_maquina,
	codigo_operador, qtde_voltas, turno_producao, rolo_estoque,
	qualidade_rolo, codigo_deposito, pedido_venda, nr_solic_volume,
	nr_volume, seq_item_pedido, cod_empresa_nota, nota_fiscal,
	serie_fiscal_sai, seq_nota_fiscal, cliente_cgc9, cliente_cgc4,
	cliente_cgc2, nota_fiscal_ent, seri_fiscal_ent, sequ_fiscal_ent,
	numero_lote, largura, gramatura, periodo_origem,
	nr_rolo_origem, transacao_ent, numero_ordem, seq_ordem_serv,
	peso_bruto, tara, codigo_rolo, ordem_tecelagem,
	ordem_producao, seq_ordem, area_producao, codigo_embalagem,
	proforma, valor_entrada, valor_despesas, data_entrada,
	endereco_rolo, nome_prog_020, largura_padrao, seq_ordem_tec,
	codigo_tecelao, pre_romaneio, tipo_producao, agrupador,
	mt_lineares, pistas, rolo_anterior, rolo_associado,
	mt_lineares_prod, numero_tubete, fornecedor_cgc9, fornecedor_cgc4,
	fornecedor_cgc2, caixa, kanban, area,
	lote_acomp, pontos_qualidade, rolo_confirmado, ordem_estampa,
	seq_ordem_estampa, qtde_quilos_cru, peso_real_fios_retilinea, desc_prod_cliente,
	data_cardex, transacao_cardex, usuario_cardex, periodo_producao,
	numero_rolo, numero_programa, panoacab_nivel99, panoacab_grupo
       or delete or
       insert on pcpt_020
for each row
declare
   ws_codigo_rolo             pcpt_020.codigo_rolo%type;
   ws_nivel                   pcpt_020.panoacab_nivel99%type;
   ws_grupo                   pcpt_020.panoacab_grupo%type;
   ws_sub                     pcpt_020.panoacab_subgrupo%type;
   ws_item                    pcpt_020.panoacab_item%type;
   ws_lote                    pcpt_020.numero_lote%type;
   ws_deposito_old            pcpt_020.codigo_deposito%type;
   ws_deposito_atu            pcpt_020.codigo_deposito%type;
   ws_rolo_estq_old           pcpt_020.rolo_estoque%type;
   ws_rolo_estq_atu           pcpt_020.rolo_estoque%type;
   ws_peso_old                pcpt_020.qtde_quilos_acab%type;
   ws_peso_atu                pcpt_020.qtde_quilos_acab%type;
   ws_nota_old                pcpt_020.nota_fiscal%type;
   ws_nota_atu                pcpt_020.nota_fiscal%type;
   ws_pedido_old              pcpt_020.pedido_venda%type;
   ws_pedido_atu              pcpt_020.pedido_venda%type;
   ws_transacao_old           pcpt_020.transacao_ent%type;
   ws_transacao_atu           pcpt_020.transacao_ent%type;
   ws_atualiza_estq           estq_005.atualiza_estoque%type;
   ws_nota_ent_old            pcpt_020.nota_fiscal_ent%type;
   ws_nota_ent_atu            pcpt_020.nota_fiscal_ent%type;
   ws_nr_rolo_origem_old      pcpt_020.nr_rolo_origem%type;
   ws_nr_rolo_origem_atu      pcpt_020.nr_rolo_origem%type;
   ws_tipo_ocorr              pcpt_020_hist.tipo_ocorr%type;
   ws_nome_programa           pcpt_020_hist.nome_programa%type;
   ws_ordem_producao          pcpt_020.ordem_producao%type;
   ws_data_prod_tecel         pcpt_020_hist.data_prod_tecel%type;
   ws_data_entrada            pcpt_020_hist.data_entrada%type;
   ws_data_prod_tecel_atu     pcpt_020.data_prod_tecel%type;
   ws_data_prod_tecel_old     pcpt_020.data_prod_tecel%type;
   ws_data_entrada_atu        pcpt_020.data_entrada%type;
   ws_data_entrada_old        pcpt_020.data_entrada%type;
   ws_nr_solic_volume_old     pcpt_020.nr_solic_volume%type;
   ws_nr_solic_volume_new     pcpt_020.nr_solic_volume%type;
   ws_rolo_confirmado_old     pcpt_020.rolo_confirmado%type;
   ws_rolo_confirmado_new     pcpt_020.rolo_confirmado%type;
   ws_qtde_quilos_acab_old    pcpt_020.qtde_quilos_acab%type;
   ws_qtde_quilos_acab_new    pcpt_020.qtde_quilos_acab%type;
   ws_usuario_rede            pcpt_020_hist.usuario_rede%type;
   ws_maquina_rede            pcpt_020_hist.maquina_rede%type;
   ws_aplicacao               pcpt_020_hist.aplicacao%type;
   ws_cod_qualidade_old       pcpt_020.qualidade_rolo%type;
   ws_cod_qualidade_new       pcpt_020.qualidade_rolo%type;
   ws_usuario_qualidade       hdoc_030.usuario%type;
   ws_endereco_rolo_old       pcpt_020.endereco_rolo%type;
   ws_endereco_rolo_new       pcpt_020.endereco_rolo%type;
   ws_pos_rolo_processo_old   pcpt_020.pos_rolo_processo%type;
   ws_pos_rolo_processo_new   pcpt_020.pos_rolo_processo%type;
   ws_pre_romaneio_old        pcpt_020.pre_romaneio%type;
   ws_pre_romaneio_new        pcpt_020.pre_romaneio%type;
   ws_mt_lineares_prod_old    pcpt_020.mt_lineares_prod%type;
   ws_mt_lineares_prod_new    pcpt_020.mt_lineares_prod%type;
   ws_sid                     number;
   ws_usuario_systextil       hdoc_030.usuario%type;
   ws_processo_systextil      hdoc_090.programa%type;
   v_executa_trigger          pcpt_020.executa_trigger%type;
   ws_aplicativo              varchar2(20); 
   ws_empresa                 number(3); 
   ws_locale_usuario          varchar2(5);
   ws_lote_acomp_old          pcpt_020.lote_acomp%type;
   ws_lote_acomp_new          pcpt_020.lote_acomp%type;
   ws_fornecedor_cgc9_old     pcpt_020.fornecedor_cgc9%type;
   ws_fornecedor_cgc4_old     pcpt_020.fornecedor_cgc4%type;
   ws_fornecedor_cgc2_old     pcpt_020.fornecedor_cgc2%type;
   ws_fornecedor_cgc9_new     pcpt_020.fornecedor_cgc9%type;
   ws_fornecedor_cgc4_new     pcpt_020.fornecedor_cgc4%type;
   ws_fornecedor_cgc2_new     pcpt_020.fornecedor_cgc2%type;
   ws_executa_trigger_old     pcpt_020.executa_trigger%type;
   ws_executa_trigger_new     pcpt_020.executa_trigger%type;  
   ws_metros_cubicos_new      basi_020.metros_cubicos%type;
   ws_metros_cubicos_old      basi_020.metros_cubicos%type;
   ws_sequ_fiscal_ent_old     pcpt_020.sequ_fiscal_ent%type;
   ws_sequ_fiscal_ent_new     pcpt_020.sequ_fiscal_ent%type;

begin

    inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                         ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ws_processo_systextil := inter_fn_nome_programa(ws_sid); 
       ws_nome_programa := ws_processo_systextil;


    if INSERTING then
       ws_ordem_producao           := :new.ordem_producao;
       ws_deposito_atu             := :new.codigo_deposito;
       ws_deposito_old             := 0;
       ws_rolo_estq_atu            := :new.rolo_estoque;
       ws_rolo_estq_old            := 0;
       ws_peso_atu                 := :new.qtde_quilos_acab;
       ws_peso_old                 := 0;
       ws_nota_atu                 := :new.nota_fiscal;
       ws_nota_old                 := 0;
       ws_pedido_atu               := :new.pedido_venda;
       ws_pedido_old               := 0;
       ws_transacao_atu            := :new.transacao_ent;
       ws_transacao_old            := 0;
       ws_nota_ent_atu             := :new.nota_fiscal_ent;
       ws_nota_ent_old             := 0;
       ws_nr_rolo_origem_atu       := :new.nr_rolo_origem;
       ws_nr_rolo_origem_old       := 0;
       ws_tipo_ocorr               := 'I';
               if :new.nome_prog_020 is not null and :new.nome_prog_020 <> ' ' 
       then ws_nome_programa            := :new.nome_prog_020;
               end if;
       ws_codigo_rolo              := :new.codigo_rolo;
       ws_nivel                    := :new.panoacab_nivel99;
       ws_grupo                    := :new.panoacab_grupo;
       ws_sub                      := :new.panoacab_subgrupo;
       ws_item                     := :new.panoacab_item;
       ws_lote                     := :new.numero_lote;
       ws_data_prod_tecel_atu      := :new.data_prod_tecel;
       ws_data_prod_tecel_old      := null;
       ws_data_entrada_atu         := :new.data_entrada;
       ws_data_entrada_old         := null;
       ws_nr_solic_volume_old      := null;
       ws_nr_solic_volume_new      := :new.nr_solic_volume;
       ws_rolo_confirmado_old      := null;
       ws_rolo_confirmado_new      := :new.rolo_confirmado;
       ws_qtde_quilos_acab_old     := null;
       ws_qtde_quilos_acab_new     := :new.qtde_quilos_acab;
       ws_cod_qualidade_old        := null;
       ws_cod_qualidade_new        := :new.qualidade_rolo;
       ws_usuario_qualidade        := ws_usuario_systextil;
       ws_endereco_rolo_new        := :new.endereco_rolo;
       ws_endereco_rolo_old        := null;
       ws_pos_rolo_processo_new    := :new.pos_rolo_processo;
       ws_pos_rolo_processo_old    := null;
       ws_pre_romaneio_new         := :new.pre_romaneio;
       ws_pre_romaneio_old         := null;
       ws_mt_lineares_prod_new     := :new.mt_lineares_prod;
       ws_mt_lineares_prod_old     := 0.0;
       ws_lote_acomp_old           := null;
       ws_lote_acomp_new           := :new.lote_acomp;
       ws_fornecedor_cgc9_old      := 0.0;
       ws_fornecedor_cgc9_new      := :new.fornecedor_cgc9;
       ws_fornecedor_cgc4_old      := 0.0;
       ws_fornecedor_cgc4_new      := :new.fornecedor_cgc4;
       ws_fornecedor_cgc2_old      := 0.0;
       ws_fornecedor_cgc2_new      := :new.fornecedor_cgc2;
       ws_executa_trigger_old      := null;
       ws_executa_trigger_new      := :new.executa_trigger;    
       ws_metros_cubicos_old       := null;
       ws_metros_cubicos_new       := :new.metros_cubicos; 
       ws_sequ_fiscal_ent_old      := null;
       ws_sequ_fiscal_ent_new      := :new.sequ_fiscal_ent;

       select atualiza_estoque
       into   ws_atualiza_estq
       from estq_005
       where codigo_transacao = ws_transacao_atu;

    elsif UPDATING then
          ws_codigo_rolo              := :old.codigo_rolo;
          ws_nivel                    := :old.panoacab_nivel99;
          ws_grupo                    := :old.panoacab_grupo;
          ws_sub                      := :old.panoacab_subgrupo;
          ws_item                     := :old.panoacab_item;
          ws_lote                     := :old.numero_lote;
          ws_ordem_producao           := :old.ordem_producao;
          ws_deposito_atu             := :new.codigo_deposito;
          ws_deposito_old             := :old.codigo_deposito;
          ws_rolo_estq_atu            := :new.rolo_estoque;
          ws_rolo_estq_old            := :old.rolo_estoque;
          ws_peso_atu                 := :new.qtde_quilos_acab;
          ws_peso_old                 := :old.qtde_quilos_acab;
          ws_nota_atu                 := :new.nota_fiscal;
          ws_nota_old                 := :old.nota_fiscal;
          ws_pedido_atu               := :new.pedido_venda;
          ws_pedido_old               := :old.pedido_venda;
          ws_transacao_atu            := :new.transacao_ent;
          ws_transacao_old            := :old.transacao_ent;
          ws_nota_ent_atu             := :new.nota_fiscal_ent;
          ws_nota_ent_old             := :old.nota_fiscal_ent;
          ws_nr_rolo_origem_atu       := :new.nr_rolo_origem;
          ws_nr_rolo_origem_old       := :old.nr_rolo_origem;
          ws_tipo_ocorr               := 'A';
                      if :new.nome_prog_020 is not null and :new.nome_prog_020 <> ' ' 
                      then ws_nome_programa            := :new.nome_prog_020;
                      end if;
          ws_data_prod_tecel_atu      := :new.data_prod_tecel;
          ws_data_prod_tecel_old      := :old.data_prod_tecel;
          ws_data_entrada_atu         := :new.data_entrada;
          ws_data_entrada_old         := :old.data_entrada;
          ws_nr_solic_volume_old      := :old.nr_solic_volume;
          ws_nr_solic_volume_new      := :new.nr_solic_volume;
          ws_rolo_confirmado_old      := :old.rolo_confirmado;
          ws_rolo_confirmado_new      := :new.rolo_confirmado;
          ws_qtde_quilos_acab_old     := :old.qtde_quilos_acab;
          ws_qtde_quilos_acab_new     := :new.qtde_quilos_acab;
          ws_cod_qualidade_old        := :old.qualidade_rolo;
          ws_cod_qualidade_new        := :new.qualidade_rolo;
          ws_usuario_qualidade        := ws_usuario_systextil;
          ws_endereco_rolo_old        := :old.endereco_rolo;
          ws_endereco_rolo_new        := :new.endereco_rolo;
          ws_pos_rolo_processo_old    := :old.pos_rolo_processo;
          ws_pos_rolo_processo_new    := :new.pos_rolo_processo;
          ws_pre_romaneio_old         := :old.pre_romaneio;
          ws_pre_romaneio_new         := :new.pre_romaneio;
          ws_mt_lineares_prod_old     := :old.mt_lineares_prod;
          ws_mt_lineares_prod_new     := :new.mt_lineares_prod;
          ws_lote_acomp_old           := :old.lote_acomp;
          ws_lote_acomp_new           := :new.lote_acomp;
          ws_fornecedor_cgc9_old      := :old.fornecedor_cgc9;
          ws_fornecedor_cgc9_new      := :new.fornecedor_cgc9;
          ws_fornecedor_cgc4_old      := :old.fornecedor_cgc4;
          ws_fornecedor_cgc4_new      := :new.fornecedor_cgc4;
          ws_fornecedor_cgc2_old      := :old.fornecedor_cgc2;
          ws_fornecedor_cgc2_new      := :new.fornecedor_cgc2;
          ws_executa_trigger_old      := :old.executa_trigger;
          ws_executa_trigger_new      := :new.executa_trigger;
          ws_metros_cubicos_old       := :old.metros_cubicos;
       	  ws_metros_cubicos_new       := :new.metros_cubicos; 
          ws_sequ_fiscal_ent_old      := :old.sequ_fiscal_ent;
          ws_sequ_fiscal_ent_new      := :new.sequ_fiscal_ent;

          select atualiza_estoque
          into   ws_atualiza_estq
          from estq_005
          where codigo_transacao = ws_transacao_atu;

       elsif DELETING then
             ws_codigo_rolo              := :old.codigo_rolo;
             ws_nivel                    := :old.panoacab_nivel99;
             ws_grupo                    := :old.panoacab_grupo;
             ws_sub                      := :old.panoacab_subgrupo;
             ws_item                     := :old.panoacab_item;
             ws_lote                     := :old.numero_lote;
             ws_ordem_producao           := :old.ordem_producao;
             ws_deposito_atu             := 0;
             ws_deposito_old             := :old.codigo_deposito;
             ws_rolo_estq_atu            := 0;
             ws_rolo_estq_old            := :old.rolo_estoque;
             ws_peso_atu                 := 0.0;
             ws_peso_old                 := :old.qtde_quilos_acab;
             ws_nota_atu                 := 0.0;
             ws_nota_old                 := :old.nota_fiscal;
             ws_pedido_atu               := 0;
             ws_pedido_old               := :old.pedido_venda;
             ws_transacao_atu            := 0;
             ws_transacao_old            := :old.transacao_ent;
             ws_nota_ent_atu             := 0;
             ws_nota_ent_old             := :old.nota_fiscal_ent;
             ws_nr_rolo_origem_atu       := 0;
             ws_nr_rolo_origem_old       := :old.nr_rolo_origem;
             ws_tipo_ocorr               := 'D';
                         if :old.nome_prog_020 is not null and :old.nome_prog_020 <> ' ' 
                         then ws_nome_programa            := :old.nome_prog_020;
                         end if;
             ws_data_prod_tecel_atu      := null;
             ws_data_prod_tecel_old      := :old.data_prod_tecel;
             ws_data_entrada_atu         := null;
             ws_data_entrada_old         := :old.data_entrada;
             ws_nr_solic_volume_new      := null;
             ws_rolo_confirmado_old      := :old.rolo_confirmado;
             ws_rolo_confirmado_new      := null;
             ws_qtde_quilos_acab_old     := :old.qtde_quilos_acab;
             ws_qtde_quilos_acab_new     := null;
             ws_cod_qualidade_old        := :old.qualidade_rolo;
             ws_cod_qualidade_new        := null;
             ws_usuario_qualidade        := ws_usuario_systextil;
             ws_endereco_rolo_old        := :old.endereco_rolo;
             ws_endereco_rolo_new        := null;
             ws_pos_rolo_processo_old    := :old.pos_rolo_processo;
             ws_pos_rolo_processo_new    := null;
             ws_pre_romaneio_old         := :old.pre_romaneio;
             ws_pre_romaneio_new         := null;               
             ws_mt_lineares_prod_old     := :old.mt_lineares_prod;
             ws_mt_lineares_prod_new     := 0.0;
             ws_lote_acomp_old           := :old.lote_acomp;
             ws_lote_acomp_new           := null;
             ws_fornecedor_cgc9_old      := :old.fornecedor_cgc9;
             ws_fornecedor_cgc9_new      := null;
             ws_fornecedor_cgc4_old      := :old.fornecedor_cgc4;
             ws_fornecedor_cgc4_new      := null;
             ws_fornecedor_cgc2_old      := :old.fornecedor_cgc2;
             ws_fornecedor_cgc2_new      := null;
             ws_executa_trigger_old      := :old.executa_trigger;
             ws_executa_trigger_new      := null;
             ws_metros_cubicos_old       := :old.metros_cubicos;
       	  	 ws_metros_cubicos_new       := null;
             ws_sequ_fiscal_ent_old      := :old.sequ_fiscal_ent;
             ws_sequ_fiscal_ent_new      := null;

             select atualiza_estoque
             into   ws_atualiza_estq
             from estq_005
             where codigo_transacao = ws_transacao_old;

    end if;

    INSERT INTO pcpt_020_hist
      (codigo_rolo,             nivel,                    grupo,
       sub,                     item,                     lote,
       deposito_old,            deposito_atu,             rolo_estq_old,
       rolo_estq_atu,           peso_old,                 peso_atu,
       nota_old,                nota_atu,                 pedido_old,
       pedido_atu,              transacao_old,            transacao_atu,
       atualiza_estq,           nota_ent_old,             nota_ent_atu,
       data_ocorr,              tipo_ocorr,               nome_programa,
       ordem_producao,          data_prod_tecel,          data_entrada,
       data_prod_tecel_atu,     data_entrada_atu,         data_prod_tecel_old,
       data_entrada_old,        usuario_rede,             maquina_rede,
       aplicacao,               nr_solic_volume_old,      nr_solic_volume_new,
       rolo_confirmado_old,     rolo_confirmado_new,      qtde_quilos_acab_old,
       qtde_quilos_acab_new,    cod_qualidade_old,        cod_qualidade_new,
       usuario_qualidade,       processo_systextil,       endereco_rolo_old,
       endereco_rolo_new,       pos_rolo_processo_old,    pos_rolo_processo_new,
       nr_rolo_origem_old,      nr_rolo_origem_atu,       pre_romaneio_new,
       pre_romaneio_old,        mt_lineares_prod_new,     mt_lineares_prod_old,
       lote_acomp_old,          lote_acomp_new,           fornecedor_cgc9_old,
       fornecedor_cgc9_new,     fornecedor_cgc4_old,      fornecedor_cgc4_new,     
       fornecedor_cgc2_old,     fornecedor_cgc2_new,      executa_trigger_old,
       executa_trigger_new,     metros_cubicos_old,       metros_cubicos_new,
       sequ_fiscal_ent_old,     sequ_fiscal_ent_new
       )
    VALUES
      (ws_codigo_rolo,          ws_nivel,                 ws_grupo,
       ws_sub,                  ws_item,                  ws_lote,
       ws_deposito_old,         ws_deposito_atu,          ws_rolo_estq_old,
       ws_rolo_estq_atu,        ws_peso_old,              ws_peso_atu,
       ws_nota_old,             ws_nota_atu,              ws_pedido_old,
       ws_pedido_atu,           ws_transacao_old,         ws_transacao_atu,
       ws_atualiza_estq,        ws_nota_ent_old,          ws_nota_ent_atu,
       sysdate,                 ws_tipo_ocorr,            ws_nome_programa,
       ws_ordem_producao,       ws_data_prod_tecel,       ws_data_entrada,
       ws_data_prod_tecel_atu,  ws_data_entrada_atu,      ws_data_prod_tecel_old,
       ws_data_entrada_old,     ws_usuario_rede,          ws_maquina_rede,
       ws_aplicacao,            ws_nr_solic_volume_old,   ws_nr_solic_volume_new,
       ws_rolo_confirmado_old,  ws_rolo_confirmado_new,   ws_qtde_quilos_acab_old,
       ws_qtde_quilos_acab_new, ws_cod_qualidade_old,     ws_cod_qualidade_new,
       ws_usuario_qualidade,    ws_processo_systextil,    ws_endereco_rolo_old,
       ws_endereco_rolo_new,    ws_pos_rolo_processo_old, ws_pos_rolo_processo_new,
       ws_nr_rolo_origem_old,   ws_nr_rolo_origem_atu,    ws_pre_romaneio_new,
       ws_pre_romaneio_old,     ws_mt_lineares_prod_new,  ws_mt_lineares_prod_old,
       ws_lote_acomp_old,       ws_lote_acomp_new,        ws_fornecedor_cgc9_old,
       ws_fornecedor_cgc9_new,  ws_fornecedor_cgc4_old,   ws_fornecedor_cgc4_new,     
       ws_fornecedor_cgc2_old,  ws_fornecedor_cgc2_new,   ws_executa_trigger_old,
       ws_executa_trigger_new,  ws_metros_cubicos_old,    ws_metros_cubicos_new,
       ws_sequ_fiscal_ent_old,  ws_sequ_fiscal_ent_new);

    if INSERTING or UPDATING then
       :new.nome_prog_020 := '';
    end if;

end trigger_pcpt_020_hist;

-- ALTER TRIGGER "TRIGGER_PCPT_020_HIST" ENABLE
 

/

exec inter_pr_recompile;

