create table pcpc_405(
    id number(4),
    constraint pcpc_405_pk primary key (id),
    descricao varchar2(120));

comment on column pcpc_405.id is 'Identificador da ocorrência';
comment on column pcpc_405.descricao is 'Descrição da ocorrência';
comment on table pcpc_405 is 'Tabela de cadastro de ocorrências de encaixes do enfesto';

/
