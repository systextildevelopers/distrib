
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_030_INTEGRACAO" 
before insert or
       delete or
       update of nivel_estrutura,   referencia,         descr_referencia,
                 unidade_medida,    classific_fiscal,   tipo_produto
       on basi_030
for each row
declare

                                                                               v_peso_amostra                 e_basi_030.peso_amostra%type;
  v_nivel_estrutura         	e_basi_030.nivel_estrutura%type;               v_comp_serra_fita              e_basi_030.comp_serra_fita%type;
  v_referencia              	e_basi_030.referencia%type;                    v_parte_composicao_01          e_basi_030.parte_composicao_01%type;
  v_descr_referencia        	e_basi_030.descr_referencia%type;              v_parte_composicao_02          e_basi_030.parte_composicao_02%type;
  v_unidade_medida          	e_basi_030.unidade_medida%type;                v_parte_composicao_03          e_basi_030.parte_composicao_03%type;
  v_colecao                 	e_basi_030.colecao%type;                       v_parte_composicao_04          e_basi_030.parte_composicao_04%type;
  v_conta_estoque           	e_basi_030.conta_estoque%type;                 v_parte_composicao_05          e_basi_030.parte_composicao_05%type;
  v_linha_produto           	e_basi_030.linha_produto%type;                 v_composicao_01                e_basi_030.composicao_01%type;
  v_artigo                  	e_basi_030.artigo%type;                        v_composicao_02                e_basi_030.composicao_02%type;
  v_artigo_cotas            	e_basi_030.artigo_cotas%type;                  v_composicao_03                e_basi_030.composicao_03%type;
  v_tipo_produto            	e_basi_030.tipo_produto%type;                  v_composicao_04                e_basi_030.composicao_04%type;
  v_classific_fiscal        	e_basi_030.classific_fiscal%type;              v_composicao_05                e_basi_030.composicao_05%type;
  v_comprado_fabric         	e_basi_030.comprado_fabric%type;               v_composicao_01_ingles         e_basi_030.composicao_01_ingles%type;
  v_cor_de_estoque          	e_basi_030.cor_de_estoque%type;                v_composicao_02_ingles         e_basi_030.composicao_02_ingles%type;
  v_tipo_estampa            	e_basi_030.tipo_estampa%type;                  v_composicao_03_ingles         e_basi_030.composicao_03_ingles%type;
  v_codigo_contabil         	e_basi_030.codigo_contabil%type;               v_composicao_04_ingles         e_basi_030.composicao_04_ingles%type;
  v_data_importacao             e_basi_030.data_importacao%type;               v_composicao_05_ingles         e_basi_030.composicao_05_ingles%type;
                                                                               v_imp_inf_empresa_tag          e_basi_030.imp_inf_empresa_tag%type;
  	                                                                       v_estampa_impressa             e_basi_030.estampa_impressa%type;
  v_perc_2_qualidade       	e_basi_030.perc_2_qualidade%type;     	       v_cod_tipo_aviamento           e_basi_030.cod_tipo_aviamento%type;
  v_artigo_aproveit        	e_basi_030.artigo_aproveit%type;      	       v_cod_atributo_01              e_basi_030.cod_atributo_01%type;
  v_percent_desconto       	e_basi_030.percent_desconto%type;     	       v_cod_atributo_02              e_basi_030.cod_atributo_02%type;
  v_variacao_cores         	e_basi_030.variacao_cores%type;     	       v_cod_atributo_03              e_basi_030.cod_atributo_03%type;
  v_tamanho_filme          	e_basi_030.tamanho_filme%type;      	       v_cod_atributo_04              e_basi_030.cod_atributo_04%type;
  v_raportagem             	e_basi_030.raportagem%type;     	       v_cod_atributo_05              e_basi_030.cod_atributo_05%type;
  v_percent_perda_1        	e_basi_030.percent_perda_1%type;     	       v_cod_atributo_06              e_basi_030.cod_atributo_06%type;
  v_percent_perda_2        	e_basi_030.percent_perda_2%type;     	       v_cod_atributo_07              e_basi_030.cod_atributo_07%type;
  v_percent_perda_3        	e_basi_030.percent_perda_3%type;     	       v_cod_atributo_08              e_basi_030.cod_atributo_08%type;
  v_aproveit_medio         	e_basi_030.aproveit_medio%type;      	       v_cod_atributo_09              e_basi_030.cod_atributo_09%type;
  v_metros_lineares        	e_basi_030.metros_lineares%type;     	       v_cod_atributo_10              e_basi_030.cod_atributo_10%type;
  v_indice_reajuste        	e_basi_030.indice_reajuste%type;     	       v_referencia_agrupador         e_basi_030.referencia_agrupador%type;
  v_item_baixa_estq        	e_basi_030.item_baixa_estq%type;     	       v_maquina_piloto               e_basi_030.maquina_piloto%type;
  v_demanda                	e_basi_030.demanda%type;     		       v_finura_piloto                e_basi_030.finura_piloto%type;
  v_pontos_por_cm          	e_basi_030.pontos_por_cm%type;     	       v_graduacao_piloto             e_basi_030.graduacao_piloto%type;
  v_observacao             	e_basi_030.observacao%type;     	       v_programa_piloto              e_basi_030.programa_piloto%type;
  v_agulhas_falhadas       	e_basi_030.agulhas_falhadas%type;     	       v_programador_piloto           e_basi_030.programador_piloto%type;
  v_formacao_agulhas       	e_basi_030.formacao_agulhas%type;     	       v_peso_bruto_piloto            e_basi_030.peso_bruto_piloto%type;
  v_aberto_tubular         	e_basi_030.aberto_tubular%type;     	       v_peso_acabamento_piloto       e_basi_030.peso_acabamento_piloto%type;
  v_numero_roteiro         	e_basi_030.numero_roteiro%type;     	       v_tecelagem_piloto             e_basi_030.tecelagem_piloto%type;
  v_ref_original           	e_basi_030.ref_original%type;      	       v_faccao_piloto                e_basi_030.faccao_piloto%type;
  v_numero_molde           	e_basi_030.numero_molde%type;     	       v_pers_bordado_piloto          e_basi_030.pers_bordado_piloto%type;
  v_div_prod               	e_basi_030.div_prod%type;     		       v_pers_lavacao_piloto          e_basi_030.pers_lavacao_piloto%type;
  v_cor_de_producao        	e_basi_030.cor_de_producao%type;     	       v_pers_serigrafia_piloto       e_basi_030.pers_serigrafia_piloto%type;
  v_cgc_cliente_2          	e_basi_030.cgc_cliente_2%type;      	       v_personalizacao_piloto        e_basi_030.personalizacao_piloto%type;
  v_cgc_cliente_4          	e_basi_030.cgc_cliente_4%type;     	       v_grupo_tecido_corte           e_basi_030.grupo_tecido_corte%type;
  v_cgc_cliente_9          	e_basi_030.cgc_cliente_9%type;     	       v_data_mov_cilindro            e_basi_030.data_mov_cilindro%type;
  v_perc_composicao1       	e_basi_030.perc_composicao1%type;     	       v_serie_tamanho                e_basi_030.serie_tamanho%type;
  v_perc_composicao2       	e_basi_030.perc_composicao2%type;    	       v_numero_pontos_laser          e_basi_030.numero_pontos_laser%type;
  v_perc_composicao3       	e_basi_030.perc_composicao3%type;      	       v_percentual_amaciante         e_basi_030.percentual_amaciante%type;
  v_perc_composicao4       	e_basi_030.perc_composicao4%type;     	       v_simbolo_1                    e_basi_030.simbolo_1%type;
  v_perc_composicao5       	e_basi_030.perc_composicao5%type;      	       v_simbolo_2                    e_basi_030.simbolo_2%type;
  v_estagio_altera_programado   e_basi_030.estagio_altera_programado%type;     v_simbolo_3                    e_basi_030.simbolo_3%type;
  v_risco_padrao               	e_basi_030.risco_padrao%type;		       v_simbolo_4                    e_basi_030.simbolo_4%type;
  v_responsavel                	e_basi_030.responsavel%type;		       v_simbolo_5                    e_basi_030.simbolo_5%type;
  v_multiplicador              	e_basi_030.multiplicador%type;		       v_lote_estoque                 e_basi_030.lote_estoque%type;
  v_num_sol_desenv             	e_basi_030.num_sol_desenv%type;		       v_dias_maturacao               e_basi_030.dias_maturacao%type;
  v_sortimento_diversos        	e_basi_030.sortimento_diversos%type;	       v_cod_bar_por_cli              e_basi_030.cod_bar_por_cli%type;
  v_grupo_agrupador            	e_basi_030.grupo_agrupador%type;	       v_familia_atributo             e_basi_030.familia_atributo%type;
  v_sub_agrupador              	e_basi_030.sub_agrupador%type;       	       v_doc_referencial              e_basi_030.doc_referencial%type;
  v_cod_embalagem              	e_basi_030.cod_embalagem%type;       	       v_ligamento                    e_basi_030.ligamento%type;
  v_conf_altera_prog           	e_basi_030.conf_altera_prog%type;
  v_observacao2                	e_basi_030.observacao2%type;         	       v_perc_composicao6             e_basi_030.perc_composicao6%type;
  v_observacao3                	e_basi_030.observacao3%type;         	       v_perc_composicao7             e_basi_030.perc_composicao7%type;
  v_publico_alvo                e_basi_030.publico_alvo%type;         	       v_perc_composicao8             e_basi_030.perc_composicao8%type;
  v_cor_de_sortido              e_basi_030.cor_de_sortido%type;       	       v_perc_composicao9             e_basi_030.perc_composicao9%type;
  v_tipo_codigo_ean             e_basi_030.tipo_codigo_ean%type;      	       v_perc_composicao10            e_basi_030.perc_composicao10%type;
  v_num_programa                e_basi_030.num_programa%type;         	       v_composicao_06                e_basi_030.composicao_06%type;
  v_referencia_desenv           e_basi_030.referencia_desenv%type;    	       v_composicao_07                e_basi_030.composicao_07%type;
  v_tab_rendimento              e_basi_030.tab_rendimento%type;       	       v_composicao_08                e_basi_030.composicao_08%type;
  v_largura_cilindro            e_basi_030.largura_cilindro%type;     	       v_composicao_09                e_basi_030.composicao_09%type;
  v_largura_estampa             e_basi_030.largura_estampa%type;      	       v_composicao_10                e_basi_030.composicao_10%type;
  v_descricao_tag1              e_basi_030.descricao_tag1%type;       	       v_simbolo_6                    e_basi_030.simbolo_6%type;
  v_descricao_tag2              e_basi_030.descricao_tag2%type;       	       v_simbolo_7                    e_basi_030.simbolo_7%type;
  v_descr_complementar          e_basi_030.descr_complementar%type;   	       v_simbolo_8                    e_basi_030.simbolo_8%type;
  v_pasta_banho                 e_basi_030.pasta_banho%type;          	       v_simbolo_9                    e_basi_030.simbolo_9%type;
  v_perimetro_corte             e_basi_030.perimetro_corte%type;      	       v_simbolo_10                   e_basi_030.simbolo_10%type;
  v_produto_prototipo           e_basi_030.produto_prototipo%type;    	       v_parte_composicao_06          e_basi_030.parte_composicao_06%type;
  v_tempo_laser                 e_basi_030.tempo_laser%type;          	       v_parte_composicao_07          e_basi_030.parte_composicao_07%type;
  v_colecao_cliente             e_basi_030.colecao_cliente%type;      	       v_parte_composicao_08          e_basi_030.parte_composicao_08%type;
  v_descr_ingles    		e_basi_030.descr_ingles%type;		       v_parte_composicao_09          e_basi_030.parte_composicao_09%type;
  v_descr_espanhol  		e_basi_030.descr_espanhol%type;		       v_parte_composicao_10          e_basi_030.parte_composicao_10%type;
  v_tamanho_amostra 		e_basi_030.tamanho_amostra%type;

  v_tipo_operacao	        e_basi_030.tipo_operacao%type;
  ws_usuario_systextil          e_basi_030.usuario_operacao%type;

  ws_usuario_rede               varchar2(20);
  ws_maquina_rede               varchar2(40);
  ws_aplicativo                 varchar2(20);
  ws_sid                        number(9);
  ws_empresa                    number(3);
  ws_locale_usuario             varchar2(5);


begin

  inter_pr_dados_usuario (ws_usuario_rede, ws_maquina_rede, ws_aplicativo, ws_sid, ws_usuario_systextil, ws_empresa, ws_locale_usuario);

  v_peso_amostra              := :new.peso_amostra;
  v_nivel_estrutura         	:= :new.nivel_estrutura;
  v_comp_serra_fita           := :new.comp_serra_fita;
  v_referencia              	:= :new.referencia;               	       v_parte_composicao_01          := :new.parte_composicao_01;
  v_descr_referencia        	:= :new.descr_referencia;         	       v_parte_composicao_02          := :new.parte_composicao_02;
  v_unidade_medida          	:= :new.unidade_medida;           	       v_parte_composicao_03          := :new.parte_composicao_03;
  v_colecao                 	:= 0;                             	       v_parte_composicao_04          := :new.parte_composicao_04;
  v_conta_estoque           	:= 0;                             	       v_parte_composicao_05          := :new.parte_composicao_05;
  v_linha_produto           	:= 0;                             	       v_composicao_01                := :new.composicao_01;
  v_artigo                  	:= 0;                             	       v_composicao_02                := :new.composicao_02;
  v_artigo_cotas            	:= 0;                             	       v_composicao_03                := :new.composicao_03;
  v_tipo_produto            	:= :new.tipo_produto;             	       v_composicao_04                := :new.composicao_04;
  v_classific_fiscal        	:= :new.classific_fiscal;         	       v_composicao_05                := :new.composicao_05;
  v_comprado_fabric         	:= 1;                             	       v_composicao_01_ingles         := :new.composicao_01_ingles;
  v_cor_de_estoque          	:= :new.cor_de_estoque;           	       v_composicao_02_ingles         := :new.composicao_02_ingles;
  v_tipo_estampa            	:= :new.tipo_estampa;             	       v_composicao_03_ingles         := :new.composicao_03_ingles;
  v_codigo_contabil         	:= 0;                              	       v_composicao_04_ingles         := :new.composicao_04_ingles;
  v_data_importacao           := sysdate();            	                 v_composicao_05_ingles         := :new.composicao_05_ingles;
                                                                  	     v_imp_inf_empresa_tag          := :new.imp_inf_empresa_tag;
            	                                                           v_estampa_impressa             := :new.estampa_impressa;
  v_perc_2_qualidade       	:= :new.perc_2_qualidade;        	       v_cod_tipo_aviamento           := :new.cod_tipo_aviamento;
  v_artigo_aproveit        	:= :new.artigo_aproveit;         	       v_cod_atributo_01              := :new.cod_atributo_01;
  v_percent_desconto       	:= :new.percent_desconto;        	       v_cod_atributo_02              := :new.cod_atributo_02;
  v_variacao_cores         	:= :new.variacao_cores;          	       v_cod_atributo_03              := :new.cod_atributo_03;
  v_tamanho_filme          	:= :new.tamanho_filme;           	       v_cod_atributo_04              := :new.cod_atributo_04;
  v_raportagem             	:= :new.raportagem;              	       v_cod_atributo_05              := :new.cod_atributo_05;
  v_percent_perda_1        	:= :new.percent_perda_1;         	       v_cod_atributo_06              := :new.cod_atributo_06;
  v_percent_perda_2        	:= :new.percent_perda_2;         	       v_cod_atributo_07              := :new.cod_atributo_07;
  v_percent_perda_3        	:= :new.percent_perda_3;         	       v_cod_atributo_08              := :new.cod_atributo_08;
  v_aproveit_medio         	:= :new.aproveit_medio;         	       v_cod_atributo_09              := :new.cod_atributo_09;
  v_metros_lineares        	:= :new.metros_lineares;         	       v_cod_atributo_10              := :new.cod_atributo_10;
  v_indice_reajuste        	:= :new.indice_reajuste;         	       v_referencia_agrupador         := :new.referencia_agrupador;
  v_item_baixa_estq        	:= :new.item_baixa_estq;         	       v_maquina_piloto               := :new.maquina_piloto;
  v_demanda                	:= :new.demanda;                 	       v_finura_piloto                := :new.finura_piloto;
  v_pontos_por_cm          	:= :new.pontos_por_cm;           	       v_graduacao_piloto             := :new.graduacao_piloto;
  v_observacao             	:= :new.observacao;              	       v_programa_piloto              := :new.programa_piloto;
  v_agulhas_falhadas       	:= :new.agulhas_falhadas;        	       v_programador_piloto           := :new.programador_piloto;
  v_formacao_agulhas       	:= :new.formacao_agulhas;        	       v_peso_bruto_piloto            := :new.peso_bruto_piloto;
  v_aberto_tubular         	:= :new.aberto_tubular;          	       v_peso_acabamento_piloto       := :new.peso_acabamento_piloto;
  v_numero_roteiro         	:= :new.numero_roteiro;          	       v_tecelagem_piloto             := :new.tecelagem_piloto;
  v_ref_original           	:= :new.ref_original;            	       v_faccao_piloto                := :new.faccao_piloto;
  v_numero_molde           	:= :new.numero_molde;            	       v_pers_bordado_piloto          := :new.pers_bordado_piloto;
  v_div_prod               	:= :new.div_prod;                	       v_pers_lavacao_piloto          := :new.pers_lavacao_piloto;
  v_cor_de_producao        	:= :new.cor_de_producao;         	       v_pers_serigrafia_piloto       := :new.pers_serigrafia_piloto;
  v_cgc_cliente_2          	:= :new.cgc_cliente_2;           	       v_personalizacao_piloto        := :new.personalizacao_piloto;
  v_cgc_cliente_4          	:= :new.cgc_cliente_4;           	       v_grupo_tecido_corte           := :new.grupo_tecido_corte;
  v_cgc_cliente_9          	:= :new.cgc_cliente_9;           	       v_data_mov_cilindro            := :new.data_mov_cilindro;
  v_perc_composicao1       	:= :new.perc_composicao1;        	       v_serie_tamanho                := :new.serie_tamanho;
  v_perc_composicao2       	:= :new.perc_composicao2;        	       v_numero_pontos_laser          := :new.numero_pontos_laser;
  v_perc_composicao3       	:= :new.perc_composicao3;        	       v_percentual_amaciante         := :new.percentual_amaciante;
  v_perc_composicao4       	:= :new.perc_composicao4;        	       v_simbolo_1                    := :new.simbolo_1;
  v_perc_composicao5       	:= :new.perc_composicao5;        	       v_simbolo_2                    := :new.simbolo_2;
  v_estagio_altera_programado   := :new.estagio_altera_programado; 	       v_simbolo_3                    := :new.simbolo_3;
  v_risco_padrao               	:= :new.risco_padrao;              	       v_simbolo_4                    := :new.simbolo_4;
  v_responsavel                	:= :new.responsavel;               	       v_simbolo_5                    := :new.simbolo_5;
  v_multiplicador              	:= :new.multiplicador;             	       v_lote_estoque                 := :new.lote_estoque;
  v_num_sol_desenv             	:= :new.num_sol_desenv;            	       v_dias_maturacao               := :new.dias_maturacao;
  v_sortimento_diversos        	:= :new.sortimento_diversos;       	       v_cod_bar_por_cli              := :new.cod_bar_por_cli;
  v_grupo_agrupador            	:= :new.grupo_agrupador;           	       v_familia_atributo             := :new.familia_atributo;
  v_sub_agrupador              	:= :new.sub_agrupador;             	       v_doc_referencial              := :new.doc_referencial;
  v_cod_embalagem              	:= :new.cod_embalagem;             	       v_ligamento                    := :new.ligamento;
  v_conf_altera_prog           	:= :new.conf_altera_prog;
  v_observacao2                	:= :new.observacao2;               	       v_perc_composicao6             := :new.perc_composicao6;
  v_observacao3                	:= :new.observacao3;               	       v_perc_composicao7             := :new.perc_composicao7;
  v_publico_alvo                := :new.publico_alvo;              	       v_perc_composicao8             := :new.perc_composicao8;
  v_cor_de_sortido              := :new.cor_de_sortido;            	       v_perc_composicao9             := :new.perc_composicao9;
  v_tipo_codigo_ean             := :new.tipo_codigo_ean;           	       v_perc_composicao10            := :new.perc_composicao10;
  v_num_programa                := :new.num_programa;              	       v_composicao_06                := :new.composicao_06;
  v_referencia_desenv           := :new.referencia_desenv;         	       v_composicao_07                := :new.composicao_07;
  v_tab_rendimento              := :new.tab_rendimento;            	       v_composicao_08                := :new.composicao_08;
  v_largura_cilindro            := :new.largura_cilindro;          	       v_composicao_09                := :new.composicao_09;
  v_largura_estampa             := :new.largura_estampa;           	       v_composicao_10                := :new.composicao_10;
  v_descricao_tag1              := :new.descricao_tag1;            	       v_simbolo_6                    := :new.simbolo_6;
  v_descricao_tag2              := :new.descricao_tag2;            	       v_simbolo_7                    := :new.simbolo_7;
  v_descr_complementar          := :new.descr_complementar;        	       v_simbolo_8                    := :new.simbolo_8;
  v_pasta_banho                 := :new.pasta_banho;               	       v_simbolo_9                    := :new.simbolo_9;
  v_perimetro_corte             := :new.perimetro_corte;           	       v_simbolo_10                   := :new.simbolo_10;
  v_produto_prototipo           := :new.produto_prototipo;         	       v_parte_composicao_06          := :new.parte_composicao_06;
  v_tempo_laser                 := :new.tempo_laser;               	       v_parte_composicao_07          := :new.parte_composicao_07;
  v_colecao_cliente             := :new.colecao_cliente;           	       v_parte_composicao_08          := :new.parte_composicao_08;
  v_descr_ingles    	        	:= :new.descr_ingles;    		               v_parte_composicao_09          := :new.parte_composicao_09;
  v_descr_espanhol  	         	:= :new.descr_espanhol; 		               v_parte_composicao_10          := :new.parte_composicao_10;
  v_tamanho_amostra 		        := :new.tamanho_amostra;


  if inserting or updating
  then

      if inserting
      then v_tipo_operacao := 'I';
      else v_tipo_operacao := 'A';
      end if;

      if v_nivel_estrutura  = '2'  and
         ((v_tipo_operacao  = 'I') or
          (v_tipo_operacao  = 'A'  and
         (:old.nivel_estrutura  <> :new.nivel_estrutura  or
          :old.referencia       <> :new.referencia      or
          :old.descr_referencia <> :new.descr_referencia or
          :old.unidade_medida   <> :new.unidade_medida   or
          :old.classific_fiscal <> :new.classific_fiscal or
          :old.tipo_produto     <> :new.tipo_produto)))
      then

      begin

      insert into e_basi_030
      (                          	peso_amostra,
       nivel_estrutura,         	comp_serra_fita,
       referencia,              	parte_composicao_01,
       descr_referencia,        	parte_composicao_02,
       unidade_medida,          	parte_composicao_03,
       colecao,                 	parte_composicao_04,
       conta_estoque,           	parte_composicao_05,
       linha_produto,           	composicao_01,
       artigo,                  	composicao_02,
       artigo_cotas,            	composicao_03,
       tipo_produto,            	composicao_04,
       classific_fiscal,        	composicao_05,
       comprado_fabric,         	composicao_01_ingles,
       cor_de_estoque,          	composicao_02_ingles,
       tipo_estampa,            	composicao_03_ingles,
       codigo_contabil,         	composicao_04_ingles,
       data_importacao,             	composicao_05_ingles,
                                        imp_inf_empresa_tag,
                                        estampa_impressa,
       perc_2_qualidade,       		cod_tipo_aviamento,
       artigo_aproveit,        		cod_atributo_01,
       percent_desconto,       		cod_atributo_02,
       variacao_cores,         		cod_atributo_03,
       tamanho_filme,          		cod_atributo_04,
       raportagem,             		cod_atributo_05,
       percent_perda_1,        		cod_atributo_06,
       percent_perda_2,        		cod_atributo_07,
       percent_perda_3,        		cod_atributo_08,
       aproveit_medio,         		cod_atributo_09,
       metros_lineares,        		cod_atributo_10,
       indice_reajuste,        		referencia_agrupador,
       item_baixa_estq,        		maquina_piloto,
       demanda,                		finura_piloto,
       pontos_por_cm,          		graduacao_piloto,
       observacao,             		programa_piloto,
       agulhas_falhadas,       		programador_piloto,
       formacao_agulhas,       		peso_bruto_piloto,
       aberto_tubular,         		peso_acabamento_piloto,
       numero_roteiro,         		tecelagem_piloto,
       ref_original,           		faccao_piloto,
       numero_molde,           		pers_bordado_piloto,
       div_prod,               		pers_lavacao_piloto,
       cor_de_producao,        		pers_serigrafia_piloto,
       cgc_cliente_2,          		personalizacao_piloto,
       cgc_cliente_4,          		grupo_tecido_corte,
       cgc_cliente_9,          		data_mov_cilindro,
       perc_composicao1,       		serie_tamanho,
       perc_composicao2,       		numero_pontos_laser,
       perc_composicao3,       		percentual_amaciante,
       perc_composicao4,      		simbolo_1,
       perc_composicao5,       		simbolo_2,
       estagio_altera_programado,   	simbolo_3,
       risco_padrao,               	simbolo_4,
       responsavel,                	simbolo_5,
       multiplicador,              	lote_estoque,
       num_sol_desenv,             	dias_maturacao,
       sortimento_diversos,        	cod_bar_por_cli,
       grupo_agrupador,            	familia_atributo,
       sub_agrupador,              	doc_referencial,
       cod_embalagem,              	ligamento,
       conf_altera_prog,
       observacao2,                	perc_composicao6,
       observacao3,                	perc_composicao7,
       publico_alvo,                	perc_composicao8,
       cor_de_sortido,              	perc_composicao9,
       tipo_codigo_ean,             	perc_composicao10,
       num_programa,                	composicao_06,
       referencia_desenv,           	composicao_07,
       tab_rendimento,              	composicao_08,
       largura_cilindro,            	composicao_09,
       largura_estampa,             	composicao_10,
       descricao_tag1,              	simbolo_6,
       descricao_tag2,              	simbolo_7,
       descr_complementar,          	simbolo_8,
       pasta_banho,                 	simbolo_9,
       perimetro_corte,             	simbolo_10,
       produto_prototipo,           	parte_composicao_06,
       tempo_laser,                 	parte_composicao_07,
       colecao_cliente,             	parte_composicao_08,
       descr_ingles,    		parte_composicao_09,
       descr_espanhol,  		parte_composicao_10,
       tamanho_amostra, 		tipo_operacao,
       usuario_operacao,                flag_integracao)
       VALUES
       (                           	v_peso_amostra,
        v_nivel_estrutura,         	v_comp_serra_fita,
	v_referencia,              	v_parte_composicao_01,
	v_descr_referencia,        	v_parte_composicao_02,
	v_unidade_medida,          	v_parte_composicao_03,
	v_colecao,                 	v_parte_composicao_04,
	v_conta_estoque,           	v_parte_composicao_05,
	v_linha_produto,           	v_composicao_01,
	v_artigo,                  	v_composicao_02,
	v_artigo_cotas,            	v_composicao_03,
	v_tipo_produto,            	v_composicao_04,
	v_classific_fiscal,        	v_composicao_05,
	v_comprado_fabric,         	v_composicao_01_ingles,
	v_cor_de_estoque,          	v_composicao_02_ingles,
	v_tipo_estampa,            	v_composicao_03_ingles,
	v_codigo_contabil,         	v_composicao_04_ingles,
	v_data_importacao,          v_composicao_05_ingles,
	v_imp_inf_empresa_tag,
	v_estampa_impressa,
	v_perc_2_qualidade,       	v_cod_tipo_aviamento,
	v_artigo_aproveit,        	v_cod_atributo_01,
	v_percent_desconto,       	v_cod_atributo_02,
	v_variacao_cores,         	v_cod_atributo_03,
	v_tamanho_filme,          	v_cod_atributo_04,
	v_raportagem,             	v_cod_atributo_05,
	v_percent_perda_1,        	v_cod_atributo_06,
	v_percent_perda_2,        	v_cod_atributo_07,
	v_percent_perda_3,        	v_cod_atributo_08,
	v_aproveit_medio,         	v_cod_atributo_09,
	v_metros_lineares,        	v_cod_atributo_10,
	v_indice_reajuste,        	v_referencia_agrupador,
	v_item_baixa_estq,        	v_maquina_piloto,
	v_demanda,                	v_finura_piloto,
	v_pontos_por_cm,          	v_graduacao_piloto,
	v_observacao,             	v_programa_piloto,
	v_agulhas_falhadas,       	v_programador_piloto,
	v_formacao_agulhas,       	v_peso_bruto_piloto,
	v_aberto_tubular,         	v_peso_acabamento_piloto,
	v_numero_roteiro,         	v_tecelagem_piloto,
	v_ref_original,           	v_faccao_piloto,
	v_numero_molde,           	v_pers_bordado_piloto,
	v_div_prod,               	v_pers_lavacao_piloto,
	v_cor_de_producao,        	v_pers_serigrafia_piloto,
	v_cgc_cliente_2,          	v_personalizacao_piloto,
	v_cgc_cliente_4,          	v_grupo_tecido_corte,
	v_cgc_cliente_9,          	v_data_mov_cilindro,
	v_perc_composicao1,       	v_serie_tamanho,
	v_perc_composicao2,       	v_numero_pontos_laser,
	v_perc_composicao3,       	v_percentual_amaciante,
	v_perc_composicao4,       	v_simbolo_1,
	v_perc_composicao5,       	v_simbolo_2,
	v_estagio_altera_programado,   	v_simbolo_3,
	v_risco_padrao,               	v_simbolo_4,
	v_responsavel,                	v_simbolo_5,
	v_multiplicador,              	v_lote_estoque,
	v_num_sol_desenv,             	v_dias_maturacao,
	v_sortimento_diversos,        	v_cod_bar_por_cli,
	v_grupo_agrupador,            	v_familia_atributo,
	v_sub_agrupador,              	v_doc_referencial,
	v_cod_embalagem,              	v_ligamento,
	v_conf_altera_prog,
	v_observacao2,                	v_perc_composicao6,
	v_observacao3,                	v_perc_composicao7,
	v_publico_alvo,                	v_perc_composicao8,
	v_cor_de_sortido,              	v_perc_composicao9,
	v_tipo_codigo_ean,             	v_perc_composicao10,
	v_num_programa,                	v_composicao_06,
	v_referencia_desenv,           	v_composicao_07,
	v_tab_rendimento,              	v_composicao_08,
	v_largura_cilindro,            	v_composicao_09,
	v_largura_estampa,             	v_composicao_10,
	v_descricao_tag1,              	v_simbolo_6,
	v_descricao_tag2,              	v_simbolo_7,
	v_descr_complementar,          	v_simbolo_8,
	v_pasta_banho,                 	v_simbolo_9,
	v_perimetro_corte,             	v_simbolo_10,
	v_produto_prototipo,           	v_parte_composicao_06,
	v_tempo_laser,                	v_parte_composicao_07,
	v_colecao_cliente,             	v_parte_composicao_08,
	v_descr_ingles,    		v_parte_composicao_09,
	v_descr_espanhol,  		v_parte_composicao_10,
	v_tamanho_amostra, 		v_tipo_operacao,
	ws_usuario_systextil,           1);

        exception
        when OTHERS
           then raise_application_error (-20000, 'N�o atualizou a tabela e_basi_030' || Chr(10) || SQLERRM);
        end;


      if v_tipo_operacao = 'A' and
         :new.nivel_estrutura <> :old.nivel_estrutura or
         :new.referencia      <> :old.referencia
      then
         v_tipo_operacao := 'E';

                                                                                v_peso_amostra                 := :old.peso_amostra;
         v_nivel_estrutura         	:= :old.nivel_estrutura;           	v_comp_serra_fita              := :old.comp_serra_fita;
         v_referencia              	:= :old.referencia;               	v_parte_composicao_01          := :old.parte_composicao_01;
         v_descr_referencia        	:= :old.descr_referencia;         	v_parte_composicao_02          := :old.parte_composicao_02;
         v_unidade_medida          	:= :old.unidade_medida;          	v_parte_composicao_03          := :old.parte_composicao_03;
         v_colecao                 	:= :old.colecao;                  	v_parte_composicao_04          := :old.parte_composicao_04;
         v_conta_estoque           	:= :old.conta_estoque;            	v_parte_composicao_05          := :old.parte_composicao_05;
         v_linha_produto           	:= :old.linha_produto;           	v_composicao_01                := :old.composicao_01;
         v_artigo                  	:= :old.artigo;                   	v_composicao_02                := :old.composicao_02;
         v_artigo_cotas            	:= :old.artigo_cotas;             	v_composicao_03                := :old.composicao_03;
         v_tipo_produto            	:= :old.tipo_produto;             	v_composicao_04                := :old.composicao_04;
         v_classific_fiscal        	:= :old.classific_fiscal;         	v_composicao_05                := :old.composicao_05;
         v_comprado_fabric         	:= :old.comprado_fabric;          	v_composicao_01_ingles         := :old.composicao_01_ingles;
         v_cor_de_estoque          	:= :old.cor_de_estoque;           	v_composicao_02_ingles         := :old.composicao_02_ingles;
         v_tipo_estampa            	:= :old.tipo_estampa;             	v_composicao_03_ingles         := :old.composicao_03_ingles;
         v_codigo_contabil         	:= :old.codigo_contabil;          	v_composicao_04_ingles         := :old.composicao_04_ingles;
         v_data_importacao              := sysdate();           	   	v_composicao_05_ingles         := :old.composicao_05_ingles;
                                                                        	v_imp_inf_empresa_tag          := :old.imp_inf_empresa_tag;
                 	                                                        v_estampa_impressa             := :old.estampa_impressa;
         v_perc_2_qualidade       	:= :old.perc_2_qualidade;        	v_cod_tipo_aviamento           := :old.cod_tipo_aviamento;
         v_artigo_aproveit        	:= :old.artigo_aproveit;         	v_cod_atributo_01              := :old.cod_atributo_01;
         v_percent_desconto       	:= :old.percent_desconto;        	v_cod_atributo_02              := :old.cod_atributo_02;
         v_variacao_cores         	:= :old.variacao_cores;          	v_cod_atributo_03              := :old.cod_atributo_03;
         v_tamanho_filme          	:= :old.tamanho_filme;           	v_cod_atributo_04              := :old.cod_atributo_04;
         v_raportagem             	:= :old.raportagem;              	v_cod_atributo_05              := :old.cod_atributo_05;
         v_percent_perda_1        	:= :old.percent_perda_1;         	v_cod_atributo_06              := :old.cod_atributo_06;
         v_percent_perda_2        	:= :old.percent_perda_2;         	v_cod_atributo_07              := :old.cod_atributo_07;
         v_percent_perda_3        	:= :old.percent_perda_3;         	v_cod_atributo_08              := :old.cod_atributo_08;
         v_aproveit_medio         	:= :old.aproveit_medio;         	v_cod_atributo_09              := :old.cod_atributo_09;
         v_metros_lineares        	:= :old.metros_lineares;         	v_cod_atributo_10              := :old.cod_atributo_10;
         v_indice_reajuste        	:= :old.indice_reajuste;         	v_referencia_agrupador         := :old.referencia_agrupador;
         v_item_baixa_estq        	:= :old.item_baixa_estq;         	v_maquina_piloto               := :old.maquina_piloto;
         v_demanda                	:= :old.demanda;                 	v_finura_piloto                := :old.finura_piloto;
         v_pontos_por_cm          	:= :old.pontos_por_cm;           	v_graduacao_piloto             := :old.graduacao_piloto;
         v_observacao             	:= :old.observacao;              	v_programa_piloto              := :old.programa_piloto;
         v_agulhas_falhadas       	:= :old.agulhas_falhadas;        	v_programador_piloto           := :old.programador_piloto;
         v_formacao_agulhas       	:= :old.formacao_agulhas;        	v_peso_bruto_piloto            := :old.peso_bruto_piloto;
         v_aberto_tubular         	:= :old.aberto_tubular;          	v_peso_acabamento_piloto       := :old.peso_acabamento_piloto;
         v_numero_roteiro         	:= :old.numero_roteiro;          	v_tecelagem_piloto             := :old.tecelagem_piloto;
         v_ref_original           	:= :old.ref_original;            	v_faccao_piloto                := :old.faccao_piloto;
         v_numero_molde           	:= :old.numero_molde;            	v_pers_bordado_piloto          := :old.pers_bordado_piloto;
         v_div_prod               	:= :old.div_prod;                	v_pers_lavacao_piloto          := :old.pers_lavacao_piloto;
         v_cor_de_producao        	:= :old.cor_de_producao;         	v_pers_serigrafia_piloto       := :old.pers_serigrafia_piloto;
         v_cgc_cliente_2          	:= :old.cgc_cliente_2;           	v_personalizacao_piloto        := :old.personalizacao_piloto;
         v_cgc_cliente_4          	:= :old.cgc_cliente_4;           	v_grupo_tecido_corte           := :old.grupo_tecido_corte;
         v_cgc_cliente_9          	:= :old.cgc_cliente_9;           	v_data_mov_cilindro            := :old.data_mov_cilindro;
         v_perc_composicao1       	:= :old.perc_composicao1;        	v_serie_tamanho                := :old.serie_tamanho;
         v_perc_composicao2       	:= :old.perc_composicao2;        	v_numero_pontos_laser          := :old.numero_pontos_laser;
         v_perc_composicao3       	:= :old.perc_composicao3;        	v_percentual_amaciante         := :old.percentual_amaciante;
         v_perc_composicao4       	:= :old.perc_composicao4;        	v_simbolo_1                    := :old.simbolo_1;
         v_perc_composicao5       	:= :old.perc_composicao5;        	v_simbolo_2                    := :old.simbolo_2;
         v_estagio_altera_programado    := :old.estagio_altera_programado; 	v_simbolo_3                    := :old.simbolo_3;
         v_risco_padrao               	:= :old.risco_padrao;              	v_simbolo_4                    := :old.simbolo_4;
         v_responsavel                	:= :old.responsavel;               	v_simbolo_5                    := :old.simbolo_5;
         v_multiplicador              	:= :old.multiplicador;             	v_lote_estoque                 := :old.lote_estoque;
         v_num_sol_desenv             	:= :old.num_sol_desenv;            	v_dias_maturacao               := :old.dias_maturacao;
         v_sortimento_diversos        	:= :old.sortimento_diversos;       	v_cod_bar_por_cli              := :old.cod_bar_por_cli;
         v_grupo_agrupador            	:= :old.grupo_agrupador;           	v_familia_atributo             := :old.familia_atributo;
         v_sub_agrupador              	:= :old.sub_agrupador;             	v_doc_referencial              := :old.doc_referencial;
         v_cod_embalagem              	:= :old.cod_embalagem;             	v_ligamento                    := :old.ligamento;
         v_conf_altera_prog           	:= :old.conf_altera_prog;
         v_observacao2                	:= :old.observacao2;               	v_perc_composicao6             := :old.perc_composicao6;
         v_observacao3                	:= :old.observacao3;               	v_perc_composicao7             := :old.perc_composicao7;
         v_publico_alvo                 := :old.publico_alvo;              	v_perc_composicao8             := :old.perc_composicao8;
         v_cor_de_sortido               := :old.cor_de_sortido;            	v_perc_composicao9             := :old.perc_composicao9;
         v_tipo_codigo_ean              := :old.tipo_codigo_ean;           	v_perc_composicao10            := :old.perc_composicao10;
         v_num_programa                 := :old.num_programa;              	v_composicao_06                := :old.composicao_06;
         v_referencia_desenv            := :old.referencia_desenv;         	v_composicao_07                := :old.composicao_07;
         v_tab_rendimento               := :old.tab_rendimento;            	v_composicao_08                := :old.composicao_08;
         v_largura_cilindro             := :old.largura_cilindro;          	v_composicao_09                := :old.composicao_09;
         v_largura_estampa              := :old.largura_estampa;           	v_composicao_10                := :old.composicao_10;
         v_descricao_tag1               := :old.descricao_tag1;            	v_simbolo_6                    := :old.simbolo_6;
         v_descricao_tag2               := :old.descricao_tag2;            	v_simbolo_7                    := :old.simbolo_7;
         v_descr_complementar           := :old.descr_complementar;        	v_simbolo_8                    := :old.simbolo_8;
         v_pasta_banho                  := :old.pasta_banho;               	v_simbolo_9                    := :old.simbolo_9;
         v_perimetro_corte              := :old.perimetro_corte;           	v_simbolo_10                   := :old.simbolo_10;
         v_produto_prototipo            := :old.produto_prototipo;         	v_parte_composicao_06          := :old.parte_composicao_06;
         v_tempo_laser                  := :old.tempo_laser;               	v_parte_composicao_07          := :old.parte_composicao_07;
         v_colecao_cliente              := :old.colecao_cliente;           	v_parte_composicao_08          := :old.parte_composicao_08;
         v_descr_ingles    		:= :old.descr_ingles;    		v_parte_composicao_09          := :old.parte_composicao_09;
         v_descr_espanhol  		:= :old.descr_espanhol; 		v_parte_composicao_10          := :old.parte_composicao_10;
         v_tamanho_amostra 		:= :old.tamanho_amostra;


         begin

      	    insert into e_basi_030
            (                          	   peso_amostra,
      	     nivel_estrutura,         	   comp_serra_fita,
             referencia,              	   parte_composicao_01,
             descr_referencia,        	   parte_composicao_02,
             unidade_medida,          	   parte_composicao_03,
      	     colecao,                 	   parte_composicao_04,
      	     conta_estoque,           	   parte_composicao_05,
      	     linha_produto,           	   composicao_01,
      	     artigo,                  	   composicao_02,
      	     artigo_cotas,            	   composicao_03,
      	     tipo_produto,            	   composicao_04,
      	     classific_fiscal,        	   composicao_05,
      	     comprado_fabric,         	   composicao_01_ingles,
      	     cor_de_estoque,          	   composicao_02_ingles,
      	     tipo_estampa,            	   composicao_03_ingles,
      	     codigo_contabil,         	   composicao_04_ingles,
      	     data_importacao,              composicao_05_ingles,
      	                                   imp_inf_empresa_tag,
      	                                   estampa_impressa,
      	     perc_2_qualidade,       	   cod_tipo_aviamento,
      	     artigo_aproveit,        	   cod_atributo_01,
      	     percent_desconto,       	   cod_atributo_02,
      	     variacao_cores,         	   cod_atributo_03,
      	     tamanho_filme,          	   cod_atributo_04,
      	     raportagem,             	   cod_atributo_05,
      	     percent_perda_1,        	   cod_atributo_06,
      	     percent_perda_2,        	   cod_atributo_07,
      	     percent_perda_3,        	   cod_atributo_08,
      	     aproveit_medio,         	   cod_atributo_09,
      	     metros_lineares,        	   cod_atributo_10,
      	     indice_reajuste,        	   referencia_agrupador,
      	     item_baixa_estq,        	   maquina_piloto,
      	     demanda,                	   finura_piloto,
      	     pontos_por_cm,          	   graduacao_piloto,
      	     observacao,             	   programa_piloto,
      	     agulhas_falhadas,       	   programador_piloto,
      	     formacao_agulhas,       	   peso_bruto_piloto,
      	     aberto_tubular,         	   peso_acabamento_piloto,
      	     numero_roteiro,         	   tecelagem_piloto,
      	     ref_original,           	   faccao_piloto,
      	     numero_molde,           	   pers_bordado_piloto,
      	     div_prod,               	   pers_lavacao_piloto,
      	     cor_de_producao,        	   pers_serigrafia_piloto,
      	     cgc_cliente_2,          	   personalizacao_piloto,
      	     cgc_cliente_4,          	   grupo_tecido_corte,
      	     cgc_cliente_9,          	   data_mov_cilindro,
      	     perc_composicao1,       	   serie_tamanho,
      	     perc_composicao2,       	   numero_pontos_laser,
      	     perc_composicao3,       	   percentual_amaciante,
      	     perc_composicao4,      	   simbolo_1,
      	     perc_composicao5,       	   simbolo_2,
      	     estagio_altera_programado,    simbolo_3,
      	     risco_padrao,                 simbolo_4,
      	     responsavel,                  simbolo_5,
      	     multiplicador,                lote_estoque,
      	     num_sol_desenv,               dias_maturacao,
      	     sortimento_diversos,          cod_bar_por_cli,
      	     grupo_agrupador,              familia_atributo,
      	     sub_agrupador,                doc_referencial,
      	     cod_embalagem,                ligamento,
      	     conf_altera_prog,
      	     observacao2,                  perc_composicao6,
      	     observacao3,                  perc_composicao7,
      	     publico_alvo,                 perc_composicao8,
      	     cor_de_sortido,               perc_composicao9,
      	     tipo_codigo_ean,              perc_composicao10,
      	     num_programa,                 composicao_06,
      	     referencia_desenv,            composicao_07,
      	     tab_rendimento,               composicao_08,
      	     largura_cilindro,             composicao_09,
      	     largura_estampa,              composicao_10,
      	     descricao_tag1,               simbolo_6,
      	     descricao_tag2,               simbolo_7,
      	     descr_complementar,           simbolo_8,
      	     pasta_banho,                  simbolo_9,
      	     perimetro_corte,              simbolo_10,
      	     produto_prototipo,            parte_composicao_06,
      	     tempo_laser,                  parte_composicao_07,
      	     colecao_cliente,              parte_composicao_08,
      	     descr_ingles,    		   parte_composicao_09,
      	     descr_espanhol,  		   parte_composicao_10,
      	     tamanho_amostra, 		   tipo_operacao,
      	     usuario_operacao,             flag_integracao)
      	   VALUES
      	   (                               v_peso_amostra,
      	     v_nivel_estrutura,            v_comp_serra_fita,
      	     v_referencia,                 v_parte_composicao_01,
      	     v_descr_referencia,           v_parte_composicao_02,
      	     v_unidade_medida,             v_parte_composicao_03,
      	     v_colecao,                    v_parte_composicao_04,
      	     v_conta_estoque,              v_parte_composicao_05,
      	     v_linha_produto,              v_composicao_01,
      	     v_artigo,                     v_composicao_02,
      	     v_artigo_cotas,               v_composicao_03,
      	     v_tipo_produto,               v_composicao_04,
      	     v_classific_fiscal,           v_composicao_05,
      	     v_comprado_fabric,            v_composicao_01_ingles,
      	     v_cor_de_estoque,             v_composicao_02_ingles,
      	     v_tipo_estampa,               v_composicao_03_ingles,
      	     v_codigo_contabil,            v_composicao_04_ingles,
      	     v_data_importacao,            v_composicao_05_ingles,
         	                           v_imp_inf_empresa_tag,
        	                           v_estampa_impressa,
      	     v_perc_2_qualidade,       	   v_cod_tipo_aviamento,
      	     v_artigo_aproveit,        	   v_cod_atributo_01,
      	     v_percent_desconto,       	   v_cod_atributo_02,
      	     v_variacao_cores,         	   v_cod_atributo_03,
      	     v_tamanho_filme,          	   v_cod_atributo_04,
      	     v_raportagem,                 v_cod_atributo_05,
      	     v_percent_perda_1,        	   v_cod_atributo_06,
      	     v_percent_perda_2,        	   v_cod_atributo_07,
      	     v_percent_perda_3,            v_cod_atributo_08,
      	     v_aproveit_medio,         	   v_cod_atributo_09,
      	     v_metros_lineares,        	   v_cod_atributo_10,
      	     v_indice_reajuste,        	   v_referencia_agrupador,
      	     v_item_baixa_estq,        	   v_maquina_piloto,
      	     v_demanda,                	   v_finura_piloto,
      	     v_pontos_por_cm,          	   v_graduacao_piloto,
      	     v_observacao,             	   v_programa_piloto,
      	     v_agulhas_falhadas,       	   v_programador_piloto,
      	     v_formacao_agulhas,       	   v_peso_bruto_piloto,
      	     v_aberto_tubular,         	   v_peso_acabamento_piloto,
      	     v_numero_roteiro,         	   v_tecelagem_piloto,
      	     v_ref_original,           	   v_faccao_piloto,
      	     v_numero_molde,           	   v_pers_bordado_piloto,
      	     v_div_prod,               	   v_pers_lavacao_piloto,
      	     v_cor_de_producao,        	   v_pers_serigrafia_piloto,
      	     v_cgc_cliente_2,          	   v_personalizacao_piloto,
      	     v_cgc_cliente_4,          	   v_grupo_tecido_corte,
      	     v_cgc_cliente_9,          	   v_data_mov_cilindro,
      	     v_perc_composicao1,       	   v_serie_tamanho,
      	     v_perc_composicao2,       	   v_numero_pontos_laser,
      	     v_perc_composicao3,           v_percentual_amaciante,
      	     v_perc_composicao4,       	   v_simbolo_1,
      	     v_perc_composicao5,       	   v_simbolo_2,
      	     v_estagio_altera_programado,  v_simbolo_3,
      	     v_risco_padrao,               v_simbolo_4,
      	     v_responsavel,                v_simbolo_5,
      	     v_multiplicador,              v_lote_estoque,
      	     v_num_sol_desenv,             v_dias_maturacao,
      	     v_sortimento_diversos,        v_cod_bar_por_cli,
      	     v_grupo_agrupador,            v_familia_atributo,
      	     v_sub_agrupador,              v_doc_referencial,
      	     v_cod_embalagem,              v_ligamento,
      	     v_conf_altera_prog,
      	     v_observacao2,                v_perc_composicao6,
      	     v_observacao3,                v_perc_composicao7,
      	     v_publico_alvo,               v_perc_composicao8,
      	     v_cor_de_sortido,             v_perc_composicao9,
      	     v_tipo_codigo_ean,            v_perc_composicao10,
      	     v_num_programa,               v_composicao_06,
      	     v_referencia_desenv,          v_composicao_07,
      	     v_tab_rendimento,             v_composicao_08,
      	     v_largura_cilindro,           v_composicao_09,
      	     v_largura_estampa,            v_composicao_10,
      	     v_descricao_tag1,             v_simbolo_6,
      	     v_descricao_tag2,             v_simbolo_7,
      	     v_descr_complementar,         v_simbolo_8,
      	     v_pasta_banho,                v_simbolo_9,
      	     v_perimetro_corte,            v_simbolo_10,
      	     v_produto_prototipo,          v_parte_composicao_06,
      	     v_tempo_laser,                v_parte_composicao_07,
      	     v_colecao_cliente,            v_parte_composicao_08,
      	     v_descr_ingles,    	   v_parte_composicao_09,
      	     v_descr_espanhol,  	   v_parte_composicao_10,
      	     v_tamanho_amostra, 	   v_tipo_operacao,
      	     ws_usuario_systextil,         1);

          exception
             when OTHERS
             then raise_application_error (-20000, 'N�o atualizou a tabela e_basi_030' || Chr(10) || SQLERRM);
          end;
       end if;
    end if;
 end if;


  if deleting
  then
        v_tipo_operacao := 'E';

                                                                                v_peso_amostra                 := :old.peso_amostra;
        v_nivel_estrutura         	:= :old.nivel_estrutura;           	v_comp_serra_fita              := :old.comp_serra_fita;
        v_referencia              	:= :old.referencia;               	v_parte_composicao_01          := :old.parte_composicao_01;
        v_descr_referencia        	:= :old.descr_referencia;         	v_parte_composicao_02          := :old.parte_composicao_02;
        v_unidade_medida          	:= :old.unidade_medida;          	v_parte_composicao_03          := :old.parte_composicao_03;
        v_colecao                 	:= :old.colecao;                  	v_parte_composicao_04          := :old.parte_composicao_04;
        v_conta_estoque           	:= :old.conta_estoque;            	v_parte_composicao_05          := :old.parte_composicao_05;
        v_linha_produto           	:= :old.linha_produto;           	v_composicao_01                := :old.composicao_01;
        v_artigo                  	:= :old.artigo;                   	v_composicao_02                := :old.composicao_02;
        v_artigo_cotas            	:= :old.artigo_cotas;             	v_composicao_03                := :old.composicao_03;
        v_tipo_produto            	:= :old.tipo_produto;             	v_composicao_04                := :old.composicao_04;
        v_classific_fiscal        	:= :old.classific_fiscal;         	v_composicao_05                := :old.composicao_05;
        v_comprado_fabric         	:= :old.comprado_fabric;          	v_composicao_01_ingles         := :old.composicao_01_ingles;
        v_cor_de_estoque          	:= :old.cor_de_estoque;           	v_composicao_02_ingles         := :old.composicao_02_ingles;
        v_tipo_estampa            	:= :old.tipo_estampa;             	v_composicao_03_ingles         := :old.composicao_03_ingles;
        v_codigo_contabil         	:= :old.codigo_contabil;          	v_composicao_04_ingles         := :old.composicao_04_ingles;
        v_data_importacao               := sysdate();           	   	v_composicao_05_ingles         := :old.composicao_05_ingles;
                                                                        	v_imp_inf_empresa_tag          := :old.imp_inf_empresa_tag;
                 	                                                        v_estampa_impressa             := :old.estampa_impressa;
        v_perc_2_qualidade       	:= :old.perc_2_qualidade;        	v_cod_tipo_aviamento           := :old.cod_tipo_aviamento;
        v_artigo_aproveit        	:= :old.artigo_aproveit;         	v_cod_atributo_01              := :old.cod_atributo_01;
        v_percent_desconto       	:= :old.percent_desconto;        	v_cod_atributo_02              := :old.cod_atributo_02;
        v_variacao_cores         	:= :old.variacao_cores;          	v_cod_atributo_03              := :old.cod_atributo_03;
        v_tamanho_filme          	:= :old.tamanho_filme;           	v_cod_atributo_04              := :old.cod_atributo_04;
        v_raportagem             	:= :old.raportagem;              	v_cod_atributo_05              := :old.cod_atributo_05;
        v_percent_perda_1        	:= :old.percent_perda_1;         	v_cod_atributo_06              := :old.cod_atributo_06;
        v_percent_perda_2        	:= :old.percent_perda_2;         	v_cod_atributo_07              := :old.cod_atributo_07;
        v_percent_perda_3        	:= :old.percent_perda_3;         	v_cod_atributo_08              := :old.cod_atributo_08;
        v_aproveit_medio         	:= :old.aproveit_medio;         	v_cod_atributo_09              := :old.cod_atributo_09;
        v_metros_lineares        	:= :old.metros_lineares;         	v_cod_atributo_10              := :old.cod_atributo_10;
        v_indice_reajuste        	:= :old.indice_reajuste;         	v_referencia_agrupador         := :old.referencia_agrupador;
        v_item_baixa_estq        	:= :old.item_baixa_estq;         	v_maquina_piloto               := :old.maquina_piloto;
        v_demanda                	:= :old.demanda;                 	v_finura_piloto                := :old.finura_piloto;
        v_pontos_por_cm          	:= :old.pontos_por_cm;           	v_graduacao_piloto             := :old.graduacao_piloto;
        v_observacao             	:= :old.observacao;              	v_programa_piloto              := :old.programa_piloto;
        v_agulhas_falhadas       	:= :old.agulhas_falhadas;        	v_programador_piloto           := :old.programador_piloto;
        v_formacao_agulhas       	:= :old.formacao_agulhas;        	v_peso_bruto_piloto            := :old.peso_bruto_piloto;
        v_aberto_tubular         	:= :old.aberto_tubular;          	v_peso_acabamento_piloto       := :old.peso_acabamento_piloto;
        v_numero_roteiro         	:= :old.numero_roteiro;          	v_tecelagem_piloto             := :old.tecelagem_piloto;
        v_ref_original           	:= :old.ref_original;            	v_faccao_piloto                := :old.faccao_piloto;
        v_numero_molde           	:= :old.numero_molde;            	v_pers_bordado_piloto          := :old.pers_bordado_piloto;
        v_div_prod               	:= :old.div_prod;                	v_pers_lavacao_piloto          := :old.pers_lavacao_piloto;
        v_cor_de_producao        	:= :old.cor_de_producao;         	v_pers_serigrafia_piloto       := :old.pers_serigrafia_piloto;
        v_cgc_cliente_2          	:= :old.cgc_cliente_2;           	v_personalizacao_piloto        := :old.personalizacao_piloto;
        v_cgc_cliente_4          	:= :old.cgc_cliente_4;           	v_grupo_tecido_corte           := :old.grupo_tecido_corte;
        v_cgc_cliente_9          	:= :old.cgc_cliente_9;           	v_data_mov_cilindro            := :old.data_mov_cilindro;
        v_perc_composicao1       	:= :old.perc_composicao1;        	v_serie_tamanho                := :old.serie_tamanho;
        v_perc_composicao2       	:= :old.perc_composicao2;        	v_numero_pontos_laser          := :old.numero_pontos_laser;
        v_perc_composicao3       	:= :old.perc_composicao3;        	v_percentual_amaciante         := :old.percentual_amaciante;
        v_perc_composicao4       	:= :old.perc_composicao4;        	v_simbolo_1                    := :old.simbolo_1;
        v_perc_composicao5       	:= :old.perc_composicao5;        	v_simbolo_2                    := :old.simbolo_2;
        v_estagio_altera_programado     := :old.estagio_altera_programado; 	v_simbolo_3                    := :old.simbolo_3;
        v_risco_padrao               	:= :old.risco_padrao;              	v_simbolo_4                    := :old.simbolo_4;
        v_responsavel                	:= :old.responsavel;               	v_simbolo_5                    := :old.simbolo_5;
        v_multiplicador              	:= :old.multiplicador;             	v_lote_estoque                 := :old.lote_estoque;
        v_num_sol_desenv             	:= :old.num_sol_desenv;            	v_dias_maturacao               := :old.dias_maturacao;
        v_sortimento_diversos        	:= :old.sortimento_diversos;       	v_cod_bar_por_cli              := :old.cod_bar_por_cli;
        v_grupo_agrupador            	:= :old.grupo_agrupador;           	v_familia_atributo             := :old.familia_atributo;
        v_sub_agrupador              	:= :old.sub_agrupador;             	v_doc_referencial              := :old.doc_referencial;
        v_cod_embalagem              	:= :old.cod_embalagem;             	v_ligamento                    := :old.ligamento;
        v_conf_altera_prog           	:= :old.conf_altera_prog;
        v_observacao2                	:= :old.observacao2;               	v_perc_composicao6             := :old.perc_composicao6;
        v_observacao3                	:= :old.observacao3;               	v_perc_composicao7             := :old.perc_composicao7;
        v_publico_alvo                  := :old.publico_alvo;              	v_perc_composicao8             := :old.perc_composicao8;
        v_cor_de_sortido                := :old.cor_de_sortido;            	v_perc_composicao9             := :old.perc_composicao9;
        v_tipo_codigo_ean               := :old.tipo_codigo_ean;           	v_perc_composicao10            := :old.perc_composicao10;
        v_num_programa                  := :old.num_programa;              	v_composicao_06                := :old.composicao_06;
        v_referencia_desenv             := :old.referencia_desenv;         	v_composicao_07                := :old.composicao_07;
        v_tab_rendimento                := :old.tab_rendimento;            	v_composicao_08                := :old.composicao_08;
        v_largura_cilindro              := :old.largura_cilindro;          	v_composicao_09                := :old.composicao_09;
        v_largura_estampa               := :old.largura_estampa;           	v_composicao_10                := :old.composicao_10;
        v_descricao_tag1                := :old.descricao_tag1;            	v_simbolo_6                    := :old.simbolo_6;
        v_descricao_tag2                := :old.descricao_tag2;            	v_simbolo_7                    := :old.simbolo_7;
        v_descr_complementar            := :old.descr_complementar;        	v_simbolo_8                    := :old.simbolo_8;
        v_pasta_banho                   := :old.pasta_banho;               	v_simbolo_9                    := :old.simbolo_9;
        v_perimetro_corte               := :old.perimetro_corte;           	v_simbolo_10                   := :old.simbolo_10;
        v_produto_prototipo             := :old.produto_prototipo;         	v_parte_composicao_06          := :old.parte_composicao_06;
        v_tempo_laser                   := :old.tempo_laser;               	v_parte_composicao_07          := :old.parte_composicao_07;
        v_colecao_cliente               := :old.colecao_cliente;           	v_parte_composicao_08          := :old.parte_composicao_08;
        v_descr_ingles    		:= :old.descr_ingles;    		v_parte_composicao_09          := :old.parte_composicao_09;
        v_descr_espanhol  		:= :old.descr_espanhol; 		v_parte_composicao_10          := :old.parte_composicao_10;
      	v_tamanho_amostra 		:= :old.tamanho_amostra;

     if v_nivel_estrutura = '2'
     then
        begin


      	insert into e_basi_030
      	(                          	peso_amostra,
      	 nivel_estrutura,         	comp_serra_fita,
      	 referencia,              	parte_composicao_01,
      	 descr_referencia,        	parte_composicao_02,
      	 unidade_medida,          	parte_composicao_03,
      	 colecao,                 	parte_composicao_04,
      	 conta_estoque,           	parte_composicao_05,
      	 linha_produto,           	composicao_01,
      	 artigo,                  	composicao_02,
      	 artigo_cotas,            	composicao_03,
      	 tipo_produto,            	composicao_04,
      	 classific_fiscal,        	composicao_05,
      	 comprado_fabric,         	composicao_01_ingles,
      	 cor_de_estoque,          	composicao_02_ingles,
      	 tipo_estampa,            	composicao_03_ingles,
      	 codigo_contabil,         	composicao_04_ingles,
      	 data_importacao,             	composicao_05_ingles,
      	                                imp_inf_empresa_tag,
      	                                estampa_impressa,
      	 perc_2_qualidade,       	cod_tipo_aviamento,
      	 artigo_aproveit,        	cod_atributo_01,
      	 percent_desconto,       	cod_atributo_02,
      	 variacao_cores,         	cod_atributo_03,
      	 tamanho_filme,          	cod_atributo_04,
      	 raportagem,             	cod_atributo_05,
      	 percent_perda_1,        	cod_atributo_06,
      	 percent_perda_2,        	cod_atributo_07,
      	 percent_perda_3,        	cod_atributo_08,
      	 aproveit_medio,         	cod_atributo_09,
      	 metros_lineares,        	cod_atributo_10,
      	 indice_reajuste,        	referencia_agrupador,
      	 item_baixa_estq,        	maquina_piloto,
      	 demanda,                	finura_piloto,
      	 pontos_por_cm,          	graduacao_piloto,
      	 observacao,             	programa_piloto,
      	 agulhas_falhadas,       	programador_piloto,
      	 formacao_agulhas,       	peso_bruto_piloto,
      	 aberto_tubular,         	peso_acabamento_piloto,
      	 numero_roteiro,         	tecelagem_piloto,
      	 ref_original,           	faccao_piloto,
      	 numero_molde,           	pers_bordado_piloto,
      	 div_prod,               	pers_lavacao_piloto,
      	 cor_de_producao,        	pers_serigrafia_piloto,
      	 cgc_cliente_2,          	personalizacao_piloto,
      	 cgc_cliente_4,          	grupo_tecido_corte,
      	 cgc_cliente_9,          	data_mov_cilindro,
      	 perc_composicao1,       	serie_tamanho,
      	 perc_composicao2,       	numero_pontos_laser,
      	 perc_composicao3,       	percentual_amaciante,
      	 perc_composicao4,      	simbolo_1,
      	 perc_composicao5,       	simbolo_2,
      	 estagio_altera_programado,   	simbolo_3,
      	 risco_padrao,               	simbolo_4,
      	 responsavel,                	simbolo_5,
      	 multiplicador,              	lote_estoque,
      	 num_sol_desenv,             	dias_maturacao,
      	 sortimento_diversos,        	cod_bar_por_cli,
      	 grupo_agrupador,            	familia_atributo,
      	 sub_agrupador,              	doc_referencial,
      	 cod_embalagem,              	ligamento,
      	 conf_altera_prog,
      	 observacao2,                	perc_composicao6,
      	 observacao3,                	perc_composicao7,
      	 publico_alvo,                	perc_composicao8,
      	 cor_de_sortido,              	perc_composicao9,
      	 tipo_codigo_ean,             	perc_composicao10,
      	 num_programa,                	composicao_06,
      	 referencia_desenv,           	composicao_07,
      	 tab_rendimento,              	composicao_08,
      	 largura_cilindro,            	composicao_09,
      	 largura_estampa,             	composicao_10,
      	 descricao_tag1,              	simbolo_6,
      	 descricao_tag2,              	simbolo_7,
      	 descr_complementar,          	simbolo_8,
      	 pasta_banho,                 	simbolo_9,
      	 perimetro_corte,             	simbolo_10,
      	 produto_prototipo,           	parte_composicao_06,
      	 tempo_laser,                 	parte_composicao_07,
      	 colecao_cliente,             	parte_composicao_08,
      	 descr_ingles,    		parte_composicao_09,
      	 descr_espanhol,  		parte_composicao_10,
      	 tamanho_amostra, 		tipo_operacao,
      	 usuario_operacao,              flag_integracao)
      	 VALUES
      	 (                              v_peso_amostra,
      	  v_nivel_estrutura,         	v_comp_serra_fita,
      	  v_referencia,              	v_parte_composicao_01,
      	  v_descr_referencia,        	v_parte_composicao_02,
      	  v_unidade_medida,          	v_parte_composicao_03,
      	  v_colecao,                 	v_parte_composicao_04,
      	  v_conta_estoque,           	v_parte_composicao_05,
      	  v_linha_produto,           	v_composicao_01,
      	  v_artigo,                  	v_composicao_02,
      	  v_artigo_cotas,            	v_composicao_03,
      	  v_tipo_produto,            	v_composicao_04,
      	  v_classific_fiscal,        	v_composicao_05,
      	  v_comprado_fabric,         	v_composicao_01_ingles,
      	  v_cor_de_estoque,          	v_composicao_02_ingles,
      	  v_tipo_estampa,            	v_composicao_03_ingles,
      	  v_codigo_contabil,         	v_composicao_04_ingles,
      	  v_data_importacao,            v_composicao_05_ingles,
      	                                v_imp_inf_empresa_tag,
      	                                v_estampa_impressa,
      	  v_perc_2_qualidade,       	v_cod_tipo_aviamento,
      	  v_artigo_aproveit,        	v_cod_atributo_01,
      	  v_percent_desconto,       	v_cod_atributo_02,
      	  v_variacao_cores,         	v_cod_atributo_03,
      	  v_tamanho_filme,          	v_cod_atributo_04,
      	  v_raportagem,             	v_cod_atributo_05,
      	  v_percent_perda_1,        	v_cod_atributo_06,
      	  v_percent_perda_2,        	v_cod_atributo_07,
      	  v_percent_perda_3,        	v_cod_atributo_08,
      	  v_aproveit_medio,         	v_cod_atributo_09,
      	  v_metros_lineares,        	v_cod_atributo_10,
      	  v_indice_reajuste,        	v_referencia_agrupador,
      	  v_item_baixa_estq,        	v_maquina_piloto,
      	  v_demanda,                	v_finura_piloto,
      	  v_pontos_por_cm,          	v_graduacao_piloto,
      	  v_observacao,             	v_programa_piloto,
      	  v_agulhas_falhadas,       	v_programador_piloto,
      	  v_formacao_agulhas,       	v_peso_bruto_piloto,
      	  v_aberto_tubular,         	v_peso_acabamento_piloto,
      	  v_numero_roteiro,         	v_tecelagem_piloto,
      	  v_ref_original,           	v_faccao_piloto,
      	  v_numero_molde,           	v_pers_bordado_piloto,
      	  v_div_prod,               	v_pers_lavacao_piloto,
      	  v_cor_de_producao,        	v_pers_serigrafia_piloto,
      	  v_cgc_cliente_2,          	v_personalizacao_piloto,
      	  v_cgc_cliente_4,          	v_grupo_tecido_corte,
      	  v_cgc_cliente_9,          	v_data_mov_cilindro,
      	  v_perc_composicao1,       	v_serie_tamanho,
      	  v_perc_composicao2,       	v_numero_pontos_laser,
      	  v_perc_composicao3,       	v_percentual_amaciante,
      	  v_perc_composicao4,       	v_simbolo_1,
      	  v_perc_composicao5,       	v_simbolo_2,
      	  v_estagio_altera_programado,  v_simbolo_3,
      	  v_risco_padrao,               v_simbolo_4,
      	  v_responsavel,                v_simbolo_5,
      	  v_multiplicador,              v_lote_estoque,
      	  v_num_sol_desenv,             v_dias_maturacao,
      	  v_sortimento_diversos,        v_cod_bar_por_cli,
      	  v_grupo_agrupador,            v_familia_atributo,
      	  v_sub_agrupador,              v_doc_referencial,
      	  v_cod_embalagem,              v_ligamento,
      	  v_conf_altera_prog,
      	  v_observacao2,                v_perc_composicao6,
      	  v_observacao3,                v_perc_composicao7,
      	  v_publico_alvo,               v_perc_composicao8,
      	  v_cor_de_sortido,             v_perc_composicao9,
      	  v_tipo_codigo_ean,            v_perc_composicao10,
      	  v_num_programa,               v_composicao_06,
      	  v_referencia_desenv,          v_composicao_07,
      	  v_tab_rendimento,             v_composicao_08,
      	  v_largura_cilindro,           v_composicao_09,
      	  v_largura_estampa,            v_composicao_10,
      	  v_descricao_tag1,             v_simbolo_6,
      	  v_descricao_tag2,             v_simbolo_7,
      	  v_descr_complementar,         v_simbolo_8,
      	  v_pasta_banho,                v_simbolo_9,
      	  v_perimetro_corte,            v_simbolo_10,
      	  v_produto_prototipo,          v_parte_composicao_06,
      	  v_tempo_laser,                v_parte_composicao_07,
      	  v_colecao_cliente,            v_parte_composicao_08,
      	  v_descr_ingles,    		v_parte_composicao_09,
      	  v_descr_espanhol,  		v_parte_composicao_10,
      	  v_tamanho_amostra, 		v_tipo_operacao,
      	  ws_usuario_systextil,         1);

        exception
           when OTHERS
           then raise_application_error (-20000, 'N�o atualizou a tabela e_basi_030' || Chr(10) || SQLERRM);
        end;
     end if;
   end if;
 end inter_tr_basi_030_integracao;
-- ALTER TRIGGER "INTER_TR_BASI_030_INTEGRACAO" DISABLE
 

/

exec inter_pr_recompile;

