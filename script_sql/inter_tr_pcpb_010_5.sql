create or replace trigger "INTER_TR_PCPB_010_5"
     before insert
         or update of periodo_producao
     on pcpb_010
     for each row
declare

begin

   if inserting
   then
      :new.periodo_prod_original := :new.periodo_producao;
   end if;

   if updating
   then
      declare
          isApontamento number;
      begin
          select nvl(count(*),0)
          into isApontamento
          from pcpb_015
          where pcpb_015.ordem_producao = :new.ordem_producao
          and   pcpb_015.data_inicio    is not null;

          if  :new.periodo_prod_original <> :new.periodo_producao
          and isApontamento = 0
          then
              :new.periodo_prod_original  := :new.periodo_producao;
          end if;
      exception when others then
          null;
      end;
   end if;

end inter_tr_pcpb_010_5;
/
exec inter_pr_recompile;

