
  CREATE OR REPLACE TRIGGER "INTER_TR_CREC_100_LOG" 
after insert or delete or update
on CREC_100
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid); 

 if inserting
 then
    begin

        insert into CREC_100_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           NUMERO_PERIODO,   /*8*/
           DIVISAO_PRODUTO,   /*10*/
           DATA_INICIAL_OLD,   /*12*/
           DATA_INICIAL_NEW,   /*13*/
           DATA_FINAL_OLD,   /*14*/
           DATA_FINAL_NEW,   /*15*/
           SITUACAO_PERIODO_OLD,   /*16*/
           SITUACAO_PERIODO_NEW    /*17*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :new.NUMERO_PERIODO, /*8*/
           :new.DIVISAO_PRODUTO, /*10*/
           null,/*12*/
           :new.DATA_INICIAL, /*13*/
           null,/*14*/
           :new.DATA_FINAL, /*15*/
           '',/*16*/
           :new.SITUACAO_PERIODO /*17*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into CREC_100_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           NUMERO_PERIODO, /*8*/
           DIVISAO_PRODUTO, /*10*/
           DATA_INICIAL_OLD, /*12*/
           DATA_INICIAL_NEW, /*13*/
           DATA_FINAL_OLD, /*14*/
           DATA_FINAL_NEW, /*15*/
           SITUACAO_PERIODO_OLD, /*16*/
           SITUACAO_PERIODO_NEW  /*17*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.NUMERO_PERIODO,  /*8*/
           :old.DIVISAO_PRODUTO,  /*10*/
           :old.DATA_INICIAL,  /*12*/
           :new.DATA_INICIAL, /*13*/
           :old.DATA_FINAL,  /*14*/
           :new.DATA_FINAL, /*15*/
           :old.SITUACAO_PERIODO,  /*16*/
           :new.SITUACAO_PERIODO  /*17*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into CREC_100_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           NUMERO_PERIODO, /*8*/
           DIVISAO_PRODUTO, /*10*/
           DATA_INICIAL_OLD, /*12*/
           DATA_INICIAL_NEW, /*13*/
           DATA_FINAL_OLD, /*14*/
           DATA_FINAL_NEW, /*15*/
           SITUACAO_PERIODO_OLD, /*16*/
           SITUACAO_PERIODO_NEW /*17*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.NUMERO_PERIODO, /*8*/
           :old.DIVISAO_PRODUTO, /*10*/
           :old.DATA_INICIAL, /*12*/
           null, /*13*/
           :old.DATA_FINAL, /*14*/
           null, /*15*/
           :old.SITUACAO_PERIODO, /*16*/
           '' /*17*/
         );
    end;
 end if;
end inter_tr_CREC_100_log;

-- ALTER TRIGGER "INTER_TR_CREC_100_LOG" ENABLE
 

/

exec inter_pr_recompile;

