alter table MQOP_030 add lastro number(5,1) default 0.0;
-- Add comments to the columns 
comment on column MQOP_030.lastro
  is 'Quantidade de receita adicional.';
