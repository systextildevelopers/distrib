
  CREATE OR REPLACE PROCEDURE "INTER_PR_CONF_PARTE_PC" (
   p_ordem_producao   in number,
   p_ordem_confeccao  in number
)
is
   ws_usuario_rede                 varchar2(20);
   ws_maquina_rede                 varchar2(40);
   ws_aplicativo                   varchar2(20);
   ws_sid                          number(9);
   ws_empresa                      number(3);
   ws_usuario_systextil            varchar2(250);
   ws_locale_usuario               varchar2(5);
begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   for reg_conf_pc in (select pcpc_090.sequencia_enfesto,
                              pcpc_090.sequencia_parte
                       from pcpc_090
                       where pcpc_090.ordem_producao            = p_ordem_producao
                         and pcpc_090.ordem_confeccao           = p_ordem_confeccao
                         and pcpc_090.data_conferencia_parte    is null
                         and pcpc_090.hora_conferencia_parte    is null
                         and pcpc_090.usuario_conferencia_parte is null
                       group by pcpc_090.sequencia_enfesto,
                                pcpc_090.sequencia_parte
                       order by pcpc_090.sequencia_parte)
   loop
      begin
         update pcpc_090
         set    pcpc_090.data_conferencia_parte     = sysdate,
                pcpc_090.hora_conferencia_parte     = sysdate,
                pcpc_090.usuario_conferencia_parte  = ws_usuario_systextil
         where pcpc_090.ordem_producao              = p_ordem_producao
            and pcpc_090.sequencia_enfesto          = reg_conf_pc.sequencia_enfesto
            and pcpc_090.ordem_confeccao            = p_ordem_confeccao
            and pcpc_090.sequencia_parte            = reg_conf_pc.sequencia_parte
            and pcpc_090.data_conferencia_parte     is null
            and pcpc_090.hora_conferencia_parte     is null
            and pcpc_090.usuario_conferencia_parte  is null;
      end;
   end loop;

end inter_pr_conf_parte_pc;

 

/

exec inter_pr_recompile;

