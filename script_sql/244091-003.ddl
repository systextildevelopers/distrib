INSERT INTO hdoc_035
          ( codigo_programa
          , programa_menu
          , item_menu_def
          , descricao)
   VALUES ( 'pedi_f223'
          , 0
          , 0
          , 'Cadastro de Ocorrências de Pedido de Venda');
 
INSERT INTO hdoc_033
          ( usu_prg_cdusu
          , usu_prg_empr_usu
          , programa
          , nome_menu
          , item_menu
          , ordem_menu
          , incluir
          , modificar
          , excluir
          , procurar)
    VALUES( 'INTERSYS'
          , 1
          , 'pedi_f223'
          , 'nenhum'
          , 0
          , 0
          , 'S'
          , 'S'
          , 'S'
          , 'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Registro de Incidentes de Pedido de Venta'
 WHERE hdoc_036.codigo_programa = 'pedi_f223'
   AND hdoc_036.locale          = 'es_ES';
  
commit work;
