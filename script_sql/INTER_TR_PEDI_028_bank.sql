create or replace trigger INTER_TR_PEDI_028_bank
   before insert on pedi_028
   for each row

declare
   ws_bank                number(9);

begin
   if :new.cod_portador = 0 and (:new.cgc9 > 0 or :new.cgc4 > 0 or :new.cgc2 > 0)
   then

      select pedi_010.portador_cliente into ws_bank
      from pedi_010
      where pedi_010.cgc_9 = :new.cgc9
      and pedi_010.cgc_4   = :new.cgc4
      and pedi_010.cgc_2   = :new.cgc2 ;

     if ws_bank > 0
     then

       :new.cod_portador := ws_bank ;

     end if;

   end if;

end INTER_TR_PEDI_028_bank;
/

exec inter_pr_recompile;

