
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_810_LOG" 
   after insert
   or delete
   or update of
	   pedido_venda, codigo_empresa, qtde_milheiro, qtde_metro,
	   qtde_unidade, valor_milheiro, valor_metro, valor_unidade,
	   valor_total, valor_inf_milheiro, valor_inf_metro, valor_inf_unidade,
	   valor_inf_total, narrativa, narrativa2, ens,
	   rep, rep_estacao, dim_largura, dim_comprimento,
	   pec_especial, pec_overloque, gra_gramatura, mar_marca,
	   qtd_ensacado, str_tp_ultrassonico, desconto, acrescimo,
	   coeficiente, rep_numero, arte_impressa, str_tp_tear,
	   coeficiente_verso
   on pedi_810
   for each row

declare
   ws_usuario_rede          varchar2(20);
   ws_maquina_rede          varchar2(40);
   ws_aplicativo            varchar2(20);

   long_aux                 varchar2(2000);
begin

    if inserting
    then


       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;


       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01,             long01
          )
       VALUES
          ( 'PEDI_810',        'I',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :new.pedido_venda,
            '                              PEDIDO DE VENDAS - DADOS'||
                                     chr(10)                  ||
                                     chr(10)                  ||
            'PEDIDO VENDA.....: ' || :new.pedido_venda        ||
            chr(10)                                           ||
            'COD EMPRESA......: ' || :new.codigo_empresa      ||
            chr(10)                                           ||
            'QTDE MILHEIRO....: ' || :new.qtde_milheiro       ||
            chr(10)                                           ||
            'QTDE METRO.......: ' || :new.qtde_metro          ||
            chr(10)                                           ||
            'QTDE UNIDADE.....: ' || :new.qtde_unidade        ||
            chr(10)                                           ||
            'VALOR MILHEIRO...: ' || :new.valor_milheiro      ||
            chr(10)                                           ||
            'VALOR METRO......: ' || :new.valor_metro         ||
            chr(10)                                           ||
            'VALOR UNIDADE....: ' || :new.valor_unidade       ||
            chr(10)                                           ||
            'VALOR TOTAL......: ' || :new.valor_total         ||
            chr(10)                                           ||
            'VALOR INF MILHEI.: ' || :new.valor_inf_milheiro  ||
            chr(10)                                           ||
            'VALOR INF METRO..: ' || :new.valor_inf_metro     ||
            chr(10)                                           ||
            'VALOR INF UNIDAD.: ' || :new.valor_inf_unidade   ||
            chr(10)                                           ||
            'VALOR INF TOTAL..: ' || :new.valor_inf_total     ||
            chr(10)                                           ||
            'NARRATIVA........: ' || :new.narrativa           ||
            chr(10)                                           ||
            'NARRATIVA2.......: ' || :new.narrativa2          ||
            chr(10)                                           ||
            'ENS..............: ' || :new.ens                 ||
            chr(10)                                           ||
            'REP..............: ' || :new.rep                 ||
            chr(10)                                           ||
            'REP ESTACAO......: ' || :new.rep_estacao         ||
            chr(10)                                           ||
            'DIM LARGURA......: ' || :new.dim_largura         ||
            chr(10)                                           ||
            'DIM COMPRIMENTO..: ' || :new.dim_comprimento     ||
            chr(10)                                           ||
            'PEC ESPECIAL.....: ' || :new.pec_especial        ||
            chr(10)                                           ||
            'PEC OVERLOQUE....: ' || :new.pec_overloque       ||
            chr(10)                                           ||
            'GRA GRAMATURA....: ' || :new.gra_gramatura       ||
            chr(10)                                           ||
            'MAR MARCA........: ' || :new.mar_marca           ||
            chr(10)                                           ||
            'QTD ENSACADO.....: ' || :new.qtd_ensacado        ||
            chr(10)                                           ||
            'STR_TP_ULTRASSON.: ' || :new.str_tp_ultrassonico ||
            chr(10)                                           ||
            'DESCONTO.........: ' || :new.desconto            ||
            chr(10)                                           ||
            'ACRESCIMO........: ' || :new.acrescimo           ||
            chr(10)                                           ||
            'COEFICIENTE......: ' || :new.coeficiente         ||
            chr(10)                                           ||
            'REP NUMERO......: ' || :new.rep_numero           ||
            chr(10)                                           ||
            'ARTE IMPRESSA...: ' || :new.arte_impressa
         );
    end if;

    if updating and
       (:old.pedido_venda        <> :new.pedido_venda        or
        :old.codigo_empresa      <> :new.codigo_empresa      or
        :old.qtde_milheiro       <> :new.qtde_milheiro       or
        :old.qtde_metro          <> :new.qtde_metro          or
        :old.qtde_unidade        <> :new.qtde_unidade        or
        :old.valor_milheiro      <> :new.valor_milheiro      or
        :old.valor_metro         <> :new.valor_metro         or
        :old.valor_unidade       <> :new.valor_unidade       or
        :old.valor_total         <> :new.valor_total         or
        :old.valor_inf_milheiro  <> :new.valor_inf_milheiro  or
        :old.valor_inf_metro     <> :new.valor_inf_metro     or
        :old.valor_inf_unidade   <> :new.valor_inf_unidade   or
        :old.valor_inf_total     <> :new.valor_inf_total     or
        :old.narrativa           <> :new.narrativa           or
        :old.narrativa2          <> :new.narrativa2          or
        :old.ens                 <> :new.ens                 or
        :old.rep                 <> :new.rep                 or
        :old.rep_estacao         <> :new.rep_estacao         or
        :old.dim_largura         <> :new.dim_largura         or
        :old.dim_comprimento     <> :new.dim_comprimento     or
        :old.pec_especial        <> :new.pec_especial        or
        :old.pec_overloque       <> :new.pec_overloque       or
        :old.gra_gramatura       <> :new.gra_gramatura       or
        :old.mar_marca           <> :new.mar_marca           or
        :old.qtd_ensacado        <> :new.qtd_ensacado        or
        :old.str_tp_ultrassonico <> :new.str_tp_ultrassonico or
        :old.desconto            <> :new.desconto            or
        :old.acrescimo           <> :new.acrescimo           or
        :old.coeficiente         <> :new.coeficiente         or
        :old.rep_numero          <> :new.rep_numero          or
        :old.arte_impressa       <> :new.arte_impressa
      )
    then
       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;

       long_aux := long_aux ||
                  '                              PEDIDO DE VENDAS - DADOS'||
                   chr(10)                    ||
                   chr(10);

       if  :old.pedido_venda <> :new.pedido_venda
       then
           long_aux := long_aux ||
                'PEDIDO VENDA....: ' || :old.pedido_venda    || ' - '
                                     || :new.pedido_venda
                                     || chr(10);
       end if;

       if  :old.codigo_empresa <> :new.codigo_empresa
       then
           long_aux := long_aux ||
                'COD EMPRESA.....: ' || :old.codigo_empresa    || ' - '
                                     || :new.codigo_empresa
                                     || chr(10);
       end if;

       if  :old.qtde_milheiro <> :new.qtde_milheiro
       then
           long_aux := long_aux ||
                'QTDE MILHEIRO...: ' || :old.qtde_milheiro    || ' - '
                                     || :new.qtde_milheiro
                                     || chr(10);
       end if;

       if  :old.qtde_metro <> :new.qtde_metro
       then
           long_aux := long_aux ||
                'QTDE METRO......: ' || :old.qtde_metro    || ' - '
                                     || :new.qtde_metro
                                     || chr(10);
       end if;

       if  :old.qtde_unidade <> :new.qtde_unidade
       then
           long_aux := long_aux ||
                'QTDE UNIDADE....: ' || :old.qtde_unidade    || ' - '
                                     || :new.qtde_unidade
                                     || chr(10);
       end if;

       if  :old.valor_milheiro <> :new.valor_milheiro
       then
           long_aux := long_aux ||
                'VALOR MILHEIRO..: ' || :old.valor_milheiro    || ' - '
                                     || :new.valor_milheiro
                                     || chr(10);
       end if;

       if  :old.valor_metro <> :new.valor_metro
       then
           long_aux := long_aux ||
                'VALOR METRO.....: ' || :old.valor_metro    || ' - '
                                     || :new.valor_metro
                                     || chr(10);
       end if;

       if  :old.valor_unidade <> :new.valor_unidade
       then
           long_aux := long_aux ||
                'VALOR UNIDADE...: ' || :old.valor_unidade    || ' - '
                                     || :new.valor_unidade
                                     || chr(10);
       end if;

       if  :old.valor_total <> :new.valor_total
       then
           long_aux := long_aux ||
                'VALOR TOTAL.....: ' || :old.valor_total    || ' - '
                                     || :new.valor_total
                                     || chr(10);
       end if;

       if  :old.valor_inf_milheiro <> :new.valor_inf_milheiro
       then
           long_aux := long_aux ||
                'VALOR INF MIL...: ' || :old.valor_inf_milheiro    || ' - '
                                     || :new.valor_inf_milheiro
                                     || chr(10);
       end if;

       if  :old.valor_inf_metro <> :new.valor_inf_metro
       then
           long_aux := long_aux ||
                'VALOR INF METRO.: ' || :old.valor_inf_metro    || ' - '
                                     || :new.valor_inf_metro
                                     || chr(10);
       end if;

       if  :old.valor_inf_unidade <> :new.valor_inf_unidade
       then
           long_aux := long_aux ||
                'VALOR INF UNID..: ' || :old.valor_inf_unidade    || ' - '
                                     || :new.valor_inf_unidade
                                     || chr(10);
       end if;

       if  :old.valor_inf_total <> :new.valor_inf_total
       then
           long_aux := long_aux ||
                'VALOR INF TOTAL.: ' || :old.valor_inf_total    || ' - '
                                     || :new.valor_inf_total
                                     || chr(10);
       end if;

       if  :old.narrativa <> :new.narrativa
       then
           long_aux := long_aux ||
                'NARRATIVA.......: ' || :old.narrativa    || ' - '
                                     || :new.narrativa
                                     || chr(10);
       end if;

       if  :old.narrativa2 <> :new.narrativa2
       then
           long_aux := long_aux ||
                'NARRATIVA2......: ' || :old.narrativa2    || ' - '
                                     || :new.narrativa2
                                     || chr(10);
       end if;

       if  :old.ens <> :new.ens
       then
           long_aux := long_aux ||
                'ENS.............: ' || :old.ens    || ' - '
                                     || :new.ens
                                     || chr(10);
       end if;

       if  :old.rep <> :new.rep
       then
           long_aux := long_aux ||
                'REP.............: ' || :old.rep    || ' - '
                                     || :new.rep
                                     || chr(10);
       end if;

       if  :old.rep_estacao <> :new.rep_estacao
       then
           long_aux := long_aux ||
                'REP_ESTACAO.....: ' || :old.rep_estacao    || ' - '
                                     || :new.rep_estacao
                                     || chr(10);
       end if;

       if  :old.dim_largura <> :new.dim_largura
       then
           long_aux := long_aux ||
                'DIM LARGURA.....: ' || :old.dim_largura    || ' - '
                                     || :new.dim_largura
                                     || chr(10);
       end if;

       if  :old.dim_comprimento <> :new.dim_comprimento
       then
           long_aux := long_aux ||
                'DIM COMPRIMENTO.: ' || :old.dim_comprimento    || ' - '
                                     || :new.dim_comprimento
                                     || chr(10);
       end if;

       if  :old.pec_especial <> :new.pec_especial
       then
           long_aux := long_aux ||
                'PEC ESPECIAL....: ' || :old.pec_especial    || ' - '
                                     || :new.pec_especial
                                     || chr(10);
       end if;

       if  :old.pec_overloque <> :new.pec_overloque
       then
           long_aux := long_aux ||
                'PEC OVERLOQUE...: ' || :old.pec_overloque    || ' - '
                                     || :new.pec_overloque
                                     || chr(10);
       end if;

       if  :old.gra_gramatura <> :new.gra_gramatura
       then
           long_aux := long_aux ||
                'GRA GRAMATURA...: ' || :old.gra_gramatura    || ' - '
                                     || :new.gra_gramatura
                                     || chr(10);
       end if;

       if  :old.mar_marca <> :new.mar_marca
       then
           long_aux := long_aux ||
                'MAR MARCA.......: ' || :old.mar_marca    || ' - '
                                     || :new.mar_marca
                                     || chr(10);
       end if;

       if  :old.qtd_ensacado <> :new.qtd_ensacado
       then
           long_aux := long_aux ||
                'QTD ENSACADO....: ' || :old.qtd_ensacado    || ' - '
                                     || :new.qtd_ensacado
                                     || chr(10);
       end if;

       if  :old.str_tp_ultrassonico <> :new.str_tp_ultrassonico
       then
           long_aux := long_aux ||
                'STR TP ULTRASSO.: ' || :old.str_tp_ultrassonico    || ' - '
                                     || :new.str_tp_ultrassonico
                                     || chr(10);
       end if;

       if  :old.desconto <> :new.desconto
       then
           long_aux := long_aux ||
                'DESCONTO........: ' || :old.desconto    || ' - '
                                     || :new.desconto
                                     || chr(10);
       end if;

       if  :old.acrescimo <> :new.acrescimo
       then
           long_aux := long_aux ||
                'ACRESCIMO.......: ' || :old.acrescimo    || ' - '
                                     || :new.acrescimo
                                     || chr(10);
       end if;

       if  :old.coeficiente <> :new.coeficiente
       then
           long_aux := long_aux ||
                'COEFICIENTE.....: ' || :old.coeficiente    || ' - '
                                     || :new.coeficiente
                                     || chr(10);
       end if;

       if  :old.rep_numero <> :new.rep_numero
       then
           long_aux := long_aux ||
                'REP NUMERO......: ' || :old.rep_numero    || ' - '
                                     || :new.rep_numero
                                     || chr(10);
       end if;

       if  :old.arte_impressa <> :new.arte_impressa
       then
           long_aux := long_aux ||
                'ARTE IMPRESSA...: ' || :old.arte_impressa    || ' - '
                                     || :new.arte_impressa
                                     || chr(10);
       end if;

       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01,             long01
          )
       VALUES
          ( 'PEDI_810',        'A',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :new.pedido_venda, long_aux
         );
    end if;

    if deleting
    then


       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;


       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01
          )
       VALUES
          ( 'PEDI_810',        'D',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :old.pedido_venda
         );
    end if;

end inter_tr_pedi_810_log;

-- ALTER TRIGGER "INTER_TR_PEDI_810_LOG" ENABLE
 

/

exec inter_pr_recompile;

