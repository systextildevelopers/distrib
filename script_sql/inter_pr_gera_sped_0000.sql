  create or replace procedure inter_pr_gera_sped_0000 (p_tip_contabil_fiscal in varchar2,
                                                       p_cod_empresa  IN NUMBER,
                                                       p_des_erro     OUT varchar2) is
--
-- Finalidade: Gerar a tabela sped_000 - Dados da empresa
-- Autor.....: Cesar Anton
-- Data......: 19/11/08
--
-- Historicos
--
-- Data    Autor    Observacoes
--
CURSOR u_fatu_500 (p_cod_empresa    NUMBER) IS
   SELECT *
   from   fatu_500
   where  fatu_500.codigo_empresa = p_cod_empresa;

w_erro                 EXCEPTION;
w_sig_estado_empresa   basi_160.estado%TYPE;
w_num_ddd_empresa      basi_160.ddd%TYPE;
w_sig_estado_contabili basi_160.estado%TYPE;
w_num_ddd_contabili    basi_160.ddd%TYPE;
w_codigo_fiscal_uf     basi_167.codigo_fiscal_uf%type;
w_codigo_cidade_ibge   basi_160.codigo_fiscal%type;
w_codigo_cidade_ibge_contab basi_160.codigo_fiscal%type;
w_codigo_fiscal_uf_contab   basi_167.codigo_fiscal_uf%type;

BEGIN
   p_des_erro          := NULL;
   FOR fatu_500 IN u_fatu_500 (p_cod_empresa) LOOP
       BEGIN
       SELECT basi_160.estado,                 basi_160.ddd,                basi_160.codigo_fiscal,
              basi_167.codigo_fiscal_uf
        INTO   w_sig_estado_empresa,           w_num_ddd_empresa,           w_codigo_cidade_ibge,
               w_codigo_fiscal_uf
        from   basi_160,
               basi_167,
               basi_165
        where basi_160.cod_cidade  = fatu_500.codigo_cidade
          and   basi_160.estado      = basi_167.estado
          and   basi_160.codigo_pais = basi_167.codigo_pais
          and   basi_160.codigo_pais = basi_165.codigo_pais;
        EXCEPTION
        WHEN OTHERS THEN
           w_sig_estado_empresa := NULL;
           w_num_ddd_empresa    := NULL;
           w_codigo_cidade_ibge := NULL;
           w_codigo_fiscal_uf   := NULL;
           inter_pr_insere_erro_sped ('F',p_cod_empresa,
                              'Erro na leitura da cidade da empresa ' || p_cod_empresa || Chr(10) ||
                              '   Cidade: ' || fatu_500.codigo_cidade);
       END;


       begin
          select basi_160.estado,             basi_160.ddd,
                 basi_160.codigo_fiscal,      basi_167.codigo_fiscal_uf
          into   w_sig_estado_contabili,      w_num_ddd_contabili,
                 w_codigo_cidade_ibge_contab, w_codigo_fiscal_uf_contab
        from   basi_160,
               basi_167,
               basi_165
        where  basi_160.cod_cidade = fatu_500.cod_cidade_contador
          and  basi_160.estado      = basi_167.estado
          and  basi_160.codigo_pais = basi_167.codigo_pais
          and  basi_160.codigo_pais = basi_165.codigo_pais;
       exception
          when no_data_found then
           w_sig_estado_contabili := NULL;
           w_num_ddd_contabili    := NULL;
           w_codigo_cidade_ibge_contab :=NULL;
           w_codigo_fiscal_uf_contab   :=NULL;
           inter_pr_insere_erro_sped ('F',p_cod_empresa,
                               'Erro na leitura da cidade do contador da empresa ' || Chr(10) ||
                               '   Cidade: ' || fatu_500.cod_cidade_contador);
       END;

       IF  Nvl(Length(trim(fatu_500.telefone_contador)),0) > 10 THEN

            inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                'Procedure: p_gera_sped_0000 ' || Chr(10) ||
                                'Tamanho do campo Telefone contador e maior que o permitido ' || Chr(10) ||
                                '   Fone: ' || fatu_500.telefone_contador || Chr(10)||
                                '   Empresa: ' || p_cod_empresa || Chr(10));
            fatu_500.telefone_contador := substr(trim(fatu_500.telefone_contador),1,10);
       END IF;

       IF  Nvl(Length(trim(fatu_500.fax_contador)),0) > 10 THEN

            inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                'Procedure: p_gera_sped_0000 ' || Chr(10) ||
                                'Tamanho do campo Fax contador e maior que o permitido ' || Chr(10) ||
                                '   Fone: ' || fatu_500.fax_contador || Chr(10) ||
                                '   Empresa: ' || p_cod_empresa || Chr(10));
            fatu_500.fax_contador    := substr(trim(fatu_500.fax_contador),1,10);
       END IF;


       begin
          insert into sped_0000
            (tip_contabil_fiscal,
             cod_empresa,                           num_cnpj9,                            num_cnpj4,
             num_cnpj2,                             nom_empresa,                          nom_fantasia,
             num_cep,                               des_endereco,                         num_endereco,

             des_compl_endereco,                    nom_bairro,                           num_ddd_empresa,
             num_fone,                              num_fax,                              des_email,
             sig_estado,                            num_inscri_estadual,                  cod_cidade ,

             num_inscri_municipal,                  num_suframa,                          nom_contabilista,
             num_cpf_contabilista,                  num_reg_contabilista,                 num_cep_contabilista,
             des_ender_contabilista,                num_ender_contabilista,               des_compl_contabilista,

             des_bairro_contabilista,               num_ddd_contabilista,                 num_fone_contabilista,
             num_fax_contabilista,                  des_email_contabilista,               cod_cidade_contabilista
            ) values
            (p_tip_contabil_fiscal,
             fatu_500.codigo_empresa,               fatu_500.cgc_9,                       fatu_500.cgc_4,
             fatu_500.cgc_2,                        fatu_500.nome_empresa,                fatu_500.nome_fantasia,
             fatu_500.cep_empresa,                  fatu_500.endereco,                    fatu_500.numero,

             fatu_500.complemento,                  fatu_500.bairro,                      w_num_ddd_empresa,
             fatu_500.telefone,                     fatu_500.fax_empresa,                 fatu_500.e_mail,
             w_sig_estado_empresa,                  substr(REPLACE(fatu_500.inscr_estadual,'.'),1,14),
             lpad(w_codigo_fiscal_uf,2,0) || lpad(w_codigo_cidade_ibge,5,0),

             substr(fatu_500.inscr_municipal,1,14), null,                                 fatu_500.nome_contador,
             substr(lpad(fatu_500.cpf_contador,11,0),1,11), REPLACE(REPLACE(REPLACE(REPLACE(fatu_500.crc_contador,'.'),'-'),'/'),' '),        fatu_500.cep_contador,
             fatu_500.ender_contador,               fatu_500.num_end_contador,            fatu_500.complem_contador,
             fatu_500.bairro_contador,              w_num_ddd_contabili,                  fatu_500.telefone_contador,
             fatu_500.fax_contador,                 fatu_500.e_mail_contador,
             lpad(w_codigo_fiscal_uf_contab,2,0) || lpad(w_codigo_cidade_ibge_contab,5,0)
             );
       EXCEPTION
          WHEN OTHERS THEN
             p_des_erro := 'Erro na inclusao da tabela sped_0000 ' || Chr(10) || SQLERRM;
             RAISE W_ERRO;
       END;
   END LOOP;
   COMMIT;
EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_0000 ' || Chr(10) || p_des_erro;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure p_gera_sped_0000 ' || Chr(10) || SQLERRM;
END inter_pr_gera_sped_0000;
/
exec inter_pr_recompile;

