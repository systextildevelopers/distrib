ALTER TABLE obrf_056
  ADD(
    PERC_PIS_CREDITAR NUMBER(5,2) default 0 null,
    PERC_COFINS_CREDITAR NUMBER(5,2) default 0 null
  );
  
COMMIT;
EXEC inter_pr_recompile;
