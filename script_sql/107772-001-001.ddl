alter table obrf_059
add (debito_orig_cred_pres_recebi number(15,2) default 0.0,
     debito_orig_cred_pres_transf number(15,2) default 0.0);
     
comment on column obrf_059.debito_orig_cred_pres_recebi is 'Débito pela utilização do crédito presumido recebido de estabelecimento consolidado';
comment on column obrf_059.debito_orig_cred_pres_transf is 'Débito pela utilização do crédito presumido transferido ao estabelecimento consolidador consolidado';

exec inter_pr_recompile;
/
