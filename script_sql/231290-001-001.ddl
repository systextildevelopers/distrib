delete hdoc_033 where programa = 'pcpb_fa137';
delete hdoc_035 where codigo_programa = 'pcpb_fa137';

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('pcpb_fa24', 'Gerador de ordens de beneficiamento', 0, 1, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir,
modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpb_fa24', 'menu_gp64', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir,
modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpb_fa24', 'menu_gp64', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.codigo_programa = 'pcpb_fa24'
 where hdoc_036.codigo_programa = 'pcpb_fa137'
   and hdoc_036.locale          = 'es_ES';

   
update hdoc_036
   set hdoc_036.codigo_programa = 'pcpb_fa24'
 where hdoc_036.codigo_programa = 'pcpb_fa137'
   and hdoc_036.locale          = 'pt_BR';
commit;
