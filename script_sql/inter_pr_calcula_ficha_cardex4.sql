CREATE OR REPLACE PROCEDURE "INTER_PR_CALCULA_FICHA_CARDEX4"
   (p_inc_exc           in number,
    p_empresa1           in number,
    p_empresa2          in number,
    p_empresa3           in number,
    p_empresa4          in number,
    p_empresa5           in number,
    p_nivel_prod        in varchar2,
    p_grupo_prod         in varchar2,
    p_nivel_prod1       in varchar2,
    p_grupo_prod1        in varchar2,
    p_nivel_prod2       in varchar2,
    p_grupo_prod2        in varchar2,
    p_nivel_prod3       in varchar2,
    p_grupo_prod3        in varchar2,
    p_conta_estoque     in number,
    p_data_inicial_ficha in date,
    p_data_final_ficha  in date,
    p_origem_custo_fabr  in number,
    p_estq_300_estq_310 in varchar2)

is
   cursor fatu500 is
      select fatu_500.codigo_empresa
      from fatu_500
      where (fatu_500.codigo_empresa in (p_empresa1,p_empresa2,p_empresa3,p_empresa4,p_empresa5)
         or  p_empresa1 = 0);

   cursor estq300 (p_codigo_empresa number, p_data_inicial_ficha date) is
      select estq_300.data_movimento,                 estq_300.codigo_deposito,
             estq_300.nivel_estrutura,                estq_300.grupo_estrutura,
             estq_300.subgrupo_estrutura,             estq_300.item_estrutura,
             estq_300.sequencia_insercao,             estq_300.numero_lote,
             estq_300.codigo_transacao,               estq_300.entrada_saida tipo_e_s,
             estq_300.quantidade,                     estq_300.valor_movimento_unitario,
             estq_300.valor_contabil_unitario,        estq_300.preco_medio_unitario,
             estq_300.numero_documento,               upper(estq_300.tabela_origem) tabela_origem,
             estq_300.sequencia_ficha,                estq_005.calcula_preco,
             estq_300.valor_movimento_unitario_proj,  estq_300.valor_contabil_unitario_proj,
             estq_300.serie_documento,                estq_300.cnpj_9,
             estq_300.cnpj_4,                         estq_300.cnpj_2,
             estq_300.sequencia_documento,            estq_300.valor_movto_unit_estimado,
             estq_300.valor_total,                    nvl(estq_300.quantidade_quilo, 0) QUANTIDADE_QUILO,
             estq_005.altera_custo,                   estq_005.tipo_transacao,
             estq_300.data_insercao
      from estq_300, estq_005
      where estq_300.codigo_transacao   = estq_005.codigo_transacao
      and   lower(p_estq_300_estq_310)  = 'estq_300'
      and   estq_300.codigo_deposito    in (
         select basi_205.codigo_deposito
         from basi_205
         where (basi_205.local_deposito   = p_codigo_empresa or  p_empresa1 = 0)
           and basi_205.controla_ficha_cardex = 1
      )
      and ((estq_300.nivel_estrutura    = p_nivel_prod  and (estq_300.grupo_estrutura = p_grupo_prod  or p_grupo_prod  = 'XXXXXX'))
      or   (estq_300.nivel_estrutura    = p_nivel_prod1 and (estq_300.grupo_estrutura = p_grupo_prod1 or p_grupo_prod1 = 'XXXXXX'))
      or   (estq_300.nivel_estrutura    = p_nivel_prod2 and (estq_300.grupo_estrutura = p_grupo_prod2 or p_grupo_prod2 = 'XXXXXX'))
      or   (estq_300.nivel_estrutura    = p_nivel_prod3 and (estq_300.grupo_estrutura = p_grupo_prod3 or p_grupo_prod3 = 'XXXXXX')))
      and   estq_300.data_movimento between p_data_inicial_ficha and p_data_final_ficha
      UNION ALL
      select estq_310.data_movimento,                 estq_310.codigo_deposito,
             estq_310.nivel_estrutura,                estq_310.grupo_estrutura,
             estq_310.subgrupo_estrutura,             estq_310.item_estrutura,
             estq_310.sequencia_insercao,             estq_310.numero_lote,
             estq_310.codigo_transacao,               estq_310.entrada_saida tipo_e_s,
             estq_310.quantidade,                     estq_310.valor_movimento_unitario,
             estq_310.valor_contabil_unitario,        estq_310.preco_medio_unitario,
             estq_310.numero_documento,               upper(estq_310.tabela_origem),
             estq_310.sequencia_ficha,                estq_005.calcula_preco,
             estq_310.valor_movimento_unitario_proj,  estq_310.valor_contabil_unitario_proj,
             estq_310.serie_documento,                estq_310.cnpj_9,
             estq_310.cnpj_4,                         estq_310.cnpj_2,
             estq_310.sequencia_documento,            estq_310.valor_movto_unit_estimado,
             estq_310.valor_total,                    nvl(estq_310.quantidade_quilo, 0) QUANTIDADE_QUILO,
             estq_005.altera_custo,                   estq_005.tipo_transacao,
             estq_310.data_insercao
      from estq_310, estq_005
      where estq_310.codigo_transacao   = estq_005.codigo_transacao
      and   lower(p_estq_300_estq_310)  = 'estq_310'
      and   estq_310.codigo_deposito    in (
         select basi_205.codigo_deposito
         from basi_205
         where (basi_205.local_deposito   = p_codigo_empresa or  p_empresa1 = 0)
           and basi_205.controla_ficha_cardex = 1
      )
      and ((estq_310.nivel_estrutura    = p_nivel_prod  and (estq_310.grupo_estrutura = p_grupo_prod  or p_grupo_prod  = 'XXXXXX'))
      or   (estq_310.nivel_estrutura    = p_nivel_prod1 and (estq_310.grupo_estrutura = p_grupo_prod1 or p_grupo_prod1 = 'XXXXXX'))
      or   (estq_310.nivel_estrutura    = p_nivel_prod2 and (estq_310.grupo_estrutura = p_grupo_prod2 or p_grupo_prod2 = 'XXXXXX'))
      or   (estq_310.nivel_estrutura    = p_nivel_prod3 and (estq_310.grupo_estrutura = p_grupo_prod3 or p_grupo_prod3 = 'XXXXXX')))
      and   estq_310.data_movimento between p_data_inicial_ficha and p_data_final_ficha
    --  and estq_310.subgrupo_estrutura = '632'
    --  and estq_310.item_estrutura = 'Y70003'
      order by nivel_estrutura, grupo_estrutura, subgrupo_estrutura,
               item_estrutura,  data_insercao,   codigo_deposito,   
               data_movimento,  tipo_e_s,        sequencia_insercao,
               sequencia_ficha;

  cursor estq300_calculada (p_codigo_empresa number, p_data_inicial_ficha date) is
    select sum(decode(entrada_saida, 'E', quantidade, quantidade * -1)) saldo_fisico, codigo_deposito,
         nivel_estrutura, grupo_estrutura, subgrupo_estrutura, item_estrutura
     from (
        select estq_300.entrada_saida, estq_300.quantidade,   estq_300.codigo_deposito,
             estq_300.nivel_estrutura, estq_300.grupo_estrutura, estq_300.subgrupo_estrutura, estq_300.item_estrutura
        from estq_300
        where lower(p_estq_300_estq_310)  = 'estq_300'
        and   estq_300.codigo_deposito    in (
           select basi_205.codigo_deposito
           from basi_205
           where (basi_205.local_deposito   = p_codigo_empresa or  p_empresa1 = 0)
             and basi_205.controla_ficha_cardex = 1
        )
        and ((estq_300.nivel_estrutura    = p_nivel_prod  and (estq_300.grupo_estrutura = p_grupo_prod  or p_grupo_prod  = 'XXXXXX'))
        or   (estq_300.nivel_estrutura    = p_nivel_prod1 and (estq_300.grupo_estrutura = p_grupo_prod1 or p_grupo_prod1 = 'XXXXXX'))
        or   (estq_300.nivel_estrutura    = p_nivel_prod2 and (estq_300.grupo_estrutura = p_grupo_prod2 or p_grupo_prod2 = 'XXXXXX'))
        or   (estq_300.nivel_estrutura    = p_nivel_prod3 and (estq_300.grupo_estrutura = p_grupo_prod3 or p_grupo_prod3 = 'XXXXXX')))
        and   estq_300.data_movimento between p_data_inicial_ficha and p_data_final_ficha
        UNION ALL
        select estq_310.entrada_saida, estq_310.quantidade,   estq_310.codigo_deposito,
           estq_310.nivel_estrutura, estq_310.grupo_estrutura, estq_310.subgrupo_estrutura, estq_310.item_estrutura
          from estq_310
        where lower(p_estq_300_estq_310)  = 'estq_310'
        and   estq_310.codigo_deposito    in (
           select basi_205.codigo_deposito
           from basi_205
           where (basi_205.local_deposito   = p_codigo_empresa or  p_empresa1 = 0)
             and basi_205.controla_ficha_cardex = 1
        )
        and ((estq_310.nivel_estrutura    = p_nivel_prod  and (estq_310.grupo_estrutura = p_grupo_prod  or p_grupo_prod  = 'XXXXXX'))
        or   (estq_310.nivel_estrutura    = p_nivel_prod1 and (estq_310.grupo_estrutura = p_grupo_prod1 or p_grupo_prod1 = 'XXXXXX'))
        or   (estq_310.nivel_estrutura    = p_nivel_prod2 and (estq_310.grupo_estrutura = p_grupo_prod2 or p_grupo_prod2 = 'XXXXXX'))
        or   (estq_310.nivel_estrutura    = p_nivel_prod3 and (estq_310.grupo_estrutura = p_grupo_prod3 or p_grupo_prod3 = 'XXXXXX')))
        and   estq_310.data_movimento between p_data_inicial_ficha and p_data_final_ficha)
     group by codigo_deposito, nivel_estrutura, grupo_estrutura, subgrupo_estrutura, item_estrutura;

   v_calcula_consignado         empr_002.calcula_consignado%type;
   v_nivel_estrutura_ant             estq_300.nivel_estrutura%type;
   v_grupo_estrutura_ant             estq_300.grupo_estrutura%type;
   v_subgrupo_estrutura_ant          estq_300.subgrupo_estrutura%type;
   v_item_estrutura_ant              estq_300.item_estrutura%type;
   v_data_movimento_ant              estq_300.data_movimento%type;
   v_saldo_fisico_anterior           estq_300.saldo_fisico%type;
   v_saldo_fisico_anterior_dep       estq_300.saldo_fisico%type;
   v_saldo_financeiro_anterior       estq_300.saldo_financeiro%type;
   v_saldo_financeiro_ant_dep      estq_300.saldo_financeiro%type;
   v_saldo_financeiro_ant_est        estq_300.saldo_financeiro_estimado%type;
   v_saldo_financeiro_ant_est_dep    estq_300.saldo_financeiro_estimado%type;
   v_saldo_financeiro_ant_proj       estq_300.saldo_financeiro_proj%type;
   v_saldo_finan_ant_proj_dep      estq_300.saldo_financeiro_proj%type;
   v_preco_medio_anterior            estq_300.preco_medio_unitario%type;
   v_preco_medio_anterior_dep        estq_300.preco_medio_unitario%type;
   v_preco_medio_anterior_est        estq_300.preco_medio_unit_estimado%type;
   v_preco_medio_anterior_est_dep    estq_300.preco_medio_unit_estimado%type;
   v_preco_medio_anterior_proj       estq_300.preco_medio_unitario_proj%type;
   v_preco_medio_ant_proj_dep        estq_300.preco_medio_unitario_proj%type;
   v_novo_valor_movimento_unit       estq_300.valor_movimento_unitario%type;
   v_novo_valor_movimento_tot        estq_300.valor_total%type;
   v_novo_valor_movto_unit_est       estq_300.valor_movto_unit_estimado%type;
   v_novo_valor_movto_unit_proj      estq_300.valor_movimento_unitario_proj%type;
   v_novo_valor_custo_unit           estq_300.preco_medio_unitario%type;
   v_novo_valor_custo_unit_proj      estq_300.preco_medio_unitario_proj%type;
   v_novo_valor_custo_unit_est       estq_300.preco_medio_unitario%type;
   v_novo_saldo_fisico               estq_300.saldo_fisico%type;
   v_novo_saldo_financeiro           estq_300.saldo_financeiro%type;
   v_novo_saldo_financeiro_est       estq_300.saldo_financeiro_estimado%type;
   v_novo_saldo_financeiro_proj      estq_300.saldo_financeiro_proj%type;
   v_novo_preco_medio                estq_300.preco_medio_unitario%type;
   v_novo_preco_medio_est            estq_300.preco_medio_unit_estimado%type;
   v_novo_preco_medio_proj           estq_300.preco_medio_unitario_proj%type;
   v_seq_ficha                       estq_300.sequencia_ficha%type;
   v_valor_contabil                  estq_300.valor_contabil_unitario%type;
   v_valor_contabil_proj             estq_300.valor_contabil_unitario_proj%type;
   v_data_inicial                    empr_001.periodo_estoque%type;
   v_conta_estoque                   basi_030.conta_estoque%type;
   v_comprado_fabric                 basi_030.comprado_fabric%type;
   v_comprado_fabric_015             basi_030.comprado_fabric%type;
   v_tipo_empresa                    fatu_500.tipo_empresa%type;

   v_ultimo_saldo_fisico             estq_300.saldo_fisico%type;
   v_ultimo_saldo_financeiro         estq_300.saldo_financeiro%type;
   v_ultimo_preco_medio              estq_300.preco_medio_unitario%type;
   v_ultimo_preco_custo              estq_300.valor_movimento_unitario%type;
   v_ultimo_saldo_financeiro_est     estq_300.saldo_financeiro_estimado%type;
   v_ultimo_preco_medio_est          estq_300.preco_medio_unit_estimado%type;
   v_ultimo_preco_custo_est          estq_300.valor_movto_unit_estimado%type;
   v_ultimo_saldo_financeiro_proj    estq_300.saldo_financeiro_proj%type;
   v_ultimo_preco_medio_proj         estq_300.preco_medio_unitario_proj%type;
   v_ultimo_preco_custo_proj         estq_300.valor_movimento_unitario_proj%type;

   v_valor_subproduto                basi_305.valor_subproduto%type;
   v_valorizacao_cardex              empr_002.valorizacao_cardex%type;

   v_apuracao_ir                     fatu_503.apuracao_ir%type;

   v_saldo_fisico_anterior_quilo     estq_300.saldo_fisico_quilo%type;
   v_saldo_fisico_ant_quilo_dep      estq_300.saldo_fisico_quilo%type;
   v_novo_saldo_fisico_quilo         estq_300.saldo_fisico_quilo%type;
   v_ultimo_saldo_fisico_quilo       estq_300.saldo_fisico_quilo%type;

  -- variaveis utilizadas pelo processo de gravacao dos valores mensais (estq_301)
   v_mes_ant_301                     number;
   v_ano_ant_301                     number;
   v_mes_movto_301                   number;
   v_ano_movto_301                   number;
   v_mes_ano_301                     date;
   grava_estq_301                    varchar2(1);

   v_processa_deposito               varchar2(1);
   v_processa_produto                varchar2(1);
   v_ref_original                    varchar2(5);

   v_custo_fab_prev                  number;
   v_custo_fab_proj                  number;
   v_custo_fab_est                   number;
   v_mes_movimento                   number;
   v_ano_movimento                   number;
   v_mes_ant                         number;
   v_ano_ant                         number;
   v_total_movimento_pcpc_320        number;
   v_total_movto_pcpc_320_proj       number;
   v_mes_movimento2                  number;
   v_ano_movimento2                  number;
   v_mes_movto_140                   number;
   v_ano_movto_140                   number;

   v_valor_indice_140                number;
   v_tipo_aplic_140                  number;
   v_tem_reg_140                     number;
   v_nr_registros                    number;

-- inicio alterac?o kg
   numero_documento_ant              number;
   v_valor_total_pcpc_320            number;
   v_valor_total_pcpc_320_proj       number;
   v_qtde_total_pcpc_320             number;
   v_qtde_pcpc_320                   number;
   v_valor_prod_pcpc_320             number;
   v_valor_prod_pcpc_320_proj        number;

   v_montagem_volume                 number;
-- fim alterac?o  kg

--variaveis de eliminac?o
   v_mes_del_301                     number;
   v_ano_del_301                     number;


   v_ultimo_calculo                  date;
   v_data_vigencia                   date;

   v_prc_presum                      number;

   v_caracteristica                  pcpc_300.caracteristica%type;

   erro_periodo_estoque              exception;

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   v_par_origem_preco_custo  number(1);
   v_ind_atual_prc_medio_basi_010  fatu_510.ind_atual_prc_medio_basi_010%type;
   v_deposito_terceiro             basi_205.deposito_origem%type;
   v_empresa_custo_gerencial       basi_205.empresa_custo_gerencial%type;


BEGIN

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- le a data inicial
   v_data_inicial := p_data_inicial_ficha;

   -- le parametro global
   begin
      select empr_002.valorizacao_cardex
      into   v_valorizacao_cardex
      from empr_002;
      exception
      when OTHERS
         then v_valorizacao_cardex := 1;
   end;

   -- le depositos escolhidos em paramentros
   for reg_fatu500 in fatu500
   loop

      -- seta avariavel de controle para processar o deposito
      v_processa_deposito := 'S';

      if v_processa_deposito = 'S'
      then

         -- le a empresa para pegar o seu tipo
         begin
            select tipo_empresa into v_tipo_empresa from fatu_500
            where codigo_empresa  = reg_fatu500.codigo_empresa;

         exception
            when others then
               v_tipo_empresa := 1;
         end;

         begin
            select fatu_510.origem_preco_custo, fatu_510.ind_atual_prc_medio_basi_010
            into   v_par_origem_preco_custo,    v_ind_atual_prc_medio_basi_010
            from fatu_510
            where fatu_510.codigo_empresa  = reg_fatu500.codigo_empresa;
         exception
             when no_data_found then
                v_par_origem_preco_custo := 0;
                v_ind_atual_prc_medio_basi_010 := 'N';
         end;


         -- le a empresa para verificar o tipo do custo utilizado (real ou previsto)
         begin
            select fatu_503.apuracao_ir
            into   v_apuracao_ir
            from fatu_503
            where fatu_503.codigo_empresa  = reg_fatu500.codigo_empresa;

            if v_apuracao_ir not in (1,2)
            then
               v_apuracao_ir := 1;
            end if;

            exception
              when others then
                v_apuracao_ir      := 1;
         end;

         v_par_origem_preco_custo := inter_fn_get_param_int(reg_fatu500.codigo_empresa, 'estoque.origemPrecoCusto');

         -- inicializa variaveis de controle, estas variaveis servirao para controlar
         -- se houve quebra de produto dentro do deposito
         v_nivel_estrutura_ant    := '#';
         v_grupo_estrutura_ant    := '#####';
         v_subgrupo_estrutura_ant := '###';
         v_item_estrutura_ant     := '######';

         v_processa_produto       := 's';
         -- le movimentos da ficha cardex

         for reg_estq300 in estq300(reg_fatu500.codigo_empresa, v_data_inicial)
         loop

            -- cria variaveis de mes e ano para filtro da dos complementos da rcnb_140
            v_mes_movto_140 := to_number(to_char(reg_estq300.data_movimento,'MM'));
            v_ano_movto_140 := to_number(to_char(reg_estq300.data_movimento,'YYYY'));

            -- rotina para gravar registros na tabela estq_301, tabela esta que guarda
            -- os valores de saldos, e valores de cada mes, por deposito/produto
            if v_nivel_estrutura_ant = '#' or v_data_movimento_ant is null
            then
               v_mes_movto_301 := to_number(to_char(reg_estq300.data_movimento,'MM'));
               v_ano_movto_301 := to_number(to_char(reg_estq300.data_movimento,'YYYY'));
               v_mes_ant_301 := v_mes_movto_301;
               v_ano_ant_301 := v_ano_movto_301;
            end if;

            if v_nivel_estrutura_ant <> '#'
            then

               -- se o produto for diferente do anterior, grava os dados do produto anterior na estq_301
               if reg_estq300.nivel_estrutura    <> v_nivel_estrutura_ant
               or reg_estq300.grupo_estrutura    <> v_grupo_estrutura_ant
               or reg_estq300.subgrupo_estrutura <> v_subgrupo_estrutura_ant
               or reg_estq300.item_estrutura     <> v_item_estrutura_ant
               then
                  grava_estq_301 := 's';
               end if;

               -- se a data de movimento for diferente da anterior, realiza checagens para verificar
               -- se deve gravar os dados na estq_301
               if v_data_movimento_ant is null
               or v_data_movimento_ant <> reg_estq300.data_movimento
               then

                  v_mes_movto_301 := to_number(to_char(reg_estq300.data_movimento,'MM'));
                  v_ano_movto_301 := to_number(to_char(reg_estq300.data_movimento,'YYYY'));

                  -- se o mes/ano for diferente do anterior, grava os dados do produto anterior na estq_301
                  if v_mes_movto_301 <> v_mes_ant_301 or v_ano_movto_301 <> v_ano_ant_301
                  then
                     grava_estq_301 := 's';
                  end if;
               end if;

               -- se for detectado que o produto ou o mes/ano for diferente da anterior, gava dados na estq_301
               if grava_estq_301 = 's'
               then

                  -- cria variavel data para servir de indice
                  v_mes_ano_301  := to_date('01/' || to_char(v_mes_ant_301, '00') || '/' || to_char(v_ano_ant_301, '0000'), 'dd/mm/yyyy');

                  -- para clientes que nao controlam kardex, onde as variaveis podem estourar valores
                  if v_ultimo_saldo_financeiro < -9999999999999 or v_ultimo_saldo_financeiro > 9999999999999
                  then v_ultimo_saldo_financeiro      := 1;
                  end if;

                  if v_ultimo_saldo_financeiro_est < -9999999999999 or v_ultimo_saldo_financeiro_est > 9999999999999
                  then v_ultimo_saldo_financeiro_est  := 1;
                  end if;

                  if v_ultimo_saldo_financeiro_proj < -9999999999999 or v_ultimo_saldo_financeiro_proj > 9999999999999
                  then v_ultimo_saldo_financeiro_proj := 1;
                  end if;

                  if v_ultimo_preco_medio < -9999999999999 or v_ultimo_preco_medio > 9999999999999
                  then v_ultimo_preco_medio      := 1;
                  end if;

                  if v_ultimo_preco_medio_est < -9999999999999 or v_ultimo_preco_medio_est > 9999999999999
                  then v_ultimo_preco_medio_est  := 1;
                  end if;

                  if v_ultimo_preco_medio_proj < -9999999999999 or v_ultimo_preco_medio_proj > 9999999999999
                  then v_ultimo_preco_medio_proj := 1;
                  end if;

                  if v_ultimo_preco_custo < -9999999999999 or v_ultimo_preco_custo > 9999999999999
                  then v_ultimo_preco_custo      := 1;
                  end if;

                  if v_ultimo_preco_custo_est < -9999999999999 or v_ultimo_preco_custo_est > 9999999999999
                  then v_ultimo_preco_custo_est  := 1;
                  end if;

                  if v_ultimo_preco_custo_proj < -9999999999999 or v_ultimo_preco_custo_proj > 9999999999999
                  then v_ultimo_preco_custo_proj := 1;
                  end if;

                  if v_ultimo_saldo_fisico < -9999999999999 or v_ultimo_saldo_fisico > 9999999999999
                  then v_ultimo_saldo_fisico := 1;
                  end if;

                  if v_ultimo_saldo_fisico_quilo < -9999999999999 or v_ultimo_saldo_fisico_quilo > 9999999999999
                  then v_ultimo_saldo_fisico_quilo := 1;
                  end if;

                  -- se o tipo da valorizacao for PRESUMIDA e o material for fabricado,
                  -- calcula o valor atraves do percentual em cima do maior valor da venda
                  if v_apuracao_ir = 2 and (reg_estq300.nivel_estrutura <> '9' and v_comprado_fabric = 2)
                  then
                     begin
                        v_ultimo_saldo_financeiro      := round(v_ultimo_saldo_fisico * v_prc_presum, 2);
                     exception
                        when others then
                           v_ultimo_saldo_financeiro := 1;
                     end;

                     v_ultimo_preco_medio           := v_prc_presum;
                     v_ultimo_preco_custo           := v_prc_presum;
                     begin
                        v_ultimo_saldo_financeiro_est  := round(v_ultimo_saldo_fisico * v_prc_presum, 2);
                     exception
                        when others then
                           v_ultimo_saldo_financeiro_est := 1;
                     end;
                     v_ultimo_preco_medio_est       := v_prc_presum;
                     v_ultimo_preco_custo_est       := v_prc_presum;
                     begin
                        v_ultimo_saldo_financeiro_proj := round(v_ultimo_saldo_fisico * v_prc_presum, 2);
                     exception
                        when others then
                           v_ultimo_saldo_financeiro_proj := 1;
                     end;
                     v_ultimo_preco_medio_proj      := v_prc_presum;
                     v_ultimo_preco_custo_proj      := v_prc_presum;
                  end if;

                  -- sistema tenta inserir valores guradados em memoria, se nao conseguir ele
                  -- tentara gravar por update
                  begin

                     insert into estq_301 (
                        codigo_deposito,                nivel_estrutura,           grupo_estrutura,
                        subgrupo_estrutura,             item_estrutura,            mes_movimento,
                        ano_movimento,                  mes_ano_movimento,         saldo_fisico,
                        saldo_financeiro,               preco_medio_unitario,      preco_custo_unitario,
                        saldo_financeiro_estimado,      preco_medio_unit_estimado, preco_custo_unit_estimado,
                        saldo_financeiro_proj,          preco_medio_unit_proj,     preco_custo_unit_proj,
                        saldo_fisico_quilo,             codigo_empresa)
                     values (
                        0,    v_nivel_estrutura_ant,     v_grupo_estrutura_ant,
                        v_subgrupo_estrutura_ant,       v_item_estrutura_ant,      v_mes_ant_301,
                        v_ano_ant_301,                  v_mes_ano_301,             v_ultimo_saldo_fisico,
                        v_ultimo_saldo_financeiro,      v_ultimo_preco_medio,      v_ultimo_preco_custo,
                        v_ultimo_saldo_financeiro_est,  v_ultimo_preco_medio_est,  v_ultimo_preco_custo_est,
                        v_ultimo_saldo_financeiro_proj, v_ultimo_preco_medio_proj, v_ultimo_preco_custo_proj,
                        v_ultimo_saldo_fisico_quilo,    reg_fatu500.codigo_empresa);

                  exception
                     when others then
                        update estq_301
                        set  mes_ano_movimento         = v_mes_ano_301,
                             saldo_fisico              = v_ultimo_saldo_fisico,
                             saldo_financeiro          = v_ultimo_saldo_financeiro,
                             preco_medio_unitario      = v_ultimo_preco_medio,
                             preco_custo_unitario      = v_ultimo_preco_custo,
                             saldo_financeiro_estimado = v_ultimo_saldo_financeiro_est,
                             preco_medio_unit_estimado = v_ultimo_preco_medio_est,
                             preco_custo_unit_estimado = v_ultimo_preco_custo_est,
                             saldo_financeiro_proj     = v_ultimo_saldo_financeiro_proj,
                             preco_medio_unit_proj     = v_ultimo_preco_medio_proj,
                             preco_custo_unit_proj     = v_ultimo_preco_custo_proj,
                             saldo_fisico_quilo        = v_ultimo_saldo_fisico_quilo
                        where codigo_deposito    = 0
                        and   nivel_estrutura    = v_nivel_estrutura_ant
                        and   grupo_estrutura    = v_grupo_estrutura_ant
                        and   subgrupo_estrutura = v_subgrupo_estrutura_ant
                        and   item_estrutura     = v_item_estrutura_ant
                        and   mes_movimento      = v_mes_ant_301
                        and   ano_movimento      = v_ano_ant_301
						and   codigo_empresa     = reg_fatu500.codigo_empresa;
                  end;
          commit;
               end if;
               v_mes_ant_301 := v_mes_movto_301;
               v_ano_ant_301 := v_ano_movto_301;
            end if;

            -- verifica se houve quebra do produto
            if reg_estq300.nivel_estrutura    <> v_nivel_estrutura_ant
            or reg_estq300.grupo_estrutura    <> v_grupo_estrutura_ant
            or reg_estq300.subgrupo_estrutura <> v_subgrupo_estrutura_ant
            or reg_estq300.item_estrutura     <> v_item_estrutura_ant
            then

               -- atribui as variaveis de controle com o produto lido
               v_nivel_estrutura_ant    := reg_estq300.nivel_estrutura;
               v_grupo_estrutura_ant    := reg_estq300.grupo_estrutura;
               v_subgrupo_estrutura_ant := reg_estq300.subgrupo_estrutura;
               v_item_estrutura_ant     := reg_estq300.item_estrutura;

-- inicio alterac?o kg
               v_total_movimento_pcpc_320 := 0.00;
               numero_documento_ant       := 0;
-- fim alterac?o kg


               -- encontra a conta de estoque e se o produto e comprado (1) ou fabricado (2)
               begin
                  select conta_estoque,   comprado_fabric,   ref_original
                  into   v_conta_estoque, v_comprado_fabric, v_ref_original
                  from basi_030
                  where nivel_estrutura = reg_estq300.nivel_estrutura
                  and   referencia      = reg_estq300.grupo_estrutura;

                  exception
                     when others
                     then
                        v_conta_estoque   := 0;
                        v_comprado_fabric := 1;
                        v_ref_original    := '00000';
               end;

               -- le os atributos de compras para identificar se o produto e comprado ou fabricado cra
               begin
                  select comprado_fabric
                  into   v_comprado_fabric_015
                  from basi_015
                  where basi_015.codigo_empresa   = reg_fatu500.codigo_empresa
                  and   basi_015.nivel_estrutura  = reg_estq300.nivel_estrutura
                  and   basi_015.grupo_estrutura  = reg_estq300.grupo_estrutura
                  and   basi_015.subgru_estrutura = reg_estq300.subgrupo_estrutura
                  and   basi_015.item_estrutura   = reg_estq300.item_estrutura;

               exception
                  when others then
                     v_comprado_fabric_015 := 0;
               end;

               if v_comprado_fabric_015 <> 0
               then
                  v_comprado_fabric := v_comprado_fabric_015;
               end if;

               -- se existir referencia original,  procura o prei??o de custo
               -- pela referencia original
               if v_ref_original = '00000' or reg_estq300.nivel_estrutura <> '1'
               then
                  v_ref_original := reg_estq300.grupo_estrutura;
               end if;

               -- se foi informado uma conta de estoque especifica,
               -- verifica se a conta informada e a mesma da conta lida no basi_030
               if p_conta_estoque <> 0 and p_conta_estoque <> v_conta_estoque
               then
                  v_processa_produto := 'n';
               end if;

               if v_processa_produto = 's'
               then
                  -- zera variavel de controle da quebra de data de movimento, esta
                  -- quebra serve para controlar a sequencia de controle da ficha
                  -- cardex (sequencia_ficha), bem como as variaveis de controle se
                  -- houve quebra do mes/ano do movimento, estas variaveis servem
                  -- para que a leitura das tabelas de custo sejam feitas
                  -- somente se houver quebra do mes/ano

                  v_data_movimento_ant := null;
                  v_mes_ant            := 0;
                  v_ano_ant            := 0;

                  -- chama a funcao para encontrar o saldo fisico, saldo financeiro e
                  -- o preco medio do ultimo movimento da estq_310 (movimentos ja fechados
                  -- de estoque) pelo deposito e produto.
                  -- esta funcao retorna valores por referencia, ou seja, no retorno da
                  -- funcao, ela trara os resultados pelas variaveis "saldo_fisico_anterior",
                  -- "saldo_financeiro_anterior" e "preco_medio_anterior"

                  inter_pr_procura_dados_cardex4(reg_fatu500.codigo_empresa,  reg_estq300.nivel_estrutura,
                                                reg_estq300.grupo_estrutura,  reg_estq300.subgrupo_estrutura,
                                                reg_estq300.item_estrutura,   v_data_inicial,
                                                v_saldo_fisico_anterior,      v_saldo_financeiro_anterior,
                                                v_preco_medio_anterior,       v_saldo_financeiro_ant_proj,
                                                v_preco_medio_anterior_proj,  v_saldo_financeiro_ant_est,
                                                v_preco_medio_anterior_est,   v_saldo_fisico_anterior_quilo);

                  -- se a valorizacao e calculada pelo PRESUMIDO, zera os valores encontrados no mes anterior
                  -- pois os movimentos n?o sao valorizados, apenas o estq_301 (apenas para material fabricado
                  if v_apuracao_ir = 2 and (reg_estq300.nivel_estrutura <> '9' and v_comprado_fabric = 2)
                  then
                     v_saldo_financeiro_anterior := 0.00;
                     v_preco_medio_anterior      := 0.00;
                     v_saldo_financeiro_ant_est  := 0.00;
                     v_preco_medio_anterior_est  := 0.00;
                     v_ultimo_preco_custo        := 0.00;
                     v_ultimo_preco_custo_est    := 0.00;
                     v_ultimo_preco_custo_proj   := 0.00;
                  end if;

                  -- inicializa variaveis para gravar registro no estq_301
                  v_ultimo_saldo_fisico         := v_saldo_fisico_anterior;
                  v_ultimo_saldo_fisico_quilo   := v_saldo_fisico_anterior_quilo;
                  v_ultimo_saldo_financeiro     := v_saldo_financeiro_anterior;
                  v_ultimo_preco_medio          := v_preco_medio_anterior;
                  v_ultimo_saldo_financeiro_est := v_saldo_financeiro_ant_est;
                  v_ultimo_preco_medio_est      := v_preco_medio_anterior_est;

                  -- busca o ultimo preco de custo do ultimo mes processado para o produto/deposito
                  -- apenas se o tipo de apuracao n?o for PRESUMIDO (ou for material comprado)
                  if not (v_apuracao_ir = 2 and (reg_estq300.nivel_estrutura <> '9' and v_comprado_fabric = 2))
                  then

                     begin
                        select estq_301.preco_custo_unitario , estq_301.preco_custo_unit_estimado,
                               estq_301.preco_custo_unit_proj
                        into   v_ultimo_preco_custo,           v_ultimo_preco_custo_est,
                               v_ultimo_preco_custo_proj
                        from estq_301, (

                           select max(mes_ano_movimento) MES_ANO_MOVIMENTO
                           from estq_301
                           where codigo_deposito    = reg_estq300.codigo_deposito
                           and   nivel_estrutura    = reg_estq300.nivel_estrutura
                           and   grupo_estrutura    = reg_estq300.grupo_estrutura
                           and   subgrupo_estrutura = reg_estq300.subgrupo_estrutura
                           and   item_estrutura     = reg_estq300.item_estrutura
                           and   preco_custo_unitario is not null
                           and   mes_ano_movimento  < trunc(reg_estq300.data_movimento, 'MM')) estq301

                        where estq_301.codigo_deposito    = reg_estq300.codigo_deposito
                        and   estq_301.nivel_estrutura    = reg_estq300.nivel_estrutura
                        and   estq_301.grupo_estrutura    = reg_estq300.grupo_estrutura
                        and   estq_301.subgrupo_estrutura = reg_estq300.subgrupo_estrutura
                        and   estq_301.item_estrutura     = reg_estq300.item_estrutura
                        and   estq_301.mes_ano_movimento  = estq301.mes_ano_movimento;

                     exception
                        when others then
                           v_ultimo_preco_custo      := 0.00;
                           v_ultimo_preco_custo_est  := 0.00;
                           v_ultimo_preco_custo_proj := 0.00;
                     end;
                  end if;
               end if;
            end if;

            if v_processa_produto = 's'
            then

               -- se houver quebra da data_movimento, entao reinicia o contador da
               -- sequencia da ficha cardex e verifica se precisa ler tabelas de custo
               if v_data_movimento_ant is null
               or v_data_movimento_ant <> reg_estq300.data_movimento
               then

                  v_data_movimento_ant := reg_estq300.data_movimento;
                  v_seq_ficha          := 1;

                  -- encontra preco do movimento com base na ficha de custos
                  v_mes_movimento := to_number(to_char(reg_estq300.data_movimento,'MM'));
                  v_ano_movimento := to_number(to_char(reg_estq300.data_movimento,'YYYY'));

                  -- se ni??o for material comprado, encontra o prei??o de custo, calculado
                  -- pela rotina de custos. Se for material comprado, o prei??o seri?? do
                  -- pri??prio movimento

                  if v_comprado_fabric = 2
                  then

                     if v_mes_movimento <> v_mes_ant or v_ano_movimento <> v_ano_ant
                     then
                        v_mes_ant := v_mes_movimento;
                        v_ano_ant := v_ano_movimento;

                        begin

                           -- se a empresa utiliza a valorizacao dos estoques pelo "PRESUMIDO"
                           -- le o maior preco de venda e aplica os percentual parametrizado
                           if v_apuracao_ir = 2
                           then
                              -- zera os valores de custos, pois os movimentos de cardex n?o ser?o
                              -- atualizados
                              v_novo_valor_custo_unit      := 0.000;
                              v_novo_valor_custo_unit_proj := 0.000;
                              v_novo_valor_custo_unit_est  := 0.000;

                              v_prc_presum := inter_fn_busca_maior_prc_venda(reg_fatu500.codigo_empresa,
                                                                             reg_estq300.data_movimento,
                                                                             reg_estq300.nivel_estrutura,
                                                                             v_ref_original,
                                                                             reg_estq300.subgrupo_estrutura,
                                                                             reg_estq300.item_estrutura,
                                                                             'S', -- Ira gravar a valorizacao na tabela rcnb_380
                                                                             2,   -- O Tipo de valorizac?o sera por deposito
                                                                             reg_estq300.codigo_deposito);

                           else

                              v_prc_presum := 0.00;

                              if p_origem_custo_fabr = 1 -- custo PADR?O (gerado pela FICHA DE CUSTOS - BASI_350)
                              then

                                 select basi_205.deposito_terceiro, basi_205.empresa_custo_gerencial
                                 into v_deposito_terceiro, v_empresa_custo_gerencial
                                 from basi_205
                                 where basi_205.codigo_deposito = reg_estq300.codigo_deposito;

                                 if v_deposito_terceiro = 1  -- e deposito de terceiro
                                 then

                                    -- le.custo.de.fabricacao.nao.incluindo.o.valor.da.materia.prima.de.terceiro
                                    select round(nvl(sum(valor_mo + valor_cp + valor_cd), 0.000),5),
                                           round(nvl(sum(valor_mo + valor_cp + valor_cd), 0.000),5)
                                    into   v_novo_valor_custo_unit,
                                           v_novo_valor_custo_unit_proj
                                    from basi_350
                                    where codigo_empresa   = reg_fatu500.codigo_empresa
                                    and   mes              = v_mes_movimento
                                    and   ano              = v_ano_movimento
                                    and   nivel_estrutura  = reg_estq300.nivel_estrutura
                                    and   grupo_estrutura  = v_ref_original
                                    and   subgru_estrutura = reg_estq300.subgrupo_estrutura
                                    and   item_estrutura   = reg_estq300.item_estrutura;

                                    select round(nvl(sum(valor_mo + valor_cp + valor_cd), 0.000),5)
                                    into   v_novo_valor_custo_unit_est
                                    from basi_350
                                    where codigo_empresa   = v_empresa_custo_gerencial
                                    and   mes              = v_mes_movimento
                                    and   ano              = v_ano_movimento
                                    and   nivel_estrutura  = reg_estq300.nivel_estrutura
                                    and   grupo_estrutura  = v_ref_original
                                    and   subgru_estrutura = reg_estq300.subgrupo_estrutura
                                    and   item_estrutura   = reg_estq300.item_estrutura;

                                    -- se nao encontrar registros nas tabelas de custo, ou se o valor encontrado
                                    -- for zero, procura o preco de custo do ultimo periodo calculado na ficha de custo
                                    if v_novo_valor_custo_unit = 0.000
                                    then

                                       -- encontra a ultima ficha de custo calculada
                                       select max(to_date('01/' || mes || '/' || ano, 'dd/mm/yyyy'))
                                       into   v_ultimo_calculo
                                       from basi_350
                                       where codigo_empresa   = reg_fatu500.codigo_empresa
                                       and   to_date('01/' || mes || '/' || ano, 'dd/mm/yyyy') < to_date('01/' || v_mes_movimento || '/' || v_ano_movimento, 'dd/mm/yyyy')
                                       and   nivel_estrutura  = reg_estq300.nivel_estrutura
                                       and   grupo_estrutura  = v_ref_original
                                       and   subgru_estrutura = reg_estq300.subgrupo_estrutura
                                       and   item_estrutura   = reg_estq300.item_estrutura;

                                       -- se encontrar, pega o preco de custo do periodo encontrado
                                       if v_ultimo_calculo is not null
                                       then
                                          v_mes_movimento2 := to_number(to_char(v_ultimo_calculo,'MM'));
                                          v_ano_movimento2 := to_number(to_char(v_ultimo_calculo,'YYYY'));

                                          select round(nvl(sum(valor_mo + valor_cp + valor_cd), 0.000),5)
                                          into   v_novo_valor_custo_unit
                                          from basi_350
                                          where codigo_empresa   = reg_fatu500.codigo_empresa
                                          and   mes              = v_mes_movimento2
                                          and   ano              = v_ano_movimento2
                                          and   nivel_estrutura  = reg_estq300.nivel_estrutura
                                          and   grupo_estrutura  = v_ref_original
                                          and   subgru_estrutura = reg_estq300.subgrupo_estrutura
                                          and   item_estrutura   = reg_estq300.item_estrutura;

                                          select round(nvl(sum(valor_mo + valor_cp + valor_cd), 0.000),5)
                                          into   v_novo_valor_custo_unit_est
                                          from basi_350
                                          where codigo_empresa   = v_empresa_custo_gerencial
                                          and   mes              = v_mes_movimento2
                                          and   ano              = v_ano_movimento2
                                          and   nivel_estrutura  = reg_estq300.nivel_estrutura
                                          and   grupo_estrutura  = v_ref_original
                                          and   subgru_estrutura = reg_estq300.subgrupo_estrutura
                                          and   item_estrutura   = reg_estq300.item_estrutura;
                                       end if;
                                    end if;
                                 else -- Deposito nao e de terceiro

                                    -- le.custo.de.fabricacao.incluindo.o.valor.da.materia.prima
                                    select nvl(sum(basi_350.custo_fabricacao), 0.000),
                                           nvl(sum(basi_350.custo_fabricacao_proj), 0.000)
                                    into   v_custo_fab_prev,
                                           v_custo_fab_proj
                                    from basi_350
                                    where codigo_empresa   = reg_fatu500.codigo_empresa
                                    and   mes              = v_mes_movimento
                                    and   ano              = v_ano_movimento
                                    and   nivel_estrutura  = reg_estq300.nivel_estrutura
                                    and   grupo_estrutura  = v_ref_original
                                    and   subgru_estrutura = reg_estq300.subgrupo_estrutura
                                    and   item_estrutura   = reg_estq300.item_estrutura;

                                    v_novo_valor_custo_unit      := round(v_custo_fab_prev,5);
                                    v_novo_valor_custo_unit_proj := round(v_custo_fab_proj,5);

                                    -- le.custo.de.fabricacao.incluindo.o.valor.da.materia.prima para EMPRESA CONSOLIDACAO (gerencial)
                                    select nvl(sum(basi_350.custo_fabricacao), 0.000)
                                    into   v_custo_fab_est
                                    from basi_350
                                    where codigo_empresa   = v_empresa_custo_gerencial
                                    and   mes              = v_mes_movimento
                                    and   ano              = v_ano_movimento
                                    and   nivel_estrutura  = reg_estq300.nivel_estrutura
                                    and   grupo_estrutura  = v_ref_original
                                    and   subgru_estrutura = reg_estq300.subgrupo_estrutura
                                    and   item_estrutura   = reg_estq300.item_estrutura;

                                    v_novo_valor_custo_unit_est := round(v_custo_fab_est,5);

                                    -- se nao encontrar registros nas tabelas de custo, ou se o valor encontrado
                                    -- for zero, procura o preco de custo do ultimo periodo calculado na ficha de custo
                                    if v_novo_valor_custo_unit = 0.000
                                    then

                                       -- encontra a ultima ficha de custo calculada
                                       select max(to_date('01/' || mes || '/' || ano, 'dd/mm/yyyy'))
                                       into   v_ultimo_calculo
                                       from basi_350
                                       where codigo_empresa   = reg_fatu500.codigo_empresa
                                       and   to_date('01/' || mes || '/' || ano, 'dd/mm/yyyy') < to_date('01/' || v_mes_movimento || '/' || v_ano_movimento, 'dd/mm/yyyy')
                                       and   nivel_estrutura  = reg_estq300.nivel_estrutura
                                       and   grupo_estrutura  = v_ref_original
                                       and   subgru_estrutura = reg_estq300.subgrupo_estrutura
                                       and   item_estrutura   = reg_estq300.item_estrutura;

                                       -- se encontrar, pega o preco de custo do periodo encontrado
                                       if v_ultimo_calculo is not null
                                       then

                                          v_mes_movimento2 := to_number(to_char(v_ultimo_calculo,'MM'));
                                          v_ano_movimento2 := to_number(to_char(v_ultimo_calculo,'YYYY'));

                                          select nvl(sum(basi_350.custo_fabricacao), 0.000),
                                                 nvl(sum(basi_350.custo_fabricacao_proj), 0.000)
                                          into   v_custo_fab_prev,
                                                 v_custo_fab_proj
                                          from basi_350
                                          where codigo_empresa   = reg_fatu500.codigo_empresa
                                          and   mes              = v_mes_movimento2
                                          and   ano              = v_ano_movimento2
                                          and   nivel_estrutura  = reg_estq300.nivel_estrutura
                                          and   grupo_estrutura  = v_ref_original
                                          and   subgru_estrutura = reg_estq300.subgrupo_estrutura
                                          and   item_estrutura   = reg_estq300.item_estrutura;

                                          v_novo_valor_custo_unit      := round(v_custo_fab_prev,5);
                                          v_novo_valor_custo_unit_proj := round(v_custo_fab_proj,5);

                                          select nvl(sum(basi_350.custo_fabricacao), 0.000)
                                          into   v_custo_fab_est
                                          from basi_350
                                          where codigo_empresa   = v_empresa_custo_gerencial
                                          and   mes              = v_mes_movimento2
                                          and   ano              = v_ano_movimento2
                                          and   nivel_estrutura  = reg_estq300.nivel_estrutura
                                          and   grupo_estrutura  = v_ref_original
                                          and   subgru_estrutura = reg_estq300.subgrupo_estrutura
                                          and   item_estrutura   = reg_estq300.item_estrutura;

                                          v_novo_valor_custo_unit_est := round(v_custo_fab_est,5);
                                       end if;
                                    end if;
                                 end if;
                              else -- custo REAL (gerado pela APURAC?O DO CUSTO REAL - RCNB_350)

                                 -- le.custo.de.fabricacao.incluindo.o.valor.da.materia.prima
                                 select nvl(sum(rcnb_350.custo_real_unit_fabric), 0.000),
                                        nvl(sum(rcnb_350.custo_real_unit_fabric), 0.000)
                                 into   v_custo_fab_prev,
                                        v_custo_fab_proj
                                 from rcnb_350
                                 where codigo_empresa   = reg_fatu500.codigo_empresa
                                 and   mes              = v_mes_movimento
                                 and   ano              = v_ano_movimento
                                 and   nivel_produto    = reg_estq300.nivel_estrutura
                                 and   grupo_produto    = v_ref_original
                                 and   sub_produto      = reg_estq300.subgrupo_estrutura
                                 and   item_produto     = reg_estq300.item_estrutura;

                                 v_novo_valor_custo_unit      := round(v_custo_fab_prev,5);
                                 v_novo_valor_custo_unit_proj := round(v_custo_fab_proj,5);

                                 -- se nao encontrar registros na tabela de custo REAL, ou se o valor encontrado
                                 -- for zero, procura o preco de custo do ultimo periodo calculado na Apurac?o do Custo Real
                                 if v_novo_valor_custo_unit = 0.000
                                 then

                                    -- encontra a ultima ficha de custo calculada
                                    select max(to_date('01/' || mes || '/' || ano, 'dd/mm/yyyy'))
                                    into   v_ultimo_calculo
                                    from rcnb_350
                                    where codigo_empresa   = reg_fatu500.codigo_empresa
                                    and   to_date('01/' || mes || '/' || ano, 'dd/mm/yyyy') < to_date('01/' || v_mes_movimento || '/' || v_ano_movimento, 'dd/mm/yyyy')
                                    and   nivel_produto = reg_estq300.nivel_estrutura
                                    and   grupo_produto = v_ref_original
                                    and   sub_produto   = reg_estq300.subgrupo_estrutura
                                    and   item_produto  = reg_estq300.item_estrutura;

                                    -- se encontrar, pega o preco de custo do periodo encontrado
                                    if v_ultimo_calculo is not null
                                    then

                                       v_mes_movimento2 := to_number(to_char(v_ultimo_calculo,'MM'));
                                       v_ano_movimento2 := to_number(to_char(v_ultimo_calculo,'YYYY'));

                                       select nvl(sum(rcnb_350.custo_real_unit_fabric), 0.000),
                                              nvl(sum(rcnb_350.custo_real_unit_fabric), 0.000)
                                       into   v_custo_fab_prev,
                                              v_custo_fab_proj
                                       from rcnb_350
                                       where codigo_empresa = reg_fatu500.codigo_empresa
                                       and   mes            = v_mes_movimento2
                                       and   ano            = v_ano_movimento2
                                       and   nivel_produto  = reg_estq300.nivel_estrutura
                                       and   grupo_produto  = v_ref_original
                                       and   sub_produto    = reg_estq300.subgrupo_estrutura
                                       and   item_produto   = reg_estq300.item_estrutura;

                                       v_novo_valor_custo_unit      := round(v_custo_fab_prev,5);
                                       v_novo_valor_custo_unit_proj := round(v_custo_fab_proj,5);
                                    end if;
                                 end if;
                              end if;

                              if v_novo_valor_custo_unit = 0.000
                              then
                                 select round(preco_custo,5) into v_novo_valor_custo_unit from basi_010
                                 where nivel_estrutura  = reg_estq300.nivel_estrutura
                                 and   grupo_estrutura  = reg_estq300.grupo_estrutura
                                 and   subgru_estrutura = reg_estq300.subgrupo_estrutura
                                 and   item_estrutura   = reg_estq300.item_estrutura;
                              end if;

                              if v_novo_valor_custo_unit_proj = 0.000
                              then
                                 v_novo_valor_custo_unit_proj := v_novo_valor_custo_unit;
                              end if;

                              if v_novo_valor_custo_unit_est = 0.000
                then
                                 v_novo_valor_custo_unit_est := v_novo_valor_custo_unit;
                              end if;

                           end if;   -- fim do "if" que indica se a valorizacao e' REAL ou PRESUMIDO

                           exception
                              when others then
                                 v_novo_valor_custo_unit      := 0.000;
                                 v_novo_valor_custo_unit_proj := 0.000;
                                 v_novo_valor_custo_unit_est  := 0.000;
                        end;
                     end if;
                  end if;   -- fim da rotina para encontrar o preco calculado pelo CUSTO
               else
                  begin
                     v_seq_ficha := v_seq_ficha + 1;
                  exception
                     when others then
                        v_seq_ficha := 1;
                  end;
               end if;

               -- calcula valores da ficha cardex
               if reg_estq300.tipo_e_s = 'E'   /* entrada */
               then

                                 -- se a transacao atualizar o preco medio, entao encontra o valor do
                  -- movimento unitario (preco_custo), sen?o, sera o valor do preco medio
                  -- anterior
                  if reg_estq300.calcula_preco = 1 -- atualiza preco medio
                  then

                     -- se nivel = 9 (comprado) ou se a empresa for loja e a origem do movimento
                     -- for nota fiscal de entrada (OBRF_015), entao o valor do movimento unitario
                     -- (preco_custo) sera o proprio movimento

                     if (v_comprado_fabric = 1 and reg_estq300.tabela_origem <> 'PCPC_320')
                     or (v_tipo_empresa    = 2 and reg_estq300.tabela_origem =  'OBRF_015')
                     or (reg_estq300.nivel_estrutura in ('1', '2', '4', '7','9') and reg_estq300.altera_custo  = 1 and reg_estq300.tabela_origem = 'OBRF_015')
                     or (reg_estq300.quantidade = 0.000 and reg_estq300.tabela_origem = 'ESTQ_300')
                     then


                        if reg_estq300.tabela_origem = 'OBRF_015'
                        then
                           v_novo_valor_movimento_unit  := round(reg_estq300.valor_movimento_unitario,5);
                           v_novo_valor_movto_unit_est  := round(reg_estq300.valor_movimento_unitario,5);
                           v_novo_valor_movto_unit_proj := round(reg_estq300.valor_movimento_unitario,5);
                           v_novo_valor_movimento_tot   := round(reg_estq300.valor_total,5);
                        else
                           v_novo_valor_movimento_unit  := round(reg_estq300.valor_movimento_unitario,5);
                           v_novo_valor_movto_unit_est  := round(reg_estq300.valor_movimento_unitario,5);
                           v_novo_valor_movto_unit_proj := round(reg_estq300.valor_movimento_unitario,5);

                           if reg_estq300.quantidade <> 0.00
                           then
                              begin
                                 v_novo_valor_movimento_tot := round(reg_estq300.valor_movimento_unitario * reg_estq300.quantidade,5);
                              exception
                                 when others then
                                    v_novo_valor_movimento_tot := 1;
                              end;
                           else
                              v_novo_valor_movimento_tot := round(reg_estq300.valor_movimento_unitario,5);
                           end if;
                        end if;
                     else

                        -- se a tabela de origem for 'PCPC_320' or 'PCPC_330', quer dizer que o movimento foi gerado
                        -- por uma transferencia de pecas para quilos, assim a valorizacao do movimento
                        -- e' com base nos movimentos dos produtos que originaram o movimento em quilo.
                        -- Por isso, e'  lido todos os movimentos da cardex que originou a transferncia
                        -- para quilo, sendo sua soma o valor total da movimentacao em quilo, e este
                        -- deve ser dividido pela quantidade (peso total) para se achar o valor unitario
                        -- do movimento
                        if reg_estq300.tabela_origem = 'PCPC_320' or reg_estq300.tabela_origem = 'PCPC_330'
                        then

                           --inicio alterac?o valorizac?o produtos de KG .
                           -- encontra o valor do movimento em quilo, tomando como base a soma de
                           -- todos os movimentos em pecas que originaram o movimento em quilo

                           -- ALTERAR PARA BUSCAR O CUSTO DO PRODUTO QUE ESTA RELACIONADO AO PRODUTO DE ESTOQUE DA PCPC_325
                           if v_total_movimento_pcpc_320 = 0.00 or reg_estq300.numero_documento <> numero_documento_ant
                           then

                             numero_documento_ant := reg_estq300.numero_documento;

                             v_valor_total_pcpc_320      := 0.00;
                             v_valor_total_pcpc_320_proj := 0.00;
                             v_qtde_total_pcpc_320       := 0.00;

                             begin
                               select pcpc_300.caracteristica
                               into   v_caracteristica
                               from pcpc_320, pcpc_300
                               where pcpc_320.cod_tipo_volume = pcpc_300.cod_tipo_volume
                                 and pcpc_320.numero_volume   = reg_estq300.numero_documento;
                             exception
                                 when no_data_found then
                                   v_caracteristica := 0;
                             end;

                             if v_caracteristica = 13
                             then

                               for pcpc325 in (select pcpc_325.deposito_saida_item
                                               from pcpc_325
                                               where pcpc_325.numero_volume         = reg_estq300.numero_documento
                                                 and pcpc_325.nivel                 = reg_estq300.nivel_estrutura
                                                 and pcpc_325.grupo                 = reg_estq300.grupo_estrutura
                                                 and pcpc_325.sub                   = reg_estq300.subgrupo_estrutura
                                                 and pcpc_325.item                  = reg_estq300.item_estrutura
                                                 and pcpc_325.deposito_entrada_item = reg_estq300.codigo_deposito)
                               loop

                                 -- encontra o valor do movimento em peca, tomando como base a soma de
                                 -- todos os movimentos de saida em pecas que originaram o movimento em quilo
                                 if lower(p_estq_300_estq_310) = 'estq_300'
                                 then
                                     select nvl(sum(estq_300.quantidade*estq_300.valor_movimento_unitario),0.00),
                                            nvl(sum(estq_300.quantidade*estq_300.valor_movimento_unitario),0.00),
                                            nvl(sum(estq_300.quantidade),0.00)
                                     into   v_valor_prod_pcpc_320,
                                            v_valor_prod_pcpc_320_proj,
                                            v_qtde_pcpc_320
                                     from estq_300, basi_030
                                     where  estq_300.codigo_deposito    = pcpc325.deposito_saida_item
                                       and  estq_300.nivel_estrutura    = reg_estq300.nivel_estrutura
                                       and  estq_300.grupo_estrutura    = reg_estq300.grupo_estrutura
                                       and  estq_300.subgrupo_estrutura = reg_estq300.subgrupo_estrutura
                                       and (estq_300.item_estrutura     = reg_estq300.item_estrutura
                                        or  basi_030.cor_de_estoque     = estq_300.item_estrutura)
                                       and  basi_030.nivel_estrutura    = estq_300.nivel_estrutura
                                       and  basi_030.referencia         = estq_300.grupo_estrutura
                                       and  estq_300.numero_documento   = reg_estq300.numero_documento
                                       and  estq_300.tabela_origem      in  ('PCPC_320','PCPC_330')
                                       and  estq_300.entrada_saida      = 'S';
                                 else
                                     select nvl(sum(estq_310.quantidade*estq_310.valor_movimento_unitario),0.00),
                                            nvl(sum(estq_310.quantidade*estq_310.valor_movimento_unitario),0.00),
                                            nvl(sum(estq_310.quantidade),0.00)
                                     into   v_valor_prod_pcpc_320,
                                            v_valor_prod_pcpc_320_proj,
                                            v_qtde_pcpc_320
                                     from estq_310, basi_030
                                     where  estq_310.codigo_deposito    = pcpc325.deposito_saida_item
                                       and  estq_310.nivel_estrutura    = reg_estq300.nivel_estrutura
                                       and  estq_310.grupo_estrutura    = reg_estq300.grupo_estrutura
                                       and  estq_310.subgrupo_estrutura = reg_estq300.subgrupo_estrutura
                                       and (estq_310.item_estrutura     = reg_estq300.item_estrutura
                                        or  basi_030.cor_de_estoque     = estq_310.item_estrutura)
                                       and  basi_030.nivel_estrutura    = estq_310.nivel_estrutura
                                       and  basi_030.referencia         = estq_310.grupo_estrutura
                                       and  estq_310.numero_documento   = reg_estq300.numero_documento
                                       and  estq_310.tabela_origem      in  ('PCPC_320','PCPC_330')
                                       and  estq_310.entrada_saida      = 'S';
                                 end if;

                                 begin
                                    v_valor_total_pcpc_320      := v_valor_total_pcpc_320      + v_valor_prod_pcpc_320;
                                 exception
                                    when others then
                                       v_valor_total_pcpc_320 := 1;
                                 end;
                                 begin
                                    v_valor_total_pcpc_320_proj := v_valor_total_pcpc_320_proj + v_valor_prod_pcpc_320_proj;
                                 exception
                                    when others then
                                       v_valor_total_pcpc_320_proj := 1;
                                 end;
                                 begin
                                    v_qtde_total_pcpc_320       := v_qtde_total_pcpc_320       + v_qtde_pcpc_320;
                                 exception
                                    when others then
                                       v_qtde_total_pcpc_320 := 1;
                                 end;
                               end loop;

                               if v_qtde_total_pcpc_320 > 0.00 -- valor unitario total da movimentac?o em kg
                               then
                                  begin
                                     v_total_movimento_pcpc_320  := v_valor_total_pcpc_320      / v_qtde_total_pcpc_320;
                                  exception
                                     when others then
                                        v_total_movimento_pcpc_320 := 1;
                                  end;
                                  begin
                                     v_total_movto_pcpc_320_proj := v_valor_total_pcpc_320_proj / v_qtde_total_pcpc_320;
                                  exception
                                     when others then
                                        v_total_movto_pcpc_320_proj := 1;
                                  end;
                               else
                                  v_total_movimento_pcpc_320  := v_valor_total_pcpc_320;
                                  v_total_movto_pcpc_320_proj := v_valor_total_pcpc_320_proj;
                               end if;

                             else

                               -- VERIFICA SE A CAIXA FOI CRIADA NESTE PERIODO DE ESTOQUE
                               begin
                                 select 1
                                 into   v_montagem_volume
                                 from pcpc_320
                                 where pcpc_320.numero_volume = reg_estq300.numero_documento
                                   and pcpc_320.data_montagem >= trunc(reg_estq300.data_movimento,'DD');
                               exception
                                 when no_data_found then
                                   v_montagem_volume := 0;
                               end;

                               if v_montagem_volume = 1 -- QUANDO O VOLUME FOI CRIADO NO PERIODO
                               then
                                 -- QUANDO O VOLUME FOI CRIADO NO PERIODO ATUAL
                                 -- encontra o valor do movimento em quilo, tomando como base a soma de
                                 -- todos os movimentos em pecas que originaram o movimento em quilo


                                 for pcpc325 in (select pcpc_325.nivel, pcpc_325.grupo,
                                                        pcpc_325.sub,
                                                        decode(basi_030.cor_de_estoque, '000000', pcpc_325.item, basi_030.cor_de_estoque) ITEM
                                                 from pcpc_325, pcpc_320, pcpc_300, basi_030
                                                 where basi_030.nivel_estrutura = pcpc_325.nivel
                                                   and basi_030.referencia      = pcpc_325.grupo
                                                   and pcpc_320.numero_volume   = pcpc_325.numero_volume
                                                   and pcpc_320.cod_tipo_volume = pcpc_300.cod_tipo_volume
                                                   and pcpc_300.caracteristica  = 0
                                                   and pcpc_320.numero_volume   = reg_estq300.numero_documento
                                                   and pcpc_325.nivel_estoque   = reg_estq300.nivel_estrutura
                                                   and pcpc_325.grupo_estoque   = reg_estq300.grupo_estrutura
                                                   and pcpc_325.sub_estoque     = reg_estq300.subgrupo_estrutura
                                                   and pcpc_325.item_estoque    = reg_estq300.item_estrutura
                                                   and pcpc_325.qtde_quilos_estoque > 0
                                                  group by pcpc_325.nivel, pcpc_325.grupo, pcpc_325.sub,
                                                           decode(basi_030.cor_de_estoque, '000000', pcpc_325.item, basi_030.cor_de_estoque)
                                                   )
                                 loop

                                    -- encontra o valor do movimento em peca, tomando como base a soma de
                                    -- todos os movimentos de saida em pecas que originaram o movimento em quilo
                                    if lower(p_estq_300_estq_310) = 'estq_300'
                                    then
                                        select nvl(sum(estq_300.valor_total),0.00),
                                               nvl(sum(estq_300.valor_total),0.00),
                                               nvl(sum(estq_300.quantidade),0.00)
                                        into   v_valor_prod_pcpc_320,
                                               v_valor_prod_pcpc_320_proj,
                                               v_qtde_total_pcpc_320
                                        from estq_300, basi_030
                                        where estq_300.numero_documento   = reg_estq300.numero_documento
                                         and  estq_300.nivel_estrutura    = pcpc325.nivel --PECA
                                         and  estq_300.grupo_estrutura    = pcpc325.grupo
                                         and  estq_300.subgrupo_estrutura = pcpc325.sub
                                         and (estq_300.item_estrutura     = pcpc325.item
                                          or  basi_030.cor_de_estoque     = estq_300.item_estrutura)
                                         and  basi_030.nivel_estrutura    = estq_300.nivel_estrutura
                                         and  basi_030.referencia         = estq_300.grupo_estrutura
                                         and  estq_300.tabela_origem      in  ('PCPC_320','PCPC_330')
                                         and  estq_300.entrada_saida      = 'S';
                                    else
                                        select nvl(sum(estq_310.valor_total),0.00),
                                               nvl(sum(estq_310.valor_total),0.00),
                                               nvl(sum(estq_310.quantidade),0.00)
                                        into   v_valor_prod_pcpc_320,
                                               v_valor_prod_pcpc_320_proj,
                                               v_qtde_total_pcpc_320
                                        from estq_310, basi_030
                                        where estq_310.numero_documento   = reg_estq300.numero_documento
                                         and  estq_310.nivel_estrutura    = pcpc325.nivel --PECA
                                         and  estq_310.grupo_estrutura    = pcpc325.grupo
                                         and  estq_310.subgrupo_estrutura = pcpc325.sub
                                         and (estq_310.item_estrutura     = pcpc325.item
                                          or  basi_030.cor_de_estoque     = estq_310.item_estrutura)
                                         and  basi_030.nivel_estrutura    = estq_310.nivel_estrutura
                                         and  basi_030.referencia         = estq_310.grupo_estrutura
                                         and  estq_310.tabela_origem      in  ('PCPC_320','PCPC_330')
                                         and  estq_310.entrada_saida      = 'S';
                                    end if;

                                    begin
                                       v_valor_total_pcpc_320      := v_valor_total_pcpc_320      + v_valor_prod_pcpc_320;
                                    exception
                                       when others then
                                          v_valor_total_pcpc_320 := 1;
                                    end;
                                    begin
                                       v_valor_total_pcpc_320_proj := v_valor_total_pcpc_320_proj + v_valor_prod_pcpc_320_proj;
                                    exception
                                       when others then
                                          v_valor_total_pcpc_320_proj := 1;
                                    end;
                                 end loop;
                               end if;

                               if reg_estq300.quantidade > 0.00 -- valor unitario total da movimentac?o em kg
                               then
                                  begin
                                     v_total_movimento_pcpc_320  := v_valor_total_pcpc_320      / reg_estq300.quantidade;
                                  exception
                                     when others then
                                        v_total_movimento_pcpc_320 := 1;
                                  end;
                                  begin
                                     v_total_movto_pcpc_320_proj := v_valor_total_pcpc_320_proj / reg_estq300.quantidade;
                                  exception
                                     when others then
                                        v_total_movto_pcpc_320_proj := 1;
                                  end;
                               else
                                  v_total_movimento_pcpc_320  := v_valor_total_pcpc_320;
                                  v_total_movto_pcpc_320_proj := v_valor_total_pcpc_320_proj;
                               end if;

                             end if;
                           end if;

                           if v_total_movimento_pcpc_320 > 0.0
                           then

                              if reg_estq300.quantidade > 0.00
                              then
                                 v_novo_valor_movimento_unit  := round(v_total_movimento_pcpc_320,5);
                                 v_novo_valor_movto_unit_est  := round(v_total_movimento_pcpc_320,5);
                                 v_novo_valor_movto_unit_proj := round(v_total_movto_pcpc_320_proj,5);
                                 begin
                                    v_novo_valor_movimento_tot   := round(v_total_movimento_pcpc_320 * reg_estq300.quantidade,5);
                                 exception
                                    when others then
                                       v_novo_valor_movimento_tot := 1;
                                 end;
                              else
                                 v_novo_valor_movimento_unit  := round(v_total_movimento_pcpc_320,5);
                                 v_novo_valor_movto_unit_est  := round(v_total_movimento_pcpc_320,5);
                                 v_novo_valor_movto_unit_proj := round(v_total_movto_pcpc_320_proj,5);
                                 v_novo_valor_movimento_tot   := round(v_total_movimento_pcpc_320,5);
                              end if;

                           else
                              v_novo_valor_movimento_unit  := v_novo_valor_custo_unit;
                              v_novo_valor_movto_unit_est  := v_novo_valor_custo_unit_est;
                              v_novo_valor_movto_unit_proj := v_novo_valor_custo_unit_proj;
                              begin
                                 v_novo_valor_movimento_tot   := round(v_novo_valor_custo_unit * reg_estq300.quantidade,5);
                              exception
                                 when others then
                                    v_novo_valor_movimento_tot := 1;
                              end;
                           end if;
--fim alterac?o  valorizac?o produtos de KG .

                        else

                           -- se a tabela de origem for 'OBRF_015', quer dizer que o movimento foi gerado
                           -- por uma Nota Fiscal de Entrada, assim a valorizacao do movimento sera com
                           -- base no cadastro de valores de entradas OBRF_950. Caso n?o tenha esse cadastro
                           -- o movimento sera valorizado pelo proprio valor de custo

                           if (reg_estq300.tabela_origem = 'ESTQ_060' or reg_estq300.tabela_origem = 'OBRF_015') and reg_estq300.nivel_estrutura = '7'
                           then
                              select nvl(max(obrf_950.data_vigencia),null) into v_data_vigencia
                              from obrf_950
                              where obrf_950.nivel_produto     = reg_estq300.nivel_estrutura
                                and obrf_950.grupo_produto     = reg_estq300.grupo_estrutura
                                and obrf_950.subgru_produto    = reg_estq300.subgrupo_estrutura
                                and obrf_950.item_produto      = reg_estq300.item_estrutura
                                and obrf_950.codigo_transacao  = reg_estq300.codigo_transacao
                                and obrf_950.data_vigencia    <= reg_estq300.data_movimento;

                              if v_data_vigencia is not null
                              then
                                 select obrf_950.valor_entrada,       obrf_950.valor_entrada
                                 into   v_novo_valor_movimento_unit,  v_novo_valor_movto_unit_proj
                                 from obrf_950
                                 where obrf_950.nivel_produto     = reg_estq300.nivel_estrutura
                                   and obrf_950.grupo_produto     = reg_estq300.grupo_estrutura
                                   and obrf_950.subgru_produto    = reg_estq300.subgrupo_estrutura
                                   and obrf_950.item_produto      = reg_estq300.item_estrutura
                                   and obrf_950.codigo_transacao  = reg_estq300.codigo_transacao
                                   and obrf_950.data_vigencia     = v_data_vigencia;

                                 begin
                                    v_novo_valor_movimento_tot   := round(v_novo_valor_movimento_unit * reg_estq300.quantidade,5);
                                 exception
                                    when others then
                                       v_novo_valor_movimento_tot := 1;
                                 end;
                              else
                                 v_novo_valor_movimento_unit  := v_novo_valor_custo_unit;
                                 v_novo_valor_movto_unit_est  := v_novo_valor_custo_unit;
                                 v_novo_valor_movto_unit_proj := v_novo_valor_custo_unit_proj;
                                 begin
                                    v_novo_valor_movimento_tot   := round(v_novo_valor_custo_unit * reg_estq300.quantidade,5);
                                 exception
                                    when others then
                                       v_novo_valor_movimento_tot := 1;
                                 end;
                              end if;
                           else
                              v_novo_valor_movimento_unit  := v_novo_valor_custo_unit;
                              v_novo_valor_movto_unit_est  := v_novo_valor_custo_unit_est;
                              v_novo_valor_movto_unit_proj := v_novo_valor_custo_unit_proj;

                              if reg_estq300.quantidade <> 0.00
                              then
                                 begin
                                    v_novo_valor_movimento_tot := round(v_novo_valor_custo_unit * reg_estq300.quantidade,5);
                                 exception
                                    when others then
                                       v_novo_valor_movimento_tot := 1;
                                 end;
                              else
                                 v_novo_valor_movimento_tot := round(v_novo_valor_custo_unit,5);
                              end if;
                           end if;
                        end if;
                     end if;

                     -- Le o valor cadastrado para o Sub-Produto na tela
                     -- Atualizac?o de Valores dos Sub-Produtos (basi_f432)

                     begin
                        select basi_305.valor_subproduto
                        into   v_valor_subproduto
                        from basi_305
                        where basi_305.codigo_empresa    = reg_fatu500.codigo_empresa
                          and basi_305.mes               = v_mes_movimento
                          and basi_305.ano               = v_ano_movimento
                          and basi_305.nivel_subproduto  = reg_estq300.nivel_estrutura
                          and basi_305.grupo_subproduto  = reg_estq300.grupo_estrutura
                          and basi_305.subgru_subproduto = reg_estq300.subgrupo_estrutura
                          and basi_305.item_subproduto   = reg_estq300.item_estrutura
                          and exists (select * from basi_300
                                      where basi_300.nivel_subproduto  = basi_305.nivel_subproduto
                                        and basi_300.grupo_subproduto  = basi_305.grupo_subproduto
                                        and basi_300.subgru_subproduto = basi_305.subgru_subproduto
                                        and basi_300.item_subproduto   = basi_305.item_subproduto);
                        exception
                           when OTHERS
                           then v_valor_subproduto := 0.00;
                     end;

                     -- Verifica se a valorizac?o das entradas dos Sub-Produtos
                     -- deve considerar o (1) Preco Medio do movimento anterior ou
                     -- o (2) Valor informado na tela de Sub-Produtos (basi_f432).
                     -- Este e um Parametro global de Estoques (aba '++').

                     if v_valorizacao_cardex = 2 and v_valor_subproduto > 0
                     then
                        v_novo_valor_movimento_unit  := round(v_valor_subproduto,5);
                        v_novo_valor_movto_unit_est  := round(v_valor_subproduto,5);
                        v_novo_valor_movto_unit_proj := round(v_valor_subproduto,5);
                        begin
                           v_novo_valor_movimento_tot   := round(v_valor_subproduto * reg_estq300.quantidade,5);
                        exception
                           when others then
                              v_novo_valor_movimento_tot := 1;
                        end;
                     end if;

                  else
                     v_novo_valor_movimento_unit  := round(v_preco_medio_anterior,5);      -- nao atualiza preco medio
                     v_novo_valor_movto_unit_est  := round(v_preco_medio_anterior,5);      -- nao atualiza preco medio
                     v_novo_valor_movto_unit_proj := round(v_preco_medio_anterior_proj,5); -- nao atualiza preco medio
                     begin
                        v_novo_valor_movimento_tot   := round(v_preco_medio_anterior * reg_estq300.quantidade,5); -- nao atualiza preco medio
                     exception
                        when others then
                           v_novo_valor_movimento_tot := 1;
                     end;
                  end if;

                  v_tipo_aplic_140   := 0;
                  v_valor_indice_140 := 0.00;

                  select count(*) into v_tem_reg_140
                  from rcnb_140
                  where (rcnb_140.nr_nota_fiscal    = reg_estq300.numero_documento    or rcnb_140.nr_nota_fiscal    = 0)
                    and (rcnb_140.serie_nota_fiscal = reg_estq300.serie_documento     or rcnb_140.serie_nota_fiscal = ' ' or ltrim(rtrim(rcnb_140.serie_nota_fiscal)) is null)
                    and (rcnb_140.seq_nota_fiscal   = reg_estq300.sequencia_documento or rcnb_140.seq_nota_fiscal   = 0)
                    and (rcnb_140.cnpj_fornecedor9  = reg_estq300.cnpj_9              or rcnb_140.cnpj_fornecedor9  = 0)
                    and (rcnb_140.cnpj_fornecedor4  = reg_estq300.cnpj_4              or rcnb_140.cnpj_fornecedor4  = 0)
                    and (rcnb_140.cnpj_fornecedor2  = reg_estq300.cnpj_2              or rcnb_140.cnpj_fornecedor2  = 0)
                    and (rcnb_140.nivel_produto     = reg_estq300.nivel_estrutura     or rcnb_140.nivel_produto     = 'X')
                    and (rcnb_140.grupo_produto     = reg_estq300.grupo_estrutura     or rcnb_140.grupo_produto     = 'XXXXX')
                    and (rcnb_140.subgru_produto    = reg_estq300.subgrupo_estrutura  or rcnb_140.subgru_produto    = 'XXX')
                    and (rcnb_140.item_produto      = reg_estq300.item_estrutura      or rcnb_140.item_produto      = 'XXXXXX')
                    and  rcnb_140.mes_periodo       = v_mes_movto_140
                    and  rcnb_140.ano_periodo       = v_ano_movto_140;

                  if v_tem_reg_140 > 0
                  then

                     begin

                        select rcnb_140.tipo_aplicacao,    rcnb_140.valor_indice
                        into   v_tipo_aplic_140,           v_valor_indice_140
                        from rcnb_140
                        where rcnb_140.nr_nota_fiscal    = reg_estq300.numero_documento
                          and rcnb_140.serie_nota_fiscal = reg_estq300.serie_documento
                          and rcnb_140.seq_nota_fiscal   = reg_estq300.sequencia_documento
                          and rcnb_140.cnpj_fornecedor9  = reg_estq300.cnpj_9
                          and rcnb_140.cnpj_fornecedor4  = reg_estq300.cnpj_4
                          and rcnb_140.cnpj_fornecedor2  = reg_estq300.cnpj_2
                          and rcnb_140.mes_periodo       = v_mes_movto_140
                          and rcnb_140.ano_periodo       = v_ano_movto_140;

                        exception
                        when no_data_found
                        then begin

                           select rcnb_140.tipo_aplicacao,    rcnb_140.valor_indice
                           into   v_tipo_aplic_140,           v_valor_indice_140
                           from rcnb_140
                           where rcnb_140.nr_nota_fiscal    = reg_estq300.numero_documento
                             and rcnb_140.serie_nota_fiscal = reg_estq300.serie_documento
                             and rcnb_140.seq_nota_fiscal   = reg_estq300.sequencia_documento
                             and rcnb_140.cnpj_fornecedor9  = 0
                             and rcnb_140.cnpj_fornecedor4  = 0
                             and rcnb_140.cnpj_fornecedor2  = 0
                             and rcnb_140.mes_periodo       = v_mes_movto_140
                             and rcnb_140.ano_periodo       = v_ano_movto_140;

                           exception
                           when no_data_found
                           then begin

                              select rcnb_140.tipo_aplicacao,    rcnb_140.valor_indice
                              into   v_tipo_aplic_140,           v_valor_indice_140
                              from rcnb_140
                              where rcnb_140.nr_nota_fiscal    = reg_estq300.numero_documento
                                and rcnb_140.serie_nota_fiscal = reg_estq300.serie_documento
                                and rcnb_140.seq_nota_fiscal   = 0
                                and rcnb_140.cnpj_fornecedor9  = 0
                                and rcnb_140.cnpj_fornecedor4  = 0
                                and rcnb_140.cnpj_fornecedor2  = 0
                                and rcnb_140.mes_periodo       = v_mes_movto_140
                                and rcnb_140.ano_periodo       = v_ano_movto_140;

                              exception
                              when no_data_found
                              then begin

                                 select rcnb_140.tipo_aplicacao,    rcnb_140.valor_indice
                                 into   v_tipo_aplic_140,           v_valor_indice_140
                                 from rcnb_140
                                 where rcnb_140.nr_nota_fiscal    = reg_estq300.numero_documento
                                   and rcnb_140.serie_nota_fiscal = reg_estq300.serie_documento
                                   and rcnb_140.seq_nota_fiscal   = 0
                                   and rcnb_140.cnpj_fornecedor9  = reg_estq300.cnpj_9
                                   and rcnb_140.cnpj_fornecedor4  = reg_estq300.cnpj_4
                                   and rcnb_140.cnpj_fornecedor2  = reg_estq300.cnpj_2
                                   and rcnb_140.nivel_produto     = reg_estq300.nivel_estrutura
                                   and rcnb_140.grupo_produto     = reg_estq300.grupo_estrutura
                                   and rcnb_140.subgru_produto    = reg_estq300.subgrupo_estrutura
                                   and rcnb_140.item_produto      = 'XXXXXX'
                                   and rcnb_140.mes_periodo       = v_mes_movto_140
                                   and rcnb_140.ano_periodo       = v_ano_movto_140;

                                 exception
                                 when no_data_found
                                 then begin

                                    select rcnb_140.tipo_aplicacao,    rcnb_140.valor_indice
                                    into   v_tipo_aplic_140,           v_valor_indice_140
                                    from rcnb_140
                                    where rcnb_140.nr_nota_fiscal    = reg_estq300.numero_documento
                                      and rcnb_140.serie_nota_fiscal = reg_estq300.serie_documento
                                      and rcnb_140.seq_nota_fiscal   = 0
                                      and rcnb_140.cnpj_fornecedor9  = reg_estq300.cnpj_9
                                      and rcnb_140.cnpj_fornecedor4  = reg_estq300.cnpj_4
                                      and rcnb_140.cnpj_fornecedor2  = reg_estq300.cnpj_2
                                      and rcnb_140.nivel_produto     = reg_estq300.nivel_estrutura
                                      and rcnb_140.grupo_produto     = reg_estq300.grupo_estrutura
                                      and rcnb_140.subgru_produto    = 'XXX'
                                      and rcnb_140.item_produto      = 'XXXXXX'
                                      and rcnb_140.mes_periodo       = v_mes_movto_140
                                      and rcnb_140.ano_periodo       = v_ano_movto_140;

                                    exception
                                    when no_data_found
                                    then begin

                                       select rcnb_140.tipo_aplicacao,    rcnb_140.valor_indice
                                       into   v_tipo_aplic_140,           v_valor_indice_140
                                       from rcnb_140
                                       where rcnb_140.nr_nota_fiscal    = reg_estq300.numero_documento
                                         and rcnb_140.serie_nota_fiscal = reg_estq300.serie_documento
                                         and rcnb_140.seq_nota_fiscal   = 0
                                         and rcnb_140.cnpj_fornecedor9  = reg_estq300.cnpj_9
                                         and rcnb_140.cnpj_fornecedor4  = reg_estq300.cnpj_4
                                         and rcnb_140.cnpj_fornecedor2  = reg_estq300.cnpj_2
                                         and rcnb_140.nivel_produto     = reg_estq300.nivel_estrutura
                                         and rcnb_140.grupo_produto     = 'XXXXX'
                                         and rcnb_140.subgru_produto    = 'XXX'
                                         and rcnb_140.item_produto      = 'XXXXXX'
                                         and rcnb_140.mes_periodo       = v_mes_movto_140
                                         and rcnb_140.ano_periodo       = v_ano_movto_140;

                                       exception
                                       when no_data_found
                                       then begin

                                          select rcnb_140.tipo_aplicacao,    rcnb_140.valor_indice
                                          into   v_tipo_aplic_140,           v_valor_indice_140
                                          from rcnb_140
                                          where rcnb_140.nr_nota_fiscal    = 0
                                            and rcnb_140.seq_nota_fiscal   = 0
                                            and rcnb_140.cnpj_fornecedor9  = reg_estq300.cnpj_9
                                            and rcnb_140.cnpj_fornecedor4  = reg_estq300.cnpj_4
                                            and rcnb_140.cnpj_fornecedor2  = reg_estq300.cnpj_2
                                            and rcnb_140.nivel_produto     = reg_estq300.nivel_estrutura
                                            and rcnb_140.grupo_produto     = reg_estq300.grupo_estrutura
                                            and rcnb_140.subgru_produto    = reg_estq300.subgrupo_estrutura
                                            and rcnb_140.item_produto      = reg_estq300.item_estrutura
                                            and rcnb_140.mes_periodo       = v_mes_movto_140
                                            and rcnb_140.ano_periodo       = v_ano_movto_140;

                                          exception
                                          when no_data_found
                                          then begin

                                             select rcnb_140.tipo_aplicacao,    rcnb_140.valor_indice
                                             into   v_tipo_aplic_140,           v_valor_indice_140
                                             from rcnb_140
                                             where rcnb_140.nr_nota_fiscal    = 0
                                               and rcnb_140.seq_nota_fiscal   = 0
                                               and rcnb_140.cnpj_fornecedor9  = reg_estq300.cnpj_9
                                               and rcnb_140.cnpj_fornecedor4  = reg_estq300.cnpj_4
                                               and rcnb_140.cnpj_fornecedor2  = reg_estq300.cnpj_2
                                               and rcnb_140.nivel_produto     = 'X'
                                               and rcnb_140.grupo_produto     = 'XXXXX'
                                               and rcnb_140.subgru_produto    = 'XXX'
                                               and rcnb_140.item_produto      = 'XXXXXX'
                                               and rcnb_140.mes_periodo       = v_mes_movto_140
                                               and rcnb_140.ano_periodo       = v_ano_movto_140;

                                             exception
                                             when no_data_found
                                             then begin

                                                select rcnb_140.tipo_aplicacao,    rcnb_140.valor_indice
                                                into   v_tipo_aplic_140,           v_valor_indice_140
                                                from rcnb_140
                                                where rcnb_140.nr_nota_fiscal    = 0
                                                  and rcnb_140.seq_nota_fiscal   = 0
                                                  and rcnb_140.cnpj_fornecedor9  = 0
                                                  and rcnb_140.cnpj_fornecedor4  = 0
                                                  and rcnb_140.cnpj_fornecedor2  = 0
                                                  and rcnb_140.nivel_produto     = reg_estq300.nivel_estrutura
                                                  and rcnb_140.grupo_produto     = reg_estq300.grupo_estrutura
                                                  and rcnb_140.subgru_produto    = reg_estq300.subgrupo_estrutura
                                                  and rcnb_140.item_produto      = reg_estq300.item_estrutura
                                                  and rcnb_140.mes_periodo       = v_mes_movto_140
                                                  and rcnb_140.ano_periodo       = v_ano_movto_140;

                                                exception
                                                when no_data_found
                                                then begin

                                                   select rcnb_140.tipo_aplicacao,    rcnb_140.valor_indice
                                                   into   v_tipo_aplic_140,           v_valor_indice_140
                                                   from rcnb_140
                                                   where rcnb_140.nr_nota_fiscal    = 0
                                                     and rcnb_140.seq_nota_fiscal   = 0
                                                     and rcnb_140.cnpj_fornecedor9  = 0
                                                     and rcnb_140.cnpj_fornecedor4  = 0
                                                     and rcnb_140.cnpj_fornecedor2  = 0
                                                     and rcnb_140.nivel_produto     = reg_estq300.nivel_estrutura
                                                     and rcnb_140.grupo_produto     = reg_estq300.grupo_estrutura
                                                     and rcnb_140.subgru_produto    = reg_estq300.subgrupo_estrutura
                                                     and rcnb_140.item_produto      = 'XXXXXX'
                                                     and rcnb_140.mes_periodo       = v_mes_movto_140
                                                     and rcnb_140.ano_periodo       = v_ano_movto_140;

                                                   exception
                                                   when no_data_found
                                                   then begin

                                                      select rcnb_140.tipo_aplicacao,    rcnb_140.valor_indice
                                                      into   v_tipo_aplic_140,           v_valor_indice_140
                                                      from rcnb_140
                                                      where rcnb_140.nr_nota_fiscal    = 0
                                                        and rcnb_140.seq_nota_fiscal   = 0
                                                        and rcnb_140.cnpj_fornecedor9  = 0
                                                        and rcnb_140.cnpj_fornecedor4  = 0
                                                        and rcnb_140.cnpj_fornecedor2  = 0
                                                        and rcnb_140.nivel_produto     = reg_estq300.nivel_estrutura
                                                        and rcnb_140.grupo_produto     = reg_estq300.grupo_estrutura
                                                        and rcnb_140.subgru_produto    = 'XXX'
                                                        and rcnb_140.item_produto      = 'XXXXXX'
                                                        and rcnb_140.mes_periodo       = v_mes_movto_140
                                                        and rcnb_140.ano_periodo       = v_ano_movto_140;

                                                      exception
                                                      when no_data_found
                                                      then begin

                                                         select rcnb_140.tipo_aplicacao,    rcnb_140.valor_indice
                                                         into   v_tipo_aplic_140,           v_valor_indice_140
                                                         from rcnb_140
                                                         where rcnb_140.nr_nota_fiscal    = 0
                                                           and rcnb_140.seq_nota_fiscal   = 0
                                                           and rcnb_140.cnpj_fornecedor9  = 0
                                                           and rcnb_140.cnpj_fornecedor4  = 0
                                                           and rcnb_140.cnpj_fornecedor2  = 0
                                                           and rcnb_140.nivel_produto     = reg_estq300.nivel_estrutura
                                                           and rcnb_140.grupo_produto     = 'XXXXX'
                                                           and rcnb_140.subgru_produto    = 'XXX'
                                                           and rcnb_140.item_produto      = 'XXXXXX'
                                                           and rcnb_140.mes_periodo       = v_mes_movto_140
                                                           and rcnb_140.ano_periodo       = v_ano_movto_140;

                                                         exception
                                                         when no_data_found
                                                         then begin
                                                            v_tipo_aplic_140   := 0;
                                                            v_valor_indice_140 := 0.00;
                                                         end;
                                                      end;
                                                   end;
                                                end;
                                             end;
                                          end;
                                       end;
                                    end;
                                 end;
                              end;
                           end;
                        end;
                     end;
                  end if;

                  if v_valor_indice_140 is not null and v_valor_indice_140 > 0.00
                  then

                     if v_tipo_aplic_140 = 0
                     then
                        begin
                           v_novo_valor_movto_unit_est := round(v_novo_valor_movimento_unit + v_valor_indice_140,5);
                        exception
                           when others then
                              v_novo_valor_movto_unit_est := 1;
                        end;
                     else
                        begin
                           v_novo_valor_movto_unit_est := round(v_novo_valor_movimento_unit * v_valor_indice_140,5);
                        exception
                           when others then
                              v_novo_valor_movto_unit_est := 1;
                        end;
                     end if;
                  end if;

                  -- encontra o saldo fisico apos movimento
                  begin
                     v_novo_saldo_fisico       := round(v_saldo_fisico_anterior       + reg_estq300.quantidade,3);
                  exception
                     when others then
                        v_novo_saldo_fisico := 1;
                  end;
                  begin
                     v_novo_saldo_fisico_quilo := round(v_saldo_fisico_anterior_quilo + reg_estq300.quantidade_quilo,3);
                  exception
                     when others then
                        v_novo_saldo_fisico_quilo := 1;
                  end;

                  -- encontra o saldo financeiro apos movimento
                  if v_novo_saldo_fisico  = 0.000
                  then
                     v_novo_saldo_fisico_quilo    := 0.000;
                     v_novo_saldo_financeiro      := 0.000;
                     v_novo_saldo_financeiro_est  := 0.000;
                     v_novo_saldo_financeiro_proj := 0.000;
                  else
                     if reg_estq300.quantidade <> 0.000
                     then
                        begin
                           v_novo_saldo_financeiro      := v_saldo_financeiro_anterior + round(v_novo_valor_movimento_tot,2);
                        exception
                           when others then
                              v_novo_saldo_financeiro := 1;
                        end;
                        begin
                           v_novo_saldo_financeiro_est  := v_saldo_financeiro_ant_est  + round(v_novo_valor_movto_unit_est  * reg_estq300.quantidade,2);
                        exception
                           when others then
                              v_novo_saldo_financeiro_est := 1;
                        end;
                        begin
                           v_novo_saldo_financeiro_proj := v_saldo_financeiro_ant_proj + round(v_novo_valor_movto_unit_proj * reg_estq300.quantidade,2);
                        exception
                           when others then
                              v_novo_saldo_financeiro_proj := 1;
                        end;
                     else
                        begin
                           v_novo_saldo_financeiro      := v_saldo_financeiro_anterior + round(v_novo_valor_movimento_tot,2);
                        exception
                           when others then
                              v_novo_saldo_financeiro := 1;
                        end;
                        begin
                           v_novo_saldo_financeiro_est  := v_saldo_financeiro_ant_est  + round(v_novo_valor_movto_unit_est,2);
                        exception
                           when others then
                              v_novo_saldo_financeiro_est := 1;
                        end;
                        begin
                           v_novo_saldo_financeiro_proj := v_saldo_financeiro_ant_proj + round(v_novo_valor_movto_unit_proj,2);
                        exception
                           when others then
                              v_novo_saldo_financeiro_proj := 1;
                        end;
                     end if;
                  end if;

               else -- movimento de saida
                  -- se a transacao atualizar o preco medio, entao encontra o valor do
                  -- movimento unitario (preco_custo), sen?o, sera o valor do preco medio
                  -- anterior
                  if reg_estq300.calcula_preco = 1 -- atualiza preco medio
                  and round(reg_estq300.valor_movimento_unitario,5) > 0.00000
                  and reg_estq300.quantidade = 0.000
                  then
                     v_novo_valor_movimento_unit  := round(reg_estq300.valor_movimento_unitario,5);
                     v_novo_valor_movto_unit_est  := round(reg_estq300.valor_movto_unit_estimado,5);
                     v_novo_valor_movto_unit_proj := round(reg_estq300.valor_movimento_unitario,5);
                     v_novo_valor_movimento_tot   := round(reg_estq300.valor_movimento_unitario,5);
                  else

                     if v_par_origem_preco_custo = 1 and reg_estq300.tipo_transacao = 'C' and reg_estq300.tabela_origem = 'OBRF_015' -- CALCULA PELO PRECO MEDIO
                     then
                         v_novo_valor_movimento_unit  := round(reg_estq300.valor_movimento_unitario,5);
                         v_novo_valor_movto_unit_est  := round(reg_estq300.valor_movto_unit_estimado,5);
                         v_novo_valor_movto_unit_proj := round(reg_estq300.valor_movimento_unitario,5);
                     else
                         v_novo_valor_movimento_unit  := v_preco_medio_anterior;      -- nao atualiza preco medio
                         v_novo_valor_movto_unit_est  := v_preco_medio_anterior_est;  -- nao atualiza preco medio
                         v_novo_valor_movto_unit_proj := v_preco_medio_anterior_proj; -- nao atualiza preco medio
                     end if;

                     if reg_estq300.quantidade <> 0.000
                     then
                        begin
                           v_novo_valor_movimento_tot := round(v_novo_valor_movimento_unit,5) * reg_estq300.quantidade;
                        exception
                           when others then
                              v_novo_valor_movimento_tot := 1;
                        end;
                     else
                        v_novo_valor_movimento_tot := round(v_novo_valor_movimento_unit,5);
                     end if;
                  end if;

                  -- encontra o saldo fisico apos movimento
                  begin
                     v_novo_saldo_fisico       := round(v_saldo_fisico_anterior - reg_estq300.quantidade,3);
                  exception
                     when others then
                        v_novo_saldo_fisico := 1;
                  end;
                  begin
                     v_novo_saldo_fisico_quilo := round(v_saldo_fisico_anterior_quilo - reg_estq300.quantidade_quilo,3);
                  exception
                     when others then
                        v_novo_saldo_fisico_quilo := 1;
                  end;

                  -- encontra o saldo financeiro apos movimento
                  if v_novo_saldo_fisico  = 0.000
                  then
                     v_novo_saldo_fisico_quilo    := 0.000;
                     v_novo_saldo_financeiro      := 0.000;
                     v_novo_saldo_financeiro_est  := 0.000;
                     v_novo_saldo_financeiro_proj := 0.000;
                     v_novo_valor_movimento_tot   := v_saldo_financeiro_anterior;
                  else

                     if reg_estq300.quantidade <> 0.000
                     then
                        begin
                           v_novo_saldo_financeiro      := v_saldo_financeiro_anterior - v_novo_valor_movimento_tot;
                        exception
                           when others then
                              v_novo_saldo_financeiro := 1;
                        end;
                        begin
                           v_novo_saldo_financeiro_est  := v_saldo_financeiro_ant_est  - round(v_novo_valor_movto_unit_est  * reg_estq300.quantidade,2);
                        exception
                           when others then
                              v_novo_saldo_financeiro_est := 1;
                        end;
                        begin
                           v_novo_saldo_financeiro_proj := v_saldo_financeiro_ant_proj - round(v_novo_valor_movto_unit_proj * reg_estq300.quantidade,2);
                        exception
                           when others then
                              v_novo_saldo_financeiro_proj := 1;
                        end;
                     else
                        begin
                           v_novo_saldo_financeiro      := v_saldo_financeiro_anterior - v_novo_valor_movimento_tot;
                        exception
                           when others then
                              v_novo_saldo_financeiro := 1;
                        end;
                        begin
                           v_novo_saldo_financeiro_est  := v_saldo_financeiro_ant_est  - round(v_novo_valor_movto_unit_est,2);
                        exception
                           when others then
                              v_novo_saldo_financeiro_est := 1;
                        end;
                        begin
                           v_novo_saldo_financeiro_proj := v_saldo_financeiro_ant_proj - round(v_novo_valor_movto_unit_proj,2);
                        exception
                           when others then
                              v_novo_saldo_financeiro_proj := 1;
                        end;
                     end if;
                  end if;
               end if;

               -- encontra o novo preco medio
               if v_novo_saldo_fisico <> 0.00 and reg_estq300.calcula_preco = 1
               then
                  begin
                     v_novo_preco_medio      := round(v_novo_saldo_financeiro      / v_novo_saldo_fisico,5);
                  exception
                     when others then
                        v_novo_preco_medio := 1;
                  end;
                  begin
                     v_novo_preco_medio_est  := round(v_novo_saldo_financeiro_est  / v_novo_saldo_fisico,5);
                  exception
                     when others then
                        v_novo_preco_medio_est := 1;
                  end;
                  begin
                     v_novo_preco_medio_proj := round(v_novo_saldo_financeiro_proj / v_novo_saldo_fisico,5);
                  exception
                     when others then
                        v_novo_preco_medio_proj := 1;
                  end;
               else
                  v_novo_preco_medio      := v_preco_medio_anterior;
                  v_novo_preco_medio_est  := v_preco_medio_anterior_est;
                  v_novo_preco_medio_proj := v_preco_medio_anterior_proj;
               end if;

               -- atualiza valor movimento unitario (preco custo,saldo fisico, saldo financeiro
               -- e preco medio do movimento
               if reg_estq300.tabela_origem not in ('FATU_060','OBRF_015')
               then
                  v_valor_contabil      := v_novo_valor_movimento_unit;
                  v_valor_contabil_proj := v_novo_valor_movto_unit_proj;
               else
                  v_valor_contabil      := reg_estq300.valor_contabil_unitario;
                  v_valor_contabil_proj := reg_estq300.valor_contabil_unitario_proj;
               end if;

               -- para clientes que nao controlam kardex, onde as variaveis podem estourar valores
               if v_novo_valor_movimento_unit < -9999999999999 or v_novo_valor_movimento_unit > 9999999999999
               then v_novo_valor_movimento_unit  := 1;
               end if;

               if v_novo_valor_movto_unit_est < -9999999999999 or v_novo_valor_movto_unit_est > 9999999999999
               then v_novo_valor_movto_unit_est  := 1;
               end if;

               if v_novo_valor_movto_unit_proj < -9999999999999 or v_novo_valor_movto_unit_proj > 9999999999999
               then v_novo_valor_movto_unit_proj  := 1;
               end if;

               if v_novo_saldo_financeiro < -9999999999999 or v_novo_saldo_financeiro > 9999999999999
               then v_novo_saldo_financeiro      := 1;
               end if;

               if v_novo_saldo_financeiro_est < -9999999999999 or v_novo_saldo_financeiro_est > 9999999999999
               then v_novo_saldo_financeiro_est      := 1;
               end if;

               if v_novo_saldo_financeiro_proj < -9999999999999 or v_novo_saldo_financeiro_proj > 9999999999999
               then v_novo_saldo_financeiro_proj      := 1;
               end if;

               if v_novo_preco_medio < -9999999999999 or v_novo_preco_medio > 9999999999999
               then v_novo_preco_medio      := 1;
               end if;

               if v_novo_preco_medio_est < -9999999999999 or v_novo_preco_medio_est > 9999999999999
               then v_novo_preco_medio_est      := 1;
               end if;

               if v_novo_preco_medio_proj < -9999999999999 or v_novo_preco_medio_proj > 9999999999999
               then v_novo_preco_medio_proj      := 1;
               end if;

               if v_valor_contabil < -9999999999999 or v_valor_contabil > 9999999999999
               then v_valor_contabil      := 1;
               end if;

               if v_valor_contabil_proj < -9999999999999 or v_valor_contabil_proj > 9999999999999
               then v_valor_contabil_proj      := 1;
               end if;

               if v_novo_saldo_fisico < -9999999999999 or v_novo_saldo_fisico > 9999999999999
               then v_novo_saldo_fisico := 1;
               end if;

               if v_novo_saldo_fisico_quilo < -9999999999999 or v_novo_saldo_fisico_quilo > 9999999999999
               then v_novo_saldo_fisico_quilo := 1;
               end if;

               if v_novo_valor_movimento_tot < -9999999999999 or v_novo_valor_movimento_tot > 9999999999999
               then v_novo_valor_movimento_tot := 1;
               end if;

               -- se o tipo da valorizacao for PRESUMIDA e o material for fabricado,
               -- zera os valores dos acumulados, pois n?o e gravado estes valores
               if v_apuracao_ir = 2 and (reg_estq300.nivel_estrutura <> '9' and v_comprado_fabric = 2)
               then

                  v_novo_valor_movimento_unit   := 0.00;
                  v_novo_valor_movto_unit_est   := 0.00;
                  v_novo_valor_movto_unit_proj  := 0.00;
                  v_novo_saldo_financeiro       := 0.00;
                  v_novo_saldo_financeiro_est   := 0.00;
                  v_novo_saldo_financeiro_proj  := 0.00;
                  v_novo_preco_medio            := 0.00;
                  v_novo_preco_medio_est        := 0.00;
                  v_novo_preco_medio_proj       := 0.00;
                  v_valor_contabil              := 0.00;
                  v_valor_contabil_proj         := 0.00;
               end if;

               if lower(p_estq_300_estq_310) = 'estq_300'
               then
                   update estq_300
                   set sequencia_ficha               = v_seq_ficha,
                       valor_movimento_unitario      = v_novo_valor_movimento_unit,
                       valor_movto_unit_estimado     = v_novo_valor_movto_unit_est,
                       valor_movimento_unitario_proj = v_novo_valor_movto_unit_proj,
                       saldo_financeiro              = v_novo_saldo_financeiro,
                       saldo_financeiro_estimado     = v_novo_saldo_financeiro_est,
                       saldo_financeiro_proj         = v_novo_saldo_financeiro_proj,
                       preco_medio_unitario          = v_novo_preco_medio,
                       preco_medio_unit_estimado     = v_novo_preco_medio_est,
                       preco_medio_unitario_proj     = v_novo_preco_medio_proj,
                       valor_contabil_unitario       = v_valor_contabil,
                       valor_contabil_unitario_proj  = v_valor_contabil_proj,
                       saldo_fisico                  = v_novo_saldo_fisico,
                       valor_total                   = v_novo_valor_movimento_tot,
                       saldo_fisico_quilo            = v_novo_saldo_fisico_quilo
                   where codigo_deposito    = reg_estq300.codigo_deposito
                   and   nivel_estrutura    = reg_estq300.nivel_estrutura
                   and   grupo_estrutura    = reg_estq300.grupo_estrutura
                   and   subgrupo_estrutura = reg_estq300.subgrupo_estrutura
                   and   item_estrutura     = reg_estq300.item_estrutura
                   and   data_movimento     = reg_estq300.data_movimento
                   and   sequencia_ficha    = reg_estq300.sequencia_ficha
                   and   sequencia_insercao = reg_estq300.sequencia_insercao;
               else
                   update estq_310
                   set sequencia_ficha               = v_seq_ficha,
                       valor_movimento_unitario      = v_novo_valor_movimento_unit,
                       valor_movto_unit_estimado     = v_novo_valor_movto_unit_est,
                       valor_movimento_unitario_proj = v_novo_valor_movto_unit_proj,
                       saldo_financeiro              = v_novo_saldo_financeiro,
                       saldo_financeiro_estimado     = v_novo_saldo_financeiro_est,
                       saldo_financeiro_proj         = v_novo_saldo_financeiro_proj,
                       preco_medio_unitario          = v_novo_preco_medio,
                       preco_medio_unit_estimado     = v_novo_preco_medio_est,
                       preco_medio_unitario_proj     = v_novo_preco_medio_proj,
                       valor_contabil_unitario       = v_valor_contabil,
                       valor_contabil_unitario_proj  = v_valor_contabil_proj,
                       saldo_fisico                  = v_novo_saldo_fisico,
                       valor_total                   = v_novo_valor_movimento_tot,
                       saldo_fisico_quilo            = v_novo_saldo_fisico_quilo
                   where codigo_deposito    = reg_estq300.codigo_deposito
                   and   nivel_estrutura    = reg_estq300.nivel_estrutura
                   and   grupo_estrutura    = reg_estq300.grupo_estrutura
                   and   subgrupo_estrutura = reg_estq300.subgrupo_estrutura
                   and   item_estrutura     = reg_estq300.item_estrutura
                   and   data_movimento     = reg_estq300.data_movimento
                   and   sequencia_ficha    = reg_estq300.sequencia_ficha
                   and   sequencia_insercao = reg_estq300.sequencia_insercao;
               end if;

               commit;

               v_saldo_fisico_anterior        := v_novo_saldo_fisico;
               v_saldo_fisico_anterior_quilo  := v_novo_saldo_fisico_quilo;

               v_saldo_financeiro_anterior    := v_novo_saldo_financeiro;
               v_saldo_financeiro_ant_est     := v_novo_saldo_financeiro_est;
               v_saldo_financeiro_ant_proj    := v_novo_saldo_financeiro_proj;

               v_preco_medio_anterior         := v_novo_preco_medio;
               v_preco_medio_anterior_est     := v_novo_preco_medio_est;
               v_preco_medio_anterior_proj    := v_novo_preco_medio_proj;

               -- salva valores em memoria para gravar na estq_301
               v_ultimo_saldo_fisico          := v_novo_saldo_fisico;
               v_ultimo_saldo_fisico_quilo    := v_novo_saldo_fisico_quilo;

               v_ultimo_saldo_financeiro      := v_novo_saldo_financeiro;
               v_ultimo_preco_medio           := v_novo_preco_medio;
               v_ultimo_saldo_financeiro_est  := v_novo_saldo_financeiro_est;
               v_ultimo_preco_medio_est       := v_novo_preco_medio_est;
               v_ultimo_saldo_financeiro_proj := v_novo_saldo_financeiro_proj;
               v_ultimo_preco_medio_proj      := v_novo_preco_medio_proj;

               -- so salva o preco de custo, se o movimento for de entrada e este atualizar o preco medio
               if reg_estq300.tipo_e_s = 'E' and reg_estq300.calcula_preco = 1 and reg_estq300.quantidade > 0
               then
                  v_ultimo_preco_custo      := v_novo_valor_movimento_unit;
                  v_ultimo_preco_custo_est  := v_novo_valor_movto_unit_est;
                  v_ultimo_preco_custo_proj := v_novo_valor_movto_unit_proj;
               end if;

            end if;
         end loop;

         -- rotina para gravar registros na tabela estq_301, tabela esta que guarda
         -- os valores de saldos, e valores de cada mes, por deposito/produto
         -- grava os dados do ultimo produto processado, pois este nao e' gravado na rotina acima
         if v_nivel_estrutura_ant <> '#'
         then

            v_mes_ano_301  := to_date('01/' || to_char(v_mes_ant_301, '00') || '/' || to_char(v_ano_ant_301, '0000'), 'dd/mm/yyyy');

            -- para clientes que nao controlam kardex, onde as variaveis podem estourar valores
            if v_ultimo_saldo_financeiro < -9999999999999 or v_ultimo_saldo_financeiro > 9999999999999
            then v_ultimo_saldo_financeiro      := 1;
            end if;

            if v_ultimo_saldo_financeiro_est < -9999999999999 or v_ultimo_saldo_financeiro_est > 9999999999999
            then v_ultimo_saldo_financeiro_est  := 1;
            end if;

            if v_ultimo_saldo_financeiro_proj < -9999999999999 or v_ultimo_saldo_financeiro_proj > 9999999999999
            then v_ultimo_saldo_financeiro_proj := 1;
            end if;

            if v_ultimo_preco_medio < -9999999999999 or v_ultimo_preco_medio > 9999999999999
            then v_ultimo_preco_medio      := 1;
            end if;

            if v_ultimo_preco_medio_est < -9999999999999 or v_ultimo_preco_medio_est > 9999999999999
            then v_ultimo_preco_medio_est  := 1;
            end if;

            if v_ultimo_preco_medio_proj < -9999999999999 or v_ultimo_preco_medio_proj > 9999999999999
            then v_ultimo_preco_medio_proj := 1;
            end if;

            if v_ultimo_preco_custo < -9999999999999 or v_ultimo_preco_custo > 9999999999999
            then v_ultimo_preco_custo      := 1;
            end if;

            if v_ultimo_preco_custo_est < -9999999999999 or v_ultimo_preco_custo_est > 9999999999999
            then v_ultimo_preco_custo_est  := 1;
            end if;

            if v_ultimo_preco_custo_proj < -9999999999999 or v_ultimo_preco_custo_proj > 9999999999999
            then v_ultimo_preco_custo_proj := 1;
            end if;

            if v_ultimo_saldo_fisico < -9999999999999 or v_ultimo_saldo_fisico > 9999999999999
            then v_ultimo_saldo_fisico := 1;
            end if;

            if v_ultimo_saldo_fisico_quilo < -9999999999999 or v_ultimo_saldo_fisico_quilo > 9999999999999
            then v_ultimo_saldo_fisico_quilo := 1;
            end if;

            -- se o tipo da valorizacao for PRESUMIDA e o material for fabricado,
            -- calcula o valor atraves do percentual em cima do maior valor da venda
            if v_apuracao_ir = 2 and (v_nivel_estrutura_ant <> '9' and v_comprado_fabric = 2)
            then
               begin
                  v_ultimo_saldo_financeiro      := round(v_ultimo_saldo_fisico * v_prc_presum, 2);
               exception
                  when others then
                     v_ultimo_saldo_financeiro := 1;
               end;
               v_ultimo_preco_medio           := v_prc_presum;
               v_ultimo_preco_custo           := v_prc_presum;
               begin
                  v_ultimo_saldo_financeiro_est  := round(v_ultimo_saldo_fisico * v_prc_presum, 2);
               exception
                  when others then
                     v_ultimo_saldo_financeiro_est := 1;
               end;
               v_ultimo_preco_medio_est       := v_prc_presum;
               v_ultimo_preco_custo_est       := v_prc_presum;
               begin
                  v_ultimo_saldo_financeiro_proj := round(v_ultimo_saldo_fisico * v_prc_presum, 2);
               exception
                  when others then
                     v_ultimo_saldo_financeiro_proj := 1;
               end;
               v_ultimo_preco_medio_proj      := v_prc_presum;
               v_ultimo_preco_custo_proj      := v_prc_presum;
            end if;

            -- sistema tenta inserir valores geradados em memoria, se nao conseguir ele
            -- tentara gravar por update
            begin

               insert into estq_301 (
                  codigo_deposito,                nivel_estrutura,           grupo_estrutura,
                  subgrupo_estrutura,             item_estrutura,            mes_movimento,
                  ano_movimento,                  mes_ano_movimento,         saldo_fisico,
                  saldo_financeiro,               preco_medio_unitario,      preco_custo_unitario,
                  saldo_financeiro_estimado,      preco_medio_unit_estimado, preco_custo_unit_estimado,
                  saldo_financeiro_proj,          preco_medio_unit_proj,     preco_custo_unit_proj,
                  saldo_fisico_quilo,             codigo_empresa)
               values (
                  0,    v_nivel_estrutura_ant,     v_grupo_estrutura_ant,
                  v_subgrupo_estrutura_ant,       v_item_estrutura_ant,      v_mes_ant_301,
                  v_ano_ant_301,                  v_mes_ano_301,             v_ultimo_saldo_fisico,
                  v_ultimo_saldo_financeiro,      v_ultimo_preco_medio,      v_ultimo_preco_custo,
                  v_ultimo_saldo_financeiro_est,  v_ultimo_preco_medio_est,  v_ultimo_preco_custo_est,
                  v_ultimo_saldo_financeiro_proj, v_ultimo_preco_medio_proj, v_ultimo_preco_custo_proj,
                  v_ultimo_saldo_fisico_quilo,    reg_fatu500.codigo_empresa);

            exception
               when others then
                  update estq_301
                  set  mes_ano_movimento         = v_mes_ano_301,
                       saldo_fisico              = v_ultimo_saldo_fisico,
                       saldo_financeiro          = v_ultimo_saldo_financeiro,
                       preco_medio_unitario      = v_ultimo_preco_medio,
                       preco_custo_unitario      = v_ultimo_preco_custo,
                       saldo_financeiro_estimado = v_ultimo_saldo_financeiro_est,
                       preco_medio_unit_estimado = v_ultimo_preco_medio_est,
                       preco_custo_unit_estimado = v_ultimo_preco_custo_est,
                       saldo_financeiro_proj     = v_ultimo_saldo_financeiro_proj,
                       preco_medio_unit_proj     = v_ultimo_preco_medio_proj,
                       preco_custo_unit_proj     = v_ultimo_preco_custo_proj,
                       saldo_fisico_quilo        = v_ultimo_saldo_fisico_quilo
                  where codigo_deposito    = 0
                  and   nivel_estrutura    = v_nivel_estrutura_ant
                  and   grupo_estrutura    = v_grupo_estrutura_ant
                  and   subgrupo_estrutura = v_subgrupo_estrutura_ant
                  and   item_estrutura     = v_item_estrutura_ant
                  and   mes_movimento      = v_mes_ant_301
                  and   ano_movimento      = v_ano_ant_301
                  and   codigo_empresa     = reg_fatu500.codigo_empresa;
            end;
            commit;
         end if;
      end if;

      --BALENA
      for reg_estq300_calculada in estq300_calculada(reg_fatu500.codigo_empresa, v_data_inicial)
      loop
          inter_pr_procura_dados_cardex(reg_estq300_calculada.codigo_deposito, reg_estq300_calculada.nivel_estrutura,
                                        reg_estq300_calculada.grupo_estrutura, reg_estq300_calculada.subgrupo_estrutura,
                                        reg_estq300_calculada.item_estrutura,  v_data_inicial,
                                        v_saldo_fisico_anterior_dep,           v_saldo_financeiro_ant_dep,
                                        v_preco_medio_anterior_dep,            v_saldo_finan_ant_proj_dep,
                                        v_preco_medio_ant_proj_dep,       v_saldo_financeiro_ant_est_dep,
                                        v_preco_medio_anterior_est_dep,        v_saldo_fisico_ant_quilo_dep);
      
          begin
             insert into estq_301 (
                codigo_deposito,                           nivel_estrutura,           grupo_estrutura,
                subgrupo_estrutura,                        item_estrutura,            mes_movimento,
                ano_movimento,                             mes_ano_movimento,         saldo_fisico,
                saldo_financeiro,                          preco_medio_unitario,      preco_custo_unitario,
                saldo_financeiro_estimado,                 preco_medio_unit_estimado, preco_custo_unit_estimado,
                saldo_financeiro_proj,                     preco_medio_unit_proj,     preco_custo_unit_proj,
                saldo_fisico_quilo,                        codigo_empresa)
             select reg_estq300_calculada.codigo_deposito, nivel_estrutura,           grupo_estrutura,
                subgrupo_estrutura,                        item_estrutura,            mes_movimento,
                ano_movimento,                             mes_ano_movimento,         (reg_estq300_calculada.saldo_fisico + v_saldo_fisico_anterior_dep),
                saldo_financeiro,                          preco_medio_unitario,      preco_custo_unitario,
                saldo_financeiro_estimado,                 preco_medio_unit_estimado, preco_custo_unit_estimado,
                saldo_financeiro_proj,                     preco_medio_unit_proj,     preco_custo_unit_proj,
                saldo_fisico_quilo,                        codigo_empresa
             from estq_301
             where estq_301.codigo_deposito    = 0
               and estq_301.codigo_empresa     = reg_fatu500.codigo_empresa
               and estq_301.mes_ano_movimento  = v_data_inicial
               and estq_301.nivel_estrutura    = reg_estq300_calculada.nivel_estrutura
               and estq_301.grupo_estrutura    = reg_estq300_calculada.grupo_estrutura
               and estq_301.subgrupo_estrutura = reg_estq300_calculada.subgrupo_estrutura
               and estq_301.item_estrutura     = reg_estq300_calculada.item_estrutura;
          exception
             when others then
                update estq_301
                set  saldo_fisico        = (reg_estq300_calculada.saldo_fisico + v_saldo_fisico_anterior_dep)
                where codigo_deposito    = reg_estq300_calculada.codigo_deposito
                and   nivel_estrutura    = reg_estq300_calculada.nivel_estrutura
                and   grupo_estrutura    = reg_estq300_calculada.grupo_estrutura
                and   subgrupo_estrutura = reg_estq300_calculada.subgrupo_estrutura
                and   item_estrutura     = reg_estq300_calculada.item_estrutura
                and   mes_ano_movimento  = v_data_inicial;
          end;
      end loop;

      commit;
   end loop;

   begin
       select empr_002.calcula_consignado
       into v_calcula_consignado
       from empr_002;
   end;

   if v_calcula_consignado = 1
   then
      inter_pr_calc_cardex_consig(    p_inc_exc ,
                    p_empresa1,
                    p_empresa2,
                    p_empresa3,
                    p_empresa4,
                    p_empresa5,
                    p_nivel_prod,
                    p_grupo_prod,
                    p_nivel_prod1,
                    p_grupo_prod1,
                    p_nivel_prod2,
                    p_grupo_prod2,
                    p_nivel_prod3,
                    p_grupo_prod3,
                    v_data_inicial,
                    Last_day(v_data_inicial),
                    p_estq_300_estq_310);
   end if;


   exception
      when erro_periodo_estoque then
         raise_application_error(-20000,'N?o encontrou periodo de estoque');

end inter_pr_calcula_ficha_cardex4;

/

exec inter_pr_recompile;

/
