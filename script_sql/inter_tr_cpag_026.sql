create or replace trigger "INTER_TR_CPAG_026" 
   before insert or
          update of banco, conta_corrente, data_movimento, sequencia_lcto,
    codigo_evento, nr_documento, debito_credito, historico,
    valor_cheque, valor_compensado, data_pre_datado, data_compensacao,
    origem_lcto, situacao_lancto, tipo_movimento, codigo_historico,
    codigo_transacao, codigo_contabil, num_contabil, centro_custo,
    controle_leitura, conciliado, marca_concilia, cod_conciliacao,
    data_conciliacao, maquina_usuario, usuario, data_ocorr,
    executa_trigger, flag_conciliacao
          on cpag_026
   for each row

declare
   -- local variables here
   v_data_ocorr            date;

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_concilia               number;

   v_executa_trigger      number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
      
      ws_concilia := 0 ;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
      
      ws_concilia := 1;
   end if;

   if v_executa_trigger = 0
   then
       -- grava a data/hora da insercao do registro (log)
       v_data_ocorr := sysdate();

        -- Dados do usuario logado
     inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                             ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

       --alimenta as variaveis new caso seja insert ou update
      
       
       if ws_concilia = 1
       then 
          if :new.conciliado <> :old.conciliado or  
             :new.marca_concilia <> :old.marca_concilia or
             :new.cod_conciliacao <> :old.cod_conciliacao or
             :new.data_conciliacao <> :old.data_conciliacao or 
             :new.flag_conciliacao  <> :old.flag_conciliacao      
          then ws_concilia := 1;
          else ws_concilia := 0;
          end if;
       
       end if;
       
       
       if UPPER(ws_usuario_rede) <> 'ROOT' and ws_concilia = 0 then
	   	   :new.usuario := ws_usuario_rede;
	   	   :new.maquina_usuario := ws_maquina_rede;
         :new.data_ocorr      := v_data_ocorr;
	   end if;
    end if;
end inter_tr_cpag_026;

-- ALTER TRIGGER "INTER_TR_CPAG_026" ENABLE

-- ALTER TRIGGER "INTER_TR_CPAG_026" ENABLE
 

/

exec inter_pr_recompile;

