
  CREATE OR REPLACE TRIGGER "INTER_TR_CONT_010_LOG" 
after insert or delete or update
on cont_010
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid); 

 if inserting
 then
    begin

        insert into cont_010_log (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           codigo_historico_old,   /*8*/
           codigo_historico_new,   /*9*/
           historico_contab_old,   /*10*/
           historico_contab_new,   /*11*/
           sinal_titulo_old,   /*12*/
           sinal_titulo_new,   /*13*/
           sinal_diario_old,   /*14*/
           sinal_diario_new,   /*15*/
           fluxo_old,   /*16*/
           fluxo_new,   /*17*/
           entrada_saida_old,   /*18*/
           entrada_saida_new,   /*19*/
           sinal_comissao_old,   /*20*/
           sinal_comissao_new,   /*21*/
           tipo_historico_old,   /*22*/
           tipo_historico_new,   /*23*/
           diario_old,   /*24*/
           diario_new,   /*25*/
           atualiza_comis_old,   /*26*/
           atualiza_comis_new,   /*27*/
           cod_hist_cont_old,   /*28*/
           cod_hist_cont_new,   /*29*/
           conta_corrente_old,   /*30*/
           conta_corrente_new,   /*31*/
           atualiza_acc_old,   /*32*/
           atualiza_acc_new,   /*33*/
           mot_exclusao_cli_old,   /*34*/
           mot_exclusao_cli_new    /*35*/
        ) values (
            'i', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.codigo_historico, /*9*/
           '',/*10*/
           :new.historico_contab, /*11*/
           0,/*12*/
           :new.sinal_titulo, /*13*/
           0,/*14*/
           :new.sinal_diario, /*15*/
           0,/*16*/
           :new.fluxo, /*17*/
           '',/*18*/
           :new.entrada_saida, /*19*/
           0,/*20*/
           :new.sinal_comissao, /*21*/
           '',/*22*/
           :new.tipo_historico, /*23*/
           0,/*24*/
           :new.diario, /*25*/
           0,/*26*/
           :new.atualiza_comis, /*27*/
           0,/*28*/
           :new.cod_hist_cont, /*29*/
           0,/*30*/
           :new.conta_corrente, /*31*/
           0,/*32*/
           :new.atualiza_acc, /*33*/
           0,/*34*/
           :new.mot_exclusao_cli /*35*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into cont_010_log (
           tipo_ocorr, /*0*/
           data_ocorr, /*1*/
           hora_ocorr, /*2*/
           usuario_rede, /*3*/
           maquina_rede, /*4*/
           aplicacao, /*5*/
           usuario_sistema, /*6*/
           nome_programa, /*7*/
           codigo_historico_old, /*8*/
           codigo_historico_new, /*9*/
           historico_contab_old, /*10*/
           historico_contab_new, /*11*/
           sinal_titulo_old, /*12*/
           sinal_titulo_new, /*13*/
           sinal_diario_old, /*14*/
           sinal_diario_new, /*15*/
           fluxo_old, /*16*/
           fluxo_new, /*17*/
           entrada_saida_old, /*18*/
           entrada_saida_new, /*19*/
           sinal_comissao_old, /*20*/
           sinal_comissao_new, /*21*/
           tipo_historico_old, /*22*/
           tipo_historico_new, /*23*/
           diario_old, /*24*/
           diario_new, /*25*/
           atualiza_comis_old, /*26*/
           atualiza_comis_new, /*27*/
           cod_hist_cont_old, /*28*/
           cod_hist_cont_new, /*29*/
           conta_corrente_old, /*30*/
           conta_corrente_new, /*31*/
           atualiza_acc_old, /*32*/
           atualiza_acc_new, /*33*/
           mot_exclusao_cli_old, /*34*/
           mot_exclusao_cli_new  /*35*/
        ) values (
            'a', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.codigo_historico,  /*8*/
           :new.codigo_historico, /*9*/
           :old.historico_contab,  /*10*/
           :new.historico_contab, /*11*/
           :old.sinal_titulo,  /*12*/
           :new.sinal_titulo, /*13*/
           :old.sinal_diario,  /*14*/
           :new.sinal_diario, /*15*/
           :old.fluxo,  /*16*/
           :new.fluxo, /*17*/
           :old.entrada_saida,  /*18*/
           :new.entrada_saida, /*19*/
           :old.sinal_comissao,  /*20*/
           :new.sinal_comissao, /*21*/
           :old.tipo_historico,  /*22*/
           :new.tipo_historico, /*23*/
           :old.diario,  /*24*/
           :new.diario, /*25*/
           :old.atualiza_comis,  /*26*/
           :new.atualiza_comis, /*27*/
           :old.cod_hist_cont,  /*28*/
           :new.cod_hist_cont, /*29*/
           :old.conta_corrente,  /*30*/
           :new.conta_corrente, /*31*/
           :old.atualiza_acc,  /*32*/
           :new.atualiza_acc, /*33*/
           :old.mot_exclusao_cli,  /*34*/
           :new.mot_exclusao_cli  /*35*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into cont_010_log (
           tipo_ocorr, /*0*/
           data_ocorr, /*1*/
           hora_ocorr, /*2*/
           usuario_rede, /*3*/
           maquina_rede, /*4*/
           aplicacao, /*5*/
           usuario_sistema, /*6*/
           nome_programa, /*7*/
           codigo_historico_old, /*8*/
           codigo_historico_new, /*9*/
           historico_contab_old, /*10*/
           historico_contab_new, /*11*/
           sinal_titulo_old, /*12*/
           sinal_titulo_new, /*13*/
           sinal_diario_old, /*14*/
           sinal_diario_new, /*15*/
           fluxo_old, /*16*/
           fluxo_new, /*17*/
           entrada_saida_old, /*18*/
           entrada_saida_new, /*19*/
           sinal_comissao_old, /*20*/
           sinal_comissao_new, /*21*/
           tipo_historico_old, /*22*/
           tipo_historico_new, /*23*/
           diario_old, /*24*/
           diario_new, /*25*/
           atualiza_comis_old, /*26*/
           atualiza_comis_new, /*27*/
           cod_hist_cont_old, /*28*/
           cod_hist_cont_new, /*29*/
           conta_corrente_old, /*30*/
           conta_corrente_new, /*31*/
           atualiza_acc_old, /*32*/
           atualiza_acc_new, /*33*/
           mot_exclusao_cli_old, /*34*/
           mot_exclusao_cli_new /*35*/
        ) values (
            'd', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.codigo_historico, /*8*/
           0, /*9*/
           :old.historico_contab, /*10*/
           '', /*11*/
           :old.sinal_titulo, /*12*/
           0, /*13*/
           :old.sinal_diario, /*14*/
           0, /*15*/
           :old.fluxo, /*16*/
           0, /*17*/
           :old.entrada_saida, /*18*/
           '', /*19*/
           :old.sinal_comissao, /*20*/
           0, /*21*/
           :old.tipo_historico, /*22*/
           '', /*23*/
           :old.diario, /*24*/
           0, /*25*/
           :old.atualiza_comis, /*26*/
           0, /*27*/
           :old.cod_hist_cont, /*28*/
           0, /*29*/
           :old.conta_corrente, /*30*/
           0, /*31*/
           :old.atualiza_acc, /*32*/
           0, /*33*/
           :old.mot_exclusao_cli, /*34*/
           0 /*35*/
         );
    end;
 end if;
end inter_tr_cont_010_log;
-- ALTER TRIGGER "INTER_TR_CONT_010_LOG" ENABLE
 

/

exec inter_pr_recompile;

