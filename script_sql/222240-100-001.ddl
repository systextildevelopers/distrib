create table pcpc_605 (
    codigo_familia          number(9)       not null,
    codigo_aparelho         varchar2(4)     not null
);

ALTER TABLE PCPC_605 ADD CONSTRAINT PCPC_605_PK PRIMARY KEY (CODIGO_FAMILIA, CODIGO_APARELHO);

exec inter_pr_recompile;
