alter table basi_100 add (tonalidade_estampa VARCHAR2(2) default '');
                      
alter table basi_100 add (tipo_barrado VARCHAR2(2) default '');

alter table basi_122 add (tipo_estampa VARCHAR2(2) default '');

alter table mqop_060 add (codigo_perfil NUMBER(5) default 0 not null);

alter table ftec_208 add (passadas NUMBER(3),
                          processo VARCHAR2(2) default '');

alter table basi_010 add (obs_ficha_tec_estamp VARCHAR2(100));

alter table basi_050 add (pressao NUMBER(5,2),
                          vareta NUMBER(5,2),
                          tipo_vareta VARCHAR2(2) default '',
                          seq_execucao NUMBER(3));

alter table fatu_504 add (estagio_estampa_digital NUMBER(2) default 0,
                          estagio_estampa_transfer NUMBER(2) default 0,
                          estagio_estampa_rotativa NUMBER(2) default 0,
                          estagio_preparacao_pasta NUMBER(2) default 0);                                                                                                                                                            
                          
