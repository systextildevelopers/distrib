
  CREATE OR REPLACE PROCEDURE "INTER_PR_ESTRUTURA_PLAN" (p_ordem_planejamento in number,    p_pedido_venda        in number,
                                                     p_pedido_reserva     in number,    p_nivel_produto       in varchar2,
                                                     p_grupo_produto      in varchar2,  p_subgrupo_produto    in varchar2,
                                                     p_item_produto       in varchar2,  p_alternativa_produto in number,
                                                     p_quantidade         in number)
is
   cursor estrutura625 is select * from tmrp_625
                          where tmrp_625.ordem_planejamento         = p_ordem_planejamento
                            and tmrp_625.pedido_venda               = p_pedido_venda
                            and tmrp_625.pedido_reserva             = p_pedido_reserva
                            and tmrp_625.nivel_produto_origem       = p_nivel_produto
                            and tmrp_625.grupo_produto_origem       = p_grupo_produto
                            and tmrp_625.subgrupo_produto_origem    = p_subgrupo_produto
                            and tmrp_625.item_produto_origem        = p_item_produto
                            and tmrp_625.alternativa_produto_origem = p_alternativa_produto;

   cursor estrutura050 is select * from basi_050
                          where  basi_050.nivel_item       = p_nivel_produto
                            and  basi_050.grupo_item       = p_grupo_produto
                            and (basi_050.sub_item         = p_subgrupo_produto or basi_050.sub_item  = '000')
                            and (basi_050.item_item        = p_item_produto     or basi_050.item_item = '000000')
                            and  basi_050.alternativa_item = p_alternativa_produto;

   v_existe_estrutura   number(9) := 0;
   v_nivel_comp         basi_050.nivel_item%type := '#';
   v_grupo_comp         basi_050.grupo_item%type := '#####';
   v_sub_comp           basi_050.sub_comp%type := '###';
   v_item_comp          basi_050.item_comp%type := '######';
   v_sub_item           basi_050.sub_item%type := '###';
   v_item_tem           basi_050.item_item%type := '######';
   v_seq_comp           basi_050.sequencia%type;
   v_consumo            basi_050.consumo%type;
   v_alternativa40      basi_040.alternativa_comp%type;
   v_quantidade_plan    tmrp_625.qtde_reserva_planejada%type;

begin
   v_quantidade_plan := 0.000;

   select count(1)
   into v_existe_estrutura
   from tmrp_625
   where tmrp_625.ordem_planejamento         = p_ordem_planejamento
     and tmrp_625.pedido_venda               = p_pedido_venda
     and tmrp_625.pedido_reserva             = p_pedido_reserva
     and tmrp_625.nivel_produto_origem       = p_nivel_produto
     and tmrp_625.grupo_produto_origem       = p_grupo_produto
     and tmrp_625.subgrupo_produto_origem    = p_subgrupo_produto
     and tmrp_625.item_produto_origem        = p_item_produto
     and tmrp_625.alternativa_produto_origem = p_alternativa_produto;

   if v_existe_estrutura > 0
   then begin
      for tmrp625 in estrutura625
      loop
         v_quantidade_plan := p_quantidade * tmrp625.consumo;

         begin
            update tmrp_625
               set tmrp_625.qtde_reserva_planejada = tmrp_625.qtde_reserva_planejada + v_quantidade_plan
            where tmrp_625.ordem_planejamento         = tmrp625.ordem_planejamento
              and tmrp_625.pedido_venda               = tmrp625.pedido_venda
              and tmrp_625.pedido_reserva             = tmrp625.pedido_reserva
              and tmrp_625.nivel_produto_origem       = tmrp625.nivel_produto_origem
              and tmrp_625.grupo_produto_origem       = tmrp625.grupo_produto_origem
              and tmrp_625.subgrupo_produto_origem    = tmrp625.subgrupo_produto_origem
              and tmrp_625.item_produto_origem        = tmrp625.item_produto_origem
              and tmrp_625.alternativa_produto_origem = tmrp625.alternativa_produto_origem
              and tmrp_625.nivel_produto              = tmrp625.nivel_produto
              and tmrp_625.grupo_produto              = tmrp625.grupo_produto
              and tmrp_625.subgrupo_produto           = tmrp625.subgrupo_produto
              and tmrp_625.item_produto               = tmrp625.item_produto
              and tmrp_625.alternativa_produto        = tmrp625.alternativa_produto;
         exception
         when OTHERS then
            raise_application_error(-20000,'Erro ao atualizar tmrp_625.');
         end;

         inter_pr_estrutura_plan (p_ordem_planejamento,  p_pedido_venda,
                                  p_pedido_reserva,      tmrp625.nivel_produto,
                                  tmrp625.grupo_produto, tmrp625.subgrupo_produto,
                                  tmrp625.item_produto,  tmrp625.alternativa_produto,
                                  v_quantidade_plan);
      end loop;
   end;
   else
      for basi050 in estrutura050
      loop
         v_nivel_comp := basi050.nivel_comp;
         v_grupo_comp := basi050.grupo_comp;
         v_sub_comp   := basi050.sub_comp;
         v_item_comp  := basi050.item_comp;
         v_sub_item   := basi050.sub_item;
         v_item_tem   := basi050.item_item;
         v_consumo    := basi050.consumo;
         v_seq_comp   := basi050.sequencia;

         if v_sub_comp = '000' or v_consumo = 0.0000000
         then
            begin
               select basi_040.sub_comp, basi_040.consumo
               into   v_sub_comp,        v_consumo
               from basi_040
               where basi_040.nivel_item       = basi050.nivel_item
                 and basi_040.grupo_item       = basi050.grupo_item
                 and basi_040.sub_item         = v_sub_item
                 and basi_040.item_item        = v_item_comp
                 and basi_040.alternativa_item = basi050.alternativa_item
                 and basi_040.sequencia        = v_seq_comp;
               exception
               when others then
                  v_sub_comp := '000';
                  v_consumo  := 0.0000000;
            end;
         end if;

         v_alternativa40 := 0;

         if v_item_comp = '000000'
         then
            begin
               select basi_040.item_comp, basi_040.alternativa_comp
               into   v_item_comp,        v_alternativa40
               from basi_040
               where basi_040.nivel_item       = basi050.nivel_item
                 and basi_040.grupo_item       = basi050.grupo_item
                 and basi_040.sub_item         = v_sub_comp
                 and basi_040.item_item        = v_item_tem
                 and basi_040.alternativa_item = basi050.alternativa_item
                 and basi_040.sequencia        = v_seq_comp;
               exception
               when others then
                  v_item_comp     := '000000';
                  v_alternativa40 := 0;
            end;
         end if;

         if v_alternativa40 = 0
         then
            v_alternativa40 := basi050.alternativa_comp;
         end if;

         v_quantidade_plan := p_quantidade * v_consumo;

         begin
            insert into tmrp_625
               (ordem_planejamento,                   pedido_venda,
                pedido_reserva,                       nivel_produto_origem,
                grupo_produto_origem,                 subgrupo_produto_origem,
                item_produto_origem,                  alternativa_produto_origem,
                nivel_produto,
                grupo_produto,                        subgrupo_produto,
                item_produto,                         alternativa_produto,
                consumo,                              qtde_reserva_planejada,
                qtde_reserva_programada,              qtde_areceber_programada
             )values(
                p_ordem_planejamento,                 p_pedido_venda,
                p_pedido_reserva,                     p_nivel_produto,
                p_grupo_produto,                      p_subgrupo_produto,
                p_item_produto,                       p_alternativa_produto,
                v_nivel_comp,
                v_grupo_comp,                         v_sub_comp,
                v_item_comp,                          v_alternativa40,
                v_consumo,                            v_quantidade_plan,
                0.000,                                0.000);

         exception when
         OTHERS then
            raise_application_error(-20000,'Nao inseriu tmrp_625 ' || sqlerrm);
         end;

         inter_pr_estrutura_plan (p_ordem_planejamento, p_pedido_venda,
                                  p_pedido_reserva,     v_nivel_comp,
                                  v_grupo_comp,         v_sub_comp,
                                  v_item_comp,          v_alternativa40,
                                  v_quantidade_plan);
      end loop;
   end if;
end inter_pr_estrutura_plan;

 

/

exec inter_pr_recompile;

