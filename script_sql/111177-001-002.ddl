create table sped_0220
(periodo_sped  number(6) default 0);

drop table sped_0220;

CREATE TABLE SPED_0220 (
  COD_EMPRESA             NUMBER(3)     DEFAULT 0   NOT NULL,
  COD_NIVEL               VARCHAR2(1)   DEFAULT ' ' NOT NULL,
  COD_GRUPO               VARCHAR2(5)   DEFAULT ' ' NOT NULL,
  COD_SUBGRUPO            VARCHAR2(3)   DEFAULT ' ' NOT NULL,
  COD_ITEM                VARCHAR2(6)   DEFAULT ' ' NOT NULL,
  cod_unid_cov  VARCHAR2(2)  DEFAULT ' ',
  fator_conv    NUMBER(13,6) DEFAULT 0
);

ALTER TABLE SPED_0220 ADD CONSTRAINT PK_SPED_0220 PRIMARY KEY(COD_EMPRESA, COD_NIVEL, COD_GRUPO, COD_SUBGRUPO, COD_ITEM);

COMMENT ON COLUMN SPED_0220.COD_EMPRESA  IS 'Codigo da empresa que gera o arquivo';
COMMENT ON COLUMN SPED_0220.COD_NIVEL    IS 'Nivel do produto que teve fator de conversao';
COMMENT ON COLUMN SPED_0220.COD_GRUPO    IS 'Grupo do produto que teve fator de conversao';
COMMENT ON COLUMN SPED_0220.COD_SUBGRUPO IS 'Sub grupo do produto que teve fator de conversao';
COMMENT ON COLUMN SPED_0220.COD_ITEM     IS 'Item/cor do produto que teve fator de conversao';
COMMENT ON COLUMN SPED_0220.cod_unid_cov IS 'Unidade de conversao';
COMMENT ON COLUMN SPED_0220.fator_conv   IS 'Fator de conversao';

/
exec inter_pr_recompile;
