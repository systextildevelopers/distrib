CREATE OR REPLACE PROCEDURE "INTER_PR_LIQUIDA_PEDIDOS_VENDA"
is

       cursor pedidos is 
       select pedi_100.pedido_venda, pedi_100.situacao_venda,
              pedi_100.desconto1,    pedi_100.desconto2,
              pedi_100.desconto3
       from pedi_100
       where pedi_100.cod_cancelamento = 0
         and pedi_100.situacao_venda   <> 10
         and pedi_100.qtde_total_pedi > 0
         and round((qtde_saldo_pedi / pedi_100.qtde_total_pedi) * 100, 2) <=  inter_fn_get_param_double(pedi_100.codigo_empresa, 'vendas.percLiqSldPed')
         and exists (
             select 1 from pedi_110 
             where pedi_110.pedido_venda      = pedi_100.pedido_venda
               and pedi_110.situacao_fatu_it  = 2
               and pedi_110.cod_cancelamento  = 0
         );

       cursor itens (p_pedido_venda number) is
         select pedi_110.* from pedi_100, pedi_110 
         where pedi_100.pedido_venda     = p_pedido_venda
           and pedi_100.pedido_venda     = pedi_110.pedido_venda
           and pedi_110.situacao_fatu_it = 2
           and pedi_110.cod_cancelamento = 0
           and not exists (
               select 1 from pedi_110 
               where pedi_110.pedido_venda      = pedi_100.pedido_venda
                 and (pedi_110.qtde_faturada    = 0 or pedi_110.qtde_afaturar > 0)
           )
           and not exists (
               select pcpb_030.ordem_producao
               from pcpb_040, pcpb_010, pcpb_030
               where pcpb_040.data_termino     is null 
                 and pcpb_040.ordem_producao   = pcpb_010.ordem_producao 
                 and pcpb_010.cod_cancelamento = 0 
                 and pcpb_010.ordem_producao   = pcpb_030.ordem_producao 
                 and pcpb_030.nr_pedido_ordem  = pedi_100.pedido_venda
                 and pcpb_030.sequenci_periodo = pedi_110.seq_item_pedido
                 and pcpb_030.pedido_corte     = 3 
           )
           and not exists(
               select pcpt_020.codigo_rolo 
               from pcpt_020
               where pcpt_020.pedido_venda    = pedi_100.pedido_venda 
                 and pcpt_020.seq_item_pedido = pedi_110.seq_item_pedido
                 and pcpt_020.rolo_estoque    in (3, 4, 5) 
                 and pcpt_020.nr_solic_volume = 0
           )
           and not exists(
               select estq_060.status_caixa
               from estq_060 
               where estq_060.numero_pedido    = pedi_100.pedido_venda 
                 and estq_060.sequencia_pedido = pedi_110.seq_item_pedido
                 and estq_060.status_caixa     not in (4,9)
           );
     
       cursor itens_nao_cancelados (p_pedido_venda number) is
         select * 
         from pedi_110 
         where pedi_110.pedido_venda = p_pedido_venda
           and pedi_110.cod_cancelamento = 0;
       
       v_pronta_entrega                        basi_205.pronta_entrega%type;
       v_qtde_empenhada                        estq_040.qtde_empenhada%type;
       v_qtde_dev                              pedi_110.qtde_pedida%type;
       v_registro_ok                           boolean;
       v_existe_reg1                           boolean;
       v_existe_reg2                           boolean;
       v_valor_liq_unit                        pedi_110.valor_unitario%type;
       v_qtde_saldo_item                       pedi_110.qtde_pedida%type;
       v_qtde_total_saldo                      pedi_110.qtde_pedida%type;
       v_valor_total_saldo                     pedi_110.valor_unitario%type;
       v_situacao_pedi_ant                     pedi_100.situacao_venda%type;
       v_situacao_atualiza                     pedi_100.situacao_venda%type;

begin

   for pedido in pedidos
   loop
     
     v_registro_ok := false;
   
     for item in itens(pedido.pedido_venda)
     loop
     
         v_registro_ok := true;    
     
         v_qtde_dev := item.qtde_pedida - item.qtde_faturada;
     
         /*DELETE A SUGESTÃO DE FATURAMENTO*/
         begin
           delete fatu_780 
           where pedido    = item.pedido_venda 
             and sequencia = item.seq_item_pedido;
         end;

         /*ATUALIZA A SITUAÇÃO DO ITEM PARA FATURADO TOTAL*/
        begin
          update pedi_110 
          set situacao_fatu_it = 1 
          where pedi_110.pedido_venda    = item.pedido_venda 
            and pedi_110.seq_item_pedido = item.seq_item_pedido;
        end;
        
        /*SE FOR DEPOSITO DE PRONTA ENTREGA, FAZ O DESEMPENHO AUTOMATICAMENTE*/
        begin
          select basi_205.pronta_entrega 
          into v_pronta_entrega
          from basi_205
          where basi_205.codigo_deposito = item.codigo_deposito;
        end;
        
        if v_pronta_entrega = 1
        then 
          begin
            select estq_040.qtde_empenhada
            into v_qtde_empenhada
            from estq_040 
            where estq_040.cditem_nivel99  = item.cd_it_pe_nivel99
              and estq_040.cditem_grupo    = item.cd_it_pe_grupo
              and estq_040.cditem_subgrupo = item.cd_it_pe_subgrupo
              and estq_040.cditem_item     = item.cd_it_pe_subgrupo
              and estq_040.deposito        = item.codigo_deposito
              and estq_040.lote_acomp      = item.lote_empenhado;
          exception 
            when no_data_found 
              then v_qtde_empenhada := 0;
          end;
          
          if v_qtde_empenhada < v_qtde_dev
          then 
            v_qtde_dev := v_qtde_empenhada;
          end if;
          
          begin
            update estq_040 
            set qtde_empenhada = qtde_empenhada - v_qtde_dev
            where estq_040.cditem_nivel99  = item.cd_it_pe_nivel99
              and estq_040.cditem_grupo    = item.cd_it_pe_grupo
              and estq_040.cditem_subgrupo = item.cd_it_pe_subgrupo
              and estq_040.cditem_item     = item.cd_it_pe_item
              and estq_040.deposito        = item.codigo_deposito
              and estq_040.lote_acomp      = item.lote_empenhado;
          end;
        end if;
        
        /*SE HOUVER TAG ASSOCIADA A SEQ DO PEDIDO, RECOLOCÁ-LA NO ESTOQUE*/
        begin
          update pcpc_330 
          set pcpc_330.pedido_venda      = 0, 
              pcpc_330.seq_item_pedido   = 0,
              pcpc_330.flag_controle     = 0, 
              pcpc_330.estoque_tag       = 1,
              pcpc_330.nome_prog_050     = 'fatu_f860' 
          where pcpc_330.pedido_venda    = item.pedido_venda 
            and pcpc_330.seq_item_pedido = item.seq_item_pedido
            and pcpc_330.estoque_tag     in (2,3);
        end;
        
        commit;
     end loop;
     
     if v_registro_ok
     then 
        
        v_existe_reg1 := false;  
        v_existe_reg2 := false;
        
        v_qtde_total_saldo := 0;
        v_valor_total_saldo := 0.00;
        
        for item in itens_nao_cancelados(pedido.pedido_venda)
        loop
            v_existe_reg1 := true;
            
            if item.situacao_fatu_it <> 1 or item.qtde_afaturar > 0.00
            then
              v_existe_reg2 := true;
              
              v_valor_liq_unit := item.valor_unitario - (item.valor_unitario * item.percentual_desc / 100.00);
              
              v_qtde_saldo_item := item.qtde_pedida - item.qtde_faturada;
              if v_qtde_saldo_item < 0.00
              then 
                v_qtde_saldo_item := 0;
              end if;
              
              v_qtde_total_saldo := v_qtde_total_saldo + v_qtde_saldo_item;
              
              v_valor_total_saldo := v_valor_total_saldo + (v_qtde_saldo_item * v_valor_liq_unit);
              
            end if;
        end loop;
        
        v_situacao_pedi_ant := pedido.situacao_venda;
        
        v_situacao_atualiza := 0;
        
        if v_existe_reg2
        then
          v_situacao_atualiza := v_situacao_pedi_ant;
        else 
          if v_existe_reg1 or v_situacao_pedi_ant = 9
          then
            v_situacao_atualiza := 10;
          else 
            v_situacao_atualiza := v_situacao_pedi_ant;
          end if;
        end if;
        
        /*ATUALIZA VALOR SALDO COM O VALOR DE LIQUIDO DO ITEM MENOS DESCONTO DA CAPA*/
        v_valor_total_saldo := v_valor_total_saldo - (v_valor_total_saldo * pedido.desconto1 / 100.00);
        v_valor_total_saldo := v_valor_total_saldo - (v_valor_total_saldo * pedido.desconto2 / 100.00);
        v_valor_total_saldo := v_valor_total_saldo - (v_valor_total_saldo * pedido.desconto3 / 100.00);
        
        begin
          update pedi_100 
          set qtde_saldo_pedi  = v_qtde_total_saldo, 
              valor_saldo_pedi = v_valor_total_saldo,
              situacao_venda   = v_situacao_atualiza 
          where pedido_venda   = pedido.pedido_venda;
        end;
        
        begin
          delete pedi_135 
          where pedido_venda = pedido.pedido_venda 
            and flag_liberacao = 'N';
        end;
     end if;
     
   end loop;
end inter_pr_liquida_pedidos_venda;
