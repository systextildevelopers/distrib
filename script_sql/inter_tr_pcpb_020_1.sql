
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_020_1" 
   before update of qtde_quilos_real
   on pcpb_020
   for each row

declare
   v_periodo_producao    pcpb_010.periodo_producao%type;
   v_ordem_servico       number;
   v_qtde_atu_os         number;
   v_situacao_os         number;
   v_codigo_estagio_os   number;
   v_ob_preparadas       number;
   v_qtde_areceber       number;
   v_sequencia_oper      number(9);

begin
   ----------------------------------------------------------------------------------
   ----- INICIO - BLOCO DE ATUALIZACAO (UPDATING)                               -----
   -----                                                                        -----
   -----                                                                        -----
   ----------------------------------------------------------------------------------
   if updating
   then
      -- Busca o periodo e tipo da ordem de producao da OB.
      begin
         select pcpb_010.periodo_producao,
                pcpb_010.ordem_servico
         into   v_periodo_producao,
                v_ordem_servico
         from pcpb_010
         where pcpb_010.ordem_producao = :old.ordem_producao;
         exception
         when others then
            v_periodo_producao := 0;
            v_ordem_servico    := 0;
            raise_application_error(-20000,'Capa da Ordem de Beneficiamento nao cadastrada (PCPB_010).');
      end;

      if :new.qtde_quilos_real <> :old.qtde_quilos_real
      then
         -- Se a OB estiver relacionada a uma OS, as quantidades a retornar da OS devem ser atualizadas.
         -- Para geracao automatica de OS para OB, a OB nao precisa estar preparada para digitar a OS,
         -- entao, a quantidade que sera enviada na OS deve ser atualizada com a quantidade preparada
         -- da OB.
         if v_ordem_servico > 0
         then
            -- Display de teste...
            -- raise_application_error(-20000,'NEYLOR - TESTANDO A TRIGGER DE BENEFICIAMENTO');
            select obrf_080.situacao_ordem, obrf_070.estagio_baixa
            into   v_situacao_os,           v_codigo_estagio_os
            from obrf_080, obrf_070
            where obrf_080.numero_ordem   = v_ordem_servico
              and obrf_080.codigo_servico = obrf_070.codigo_terceiro;

            -- Se a ordem de servico estiver em producao, nao pode mais alterar a quantidade
            -- preparada para a ordem de beneficiamento da ordem de servico, pois o sistema
            -- entende que a ordem ja foi enviada para o terceiro.
            if v_situacao_os >= 2
            then
               raise_application_error(-20000,'ATENCAO! ORDEM DE SERVICO DA ORDEM DE BEMEFICIAMENTO EM PRODUCAO. NAO PODE ALTERAR A QUANTIDADE PREPARADA. ');
            end if;

            -- Quantidade que sera atualizada na quantidade a retornar.
            v_qtde_atu_os := 0.000;

            -- Verifica se ja tem alguma outra OB preparada para a mesma O.S/Seq.
            begin
               select obrf_081.preparacao_iniciada into v_ob_preparadas
               from obrf_081
               where obrf_081.numero_ordem = v_ordem_servico
                 and obrf_081.sequencia    = :new.seq_ordem_servico;
               exception when no_data_found then
                  v_ob_preparadas := 0;
            end;

            -- Quantidade que sera atualizada na quantidade a retornar.
            v_qtde_atu_os := 0.000;

            if :old.qtde_quilos_real = 0.000 and :new.qtde_quilos_real > 0.000 and v_ob_preparadas = 0
            then
               v_qtde_atu_os := :new.qtde_quilos_real;

               update obrf_081
               set obrf_081.qtde_areceber       = v_qtde_atu_os,
                   obrf_081.preparacao_iniciada = 1
               where obrf_081.numero_ordem = v_ordem_servico
                 and obrf_081.sequencia    = :new.seq_ordem_servico;
            else
               v_qtde_atu_os := :new.qtde_quilos_real - :old.qtde_quilos_real;

               update obrf_081
               set obrf_081.qtde_areceber = obrf_081.qtde_areceber + v_qtde_atu_os
               where obrf_081.numero_ordem = v_ordem_servico
                 and obrf_081.sequencia    = :new.seq_ordem_servico;

               -- Verifica como ficou a qtde_areceber para saber se deixa a flag de preparacao zerada.
               begin
                  select obrf_081.qtde_areceber into v_qtde_areceber
                  from obrf_081
                  where obrf_081.numero_ordem = v_ordem_servico
                    and obrf_081.sequencia    = :new.seq_ordem_servico;
                  exception when no_data_found then
                     v_qtde_areceber := 0;
               end;

               if v_qtde_areceber <= 0
               then
                  update obrf_081
                  set obrf_081.preparacao_iniciada = 0
                  where obrf_081.numero_ordem = v_ordem_servico
                    and obrf_081.sequencia    = :new.seq_ordem_servico;
               else
                  update obrf_081
                  set obrf_081.preparacao_iniciada = 1
                  where obrf_081.numero_ordem = v_ordem_servico
                    and obrf_081.sequencia    = :new.seq_ordem_servico;
               end if;
            end if;

            begin
               select obrf_081.qtde_areceber into v_qtde_areceber
               from obrf_081
               where obrf_081.numero_ordem = v_ordem_servico
                 and obrf_081.sequencia    = :new.seq_ordem_servico;
               exception when no_data_found then
                  v_qtde_areceber := 0;
            end;

            -- Chama procedure de explosao de estrutura, indentificando a chamada como 'obrf'
            -- para que a procedure atualize a quantidade dos itens a enviar para o fornecedor
            -- que estao gravados na tabela obrf_082.
            inter_pr_explode_estrutura(:new.pano_sbg_nivel99,
                                       :new.pano_sbg_grupo,
                                       :new.pano_sbg_subgrupo,
                                       :new.pano_sbg_item,
                                       :new.alternativa_item,
                                       v_periodo_producao,
                                       v_qtde_atu_os,
                                       v_ordem_servico,
                                       :new.seq_ordem_servico,
                                       2,
                                       0,
                                       v_codigo_estagio_os,
                                       v_codigo_estagio_os,
                                       v_qtde_areceber,
                                       0,
                                       'obrf',1);

         end if;
      end if;
   end if;
   ----------------------------------------------------------------------------------
   ----- FINAL - BLOCO DE ATUALIZACAO (UPDATING)                                -----
   -----                                                                        -----
   -----                                                                        -----
   ----------------------------------------------------------------------------------
end inter_tr_pcpb_020_1;
-- ALTER TRIGGER "INTER_TR_PCPB_020_1" ENABLE
 

/

exec inter_pr_recompile;

