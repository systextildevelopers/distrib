
  CREATE OR REPLACE FUNCTION "INTER_FN_TMRP_F107_START" (p_codigo_empresa number,  p_data_emissao_ini date,
                                                    p_data_emissao_fim date,  p_data_embarque_ini date,
                                                    p_data_embarque_fim date, p_codigo_usuario number,
                                                    p_num_aleatorio number,   p_nr_solicitacao number,
                                                    p_usuario varchar,        p_ie_pedido number,
                                                    p_ie_cliente number,      p_ie_produto number,
                                                    p_ie_div_prod number,     p_ie_estagio number,
                                                    p_ie_artigo number)
return number is
   v_periodo_producao        number := 0;
   v_seq_registro            number := 0;
   v_retorno                 number := 1;
   v_tmpInt                  number := 0;

begin
   /*************************************************************************************************************
      GERAR A BASE DE DADOS PARA QUE OS SUBFORM POSSAM USAR
    *************************************************************************************************************/
   for reg_itens in (select
                            pedi_100.pedido_venda,                           pedi_100.data_entr_venda data_requerida,
                            pedi_100.data_emis_venda data_emissao,
                            pedi_110.cd_it_pe_nivel99 nivel_produto,         pedi_110.cd_it_pe_grupo grupo_produto,
                            pedi_110.cd_it_pe_subgrupo subgrupo_produto,     pedi_110.cd_it_pe_item item_produto,

                            pedi_110.qtde_pedida - nvl(t630.qtde,0) qtde_a_programar,

                            basi_010.numero_alternati alternativa_peca,      basi_010.numero_roteiro roteiro_peca,
                            pedi_100.cod_ped_cliente cod_pedido_cliente,     basi_030.div_prod codigo_familia,
                            nvl(basi_001.codigo_projeto,' ') codigo_projeto, pedi_100.classificacao_pedido
                  from pedi_110,pedi_100,mqop_050,basi_010,basi_030,basi_001,(select t630.nivel_produto,    t630.grupo_produto,
                                                                                     t630.subgrupo_produto, t630.item_produto,
                                                                                     t630.pedido_venda,
                                                                                     nvl(sum(t630.programado_comprado),0) qtde
                                                                              from tmrp_630 t630
                                                                              where t630.area_producao = 1
                                                                              group by t630.nivel_produto,    t630.grupo_produto,
                                                                                       t630.subgrupo_produto, t630.item_produto,
                                                                                       t630.pedido_venda) t630
                  where  pedi_110.pedido_venda      = pedi_100.pedido_venda
                  and    pedi_110.cd_it_pe_nivel99  = basi_010.nivel_estrutura
                  and    pedi_110.cd_it_pe_grupo    = basi_010.grupo_estrutura
                  and    pedi_110.cd_it_pe_subgrupo = basi_010.subgru_estrutura
                  and    pedi_110.cd_it_pe_item     = basi_010.item_estrutura
                  and    pedi_110.cd_it_pe_nivel99  = basi_030.nivel_estrutura
                  and    pedi_110.cd_it_pe_grupo    = basi_030.referencia
                  and    pedi_110.cd_it_pe_nivel99  = mqop_050.nivel_estrutura
                  and    pedi_110.cd_it_pe_grupo    = mqop_050.grupo_estrutura
                  and   (pedi_110.cd_it_pe_subgrupo = mqop_050.subgru_estrutura or mqop_050.subgru_estrutura = '000')
                  and   (pedi_110.cd_it_pe_item     = mqop_050.item_estrutura or mqop_050.item_estrutura = '000000')
                  and   (mqop_050.numero_alternati  = basi_010.numero_alternati
                  or     basi_010.numero_alternati  = 0)
                  and   (mqop_050.numero_roteiro    = basi_010.numero_roteiro
                  or     basi_010.numero_roteiro    = 0)
                  and    basi_010.nivel_estrutura   = basi_001.nivel_produto (+)
                  and    basi_010.grupo_estrutura   = basi_001.grupo_produto (+)
                  and    pedi_110.pedido_venda      = t630.pedido_venda (+)
                  and    pedi_110.cd_it_pe_nivel99  = t630.nivel_produto (+)
                  and    pedi_110.cd_it_pe_grupo    = t630.grupo_produto (+)
                  and    pedi_110.cd_it_pe_subgrupo = t630.subgrupo_produto (+)
                  and    pedi_110.cd_it_pe_item     = t630.item_produto (+)
                  and    pedi_110.cod_cancelamento  = 0
                  and    pedi_100.cod_cancelamento  = 0
                  and    pedi_100.tecido_peca       = 1
                  and    pedi_100.tipo_pedido       = 0
                  and    pedi_100.situacao_venda   <> 5
                  and   (pedi_110.liquida_saldo_aprogramar is null or pedi_110.liquida_saldo_aprogramar = 0)
                  and    pedi_100.codigo_empresa    = p_codigo_empresa
                  and    pedi_110.qtde_pedida - nvl(t630.qtde,0) > 0
                  and    pedi_100.data_emis_venda between p_data_emissao_ini  and p_data_emissao_fim
                  and    pedi_100.data_entr_venda between p_data_embarque_ini and p_data_embarque_fim

                  and    exists (select 1
                                 from rcnb_060
                                 where rcnb_060.tipo_registro     = 179
                                 and   rcnb_060.nr_solicitacao    = 107
                                 and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                 and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                 and   pedi_100.pedido_venda      = rcnb_060.grupo_estrutura
                                 and   p_ie_pedido   = 1
                                 UNION
                                 select 1
                                 from rcnb_060
                                 where rcnb_060.tipo_registro     = 179
                                 and   rcnb_060.nr_solicitacao    = 107
                                 and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                 and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                 and   pedi_100.pedido_venda      <> rcnb_060.grupo_estrutura
                                 and   p_ie_pedido   = 2)
                  and    exists (select 1
                                 from rcnb_060
                                 where rcnb_060.tipo_registro     = 60
                                 and   rcnb_060.nr_solicitacao    = 107
                                 and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                 and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                 and   pedi_100.cli_ped_cgc_cli9  = rcnb_060.grupo_estrutura and pedi_100.cli_ped_cgc_cli4 = rcnb_060.item_estrutura and pedi_100.cli_ped_cgc_cli2 = rcnb_060.nivel_estrutura
                                 and   p_ie_cliente  = 1
                                 UNION
                                 select 1
                                 from rcnb_060
                                 where rcnb_060.tipo_registro     = 60
                                 and   rcnb_060.nr_solicitacao    = 107
                                 and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                 and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                 and   pedi_100.cli_ped_cgc_cli9  <> rcnb_060.grupo_estrutura and pedi_100.cli_ped_cgc_cli4 <> rcnb_060.item_estrutura and pedi_100.cli_ped_cgc_cli2 <> rcnb_060.nivel_estrutura
                                 and   p_ie_cliente  = 2)
                  and    exists (select 1
                                 from rcnb_060
                                 where rcnb_060.tipo_registro        = 548
                                 and   rcnb_060.nr_solicitacao       = 107
                                 and   rcnb_060.subgru_estrutura     = p_codigo_usuario
                                 and   rcnb_060.tipo_registro_rel    = p_num_aleatorio
                                 and   rcnb_060.nivel_estrutura_str  = pedi_110.cd_it_pe_nivel99 and rcnb_060.grupo_estrutura_str in (pedi_110.cd_it_pe_grupo,'XXXXXX') and rcnb_060.subgru_estrutura_str in (pedi_110.cd_it_pe_subgrupo,'XXXX') and rcnb_060.descricao in (pedi_110.cd_it_pe_item,'XXXXXXX')
                                 and   p_ie_produto     = 1
                                 UNION
                                 select 1
                                 from rcnb_060
                                 where rcnb_060.tipo_registro        = 548
                                 and   rcnb_060.nr_solicitacao       = 107
                                 and   rcnb_060.subgru_estrutura     = p_codigo_usuario
                                 and   rcnb_060.tipo_registro_rel    = p_num_aleatorio
                                 and   rcnb_060.nivel_estrutura_str  = pedi_110.cd_it_pe_nivel99 and rcnb_060.grupo_estrutura_str <> pedi_110.cd_it_pe_grupo and rcnb_060.subgru_estrutura_str <> pedi_110.cd_it_pe_subgrupo and rcnb_060.descricao <> pedi_110.cd_it_pe_item
                                 and   p_ie_produto     = 2)
                  and    exists (select 1
                                 from rcnb_060
                                 where rcnb_060.tipo_registro     = 595
                                 and   rcnb_060.nr_solicitacao    = 107
                                 and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                 and   rcnb_060.grupo_estrutura   = p_codigo_empresa
                                 and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                 and   rcnb_060.item_estrutura    = basi_030.div_prod
                                 and   p_ie_div_prod = 1
                                 UNION
                                 select 1
                                 from rcnb_060
                                 where rcnb_060.tipo_registro     = 595
                                 and   rcnb_060.nr_solicitacao    = 107
                                 and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                 and   rcnb_060.grupo_estrutura   = p_codigo_empresa
                                 and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                 and   rcnb_060.item_estrutura    <> basi_030.div_prod
                                 and   p_ie_div_prod = 2)
                  and    exists (select 1
                                 from rcnb_060
                                 where rcnb_060.tipo_registro     = 270
                                 and   rcnb_060.nr_solicitacao    = 107
                                 and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                 and   rcnb_060.empresa_rel       = p_codigo_empresa
                                 and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                 and   rcnb_060.grupo_estrutura   = mqop_050.codigo_estagio
                                 and   p_ie_estagio = 1
                                 UNION
                                 select 1
                                 from rcnb_060
                                 where rcnb_060.tipo_registro     = 270
                                 and   rcnb_060.nr_solicitacao    = 107
                                 and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                 and   rcnb_060.empresa_rel       = p_codigo_empresa
                                 and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                 and   rcnb_060.grupo_estrutura  <> mqop_050.codigo_estagio
                                 and   p_ie_estagio = 2)
                  and    exists (select 1
                                 from rcnb_060
                                 where rcnb_060.tipo_registro     = 778
                                 and   rcnb_060.nr_solicitacao    = 107
                                 and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                 and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                 and   rcnb_060.grupo_estrutura   = basi_030.artigo
                                 and   p_ie_artigo = 1
                                 UNION
                                 select 1
                                 from rcnb_060
                                 where rcnb_060.tipo_registro     = 778
                                 and   rcnb_060.nr_solicitacao    = 107
                                 and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                 and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                 and   rcnb_060.grupo_estrutura  <> basi_030.artigo
                                 and   p_ie_artigo   = 2)
                  and    exists (select 1
                                 from inter_vi_estrutura, basi_020
                                 where inter_vi_estrutura.nivel_comp       = basi_020.basi030_nivel030
                                 and   inter_vi_estrutura.grupo_comp       = basi_020.basi030_referenc
                                 and   inter_vi_estrutura.sub_comp         = basi_020.tamanho_ref
                                 and   inter_vi_estrutura.nivel_item       = pedi_110.cd_it_pe_nivel99
                                 and   inter_vi_estrutura.grupo_item       = pedi_110.cd_it_pe_grupo
                                 and   inter_vi_estrutura.sub_item         = pedi_110.cd_it_pe_subgrupo
                                 and   inter_vi_estrutura.item_item        = pedi_110.cd_it_pe_item
                                 and   inter_vi_estrutura.alternativa_item = basi_010.numero_alternati
                                 and ((inter_vi_estrutura.nivel_comp       = '2' )
                                  or   inter_vi_estrutura.nivel_comp       = '1') )
                  group by pedi_100.pedido_venda,             pedi_100.data_entr_venda,
                           pedi_100.data_emis_venda,
                           pedi_110.cd_it_pe_nivel99,         pedi_110.cd_it_pe_grupo,
                           pedi_110.cd_it_pe_subgrupo,        pedi_110.cd_it_pe_item,

                           pedi_110.qtde_pedida - nvl(t630.qtde,0),

                           basi_010.numero_alternati,         basi_010.numero_roteiro,
                           pedi_100.cod_ped_cliente,          basi_030.div_prod,
                           nvl(basi_001.codigo_projeto,' '),  pedi_100.classificacao_pedido)
   loop

      v_tmpInt := 0;


      begin
       select 1
       into v_periodo_producao
       from rcnb_060
       where rcnb_060.tipo_registro     = 132
         and rcnb_060.nr_solicitacao    = 107
         and rcnb_060.subgru_estrutura  = p_codigo_usuario
         and  rcnb_060.empresa_rel      = p_codigo_empresa
         and  rcnb_060.relatorio_rel    = 'tmrp_f107'
         and (rcnb_060.grupo_estrutura  = reg_itens.classificacao_pedido
          or  rcnb_060.grupo_estrutura  = 999999);
       exception when others then
         v_tmpInt := 1;

    end;



      v_periodo_producao := 0;
      if  reg_itens.data_requerida is not null
      then
         begin
            select pcpc_010.periodo_producao
            into   v_periodo_producao
            from pcpc_010
            where pcpc_010.data_ini_periodo <= reg_itens.data_requerida
            and   pcpc_010.data_fim_periodo >= reg_itens.data_requerida
            and   pcpc_010.codigo_empresa    = p_codigo_empresa
            and   pcpc_010.area_periodo      = 1;
            exception when others then
               v_periodo_producao := 0;
         end;
      end if;


     if v_tmpInt=0
     then
      v_seq_registro := v_seq_registro + 1;
      begin
         INSERT /*+APPEND */ into tmrp_615_107 (
            nome_programa,                    nr_solicitacao,
            codigo_usuario,                   tipo_registro,
            seq_registro,                     situacao,
            codigo_empresa,                   usuario,

            ordem_planejamento,               pedido_venda,
            grupo_planejamento,               codigo_projeto,
            periodo_producao,

            nivel_prod,                       grupo_prod,
            subgrupo_prod,                    item_prod,
            alternativa,                      roteiro,

            data_inicio,
            qtde_programacao,                 data_real_termino,
            seq_ot_original,                  qtde_reserva
         ) VALUES (
            'tmrp_f107',                      p_nr_solicitacao,
            p_codigo_usuario,                 999,  -- GERAR BASE DE DADOS
            v_seq_registro,                   'X',
            p_codigo_empresa,                 p_usuario,

            0,                                reg_itens.pedido_venda,
            ' ',                              reg_itens.codigo_projeto,
            v_periodo_producao,

            reg_itens.nivel_produto,          reg_itens.grupo_produto,
            reg_itens.subgrupo_produto,       reg_itens.item_produto,
            reg_itens.alternativa_peca,       reg_itens.roteiro_peca,

            reg_itens.data_emissao,
            reg_itens.qtde_a_programar,       reg_itens.data_requerida,
            reg_itens.codigo_familia,         reg_itens.qtde_a_programar
         );
      end;
     end if;
   end loop;

   if not sql%found
   then
      v_retorno:= 0;
   end if;
   commit;


   return(v_retorno);
end;

 

/

exec inter_pr_recompile;

