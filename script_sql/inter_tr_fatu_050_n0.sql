
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_050_N0" 
after insert or delete or update
on fatu_050
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
   -- dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);  
       

   if :new.num_nota_fiscal = 0
   then
      if inserting
      then
         begin
            insert into fatu_050_n0 (
                tipo_ocorr,   /*0*/
                data_ocorr,   /*1*/
                hora_ocorr,   /*2*/
                usuario_rede,   /*3*/
                maquina_rede,   /*4*/
                aplicacao,   /*5*/
                usuario_sistema,   /*6*/
                nome_programa,   /*7*/
                codigo_empresa_old,   /*8*/
                codigo_empresa_new,   /*9*/
                num_nota_fiscal_old,   /*10*/
                num_nota_fiscal_new,   /*11*/
                serie_nota_fisc_old,   /*12*/
                serie_nota_fisc_new,   /*13*/
                especie_docto_old,   /*14*/
                especie_docto_new,   /*15*/
                data_base_fatur_old,   /*16*/
                data_base_fatur_new,   /*17*/
                data_saida_old,   /*18*/
                data_saida_new,   /*19*/
                data_emissao_old,   /*20*/
                data_emissao_new    /*21*/
            ) values (
                'i', /*0*/
                sysdate, /*1*/
                sysdate,/*2*/
                ws_usuario_rede,/*3*/
                ws_maquina_rede, /*4*/
                ws_aplicativo, /*5*/
                ws_usuario_systextil,/*6*/
                v_nome_programa, /*7*/
                0,/*8*/
                :new.codigo_empresa, /*9*/
                0,/*10*/
                :new.num_nota_fiscal, /*11*/
                '',/*12*/
                :new.serie_nota_fisc, /*13*/
                '',/*14*/
                :new.especie_docto, /*15*/
                null,/*16*/
                :new.data_base_fatur, /*17*/
                null,/*18*/
                :new.data_saida, /*19*/
                null,/*20*/
                :new.data_emissao /*21*/
            );
         end;
      end if;

      if updating
      then
         begin
            insert into fatu_050_n0 (
                tipo_ocorr, /*0*/
                data_ocorr, /*1*/
                hora_ocorr, /*2*/
                usuario_rede, /*3*/
                maquina_rede, /*4*/
                aplicacao, /*5*/
                usuario_sistema, /*6*/
                nome_programa, /*7*/
                codigo_empresa_old, /*8*/
                codigo_empresa_new, /*9*/
                num_nota_fiscal_old, /*10*/
                num_nota_fiscal_new, /*11*/
                serie_nota_fisc_old, /*12*/
                serie_nota_fisc_new, /*13*/
                especie_docto_old, /*14*/
                especie_docto_new, /*15*/
                data_base_fatur_old, /*16*/
                data_base_fatur_new, /*17*/
                data_saida_old, /*18*/
                data_saida_new, /*19*/
                data_emissao_old, /*20*/
                data_emissao_new  /*21*/
            ) values (
                'a', /*0*/
                sysdate, /*1*/
                sysdate, /*2*/
                ws_usuario_rede,/*3*/
                ws_maquina_rede, /*4*/
                ws_aplicativo, /*5*/
                ws_usuario_systextil,/*6*/
                v_nome_programa, /*7*/
               :old.codigo_empresa,  /*8*/
               :new.codigo_empresa, /*9*/
               :old.num_nota_fiscal,  /*10*/
               :new.num_nota_fiscal, /*11*/
               :old.serie_nota_fisc,  /*12*/
               :new.serie_nota_fisc, /*13*/
               :old.especie_docto,  /*14*/
               :new.especie_docto, /*15*/
               :old.data_base_fatur,  /*16*/
               :new.data_base_fatur, /*17*/
               :old.data_saida,  /*18*/
               :new.data_saida, /*19*/
               :old.data_emissao,  /*20*/
               :new.data_emissao  /*21*/
            );
         end;
      end if;

      if deleting
      then
         begin
             insert into fatu_050_n0 (
                tipo_ocorr, /*0*/
                data_ocorr, /*1*/
                hora_ocorr, /*2*/
                usuario_rede, /*3*/
                maquina_rede, /*4*/
                aplicacao, /*5*/
                usuario_sistema, /*6*/
                nome_programa, /*7*/
                codigo_empresa_old, /*8*/
                codigo_empresa_new, /*9*/
                num_nota_fiscal_old, /*10*/
                num_nota_fiscal_new, /*11*/
                serie_nota_fisc_old, /*12*/
                serie_nota_fisc_new, /*13*/
                especie_docto_old, /*14*/
                especie_docto_new, /*15*/
                data_base_fatur_old, /*16*/
                data_base_fatur_new, /*17*/
                data_saida_old, /*18*/
                data_saida_new, /*19*/
                data_emissao_old, /*20*/
                data_emissao_new /*21*/
             ) values (
                'd', /*0*/
                sysdate, /*1*/
                sysdate, /*2*/
                ws_usuario_rede,/*3*/
                ws_maquina_rede,/*4*/
                ws_aplicativo, /*5*/
                ws_usuario_systextil,/*6*/
                v_nome_programa, /*7*/
                :old.codigo_empresa, /*8*/
                0, /*9*/
                :old.num_nota_fiscal, /*10*/
                0, /*11*/
                :old.serie_nota_fisc, /*12*/
                '', /*13*/
                :old.especie_docto, /*14*/
                '', /*15*/
                :old.data_base_fatur, /*16*/
                null, /*17*/
                :old.data_saida, /*18*/
                null, /*19*/
                :old.data_emissao, /*20*/
                null /*21*/
             );
         end;
      end if;
   -- raise_application_error(-20000,'O Sistema est� tentando inserir uma nota zerada. Favor entrar em contato com a equipe de Atendimento da Intersys.');
   end if;
end inter_tr_fatu_050_n0;
-- ALTER TRIGGER "INTER_TR_FATU_050_N0" ENABLE
 

/

exec inter_pr_recompile;

