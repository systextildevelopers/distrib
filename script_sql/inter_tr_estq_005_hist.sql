
  CREATE OR REPLACE TRIGGER "INTER_TR_ESTQ_005_HIST" 
   after insert or delete or
   update of codigo_transacao, descricao, entrada_saida, transac_entrada,
	tipo_consumo, calcula_preco, conta_debito, conta_credito,
	atualiza_estoque, altera_custo, tipo_transacao, atualiza_contabi,
	atualiza_fcardex, trans_cancelamento, inside_pis_confins, exige_centro_custo,
	atual_estoque_f, tran_inativa, cod_agrup_trans, contab_cardex,
	icdtransdepterceiros, istajustefinanceiro, atualiza_estq_terceiro
   on estq_005
   for each row

-- Finalidade: Registrar altera��es feitas na tabela obrf_015 - itens das notas fiscais de entrada
-- Autor.....: Aline Blanski
-- Data......: 09/06/10
--
-- Hist�ricos de altera��es na trigger
--
-- Data      Autor           SS          Observa��es
-- 12/07/10  Aline Blanski   58329/001   Adicionado campos na verifica��o da trigger.

declare
   ws_tipo_ocorr                   varchar2 (1);
   ws_usuario_rede                 varchar2(20);
   ws_maquina_rede                 varchar2(40);
   ws_aplicativo                   varchar2(20);
   ws_sid                          number(9);
   ws_empresa                      number(3);
   ws_usuario_systextil            varchar2(250);
   ws_locale_usuario               varchar2(5);
   ws_nome_programa                varchar2(20);
   ws_codigo_transacao_old         NUMBER(3)    default 555;
   ws_codigo_transacao_new         NUMBER(3)    default 555;
   ws_descricao_old                VARCHAR2(20) default '' ;
   ws_descricao_new                VARCHAR2(20) default '' ;
   ws_tipo_transacao_old           VARCHAR2(1)  default '' ;
   ws_tipo_transacao_new           VARCHAR2(1)  default '' ;
   ws_entrada_saida_old            VARCHAR2(1)  default '' ;
   ws_entrada_saida_new            VARCHAR2(1)  default '' ;
   ws_transac_entrada_old          NUMBER(3)    default 0  ;
   ws_transac_entrada_new          NUMBER(3)    default 0  ;
   ws_calcula_preco_old            NUMBER(1)    default 0  ;
   ws_calcula_preco_new            NUMBER(1)    default 0  ;
   ws_altera_custo_old             NUMBER(1)    default 0  ;
   ws_altera_custo_new             NUMBER(1)    default 0  ;
   ws_atualiza_estoque_old         NUMBER(1)    default 0  ;
   ws_atualiza_estoque_new         NUMBER(1)    default 0  ;
   ws_atualiza_contabi_old         NUMBER(1)    default 0  ;
   ws_atualiza_contabi_new         NUMBER(1)    default 0  ;
   ws_contab_cardex_old            NUMBER(1)    default 1  ;
   ws_contab_cardex_new            NUMBER(1)    default 1  ;
   ws_trans_cancelamento_old       NUMBER(3)    default 0  ;
   ws_trans_cancelamento_new       NUMBER(3)    default 0  ;
   ws_inside_pis_confins_old       NUMBER(1)    default 0  ;
   ws_inside_pis_confins_new       NUMBER(1)    default 0  ;
   ws_exige_centro_custo_old       VARCHAR2(1)  default 'N';
   ws_exige_centro_custo_new       VARCHAR2(1)  default 'N';
   ws_cod_agrup_trans_old          NUMBER(2)    default 0  ;
   ws_cod_agrup_trans_new          NUMBER(2)    default 0  ;
   ws_atualiza_estq_terceiro_old   NUMBER(1)    default 0;
   ws_atualiza_estq_terceiro_new   NUMBER(1)    default 0;

begin
   if INSERTING then

      ws_tipo_ocorr                := 'I';
      ws_codigo_transacao_old      := 0;
      ws_codigo_transacao_new      := :new.codigo_transacao;
      ws_descricao_old             := null;
      ws_descricao_new             := :new.descricao;
      ws_tipo_transacao_old        := null;
      ws_tipo_transacao_new        := :new.tipo_transacao;
      ws_entrada_saida_old         := null;
      ws_entrada_saida_new         := :new.entrada_saida;
      ws_transac_entrada_old       := 0;
      ws_transac_entrada_new       := :new.transac_entrada;
      ws_calcula_preco_old         := 0;
      ws_calcula_preco_new         := :new.calcula_preco;
      ws_altera_custo_old          := 0;
      ws_altera_custo_new          := :new.altera_custo;
      ws_atualiza_estoque_old      := 0;
      ws_atualiza_estoque_new      := :new.atualiza_estoque;
      ws_atualiza_contabi_old      := 0;
      ws_atualiza_contabi_new      := :new.atualiza_contabi;
      ws_contab_cardex_old         := 0;
      ws_contab_cardex_new         := :new.contab_cardex;
      ws_trans_cancelamento_old    := 0;
      ws_trans_cancelamento_new    := :new.trans_cancelamento;
      ws_inside_pis_confins_old    := 0;
      ws_inside_pis_confins_new    := :new.inside_pis_confins;
      ws_exige_centro_custo_old    := null;
      ws_exige_centro_custo_new    := :new.exige_centro_custo;
      ws_cod_agrup_trans_old       := 0;
      ws_cod_agrup_trans_new       := :new.cod_agrup_trans;
      ws_atualiza_estq_terceiro_old := 0;
      ws_atualiza_estq_terceiro_new := :new.atualiza_estq_terceiro;

   elsif UPDATING then

      ws_tipo_ocorr                := 'A';
      ws_codigo_transacao_old      := :old.codigo_transacao;
      ws_codigo_transacao_new      := :new.codigo_transacao;
      ws_descricao_old             := :old.descricao;
      ws_descricao_new             := :new.descricao;
      ws_tipo_transacao_old        := :old.tipo_transacao;
      ws_tipo_transacao_new        := :new.tipo_transacao;
      ws_entrada_saida_old         := :old.entrada_saida;
      ws_entrada_saida_new         := :new.entrada_saida;
      ws_transac_entrada_old       := :old.transac_entrada;
      ws_transac_entrada_new       := :new.transac_entrada;
      ws_calcula_preco_old         := :old.calcula_preco;
      ws_calcula_preco_new         := :new.calcula_preco;
      ws_altera_custo_old          := :old.altera_custo;
      ws_altera_custo_new          := :new.altera_custo;
      ws_atualiza_estoque_old      := :old.atualiza_estoque;
      ws_atualiza_estoque_new      := :new.atualiza_estoque;
      ws_atualiza_contabi_old      := :old.atualiza_contabi;
      ws_atualiza_contabi_new      := :new.atualiza_contabi;
      ws_contab_cardex_old         := :old.contab_cardex;
      ws_contab_cardex_new         := :new.contab_cardex;
      ws_trans_cancelamento_old    := :old.trans_cancelamento;
      ws_trans_cancelamento_new    := :new.trans_cancelamento;
      ws_inside_pis_confins_old    := :old.inside_pis_confins;
      ws_inside_pis_confins_new    := :new.inside_pis_confins;
      ws_exige_centro_custo_old    := :old.exige_centro_custo;
      ws_exige_centro_custo_new    := :new.exige_centro_custo;
      ws_cod_agrup_trans_old       := :old.cod_agrup_trans;
      ws_cod_agrup_trans_new       := :new.cod_agrup_trans;
      ws_atualiza_estq_terceiro_old := :old.atualiza_estq_terceiro;
      ws_atualiza_estq_terceiro_new := :new.atualiza_estq_terceiro;

   elsif DELETING then

      ws_tipo_ocorr                := 'D';
      ws_codigo_transacao_old      := :old.codigo_transacao;
      ws_codigo_transacao_new      := 0;
      ws_descricao_old             := :old.descricao;
      ws_descricao_new             := null;
      ws_tipo_transacao_old        := :old.tipo_transacao;
      ws_tipo_transacao_new        := null;
      ws_entrada_saida_old         := :old.entrada_saida;
      ws_entrada_saida_new         := null;
      ws_transac_entrada_old       := :old.transac_entrada;
      ws_transac_entrada_new       := 0;
      ws_calcula_preco_old         := :old.calcula_preco;
      ws_calcula_preco_new         := 0;
      ws_altera_custo_old          := :old.altera_custo;
      ws_altera_custo_new          := 0;
      ws_atualiza_estoque_old      := :old.atualiza_estoque;
      ws_atualiza_estoque_new      := 0;
      ws_atualiza_contabi_old      := :old.atualiza_contabi;
      ws_atualiza_contabi_new      := 0;
      ws_contab_cardex_old         := :old.contab_cardex;
      ws_contab_cardex_new         := 0;
      ws_trans_cancelamento_old    := :old.trans_cancelamento;
      ws_trans_cancelamento_new    := 0;
      ws_inside_pis_confins_old    := :old.inside_pis_confins;
      ws_inside_pis_confins_new    := 0;
      ws_exige_centro_custo_old    := :old.exige_centro_custo;
      ws_exige_centro_custo_new    := null;
      ws_cod_agrup_trans_old       := :old.cod_agrup_trans;
      ws_cod_agrup_trans_new       := 0;
      ws_atualiza_estq_terceiro_old := :old.atualiza_estq_terceiro;
      ws_atualiza_estq_terceiro_new := 0;

   end if;

   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   ws_nome_programa := inter_fn_nome_programa(ws_sid);  
      
   INSERT INTO estq_005_hist
      ( aplicacao,                     tipo_ocorr,
        data_ocorr,
        usuario_rede,                  maquina_rede,
        nome_programa,                 usuario_systextil,
        codigo_transacao_old,          codigo_transacao_new,
        descricao_old,                 descricao_new,
        tipo_transacao_old,            tipo_transacao_new,
        entrada_saida_old,             entrada_saida_new,
        transac_entrada_old,           transac_entrada_new,
        calcula_preco_old,             calcula_preco_new,
        altera_custo_old,              altera_custo_new,
        atualiza_estoque_old,          atualiza_estoque_new,
        atualiza_contabi_old,          atualiza_contabi_new,
        contab_cardex_old,             contab_cardex_new,
        trans_cancelamento_old,        trans_cancelamento_new,
        inside_pis_confins_old,        inside_pis_confins_new,
        exige_centro_custo_old,        exige_centro_custo_new,
        cod_agrup_trans_old,           cod_agrup_trans_new,
        atualiza_estq_terceiro_old,    atualiza_estq_terceiro_new
      )
   VALUES
      ( ws_aplicativo,                 ws_tipo_ocorr,
        sysdate,
        ws_usuario_rede,               ws_maquina_rede,
        ws_nome_programa,              ws_usuario_systextil,
        ws_codigo_transacao_old,       ws_codigo_transacao_new,
        ws_descricao_old,              ws_descricao_new,
        ws_tipo_transacao_old,         ws_tipo_transacao_new,
        ws_entrada_saida_old,          ws_entrada_saida_new,
        ws_transac_entrada_old,        ws_transac_entrada_new,
        ws_calcula_preco_old,          ws_calcula_preco_new,
        ws_altera_custo_old,           ws_altera_custo_new,
        ws_atualiza_estoque_old,       ws_atualiza_estoque_new,
        ws_atualiza_contabi_old,       ws_atualiza_contabi_new,
        ws_contab_cardex_old,          ws_contab_cardex_new,
        ws_trans_cancelamento_old,     ws_trans_cancelamento_new,
        ws_inside_pis_confins_old,     ws_inside_pis_confins_new,
        ws_exige_centro_custo_old,     ws_exige_centro_custo_new,
        ws_cod_agrup_trans_old,        ws_cod_agrup_trans_new,
        ws_atualiza_estq_terceiro_old, ws_atualiza_estq_terceiro_new
      );

end inter_tr_estq_005_hist;
-- ALTER TRIGGER "INTER_TR_ESTQ_005_HIST" ENABLE
 

/

exec inter_pr_recompile;

