create or replace procedure inter_pr_saldo_fatu_070_tr (
   p_codigo_empresa    in fatu_070.codigo_empresa%type,
   p_cli_dup_cgc_cli9  in fatu_070.cli_dup_cgc_cli9%type,
   p_cli_dup_cgc_cli4  in fatu_070.cli_dup_cgc_cli4%type,
   p_cli_dup_cgc_cli2  in fatu_070.cli_dup_cgc_cli2%type,
   p_tipo_titulo       in fatu_070.tipo_titulo%type,
   p_num_duplicata     in fatu_070.num_duplicata%type,
   p_seq_duplicatas    in fatu_070.seq_duplicatas%type)

is   
   v_data_pagamento date;
   v_data_credito   date;

begin
   select max(data_pagamento), max(data_credito)
   into   v_data_pagamento,    v_data_credito
   from fatu_075
   where nr_titul_codempr           = p_codigo_empresa
   and   nr_titul_cli_dup_cgc_cli9  = p_cli_dup_cgc_cli9
   and   nr_titul_cli_dup_cgc_cli4  = p_cli_dup_cgc_cli4
   and   nr_titul_cli_dup_cgc_cli2  = p_cli_dup_cgc_cli2
   and   nr_titul_cod_tit           = p_tipo_titulo
   and   nr_titul_num_dup           = p_num_duplicata
   and   nr_titul_seq_dup           = p_seq_duplicatas;

   update fatu_070
   set data_ult_movim_pagto   = v_data_pagamento,
       data_ult_movim_credito = v_data_credito
   where codigo_empresa   = p_codigo_empresa
   and   cli_dup_cgc_cli9 = p_cli_dup_cgc_cli9
   and   cli_dup_cgc_cli4 = p_cli_dup_cgc_cli4
   and   cli_dup_cgc_cli2 = p_cli_dup_cgc_cli2
   and   tipo_titulo      = p_tipo_titulo
   and   num_duplicata    = p_num_duplicata
   and   seq_duplicatas   = p_seq_duplicatas;
end inter_pr_saldo_fatu_070_tr;
/
/* versao: 1 */
