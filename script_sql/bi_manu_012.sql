CREATE OR REPLACE trigger "BI_MANU_012"  
  before insert on "MANU_012"              
  for each row 
begin  
  if :NEW."ID" is null then
    select "MANU_012_SEQ".nextval into :NEW."ID" from dual;
  end if;
end;
/   
