
  CREATE OR REPLACE PROCEDURE "INTER_PR_PCPB_030_PLAN" (
p_ordem_producao    number,
p_nivel_produto     varchar2,
p_grupo_produto     varchar2,
p_subgrupo_produto  varchar2,
p_item_produto      varchar2,
p_alternativa       number,
p_numero_destino    number,
p_saldo_prog        number)
is
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   v_total_necessario        number;
   v_qtde_update             number;
   v_count_reg               number;
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   for reg_tmrp_630 in (select tmrp_630.ordem_planejamento,          tmrp_630.pedido_venda,
                               tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                               tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                               tmrp_625.alternativa_produto_origem,
                               tmrp_630.area_producao,               tmrp_630.ordem_prod_compra,
                               tmrp_625.seq_produto,                 tmrp_625.seq_produto_origem
                        from tmrp_630, tmrp_625
                        where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
                          and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
                          and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
                          and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
                          and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
                          and tmrp_625.item_produto_origem        = tmrp_630.item_produto
                          and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto
                          and tmrp_625.nivel_produto              = p_nivel_produto
                          and tmrp_625.grupo_produto              = p_grupo_produto
                          and tmrp_625.subgrupo_produto           = p_subgrupo_produto
                          and tmrp_625.item_produto               = p_item_produto
                          and tmrp_625.alternativa_produto        = p_alternativa
                          and tmrp_630.ordem_prod_compra          = p_numero_destino
                          and tmrp_630.area_producao              = decode(p_nivel_produto,'2',1,4)
                        group by  tmrp_630.ordem_planejamento,          tmrp_630.pedido_venda,
                                  tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                  tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                  tmrp_625.alternativa_produto_origem,
                                  tmrp_630.area_producao,               tmrp_630.ordem_prod_compra,
                                  tmrp_625.seq_produto,                 tmrp_625.seq_produto_origem)
   loop
      begin
         insert into tmrp_615 (
            ordem_prod,                               area_producao,
            nivel,                                    grupo,
            subgrupo,                                 item,
            alternativa,                              ordem_planejamento,
            pedido_venda,                             nome_programa,
            nr_solicitacao,                           tipo_registro,
            cgc_cliente9,
            nivel_prod,                               grupo_prod,
            subgrupo_prod,                            item_prod,
            alternativa_prod,
            seq_registro,                             sequencia
         ) values (
            reg_tmrp_630.ordem_prod_compra,           reg_tmrp_630.area_producao,
            reg_tmrp_630.nivel_produto_origem,        reg_tmrp_630.grupo_produto_origem,
            reg_tmrp_630.subgrupo_produto_origem,     reg_tmrp_630.item_produto_origem,
            reg_tmrp_630.alternativa_produto_origem,  reg_tmrp_630.ordem_planejamento,
            reg_tmrp_630.pedido_venda,                'trigger_planejamento',
            888,                                      888,
            ws_sid,
            p_nivel_produto,                          p_grupo_produto,
            p_subgrupo_produto,                       p_item_produto,
            p_alternativa,
            reg_tmrp_630.seq_produto,                 reg_tmrp_630.seq_produto_origem
         );
      exception when others then
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end loop;  -- reg_tmrp_630

   begin
      select nvl(sum(tmrp_625.qtde_reserva_planejada),0)
      into v_total_necessario
      from tmrp_630, tmrp_625
      where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
        and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
        and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
        and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
        and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
        and tmrp_625.item_produto_origem        = tmrp_630.item_produto
        and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto
        and tmrp_625.nivel_produto              = p_nivel_produto
        and tmrp_625.grupo_produto              = p_grupo_produto
        and tmrp_625.subgrupo_produto           = p_subgrupo_produto
        and tmrp_625.item_produto               = p_item_produto
        and tmrp_625.alternativa_produto        = p_alternativa
        and tmrp_630.ordem_prod_compra          = p_numero_destino
        and tmrp_630.area_producao              = decode(p_nivel_produto,'2',1,4);
--        and tmrp_625.qtde_reserva_programada - tmrp_625.qtde_areceber_programada > 0;
   exception when others then
      v_total_necessario := 0.00;
   end;

   for reg_ordens_planej in (select tmrp_630.ordem_planejamento,
                                    tmrp_630.pedido_venda,
                                    nvl(sum(tmrp_625.qtde_reserva_planejada),0) qtde_necessaria
                             from tmrp_630, tmrp_625
                             where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
                               and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
                               and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
                               and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
                               and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
                               and tmrp_625.item_produto_origem        = tmrp_630.item_produto
                               and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto
                               and tmrp_625.nivel_produto              = p_nivel_produto
                               and tmrp_625.grupo_produto              = p_grupo_produto
                               and tmrp_625.subgrupo_produto           = p_subgrupo_produto
                               and tmrp_625.item_produto               = p_item_produto
                               and tmrp_625.alternativa_produto        = p_alternativa
                               and tmrp_630.ordem_prod_compra          = p_numero_destino
                               and tmrp_630.area_producao              = decode(p_nivel_produto,'2',1,4)
                               and tmrp_625.qtde_reserva_programada - tmrp_625.qtde_areceber_programada > 0
                             group by tmrp_630.ordem_planejamento, tmrp_630.pedido_venda)
   loop
      -- Esclarecimentos:
      --   Toma o total necessario pelo planejamento e obtem o percentual relativo de cada item.
      --   Depois extrai eset percentual da quantidade sendo programada.
      -- Ex.:
      --   Tenho uma necessidade de 200, dividida em 10 itens de 20 (10% para cada um).
      --   Ao programar 300, sei que tenho que atualizar o primeiro item com 30 (10% de 300).
      if v_total_necessario > 0
      then
         v_qtde_update := (((reg_ordens_planej.qtde_necessaria * 100) / v_total_necessario) * p_saldo_prog) / 100;
      end if;

      begin
         select count(1)
         into   v_count_reg
         from tmrp_630
         where tmrp_630.ordem_planejamento   = reg_ordens_planej.ordem_planejamento
           and tmrp_630.pedido_venda         = reg_ordens_planej.pedido_venda
           and tmrp_630.ordem_prod_compra    = p_ordem_producao
           and tmrp_630.area_producao        = decode(p_nivel_produto,'2',2,7)
           and tmrp_630.nivel_produto        = p_nivel_produto
           and tmrp_630.grupo_produto        = p_grupo_produto
           and tmrp_630.subgrupo_produto     = p_subgrupo_produto
           and tmrp_630.item_produto         = p_item_produto
           and tmrp_630.alternativa_produto  = p_alternativa;
      exception when others then
         v_count_reg := 0;
      end;

      if v_count_reg > 0
      then
         begin
            update tmrp_630
               set tmrp_630.programado_comprado = round(tmrp_630.programado_comprado + v_qtde_update,3)
            where tmrp_630.ordem_planejamento   = reg_ordens_planej.ordem_planejamento
              and tmrp_630.pedido_venda         = reg_ordens_planej.pedido_venda
              and tmrp_630.ordem_prod_compra    = p_ordem_producao
              and tmrp_630.area_producao        = decode(p_nivel_produto,'2',2,7)
              and tmrp_630.nivel_produto        = p_nivel_produto
              and tmrp_630.grupo_produto        = p_grupo_produto
              and tmrp_630.subgrupo_produto     = p_subgrupo_produto
              and tmrp_630.item_produto         = p_item_produto
              and tmrp_630.alternativa_produto  = p_alternativa;
         exception when others then
            -- ATENCAO! Nao atualizou {0}. Mensagem do banco de dados: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630(344)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      else
         begin
            insert into tmrp_630 (
               ordem_planejamento,                   area_producao,                   ordem_prod_compra,
               nivel_produto,                        grupo_produto,                   subgrupo_produto,
               item_produto,                         pedido_venda,                    programado_comprado,
               alternativa_produto
            ) values (
               reg_ordens_planej.ordem_planejamento, decode(p_nivel_produto,'2',2,7), p_ordem_producao,
               p_nivel_produto,                      p_grupo_produto,                 p_subgrupo_produto,
               p_item_produto,                       reg_ordens_planej.pedido_venda,  round(v_qtde_update,3),
               p_alternativa);
         exception when others then
            -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end loop; -- reg_ordens_planej
end inter_pr_pcpb_030_plan;

 

/

exec inter_pr_recompile;

