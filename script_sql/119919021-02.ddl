alter table i_obrf_015 add flag_rateado number(1);
alter table i_obrf_015 modify flag_rateado default 0;

alter table i_obrf_015 add sequencia_original number(9);
alter table i_obrf_015 modify sequencia_original default 0;

alter table i_obrf_015 add qtde_original number(9);
alter table i_obrf_015 modify qtde_original default 0;
