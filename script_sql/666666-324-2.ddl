create table oper_795 (
       company_cad    varchar2(15) default 0 not null,
       cod_marca      number(3) default 0 not null
);

alter table oper_795 add constraint PK_OPER_795 primary key (company_cad, cod_marca);
