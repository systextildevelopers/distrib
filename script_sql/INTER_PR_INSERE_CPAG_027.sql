CREATE OR REPLACE PROCEDURE INTER_PR_INSERE_CPAG_027
   (p_banco in number, p_conta in number, p_data_credito in date, p_val_lancar in number)

 is

   v_saldo_atual number;
   v_saldo_ins number :=0;
   ok boolean := false;
   

begin
   begin    
       select cpag_027.valor_saldo
       into v_saldo_atual
       from cpag_027
       where cpag_027.banco = p_banco
           and cpag_027.conta_corrente = p_conta
           and cpag_027.data_saldo <= p_data_credito
           and rownum = 1
         order by cpag_027.data_saldo desc;
       exception when no_data_found then
          v_saldo_atual := 0;    
         
   end;
   v_saldo_ins := v_saldo_atual + p_val_lancar;

   begin

      insert into cpag_027(
         banco,                 conta_corrente,
         data_saldo,         valor_saldo
      )values(
         p_banco,             p_conta,
         p_data_credito,   v_saldo_ins
      );
    
      ok := true;   

      EXCEPTION
      when others THEN null;
   end;
      if  ok = false then
         update cpag_027
         set valor_saldo =  valor_saldo + p_val_lancar
         where banco = p_banco
            and conta_corrente = p_conta
            and data_saldo >= p_data_credito;
      end if;
end INTER_PR_INSERE_CPAG_027;

/
exec inter_pr_recompile;   
 
