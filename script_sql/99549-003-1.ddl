alter table pedi_051
add (banco_envio_cobr number(1) default 0,
    variacao_correspondente number(3) default 0,
    conta_correspondente number(9) default 0,
    digito_cc_correspondente varchar2(1) default '',
    agencia_correspondente number(4) default 0,
    carteira_correspondente number(3) default 0,
    agencia_cob_correspondente number(4) default 0
);

comment on column pedi_051.banco_envio_cobr   is 'Indica se o layout de remessa do cobrança escritural será do banco do atributo ou correspondente.';
comment on column pedi_051.variacao_correspondente is 'Variação do banco correspondente (que irá no arquivo de remessa).';   
comment on column pedi_051.conta_correspondente is 'Conta corrente que irá no arquivo de remessa.';
comment on column pedi_051.digito_cc_correspondente is 'Dígito verificador da conta corrente que irá no arquivo de remessa.';
comment on column pedi_051.agencia_correspondente is 'Código da agência que irá no arquivo de remessa.';
comment on column pedi_051.carteira_correspondente is 'Código da carteira da cobrança que irá no arquivo de remessa.';
comment on column pedi_051.agencia_cob_correspondente is 'Código da agência de cobrança que irá no arquivo de remessa.';

exec inter_pr_recompile;



        
