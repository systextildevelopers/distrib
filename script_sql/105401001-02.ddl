create table obrf_164 (
	id_nfe         			number(9) 			default 0,
	tipo_pagamento 			varchar2(2)    		default '',
	valor_pagamento 		number(13,2) 		default 0.00,
	valor_troco     		number(13,2) 		default 0.00,
	bandeira_cartao      	varchar2(2)    		default '',
	cnpj9_card 				varchar2(9) 		default '',
	cnpj4_card 				varchar2(4) 		default '',
	cnpj2_card 				varchar2(2)  		default '',
	tipo_integracao  		number(1)    		default 0,
	num_autoriza_card       varchar2(20) 		default '',
	a_vista_a_prazo 		varchar2(1) 		default ''
);

alter table obrf_164
	add CONSTRAINT obrf_164_PK PRIMARY KEY (id_nfe);

create synonym systextilrpt.obrf_164 for obrf_164;
comment on table obrf_164 is 'Informações de pagamento sobre NF-e';
comment on column obrf_164.tipo_pagamento is 	'Id da forma de pagamento informada pela receita';
comment on column obrf_164.valor_pagamento is 	'Valor do pagamento ';
comment on column obrf_164.valor_troco is 		'Valor do troco';
comment on column obrf_164.bandeira_cartao is 	'Bandeira da operadora de cartão de crédito e/ou débito';
comment on column obrf_164.cnpj9_card is 		'CNPJ da credenciadora de cartão de crédito e/ou débito';
comment on column obrf_164.tipo_integracao is 	'Tipo de integração para pagamento';
comment on column obrf_164.num_autoriza_card is 'Número de autorização da operação cartão de crédito e/ou débito';
comment on column obrf_164.a_vista_a_prazo is 'Indica se o pagamento é a vista ou a prazo';
