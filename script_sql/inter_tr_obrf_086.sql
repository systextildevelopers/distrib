
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_086" 
   before insert or
          update of qtde_ent on obrf_086 for each row

declare
   Pragma Autonomous_Transaction;

   qtde_areceber      number;
   qtde_entradas      number;
   qtde_entradas_aux  number;

   estagio_corte      number;
   tipo_servico       number;
   situacao_ordem     number;
   estagio_servico    number;

   sinal_reg_atu      varchar2(1);
   nivel_os           varchar(2);
   v_executa_trigger  number;
   encontrou_controle number;
   
   rpt_os_benef       varchar2(15);
   ordem_servico      number;
   seq_ordem_servico  number;
   valor_servico_os   number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   -- Conforme a forma de trabalho da empresa, este controle de entrada de pecas nao deve ser feito.
   -- este controle nao pode ser feito, quando a empresa controla os tag's de peca com defeito pelo
   -- codigo de barras, ou seja, ele da entrada do tag com defeito e depois da entrada do tag novamente
   -- pela tela de leitura otica.
   encontrou_controle := 0;

   begin
      select count(*)
      into encontrou_controle
      from fatu_505
      where fatu_505.rpt_nota_saida = 'obrf_nfs012';
   end;
   
   -- Encontra layout da ordem de servir�o para ver se � DIMATEX.
   -- Se encontrar, ir� mais a frente validar se o valor do servi�o est� zerado, se tiver, ir� barrar o lan�amento.
   rpt_os_benef := '';
   
   begin
      select fatu_500.rpt_ordem_servic_benef
      into rpt_os_benef
      from fatu_500
      where fatu_500.rpt_ordem_servic_benef = 'ob_oimos180_ab' -- Layout da DIMATEX.
        and ROWNUM = 1;
      
      exception when no_data_found
      then rpt_os_benef := '';
   end;

   if v_executa_trigger = 0 and encontrou_controle = 0
   then
      if inserting or updating
      then
         -- Somente a entrada para o tipo de servico do corte,
         -- pode aceitar quantidade a maior de entrada para a Ordem de Servico.
         begin
            select empr_001.estagio_corte
            into estagio_corte
            from empr_001;
         end;

         -- Carrega o codigo do servico da Ordem de Servico para buscar o estagio
         -- e comparar o estagio do tipo de servico com o estagio do corte.
         -- Isto porque, o estagio do corte e o unico que pode aceitar entrada
         -- a maior do que a quantidade a receber prevista.
         begin
            select obrf_080.codigo_servico,  obrf_080.situacao_ordem
            into   tipo_servico,             situacao_ordem
            from obrf_080
            where obrf_080.numero_ordem = :new.ORD086_NUMORD;
         end;

         begin
            select obrf_070.codigo_estagio
            into estagio_servico
            from obrf_070
            where obrf_070.codigo_terceiro = tipo_servico;
         end;

         begin
            select obrf_081.numero_ordem, obrf_081.sequencia, obrf_081.qtde_areceber, obrf_081.prodord_nivel99, obrf_081.valor_servico
            into   ordem_servico,         seq_ordem_servico,  qtde_areceber,          nivel_os,                 valor_servico_os
            from obrf_081
            where obrf_081.numero_ordem = :new.ORD086_NUMORD
              and obrf_081.sequencia    = :new.ORD086_SEQORD;

            exception when no_data_found
            then
               ordem_servico     := 0;
               seq_ordem_servico := 0;
               qtde_areceber     := 0.00;
               nivel_os          := '0';
               valor_servico_os  := 0.00;
         end;
      end if;

      if nivel_os = '1'
      then
         -- Quando estiver inserindo a producao, primeiro verifica se a ordem de servico nao esta com situacao 4 - Baixa total.
         --
         --if inserting and situacao_ordem > 3
         --then
         --   raise_application_error(-20000,chr(10)||chr(10)||'ATENCAO! Nao pode dar entrada de quantidade produzida quando a situacao da Ordem de Servico estiver como baixa total. '||chr(10));
         --end if;

         -- Neste momento, e analisada a quantidade da entrada com a quantidade a receber.
         -- Se a quantidade de entrada, for maior que a quantidade a receber, nao pode aceitar
         -- esta entrada, somente aceita a entrada a maior se o estagio de entrada for o corte.

         if estagio_corte <> estagio_servico
         then
            -- Verifica se a entrada atual e do tipo "+". Se for soma ela ao total de "+" para ver se esta tentando dar entrada
            -- maior que o areceber
            begin
               select nvl(obrf_088.sinal,'')
               into   sinal_reg_atu
               from obrf_088
               where obrf_088.tipo_entrada = :new.tipo_entrada;

               exception when no_data_found
               then sinal_reg_atu := '';
            end;

            if sinal_reg_atu = '+'
            then
               begin
                  select sum(obrf_086.qtde_ent)
                  into   qtde_entradas_aux
                  from obrf_086, obrf_088
                  where obrf_086.ord086_numord = :new.ORD086_NUMORD
                    and obrf_086.ord086_seqord = :new.ORD086_SEQORD
                    and obrf_086.tipo_entrada  = obrf_088.tipo_entrada
                    and obrf_088.sinal         = '+';
                  exception when no_data_found
                  then qtde_entradas_aux := 0.00;

                  if sql%notfound
                  or qtde_entradas_aux is null
                  then
                     qtde_entradas_aux := 0.00;
                  end if;

               end;

               qtde_entradas := 0;

               if inserting
               then
                  qtde_entradas := qtde_entradas_aux + :new.qtde_ent;
               else
                  qtde_entradas := :new.qtde_ent - qtde_entradas_aux;
               end if;

               if qtde_entradas > qtde_areceber
               then
                  raise_application_error(-20000,chr(10)||chr(10)||'ATENCAO! Nao pode dar entrada de quantidade a maior do que a quantidade a receber para a Ordem de Servico. '||chr(10)||'Qtde.a receber: '||to_char(qtde_areceber)||chr(10)||'Qtde.entradas.: '||to_char(qtde_entradas)||chr(10)||chr(10));
               end if;
            end if;
         end if;
      end if;
      
      if nivel_os = '2' and rpt_os_benef = 'ob_oimos180_ab' and valor_servico_os = 0.00
      then
         raise_application_error(-20000,'ATENCAO! Nao pode dar entrada para produtos da ordem de servico que estejam com valor do serviço igual a zero. '||'Ordem Servico: '||to_char(ordem_servico)||' Sequencia: '||to_char(seq_ordem_servico));
      end if;
   end if;
end inter_tr_obrf_086;

-- ALTER TRIGGER "INTER_TR_OBRF_086" ENABLE
 

/

exec inter_pr_recompile;

