
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_615_LOG" 
after insert or delete or update
on tmrp_615
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);                           

 if inserting
 then
    begin

        insert into tmrp_615_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           pedido_venda_OLD,   /*8*/
           pedido_venda_NEW,   /*9*/
           sequencia_projeto_OLD,   /*10*/
           sequencia_projeto_NEW,   /*11*/
           nivel_OLD,   /*12*/
           nivel_NEW,   /*13*/
           grupo_OLD,   /*14*/
           grupo_NEW,   /*15*/
           subgrupo_OLD,   /*16*/
           subgrupo_NEW,   /*17*/
           item_OLD,   /*18*/
           item_NEW,   /*19*/
           nome_cliente_OLD,   /*20*/
           nome_cliente_NEW,   /*21*/
           qtde_requisitada_OLD,   /*22*/
           qtde_requisitada_NEW,   /*23*/
           qtde_pedido_OLD,   /*24*/
           qtde_pedido_NEW,   /*25*/
           valor_unitario_OLD,   /*26*/
           valor_unitario_NEW,   /*27*/
           valor_total_OLD,   /*28*/
           valor_total_NEW,   /*29*/
           perc_perda_OLD,   /*30*/
           perc_perda_NEW,   /*31*/
           tabela_preco_OLD,   /*32*/
           tabela_preco_NEW    /*33*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.pedido_venda,  /*8*/
           :new.pedido_venda, /*9*/
           :old.sequencia_projeto,  /*10*/
           :new.sequencia_projeto, /*11*/
           :old.nivel,  /*12*/
           :new.nivel, /*13*/
           :old.grupo,  /*14*/
           :new.grupo, /*15*/
           :old.subgrupo,  /*16*/
           :new.subgrupo, /*17*/
           :old.item,  /*18*/
           :new.item, /*19*/
           :old.nome_cliente,  /*20*/
           :new.nome_cliente, /*21*/
           :old.qtde_requisitada,  /*22*/
           :new.qtde_requisitada, /*23*/
           :old.qtde_pedido,  /*24*/
           :new.qtde_pedido, /*25*/
           :old.valor_unitario,  /*26*/
           :new.valor_unitario, /*27*/
           :old.valor_total,  /*28*/
           :new.valor_total, /*29*/
           :old.perc_perda,  /*30*/
           :new.perc_perda, /*31*/
           :old.tabela_preco,  /*32*/
           :new.tabela_preco  /*33*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into tmrp_615_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           pedido_venda_OLD, /*8*/
           pedido_venda_NEW, /*9*/
           sequencia_projeto_OLD, /*10*/
           sequencia_projeto_NEW, /*11*/
           nivel_OLD, /*12*/
           nivel_NEW, /*13*/
           grupo_OLD, /*14*/
           grupo_NEW, /*15*/
           subgrupo_OLD, /*16*/
           subgrupo_NEW, /*17*/
           item_OLD, /*18*/
           item_NEW, /*19*/
           nome_cliente_OLD, /*20*/
           nome_cliente_NEW, /*21*/
           qtde_requisitada_OLD, /*22*/
           qtde_requisitada_NEW, /*23*/
           qtde_pedido_OLD, /*24*/
           qtde_pedido_NEW, /*25*/
           valor_unitario_OLD, /*26*/
           valor_unitario_NEW, /*27*/
           valor_total_OLD, /*28*/
           valor_total_NEW, /*29*/
           perc_perda_OLD, /*30*/
           perc_perda_NEW, /*31*/
           tabela_preco_OLD, /*32*/
           tabela_preco_NEW  /*33*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.pedido_venda,  /*8*/
           :new.pedido_venda, /*9*/
           :old.sequencia_projeto,  /*10*/
           :new.sequencia_projeto, /*11*/
           :old.nivel,  /*12*/
           :new.nivel, /*13*/
           :old.grupo,  /*14*/
           :new.grupo, /*15*/
           :old.subgrupo,  /*16*/
           :new.subgrupo, /*17*/
           :old.item,  /*18*/
           :new.item, /*19*/
           :old.nome_cliente,  /*20*/
           :new.nome_cliente, /*21*/
           :old.qtde_requisitada,  /*22*/
           :new.qtde_requisitada, /*23*/
           :old.qtde_pedido,  /*24*/
           :new.qtde_pedido, /*25*/
           :old.valor_unitario,  /*26*/
           :new.valor_unitario, /*27*/
           :old.valor_total,  /*28*/
           :new.valor_total, /*29*/
           :old.perc_perda,  /*30*/
           :new.perc_perda, /*31*/
           :old.tabela_preco,  /*32*/
           :new.tabela_preco  /*33*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into tmrp_615_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           pedido_venda_OLD, /*8*/
           pedido_venda_NEW, /*9*/
           sequencia_projeto_OLD, /*10*/
           sequencia_projeto_NEW, /*11*/
           nivel_OLD, /*12*/
           nivel_NEW, /*13*/
           grupo_OLD, /*14*/
           grupo_NEW, /*15*/
           subgrupo_OLD, /*16*/
           subgrupo_NEW, /*17*/
           item_OLD, /*18*/
           item_NEW, /*19*/
           nome_cliente_OLD, /*20*/
           nome_cliente_NEW, /*21*/
           qtde_requisitada_OLD, /*22*/
           qtde_requisitada_NEW, /*23*/
           qtde_pedido_OLD, /*24*/
           qtde_pedido_NEW, /*25*/
           valor_unitario_OLD, /*26*/
           valor_unitario_NEW, /*27*/
           valor_total_OLD, /*28*/
           valor_total_NEW, /*29*/
           perc_perda_OLD, /*30*/
           perc_perda_NEW, /*31*/
           tabela_preco_OLD, /*32*/
           tabela_preco_NEW /*33*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.pedido_venda,  /*8*/
           :new.pedido_venda, /*9*/
           :old.sequencia_projeto,  /*10*/
           :new.sequencia_projeto, /*11*/
           :old.nivel,  /*12*/
           :new.nivel, /*13*/
           :old.grupo,  /*14*/
           :new.grupo, /*15*/
           :old.subgrupo,  /*16*/
           :new.subgrupo, /*17*/
           :old.item,  /*18*/
           :new.item, /*19*/
           :old.nome_cliente,  /*20*/
           :new.nome_cliente, /*21*/
           :old.qtde_requisitada,  /*22*/
           :new.qtde_requisitada, /*23*/
           :old.qtde_pedido,  /*24*/
           :new.qtde_pedido, /*25*/
           :old.valor_unitario,  /*26*/
           :new.valor_unitario, /*27*/
           :old.valor_total,  /*28*/
           :new.valor_total, /*29*/
           :old.perc_perda,  /*30*/
           :new.perc_perda, /*31*/
           :old.tabela_preco,  /*32*/
           :new.tabela_preco  /*33*/
         );
    end;
 end if;
end inter_tr_tmrp_615_log;

-- ALTER TRIGGER "INTER_TR_TMRP_615_LOG" ENABLE
 

/

exec inter_pr_recompile;

