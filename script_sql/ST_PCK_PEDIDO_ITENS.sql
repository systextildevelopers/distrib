create or replace PACKAGE ST_PCK_PEDIDO_ITENS AS
    TYPE t_dados_pedi_110 IS TABLE OF pedi_110%ROWTYPE;
    TYPE t_dados_pedi_110_saldo IS TABLE OF pedi_110%ROWTYPE;

    FUNCTION get_itens_pedido(p_pedido_venda NUMBER, p_msg_erro IN OUT VARCHAR2) RETURN t_dados_pedi_110;

    FUNCTION get_itens_pedido_saldo(p_pedido_venda NUMBER, p_msg_erro IN OUT VARCHAR2) RETURN t_dados_pedi_110_saldo;

    FUNCTION get_itens_pedido_saldo_by_seq(p_pedido_venda NUMBER, p_cod_proforma VARCHAR2,  p_msg_erro IN OUT VARCHAR2) RETURN t_dados_pedi_110_saldo; 
END;
