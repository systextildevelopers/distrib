create table inte_750 (
  forma_pagamento number(2) default 0,
  portador number(3) default 0,
  historico number(4) default 0,
  conta number(10) default 0
);

alter table inte_750 add constraint pk_inte750 primary key (forma_pagamento);

exec inter_pr_recompile;
/
