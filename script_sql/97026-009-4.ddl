  Create table obrf_742 (
    COD_EMPRESA      number(03)    default 0    not null ,
    MES              number(02)    default 0    not null ,
    ANO              number(04)    default 0    not null ,
    UF               varchar2(2)   default ' '  not null,
    COD_AJ_APUR      varchar2(08)  default ' '  not null,
    SEQ_LANCTO       number(9)     default 0    not null,
    NUM_DA           varchar2(100) default ' ',
    NUM_PROC         varchar2(15)  default ' ',
    IND_PROC         number(1)     default 0 ,
    PROC             varchar2(200) default ' ',
    TXT_COMPL        varchar2(200) default ' ' ,
    VLR_PROC       number(13,2)  default 0.00);
    
  alter table obrf_742
  add constraint PK_OBRF_742 primary key (COD_EMPRESA, MES, ANO,UF, COD_AJ_APUR, SEQ_LANCTO );
  
  comment on column obrf_742.COD_EMPRESA is 'Codigo de empresa para registro E312 filho 311';
  comment on column obrf_742.MES is 'Mes para registro E312 filho 311';  
  comment on column obrf_742.ANO is 'ano para registro E312 filho 311';  
  comment on column obrf_742.UF is 'Estado para registro E312 filho 311';  
  comment on column obrf_742.COD_AJ_APUR is 'Codigo de registro E312 filho 311';  
  comment on column obrf_742.SEQ_LANCTO is 'Sequencia para lancamento do registro E312 filho 311';  
  
  comment on column obrf_742.NUM_DA      is 'N�mero do documento de arrecada��o estadual, se houver';
  comment on column obrf_742.NUM_PROC   is 'N�mero do processo ao qual o ajuste est� vinculado, se houver';
  comment on column obrf_742.IND_PROC   is 'Indicador da origem do processo 0- Sefaz,1- Justi�a Federal,2- Justi�a Estadual,9- Outros';
  comment on column obrf_742.PROC       is 'Descri��o resumida do processo que embasou o lancamento';
  comment on column obrf_742.TXT_COMPL  is 'Complemento de descricao do processo';
  comment on column obrf_742.VLR_PROC  is 'VALOR DO PROCESSO, para registo E312 este campo nao existe ';
   
  exec inter_pr_recompile;
