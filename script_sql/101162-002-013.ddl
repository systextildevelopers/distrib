declare tmpInt number;

cursor parametro_c is select codigo_empresa, tipo_beneficio_fiscal from fatu_500;

begin
  for reg_parametro in parametro_c
  loop
    select nvl(count(*),0) into tmpInt
    from fatu_500
    where fatu_500.codigo_empresa = reg_parametro.codigo_empresa;

    if(tmpInt > 0)
    then
      begin
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'contab.tipoBeneficioFiscal', reg_parametro.tipo_beneficio_fiscal);
      end;
    end if;

  end loop;
  commit;
end;
/

exec inter_pr_recompile;
