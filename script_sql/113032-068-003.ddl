insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f890', 'Entrada de Rolos por Notas F. de Entrada',0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f890', ' ' ,1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Entrada de Rolos por Notas F. de Entrada'
where hdoc_036.codigo_programa = 'obrf_f890'
  and hdoc_036.locale          = 'es_ES';

exec inter_pr_recompile;
