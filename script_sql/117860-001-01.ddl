alter table cpag_026 
modify sequencia_lcto number(4) default 0;

alter table cpag_026_log
modify sequencia_lcto_old number(4) default 0;

alter table cpag_026_log
modify sequencia_lcto_new number(4) default 0;
