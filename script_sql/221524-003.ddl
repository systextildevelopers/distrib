create table obrf_064
(
CODIGO_EMPRESA                 NUMBER(3)     default 0,                                                                                                                                                
MES_REFERENCIA                 NUMBER(2)     default 0,     
ANO_REFERENCIA                 NUMBER(4)     default 0,                                                              
II_TIPO_DECLARACAO             NUMBER(1)     default 1,  
TIPO_REGISTRO                  VARCHAR2(2)   default '',
QUADRO                         VARCHAR2(2)   default '',
VLR_IRPJ_EX_ANT                NUMBER(17,2)  default 0,
VLR_CONTRIB_FIA                NUMBER(17,2)  default 0,
TRANS_CONTRIB_FIA              NUMBER(17,2)  default 0,
TOT_CONTRIB_TRANS_FIA          NUMBER(17,2)  default 0,
CONTRIB_FEI                    NUMBER(17,2)  default 0,
TRANS_CONTRIB_FEI              NUMBER(17,2)  default 0,
TOT_CONTRIB_TRANS_FEI          NUMBER(17,2)  default 0
);

ALTER TABLE obrf_064
ADD CONSTRAINT OBRF_064_PK PRIMARY KEY (CODIGO_EMPRESA,MES_REFERENCIA,ANO_REFERENCIA,II_TIPO_DECLARACAO,TIPO_REGISTRO,QUADRO);


COMMENT ON COLUMN obrf_064.CODIGO_EMPRESA IS 'código da empresa';  
COMMENT ON COLUMN obrf_064.MES_REFERENCIA IS 'mes de referência';                                                                                                                                            
COMMENT ON COLUMN obrf_064.ANO_REFERENCIA IS 'ano de referência';    
