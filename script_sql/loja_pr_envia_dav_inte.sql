
  CREATE OR REPLACE PROCEDURE "LOJA_PR_ENVIA_DAV_INTE" (fCODEXT IN number,
                                                  fCODSER IN varchar2,
                                                  fDOCNFP IN varchar2,

                                                  fDATAPR IN varchar2,
                                                  fCODFIL IN basi_010.subgru_estrutura%type,
                                                  fTIPPED IN varchar2,

                                                  fCLIEXT IN pedi_010.nome_cliente%type,
                                                  fVLRTOT IN loja_210.valor_desc_total_item%type,
                                                  fVLRPAR IN number,
                                                  pVLRDES IN number,
                                                  pQTDPAR IN number,
                                                  pFILFAT IN varchar2,
                                                  pNUMFAT IN number,
                                                  pFILENT IN varchar2,
                                                  pNUMENT IN number,
                                                  pFILCOB IN varchar2,
                                                  pNUMCOB IN number,
                                                  pDATVAL IN date,
                                                  pDATPRI IN date,
                                                  pAPEVEN IN varchar2,
                                                  pPLANO  IN varchar2,
                                                  p_descpre IN varchar2,
                                                  pRESERV IN varchar2,
                                                  pEMIDES IN varchar2,
                                                  pPESLIQ IN number,
                                                  pPESBRU IN number,
                                                  pVLRSEG IN number,
                                                  pVLRFRE IN number,
                                                  pSINAL  IN number,
                                                  pOBSERV IN varchar2,
                                                  pNUMBLO IN varchar2,
                                                  pOBSORC IN varchar2,
                                                  pDATSEG IN date,
                                                  pAPROVA IN varchar2,
                                                  pAPRCLI IN varchar2,
                                                  pPLACAS IN varchar2,
                                                  pQTD_KM IN number,
                                                  pCODSEQ IN number,
                                                  p_cnpj9 IN number,
                                                  p_cnpj4 IN number,
                                                  p_cnpj2 IN number,
                                                  pAssina IN varchar2,
                                                  p_ret_numped out varchar2,
                                                  p_msg_erro out varchar2) is

  pvar  number;
BEGIN

  pvar := pvar;

end loja_pr_envia_dav_inte;

 

/

exec inter_pr_recompile;

