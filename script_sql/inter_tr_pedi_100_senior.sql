
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_100_SENIOR" 
after insert or
      update
of cod_ped_cliente

on pedi_100
for each row

-- Finalidade: Consistir cadastro de funcionarios
-- Autor.....: Edson Pio
-- Data......: 17/02/09
--
-- Historicos de alteracoes na trigger
--
-- Data    Autor    Observacoes
--
declare
   w_data_admissao     efic_050.data_admissao%type;
   w_sit_funcionario   efic_050.sit_funcionario%type;
   w_cod_empresa       fatu_500.codigo_empresa%type;

begin
   if (inserting or updating) and :new.cod_funcionario > 0
   then
      BEGIN
         select fatu_500.codigo_empresa
         into w_cod_empresa
         from fatu_500
         where fatu_500.cgc_9 = :new.cli_ped_cgc_cli9
           and fatu_500.cgc_4 = :new.cli_ped_cgc_cli4
           and fatu_500.cgc_2 = :new.cli_ped_cgc_cli2
           and rownum = 1;
      EXCEPTION
      when no_data_found then
         w_cod_empresa := 0;
      END;

      if w_cod_empresa > 0
      then
         BEGIN
            select efic_050.data_admissao, efic_050.sit_funcionario
            into   w_data_admissao,        w_sit_funcionario
            from efic_050
               where efic_050.cod_empresa     = w_cod_empresa
                 and efic_050.cod_funcionario = :new.cod_funcionario;
         EXCEPTION
         when no_data_found then
            raise_application_error(-20101,' Funcionario nao encontrado para uma venda interna.' || Chr(10) ||
            'Cod. Funcionario: ' || to_char(substr(trim(:new.cod_ped_cliente),4,15),'000000') || Chr(10) ||
            'Cod. Empresa: ' || to_char(substr(trim(:new.cod_ped_cliente),1,3),'000'));
         END;

         if sysdate - w_data_admissao < 90
         then
            raise_application_error(-20101,'Nao e possivel digitar pedido de venda para Funcionario com menos 90 dias de empresa '  || Chr(10) ||
            'Data de admissao: ' || to_char(w_data_admissao,'DD/MM/YYYY'));
         end if;

         if w_sit_funcionario <> 1
         then
            raise_application_error(-20101,'Nao e possivel digitar pedido de venda para Funcionario com situacao: ' ||
            to_char(w_sit_funcionario,'0') || ' - Nao ativo.');
         end if;
      end if;
   end if;
end;
-- ALTER TRIGGER "INTER_TR_PEDI_100_SENIOR" DISABLE
 

/

exec inter_pr_recompile;

