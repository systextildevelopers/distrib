alter table pedi_611
add ( opcao_desconto_item number(1)   default 1,
      desconto_item1      number(6,2) default 0.00,
      desconto_item2      number(6,2) default 0.00,
      desconto_item3      number(6,2) default 0.00,
      cod_local           number(3)   default 0
);

--Add comments to the columns 
comment on column pedi_611.opcao_desconto_item is '1 - copia descontos do pedido original, 2 - zera descontos para o pedido destino e 3 � informa novos descontos para o item';
comment on column pedi_611.cod_local   is 'local embarque';

/
exec inter_pr_recompile;
