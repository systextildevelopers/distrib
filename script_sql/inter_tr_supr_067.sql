
  CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_067" 
before insert or
       update of item_req_nivel99, item_req_grupo, item_req_subgrupo, item_req_item
  on supr_067
  for each row

declare
   v_executa_trigger   number;
   v_codigo_empresa    number;

begin
   -- INICIO - Logica implementada para SS 66676/001
   -- (Nao permitir a insercao de item inativo de acordo com a comfiguracao de empresa)
   if inserting
   then
      if :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting
      or updating
      then
         begin
            select supr_065.codigo_empresa
            into v_codigo_empresa
            from supr_065
            where supr_065.num_requisicao = :new.num_requisicao;
         end;

         begin
            inter_pr_entr_doc_fiscal_item (v_codigo_empresa,
                                           :new.item_req_nivel99,
                                           :new.item_req_grupo,
                                           :new.item_req_subgrupo,
                                           :new.item_req_item);

         end;
      end if;
   end if;
end inter_tr_supr_067;

-- ALTER TRIGGER "INTER_TR_SUPR_067" ENABLE
 

/

exec inter_pr_recompile;

