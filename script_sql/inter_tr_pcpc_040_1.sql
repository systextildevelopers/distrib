
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_040_1" 
   before insert
   on pcpc_040
   for each row

declare
   t_enderecao_automatico        mqop_005.endereco_automatico%type;
   t_roteiro_peca                pcpc_020.roteiro_peca%type;
   t_alternativa_peca            pcpc_020.alternativa_peca%type;
   v_total_estq_450              number;

   v_sub_ler                     mqop_050.subgru_estrutura%type := '###';
   v_item_ler                    mqop_050.item_estrutura%type   := '######';
   v_nr_registro                 number;

   v_executa_trigger             number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger = 0
   then
      if inserting
      then
         -- Verifica se o estagio de que esta sendo lido, esta configurado para enderecar
         begin
            select nvl(mqop_005.endereco_automatico,'N')
            into t_enderecao_automatico
            from mqop_005
            where mqop_005.codigo_estagio = :new.codigo_estagio;
         exception when no_data_found then
               t_enderecao_automatico := 'N';
         end;

         if t_enderecao_automatico = 'S'
         then
            begin
               select nvl(pcpc_020.roteiro_peca,0), nvl(pcpc_020.alternativa_peca,0)
               into t_roteiro_peca,                 t_alternativa_peca
               from pcpc_020
               where pcpc_020.ordem_producao = :new.ordem_producao;
            exception when no_data_found then
                  t_roteiro_peca     := 0;
                  t_alternativa_peca := 0;
            end;

            begin
               v_sub_ler  := :new.proconf_subgrupo;
               v_item_ler := :new.proconf_item;
            end;

            begin
               select count(*)
               into v_nr_registro
               from mqop_050
               where mqop_050.nivel_estrutura  = :new.proconf_nivel99
                 and mqop_050.grupo_estrutura  = :new.proconf_grupo
                 and mqop_050.subgru_estrutura = v_sub_ler
                 and mqop_050.item_estrutura   = v_item_ler
                 and mqop_050.numero_alternati = t_alternativa_peca
                 and mqop_050.numero_roteiro   = t_roteiro_peca;
               if v_nr_registro = 0
               then
                  v_item_ler := '000000';
                  begin
                     select count(*)
                     into v_nr_registro
                     from mqop_050
                     where mqop_050.nivel_estrutura  = :new.proconf_nivel99
                       and mqop_050.grupo_estrutura  = :new.proconf_grupo
                       and mqop_050.subgru_estrutura = v_sub_ler
                       and mqop_050.item_estrutura   = v_item_ler
                       and mqop_050.numero_alternati = t_alternativa_peca
                       and mqop_050.numero_roteiro   = t_roteiro_peca;
                     if v_nr_registro = 0
                     then
                        v_sub_ler  := '000';
                        v_item_ler := :new.proconf_item;
                        begin
                           select count(*)
                           into v_nr_registro
                           from mqop_050
                           where mqop_050.nivel_estrutura  = :new.proconf_nivel99
                             and mqop_050.grupo_estrutura  = :new.proconf_grupo
                             and mqop_050.subgru_estrutura = v_sub_ler
                             and mqop_050.item_estrutura   = v_item_ler
                             and mqop_050.numero_alternati = t_alternativa_peca
                             and mqop_050.numero_roteiro   = t_roteiro_peca;
                           if v_nr_registro = 0
                           then
                              v_item_ler := '000000';
                           end if;
                        end;
                     end if;
                  end;
               end if;
            end;

            for reg_mqop_050 in (select mqop_040.grupo_maquinas,    mqop_040.sub_maquina,
                                        mqop_050.centro_custo,      sum(mqop_050.minutos) minutos
                                 from mqop_050, mqop_040
                                 where mqop_050.nivel_estrutura  = :new.proconf_nivel99
                                   and mqop_050.grupo_estrutura  = :new.proconf_grupo
                                   and mqop_050.subgru_estrutura = v_sub_ler
                                   and mqop_050.item_estrutura   = v_item_ler
                                   and mqop_050.numero_alternati = t_alternativa_peca
                                   and mqop_050.numero_roteiro   = t_roteiro_peca
                                   and mqop_050.codigo_estagio   = :new.codigo_estagio
                                   and mqop_040.codigo_operacao  = mqop_050.codigo_operacao
                                   and mqop_040.pede_produto     = 2
                                 group by mqop_040.grupo_maquinas, mqop_040.sub_maquina,
                                          mqop_050.centro_custo)
                                 /* mqop_040.pede_produto     = 2
                                    CONFORME SS 42654
                                    SOLICITADO PARA QUE AS MAQUINAS QUE PEDEM PRODUTO SEJAM
                                    RETIRADAS DA SELECAO DAS MAQUINAS. */
            loop
               select nvl(count(1),0)
               into v_total_estq_450
               from estq_450
               where estq_450.ordem_producao = :new.ordem_producao
                 and estq_450.estagio        = :new.codigo_estagio;

               if v_total_estq_450 = 0
               then
                  INSERT INTO estq_450 (
                     ORDEM_PRODUCAO,                         ESTAGIO,
                     PERIODO_PRODUCAO,                       GRUPO_MAQUINA,
                     SUBGRUPO_MAQUINA,                       REFERENCIA,
                     AREA_PRODUCAO,                          CENTRO_CUSTO,
                     MINUTOS_MAQUINA
                  ) VALUES (
                     :new.ordem_producao,                    :new.codigo_estagio,
                     :new.periodo_producao,                  reg_mqop_050.grupo_maquinas,
                     reg_mqop_050.sub_maquina,               :new.proconf_grupo,
                     1,                                      reg_mqop_050.centro_custo,
                     reg_mqop_050.minutos
                  );
               end if;
            end loop;
         end if;
      end if;
   end if;
end inter_tr_pcpc_040_1;

-- ALTER TRIGGER "INTER_TR_PCPC_040_1" ENABLE
 

/

exec inter_pr_recompile;

