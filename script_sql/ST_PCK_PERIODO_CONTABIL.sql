create or replace package ST_PCK_PERIODO_CONTABIL as

    TYPE t_dados_cont_510 IS TABLE OF cont_510%rowtype;  

    function get_dados(p_codigo_empresa number, p_exercicio number, p_periodo number, p_msg_erro in out varchar2) return t_dados_cont_510;

    function get_by_date( p_codigo_empresa number,    p_codigo_exercicio number, 
                          p_data date,                p_msg_erro in out varchar2) return t_dados_cont_510;

    function exist_periodos_posteriores(p_codigo_empresa number, p_exercicio number, p_data date, p_situacao number) return boolean; 

    function exist_periodos_anteriores(p_codigo_empresa number, p_exercicio number, p_data date, p_situacao number) return boolean; 

    function update_situ_periodo(p_codigo_empresa number, p_codigo_exercicio number, p_periodo number, p_new_sit number) return boolean; 

end ST_PCK_PERIODO_CONTABIL;
