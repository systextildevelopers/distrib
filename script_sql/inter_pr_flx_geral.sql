CREATE OR REPLACE PROCEDURE "INTER_PR_FLX_GERAL" (p_cod_usuario          IN NUMBER,
                                                p_codigo_empresa       IN NUMBER,
                                                p_nom_usuario          IN VARCHAR2,
                                                p_data_ini_consulta    IN DATE,
                                                p_data_fim_consulta    IN DATE,
                                                p_verificar_vencidos   IN NUMBER,
                                                p_programa             IN varchar2,
                                                p_des_erro             OUT varchar2,
                                                p_des_venc_erro        OUT varchar2
                                                ) is

-- Finalidade: Gerar tabelas oper_flx - Fluxo de caixa
-- Autor.....: Edson Pio
-- Data......: 20/10/10

   TYPE datas is table of basi_260.data_calendario%type index by binary_integer;

   w_he_dia_util_finan_ini basi_260.dia_util_finan%type;
   w_data_util_ini         basi_260.data_calendario%type;
   w_he_dia_util_finan_fim basi_260.dia_util_finan%type;
   w_data_util_fim         basi_260.data_calendario%type;
   w_cod_moeda             oper_001.campo_21%type;
   w_acresimo              oper_001.campo_24%type;
   w_inc_exc_port_c        oper_001.campo_79%type;
   w_inc_exc_port_p        oper_001.campo_50%type;
   w_posic1                oper_001.campo_74%type;
   w_posic2                oper_001.campo_75%type;
   w_posic3                oper_001.campo_76%type;
   w_posic4                oper_001.campo_77%type;
   w_posic5                oper_001.campo_78%type;
   w_inc_exc_emp           oper_001.campo_34%type;
   w_cod_emp1              oper_001.campo_29%type;
   w_cod_emp2              oper_001.campo_30%type;
   w_cod_emp3              oper_001.campo_31%type;
   w_cod_emp4              oper_001.campo_32%type;
   w_cod_emp5              oper_001.campo_33%type;
   w_dias_atraso           oper_001.campo_125%type;
   w_inc_exc_posic_r       oper_001.campo_80%type;
   w_inc_exc_posic_p       oper_001.campo_119%type;
   w_posic_a1              oper_001.campo_120%type;
   w_posic_a2              oper_001.campo_121%type;
   w_posic_a3              oper_001.campo_122%type;
   w_posic_a4              oper_001.campo_123%type;
   w_posic_a5              oper_001.campo_124%type;
   w_tp_ordenacao          oper_001.campo_51%type;
   w_consid_exp            oper_001.campo_127%type;
   w_atualiza_autom        oper_001.campo_132%type;
   p_filtro_empresa        oper_001.campo_25%type;
   w_origem_dados_lh_tp    oper_001.campo_23%type;
   w_rec_confir_vencido    oper_001.campo_128%type;
   w_rec_previs_vencido    oper_001.campo_129%type;
   w_cpag_confir_vencido   oper_001.campo_130%type;
   w_cpag_previs_vencido    oper_001.campo_131%type;
   w_parm_previs_venc      number(1);
   w_data                  datas;
   w_sequencia             number;
   w_data_ini_aux          number;
   w_rat_tit_pg            oper_001.campo_133%type;
   w_rat_tit_rc            oper_001.campo_134%type;

   w_erro                  EXCEPTION;
   w_des_erro              VARCHAR2(2000);
   w_des_venc_erro         VARCHAR2(2000);
   w_venc_erro             EXCEPTION;
BEGIN

   w_des_erro := NULL;
   w_data_ini_aux := 0;
   BEGIN
      select    oper_001.campo_21  w_cod_moeda,
                oper_001.campo_24  w_acresimo,
                oper_001.campo_79  w_inc_exc_port_c,
                oper_001.campo_50  w_inc_exc_port_p,
                oper_001.campo_74  w_posic1,
                oper_001.campo_75  w_posic2,
                oper_001.campo_76  w_posic3,
                oper_001.campo_77  w_posic4,
                oper_001.campo_78  w_posic5,
                oper_001.campo_34  w_inc_exc_emp,
                oper_001.campo_29  w_cod_emp1,
                oper_001.campo_30  w_cod_emp2,
                oper_001.campo_31  w_cod_emp3,
                oper_001.campo_32  w_cod_emp4,
                oper_001.campo_33  w_cod_emp5,
                oper_001.campo_125 w_dias_atraso,
                oper_001.campo_80  w_inc_exc_posic_r,
                oper_001.campo_119 w_inc_exc_posic_p,
                oper_001.campo_120 w_posic_a1,
                oper_001.campo_121 w_posic_a2,
                oper_001.campo_122 w_posic_a3,
                oper_001.campo_123 w_posic_a4,
                oper_001.campo_124 w_posic_a5,
                oper_001.campo_51  w_tp_ordenacao,
                oper_001.campo_127 w_consid_exp,
                oper_001.campo_132 w_atualiza_autom,
                oper_001.campo_25  p_filtro_empresa,
                oper_001.campo_23  w_origem_dados_lh_tp,
                oper_001.campo_128 w_rec_confir_vencido,
                oper_001.campo_129 w_rec_previs_vencido,
                oper_001.campo_130 w_cpag_confir_vencido,
                oper_001.campo_131 w_cpag_previs_vencido,
                oper_001.campo_133 w_rat_tit_pg,
                oper_001.campo_134 w_rat_tit_rc

      into      w_cod_moeda,
                w_acresimo,
                w_inc_exc_port_c,
                w_inc_exc_port_p,
                w_posic1,
                w_posic2,
                w_posic3,
                w_posic4,
                w_posic5,
                w_inc_exc_emp,
                w_cod_emp1,
                w_cod_emp2,
                w_cod_emp3,
                w_cod_emp4,
                w_cod_emp5,
                w_dias_atraso,
                w_inc_exc_posic_r,
                w_inc_exc_posic_p,
                w_posic_a1,
                w_posic_a2,
                w_posic_a3,
                w_posic_a4,
                w_posic_a5,
                w_tp_ordenacao,
                w_consid_exp,
                w_atualiza_autom,
                p_filtro_empresa,
                w_origem_dados_lh_tp,
                w_rec_confir_vencido,
                w_rec_previs_vencido,
                w_cpag_confir_vencido,
                w_cpag_previs_vencido,
                w_rat_tit_pg,
                w_rat_tit_rc

      from oper_001
      where oper_001.nome_usuario     = p_nom_usuario
        and oper_001.codigo_empresa   = p_codigo_empresa
        and oper_001.codigo_usuario   = p_cod_usuario
        and oper_001.codigo_relatorio = 'cpag_f812';
   EXCEPTION
      when no_data_found then
         w_des_erro := 'Nao encontrou registro na tabela de parametros - oper_flx (1) ';
   END;

   if p_programa = 'cpag_f825'
   then
      w_origem_dados_lh_tp := 1; /*BUSCA DADOS DO SISTEMA*/
      w_inc_exc_emp := 1;
      w_cod_emp1 := p_codigo_empresa;
      w_cod_emp2 := 9999;
      w_cod_emp3 := 9999;
      w_cod_emp4 := 9999;
      w_cod_emp5 := 9999;
   end if;

   if w_acresimo is null
   then
      w_acresimo := 0;
   end if;


   /* PROCURA DIA UTIL INICIAL */
   BEGIN
      select basi_260.dia_util_finan into w_he_dia_util_finan_ini from basi_260
      where basi_260.data_calendario = (p_data_ini_consulta - w_acresimo);
   EXCEPTION
      when no_data_found then
         w_he_dia_util_finan_ini := 0;
   END;

   if w_he_dia_util_finan_ini = 1 /* nao.util */
   then
      BEGIN
         select data_calendario into w_data_util_ini from basi_260
         where basi_260.data_calendario <= (p_data_ini_consulta - w_acresimo)
           and basi_260.dia_util_finan   = 0
           and rownum                    = 1
         order by basi_260.data_calendario desc;
      EXCEPTION
         when no_data_found then
            w_data_util_ini := null;
      END;
   else
     w_data_util_ini := (p_data_ini_consulta - w_acresimo);
   end if;
   /* PROCURA DIA UTIL FINAL */
   BEGIN
      select basi_260.dia_util_finan into w_he_dia_util_finan_fim from basi_260
      where basi_260.data_calendario = (p_data_fim_consulta - w_acresimo);
   EXCEPTION
      when no_data_found then
         w_he_dia_util_finan_fim := null;
   END;

   if w_he_dia_util_finan_fim is not null
   then
      w_he_dia_util_finan_fim := 1;
   end if;

   if w_he_dia_util_finan_fim = 1
   then
      BEGIN
         select data_calendario into w_data_util_fim from basi_260
         where basi_260.data_calendario <= (p_data_fim_consulta - w_acresimo)
           and basi_260.dia_util_finan   = 0
           and rownum                    = 1
         order by basi_260.data_calendario desc;
      EXCEPTION
         when no_data_found then
            w_data_util_fim := null;
      END;
   else
      w_data_util_fim := (p_data_fim_consulta - w_acresimo);
   end if;


   /*Elimina o registro das tabelas*/

   BEGIN
      delete from oper_flx
      where oper_flx.int_01           = p_cod_usuario
        and oper_flx.int_12           = 1
        and oper_flx.nr_solicitacao   = p_cod_usuario
        and oper_flx.nome_relatorio   = p_programa
        and oper_flx.str_61           = p_nom_usuario;
   EXCEPTION
   WHEN OTHERS THEN
      w_des_erro := 'Erro eliminar tabela oper_flx (1) ' || Chr(10) || SQLERRM;
   END;

   commit;

   -- VERIFICA TITULOS PREVISTOS VENCIDOS, CASO PARAMETRO SEJA IGUAL A 1

   w_parm_previs_venc := 0;

   if w_rec_previs_vencido = 1 and w_cpag_previs_vencido <> 1
   then
      w_parm_previs_venc := 1;
   elsif w_rec_previs_vencido <> 1 and w_cpag_previs_vencido = 1
   then
      w_parm_previs_venc := 2;
   elsif w_rec_previs_vencido = 1 and w_cpag_previs_vencido = 1
   then
      w_parm_previs_venc := 3;
   end if;

   if w_parm_previs_venc > 0 and p_verificar_vencidos = 1
   then

      inter_pr_flx_venc(p_cod_usuario,
                        w_data_util_ini,
                        w_parm_previs_venc,
                        p_filtro_empresa,
                        w_inc_exc_emp,
                        w_inc_exc_posic_r,
                        w_posic1,
                        w_posic2,
                        w_posic3,
                        w_posic4,
                        w_posic5,
                        w_cod_emp1,
                        w_cod_emp2,
                        w_cod_emp3,
                        w_cod_emp4,
                        w_cod_emp5,
                        w_inc_exc_port_c,
                        w_cod_moeda,
                        w_des_venc_erro,
                        w_des_erro);

      IF w_des_venc_erro IS NOT NULL THEN
         RAISE w_venc_erro;
      END IF;

      IF w_des_erro IS NOT NULL THEN
         RAISE w_erro;
      END IF;

   end if;

   FOR calendario in (
     select basi_260.data_calendario, basi_260.numero_semana, rownum
     from basi_260
     where basi_260.data_calendario between w_data_util_ini and w_data_util_fim
     order by basi_260.data_calendario
   )
   LOOP

      w_data_ini_aux := w_data_ini_aux + 1;
      inter_pr_flx_crec (p_cod_usuario,
                         p_nom_usuario,
                         calendario.data_calendario,
                         w_data_util_ini,
                         w_data_util_fim,
                         w_inc_exc_port_p,
                         p_filtro_empresa,
                         w_inc_exc_emp,
                         w_inc_exc_posic_r,
                         w_consid_exp,
                         w_posic1,
                         w_posic2,
                         w_posic3,
                         w_posic4,
                         w_posic5,
                         w_cod_emp1,
                         w_cod_emp2,
                         w_cod_emp3,
                         w_cod_emp4,
                         w_cod_emp5,
                         w_cod_moeda,
                         w_origem_dados_lh_tp,
                         w_rec_confir_vencido,
                         w_rec_previs_vencido,
                         p_programa,
                         w_data_ini_aux,
                         w_des_erro);

       IF  w_des_erro IS NOT NULL THEN
           RAISE w_erro;
       END IF;

      inter_pr_flx_cpag (p_cod_usuario,
                         p_nom_usuario,
                         calendario.data_calendario,
                         p_data_ini_consulta,
                         p_data_fim_consulta,
                         w_inc_exc_port_c,
                         p_filtro_empresa,
                         w_inc_exc_emp,
                         w_inc_exc_posic_r,
                         w_posic1,
                         w_posic2,
                         w_posic3,
                         w_posic4,
                         w_posic5,
                         w_cod_emp1,
                         w_cod_emp2,
                         w_cod_emp3,
                         w_cod_emp4,
                         w_cod_emp5,
                         w_cpag_confir_vencido,
                         w_cpag_previs_vencido,
                         w_origem_dados_lh_tp,
                         p_programa,
                         w_data_ini_aux,
                         w_des_erro);

       IF  w_des_erro IS NOT NULL THEN
           RAISE w_erro;
       END IF;

       w_data(calendario.rownum) := calendario.data_calendario;

   END LOOP;

  IF w_rat_tit_rc = 'S' THEN
     inter_pr_flx_previs (p_cod_usuario,
                         p_nom_usuario,
                         'B',
                         w_des_erro);
  END IF;

  IF  w_des_erro IS NOT NULL THEN
      RAISE w_erro;
  END IF;

  IF w_rat_tit_pg = 'S' THEN
      inter_pr_flx_previs (p_cod_usuario,
                         p_nom_usuario,
                         'C',
                         w_des_erro);
  END IF;


  IF  w_des_erro IS NOT NULL THEN
      RAISE w_erro;
  END IF;

  BEGIN
     select max(oper_flx.sequencia) into w_sequencia
     from oper_flx
     where oper_flx.nome_relatorio = 'cpag_f820'
       and oper_flx.int_01         = p_cod_usuario
       and oper_flx.nr_solicitacao = p_cod_usuario
       and oper_flx.str_61         = p_nom_usuario;
  EXCEPTION
     WHEN NO_DATA_FOUND THEN
        w_sequencia := 0;
  END;


  FOR eventos in (select oper_flx.int_04, oper_flx.str_63, oper_flx.int_01,
                         oper_flx.nr_solicitacao, oper_flx.nome_relatorio,
                         oper_flx.str_61, oper_flx.str_92
                  from oper_flx
                  where oper_flx.nome_relatorio = 'cpag_f820'
                    and oper_flx.str_63 in ('B','C')
                    and oper_flx.int_01         = p_cod_usuario
                    and oper_flx.nr_solicitacao = p_cod_usuario
                    and oper_flx.str_61         = p_nom_usuario
                  group by oper_flx.int_04, oper_flx.str_63, oper_flx.int_01,
                         oper_flx.nr_solicitacao, oper_flx.nome_relatorio,
                         oper_flx.str_61, oper_flx.str_92)
  LOOP
      FOR date_index in 1..w_data.COUNT
      LOOP
         w_sequencia := w_sequencia + 1;
         BEGIN
              INSERT INTO oper_flx
                 (dat_01,                        int_01,
                  int_04,                        str_92,
                  str_63,                        dat_03,

                  dat_02,                        int_12,

                  nr_solicitacao,                nome_relatorio,
                  sequencia,                     data_criacao,
                  str_61)
              VALUES
                 (w_data(date_index),            p_cod_usuario,
                  eventos.int_04,                eventos.str_92,
                  eventos.str_63,                p_data_ini_consulta,

                  p_data_fim_consulta,           1,

                  eventos.nr_solicitacao,        eventos.nome_relatorio,
                  w_sequencia,                   sysdate,
                  eventos.str_61);
         EXCEPTION
         WHEN OTHERS THEN
             p_des_erro := 'Erro na atualizacao da tabela oper_flx - COMPLETAR PERIODOS ' || Chr(10) || SQLERRM;
             RAISE W_ERRO;
         END;
         COMMIT;
      END LOOP;
  END LOOP;

EXCEPTION
   WHEN w_erro THEN
      p_des_erro := 'Erro na procedure inter_pr_flx_geral ' || Chr(10) || w_des_erro;
   WHEN w_venc_erro THEN
      p_des_venc_erro := w_des_venc_erro;
END inter_pr_flx_geral;

/

exec inter_pr_recompile;
