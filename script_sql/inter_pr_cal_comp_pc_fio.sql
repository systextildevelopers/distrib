create or replace procedure inter_pr_cal_comp_pc_fio
(
       p_nivel varchar2
      ,p_grupo varchar2
)
is

-- ####################################################
-- Calculo de composicao de pecas feitas de fio 

v_existe_reg                         number := 0;
v_num_aleatorio                      number;
v_sequencia                          number :=0;
v_output                             varchar2(1) := 'N';
v_alternativa                        number;

v_comp_30_01                         basi_030.composicao_01%TYPE;
v_comp_30_02                         basi_030.composicao_02%TYPE;
v_comp_30_03                         basi_030.composicao_03%TYPE;
v_comp_30_04                         basi_030.composicao_04%TYPE;
v_comp_30_05                         basi_030.composicao_05%TYPE;
v_perc_comp_30_1                     basi_030.perc_composicao1%TYPE;
v_perc_comp_30_2                     basi_030.perc_composicao2%TYPE;
v_perc_comp_30_3                     basi_030.perc_composicao3%TYPE;
v_perc_comp_30_4                     basi_030.perc_composicao4%TYPE;
v_perc_comp_30_5                     basi_030.perc_composicao5%TYPE;

v_comp_490_01                        basi_490.parte_composicao1%TYPE;
v_comp_490_02                        basi_490.parte_composicao2%TYPE;
v_comp_490_03                        basi_490.parte_composicao3%TYPE;
v_comp_490_04                        basi_490.parte_composicao4%TYPE;
v_comp_490_05                        basi_490.parte_composicao5%TYPE;
v_perc_comp_490_1                    basi_490.perc_composicao1%TYPE;
v_perc_comp_490_2                    basi_490.perc_composicao2%TYPE;
v_perc_comp_490_3                    basi_490.perc_composicao3%TYPE;
v_perc_comp_490_4                    basi_490.perc_composicao4%TYPE;
v_perc_comp_490_5                    basi_490.perc_composicao5%TYPE;


-- ####################################################
-- Sub procedure
procedure ler_estrutura
(
     p_nivel_est                 varchar2
    ,p_grupo_est                 varchar2
    ,p_sub_est                   varchar2
    ,p_item_est                  varchar2
    ,p_cor_composicao            varchar2
    ,p_alternativa_est           number

)
is

begin
   -- Ler o basi_050/estrutura do produto.
   for estr in (select *
                  from inter_vi_estrutura vi
                 where vi.nivel_item        = p_nivel_est
                   and vi.grupo_item        = p_grupo_est
                   and vi.sub_item          = p_sub_est
                   and vi.item_item         = p_item_est
                   and vi.alternativa_item  = p_alternativa_est
                 order by vi.nivel_item, vi.grupo_item, vi.sub_item, vi.item_item, vi.alternativa_item, vi.sequencia,
                          vi.nivel_comp, vi.grupo_comp, vi.sub_comp, vi.item_comp
               )
   loop

     if estr.nivel_comp = '1' then
        ler_estrutura(estr.nivel_comp, estr.grupo_comp, estr.sub_comp, estr.item_comp, p_cor_composicao, estr.alternativa_comp);
     end if;
     
     if estr.nivel_comp = '7' then
        begin
            -- basi_030 composicao null (macarrao) desconsiderar          
            for basi030 in (select b30.nivel_estrutura, b30.referencia
                                 , b30.composicao_01, b30.composicao_02, b30.composicao_03, b30.composicao_04, b30.composicao_05
                                 , b30.perc_composicao1, b30.perc_composicao2, b30.perc_composicao3, b30.perc_composicao4, b30.perc_composicao5
                              from basi_030 b30
                             where b30.nivel_estrutura = estr.nivel_comp
                               and b30.referencia = estr.grupo_comp
                               and (b30.composicao_01 is not null and b30.composicao_01 <> ' ')
                           )
            loop
              if v_output = 'S' then dbms_output.put_line('Item: '||estr.nivel_item||'.'||estr.grupo_item||'.'||estr.sub_item||'.'||estr.item_item||' - '||estr.alternativa_item||' - '||estr.sequencia||' - '||
                                     estr.nivel_comp||'.'||estr.grupo_comp||'.'||estr.sub_comp||'.'||estr.item_comp||' - '|| estr.alternativa_comp||' - '||estr.consumo||' - '||
                                     basi030.composicao_01||' - '||basi030.perc_composicao1||' - '||estr.consumo*(basi030.perc_composicao1/100)||' - '||
                                     basi030.composicao_02||' - '||basi030.perc_composicao2||' - '||estr.consumo*(basi030.perc_composicao2/100)||' - '||
                                     basi030.composicao_03||' - '||basi030.perc_composicao3||' - '||estr.consumo*(basi030.perc_composicao3/100)||' - '||
                                     basi030.composicao_04||' - '||basi030.perc_composicao4||' - '||estr.consumo*(basi030.perc_composicao4/100)||' - '||
                                     basi030.composicao_05||' - '||basi030.perc_composicao5||' - '||estr.consumo*(basi030.perc_composicao5/100)
                                   );  end if;
                                   
               if basi030.composicao_01 is not null then                   
                   begin
                       select 1
                         into v_existe_reg
                         from oper_tmp    
                        where oper_tmp.nr_solicitacao = v_num_aleatorio
                          and oper_tmp.nome_relatorio = 'ftec_f710'
                          and oper_tmp.str_02 = p_cor_composicao
                          and oper_tmp.str_01 = basi030.composicao_01;
                       
                       update oper_tmp
                          set flo_01 = flo_01 + (estr.consumo*(basi030.perc_composicao1/100))
                        where oper_tmp.nr_solicitacao = v_num_aleatorio
                          and oper_tmp.nome_relatorio = 'ftec_f710'
                          and oper_tmp.str_02 = p_cor_composicao
                          and oper_tmp.str_01 = basi030.composicao_01; 
                       
                   exception
                     when NO_DATA_FOUND then 
                       
                       v_sequencia := v_sequencia +1;
                     
                       insert into oper_tmp (nr_solicitacao, nome_relatorio, sequencia, data_criacao, str_02, str_01, flo_01, int_01)
                                     values (v_num_aleatorio, 'ftec_f710', v_sequencia, sysdate, p_cor_composicao, basi030.composicao_01, (estr.consumo*(basi030.perc_composicao1/100)), estr.alternativa_comp); 
                   
                   end;
               
               end if;
               
               if basi030.composicao_02 is not null then                   
                   begin
                       select 1
                         into v_existe_reg
                         from oper_tmp    
                        where oper_tmp.nr_solicitacao = v_num_aleatorio
                          and oper_tmp.nome_relatorio = 'ftec_f710'
                          and oper_tmp.str_02 = p_cor_composicao
                          and oper_tmp.str_01 = basi030.composicao_02;
                       
                       update oper_tmp
                          set flo_01 = flo_01 + (estr.consumo*(basi030.perc_composicao2/100))
                        where oper_tmp.nr_solicitacao = v_num_aleatorio
                          and oper_tmp.nome_relatorio = 'ftec_f710'
                          and oper_tmp.str_02 = p_cor_composicao
                          and oper_tmp.str_01 = basi030.composicao_02; 
                       
                   exception
                     when NO_DATA_FOUND then 
                       
                       v_sequencia := v_sequencia +1;
                     
                       insert into oper_tmp (nr_solicitacao, nome_relatorio, sequencia, data_criacao, str_02, str_01, flo_01, int_01)
                                     values (v_num_aleatorio, 'ftec_f710', v_sequencia, sysdate, p_cor_composicao, basi030.composicao_02, (estr.consumo*(basi030.perc_composicao2/100)), estr.alternativa_comp); 
                   
                   end;
               
               end if;
                   
               if basi030.composicao_03 is not null then                   
                   begin
                       select 1
                         into v_existe_reg
                         from oper_tmp    
                        where oper_tmp.nr_solicitacao = v_num_aleatorio
                          and oper_tmp.nome_relatorio = 'ftec_f710'
                          and oper_tmp.str_02 = p_cor_composicao
                          and oper_tmp.str_01 = basi030.composicao_03;
                       
                       update oper_tmp
                          set flo_01 = flo_01 + (estr.consumo*(basi030.perc_composicao3/100))
                        where oper_tmp.nr_solicitacao = v_num_aleatorio
                          and oper_tmp.nome_relatorio = 'ftec_f710'
                          and oper_tmp.str_02 = p_cor_composicao
                          and oper_tmp.str_01 = basi030.composicao_03; 
                       
                   exception
                     when NO_DATA_FOUND then 
                       
                       v_sequencia := v_sequencia +1;
                     
                       insert into oper_tmp (nr_solicitacao, nome_relatorio, sequencia, data_criacao, str_02, str_01, flo_01, int_01)
                                     values (v_num_aleatorio, 'ftec_f710', v_sequencia, sysdate, p_cor_composicao, basi030.composicao_03, (estr.consumo*(basi030.perc_composicao3/100)), estr.alternativa_comp); 
                   
                   end;
               
               end if;  
               
               if basi030.composicao_04 is not null then                   
                   begin
                       select 1
                         into v_existe_reg
                         from oper_tmp    
                        where oper_tmp.nr_solicitacao = v_num_aleatorio
                          and oper_tmp.nome_relatorio = 'ftec_f710'
                          and oper_tmp.str_02 = p_cor_composicao
                          and oper_tmp.str_01 = basi030.composicao_04;
                       
                       update oper_tmp
                          set flo_01 = flo_01 + (estr.consumo*(basi030.perc_composicao4/100))
                        where oper_tmp.nr_solicitacao = v_num_aleatorio
                          and oper_tmp.nome_relatorio = 'ftec_f710'
                          and oper_tmp.str_02 = p_cor_composicao
                          and oper_tmp.str_01 = basi030.composicao_04; 
                       
                   exception
                     when NO_DATA_FOUND then 
                       
                       v_sequencia := v_sequencia +1;
                     
                       insert into oper_tmp (nr_solicitacao, nome_relatorio, sequencia, data_criacao, str_02, str_01, flo_01, int_01)
                                     values (v_num_aleatorio, 'ftec_f710', v_sequencia, sysdate, p_cor_composicao, basi030.composicao_04, (estr.consumo*(basi030.perc_composicao4/100)), estr.alternativa_comp); 
                   
                   end;                                
               
               end if;  

               if basi030.composicao_05 is not null then                   
                   begin
                       select 1
                         into v_existe_reg
                         from oper_tmp    
                        where oper_tmp.nr_solicitacao = v_num_aleatorio
                          and oper_tmp.nome_relatorio = 'ftec_f710'
                          and oper_tmp.str_02 = p_cor_composicao
                          and oper_tmp.str_01 = basi030.composicao_05;
                       
                       update oper_tmp
                          set flo_01 = flo_01 + (estr.consumo*(basi030.perc_composicao5/100))
                        where oper_tmp.nr_solicitacao = v_num_aleatorio
                          and oper_tmp.nome_relatorio = 'ftec_f710'
                          and oper_tmp.str_02 = p_cor_composicao
                          and oper_tmp.str_01 = basi030.composicao_05; 
                       
                   exception
                     when NO_DATA_FOUND then 
                       
                       v_sequencia := v_sequencia +1;
                     
                       insert into oper_tmp (nr_solicitacao, nome_relatorio, sequencia, data_criacao, str_02, str_01, flo_01, int_01)
                                     values (v_num_aleatorio, 'ftec_f710', v_sequencia, sysdate, p_cor_composicao, basi030.composicao_05, (estr.consumo*(basi030.perc_composicao5/100)), estr.alternativa_comp); 
                   
                   end;
               
               end if;                            
               
            end loop; -- basi030
            
        end;
        
     end if;

   end loop;  -- estr

end;

-- ####################################################
-- Rotina principal
begin 
    
    select round(DBMS_RANDOM.VALUE (100000000, 999999999)) 
    into v_num_aleatorio
    from dual; 
    
    if v_output = 'S' then dbms_output.put_line('v_num_aleatorio: '||v_num_aleatorio); end if;
    if v_output = 'S' then dbms_output.put_line(''); end if;
    
    delete basi_490
    where nivel = p_nivel
    and grupo = p_grupo;
    
    delete oper_tmp
    where oper_tmp.nome_relatorio = 'ftec_f710'
    and oper_tmp.data_criacao < sysdate -(1/24)
    ;    

    delete oper_tmp
    where oper_tmp.nome_relatorio = 'ftec_f710'
    and oper_tmp.nr_solicitacao = v_num_aleatorio
    ;
        
    for prd in (select *
                  from basi_010 b10
                 where b10.nivel_estrutura = p_nivel
                   and b10.grupo_estrutura = p_grupo
                   and b10.subgru_estrutura = (select max(subgru_estrutura) from basi_010 
                                                where nivel_estrutura = p_nivel
                                                  and grupo_estrutura = p_grupo)
                 order by nivel_estrutura, grupo_estrutura, subgru_estrutura, item_estrutura
               )
    loop
       ler_estrutura(prd.nivel_estrutura, prd.grupo_estrutura, prd.subgru_estrutura, prd.item_estrutura, prd.item_estrutura, prd.numero_alternati);
    
    end loop; -- prd
    
    for perc in (select t1.nr_solicitacao, t1.nome_relatorio, t1.sequencia, t1.data_criacao, t1.str_02, t1.str_01, t1.flo_01, t1.int_01
                      , sum(t1.flo_01) OVER (PARTITION BY str_02) as ptotal
                      , round(t1.flo_01 / sum(t1.flo_01) OVER (PARTITION BY str_02) * 100,2) as percentual
                   from oper_tmp t1
                  where t1.nome_relatorio = 'ftec_f710'
                    and t1.nr_solicitacao = v_num_aleatorio     
                  order by nr_solicitacao, nome_relatorio, sequencia
                )
    loop
       update oper_tmp
          set oper_tmp.flo_02 = perc.ptotal
            , oper_tmp.flo_03 = perc.percentual
        where oper_tmp.nr_solicitacao = v_num_aleatorio
          and oper_tmp.nome_relatorio = 'ftec_f710'
          and oper_tmp.str_02 = perc.str_02
          and oper_tmp.str_01 = perc.str_01;    
            
    end loop; -- perc
    
    if v_output = 'S' then dbms_output.put_line(''); end if;
    
    for lista in (select rownum as linha
                       , cc.* 
                    from (select fibra_perc, listagg(distinct cor, '#') within group (order by cor) as lista_cor, count(*) as qtd_cores
                            from (select t1.str_02 as cor
                                       , listagg(distinct t1.str_01||'-'|| round(t1.flo_03,2), '#') within group (order by t1.str_01) as fibra_perc
                                    from oper_tmp t1
                                   where t1.nome_relatorio = 'ftec_f710'
                                     and t1.nr_solicitacao = v_num_aleatorio
                                   group by t1.str_02
                                  )
                           group by fibra_perc   
                         ) cc
                   order by qtd_cores desc
                 )
     loop
        -- update basi_030
        if v_output = 'S' then dbms_output.put_line(''); end if;
        if v_output = 'S' then dbms_output.put_line('Lista: '||lista.linha||' - '||lista.fibra_perc||' - '||lista.lista_cor||' - '||lista.qtd_cores); end if;
        
        if lista.linha = 1 then
           if v_output = 'S' then dbms_output.put_line('update basi_030: '|| p_nivel||'.'||p_grupo||' - '||lista.fibra_perc); end if;
           
           for b30 in (select rownum as lin, tmp.nr_solicitacao, tmp.nome_relatorio, tmp.sequencia, tmp.data_criacao, tmp.str_02
                            , tmp.str_01, tmp.int_01, tmp.flo_01, tmp.flo_02, tmp.flo_03
                         from (select t1.nr_solicitacao, t1.nome_relatorio, t1.sequencia, t1.data_criacao
                                    , t1.str_02, t1.str_01, t1.flo_01, t1.flo_02, t1.flo_03, t1.int_01
                                 from oper_tmp t1
                                where t1.nome_relatorio = 'ftec_f710'   
                                  and t1.nr_solicitacao = v_num_aleatorio
                                  and t1.str_02 = nvl(substr(lista.lista_cor, 0, INSTR(lista.lista_cor, '#')-1), lista.lista_cor)
                                order by t1.flo_03 desc, t1.str_01
                               ) tmp
                        where rownum < 6
                      ) 
           loop
               if b30.lin = 1 then
                  v_comp_30_01     := b30.str_01;
                  v_perc_comp_30_1 := b30.flo_03;
               end if;
             
               if b30.lin = 2 then
                  v_comp_30_02     := b30.str_01;
                  v_perc_comp_30_2 := b30.flo_03;
               end if;
               
               if b30.lin = 3 then
                  v_comp_30_03     := b30.str_01;
                  v_perc_comp_30_3 := b30.flo_03;
               end if;
               
               if b30.lin = 4 then
                  v_comp_30_04     := b30.str_01;
                  v_perc_comp_30_4 := b30.flo_03;
               end if;
               
               if b30.lin = 5 then
                  v_comp_30_05     := b30.str_01;
                  v_perc_comp_30_5 := b30.flo_03;
               end if;                                                        
           
           end loop; -- b30 
           
           update basi_030 ba
              set ba.composicao_01 = v_comp_30_01
                , ba.composicao_02 = v_comp_30_02
                , ba.composicao_03 = v_comp_30_03
                , ba.composicao_04 = v_comp_30_04
                , ba.composicao_05 = v_comp_30_05
                , ba.perc_composicao1 = nvl(v_perc_comp_30_1,0)
                , ba.perc_composicao2 = nvl(v_perc_comp_30_2,0)
                , ba.perc_composicao3 = nvl(v_perc_comp_30_3,0)
                , ba.perc_composicao4 = nvl(v_perc_comp_30_4,0)
                , ba.perc_composicao5 = nvl(v_perc_comp_30_5,0)
            where ba.nivel_estrutura = p_nivel
              and ba.referencia = p_grupo           
           ;
           
        else
           -- insert basi_490
           for cor in (select regexp_substr(lista.lista_cor, '[^#]+', 1, level) cor from dual
                       connect by regexp_substr(lista.lista_cor, '[^#]+', 1, level) is not null
                      )
           loop
              for b490 in (select rownum as lin, tmp.nr_solicitacao, tmp.nome_relatorio, tmp.sequencia, tmp.data_criacao
                                , tmp.str_02, tmp.str_01, tmp.int_01, tmp.flo_01, tmp.flo_02, tmp.flo_03
                             from (select t1.nr_solicitacao, t1.nome_relatorio, t1.sequencia, t1.data_criacao
                                        , t1.str_02, t1.str_01, t1.flo_01, t1.flo_02, t1.flo_03, t1.int_01
                                     from oper_tmp t1
                                    where t1.nome_relatorio = 'ftec_f710'   
                                      and t1.nr_solicitacao = v_num_aleatorio
                                      and t1.str_02 = cor.cor
                                    order by t1.flo_03 desc, t1.str_01
                                   ) tmp
                            where rownum < 6
                          ) 
              loop
                  v_alternativa := b490.int_01;
                  
                  if b490.lin = 1 then
                     v_comp_490_01     := b490.str_01;
                     v_perc_comp_490_1 := b490.flo_03;
                  end if;
                
                  if b490.lin = 2 then
                     v_comp_490_02     := b490.str_01;
                     v_perc_comp_490_2 := b490.flo_03;
                  end if;
                   
                  if b490.lin = 3 then
                     v_comp_490_03     := b490.str_01;
                     v_perc_comp_490_3 := b490.flo_03;
                  end if;
                   
                  if b490.lin = 4 then
                     v_comp_490_04     := b490.str_01;
                     v_perc_comp_490_4 := b490.flo_03;
                  end if;
                   
                  if b490.lin = 5 then
                     v_comp_490_05     := b490.str_01;
                     v_perc_comp_490_5 := b490.flo_03;
                  end if;                                                      
               
              end loop; -- b490
              
              if v_output = 'S' then 
                 dbms_output.put_line('insert basi_490: '||p_nivel||'.'||p_grupo||'.'||'000'||'.'||'000000'||' - '||0||' - '||0||' - '||0
                                     ||v_comp_490_01||' - '||v_comp_490_02||' - '||v_comp_490_03||' - '||v_comp_490_04||' - '||v_comp_490_05||' - '
                                     ||v_perc_comp_490_1||' - '||v_perc_comp_490_2||' - '||v_perc_comp_490_3||' - '||v_perc_comp_490_4||' - '||v_perc_comp_490_5); 
              end if;
                                   
              insert into basi_490(nivel, grupo, subgrupo, item, cod_idioma, cod_tipo_tec1, alternativa
                                  ,parte_composicao1, parte_composicao2, parte_composicao3, parte_composicao4, parte_composicao5
                                  ,perc_composicao1, perc_composicao2, perc_composicao3, perc_composicao4, perc_composicao5
                                  )
                           values (p_nivel, p_grupo, '000', cor.cor, 0, 0, v_alternativa
                                  ,v_comp_490_01, v_comp_490_02, v_comp_490_03, v_comp_490_04, v_comp_490_05
                                  ,nvl(v_perc_comp_490_1,0), nvl(v_perc_comp_490_2,0), nvl(v_perc_comp_490_3,0), nvl(v_perc_comp_490_4,0), nvl(v_perc_comp_490_5,0)
                                  );              
           
           end loop; -- cor
           
        end if;
      
     end loop; -- lista
    
    commit;

end inter_pr_cal_comp_pc_fio;
/
exec inter_pr_recompile;
/
