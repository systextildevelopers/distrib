
  CREATE OR REPLACE FUNCTION "INTER_FN_GERA_CALEND_RECURSO" 
  (p_data_inicial          in  date,      p_data_final    in date,
   p_tipo_alocacao         in  number,    p_tipo_recurso  in number,
   p_codigo_recurso        in  varchar2,  p_centro_custo  in number,
   p_qtde_recursos         in  number)
return number
is
  p_resultado number :=0;

begin
   begin
      inter_pr_gera_calend_recurso
         (p_data_inicial,   p_data_final,
          p_tipo_alocacao,  p_tipo_recurso,
          p_codigo_recurso, p_centro_custo,
          p_qtde_recursos,  p_resultado);
      exception
         when others then
            p_resultado := 0;
   end;

   return(p_resultado);

end inter_fn_gera_calend_recurso;



 

/

exec inter_pr_recompile;

