insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('mppm_f001', 'Manutenção de Pedidos de Venda em Massa', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'mppm_f001', 'mppm_menu', 1, 1, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'mppm_f001', 'mppm_menu', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Manutenção de Pedidos de Venda em Massa'
 where hdoc_036.codigo_programa = 'mppm_f001'
   and hdoc_036.locale          = 'es_ES';
commit;
