create table OPER_038 (
   EMPRESA        NUMBER(3)    DEFAULT 0 NOT NULL,
   PROGRAMA       varchar2(10) default '' NOT NULL,
   USUARIO        varchar2(15) default '' NOT NULL
);
 
 
comment on column OPER_038.USUARIO is 'Usuários responsáveis por liberar este programa';
comment on column OPER_038.PROGRAMA is 'Programa supervisonado';
comment on column OPER_038.EMPRESA is 'Empresa que o supervisor pode liberar o programa';
 
alter table OPER_038 add constraint OPER_038_pk primary key (EMPRESA, USUARIO, PROGRAMA);

create synonym systextilrpt.OPER_038 for OPER_038; 

exec inter_pr_recompile;

/
