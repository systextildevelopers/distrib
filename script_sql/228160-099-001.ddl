alter table estq_300 ADD REPOSICAO number(9,0);
alter table estq_310 ADD REPOSICAO number(9,0);

COMMENT ON COLUMN ESTQ_300.REPOSICAO IS 'Numero da reposição dos tecidos da ordem de confecção';
COMMENT ON COLUMN ESTQ_310.REPOSICAO IS 'Numero da reposição dos tecidos da ordem de confecção';
