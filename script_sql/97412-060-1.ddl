ALTER TABLE pcpb_020
ADD nro_pecas NUMBER(5) default 0;

comment on column pcpb_020.nro_pecas is 'Obter a quantidade de pe�as(cones) que devem ser utilizados no tingimento';

commit work;

/
exec inter_pr_recompile;
