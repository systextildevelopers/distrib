
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_052_LOG" 
after insert or delete
or update of cod_empresa, num_nota, cod_serie_nota, cod_mensagem,
	seq_mensagem, ind_local, des_mensag_1, des_mensag_2,
	des_mensag_3, des_mensag_4, des_mensag_5, des_mensag_6,
	des_mensag_7, des_mensag_8, des_mensag_9, des_mensag_10,
	cnpj9, cnpj4, cnpj2, ind_entr_saida,
	des_mensag_11, des_mensag_12
on FATU_052
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);  
        

 if inserting
 then
    begin

        insert into FATU_052_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           COD_EMPRESA_OLD,   /*8*/
           COD_EMPRESA_NEW,   /*9*/
           NUM_NOTA_OLD,   /*10*/
           NUM_NOTA_NEW,   /*11*/
           COD_SERIE_NOTA_OLD,   /*12*/
           COD_SERIE_NOTA_NEW,   /*13*/
           COD_MENSAGEM_OLD,   /*14*/
           COD_MENSAGEM_NEW,   /*15*/
           SEQ_MENSAGEM_OLD,   /*16*/
           SEQ_MENSAGEM_NEW,   /*17*/
           IND_LOCAL_OLD,   /*18*/
           IND_LOCAL_NEW,   /*19*/
           DES_MENSAG_1_OLD,   /*20*/
           DES_MENSAG_1_NEW,   /*21*/
           DES_MENSAG_2_OLD,   /*22*/
           DES_MENSAG_2_NEW,   /*23*/
           DES_MENSAG_3_OLD,   /*24*/
           DES_MENSAG_3_NEW,   /*25*/
           DES_MENSAG_4_OLD,   /*26*/
           DES_MENSAG_4_NEW,   /*27*/
           DES_MENSAG_5_OLD,   /*28*/
           DES_MENSAG_5_NEW,   /*29*/
           DES_MENSAG_6_OLD,   /*30*/
           DES_MENSAG_6_NEW,   /*31*/
           DES_MENSAG_7_OLD,   /*32*/
           DES_MENSAG_7_NEW,   /*33*/
           DES_MENSAG_8_OLD,   /*34*/
           DES_MENSAG_8_NEW,   /*35*/
           DES_MENSAG_9_OLD,   /*36*/
           DES_MENSAG_9_NEW,   /*37*/
           DES_MENSAG_10_OLD,   /*38*/
           DES_MENSAG_10_NEW,   /*39*/
           CNPJ9_OLD,   /*40*/
           CNPJ9_NEW,   /*41*/
           CNPJ4_OLD,   /*42*/
           CNPJ4_NEW,   /*43*/
           CNPJ2_OLD,   /*44*/
           CNPJ2_NEW,   /*45*/
           IND_ENTR_SAIDA_OLD,   /*46*/
           IND_ENTR_SAIDA_NEW    /*47*/
        ) values (
            'I', /*1*/
            sysdate, /*2*/
            sysdate,/*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           0,/*8*/
           :new.COD_EMPRESA, /*9*/
           0,/*10*/
           :new.NUM_NOTA, /*11*/
           '',/*12*/
           :new.COD_SERIE_NOTA, /*13*/
           0,/*14*/
           :new.COD_MENSAGEM, /*15*/
           0,/*16*/
           :new.SEQ_MENSAGEM, /*17*/
           '',/*18*/
           :new.IND_LOCAL, /*19*/
           '',/*20*/
           :new.DES_MENSAG_1, /*21*/
           '',/*22*/
           :new.DES_MENSAG_2, /*23*/
           '',/*24*/
           :new.DES_MENSAG_3, /*25*/
           '',/*26*/
           :new.DES_MENSAG_4, /*27*/
           '',/*28*/
           :new.DES_MENSAG_5, /*29*/
           '',/*30*/
           :new.DES_MENSAG_6, /*31*/
           '',/*32*/
           :new.DES_MENSAG_7, /*33*/
           '',/*34*/
           :new.DES_MENSAG_8, /*35*/
           '',/*36*/
           :new.DES_MENSAG_9, /*37*/
           '',/*38*/
           :new.DES_MENSAG_10, /*39*/
           0,/*40*/
           :new.CNPJ9, /*41*/
           0,/*42*/
           :new.CNPJ4, /*43*/
           0,/*44*/
           :new.CNPJ2, /*45*/
           '',/*46*/
           :new.IND_ENTR_SAIDA /*47*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into FATU_052_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_EMPRESA_OLD, /*8*/
           COD_EMPRESA_NEW, /*9*/
           NUM_NOTA_OLD, /*10*/
           NUM_NOTA_NEW, /*11*/
           COD_SERIE_NOTA_OLD, /*12*/
           COD_SERIE_NOTA_NEW, /*13*/
           COD_MENSAGEM_OLD, /*14*/
           COD_MENSAGEM_NEW, /*15*/
           SEQ_MENSAGEM_OLD, /*16*/
           SEQ_MENSAGEM_NEW, /*17*/
           IND_LOCAL_OLD, /*18*/
           IND_LOCAL_NEW, /*19*/
           DES_MENSAG_1_OLD, /*20*/
           DES_MENSAG_1_NEW, /*21*/
           DES_MENSAG_2_OLD, /*22*/
           DES_MENSAG_2_NEW, /*23*/
           DES_MENSAG_3_OLD, /*24*/
           DES_MENSAG_3_NEW, /*25*/
           DES_MENSAG_4_OLD, /*26*/
           DES_MENSAG_4_NEW, /*27*/
           DES_MENSAG_5_OLD, /*28*/
           DES_MENSAG_5_NEW, /*29*/
           DES_MENSAG_6_OLD, /*30*/
           DES_MENSAG_6_NEW, /*31*/
           DES_MENSAG_7_OLD, /*32*/
           DES_MENSAG_7_NEW, /*33*/
           DES_MENSAG_8_OLD, /*34*/
           DES_MENSAG_8_NEW, /*35*/
           DES_MENSAG_9_OLD, /*36*/
           DES_MENSAG_9_NEW, /*37*/
           DES_MENSAG_10_OLD, /*38*/
           DES_MENSAG_10_NEW, /*39*/
           CNPJ9_OLD, /*40*/
           CNPJ9_NEW, /*41*/
           CNPJ4_OLD, /*42*/
           CNPJ4_NEW, /*43*/
           CNPJ2_OLD, /*44*/
           CNPJ2_NEW, /*45*/
           IND_ENTR_SAIDA_OLD, /*46*/
           IND_ENTR_SAIDA_NEW  /*47*/
        ) values (
            'A', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.COD_EMPRESA,  /*8*/
           :new.COD_EMPRESA, /*9*/
           :old.NUM_NOTA,  /*10*/
           :new.NUM_NOTA, /*11*/
           :old.COD_SERIE_NOTA,  /*12*/
           :new.COD_SERIE_NOTA, /*13*/
           :old.COD_MENSAGEM,  /*14*/
           :new.COD_MENSAGEM, /*15*/
           :old.SEQ_MENSAGEM,  /*16*/
           :new.SEQ_MENSAGEM, /*17*/
           :old.IND_LOCAL,  /*18*/
           :new.IND_LOCAL, /*19*/
           :old.DES_MENSAG_1,  /*20*/
           :new.DES_MENSAG_1, /*21*/
           :old.DES_MENSAG_2,  /*22*/
           :new.DES_MENSAG_2, /*23*/
           :old.DES_MENSAG_3,  /*24*/
           :new.DES_MENSAG_3, /*25*/
           :old.DES_MENSAG_4,  /*26*/
           :new.DES_MENSAG_4, /*27*/
           :old.DES_MENSAG_5,  /*28*/
           :new.DES_MENSAG_5, /*29*/
           :old.DES_MENSAG_6,  /*30*/
           :new.DES_MENSAG_6, /*31*/
           :old.DES_MENSAG_7,  /*32*/
           :new.DES_MENSAG_7, /*33*/
           :old.DES_MENSAG_8,  /*34*/
           :new.DES_MENSAG_8, /*35*/
           :old.DES_MENSAG_9,  /*36*/
           :new.DES_MENSAG_9, /*37*/
           :old.DES_MENSAG_10,  /*38*/
           :new.DES_MENSAG_10, /*39*/
           :old.CNPJ9,  /*40*/
           :new.CNPJ9, /*41*/
           :old.CNPJ4,  /*42*/
           :new.CNPJ4, /*43*/
           :old.CNPJ2,  /*44*/
           :new.CNPJ2, /*45*/
           :old.IND_ENTR_SAIDA,  /*46*/
           :new.IND_ENTR_SAIDA  /*47*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into FATU_052_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_EMPRESA_OLD, /*8*/
           COD_EMPRESA_NEW, /*9*/
           NUM_NOTA_OLD, /*10*/
           NUM_NOTA_NEW, /*11*/
           COD_SERIE_NOTA_OLD, /*12*/
           COD_SERIE_NOTA_NEW, /*13*/
           COD_MENSAGEM_OLD, /*14*/
           COD_MENSAGEM_NEW, /*15*/
           SEQ_MENSAGEM_OLD, /*16*/
           SEQ_MENSAGEM_NEW, /*17*/
           IND_LOCAL_OLD, /*18*/
           IND_LOCAL_NEW, /*19*/
           DES_MENSAG_1_OLD, /*20*/
           DES_MENSAG_1_NEW, /*21*/
           DES_MENSAG_2_OLD, /*22*/
           DES_MENSAG_2_NEW, /*23*/
           DES_MENSAG_3_OLD, /*24*/
           DES_MENSAG_3_NEW, /*25*/
           DES_MENSAG_4_OLD, /*26*/
           DES_MENSAG_4_NEW, /*27*/
           DES_MENSAG_5_OLD, /*28*/
           DES_MENSAG_5_NEW, /*29*/
           DES_MENSAG_6_OLD, /*30*/
           DES_MENSAG_6_NEW, /*31*/
           DES_MENSAG_7_OLD, /*32*/
           DES_MENSAG_7_NEW, /*33*/
           DES_MENSAG_8_OLD, /*34*/
           DES_MENSAG_8_NEW, /*35*/
           DES_MENSAG_9_OLD, /*36*/
           DES_MENSAG_9_NEW, /*37*/
           DES_MENSAG_10_OLD, /*38*/
           DES_MENSAG_10_NEW, /*39*/
           CNPJ9_OLD, /*40*/
           CNPJ9_NEW, /*41*/
           CNPJ4_OLD, /*42*/
           CNPJ4_NEW, /*43*/
           CNPJ2_OLD, /*44*/
           CNPJ2_NEW, /*45*/
           IND_ENTR_SAIDA_OLD, /*46*/
           IND_ENTR_SAIDA_NEW /*47*/
        ) values (
            'D', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede,/*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.COD_EMPRESA, /*8*/
           0, /*9*/
           :old.NUM_NOTA, /*10*/
           0, /*11*/
           :old.COD_SERIE_NOTA, /*12*/
           '', /*13*/
           :old.COD_MENSAGEM, /*14*/
           0, /*15*/
           :old.SEQ_MENSAGEM, /*16*/
           0, /*17*/
           :old.IND_LOCAL, /*18*/
           '', /*19*/
           :old.DES_MENSAG_1, /*20*/
           '', /*21*/
           :old.DES_MENSAG_2, /*22*/
           '', /*23*/
           :old.DES_MENSAG_3, /*24*/
           '', /*25*/
           :old.DES_MENSAG_4, /*26*/
           '', /*27*/
           :old.DES_MENSAG_5, /*28*/
           '', /*29*/
           :old.DES_MENSAG_6, /*30*/
           '', /*31*/
           :old.DES_MENSAG_7, /*32*/
           '', /*33*/
           :old.DES_MENSAG_8, /*34*/
           '', /*35*/
           :old.DES_MENSAG_9, /*36*/
           '', /*37*/
           :old.DES_MENSAG_10, /*38*/
           '', /*39*/
           :old.CNPJ9, /*40*/
           0, /*41*/
           :old.CNPJ4, /*42*/
           0, /*43*/
           :old.CNPJ2, /*44*/
           0, /*45*/
           :old.IND_ENTR_SAIDA, /*46*/
           '' /*47*/
         );
    end;
 end if;
end inter_tr_FATU_052_log;
-- ALTER TRIGGER "INTER_TR_FATU_052_LOG" ENABLE
 

/

exec inter_pr_recompile;

