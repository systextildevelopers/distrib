alter table SPED_CTB_I155
  drop constraint PK_SPED_CTB_I155;
prompt ATENCAO! Ao excluir este index se aparecer a mensagem "ORA-01418: specified index does not exist" favor ignorar.
drop index PK_SPED_CTB_I155;

alter table SPED_CTB_I155
  add constraint PK_SPED_CTB_I155 primary key (COD_EMPRESA, EXERCICIO, COD_PERIODO, COD_CONTA, CENTRO_CUSTO) novalidate;
