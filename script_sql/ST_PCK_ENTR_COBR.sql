create or replace package ST_PCK_ENTR_COBR as

    TYPE t_dados_pedi_150 IS TABLE OF pedi_150%rowtype;
    
    FUNCTION get_dados_end_entr_cobr(p_cgc_9 number, p_cgc_4 number, p_cgc_2 number, p_tipo_end number, p_seq_end number, p_msg_erro in out varchar2) return t_dados_pedi_150;

end;
