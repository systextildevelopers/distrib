
  CREATE OR REPLACE TRIGGER "INTER_TR_FINA_490" 
before insert on fina_490
  for each row





declare
   v_nr_registro number;

begin
   select seq_fina_490.nextval into v_nr_registro from dual;

   :new.cod_aplicacao := v_nr_registro;

end inter_tr_fina_490;

-- ALTER TRIGGER "INTER_TR_FINA_490" ENABLE
 

/

exec inter_pr_recompile;

