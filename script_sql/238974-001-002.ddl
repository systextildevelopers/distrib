-----------------------------------
-- Depósito da Sugestão de Rolos --
-----------------------------------
create table pedi_920 (
  deposito_pedido_venda number(9) not null,
  deposito_sugestao number(9) not null
);

alter table pedi_920 add constraint pk_pedi_920 primary key (deposito_pedido_venda, deposito_sugestao);
