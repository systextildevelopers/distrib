CREATE OR REPLACE VIEW "VW_PROCESSO_ANTECIPADO" ("PEDIDO_VENDA", "DATA_EMISSAO", "DATA_ENTREGA", "CLIENTE", "CONDICAO_PGTO", "QTDE_PEDIDA", "VALOR_PEDIDO", "QTDE_SUGERIDA", "VALOR_SUGERIDO", "SITUACAO_ANTEC", "DATA_VENCIMENTO", "CODIGO_EMPRESA", "COD_CIDADE", "COD_FORMA_PAGTO", "CODIGO_SERVICO_SPH", "DATA_EXPIRACAO", "URL_PGTO") AS 
  select
        PEDIDO_VENDA
        ,DATA_EMISSAO
        ,DATA_ENTREGA
        ,CLIENTE
        ,CONDICAO_PGTO
        ,QTDE_PEDIDA
        ,VALOR_PEDIDO
        ,QTDE_SUGERIDA
        ,VALOR_SUGERIDO
        ,SITUACAO_ANTEC
        ,max(data_vencimento) as data_vencimento
        ,CODIGO_EMPRESA
        ,COD_CIDADE
        ,COD_FORMA_PAGTO -- Adcionado para SPH
        ,CODIGO_SERVICO_SPH -- Adcionado para o SPH
        ,DATA_EXPIRACAO -- adicionado para SPH
        ,URL_PGTO -- adicionado para SPH
from (select distinct
            pedi_100.PEDIDO_VENDA
            ,pedi_100.CODIGO_EMPRESA
            ,pedi_100.DATA_EMIS_VENDA as data_emissao
            ,pedi_100.DATA_ENTR_VENDA as DATA_ENTREGA
            ,LPAD(pedi_100.CLI_PED_CGC_CLI9,9,0) ||'/'|| lpad(pedi_100.CLI_PED_CGC_CLI4,4,0) ||'-'||lpad(pedi_100.CLI_PED_CGC_CLI2, 2,0) ||' - '|| pedi_010.FANTASIA_CLIENTE as cliente
            ,pedi_070.COND_PGT_CLIENTE ||' - '|| pedi_070.DESCR_PG_CLIENTE as condicao_pgto
            ,case when pedi_100.situacao_venda = 9 then pedi_100.QTDE_SALDO_PEDI
            else pedi_100.QTDE_TOTAL_PEDI end
             AS QTDE_PEDIDA
            ,pedi_100.VALOR_SALDO_PEDI as valor_pedido
            ,(select sum(QTDE_SUGERIDA) from pedi_110 where pedido_venda = pedi_100.pedido_venda) as QTDE_SUGERIDA
            ,(select round(sum(round((VALOR_UNITARIO - (VALOR_UNITARIO * PERCENTUAL_DESC / 100.00)), 2) * QTDE_SUGERIDA), 2) from pedi_110 where pedido_venda = pedi_100.pedido_venda) as valor_sugerido
            ,case
                when pedi_117.sit_boleto != 2 and (select count(1) as data_venc from FATU_070 fatu, FATU_504
                                                                    where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA
                                                                    and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9
                                                                    and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4
                                                                    and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2
                                                                    and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO
                                                                    and fatu.PEDIDO_VENDA = pedi_100.pedido_venda
                                                                    and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO
                                                                    and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA
                                                                    and fatu.DATA_PRORROGACAO >= to_date(sysdate)
                                                                    and fatu.SALDO_DUPLICATA > 0 ) = 1 then 'AGUARDANDO PAGAMENTO'
                when pedi_117.sit_boleto != 2 and (select count(1) as data_venc from FATU_070 fatu, FATU_504
                                                                    where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA
                                                                    and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9
                                                                    and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4
                                                                    and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2
                                                                    and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO
                                                                    and fatu.PEDIDO_VENDA = pedi_100.pedido_venda
                                                                    and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO
                                                                    and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA
                                                                    and fatu.DATA_PRORROGACAO < to_date(sysdate)
                                                                    and fatu.SALDO_DUPLICATA > 0) = 1 then 'ANTECIPAÇÃO EM ATRASO'
                when pedi_117.sit_boleto != 2 and (select count(1) as data_venc from FATU_070 fatu, FATU_504
                                                                    where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA
                                                                    and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9
                                                                    and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4
                                                                    and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2
                                                                    and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO
                                                                    and fatu.PEDIDO_VENDA = pedi_100.pedido_venda
                                                                    and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO
                                                                    and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA
                                                                    and fatu.SALDO_DUPLICATA = 0) = 1 then 'ANTECIPAÇÃO RECEBIDA'
                when pedi_117.sit_boleto = 2 then 'ANTECIPAÇÃO CANCELADA'
                when pedi_117.sit_boleto in (0,1) and pedi_100.pedido_venda = pedi_118.pedido_venda   then 'PROCESSO DE ANTECIPAÇÃO REALIZADO'
                --when ((select nvl(sum(QTDE_SUGERIDA),0) from pedi_110 where pedido_venda = pedi_100.pedido_venda) <= 0)  and not exists (select fatu.pedido_venda from fatu_070 fatu where fatu.pedido_venda = pedi_100.pedido_venda) then 'PEDIDO DIGITADO' --and pedi_100.pedido_venda != pedi_118.pedido_venda
                when ((select nvl(sum(QTDE_SUGERIDA),0) from pedi_110 where pedido_venda = pedi_100.pedido_venda) > 0) and pedi_118.pedido_venda is null then 'PEDIDO SUGERIDO'
                else 'PEDIDO DIGITADO'
            end as situacao_antec
            , (select max(fatu.DATA_PRORROGACAO) as data_venc from FATU_070 fatu, FATU_504
                                                                    where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA
                                                                    and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9
                                                                    and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4
                                                                    and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2
                                                                    and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO
                                                                    and fatu.PEDIDO_VENDA = pedi_100.pedido_venda
                                                                    and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO
                                                                    and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA) as data_vencimento
            , pedi_010.cod_cidade
			      , nvl(pedi_100.cod_forma_pagto,0) as COD_FORMA_PAGTO -- Adicionado SPH
            , nvl(loja_010.codigo_servico_sph,0) as codigo_servico_sph -- Adcionado SPH
            , T01.data_expiracao as data_expiracao -- adicionado SPH
            , T01.URL_PGTO
        from  pedi_100
              ,pedi_110
              ,pedi_070 --condição de pagamento
              ,pedi_010 --cliente
              ,pedi_075
              ,pedi_118
              ,pedi_117
              ,pedi_121
              ,loja_010 --adicionado SPH
              ,INTER_VI_URL_PGTO_SPH T01 --adicionado SPH
                where
                pedi_100.COND_PGTO_VENDA =  pedi_070.COND_PGT_CLIENTE
				        and pedi_100.cod_forma_pagto = loja_010.forma_pgto(+) -- Adicionado SPH
                and pedi_100.numero_referencia = t01.numero_referencia(+) -- adicionado SPH
                and pedi_100.pedido_venda = t01.pedido_venda(+) -- adicionado SPH
                and pedi_100.CLI_PED_CGC_CLI9 = t01.CD_CLI_CGC_CLI9 (+) -- adicionado SPH
                and pedi_100.CLI_PED_CGC_CLI4 = t01.CD_CLI_CGC_CLI4 (+) -- adicionado SPH
                and pedi_100.CLI_PED_CGC_CLI2 = t01.CD_CLI_CGC_CLI2 (+) -- adicionado SPH
                and pedi_100.codigo_empresa = t01.codigo_empresa (+) -- adicionado SPH
                and pedi_100.pedido_venda = pedi_110.pedido_venda
                and pedi_100.CLI_PED_CGC_CLI9 = pedi_010.CGC_9 (+)
                and pedi_100.CLI_PED_CGC_CLI4 = pedi_010.CGC_4 (+)
                and pedi_100.CLI_PED_CGC_CLI2 = pedi_010.CGC_2 (+)
                and pedi_100.cond_pgto_venda = pedi_075.condicao_pagto
                and pedi_100.CLI_PED_CGC_CLI9 = pedi_118.CGC9 (+)
                and pedi_100.CLI_PED_CGC_CLI4 = pedi_118.CGC4 (+)
                and pedi_100.CLI_PED_CGC_CLI2 = pedi_118.CGC2 (+)
                and pedi_100.pedido_venda = pedi_118.pedido_venda  (+)
                and pedi_118.CGC9 = pedi_117.CGC9 (+)
                and pedi_118.CGC4 = pedi_117.CGC4 (+)
                and pedi_118.CGC2 = pedi_117.CGC2 (+)
                and pedi_118.SEQUENCIA = pedi_117.SEQUENCIA (+)
                and pedi_118.CGC9 = pedi_121.CGC9 (+)
                and pedi_118.CGC4 = pedi_121.CGC4 (+)
                and pedi_118.CGC2 = pedi_121.CGC2 (+)
                and pedi_118.SEQUENCIA = pedi_121.seq_deposito (+)
                and nvl(pedi_117.sit_boleto,0) != 5
                and pedi_075.vencimento = 0
                and (pedi_075.vencimento = 0 or (loja_010.codigo_servico_sph > 0)) -- condiserar venda prazo quando for SPH
                and pedi_100.cod_cancelamento = 0
                and pedi_110.situacao_fatu_it <> 1
                and pedi_110.cod_cancelamento  = 0
                and pedi_100.SITUACAO_VENDA != 10
                and (pedi_110.QTDE_SUGERIDA > 0
      or (((select count(1) from pedi_118 pedi118
            where pedi118.pedido_venda = pedi_100.pedido_venda) > 0)
        and pedi_100.situacao_venda <> 9))
                )
            where SITUACAO_ANTEC is not null
            group by
            PEDIDO_VENDA
            ,DATA_EMISSAO
            ,DATA_ENTREGA
            ,CLIENTE
            ,CONDICAO_PGTO
            ,QTDE_PEDIDA
            ,VALOR_PEDIDO
            ,QTDE_SUGERIDA
            ,VALOR_SUGERIDO
            ,SITUACAO_ANTEC
            ,CODIGO_EMPRESA
            ,COD_CIDADE
            ,COD_FORMA_PAGTO -- Adcionado para SPH
            ,CODIGO_SERVICO_SPH -- Adcionado para o SPH
            ,DATA_EXPIRACAO -- adicionado para SPH
            ,URL_PGTO -- adicionado para SPH
            order by pedido_venda
/

