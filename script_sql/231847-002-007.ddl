CREATE SEQUENCE   "ESTQ_250_SEQ";
/
CREATE OR REPLACE TRIGGER  "BI_ESTQ_250" 
before insert on estq_250
for each row
begin
  :new.id := estq_250_seq.nextval;
end;

/
ALTER TRIGGER  "BI_ESTQ_250" ENABLE
/
