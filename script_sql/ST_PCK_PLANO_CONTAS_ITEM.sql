create or replace package ST_PCK_PLANO_CONTAS_ITEM as
	TYPE t_dados_cont_535 IS TABLE OF cont_535%rowtype;

	function get_by_cod_plano(p_cod_plano_cta number, p_msg_erro in out varchar2) return t_dados_cont_535;
    
    function get_by_cod_reduzido(p_cod_plano_cta number, p_cod_reduzido number, p_msg_erro in out varchar2) return t_dados_cont_535;

    function get_by_nivel_patr(p_cod_plano_cta number, p_nivel number, p_patr number, p_msg_erro in out varchar2)  return t_dados_cont_535;
		    
    procedure insere_item(  p_cod_plano number, p_conta_contabil varchar2,
                            p_sub_conta number, p_cod_reduzido number, 
                            p_conta_mae number, p_descricao varchar2, 
                            p_descr_sub_conta varchar2, p_exige_sub_conta number,
                            p_nivel number, p_patr_result number,
                            p_tipo_conta number, p_gera_custos number, 
                            p_tipo_orcamento number, p_familia_orcamento number, 
                            p_debito_credito varchar2, p_indicador_natu number, 
                            p_permite_lancamento_manual number, p_quebra_filial number,
                            p_conta_contabil_ref varchar2, p_indic_natu_sped number,
                            p_tp_livro_aux_sped varchar2, p_data_alteracao date, 
                            p_tipo_natureza number, p_msg_erro in out varchar2);
end ST_PCK_PLANO_CONTAS_ITEM;
