
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_615" 
   before insert
   on tmrp_615
   for each row

begin
   if :new.nome_programa != 'pcpb_f041'
   then
      :new.data_criacao := sysdate;
   end if;

end inter_tr_tmrp_615;

-- ALTER TRIGGER "INTER_TR_TMRP_615" ENABLE
 

/

exec inter_pr_recompile;

