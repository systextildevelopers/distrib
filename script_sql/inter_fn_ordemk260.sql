CREATE OR REPLACE FUNCTION inter_fn_ordemk260 (
        p_ordem_producao  NUMBER,
        p_ordem_servico   NUMBER
) RETURN NUMBER

IS

v_tipo_ordemk260     number;

BEGIN
    -- Se o tecido do rolo da ordem de producao for igual ao tecido da ordem de producao
    -- Isto indica que e ordem de reprocesso K260/K265
    -- Se o tecido do rolo nao for igual a da ordem de producao e uma ordem de producao
    -- com tecido de outra ordem, mudando a cor do produto.
    -- v_tipo_ordemk260 = 0 - is uma ordem normal
    -- v_tipo_ordemk260 = 1 - is uma ordem k260/k265
    v_tipo_ordemk260 := 0;
    
    declare
        nro_reg_pcpt_025 number;
    begin
        select nvl(count(*),0)
        into nro_reg_pcpt_025
        from (
               select pcpt_025.pano_ini_nivel99
                    , pcpt_025.pano_ini_grupo
                    , pcpt_025.pano_ini_subgrupo
                    , pcpt_025.pano_ini_item
               from pcpt_025
               where pcpt_025.ordem_producao    = p_ordem_producao
               and   pcpt_025.area_ordem        = 2
               group  by pcpt_025.pano_ini_nivel99
                       , pcpt_025.pano_ini_grupo
                       , pcpt_025.pano_ini_subgrupo
                       , pcpt_025.pano_ini_item
              ) pcpt025Tec;

        -- Só pode ter um tecido preparado
        -- Não ter ordem de serviço
        if   nro_reg_pcpt_025 = 1
        and (p_ordem_servico is null
        or   p_ordem_servico = 0)
        then
            begin
                 select 1
                 into v_tipo_ordemk260
                 from pcpt_025, pcpb_010, pcpb_020
                 where pcpb_020.ordem_producao    = pcpb_010.ordem_producao
                 and   pcpb_010.ordem_producao    = p_ordem_producao
                 and   pcpb_010.tipo_ordem        in (1,2)
                 and   pcpt_025.ordem_producao    = pcpb_010.ordem_producao
                 and   pcpt_025.area_ordem        = 2
                 and   pcpt_025.pano_ini_nivel99  = pcpb_020.pano_sbg_nivel99
                 and   pcpt_025.pano_ini_grupo    = pcpb_020.pano_sbg_grupo
                 and   pcpt_025.pano_ini_subgrupo = pcpb_020.pano_sbg_subgrupo
                 and   pcpt_025.pano_ini_item     = pcpb_020.pano_sbg_item
                 and   rownum                     = 1;
            exception when others then
                 v_tipo_ordemk260 := 0;
            end;
        else
            v_tipo_ordemk260 := 0;
        end if;
    exception when others then
        v_tipo_ordemk260 := 0;
    end;

  return v_tipo_ordemk260;

end inter_fn_ordemk260;
/
