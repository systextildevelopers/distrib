
  CREATE OR REPLACE TRIGGER "INTER_TR_PLAN_MOV_TMRP_640" 
   after insert or delete or update
       of data_cadastro,     data_validade,
          data_embarque_cli, data_embarque_plan,
          cod_cancelamento
   on tmrp_640
   for each row

declare
   v_executa_trigger   number(1);
   v_ordem_planej      number(9);
   v_numero_reserva    number(6);

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
begin
   v_executa_trigger := 1;

   if inserting
   then
      v_numero_reserva := :new.numero_reserva;
   else
      v_numero_reserva := :old.numero_reserva;
   end if;

   begin
      select tmrp_610.ordem_planejamento
      into   v_ordem_planej
      from tmrp_625, tmrp_610
      where   tmrp_625.pedido_reserva           = v_numero_reserva
        and   tmrp_625.ordem_planejamento       = tmrp_610.ordem_planejamento
        and   tmrp_610.situacao_ordem           < 9
        and   tmrp_625.nivel_produto_origem     = '0'
        and   tmrp_625.grupo_produto_origem     = '00000'
        and   tmrp_625.subgrupo_produto_origem  = '000'
        and   tmrp_625.item_produto_origem      = '000000'
        and ((tmrp_625.qtde_areceber_programada > 0
        and exists (select 1 from tmrp_041, pcpc_020, tmrp_630
                                                                where tmrp_041.periodo_producao   = pcpc_020.periodo_producao
                                                                  and tmrp_041.area_producao      = 1
                                                                  and tmrp_041.nr_pedido_ordem    = tmrp_630.ordem_prod_compra
                                                                  and tmrp_041.codigo_estagio     = pcpc_020.ultimo_estagio
                                                                  and tmrp_041.nivel_estrutura    = tmrp_625.nivel_produto
                                                                  and tmrp_041.grupo_estrutura    = tmrp_625.grupo_produto
                                                                  and tmrp_041.subgru_estrutura   = tmrp_625.subgrupo_produto
                                                                  and tmrp_041.item_estrutura     = tmrp_625.item_produto
                                                                  and tmrp_041.qtde_areceber      > 0
                                                                  and pcpc_020.ordem_producao     = tmrp_630.ordem_prod_compra
                                                                  and pcpc_020.cod_cancelamento   = 0
                                                                  and tmrp_630.ordem_planejamento = tmrp_610.ordem_planejamento
                                                                  and tmrp_630.area_producao      = 1)) or
              tmrp_625.qtde_areceber_programada = 0)
        and rownum = 1;
      exception when no_data_found then
            v_executa_trigger := 0;
   end;

   if v_executa_trigger = 1
   then
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

      -- Alteracao do item do pedido, quantidade reservada do item e codigo de cancelamento.
      if updating
      then
   <<produtos_reserva>>
   for reg_produtos in (select tmrp_645.nivel_produto nivel, tmrp_645.grupo_produto grupo, tmrp_645.subgru_produto subgrupo, tmrp_645.item_produto item
                        from tmrp_645
                        where tmrp_645.numero_reserva = v_numero_reserva
                        group by tmrp_645.nivel_produto, tmrp_645.grupo_produto, tmrp_645.subgru_produto, tmrp_645.item_produto)
   loop
            -- Alteracao do produto
             if :old.data_validade  <> :new.data_validade
             then
                inter_pr_insere_motivo_plan(
                     v_ordem_planej,                         v_numero_reserva,
                     reg_produtos.nivel,                     reg_produtos.grupo,
                     reg_produtos.subgrupo,                  reg_produtos.item,
                     '0',                                    '00000',
                     --Pendente                              Pedido
                     0,                                      1,
                     -- PEDIDO DE RESERVA ALTERADO
                    inter_fn_buscar_tag_composta('lb38809', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                    --Data de Validade do pedido de reserva {0}, foi modificado de {1} para {2}
                    inter_fn_buscar_tag_composta('lb38919', to_char(:old.numero_reserva,'000000'),
                                                            :old.data_validade,
                                                            :new.data_validade,
                                                            '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
           end if;

           -- Alteracao da quantidade pedida
            if :old.data_cadastro <> :new.data_cadastro
            then
               inter_pr_insere_motivo_plan(
                     v_ordem_planej,                         v_numero_reserva,
                     reg_produtos.nivel,                     reg_produtos.grupo,
                     reg_produtos.subgrupo,                  reg_produtos.item,
                     '0',                                    '00000',
                     0,                                      1,
                     -- PEDIDO DE RESERVA ALTERADO
                     inter_fn_buscar_tag_composta('lb38809', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                     --Data de Cadastro do pedido de reserva {0}, foi modificado de {1} para {2}
                     inter_fn_buscar_tag_composta('lb38918', to_char(:new.numero_reserva,'000000'),
                                                             to_char(:old.data_cadastro),
                                                             to_char(:new.data_cadastro),
                                                             '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
            end if;

            if :old.data_embarque_cli <> :new.data_embarque_cli
            then
               inter_pr_insere_motivo_plan(
                     v_ordem_planej,                         v_numero_reserva,
                     reg_produtos.nivel,                     reg_produtos.grupo,
                     reg_produtos.subgrupo,                  reg_produtos.item,
                     '0',                                    '00000',
                     --Pendente                              Pedido
                     0,                                      1,
                     -- PEDIDO DE RESERVA ALTERADO
                     inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                     --Data de Embarque do cliente no pedido de reserva {0}, foi modificado de {1} para {2}
                     inter_fn_buscar_tag_composta('lb38920', to_char(:old.numero_reserva,'000000'),
                                                             :old.data_embarque_cli,
                                                             :new.data_embarque_cli,
                                                             '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
            end if;

            if :old.data_embarque_plan <> :new.data_embarque_plan
            then
               inter_pr_insere_motivo_plan(
                     v_ordem_planej,                         v_numero_reserva,
                     reg_produtos.nivel,                     reg_produtos.grupo,
                     reg_produtos.subgrupo,                  reg_produtos.item,
                     '0',                                    '00000',
                     --Pendente                              Pedido
                     0,                                      1,
                     -- PEDIDO DE RESERVA ALTERADO
                     inter_fn_buscar_tag_composta('lb38809', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                     --Data de Embarque do cliente no pedido de reserva {0}, foi modificado de {1} para {2}
                     inter_fn_buscar_tag_composta('lb38921', to_char(:old.numero_reserva,'000000'),
                                                             :old.data_embarque_plan,
                                                             :new.data_embarque_plan,
                                                             '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
            end if;

            if :old.cod_cancelamento <> :new.cod_cancelamento
            then
               inter_pr_insere_motivo_plan(
                     v_ordem_planej,                         v_numero_reserva,
                     reg_produtos.nivel,                     reg_produtos.grupo,
                     reg_produtos.subgrupo,                  reg_produtos.item,
                     '0',                                    '00000',
                     --Pendente                              Pedido
                     0,                                      1,
                     -- PEDIDO DE RESERVA ALTERADO
                     inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                     inter_fn_buscar_tag_composta('lb38923',to_char(:old.numero_reserva),
                                                                    :new.cod_cancelamento,
                                                                    '',
                                                                    '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
            end if;
         end loop;

      end if;

      -- Inclusao de um item do pedido.
      if inserting
      then

         inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_numero_reserva,
                  '',                                     '',
                  '',                                     '',
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- ITEM PEDIDO DE RESERVA CRIADO
                  inter_fn_buscar_tag_composta('lb38865', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                 -- Novo item: {0}.
                 inter_fn_buscar_tag_composta('lb38924',:new.numero_reserva,
                                                        '','','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
      end if;

      -- Eliminacao de um item do pedido.
      if  deleting
      then
         inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_numero_reserva,
                  '',                                     '',
                  '',                                     '',
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE RESERVA ALTERADO
                  inter_fn_buscar_tag_composta('lb38809', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                 -- Item {0}, na sequencia {1}, cancelado ou eliminado.
                 inter_fn_buscar_tag_composta('lb38925',to_char(:old.numero_reserva),
                                                        '','','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
      end if;
   end if;
end inter_tr_plan_mov_tmrp_640;
-- ALTER TRIGGER "INTER_TR_PLAN_MOV_TMRP_640" ENABLE
 

/

exec inter_pr_recompile;

