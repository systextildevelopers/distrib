create table pcpb_812
( ID        		NUMBER(9)      	DEFAULT 0 NOT NULL,
  MENSAGEM       	VARCHAR2(255)	DEFAULT ' ',
  TIPO_PROCESSO		VARCHAR2(2)		DEFAULT ' ',
  DATA_INSERCAO		DATE			DEFAULT sysdate);
  
alter table pcpb_812
   ADD CONSTRAINT PK_PCPB_812 PRIMARY KEY (ID);
   
create synonym systextilrpt.pcpb_812 for pcpb_812;

comment on table pcpb_812 is 'Log do processo de cadastro das OB - pcpb_810 e OT - pcpb_814';

comment on column pcpb_812.MENSAGEM is 'Mensagem de erro ou sucesso na geração da OB';
comment on column pcpb_812.TIPO_PROCESSO is 'Tipo de Processo que deu erro (OB ou OT)';
/
