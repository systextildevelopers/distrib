INSERT INTO EMPR_007 (PARAM,             TIPO,   LABEL,     FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('fiscal.msgAjBeneficioDevolVenda',  1,     'null', 	' ',         null,        0,        null,        null);

COMMIT;

declare tmpInt number;

cursor parametro_c is select codigo_empresa from fatu_500;

begin
  for reg_parametro in parametro_c
  loop
	select count(*)
 into tmpInt
    from empr_008
    where empr_008.codigo_empresa = reg_parametro.codigo_empresa
      and empr_008.param = 'fiscal.msgAjBeneficioDevolVenda';
    if(tmpInt = 0)
    then
      begin
        insert into empr_008 ( codigo_empresa, param, val_int) 
            values (reg_parametro.codigo_empresa, 'fiscal.msgAjBeneficioDevolVenda', 0);
      end;
    end if;

  end loop;
  commit;
end;
