create or replace trigger inter_tr_PEDI_156_log 
after insert or delete or update 
on PEDI_156 
for each row 
declare 
   ws_usuario_rede           varchar2(250) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    -- begin 
    --    select hdoc_090.programa 
    --    into v_nome_programa 
    --    from hdoc_090 
    --    where hdoc_090.sid = ws_sid 
    --      and rownum       = 1 
    --      and hdoc_090.programa not like '%menu%' 
    --      and hdoc_090.programa not like '%_m%'; 
    --    exception 
    --      when no_data_found then            v_nome_programa := 'SQL'; 
    -- end; 
 
    v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 
 
 if inserting 
 then 
    begin 
 
        insert into PEDI_156_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           cgc_9_OLD,   /*8*/ 
           cgc_9_NEW,   /*9*/ 
           cgc_4_OLD,   /*10*/ 
           cgc_4_NEW,   /*11*/ 
           cgc_2_OLD,   /*12*/ 
           cgc_2_NEW,   /*13*/ 
           metragem_min_OLD,   /*14*/ 
           metragem_min_NEW,   /*15*/ 
           pedidos_acima_de_OLD,   /*16*/ 
           pedidos_acima_de_NEW,   /*17*/ 
           pct_peca_media_OLD,   /*18*/ 
           pct_peca_media_NEW,   /*19*/ 
           mtrgm_min_sgnda_qualidade_OLD,   /*20*/ 
           mtrgm_min_sgnda_qualidade_NEW,   /*21*/ 
           pct_menor_peso_OLD,   /*22*/ 
           pct_menor_peso_NEW,   /*23*/ 
           pct_maior_peso_OLD,   /*24*/ 
           pct_maior_peso_NEW,   /*25*/ 
           peso_max_rolo_OLD,   /*26*/ 
           peso_max_rolo_NEW,   /*27*/ 
           preferencia_OLD,   /*28*/ 
           preferencia_NEW,   /*29*/ 
           variacao_de_largura_OLD,   /*30*/ 
           variacao_de_largura_NEW    /*31*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.cgc_9, /*9*/   
           0,/*10*/
           :new.cgc_4, /*11*/   
           0,/*12*/
           :new.cgc_2, /*13*/   
           0,/*14*/
           :new.metragem_min, /*15*/   
           0,/*16*/
           :new.pedidos_acima_de, /*17*/   
           0,/*18*/
           :new.pct_peca_media, /*19*/   
           0,/*20*/
           :new.metragem_min_segunda_qualidade, /*21*/   
           0,/*22*/
           :new.pct_menor_peso, /*23*/   
           0,/*24*/
           :new.pct_maior_peso, /*25*/   
           0,/*26*/
           :new.peso_max_rolo, /*27*/   
           0,/*28*/
           :new.preferencia, /*29*/   
           0,/*30*/
           :new.variacao_de_largura /*31*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into PEDI_156_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           cgc_9_OLD, /*8*/   
           cgc_9_NEW, /*9*/   
           cgc_4_OLD, /*10*/   
           cgc_4_NEW, /*11*/   
           cgc_2_OLD, /*12*/   
           cgc_2_NEW, /*13*/   
           metragem_min_OLD, /*14*/   
           metragem_min_NEW, /*15*/   
           pedidos_acima_de_OLD, /*16*/   
           pedidos_acima_de_NEW, /*17*/   
           pct_peca_media_OLD, /*18*/   
           pct_peca_media_NEW, /*19*/   
           mtrgm_min_sgnda_qualidade_OLD, /*20*/   
           mtrgm_min_sgnda_qualidade_NEW, /*21*/   
           pct_menor_peso_OLD, /*22*/   
           pct_menor_peso_NEW, /*23*/   
           pct_maior_peso_OLD, /*24*/   
           pct_maior_peso_NEW, /*25*/   
           peso_max_rolo_OLD, /*26*/   
           peso_max_rolo_NEW, /*27*/   
           preferencia_OLD, /*28*/   
           preferencia_NEW, /*29*/   
           variacao_de_largura_OLD, /*30*/   
           variacao_de_largura_NEW  /*31*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.cgc_9,  /*8*/  
           :new.cgc_9, /*9*/   
           :old.cgc_4,  /*10*/  
           :new.cgc_4, /*11*/   
           :old.cgc_2,  /*12*/  
           :new.cgc_2, /*13*/   
           :old.metragem_min,  /*14*/  
           :new.metragem_min, /*15*/   
           :old.pedidos_acima_de,  /*16*/  
           :new.pedidos_acima_de, /*17*/   
           :old.pct_peca_media,  /*18*/  
           :new.pct_peca_media, /*19*/   
           :old.metragem_min_segunda_qualidade,  /*20*/  
           :new.metragem_min_segunda_qualidade, /*21*/   
           :old.pct_menor_peso,  /*22*/  
           :new.pct_menor_peso, /*23*/   
           :old.pct_maior_peso,  /*24*/  
           :new.pct_maior_peso, /*25*/   
           :old.peso_max_rolo,  /*26*/  
           :new.peso_max_rolo, /*27*/   
           :old.preferencia,  /*28*/  
           :new.preferencia, /*29*/   
           :old.variacao_de_largura,  /*30*/  
           :new.variacao_de_largura  /*31*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into PEDI_156_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           cgc_9_OLD, /*8*/   
           cgc_9_NEW, /*9*/   
           cgc_4_OLD, /*10*/   
           cgc_4_NEW, /*11*/   
           cgc_2_OLD, /*12*/   
           cgc_2_NEW, /*13*/   
           metragem_min_OLD, /*14*/   
           metragem_min_NEW, /*15*/   
           pedidos_acima_de_OLD, /*16*/   
           pedidos_acima_de_NEW, /*17*/   
           pct_peca_media_OLD, /*18*/   
           pct_peca_media_NEW, /*19*/   
           mtrgm_min_sgnda_qualidade_OLD, /*20*/   
           mtrgm_min_sgnda_qualidade_NEW, /*21*/   
           pct_menor_peso_OLD, /*22*/   
           pct_menor_peso_NEW, /*23*/   
           pct_maior_peso_OLD, /*24*/   
           pct_maior_peso_NEW, /*25*/   
           peso_max_rolo_OLD, /*26*/   
           peso_max_rolo_NEW, /*27*/   
           preferencia_OLD, /*28*/   
           preferencia_NEW, /*29*/   
           variacao_de_largura_OLD, /*30*/   
           variacao_de_largura_NEW /*31*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.cgc_9, /*8*/   
           0, /*9*/
           :old.cgc_4, /*10*/   
           0, /*11*/
           :old.cgc_2, /*12*/   
           0, /*13*/
           :old.metragem_min, /*14*/   
           0, /*15*/
           :old.pedidos_acima_de, /*16*/   
           0, /*17*/
           :old.pct_peca_media, /*18*/   
           0, /*19*/
           :old.metragem_min_segunda_qualidade, /*20*/   
           0, /*21*/
           :old.pct_menor_peso, /*22*/   
           0, /*23*/
           :old.pct_maior_peso, /*24*/   
           0, /*25*/
           :old.peso_max_rolo, /*26*/   
           0, /*27*/
           :old.preferencia, /*28*/   
           0, /*29*/
           :old.variacao_de_largura, /*30*/   
           0 /*31*/
         );    
    end;    
 end if;    
end inter_tr_PEDI_156_log;
