CREATE OR REPLACE PROCEDURE "INTER_PR_SEND_FILE" (p_arquivo_blob in blob, 		p_filename in varchar2,
														p_mime_type in varchar2,
														p_cod_empresa in number,	p_usuario_logado in varchar2,
														p_pass in varchar2,			p_pasta in varchar2,
														p_nome_programa in varchar2)
IS
  v_url VARCHAR2(1);
begin
	--Está procedure só existe para compilar a primeira vez o objeto
	--o que vale está no systextil-apex
	v_url := '';
end inter_pr_send_file;
 

/

exec inter_pr_recompile;

