INSERT INTO hdoc_035
( codigo_programa, programa_menu, 
 item_menu_def,   descricao)
VALUES
( 'inte_l050',     1,
 1,        'Importação de Pedido de Paraguai');
 
INSERT INTO hdoc_033
( usu_prg_cdusu, usu_prg_empr_usu, 
 programa,      nome_menu, 
 item_menu,     ordem_menu, 
 incluir,       modificar, 
 excluir,       procurar)
VALUES
( 'INTERSYS',    1, 
 'inte_l050',   'inte_menu', 
 1,             1, 
 'S',           'S', 
 'S',           'S');
 
INSERT INTO hdoc_033
( usu_prg_cdusu,  usu_prg_empr_usu, 
 programa,       nome_menu, 
 item_menu,      ordem_menu, 
 incluir,        modificar, 
 excluir,        procurar)
VALUES
( 'TREINAMENTO',  1, 
 'inte_l050',    'inte_menu', 
 1,              1, 
 'S',            'S', 
 'S',            'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Ficha Técnica de Insumos'
 WHERE hdoc_036.codigo_programa = 'inte_l050'
   AND hdoc_036.locale          = 'es_ES';
   
commit;

exec inter_pr_recompile;
