alter table pedi_100 add (cod_forma_pagto number(2));

comment on column pedi_100.cod_forma_pagto is 'Indica a forma de pagamento.';

alter table pedi_100
modify cod_forma_pagto default 0;

declare
    conta_reg number := 0;
begin
   for pedi in (
      select rowid from pedi_100 where pedi_100.cod_forma_pagto is null
   )
   loop   
      update pedi_100
      set cod_forma_pagto = 0
      where rowid = pedi.rowid;
      
      conta_reg := conta_reg + 1;
      
      if conta_reg = 5000
      then 
         conta_reg := 0;
         commit;
      end if;
      
   end loop;
   
   commit;
end;
/
