CREATE OR REPLACE PROCEDURE "INTER_PR_ATUALIZA_REQUISICAO" (
   p_requisicao               in number,
   p_seq_requisicao           in number,
   
   p_nivel_new                in varchar2,
   p_grupo_new                in varchar2,
   p_subgrupo_new             in varchar2,
   p_item_new                 in varchar2,
   
   p_quantidade_new           in number,
   
   p_estagio_new              in number,
   p_combinacao               in varchar2,
   p_consumo                  in number
)
is
   v_qtde_requisitada_aux     number;
   v_saldo_requisicao		  number;
   
   v_saldo_requisicao_new     number;
   v_qtde_requisicao_new	  number;
  
   v_qtde_saldo_525           number;
   v_seq_requisicao_520_new   number;
   v_grupo                    varchar2(5);
   v_subgrupo                 varchar2(3);
   v_item                     varchar2(6);
  
   ws_desc_grupo              varchar2(200);
   ws_desc_subgrupo           varchar2(200);
   ws_desc_item               varchar2(200);
  
   
   v_nivel_confeccionado varchar2(200);
   v_grupo_confeccionado varchar2(200);
   v_subgrupo_confeccionado varchar2(200);
   v_item_confeccionado varchar2(200);
	
   v_pacote_new number;
  
   v_desc_produto     	  	 varchar2(2000);
   v_narrativa_item 		 varchar2(2000);
   
   v_temp                     number(5);
   p_requisicao1              number;
   p_seq_requisicao1          number;
  
   v_estagio_old              number;
   v_estagio_new              number;

   ws_usuario_rede            varchar2(20);
   ws_maquina_rede            varchar2(40);
   ws_aplicativo              varchar2(20);
   ws_sid                     number(9);
   ws_empresa                 number(3);
   ws_usuario_systextil       varchar2(250);
   ws_locale_usuario          varchar2(5);
   v_fazer_loop               boolean;
   v_fez_loop                 boolean;
   v_houve_troca_estagio      boolean;
   consumoInsert              number(12,5);
   v_contador              	  number(10);
   v_contador_produto		  number;
   v_novo_saldo				  number;
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,      ws_maquina_rede, ws_aplicativo,     ws_sid,
                           ws_usuario_systextil, ws_empresa,      ws_locale_usuario);
 
   v_fez_loop := false;
   -- Valida produto
   begin
      select 1
      into   v_grupo
      from basi_030
      where basi_030.nivel_estrutura = p_nivel_new 
      and   basi_030.referencia      = p_grupo_new;
   exception when OTHERS then
      RAISE_APPLICATION_ERROR (-20205, 'ATEN��O! Grupo do componente n�o cadastrado.');
   end;
   
   begin
      select 1
      into   v_subgrupo
      from basi_020
      where basi_020.basi030_nivel030 = p_nivel_new
      and   basi_020.basi030_referenc = p_grupo_new 
      and   basi_020.tamanho_ref      = p_subgrupo_new;
   exception when OTHERS then
      RAISE_APPLICATION_ERROR (-20205, 'ATEN��O! Subgrupo do componente n�o cadastrado.');
   end;
    
   begin
      select 1
      into   v_item
      from basi_010
      where basi_010.nivel_estrutura  = p_nivel_new 
      and   basi_010.grupo_estrutura  = p_grupo_new 
      and   basi_010.subgru_estrutura = p_subgrupo_new 
      and   basi_010.item_estrutura   = p_item_new;
   exception when OTHERS then
      RAISE_APPLICATION_ERROR (-20205, 'ATEN��O! Item do componente n�o cadastrado.');
   end;
   
   begin
      select a.qtde_requisitada,     a.cod_estagio
      into   v_qtde_requisitada_aux, v_estagio_old
      from supr_520 a
      where a.num_requisicao = p_requisicao
      and   a.sequencia      = p_seq_requisicao;
   exception when OTHERS then
      v_qtde_requisitada_aux := 0;
     v_estagio_old := 0;
   end;

   v_saldo_requisicao := 0;
  
   begin
	   select sum(qtde_saldo) as qtde_saldo 
	   into v_saldo_requisicao
	   from supr_525 
	   where num_requisicao = p_requisicao
	   and   seq_requisicao = p_seq_requisicao
	   group by seq_requisicao;
   exception when OTHERS then
      v_saldo_requisicao := 0;
   end;
  
   if(p_quantidade_new is null or p_quantidade_new = 0) then
   	RAISE_APPLICATION_ERROR (-20205, 'ATEN��O! Preencha o campo com uma quantidade valida!.');
   end if;
  
   if (p_quantidade_new > v_qtde_requisitada_aux) 
   then
      RAISE_APPLICATION_ERROR (-20205, 'ATEN��O! A quantidade informada n�o pode maior que a quantidade requisitada ' || v_qtde_requisitada_aux ||'.');
   end if;
  
   if p_quantidade_new > v_saldo_requisicao
   then
      RAISE_APPLICATION_ERROR (-20205, 'ATEN��O! A quantidade informada passou o saldo da requisi��o ' || v_saldo_requisicao ||'.');
   end if;
  
   -- Valida est�gio
   if p_estagio_new is null or p_estagio_new = 0
   then
      v_estagio_new := v_estagio_old;
   else
      v_estagio_new := p_estagio_new;
   end if;
  
   if v_estagio_new is null or v_estagio_new = 0
   then
      RAISE_APPLICATION_ERROR (-20205, 'ATEN��O! O est�gio do componente n�o foi informado.');
   end if;
  
   v_houve_troca_estagio := false;
   if v_estagio_old <> p_estagio_new 
   then
   	   v_houve_troca_estagio := true;
   end if;
  
   begin
      select nvl(max(a.sequencia),0) + 1
      into   v_seq_requisicao_520_new
      from supr_520 a
      where a.num_requisicao = p_requisicao;
   exception when OTHERS then
      v_seq_requisicao_520_new := 0;
   end;
   
   --Busca a descr_referencia
   begin
      select nvl(basi_030.descr_referencia,' ')
      into ws_desc_grupo
      from basi_030
      where basi_030.nivel_estrutura = p_nivel_new
        and basi_030.referencia      = p_grupo_new;
   exception when no_data_found then
      ws_desc_grupo := ' ';
   end;
  

   begin
      select nvl(basi_020.descr_tam_refer,' ')
      into ws_desc_subgrupo
      from basi_020
      where basi_020.basi030_nivel030 = p_nivel_new
        and basi_020.basi030_referenc = p_grupo_new
        and basi_020.tamanho_ref      = p_subgrupo_new;
   exception when no_data_found then
      ws_desc_subgrupo := ' ';
   end;

   begin
      select nvl(basi_010.descricao_15,' '), nvl(basi_010.narrativa,' ')
      into ws_desc_item,                     v_narrativa_item
      from basi_010
      where basi_010.nivel_estrutura  = p_nivel_new
        and basi_010.grupo_estrutura  = p_grupo_new
        and basi_010.subgru_estrutura = p_subgrupo_new
        and basi_010.item_estrutura   = p_item_new;
   exception when no_data_found then
      ws_desc_item     := ' ';
      v_narrativa_item := ' ';
   end;

   --v_desc_produto := ws_desc_grupo || ' ' || ws_desc_subgrupo  || ' ' || ws_desc_item;
   
   begin
      insert into supr_520 (
         num_requisicao,      sequencia, 
         nivel,               grupo, 
         subgrupo,            item, 
         situacao,            numero_os, 
         obs_requerente,      qtde_requisitada, 
         qtde_entregue,       data_entregue, 
         usuario_receptor,    almoxarife, 
         obs_almoxarife,      usuario_canc,
         data_canc,           cod_transacao,
         marca_atendimento,   qtde_entregue_aux, 
         qtde_nao_emp,        requis_compra, 
         deposito,            data_requis, 
         ccusto_destino,      narrativa_prod, 
         solicitacao,         emp_solic, 
         usuario_cardex,      nome_programa, 
         req_almox_origem,    seq_almox_origem, 
         projeto,             subprojeto, 
         servico,             ordem_producao,
         cod_estagio,         area_producao,
         lote_estoque,        deposito_entrega,
         executa_trigger,     hora_requis, 
         qtde_emp_faccao_nfs, flag_faccionista, 
         serv_solicitacao,    numero_ordem,
         seq_ordem_serv,      flag_separacao, 
         controla_log,        sit_transf, 
         seq_original
      ) select       
               num_requisicao,      v_seq_requisicao_520_new, 
               p_nivel_new,         p_grupo_new, 
               p_subgrupo_new,      p_item_new, 
               situacao,            numero_os, 
               obs_requerente,      p_quantidade_new, 
               qtde_entregue,       data_entregue, 
               usuario_receptor,    almoxarife, 
               obs_almoxarife,      usuario_canc,
               data_canc,           cod_transacao,
               marca_atendimento,   qtde_entregue_aux, 
               qtde_nao_emp,        requis_compra, 
               deposito,            data_requis, 
               ccusto_destino,      v_narrativa_item, 
               solicitacao,         emp_solic, 
               usuario_cardex,      nome_programa, 
               req_almox_origem,    seq_almox_origem, 
               projeto,             subprojeto, 
               servico,             ordem_producao,
               v_estagio_new,       area_producao,
               lote_estoque,        deposito_entrega,
               executa_trigger,     hora_requis, 
               qtde_emp_faccao_nfs, flag_faccionista, 
               serv_solicitacao,    numero_ordem,
               seq_ordem_serv,      flag_separacao, 
               controla_log,        sit_transf, 
               p_seq_requisicao
            from supr_520
            where num_requisicao = p_requisicao
            and   sequencia      = p_seq_requisicao;
   end;
  
   if p_combinacao is null
   then
      --Fecha a quantidade
      if v_qtde_requisitada_aux = p_quantidade_new
      then
         begin
            insert into supr_525 (
              ordem_producao,              pacote,
              num_requisicao,              seq_requisicao,
              nivel_prod,                  grupo_prod,
              subgrupo_prod,               item_prod,
              sequencia,                   nivel_comp,
              grupo_comp,                  subgrupo_comp,
              item_comp,                   consumo,
              estagio,                     qtde_solicitado,
              qtde_saldo
            ) select
              ordem_producao,              pacote,
              num_requisicao,              v_seq_requisicao_520_new,
              nivel_prod,                  grupo_prod,
              subgrupo_prod,               item_prod,           
              sequencia,                   p_nivel_new,         
              p_grupo_new,                 p_subgrupo_new,      
              p_item_new,                  consumo,
              v_estagio_new,               qtde_saldo,
              qtde_saldo
              from supr_525
              where num_requisicao = p_requisicao
              and   seq_requisicao = p_seq_requisicao;
         end;
         v_fez_loop := true;
        
         begin
            update supr_525
            set   qtde_saldo      = 0,
                  qtde_solicitado = 0
            where num_requisicao  = p_requisicao
              and seq_requisicao  = p_seq_requisicao;
         end;
        
      else
         v_qtde_requisitada_aux := p_quantidade_new;
         v_fazer_loop := true;
         
        --Substitui produto
         for dados_525 in (select supr_525.ordem_producao,  supr_525.pacote,     supr_525.num_requisicao, 
                                  supr_525.seq_requisicao,  supr_525.nivel_prod, supr_525.grupo_prod, 
                                  supr_525.subgrupo_prod,   supr_525.item_prod,  supr_525.sequencia, 
                                  supr_525.nivel_comp,      supr_525.grupo_comp, supr_525.subgrupo_comp, 
                                  supr_525.item_comp,       supr_525.consumo,    supr_525.estagio, 
                                  supr_525.qtde_solicitado, supr_525.qtde_saldo 
                           from supr_525
                           where supr_525.num_requisicao = p_requisicao
                           and   supr_525.seq_requisicao = p_seq_requisicao
                           and   supr_525.qtde_saldo 	 > 0
                           and exists( select 1
									   from pcpc_040
									   where ordem_producao  = supr_525.ordem_producao
									   and   ordem_confeccao = supr_525.pacote
									   and   codigo_estagio  = v_estagio_new)
                           order by supr_525.item_prod, supr_525.pacote)
         loop
            
            if v_fazer_loop
            then
               if dados_525.qtde_saldo < v_qtde_requisitada_aux 
               then
                  if p_consumo is not null and p_consumo > 0
                  then
                      v_qtde_saldo_525       := ((dados_525.qtde_saldo / dados_525.consumo) * p_consumo);
                      v_qtde_requisitada_aux := v_qtde_requisitada_aux - ((dados_525.qtde_saldo / dados_525.consumo) * p_consumo);
                  else
                      v_qtde_saldo_525       := dados_525.qtde_saldo;
                      v_qtde_requisitada_aux := v_qtde_requisitada_aux - dados_525.qtde_saldo;
               	  end if;
               else
                  v_qtde_saldo_525       := v_qtde_requisitada_aux;
                 
                  v_qtde_requisitada_aux := 0;
                  v_fazer_loop := false;
               end if;
			  
               if p_consumo is not null and p_consumo > 0
               then
                  consumoInsert := p_consumo;
               else
                  consumoInsert := dados_525.consumo;
               end if;
               
               begin
                  update supr_525
                  set   supr_525.qtde_saldo      = supr_525.qtde_saldo - v_qtde_saldo_525,
                        supr_525.qtde_solicitado = supr_525.qtde_solicitado - v_qtde_saldo_525
                  where supr_525.num_requisicao  = p_requisicao
                    and supr_525.seq_requisicao  = p_seq_requisicao
                    and supr_525.pacote          = dados_525.pacote;
               end;
               
               begin
                  insert into supr_525 (
                        ordem_producao,            pacote,               num_requisicao, 
                        seq_requisicao,            nivel_prod,           grupo_prod, 
                        subgrupo_prod,             item_prod,            sequencia, 
                        nivel_comp,                grupo_comp,           subgrupo_comp, 
                        item_comp,                 consumo,              estagio, 
                        qtde_solicitado,           qtde_saldo
                  ) values ( 
                        dados_525.ordem_producao,  dados_525.pacote,     p_requisicao, 
                        v_seq_requisicao_520_new,  dados_525.nivel_prod, dados_525.grupo_prod, 
                        dados_525.subgrupo_prod,   dados_525.item_prod,  dados_525.sequencia, 
                        p_nivel_new,               p_grupo_new,          p_subgrupo_new, 
                        p_item_new,                consumoInsert,        v_estagio_new, 
                        v_qtde_saldo_525,          v_qtde_saldo_525
                  );
               end;
               
               v_fez_loop := true;
            end if;        
         end loop;
      end if;
   else
   	  --Combinacao
      v_qtde_requisitada_aux := p_quantidade_new;
      v_fazer_loop := true;
      v_novo_saldo := 0;
     
      for reg in (select regexp_substr(p_combinacao,'[^:]+', 1, level) as pacote from dual 
                  connect by regexp_substr(p_combinacao, '[^:]+', 1, level) is not NULL)
      loop
      	  v_contador := 0;
      	 
      	  FOR SPLITARPACOTEINFO IN (    SELECT REGEXP_SUBSTR (reg.pacote,
                                                '[^-]+',
                                                1,
                                                LEVEL)
                                    TXT
                            FROM DUAL
                    CONNECT BY REGEXP_SUBSTR (reg.pacote,
                                                '[^-]+',
                                                1,
                                                LEVEL)
                                    IS NOT NULL)
		  LOOP
		    v_contador := v_contador + 1;
		   
		    if(v_contador = 1) THEN
		   		v_pacote_new := SPLITARPACOTEINFO.txt;
		    end if;
		   
		    if(v_contador = 2) THEN
		        v_contador_produto := 0;
		        FOR SPLITARPRODUTOCONF IN (    SELECT REGEXP_SUBSTR (SPLITARPACOTEINFO.TXT,
                                                '[^\.]+',
                                                1,
                                                LEVEL)
                                    TXT
                            FROM DUAL
                    CONNECT BY REGEXP_SUBSTR (SPLITARPACOTEINFO.TXT,
                                                '[^\.]+',
                                                1,
                                                LEVEL)
                                    IS NOT NULL)
		  		LOOP
		  		    v_contador_produto := v_contador_produto + 1;
		  		   
		  		    if(v_contador_produto = 1) then
			   			v_nivel_confeccionado := SPLITARPRODUTOCONF.txt;
			   		end if;
			   	
			   		if(v_contador_produto = 2) then
			   			v_grupo_confeccionado := SPLITARPRODUTOCONF.txt;
			   		end if;
			   	
			   		if(v_contador_produto = 3) then
			   			v_subgrupo_confeccionado := SPLITARPRODUTOCONF.txt;
			   		end if;
			   	
			   		if(v_contador_produto = 4) then
			   			v_item_confeccionado := SPLITARPRODUTOCONF.txt;
			   		end if;
			   	END LOOP;
			end if;
		 END LOOP;
      
      	 for regComb in (select ordem_producao,              pacote,
                                num_requisicao,              v_seq_requisicao_520_new,
                                 nivel_prod,                  grupo_prod,
                                 subgrupo_prod,               item_prod,           
                                 sequencia,                   p_nivel_new,         
                                 p_grupo_new,                 p_subgrupo_new,      
                                 p_item_new,                  consumo,
                                 v_estagio_new,               qtde_solicitado,
                                 qtde_saldo
                                 from supr_525
                                 where num_requisicao = p_requisicao
                                 and   seq_requisicao = p_seq_requisicao
                                 and   nivel_prod     = v_nivel_confeccionado
                                 and   grupo_prod     = v_grupo_confeccionado
                                 and   subgrupo_prod  = v_subgrupo_confeccionado
                                 and   item_prod      = v_item_confeccionado
                                 and   pacote      = v_pacote_new
                                 and exists( select 1
										     from pcpc_040
										     where ordem_producao  = supr_525.ordem_producao
										     and   ordem_confeccao = supr_525.pacote
										     and   codigo_estagio  = v_estagio_new)
                                 order by supr_525.pacote)
         loop
            v_novo_saldo := v_novo_saldo + regComb.qtde_saldo;
            
            if v_fazer_loop
            then
               if regComb.qtde_saldo < v_qtde_requisitada_aux 
               then
                  if p_consumo is not null and p_consumo > 0
                  then
                      v_qtde_saldo_525       := ((regComb.qtde_saldo / regComb.consumo) * p_consumo);
                      v_qtde_requisitada_aux := v_qtde_requisitada_aux - ((regComb.qtde_saldo / regComb.consumo) * p_consumo);
                  else
                      v_qtde_saldo_525       := regComb.qtde_saldo;
                      v_qtde_requisitada_aux := v_qtde_requisitada_aux - regComb.qtde_saldo;
                  end if;                  
               else
                  v_qtde_saldo_525       := v_qtde_requisitada_aux;
                  
                  v_qtde_requisitada_aux := 0;
                  v_fazer_loop := false;
               end if;
            
               if p_consumo is not null and p_consumo > 0
               then
                  consumoInsert := p_consumo;
               else
                  consumoInsert := regComb.consumo;
               end if;
               
               begin
                  update supr_525
                  set   qtde_saldo      = supr_525.qtde_saldo - v_qtde_saldo_525,
                        qtde_solicitado = supr_525.qtde_solicitado - v_qtde_saldo_525
                  where num_requisicao  = p_requisicao
                    and seq_requisicao  = p_seq_requisicao
                    and ordem_producao  = regComb.ordem_producao
                    and pacote          = regComb.pacote;
               end;
               
               begin
                  insert into supr_525 (
                     ordem_producao,              pacote,
                     num_requisicao,              seq_requisicao,
                     nivel_prod,                  grupo_prod,
                     subgrupo_prod,               item_prod,
                     sequencia,                   nivel_comp,
                     grupo_comp,                  subgrupo_comp,
                     item_comp,                   consumo,
                     estagio,                     qtde_solicitado,
                     qtde_saldo 
                  ) VALUES (
                     regComb.ordem_producao,      regComb.pacote,
                     regComb.num_requisicao,      regComb.v_seq_requisicao_520_new,
                     regComb.nivel_prod,          regComb.grupo_prod,
                     regComb.subgrupo_prod,       regComb.item_prod,
                     regComb.sequencia,           p_nivel_new,         
                     p_grupo_new,                 p_subgrupo_new,      
                     p_item_new,                  regComb.consumo,
                     v_estagio_new,               v_qtde_saldo_525,
                     v_qtde_saldo_525
                  );
               end;
               
               v_fez_loop := true;
           
            end if; 
         end loop;
      end loop;
     
      if v_qtde_requisitada_aux > 0
      then
         RAISE_APPLICATION_ERROR (-20205, 'ATEN��O! A quantidade informada para as combina��es selecionadas n�o pode ser transferida, saldo insuficiente: ' || v_qtde_requisitada_aux);
      end if;
     
      if v_houve_troca_estagio = true
      then
          if p_quantidade_new < v_novo_saldo
	      then
	         RAISE_APPLICATION_ERROR (-20205, 'ATEN��O! Quantidade do saldo n�o pode ser menor que a requisitada: ' || p_quantidade_new);
	      end if;
      end if;
   end if;
  
   if v_fez_loop = false 
   then
   	RAISE_APPLICATION_ERROR (-20205, 'ATEN��O! N�o foi possivel encontrar um pacote para concluir a substitui��o.');
   end if;
  
   if v_qtde_requisitada_aux = p_quantidade_new
   then
     begin
         update supr_520
         set   supr_520.situacao          = 7,
               supr_520.qtde_requisitada  = 0,
               supr_520.usuario_canc      = ws_usuario_rede,
               supr_520.data_canc         = sysdate
         where supr_520.num_requisicao    = p_requisicao
           and supr_520.sequencia         = p_seq_requisicao;
     end;
     
     begin
	    select nvl(count(1), 0)
	    into v_temp
	    from supr_520
	    where supr_520.num_requisicao = p_requisicao
	    and supr_520.situacao in (1,2,3,4,5,6,9);
	 exception when OTHERS then
     	v_temp := 0;
     end;
     
     if v_temp = 0 then
	 	update supr_510
	    set   supr_510.situacao       = 2
	    where supr_510.num_requisicao = p_requisicao;
	 end if;
	
   else
      begin
         update supr_520
         set   supr_520.qtde_requisitada  = supr_520.qtde_requisitada - p_quantidade_new
         where supr_520.num_requisicao    = p_requisicao
           and supr_520.sequencia         = p_seq_requisicao;
      end;
   end if;
   
   begin
      delete from supr_525
      where num_requisicao  = p_requisicao
        and seq_requisicao  = p_seq_requisicao
        and qtde_solicitado = 0
        and qtde_saldo      = 0;
   end;
  
   --Ajusta a qtde_requisicao conforme o saldo que possui nos pacotes
   begin
	   select sum(qtde_saldo) 
	   into v_saldo_requisicao_new
	   from supr_525 
	   where num_requisicao = p_requisicao
	   and   seq_requisicao = v_seq_requisicao_520_new
	   group by seq_requisicao;
   exception when OTHERS then
      v_saldo_requisicao_new := 0;
   end;
  
   begin
	   select sum(QTDE_SOLICITADO) 
	   into v_qtde_requisicao_new
	   from supr_525 
	   where num_requisicao = p_requisicao
	   and   seq_requisicao = p_seq_requisicao
	   group by seq_requisicao;
   exception when OTHERS then
      v_qtde_requisicao_new := 0;
   end;
  
   if v_saldo_requisicao_new <> 0 and v_saldo_requisicao_new <> p_quantidade_new
   then
       begin
		   update supr_520 set qtde_requisitada = v_saldo_requisicao_new
		   where num_requisicao = p_requisicao
		   and   sequencia      = v_seq_requisicao_520_new;
       end;
      
       begin
		   update supr_520 set qtde_requisitada = v_qtde_requisicao_new
		   where num_requisicao = p_requisicao
		   and   sequencia      = p_seq_requisicao;
       end;
   end if;
end inter_pr_atualiza_requisicao;
