alter table estq_310
ADD ORDEM_AGRUPAMENTO number(9,0) DEFAULT 0 NOT NULL;

COMMENT ON COLUMN ESTQ_310.TIPO_ORDEM  IS 'estq_310.tipo_ordem faz referência ao campo pcpb_100.tipo_ordem';
COMMENT ON COLUMN ESTQ_310.ORDEM_AGRUPAMENTO  IS 'estq_310.ORDEM_AGRUPAMENTO faz referência ao campo pcpb_100.ORDEM_AGRUPAMENTO';
