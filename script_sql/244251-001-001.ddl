create table pcpt_074(
    codigo_produto_global  number(9,0)
    ,realiza_manutencao    number(1,0) default 0
);

alter table pcpt_074
add constraint pk_pcpt_074 primary key (codigo_produto_global);
