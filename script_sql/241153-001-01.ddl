drop table obrf_520; -- Motoristas Cadastrados para o transporte (MDF-e)
drop table obrf_523; -- NF-e relacionada ao MDF-e
drop table obrf_522; -- Dados do MDF-e
drop table obrf_521; -- XML do MDF-e
drop table obrf_524; -- Veículos Cadastrados para o transporte do MDF-e


---------- obrf_520
create table obrf_520(
    cgc_09          number(9,0)
	,cgc_04         number(4,0)
	,cgc_02         number(2,0)
	,codigo_empresa number(3,0)
	,nome_motorista varchar2(400)
	,constraint pk_obrf_520 primary key (cgc_09, cgc_04, cgc_02) using index enable
);

---------- obrf_521
create table obrf_521(
    id            number not null 
	,xml          clob
	,data_xml     timestamp (6) with local time zone
	,retorno      clob
	,data_retorno timestamp (6) with local time zone
	,evento       number(1,0)
	,constraint obrf_521_id_pk primary key (id) using index enable
);

-------- obrf_522 
create table obrf_522( 
    codigo_empresa         number(3,0)
	,numero_mdfe           number(9,0)
	,serie_mdfe            number(3,0) 
	,ambiente_envio        number(1,0) 
	,unidade_transporte    number(1,0) 
	,cgc_09_motorista      number(9,0) 
	,cgc_04_motorista      number(4,0) 
	,cgc_02_motorista      number(2,0) 
	,placa_veiculo         varchar2(7) 
	,rntrc_veiculo         varchar2(8) 
	,modalidade            number(1,0) default 1 
	,status                number(1,0) 
	,cod_municipio_destino number(5,0) 
	,itinerario            varchar2(244) 
	,cod_municipio_origem  number(7,0) 
	,uf_veiculo            varchar2(2) 
	,xml                   number 
	,data_mdfe             timestamp (6) with local time zone 
	,tipo_rodado           number
	,data_autorizacao_mdfe timestamp (6) with local time zone 
	,numero_protocolo_mdfe number(15,0)
	,chave_mdfe            varchar2(244)
	,constraint pk_obrf_522 primary key (codigo_empresa, numero_mdfe, serie_mdfe, ambiente_envio) using index  enable
);

alter table  obrf_522 add constraint obrf_522_fk_obrf_521 foreign key (xml) references obrf_521 (id) enable;

----------- obrf_523
create table  obrf_523(
    chave_acesso_nfe               varchar2(44)
	,numero_nfe                    number(9,0)
	,serie_nfe                     number(3,0)
	,cod_municipio_origem          number(7,0)
	,cod_municipio_destino         number(7,0)
	,itinerario                    varchar2(244)
	,unidade_medida                number(1,0)
	,peso_total                    number(9,3)
	,valor_nfe                     number(15,2)
	,cod_municipio_descarga_nfe    number(7,0)
	,codigo_empresa                number(3,0)
	,numero_mdfe                   number(9,0)
	,serie_mdfe                    number(3,0)
	,cod_ibge_municipio_origem     number(7,0)
	,cod_ibge_municipio_destino    number(7,0)
	,cod_ibge_municip_descarga_nfe number(7,0)
	,ambiente_envio                number(1,0)
	,constraint pk_obrf_523 primary key (chave_acesso_nfe) using index  enable
);

alter table  obrf_523 add constraint ref_obrf_522_obrf_523 foreign key (codigo_empresa, numero_mdfe, serie_mdfe, ambiente_envio) references obrf_522 (codigo_empresa, numero_mdfe, serie_mdfe, ambiente_envio) enable;

------------- obrf_524
create table obrf_524(
    id              number
	,codigo_empresa number(3,0)
	,placa_veiculo  varchar2(7)
	,rntrc_veiculo  varchar2(8)
	,uf_veiculo     varchar2(2)
	,descricao      varchar2(244)
	,placa_ativa    number(1,0) default 0
	,constraint pk_obrf_524 primary key (id) using index enable
);
