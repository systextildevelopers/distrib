alter table obrf_180 add cod_msg_nf_saida_relacionada number(6) default 0;
alter table obrf_180 add seq_msg_nf_saida_relacionada number(2) default 0;
alter table obrf_180 add loc_msg_nf_saida_relacionada varchar2(1) default ' ';

comment on column obrf_180.cod_msg_nf_saida_relacionada is 'Código da mensagem referent a nota fiscal de saída que estiver associada uma ordem de serviço';
comment on column obrf_180.seq_msg_nf_saida_relacionada is 'Sequencia da mensagem referente a nota fiscal de saída que estiver associada uma ordem de serviço ';
comment on column obrf_180.loc_msg_nf_saida_relacionada is 'Local da mensagem referente a nota fiscal de saída que estiver associada uma ordem de serviço';
