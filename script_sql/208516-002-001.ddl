create table MANU_016
(
  codigo    NUMBER(5) default 0 not null,
  descricao VARCHAR2(60) default ''
);

alter table manu_016 add constraint pk_manu_016 primary key (codigo);

/
exec inter_pr_recompile;
