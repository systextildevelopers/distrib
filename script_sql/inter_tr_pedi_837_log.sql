
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_837_LOG" 
   after insert
   or delete
   or update of
	   pedido_venda, codigo_variante, tamanho_ref, qtde_metro,
	   qtde_unidade, seq_entrega
   on pedi_837
   for each row

declare
   ws_usuario_rede          varchar2(20);
   ws_maquina_rede          varchar2(40);
   ws_aplicativo            varchar2(20);
   long_aux                 varchar2(2000);
begin

    if inserting
    then


       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;


       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01,             long01
          )
       VALUES
          ( 'PEDI_837',        'I',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :new.pedido_venda,
            '                              PEDIDO DE VENDAS - TAMANHOS'||
                                     chr(10)                  ||
                                     chr(10)                  ||
            'PEDIDO VENDA.....: ' || :new.pedido_venda        ||
            chr(10)                                           ||
            'CODIGO VARIANTE..: ' || :new.codigo_variante     ||
            chr(10)                                           ||
            'TAMANHO_REF......: ' || :new.tamanho_ref         ||
            chr(10)                                           ||
            'QTDE METRO.......: ' || :new.qtde_metro          ||
            chr(10)                                           ||
            'QTDE UNIDADE.....: ' || :new.qtde_unidade        ||
            chr(10)                                           ||
            'SEQ ENTREGA......: ' || :new.seq_entrega
         );
    end if;

    if updating and
       (:old.pedido_venda        <> :new.pedido_venda      or
        :old.codigo_variante     <> :new.codigo_variante   or
        :old.tamanho_ref         <> :new.tamanho_ref       or
        :old.qtde_metro          <> :new.qtde_metro        or
        :old.qtde_unidade        <> :new.qtde_unidade      or
        :old.seq_entrega         <> :new.seq_entrega
      )
    then
       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;

       long_aux := long_aux ||
                  '                              PEDIDO DE VENDAS - TAMANHOS'||
                   chr(10)                    ||
                   chr(10);

       if  :old.pedido_venda <> :new.pedido_venda
       then
           long_aux := long_aux ||
                'PEDIDO VENDA....: ' || :old.pedido_venda    || ' - '
                                     || :new.pedido_venda
                                     || chr(10);
       end if;

       if  :old.codigo_variante  <> :new.codigo_variante
       then
           long_aux := long_aux ||
                'CODIGO VARIANTE.: ' || :old.codigo_variante     || ' - '
                                     || :new.codigo_variante
                                     || chr(10);
       end if;

       if  :old.tamanho_ref   <> :new.tamanho_ref
       then
           long_aux := long_aux ||
                'TAMANHO REF.....: ' || :old.tamanho_ref      || ' - '
                                     || :new.tamanho_ref
                                     || chr(10);
       end if;

       if  :old.qtde_metro   <> :new.qtde_metro
       then
           long_aux := long_aux ||
                'QTDE METRO......: ' || :old.qtde_metro      || ' - '
                                     || :new.qtde_metro
                                     || chr(10);
       end if;

       if  :old.qtde_unidade   <> :new.qtde_unidade
       then
           long_aux := long_aux ||
                'QTDE UNIDADE....: ' || :old.qtde_unidade      || ' - '
                                     || :new.qtde_unidade
                                     || chr(10);
       end if;

       if  :old.seq_entrega   <> :new.seq_entrega
       then
           long_aux := long_aux ||
                'SEQ ENTREGA.....: ' || :old.seq_entrega      || ' - '
                                     || :new.seq_entrega
                                     || chr(10);
       end if;

       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01,             long01
          )
       VALUES
          ( 'PEDI_837',        'A',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :new.pedido_venda, long_aux
         );
    end if;

    if deleting
    then


       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;


       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01
          )
       VALUES
          ( 'PEDI_837',        'D',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :old.pedido_venda
         );
    end if;

end inter_tr_pedi_837_log;

-- ALTER TRIGGER "INTER_TR_PEDI_837_LOG" ENABLE
 

/

exec inter_pr_recompile;

