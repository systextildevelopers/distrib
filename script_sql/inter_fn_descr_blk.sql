CREATE OR REPLACE FUNCTION inter_fn_descr_blk
        (COD_ESTAGIO_AGRUPADOR_INSU  NUMBER,
         COD_ESTAGIO_SIMULTANEO_INSU NUMBER)         

RETURN VARCHAR2

IS
v_desc_estagio_agrupador      varchar2(100);
v_desc_estagio_simultaneo     varchar2(100);

BEGIN
  v_desc_estagio_agrupador    := '';
  v_desc_estagio_simultaneo   := '';
  begin
    if COD_ESTAGIO_AGRUPADOR_INSU  > 0
    then
       begin
          select '' || desc_estagio
          into v_desc_estagio_agrupador
          from mqop_005
          where codigo_estagio = COD_ESTAGIO_AGRUPADOR_INSU
          and rownum = 1;
          exception
            when no_data_found then
               v_desc_estagio_agrupador := '';            
       end;
    end if;
  end;  
  
  if COD_ESTAGIO_SIMULTANEO_INSU > 0
  then
     begin
       select desc_estagio
       into v_desc_estagio_simultaneo
       from mqop_005
       where codigo_estagio = COD_ESTAGIO_SIMULTANEO_INSU
       and rownum = 1;
       exception
            when no_data_found then
               v_desc_estagio_simultaneo := ''; 
     end;
  else
     v_desc_estagio_simultaneo := '';  
  end if;
  
  return(v_desc_estagio_agrupador || ' ' || v_desc_estagio_simultaneo);

end inter_fn_descr_blk;

