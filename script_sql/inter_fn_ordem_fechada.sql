
  CREATE OR REPLACE FUNCTION "INTER_FN_ORDEM_FECHADA" (
p_ordem_producao        number
) RETURN NUMBER
IS

   qtde_programada                  NUMBER;
   qtde_2a                          NUMBER;
   qtde_conserto                    NUMBER;
   qtde_perdas                      NUMBER;
   v_deposito_conserto              NUMBER;
   qtde_2a_ant                      NUMBER;
   qtde_perdas_ant                  NUMBER;
   qtde_conserto_ant                NUMBER;
   qtde_prod_ant                    NUMBER;
   v_atu_estagios_seguintes         NUMBER;
   qtde_perdas_2a_ant_descontar     NUMBER;

   v_tem_qtde_prog                  number;

BEGIN
   v_tem_qtde_prog := 0;

   -- Inicia o calculo do em produ��o e a produzir
   for reg_pcpc_040 in (select pcpc_040.periodo_producao,  pcpc_040.ordem_confeccao,
                               pcpc_040.proconf_nivel99,   pcpc_040.proconf_grupo,
                               pcpc_040.proconf_subgrupo,  pcpc_040.proconf_item,
                               pcpc_040.estagio_anterior,  pcpc_040.estagio_depende,
                               pcpc_040.qtde_pecas_prog,   pcpc_040.qtde_pecas_prod,
                               pcpc_040.qtde_pecas_2a,     pcpc_040.qtde_conserto,
                               pcpc_040.qtde_perdas,       pcpc_040.qtde_a_produzir_pacote,
                               pcpc_020.ultimo_estagio
                        from pcpc_040, pcpc_020
                        where pcpc_040.ordem_producao   = pcpc_020.ordem_producao
                          and pcpc_040.codigo_estagio   = pcpc_020.ultimo_estagio
                          and pcpc_020.ordem_producao   = p_ordem_producao)
   loop
      qtde_programada := reg_pcpc_040.qtde_pecas_prog;
      qtde_2a         := reg_pcpc_040.qtde_pecas_2a;
      qtde_conserto   := reg_pcpc_040.qtde_conserto;
      qtde_perdas     := reg_pcpc_040.qtde_perdas;

      v_deposito_conserto := 0;

      begin
         select fatu_501.deposito_conserto
         into   v_deposito_conserto
         from fatu_501, pcpc_010
         where fatu_501.codigo_empresa   =  pcpc_010.codigo_empresa
           and pcpc_010.area_periodo     = 1
           and pcpc_010.periodo_producao = reg_pcpc_040.periodo_producao;
      exception when others then
         v_deposito_conserto := 0;
      end;

      v_atu_estagios_seguintes := 0;

      begin
         select fatu_502.atu_estagios_seguintes
         into   v_atu_estagios_seguintes
         from fatu_502, pcpc_010
         where fatu_502.codigo_empresa   =  pcpc_010.codigo_empresa
           and pcpc_010.area_periodo     = 1
           and pcpc_010.periodo_producao = reg_pcpc_040.periodo_producao;
      exception when others then
         v_atu_estagios_seguintes := 0;
      end;

      if reg_pcpc_040.estagio_anterior > 0
      then
         if reg_pcpc_040.estagio_depende > 0
         then
            begin
               select nvl(sum(pcpc_040.qtde_pecas_2a),0),         nvl(sum(pcpc_040.qtde_perdas),0),
                      nvl(sum(pcpc_040.qtde_conserto),0),         nvl(sum(pcpc_040.qtde_pecas_prod),0)
               into   qtde_2a_ant,                                qtde_perdas_ant,
                      qtde_conserto_ant,                          qtde_prod_ant
               from pcpc_040
               where pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                 and pcpc_040.codigo_estagio   = reg_pcpc_040.estagio_depende;
            exception when others then
               qtde_2a_ant         := 0;
               qtde_perdas_ant     := 0;
               qtde_conserto_ant   := 0;
               qtde_prod_ant       := 0;
            end;

            if  qtde_conserto_ant   > 0
            and v_deposito_conserto > 0
            then
               qtde_conserto_ant := qtde_conserto_ant;
            else
               qtde_conserto_ant := 0;
            end if;

            if v_atu_estagios_seguintes > 0
            then
               if qtde_2a < qtde_2a_ant
               then
                  qtde_2a_ant := 0;
               end if;

               if reg_pcpc_040.qtde_perdas < qtde_perdas_ant
               then
                  qtde_perdas_ant := 0;
               end if;

               if qtde_2a = qtde_2a_ant
               then
                  qtde_2a := 0;
               end if;

               if reg_pcpc_040.qtde_perdas = qtde_perdas_ant
               then
                  qtde_perdas := 0;
               end if;

               if reg_pcpc_040.qtde_conserto = qtde_conserto_ant
               then
                  qtde_conserto := 0;
               end if;

               if qtde_2a > qtde_2a_ant
               then
                  qtde_2a := (qtde_2a - qtde_2a_ant);
               end if;

               if reg_pcpc_040.qtde_perdas > qtde_perdas_ant
               then
                  qtde_perdas := (reg_pcpc_040.qtde_perdas - qtde_perdas_ant);
               end if;
            end if;

            if v_atu_estagios_seguintes = 0
            then
               begin
                  select nvl(sum(pcpc_040.qtde_perdas + pcpc_040.qtde_pecas_2a),0)
                  into   qtde_perdas_2a_ant_descontar
                  from pcpc_040
                  where pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                    and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                    and pcpc_040.seq_operacao     < (select min(b.seq_operacao)
                                                     from pcpc_040 b
                                                     where b.periodo_producao = reg_pcpc_040.periodo_producao
                                                       and b.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                                                       and b.codigo_estagio   = reg_pcpc_040.ultimo_estagio);
               exception when others then
                  qtde_perdas_2a_ant_descontar := 0;
               end;

               qtde_programada := qtde_programada - qtde_perdas_2a_ant_descontar;
            else
               qtde_programada := qtde_programada - (qtde_2a_ant + qtde_perdas_ant + qtde_conserto_ant);
            end if;
         else
            begin
               select nvl(sum(pcpc_040.qtde_pecas_2a),0),  nvl(sum(pcpc_040.qtde_perdas),0),
                      nvl(sum(pcpc_040.qtde_conserto),0),  nvl(sum(pcpc_040.qtde_pecas_prod),0)
               into   qtde_2a_ant,                         qtde_perdas_ant,
                      qtde_conserto_ant,                   qtde_prod_ant
               from pcpc_040
               where pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                 and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                 and pcpc_040.codigo_estagio   = reg_pcpc_040.estagio_anterior;
            exception when others then
               qtde_2a_ant       := 0;
               qtde_perdas_ant   := 0;
               qtde_conserto_ant := 0;
               qtde_prod_ant     := 0;
            end;

            if qtde_conserto_ant > 0 and v_deposito_conserto > 0
            then
               qtde_conserto_ant := qtde_conserto_ant;
            else
               qtde_conserto_ant := 0;
            end if;

            if v_atu_estagios_seguintes > 0
            then
               if qtde_2a < qtde_2a_ant
               then
                  qtde_2a_ant := 0;
               end if;

               if reg_pcpc_040.qtde_perdas < qtde_perdas_ant
               then
                  qtde_perdas_ant := 0;
               end if;

               if qtde_2a = qtde_2a_ant
               then
                  qtde_2a := 0;
               end if;

               if reg_pcpc_040.qtde_perdas = qtde_perdas_ant
               then
                  reg_pcpc_040.qtde_perdas := 0;
               end if;

               if reg_pcpc_040.qtde_conserto = qtde_conserto_ant
               then
                  reg_pcpc_040.qtde_conserto := 0;
               end if;

               if qtde_2a > qtde_2a_ant
               then
                  qtde_2a := (qtde_2a - qtde_2a_ant);
               end if;

               if reg_pcpc_040.qtde_perdas > qtde_perdas_ant
               then
                  reg_pcpc_040.qtde_perdas := (reg_pcpc_040.qtde_perdas - qtde_perdas_ant);
               end if;
            end if;

            if v_atu_estagios_seguintes = 0
            then
               begin
                  select nvl(sum(pcpc_040.qtde_perdas + pcpc_040.qtde_pecas_2a),0)
                  into qtde_perdas_2a_ant_descontar
                  from pcpc_040
                  where pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao
                    and pcpc_040.ordem_confeccao   = reg_pcpc_040.ordem_confeccao
                    and pcpc_040.seq_operacao      < (select min(b.seq_operacao)
                                                      from pcpc_040 b
                                                      where b.periodo_producao = reg_pcpc_040.periodo_producao
                                                        and b.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                                                        and b.codigo_estagio   = reg_pcpc_040.ultimo_estagio);
               exception when others then
                 qtde_perdas_2a_ant_descontar := 0;
               end;

               qtde_programada := qtde_programada - qtde_perdas_2a_ant_descontar;
            else
               qtde_programada := qtde_programada - (qtde_2a_ant + qtde_perdas_ant + qtde_conserto_ant);
            end if;
         end if;
      else
         qtde_programada := qtde_programada;
      end if;

      qtde_programada := qtde_programada - (reg_pcpc_040.qtde_pecas_prod +
                                            reg_pcpc_040.qtde_pecas_2a   +
                                            reg_pcpc_040.qtde_conserto   +
                                            reg_pcpc_040.qtde_perdas);

      if qtde_programada > 0
      then
         v_tem_qtde_prog := 1;
      end if;

   end loop;

   RETURN(v_tem_qtde_prog);

END;

 

/

exec inter_pr_recompile;

