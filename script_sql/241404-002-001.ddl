CREATE TABLE supr_680_LOG
(
    TIPO_OCORR         VARCHAR2(1)   DEFAULT ''    NULL,
    DATA_OCORR         DATE                       NULL,
    HORA_OCORR         DATE                       NULL,
    USUARIO_REDE       VARCHAR2(20)  DEFAULT ''   NULL,
    MAQUINA_REDE       VARCHAR2(40)  DEFAULT ''   NULL,
    APLICACAO          VARCHAR2(20)  DEFAULT ''   NULL,
    USUARIO_SISTEMA    VARCHAR2(20)  DEFAULT ''   NULL,
    NOME_PROGRAMA      VARCHAR2(20)  DEFAULT ''   NULL,
    id_OLD             NUMBER(9),
    id_NEW             NUMBER(9),
    fornecedor9_OLD    NUMBER(9),
    fornecedor9_NEW    NUMBER(9),
    fornecedor4_OLD    NUMBER(4),
    fornecedor4_NEW    NUMBER(4),
    fornecedor2_OLD    NUMBER(2),
    fornecedor2_NEW    NUMBER(2),
    tipo_chave_OLD     NUMBER(2),
    tipo_chave_NEW     NUMBER(2),
    chave_OLD          VARCHAR2(100),
    chave_NEW          VARCHAR2(100),
    padrao_OLD         VARCHAR2(1)   DEFAULT 'N',
    padrao_NEW         VARCHAR2(1)   DEFAULT 'N'
);

