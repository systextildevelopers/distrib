  Create table obrf_741 (
    COD_EMPRESA      number(03)    default 0    not null ,
    MES              number(02)    default 0    not null ,
    ANO              number(04)    default 0    not null ,
    UF               varchar2(2)   default ' '  not null,
    COD_AJ_APUR      varchar2(08)  default ' '  not null,
    DESCR_COMPL_AJ   varchar2(200) default ' '  ,
    VL_AJ_APUR       number(13,2)  default 0.00    );
    
  alter table obrf_741
  add constraint PK_OBRF_741 primary key (COD_EMPRESA, MES, ANO,UF, COD_AJ_APUR);
  
  comment on column obrf_741.COD_EMPRESA is 'Codigo de empresa para registro E311 sped';  
  comment on column obrf_741.MES is 'Mes para registro E311 sped';  
  comment on column obrf_741.ANO is 'ano para registro E311 sped';  
  comment on column obrf_741.UF is 'Estado para registro E311 sped';  
  comment on column obrf_741.COD_AJ_APUR is 'Codigo de apuracao para registro E311 sped';  
  comment on column obrf_741.DESCR_COMPL_AJ is 'Descri��o complementar do ajuste da apura��o para registro E311 sped';  
  comment on column obrf_741.VL_AJ_APUR is 'Valor TOTAL DE AJUste da apura��o';  
  
  exec inter_pr_recompile ;
