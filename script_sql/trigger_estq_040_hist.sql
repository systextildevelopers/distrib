create or replace trigger trigger_estq_040_hist
BEFORE UPDATE or DELETE or INSERT on estq_040
for each row

declare
   ws_nivel                varchar2(1);
   ws_grupo                varchar2(5);
   ws_sub                  varchar2(3);
   ws_item                 varchar2(6);
   ws_lote                 number(6);
   ws_deposito             number(3);
   ws_qtde_estq_old        number(14,3);
   ws_qtde_estq_atu        number(14,3);
   ws_qtde_estq_ant_old    number(14,3);
   ws_qtde_estq_ant_atu    number(14,3);
   ws_qtde_estq_mes_old    number(14,3);
   ws_qtde_estq_mes_atu    number(14,3);
   ws_qtde_estq_emp_old    number(14,3);
   ws_qtde_estq_emp_atu    number(14,3);
   ws_qtde_sugerida_old    number(14,3);
   ws_qtde_sugerida_atu    number(14,3);
   ws_tipo_ocorr           char(1);
   ws_nome_programa        varchar2(15);
   ws_data_entrada_old     date;
   ws_data_entrada_atu     date;
   ws_data_saida_old       date;
   ws_data_saida_atu       date;
   ws_usuario_rede         varchar2(20);
   ws_maquina_rede         varchar2(40);
   ws_aplicacao            varchar2(20);
begin
   if INSERTING then
      ws_nivel               := :new.cditem_nivel99;
      ws_grupo               := :new.cditem_grupo;
      ws_sub                 := :new.cditem_subgrupo;
      ws_item                := :new.cditem_item;
      ws_lote                := :new.lote_acomp;
      ws_deposito            := :new.deposito;
      ws_qtde_estq_old       := 0;
      ws_qtde_estq_atu       := :new.qtde_estoque_atu;
      ws_qtde_estq_ant_old   := 0;
      ws_qtde_estq_ant_atu   := :new.qtde_estoque_ant;
      ws_qtde_estq_mes_old   := 0;
      ws_qtde_estq_mes_atu   := :new.qtde_estoque_mes;
      ws_qtde_estq_emp_old   := 0;
      ws_qtde_estq_emp_atu   := :new.qtde_empenhada;
      ws_qtde_sugerida_old   := 0;
      ws_qtde_sugerida_atu   := :new.qtde_sugerida;
      ws_tipo_ocorr          := 'I';
      ws_nome_programa       := :new.nome_prog_040;
      ws_data_entrada_old    := null;
      ws_data_entrada_atu    := :new.data_ult_entrada;
      ws_data_saida_old      := null;
      ws_data_saida_atu      := :new.data_ult_saida;
   elsif UPDATING then
         ws_nivel               := :old.cditem_nivel99;
         ws_grupo               := :old.cditem_grupo;
         ws_sub                 := :old.cditem_subgrupo;
         ws_item                := :old.cditem_item;
         ws_lote                := :old.lote_acomp;
         ws_deposito            := :old.deposito;
         ws_qtde_estq_old       := :old.qtde_estoque_atu;
         ws_qtde_estq_atu       := :new.qtde_estoque_atu;
         ws_qtde_estq_ant_old   := :old.qtde_estoque_ant;
         ws_qtde_estq_ant_atu   := :new.qtde_estoque_ant;
         ws_qtde_estq_mes_old   := :old.qtde_estoque_mes;
         ws_qtde_estq_mes_atu   := :new.qtde_estoque_mes;
         ws_qtde_estq_emp_old   := :old.qtde_empenhada;
         ws_qtde_estq_emp_atu   := :new.qtde_empenhada;
         ws_qtde_sugerida_old   := :old.qtde_sugerida;
         ws_qtde_sugerida_atu   := :new.qtde_sugerida;
         ws_tipo_ocorr          := 'A';
         ws_nome_programa       := :new.nome_prog_040;
         ws_data_entrada_old    := :old.data_ult_entrada;
         ws_data_entrada_atu    := :new.data_ult_entrada;
         ws_data_saida_old      := :old.data_ult_saida;
         ws_data_saida_atu      := :new.data_ult_saida;
      elsif DELETING then
            ws_nivel               := :old.cditem_nivel99;
            ws_grupo               := :old.cditem_grupo;
            ws_sub                 := :old.cditem_subgrupo;
            ws_item                := :old.cditem_item;
            ws_lote                := :old.lote_acomp;
            ws_deposito            := :old.deposito;
            ws_qtde_estq_old       := :old.qtde_estoque_atu;
            ws_qtde_estq_atu       := 0;
            ws_qtde_estq_ant_old   := :old.qtde_estoque_ant;
            ws_qtde_estq_ant_atu   := 0;
            ws_qtde_estq_mes_old   := :old.qtde_estoque_mes;
            ws_qtde_estq_mes_atu   := 0;
            ws_qtde_estq_emp_old   := :old.qtde_empenhada;
            ws_qtde_estq_emp_atu   := 0;
            ws_qtde_sugerida_old   := :old.qtde_sugerida;
            ws_qtde_sugerida_atu   := 0;
            ws_tipo_ocorr          := 'D';
            ws_nome_programa       := :old.nome_prog_040;
            ws_data_entrada_old    := :old.data_ult_entrada;
            ws_data_entrada_atu    := null;
            ws_data_saida_old      := :old.data_ult_saida;
            ws_data_saida_atu      := null;
   end if;
      
   select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
   into   ws_usuario_rede,     ws_maquina_rede,      ws_aplicacao 
   from sys.v_$session
   where audsid = userenv('SESSIONID')
     and rownum=1;      
      
   INSERT INTO estq_040_hist
     (nivel,                grupo,                sub,
      item,                 lote,                 deposito,
      qtde_estq_old,        qtde_estq_atu,        data_ocorr,
      tipo_ocorr,           nome_programa,        qtde_estq_ant_old,
      qtde_estq_ant_atu,    qtde_estq_mes_old,    qtde_estq_mes_atu,
      qtde_estq_emp_old,    qtde_estq_emp_atu,    qtde_sugerida_old,
      qtde_sugerida_atu,    data_entrada_old,     data_entrada_atu,
      data_saida_old,       data_saida_atu,       usuario_rede,
      maquina_rede,         aplicacao) 
   VALUES
     (ws_nivel,             ws_grupo,             ws_sub,
      ws_item,              ws_lote,              ws_deposito,
      ws_qtde_estq_old,     ws_qtde_estq_atu,     sysdate,
      ws_tipo_ocorr,        ws_nome_programa,     ws_qtde_estq_ant_old,
      ws_qtde_estq_ant_atu, ws_qtde_estq_mes_old, ws_qtde_estq_mes_atu,
      ws_qtde_estq_emp_old, ws_qtde_estq_emp_atu, ws_qtde_sugerida_old,
      ws_qtde_sugerida_atu, ws_data_entrada_old,  ws_data_entrada_atu,
      ws_data_saida_old,    ws_data_saida_atu,    ws_usuario_rede,
      ws_maquina_rede,      ws_aplicacao);
   
   if INSERTING or UPDATING then
      :new.nome_prog_040 := '';
   end if;

end trigger_estq_040_hist;
/

/* versao: 1 */
