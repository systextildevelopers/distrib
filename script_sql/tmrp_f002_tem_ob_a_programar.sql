
  CREATE OR REPLACE FUNCTION "TMRP_F002_TEM_OB_A_PROGRAMAR" 
        (f_nome_programa    varchar2,
         f_codigo_usuario   number,
         f_nr_solicitacao   number,
         f_codigo_empresa   number,
         f_usuario          varchar2,
         f_selecionado      number)
RETURN string
IS
   contador   number;

BEGIN
   /*
      VERIFCA SE TEM OB A PROGRAMAR NO SUB-FORM (ob_a_prog)
   */
   select nvl(count(1),0)
   into contador
   from tmrp_615
   where tmrp_615.nome_programa    = f_nome_programa
     and tmrp_615.codigo_usuario   = f_codigo_usuario
     and tmrp_615.nr_solicitacao   = f_nr_solicitacao
     and tmrp_615.codigo_empresa   = f_codigo_empresa
     and tmrp_615.usuario          = f_usuario
     and tmrp_615.situacao         = 'X'   /* NAO CALCULADO */
     and tmrp_615.tipo_registro    = 568   /* OB PENDENTES A RESOLVER */
     and tmrp_615.selecionado2     = f_selecionado;
   if contador =  0
   then return('N');
   else return('S');
   end if;

END tmrp_f002_tem_ob_a_programar;

 

/

exec inter_pr_recompile;

