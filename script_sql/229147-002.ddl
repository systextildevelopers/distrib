declare tmpInt number;
cursor parametro_c is select codigo_empresa from fatu_500;
begin
  for reg_parametro in parametro_c
  loop
      begin
        insert into empr_008 ( 
		   codigo_empresa, param, val_int
		) values (
		   reg_parametro.codigo_empresa, 'OrdemNaoAprovadaQualidade', '0');
      end;
  end loop;
  commit;
end;
