create table blocok_291 (
	COD_EMPRESA             		number(3),
	ANO_PERIODO_APUR        		number(4),
	MES_PERIODO_APUR        		number(2),
	INF_ORIG                		varchar2(200),

	ORIGEM                  		varchar2(20),
	ORDEM_PRODUCAO          		number(9),
	PERIODO                 		number(4),
	PACOTE                  		number(5),
	ESTAGIO_AGRUPADOR       		number(2),

	COD_ITEM                		varchar2(60),
	QTD                     		number(15,6),

	COD_ITEM_NIVEL                  varchar2(1),
	COD_ITEM_GRUPO                  varchar2(5),
	COD_ITEM_SUBGRUPO               varchar2(3),
	COD_ITEM_ITEM                   varchar2(6),
	COD_ITEM_EST_AGRUP              number(2),
	COD_ITEM_EST_AGRUP_SIMULT       number(2),
	
	data_hora_ins           		date            default sysdate,
	
	cod_doc_op_orig 				varchar2(30)
);
