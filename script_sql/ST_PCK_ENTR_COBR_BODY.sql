create or replace package body "ST_PCK_ENTR_COBR" is

    FUNCTION get_dados_end_entr_cobr(p_cgc_9 number, p_cgc_4 number, p_cgc_2 number, p_tipo_end number, p_seq_end number, p_msg_erro in out varchar2) return t_dados_pedi_150
    AS 
        v_dados_pedi_150 t_dados_pedi_150;
    BEGIN
    
            begin
            SELECT *  
            BULK COLLECT 
            INTO v_dados_pedi_150
            FROM pedi_150
            WHERE  PEDI_150.CD_CLI_CGC_CLI9 = p_cgc_9
            AND PEDI_150.CD_CLI_CGC_CLI4 = p_cgc_4
            AND PEDI_150.CD_CLI_CGC_CLI2 = p_cgc_2
            AND PEDI_150.SEQ_ENDERECO    = p_seq_end 
            AND PEDI_150.TIPO_ENDERECO   = p_tipo_end;


        exception when others then
            p_msg_erro := 'Não foi possível buscar os dados do endereço! p_tipo_end: ' || p_tipo_end || ' p_seq_end: ' || p_seq_end || ' p_cgc_9: ' || p_cgc_9 || ' p_cgc_4: ' || p_cgc_4 || ' p_cgc_2: ' || p_cgc_2;
            return null;
        end;

        return v_dados_pedi_150;
    end;

end "ST_PCK_ENTR_COBR";
