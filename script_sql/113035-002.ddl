alter table pedi_080 
add exige_centro_de_custo number(1);

alter table pedi_080 
modify exige_centro_de_custo default 0;

declare
nro_registro number;
begin
  nro_registro := 0;

  for reg in (select rowid
              from pedi_080
              where exige_centro_de_custo is null)
  loop
    update pedi_080
      set exige_centro_de_custo = 0
    where rowid = reg.rowid;

    nro_registro := nro_registro + 1;

    if nro_registro > 1000
    then
       nro_registro := 0;
       commit;
    end if;
  end loop;

  commit;

end;

/

exec inter_pr_recompile;












