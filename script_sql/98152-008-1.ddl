alter table basi_180 add (
codigo_responsavel          number(5) default  0   null);

comment on column basi_180.codigo_responsavel 
is 'Responsável por atender a feccao.';

exec inter_pr_recompile;
