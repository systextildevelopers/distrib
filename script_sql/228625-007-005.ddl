create or replace procedure inter_pr_insere_fatu_070 (
	p_codigo_empresa in fatu_070.codigo_empresa%type,
	p_cli_dup_cgc_cli9 in fatu_070.cli_dup_cgc_cli9%type,
	p_cli_dup_cgc_cli4 in fatu_070.cli_dup_cgc_cli4%type, 
	p_cli_dup_cgc_cli2 in fatu_070.cli_dup_cgc_cli9%type,
	p_tipo_titulo in fatu_070.tipo_titulo%type,
	p_num_duplicata in fatu_070.num_duplicata%type,
	p_seq_duplicatas in fatu_070.seq_duplicatas%type,
	p_data_venc_duplic in fatu_070.data_venc_duplic%type,
	p_valor_duplicata in fatu_070.valor_duplicata%type,
	p_cod_rep_cliente in fatu_070.cod_rep_cliente%type,
	p_data_emissao in fatu_070.data_emissao%type,
	p_numero_titulo in fatu_070.numero_titulo%type,
	p_numero_sequencia in fatu_070.numero_sequencia%type,
	p_cod_historico in fatu_070.cod_historico%type,
	p_cod_local in fatu_070.cod_local%type,
	p_port_anterior in fatu_070.port_anterior%type,
	p_portador_duplic in fatu_070.portador_duplic%type,
	p_posicao_duplic in fatu_070.posicao_duplic%type,
	p_data_prorrogacao in fatu_070.data_prorrogacao%type,
	p_tecido_peca in fatu_070.tecido_peca%type,
	p_seq_end_cobranca in fatu_070.seq_end_cobranca%type,
	p_comissao_lancada in fatu_070.comissao_lancada%type,
	p_tipo_tit_origem in fatu_070.tipo_tit_origem%type,
	p_num_dup_origem in fatu_070.num_dup_origem%type,
	p_seq_dup_origem in fatu_070.seq_dup_origem%type,
	p_cli9resptit in fatu_070.cli9resptit%type,
	p_cli4resptit in fatu_070.cli4resptit%type,
	p_cli2resptit in fatu_070.cli2resptit%type,
	p_origem_pedido in fatu_070.origem_pedido%type,
	p_cod_transacao in fatu_070.cod_transacao%type,
	p_codigo_contabil in fatu_070.codigo_contabil%type,
	p_valor_moeda in fatu_070.valor_moeda%type,
	p_nr_identificacao in fatu_070.nr_identificacao%type,
	p_conta_corrente in fatu_070.conta_corrente%type,
	p_num_contabil in fatu_070.num_contabil%type,
	p_duplic_emitida in fatu_070.duplic_emitida%type,
	p_pedido_venda in fatu_070.pedido_venda%type,
  p_compl_historico in fatu_070.compl_historico%type,
   r_des_erro out varchar2
   )
is   
   v_data_pagamento date;
   v_data_credito   date;
begin
   BEGIN
   insert into fatu_070 ( 
        codigo_empresa,   cli_dup_cgc_cli9, 
        cli_dup_cgc_cli4, cli_dup_cgc_cli2, 
        tipo_titulo,      num_duplicata, 
        seq_duplicatas,   data_venc_duplic, 
        valor_duplicata,  cod_rep_cliente, 
        data_emissao,     numero_titulo, 
        numero_sequencia, cod_historico, 
        cod_local,        port_anterior, 
        portador_duplic,  posicao_duplic, 
        data_prorrogacao, tecido_peca, 
        seq_end_cobranca, comissao_lancada, 
        tipo_tit_origem,  num_dup_origem, 
        seq_dup_origem,   cli9resptit, 
        cli4resptit,      cli2resptit, 
        origem_pedido,    cod_transacao, 
        codigo_contabil,  valor_moeda, 
        nr_identificacao, conta_corrente, 
        num_contabil,     duplic_emitida, 
        pedido_venda,     compl_historico 
    ) values (
        p_codigo_empresa, p_cli_dup_cgc_cli9, 
        p_cli_dup_cgc_cli4, p_cli_dup_cgc_cli2, 
        p_tipo_titulo, p_num_duplicata, 
        p_seq_duplicatas, p_data_venc_duplic, 
        p_valor_duplicata, p_cod_rep_cliente, 
        p_data_emissao, p_numero_titulo, 
        p_numero_sequencia, p_cod_historico, 
        p_cod_local, p_port_anterior, 
        p_portador_duplic, p_posicao_duplic, 
        p_data_prorrogacao, p_tecido_peca, 
        p_seq_end_cobranca, p_comissao_lancada, 
        p_tipo_tit_origem, p_num_dup_origem, 
        p_seq_dup_origem, p_cli9resptit, 
        p_cli4resptit, p_cli2resptit, 
        p_origem_pedido, p_cod_transacao, 
        p_codigo_contabil, p_valor_moeda, 
        p_nr_identificacao, p_conta_corrente, 
        p_num_contabil, p_duplic_emitida, 
        p_pedido_venda, p_compl_historico
    );
        EXCEPTION WHEN OTHERS THEN
           if sqlcode <> -1
           then
              r_des_erro := 'Erro ao inserir baixa de titulos - fatu_070' || Chr(10) || SQLERRM;
           end if;
    END;
end inter_pr_insere_fatu_070;




/

exec inter_pr_recompile;

