
  CREATE OR REPLACE TRIGGER "INTER_TR_I_BASI_100" 
before insert on i_basi_100
for each row

declare
  v_id_registro number;
begin
   select SEQ_IMPORTACAO.nextval into v_id_registro from dual;
   :new.id_registro     := v_id_registro;
end inter_tr_i_basi_100;
-- ALTER TRIGGER "INTER_TR_I_BASI_100" DISABLE
 

/

exec inter_pr_recompile;

