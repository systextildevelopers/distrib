alter table OBRF_823
  drop constraint PK_OBRF_823;
 
drop index PK_OBRF_823;

alter table OBRF_823 add constraint PK_OBRF_823 primary key (COD_EMPRESA, MES, ANO, ID, COD_MENSAGEM, COD_AJUSTE53, NOTA, SERIE, CNPJ9, CNPJ4, CNPJ2, NIVEL, GRUPO, SUBGRUPO, ITEM, COD_AJUSTE,icms_perc) NOVALIDATE;
