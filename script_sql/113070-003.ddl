create table OBRF_830  
( 
  empresa           number(3)    	default 0,
  mes 	            number(2)	 	default 0,
  ano	            number(4)	 	default 0,
  contab            number(5)	 	default 0,
  cfop	            varchar2(5)	 	default 0,
  cst	            number(5)		default 0,
  valor_contabil	number(12,2) 	default 0,
  base_calculo		number(12,2) 	default 0,
  valor_imposto 	number(12,2) 	default 0,
  base_isento		number(12,2) 	default 0, 
  base_outras		number(12,2) 	default 0,
  ind_entrada_saida	varchar2(1) 	default '',
  descr_natureza	varchar2(45) 	default '',
  basedif			number(12,2)	default 0,
  cvfdif			number(12,2)	default 0
);
 
alter table OBRF_830 add constraint pk_obrf_830 primary key (empresa,mes,ano,contab,cfop);
/
