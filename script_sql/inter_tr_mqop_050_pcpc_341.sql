
  CREATE OR REPLACE TRIGGER "INTER_TR_MQOP_050_PCPC_341" 
before insert
       or update of minutos, centro_custo
on mqop_050
for each row

declare

   v_tem_pcpc_341 number(1);
   linha number(2);
   
   v_sid           number(9);
   v_osuser        varchar2(20);
   v_nome_usuario  varchar2(20);

   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_empresa                number(3);
   ws_locale_usuario         varchar2(5);
   v_origem_valor_operacao   number(1);
   
   function get_linha(nivel varchar2, grupo varchar2) return number is
       linha number(2);
     begin
       select linha_produto INTO linha from basi_030
       where nivel_estrutura  = nivel
         and basi_030.referencia = grupo;
       return linha;
     end;
   
begin
   inter_pr_dados_usuario (v_osuser,        ws_maquina_rede,   ws_aplicativo,     v_sid,
						   v_nome_usuario,  ws_empresa,        ws_locale_usuario);
   v_tem_pcpc_341 := 0;
   
   	begin
		select origem_valor_operacao
		INTO v_origem_valor_operacao
		from fatu_501
		where codigo_empresa = ws_empresa;
	exception when others then
		v_origem_valor_operacao := 0;
	end;

   begin
      select 1
      into v_tem_pcpc_341
      from pcpc_341
      where pcpc_341.nivel_rot      = :new.nivel_estrutura
        and pcpc_341.referencia_rot = :new.grupo_estrutura
        and pcpc_341.subgrupo_rot   = :new.subgru_estrutura
        and pcpc_341.item_rot       = :new.item_estrutura
        and pcpc_341.alt_rot        = :new.numero_alternati
        and pcpc_341.rot_rot        = :new.numero_roteiro
        and pcpc_341.seq_operacao   = :new.seq_operacao
        and rownum                  = 1;
      exception
         when no_data_found then
            v_tem_pcpc_341 := 0;
   end;

   if v_tem_pcpc_341 = 1
   then

      if inserting or (updating and :old.minutos <> :new.minutos)
      then

         update pcpc_341
            set pcpc_341.tempo = :new.minutos
          where pcpc_341.nivel_rot      = :new.nivel_estrutura
            and pcpc_341.referencia_rot = :new.grupo_estrutura
            and pcpc_341.subgrupo_rot   = :new.subgru_estrutura
            and pcpc_341.item_rot       = :new.item_estrutura
            and pcpc_341.alt_rot        = :new.numero_alternati
            and pcpc_341.rot_rot        = :new.numero_roteiro
            and pcpc_341.seq_operacao   = :new.seq_operacao;

      end if;

      if inserting or (updating and ((:old.centro_custo <> :new.centro_custo and v_origem_valor_operacao = 0)
                                    or (:old.codigo_estagio <> :new.codigo_estagio)))
      then

         linha := get_linha(:new.nivel_estrutura, :new.grupo_estrutura);

         update pcpc_341
            set pcpc_341.valor_minuto = 
DECODE(v_origem_valor_operacao, 0, 
  (select custo_minuto_previsto from basi_185 where centro_custo = :new.centro_custo)
, inter_fn_custo_minuto(pcpc_341.cnpj9_terceiro, pcpc_341.cnpj4_terceiro, pcpc_341.cnpj2_terceiro, linha)
)
          where pcpc_341.nivel_rot      = :new.nivel_estrutura
            and pcpc_341.referencia_rot = :new.grupo_estrutura
            and pcpc_341.subgrupo_rot   = :new.subgru_estrutura
            and pcpc_341.item_rot       = :new.item_estrutura
            and pcpc_341.alt_rot        = :new.numero_alternati
            and pcpc_341.rot_rot        = :new.numero_roteiro
            and pcpc_341.seq_operacao   = :new.seq_operacao
            and exists (
                select 1 from basi_400
                where basi_400.tipo_informacao = 500
                  and basi_400.codigo_informacao = :new.codigo_estagio
                  and basi_400.sequencia = pcpc_341.cnpj9_terceiro
                  and basi_400.grupo     = to_char(pcpc_341.cnpj4_terceiro)
                  and basi_400.subgrupo  = to_char(pcpc_341.cnpj2_terceiro)
                  and basi_400.item      = to_char(pcpc_341.codigo_servico)
                  and basi_400.string_01 = 'pcpc_f340'
            );

      end if;

   else
      if inserting then
      
         linha := get_linha(:new.nivel_estrutura, :new.grupo_estrutura);

         for p in (select distinct
             cnpj9_terceiro, cnpj4_terceiro, cnpj2_terceiro, codigo_servico,
             nivel, referencia, subgrupo, item,
             estagio, selecao, ordenacao
            from pcpc_341
            where pcpc_341.nivel_rot      = :new.nivel_estrutura
              and pcpc_341.referencia_rot = :new.grupo_estrutura
              and pcpc_341.subgrupo_rot   = :new.subgru_estrutura
              and pcpc_341.item_rot       = :new.item_estrutura
              and pcpc_341.alt_rot        = :new.numero_alternati
              and pcpc_341.rot_rot        = :new.numero_roteiro
              and exists (
                select 1 from basi_400
                where basi_400.tipo_informacao = 500
                  and basi_400.codigo_informacao = :new.codigo_estagio
                  and basi_400.sequencia = pcpc_341.cnpj9_terceiro
                  and basi_400.grupo     = to_char(pcpc_341.cnpj4_terceiro)
                  and basi_400.subgrupo  = to_char(pcpc_341.cnpj2_terceiro)
                  and basi_400.item      = to_char(pcpc_341.codigo_servico)
                  and basi_400.string_01 = 'pcpc_f340'
              )
            )
         loop
         
            insert into pcpc_341 (
            cnpj9_terceiro, cnpj4_terceiro, cnpj2_terceiro, codigo_servico,
            nivel, referencia, subgrupo, item,
            sequencia, estagio, operacao, tempo, selecao,
            nivel_rot, referencia_rot, subgrupo_rot, item_rot, alt_rot,
            rot_rot, seq_operacao, ordenacao, valor_minuto)
            values (
            p.cnpj9_terceiro, p.cnpj4_terceiro, p.cnpj2_terceiro, p.codigo_servico,
            p.nivel, p.referencia, p.subgrupo, p.item,
            (select max(x.sequencia)+1 from pcpc_341 x
             where x.cnpj9_terceiro = p.cnpj9_terceiro
               and x.cnpj4_terceiro = p.cnpj4_terceiro
               and x.cnpj2_terceiro = p.cnpj2_terceiro
               and x.codigo_servico = p.codigo_servico
               and x.nivel          = p.nivel
               and x.referencia     = p.referencia
               and x.subgrupo       = p.subgrupo
               and x.item           = p.item
               and x.alt_rot        = :new.numero_alternati
               and x.rot_rot        = :new.numero_roteiro),
            p.estagio, :new.codigo_operacao, :new.minutos, p.selecao,
            :new.nivel_estrutura, :new.grupo_estrutura, :new.subgru_estrutura, :new.item_estrutura, :new.numero_alternati,
            :new.numero_roteiro, :new.seq_operacao, p.ordenacao,
            DECODE(v_origem_valor_operacao, 0, 
                (select custo_minuto_previsto from basi_185 where centro_custo = :new.centro_custo)
              , inter_fn_custo_minuto(p.cnpj9_terceiro, p.cnpj4_terceiro, p.cnpj2_terceiro, linha)
            ));
         end loop;
         
      end if;
   end if;

end inter_tr_mqop_050_pcpc_341;

-- ALTER TRIGGER "INTER_TR_MQOP_050_PCPC_341" ENABLE
