create or replace procedure inter_pr_gera_sped_bk200(p_cod_empresa    in number,
                                                     p_dat_final      IN DATE,
                                                     p_des_erro       out varchar2) is
  w_id_k100 sped_k100.id%type;
  w_mes_ano date;
  w_erro EXCEPTION;
  
  temTpSpedK200 number;
  
  CURSOR u_sped_k200_h010(p_cod_empresa NUMBER, p_dat_final DATE) IS
    select sped_k200_h010.empresa,
           sped_k200_h010.cnpj9_participante,
           sped_k200_h010.cnpj4_participante,
           sped_k200_h010.cnpj2_participante,
           sped_k200_h010.nivel,
           sped_k200_h010.grupo,
           sped_k200_h010.subgrupo,
           sped_k200_h010.item,
           sped_k200_h010.estagio_agrupador,
           sped_k200_h010.estagio_agrupador_simultaneo,
           sum(sped_k200_h010.quantidade) quantidade,
           sped_k200_h010.tipo_propriedade,
           sped_k200_h010.sequencia_operacao_agrupador as seq_agrupador,
           sped_k200_h010.tipo_produto_sped      
      from sped_k200_h010
     where sped_k200_h010.empresa = p_cod_empresa
     and   sped_k200_h010.ano = extract(year from p_dat_final) 
     and   sped_k200_h010.mes = extract(month from p_dat_final) 
     group by sped_k200_h010.empresa,
           sped_k200_h010.cnpj9_participante,
           sped_k200_h010.cnpj4_participante,
           sped_k200_h010.cnpj2_participante,
           sped_k200_h010.nivel,
           sped_k200_h010.grupo,
           sped_k200_h010.subgrupo,
           sped_k200_h010.item,
           sped_k200_h010.estagio_agrupador,
           sped_k200_h010.estagio_agrupador_simultaneo,
           sped_k200_h010.tipo_propriedade,
           sped_k200_h010.sequencia_operacao_agrupador,
           sped_k200_h010.tipo_produto_sped          
     having sum(sped_k200_h010.quantidade) <> 0;

BEGIN

  FOR sped_k200_h010 IN u_sped_k200_h010(p_cod_empresa, p_dat_final) LOOP
     -- POR PADR?O S?O OS TIPO PRODUTO SPED 0,1,2,3,4,5,6
     SELECT count(*) into temTpSpedK200  from empr_008 
     where param          = 'obrf.tipoSpedFiscalK200'
     and codigo_empresa = p_cod_empresa
     and sped_k200_h010.tipo_produto_sped IN (
           SELECT 
             TO_NUMBER(
               REGEXP_SUBSTR(
                 val_str, '[^,]+', 1, LEVEL
               )
             ) 
           FROM DUAL CONNECT BY REGEXP_SUBSTR(
              val_str, '[^,]+', 1, LEVEL
             ) IS NOT NULL
         );
   
     if temTpSpedK200 > 0
     then
        begin
          select max(id)
            into w_id_k100
            from sped_k100
           where sped_k100.cod_empresa = p_cod_empresa
             and sped_k100.dt_fim = p_dat_final;
        end;
      
        BEGIN
          INSERT INTO sped_k200
            (id_k100,
             cod_nivel,
             cod_grupo,
             cod_subgrupo,
             cod_item,
             registro,
             qtd,
             dt_est,
             ind_est,
             cnpj_9,
             cnpj_4,
             cnpj_2,
             estagio_agrupador,
             estagio_agrupador_simultaneo,
             sequencia_operacao_agrupador)
          values
            (w_id_k100,
             sped_k200_h010.nivel,
             sped_k200_h010.grupo,
             sped_k200_h010.subgrupo,
             sped_k200_h010.item,
             'K200',
             sped_k200_h010.quantidade,
             p_dat_final,
             sped_k200_h010.tipo_propriedade,
             sped_k200_h010.cnpj9_participante,
             sped_k200_h010.cnpj4_participante,
             sped_k200_h010.cnpj2_participante,
             sped_k200_h010.estagio_agrupador,
             sped_k200_h010.estagio_agrupador_simultaneo,
             sped_k200_h010.seq_agrupador);
        EXCEPTION
          WHEN OTHERS THEN
            p_des_erro := 'Erro na inclus?o da tabela sped_k200 (1)' || Chr(10) ||
                          ' PRODUTO: ' || sped_k200_h010.nivel ||
                          sped_k200_h010.grupo || sped_k200_h010.subgrupo ||
                          sped_k200_h010.item || Chr(10) || SQLERRM;
            RAISE W_ERRO;
        END;
     end if;
    
  END LOOP;

  EXCEPTION
  WHEN W_ERRO then
    p_des_erro := 'Erro na procedure inter_pr_gera_sped_bk200 ' || Chr(10) ||
                  p_des_erro;
  WHEN OTHERS THEN
    p_des_erro := 'Outros erros na procedure inter_pr_gera_sped_bk200 ' ||
                  Chr(10) || SQLERRM;
END inter_pr_gera_sped_bk200;
/
