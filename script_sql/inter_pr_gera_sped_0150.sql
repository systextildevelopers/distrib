create or replace procedure inter_pr_gera_sped_0150 (p_tip_contabil_fiscal IN VARCHAR2,
                                                     p_cod_empresa  NUMBER,
                                                     p_tipo_layout NUMBER,
                                                     p_dat_inicial   IN  DATE,
                                                     p_dat_final     IN  DATE,
                           p_gera_sped_k200 IN varchar2,
                                                     p_des_erro    out varchar2) is
--
-- Finalidade: Gerar a tabela sped_0150 - Clientes e Fornecedores
-- Autor.....: César Anton
-- Data......: 21/11/08
--
-- Históricos
--
-- Data    Autor    Observações

w_estado                   basi_160.estado%TYPE;
w_ddd                      basi_160.ddd%TYPE;
w_codigo_cidade_ibge       number;
w_codigo_pais              basi_160.codigo_pais%TYPE;
w_cidade                   basi_160.cidade%TYPE;
w_codigo_fiscal_uf         basi_167.codigo_fiscal_uf%TYPE;
w_codigo_pais_sisc         basi_165.codigo_fiscal%TYPE;
w_cod_cid_z_franca         basi_160.cod_cidade_zona_franca%TYPE;
w_des_endereco             pedi_010.endereco_cliente%TYPE;
w_numero_imovel            pedi_010.numero_imovel%TYPE;
w_complemento_end          pedi_010.complemento%TYPE;
w_bairro                   pedi_010.bairro%TYPE;
w_cep_cliente              pedi_010.cep_cliente%TYPE;
w_cod_cidade               pedi_010.cod_cidade%TYPE;
w_estado_forn                basi_160.estado%TYPE;
w_ddd_forn                   basi_160.ddd%TYPE;
w_codigo_cidade_ibge_forn    number;
w_codigo_pais_forn           basi_160.codigo_pais%TYPE;
w_cidade_forn                basi_160.cidade%TYPE;
w_codigo_fiscal_uf_forn      basi_167.codigo_fiscal_uf%TYPE;
w_codigo_pais_sisc_forn      basi_165.codigo_fiscal%TYPE;
w_cod_cid_z_franca_forn      basi_160.cod_cidade_zona_franca%TYPE;
w_des_endereco_forn          supr_010.endereco_forne%TYPE;
w_numero_imovel_forn         supr_010.numero_imovel%TYPE;
w_complemento_end_forn       supr_010.complemento%TYPE;
w_bairro_forn                supr_010.bairro%TYPE;
w_cep_forn                supr_010.cep_fornecedor%TYPE;
w_cod_cidade_forn            supr_010.cod_cidade%TYPE;
w_tp_pessoa_fisica         pedi_010.fisica_juridica%TYPE;
w_ind_achou                varchar2(1);
w_tip_contribuinte_icms    varchar2(1);
w_erro                     EXCEPTION;
w_fornec_cliente           number;

-- INCLUSO UNION PARA PEGAR PARTICIPANTES DAS NOTAS REFERENCIADAS
CURSOR u_sped_c100 (p_cod_empresa     NUMBER) IS
   SELECT distinct cod_empresa,
          tip_entrada_saida,
          num_cnpj_9,
          num_cnpj_4,
          num_cnpj_2,
          seq_end_entrega
   FROM   ((select distinct sped_c100.cod_empresa, sped_c100.tip_entrada_saida, sped_c100.num_cnpj_9, sped_c100.num_cnpj_4, sped_c100.num_cnpj_2, sped_c100.seq_end_entrega
           from sped_c100
           WHERE   sped_c100.cod_empresa     = p_cod_empresa
             and   sped_c100.tip_nota_cancel = 'N'
             and   sped_c100.cod_serie_nota <> 'ECF'
             and   sped_c100.cod_serie_nota <> 'CF'
             and   sped_c100.cod_modelo     not in ('02', '65', '59')
             and   sped_c100.flag_exp        = 'N')
   union all
           (select distinct sped_c100.cod_empresa, 'E', sped_c100.num_cnpj_9_transp, sped_c100.num_cnpj_4_transp, sped_c100.num_cnpj_2_transp, sped_c100.seq_end_entrega
            from sped_c100, fatu_504
            WHERE sped_c100.cod_empresa        = p_cod_empresa
              and fatu_504.codigo_empresa      = sped_c100.cod_empresa
              and sped_c100.tip_nota_cancel    = 'N'
              and sped_c100.num_cnpj_9_transp  > 0
              and (sped_c100.cod_modelo        not in ('55', '65', '59') or fatu_504.tipo_sped = 1)
              and sped_c100.tip_entrada_saida <> 'E'
              and sped_c100.cod_serie_nota <> 'ECF'
              and sped_c100.cod_serie_nota <> 'CF'
              and sped_c100.flag_exp           = 'N')
   union all
          (select distinct sped_c170.cod_empresa, 'E', sped_c170.num_cnpj9_refer , sped_c170.num_cnpj4_refer, sped_c170.num_cnpj2_refer, sped_c100.seq_end_entrega
           from sped_c170,
                sped_c100,
                fatu_504
           WHERE   sped_c170.cod_empresa     = p_cod_empresa
             and   fatu_504.codigo_empresa   = sped_c100.cod_empresa
             and   sped_c170.num_cnpj9_refer > 0
             and   sped_c170.cod_empresa     = sped_c100.cod_empresa
             and   sped_c170.num_nota_fiscal = sped_c100.num_nota_fiscal
             and   sped_c170.cod_serie_nota  = sped_c100.cod_serie_nota
             and   sped_c170.num_cnpj_9      = sped_c100.num_cnpj_9
             and   sped_c170.num_cnpj_4      = sped_c100.num_cnpj_4
             and   sped_c170.num_cnpj_2      = sped_c100.num_cnpj_2
             and   (sped_c100.cod_modelo        not in ('55', '65', '59') or fatu_504.tipo_sped = 1)
             and   sped_c100.tip_entrada_saida <> 'E'
             and   sped_c100.tip_nota_cancel = 'N'
             and   sped_c100.flag_exp        = 'N')
--logica adicionada para verificacao de participantes exportados no CIAP ( allan )--
   union all
           (select distinct obrf_130.cod_empresa, 'E',
                           obrf_130.cgc9, obrf_130.cgc4,
                           obrf_130.cgc2, 0
            from  obrf_130
            where obrf_130.cod_empresa  = p_cod_empresa
              and obrf_130.periodo_ini  = p_dat_inicial
              and obrf_130.periodo_fim  = p_dat_final)
   union all
          (select distinct obrf_195.cod_empresa, 'E', obrf_195.cnpj_9, obrf_195.cnpj_4, obrf_195.cnpj_2, 0
           from obrf_195
           where obrf_195.cod_empresa = p_cod_empresa
             and obrf_195.mes_per    = to_char(p_dat_inicial, 'MM')
             and obrf_195.ano_per    = to_char(p_dat_inicial, 'YYYY'))
   union all
          (select distinct sped_1601.cod_empresa, 'E', sped_1601.part_ip_cnpj9,sped_1601.part_ip_cnpj4,sped_1601.part_ip_cnpj2, 0
           from sped_1601
           where sped_1601.cod_empresa = p_cod_empresa
             and sped_1601.mes    = to_char(p_dat_inicial, 'MM')
             and sped_1601.ano    = to_char(p_dat_inicial, 'YYYY')
             and (sped_1601.part_ip_cnpj9 > 0 or sped_1601.part_ip_cnpj4 > 0 or sped_1601.part_ip_cnpj2 > 0))
   union all
          (select distinct sped_1601.cod_empresa, 'E', sped_1601.part_it_cnpj9,sped_1601.part_it_cnpj4,sped_1601.part_it_cnpj2, 0
           from sped_1601
           where sped_1601.cod_empresa = p_cod_empresa
             and sped_1601.mes    = to_char(p_dat_inicial, 'MM')
             and sped_1601.ano    = to_char(p_dat_inicial, 'YYYY')
             and (sped_1601.part_it_cnpj9 > 0 or sped_1601.part_it_cnpj4 > 0 or sped_1601.part_it_cnpj2 > 0))

   union all
         (select distinct obrf_743.cod_empresa, decode(obrf_743.ind_entr_said,0, 'E','S') tip_entrada_saida, 
                 obrf_743.cnpj9,obrf_743.cnpj4,obrf_743.cnpj2, 0
          from obrf_743
          where obrf_743.cod_empresa = p_cod_empresa
            and obrf_743.mes         = to_char(p_dat_inicial, 'MM')
            and obrf_743.ano         = to_char(p_dat_inicial, 'YYYY'))             
   union all
         (select distinct sped_h010.cod_empresa, 'E', sped_h010.cnpj9, sped_h010.cnpj4,  sped_h010.cnpj2, 0
          from sped_h010
          where sped_h010.cod_empresa = p_cod_empresa
            and sped_h010.cnpj9       > 0)
union all
     (select distinct sped_k100.cod_empresa, 'E', sped_k200.cnpj_9, sped_k200.cnpj_4, 
                    sped_k200.cnpj_2, 0     
    from sped_k100, sped_k200      
    where sped_k100.cod_empresa = p_cod_empresa
            and sped_k100.dt_ini      = p_dat_inicial
      and sped_k100.id = sped_k200.id_k100
     and (sped_k200.cnpj_9 + sped_k200.cnpj_4 + sped_k200.cnpj_2) > 0
      and p_gera_sped_k200 <> 'N')
   union all
   (select distinct sped_c100.cod_empresa, 'E', fatu_060.cgc9_origem, fatu_060.cgc4_origem,
                    fatu_060.cgc2_origem, 0
    from fatu_060
    left join sped_c100
      on fatu_060.ch_it_nf_cd_empr = sped_c100.cod_empresa
     and fatu_060.ch_it_nf_num_nfis = sped_c100.num_nota_fiscal
     and fatu_060.ch_it_nf_ser_nfis = sped_c100.cod_serie_nota
   where fatu_060.ch_it_nf_cd_empr = p_cod_empresa
     and sped_c100.dat_emissao between p_dat_inicial and p_dat_final)
   )
   ORDER BY tip_entrada_saida DESC;


CURSOR u_pedi_010 (p_num_cnpj_9         number,
                   p_num_cnpj_4         NUMBER,
                   p_num_cnpj_2         NUMBER) IS
   SELECT *
   FROM   pedi_010
   WHERE  cgc_9   = p_num_cnpj_9
   AND    cgc_4   = p_num_cnpj_4
   AND    cgc_2   = p_num_cnpj_2;


CURSOR u_supr_010 (p_num_cnpj_9         number,
                   p_num_cnpj_4         NUMBER,
                   p_num_cnpj_2         NUMBER) IS
   SELECT *
   FROM   supr_010
   WHERE  fornecedor9   = p_num_cnpj_9
   AND    fornecedor4   = p_num_cnpj_4
   AND    fornecedor2   = p_num_cnpj_2;


BEGIN
   p_des_erro := NULL;

   FOR sped_c100 IN u_sped_c100 (p_cod_empresa)
   LOOP
       w_ind_achou := 'N';

       -- o documento é de saida (clientes)
       IF sped_c100.tip_entrada_saida = 'S'
       THEN

          -- le as informações do cliente
          FOR pedi_010 IN u_pedi_010 (sped_c100.num_cnpj_9, sped_c100.num_cnpj_4, sped_c100.num_cnpj_2)
          LOOP

              w_ind_achou := 'S';

              -- le os dados da cidade, estado e pais do cliente
              BEGIN
                 SELECT basi_160.estado,                 basi_160.ddd,                to_number(to_char(basi_160.codigo_fiscal,'00000')),
                        basi_160.codigo_pais,            basi_160.cidade,             basi_167.codigo_fiscal_uf,
                        basi_165.codigo_fiscal,          basi_160.cod_cidade_zona_franca
                 INTO   w_estado,                        w_ddd,                       w_codigo_cidade_ibge,
                        w_codigo_pais,                   w_cidade,                    w_codigo_fiscal_uf,
                        w_codigo_pais_sisc,              w_cod_cid_z_franca
                 from   basi_160,
                        basi_167,
                        basi_165
                 where pedi_010.cod_cidade  = basi_160.cod_cidade
                 and   basi_160.estado      = basi_167.estado
                 and   basi_160.codigo_pais = basi_167.codigo_pais
                 and   basi_160.codigo_pais = basi_165.codigo_pais;
         
         w_des_endereco     := pedi_010.endereco_cliente;
                 w_numero_imovel    := pedi_010.numero_imovel;
                 w_complemento_end  := pedi_010.complemento;
                 w_bairro           := pedi_010.bairro;
                 w_cep_cliente      := pedi_010.cep_cliente;
                 w_cod_cidade       := pedi_010.cod_cidade;
         
              IF sped_c100.seq_end_entrega != 0
              THEN  
        
                 SELECT pedi_150.end_entr_cobr,           pedi_150.numero_imovel,     pedi_150.complemento_endereco,
                        pedi_150.bairro_entr_cobr,        pedi_150.cep_entr_cobr,     pedi_150.cid_entr_cobr
                 INTO   w_des_endereco,                   w_numero_imovel,            w_complemento_end,
                        w_bairro,                         w_cep_cliente,              w_cod_cidade
                 from   pedi_150
                 where pedi_150.cd_cli_cgc_cli9 = sped_c100.num_cnpj_9
                 and   pedi_150.cd_cli_cgc_cli4 = sped_c100.num_cnpj_4
                 and   pedi_150.cd_cli_cgc_cli2 = sped_c100.num_cnpj_2
                 and   pedi_150.tipo_endereco   = 1
                 and   pedi_150.seq_endereco    = sped_c100.seq_end_entrega;
         
                 SELECT basi_160.estado,                     basi_160.ddd,                                to_number(to_char(basi_160.codigo_fiscal,'00000')),
                        basi_160.codigo_pais,                basi_160.cidade,                             basi_167.codigo_fiscal_uf,
                        basi_165.codigo_fiscal,              basi_160.cod_cidade_zona_franca
                 INTO   w_estado,                            w_ddd,                                       w_codigo_cidade_ibge,
                        w_codigo_pais,                  w_cidade,                                    w_codigo_fiscal_uf,
                        w_codigo_pais_sisc,          w_cod_cid_z_franca
                 from  basi_160,
                       basi_167,
                       basi_165
                 where w_cod_cidade         = basi_160.cod_cidade
                 and   basi_160.estado      = basi_167.estado
                 and   basi_160.codigo_pais = basi_167.codigo_pais
                 and   basi_160.codigo_pais = basi_165.codigo_pais;
         
              END IF;
              
              EXCEPTION
                 WHEN OTHERS THEN
                    w_estado             := NULL;
                    w_ddd                := NULL;
                    w_codigo_pais_sisc   := NULL;
                    w_codigo_pais        := NULL;
                    w_cidade             := NULL;
                    w_codigo_fiscal_uf   := NULL;
                    w_codigo_cidade_ibge := NULL;

                    inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                        'Erro na leitura das informações de cidade, estado e pais do cliente ' || Chr(10) ||
                                        '   Cliente: ' || sped_c100.num_cnpj_9 || '/' || LPad(sped_c100.num_cnpj_4,4,0) ||
                                       '-' || LPad(sped_c100.num_cnpj_2,2,0));
              END;

              -- verifica o tamanho do campo do número do suframa. Se for maior que 9 gera erro.
              IF  Length(trim(pedi_010.nr_suframa_cli)) > 9
              THEN

                  inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                      'Procedure: p_gera_sped_0150 ' || Chr(10) ||
                                      'Tamanho do campo numero da suframa é maior que o permitido ' || Chr(10) ||
                                      '   Cliente: ' || sped_c100.num_cnpj_9 || '/' || LPad(sped_c100.num_cnpj_4,4,0) || '-' ||
                                                        LPad(sped_c100.num_cnpj_2,2,0) || Chr(10) ||
                                      '   Suframa: ' || pedi_010.nr_suframa_cli);

                  pedi_010.nr_suframa_cli := ' ';
              END IF;

              -- se o cliente for ISENTO, seta as variaveis como não contribuionte e anula o campo da inscricao
              BEGIN

                 if trim(pedi_010.insc_est_cliente) = 'ISENTO'
                 then
                    w_tip_contribuinte_icms    := 'N';
                    pedi_010.insc_est_cliente  := null;
                 else
                    w_tip_contribuinte_icms := 'S';
                 end if;

                 if w_estado = 'EX'
                 then
                    w_codigo_cidade_ibge := null;
                 end if;

                 -- insere o registro do cliente
                 INSERT INTO sped_0150
                    (tip_contabil_fiscal,
                     cod_empresa
                    ,num_cnpj9
                    ,num_cnpj4
                    ,num_cnpj2
                    ,tip_participante
                    ,nom_participante
                    ,nom_fantas_particip
                    ,cod_pais_particip
                    ,num_inscri_estadual
                    ,num_inscri_municipal
                    ,cod_estado_ibge
                    ,cod_cidade_ibge
                    ,cod_cidade_syst
                    ,nom_cidade_syst
                    ,num_suframa
                    ,des_endereco
                    ,num_endereco
                    ,des_complemento
                    ,nom_bairro
                    ,sig_estado_particip
                    ,cod_cidade_zona_franca
                    ,tip_contribuinte_icms
                    ,tip_fisica_juridica
                    ,cep_particip
                    ,num_caixa_postal
                    ,num_inscri_inss
                    ,num_ddd_participante
                    ,num_fone_participante
                    ,num_fax_participante
          ,seq_end_entrega
                    ) values
                    (p_tip_contabil_fiscal
                    ,sped_c100.cod_empresa                                       -- cod_empresa
                    ,sped_c100.num_cnpj_9                                        -- num_cnpj9
                    ,sped_c100.num_cnpj_4                                        -- num_cnpj4
                    ,sped_c100.num_cnpj_2                                        -- num_cnpj2
                    ,'1'                                                         -- tip_participante
                    ,trim(pedi_010.nome_cliente)                                   -- nom_participante
                    ,trim(pedi_010.fantasia_cliente)                             -- nom_fantas_particip
                    ,w_codigo_pais_sisc                                          -- cod_pais_particip
                    ,REPLACE(REPLACE(REPLACE(REPLACE(pedi_010.insc_est_cliente,' '),'-'),'.'),'/')  -- num_inscri_estadual
                    ,' '                                                         -- num_inscri_municipal
                    ,w_codigo_fiscal_uf
                    ,w_codigo_cidade_ibge                                        -- cod_cidade_ibge
                    ,w_cod_cidade
                    ,w_cidade                                                    -- nom_cidade_syst
                    ,trim(pedi_010.nr_suframa_cli)                               -- num_suframa
                    ,trim(w_des_endereco)                                      -- des_endereco
                    ,trim(w_numero_imovel)                                       -- num_endereco
                    ,trim(w_complemento_end)                                     -- des_complemento
                    ,trim(w_bairro)                                               -- nom_bairro
                    ,trim(w_estado)                                                    -- sig_estado_particip
                    ,pedi_010.cod_cidade_zona_franca                             -- cod_cidade_zona_franca
                    ,w_tip_contribuinte_icms
                    ,pedi_010.fisica_juridica                                    -- tip_fisica_juridica
                    ,w_cep_cliente
                    ,pedi_010.cxpostal_cliente                                   -- num_caixa_postal
                    ,''                                                          -- num_inscri_inss
                    ,w_ddd                                                       -- num_ddd_participante
                    ,pedi_010.telefone_cliente                                   -- num_fone_participante
                    ,pedi_010.fax_cliente                                        -- num_fax_participante
                    ,sped_c100.seq_end_entrega
          );

              EXCEPTION
                 WHEN OTHERS THEN
                    p_des_erro := 'Erro na inclusão da tabela sped_0150 ' || Chr(10) ||
                                  'Cliente: ' || sped_c100.num_cnpj_9 || '/' || sped_c100.num_cnpj_4 || '-' || sped_c100.num_cnpj_2 ||
                                  Chr(10) || SQLERRM;
                    RAISE W_ERRO;
              END;

              COMMIT;

          END LOOP;

          IF  w_ind_achou = 'N' THEN
              inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                  'Não achou o cliente da nota fiscal na tabela PEDI_010 ' || Chr(10) ||
                                  '   Cliente: ' || sped_c100.num_cnpj_9 || '/' ||
                                                    LPad(sped_c100.num_cnpj_4,4,0) || '-' ||
                                                    LPad(sped_c100.num_cnpj_2,2,0));
          END IF;
       ELSE

          -- le os dados do fornecedor, pois o documento é de entrada
          FOR supr_010 IN u_supr_010 (sped_c100.num_cnpj_9, sped_c100.num_cnpj_4, sped_c100.num_cnpj_2)
          LOOP

              w_ind_achou := 'S';

              -- le os dados da cidade, estado e pais do fornecedor
              BEGIN
                 SELECT basi_160.estado,                 basi_160.ddd,                to_char(basi_160.codigo_fiscal,'00000'),
                        basi_160.codigo_pais,            basi_160.cidade,             basi_167.codigo_fiscal_uf,
                        basi_165.codigo_fiscal,          basi_160.cod_cidade_zona_franca
                 INTO   w_estado_forn,                   w_ddd_forn,                       w_codigo_cidade_ibge_forn,
                        w_codigo_pais_forn,              w_cidade_forn,                    w_codigo_fiscal_uf_forn,
                        w_codigo_pais_sisc_forn,         w_cod_cid_z_franca_forn
                 from   basi_160,
                        basi_167,
                        basi_165
                 where supr_010.cod_cidade  = basi_160.cod_cidade
                 and   basi_160.estado      = basi_167.estado
                 and   basi_160.codigo_pais = basi_167.codigo_pais
                 and   basi_160.codigo_pais = basi_165.codigo_pais;
        
         w_des_endereco_forn     := supr_010.endereco_forne;
                 w_numero_imovel_forn    := supr_010.numero_imovel;
                 w_complemento_end_forn  := supr_010.complemento;
                 w_bairro_forn         := supr_010.bairro;
                 w_cep_forn          := supr_010.cep_fornecedor;
                 w_cod_cidade_forn     := supr_010.cod_cidade;
         
        IF sped_c100.seq_end_entrega != 0
        THEN  
        
          SELECT  pedi_150.end_entr_cobr,           pedi_150.numero_imovel,     pedi_150.complemento_endereco,
              pedi_150.bairro_entr_cobr,        pedi_150.cep_entr_cobr,     pedi_150.cid_entr_cobr
          INTO    w_des_endereco_forn,              w_numero_imovel_forn,       w_complemento_end_forn,
              w_bairro_forn,                    w_cep_forn,                  w_cod_cidade_forn
          from     pedi_150
          where     pedi_150.cd_cli_cgc_cli9 = sped_c100.num_cnpj_9
          and       pedi_150.cd_cli_cgc_cli4 = sped_c100.num_cnpj_4
          and       pedi_150.cd_cli_cgc_cli2 = sped_c100.num_cnpj_2
          and       pedi_150.tipo_endereco   = 1
          and       pedi_150.seq_endereco    = sped_c100.seq_end_entrega;
         
          SELECT   basi_160.estado,                     basi_160.ddd,                                to_number(to_char(basi_160.codigo_fiscal,'00000')),
              basi_160.codigo_pais,                basi_160.cidade,                             basi_167.codigo_fiscal_uf,
              basi_165.codigo_fiscal,              basi_160.cod_cidade_zona_franca
          INTO     w_estado_forn,                       w_ddd_forn,                                  w_codigo_cidade_ibge_forn,
              w_codigo_pais_forn,                 w_cidade_forn,                               w_codigo_fiscal_uf_forn,
              w_codigo_pais_sisc_forn,            w_cod_cid_z_franca_forn
          from    basi_160,
              basi_167,
              basi_165
          where     w_cod_cidade_forn    = basi_160.cod_cidade
          and       basi_160.estado      = basi_167.estado
          and       basi_160.codigo_pais = basi_167.codigo_pais
          and       basi_160.codigo_pais = basi_165.codigo_pais;
         
                END IF;
        
              EXCEPTION
                 WHEN OTHERS THEN
                    w_estado_forn            := NULL;
                    w_ddd_forn               := NULL;
                    w_codigo_pais_sisc_forn   := NULL;
                    w_codigo_pais_forn        := NULL;
                    w_cidade_forn             := NULL;
                    w_codigo_fiscal_uf_forn    := NULL;
                    w_codigo_cidade_ibge_forn   := NULL;

                    inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                        'Erro na leitura das informações de cidade, estado e pais do fornecedor ' || Chr(10) ||
                                        '   Fornecedor: ' || sped_c100.num_cnpj_9 || '/' || LPad(sped_c100.num_cnpj_4,4,0) ||
                                       '-' || LPad(sped_c100.num_cnpj_2,2,0));
              END;

              begin
                 select count(*)
                 into w_fornec_cliente
                 from sped_0150
                 where cod_empresa = p_cod_empresa
                   and sped_0150.tip_contabil_fiscal = 'F'
                   and num_cnpj9 = supr_010.fornecedor9
                   and num_cnpj4 = supr_010.fornecedor4
                   and num_cnpj2 = supr_010.fornecedor2
                       and seq_end_entrega = sped_c100.seq_end_entrega
                   and p_tipo_layout = 0;
              end;

              if w_fornec_cliente = 0
              THEN

                BEGIN
                   -- verifica se o fornecedor é pessoa fisica ou juridica (pelo CNPJ)
                   if sped_c100.num_cnpj_4 = 0 and (sped_c100.num_cnpj_9 > 0 or sped_c100.num_cnpj_2 > 0)
                   then
                      w_tp_pessoa_fisica := '1';
                   else
                      w_tp_pessoa_fisica := '2';
                   end if;

                   -- se o fornecedor for ISENTO, seta as variaveis como não contribuinte e anula o campo da inscricao
                   if trim(supr_010.inscr_est_forne) = 'ISENTO'
                   then
                      w_tip_contribuinte_icms  := 'N';
                      supr_010.inscr_est_forne := null;
                   else
                      w_tip_contribuinte_icms := 'S';
                   end if;

                   if w_estado_forn = 'EX'
                   then
                      w_codigo_cidade_ibge_forn := null;
                   end if;

                   -- insere o registro do fornecedor
                   INSERT INTO sped_0150
                      (tip_contabil_fiscal
                      ,cod_empresa
                      ,num_cnpj9
                      ,num_cnpj4
                      ,num_cnpj2
                      ,tip_participante
                      ,nom_participante
                      ,nom_fantas_particip
                      ,cod_pais_particip
                      ,num_inscri_estadual
                      ,num_inscri_municipal
                      ,cod_estado_ibge
                      ,cod_cidade_ibge
                      ,cod_cidade_syst
                      ,nom_cidade_syst
                      ,num_suframa
                      ,des_endereco
                      ,num_endereco
                      ,des_complemento
                      ,nom_bairro
                      ,sig_estado_particip
                      ,cod_cidade_zona_franca
                      ,tip_contribuinte_icms
                      ,tip_fisica_juridica
                      ,cep_particip
                      ,num_caixa_postal
                      ,num_inscri_inss
                      ,num_ddd_participante
                      ,num_fone_participante
                      ,num_fax_participante
                           ,seq_end_entrega
                      ) values
                      (p_tip_contabil_fiscal
                      ,sped_c100.cod_empresa                                       -- cod_empresa
                      ,sped_c100.num_cnpj_9                                        -- num_cnpj9
                      ,sped_c100.num_cnpj_4                                        -- num_cnpj4
                      ,sped_c100.num_cnpj_2                                        -- num_cnpj2
                      ,'2'                                                           -- tip_participante
                      ,supr_010.nome_fornecedor                                    -- nom_participante
                      ,supr_010.nome_fantasia                                      -- nom_fantas_particip
                      ,w_codigo_pais_sisc_forn                                     -- cod_pais_particip
                      ,REPLACE(REPLACE(REPLACE(supr_010.inscr_est_forne,'.'),'-'),'/') -- num_inscri_estadual
                      ,' '                                                         -- num_inscri_municipal
                      ,w_codigo_fiscal_uf_forn                                     -- codigo estado ibge
                      ,w_codigo_cidade_ibge_forn                                   -- cod_cidade_ibge
                      ,w_cod_cidade_forn
                      ,trim(w_cidade_forn)                                         -- nom_cidade_syst
                      ,' '                                                         -- num_suframa
                      ,trim(w_des_endereco_forn)                                 -- des_endereco
                      ,trim(w_numero_imovel_forn)                                  -- num_endereco
                      ,trim(w_complemento_end_forn)                                -- des_complemento
                      ,trim(w_bairro_forn)                                         -- nom_bairro
                      ,trim(w_estado_forn)                                         -- sig_estado_particip
                      ,w_cod_cid_z_franca_forn                                     -- cod_cidade_zona_franca
                      ,w_tip_contribuinte_icms
                      ,w_tp_pessoa_fisica                                          -- tip_fisica_juridica
                      ,supr_010.cep_fornecedor
                      ,supr_010.cx_postal_forne                                    -- num_caixa_postal
                      ,supr_010.inscr_inss                                         -- num_inscri_inss
                      ,w_ddd_forn                                                  -- num_ddd_participante
                      ,decode(length(to_char(supr_010.telefone_forne)),9,substr(to_char(supr_010.telefone_forne),2,8), supr_010.telefone_forne) -- num_fone_participante
                      ,supr_010.fax_fornecedor                                     -- num_fax_participante
                      ,sped_c100.seq_end_entrega
                     );

                EXCEPTION
                   WHEN OTHERS THEN
                      p_des_erro := 'Erro na inclusão da tabela sped_0150 ' || Chr(10) ||
                                    'Cliente: ' || sped_c100.num_cnpj_9 || '/' || sped_c100.num_cnpj_4 || '-' || sped_c100.num_cnpj_2 ||
                                    Chr(10) || SQLERRM;
                      RAISE W_ERRO;
                END;
              END IF;
              COMMIT;
          END LOOP;

          IF  w_ind_achou = 'N'
          THEN
              inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                  'Não achou o fornecedor da nota fiscal na tabela SUPR_010 ' || Chr(10) ||
                                  '   Cliente: ' || sped_c100.num_cnpj_9 || '/' ||
                                                    LPad(sped_c100.num_cnpj_4,4,0) || '-' ||
                                                    LPad(sped_c100.num_cnpj_2,2,0));
          END IF;
       END IF;
   END LOOP;
   COMMIT;

EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_0150 ' || Chr(10) || p_des_erro;

END inter_pr_gera_sped_0150;
