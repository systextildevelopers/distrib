alter table FATU_504 
add (opcao_controle_cubagem number(1) default 0);

comment on column fatu_504.opcao_controle_cubagem is 'Opcao para controle de cubagem: 0 - Todos estagios / 1 - Ultimo estagio';

/
