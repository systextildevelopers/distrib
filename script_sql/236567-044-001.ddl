alter table pedi_100 add(
ind_integ_processo_narw_pedi number(1)
);

alter table pedi_100
modify ind_integ_processo_narw_pedi default 0;

COMMENT ON COLUMN pedi_100.ind_integ_processo_narw_pedi IS '0-Nao integrado, 1-Integrado com sucesso, 2-Erro ao integrar, 3-Re-integrar';
