
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_082_LOG" 
before insert or
       delete or
       update of numero_ordem, seq_areceber, sequencia, prodsai_nivel99,
	prodsai_grupo, prodsai_subgrupo, prodsai_item, lote_saida,
	qtde_estrutura, qtde_enviada, valor_produto, deposito,
	num_nf_sai, ser_nf_sai, seq_nf_sai, qtde_auxiliar,
	executa_trigger
       on obrf_082
for each row

declare
  v_prodsai_nivel99_old   obrf_082.prodsai_nivel99%type;
  v_prodsai_grupo_old     obrf_082.prodsai_grupo%type;
  v_prodsai_subgrupo_old  obrf_082.prodsai_subgrupo%type;
  v_prodsai_item_old      obrf_082.prodsai_item%type;
  v_lote_saida_old        obrf_082.lote_saida%type;
  v_qtde_estrutura_old    obrf_082.qtde_estrutura%type;
  v_qtde_enviada_old      obrf_082.qtde_enviada%type;
  v_valor_produto_old     obrf_082.valor_produto%type;
  v_deposito_old          obrf_082.deposito%type;
  v_num_nf_sai_old        obrf_082.num_nf_sai%type;
  v_ser_nf_sai_old        obrf_082.ser_nf_sai%type;
  v_seq_nf_sai_old        obrf_082.seq_nf_sai%type;
  v_qtde_auxiliar_old     obrf_082.qtde_auxiliar%type;

  v_prodsai_nivel99_new   obrf_082.prodsai_nivel99%type;
  v_prodsai_grupo_new     obrf_082.prodsai_grupo%type;
  v_prodsai_subgrupo_new  obrf_082.prodsai_subgrupo%type;
  v_prodsai_item_new      obrf_082.prodsai_item%type;
  v_lote_saida_new        obrf_082.lote_saida%type;
  v_qtde_estrutura_new    obrf_082.qtde_estrutura%type;
  v_qtde_enviada_new      obrf_082.qtde_enviada%type;
  v_valor_produto_new     obrf_082.valor_produto%type;
  v_deposito_new          obrf_082.deposito%type;
  v_num_nf_sai_new        obrf_082.num_nf_sai%type;
  v_ser_nf_sai_new        obrf_082.ser_nf_sai%type;
  v_seq_nf_sai_new        obrf_082.seq_nf_sai%type;
  v_qtde_auxiliar_new     obrf_082.qtde_auxiliar%type;

  v_numero_ordem          obrf_082.numero_ordem%type;
  v_seq_areceber          obrf_082.seq_areceber%type;
  v_sequencia             obrf_082.sequencia%type;

  v_operacao              varchar(1);
  v_data_operacao         date;
  v_usuario_rede          varchar(20);
  v_maquina_rede          varchar(40);
  v_aplicativo            varchar(20);
  v_sid                   number(9);
  v_usuario_systextil     varchar(250);
  v_empresa               number(3);
  v_locale_usuario        varchar(5);

  v_executa_trigger      number;

begin

   inter_pr_dados_usuario(v_usuario_rede, v_maquina_rede, v_aplicativo, v_sid, v_usuario_systextil, v_empresa, v_locale_usuario);

   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger = 0
   then
      begin
      -- Grava a data/hora da insercao do registro (log)
      v_data_operacao := sysdate();

      --alimenta as variaveis new caso seja insert ou update
      if inserting or updating
      then
         if inserting
         then v_operacao := 'I';
         else v_operacao := 'U';
         end if;

         v_numero_ordem          := :new.numero_ordem;
         v_seq_areceber          := :new.seq_areceber;
         v_sequencia             := :new.sequencia;
         v_prodsai_nivel99_new   := :new.prodsai_nivel99;
         v_prodsai_grupo_new     := :new.prodsai_grupo;
         v_prodsai_subgrupo_new  := :new.prodsai_subgrupo;
         v_prodsai_item_new      := :new.prodsai_item;
         v_lote_saida_new        := :new.lote_saida;
         v_qtde_estrutura_new    := :new.qtde_estrutura;
         v_qtde_enviada_new      := :new.qtde_enviada;
         v_valor_produto_new     := :new.valor_produto;
         v_deposito_new          := :new.deposito;
         v_num_nf_sai_new        := :new.num_nf_sai;
         v_ser_nf_sai_new        := :new.ser_nf_sai;
         v_seq_nf_sai_new        := :new.seq_nf_sai;
         v_qtde_auxiliar_new     := :new.qtde_auxiliar;

      end if; --fim do if inserting or updating

      --alimenta as variaveis old caso seja insert ou update
      if deleting or updating
      then
         if deleting
         then v_operacao := 'D';
         else v_operacao := 'U';
         end if;

         v_numero_ordem          := :old.numero_ordem;
         v_seq_areceber          := :old.seq_areceber;
         v_sequencia             := :old.sequencia;
         v_prodsai_nivel99_old   := :old.prodsai_nivel99;
         v_prodsai_grupo_old     := :old.prodsai_grupo;
         v_prodsai_subgrupo_old  := :old.prodsai_subgrupo;
         v_prodsai_item_old      := :old.prodsai_item;
         v_lote_saida_old        := :old.lote_saida;
         v_qtde_estrutura_old    := :old.qtde_estrutura;
         v_qtde_enviada_old      := :old.qtde_enviada;
         v_valor_produto_old     := :old.valor_produto;
         v_deposito_old          := :old.deposito;
         v_num_nf_sai_old        := :old.num_nf_sai;
         v_ser_nf_sai_old        := :old.ser_nf_sai;
         v_seq_nf_sai_old        := :old.seq_nf_sai;
         v_qtde_auxiliar_old     := :old.qtde_auxiliar;

      end if; --fim do if inserting or updating

      --insere na obrf_081_log o registro
      insert into obrf_082_log
        (numero_ordem,            seq_areceber,
         sequencia,               operacao,
         data_operacao,           usuario_rede,
         maquina_rede,            aplicativo,
         prodsai_nivel99_old,     prodsai_grupo_old,
         prodsai_subgrupo_old,    prodsai_item_old,
         lote_saida_old,          qtde_estrutura_old,
         qtde_enviada_old,        valor_produto_old,
         deposito_old,            num_nf_sai_old,
         ser_nf_sai_old,          seq_nf_sai_old,
         qtde_auxiliar_old,       prodsai_nivel99_new,
         prodsai_grupo_new,       prodsai_subgrupo_new,
         prodsai_item_new,        lote_saida_new,
         qtde_estrutura_new,      qtde_enviada_new,
         valor_produto_new,       deposito_new,
         num_nf_sai_new,          ser_nf_sai_new,
         seq_nf_sai_new,          qtde_auxiliar_new)
      values
        (v_numero_ordem,          v_seq_areceber,
         v_sequencia,             v_operacao,
         v_data_operacao,         v_usuario_rede,
         v_maquina_rede,          v_aplicativo,
         v_prodsai_nivel99_old,   v_prodsai_grupo_old,
         v_prodsai_subgrupo_old,  v_prodsai_item_old,
         v_lote_saida_old,        v_qtde_estrutura_old,
         v_qtde_enviada_old,      v_valor_produto_old,
         v_deposito_old,          v_num_nf_sai_old,
         v_ser_nf_sai_old,        v_seq_nf_sai_old,
         v_qtde_auxiliar_old,     v_prodsai_nivel99_new,
         v_prodsai_grupo_new,     v_prodsai_subgrupo_new,
         v_prodsai_item_new,      v_lote_saida_new,
         v_qtde_estrutura_new,    v_qtde_enviada_new,
         v_valor_produto_new,     v_deposito_new,
         v_num_nf_sai_new,        v_ser_nf_sai_new,
         v_seq_nf_sai_new,        v_qtde_auxiliar_new);


      exception
         when OTHERS
         then raise_application_error (-20000, 'Nao atualizou a tabela de log da obrf_082.');

      end;
   end if;
end inter_tr_obrf_082_log;

-- ALTER TRIGGER "INTER_TR_OBRF_082_LOG" ENABLE
 

/

exec inter_pr_recompile;

