
  CREATE OR REPLACE FUNCTION "INTER_FN_PERC_ATR_SERASA" 
  (p_pedido_venda pedi_100.pedido_venda%type)
return number
is

   v_perc_atraso      number(5,2);
   v_qtde_aberto      number(15,2);
   v_qtde_atraso      number(15,2);
   v_pago_chq	        number(15,2);
   v_dias_em_atraso   number;
   v_max_dias_atraso  fatu_500.max_dias_atraso%type;
   v_dia_util         basi_260.dia_util_finan%type;
   v_codigo_empresa   pedi_100.codigo_empresa%type;
   v_cli_ped_cgc_cli9 pedi_100.cli_ped_cgc_cli9%type;
   v_cli_ped_cgc_cli4 pedi_100.cli_ped_cgc_cli4%type;
   v_cli_ped_cgc_cli2 pedi_100.cli_ped_cgc_cli2%type;

   type saldoduplicata is table of fatu_070.saldo_duplicata%type;
   v_saldo_duplicata saldoduplicata;

   type dataprorrogacao is table of fatu_070.data_prorrogacao%type;
   v_data_prorrogacao dataprorrogacao;

begin

   begin
      select pedi_100.codigo_empresa,   pedi_100.cli_ped_cgc_cli9,
             pedi_100.cli_ped_cgc_cli4, pedi_100.cli_ped_cgc_cli2
      into   v_codigo_empresa,          v_cli_ped_cgc_cli9,
             v_cli_ped_cgc_cli4,        v_cli_ped_cgc_cli2
      from pedi_100
      where pedi_100.pedido_venda = p_pedido_venda;
   end;

   /*encontra permitidos de atraso configurados no par�metro de empresa*/
   begin
      select fatu_500.max_dias_atraso
      into v_max_dias_atraso
      from fatu_500
      where fatu_500.codigo_empresa   = v_codigo_empresa;
      exception when others then
         v_max_dias_atraso := 0;
   end;

   /*encontra total dos t�tulos pago em cheque*/
   begin
      select sum(crec_180.valor_usado)
      into v_pago_chq
      from crec_180, fatu_070
      where   crec_180.empresa        = fatu_070.codigo_empresa
        and ((crec_180.cgc9_cli       = fatu_070.cli9resptit
        and   crec_180.cgc4_cli       = fatu_070.cli4resptit
        and   crec_180.cgc2_cli       = fatu_070.cli2resptit)
        or   (crec_180.cgc9_cli       = fatu_070.cli_dup_cgc_cli9
        and   crec_180.cgc4_cli       = fatu_070.cli_dup_cgc_cli4
        and   crec_180.cgc2_cli       = fatu_070.cli_dup_cgc_cli2))
        and   crec_180.tipo_titulo    = fatu_070.tipo_titulo
        and   crec_180.numero_titulo  = fatu_070.num_duplicata
        and   crec_180.parcela_titulo = fatu_070.seq_duplicatas
        and   crec_180.flag_reg       = 0
        and   fatu_070.codigo_empresa   = v_codigo_empresa
        and   fatu_070.cli_dup_cgc_cli9 = v_cli_ped_cgc_cli9
        and   fatu_070.cli_dup_cgc_cli4 = v_cli_ped_cgc_cli4
        and   fatu_070.cli_dup_cgc_cli2 = v_cli_ped_cgc_cli2
        and   fatu_070.situacao_duplic  <> 2
        and   fatu_070.saldo_duplicata  <> 0;
      exception when others then
           v_pago_chq := 0;
   end;
   if sql%notfound
   then
      v_pago_chq := 0;
   end if;


   /*encontra total dos t�tulos em aberto para o cliente do pedido*/
   begin
      select sum(fatu_070.saldo_duplicata)
      into   v_qtde_aberto
      from fatu_070
      where fatu_070.codigo_empresa   = v_codigo_empresa
        and fatu_070.cli_dup_cgc_cli9 = v_cli_ped_cgc_cli9
        and fatu_070.cli_dup_cgc_cli4 = v_cli_ped_cgc_cli4
        and fatu_070.cli_dup_cgc_cli2 = v_cli_ped_cgc_cli2
        and fatu_070.situacao_duplic  <> 2
        and fatu_070.saldo_duplicata  <> 0;
      exception when others then
         v_qtde_aberto := 0.00;
   end;
   if sql%notfound
   then
      v_qtde_aberto := 0.00;
   end if;

   v_qtde_aberto := v_qtde_aberto - v_pago_chq;

   /*encontra total dos t�tulos em atraso para o cliente do pedido*/
   v_qtde_atraso := 0.00;

   begin
      select fatu_070.saldo_duplicata, fatu_070.data_prorrogacao
      bulk collect into
             v_saldo_duplicata,        v_data_prorrogacao
        from fatu_070, pedi_100
       where fatu_070.codigo_empresa   = pedi_100.codigo_empresa
         and fatu_070.cli_dup_cgc_cli9 = pedi_100.cli_ped_cgc_cli9
         and fatu_070.cli_dup_cgc_cli4 = pedi_100.cli_ped_cgc_cli4
         and fatu_070.cli_dup_cgc_cli2 = pedi_100.cli_ped_cgc_cli2
         and fatu_070.situacao_duplic  <> 2
         and fatu_070.saldo_duplicata  > 0
         and pedi_100.pedido_venda     = p_pedido_venda;
   end;
   if sql%found
   then

      for i in v_saldo_duplicata.first .. v_saldo_duplicata.last
      loop

         /*verifica se a data da duplicata � dia �til, caso n�o for, encontra o pr�ximo dia �til do m�s*/
         loop
            begin
               select basi_260.dia_util_finan
                 into v_dia_util
                 from basi_260
                where basi_260.data_calendario = v_data_prorrogacao(i);
                exception when others then
                   v_dia_util := 0;
            end;
            if sql%notfound
            then
               v_dia_util := 0;
            end if;

            if v_dia_util = 1
            then
               v_data_prorrogacao(i) := v_data_prorrogacao(i) + 1;
            end if;

            exit when v_dia_util = 0;
         end loop;

         v_dias_em_atraso := 0;

         if v_data_prorrogacao(i) < sysdate
         then
            v_dias_em_atraso := sysdate - v_data_prorrogacao(i);
         end if;

         /*verifica se os dias em atraso est�o dentro do permitido*/
         if v_dias_em_atraso > v_max_dias_atraso
         then
            v_qtde_atraso := v_qtde_atraso + v_saldo_duplicata(i);
         end if;

      end loop;

   end if;

   /*calcula o percentual de t�tulos em atraso em rela��o aos t�tulos em aberto*/
   v_perc_atraso := (v_qtde_atraso * 100) / v_qtde_aberto;

   return(v_qtde_atraso);

end inter_fn_perc_atr_serasa;
 

/

exec inter_pr_recompile;

