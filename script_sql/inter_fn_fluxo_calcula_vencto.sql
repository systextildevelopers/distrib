
  CREATE OR REPLACE FUNCTION "INTER_FN_FLUXO_CALCULA_VENCTO" (p_data_entrega in date, p_cond_pagto in number, p_sequencia in number, p_venda_compra in number)
return date 
is
   v_data_vencto date;
   v_vencimento  number;
 
begin

   if p_venda_compra = 1  -- venda
   then  

      select nvl(sum(vencimento),0) into v_vencimento from pedi_075
      where condicao_pagto = p_cond_pagto
      and   sequencia     <= p_sequencia;
   else
      -- compra 
      select nvl(sum(vencimento),0) into v_vencimento from supr_055
      where condicao_pagto = p_cond_pagto
      and   sequencia     <= p_sequencia;
   end if;

   v_data_vencto := p_data_entrega + v_vencimento;
     
   return(v_data_vencto);
end inter_fn_fluxo_calcula_vencto;

 

/

exec inter_pr_recompile;

