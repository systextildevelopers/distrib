alter table oper_027
  add COD_EMPRESA NUMBER(3) default 0;
  
alter table oper_027
  modify COD_EMPRESA NUMBER(3) default 0;
  
delete from oper_027; 
   
alter table oper_027
  drop constraint PK_oper_027; 
  
alter table oper_027
  add constraint PK_oper_027 primary key (COD_EMPRESA);
  
exec inter_pr_recompile;  



