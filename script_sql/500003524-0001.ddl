declare

begin
   begin
      update hdoc_036
         set hdoc_036.descricao       = 'Cancelación de Títulos - Asociación de Cheques (vía lector)'
      where hdoc_036.codigo_programa = 'cpag_f069'
        and hdoc_036.locale          = 'es_ES';
   exception
   when no_data_found then
      Insert into HDOC_036 (CODIGO_PROGRAMA,LOCALE,DESCRICAO,FLAG_REPASSADO,DATA_TRADUCAO,USUARIO_TRADUCAO,DATA_CRIACAO) 
      values ('cpag_f069','es_ES','Cancelación de Títulos - Asociación de Cheques (vía lector)','3',null,null,to_date('28/07/17','DD/MM/RR'));      
   end; 

   begin
   update hdoc_036
      set hdoc_036.descricao       = 'Baixa de Títulos - Associação de Cheques (via leitor)'
   where hdoc_036.codigo_programa = 'cpag_f069'
     and hdoc_036.locale          = 'pt_BR';
   exception
   when no_data_found then
      Insert into HDOC_036 (CODIGO_PROGRAMA,LOCALE,DESCRICAO,FLAG_REPASSADO,DATA_TRADUCAO,USUARIO_TRADUCAO,DATA_CRIACAO) 
      values ('cpag_f069','pt_BR','Baixa de Títulos - Associação de Cheques (via leitor)','3',null,null,to_date('28/07/28','DD/MM/RR'));
   end; 
   
   commit;
end;
