CREATE TABLE  PROJ_030 
   (	SEQ_IMPORTACAO_DPV NUMBER(30,0) NOT NULL ENABLE, 
	CODIGO_EMPRESA NUMBER(3,0) NOT NULL ENABLE, 
	DATA_IMPORTACAO DATE NOT NULL ENABLE, 
	CODIGO_ARTIGO VARCHAR2(50) NOT NULL ENABLE, 
	NIVEL VARCHAR2(1) NOT NULL ENABLE, 
	GRUPO VARCHAR2(5) NOT NULL ENABLE, 
	SUBGRUPO VARCHAR2(3) NOT NULL ENABLE, 
	ITEM VARCHAR2(6) NOT NULL ENABLE, 
	QTDE_DPV NUMBER(9,0), 
	QTDE_WIP NUMBER(9,0), 
	QTDE_ORDEM NUMBER(9,0), 
	QTDE_TEAR NUMBER(9,0), 
	QTDE_ESTOQUE_TINTO NUMBER(9,0), 
	QTDE_NECESSIDADE_TECELAGEM NUMBER(9,0), 
	QTDE_POSICAO_ESTOQUE NUMBER(9,0), 
	QTDE_PROJECAO_ESTOQUE NUMBER(9,0), 
	QTDE_PRODUZIR_TECELAGEM NUMBER(9,0), 
	NUMERO_ALTERNATIVA NUMBER(2,0), 
	NUMERO_ROTEIRO NUMBER(2,0), 
	USUARIO_ATUALIZACAO VARCHAR2(30) NOT NULL ENABLE, 
	DATA_ATUALIZACAO DATE NOT NULL ENABLE, 
	PERC_SEGUNDA_QUALIDADE NUMBER(5,2), 
	 CONSTRAINT UK_PROJ_030 UNIQUE (CODIGO_EMPRESA, DATA_IMPORTACAO, CODIGO_ARTIGO)
  USING INDEX  ENABLE, 
	 CONSTRAINT PK_PROJ_030 PRIMARY KEY (SEQ_IMPORTACAO_DPV)
  USING INDEX  ENABLE
);

/

CREATE OR REPLACE EDITIONABLE TRIGGER  INTER_TR_PROJ_030 
BEFORE INSERT OR UPDATE ON PROJ_030 FOR EACH ROW
DECLARE
BEGIN

   IF inserting then 
      IF :new.seq_importacao_dpv is null THEN 
         :new.seq_importacao_dpv:= seq_proj_030.nextval;
      END IF;
      IF :new.nivel is null THEN 
         :new.nivel:= substr(:new.codigo_artigo,0,instr(:new.codigo_artigo,'.',1)- 1);
      END IF;
      IF :new.grupo is null THEN 
         :new.grupo:= substr(:new.codigo_artigo,instr(:new.codigo_artigo,'.',1)+1, (instr(:new.codigo_artigo,'.',1,2) - instr(:new.codigo_artigo,'.',1,1)) -1 );
      END IF;
      IF :new.subgrupo is null THEN 
         :new.subgrupo:= substr(:new.codigo_artigo,instr(:new.codigo_artigo,'.',1,2)+1, (instr(:new.codigo_artigo,'.',1,3) - instr(:new.codigo_artigo,'.',1,2)) -1 );
      END IF;
      IF :new.item is null THEN 
         :new.item:= substr(:new.codigo_artigo,instr(:new.codigo_artigo,'.',1,3)+1);
      END IF;
      BEGIN
         select numero_alternati,numero_roteiro
         into :new.numero_alternativa,:new.numero_roteiro
         from basi_010
         where nivel_estrutura =:new.nivel
         and grupo_estrutura = :new.grupo
         and subgru_estrutura = :new.subgrupo
         and item_estrutura = :new.item;
      EXCEPTION 
         WHEN OTHERS THEN 
         raise_application_error(-20005,'Alternativa e Roteiro não encontrados!');
      END;   
   END IF;
   :NEW.USUARIO_ATUALIZACAO:= nvl(sys_context('APEX$SESSION','APP_USER'),user);
   :NEW.DATA_ATUALIZACAO:= sysdate;   
END INTER_TR_PROJ_030;

/

ALTER TRIGGER  INTER_TR_PROJ_030 ENABLE;

/

CREATE OR REPLACE EDITIONABLE TRIGGER  INTER_TR_PROJ_030_LOG 
after insert or delete or update 
on proj_030 
for each row 
declare 
   ws_usuario_rede           varchar2(2000) ; 
   ws_maquina_rede           varchar2(2000) ; 
   ws_aplicativo             varchar2(2000) ; 
   ws_sid                    number; 
   ws_empresa                number; 
   ws_usuario_systextil      varchar2(2000) ; 
   ws_locale_usuario         varchar2(2000) ; 
   ws_nome_programa          varchar2(2000) ; 

begin
-- dados do usuario logado
 inter_pr_dados_usuario (ws_usuario_rede,ws_maquina_rede,ws_aplicativo,ws_sid,ws_usuario_systextil,ws_empresa,ws_locale_usuario); 
 ws_nome_programa := inter_fn_nome_programa(ws_sid);
 
 if inserting 
 then 
    begin 
        insert into proj_030_log (
           tipo_ocorr,   /*0*/ 
           data_ocorr,   /*1*/ 
           hora_ocorr,   /*2*/ 
           usuario_rede,   /*3*/ 
           maquina_rede,   /*4*/ 
           aplicacao,   /*5*/ 
           usuario_sistema,   /*6*/ 
           nome_programa,   /*7*/ 
           seq_importacao_dpv, /*7.1*/ 
           codigo_empresa,   /*8*/ 
           data_importacao, /*9*/ 
           codigo_artigo, /*10*/ 
           nivel, /*11*/ 
           grupo, /*12*/ 
           subgrupo, /*13*/ 
           item, /*14*/ 
           qtde_dpv_new, /*15*/ 
           qtde_dpv_old, /*16*/ 
           qtde_wip_new, /*17*/ 
           qtde_wip_old, /*18*/ 
           qtde_ordem_new, /*19*/ 
           qtde_ordem_old, /*20*/ 
           qtde_tear_new, /*21*/ 
           qtde_tear_old, /*22*/ 
           qtde_estoque_tinto_new, /*23*/ 
           qtde_estoque_tinto_old, /*24*/ 
           qtde_necessidade_tecelagem_new, /*25*/
           qtde_necessidade_tecelagem_old, /*26*/
           qtde_posicao_estoque_new, /*27*/
           qtde_posicao_estoque_old, /*28*/
           qtde_projecao_estoque_new, /*29*/
           qtde_projecao_estoque_old, /*30*/
           qtde_produzir_tecelagem_new, /*31*/
           qtde_produzir_tecelagem_old, /*32*/
           perc_segunda_qualidade_new, /*33*/
           perc_segunda_qualidade_old, /*34*/
           numero_alternativa_new, /*35*/
           numero_alternativa_old, /*36*/
           numero_roteiro_new, /*37*/
           numero_roteiro_old /*38*/
        ) values (    
           'i', /*o*/
           sysdate, /*1*/
           sysdate,/*2*/ 
           ws_usuario_rede,/*3*/ 
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/ 
           ws_nome_programa, /*7*/
           null,/*7.1*/
           :new.codigo_empresa,/*8*/
           :new.data_importacao, /*9*/   
           :new.codigo_artigo,/*10*/
           :new.nivel, /*11*/   
           :new.grupo,/*12*/
           :new.subgrupo, /*13*/   
           :new.item,/*14*/
           :new.qtde_dpv, /*15*/   
           null,/*16*/
           :new.qtde_wip, /*17*/   
           null,/*18*/
           :new.qtde_ordem, /*19*/   
           null,/*20*/
           :new.qtde_tear, /*21*/   
           null,/*22*/
           :new.qtde_estoque_tinto, /*23*/   
           null,/*24*/
           :new.qtde_necessidade_tecelagem, /*25*/
           null, /*26*/
           :new.qtde_posicao_estoque, /*27*/
           null, /*28*/
           :new.qtde_projecao_estoque, /*29*/
           null, /*30*/
           :new.qtde_produzir_tecelagem, /*31*/
           null, /*32*/
           :new.perc_segunda_qualidade, /*33*/
           null, /*34*/
           :new.numero_alternativa, /*35*/
           null, /*36*/
           :new.numero_roteiro, /*37*/
           null /*38*/
         );    
    end;    
 end if;    
  
 if updating
 then 
    begin 
        insert into proj_030_log (
           tipo_ocorr,   /*0*/ 
           data_ocorr,   /*1*/ 
           hora_ocorr,   /*2*/ 
           usuario_rede,   /*3*/ 
           maquina_rede,   /*4*/ 
           aplicacao,   /*5*/ 
           usuario_sistema,   /*6*/ 
           nome_programa,   /*7*/ 
           seq_importacao_dpv,/*7.1*/
           codigo_empresa,   /*8*/ 
           data_importacao, /*9*/ 
           codigo_artigo, /*10*/ 
           nivel, /*11*/ 
           grupo, /*12*/ 
           subgrupo, /*13*/ 
           item, /*14*/ 
           qtde_dpv_new, /*15*/ 
           qtde_dpv_old, /*16*/ 
           qtde_wip_new, /*17*/ 
           qtde_wip_old, /*18*/ 
           qtde_ordem_new, /*19*/ 
           qtde_ordem_old, /*20*/ 
           qtde_tear_new, /*21*/ 
           qtde_tear_old, /*22*/ 
           qtde_estoque_tinto_new, /*23*/ 
           qtde_estoque_tinto_old, /*24*/ 
           qtde_necessidade_tecelagem_new, /*25*/
           qtde_necessidade_tecelagem_old, /*26*/
           qtde_posicao_estoque_new, /*27*/
           qtde_posicao_estoque_old, /*28*/
           qtde_projecao_estoque_new, /*29*/
           qtde_projecao_estoque_old, /*30*/
           qtde_produzir_tecelagem_new, /*31*/
           qtde_produzir_tecelagem_old, /*32*/
           perc_segunda_qualidade_new, /*33*/
           perc_segunda_qualidade_old, /*34*/
           numero_alternativa_new, /*35*/
           numero_alternativa_old, /*36*/
           numero_roteiro_new, /*37*/
           numero_roteiro_old /*38*/
        ) values (    
           'a', /*o*/
           sysdate, /*1*/
           sysdate,/*2*/ 
           ws_usuario_rede,/*3*/ 
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/ 
           ws_nome_programa, /*7*/
           :new.seq_importacao_dpv,/*7.1*/
           :new.codigo_empresa,/*8*/
           :new.data_importacao, /*9*/   
           :new.codigo_artigo,/*10*/
           :new.nivel, /*11*/   
           :new.grupo,/*12*/
           :new.subgrupo, /*13*/   
           :new.item,/*14*/
           :new.qtde_dpv, /*15*/   
           :old.qtde_dpv,/*16*/
           :new.qtde_wip, /*17*/   
           :old.qtde_wip,/*18*/
           :new.qtde_ordem, /*19*/   
           :old.qtde_ordem,/*20*/
           :new.qtde_tear, /*21*/   
           :old.qtde_tear,/*22*/
           :new.qtde_estoque_tinto, /*23*/   
           :old.qtde_estoque_tinto,/*24*/
           :new.qtde_necessidade_tecelagem, /*25*/
           :old.qtde_necessidade_tecelagem, /*26*/
           :new.qtde_posicao_estoque, /*27*/
           :old.qtde_posicao_estoque, /*28*/
           :new.qtde_projecao_estoque, /*29*/
           :old.qtde_projecao_estoque, /*30*/
           :new.qtde_produzir_tecelagem, /*31*/
           :old.qtde_produzir_tecelagem, /*32*/
           :new.perc_segunda_qualidade, /*33*/
           :old.perc_segunda_qualidade, /*34*/
           :new.numero_alternativa, /*35*/
           :old.numero_alternativa, /*36*/
           :new.numero_roteiro, /*37*/
           :old.numero_roteiro /*38*/
         );    
    end;    
 end if;    
  
 if deleting 
 then 
    begin 
        insert into proj_030_log (
           tipo_ocorr,   /*0*/ 
           data_ocorr,   /*1*/ 
           hora_ocorr,   /*2*/ 
           usuario_rede,   /*3*/ 
           maquina_rede,   /*4*/ 
           aplicacao,   /*5*/ 
           usuario_sistema,   /*6*/ 
           nome_programa,   /*7*/ 
           seq_importacao_dpv,/*7.1*/
           codigo_empresa,   /*8*/ 
           data_importacao, /*9*/ 
           codigo_artigo, /*10*/ 
           nivel, /*11*/ 
           grupo, /*12*/ 
           subgrupo, /*13*/ 
           item, /*14*/ 
           qtde_dpv_new, /*15*/ 
           qtde_dpv_old, /*16*/ 
           qtde_wip_new, /*17*/ 
           qtde_wip_old, /*18*/ 
           qtde_ordem_new, /*19*/ 
           qtde_ordem_old, /*20*/ 
           qtde_tear_new, /*21*/ 
           qtde_tear_old, /*22*/ 
           qtde_estoque_tinto_new, /*23*/ 
           qtde_estoque_tinto_old, /*24*/ 
           qtde_necessidade_tecelagem_new, /*25*/
           qtde_necessidade_tecelagem_old, /*26*/
           qtde_posicao_estoque_new, /*27*/
           qtde_posicao_estoque_old, /*28*/
           qtde_projecao_estoque_new, /*29*/
           qtde_projecao_estoque_old, /*30*/
           qtde_produzir_tecelagem_new, /*31*/
           qtde_produzir_tecelagem_old, /*32*/
           perc_segunda_qualidade_new, /*33*/
           perc_segunda_qualidade_old, /*34*/
           numero_alternativa_new, /*35*/
           numero_alternativa_old, /*36*/
           numero_roteiro_new, /*37*/
           numero_roteiro_old /*38*/
       ) values (    
           'd', /*0*/
           sysdate, /*1*/
           sysdate,/*2*/ 
           ws_usuario_rede,/*3*/ 
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/ 
           ws_nome_programa, /*7*/
           :old.seq_importacao_dpv,/*7.1*/
           :old.codigo_empresa,/*8*/
           :old.data_importacao, /*9*/   
           :old.codigo_artigo,/*10*/
           :old.nivel, /*11*/   
           :old.grupo,/*12*/
           :old.subgrupo, /*13*/   
           :old.item,/*14*/
           null, /*15*/   
           :old.qtde_dpv,/*16*/
           null, /*17*/   
           :old.qtde_wip,/*18*/
           null, /*19*/   
           :old.qtde_ordem,/*20*/
           null, /*21*/   
           :old.qtde_tear,/*22*/
           null, /*23*/   
           :old.qtde_estoque_tinto, /*24*/
           null, /*25*/
           :old.qtde_necessidade_tecelagem, /*26*/
           null /*27*/,
           :old.qtde_posicao_estoque, /*28*/
           null, /*29*/
           :old.qtde_projecao_estoque, /*30*/
           null, /*31*/
           :old.qtde_produzir_tecelagem, /*32*/
           null, /*33*/
           :old.perc_segunda_qualidade, /*34*/
           null, /*35*/
           :old.numero_alternativa, /*36*/
           null, /*37*/
           :old.numero_roteiro /*38*/           
         );
    end;    
 end if;
end inter_tr_proj_030_log;

/

ALTER TRIGGER  INTER_TR_PROJ_030_LOG ENABLE;
/
