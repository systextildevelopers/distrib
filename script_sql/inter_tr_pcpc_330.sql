
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_330" 
  before insert or
         delete or
         update of situacao_volume on pcpc_320
  for each row

declare
   v_data_ocorr        date;
   v_qtde_registros    number(1);

   v_executa_trigger      number;

begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      select count(*)  into v_qtde_registros from fatu_115
      where fatu_115.nr_embalagem = :new.numero_volume ;

      if v_qtde_registros is null
      then v_qtde_registros := 0;
      end if;

      if inserting
      then
         if v_qtde_registros > 0
         then :new.local_caixa := 1;
         end if;
      else
         if updating
         then
            if :new.situacao_volume = 2 and
               :new.local_caixa     < 2 and
               v_qtde_registros > 0
            then :new.local_caixa := 2;
            end if;

            if (:new.situacao_volume = 0  or
                :new.situacao_volume = 1  or
                :new.situacao_volume = 3) and
                v_qtde_registros > 0
            then :new.local_caixa := 1;
            end if;

            if :new.situacao_volume = 4 or
               :new.situacao_volume = 5
            then :new.local_caixa := 0;
            end if;

            if :new.situacao_volume = 2
            then
               update pcpc_330
                  set nota_inclusao = :new.nota_fiscal,
                      serie_nota    = :new.serie_nota
               where pcpc_330.pedido_venda  = :new.pedido_venda
                and  pcpc_330.nr_volume     = :new.numero_volume
                and  pcpc_330.flag_controle = 1;
            end if;
         else
            select count(*)  into v_qtde_registros from fatu_115
            where fatu_115.nr_embalagem = :old.numero_volume ;

            if v_qtde_registros is null
            then v_qtde_registros := 0;
            end if;


            if v_qtde_registros > 0
            then
               delete fatu_115
               where fatu_115.nr_embalagem = :old.numero_volume ;
            end if;
         end if;
      end if;

   end if;
end inter_tr_pcpc_330;
-- ALTER TRIGGER "INTER_TR_PCPC_330" ENABLE
 

/

exec inter_pr_recompile;

