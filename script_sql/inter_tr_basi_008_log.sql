
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_008_LOG" 
   after insert or delete or update
       of  CODIGO_PROJETO,                  SEQUENCIA_PROJETO,
           CODIGO_ATIVIDADE,                SEQUENCIA_ATIVIDADE,
           CNPJ9_FORNECEDOR,                CNPJ4_FORNECEDOR,
           CNPJ2_FORNECEDOR,                NOME_CONTATO
   on basi_008
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_cod_tipo_produto_n     varchar2(60);
   ws_tipo_produto_n         varchar2(60);
   ws_desc_atividade_n       varchar2(60);
   ws_desc_projeto_n         varchar2(60);
   ws_nome_fornecedor_n      varchar2(60);

   ws_cod_tipo_produto_o     varchar2(60);
   ws_tipo_produto_o         varchar2(60);
   ws_desc_atividade_o       varchar2(60);
   ws_desc_projeto_o         varchar2(60);
   ws_nome_fornecedor_o      varchar2(60);

   ws_teve                   varchar2(1);

   long_aux                  varchar2(2000);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = :new.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_n := ' ';
      end;

      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n;
      exception when no_data_found then
         ws_tipo_produto_n := ' ';
      end;

      begin
         select basi_002.descricao
         into ws_desc_atividade_n
         from basi_002
         where basi_002.codigo_atividade = :new.codigo_atividade;
      exception when no_data_found then
         ws_desc_atividade_n := ' ';
      end;

      begin
         select basi_001.descricao_projeto
         into ws_desc_projeto_n
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_n := ' ';
      end;

      begin
         select supr_010.nome_fornecedor
         into ws_nome_fornecedor_n
         from supr_010
         where supr_010.fornecedor9 = :new.cnpj9_fornecedor
           and supr_010.fornecedor4 = :new.cnpj4_fornecedor
           and supr_010.fornecedor2 = :new.cnpj2_fornecedor;
      exception when no_data_found then
         ws_nome_fornecedor_n := ' ';
      end;

      INSERT INTO hist_100
         ( tabela,             operacao,
           data_ocorr,         aplicacao,
           usuario_rede,       maquina_rede,
           str01,              num05,
           long01
         )
      VALUES
         ( 'BASI_008',         'I',
           sysdate,            ws_aplicativo,
           ws_usuario_rede,    ws_maquina_rede,
           :new.codigo_projeto, :new.sequencia_projeto,
           inter_fn_buscar_tag('lb34970#FORNECEDORES DA ATIVIDADE DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                                    chr(10)               ||
                                    chr(10)               ||
           inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.codigo_projeto    ||
           ' - '                       || ws_desc_projeto_n      ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb08486#TIPO DE PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_projeto ||
           ' - '                       || ws_cod_tipo_produto_n  ||
           ' - '                       || ws_tipo_produto_n      ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_atividade    ||
           ' - '                       || :new.codigo_atividade  ||
           ' - '                       || ws_desc_atividade_n    ||
           chr(10)                                              ||
           inter_fn_buscar_tag('lb00699FORNECEDOR::',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:new.cnpj9_fornecedor,'000000000')     || '/'
                                       || to_char(:new.cnpj4_fornecedor,'0000')          || '-'
                                       || to_char(:new.cnpj2_fornecedor,'00')            ||
           ' - '                       || ws_nome_fornecedor_n                           ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb02269#CONTATO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.nome_contato
        );
   end if;

   if updating
   then
      begin
         select basi_001.descricao_projeto
         into ws_desc_projeto_n
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_n := ' ';
      end;

      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = :new.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_n := ' ';
      end;

      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n;
      exception when no_data_found then
         ws_tipo_produto_n := ' ';
      end;

      begin
         select basi_002.descricao
         into ws_desc_atividade_n
         from basi_002
         where basi_002.codigo_atividade = :new.codigo_atividade;
      exception when no_data_found then
         ws_desc_atividade_n := ' ';
      end;

      begin
         select supr_010.nome_fornecedor
         into ws_nome_fornecedor_o
         from supr_010
         where supr_010.fornecedor9 = :old.cnpj9_fornecedor
           and supr_010.fornecedor4 = :old.cnpj4_fornecedor
           and supr_010.fornecedor2 = :old.cnpj2_fornecedor;
      exception when no_data_found then
         ws_nome_fornecedor_o := ' ';
      end;

      begin
         select supr_010.nome_fornecedor
         into ws_nome_fornecedor_n
         from supr_010
         where supr_010.fornecedor9 = :new.cnpj9_fornecedor
           and supr_010.fornecedor4 = :new.cnpj4_fornecedor
           and supr_010.fornecedor2 = :new.cnpj2_fornecedor;
      exception when no_data_found then
         ws_nome_fornecedor_n := ' ';
      end;

      ws_teve := 'n';

      long_aux := long_aux ||
           inter_fn_buscar_tag('lb34970#FORNECEDORES DA ATIVIDADE DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                  chr(10)                    ||
                  chr(10);
      long_aux := long_aux ||
           inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.codigo_projeto    ||
           ' - '                       || ws_desc_projeto_n      ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb08486#TIPO DE PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_projeto ||
           ' - '                       || ws_cod_tipo_produto_n  ||
           ' - '                       || ws_tipo_produto_n      ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_atividade    ||
           ' - '                       || :new.codigo_atividade  ||
           ' - '                       || ws_desc_atividade_n    ||
           chr(10);

      if (:old.cnpj9_fornecedor <> :new.cnpj9_fornecedor) or
         (:old.cnpj4_fornecedor <> :new.cnpj4_fornecedor) or
         (:old.cnpj2_fornecedor <> :new.cnpj2_fornecedor)
      then
         ws_teve := 's';

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb00699FORNECEDOR::',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.cnpj9_fornecedor,'000000000')     || '/'
                                       || to_char(:old.cnpj4_fornecedor,'0000')          || '-'
                                       || to_char(:old.cnpj2_fornecedor,'00')            ||
           ' - '                       || ws_nome_fornecedor_o                           ||
           '-> '                       || to_char(:new.cnpj9_fornecedor,'000000000')     || '/'
                                       || to_char(:new.cnpj4_fornecedor,'0000')          || '-'
                                       || to_char(:new.cnpj2_fornecedor,'00')            ||
           ' - '                       || ws_nome_fornecedor_n
                                       || chr(10);
      end if;

      if :old.nome_contato <> :new.nome_contato
      then
          ws_teve := 's';

          long_aux := long_aux ||
             inter_fn_buscar_tag('lb02269#CONTATO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                           || :old.nome_contato    ||
               ' -> '                      || :new.nome_contato
                                           || chr(10);
      end if;

      if ws_teve = 's'
      then
         INSERT INTO hist_100
            ( tabela,              operacao,
              data_ocorr,          aplicacao,
              usuario_rede,        maquina_rede,
              str01,               num05,
              long01
            )
         VALUES
            ( 'BASI_008',          'A',
              sysdate,             ws_aplicativo,
              ws_usuario_rede,     ws_maquina_rede,
              :new.codigo_projeto, :new.sequencia_projeto,
              long_aux
           );
      end if;
   end if;

   if deleting
   then
      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_o
         from basi_101
         where basi_101.codigo_projeto    = :old.codigo_projeto
           and basi_101.sequencia_projeto = :old.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_o := ' ';
      end;

      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produto_o
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_o;
      exception when no_data_found then
         ws_tipo_produto_o := ' ';
      end;

      begin
         select basi_002.descricao
         into ws_desc_atividade_o
         from basi_002
         where basi_002.codigo_atividade = :old.codigo_atividade;
      exception when no_data_found then
         ws_desc_atividade_o := ' ';
      end;

      begin
         select basi_001.descricao_projeto
         into ws_desc_projeto_o
         from basi_001
         where basi_001.codigo_projeto = :old.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_o := ' ';
      end;

      begin
         select supr_010.nome_fornecedor
         into ws_nome_fornecedor_o
         from supr_010
         where supr_010.fornecedor9 = :old.cnpj9_fornecedor
           and supr_010.fornecedor4 = :old.cnpj4_fornecedor
           and supr_010.fornecedor2 = :old.cnpj2_fornecedor;
      exception when no_data_found then
         ws_nome_fornecedor_o := ' ';
      end;

      begin
         INSERT INTO hist_100 (
            tabela,             operacao,
            data_ocorr,         aplicacao,
            usuario_rede,       maquina_rede,
            str01,              num05,
            long01
         ) VALUES (
            'BASI_008',         'D',
            sysdate,            ws_aplicativo,
            ws_usuario_rede,    ws_maquina_rede,
            :old.codigo_projeto, :old.sequencia_projeto,
            inter_fn_buscar_tag('lb34970#FORNECEDORES DA ATIVIDADE DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                                     chr(10)               ||
                                     chr(10)               ||
            inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.codigo_projeto    ||
            ' - '                       || ws_desc_projeto_o      ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb08486#TIPO DE PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.sequencia_projeto ||
            ' - '                       || ws_cod_tipo_produto_o  ||
            ' - '                       || ws_tipo_produto_o      ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.sequencia_atividade    ||
            ' - '                       || :old.codigo_atividade  ||
            ' - '                       || ws_desc_atividade_o    ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb00699FORNECEDOR::',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || to_char(:old.cnpj9_fornecedor,'000000000')     || '/'
                                        || to_char(:old.cnpj4_fornecedor,'0000')          || '-'
                                        || to_char(:old.cnpj2_fornecedor,'00')            ||
            ' - '                       || ws_nome_fornecedor_o                           ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb02269#CONTATO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.nome_contato
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(008-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;
end inter_tr_basi_008_log;

-- ALTER TRIGGER "INTER_TR_BASI_008_LOG" ENABLE
 

/

exec inter_pr_recompile;

