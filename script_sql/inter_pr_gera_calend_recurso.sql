CREATE OR REPLACE PROCEDURE "INTER_PR_GERA_CALEND_RECURSO"
  (p_data_inicial          in  date,      p_data_final    in date,
   p_tipo_alocacao         in  number,    p_tipo_recurso  in number,
   p_codigo_recurso        in  varchar2,  p_centro_custo  in number,
   p_qtde_recursos         in  number,    p_resultado     out number)
is
   numero_registros        number;
   v_tempo_turno01_185     number;
   v_tempo_turno02_185     number;
   v_tempo_turno03_185     number;
   v_tempo_turno04_185     number;
   v_tempo_turno01_189     number;
   v_tempo_turno02_189     number;
   v_tempo_turno03_189     number;
   v_tempo_turno04_189     number;
   v_tempo_disponivel      number;
   v_hora_ini_turno01_185  date;
   v_hora_ini_turno02_185  date;
   v_hora_ini_turno03_185  date;
   v_hora_ini_turno04_185  date;
   v_hora_fim_turno01_185  date;
   v_hora_fim_turno02_185  date;
   v_hora_fim_turno03_185  date;
   v_hora_fim_turno04_185  date;
   v_hora_ini_turno01_189  date;
   v_hora_ini_turno02_189  date;
   v_hora_ini_turno03_189  date;
   v_hora_ini_turno04_189  date;
   v_hora_fim_turno01_189  date;
   v_hora_fim_turno02_189  date;
   v_hora_fim_turno03_189  date;
   v_hora_fim_turno04_189  date;
   v_hora_inicio_185       date;
   v_hora_termino_185      date;
   v_hora_inicio           date;
   v_hora_termino          date;
   v_intervalo_turno01_185 number;
   v_intervalo_turno02_185 number;
   v_intervalo_turno03_185 number;
   v_intervalo_turno04_185 number;
   v_intervalo_turno01     number;
   v_intervalo_turno02     number;
   v_intervalo_turno03     number;
   v_intervalo_turno04     number;
   v_nr_registro_189       number;
   v_rpt_pette             number;
   v_data_inicial          date;
   v_data_final            date;
begin

   if p_data_inicial is null
   then v_data_inicial := trunc(sysdate, 'dd');
   else v_data_inicial := p_data_inicial;
   end if;

   if p_data_final is null
   then v_data_final := trunc(sysdate+180, 'dd');
   else v_data_final := p_data_final;
   end if;

   begin
      select basi_185.tempo_turno1,     basi_185.tempo_turno2,
             basi_185.tempo_turno3,     basi_185.tempo_turno4,
             basi_185.horaini_t1,       basi_185.horafim_t1,
             basi_185.horaini_t2,       basi_185.horafim_t2,
             basi_185.horaini_t3,       basi_185.horafim_t3,
             basi_185.horaini_t4,       basi_185.horafim_t4,
             decode(basi_185.aplicacao_intervalo_t1,1,0,nvl(to_number((basi_185.intervalofim_t1) - (basi_185.intervaloini_t1))*1440,0)),
             decode(basi_185.aplicacao_intervalo_t2,1,0,nvl(to_number((basi_185.intervalofim_t2) - (basi_185.intervaloini_t2))*1440,0)),
             decode(basi_185.aplicacao_intervalo_t3,1,0,nvl(to_number((basi_185.intervalofim_t3) - (basi_185.intervaloini_t3))*1440,0)),
             decode(basi_185.aplicacao_intervalo_t4,1,0,nvl(to_number((basi_185.intervalofim_t4) - (basi_185.intervaloini_t4))*1440,0))
      into   v_tempo_turno01_185,        v_tempo_turno02_185,
             v_tempo_turno03_185,        v_tempo_turno04_185,
             v_hora_ini_turno01_185,     v_hora_fim_turno01_185,
             v_hora_ini_turno02_185,     v_hora_fim_turno02_185,
             v_hora_ini_turno03_185,     v_hora_fim_turno03_185,
             v_hora_ini_turno04_185,     v_hora_fim_turno04_185,
             v_intervalo_turno01_185,    v_intervalo_turno02_185,
             v_intervalo_turno03_185,    v_intervalo_turno04_185
      from basi_185
      where basi_185.centro_custo = p_centro_custo;
      exception
        when others then
             v_tempo_turno01_185      := 0;
             v_tempo_turno02_185      := 0;
             v_tempo_turno03_185      := 0;
             v_tempo_turno04_185      := 0;
             v_hora_ini_turno01_185   := null;
             v_hora_fim_turno01_185   := null;
             v_hora_ini_turno02_185   := null;
             v_hora_fim_turno02_185   := null;
             v_hora_ini_turno03_185   := null;
             v_hora_fim_turno03_185   := null;
             v_hora_ini_turno04_185   := null;
             v_hora_fim_turno04_185   := null;
             v_intervalo_turno01_185  := 0;
             v_intervalo_turno02_185  := 0;
             v_intervalo_turno03_185  := 0;
             v_intervalo_turno04_185  := 0;
   end;

   -- Atualizando a hora inicio e fim do recurso
   if v_hora_ini_turno01_185 is not null and v_tempo_turno01_185 > 0
   then
      v_hora_inicio_185 := v_hora_ini_turno01_185;
      v_hora_termino_185 := v_hora_fim_turno01_185;
   end if;
   if v_hora_ini_turno02_185 is not null and v_tempo_turno02_185 > 0
   then
      if v_tempo_turno01_185 = 0
      then
         v_hora_inicio_185 := v_hora_ini_turno02_185;
      end if;
      v_hora_termino_185 := v_hora_fim_turno02_185;
   end if;
   if v_hora_ini_turno03_185 is not null and v_tempo_turno03_185 > 0
   then
      if v_tempo_turno01_185 = 0 and v_tempo_turno02_185 = 0
      then
         v_hora_inicio_185 := v_hora_ini_turno03_185;
      end if;
      v_hora_termino_185 := v_hora_fim_turno03_185;
   end if;
   if v_hora_ini_turno04_185 is not null and v_tempo_turno04_185 > 0
   then
      if v_tempo_turno01_185 = 0 and v_tempo_turno02_185 = 0  and v_tempo_turno03_185 = 0
      then
         v_hora_inicio_185 := v_hora_ini_turno04_185;
      end if;
      v_hora_termino_185 := v_hora_fim_turno04_185;
   end if;

   v_hora_inicio  := v_hora_inicio_185;
   v_hora_termino := v_hora_termino_185;

-- Verifica se o rpt que esta configurado e Pettenati para tratar
   -- regras específicas de funcionamento para o painel de planejamento
   -- da costura, cost_f003... PAINEL DE PLANIFICAÇÃO.
   v_rpt_pette := 0;
   begin
      select count(*)
      into v_rpt_pette
      from fatu_500
      where fatu_500.rpt_ordem_benef = 'pcpb_opb002';
   end;

   -- Fazendo um LOOP pelo calendario para atualiar o calendario do recurso
   for reg_basi_260 in (select basi_260.dia_semana,
                               basi_260.data_calendario
                        from basi_260
                        where  basi_260.data_calendario >= v_data_inicial
                          and  basi_260.data_calendario <= v_data_final
                          and (basi_260.dia_util         = 0 --- Dia util
                           or exists (select 1 from basi_189
                                       where (basi_189.dia_semana       = basi_260.dia_semana
                                          or  basi_189.data_excecao     = basi_260.data_calendario)
                                         and  basi_189.centro_custo     = p_centro_custo
                                         and (v_rpt_pette > 0
                                             and (basi_189.divisao_producao = p_codigo_recurso or basi_189.divisao_producao = 0)
                                             or v_rpt_pette = 0)))
                        order by basi_260.data_calendario asc)
   loop
      --- Quando ainda faltam tempo a produzir
      begin
         v_intervalo_turno01 := 0;
         v_intervalo_turno02 := 0;
         v_intervalo_turno03 := 0;
         v_intervalo_turno04 := 0;
         v_nr_registro_189   := 1;

         --- Busca uma exe??o de tempso por tudo e horario para o dia
         -- v_nr_registro_189        := 0;
         v_tempo_turno01_189      := 0;
         v_tempo_turno02_189      := 0;
         v_tempo_turno03_189      := 0;
         v_tempo_turno04_189      := 0;
         v_hora_ini_turno01_189   := null;
         v_hora_fim_turno01_189   := null;
         v_hora_ini_turno02_189   := null;
         v_hora_fim_turno02_189   := null;
         v_hora_ini_turno03_189   := null;
         v_hora_fim_turno03_189   := null;
         v_hora_ini_turno04_189   := null;
         v_hora_fim_turno04_189   := null;

         select basi_189.tempo_turno1,    basi_189.tempo_turno2,
                basi_189.tempo_turno3,    basi_189.tempo_turno4,
                basi_189.horaini_t1,      basi_189.horaini_t2,
                basi_189.horaini_t3,      basi_189.horaini_t4,
                basi_189.horafim_t1,      basi_189.horafim_t2,
                basi_189.horafim_t3,      basi_189.horafim_t4
         into   v_tempo_turno01_189,      v_tempo_turno02_189,
                v_tempo_turno03_189,      v_tempo_turno04_189,
                v_hora_ini_turno01_189,   v_hora_ini_turno02_189,
                v_hora_ini_turno03_189,   v_hora_ini_turno04_189,
                v_hora_fim_turno01_189,   v_hora_fim_turno02_189,
                v_hora_fim_turno03_189,   v_hora_fim_turno04_189
         from basi_189
         where  basi_189.centro_custo     = p_centro_custo
           and  basi_189.data_excecao     = reg_basi_260.data_calendario -- Primeiro procura o tempo disponivel por data.
         and (v_rpt_pette > 0 and (basi_189.divisao_producao = p_codigo_recurso
                                    or basi_189.divisao_producao = 0)
               or v_rpt_pette = 0);
         exception
            when others then
               v_nr_registro_189        := 0;
               v_tempo_turno01_189      := 0;
               v_tempo_turno02_189      := 0;
               v_tempo_turno03_189      := 0;
               v_tempo_turno04_189      := 0;
               v_hora_ini_turno01_189   := null;
               v_hora_fim_turno01_189   := null;
               v_hora_ini_turno02_189   := null;
               v_hora_fim_turno02_189   := null;
               v_hora_ini_turno03_189   := null;
               v_hora_fim_turno03_189   := null;
               v_hora_ini_turno04_189   := null;
               v_hora_fim_turno04_189   := null;
      end;

      if v_nr_registro_189 = 0 -- Não encontrou registro na basi_189 por data.
                               -- Busca pelo dia da semana...
      then
         v_nr_registro_189 := 1;
         ---  Se n?o encontrar exce??o para o dia, verifica se tem exec??o para o dia de semana
         begin
            select basi_189.tempo_turno1,  basi_189.tempo_turno2,
                   basi_189.tempo_turno3,  basi_189.tempo_turno4,
                   basi_189.horaini_t1,    basi_189.horaini_t2,
                   basi_189.horaini_t3,    basi_189.horaini_t4,
                   basi_189.horafim_t1,    basi_189.horafim_t2,
                   basi_189.horafim_t3,    basi_189.horafim_t4
            into v_tempo_turno01_189,      v_tempo_turno02_189,
                 v_tempo_turno03_189,      v_tempo_turno04_189,
                 v_hora_ini_turno01_189,   v_hora_ini_turno02_189,
                 v_hora_ini_turno03_189,   v_hora_ini_turno04_189,
                 v_hora_fim_turno01_189,   v_hora_fim_turno02_189,
                 v_hora_fim_turno03_189,   v_hora_fim_turno04_189
            from basi_189
            where  basi_189.centro_custo     = p_centro_custo
              and  basi_189.dia_semana       = reg_basi_260.dia_semana -- Procura o tempo disponível por dia da semana.
              --and (basi_189.divisao_producao = p_codigo_recurso or basi_189.divisao_producao = 0)
              and (v_rpt_pette > 0 and (basi_189.divisao_producao = p_codigo_recurso
                                    or basi_189.divisao_producao = 0)
              or v_rpt_pette = 0)
              and  basi_189.data_excecao     = '01-jan-80';
            exception
               when others then
                 v_nr_registro_189        := 0;
                 v_tempo_turno01_189      := 0;
                 v_tempo_turno02_189      := 0;
                 v_tempo_turno03_189      := 0;
                 v_tempo_turno04_189      := 0;
                 v_hora_ini_turno01_189   := null;
                 v_hora_fim_turno01_189   := null;
                 v_hora_ini_turno02_189   := null;
                 v_hora_fim_turno02_189   := null;
                 v_hora_ini_turno03_189   := null;
                 v_hora_fim_turno03_189   := null;
                 v_hora_ini_turno04_189   := null;
                 v_hora_fim_turno04_189   := null;
         end;

         -- Se não encontrou nenhum registro de exceção, carrega os registros por centro de custo.
         if v_nr_registro_189 = 0
         then
            --- Se n?o enconttrar exe??o nenhuma, vale o tempo default do cadastro de centro de custos
            v_tempo_turno01_189 := v_tempo_turno01_185;
            v_tempo_turno02_189 := v_tempo_turno02_185;
            v_tempo_turno03_189 := v_tempo_turno03_185;
            v_tempo_turno04_189 := v_tempo_turno04_185;
            v_hora_inicio       := v_hora_inicio_185;
            v_hora_termino      := v_hora_termino_185;
            v_intervalo_turno01 := v_intervalo_turno01_185;
            v_intervalo_turno02 := v_intervalo_turno02_185;
            v_intervalo_turno03 := v_intervalo_turno03_185;
            v_intervalo_turno04 := v_intervalo_turno04_185;
          end if;
      end if;

      -- Atualizando a hora inicio e fim do recurso
      if v_hora_ini_turno01_189 is not null and v_tempo_turno01_189 > 0
      then
         v_hora_inicio  := v_hora_ini_turno01_189;
         v_hora_termino := v_hora_fim_turno01_189;
      end if;

      if v_hora_ini_turno02_189 is not null and v_tempo_turno02_189 > 0
      then
         if v_tempo_turno01_189 = 0
         then
            v_hora_inicio := v_hora_ini_turno02_189;
         end if;

         v_hora_termino := v_hora_fim_turno02_189;
      end if;

      if v_hora_ini_turno03_189 is not null and v_tempo_turno03_189 > 0
      then
         if v_tempo_turno01_189 = 0 and v_tempo_turno02_189 = 0
         then
            v_hora_inicio := v_hora_ini_turno03_189;
         end if;

         v_hora_termino := v_hora_fim_turno03_189;
      end if;

      if v_hora_ini_turno04_189 is not null and v_tempo_turno04_189 > 0
      then
         if v_tempo_turno01_189 = 0 and v_tempo_turno02_189 = 0  and v_tempo_turno03_189 = 0
         then
            v_hora_inicio := v_hora_ini_turno04_189;
         end if;

         v_hora_termino := v_hora_fim_turno04_189;
      end if;

      v_tempo_disponivel := 0;
      v_tempo_disponivel := (v_tempo_turno01_189 + v_tempo_turno02_189 +
                              v_tempo_turno03_189 + v_tempo_turno04_189)
                           - (v_intervalo_turno01      + v_intervalo_turno02 +
                              v_intervalo_turno03      + v_intervalo_turno04);

-- PETTENATI (Se não for Pettenati, faz este cálculo de ajuste
      -- de tempo disponível conforme padrão...
      -- Para Pettenati, o tempo tem que ser considerado o total e o desconto é nas paradas na equipe
      -- ou cadastro de exceção por centro de custo.
      if v_rpt_pette = 0
      then
         if v_tempo_disponivel = 1440
         then
            v_hora_termino := v_hora_inicio + (1439/1440);
         else
            v_hora_termino := v_hora_inicio + (v_tempo_disponivel/1440);
         end if;
      end if;

      --- Verifica se tem exec??o de turno, reduz o tempo disponivel
      for reg_basi_275 in (select basi_275.numero_turno
                           from basi_275
                           where basi_275.data_nao_util = reg_basi_260.data_calendario
                             and basi_275.centro_custo  = p_centro_custo)
      loop
         if reg_basi_275.numero_turno = 1
         then
            v_tempo_disponivel := v_tempo_disponivel - v_tempo_turno01_189 + v_intervalo_turno01;
         end if;

         if reg_basi_275.numero_turno = 2
         then
            v_tempo_disponivel := v_tempo_disponivel - v_tempo_turno02_189 + v_intervalo_turno02;
         end if;

         if reg_basi_275.numero_turno = 3
         then
            v_tempo_disponivel := v_tempo_disponivel - v_tempo_turno03_189 + v_intervalo_turno03;
         end if;

         if reg_basi_275.numero_turno = 4
         then
            v_tempo_disponivel := v_tempo_disponivel - v_tempo_turno04_189 + v_intervalo_turno04;
         end if;
      end loop; -- basi_275

       -- Se for confecção e encontrar os registros de tempo de turno igual a zero,
      -- zera o tempo disponível.
      if p_tipo_alocacao = 1 and v_tempo_turno01_189 = 0
                             and v_tempo_turno02_189 = 0
                             and v_tempo_turno03_189 = 0
                             and v_tempo_turno04_189 = 0
      then
         v_tempo_disponivel := 0.00;
      end if;

      -- Só insere os registros na Pettenati se o tempo disponível for maior que zero.
      -- Para demais clientes insere os registros se o tempo disponível for maior ou igual a zero.
      if ((v_rpt_pette > 0 and v_tempo_disponivel > 0) or (v_rpt_pette = 0 and v_tempo_disponivel >= 0))
      then
       begin
            select nvl(count(1),0)
            into numero_registros
            from tmrp_660
            where tmrp_660.tipo_alocacao  = p_tipo_alocacao
              and tmrp_660.tipo_recurso   = p_tipo_recurso
              and tmrp_660.codigo_recurso = p_codigo_recurso
              and tmrp_660.data_recurso   = reg_basi_260.data_calendario;
         end;

         -- atualiza a tabela de calendario do recurso
         if numero_registros > 0
         then
           begin
              update tmrp_660
            set tmrp_660.tempo_disponivel = v_tempo_disponivel,
                    tmrp_660.qtde_recursos    = p_qtde_recursos,
                    tmrp_660.hora_inicio      = v_hora_inicio,
                    tmrp_660.hora_termino     = v_hora_termino
              where tmrp_660.tipo_alocacao  = p_tipo_alocacao
                and tmrp_660.tipo_recurso   = p_tipo_recurso
                and tmrp_660.codigo_recurso = p_codigo_recurso
                and tmrp_660.data_recurso   = reg_basi_260.data_calendario;
           end;
        else
           begin
               insert into tmrp_660
                 (tipo_alocacao,      tipo_recurso,
                  codigo_recurso,     data_recurso,
                  hora_inicio,        hora_termino,
                  tempo_disponivel,   qtde_recursos)
               values
                 (p_tipo_alocacao,    p_tipo_recurso,
                  p_codigo_recurso,   reg_basi_260.data_calendario,
                  v_hora_inicio,      v_hora_termino,
                  v_tempo_disponivel, p_qtde_recursos);
            end;
         end if;
      end if;
   end loop;

   select count(*)
   into p_resultado
   from tmrp_660
   where tmrp_660.tipo_alocacao  = p_tipo_alocacao
     and tmrp_660.tipo_recurso   = p_tipo_recurso
     and tmrp_660.codigo_recurso = p_codigo_recurso
     and tmrp_660.data_recurso   >= v_data_inicial
     and tmrp_660.data_recurso   <= v_data_final;

end inter_pr_gera_calend_recurso;
 
--/

--exec inter_pr_recompile;

