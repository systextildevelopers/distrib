CREATE TABLE oper_112 (
codigo_empresa number(3)     default 0,
usuario        varchar(50)   default  ' ',
data_inclusao  date default  sysdate,
senha_ant      varchar2(200) default 'N');

alter table oper_112 add constraint pk_oper_112 primary key (codigo_empresa,usuario,data_inclusao);
/
