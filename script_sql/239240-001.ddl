create table obrf_142
(
   CST      number(3),
   CVF_ICM_DIFERENC     number(3) default 3
);


insert into obrf_142 (CST) values (00);
insert into obrf_142 (CST) values (10);
insert into obrf_142 (CST) values (20);
insert into obrf_142 (CST) values (30);
insert into obrf_142 (CST) values (40);
insert into obrf_142 (CST) values (41);
insert into obrf_142 (CST) values (50);
insert into obrf_142 (CST) values (51);
insert into obrf_142 (CST) values (60);
insert into obrf_142 (CST) values (70);
insert into obrf_142 (CST) values (90);



insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f142', 'Relacionamneto CST com CVF_ICM_DIFERENC',0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f142', 'obrf_menu' ,1, 0, 'S', 'S', 'S', 'S');

