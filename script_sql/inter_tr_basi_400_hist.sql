
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_400_HIST" 
BEFORE INSERT
    OR UPDATE of string_03, string_04, int_01, int_02,
	int_03, int_04, int_05, int_06,
	int_07, complemento, nivel, grupo,
	subgrupo, item, tipo_informacao, codigo_informacao,
	valor, sequencia, data_01, data_02,
	data_03, situacao, classificacao, valor_02,
	valor_03, valor_04, observacao, usuario_bloq,
	data_bloq, hora_bloq, descricao_01, descricao_02,
	descricao_03, valor_05, valor_06, data_04,
	valor_07, valor_08, valor_09, valor_10,
	valor_11, valor_12, valor_13, valor_14,
	valor_15, exige_baixa, cnpj9, cnpj4,
	cnpj2, string_01, string_02
    OR DELETE on basi_400
for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_tipo_ocorr             varchar2(1);
   ws_nome_programa          hdoc_090.programa%type;
begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   
   ws_nome_programa := inter_fn_nome_programa(ws_sid); 

   if inserting or updating
   then
      if :new.tipo_informacao = 12 or :new.tipo_informacao = 37
      then
         if INSERTING
         then
            ws_tipo_ocorr := 'I';
         else
            ws_tipo_ocorr := 'U';
         end if;

         begin
            INSERT INTO basi_400_hist (
               tipo_ocorr,                     data_ocorr,
               usuario_rede,                   maquina_rede,
               aplicacao,                      usuario,
               programa,                       hora_ocorrencia,

               nivel,                          grupo,
               subgrupo,                       item,
               tipo_informacao,                codigo_informacao,
               valor,                          sequencia,
               data_01,                        data_02,
               data_03,                        situacao,
               classificacao,                  valor_02,
               valor_03,                       valor_04
            ) VALUES (
               ws_tipo_ocorr,                  sysdate,
               ws_usuario_rede,                ws_maquina_rede,
               ws_aplicativo,                  ws_usuario_systextil,
               ws_nome_programa,               sysdate,

               :new.nivel,                     :new.grupo,
               :new.subgrupo,                  :new.item,
               :new.tipo_informacao,           :new.codigo_informacao,
               :new.valor,                     :new.sequencia,
               :new.data_01,                   :new.data_02,
               :new.data_03,                   :new.situacao,
               :new.classificacao,             :new.valor_02,
               :new.valor_03,                  :new.valor_04
            );
         end;
      end if;
   end if;

   if deleting
   then
      if :old.tipo_informacao = 12 or :old.tipo_informacao = 37
      then
         ws_tipo_ocorr := 'D';

         begin
            INSERT INTO basi_400_hist (
               tipo_ocorr,                     data_ocorr,
               usuario_rede,                   maquina_rede,
               aplicacao,                      usuario,
               programa,                       hora_ocorrencia,

               nivel,                          grupo,
               subgrupo,                       item,
               tipo_informacao,                codigo_informacao,
               valor,                          sequencia,
               data_01,                        data_02,
               data_03,                        situacao,
               classificacao,                  valor_02,
               valor_03,                       valor_04
            ) VALUES (
               ws_tipo_ocorr,                  sysdate,
               ws_usuario_rede,                ws_maquina_rede,
               ws_aplicativo,                  ws_usuario_systextil,
               ws_nome_programa,               sysdate,

               :old.nivel,                     :old.grupo,
               :old.subgrupo,                  :old.item,
               :old.tipo_informacao,           :old.codigo_informacao,
               :old.valor,                     :old.sequencia,
               :old.data_01,                   :old.data_02,
               :old.data_03,                   :old.situacao,
               :old.classificacao,             :old.valor_02,
               :old.valor_03,                  :old.valor_04
            );
         end;
      end if;
   end if;
end inter_tr_basi_400_hist;

-- ALTER TRIGGER "INTER_TR_BASI_400_HIST" ENABLE
 

/

exec inter_pr_recompile;

