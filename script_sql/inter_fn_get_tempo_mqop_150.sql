create or replace function inter_fn_get_tempo_mqop_150 (p_numero_grafico   in number,
                                                        p_grupo_maquina    in varchar2,
                                                        p_subgrupo_maquina in varchar2)
return number is
   v_tempo_grafico number;
begin
   v_tempo_grafico := 0.0000;

   -- Busca o tempo do gr�fico prevendo todas as formas de cadastro poss�ves.
   -- 1) Por grupo e subgrupo definidos
   begin
      select nvl(sum(mqop_150.tempo),0)
        into v_tempo_grafico
        from mqop_150
       where mqop_150.numero_grafico   = p_numero_grafico
         and mqop_150.grupo_maquina    = p_grupo_maquina
         and mqop_150.subgrupo_maquina = p_subgrupo_maquina;
   end;

   if v_tempo_grafico = 0
   then
      -- 2) Por grupo definido e subgrupo '000'
      begin
         select nvl(sum(mqop_150.tempo),0)
           into v_tempo_grafico
           from mqop_150
          where mqop_150.numero_grafico   = p_numero_grafico
            and mqop_150.grupo_maquina    = p_grupo_maquina
            and mqop_150.subgrupo_maquina = '000';
      end;
   end if;

   if v_tempo_grafico = 0.0000
   then
      -- 3) Por grupo e subgrupo sem defini��o
      begin
         select nvl(sum(mqop_150.tempo),0)
           into v_tempo_grafico
           from mqop_150
          where mqop_150.numero_grafico   = p_numero_grafico
            and mqop_150.grupo_maquina    = '0000'
            and mqop_150.subgrupo_maquina = '000';
      end;
   end if;

   if v_tempo_grafico = 0
   then
      -- 1) Por grupo definido e subgrupo 'XXX'
      begin
         select  nvl(sum(mqop_115.tempo),0)
           into  v_tempo_grafico
           from  mqop_115
          where  mqop_115.numero_grafico   = p_numero_grafico
            and  mqop_115.grupo_maquina    = p_grupo_maquina
            and (mqop_115.subgrupo_maquina = p_subgrupo_maquina or mqop_115.subgrupo_maquina = 'XXX');
      end;
   end if;
   
   return v_tempo_grafico;
end inter_fn_get_tempo_mqop_150;
/
exec inter_pr_recompile;
/* versao: 1 */
