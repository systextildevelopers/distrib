
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_159_ID" 
before insert on obrf_159 for each row
declare

   v_nr_registro number;

begin

   select seq_obrf_159.nextval
   into v_nr_registro from dual;

   :new.id := v_nr_registro;

end inter_tr_obrf_159_id;

-- ALTER TRIGGER "INTER_TR_OBRF_159_ID" ENABLE
 

/

exec inter_pr_recompile;

