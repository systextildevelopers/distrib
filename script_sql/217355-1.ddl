CREATE TABLE HDOC_032 (
USUARIO VARCHAR2(15),
EMPRESA NUMBER(3),
ENDERECO VARCHAR2(40),
NOME VARCHAR2(120),
USER_ID VARCHAR2(200),
SENHA VARCHAR2(500),
CORPO VARCHAR2(2000),
CONSTRAINT FK_HDOC_032_030 FOREIGN KEY (USUARIO, EMPRESA) REFERENCES HDOC_030 ON DELETE CASCADE
);

INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR)
VALUES ('mail.defaultSender.address', 0, 'lb03447', 'fy14149', null);

INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR)
VALUES ('mail.defaultSender.userId', 0, 'lb03446', 'fy07292', null);

INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR)
VALUES ('mail.defaultSender.password', 0, 'lb08754', 'fy07293', null);

UPDATE HDOC_035 SET DESCRICAO='Credenciais do usu�rio para envio de e-mail'
WHERE CODIGO_PROGRAMA='oper_f540';
UPDATE HDOC_036 SET DESCRICAO='Credenciais do usu�rio para envio de e-mail'
WHERE CODIGO_PROGRAMA='oper_f540' AND LOCALE='pt_BR';
UPDATE HDOC_036 SET DESCRICAO='Credenciales de usuario para enviar correo electr�nico'
WHERE CODIGO_PROGRAMA='oper_f540' AND LOCALE='es_ES';

COMMIT;
