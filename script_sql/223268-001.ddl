alter table fatu_504 add apont_qtde_prod_rolo number(1) default 0;

comment on column fatu_504.apont_qtde_prod_rolo is '0 - Aponta Quantidade Produzida no momento da entrada do Rolo Previsto 1 - Aponta Quantidade Produzida no momento da confirmação do Rolo Previsto'
