alter table crec_460 
add (
    nr_atributo number(2) default 0,
    prefixo varchar2(5) default ''
);

alter table crec_460 drop constraint PK_CREC_460;

drop index PK_CREC_460;

alter table crec_460 add constraint PK_CREC_460 primary key (tipo_processo, cod_empresa, nr_atributo);
