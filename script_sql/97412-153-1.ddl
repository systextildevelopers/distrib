create table pcpb_602
(
  NR_SOLICITACAO            NUMBER(9) default 0,
  COPIA_ORDEM_AGRUPAMENTO   NUMBER(9) default 0,
  COPIA_ORDEM_PRODUCAO      NUMBER(9) default 0,
  ORIG_ORDEM_AGRUPAMENTO    NUMBER(9) default 0,
  ORIG_ORDEM_PRODUCAO       NUMBER(9) default 0
);

create synonym systextilrpt.pcpb_602 for pcpb_602; 
 
