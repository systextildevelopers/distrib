
  CREATE OR REPLACE TRIGGER "INTER_TR_HDOC_001_LOG" 
    after insert or
          update of tipo, codigo, descricao, data1,
		data2, classificacao1, classificacao2, quantidade,
		observacao, descricao2, campo_numerico01, campo_numerico02,
		campo_numerico03, campo_numerico04, descricao3, campo_numerico05,
		campo_numerico06, valor01, descricao4, obs_ingles,
		valor02, valor03, valor04, valor05,
		campo_numerico07, campo_numerico08, campo_numerico09, campo_numerico10,
		campo_numerico11, campo_numerico12, campo_numerico13, campo_numerico14,
		campo_numerico15, campo_numerico16, campo_numerico17, campo_numerico18,
		campo_numerico19, descricao5, descricao6, descricao7,
		descricao8, descricao9, descricao10, descricao11,
		campo_numerico20, campo_numerico21, campo_numerico22, campo_numerico23,
		campo_numerico24, campo_numerico25, descricao12, descricao13,
		descricao14, descricao15, campo_numerico26, campo_numerico27,
		campo_numerico28, campo_numerico29, campo_numerico30, codigo2,
		descricao16, campo_numerico31, campo_numerico32, campo_numerico33,
		campo_numerico34, campo_numerico35, descricao17, descricao18,
		descricao19, descricao20, descricao21
          or delete on hdoc_001
    for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_tipo                    number;
   v_codigo                  number;
   v_texto_log               varchar2(2000);
   v_operacao                varchar2(1);

begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      v_tipo      := :new.tipo;
      v_codigo    := :new.codigo;

      v_texto_log := '                               ' ||
                                                     inter_fn_buscar_tag('lb34977#CONTROLA ESTOQUE NEGATIVO POR DEP�SITO/N�VEL PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                                                     chr(10)           ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb08048#DEP�SITO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :new.codigo           ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb11339#PE�A%:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :new.campo_numerico01 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb06233#TECIDO ACABADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :new.campo_numerico02 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb10227#TECIDO CR�:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :new.campo_numerico03 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb03766#FIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                                       || :new.campo_numerico04 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb34976#MAT. COMPRADOS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                                       || :new.campo_numerico05;

      v_operacao   := 'I';

   end if;

   if updating
   then
      v_tipo      := :new.tipo;
      v_codigo    := :new.codigo;

      v_texto_log := '                               ' ||
                                                     inter_fn_buscar_tag('lb34977#CONTROLA ESTOQUE NEGATIVO POR DEP�SITO/N�VEL PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                                                     chr(10)           ||
                                                     chr(10)           ||
                                                     'NEW'             ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb08048#DEP�SITO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :new.codigo           ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb11339#PE�A%:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :new.campo_numerico01 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb06233#TECIDO ACABADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :new.campo_numerico02 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb10227#TECIDO CR�:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :new.campo_numerico03 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb03766#FIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :new.campo_numerico04 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb34976#MAT. COMPRADOS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :new.campo_numerico05 ||
                                                     chr(10)           ||
                                                     chr(10)           ||
                                                     'OLD'             ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb08048#DEP�SITO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :old.codigo           ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb11339#PE�A%:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :old.campo_numerico01 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb06233#TECIDO ACABADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :old.campo_numerico02 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb10227#TECIDO CR�:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :old.campo_numerico03 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb03766#FIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :old.campo_numerico04 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb34976#MAT. COMPRADOS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :old.campo_numerico05;

      v_operacao   := 'U';

   end if;

   if deleting
   then
      v_tipo      := :old.tipo;
      v_codigo    := :old.codigo;

      v_texto_log := '                               ' ||
                                                     inter_fn_buscar_tag('lb34977#CONTROLA ESTOQUE NEGATIVO POR DEP�SITO/N�VEL PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                                                     chr(10)           ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb08048#DEP�SITO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :old.codigo           ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb11339#PE�A%:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :old.campo_numerico01 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb06233#TECIDO ACABADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :old.campo_numerico02 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb10227#TECIDO CR�:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :old.campo_numerico03 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb03766#FIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :old.campo_numerico04 ||
                                                     chr(10)           ||
                                                     inter_fn_buscar_tag('lb34976#MAT. COMPRADOS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                                     || :old.campo_numerico05;

      v_operacao   := 'D';

   end if;

   if v_tipo = 81
   then
      INSERT INTO hist_100
         (tabela,            operacao,
          data_ocorr,        usuario_rede,
          maquina_rede,      aplicacao,
          num01,             num02,
          long01)
      VALUES
         ('HDOC_001',        v_operacao,
          sysdate,           ws_usuario_rede,
          ws_maquina_rede,   ws_aplicativo,
          v_tipo,            v_codigo,
          v_texto_log);

   end if;

end inter_tr_hdoc_001_log;

-- ALTER TRIGGER "INTER_TR_HDOC_001_LOG" ENABLE
 

/

exec inter_pr_recompile;

