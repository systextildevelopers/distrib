create table crec_460 (
	tipo_processo 		number(1),
	cod_empresa 		number(3),
	endereco_van 		varchar2(100),
	endereco_processado varchar2(100),
	
	CONSTRAINT PK_CREC_460 PRIMARY KEY (TIPO_PROCESSO, COD_EMPRESA)
);
