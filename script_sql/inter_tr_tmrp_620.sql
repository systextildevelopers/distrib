
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_620" 
before insert or
       delete on tmrp_620
for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_alternativa_produto   basi_010.numero_alternati%type;
   v_total_625             number;
   v_nro_reg_tmrp_625      number;
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      begin
         select nvl(count(1),0)
         into   v_nro_reg_tmrp_625
         from tmrp_625
         where tmrp_625.ordem_planejamento = :new.ordem_planejamento
           and tmrp_625.pedido_venda       = :new.pedido_venda
           and tmrp_625.pedido_reserva     = :new.pedido_reserva;
      end;

      if v_nro_reg_tmrp_625 > 0
      then
         begin
            update tmrp_625
              set tmrp_625.qtde_areceber_programada     = 0,
                  tmrp_625.unidades_areceber_programada = 0
            where tmrp_625.ordem_planejamento = :new.ordem_planejamento
              and tmrp_625.pedido_venda       = :new.pedido_venda
              and tmrp_625.pedido_reserva       = :new.pedido_reserva;
         exception when others then
            -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_625(inter_tr_tmrp_620(3))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;

      --BUSCA ALTERNATIVA DA BASI_010 SE NAO FOI INFORMADA UMA ALTERNATIVA NO PLANEJAMENTO
      if :new.alternativa_produto <> 0
      then
         v_alternativa_produto := :new.alternativa_produto;
      else
          begin
             select basi_010.numero_alternati
             into   v_alternativa_produto
             from basi_010
             where basi_010.nivel_estrutura  = :new.nivel_produto
               and basi_010.grupo_estrutura  = :new.grupo_produto
               and basi_010.subgru_estrutura = :new.subgrupo_produto
               and basi_010.item_estrutura   = :new.item_produto;
          exception when others then
             v_alternativa_produto := 1;
          end;
      end if;

      begin
         select nvl(count(1),0)
         into v_nro_reg_tmrp_625
         from tmrp_625
         where  tmrp_625.ordem_planejamento         = :new.ordem_planejamento
           and  tmrp_625.pedido_venda               = :new.pedido_venda
           and  tmrp_625.pedido_reserva             = :new.pedido_reserva
           and  tmrp_625.seq_produto_origem         = 0
           and  tmrp_625.nivel_produto_origem       = :new.nivel_produto
           and  tmrp_625.grupo_produto_origem       = :new.grupo_produto
           and  tmrp_625.subgrupo_produto_origem    = :new.subgrupo_produto
           and  tmrp_625.item_produto_origem        = :new.item_produto
           and  tmrp_625.alternativa_produto_origem = :new.alternativa_produto
           and tmrp_625.nivel_produto               = '2';
      end;

      if v_nro_reg_tmrp_625 > 0
      then
         for reg_tmrp_630 in (select tmrp_630.ordem_planejamento,          tmrp_630.pedido_venda,
                                     tmrp_630.pedido_reserva,
                                     tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                     tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                     tmrp_625.alternativa_produto_origem,
                                     tmrp_630.area_producao,               tmrp_630.ordem_prod_compra
                              from tmrp_630, tmrp_625
                              where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
                                and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
                                and tmrp_625.pedido_reserva             = tmrp_630.pedido_reserva
                                and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
                                and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
                                and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
                                and tmrp_625.item_produto_origem        = tmrp_630.item_produto
                                and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto
                                and tmrp_625.nivel_produto              = :new.nivel_produto
                                and tmrp_625.grupo_produto              = :new.grupo_produto
                                and tmrp_625.subgrupo_produto           = :new.subgrupo_produto
                                and tmrp_625.item_produto               = :new.item_produto
                                and tmrp_625.alternativa_produto        = :new.alternativa_produto
                                and tmrp_630.area_producao              = decode(:new.nivel_produto,'2',1,
                                                                                                    '4',2,
                                                                                                    '7',4)
                              group by  tmrp_630.ordem_planejamento,          tmrp_630.pedido_venda,
                                        tmrp_630.pedido_reserva,
                                        tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                        tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                        tmrp_625.alternativa_produto_origem,
                                        tmrp_630.area_producao,               tmrp_630.ordem_prod_compra)
         loop
            begin
               insert into tmrp_615 (
                  nome_programa,                            cgc_cliente9,
                  nr_solicitacao,                           tipo_registro,

                  ordem_planejamento,                       pedido_venda,
                  pedido_reserva,
                  ordem_prod,                               area_producao,
                  nivel,                                    grupo,
                  subgrupo,                                 item,
                  alternativa
               ) values (
                  'trigger_planejamento',                   ws_sid,
                  888,                                      888,

                  reg_tmrp_630.ordem_planejamento,          reg_tmrp_630.pedido_venda,
                  reg_tmrp_630.pedido_reserva,
                  reg_tmrp_630.ordem_prod_compra,           reg_tmrp_630.area_producao,
                  reg_tmrp_630.nivel_produto_origem,        reg_tmrp_630.grupo_produto_origem,
                  reg_tmrp_630.subgrupo_produto_origem,     reg_tmrp_630.item_produto_origem,
                  reg_tmrp_630.alternativa_produto_origem
               );
            exception when others then
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end loop;
      end if;

      INTER_PR_NEC_ORDEM_PLANEJ(:new.ordem_planejamento,
                                :new.pedido_venda,
                                :new.pedido_reserva,

                                :new.codigo_projeto,
                                :new.seq_projeto,

                                :new.nivel_produto,
                                :new.grupo_produto,
                                :new.subgrupo_produto,
                                :new.item_produto,
                                v_alternativa_produto,
                                0,

                                :new.qtde_vendida,                       /* QTDE PROGRAMADA */
                                :new.qtde_perda,                         /* QTDE PERDA/ADICIONAIS */
                                :new.qtde_vendida,                       /* QTDE VENDIDA */
                                0 );


   end if;

   /*
      DEELETA UM PRODUTO DA ORDEM DE PLANEJAMENTO
   */
   if deleting
   then
      begin
         select count(1)
         into v_total_625
         from tmrp_625
         where tmrp_625.qtde_areceber_programada   = 0
           and tmrp_625.ordem_planejamento         = :old.ordem_planejamento
           and tmrp_625.pedido_venda               = :old.pedido_venda
           and tmrp_625.pedido_reserva             = :old.pedido_reserva
           and tmrp_625.liquida_saldo              = 0;
      end;

      if v_total_625 > 0
      then
         begin
            delete tmrp_625
            where tmrp_625.qtde_areceber_programada   = 0
              and tmrp_625.ordem_planejamento         = :old.ordem_planejamento
              and tmrp_625.pedido_venda               = :old.pedido_venda
              and tmrp_625.pedido_reserva             = :old.pedido_reserva
              and tmrp_625.liquida_saldo              = 0;
         exception when others then
            -- ATENCAO! Nao eliminou dados da tabela {0}. Status: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22626', 'TMRP_625(inter_tr_tmrp_620(1))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;

      begin
         select nvl(count(1),0)
         into v_total_625
         from tmrp_625
         where tmrp_625.ordem_planejamento         = :old.ordem_planejamento
           and tmrp_625.pedido_venda               = :old.pedido_venda
           and tmrp_625.pedido_reserva             = :old.pedido_reserva;
      end;

      if v_total_625 > 0
      then
         begin
            update tmrp_625
              set tmrp_625.qtde_reserva_planejada      = 0,
                  tmrp_625.unidades_reserva_planejada  = 0,
                  tmrp_625.qtde_vendida                = 0,
                  tmrp_625.qtde_reserva_programada     = 0,
                  tmrp_625.unidades_reserva_programada = 0
            where tmrp_625.ordem_planejamento          = :old.ordem_planejamento
              and tmrp_625.pedido_venda                = :old.pedido_venda
              and tmrp_625.pedido_reserva              = :old.pedido_reserva;
         exception when others then
            -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_625(inter_tr_tmrp_620(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;

end inter_tr_tmrp_620;

-- ALTER TRIGGER "INTER_TR_TMRP_620" ENABLE
 

/

exec inter_pr_recompile;

