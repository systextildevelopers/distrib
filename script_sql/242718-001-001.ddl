-- campos criados para processo se considera nf referenciada no xml e se vai cadastrar a referencia na supr_060 
alter table oper_275
add considera_nf_referenciada number(1) default 1; --(1) ativo (0) inativo help na tela oper_275

alter table oper_275
add referencia_do_fornecedor number(1) default 1; --(1) ativo (0) inativo help na tela oper_275

COMMENT ON COLUMN oper_275.considera_nf_referenciada IS ' (1) ativo (0) inativo help na tela oper_275';

COMMENT ON COLUMN oper_275.referencia_do_fornecedor IS ' (1) ativo (0) inativo help na tela oper_275';


exec inter_pr_recompile;
