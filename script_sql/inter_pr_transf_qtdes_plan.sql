
  CREATE OR REPLACE PROCEDURE "INTER_PR_TRANSF_QTDES_PLAN" (
p_ordem_planejamento in number,
p_pedido_venda in number,
p_ordem_planejamento_reserva in number,
p_pedido_reserva in number)
is

-- Finalidade: Transferir as quantidades programadas da reserva para a ordem de planejamento do pedido de venda
-- Autor.....: Junior
-- Empresa...: Toth Sistemas
-- Data......: 26/07/10
--
-- Historicos de alteracoes na procedure
--
-- Data    Autor    Empresa      Observacoes

   v_saldo number;
   v_qtde number;
   v_area number;
   v_ordem_producao number;
   v_nivel varchar2(1);
   v_grupo varchar2(5);
   v_subgrupo varchar2(3);
   v_item varchar2(6);
   v_alternativa number;

   v_consulta_origem boolean;

   v_nivel_consulta varchar2(1);
   v_grupo_consulta varchar2(5);
   v_subgrupo_consulta varchar2(3);
   v_item_consulta varchar2(6);
   v_alternativa_consulta number;
   v_seq_produto_consulta number;

   ws_nivel_origem varchar2(1);
   ws_grupo_origem varchar2(5);
   ws_subgrupo_origem varchar2(3);
   ws_item_origem varchar2(6);
   ws_alternativa_origem number;
   ws_seq_produto_origem number;

   v_reserva_comparacao number;

procedure atualiza_ordem
is
   v_qtde_rolos number;
   v_qtde_rolos_aux number;
begin
   if v_area = 2
   then
      begin
         select pcpb_020.qtde_rolos_prog
         into v_qtde_rolos
         from pcpb_020
         where pcpb_020.ordem_producao = v_ordem_producao
           and pcpb_020.pano_sbg_nivel99 = v_nivel
           and pcpb_020.pano_sbg_grupo = v_grupo
           and pcpb_020.pano_sbg_subgrupo = v_subgrupo
           and pcpb_020.pano_sbg_item = v_item
           and rownum = 1;
         exception when others
         then
            v_qtde_rolos := 0;
      end;

      begin
         select pcpb_030.qtde_rolos_prog
         into v_qtde_rolos_aux
         from pcpb_030
         where pcpb_030.ordem_producao = v_ordem_producao
           and pcpb_030.pano_nivel99 = v_nivel
           and pcpb_030.pano_grupo = v_grupo
           and pcpb_030.pano_subgrupo = v_subgrupo
           and pcpb_030.pano_item = v_item
           and pcpb_030.pedido_corte = 8
           and pcpb_030.nr_pedido_ordem = p_ordem_planejamento
           and rownum = 1;
         exception when others
         then
            v_qtde_rolos_aux := 0;
      end;

      begin
         update pcpb_030
         set pcpb_030.qtde_quilos_prog = v_saldo,
             pcpb_030.qtde_rolos_prog = v_qtde_rolos - v_qtde_rolos_aux
         where pcpb_030.ordem_producao = v_ordem_producao
           and pcpb_030.pano_nivel99 = v_nivel
           and pcpb_030.pano_grupo = v_grupo
           and pcpb_030.pano_subgrupo = v_subgrupo
           and pcpb_030.pano_item = v_item
           and pcpb_030.pedido_corte = 8
           and pcpb_030.nr_pedido_ordem = p_ordem_planejamento_reserva;
      end;
   elsif v_area = 4
   then
      begin
         update pcpt_016
         set pcpt_016.quantidade = v_saldo
         where pcpt_016.ordem_tecelagem = v_ordem_producao
           and pcpt_016.tipo_destino = 5
           and pcpt_016.ordem_pedido = p_ordem_planejamento_reserva;
      end;
   elsif v_area = 9
   then
      begin
         update supr_071
         set supr_071.qtde_item = v_saldo
         where supr_071.pedido_compra = v_ordem_producao
           and supr_071.nivel_item_ped = v_nivel
           and supr_071.grupo_item_ped = v_grupo
           and supr_071.subgrupo_item_ped = v_subgrupo
           and supr_071.item_item_ped = v_item
           and supr_071.tipo_relacionamento = 4
           and supr_071.num_relacionamento = p_ordem_planejamento_reserva;
      end;
   end if;
end;

procedure elimina_destino
is

begin
   if v_area = 2
   then
      begin
         delete pcpb_030
         where pcpb_030.ordem_producao = v_ordem_producao
           and pcpb_030.pano_nivel99 = v_nivel
           and pcpb_030.pano_grupo = v_grupo
           and pcpb_030.pano_subgrupo = v_subgrupo
           and pcpb_030.pano_item = v_item
           and pcpb_030.pedido_corte = 8
           and pcpb_030.nr_pedido_ordem = p_ordem_planejamento_reserva
           and rownum = 1;
      end;
   elsif v_area = 4
   then
      begin
         delete pcpt_016
         where pcpt_016.ordem_tecelagem = v_ordem_producao
           and pcpt_016.tipo_destino = 5
           and pcpt_016.ordem_pedido = p_ordem_planejamento_reserva;
      end;
   elsif v_area = 9
   then
      begin
         delete supr_071
         where supr_071.pedido_compra = v_ordem_producao
           and supr_071.nivel_item_ped = v_nivel
           and supr_071.grupo_item_ped = v_grupo
           and supr_071.subgrupo_item_ped = v_subgrupo
           and supr_071.item_item_ped = v_item
           and supr_071.tipo_relacionamento = 4
           and supr_071.num_relacionamento = p_ordem_planejamento_reserva;
      end;
   end if;
end;

procedure insere_novo_destino
is
   v_peso_rolo number;
   v_sequencia number;
   v_codigo_deposito number;
   v_periodo_producao number;
   v_roteiro number;
   v_codigo_transacao number;
   v_codigo_acomp number;
   v_lote_acomp number;

   v_cgc_9 number;
   v_cgc_4 number;
   v_cgc_2 number;
   v_seq_ordem_tec number;
   v_cor_confeccao varchar2(6);
   v_usuario varchar2(30);

   v_seq_item_pedido number;
begin
   if v_area = 2
   then
      begin
         select basi_020.peso_rolo
         into v_peso_rolo
         from basi_020
         where basi_020.basi030_nivel030 = v_nivel
           and basi_020.basi030_referenc = v_grupo
           and basi_020.tamanho_ref = v_subgrupo;
         exception when others
         then
            v_peso_rolo := 0;
      end;

      begin
         select pcpb_020.sequencia
         into v_sequencia
         from pcpb_020
         where pcpb_020.ordem_producao = v_ordem_producao
           and pcpb_020.pano_sbg_nivel99 = v_nivel
           and pcpb_020.pano_sbg_grupo = v_grupo
           and pcpb_020.pano_sbg_subgrupo = v_subgrupo
           and pcpb_020.pano_sbg_item = v_item
           and rownum = 1;
         exception when others
         then
            v_sequencia := 1;
      end;

      begin
         select pcpb_030.codigo_deposito, pcpb_030.periodo_producao,
                pcpb_030.roteiro,         pcpb_030.codigo_transacao,
                pcpb_030.codigo_acomp,    pcpb_030.lote_acomp
         into   v_codigo_deposito,        v_periodo_producao,
                v_roteiro,                v_codigo_transacao,
                v_codigo_acomp,           v_lote_acomp
         from pcpb_030
         where pcpb_030.ordem_producao = v_ordem_producao
           and pcpb_030.pano_nivel99 = v_nivel
           and pcpb_030.pano_grupo = v_grupo
           and pcpb_030.pano_subgrupo = v_subgrupo
           and pcpb_030.pano_item = v_item
           and pcpb_030.pedido_corte = 8
           and pcpb_030.nr_pedido_ordem = p_ordem_planejamento_reserva
           and rownum = 1;
         exception when others
         then
            v_codigo_deposito := 0;
            v_periodo_producao := 0;
            v_roteiro := 0;
            v_codigo_transacao := 0;
            v_codigo_acomp := 0;
            v_lote_acomp := 0;
      end;

      begin
         insert into pcpb_030
           (ordem_producao,     pano_nivel99,             pano_grupo,
            pano_subgrupo,      pano_item,                codigo_deposito,
            pedido_corte,       sequenci_periodo,         nr_pedido_ordem,
            periodo_producao,   qtde_rolos_prog,          qtde_quilos_prog,
            alternativa,        roteiro,                  codigo_transacao,
            codigo_acomp,       lote_acomp,               sequencia)
         values
           (v_ordem_producao,   v_nivel,                  v_grupo,
            v_subgrupo,         v_item,                   v_codigo_deposito,
            8,                  0,                        p_ordem_planejamento,
            v_periodo_producao, ceil(v_qtde/v_peso_rolo), v_qtde,
            v_alternativa,      v_roteiro,                v_codigo_transacao,
            v_codigo_acomp,     v_lote_acomp,             v_sequencia);
      end;
   elsif v_area = 4
   then
      begin
         select pcpt_016.cgc_9,         pcpt_016.cgc_4,         pcpt_016.cgc_2,
                pcpt_016.seq_ordem_tec, pcpt_016.cor_confeccao, pcpt_016.usuario
         into   v_cgc_9,                v_cgc_4,                v_cgc_2,
                v_seq_ordem_tec,        v_cor_confeccao,        v_usuario
         from pcpt_016
         where pcpt_016.ordem_tecelagem = v_ordem_producao
           and pcpt_016.tipo_destino = 5
           and pcpt_016.ordem_pedido = p_ordem_planejamento_reserva;
         exception when others
         then
            v_cgc_9 := 0;
            v_cgc_4 := 0;
            v_cgc_2 := 0;
            v_seq_ordem_tec := 0;
            v_cor_confeccao := ' ';
            v_usuario := ' ';
      end;

      begin
         insert into pcpt_016
           (ordem_tecelagem,   tipo_destino,      ordem_pedido,
            quantidade,        cgc_2,             cgc_4,
            cgc_9,             qtde_programada,   seq_ordem_tec,
            cor_confeccao,     usuario)
         values
           (v_ordem_producao,  5,                 p_ordem_planejamento,
            v_qtde,            v_cgc_2,           v_cgc_4,
            v_cgc_9,           0,                 v_seq_ordem_tec,
            v_cor_confeccao,   v_usuario);
      end;
   elsif v_area = 9
   then
      -- embora a sequencia do item seja a melhor distincao, o planejamento
      -- nao conhece esta informacao. Por isso, o proprio esta sendo utilizado.
      begin
         select supr_071.seq_item_pedido
         into v_seq_item_pedido
         from supr_071
         where supr_071.pedido_compra = v_ordem_producao
           and supr_071.nivel_item_ped = v_nivel
           and supr_071.grupo_item_ped = v_grupo
           and supr_071.subgrupo_item_ped = v_subgrupo
           and supr_071.item_item_ped = v_item
           and supr_071.tipo_relacionamento = 4
           and supr_071.num_relacionamento = p_ordem_planejamento_reserva;
         exception when others
         then
            v_seq_item_pedido := 0;
      end;

      begin
         insert into supr_071
           (pedido_compra,         seq_item_pedido,      nivel_item_ped,
            grupo_item_ped,        subgrupo_item_ped,    item_item_ped,
            tipo_relacionamento,   num_relacionamento,   qtde_item)
         values
           (v_ordem_producao,      v_seq_item_pedido,    v_nivel,
            v_grupo,               v_subgrupo,           v_item,
            4,                     p_ordem_planejamento, v_qtde);
      end;
   end if;
end;

begin
   <<loop_ordens>>
   for ordens in (select tmrp_630.ordem_prod_compra,   tmrp_630.area_producao,
                         tmrp_630.nivel_produto,       tmrp_630.grupo_produto,
                         tmrp_630.subgrupo_produto,    tmrp_630.item_produto,
                         tmrp_630.alternativa_produto, tmrp_630.programado_comprado qtde
                  from tmrp_630
                  where tmrp_630.ordem_planejamento = p_ordem_planejamento_reserva
                    and tmrp_630.pedido_reserva = p_pedido_reserva
                    and tmrp_630.nivel_produto <> '1')
   loop
      v_saldo := ordens.qtde;
      v_area := ordens.area_producao;
      v_ordem_producao := ordens.ordem_prod_compra;
      v_nivel := ordens.nivel_produto;
      v_grupo := ordens.grupo_produto;
      v_subgrupo := ordens.subgrupo_produto;
      v_item := ordens.item_produto;
      v_alternativa := ordens.alternativa_produto;

      <<loop_produtos>>
      for itens_transf in (select tmrp_625.seq_produto, tmrp_625.qtde_reserva_planejada qtde
                           from tmrp_625
                           where tmrp_625.nivel_produto = ordens.nivel_produto
                             and tmrp_625.grupo_produto = ordens.grupo_produto
                             and tmrp_625.subgrupo_produto = ordens.subgrupo_produto
                             and tmrp_625.item_produto = ordens.item_produto
                             and tmrp_625.alternativa_produto = ordens.alternativa_produto
                             and tmrp_625.ordem_planejamento = p_ordem_planejamento
                             and tmrp_625.pedido_venda = p_pedido_venda)
      loop
         v_consulta_origem := true;

         v_nivel_consulta       := ordens.nivel_produto;
         v_grupo_consulta       := ordens.grupo_produto;
         v_subgrupo_consulta    := ordens.subgrupo_produto;
         v_item_consulta        := ordens.item_produto;
         v_alternativa_consulta := ordens.alternativa_produto;
         v_seq_produto_consulta := itens_transf.seq_produto;

         while(v_consulta_origem)
         loop
            inter_pr_produto_origem (v_nivel_consulta,
                                     v_grupo_consulta,
                                     v_subgrupo_consulta,
                                     v_item_consulta,
                                     v_alternativa_consulta,
                                     v_seq_produto_consulta,
                                     p_ordem_planejamento,
                                     p_pedido_venda,
                                     ws_nivel_origem,
                                     ws_grupo_origem,
                                     ws_subgrupo_origem,
                                     ws_item_origem,
                                     ws_alternativa_origem,
                                     ws_seq_produto_origem);

            if ws_nivel_origem = 'A'
            or ws_nivel_origem = '1'
            then
               v_consulta_origem := false;
            else
               v_nivel_consulta       := ws_nivel_origem;
               v_grupo_consulta       := ws_grupo_origem;
               v_subgrupo_consulta    := ws_subgrupo_origem;
               v_item_consulta        := ws_item_origem;
               v_alternativa_consulta := ws_alternativa_origem;
               v_seq_produto_consulta := ws_seq_produto_origem;
            end if;
         end loop;

         if ws_nivel_origem = '1'
         then
            v_reserva_comparacao := 0;

            begin
               select pedi_110.numero_reserva
               into v_reserva_comparacao
               from pedi_110
               where pedi_110.pedido_venda      = p_pedido_venda
                 and pedi_110.cd_it_pe_nivel99  = ws_nivel_origem
                 and pedi_110.cd_it_pe_grupo    = ws_grupo_origem
                 and pedi_110.cd_it_pe_subgrupo = ws_subgrupo_origem
                 and pedi_110.cd_it_pe_item     = ws_item_origem
                 and rownum = 1;
               exception when others
               then
                  v_reserva_comparacao := 0;
            end;

            if v_reserva_comparacao = p_pedido_reserva
            then
               if v_saldo > itens_transf.qtde
               then
                  v_qtde := itens_transf.qtde;
                  v_saldo := v_saldo - itens_transf.qtde;

                  insere_novo_destino;
                  atualiza_ordem;
               else
                  v_qtde := v_saldo;
                  v_saldo := 0;

                  insere_novo_destino;

                  elimina_destino;

                  exit loop_produtos;
               end if;
            end if;
         end if;
      end loop;
   end loop;
end inter_pr_transf_qtdes_plan;
 

/

exec inter_pr_recompile;

