create table PEDI_194
(
  NIVEL        VARCHAR2(1) default '' not null,
  GRUPO        VARCHAR2(5) default '' not null,
  SUBGRUPO     VARCHAR2(3) default '' not null,
  ITEM         VARCHAR2(6) default '' not null,
  MARCA        NUMBER(3) default 0 not null,
  CATALOGO     NUMBER(3) default 0 not null,
  COLECAO      NUMBER(3) default 0 not null,
  CENTRO_CUSTO NUMBER(6),
  EMPRESA      NUMBER(3)
);

comment on table PEDI_194
  is 'Tabela de Relacionamento de centro de custo por empresa';

alter table PEDI_194
  add foreign key (CENTRO_CUSTO)
  references BASI_185 (CENTRO_CUSTO) on delete set null;
