alter table hdoc_030 
add (data_alteracao_senha date default sysdate,
     nr_tentativas_senha  number(2) default 0,
     data_hora            date);
   
comment on column hdoc_030.data_alteracao_senha is 'Data da alteração da senha para controle de expiração da mesma';
comment on column hdoc_030.nr_tentativas_senha is 'Número máximo de tentativas de senha errada';
comment on column hdoc_030.data_hora is 'data e hora em que foi digitado a senha errada';
/
