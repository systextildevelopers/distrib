CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_103_HIST"
  AFTER INSERT OR DELETE OR UPDATE OF COD_EMPRESA, CNPJ9, CNPJ4, CNPJ2, UF_CLI, COD_NATUREZA

ON PEDI_103
  FOR EACH ROW

DECLARE
  WS_USUARIO_REDE      VARCHAR2(20);
  WS_MAQUINA_REDE      VARCHAR2(40);
  WS_APLICATIVO        VARCHAR2(20);
  WS_SID               NUMBER(9);
  WS_EMPRESA           NUMBER(3);
  WS_USUARIO_SYSTEXTIL VARCHAR2(250);
  WS_LOCALE_USUARIO    VARCHAR2(5);

  WS_NOME_PROGRAMA HDOC_090.PROGRAMA%TYPE;
  LONG_AUX         VARCHAR2(4000);

BEGIN

  INTER_PR_DADOS_USUARIO(WS_USUARIO_REDE,
                         WS_MAQUINA_REDE,
                         WS_APLICATIVO,
                         WS_SID,
                         WS_USUARIO_SYSTEXTIL,
                         WS_EMPRESA,
                         WS_LOCALE_USUARIO);

  WS_NOME_PROGRAMA := INTER_FN_NOME_PROGRAMA(WS_SID);

  IF INSERTING THEN
    INSERT INTO HIST_100
      (PROGRAMA,
       TABELA,
       OPERACAO,
       DATA_OCORR,
       USUARIO_REDE,
       MAQUINA_REDE,
       APLICACAO,
       NUM01,
       LONG01
       
       )
    VALUES
      (WS_NOME_PROGRAMA,
       'PEDI_103',
       'I',
       SYSDATE,
       WS_USUARIO_REDE,
       WS_MAQUINA_REDE,
       WS_APLICATIVO,
       :NEW.COD_EMPRESA,
       INTER_FN_BUSCAR_TAG('lb07207#EMPRESA:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' ' || :NEW.COD_EMPRESA ||
       CHR(10) ||
       
       INTER_FN_BUSCAR_TAG('lb02839#CNPJ:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' ' ||
       TO_CHAR(:NEW.CNPJ9 || '-' || :NEW.CNPJ4 || '-' || :NEW.CNPJ2) ||
       CHR(15) ||
       
       INTER_FN_BUSCAR_TAG('lb01037#UF:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' ' ||
       TO_CHAR(:NEW.UF_CLI) || 
       CHR(10) ||
       
       INTER_FN_BUSCAR_TAG('lb29662#NATUREZA:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' ' ||
       TO_CHAR(:NEW.COD_NATUREZA) || 
       CHR(10)
       
       );
  END IF;

  IF UPDATING THEN
  
    IF :OLD.COD_EMPRESA <> :NEW.COD_EMPRESA THEN
      LONG_AUX := LONG_AUX ||
                  INTER_FN_BUSCAR_TAG('lb07207#EMPRESA:',
                                      WS_LOCALE_USUARIO,
                                      WS_USUARIO_SYSTEXTIL) || ' ' ||
                  :OLD.COD_EMPRESA || ' -> ' || :NEW.COD_EMPRESA || 
                  CHR(10);
    END IF;
    IF :OLD.CNPJ9 <> :NEW.CNPJ9 AND :OLD.CNPJ4 <> :NEW.CNPJ4 AND
       :OLD.CNPJ2 <> :NEW.CNPJ2 THEN
      LONG_AUX := LONG_AUX ||
                  INTER_FN_BUSCAR_TAG('lb02839#CNPJ:',
                                      WS_LOCALE_USUARIO,
                                      WS_USUARIO_SYSTEXTIL) || ' ' ||
                  TO_CHAR(:OLD.CNPJ9 || '-' || :OLD.CNPJ4 || '-' ||
                          :OLD.CNPJ2) || ' -> ' ||
                  TO_CHAR(:NEW.CNPJ9 || '-' || :NEW.CNPJ4 || '-' ||
                          :NEW.CNPJ2) || 
                          CHR(15);
    END IF;
    IF :OLD.UF_CLI <> :NEW.UF_CLI THEN
      LONG_AUX := LONG_AUX ||
                  INTER_FN_BUSCAR_TAG('lb01037#UF:',
                                      WS_LOCALE_USUARIO,
                                      WS_USUARIO_SYSTEXTIL) || ' ' ||
                  TO_CHAR(:OLD.UF_CLI) || ' -> ' || TO_CHAR(:NEW.UF_CLI) ||
                  CHR(10);
    END IF;
    IF :OLD.COD_NATUREZA <> :NEW.COD_NATUREZA THEN
      LONG_AUX := LONG_AUX ||
                  INTER_FN_BUSCAR_TAG('lb29662#NATUREZA:',
                                      WS_LOCALE_USUARIO,
                                      WS_USUARIO_SYSTEXTIL) || ' ' ||
                  TO_CHAR(:OLD.COD_NATUREZA) || ' -> ' ||
                  TO_CHAR(:NEW.COD_NATUREZA) || 
                  CHR(10);
    END IF;
  
    BEGIN
    
      INSERT INTO HIST_100
        (PROGRAMA,
         TABELA,
         OPERACAO,
         DATA_OCORR,
         USUARIO_REDE,
         MAQUINA_REDE,
         APLICACAO,
         NUM01,
         LONG01)
      VALUES
        (WS_NOME_PROGRAMA,
         'PEDI_103',
         'A',
         SYSDATE,
         WS_USUARIO_REDE,
         WS_MAQUINA_REDE,
         WS_APLICATIVO,
         :NEW.COD_EMPRESA,
         LONG_AUX);
    
    EXCEPTION
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20000,
                                INTER_FN_BUSCAR_TAG_COMPOSTA('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.',
                                                             'HIST_100(001-2)',
                                                             SQLERRM,
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             WS_LOCALE_USUARIO,
                                                             WS_USUARIO_SYSTEXTIL));
      
    END;
  END IF;

  IF DELETING THEN
    INSERT INTO HIST_100
      (PROGRAMA,
       TABELA,
       OPERACAO,
       DATA_OCORR,
       USUARIO_REDE,
       MAQUINA_REDE,
       APLICACAO,
       NUM01,
       LONG01
       
       )
    VALUES
      (WS_NOME_PROGRAMA,
       'PEDI_103',
       'D',
       SYSDATE,
       WS_USUARIO_REDE,
       WS_MAQUINA_REDE,
       WS_APLICATIVO,
       :OLD.COD_EMPRESA,
       INTER_FN_BUSCAR_TAG('lb07207#EMPRESA:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' ' || :OLD.COD_EMPRESA ||
       CHR(10) ||
       
       INTER_FN_BUSCAR_TAG('lb02839#CNPJ:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' ' ||
       TO_CHAR(:OLD.CNPJ9 || '-' || :OLD.CNPJ4 || '-' || :OLD.CNPJ2) ||
       CHR(15) ||
       
       INTER_FN_BUSCAR_TAG('lb01037#UF:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' ' ||
       TO_CHAR(:OLD.UF_CLI) || 
       CHR(10) ||
       
       INTER_FN_BUSCAR_TAG('lb29662#NATUREZA:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' ' ||
       TO_CHAR(:OLD.COD_NATUREZA) || 
       CHR(10)
       
       );
  END IF;
END INTER_TR_PEDI_103_HIST;
