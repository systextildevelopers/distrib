CREATE TABLE OPER_006 (
CODIGO_EMPRESA NUMBER(3),
USUARIO VARCHAR2(15),
NOME_FORM VARCHAR2(20),
QUANDO DATE,
JAVASCRIPT CLOB
);
COMMENT ON TABLE OPER_006 IS 'Fila de eventos para envio a formul�rios';
COMMENT ON COLUMN OPER_006.CODIGO_EMPRESA IS 'C�digo da empresa do usu�rio (nulo = todas)';
COMMENT ON COLUMN OPER_006.USUARIO IS 'Identifica��o do usu�rio (nulo = todos)';
COMMENT ON COLUMN OPER_006.NOME_FORM IS 'Nome do formul�rio autorizado a receber o evento (nulo = todos)';
COMMENT ON COLUMN OPER_006.QUANDO IS 'Quando enviar o evento (nulo = imediatamente)';
COMMENT ON COLUMN OPER_006.JAVASCRIPT IS 'Script a executar no formul�rio do usu�rio';
