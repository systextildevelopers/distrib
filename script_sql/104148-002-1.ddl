alter table obrf_740 
add (VL_SLD_CRED_ANT_FCP          NUMBER(13,2) default 0.0,
     VL_OUT_DEB_FCP               NUMBER(13,2) default 0.0,
     VL_OUT_CRED_FCP              NUMBER(13,2) default 0.0,
     VL_SLD_DEV_ANT_FCP           NUMBER(13,2) default 0.0,
     VL_DEDUCOES_FCP              NUMBER(13,2) default 0.0,
     VL_RECOL_FCP                 NUMBER(13,2) default 0.0,
     VL_SLD_CRED_TRANSPORTAR_FCP  NUMBER(13,2) default 0.0,
     DEB_ESP_FCP                  NUMBER(13,2) default 0.0);


comment on column obrf_740.VL_SLD_CRED_ANT_FCP is 'Valor do "Saldo credor de período anterior - FCP';
comment on column obrf_740.VL_OUT_DEB_FCP is 'Valor total dos ajustes - Outros débitos FCP e Estorno de créditos FCP';
comment on column obrf_740.VL_OUT_CRED_FCP is 'Valor total de Ajustes - Outros créditos FCP e Estorno de débitos FCP';
comment on column obrf_740.VL_SLD_DEV_ANT_FCP is 'Valor total de Saldo devedor FCP antes das deduções';
comment on column obrf_740.VL_DEDUCOES_FCP is 'Valor total das deduções - FCP';
comment on column obrf_740.VL_RECOL_FCP is 'Valor recolhido ou a recolher referente ao FCP (18-19)';
comment on column obrf_740.VL_SLD_CRED_TRANSPORTAR_FCP is 'Saldo credor a transportar para o período seguinte referente ao FCP';
comment on column obrf_740.DEB_ESP_FCP is 'Valores recolhidos ou a recolher, extraapuração - FCP';

exec inter_pr_recompile;
/
