create table fatu_890 (
cod_empresa number(3) default 0,
referencia_a varchar2(5) not null,
referencia_b varchar2(5) not null,
cor_ref_a varchar2(6) not null,
cor_ref_b varchar2(6) not null);

comment on table fatu_890 is 'Relacionamento de Referencias entre Conjuntos';
comment on column fatu_890.cod_empresa is 'Código da empresa';
comment on column fatu_890.referencia_a is 'Referencia A da peça a ser relacionada para formar o conjunto';
comment on column fatu_890.referencia_a is 'Referencia B da peça a ser relacionada para formar o conjunto';

exec inter_pr_recompile;
