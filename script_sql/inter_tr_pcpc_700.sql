create or replace trigger inter_tr_pcpc_700
before insert on pcpc_700 for each row
declare
    proximo_valor number;
BEGIN
    if :new.id is null then
        select ID_pcpc_700.nextval into proximo_valor from dual;
        :new.id := proximo_valor;
    end if;
END inter_tr_pcpc_700;
/
exec inter_pr_recompile;
