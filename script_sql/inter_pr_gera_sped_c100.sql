create or replace procedure inter_pr_gera_sped_c100 (p_cod_empresa   IN NUMBER,
                                                     p_dat_inicial   IN DATE,
                                                     p_dat_final     IN DATE,
                                                     p_cnpj9         IN NUMBER,
                                                     p_cnpj4         IN NUMBER,
                                                     p_cnpj2         IN NUMBER,
                                                     p_junta_empresa IN NUMBER,
                                                     p_des_erro      OUT varchar2) is
--
-- Finalidade: Gerar a tabela sped_c100 - Notas Fiscais
-- Autor.....: Cesar Anton
-- Data......: 20/11/08
--
-- Historicos
--
-- Data    Autor    Observac?es


CURSOR u_fatu_050 (p_cod_empresa     NUMBER,
                   p_dat_inicial     DATE,
                   p_dat_final       DATE,
                   p_cnpj9           NUMBER,
                   p_cnpj4           NUMBER,
                   p_cnpj2           NUMBER,
                   p_junta_empresa   NUMBER) IS
   SELECT fatu_050.*, trim(to_char(pedi_080.modelo_doc_fisc, '00')) modelo_doc_fisc,
          pedi_080.consumidor_final, pedi_080.cod_natureza,
          pedi_080.divisao_natur , fatu_050.tipo_frete wtipo_frete
   from   fatu_050,pedi_080
   where  fatu_050.natop_nf_nat_oper             = pedi_080.natur_operacao
     and  fatu_050.natop_nf_est_oper             = pedi_080.estado_natoper
     and  pedi_080.livros_fiscais                = 1 -- 1 - Entra nos livros fiscais
     and ((fatu_050.data_emissao                 between p_dat_inicial and p_dat_final)
          or fatu_050.data_averbacao_sped        between p_dat_inicial and p_dat_final
          or exists (select 1 from fatu_067
                     where fatu_067.codigo_empresa  = fatu_050.codigo_empresa
                       and fatu_067.num_nota_fiscal = fatu_050.num_nota_fiscal
                       and fatu_067.serie_nota_fisc = fatu_050.serie_nota_fisc
                       and fatu_067.data_averbacao_sped        between p_dat_inicial and p_dat_final))
   and    fatu_050.situacao_nfisc   in (1,2,3,4,6)
   and    ((fatu_050.codigo_empresa in (select fatu_500.codigo_empresa
                                        from   fatu_500
                                        where  fatu_500.cgc_9 = p_cnpj9
                                          and  fatu_500.cgc_4 = p_cnpj4
                                          and  fatu_500.cgc_2 = p_cnpj2
                                          and  p_junta_empresa = 1)
           or  (fatu_050.codigo_empresa = p_cod_empresa
                 and  p_junta_empresa = 0)
          )
         );

CURSOR u_obrf_010 (p_cod_empresa     NUMBER,
                   p_dat_inicial     DATE,
                   p_dat_final       DATE,
                   p_cnpj9           NUMBER,
                   p_cnpj4           NUMBER,
                   p_cnpj2           NUMBER,
                   p_junta_empresa   NUMBER) IS
   SELECT distinct obrf_010.*, trim(to_char(pedi_080.modelo_doc_fisc, '00')) modelo_doc_fisc,
          pedi_080.consumidor_final,pedi_080.cod_natureza,
          pedi_080.divisao_natur , obrf_010.tipo_frete wtipo_frete
   from   obrf_010,pedi_080
   where  obrf_010.natoper_nat_oper = pedi_080.natur_operacao
   and    obrf_010.natoper_est_oper = pedi_080.estado_natoper
   and    pedi_080.livros_fiscais   = 1 -- 1 - Entra nos livros fiscais
   and    obrf_010.data_transacao   between p_dat_inicial and p_dat_final
   and    obrf_010.situacao_entrada in (1,2,4)
   and    ((obrf_010.local_entrega  in (select fatu_500.codigo_empresa
                                        from   fatu_500
                                        where  fatu_500.cgc_9 = p_cnpj9
                                          and  fatu_500.cgc_4 = p_cnpj4
                                          and  fatu_500.cgc_2 = p_cnpj2
                                          and  p_junta_empresa = 1)
              or  (obrf_010.local_entrega = p_cod_empresa
                   and  p_junta_empresa = 0)
            ));


CURSOR u_fatu_060 (p_cod_empresa    NUMBER,
                   p_num_nota       number,
                   p_cod_serie_nota varchar2) IS
select fatu_060.nivel_estrutura
            ,fatu_060.grupo_estrutura
            ,decode(decode(fatu_060.nivel_estrutura,'1',niv_quebra_peca,
                    decode(fatu_060.nivel_estrutura,'2',niv_quebra_tecid,
                    decode(fatu_060.nivel_estrutura,'4',niv_quebra_pano,
                    decode(fatu_060.nivel_estrutura,'7',niv_quebra_fio,
                    decode(fatu_060.nivel_estrutura,'9',3,
                    decode(fatu_060.nivel_estrutura,'0',3)))))),1,null,fatu_060.subgru_estrutura)
                    as subgru_estrutura
             ,decode(decode(fatu_060.nivel_estrutura,'1',niv_quebra_peca,
                    decode(fatu_060.nivel_estrutura,'2',niv_quebra_tecid,
                    decode(fatu_060.nivel_estrutura,'4',niv_quebra_pano,
                    decode(fatu_060.nivel_estrutura,'7',niv_quebra_fio,
                    decode(fatu_060.nivel_estrutura,'9',3,
                    decode(fatu_060.nivel_estrutura,'0',3)))))), 1,null,
               decode(decode(fatu_060.nivel_estrutura,'1',niv_quebra_peca,
                    decode(fatu_060.nivel_estrutura,'2',niv_quebra_tecid,
                    decode(fatu_060.nivel_estrutura,'4',niv_quebra_pano,
                    decode(fatu_060.nivel_estrutura,'7',niv_quebra_fio,
                    decode(fatu_060.nivel_estrutura,'9',3,
                    decode(fatu_060.nivel_estrutura,'0',3)))))), 2,null,
                    fatu_060.item_estrutura)) as item_estrutura
                    ,trim(inter_fn_dados_produto_nf(0,6,p_cod_empresa,p_num_nota,p_cod_serie_nota,fatu_060.nivel_estrutura,fatu_060.grupo_estrutura,fatu_060.subgru_estrutura,fatu_060.item_estrutura,fatu_060.descricao_item))
                    as descricao_item
                    ,fatu_060.unidade_medida
                    ,fatu_060.centro_custo
                    ,fatu_060.cvf_icms
                    ,fatu_060.natopeno_nat_oper
                    ,fatu_060.natopeno_est_oper
                    ,fatu_060.perc_icms
                    ,fatu_060.perc_ipi
                    ,fatu_060.cvf_ipi_saida
                    ,fatu_060.perc_pis
                    ,fatu_060.cvf_pis
                    ,fatu_060.cvf_cofins
                    ,fatu_060.perc_cofins
                    ,fatu_060.perc_iss
                    ,fatu_060.data_emissao
                    ,REPLACE(fatu_060.classific_fiscal,'.') classific_fiscal

                    ,fatu_060.procedencia
                    ,fatu_060.lote_acomp
                    ,fatu_060.ch_it_nf_cd_empr
                    ,fatu_060.num_nota_orig
                    ,trim(fatu_060.serie_nota_orig) serie_nota_orig
                    ,fatu_060.cgc9_origem
                    ,fatu_060.cgc4_origem
                    ,fatu_060.cgc2_origem
                    ,fatu_060.deposito
                    ,fatu_060.transacao
                    ,fatu_060.codigo_contabil
                    ,fatu_060.cvf_ipi
                    ,sum(fatu_060.valor_faturado) valor_faturado
                    ,sum(fatu_060.valor_contabil) valor_contabil
                    ,sum(fatu_060.qtde_item_fatur) qtde_item_fatur
                    ,sum(fatu_060.rateio_descontos_ipi) desconto_item
                    ,sum(fatu_060.base_icms + fatu_060.rateio_despesa) base_icms
                    ,sum(fatu_060.valor_icms) valor_icms
                    ,sum(fatu_060.base_ipi) base_ipi
                    ,sum(fatu_060.valor_ipi) valor_ipi
                    ,sum(fatu_060.basi_pis_cofins) basi_pis_cofins
                    ,sum(fatu_060.valor_pis) valor_pis
                    ,sum(fatu_060.basi_pis_cofins) base_pis_cofins
                    ,sum(fatu_060.peso_liquido) peso_liquido
                    ,sum(fatu_060.valor_cofins) valor_cofins
                    ,sum(fatu_060.valor_iss) valor_iss
                    ,sum(fatu_060.rateio_despesa) rateio_despesa
                    ,sum(fatu_060.rateio_despesas_ipi) rateio_despesas_ipi
                    ,sum(decode(fatu_060.valor_icms_difer,0.00,0.00,fatu_060.base_icms_difer)) base_icms_difer
                    ,sum(fatu_060.valor_icms_difer) valor_icms_difer
                    ,sum(fatu_060.rateio_desc_propaganda) rateio_desc_propaganda
                    ,sum(fatu_060.val_fcp_uf_dest) val_fcp_uf_dest
                    ,sum(fatu_060.val_icms_uf_dest) val_icms_uf_dest
                    ,sum(fatu_060.val_icms_uf_remet) val_icms_uf_remet
                    ,fatu_050.tipo_valores_fiscal
                    ,nvl(fatu_060.cod_estagio_agrupador_insu, 0) as estagio_agrupador
                    ,nvl(fatu_060.cod_estagio_simultaneo_insu, 0) as estagio_agrupador_simultaneo
                    ,nvl(fatu_060.seq_operacao_agrupador_insu, 0) as seq_operacao_agrupador_insu

   from   fatu_060,
          pedi_080,
          fatu_500,
          fatu_050
   where  ch_it_nf_cd_empr         = p_cod_empresa
     and  ch_it_nf_num_nfis        = p_num_nota
     and  ch_it_nf_ser_nfis        = p_cod_serie_nota
     and  fatu_500.codigo_empresa  = p_cod_empresa
     and pedi_080.natur_operacao   = fatu_060.natopeno_nat_oper
     and pedi_080.estado_natoper   = fatu_060.natopeno_est_oper
     and fatu_050.codigo_empresa   = fatu_060.ch_it_nf_cd_empr
     and fatu_050.num_nota_fiscal  = fatu_060.ch_it_nf_num_nfis
     and fatu_050.serie_nota_fisc  = fatu_060.ch_it_nf_ser_nfis
group by fatu_060.nivel_estrutura
            ,fatu_060.grupo_estrutura
            ,decode(decode(fatu_060.nivel_estrutura,'1',niv_quebra_peca,
                    decode(fatu_060.nivel_estrutura,'2',niv_quebra_tecid,
                    decode(fatu_060.nivel_estrutura,'4',niv_quebra_pano,
                    decode(fatu_060.nivel_estrutura,'7',niv_quebra_fio,
                    decode(fatu_060.nivel_estrutura,'9',3,
                    decode(fatu_060.nivel_estrutura,'0',3)))))),1,null,fatu_060.subgru_estrutura)
             ,decode(decode(fatu_060.nivel_estrutura,'1',niv_quebra_peca,
                    decode(fatu_060.nivel_estrutura,'2',niv_quebra_tecid,
                    decode(fatu_060.nivel_estrutura,'4',niv_quebra_pano,
                    decode(fatu_060.nivel_estrutura,'7',niv_quebra_fio,
                    decode(fatu_060.nivel_estrutura,'9',3,
                    decode(fatu_060.nivel_estrutura,'0',3)))))), 1,null,
               decode(decode(fatu_060.nivel_estrutura,'1',niv_quebra_peca,
                    decode(fatu_060.nivel_estrutura,'2',niv_quebra_tecid,
                    decode(fatu_060.nivel_estrutura,'4',niv_quebra_pano,
                    decode(fatu_060.nivel_estrutura,'7',niv_quebra_fio,
                    decode(fatu_060.nivel_estrutura,'9',3,
                    decode(fatu_060.nivel_estrutura,'0',3)))))), 2,null,
                    fatu_060.item_estrutura))
                    ,trim(inter_fn_dados_produto_nf(0,6,p_cod_empresa,p_num_nota,p_cod_serie_nota,fatu_060.nivel_estrutura,fatu_060.grupo_estrutura,fatu_060.subgru_estrutura,fatu_060.item_estrutura,fatu_060.descricao_item))
                    ,fatu_060.unidade_medida
                    ,fatu_060.centro_custo
                    ,fatu_060.cvf_icms
                    ,fatu_060.natopeno_nat_oper
                    ,fatu_060.natopeno_est_oper
                    ,fatu_060.perc_icms
                    ,fatu_060.perc_ipi
                    ,fatu_060.cvf_ipi_saida
                    ,fatu_060.perc_pis
                    ,fatu_060.cvf_pis
                    ,fatu_060.cvf_cofins
                    ,fatu_060.perc_cofins
                    ,fatu_060.perc_iss
                    ,fatu_060.data_emissao
                    ,REPLACE(fatu_060.classific_fiscal,'.')

                    ,fatu_060.procedencia
                    ,fatu_060.lote_acomp
                    ,fatu_060.ch_it_nf_cd_empr
                    ,fatu_060.num_nota_orig
                    ,trim(fatu_060.serie_nota_orig)
                    ,fatu_060.cgc9_origem
                    ,fatu_060.cgc4_origem
                    ,fatu_060.cgc2_origem
                    ,fatu_060.deposito
                    ,fatu_060.transacao
                    ,fatu_060.codigo_contabil
                    ,fatu_060.cvf_ipi
                    ,pedi_080.cod_natureza
                    ,pedi_080.divisao_natur
                    ,fatu_050.tipo_valores_fiscal
                    ,fatu_060.cod_estagio_agrupador_insu
                    ,fatu_060.cod_estagio_simultaneo_insu
                    ,fatu_060.seq_operacao_agrupador_insu;


CURSOR u_obrf_015 (p_cod_empresa    NUMBER,
                   p_num_nota       number,
                   p_cod_serie_nota VARCHAR2,
                   p_num_cnpj_9     NUMBER,
                   p_num_cnpj_4     NUMBER,
                   p_num_cnpj_2     NUMBER) IS

select * from (
   select
                    obrf_015.coditem_nivel99
                    ,obrf_015.coditem_grupo
            ,decode(decode(obrf_015.coditem_nivel99,'1',niv_quebra_peca,
                    decode(obrf_015.coditem_nivel99,'2',niv_quebra_tecid,
                    decode(obrf_015.coditem_nivel99,'4',niv_quebra_pano,
                    decode(obrf_015.coditem_nivel99,'7',niv_quebra_fio,
                    decode(obrf_015.coditem_nivel99,'9',3,
                    decode(obrf_015.coditem_nivel99,'0',3)))))),1,null,obrf_015.coditem_subgrupo)
                    as coditem_subgrupo
            ,decode(decode(obrf_015.coditem_nivel99,'1',niv_quebra_peca,
                    decode(obrf_015.coditem_nivel99,'2',niv_quebra_tecid,
                    decode(obrf_015.coditem_nivel99,'4',niv_quebra_pano,
                    decode(obrf_015.coditem_nivel99,'7',niv_quebra_fio,
                    decode(obrf_015.coditem_nivel99,'9',3,
                    decode(obrf_015.coditem_nivel99,'0',3)))))),1,null,
   decode(decode(obrf_015.coditem_nivel99,'1',niv_quebra_peca,
                    decode(obrf_015.coditem_nivel99,'2',niv_quebra_tecid,
                    decode(obrf_015.coditem_nivel99,'4',niv_quebra_pano,
                    decode(obrf_015.coditem_nivel99,'7',niv_quebra_fio,
                    decode(obrf_015.coditem_nivel99,'9',3,
                    decode(obrf_015.coditem_nivel99,'0',3)))))),2,null,obrf_015.coditem_item))
                    as coditem_item
                    ,trim(inter_fn_dados_produto_nf(0,6,p_cod_empresa,p_num_nota,p_cod_serie_nota,obrf_015.coditem_nivel99,obrf_015.coditem_grupo,obrf_015.coditem_subgrupo,obrf_015.coditem_item,obrf_015.descricao_item)) descricao_item
                    ,obrf_015.unidade_medida
                    ,obrf_015.centro_custo
                    ,obrf_015.natitem_nat_oper
                    ,obrf_015.natitem_est_oper
                    ,obrf_015.percentual_ipi
                    ,obrf_015.cvf_ipi_entrada
                    ,obrf_015.perc_pis
                    ,obrf_015.cvf_pis
                    ,obrf_015.cvf_cofins
                    ,obrf_015.perc_cofins
                    ,obrf_015.classific_fiscal
                    ,obrf_015.procedencia
                    ,obrf_015.cod_vlfiscal_icm
                    ,obrf_015.percentual_icm
                    ,obrf_015.lote_entrega
                    ,obrf_015.num_nota_orig
                    ,trim(obrf_015.serie_nota_orig) serie_nota_orig
                    ,obrf_015.capa_ent_forcli9
                    ,obrf_015.capa_ent_forcli4
                    ,obrf_015.capa_ent_forcli2
                    ,obrf_015.codigo_deposito
                    ,obrf_015.perc_dif_aliq
                    ,obrf_015.codigo_transacao
                    ,obrf_015.codigo_contabil
                    ,obrf_015.cod_vlfiscal_ipi
                    ,obrf_015.perc_subtituicao
                    ,sum(obrf_015.valor_total) valor_total
                    ,sum(obrf_015.quantidade) quantidade
                    ,sum(obrf_015.base_calc_icm) base_calc_icm
                    ,sum(obrf_015.valor_icms) valor_icms
                    ,sum(obrf_015.base_diferenca) base_diferenca
                    ,sum(obrf_015.base_ipi) base_ipi
                    ,sum(obrf_015.valor_ipi) valor_ipi
                    ,sum(obrf_015.base_pis_cofins) base_pis_cofins
                    ,sum(obrf_015.valor_pis) valor_pis
                    ,sum(obrf_015.valor_cofins) valor_cofins
                    ,sum(obrf_015.valor_icms_diferido) valor_icms_diferido
                    ,sum(obrf_015.rateio_despesas) rateio_despesas
                    ,sum(obrf_015.rateio_despesas_ipi) rateio_despesas_ipi
                    ,sum(decode(obrf_015.valor_subtituicao,0.00,0.00,obrf_015.base_subtituicao)) base_subtituicao
                    ,sum(decode(obrf_015.cod_vlfiscal_icm,60,0.00,obrf_015.valor_subtituicao) ) valor_subtituicao
                    ,sum(obrf_015.val_fcp_uf_dest) val_fcp_uf_dest
                    ,sum(obrf_015.val_icms_uf_dest) val_icms_uf_dest
                    ,sum(obrf_015.val_icms_uf_remet) val_icms_uf_remet
                    ,obrf_010.tipo_valores_fiscal
                    ,sum(obrf_015.desconto_item) desconto_item
                    ,sum(obrf_015.rateio_descontos_ipi) rateio_descontos_ipi
                    ,max(obrf_015.sequencia) sequencia_nf_ent
          ,sum(obrf_015.quantidade_conv) quantidade_conv
          ,obrf_015.unidade_conv
                    ,nvl(obrf_015.cod_estagio_agrupador_insu, 0) as estagio_agrupador
                    ,nvl(obrf_015.cod_estagio_simultaneo_insu, 0) as estagio_agrupador_simultaneo
                    ,nvl(obrf_015.seq_operacao_agrupador_insu, 0) as  seq_operacao_agrupador_insu

   from   obrf_015,
          pedi_080,
          fatu_500,
          obrf_010
   where obrf_015.capa_ent_nrdoc   = p_num_nota
     and obrf_015.capa_ent_serie   = p_cod_serie_nota
     and obrf_015.capa_ent_forcli9 = p_num_cnpj_9
     and obrf_015.capa_ent_forcli4 = p_num_cnpj_4
     and obrf_015.capa_ent_forcli2 = p_num_cnpj_2
     and pedi_080.natur_operacao   = obrf_015.natitem_nat_oper
     and pedi_080.estado_natoper   = obrf_015.natitem_est_oper
     and fatu_500.codigo_empresa   = p_cod_empresa
     and obrf_010.documento        = obrf_015.capa_ent_nrdoc
     and obrf_010.serie            = obrf_015.capa_ent_serie
     and obrf_010.cgc_cli_for_9    = obrf_015.capa_ent_forcli9
     and obrf_010.cgc_cli_for_4    = obrf_015.capa_ent_forcli4
     and obrf_010.cgc_cli_for_2    = obrf_015.capa_ent_forcli2
group by             obrf_015.coditem_nivel99
                    ,obrf_015.coditem_grupo
            ,decode(decode(obrf_015.coditem_nivel99,'1',niv_quebra_peca,
                    decode(obrf_015.coditem_nivel99,'2',niv_quebra_tecid,
                    decode(obrf_015.coditem_nivel99,'4',niv_quebra_pano,
                    decode(obrf_015.coditem_nivel99,'7',niv_quebra_fio,
                    decode(obrf_015.coditem_nivel99,'9',3,
                    decode(obrf_015.coditem_nivel99,'0',3)))))),1,null,obrf_015.coditem_subgrupo)
            ,decode(decode(obrf_015.coditem_nivel99,'1',niv_quebra_peca,
                    decode(obrf_015.coditem_nivel99,'2',niv_quebra_tecid,
                    decode(obrf_015.coditem_nivel99,'4',niv_quebra_pano,
                    decode(obrf_015.coditem_nivel99,'7',niv_quebra_fio,
                    decode(obrf_015.coditem_nivel99,'9',3,
                    decode(obrf_015.coditem_nivel99,'0',3)))))),1,null,
   decode(decode(obrf_015.coditem_nivel99,'1',niv_quebra_peca,
                    decode(obrf_015.coditem_nivel99,'2',niv_quebra_tecid,
                    decode(obrf_015.coditem_nivel99,'4',niv_quebra_pano,
                    decode(obrf_015.coditem_nivel99,'7',niv_quebra_fio,
                    decode(obrf_015.coditem_nivel99,'9',3,
                    decode(obrf_015.coditem_nivel99,'0',3)))))),2,null,obrf_015.coditem_item))
                    ,trim(inter_fn_dados_produto_nf(0,6,p_cod_empresa,p_num_nota,p_cod_serie_nota,obrf_015.coditem_nivel99,obrf_015.coditem_grupo,obrf_015.coditem_subgrupo,obrf_015.coditem_item,obrf_015.descricao_item))
                    ,obrf_015.unidade_medida
                    ,obrf_015.centro_custo
                    ,obrf_015.cod_vlfiscal_icm
                    ,obrf_015.natitem_nat_oper
                    ,obrf_015.natitem_est_oper
                    ,obrf_015.percentual_ipi
                    ,obrf_015.cvf_ipi_entrada
                    ,obrf_015.perc_pis
                    ,obrf_015.cvf_pis
                    ,obrf_015.cvf_cofins
                    ,obrf_015.perc_cofins
                    ,obrf_015.classific_fiscal
                    ,obrf_015.procedencia
                    ,obrf_015.lote_entrega
                    ,obrf_015.num_nota_orig
                    ,obrf_015.serie_nota_orig
                    ,obrf_015.capa_ent_forcli9
                    ,obrf_015.capa_ent_forcli4
                    ,obrf_015.capa_ent_forcli2
                    ,obrf_015.codigo_deposito
                    ,obrf_015.perc_dif_aliq
                    ,obrf_015.codigo_transacao
                    ,obrf_015.codigo_contabil
                    ,obrf_015.percentual_icm
                    ,obrf_015.cod_vlfiscal_ipi
                    ,pedi_080.cod_natureza
                    ,pedi_080.divisao_natur
                    ,obrf_015.perc_subtituicao
                    ,obrf_010.tipo_valores_fiscal
               ,obrf_015.unidade_conv
                    ,obrf_015.cod_estagio_agrupador_insu
                    ,obrf_015.cod_estagio_simultaneo_insu
               ,obrf_015.seq_operacao_agrupador_insu
                    )
    order by sequencia_nf_ent ;

w_erro                  EXCEPTION;


w_qtde_parcela          number(3);
w_intervalo_parcela     number(3);
w_aprazo_avista         number(1);
w_modelo_nota           varchar2(2);
w_data_vencto_duplic    fatu_070.data_prorrogacao%type;
w_tip_nota_cancel       varchar2(1);
w_num_cfop              pedi_080.cod_natureza%type;
w_divisao_natureza      pedi_080.divisao_natur%type;
w_consiste_cvf_icms     pedi_080.consiste_cvf_icms%type;
w_tipo_transacao        estq_005.atualiza_estoque%type;
w_val_total_pis         sped_c100.val_pis%type;
w_val_total_cofins      sped_c100.val_cofins%type;
w_cod_mensagem          pedi_080.cod_mensagem%type;
w_tipo_titulo           fatu_505.tipo_titulo%type;
w_val_base_ipi          sped_c100.val_base_ipi%type;
w_base_dif              sped_c100.val_base_ipi%type;
w_insc_est_cliente      pedi_010.insc_est_cliente%type;
w_codigo_cidade_ibge    number;
w_codigo_fiscal_uf      basi_167.codigo_fiscal_uf%type;
w_nf_expecial           obrf_190.tip_nf_espedical%type;
w_situacao_entrada      obrf_010.situacao_entrada%type;
w_val_base_icms         fatu_060.base_icms%type;
w_val_icms              fatu_060.valor_icms%type;
w_cod_enquadramento_ipi basi_240.cod_enquadramento_ipi%type;
w_tp_forne_elet         empr_001.fornec_ener_elet%type;
w_tp_forne_tele         empr_001.fornec_telecomu%type;
w_tp_forne_trans        empr_001.fornec_transport%type;
w_tp_forne_agua         empr_001.fornec_agua%type;
w_tp_forne_gas          empr_001.fornec_gas%type;
w_tipo_fornecedor       supr_010.tipo_fornecedor%type;
w_placa_veiculo         fatu_050.placa_veiculo%type;
w_num_seq_modelo        sped_c100.num_seq_modelo%type;
w_emite_duplicata       pedi_080.emite_duplicata%type;
w_sequencia             number;
w_conta_msg             number;
w_tem_referenciado      varchar2(1);
w_cod_especie           obrf_200.codigo_especie%type;
w_serie_nfe             fatu_505.serie_nota_fisc%type;
w_modelo_especie        obrf_200.modelo_documento%type;
w_val_ipi               obrf_015.valor_ipi%type;
w_val_desconto          sped_c170.val_desconto%type;
w_val_desconto_f        sped_c170.val_desconto%type;
ws_grupo_clas           fatu_060.grupo_estrutura%type;
ws_subgru_clas          fatu_060.subgru_estrutura%type;
w_valor_reducao_icms    obrf_015.base_diferenca%type;
w_perc_substituica      pedi_080.perc_substituica%type;
w_val_base_icms_sub_trib obrf_015.base_subtituicao%type;
w_val_icms_sub_trib      obrf_015.valor_subtituicao%type;
w_valor_contabil         obrf_015.valor_total%type;
w_valor_total_nf        fatu_050.valor_itens_nfis%type;
w_ipi_sobre_icms        pedi_080.ipi_sobre_icms%type;
w_val_ipi_sobre_icms    fatu_060.valor_ipi%type;
w_tipo_frete         fatu_050.tipo_frete%type;
w_itens_entram_livro     number;
w_tp_nf                  fatu_505.tipo_nf%type;

w_flag_exp               sped_c100.flag_exp%type;
w_tem_registro           number(3);
w_data_icms_sped         date;
w_conta_reg_export       number;

w_val_fcp_uf_dest        fatu_060.val_fcp_uf_dest%type;
w_val_icms_uf_dest       fatu_060.val_icms_uf_dest%type;
w_val_icms_uf_remet      fatu_060.val_icms_uf_remet%type;
w_numero_di              obrf_056.numero_di%type;
w_ind_nf_consumo         sped_c100.ind_nf_consumo%type;

w_cidade_origem          number(7);
w_cidade_destino         number(7);
w_cidadde_fiscal_empresa number(7);
w_cidade_origemNF        number(7);
w_cidade_destinoNF       number(7);

v_seq_end_entr           number(3);
v_considera_seq_end_entr number(1);
v_conta_item        number(1);
v_parametro_endereco     number(1);
v_eh_devolucao       estq_005.tipo_transacao%type;
w_val_faturado           number;
v_val_faturado           number;

v_ind_soma_ipi           varchar(2);

BEGIN
   p_des_erro          := NULL;
   v_conta_item       := 0;
   v_seq_end_entr     :=0;

   begin
      select lpad(basi_167.codigo_fiscal_uf,2,0) || lpad(basi_160.codigo_fiscal,5,0)
      into   w_cidadde_fiscal_empresa
      from   basi_160,  basi_167, fatu_500
          where fatu_500.codigo_empresa = p_cod_empresa
            and basi_160.cod_cidade     = fatu_500.codigo_cidade
            and basi_160.estado         = basi_167.estado
            and basi_160.codigo_pais    = basi_167.codigo_pais;
   exception
       when others then
          w_cidadde_fiscal_empresa := '0000000';
   end;

   begin
    select  NVL(empr_008.val_int,0)
    into    v_parametro_endereco
    from    empr_008
    where   empr_008.codigo_empresa   =   p_cod_empresa
    and     empr_008.param         =   'vendas.enderecoDanfe'; -- CONSIDERA PARyMETRO
    exception
      when others then
        v_parametro_endereco := 0;
   end;

   --
   -- Notas de Saida
   --
   FOR fatu_050 IN u_fatu_050 (p_cod_empresa, p_dat_inicial, p_dat_final,
                               p_cnpj9, p_cnpj4, p_cnpj2, p_junta_empresa)
   LOOP

      --Feito assim por causa da nova implantac?o de zerar valores da NF nos arquivos fiscais

      w_valor_total_nf := fatu_050.valor_itens_nfis + fatu_050.valor_ipi      + fatu_050.valor_encar_nota +
                          fatu_050.valor_frete_nfis + fatu_050.valor_seguro   - fatu_050.valor_desc_nota -
                          fatu_050.valor_suframa    + fatu_050.valor_despesas - fatu_050.vlr_desc_especial +
                          fatu_050.valor_icms_sub;

    w_tipo_frete := fatu_050.wtipo_frete;

      if fatu_050.tipo_valores_fiscal <> '0'
      then

         w_valor_total_nf            := 0.00;
         fatu_050.valor_desc_nota    := 0.00;
         fatu_050.valor_itens_nfis   := 0.00;
         fatu_050.valor_despesas     := 0.00;
         fatu_050.base_icms          := 0.00;
         fatu_050.valor_icms         := 0.00;
         fatu_050.base_icms_sub      := 0.00;
         fatu_050.valor_icms_sub     := 0.00;

         if fatu_050.tipo_valores_fiscal = '1'
         then

            fatu_050.valor_ipi       := 0.00;

         end if;

      end if;

      -- verifica se placa do veiculo digitada na nota fiscal tem sete digitos ou mais,
      -- se n?o tiver, coloca o campo como '', pois sera considerado como placa invalida
      w_sequencia := 0;
      w_tem_referenciado := 'n';

      if length(fatu_050.placa_veiculo) < 7
      then
         w_placa_veiculo := '';
      else
         w_placa_veiculo := replace(replace(fatu_050.placa_veiculo,'-'),' ');
      end if;

      --Se a data inicial da gerac?o do Sped for maior que a emiss?o da nota, marca a flag_exp como "S",
      --para que so efetue a leitura do sped_c100 no registro 1100 no programa obrf_f135
      if p_dat_inicial > fatu_050.data_emissao
      then
         w_flag_exp := 'S';
      else
         w_flag_exp := 'N';
      end if;

      -- le o tipo de titulo da serie de nota fiscal para poder encontrar os titulos relacionados a nota
      begin
         select fatu_505.tipo_titulo,  fatu_505.tipo_nf
         into   w_tipo_titulo,         w_tp_nf
         from fatu_505
         where fatu_505.codigo_empresa = fatu_050.codigo_empresa
           and trim(fatu_505.serie_nota_fisc) = trim(fatu_050.serie_nota_fisc);

       exception
          when no_data_found then
             w_tipo_titulo  := null;
             w_tp_nf        := null;
       end;

       -- verifica o numero de parcelas geradas para a nota fiscal para identificar se a nota e a vista ou a prazo
       begin
          select count(*) into w_qtde_parcela from fatu_070
          where fatu_070.codigo_empresa   = fatu_050.codigo_empresa
            and fatu_070.cli_dup_cgc_cli9 = fatu_050.cgc_9
            and fatu_070.cli_dup_cgc_cli4 = fatu_050.cgc_4
            and fatu_070.cli_dup_cgc_cli2 = fatu_050.cgc_2
            and fatu_070.num_duplicata    = fatu_050.num_nota_fiscal
            and fatu_070.tipo_titulo      = w_tipo_titulo;
       exception
         when no_data_found then
            w_qtde_parcela  := 0;
       end;


       -- se encontrar titulos verifica o intervalo de datas para verificar se a nota fiscal e a prazo ou a vista
       w_intervalo_parcela  := 0;

       w_intervalo_parcela := 100;
       
       if w_qtde_parcela > 0
       then
          begin
             select pedi_075.vencimento into w_intervalo_parcela from pedi_075
             where pedi_075.condicao_pagto = fatu_050.cond_pgto_venda
               and rownum = 1;

          exception
             when no_data_found then
                w_intervalo_parcela  := 0;
          end;
       end if;

       if w_qtde_parcela = 0
       then
          w_aprazo_avista := 9; -- sem pagamento
       else

          if w_intervalo_parcela = 0 and w_qtde_parcela = 1
          then
              w_aprazo_avista := 1; -- avista
          else
             w_aprazo_avista := 2; -- aprazo
          end if;
       end if;

       -- se for a prazo, le a primeira parcela para ver a data de vencimento
       if w_aprazo_avista = 2
       then

          begin
             select fatu_070.data_prorrogacao into w_data_vencto_duplic from fatu_070
             where fatu_070.codigo_empresa   = fatu_050.codigo_empresa
               and fatu_070.num_duplicata    = fatu_050.num_nota_fiscal
               and fatu_070.seq_duplicatas   = 1    -- so primeira parcela
               and fatu_070.tipo_titulo      = w_tipo_titulo
               and fatu_070.cli_dup_cgc_cli9 = fatu_050.cgc_9
               and fatu_070.cli_dup_cgc_cli4 = fatu_050.cgc_4
               and fatu_070.cli_dup_cgc_cli2 = fatu_050.cgc_2;

          exception
             when no_data_found then
                w_data_vencto_duplic  := null;
          end;
       else

          if w_aprazo_avista = 1
          then
             w_data_vencto_duplic := fatu_050.data_saida;
          else
             w_data_vencto_duplic := NULL;
          end if;
       end if;

       w_modelo_especie := null;

       -- se o modelo do documento fiscal for nulo ou zero, seta o modelo da nota para 01 (default)
       begin
          select trim(fatu_505.codigo_especie), trim(fatu_505.serie_nfe)
            into w_cod_especie,           w_serie_nfe
          from fatu_505
          where fatu_505.codigo_empresa = fatu_050.codigo_empresa
            and trim(fatu_505.serie_nota_fisc) = trim(fatu_050.serie_nota_fisc);
       EXCEPTION
          WHEN OTHERS THEN
             w_cod_especie := null;
             w_serie_nfe   := null;
       END;

       if w_serie_nfe = 'S' and trim(fatu_050.numero_danf_nfe) is not null and w_cod_especie <> 'NFC-e' -- em caso de notas antigas que utilizam a mesma serie da NF-e
       then
          fatu_050.modelo_doc_fisc := '55';
       else
          if w_cod_especie is not null
          then
             begin
                select trim(obrf_200.modelo_documento)
                  into w_modelo_especie
                from obrf_200
                where obrf_200.codigo_especie  = w_cod_especie;
             EXCEPTION
                WHEN OTHERS THEN
                   w_modelo_especie := null;
             END;
          end if;
       end if;

       if w_modelo_especie is not null
       then
          if Nvl(Length(w_modelo_especie),0) > 2
          then

                   inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                'Procedure: p_gera_sped_C100 ' || Chr(10) ||
                                'Tamanho do campo modelo nota no cadastro de especie e maior que o permitido ' || Chr(10) ||
                                '   Serie: ' || fatu_050.serie_nota_fisc   || Chr(10) ||
                                '   Modelo ' || w_modelo_especie);
                   w_modelo_especie := substr(w_modelo_especie,1,2);
          end if;

          fatu_050.modelo_doc_fisc := w_modelo_especie;
       end if;

       if fatu_050.modelo_doc_fisc is null or fatu_050.modelo_doc_fisc = '00'
       then
          w_modelo_nota := '01';
       else
          w_modelo_nota := fatu_050.modelo_doc_fisc;
       end if;

       -- identifica se a nota fiscal esta cancelada ou n?o
       if fatu_050.situacao_nfisc = 2
       then
          w_tip_nota_cancel := 'S';
       else
          w_tip_nota_cancel := 'N';
       end if;

       if fatu_050.situacao_nfisc <> 2
       then
          /* Se a nota for diferente de 1 (Normal) e n?o estiver cancelada,
             classifica ela como nota complementar */
          if fatu_050.tipo_nf_referenciada = 1
          then
             fatu_050.situacao_nfisc := 0;
          else
             fatu_050.situacao_nfisc := 6;
          end if;
       end if;

       if fatu_050.situacao_nfisc = 2 and fatu_050.cod_justificativa = 1
       then
          fatu_050.situacao_nfisc := 5;
          else if fatu_050.situacao_nfisc = 2 and fatu_050.cod_status in ('205','110','301','302') /*Nota denegada*/
          then
             fatu_050.situacao_nfisc := 4;
          end if;
       end if;

       -- le a tabela de processos referenciados,
       -- se houver dados e o processo for especial (tip_nf_espedical = 1) seta a situacao da NF para 8
       BEGIN
          select obrf_190.tip_nf_espedical into w_nf_expecial
          from obrf_190
          where obrf_190.cod_empresa       = fatu_050.codigo_empresa
            and obrf_190.num_nota          = fatu_050.num_nota_fiscal
            and trim(obrf_190.cod_serie_nota)    = trim(fatu_050.serie_nota_fisc)
            and obrf_190.num_cnpj_9        = fatu_050.cgc_9
            and obrf_190.num_cnpj_4        = fatu_050.cgc_4
            and obrf_190.num_cnpj_2        = fatu_050.cgc_2
            and trim(obrf_190.num_processo) is not null;

       EXCEPTION
          WHEN OTHERS THEN
             w_nf_expecial := 0;
       END;

       if w_nf_expecial = 1
       then
          fatu_050.situacao_nfisc := 8;
       end if;

       -- le os dados dos cliente para gravar na tabela de NF's
       BEGIN
          SELECT basi_160.codigo_fiscal,        basi_167.codigo_fiscal_uf,
                 pedi_010.insc_est_cliente
          INTO   w_codigo_cidade_ibge,          w_codigo_fiscal_uf,
                 w_insc_est_cliente
          from   basi_160,  basi_167, basi_165,  pedi_010
          where pedi_010.cgc_9       = fatu_050.cgc_9
            and pedi_010.cgc_4       = fatu_050.cgc_4
            and pedi_010.cgc_2       = fatu_050.cgc_2
            and basi_160.cod_cidade  = pedi_010.cod_cidade
            and basi_160.estado      = basi_167.estado
            and basi_160.codigo_pais = basi_167.codigo_pais
            and basi_160.codigo_pais = basi_165.codigo_pais;

       EXCEPTION
          WHEN OTHERS THEN
             w_codigo_fiscal_uf   := NULL;
             w_codigo_cidade_ibge := NULL;
             w_insc_est_cliente   := NULL;

             inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                'Erro na leitura das informac?es de cidade, estado e pais do cliente ' || Chr(10) ||
                                '   Cliente: ' || fatu_050.cgc_9 || '/' || LPad(fatu_050.cgc_4,4,0) ||
                                '-' || LPad(fatu_050.cgc_2,2,0));
       END;

      if fatu_050.serie_nota_fisc = 'ECF'
       then
          w_modelo_nota := '02';
       end if;


       if w_modelo_nota = '08' or w_modelo_nota = '09' or
          w_modelo_nota = '10' or w_modelo_nota = '11' or
          w_modelo_nota = '57' or w_modelo_nota = '67' or
          w_modelo_nota = '07' or w_modelo_nota = '8B' or
          w_modelo_nota = '29' or w_modelo_nota = '27'

       then
         w_num_seq_modelo := 1;
       else
         if w_modelo_nota = '21' or w_modelo_nota = '22'
         then
            w_num_seq_modelo := 2;
         end if;
       end if;

       if (fatu_050.transpor_forne9 = p_cnpj9 and fatu_050.transpor_forne4 = p_cnpj4 and
          fatu_050.transpor_forne2 = p_cnpj2) or
          (fatu_050.transpor_forne9 = fatu_050.cgc_9 and fatu_050.transpor_forne4 = fatu_050.cgc_9 and
          fatu_050.transpor_forne2 = fatu_050.cgc_9)
       then
          fatu_050.transpor_forne9 := 0;
          fatu_050.transpor_forne4 := 0;
          fatu_050.transpor_forne2 := 0;
       end if;

       BEGIN
       select nvl(count(*),0)  into w_conta_reg_export
        from fatu_067
        where fatu_067.codigo_empresa        = fatu_050.codigo_empresa
          and fatu_067.num_nota_fiscal       = fatu_050.num_nota_fiscal
          and fatu_067.serie_nota_fisc       = fatu_050.serie_nota_fisc
          and fatu_067.data_averbacao_sped   between p_dat_inicial and p_dat_final;
       END;

       if w_conta_reg_export > 0
       then
           BEGIN
           select min(fatu_067.nr_declaracao_exp_sped), min(fatu_067.data_exp_sped),
                    min(fatu_067.tipo_exp_sped),          min(fatu_067.data_registro_exp_sped),
                    min(fatu_067.conhecimento_emb_sped),  min(fatu_067.data_conhecimento_emb_sped),
                    min(fatu_067.tipo_inform_conhecimento_sped), min(fatu_067.codigo_pais_siscomex_sped),
                    min(fatu_067.data_averbacao_sped),    min(fatu_067.tipo_nat_exp_sped),
                    min(fatu_067.numero_re)
            into fatu_050.nr_declaracao_exp_sped        ,fatu_050.data_exp_sped
                   ,fatu_050.tipo_exp_sped                ,fatu_050.data_registro_exp_sped
                   ,fatu_050.conhecimento_emb_sped        ,fatu_050.data_conhecimento_emb_sped
                   ,fatu_050.tipo_inform_conhecimento_sped,fatu_050.codigo_pais_siscomex_sped
                   ,fatu_050.data_averbacao_sped          ,fatu_050.tipo_nat_exp_sped
                   ,fatu_050.numero_re
              from fatu_067
              where fatu_067.codigo_empresa        = fatu_050.codigo_empresa
                and fatu_067.num_nota_fiscal       = fatu_050.num_nota_fiscal
                and fatu_067.serie_nota_fisc       = fatu_050.serie_nota_fisc
                and fatu_067.data_averbacao_sped   between p_dat_inicial and p_dat_final;
              EXCEPTION
              when no_data_found then
                  fatu_050.nr_declaracao_exp_sped        := null;
                  fatu_050.data_exp_sped                 := null;
                  fatu_050.tipo_exp_sped                 := null;
                  fatu_050.data_registro_exp_sped        := null;
                  fatu_050.conhecimento_emb_sped         := null;
                  fatu_050.data_conhecimento_emb_sped    := null;
                  fatu_050.tipo_inform_conhecimento_sped := null;
                  fatu_050.codigo_pais_siscomex_sped     := null;
                  fatu_050.data_averbacao_sped           := null;
                  fatu_050.tipo_nat_exp_sped             := null;
                  fatu_050.numero_re                     := null;
              END;
          end if;

       -- define se insere o endereco da nota no seq_end_entrega ou 0
       v_seq_end_entr := 0;

       BEGIN
          select  count(1)
          into    v_considera_seq_end_entr
          from    pedi_150
          where   1                         =    (select  MAX(empr_008.val_int)
                                                  from    empr_008
                                                  where  empr_008.codigo_empresa = p_cod_empresa
                                                  and    empr_008.param = 'vendas.enderecoDanfe') -- CONSIDERA PARyMETRO
          and     pedi_150.tipo_endereco    =     1 -- ENDEREyO DE ENTREGA
          and     pedi_150.cd_cli_cgc_cli9  =     fatu_050.cgc_9
          and     pedi_150.cd_cli_cgc_cli4  =     fatu_050.cgc_4
          and     pedi_150.cd_cli_cgc_cli2  =     fatu_050.cgc_2
          and     pedi_150.seq_endereco     =    fatu_050.seq_end_entr -- ENDEREyO DO PEDIDO != ENDEREyO DA NOTA
          and     pedi_150.cd_cli_cgc_cli4  = 0
          and    (pedi_150.cd_cli_cgc_cli9  > 0
          or      pedi_150.cd_cli_cgc_cli2  > 0); -- y CPF
       EXCEPTION
          WHEN OTHERS THEN
             v_seq_end_entr := 0;
       END;

      if v_considera_seq_end_entr = 1
      then
          v_seq_end_entr := nvl(fatu_050.seq_end_entr,0);
      else
          v_seq_end_entr := 0;
      end if;

      -- SE A NOTA FOR DENEGADA/inutilizada NyO INSERE - N = nota denegada/inutilizada
      if INTER_FN_NF_EH_DENEGADA(fatu_050.situacao_nfisc,w_modelo_nota,fatu_050.data_emissao) = 'N'
      then
         -- insere o regsitro da capa da nota fiscal na tabela
         BEGIN

            INSERT INTO sped_c100
               (cod_empresa
               ,num_nota_fiscal
               ,cod_serie_nota
               ,num_cnpj_9
               ,num_cnpj_4
               ,num_cnpj_2
               ,tip_entrada_saida
               ,tip_emissao_prop
               ,cod_modelo
               ,cod_situacao_nota
               ,dat_saida
               ,dat_emissao
               ,val_nota
               ,tip_pagto
               ,val_descon
               ,val_itens
               ,tip_frete
               ,val_frete
               ,val_seguro
               ,val_despesas
               ,val_base_icms
               ,val_icms
               ,val_base_icms_sub
               ,val_icms_sub
               ,val_ipi
               ,val_pis
               ,val_cofins
               ,val_pis_sub
               ,val_cofins_sub
               ,val_iss
               ,num_doc_referen
               ,cod_serie_doc_referen
               ,num_incri_substi
               ,tip_contrib_fim
               ,tip_nota_cancel
               ,sig_estado
               ,cod_natur_oper
               ,cod_cidade_ibge
               ,dat_vencto_fatura
               ,tip_transp
               ,num_cnpj_9_transp
               ,num_cnpj_4_transp
               ,num_cnpj_2_transp
               ,cod_especie_volume
               ,pes_bruto
               ,pes_liquido
               ,num_placa_veiculo
               ,tip_contrib_icms
               ,num_nota_eletric
               ,cod_observ
               ,num_conta_contabil
               ,num_declaracao_exp
               ,dat_exp
               ,tip_exp
               ,dat_registro_exp
               ,num_conhecimento_emb
               ,dat_conhecimento_emb
               ,tip_inform_conhecimento
               ,cod_pais_siscomex
               ,dat_averbacao
               ,tip_doc_importacao
               ,seq_end_entr
               ,num_reg_exp
               ,num_cupom_fiscal
               ,cod_impressora_fiscal
               ,qtde_volume
               ,especie_docto
               ,num_seq_modelo
               ,num_serie_ecf
               ,num_protocolo
               ,tp_nf
               ,flag_exp
               ,tipo_nf
               ,val_fcp_uf_dest_nf
               ,val_icms_uf_dest_nf
               ,val_icms_uf_remet_nf
               ,seq_end_entrega
               ) VALUES
               (p_cod_empresa
               ,fatu_050.num_nota_fiscal
               ,trim(fatu_050.serie_nota_fisc)
               ,fatu_050.cgc_9
               ,fatu_050.cgc_4
               ,fatu_050.cgc_2
               ,'S'
               ,'0' -- saidas sempre emiss?o propria
               ,lpad(w_modelo_nota,2,'0')
               ,fatu_050.situacao_nfisc
               ,fatu_050.data_saida
               ,fatu_050.data_emissao
               ,w_valor_total_nf  -- valor da nota
               ,w_aprazo_avista
               ,fatu_050.valor_desc_nota + fatu_050.vlr_desc_especial + fatu_050.valor_suframa
               ,fatu_050.valor_itens_nfis
               ,w_tipo_frete
               ,fatu_050.valor_frete_nfis
               ,fatu_050.valor_seguro
               ,fatu_050.valor_despesas+fatu_050.valor_encar_nota
               ,fatu_050.base_icms
               ,fatu_050.valor_icms
               ,fatu_050.base_icms_sub
               ,fatu_050.valor_icms_sub
               ,fatu_050.valor_ipi
               ,NULL  --val_pis
               ,NULL  --val_cofins
               ,0.00  --val_pis_sub
               ,0.00  --val_cofins_sub
               ,fatu_050.valor_iss
               ,decode(fatu_050.nota_fatura,0,fatu_050.nota_entrega,fatu_050.nota_fatura)  --num_doc_referen
               ,trim(fatu_050.serie_fatura)  --cod_serie_doc_referen
               ,' '  --num_incri_substi
               ,fatu_050.consumidor_final
               ,w_tip_nota_cancel
               ,fatu_050.natop_nf_est_oper
               ,fatu_050.natop_nf_nat_oper
               ,lpad(w_codigo_fiscal_uf,2,0) || lpad(w_codigo_cidade_ibge,5,0)
               ,w_data_vencto_duplic  --dat_vencto_fatura
               ,fatu_050.via_transporte
               ,fatu_050.transpor_forne9
               ,fatu_050.transpor_forne4
               ,fatu_050.transpor_forne2
               ,fatu_050.especie_volume
               ,fatu_050.peso_bruto
               ,fatu_050.peso_liquido
               ,w_placa_veiculo
               ,NULL
               ,trim(fatu_050.numero_danf_nfe)
               ,NULL
               ,NULL
               ,fatu_050.nr_declaracao_exp_sped
               ,fatu_050.data_exp_sped
               ,fatu_050.tipo_exp_sped
               ,fatu_050.data_registro_exp_sped
               ,fatu_050.conhecimento_emb_sped
               ,fatu_050.data_conhecimento_emb_sped
               ,fatu_050.tipo_inform_conhecimento_sped
               ,fatu_050.codigo_pais_siscomex_sped
               ,fatu_050.data_averbacao_sped
               ,fatu_050.tipo_nat_exp_sped
               ,fatu_050.seq_end_entr
               ,fatu_050.numero_re
               ,fatu_050.nr_cupom
               ,fatu_050.cod_impressora_fiscal
               ,fatu_050.qtde_embalagens
               ,fatu_050.especie_docto
               ,w_num_seq_modelo
               ,fatu_050.num_serie_ecf
               ,fatu_050.nr_protocolo
               ,w_tp_nf
               ,w_flag_exp
               ,fatu_050.tipo_nf
               ,fatu_050.val_fcp_uf_dest_nf
               ,fatu_050.val_icms_uf_dest_nf
               ,fatu_050.val_icms_uf_remet_nf
         ,v_seq_end_entr);

         EXCEPTION
            WHEN OTHERS THEN
               p_des_erro := 'Erro na inclus?o da tabela sped_c100 - Saidas ' || '- '||fatu_050.num_nota_fiscal|| Chr(10) || SQLERRM;
               RAISE W_ERRO;
         END;

         COMMIT;


         --
         -- Atualiza os itens da nota fiscal
         --
         FOR fatu_060 IN u_fatu_060 (fatu_050.codigo_empresa, fatu_050.num_nota_fiscal, fatu_050.serie_nota_fisc)
         LOOP

            if fatu_060.tipo_valores_fiscal <> '0'
            then

               fatu_060.perc_icms             := 0.00;
               fatu_060.valor_faturado        := 0.00;
               fatu_060.desconto_item         := 0.00;
               fatu_060.base_icms             := 0.00;
               fatu_060.valor_icms            := 0.00;
               fatu_060.rateio_despesa        := 0.00;
               fatu_060.rateio_despesas_ipi   := 0.00;
               fatu_060.base_icms_difer       := 0.00;
               fatu_060.valor_icms_difer      := 0.00;

               if fatu_060.tipo_valores_fiscal = '1'
               then

                  fatu_060.perc_ipi  := 0.00;
                  fatu_060.base_ipi  := 0.00;
                  fatu_060.valor_ipi := 0.00;

               end if;

            end if;

            begin
               select pedi_080.cod_natureza,     pedi_080.divisao_natur,
                      pedi_080.consiste_cvf_icms,pedi_080.cod_mensagem,
                      pedi_080.perc_substituica, pedi_080.ipi_sobre_icms
               into   w_num_cfop,                w_divisao_natureza,
                      w_consiste_cvf_icms,       w_cod_mensagem,
                      w_perc_substituica,        w_ipi_sobre_icms
               from pedi_080
               where pedi_080.natur_operacao = fatu_060.natopeno_nat_oper
                 and pedi_080.estado_natoper = fatu_060.natopeno_est_oper;
            exception
            when no_data_found then
               w_num_cfop          := '';
               w_divisao_natureza  := 0;
               w_consiste_cvf_icms := 1;
               w_cod_mensagem      := 0;
               w_ipi_sobre_icms    := 0;
            end;

            -- caso cliente tenha a inscricao fiscal igual a ISENTO, ele ira consistir o ipi na base do icms
            begin
              select empr_002.data_alt_valorizacao into w_data_icms_sped
              from empr_002;

              if w_data_icms_sped >= p_dat_inicial
              then
                 select count(*) into w_tem_registro from pedi_010
                 where  pedi_010.insc_est_cliente like 'ISENTO'
                    and pedi_010.cgc_9  = fatu_050.cgc_9
                    and pedi_010.cgc_4  = fatu_050.cgc_4
                    and pedi_010.cgc_2  = fatu_050.cgc_2;

                 if w_tem_registro > 0
                 then
                   w_ipi_sobre_icms := 1;
                 end if;
              end if;
            end;

            if w_ipi_sobre_icms =1
            then
               w_val_ipi_sobre_icms   := fatu_060.valor_ipi ;
            else
                w_val_ipi_sobre_icms   := 0.00 ;
            end if;

            if fatu_060.cvf_icms >= 30 and fatu_060.cvf_icms <> 70 and w_consiste_cvf_icms <> 3
            then
               fatu_060.valor_icms := 0.00;
            end if;

            if fatu_060.valor_icms = 0.00
                 then
                    fatu_060.base_icms   := 0.00;
                    fatu_060.perc_icms   := 0.00;
                    w_valor_reducao_icms := 0.00;
                 else
                    if fatu_060.cvf_icms = 20 or fatu_060.cvf_icms = 70
                    then
                       w_valor_reducao_icms := fatu_060.valor_contabil - ( fatu_060.base_icms + fatu_060.rateio_desc_propaganda + w_val_ipi_sobre_icms);
                    else
                       w_valor_reducao_icms := 0.00;
                    end if;

                    if w_valor_reducao_icms < 0.00
                    then
                         w_valor_reducao_icms := 0.00;
                    end if;
                 end if;

                 if fatu_060.valor_pis = 0.00
                 then
                    fatu_060.base_pis_cofins     := 0.00;
                    fatu_060.perc_pis            := 0.00;
                    fatu_060.perc_cofins         := 0.00;
                 end if;

                 if fatu_060.cvf_ipi > 1
                 then
                    fatu_060.base_ipi        := 0.00;
                    fatu_060.valor_ipi       := 0.00;
                    fatu_060.cvf_ipi         := 0.00;
                 end if;

             --BALENA
                 if (fatu_060.estagio_agrupador + fatu_060.estagio_agrupador_simultaneo) > 0
                 then
                   fatu_060.classific_fiscal := inter_fn_get_classific(fatu_060.nivel_estrutura, fatu_060.grupo_estrutura, fatu_060.subgru_estrutura,
                                              fatu_060.item_estrutura, fatu_060.estagio_agrupador, fatu_060.estagio_agrupador_simultaneo);
                 end if;

                begin
                   select trim(basi_240.cod_enquadramento_ipi)
                   into   w_cod_enquadramento_ipi
                   from basi_240
                   where basi_240.classific_fiscal = fatu_060.classific_fiscal;
                exception
                when no_data_found then
                   w_cod_enquadramento_ipi := null;
                end;

             w_cod_enquadramento_ipi := null;

                 --Verifica se o item possui movimentac?o fisica
                 begin
                    select pedi_080.tem_mov_fisica into w_tipo_transacao
                    from pedi_080
                    where pedi_080.natur_operacao = fatu_060.natopeno_nat_oper
                      and pedi_080.estado_natoper = fatu_060.natopeno_est_oper;
                 exception
                 when no_data_found then
                    w_tipo_transacao := 0;
                 end;

                 -- verifica se o tamanho da classificacao fiscal esta correta, caso nao esteja, gera mensagem de erro
                 IF  Nvl(Length(REPLACE(fatu_060.classific_fiscal,'.')),0) > 8
                 THEN

                     inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                  'Procedure: p_gera_sped_C100 ' || Chr(10) ||
                                  'Tamanho do campo classificac?o fiscal e maior que o permitido ' || Chr(10) ||
                                  '   Produto: ' || fatu_060.nivel_estrutura || '-' ||
                                                    fatu_060.grupo_estrutura  || '-' ||
                                                    fatu_060.subgru_estrutura || '-' ||
                                                    fatu_060.item_estrutura   || Chr(10) ||
                                  '   Classif. Fiscal ' || fatu_060.classific_fiscal);
                     fatu_060.classific_fiscal := substr(trim(REPLACE(fatu_060.classific_fiscal,'.')),1,8);
                 ELSE
                    fatu_060.classific_fiscal := substr(trim(REPLACE(fatu_060.classific_fiscal,'.')),1,8);

                    IF  fatu_060.classific_fiscal = '0'
                    THEN
                     inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                  'Procedure: p_gera_sped_C100 ' || Chr(10) ||
                                  'O campo classificac?o fiscal esta com ZERO ' || Chr(10) ||
                                  '   Nota fiscal: ' || to_char(fatu_050.codigo_empresa,'000') || '-' ||
                                                        to_char(fatu_050.num_nota_fiscal,'000000000')  || '-' ||
                                                        fatu_050.serie_nota_fisc || '-' ||
                                  '   Classif. Fiscal ' || fatu_060.classific_fiscal);

                    END IF;
                 END IF;

                 /*FAZ TRATAMENTO PARA PRODUTOS ZERO - TROCA O CyDIGO DO PRODUTO PELA CLASSIFICAyyO FISCAL QUE ESTy NA NF** */


                 if fatu_060.nivel_estrutura = '0' and
                    fatu_060.grupo_estrutura = '00000'
                 then

                    if substr(fatu_060.classific_fiscal,1,5) is not null
                    then
                       ws_grupo_clas := substr(fatu_060.classific_fiscal,1,5);
                    else
                       ws_grupo_clas := ' ';
                    end if;

                    if substr(fatu_060.classific_fiscal,6,3) is not null
                    then
                       ws_subgru_clas := substr(fatu_060.classific_fiscal,6,3);
                    else
                       ws_subgru_clas := ' ';
                    end if;


                    fatu_060.grupo_estrutura  := ws_grupo_clas;
                    fatu_060.subgru_estrutura := ws_subgru_clas;
                    fatu_060.nivel_estrutura  := ' ';
                    fatu_060.item_estrutura   := ' ';
                 end if;

                 if w_tem_referenciado = 'n'
                 then
                    begin
                       select count(*) into w_conta_msg from fatu_052
                       where fatu_052.cod_empresa    = fatu_050.codigo_empresa
                         and fatu_052.num_nota       = fatu_050.num_nota_fiscal
                         and trim(fatu_052.cod_serie_nota) = trim(fatu_050.serie_nota_fisc)
                         and fatu_052.cnpj9                = fatu_050.cgc_9
                         and fatu_052.cnpj4                = fatu_050.cgc_4
                         and fatu_052.cnpj2                = fatu_050.cgc_2
                         and fatu_052.ind_entr_saida       = 'S';
                    end;

                    if fatu_060.num_nota_orig > 0 and w_conta_msg = 0
                    then
                       w_tem_referenciado := 's';

                       inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                      'Procedure: p_gera_sped_C100 ' || Chr(10) ||
                                      'Esta e uma nota de devoluc?o e n?o existe codigo de mensagem relacionado. Verifique ' || Chr(10) ||
                                      'Sem codigo de mensagem relacionado n?o ira gerar o registor C113, obrigatorio neste caso.' || Chr(10) ||
                                      '   Nota fiscal: ' || to_char(fatu_050.num_nota_fiscal,'000000') || ' - ' ||
                                                                    fatu_050.serie_nota_fisc);
                    end if;
                 end if;
               w_sequencia := w_sequencia + 1;

               BEGIN
                  INSERT INTO sped_c170
                      (cod_empresa
                      ,num_nota_fiscal
                      ,cod_serie_nota
                      ,seq_item_nota
                      ,num_cnpj_9
                      ,num_cnpj_4
                      ,num_cnpj_2
                      ,tip_entrada_saida
                      ,cod_nivel
                      ,cod_grupo
                      ,cod_subgru
                      ,cod_item
                      ,des_item
                      ,cod_unid_medida
                      ,val_unitario
                      ,val_faturado
                      ,val_contabil
                      ,qtde_item_fatur
                      ,cod_c_custo
                      ,tip_transacao
                      ,val_desconto
                      ,cvf_icms
                      ,cod_nat_oper
                      ,sig_estado_oper
                      ,num_cfop
                      ,val_base_icms
                      ,perc_icms
                      ,val_icms
                      ,val_base_icms_sub_trib
                      ,perc_icms_sub_trib
                      ,val_base_red_sub_trib
                      ,val_icms_sub_trib
                      ,tip_apuracao
                      ,cod_enquadramento_ipi
                      ,val_base_ipi
                      ,perc_ipi
                      ,val_ipi
                      ,cvf_ipi
                      ,val_basi_pis_cofins
                      ,val_pis
                      ,perc_pis
                      ,cvf_pis
                      ,cvf_cofins
                      ,perc_cofins
                      ,val_cofins
                      ,perc_iss
                      ,dat_emissao
                      ,tip_cli_forn
                      ,cod_classific_fiscal
                      ,pes_liquido
                      ,val_frete
                      ,val_seguro
                      ,val_outros
                      ,val_icms_difer
                      ,tip_trib_federal
                      ,tip_trib_estadual
                      ,tip_trib_icms
                      ,num_lote
                      ,tip_apuracao_ipi
                      ,val_iss
                      ,num_conta_contabil
                      ,cod_empresa_refer
                      ,num_nota_refer
                      ,cod_serie_refer
                      ,seq_nota_refer
                      ,num_cnpj9_refer
                      ,num_cnpj4_refer
                      ,num_cnpj2_refer
                      ,cod_deposito
                      ,val_rateio
                      ,tipo_transacao
                      ,cod_mensagem_nat_oper
                      ,cod_transacao
                      ,cod_contabil
                      ,despesa_nf_item
                      ,cod_mensagem_fiscal
                      ,val_fcp_uf_dest
                      ,val_icms_uf_dest
                      ,val_icms_uf_remet
                      ,estagio_agrupador
                      ,estagio_agrupador_simultaneo
                      ,seq_operacao_agrupador_insu
                      ) VALUES
                      (p_cod_empresa                     -- cod_empresa
                      ,fatu_050.num_nota_fiscal                  -- num_nota_fiscal
                      ,trim(fatu_050.serie_nota_fisc)             -- cod_serie_nota
                      ,w_sequencia                                 -- seq_item_nota
                      ,fatu_050.cgc_9                              --num_cnpj_9
                      ,fatu_050.cgc_4                              --num_cnpj_4
                      ,fatu_050.cgc_2                              --num_cnpj_2
                      ,'S'                                         --tip_entrada_saida
                      ,fatu_060.nivel_estrutura                    --cod_nivel
                      ,fatu_060.grupo_estrutura                    --cod_grupo
                      ,fatu_060.subgru_estrutura                   --cod_subgru
                      ,fatu_060.item_estrutura                     --cod_item
                      ,trim(replace(fatu_060.descricao_item, chr(9), ' ')) || ' ' || inter_fn_descr_blk(fatu_060.estagio_agrupador ,fatu_060.estagio_agrupador_simultaneo)  --des_item
                      ,fatu_060.unidade_medida                     --cod_unid_medida
                      ,decode(fatu_060.qtde_item_fatur,0.00,0.00,fatu_060.valor_faturado/fatu_060.qtde_item_fatur)  --val_unitario
                      ,fatu_060.valor_faturado                     --val_faturado
                      ,fatu_060.valor_contabil                     --val_contabil
                      ,decode(fatu_060.qtde_item_fatur,0.000,1.000,fatu_060.qtde_item_fatur)   --qtde_item_fatur
                      ,fatu_060.centro_custo                       --cod_c_custo
                      ,w_tipo_transacao                                        --tip_transacao
                      ,fatu_060.desconto_item                      --val_desconto
                      ,fatu_060.cvf_icms                           --cvf_icms
                      ,fatu_060.natopeno_nat_oper                  --cod_nat_oper
                      ,fatu_060.natopeno_est_oper                  --sig_estado_oper
                      ,REPLACE(w_num_cfop,'.') || w_divisao_natureza
                      ,fatu_060.base_icms                          --val_base_icms
                      ,fatu_060.perc_icms                          --perc_icms
                      ,fatu_060.valor_icms                         --val_icms
                      ,decode(fatu_060.valor_icms_difer,0.00,0.00,fatu_060.base_icms_difer)  --val_base_icms_sub_trib
                      ,w_perc_substituica                          --perc_icms_sub_trib
                      ,w_valor_reducao_icms                        --val_base_red_sub_trib
                      ,fatu_060.valor_icms_difer                   --val_icms_sub_trib
                      ,1                                           --tip_apuracao
                      ,w_cod_enquadramento_ipi                     --cod_enquadramento_ipi
                      ,fatu_060.base_ipi                           --val_base_ipi
                      ,fatu_060.perc_ipi                           --perc_ipi
                      ,fatu_060.valor_ipi                          --val_ipi
                      ,fatu_060.cvf_ipi_saida                      --cvf_ipi
                      ,fatu_060.basi_pis_cofins                    --val_basi_pis_cofins
                      ,fatu_060.valor_pis                          --val_pis
                      ,fatu_060.perc_pis                           --perc_pis
                      ,fatu_060.cvf_pis                            --cvf_pis
                      ,fatu_060.cvf_cofins                         --cvf_cofins
                      ,fatu_060.perc_cofins                        --perc_cofins
                      ,fatu_060.valor_cofins                       --val_cofins
                      ,fatu_060.perc_iss                           --perc_iss
                      ,fatu_060.data_emissao                       --dat_emissao
                      ,NULL                                        --tip_cli_forn
                      ,REPLACE(fatu_060.classific_fiscal,'.')      --cod_classific_fiscal
                      ,fatu_060.peso_liquido                       --pes_liquido
                      ,0.00                                        --val_frete
                      ,0.00                                        --val_seguro
                      ,0.00                                        --val_outros
                      ,0.00                                        --val_icms_difer
                      ,0.00                                        --tip_trib_federal
                      ,fatu_060.procedencia                        --tip_trib_estadual
                      ,fatu_060.cvf_icms                           --tip_trib_icms
                      ,fatu_060.lote_acomp                         --num_lote
                      ,1                                           --tip_apuracao_ipi
                      ,fatu_060.valor_iss                          --val_iss
                      ,NULL
                      ,fatu_060.ch_it_nf_cd_empr
                      ,fatu_060.num_nota_orig
                      ,trim(fatu_060.serie_nota_orig)
                      ,null
                      ,fatu_060.cgc9_origem
                      ,fatu_060.cgc4_origem
                      ,fatu_060.cgc2_origem
                      ,fatu_060.deposito
                      ,fatu_060.rateio_despesa
                      ,w_tipo_transacao
                      ,null
                      ,fatu_060.transacao
                      ,fatu_060.codigo_contabil
                      ,fatu_060.rateio_despesas_ipi
                      ,w_cod_mensagem
                      ,fatu_060.val_fcp_uf_dest
                      ,fatu_060.val_icms_uf_dest
                      ,fatu_060.val_icms_uf_remet
                      ,fatu_060.estagio_agrupador
                      ,fatu_060.estagio_agrupador_simultaneo
                      ,fatu_060.seq_operacao_agrupador_insu
                      );
             EXCEPTION
                 WHEN OTHERS THEN
                   p_des_erro := 'Erro na inclus?o da tabela sped_c170 - Saidas ' || Chr(10) || SQLERRM;
                   RAISE W_ERRO;
             END;

         END LOOP;

         begin
            select sum(sped_c170.val_pis),      sum(sped_c170.val_cofins),
                   sum(sped_c170.val_base_ipi), sum(sped_c170.val_ipi),
                   sum(sped_c170.val_base_icms),sum(sped_c170.val_icms),
                   sum(decode(val_icms_sub_trib,0.00,0.00,val_base_icms_sub_trib)), sum(val_icms_sub_trib),
                   sum(sped_c170.val_fcp_uf_dest),
                   sum(sped_c170.val_icms_uf_dest),
                   sum(sped_c170.val_icms_uf_remet)
            into   w_val_total_pis,             w_val_total_cofins,
                   w_val_base_ipi,              w_val_ipi,
                   w_val_base_icms,             w_val_icms,
                   w_val_base_icms_sub_trib,    w_val_icms_sub_trib,
                   w_val_fcp_uf_dest,
                   w_val_icms_uf_dest,
                   w_val_icms_uf_remet
            from sped_c170
            where sped_c170.cod_empresa     = p_cod_empresa
              and sped_c170.num_nota_fiscal = fatu_050.num_nota_fiscal
              and trim(sped_c170.cod_serie_nota)  = trim(fatu_050.serie_nota_fisc)
              and sped_c170.num_cnpj_9      = fatu_050.cgc_9
              and sped_c170.num_cnpj_4      = fatu_050.cgc_4
              and sped_c170.num_cnpj_2      = fatu_050.cgc_2
              and sped_c170.tip_entrada_saida = 'S';
         exception
         when no_data_found then
              w_val_total_pis    := 0.00;
              w_val_total_cofins := 0.00;
              w_val_base_ipi     := 0.00;
              w_val_ipi          := 0.00;
              w_val_base_icms    := 0.00;
              w_val_icms         := 0.00;
              w_val_base_icms_sub_trib := 0.00;
              w_val_icms_sub_trib      := 0.00;
              w_val_fcp_uf_dest        := 0.00;
              w_val_icms_uf_dest       := 0.00;
              w_val_icms_uf_remet      := 0.00;
         end;


         begin
            update sped_c100
              set sped_c100.val_pis           = w_val_total_pis,
                  sped_c100.val_cofins        = w_val_total_cofins,
                  sped_c100.val_base_ipi      = w_val_base_ipi,
                  sped_c100.val_ipi           = w_val_ipi,
                  sped_c100.val_base_icms     = w_val_base_icms,
                  sped_c100.val_icms          = w_val_icms,
                  sped_c100.val_base_icms_sub = w_val_base_icms_sub_trib,
                  sped_c100.val_icms_sub      = w_val_icms_sub_trib,
                  sped_c100.val_fcp_uf_dest_nf    = w_val_fcp_uf_dest,
                  sped_c100.val_icms_uf_dest_nf   = w_val_icms_uf_dest,
                  sped_c100.val_icms_uf_remet_nf  = w_val_icms_uf_remet
              where sped_c100.cod_empresa     = p_cod_empresa
                and sped_c100.num_nota_fiscal = fatu_050.num_nota_fiscal
                and trim(sped_c100.cod_serie_nota)  = trim(fatu_050.serie_nota_fisc)
                and sped_c100.num_cnpj_9      = fatu_050.cgc_9
                and sped_c100.num_cnpj_4      = fatu_050.cgc_4
                and sped_c100.num_cnpj_2      = fatu_050.cgc_2
                and sped_c100.tip_entrada_saida = 'S';
          EXCEPTION
          WHEN OTHERS THEN
               p_des_erro := 'Erro na atualizac?o da tabela sped_c100 - Saidas ' || Chr(10) || SQLERRM;
               RAISE W_ERRO;
          END;


         COMMIT;
       end if;
   END LOOP;

   --
   -- Notas de Entrada
   --

   select empr_001.fornec_ener_elet, empr_001.fornec_telecomu,
          empr_001.fornec_transport, empr_001.fornec_agua,
          empr_001.fornec_gas
   into   w_tp_forne_elet,           w_tp_forne_tele,
          w_tp_forne_trans,          w_tp_forne_agua,
          w_tp_forne_gas
   from empr_001;

   FOR obrf_010 IN u_obrf_010 (p_cod_empresa, p_dat_inicial, p_dat_final,
                               p_cnpj9, p_cnpj4, p_cnpj2, p_junta_empresa) LOOP

      if obrf_010.tipo_valores_fiscal <> '0'
      then

         obrf_010.valor_desconto         := 0.00;
         obrf_010.valor_itens            := 0.00;
         obrf_010.valor_despesas         := 0.00;
         obrf_010.base_icms              := 0.00;
         obrf_010.valor_icms             := 0.00;
         obrf_010.base_icms_sub          := 0.00;
         obrf_010.valor_icms_sub         := 0.00;

         if obrf_010.tipo_valores_fiscal = '1'
         then

            obrf_010.valor_total_ipi := 0.00;

         end if;

      end if;

    w_tipo_frete := obrf_010.wtipo_frete;

      select count(*) into w_itens_entram_livro
      from obrf_015 , pedi_080
      where obrf_015.capa_ent_nrdoc   = obrf_010.documento
      and   obrf_015.capa_ent_serie   = obrf_010.serie
      and   obrf_015.capa_ent_forcli9 = obrf_010.cgc_cli_for_9
      and   obrf_015.capa_ent_forcli4 = obrf_010.cgc_cli_for_4
      and   obrf_015.capa_ent_forcli2 = obrf_010.cgc_cli_for_2
      and   obrf_015.natitem_nat_oper = pedi_080.natur_operacao
      and   obrf_015.natitem_est_oper = pedi_080.estado_natoper
      and   pedi_080.livros_fiscais  <> 2;

      if w_itens_entram_livro > 0
      then

           w_sequencia := 0;

           begin
              select fatu_505.tipo_titulo, fatu_505.tipo_nf
              into   w_tipo_titulo,        w_tp_nf
              from fatu_505
              where fatu_505.codigo_empresa  = obrf_010.local_entrega
                and fatu_505.serie_nota_fisc = obrf_010.serie;
           exception
           when no_data_found then
              w_tipo_titulo  := null;
              w_tp_nf        := w_tp_nf;
           end;

          begin
            select pedi_080.emite_duplicata into w_emite_duplicata from pedi_080
            where pedi_080.natur_operacao = obrf_010.natoper_nat_oper
              and pedi_080.estado_natoper = obrf_010.natoper_est_oper;
          exception
           when no_data_found then
              w_emite_duplicata  := 0;
           end;

          if w_emite_duplicata = 2
          then
             w_qtde_parcela  := 0;
          else
             begin
                select count(*) into w_qtde_parcela from cpag_010
                where cpag_010.codigo_empresa = obrf_010.local_entrega
                  and cpag_010.documento      = obrf_010.documento
                  and cpag_010.serie          = obrf_010.serie
                  and cpag_010.cgc_9          = obrf_010.cgc_cli_for_9
                  and cpag_010.cgc_4          = obrf_010.cgc_cli_for_4
                  and cpag_010.cgc_2          = obrf_010.cgc_cli_for_2
                  and cpag_010.referente_nf   = 1
                  and rownum                  = 1;
             exception
               when no_data_found then
                  w_qtde_parcela  := 0;
             end;
          end if;

           if w_qtde_parcela > 0
           then
              begin
                 select supr_055.vencimento into w_intervalo_parcela from supr_055
                 where supr_055.condicao_pagto = obrf_010.condicao_pagto
                   and rownum = 1;
              exception
              when no_data_found then
                 w_intervalo_parcela  := 0;
              end;
           end if;

           if w_qtde_parcela = 0
           then
              w_aprazo_avista := 9; -- sem pagamento
           else
              if w_intervalo_parcela = 0 and w_qtde_parcela = 1
              then
                  w_aprazo_avista := 1; -- avista
              else
                 w_aprazo_avista := 2; -- aprazo
              end if;
           end if;


           if w_aprazo_avista = 2
           then
              begin
              select cpag_010.data_vencimento into w_data_vencto_duplic   from cpag_010
                 where cpag_010.codigo_empresa = obrf_010.local_entrega
                   and cpag_010.documento      = obrf_010.documento
                   and cpag_010.serie          = obrf_010.serie
                   and cpag_010.cgc_9          = obrf_010.cgc_cli_for_9
                   and cpag_010.cgc_4          = obrf_010.cgc_cli_for_4
                   and cpag_010.cgc_2          = obrf_010.cgc_cli_for_2
                   and cpag_010.referente_nf   = 1
                   and rownum                  = 1
                   and cpag_010.tipo_titulo    = w_tipo_titulo;
              exception
              when no_data_found then
                 w_data_vencto_duplic  := null;
              end;
           else
              if w_aprazo_avista = 1
              then
                 w_data_vencto_duplic := obrf_010.data_transacao;
              else
                 w_data_vencto_duplic := NULL;
              end if;
           end if;

           -- encontra o modelo da nota fiscal conforme regra ja existente no sintegra.
           -- se o modelo que esta na cadastrado no cadastro de natirezas de operac?es for nulo ou zero
           -- ent?o forca o modelo conforme tipo dpo documento ou tipo do fornecedor

           w_cod_especie := null;
           w_serie_nfe   := null;
           w_modelo_especie := null;

           if obrf_010.situacao_entrada <> 4
           then
              BEGIN
                 select trim(fatu_505.codigo_especie), trim(fatu_505.serie_nfe)
                 into w_cod_especie,           w_serie_nfe
                 from fatu_505
                 where fatu_505.codigo_empresa  = obrf_010.local_entrega
                   and fatu_505.serie_nota_fisc = obrf_010.serie;
              EXCEPTION
                 WHEN OTHERS THEN
                    w_cod_especie := null;
                    w_serie_nfe   := null;
              END;
           else
              w_cod_especie := obrf_010.especie_docto;
           end if;

           if w_serie_nfe = 'S' and trim(obrf_010.numero_danf_nfe) is not null and obrf_010.tipo_conhecimento = 0 or
              (obrf_010.tipo_conhecimento = 0 and trim(obrf_010.numero_danf_nfe) is not null and obrf_010.situacao_entrada = 4)
           then
              obrf_010.modelo_doc_fisc := '55';

              if w_cod_especie is not null
              then
                 begin
                    select trim(obrf_200.modelo_documento)
                      into w_modelo_especie
                    from obrf_200
                    where obrf_200.codigo_especie  = w_cod_especie;
                 EXCEPTION
                    WHEN OTHERS THEN
                       w_modelo_especie := null;
                 END;
              end if;

              if w_modelo_especie is not null
              then
                 obrf_010.modelo_doc_fisc := w_modelo_especie;
              else
                 obrf_010.modelo_doc_fisc := '55';
                 w_modelo_especie := '55';
              end if;
           else
              if w_cod_especie is not null
              then
                 begin
                    select trim(obrf_200.modelo_documento)
                      into w_modelo_especie
                    from obrf_200
                    where obrf_200.codigo_especie  = w_cod_especie;
                 EXCEPTION
                    WHEN OTHERS THEN
                       w_modelo_especie := null;
                 END;
              end if;
           end if;

           if w_modelo_especie is not null
           then
              if Nvl(Length(w_modelo_especie),0) > 2
              then
                 inter_pr_insere_erro_sped ('F',p_cod_empresa,
                          'Procedure: p_gera_sped_C100 ' || Chr(10) ||
                          'Tamanho do campo modelo nota no cadastro de especie e maior que o permitido ' || Chr(10) ||
                          '   Serie: ' || obrf_010.local_entrega   || Chr(10) ||
                          '   Modelo ' || w_modelo_especie);
                       w_modelo_especie := substr(w_modelo_especie,1,2);
              end if;


              obrf_010.modelo_doc_fisc := w_modelo_especie;
           end if;

           w_ind_nf_consumo := 'N';

           begin
              select tipo_fornecedor into w_tipo_fornecedor
              from supr_010
              where fornecedor9 = obrf_010.cgc_cli_for_9
              and   fornecedor4 = obrf_010.cgc_cli_for_4
              and   fornecedor2 = obrf_010.cgc_cli_for_2;
           EXCEPTION
              WHEN OTHERS THEN
                 w_tipo_fornecedor := null;
           END;

           if obrf_010.modelo_doc_fisc is null or obrf_010.modelo_doc_fisc = '00'
           then

              if obrf_010.tipo_conhecimento in (1,3)
              then
                 if trim(obrf_010.numero_danf_nfe) is not null
                 then
                    w_modelo_nota := w_modelo_especie;
                 else
                    if obrf_010.via_transporte = 1
                    then
                       w_modelo_nota := '08';
                    else

                       if obrf_010.via_transporte = 2
                       then
                          w_modelo_nota := '10';
                       else

                          if obrf_010.via_transporte = 3 or obrf_010.via_transporte = 5
                          then
                             w_modelo_nota := '09';
                          else

                             if obrf_010.via_transporte = 4
                             then
                                 w_modelo_nota := '11';
                             end if;
                          end if;
                       end if;
                    end if;
                 end if;
              else

                 if w_tipo_fornecedor = w_tp_forne_elet
                 then
                    w_modelo_nota := '06';
                 else

                    if w_tipo_fornecedor = w_tp_forne_tele
                    then
                       w_modelo_nota := '22';
                    else

                       if w_tipo_fornecedor = w_tp_forne_agua
                       then
                          w_modelo_nota := '27';
                       else

                          if w_tipo_fornecedor = w_tp_forne_gas
                          then
                             w_modelo_nota := '28';
                          else
                              w_modelo_nota := '01';
                          end if;
                       end if;
                    end if;
                 end if;
              end if;
           else
              w_modelo_nota := obrf_010.modelo_doc_fisc;
           end if;

           if obrf_010.situacao_entrada = 2
           then
              w_tip_nota_cancel := 'S';
           else
              w_tip_nota_cancel := 'N';
           end if;

           if obrf_010.situacao_entrada <> 2
           then
              /* Se a nota for diferente de 1 (Normal) e n?o estiver cancelada,
              classifica ela como nota complementar */
              if (obrf_010.tipo_nf_referenciada = 2)
              then
                 w_situacao_entrada := 6;
              else
                 if obrf_010.tipo_nf_referenciada = 3 or
                    obrf_010.serie = '890' or
                    obrf_010.serie = '891' or
                    obrf_010.serie = '892' or
                    obrf_010.serie = '893' or
                    obrf_010.serie = '894' or
                    obrf_010.serie = '895' or
                    obrf_010.serie = '896' or
                    obrf_010.serie = '897' or
                    obrf_010.serie = '898' or
                    obrf_010.serie = '899'
                 then
                    w_situacao_entrada := 8;
                 else
                    w_situacao_entrada := 0;
                 end if;
              end if;
           else
              if obrf_010.situacao_entrada = 2 and obrf_010.cod_justificativa = 1
              then
                 w_situacao_entrada := 5;
                 else if obrf_010.situacao_entrada = 2 and obrf_010.cod_status in ('205','110','301','302') /*Nota denegada*/
                 then
                    w_situacao_entrada := 4;
                 else
                    w_situacao_entrada := obrf_010.situacao_entrada;
                 end if;
              end if;
           end if;

           if INTER_FN_NF_EH_DENEGADA(w_situacao_entrada,w_modelo_nota,obrf_010.data_transacao) = 'N'
           then

             BEGIN
                SELECT basi_160.codigo_fiscal,        basi_167.codigo_fiscal_uf,
                       supr_010.inscr_est_forne
                INTO   w_codigo_cidade_ibge,          w_codigo_fiscal_uf,
                       w_insc_est_cliente
                from   basi_160,  basi_167, basi_165, supr_010
                where supr_010.fornecedor9  = obrf_010.cgc_cli_for_9
                  and supr_010.fornecedor4  = obrf_010.cgc_cli_for_4
                  and supr_010.fornecedor2  = obrf_010.cgc_cli_for_2
                  and basi_160.cod_cidade   = supr_010.cod_cidade
                  and basi_160.estado       = basi_167.estado
                  and basi_160.codigo_pais  = basi_167.codigo_pais
                  and basi_160.codigo_pais  = basi_165.codigo_pais;
             EXCEPTION
             WHEN OTHERS THEN
                w_codigo_fiscal_uf   := NULL;
                w_codigo_cidade_ibge := NULL;
                w_insc_est_cliente   := NULL;

                inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                   'Erro na leitura das informac?es de cidade, estado e pais do cliente ' || Chr(10) ||
                                   '   Fornecedor: ' || obrf_010.cgc_cli_for_9 || '/' || LPad(obrf_010.cgc_cli_for_4,4,0) ||
                                   '-' || LPad(obrf_010.cgc_cli_for_2,2,0));
             END;

             w_num_seq_modelo := 0;

             if w_modelo_nota = '08' or w_modelo_nota = '09' or
                w_modelo_nota = '10' or w_modelo_nota = '11' or
                w_modelo_nota = '57' or w_modelo_nota = '67' or
                w_modelo_nota = '07' or w_modelo_nota = '8B' or
                w_modelo_nota = '29' or w_modelo_nota = '27'

             then
               w_num_seq_modelo := 1;
             else
               if w_modelo_nota = '21' or w_modelo_nota = '22'
               then
                  w_num_seq_modelo := 2;
               end if;
             end if;


             if p_dat_inicial > obrf_010.data_transacao
             then
                w_flag_exp := 'S';
             else
                w_flag_exp := 'N';
             end if;

             begin
             select distinct trim(replace(replace(obrf_056.numero_di,'/'),'-'))
             into w_numero_di
             from obrf_056
             where obrf_056.capa_ent_nrdoc   = obrf_010.documento
               and obrf_056.capa_ent_serie   = trim(obrf_010.serie)
               and obrf_056.capa_ent_forcli9 = obrf_010.cgc_cli_for_9
               and obrf_056.capa_ent_forcli4 = obrf_010.cgc_cli_for_4
               and obrf_056.capa_ent_forcli2 = obrf_010.cgc_cli_for_2
               and rownum = 1;
             exception
                when no_data_found then

                w_numero_di := null;
             end;

             -- inicio do bloco para encontrar a cidade de origem e destino para o bloco D100
             w_cidade_origem    := 0000000;
             w_cidade_destino   := 0000000;
             w_cidade_origemNF  := 0000000;
             w_cidade_destinoNF := 0000000;

             if obrf_010.tipo_conhecimento = 1
             then
                w_cidade_destino := w_cidadde_fiscal_empresa;

                begin
                   select lpad(basi_167.codigo_fiscal_uf,2,0) || lpad(basi_160.codigo_fiscal,5,0) , f.tipo_frete
                   into w_cidade_origem , w_tipo_frete
                   from obrf_016, supr_010, basi_160, basi_167 , obrf_010 f
                   where obrf_016.num_conhecimento = obrf_010.documento
                   and   obrf_016.ser_conhecimento = obrf_010.serie
                   and   obrf_016.transportadora9  = obrf_010.cgc_cli_for_9
                   and   obrf_016.transportadora4  = obrf_010.cgc_cli_for_4
                   and   obrf_016.transportadora2  = obrf_010.cgc_cli_for_2
                   and   f.documento               = obrf_016.numero_nota
                   and   f.serie                   = obrf_016.serie_nota
                   and   obrf_016.fornecedor9      = supr_010.fornecedor9
                   and   obrf_016.fornecedor4      = supr_010.fornecedor4
                   and   obrf_016.fornecedor2      = supr_010.fornecedor2

                   and   supr_010.cod_cidade       = basi_160.cod_cidade

                   and   basi_160.estado           = basi_167.estado
                   and   basi_160.codigo_pais      = basi_167.codigo_pais
                   and   rownum                    = 1;
                exception
                   when others then
                      w_cidade_origem := lpad(w_codigo_fiscal_uf,2,0) || lpad(w_codigo_cidade_ibge,5,0);
                      w_tipo_frete   := 0;
                end;
             end if;

             if obrf_010.tipo_conhecimento = 3
             then

                w_cidade_origem := w_cidadde_fiscal_empresa;

                begin

                   select lpad(basi_167.codigo_fiscal_uf,2,0) || lpad(basi_160.codigo_fiscal,5,0) , fatu_050.tipo_frete
                   into   w_cidade_destino  , w_tipo_frete
                   from obrf_015, fatu_050, pedi_010, basi_160, basi_167
                   where obrf_015.capa_ent_nrdoc    = obrf_010.documento
                   and   obrf_015.capa_ent_serie    = obrf_010.serie
                   and   obrf_015.capa_ent_forcli9  = obrf_010.cgc_cli_for_9
                   and   obrf_015.capa_ent_forcli4  = obrf_010.cgc_cli_for_4
                   and   obrf_015.capa_ent_forcli2  = obrf_010.cgc_cli_for_2

                   and   fatu_050.codigo_empresa    = obrf_015.empr_nf_saida
                   and   fatu_050.num_nota_fiscal   = obrf_015.num_nf_saida
                   and   fatu_050.serie_nota_fisc   = obrf_015.serie_nf_saida

                   and   pedi_010.cgc_9             = fatu_050.cgc_9
                   and   pedi_010.cgc_4             = fatu_050.cgc_4
                   and   pedi_010.cgc_2             = fatu_050.cgc_2

                   and   basi_160.cod_cidade        = pedi_010.cod_cidade

                   and   basi_167.estado            = basi_160.estado
                   and   basi_167.codigo_pais       = basi_160.codigo_pais
                   and   rownum                     = 1;

                exception
                   when others then
                      w_cidade_destino := lpad(w_codigo_fiscal_uf,2,0) || lpad(w_codigo_cidade_ibge,5,0);
                      w_tipo_frete   := 0 ;
                end;
             end if;

             if obrf_010.cod_cidade_cte > 0
             then
                begin
                  select lpad(basi_167.codigo_fiscal_uf,2,0) || lpad(basi_160.codigo_fiscal,5,0)
                     into   w_cidade_origemNF
                     from basi_160, basi_167
                     where basi_160.cod_cidade        = obrf_010.cod_cidade_cte
                       and   basi_167.estado          = basi_160.estado
                       and   basi_167.codigo_pais     = basi_160.codigo_pais
                       and   rownum                   = 1;
                exception
                   when others then
                      w_cidade_origemNF := 0000000;
                end;
             else
                w_cidade_origemNF := w_cidade_origem;
             end if;

             if obrf_010.cod_cidade_cte_dest > 0
             then
                begin
                  select lpad(basi_167.codigo_fiscal_uf,2,0) || lpad(basi_160.codigo_fiscal,5,0)
                     into   w_cidade_destinoNF
                     from basi_160, basi_167
                     where basi_160.cod_cidade        = obrf_010.cod_cidade_cte_dest
                       and   basi_167.estado          = basi_160.estado
                       and   basi_167.codigo_pais     = basi_160.codigo_pais
                       and   rownum                   = 1;
                exception
                   when others then
                      w_cidade_destinoNF := 0000000;
                end;
             else
                w_cidade_destinoNF := w_cidade_destino;
             end if;

             BEGIN
                INSERT INTO sped_c100
                   (cod_empresa
                   ,num_nota_fiscal
                   ,cod_serie_nota
                   ,num_cnpj_9
                   ,num_cnpj_4
                   ,num_cnpj_2
                   ,tip_entrada_saida
                   ,tip_emissao_prop
                   ,cod_modelo
                   ,cod_situacao_nota
                   ,dat_saida
                   ,dat_emissao
                   ,val_nota
                   ,tip_pagto
                   ,val_descon
                   ,val_itens
                   ,tip_frete
                   ,val_frete
                   ,val_seguro
                   ,val_despesas
                   ,val_base_icms
                   ,val_icms
                   ,val_base_icms_sub
                   ,val_icms_sub
                   ,val_ipi
                   ,val_pis
                   ,val_cofins
                   ,val_pis_sub
                   ,val_cofins_sub
                   ,val_iss
                   ,num_doc_referen
                   ,cod_serie_doc_referen
                   ,num_incri_substi
                   ,tip_contrib_fim
                   ,tip_nota_cancel
                   ,sig_estado
                   ,cod_natur_oper
                   ,cod_cidade_ibge
                   ,dat_vencto_fatura
                   ,tip_transp
                   ,num_cnpj_9_transp
                   ,num_cnpj_4_transp
                   ,num_cnpj_2_transp
                   ,cod_especie_volume
                   ,pes_bruto
                   ,pes_liquido
                   ,num_placa_veiculo
                   ,tip_contrib_icms
                   ,num_nota_eletric
                   ,cod_observ
                   ,num_conta_contabil
                   ,num_declaracao_exp
                   ,dat_exp
                   ,tip_exp
                   ,dat_registro_exp
                   ,num_conhecimento_emb
                   ,dat_conhecimento_emb
                   ,tip_inform_conhecimento
                   ,cod_pais_siscomex
                   ,dat_averbacao
                   ,seq_end_entr
                   ,qtde_volume
                   ,especie_docto
                   ,val_pis_import
                   ,val_cofins_import
                   ,num_seq_modelo
                   ,val_base_icms_n_tribut
                   ,num_protocolo
                   ,tp_nf
                   ,flag_exp
                   ,tipo_nf
                   ,val_fcp_uf_dest_nf
                   ,val_icms_uf_dest_nf
                   ,val_icms_uf_remet_nf
                   ,TIPO_CTE
                   ,IND_NF_CONSUMO
                   ,cidade_origem_transp
                   ,cidade_destino_transp
                   ,seq_end_entrega
                   ) VALUES
                   (p_cod_empresa
                   ,obrf_010.documento
                   ,trim(obrf_010.serie)
                   ,obrf_010.cgc_cli_for_9
                   ,obrf_010.cgc_cli_for_4
                   ,obrf_010.cgc_cli_for_2
                   ,'E'
                   ,decode(obrf_010.situacao_entrada,4,1,0) -- tip_emissao_prop
                   ,lpad(w_modelo_nota,2,'0')
                   ,w_situacao_entrada
                   ,obrf_010.data_transacao
                   ,obrf_010.data_emissao
                   ,obrf_010.total_docto      -- valor_nota
                   ,w_aprazo_avista           -- avista ou aprazo
                   ,obrf_010.valor_desconto
                   ,obrf_010.valor_itens
                   ,w_tipo_frete
                   ,obrf_010.valor_frete
                   ,obrf_010.valor_seguro
                   ,obrf_010.valor_despesas
                   ,obrf_010.base_icms
                   ,obrf_010.valor_icms
                   ,obrf_010.base_icms_sub
                   ,obrf_010.valor_icms_sub
                   ,obrf_010.valor_total_ipi
                   ,NULL          -- val_pis
                   ,NULL          -- val_cofins
                   ,0.00          -- val_pis_sub
                   ,000          -- val_cofins_sub
                   ,0.00
                   ,0          -- num_doc_referen
                   ,null          -- cod_serie_doc_referen
                   ,null          -- num_incri_substi
                   ,obrf_010.consumidor_final          -- tip_contrib_fim
                   ,w_tip_nota_cancel
                   ,obrf_010.natoper_est_oper
                   ,obrf_010.natoper_nat_oper
                   ,lpad(w_codigo_fiscal_uf,2,0) || lpad(w_codigo_cidade_ibge,5,0)
                   ,w_data_vencto_duplic
                   ,obrf_010.via_transporte
                   ,obrf_010.transpa_forne9
                   ,obrf_010.transpa_forne4
                   ,obrf_010.transpa_forne2
                   ,obrf_010.especie_volumes
                   ,obrf_010.peso_bruto
                   ,obrf_010.peso_liquido
                   ,null
                   ,obrf_010.consumidor_final
                   ,trim(obrf_010.numero_danf_nfe)
                   ,NULL                                        -- cod_observ
                   ,NULL                                        --num_conta_contabil
                   ,decode(w_numero_di, null, to_char(obrf_010.numero_di), w_numero_di)-- num_declaracao_exp
                   ,NULL                                         -- dat_exp
                   ,obrf_010.tipo_doc_importacao_sped           -- tip_exp
                   ,NULL                                        -- dat_registro_exp
                   ,decode(w_numero_di, null, to_char(obrf_010.numero_di), w_numero_di) -- num_conhecimento_emb
                   ,NULL                                        -- dat_conhecimento_emb
                   ,NULL                                        -- tip_inform_conhecimento
                   ,NULL                                        -- cod_pais_siscomex
                   ,NULL                                        -- dat_averbacao
                   ,0
                   ,obrf_010.qtde_volumes
                   ,obrf_010.especie_docto
                   ,obrf_010.valor_pis_import_sped
                   ,obrf_010.valor_cofins_import_sped
                   ,w_num_seq_modelo
                   ,obrf_010.base_diferenca
                   ,obrf_010.nr_protocolo
                   ,w_tp_nf
                   ,w_flag_exp
                   ,obrf_010.tipo_nf
                   ,obrf_010.val_fcp_uf_dest_nf
                   ,obrf_010.val_icms_uf_dest_nf
                   ,obrf_010.val_icms_uf_remet_nf
                   ,obrf_010.tipo_cte
                   ,w_ind_nf_consumo
                   ,w_cidade_origemNF
                   ,w_cidade_destinoNF
                   ,0
                   );
             EXCEPTION
                WHEN OTHERS THEN
                   p_des_erro := 'Erro na inclus?o da tabela sped_c100 - Entradas ' || Chr(10) || SQLERRM;
                   RAISE W_ERRO;
             END;
             COMMIT;
             --
             -- Atualiza os itens da nota fiscal - Entradas
             --

          v_seq_end_entr := 0;
          v_conta_item := 0;

             FOR obrf_015 IN u_obrf_015 (obrf_010.local_entrega, obrf_010.documento, obrf_010.serie, obrf_010.cgc_cli_for_9,
                                         obrf_010.cgc_cli_for_4, obrf_010.cgc_cli_for_2)
             LOOP

             -- define se insere o endereco da nota no seq_end_entrega ou 0
          begin
          select  NVL(estq_005.tipo_transacao,'N')
          into    v_eh_devolucao
          from    estq_005
          where    estq_005.codigo_transacao   =   obrf_015.codigo_transacao; -- y DEVOLUyyO
          exception
                when no_data_found then
                v_eh_devolucao := 'N';
          end;

          if  ( v_parametro_endereco      =   1  -- CONSIDERA PARyMETRO
            and v_conta_item               =   0  -- CONTADOR
            and v_eh_devolucao             =   'D' -- y DEVOLUyyO
            and obrf_015.num_nota_orig     >   0
            and obrf_010.cgc_cli_for_4    =   0
            and (obrf_010.cgc_cli_for_9    >   0
             or obrf_010.cgc_cli_for_2    >   0) ) -- y CPF
          then
          v_conta_item := 1; -- ENTRA Sy NO PRIMEIRO ITEM

        begin
        select   NVL(f050.seq_end_entr,0)
        into     v_seq_end_entr
        from     fatu_050 f050
        where    f050.codigo_empresa   =   obrf_010.local_entrega
        and      f050.num_nota_fiscal   =   obrf_015.num_nota_orig
        and      f050.serie_nota_fisc   =   obrf_015.serie_nota_orig;
        exception
            when no_data_found then
            v_seq_end_entr := 0;
        end;

          end if;

                if obrf_015.tipo_valores_fiscal <> '0'
                then

                   obrf_015.percentual_icm             := 0.00;
                   obrf_015.perc_dif_aliq              := 0.00;
                   obrf_015.perc_subtituicao           := 0.00;
                   obrf_015.valor_total                := 0.00;
                   obrf_015.base_calc_icm              := 0.00;
                   obrf_015.valor_icms                 := 0.00;
                   obrf_015.base_diferenca             := 0.00;
                   obrf_015.valor_icms_diferido        := 0.00;
                   obrf_015.rateio_despesas            := 0.00;
                   obrf_015.base_subtituicao           := 0.00;
                   obrf_015.valor_subtituicao          := 0.00;

                   if obrf_015.tipo_valores_fiscal = '1'
                   then

                      obrf_015.percentual_ipi          := 0.00;
                      obrf_015.rateio_despesas_ipi     := 0.00;
                      obrf_015.base_ipi                := 0.00;
                      obrf_015.valor_ipi               := 0.00;

                   end if;

                end if;

                begin
                   select pedi_080.cod_natureza,     pedi_080.divisao_natur,
                          pedi_080.consiste_cvf_icms,pedi_080.cod_mensagem
                   into   w_num_cfop,                w_divisao_natureza,
                          w_consiste_cvf_icms,       w_cod_mensagem
                   from pedi_080
                   where pedi_080.natur_operacao = obrf_015.natitem_nat_oper
                     and pedi_080.estado_natoper = obrf_015.natitem_est_oper;
                exception
                when no_data_found then
                   w_num_cfop          := '';
                   w_divisao_natureza  := 0;
                   w_consiste_cvf_icms := 1;
                end;

                if obrf_015.cod_vlfiscal_icm >= 30 and obrf_015.cod_vlfiscal_icm <> 70 and w_consiste_cvf_icms <> 3
                then
                   obrf_015.valor_icms := 0.00;
                end if;



                if obrf_010.valor_desconto > 0
                then
                   if obrf_010.tipo_desconto = 0
                   then
                      w_val_desconto := obrf_015.rateio_descontos_ipi;
                   else
                      w_val_desconto := obrf_015.desconto_item;
                   end if;
                else
                   w_val_desconto := 0.00;
                end if;

                if obrf_015.valor_icms = 0.00
                then
                   obrf_015.base_calc_icm   := 0.00;
                   obrf_015.percentual_icm  := 0.00;
                   --obrf_015.base_diferenca  := 0.00;
                end if;

                if obrf_015.valor_pis = 0.00 
                then
                   obrf_015.base_pis_cofins   := 0.00;
                   obrf_015.perc_pis          := 0.00;
                   obrf_015.perc_cofins       := 0.00;
                end if;

                w_valor_contabil := obrf_015.valor_total + obrf_015.rateio_despesas + obrf_015.valor_ipi + obrf_015.valor_subtituicao;
                
                BEGIN
                    SELECT val_str 
                    INTO v_ind_soma_ipi
                    FROM empr_008
                    WHERE empr_008.codigo_empresa = p_cod_empresa
                      AND empr_008.param = 'fiscal.indica_soma_ipi';
                EXCEPTION
                    -- Caso n�o encontre o valor, atribui valor default 'N'
                    WHEN NO_DATA_FOUND THEN
                        v_ind_soma_ipi := 'N';
                END;

                
                -- QUANDO FOR USO E CONSUMO, DEVE SOMAR O VALOR DE IPI NA MERCADORIA
                if v_ind_soma_ipi = 'S' AND obrf_015.cvf_ipi_entrada = 49
                then
                   v_val_faturado := obrf_015.valor_total + obrf_015.valor_ipi;
                else
                   v_val_faturado := obrf_015.valor_total;
                end if;
                

                if obrf_015.cod_vlfiscal_ipi > 1
                then
                   obrf_015.base_ipi        := 0.00;
                   obrf_015.valor_ipi       := 0.00;
                   obrf_015.percentual_ipi  := 0.00;
                end if;

                w_sequencia := w_sequencia + 1; -- cria uma sequencia automaticamente

            --BALENA
                if (obrf_015.estagio_agrupador + obrf_015.estagio_agrupador_simultaneo) > 0
                then
                  obrf_015.classific_fiscal := inter_fn_get_classific(obrf_015.coditem_nivel99, obrf_015.coditem_grupo, obrf_015.coditem_subgrupo,
                                            obrf_015.coditem_item, obrf_015.estagio_agrupador, obrf_015.estagio_agrupador_simultaneo);
                end if;

                begin
                   select trim(basi_240.cod_enquadramento_ipi)
                   into   w_cod_enquadramento_ipi
                   from basi_240
                   where basi_240.classific_fiscal = obrf_015.classific_fiscal;
                exception
                when no_data_found then
                   w_cod_enquadramento_ipi := null;
                end;

                w_cod_enquadramento_ipi := null;

                --Verifica se o item possui movimentac?o fisica
                begin
                   select pedi_080.tem_mov_fisica into w_tipo_transacao
                   from pedi_080
                   where pedi_080.natur_operacao = obrf_015.natitem_nat_oper
                     and pedi_080.estado_natoper = obrf_015.natitem_est_oper;
                exception
                when no_data_found then
                   w_tipo_transacao := 0;
                end;

                if w_tem_referenciado = 'n'
                then
                   begin
                      select count(*) into w_conta_msg from fatu_052
                      where fatu_052.cod_empresa    = obrf_010.local_entrega
                        and fatu_052.num_nota       = obrf_010.documento
                        and fatu_052.cod_serie_nota = obrf_010.serie
                        and fatu_052.cnpj9          = obrf_010.cgc_cli_for_9
                        and fatu_052.cnpj4          = obrf_010.cgc_cli_for_4
                        and fatu_052.cnpj2          = obrf_010.cgc_cli_for_2
                        and fatu_052.ind_entr_saida = 'E';
                   end;

                   if obrf_015.num_nota_orig > 0 and w_conta_msg = 0
                   then
                      w_tem_referenciado := 's';

                      inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                     'Procedure: p_gera_sped_C100 ' || Chr(10) ||
                                     'Esta e uma nota de devoluc?o e n?o existe codigo de mensagem relacionado. Verifique ' || Chr(10) ||
                                     'Sem codigo de mensagem relacionado n?o ira gerar o registor C113, obrigatorio neste caso.' || Chr(10) ||
                                     '   Nota fiscal: ' || to_char(obrf_010.documento,'000000') || ' - ' ||
                                                                   obrf_010.serie || ' ' ||
                                                           to_char(obrf_010.cgc_cli_for_9, '000000000') || '/' ||
                                                           to_char(obrf_010.cgc_cli_for_4, '0000') || '-' ||
                                                           to_char(obrf_010.cgc_cli_for_2, '00'));
                   end if;
                end if;

                obrf_015.classific_fiscal := substr(trim(REPLACE(obrf_015.classific_fiscal,'.')),1,8);

                -- verifica se o tamanho da classificacao fiscal esta correta, caso nao esteja, gera mensagem de erro
                IF  Nvl(Length(REPLACE(obrf_015.classific_fiscal,'.')),0) > 8
                THEN
                   inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                 'Procedure: p_gera_sped_C100 ' || Chr(10) ||
                                 'Tamanho do campo classificac?o fiscal e maior que o permitido ' || Chr(10) ||
                                 '   Nota fiscal: ' || to_char(obrf_010.local_entrega,'000') || '-' ||
                                                       to_char(obrf_010.documento,'000000000')  || '-' ||
                                                       obrf_010.serie || '-' ||
                                 '   Classif. Fiscal ' || obrf_015.classific_fiscal);
                    obrf_015.classific_fiscal := substr(trim(REPLACE(obrf_015.classific_fiscal,'.')),1,8);
                ELSE
                   obrf_015.classific_fiscal := substr(trim(REPLACE(obrf_015.classific_fiscal,'.')),1,8);

                   IF  obrf_015.classific_fiscal = '0'
                   THEN
                      inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                 'Procedure: p_gera_sped_C100 ' || Chr(10) ||
                                 'O campo classificac?o fiscal esta com ZERO ' || Chr(10) ||
                                 '   Nota fiscal: ' || to_char(obrf_010.local_entrega,'000') || '-' ||
                                                       to_char(obrf_010.documento,'000000000')  || '-' ||
                                                       obrf_010.serie || '-' ||
                                 '   Classif. Fiscal ' || obrf_015.classific_fiscal);

                   END IF;
                END IF;



                if obrf_015.coditem_nivel99 = '0' and
                   obrf_015.coditem_grupo = '00000'
                then

                   if substr(obrf_015.classific_fiscal,1,5) is not null
                   then
                      ws_grupo_clas := substr(obrf_015.classific_fiscal,1,5);
                   else
                      ws_grupo_clas := ' ';
                   end if;


                   if substr(obrf_015.classific_fiscal,6,3) is not null
                   then
                      ws_subgru_clas := substr(obrf_015.classific_fiscal,6,3);
                   else
                      ws_subgru_clas := ' ';
                   end if;

                   obrf_015.coditem_grupo    := ws_grupo_clas;
                   obrf_015.coditem_subgrupo := ws_subgru_clas;
                   obrf_015.coditem_nivel99  := ' ';
                   obrf_015.coditem_item     := ' ';
                end if;

                if obrf_015.cod_vlfiscal_icm = 20 or obrf_015.cod_vlfiscal_icm = 70
                then
                   if w_valor_contabil - obrf_015.base_calc_icm < 0.00
                   then
                      w_base_dif := 0.00;
                   else
                      w_base_dif := w_valor_contabil - obrf_015.base_calc_icm;
                   end if;
                else
                   w_base_dif := 0.00;
                end if;

                BEGIN
                     INSERT INTO sped_c170
                         (cod_empresa
                         ,num_nota_fiscal
                         ,cod_serie_nota
                         ,seq_item_nota
                         ,num_cnpj_9
                         ,num_cnpj_4
                         ,num_cnpj_2
                         ,tip_entrada_saida
                         ,cod_nivel
                         ,cod_grupo
                         ,cod_subgru
                         ,cod_item
                         ,des_item
                         ,cod_unid_medida
                         ,val_unitario
                         ,val_faturado
                         ,val_contabil
                         ,qtde_item_fatur
                         ,cod_c_custo
                         ,tip_transacao
                         ,val_desconto
                         ,cvf_icms
                         ,cod_nat_oper
                         ,sig_estado_oper
                         ,num_cfop
                         ,val_base_icms
                         ,perc_icms
                         ,val_icms
                         ,val_base_icms_sub_trib
                         ,perc_icms_sub_trib
                         ,val_base_red_sub_trib
                         ,val_icms_sub_trib
                         ,tip_apuracao
                         ,cod_enquadramento_ipi
                         ,val_base_ipi
                         ,perc_ipi
                         ,val_ipi
                         ,cvf_ipi
                         ,val_basi_pis_cofins
                         ,val_pis
                         ,perc_pis
                         ,cvf_pis
                         ,cvf_cofins
                         ,perc_cofins
                         ,val_cofins
                         ,perc_iss
                         ,dat_emissao
                         ,tip_cli_forn
                         ,cod_classific_fiscal
                         ,pes_liquido
                         ,val_frete
                         ,val_seguro
                         ,val_outros
                         ,val_icms_difer
                         ,tip_trib_federal
                         ,tip_trib_estadual
                         ,tip_trib_icms
                         ,num_lote
                         ,tip_apuracao_ipi
                         ,val_iss
                         ,num_conta_contabil
                         ,cod_empresa_refer
                         ,num_nota_refer
                         ,cod_serie_refer
                         ,seq_nota_refer
                         ,num_cnpj9_refer
                         ,num_cnpj4_refer
                         ,num_cnpj2_refer
                         ,cod_deposito
                         ,perc_diferencial
                         ,val_rateio
                         ,tipo_transacao
                         ,cod_mensagem_nat_oper
                         ,cod_transacao
                         ,cod_contabil
                         ,despesa_nf_item
                         ,cod_mensagem_fiscal
                         ,val_fcp_uf_dest
                         ,val_icms_uf_dest
                         ,val_icms_uf_remet
                         ,qtde_conver
                         ,unid_med_conver
                         ,estagio_agrupador
                         ,estagio_agrupador_simultaneo
                         ,seq_operacao_agrupador_insu
                         ) VALUES
                         (p_cod_empresa                      -- cod_empresa
                         ,obrf_010.documento                          -- num_nota_fiscal
                         ,trim(obrf_010.serie)                        -- cod_serie_nota
                         ,w_sequencia                                 -- seq_item_nota
                         ,obrf_010.cgc_cli_for_9                      --num_cnpj_9
                         ,obrf_010.cgc_cli_for_4                      --num_cnpj_4
                         ,obrf_010.cgc_cli_for_2                      --num_cnpj_2
                         ,'E'                                         --tip_entrada_saida
                         ,obrf_015.coditem_nivel99                    --cod_nivel
                         ,obrf_015.coditem_grupo                      --cod_grupo
                         ,obrf_015.coditem_subgrupo                   --cod_subgru
                         ,obrf_015.coditem_item                       --cod_item
                         ,trim(replace(obrf_015.descricao_item, chr(9), ' '))  || ' ' || inter_fn_descr_blk(obrf_015.estagio_agrupador,obrf_015.estagio_agrupador_simultaneo)  --des_item
                         ,obrf_015.unidade_medida                     --cod_unid_medida
                         ,decode(obrf_015.quantidade,0.00,0.00,obrf_015.valor_total/obrf_015.quantidade)   --val_unitario
                         ,v_val_faturado                        --val_faturado
                         ,w_valor_contabil                            --val_contabil
                         ,decode(obrf_015.quantidade,0.000,1.000,obrf_015.quantidade)  --qtde_item_fatur
                         ,obrf_015.centro_custo                       --cod_c_custo
                         ,w_tipo_transacao                            --tip_transacao
                         ,w_val_desconto                              --val_desconto
                         ,obrf_015.cod_vlfiscal_icm                   --cvf_icms
                         ,obrf_015.natitem_nat_oper                   --cod_nat_oper
                         ,obrf_015.natitem_est_oper                   --sig_estado_oper
                         ,REPLACE(w_num_cfop,'.') || w_divisao_natureza
                         ,obrf_015.base_calc_icm                      --val_base_icms
                         ,obrf_015.percentual_icm                     --perc_icms
                         ,obrf_015.valor_icms                         --val_icms
                         ,decode(obrf_015.valor_subtituicao,0.00,0.00,decode(obrf_015.cod_vlfiscal_icm,60,0.00,obrf_015.base_subtituicao))   --val_base_icms_sub_trib
                         ,obrf_015.perc_subtituicao                   --perc_icms_sub_trib   cra
                         ,w_base_dif                                  --val_base_red_sub_trib
                         ,decode(obrf_015.cod_vlfiscal_icm,60,0.00,obrf_015.valor_subtituicao)                  --val_icms_sub_trib  cra
                         ,NULL                                        --tip_apuracao
                         ,w_cod_enquadramento_ipi                     --cod_enquadramento_ipi
                         ,obrf_015.base_ipi                           --val_base_ipi
                         ,obrf_015.percentual_ipi                     --perc_ipi
                         ,obrf_015.valor_ipi                          --val_ipi
                         ,obrf_015.cvf_ipi_entrada                    --cvf_ipi
                         ,obrf_015.base_pis_cofins                    --val_basi_pis_cofins
                         ,obrf_015.valor_pis                          --val_pis
                         ,obrf_015.perc_pis                           --perc_pis
                         ,obrf_015.cvf_pis                            --cvf_pis
                         ,obrf_015.cvf_cofins                         --cvf_cofins
                         ,obrf_015.perc_cofins                        --perc_cofins
                         ,obrf_015.valor_cofins                       --val_cofins
                         ,0.00                                        --perc_iss
                         ,obrf_010.data_transacao                     --dat_emissao
                         ,NULL                                        --tip_cli_forn
                         ,REPLACE(obrf_015.classific_fiscal,'.')     --cod_classific_fiscal
                         ,0.00                                        --pes_liquido
                         ,0.00                                        --val_frete
                         ,0.00                                        --val_seguro
                         ,0.00                                        --val_outros
                         ,obrf_015.valor_icms_diferido                --val_icms_difer
                         ,0                                           --tip_trib_federal
                         ,obrf_015.procedencia                        --tip_trib_estadual
                         ,obrf_015.cod_vlfiscal_icm                   --tip_trib_icms
                         ,obrf_015.lote_entrega                       --num_lote
                         ,1                                           --tip_apuracao_ipi
                         ,0.00                                        --val_iss
                         ,'0'                                           --num_conta_contabil
                         ,obrf_010.local_entrega
                         ,obrf_015.num_nota_orig
                         ,trim(obrf_015.serie_nota_orig)
                         ,null
                         ,decode(obrf_015.num_nota_orig,0,0,obrf_015.capa_ent_forcli9)
                         ,decode(obrf_015.num_nota_orig,0,0,obrf_015.capa_ent_forcli4)
                         ,decode(obrf_015.num_nota_orig,0,0,obrf_015.capa_ent_forcli2)
                         ,obrf_015.codigo_deposito
                         ,obrf_015.perc_dif_aliq
                         ,obrf_015.rateio_despesas
                         ,w_tipo_transacao
                         ,null
                         ,obrf_015.codigo_transacao
                         ,obrf_015.codigo_contabil
                         ,obrf_015.rateio_despesas_ipi
                         ,w_cod_mensagem
                         ,obrf_015.val_fcp_uf_dest
                         ,obrf_015.val_icms_uf_dest
                         ,obrf_015.val_icms_uf_remet
                         ,obrf_015.quantidade_conv -- qtde_conver
                         ,obrf_015.unidade_conv -- unid_med_conver
                         ,obrf_015.estagio_agrupador
                         ,obrf_015.estagio_agrupador_simultaneo
                         ,obrf_015.seq_operacao_agrupador_insu
                        );
                EXCEPTION
                    WHEN OTHERS THEN
                      p_des_erro := 'Erro na inclus?o da tabela sped_c170 - Entradas ' || Chr(10) || SQLERRM;
                      RAISE W_ERRO;
                END;
             END LOOP;


             /* Conforme manual do SPED:
              * Os campos 22 (VL_ICMS) e 24 (VL_ICMS_ST) do registro C100 somente ser?o preenchidos se
              * a empresa declarante tiver direito ao credito ou o dever do debito sobre a operac?o.
              * O contribuinte substituido, nas operac?es em que o ICMS tenha sido retido por substituic?o
              * tributaria, deixara estes campos em branco na EFD.
              *
              * Ou seja, Se a CST de ICMS for 60, o ICMS sera considerado no total da nota, mas n?o pode ser destacado no campo referente a ST de ICMS*/
             begin
                select sum(sped_c170.val_pis),      sum(sped_c170.val_cofins),
                       sum(sped_c170.val_base_ipi), sum(sped_c170.val_ipi),
                       sum(sped_c170.val_base_icms),sum(sped_c170.val_icms),
                       sum(decode(val_icms_sub_trib,0.00,0.00,decode(sped_c170.cvf_icms,60,0.00,val_base_icms_sub_trib))),
                       sum(decode(sped_c170.cvf_icms,60,0.00,sped_c170.val_icms_sub_trib)),
                       sum(val_desconto),
                       sum(sped_c170.val_fcp_uf_dest),
                       sum(sped_c170.val_icms_uf_dest),
                       sum(sped_c170.val_icms_uf_remet),
                       sum(sped_c170.val_faturado)
                into   w_val_total_pis,             w_val_total_cofins,
                       w_val_base_ipi,              w_val_ipi,
                       w_val_base_icms,             w_val_icms,
                       w_val_base_icms_sub_trib,
                       w_val_icms_sub_trib,
                       w_val_desconto_f,
                       w_val_fcp_uf_dest,
                       w_val_icms_uf_dest,
                       w_val_icms_uf_remet,
                       w_val_faturado

                from sped_c170
                where sped_c170.cod_empresa     = p_cod_empresa
                  and sped_c170.num_nota_fiscal = obrf_010.documento
                  and trim(sped_c170.cod_serie_nota)  = trim(obrf_010.serie)
                  and sped_c170.num_cnpj_9      = obrf_010.cgc_cli_for_9
                  and sped_c170.num_cnpj_4      = obrf_010.cgc_cli_for_4
                  and sped_c170.num_cnpj_2      = obrf_010.cgc_cli_for_2
                  and sped_c170.tip_entrada_saida = 'E';
             exception
             when no_data_found then
                  w_val_total_pis    := 0.00;
                  w_val_total_cofins := 0.00;
                  w_val_base_ipi     := 0.00;
                  w_val_ipi          := 0.00;
                  w_val_base_icms    := 0.00;
                  w_val_icms         := 0.00;
                  w_val_base_icms_sub_trib := 0.00;
                  w_val_icms_sub_trib      := 0.00;
                  w_val_desconto_f         := 0.00;
                  w_val_fcp_uf_dest        := 0.00;
                  w_val_icms_uf_dest       := 0.00;
                  w_val_icms_uf_remet      := 0.00;
                  w_val_faturado           := 0.00;
             end;

             begin
                update sped_c100
                  set sped_c100.val_pis               = w_val_total_pis,
                      sped_c100.val_cofins            = w_val_total_cofins,
                      sped_c100.val_base_ipi          = w_val_base_ipi,
                      sped_c100.val_ipi               = w_val_ipi,
                      sped_c100.val_base_icms         = w_val_base_icms,
                      sped_c100.val_icms              = w_val_icms,
                      sped_c100.val_base_icms_sub     = w_val_base_icms_sub_trib,
                      sped_c100.val_icms_sub          = w_val_icms_sub_trib,
                      sped_c100.val_fcp_uf_dest_nf    = w_val_fcp_uf_dest,
                      sped_c100.val_icms_uf_dest_nf   = w_val_icms_uf_dest,
                      sped_c100.val_icms_uf_remet_nf  = w_val_icms_uf_remet,
                      sped_c100.seq_end_entrega       = v_seq_end_entr,
                      sped_c100.val_itens             = w_val_faturado

                  where sped_c100.cod_empresa     = p_cod_empresa
                    and sped_c100.num_nota_fiscal = obrf_010.documento
                    and trim(sped_c100.cod_serie_nota)  = trim(obrf_010.serie)
                    and sped_c100.num_cnpj_9      = obrf_010.cgc_cli_for_9
                    and sped_c100.num_cnpj_4      = obrf_010.cgc_cli_for_4
                    and sped_c100.num_cnpj_2      = obrf_010.cgc_cli_for_2
                    and sped_c100.tip_entrada_saida = 'E';
              EXCEPTION
              WHEN OTHERS THEN
                   p_des_erro := 'Erro na atualizac?o da tabela sped_c100 - Entradas ' || Chr(10) || SQLERRM;
                   RAISE W_ERRO;
              END;

              if w_val_desconto_f <> obrf_010.valor_desconto
              then
                 BEGIN
                    update sped_c170
                    set val_desconto = val_desconto + (obrf_010.valor_desconto - w_val_desconto_f)
                    where sped_c170.cod_empresa     = p_cod_empresa
                      and sped_c170.num_nota_fiscal = obrf_010.documento
                      and trim(sped_c170.cod_serie_nota)  = trim(obrf_010.serie)
                      and sped_c170.num_cnpj_9      = obrf_010.cgc_cli_for_9
                      and sped_c170.num_cnpj_4      = obrf_010.cgc_cli_for_4
                      and sped_c170.num_cnpj_2      = obrf_010.cgc_cli_for_2
                      and sped_c170.seq_item_nota   = 1;
                 END;
              end if;

             COMMIT;
           end if; /* FIM NOTA DENEGADA*/
       END IF; /* FIM DO IF TEM ITENS NO LIVRO */

   END LOOP;

   COMMIT;
EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_c100 ' || Chr(10) || p_des_erro;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure p_gera_sped_c100 ' || Chr(10) || SQLERRM;
END inter_pr_gera_sped_c100;
/
