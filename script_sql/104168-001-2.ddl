alter table estq_040
add (qtde_estoque_wms_imagem  number(13,3));

comment on column estq_040.qtde_estoque_wms_imagem
is 'Este campo serve para guardar a quantidade do estoque congelado do WMS para executar a sugest�o de faturamento.';

/

exec inter_pr_recompile;
