
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_141" 
   before insert on tmrp_141
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   qtdeRequi                 number(10,3);
    pesoRolo                 pcpt_020.qtde_quilos_acab%type;

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- Inserindo o codigo do usuario que requisitou o rolo.
   if inserting
   then

      if (:new.tipo_reserva = 4 and
         :new.area_producao_reserva = 1)
      then

         begin
            select pcpt_020.qtde_quilos_acab
            into pesoRolo
            from pcpt_020
            where pcpt_020.codigo_rolo = :new.codigo_rolo;
         exception
         when no_data_found then
             pesoRolo:= 0;
         end;


         begin
            select sum(tmrp_141.qtde_reservada)
            into qtdeRequi
            from tmrp_141
            where tmrp_141.codigo_rolo           = :new.codigo_rolo
              and tmrp_141.tipo_reserva          = 4
              and tmrp_141.area_producao_reserva = 1;
         exception
         when no_data_found then
            qtdeRequi:= 0;
         end;

         if ((pesoRolo-qtdeRequi) < :new.qtde_reservada)
         then
            raise_application_error(-20000, 'ATEN��O! Este rolo j� foi reservado, e n�o h� saldo para atender sua reserva. Favor consulte novamente os rolos disponiveis.');
         end if;


      end if;
      :new.usuario_reserva := ws_usuario_systextil;

   end if;
end inter_tr_tmrp_141;



-- ALTER TRIGGER "INTER_TR_TMRP_141" ENABLE
 

/

exec inter_pr_recompile;

