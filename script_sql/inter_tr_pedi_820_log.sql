
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_820_LOG" 
   after insert
   or delete
   or update of
	   pedido_venda, cara_codigo, vari_codigo, comp_nivel99,
	   comp_grupo, comp_subgrupo, comp_item, percentual,
	   valor, qtde_consumo, dupla_densidade
   on pedi_820
   for each row

declare
   ws_usuario_rede          varchar2(20);
   ws_maquina_rede          varchar2(40);
   ws_aplicativo            varchar2(20);

   long_aux                varchar2(2000);
begin

    if inserting
    then


       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;


       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01,             long01
          )
       VALUES
          ( 'PEDI_820',        'I',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :new.pedido_venda,
            '                              PEDIDO DE VENDAS - CARACTERISTICAS'||
                                     chr(10)                  ||
                                     chr(10)                  ||
            'PEDIDO VENDA.....: ' || :new.pedido_venda        ||
            chr(10)                                           ||
            'CARA CODIGO......: ' || :new.cara_codigo         ||
            chr(10)                                           ||
            'VARI CODIGO......: ' || :new.vari_codigo         ||
            chr(10)                                           ||
            'COMP NIVEL99.....: ' || :new.comp_nivel99        ||
            chr(10)                                           ||
            'COMP GRUPO.......: ' || :new.comp_grupo          ||
            chr(10)                                           ||
            'COMP SUBGRUPO....: ' || :new.comp_subgrupo       ||
            chr(10)                                           ||
            'COMP ITEM........: ' || :new.comp_item           ||
            chr(10)                                           ||
            'PERCENTUAL.......: ' || :new.percentual          ||
            chr(10)                                           ||
            'VALOR............: ' || :new.valor               ||
            chr(10)                                           ||
            'QTDE CONSUMO.....: ' || :new.qtde_consumo        ||
            chr(10)                                           ||
            'DUPLA DENSIDADE..: ' || :new.dupla_densidade
         );
    end if;

    if updating and
       (:old.pedido_venda        <> :new.pedido_venda      or
        :old.cara_codigo         <> :new.cara_codigo       or
        :old.vari_codigo         <> :new.vari_codigo       or
        :old.comp_nivel99        <> :new.comp_nivel99      or
        :old.comp_grupo          <> :new.comp_grupo        or
        :old.comp_subgrupo       <> :new.comp_subgrupo     or
        :old.comp_item           <> :new.comp_item         or
        :old.percentual          <> :new.percentual        or
        :old.valor               <> :new.valor             or
        :old.qtde_consumo        <> :new.qtde_consumo      or
        :old.dupla_densidade     <> :new.dupla_densidade
      )
    then
       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;

       long_aux := long_aux ||
                  '                              PEDIDO DE VENDAS - CARACTERISTICAS'||
                   chr(10)                    ||
                   chr(10);

       if  :old.pedido_venda <> :new.pedido_venda
       then
           long_aux := long_aux ||
                'PEDIDO VENDA....: ' || :old.pedido_venda    || ' - '
                                     || :new.pedido_venda
                                     || chr(10);
       end if;

       if  :old.cara_codigo  <> :new.cara_codigo
       then
           long_aux := long_aux ||
                'CARA CODIGO.....: ' || :old.cara_codigo     || ' - '
                                     || :new.cara_codigo
                                     || chr(10);
       end if;

       if  :old.vari_codigo  <> :new.vari_codigo
       then
           long_aux := long_aux ||
                'VARI CODIGO.....: ' || :old.vari_codigo     || ' - '
                                     || :new.vari_codigo
                                     || chr(10);
       end if;

       if  :old.comp_nivel99  <> :new.comp_nivel99
       then
           long_aux := long_aux ||
                'COMP NIVEL99....: ' || :old.comp_nivel99     || ' - '
                                     || :new.comp_nivel99
                                     || chr(10);
       end if;

       if  :old.comp_grupo  <> :new.comp_grupo
       then
           long_aux := long_aux ||
                'COMP GRUPO......: ' || :old.comp_grupo     || ' - '
                                     || :new.comp_grupo
                                     || chr(10);
       end if;

       if  :old.comp_subgrupo  <> :new.comp_subgrupo
       then
           long_aux := long_aux ||
                'COMP SUBGRUPO...: ' || :old.comp_subgrupo     || ' - '
                                     || :new.comp_subgrupo
                                     || chr(10);
       end if;

       if  :old.comp_item  <> :new.comp_item
       then
           long_aux := long_aux ||
                'COMP ITEM.......: ' || :old.comp_item     || ' - '
                                     || :new.comp_item
                                     || chr(10);
       end if;

       if  :old.percentual  <> :new.percentual
       then
           long_aux := long_aux ||
                'PERCENTUAL......: ' || :old.percentual     || ' - '
                                     || :new.percentual
                                     || chr(10);
       end if;

       if  :old.valor  <> :new.valor
       then
           long_aux := long_aux ||
                'VALOR...........: ' || :old.valor     || ' - '
                                     || :new.valor
                                     || chr(10);
       end if;

       if  :old.qtde_consumo  <> :new.qtde_consumo
       then
           long_aux := long_aux ||
                'QTDE CONSUMO....: ' || :old.qtde_consumo     || ' - '
                                     || :new.qtde_consumo
                                     || chr(10);
       end if;

       if  :old.dupla_densidade  <> :new.dupla_densidade
       then
           long_aux := long_aux ||
                'DUPLA DENSIDADE.: ' || :old.dupla_densidade     || ' - '
                                     || :new.dupla_densidade
                                     || chr(10);
       end if;


       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01,             long01
          )
       VALUES
          ( 'PEDI_820',        'A',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :new.pedido_venda, long_aux
         );
    end if;

    if deleting
    then


       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;


       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01
          )
       VALUES
          ( 'PEDI_820',        'D',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :old.pedido_venda
         );
    end if;

end inter_tr_pedi_820_log;

-- ALTER TRIGGER "INTER_TR_PEDI_820_LOG" ENABLE
 

/

exec inter_pr_recompile;

