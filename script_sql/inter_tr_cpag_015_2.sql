create or replace trigger inter_tr_cpag_015_2
   after insert or delete or update on cpag_015

   -- Author  : Neylor Zimmermann de Carvalho
   -- Created : 24/10/05

begin
   -- se foi atualizado os campos data_pagamento,
   -- atualiza o titulo com a maior data
   if inter_pc_cpag_variaveis.cgc_9 is not null
   then
      -- chama a procedure para atualizar a duplicata com a data do ultimo
      -- pagamento e do ultimo credito
      inter_pr_saldo_cpag_010_tr(inter_pc_cpag_variaveis.cgc_9,
                                 inter_pc_cpag_variaveis.cgc_4,
                                 inter_pc_cpag_variaveis.cgc_2,
                                 inter_pc_cpag_variaveis.tipo_titulo,
                                 inter_pc_cpag_variaveis.nr_duplicata,
                                 inter_pc_cpag_variaveis.parcela);

      -- seta null para as variaveis compartilhada, isto � pera o controle se
      -- a procedure deve ou nao ser executada (apenas quando se altera a
      -- data_pagamento ou de credito
      inter_pc_cpag_variaveis.cgc_9        := null;
      inter_pc_cpag_variaveis.cgc_4        := null;
      inter_pc_cpag_variaveis.cgc_2        := null;
      inter_pc_cpag_variaveis.tipo_titulo  := null;
      inter_pc_cpag_variaveis.nr_duplicata := null;
      inter_pc_cpag_variaveis.parcela      := null;
   end if;
end inter_tr_cpag_015_2;

/
/* versao: 1 */
