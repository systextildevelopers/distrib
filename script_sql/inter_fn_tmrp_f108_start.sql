
  CREATE OR REPLACE FUNCTION "INTER_FN_TMRP_F108_START" (p_codigo_empresa number,       p_data_emissao_ini date,
                                                    p_data_emissao_fim date,       p_data_embarque_ini date,
                                                    p_data_embarque_fim date,      p_periodo_producao_ini number,
                                                    p_periodo_producao_fim number, p_data_req_ini date,
                                                    p_data_req_fim date,

                                                    p_codigo_usuario number,
                                                    p_num_aleatorio number,        p_nr_solicitacao number,
                                                    p_usuario varchar,             p_ie_pedido number,
                                                    p_ie_cliente number,           p_ie_produto number,
                                                    p_ie_familia number,           p_ie_artigo number)
return number is
   v_retorno                 number := 1;
   v_data_req_comercial      date := null;
    v_tmpInt                 number := 0;
     v_tmp                   number := 0;
begin
   for reg_itens in (select
                            0 ordem_planejamento,               pedi_100.data_entr_venda data_entrega,
                            pedi_100.pedido_venda,              pedi_110.cd_it_pe_nivel99 nivel,
                            pedi_110.cd_it_pe_grupo grupo,      pedi_110.cd_it_pe_subgrupo subgrupo,
                            pedi_110.cd_it_pe_item item,        basi_010.numero_alternati alternativa,
                            basi_010.numero_roteiro roteiro,
                            pedi_110.qtde_pedida - nvl(t630.qtde,0) saldo,
                            min(pcpc_010.periodo_producao) periodo_producao, pedi_100.classificacao_pedido
                     from pedi_100,pedi_110,basi_010,pcpc_010,basi_030,(select t630.nivel_produto,    t630.grupo_produto,
                                                                               t630.subgrupo_produto, t630.item_produto,
                                                                               t630.pedido_venda,
                                                                               nvl(sum(t630.programado_comprado),0) qtde
                                                                        from tmrp_630 t630
                                                                        where t630.area_producao = 1
                                                                        group by t630.nivel_produto,    t630.grupo_produto,
                                                                                 t630.subgrupo_produto, t630.item_produto,
                                                                                 t630.pedido_venda) t630
                  where pedi_100.pedido_venda       = pedi_110.pedido_venda
                  and   pedi_110.cd_it_pe_nivel99   = basi_010.nivel_estrutura
                  and   pedi_110.cd_it_pe_grupo     = basi_010.grupo_estrutura
                  and   pedi_110.cd_it_pe_subgrupo  = basi_010.subgru_estrutura
                  and   pedi_110.cd_it_pe_item      = basi_010.item_estrutura
                  and   pcpc_010.codigo_empresa     = pedi_100.codigo_empresa
                  and   pedi_110.cd_it_pe_nivel99   = basi_030.nivel_estrutura
                  and   pedi_110.cd_it_pe_grupo     = basi_030.referencia
                  and   pedi_110.pedido_venda       = t630.pedido_venda (+)
                  and   pedi_110.cd_it_pe_nivel99   = t630.nivel_produto (+)
                  and   pedi_110.cd_it_pe_grupo     = t630.grupo_produto (+)
                  and   pedi_110.cd_it_pe_subgrupo  = t630.subgrupo_produto (+)
                  and   pedi_110.cd_it_pe_item      = t630.item_produto (+)
                  and   pedi_100.data_entr_venda between pcpc_010.data_ini_periodo and pcpc_010.data_fim_periodo
                  and   pcpc_010.periodo_producao between p_periodo_producao_ini and p_periodo_producao_fim
                  and   pcpc_010.area_periodo       = 1
                  and   pedi_100.cod_cancelamento   = 0
                  and   pedi_110.cod_cancelamento   = 0
                  and   pedi_100.tecido_peca        = 1
                  and   pedi_100.tipo_pedido        = 0
                  and   pedi_100.situacao_venda    <> 5
                  and  (pedi_110.liquida_saldo_aprogramar is null or pedi_110.liquida_saldo_aprogramar = 0)
                  and   pedi_100.codigo_empresa     = p_codigo_empresa
                  and   pedi_110.qtde_pedida - nvl(t630.qtde,0) > 0
                  and   pedi_100.data_emis_venda between p_data_emissao_ini and p_data_emissao_fim
                  and   pedi_100.data_entr_venda between p_data_embarque_ini and p_data_embarque_fim
                  and   exists (select 1
                                from rcnb_060
                                where rcnb_060.tipo_registro     = 179
                                and   rcnb_060.nr_solicitacao    = 108
                                and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                and   rcnb_060.grupo_estrutura   = pedi_100.pedido_venda
                                and   p_ie_pedido   = 1
                                UNION
                                select 1
                                from rcnb_060
                                where rcnb_060.tipo_registro     = 179
                                and   rcnb_060.nr_solicitacao    = 108
                                and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                and   rcnb_060.grupo_estrutura   <> pedi_100.pedido_venda
                                and   p_ie_pedido   = 2)
                  and   exists (select 1
                                from rcnb_060
                                where rcnb_060.tipo_registro     = 595
                                and   rcnb_060.nr_solicitacao    = 108
                                and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                and   rcnb_060.item_estrutura    = basi_030.div_prod
                                and   p_ie_familia  = 1
                                UNION
                                select 1
                                 from rcnb_060
                                where rcnb_060.tipo_registro     = 595
                                and   rcnb_060.nr_solicitacao    = 108
                                and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                and   rcnb_060.item_estrutura    <> basi_030.div_prod
                                and   p_ie_familia  = 2)
                  and   exists (select 1
                                from rcnb_060
                                where rcnb_060.tipo_registro        = 548
                                and   rcnb_060.nr_solicitacao       = 108
                                and   rcnb_060.subgru_estrutura     = p_codigo_usuario
                                and   rcnb_060.tipo_registro_rel    = p_num_aleatorio
                                and   rcnb_060.nivel_estrutura_str  = pedi_110.cd_it_pe_nivel99 and rcnb_060.grupo_estrutura_str in (pedi_110.cd_it_pe_grupo,'XXXXXX') and rcnb_060.subgru_estrutura_str in (pedi_110.cd_it_pe_subgrupo,'XXXX') and rcnb_060.descricao in (pedi_110.cd_it_pe_item,'XXXXXXX')
                                and   p_ie_produto     = 1
                                UNION
                                select 1
                                from rcnb_060
                                where rcnb_060.tipo_registro        = 548
                                and   rcnb_060.nr_solicitacao       = 108
                                and   rcnb_060.subgru_estrutura     = p_codigo_usuario
                                and   rcnb_060.tipo_registro_rel    = p_num_aleatorio
                                and   rcnb_060.nivel_estrutura_str  = pedi_110.cd_it_pe_nivel99 and rcnb_060.grupo_estrutura_str <> pedi_110.cd_it_pe_grupo and rcnb_060.subgru_estrutura_str <> pedi_110.cd_it_pe_subgrupo and rcnb_060.descricao <> pedi_110.cd_it_pe_item
                                and   p_ie_produto     = 2)
                  and   exists (select 1
                                from rcnb_060
                                where rcnb_060.tipo_registro     = 778
                                and   rcnb_060.nr_solicitacao    = 108
                                and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                and   rcnb_060.grupo_estrutura   = basi_030.artigo
                                and   p_ie_artigo   = 1
                                UNION
                                select 1
                                from rcnb_060
                                where rcnb_060.tipo_registro     = 778
                                and   rcnb_060.nr_solicitacao    = 108
                                and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                and   rcnb_060.grupo_estrutura   <> basi_030.artigo
                                and   p_ie_artigo   = 2)
                  and   exists (select 1
                                from rcnb_060
                                where rcnb_060.tipo_registro     = 60
                                and   rcnb_060.nr_solicitacao    = 108
                                and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                and   pedi_100.cli_ped_cgc_cli9  = rcnb_060.grupo_estrutura and pedi_100.cli_ped_cgc_cli4 = rcnb_060.item_estrutura and pedi_100.cli_ped_cgc_cli2 = rcnb_060.nivel_estrutura
                                and   p_ie_cliente  = 1
                                UNION
                                select 1
                                from rcnb_060
                                where rcnb_060.tipo_registro     = 60
                                and   rcnb_060.nr_solicitacao    = 108
                                and   rcnb_060.subgru_estrutura  = p_codigo_usuario
                                and   rcnb_060.tipo_registro_rel = p_num_aleatorio
                                and   pedi_100.cli_ped_cgc_cli9  <> rcnb_060.grupo_estrutura and pedi_100.cli_ped_cgc_cli4 <> rcnb_060.item_estrutura and pedi_100.cli_ped_cgc_cli2 <> rcnb_060.nivel_estrutura
                                and   p_ie_cliente  = 2)
                  and   exists (select 1
                                from inter_vi_estrutura, basi_020
                                where inter_vi_estrutura.nivel_comp       = basi_020.basi030_nivel030
                                and   inter_vi_estrutura.grupo_comp       = basi_020.basi030_referenc
                                and   inter_vi_estrutura.sub_comp         = basi_020.tamanho_ref
                                and   inter_vi_estrutura.nivel_item       = pedi_110.cd_it_pe_nivel99
                                and   inter_vi_estrutura.grupo_item       = pedi_110.cd_it_pe_grupo
                                and   inter_vi_estrutura.sub_item         = pedi_110.cd_it_pe_subgrupo
                                and   inter_vi_estrutura.item_item        = pedi_110.cd_it_pe_item
                                and   inter_vi_estrutura.alternativa_item = basi_010.numero_alternati
                                and   inter_vi_estrutura.nivel_comp       = '2'
                                and   basi_020.tipo_produto               in (3,4))
                  group by 0 ,                             pedi_100.data_entr_venda ,
                           pedi_100.pedido_venda,          pedi_110.cd_it_pe_nivel99 ,
                           pedi_110.cd_it_pe_grupo,        pedi_110.cd_it_pe_subgrupo ,
                           pedi_110.cd_it_pe_item ,        basi_010.numero_alternati ,
                           basi_010.numero_roteiro ,
                           pedi_110.qtde_pedida - nvl(t630.qtde,0), pedi_100.classificacao_pedido)
   loop
      --VALIDA A DATA DE REQUERIMENTO COMERCIAL DO PROJETO
      --DO PRODUTO AO QUAL O PEDIDO ESTA RELACIONADO

       v_tmpInt := 0;


      begin
       select 1
       into v_tmp
       from rcnb_060
       where rcnb_060.tipo_registro     = 132
         and rcnb_060.nr_solicitacao   = 108
         and rcnb_060.subgru_estrutura = p_codigo_usuario
         and  rcnb_060.empresa_rel      = p_codigo_empresa
         and  rcnb_060.relatorio_rel    = 'tmrp_f108'
         and (rcnb_060.grupo_estrutura  = reg_itens.classificacao_pedido
          or  rcnb_060.grupo_estrutura  = 999);
       exception when others then
         v_tmpInt := 1;
      end;


      begin
         select pedi_665.data_requerida_comercial
         into   v_data_req_comercial
         from pedi_665, pedi_666, basi_001
         where pedi_665.cod_solicitacao_dp = pedi_666.cod_solicitacao_dp
           and pedi_666.codigo_projeto     = basi_001.codigo_projeto
           and basi_001.grupo_produto      = reg_itens.grupo;
         exception when others then
            v_data_req_comercial := null;
      end;

      if (v_data_req_comercial is null
      or (v_data_req_comercial >= p_data_req_ini
      and v_data_req_comercial <= p_data_req_fim))
      and v_tmpInt = 0
      then
         begin
            insert into tmrp_615_108 (
                  nr_solicitacao,                     tipo_registro,
                  nome_programa,                      selecionado,
                  codigo_usuario,                     usuario,
                  codigo_empresa,

                  ordem_planejamento,                 pedido_venda,
                  data_inicio,

                  nivel,                              grupo,
                  subgrupo,                           item,
                  alternativa,                        roteiro,

                  qtde_programacao,
                  qtde_maxima,                        tabela_preco,
                  qtde_minima,                        qtde_receber
               ) VALUES (
                  p_nr_solicitacao,                   655,
                  'tmrp_f108',                        0,
                  p_codigo_usuario,                   p_usuario,
                  p_codigo_empresa,

                  reg_itens.ordem_planejamento,       reg_itens.pedido_venda,
                  reg_itens.data_entrega,

                  reg_itens.nivel,                    reg_itens.grupo,
                  reg_itens.subgrupo,                 reg_itens.item,
                  reg_itens.alternativa,              reg_itens.roteiro,

                  reg_itens.saldo,
                  reg_itens.saldo,                    reg_itens.periodo_producao,
                  0,                                  reg_itens.saldo
               );
         end;
      end if;
   end loop;

   if not sql%found
   then
      v_retorno:= 0;
   end if;

   commit;
   return(v_retorno);
end;

 

/

exec inter_pr_recompile;

