CREATE TABLE pedi_106 (
  ID                     NUMBER(9)   NOT NULL,
  ID_PLANO_PGTO          NUMBER(9)   NOT NULL,
  CODIGO_EMPRESA         NUMBER(3)   NOT NULL,
  CODIGO_REPRES          NUMBER(6)   NOT NULL,
  COL_TABELA_PRECO       NUMBER(3)   NOT NULL,
  MES_TABELA_PRECO       NUMBER(3)   NOT NULL,
  SEQ_TABELA_PRECO       NUMBER(3)   NOT NULL,
  ATIVO                  VARCHAR(1) NOT NULL,
  USUARIO                VARCHAR2(15 BYTE),
  DATA_HORA_ALTERACAO    DATE
);
  
ALTER TABLE PEDI_106 ADD CONSTRAINT PK_PEDI_106 PRIMARY KEY (ID);
ALTER TABLE PEDI_106 ADD CONSTRAINT UNIQ_PEDI_106 UNIQUE(CODIGO_EMPRESA, CODIGO_REPRES, COL_TABELA_PRECO, MES_TABELA_PRECO, SEQ_TABELA_PRECO, ID_PLANO_PGTO);
ALTER TABLE PEDI_106
ADD CONSTRAINT REF_PEDI_106_PEDI_422 FOREIGN KEY (ID_PLANO_PGTO) REFERENCES PEDI_422 (ID);

COMMENT ON COLUMN PEDI_106.CODIGO_EMPRESA is 'Codigo de empresa para relacionamento de plano de pagamento de comiss�o X Empresa X representante X tabela de Preco';  
COMMENT ON COLUMN PEDI_106.CODIGO_REPRES is 'Codigo do representante para relacionamento de plano de pagamento de comiss�o X Empresa X representante X tabela de Preco';  
COMMENT ON COLUMN PEDI_106.COL_TABELA_PRECO is 'Colecao da tabela de preco';  
COMMENT ON COLUMN PEDI_106.MES_TABELA_PRECO is 'mes da tabela de preco';  
COMMENT ON COLUMN PEDI_106.SEQ_TABELA_PRECO is 'sequencia da tabela de preco';  
COMMENT ON COLUMN PEDI_106.ID_PLANO_PGTO is 'ID do plano de pagamento cadastrado na pedi_422';  
COMMENT ON COLUMN PEDI_106.ATIVO is 'Ativo S para relacionamento Ativo e N para relacionamento Inativo'; 
COMMENT ON COLUMN PEDI_106.DATA_HORA_ALTERACAO is 'Corresponde ao momento em que o usu�rio efetuou a altera��o no relacionamento.';

CREATE SEQUENCE ID_PEDI_106 START WITH 1 INCREMENT BY 1;
/
exec inter_pr_recompile ;
