CREATE OR REPLACE procedure inter_pr_inclui_rcnb_856 (p_empresa in number,
                                                      p_mes     in number,
                                                      p_ano     in number,
                                                      p_nivel   in varchar2,
                                                      p_grupo   in varchar2,
                                                      p_sub     in varchar2,
                                                      p_item    in varchar2,
                                                      p_versao  in number)
is
  cursor rcnb851 (p_nivel varchar2, p_grupo varchar2, p_sub varchar2, p_item varchar2) is
    select a.codigo_empresa,      a.mes, a.ano,       a.nivel_estrutura,
           a.grupo_estrutura,     a.subgru_estrutura, a.item_estrutura,
           a.ordem_leitura,       a.nivel_explosao,   a.nivel_componente,
           a.grupo_componente,    a.subgru_componente,a.item_componente,
           a.consumo,             a.preco_comp,       a.alternativa_estrutura,
           a.nr_versao
    from rcnb_851 a
    where a.codigo_empresa   = p_empresa
      and a.mes              = p_mes
      and a.ano              = p_ano
      and a.nivel_estrutura  = p_nivel
      and a.grupo_estrutura  = p_grupo
      and a.subgru_estrutura = p_sub
      and a.item_estrutura   = p_item
      and a.nr_versao        = p_versao
     --Verifica excecao para inclusao do produto
      and not exists (select 1 from rcnb_890 e
                      where e.nivel    = a.nivel_componente
                        and e.grupo    = a.grupo_componente
                        and e.subgrupo = a.subgru_componente
                        and e.item     = a.item_componente)
    order by a.codigo_empresa, a.ano, a.mes, a.nivel_estrutura, a.grupo_estrutura, a.subgru_estrutura, a.item_estrutura, a.ordem_leitura;

  consumo_n0     number(15,10) := 0;
  consumo_n1     number(15,10) := 0;
  consumo_n2     number(15,10) := 0;
  consumo_n3     number(15,10) := 0;
  consumo_n4     number(15,10) := 0;
  consumo_n5     number(15,10) := 0;
  consumo_nivel2 number(15,10) := 0;
  neps           number := 0;
  total_comp     number(15,6) := 0;
  tem_excecao    number := 0;


  v_nivel   rcnb_850.nivel_estrutura%type;
  v_grupo   rcnb_850.grupo_estrutura%type;
  v_sub     rcnb_850.subgru_estrutura%type;
  v_item    rcnb_850.item_estrutura%type;
  v_tem_insumo boolean;

begin
  if p_nivel = '1'
  then
     /* QUANDO Ã¿ NÃ?VEL 1 PEGA O MAIOR CONSUMO DE MATERIA PRIMA */
     begin
        select f.nivel_estrutura, f.grupo_estrutura, f.subgru_estrutura, f.item_estrutura
        into   v_nivel,           v_grupo,           v_sub,              v_item
        from rcnb_850 f
        where f.codigo_empresa   = p_empresa
          and f.mes              = p_mes
          and f.ano              = p_ano
          and f.nivel_estrutura  = p_nivel
          and f.grupo_estrutura  = p_grupo
          and f.nr_versao        = p_versao
          and f.valor_mp = (select max(a.valor_mp)
                            from rcnb_850 a
                            where a.codigo_empresa   = f.codigo_empresa
                              and a.mes              = f.mes
                              and a.ano              = f.ano
                              and a.nivel_estrutura  = f.nivel_estrutura
                              and a.grupo_estrutura  = f.grupo_estrutura
                              and a.nr_versao        = p_versao)
        and rownum < 2;
     exception
     when others then
        v_nivel := p_nivel;
        v_grupo := p_grupo;
        v_sub   := p_sub;
        v_item  := p_item;
     end;

  else
     v_nivel := p_nivel;
     v_grupo := p_grupo;
     v_sub   := p_sub;
     v_item  := p_item;
  end if;

  v_tem_insumo := false;

  for reg in rcnb851 (v_nivel, v_grupo, v_sub, v_item)
  loop
     v_tem_insumo := true;

     if reg.nivel_explosao = 0
     then
        consumo_nivel2 := 0;
        consumo_n0     := 0;
        consumo_n1     := 0;
        consumo_n2     := 0;
        consumo_n3     := 0;
        consumo_n4     := 0;
        consumo_n5     := 0;
     end if;

     if reg.nivel_explosao = 0
     then
        consumo_n0 := reg.consumo;
        consumo_nivel2 := consumo_n0;
     end if;

     if reg.nivel_explosao = 1
     then
        consumo_n1 := reg.consumo;
        consumo_nivel2 := consumo_n0 * consumo_n1;
     end if;

     if reg.nivel_explosao = 2
     then
        consumo_n2 := reg.consumo;
        consumo_nivel2 := consumo_n0 * consumo_n1 * consumo_n2;
     end if;

     if reg.nivel_explosao = 3
     then
        consumo_n3 := reg.consumo;
        consumo_nivel2 := consumo_n0 * consumo_n1 * consumo_n2 * consumo_n3;
     end if;

     if reg.nivel_explosao = 4
     then
        consumo_n4 := reg.consumo;
        consumo_nivel2 := consumo_n0 * consumo_n1 * consumo_n2 * consumo_n3 * consumo_n4;
     end if;

     if reg.nivel_explosao = 5
     then
        consumo_n5 := reg.consumo;
        consumo_nivel2 := consumo_n0 * consumo_n1 * consumo_n2 * consumo_n3 * consumo_n4 * consumo_n5;
     end if;

     neps := 0;

     begin
        select a.nivel_explosao into neps
        from rcnb_851 a
        where a.codigo_empresa   =  reg.codigo_empresa
          and a.mes              =  reg.mes
          and a.ano              =  reg.ano
          and a.nivel_estrutura  =  reg.nivel_estrutura
          and a.grupo_estrutura  =  reg.grupo_estrutura
          and a.subgru_estrutura =  reg.subgru_estrutura
          and a.item_estrutura   =  reg.item_estrutura
          and a.nr_versao        =  reg.nr_versao
          and a.ordem_leitura    = (reg.ordem_leitura + 1);
        exception
        when no_data_found then
          neps := reg.nivel_explosao;
        when others then
        select min(a.nivel_explosao) into neps
        from rcnb_851 a
        where a.codigo_empresa   =  reg.codigo_empresa
          and a.mes              =  reg.mes
          and a.ano              =  reg.ano
          and a.nivel_estrutura  =  reg.nivel_estrutura
          and a.grupo_estrutura  =  reg.grupo_estrutura
          and a.subgru_estrutura =  reg.subgru_estrutura
          and a.item_estrutura   =  reg.item_estrutura
          and a.nr_versao        =  reg.nr_versao
          and a.ordem_leitura    = (reg.ordem_leitura + 1);
     end;

     if neps = reg.nivel_explosao or neps < reg.nivel_explosao
     then

        begin
           insert into rcnb_856
           (codigo_empresa,
            mes,
            ano,
            nivel_estrutura,
            grupo_estrutura,
            subgru_estrutura,
            item_estrutura,
            ordem_leitura,
            nivel_explosao,
            nivel_componente,
            grupo_componente,
            subgru_componente,
            item_componente,
            nr_versao,
            consumo,
            preco_comp,
            alternativa_estrutura,
            custo_mp)
         values
           (reg.codigo_empresa,
            reg.mes,
            reg.ano,
            reg.nivel_estrutura,
            reg.grupo_estrutura,
            reg.subgru_estrutura,
            reg.item_estrutura,
            reg.ordem_leitura,
            reg.nivel_explosao,
            reg.nivel_componente,
            reg.grupo_componente,
            reg.subgru_componente,
            reg.item_componente,
            reg.nr_versao,
            consumo_nivel2,
            reg.preco_comp,
            reg.alternativa_estrutura,
            consumo_nivel2 * reg.preco_comp);
         exception
         when DUP_VAL_ON_INDEX then
           update rcnb_856
           set consumo        = consumo_nivel2,
               preco_comp            = reg.preco_comp,
               alternativa_estrutura = reg.alternativa_estrutura
           where codigo_empresa      = reg.codigo_empresa
           and mes                   = reg.mes
           and ano                   = reg.ano
           and nivel_estrutura       = reg.nivel_estrutura
           and grupo_estrutura       = reg.grupo_estrutura
           and subgru_estrutura      = reg.subgru_estrutura
           and item_estrutura        = reg.item_estrutura
           and nivel_explosao        = reg.nivel_explosao
           and ordem_leitura         = reg.ordem_leitura
           and nr_versao             = reg.nr_versao
           and nivel_componente      = reg.nivel_componente
           and grupo_componente      = reg.grupo_componente
           and subgru_componente     = reg.subgru_componente
           and item_componente       = reg.item_componente
           and alternativa_estrutura = reg.alternativa_estrutura;
        end;

     end if;
  end loop;

  if not v_tem_insumo
  then
     begin
           insert into rcnb_856
           (codigo_empresa,
            mes,
            ano,
            nivel_estrutura,
            grupo_estrutura,
            subgru_estrutura,
            item_estrutura,
            ordem_leitura,
            nivel_explosao,
            nivel_componente,
            grupo_componente,
            subgru_componente,
            item_componente,
            nr_versao,
            consumo,
            preco_comp,
            alternativa_estrutura)
         values
           (p_empresa,
            p_mes,
            p_ano,
            v_nivel,
            v_grupo,
            v_sub,
            v_item,
            0,
            0,
            '0',
            '00000',
            '000',
            '000000',
            p_versao,
            0,
            0,
            0);
         exception
         when DUP_VAL_ON_INDEX then
           update rcnb_856
           set consumo               = 0,
               preco_comp            = 0,
               alternativa_estrutura = 0
           where codigo_empresa      = p_empresa
           and mes                   = p_mes
           and ano                   = p_ano
           and nivel_estrutura       = v_nivel
           and grupo_estrutura       = v_grupo
           and subgru_estrutura      = v_sub
           and item_estrutura        = v_item
           and nivel_explosao        = 0
           and ordem_leitura         = 0
           and nr_versao             = p_versao
           and nivel_componente      = '0'
           and grupo_componente      = '00000'
           and subgru_componente     = '000'
           and item_componente       = '000000'
           and alternativa_estrutura = 0;
        end;
  end if;

end inter_pr_inclui_rcnb_856 ;

/
exec inter_pr_recompile;
