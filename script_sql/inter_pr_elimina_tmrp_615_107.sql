create or replace procedure inter_pr_elimina_tmrp_615_107 is
begin

  delete tmrp_615_107
  where tmrp_615_107.data_criacao < trunc(sysdate,'DD') - 2;
  commit;

end inter_pr_elimina_tmrp_615_107;


/

exec inter_pr_recompile;

/* versao: 1 */

