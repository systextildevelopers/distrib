insert into empr_007 (param, tipo, label, fyi_message, default_str, default_int, default_dbl, default_dat)
values ('pcpb.calcRecEstampa', 1, '', '', null, 0, null, null);
commit;


declare 
cursor parametro_c is select codigo_empresa from fatu_500;
tmpint number;

begin
  for reg_parametro in parametro_c
  loop
  
    select count(*) into tmpint
    from empr_008
    where empr_008.codigo_empresa = reg_parametro.codigo_empresa
      and empr_008.param = 'pcpb.calcRecEstampa';
    if(tmpint = 0)
    then
      begin
        insert into empr_008 (
          codigo_empresa, param, val_int
        ) values (
          reg_parametro.codigo_empresa, 'pcpb.calcRecEstampa', 0);
      end;
     end if; 
 
  end loop;
  commit;
end;
