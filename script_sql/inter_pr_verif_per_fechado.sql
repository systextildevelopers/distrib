
  CREATE OR REPLACE PROCEDURE "INTER_PR_VERIF_PER_FECHADO" (
   p_periodo_producao          number,
   p_area_producao             number
) is
   v_existe_reg          integer;
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_nome_programa          varchar2(50);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_existe_reg := 0;

   begin
      select nvl(count(*),0)
      into v_existe_reg
      from pcpc_010
      where pcpc_010.periodo_producao = p_periodo_producao
        and pcpc_010.area_periodo     = p_area_producao
        and pcpc_010.situacao_periodo = 3;
   exception when others then
      v_existe_reg := 0;
   end;

   ws_nome_programa := inter_fn_nome_programa(ws_sid);

   if  ws_nome_programa = 'estq_f011'
   or  ws_nome_programa = 'estq_f083'
   or  ws_nome_programa = 'pcpc_f150'
   then
      v_existe_reg := 0;
   end if;

   if v_existe_reg <> 0
   then
       raise_application_error(-20000,'ATENCAO! Periodo de producao fechado. Nao pode digitar ordens, lancar producao, entre outras operacoes.');
   end if;

end inter_pr_verif_per_fechado;

 

/

exec inter_pr_recompile;

