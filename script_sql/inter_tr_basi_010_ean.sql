
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_010_EAN" 
before insert or
       update of codigo_barras
on basi_010
for each row
declare
  Pragma Autonomous_Transaction;
  v_tipo_cod_barras            number;
  v_consiste_conflito_eans     number;
  v_tipo_codigo_ean            number;
  v_encontrou_prod             number;
  v_qtde_estoque               number;
  v_em_producao                number; --0: Nao, 1: Sim
  v_produtos_conflito          varchar2(4000);
  v_produtos_conflito_com_estq varchar2(4000);
  v_produtos_conflito_em_prod  varchar2(4000);
  v_estq_ou_em_prod            boolean;
  v_mensagem_exibir            varchar2(4000);
begin

   begin
     select max(fatu_500.tipo_cod_barras)
     into   v_tipo_cod_barras
     from fatu_500
     where fatu_500.tipo_cod_barras = 2;
   exception
     when no_data_found then
       v_tipo_cod_barras := 0;
   end;

   begin
     select max(fatu_503.consiste_conflito_eans)
     into   v_consiste_conflito_eans
     from fatu_503
     where fatu_503.consiste_conflito_eans = 1;
   exception
     when no_data_found then
       v_consiste_conflito_eans := 0;
   end;

   /* Verificar se a referencia possui tipo Ean por ITEM e nao por TAMANHO, conforme conversa com Osmar */
   begin
     select basi_030.tipo_codigo_ean
     into   v_tipo_codigo_ean
     from basi_030
     where basi_030.nivel_estrutura = :new.nivel_estrutura
       and basi_030.referencia      = :new.grupo_estrutura;
   exception
     when no_data_found then
       v_tipo_codigo_ean := 0;
   end;

   /* SE O CODIGO FOR EAN */
   if v_consiste_conflito_eans = 1 and v_tipo_cod_barras = 2 and v_tipo_codigo_ean = 1 and :new.nivel_estrutura = '1' and :new.codigo_barras is not null and :new.codigo_barras <> ' ' and :new.codigo_barras <> :old.codigo_barras /* EAN POR ITEM/COR */
   then

      /* VERIFICAR SE EXISTE OUTRO PRODUTO DIFERENTE COM MESMO CODIGO EAN */
      v_encontrou_prod := 1;
      begin
         select max(1)
         into  v_encontrou_prod
         from  basi_010
         where basi_010.codigo_barras     = :new.codigo_barras
           and (basi_010.nivel_estrutura  <> :new.nivel_estrutura  or
                basi_010.grupo_estrutura  <> :new.grupo_estrutura  or
                basi_010.subgru_estrutura <> :new.subgru_estrutura or
                basi_010.item_estrutura   <> :new.item_estrutura);
      exception
      when no_data_found then
         /* NAO ENCONTROU OUTRO PRODUTO COM MESMO CODIGO DE BARRAS */
         v_encontrou_prod := 0;
      end;

      if v_encontrou_prod = 1
      then

         /* ENCONTROU OUTRO PRODUTO COM MESMO CODIGO DE BARRAS */

         /* Buscar os produtos conflitantes */
         v_produtos_conflito          := '';
         v_produtos_conflito_com_estq := '';
         v_produtos_conflito_em_prod  := '';
         v_estq_ou_em_prod            := false;

         for f_conflitantes in (
            select basi_010.nivel_estrutura,    basi_010.grupo_estrutura,
                   basi_010.subgru_estrutura,   basi_010.item_estrutura
            from  basi_010
            where basi_010.codigo_barras     = :new.codigo_barras
              and (basi_010.nivel_estrutura  <> :new.nivel_estrutura  or
                   basi_010.grupo_estrutura  <> :new.grupo_estrutura  or
                   basi_010.subgru_estrutura <> :new.subgru_estrutura or
                   basi_010.item_estrutura   <> :new.item_estrutura))
         LOOP

            v_produtos_conflito := v_produtos_conflito ||
                                   f_conflitantes.nivel_estrutura || '.' || f_conflitantes.grupo_estrutura || '.' || f_conflitantes.subgru_estrutura || '.' || f_conflitantes.item_estrutura || chr(10);

            /* Verificar se o produto possui estoque */
            v_qtde_estoque := 0.000;

            begin
               select sum(estq_040.qtde_estoque_atu)
               into   v_qtde_estoque
               from estq_040
               where estq_040.cditem_nivel99  = f_conflitantes.nivel_estrutura
                 and estq_040.cditem_grupo    = f_conflitantes.grupo_estrutura
                 and estq_040.cditem_subgrupo = f_conflitantes.subgru_estrutura
                 and estq_040.cditem_item     = f_conflitantes.item_estrutura;
            exception
            when no_data_found then
               v_qtde_estoque := 0.000;
            end;

            if v_qtde_estoque > 0.000
            then
               v_estq_ou_em_prod            := true;
               v_produtos_conflito_com_estq := v_produtos_conflito_com_estq ||
                                               f_conflitantes.nivel_estrutura || '.' || f_conflitantes.grupo_estrutura || '.' || f_conflitantes.subgru_estrutura || '.' || f_conflitantes.item_estrutura || chr(10);
            end if;


            /* Verificar se o produto esta em producao */
            v_em_producao := 1;

            begin
               select 1
               into   v_em_producao
               from pcpc_040, pcpc_020
               where pcpc_040.ordem_producao   = pcpc_020.ordem_producao
                 and pcpc_040.codigo_estagio   = pcpc_020.ultimo_estagio
                 and pcpc_020.cod_cancelamento = 0
                 and pcpc_040.qtde_pecas_prog  > pcpc_040.qtde_pecas_prod + pcpc_040.qtde_conserto + pcpc_040.qtde_pecas_2a + pcpc_040.qtde_perdas
                 and pcpc_040.proconf_nivel99  = f_conflitantes.nivel_estrutura
                 and pcpc_040.proconf_grupo    = f_conflitantes.grupo_estrutura
                 and pcpc_040.proconf_subgrupo = f_conflitantes.subgru_estrutura
                 and pcpc_040.proconf_item     = f_conflitantes.item_estrutura;
            exception
            when no_data_found then
               v_em_producao := 0;
            end;

            if v_em_producao = 1
            then
               v_estq_ou_em_prod           := true;
               v_produtos_conflito_em_prod := v_produtos_conflito_em_prod ||
                                              f_conflitantes.nivel_estrutura || '.' || f_conflitantes.grupo_estrutura || '.' || f_conflitantes.subgru_estrutura || '.' || f_conflitantes.item_estrutura || chr(10);
            end if;

         END LOOP;

         /* SE ESTIVER EM PRODUCAO OU SE POSSUIR ESTOQUE, NAO PERMITIR. EXIBIR MENSAGEM E PROIBIR */
         if v_estq_ou_em_prod
         then

            v_mensagem_exibir := 'ATENCAO! Este codigo de barras (EAN) ja esta sendo usado para outro produto que esta em producao ou possui estoque. Nao e possivel utilizar este codigo EAN. ' || chr(10) || chr(10) ||
                                 'Produtos conflitantes e com estoque: ' || v_produtos_conflito_com_estq || chr(10) || chr(10) ||
                                 'Produtos conflitantes e em producao: ' || v_produtos_conflito_em_prod;

            raise_application_error(-20000, v_mensagem_exibir);

         else

            /* SE NAO ESTIVER EM PRODUCAO E SE NAO POSSUIR ESTOQUE... */

            v_mensagem_exibir := 'ATENCAO! Ja existe(m) outro(s) produto(s) com mesmo codigo EAN. Nao e possivel utilizar este codigo de barras. ' || chr(10) || chr(10) ||
                                 'Produtos conflitantes: ' || v_produtos_conflito;

            raise_application_error(-20000, v_mensagem_exibir);

         end if;

      end if;

   end if;
end inter_tr_basi_010_ean;

-- ALTER TRIGGER "INTER_TR_BASI_010_EAN" ENABLE
 

/

exec inter_pr_recompile;

