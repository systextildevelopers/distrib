
  CREATE OR REPLACE procedure INTER_PR_INTEGRA_INVENT (p_cod_empresa in number, p_cnpj_transp in number, p_nota in number, p_serie in varchar2, p_chave_nfe in varchar2, p_msg_erro out varchar2, p_chave_cte out varchar2) is
/*
-- ESTA TRIGGER POSSUI CONTROLE DE TRANSACAO FAVOR CUIDAR!!
v_cod_empresa        obrf_162.cod_empresa%type;
v_num_nota           obrf_162.num_nota_fiscal%type;
v_serie              obrf_162.serie_nota_fiscal%type;
v_cnpj_9             obrf_162.cnpj9%type;
v_cnpj_4             obrf_162.cnpj4%type;
v_cnpj_2             obrf_162.cnpj2%type;
v_cnpj_9_emp         obrf_162.cnpj9%type;
v_cnpj_4_emp         obrf_162.cnpj4%type;
v_cnpj_2_emp         obrf_162.cnpj2%type;
v_chave_nfe          obrf_162.numero_danfe_nfe%type;
v_data_emis          obrf_162.data_emissao%type;
v_total_nota         obrf_162.total_nota%type;
v_total_ipi          obrf_162.total_ipi%type;
v_total_frete        obrf_162.total_frete%type;
v_total_seguro       obrf_162.total_seguro%type;
v_total_despesas     obrf_162.total_despesas%type;
v_total_desconto     obrf_162.total_desconto%type;
v_total_mercadoria   obrf_162.total_mercadoria%type;
v_total_base_icms    obrf_162.total_base_icms%type;
v_total_icms         obrf_162.total_icms%type;
v_total_icms_st      obrf_162.total_icms_st%type;
v_total_base_icms_st obrf_162.total_base_icms_st%type;
v_peso_liquido       obrf_162.peso_liquido%type;
v_peso_bruto         obrf_162.peso_bruto%type;
v_tipo_frete         obrf_162.tipo_frete%type;
v_ind_status_nfe     nfepack.interf_nfe.ind_status_nfe%type;
v_e_cnpj             nfepack.interf_nfe.e_cnpj%type;
v_c_cnpj             nfepack.interf_nfe.c_cnpj%type;
v_critica            nfepack.crit_integr_nfe.dsc_crit_integr%type;
ws_sid               v_lista_sessao_banco.sid%type;
ws_id                v_lista_sessao_banco.inst_id%type;
processo_systextil   hdoc_090.programa%type;

begin

   begin
      select sid,        inst_id
      into ws_sid, ws_id
      from v_lista_sessao_banco;
   exception when no_data_found
   then
      ws_sid := 0;
      ws_id  := 0;
   end;

   begin
      select hdoc_090.programa
      into processo_systextil
      from hdoc_090
      where hdoc_090.sid       = ws_sid
      and hdoc_090.instancia = ws_id
      and rownum             = 1;
   exception when no_data_found
   then
        processo_systextil := 'SQL';
   end;

   begin
      select interf_nfe.ind_status_nfe,            interf_nfe.a_id,
             interf_nfe.b_nnf,                     interf_nfe.b_serie,
             interf_nfe.c_cnpj,                    interf_nfe.e_cnpj,
             interf_nfe.b_demi,                    interf_nfe.w_vnf,
             interf_nfe.w_vipi,                    interf_nfe.w_vbcst,
             interf_nfe.w_vfrete,                  interf_nfe.w_vseg,
             interf_nfe.w_voutro,                  interf_nfe.w_vdesc,
             interf_nfe.w_vprod,                   interf_nfe.w_vbc,
             interf_nfe.w_vicms,                   interf_nfe.x_modfrete,
             sum(interf_volume_nfe.x_pesol),       sum(interf_volume_nfe.x_pesob)
      into   v_ind_status_nfe,                     v_chave_nfe,
             v_num_nota,                           v_serie,
             v_c_cnpj,                             v_e_cnpj,
             v_data_emis,                          v_total_nota,
             v_total_ipi,                          v_total_base_icms_st,
             v_total_frete,                        v_total_seguro,
             v_total_despesas,                     v_total_desconto,
             v_total_mercadoria,                   v_total_base_icms,
             v_total_icms,                         v_tipo_frete,
             v_peso_liquido,                       v_peso_bruto
      from nfepack.interf_nfe,
           nfepack.interf_volume_nfe\*,
           nfepack.crit_integr_nfe*\
      where interf_nfe.a_id                             = p_chave_nfe
        and interf_nfe.ind_tipo_proces                  = 'R'
        and interf_volume_nfe.num_seq_nfe           (+) = interf_nfe.num_seq_nfe
        \*and interf_volume_nfe.num_seq_nfe           (+) = interf_nfe.num_seq_nfe
        and crit_integr_nfe.num_cnpj_estab_gerdor   (+) = interf_nfe.num_cnpj_estab_gerdor
        and crit_integr_nfe.num_serie_docto_fiscal  (+) = interf_nfe.num_serie_docto_fiscal
        and crit_integr_nfe.num_docto_fiscal        (+) = interf_nfe.num_docto_fiscal*\
      group by interf_nfe.ind_status_nfe,       interf_nfe.a_id,
               interf_nfe.b_nnf,                interf_nfe.b_serie,
               interf_nfe.c_cnpj,               interf_nfe.e_cnpj,
               interf_nfe.b_demi,               interf_nfe.w_vnf,
               interf_nfe.w_vipi,               interf_nfe.w_vbcst,
               interf_nfe.w_vfrete,             interf_nfe.w_vseg,
               interf_nfe.w_voutro,             interf_nfe.w_vdesc,
               interf_nfe.w_vprod,              interf_nfe.w_vbc,
               interf_nfe.w_vicms,              interf_nfe.x_modfrete;
   exception
       when no_data_found then
          v_ind_status_nfe := 0;
   end;

   if v_ind_status_nfe  <> 0 and v_ind_status_nfe <> 34 and v_ind_status_nfe <> 32
   then
      v_cnpj_4_emp := substr(v_e_cnpj, 8, 4);
      v_cnpj_2_emp := substr(v_e_cnpj, 12, 2);
      select to_number(substr(to_char(v_e_cnpj),1,length(to_char(v_e_cnpj))-6)),
             to_number(reverse(substr(reverse(to_char(v_e_cnpj)),1, 2))),
             to_number(reverse(substr(reverse(to_char(v_e_cnpj)),3, 4)))
      into v_cnpj_9_emp,
           v_cnpj_2_emp,
           v_cnpj_4_emp
      from dual;

      v_cnpj_4 := substr(v_c_cnpj, 8, 4);
      v_cnpj_2 := substr(v_c_cnpj, 12, 2);
      select to_number(substr(to_char(v_c_cnpj),1,length(to_char(v_c_cnpj))-6)),
             to_number(reverse(substr(reverse(to_char(v_c_cnpj)),1, 2))),
             to_number(reverse(substr(reverse(to_char(v_c_cnpj)),3, 4)))
      into v_cnpj_9,
           v_cnpj_2,
           v_cnpj_4
      from dual;

      begin
         select fatu_500.codigo_empresa
         into   v_cod_empresa
         from fatu_500
         where fatu_500.cgc_9 = v_cnpj_9_emp
           and fatu_500.cgc_4 = v_cnpj_4_emp
           and fatu_500.cgc_2 = v_cnpj_2_emp;
      exception
          when no_data_found then
             v_cod_empresa := 0;
      end;

      if p_cod_empresa <> v_cod_empresa
      then
         v_cod_empresa := 0;
         p_msg_erro := 'ATENCAO! Nota Fiscal nao pertence a esta empresa.';
      end if;

      begin
         select supr_010.fornecedor9
         into v_cnpj_9
         from supr_010
         where supr_010.fornecedor9 = v_cnpj_9
           and supr_010.fornecedor4 = v_cnpj_4
           and supr_010.fornecedor2 = v_cnpj_2;
      exception
          when no_data_found then
             v_cod_empresa := 0;
             p_msg_erro := 'ATENCAO! Fornecedor nao esta cadastrado.';
      end;

      if v_cod_empresa <> 0
      then

         --Conversao do tipo de frete para o ST
         if v_tipo_frete = 0
         then
            v_tipo_frete := 1;
            else
               if v_tipo_frete = 1
               then
                  v_tipo_frete := 2;
               else
               if v_tipo_frete = 2
               then
                  v_tipo_frete := 3;
               end if;
            end if;
         end if;

         begin
            insert into obrf_162
            (obrf_162.cod_empresa --001
            ,obrf_162.num_nota_fiscal --002
            ,obrf_162.serie_nota_fiscal --003
            ,obrf_162.cnpj9 --004
            ,obrf_162.cnpj4 --005
            ,obrf_162.cnpj2 --006
            ,obrf_162.numero_danfe_nfe --007
            ,obrf_162.data_emissao --008
            ,obrf_162.total_nota --009
            ,obrf_162.total_ipi --010
            ,obrf_162.total_frete --011
            ,obrf_162.total_seguro --012
            ,obrf_162.total_despesas --013
            ,obrf_162.total_desconto --014
            ,obrf_162.total_mercadoria --015
            ,obrf_162.total_base_icms --016
            ,obrf_162.total_icms --017
            ,obrf_162.total_icms_st --018
            ,obrf_162.total_base_icms_st --019
            ,obrf_162.peso_liquido --020
            ,obrf_162.peso_bruto --021
            ,obrf_162.tipo_frete) --022
            values
            (v_cod_empresa --001
            ,v_num_nota --002
            ,v_serie --003
            ,v_cnpj_9 --004
            ,v_cnpj_4 --005
            ,v_cnpj_2 --006
            ,v_chave_nfe --007
            ,v_data_emis --008
            ,decode(v_total_nota, null, 0.00, v_total_nota) --009
            ,decode(v_total_ipi, null, 0.00, v_total_ipi) --010
            ,decode(v_total_frete, null, 0.00, v_total_frete) --011
            ,decode(v_total_seguro, null, 0.00, v_total_seguro) --012
            ,decode(v_total_despesas, null, 0.00, v_total_despesas) --013
            ,decode(v_total_desconto, null, 0.00, v_total_desconto) --014
            ,decode(v_total_mercadoria, null, 0.00, v_total_mercadoria) --015
            ,decode(v_total_base_icms, null, 0.00, v_total_base_icms) --016
            ,decode(v_total_icms, null, 0.00, v_total_icms) --017
            ,decode(v_total_icms_st, null, 0.00, v_total_icms_st) --018
            ,decode(v_total_base_icms_st, null, 0.00, v_total_base_icms_st) --019
            ,decode(v_peso_liquido, null, 0.00, v_peso_liquido) --020
            ,decode(v_peso_bruto, null, 0.00, v_peso_bruto) --021
            ,decode(v_tipo_frete, null, 0.00, v_tipo_frete)); --022
         exception
             when others then
                p_msg_erro := 'Nao inseriu dados na tabela obrf_162: '  || SQLERRM;
         end;
         if processo_systextil <> 'inte_f260'
         then commit;
         end if;
      end if; --if v_cod_empresa <> 0
   else
      if v_ind_status_nfe = 0
      then
         begin
            select crit_integr_nfe.dsc_crit_integr
            into v_critica
            from nfepack.crit_integr_nfe
            where crit_integr_nfe.num_docto_fiscal       = to_number(substr(p_chave_nfe, 26, 9))
              and crit_integr_nfe.num_serie_docto_fiscal = to_char(to_number(substr(p_chave_nfe, 23, 3)))
              and crit_integr_nfe.num_cnpj_emitente      = to_number(substr(p_chave_nfe, 7, 14));
         exception
             when no_data_found then
                v_critica := ' ';
         end;
         if v_critica <> ' '
         then
            p_msg_erro := 'ATENCAO! XML com problemas. Favor entrar com um XML valido.: ' || v_critica;
         end if;
      end if; --if v_ind_status_nfe = 0

      if v_ind_status_nfe = 34
      then
         p_msg_erro := 'ATENCAO! A Nota Fiscal esta cancelada.';
      end if;

      if v_ind_status_nfe = 32
      then
         begin
            select d.dsc_crit
            into v_critica
            from nfepack.interf_nfe a,
                 nfepack.evento_nfe b,
                 nfepack.crit_evento_nfe c,
                 nfepack.crit d
            where a.a_id = p_chave_nfe
              and ind_status_nfe = 32
              and a.num_seq_nfe          = b.num_seq_nfe
              and b.ind_tipo_evento_nfe  = 52
              and c.num_seq_evento_nfe   = b.num_seq_evento_nfe
              and c.cod_crit             = d.cod_crit
              and rownum = 1
              order by b.dat_hor_evento_nfe;
         exception
             when no_data_found then
                v_critica := ' ';
         end;
         p_msg_erro := 'ATENCAO! XML com problemas. Favor entrar com um XML valido.: ' || v_critica;
      end if; --if v_ind_status_nfe = 32
   end if; --if v_ind_status_nfe  <> 0*/

/*Apagar as linhas com as anotacoes "Apagar aqui" e descomentar a procedure*/
begin /*Apagar aqui*/
    p_msg_erro := 'ATENCAO! Entre em contato com a TI para atualizar a integracao do Systextil com a Inventti.'; /*Apagar aqui*/
end INTER_PR_INTEGRA_INVENT;

 

/

exec inter_pr_recompile;

