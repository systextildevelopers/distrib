alter table tmrp_695 add dep_entrada_op number(3) default 0;
comment on column tmrp_695.dep_entrada_op is 'Depósito informado no quadro Distribuição Ordens da Projeção (tmrp_f690), que será gravado na Ordem de Producao (pcpc_020.deposito_entrada).';

exec inter_pr_recompile;
