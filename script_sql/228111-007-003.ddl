create table HAUS_006
(
  ID          NUMBER(9) not null,
  ID_HAUS_002 NUMBER(9),
  NUMERO_NOTA NUMBER(15),
  CONSTRAINT PK_HAUS_006 PRIMARY KEY (ID)

);

ALTER TABLE HAUS_006
  ADD CONSTRAINT FK_HAUS_006_ID_HAUS_002 
  FOREIGN KEY (ID_HAUS_002)
  REFERENCES HAUS_002 (ID)
  ON DELETE CASCADE;
  
create sequence ID_HAUS_006
minvalue 0
maxvalue 99999999999999999999
start with 1
increment by 1
cache 20;

create or replace trigger "INTER_TR_HAUS_006_ID"
before insert on HAUS_006
 for each row

declare
  v_nr_registro number;

begin
  select ID_HAUS_006.nextval into v_nr_registro from dual;

  :new.id := v_nr_registro;

end INTER_TR_HAUS_006_ID;
