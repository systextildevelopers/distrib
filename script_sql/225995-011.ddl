alter table empr_002
modify CLASSIFIC_FISCAL default 'N';

update empr_002
set CLASSIFIC_FISCAL = 'N'
where trim(CLASSIFIC_FISCAL) is null;

commit;
/
