CREATE TABLE OBRF_442_HIST
(
   aplicacao                 	  VARCHAR2(20),
   tipo_ocorr                	  VARCHAR2(1),
   data_ocorr                 	DATE,
   usuario_rede              	  VARCHAR2(20),
   maquina_rede              	  VARCHAR2(40),
   codigo_old				            NUMBER(5),
   codigo_new				            NUMBER(5),
   nome_old				              VARCHAR2(45),
   nome_new				              VARCHAR2(45),
   descricao_old				        VARCHAR2(250),
   descricao_new				        VARCHAR2(250),
   script_sql_old			          VARCHAR2(4000),
   script_sql_new	              VARCHAR2(4000)
);
