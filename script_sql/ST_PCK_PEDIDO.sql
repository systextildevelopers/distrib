create or replace package ST_PCK_PEDIDO as
    TYPE t_dados_pedi_100 IS TABLE OF pedi_100%rowtype;
    TYPE t_dados_pedi_100_process IS TABLE OF pedi_100%rowtype;

    function get_dados_pedido(p_pedido_venda number, p_msg_erro in out varchar2) return t_dados_pedi_100;

        FUNCTION get_pedido_by_exportacao(  p_pedido_venda number,  p_codigo_empresa number, 
                                        p_cod_proforma varchar2,
                                        p_flag_por_item number,
                                        p_msg_erro in out varchar2) return t_dados_pedi_100;
                                        
    function get_pedido_by_processo(p_chave_processo varchar2, p_codigo_empresa number, p_msg_erro in out varchar2) return t_dados_pedi_100;

    function get_pedidos_por_proforma( p_cod_proforma varchar2, p_msg_erro in out varchar2, p_flag_por_item number) return t_dados_pedi_100;
    
    FUNCTION get_pedido_by_draft(  p_chave_processo varchar2,  p_codigo_empresa number, p_flag_por_item varchar2,  
                                      p_msg_erro in out varchar2) return t_dados_pedi_100;
    
    
end;
/
