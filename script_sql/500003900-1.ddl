alter table pedi_080 add ind_cfop_exportacao varchar2(1) default 'N';
comment on column pedi_080.ind_cfop_exportacao is 'Indica se a natureza sera utilizada para exportacao, mesmo que o destino seja uma UF interna';
exec inter_pr_recompile;
