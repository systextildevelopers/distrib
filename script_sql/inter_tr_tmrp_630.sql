
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_630" 
   before insert or
          delete or
          update of programado_comprado
   on tmrp_630
   for each row

declare
    v_qtde_update             number(15,5);
    v_qtde_atendida_estoque   number(15,5);
    v_ordem_planejamento      number(9);
    v_pedido_venda            tmrp_630.pedido_venda%TYPE;
    v_pedido_reserva          tmrp_630.pedido_reserva%TYPE;
    v_nivel_produto           varchar2(1);
    v_grupo_produto           varchar2(5);
    v_subgrupo_produto        varchar2(3);
    v_item_produto            varchar2(6);
    v_alternativa_produto     number(2);
    v_codigo_risco            number(5);
    v_area_producao           number(1);

begin
   v_codigo_risco := 0;

   if updating or inserting
   then
      if updating
      then
         v_qtde_update           := :new.programado_comprado - :old.programado_comprado;
         v_qtde_atendida_estoque := :new.qtde_atendida_estoque - :old.qtde_atendida_estoque;
      else
         v_qtde_update           := :new.programado_comprado;
         v_qtde_atendida_estoque := :new.qtde_atendida_estoque;
      end if;

      v_ordem_planejamento    := :new.ordem_planejamento;
      v_pedido_venda          := :new.pedido_venda;
      v_pedido_reserva        := :new.pedido_reserva;
      v_nivel_produto         := :new.nivel_produto;
      v_grupo_produto         := :new.grupo_produto;
      v_subgrupo_produto      := :new.subgrupo_produto;
      v_item_produto          := :new.item_produto;
      v_alternativa_produto   := :new.alternativa_produto;
      v_area_producao         := :new.area_producao;

      if  :new.area_producao = 1
      and :new.codigo_risco  = 0
      then
         begin
            select pcpc_020.codigo_risco
            into v_codigo_risco
            from pcpc_020
            where pcpc_020.ordem_producao = :new.ordem_prod_compra;
            exception when no_data_found
            then v_codigo_risco := 0;
         end;
      end if;

      if :new.codigo_risco < 0
      then
         :new.codigo_risco := 0;
      end if;
   else
      v_qtde_update           := :old.programado_comprado * (-1);
      v_qtde_atendida_estoque := :old.qtde_atendida_estoque;

      v_ordem_planejamento    := :old.ordem_planejamento;
      v_pedido_venda          := :old.pedido_venda;
      v_pedido_reserva        := :old.pedido_reserva;
      v_nivel_produto         := :old.nivel_produto;
      v_grupo_produto         := :old.grupo_produto;
      v_subgrupo_produto      := :old.subgrupo_produto;
      v_item_produto          := :old.item_produto;
      v_alternativa_produto   := :old.alternativa_produto;
      v_area_producao         := :old.area_producao;
   end if;

   if  updating
   and (:new.qtde_programada_orig =  0
   and  :old.programado_comprado  <> 0
   and  :old.programado_comprado  <> :new.programado_comprado
   and  :new.programado_comprado  <> 0)
   then
      :new.qtde_programada_orig := trunc(:old.programado_comprado);
   end if;

   if (updating or inserting)
   and :new.qtde_programada_orig is null
   then
      :new.qtde_programada_orig := 0;
   end if;


   inter_pr_update_producao_plan(v_ordem_planejamento,
                                 v_pedido_venda,
                                 v_pedido_reserva,
                                 v_nivel_produto,
                                 v_grupo_produto,
                                 v_subgrupo_produto,
                                 v_item_produto,
                                 v_qtde_update,
                                 v_qtde_atendida_estoque,
                                 v_alternativa_produto,
                                 v_codigo_risco,
                                 v_area_producao);

   if updating or inserting
   then
      :new.codigo_risco := 0;
   end if;

end inter_tr_tmrp_630;

-- ALTER TRIGGER "INTER_TR_TMRP_630" ENABLE
 

/

exec inter_pr_recompile;

