------------------------------
-- Classificação de tecidos --
------------------------------

create table pcpt_072 (
  id number primary key not null,
  definicao varchar2(200) not null,
  qtde_metros_de number,
  qtde_metros_ate number,
  unidade_medida_embalagem varchar2(10) not null,
  descricao_da_embalagem varchar2(400)
);

insert into pcpt_072 values(1, 'Aparas', 0, 0.49, 'KG', 'Saco amarelo de 40kg');
insert into pcpt_072 values(2, 'Retalhos', 0.50, 5.99, 'KG', 'Sacos coloridos de 40kg');
insert into pcpt_072 values(3, 'Peça pequena', 6, 30, 'MT', 'Rolo de tecido');
insert into pcpt_072 values(4, 'Peça média', 30, 80, 'MT', 'Rolo de tecido');
insert into pcpt_072 values(5, 'Peça Grande', 80, 999, 'MT', 'Rolo de tecido');

comment on table pcpt_072 is 'Classificação de tecido';
comment on column pcpt_072.id is 'Chave primária';
comment on column pcpt_072.qtde_metros_de is 'Quantidade miníma de metros do tecido';
comment on column pcpt_072.qtde_metros_ate is 'Quantidade máxima de metros do tecido';
comment on column pcpt_072.unidade_medida_embalagem is 'Unidade de medida da embalagem';
comment on column pcpt_072.descricao_da_embalagem is 'Descrição da embalagem';
