
  CREATE OR REPLACE TRIGGER "INTER_TR_INTE_100" 
  before insert on inte_100
  for each row

declare
  v_parametro fatu_503.integracao_pedidos_venda%type;

begin

  select fatu_503.integracao_pedidos_venda
    into v_parametro
    from fatu_503
   where fatu_503.codigo_empresa = :new.codigo_empresa;

  if v_parametro > 0 then
    :new.sit_importacao := v_parametro;
  end if;

end;
-- ALTER TRIGGER "INTER_TR_INTE_100" ENABLE
 

/

exec inter_pr_recompile;

