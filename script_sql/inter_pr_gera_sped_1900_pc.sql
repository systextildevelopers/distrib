create or replace procedure inter_pr_gera_sped_1900_pc (p_cod_empresa   IN NUMBER,
                                                        p_cod_matriz    IN NUMBER,
                                                        p_cnpj9_empresa IN  NUMBER,
                                                        p_cnpj4_empresa IN  NUMBER,
                                                        p_cnpj2_empresa IN  NUMBER,
                                                        p_dat_inicial   IN DATE,
                                                        p_dat_final     IN DATE,
                                                        p_des_erro      OUT varchar2) is
--
-- Finalidade: Gerar a tabela sped_pc_1900 - Notas Fiscais
-- Autor.....: Edson Pio
-- Data......: 05/06/13


CURSOR u_fatu_050 (p_cod_empresa     NUMBER,
                   p_dat_inicial     DATE,
                   p_dat_final       DATE) IS
    select pedi_080.cod_natureza || pedi_080.divisao_natur CFOP,
                  fatu_060.cvf_pis cst,
                  fatu_060.perc_pis,
                  fatu_050.serie_nota_fisc serie,

                  fatu_060.perc_cofins,
                  fatu_060.transacao,
                  fatu_060.codigo_contabil,
                  sum(fatu_060.valor_contabil) vlr_contabil,
                  decode(fatu_050.situacao_nfisc,1,0,fatu_050.situacao_nfisc) situacao_nfisc,
                  fatu_050.historico_cont,
                  fatu_060.centro_custo,
                  pedi_080.modelo_doc_fisc

        from fatu_060,
             fatu_050,
             pedi_080
        where fatu_060.ch_it_nf_cd_empr  = fatu_050.codigo_empresa
          and fatu_060.ch_it_nf_num_nfis = fatu_050.num_nota_fiscal
          and fatu_060.ch_it_nf_ser_nfis = fatu_050.serie_nota_fisc
          and fatu_050.data_emissao      between p_dat_inicial and p_dat_final
          and fatu_050.codigo_empresa    = p_cod_empresa
          and fatu_050.cod_justificativa <> 1
          and fatu_050.situacao_nfisc in (1,2)
          and fatu_060.natopeno_nat_oper = pedi_080.natur_operacao
          and fatu_060.natopeno_est_oper = pedi_080.estado_natoper
          and pedi_080.livros_fiscais    = 1
          and pedi_080.faturamento       = 1
          and exists (select 1 from fatu_070
                      where fatu_070.codigo_empresa  = fatu_050.codigo_empresa
                        and fatu_070.num_duplicata = fatu_050.num_nota_fiscal
                        and fatu_070.serie_nota_fisc = fatu_070.serie_nota_fisc)
        group by pedi_080.cod_natureza || pedi_080.divisao_natur,
                 fatu_060.cvf_pis,
                 fatu_060.perc_pis,
                 fatu_060.perc_cofins,
                 fatu_060.transacao,
                 fatu_060.codigo_contabil,
                 fatu_050.serie_nota_fisc,
                 decode(fatu_050.situacao_nfisc,1,0,fatu_050.situacao_nfisc),
                  fatu_050.historico_cont,
                  fatu_060.centro_custo,
                  pedi_080.modelo_doc_fisc;

  w_erro                  EXCEPTION;
  v_historico_contab cont_010.historico_contab%type;
  v_exercicio_doc  number;
  v_exercicio      number;
  v_plano_conta    number;
  v_conta_contabil cont_535.conta_contabil%type;
  v_num_cc         cont_535.cod_reduzido%type;
  w_serie_nfe      fatu_505.serie_nfe%type;
  w_modelo_especie obrf_200.modelo_documento%type;
  w_cod_especie    fatu_505.codigo_especie%type;
  w_modelo_nota    varchar2(2);

BEGIN
   p_des_erro          := NULL;

   FOR fatu_050 IN u_fatu_050 (p_cod_empresa, p_dat_inicial, p_dat_final)
   LOOP

       v_exercicio_doc := inter_fn_checa_data(p_cod_matriz, p_dat_inicial, 0);

       v_exercicio := inter_fn_checa_data(p_cod_matriz, p_dat_inicial, fatu_050.transacao);
       if v_exercicio_doc < 0
       then
          v_exercicio_doc := v_exercicio;
       end if;

       v_plano_conta := 0;
       v_conta_contabil := null;

       if v_exercicio > 0 and v_exercicio_doc > 0
       then
          v_num_cc := inter_fn_encontra_conta(p_cod_empresa, 3,
                                              fatu_050.codigo_contabil,
                                              fatu_050.transacao,
                                              fatu_050.centro_custo,
                                              v_exercicio,
                                              v_exercicio_doc);

          begin
             select cont_500.cod_plano_cta
             into   v_plano_conta
             from cont_500
             where cont_500.cod_empresa  = p_cod_matriz
               and cont_500.exercicio    = v_exercicio_doc;
          exception
             when no_data_found then
              v_plano_conta := 0;
          end;

          if v_plano_conta > 0
          then
             begin
                select cont_535.conta_contabil into   v_conta_contabil
                from cont_535
                where cont_535.cod_plano_cta = v_plano_conta
                  and cont_535.cod_reduzido  = v_num_cc;
             exception
                 when no_data_found then
                     v_conta_contabil := null;
             end;
          end if;
       end if;

       begin
          select cont_010.historico_contab into v_historico_contab
          from cont_010
          where cont_010.codigo_historico = fatu_050.historico_cont;
       exception
          when no_data_found then
             v_historico_contab := null;
       end;

      -- le o tipo de titulo da serie de nota fiscal para poder encontrar os titulos relacionados a nota
      begin
         select fatu_505.serie_nfe, trim(fatu_505.codigo_especie)
         into   w_serie_nfe,        w_cod_especie
         from fatu_505
         where fatu_505.codigo_empresa = p_cod_empresa
           and trim(fatu_505.serie_nota_fisc) = trim(fatu_050.serie);

       exception
          when no_data_found then
             w_serie_nfe    := null;
             w_cod_especie  := null;
       end;


       if w_serie_nfe = 'S'
       then
          fatu_050.modelo_doc_fisc := '55';
       else
          if w_cod_especie is not null and w_serie_nfe = 'S'
          then
             begin
                select trim(obrf_200.modelo_documento)
                  into w_modelo_especie
                from obrf_200
                where obrf_200.codigo_especie  = w_cod_especie;
             EXCEPTION
                WHEN OTHERS THEN
                   w_modelo_especie := null;
             END;
          end if;
       end if;

       if w_modelo_especie is not null
       then
          if Nvl(Length(w_modelo_especie),0) > 2
          then
              w_modelo_especie := substr(w_modelo_especie,1,2);
          end if;

          fatu_050.modelo_doc_fisc := w_modelo_especie;
       end if;

       if fatu_050.modelo_doc_fisc is null or fatu_050.modelo_doc_fisc = '00'
       then
          w_modelo_nota := '01';
       else
          w_modelo_nota := fatu_050.modelo_doc_fisc;
       end if;

      BEGIN
      insert into sped_pc_1900
      (
        dat_apuracao,
        cod_matriz,
        cod_empresa,

        num_cnpj9_empr,
        num_cnpj4_empr,
        num_cnpj2_empr,

        num_cfop,
        cst,
        perc_pis,

        perc_cofins,
        serie_nf,
        valor_nf,

        situacao_nf,
        inf_compl,
        conta_contabil,
        cod_modelo
      )
      values
      (
        p_dat_inicial   ,
        p_cod_matriz    ,
        p_cod_empresa   ,

        p_cnpj9_empresa ,
        p_cnpj4_empresa ,
        p_cnpj2_empresa ,

        fatu_050.cfop,
        fatu_050.cst,
        fatu_050.perc_pis,

        fatu_050.perc_cofins,
        fatu_050.serie,
        fatu_050.vlr_contabil,

        fatu_050.situacao_nfisc,
        v_historico_contab,
        v_num_cc,
        w_modelo_nota
      );
      EXCEPTION
      WHEN OTHERS THEN
           p_des_erro := 'Erro na inclusao da tabela sped_pc_1900 ' || Chr(10) || SQLERRM;
           RAISE W_ERRO;
      END;

      COMMIT;

   END LOOP;

   EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_gera_sped_pc_1900 ' || Chr(10) || p_des_erro;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure inter_pr_gera_sped_pc_1900 ' || Chr(10) || SQLERRM;

end inter_pr_gera_sped_1900_pc;
/
exec inter_pr_recompile;

