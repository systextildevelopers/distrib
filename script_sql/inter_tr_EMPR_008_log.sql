create or replace trigger inter_tr_EMPR_008_log 
after insert or delete or update 
on EMPR_008 
for each row 
declare 
   ws_usuario_rede           varchar2(250) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ;
 
 
begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
 
 
   v_nome_programa :=  inter_fn_nome_programa(ws_sid); 

if inserting 
then 
    begin 
        insert into EMPR_008_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           CODIGO_EMPRESA_OLD,   /*8*/ 
           CODIGO_EMPRESA_NEW,   /*9*/ 
           PARAM_OLD,   /*10*/ 
           PARAM_NEW,   /*11*/ 
           VAL_STR_OLD,   /*12*/ 
           VAL_STR_NEW,   /*13*/ 
           VAL_INT_OLD,   /*14*/ 
           VAL_INT_NEW,   /*15*/ 
           VAL_DBL_OLD,   /*16*/ 
           VAL_DBL_NEW,   /*17*/ 
           VAL_DAT_OLD,   /*18*/ 
           VAL_DAT_NEW    /*19*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.CODIGO_EMPRESA, /*9*/   
           '',/*10*/
           :new.PARAM, /*11*/   
           '',/*12*/
           :new.VAL_STR, /*13*/   
           0,/*14*/
           :new.VAL_INT, /*15*/   
           0,/*16*/
           :new.VAL_DBL, /*17*/   
           null,/*18*/
           :new.VAL_DAT /*19*/   
         );    
    end;    
end if;    

if updating 
then 
    begin 
        insert into EMPR_008_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           CODIGO_EMPRESA_OLD, /*8*/   
           CODIGO_EMPRESA_NEW, /*9*/   
           PARAM_OLD, /*10*/   
           PARAM_NEW, /*11*/   
           VAL_STR_OLD, /*12*/   
           VAL_STR_NEW, /*13*/   
           VAL_INT_OLD, /*14*/   
           VAL_INT_NEW, /*15*/   
           VAL_DBL_OLD, /*16*/   
           VAL_DBL_NEW, /*17*/   
           VAL_DAT_OLD, /*18*/   
           VAL_DAT_NEW  /*19*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.CODIGO_EMPRESA,  /*8*/  
           :new.CODIGO_EMPRESA, /*9*/   
           :old.PARAM,  /*10*/  
           :new.PARAM, /*11*/   
           :old.VAL_STR,  /*12*/  
           :new.VAL_STR, /*13*/   
           :old.VAL_INT,  /*14*/  
           :new.VAL_INT, /*15*/   
           :old.VAL_DBL,  /*16*/  
           :new.VAL_DBL, /*17*/   
           :old.VAL_DAT,  /*18*/  
           :new.VAL_DAT  /*19*/  
         );    
    end;    
end if;    

if deleting 
then 
    begin 
        insert into EMPR_008_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           CODIGO_EMPRESA_OLD, /*8*/   
           CODIGO_EMPRESA_NEW, /*9*/   
           PARAM_OLD, /*10*/   
           PARAM_NEW, /*11*/   
           VAL_STR_OLD, /*12*/   
           VAL_STR_NEW, /*13*/   
           VAL_INT_OLD, /*14*/   
           VAL_INT_NEW, /*15*/   
           VAL_DBL_OLD, /*16*/   
           VAL_DBL_NEW, /*17*/   
           VAL_DAT_OLD, /*18*/   
           VAL_DAT_NEW /*19*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.CODIGO_EMPRESA, /*8*/   
           0, /*9*/
           :old.PARAM, /*10*/   
           '', /*11*/
           :old.VAL_STR, /*12*/   
           '', /*13*/
           :old.VAL_INT, /*14*/   
           0, /*15*/
           :old.VAL_DBL, /*16*/   
           0, /*17*/
           :old.VAL_DAT, /*18*/   
           null /*19*/
         );    
    end;    
end if;    
end inter_tr_EMPR_008_log;
