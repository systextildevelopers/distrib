CREATE OR REPLACE TRIGGER INTER_TR_PEDI_100_REPR
AFTER INSERT 
  or update of COD_REP_CLIENTE, CODIGO_EMPRESA on pedi_100
for each row

declare
  v_executa_trigger       number(1);
  v_empresa_representante number(3);
  v_representante_empr    number(1);
  
begin

  if inserting
  then
    if :new.executa_trigger = 1
    then v_executa_trigger := 1;
    else v_executa_trigger := 0;
    end if;
  end if;

  if updating
  then
    if :old.executa_trigger = 1 or :new.executa_trigger = 1
    then v_executa_trigger := 1;
    else v_executa_trigger := 0;
    end if;
  end if;

  if deleting
  then
    if :old.executa_trigger = 1
    then v_executa_trigger := 1;
    else v_executa_trigger := 0;
    end if;
  end if;

  if v_executa_trigger = 0
  then
    select pedi_020.cod_empresa 
    into v_empresa_representante
    from pedi_020
    where pedi_020.cod_rep_cliente = :new.cod_rep_cliente;

    select empr_002.representante_empr 
    into v_representante_empr 
    from empr_002;

    if v_empresa_representante != 0 and v_representante_empr = 1 and v_empresa_representante <> :new.codigo_empresa
    then 
      raise_application_error(-20000,'ATEN��O! Representante n�o cadastrado para a empresa do pedido de venda!');
    end if;
  end if;
end inter_tr_pedi_100_repr;
/

exec inter_pr_recompile;
