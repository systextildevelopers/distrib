ALTER TABLE OBRF_130
ADD(NUM_DA VARCHAR2(50) default ' ');
/

ALTER TABLE OBRF_141
ADD (QTDE                 NUMBER(13,5) default 0 not null,
     UNID                 VARCHAR2(6) default ' ' not null,
     VL_ICMS_OP_APLICADO  NUMBER(13,2) default 0 not null,
     VL_ICMS_ST_APLICADO  NUMBER(13,2) default 0 not null,
     VL_ICMS_FRT_APLICADO NUMBER(13,2) default 0 not null,
     VL_ICMS_DIF_APLICADO NUMBER(13,2) default 0 not null);
/

ALTER TABLE FATU_502
ADD (CLASSIFICACAO_ESTAB_IND  NUMBER(2) default 0);

/

exec inter_pr_recompile;
