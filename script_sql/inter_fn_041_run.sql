CREATE OR REPLACE FUNCTION "INTER_FN_041_RUN" (p_data_inicio_programacao date,   p_data_final_programacao date,    p_codigo_usuario number,
                                            p_codigo_solicitacao number,       p_codigo_empresa number,         p_usuario varchar2,
                                            p_opcao_geracao number,            p_periodo_producao number,       p_gerar_com_data varchar2,
                                            p_lotes_tinturaria number,         p_lotes_fundo number,
                                            p_solicitacao_necessidades number, p_opcao_estampados number,       p_forma_programacao number,
                                            p_antecipacao_tinturaria number,   p_antecipacao_estamparia number, p_antecipacao_fundo number,
                                            p_modalidade_planificacao number,  p_percentual_atendimento number)
return number is

v_data_analise           date;
v_data_termino           date;
v_hora_termino           date;
v_data_hora_termino      date;
v_contador               number;
v_codigo_recurso         varchar2(15);

v_contador_ordens_unir   number;
v_contador_ordens_geral  number;
v_lote_acumulado         number;
v_ob                     number;
v_data_calculada_termino date;
v_controle_erros         number;
v_tecido_encontrado      number;

v_pedido_venda           number;
v_seq_item_pedido        number;
v_nome_cliente           varchar2(100);
v_categoria              number;
v_nivel                  varchar2(1);
v_grupo                  varchar2(5);
v_subgrupo               varchar2(3);
v_item                   varchar2(6);
v_tonalidade             varchar2(2);
v_cor_formulario         varchar2(15);
v_tipo_servico           number;
v_codigo_deposito        number;
v_transacao              number;
v_alternativa            number;
v_roteiro                number;
v_peso_padrao_rolo       number;
v_largura                number;
v_gramatura              number;
v_rendimento             number;
v_codigo_acomp           number;
v_sequencia_principal    number;
v_tipo_tecido            number;
v_tipo_natureza          number;
v_grupo_tingimento       number;
v_consumo                number;
v_origem_tempo           number;
v_data_entrega           date;
v_tempo_tingimento       number;
v_saldo                  number;
v_capacidade_maxima      number;
v_capacidade_minima      number;
v_ob_alocada             number;
v_continua_1             boolean;
v_aplica_antecipacao     boolean;
v_antecipacao            number;
v_hora_retorno           varchar2(2);
v_minutos_retorno        varchar2(2);
v_saldo_complementar     number;
v_sequencia_log          number;
v_analisar_lacuna        boolean;
v_tentativa              number;
v_analisar_maquina       boolean;
v_existem_lacunas        number;
v_ultima_sequencia       number;

type v_arr is table of number index by binary_integer;

ordens_beneficiamento_unir v_arr;
ordens_beneficiamento_geral v_arr;
array_vazio v_arr;

--------------------------------------------------------------------------------------------------------------------------------------------------

function registrar_excecao(p_mensagem varchar2,p_tabela varchar2,p_acao number)
return number is
  v_controle_erros number;
begin
   begin
      insert into tmrp_615
        (tmrp_615.criticas,                tmrp_615.nome_cliente,
         tmrp_615.selecionado,

         tmrp_615.nome_programa,           tmrp_615.nr_solicitacao,
         tmrp_615.codigo_usuario,          tmrp_615.tipo_registro,
         tmrp_615.codigo_empresa,          tmrp_615.usuario)
      values
        (p_mensagem,                       p_tabela,
         p_acao,

         'pcpb_f041',                      p_codigo_solicitacao,
         p_codigo_usuario,                 500, -- excecoes
         p_codigo_empresa,                 p_usuario);
      exception when others
      then v_controle_erros := registrar_excecao(sqlerrm,'TMRP_615',0);
   end;

   return (v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function atualizar_qtde_programacao(p_pedido_venda number,p_seq_item_pedido number,p_categoria number,p_qtde number)
return number is
   v_controle_erros number;
begin
   begin
      update tmrp_615
         set tmrp_615.qtde_programacao = tmrp_615.qtde_programacao + p_qtde,
             tmrp_615.seq_oc           = tmrp_615.seq_oc + 1
      where tmrp_615.pedido_venda        = p_pedido_venda
        and tmrp_615.seq_item_pedido     = p_seq_item_pedido
        and tmrp_615.categoria           = p_categoria
        and tmrp_615.nome_programa       = 'pcpb_f041'
        and tmrp_615.codigo_usuario      = p_codigo_usuario
        and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
        and tmrp_615.codigo_empresa      = p_codigo_empresa
        and tmrp_615.usuario             = p_usuario
        and tmrp_615.tipo_registro       = 100;
      exception when others
      then v_controle_erros := registrar_excecao(sqlerrm,'TMRP_615',1);
   end;

   return (v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function registrar_ordem(p_ot number,                 p_ob number,             p_grupo_maquina varchar2,
                         p_subgrupo_maquina varchar2, p_numero_maquina number, p_nivel varchar2,
                         p_grupo varchar2,            p_subgrupo varchar2,     p_item varchar2,
                         p_alternativa number,        p_roteiro number,        p_pedido_venda number,
                         p_seq_item_pedido number,    p_qtde_lote number)
return number is
   v_controle_erros number;
begin
   begin
      insert into tmrp_615
        (tmrp_615.ordem_planejamento,      tmrp_615.ordem_prod,
         tmrp_615.grupo_maquina,           tmrp_615.subgrupo_maquina,
         tmrp_615.numero_maquina,
         tmrp_615.nivel,                   tmrp_615.grupo,
         tmrp_615.subgrupo,                tmrp_615.item,
         tmrp_615.alternativa,             tmrp_615.roteiro,

         tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
         tmrp_615.qtde_programacao,

         tmrp_615.nome_programa,           tmrp_615.nr_solicitacao,
         tmrp_615.codigo_usuario,          tmrp_615.tipo_registro,
         tmrp_615.codigo_empresa,          tmrp_615.usuario)
      values
        (p_ot,                             p_ob,
         p_grupo_maquina,                  p_subgrupo_maquina,
         p_numero_maquina,
         p_nivel,                          p_grupo,
         p_subgrupo,                       p_item,
         p_alternativa,                    p_roteiro,

         p_pedido_venda,                   p_seq_item_pedido,
         p_qtde_lote,

         'pcpb_f041',                      p_codigo_solicitacao,
         p_codigo_usuario,                 700, -- ordens geradas
         p_codigo_empresa,                 p_usuario);
      exception when others
      then v_controle_erros := registrar_excecao(sqlerrm,'TMRP_615',0);
   end;

   return (v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function get_numero_ob(p_opcao in number)
return number is

v_ob number;
v_ultimo_numero_empr_001 number;

begin
   if p_opcao = 2 -- confirmada
   then
      begin
         select empr_001.ordem_benefic
         into v_ob
         from empr_001;
         exception when others
         then v_ob := 0;
      end;

      if v_ob > 0
      then
         v_ob := v_ob + 1;

         if v_ob = v_ob_alocada
         then
            v_ob := v_ob + 1;
         end if;

         begin
            update empr_001 set ordem_benefic = v_ob;
         end;
      end if;
   else -- simulacao
      begin
         select empr_001.ordem_benefic
         into v_ultimo_numero_empr_001
         from empr_001;
         exception when others
         then v_ultimo_numero_empr_001 := 0;
      end;

      if v_ultimo_numero_empr_001 > 0
      then
         begin
            select nvl(min(pcpb_010.ordem_producao),999999999)
            into v_ob
            from pcpb_010
            where pcpb_010.ordem_producao > v_ultimo_numero_empr_001;
            exception when others
            then v_ob := 0;
         end;
      end if;

      v_ob := v_ob - 1;

      if v_ob = v_ob_alocada
      then
         v_ob := v_ob - 1;
      end if;
   end if;

   return (v_ob);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function gerar_solicitacao_necessidades(p_numero_solicitacao number, p_grupo_maquina varchar2,
                                        p_subgrupo_maquina varchar2, p_numero_maquina number,
                                        p_nivel varchar2,            p_grupo varchar2,        p_subgrupo varchar2,
                                        p_item varchar2,             p_alternativa number,    p_roteiro number,
                                        p_qtde_lote number)
return number is
   v_existe_registro number;
   v_controle_erros number;
begin
   v_existe_registro := 1;
   v_controle_erros := 0;

   begin
      select 1
      into v_existe_registro
      from rcnb_020
      where rcnb_020.nr_solicitacao   = p_numero_solicitacao
        and rcnb_020.nivel_estrutura  = p_nivel
        and rcnb_020.grupo_estrutura  = p_grupo
        and rcnb_020.subgru_estrutura = p_subgrupo
        and rcnb_020.item_estrutura   = p_item
        and rcnb_020.alternativa      = p_alternativa
        and rcnb_020.roteiro          = p_roteiro
        and rcnb_020.grupo_maquina    = p_grupo_maquina
        and rcnb_020.subgrupo_maquina = p_subgrupo_maquina
        and rcnb_020.numero_maquina   = p_numero_maquina;
      exception when others
      then v_existe_registro := 0;
   end;

   if v_existe_registro = 1
   then
      begin
         update rcnb_020
         set rcnb_020.qtde_nec_brutas = rcnb_020.qtde_nec_brutas + p_qtde_lote
         where rcnb_020.nr_solicitacao   = p_numero_solicitacao
           and rcnb_020.nivel_estrutura  = p_nivel
           and rcnb_020.grupo_estrutura  = p_grupo
           and rcnb_020.subgru_estrutura = p_subgrupo
           and rcnb_020.item_estrutura   = p_item
           and rcnb_020.alternativa      = p_alternativa
           and rcnb_020.roteiro          = p_roteiro
           and rcnb_020.grupo_maquina    = p_grupo_maquina
           and rcnb_020.subgrupo_maquina = p_subgrupo_maquina
           and rcnb_020.numero_maquina   = p_numero_maquina;
         exception when others
         then v_controle_erros := registrar_excecao(sqlerrm,'RCNB_020',1);
      end;
   else
      begin
         insert into rcnb_020
           (nr_solicitacao,       descricao,
            nivel_estrutura,      grupo_estrutura,
            subgru_estrutura,     item_estrutura,
            alternativa,          roteiro,
            qtde_nec_brutas,
            grupo_maquina,        subgrupo_maquina,
            numero_maquina)
         values
           (p_numero_solicitacao, 'pcpb_f041: ' || to_char(sysdate, 'dd/mm/yyyy hh24:mi'),
            p_nivel,              p_grupo,
            p_subgrupo,           p_item,
            p_alternativa,        p_roteiro,
            p_qtde_lote,
            p_grupo_maquina,      p_subgrupo_maquina,
            p_numero_maquina);
         exception when others
         then v_controle_erros := registrar_excecao(sqlerrm,'RCNB_020',0);
      end;
   end if;

   return(v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function aplicar_percentual_antecipacao(p_percentual number,p_peso_rolo number,p_codigo_deposito number,p_transacao number)
return number is
   v_i              number;
   v_controle_erros number;

begin
   for v_i in ordens_beneficiamento_unir.first..ordens_beneficiamento_unir.last
   loop
      if ordens_beneficiamento_unir.exists(v_i)
      then
         begin
            update pcpb_010
            set pcpb_010.qtde_quilos_prog = pcpb_010.qtde_quilos_prog + (pcpb_010.qtde_quilos_prog * (p_percentual / 100)),
                pcpb_010.qtde_rolos_prog = (pcpb_010.qtde_quilos_prog + (pcpb_010.qtde_quilos_prog * (p_percentual / 100))) / decode(p_peso_rolo,0,1,p_peso_rolo)
            where pcpb_010.ordem_producao = ordens_beneficiamento_unir(v_i);
            exception when others
            then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_010',1);
         end;

         for reg_tecidos in (select pcpb_020.pano_sbg_nivel99 nivel,pcpb_020.pano_sbg_grupo grupo,pcpb_020.pano_sbg_subgrupo subgrupo,pcpb_020.pano_sbg_item item,
                                    pcpb_020.alternativa_item alternativa,pcpb_020.roteiro_opcional roteiro,pcpb_020.sequencia,
                                    pcpb_020.qtde_quilos_prog
                             from pcpb_020
                             where pcpb_020.ordem_producao = ordens_beneficiamento_unir(v_i))
         loop
            begin
               update pcpb_020
               set pcpb_020.qtde_quilos_prog = reg_tecidos.qtde_quilos_prog + (reg_tecidos.qtde_quilos_prog * (p_percentual / 100)),
                   pcpb_020.qtde_rolos_prog = (reg_tecidos.qtde_quilos_prog + (reg_tecidos.qtde_quilos_prog * (p_percentual / 100))) / decode(p_peso_rolo,0,1,p_peso_rolo)
               where pcpb_020.ordem_producao = ordens_beneficiamento_unir(v_i)
                 and pcpb_020.sequencia      = reg_tecidos.sequencia;
               exception when others
               then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_020',1);
            end;

            begin
               INSERT into pcpb_030
                 (ordem_producao,                   pano_nivel99,
                  pano_grupo,                       pano_subgrupo,
                  pano_item,                        codigo_deposito,
                  alternativa,                      roteiro,
                  pedido_corte,
                  sequenci_periodo,
                  nr_pedido_ordem,
                  periodo_producao,
                  seq_ordem_prod,                   qtde_quilos_prog,
                  qtde_rolos_prog,
                  sequencia,
                  codigo_acomp,                     lote_acomp,
                  codigo_transacao)
               VALUES
                 (ordens_beneficiamento_unir(v_i),  reg_tecidos.nivel,
                  reg_tecidos.grupo,                reg_tecidos.subgrupo,
                  reg_tecidos.item,                 p_codigo_deposito,
                  reg_tecidos.alternativa,          reg_tecidos.roteiro,
                  1,
                  0,
                  0,
                  p_periodo_producao,
                  0,                                reg_tecidos.qtde_quilos_prog * (p_percentual / 100),
                  ceil(reg_tecidos.qtde_quilos_prog * (p_percentual / 100) / decode(p_peso_rolo,0,1,p_peso_rolo)),
                  reg_tecidos.sequencia,
                  0,                                0,
                  p_transacao);
               exception when others
               then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_030',0);
            end;
         end loop;
      end if;
   end loop;

   return(v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function gerar_ob(p_ob number,                 p_data_final date,       p_grupo_maquina varchar2,
                  p_subgrupo_maquina varchar2, p_numero_maquina number, p_cor_formulario varchar2,
                  p_tipo_servico number,       p_pedido_venda number,   p_seq_item_pedido number,
                  p_nivel varchar2,            p_grupo varchar2,        p_subgrupo varchar2,
                  p_item varchar2,             p_alternativa number,    p_roteiro number,
                  p_qtde_lote number,          p_peso_rolo number,      p_largura number,
                  p_gramatura number,          p_rendimento number,     p_codigo_deposito number,
                  p_transacao number,          p_codigo_acomp number,   p_sequencia_principal number,
                  p_ordem_estampado number,    p_categoria number)
return number is
   v_receita number;
   v_sequencia_pcpb_020 number;
   v_existe_pcpb_010 number;
   v_controle_erros number;
begin
   if p_categoria <> 1
   then
      v_lote_acumulado := v_lote_acumulado + p_qtde_lote;
   end if;

   begin
      select 1
      into v_existe_pcpb_010
      from pcpb_010
      where pcpb_010.ordem_producao = p_ob;
      exception when others
      then v_existe_pcpb_010 := 0;
   end;

   if v_existe_pcpb_010 = 0
   then
      begin
         select nvl(max(pcpb_010.numero_receita),0)
         into v_receita
         from pcpb_010;
         exception when others
         then v_receita := 0;
      end;

      v_receita := v_receita + 1;

      begin
         insert into pcpb_010
           (periodo_producao,             ordem_tingimento,
            data_programa,                previsao_termino,
            grupo_maquina,                subgrupo_maquina,
            numero_maquina,               numero_receita,
            relacao_banho,                qtde_quilos_prog,
            ordem_producao,               nivel_estrutura,
            data_prevista_original,       situacao_ordem,
            qtde_rolos_prog,
            cor_imp_teste,
            tipo_ordem)
         values
           (p_periodo_producao,           0,
            sysdate,                      p_data_final,
            p_grupo_maquina,              p_subgrupo_maquina,
            p_numero_maquina,             v_receita,
            0,                            p_qtde_lote,
            p_ob,                         '2',
            p_data_final,                 decode(p_opcao_geracao,2,0,5),
            ceil(p_qtde_lote / decode(p_peso_rolo,0,1,p_peso_rolo)),
            p_cor_formulario,
            p_tipo_servico);
         exception when others
         then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_010',0);
      end;
   else
      begin
         update pcpb_010
            set qtde_quilos_prog        = pcpb_010.qtde_quilos_prog + p_qtde_lote,
                qtde_rolos_prog         = pcpb_010.qtde_rolos_prog + ceil(p_qtde_lote / decode(p_peso_rolo,0,1,p_peso_rolo))
         where pcpb_010.ordem_producao     = p_ob;
         exception when others
         then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_010',1);
      end;
   end if;

   begin
      select pcpb_020.sequencia
      into v_sequencia_pcpb_020
      from pcpb_020
      where pcpb_020.ordem_producao    = p_ob
        and pcpb_020.pano_sbg_nivel99  = p_nivel
        and pcpb_020.pano_sbg_grupo    = p_grupo
        and pcpb_020.pano_sbg_subgrupo = p_subgrupo
        and pcpb_020.pano_sbg_item     = p_item;
      exception when others
      then v_sequencia_pcpb_020 := 0;
   end;

   if v_sequencia_pcpb_020 > 0
   then
      begin
         update pcpb_020
            set qtde_quilos_prog        = pcpb_020.qtde_quilos_prog + p_qtde_lote,
                qtde_rolos_prog         = ceil((pcpb_020.qtde_quilos_prog + p_qtde_lote) / decode(p_peso_rolo,0,1,p_peso_rolo))
         where pcpb_020.ordem_producao     = p_ob
           and pcpb_020.pano_sbg_nivel99   = p_nivel
           and pcpb_020.pano_sbg_grupo     = p_grupo
           and pcpb_020.pano_sbg_subgrupo  = p_subgrupo
           and pcpb_020.pano_sbg_item      = p_item
           and pcpb_020.sequencia          = v_sequencia_pcpb_020;
         exception when others
         then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_020',1);
      end;
   else
      begin
         select nvl(max(pcpb_020.sequencia),0) + 1
         into v_sequencia_pcpb_020
         from pcpb_020
         where pcpb_020.ordem_producao = p_ob;
         exception when others
         then v_sequencia_pcpb_020 := 1;
      end;

      begin
         insert into pcpb_020
           (pano_sbg_nivel99,        pano_sbg_grupo,
            pano_sbg_subgrupo,       pano_sbg_item,
            alternativa_item,        roteiro_opcional,
            qtde_quilos_prog,        ordem_producao,
            qtde_rolos_prog,
            largura_tecido,
            gramatura,               rendimento,
            sequencia,               codigo_acomp,
            lote_acomp,              sequencia_principal)
         values
           (p_nivel,                 p_grupo,
            p_subgrupo,              p_item,
            p_alternativa,           p_roteiro,
            p_qtde_lote,             p_ob,
            ceil(p_qtde_lote / decode(p_peso_rolo,0,1,p_peso_rolo)),
            p_largura,
            p_gramatura,             p_rendimento,
            v_sequencia_pcpb_020,    p_codigo_acomp,
            0,                       p_sequencia_principal);
         exception when others
         then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_020',0);
      end;
   end if;

   begin
      INSERT into pcpb_030
        (ordem_producao,          pano_nivel99,
         pano_grupo,              pano_subgrupo,
         pano_item,               codigo_deposito,
         alternativa,             roteiro,
         pedido_corte,
         sequenci_periodo,
         nr_pedido_ordem,
         periodo_producao,
         seq_ordem_prod,          qtde_quilos_prog,
         qtde_rolos_prog,
         sequencia,
         codigo_acomp,            lote_acomp,
         codigo_transacao)
      VALUES
        (p_ob,                    p_nivel,
         p_grupo,                 p_subgrupo,
         p_item,                  p_codigo_deposito,
         p_alternativa,           p_roteiro,
         decode(p_ordem_estampado,0,3,7),
         decode(p_ordem_estampado,0,p_seq_item_pedido,0),
         decode(p_ordem_estampado,0,p_pedido_venda,p_ordem_estampado),
         p_periodo_producao,
         0,                       p_qtde_lote,
         ceil(p_qtde_lote / decode(p_peso_rolo,0,1,p_peso_rolo)),
         v_sequencia_pcpb_020,
         p_codigo_acomp,          0,
         p_transacao);
      exception when others
      then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_030',0);
   end;

   if p_solicitacao_necessidades > 0
   then
      v_controle_erros := gerar_solicitacao_necessidades(p_solicitacao_necessidades, p_grupo_maquina,
                                                         p_subgrupo_maquina,         p_numero_maquina,
                                                         p_nivel,                    p_grupo,          p_subgrupo,
                                                         p_item,                     p_alternativa,    p_roteiro,
                                                         p_qtde_lote);
   end if;

   v_controle_erros := registrar_ordem(0,                 p_ob,
                                       p_grupo_maquina,   p_subgrupo_maquina,
                                       p_numero_maquina,  p_nivel,
                                       p_grupo,           p_subgrupo,
                                       p_item,            p_alternativa,
                                       p_roteiro,         p_pedido_venda,
                                       p_seq_item_pedido, p_qtde_lote);

   return (v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function processar_acompanhamentos(p_ob number,p_pedido_venda number,p_seq_item_pedido number,p_categoria number,p_grupo_maquina varchar2,p_subgrupo_maquina varchar2,p_numero_maquina number,p_qtde_lote number,p_tipo_tecido number)
return number is
   v_ob number;
   v_controle_erros number;
begin
   for reg_acompanhamentos in
   (select tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
          tmrp_615.categoria,               tmrp_615.nivel,
          tmrp_615.grupo,                   tmrp_615.subgrupo,
          tmrp_615.item,                    tmrp_615.tonalidade,
          tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
          tmrp_615.codigo_deposito,         tmrp_615.transacao,
          tmrp_615.alternativa,             tmrp_615.roteiro,
          tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
          tmrp_615.gramatura,               tmrp_615.rendimento,
          tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
          tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
          p_qtde_lote * (tmrp_615.codigo_acomp/100) qtde
   from tmrp_615
   where tmrp_615.nome_programa       = 'pcpb_f041'
     and tmrp_615.codigo_usuario      = p_codigo_usuario
     and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
     and tmrp_615.codigo_empresa      = p_codigo_empresa
     and tmrp_615.usuario             = p_usuario
     and tmrp_615.tipo_registro       = 100
     and tmrp_615.pedido_venda        = p_pedido_venda
     and tmrp_615.sequencia_principal = p_seq_item_pedido
     and tmrp_615.grupo_maquina       = p_grupo_maquina
     and tmrp_615.subgrupo_maquina    = p_subgrupo_maquina
     and tmrp_615.numero_maquina      = p_numero_maquina
     and tmrp_615.categoria           = decode(p_categoria,2,0,p_categoria)
   group by tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
            tmrp_615.categoria,               tmrp_615.nivel,
            tmrp_615.grupo,                   tmrp_615.subgrupo,
            tmrp_615.item,                    tmrp_615.tonalidade,
            tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
            tmrp_615.codigo_deposito,         tmrp_615.transacao,
            tmrp_615.alternativa,             tmrp_615.roteiro,
            tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
            tmrp_615.gramatura,               tmrp_615.rendimento,
            tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
            tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
            p_qtde_lote * (tmrp_615.codigo_acomp/100)
   order by tmrp_615.pedido_venda, tmrp_615.seq_item_pedido)
   loop
      if reg_acompanhamentos.categoria = 1 /* estampado */
      then
         v_controle_erros := gerar_ob(p_ob,                                 sysdate,
                                      p_grupo_maquina,                      p_subgrupo_maquina,
                                      p_numero_maquina,                     reg_acompanhamentos.cor_formulario,
                                      reg_acompanhamentos.tipo_servico,     reg_acompanhamentos.pedido_venda,
                                      reg_acompanhamentos.seq_item_pedido,  reg_acompanhamentos.nivel,
                                      reg_acompanhamentos.grupo,            reg_acompanhamentos.subgrupo,
                                      reg_acompanhamentos.item,             reg_acompanhamentos.alternativa,
                                      reg_acompanhamentos.roteiro,          reg_acompanhamentos.qtde,
                                      reg_acompanhamentos.peso_padrao_rolo, reg_acompanhamentos.largura,
                                      reg_acompanhamentos.gramatura,        reg_acompanhamentos.rendimento,
                                      reg_acompanhamentos.codigo_deposito,  reg_acompanhamentos.transacao,
                                      reg_acompanhamentos.codigo_acomp,     reg_acompanhamentos.sequencia_principal,
                                      0,                                    reg_acompanhamentos.categoria);

         v_controle_erros := atualizar_qtde_programacao(reg_acompanhamentos.pedido_venda,reg_acompanhamentos.seq_item_pedido,reg_acompanhamentos.categoria,reg_acompanhamentos.qtde);

         /*********************************************************
         Acompanhamento estampado e gerado na mesma Ob do principal
         *********************************************************/
      else
         if reg_acompanhamentos.tipo_natureza > 0 /* pedido de servicos */
         then
            /*********************************************************
            Pedidos de servicos devem ter cada item gerado em OB sepa-
            rada, sendo agrupada na mesma OT
            *********************************************************/
            v_ob := get_numero_ob(p_opcao_geracao);
            v_contador_ordens_unir := v_contador_ordens_unir + 1;
            v_contador_ordens_geral := v_contador_ordens_geral + 1;
            ordens_beneficiamento_unir(v_contador_ordens_unir) := v_ob;
            ordens_beneficiamento_geral(v_contador_ordens_geral) := v_ob;

            v_controle_erros := gerar_ob(v_ob,                                 sysdate,
                                         p_grupo_maquina,                      p_subgrupo_maquina,
                                         p_numero_maquina,                     reg_acompanhamentos.cor_formulario,
                                         reg_acompanhamentos.tipo_servico,     reg_acompanhamentos.pedido_venda,
                                         reg_acompanhamentos.seq_item_pedido,  reg_acompanhamentos.nivel,
                                         reg_acompanhamentos.grupo,            reg_acompanhamentos.subgrupo,
                                         reg_acompanhamentos.item,             reg_acompanhamentos.alternativa,
                                         reg_acompanhamentos.roteiro,          reg_acompanhamentos.qtde,
                                         reg_acompanhamentos.peso_padrao_rolo, reg_acompanhamentos.largura,
                                         reg_acompanhamentos.gramatura,        reg_acompanhamentos.rendimento,
                                         reg_acompanhamentos.codigo_deposito,  reg_acompanhamentos.transacao,
                                         reg_acompanhamentos.codigo_acomp,     reg_acompanhamentos.sequencia_principal,
                                         0,                                    reg_acompanhamentos.categoria);

            v_controle_erros := atualizar_qtde_programacao(reg_acompanhamentos.pedido_venda,reg_acompanhamentos.seq_item_pedido,reg_acompanhamentos.categoria,reg_acompanhamentos.qtde);
         else
            if  (p_tipo_tecido = 3 or p_tipo_tecido = 4 or p_tipo_tecido = 6)
            and (reg_acompanhamentos.tipo_tecido = 3 or reg_acompanhamentos.tipo_tecido = 4 or reg_acompanhamentos.tipo_tecido = 6)
            then
               /*********************************************************
               Retilineos devem ser programados na mesma OB
               *********************************************************/
               v_controle_erros := gerar_ob(p_ob,                                 sysdate,
                                            p_grupo_maquina,                      p_subgrupo_maquina,
                                            p_numero_maquina,                     reg_acompanhamentos.cor_formulario,
                                            reg_acompanhamentos.tipo_servico,     reg_acompanhamentos.pedido_venda,
                                            reg_acompanhamentos.seq_item_pedido,  reg_acompanhamentos.nivel,
                                            reg_acompanhamentos.grupo,            reg_acompanhamentos.subgrupo,
                                            reg_acompanhamentos.item,             reg_acompanhamentos.alternativa,
                                            reg_acompanhamentos.roteiro,          reg_acompanhamentos.qtde,
                                            reg_acompanhamentos.peso_padrao_rolo, reg_acompanhamentos.largura,
                                            reg_acompanhamentos.gramatura,        reg_acompanhamentos.rendimento,
                                            reg_acompanhamentos.codigo_deposito,  reg_acompanhamentos.transacao,
                                            reg_acompanhamentos.codigo_acomp,     reg_acompanhamentos.sequencia_principal,
                                            0,                                    reg_acompanhamentos.categoria);

               v_controle_erros := atualizar_qtde_programacao(reg_acompanhamentos.pedido_venda,reg_acompanhamentos.seq_item_pedido,reg_acompanhamentos.categoria,reg_acompanhamentos.qtde);
            else
               /*********************************************************
               Nao podem ser misturados em uma mesma OB retilineos e lisos.
               *********************************************************/
               v_ob :=get_numero_ob(p_opcao_geracao);
               v_contador_ordens_unir := v_contador_ordens_unir + 1;
               v_contador_ordens_geral := v_contador_ordens_geral + 1;
               ordens_beneficiamento_unir(v_contador_ordens_unir) := v_ob;
               ordens_beneficiamento_geral(v_contador_ordens_geral) := v_ob;

               v_controle_erros := gerar_ob(v_ob,                                 sysdate,
                                            p_grupo_maquina,                      p_subgrupo_maquina,
                                            p_numero_maquina,                     reg_acompanhamentos.cor_formulario,
                                            reg_acompanhamentos.tipo_servico,     reg_acompanhamentos.pedido_venda,
                                            reg_acompanhamentos.seq_item_pedido,  reg_acompanhamentos.nivel,
                                            reg_acompanhamentos.grupo,            reg_acompanhamentos.subgrupo,
                                            reg_acompanhamentos.item,             reg_acompanhamentos.alternativa,
                                            reg_acompanhamentos.roteiro,          reg_acompanhamentos.qtde,
                                            reg_acompanhamentos.peso_padrao_rolo, reg_acompanhamentos.largura,
                                            reg_acompanhamentos.gramatura,        reg_acompanhamentos.rendimento,
                                            reg_acompanhamentos.codigo_deposito,  reg_acompanhamentos.transacao,
                                            reg_acompanhamentos.codigo_acomp,     reg_acompanhamentos.sequencia_principal,
                                            0,                                    reg_acompanhamentos.categoria);

               v_controle_erros := atualizar_qtde_programacao(reg_acompanhamentos.pedido_venda,reg_acompanhamentos.seq_item_pedido,reg_acompanhamentos.categoria,reg_acompanhamentos.qtde);
            end if;
         end if;
      end if;
   end loop;

   return(v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function get_percentual_acompanhamento(p_pedido_venda number,p_seq_item_pedido number,p_categoria number,
                                       p_grupo_maquina varchar2, p_subgrupo_maquina varchar2, p_numero_maquina number)
  return number is

  v_codigo_acomp number;

begin
   select nvl(sum(tmrp_615.codigo_acomp),0)
   into   v_codigo_acomp
   from tmrp_615
   where tmrp_615.nome_programa       = 'pcpb_f041'
     and tmrp_615.codigo_usuario      = p_codigo_usuario
     and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
     and tmrp_615.codigo_empresa      = p_codigo_empresa
     and tmrp_615.usuario             = p_usuario
     and tmrp_615.tipo_registro       = 100
     and tmrp_615.pedido_venda        = p_pedido_venda
     and tmrp_615.sequencia_principal = p_seq_item_pedido
     --and tmrp_615.categoria           = p_categoria
     and tmrp_615.grupo_maquina       = p_grupo_maquina
     and tmrp_615.subgrupo_maquina    = p_subgrupo_maquina
     and tmrp_615.numero_maquina      = p_numero_maquina;

   return v_codigo_acomp;
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

procedure programar_estampado(p_ob_fundo number,       p_data_final date,           p_pedido_venda number,
                             p_seq_item_pedido number, p_nivel varchar2,            p_grupo varchar2,
                             p_subgrupo varchar2,      p_item varchar2,             p_cor_formulario varchar2,
                             p_tipo_servico number,    p_consumo number,            p_alternativa number,
                             p_roteiro number,         p_largura number,            p_gramatura number,
                             p_grupo_maquina varchar2, p_subgrupo_maquina varchar2, p_numero_maquina number,
                             p_peso_rolo number,       p_rendimento number,         p_codigo_deposito number,
                             p_transacao number,       p_qtde number)
is
   v_ob                      number;
   v_qtde_programada_fundo   number;
   v_pedido_venda          tmrp_615.pedido_venda%type;
   v_seq_item_pedido       tmrp_615.seq_item_pedido%type;
   v_categoria             tmrp_615.categoria%type;
   v_nivel                 tmrp_615.nivel%type;
   v_grupo                 tmrp_615.grupo%type;
   v_subgrupo              tmrp_615.subgrupo%type;
   v_item                  tmrp_615.item%type;
   v_tonalidade            tmrp_615.tonalidade%type;
   v_cor_formulario_fal    tmrp_615.cor_formulario%type;
   v_tipo_servico          tmrp_615.tipo_servico%type;
   v_codigo_deposito       tmrp_615.codigo_deposito%type;
   v_transacao             tmrp_615.transacao%type;
   v_alternativa           tmrp_615.alternativa%type;
   v_roteiro               tmrp_615.roteiro%type;
   v_peso_padrao_rolo      tmrp_615.peso_padrao_rolo%type;
   v_largura               tmrp_615.largura%type;
   v_gramatura             tmrp_615.gramatura%type;
   v_rendimento            tmrp_615.rendimento%type;
   v_codigo_acomp          tmrp_615.codigo_acomp%type;
   v_sequencia_principal   tmrp_615.sequencia_principal%type;
   v_tipo_tecido           tmrp_615.tipo_tecido%type;
   v_tipo_natureza         tmrp_615.tipo_natureza%type;
   v_saldo                 tmrp_615.qtde_pedido%type;
   v_grupo_maquina         tmrp_615.grupo_maquina%type;
   v_subgrupo_maquina      tmrp_615.subgrupo_maquina%type;
   v_numero_maquina        tmrp_615.numero_maquina%type;
   v_capacidade_maxima     tmrp_615.capacidade_maxima%type;
   v_capacidade_minima     tmrp_615.capacidade_minima%type;
   v_estampado_encontrado  number;
   v_qtde_lote             number;
   v_controle_erros        number;

begin
   v_estampado_encontrado := 1;
   v_qtde_programada_fundo := 0;

   begin
      select tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
             tmrp_615.categoria,               tmrp_615.nivel,
             tmrp_615.grupo,                   tmrp_615.subgrupo,
             tmrp_615.item,                    tmrp_615.tonalidade,
             tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
             tmrp_615.codigo_deposito,         tmrp_615.transacao,
             tmrp_615.alternativa,             tmrp_615.roteiro,
             tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
             tmrp_615.gramatura,               tmrp_615.rendimento,
             tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
             tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
             p_qtde,

             tmrp_615.grupo_maquina,           tmrp_615.subgrupo_maquina,
             tmrp_615.numero_maquina,          max(tmrp_615.capacidade_maxima),
             max(tmrp_615.capacidade_minima)
      into   v_pedido_venda,                   v_seq_item_pedido,
             v_categoria,                      v_nivel,
             v_grupo,                          v_subgrupo,
             v_item,                           v_tonalidade,
             v_cor_formulario_fal,             v_tipo_servico,
             v_codigo_deposito,                v_transacao,
             v_alternativa,                    v_roteiro,
             v_peso_padrao_rolo,               v_largura,
             v_gramatura,                      v_rendimento,
             v_codigo_acomp,                   v_sequencia_principal,
             v_tipo_tecido,                    v_tipo_natureza,
             v_saldo,

             v_grupo_maquina,                  v_subgrupo_maquina,
             v_numero_maquina,                 v_capacidade_maxima,
             v_capacidade_minima
      from tmrp_615
      where tmrp_615.pedido_venda        = p_pedido_venda
        and tmrp_615.seq_item_pedido     = p_seq_item_pedido
        and tmrp_615.categoria           = 1 -- estampado
        and tmrp_615.sequencia_principal = 0 -- somente tecidos principais
        and tmrp_615.nome_programa       = 'pcpb_f041'
        and tmrp_615.codigo_usuario      = p_codigo_usuario
        and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
        and tmrp_615.codigo_empresa      = p_codigo_empresa
        and tmrp_615.usuario             = p_usuario
        and tmrp_615.tipo_registro       = 100
        and rownum                       < 2
      group by tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
               tmrp_615.categoria,               tmrp_615.nivel,
               tmrp_615.grupo,                   tmrp_615.subgrupo,
               tmrp_615.item,                    tmrp_615.tonalidade,
               tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
               tmrp_615.codigo_deposito,         tmrp_615.transacao,
               tmrp_615.alternativa,             tmrp_615.roteiro,
               tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
               tmrp_615.gramatura,               tmrp_615.rendimento,
               tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
               tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
               tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao,

               tmrp_615.grupo_maquina,           tmrp_615.subgrupo_maquina,
               tmrp_615.numero_maquina;
      exception when others
      then v_estampado_encontrado := 0;
   end;

   if v_estampado_encontrado = 1
   then
      while v_saldo > v_capacidade_maxima
      loop
         v_qtde_lote := v_capacidade_maxima;
         v_saldo := v_saldo - v_capacidade_maxima;

         v_ob := get_numero_ob(p_opcao_geracao);

         v_contador_ordens_geral := v_contador_ordens_geral + 1;
         ordens_beneficiamento_geral(v_contador_ordens_geral) := v_ob;

         /********************************
         Gera a OB para o estampado...
         ********************************/
         v_controle_erros := gerar_ob(v_ob,               sysdate,
                                      v_grupo_maquina,    v_subgrupo_maquina,
                                      v_numero_maquina,   v_cor_formulario_fal,
                                      v_tipo_servico,     v_pedido_venda,
                                      v_seq_item_pedido,  v_nivel,
                                      v_grupo,            v_subgrupo,
                                      v_item,             v_alternativa,
                                      v_roteiro,          v_qtde_lote,
                                      v_peso_padrao_rolo, v_largura,
                                      v_gramatura,        v_rendimento,
                                      v_codigo_deposito,  v_transacao,
                                      0,                  0,
                                      0,                  1);

         v_controle_erros := atualizar_qtde_programacao(v_pedido_venda,v_seq_item_pedido,1,v_qtde_lote);

         v_controle_erros := processar_acompanhamentos(v_ob,v_pedido_venda,v_seq_item_pedido,v_categoria,v_grupo_maquina,v_subgrupo_maquina,v_numero_maquina,v_qtde_lote,v_tipo_tecido);

         /********************************
         ... e para o fundo
         ********************************/
         v_controle_erros := gerar_ob(p_ob_fundo,         p_data_final,
                                      p_grupo_maquina,    p_subgrupo_maquina,
                                      p_numero_maquina,   p_cor_formulario,
                                      p_tipo_servico,     p_pedido_venda,
                                      p_seq_item_pedido,  p_nivel,
                                      p_grupo,            p_subgrupo,
                                      p_item,             p_alternativa,
                                      p_roteiro,          v_qtde_lote * p_consumo,
                                      p_peso_rolo,        p_largura,
                                      p_gramatura,        p_rendimento,
                                      p_codigo_deposito,  p_transacao,
                                      0,                  0,
                                      v_ob,               2);

         v_controle_erros := atualizar_qtde_programacao(p_pedido_venda,p_seq_item_pedido,2,v_qtde_lote * p_consumo);

         v_controle_erros := processar_acompanhamentos(p_ob_fundo,p_pedido_venda,p_seq_item_pedido,2 /* categoria */,p_grupo_maquina,p_subgrupo_maquina,p_numero_maquina,v_qtde_lote * p_consumo,v_tipo_tecido);

         v_qtde_programada_fundo := v_qtde_programada_fundo + (v_qtde_lote * p_consumo);

         v_qtde_programada_fundo := v_qtde_programada_fundo + (v_qtde_programada_fundo * (get_percentual_acompanhamento(p_pedido_venda,p_seq_item_pedido /* categoria */,0,p_grupo_maquina, p_subgrupo_maquina, p_numero_maquina) / 100));
      end loop;

      if v_saldo > 0
      then
         /********************************
         Gera a OB para o estampado...
         ********************************/
         v_ob := get_numero_ob(p_opcao_geracao);

         v_contador_ordens_geral := v_contador_ordens_geral + 1;
         ordens_beneficiamento_geral(v_contador_ordens_geral) := v_ob;

         v_controle_erros := gerar_ob(v_ob,                 sysdate,
                                      v_grupo_maquina,      v_subgrupo_maquina,
                                      v_numero_maquina,     v_cor_formulario_fal,
                                      v_tipo_servico,       v_pedido_venda,
                                      v_seq_item_pedido,    v_nivel,
                                      v_grupo,              v_subgrupo,
                                      v_item,               v_alternativa,
                                      v_roteiro,            v_saldo,
                                      v_peso_padrao_rolo,   v_largura,
                                      v_gramatura,          v_rendimento,
                                      v_codigo_deposito,    v_transacao,
                                      0,                    0,
                                      0,                    1);

         v_controle_erros := atualizar_qtde_programacao(v_pedido_venda,v_seq_item_pedido,1,v_saldo);

         v_controle_erros := processar_acompanhamentos(v_ob,v_pedido_venda,v_seq_item_pedido,v_categoria,v_grupo_maquina,v_subgrupo_maquina,v_numero_maquina,v_saldo,v_tipo_tecido);

         /********************************
         ... e para o fundo
         ********************************/
         v_controle_erros := gerar_ob(p_ob_fundo,         p_data_final,
                                      p_grupo_maquina,    p_subgrupo_maquina,
                                      p_numero_maquina,   p_cor_formulario,
                                      p_tipo_servico,     p_pedido_venda,
                                      p_seq_item_pedido,  p_nivel,
                                      p_grupo,            p_subgrupo,
                                      p_item,             p_alternativa,
                                      p_roteiro,          v_saldo * p_consumo,
                                      p_peso_rolo,        p_largura,
                                      p_gramatura,        p_rendimento,
                                      p_codigo_deposito,  p_transacao,
                                      0,                  0,
                                      v_ob,               2);

         v_controle_erros := atualizar_qtde_programacao(p_pedido_venda,p_seq_item_pedido,2,v_saldo * p_consumo);

         v_controle_erros := processar_acompanhamentos(p_ob_fundo,p_pedido_venda,p_seq_item_pedido,2 /* categoria */,p_grupo_maquina,p_subgrupo_maquina,p_numero_maquina,v_saldo * p_consumo,v_tipo_tecido);

         v_qtde_programada_fundo := v_qtde_programada_fundo + (v_saldo * p_consumo);

         v_qtde_programada_fundo := v_qtde_programada_fundo + (v_qtde_programada_fundo * (get_percentual_acompanhamento(p_pedido_venda,p_seq_item_pedido /* categoria */,0,p_grupo_maquina, p_subgrupo_maquina, p_numero_maquina) / 100));
      end if;
   end if;
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function processar_tecidos(p_grupo_maquina       varchar2, p_subgrupo_maquina varchar2,
                           p_numero_maquina      number,   p_pedido_venda     number,
                           p_seq_item_pedido     number,   p_categoria        number,
                           p_nivel               varchar2, p_grupo            varchar2,
                           p_subgrupo            varchar2, p_item             varchar2,
                           p_alternativa         number,   p_roteiro          number,
                           p_qtde                number,   p_peso_rolo        number,
                           p_largura             number,   p_gramatura        number,
                           p_rendimento          number,   p_codigo_deposito  number,
                           p_transacao           number,   p_codigo_acomp     number,
                           p_sequencia_principal number,   p_cor_formulario   varchar2,
                           p_tipo_servico        number,   p_tipo_tecido      number,
                           p_consumo             number)
return number is
   v_controle_erros number;
begin
   if p_categoria = 0
   then
      v_controle_erros := gerar_ob(v_ob,              v_data_calculada_termino,
                                   p_grupo_maquina,   p_subgrupo_maquina,
                                   p_numero_maquina,  p_cor_formulario,
                                   p_tipo_servico,    p_pedido_venda,
                                   p_seq_item_pedido, p_nivel,
                                   p_grupo,           p_subgrupo,
                                   p_item,            p_alternativa,
                                   p_roteiro,         p_qtde,
                                   p_peso_rolo,       p_largura,
                                   p_gramatura,       p_rendimento,
                                   p_codigo_deposito, p_transacao,
                                   p_codigo_acomp,    p_sequencia_principal,
                                   0,                 0);

      v_controle_erros := atualizar_qtde_programacao(p_pedido_venda,p_seq_item_pedido,p_categoria,p_qtde);
      v_controle_erros := processar_acompanhamentos(v_ob,p_pedido_venda,p_seq_item_pedido,p_categoria,p_grupo_maquina,p_subgrupo_maquina,p_numero_maquina,p_qtde,p_tipo_tecido);
   else
      /********************************************
      Quando tratar-se de um fundo de estampado,
      deve-se programar primeiro o tecido estampado
      em si.
      ********************************************/
      v_ob_alocada := v_ob;

      programar_estampado(v_ob,                 v_data_calculada_termino,
                          p_pedido_venda,       p_seq_item_pedido,
                          p_nivel,              p_grupo,
                          p_subgrupo,           p_item,
                          p_cor_formulario,     p_tipo_servico,
                          p_consumo,            p_alternativa,
                          p_roteiro,            p_largura,
                          p_gramatura,          p_grupo_maquina,
                          p_subgrupo_maquina,   p_numero_maquina,
                          p_peso_rolo,          p_rendimento,
                          p_codigo_deposito,    p_transacao,
                          p_qtde / p_consumo);
   end if;

   return(v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function processar_estampado_simples
return number is

   v_qtde_lote number;
   v_saldo number;
   v_ob number;
   v_controle_erros number;

begin
   for reg_tecidos_estampados in (select tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
                                         tmrp_615.categoria,               tmrp_615.nivel,
                                         tmrp_615.grupo,                   tmrp_615.subgrupo,
                                         tmrp_615.item,                    tmrp_615.tonalidade,
                                         tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
                                         tmrp_615.codigo_deposito,         tmrp_615.transacao,
                                         tmrp_615.alternativa,             tmrp_615.roteiro,
                                         tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
                                         tmrp_615.gramatura,               tmrp_615.rendimento,
                                         tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
                                         tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
                                         tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao saldo,

                                         tmrp_615.grupo_maquina,           tmrp_615.subgrupo_maquina,
                                         tmrp_615.numero_maquina,          max(tmrp_615.capacidade_maxima) capacidade_maxima,
                                         max(tmrp_615.capacidade_minima) capacidade_minima
                                  from tmrp_615
                                  where tmrp_615.categoria           = 1 -- estampado
                                    and tmrp_615.sequencia_principal = 0 -- somente tecidos principais
                                    and tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao > 0
                                    and tmrp_615.nome_programa       = 'pcpb_f041'
                                    and tmrp_615.codigo_usuario      = p_codigo_usuario
                                    and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                                    and tmrp_615.codigo_empresa      = p_codigo_empresa
                                    and tmrp_615.usuario             = p_usuario
                                    and tmrp_615.tipo_registro       = 100
                                  group by tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
                                           tmrp_615.categoria,               tmrp_615.nivel,
                                           tmrp_615.grupo,                   tmrp_615.subgrupo,
                                           tmrp_615.item,                    tmrp_615.tonalidade,
                                           tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
                                           tmrp_615.codigo_deposito,         tmrp_615.transacao,
                                           tmrp_615.alternativa,             tmrp_615.roteiro,
                                           tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
                                           tmrp_615.gramatura,               tmrp_615.rendimento,
                                           tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
                                           tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
                                           tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao,

                                           tmrp_615.grupo_maquina,           tmrp_615.subgrupo_maquina,
                                           tmrp_615.numero_maquina)
   loop
      v_saldo := reg_tecidos_estampados.saldo;

      while v_saldo > reg_tecidos_estampados.capacidade_maxima
      loop
         v_qtde_lote := reg_tecidos_estampados.capacidade_maxima;
         v_saldo := v_saldo - reg_tecidos_estampados.capacidade_maxima;

         v_ob := get_numero_ob(p_opcao_geracao);

         v_controle_erros := gerar_ob(v_ob,                                    sysdate,
                                      reg_tecidos_estampados.grupo_maquina,    reg_tecidos_estampados.subgrupo_maquina,
                                      reg_tecidos_estampados.numero_maquina,   reg_tecidos_estampados.cor_formulario,
                                      reg_tecidos_estampados.tipo_servico,     reg_tecidos_estampados.pedido_venda,
                                      reg_tecidos_estampados.seq_item_pedido,  reg_tecidos_estampados.nivel,
                                      reg_tecidos_estampados.grupo,            reg_tecidos_estampados.subgrupo,
                                      reg_tecidos_estampados.item,             reg_tecidos_estampados.alternativa,
                                      reg_tecidos_estampados.roteiro,          v_qtde_lote,
                                      reg_tecidos_estampados.peso_padrao_rolo, reg_tecidos_estampados.largura,
                                      reg_tecidos_estampados.gramatura,        reg_tecidos_estampados.rendimento,
                                      reg_tecidos_estampados.codigo_deposito,  reg_tecidos_estampados.transacao,
                                      0,                  0,
                                      0,                  reg_tecidos_estampados.categoria);

         v_controle_erros := atualizar_qtde_programacao(reg_tecidos_estampados.pedido_venda,reg_tecidos_estampados.seq_item_pedido,1,v_qtde_lote);

         v_controle_erros := processar_acompanhamentos(v_ob,reg_tecidos_estampados.pedido_venda,reg_tecidos_estampados.seq_item_pedido,reg_tecidos_estampados.categoria,reg_tecidos_estampados.grupo_maquina,reg_tecidos_estampados.subgrupo_maquina,reg_tecidos_estampados.numero_maquina,v_qtde_lote,reg_tecidos_estampados.tipo_tecido);
      end loop;

      if v_saldo > 0
      then
         v_ob := get_numero_ob(p_opcao_geracao);

         v_controle_erros := gerar_ob(v_ob,                                    sysdate,
                                      reg_tecidos_estampados.grupo_maquina,    reg_tecidos_estampados.subgrupo_maquina,
                                      reg_tecidos_estampados.numero_maquina,   reg_tecidos_estampados.cor_formulario,
                                      reg_tecidos_estampados.tipo_servico,     reg_tecidos_estampados.pedido_venda,
                                      reg_tecidos_estampados.seq_item_pedido,  reg_tecidos_estampados.nivel,
                                      reg_tecidos_estampados.grupo,            reg_tecidos_estampados.subgrupo,
                                      reg_tecidos_estampados.item,             reg_tecidos_estampados.alternativa,
                                      reg_tecidos_estampados.roteiro,          v_saldo,
                                      reg_tecidos_estampados.peso_padrao_rolo, reg_tecidos_estampados.largura,
                                      reg_tecidos_estampados.gramatura,        reg_tecidos_estampados.rendimento,
                                      reg_tecidos_estampados.codigo_deposito,  reg_tecidos_estampados.transacao,
                                      0,                  0,
                                      0,                  reg_tecidos_estampados.categoria);

         v_controle_erros := atualizar_qtde_programacao(reg_tecidos_estampados.pedido_venda,reg_tecidos_estampados.seq_item_pedido,1,v_saldo);

         v_controle_erros := processar_acompanhamentos(v_ob,reg_tecidos_estampados.pedido_venda,reg_tecidos_estampados.seq_item_pedido,reg_tecidos_estampados.categoria,reg_tecidos_estampados.grupo_maquina,reg_tecidos_estampados.subgrupo_maquina,reg_tecidos_estampados.numero_maquina,v_saldo,reg_tecidos_estampados.tipo_tecido);
      end if;
   end loop;

   return(v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function criticar(p_pedido_venda number,   p_seq_item_pedido number,    p_categoria number,
                  p_data_entrega date,     p_nivel varchar2,            p_grupo varchar2,
                  p_subgrupo varchar2,     p_item varchar2,             p_nome_cliente varchar2,
                  p_saldo number,          p_grupo_maquina varchar2,    p_subgrupo_maquina varchar2,
                  p_numero_maquina number, p_codigo_critica number,     p_critica varchar2,
                  p_tipo_critica number)
return number is
   v_controle_erros number;
begin
   begin
      insert into tmrp_615
        (tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
         tmrp_615.categoria,
         tmrp_615.data_entrega,            tmrp_615.nivel,
         tmrp_615.grupo,                   tmrp_615.subgrupo,
         tmrp_615.item,
         tmrp_615.nome_cliente,

         tmrp_615.qtde_pedido,

         tmrp_615.grupo_maquina,           tmrp_615.subgrupo_maquina,
         tmrp_615.numero_maquina,

         tmrp_615.seq_registro,            tmrp_615.criticas,

         tmrp_615.selecionado4,

         tmrp_615.nome_programa,           tmrp_615.nr_solicitacao,
         tmrp_615.codigo_usuario,          tmrp_615.tipo_registro,
         tmrp_615.codigo_empresa,          tmrp_615.usuario)
      values
        (p_pedido_venda,                   p_seq_item_pedido,
         p_categoria,
         p_data_entrega,                   p_nivel,
         p_grupo,                          p_subgrupo,
         p_item,
         substr(p_nome_cliente,1,59),

         p_saldo,

         p_grupo_maquina,                  p_subgrupo_maquina,
         p_numero_maquina,

         p_codigo_critica,                 p_critica,

         p_tipo_critica,

         'pcpb_f041',                      p_codigo_solicitacao,
         p_codigo_usuario,                 600, -- criticas
         p_codigo_empresa,                 p_usuario);
      exception when others
      then v_controle_erros := registrar_excecao(sqlerrm,'tmrp_615',0);
   end;

   begin
      update tmrp_615
      set selecionado2 = 1
      where tmrp_615.nome_programa   = 'pcpb_f041'
        and tmrp_615.nr_solicitacao  = p_codigo_solicitacao
        and tmrp_615.codigo_usuario  = p_codigo_usuario
        and tmrp_615.usuario         = p_usuario
        and tmrp_615.codigo_empresa  = p_codigo_empresa
        and tmrp_615.tipo_registro   = 100
        and tmrp_615.pedido_venda    = p_pedido_venda
        and tmrp_615.seq_item_pedido = p_seq_item_pedido
        and tmrp_615.categoria       = p_categoria;
   end;

   return(v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function atualiza_mapeamento(p_data_inicio date,p_hora_inicio date,p_data_final date,p_hora_final date,p_codigo_recurso varchar2,p_cor varchar2,p_tonalidade varchar2)
return number is

begin
   inter_pr_mapeamento_tmrp_650(trunc(p_data_inicio),trunc(p_data_final),p_codigo_recurso);

   return(0);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function gerar_ot(p_data_inicio date,      p_hora_inicio date,      p_data_final date,
                  p_hora_final date,       p_tempo_tingimento number, p_grupo_maquina varchar2,
                  p_subgrupo_maquina varchar2, p_numero_maquina number)
return number is

   v_i                         number;
   v_ot                        number;
   v_estagio_critico           number;
   v_retorno                   number;
   v_codigo_recurso            varchar2(15);
   v_sequencia_entrada_maquina number;
   v_sequencia_impressao       number;
   v_controle_erros            number;
   v_cor                       varchar2(6);
   v_tonalidade                varchar2(2);

begin
   v_cor := '000000';
   v_tonalidade := '0';

   begin
      select nvl(max(pcpb_100.ordem_agrupamento),0) + 1
      into v_ot
      from pcpb_100
      where pcpb_100.tipo_ordem = '1';
      exception when others
      then v_ot := 0;
   end;

   for v_i in ordens_beneficiamento_unir.first..ordens_beneficiamento_unir.last
   loop
      if ordens_beneficiamento_unir.exists(v_i)
      then
         if v_cor = '000000'
         then
            begin
               select pcpb_020.pano_sbg_item
               into   v_cor
               from pcpb_020
               where pcpb_020.ordem_producao = ordens_beneficiamento_unir(v_i)
                 and rownum                  < 2;
               exception when others
               then
                  v_cor := '000000';
            end;

            begin
               select basi_100.tonalidade
               into v_tonalidade
               from basi_100
               where basi_100.cor_sortimento = v_cor
                 and basi_100.tipo_cor = 1;
               exception when others
               then
                  v_tonalidade := '0';
            end;
         end if;

         v_contador := v_contador + 1;

         begin
            insert into pcpb_100
              (tipo_ordem,                 ordem_agrupamento,
               ordem_producao,
               data_digitacao,
               data_prev_termino,          grupo_maquina,
               subgrupo_maquina,           numero_maquina,
               relacao_banho,              nivel_estrutura,
               data_prevista_maquina,      sequencia_entrada_maquina,
               cod_ordem_impressa)
            values
              ('1',                        v_ot,
               ordens_beneficiamento_unir(v_i),
               sysdate, -- pendencia
               p_data_final,               p_grupo_maquina,
               p_subgrupo_maquina,         p_numero_maquina,
               0,                          '2',
               p_data_inicio,              99,
               0); -- pendencia
            exception when others
            then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_100',0);
         end;

         begin
            update pcpb_010
               set ordem_tingimento = v_ot,
                   previsao_termino = p_data_final
            where pcpb_010.ordem_producao = ordens_beneficiamento_unir(v_i);
            exception when others
            then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_010',1);
         end;

         begin
            update tmrp_615
            set tmrp_615.data_inicio        = p_data_inicio,
                tmrp_615.data_termino       = p_data_final,
                tmrp_615.data_real_inicio   = p_hora_inicio,
                tmrp_615.data_real_termino  = p_hora_final,
                tmrp_615.tempo_tingimento   = p_tempo_tingimento,
                tmrp_615.ordem_planejamento = v_ot
            where tmrp_615.ordem_prod          = ordens_beneficiamento_unir(v_i)
              and tmrp_615.nome_programa       = 'pcpb_f041'
              and tmrp_615.codigo_usuario      = p_codigo_usuario
              and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
              and tmrp_615.codigo_empresa      = p_codigo_empresa
              and tmrp_615.usuario             = p_usuario
              and tmrp_615.tipo_registro       = 700; -- ordens geradas
            exception when others
            then v_controle_erros := registrar_excecao(sqlerrm,'TMRP_615',1);
         end;
      end if;
   end loop;

   begin
      select basi_170.estagio_critico
      into v_estagio_critico
      from basi_170
      where basi_170.area_producao = 2;
      exception when others
      then v_estagio_critico := 0;
   end;

   if p_gerar_com_data = 'S'
   then
      v_retorno := inter_fn_cria_estagios_painel(2,1,v_ot,0,0,v_estagio_critico,'1');

      if v_retorno is null
      then
         v_retorno := 0;
      end if;

      v_codigo_recurso := replace(p_grupo_maquina || '.' || p_subgrupo_maquina || '.' || to_char(p_numero_maquina,'00000'),' ','');

      begin
         update tmrp_650
         set tmrp_650.codigo_recurso    = v_codigo_recurso,
             tmrp_650.data_inicio       = p_data_inicio,
             tmrp_650.hora_inicio       = p_hora_inicio,
             tmrp_650.data_termino      = p_data_final,
             tmrp_650.hora_termino      = p_hora_final,
             tmrp_650.tempo_producao    = p_tempo_tingimento,
             tmrp_650.minutos_unitario  = decode(tmrp_650.quantidade,0,p_tempo_tingimento,p_tempo_tingimento / tmrp_650.quantidade),
             tmrp_650.aplicativo        = null
         where tmrp_650.ordem_trabalho = v_ot
           and tmrp_650.tipo_alocacao  = 2
           and tmrp_650.tipo_recurso   = 1
           and tmrp_650.codigo_estagio = v_estagio_critico;
         exception when others
         then v_controle_erros := registrar_excecao(sqlerrm,'TMRP_650',1);
      end;

      begin
         select nvl(max(pcpb_100.sequencia_entrada_maquina),0) + 1
         into v_sequencia_entrada_maquina
         from pcpb_100
         where pcpb_100.data_prevista_ini = p_data_inicio
           and pcpb_100.tipo_ordem        = 1;
         exception when others
         then v_sequencia_entrada_maquina := 0;
      end;

      if v_sequencia_entrada_maquina > 9999
      then
         v_sequencia_entrada_maquina := 9999;
      end if;

      begin
         select nvl(max(pcpb_100.seq_impressao),0) + 1
         into v_sequencia_impressao
         from pcpb_100
         where pcpb_100.data_prevista_ini = p_data_inicio
           and pcpb_100.tipo_ordem        = 1;
         exception when others
         then v_sequencia_impressao := 0;
      end;

      if v_sequencia_impressao > 99
      then
         v_sequencia_impressao := 99;
      end if;

      begin
         update pcpb_100
         set pcpb_100.data_prevista_ini         = p_data_inicio,
             pcpb_100.hora_prevista_ini         = p_hora_inicio,
             pcpb_100.data_prevista_fim         = p_data_final,
             pcpb_100.hora_prevista_fim         = p_hora_final,
             pcpb_100.sequencia_entrada_maquina = v_sequencia_entrada_maquina,
             pcpb_100.seq_impressao             = v_sequencia_impressao,
             pcpb_100.data_prevista_maquina     = p_data_inicio
         where pcpb_100.ordem_agrupamento = v_ot
           and pcpb_100.tipo_ordem        = '1';
         exception when others
         then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_100',1);
      end;

      v_controle_erros := atualiza_mapeamento(p_data_inicio,p_hora_inicio,p_data_final,p_hora_final,v_codigo_recurso,v_cor,v_tonalidade);
   end if;

   return(v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function subtrair_qtde_programacao(p_pedido_venda number,p_seq_item_pedido number,p_nivel varchar2, p_grupo varchar2, p_subgrupo varchar2, p_item varchar2,p_qtde number)
return number is
   v_controle_erros number;
begin
   begin
      update tmrp_615
         set tmrp_615.qtde_programacao = tmrp_615.qtde_programacao - p_qtde,
             tmrp_615.seq_oc           = tmrp_615.seq_oc - 1
      where tmrp_615.pedido_venda        = p_pedido_venda
        and tmrp_615.seq_item_pedido     = p_seq_item_pedido
        and tmrp_615.nivel               = p_nivel
        and tmrp_615.grupo               = p_grupo
        and tmrp_615.subgrupo            = p_subgrupo
        and tmrp_615.item                = p_item
        and tmrp_615.nome_programa       = 'pcpb_f041'
        and tmrp_615.codigo_usuario      = p_codigo_usuario
        and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
        and tmrp_615.codigo_empresa      = p_codigo_empresa
        and tmrp_615.usuario             = p_usuario
        and tmrp_615.tipo_registro       = 100;
      exception when others
      then v_controle_erros := registrar_excecao(sqlerrm,'TMRP_615',1);
   end;

   return (v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

function eliminar_obs_em_geracao
return number is
   pedido_estampado number;
   sequencia_estampado number;
   nivel_estampado varchar2(1);
   grupo_estampado varchar2(5);
   subgrupo_estampado varchar2(3);
   item_estampado varchar2(6);
   qtde_estampado number;
   grupo_maquina_estampado varchar2(4);
   subgrupo_maquina_estampado varchar2(3);
   numero_maquina_estampado number;
   v_i number;
begin
   for v_i in ordens_beneficiamento_geral.first..ordens_beneficiamento_geral.last
   loop
      if ordens_beneficiamento_geral.exists(v_i)
      then
         for reg_destinos in (select pcpb_030.nr_pedido_ordem,pcpb_030.sequenci_periodo,pcpb_030.pedido_corte,
                                     pcpb_030.pano_nivel99 nivel,pcpb_030.pano_grupo grupo,pcpb_030.pano_subgrupo subgrupo,pcpb_030.pano_item item,
                                     pcpb_030.qtde_quilos_prog qtde,
                                     pcpb_010.grupo_maquina grupo_maquina,pcpb_010.subgrupo_maquina subgrupo_maquina,pcpb_010.numero_maquina numero_maquina,
                                     pcpb_020.alternativa_item alternativa
                              from pcpb_030,pcpb_010,pcpb_020
                              where pcpb_030.ordem_producao = pcpb_010.ordem_producao
                                and pcpb_030.ordem_producao = pcpb_020.ordem_producao
                                and pcpb_030.pano_nivel99   = pcpb_020.pano_sbg_nivel99
                                and pcpb_030.pano_grupo     = pcpb_020.pano_sbg_grupo
                                and pcpb_030.pano_subgrupo  = pcpb_020.pano_sbg_subgrupo
                                and pcpb_030.pano_item      = pcpb_020.pano_sbg_item
                                and pcpb_030.sequencia      = pcpb_020.sequencia
                                and pcpb_030.ordem_producao = ordens_beneficiamento_geral(v_i))
         loop
            if reg_destinos.pedido_corte = 3
            then
                  v_controle_erros := subtrair_qtde_programacao(reg_destinos.nr_pedido_ordem,reg_destinos.sequenci_periodo,reg_destinos.nivel,reg_destinos.grupo,reg_destinos.subgrupo,reg_destinos.item,reg_destinos.qtde);
            elsif reg_destinos.pedido_corte = 7
            then
               qtde_estampado := 0;

               begin
                  select pcpb_030.nr_pedido_ordem, pcpb_030.sequenci_periodo,
                         pcpb_030.pano_nivel99,    pcpb_030.pano_grupo,       pcpb_030.pano_subgrupo, pcpb_030.pano_item,
                         pcpb_030.qtde_quilos_prog,
                         pcpb_010.grupo_maquina,   pcpb_010.subgrupo_maquina, pcpb_010.numero_maquina
                  into   pedido_estampado,         sequencia_estampado,
                         nivel_estampado,          grupo_estampado,           subgrupo_estampado,     item_estampado,
                         qtde_estampado,
                         grupo_maquina_estampado,  subgrupo_maquina_estampado, numero_maquina_estampado
                  from pcpb_030,pcpb_010
                  where pcpb_030.ordem_producao = pcpb_010.ordem_producao
                    and pcpb_030.ordem_producao = reg_destinos.nr_pedido_ordem;
                  exception when others
                  then qtde_estampado := 0;
               end;

               if qtde_estampado > 0
               then
                  v_controle_erros := subtrair_qtde_programacao(pedido_estampado,sequencia_estampado,nivel_estampado,grupo_estampado,subgrupo_estampado,item_estampado,qtde_estampado);
                  v_controle_erros := subtrair_qtde_programacao(pedido_estampado,sequencia_estampado,reg_destinos.nivel,reg_destinos.grupo,reg_destinos.subgrupo,reg_destinos.item,reg_destinos.qtde);
               end if;

               begin
                  delete pcpb_030
                  where ordem_producao = reg_destinos.nr_pedido_ordem;
                  exception when others
                  then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_030',2);
               end;

               begin
                  delete pcpb_020
                  where ordem_producao = reg_destinos.nr_pedido_ordem;
                  exception when others
                  then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_020',2);
               end;

               begin
                  delete pcpb_010
                  where ordem_producao = reg_destinos.nr_pedido_ordem;
                  exception when others
                  then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_010',2);
               end;

               begin
                  delete tmrp_615
                  where tmrp_615.nr_solicitacao = p_codigo_solicitacao
                    and tmrp_615.codigo_usuario = p_codigo_usuario
                    and tmrp_615.codigo_empresa = p_codigo_empresa
                    and tmrp_615.usuario        = p_usuario
                    and tmrp_615.tipo_registro  = 700
                    and tmrp_615.ordem_prod     = reg_destinos.nr_pedido_ordem;
               end;

               if p_solicitacao_necessidades > 0
               then
                  begin
                     update rcnb_020
                     set rcnb_020.qtde_nec_brutas = rcnb_020.qtde_nec_brutas - qtde_estampado
                     where rcnb_020.nr_solicitacao     = p_solicitacao_necessidades
                       and rcnb_020.nivel_estrutura    = nivel_estampado
                       and rcnb_020.grupo_estrutura    = grupo_estampado
                       and rcnb_020.subgru_estrutura   = subgrupo_estampado
                       and rcnb_020.item_estrutura     = item_estampado
                       and rcnb_020.grupo_maquina      = grupo_maquina_estampado
                       and rcnb_020.subgrupo_maquina   = subgrupo_maquina_estampado
                       and rcnb_020.numero_maquina     = numero_maquina_estampado;
                     exception when others
                     then v_controle_erros := registrar_excecao(sqlerrm,'RCNB_020',1);
                  end;
               end if;
            end if;

            if p_solicitacao_necessidades > 0
            then
               begin
                  update rcnb_020
                  set rcnb_020.qtde_nec_brutas = rcnb_020.qtde_nec_brutas - reg_destinos.qtde
                  where rcnb_020.nr_solicitacao     = p_solicitacao_necessidades
                    and rcnb_020.nivel_estrutura    = reg_destinos.nivel
                    and rcnb_020.grupo_estrutura    = reg_destinos.grupo
                    and rcnb_020.subgru_estrutura   = reg_destinos.subgrupo
                    and rcnb_020.item_estrutura     = reg_destinos.item
                    and rcnb_020.alternativa        = reg_destinos.alternativa
                    and rcnb_020.grupo_maquina      = reg_destinos.grupo_maquina
                    and rcnb_020.subgrupo_maquina   = reg_destinos.subgrupo_maquina
                    and rcnb_020.numero_maquina     = reg_destinos.numero_maquina;
                  exception when others
                  then v_controle_erros := registrar_excecao(sqlerrm,'RCNB_020',1);
               end;
            end if;
         end loop;

         begin
            delete pcpb_030
            where ordem_producao = ordens_beneficiamento_geral(v_i);
            exception when others
            then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_030',2);
         end;

         begin
            delete pcpb_020
            where ordem_producao = ordens_beneficiamento_geral(v_i);
            exception when others
            then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_020',2);
         end;

         begin
            delete pcpb_010
            where ordem_producao = ordens_beneficiamento_geral(v_i);
            exception when others
            then v_controle_erros := registrar_excecao(sqlerrm,'PCPB_010',2);
         end;

         begin
            delete tmrp_615
            where tmrp_615.nr_solicitacao = p_codigo_solicitacao
              and tmrp_615.codigo_usuario = p_codigo_usuario
              and tmrp_615.codigo_empresa = p_codigo_empresa
              and tmrp_615.usuario        = p_usuario
              and tmrp_615.tipo_registro  = 700
              and tmrp_615.ordem_prod     = ordens_beneficiamento_geral(v_i);
         end;
      end if;
   end loop;

   return(v_controle_erros);
end;

--------------------------------------------------------------------------------------------------------------------------------------------------

begin
   v_controle_erros := criticar(0,                    0,                    0,
                                sysdate,              '',                   '',
                                '',                   '',                   '',
                                0,                    '',                   '',
                                0,                    125,                  '',
                                1);

   v_data_analise := p_data_inicio_programacao;

   v_sequencia_log := 0;

   -- percorre todos os dias do periodo
   while (v_data_analise <= p_data_final_programacao)
   loop
      v_contador := 0;

      -- obtem a lista de maquinas passiveis de programacao no dia e percorre uma a uma
      for reg_maquinas in (select tmrp_651.codigo_recurso,tmrp_615.sequencia_carga
                           from tmrp_615, tmrp_651
                           where tmrp_615.grupo_maquina    = inter_fn_string_tokenizer(tmrp_651.codigo_recurso,1)
                             and tmrp_615.subgrupo_maquina = inter_fn_string_tokenizer(tmrp_651.codigo_recurso,2)
                             and tmrp_615.numero_maquina   = inter_fn_string_tokenizer(tmrp_651.codigo_recurso,3)
                             and trunc(tmrp_651.data_recurso) between trunc(p_data_inicio_programacao) and trunc(v_data_analise)
                             and tmrp_651.minutos          > 0
                             and tmrp_651.tipo_alocacao    = 2
                             and tmrp_651.tipo_recurso     = 1
                             and tmrp_651.situacao         = 0
                             and tmrp_615.nome_programa    = 'pcpb_f041'
                             and tmrp_615.codigo_usuario   = p_codigo_usuario
                             and tmrp_615.nr_solicitacao   = p_codigo_solicitacao
                             and tmrp_615.codigo_empresa   = p_codigo_empresa
                             and tmrp_615.usuario          = p_usuario
                             and tmrp_615.tipo_registro    = 100
                           group by tmrp_651.codigo_recurso,tmrp_615.sequencia_carga
                           order by tmrp_615.sequencia_carga)
      loop
         v_analisar_maquina := true;

         <<loop_recursos>>
         while(v_analisar_maquina)
         loop
            v_existem_lacunas := 1;

            if p_modalidade_planificacao = 1 -- uniforme (maquinas, dia)
            then
               v_analisar_maquina := false;
            end if;

            v_lote_acumulado:= 0;
            v_ob_alocada := 0;

            -- reinicializa collection
            ordens_beneficiamento_unir := array_vazio;
            ordens_beneficiamento_geral := array_vazio;
            v_contador_ordens_unir := 0;
            v_contador_ordens_geral := 0;

            -- existem lacunas disponiveis na maquina?
            begin
               select 1
               into v_existem_lacunas
               from tmrp_615, tmrp_651
               where tmrp_615.grupo_maquina    = inter_fn_string_tokenizer(reg_maquinas.codigo_recurso,1)
                 and tmrp_615.subgrupo_maquina = inter_fn_string_tokenizer(reg_maquinas.codigo_recurso,2)
                 and tmrp_615.numero_maquina   = inter_fn_string_tokenizer(reg_maquinas.codigo_recurso,3)
                 and tmrp_615.grupo_maquina    = inter_fn_string_tokenizer(tmrp_651.codigo_recurso,1)
                 and tmrp_615.subgrupo_maquina = inter_fn_string_tokenizer(tmrp_651.codigo_recurso,2)
                 and tmrp_615.numero_maquina   = inter_fn_string_tokenizer(tmrp_651.codigo_recurso,3)
                 and tmrp_651.codigo_recurso   = reg_maquinas.codigo_recurso
                 and trunc(tmrp_651.data_recurso) between trunc(p_data_inicio_programacao) and trunc(v_data_analise)
                 and tmrp_651.minutos          > 0
                 and tmrp_651.tipo_alocacao    = 2
                 and tmrp_651.tipo_recurso     = 1
                 and tmrp_651.situacao         = 0
                 and tmrp_615.nome_programa    = 'pcpb_f041'
                 and tmrp_615.codigo_usuario   = p_codigo_usuario
                 and tmrp_615.nr_solicitacao   = p_codigo_solicitacao
                 and tmrp_615.codigo_empresa   = p_codigo_empresa
                 and tmrp_615.usuario          = p_usuario
                 and tmrp_615.tipo_registro    = 100
                 and rownum                    < 2;
               exception when others then
                  v_existem_lacunas := 0;
            end;

            if v_existem_lacunas = 0 --se nao ha lacunas para preencher na maquina no dia...
            then
               v_analisar_maquina := false; -- ...define que a analise deve seguir para a proxima maquina ...
               exit loop_recursos; -- ... e abandona analise da maquina atual
            end if;

            -- percorre todas as lacunas da maquina no dia
            for reg_lacunas in (select tmrp_615.grupo_maquina,                    tmrp_615.subgrupo_maquina,
                                       tmrp_615.numero_maquina,
                                       trunc(tmrp_651.data_recurso) data_recurso, to_date(to_char(tmrp_651.data_recurso,'dd-mm-yyyy') || ' ' || to_char(tmrp_651.hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi') hora_inicio,
                                       tmrp_615.sequencia_carga,
                                       tmrp_651.cor,                              tmrp_651.tonalidade,        tmrp_651.minutos,
                                       tmrp_651.sequencia
                                from tmrp_615, tmrp_651
                                where tmrp_615.grupo_maquina    = inter_fn_string_tokenizer(reg_maquinas.codigo_recurso,1)
                                  and tmrp_615.subgrupo_maquina = inter_fn_string_tokenizer(reg_maquinas.codigo_recurso,2)
                                  and tmrp_615.numero_maquina   = inter_fn_string_tokenizer(reg_maquinas.codigo_recurso,3)
                                  and tmrp_615.grupo_maquina    = inter_fn_string_tokenizer(tmrp_651.codigo_recurso,1)
                                  and tmrp_615.subgrupo_maquina = inter_fn_string_tokenizer(tmrp_651.codigo_recurso,2)
                                  and tmrp_615.numero_maquina   = inter_fn_string_tokenizer(tmrp_651.codigo_recurso,3)
                                  and tmrp_651.codigo_recurso   = reg_maquinas.codigo_recurso
                                  and trunc(tmrp_651.data_recurso) between trunc(p_data_inicio_programacao) and trunc(v_data_analise)
                                  and tmrp_651.minutos          > 0
                                  and tmrp_651.tipo_alocacao    = 2
                                  and tmrp_651.tipo_recurso     = 1
                                  and tmrp_651.situacao         = 0
                                  and tmrp_615.nome_programa    = 'pcpb_f041'
                                  and tmrp_615.codigo_usuario   = p_codigo_usuario
                                  and tmrp_615.nr_solicitacao   = p_codigo_solicitacao
                                  and tmrp_615.codigo_empresa   = p_codigo_empresa
                                  and tmrp_615.usuario          = p_usuario
                                  and tmrp_615.tipo_registro    = 100
                                group by tmrp_615.grupo_maquina,                    tmrp_615.subgrupo_maquina,
                                         tmrp_615.numero_maquina,
                                         trunc(tmrp_651.data_recurso),              to_date(to_char(tmrp_651.data_recurso,'dd-mm-yyyy') || ' ' || to_char(tmrp_651.hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi'),
                                         tmrp_615.sequencia_carga,
                                         tmrp_651.cor,                              tmrp_651.tonalidade,        tmrp_651.minutos,
                                         tmrp_651.sequencia
                                order by to_date(to_char(tmrp_651.data_recurso,'dd-mm-yyyy') || ' ' || to_char(tmrp_651.hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi') asc, tmrp_615.sequencia_carga, tmrp_615.numero_maquina)
            loop
               -- obtem a ultima sequencia do dia na maquina
               begin
                  select nvl(max(tmrp_651.sequencia),reg_lacunas.sequencia)
                  into v_ultima_sequencia
                  from tmrp_651
                  where tmrp_651.tipo_alocacao  = 2
                    and tmrp_651.tipo_recurso   = 1
                    and tmrp_651.codigo_recurso = reg_maquinas.codigo_recurso
                    and tmrp_651.data_recurso   = reg_lacunas.data_recurso;
               end;

               v_continua_1 := true;

               v_capacidade_minima:= 0;
               v_lote_acumulado:= 0;
               v_ob_alocada := 0;

               -- reinicializa collection
               ordens_beneficiamento_unir := array_vazio;
               ordens_beneficiamento_geral := array_vazio;
               v_contador_ordens_unir := 0;
               v_contador_ordens_geral := 0;

               v_analisar_lacuna := true;
               v_tentativa := 0;

               -- a lacuna passa por tres tentativas de preenchimento:
               -- 1) tentando repetir cor;
               -- 2) tentando repetir tonalidade;
               -- 3) sem criterio de repeticao.
               -- v_analisar_lacuna indica que ainda nao houve
               -- preenchimento e restam tentativas
               while(v_analisar_lacuna)
               loop
                  v_tecido_encontrado := 1;

                  v_tentativa := v_tentativa + 1;

                  --Primeiro, tenta encontrar tecido com a mesma cor da lacuna
                  if v_tentativa = 1
                  then
                     begin
                        select pedido_venda,            seq_item_pedido,
                               nome_cliente,
                               categoria,               nivel,
                               grupo,                   subgrupo,
                               item,                    tonalidade,
                               cor_formulario,          tipo_servico,
                               codigo_deposito,         transacao,
                               alternativa,             roteiro,
                               peso_padrao_rolo,        largura,
                               gramatura,               rendimento,
                               codigo_acomp,            sequencia_principal,
                               tipo_tecido,             tipo_natureza,
                               grupo_tingimento,        consumo,
                               origem_tempo,            data_entrega,
                               tempo_tingimento,
                               saldo,
                               capacidade_maxima,
                               capacidade_minima
                        into   v_pedido_venda,          v_seq_item_pedido,
                               v_nome_cliente,
                               v_categoria,             v_nivel,
                               v_grupo,                 v_subgrupo,
                               v_item,                  v_tonalidade,
                               v_cor_formulario,        v_tipo_servico,
                               v_codigo_deposito,       v_transacao,
                               v_alternativa,           v_roteiro,
                               v_peso_padrao_rolo,      v_largura,
                               v_gramatura,             v_rendimento,
                               v_codigo_acomp,          v_sequencia_principal,
                               v_tipo_tecido,           v_tipo_natureza,
                               v_grupo_tingimento,      v_consumo,
                               v_origem_tempo,          v_data_entrega,
                               v_tempo_tingimento,
                               v_saldo,
                               v_capacidade_maxima,
                               v_capacidade_minima
                        from (select tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
                                     tmrp_615.nome_cliente,
                                     tmrp_615.categoria,               tmrp_615.nivel,
                                     tmrp_615.grupo,                   tmrp_615.subgrupo,
                                     tmrp_615.item,                    tmrp_615.tonalidade,
                                     tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
                                     tmrp_615.codigo_deposito,         tmrp_615.transacao,
                                     tmrp_615.alternativa,             tmrp_615.roteiro,
                                     tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
                                     tmrp_615.gramatura,               tmrp_615.rendimento,
                                     tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
                                     tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
                                     tmrp_615.grupo_tingimento,        tmrp_615.consumo,
                                     tmrp_615.origem_tempo,            tmrp_615.data_entrega,
                                     decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * tmrp_615.capacidade_maxima) tempo_tingimento,
                                     tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao  saldo,
                                     tmrp_615.capacidade_maxima,
                                     tmrp_615.capacidade_minima,
                                     sum(tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao) over
                                     (partition by item,grupo_tingimento) saldo_uniao
                        from tmrp_615, (select tmrp_615.pedido_venda, tmrp_615.sequencia_principal, nvl(sum(tmrp_615.qtde_pedido),0) qtde_pedido
                                        from tmrp_615
                                        where tmrp_615.nome_programa       = 'pcpb_f041'
                                          and tmrp_615.codigo_usuario      = p_codigo_usuario
                                          and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                                          and tmrp_615.codigo_empresa      = p_codigo_empresa
                                          and tmrp_615.usuario             = p_usuario
                                          and tmrp_615.tipo_registro       = 100
                                          and tmrp_615.sequencia_principal > 0
                                          and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                                          and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                                          and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                                        group by tmrp_615.pedido_venda, tmrp_615.sequencia_principal) acompanhamentos
                        where tmrp_615.pedido_venda        = acompanhamentos.pedido_venda (+)
                          and tmrp_615.seq_item_pedido     = acompanhamentos.sequencia_principal (+)
                          and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                          and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                          and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                          and round(tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao,3) > 0
                          and ((tmrp_615.qtde_faturada + tmrp_615.qtde_estoque + tmrp_615.qtde_destinada + tmrp_615.qtde_programacao) * 100) / decode(tmrp_615.qtde_pedido,0,1,tmrp_615.qtde_pedido) < p_percentual_atendimento
                          and decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * tmrp_615.capacidade_maxima) <= reg_lacunas.minutos
                          and tmrp_615.item                = reg_lacunas.cor
                          and trunc(tmrp_615.data_entrega) >= trunc(reg_lacunas.data_recurso)
                          and tmrp_615.categoria           in (0,2) -- tinturaria e fundo de estampado
                          and tmrp_615.sequencia_principal = 0 -- somente itens principais
                          and tmrp_615.selecionado2        = 0 -- nao criticado
                          and (tmrp_615.seq_oc             < decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) or decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) = 0)
                          and tmrp_615.nome_programa       = 'pcpb_f041'
                          and tmrp_615.codigo_usuario      = p_codigo_usuario
                          and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                          and tmrp_615.codigo_empresa      = p_codigo_empresa
                          and tmrp_615.usuario             = p_usuario
                          and tmrp_615.tipo_registro       = 100
                        UNION
                        select tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
                                     tmrp_615.nome_cliente,
                                     tmrp_615.categoria,               tmrp_615.nivel,
                                     tmrp_615.grupo,                   tmrp_615.subgrupo,
                                     tmrp_615.item,                    tmrp_615.tonalidade,
                                     tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
                                     tmrp_615.codigo_deposito,         tmrp_615.transacao,
                                     tmrp_615.alternativa,             tmrp_615.roteiro,
                                     tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
                                     tmrp_615.gramatura,               tmrp_615.rendimento,
                                     tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
                                     tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
                                     tmrp_615.grupo_tingimento,        tmrp_615.consumo,
                                     tmrp_615.origem_tempo,            tmrp_615.data_entrega,
                                     decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * tmrp_615.capacidade_maxima) tempo_tingimento,
                                     tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao  saldo,
                                     tmrp_615.capacidade_maxima,
                                     tmrp_615.capacidade_minima,
                                     sum(tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao) over
                                     (partition by item,grupo_tingimento) saldo_uniao
                        from tmrp_615, (select tmrp_615.pedido_venda, tmrp_615.sequencia_principal, nvl(sum(tmrp_615.qtde_pedido),0) qtde_pedido
                                        from tmrp_615
                                        where tmrp_615.nome_programa       = 'pcpb_f041'
                                          and tmrp_615.codigo_usuario      = p_codigo_usuario
                                          and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                                          and tmrp_615.codigo_empresa      = p_codigo_empresa
                                          and tmrp_615.usuario             = p_usuario
                                          and tmrp_615.tipo_registro       = 100
                                          and tmrp_615.sequencia_principal > 0
                                          and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                                          and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                                          and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                                        group by tmrp_615.pedido_venda, tmrp_615.sequencia_principal) acompanhamentos
                        where tmrp_615.pedido_venda        = acompanhamentos.pedido_venda (+)
                          and tmrp_615.seq_item_pedido     = acompanhamentos.sequencia_principal (+)
                          and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                          and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                          and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                          and round(tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao,3) > 0
                          and ((tmrp_615.qtde_faturada + tmrp_615.qtde_estoque + tmrp_615.qtde_destinada + tmrp_615.qtde_programacao) * 100) / decode(tmrp_615.qtde_pedido,0,1,tmrp_615.qtde_pedido) < p_percentual_atendimento
                          and decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * tmrp_615.capacidade_maxima) <= reg_lacunas.minutos
                          and tmrp_615.item                = reg_lacunas.cor
                          and tmrp_615.categoria           in (0,2) -- tinturaria e fundo de estampado
                          and tmrp_615.sequencia_principal = 0 -- somente itens principais`
                          and tmrp_615.selecionado2        = 0 -- nao criticado
                          and (tmrp_615.seq_oc             < decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) or decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) = 0)
                          and tmrp_615.nome_programa       = 'pcpb_f041'
                          and tmrp_615.codigo_usuario      = p_codigo_usuario
                          and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                          and tmrp_615.codigo_empresa      = p_codigo_empresa
                          and tmrp_615.usuario             = p_usuario
                          and tmrp_615.tipo_registro       = 100
                        order by saldo_uniao desc,saldo desc) tmrp_615a
                        where rownum < 2;
                        exception when others
                        then v_tecido_encontrado := 0;
                     end;
                  end if;

                  if v_tentativa = 2
                  then
                     v_tecido_encontrado := 1;

                     -- Em seguida, tecidos com a mesma tonalidade
                     begin
                        select pedido_venda,            seq_item_pedido,
                               nome_cliente,
                               categoria,               nivel,
                               grupo,                   subgrupo,
                               item,                    tonalidade,
                               cor_formulario,          tipo_servico,
                               codigo_deposito,         transacao,
                               alternativa,             roteiro,
                               peso_padrao_rolo,        largura,
                               gramatura,               rendimento,
                               codigo_acomp,            sequencia_principal,
                               tipo_tecido,             tipo_natureza,
                               grupo_tingimento,        consumo,
                               origem_tempo,            data_entrega,
                               tempo_tingimento,
                               saldo,
                               capacidade_maxima,
                               capacidade_minima
                        into   v_pedido_venda,          v_seq_item_pedido,
                               v_nome_cliente,
                               v_categoria,             v_nivel,
                               v_grupo,                 v_subgrupo,
                               v_item,                  v_tonalidade,
                               v_cor_formulario,        v_tipo_servico,
                               v_codigo_deposito,       v_transacao,
                               v_alternativa,           v_roteiro,
                               v_peso_padrao_rolo,      v_largura,
                               v_gramatura,             v_rendimento,
                               v_codigo_acomp,          v_sequencia_principal,
                               v_tipo_tecido,           v_tipo_natureza,
                               v_grupo_tingimento,      v_consumo,
                               v_origem_tempo,          v_data_entrega,
                               v_tempo_tingimento,
                               v_saldo,
                               v_capacidade_maxima,
                               v_capacidade_minima
                        from (select tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
                                     tmrp_615.nome_cliente,
                                     tmrp_615.categoria,               tmrp_615.nivel,
                                     tmrp_615.grupo,                   tmrp_615.subgrupo,
                                     tmrp_615.item,                    tmrp_615.tonalidade,
                                     tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
                                     tmrp_615.codigo_deposito,         tmrp_615.transacao,
                                     tmrp_615.alternativa,             tmrp_615.roteiro,
                                     tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
                                     tmrp_615.gramatura,               tmrp_615.rendimento,
                                     tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
                                     tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
                                     tmrp_615.grupo_tingimento,        tmrp_615.consumo,
                                     tmrp_615.origem_tempo,            tmrp_615.data_entrega,
                                     decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * tmrp_615.capacidade_maxima) tempo_tingimento,
                                     tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao saldo,
                                     tmrp_615.capacidade_maxima,
                                     tmrp_615.capacidade_minima,
                                     sum(tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao) over
                                     (partition by item,grupo_tingimento) saldo_uniao
                        from tmrp_615, (select tmrp_615.pedido_venda, tmrp_615.sequencia_principal, nvl(sum(tmrp_615.qtde_pedido),0) qtde_pedido
                                        from tmrp_615
                                        where tmrp_615.nome_programa       = 'pcpb_f041'
                                          and tmrp_615.codigo_usuario      = p_codigo_usuario
                                          and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                                          and tmrp_615.codigo_empresa      = p_codigo_empresa
                                          and tmrp_615.usuario             = p_usuario
                                          and tmrp_615.tipo_registro       = 100
                                          and tmrp_615.sequencia_principal > 0
                                          and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                                          and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                                          and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                                        group by tmrp_615.pedido_venda, tmrp_615.sequencia_principal) acompanhamentos
                        where tmrp_615.pedido_venda        = acompanhamentos.pedido_venda (+)
                          and tmrp_615.seq_item_pedido     = acompanhamentos.sequencia_principal (+)
                          and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                          and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                          and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                          and round(tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao,3) > 0
                          and ((tmrp_615.qtde_faturada + tmrp_615.qtde_estoque + tmrp_615.qtde_destinada + tmrp_615.qtde_programacao) * 100) / decode(tmrp_615.qtde_pedido,0,1,tmrp_615.qtde_pedido) < p_percentual_atendimento
                          and decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * tmrp_615.capacidade_maxima) <= reg_lacunas.minutos
                          and tmrp_615.tonalidade          = reg_lacunas.tonalidade
                          and trunc(tmrp_615.data_entrega) >= trunc(reg_lacunas.data_recurso)
                          and tmrp_615.categoria           in (0,2) -- tinturaria e fundo de estampado
                          and tmrp_615.sequencia_principal = 0 -- somente itens principais
                          and tmrp_615.selecionado2        = 0 -- nao criticado
                          and (tmrp_615.seq_oc             < decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) or decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) = 0)
                          and tmrp_615.nome_programa       = 'pcpb_f041'
                          and tmrp_615.codigo_usuario      = p_codigo_usuario
                          and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                          and tmrp_615.codigo_empresa      = p_codigo_empresa
                          and tmrp_615.usuario             = p_usuario
                          and tmrp_615.tipo_registro       = 100
                        UNION
                        select tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
                                     tmrp_615.nome_cliente,
                                     tmrp_615.categoria,               tmrp_615.nivel,
                                     tmrp_615.grupo,                   tmrp_615.subgrupo,
                                     tmrp_615.item,                    tmrp_615.tonalidade,
                                     tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
                                     tmrp_615.codigo_deposito,         tmrp_615.transacao,
                                     tmrp_615.alternativa,             tmrp_615.roteiro,
                                     tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
                                     tmrp_615.gramatura,               tmrp_615.rendimento,
                                     tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
                                     tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
                                     tmrp_615.grupo_tingimento,        tmrp_615.consumo,
                                     tmrp_615.origem_tempo,            tmrp_615.data_entrega,
                                     decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * tmrp_615.capacidade_maxima) tempo_tingimento,
                                     tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao saldo,
                                     tmrp_615.capacidade_maxima,
                                     tmrp_615.capacidade_minima,
                                     sum(tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao) over
                                     (partition by item,grupo_tingimento) saldo_uniao
                        from tmrp_615, (select tmrp_615.pedido_venda, tmrp_615.sequencia_principal, nvl(sum(tmrp_615.qtde_pedido),0) qtde_pedido
                                        from tmrp_615
                                        where tmrp_615.nome_programa       = 'pcpb_f041'
                                          and tmrp_615.codigo_usuario      = p_codigo_usuario
                                          and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                                          and tmrp_615.codigo_empresa      = p_codigo_empresa
                                          and tmrp_615.usuario             = p_usuario
                                          and tmrp_615.tipo_registro       = 100
                                          and tmrp_615.sequencia_principal > 0
                                          and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                                          and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                                          and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                                        group by tmrp_615.pedido_venda, tmrp_615.sequencia_principal) acompanhamentos
                        where tmrp_615.pedido_venda        = acompanhamentos.pedido_venda (+)
                          and tmrp_615.seq_item_pedido     = acompanhamentos.sequencia_principal (+)
                          and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                          and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                          and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                          and round(tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao,3) > 0
                          and ((tmrp_615.qtde_faturada + tmrp_615.qtde_estoque + tmrp_615.qtde_destinada + tmrp_615.qtde_programacao) * 100) / decode(tmrp_615.qtde_pedido,0,1,tmrp_615.qtde_pedido) < p_percentual_atendimento
                          and decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * tmrp_615.capacidade_maxima) <= reg_lacunas.minutos
                          and tmrp_615.tonalidade          = reg_lacunas.tonalidade
                          and tmrp_615.categoria           in (0,2) -- tinturaria e fundo de estampado
                          and tmrp_615.sequencia_principal = 0 -- somente itens principais
                          and tmrp_615.selecionado2        = 0 -- nao criticado
                          and (tmrp_615.seq_oc             < decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) or decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) = 0)
                          and tmrp_615.nome_programa       = 'pcpb_f041'
                          and tmrp_615.codigo_usuario      = p_codigo_usuario
                          and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                          and tmrp_615.codigo_empresa      = p_codigo_empresa
                          and tmrp_615.usuario             = p_usuario
                          and tmrp_615.tipo_registro       = 100
                        order by saldo_uniao desc,saldo desc) tmrp_615a
                        where rownum < 2;
                        exception when others
                        then v_tecido_encontrado := 0;
                     end;
                  end if;

                  if v_tentativa = 3
                  then
                     v_tecido_encontrado := 1;
                     -- define que a lacuna deve ser abandonada apos esta tentativa
                     v_analisar_lacuna := false;

                     -- Por ultimo, tecidos sem criterio de repeticao
                     begin
                        select pedido_venda,            seq_item_pedido,
                               nome_cliente,
                               categoria,               nivel,
                               grupo,                   subgrupo,
                               item,                    tonalidade,
                               cor_formulario,          tipo_servico,
                               codigo_deposito,         transacao,
                               alternativa,             roteiro,
                               peso_padrao_rolo,        largura,
                               gramatura,               rendimento,
                               codigo_acomp,            sequencia_principal,
                               tipo_tecido,             tipo_natureza,
                               grupo_tingimento,        consumo,
                               origem_tempo,            data_entrega,
                               tempo_tingimento,
                               saldo,
                               capacidade_maxima,
                               capacidade_minima
                        into   v_pedido_venda,          v_seq_item_pedido,
                               v_nome_cliente,
                               v_categoria,             v_nivel,
                               v_grupo,                 v_subgrupo,
                               v_item,                  v_tonalidade,
                               v_cor_formulario,        v_tipo_servico,
                               v_codigo_deposito,       v_transacao,
                               v_alternativa,           v_roteiro,
                               v_peso_padrao_rolo,      v_largura,
                               v_gramatura,             v_rendimento,
                               v_codigo_acomp,          v_sequencia_principal,
                               v_tipo_tecido,           v_tipo_natureza,
                               v_grupo_tingimento,      v_consumo,
                               v_origem_tempo,          v_data_entrega,
                               v_tempo_tingimento,
                               v_saldo,
                               v_capacidade_maxima,
                               v_capacidade_minima
                        from (select tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
                                     tmrp_615.nome_cliente,
                                     tmrp_615.categoria,               tmrp_615.nivel,
                                     tmrp_615.grupo,                   tmrp_615.subgrupo,
                                     tmrp_615.item,                    tmrp_615.tonalidade,
                                     tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
                                     tmrp_615.codigo_deposito,         tmrp_615.transacao,
                                     tmrp_615.alternativa,             tmrp_615.roteiro,
                                     tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
                                     tmrp_615.gramatura,               tmrp_615.rendimento,
                                     tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
                                     tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
                                     tmrp_615.grupo_tingimento,        tmrp_615.consumo,
                                     tmrp_615.origem_tempo,            tmrp_615.data_entrega,
                                     decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * tmrp_615.capacidade_maxima) tempo_tingimento,
                                     tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao saldo,
                                     tmrp_615.capacidade_maxima,
                                     tmrp_615.capacidade_minima,
                                     sum(tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao) over
                                     (partition by item,grupo_tingimento) saldo_uniao
                        from tmrp_615, (select tmrp_615.pedido_venda, tmrp_615.sequencia_principal, nvl(sum(tmrp_615.qtde_pedido),0) qtde_pedido
                                        from tmrp_615
                                        where tmrp_615.nome_programa       = 'pcpb_f041'
                                          and tmrp_615.codigo_usuario      = p_codigo_usuario
                                          and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                                          and tmrp_615.codigo_empresa      = p_codigo_empresa
                                          and tmrp_615.usuario             = p_usuario
                                          and tmrp_615.tipo_registro       = 100
                                          and tmrp_615.sequencia_principal > 0
                                          and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                                          and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                                          and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                                        group by tmrp_615.pedido_venda, tmrp_615.sequencia_principal) acompanhamentos
                        where tmrp_615.pedido_venda        = acompanhamentos.pedido_venda (+)
                          and tmrp_615.seq_item_pedido     = acompanhamentos.sequencia_principal (+)
                          and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                          and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                          and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                          and round(tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao,3) > 0
                          and ((tmrp_615.qtde_faturada + tmrp_615.qtde_estoque + tmrp_615.qtde_destinada + tmrp_615.qtde_programacao) * 100) / decode(tmrp_615.qtde_pedido,0,1,tmrp_615.qtde_pedido) < p_percentual_atendimento
                          and decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * tmrp_615.capacidade_maxima) <= reg_lacunas.minutos
                          and tmrp_615.categoria           in (0,2) -- tinturaria e fundo de estampado
                          and tmrp_615.sequencia_principal = 0 -- somente itens principais
                          and tmrp_615.selecionado2        = 0 -- nao criticado
                          and trunc(tmrp_615.data_entrega) >= trunc(reg_lacunas.data_recurso)
                          and (tmrp_615.seq_oc             < decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) or decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) = 0)
                          and tmrp_615.nome_programa       = 'pcpb_f041'
                          and tmrp_615.codigo_usuario      = p_codigo_usuario
                          and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                          and tmrp_615.codigo_empresa      = p_codigo_empresa
                          and tmrp_615.usuario             = p_usuario
                          and tmrp_615.tipo_registro       = 100
                        UNION
                        select tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
                                     tmrp_615.nome_cliente,
                                     tmrp_615.categoria,               tmrp_615.nivel,
                                     tmrp_615.grupo,                   tmrp_615.subgrupo,
                                     tmrp_615.item,                    tmrp_615.tonalidade,
                                     tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
                                     tmrp_615.codigo_deposito,         tmrp_615.transacao,
                                     tmrp_615.alternativa,             tmrp_615.roteiro,
                                     tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
                                     tmrp_615.gramatura,               tmrp_615.rendimento,
                                     tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
                                     tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
                                     tmrp_615.grupo_tingimento,        tmrp_615.consumo,
                                     tmrp_615.origem_tempo,            tmrp_615.data_entrega,
                                     decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * tmrp_615.capacidade_maxima) tempo_tingimento,
                                     tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao saldo,
                                     tmrp_615.capacidade_maxima,
                                     tmrp_615.capacidade_minima,
                                     sum(tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao) over
                                     (partition by item,grupo_tingimento) saldo_uniao
                        from tmrp_615, (select tmrp_615.pedido_venda, tmrp_615.sequencia_principal, nvl(sum(tmrp_615.qtde_pedido),0) qtde_pedido
                                        from tmrp_615
                                        where tmrp_615.nome_programa       = 'pcpb_f041'
                                          and tmrp_615.codigo_usuario      = p_codigo_usuario
                                          and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                                          and tmrp_615.codigo_empresa      = p_codigo_empresa
                                          and tmrp_615.usuario             = p_usuario
                                          and tmrp_615.tipo_registro       = 100
                                          and tmrp_615.sequencia_principal > 0
                                          and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                                          and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                                          and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                                        group by tmrp_615.pedido_venda, tmrp_615.sequencia_principal) acompanhamentos
                        where tmrp_615.pedido_venda        = acompanhamentos.pedido_venda (+)
                          and tmrp_615.seq_item_pedido     = acompanhamentos.sequencia_principal (+)
                          and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                          and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                          and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                          and round(tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao,3) > 0
                          and ((tmrp_615.qtde_faturada + tmrp_615.qtde_estoque + tmrp_615.qtde_destinada + tmrp_615.qtde_programacao) * 100) / decode(tmrp_615.qtde_pedido,0,1,tmrp_615.qtde_pedido) < p_percentual_atendimento
                          and decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * tmrp_615.capacidade_maxima) <= reg_lacunas.minutos
                          and tmrp_615.categoria           in (0,2) -- tinturaria e fundo de estampado
                          and tmrp_615.sequencia_principal = 0 -- somente itens principais
                          and tmrp_615.selecionado2        = 0 -- nao criticado
                          and (tmrp_615.seq_oc             < decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) or decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) = 0)
                          and tmrp_615.nome_programa       = 'pcpb_f041'
                          and tmrp_615.codigo_usuario      = p_codigo_usuario
                          and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                          and tmrp_615.codigo_empresa      = p_codigo_empresa
                          and tmrp_615.usuario             = p_usuario
                          and tmrp_615.tipo_registro       = 100
                        order by saldo_uniao desc, saldo desc) tmrp_615a
                        where rownum < 2;
                        exception when others
                        then v_tecido_encontrado := 0;
                     end;
                  end if;

                  if v_tecido_encontrado = 1
                  then
                     v_lote_acumulado := 0;

                     if v_tempo_tingimento = 0
                     then
                        v_controle_erros := criticar(v_pedido_venda,               v_seq_item_pedido,              v_categoria,
                                                     v_data_entrega,               v_nivel,                        v_grupo,
                                                     v_subgrupo,                   v_item,                         v_nome_cliente,
                                                     v_saldo,                      reg_lacunas.grupo_maquina,      reg_lacunas.subgrupo_maquina,
                                                     reg_lacunas.numero_maquina,   320,                            'Tempo de producao nao definido',
                                                     1);

                        v_continua_1 := false;
                     end if;

                     v_data_hora_termino := to_date(to_char(reg_lacunas.data_recurso,'dd-mm-yyyy') || ' ' || to_char(reg_lacunas.hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi') + (v_tempo_tingimento / 1440);

                     -- um pixel no painel java correponde a 15 minutos.
                     -- a logica abaixo busca arredondar os minutos obtidos para multiplos de 15.
                     v_hora_retorno    := substr(to_char(v_data_hora_termino,'HH24:MI'),1,2);
                     v_minutos_retorno := substr(to_char(v_data_hora_termino,'HH24:MI'),4,5);

                     if  to_number(v_minutos_retorno) > 0
                     and to_number(v_minutos_retorno) < 15
                     then
                        v_minutos_retorno := '00';
                     else
                        if  to_number(v_minutos_retorno) > 15
                        and to_number(v_minutos_retorno) < 30
                        then
                           v_minutos_retorno := '15';
                        else
                           if  to_number(v_minutos_retorno) > 30
                           and to_number(v_minutos_retorno) < 45
                           then
                              v_minutos_retorno := '30';
                           else
                              if  to_number(v_minutos_retorno) > 45
                              and to_number(v_minutos_retorno) < 60
                              then
                                 v_minutos_retorno := '45';
                              end if;
                           end if;
                        end if;
                     end if;

                     v_data_hora_termino := to_date(to_char(v_data_hora_termino,'dd-mm-yyyy') || ' ' || v_hora_retorno || ':' || v_minutos_retorno,'dd-mm-yyyy hh24:mi');

                     if v_data_hora_termino is null
                     then
                        v_controle_erros := criticar(v_pedido_venda,               v_seq_item_pedido,             v_categoria,
                                                     v_data_entrega,               v_nivel,                       v_grupo,
                                                     v_subgrupo,                   v_item,                        v_nome_cliente,
                                                     v_saldo,                      reg_lacunas.grupo_maquina,     reg_lacunas.subgrupo_maquina,
                                                     reg_lacunas.numero_maquina,   310,                           'A data de planificacao nao pode ser calculada',
                                                     1);

                        v_continua_1 := false;
                     else
                        v_data_termino := v_data_hora_termino;
                        v_hora_termino := v_data_hora_termino;
                     end if;

                     if v_continua_1 = true
                     then
                        v_ob := get_numero_ob(p_opcao_geracao);
                        v_contador_ordens_unir := v_contador_ordens_unir + 1;
                        v_contador_ordens_geral := v_contador_ordens_geral + 1;
                        ordens_beneficiamento_unir(v_contador_ordens_unir) := v_ob;
                        ordens_beneficiamento_geral(v_contador_ordens_geral) := v_ob;

                        if v_lote_acumulado + v_saldo + (v_saldo * (get_percentual_acompanhamento(v_pedido_venda,v_seq_item_pedido,v_categoria,reg_lacunas.grupo_maquina,reg_lacunas.subgrupo_maquina,reg_lacunas.numero_maquina) / 100)) < v_capacidade_maxima
                        then
                           v_controle_erros := processar_tecidos(reg_lacunas.grupo_maquina,         reg_lacunas.subgrupo_maquina,
                                                                 reg_lacunas.numero_maquina,        v_pedido_venda,
                                                                 v_seq_item_pedido,                 v_categoria,
                                                                 v_nivel,                           v_grupo,
                                                                 v_subgrupo,                        v_item,
                                                                 v_alternativa,                     v_roteiro,
                                                                 v_saldo,                           v_peso_padrao_rolo,
                                                                 v_largura,                         v_gramatura,
                                                                 v_rendimento,                      v_codigo_deposito,
                                                                 v_transacao,                       v_codigo_acomp,
                                                                 v_sequencia_principal,             v_cor_formulario,
                                                                 v_tipo_servico,                    v_tipo_tecido,
                                                                 v_consumo);

                           if p_forma_programacao = 2
                           then
                              <<tecidos_adicionais>>
                              for reg_tecidos_complementares in
                              (select tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
                                      tmrp_615.nome_cliente,
                                      tmrp_615.categoria,               tmrp_615.nivel,
                                      tmrp_615.grupo,                   tmrp_615.subgrupo,
                                      tmrp_615.item,                    tmrp_615.tonalidade,
                                      tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
                                      tmrp_615.codigo_deposito,         tmrp_615.transacao,
                                      tmrp_615.alternativa,             tmrp_615.roteiro,
                                      tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
                                      tmrp_615.gramatura,               tmrp_615.rendimento,
                                      tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
                                      tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
                                      tmrp_615.grupo_tingimento,        tmrp_615.consumo,
                                      tmrp_615.origem_tempo,            tmrp_615.data_entrega,
                                      decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * (tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao)) tempo_tingimento,
                                      tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao saldo
                              from tmrp_615,  (select tmrp_615.pedido_venda, tmrp_615.sequencia_principal, nvl(sum(tmrp_615.qtde_pedido),0) qtde_pedido
                                               from tmrp_615
                                               where tmrp_615.nome_programa       = 'pcpb_f041'
                                                 and tmrp_615.codigo_usuario      = p_codigo_usuario
                                                 and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                                                 and tmrp_615.codigo_empresa      = p_codigo_empresa
                                                 and tmrp_615.usuario             = p_usuario
                                                 and tmrp_615.tipo_registro       = 100
                                                 and tmrp_615.sequencia_principal > 0
                                                 and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                                                 and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                                                 and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                                               group by tmrp_615.pedido_venda, tmrp_615.sequencia_principal) acompanhamentos
                              where tmrp_615.pedido_venda        = acompanhamentos.pedido_venda (+)
                                and tmrp_615.seq_item_pedido     = acompanhamentos.sequencia_principal (+)
                                and tmrp_615.grupo_tingimento    = v_grupo_tingimento
                                and tmrp_615.item                = v_item
                                and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                                and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                                and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                                and tmrp_615.data_entrega       >= reg_lacunas.data_recurso
                                and tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao > 0
                                and ((tmrp_615.qtde_faturada + tmrp_615.qtde_estoque + tmrp_615.qtde_destinada + tmrp_615.qtde_programacao) * 100) / decode(tmrp_615.qtde_pedido,0,1,tmrp_615.qtde_pedido) < p_percentual_atendimento
                                and tmrp_615.categoria           in (0,2) -- tinturaria e fundo de estampado
                                and tmrp_615.sequencia_principal = 0 -- somente itens principais
                                and (tmrp_615.seq_oc             < decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) or decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) = 0)
                                and tmrp_615.nome_programa       = 'pcpb_f041'
                                and tmrp_615.codigo_usuario      = p_codigo_usuario
                                and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                                and tmrp_615.codigo_empresa      = p_codigo_empresa
                                and tmrp_615.usuario             = p_usuario
                                and tmrp_615.tipo_registro       = 100
                              UNION
                              select tmrp_615.pedido_venda,            tmrp_615.seq_item_pedido,
                                      tmrp_615.nome_cliente,
                                      tmrp_615.categoria,               tmrp_615.nivel,
                                      tmrp_615.grupo,                   tmrp_615.subgrupo,
                                      tmrp_615.item,                    tmrp_615.tonalidade,
                                      tmrp_615.cor_formulario,          tmrp_615.tipo_servico,
                                      tmrp_615.codigo_deposito,         tmrp_615.transacao,
                                      tmrp_615.alternativa,             tmrp_615.roteiro,
                                      tmrp_615.peso_padrao_rolo,        tmrp_615.largura,
                                      tmrp_615.gramatura,               tmrp_615.rendimento,
                                      tmrp_615.codigo_acomp,            tmrp_615.sequencia_principal,
                                      tmrp_615.tipo_tecido,             tmrp_615.tipo_natureza,
                                      tmrp_615.grupo_tingimento,        tmrp_615.consumo,
                                      tmrp_615.origem_tempo,            tmrp_615.data_entrega,
                                      decode(tmrp_615.tempo_unitario, tmrp_615.tempo_tingimento,tmrp_615.tempo_tingimento, tmrp_615.tempo_unitario * (tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao)) tempo_tingimento,
                                      tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao saldo
                              from tmrp_615,  (select tmrp_615.pedido_venda, tmrp_615.sequencia_principal, nvl(sum(tmrp_615.qtde_pedido),0) qtde_pedido
                                               from tmrp_615
                                               where tmrp_615.nome_programa       = 'pcpb_f041'
                                                 and tmrp_615.codigo_usuario      = p_codigo_usuario
                                                 and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                                                 and tmrp_615.codigo_empresa      = p_codigo_empresa
                                                 and tmrp_615.usuario             = p_usuario
                                                 and tmrp_615.tipo_registro       = 100
                                                 and tmrp_615.sequencia_principal > 0
                                                 and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                                                 and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                                                 and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                                               group by tmrp_615.pedido_venda, tmrp_615.sequencia_principal) acompanhamentos
                              where tmrp_615.pedido_venda        = acompanhamentos.pedido_venda (+)
                                and tmrp_615.seq_item_pedido     = acompanhamentos.sequencia_principal (+)
                                and tmrp_615.grupo_tingimento    = v_grupo_tingimento
                                and tmrp_615.item                = v_item
                                and tmrp_615.grupo_maquina       = reg_lacunas.grupo_maquina
                                and tmrp_615.subgrupo_maquina    = reg_lacunas.subgrupo_maquina
                                and tmrp_615.numero_maquina      = reg_lacunas.numero_maquina
                                --and ((tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao) + v_lote_acumulado + decode(acompanhamentos.qtde_pedido,null,0,acompanhamentos.qtde_pedido)) < v_capacidade_maxima
                                and tmrp_615.qtde_pedido - tmrp_615.qtde_faturada - tmrp_615.qtde_estoque - tmrp_615.qtde_destinada - tmrp_615.qtde_programacao > 0
                                and ((tmrp_615.qtde_faturada + tmrp_615.qtde_estoque + tmrp_615.qtde_destinada + tmrp_615.qtde_programacao) * 100) / decode(tmrp_615.qtde_pedido,0,1,tmrp_615.qtde_pedido) < p_percentual_atendimento
                                and tmrp_615.categoria           in (0,2) -- tinturaria e fundo de estampado
                                and tmrp_615.sequencia_principal = 0 -- somente itens principais
                                and (tmrp_615.seq_oc             < decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) or decode(tmrp_615.categoria,0,p_lotes_tinturaria,p_lotes_fundo) = 0)
                                and tmrp_615.nome_programa       = 'pcpb_f041'
                                and tmrp_615.codigo_usuario      = p_codigo_usuario
                                and tmrp_615.nr_solicitacao      = p_codigo_solicitacao
                                and tmrp_615.codigo_empresa      = p_codigo_empresa
                                and tmrp_615.usuario             = p_usuario
                                and tmrp_615.tipo_registro       = 100
                              order by saldo desc)
                              loop
                                 if reg_tecidos_complementares.tempo_tingimento = 0
                                 then
                                    v_controle_erros := criticar(reg_tecidos_complementares.pedido_venda,   reg_tecidos_complementares.seq_item_pedido,  reg_tecidos_complementares.categoria,
                                                                 reg_tecidos_complementares.data_entrega,   reg_tecidos_complementares.nivel,            reg_tecidos_complementares.grupo,
                                                                 reg_tecidos_complementares.subgrupo,       reg_tecidos_complementares.item,             reg_tecidos_complementares.nome_cliente,
                                                                 reg_tecidos_complementares.saldo,          reg_lacunas.grupo_maquina,                   reg_lacunas.subgrupo_maquina,
                                                                 reg_lacunas.numero_maquina,                320,                                         'Tempo de producao nao definido',
                                                                 1);
                                    goto tecidos_adicionais;
                                 end if;

                                 if v_lote_acumulado + reg_tecidos_complementares.saldo + (reg_tecidos_complementares.saldo * (get_percentual_acompanhamento(reg_tecidos_complementares.pedido_venda,reg_tecidos_complementares.seq_item_pedido,reg_tecidos_complementares.categoria,reg_lacunas.grupo_maquina,reg_lacunas.subgrupo_maquina,reg_lacunas.numero_maquina) / 100)) < v_capacidade_maxima
                                 then
                                    v_saldo_complementar := reg_tecidos_complementares.saldo;
                                 else
                                    v_saldo_complementar := v_capacidade_maxima - v_lote_acumulado - ((v_capacidade_maxima - v_lote_acumulado) * (get_percentual_acompanhamento(reg_tecidos_complementares.pedido_venda,reg_tecidos_complementares.seq_item_pedido,reg_tecidos_complementares.categoria,reg_lacunas.grupo_maquina,reg_lacunas.subgrupo_maquina,reg_lacunas.numero_maquina) / 100));
                                 end if;

                                 if v_saldo_complementar > 0
                                 then
                                    v_controle_erros := processar_tecidos(reg_lacunas.grupo_maquina,                      reg_lacunas.subgrupo_maquina,
                                                                          reg_lacunas.numero_maquina,                     reg_tecidos_complementares.pedido_venda,
                                                                          reg_tecidos_complementares.seq_item_pedido,     reg_tecidos_complementares.categoria,
                                                                          reg_tecidos_complementares.nivel,               reg_tecidos_complementares.grupo,
                                                                          reg_tecidos_complementares.subgrupo,            reg_tecidos_complementares.item,
                                                                          reg_tecidos_complementares.alternativa,         reg_tecidos_complementares.roteiro,
                                                                          v_saldo_complementar,                           reg_tecidos_complementares.peso_padrao_rolo,
                                                                          reg_tecidos_complementares.largura,             reg_tecidos_complementares.gramatura,
                                                                          reg_tecidos_complementares.rendimento,          reg_tecidos_complementares.codigo_deposito,
                                                                          reg_tecidos_complementares.transacao,           reg_tecidos_complementares.codigo_acomp,
                                                                          reg_tecidos_complementares.sequencia_principal, reg_tecidos_complementares.cor_formulario,
                                                                          reg_tecidos_complementares.tipo_servico,        reg_tecidos_complementares.tipo_tecido,
                                                                          reg_tecidos_complementares.consumo);
                                 end if;
                              end loop;
                           end if;
                        else
                           v_controle_erros := processar_tecidos(reg_lacunas.grupo_maquina,         reg_lacunas.subgrupo_maquina,
                                                                 reg_lacunas.numero_maquina,        v_pedido_venda,
                                                                 v_seq_item_pedido,                 v_categoria,
                                                                 v_nivel,                           v_grupo,
                                                                 v_subgrupo,                        v_item,
                                                                 v_alternativa,                     v_roteiro,
                                                                 v_capacidade_maxima - v_lote_acumulado - (v_capacidade_maxima * (get_percentual_acompanhamento(v_pedido_venda,v_seq_item_pedido,v_categoria,reg_lacunas.grupo_maquina,reg_lacunas.subgrupo_maquina,reg_lacunas.numero_maquina) / 100)),
                                                                 v_peso_padrao_rolo,
                                                                 v_largura,                         v_gramatura,
                                                                 v_rendimento,                      v_codigo_deposito,
                                                                 v_transacao,                       v_codigo_acomp,
                                                                 v_sequencia_principal,             v_cor_formulario,
                                                                 v_tipo_servico,                    v_tipo_tecido,
                                                                 v_consumo);
                        end if;
                     end if;
                  else
                     -- nao tendo encontrado tecido para programacao - mesmo sem criterio de repeticao -
                     -- abandona a analise da lacuna.
                     if v_tentativa = 3
                     then
                        v_analisar_lacuna := false;

                        -- se a lacuna nao pode ser preenchida e nao houver lacunas posteriores no dia,
                        -- abandona a analise da maquina
                        if reg_lacunas.sequencia = v_ultima_sequencia
                        then
                           v_analisar_maquina := false;
                        end if;
                     end if;
                  end if;

                  if v_lote_acumulado < v_capacidade_minima
                  then
                     if v_contador_ordens_unir > 0
                     then
                        v_aplica_antecipacao := false;

                        -- se a capacidade minima nao foi atingida, a programacao por item
                        -- permite a aplicacao do percentual de antecipacao
                        if p_forma_programacao = 1 -- Por item
                        then
                           v_antecipacao := 0;

                           if v_categoria = 0
                           then
                              if v_lote_acumulado + (v_lote_acumulado * (p_antecipacao_tinturaria / 100)) >= v_capacidade_minima
                              then
                                 v_aplica_antecipacao := true;
                                 v_antecipacao := p_antecipacao_tinturaria;
                              end if;
                           elsif v_categoria = 1
                           then
                              if v_lote_acumulado + (v_lote_acumulado * (p_antecipacao_estamparia / 100)) >= v_capacidade_minima
                              then
                                 v_aplica_antecipacao := true;
                                 v_antecipacao := p_antecipacao_estamparia;
                              end if;
                           else
                              if v_lote_acumulado + (v_lote_acumulado * (p_antecipacao_fundo / 100)) >= v_capacidade_minima
                              then
                                 v_aplica_antecipacao := true;
                                 v_antecipacao := p_antecipacao_fundo;
                              end if;
                           end if;

                           if v_aplica_antecipacao = true
                           then
                              v_controle_erros := aplicar_percentual_antecipacao(v_antecipacao,v_peso_padrao_rolo,v_codigo_deposito,v_transacao);

                              if v_categoria <> 1
                              then
                                 v_controle_erros := gerar_ot(reg_lacunas.data_recurso,      reg_lacunas.hora_inicio,      v_data_hora_termino,
                                                              v_data_hora_termino,           v_tempo_tingimento,           reg_lacunas.grupo_maquina,
                                                              reg_lacunas.subgrupo_maquina,  reg_lacunas.numero_maquina);

                              end if;
                           end if;
                        end if;

                        if v_aplica_antecipacao = false
                        then
                           v_controle_erros:= eliminar_obs_em_geracao;

                           -- reinicializa collection
                           ordens_beneficiamento_unir := array_vazio;
                           ordens_beneficiamento_geral := array_vazio;
                           v_contador_ordens_unir := 0;
                           v_contador_ordens_geral := 0;

                           -- se nao alcancou a capacidade minima e a analise estiver na ultima lacuna do dia,
                           -- com tecido obtido sem criterio de repeticao, abandona a analise da maquina
                           if reg_lacunas.sequencia = v_ultima_sequencia
                           and v_tentativa = 3
                           then
                              v_analisar_maquina := false;
                           end if;
                        end if;
                     end if;
                  else
                     if v_contador_ordens_unir > 0
                     then
                        v_controle_erros := gerar_ot(to_date(to_char(reg_lacunas.data_recurso,'dd-mm-yyyy') || ' ' || to_char(reg_lacunas.hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi'),
                                                     to_date(to_char(reg_lacunas.data_recurso,'dd-mm-yyyy') || ' ' || to_char(reg_lacunas.hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi'),
                                                     v_data_hora_termino,
                                                     v_data_hora_termino,       v_tempo_tingimento,           reg_lacunas.grupo_maquina,
                                                     reg_lacunas.subgrupo_maquina,  reg_lacunas.numero_maquina);

                        v_analisar_lacuna := false;

                        -- reinicializa collection
                        ordens_beneficiamento_unir := array_vazio;
                        ordens_beneficiamento_geral := array_vazio;
                        v_contador_ordens_unir := 0;
                        v_contador_ordens_geral := 0;
                     end if;
                  end if;
               end loop; -- loop persistencia lacuna
            end loop; -- loop lacunas
         end loop; -- loop persistencia maquina
      end loop; -- loop recursos

      if v_contador = 0
      then
         commit;
         v_data_analise := v_data_analise + 1;
      end if;
   end loop;

   -- Se a programacao do fundo de estampados nao foi
   -- selecionada, somente o estampado e programado.
   if p_opcao_estampados <> 3
   then
      v_controle_erros := processar_estampado_simples;
   end if;

   commit;

   return(v_controle_erros);
end inter_fn_041_run;



 

/

exec inter_pr_recompile;

