declare tmpInt number;

cursor parametro_c is select codigo_empresa from fatu_500;

begin
  for reg_parametro in parametro_c
  loop
    select nvl(count(*),0) into tmpInt
    from fatu_500
    where fatu_500.codigo_empresa = reg_parametro.codigo_empresa;

    if(tmpInt > 0)
    then
      begin
        insert into empr_008 ( codigo_empresa, param, val_str) 
            values (reg_parametro.codigo_empresa, 'reinf.caminhoArquivos', ' ');
        insert into empr_008 ( codigo_empresa, param, val_str) 
            values (reg_parametro.codigo_empresa, 'reinf.nomePrestadora', ' ');
        insert into empr_008 ( codigo_empresa, param, val_str) 
            values (reg_parametro.codigo_empresa, 'reinf.nomeContatoPrestadora', ' ');
        insert into empr_008 ( codigo_empresa, param, val_str) 
            values (reg_parametro.codigo_empresa, 'reinf.numTelPrestadora', ' ');
        insert into empr_008 ( codigo_empresa, param, val_str) 
            values (reg_parametro.codigo_empresa, 'reinf.emailPrestadora', ' ');
        insert into empr_008 ( codigo_empresa, param, val_str) 
            values (reg_parametro.codigo_empresa, 'reinf.cnpjPrestadora', ' ');

        insert into empr_008 ( codigo_empresa, param, val_int) 
            values (reg_parametro.codigo_empresa, 'reinf.ambiente', '3');
        insert into empr_008 ( codigo_empresa, param, val_int) 
            values (reg_parametro.codigo_empresa, 'reinf.classTrib', '0');
        insert into empr_008 ( codigo_empresa, param, val_int) 
            values (reg_parametro.codigo_empresa, 'reinf.entregaEcd', '0');
        insert into empr_008 ( codigo_empresa, param, val_int) 
            values (reg_parametro.codigo_empresa, 'reinf.desoneraFolha', '0');
        insert into empr_008 ( codigo_empresa, param, val_int) 
            values (reg_parametro.codigo_empresa, 'reinf.acordoIsentMulta', '0');
        insert into empr_008 ( codigo_empresa, param, val_int) 
            values (reg_parametro.codigo_empresa, 'reinf.sitPessoaJurid', '0');
        
      end;
    end if;

  end loop;
  commit;
end;
/
