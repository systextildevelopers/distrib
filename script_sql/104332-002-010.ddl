insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f446', 'Guia de Recolhimento de ICMS - E116',0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f446', ' ' ,1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'obrf_f446', ' ' ,1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Guia de Recolhimento de ICMS - E116'
where hdoc_036.codigo_programa = 'obrf_f446'
  and hdoc_036.locale          = 'es_ES';

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f447', 'Informações Adicionais da Apuração - E115',0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f447', ' ' ,1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'obrf_f447', ' ' ,1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Informações Adicionais da Apuração - E115'
where hdoc_036.codigo_programa = 'obrf_f447'
  and hdoc_036.locale          = 'es_ES';
  
  
insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f444', 'Processo de Apuração de ICMS',0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f444', ' ' ,1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'obrf_f444', ' ' ,1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Processo de Apuração de ICMS'
where hdoc_036.codigo_programa = 'obrf_f444'
  and hdoc_036.locale          = 'es_ES';
  
commit;
