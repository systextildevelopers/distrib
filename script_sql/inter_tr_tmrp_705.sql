
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_705" 
   before insert or
          delete or
          update of perc_distribuicao
   on tmrp_705
   for each row

declare

   v_qtde_distribuicao tmrp_700.qtde_previsao%type;
   v_qtde_distribuicao_old tmrp_700.qtde_previsao%type;
   v_alternativa basi_050.alternativa_item%type;
   v_existe_710 integer;

begin

   if inserting
   then
      select ((tmrp_700.qtde_previsao * :new.perc_distribuicao) /100)
      into v_qtde_distribuicao
      from tmrp_700
      where tmrp_700.mes      = :new.mes
        and tmrp_700.ano      = :new.ano
        and tmrp_700.nivel    = :new.nivel
        and tmrp_700.grupo    = :new.grupo
        and tmrp_700.subgrupo = :new.subgrupo
        and tmrp_700.item     = :new.item;

      for basi010 in (select basi_010.nivel_estrutura,
                             basi_010.grupo_estrutura,
                             basi_010.subgru_estrutura,
                             basi_010.item_estrutura,
                             basi_010.numero_alternati
                      from basi_010
                      where basi_010.nivel_estrutura   = :new.nivel
                        and basi_010.grupo_estrutura   = :new.grupo
                        and (basi_010.subgru_estrutura = :new.subgrupo or :new.subgrupo = '000')
                        and (basi_010.item_estrutura   = :new.item or :new.item = '000000')
                      )
      loop
         -- Procedure que executar a explosao da estrutura do produto
         -- que sera produzido.
         inter_pr_expl_estr_tmrp_710(basi010.nivel_estrutura,
                                     basi010.grupo_estrutura,
                                     basi010.subgru_estrutura,
                                     basi010.item_estrutura,
                                     basi010.numero_alternati,
                                     :new.periodo_producao,
                                     v_qtde_distribuicao,
                                     0,
                                     0,
                                     2,
                                     0,
                                     0,
                                     0.00,
                                     0,
                                    'pcpb');

         --Insere o tecido cadastrado na tmrp_700

         select count(*)
         into v_existe_710
         from tmrp_710
         where tmrp_710.periodo_producao    = :new.periodo_producao
           and tmrp_710.area_producao       = 2
           and tmrp_710.nivel_estrutura     = basi010.nivel_estrutura
           and tmrp_710.grupo_estrutura     = basi010.grupo_estrutura
           and tmrp_710.subgru_estrutura    = basi010.subgru_estrutura
           and tmrp_710.item_estrutura      = basi010.item_estrutura
           and tmrp_710.sequencia_estrutura = 0;
         if v_existe_710 = 0
         then
            -- Atualiza tabela de planejamento.
            insert into tmrp_710
                   (periodo_producao,    area_producao,
                    nivel_estrutura,     grupo_estrutura,
                    subgru_estrutura,    item_estrutura,
                    qtde_reservada,      consumo,
                    sequencia_estrutura)
            values (:new.periodo_producao,     2,
                    basi010.nivel_estrutura,   basi010.grupo_estrutura,
                    basi010.subgru_estrutura,  basi010.item_estrutura,
                    v_qtde_distribuicao,       1,
                    0);
         else
            update tmrp_710
            set tmrp_710.qtde_reservada =  v_qtde_distribuicao
            where tmrp_710.periodo_producao    = :new.periodo_producao
              and tmrp_710.area_producao       = 2
              and tmrp_710.nivel_estrutura     = basi010.nivel_estrutura
              and tmrp_710.grupo_estrutura     = basi010.grupo_estrutura
              and tmrp_710.subgru_estrutura    = basi010.subgru_estrutura
              and tmrp_710.item_estrutura      = basi010.item_estrutura
              and tmrp_710.sequencia_estrutura = 0;
         end if;
      end loop;
   end if;

   if updating
   then

      select (((tmrp_700.qtde_previsao * :old.perc_distribuicao) /100) * -1)
      into v_qtde_distribuicao_old
      from tmrp_700
      where tmrp_700.mes      = :new.mes
        and tmrp_700.ano      = :new.ano
        and tmrp_700.nivel    = :new.nivel
        and tmrp_700.grupo    = :new.grupo
        and tmrp_700.subgrupo = :new.subgrupo
        and tmrp_700.item     = :new.item;

      select (((tmrp_700.qtde_previsao * :new.perc_distribuicao) /100))
      into v_qtde_distribuicao
      from tmrp_700
      where tmrp_700.mes      = :new.mes
        and tmrp_700.ano      = :new.ano
        and tmrp_700.nivel    = :new.nivel
        and tmrp_700.grupo    = :new.grupo
        and tmrp_700.subgrupo = :new.subgrupo
        and tmrp_700.item     = :new.item;

     for basi010 in (select basi_010.nivel_estrutura,
                            basi_010.grupo_estrutura,
                            basi_010.subgru_estrutura,
                            basi_010.item_estrutura,
                            basi_010.numero_alternati
                     from basi_010
                     where basi_010.nivel_estrutura   = :new.nivel
                       and basi_010.grupo_estrutura   = :new.grupo
                       and (basi_010.subgru_estrutura = :new.subgrupo or :new.subgrupo = '000')
                       and (basi_010.item_estrutura   = :new.item or :new.item = '000000')
                     )
     loop
        -- Procedure que executar a explosao da estrutura do produto
        -- que sera produzido.
        inter_pr_expl_estr_tmrp_710(basi010.nivel_estrutura,
                                    basi010.grupo_estrutura,
                                    basi010.subgru_estrutura,
                                    basi010.item_estrutura,
                                    basi010.numero_alternati,
                                   :new.periodo_producao,
                                   v_qtde_distribuicao_old,
                                   0,
                                   0,
                                   2,
                                   0,
                                   0,
                                   0.00,
                                   0,
                                  'pcpb');


        -- Procedure que executar a explosao da estrutura do produto
        -- que sera produzido.
        inter_pr_expl_estr_tmrp_710(basi010.nivel_estrutura,
                                    basi010.grupo_estrutura,
                                    basi010.subgru_estrutura,
                                    basi010.item_estrutura,
                                    basi010.numero_alternati,
                                   :new.periodo_producao,
                                    v_qtde_distribuicao,
                                    0,
                                    0,
                                    2,
                                    0,
                                    0,
                                    0.00,
                                    0,
                                   'pcpb');

         --atualiza o tecido cadastrado na tmrp_700

         select count(*)
         into v_existe_710
         from tmrp_710
         where tmrp_710.periodo_producao    = :new.periodo_producao
           and tmrp_710.area_producao       = 2
           and tmrp_710.nivel_estrutura     = basi010.nivel_estrutura
           and tmrp_710.grupo_estrutura     = basi010.grupo_estrutura
           and tmrp_710.subgru_estrutura    = basi010.subgru_estrutura
           and tmrp_710.item_estrutura      = basi010.item_estrutura
           and tmrp_710.sequencia_estrutura = 0;
         if v_existe_710 > 0
         then
            update tmrp_710
            set tmrp_710.qtde_reservada = v_qtde_distribuicao
            where tmrp_710.periodo_producao    = :new.periodo_producao
              and tmrp_710.area_producao       = 2
              and tmrp_710.nivel_estrutura     = basi010.nivel_estrutura
              and tmrp_710.grupo_estrutura     = basi010.grupo_estrutura
              and tmrp_710.subgru_estrutura    = basi010.subgru_estrutura
              and tmrp_710.item_estrutura      = basi010.item_estrutura
              and tmrp_710.sequencia_estrutura = 0;
         end if;
     end loop;
   end if;

   if deleting
   then

      select (((tmrp_700.qtde_previsao * :old.perc_distribuicao) /100) * -1)
      into v_qtde_distribuicao_old
      from tmrp_700
      where tmrp_700.mes      = :old.mes
        and tmrp_700.ano      = :old.ano
        and tmrp_700.nivel    = :old.nivel
        and tmrp_700.grupo    = :old.grupo
        and tmrp_700.subgrupo = :old.subgrupo
        and tmrp_700.item     = :old.item;

      for basi010 in (select basi_010.nivel_estrutura,
                             basi_010.grupo_estrutura,
                             basi_010.subgru_estrutura,
                             basi_010.item_estrutura,
                             basi_010.numero_alternati
                      from basi_010
                      where basi_010.nivel_estrutura   = :old.nivel
                        and basi_010.grupo_estrutura   = :old.grupo
                        and (basi_010.subgru_estrutura = :old.subgrupo or :old.subgrupo = '000')
                        and (basi_010.item_estrutura   = :old.item or :old.item = '000000')
                      )
      loop
         -- Procedure que executar a explosao da estrutura do produto
         -- que sera produzido.
         inter_pr_expl_estr_tmrp_710(basi010.nivel_estrutura,
                                     basi010.grupo_estrutura,
                                     basi010.subgru_estrutura,
                                     basi010.item_estrutura,
                                     basi010.numero_alternati,
                                     :old.periodo_producao,
                                     v_qtde_distribuicao_old,
                                     0,
                                     0,
                                     2,
                                     0,
                                     0,
                                     0.00,
                                     0,
                                    'pcpb');

         --atualiza o tecido cadastrado na tmrp_700

         select count(*)
         into v_existe_710
         from tmrp_710
         where tmrp_710.periodo_producao    = :old.periodo_producao
           and tmrp_710.area_producao       = 2
           and tmrp_710.nivel_estrutura     = basi010.nivel_estrutura
           and tmrp_710.grupo_estrutura     = basi010.grupo_estrutura
           and tmrp_710.subgru_estrutura    = basi010.subgru_estrutura
           and tmrp_710.item_estrutura      = basi010.item_estrutura
           and tmrp_710.sequencia_estrutura = 0;
         if v_existe_710 > 0
         then
            update tmrp_710
            set tmrp_710.qtde_reservada        = 0.00
            where tmrp_710.periodo_producao    = :old.periodo_producao
              and tmrp_710.area_producao       = 2
              and tmrp_710.nivel_estrutura     = basi010.nivel_estrutura
              and tmrp_710.grupo_estrutura     = basi010.grupo_estrutura
              and tmrp_710.subgru_estrutura    = basi010.subgru_estrutura
              and tmrp_710.item_estrutura      = basi010.item_estrutura
              and tmrp_710.sequencia_estrutura = 0;
         end if;
      end loop;
   end if;
end inter_tr_tmrp_705;
-- ALTER TRIGGER "INTER_TR_TMRP_705" ENABLE
 

/

exec inter_pr_recompile;

