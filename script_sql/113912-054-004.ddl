alter table rcnb_755
  drop constraint pk_rcnb_755;

drop index pk_rcnb_755;

alter table rcnb_755 drop column id_execucao;
alter table rcnb_755 add id_execucao varchar2(20) default 0;

alter table rcnb_755
  add constraint pk_rcnb_755 primary key (codigo_empresa, nivel_estrutura, grupo_estrutura, subgru_estrutura, item_estrutura, nome_usuario, colecao, codigo_agrupador_cc, 
  descricao_agrupador, alternativa, tipo_analise, faixa_etaria_custos,id_execucao);

exec inter_pr_recompile;
