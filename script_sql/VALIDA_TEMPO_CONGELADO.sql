create or replace function Valida_Tempo_Congelado(p_pedido_venda in number, p_data_baixa_hoje in date)
return date
is
    
    v_tem_retorno_sql number := 0;
    v_data_retorno    date    := null;
    v_data_movimento  date    := null;
    v_per_estq_emprsa date    := null;

begin
    select
        count(*)
    into v_tem_retorno_sql
    from pcpc_865
    where pcpc_865.pedido_venda    =  p_pedido_venda
        and pcpc_865.per_movto_ini <= p_data_baixa_hoje
        and pcpc_865.per_movto_fim >= p_data_baixa_hoje
        and pcpc_865.ativ_inativ   = 1;

    if v_tem_retorno_sql <> 0 then
        select
            data_movto
        into v_data_movimento
        from pcpc_865
        where pcpc_865.pedido_venda    =  p_pedido_venda
            and pcpc_865.per_movto_ini <= p_data_baixa_hoje
            and pcpc_865.per_movto_fim >= p_data_baixa_hoje
            and pcpc_865.ativ_inativ   = 1;

        select
            empr_001.periodo_estoque
        into v_per_estq_emprsa
        from empr_001;

        if (v_per_estq_emprsa > v_data_movimento) then
            v_data_movimento := p_data_baixa_hoje;
            --raise_application_error(-20000,'Atenção! A Data Congelada se Encontra Dentro de um Período de Estoque Já Fechado. A Nova Data Adotada Para a Movimentação: '|| p_data_baixa_hoje);
        end if;

        v_data_retorno := p_data_baixa_hoje;
    else
        v_data_retorno := p_data_baixa_hoje;
    end if;

    return v_data_retorno;
end;

/
