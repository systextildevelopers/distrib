
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_847" 
   after insert
      or delete
      or update of cor, codigo_desenho, comb_desenho
on basi_847
for each row

declare
   v_item_comp             basi_013.item_comp%type;

   v_nro_reg_new       number;
   v_nro_reg_old       number;

begin
   /*
      Controles da tabela basi_039
   */
   if updating
   then
      select nvl(count(*),0)
      into v_nro_reg_new
      from basi_039
      where basi_039.codigo_projeto    = :new.codigo_projeto
        and basi_039.combinacao_item   = :new.combinacao_item
        and basi_039.alternativa_prod  = :new.alternativa_prod
        and basi_039.codigo_desenho    = :new.codigo_desenho
        and basi_039.comb_desenho      = :new.comb_desenho;

      select nvl(count(*),0)
      into v_nro_reg_old
      from basi_039
      where basi_039.codigo_projeto    = :new.codigo_projeto
        and basi_039.combinacao_item   = :new.combinacao_item
        and basi_039.alternativa_prod  = :new.alternativa_prod
        and basi_039.codigo_desenho    = :old.codigo_desenho
        and basi_039.comb_desenho      = :old.comb_desenho;

      if  :new.codigo_desenho     is not null
      and :new.codigo_desenho     <> ' '
      and v_nro_reg_new           > 0
      and v_nro_reg_old           > 0
      and (:old.codigo_desenho    <> :new.codigo_desenho
      or   :old.comb_desenho      <> :new.comb_desenho)
      then
         begin
            delete from basi_039
            where basi_039.codigo_projeto    = :new.codigo_projeto
              and basi_039.combinacao_item   = :new.combinacao_item
              and basi_039.alternativa_prod  = :new.alternativa_prod
              and basi_039.codigo_desenho    = :new.codigo_desenho
              and basi_039.comb_desenho      = :new.comb_desenho;
         exception when OTHERS then
            raise_application_error (-20000, 'Nao excluiu a tabela (BASI_039-inter_tr_basi_847)' || SQLERRM);
         end;
      end if;

      if  :new.codigo_desenho     is not null
      and :new.codigo_desenho     <> ' '
      and v_nro_reg_old           > 0
      and (:old.codigo_desenho    <> :new.codigo_desenho
      or   :old.comb_desenho      <> :new.comb_desenho)
      then
         begin
            update basi_039
              set basi_039.codigo_desenho    = :new.codigo_desenho,
                  basi_039.comb_desenho      = :new.comb_desenho
            where basi_039.codigo_projeto    = :new.codigo_projeto
              and basi_039.combinacao_item   = :new.combinacao_item
              and basi_039.alternativa_prod  = :new.alternativa_prod
              and basi_039.codigo_desenho    = :old.codigo_desenho
              and basi_039.comb_desenho      = :old.comb_desenho;
         exception when OTHERS then
            raise_application_error (-20000, 'Nao atualizou a tabela (BASI_039-inter_tr_basi_847)' || SQLERRM);
         end;
      end if;
   end if;

   if deleting
   then
      select nvl(count(*),0)
      into v_nro_reg_old
      from basi_039
      where basi_039.codigo_projeto    = :old.codigo_projeto
        and basi_039.combinacao_item   = :old.combinacao_item
        and basi_039.alternativa_prod  = :old.alternativa_prod
        and basi_039.codigo_desenho    = :old.codigo_desenho
        and basi_039.comb_desenho      = :old.comb_desenho;

      if  :old.codigo_desenho is not null
      and :old.codigo_desenho <> ' '
      and v_nro_reg_old       > 0
      then
         begin
            delete from basi_039
            where basi_039.codigo_projeto    = :old.codigo_projeto
              and basi_039.combinacao_item   = :old.combinacao_item
              and basi_039.alternativa_prod  = :old.alternativa_prod
              and basi_039.codigo_desenho    = :old.codigo_desenho
              and basi_039.comb_desenho      = :old.comb_desenho;
         exception when OTHERS then
            raise_application_error (-20000, 'Nao excluiu da tabela (BASI_039-inter_tr_basi_847)');
         end;
      end if;
   end if;

   if inserting or updating
   and 1=5
   then
      /*
        Aprovacao de componente -> SEQ. DE COR
      */
      if  :new.cor is not null
      and :new.cor <> ' '
      then
         for reg_basi_013 in (select basi_013.sequencia_projeto,                   basi_013.alternativa_produto,
                                     basi_013.nivel_item,                          basi_013.grupo_item,
                                     basi_013.subgru_item,                         basi_013.item_item,
                                     basi_013.nivel_comp,                          basi_013.grupo_comp,
                                     basi_013.subgru_comp,                         basi_013.item_comp,
                                     basi_013.sequencia_subprojeto,
                                     nvl(basi_001.cnpj_cliente9,0) cliente9,       nvl(basi_001.cnpj_cliente4,0) cliente4,
                                     nvl(basi_001.cnpj_cliente2,0) cliente2
                              from basi_013, basi_001
                              where  basi_013.codigo_projeto      = :new.codigo_projeto
                                and  basi_013.seq_cor             = :new.seq_cor
                                and  basi_001.codigo_projeto      = basi_013.codigo_projeto
                                and (basi_013.alternativa_produto = :new.alternativa_prod
                                or   :new.alternativa_prod        = 0))
         loop
            v_item_comp := :new.cor;

            if reg_basi_013.subgru_comp <> '000'
            then
               inter_pr_inserir_basi_027_028 (reg_basi_013.nivel_comp,
                                              reg_basi_013.grupo_comp,
                                              reg_basi_013.subgru_comp,
                                              v_item_comp,
                                              :new.codigo_projeto,
                                              reg_basi_013.sequencia_subprojeto,
                                              reg_basi_013.nivel_item,
                                              reg_basi_013.grupo_item,
                                              reg_basi_013.subgru_item,
                                              reg_basi_013.item_item,
                                              reg_basi_013.alternativa_produto,
                                              reg_basi_013.cliente9,
                                              reg_basi_013.cliente4,
                                              reg_basi_013.cliente2);
            else
               for reg_basi_020 in (select basi_020.tamanho_ref
                                    from basi_020
                                    where basi_020.basi030_nivel030 = reg_basi_013.nivel_comp
                                      and basi_020.basi030_referenc = reg_basi_013.grupo_comp)
               loop
                  reg_basi_013.subgru_comp := reg_basi_020.tamanho_ref;

                  inter_pr_inserir_basi_027_028 (reg_basi_013.nivel_comp,
                                                 reg_basi_013.grupo_comp,
                                                 reg_basi_013.subgru_comp,
                                                 v_item_comp,
                                                 :new.codigo_projeto,
                                                 reg_basi_013.sequencia_subprojeto,
                                                 reg_basi_013.nivel_item,
                                                 reg_basi_013.grupo_item,
                                                 reg_basi_013.subgru_item,
                                                 reg_basi_013.item_item,
                                                 reg_basi_013.alternativa_produto,
                                                 reg_basi_013.cliente9,
                                                 reg_basi_013.cliente4,
                                                 reg_basi_013.cliente2);
               end loop;
            end if;
         end loop;
      end if;

      /*
        Aprovacao de componente -> DESENHO
      */
      if  :new.comb_desenho is not null
      and :new.comb_desenho <> ' '
      and 1=5
      then
         for reg_basi_013 in (select basi_013.sequencia_projeto,                   basi_013.alternativa_produto,
                                     basi_013.nivel_item,                          basi_013.grupo_item,
                                     basi_013.subgru_item,                         basi_013.item_item,
                                     basi_013.nivel_comp,                          basi_013.grupo_comp,
                                     basi_013.subgru_comp,                         basi_013.item_comp,
                                     basi_013.sequencia_subprojeto,
                                     nvl(basi_001.cnpj_cliente9,0) cliente9,       nvl(basi_001.cnpj_cliente4,0) cliente4,
                                     nvl(basi_001.cnpj_cliente2,0) cliente2
                              from basi_013, basi_001, basi_039
                              where basi_013.codigo_projeto        = basi_001.codigo_projeto
                                and basi_013.codigo_projeto        = :new.codigo_projeto
                                and basi_013.codigo_desenho        = :new.codigo_desenho
                                and (basi_013.comb_desenho         = :new.comb_desenho
                                or   basi_013.comb_desenho         = '00')
                                and (basi_013.alternativa_produto  = :new.alternativa_prod
                                or   :new.alternativa_prod         = 0)
                                and basi_039.codigo_projeto        = basi_013.codigo_projeto
                                and basi_039.combinacao_item       = :new.combinacao_item
                                and basi_039.alternativa_prod      = :new.alternativa_prod
                                and basi_039.codigo_desenho        = basi_013.codigo_desenho
                                and basi_039.comb_desenho          = :new.comb_desenho
                                and basi_039.sequencia_estrutura   = basi_013.sequencia_estrutura
                                and basi_039.sequencia_variacao    = basi_013.sequencia_variacao
                                and basi_039.alternativa_produto   = basi_013.alternativa_produto
                                and basi_039.selecionado           = 1)
         loop
            v_item_comp := :new.codigo_desenho || :new.comb_desenho;

            if  reg_basi_013.nivel_comp   is not null
            and reg_basi_013.nivel_comp   <> '0'
            and reg_basi_013.grupo_comp   is not null
            and reg_basi_013.grupo_comp   <> '00000'
            and v_item_comp               is not null
            and v_item_comp               <> '000000'
            and v_item_comp               <> ' '
            then
               if reg_basi_013.subgru_comp <> '000'
               then
                  inter_pr_inserir_basi_027_028 (reg_basi_013.nivel_comp,
                                                 reg_basi_013.grupo_comp,
                                                 reg_basi_013.subgru_comp,
                                                 v_item_comp,
                                                 :new.codigo_projeto,
                                                 reg_basi_013.sequencia_subprojeto,
                                                 reg_basi_013.nivel_item,
                                                 reg_basi_013.grupo_item,
                                                 reg_basi_013.subgru_item,
                                                 reg_basi_013.item_item,
                                                 reg_basi_013.alternativa_produto,
                                                 reg_basi_013.cliente9,
                                                 reg_basi_013.cliente4,
                                                 reg_basi_013.cliente2);
               else
                  for reg_basi_020 in (select basi_020.tamanho_ref
                                       from basi_020
                                       where basi_020.basi030_nivel030 = reg_basi_013.nivel_comp
                                         and basi_020.basi030_referenc = reg_basi_013.grupo_comp)
                  loop
                     reg_basi_013.subgru_comp := reg_basi_020.tamanho_ref;

                     inter_pr_inserir_basi_027_028 (reg_basi_013.nivel_comp,
                                                    reg_basi_013.grupo_comp,
                                                    reg_basi_013.subgru_comp,
                                                    v_item_comp,
                                                    :new.codigo_projeto,
                                                    reg_basi_013.sequencia_subprojeto,
                                                    reg_basi_013.nivel_item,
                                                    reg_basi_013.grupo_item,
                                                    reg_basi_013.subgru_item,
                                                    reg_basi_013.item_item,
                                                    reg_basi_013.alternativa_produto,
                                                    reg_basi_013.cliente9,
                                                    reg_basi_013.cliente4,
                                                    reg_basi_013.cliente2);
                  end loop;
               end if;
            end if;
         end loop;
      end if;
   end if;

end inter_tr_basi_847;
-- ALTER TRIGGER "INTER_TR_BASI_847" ENABLE
 

/

exec inter_pr_recompile;

