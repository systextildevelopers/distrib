create or replace trigger inter_tr_obrf_287_alt
before delete or insert or update on obrf_287
for each row

declare
  PRAGMA Autonomous_Transaction;
  enviado number(1);
  id_var number(9);

begin
    BEGIN
      select enviado
        into enviado
      from obrf_285
      where id = (SELECT OBRF_286.ID_OBRF_285 FROM OBRF_286 WHERE ID = :NEW.ID_OBRF_286);
      
      SELECT OBRF_286.ID_OBRF_285 INTO id_var
      FROM OBRF_286 WHERE ID = :NEW.ID_OBRF_286;
    EXCEPTION
    WHEN OTHERS THEN
      BEGIN
        select enviado
          into enviado
        from obrf_285
        where id = (SELECT OBRF_286.ID_OBRF_285 FROM OBRF_286 WHERE ID = :OLD.ID_OBRF_286);
        
        SELECT OBRF_286.ID_OBRF_285 INTO id_var
        FROM OBRF_286 WHERE ID = :OLD.ID_OBRF_286;
      EXCEPTION
      WHEN OTHERS THEN
        enviado := 0;
      END;
    END;

    if enviado = 1
    then
      BEGIN
        update obrf_285 set ENVIADO = 0, indretif = 2
        where obrf_285.id = id_var;
      EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
      END;
    end if;

    COMMIT;
end;
