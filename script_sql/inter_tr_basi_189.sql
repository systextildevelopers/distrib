
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_189" 
before insert or
update of centro_custo, dia_semana, data_excecao, horaini_t1,
	horafim_t1, horaini_t2, horafim_t2, horaini_t3,
	horafim_t3, horaini_t4, horafim_t4, tempo_turno1,
	tempo_turno2, tempo_turno3, tempo_turno4, intervaloini_t1,
	intervalofim_t1, aplicacao_intervalo_t1, intervaloini_t2, intervalofim_t2,
	aplicacao_intervalo_t2, intervaloini_t3, intervalofim_t3, aplicacao_intervalo_t3,
	intervaloini_t4, intervalofim_t4, aplicacao_intervalo_t4
on basi_189
for each row

begin
   -- Logica para gravar a hora temino e hora inicio com uma data valida.
   -- A data definida '16/11/1989', foi definida com base da data de gravacao padrao do Vision.
   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horaini_t1 := to_date('16/11/1989 '||to_char(:new.horaini_t1,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horafim_t1 is not null)
   or (inserting and :new.horafim_t1 is not null)
   then
      :new.horafim_t1 := to_date('16/11/1989 '||to_char(:new.horafim_t1,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horaini_t2 := to_date('16/11/1989 '||to_char(:new.horaini_t2,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horafim_t2 := to_date('16/11/1989 '||to_char(:new.horafim_t2,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horaini_t3 := to_date('16/11/1989 '||to_char(:new.horaini_t3,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horafim_t3 := to_date('16/11/1989 '||to_char(:new.horafim_t3,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horaini_t4 := to_date('16/11/1989 '||to_char(:new.horaini_t4,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.horafim_t4 := to_date('16/11/1989 '||to_char(:new.horafim_t4,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;



   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervaloini_t1 := to_date('16/11/1989 '||to_char(:new.intervaloini_t1,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervalofim_t1 := to_date('16/11/1989 '||to_char(:new.intervalofim_t1,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervaloini_t2 := to_date('16/11/1989 '||to_char(:new.intervaloini_t2,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervalofim_t2 := to_date('16/11/1989 '||to_char(:new.intervalofim_t2,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervaloini_t3 := to_date('16/11/1989 '||to_char(:new.intervaloini_t3,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervalofim_t3 := to_date('16/11/1989 '||to_char(:new.intervalofim_t3,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervaloini_t4 := to_date('16/11/1989 '||to_char(:new.intervaloini_t4,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.horaini_t1 is not null)
   or (inserting and :new.horaini_t1 is not null)
   then
      :new.intervalofim_t4 := to_date('16/11/1989 '||to_char(:new.intervalofim_t4,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

end inter_tr_basi_189;

-- ALTER TRIGGER "INTER_TR_BASI_189" ENABLE
 

/

exec inter_pr_recompile;

