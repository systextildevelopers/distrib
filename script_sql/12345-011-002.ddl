ALTER TABLE MQOP_060 MODIFY PISTA_DISCO1      VARCHAR2(80);
ALTER TABLE MQOP_060 MODIFY PISTA_DISCO2      VARCHAR2(80);
ALTER TABLE MQOP_060 MODIFY PISTA_CILINDRO1   VARCHAR2(80);
ALTER TABLE MQOP_060 MODIFY PISTA_CILINDRO2   VARCHAR2(80);
ALTER TABLE MQOP_060 MODIFY PISTA_CILINDRO3   VARCHAR2(80);
ALTER TABLE MQOP_060 MODIFY PISTA_CILINDRO4   VARCHAR2(80);
ALTER TABLE MQOP_060 MODIFY PISTA_CILINDRO5   VARCHAR2(80);
ALTER TABLE MQOP_060 MODIFY CAMOS_DISCO1      VARCHAR2(160);
ALTER TABLE MQOP_060 MODIFY CAMOS_DISCO2      VARCHAR2(160);
ALTER TABLE MQOP_060 MODIFY CAMOS_CILINDRO1   VARCHAR2(160);
ALTER TABLE MQOP_060 MODIFY CAMOS_CILINDRO2   VARCHAR2(160);
ALTER TABLE MQOP_060 MODIFY CAMOS_CILINDRO3   VARCHAR2(160);
ALTER TABLE MQOP_060 MODIFY CAMOS_CILINDRO4   VARCHAR2(160);
ALTER TABLE MQOP_060 MODIFY CAMOS_CILINDRO5   VARCHAR2(160);

/
exec inter_pr_recompile;
