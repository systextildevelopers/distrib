alter table SUPR_090
  DROP constraint CK_SUPR_090_TIPO_FRETE;

alter table SUPR_090
  add constraint CK_SUPR_090_TIPO_FRETE
  check (tipo_frete in (1,2,3,4,9));

exec inter_pr_recompile;
