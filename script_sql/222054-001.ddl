create or replace view pedi_100_pedi_110
(codigo_empresa, pedido_venda, data_emis_venda, data_entr_venda, cli_ped_cgc_cli9, cli_ped_cgc_cli4, cli_ped_cgc_cli2, cond_pgto_venda, qtde_total_pedi, qtde_saldo_pedi, qtde_afaturar, situacao_venda, valor_saldo_pedi, cod_cancelamento, codigo_moeda, desconto1, desconto2, desconto3, tipo_frete, valor_despesas_pedido, valor_seguro_pedido, valor_frete_pedido, encargos, criterio_qualidade, status_expedicao, expedidor, classificacao_pedido, qtde_sugerida, qtde_atransferir, qtde_emtransito, qtde_transferida, fantasia_cliente, data_liberacao, hora_liberacao, cod_rep_cliente, nome_rep_cliente)
as
select  pedi_100.codigo_empresa,       pedi_100.pedido_venda,
        pedi_100.data_emis_venda,      pedi_100.data_entr_venda,
        pedi_100.cli_ped_cgc_cli9,     pedi_100.cli_ped_cgc_cli4,
        pedi_100.cli_ped_cgc_cli2,     pedi_100.cond_pgto_venda,
        pedi_100.qtde_total_pedi,      pedi_100.qtde_saldo_pedi,
        sum(pedi_110.qtde_afaturar),   pedi_100.situacao_venda,
        pedi_100.valor_saldo_pedi,     pedi_100.cod_cancelamento,
        pedi_100.codigo_moeda,         pedi_100.desconto1,
        pedi_100.desconto2,            pedi_100.desconto3,
        pedi_100.tipo_frete,           pedi_100.valor_despesas_pedido,
        pedi_100.valor_seguro_pedido,  pedi_100.valor_frete_pedido,
        pedi_100.encargos,             pedi_100.criterio_qualidade,
        pedi_100.status_expedicao,     pedi_100.expedidor,
        pedi_100.classificacao_pedido, sum(pedi_110.qtde_sugerida),
        nvl(sum(sub_sql1.qtde_1),0) qtde_atransferir,
        nvl(sum(sub_sql2.qtde_2),0) qtde_emtransito,
        nvl(sum(sub_sql3.qtde_3),0) qtde_transferida,
        pedi_010.fantasia_cliente,
       (select DECODE(max(hdoc_110.data_liberacao), NULL, pedi_100.data_emis_venda, max(hdoc_110.data_liberacao))
          from hdoc_110
         where hdoc_110.empresa   = pedi_100.codigo_empresa
           and hdoc_110.documento = pedi_100.pedido_venda
           and pedi_100.situacao_venda <> 5) as DATA_LIBERACAO,
       (select DECODE(max(hdoc_110.hora_liberacao), NULL, '00:00:00' , to_char(max(hdoc_110.hora_liberacao),'HH24:MI:SS'))
          from hdoc_110
         where hdoc_110.empresa   = pedi_100.codigo_empresa
           and hdoc_110.documento = pedi_100.pedido_venda
           and pedi_100.situacao_venda <> 5
           and hdoc_110.data_liberacao = (select max(hdoc_110.data_liberacao)
                                            from hdoc_110
                                           where hdoc_110.empresa   = pedi_100.codigo_empresa
                                             and hdoc_110.documento = pedi_100.pedido_venda
                                             and pedi_100.situacao_venda <> 5)) as HORA_LIBERACAO,
      pedi_100.cod_rep_cliente,
      pedi_020.nome_rep_cliente
from pedi_110, pedi_100,pedi_010,pedi_020,
    (select pcpt_020.pedido_venda, basi_205.local_deposito,
            nvl(sum(pcpt_020.qtde_quilos_acab),0) qtde_1
     from pcpt_020, basi_205,pedi_010
     where pcpt_020.rolo_estoque = 3
       /* SOMENTE AS SEQ. NAO CANCELADAS */
       and exists (select 1 from pedi_110
                   where pedi_110.pedido_venda     = pcpt_020.pedido_venda
                     and pedi_110.seq_item_pedido  = pcpt_020.seq_item_pedido
                     and pedi_110.cod_cancelamento = 0)
       /* SOMENTE PRODUTOS QUE NAO ESTEJAM NO DEPOSITO DE TRANSICAO*/
       and not exists (select 1 from empr_002
                       where empr_002.deposito_transicao = pcpt_020.codigo_deposito)
       /* SOMENTE PRODUTOS QUE NAO ESTEJAM EM UM DEPOSITO DA EMPRESA DO PEDIDO*/
       and  basi_205.codigo_deposito = pcpt_020.codigo_deposito
       /* SOMENTE PRODUTOS QUE SEJAM EM ELABORACAO */
       and exists (select 1 from pedi_860
                   where pedi_860.niv_prod_elab  = pcpt_020.panoacab_nivel99
                     and pedi_860.gru_prod_elab  = pcpt_020.panoacab_grupo
                     and pedi_860.sub_prod_elab  = pcpt_020.panoacab_subgrupo
                     and pedi_860.item_prod_elab = pcpt_020.panoacab_item)
     group by pcpt_020.pedido_venda, basi_205.local_deposito) sub_sql1,
    (select pcpt_020.pedido_venda,
            nvl(sum(pcpt_020.qtde_quilos_acab),0) qtde_2
     from pcpt_020
         where pcpt_020.rolo_estoque = 3
           /* SOMENTE AS SEQ. NAO CANCELADAS */
           and exists (select 1 from pedi_110
                       where pedi_110.pedido_venda     = pcpt_020.pedido_venda
                         and pedi_110.seq_item_pedido  = pcpt_020.seq_item_pedido
                         and pedi_110.cod_cancelamento = 0)
           /* SOMENTE PRODUTOS QUE ESTEJAM NO DEPOSITO DE TRANSICAO*/
           and exists (select 1 from empr_002
                       where empr_002.deposito_transicao = pcpt_020.codigo_deposito)
           /* SOMENTE PRODUTOS QUE SEJAM EM ELABORACAO */
           and exists (select 1 from pedi_860
                       where pedi_860.niv_prod_elab  = pcpt_020.panoacab_nivel99
                         and pedi_860.gru_prod_elab  = pcpt_020.panoacab_grupo
                         and pedi_860.sub_prod_elab  = pcpt_020.panoacab_subgrupo
                         and pedi_860.item_prod_elab = pcpt_020.panoacab_item)
     group by pcpt_020.pedido_venda) sub_sql2,
    (select pcpt_020.pedido_venda, basi_205.local_deposito,
            nvl(sum(pcpt_020.qtde_quilos_acab),0) qtde_3
     from pcpt_020, basi_205
     where pcpt_020.rolo_estoque = 3
       /* SOMENTE AS SEQ. NAO CANCELADAS */
       and exists (select 1 from pedi_110
                   where pedi_110.pedido_venda     = pcpt_020.pedido_venda
                     and pedi_110.seq_item_pedido  = pcpt_020.seq_item_pedido
                     and pedi_110.cod_cancelamento = 0)
       /* SOMENTE PRODUTOS QUE NAO ESTEJAM NO DEPOSITO DE TRANSICAO*/
       and not exists (select 1 from empr_002
                       where empr_002.deposito_transicao = pcpt_020.codigo_deposito)
       /* SOMENTE PRODUTOS QUE ESTEJAM EM UM DEPOSITO DA EMPRESA DO PEDIDO*/
       and basi_205.codigo_deposito = pcpt_020.codigo_deposito
       /* SOMENTE PRODUTOS QUE SEJAM EM ELABORACAO */
       and exists (select 1 from pedi_860
                   where pedi_860.niv_prod_elab  = pcpt_020.panoacab_nivel99
                     and pedi_860.gru_prod_elab  = pcpt_020.panoacab_grupo
                     and pedi_860.sub_prod_elab  = pcpt_020.panoacab_subgrupo
                     and pedi_860.item_prod_elab = pcpt_020.panoacab_item)
     group by pcpt_020.pedido_venda, basi_205.local_deposito) sub_sql3
where pedi_110.pedido_venda     = pedi_100.pedido_venda
  and sub_sql1.pedido_venda   (+)  = pedi_100.pedido_venda
  and sub_sql1.local_deposito (+) <> pedi_100.codigo_empresa
  and sub_sql2.pedido_venda   (+)  = pedi_100.pedido_venda
  and sub_sql3.pedido_venda   (+)  = pedi_100.pedido_venda
  and sub_sql3.local_deposito (+)  = pedi_100.codigo_empresa
  and pedi_110.cod_cancelamento = 0
  and pedi_100.cli_ped_cgc_cli9    = pedi_010.cgc_9
  and pedi_100.cli_ped_cgc_cli4    = pedi_010.cgc_4
  and pedi_100.cli_ped_cgc_cli2    = pedi_010.cgc_2
  and pedi_100.cod_rep_cliente     = pedi_020.cod_rep_cliente
GROUP BY pedi_100.codigo_empresa,     pedi_100.pedido_venda,
         pedi_100.data_emis_venda,    pedi_100.data_entr_venda,
         pedi_100.cli_ped_cgc_cli9,   pedi_100.cli_ped_cgc_cli4,
         pedi_100.cli_ped_cgc_cli2,   pedi_100.cond_pgto_venda,
         pedi_100.qtde_total_pedi,    pedi_100.qtde_saldo_pedi,
         pedi_100.situacao_venda,     pedi_100.valor_saldo_pedi,
         pedi_100.cod_cancelamento,   pedi_100.codigo_moeda,
         pedi_100.desconto1,          pedi_100.desconto2,
         pedi_100.desconto3,          pedi_100.tipo_frete,
         pedi_100.valor_despesas_pedido, pedi_100.valor_seguro_pedido,
         pedi_100.valor_frete_pedido, pedi_100.encargos,
         pedi_100.criterio_qualidade, pedi_100.status_expedicao,
         pedi_100.expedidor,          pedi_100.classificacao_pedido,
         pedi_010.fantasia_cliente,   pedi_100.cod_rep_cliente,
         pedi_020.nome_rep_cliente;
