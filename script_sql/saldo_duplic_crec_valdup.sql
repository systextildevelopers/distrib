
  CREATE OR REPLACE FUNCTION "SALDO_DUPLIC_CREC_VALDUP" 
        (emp_duplic in fatu_070.codigo_empresa%type,
         num_duplic in fatu_070.num_duplicata%type,
         seq_duplic in fatu_070.seq_duplicatas%type,
         tip_duplic in fatu_070.tipo_titulo%type,
         cg9_duplic in fatu_070.cli_dup_cgc_cli9%type,
         cg4_duplic in fatu_070.cli_dup_cgc_cli4%type,
         cg2_duplic in fatu_070.cli_dup_cgc_cli2%type)
RETURN number
IS
   v_saldo_valdup     number(15,7);
begin
   select valor_duplicata into v_saldo_valdup from fatu_070
   where fatu_070.codigo_empresa            = emp_duplic
     and fatu_070.num_duplicata             = num_duplic
     and fatu_070.seq_duplicatas            = seq_duplic
     and fatu_070.tipo_titulo               = tip_duplic
     and fatu_070.cli_dup_cgc_cli9          = cg9_duplic
     and fatu_070.cli_dup_cgc_cli4          = cg4_duplic
     and fatu_070.cli_dup_cgc_cli2          = cg2_duplic;

   return(v_saldo_valdup);

end saldo_duplic_crec_valdup;

 

/

exec inter_pr_recompile;

