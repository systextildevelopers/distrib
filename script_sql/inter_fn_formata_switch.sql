create or replace FUNCTION inter_fn_formata_switch( v_value in VARCHAR2 ) 
RETURN varchar2 IS
BEGIN
	if v_value = 'Y' then
		RETURN 'S';
	else
		RETURN 'N';
	end if;
END inter_fn_formata_switch;

/

exec inter_pr_recompile;

