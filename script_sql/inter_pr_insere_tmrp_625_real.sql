
  CREATE OR REPLACE PROCEDURE "INTER_PR_INSERE_TMRP_625_REAL" 
   (p_ordem_planejamento           in number,    p_pedido_venda                 in number,
    p_pedido_reserva               in number,    p_codigo_projeto               in varchar2,
    p_seq_projeto_origem           in number,    p_tipo_produto_origem          in varchar2,
    p_seq_produto_origem           in number,    p_nivel_produto_origem         in varchar2,
    p_grupo_produto_origem         in varchar2,  p_subgrupo_produto_origem      in varchar2,
    p_item_produto_origem          in varchar2,  p_alternativa_produto_origem   in number,
    p_tipo_produto                 in varchar2,  p_seq_projeto                  in number,
    p_seq_produto                  in number,    p_nivel_produto                in varchar2,
    p_grupo_produto                in varchar2,  p_subgrupo_produto             in varchar2,
    p_item_produto                 in varchar2,  p_alternativa_produto          in number,
    p_estagio_producao             in number,    p_tipo_calculo                 in number,
    p_consumo                      in number,    p_qtde_reserva_planejada       in number,
    p_qtde_reserva_programada      in number,    p_situacao                     in varchar2,
    p_qtde_areceber_programada     in number,    p_qtde_vendida                 in number,
    p_qtde_adicional               in number,    p_qtde_reposicao               in number,
    p_data_requerida               in date,      p_qtde_produzida_comprada      in number,
    p_qtde_conserto                in number,    p_qtde_2qualidade              in number,
    p_qtde_perdas                  in number,    p_qtde_atendida_adicional      in number,
    p_qtde_atendida_devolvida      in number,    p_data_requerida_prev          in date,
    p_data_requerida_real          in date,      p_data_acertada_compra         in date,
    p_data_acertada_desenv         in date,      p_observacao                   in varchar2,
    p_setor                        in varchar2,  p_relacao_banho                in number,
    p_qtde_atendida_estoque        in number,    p_cons_unid_med_generica       in number,
    p_unidades_reserva_planejada   in number,    p_unidades_reserva_programada  in number,
    p_unidades_areceber_programada in number,    p_tecido_principal             in number,
    p_liquida_saldo                in number,    p_qtde_tecido_real             in number)
is
begin

   begin
   insert into tmrp_625
      (ordem_planejamento,                   pedido_venda,
       pedido_reserva,                       codigo_projeto,
       seq_projeto_origem,                   tipo_produto_origem,
       seq_produto_origem,                   nivel_produto_origem,
       grupo_produto_origem,                 subgrupo_produto_origem,
       item_produto_origem,                  alternativa_produto_origem,
       tipo_produto,                         seq_projeto,
       seq_produto,                          nivel_produto,
       grupo_produto,                        subgrupo_produto,
       item_produto,                         alternativa_produto,
       estagio_producao,                     tipo_calculo,
       consumo,                              qtde_reserva_planejada,
       qtde_reserva_programada,
       situacao,
       qtde_areceber_programada,             qtde_vendida,
       qtde_adicional,                       qtde_reposicao,
       data_requerida,                       qtde_produzida_comprada,
       qtde_conserto,                        qtde_2qualidade,
       qtde_perdas,                          qtde_atendida_adicional,
       qtde_atendida_devolvida,              data_requerida_prev,
       data_requerida_real,                  data_acertada_compra,
       data_acertada_desenv,                 observacao,
       setor,                                relacao_banho,
       qtde_atendida_estoque,                cons_unid_med_generica,
       unidades_reserva_planejada,           unidades_reserva_programada,
       unidades_areceber_programada,         tecido_principal,
       liquida_saldo                        
    )values(
       p_ordem_planejamento,                 p_pedido_venda,
       p_pedido_reserva,                     p_codigo_projeto,
       p_seq_projeto_origem,                 p_tipo_produto_origem,
       p_seq_produto_origem,                 p_nivel_produto_origem,
       p_grupo_produto_origem,               p_subgrupo_produto_origem,
       p_item_produto_origem,                p_alternativa_produto_origem,
       p_tipo_produto,                       p_seq_projeto,
       p_seq_produto,                        p_nivel_produto,
       p_grupo_produto,                      p_subgrupo_produto,
       p_item_produto,                       p_alternativa_produto,
       p_estagio_producao,                   p_tipo_calculo,
       p_consumo,                            p_qtde_tecido_real,
       p_qtde_tecido_real,
       p_situacao,
       p_qtde_areceber_programada,           p_qtde_vendida,
       p_qtde_adicional,                     p_qtde_reposicao,
       p_data_requerida,                     p_qtde_produzida_comprada,
       p_qtde_conserto,                      p_qtde_2qualidade,
       p_qtde_perdas,                        p_qtde_atendida_adicional,
       p_qtde_atendida_devolvida,            p_data_requerida_prev,
       p_data_requerida_real,                p_data_acertada_compra,
       p_data_acertada_desenv,               p_observacao,
       p_setor,                              p_relacao_banho,
       p_qtde_atendida_estoque,              p_cons_unid_med_generica,
       p_unidades_reserva_planejada,         p_unidades_reserva_programada,
       p_unidades_areceber_programada,       p_tecido_principal,
       p_liquida_saldo
    );
   exception when
   OTHERS then
      raise_application_error(-20000,'Nao inseriu tmrp_625 ' || sqlerrm);
   end;
end inter_pr_insere_tmrp_625_real;

 

/

exec inter_pr_recompile;

