CREATE OR REPLACE VIEW "LEAD_ESTAGIOS" ("ORDEM_FIL", "ORDEM_PAI", "PER_PAI", "REF_FIL", "REF_PAI", "ALT_FIL", "ROT_FIL", "PEDIDO_FIL", "QTDEPROGRAMADA", "DATA_PROD_FIL", "DT_INICIAL_FIL", "DT_FINAL_FIL", "DT_INICIAL_PAI", "DT_FINAL_PAI", "DATA_ENTR_VENDA", "CLI_PED_CGC_CLI9", "CLI_PED_CGC_CLI4", "CLI_PED_CGC_CLI2", "COD_PED_CLIENTE", "TIPO_ORDEM_FIL", "TIPO_ORDEM_PAI", "ESTAGIO_GARGALO_FIL", "ESTAGIO_GARGALO_PAI") AS 
select pcpc020.ordem_fil              ordem_fil,
       pcpc020.ordem_pai              ordem_pai,
       pcpc020.per_pai                per_pai,
       pcpc020.ref_fil                ref_fil,
       pcpc020.ref_pai                ref_pai,
       pcpc020.alt_fil                alt_fil,
       pcpc020.rot_fil                rot_fil,
       pcpc020.pedido_fil             pedido_fil,
       pcpc020.qtdeprogramada         qtdeprogramada,
       pcpc020.data_prod_fil          data_prod_fil,
       p700_dta_filha.dt_inicial      dt_inicial_fil,
       p700_dta_filha.dt_final        dt_final_fil,
       p700_dta_pai.dt_inicial        dt_inicial_pai,
       p700_dta_pai.dt_final          dt_final_pai,
       pedi100.data_entr_venda        data_entr_venda,
       pedi100.cli_ped_cgc_cli9       cli_ped_cgc_cli9,
       pedi100.cli_ped_cgc_cli4       cli_ped_cgc_cli4,
       pedi100.cli_ped_cgc_cli2       cli_ped_cgc_cli2,
       pedi100.cod_ped_cliente        cod_ped_cliente,
       pcpc020.tipo_ordem_fil         tipo_ordem_fil,
       pcpc020.tipo_ordem_pai         tipo_ordem_pai,
       p700_est_filha.estagio_gargalo estagio_gargalo_fil,
       p700_est_pai.estagio_gargalo   estagio_gargalo_pai       
from (select pcpc_020.ordem_producao      ordem_fil, 
             pcpc_020.ordem_principal     ordem_pai,   
             pcpc_020.referencia_peca     ref_fil, 
             ord_pai.referencia_peca      ref_pai,
             pcpc_020.periodo_producao    per_fil, 
             ord_pai.periodo_producao     per_pai,
             pcpc_020.alternativa_peca    alt_fil,
             ord_pai.alternativa_peca     alt_pai,
             pcpc_020.roteiro_peca        rot_fil,
             ord_pai.roteiro_peca         rot_pai,
             pcpc_020.pedido_venda        pedido_fil,
             ord_pai.pedido_venda         pedido_pai,
             pcpc_020.DATA_ENTRADA_CORTE data_prod_fil,
             ord_pai.DATA_ENTRADA_CORTE  data_prod_pai,
             pcpc040.qtdeProgramada      qtdeProgramada,
             pcpc_020.tipo_ordem         tipo_ordem_fil,
             ord_pai.tipo_ordem          tipo_ordem_pai
      from pcpc_020, pcpc_020 ord_pai,
            (select pcpc_040.ordem_producao,
                   nvl(sum(pcpc_040.qtde_pecas_prog),0) qtdeProgramada
            from pcpc_040, pcpc_020
            where pcpc_040.ordem_producao = pcpc_020.ordem_producao
              and pcpc_040.codigo_estagio = pcpc_020.ultimo_estagio
            group by pcpc_040.ordem_producao) pcpc040
      where pcpc_020.ordem_principal = ord_pai.ordem_producao
        and pcpc_020.ordem_principal > 0
        and pcpc040.ordem_producao = pcpc_020.ordem_producao
      UNION ALL
      select pcpc_020.ordem_producao    ordem_fil, 
             pcpc_020.ordem_producao    ordem_pai,   
             pcpc_020.referencia_peca   ref_fil, 
             ''                         ref_pai, 
             0                          per_fil, 
             pcpc_020.periodo_producao  per_pai,
             pcpc_020.alternativa_peca  alt_fil,
             0                          alt_pai,
             pcpc_020.roteiro_peca      rot_fil,
             0                          rot_pai,
             0                          pedido_fil,
             pcpc_020.pedido_venda      pedido_pai,
             pcpc_020.DATA_ENTRADA_CORTE data_prod_fil,
             to_date('')                 data_prod_pai,
             pcpc040.qtdeProgramada      qtdeProgramada,
             0                           tipo_ordem_fil,
             pcpc_020.tipo_ordem         tipo_ordem_pai
      from pcpc_020,
            (select pcpc_040.ordem_producao,
                   nvl(sum(pcpc_040.qtde_pecas_prog),0) qtdeProgramada
            from pcpc_040, pcpc_020
            where pcpc_040.ordem_producao = pcpc_020.ordem_producao
              and pcpc_040.codigo_estagio = pcpc_020.ultimo_estagio
            group by pcpc_040.ordem_producao) pcpc040
      where pcpc_020.ordem_principal = 0
        and pcpc040.ordem_producao = pcpc_020.ordem_producao
        and not exists (select 1 
                        from pcpc_020 ord_filha 
                        where ord_filha.ordem_principal = pcpc_020.ordem_producao)) pcpc020,
      (select ordem_producao,
              min(pcpc_700.dt_incial)  dt_inicial,
              max(pcpc_700.dt_final)   dt_final
       from pcpc_700
       group by ordem_producao) p700_dta_pai,
      (select ordem_producao,
              min(pcpc_700.dt_incial)  dt_inicial,
              max(pcpc_700.dt_final)   dt_final
       from pcpc_700
       group by ordem_producao) p700_dta_filha,
      (select ordem_producao,
              nvl(max(pcpc_700.CODIGO_ESTAGIO),0) estagio_gargalo
       from pcpc_700
       where pcpc_700.IND_ESTAGIO_GARGALO = 1
       group by ordem_producao) p700_est_pai,
      (select ordem_producao,
              nvl(max(pcpc_700.CODIGO_ESTAGIO),0) estagio_gargalo
       from pcpc_700
       where pcpc_700.IND_ESTAGIO_GARGALO = 1
       group by ordem_producao) p700_est_filha,
       (select pedi_100.pedido_venda        pedido_venda, 
               pedi_100.data_entr_venda     data_entr_venda,
               pedi_100.cli_ped_cgc_cli9    cli_ped_cgc_cli9,
               pedi_100.cli_ped_cgc_cli4    cli_ped_cgc_cli4,
               pedi_100.cli_ped_cgc_cli2    cli_ped_cgc_cli2,
               pedi_100.cod_ped_cliente     cod_ped_cliente
        from pedi_100
        where pedi_100.pedido_venda > 0) pedi100
where pcpc020.pedido_fil       = pedi100.pedido_venda(+)
  and pcpc020.ordem_fil        = p700_dta_filha.ordem_producao(+)
  and pcpc020.ordem_pai        = p700_dta_pai.ordem_producao(+)
  and pcpc020.ordem_fil        = p700_est_filha.ordem_producao(+)
  and pcpc020.ordem_pai        = p700_est_pai.ordem_producao(+);

  /
