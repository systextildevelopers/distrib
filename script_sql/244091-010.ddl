-- Create table
create table PCPB_224
(
  ordem_producao       NUMBER(9) default 0 not null,
  codigo_estagio       NUMBER(5) default 0 not null,
  codigo_ocorrencia    NUMBER(4) default 0 not null,
  sequencia_lancamento NUMBER(4) default 0 not null,
  sequencia_estagio    NUMBER(3) default 0,
  seq_operacao         NUMBER(4) default 0,
  usuario_lancamento   VARCHAR2(250),
  data_lancamento      DATE,
  hora_lancamento      DATE
);
comment on table PCPB_224 is 'Ocorrencias de Beneficiamento';
comment on column PCPB_224.ordem_producao is 'Ordem de produção.';
comment on column PCPB_224.codigo_estagio is 'Código do Estágio.';
comment on column PCPB_224.codigo_ocorrencia is 'Ocorrências de Beneficiamento.';
comment on column PCPB_224.sequencia_estagio is 'Sequencia de estágio do pacote.';
comment on column PCPB_224.seq_operacao is 'Sequencia de operacão do pacote.';
comment on column PCPB_224.usuario_lancamento is 'Usuário que esta lançando a ocorrência.';
comment on column PCPB_224.data_lancamento is 'Data que esta lançando a ocorrência.';
comment on column PCPB_224.hora_lancamento is 'Hora que esta lançando a ocorrência.';
alter table PCPB_224
  add constraint PK_PCPB_224 primary key (ORDEM_PRODUCAO, CODIGO_ESTAGIO, CODIGO_OCORRENCIA, SEQUENCIA_LANCAMENTO);
alter table PCPB_224
  add constraint REF_PCPB_224_PCPB_223 foreign key (CODIGO_OCORRENCIA)
  references PCPB_223 (CODIGO_OCORRENCIA);
