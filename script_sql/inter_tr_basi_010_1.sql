CREATE OR REPLACE TRIGGER "INTER_TR_BASI_010_1" 
before insert or
       delete or
       update of numero_roteiro, roteiro_custos, numero_alternati, alternativa_custos
on basi_010
for each row
declare
   var1                 number(1);
   v_fator_1            empr_002.fator_1%type;
   v_fator_2            empr_002.fator_2%type;
   v_preco_transitorio  inte_989.preco_transitorio%type;
   v_preco_sugerido     tmrp_645.nvlprecosugerido%type;
   v_preco_venda        inte_989.preco_venda%type;
   v_nvlprecomontagem   basi_030.nvlprecomontagem%type;
   v_sdsobservacao      inte_989.sdsobservacao%type;
   v_e_mail_destino     oper_130.e_mail_destino%type;
   v_codigo_projeto     basi_001.codigo_projeto%type;
   v_cod_solicitacao_dp pedi_665.cod_solicitacao_dp%type;
   v_seq_item_reserva   pedi_666.seq_solicitacao_dp%type;
begin
   begin
      select oper_130.e_mail_destino
      into   v_e_mail_destino
      from oper_130
      where oper_130.nome_programa = 'inte_f984';
   exception
      when no_data_found then
         v_e_mail_destino := '';
   end;

   begin
      select empr_002.fator_1, empr_002.fator_2
         into v_fator_1,       v_fator_2
         from empr_002;
   exception
      when no_data_found then
         v_fator_1 := 0.00;
         v_fator_2 := 0.00;
   end;
   
   if inserting
   then
      if :new.data_cadastro is null
      then
         :new.data_cadastro := sysdate;
      end if;
   end if;
   
   v_codigo_projeto := '000000';
   if inserting or updating
   then
      begin
         select basi_001.codigo_projeto
           into v_codigo_projeto
           from basi_001
          where basi_001.nivel_produto = :new.nivel_estrutura
            and basi_001.grupo_produto = :new.grupo_estrutura;
      exception
         when no_data_found then
            v_codigo_projeto := '000000';
      end;

      begin
         select basi_030.nvlprecomontagem
           into v_nvlprecomontagem
           from basi_030
         where basi_030.nivel_estrutura = :new.nivel_estrutura
           and basi_030.referencia      = :new.grupo_estrutura;
      exception when no_data_found then
         v_nvlprecomontagem := 0.00;
      end;
   end if;

   if  v_codigo_projeto is not null
   and v_codigo_projeto <> '000000'
   then
      begin
         select pedi_665.cod_solicitacao_dp,  pedi_666.seq_solicitacao_dp
           into v_cod_solicitacao_dp,         v_seq_item_reserva
           from pedi_665, pedi_666
          where pedi_665.cod_solicitacao_dp = pedi_666.cod_solicitacao_dp
            and pedi_666.codigo_projeto     = v_codigo_projeto
            and rownum                      = 1;
      exception when no_data_found then
            v_cod_solicitacao_dp := null;
            v_seq_item_reserva   := null;
      end;

      begin
         select tmrp_645.nvlprecosugerido
           into v_preco_sugerido
           from tmrp_640,tmrp_645
          where tmrp_640.numero_reserva     = tmrp_645.numero_reserva
            and tmrp_640.cod_solicitacao_dp = v_cod_solicitacao_dp
            and tmrp_645.seq_item_reserva   = v_seq_item_reserva
            and rownum                      = 1;
      exception when no_data_found then
            v_preco_sugerido := 0;
      end;
   end if;

   if trim(v_e_mail_destino) is not null and
     :new.nivel_estrutura = 1
   then
      if inserting
      then
         begin
            select 1
              into var1
              from inte_989
             where inte_989.nivel_produto  = :new.nivel_estrutura
               and inte_989.grupo_produto  = :new.grupo_estrutura
               and inte_989.subgru_produto = :new.subgru_estrutura
               and inte_989.item_produto   = :new.item_estrutura;
         exception
         when no_data_found then
            begin
               select inte_989.preco_transitorio, inte_989.preco_venda,
                      inte_989.sdsobservacao
               into   v_preco_transitorio,        v_preco_venda,
                      v_sdsobservacao
               from inte_989
               where inte_989.nivel_produto   =  :new.nivel_estrutura
                 and inte_989.grupo_produto   =  :new.grupo_estrutura
                 and (inte_989.subgru_produto <> :new.subgru_estrutura
                  or inte_989.item_produto    <> :new.item_estrutura)
                 and rownum = 1;
            exception
            when no_data_found then
               v_preco_transitorio := 0.00;
               v_preco_venda       := 0.00;
               v_sdsobservacao     := ' ';
            end;

            insert into inte_989
               (nivel_produto,     grupo_produto,
                subgru_produto,    item_produto,
                fator_1,           fator_2,
                preco_transitorio, preco_sugerido,
                preco_venda,       nvlprecomontagem,
                sdsobservacao)
            values
               (:new.nivel_estrutura,    :new.grupo_estrutura,
                :new.subgru_estrutura,   :new.item_estrutura,
                v_fator_1,               v_fator_2,
                v_preco_transitorio,     v_preco_sugerido,
                v_preco_venda,           v_nvlprecomontagem,
                v_sdsobservacao
               );
         end;
      end if;

      if v_codigo_projeto is not null
      then
         if updating
         then

            begin
               select 1
                into var1
                from inte_989
               where inte_989.nivel_produto  = :new.nivel_estrutura
                 and inte_989.grupo_produto  = :new.grupo_estrutura
                 and inte_989.subgru_produto = :new.subgru_estrutura
                 and inte_989.item_produto   = :new.item_estrutura;
             exception
                when no_data_found then
                   begin
                   select inte_989.preco_transitorio, inte_989.preco_venda,
                          inte_989.sdsobservacao
                     into v_preco_transitorio,        v_preco_venda,
                          v_sdsobservacao
                     from inte_989
                    where inte_989.nivel_produto   = :new.nivel_estrutura
                     and  inte_989.grupo_produto   = :new.grupo_estrutura
                     and (inte_989.subgru_produto <> :new.subgru_estrutura
                       or inte_989.item_produto   <> :new.item_estrutura)
                      and rownum = 1;
                   exception
                      when no_data_found then
                         v_preco_transitorio := 0.00;
                         v_preco_venda       := 0.00;
                         v_sdsobservacao     := ' ';
                   end;
                   insert into inte_989
                      (nivel_produto,     grupo_produto,
                       subgru_produto,    item_produto,
                       fator_1,           fator_2,
                       preco_transitorio, preco_sugerido,
                       preco_venda,       nvlprecomontagem,
                       sdsobservacao)
                   values
                      (:new.nivel_estrutura,    :new.grupo_estrutura,
                       :new.subgru_estrutura,   :new.item_estrutura,
                       v_fator_1,               v_fator_2,
                       v_preco_transitorio,     v_preco_sugerido,
                       v_preco_venda,           v_nvlprecomontagem,
                       v_sdsobservacao
                      );
            end;
         end if;
      end if;

      if deleting
      then
         begin
            delete from inte_989
            where inte_989.nivel_produto  = :old.nivel_estrutura
              and inte_989.grupo_produto  = :old.grupo_estrutura
              and inte_989.subgru_produto = :old.subgru_estrutura
              and inte_989.item_produto   = :old.item_estrutura;
         exception
            when others then
              raise_application_error(-20000, sqlerrm);
         end;
      end if;
   end if;
end inter_tr_basi_010_1;

-- ALTER TRIGGER "INTER_TR_BASI_010_1" ENABLE
 

/

exec inter_pr_recompile;

