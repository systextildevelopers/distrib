
  CREATE OR REPLACE FUNCTION "INTER_FN_CRIA_ESTAGIOS_PAINEL" (
   p_tipo_alocacao           in number,
   p_tipo_recurso            in number,
   p_ordem_planej_producao   in number,
   p_pedido_venda            in number,
   p_tipo_reserva            in number,
   p_estagio_critico         in number,
   p_tipo_ordem_agrupamento  in varchar2)
return number
is
   -- Criando cursor para Pecas (nivel 1) da Ordem de Planejamento - AGRUPADA.
   cursor tmrp_625_pecas_agrup is
   select tmrp_625.ordem_planejamento,
          tmrp_625.pedido_venda,
          tmrp_625.pedido_reserva,
          tmrp_625.nivel_produto_origem,
          tmrp_625.grupo_produto_origem,
          tmrp_625.nivel_produto,
          tmrp_625.grupo_produto,
          tmrp_625.seq_produto
   from tmrp_625, tmrp_620
   where tmrp_625.ordem_planejamento        = tmrp_620.ordem_planejamento
     and tmrp_625.pedido_venda              = tmrp_620.pedido_venda
     and tmrp_625.pedido_reserva            = tmrp_620.pedido_reserva
     and tmrp_625.nivel_produto             = tmrp_620.nivel_produto
     and tmrp_625.grupo_produto             = tmrp_620.grupo_produto
     and tmrp_625.subgrupo_produto          = tmrp_620.subgrupo_produto
     and tmrp_625.item_produto              = tmrp_620.item_produto
     and tmrp_625.nivel_produto_origem      = '0'
     and tmrp_625.grupo_produto_origem      = '00000'
     and tmrp_625.subgrupo_produto_origem   = '000'
     and tmrp_625.item_produto_origem       = '000000'
     and tmrp_625.nivel_produto             = '1'
     and tmrp_620.ordem_planejamento        = p_ordem_planej_producao
     and ((tmrp_620.pedido_venda            = p_pedido_venda and tmrp_625.pedido_venda   > 0) or
          (tmrp_620.pedido_reserva          = p_pedido_venda and tmrp_625.pedido_reserva > 0))
   group by tmrp_625.ordem_planejamento,
            tmrp_625.pedido_venda,
            tmrp_625.pedido_reserva,
            tmrp_625.nivel_produto_origem,
            tmrp_625.grupo_produto_origem,
            tmrp_625.nivel_produto,
            tmrp_625.grupo_produto,
            tmrp_625.seq_produto
   UNION ALL
   select tmrp_625.ordem_planejamento,
          tmrp_625.pedido_venda,
          tmrp_625.pedido_reserva,
          tmrp_625.nivel_produto_origem,
          tmrp_625.grupo_produto_origem,
          tmrp_625.nivel_produto,
          tmrp_625.grupo_produto,
          tmrp_625.seq_produto
   from tmrp_625, tmrp_620
   where tmrp_625.ordem_planejamento        = tmrp_620.ordem_planejamento
     and tmrp_625.pedido_venda              = tmrp_620.pedido_venda
     and tmrp_625.pedido_reserva            = tmrp_620.pedido_reserva
     and tmrp_625.nivel_produto_origem      = tmrp_620.nivel_produto
     and tmrp_625.grupo_produto_origem      = tmrp_620.grupo_produto
     and tmrp_625.subgrupo_produto_origem   = tmrp_620.subgrupo_produto
     and tmrp_625.item_produto_origem       = tmrp_620.item_produto
     and tmrp_625.nivel_produto             = '1'
     and tmrp_620.ordem_planejamento        = p_ordem_planej_producao
     and ((tmrp_620.pedido_venda            = p_pedido_venda and tmrp_625.pedido_venda   > 0) or
          (tmrp_620.pedido_reserva          = p_pedido_venda and tmrp_625.pedido_reserva > 0))
   group by tmrp_625.ordem_planejamento,
            tmrp_625.pedido_venda,
            tmrp_625.pedido_reserva,
            tmrp_625.nivel_produto_origem,
            tmrp_625.grupo_produto_origem,
            tmrp_625.nivel_produto,
            tmrp_625.grupo_produto,
            tmrp_625.seq_produto;

   -- Criando cursor para Pecas (nivel 1) da Ordem de Planejamento.


   -- Criando cursor para Ordens de Confeccao.
   cursor pcpc_040_conf is
   select pcpc_040.proconf_nivel99,
          pcpc_040.proconf_grupo,
          pcpc_040.proconf_subgrupo,
          pcpc_040.proconf_item,
          nvl(tmrp_665.codigo_recurso,'0') recurso,
          pcpc_040.codigo_balanceio,
          sum(pcpc_040.qtde_pecas_prog) qtde_programada_conf
   from tmrp_665, pcpc_040
   where tmrp_665.codigo_recurso (+) = to_char(pcpc_040.codigo_familia)             -- NAO TIRAR O EXTERNO A ESQUERDA, POIS PRECISA PLANIFICAR ORDEM DE CORTE SEM FAMILIA, CELULA.
     and tmrp_665.tipo_alocacao  (+) = p_tipo_alocacao -- parametro tipo alocacao   -- NAO TIRAR O EXTERNO A ESQUERDA, POIS PRECISA PLANIFICAR ORDEM DE CORTE SEM FAMILIA, CELULA.
     and tmrp_665.tipo_recurso   (+) = p_tipo_recurso  -- parametro tipo recurso    -- NAO TIRAR O EXTERNO A ESQUERDA, POIS PRECISA PLANIFICAR ORDEM DE CORTE SEM FAMILIA, CELULA.
     and pcpc_040.ordem_producao     = p_ordem_planej_producao
     and pcpc_040.codigo_estagio     = p_estagio_critico
   group by pcpc_040.proconf_nivel99,
            pcpc_040.proconf_grupo,
            pcpc_040.proconf_subgrupo,
            pcpc_040.proconf_item,
            nvl(tmrp_665.codigo_recurso,'0'),
            pcpc_040.codigo_balanceio;

  -- Criando cursor para Ordem de Beneficiamento de TECIDO ou de FIO.
  cursor pcpb_020_benef is
  select pcpb_020.ordem_producao         ordem_producao,
         pcpb_020.pano_sbg_nivel99       pano_sbg_nivel99,
         pcpb_020.pano_sbg_grupo         pano_sbg_grupo,
         pcpb_020.pano_sbg_subgrupo      pano_sbg_subgrupo,
         pcpb_020.pano_sbg_item          pano_sbg_item,
         pcpb_020.alternativa_item       alternativa_item,
         pcpb_020.roteiro_opcional       roteiro_opcional,
         sum(pcpb_020.qtde_quilos_prog)  qtde_programada_benef
  from pcpb_020, pcpb_010, pcpb_100
  where pcpb_020.ordem_producao    = pcpb_010.ordem_producao
    and pcpb_020.cod_cancelamento  = 0
    and pcpb_010.ordem_producao    = pcpb_100.ordem_producao
    and pcpb_010.cod_cancelamento  = 0
  --  and pcpb_100.tipo_ordem       in ('1','2')
    and pcpb_100.tipo_ordem        = p_tipo_ordem_agrupamento
    and pcpb_100.ordem_agrupamento = p_ordem_planej_producao
    and pcpb_100.cod_cancelamento  = 0
  group by pcpb_020.ordem_producao,
           pcpb_020.pano_sbg_nivel99,
           pcpb_020.pano_sbg_grupo,
           pcpb_020.pano_sbg_subgrupo,
           pcpb_020.pano_sbg_item,
           pcpb_020.alternativa_item,
           pcpb_020.roteiro_opcional;

  -- Criando cursor para Ordem de Tecelagem.
  cursor pcpt_010_tecel is
  select pcpt_010.cd_pano_nivel99,
         pcpt_010.cd_pano_grupo,
         pcpt_010.cd_pano_subgrupo,
         pcpt_010.cd_pano_item,
         pcpt_010.alternativa_item,
         pcpt_010.roteiro_opcional,
         pcpt_010.grupo_maquina || '.' || pcpt_010.subgru_maquina || '.' || ltrim(to_char(pcpt_010.numero_maquina, '00000')) as maquina_tecelagem,
         pcpt_010.qtde_quilos_prog - pcpt_010.qtde_quilos_prod qtde_quilos_prog
   from pcpt_010
   where pcpt_010.ordem_tecelagem  = p_ordem_planej_producao
     and pcpt_010.cod_cancelamento = 0;


  -- Variaveis utilizadas no processo
  v_alternativa_padrao        basi_020.alternativa_padrao%type;
  v_roteiro_padrao            basi_020.roteiro_padrao%type;
  v_nr_registros              number;
  v_retorno_sequenciamento    number;
  v_tipo_ordem_agrupamento    varchar2(3);

  v_qtde_pedido_venda         number;
  v_pedido_pedido             number;
  v_seq_ordem_trabalho        number;
  v_alternativa_produto_620   number;
  v_roteiro_produto_620       number;

begin
   if p_tipo_alocacao = 0 --- Ordens de Planejamento
   then
      v_seq_ordem_trabalho := 0;

      for reg_tmrp_625_pecas_agrup in tmrp_625_pecas_agrup
      loop
         if reg_tmrp_625_pecas_agrup.pedido_venda > 0
         then
            v_pedido_pedido := reg_tmrp_625_pecas_agrup.pedido_venda;
         else
            v_pedido_pedido := reg_tmrp_625_pecas_agrup.pedido_reserva;
         end if;

         v_seq_ordem_trabalho := v_seq_ordem_trabalho + 1;

         for reg_tmrp_625_pecas in (select tmrp_625.nivel_produto_origem,
                                           tmrp_625.grupo_produto_origem,
                                           tmrp_625.subgrupo_produto_origem,
                                           tmrp_625.item_produto_origem,
                                           tmrp_625.seq_produto,
                                           tmrp_625.nivel_produto,
                                           tmrp_625.grupo_produto,
                                           tmrp_625.subgrupo_produto,
                                           tmrp_625.item_produto,
                                           sum(tmrp_625.qtde_reserva_planejada) qtde_reserva_planejada
                                    from tmrp_625
                                    where tmrp_625.nivel_produto_origem      = reg_tmrp_625_pecas_agrup.nivel_produto_origem
                                      and tmrp_625.grupo_produto_origem      = reg_tmrp_625_pecas_agrup.grupo_produto_origem
                                      and tmrp_625.nivel_produto             = reg_tmrp_625_pecas_agrup.nivel_produto
                                      and tmrp_625.grupo_produto             = reg_tmrp_625_pecas_agrup.grupo_produto
                                      and tmrp_625.seq_produto               = reg_tmrp_625_pecas_agrup.seq_produto
                                      and tmrp_625.ordem_planejamento        = p_ordem_planej_producao
                                      and ((tmrp_625.pedido_venda            = p_pedido_venda and tmrp_625.pedido_venda   > 0) or
                                           (tmrp_625.pedido_reserva          = p_pedido_venda and tmrp_625.pedido_reserva > 0))
                                    group by tmrp_625.nivel_produto_origem,
                                             tmrp_625.grupo_produto_origem,
                                             tmrp_625.subgrupo_produto_origem,
                                             tmrp_625.item_produto_origem,
                                             tmrp_625.seq_produto,
                                             tmrp_625.nivel_produto,
                                             tmrp_625.grupo_produto,
                                             tmrp_625.subgrupo_produto,
                                             tmrp_625.item_produto)
         loop
            if reg_tmrp_625_pecas.nivel_produto_origem = '0'
            then
               begin
                  select tmrp_620.alternativa_produto,   tmrp_620.roteiro_produto
                  into   v_alternativa_produto_620,      v_roteiro_produto_620
                  from tmrp_620
                  where tmrp_620.ordem_planejamento        = p_ordem_planej_producao
                    and ((tmrp_620.pedido_venda            = p_pedido_venda and tmrp_620.pedido_venda   > 0) or
                         (tmrp_620.pedido_reserva          = p_pedido_venda and tmrp_620.pedido_reserva > 0))
                    and tmrp_620.nivel_produto             = reg_tmrp_625_pecas.nivel_produto
                    and tmrp_620.grupo_produto             = reg_tmrp_625_pecas.grupo_produto
                    and tmrp_620.subgrupo_produto          = reg_tmrp_625_pecas.subgrupo_produto
                    and tmrp_620.item_produto              = reg_tmrp_625_pecas.item_produto;
               end;
            else
               begin
                  select tmrp_620.alternativa_produto,   tmrp_620.roteiro_produto
                  into   v_alternativa_produto_620,      v_roteiro_produto_620
                  from tmrp_620
                  where tmrp_620.ordem_planejamento        = p_ordem_planej_producao
                    and ((tmrp_620.pedido_venda            = p_pedido_venda and tmrp_620.pedido_venda   > 0) or
                         (tmrp_620.pedido_reserva          = p_pedido_venda and tmrp_620.pedido_reserva > 0))
                    and tmrp_620.nivel_produto             = reg_tmrp_625_pecas.nivel_produto_origem
                    and tmrp_620.grupo_produto             = reg_tmrp_625_pecas.grupo_produto_origem
                    and tmrp_620.subgrupo_produto          = reg_tmrp_625_pecas.subgrupo_produto_origem
                    and tmrp_620.item_produto              = reg_tmrp_625_pecas.item_produto_origem;
               end;
            end if;

            if v_alternativa_produto_620 <> 0 and v_roteiro_produto_620 <> 0
            then
               v_alternativa_padrao := v_alternativa_produto_620;
               v_roteiro_padrao     := v_roteiro_produto_620;
            else
                begin
                   -- Buscando os roteiros padroes do produto
                   select  basi_010.numero_alternati,   basi_010.numero_roteiro
                   into    v_alternativa_padrao,        v_roteiro_padrao
                   from basi_010
                   where basi_010.nivel_estrutura  = reg_tmrp_625_pecas.nivel_produto
                     and basi_010.grupo_estrutura  = reg_tmrp_625_pecas.grupo_produto
                     and basi_010.subgru_estrutura = reg_tmrp_625_pecas.subgrupo_produto
                     and basi_010.item_estrutura   = reg_tmrp_625_pecas.item_produto;
                exception when others then
                   v_alternativa_padrao := 01;
                   v_roteiro_padrao     := 01;
                end;

                if v_alternativa_padrao = 0
                then
                   v_alternativa_padrao := 01;
                end if;

                if v_roteiro_padrao = 0
                then
                   v_roteiro_padrao := 01;
                end if;
            end if;

            --Busca tipo da ordem, busca de forma diferente para reserva e pedido firme
            if p_tipo_reserva = 0 -- e pedido firme
            then
               begin
                   select pedi_100.tipo_prod_pedido
                   into v_tipo_ordem_agrupamento
                   from pedi_100
                   where pedi_100.pedido_venda = p_pedido_venda;
               exception when others then
                   v_tipo_ordem_agrupamento  := p_tipo_ordem_agrupamento;
               end;
            else -- e reserva
               begin
                  select tmrp_640.tipo_reserva
                  into v_tipo_ordem_agrupamento
                  from tmrp_640
                  where tmrp_640.numero_reserva = p_pedido_venda;
               exception when others then
                  v_tipo_ordem_agrupamento  := p_tipo_ordem_agrupamento;
               end;
            end if;

            begin
               select nvl(sum(tmrp_625.qtde_reserva_planejada),0)
               into v_qtde_pedido_venda
               from tmrp_625, tmrp_620
               where tmrp_625.ordem_planejamento        = tmrp_620.ordem_planejamento
                 and tmrp_625.pedido_venda              = tmrp_620.pedido_venda
                 and tmrp_625.pedido_reserva            = tmrp_620.pedido_reserva
                 and tmrp_625.nivel_produto             = tmrp_620.nivel_produto
                 and tmrp_625.grupo_produto             = tmrp_620.grupo_produto
                 and tmrp_625.subgrupo_produto          = tmrp_620.subgrupo_produto
                 and tmrp_625.item_produto              = tmrp_620.item_produto
                 and tmrp_625.nivel_produto_origem      = '0'
                 and tmrp_625.grupo_produto_origem      = '00000'
                 and tmrp_625.subgrupo_produto_origem   = '000'
                 and tmrp_625.item_produto_origem       = '000000'
                 and tmrp_625.nivel_produto             = reg_tmrp_625_pecas.nivel_produto_origem
                 and tmrp_625.grupo_produto             = reg_tmrp_625_pecas.grupo_produto_origem
                 and tmrp_625.alternativa_produto       = v_alternativa_padrao
                 and tmrp_620.ordem_planejamento        = p_ordem_planej_producao
                 and ((tmrp_620.pedido_venda            = p_pedido_venda and tmrp_625.pedido_venda   > 0) or
                      (tmrp_620.pedido_reserva          = p_pedido_venda and tmrp_625.pedido_reserva > 0));
            end;

            inter_pr_cria_estagios_painel(p_tipo_alocacao,
                                          p_tipo_recurso,
                                          0,
                                          p_ordem_planej_producao,
                                          0,
                                          p_pedido_venda,
                                          p_tipo_reserva,
                                          p_estagio_critico,
                                          reg_tmrp_625_pecas.nivel_produto,
                                          reg_tmrp_625_pecas.grupo_produto,
                                          reg_tmrp_625_pecas.subgrupo_produto,
                                          reg_tmrp_625_pecas.item_produto,
                                          v_alternativa_padrao,
                                          v_roteiro_padrao,
                                          reg_tmrp_625_pecas.qtde_reserva_planejada,
                                          0,
                                          v_tipo_ordem_agrupamento,
                                          reg_tmrp_625_pecas.nivel_produto_origem,
                                          reg_tmrp_625_pecas.grupo_produto_origem,
                                          v_qtde_pedido_venda,
                                          v_seq_ordem_trabalho,
                                          reg_tmrp_625_pecas.seq_produto);

            begin
               select nvl(count(*),0)
               into   v_nr_registros
               from tmrp_622;
            end;

            if v_nr_registros > 0
            then
               begin
                  delete from tmrp_622;
               end;
            end if;

            inter_pr_exp_tmrp_625 (reg_tmrp_625_pecas_agrup.ordem_planejamento,
                                   reg_tmrp_625_pecas_agrup.pedido_venda,
                                   reg_tmrp_625_pecas_agrup.pedido_reserva,
                                   reg_tmrp_625_pecas_agrup.nivel_produto,
                                   reg_tmrp_625_pecas_agrup.grupo_produto,
                                   reg_tmrp_625_pecas.seq_produto,
                                   reg_tmrp_625_pecas.nivel_produto,
                                   reg_tmrp_625_pecas.grupo_produto,
                                   reg_tmrp_625_pecas.subgrupo_produto,
                                   reg_tmrp_625_pecas.item_produto,
                                   v_alternativa_padrao,
                                   reg_tmrp_625_pecas.qtde_reserva_planejada);
--            commit;

            for reg_tmrp_625_materiais in (select decode(tmrp_622.nivel_produto,'7',1,'4',2,3) ordem_materiais,
                                                  tmrp_622.nivel_produto,
                                                  tmrp_622.grupo_produto,
                                                  tmrp_622.subgrupo_produto,
                                                  tmrp_622.item_produto,
                                                  tmrp_622.alternativa_produto,
                                                  sum(tmrp_622.qtde_reserva_planejada) qtde_reserva_planejada
                                           from tmrp_622
                                           where tmrp_622.ordem_planejamento        = reg_tmrp_625_pecas_agrup.ordem_planejamento
                                             and tmrp_622.pedido_venda              = reg_tmrp_625_pecas_agrup.pedido_venda
                                             and tmrp_622.pedido_reserva            = reg_tmrp_625_pecas_agrup.pedido_reserva
                                             and tmrp_622.nivel_produto             in ('7','4','2')
                                           group by tmrp_622.nivel_produto,
                                                    tmrp_622.grupo_produto,
                                                    tmrp_622.subgrupo_produto,
                                                    tmrp_622.item_produto,
                                                    tmrp_622.alternativa_produto
                                           order by ordem_materiais,
                                                    tmrp_622.nivel_produto,
                                                    tmrp_622.grupo_produto,
                                                    tmrp_622.subgrupo_produto,
                                                    tmrp_622.item_produto,
                                                    tmrp_622.alternativa_produto)
            loop
               -- Buscando os roteiros padroes do produto
               begin
                  select  basi_010.numero_roteiro
                  into    v_roteiro_padrao
                  from  basi_010
                  where basi_010.nivel_estrutura  = reg_tmrp_625_materiais.nivel_produto
                    and basi_010.grupo_estrutura  = reg_tmrp_625_materiais.grupo_produto
                    and basi_010.subgru_estrutura = reg_tmrp_625_materiais.subgrupo_produto
                    and basi_010.item_estrutura   = reg_tmrp_625_materiais.item_produto;
               exception when others then
                  v_roteiro_padrao := 01;
               end;

               if v_roteiro_padrao = 0
               then
                  v_roteiro_padrao := 01;
               end if;

               --Busca tipo da ordem, busca de forma diferente para reserva e pedido firme
               if p_tipo_reserva = 0 -- e pedido firme
               then
                  begin
                      select pedi_100.tipo_prod_pedido
                      into v_tipo_ordem_agrupamento
                      from pedi_100
                      where pedi_100.pedido_venda = p_pedido_venda;
                  exception when others then
                      v_tipo_ordem_agrupamento  := p_tipo_ordem_agrupamento;
                  end;
               else -- e reserva
                   begin
                       select tmrp_640.tipo_reserva
                       into v_tipo_ordem_agrupamento
                       from tmrp_640
                       where tmrp_640.numero_reserva = p_pedido_venda;
                   exception when others then
                       v_tipo_ordem_agrupamento  := p_tipo_ordem_agrupamento;
                   end;
               end if;

               inter_pr_cria_estagios_painel(p_tipo_alocacao,
                                             p_tipo_recurso,
                                             0,
                                             p_ordem_planej_producao,
                                             0,
                                             p_pedido_venda,
                                             p_tipo_reserva,
                                             p_estagio_critico,
                                             reg_tmrp_625_materiais.nivel_produto,
                                             reg_tmrp_625_materiais.grupo_produto,
                                             reg_tmrp_625_materiais.subgrupo_produto,
                                             reg_tmrp_625_materiais.item_produto,
                                             reg_tmrp_625_materiais.alternativa_produto,
                                             v_roteiro_padrao,
                                             reg_tmrp_625_materiais.qtde_reserva_planejada,
                                             0,
                                             v_tipo_ordem_agrupamento,
                                             '0',
                                             '00000',
                                             0,
                                             v_seq_ordem_trabalho,
                                             0);
            end loop;

--            commit;
         end loop;
      end loop;

      -- Verificando se foi incluso algum registro.
      -- Caso nao seja atualizado nenhum registro, O PAINEL ira rejeitar a planificacao do PRODUTO
      begin
         select count(*)
         into v_nr_registros
         from tmrp_650
         where tmrp_650.ordem_trabalho     = p_ordem_planej_producao
           and tmrp_650.tipo_alocacao      = p_tipo_alocacao
           and tmrp_650.tipo_recurso       = p_tipo_recurso
           and tmrp_650.pedido_venda       = p_pedido_venda;
      end;
   end if; -- if p_tipo_alocacao = 0 Ordens de Planejamento

   if p_tipo_alocacao = 1 --- Ordens de Confeccao
   then
      for reg_pcpc_040_conf in pcpc_040_conf
      loop
         begin

            -- Buscando o roteiro e alternativa da ordem de producao
            select pcpc_020.alternativa_peca , pcpc_020.roteiro_peca
            into   v_alternativa_padrao,       v_roteiro_padrao
            from pcpc_020
            where pcpc_020.ordem_producao = p_ordem_planej_producao;
            exception
               when others then
                  v_alternativa_padrao := 01;
                  v_roteiro_padrao     := 01;
         end;

         --Busca tipo da ordem,
          begin
              select pcpc_020.tipo_ordem
              into v_tipo_ordem_agrupamento
              from pcpc_020
              where pcpc_020.ordem_producao = p_ordem_planej_producao;
              exception
              when others then
                  v_tipo_ordem_agrupamento  := p_tipo_ordem_agrupamento;
          end;

         inter_pr_cria_estagios_painel(p_tipo_alocacao,
                                       p_tipo_recurso,
                                       reg_pcpc_040_conf.recurso,
                                       p_ordem_planej_producao,
                                       0,
                                       p_pedido_venda,
                                       p_tipo_reserva,
                                       p_estagio_critico,
                                       reg_pcpc_040_conf.proconf_nivel99,
                                       reg_pcpc_040_conf.proconf_grupo,
                                       reg_pcpc_040_conf.proconf_subgrupo,
                                       reg_pcpc_040_conf.proconf_item,
                                       v_alternativa_padrao,
                                       v_roteiro_padrao,
                                       reg_pcpc_040_conf.qtde_programada_conf,
                                       reg_pcpc_040_conf.codigo_balanceio,
                                       v_tipo_ordem_agrupamento,
                                       '0',
                                       '00000',
                                       0,
                                       0,
                                       0);
      end loop;


      -- Verificando se foi incluso algum registro.
      -- Caso nao seja atualizado nenhum registro, O PAINEL ira rejeitar a planificacao do PRODUTO
      select count(*)
      into v_nr_registros
      from tmrp_650
      where tmrp_650.ordem_trabalho     = p_ordem_planej_producao
        and tmrp_650.tipo_alocacao      = p_tipo_alocacao
        and tmrp_650.tipo_recurso       = p_tipo_recurso
        and tmrp_650.pedido_venda       = p_pedido_venda;

   end if;

   if p_tipo_alocacao = 2   --- Beneficiamento
   then
      for reg_pcpb_020_benef in pcpb_020_benef
      loop

         inter_pr_cria_estagios_painel(p_tipo_alocacao,
                                       p_tipo_recurso,
                                       0,
                                       p_ordem_planej_producao,
                                       reg_pcpb_020_benef.ordem_producao,
                                       p_pedido_venda,
                                       p_tipo_reserva,
                                       p_estagio_critico,
                                       reg_pcpb_020_benef.pano_sbg_nivel99,
                                       reg_pcpb_020_benef.pano_sbg_grupo,
                                       reg_pcpb_020_benef.pano_sbg_subgrupo,
                                       reg_pcpb_020_benef.pano_sbg_item,
                                       reg_pcpb_020_benef.alternativa_item,
                                       reg_pcpb_020_benef.roteiro_opcional,
                                       reg_pcpb_020_benef.qtde_programada_benef,
                                       0,
                                       p_tipo_ordem_agrupamento,
                                       '0',
                                       '00000',
                                       0,
                                       0,
                                       0);
      end loop;

      -- Verificando se foi incluso algum registro.
      -- Caso nao seja atualizado nenhum registro, O PAINEL ira rejeitar a planificacao do PRODUTO
      select count(*)
      into v_nr_registros
      from tmrp_650
      where tmrp_650.ordem_trabalho     = p_ordem_planej_producao
        and tmrp_650.tipo_alocacao      = p_tipo_alocacao
        and tmrp_650.tipo_recurso       = p_tipo_recurso
        and tmrp_650.pedido_venda       = p_pedido_venda;

   end if;

   if p_tipo_alocacao = 4   --- Tecelagem
   then
      for reg_pcpt_010_tecel in pcpt_010_tecel
      loop

         --Busca tipo da ordem,
         begin
             select pcpt_010.tipo_ordem
             into v_tipo_ordem_agrupamento
             from pcpt_010
             where pcpt_010.ordem_tecelagem = p_ordem_planej_producao;
         exception  when others then
             v_tipo_ordem_agrupamento  := p_tipo_ordem_agrupamento;
         end;

         inter_pr_cria_estagios_painel(p_tipo_alocacao,
                                       p_tipo_recurso,
                                       reg_pcpt_010_tecel.maquina_tecelagem,
                                       p_ordem_planej_producao,
                                       0,
                                       p_pedido_venda,
                                       p_tipo_reserva,
                                       p_estagio_critico,
                                       reg_pcpt_010_tecel.cd_pano_nivel99,
                                       reg_pcpt_010_tecel.cd_pano_grupo,
                                       reg_pcpt_010_tecel.cd_pano_subgrupo,
                                       reg_pcpt_010_tecel.cd_pano_item,
                                       reg_pcpt_010_tecel.alternativa_item,
                                       reg_pcpt_010_tecel.roteiro_opcional,
                                       reg_pcpt_010_tecel.qtde_quilos_prog,
                                       0,
                                       v_tipo_ordem_agrupamento,
                                       '0',
                                       '00000',
                                       0,
                                       0,
                                       0);

      end loop;

      -- Verificando se foi incluso algum registro.
      -- Caso nao seja atualizado nenhum registro, O PAINEL ira rejeitar a planificacao do PRODUTO
      select count(*)
      into v_nr_registros
      from tmrp_650
      where tmrp_650.ordem_trabalho     = p_ordem_planej_producao
        and tmrp_650.tipo_alocacao      = p_tipo_alocacao
        and tmrp_650.tipo_recurso       = p_tipo_recurso
        and tmrp_650.pedido_venda       = p_pedido_venda;

   end if;

   v_retorno_sequenciamento := inter_fn_seq_est_tmrp_650(p_tipo_alocacao, p_tipo_recurso , p_ordem_planej_producao , p_pedido_venda );

   return(v_nr_registros);

end inter_fn_cria_estagios_painel;

 

/

exec inter_pr_recompile;

