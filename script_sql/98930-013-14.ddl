create table CREC_180_SIMULA
(
   PROCESSO        NUMBER(9) default 0 not null,   
   EMPRESA         NUMBER(3) default 0 not null,
   CNPJ9           NUMBER(9) default 0 not null,
   CNPJ4           NUMBER(4) default 0 not null,
   CNPJ2           NUMBER(2) default 0 not null,
   CH_BANCO        NUMBER(3) default 0 not null,
   CH_AGENCIA      NUMBER(5) default 0 not null,
   CH_CONTA        NUMBER(9) default 0 not null,
   CH_NUM          NUMBER(8) default 0 not null,
   CH_SEQUENCIA    NUMBER(5) default 0 not null,
   DUP_TIPO        NUMBER(2) default 0 not null,
   DUP_NUMERO      NUMBER(9) default 0 not null,
   DUP_PARCELA     NUMBER(2) default 0 not null,
   VLR_USADO_CH    NUMBER(13,2) default 0.0,
   VALOR_JUROS     NUMBER(15,2) default 0.00,
   VALOR_DESCONTOS NUMBER(15,2) default 0.00,
   COD_USUARIO     NUMBER(9)    default 0,
   FLAG_SELECIONADO NUMBER(1)    default 0);
   
alter table CREC_180_SIMULA ADD CONSTRAINT PK_CREC_180_SIMULA PRIMARY KEY (PROCESSO, EMPRESA, CNPJ9, CNPJ4, CNPJ2, CH_BANCO, CH_AGENCIA, CH_CONTA, CH_NUM, CH_SEQUENCIA, DUP_TIPO, DUP_NUMERO, DUP_PARCELA);
   
/  
CREATE OR REPLACE TRIGGER inter_tr_seq_crec_180_simula
  BEFORE INSERT ON crec_180_simula
  FOR EACH ROW

  BEGIN
    SELECT NVL(MAX(PROCESSO), 0)+1
    INTO   :new.PROCESSO
    FROM   crec_180_simula
    where crec_180_simula.EMPRESA = :new.EMPRESA
    and crec_180_simula.CNPJ9 = :new.CNPJ9
    and crec_180_simula.CNPJ4 = :new.CNPJ4
    and crec_180_simula.CNPJ2 = :new.CNPJ2
    and crec_180_simula.COD_USUARIO = :new.COD_USUARIO;
  END;
/   
