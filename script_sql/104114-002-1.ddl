CREATE OR REPLACE VIEW ANDAMENTO_REQ_COT
(      data_requisicao,                                codigo_empresa,                             codigo_deposito, 
       nome_requisit,                                  codigo_agrupador,                           num_requisicao, 
       seq_item_req,                                   data_prev_entr,                             item_req_nivel99, 
       item_req_grupo,                                 item_req_subgrupo,                          item_req_item, 
       qtde_requisitada,                               descricao_item,                             unidade_medida, 
       observacao_item,                                comprador_requis,                           numero_cotacao, 
       numero_pedido,                                  seq_item_pedido,                            numero_coleta, 
       comprador_pedido,                               dt_emis_ped_comp,                           dt_prev_entr_item_pedido, 
       conta_estoque,                                  solicitacao,                                centro_custo, 
       ccusto_aplicacao,                               cod_cancelamento,                           codigo, 
       situacao_requis,                                situacao_cot)
AS
SELECT supr_065.data_requisicao,                       supr_065.codigo_empresa,                    supr_065.codigo_deposito,
       supr_065.nome_requisit,                         supr_067.codigo_agrupador,                  supr_067.num_requisicao,
       supr_067.seq_item_req,                          supr_067.data_prev_entr,                    supr_067.item_req_nivel99,
       supr_067.item_req_grupo,                        supr_067.item_req_subgrupo,                 supr_067.item_req_item,
       supr_067.qtde_requisitada,                      supr_067.descricao_item,                    supr_067.unidade_medida,
       supr_067.observacao_item,                       supr_067.codigo_comprador,                  supr_067.numero_cotacao,
       supr_067.numero_pedido,                         nvl(supr_100.seq_item_pedido,0),            supr_067.numero_coleta,
       nvl(supr_090.codigo_comprador,0),               supr_090.dt_emis_ped_comp,                  supr_100.data_prev_entr,
       nvl(basi_030.conta_estoque,0),                  supr_067.solicitacao,                       supr_065.centro_custo,
       supr_067.ccusto_aplicacao,                      supr_067.cod_cancelamento,                  supr_065.codigo,
       decode(supr_067.cod_cancelamento, 0,            -- se supr_067.cod_cancelamento = 0 (nao esta cancelado)
       decode(supr_100.situacao_item, 3, 1,            -- entao se supr_100.situacao_item = 3 entao "1-entrega total"
       decode(supr_100.situacao_item, 2, 2,            -- senao se supr_100.situacao_item = 2 entao "2-entrega parcial"
       decode(supr_067.numero_pedido, 0,               -- senao se supr_067.numero_pedidos = 0
       decode(supr_067.numero_cotacao, 0,              -- entao se supr_067.numero_cotacao = 0
       decode(supr_067.numero_coleta, 0,               -- entao se supr_067.numero_coleta = 0
       decode(supr_067.situacao, 0, 7, 6),             -- entao se supr_067.situacao = 0 entao "7-aprovada" senao "6-bloqueado"
       5),                                             -- senao "5-coletado"
       decode(supr_070.situacao_cotacao,4,11,9,10,4)), -- senao se supr_070.situacao_cotacao  = 4   entao "11-cotacao aprovada"
                                                       -- senao se supr_070.situacao_cotacao  = 9   entao "10-cotacao bloqueada"
                                                       -- senao se supr_070.situacao_cotacao <> 4,9 entao "4-com cotacao"
       3))),                                           -- senao "3-com pedido"
       9),                                             -- senao "9-cancelado"
       supr_070.situacao_cotacao
from supr_065,supr_067,supr_090,basi_030, supr_100, supr_070
where supr_065.num_requisicao   = supr_067.num_requisicao
  and supr_067.item_req_nivel99 = basi_030.nivel_estrutura (+)
  and supr_067.item_req_grupo   = basi_030.referencia      (+)
  and supr_067.numero_pedido    = supr_090.pedido_compra   (+)
  and supr_067.numero_pedido    = supr_100.num_ped_compra  (+)
  and supr_067.num_requisicao   = supr_100.num_requisicao  (+)
  and supr_067.seq_item_req     = supr_100.seq_item_req    (+)
  and supr_067.numero_cotacao   = supr_070.numero_cotacao  (+);



exec inter_pr_recompile;


/
