CREATE OR REPLACE procedure inter_pr_gerar_ob(
        p_id                in  number,
        p_grupo_maq         in  varchar2,
        p_subgrupo_maq      in  varchar2,
        p_num_maq           in  number,
        p_observacao        in  varchar2,
        p_observacao2       in  varchar2,
        p_pano_sbg_nivel    in  varchar2,
        p_pano_sbg_grupo    in  varchar2,
        p_pano_sbg_subgrupo in  varchar2,
        p_pano_sbg_item     in  varchar2,
        p_alternativa_item  in  number,
        p_qtde_quilos_prog  in  number,
        p_deposito_destino  in  number,
        p_ob_gerada         out number,
        p_semana            in varchar2 default ' ',
        p_listagem_refs     in varchar2 default ' '
        )
 is
   p_codigo_acomp            number;
   p_lote_acomp              number;
   p_msg_erro                varchar2(255);
   v_tipo_tingimento_aux     varchar2(01);
   v_unidade_medida          varchar2(02);
   v_pronta_entrega_aux      number;
   v_ordem_benefic           number;
   v_periodo_producao        number;
   v_tipo_prod_tec           number;
   v_tipo_produto_teste      number;
   v_qtde_rolos_prog         number;
   v_digita_rolo_ret         number;
   v_multip                  number;
   v_resto                   number;
   v_alternativa_item        number;
   v_alternativa_item_aux    number;
   v_alternativa_maq         number;
   v_roteiro_opcional        number;
   v_roteiro_opcional_aux    number;
   v_roteiro_maq             number;
   v_teve_dados              number;
   v_mdi_calc_qtde_da_ob     number;
   v_qtde_quilos_prog        number(14,7);
   v_tot_rolo                number(14,7);
   v_kgrolo                  number(14,7);
   v_qtde_rolo_aux           number(14,7);   
   v_multip_aux              number(14,7);
   v_resto_aux               number(14,7);
   v_largura_tecido          number(14,7);
   v_gramatura               number(14,7);
   v_rendimento              number(14,7);
   v_qtde_unidade_prog       number(14,7);
   
begin
   p_msg_erro := '';
   p_ob_gerada := 0;
   v_qtde_quilos_prog := p_qtde_quilos_prog;
   v_qtde_unidade_prog := 0;

   if p_grupo_maq IS NULL
   then
   
      p_msg_erro := p_msg_erro || 'ATENÇÃO! O campo GRUPO_MAQ não pode ser vazio. ';
      
   end if;
   
   if p_subgrupo_maq IS NULL
   then
   
      p_msg_erro := p_msg_erro || 'ATENÇÃO! O campo SUBGRUPO_MAQ não pode ser vazio. ';
      
   end if;
   
   if p_num_maq IS NULL
   then
   
      p_msg_erro := p_msg_erro || 'ATENÇÃO! O campo NUMERO_MAQ não pode ser vazio. ';
      
   end if;
   
   if p_pano_sbg_nivel IS NULL
   then
   
      p_msg_erro := p_msg_erro || 'ATENÇÃO! O campo NIVEL_PRODUTO não pode ser vazio. ';
      
   end if;
   
   if p_pano_sbg_grupo IS NULL
   then
   
      p_msg_erro := p_msg_erro || 'ATENÇÃO! O campo GRUPO_PRODUTO não pode ser vazio. ';
      
   end if;
   
   if p_pano_sbg_subgrupo IS NULL
   then
   
      p_msg_erro := p_msg_erro || 'ATENÇÃO! O campo SUBGRUPO_PRODUTO não pode ser vazio. ';
      
   end if;
   
   if p_pano_sbg_item IS NULL
   then
   
      p_msg_erro := p_msg_erro || 'ATENÇÃO! O campo ITEM_PRODUTO não pode ser vazio. ';
      
   end if;
   
   if p_deposito_destino IS NULL or p_deposito_destino <= 0
   then
   
      p_msg_erro := p_msg_erro || 'ATENÇÃO! O campo DEPOSITO_DESTINO deve ser maior que zero. ';
      
   end if;

   if p_alternativa_item IS NULL or p_alternativa_item = 0
   then
   
      p_msg_erro := p_msg_erro || 'ATENÇÃO! O campo ALTERNATIVA_PRODUTO deve ser maior que zero. ';
      
   end if;

   if trim(p_msg_erro) IS NULL
   then
   
      begin
         select empr_002.digita_rolo_retilinea, empr_002.calc_qtde_da_ob
         INTO   v_digita_rolo_ret,              v_mdi_calc_qtde_da_ob
         from   empr_002;
      end;
   
      begin
         select basi_030.tipo_produto, basi_030.unidade_medida
         INTO   v_tipo_prod_tec,       v_unidade_medida
         from   basi_030
         where basi_030.nivel_estrutura = p_pano_sbg_nivel
         and   basi_030.referencia      = p_pano_sbg_grupo;
      exception when others then
         p_msg_erro := p_msg_erro || 'ATENÇÃO! produto informado não está cadastrado. ';
      end;
   
      begin
         select basi_020.peso_rolo, basi_020.tipo_produto,
                basi_020.largura_1, basi_020.gramatura_1,
                basi_020.rendimento
         INTO   v_kgrolo,           v_tipo_produto_teste,
                v_largura_tecido,   v_gramatura,
                v_rendimento
         from   basi_020
         where basi_020.basi030_nivel030 = p_pano_sbg_nivel
         and   basi_020.basi030_referenc = p_pano_sbg_grupo
         and   basi_020.tamanho_ref      = p_pano_sbg_subgrupo;
      exception when others then
         p_msg_erro := p_msg_erro || 'ATENÇÃO! O subgrupo do produto informado não está cadastrado. ';
      end;
      
      begin
         select basi_010.numero_alternati, basi_010.numero_roteiro
         into   v_alternativa_item_aux,    v_roteiro_opcional_aux
         from   basi_010
         where basi_010.nivel_estrutura  = p_pano_sbg_nivel
         and   basi_010.grupo_estrutura  = p_pano_sbg_grupo
         and   basi_010.subgru_estrutura = p_pano_sbg_subgrupo
         and   basi_010.item_estrutura   = p_pano_sbg_item;
      exception when others then
         p_msg_erro := p_msg_erro || 'ATENÇÃO! A cor do produto informado não está cadastrada. ';
      end;
   
      begin
         select basi_100.tipo_tingimento
         INTO   v_tipo_tingimento_aux
         from   basi_100
         where basi_100.cor_sortimento = p_pano_sbg_item
         and   basi_100.tipo_cor       = v_tipo_produto_teste;
      exception when others then
         v_tipo_tingimento_aux := 1;
      end;
   
      /*
         ################ CALCULAR ROLOS
      */
      if v_qtde_quilos_prog = 0
      then
      
         p_msg_erro := p_msg_erro || 'ATENÇÃO! A quantidade de quilos informada esta zerada. ';
         
      end if;
      
      if trim(p_msg_erro) IS NULL and v_mdi_calc_qtde_da_ob = 0
      then
      
         if v_tipo_prod_tec = 1 and v_tipo_produto_teste = 1
         then
         
            if v_tipo_tingimento_aux = 2 or v_tipo_tingimento_aux = 4
            then
            
               begin
                  select basi_020.peso_rolo_pre_estampado
                  INTO   v_kgrolo
                  from   basi_050, basi_020
                  where basi_020.basi030_nivel030 =  basi_050.nivel_item
                  and   basi_020.basi030_referenc =  basi_050.grupo_item
                  and   basi_020.tamanho_ref      =  basi_050.sub_item
                  and   basi_050.nivel_comp       =  p_pano_sbg_nivel
                  and   basi_050.grupo_comp       =  p_pano_sbg_grupo
                  and   basi_050.sub_comp         in (p_pano_sbg_subgrupo, '000')
                  and   basi_050.item_comp        in (p_pano_sbg_item, '000000')
                  and   basi_050.alternativa_comp =  p_alternativa_item
                  and   basi_050.nivel_item       =  '2';
               exception when others then
                  v_kgrolo := 0;
               end;
               
            end if;
            
            v_qtde_rolo_aux := v_qtde_quilos_prog / v_kgrolo;
            v_qtde_rolos_prog :=  round(v_qtde_rolo_aux,0);
            
         end if;
         
      end if;
      /*
         ################ FINAL CALCULAR ROLOS
      */
   
      /*
         ################ VALIDAR KILOS
      */
       if trim(p_msg_erro) IS NULL and v_mdi_calc_qtde_da_ob = 0
       then
       
          if v_kgrolo > 0
          then
          
             if v_digita_rolo_ret = 1 and (v_tipo_prod_tec = 3 or v_tipo_prod_tec = 4 or v_tipo_prod_tec = 6)
             then
             
                v_qtde_rolos_prog := v_qtde_quilos_prog;
                
             else
             
                v_tot_rolo := v_qtde_quilos_prog / v_kgrolo;
   
                v_multip_aux := v_tot_rolo * v_kgrolo;
                v_multip := round(v_multip_aux,0);
   
                v_resto_aux := v_qtde_quilos_prog - v_multip;
                v_resto := v_resto_aux;
                
                if v_resto > 0
                then
                
                   v_qtde_rolos_prog := v_tot_rolo + 1;
                   v_qtde_quilos_prog := v_kgrolo * (v_tot_rolo + 1);
                   
                else
                
                   v_qtde_rolos_prog := v_tot_rolo;
                   
                end if;
                
             end if;
             
          end if;
          
       end if;
      /*
         ################ FINAL VALIDAR KILOS
      */
   
      /*
         ################ VALIDAR ROLOS
      */
      if trim(p_msg_erro) IS NULL and v_kgrolo > 0      
      then
      
         v_qtde_quilos_prog := v_kgrolo * v_qtde_rolos_prog;  
         
      end if;
      
      if v_digita_rolo_ret = 0 and (v_tipo_prod_tec = 3 or v_tipo_prod_tec = 4 or v_tipo_prod_tec = 6)
      then
      
         v_qtde_unidade_prog := v_qtde_quilos_prog * v_rendimento;
         
      end if;
      /*
         ################ FINAL VALIDAR ROLOS
      */
   
      /*
         ################ VALIDAR QTDE_UNIDADE_PROG
      */
      
      if trim(p_msg_erro) IS NULL and v_qtde_unidade_prog > 0 and v_unidade_medida <> 'KG' and v_qtde_quilos_prog = 0
      then
      
         v_qtde_quilos_prog := v_qtde_unidade_prog * v_largura_tecido * v_gramatura;
         
      end if;
      
       
      if v_qtde_quilos_prog > 0 and v_qtde_rolos_prog = 0
      then
      
         v_qtde_rolo_aux := v_qtde_quilos_prog / v_kgrolo;
         v_qtde_rolos_prog := round(v_qtde_rolo_aux,0);
         
      end if;
      /*
         ################ FINAL VALIDAR QTDE_UNIDADE_PROG
      */
      
      /*
         ################ VALIDAR PANO_SBG_ITEM
      */
      if trim(p_msg_erro) IS NULL
      then
      
         v_teve_dados := 0;
         
         begin
            select mqop_020.roteiro_opcional,  mqop_020.alternativa_padrao,
                   nvl(1,0)
            INTO   v_roteiro_maq,              v_alternativa_maq,
                   v_teve_dados
            from mqop_020
            where mqop_020.grupo_maquina    = p_grupo_maq
            and   mqop_020.subgrupo_maquina = p_subgrupo_maq
            and   mqop_020.roteiro_opcional > 0
            and   exists(
               select 1 from mqop_050
               where mqop_050.nivel_estrutura    = p_pano_sbg_nivel 
               and   mqop_050.grupo_estrutura    = p_pano_sbg_grupo 
               and  (mqop_050.subgru_estrutura   = p_pano_sbg_subgrupo 
                  or mqop_050.subgru_estrutura   = '000') 
               and  (mqop_050.item_estrutura     = p_pano_sbg_item 
                  or mqop_050.item_estrutura     = '000000') 
               and  mqop_050.numero_alternati    = mqop_020.alternativa_padrao 
               and  mqop_050.numero_roteiro      = mqop_020.roteiro_opcional);
            exception when no_data_found then
               v_teve_dados := 0;
         end;
         
         if v_teve_dados = 0
         then
         
            v_roteiro_opcional := v_roteiro_opcional_aux;
            v_alternativa_item := v_alternativa_item_aux;
            
         else
         
            v_roteiro_opcional := v_roteiro_maq;
            v_alternativa_item := v_alternativa_maq;
            
         end if;
         
      end if;
      /*
         ################ FINAL VALIDAR PANO_SBG_ITEM
      */
   
      begin
         select nvl(max(pcpc_010.periodo_producao),0)
         INTO   v_periodo_producao
         from   pcpc_010
         where sysdate               >= pcpc_010.data_ini_periodo
         and   sysdate               <= pcpc_010.data_fim_periodo
         and   pcpc_010.area_periodo =  2;
      end;
   
      if v_periodo_producao = 0
      then
      
         p_msg_erro := p_msg_erro || 'ATENÇÃO! periodo de produção não cadastrado. '; 
         
      end if;
   
      begin
         select basi_205.pronta_entrega
         INTO   v_pronta_entrega_aux
         from   basi_205
         where basi_205.codigo_deposito = p_deposito_destino;
      exception when no_data_found then
         p_msg_erro := p_msg_erro || 'ATENÇÃO! o depósito informado não existe. ';
      end;
   
      if v_pronta_entrega_aux <> 1 and p_deposito_destino = 0
      then
       
         p_msg_erro := p_msg_erro || 'ATENÇÃO! O depósito (' || p_deposito_destino || ') cadastrado não é de pronta entrega. '; 
          
      end if;
      dbms_output.put_line('p_msg_erro: ' || to_char(p_msg_erro));
      if trim(p_msg_erro) IS NULL
      then
      
         begin
            update empr_001
            set ordem_benefic = ordem_benefic + 1;
         end;
         
         begin
             select empr_001.ordem_benefic
             INTO   v_ordem_benefic
             from   empr_001;
         end;
         
         p_ob_gerada := v_ordem_benefic;
         
         begin
            insert INTO pcpb_010 (
               ordem_producao,          grupo_maquina,
               subgrupo_maquina,        numero_maquina,
               data_programa,           previsao_termino,
               data_prevista_original,  observacao,
               observacao2,             periodo_producao,
               nivel_estrutura,         qtde_rolos_prog,
               qtde_quilos_prog
            ) values (
               v_ordem_benefic,               p_grupo_maq,
               p_subgrupo_maq,                p_num_maq,
               trunc(sysdate, 'DD'),          trunc(sysdate, 'DD'),
               trunc(sysdate, 'DD'),          SUBSTR(p_observacao, 1, 22),
               SUBSTR(p_observacao2, 1, 22),  v_periodo_producao,
               p_pano_sbg_nivel,              v_qtde_rolos_prog,
               v_qtde_quilos_prog
            );
         exception when others then
            p_ob_gerada := 0;
            p_msg_erro := p_msg_erro || 'ATENÇÃO! Não inseriu dados na tabela da Capa da OB - pcpb_010. ';
         end;
   
         if trim(p_msg_erro) IS NULL
         then
         
            begin
               insert into pcpb_020 (
               ordem_producao,             pano_sbg_nivel99,
               pano_sbg_grupo,             pano_sbg_subgrupo,
               pano_sbg_item,              codigo_acomp,
               lote_acomp,                 alternativa_item,
               roteiro_opcional,           qtde_unidade_prog,
               qtde_rolos_prog,            qtde_quilos_prog,
               largura_tecido,             gramatura,
               rendimento,                 sequencia
               ) values (
               v_ordem_benefic,            p_pano_sbg_nivel,
               p_pano_sbg_grupo,           p_pano_sbg_subgrupo,
               p_pano_sbg_item,            0,
               0,                          v_alternativa_item,
               v_roteiro_opcional,         v_qtde_unidade_prog,
               v_qtde_rolos_prog,          v_qtde_quilos_prog,
               v_largura_tecido,           v_gramatura,
               v_rendimento,               1
               );
            exception when others then
               p_ob_gerada := 0;
               p_msg_erro := p_msg_erro || 'ATENÇÃO! Não inseriu dados na tabela de produtos - pcpb_020. ';
            end;
            
         end if;
         
         if trim(p_msg_erro) IS NULL
         then
         
            begin
               insert into pcpb_030 (
               periodo_producao, ordem_producao,
               pano_nivel99,     pano_grupo,
               pano_subgrupo,    pano_item,
               codigo_acomp,     lote_acomp,
               pedido_corte,     nr_pedido_ordem,
               sequenci_periodo, codigo_deposito,
               seq_ordem_prod,   qtde_quilos_prog,
               qtde_rolos_prog,  alternativa,
               roteiro,          sequencia,
               semana,           listagem_referencias
               ) values (
               v_periodo_producao,    v_ordem_benefic,
               p_pano_sbg_nivel,      p_pano_sbg_grupo,
               p_pano_sbg_subgrupo,   p_pano_sbg_item,
               0,                     0,
               2,                     0,
               0,                     p_deposito_destino,
               0,                     v_qtde_quilos_prog, 
               v_qtde_rolos_prog,     v_alternativa_item,
               v_roteiro_opcional,    1,
               p_semana,              p_listagem_refs
               );
            exception when others then
               p_ob_gerada := 0;
               p_msg_erro := p_msg_erro || 'ATENÇÃO! Não inseriu dados na tabela de Destino dos Tecidos - pcpb_030. ';
               
            end;
   
         end if;
         
      end if;
      
   end if;

   if trim(p_msg_erro) IS NOT NULL
   then
   
      begin
         insert into pcpb_812 (
            pcpb_812.id,             pcpb_812.mensagem, 
            pcpb_812.data_insercao,  pcpb_812.tipo_processo
         ) values (
            p_id,      p_msg_erro, 
            sysdate,   'OB'
         );
      end;
      
   end if;
   
end inter_pr_gerar_ob;
/
