  CREATE OR REPLACE PROCEDURE "INTER_PR_CRIA_ESTAGIOS_PAINEL" (
    -- Recebe parametros para criacao do estagios e operacoes
    p_tipo_alocacao           in number,        p_tipo_recurso            in number,
    p_codigo_recurso          in varchar2,
    p_ordem_planej_producao   in number,        p_ordem_beneficiamento    in number,
    p_pedido_venda_ordem_plan in number,        p_tipo_reserva            in number,
    p_estagio_critico         in number,        p_nivel_ordem_plan        in varchar2,
    p_grupo_ordem_plan        in varchar2,      p_subgru_ordem_plan       in varchar2,
    p_item_ordem_plan         in varchar2,      p_alternativa_produto     in number,
    p_roteiro_produto         in number,        p_qtde_produto_plan       in number,
    p_codigo_balanceio        in number,        p_tipo_ordem_agrupamento  in varchar2,
    p_nivel_conj              in varchar2,      p_grupo_conj              in varchar2,
    p_qtde_pedido_venda       in number,        p_seq_ordem_trabalho      in varchar2,
    p_seq_estrutura_conjunto  in number)
is
   v_nivel_calculo_estagio            mqop_050.nivel_estrutura%type;
   v_grupo_calculo_estagio            mqop_050.grupo_estrutura%type;
   v_sub_calculo_estagio              mqop_050.subgru_estrutura%type;
   v_item_calculo_estagio             mqop_050.item_estrutura%type;
   v_salva_estagio                    mqop_050.codigo_estagio%type;
   v_ultimo_estagio                   mqop_050.codigo_estagio%type;
   v_gera_estagio_homem               empr_002.gera_estagio_operacao_homem%type;
   v_estagio_ante                     mqop_050.codigo_estagio%type;
   v_seq_operacao_ante                mqop_050.seq_operacao%type;
   v_minutos_operacao                 mqop_050.minutos%type;
   v_estagio_costura                  mqop_005.codigo_estagio%type;
   v_lote_medio_tingimento            basi_170.lote_medio_tingimento%type;

   v_seq_estagio_ordem_post           tmrp_650.seq_estagio_ordem%type;
   v_codigo_recurso                   tmrp_650.codigo_recurso%type;
   v_ordem_trabalho_ant               tmrp_650.ordem_trabalho%type;

   v_ordem_agrupamento_aux            tmrp_650.ordem_trabalho%type;

   v_tipo_oper_cmc                    number;
   v_existe_650                       number;
   v_primeiro                         number;
   v_sequencia_upd                    number;
   v_nr_registro                      number;
   v_nr_registro_mqop_030             number;
--   v_seq_ordem_trabalho               number;

   v_tipo_ordem_apresentacao          number;
   v_existe_retilineo                 number;
   v_subgru_ordem_plan                varchar2(3);

   v_grupo_produto_gantt_benef        mqop_050.grupo_estrutura%type;
   v_subgrupo_produto_gantt_benef     mqop_050.subgru_estrutura%type;

   v_niv_650_compara_gantt_benef      mqop_050.nivel_estrutura%type;
   v_gru_650_compara_gantt_benef      mqop_050.grupo_estrutura%type;
   v_sub_650_compara_gantt_benef      mqop_050.subgru_estrutura%type;
   v_ite_650_compara_gantt_benef      mqop_050.item_estrutura%type;

   v_qtde_res_plan_produto_650        tmrp_625.qtde_reserva_planejada%type;
   v_qtde_res_plan_produto_novo       tmrp_625.qtde_reserva_planejada%type;
   v_qtde_prog_ordem_produto_650      pcpb_020.qtde_quilos_prog%type;
   v_qtde_prog_ordem_produto_novo     pcpb_020.qtde_quilos_prog%type;

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_mensagem_auxiliar      tmrp_650.mensagem_painel%type;
   v_mensagem_painel        tmrp_650.mensagem_painel%type;


begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- Calculando os estagios das pecas
   v_nivel_calculo_estagio  := p_nivel_ordem_plan;
   v_grupo_calculo_estagio  := p_grupo_ordem_plan;
   v_sub_calculo_estagio    := p_subgru_ordem_plan;
   v_item_calculo_estagio   := p_item_ordem_plan;
   v_seq_estagio_ordem_post := 0;

   select count(*)
   into v_nr_registro
   from mqop_050
   where mqop_050.nivel_estrutura  = v_nivel_calculo_estagio
     and mqop_050.grupo_estrutura  = v_grupo_calculo_estagio
     and mqop_050.subgru_estrutura = v_sub_calculo_estagio
     and mqop_050.item_estrutura   = v_item_calculo_estagio
     and mqop_050.numero_alternati = p_alternativa_produto
     and mqop_050.numero_roteiro   = p_roteiro_produto;

   if v_nr_registro = 0
   then
      -- Verifica se existe informacao no roteiro por subgrupo quando nao encontrar por produto completo
      v_item_calculo_estagio := '000000';

      begin
         select count(*)
         into v_nr_registro
         from mqop_050
         where mqop_050.nivel_estrutura  = v_nivel_calculo_estagio
           and mqop_050.grupo_estrutura  = v_grupo_calculo_estagio
           and mqop_050.subgru_estrutura = v_sub_calculo_estagio
           and mqop_050.item_estrutura   = v_item_calculo_estagio
           and mqop_050.numero_alternati = p_alternativa_produto
           and mqop_050.numero_roteiro   = p_roteiro_produto;

         if v_nr_registro = 0
         then
            -- Verifica se existe informacao no roterio por grupo/item quando nao encontrar por subgrupo ou produto completo
            v_sub_calculo_estagio  := '000';

            v_item_calculo_estagio := p_item_ordem_plan;

            begin
               select count(*)
               into v_nr_registro
               from mqop_050
               where mqop_050.nivel_estrutura  = v_nivel_calculo_estagio
                 and mqop_050.grupo_estrutura  = v_grupo_calculo_estagio
                 and mqop_050.subgru_estrutura = v_sub_calculo_estagio
                 and mqop_050.item_estrutura   = v_item_calculo_estagio
                 and mqop_050.numero_alternati = p_alternativa_produto
                 and mqop_050.numero_roteiro   = p_roteiro_produto;
               if v_nr_registro = 0
               then
                  v_item_calculo_estagio := '000000';
               end if;
            end; --final do begin do grupo/item
         end if;
      end;       -- final do begin do subgrupo
   end if;

   v_salva_estagio         := 0;

   begin
      select empr_001.estagio_confec
      into v_estagio_costura
      from empr_001;
   exception when others then
      v_estagio_costura    := 0;
   end;

   -- Criando um cursor com o roteiro de operacao e as formas de leitura encontradas acima (por produto completo,por grupo, por subgrupo)
   for reg_mqop_050 in (select mqop_050.codigo_estagio,     mqop_050.codigo_operacao,
                               mqop_050.centro_custo,       mqop_050.minutos,
                               mqop_050.sequencia_estagio,  mqop_050.estagio_depende,
                               mqop_040.pede_produto,       mqop_050.codigo_familia,
                               mqop_050.seq_operacao,       mqop_040.classificacao,
                               mqop_005.leed_time,          mqop_005.seq_planejamento,
                               mqop_040.grupo_maquinas,     mqop_040.sub_maquina,
                               mqop_040.tipo_operacao
                        from mqop_050, mqop_040, mqop_005
                        where mqop_050.nivel_estrutura    = v_nivel_calculo_estagio
                          and mqop_050.grupo_estrutura    = v_grupo_calculo_estagio
                          and mqop_050.subgru_estrutura   = v_sub_calculo_estagio
                          and mqop_050.item_estrutura     = v_item_calculo_estagio
                          and mqop_050.numero_alternati   = p_alternativa_produto
                          and mqop_050.numero_roteiro     = p_roteiro_produto
--                          and mqop_050.codigo_estagio in(5,12,10)
                          and mqop_050.codigo_operacao    = mqop_040.codigo_operacao
                          and mqop_050.codigo_estagio     = mqop_005.Codigo_Estagio
                        order by mqop_050.seq_operacao      asc,
                                 mqop_050.sequencia_estagio asc,
                                 mqop_050.estagio_depende   asc,
                                 mqop_050.codigo_estagio    asc)
   loop

      -- Quando o estagio nao for de costura, o mesmo devera ter a operacao do roteiro ligada a uma maquina cujo o
      -- estagio principal da maquina seja o estagio que encotramos na operacao. Isto para buscar o tempo gargalo
      -- para o calculo do tempo para o painel
      v_nr_registro_mqop_030 :=1;

      /*
      v_nr_registro_mqop_030 :=0;

      begin
         select count(*) into v_nr_registro_mqop_030
         from mqop_030
         where mqop_030.maq_sub_grupo_mq  = reg_mqop_050.grupo_maquinas
           and mqop_030.maq_sub_sbgr_maq  = reg_mqop_050.sub_maquina
           and mqop_030.estagio_principal = reg_mqop_050.codigo_estagio;
      end;
      */

      if v_estagio_costura  > 0
      and v_estagio_costura = reg_mqop_050.codigo_estagio
      then
         v_nr_registro_mqop_030 := 1;
      end if;

      -- Inicio da logica para gerar o GANTT dos estagios da area 1 - Confeccao
      if  v_nivel_calculo_estagio = '1'
      and v_nr_registro_mqop_030  > 0
      then
         v_tipo_oper_cmc   := 0;
         v_codigo_recurso  := ' ';

         if  p_codigo_recurso            is not null
         and p_codigo_recurso            <> '0'
         and reg_mqop_050.codigo_estagio = v_estagio_costura
         then
            v_codigo_recurso := p_codigo_recurso;
         end if;

         select count(*) into v_tipo_oper_cmc
         from mqop_045
         where mqop_045.codigo_operacao = reg_mqop_050.codigo_operacao;

         if  v_salva_estagio             <> reg_mqop_050.codigo_estagio
         and reg_mqop_050.pede_produto   <> 1
         and v_tipo_oper_cmc              = 0
         then
            -- Se for estagio critico da Confeccao, Verifica o tempo da reserva
            -- Se nao existir busca o tempo do roteiro.
            if  p_estagio_critico = reg_mqop_050.codigo_estagio
            and p_tipo_reserva    > 0
            then
               begin
                  select (tmrp_645.minutos_reserva / tmrp_645.qtde_reserva)
                  into v_minutos_operacao
                  from tmrp_645,tmrp_640
                  where tmrp_640.numero_reserva = tmrp_645.numero_reserva
                    and tmrp_640.tipo_reserva   = p_tipo_reserva
                    and tmrp_645.nivel_produto  = p_nivel_ordem_plan
                    and tmrp_645.grupo_produto  = p_grupo_ordem_plan
                    and tmrp_645.subgru_produto = p_subgru_ordem_plan
                    and tmrp_645.item_produto   = p_item_ordem_plan;
                  exception
                    when others then
                       v_minutos_operacao := reg_mqop_050.minutos;
               end;
            else
               v_minutos_operacao := reg_mqop_050.minutos;
            end if;

            /************************************************************************
             * Definido pelo Srta Tabita na SS 48182/001 que considera sempre       *
             * a soma do tempo sera todas as operacoes. Alteracao validada pela     *
             * Tabita                                                               *
             ************************************************************************/

            select nvl(sum(mqop_050.minutos),0.00)
            into   v_minutos_operacao
            from mqop_050
            where mqop_050.nivel_estrutura  = v_nivel_calculo_estagio
              and mqop_050.grupo_estrutura  = v_grupo_calculo_estagio
              and mqop_050.subgru_estrutura = v_sub_calculo_estagio
              and mqop_050.item_estrutura   = v_item_calculo_estagio
              and mqop_050.numero_alternati = p_alternativa_produto
              and mqop_050.numero_roteiro   = p_roteiro_produto
              and mqop_050.codigo_estagio   = reg_mqop_050.codigo_estagio;
            if v_minutos_operacao <> 0
            then
               if reg_mqop_050.minutos = 0
               then
                  if v_sub_calculo_estagio <> p_subgru_ordem_plan
                  or v_item_calculo_estagio <> p_item_ordem_plan
                  then
                     v_mensagem_auxiliar := '(' || p_nivel_ordem_plan || '.' ||
                                            p_grupo_ordem_plan || '.' ||
                                            p_subgru_ordem_plan || '.' ||
                                            p_item_ordem_plan || ')';
                  else
                     v_mensagem_auxiliar := '';
                  end if;

                  v_mensagem_painel :=  v_nivel_calculo_estagio || '.' ||
                                        v_grupo_calculo_estagio || '.' ||
                                        v_sub_calculo_estagio   || '.' ||
                                        v_item_calculo_estagio  || ' ' || v_mensagem_auxiliar || ' ' ||
                                        'A.: ' || to_char(p_alternativa_produto ,'00') || ' ' ||
                                        'R.: ' || to_char(p_roteiro_produto ,'00') || ' / ';

                  v_mensagem_painel := v_mensagem_painel || 'Estagio/Etapa' || ': '  || to_char(reg_mqop_050.codigo_estagio,'99900')     || ' ' ||
                                          'Operacao/Operacion' || ': ' || to_char(reg_mqop_050.codigo_operacao,'00000')|| chr(010);
               end if;

               /*********************************************************************************
               * No dia 07/10/2008 executamos a SS 45145/001 que definiu uma nova forma de      *
               * gerar a TMRP_650.                                                              *
               * Originalmente quando foi criada esta procedure havia no SELECT COUNT abaixo    *
               * e no UPDATE o link TMRP_650.SEQ_OPERACAO = REG_MQOP_050.SEQ_OPERACAO           *
               * este link foi retirado apos conversa entre Sr. Julio, Comin e Fabio k.         *
               * a razao foi:                                                                   *
               *    Quando temos um pedido de venda que tem produtos e estes contem alternativas*
               *    e roteiros de diferentes entao a sequencia de operacao pode ser diferente   *
               *    ocasionando erro na geracao das quantidades do planejamento.                *
               *********************************************************************************/

               v_existe_650 := 0;

               if p_nivel_conj = '0'
               then
                  select count(*)
                  into v_existe_650
                  from tmrp_650
                  where tmrp_650.ordem_trabalho     = p_ordem_planej_producao
                    and tmrp_650.tipo_alocacao      = p_tipo_alocacao
                    and tmrp_650.tipo_recurso       = p_tipo_recurso
                    and tmrp_650.codigo_recurso     = v_codigo_recurso
                    and tmrp_650.pedido_venda       = p_pedido_venda_ordem_plan
                    and tmrp_650.nivel_produto      = v_nivel_calculo_estagio
                    and tmrp_650.grupo_produto      = v_grupo_calculo_estagio
                    and tmrp_650.codigo_estagio     = reg_mqop_050.codigo_estagio
                    and tmrp_650.cod_balanceamento  = p_codigo_balanceio
                    and tmrp_650.seq_ordem_trabalho = p_seq_ordem_trabalho;
               else
                  select count(*)
                  into v_existe_650
                  from tmrp_650
                  where tmrp_650.ordem_trabalho     = p_ordem_planej_producao
                    and tmrp_650.tipo_alocacao      = p_tipo_alocacao
                    and tmrp_650.tipo_recurso       = p_tipo_recurso
                    and tmrp_650.codigo_recurso     = v_codigo_recurso
                    and tmrp_650.pedido_venda       = p_pedido_venda_ordem_plan
                    and tmrp_650.nivel_produto      = v_nivel_calculo_estagio
                    and tmrp_650.grupo_produto      = v_grupo_calculo_estagio
                    and tmrp_650.codigo_estagio     = reg_mqop_050.codigo_estagio
                    and tmrp_650.cod_balanceamento  = p_codigo_balanceio
                    and tmrp_650.seq_ordem_trabalho = p_seq_ordem_trabalho
                    and tmrp_650.nivel_conjunto     = p_nivel_conj
                    and tmrp_650.grupo_conjunto     = p_grupo_conj;
               end if;

               if v_existe_650 = 0
               then
                  begin
                     insert into tmrp_650 (
                        tipo_alocacao,                  ordem_trabalho,
                        pedido_venda,                   nivel_produto,
                        grupo_produto,
                        subgru_produto,
                        item_produto,                   quantidade,
                        codigo_estagio,                 minutos_unitario,
                        sequencia_estagio,              seq_operacao,
                        lead_time,                      qtde_recursos,
                        seq_estagio_ordem,              tipo_recurso,
                        codigo_recurso,                 tipo_reserva,
                        tempo_producao,
                        cod_balanceamento,              seq_ordem_trabalho,
                        tipo_ordem_agrupamento,         mensagem_painel,
                        nivel_conjunto,                 grupo_conjunto,
                        qtde_pecas_conjunto,            seq_estrutura_conjunto
                     ) values (
                        p_tipo_alocacao,                p_ordem_planej_producao,
                        p_pedido_venda_ordem_plan,      v_nivel_calculo_estagio,
--                        decode(p_nivel_conj,'0',v_grupo_calculo_estagio,p_grupo_conj),
                        v_grupo_calculo_estagio,
                        '000',
                        '000000',                       p_qtde_produto_plan,
                        reg_mqop_050.codigo_estagio,    v_minutos_operacao,
                        reg_mqop_050.sequencia_estagio, reg_mqop_050.seq_operacao,
                        reg_mqop_050.leed_time,         1,
                        0,                              p_tipo_recurso,
                        v_codigo_recurso,               p_tipo_reserva,
                        decode(reg_mqop_050.tipo_operacao,1,v_minutos_operacao * p_qtde_produto_plan,v_minutos_operacao),
                        p_codigo_balanceio,             p_seq_ordem_trabalho,
                        p_tipo_ordem_agrupamento,       v_mensagem_painel,
                        p_nivel_conj,
                        p_grupo_conj,
                        p_qtde_pedido_venda,            p_seq_estrutura_conjunto
                     );
                  end;

                  v_salva_estagio   := reg_mqop_050.codigo_estagio;

                  if reg_mqop_050.codigo_estagio = p_estagio_critico
                  then
                     v_seq_estagio_ordem_post := reg_mqop_050.seq_planejamento;
                  end if;
               else
                  begin
                    update tmrp_650
                    set tmrp_650.quantidade       = tmrp_650.quantidade + p_qtde_produto_plan,
                        tmrp_650.minutos_unitario = decode(tmrp_650.minutos_unitario, 0, tmrp_650.minutos_unitario + v_minutos_operacao, tmrp_650.minutos_unitario),
                        tmrp_650.tempo_producao   = tmrp_650.tempo_producao + decode(reg_mqop_050.tipo_operacao,1,v_minutos_operacao * p_qtde_produto_plan,v_minutos_operacao),
                        tmrp_650.mensagem_painel  = v_mensagem_painel
                    where tmrp_650.ordem_trabalho     = p_ordem_planej_producao
                      and tmrp_650.tipo_alocacao      = p_tipo_alocacao
                      and tmrp_650.tipo_recurso       = p_tipo_recurso
                      and tmrp_650.codigo_recurso     = v_codigo_recurso
                      and tmrp_650.pedido_venda       = p_pedido_venda_ordem_plan
                      and tmrp_650.nivel_produto      = v_nivel_calculo_estagio
                      and tmrp_650.grupo_produto      = v_grupo_calculo_estagio
                      and tmrp_650.codigo_estagio     = reg_mqop_050.codigo_estagio
                      and tmrp_650.cod_balanceamento  = p_codigo_balanceio
                      and tmrp_650.seq_ordem_trabalho = p_seq_ordem_trabalho;
                  end;

                  v_salva_estagio   := reg_mqop_050.codigo_estagio;

                  if reg_mqop_050.codigo_estagio = p_estagio_critico
                  then
                     v_seq_estagio_ordem_post := reg_mqop_050.seq_planejamento;
                  end if;
               end if;
            end if;
         end if;
      end if; -- if v_nivel_calculo_estagio = '1' -- Confeccao
      -- Fim da logica para gerar o GANTT dos estagios da area 1 - Confeccao


      -- Inicio da logica para gerar o GANTT dos estagios da area 2 - Beneficiamento de Tecidos (2) e Fios (7)
      if (v_nivel_calculo_estagio = 2
      or  v_nivel_calculo_estagio = 7
      or  v_nivel_calculo_estagio = 9)
      and v_nr_registro_mqop_030  > 0
      then
         -- Busca parametro de empresa executando a seguinte consistencia:
         -- S - Insere estagio independente da classificacao
         -- N - Nao gera estagio classificacao 3 - Homem
         select empr_002.gera_estagio_operacao_homem
         into v_gera_estagio_homem
         from empr_002;

         if (reg_mqop_050.classificacao <> 3 and v_gera_estagio_homem = 'N')
         or  v_gera_estagio_homem        = 'S'
         then
            if reg_mqop_050.codigo_estagio <> v_salva_estagio
            then
               if reg_mqop_050.minutos = 0
               then
                  if v_sub_calculo_estagio <> p_subgru_ordem_plan
                  or v_item_calculo_estagio <> p_item_ordem_plan
                  then
                     v_mensagem_auxiliar := '(' || p_nivel_ordem_plan || '.' ||
                                            p_grupo_ordem_plan || '.' ||
                                            p_subgru_ordem_plan || '.' ||
                                            p_item_ordem_plan || ')';
                  else
                     v_mensagem_auxiliar := '';
                  end if;

                  v_mensagem_painel :=  v_nivel_calculo_estagio || '.' ||
                                        v_grupo_calculo_estagio || '.' ||
                                        v_sub_calculo_estagio   || '.' ||
                                        v_item_calculo_estagio  || ' ' || v_mensagem_auxiliar || ' ' ||
                                        'A.: ' || to_char(p_alternativa_produto ,'00') || ' ' ||
                                        'R.: ' || to_char(p_roteiro_produto ,'00') || ' / ';

                  v_mensagem_painel := v_mensagem_painel || 'Estagio/Etapa' || ': '  || to_char(reg_mqop_050.codigo_estagio,'99900')     || ' ' ||
                                          'Operacao/Operacion' || ': ' || to_char(reg_mqop_050.codigo_operacao,'00000')|| chr(010);
               end if;

               v_ordem_agrupamento_aux := p_ordem_planej_producao;

               /************************************************************************
                * Definido pelo Srta Tabita na SS 48182/001 que considera sempre       *
                * a soma do tempo sera todas as operacoes. Alteracao validada pela     *
                * Tabita                                                               *
                ************************************************************************/
               select nvl(sum(mqop_050.minutos),0.00)
               into v_minutos_operacao
               from mqop_050
               where mqop_050.nivel_estrutura  = v_nivel_calculo_estagio
                 and mqop_050.grupo_estrutura  = v_grupo_calculo_estagio
                 and mqop_050.subgru_estrutura = v_sub_calculo_estagio
                 and mqop_050.item_estrutura   = v_item_calculo_estagio
                 and mqop_050.numero_alternati = p_alternativa_produto
                 and mqop_050.numero_roteiro   = p_roteiro_produto
                 and mqop_050.codigo_estagio   = reg_mqop_050.codigo_estagio;

               --- Regra para gerar estagios do beneficiamento
               --- Para o estagio da tinturaria sera gerado um registro para a ordem de agrupamento (pcpb_100)
               --- Quando forem estagios diferentes do tingimento, entao gera para cada ordem de beneficiamento um registro
               if  p_estagio_critico      > 0 -- Estagio da Tinturaria esta configurado
               and p_estagio_critico     <> reg_mqop_050.codigo_estagio -- Nao estamos lendo estagio da tinturaria
               and p_tipo_alocacao        = 2 --- Ordens de Beneficiamento
               and p_ordem_beneficiamento > 0
               then
                  v_ordem_agrupamento_aux := p_ordem_beneficiamento;
               end if;

               -- Quando for estagio de tinturaria e a operacao for do tipo 2 (independe da Quantidade)
               -- e feito um calculo pelo peso do lote medio do tingimento para encotrar um valor medio
               -- para os minutos do tingimento.
               begin
                 select basi_170.lote_medio_tingimento into v_lote_medio_tingimento
                 from basi_170
                 where basi_170.area_producao = v_nivel_calculo_estagio;
                 exception
                    when others then
                     v_lote_medio_tingimento  := 0.000;
               end;

               if  reg_mqop_050.tipo_operacao = 2 -- Independe da Quantidade
               and v_lote_medio_tingimento    > 0 -- Peso lote medio for informado
               and p_estagio_critico          > 0 -- Estagio da Tinturaria esta configurado
               and p_estagio_critico          = reg_mqop_050.codigo_estagio -- Estamos lendo estagio da tinturaria
               then
                  v_minutos_operacao := reg_mqop_050.minutos / v_lote_medio_tingimento;
               end if;

               -- Inicializa variaveis para atualizar o produto no gantt do beneficiamento.
               v_grupo_produto_gantt_benef    := p_grupo_ordem_plan;
               v_subgrupo_produto_gantt_benef := p_subgru_ordem_plan;

               -- Verifica se ja existe estagio gerado para esta OB.
               v_existe_650 := 0;

               select count(*)
               into v_existe_650
               from tmrp_650
               where tmrp_650.ordem_trabalho  = v_ordem_agrupamento_aux
                 and tmrp_650.tipo_alocacao   = p_tipo_alocacao
                 and tmrp_650.tipo_recurso    = p_tipo_recurso
                 and tmrp_650.pedido_venda    = p_pedido_venda_ordem_plan
                 and tmrp_650.nivel_produto   = v_nivel_calculo_estagio
                 and (tmrp_650.item_produto   = p_item_ordem_plan or p_estagio_critico = reg_mqop_050.codigo_estagio)
                 and tmrp_650.codigo_estagio  = reg_mqop_050.codigo_estagio;

               -- Se nao for o estagio critico, e se estiver analisando roteiro para tipo de alocacao ordens de planejamento,
               -- sera gravado um registro por estagio e ordem de planejamento.
               if  p_tipo_alocacao    = 0 --- Ordens de Planejamento
               and p_estagio_critico <> reg_mqop_050.codigo_estagio
               then
                  select count(*)
                  into v_existe_650
                  from tmrp_650
                  where tmrp_650.ordem_trabalho     = v_ordem_agrupamento_aux
                    and tmrp_650.tipo_alocacao      = p_tipo_alocacao
                    and tmrp_650.tipo_recurso       = p_tipo_recurso
                    and tmrp_650.pedido_venda       = p_pedido_venda_ordem_plan
                    and tmrp_650.nivel_produto      = v_nivel_calculo_estagio
                    and tmrp_650.codigo_estagio     = reg_mqop_050.codigo_estagio
                    and tmrp_650.seq_ordem_trabalho = p_seq_ordem_trabalho;
               end if;

               if v_existe_650 > 0
               then
                  if p_estagio_critico = reg_mqop_050.codigo_estagio
                  then
                     -- Encontra o produto sem o filtro da cor do item (p_item_ordem_plan).
                     select tmrp_650.nivel_produto,
                            tmrp_650.grupo_produto,
                            tmrp_650.subgru_produto,
                            tmrp_650.item_produto
                     into   v_niv_650_compara_gantt_benef,
                            v_gru_650_compara_gantt_benef,
                            v_sub_650_compara_gantt_benef,
                            v_ite_650_compara_gantt_benef
                     from tmrp_650
                     where tmrp_650.ordem_trabalho = v_ordem_agrupamento_aux
                       and tmrp_650.tipo_alocacao  = p_tipo_alocacao
                       and tmrp_650.tipo_recurso   = p_tipo_recurso
                       and tmrp_650.pedido_venda   = p_pedido_venda_ordem_plan
                       and tmrp_650.nivel_produto  = v_nivel_calculo_estagio
                       and tmrp_650.codigo_estagio = reg_mqop_050.codigo_estagio
                       and rownum = 1;
                  end if;

                  -- Se produto do gantt for diferente do produto que esta sendo analisado,
                  -- encontra qual deles (produto atual do gantt ou novo produto) que tenha a maior quantidade,
                  -- para atualizar no gantt.
                  if  p_tipo_alocacao                = 0 --- Ordens de Planejamento
                  and v_niv_650_compara_gantt_benef <> v_nivel_calculo_estagio
                  and v_gru_650_compara_gantt_benef <> v_grupo_produto_gantt_benef
                  and v_sub_650_compara_gantt_benef <> v_subgrupo_produto_gantt_benef
                  and v_ite_650_compara_gantt_benef <> p_item_ordem_plan
                  then
                     begin
                        -- Encontra quantidade do produto que esta atualmente gravado no gantt.
                        select sum(tmrp_625.qtde_reserva_planejada)
                        into   v_qtde_res_plan_produto_650
                        from tmrp_625
                        where tmrp_625.ordem_planejamento = p_ordem_planej_producao
                          and ((tmrp_625.pedido_venda     = p_pedido_venda_ordem_plan and tmrp_625.pedido_venda   > 0) or
                               (tmrp_625.pedido_reserva   = p_pedido_venda_ordem_plan and tmrp_625.pedido_reserva > 0))
                          and tmrp_625.nivel_produto      = v_niv_650_compara_gantt_benef
                          and tmrp_625.grupo_produto      = v_gru_650_compara_gantt_benef
                          and tmrp_625.subgrupo_produto   = v_sub_650_compara_gantt_benef
                          and tmrp_625.item_produto       = v_ite_650_compara_gantt_benef
                        group by tmrp_625.nivel_produto,
                                 tmrp_625.grupo_produto,
                                 tmrp_625.subgrupo_produto,
                                 tmrp_625.item_produto;
                     exception when others then
                        v_qtde_res_plan_produto_650 := 0;
                     end;

                     begin
                        -- Encontra quantidade do novo produto.
                        select sum(tmrp_625.qtde_reserva_planejada)
                        into   v_qtde_res_plan_produto_novo
                        from tmrp_625
                        where tmrp_625.ordem_planejamento = p_ordem_planej_producao
                          and ((tmrp_625.pedido_venda     = p_pedido_venda_ordem_plan and tmrp_625.pedido_venda   > 0) or
                               (tmrp_625.pedido_reserva   = p_pedido_venda_ordem_plan and tmrp_625.pedido_reserva > 0))
                          and tmrp_625.nivel_produto      = v_nivel_calculo_estagio
                          and tmrp_625.grupo_produto      = v_grupo_produto_gantt_benef
                          and tmrp_625.subgrupo_produto   = v_subgrupo_produto_gantt_benef
                          and tmrp_625.item_produto       = p_item_ordem_plan
                        group by tmrp_625.nivel_produto,
                                 tmrp_625.grupo_produto,
                                 tmrp_625.subgrupo_produto,
                                 tmrp_625.item_produto;
                     exception when others then
                           v_qtde_res_plan_produto_novo := p_qtde_produto_plan;
                     end;

                     if v_qtde_res_plan_produto_novo < v_qtde_res_plan_produto_650
                     then
                        v_grupo_produto_gantt_benef    := v_gru_650_compara_gantt_benef;
                        v_subgrupo_produto_gantt_benef := v_sub_650_compara_gantt_benef;
                     else
                        v_grupo_produto_gantt_benef    := p_grupo_ordem_plan;
                        v_subgrupo_produto_gantt_benef := p_subgru_ordem_plan;
                     end if;
                  else
                     if  p_tipo_alocacao                = 2 --- Ordens de Beneficiamento
                     and v_niv_650_compara_gantt_benef <> v_nivel_calculo_estagio
                     and v_gru_650_compara_gantt_benef <> v_grupo_produto_gantt_benef
                     and v_sub_650_compara_gantt_benef <> v_subgrupo_produto_gantt_benef
                     and v_ite_650_compara_gantt_benef <> p_item_ordem_plan
                     then
                        begin
                           -- Encontra quantidade do produto que esta atualmente gravado no gantt.
                           select sum(pcpb_020.qtde_quilos_prog) into v_qtde_prog_ordem_produto_650
                           from pcpb_020, pcpb_010, pcpb_100
                           where  pcpb_100.tipo_ordem        = p_tipo_ordem_agrupamento
                             and  pcpb_100.ordem_agrupamento = p_ordem_planej_producao
                             and (pcpb_100.ordem_producao   = p_ordem_beneficiamento or p_ordem_beneficiamento = 0)
                             and  pcpb_010.ordem_producao    = pcpb_100.ordem_producao
                             and  pcpb_020.ordem_producao    = pcpb_010.ordem_producao
                             and  pcpb_020.pano_sbg_nivel99  = v_niv_650_compara_gantt_benef
                             and  pcpb_020.pano_sbg_grupo    = v_gru_650_compara_gantt_benef
                             and  pcpb_020.pano_sbg_subgrupo = v_sub_650_compara_gantt_benef
                             and  pcpb_020.pano_sbg_item     = v_ite_650_compara_gantt_benef
                           group by pcpb_020.pano_sbg_nivel99,
                                    pcpb_020.pano_sbg_grupo,
                                    pcpb_020.pano_sbg_subgrupo,
                                    pcpb_020.pano_sbg_item;
                        exception when others then
                              v_qtde_prog_ordem_produto_650 := 0;
                        end;

                        begin
                           -- Encontra quantidade do novo produto.
                           select sum(pcpb_020.qtde_quilos_prog) into v_qtde_prog_ordem_produto_novo
                           from pcpb_020, pcpb_010, pcpb_100
                           where  pcpb_100.tipo_ordem        = p_tipo_ordem_agrupamento
                             and  pcpb_100.ordem_agrupamento = p_ordem_planej_producao
                             and (pcpb_100.ordem_producao   = p_ordem_beneficiamento or p_ordem_beneficiamento = 0)
                             and  pcpb_010.ordem_producao    = pcpb_100.ordem_producao
                             and  pcpb_020.ordem_producao    = pcpb_010.ordem_producao
                             and  pcpb_020.pano_sbg_nivel99  = v_nivel_calculo_estagio
                             and  pcpb_020.pano_sbg_grupo    = v_grupo_produto_gantt_benef
                             and  pcpb_020.pano_sbg_subgrupo = v_subgrupo_produto_gantt_benef
                             and  pcpb_020.pano_sbg_item     = p_item_ordem_plan
                           group by pcpb_020.pano_sbg_nivel99,
                                    pcpb_020.pano_sbg_grupo,
                                    pcpb_020.pano_sbg_subgrupo,
                                    pcpb_020.pano_sbg_item;
                           exception
                           when others then
                              v_qtde_prog_ordem_produto_novo := p_qtde_produto_plan;
                        end;

                        if v_qtde_prog_ordem_produto_novo < v_qtde_prog_ordem_produto_650
                        then
                           v_grupo_produto_gantt_benef    := v_gru_650_compara_gantt_benef;
                           v_subgrupo_produto_gantt_benef := v_sub_650_compara_gantt_benef;
                        else
                           v_grupo_produto_gantt_benef    := p_grupo_ordem_plan;
                           v_subgrupo_produto_gantt_benef := p_subgru_ordem_plan;
                        end if;
                     end if;
                  end if;
               end if;

               if v_existe_650 = 0
               then
                  --Busca tipo da ordem,
                  begin
                      select tipo_ordem.tipo_ordem_prioritario
                      into v_tipo_ordem_apresentacao
                      from (select p1.ordem_agrupamento, p2.ordem_producao, p2.tipo_ordem,
                            decode(p2.tipo_ordem,6,1, --Amostra tem prioridade = 1
                                                 5,2, --Reposicao tem prioridade = 2
                                                 1,3, --Reprocesso tem prioridade = 3
                                                 2,3, --Reprocesso tem prioridade = 3
                                                 0,4, --Normal tem prioridade = 3
                                                 3,5, --Aproveitamento tem prioridade = 5
                                                 4,5, --Aproveitamento tem prioridade = 5
                                                 1) tipo_ordem_prioritario --Se nao encontrar igual a zero
                            from pcpb_100 p1, pcpb_010 p2
                            where p1.ordem_producao = p2.ordem_producao
                            and p1.ordem_agrupamento = p_ordem_planej_producao -- aqui vai o numero da OT
                            order by p1.ordem_agrupamento, tipo_ordem_prioritario) tipo_ordem
                      where rownum = 1;
                      exception
                      when others then
                          v_tipo_ordem_apresentacao := p_tipo_ordem_agrupamento;
                  end;

                  insert into tmrp_650
                    (tipo_alocacao,                   ordem_trabalho,
                     pedido_venda,                    nivel_produto,
                     grupo_produto,                   subgru_produto,
                     item_produto,                    quantidade,
                     codigo_estagio,                  minutos_unitario,
                     sequencia_estagio,               seq_operacao,
                     lead_time,                       qtde_recursos,
                     seq_estagio_ordem,               tipo_recurso,
                     tipo_reserva,                    tempo_producao,
                     tipo_ordem_agrupamento,          tipo_ordem_apresentacao,
                     mensagem_painel,                 seq_ordem_trabalho
                  ) values (
                     p_tipo_alocacao,                 v_ordem_agrupamento_aux,
                     p_pedido_venda_ordem_plan,       v_nivel_calculo_estagio,
                     v_grupo_produto_gantt_benef,     v_subgrupo_produto_gantt_benef,
                     p_item_ordem_plan,               p_qtde_produto_plan,
                     reg_mqop_050.codigo_estagio,     v_minutos_operacao,
                     reg_mqop_050.sequencia_estagio,  reg_mqop_050.seq_operacao,
                     reg_mqop_050.leed_time,          1,
                     0,                               p_tipo_recurso,
                     p_tipo_reserva,                  decode(reg_mqop_050.tipo_operacao,1,v_minutos_operacao * p_qtde_produto_plan,2,v_minutos_operacao),
                     p_tipo_ordem_agrupamento,        v_tipo_ordem_apresentacao,
                     v_mensagem_painel,               p_seq_ordem_trabalho
                  );
               else
                  begin
                     update tmrp_650
                     set tmrp_650.quantidade           = tmrp_650.quantidade     + p_qtde_produto_plan,
                         tmrp_650.tempo_producao       = tmrp_650.tempo_producao + decode(reg_mqop_050.tipo_operacao,1,v_minutos_operacao * p_qtde_produto_plan,2,v_minutos_operacao),
                         tmrp_650.grupo_produto        = v_grupo_produto_gantt_benef,
                         tmrp_650.subgru_produto       = v_subgrupo_produto_gantt_benef,
                         tmrp_650.mensagem_painel      = v_mensagem_painel
                     where tmrp_650.ordem_trabalho     = v_ordem_agrupamento_aux
                       and tmrp_650.tipo_alocacao      = p_tipo_alocacao
                       and tmrp_650.tipo_recurso       = p_tipo_recurso
                       and tmrp_650.pedido_venda       = p_pedido_venda_ordem_plan
                       and tmrp_650.nivel_produto      = v_nivel_calculo_estagio
                       and tmrp_650.codigo_estagio     = reg_mqop_050.codigo_estagio
                       and tmrp_650.seq_ordem_trabalho = p_seq_ordem_trabalho;
                  end;
               end if;

               v_salva_estagio := reg_mqop_050.codigo_estagio;

               if v_salva_estagio > v_ultimo_estagio
               then
                  v_ultimo_estagio := v_salva_estagio;
               end if;
            end if;

            -- Executa calculo para atualizar a sequencia dos estagios para producao.
            v_primeiro      := 0;
            v_sequencia_upd := 0;

            for reg_tmrp_650 in (select tmrp_650.codigo_estagio,  tmrp_650.seq_operacao,
                                        tmrp_650.ordem_trabalho
                                 from   tmrp_650
                                 where ((tmrp_650.ordem_trabalho   = p_ordem_planej_producao and p_tipo_alocacao = 0) or
                                       ((tmrp_650.ordem_trabalho   = p_ordem_planej_producao or
                                         tmrp_650.ordem_trabalho   = p_ordem_beneficiamento) and p_tipo_alocacao = 2))
                                   and tmrp_650.tipo_alocacao      = p_tipo_alocacao
                                   and tmrp_650.tipo_recurso       = p_tipo_recurso
                                   and tmrp_650.pedido_venda       = p_pedido_venda_ordem_plan
                                   and tmrp_650.nivel_produto      = v_nivel_calculo_estagio
                                   and tmrp_650.item_produto       = p_item_ordem_plan
                                   and tmrp_650.seq_ordem_trabalho = p_seq_ordem_trabalho
                                 order by tmrp_650.codigo_estagio,
                                          tmrp_650.seq_operacao asc)
            loop
               if v_primeiro = 1
               then
                  if v_estagio_ante <> reg_tmrp_650.codigo_estagio
                  then
                     update tmrp_650
                     set   tmrp_650.sequencia_estagio  = 9
                     where tmrp_650.ordem_trabalho     = reg_tmrp_650.ordem_trabalho
                       and tmrp_650.tipo_alocacao      = p_tipo_alocacao
                       and tmrp_650.tipo_recurso       = p_tipo_recurso
                       and tmrp_650.pedido_venda       = p_pedido_venda_ordem_plan
                       and tmrp_650.nivel_produto      = v_nivel_calculo_estagio
                       and tmrp_650.item_produto       = p_item_ordem_plan
                       and tmrp_650.codigo_estagio     = v_estagio_ante
                       and tmrp_650.seq_operacao       = v_seq_operacao_ante
                       and tmrp_650.seq_ordem_trabalho = p_seq_ordem_trabalho;

                     v_sequencia_upd := 0;
                  else
                     update tmrp_650
                     set   tmrp_650.sequencia_estagio  = v_sequencia_upd
                     where tmrp_650.ordem_trabalho     = reg_tmrp_650.ordem_trabalho
                       and tmrp_650.tipo_alocacao      = p_tipo_alocacao
                       and tmrp_650.tipo_recurso       = p_tipo_recurso
                       and tmrp_650.pedido_venda       = p_pedido_venda_ordem_plan
                       and tmrp_650.nivel_produto      = v_nivel_calculo_estagio
                       and tmrp_650.item_produto       = p_item_ordem_plan
                       and tmrp_650.codigo_estagio     = v_estagio_ante
                       and tmrp_650.seq_operacao       = v_seq_operacao_ante
                       and tmrp_650.seq_ordem_trabalho = p_seq_ordem_trabalho;
                  end if;
               end if;

               v_estagio_ante       := reg_tmrp_650.codigo_estagio;
               v_seq_operacao_ante  := reg_tmrp_650.seq_operacao;
               v_ordem_trabalho_ant := reg_tmrp_650.ordem_trabalho;

               v_sequencia_upd := v_sequencia_upd + 1;
               v_primeiro      := 1;
            end loop;

            begin
               update tmrp_650
               set sequencia_estagio = 9
               where tmrp_650.ordem_trabalho     = v_ordem_trabalho_ant
                 and tmrp_650.tipo_alocacao      = p_tipo_alocacao
                 and tmrp_650.tipo_recurso       = p_tipo_recurso
                 and tmrp_650.pedido_venda       = p_pedido_venda_ordem_plan
                 and tmrp_650.nivel_produto      = v_nivel_calculo_estagio
                 and tmrp_650.item_produto       = p_item_ordem_plan
                 and tmrp_650.codigo_estagio     = v_estagio_ante
                 and tmrp_650.seq_operacao       = v_seq_operacao_ante
                 and tmrp_650.seq_ordem_trabalho = p_seq_ordem_trabalho;
            end;
         end if;
      end if;
      -- Fim da logica para gerar o GANTT dos estagios da area 2 - Beneficiamento de Tecidos (2) e Fios (7)


      -- Inicio da logica para gerar o GANTT dos estagios da area 4 - Tecelagem
      if v_nivel_calculo_estagio = '4' -- Tecelagem
      and v_nr_registro_mqop_030 > 0
      then

            v_codigo_recurso  := null;

            if  p_tipo_alocacao    = 4 --- Ordens de Tecelagem
            then
                v_codigo_recurso := p_codigo_recurso;
            end if;

            if reg_mqop_050.minutos = 0
            then
               if v_sub_calculo_estagio <> p_subgru_ordem_plan
               or v_item_calculo_estagio <> p_item_ordem_plan
               then
                  v_mensagem_auxiliar := '(' || p_nivel_ordem_plan || '.' ||
                                         p_grupo_ordem_plan || '.' ||
                                         p_subgru_ordem_plan || '.' ||
                                         p_item_ordem_plan || ')';
               else
                  v_mensagem_auxiliar := '';
               end if;

               v_mensagem_painel :=  v_nivel_calculo_estagio || '.' ||
                                     v_grupo_calculo_estagio || '.' ||
                                     v_sub_calculo_estagio   || '.' ||
                                     v_item_calculo_estagio  || ' ' || v_mensagem_auxiliar || ' ' ||
                                     'A.: ' || to_char(p_alternativa_produto ,'00') || ' ' ||
                                     'R.: ' || to_char(p_roteiro_produto ,'00') || ' / ';

               v_mensagem_painel := v_mensagem_painel || 'Estagio/Etapa' || ': '  || to_char(reg_mqop_050.codigo_estagio,'99900')     || ' ' ||
                                       'Operacao/Operacion' || ': ' || to_char(reg_mqop_050.codigo_operacao,'00000')|| chr(010);
            end if;

         select count(*)
         into v_existe_retilineo
         from basi_030
         where basi_030.nivel_estrutura = v_nivel_calculo_estagio
           and basi_030.referencia = v_grupo_calculo_estagio
           and basi_030.tipo_produto in (3,4,6); --Retilineo

         if v_existe_retilineo > 0 and p_tipo_alocacao = 0
         then
             v_subgru_ordem_plan := '000';
         else
             v_subgru_ordem_plan := p_subgru_ordem_plan;
         end if;

         select count(*)
         into v_existe_650
         from tmrp_650
         where tmrp_650.ordem_trabalho     = p_ordem_planej_producao
           and tmrp_650.tipo_alocacao      = p_tipo_alocacao
           and tmrp_650.tipo_recurso       = p_tipo_recurso
           and tmrp_650.pedido_venda       = p_pedido_venda_ordem_plan
           and tmrp_650.nivel_produto      = v_nivel_calculo_estagio
           and tmrp_650.grupo_produto      = v_grupo_calculo_estagio
           and tmrp_650.item_produto       = p_item_ordem_plan
           and tmrp_650.codigo_estagio     = reg_mqop_050.codigo_estagio
           and tmrp_650.seq_ordem_trabalho = p_seq_ordem_trabalho;

         if v_existe_retilineo > 0 and v_existe_650 > 0
         then
            update tmrp_650
            set   tmrp_650.tempo_producao     = tempo_producao + decode(reg_mqop_050.tipo_operacao,1,reg_mqop_050.minutos * p_qtde_produto_plan,reg_mqop_050.minutos),
                  tmrp_650.minutos_unitario   = (tempo_producao + reg_mqop_050.tipo_operacao) / (quantidade + p_qtde_produto_plan),
                  tmrp_650.quantidade         = quantidade + p_qtde_produto_plan,
                  tmrp_650.mensagem_painel    = v_mensagem_painel
            where tmrp_650.ordem_trabalho     = p_ordem_planej_producao
              and tmrp_650.tipo_alocacao      = p_tipo_alocacao
              and tmrp_650.tipo_recurso       = p_tipo_recurso
              and tmrp_650.pedido_venda       = p_pedido_venda_ordem_plan
              and tmrp_650.nivel_produto      = v_nivel_calculo_estagio
              and tmrp_650.grupo_produto      = v_grupo_calculo_estagio
              and tmrp_650.item_produto       = p_item_ordem_plan
              and tmrp_650.codigo_estagio     = reg_mqop_050.codigo_estagio
              and tmrp_650.seq_ordem_trabalho = p_seq_ordem_trabalho;
         else
            insert into tmrp_650 (
                tipo_alocacao,                  ordem_trabalho,
                pedido_venda,                   nivel_produto,
                grupo_produto,                  subgru_produto,
                item_produto,                   quantidade,
                codigo_estagio,                 minutos_unitario,
                sequencia_estagio,              seq_operacao,
                lead_time,                      qtde_recursos,
                seq_estagio_ordem,              tipo_recurso,
                tipo_reserva,                   tempo_producao,
                tipo_ordem_agrupamento,         mensagem_painel,
                seq_ordem_trabalho,             codigo_recurso
            ) values (
                p_tipo_alocacao,                p_ordem_planej_producao,
                p_pedido_venda_ordem_plan,      v_nivel_calculo_estagio,
                v_grupo_calculo_estagio,        v_subgru_ordem_plan,
                p_item_ordem_plan,              p_qtde_produto_plan,
                reg_mqop_050.codigo_estagio,    reg_mqop_050.minutos,
                reg_mqop_050.sequencia_estagio, reg_mqop_050.seq_operacao,
                reg_mqop_050.leed_time,         1,
                0,                              p_tipo_recurso,
                p_tipo_reserva,                 decode(reg_mqop_050.tipo_operacao,1,reg_mqop_050.minutos * p_qtde_produto_plan,reg_mqop_050.minutos),
                p_tipo_ordem_agrupamento,       v_mensagem_painel,
                p_seq_ordem_trabalho,           v_codigo_recurso
             );
         end if;
      end if; -- if v_nivel_calculo_estagio = '4'Tecelagem
      -- Fim da logica para gerar o GANTT dos estagios da area 4 - Tecelagem

   end loop; --- Roteiro de producao

end inter_pr_cria_estagios_painel;

 

/

exec inter_pr_recompile;

