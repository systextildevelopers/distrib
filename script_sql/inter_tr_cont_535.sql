
  CREATE OR REPLACE TRIGGER "INTER_TR_CONT_535" 
before insert or update of
          descricao,         tp_livro_aux_sped,
          tipo_conta,        nivel,
          patr_result,       cod_reduzido,
          conta_mae,         debito_credito,
          indicador_natu,    indic_natu_sped,
          conta_contabil_ref

on cont_535
for each row

begin
   if updating  and
     (:old.descricao          <> :new.descricao          or
      :old.tp_livro_aux_sped  <> :new.tp_livro_aux_sped  or
      :old.tipo_conta         <> :new.tipo_conta         or
      :old.nivel              <> :new.nivel              or
      :old.patr_result        <> :new.patr_result        or
      :old.cod_reduzido       <> :new.cod_reduzido       or
      :old.conta_mae          <> :new.conta_mae          or
      :old.debito_credito     <> :new.debito_credito     or
      :old.indicador_natu     <> :new.indicador_natu     or
      :old.indic_natu_sped    <> :new.indic_natu_sped    or
      :old.conta_contabil_ref <> :new.conta_contabil_ref)
   or inserting
   then
      :new.data_alteracao := sysdate;
   end if;

end inter_tr_cont_535;
-- ALTER TRIGGER "INTER_TR_CONT_535" ENABLE
 

/

exec inter_pr_recompile;

