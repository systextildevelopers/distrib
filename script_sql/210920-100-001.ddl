alter table fatu_030 add 
(   total_qtde          number(17,2) default 0,
    total_mercadoria    number(17,2) default 0,
    valor_nota_fatura   number(17,2) default 0,
    valor_nota_fiscal   number(17,2) default 0
);

exec inter_pr_recompile;
