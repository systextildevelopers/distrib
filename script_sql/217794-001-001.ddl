create table cpvd_421
( pedido_venda     NUMBER(6) default 0 not null,
  data_eliminacao  DATE not null,
  seq_informacao   NUMBER(3) default 0 not null,
  tipo_informacao  NUMBER(2) default 0,
  data_informacao  DATE,
  informante       VARCHAR2(25) default '',
  descricao        VARCHAR2(2000) default '',
  data_complemento DATE,
  usuario          VARCHAR2(15) default '',
  hora_informacao  DATE
);

alter table cpvd_421
add constraint pk_cpvd_421 primary key (pedido_venda, data_eliminacao , seq_informacao);
