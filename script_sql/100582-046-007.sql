create or replace trigger inter_tr_obrf_089_2_obrf_889
     after delete
         or update 
     on obrf_089
     for each row
declare
begin
   if updating 
   then 
      begin
         update obrf_889
         set situacao_item             = :new.situacao_item,
             sequencia                 = :new.sequencia,
             tipo_solicitacao_conserto = :new.tipo_solicitacao_conserto,
             tag_atualiza_conserto     = :new.tag_atualiza_conserto,
             nivel_estrutura           = :new.nivel_estrutura,
             grupo_estrutura           = :new.grupo_estrutura,
             subgrupo_estrutura        = :new.subgrupo_estrutura,
             item_estrutura            = :new.item_estrutura,
             ordem_producao            = :new.ordem_producao,
             ordem_confeccao           = :new.ordem_confeccao,
             periodo_producao          = :new.periodo_producao,
             seq_ordem_confeccao       = :new.seq_ordem_confeccao
          where numero_solicitacao = :new.numero_solicitacao
            and codigo_barras      = :new.codigo_barras
            and ordem_servico      = :new.ordem_servico;
      end; 
   end if;
   if deleting
   then
      begin
         delete from obrf_889
         where numero_solicitacao = :old.numero_solicitacao
            and codigo_barras     = :old.codigo_barras
            and ordem_servico     = :old.ordem_servico;  
      end;
   end if;
end;
/

exec inter_pr_recompile;
/
