declare 
 
  conta_reg number;
begin
   conta_reg := 0;
    /* SELECIONA E ATUALIZA O FATOR TRIBUTÁRIO DE PRODUTO DE NÍVEL 1 QUE TEM UNIDADE MEDIDA UN  QUE AINDA NÃO TEM INFORMAÇÃO */
    FOR basi010 in (
    select ROWID from basi_010 b
    where b.NIVEL_ESTRUTURA = '1'
      and nvl(b.FATOR_UMED_TRIB,0) = 0
      and exists (select 1 from basi_240 n where n.CLASSIFIC_FISCAL = n.CLASSIFIC_FISCAL and n.UNID_MED_TRIB = 'UN'))
    LOOP
       conta_reg := conta_reg + 1;
        UPDATE basi_010 b
        SET b.FATOR_UMED_TRIB = 1
        where rowid = basi010.rowid;
        
        if conta_reg > 999
        then
           commit;
           conta_reg := 0;
        end if; 
    END LOOP;
    
    commit;
end;
/
