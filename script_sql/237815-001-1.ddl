alter table pedi_170 
add (
    referencia 		varchar2(5) 	default ' ',
    nivel      		varchar2(1) 	default ' ',
    grupo      		varchar2(5) 	default ' ',
    subgrupo   		varchar2(3) 	default ' ',
    item       		varchar2(6) 	default ' ',
	tipo_controle   varchar2(100) 	default ' ',
	nivel_controle  varchar2(100) 	default ' ',
	grupo_economico varchar2(100) 	default ' ',
	tipo_estampa	number(1)		default 0 not null
);


alter table pedi_170 drop constraint PK_PEDI_170; 

alter table pedi_170
add constraint PK_PEDI_170 primary key (PERIODO_COTAS, CODIGO_REGIAO, CODIGO_REPR, ARTIGO_COTAS, CLIENTE9, CLIENTE4, 
										CLIENTE2, REFERENCIA, NIVEL, GRUPO, SUBGRUPO, ITEM, NIVEL_CONTROLE, 
										TIPO_CONTROLE, GRUPO_ECONOMICO,	TIPO_ESTAMPA);
	  
	  
