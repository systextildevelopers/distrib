
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_100_COD_INTE" 
before insert or
       update
       of cod_integracao
on fatu_100
for each row

declare

   Pragma autonomous_transaction;

   v_cod_emabalagem fatu_100.codigo_embalagem %type;
   v_cod_integracao fatu_100.cod_integracao   %type;

begin

   if :new.cod_integracao > 0
   then

      v_cod_integracao := 0;

      if inserting
      then
         v_cod_emabalagem := :new.codigo_embalagem;
      else
         v_cod_emabalagem := :old.codigo_embalagem;
      end if;

      begin

         select max(1)
         into   v_cod_integracao
         from  fatu_100
         where fatu_100.codigo_embalagem <> v_cod_emabalagem
           and fatu_100.cod_integracao   =  :new.cod_integracao;
         exception
            when others then
               v_cod_integracao := 0;

      end;

      if v_cod_integracao = 1
      then
         raise_application_error(-20000, 'Esse codigo de integracao ja esta associado a outra embalagem.');
      end if;
   end if;
end inter_tr_fatu_100_cod_inte;

-- ALTER TRIGGER "INTER_TR_FATU_100_COD_INTE" ENABLE
 

/

exec inter_pr_recompile;

