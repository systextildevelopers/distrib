
  CREATE OR REPLACE  VIEW "INTER_VI_PCPB_030_EFIC_100" ("TIPO", "ORDEM_PRODUCAO", "PANO_NIVEL99", "PANO_GRUPO", "PANO_SUBGRUPO", "PANO_ITEM", "NIVEL_REJEITADO", "GRUPO_REJEITADO", "SUBGRUPO_REJEITADO", "ITEM_REJEITADO", "CODIGO_DEPOSITO", "PEDIDO_CORTE", "SEQUENCI_PERIODO", "NR_PEDIDO_ORDEM", "PERIODO_PRODUCAO", "NUMERO_PARTIDA", "SEQ_ORDEM_PROD", "QTDE_ROLOS_PROG", "QTDE_QUILOS_PROG", "QTDE_QUILOS_PRODUZIDO", "QTDE_ROLOS_PRODUZIDO", "ALTERNATIVA", "ROTEIRO", "QTDE_UNIDADE_PROG", "CODIGO_TRANSACAO", "CODIGO_ACOMP", "LOTE_ACOMP", "SEQUENCIA", "SITUACAO", "CODIGO_EMBALAGEM", "EXECUTA_TRIGGER", "QTDE_UNIDADE", "QTDE_UNIDADE_PROD", "ROLADA", "SEQ_ROLADA", "SEQUENCIA_TINGIMENTO") AS
  select  1 tipo,
        pcpb_030.ordem_producao       , pcpb_030.pano_nivel99,
        pcpb_030.pano_grupo           , pcpb_030.pano_subgrupo,
        pcpb_030.pano_item            , '' nivel_rejeitado, ''
        grupo_rejeitado               , '' subgrupo_rejeitado,
        '' item_rejeitado             , pcpb_030.codigo_deposito,
        pcpb_030.pedido_corte         , pcpb_030.sequenci_periodo,
        pcpb_030.nr_pedido_ordem      , pcpb_030.periodo_producao,
        pcpb_030.numero_partida       , pcpb_030.seq_ordem_prod,
        pcpb_030.qtde_rolos_prog      , pcpb_030.qtde_quilos_prog,
        pcpb_030.qtde_quilos_produzido, pcpb_030.qtde_rolos_produzido,
        pcpb_030.alternativa          , pcpb_030.roteiro,
        pcpb_030.qtde_unidade_prog    , pcpb_030.codigo_transacao,
        pcpb_030.codigo_acomp         , pcpb_030.lote_acomp,
        pcpb_030.sequencia            , pcpb_030.situacao,
        pcpb_030.codigo_embalagem     , pcpb_030.executa_trigger,
        pcpb_030.qtde_unidade         , pcpb_030.qtde_unidade_prod,
        pcpb_030.rolada               , pcpb_030.seq_rolada,
        pcpb_030.sequencia_tingimento
from pcpb_030
where pcpb_030.rejeicao = 0
  and not exists (select 1 from efic_100
                  where pcpb_030.ordem_producao  = efic_100.numero_ordem
                    and pcpb_030.pano_nivel99    = efic_100.prod_rej_nivel99
                    and pcpb_030.pano_grupo      = efic_100.prod_rej_grupo
                    and pcpb_030.pano_subgrupo   = efic_100.prod_rej_subgrupo
                    and pcpb_030.pano_item       = efic_100.prod_rej_item
                    and pcpb_030.sequencia       = efic_100.sequencia_justificativa
                    and pcpb_030.codigo_deposito = efic_100.deposito
                    and efic_100.area_producao   = 2
                    and efic_100.classificacao in (2,3,4,8)
                    and pcpb_030.codigo_deposito = efic_100.dep_classificado
                    and pcpb_030.pedido_corte    = efic_100.motivo_forne)
union
select  1 tipo,
        pcpb_030.ordem_producao       , pcpb_030.pano_nivel99,
        pcpb_030.pano_grupo           , pcpb_030.pano_subgrupo,
        pcpb_030.pano_item            , '' nivel_rejeitado, ''
        grupo_rejeitado               , '' subgrupo_rejeitado,
        '' item_rejeitado             , pcpb_030.codigo_deposito,
        pcpb_030.pedido_corte         , pcpb_030.sequenci_periodo,
        pcpb_030.nr_pedido_ordem      , pcpb_030.periodo_producao,
        pcpb_030.numero_partida       , pcpb_030.seq_ordem_prod,
        (pcpb_030.qtde_rolos_prog - efic100.qtde_defeitos) qtde_rolos_prog, (pcpb_030.qtde_quilos_prog - efic100.quantidade) qtde_quilos_prog,
        pcpb_030.qtde_quilos_produzido, pcpb_030.qtde_rolos_produzido,
        pcpb_030.alternativa          , pcpb_030.roteiro,
        pcpb_030.qtde_unidade_prog    , pcpb_030.codigo_transacao,
        pcpb_030.codigo_acomp         , pcpb_030.lote_acomp,
        pcpb_030.sequencia            , pcpb_030.situacao,
        pcpb_030.codigo_embalagem     , pcpb_030.executa_trigger,
        pcpb_030.qtde_unidade         , pcpb_030.qtde_unidade_prod,
        pcpb_030.rolada               , pcpb_030.seq_rolada,
        pcpb_030.sequencia_tingimento
from pcpb_030,
     (select efic_100.numero_ordem, efic_100.prod_rej_nivel99,
             efic_100.prod_rej_grupo, efic_100.prod_rej_subgrupo,
             efic_100.prod_rej_item, efic_100.sequencia_justificativa,
             efic_100.deposito, efic_100.dep_classificado,
             sum(efic_100.qtde_defeitos) qtde_defeitos, sum(efic_100.quantidade) quantidade,
             efic_100.motivo_forne
      from efic_100
      where efic_100.area_producao  = 2
        and efic_100.classificacao in (2,3,4,8)
      group by efic_100.numero_ordem  , efic_100.prod_rej_nivel99,
               efic_100.prod_rej_grupo, efic_100.prod_rej_subgrupo,
               efic_100.prod_rej_item , efic_100.sequencia_justificativa,
               efic_100.deposito      , efic_100.dep_classificado,
               efic_100.motivo_forne ) efic100
where pcpb_030.ordem_producao = efic100.numero_ordem
  and pcpb_030.pano_nivel99   = efic100.prod_rej_nivel99
  and pcpb_030.pano_grupo     = efic100.prod_rej_grupo
  and pcpb_030.pano_subgrupo  = efic100.prod_rej_subgrupo
  and pcpb_030.pano_item      = efic100.prod_rej_item
  and pcpb_030.sequencia      = efic100.sequencia_justificativa
  and pcpb_030.codigo_deposito= efic100.deposito
  and pcpb_030.codigo_deposito= efic100.dep_classificado
  and pcpb_030.pedido_corte   = efic100.motivo_forne
union
select 2 tipo,
       pcpb_030.ordem_producao                , efic_100.nivel_justificativa pano_nivel99,
       efic_100.grupo_justificativa pano_grupo, efic_100.sub_justificativa pano_subgrupo,
       efic_100.item_justificativa pano_item  , pcpb_030.pano_nivel99 nivel_rejeitado,
       pcpb_030.pano_grupo grupo_rejeitado    , pcpb_030.pano_subgrupo subgrupo_rejeitado,
       pcpb_030.pano_item item_rejeitado      , efic_100.dep_classificado codigo_deposito,
       pcpb_030.pedido_corte                  , pcpb_030.sequenci_periodo,
       pcpb_030.nr_pedido_ordem               , pcpb_030.periodo_producao,
       pcpb_030.numero_partida                , pcpb_030.seq_ordem_prod,
       efic_100.qtde_defeitos qtde_rolos_prog , efic_100.quantidade qtde_quilos_prog,
       pcpb_030.qtde_quilos_produzido         , pcpb_030.qtde_rolos_produzido,
       pcpb_030.alternativa                   , pcpb_030.roteiro,
       pcpb_030.qtde_unidade_prog             , pcpb_030.codigo_transacao,
       pcpb_030.codigo_acomp                  , pcpb_030.lote_acomp,
       pcpb_030.sequencia                     , pcpb_030.situacao,
       pcpb_030.codigo_embalagem              , pcpb_030.executa_trigger,
       pcpb_030.qtde_unidade                  , pcpb_030.qtde_unidade_prod,
       pcpb_030.rolada                        , pcpb_030.seq_rolada,
       pcpb_030.sequencia_tingimento
from pcpb_030, efic_100
where pcpb_030.ordem_producao = efic_100.numero_ordem
  and pcpb_030.pano_nivel99   = efic_100.prod_rej_nivel99
  and pcpb_030.pano_grupo     = efic_100.prod_rej_grupo
  and pcpb_030.pano_subgrupo  = efic_100.prod_rej_subgrupo
  and pcpb_030.pano_item      = efic_100.prod_rej_item
  and pcpb_030.sequencia      = efic_100.sequencia_justificativa
  and pcpb_030.codigo_deposito= efic_100.dep_classificado
  and pcpb_030.pedido_corte   = efic_100.cod_defeito_agrup
  and efic_100.area_producao  = 2
  and efic_100.classificacao in (2,3,4,8);
