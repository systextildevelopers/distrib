create or replace trigger "INTER_TR_FATU_070_FIDIC"
   after update of cod_canc_duplic, saldo_duplicata, nr_identificacao
   on fatu_070
   for each row
declare
	eh_identificacao_fidic_old number;
	eh_identificacao_fidic number;
	eh_tipo_fidic number;
	v_sequencia_informacao number;
	v_cod_mensagem_new number;
	v_cod_mensagem_old number;
	v_descricao varchar2(1000);

	ws_usuario_rede           varchar2(20);
	ws_maquina_rede           varchar2(40);
	ws_aplicativo             varchar2(20);
	ws_sid                    number(9);
	ws_usuario_systextil      varchar2(400);
	ws_empresa                number(3);
	ws_locale_usuario         varchar2(5);
    ws_nome_programa hdoc_090.programa%type;
begin
	-- Dados do usuï¿¿rio logado
	inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
						   ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   
	ws_nome_programa := inter_fn_nome_programa(ws_sid); 
  
    if updating
    then
     
		select count(1) into eh_identificacao_fidic_old from fndc_002
		where fndc_002.cod_empresa = :old.codigo_empresa
		and   fndc_002.nr_atributo = :old.nr_identificacao
		and   fndc_002.status = 1;

		select count(1) into eh_identificacao_fidic from fndc_002
		where fndc_002.cod_empresa = :new.codigo_empresa
		and fndc_002.nr_atributo = :new.nr_identificacao
		and fndc_002.status = 1;
	BEGIN
        select count(1) into eh_tipo_fidic from fndc_004 
        where fndc_004.fnd_tipo_fidc = :new.tipo_titulo
        and fndc_004.fnd_empresa   = :new.codigo_empresa;
    exception 
	when no_data_found then
        eh_tipo_fidic := 0;
    END;
    
    if eh_tipo_fidic = 0
    then
		select count(1) into eh_tipo_fidic from fndc_003
        where :new.tipo_titulo = fndc_003.fnd_tipo
        and   :new.codigo_empresa = fndc_003.fnd_empresa;
    end if;

    --Quando o título estiver relacionado a carteira do Fidic e esta for alterada
    --vai ser atualizada a tabela crec_450(Informações gerais).	
    if  eh_identificacao_fidic_old > 0 
    and eh_identificacao_fidic = 0 
    and eh_tipo_fidic = 0 then
    	v_sequencia_informacao := 0; 
	    begin 
		    select  nvl(max(crec_450.seq_informacao)+1, 1)
		  	into v_sequencia_informacao
		  	from crec_450
		  	where crec_450.codigo_empresa = :new.CODIGO_EMPRESA
		  	and crec_450.cnpj_cli9 = :new.CLI_DUP_CGC_CLI9
		  	and crec_450.cnpj_cli4 = :new.CLI_DUP_CGC_CLI4
		  	and crec_450.cnpj_cli2 = :new.CLI_DUP_CGC_CLI2
		  	and crec_450.tipo_titulo = :new.TIPO_TITULO
		  	and crec_450.num_duplicata = :new.NUM_DUPLICATA
		  	and crec_450.seq_duplicatas = :new.SEQ_DUPLICATAS;
		end;
      
		v_cod_mensagem_new := 0;
	    begin
			select fndc_002.cod_mensagem
		 	into v_cod_mensagem_new
		 	from fndc_002
	     	where fndc_002.cod_empresa = :new.codigo_empresa
	       	and   fndc_002.nr_atributo = :new.nr_identificacao
	       	and   fndc_002.status = 1;
	    exception 
		when no_data_found then 
			v_cod_mensagem_new := 0;
		end;
          
	    v_cod_mensagem_old := 0;
	    begin           	
			select fndc_002.cod_mensagem
		 	into v_cod_mensagem_old
		 	from fndc_002
	     	where fndc_002.cod_empresa = :old.codigo_empresa
	       	and fndc_002.nr_atributo = :old.nr_identificacao
	       	and fndc_002.status = 1; 
	    exception
	    when no_data_found then 
			v_cod_mensagem_old := 0;
	    end;
  	 								
	    if  v_cod_mensagem_old > 0 then
   			v_descricao := 'Troca carteira FIDIC ' || :old.nr_identificacao  || ' => ' || :new.nr_identificacao;
   		
	   		insert into crec_450 (
	   			CODIGO_EMPRESA,	 CNPJ_CLI9,
	   			CNPJ_CLI4,		 CNPJ_CLI2,
	   			TIPO_TITULO,	 NUM_DUPLICATA,
	   			SEQ_DUPLICATAS,	 SEQ_INFORMACAO,
	   			TIPO_INFORMACAO, DATA_INFORMACAO,
	   			INFORMANTE,		 DESCRICAO
	   		) values (
	   			:new.CODIGO_EMPRESA, 	:new.CLI_DUP_CGC_CLI9,
	   			:new.CLI_DUP_CGC_CLI4,	:new.CLI_DUP_CGC_CLI2,
	   			:new.TIPO_TITULO,	 	:new.NUM_DUPLICATA,
	   			:new.SEQ_DUPLICATAS, 	v_sequencia_informacao,
	   			v_cod_mensagem_old,		sysdate,
	   			ws_usuario_systextil,	v_descricao
	   		);
	   	 end if;
    end if;
     
   end if;
end INTER_TR_FATU_070_FIDIC;

/
exec inter_pr_recompile;
