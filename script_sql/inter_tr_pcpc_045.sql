create or replace trigger "INTER_TR_PCPC_045" 
  before insert or
         update of
         pcpc040_perconf, pcpc040_ordconf, pcpc040_estconf, sequencia,
         data_producao, hora_producao, qtde_produzida, qtde_pecas_2a,
         qtde_conserto, turno_producao, tipo_entrada_ord, nota_entr_ordem,
         serie_nf_ent_ord, seq_nf_entr_ord, ordem_producao, codigo_usuario,
         qtde_perdas, numero_documento, codigo_deposito, codigo_familia,
         codigo_intervalo, executa_trigger, data_insercao, processo_systextil,
         numero_volume, nr_operadores
         on pcpc_045
  for each row

declare
  v_ultimo_estagio     pcpc_020.ultimo_estagio%type;
  v_pedido_venda       pcpc_020.pedido_venda%type;
  v_nivel_produto      pcpc_040.proconf_nivel99%type;
  v_grupo_produto      pcpc_040.proconf_grupo%type;
  v_subgrupo_produto   pcpc_040.proconf_subgrupo%type;
  v_item_produto       pcpc_040.proconf_item%type;
  v_processo_systextil hdoc_090.programa%type;
  ws_sid               sys.gv_$session.sid%type;
  ws_aplicativo        varchar2(20);

  v_executa_trigger  number;

begin

   if updating or inserting
   then
       -- Dados do usuario logado
       begin
            select substr(program,1,20), sid
            into   ws_aplicativo,        ws_sid
            from sys.gv_$session
            where audsid  = userenv('SESSIONID')
              and inst_id = userenv('INSTANCE')
              and rownum < 2;
       exception when no_data_found then
            ws_aplicativo   := '';
            ws_sid          := 0;
       end;

       -- O where usando impressora, e somente para esta tabela.
       v_processo_systextil := inter_fn_nome_programa(ws_sid); 

       if  updating
       and (v_processo_systextil = 'pcpc_f355'
       or   v_processo_systextil = 'pcpc_f144'
       or   v_processo_systextil = 'tmrp_f070'
       or   v_processo_systextil = 'pcpc_f350')
       then
          :new.processo_systextil := :new.processo_systextil;
       elsif inserting
       and (:new.processo_systextil = 'pcpc_f235'
       or :new.processo_systextil = 'inte_p790')
       then
          :new.processo_systextil := :new.processo_systextil;
       else
          :new.processo_systextil := v_processo_systextil;
       end if;

   end if;


   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then

      if :new.processo_systextil <> 'pcpc_f235'
      or :new.processo_systextil is null
      then
        :new.data_insercao := sysdate;
      end if;

      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger = 0
   then
      if inserting and :new.hora_producao is not null
      then
         :new.hora_producao := to_date('16/11/1989 ' || to_CHAR(sysdate,'hh24:mi'),'dd/mm/yyyy hh24:mi');
      end if;

      -- INICIO - Atualizacao da quantidade produzida na ordem de planejamento.
      if inserting
      then
          begin
             select pedido_venda,     ultimo_estagio
             into   v_pedido_venda,   v_ultimo_estagio
             from pcpc_020
             where pcpc_020.ordem_producao = :new.ordem_producao;
          end;

          begin
             select proconf_nivel99,      proconf_grupo,
                    proconf_subgrupo,     proconf_item
             into   v_nivel_produto,      v_grupo_produto,
                    v_subgrupo_produto,   v_item_produto
             from pcpc_040
             where pcpc_040.ordem_producao   = :new.ordem_producao -- Neylor - Pette
               and pcpc_040.periodo_producao = :new.pcpc040_perconf
               and pcpc_040.ordem_confeccao  = :new.pcpc040_ordconf
               and pcpc_040.codigo_estagio   = :new.pcpc040_estconf;
          end;

          if :new.pcpc040_estconf = v_ultimo_estagio
          then
             -- Atualiza a quantidade produzida de conserto.
             if :new.qtde_conserto > 0
             then
                inter_pr_atu_prod_ordem_planej(1,
                                               :new.ordem_producao,
                                               v_pedido_venda,
                                               v_nivel_produto,
                                               v_grupo_produto,
                                               v_subgrupo_produto,
                                               v_item_produto,
                                               :new.qtde_conserto,
                                               0,
                                               0,
                                               0,
                                               'P');
             end if;
             -- Atualiza o estorno da quantidade produzida de conserto.
             if :new.qtde_conserto < 0
             then
                inter_pr_atu_prod_ordem_planej(1,
                                               :new.ordem_producao,
                                               v_pedido_venda,
                                               v_nivel_produto,
                                               v_grupo_produto,
                                               v_subgrupo_produto,
                                               v_item_produto,
                                               :new.qtde_conserto,
                                               0,
                                               0,
                                               0,
                                               'E');
             end if;

             -- Atualiza a quantidade produzida de segunda qualidade.
             if :new.qtde_pecas_2a > 0
             then
                inter_pr_atu_prod_ordem_planej(1,
                                               :new.ordem_producao,
                                               v_pedido_venda,
                                               v_nivel_produto,
                                               v_grupo_produto,
                                               v_subgrupo_produto,
                                               v_item_produto,
                                               0,
                                               :new.qtde_pecas_2a,
                                               0,
                                               0,
                                               'P');
             end if;
             -- Atualiza o estorno da quantidade produzida de segunda qualidade.
             if :new.qtde_pecas_2a < 0
             then
                inter_pr_atu_prod_ordem_planej(1,
                                               :new.ordem_producao,
                                               v_pedido_venda,
                                               v_nivel_produto,
                                               v_grupo_produto,
                                               v_subgrupo_produto,
                                               v_item_produto,
                                               0,
                                               :new.qtde_pecas_2a,
                                               0,
                                               0,
                                               'E');
             end if;

             -- Atualiza a quantidade produzida de perdas.
             if :new.qtde_perdas > 0
             then
                inter_pr_atu_prod_ordem_planej(1,
                                               :new.ordem_producao,
                                               v_pedido_venda,
                                               v_nivel_produto,
                                               v_grupo_produto,
                                               v_subgrupo_produto,
                                               v_item_produto,
                                               0,
                                               0,
                                               :new.qtde_perdas,
                                               0,
                                               'P');
             end if;
             -- Atualiza o estorno da quantidade produzida de perdas.
             if :new.qtde_perdas < 0
             then
                inter_pr_atu_prod_ordem_planej(1,
                                               :new.ordem_producao,
                                               v_pedido_venda,
                                               v_nivel_produto,
                                               v_grupo_produto,
                                               v_subgrupo_produto,
                                               v_item_produto,
                                               0,
                                               0,
                                               :new.qtde_perdas,
                                               0,
                                               'E');
             end if;

             -- Atualiza a quantidade produzida de primeira qualidade.
             if :new.qtde_produzida > 0
             then
                inter_pr_atu_prod_ordem_planej(1,
                                               :new.ordem_producao,
                                               v_pedido_venda,
                                               v_nivel_produto,
                                               v_grupo_produto,
                                               v_subgrupo_produto,
                                               v_item_produto,
                                               0,
                                               0,
                                               0,
                                               :new.qtde_produzida,
                                               'P');
             end if;
             -- Atualiza o estorno da quantidade produzida de primeira qualidade.
             if :new.qtde_produzida < 0
             then
                inter_pr_atu_prod_ordem_planej(1,
                                               :new.ordem_producao,
                                               v_pedido_venda,
                                               v_nivel_produto,
                                               v_grupo_produto,
                                               v_subgrupo_produto,
                                               v_item_produto,
                                               0,
                                               0,
                                               0,
                                               :new.qtde_produzida,
                                               'E');
             end if;
          end if;
      end if;
      -- FIM - Atualizacao da quantidade produzida na ordem de planejamento.
   end if;
end inter_tr_pcpc_045;

-- ALTER TRIGGER "INTER_TR_PCPC_045" ENABLE
