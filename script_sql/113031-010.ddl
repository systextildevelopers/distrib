alter table basi_140 add disponivel_b2b number(1) default 0;

COMMENT ON COLUMN basi_140.disponivel_b2b IS 'Campo para indicar se a coleção está disponível no sistema B2B.';

exec inter_pr_recompile;
