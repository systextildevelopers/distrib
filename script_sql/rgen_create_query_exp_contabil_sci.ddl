DECLARE
  query_id number(10);
  sql_help clob;
BEGIN

-- CONSULTA
  sql_help := q'[select a.empresa
      ,a.identificador
      ,a.tipo_movimento
      ,a.cnpj_cpf_participante
      ,a.inscricao_estadual
      ,a.numero_do_documento
      ,a.data_do_lancamento
      ,a.serie
      ,a.especie
      ,a.modelo
      ,a.natureza_de_operacao
      ,a.tipo_documento
      ,a.conta_debito
      ,a.conta_credito
      ,a.sequencial
      ,a.nome
      ,a.percentual
      ,a.cnpj_empresa
from STVIEW_exp_contabil_sci a
where a.empresa = {empresa}
and    a.tipo_movimento = {tipo_movimento}
and    a.data_do_lancamento between {data_inicial_data_do_lancamento} and {data_final_data_do_lancamento}]';
  insert into rgen_query ( id, description, editable, sql ) values ( seq_rgen_query.nextval, 'Exp. Cont�bil SCI', 0, sql_help ) RETURNING id INTO query_id;


-- CAMPOS DA CONSULTA
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'EMPRESA', 'integer', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'IDENTIFICADOR', 'text', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'TIPO_MOVIMENTO', 'text', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'CNPJ_CPF_PARTICIPANTE', 'text', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'INSCRICAO_ESTADUAL', 'text', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'NUMERO_DO_DOCUMENTO', 'integer', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'DATA_DO_LANCAMENTO', 'datetime', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'SERIE', 'text', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'ESPECIE', 'text', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'MODELO', 'text', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'NATUREZA_DE_OPERACAO', 'text', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'TIPO_DOCUMENTO', 'integer', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'CONTA_DEBITO', 'text', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'CONTA_CREDITO', 'text', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'SEQUENCIAL', 'integer', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'NOME', 'integer', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'PERCENTUAL', 'text', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'CNPJ_EMPRESA', 'text', query_id );


-- PARAMETROS DA CONSULTA
  insert into rgen_query_params ( id, name, query_id, default_value, fyi_message ) values ( seq_rgen_query_params.nextval, '{empresa}', query_id, '', '' );
  insert into rgen_query_params ( id, name, query_id, default_value, fyi_message ) values ( seq_rgen_query_params.nextval, '{tipo_movimento}', query_id, '', '' );
  insert into rgen_query_params ( id, name, query_id, default_value, fyi_message ) values ( seq_rgen_query_params.nextval, '{data_inicial_data_do_lancamento}', query_id, '', '' );
  insert into rgen_query_params ( id, name, query_id, default_value, fyi_message ) values ( seq_rgen_query_params.nextval, '{data_final_data_do_lancamento}', query_id, '', '' );


COMMIT;

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    NULL;
END;
