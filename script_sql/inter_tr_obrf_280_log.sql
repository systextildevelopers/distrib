CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_280_LOG" 
after insert or delete
or update of obs_cpl_3, obs_cpl_4, obs_cpl_5, credito_ressarcimento,
	cre_res_val01, cre_res_val02, cre_res_val03, cre_res_val04,
	cre_res_val05, cre_res_val06, cre_res_val07, cre_res_val08,
	cre_res_val09, cre_res_val10, cre_res_obs01, cre_res_obs02,
	cre_res_obs03, cre_res_obs04, cre_res_obs05, cre_res_obs06,
	cre_res_obs07, cre_res_obs08, cre_res_obs09, cre_res_obs10,
	deb_descr_out3, deb_descr_out9, deb_val_out5, ded_descr_out1,
	ded_descr_out6, ded_val_out2, ded_val_out8, nr_cpl_4,
	val_cpl_3, org_cpl_2, org_cpl_7, loc_cpl_6,
	cre_descr_out5, cre_descr_out6, cre_descr_out7, cre_descr_out8,
	cre_descr_out9, cre_descr_out0, cre_val_out1, cre_val_out2,
	cre_val_out3, cre_val_out4, cre_val_out5, cre_val_out6,
	cre_val_out7, cre_val_out8, cre_val_out9, cre_val_out0,
	cre_descr_est1, cre_descr_est2, cre_descr_est3, cre_descr_est4,
	cre_descr_est5, cre_descr_est6, cre_descr_est7, cre_descr_est8,
	cre_descr_est9, cre_descr_est0, cre_val_est1, cre_val_est2,
	cre_val_est3, cre_val_est4, cre_val_est5, cre_val_est6,
	cre_val_est7, cre_val_est8, cre_val_est9, cre_val_est0,
	deb_descr_out1, deb_descr_out2, deb_descr_out4, deb_descr_out5,
	deb_descr_out6, deb_descr_out7, deb_descr_out8, deb_descr_out0,
	deb_val_out1, deb_val_out2, deb_val_out3, deb_val_out4,
	deb_val_out6, deb_val_out7, deb_val_out8, deb_val_out9,
	deb_val_out0, ded_descr_out2, ded_descr_out3, ded_descr_out4,
	ded_descr_out5, ded_descr_out7, ded_descr_out8, ded_descr_out9,
	ded_descr_out0, ded_val_out1, ded_val_out3, ded_val_out4,
	ded_val_out5, ded_val_out6, ded_val_out7, ded_val_out9,
	ded_val_out0, nr_cpl_1, nr_cpl_2, nr_cpl_3,
	nr_cpl_5, nr_cpl_6, nr_cpl_7, dt_cpl_rec_1,
	dt_cpl_rec_2, dt_cpl_rec_3, dt_cpl_rec_4, dt_cpl_rec_5,
	dt_cpl_rec_6, dt_cpl_rec_7, val_cpl_1, val_cpl_2,
	val_cpl_4, val_cpl_5, val_cpl_6, val_cpl_7,
	org_cpl_1, org_cpl_3, org_cpl_4, org_cpl_5,
	org_cpl_6, dt_cpl_inf_1, dt_cpl_inf_2, dt_cpl_inf_3,
	dt_cpl_inf_4, dt_cpl_inf_5, dt_cpl_inf_6, dt_cpl_inf_7,
	loc_cpl_1, loc_cpl_2, loc_cpl_3, loc_cpl_4,
	loc_cpl_5, loc_cpl_7, obs_cpl_1, obs_cpl_2,
	cod_empresa, mes, ano, icms_ipi,
	periodo_ini, periodo_fim, num_pagina, numero_livro,
	debito_saidas, debito_outros_tot, deducoes_outros_tot, credito_estornos_tot,
	credito_entradas, credito_outros_tot, debito_estornos_tot, saldo_anterior,
	deb_descr_est1, deb_descr_est2, deb_descr_est3, deb_descr_est4,
	deb_descr_est5, deb_descr_est6, deb_descr_est7, deb_descr_est8,
	deb_descr_est9, deb_descr_est0, deb_val_est1, deb_val_est2,
	deb_val_est3, deb_val_est4, deb_val_est5, deb_val_est6,
	deb_val_est7, deb_val_est8, deb_val_est9, deb_val_est0,
	cre_descr_out1, cre_descr_out2, cre_descr_out3, cre_descr_out4
on OBRF_280
for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   v_nome_programa           varchar2(20);


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
                           
   v_nome_programa := inter_fn_nome_programa(ws_sid);  
     

 if inserting
 then
    begin

        insert into OBRF_280_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           COD_EMPRESA_OLD,   /*8*/
           COD_EMPRESA_NEW,   /*9*/
           MES_OLD,   /*10*/
           MES_NEW,   /*11*/
           ANO_OLD,   /*12*/
           ANO_NEW,   /*13*/
           ICMS_IPI_OLD,   /*14*/
           ICMS_IPI_NEW,   /*15*/
           PERIODO_INI_OLD,   /*16*/
           PERIODO_INI_NEW,   /*17*/
           PERIODO_FIM_OLD,   /*18*/
           PERIODO_FIM_NEW,   /*19*/
           NUM_PAGINA_OLD,   /*20*/
           NUM_PAGINA_NEW,   /*21*/
           NUMERO_LIVRO_OLD,   /*22*/
           NUMERO_LIVRO_NEW,   /*23*/
           DEBITO_SAIDAS_OLD,   /*24*/
           DEBITO_SAIDAS_NEW,   /*25*/
           DEBITO_OUTROS_TOT_OLD,   /*26*/
           DEBITO_OUTROS_TOT_NEW,   /*27*/
           DEDUCOES_OUTROS_TOT_OLD,   /*28*/
           DEDUCOES_OUTROS_TOT_NEW,   /*29*/
           CREDITO_ESTORNOS_TOT_OLD,   /*30*/
           CREDITO_ESTORNOS_TOT_NEW,   /*31*/
           CREDITO_ENTRADAS_OLD,   /*32*/
           CREDITO_ENTRADAS_NEW,   /*33*/
           CREDITO_OUTROS_TOT_OLD,   /*34*/
           CREDITO_OUTROS_TOT_NEW,   /*35*/
           DEBITO_ESTORNOS_TOT_OLD,   /*36*/
           DEBITO_ESTORNOS_TOT_NEW,   /*37*/
           SALDO_ANTERIOR_OLD,   /*38*/
           SALDO_ANTERIOR_NEW,   /*39*/
           DEB_DESCR_EST1_OLD,   /*40*/
           DEB_DESCR_EST1_NEW,   /*41*/
           DEB_DESCR_EST2_OLD,   /*42*/
           DEB_DESCR_EST2_NEW,   /*43*/
           DEB_DESCR_EST3_OLD,   /*44*/
           DEB_DESCR_EST3_NEW,   /*45*/
           DEB_DESCR_EST4_OLD,   /*46*/
           DEB_DESCR_EST4_NEW,   /*47*/
           DEB_DESCR_EST5_OLD,   /*48*/
           DEB_DESCR_EST5_NEW,   /*49*/
           DEB_DESCR_EST6_OLD,   /*50*/
           DEB_DESCR_EST6_NEW,   /*51*/
           DEB_DESCR_EST7_OLD,   /*52*/
           DEB_DESCR_EST7_NEW,   /*53*/
           DEB_DESCR_EST8_OLD,   /*54*/
           DEB_DESCR_EST8_NEW,   /*55*/
           DEB_DESCR_EST9_OLD,   /*56*/
           DEB_DESCR_EST9_NEW,   /*57*/
           DEB_DESCR_EST0_OLD,   /*58*/
           DEB_DESCR_EST0_NEW,   /*59*/
           DEB_VAL_EST1_OLD,   /*60*/
           DEB_VAL_EST1_NEW,   /*61*/
           DEB_VAL_EST2_OLD,   /*62*/
           DEB_VAL_EST2_NEW,   /*63*/
           DEB_VAL_EST3_OLD,   /*64*/
           DEB_VAL_EST3_NEW,   /*65*/
           DEB_VAL_EST4_OLD,   /*66*/
           DEB_VAL_EST4_NEW,   /*67*/
           DEB_VAL_EST5_OLD,   /*68*/
           DEB_VAL_EST5_NEW,   /*69*/
           DEB_VAL_EST6_OLD,   /*70*/
           DEB_VAL_EST6_NEW,   /*71*/
           DEB_VAL_EST7_OLD,   /*72*/
           DEB_VAL_EST7_NEW,   /*73*/
           DEB_VAL_EST8_OLD,   /*74*/
           DEB_VAL_EST8_NEW,   /*75*/
           DEB_VAL_EST9_OLD,   /*76*/
           DEB_VAL_EST9_NEW,   /*77*/
           DEB_VAL_EST0_OLD,   /*78*/
           DEB_VAL_EST0_NEW,   /*79*/
           CRE_DESCR_OUT1_OLD,   /*80*/
           CRE_DESCR_OUT1_NEW,   /*81*/
           CRE_DESCR_OUT2_OLD,   /*82*/
           CRE_DESCR_OUT2_NEW,   /*83*/
           CRE_DESCR_OUT3_OLD,   /*84*/
           CRE_DESCR_OUT3_NEW,   /*85*/
           CRE_DESCR_OUT4_OLD,   /*86*/
           CRE_DESCR_OUT4_NEW,   /*87*/
           CRE_DESCR_OUT5_OLD,   /*88*/
           CRE_DESCR_OUT5_NEW,   /*89*/
           CRE_DESCR_OUT6_OLD,   /*90*/
           CRE_DESCR_OUT6_NEW,   /*91*/
           CRE_DESCR_OUT7_OLD,   /*92*/
           CRE_DESCR_OUT7_NEW,   /*93*/
           CRE_DESCR_OUT8_OLD,   /*94*/
           CRE_DESCR_OUT8_NEW,   /*95*/
           CRE_DESCR_OUT9_OLD,   /*96*/
           CRE_DESCR_OUT9_NEW,   /*97*/
           CRE_DESCR_OUT0_OLD,   /*98*/
           CRE_DESCR_OUT0_NEW,   /*99*/
           CRE_VAL_OUT1_OLD,   /*100*/
           CRE_VAL_OUT1_NEW,   /*101*/
           CRE_VAL_OUT2_OLD,   /*102*/
           CRE_VAL_OUT2_NEW,   /*103*/
           CRE_VAL_OUT3_OLD,   /*104*/
           CRE_VAL_OUT3_NEW,   /*105*/
           CRE_VAL_OUT4_OLD,   /*106*/
           CRE_VAL_OUT4_NEW,   /*107*/
           CRE_VAL_OUT5_OLD,   /*108*/
           CRE_VAL_OUT5_NEW,   /*109*/
           CRE_VAL_OUT6_OLD,   /*110*/
           CRE_VAL_OUT6_NEW,   /*111*/
           CRE_VAL_OUT7_OLD,   /*112*/
           CRE_VAL_OUT7_NEW,   /*113*/
           CRE_VAL_OUT8_OLD,   /*114*/
           CRE_VAL_OUT8_NEW,   /*115*/
           CRE_VAL_OUT9_OLD,   /*116*/
           CRE_VAL_OUT9_NEW,   /*117*/
           CRE_VAL_OUT0_OLD,   /*118*/
           CRE_VAL_OUT0_NEW,   /*119*/
           CRE_DESCR_EST1_OLD,   /*120*/
           CRE_DESCR_EST1_NEW,   /*121*/
           CRE_DESCR_EST2_OLD,   /*122*/
           CRE_DESCR_EST2_NEW,   /*123*/
           CRE_DESCR_EST3_OLD,   /*124*/
           CRE_DESCR_EST3_NEW,   /*125*/
           CRE_DESCR_EST4_OLD,   /*126*/
           CRE_DESCR_EST4_NEW,   /*127*/
           CRE_DESCR_EST5_OLD,   /*128*/
           CRE_DESCR_EST5_NEW,   /*129*/
           CRE_DESCR_EST6_OLD,   /*130*/
           CRE_DESCR_EST6_NEW,   /*131*/
           CRE_DESCR_EST7_OLD,   /*132*/
           CRE_DESCR_EST7_NEW,   /*133*/
           CRE_DESCR_EST8_OLD,   /*134*/
           CRE_DESCR_EST8_NEW,   /*135*/
           CRE_DESCR_EST9_OLD,   /*136*/
           CRE_DESCR_EST9_NEW,   /*137*/
           CRE_DESCR_EST0_OLD,   /*138*/
           CRE_DESCR_EST0_NEW,   /*139*/
           CRE_VAL_EST1_OLD,   /*140*/
           CRE_VAL_EST1_NEW,   /*141*/
           CRE_VAL_EST2_OLD,   /*142*/
           CRE_VAL_EST2_NEW,   /*143*/
           CRE_VAL_EST3_OLD,   /*144*/
           CRE_VAL_EST3_NEW,   /*145*/
           CRE_VAL_EST4_OLD,   /*146*/
           CRE_VAL_EST4_NEW,   /*147*/
           CRE_VAL_EST5_OLD,   /*148*/
           CRE_VAL_EST5_NEW,   /*149*/
           CRE_VAL_EST6_OLD,   /*150*/
           CRE_VAL_EST6_NEW,   /*151*/
           CRE_VAL_EST7_OLD,   /*152*/
           CRE_VAL_EST7_NEW,   /*153*/
           CRE_VAL_EST8_OLD,   /*154*/
           CRE_VAL_EST8_NEW,   /*155*/
           CRE_VAL_EST9_OLD,   /*156*/
           CRE_VAL_EST9_NEW,   /*157*/
           CRE_VAL_EST0_OLD,   /*158*/
           CRE_VAL_EST0_NEW,   /*159*/
           DEB_DESCR_OUT1_OLD,   /*160*/
           DEB_DESCR_OUT1_NEW,   /*161*/
           DEB_DESCR_OUT2_OLD,   /*162*/
           DEB_DESCR_OUT2_NEW,   /*163*/
           DEB_DESCR_OUT3_OLD,   /*164*/
           DEB_DESCR_OUT3_NEW,   /*165*/
           DEB_DESCR_OUT4_OLD,   /*166*/
           DEB_DESCR_OUT4_NEW,   /*167*/
           DEB_DESCR_OUT5_OLD,   /*168*/
           DEB_DESCR_OUT5_NEW,   /*169*/
           DEB_DESCR_OUT6_OLD,   /*170*/
           DEB_DESCR_OUT6_NEW,   /*171*/
           DEB_DESCR_OUT7_OLD,   /*172*/
           DEB_DESCR_OUT7_NEW,   /*173*/
           DEB_DESCR_OUT8_OLD,   /*174*/
           DEB_DESCR_OUT8_NEW,   /*175*/
           DEB_DESCR_OUT9_OLD,   /*176*/
           DEB_DESCR_OUT9_NEW,   /*177*/
           DEB_DESCR_OUT0_OLD,   /*178*/
           DEB_DESCR_OUT0_NEW,   /*179*/
           DEB_VAL_OUT1_OLD,   /*180*/
           DEB_VAL_OUT1_NEW,   /*181*/
           DEB_VAL_OUT2_OLD,   /*182*/
           DEB_VAL_OUT2_NEW,   /*183*/
           DEB_VAL_OUT3_OLD,   /*184*/
           DEB_VAL_OUT3_NEW,   /*185*/
           DEB_VAL_OUT4_OLD,   /*186*/
           DEB_VAL_OUT4_NEW,   /*187*/
           DEB_VAL_OUT5_OLD,   /*188*/
           DEB_VAL_OUT5_NEW,   /*189*/
           DEB_VAL_OUT6_OLD,   /*190*/
           DEB_VAL_OUT6_NEW,   /*191*/
           DEB_VAL_OUT7_OLD,   /*192*/
           DEB_VAL_OUT7_NEW,   /*193*/
           DEB_VAL_OUT8_OLD,   /*194*/
           DEB_VAL_OUT8_NEW,   /*195*/
           DEB_VAL_OUT9_OLD,   /*196*/
           DEB_VAL_OUT9_NEW,   /*197*/
           DEB_VAL_OUT0_OLD,   /*198*/
           DEB_VAL_OUT0_NEW,   /*199*/
           DED_DESCR_OUT1_OLD,   /*200*/
           DED_DESCR_OUT1_NEW,   /*201*/
           DED_DESCR_OUT2_OLD,   /*202*/
           DED_DESCR_OUT2_NEW,   /*203*/
           DED_DESCR_OUT3_OLD,   /*204*/
           DED_DESCR_OUT3_NEW,   /*205*/
           DED_DESCR_OUT4_OLD,   /*206*/
           DED_DESCR_OUT4_NEW,   /*207*/
           DED_DESCR_OUT5_OLD,   /*208*/
           DED_DESCR_OUT5_NEW,   /*209*/
           DED_DESCR_OUT6_OLD,   /*210*/
           DED_DESCR_OUT6_NEW,   /*211*/
           DED_DESCR_OUT7_OLD,   /*212*/
           DED_DESCR_OUT7_NEW,   /*213*/
           DED_DESCR_OUT8_OLD,   /*214*/
           DED_DESCR_OUT8_NEW,   /*215*/
           DED_DESCR_OUT9_OLD,   /*216*/
           DED_DESCR_OUT9_NEW,   /*217*/
           DED_DESCR_OUT0_OLD,   /*218*/
           DED_DESCR_OUT0_NEW,   /*219*/
           DED_VAL_OUT1_OLD,   /*220*/
           DED_VAL_OUT1_NEW,   /*221*/
           DED_VAL_OUT2_OLD,   /*222*/
           DED_VAL_OUT2_NEW,   /*223*/
           DED_VAL_OUT3_OLD,   /*224*/
           DED_VAL_OUT3_NEW,   /*225*/
           DED_VAL_OUT4_OLD,   /*226*/
           DED_VAL_OUT4_NEW,   /*227*/
           DED_VAL_OUT5_OLD,   /*228*/
           DED_VAL_OUT5_NEW,   /*229*/
           DED_VAL_OUT6_OLD,   /*230*/
           DED_VAL_OUT6_NEW,   /*231*/
           DED_VAL_OUT7_OLD,   /*232*/
           DED_VAL_OUT7_NEW,   /*233*/
           DED_VAL_OUT8_OLD,   /*234*/
           DED_VAL_OUT8_NEW,   /*235*/
           DED_VAL_OUT9_OLD,   /*236*/
           DED_VAL_OUT9_NEW,   /*237*/
           DED_VAL_OUT0_OLD,   /*238*/
           DED_VAL_OUT0_NEW,   /*239*/
           NR_CPL_1_OLD,   /*240*/
           NR_CPL_1_NEW,   /*241*/
           NR_CPL_2_OLD,   /*242*/
           NR_CPL_2_NEW,   /*243*/
           NR_CPL_3_OLD,   /*244*/
           NR_CPL_3_NEW,   /*245*/
           NR_CPL_4_OLD,   /*246*/
           NR_CPL_4_NEW,   /*247*/
           NR_CPL_5_OLD,   /*248*/
           NR_CPL_5_NEW,   /*249*/
           NR_CPL_6_OLD,   /*250*/
           NR_CPL_6_NEW,   /*251*/
           NR_CPL_7_OLD,   /*252*/
           NR_CPL_7_NEW,   /*253*/
           DT_CPL_REC_1_OLD,   /*254*/
           DT_CPL_REC_1_NEW,   /*255*/
           DT_CPL_REC_2_OLD,   /*256*/
           DT_CPL_REC_2_NEW,   /*257*/
           DT_CPL_REC_3_OLD,   /*258*/
           DT_CPL_REC_3_NEW,   /*259*/
           DT_CPL_REC_4_OLD,   /*260*/
           DT_CPL_REC_4_NEW,   /*261*/
           DT_CPL_REC_5_OLD,   /*262*/
           DT_CPL_REC_5_NEW,   /*263*/
           DT_CPL_REC_6_OLD,   /*264*/
           DT_CPL_REC_6_NEW,   /*265*/
           DT_CPL_REC_7_OLD,   /*266*/
           DT_CPL_REC_7_NEW,   /*267*/
           VAL_CPL_1_OLD,   /*268*/
           VAL_CPL_1_NEW,   /*269*/
           VAL_CPL_2_OLD,   /*270*/
           VAL_CPL_2_NEW,   /*271*/
           VAL_CPL_3_OLD,   /*272*/
           VAL_CPL_3_NEW,   /*273*/
           VAL_CPL_4_OLD,   /*274*/
           VAL_CPL_4_NEW,   /*275*/
           VAL_CPL_5_OLD,   /*276*/
           VAL_CPL_5_NEW,   /*277*/
           VAL_CPL_6_OLD,   /*278*/
           VAL_CPL_6_NEW,   /*279*/
           VAL_CPL_7_OLD,   /*280*/
           VAL_CPL_7_NEW,   /*281*/
           ORG_CPL_1_OLD,   /*282*/
           ORG_CPL_1_NEW,   /*283*/
           ORG_CPL_2_OLD,   /*284*/
           ORG_CPL_2_NEW,   /*285*/
           ORG_CPL_3_OLD,   /*286*/
           ORG_CPL_3_NEW,   /*287*/
           ORG_CPL_4_OLD,   /*288*/
           ORG_CPL_4_NEW,   /*289*/
           ORG_CPL_5_OLD,   /*290*/
           ORG_CPL_5_NEW,   /*291*/
           ORG_CPL_6_OLD,   /*292*/
           ORG_CPL_6_NEW,   /*293*/
           ORG_CPL_7_OLD,   /*294*/
           ORG_CPL_7_NEW,   /*295*/
           DT_CPL_INF_1_OLD,   /*296*/
           DT_CPL_INF_1_NEW,   /*297*/
           DT_CPL_INF_2_OLD,   /*298*/
           DT_CPL_INF_2_NEW,   /*299*/
           DT_CPL_INF_3_OLD,   /*300*/
           DT_CPL_INF_3_NEW,   /*301*/
           DT_CPL_INF_4_OLD,   /*302*/
           DT_CPL_INF_4_NEW,   /*303*/
           DT_CPL_INF_5_OLD,   /*304*/
           DT_CPL_INF_5_NEW,   /*305*/
           DT_CPL_INF_6_OLD,   /*306*/
           DT_CPL_INF_6_NEW,   /*307*/
           DT_CPL_INF_7_OLD,   /*308*/
           DT_CPL_INF_7_NEW,   /*309*/
           LOC_CPL_1_OLD,   /*310*/
           LOC_CPL_1_NEW,   /*311*/
           LOC_CPL_2_OLD,   /*312*/
           LOC_CPL_2_NEW,   /*313*/
           LOC_CPL_3_OLD,   /*314*/
           LOC_CPL_3_NEW,   /*315*/
           LOC_CPL_4_OLD,   /*316*/
           LOC_CPL_4_NEW,   /*317*/
           LOC_CPL_5_OLD,   /*318*/
           LOC_CPL_5_NEW,   /*319*/
           LOC_CPL_6_OLD,   /*320*/
           LOC_CPL_6_NEW,   /*321*/
           LOC_CPL_7_OLD,   /*322*/
           LOC_CPL_7_NEW,   /*323*/
           OBS_CPL_1_OLD,   /*324*/
           OBS_CPL_1_NEW,   /*325*/
           OBS_CPL_2_OLD,   /*326*/
           OBS_CPL_2_NEW,   /*327*/
           OBS_CPL_3_OLD,   /*328*/
           OBS_CPL_3_NEW,   /*329*/
           OBS_CPL_4_OLD,   /*330*/
           OBS_CPL_4_NEW,   /*331*/
           OBS_CPL_5_OLD,   /*332*/
           OBS_CPL_5_NEW,   /*333*/
           CREDITO_RESSARCIMENTO_OLD,   /*334*/
           CREDITO_RESSARCIMENTO_NEW,   /*335*/
           CRE_RES_VAL01_OLD,   /*336*/
           CRE_RES_VAL01_NEW,   /*337*/
           CRE_RES_VAL02_OLD,   /*338*/
           CRE_RES_VAL02_NEW,   /*339*/
           CRE_RES_VAL03_OLD,   /*340*/
           CRE_RES_VAL03_NEW,   /*341*/
           CRE_RES_VAL04_OLD,   /*342*/
           CRE_RES_VAL04_NEW,   /*343*/
           CRE_RES_VAL05_OLD,   /*344*/
           CRE_RES_VAL05_NEW,   /*345*/
           CRE_RES_VAL06_OLD,   /*346*/
           CRE_RES_VAL06_NEW,   /*347*/
           CRE_RES_VAL07_OLD,   /*348*/
           CRE_RES_VAL07_NEW,   /*349*/
           CRE_RES_VAL08_OLD,   /*350*/
           CRE_RES_VAL08_NEW,   /*351*/
           CRE_RES_VAL09_OLD,   /*352*/
           CRE_RES_VAL09_NEW,   /*353*/
           CRE_RES_VAL10_OLD,   /*354*/
           CRE_RES_VAL10_NEW,   /*355*/
           CRE_RES_OBS01_OLD,   /*356*/
           CRE_RES_OBS01_NEW,   /*357*/
           CRE_RES_OBS02_OLD,   /*358*/
           CRE_RES_OBS02_NEW,   /*359*/
           CRE_RES_OBS03_OLD,   /*360*/
           CRE_RES_OBS03_NEW,   /*361*/
           CRE_RES_OBS04_OLD,   /*362*/
           CRE_RES_OBS04_NEW,   /*363*/
           CRE_RES_OBS05_OLD,   /*364*/
           CRE_RES_OBS05_NEW,   /*365*/
           CRE_RES_OBS06_OLD,   /*366*/
           CRE_RES_OBS06_NEW,   /*367*/
           CRE_RES_OBS07_OLD,   /*368*/
           CRE_RES_OBS07_NEW,   /*369*/
           CRE_RES_OBS08_OLD,   /*370*/
           CRE_RES_OBS08_NEW,   /*371*/
           CRE_RES_OBS09_OLD,   /*372*/
           CRE_RES_OBS09_NEW,   /*373*/
           CRE_RES_OBS10_OLD,   /*374*/
           CRE_RES_OBS10_NEW    /*375*/
        ) values (
            'I', /*1*/
            sysdate, /*2*/
            sysdate,/*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           0,/*8*/
           :new.COD_EMPRESA, /*9*/
           0,/*10*/
           :new.MES, /*11*/
           0,/*12*/
           :new.ANO, /*13*/
           0,/*14*/
           :new.ICMS_IPI, /*15*/
           null,/*16*/
           :new.PERIODO_INI, /*17*/
           null,/*18*/
           :new.PERIODO_FIM, /*19*/
           0,/*20*/
           :new.NUM_PAGINA, /*21*/
           0,/*22*/
           :new.NUMERO_LIVRO, /*23*/
           0,/*24*/
           :new.DEBITO_SAIDAS, /*25*/
           0,/*26*/
           :new.DEBITO_OUTROS_TOT, /*27*/
           0,/*28*/
           :new.DEDUCOES_OUTROS_TOT, /*29*/
           0,/*30*/
           :new.CREDITO_ESTORNOS_TOT, /*31*/
           0,/*32*/
           :new.CREDITO_ENTRADAS, /*33*/
           0,/*34*/
           :new.CREDITO_OUTROS_TOT, /*35*/
           0,/*36*/
           :new.DEBITO_ESTORNOS_TOT, /*37*/
           0,/*38*/
           :new.SALDO_ANTERIOR, /*39*/
           '',/*40*/
           :new.DEB_DESCR_EST1, /*41*/
           '',/*42*/
           :new.DEB_DESCR_EST2, /*43*/
           '',/*44*/
           :new.DEB_DESCR_EST3, /*45*/
           '',/*46*/
           :new.DEB_DESCR_EST4, /*47*/
           '',/*48*/
           :new.DEB_DESCR_EST5, /*49*/
           '',/*50*/
           :new.DEB_DESCR_EST6, /*51*/
           '',/*52*/
           :new.DEB_DESCR_EST7, /*53*/
           '',/*54*/
           :new.DEB_DESCR_EST8, /*55*/
           '',/*56*/
           :new.DEB_DESCR_EST9, /*57*/
           '',/*58*/
           :new.DEB_DESCR_EST0, /*59*/
           0,/*60*/
           :new.DEB_VAL_EST1, /*61*/
           0,/*62*/
           :new.DEB_VAL_EST2, /*63*/
           0,/*64*/
           :new.DEB_VAL_EST3, /*65*/
           0,/*66*/
           :new.DEB_VAL_EST4, /*67*/
           0,/*68*/
           :new.DEB_VAL_EST5, /*69*/
           0,/*70*/
           :new.DEB_VAL_EST6, /*71*/
           0,/*72*/
           :new.DEB_VAL_EST7, /*73*/
           0,/*74*/
           :new.DEB_VAL_EST8, /*75*/
           0,/*76*/
           :new.DEB_VAL_EST9, /*77*/
           0,/*78*/
           :new.DEB_VAL_EST0, /*79*/
           '',/*80*/
           :new.CRE_DESCR_OUT1, /*81*/
           '',/*82*/
           :new.CRE_DESCR_OUT2, /*83*/
           '',/*84*/
           :new.CRE_DESCR_OUT3, /*85*/
           '',/*86*/
           :new.CRE_DESCR_OUT4, /*87*/
           '',/*88*/
           :new.CRE_DESCR_OUT5, /*89*/
           '',/*90*/
           :new.CRE_DESCR_OUT6, /*91*/
           '',/*92*/
           :new.CRE_DESCR_OUT7, /*93*/
           '',/*94*/
           :new.CRE_DESCR_OUT8, /*95*/
           '',/*96*/
           :new.CRE_DESCR_OUT9, /*97*/
           '',/*98*/
           :new.CRE_DESCR_OUT0, /*99*/
           0,/*100*/
           :new.CRE_VAL_OUT1, /*101*/
           0,/*102*/
           :new.CRE_VAL_OUT2, /*103*/
           0,/*104*/
           :new.CRE_VAL_OUT3, /*105*/
           0,/*106*/
           :new.CRE_VAL_OUT4, /*107*/
           0,/*108*/
           :new.CRE_VAL_OUT5, /*109*/
           0,/*110*/
           :new.CRE_VAL_OUT6, /*111*/
           0,/*112*/
           :new.CRE_VAL_OUT7, /*113*/
           0,/*114*/
           :new.CRE_VAL_OUT8, /*115*/
           0,/*116*/
           :new.CRE_VAL_OUT9, /*117*/
           0,/*118*/
           :new.CRE_VAL_OUT0, /*119*/
           '',/*120*/
           :new.CRE_DESCR_EST1, /*121*/
           '',/*122*/
           :new.CRE_DESCR_EST2, /*123*/
           '',/*124*/
           :new.CRE_DESCR_EST3, /*125*/
           '',/*126*/
           :new.CRE_DESCR_EST4, /*127*/
           '',/*128*/
           :new.CRE_DESCR_EST5, /*129*/
           '',/*130*/
           :new.CRE_DESCR_EST6, /*131*/
           '',/*132*/
           :new.CRE_DESCR_EST7, /*133*/
           '',/*134*/
           :new.CRE_DESCR_EST8, /*135*/
           '',/*136*/
           :new.CRE_DESCR_EST9, /*137*/
           '',/*138*/
           :new.CRE_DESCR_EST0, /*139*/
           0,/*140*/
           :new.CRE_VAL_EST1, /*141*/
           0,/*142*/
           :new.CRE_VAL_EST2, /*143*/
           0,/*144*/
           :new.CRE_VAL_EST3, /*145*/
           0,/*146*/
           :new.CRE_VAL_EST4, /*147*/
           0,/*148*/
           :new.CRE_VAL_EST5, /*149*/
           0,/*150*/
           :new.CRE_VAL_EST6, /*151*/
           0,/*152*/
           :new.CRE_VAL_EST7, /*153*/
           0,/*154*/
           :new.CRE_VAL_EST8, /*155*/
           0,/*156*/
           :new.CRE_VAL_EST9, /*157*/
           0,/*158*/
           :new.CRE_VAL_EST0, /*159*/
           '',/*160*/
           :new.DEB_DESCR_OUT1, /*161*/
           '',/*162*/
           :new.DEB_DESCR_OUT2, /*163*/
           '',/*164*/
           :new.DEB_DESCR_OUT3, /*165*/
           '',/*166*/
           :new.DEB_DESCR_OUT4, /*167*/
           '',/*168*/
           :new.DEB_DESCR_OUT5, /*169*/
           '',/*170*/
           :new.DEB_DESCR_OUT6, /*171*/
           '',/*172*/
           :new.DEB_DESCR_OUT7, /*173*/
           '',/*174*/
           :new.DEB_DESCR_OUT8, /*175*/
           '',/*176*/
           :new.DEB_DESCR_OUT9, /*177*/
           '',/*178*/
           :new.DEB_DESCR_OUT0, /*179*/
           0,/*180*/
           :new.DEB_VAL_OUT1, /*181*/
           0,/*182*/
           :new.DEB_VAL_OUT2, /*183*/
           0,/*184*/
           :new.DEB_VAL_OUT3, /*185*/
           0,/*186*/
           :new.DEB_VAL_OUT4, /*187*/
           0,/*188*/
           :new.DEB_VAL_OUT5, /*189*/
           0,/*190*/
           :new.DEB_VAL_OUT6, /*191*/
           0,/*192*/
           :new.DEB_VAL_OUT7, /*193*/
           0,/*194*/
           :new.DEB_VAL_OUT8, /*195*/
           0,/*196*/
           :new.DEB_VAL_OUT9, /*197*/
           0,/*198*/
           :new.DEB_VAL_OUT0, /*199*/
           '',/*200*/
           :new.DED_DESCR_OUT1, /*201*/
           '',/*202*/
           :new.DED_DESCR_OUT2, /*203*/
           '',/*204*/
           :new.DED_DESCR_OUT3, /*205*/
           '',/*206*/
           :new.DED_DESCR_OUT4, /*207*/
           '',/*208*/
           :new.DED_DESCR_OUT5, /*209*/
           '',/*210*/
           :new.DED_DESCR_OUT6, /*211*/
           '',/*212*/
           :new.DED_DESCR_OUT7, /*213*/
           '',/*214*/
           :new.DED_DESCR_OUT8, /*215*/
           '',/*216*/
           :new.DED_DESCR_OUT9, /*217*/
           '',/*218*/
           :new.DED_DESCR_OUT0, /*219*/
           0,/*220*/
           :new.DED_VAL_OUT1, /*221*/
           0,/*222*/
           :new.DED_VAL_OUT2, /*223*/
           0,/*224*/
           :new.DED_VAL_OUT3, /*225*/
           0,/*226*/
           :new.DED_VAL_OUT4, /*227*/
           0,/*228*/
           :new.DED_VAL_OUT5, /*229*/
           0,/*230*/
           :new.DED_VAL_OUT6, /*231*/
           0,/*232*/
           :new.DED_VAL_OUT7, /*233*/
           0,/*234*/
           :new.DED_VAL_OUT8, /*235*/
           0,/*236*/
           :new.DED_VAL_OUT9, /*237*/
           0,/*238*/
           :new.DED_VAL_OUT0, /*239*/
           0,/*240*/
           :new.NR_CPL_1, /*241*/
           0,/*242*/
           :new.NR_CPL_2, /*243*/
           0,/*244*/
           :new.NR_CPL_3, /*245*/
           0,/*246*/
           :new.NR_CPL_4, /*247*/
           0,/*248*/
           :new.NR_CPL_5, /*249*/
           0,/*250*/
           :new.NR_CPL_6, /*251*/
           0,/*252*/
           :new.NR_CPL_7, /*253*/
           null,/*254*/
           :new.DT_CPL_REC_1, /*255*/
           null,/*256*/
           :new.DT_CPL_REC_2, /*257*/
           null,/*258*/
           :new.DT_CPL_REC_3, /*259*/
           null,/*260*/
           :new.DT_CPL_REC_4, /*261*/
           null,/*262*/
           :new.DT_CPL_REC_5, /*263*/
           null,/*264*/
           :new.DT_CPL_REC_6, /*265*/
           null,/*266*/
           :new.DT_CPL_REC_7, /*267*/
           0,/*268*/
           :new.VAL_CPL_1, /*269*/
           0,/*270*/
           :new.VAL_CPL_2, /*271*/
           0,/*272*/
           :new.VAL_CPL_3, /*273*/
           0,/*274*/
           :new.VAL_CPL_4, /*275*/
           0,/*276*/
           :new.VAL_CPL_5, /*277*/
           0,/*278*/
           :new.VAL_CPL_6, /*279*/
           0,/*280*/
           :new.VAL_CPL_7, /*281*/
           '',/*282*/
           :new.ORG_CPL_1, /*283*/
           '',/*284*/
           :new.ORG_CPL_2, /*285*/
           '',/*286*/
           :new.ORG_CPL_3, /*287*/
           '',/*288*/
           :new.ORG_CPL_4, /*289*/
           '',/*290*/
           :new.ORG_CPL_5, /*291*/
           '',/*292*/
           :new.ORG_CPL_6, /*293*/
           '',/*294*/
           :new.ORG_CPL_7, /*295*/
           null,/*296*/
           :new.DT_CPL_INF_1, /*297*/
           null,/*298*/
           :new.DT_CPL_INF_2, /*299*/
           null,/*300*/
           :new.DT_CPL_INF_3, /*301*/
           null,/*302*/
           :new.DT_CPL_INF_4, /*303*/
           null,/*304*/
           :new.DT_CPL_INF_5, /*305*/
           null,/*306*/
           :new.DT_CPL_INF_6, /*307*/
           null,/*308*/
           :new.DT_CPL_INF_7, /*309*/
           '',/*310*/
           :new.LOC_CPL_1, /*311*/
           '',/*312*/
           :new.LOC_CPL_2, /*313*/
           '',/*314*/
           :new.LOC_CPL_3, /*315*/
           '',/*316*/
           :new.LOC_CPL_4, /*317*/
           '',/*318*/
           :new.LOC_CPL_5, /*319*/
           '',/*320*/
           :new.LOC_CPL_6, /*321*/
           '',/*322*/
           :new.LOC_CPL_7, /*323*/
           '',/*324*/
           :new.OBS_CPL_1, /*325*/
           '',/*326*/
           :new.OBS_CPL_2, /*327*/
           '',/*328*/
           :new.OBS_CPL_3, /*329*/
           '',/*330*/
           :new.OBS_CPL_4, /*331*/
           '',/*332*/
           :new.OBS_CPL_5, /*333*/
           0,/*334*/
           :new.CREDITO_RESSARCIMENTO, /*335*/
           0,/*336*/
           :new.CRE_RES_VAL01, /*337*/
           0,/*338*/
           :new.CRE_RES_VAL02, /*339*/
           0,/*340*/
           :new.CRE_RES_VAL03, /*341*/
           0,/*342*/
           :new.CRE_RES_VAL04, /*343*/
           0,/*344*/
           :new.CRE_RES_VAL05, /*345*/
           0,/*346*/
           :new.CRE_RES_VAL06, /*347*/
           0,/*348*/
           :new.CRE_RES_VAL07, /*349*/
           0,/*350*/
           :new.CRE_RES_VAL08, /*351*/
           0,/*352*/
           :new.CRE_RES_VAL09, /*353*/
           0,/*354*/
           :new.CRE_RES_VAL10, /*355*/
           '',/*356*/
           :new.CRE_RES_OBS01, /*357*/
           '',/*358*/
           :new.CRE_RES_OBS02, /*359*/
           '',/*360*/
           :new.CRE_RES_OBS03, /*361*/
           '',/*362*/
           :new.CRE_RES_OBS04, /*363*/
           '',/*364*/
           :new.CRE_RES_OBS05, /*365*/
           '',/*366*/
           :new.CRE_RES_OBS06, /*367*/
           '',/*368*/
           :new.CRE_RES_OBS07, /*369*/
           '',/*370*/
           :new.CRE_RES_OBS08, /*371*/
           '',/*372*/
           :new.CRE_RES_OBS09, /*373*/
           '',/*374*/
           :new.CRE_RES_OBS10 /*375*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into OBRF_280_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_EMPRESA_OLD, /*8*/
           COD_EMPRESA_NEW, /*9*/
           MES_OLD, /*10*/
           MES_NEW, /*11*/
           ANO_OLD, /*12*/
           ANO_NEW, /*13*/
           ICMS_IPI_OLD, /*14*/
           ICMS_IPI_NEW, /*15*/
           PERIODO_INI_OLD, /*16*/
           PERIODO_INI_NEW, /*17*/
           PERIODO_FIM_OLD, /*18*/
           PERIODO_FIM_NEW, /*19*/
           NUM_PAGINA_OLD, /*20*/
           NUM_PAGINA_NEW, /*21*/
           NUMERO_LIVRO_OLD, /*22*/
           NUMERO_LIVRO_NEW, /*23*/
           DEBITO_SAIDAS_OLD, /*24*/
           DEBITO_SAIDAS_NEW, /*25*/
           DEBITO_OUTROS_TOT_OLD, /*26*/
           DEBITO_OUTROS_TOT_NEW, /*27*/
           DEDUCOES_OUTROS_TOT_OLD, /*28*/
           DEDUCOES_OUTROS_TOT_NEW, /*29*/
           CREDITO_ESTORNOS_TOT_OLD, /*30*/
           CREDITO_ESTORNOS_TOT_NEW, /*31*/
           CREDITO_ENTRADAS_OLD, /*32*/
           CREDITO_ENTRADAS_NEW, /*33*/
           CREDITO_OUTROS_TOT_OLD, /*34*/
           CREDITO_OUTROS_TOT_NEW, /*35*/
           DEBITO_ESTORNOS_TOT_OLD, /*36*/
           DEBITO_ESTORNOS_TOT_NEW, /*37*/
           SALDO_ANTERIOR_OLD, /*38*/
           SALDO_ANTERIOR_NEW, /*39*/
           DEB_DESCR_EST1_OLD, /*40*/
           DEB_DESCR_EST1_NEW, /*41*/
           DEB_DESCR_EST2_OLD, /*42*/
           DEB_DESCR_EST2_NEW, /*43*/
           DEB_DESCR_EST3_OLD, /*44*/
           DEB_DESCR_EST3_NEW, /*45*/
           DEB_DESCR_EST4_OLD, /*46*/
           DEB_DESCR_EST4_NEW, /*47*/
           DEB_DESCR_EST5_OLD, /*48*/
           DEB_DESCR_EST5_NEW, /*49*/
           DEB_DESCR_EST6_OLD, /*50*/
           DEB_DESCR_EST6_NEW, /*51*/
           DEB_DESCR_EST7_OLD, /*52*/
           DEB_DESCR_EST7_NEW, /*53*/
           DEB_DESCR_EST8_OLD, /*54*/
           DEB_DESCR_EST8_NEW, /*55*/
           DEB_DESCR_EST9_OLD, /*56*/
           DEB_DESCR_EST9_NEW, /*57*/
           DEB_DESCR_EST0_OLD, /*58*/
           DEB_DESCR_EST0_NEW, /*59*/
           DEB_VAL_EST1_OLD, /*60*/
           DEB_VAL_EST1_NEW, /*61*/
           DEB_VAL_EST2_OLD, /*62*/
           DEB_VAL_EST2_NEW, /*63*/
           DEB_VAL_EST3_OLD, /*64*/
           DEB_VAL_EST3_NEW, /*65*/
           DEB_VAL_EST4_OLD, /*66*/
           DEB_VAL_EST4_NEW, /*67*/
           DEB_VAL_EST5_OLD, /*68*/
           DEB_VAL_EST5_NEW, /*69*/
           DEB_VAL_EST6_OLD, /*70*/
           DEB_VAL_EST6_NEW, /*71*/
           DEB_VAL_EST7_OLD, /*72*/
           DEB_VAL_EST7_NEW, /*73*/
           DEB_VAL_EST8_OLD, /*74*/
           DEB_VAL_EST8_NEW, /*75*/
           DEB_VAL_EST9_OLD, /*76*/
           DEB_VAL_EST9_NEW, /*77*/
           DEB_VAL_EST0_OLD, /*78*/
           DEB_VAL_EST0_NEW, /*79*/
           CRE_DESCR_OUT1_OLD, /*80*/
           CRE_DESCR_OUT1_NEW, /*81*/
           CRE_DESCR_OUT2_OLD, /*82*/
           CRE_DESCR_OUT2_NEW, /*83*/
           CRE_DESCR_OUT3_OLD, /*84*/
           CRE_DESCR_OUT3_NEW, /*85*/
           CRE_DESCR_OUT4_OLD, /*86*/
           CRE_DESCR_OUT4_NEW, /*87*/
           CRE_DESCR_OUT5_OLD, /*88*/
           CRE_DESCR_OUT5_NEW, /*89*/
           CRE_DESCR_OUT6_OLD, /*90*/
           CRE_DESCR_OUT6_NEW, /*91*/
           CRE_DESCR_OUT7_OLD, /*92*/
           CRE_DESCR_OUT7_NEW, /*93*/
           CRE_DESCR_OUT8_OLD, /*94*/
           CRE_DESCR_OUT8_NEW, /*95*/
           CRE_DESCR_OUT9_OLD, /*96*/
           CRE_DESCR_OUT9_NEW, /*97*/
           CRE_DESCR_OUT0_OLD, /*98*/
           CRE_DESCR_OUT0_NEW, /*99*/
           CRE_VAL_OUT1_OLD, /*100*/
           CRE_VAL_OUT1_NEW, /*101*/
           CRE_VAL_OUT2_OLD, /*102*/
           CRE_VAL_OUT2_NEW, /*103*/
           CRE_VAL_OUT3_OLD, /*104*/
           CRE_VAL_OUT3_NEW, /*105*/
           CRE_VAL_OUT4_OLD, /*106*/
           CRE_VAL_OUT4_NEW, /*107*/
           CRE_VAL_OUT5_OLD, /*108*/
           CRE_VAL_OUT5_NEW, /*109*/
           CRE_VAL_OUT6_OLD, /*110*/
           CRE_VAL_OUT6_NEW, /*111*/
           CRE_VAL_OUT7_OLD, /*112*/
           CRE_VAL_OUT7_NEW, /*113*/
           CRE_VAL_OUT8_OLD, /*114*/
           CRE_VAL_OUT8_NEW, /*115*/
           CRE_VAL_OUT9_OLD, /*116*/
           CRE_VAL_OUT9_NEW, /*117*/
           CRE_VAL_OUT0_OLD, /*118*/
           CRE_VAL_OUT0_NEW, /*119*/
           CRE_DESCR_EST1_OLD, /*120*/
           CRE_DESCR_EST1_NEW, /*121*/
           CRE_DESCR_EST2_OLD, /*122*/
           CRE_DESCR_EST2_NEW, /*123*/
           CRE_DESCR_EST3_OLD, /*124*/
           CRE_DESCR_EST3_NEW, /*125*/
           CRE_DESCR_EST4_OLD, /*126*/
           CRE_DESCR_EST4_NEW, /*127*/
           CRE_DESCR_EST5_OLD, /*128*/
           CRE_DESCR_EST5_NEW, /*129*/
           CRE_DESCR_EST6_OLD, /*130*/
           CRE_DESCR_EST6_NEW, /*131*/
           CRE_DESCR_EST7_OLD, /*132*/
           CRE_DESCR_EST7_NEW, /*133*/
           CRE_DESCR_EST8_OLD, /*134*/
           CRE_DESCR_EST8_NEW, /*135*/
           CRE_DESCR_EST9_OLD, /*136*/
           CRE_DESCR_EST9_NEW, /*137*/
           CRE_DESCR_EST0_OLD, /*138*/
           CRE_DESCR_EST0_NEW, /*139*/
           CRE_VAL_EST1_OLD, /*140*/
           CRE_VAL_EST1_NEW, /*141*/
           CRE_VAL_EST2_OLD, /*142*/
           CRE_VAL_EST2_NEW, /*143*/
           CRE_VAL_EST3_OLD, /*144*/
           CRE_VAL_EST3_NEW, /*145*/
           CRE_VAL_EST4_OLD, /*146*/
           CRE_VAL_EST4_NEW, /*147*/
           CRE_VAL_EST5_OLD, /*148*/
           CRE_VAL_EST5_NEW, /*149*/
           CRE_VAL_EST6_OLD, /*150*/
           CRE_VAL_EST6_NEW, /*151*/
           CRE_VAL_EST7_OLD, /*152*/
           CRE_VAL_EST7_NEW, /*153*/
           CRE_VAL_EST8_OLD, /*154*/
           CRE_VAL_EST8_NEW, /*155*/
           CRE_VAL_EST9_OLD, /*156*/
           CRE_VAL_EST9_NEW, /*157*/
           CRE_VAL_EST0_OLD, /*158*/
           CRE_VAL_EST0_NEW, /*159*/
           DEB_DESCR_OUT1_OLD, /*160*/
           DEB_DESCR_OUT1_NEW, /*161*/
           DEB_DESCR_OUT2_OLD, /*162*/
           DEB_DESCR_OUT2_NEW, /*163*/
           DEB_DESCR_OUT3_OLD, /*164*/
           DEB_DESCR_OUT3_NEW, /*165*/
           DEB_DESCR_OUT4_OLD, /*166*/
           DEB_DESCR_OUT4_NEW, /*167*/
           DEB_DESCR_OUT5_OLD, /*168*/
           DEB_DESCR_OUT5_NEW, /*169*/
           DEB_DESCR_OUT6_OLD, /*170*/
           DEB_DESCR_OUT6_NEW, /*171*/
           DEB_DESCR_OUT7_OLD, /*172*/
           DEB_DESCR_OUT7_NEW, /*173*/
           DEB_DESCR_OUT8_OLD, /*174*/
           DEB_DESCR_OUT8_NEW, /*175*/
           DEB_DESCR_OUT9_OLD, /*176*/
           DEB_DESCR_OUT9_NEW, /*177*/
           DEB_DESCR_OUT0_OLD, /*178*/
           DEB_DESCR_OUT0_NEW, /*179*/
           DEB_VAL_OUT1_OLD, /*180*/
           DEB_VAL_OUT1_NEW, /*181*/
           DEB_VAL_OUT2_OLD, /*182*/
           DEB_VAL_OUT2_NEW, /*183*/
           DEB_VAL_OUT3_OLD, /*184*/
           DEB_VAL_OUT3_NEW, /*185*/
           DEB_VAL_OUT4_OLD, /*186*/
           DEB_VAL_OUT4_NEW, /*187*/
           DEB_VAL_OUT5_OLD, /*188*/
           DEB_VAL_OUT5_NEW, /*189*/
           DEB_VAL_OUT6_OLD, /*190*/
           DEB_VAL_OUT6_NEW, /*191*/
           DEB_VAL_OUT7_OLD, /*192*/
           DEB_VAL_OUT7_NEW, /*193*/
           DEB_VAL_OUT8_OLD, /*194*/
           DEB_VAL_OUT8_NEW, /*195*/
           DEB_VAL_OUT9_OLD, /*196*/
           DEB_VAL_OUT9_NEW, /*197*/
           DEB_VAL_OUT0_OLD, /*198*/
           DEB_VAL_OUT0_NEW, /*199*/
           DED_DESCR_OUT1_OLD, /*200*/
           DED_DESCR_OUT1_NEW, /*201*/
           DED_DESCR_OUT2_OLD, /*202*/
           DED_DESCR_OUT2_NEW, /*203*/
           DED_DESCR_OUT3_OLD, /*204*/
           DED_DESCR_OUT3_NEW, /*205*/
           DED_DESCR_OUT4_OLD, /*206*/
           DED_DESCR_OUT4_NEW, /*207*/
           DED_DESCR_OUT5_OLD, /*208*/
           DED_DESCR_OUT5_NEW, /*209*/
           DED_DESCR_OUT6_OLD, /*210*/
           DED_DESCR_OUT6_NEW, /*211*/
           DED_DESCR_OUT7_OLD, /*212*/
           DED_DESCR_OUT7_NEW, /*213*/
           DED_DESCR_OUT8_OLD, /*214*/
           DED_DESCR_OUT8_NEW, /*215*/
           DED_DESCR_OUT9_OLD, /*216*/
           DED_DESCR_OUT9_NEW, /*217*/
           DED_DESCR_OUT0_OLD, /*218*/
           DED_DESCR_OUT0_NEW, /*219*/
           DED_VAL_OUT1_OLD, /*220*/
           DED_VAL_OUT1_NEW, /*221*/
           DED_VAL_OUT2_OLD, /*222*/
           DED_VAL_OUT2_NEW, /*223*/
           DED_VAL_OUT3_OLD, /*224*/
           DED_VAL_OUT3_NEW, /*225*/
           DED_VAL_OUT4_OLD, /*226*/
           DED_VAL_OUT4_NEW, /*227*/
           DED_VAL_OUT5_OLD, /*228*/
           DED_VAL_OUT5_NEW, /*229*/
           DED_VAL_OUT6_OLD, /*230*/
           DED_VAL_OUT6_NEW, /*231*/
           DED_VAL_OUT7_OLD, /*232*/
           DED_VAL_OUT7_NEW, /*233*/
           DED_VAL_OUT8_OLD, /*234*/
           DED_VAL_OUT8_NEW, /*235*/
           DED_VAL_OUT9_OLD, /*236*/
           DED_VAL_OUT9_NEW, /*237*/
           DED_VAL_OUT0_OLD, /*238*/
           DED_VAL_OUT0_NEW, /*239*/
           NR_CPL_1_OLD, /*240*/
           NR_CPL_1_NEW, /*241*/
           NR_CPL_2_OLD, /*242*/
           NR_CPL_2_NEW, /*243*/
           NR_CPL_3_OLD, /*244*/
           NR_CPL_3_NEW, /*245*/
           NR_CPL_4_OLD, /*246*/
           NR_CPL_4_NEW, /*247*/
           NR_CPL_5_OLD, /*248*/
           NR_CPL_5_NEW, /*249*/
           NR_CPL_6_OLD, /*250*/
           NR_CPL_6_NEW, /*251*/
           NR_CPL_7_OLD, /*252*/
           NR_CPL_7_NEW, /*253*/
           DT_CPL_REC_1_OLD, /*254*/
           DT_CPL_REC_1_NEW, /*255*/
           DT_CPL_REC_2_OLD, /*256*/
           DT_CPL_REC_2_NEW, /*257*/
           DT_CPL_REC_3_OLD, /*258*/
           DT_CPL_REC_3_NEW, /*259*/
           DT_CPL_REC_4_OLD, /*260*/
           DT_CPL_REC_4_NEW, /*261*/
           DT_CPL_REC_5_OLD, /*262*/
           DT_CPL_REC_5_NEW, /*263*/
           DT_CPL_REC_6_OLD, /*264*/
           DT_CPL_REC_6_NEW, /*265*/
           DT_CPL_REC_7_OLD, /*266*/
           DT_CPL_REC_7_NEW, /*267*/
           VAL_CPL_1_OLD, /*268*/
           VAL_CPL_1_NEW, /*269*/
           VAL_CPL_2_OLD, /*270*/
           VAL_CPL_2_NEW, /*271*/
           VAL_CPL_3_OLD, /*272*/
           VAL_CPL_3_NEW, /*273*/
           VAL_CPL_4_OLD, /*274*/
           VAL_CPL_4_NEW, /*275*/
           VAL_CPL_5_OLD, /*276*/
           VAL_CPL_5_NEW, /*277*/
           VAL_CPL_6_OLD, /*278*/
           VAL_CPL_6_NEW, /*279*/
           VAL_CPL_7_OLD, /*280*/
           VAL_CPL_7_NEW, /*281*/
           ORG_CPL_1_OLD, /*282*/
           ORG_CPL_1_NEW, /*283*/
           ORG_CPL_2_OLD, /*284*/
           ORG_CPL_2_NEW, /*285*/
           ORG_CPL_3_OLD, /*286*/
           ORG_CPL_3_NEW, /*287*/
           ORG_CPL_4_OLD, /*288*/
           ORG_CPL_4_NEW, /*289*/
           ORG_CPL_5_OLD, /*290*/
           ORG_CPL_5_NEW, /*291*/
           ORG_CPL_6_OLD, /*292*/
           ORG_CPL_6_NEW, /*293*/
           ORG_CPL_7_OLD, /*294*/
           ORG_CPL_7_NEW, /*295*/
           DT_CPL_INF_1_OLD, /*296*/
           DT_CPL_INF_1_NEW, /*297*/
           DT_CPL_INF_2_OLD, /*298*/
           DT_CPL_INF_2_NEW, /*299*/
           DT_CPL_INF_3_OLD, /*300*/
           DT_CPL_INF_3_NEW, /*301*/
           DT_CPL_INF_4_OLD, /*302*/
           DT_CPL_INF_4_NEW, /*303*/
           DT_CPL_INF_5_OLD, /*304*/
           DT_CPL_INF_5_NEW, /*305*/
           DT_CPL_INF_6_OLD, /*306*/
           DT_CPL_INF_6_NEW, /*307*/
           DT_CPL_INF_7_OLD, /*308*/
           DT_CPL_INF_7_NEW, /*309*/
           LOC_CPL_1_OLD, /*310*/
           LOC_CPL_1_NEW, /*311*/
           LOC_CPL_2_OLD, /*312*/
           LOC_CPL_2_NEW, /*313*/
           LOC_CPL_3_OLD, /*314*/
           LOC_CPL_3_NEW, /*315*/
           LOC_CPL_4_OLD, /*316*/
           LOC_CPL_4_NEW, /*317*/
           LOC_CPL_5_OLD, /*318*/
           LOC_CPL_5_NEW, /*319*/
           LOC_CPL_6_OLD, /*320*/
           LOC_CPL_6_NEW, /*321*/
           LOC_CPL_7_OLD, /*322*/
           LOC_CPL_7_NEW, /*323*/
           OBS_CPL_1_OLD, /*324*/
           OBS_CPL_1_NEW, /*325*/
           OBS_CPL_2_OLD, /*326*/
           OBS_CPL_2_NEW, /*327*/
           OBS_CPL_3_OLD, /*328*/
           OBS_CPL_3_NEW, /*329*/
           OBS_CPL_4_OLD, /*330*/
           OBS_CPL_4_NEW, /*331*/
           OBS_CPL_5_OLD, /*332*/
           OBS_CPL_5_NEW, /*333*/
           CREDITO_RESSARCIMENTO_OLD, /*334*/
           CREDITO_RESSARCIMENTO_NEW, /*335*/
           CRE_RES_VAL01_OLD, /*336*/
           CRE_RES_VAL01_NEW, /*337*/
           CRE_RES_VAL02_OLD, /*338*/
           CRE_RES_VAL02_NEW, /*339*/
           CRE_RES_VAL03_OLD, /*340*/
           CRE_RES_VAL03_NEW, /*341*/
           CRE_RES_VAL04_OLD, /*342*/
           CRE_RES_VAL04_NEW, /*343*/
           CRE_RES_VAL05_OLD, /*344*/
           CRE_RES_VAL05_NEW, /*345*/
           CRE_RES_VAL06_OLD, /*346*/
           CRE_RES_VAL06_NEW, /*347*/
           CRE_RES_VAL07_OLD, /*348*/
           CRE_RES_VAL07_NEW, /*349*/
           CRE_RES_VAL08_OLD, /*350*/
           CRE_RES_VAL08_NEW, /*351*/
           CRE_RES_VAL09_OLD, /*352*/
           CRE_RES_VAL09_NEW, /*353*/
           CRE_RES_VAL10_OLD, /*354*/
           CRE_RES_VAL10_NEW, /*355*/
           CRE_RES_OBS01_OLD, /*356*/
           CRE_RES_OBS01_NEW, /*357*/
           CRE_RES_OBS02_OLD, /*358*/
           CRE_RES_OBS02_NEW, /*359*/
           CRE_RES_OBS03_OLD, /*360*/
           CRE_RES_OBS03_NEW, /*361*/
           CRE_RES_OBS04_OLD, /*362*/
           CRE_RES_OBS04_NEW, /*363*/
           CRE_RES_OBS05_OLD, /*364*/
           CRE_RES_OBS05_NEW, /*365*/
           CRE_RES_OBS06_OLD, /*366*/
           CRE_RES_OBS06_NEW, /*367*/
           CRE_RES_OBS07_OLD, /*368*/
           CRE_RES_OBS07_NEW, /*369*/
           CRE_RES_OBS08_OLD, /*370*/
           CRE_RES_OBS08_NEW, /*371*/
           CRE_RES_OBS09_OLD, /*372*/
           CRE_RES_OBS09_NEW, /*373*/
           CRE_RES_OBS10_OLD, /*374*/
           CRE_RES_OBS10_NEW  /*375*/
        ) values (
            'A', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.COD_EMPRESA,  /*8*/
           :new.COD_EMPRESA, /*9*/
           :old.MES,  /*10*/
           :new.MES, /*11*/
           :old.ANO,  /*12*/
           :new.ANO, /*13*/
           :old.ICMS_IPI,  /*14*/
           :new.ICMS_IPI, /*15*/
           :old.PERIODO_INI,  /*16*/
           :new.PERIODO_INI, /*17*/
           :old.PERIODO_FIM,  /*18*/
           :new.PERIODO_FIM, /*19*/
           :old.NUM_PAGINA,  /*20*/
           :new.NUM_PAGINA, /*21*/
           :old.NUMERO_LIVRO,  /*22*/
           :new.NUMERO_LIVRO, /*23*/
           :old.DEBITO_SAIDAS,  /*24*/
           :new.DEBITO_SAIDAS, /*25*/
           :old.DEBITO_OUTROS_TOT,  /*26*/
           :new.DEBITO_OUTROS_TOT, /*27*/
           :old.DEDUCOES_OUTROS_TOT,  /*28*/
           :new.DEDUCOES_OUTROS_TOT, /*29*/
           :old.CREDITO_ESTORNOS_TOT,  /*30*/
           :new.CREDITO_ESTORNOS_TOT, /*31*/
           :old.CREDITO_ENTRADAS,  /*32*/
           :new.CREDITO_ENTRADAS, /*33*/
           :old.CREDITO_OUTROS_TOT,  /*34*/
           :new.CREDITO_OUTROS_TOT, /*35*/
           :old.DEBITO_ESTORNOS_TOT,  /*36*/
           :new.DEBITO_ESTORNOS_TOT, /*37*/
           :old.SALDO_ANTERIOR,  /*38*/
           :new.SALDO_ANTERIOR, /*39*/
           :old.DEB_DESCR_EST1,  /*40*/
           :new.DEB_DESCR_EST1, /*41*/
           :old.DEB_DESCR_EST2,  /*42*/
           :new.DEB_DESCR_EST2, /*43*/
           :old.DEB_DESCR_EST3,  /*44*/
           :new.DEB_DESCR_EST3, /*45*/
           :old.DEB_DESCR_EST4,  /*46*/
           :new.DEB_DESCR_EST4, /*47*/
           :old.DEB_DESCR_EST5,  /*48*/
           :new.DEB_DESCR_EST5, /*49*/
           :old.DEB_DESCR_EST6,  /*50*/
           :new.DEB_DESCR_EST6, /*51*/
           :old.DEB_DESCR_EST7,  /*52*/
           :new.DEB_DESCR_EST7, /*53*/
           :old.DEB_DESCR_EST8,  /*54*/
           :new.DEB_DESCR_EST8, /*55*/
           :old.DEB_DESCR_EST9,  /*56*/
           :new.DEB_DESCR_EST9, /*57*/
           :old.DEB_DESCR_EST0,  /*58*/
           :new.DEB_DESCR_EST0, /*59*/
           :old.DEB_VAL_EST1,  /*60*/
           :new.DEB_VAL_EST1, /*61*/
           :old.DEB_VAL_EST2,  /*62*/
           :new.DEB_VAL_EST2, /*63*/
           :old.DEB_VAL_EST3,  /*64*/
           :new.DEB_VAL_EST3, /*65*/
           :old.DEB_VAL_EST4,  /*66*/
           :new.DEB_VAL_EST4, /*67*/
           :old.DEB_VAL_EST5,  /*68*/
           :new.DEB_VAL_EST5, /*69*/
           :old.DEB_VAL_EST6,  /*70*/
           :new.DEB_VAL_EST6, /*71*/
           :old.DEB_VAL_EST7,  /*72*/
           :new.DEB_VAL_EST7, /*73*/
           :old.DEB_VAL_EST8,  /*74*/
           :new.DEB_VAL_EST8, /*75*/
           :old.DEB_VAL_EST9,  /*76*/
           :new.DEB_VAL_EST9, /*77*/
           :old.DEB_VAL_EST0,  /*78*/
           :new.DEB_VAL_EST0, /*79*/
           :old.CRE_DESCR_OUT1,  /*80*/
           :new.CRE_DESCR_OUT1, /*81*/
           :old.CRE_DESCR_OUT2,  /*82*/
           :new.CRE_DESCR_OUT2, /*83*/
           :old.CRE_DESCR_OUT3,  /*84*/
           :new.CRE_DESCR_OUT3, /*85*/
           :old.CRE_DESCR_OUT4,  /*86*/
           :new.CRE_DESCR_OUT4, /*87*/
           :old.CRE_DESCR_OUT5,  /*88*/
           :new.CRE_DESCR_OUT5, /*89*/
           :old.CRE_DESCR_OUT6,  /*90*/
           :new.CRE_DESCR_OUT6, /*91*/
           :old.CRE_DESCR_OUT7,  /*92*/
           :new.CRE_DESCR_OUT7, /*93*/
           :old.CRE_DESCR_OUT8,  /*94*/
           :new.CRE_DESCR_OUT8, /*95*/
           :old.CRE_DESCR_OUT9,  /*96*/
           :new.CRE_DESCR_OUT9, /*97*/
           :old.CRE_DESCR_OUT0,  /*98*/
           :new.CRE_DESCR_OUT0, /*99*/
           :old.CRE_VAL_OUT1,  /*100*/
           :new.CRE_VAL_OUT1, /*101*/
           :old.CRE_VAL_OUT2,  /*102*/
           :new.CRE_VAL_OUT2, /*103*/
           :old.CRE_VAL_OUT3,  /*104*/
           :new.CRE_VAL_OUT3, /*105*/
           :old.CRE_VAL_OUT4,  /*106*/
           :new.CRE_VAL_OUT4, /*107*/
           :old.CRE_VAL_OUT5,  /*108*/
           :new.CRE_VAL_OUT5, /*109*/
           :old.CRE_VAL_OUT6,  /*110*/
           :new.CRE_VAL_OUT6, /*111*/
           :old.CRE_VAL_OUT7,  /*112*/
           :new.CRE_VAL_OUT7, /*113*/
           :old.CRE_VAL_OUT8,  /*114*/
           :new.CRE_VAL_OUT8, /*115*/
           :old.CRE_VAL_OUT9,  /*116*/
           :new.CRE_VAL_OUT9, /*117*/
           :old.CRE_VAL_OUT0,  /*118*/
           :new.CRE_VAL_OUT0, /*119*/
           :old.CRE_DESCR_EST1,  /*120*/
           :new.CRE_DESCR_EST1, /*121*/
           :old.CRE_DESCR_EST2,  /*122*/
           :new.CRE_DESCR_EST2, /*123*/
           :old.CRE_DESCR_EST3,  /*124*/
           :new.CRE_DESCR_EST3, /*125*/
           :old.CRE_DESCR_EST4,  /*126*/
           :new.CRE_DESCR_EST4, /*127*/
           :old.CRE_DESCR_EST5,  /*128*/
           :new.CRE_DESCR_EST5, /*129*/
           :old.CRE_DESCR_EST6,  /*130*/
           :new.CRE_DESCR_EST6, /*131*/
           :old.CRE_DESCR_EST7,  /*132*/
           :new.CRE_DESCR_EST7, /*133*/
           :old.CRE_DESCR_EST8,  /*134*/
           :new.CRE_DESCR_EST8, /*135*/
           :old.CRE_DESCR_EST9,  /*136*/
           :new.CRE_DESCR_EST9, /*137*/
           :old.CRE_DESCR_EST0,  /*138*/
           :new.CRE_DESCR_EST0, /*139*/
           :old.CRE_VAL_EST1,  /*140*/
           :new.CRE_VAL_EST1, /*141*/
           :old.CRE_VAL_EST2,  /*142*/
           :new.CRE_VAL_EST2, /*143*/
           :old.CRE_VAL_EST3,  /*144*/
           :new.CRE_VAL_EST3, /*145*/
           :old.CRE_VAL_EST4,  /*146*/
           :new.CRE_VAL_EST4, /*147*/
           :old.CRE_VAL_EST5,  /*148*/
           :new.CRE_VAL_EST5, /*149*/
           :old.CRE_VAL_EST6,  /*150*/
           :new.CRE_VAL_EST6, /*151*/
           :old.CRE_VAL_EST7,  /*152*/
           :new.CRE_VAL_EST7, /*153*/
           :old.CRE_VAL_EST8,  /*154*/
           :new.CRE_VAL_EST8, /*155*/
           :old.CRE_VAL_EST9,  /*156*/
           :new.CRE_VAL_EST9, /*157*/
           :old.CRE_VAL_EST0,  /*158*/
           :new.CRE_VAL_EST0, /*159*/
           :old.DEB_DESCR_OUT1,  /*160*/
           :new.DEB_DESCR_OUT1, /*161*/
           :old.DEB_DESCR_OUT2,  /*162*/
           :new.DEB_DESCR_OUT2, /*163*/
           :old.DEB_DESCR_OUT3,  /*164*/
           :new.DEB_DESCR_OUT3, /*165*/
           :old.DEB_DESCR_OUT4,  /*166*/
           :new.DEB_DESCR_OUT4, /*167*/
           :old.DEB_DESCR_OUT5,  /*168*/
           :new.DEB_DESCR_OUT5, /*169*/
           :old.DEB_DESCR_OUT6,  /*170*/
           :new.DEB_DESCR_OUT6, /*171*/
           :old.DEB_DESCR_OUT7,  /*172*/
           :new.DEB_DESCR_OUT7, /*173*/
           :old.DEB_DESCR_OUT8,  /*174*/
           :new.DEB_DESCR_OUT8, /*175*/
           :old.DEB_DESCR_OUT9,  /*176*/
           :new.DEB_DESCR_OUT9, /*177*/
           :old.DEB_DESCR_OUT0,  /*178*/
           :new.DEB_DESCR_OUT0, /*179*/
           :old.DEB_VAL_OUT1,  /*180*/
           :new.DEB_VAL_OUT1, /*181*/
           :old.DEB_VAL_OUT2,  /*182*/
           :new.DEB_VAL_OUT2, /*183*/
           :old.DEB_VAL_OUT3,  /*184*/
           :new.DEB_VAL_OUT3, /*185*/
           :old.DEB_VAL_OUT4,  /*186*/
           :new.DEB_VAL_OUT4, /*187*/
           :old.DEB_VAL_OUT5,  /*188*/
           :new.DEB_VAL_OUT5, /*189*/
           :old.DEB_VAL_OUT6,  /*190*/
           :new.DEB_VAL_OUT6, /*191*/
           :old.DEB_VAL_OUT7,  /*192*/
           :new.DEB_VAL_OUT7, /*193*/
           :old.DEB_VAL_OUT8,  /*194*/
           :new.DEB_VAL_OUT8, /*195*/
           :old.DEB_VAL_OUT9,  /*196*/
           :new.DEB_VAL_OUT9, /*197*/
           :old.DEB_VAL_OUT0,  /*198*/
           :new.DEB_VAL_OUT0, /*199*/
           :old.DED_DESCR_OUT1,  /*200*/
           :new.DED_DESCR_OUT1, /*201*/
           :old.DED_DESCR_OUT2,  /*202*/
           :new.DED_DESCR_OUT2, /*203*/
           :old.DED_DESCR_OUT3,  /*204*/
           :new.DED_DESCR_OUT3, /*205*/
           :old.DED_DESCR_OUT4,  /*206*/
           :new.DED_DESCR_OUT4, /*207*/
           :old.DED_DESCR_OUT5,  /*208*/
           :new.DED_DESCR_OUT5, /*209*/
           :old.DED_DESCR_OUT6,  /*210*/
           :new.DED_DESCR_OUT6, /*211*/
           :old.DED_DESCR_OUT7,  /*212*/
           :new.DED_DESCR_OUT7, /*213*/
           :old.DED_DESCR_OUT8,  /*214*/
           :new.DED_DESCR_OUT8, /*215*/
           :old.DED_DESCR_OUT9,  /*216*/
           :new.DED_DESCR_OUT9, /*217*/
           :old.DED_DESCR_OUT0,  /*218*/
           :new.DED_DESCR_OUT0, /*219*/
           :old.DED_VAL_OUT1,  /*220*/
           :new.DED_VAL_OUT1, /*221*/
           :old.DED_VAL_OUT2,  /*222*/
           :new.DED_VAL_OUT2, /*223*/
           :old.DED_VAL_OUT3,  /*224*/
           :new.DED_VAL_OUT3, /*225*/
           :old.DED_VAL_OUT4,  /*226*/
           :new.DED_VAL_OUT4, /*227*/
           :old.DED_VAL_OUT5,  /*228*/
           :new.DED_VAL_OUT5, /*229*/
           :old.DED_VAL_OUT6,  /*230*/
           :new.DED_VAL_OUT6, /*231*/
           :old.DED_VAL_OUT7,  /*232*/
           :new.DED_VAL_OUT7, /*233*/
           :old.DED_VAL_OUT8,  /*234*/
           :new.DED_VAL_OUT8, /*235*/
           :old.DED_VAL_OUT9,  /*236*/
           :new.DED_VAL_OUT9, /*237*/
           :old.DED_VAL_OUT0,  /*238*/
           :new.DED_VAL_OUT0, /*239*/
           :old.NR_CPL_1,  /*240*/
           :new.NR_CPL_1, /*241*/
           :old.NR_CPL_2,  /*242*/
           :new.NR_CPL_2, /*243*/
           :old.NR_CPL_3,  /*244*/
           :new.NR_CPL_3, /*245*/
           :old.NR_CPL_4,  /*246*/
           :new.NR_CPL_4, /*247*/
           :old.NR_CPL_5,  /*248*/
           :new.NR_CPL_5, /*249*/
           :old.NR_CPL_6,  /*250*/
           :new.NR_CPL_6, /*251*/
           :old.NR_CPL_7,  /*252*/
           :new.NR_CPL_7, /*253*/
           :old.DT_CPL_REC_1,  /*254*/
           :new.DT_CPL_REC_1, /*255*/
           :old.DT_CPL_REC_2,  /*256*/
           :new.DT_CPL_REC_2, /*257*/
           :old.DT_CPL_REC_3,  /*258*/
           :new.DT_CPL_REC_3, /*259*/
           :old.DT_CPL_REC_4,  /*260*/
           :new.DT_CPL_REC_4, /*261*/
           :old.DT_CPL_REC_5,  /*262*/
           :new.DT_CPL_REC_5, /*263*/
           :old.DT_CPL_REC_6,  /*264*/
           :new.DT_CPL_REC_6, /*265*/
           :old.DT_CPL_REC_7,  /*266*/
           :new.DT_CPL_REC_7, /*267*/
           :old.VAL_CPL_1,  /*268*/
           :new.VAL_CPL_1, /*269*/
           :old.VAL_CPL_2,  /*270*/
           :new.VAL_CPL_2, /*271*/
           :old.VAL_CPL_3,  /*272*/
           :new.VAL_CPL_3, /*273*/
           :old.VAL_CPL_4,  /*274*/
           :new.VAL_CPL_4, /*275*/
           :old.VAL_CPL_5,  /*276*/
           :new.VAL_CPL_5, /*277*/
           :old.VAL_CPL_6,  /*278*/
           :new.VAL_CPL_6, /*279*/
           :old.VAL_CPL_7,  /*280*/
           :new.VAL_CPL_7, /*281*/
           :old.ORG_CPL_1,  /*282*/
           :new.ORG_CPL_1, /*283*/
           :old.ORG_CPL_2,  /*284*/
           :new.ORG_CPL_2, /*285*/
           :old.ORG_CPL_3,  /*286*/
           :new.ORG_CPL_3, /*287*/
           :old.ORG_CPL_4,  /*288*/
           :new.ORG_CPL_4, /*289*/
           :old.ORG_CPL_5,  /*290*/
           :new.ORG_CPL_5, /*291*/
           :old.ORG_CPL_6,  /*292*/
           :new.ORG_CPL_6, /*293*/
           :old.ORG_CPL_7,  /*294*/
           :new.ORG_CPL_7, /*295*/
           :old.DT_CPL_INF_1,  /*296*/
           :new.DT_CPL_INF_1, /*297*/
           :old.DT_CPL_INF_2,  /*298*/
           :new.DT_CPL_INF_2, /*299*/
           :old.DT_CPL_INF_3,  /*300*/
           :new.DT_CPL_INF_3, /*301*/
           :old.DT_CPL_INF_4,  /*302*/
           :new.DT_CPL_INF_4, /*303*/
           :old.DT_CPL_INF_5,  /*304*/
           :new.DT_CPL_INF_5, /*305*/
           :old.DT_CPL_INF_6,  /*306*/
           :new.DT_CPL_INF_6, /*307*/
           :old.DT_CPL_INF_7,  /*308*/
           :new.DT_CPL_INF_7, /*309*/
           :old.LOC_CPL_1,  /*310*/
           :new.LOC_CPL_1, /*311*/
           :old.LOC_CPL_2,  /*312*/
           :new.LOC_CPL_2, /*313*/
           :old.LOC_CPL_3,  /*314*/
           :new.LOC_CPL_3, /*315*/
           :old.LOC_CPL_4,  /*316*/
           :new.LOC_CPL_4, /*317*/
           :old.LOC_CPL_5,  /*318*/
           :new.LOC_CPL_5, /*319*/
           :old.LOC_CPL_6,  /*320*/
           :new.LOC_CPL_6, /*321*/
           :old.LOC_CPL_7,  /*322*/
           :new.LOC_CPL_7, /*323*/
           :old.OBS_CPL_1,  /*324*/
           :new.OBS_CPL_1, /*325*/
           :old.OBS_CPL_2,  /*326*/
           :new.OBS_CPL_2, /*327*/
           :old.OBS_CPL_3,  /*328*/
           :new.OBS_CPL_3, /*329*/
           :old.OBS_CPL_4,  /*330*/
           :new.OBS_CPL_4, /*331*/
           :old.OBS_CPL_5,  /*332*/
           :new.OBS_CPL_5, /*333*/
           :old.CREDITO_RESSARCIMENTO,  /*334*/
           :new.CREDITO_RESSARCIMENTO, /*335*/
           :old.CRE_RES_VAL01,  /*336*/
           :new.CRE_RES_VAL01, /*337*/
           :old.CRE_RES_VAL02,  /*338*/
           :new.CRE_RES_VAL02, /*339*/
           :old.CRE_RES_VAL03,  /*340*/
           :new.CRE_RES_VAL03, /*341*/
           :old.CRE_RES_VAL04,  /*342*/
           :new.CRE_RES_VAL04, /*343*/
           :old.CRE_RES_VAL05,  /*344*/
           :new.CRE_RES_VAL05, /*345*/
           :old.CRE_RES_VAL06,  /*346*/
           :new.CRE_RES_VAL06, /*347*/
           :old.CRE_RES_VAL07,  /*348*/
           :new.CRE_RES_VAL07, /*349*/
           :old.CRE_RES_VAL08,  /*350*/
           :new.CRE_RES_VAL08, /*351*/
           :old.CRE_RES_VAL09,  /*352*/
           :new.CRE_RES_VAL09, /*353*/
           :old.CRE_RES_VAL10,  /*354*/
           :new.CRE_RES_VAL10, /*355*/
           :old.CRE_RES_OBS01,  /*356*/
           :new.CRE_RES_OBS01, /*357*/
           :old.CRE_RES_OBS02,  /*358*/
           :new.CRE_RES_OBS02, /*359*/
           :old.CRE_RES_OBS03,  /*360*/
           :new.CRE_RES_OBS03, /*361*/
           :old.CRE_RES_OBS04,  /*362*/
           :new.CRE_RES_OBS04, /*363*/
           :old.CRE_RES_OBS05,  /*364*/
           :new.CRE_RES_OBS05, /*365*/
           :old.CRE_RES_OBS06,  /*366*/
           :new.CRE_RES_OBS06, /*367*/
           :old.CRE_RES_OBS07,  /*368*/
           :new.CRE_RES_OBS07, /*369*/
           :old.CRE_RES_OBS08,  /*370*/
           :new.CRE_RES_OBS08, /*371*/
           :old.CRE_RES_OBS09,  /*372*/
           :new.CRE_RES_OBS09, /*373*/
           :old.CRE_RES_OBS10,  /*374*/
           :new.CRE_RES_OBS10  /*375*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into OBRF_280_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_EMPRESA_OLD, /*8*/
           COD_EMPRESA_NEW, /*9*/
           MES_OLD, /*10*/
           MES_NEW, /*11*/
           ANO_OLD, /*12*/
           ANO_NEW, /*13*/
           ICMS_IPI_OLD, /*14*/
           ICMS_IPI_NEW, /*15*/
           PERIODO_INI_OLD, /*16*/
           PERIODO_INI_NEW, /*17*/
           PERIODO_FIM_OLD, /*18*/
           PERIODO_FIM_NEW, /*19*/
           NUM_PAGINA_OLD, /*20*/
           NUM_PAGINA_NEW, /*21*/
           NUMERO_LIVRO_OLD, /*22*/
           NUMERO_LIVRO_NEW, /*23*/
           DEBITO_SAIDAS_OLD, /*24*/
           DEBITO_SAIDAS_NEW, /*25*/
           DEBITO_OUTROS_TOT_OLD, /*26*/
           DEBITO_OUTROS_TOT_NEW, /*27*/
           DEDUCOES_OUTROS_TOT_OLD, /*28*/
           DEDUCOES_OUTROS_TOT_NEW, /*29*/
           CREDITO_ESTORNOS_TOT_OLD, /*30*/
           CREDITO_ESTORNOS_TOT_NEW, /*31*/
           CREDITO_ENTRADAS_OLD, /*32*/
           CREDITO_ENTRADAS_NEW, /*33*/
           CREDITO_OUTROS_TOT_OLD, /*34*/
           CREDITO_OUTROS_TOT_NEW, /*35*/
           DEBITO_ESTORNOS_TOT_OLD, /*36*/
           DEBITO_ESTORNOS_TOT_NEW, /*37*/
           SALDO_ANTERIOR_OLD, /*38*/
           SALDO_ANTERIOR_NEW, /*39*/
           DEB_DESCR_EST1_OLD, /*40*/
           DEB_DESCR_EST1_NEW, /*41*/
           DEB_DESCR_EST2_OLD, /*42*/
           DEB_DESCR_EST2_NEW, /*43*/
           DEB_DESCR_EST3_OLD, /*44*/
           DEB_DESCR_EST3_NEW, /*45*/
           DEB_DESCR_EST4_OLD, /*46*/
           DEB_DESCR_EST4_NEW, /*47*/
           DEB_DESCR_EST5_OLD, /*48*/
           DEB_DESCR_EST5_NEW, /*49*/
           DEB_DESCR_EST6_OLD, /*50*/
           DEB_DESCR_EST6_NEW, /*51*/
           DEB_DESCR_EST7_OLD, /*52*/
           DEB_DESCR_EST7_NEW, /*53*/
           DEB_DESCR_EST8_OLD, /*54*/
           DEB_DESCR_EST8_NEW, /*55*/
           DEB_DESCR_EST9_OLD, /*56*/
           DEB_DESCR_EST9_NEW, /*57*/
           DEB_DESCR_EST0_OLD, /*58*/
           DEB_DESCR_EST0_NEW, /*59*/
           DEB_VAL_EST1_OLD, /*60*/
           DEB_VAL_EST1_NEW, /*61*/
           DEB_VAL_EST2_OLD, /*62*/
           DEB_VAL_EST2_NEW, /*63*/
           DEB_VAL_EST3_OLD, /*64*/
           DEB_VAL_EST3_NEW, /*65*/
           DEB_VAL_EST4_OLD, /*66*/
           DEB_VAL_EST4_NEW, /*67*/
           DEB_VAL_EST5_OLD, /*68*/
           DEB_VAL_EST5_NEW, /*69*/
           DEB_VAL_EST6_OLD, /*70*/
           DEB_VAL_EST6_NEW, /*71*/
           DEB_VAL_EST7_OLD, /*72*/
           DEB_VAL_EST7_NEW, /*73*/
           DEB_VAL_EST8_OLD, /*74*/
           DEB_VAL_EST8_NEW, /*75*/
           DEB_VAL_EST9_OLD, /*76*/
           DEB_VAL_EST9_NEW, /*77*/
           DEB_VAL_EST0_OLD, /*78*/
           DEB_VAL_EST0_NEW, /*79*/
           CRE_DESCR_OUT1_OLD, /*80*/
           CRE_DESCR_OUT1_NEW, /*81*/
           CRE_DESCR_OUT2_OLD, /*82*/
           CRE_DESCR_OUT2_NEW, /*83*/
           CRE_DESCR_OUT3_OLD, /*84*/
           CRE_DESCR_OUT3_NEW, /*85*/
           CRE_DESCR_OUT4_OLD, /*86*/
           CRE_DESCR_OUT4_NEW, /*87*/
           CRE_DESCR_OUT5_OLD, /*88*/
           CRE_DESCR_OUT5_NEW, /*89*/
           CRE_DESCR_OUT6_OLD, /*90*/
           CRE_DESCR_OUT6_NEW, /*91*/
           CRE_DESCR_OUT7_OLD, /*92*/
           CRE_DESCR_OUT7_NEW, /*93*/
           CRE_DESCR_OUT8_OLD, /*94*/
           CRE_DESCR_OUT8_NEW, /*95*/
           CRE_DESCR_OUT9_OLD, /*96*/
           CRE_DESCR_OUT9_NEW, /*97*/
           CRE_DESCR_OUT0_OLD, /*98*/
           CRE_DESCR_OUT0_NEW, /*99*/
           CRE_VAL_OUT1_OLD, /*100*/
           CRE_VAL_OUT1_NEW, /*101*/
           CRE_VAL_OUT2_OLD, /*102*/
           CRE_VAL_OUT2_NEW, /*103*/
           CRE_VAL_OUT3_OLD, /*104*/
           CRE_VAL_OUT3_NEW, /*105*/
           CRE_VAL_OUT4_OLD, /*106*/
           CRE_VAL_OUT4_NEW, /*107*/
           CRE_VAL_OUT5_OLD, /*108*/
           CRE_VAL_OUT5_NEW, /*109*/
           CRE_VAL_OUT6_OLD, /*110*/
           CRE_VAL_OUT6_NEW, /*111*/
           CRE_VAL_OUT7_OLD, /*112*/
           CRE_VAL_OUT7_NEW, /*113*/
           CRE_VAL_OUT8_OLD, /*114*/
           CRE_VAL_OUT8_NEW, /*115*/
           CRE_VAL_OUT9_OLD, /*116*/
           CRE_VAL_OUT9_NEW, /*117*/
           CRE_VAL_OUT0_OLD, /*118*/
           CRE_VAL_OUT0_NEW, /*119*/
           CRE_DESCR_EST1_OLD, /*120*/
           CRE_DESCR_EST1_NEW, /*121*/
           CRE_DESCR_EST2_OLD, /*122*/
           CRE_DESCR_EST2_NEW, /*123*/
           CRE_DESCR_EST3_OLD, /*124*/
           CRE_DESCR_EST3_NEW, /*125*/
           CRE_DESCR_EST4_OLD, /*126*/
           CRE_DESCR_EST4_NEW, /*127*/
           CRE_DESCR_EST5_OLD, /*128*/
           CRE_DESCR_EST5_NEW, /*129*/
           CRE_DESCR_EST6_OLD, /*130*/
           CRE_DESCR_EST6_NEW, /*131*/
           CRE_DESCR_EST7_OLD, /*132*/
           CRE_DESCR_EST7_NEW, /*133*/
           CRE_DESCR_EST8_OLD, /*134*/
           CRE_DESCR_EST8_NEW, /*135*/
           CRE_DESCR_EST9_OLD, /*136*/
           CRE_DESCR_EST9_NEW, /*137*/
           CRE_DESCR_EST0_OLD, /*138*/
           CRE_DESCR_EST0_NEW, /*139*/
           CRE_VAL_EST1_OLD, /*140*/
           CRE_VAL_EST1_NEW, /*141*/
           CRE_VAL_EST2_OLD, /*142*/
           CRE_VAL_EST2_NEW, /*143*/
           CRE_VAL_EST3_OLD, /*144*/
           CRE_VAL_EST3_NEW, /*145*/
           CRE_VAL_EST4_OLD, /*146*/
           CRE_VAL_EST4_NEW, /*147*/
           CRE_VAL_EST5_OLD, /*148*/
           CRE_VAL_EST5_NEW, /*149*/
           CRE_VAL_EST6_OLD, /*150*/
           CRE_VAL_EST6_NEW, /*151*/
           CRE_VAL_EST7_OLD, /*152*/
           CRE_VAL_EST7_NEW, /*153*/
           CRE_VAL_EST8_OLD, /*154*/
           CRE_VAL_EST8_NEW, /*155*/
           CRE_VAL_EST9_OLD, /*156*/
           CRE_VAL_EST9_NEW, /*157*/
           CRE_VAL_EST0_OLD, /*158*/
           CRE_VAL_EST0_NEW, /*159*/
           DEB_DESCR_OUT1_OLD, /*160*/
           DEB_DESCR_OUT1_NEW, /*161*/
           DEB_DESCR_OUT2_OLD, /*162*/
           DEB_DESCR_OUT2_NEW, /*163*/
           DEB_DESCR_OUT3_OLD, /*164*/
           DEB_DESCR_OUT3_NEW, /*165*/
           DEB_DESCR_OUT4_OLD, /*166*/
           DEB_DESCR_OUT4_NEW, /*167*/
           DEB_DESCR_OUT5_OLD, /*168*/
           DEB_DESCR_OUT5_NEW, /*169*/
           DEB_DESCR_OUT6_OLD, /*170*/
           DEB_DESCR_OUT6_NEW, /*171*/
           DEB_DESCR_OUT7_OLD, /*172*/
           DEB_DESCR_OUT7_NEW, /*173*/
           DEB_DESCR_OUT8_OLD, /*174*/
           DEB_DESCR_OUT8_NEW, /*175*/
           DEB_DESCR_OUT9_OLD, /*176*/
           DEB_DESCR_OUT9_NEW, /*177*/
           DEB_DESCR_OUT0_OLD, /*178*/
           DEB_DESCR_OUT0_NEW, /*179*/
           DEB_VAL_OUT1_OLD, /*180*/
           DEB_VAL_OUT1_NEW, /*181*/
           DEB_VAL_OUT2_OLD, /*182*/
           DEB_VAL_OUT2_NEW, /*183*/
           DEB_VAL_OUT3_OLD, /*184*/
           DEB_VAL_OUT3_NEW, /*185*/
           DEB_VAL_OUT4_OLD, /*186*/
           DEB_VAL_OUT4_NEW, /*187*/
           DEB_VAL_OUT5_OLD, /*188*/
           DEB_VAL_OUT5_NEW, /*189*/
           DEB_VAL_OUT6_OLD, /*190*/
           DEB_VAL_OUT6_NEW, /*191*/
           DEB_VAL_OUT7_OLD, /*192*/
           DEB_VAL_OUT7_NEW, /*193*/
           DEB_VAL_OUT8_OLD, /*194*/
           DEB_VAL_OUT8_NEW, /*195*/
           DEB_VAL_OUT9_OLD, /*196*/
           DEB_VAL_OUT9_NEW, /*197*/
           DEB_VAL_OUT0_OLD, /*198*/
           DEB_VAL_OUT0_NEW, /*199*/
           DED_DESCR_OUT1_OLD, /*200*/
           DED_DESCR_OUT1_NEW, /*201*/
           DED_DESCR_OUT2_OLD, /*202*/
           DED_DESCR_OUT2_NEW, /*203*/
           DED_DESCR_OUT3_OLD, /*204*/
           DED_DESCR_OUT3_NEW, /*205*/
           DED_DESCR_OUT4_OLD, /*206*/
           DED_DESCR_OUT4_NEW, /*207*/
           DED_DESCR_OUT5_OLD, /*208*/
           DED_DESCR_OUT5_NEW, /*209*/
           DED_DESCR_OUT6_OLD, /*210*/
           DED_DESCR_OUT6_NEW, /*211*/
           DED_DESCR_OUT7_OLD, /*212*/
           DED_DESCR_OUT7_NEW, /*213*/
           DED_DESCR_OUT8_OLD, /*214*/
           DED_DESCR_OUT8_NEW, /*215*/
           DED_DESCR_OUT9_OLD, /*216*/
           DED_DESCR_OUT9_NEW, /*217*/
           DED_DESCR_OUT0_OLD, /*218*/
           DED_DESCR_OUT0_NEW, /*219*/
           DED_VAL_OUT1_OLD, /*220*/
           DED_VAL_OUT1_NEW, /*221*/
           DED_VAL_OUT2_OLD, /*222*/
           DED_VAL_OUT2_NEW, /*223*/
           DED_VAL_OUT3_OLD, /*224*/
           DED_VAL_OUT3_NEW, /*225*/
           DED_VAL_OUT4_OLD, /*226*/
           DED_VAL_OUT4_NEW, /*227*/
           DED_VAL_OUT5_OLD, /*228*/
           DED_VAL_OUT5_NEW, /*229*/
           DED_VAL_OUT6_OLD, /*230*/
           DED_VAL_OUT6_NEW, /*231*/
           DED_VAL_OUT7_OLD, /*232*/
           DED_VAL_OUT7_NEW, /*233*/
           DED_VAL_OUT8_OLD, /*234*/
           DED_VAL_OUT8_NEW, /*235*/
           DED_VAL_OUT9_OLD, /*236*/
           DED_VAL_OUT9_NEW, /*237*/
           DED_VAL_OUT0_OLD, /*238*/
           DED_VAL_OUT0_NEW, /*239*/
           NR_CPL_1_OLD, /*240*/
           NR_CPL_1_NEW, /*241*/
           NR_CPL_2_OLD, /*242*/
           NR_CPL_2_NEW, /*243*/
           NR_CPL_3_OLD, /*244*/
           NR_CPL_3_NEW, /*245*/
           NR_CPL_4_OLD, /*246*/
           NR_CPL_4_NEW, /*247*/
           NR_CPL_5_OLD, /*248*/
           NR_CPL_5_NEW, /*249*/
           NR_CPL_6_OLD, /*250*/
           NR_CPL_6_NEW, /*251*/
           NR_CPL_7_OLD, /*252*/
           NR_CPL_7_NEW, /*253*/
           DT_CPL_REC_1_OLD, /*254*/
           DT_CPL_REC_1_NEW, /*255*/
           DT_CPL_REC_2_OLD, /*256*/
           DT_CPL_REC_2_NEW, /*257*/
           DT_CPL_REC_3_OLD, /*258*/
           DT_CPL_REC_3_NEW, /*259*/
           DT_CPL_REC_4_OLD, /*260*/
           DT_CPL_REC_4_NEW, /*261*/
           DT_CPL_REC_5_OLD, /*262*/
           DT_CPL_REC_5_NEW, /*263*/
           DT_CPL_REC_6_OLD, /*264*/
           DT_CPL_REC_6_NEW, /*265*/
           DT_CPL_REC_7_OLD, /*266*/
           DT_CPL_REC_7_NEW, /*267*/
           VAL_CPL_1_OLD, /*268*/
           VAL_CPL_1_NEW, /*269*/
           VAL_CPL_2_OLD, /*270*/
           VAL_CPL_2_NEW, /*271*/
           VAL_CPL_3_OLD, /*272*/
           VAL_CPL_3_NEW, /*273*/
           VAL_CPL_4_OLD, /*274*/
           VAL_CPL_4_NEW, /*275*/
           VAL_CPL_5_OLD, /*276*/
           VAL_CPL_5_NEW, /*277*/
           VAL_CPL_6_OLD, /*278*/
           VAL_CPL_6_NEW, /*279*/
           VAL_CPL_7_OLD, /*280*/
           VAL_CPL_7_NEW, /*281*/
           ORG_CPL_1_OLD, /*282*/
           ORG_CPL_1_NEW, /*283*/
           ORG_CPL_2_OLD, /*284*/
           ORG_CPL_2_NEW, /*285*/
           ORG_CPL_3_OLD, /*286*/
           ORG_CPL_3_NEW, /*287*/
           ORG_CPL_4_OLD, /*288*/
           ORG_CPL_4_NEW, /*289*/
           ORG_CPL_5_OLD, /*290*/
           ORG_CPL_5_NEW, /*291*/
           ORG_CPL_6_OLD, /*292*/
           ORG_CPL_6_NEW, /*293*/
           ORG_CPL_7_OLD, /*294*/
           ORG_CPL_7_NEW, /*295*/
           DT_CPL_INF_1_OLD, /*296*/
           DT_CPL_INF_1_NEW, /*297*/
           DT_CPL_INF_2_OLD, /*298*/
           DT_CPL_INF_2_NEW, /*299*/
           DT_CPL_INF_3_OLD, /*300*/
           DT_CPL_INF_3_NEW, /*301*/
           DT_CPL_INF_4_OLD, /*302*/
           DT_CPL_INF_4_NEW, /*303*/
           DT_CPL_INF_5_OLD, /*304*/
           DT_CPL_INF_5_NEW, /*305*/
           DT_CPL_INF_6_OLD, /*306*/
           DT_CPL_INF_6_NEW, /*307*/
           DT_CPL_INF_7_OLD, /*308*/
           DT_CPL_INF_7_NEW, /*309*/
           LOC_CPL_1_OLD, /*310*/
           LOC_CPL_1_NEW, /*311*/
           LOC_CPL_2_OLD, /*312*/
           LOC_CPL_2_NEW, /*313*/
           LOC_CPL_3_OLD, /*314*/
           LOC_CPL_3_NEW, /*315*/
           LOC_CPL_4_OLD, /*316*/
           LOC_CPL_4_NEW, /*317*/
           LOC_CPL_5_OLD, /*318*/
           LOC_CPL_5_NEW, /*319*/
           LOC_CPL_6_OLD, /*320*/
           LOC_CPL_6_NEW, /*321*/
           LOC_CPL_7_OLD, /*322*/
           LOC_CPL_7_NEW, /*323*/
           OBS_CPL_1_OLD, /*324*/
           OBS_CPL_1_NEW, /*325*/
           OBS_CPL_2_OLD, /*326*/
           OBS_CPL_2_NEW, /*327*/
           OBS_CPL_3_OLD, /*328*/
           OBS_CPL_3_NEW, /*329*/
           OBS_CPL_4_OLD, /*330*/
           OBS_CPL_4_NEW, /*331*/
           OBS_CPL_5_OLD, /*332*/
           OBS_CPL_5_NEW, /*333*/
           CREDITO_RESSARCIMENTO_OLD, /*334*/
           CREDITO_RESSARCIMENTO_NEW, /*335*/
           CRE_RES_VAL01_OLD, /*336*/
           CRE_RES_VAL01_NEW, /*337*/
           CRE_RES_VAL02_OLD, /*338*/
           CRE_RES_VAL02_NEW, /*339*/
           CRE_RES_VAL03_OLD, /*340*/
           CRE_RES_VAL03_NEW, /*341*/
           CRE_RES_VAL04_OLD, /*342*/
           CRE_RES_VAL04_NEW, /*343*/
           CRE_RES_VAL05_OLD, /*344*/
           CRE_RES_VAL05_NEW, /*345*/
           CRE_RES_VAL06_OLD, /*346*/
           CRE_RES_VAL06_NEW, /*347*/
           CRE_RES_VAL07_OLD, /*348*/
           CRE_RES_VAL07_NEW, /*349*/
           CRE_RES_VAL08_OLD, /*350*/
           CRE_RES_VAL08_NEW, /*351*/
           CRE_RES_VAL09_OLD, /*352*/
           CRE_RES_VAL09_NEW, /*353*/
           CRE_RES_VAL10_OLD, /*354*/
           CRE_RES_VAL10_NEW, /*355*/
           CRE_RES_OBS01_OLD, /*356*/
           CRE_RES_OBS01_NEW, /*357*/
           CRE_RES_OBS02_OLD, /*358*/
           CRE_RES_OBS02_NEW, /*359*/
           CRE_RES_OBS03_OLD, /*360*/
           CRE_RES_OBS03_NEW, /*361*/
           CRE_RES_OBS04_OLD, /*362*/
           CRE_RES_OBS04_NEW, /*363*/
           CRE_RES_OBS05_OLD, /*364*/
           CRE_RES_OBS05_NEW, /*365*/
           CRE_RES_OBS06_OLD, /*366*/
           CRE_RES_OBS06_NEW, /*367*/
           CRE_RES_OBS07_OLD, /*368*/
           CRE_RES_OBS07_NEW, /*369*/
           CRE_RES_OBS08_OLD, /*370*/
           CRE_RES_OBS08_NEW, /*371*/
           CRE_RES_OBS09_OLD, /*372*/
           CRE_RES_OBS09_NEW, /*373*/
           CRE_RES_OBS10_OLD, /*374*/
           CRE_RES_OBS10_NEW /*375*/
        ) values (
            'D', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede,/*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.COD_EMPRESA, /*8*/
           0, /*9*/
           :old.MES, /*10*/
           0, /*11*/
           :old.ANO, /*12*/
           0, /*13*/
           :old.ICMS_IPI, /*14*/
           0, /*15*/
           :old.PERIODO_INI, /*16*/
           null, /*17*/
           :old.PERIODO_FIM, /*18*/
           null, /*19*/
           :old.NUM_PAGINA, /*20*/
           0, /*21*/
           :old.NUMERO_LIVRO, /*22*/
           0, /*23*/
           :old.DEBITO_SAIDAS, /*24*/
           0, /*25*/
           :old.DEBITO_OUTROS_TOT, /*26*/
           0, /*27*/
           :old.DEDUCOES_OUTROS_TOT, /*28*/
           0, /*29*/
           :old.CREDITO_ESTORNOS_TOT, /*30*/
           0, /*31*/
           :old.CREDITO_ENTRADAS, /*32*/
           0, /*33*/
           :old.CREDITO_OUTROS_TOT, /*34*/
           0, /*35*/
           :old.DEBITO_ESTORNOS_TOT, /*36*/
           0, /*37*/
           :old.SALDO_ANTERIOR, /*38*/
           0, /*39*/
           :old.DEB_DESCR_EST1, /*40*/
           '', /*41*/
           :old.DEB_DESCR_EST2, /*42*/
           '', /*43*/
           :old.DEB_DESCR_EST3, /*44*/
           '', /*45*/
           :old.DEB_DESCR_EST4, /*46*/
           '', /*47*/
           :old.DEB_DESCR_EST5, /*48*/
           '', /*49*/
           :old.DEB_DESCR_EST6, /*50*/
           '', /*51*/
           :old.DEB_DESCR_EST7, /*52*/
           '', /*53*/
           :old.DEB_DESCR_EST8, /*54*/
           '', /*55*/
           :old.DEB_DESCR_EST9, /*56*/
           '', /*57*/
           :old.DEB_DESCR_EST0, /*58*/
           '', /*59*/
           :old.DEB_VAL_EST1, /*60*/
           0, /*61*/
           :old.DEB_VAL_EST2, /*62*/
           0, /*63*/
           :old.DEB_VAL_EST3, /*64*/
           0, /*65*/
           :old.DEB_VAL_EST4, /*66*/
           0, /*67*/
           :old.DEB_VAL_EST5, /*68*/
           0, /*69*/
           :old.DEB_VAL_EST6, /*70*/
           0, /*71*/
           :old.DEB_VAL_EST7, /*72*/
           0, /*73*/
           :old.DEB_VAL_EST8, /*74*/
           0, /*75*/
           :old.DEB_VAL_EST9, /*76*/
           0, /*77*/
           :old.DEB_VAL_EST0, /*78*/
           0, /*79*/
           :old.CRE_DESCR_OUT1, /*80*/
           '', /*81*/
           :old.CRE_DESCR_OUT2, /*82*/
           '', /*83*/
           :old.CRE_DESCR_OUT3, /*84*/
           '', /*85*/
           :old.CRE_DESCR_OUT4, /*86*/
           '', /*87*/
           :old.CRE_DESCR_OUT5, /*88*/
           '', /*89*/
           :old.CRE_DESCR_OUT6, /*90*/
           '', /*91*/
           :old.CRE_DESCR_OUT7, /*92*/
           '', /*93*/
           :old.CRE_DESCR_OUT8, /*94*/
           '', /*95*/
           :old.CRE_DESCR_OUT9, /*96*/
           '', /*97*/
           :old.CRE_DESCR_OUT0, /*98*/
           '', /*99*/
           :old.CRE_VAL_OUT1, /*100*/
           0, /*101*/
           :old.CRE_VAL_OUT2, /*102*/
           0, /*103*/
           :old.CRE_VAL_OUT3, /*104*/
           0, /*105*/
           :old.CRE_VAL_OUT4, /*106*/
           0, /*107*/
           :old.CRE_VAL_OUT5, /*108*/
           0, /*109*/
           :old.CRE_VAL_OUT6, /*110*/
           0, /*111*/
           :old.CRE_VAL_OUT7, /*112*/
           0, /*113*/
           :old.CRE_VAL_OUT8, /*114*/
           0, /*115*/
           :old.CRE_VAL_OUT9, /*116*/
           0, /*117*/
           :old.CRE_VAL_OUT0, /*118*/
           0, /*119*/
           :old.CRE_DESCR_EST1, /*120*/
           '', /*121*/
           :old.CRE_DESCR_EST2, /*122*/
           '', /*123*/
           :old.CRE_DESCR_EST3, /*124*/
           '', /*125*/
           :old.CRE_DESCR_EST4, /*126*/
           '', /*127*/
           :old.CRE_DESCR_EST5, /*128*/
           '', /*129*/
           :old.CRE_DESCR_EST6, /*130*/
           '', /*131*/
           :old.CRE_DESCR_EST7, /*132*/
           '', /*133*/
           :old.CRE_DESCR_EST8, /*134*/
           '', /*135*/
           :old.CRE_DESCR_EST9, /*136*/
           '', /*137*/
           :old.CRE_DESCR_EST0, /*138*/
           '', /*139*/
           :old.CRE_VAL_EST1, /*140*/
           0, /*141*/
           :old.CRE_VAL_EST2, /*142*/
           0, /*143*/
           :old.CRE_VAL_EST3, /*144*/
           0, /*145*/
           :old.CRE_VAL_EST4, /*146*/
           0, /*147*/
           :old.CRE_VAL_EST5, /*148*/
           0, /*149*/
           :old.CRE_VAL_EST6, /*150*/
           0, /*151*/
           :old.CRE_VAL_EST7, /*152*/
           0, /*153*/
           :old.CRE_VAL_EST8, /*154*/
           0, /*155*/
           :old.CRE_VAL_EST9, /*156*/
           0, /*157*/
           :old.CRE_VAL_EST0, /*158*/
           0, /*159*/
           :old.DEB_DESCR_OUT1, /*160*/
           '', /*161*/
           :old.DEB_DESCR_OUT2, /*162*/
           '', /*163*/
           :old.DEB_DESCR_OUT3, /*164*/
           '', /*165*/
           :old.DEB_DESCR_OUT4, /*166*/
           '', /*167*/
           :old.DEB_DESCR_OUT5, /*168*/
           '', /*169*/
           :old.DEB_DESCR_OUT6, /*170*/
           '', /*171*/
           :old.DEB_DESCR_OUT7, /*172*/
           '', /*173*/
           :old.DEB_DESCR_OUT8, /*174*/
           '', /*175*/
           :old.DEB_DESCR_OUT9, /*176*/
           '', /*177*/
           :old.DEB_DESCR_OUT0, /*178*/
           '', /*179*/
           :old.DEB_VAL_OUT1, /*180*/
           0, /*181*/
           :old.DEB_VAL_OUT2, /*182*/
           0, /*183*/
           :old.DEB_VAL_OUT3, /*184*/
           0, /*185*/
           :old.DEB_VAL_OUT4, /*186*/
           0, /*187*/
           :old.DEB_VAL_OUT5, /*188*/
           0, /*189*/
           :old.DEB_VAL_OUT6, /*190*/
           0, /*191*/
           :old.DEB_VAL_OUT7, /*192*/
           0, /*193*/
           :old.DEB_VAL_OUT8, /*194*/
           0, /*195*/
           :old.DEB_VAL_OUT9, /*196*/
           0, /*197*/
           :old.DEB_VAL_OUT0, /*198*/
           0, /*199*/
           :old.DED_DESCR_OUT1, /*200*/
           '', /*201*/
           :old.DED_DESCR_OUT2, /*202*/
           '', /*203*/
           :old.DED_DESCR_OUT3, /*204*/
           '', /*205*/
           :old.DED_DESCR_OUT4, /*206*/
           '', /*207*/
           :old.DED_DESCR_OUT5, /*208*/
           '', /*209*/
           :old.DED_DESCR_OUT6, /*210*/
           '', /*211*/
           :old.DED_DESCR_OUT7, /*212*/
           '', /*213*/
           :old.DED_DESCR_OUT8, /*214*/
           '', /*215*/
           :old.DED_DESCR_OUT9, /*216*/
           '', /*217*/
           :old.DED_DESCR_OUT0, /*218*/
           '', /*219*/
           :old.DED_VAL_OUT1, /*220*/
           0, /*221*/
           :old.DED_VAL_OUT2, /*222*/
           0, /*223*/
           :old.DED_VAL_OUT3, /*224*/
           0, /*225*/
           :old.DED_VAL_OUT4, /*226*/
           0, /*227*/
           :old.DED_VAL_OUT5, /*228*/
           0, /*229*/
           :old.DED_VAL_OUT6, /*230*/
           0, /*231*/
           :old.DED_VAL_OUT7, /*232*/
           0, /*233*/
           :old.DED_VAL_OUT8, /*234*/
           0, /*235*/
           :old.DED_VAL_OUT9, /*236*/
           0, /*237*/
           :old.DED_VAL_OUT0, /*238*/
           0, /*239*/
           :old.NR_CPL_1, /*240*/
           0, /*241*/
           :old.NR_CPL_2, /*242*/
           0, /*243*/
           :old.NR_CPL_3, /*244*/
           0, /*245*/
           :old.NR_CPL_4, /*246*/
           0, /*247*/
           :old.NR_CPL_5, /*248*/
           0, /*249*/
           :old.NR_CPL_6, /*250*/
           0, /*251*/
           :old.NR_CPL_7, /*252*/
           0, /*253*/
           :old.DT_CPL_REC_1, /*254*/
           null, /*255*/
           :old.DT_CPL_REC_2, /*256*/
           null, /*257*/
           :old.DT_CPL_REC_3, /*258*/
           null, /*259*/
           :old.DT_CPL_REC_4, /*260*/
           null, /*261*/
           :old.DT_CPL_REC_5, /*262*/
           null, /*263*/
           :old.DT_CPL_REC_6, /*264*/
           null, /*265*/
           :old.DT_CPL_REC_7, /*266*/
           null, /*267*/
           :old.VAL_CPL_1, /*268*/
           0, /*269*/
           :old.VAL_CPL_2, /*270*/
           0, /*271*/
           :old.VAL_CPL_3, /*272*/
           0, /*273*/
           :old.VAL_CPL_4, /*274*/
           0, /*275*/
           :old.VAL_CPL_5, /*276*/
           0, /*277*/
           :old.VAL_CPL_6, /*278*/
           0, /*279*/
           :old.VAL_CPL_7, /*280*/
           0, /*281*/
           :old.ORG_CPL_1, /*282*/
           '', /*283*/
           :old.ORG_CPL_2, /*284*/
           '', /*285*/
           :old.ORG_CPL_3, /*286*/
           '', /*287*/
           :old.ORG_CPL_4, /*288*/
           '', /*289*/
           :old.ORG_CPL_5, /*290*/
           '', /*291*/
           :old.ORG_CPL_6, /*292*/
           '', /*293*/
           :old.ORG_CPL_7, /*294*/
           '', /*295*/
           :old.DT_CPL_INF_1, /*296*/
           null, /*297*/
           :old.DT_CPL_INF_2, /*298*/
           null, /*299*/
           :old.DT_CPL_INF_3, /*300*/
           null, /*301*/
           :old.DT_CPL_INF_4, /*302*/
           null, /*303*/
           :old.DT_CPL_INF_5, /*304*/
           null, /*305*/
           :old.DT_CPL_INF_6, /*306*/
           null, /*307*/
           :old.DT_CPL_INF_7, /*308*/
           null, /*309*/
           :old.LOC_CPL_1, /*310*/
           '', /*311*/
           :old.LOC_CPL_2, /*312*/
           '', /*313*/
           :old.LOC_CPL_3, /*314*/
           '', /*315*/
           :old.LOC_CPL_4, /*316*/
           '', /*317*/
           :old.LOC_CPL_5, /*318*/
           '', /*319*/
           :old.LOC_CPL_6, /*320*/
           '', /*321*/
           :old.LOC_CPL_7, /*322*/
           '', /*323*/
           :old.OBS_CPL_1, /*324*/
           '', /*325*/
           :old.OBS_CPL_2, /*326*/
           '', /*327*/
           :old.OBS_CPL_3, /*328*/
           '', /*329*/
           :old.OBS_CPL_4, /*330*/
           '', /*331*/
           :old.OBS_CPL_5, /*332*/
           '', /*333*/
           :old.CREDITO_RESSARCIMENTO, /*334*/
           0, /*335*/
           :old.CRE_RES_VAL01, /*336*/
           0, /*337*/
           :old.CRE_RES_VAL02, /*338*/
           0, /*339*/
           :old.CRE_RES_VAL03, /*340*/
           0, /*341*/
           :old.CRE_RES_VAL04, /*342*/
           0, /*343*/
           :old.CRE_RES_VAL05, /*344*/
           0, /*345*/
           :old.CRE_RES_VAL06, /*346*/
           0, /*347*/
           :old.CRE_RES_VAL07, /*348*/
           0, /*349*/
           :old.CRE_RES_VAL08, /*350*/
           0, /*351*/
           :old.CRE_RES_VAL09, /*352*/
           0, /*353*/
           :old.CRE_RES_VAL10, /*354*/
           0, /*355*/
           :old.CRE_RES_OBS01, /*356*/
           '', /*357*/
           :old.CRE_RES_OBS02, /*358*/
           '', /*359*/
           :old.CRE_RES_OBS03, /*360*/
           '', /*361*/
           :old.CRE_RES_OBS04, /*362*/
           '', /*363*/
           :old.CRE_RES_OBS05, /*364*/
           '', /*365*/
           :old.CRE_RES_OBS06, /*366*/
           '', /*367*/
           :old.CRE_RES_OBS07, /*368*/
           '', /*369*/
           :old.CRE_RES_OBS08, /*370*/
           '', /*371*/
           :old.CRE_RES_OBS09, /*372*/
           '', /*373*/
           :old.CRE_RES_OBS10, /*374*/
           '' /*375*/
         );
    end;
 end if;
end inter_tr_OBRF_280_log;

-- ALTER TRIGGER "INTER_TR_OBRF_280_LOG" ENABLE
 

/

exec inter_pr_recompile;

