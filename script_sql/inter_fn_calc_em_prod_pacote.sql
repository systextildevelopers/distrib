
  CREATE OR REPLACE FUNCTION "INTER_FN_CALC_EM_PROD_PACOTE" (p_periodo_producao          in number,      p_ordem_confeccao                in number,
                                                        p_codigo_estagio_consulta   in number)
RETURN number is
   v_qtde_em_producao              number (15,4) :=0;

begin
   begin
      select pcpc_040.qtde_em_producao_pacote
      into    v_qtde_em_producao
      from pcpc_040
      where pcpc_040.periodo_producao = p_periodo_producao           ---- Periodo Producao
        and pcpc_040.ordem_confeccao  = p_ordem_confeccao            ---- Pacote
        and pcpc_040.codigo_estagio   = p_codigo_estagio_consulta;   ---- Codigo do estagio
   exception when others then
      v_qtde_em_producao := 0;
   end;
   
   return(v_qtde_em_producao);

end INTER_FN_CALC_EM_PROD_PACOTE;

 

/

exec inter_pr_recompile;

