alter table Fatu_070
	add FLAG_MARCA_CHEQUE number(1) default 0;
	
alter table crec_170
	add FLAG_MARCA_CHEQUE number(1) default 0;
	
alter table crec_180
	add ( VALOR_JUROS NUMBER(15,2) default 0.00,
		VALOR_DESCONTOS NUMBER(15,2) default 0.00);
		
alter table crec_150
	add ( VALOR_JUROS NUMBER(15,2) default 0.00,
		VALOR_DESCONTOS NUMBER(15,2) default 0.00);
	
alter table fatu_070
	add COD_USUARIO NUMBER(9);
	
alter table crec_170
	add VLR_PRESENTE number(15,2) default 0;
	
alter table fatu_070
	add VLR_PRESENTE number(15,2);

exec inter_pr_recompile;
/
