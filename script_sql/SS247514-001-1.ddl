CREATE TABLE OBRF_772 (
   CODIGO_EMPRESA NUMBER(3) NOT NULL,
   CNPJ9          NUMBER(9) NOT NULL,
   CNPJ4          NUMBER(4) NOT NULL,
   CNPJ2          NUMBER(2) NOT NULL,
   NAT_OPERACAO   NUMBER(3) NOT NULL,
   COD_DEPOSITO   NUMBER(3) NOT NULL,
   FUNCIONARIO    NUMBER(1) NOT NULL,
   COD_PROCESSO   NUMBER(3) NOT NULL,
   ATIVO_INATIVO  NUMBER(1) NOT NULL
);

create table OBRF_772_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(20) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(20) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  CODIGO_EMPRESA_OLD NUMBER(3) ,  
  CODIGO_EMPRESA_NEW NUMBER(3) , 
  CNPJ9_OLD          NUMBER(9) ,  
  CNPJ9_NEW          NUMBER(9) , 
  CNPJ4_OLD          NUMBER(4) ,  
  CNPJ4_NEW          NUMBER(4) , 
  CNPJ2_OLD          NUMBER(2) ,  
  CNPJ2_NEW          NUMBER(2) , 
  NAT_OPERACAO_OLD   NUMBER(3) ,  
  NAT_OPERACAO_NEW   NUMBER(3) , 
  COD_DEPOSITO_OLD   NUMBER(3) ,  
  COD_DEPOSITO_NEW   NUMBER(3) , 
  FUNCIONARIO_OLD    NUMBER(1) ,  
  FUNCIONARIO_NEW    NUMBER(1) , 
  COD_PROCESSO_OLD   NUMBER(3) ,  
  COD_PROCESSO_NEW   NUMBER(3) , 
  ATIVO_INATIVO_OLD  NUMBER(1) , 
  ATIVO_INATIVO_NEW  NUMBER(1)  
) ;
