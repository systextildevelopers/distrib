
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_320_1" 
before insert
    or update of situacao_volume, flag_complemento_tag
    on pcpc_320
for each row
declare
  v_executa_trigger  number(1);
  v_reg_ok           number;

begin
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      if UPDATING
      then
         if  :old.situacao_volume      = 4
         and :old.flag_complemento_tag = 1
         then
            v_reg_ok := 1;  -- Nao esta OK

            if  :new.situacao_volume      = 4
            and :new.flag_complemento_tag = 1
            then
               v_reg_ok := 0;  -- Esta OK
            end if;

            if  :new.situacao_volume      = 1
            and :new.flag_complemento_tag = 0
            then
               v_reg_ok := 0;  -- Esta OK
            end if;

            if v_reg_ok = 1  -- Nao esta OK
            then
               raise_application_error(-20000,'Este volume esta no setor de colocacao de TAG de cliente.');
            end if;
         end if;

         if  :old.situacao_volume      = 1
         and :old.flag_complemento_tag = 0
         then
            v_reg_ok := 1;  -- Nao esta OK

            if  :new.situacao_volume      = 4
            and :new.flag_complemento_tag = 1
            then
               v_reg_ok := 0;  -- Esta OK
            end if;

            if  :new.situacao_volume      = 1
            and :new.flag_complemento_tag = 0
            then
               v_reg_ok := 0;  -- Esta OK
            end if;

            if  :new.flag_complemento_tag = 0
            then
               v_reg_ok := 0;  -- Esta OK
            end if;

            if v_reg_ok = 1  -- Nao esta OK
            then
               raise_application_error(-20000,'Este volume foi enviado para o setor de colocacao de TAG de cliente.');
            end if;
         end if;
      end if;
   end if;
end inter_tr_pcpc_320_1;

-- ALTER TRIGGER "INTER_TR_PCPC_320_1" ENABLE
 

/

exec inter_pr_recompile;

