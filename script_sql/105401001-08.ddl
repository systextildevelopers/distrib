alter table loja_075 add (nr_autorizacao_opera  varchar2(100));
alter table loja_075 modify nr_autorizacao_opera  default ' ';

comment on COLUMN loja_075.nr_autorizacao_opera is 'Numero de autorizacao do cartao para a venda';
/
exec inter_pr_recompile;
