
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_110_1" 
  before insert or delete or update of qtde_pedida, percentual_desc, valor_unitario on pedi_110
  for each row
declare
  v_qtde_registros   number(01);
  v_sequencia        pedi_135.seq_situacao%type;
  v_pedido           pedi_110.pedido_venda%type;
  v_executa_trigger  number(1);
  v_gera_bloqueio    varchar2(1);
  v_tem_origem       varchar2(1);
  v_origem_pedido    pedi_100.origem_pedido%type;
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if deleting
      then v_pedido     := :old.pedido_venda;
      else v_pedido     := :new.pedido_venda;
      end if;
      select count(*)
      into v_qtde_registros
      from pedi_135
      where pedido_venda = v_pedido
       and  codigo_situacao in (25,26);
      if v_qtde_registros is null
      then v_qtde_registros := 0;
      end if;
      if v_qtde_registros = 0
      then

         begin
            select pedi_100.origem_pedido
            into v_origem_pedido
            from pedi_100
            where pedi_100.pedido_venda = :new.pedido_venda;
            exception when no_data_found then
               v_origem_pedido := 0;
         end;

         v_tem_origem := 'n';

         begin
            select 's'
            into v_tem_origem
            from pedi_900
            where pedi_900.origem_pedido = v_origem_pedido
              and rownum = 1;
            exception when no_data_found then
               v_tem_origem := 'n';
         end;
         if v_tem_origem = 's'
         then
            v_gera_bloqueio := 'n';
            begin
               select 's'
               into v_gera_bloqueio
               from pedi_900
               where pedi_900.origem_pedido   = v_origem_pedido
                 and pedi_900.motivo_bloqueio = 26;
               exception
                  when no_data_found then
                      v_gera_bloqueio := 'n';
            end;
         else
            begin
                select 's'
                into v_gera_bloqueio
                from hdoc_001
                where hdoc_001.tipo = 200
                and hdoc_001.codigo = v_origem_pedido;
                exception
                    when no_data_found then
                       v_gera_bloqueio := 'n';
            end;
         end if;

         if v_gera_bloqueio = 's'
         then

            select max(seq_situacao)
            into  v_sequencia
            from pedi_135
            where pedido_venda = v_pedido;
            if v_sequencia is null
            then v_sequencia := 1;
            else v_sequencia := v_sequencia + 1;
            end if;
            INSERT INTO pedi_135
               (pedido_venda,
                seq_situacao,
                codigo_situacao,
                data_situacao,
                flag_liberacao,
                usuario_bloqueio)
            VALUES
               (v_pedido,
                v_sequencia,
                26,
                trunc(sysdate,'DD'),
                'N',
                'SISTEMA');
         end if;
      end if;
   end if;
end inter_tr_pedi_110_1;
-- ALTER TRIGGER "INTER_TR_PEDI_110_1" ENABLE
 

/

exec inter_pr_recompile;

