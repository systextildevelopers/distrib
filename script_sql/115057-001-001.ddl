alter table hdoc_030 add altera_dt_baixa_ot varchar2(1) default 'S';

comment on column hdoc_030.altera_dt_baixa_ot is 'Permite alterar data da baixa da OT (Nos programas pcpb_f180, pcpb_f185, pcpt_f080)';

exec inter_pr_recompile;
