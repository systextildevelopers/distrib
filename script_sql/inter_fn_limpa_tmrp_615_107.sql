
  CREATE OR REPLACE FUNCTION "INTER_FN_LIMPA_TMRP_615_107" 
return number
is
   v_status_conexao     number;

begin
   for reg_TMRP_615_107 in (select tmrp_615_107.instancia, TMRP_615_107.sid_usuario
                        from TMRP_615_107
                        group by TMRP_615_107.instancia, TMRP_615_107.sid_usuario)
   loop
      v_status_conexao := 1;

      begin
         select decode(STATUS, 'KILLED', 0, 1)
         into v_status_conexao
         from sys.gv_$session
         where audsid > 0
           and osuser <> 'oracle'
           and sys.gv_$session.inst_id = reg_TMRP_615_107.instancia
           and sys.gv_$session.sid     = reg_TMRP_615_107.sid_usuario
           and rownum < 2;
      exception when others then
         v_status_conexao := 0;
      end;

      if v_status_conexao = 0
      then
         begin
            delete TMRP_615_107
            where TMRP_615_107.instancia   = reg_TMRP_615_107.instancia
              and TMRP_615_107.sid_usuario = reg_TMRP_615_107.sid_usuario;
         end;
      end if;
   end loop;

   return(v_status_conexao);

end inter_fn_limpa_tmrp_615_107;

 

/

exec inter_pr_recompile;

