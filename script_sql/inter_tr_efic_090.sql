
  CREATE OR REPLACE TRIGGER "INTER_TR_EFIC_090" 
   before insert or
          update of hora_inicio, hora_final
   on efic_090
   for each row

begin

   if (updating  and :new.hora_inicio is not null)
   or (inserting and :new.hora_inicio is not null)
   then
      :new.hora_inicio := to_date('16/11/1989 '||to_char(:new.hora_inicio,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;


   if (updating  and :new.hora_final is not null)
   or (inserting and :new.hora_final is not null)
   then
      :new.hora_final := to_date('16/11/1989 '||to_char(:new.hora_final,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;
end inter_tr_efic_090;
-- ALTER TRIGGER "INTER_TR_EFIC_090" ENABLE
 

/

exec inter_pr_recompile;

