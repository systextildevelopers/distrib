CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_340"
BEFORE INSERT ON OBRF_340 FOR EACH ROW
DECLARE
    nextValue number;
BEGIN
    select ID_OBRF_340.nextval into nextValue from dual;
    :new.ID := nextValue;
END INTER_TR_OBRF_340;

/

exec inter_pr_recompile;
