create or replace procedure inter_pr_exclui_aux_sped_pc(p_cod_matriz  IN NUMBER,
                                                        p_des_erro     OUT varchar2) is
--
-- Finalidade: Eliminar as tabelas auxiliares geradas para o sped fiscal
-- Autor.....: Edson Pio
-- Data......: 19/02/11
--
-- Históricos
--
-- Data    Autor    Observações
--

w_erro           EXCEPTION;
w_conta_reg      number;

BEGIN
   p_des_erro          := NULL;

   begin
      delete sped_pc_0000
      where sped_pc_0000.cod_matriz  =  p_cod_matriz;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_pc_0000 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;
   commit;

   begin
      delete sped_pc_0035
      where sped_pc_0035.cod_empresa  =  p_cod_matriz;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_pc_0035 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;
   commit;

   begin
      delete sped_pc_m_dctf
      where sped_pc_m_dctf.cod_empresa  =  p_cod_matriz;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_pc_m_dctf ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;
   commit;

   begin
      delete sped_pc_9990
      where  sped_pc_9990.cod_matriz   = p_cod_matriz;

   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_pc_9990 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;
   commit;

   begin
      delete sped_pc_0150
      where sped_pc_0150.cod_matriz         = p_cod_matriz;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_pc_0150 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;
   commit;

   begin
      delete sped_pc_0190
      where  cod_matriz = p_cod_matriz;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_pc_0190 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;
   commit;

   begin
      delete sped_pc_0200
      where  sped_pc_0200.cod_matriz = p_cod_matriz;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_pc_0200 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;
   commit;

   begin
      delete sped_pc_0400
      where  sped_pc_0400.cod_matriz = p_cod_matriz;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_pc_0400 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;
   commit;

   begin
      delete sped_pc_0450
      where  sped_pc_0450.cod_matriz   = p_cod_matriz;

   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_pc_0400 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;
   commit;

   begin
      delete sped_pc_0145
      where sped_pc_0145.cod_matriz = p_cod_matriz;
   exception
      when others then
         p_des_erro := 'Erro na exclusao da tabela sped_pc_0145 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   end;
   commit;

   begin
      delete sped_pc_p100
      where sped_pc_p100.cod_matriz = p_cod_matriz;
   exception
      when others then
         p_des_erro := 'Erro na exclusao da tabela sped_pc_p100 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   end;
   commit;

   begin   
      delete sped_pc_1900
      where sped_pc_1900.cod_matriz = p_cod_matriz;
   exception
      when others then
         p_des_erro := 'Erro na exclusao da tabela sped_pc_1900 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   end;
   commit;

   begin
      delete sped_pc_056
      where sped_pc_056.cod_matriz = p_cod_matriz;
   exception
      when others then
         p_des_erro := 'Erro na exclusao da tabela sped_pc_056 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   end;

   begin
      delete sped_pc_0019
      where sped_pc_0019.cod_matriz = p_cod_matriz;
   exception
      when others then
         p_des_erro := 'Erro na exclusao da tabela sped_pc_0019 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   end;

   begin
      delete sped_pc_hist
      where sped_pc_hist.cod_matriz = p_cod_matriz;
   exception
      when others then
         p_des_erro := 'Erro na exclusao da tabela sped_pc_hist ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   end;

   begin
      delete sped_pc_0500
      where sped_pc_0500.cod_matriz = p_cod_matriz;
   exception
      when others then
         p_des_erro := 'Erro na exclusao da tabela sped_pc_0500 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   end;

   begin
      delete sped_pc_0600
      where sped_pc_0600.cod_matriz = p_cod_matriz;
   exception
      when others then
         p_des_erro := 'Erro na exclusao da tabela sped_pc_0600 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   end;

   w_conta_reg := 0;

   for exec_nfe in (
       select sped_pc_c170.rowid from sped_pc_c170
       where  sped_pc_c170.cod_matriz   = p_cod_matriz
   )
   loop
       w_conta_reg := w_conta_reg + 1;

       begin
          delete sped_pc_c170
          where  sped_pc_c170.rowid = exec_nfe.rowid;

       EXCEPTION
          WHEN OTHERS THEN
             p_des_erro := 'Erro na exclusao da tabela sped_pc_c170 ' || Chr(10) || SQLERRM;
             RAISE W_ERRO;
       END;

       if w_conta_reg = 5000
       then
          commit;
          w_conta_reg := 0;
       end if;
   end loop;
   commit;

   w_conta_reg := 0;

   for exec_nfec in (
     select sped_pc_c100.rowid from sped_pc_c100
     where  sped_pc_c100.cod_matriz   = p_cod_matriz
   )
   loop
      w_conta_reg := w_conta_reg + 1;

       begin
          delete sped_pc_c100
          where  sped_pc_c100.rowid = exec_nfec.rowid;
       EXCEPTION
          WHEN OTHERS THEN
             p_des_erro := 'Erro na exclusao da tabela sped_pc_c100 ' || Chr(10) || SQLERRM;
             RAISE W_ERRO;
       END;

       if w_conta_reg = 1000
       then
          commit;
          w_conta_reg := 0;
       end if;
   end loop;

   COMMIT;
EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_exclui_sped ' || Chr(10) || p_des_erro;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure p_exclu_sped ' || Chr(10) || SQLERRM;
END inter_pr_exclui_aux_sped_pc;
/
exec inter_pr_recompile;
