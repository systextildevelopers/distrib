alter table inte_385
add (emis_cnpj9    number(9),
     emis_cnpj4    number(4),
	 emis_cnpj2    number(2));     
comment on column inte_385.emis_cnpj9    is 'CNPJ EMISSOR PARA TRANSPORTADORA COM DUPLICATA UNICA';
comment on column inte_385.emis_cnpj4    is 'CNPJ EMISSOR PARA TRANSPORTADORA COM DUPLICATA UNICA';
comment on column inte_385.emis_cnpj2    is 'CNPJ EMISSOR PARA TRANSPORTADORA COM DUPLICATA UNICA';
exec inter_pr_recompile;
/
