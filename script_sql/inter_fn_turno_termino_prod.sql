
  CREATE OR REPLACE FUNCTION "INTER_FN_TURNO_TERMINO_PROD" 
        (ordem    in pcpb_040.ordem_producao %type,
         estagio  in pcpb_040.ordem_producao %type,
         termino  in pcpb_040.data_termino   %type)
return number is
   v_hora_termino_date date;
   v_hora_termino      varchar2(5);
   v_turno_termino     number   (1);
begin
   select max(pcpb_040.hora_termino) into v_hora_termino_date
   from pcpb_040
   where pcpb_040.ordem_producao = ordem
     and pcpb_040.data_termino   = termino
     and pcpb_040.codigo_estagio = estagio;
   v_hora_termino := to_char(v_hora_termino_date,'hh24:mi');
   -- verifica o turno
   if  v_hora_termino >= '05:00'
   and v_hora_termino <  '13:30' then
       v_turno_termino := 1;
   end if;
   if  v_hora_termino >= '13:30'
   and v_hora_termino <  '22:30' then
       v_turno_termino := 2;
   end if;
   if  v_hora_termino >= '22:30'
   and v_hora_termino <= '23:59'
   or (v_hora_termino >= '00:00'
   and v_hora_termino <  '05:00')then
       v_turno_termino := 3;
   end if;
return(v_turno_termino);
end inter_fn_turno_termino_prod;

 

/

exec inter_pr_recompile;

