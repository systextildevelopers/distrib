insert into empr_007 (param, tipo, label, fyi_message, default_str, default_int, default_dbl, default_dat)
values ('obrf.consideraUFCte',0,'lb49400','fy07026','N',NULL,NULL,NULL);

declare tmpInt number;

cursor parametro_c is select codigo_empresa from fatu_500;

begin
  for reg_parametro in parametro_c
  loop
    select nvl(count(*),0) into tmpInt
    from fatu_500
    where fatu_500.codigo_empresa = reg_parametro.codigo_empresa;

    if(tmpInt > 0)
    then
      begin
        insert into empr_008 (codigo_empresa, param, val_str) 
        values (reg_parametro.codigo_empresa, 'obrf.consideraUFCte', 'N');
      end;
    end if;

  end loop;
  commit;
end;
/
