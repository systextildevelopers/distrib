
  CREATE OR REPLACE FUNCTION "INTER_FN_CALCULA_PERDAS" (
      p_nivel in varchar2,
      p_grupo in varchar2,
      p_cor in varchar2,
      p_projeto in varchar2,
      p_qtde_limite in number)
   return number
is
   v_tipo_perda              number(1);
   v_qtde_perda              number(5);
   v_perc_perda              number(7,2);
   v_codigo_projeto          varchar2(6);
begin
   -- Obtem o projeto, se nao foi passado como parametro
   if p_projeto = '' or p_projeto = '000000'
   then
      begin
         select basi_001.codigo_projeto
         into v_codigo_projeto
         from basi_001
         where basi_001.nivel_produto = p_nivel
           and basi_001.grupo_produto = p_grupo
           and rownum = 1;
         exception when no_data_found then
            v_codigo_projeto := '';
      end;
   else
      v_codigo_projeto := p_projeto;
   end if;

   -- Obtem o tipo de perda do projeto: 0 - combinacao - ou 1 - sequencia cor.
   begin
      select basi_001.tipo_perda
      into v_tipo_perda
      from basi_001
      where basi_001.codigo_projeto = v_codigo_projeto;
      exception when no_data_found then
         v_tipo_perda := 2;
   end;

   -- Tendo o projeto e a cor, busca da basi_029 perdas informadas.
   v_qtde_perda := 0;
   v_perc_perda := 0.00;

   if v_tipo_perda = 0
   then
      begin
         select basi_029.qtde_perda , basi_029.perc_perda
         into   v_qtde_perda        , v_perc_perda
         from basi_029
         where basi_029.codigo_projeto = v_codigo_projeto
           and basi_029.item_produto = p_cor
           and basi_029.limite_inferior <= p_qtde_limite
           and basi_029.limite_superior >= p_qtde_limite;
         exception when no_data_found then
            v_qtde_perda := 0;
            v_perc_perda := 0.00;
      end;
   else
      if v_tipo_perda = 1
      then
         begin
            select basi_029.qtde_perda , basi_029.perc_perda
            into   v_qtde_perda        , v_perc_perda
            from basi_029
            where basi_029.codigo_projeto = v_codigo_projeto
              and basi_029.item_produto = '00' || substr(p_cor,3,6) -- *
              and basi_029.limite_inferior <= p_qtde_limite
              and basi_029.limite_superior >= p_qtde_limite;
            exception when no_data_found then
               v_qtde_perda := 0;
               v_perc_perda := 0.00;
         end;
         -- * Tratamento para sequencia de cor: para a cor US0003, por exemplo
         -- a sequencia de cor seria 000003 - os quatro ultimos caracteres sao
         -- considerados, precedidos de '00'.
      end if;
   end if;

   if v_qtde_perda > 0 or v_perc_perda > 0.00
   then
      if v_qtde_perda > 0
      then return (v_qtde_perda);
      else return ((v_perc_perda * p_qtde_limite)/100);
      end if;
   else
      return (0.00);
   end if;
end;
 

/

exec inter_pr_recompile;

