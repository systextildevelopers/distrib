create table blocok_cfop (
    id             number (9)
   ,codigo_empresa number (4)
   ,cfop           varchar2 (5)
   ,blocok_250     number (1) DEFAULT 0
   ,blocok_255     number (1) DEFAULT 0
);

alter table blocok_cfop 
    add constraint pk_blocok_cfop  primary key (id);
