create or replace view vi_basi_010_basi_030_colecao as
select DISTINCT nivel_estrutura,
                grupo_estrutura,
                subgru_estrutura,
                item_estrutura,
                data_fim_disp        data_fim_disp1,
                narrativa,
                calc_disponibilidade calc_disponibilidade1,
                colecao,
                tab_col_tab,
                tab_mes_tab,
                tab_seq_tab,
                nome_usuario         nome_usuario1
from (select basi_010.nivel_estrutura,
             basi_010.grupo_estrutura,
             basi_010.subgru_estrutura,
             basi_010.item_estrutura,
             basi_010.data_fim_disp,
             basi_010.narrativa,
             basi_010.calc_disponibilidade,
             basi_030.colecao,
             pedi_095.tab_col_tab,
             pedi_095.tab_mes_tab,
             pedi_095.tab_seq_tab,
             decode(basi_010.nome_usuario,null,' ',basi_010.nome_usuario) nome_usuario
      from  basi_010, basi_030, pedi_095
      where basi_010.nivel_estrutura = basi_030.nivel_estrutura
        and basi_010.grupo_estrutura = basi_030.referencia

        and ((basi_010.nivel_estrutura  = pedi_095.nivel_estrutura
        and   basi_010.grupo_estrutura  = pedi_095.grupo_estrutura
        and   basi_010.subgru_estrutura = pedi_095.subgru_estrutura
        and   basi_010.item_estrutura   = pedi_095.item_estrutura
        and   pedi_095.nivel_preco      = 4
        and   pedi_095.val_tabela_preco > 0)

        or   (basi_010.nivel_estrutura  = pedi_095.nivel_estrutura
        and   basi_010.grupo_estrutura  = pedi_095.grupo_estrutura
        and   basi_010.subgru_estrutura = pedi_095.subgru_estrutura
        and   pedi_095.nivel_preco      = 2
        and   pedi_095.val_tabela_preco > 0)

        or   (basi_010.nivel_estrutura  = pedi_095.nivel_estrutura
        and   basi_010.grupo_estrutura  = pedi_095.grupo_estrutura
        and   pedi_095.nivel_preco      = 1
        and   pedi_095.val_tabela_preco > 0))
)

/


exec inter_pr_recompile;
