
  CREATE OR REPLACE FUNCTION "INTER_FN_CARREGA_PCPB_200" 
return varchar2 is result varchar2(250);
begin
  result := ' ';
  begin
     inter_pr_carrega_pcpb_200();
     exception
        when Others then
           result := SQLERRM;
  end;

  return(result);
end inter_fn_carrega_pcpb_200;

 

/

exec inter_pr_recompile;

