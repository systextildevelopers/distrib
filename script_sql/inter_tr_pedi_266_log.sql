create or replace trigger inter_tr_pedi_266_log 
after insert or delete or update 
on pedi_266 
for each row 
declare 
   ws_usuario_rede           varchar2(20); 
   ws_maquina_rede           varchar2(40); 
   ws_aplicativo             varchar2(20); 
   ws_sid                    number(9); 
   ws_empresa                number(3); 
   ws_usuario_systextil      varchar2(250); 
   ws_locale_usuario         varchar2(5); 
   v_nome_programa           varchar2(20); 


begin
-- Dados do usuário logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 

    begin 
       select hdoc_090.programa 
       into v_nome_programa 
       from hdoc_090
      where hdoc_090.sid       = ws_sid
        and hdoc_090.instancia = userenv('INSTANCE')
        and hdoc_090.programa  not like '%menu%'
        and hdoc_090.programa  not like  '%!_m%'escape'!'
        and rownum             = 1;
       exception when no_data_found then            
		 v_nome_programa := ws_aplicativo; 
    end; 
 
 if inserting 
 then 
    begin 
        insert into pedi_266_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           pedido_OLD,   /*8*/ 
           pedido_NEW,   /*9*/ 
           sequencia_OLD,   /*10*/ 
           sequencia_NEW,   /*11*/ 
           desconto1_OLD,   /*12*/ 
           desconto1_NEW,   /*13*/ 
           desconto2_OLD,   /*14*/ 
           desconto2_NEW,   /*15*/ 
           desconto3_OLD,   /*16*/ 
           desconto3_NEW,   /*17*/ 
           desc_calc_OLD,   /*18*/ 
           desc_calc_NEW    /*19*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.pedido, /*9*/   
           0,/*10*/
           :new.sequencia, /*11*/   
           0,/*12*/
           :new.desconto1, /*13*/   
           0,/*14*/
           :new.desconto2, /*15*/   
           0,/*16*/
           :new.desconto3, /*17*/   
           0,/*18*/
           :new.desc_calc /*19*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into pedi_266_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           pedido_OLD, /*8*/   
           pedido_NEW, /*9*/   
           sequencia_OLD, /*10*/   
           sequencia_NEW, /*11*/   
           desconto1_OLD, /*12*/   
           desconto1_NEW, /*13*/   
           desconto2_OLD, /*14*/   
           desconto2_NEW, /*15*/   
           desconto3_OLD, /*16*/   
           desconto3_NEW, /*17*/   
           desc_calc_OLD, /*18*/   
           desc_calc_NEW  /*19*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.pedido,  /*8*/  
           :new.pedido, /*9*/   
           :old.sequencia,  /*10*/  
           :new.sequencia, /*11*/   
           :old.desconto1,  /*12*/  
           :new.desconto1, /*13*/   
           :old.desconto2,  /*14*/  
           :new.desconto2, /*15*/   
           :old.desconto3,  /*16*/  
           :new.desconto3, /*17*/   
           :old.desc_calc,  /*18*/  
           :new.desc_calc  /*19*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into pedi_266_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           pedido_OLD, /*8*/   
           pedido_NEW, /*9*/   
           sequencia_OLD, /*10*/   
           sequencia_NEW, /*11*/   
           desconto1_OLD, /*12*/   
           desconto1_NEW, /*13*/   
           desconto2_OLD, /*14*/   
           desconto2_NEW, /*15*/   
           desconto3_OLD, /*16*/   
           desconto3_NEW, /*17*/   
           desc_calc_OLD, /*18*/   
           desc_calc_NEW /*19*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.pedido, /*8*/   
           0, /*9*/
           :old.sequencia, /*10*/   
           0, /*11*/
           :old.desconto1, /*12*/   
           0, /*13*/
           :old.desconto2, /*14*/   
           0, /*15*/
           :old.desconto3, /*16*/   
           0, /*17*/
           :old.desc_calc, /*18*/   
           0 /*19*/
         );    
    end;    
 end if;    
end inter_tr_pedi_266_log;

/

exec inter_pr_recompile;
