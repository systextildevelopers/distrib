create or replace PACKAGE ST_PCK_ADIANTAMENTO AS

    function exists_adiantamento_imp(p_num_importacao IN varchar2,  p_cgc_9 IN number, 
                                     p_cgc_4 IN number,             p_cgc_2 IN number,
                                     p_nr_adiantam OUT number) return boolean;

    function cria_adiantamento(p_cgc9 IN number,                p_cgc4 IN number,            p_cgc2 IN number,       p_empresa IN number, 
                               p_centro_custo IN number,        p_nome IN varchar2,          p_valor IN number,      p_data_contrato IN date, 	
                               p_data_vencimento IN date,       p_cod_portador IN number,    p_moeda IN varchar2,    p_num_importacao IN varchar2,   
                               P_origem_adiantam IN varchar2,   v_num_adiantam in out number, p_tipo_titulo IN number) return varchar2;
                               
    function cria_adiantamento_cf(p_cgc9 IN number,                p_cgc4 IN number,            p_cgc2 IN number,       p_empresa IN number, 
                               p_centro_custo IN number,        p_nome IN varchar2,          p_valor IN number,      p_data_contrato IN date, 	
                               p_data_vencimento IN date,       p_cod_portador IN number,    p_moeda IN varchar2,    p_num_importacao IN varchar2,   
                               P_origem_adiantam IN varchar2,   v_num_adiantam in out number, p_tipo_titulo IN number, p_tipo_adiantam IN number) return varchar2;
    
    function valida_saldo_adiantamento( p_tipo_adiantamento IN number,  p_valor_adiantamento IN number,
                                        p_id_importacao IN varchar2,    p_nr_adiantamento IN number) return varchar2;
END ST_PCK_ADIANTAMENTO;
