create table basi_056
(unid_med_trib varchar2(10) default ' ',
 descr_unid_med_trib varchar2(100));
 
 comment on column basi_056.unid_med_trib is 'Unidade de medida tributaria criado peloa RFB';
 comment on column basi_056.descr_unid_med_trib is 'Descri��o da Unidade de medida tributaria criado peloa RFB';
 
 alter table basi_056 add constraint obrf_056_pk primary key (unid_med_trib);

 exec inter_pr_recompile;
 /
