create table estq_048 
(nr_imagem number(9),
 data_imagem date,
 cod_deposito number(3), 
 nivel varchar2(1),
 dias_retro_imagem number(3),
 qtde_itens_prod_movi number(3),
 qtde_itens_prod_nao_movi number(3),
 qtde_itens_conta_estoque number(3),
 qtde_itens_unidade number(3),
 qtde_itens_prod_especifico number(3),
 dias_prod_movi number(3),
 dias_prod_nao_movi number(3),
 contas_estoque varchar2(100),
 unidades varchar2(100),
 produtos_especificos varchar2(100),
 constraint estq_048_pk primary key (nr_imagem)
);

comment on column estq_048.nr_imagem is 'Identificador dos parâmetros';
comment on column estq_048.data_imagem is 'Data da imagem do estoque';
comment on column estq_048.cod_deposito is 'Código do depósito';
comment on column estq_048.nivel is 'Nível dos produtos';
comment on column estq_048.dias_retro_imagem is 'Dias retroativos para considerar imagens';
comment on column estq_048.qtde_itens_prod_movi is 'Quantidade de produtos movimentados';
comment on column estq_048.qtde_itens_prod_nao_movi is 'Quantidade de produtos não movimentados';
comment on column estq_048.qtde_itens_conta_estoque is 'Quantidade de produtos na conta de estoque';
comment on column estq_048.qtde_itens_unidade is 'Quantidade de produtos na unidade de medida';
comment on column estq_048.qtde_itens_prod_especifico is 'Quantidade de produtos especificos';
comment on column estq_048.dias_prod_movi is 'Numero de dias com movimentação de produtos';
comment on column estq_048.dias_prod_nao_movi is 'Numero de dias sem movimentação de produtos';
comment on column estq_048.contas_estoque is 'Contas de estoques';
comment on column estq_048.unidades is 'Unidades de medidas';
comment on column estq_048.produtos_especificos is 'produtos especificos';

comment on table estq_048 is 'Parâmetros do inventário cíclico de produtos';

/
