create or replace trigger "INTER_TR_PCPC_045_4"
  before insert on pcpc_045
  for each row

declare
   hinit1     date;
   hfimt1     date;
   hinit2     date;
   hfimt2     date;
   hinit3     date;
   hfimt3     date;
   hinit4     date;
   hfimt4     date;
begin

   if :new.turno_producao = 0
   then begin
      select basi_185.horaini_t1,  basi_185.horafim_t1,
             basi_185.horaini_t2,  basi_185.horafim_t2,
             basi_185.horaini_t3,  basi_185.horafim_t3,
             basi_185.horaini_t4,  basi_185.horafim_t4
      into   hinit1,               hfimt1,
             hinit2,               hfimt2,
             hinit3,               hfimt3,
             hinit4,               hfimt4
      from basi_185
      where basi_185.centro_custo = 0;


      if  to_CHAR(sysdate,'hh24:mi') >= to_CHAR(hinit4,'hh24:mi')
      and to_CHAR(sysdate,'hh24:mi') <= to_CHAR(hfimt4,'hh24:mi')
      then :new.turno_producao := 4;
      end if;

      if  (to_CHAR(sysdate,'hh24:mi') >= to_CHAR(hinit3,'hh24:mi')
       and to_CHAR(sysdate,'hh24:mi') <= '23:59')
         or (to_CHAR(sysdate,'hh24:mi') >= '00:00'
       and to_CHAR(sysdate, 'hh24:mi') <= to_CHAR(hfimt3,'hh24:mi'))
      then  :new.turno_producao := 3;
      end if;

      if  to_CHAR(sysdate,'hh24:mi') >= to_CHAR(hinit2,'hh24:mi')
      and to_CHAR(sysdate,'hh24:mi') <= to_CHAR(hfimt2,'hh24:mi')
      then  :new.turno_producao := 2;
      end if;

      if  to_CHAR(sysdate,'hh24:mi') >= to_CHAR(hinit1,'hh24:mi')
      and to_CHAR(sysdate,'hh24:mi') <= to_CHAR(hfimt1,'hh24:mi')
      then  :new.turno_producao := 1;
      end if;

      if :new.turno_producao is null
      then :new.turno_producao := 0;
      end if;

   end;
   end if;

   if :new.data_producao > sysdate then
       raise_application_error(-20000,'ATENCAO! A data de produção é maior que a data corrente.');
   end if;
   
end inter_tr_pcpc_045_4;
