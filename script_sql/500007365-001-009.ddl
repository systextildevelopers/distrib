create table cert_003_LOG ( 
  TIPO_OCORR                  VARCHAR2(1) default '' null,
  DATA_OCORR                  DATE null,
  HORA_OCORR                  DATE null,
  USUARIO_REDE                VARCHAR2(20) default '' null,
  MAQUINA_REDE                VARCHAR2(40) default '' null,
  APLICACAO                   VARCHAR2(20) default '' null,
  USUARIO_SISTEMA             VARCHAR2(20) default '' null,
  NOME_PROGRAMA               VARCHAR2(20) default '' null,
  codigo_divisao_producao_OLD NUMBER(4) ,  
  codigo_divisao_producao_NEW NUMBER(4) , 
  codigo_certificacao_OLD     NUMBER(3) ,  
  codigo_certificacao_NEW     NUMBER(3) , 
  data_inicio_OLD             DATE,  
  data_inicio_NEW             DATE, 
  data_fim_OLD                DATE, 
  data_fim_NEW                DATE 
);
/
exec inter_pr_recompile;
