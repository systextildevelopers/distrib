-- Add/modify columns 
alter table BASI_040 add (qtde_pecas   NUMBER(3));

alter table BASI_040 modify qtde_pecas default 0;

-- Add comments to the columns 
comment on column BASI_040.qtde_pecas
  is 'Quantidade de pe�as para ter o consumo na unidade de medida do produto.';

exec inter_pr_recompile;
/

declare
nro_registro number;
cursor basi_040_c is
   select f.rowid from basi_040 f
                  where qtde_pecas is null;

begin
   nro_registro := 0;

   for reg_basi_040_c in basi_040_c
   loop
      begin
         update basi_040
         set   basi_040.qtde_pecas     = 0
         where basi_040.qtde_pecas     is null
           and basi_040.rowid          = reg_basi_040_c.rowid; 
      end;
      
      nro_registro := nro_registro + 1;
      
      if nro_registro > 1000
      then
         nro_registro := 0;
         commit;
      end if;
   end loop; 

   commit;
end;
/

exec inter_pr_recompile;
/
