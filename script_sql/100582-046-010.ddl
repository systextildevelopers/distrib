alter table pcpc_880
add situacao_estagio_solicitacao number(1) default 0;
/
comment on column pcpc_880.situacao_estagio_solicitacao is '0 - Aguardando est�gio anterior, 1 - Pronta para envio, 2 - Enviado para terceiro, 3 - Retornado do terceiro';
/
exec inter_pr_recompile;
/

