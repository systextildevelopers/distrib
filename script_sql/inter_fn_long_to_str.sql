
  CREATE OR REPLACE FUNCTION "INTER_FN_LONG_TO_STR" (pedido in number) RETURN VARCHAR2 IS
BEGIN
   DECLARE
   long_var LONG;
   var_var  VARCHAR2(4000);

   BEGIN

     select p.observacao into long_var
       from pedi_100 p
      where p.pedido_venda = pedido;
      var_var := substr(long_var,1,4000);
      RETURN var_var;
   END;
END inter_fn_long_to_str;
 

/

exec inter_pr_recompile;

