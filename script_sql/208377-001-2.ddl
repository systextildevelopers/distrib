DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_l425'
                      and   hdoc_033.usu_prg_cdusu <> 'INTERSYS';
begin
   for reg_programa in programa
   loop
     begin
        INSERT INTO hdoc_033
        ( usu_prg_cdusu, usu_prg_empr_usu, 
          programa,      nome_menu, 
          item_menu,     ordem_menu, 
          incluir,       modificar, 
          excluir,       procurar)
        VALUES
        ( reg_programa.usu_prg_cdusu, reg_programa.usu_prg_empr_usu, 
          'pedi_f428',                'pedi_menu', 
          0,                          1, 
          'S',                        'S', 
          'S',                        'S'); 
     exception when OTHERS then null;
     end;  
     commit;
   end loop;
end;

/

exec inter_pr_recompile;
