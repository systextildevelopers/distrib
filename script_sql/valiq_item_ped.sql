
  CREATE OR REPLACE FUNCTION "VALIQ_ITEM_PED" 
        (pedido_inf in pedi_110.pedido_venda%type,
         sqitem_inf in pedi_110.seq_item_pedido%type)
RETURN number
IS
   val_liq_item number;
   perc_desc_cap1 number;
   perc_desc_cap2 number;
   perc_desc_cap3 number;
   val_calc_1     number;
   val_calc_2     number;
   val_calc_3     number;
begin

   select nvl(((pedi_110.valor_unitario-(pedi_110.valor_unitario * pedi_110.percentual_desc / 100.00))),0)
   into val_liq_item from pedi_110, pedi_100
   where pedi_100.pedido_venda    = pedi_110.pedido_venda
     and pedi_110.pedido_venda    = pedido_inf
     and pedi_110.seq_item_pedido = sqitem_inf;

   select nvl(pedi_100.desconto1,0) into perc_desc_cap1
   from pedi_100
   where pedi_100.pedido_venda    = pedido_inf;

   select nvl(pedi_100.desconto2,0) into perc_desc_cap2
   from pedi_100
   where pedi_100.pedido_venda    = pedido_inf;

   select nvl(pedi_100.desconto3,0) into perc_desc_cap3
   from pedi_100
   where pedi_100.pedido_venda    = pedido_inf;

   val_calc_1:=val_liq_item-(val_liq_item*(perc_desc_cap1/100));
   val_calc_2:=val_calc_1-(val_calc_1*(perc_desc_cap2/100));
   val_calc_3:=val_calc_2-(val_calc_2*(perc_desc_cap3/100));

   return(val_calc_3);

end valiq_item_ped;

 

/

exec inter_pr_recompile;

