-- -----------------------------------------------------
-- Table OBRF_760
-- -----------------------------------------------------
alter table obrf_760 
add (nf_por_op number(1) default 2);
   
comment on column obrf_760.nf_por_op is 'Controla emiss�o de nota de sa�da por ordem de corte/Est�gio 1-SIM/2-N�O';

-- -----------------------------------------------------
-- Table OBRF_779
-- -----------------------------------------------------
CREATE TABLE OBRF_779 (
  nota    NUMBER(9) NOT NULL,
  serie   NUMBER(3) NOT NULL,
  op      NUMBER(9) NOT NULL,
  estagio NUMBER(2) NOT NULL,
  PRIMARY KEY (nota,serie,op)
  );

comment on table OBRF_779 is 'Tabela para registrar o relacionamento dos itens da nota emitida com a ordem de corte utilizada para a gera��o da nota ';

/
