alter table basi_010 add (variante      varchar2(10),
                          obs_cor_tag   varchar2(100));

comment on column basi_010.variante    IS 'Número da Variante (vem do Centric PLM)';
comment on column basi_010.obs_cor_tag IS 'Observação da Cor (vem do Centric PLM)';

exec inter_pr_recompile;
