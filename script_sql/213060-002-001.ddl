create table fndc_001
(  CNPJ9                       NUMBER(9) default 0 not null,
  CNPJ4                        NUMBER(4) default 0 not null,
  CNPJ2                        NUMBER(2) default 0 not null, 
  STATUS_ATIVO                 NUMBER(1) default 0
);

-- Add comments to the table 
comment on table fndc_001  is 'Clientes do Fundo Fidic';

-- Add comments to the columns 
comment on column fndc_001.STATUS_ATIVO   is 'Status do cliente no fidic 0 - inativo ou 1 - ativo.';

alter table fndc_001 add constraint pk_fndc_001 primary key (CNPJ9, CNPJ4, CNPJ2);
