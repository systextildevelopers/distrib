alter table FATU_503 
add (opcao_controle_pesagem number(1) default 0);

comment on column fatu_503.opcao_controle_pesagem is 'Opcao para controle de pesagem: 0 - Todos estagios / 1 - Ultimo estagio';

/
