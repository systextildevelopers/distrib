create table fndc_004
( FND_EMPRESA                NUMBER(3)    default 0 not null,
FND_TIPO	                 NUMBER(2)    default 0 not null,
FND_TIPO_FIDC	           NUMBER(2)    default 0);
 
comment on table fndc_004 is 'Tabela de parametros para criacao dos tipos de titulos de controle do Fundo ';
 
comment on column fndc_004.FND_EMPRESA    is 'Codigo da empresa no sistema';
 
comment on column fndc_004.FND_TIPO   is 'Tipo titulo origem';
 
comment on column fndc_004.FND_TIPO_FIDC is 'Tipo titulo de acompanhamento de credito'; 

/
exec inter_pr_recompile;
