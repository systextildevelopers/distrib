alter table basi_185
modify  custo_minuto number(19,6);

alter table basi_185
modify  custo_minuto_previsto  number(19,6);

alter table basi_185
modify custo_minuto_estimado  number(19,6);


alter table rcnb_092
modify  custo_minuto number(19,6);

alter table rcnb_092
modify  custo_minuto_previsto  number(19,6);

alter table rcnb_092
modify custo_minuto_estimado  number(19,6);

exec inter_pr_recompile;
