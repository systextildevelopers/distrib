alter table cont_500
add form_apur varchar2(1) default 'A';

comment on column cont_500.form_apur is 'Forma de apura��o cont�bil, A-Anual ou T-Trimetral';

exec inter_pr_recompile;
