create or replace function inter_fn_atributos(p_nivel in varchar2,      p_grupo in varchar2,
                                              p_tipo_registro number,   p_nr_solicitacao number, p_subgru_estrutura number,
                                              p_grupo_estrutura number, p_empresa_rel number,    p_relatorio_rel in varchar2)
return number is


   cursor rcnb_atrib is
      select count(rcnb_060.item_estrutura_str) qtde_atrib,
             rcnb_060.nivel_estrutura_str,
             rcnb_060.grupo_estrutura_str,
             rcnb_060.subgru_estrutura_str,
             rcnb_060.item_estrutura_str
        from rcnb_060
       where rcnb_060.tipo_registro     = p_tipo_registro
         and rcnb_060.nr_solicitacao    = p_nr_solicitacao
         and rcnb_060.subgru_estrutura  = p_subgru_estrutura
         and rcnb_060.grupo_estrutura   = p_grupo_estrutura
         and rcnb_060.empresa_rel       = p_empresa_rel
         and rcnb_060.relatorio_rel     = p_relatorio_rel
       group by rcnb_060.nivel_estrutura_str,  rcnb_060.grupo_estrutura_str,
                rcnb_060.subgru_estrutura_str, rcnb_060.item_estrutura_str;


   cursor rcnb_conteudo_atrib(v_familia varchar2, v_grupo varchar2, v_subgrupo varchar2, v_atributo varchar2) is
      select rcnb_060.nivel_estrutura_str,  rcnb_060.grupo_estrutura_str,
             rcnb_060.subgru_estrutura_str, rcnb_060.item_estrutura_str,
             rcnb_060.descricao2,           rcnb_060.descricao
        from rcnb_060
       where rcnb_060.tipo_registro       = p_tipo_registro
         and rcnb_060.nr_solicitacao      = p_nr_solicitacao
         and rcnb_060.subgru_estrutura    = p_subgru_estrutura
         and rcnb_060.grupo_estrutura     = p_grupo_estrutura
         and rcnb_060.empresa_rel         = p_empresa_rel
         and rcnb_060.relatorio_rel       = p_relatorio_rel
         and rcnb_060.nivel_estrutura_str  = v_familia
         and rcnb_060.grupo_estrutura_str  = v_grupo
         and rcnb_060.subgru_estrutura_str = v_subgrupo
         and rcnb_060.item_estrutura_str   = v_atributo;

/* 0 - variavel iniciada
   1 - registro valido para todos atributos
   2 - nao achou um dos atributos
*/
v_return number(3);
v_returno number(3);
v_conta_atributo number;
begin
  v_return  := 0;
  v_returno := 0;
  v_conta_atributo := 0;

  for reg_atrib in rcnb_atrib
  loop
    v_conta_atributo := reg_atrib.qtde_atrib;

    if v_returno = 2
    then
      v_conta_atributo := 1;
    end if;

    for reg_cont_atrib in rcnb_conteudo_atrib(reg_atrib.nivel_estrutura_str,  reg_atrib.grupo_estrutura_str,
                                              reg_atrib.subgru_estrutura_str, reg_atrib.item_estrutura_str)
    loop
       if ((v_returno < 2 and v_conta_atributo = 1) or
           (v_conta_atributo >= 2))then
         begin
           select 1
             into v_returno
             from basi_544
            where cast(basi_544.codigo_atributo as varchar(6)) = reg_cont_atrib.item_estrutura_str
              and basi_544.conteudo_atributo        = reg_cont_atrib.descricao
              and substr(basi_544.chave_acesso,1,1) = p_nivel
              and substr(basi_544.chave_acesso,2,5) = p_grupo
              and rownum = 1;
         exception
           when no_data_found then
            v_returno := 2;
           --raise_application_error(-20000, reg_atrib.item_estrutura_str ||'; '|| reg_atrib.descricao ||'; '|| p_nivel ||'; '|| p_grupo ||'; '|| p_tipo_registro ||'; '||p_nr_solicitacao ||'; '|| p_subgru_estrutura ||'; '|| p_grupo_estrutura||'; '||p_empresa_rel ||'; '|| p_relatorio_rel);
         end;

         if v_returno = 1 then
           v_conta_atributo := 0;
         end if;
       end if;
     end loop;
     v_conta_atributo := 0;
  end loop;

  v_return := v_returno;
  return(v_return);

end inter_fn_atributos;
/

exec inter_pr_recompile;
