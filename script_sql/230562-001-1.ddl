alter table pedi_100 
add status_expedicao_adps number(1);

alter table pedi_100 modify 
status_expedicao_adps default 0;

update pedi_100 
set status_expedicao_adps = 0;

commit;
