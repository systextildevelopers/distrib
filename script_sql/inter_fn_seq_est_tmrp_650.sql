
  CREATE OR REPLACE FUNCTION "INTER_FN_SEQ_EST_TMRP_650" (
     p_tipo_alocacao           in number,
     p_tipo_recurso            in number,
     p_ordem_planejamento      in number,
     p_pedido_venda            in number)

  return number
is
  -- Declara as variaveis para a extracao do tag;
  v_sequencia                        number;

  v_data_inicial_estagio_post        tmrp_650.data_inicio%type;
  v_data_final_estagio_post          tmrp_650.data_termino%type;
  v_data_inicial_calculada           tmrp_650.data_inicio%type;
  v_data_final_calculada             tmrp_650.data_termino%type;
  v_data_inicial_estagio_atual       tmrp_650.data_inicio%type;
  v_data_final_estagio_atual         tmrp_650.data_termino%type;
  v_data_consulta                    tmrp_650.data_termino%type;
  v_codigo_recurso                   tmrp_650.codigo_recurso%type;
  v_codigo_recurso_post              tmrp_650.codigo_recurso%type;

  v_tempo_disponivel_660             tmrp_660.tempo_disponivel%type;
  v_qtde_recursos_660                tmrp_660.qtde_recursos%type;
  v_tempo_disponivel_atual           tmrp_660.tempo_disponivel%type;
  v_qtde_recursos_atual              tmrp_660.qtde_recursos%type;
  v_data_inicial_660                 tmrp_660.data_recurso%type;
  v_data_final_660                   tmrp_660.data_recurso%type;

  v_data_emb_pedido_reserva          date;
  v_dias_producao_estagio_post       number;
  v_dias_producao_estagio_atual      number;
  v_tempo_producao_estagio_atual     number;
  v_tempo_disp_estagio_atual         number;
  v_tempo_disponivel_recurso         number;
  v_saldo_tempo_disponivel           number;
  v_contador_dia                     number;
  v_contador_tmrp_650                number;
  v_contador_tmrp_660                number;
  v_conta_loop                       number;
begin
  v_sequencia := 0;

  for r0 in (select tmrp_650.seq_ordem_trabalho
             from tmrp_650
             where tmrp_650.tipo_alocacao  = p_tipo_alocacao
               and tmrp_650.tipo_recurso   = p_tipo_recurso
                 and (((tmrp_650.ordem_trabalho  in (select pcpb_100.ordem_producao
                                                     from pcpb_100
                                                     where pcpb_100.ordem_agrupamento = p_ordem_planejamento)
                  or tmrp_650.ordem_trabalho = p_ordem_planejamento)
                 and p_tipo_alocacao = 2)
                  or (tmrp_650.ordem_trabalho = p_ordem_planejamento
                 and p_tipo_alocacao <> 2))
               and tmrp_650.pedido_venda   = p_pedido_venda
             group by tmrp_650.seq_ordem_trabalho)
  loop
     v_sequencia := 0;

     for r1 in (select m007.*, t650.numero_id
                from tmrp_650 t650, mqop_007 m007
                where t650.tipo_alocacao  = p_tipo_alocacao
                  and t650.tipo_recurso   = p_tipo_recurso
                  and (((t650.ordem_trabalho  in (select pcpb_100.ordem_producao
                                                    from pcpb_100
                                                    where pcpb_100.ordem_agrupamento = p_ordem_planejamento)
                  or    t650.ordem_trabalho   = p_ordem_planejamento)
                  and  p_tipo_alocacao        = 2)
                  or   (t650.ordem_trabalho   = p_ordem_planejamento
                  and   p_tipo_alocacao       <> 2))
                  and t650.pedido_venda       = p_pedido_venda
                  and m007.codigo_estagio     = t650.codigo_estagio
                  and m007.area_producao      = t650.nivel_produto
                  and t650.seq_ordem_trabalho = r0.seq_ordem_trabalho
                order by m007.area_producao desc,
                         m007.sequencia_planejamento asc)
     loop
        v_sequencia := v_sequencia + 1;

        update tmrp_650
           set tmrp_650.seq_estagio_ordem = v_sequencia
        where  tmrp_650.numero_id = r1.numero_id;
     end loop;
  end loop;

  -- Encontra a sequencia do estagio critico da ordem
  for reg_tmrp_650_prod in (select distinct tmrp_650.nivel_produto,      tmrp_650.grupo_produto,
                                            tmrp_650.seq_estagio_ordem,  tmrp_650.codigo_estagio
                            from tmrp_650
                            where tmrp_650.tipo_alocacao  = 0 --Somente sera feito para Planejamento
                              and tmrp_650.tipo_recurso   = p_tipo_recurso
                              and tmrp_650.ordem_trabalho = p_ordem_planejamento
                              and tmrp_650.pedido_venda   = p_pedido_venda
                              and tmrp_650.codigo_estagio = (select basi_170.estagio_critico
                                                             from basi_170
                                                             where area_producao = 1
                                                              and rownum = 1)
                              and tmrp_650.nivel_produto  = '1')
  loop
      -- Encontrara a data da costura
      if reg_tmrp_650_prod.seq_estagio_ordem > 0
      then

         v_contador_tmrp_650 := 1;

         for reg_tmrp_650 in (select * from tmrp_650
                              where tmrp_650.tipo_alocacao      = p_tipo_alocacao
                                and tmrp_650.tipo_recurso       = p_tipo_recurso
                                and tmrp_650.ordem_trabalho     = p_ordem_planejamento
                                and tmrp_650.pedido_venda       = p_pedido_venda
                                and tmrp_650.nivel_produto      = reg_tmrp_650_prod.nivel_produto
                                and tmrp_650.grupo_produto      = reg_tmrp_650_prod.grupo_produto
                                and tmrp_650.seq_estagio_ordem >= reg_tmrp_650_prod.seq_estagio_ordem  --- Sequencia do estagio critico
                              order by tmrp_650.seq_estagio_ordem desc)
         loop

            v_codigo_recurso := reg_tmrp_650.codigo_estagio;

            if v_contador_tmrp_650 = 1
            then
              -- Buscando a data de embarque do pedido de venda ou pedido de reserva
              if reg_tmrp_650.tipo_reserva = 0
              then
                 begin
                    select pedi_100.data_entr_venda into v_data_emb_pedido_reserva
                    from pedi_100
                    where pedi_100.pedido_venda = p_pedido_venda;
                    exception
                       when others then
                          v_data_emb_pedido_reserva := trunc(sysdate,'DD');
                 end;
              else
                 begin
                    select tmrp_640.data_embarque_cli into v_data_emb_pedido_reserva
                    from tmrp_640
                    where tmrp_640.numero_reserva = p_pedido_venda;
                    exception
                       when others then
                          v_data_emb_pedido_reserva := trunc(sysdate,'DD');
                 end;

              end if;

               v_data_inicial_estagio_post  := trunc(v_data_emb_pedido_reserva,'DD');
               v_data_final_estagio_post    := trunc(v_data_emb_pedido_reserva,'DD');
               v_contador_tmrp_650          := 0;
               v_dias_producao_estagio_post :=1;
            else
               begin
                  select count(*)
                  into v_dias_producao_estagio_post
                  from tmrp_660
                  where tmrp_660.tipo_alocacao  = 0 --- Fixo Estagios para o tipo planejamento
                    and tmrp_660.tipo_recurso   = 6 --- Fixo Estagios
                    and tmrp_660.codigo_recurso = v_codigo_recurso_post
                    and tmrp_660.data_recurso   >= v_data_inicial_estagio_post
                    and tmrp_660.data_recurso   <= v_data_final_estagio_post;
                  exception
                     when others then
                        v_dias_producao_estagio_post := 1;
               end;
            end if;

            if reg_tmrp_650.qtde_recursos > 0
            then
               v_tempo_producao_estagio_atual :=   ((reg_tmrp_650.minutos_unitario * reg_tmrp_650.quantidade) / reg_tmrp_650.qtde_recursos);
            else
               v_tempo_producao_estagio_atual :=   (reg_tmrp_650.minutos_unitario * reg_tmrp_650.quantidade);
            end if;

            begin
               select tmrp_660.tempo_disponivel , tmrp_660.qtde_recursos
               into v_tempo_disponivel_atual,     v_qtde_recursos_atual
               from tmrp_660
               where tmrp_660.tipo_alocacao  = 0 --- Fixo Estagios para o tipo planejamento
                 and tmrp_660.tipo_recurso   = 6 --- Fixo estagios
                 and tmrp_660.codigo_recurso = v_codigo_recurso
                 and tmrp_660.data_recurso   = v_data_inicial_estagio_post;
               exception
                  when others then
                     v_tempo_disponivel_atual := 0;
                     v_qtde_recursos_atual    := 0;
            end;

            if v_qtde_recursos_atual > 0
            then
               v_tempo_disp_estagio_atual := v_tempo_disponivel_atual / v_qtde_recursos_atual;
            else
               v_tempo_disp_estagio_atual := v_tempo_disponivel_atual;
            end if;

            if v_tempo_disp_estagio_atual > 0 and v_tempo_producao_estagio_atual > 0
            then
               v_dias_producao_estagio_atual := ceil((v_tempo_producao_estagio_atual / v_tempo_disp_estagio_atual));
            else
               v_dias_producao_estagio_atual := 1;
            end if;

            if v_tempo_disp_estagio_atual > 0.000
            then
               if v_dias_producao_estagio_atual >= v_dias_producao_estagio_post
               then
                  v_data_inicial_calculada := trunc((v_data_inicial_estagio_post - reg_tmrp_650.lead_time - v_dias_producao_estagio_atual - 1),'DD');
                  v_data_final_calculada   := trunc((v_data_inicial_calculada  + v_dias_producao_estagio_atual + reg_tmrp_650.lead_time),'DD');

                  -- Verifica se a data final e um dia util. Se nao for entao encontra o primeiro dia util anterior a este.
                  -- Isto por que a data final sera a base para encontrar a data inicial com base na disponibilidade
                  select count(*) into v_contador_tmrp_660
                  from tmrp_660
                  where tmrp_660.tipo_alocacao  = 0 --- Fixo Estagios para o tipo planejamento
                    and tmrp_660.tipo_recurso   = 6 --- Fixo Estagios
                    and tmrp_660.codigo_recurso = v_codigo_recurso
                    and tmrp_660.data_recurso   = v_data_final_calculada;

                  if v_contador_tmrp_660 = 0
                  then
                     begin
                        select max(tmrp_660.data_recurso) into v_data_final_660
                        from tmrp_660
                        where tmrp_660.tipo_alocacao  = 0 --- Fixo Estagios para o tipo planejamento
                          and tmrp_660.tipo_recurso   = 6 --- Fixo Estagios
                          and tmrp_660.codigo_recurso = v_codigo_recurso
                          and tmrp_660.data_recurso   < v_data_final_calculada;
                        exception
                           when others then
                              v_data_final_660 := null;
                      end;

                      if v_data_final_660 is not null
                      then
                         v_data_final_calculada := trunc(v_data_final_660,'DD');
                      end if;
                  end if;

                  v_saldo_tempo_disponivel := v_tempo_producao_estagio_atual;
                  v_data_consulta          := trunc(v_data_final_calculada,'DD');
                  v_contador_dia           := 0;

                  -- Logica de contador do loop, foi incluida para nao gerar estouro no planejamento
                  -- somente considerando uma quantidade de 999 dias.
                  -- Erro encontrado no cliente TDV e corrigido por Neylor / Comin e Agnaldo
                  v_conta_loop := 0;

                  while v_saldo_tempo_disponivel > 0.000 and v_conta_loop < 999
                  loop
                     v_conta_loop := v_conta_loop + 1;

                     begin
                        select tmrp_660.tempo_disponivel , tmrp_660.qtde_recursos
                        into v_tempo_disponivel_660,        v_qtde_recursos_660
                        from tmrp_660
                        where tmrp_660.tipo_alocacao  = 0 --- Fixo Estagios para o tipo planejamento
                          and tmrp_660.tipo_recurso   = 6 --- Fixo Estagios
                          and tmrp_660.codigo_recurso = v_codigo_recurso
                          and tmrp_660.data_recurso   = v_data_consulta;
                        exception
                           when others then
                              v_tempo_disponivel_660 := 0;
                              v_qtde_recursos_660    := 0;
                     end;

                     if  v_qtde_recursos_660 > 0
                     then
                        v_tempo_disponivel_recurso :=  v_tempo_disponivel_660 / v_qtde_recursos_660;
                     else
                        v_tempo_disponivel_recurso :=  v_tempo_disponivel_660 ;
                     end if;

                     if v_tempo_disponivel_recurso > 0
                     then
                        v_contador_dia           := 1;
                        v_data_inicial_calculada := trunc(v_data_consulta,'DD');
                        v_saldo_tempo_disponivel := v_saldo_tempo_disponivel - v_tempo_disponivel_recurso;
                     end if;

                     v_data_consulta := trunc((v_data_consulta -1),'DD');
                  end loop;

                  if v_contador_dia > 0
                  then
                     v_data_inicial_estagio_atual := trunc((v_data_inicial_calculada - reg_tmrp_650.lead_time),'DD');
                     v_data_final_estagio_atual   := trunc(v_data_final_calculada,'DD');
                  else
                     v_data_inicial_estagio_atual := trunc(v_data_inicial_calculada,'DD');
                     v_data_final_estagio_atual   := trunc(v_data_final_calculada,'DD');
                  end if;
               else
                  v_data_inicial_calculada := trunc((v_data_final_estagio_post - reg_tmrp_650.lead_time - 1),'DD');
                  v_data_final_calculada   := trunc((v_data_inicial_calculada  + v_dias_producao_estagio_atual + reg_tmrp_650.lead_time),'DD');

                  -- Verifica se a data final e um dia util. Se nao for entao encontra o primeiro dia util anterior a este.
                  -- Isto por que a data final sera a base para encontrar a data inicial com base na disponibilidade
                  select count(*) into v_contador_tmrp_660
                  from tmrp_660
                  where tmrp_660.tipo_alocacao  = 0 --- Fixo Estagios para o tipo planejamento
                    and tmrp_660.tipo_recurso   = 6 --- Fixo Estagios
                    and tmrp_660.codigo_recurso = v_codigo_recurso
                    and tmrp_660.data_recurso   = v_data_final_calculada;

                  if v_contador_tmrp_660 = 0
                  then
                     begin
                        select max(tmrp_660.data_recurso) into v_data_final_660
                        from tmrp_660
                        where tmrp_660.tipo_alocacao  = 0 --- Fixo Estagios para o tipo planejamento
                          and tmrp_660.tipo_recurso   = 6 --- Fixo estagios
                          and tmrp_660.codigo_recurso = v_codigo_recurso
                          and tmrp_660.data_recurso   < v_data_final_calculada;
                        exception
                           when others then
                              v_data_final_660 := null;
                      end;

                      if v_data_final_660 is not null
                      then
                         v_data_final_calculada := trunc(v_data_final_660,'DD');
                      end if;
                  end if;

                  v_saldo_tempo_disponivel := v_tempo_producao_estagio_atual;
                  v_data_consulta          := v_data_final_calculada;
                  v_contador_dia           := 0;

                  -- Logica de contador do loop, foi incluida para nao gerar estouro no planejamento
                  -- somente considerando uma quantidade de 999 dias.
                  -- Erro encontrado no cliente TDV e corrigido por Neylor / Comin e Agnaldo
                  v_conta_loop := 0;

                  while v_saldo_tempo_disponivel > 0.000 and v_conta_loop < 999
                  loop
                     v_conta_loop := v_conta_loop + 1;

                     begin
                        select tmrp_660.tempo_disponivel , tmrp_660.qtde_recursos
                        into v_tempo_disponivel_660,        v_qtde_recursos_660
                        from tmrp_660
                        where tmrp_660.tipo_alocacao  = 0 --- Fixo Estagios para o tipo planejamento
                          and tmrp_660.tipo_recurso   = 6 --- Fixo estagios
                          and tmrp_660.codigo_recurso = v_codigo_recurso
                          and tmrp_660.data_recurso   = v_data_consulta;
                        exception
                           when others then
                              v_tempo_disponivel_660 := 0;
                              v_qtde_recursos_660    := 0;
                     end;

                     if  v_qtde_recursos_660 > 0
                     then
                        v_tempo_disponivel_recurso :=  v_tempo_disponivel_660 / v_qtde_recursos_660;
                     else
                        v_tempo_disponivel_recurso :=  v_tempo_disponivel_660 ;
                     end if;

                     if v_tempo_disponivel_recurso > 0
                     then
                         v_contador_dia           := 1;
                         v_data_inicial_calculada := trunc(v_data_consulta,'DD');
                         v_saldo_tempo_disponivel := v_saldo_tempo_disponivel - v_tempo_disponivel_recurso;
                     end if;

                     v_data_consulta := trunc((v_data_consulta -1),'DD');
                  end loop;

                  if v_contador_dia > 0
                  then
                     v_data_inicial_estagio_atual := trunc((v_data_inicial_calculada - reg_tmrp_650.lead_time),'DD');
                     v_data_final_estagio_atual   := trunc(v_data_final_calculada,'DD');
                  else
                     v_data_inicial_estagio_atual := trunc(v_data_inicial_calculada,'DD');
                     v_data_final_estagio_atual   := trunc(v_data_final_calculada,'DD');
                  end if;
               end if; --v_dias_producao_estagio_atual >= v_dias_producao_estagio_post

               -- Verifica se a data inicial encontrada e um dia util. Se nao for entao encontra a
               -- primira data util antes da data calculada
               begin
                  select count(*) into v_contador_tmrp_660
                  from tmrp_660
                  where tmrp_660.tipo_alocacao  = 0 --- Fixo Estagios para o tipo planejamento
                    and tmrp_660.tipo_recurso   = 6 --- Fixo Estagios
                    and tmrp_660.codigo_recurso = v_codigo_recurso
                    and tmrp_660.data_recurso   = v_data_inicial_estagio_atual;
               end;

               if v_contador_tmrp_660 = 0
               then
                  begin
                     select max(tmrp_660.data_recurso) into v_data_inicial_660
                     from tmrp_660
                     where tmrp_660.tipo_alocacao  = 0 --- Fixo Estagios para o tipo planejamento
                       and tmrp_660.tipo_recurso   = 6 --- Fixo Estagios
                       and tmrp_660.codigo_recurso = v_codigo_recurso
                       and tmrp_660.data_recurso   < v_data_inicial_estagio_atual;

                     exception
                        when others then
                           v_data_inicial_660 := null;
                   end;

                   if v_data_inicial_660 is not null
                   then
                      v_data_inicial_estagio_atual := trunc(v_data_inicial_660, 'DD');
                   end if;
               end if;

               v_data_inicial_estagio_post := trunc(v_data_inicial_estagio_atual,'DD');
               v_data_final_estagio_post   := trunc(v_data_final_estagio_atual,'DD');
               v_codigo_recurso_post       := v_codigo_recurso;

               if reg_tmrp_650.codigo_estagio = reg_tmrp_650_prod.codigo_estagio
               then
                  update tmrp_650
                  set tmrp_650.data_inicio  = v_data_inicial_estagio_atual
                  where tmrp_650.numero_id = reg_tmrp_650.numero_id;
               end if;
            end if; -- if v_tempo_disp_estagio_atual > 0.000
         end loop;  -- loop do tmrp_650
      end if;       -- if reg_tmrp_650_prod.seq_estagio_ordem > 0
  end loop;  -- loop do tmrp_650_prod

  return (1);

end;

 

/

exec inter_pr_recompile;

