CREATE OR REPLACE PACKAGE BODY ST_PCK_PED_COMPRA AS

    procedure valida_tab_preco_forn(p_tab_preco number, p_cgc_forn9 number,
                               p_cgc_forn4 number, p_cgc_forn2 number) is
    
       v_situacao number;
       v_cod_moeda supr_580.cod_moeda%type;
       
    begin
    
       if p_tab_preco <> 0
       then
          begin
             select supr_580.cod_moeda 
             into v_cod_moeda
             from supr_580
             where supr_580.cod_tabela = p_tab_preco;
          exception 
          when no_data_found 
          then
             raise_application_error(-20001,'ATENC?O! Tabela de pre?os n?o cadastrada.'); 
          end;
          
          begin
             select situacao 
             into v_situacao
             from supr_580
             where supr_580.cod_tabela = p_tab_preco
             and supr_580.cgc_forn9 = p_cgc_forn9
             and supr_580.cgc_forn4 = p_cgc_forn4
             and supr_580.cgc_forn2 = p_cgc_forn2;
          exception
          when no_data_found 
          then
             begin
                select situacao 
                into v_situacao
                from supr_580
                where supr_580.cod_tabela = p_tab_preco
                and supr_580.cgc_forn9 = 0
                and supr_580.cgc_forn4 = 0
                and supr_580.cgc_forn2 = 0;
             exception
             when no_data_found
             then
                raise_application_error(-20001,'ATENC?O! Tabela de pre?o dispon?vel para o fornecedor informado.');
             end;
          end;
          
          if v_situacao <> 1
          then
             raise_application_error(-20001,'ATENC?O! Tabela de pre?o n?o est? ativa.');
          end if;
          
       end if;
    
    end valida_tab_preco_forn;
         

    procedure cria_pedido(p_codigo_comprador NUMBER,      p_codigo_empresa NUMBER,
                          p_usuario VARCHAR2,             p_cgc_forn9 NUMBER,
                          p_cgc_forn4 NUMBER,             p_cgc_forn2 NUMBER,
                          p_data_prev_entr DATE,          p_tab_preco NUMBER,
                          p_codigo_transacao NUMBER,      p_cond_pgto_compra NUMBER,
                          p_tipo_pedido NUMBER,           p_pedido_pai NUMBER, 
                          p_tipo_frete_redespacho NUMBER, p_ordem_servico NUMBER, 
                          p_tran_ped_forne9 NUMBER,       p_tran_ped_forne4 NUMBER, 
                          p_tran_ped_forne2 NUMBER,       p_cod_portador NUMBER, 
                          p_vendedor_contato VARCHAR2,    p_cod_moeda NUMBER, 
                          p_peso_total NUMBER,            p_valor_outras NUMBER, 
                          p_val_enc_finan NUMBER,         p_vlr_frete NUMBER,
                          v_pedido_compra out NUMBER) is

        v_nr_pedc_autom number;
        v_maior_pedido number;
        v_situacao_fornecedor number;
        v_email_fornecedor VARCHAR2(256);
        v_tmp_int number;
        v_situacao_pedido number;
        v_verifica_sit number;

    begin
        
        valida_tab_preco_forn(p_tab_preco, p_cgc_forn9, p_cgc_forn4, p_cgc_forn2);

        if p_codigo_comprador = 0
        then
            raise_application_error(-20001,'ATENC?O! Codigo do comprador n?o pode ser 0.');
        end if;
        
        if p_tipo_pedido = 2
        then 
           begin
              select 1 
              into v_tmp_int
              from hdoc_030 
              where hdoc_030.usuario = p_usuario 
              and   hdoc_030.empresa = p_codigo_empresa
              and   hdoc_030.compra_emergencial = 'S';
           exception
           when no_data_found 
           then
              raise_application_error(-20001,'ATENC?O! Somente ser? poss?vel informar tipo pedido 2 se o comprador estiver configurado para permitir digitar o pedido emergencial.');
           end;
        end if;
        
        select nr_pedc_autom 
        into v_nr_pedc_autom
        from empr_001;

        if v_nr_pedc_autom = 1 
        then
           v_maior_pedido := 0;

           select nvl(max(supr_090.pedido_compra),0) 
           into v_maior_pedido
           from supr_090;
            
           v_pedido_compra := 0;

           while v_pedido_compra <= v_maior_pedido
           LOOP
              select seq_pedido_compra.nextval 
              into v_pedido_compra
              from dual;
                
              if v_pedido_compra is null
              then 
                 v_pedido_compra := 0;
              end if;
           END LOOP;
        end if;
        
        begin
           select supr_010.sit_fornecedor, supr_010.e_mail
           into v_situacao_fornecedor, v_email_fornecedor
           from supr_010
           where supr_010.fornecedor9 = p_cgc_forn9
             and supr_010.fornecedor4 = p_cgc_forn4
             and supr_010.fornecedor2 = p_cgc_forn2;
        exception when no_data_found then
           v_situacao_fornecedor := 0;
        end;

        if v_situacao_fornecedor = 9
        then
           raise_application_error(-20001,'ATENC?O! Situac?o do Fornecedor esta como PRE-CADASTRO. Precisara ser alterada para ATIVO por usuario qualificado para tal.');
        end if;

        if v_situacao_fornecedor = 2
        then
           raise_application_error(-20001,'ATENC?O! Situac?o do Fornecedor esta como INATIVO. Precisara ser alterada para ATIVO por usuario qualificado para tal.');
        end if;
        
        select fatu_502.sit_inic_pedido_compra 
        into v_verifica_sit
        from fatu_502
        where fatu_502.codigo_empresa = p_codigo_empresa;

        if v_verifica_sit = 1
        then
           v_situacao_pedido := 7;
        else
           v_situacao_pedido := 1;
            
           begin
              select 1 
              into v_tmp_int
              from hdoc_100
              where hdoc_100.usubloq_empr_usu = p_codigo_empresa
              and hdoc_100.area_bloqueio = 3
              and rownum = 1;
           exception when no_data_found then
              v_tmp_int := 0;
           end;
            
           if v_tmp_int = 1
           then
              v_situacao_pedido := 9;
           end if;
        end if;

        begin
           select 1 
           into v_tmp_int
           from hdoc_100 
           where hdoc_100.situacao_liberador = 0 
             and hdoc_100.area_bloqueio = 3 
             and hdoc_100.usubloq_empr_usu = p_codigo_empresa
             and rownum = 1;
        exception when no_data_found then
           v_situacao_pedido := 1;
        end;
        
        INSERT INTO SUPR_090(
            codigo_empresa, pedido_compra,
            dt_emis_ped_comp, data_prev_entr,
            forn_ped_forne9, forn_ped_forne4,
            forn_ped_forne2, tab_preco,
            codigo_transacao, datetime_pedido,
            codigo_comprador, tipo_frete,
            cod_end_entrega, cod_end_cobranca,
            cond_pgto_compra, situacao_pedido,
            e_mail, valor_frete,
            cod_moeda, tipo_pedido,
            pedido_pai, tipo_frete_redespacho,
            ordem_servico, tran_ped_forne9,
            tran_ped_forne4, tran_ped_forne2,
            cod_portador, vendedor_contato,
            peso_total, valor_outras, 
            val_enc_finan
        ) values(
            p_codigo_empresa, v_pedido_compra,
            sysdate, p_data_prev_entr,
            p_cgc_forn9, p_cgc_forn4,
            p_cgc_forn2, p_tab_preco,
            nvl(p_codigo_transacao, 0), sysdate,
            p_codigo_comprador, 1,
            p_codigo_empresa, p_codigo_empresa,
            p_cond_pgto_compra, v_situacao_pedido,
            v_email_fornecedor, p_vlr_frete,
            p_cod_moeda, p_tipo_pedido,
            p_pedido_pai, p_tipo_frete_redespacho,
            p_ordem_servico, p_tran_ped_forne9,
            p_tran_ped_forne4, p_tran_ped_forne2,
            p_cod_portador, p_vendedor_contato,
            p_peso_total, p_valor_outras, 
            p_val_enc_finan
        );

    end cria_pedido;
    
    procedure adicionar_item(p_cod_empresa IN NUMBER,p_pedido_compra IN NUMBER,
                            p_seq_item_pedido IN NUMBER,p_item_nivel IN VARCHAR2,
                            p_item_grupo IN VARCHAR2,p_item_subgru IN VARCHAR2,
                            p_item_item IN VARCHAR2, p_preco_item IN NUMBER,
                            p_perc_ipi IN NUMBER, p_data_prev_entr IN DATE,
                            p_cod_contabil IN NUMBER, p_cod_transacao IN NUMBER,
                            p_num_requis IN NUMBER, p_seq_item_req IN NUMBER,
                            p_qtde_pedida IN NUMBER, p_cgc9 IN NUMBER,
                            p_cgc4 IN NUMBER, p_cgc2 IN NUMBER,
                            p_ccusto IN NUMBER, p_valor_conv IN NUMBER, 
                            p_cod_fabricante_prod IN VARCHAR2, p_cod_prod_fabricante IN VARCHAR2, 
                            p_cnpj9_destino IN NUMBER, p_cnpj4_destino IN NUMBER, 
                            p_cnpj2_destino IN NUMBER, p_percentual_subs IN NUMBER, 
                            p_perc_enc_finan IN NUMBER, p_cod_deposito IN NUMBER) is

        v_un_medida varchar2(2);
        v_un_medida_aux varchar2(2);
        v_projeto number;
        v_subprojeto number;
        v_servico number;
        v_data_prev_entr date;
        v_observacao_item_req varchar(2000);
        v_periodo_compras number;
        v_perc_iva number;
        v_perc_igv number;
        v_class_fiscal varchar2(15);
        v_consiste_empresa_compras varchar2(1);
        v_unidade_conv varchar2(3);
        v_fator_conv number;
        v_cod_aplic number;
        v_desc_item varchar2(1000);
        v_cod_deposito number;

    begin
    
        v_un_medida := '';
        v_un_medida_aux := '';
        v_projeto := 0;
        v_subprojeto := 0;
        v_servico := 0;
        v_observacao_item_req := '';
        v_periodo_compras := 0;
        v_perc_iva := 0;
        v_perc_igv := 0;
        v_class_fiscal := '';
        v_unidade_conv := '';
        v_fator_conv := 1;
        v_cod_aplic := 0;
        v_cod_deposito := 0;

        begin
            select basi_030.unidade_medida
            into v_un_medida
            from basi_030
            where basi_030.nivel_estrutura = p_item_nivel
              and basi_030.referencia = p_item_grupo;
        exception when no_data_found then
            v_un_medida := '';
        end;

        begin
            select supr_067.data_prev_entr, supr_067.projeto,
                supr_067.subprojeto, supr_067.servico
            into v_data_prev_entr, v_projeto,
                v_subprojeto, v_servico
            from supr_067
            where supr_067.num_requisicao = p_num_requis
              and supr_067.seq_item_req = p_seq_item_req
              and supr_067.data_prev_entr > sysdate - 1;
        exception when no_data_found then
            v_data_prev_entr := p_data_prev_entr;
        end;
            
        begin
            select supr_067.unidade_medida, supr_067.observacao_item, supr_067.descricao_item
            into v_un_medida_aux, v_observacao_item_req, v_desc_item
            from supr_067
            where supr_067.num_requisicao = p_num_requis
              and supr_067.seq_item_req = p_seq_item_req;
        exception when no_data_found then
            v_un_medida_aux := '';
            v_observacao_item_req := '';
            v_desc_item := '';
        end;

        if v_un_medida is null
        then
            v_un_medida := v_un_medida_aux;
        end if;
            
        if v_observacao_item_req is null
        then
            v_observacao_item_req := '';
        end if;
            
        begin
            select fatu_503.consiste_empresa_compras
            into v_consiste_empresa_compras
            from fatu_503 
            where fatu_503.codigo_empresa = p_cod_empresa;
        exception when no_data_found then
            v_consiste_empresa_compras := 'N';
        end; 

        begin
            select max(pcpc_010.periodo_producao) 
            into v_periodo_compras
            from pcpc_010
            where (pcpc_010.codigo_empresa = p_cod_empresa 
              or v_consiste_empresa_compras = 'N')
              and pcpc_010.area_periodo = 9
              and pcpc_010.situacao_periodo < 3
              and pcpc_010.data_ini_periodo <= v_data_prev_entr
              and pcpc_010.data_fim_periodo >= v_data_prev_entr;
        exception when no_data_found then
            v_periodo_compras := 0;
        end;
            
        if v_periodo_compras is null
        then 
            v_periodo_compras := 0;
        end if;

        begin
            select basi_010.classific_fiscal
            into v_class_fiscal
            from basi_010
            where basi_010.nivel_estrutura = p_item_nivel
              and basi_010.grupo_estrutura = p_item_grupo
              and basi_010.subgru_estrutura = p_item_subgru
              and basi_010.item_estrutura = p_item_item;
        exception when no_data_found then
            v_class_fiscal := '';
        end;

        begin
            select basi_240.perc_igv 
            into v_perc_igv
            from basi_240
            where basi_240.classific_fiscal = v_class_fiscal;
        exception when no_data_found then
            v_perc_iva := 0;
        end;

        if v_perc_igv > 0
        then
            v_perc_iva := v_perc_igv;
        else v_perc_iva := 0;
        end if;

        if p_num_requis <> 0 
        then
           begin
              select supr_065.codigo_deposito
              into v_cod_deposito
              from supr_065
              where supr_065.num_requisicao = p_num_requis;
           exception when no_data_found then
              v_cod_deposito := p_cod_deposito;
           end;
        else
           v_cod_deposito := p_cod_deposito;
        end if;
        
            
        begin
            select basi_015.aplicacao
            into v_cod_aplic
            from basi_015 
            where basi_015.codigo_empresa = p_cod_empresa
              and basi_015.nivel_estrutura = p_item_nivel
              and basi_015.grupo_estrutura = p_item_grupo
              and basi_015.subgru_estrutura = p_item_subgru
              and basi_015.item_estrutura = p_item_item
              and basi_015.codigo_deposito = v_cod_deposito;
        exception when no_data_found then
            v_cod_aplic := 0;
        end;
            
        begin
            select supr_060.unid_conv, supr_060.fator_conv
            into v_unidade_conv, v_fator_conv
            from supr_060
            where supr_060.item_060_nivel99 = p_item_nivel
              and supr_060.item_060_grupo = p_item_grupo
              and supr_060.item_060_subgrupo = p_item_subgru
              and supr_060.item_060_item = p_item_item
              and supr_060.forn_060_forne9 = p_cgc9
              and supr_060.forn_060_forne4 = p_cgc4
              and supr_060.forn_060_forne2 = p_cgc2
              and supr_060.fator_conv > 0.000000;
        exception when no_data_found then
            v_unidade_conv := '';
            v_fator_conv := 1;
        end;

        insert into supr_100 (
            num_ped_compra, seq_item_pedido,
            item_100_nivel99, item_100_grupo,
            item_100_subgrupo, item_100_item,
            descricao_item, unidade_medida,
            qtde_pedida_item, qtde_saldo_item,
            preco_item_comp, percentual_desc,
            percentual_ipi, outras_despesas,
            centro_custo, data_prev_entr,
            num_requisicao, seq_item_req,
            situacao_item, codigo_deposito,
            codigo_contabil, projeto,
            subprojeto, servico,
            periodo_compras, observacao_item,
            perc_iva, unidade_conv, 
            fator_conv, valor_conv, 
            cod_aplicacao, codigo_transacao,
            cod_fabricante_prod, cod_prod_fabricante, 
            cnpj9_destino, cnpj4_destino, 
            cnpj2_destino, percentual_subs, 
            perc_enc_finan
        )VALUES (
            p_pedido_compra, p_seq_item_pedido,
            p_item_nivel, p_item_grupo,
            p_item_subgru, p_item_item,
            v_desc_item, v_un_medida,
            p_qtde_pedida, p_qtde_pedida,
            p_preco_item, 0,
            p_perc_ipi, 0,
            p_ccusto, v_data_prev_entr,
            p_num_requis, p_seq_item_req,
            1, v_cod_deposito,
            p_cod_contabil, v_projeto,
            v_subprojeto, v_servico,
            v_periodo_compras, v_observacao_item_req,
            v_perc_iva, v_unidade_conv, 
            v_fator_conv, p_valor_conv, 
            v_cod_aplic, p_cod_transacao,
            p_cod_fabricante_prod, p_cod_prod_fabricante, 
            p_cnpj9_destino, p_cnpj4_destino, 
            p_cnpj2_destino, p_percentual_subs, 
            p_perc_enc_finan
        );
    exception when others then
        raise_application_error(-20001,'Erro gera_item '||SQLERRM);
    end adicionar_item;
    
    procedure cancela_pedido(p_pedido_compra     NUMBER, p_cod_cancelamento NUMBER,
                             p_canc_itens_requis NUMBER, p_cod_empresa NUMBER) is
    
    v_cod_cancelamento supr_090.cod_cancelamento%type;
    v_situacao_pedido supr_090.situacao_pedido%type;
    v_cod_canc_requis supr_090.cod_cancelamento%type;
    v_reutiliza_sdcv_obc supr_090.reutiliza_sdcv_obc%type;
    v_num_requisicao supr_100.num_requisicao%type;
    v_bloqueia_requisicao fatu_500.bloqueia_requisicao%type;
    
    begin
    
       begin
          select supr_090.cod_cancelamento, supr_090.situacao_pedido
          into v_cod_cancelamento, v_situacao_pedido
          from supr_090
          where supr_090.pedido_compra = p_pedido_compra;
       exception 
       when no_data_found then
          raise_application_error(-20001,'ATEN??O! Pedido n?o cadastrado.');
       end;
       
       if v_cod_cancelamento = 0 and p_cod_cancelamento > 0 and v_situacao_pedido = 4
       then
          raise_application_error(-20001,'ATEN??O! Pedido com baixa total n?o pode ser cancelado.');
       end if;
       
       if p_canc_itens_requis = 1
       then
          v_cod_canc_requis := p_cod_cancelamento;
          v_reutiliza_sdcv_obc := 'N';
       else 
          v_cod_canc_requis := 0;
          v_reutiliza_sdcv_obc := 'S';
       end if;
       
       for item in (
          select supr_100.seq_item_pedido, supr_100.item_100_nivel99,
                 supr_100.item_100_grupo,  supr_100.item_100_subgrupo,
                 supr_100.item_100_item,   supr_100.qtde_saldo_item,
                 supr_100.preco_item_comp, supr_100.num_requisicao,
                 supr_100.seq_item_req,    supr_100.centro_custo,
                 supr_100.data_prev_entr,  supr_100.qtde_pedida_item,
                 supr_100.codigo_contabil, supr_100.percentual_ipi,
                 supr_100.percentual_desc, supr_100.perc_enc_finan
          from   supr_100
          where supr_100.num_ped_compra   = p_pedido_compra
            and supr_100.cod_cancelamento = 0
       )
       loop
          v_num_requisicao := item.num_requisicao;   
       
          update supr_100
          set cod_cancelamento = p_cod_cancelamento,
              num_requisicao   = 0
          where supr_100.num_ped_compra  = p_pedido_compra
            and supr_100.seq_item_pedido = item.seq_item_pedido;
            
          if v_num_requisicao > 0 and item.qtde_saldo_item = item.qtde_pedida_item
          then
             update supr_067
             set numero_pedido  = 0,
                 numero_cotacao = 0,
                 numero_coleta  = 0,
                 cod_cancelamento = v_cod_canc_requis
             where num_requisicao = v_num_requisicao
               and seq_item_req   = item.seq_item_req;
               
             select fatu_500.bloqueia_requisicao
             into v_bloqueia_requisicao
             from fatu_500
             where fatu_500.codigo_empresa = p_cod_empresa;
             
             if v_bloqueia_requisicao = 1 and v_cod_canc_requis = 0
             then 
                update supr_067
                set situacao = 9
                where num_requisicao = v_num_requisicao
                  and seq_item_req   = item.seq_item_req;
                
                delete from hdoc_110
                where hdoc_110.empresa = 0
                  and hdoc_110.documento = v_num_requisicao
                  and hdoc_110.sequencia = item.seq_item_req
                  and hdoc_110.serie_parcela = '001'
                  and hdoc_110.tipo_titulo = 0
                  and hdoc_110.cgc9 = 0
                  and hdoc_110.cgc4 = 0
                  and hdoc_110.cgc2 = 0
                  and hdoc_110.codigo_processo = 2;
             end if;
          end if;
          
          update supr_100
          set cod_cancelamento  = p_cod_cancelamento,
              dt_cancelamento   = sysdate
          where num_ped_compra  = p_pedido_compra
            and seq_item_pedido = item.seq_item_pedido;
            
       end loop;
       
       update supr_090
       set supr_090.cod_cancelamento   = p_cod_cancelamento,
           supr_090.reutiliza_sdcv_obc = v_reutiliza_sdcv_obc,
           supr_090.situacao_pedido    = 4
       where supr_090.pedido_compra = p_pedido_compra;
    
    end cancela_pedido;
    
    procedure cancelar_pedidos(p_pedidos_compra    VARCHAR2, p_cod_cancelamento NUMBER,
                               p_canc_itens_requis NUMBER, p_cod_empresa NUMBER) is
                             
    begin
       FOR reg IN (
         WITH split_data AS (
            SELECT REGEXP_SUBSTR(p_pedidos_compra, '[^;]+', 1, LEVEL) AS pedido_compra
            FROM   dual
            CONNECT BY REGEXP_SUBSTR(p_pedidos_compra, '[^;]+', 1, LEVEL) IS NOT NULL
         )
         SELECT pedido_compra FROM split_data) 
      LOOP
         cancela_pedido(reg.pedido_compra, p_cod_cancelamento, p_canc_itens_requis, p_cod_empresa);
      END LOOP;
    end;
    
    function busca_valor_pedido(p_pedido_compra NUMBER) 
    return number
    is 
    
    v_valor_pedido number(15,3);
    v_valor_liq number(15,3);
    v_valor_desc_prod number(15,3);
    v_valor_ipi_prod number(15,3);
    v_valor_enc_prod number(15,3);
    v_valor_iva number(15,3);
    v_total_item number(15,3);
    v_qtde_conv number(15,3);
    
    begin
       v_valor_pedido := 0;
       for item in (
          select supr_100.qtde_pedida_item, supr_100.qtde_saldo_item,
                 supr_100.preco_item_comp, supr_100.percentual_ipi,
                 supr_100.percentual_desc, supr_100.cod_cancelamento,
                 supr_100.situacao_item, supr_100.perc_enc_finan,
                 supr_100.perc_iva, supr_100.item_100_nivel99,
                 supr_100.fator_conv, supr_100.valor_conv
          from supr_100
          where supr_100.num_ped_compra = p_pedido_compra
       ) loop
           
          v_valor_desc_prod := item.preco_item_comp * item.percentual_desc / 100;
          v_valor_ipi_prod := (item.preco_item_comp - v_valor_desc_prod) * item.percentual_ipi / 100;
          v_valor_enc_prod := item.preco_item_comp * item.perc_enc_finan / 100;
          v_valor_iva := item.preco_item_comp * item.perc_iva / 100;
        
          v_valor_liq := item.preco_item_comp - v_valor_desc_prod + v_valor_ipi_prod + v_valor_enc_prod + v_valor_iva;
        
          if item.valor_conv > 0 and item.fator_conv > 0
          then 
             v_total_item := item.qtde_pedida_item * item.preco_item_comp;
             v_valor_ipi_prod := (v_total_item - v_valor_desc_prod) * item.percentual_ipi / 100;
             v_qtde_conv := item.qtde_pedida_item / item.fator_conv;
           
             v_valor_pedido := v_valor_pedido + v_qtde_conv * item.valor_conv + v_valor_ipi_prod;
          else
             v_valor_pedido := v_valor_pedido + (item.qtde_pedida_item * v_valor_liq);
          end if;
           
          /*if item.cod_cancelamento = 0
          then
             if item.valor_conv > 0 and item.fator_conv > 0
             then
                v_saldo_conv := item.qtde_saldo_item / item.fator_conv;
                v_valor_saldo_pedido := v_valor_saldo_pedido + (v_saldo_conv  * item.valor_conv);
             else 
                v_valor_saldo_pedido := v_valor_saldo_pedido + (item.qtde_saldo_item * v_valor_liq);
             end if;
          end if;*/
           
       end loop;
       
       return v_valor_pedido;
    end;
    
    function busca_valor_saldo_pedido(p_pedido_compra NUMBER) 
    return number
    is 
    
    v_valor_saldo_pedido number(15,3);
    v_valor_liq number(15,3);
    v_valor_desc_prod number(15,3);
    v_valor_ipi_prod number(15,3);
    v_valor_enc_prod number(15,3);
    v_valor_iva number(15,3);
    v_saldo_conv number(15,3);
    
    begin
       v_valor_saldo_pedido := 0;
       for item in (
          select supr_100.qtde_pedida_item, supr_100.qtde_saldo_item,
                 supr_100.preco_item_comp, supr_100.percentual_ipi,
                 supr_100.percentual_desc, supr_100.cod_cancelamento,
                 supr_100.situacao_item, supr_100.perc_enc_finan,
                 supr_100.perc_iva, supr_100.item_100_nivel99,
                 supr_100.fator_conv, supr_100.valor_conv
          from supr_100
          where supr_100.num_ped_compra = p_pedido_compra
       ) loop
           
          v_valor_desc_prod := item.preco_item_comp * item.percentual_desc / 100;
          v_valor_ipi_prod := (item.preco_item_comp - v_valor_desc_prod) * item.percentual_ipi / 100;
          v_valor_enc_prod := item.preco_item_comp * item.perc_enc_finan / 100;
          v_valor_iva := item.preco_item_comp * item.perc_iva / 100;
        
          v_valor_liq := item.preco_item_comp - v_valor_desc_prod + v_valor_ipi_prod + v_valor_enc_prod + v_valor_iva;
        
          if item.cod_cancelamento = 0
          then
             if item.valor_conv > 0 and item.fator_conv > 0
             then
                v_saldo_conv := item.qtde_saldo_item / item.fator_conv;
                v_valor_saldo_pedido := v_valor_saldo_pedido + (v_saldo_conv  * item.valor_conv);
             else 
                v_valor_saldo_pedido := v_valor_saldo_pedido + (item.qtde_saldo_item * v_valor_liq);
             end if;
          end if;
           
       end loop;
       
       return v_valor_saldo_pedido;
    end;

END ST_PCK_PED_COMPRA;
