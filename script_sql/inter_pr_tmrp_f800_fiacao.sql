
  CREATE OR REPLACE PROCEDURE "INTER_PR_TMRP_F800_FIACAO" 
 (p_num_aleatorio              in numeric,
  p_data_criacao               in date   ,
  p_codigo_usuario             in numeric,
  p_codigo_empresa             in numeric,
  p_periodo_prod_ini           in numeric,
  p_periodo_prod_fim           in numeric,
  p_calcular_carga_mp_reserva  in varchar2,
  p_inc_exc_divfabr            in numeric,
  p_inc_exc_estagios           in numeric)

IS
  v_sequencia_plano         number;

BEGIN

   SELECT pcpf_310.sequencia_plano
   INTO   v_sequencia_plano
   FROM   pcpf_300, pcpf_310, pcpc_010
   WHERE  pcpf_300.periodo_producao  = pcpf_310.periodo_producao
     AND  pcpf_300.sequencia_plano   = pcpf_310.sequencia_plano
     AND  pcpf_300.linha_produto     = pcpf_310.linha_produto
     AND  pcpf_300.codigo_estagio    = pcpf_310.codigo_estagio
     AND  pcpf_300.grupo_maquina     = pcpf_310.grupo_maquina
     AND  pcpf_300.subgrupo_maquina  = pcpf_310.subgrupo_maquina
     AND  pcpf_310.qtde_planejada    > pcpf_310.qtde_produzida
     AND  pcpf_310.periodo_producao  = pcpc_010.periodo_producao
     AND  pcpc_010.area_periodo      = 7
     AND  pcpc_010.periodo_producao BETWEEN p_periodo_prod_ini AND p_periodo_prod_ini OR p_periodo_prod_ini = 99999
     AND  pcpc_010.situacao_periodo  < 3
   ORDER BY NVL(pcpf_300.data_aprovacao,'01-jan-80') DESC,
            pcpf_300.sequencia_plano                 DESC;

  IF SQL%NOTFOUND THEN
    v_sequencia_plano := 0;
  END IF;

INSERT /*+APPEND*/ INTO oper_tmp (nr_solicitacao,  nome_relatorio,
                        sequencia,       data_criacao,
                        int_01,          str_01,
                        str_02,          str_03,
                        str_04,          int_02,
                        int_03,          int_04,
                        flo_01,
                        str_05,          str_06,
                        int_05,          int_10,
                        int_11,          flo_10)
  SELECT p_num_aleatorio,            'tmrp_f800',
         rownum,                     p_data_criacao,
         pcpf_310.periodo_producao,  pcpf_310.nivel_estrutura,
         pcpf_310.grupo_estrutura,   pcpf_310.subgru_estrutura,
         pcpf_310.item_estrutura,    basi_010.numero_alternati,
         basi_010.numero_roteiro,    pcpf_310.codigo_estagio,
         (pcpf_310.qtde_planejada -  pcpf_310.qtde_produzida) quantidade,
         '',                         '',
         0,                          0,
         basi_030.div_prod,          0


   FROM  pcpf_310, pcpc_010, basi_030, basi_010
   WHERE pcpf_310.nivel_estrutura    = basi_010.nivel_estrutura
     AND pcpf_310.grupo_estrutura    = basi_010.grupo_estrutura
     AND pcpf_310.subgru_estrutura   = basi_010.subgru_estrutura
     AND pcpf_310.item_estrutura     = basi_010.item_estrutura
     AND pcpf_310.qtde_planejada     > pcpf_310.qtde_produzida
     AND pcpf_310.periodo_producao   = pcpc_010.periodo_producao
     AND pcpc_010.area_periodo       = 7
     AND (pcpc_010.periodo_producao BETWEEN p_periodo_prod_ini AND p_periodo_prod_ini OR p_periodo_prod_ini = 99999)
     AND pcpc_010.situacao_periodo   < 3
     AND (pcpf_310.sequencia_plano   = v_sequencia_plano OR v_sequencia_plano = 0)
     AND basi_030.nivel_estrutura    = pcpf_310.nivel_estrutura
     AND basi_030.referencia         = pcpf_310.grupo_estrutura

     AND (   EXISTS (SELECT 1 FROM rcnb_060
                     WHERE  rcnb_060.tipo_registro    = 38
                       AND  rcnb_060.nr_solicitacao   = 802
                       AND  rcnb_060.subgru_estrutura = p_codigo_usuario
                       AND (rcnb_060.grupo_estrutura  = basi_030.conta_estoque OR rcnb_060.grupo_estrutura = 999))
     OR p_calcular_carga_mp_reserva = 'N')
     AND  ((p_inc_exc_divfabr = 1 AND basi_030.div_prod in     (SELECT rcnb_060.item_estrutura FROM rcnb_060
                                                                          WHERE rcnb_060.tipo_registro    = 597
                                                                            AND rcnb_060.nr_solicitacao   = 800
                                                                            AND rcnb_060.subgru_estrutura = p_codigo_usuario
                                                                            AND rcnb_060.grupo_estrutura  = p_codigo_empresa))
     OR   (p_inc_exc_divfabr = 2 AND basi_030.div_prod not in (SELECT rcnb_060.item_estrutura FROM rcnb_060
                                                                        WHERE rcnb_060.tipo_registro    = 597
                                                                           AND rcnb_060.nr_solicitacao   = 800
                                                                           AND rcnb_060.subgru_estrutura = p_codigo_usuario
                                                                           AND rcnb_060.grupo_estrutura  = p_codigo_empresa)))

     AND  ((p_inc_exc_estagios = 1 AND pcpf_310.codigo_estagio in     (SELECT rcnb_060.grupo_estrutura FROM rcnb_060
                                                                          WHERE rcnb_060.tipo_registro    = 270
                                                                            AND rcnb_060.nr_solicitacao   = 800
                                                                            AND rcnb_060.subgru_estrutura = p_codigo_usuario
                                                                            AND rcnb_060.empresa_rel      = p_codigo_empresa))
      OR   (p_inc_exc_estagios = 2 AND pcpf_310.codigo_estagio not in (SELECT rcnb_060.grupo_estrutura FROM rcnb_060
                                                                          WHERE rcnb_060.tipo_registro    = 270
                                                                            AND rcnb_060.nr_solicitacao   = 800
                                                                            AND rcnb_060.subgru_estrutura = p_codigo_usuario
                                                                            AND rcnb_060.empresa_rel      = p_codigo_empresa))) ;

END INTER_PR_TMRP_F800_FIACAO;

 

/

exec inter_pr_recompile;

