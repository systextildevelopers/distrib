
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_010_INTEGRACAO" 
before insert or
       delete or
       update of nivel_estrutura,     grupo_estrutura,         subgru_estrutura,       item_estrutura,
                 descricao_15,        classific_fiscal,        item_agrupador
       on basi_010

for each row

declare

  v_grade_distribuicao        e_basi_010.grade_distribuicao%type;	v_perc_cor               	e_basi_010.perc_cor%type;
                                                         		v_tam_ponto_1              	e_basi_010.tam_ponto_1%type;
  v_nivel_estrutura    	      e_basi_010.nivel_estrutura%type;		v_tam_ponto_2             	e_basi_010.tam_ponto_2%type;
  v_grupo_estrutura           e_basi_010.grupo_estrutura%type;		v_tam_ponto_3                	e_basi_010.tam_ponto_3%type;
  v_subgru_estrutura          e_basi_010.subgru_estrutura%type;		v_tam_ponto_4                	e_basi_010.tam_ponto_4%type;
  v_item_estrutura            e_basi_010.item_estrutura%type;		v_ne_titulo               	e_basi_010.ne_titulo%type;
  v_descricao_15              e_basi_010.descricao_15%type;		v_ne_titulo_2                	e_basi_010.ne_titulo_2%type;
  v_item_ativo                e_basi_010.item_ativo%type;		v_ne_titulo_3                	e_basi_010.ne_titulo_3%type;
  v_classific_fiscal          e_basi_010.classific_fiscal%type;		v_ne_titulo_4                	e_basi_010.ne_titulo_4%type;
  v_codigo_velho              e_basi_010.codigo_velho%type;		v_lfa_1 			e_basi_010.lfa_1%type;
  v_data_cadastro             e_basi_010.data_cadastro%type; 		v_lfa_2 			e_basi_010.lfa_2%type;
  v_data_ult_compra           e_basi_010.data_ult_compra%type;		v_lfa_3  			e_basi_010.lfa_3%type;
  v_data_ult_entrada          e_basi_010.data_ult_entrada%type;		v_lfa_4                   	e_basi_010.lfa_4%type;
  v_data_ult_saida     	      e_basi_010.data_ult_saida%type;		v_tear_finura             	e_basi_010.tear_finura%type;
  v_preco_medio        	      e_basi_010.preco_medio%type;		v_tear_diametro           	e_basi_010.tear_diametro%type;
  v_preco_ult_compra   	      e_basi_010.preco_ult_compra%type;		v_perc_alt_dim_comp       	e_basi_010.perc_alt_dim_comp%type;
  v_preco_custo        	      e_basi_010.preco_custo%type;		v_perc_alt_dim_larg       	e_basi_010.perc_alt_dim_larg%type;
  v_preco_custo_info   	      e_basi_010.preco_custo_info%type;		v_largura_proj            	e_basi_010.largura_proj%type;
  v_preco_medio_ant    	      e_basi_010.preco_medio_ant%type; 		v_num_agulhas             	e_basi_010.num_agulhas%type;
  v_estoque_minimo     	      e_basi_010.estoque_minimo%type;		v_cursos                  	e_basi_010.cursos%type;
  v_tempo_reposicao    	      e_basi_010.tempo_reposicao%type;		v_colunas                 	e_basi_010.colunas%type;
  v_lote_multiplo      	      e_basi_010.lote_multiplo%type;		v_perc_perdas             	e_basi_010.perc_perdas%type;
  v_consumo_medio      	      e_basi_010.consumo_medio%type;		v_fator_correcao          	e_basi_010.fator_correcao%type;
  v_narrativa          	      e_basi_010.narrativa%type;		v_gramatura_proj          	e_basi_010.gramatura_proj%type;
  v_narrativa_ingles   	      e_basi_010.narrativa_ingles%type;		v_ponto_reposicao            	e_basi_010.ponto_reposicao%type;
  v_narrativa2         	      e_basi_010.narrativa2%type;		v_tipo_cor                   	e_basi_010.tipo_cor%type;
  v_narrativa_espanhol 	      e_basi_010.narrativa_espanhol%type;	v_codigo_cliente          	e_basi_010.codigo_cliente%type;
  v_tipo_mat_prima     	      e_basi_010.tipo_mat_prima%type;		v_qtde_min_venda          	e_basi_010.qtde_min_venda%type;
  v_estoque_maximo     	      e_basi_010.estoque_maximo%type;		v_tonalidade              	e_basi_010.tonalidade%type;
  v_data_desativacao   	      e_basi_010.data_desativacao%type;		v_concentracao               	e_basi_010.concentracao%type;
  v_tipo_prod_quimico  	      e_basi_010.tipo_prod_quimico%type;	v_motivo_uso              	e_basi_010.motivo_uso%type;
  v_artigo_cotas       	      e_basi_010.artigo_cotas%type;		v_complemento             	e_basi_010.complemento%type;
  v_data_lancamento    	      e_basi_010.data_lancamento%type;		v_qtde_minima             	e_basi_010.qtde_minima%type;
  v_largura            	      e_basi_010.largura%type;			v_sequencia_tamanho       	e_basi_010.sequencia_tamanho%type;
  v_gramatura          	      e_basi_010.gramatura%type;		v_preco_contratipo        	e_basi_010.preco_contratipo%type;
  v_data_importacao    	      e_basi_010.data_importacao%type;		v_nivel_antigo            	e_basi_010.nivel_antigo%type;
                                                           		v_grupo_antigo            	e_basi_010.grupo_antigo%type;
                                                         		v_subgrupo_antigo            	e_basi_010.subgrupo_antigo%type;
  v_classificacao_ncm  	      e_basi_010.classificacao_ncm%type;	v_item_antigo             	e_basi_010.item_antigo%type;
  v_numero_roteiro            e_basi_010.numero_roteiro%type; 		v_alternativa_acabado     	e_basi_010.alternativa_acabado%type;
  v_numero_alternati          e_basi_010.numero_alternati%type;		v_codigo_contabil         	e_basi_010.codigo_contabil%type;
  v_distribuicao_cor          e_basi_010.distribuicao_cor%type;		v_prioridade_distribuicao 	e_basi_010.prioridade_distribuicao%type;
  v_codigo_barras             e_basi_010.codigo_barras%type;		v_agrupador_linha         	e_basi_010.agrupador_linha%type;
  v_imprime_ficha             e_basi_010.imprime_ficha%type;		v_alternativa_custos      	e_basi_010.alternativa_custos%type;
  v_qtde_ups                  e_basi_010.qtde_ups%type;			v_roteiro_custos          	e_basi_010.roteiro_custos%type;
  v_qtde_elaboracao           e_basi_010.qtde_elaboracao%type;		v_nr_fios_fita            	e_basi_010.nr_fios_fita%type;
  v_natur_operacao            e_basi_010.natur_operacao%type;		v_item_agrupador          	e_basi_010.item_agrupador%type;
  v_sugere_item               e_basi_010.sugere_item%type;		v_produto_integracao      	e_basi_010.produto_integracao%type;
  v_cgc_cliente_2             e_basi_010.cgc_cliente_2%type;		v_estacao_dosagem_orgatex 	e_basi_010.estacao_dosagem_orgatex%type;
  v_cgc_cliente_4             e_basi_010.cgc_cliente_4%type;		v_ind_envia_infotint      	e_basi_010.ind_envia_infotint%type;
  v_cgc_cliente_9             e_basi_010.cgc_cliente_9%type;		v_perc_detraccion         	e_basi_010.perc_detraccion%type;
  v_grau_solidez              e_basi_010.grau_solidez%type; 		v_classificacao_ibama        	e_basi_010.classificacao_ibama%type;
  v_classificacao_naladi      e_basi_010.classificacao_naladi%type; 	v_cod_servico_lst         	e_basi_010.cod_servico_lst%type;
  v_numero_grafico            e_basi_010.numero_grafico%type; 		v_qtde_min_venda_min      	e_basi_010.qtde_min_venda_min%type;
  v_cod_tipo_volume           e_basi_010.cod_tipo_volume%type;		v_destino_projeto           	e_basi_010.destino_projeto%type;
  v_integrado_decisor         e_basi_010.integrado_decisor%type;	v_combinacao_projeto            e_basi_010.combinacao_projeto%type;

  v_tipo_operacao	      e_basi_010.tipo_operacao%type;
  ws_usuario_systextil        e_basi_010.usuario_operacao%type;

  ws_usuario_rede              varchar2(20);
  ws_maquina_rede              varchar2(40);
  ws_aplicativo                varchar2(20);
  ws_sid                       number(9);
  ws_empresa                   number(3);
  ws_locale_usuario            varchar2(5);

begin

  inter_pr_dados_usuario (ws_usuario_rede, ws_maquina_rede, ws_aplicativo, ws_sid, ws_usuario_systextil, ws_empresa, ws_locale_usuario);

  v_grade_distribuicao        := :new.grade_distribuicao;		    v_perc_cor                      := :new.perc_cor;
                                                           		  v_tam_ponto_1                   := :new.tam_ponto_1;
  v_nivel_estrutura    	      := :new.nivel_estrutura;			    v_tam_ponto_2                   := :new.tam_ponto_2;
  v_grupo_estrutura           := :new.grupo_estrutura;			    v_tam_ponto_3                   := :new.tam_ponto_3;
  v_subgru_estrutura          := :new.subgru_estrutura;			    v_tam_ponto_4                   := :new.tam_ponto_4;
  v_item_estrutura            := :new.item_estrutura;			      v_ne_titulo                     := :new.ne_titulo;
  v_descricao_15              := :new.descricao_15;          		v_ne_titulo_2                   := :new.ne_titulo_2;
  v_item_ativo                := 0;                         		v_ne_titulo_3                   := :new.ne_titulo_3;
  v_classific_fiscal          := :new.classific_fiscal;      		v_ne_titulo_4                   := :new.ne_titulo_4;
  v_codigo_velho              := :new.codigo_velho;          		v_lfa_1 		                    := :new.lfa_1;
  v_data_cadastro             := :new.data_cadastro;         		v_lfa_2 		                    := :new.lfa_2;
  v_data_ult_compra           := :new.data_ult_compra;       		v_lfa_3  		                    := :new.lfa_3;
  v_data_ult_entrada          := :new.data_ult_entrada;      		v_lfa_4                         := :new.lfa_4;
  v_data_ult_saida     	      := :new.data_ult_saida;     	  	v_tear_finura                   := :new.tear_finura;
  v_preco_medio        	      := :new.preco_medio;        	  	v_tear_diametro                 := :new.tear_diametro;
  v_preco_ult_compra   	      := :new.preco_ult_compra;   	  	v_perc_alt_dim_comp             := :new.perc_alt_dim_comp;
  v_preco_custo        	      := :new.preco_custo;        	  	v_perc_alt_dim_larg             := :new.perc_alt_dim_larg;
  v_preco_custo_info   	      := :new.preco_custo_info;   	  	v_largura_proj                  := :new.largura_proj;
  v_preco_medio_ant    	      := :new.preco_medio_ant;    	  	v_num_agulhas                   := :new.num_agulhas;
  v_estoque_minimo     	      := :new.estoque_minimo;     	  	v_cursos                        := :new.cursos;
  v_tempo_reposicao    	      := :new.tempo_reposicao;    	  	v_colunas                       := :new.colunas;
  v_lote_multiplo      	      := :new.lote_multiplo;      	  	v_perc_perdas                   := :new.perc_perdas;
  v_consumo_medio      	      := :new.consumo_medio;      	  	v_fator_correcao                := :new.fator_correcao;
  v_narrativa          	      := :new.narrativa;          	  	v_gramatura_proj                := :new.gramatura_proj;
  v_narrativa_ingles   	      := :new.narrativa_ingles;   	  	v_ponto_reposicao               := :new.ponto_reposicao;
  v_narrativa2         	      := :new.narrativa2;         	  	v_tipo_cor                      := :new.tipo_cor;
  v_narrativa_espanhol 	      := :new.narrativa_espanhol; 	  	v_codigo_cliente                := :new.codigo_cliente;
  v_tipo_mat_prima     	      := :new.tipo_mat_prima;     	  	v_qtde_min_venda                := :new.qtde_min_venda;
  v_estoque_maximo     	      := :new.estoque_maximo;     	  	v_tonalidade                    := :new.tonalidade;
  v_data_desativacao   	      := :new.data_desativacao;   	  	v_concentracao                  := :new.concentracao;
  v_tipo_prod_quimico  	      := :new.tipo_prod_quimico;  	  	v_motivo_uso                    := :new.motivo_uso;
  v_artigo_cotas       	      := 0;                       	  	v_complemento                   := :new.complemento;
  v_data_lancamento    	      := :new.data_lancamento;    	  	v_qtde_minima                   := :new.qtde_minima;
  v_largura            	      := :new.largura;            	  	v_sequencia_tamanho             := :new.sequencia_tamanho;
  v_gramatura          	      := :new.gramatura;          	  	v_preco_contratipo              := :new.preco_contratipo;
  v_data_importacao    	      :=  sysdate();   	  			        v_nivel_antigo                  := :new.nivel_antigo;
                                                         	  	  v_grupo_antigo                  := :new.grupo_antigo;
                                                         	  	  v_subgrupo_antigo               := :new.subgrupo_antigo;
  v_classificacao_ncm  	      := :new.classificacao_ncm;  	  	v_item_antigo                   := :new.item_antigo;
  v_numero_roteiro            := 0;                          		v_alternativa_acabado           := :new.alternativa_acabado;
  v_numero_alternati          := 0;                         		v_codigo_contabil               := 0;
  v_distribuicao_cor          := :new.distribuicao_cor;      		v_prioridade_distribuicao       := :new.prioridade_distribuicao;
  v_codigo_barras             := :new.codigo_barras;         		v_agrupador_linha               := :new.agrupador_linha;
  v_imprime_ficha             := :new.imprime_ficha;         		v_alternativa_custos            := 0;
  v_qtde_ups                  := :new.qtde_ups;              		v_roteiro_custos                := 0;
  v_qtde_elaboracao           := :new.qtde_elaboracao;       		v_nr_fios_fita                  := :new.nr_fios_fita;
  v_natur_operacao            := :new.natur_operacao;        		v_item_agrupador                := '000000';
  v_sugere_item               := :new.sugere_item;           		v_produto_integracao            := :new.produto_integracao;
  v_cgc_cliente_2             := :new.cgc_cliente_2;         		v_estacao_dosagem_orgatex       := :new.estacao_dosagem_orgatex;
  v_cgc_cliente_4             := :new.cgc_cliente_4;        		v_ind_envia_infotint            := :new.ind_envia_infotint;
  v_cgc_cliente_9             := :new.cgc_cliente_9;         		v_perc_detraccion               := :new.perc_detraccion;
  v_grau_solidez              := :new.grau_solidez;          		v_classificacao_ibama           := :new.classificacao_ibama;
  v_classificacao_naladi      := :new.classificacao_naladi;  		v_cod_servico_lst               := :new.cod_servico_lst;
  v_numero_grafico            := :new.numero_grafico;        		v_qtde_min_venda_min            := :new.qtde_min_venda_min;
  v_cod_tipo_volume           := :new.cod_tipo_volume;       		v_destino_projeto               := :new.destino_projeto;
  v_integrado_decisor         := :new.integrado_decisor;     		v_combinacao_projeto            := :new.combinacao_projeto;


  if inserting or updating
  then
      if inserting
      then v_tipo_operacao := 'I';
      else v_tipo_operacao := 'A';
      end if;

      if v_nivel_estrutura  = '2'  and
         ((v_tipo_operacao  = 'I') or
          ( v_tipo_operacao = 'A'  and
         (:old.nivel_estrutura  <> :new.nivel_estrutura  or
          :old.grupo_estrutura  <> :new.grupo_estrutura  or
          :old.subgru_estrutura <> :new.subgru_estrutura or
          :old.item_estrutura   <> :new.item_estrutura   or
          :old.descricao_15     <> :new.descricao_15     or
          :old.classific_fiscal <> :new.classific_fiscal or
          :old.item_agrupador   <> :new.item_agrupador)))
      then

      begin
      insert into e_basi_010
      (grade_distribuicao,   	       perc_cor,
                         	       tam_ponto_1,
       nivel_estrutura,    	       tam_ponto_2,
       grupo_estrutura,      	       tam_ponto_3,
       subgru_estrutura,     	       tam_ponto_4,
       item_estrutura,       	       ne_titulo,
       descricao_15,       	       ne_titulo_2,
       item_ativo,           	       ne_titulo_3,
       classific_fiscal,     	       ne_titulo_4,
       codigo_velho,         	       lfa_1,
       data_cadastro,        	       lfa_2,
       data_ult_compra,      	       lfa_3,
       data_ult_entrada,     	       lfa_4,
       data_ult_saida,     	       tear_finura,
       preco_medio,        	       tear_diametro,
       preco_ult_compra,   	       perc_alt_dim_comp,
       preco_custo,        	       perc_alt_dim_larg,
       preco_custo_info,   	       largura_proj,
       preco_medio_ant,    	       num_agulhas,
       estoque_minimo,     	       cursos,
       tempo_reposicao,    	       colunas,
       lote_multiplo,      	       perc_perdas,
       consumo_medio,      	       fator_correcao,
       narrativa,          	       gramatura_proj,
       narrativa_ingles,   	       ponto_reposicao,
       narrativa2,         	       tipo_cor,
       narrativa_espanhol, 	       codigo_cliente,
       tipo_mat_prima,     	       qtde_min_venda,
       estoque_maximo,     	       tonalidade,
       data_desativacao,   	       concentracao,
       tipo_prod_quimico,  	       motivo_uso,
       artigo_cotas,       	       complemento,
       data_lancamento,    	       qtde_minima,
       largura,            	       sequencia_tamanho,
       gramatura,         	       preco_contratipo,
       data_importacao,    	       nivel_antigo,
                         	       grupo_antigo,
                         	       subgrupo_antigo,
       classificacao_ncm,  	       item_antigo,
       numero_roteiro,       	       alternativa_acabado,
       numero_alternati,     	       codigo_contabil,
       distribuicao_cor,     	       prioridade_distribuicao,
       codigo_barras,        	       agrupador_linha,
       imprime_ficha,        	       alternativa_custos,
       qtde_ups,             	       roteiro_custos,
       qtde_elaboracao,      	       nr_fios_fita,
       natur_operacao,       	       item_agrupador,
       sugere_item,          	       produto_integracao,
       cgc_cliente_2,        	       estacao_dosagem_orgatex,
       cgc_cliente_4,        	       ind_envia_infotint,
       cgc_cliente_9,        	       perc_detraccion,
       grau_solidez,         	       classificacao_ibama,
       classificacao_naladi, 	       cod_servico_lst,
       numero_grafico,       	       qtde_min_venda_min,
       cod_tipo_volume,      	       destino_projeto,
       integrado_decisor,    	       combinacao_projeto,
       tipo_operacao,                  usuario_operacao,
       flag_integracao)
      values
      (v_grade_distribuicao,  	       v_perc_cor,
                          	       v_tam_ponto_1,
       v_nivel_estrutura,    	       v_tam_ponto_2,
       v_grupo_estrutura,     	       v_tam_ponto_3,
       v_subgru_estrutura,    	       v_tam_ponto_4,
       v_item_estrutura,      	       v_ne_titulo,
       v_descricao_15,        	       v_ne_titulo_2,
       v_item_ativo,          	       v_ne_titulo_3,
       v_classific_fiscal,    	       v_ne_titulo_4,
       v_codigo_velho,        	       v_lfa_1,
       v_data_cadastro,       	       v_lfa_2,
       v_data_ult_compra,     	       v_lfa_3,
       v_data_ult_entrada,    	       v_lfa_4,
       v_data_ult_saida,     	       v_tear_finura,
       v_preco_medio,        	       v_tear_diametro,
       v_preco_ult_compra,   	       v_perc_alt_dim_comp,
       v_preco_custo,        	       v_perc_alt_dim_larg,
       v_preco_custo_info,   	       v_largura_proj,
       v_preco_medio_ant,    	       v_num_agulhas,
       v_estoque_minimo,     	       v_cursos,
       v_tempo_reposicao,    	       v_colunas,
       v_lote_multiplo,      	       v_perc_perdas,
       v_consumo_medio,      	       v_fator_correcao,
       v_narrativa,          	       v_gramatura_proj,
       v_narrativa_ingles,   	       v_ponto_reposicao,
       v_narrativa2,         	       v_tipo_cor,
       v_narrativa_espanhol, 	       v_codigo_cliente,
       v_tipo_mat_prima,     	       v_qtde_min_venda,
       v_estoque_maximo,    	       v_tonalidade,
       v_data_desativacao,   	       v_concentracao,
       v_tipo_prod_quimico,  	       v_motivo_uso,
       v_artigo_cotas,       	       v_complemento,
       v_data_lancamento,    	       v_qtde_minima,
       v_largura,            	       v_sequencia_tamanho,
       v_gramatura,          	       v_preco_contratipo,
       v_data_importacao,    	       v_nivel_antigo,
                         	       v_grupo_antigo,
                         	       v_subgrupo_antigo,
       v_classificacao_ncm,  	       v_item_antigo,
       v_numero_roteiro,      	       v_alternativa_acabado,
       v_numero_alternati,    	       v_codigo_contabil,
       v_distribuicao_cor,	       v_prioridade_distribuicao,
       v_codigo_barras,       	       v_agrupador_linha,
       v_imprime_ficha,       	       v_alternativa_custos,
       v_qtde_ups,            	       v_roteiro_custos,
       v_qtde_elaboracao,     	       v_nr_fios_fita,
       v_natur_operacao,      	       v_item_agrupador,
       v_sugere_item,         	       v_produto_integracao,
       v_cgc_cliente_2,                v_estacao_dosagem_orgatex,
       v_cgc_cliente_4,                v_ind_envia_infotint,
       v_cgc_cliente_9,                v_perc_detraccion,
       v_grau_solidez,                 v_classificacao_ibama,
       v_classificacao_naladi,         v_cod_servico_lst,
       v_numero_grafico,               v_qtde_min_venda_min,
       v_cod_tipo_volume,              v_destino_projeto,
       v_integrado_decisor,            v_combinacao_projeto,
       v_tipo_operacao,                ws_usuario_systextil,
       1);

      exception
      when OTHERS
         then raise_application_error (-20000, 'N�o atualizou a tabela e_basi_010' || Chr(10) || SQLERRM);
      end;

      if v_tipo_operacao = 'A'                          and
         :old.nivel_estrutura  <> :new.nivel_estrutura  or
         :old.grupo_estrutura  <> :new.grupo_estrutura  or
         :old.subgru_estrutura <> :new.subgru_estrutura or
         :old.item_estrutura   <> :new.item_estrutura
      then


         v_tipo_operacao := 'E';

         v_grade_distribuicao        := :old.grade_distribuicao;		 v_perc_cor                      := :old.perc_cor;
                                                         		         v_tam_ponto_1                   := :old.tam_ponto_1;
         v_nivel_estrutura    	     := :old.nivel_estrutura;		         v_tam_ponto_2                   := :old.tam_ponto_2;
         v_grupo_estrutura           := :old.grupo_estrutura;		         v_tam_ponto_3                   := :old.tam_ponto_3;
         v_subgru_estrutura          := :old.subgru_estrutura;		         v_tam_ponto_4                   := :old.tam_ponto_4;
         v_item_estrutura            := :old.item_estrutura;		         v_ne_titulo                     := :old.ne_titulo;
         v_descricao_15              := :old.descricao_15;          	         v_ne_titulo_2                   := :old.ne_titulo_2;
         v_item_ativo                := :old.item_ativo;            	         v_ne_titulo_3                   := :old.ne_titulo_3;
         v_classific_fiscal          := :old.classific_fiscal;      	         v_ne_titulo_4                   := :old.ne_titulo_4;
         v_codigo_velho              := :old.codigo_velho;          	         v_lfa_1 		         := :old.lfa_1;
         v_data_cadastro             := :old.data_cadastro;         	         v_lfa_2 		         := :old.lfa_2;
         v_data_ult_compra           := :old.data_ult_compra;       	         v_lfa_3  		         := :old.lfa_3;
         v_data_ult_entrada          := :old.data_ult_entrada;      	         v_lfa_4                         := :old.lfa_4;
         v_data_ult_saida     	     := :old.data_ult_saida;     		 v_tear_finura                   := :old.tear_finura;
         v_preco_medio        	     := :old.preco_medio;        		 v_tear_diametro                 := :old.tear_diametro;
         v_preco_ult_compra   	     := :old.preco_ult_compra;   		 v_perc_alt_dim_comp             := :old.perc_alt_dim_comp;
         v_preco_custo        	     := :old.preco_custo;        		 v_perc_alt_dim_larg             := :old.perc_alt_dim_larg;
         v_preco_custo_info   	     := :old.preco_custo_info;   		 v_largura_proj                  := :old.largura_proj;
         v_preco_medio_ant    	     := :old.preco_medio_ant;    		 v_num_agulhas                   := :old.num_agulhas;
         v_estoque_minimo     	     := :old.estoque_minimo;     		 v_cursos                        := :old.cursos;
         v_tempo_reposicao    	     := :old.tempo_reposicao;    		 v_colunas                       := :old.colunas;
         v_lote_multiplo      	     := :old.lote_multiplo;      		 v_perc_perdas                   := :old.perc_perdas;
         v_consumo_medio      	     := :old.consumo_medio;      		 v_fator_correcao                := :old.fator_correcao;
         v_narrativa          	     := :old.narrativa;          		 v_gramatura_proj                := :old.gramatura_proj;
         v_narrativa_ingles   	     := :old.narrativa_ingles;   		 v_ponto_reposicao               := :old.ponto_reposicao;
         v_narrativa2         	     := :old.narrativa2;         		 v_tipo_cor                      := :old.tipo_cor;
         v_narrativa_espanhol 	     := :old.narrativa_espanhol; 		 v_codigo_cliente                := :old.codigo_cliente;
         v_tipo_mat_prima     	     := :old.tipo_mat_prima;     		 v_qtde_min_venda                := :old.qtde_min_venda;
         v_estoque_maximo     	     := :old.estoque_maximo;     		 v_tonalidade                    := :old.tonalidade;
         v_data_desativacao   	     := :old.data_desativacao;   		 v_concentracao                  := :old.concentracao;
         v_tipo_prod_quimico  	     := :old.tipo_prod_quimico;  		 v_motivo_uso                    := :old.motivo_uso;
         v_artigo_cotas       	     := :old.artigo_cotas;       		 v_complemento                   := :old.complemento;
         v_data_lancamento    	     := :old.data_lancamento;    		 v_qtde_minima                   := :old.qtde_minima;
         v_largura            	     := :old.largura;            		 v_sequencia_tamanho             := :old.sequencia_tamanho;
         v_gramatura          	     := :old.gramatura;          		 v_preco_contratipo              := :old.preco_contratipo;
         v_data_importacao    	     :=  sysdate();   	  		         v_nivel_antigo                  := :old.nivel_antigo;
                                                         		         v_grupo_antigo                  := :old.grupo_antigo;
                                                         		         v_subgrupo_antigo               := :old.subgrupo_antigo;
         v_classificacao_ncm  	     := :old.classificacao_ncm;  		 v_item_antigo                   := :old.item_antigo;
         v_numero_roteiro            := :old.numero_roteiro;        	         v_alternativa_acabado           := :old.alternativa_acabado;
         v_numero_alternati          := :old.numero_alternati;      	         v_codigo_contabil               := :old.codigo_contabil;
         v_distribuicao_cor          := :old.distribuicao_cor;      	         v_prioridade_distribuicao       := :old.prioridade_distribuicao;
         v_codigo_barras             := :old.codigo_barras;         	         v_agrupador_linha               := :old.agrupador_linha;
         v_imprime_ficha             := :old.imprime_ficha;         	         v_alternativa_custos            := :old.alternativa_custos;
         v_qtde_ups                  := :old.qtde_ups;              	         v_roteiro_custos                := :old.roteiro_custos;
         v_qtde_elaboracao           := :old.qtde_elaboracao;       	         v_nr_fios_fita                  := :old.nr_fios_fita;
         v_natur_operacao            := :old.natur_operacao;        	         v_item_agrupador                := :old.item_agrupador;
         v_sugere_item               := :old.sugere_item;           	         v_produto_integracao            := :old.produto_integracao;
         v_cgc_cliente_2             := :old.cgc_cliente_2;         	         v_estacao_dosagem_orgatex       := :old.estacao_dosagem_orgatex;
         v_cgc_cliente_4             := :old.cgc_cliente_4;        		 v_ind_envia_infotint            := :old.ind_envia_infotint;
         v_cgc_cliente_9             := :old.cgc_cliente_9;         	         v_perc_detraccion               := :old.perc_detraccion;
         v_grau_solidez              := :old.grau_solidez;          	         v_classificacao_ibama           := :old.classificacao_ibama;
         v_classificacao_naladi      := :old.classificacao_naladi;  	         v_cod_servico_lst               := :old.cod_servico_lst;
         v_numero_grafico            := :old.numero_grafico;        	         v_qtde_min_venda_min            := :old.qtde_min_venda_min;
         v_cod_tipo_volume           := :old.cod_tipo_volume;       	         v_destino_projeto               := :old.destino_projeto;
         v_integrado_decisor         := :old.integrado_decisor;     	         v_combinacao_projeto            := :old.combinacao_projeto;


         begin

         insert into e_basi_010
         (grade_distribuicao,          perc_cor,
                        	       tam_ponto_1,
          nivel_estrutura,    	       tam_ponto_2,
          grupo_estrutura,             tam_ponto_3,
          subgru_estrutura,            tam_ponto_4,
          item_estrutura,              ne_titulo,
          descricao_15,       	       ne_titulo_2,
          item_ativo,                  ne_titulo_3,
          classific_fiscal,            ne_titulo_4,
          codigo_velho,                lfa_1,
          data_cadastro,               lfa_2,
          data_ult_compra,             lfa_3,
          data_ult_entrada,            lfa_4,
          data_ult_saida,     	       tear_finura,
          preco_medio,        	       tear_diametro,
          preco_ult_compra,   	       perc_alt_dim_comp,
          preco_custo,        	       perc_alt_dim_larg,
          preco_custo_info,   	       largura_proj,
          preco_medio_ant,    	       num_agulhas,
          estoque_minimo,     	       cursos,
          tempo_reposicao,    	       colunas,
          lote_multiplo,      	       perc_perdas,
          consumo_medio,      	       fator_correcao,
          narrativa,          	       gramatura_proj,
          narrativa_ingles,   	       ponto_reposicao,
          narrativa2,         	       tipo_cor,
          narrativa_espanhol, 	       codigo_cliente,
          tipo_mat_prima,     	       qtde_min_venda,
          estoque_maximo,     	       tonalidade,
          data_desativacao,   	       concentracao,
          tipo_prod_quimico,  	       motivo_uso,
          artigo_cotas,       	       complemento,
          data_lancamento,    	       qtde_minima,
          largura,            	       sequencia_tamanho,
          gramatura,         	       preco_contratipo,
          data_importacao,    	       nivel_antigo,
                              	       grupo_antigo,
                             	       subgrupo_antigo,
          classificacao_ncm,  	       item_antigo,
          numero_roteiro,              alternativa_acabado,
          numero_alternati,            codigo_contabil,
          distribuicao_cor,            prioridade_distribuicao,
          codigo_barras,               agrupador_linha,
          imprime_ficha,               alternativa_custos,
          qtde_ups,                    roteiro_custos,
          qtde_elaboracao,             nr_fios_fita,
          natur_operacao,              item_agrupador,
          sugere_item,                 produto_integracao,
          cgc_cliente_2,               estacao_dosagem_orgatex,
          cgc_cliente_4,               ind_envia_infotint,
          cgc_cliente_9,               perc_detraccion,
          grau_solidez,                classificacao_ibama,
          classificacao_naladi,        cod_servico_lst,
          numero_grafico,              qtde_min_venda_min,
          cod_tipo_volume,             destino_projeto,
          integrado_decisor,           combinacao_projeto,
          tipo_operacao,               usuario_operacao,
          flag_integracao)
         values
         (v_grade_distribuicao,        v_perc_cor,
                             	       v_tam_ponto_1,
          v_nivel_estrutura,           v_tam_ponto_2,
          v_grupo_estrutura,           v_tam_ponto_3,
          v_subgru_estrutura,          v_tam_ponto_4,
          v_item_estrutura,            v_ne_titulo,
          v_descricao_15,              v_ne_titulo_2,
          v_item_ativo,                v_ne_titulo_3,
          v_classific_fiscal,          v_ne_titulo_4,
          v_codigo_velho,              v_lfa_1,
          v_data_cadastro,             v_lfa_2,
          v_data_ult_compra,           v_lfa_3,
          v_data_ult_entrada,          v_lfa_4,
          v_data_ult_saida,            v_tear_finura,
          v_preco_medio,               v_tear_diametro,
          v_preco_ult_compra,          v_perc_alt_dim_comp,
          v_preco_custo,               v_perc_alt_dim_larg,
          v_preco_custo_info,          v_largura_proj,
          v_preco_medio_ant,           v_num_agulhas,
          v_estoque_minimo,            v_cursos,
          v_tempo_reposicao,           v_colunas,
          v_lote_multiplo,             v_perc_perdas,
          v_consumo_medio,             v_fator_correcao,
          v_narrativa,                 v_gramatura_proj,
          v_narrativa_ingles,          v_ponto_reposicao,
          v_narrativa2,                v_tipo_cor,
          v_narrativa_espanhol,        v_codigo_cliente,
          v_tipo_mat_prima,            v_qtde_min_venda,
          v_estoque_maximo,    	       v_tonalidade,
          v_data_desativacao,          v_concentracao,
          v_tipo_prod_quimico,         v_motivo_uso,
          v_artigo_cotas,              v_complemento,
          v_data_lancamento,           v_qtde_minima,
          v_largura,                   v_sequencia_tamanho,
          v_gramatura,                 v_preco_contratipo,
          v_data_importacao,           v_nivel_antigo,
                             	       v_grupo_antigo,
                               	       v_subgrupo_antigo,
          v_classificacao_ncm,         v_item_antigo,
          v_numero_roteiro,            v_alternativa_acabado,
          v_numero_alternati,          v_codigo_contabil,
          v_distribuicao_cor,	       v_prioridade_distribuicao,
          v_codigo_barras,             v_agrupador_linha,
          v_imprime_ficha,             v_alternativa_custos,
          v_qtde_ups,                  v_roteiro_custos,
          v_qtde_elaboracao,           v_nr_fios_fita,
          v_natur_operacao,            v_item_agrupador,
          v_sugere_item,               v_produto_integracao,
          v_cgc_cliente_2,             v_estacao_dosagem_orgatex,
          v_cgc_cliente_4,             v_ind_envia_infotint,
          v_cgc_cliente_9,             v_perc_detraccion,
          v_grau_solidez,              v_classificacao_ibama,
          v_classificacao_naladi,      v_cod_servico_lst,
          v_numero_grafico,            v_qtde_min_venda_min,
          v_cod_tipo_volume,           v_destino_projeto,
          v_integrado_decisor,         v_combinacao_projeto,
          v_tipo_operacao,             ws_usuario_systextil,
          1);

       exception
       when OTHERS
          then raise_application_error (-20000, 'N�o atualizou a tabela e_basi_010' || Chr(10) || SQLERRM);

       end;

      end if;
   end if;
end if;

  if deleting
  then

     v_tipo_operacao := 'E';

     v_grade_distribuicao        := :old.grade_distribuicao;		 v_perc_cor                      := :old.perc_cor;
                                                         		 v_tam_ponto_1                   := :old.tam_ponto_1;
     v_nivel_estrutura    	 := :old.nivel_estrutura;		 v_tam_ponto_2                   := :old.tam_ponto_2;
     v_grupo_estrutura           := :old.grupo_estrutura;		 v_tam_ponto_3                   := :old.tam_ponto_3;
     v_subgru_estrutura          := :old.subgru_estrutura;		 v_tam_ponto_4                   := :old.tam_ponto_4;
     v_item_estrutura            := :old.item_estrutura;		 v_ne_titulo                     := :old.ne_titulo;
     v_descricao_15              := :old.descricao_15;          	 v_ne_titulo_2                   := :old.ne_titulo_2;
     v_item_ativo                := :old.item_ativo;            	 v_ne_titulo_3                   := :old.ne_titulo_3;
     v_classific_fiscal          := :old.classific_fiscal;      	 v_ne_titulo_4                   := :old.ne_titulo_4;
     v_codigo_velho              := :old.codigo_velho;          	 v_lfa_1 		         := :old.lfa_1;
     v_data_cadastro             := :old.data_cadastro;         	 v_lfa_2 		         := :old.lfa_2;
     v_data_ult_compra           := :old.data_ult_compra;       	 v_lfa_3  		         := :old.lfa_3;
     v_data_ult_entrada          := :old.data_ult_entrada;      	 v_lfa_4                         := :old.lfa_4;
     v_data_ult_saida     	 := :old.data_ult_saida;     		 v_tear_finura                   := :old.tear_finura;
     v_preco_medio        	 := :old.preco_medio;        		 v_tear_diametro                 := :old.tear_diametro;
     v_preco_ult_compra   	 := :old.preco_ult_compra;   		 v_perc_alt_dim_comp             := :old.perc_alt_dim_comp;
     v_preco_custo        	 := :old.preco_custo;        		 v_perc_alt_dim_larg             := :old.perc_alt_dim_larg;
     v_preco_custo_info   	 := :old.preco_custo_info;   		 v_largura_proj                  := :old.largura_proj;
     v_preco_medio_ant    	 := :old.preco_medio_ant;    		 v_num_agulhas                   := :old.num_agulhas;
     v_estoque_minimo     	 := :old.estoque_minimo;     		 v_cursos                        := :old.cursos;
     v_tempo_reposicao    	 := :old.tempo_reposicao;    		 v_colunas                       := :old.colunas;
     v_lote_multiplo      	 := :old.lote_multiplo;      		 v_perc_perdas                   := :old.perc_perdas;
     v_consumo_medio      	 := :old.consumo_medio;      		 v_fator_correcao                := :old.fator_correcao;
     v_narrativa          	 := :old.narrativa;          		 v_gramatura_proj                := :old.gramatura_proj;
     v_narrativa_ingles   	 := :old.narrativa_ingles;   		 v_ponto_reposicao               := :old.ponto_reposicao;
     v_narrativa2         	 := :old.narrativa2;         		 v_tipo_cor                      := :old.tipo_cor;
     v_narrativa_espanhol 	 := :old.narrativa_espanhol; 		 v_codigo_cliente                := :old.codigo_cliente;
     v_tipo_mat_prima     	 := :old.tipo_mat_prima;     		 v_qtde_min_venda                := :old.qtde_min_venda;
     v_estoque_maximo     	 := :old.estoque_maximo;     		 v_tonalidade                    := :old.tonalidade;
     v_data_desativacao   	 := :old.data_desativacao;   		 v_concentracao                  := :old.concentracao;
     v_tipo_prod_quimico  	 := :old.tipo_prod_quimico;  		 v_motivo_uso                    := :old.motivo_uso;
     v_artigo_cotas       	 := :old.artigo_cotas;       		 v_complemento                   := :old.complemento;
     v_data_lancamento    	 := :old.data_lancamento;    		 v_qtde_minima                   := :old.qtde_minima;
     v_largura            	 := :old.largura;            		 v_sequencia_tamanho             := :old.sequencia_tamanho;
     v_gramatura          	 := :old.gramatura;          		 v_preco_contratipo              := :old.preco_contratipo;
     v_data_importacao    	 :=  sysdate();   	  		 v_nivel_antigo                  := :old.nivel_antigo;
                                                         		 v_grupo_antigo                  := :old.grupo_antigo;
                                                         		 v_subgrupo_antigo               := :old.subgrupo_antigo;
     v_classificacao_ncm  	 := :old.classificacao_ncm;  		 v_item_antigo                   := :old.item_antigo;
     v_numero_roteiro            := :old.numero_roteiro;        	 v_alternativa_acabado           := :old.alternativa_acabado;
     v_numero_alternati          := :old.numero_alternati;      	 v_codigo_contabil               := :old.codigo_contabil;
     v_distribuicao_cor          := :old.distribuicao_cor;      	 v_prioridade_distribuicao       := :old.prioridade_distribuicao;
     v_codigo_barras             := :old.codigo_barras;         	 v_agrupador_linha               := :old.agrupador_linha;
     v_imprime_ficha             := :old.imprime_ficha;         	 v_alternativa_custos            := :old.alternativa_custos;
     v_qtde_ups                  := :old.qtde_ups;              	 v_roteiro_custos                := :old.roteiro_custos;
     v_qtde_elaboracao           := :old.qtde_elaboracao;       	 v_nr_fios_fita                  := :old.nr_fios_fita;
     v_natur_operacao            := :old.natur_operacao;        	 v_item_agrupador                := :old.item_agrupador;
     v_sugere_item               := :old.sugere_item;           	 v_produto_integracao            := :old.produto_integracao;
     v_cgc_cliente_2             := :old.cgc_cliente_2;         	 v_estacao_dosagem_orgatex       := :old.estacao_dosagem_orgatex;
     v_cgc_cliente_4             := :old.cgc_cliente_4;        		 v_ind_envia_infotint            := :old.ind_envia_infotint;
     v_cgc_cliente_9             := :old.cgc_cliente_9;         	 v_perc_detraccion               := :old.perc_detraccion;
     v_grau_solidez              := :old.grau_solidez;          	 v_classificacao_ibama           := :old.classificacao_ibama;
     v_classificacao_naladi      := :old.classificacao_naladi;  	 v_cod_servico_lst               := :old.cod_servico_lst;
     v_numero_grafico            := :old.numero_grafico;        	 v_qtde_min_venda_min            := :old.qtde_min_venda_min;
     v_cod_tipo_volume           := :old.cod_tipo_volume;       	 v_destino_projeto               := :old.destino_projeto;
     v_integrado_decisor         := :old.integrado_decisor;     	 v_combinacao_projeto            := :old.combinacao_projeto;

     if v_nivel_estrutura = '2'
     then

     begin

     insert into e_basi_010
     (grade_distribuicao,   	       perc_cor,
                        	       tam_ponto_1,
      nivel_estrutura,    	       tam_ponto_2,
      grupo_estrutura,      	       tam_ponto_3,
      subgru_estrutura,     	       tam_ponto_4,
      item_estrutura,       	       ne_titulo,
      descricao_15,       	       ne_titulo_2,
      item_ativo,           	       ne_titulo_3,
      classific_fiscal,     	       ne_titulo_4,
      codigo_velho,         	       lfa_1,
      data_cadastro,        	       lfa_2,
      data_ult_compra,      	       lfa_3,
      data_ult_entrada,     	       lfa_4,
      data_ult_saida,     	       tear_finura,
      preco_medio,        	       tear_diametro,
      preco_ult_compra,   	       perc_alt_dim_comp,
      preco_custo,        	       perc_alt_dim_larg,
      preco_custo_info,   	       largura_proj,
      preco_medio_ant,    	       num_agulhas,
      estoque_minimo,     	       cursos,
      tempo_reposicao,    	       colunas,
      lote_multiplo,      	       perc_perdas,
      consumo_medio,      	       fator_correcao,
      narrativa,          	       gramatura_proj,
      narrativa_ingles,   	       ponto_reposicao,
      narrativa2,         	       tipo_cor,
      narrativa_espanhol, 	       codigo_cliente,
      tipo_mat_prima,     	       qtde_min_venda,
      estoque_maximo,     	       tonalidade,
      data_desativacao,   	       concentracao,
      tipo_prod_quimico,  	       motivo_uso,
      artigo_cotas,       	       complemento,
      data_lancamento,    	       qtde_minima,
      largura,            	       sequencia_tamanho,
      gramatura,         	       preco_contratipo,
      data_importacao,    	       nivel_antigo,
                          	       grupo_antigo,
                         	       subgrupo_antigo,
      classificacao_ncm,  	       item_antigo,
      numero_roteiro,       	       alternativa_acabado,
      numero_alternati,     	       codigo_contabil,
      distribuicao_cor,     	       prioridade_distribuicao,
      codigo_barras,        	       agrupador_linha,
      imprime_ficha,        	       alternativa_custos,
      qtde_ups,             	       roteiro_custos,
      qtde_elaboracao,      	       nr_fios_fita,
      natur_operacao,       	       item_agrupador,
      sugere_item,          	       produto_integracao,
      cgc_cliente_2,        	       estacao_dosagem_orgatex,
      cgc_cliente_4,        	       ind_envia_infotint,
      cgc_cliente_9,        	       perc_detraccion,
      grau_solidez,         	       classificacao_ibama,
      classificacao_naladi, 	       cod_servico_lst,
      numero_grafico,       	       qtde_min_venda_min,
      cod_tipo_volume,      	       destino_projeto,
      integrado_decisor,    	       combinacao_projeto,
      tipo_operacao,                   usuario_operacao,
      flag_integracao)
     values
     (v_grade_distribuicao,  	       v_perc_cor,
                         	       v_tam_ponto_1,
      v_nivel_estrutura,    	       v_tam_ponto_2,
      v_grupo_estrutura,     	       v_tam_ponto_3,
      v_subgru_estrutura,    	       v_tam_ponto_4,
      v_item_estrutura,      	       v_ne_titulo,
      v_descricao_15,        	       v_ne_titulo_2,
      v_item_ativo,          	       v_ne_titulo_3,
      v_classific_fiscal,    	       v_ne_titulo_4,
      v_codigo_velho,        	       v_lfa_1,
      v_data_cadastro,       	       v_lfa_2,
      v_data_ult_compra,     	       v_lfa_3,
      v_data_ult_entrada,    	       v_lfa_4,
      v_data_ult_saida,     	       v_tear_finura,
      v_preco_medio,        	       v_tear_diametro,
      v_preco_ult_compra,   	       v_perc_alt_dim_comp,
      v_preco_custo,        	       v_perc_alt_dim_larg,
      v_preco_custo_info,   	       v_largura_proj,
      v_preco_medio_ant,    	       v_num_agulhas,
      v_estoque_minimo,     	       v_cursos,
      v_tempo_reposicao,    	       v_colunas,
      v_lote_multiplo,      	       v_perc_perdas,
      v_consumo_medio,      	       v_fator_correcao,
      v_narrativa,          	       v_gramatura_proj,
      v_narrativa_ingles,   	       v_ponto_reposicao,
      v_narrativa2,         	       v_tipo_cor,
      v_narrativa_espanhol, 	       v_codigo_cliente,
      v_tipo_mat_prima,     	       v_qtde_min_venda,
      v_estoque_maximo,    	       v_tonalidade,
      v_data_desativacao,   	       v_concentracao,
      v_tipo_prod_quimico,  	       v_motivo_uso,
      v_artigo_cotas,       	       v_complemento,
      v_data_lancamento,    	       v_qtde_minima,
      v_largura,            	       v_sequencia_tamanho,
      v_gramatura,          	       v_preco_contratipo,
      v_data_importacao,    	       v_nivel_antigo,
                         	       v_grupo_antigo,
                           	       v_subgrupo_antigo,
      v_classificacao_ncm,  	       v_item_antigo,
      v_numero_roteiro,      	       v_alternativa_acabado,
      v_numero_alternati,    	       v_codigo_contabil,
      v_distribuicao_cor,	       v_prioridade_distribuicao,
      v_codigo_barras,       	       v_agrupador_linha,
      v_imprime_ficha,       	       v_alternativa_custos,
      v_qtde_ups,            	       v_roteiro_custos,
      v_qtde_elaboracao,     	       v_nr_fios_fita,
      v_natur_operacao,      	       v_item_agrupador,
      v_sugere_item,         	       v_produto_integracao,
      v_cgc_cliente_2,                 v_estacao_dosagem_orgatex,
      v_cgc_cliente_4,                 v_ind_envia_infotint,
      v_cgc_cliente_9,                 v_perc_detraccion,
      v_grau_solidez,                  v_classificacao_ibama,
      v_classificacao_naladi,          v_cod_servico_lst,
      v_numero_grafico,                v_qtde_min_venda_min,
      v_cod_tipo_volume,               v_destino_projeto,
      v_integrado_decisor,             v_combinacao_projeto,
      v_tipo_operacao,                 ws_usuario_systextil,
      1);

      exception
      when OTHERS
         then raise_application_error (-20000, 'N�o atualizou a tabela e_basi_010' || Chr(10) || SQLERRM);

      end;
   end if;
   end if;
end inter_tr_basi_010_integracao;
-- ALTER TRIGGER "INTER_TR_BASI_010_INTEGRACAO" DISABLE
 

/

exec inter_pr_recompile;

