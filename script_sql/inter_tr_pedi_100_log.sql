create or replace trigger inter_tr_pedi_100_log
    after insert or delete or update of
    data_entr_venda
    ,obs_producao
    ,natop_pv_nat_oper
    ,trans_pv_forne9,
    trans_pv_forne4
    ,trans_pv_forne2
    ,tipo_frete
    ,codigo_empresa
    ,codigo_moeda
    ,cod_cancelamento
    ,status_pedido
    ,status_comercial
    ,situacao_venda
    ,status_expedicao
    ,cli_ped_cgc_cli9
    ,cli_ped_cgc_cli4
    ,cli_ped_cgc_cli2
    ,cod_rep_cliente
    ,codigo_administr
    ,tipo_comissao
    ,perc_comis_venda
    ,comissao_administr
    ,cond_pgto_venda
    ,colecao_tabela
    ,mes_tabela
    ,sequencia_tabela
    ,seq_end_entrega
    ,seq_end_cobranca
    ,liberado_faturar
    ,aceita_antecipacao
    ,permite_parcial
    ,pedido_suppliercard
    ,data_prev_receb
    ,cod_banco
    ,perc_comissao_fatu_repres
    ,perc_comissao_fatu_adm
    ,data_base_fatur
    ,volumes_acomp
    ,volumes_tecido
    ,desconto1
    ,desconto2
    ,desconto3
    ,origem_pedido
    ,cod_ped_cliente
    ,numero_controle

on pedi_100

for each row

declare

    ws_sid                      number(9);
    ws_empresa                  number(3);
    v_executa_trigger           number(1);
    ws_usuario_rede             varchar2(20);
    ws_maquina_rede             varchar2(40);
    ws_aplicativo               varchar2(20);
    ws_usuario_systextil        varchar2(250);
    ws_locale_usuario           varchar2(5);
    ws_nome_cliente_o           varchar2(60);
    ws_nome_cliente_n           varchar2(60);
    ws_descr_nat_oper_o         varchar2(60);
    ws_descr_nat_oper_n         varchar2(60);
    ws_nome_transportadora_o    varchar2(60);
    ws_nome_transportadora_n    varchar2(60);
    ws_descr_moeda_o            varchar2(40);
    ws_descr_moeda_n            varchar2(40);
    ws_descr_cancelamento_o     varchar2(40);
    ws_descr_cancelamento_n     varchar2(30);
    ws_descr_status_pedido_o    varchar2(30);
    ws_descr_status_pedido_n    varchar2(30);
    ws_descr_status_comer_o     varchar2(30);
    ws_descr_status_comer_n     varchar2(30);
    ws_descr_status_expedicao_o varchar2(30);
    ws_descr_status_expedicao_n varchar2(30);
    ws_descr_situacao_o         varchar2(30);
    ws_descr_situacao_n         varchar2(30);
    ws_nome_rep_cliente_o       varchar2(60);
    ws_nome_rep_cliente_n       varchar2(60);
    ws_nome_admin_cliente_o     varchar2(60);
    ws_nome_admin_cliente_n     varchar2(60);
    ws_descr_cond_pgto_o        varchar2(40);
    ws_descr_cond_pgto_n        varchar2(40);
    ws_descr_tabela_o           varchar2(40);
    ws_descr_tabela_n           varchar2(40);
    ws_descr_frete_o            varchar2(20);
    ws_descr_frete_n            varchar2(20);
    ws_pedido_suppliercard_o    varchar2(1);
    ws_pedido_suppliercard_n    varchar2(1);
    long_aux                    varchar2(2000);
    
begin

    -- Dados do usuario logado
    inter_pr_dados_usuario(
        ws_usuario_rede
        ,ws_maquina_rede
        ,ws_aplicativo
        ,ws_sid
        ,ws_usuario_systextil
        ,ws_empresa
        ,ws_locale_usuario
        );

    if inserting then

        if :new.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;

    end if;

    if updating then
        if :old.executa_trigger = 1 or :new.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;

    end if;

    if deleting then

        if :old.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;

    end if;

    if v_executa_trigger = 0 then

        if inserting then
        
            insert into hist_100(
                tabela
                ,operacao
                ,data_ocorr
                ,aplicacao
                ,usuario_rede
                ,maquina_rede
                ,num01
                ,long01
            )VALUES (
                'PEDI_100'
                ,'I'
                ,sysdate
                ,ws_aplicativo
                ,ws_usuario_rede
                ,ws_maquina_rede
                ,:new.pedido_venda
                ,'                              '||(

                    inter_fn_buscar_tag(
                        'lb34978#CAPA DO PEDIDO DE VENDAS'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || chr(10)
                    
                    ||chr(10)||
                    inter_fn_buscar_tag(
                        'lb04619#CLIENTE:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :new.cli_ped_cgc_cli9  || '/' || :new.cli_ped_cgc_cli4  || '-' || :new.cli_ped_cgc_cli2
                    
                    ||chr(10)||
                    inter_fn_buscar_tag(
                        'lb02920#DATA EMBARQUE:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :new.data_entr_venda
                    
                    ||chr(10)||
                    inter_fn_buscar_tag(
                        'lb34979#OBSERVAC?ES DE PRODUC?O:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| substr(:new.obs_producao,1,200)
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb00244#NAT. DE OPERAC?O:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.natop_pv_nat_oper
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb05956#TRANSPORTADORA:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.trans_pv_forne9 || '/' || :new.trans_pv_forne4   || '-' || :new.trans_pv_forne2
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb07856#TIPO FRETE:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.tipo_frete
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb09298#PEDIDO CLIENTE:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.COD_PED_CLIENTE
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb07207#EMPRESA:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.codigo_empresa 

                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb13146#MOEDA:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :new.codigo_moeda
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb04616#CANCELAMENTO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.cod_cancelamento
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb06631#STATUS PEDIDO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.status_pedido 
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb10627#STATUS COMERCIAL:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.status_comercial
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb06630#STATUS EXPEDIC?O:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.status_expedicao
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb09662#SITUAC?O PEDIDO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :new.situacao_venda
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb00294#REPRESENTANTE:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.cod_rep_cliente
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb01633#ADMINISTRADOR:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.codigo_administr
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb05911#TIPO COMISS?O:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.tipo_comissao
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb34838#% COMIS. REPRES.:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :new.perc_comis_venda
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb03857#% COMISS?O (ADMIN):'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.comissao_administr
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb02139#CONDIC?O PAGTO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :new.cond_pgto_venda
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb03554#ENDERECO DE ENTREGA:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :new.seq_end_cobranca
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb03549#END.COBRANCA:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.seq_end_cobranca
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb16701#TABELA PRECO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.colecao_tabela || '-' || :new.mes_tabela || '-' || :new.sequencia_tabela
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb40929#ACEITAR ANTECIPACAO DO PEDIDO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.aceita_antecipacao
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb40930#PERMITE PARCIAL:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :new.permite_parcial
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb45035#SIT.SUPPLIERCARD:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :new.pedido_suppliercard
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb33157#DATA PREV. RECEB.'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.data_prev_receb
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb05205#PORTADOR:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.cod_banco
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb45845#% DE COMIS. REPRES. FAT.:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.perc_comissao_fatu_repres
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb45846#% DE COMIS. ADMIN. FAT.:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.perc_comissao_fatu_adm
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb02855#DATA BASE FATUR:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.data_base_fatur
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb49087#VOLUMES ACOMP.:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.volumes_acomp
                
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb49164#VOLUMES TECIDO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.volumes_tecido
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb50648#	DESCONTO1:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.desconto1
                    
                    ||chr(10)||
                    inter_fn_buscar_tag(
                        'lb50649#	DESCONTO2:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.desconto2
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb50650#	DESCONTO3:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.desconto3
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb08441#	Nr. Interno:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.numero_controle
                    
                    ||chr(10)||
                    
                    inter_fn_buscar_tag(
                        'lb10163#	ORIGEM PEDIDO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :new.origem_pedido
                )
            );
      end if;


    if updating and(
        
           :old.cli_ped_cgc_cli9          <> :new.cli_ped_cgc_cli9            
        or :old.cli_ped_cgc_cli4          <> :new.cli_ped_cgc_cli4            
        or :old.cli_ped_cgc_cli2          <> :new.cli_ped_cgc_cli2            
        or :old.data_entr_venda           <> :new.data_entr_venda             
        or :old.obs_producao              <> :new.obs_producao                
        or :old.natop_pv_nat_oper         <> :new.natop_pv_nat_oper           
        or :old.trans_pv_forne9           <> :new.trans_pv_forne9             
        or :old.trans_pv_forne4           <> :new.trans_pv_forne4             
        or :old.trans_pv_forne2           <> :new.trans_pv_forne2             
        or :old.tipo_frete                <> :new.tipo_frete                  
        or :old.codigo_empresa            <> :new.codigo_empresa              
        or :old.codigo_moeda              <> :new.codigo_moeda                
        or :old.cod_cancelamento          <> :new.cod_cancelamento            
        or :old.status_pedido             <> :new.status_pedido               
        or :old.status_comercial          <> :new.status_comercial            
        or :old.status_expedicao          <> :new.status_expedicao            
        or :old.situacao_venda            <> :new.situacao_venda              
        or :old.cod_rep_cliente           <> :new.cod_rep_cliente             
        or :old.codigo_administr          <> :new.codigo_administr            
        or :old.tipo_comissao             <> :new.tipo_comissao               
        or :old.perc_comis_venda          <> :new.perc_comis_venda            
        or :old.comissao_administr        <> :new.comissao_administr          
        or :old.cond_pgto_venda           <> :new.cond_pgto_venda             
        or :old.colecao_tabela            <> :new.colecao_tabela              
        or :old.mes_tabela                <> :new.mes_tabela                  
        or :old.sequencia_tabela          <> :new.sequencia_tabela            
        or :old.seq_end_entrega           <> :new.seq_end_entrega             
        or :old.seq_end_cobranca          <> :new.seq_end_cobranca            
        or :old.liberado_faturar          <> :new.liberado_faturar            
        or :old.aceita_antecipacao        <> :new.aceita_antecipacao          
        or :old.permite_parcial           <> :new.permite_parcial             
        or :old.pedido_suppliercard       <> :new.pedido_suppliercard         
        or :old.data_prev_receb           <> :new.data_prev_receb             
        or :old.cod_banco                 <> :new.cod_banco                   
        or :old.perc_comissao_fatu_repres <> :new.perc_comissao_fatu_repres   
        or :old.perc_comissao_fatu_adm    <> :new.perc_comissao_fatu_adm      
        or :old.data_base_fatur           <> :new.data_base_fatur             
        or :old.volumes_acomp             <> :new.volumes_acomp               
        or :old.volumes_tecido            <> :new.volumes_tecido              
        or :old.desconto1                 <> :new.desconto1                   
        or :old.desconto2                 <> :new.desconto2                   
        or :old.desconto3                 <> :new.desconto3                   
        or :old.cod_ped_cliente           <> :new.cod_ped_cliente             
        or :old.origem_pedido             <> :new.origem_pedido 
        or (:old.cod_ped_cliente is null and :new.cod_ped_cliente is not null)              
        or (:old.obs_producao    is null and :new.obs_producao    is not null)
        or (:old.data_prev_receb is null and :new.data_prev_receb is not null)
        or (:new.data_prev_receb is null and :old.data_prev_receb is not null)
        or (:old.data_base_fatur is null and :new.data_base_fatur is not null)
        or (:new.data_base_fatur is null and :old.data_base_fatur is not null)
        or :old.numero_controle             <> :new.numero_controle 

    )
    then

        long_aux := long_aux ||'                              '||
            inter_fn_buscar_tag('lb34978#CAPA DO PEDIDO DE VENDAS'
            ,ws_locale_usuario
            ,ws_usuario_systextil
        ) ||chr(10)||chr(10);
        
        if (:old.cli_ped_cgc_cli9 <> :new.cli_ped_cgc_cli9) or (:old.cli_ped_cgc_cli4 <> :new.cli_ped_cgc_cli4) or (:old.cli_ped_cgc_cli2 <> :new.cli_ped_cgc_cli2) then

            select
                nvl(pedi_010.nome_cliente,' ')
            into
                ws_nome_cliente_o
            from pedi_010
            where   pedi_010.cgc_9 = :old.cli_ped_cgc_cli9
                and pedi_010.cgc_4 = :old.cli_ped_cgc_cli4
                and pedi_010.cgc_2 = :old.cli_ped_cgc_cli2;

            select
                nvl(pedi_010.nome_cliente,' ')
            into ws_nome_cliente_n
            from pedi_010
            where   pedi_010.cgc_9 = :new.cli_ped_cgc_cli9
                and pedi_010.cgc_4 = :new.cli_ped_cgc_cli4
                and pedi_010.cgc_2 = :new.cli_ped_cgc_cli2;

            long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb04619#CLIENTE:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || to_char(:old.cli_ped_cgc_cli9,'000000000')  || '/' || to_char(:old.cli_ped_cgc_cli4,'0000') || '-' || to_char(:old.cli_ped_cgc_cli2,'00') || ' - ' || ws_nome_cliente_o || ' -> ' || chr(10);

            long_aux := long_aux ||'                : ' || to_char(:new.cli_ped_cgc_cli9,'000000000') || '/'|| to_char(:new.cli_ped_cgc_cli4,'0000') || '-'|| to_char(:new.cli_ped_cgc_cli2,'00') || ' - '|| ws_nome_cliente_n || chr(10);

        end if;

        if :old.data_entr_venda <> :new.data_entr_venda then

            long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb02920#DATA EMBARQUE:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.data_entr_venda || ' - ' || :new.data_entr_venda || chr(10);

        end if;
        
        if :old.numero_controle <> :new.numero_controle then
        
            long_aux := long_aux || inter_fn_buscar_tag(
                'lb08441#Nr. Interno:	'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.numero_controle || ' -> ' || :new.numero_controle || chr(10);

        end if;      

        if (:old.data_base_fatur <> :new.data_base_fatur) or ((:old.data_base_fatur is null) and (:new.data_base_fatur is not null)) or ((:new.data_base_fatur is null) and (:old.data_base_fatur is not null)) then
           
            if ((:old.data_base_fatur is null) and (:new.data_base_fatur is not null)) then 
            
                long_aux := long_aux ||
                    inter_fn_buscar_tag(
                        'lb02855#DATA BASE FATUR:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' ||  ' -> ' || :new.data_base_fatur || chr(10);

            end if;
            
            if ((:new.data_base_fatur is null) and (:old.data_base_fatur is not null)) then

                long_aux := long_aux ||
                    inter_fn_buscar_tag(
                        'lb02855#DATA BASE FATUR:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :old.data_base_fatur || ' -> ' || chr(10);  
                               
            end if;
            
            if (:old.data_base_fatur <> :new.data_base_fatur) then

                long_aux := long_aux ||
                    inter_fn_buscar_tag(
                        'lb02855#DATA BASE FATUR:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                ) || ' ' || :old.data_base_fatur || ' -> ' || :new.data_base_fatur || chr(10);

            end if;

        end if;



        if (:old.natop_pv_nat_oper <> :new.natop_pv_nat_oper) then

            select
                nvl(pedi_080.descr_nat_oper, ' ')
            into ws_descr_nat_oper_o
            from pedi_080
            where   pedi_080.natur_operacao = :old.natop_pv_nat_oper
                and pedi_080.estado_natoper = :old.natop_pv_est_oper;

            select
                nvl(pedi_080.descr_nat_oper, ' ')
            into ws_descr_nat_oper_n
            from pedi_080
            where   pedi_080.natur_operacao = :new.natop_pv_nat_oper
                and pedi_080.estado_natoper = :new.natop_pv_est_oper;

            long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb00244#NAT. DE OPERAC?O:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' '|| :old.natop_pv_nat_oper || ' - '|| ws_descr_nat_oper_o || ' -> '|| :new.natop_pv_nat_oper || ' - '|| ws_descr_nat_oper_n|| chr(10);

        end if;

        if ((:old.trans_pv_forne9 <> :new.trans_pv_forne9) or (:old.trans_pv_forne4 <> :new.trans_pv_forne4) or (:old.trans_pv_forne2 <> :new.trans_pv_forne2)) then

            select
                nvl(supr_010.nome_fornecedor, ' ')
            into ws_nome_transportadora_o
            from supr_010
            where   supr_010.fornecedor9 = :old.trans_pv_forne9
                and supr_010.fornecedor4 = :old.trans_pv_forne4
                and supr_010.fornecedor2 = :old.trans_pv_forne2;

            select
                nvl(supr_010.nome_fornecedor, ' ')
            into ws_nome_transportadora_n
            from supr_010
            where   supr_010.fornecedor9 = :new.trans_pv_forne9
                and supr_010.fornecedor4 = :new.trans_pv_forne4
                and supr_010.fornecedor2 = :new.trans_pv_forne2;

            long_aux := long_aux     ||
                inter_fn_buscar_tag(
                    'lb05956#TRANSPORTADORA:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || to_char(:old.trans_pv_forne9,'000000000') || '/' || to_char(:old.trans_pv_forne4,'0000') || '-' || to_char(:old.trans_pv_forne2,'00') || ' - ' || ws_nome_transportadora_o || ' -> ' || chr(10);

            long_aux := long_aux || '                  ' || to_char(:new.trans_pv_forne9,'000000000') || '/' || to_char(:new.trans_pv_forne4,'0000') || '-' || to_char(:new.trans_pv_forne2,'00') || ' - ' || ws_nome_transportadora_n || chr(10);

        end if;


        if :old.tipo_frete <> :new.tipo_frete then

            if :old.tipo_frete = 1 then
                ws_descr_frete_o := inter_fn_buscar_tag('lb19565#PAGO',ws_locale_usuario,ws_usuario_systextil);
            end if;

            if :old.tipo_frete = 2 then
                ws_descr_frete_o := inter_fn_buscar_tag('lb19566A PAGAR',ws_locale_usuario,ws_usuario_systextil);
            end if;

            if :old.tipo_frete = 4 then
                ws_descr_frete_o := inter_fn_buscar_tag('lb19567#CORTESIA',ws_locale_usuario,ws_usuario_systextil);
            end if;

            if :new.tipo_frete = 1 then
                ws_descr_frete_n := inter_fn_buscar_tag('lb19565#PAGO',ws_locale_usuario,ws_usuario_systextil);
            end if;

            if :new.tipo_frete = 2 then
                ws_descr_frete_n := inter_fn_buscar_tag('lb19566A PAGAR',ws_locale_usuario,ws_usuario_systextil);
            end if;

            if :new.tipo_frete = 4 then
                ws_descr_frete_n := inter_fn_buscar_tag('lb19567#CORTESIA',ws_locale_usuario,ws_usuario_systextil);
            end if;

            long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb07856#TIPO FRETE:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.tipo_frete || ' - ' || ws_descr_frete_o || ' -> ' || :new.tipo_frete || ' - ' || ws_descr_frete_n || chr(10);

        end if;

        if :old.codigo_empresa <> :new.codigo_empresa then

            long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb07207#EMPRESA:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.codigo_empresa || ' -> ' || :new.codigo_empresa || chr(10);

        end if;

        if ((:old.COD_PED_CLIENTE <> :new.COD_PED_CLIENTE) or (:old.cod_ped_cliente is null and :new.cod_ped_cliente is not null)) then

            long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb09298#PEDIDO CLIENTE:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.COD_PED_CLIENTE || ' -> ' || :new.COD_PED_CLIENTE || chr(10);

        end if;


        if :old.codigo_moeda <> :new.codigo_moeda then
    
            select
                nvl(basi_265.descricao, ' ')
            into ws_descr_moeda_o
            from basi_265
            where basi_265.codigo_moeda = :old.codigo_moeda;

            select
                nvl(basi_265.descricao, ' ')
            into ws_descr_moeda_n
            from basi_265
            where basi_265.codigo_moeda = :new.codigo_moeda;

            long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb13146#MOEDA:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.codigo_moeda || ' - ' || ws_descr_moeda_o || ' -> ' || :new.codigo_moeda || ' - ' || ws_descr_moeda_n || chr(10);

        end if;

        if :old.cod_cancelamento <> :new.cod_cancelamento then

            select
                nvl(pedi_140.desc_canc_pedido, ' ') 
            into ws_descr_cancelamento_o
            from pedi_140
            where pedi_140.cod_canc_pedido = :old.cod_cancelamento;

            select
                nvl(pedi_140.desc_canc_pedido, ' ')
            into ws_descr_cancelamento_n
            from pedi_140
            where pedi_140.cod_canc_pedido = :new.cod_cancelamento;

            long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb04616#CANCELAMENTO:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.cod_cancelamento || ' - ' || ws_descr_cancelamento_o || ' -> ' || :new.cod_cancelamento || ' - ' || ws_descr_cancelamento_n || chr(10);

        end if;

        if :old.status_pedido <> :new.status_pedido then

            if :old.status_pedido = 0 then

                ws_descr_status_pedido_o := inter_fn_buscar_tag(
                    'lb19858#DIGITADO'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );      

            end if;

            if :old.status_pedido = 1 then

                ws_descr_status_pedido_o := inter_fn_buscar_tag(
                    'lb26366#NO FINANCEIRO'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );   

            end if;

            if :old.status_pedido = 2 then

                ws_descr_status_pedido_o := inter_fn_buscar_tag(
                    'lb26367#LIB. FINANCEIRO'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ); 

            end if;

            if :old.status_pedido = 3 then

                ws_descr_status_pedido_o := inter_fn_buscar_tag(
                    'lb26368#NO FATURAMENTO'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );  

            end if;

            if :old.status_pedido = 4 then

                ws_descr_status_pedido_o := inter_fn_buscar_tag(
                    'lb26369#A CANCELAR'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );      

            end if;

            if :old.status_pedido = 5 then

                ws_descr_status_pedido_o := inter_fn_buscar_tag(
                    'lb19895#CANCELADO'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );       

            end if;

            if :old.status_pedido = 9 then

                ws_descr_status_pedido_o := inter_fn_buscar_tag(
                    'lb26370#EM ABERTO NA WEB'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;

            if :new.status_pedido = 0 then

                ws_descr_status_pedido_n := inter_fn_buscar_tag(
                    'lb19858#DIGITADO'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );  

            end if;

            if :new.status_pedido = 1 then

                ws_descr_status_pedido_n := inter_fn_buscar_tag(
                    'lb26366#NO FINANCEIRO'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );   

            end if;

            if :new.status_pedido = 2 then

                ws_descr_status_pedido_n := inter_fn_buscar_tag(
                    'lb26367#LIB. FINANCEIRO'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ); 

            end if;

            if :new.status_pedido = 3 then

                ws_descr_status_pedido_n := inter_fn_buscar_tag(
                    'lb26368#NO FATURAMENTO'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );  

            end if;

            if :new.status_pedido = 4 then

                ws_descr_status_pedido_n := inter_fn_buscar_tag(
                    'lb26369#A CANCELAR'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );      

            end if;

            if :new.status_pedido = 5 then

                ws_descr_status_pedido_n := inter_fn_buscar_tag(
                    'lb19895#CANCELADO'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );    
                  
            end if;

            if :new.status_pedido = 9 then

                ws_descr_status_pedido_n := inter_fn_buscar_tag(
                    'lb26370#EM ABERTO NA WEB'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;

            long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb06631#STATUS PEDIDO:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.status_pedido || ' - ' || ws_descr_status_pedido_o || ' -> ' || :new.status_pedido || ' - ' || ws_descr_status_pedido_n || chr(10);

        end if;

        if :old.status_comercial <> :new.status_comercial then

            if :old.status_comercial = 0 then

                ws_descr_status_comer_o := inter_fn_buscar_tag(
                    'lb26362#SUGERIR'              
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );
            
            end if;

            if :old.status_comercial = 1 then

                ws_descr_status_comer_o := inter_fn_buscar_tag(
                    'lb26363#N?O SUGERIR'          
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );
            
            end if;

            if :old.status_comercial = 2 then

                ws_descr_status_comer_o := inter_fn_buscar_tag(
                    'lb26364#N?O FATURAR'          
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );
            
            end if;

            if :old.status_comercial = 3 then

                ws_descr_status_comer_o := inter_fn_buscar_tag(
                    'lb26365#N?O DESEMP/DESAMARRAR'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );
            
            end if;

            if :new.status_comercial = 0 then

                ws_descr_status_comer_n := inter_fn_buscar_tag(
                    'lb26362#SUGERIR'              
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );
            
            end if;

            if :new.status_comercial = 1 then

                ws_descr_status_comer_n := inter_fn_buscar_tag(
                    'lb26363#N?O SUGERIR'          
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );
            
            end if;

            if :new.status_comercial = 2 then

                ws_descr_status_comer_n := inter_fn_buscar_tag(
                    'lb26364#N?O FATURAR'          
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );
            
            end if;

            if :new.status_comercial = 3 then

                ws_descr_status_comer_n := inter_fn_buscar_tag(
                    'lb26365#N?O DESEMP/DESAMARRAR'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );
            
            end if;


            long_aux := long_aux ||
            inter_fn_buscar_tag(
                'lb10627#STATUS COMERCIAL:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.status_comercial || ' - ' || ws_descr_status_comer_o || ' -> ' || :new.status_comercial || ' - ' || ws_descr_status_comer_n || chr(10);

        end if;

        if :old.status_expedicao <> :new.status_expedicao then

            if :old.status_expedicao = 0 then

                ws_descr_status_expedicao_o := inter_fn_buscar_tag(
                    'lb19858#DIGITADO'      
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :old.status_expedicao = 1 then

                ws_descr_status_expedicao_o := inter_fn_buscar_tag(
                    'lb32498#LIBERADO'      
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :old.status_expedicao = 2 then

                ws_descr_status_expedicao_o := inter_fn_buscar_tag(
                    'lb07539#MONTADO'       
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :old.status_expedicao = 3 then

                ws_descr_status_expedicao_o := inter_fn_buscar_tag(
                    'lb00436#ACOMPANHAMENTO'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :old.status_expedicao = 4 then

                ws_descr_status_expedicao_o := inter_fn_buscar_tag(
                    'lb12178#BLOQUEADO'     
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :old.status_expedicao = 5 then

                ws_descr_status_expedicao_o := inter_fn_buscar_tag(
                    'lb22361#ETIQUETA'      
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :old.status_expedicao = 9 then

                ws_descr_status_expedicao_o := inter_fn_buscar_tag(
                    'lb03741#FATURADO'      
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :new.status_expedicao = 0 then

                ws_descr_status_expedicao_n := inter_fn_buscar_tag(
                    'lb19858#DIGITADO'      
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :new.status_expedicao = 1 then

                ws_descr_status_expedicao_n := inter_fn_buscar_tag(
                    'lb32498#LIBERADO'      
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :new.status_expedicao = 2 then

                ws_descr_status_expedicao_n := inter_fn_buscar_tag(
                    'lb07539#MONTADO'       
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :new.status_expedicao = 3 then

                ws_descr_status_expedicao_n := inter_fn_buscar_tag(
                    'lb00436#ACOMPANHAMENTO'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :new.status_expedicao = 4 then

                ws_descr_status_expedicao_n := inter_fn_buscar_tag(
                    'lb12178#BLOQUEADO'     
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :new.status_expedicao = 5 then

                ws_descr_status_expedicao_n := inter_fn_buscar_tag(
                    'lb22361#ETIQUETA'      
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :new.status_expedicao = 9 then

                ws_descr_status_expedicao_n := inter_fn_buscar_tag(
                    'lb03741#FATURADO'     
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb06630#STATUS EXPEDIC?O:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            )|| ' ' || :old.status_expedicao || ' - ' || ws_descr_status_expedicao_o || ' -> ' || :new.status_expedicao || ' - ' || ws_descr_status_expedicao_n || chr(10);

        end if;

        if :old.situacao_venda <> :new.situacao_venda then

            if :old.situacao_venda =  0 then

                ws_descr_situacao_o := inter_fn_buscar_tag(
                    'lb25663#PEDIDO LIBERADO'      
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :old.situacao_venda =  5 then

                ws_descr_situacao_o := inter_fn_buscar_tag(
                    'lb25665#PEDIDO SUSPENSO'      
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :old.situacao_venda =  9 then

                ws_descr_situacao_o := inter_fn_buscar_tag(
                    'lb24783#FATURADO PARCIAL'     
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :old.situacao_venda = 10 then

                ws_descr_situacao_o := inter_fn_buscar_tag(
                    'lb24784#FATURADO TOTAL'       
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :old.situacao_venda = 15 then

                ws_descr_situacao_o := inter_fn_buscar_tag(
                    'lb26374#PEDIDO C/NF CANCELADA'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :new.situacao_venda =  0 then

                ws_descr_situacao_n := inter_fn_buscar_tag(
                    'lb25663#PEDIDO LIBERADO'      
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :new.situacao_venda =  5 then

                ws_descr_situacao_n := inter_fn_buscar_tag(
                    'lb25665#PEDIDO SUSPENSO'      
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :new.situacao_venda =  9 then

                ws_descr_situacao_n := inter_fn_buscar_tag(
                    'lb24783#FATURADO PARCIAL'     
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :new.situacao_venda = 10 then

                ws_descr_situacao_n := inter_fn_buscar_tag(
                    'lb24784#FATURADO TOTAL'       
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            
            if :new.situacao_venda = 15 then

                ws_descr_situacao_n := inter_fn_buscar_tag(
                    'lb26374#PEDIDO C/NF CANCELADA'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                );

            end if;
            

                long_aux := long_aux || inter_fn_buscar_tag(
                    'lb09662#SITUAC?O PEDIDO:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.situacao_venda || ' - ' || ws_descr_situacao_o || ' -> ' || :new.situacao_venda || ' - ' || ws_descr_situacao_n || chr(10);

        end if;

        if :old.cod_rep_cliente <> :new.cod_rep_cliente then

            begin

                select
                    nvl(pedi_020.nome_rep_cliente, ' ')
                into
                    ws_nome_rep_cliente_o
                from pedi_020
                where   pedi_020.cod_rep_cliente = :old.cod_rep_cliente
                    and pedi_020.tipo_repr       <> 5;

            exception
                when no_data_found then
                    ws_nome_rep_cliente_o := ' ';

            end;

            begin

                select
                    nvl(pedi_020.nome_rep_cliente, ' ')
                into
                    ws_nome_rep_cliente_n
                from pedi_020
                where   pedi_020.cod_rep_cliente = :new.cod_rep_cliente
                    and pedi_020.tipo_repr       <> 5;

            exception
                when no_data_found then
                    ws_nome_rep_cliente_n := ' ';

            end;

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb00294#REPRESENTANTE:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' '|| :old.cod_rep_cliente || ' - '|| ws_nome_rep_cliente_o || ' -> ' ||chr(10);

            long_aux := long_aux || '                  '  || :new.cod_rep_cliente || ' - ' || ws_nome_rep_cliente_n || chr(10);

         end if;

        if :old.codigo_administr <> :new.codigo_administr then

            begin

                select
                    nvl(pedi_020.nome_rep_cliente, ' ')
                into
                    ws_nome_admin_cliente_o
                from pedi_020
                where   pedi_020.cod_rep_cliente = :old.codigo_administr
                    and pedi_020.tipo_repr       <> 5;

            exception
                when no_data_found then
                    ws_nome_admin_cliente_o := ' ';

            end;

            begin

                select
                    nvl(pedi_020.nome_rep_cliente, ' ')
                into
                    ws_nome_admin_cliente_n
                from pedi_020
                where   pedi_020.cod_rep_cliente = :new.codigo_administr
                    and pedi_020.tipo_repr       <> 5;
                    
            exception
                when no_data_found then
                    ws_nome_admin_cliente_n := ' ';

            end;

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb01633#ADMINISTRADOR:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.codigo_administr || ' - ' || ws_nome_admin_cliente_o || ' -> ' || chr(10);

            long_aux := long_aux || '                  '  || :new.codigo_administr || ' - ' || ws_nome_admin_cliente_n || chr(10);

        end if;

        if :old.tipo_comissao <> :new.tipo_comissao then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb05911#TIPO COMISS?O:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.tipo_comissao || ' -> ' || :new.tipo_comissao || chr(10);

        end if;

        if :old.perc_comis_venda <> :new.perc_comis_venda then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb34838#% COMIS. REPRES.:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.perc_comis_venda || ' -> ' || :new.perc_comis_venda || chr(10);

        end if;

        if :old.comissao_administr <> :new.comissao_administr then
        
            long_aux := long_aux ||inter_fn_buscar_tag(
                'lb03857#% COMISS?O (ADMIN):'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            )|| ' ' || :old.comissao_administr || ' -> ' || :new.comissao_administr || chr(10);

        end if;

        if :old.cond_pgto_venda <> :new.cond_pgto_venda then

            select
                nvl(pedi_070.descr_pg_cliente, ' ')
            into
                ws_descr_cond_pgto_o
            from pedi_070
            where pedi_070.cond_pgt_cliente = :old.cond_pgto_venda;

            select
                nvl(pedi_070.descr_pg_cliente, ' ')
            into
                ws_descr_cond_pgto_n 
            from pedi_070
            where pedi_070.cond_pgt_cliente = :new.cond_pgto_venda;

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb02139#CONDIC?O PAGTO:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.cond_pgto_venda || ' - ' || ws_descr_cond_pgto_o || ' -> ' || chr(10);

            long_aux := long_aux || '                  '  || :new.cond_pgto_venda || ' - ' || ws_descr_cond_pgto_n || chr(10);

        end if;

        if ((:old.colecao_tabela <> :new.colecao_tabela) or (:old.mes_tabela <> :new.mes_tabela) or (:old.sequencia_tabela <> :new.sequencia_tabela)) then

            if ((:old.colecao_tabela > 0) or (:old.mes_tabela > 0) or (:old.sequencia_tabela > 0)) then

                select
                    nvl(pedi_090.descricao, ' ')
                into
                    ws_descr_tabela_o
                from pedi_090
                where   pedi_090.col_tabela_preco = :old.colecao_tabela
                    and pedi_090.mes_tabela_preco = :old.mes_tabela
                    and pedi_090.seq_tabela_preco = :old.sequencia_tabela;

            end if;

            if :new.colecao_tabela > 0 or :new.mes_tabela > 0 or :new.sequencia_tabela > 0 then

                select
                    nvl(pedi_090.descricao, ' ')
                into
                    ws_descr_tabela_n
                from pedi_090
                where   pedi_090.col_tabela_preco = :new.colecao_tabela
                    and pedi_090.mes_tabela_preco = :new.mes_tabela
                    and pedi_090.seq_tabela_preco = :new.sequencia_tabela;

            end if;

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb03554#ENDERECO DE ENTREGA:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.colecao_tabela || '-' || :old.mes_tabela || '-' || :old.sequencia_tabela || ' - ' || ws_descr_tabela_o || ' -> ' || chr(10);

            long_aux := long_aux || '                  ' || :new.colecao_tabela || '-' || :new.mes_tabela || '-' || :new.sequencia_tabela  || ' - ' || ws_descr_tabela_n || chr(10);

        end if;

        if ((:old.obs_producao is null) and (:new.obs_producao is not null) or (:old.obs_producao <> :new.obs_producao)) then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb34979#OBSERVAC?ES DE PRODUC?O:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || substr(:old.obs_producao,1,200) || ' -> ' || chr(10);

            long_aux := long_aux || '                   ' || substr(:new.obs_producao,1,200) || chr(10);

        end if;

        if :old.seq_end_entrega <> :new.seq_end_entrega then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb03554#ENDERECO DE ENTREGA:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            )|| ' ' || :old.seq_end_entrega || ' -> ' || :new.seq_end_entrega || chr(10);

        end if;

        if :old.seq_end_cobranca <> :new.seq_end_cobranca then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb03549#END.COBRANCA:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.seq_end_cobranca || ' -> ' || :new.seq_end_cobranca || chr(10);

        end if;

        if :old.liberado_faturar <> :new.liberado_faturar then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb41952#LIBERADO FATURAR:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            )|| ' ' || :old.liberado_faturar || ' -> ' || :new.liberado_faturar || chr(10);

        end if;

        if :old.aceita_antecipacao <> :new.aceita_antecipacao then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb40929#ACEITAR ANTECIPACAO DO PEDIDO'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.aceita_antecipacao || ' -> ' || :new.aceita_antecipacao || chr(10);

        end if;

        if :old.permite_parcial <> :new.permite_parcial then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb40930#PERMITE PARCIAL'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.permite_parcial || ' -> ' || :new.permite_parcial || chr(10);

        end if;

        if :old.pedido_suppliercard <> :new.pedido_suppliercard then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb45035#SIT.SUPPLIERCARD:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.pedido_suppliercard || ' -> ' || :new.pedido_suppliercard || chr(10);
            
        end if;

        if ((:old.data_prev_receb <> :new.data_prev_receb) or ((:old.data_prev_receb is null) and (:new.data_prev_receb is not null)) or ((:new.data_prev_receb is null) and (:old.data_prev_receb is not null))) then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb33157#DATA PREV. RECEB.'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.data_prev_receb || ' -> ' || :new.data_prev_receb || chr(10);

        end if;

        if :old.cod_banco <> :new.cod_banco then

           long_aux := long_aux || inter_fn_buscar_tag(
               'lb05205#PORTADOR:'
               ,ws_locale_usuario
               ,ws_usuario_systextil
            ) || ' ' || :old.cod_banco || ' -> ' || :new.cod_banco || chr(10);

        end if;

        if :old.perc_comissao_fatu_repres <> :new.perc_comissao_fatu_repres then

           long_aux := long_aux || inter_fn_buscar_tag(
               'lb45845#% DE COMIS. REPRES. FAT.:'
               ,ws_locale_usuario
               ,ws_usuario_systextil
            ) || ' ' || :old.perc_comissao_fatu_repres || ' -> ' || :new.perc_comissao_fatu_repres || chr(10);

        end if;

        if :old.perc_comissao_fatu_adm <> :new.perc_comissao_fatu_adm then

           long_aux := long_aux || inter_fn_buscar_tag(
               'lb45846#% DE COMIS. ADMIN. FAT.:'
               ,ws_locale_usuario
               ,ws_usuario_systextil
            ) || ' ' || :old.perc_comissao_fatu_adm || ' -> ' || :new.perc_comissao_fatu_adm || chr(10);

        end if;

        if :old.volumes_acomp <> :new.volumes_acomp then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb49087#VOLUMES ACOMP.:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.volumes_acomp      || ' -> ' || :new.volumes_acomp || chr(10);

        end if; 

        if :old.volumes_tecido <> :new.volumes_tecido then

            long_aux := long_aux || inter_fn_buscar_tag('
                lb49164#VOLUMES TECIDO:'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.volumes_tecido || ' -> ' || :new.volumes_tecido || chr(10);

        end if;
        
        if :old.desconto1 <> :new.desconto1 then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb50648#DESCONTO1:	'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.desconto1 || ' -> ' || :new.desconto1 || chr(10);

        end if;
        
        if :old.desconto1 <> :new.desconto2 then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb50649#DESCONTO2:	'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.desconto2 || ' -> ' || :new.desconto2 || chr(10);

        end if;
        
        if :old.desconto1 <> :new.desconto3 then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb50650#DESCONTO3:	'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' '|| :old.desconto3 || ' -> '|| :new.desconto3|| chr(10);

        end if;
        
        if :old.origem_pedido <> :new.origem_pedido then

            long_aux := long_aux || inter_fn_buscar_tag(
                'lb10163#ORIGEM PEDIDO:	'
                ,ws_locale_usuario
                ,ws_usuario_systextil
            ) || ' ' || :old.origem_pedido || ' -> ' || :new.origem_pedido || chr(10);

        end if;    

        INSERT INTO hist_100(
            tabela     
            ,operacao
            ,data_ocorr  
            ,aplicacao
            ,usuario_rede
            ,maquina_rede
            ,num01      
            ,long01
        )VALUES (
            'PEDI_100'
            ,'A'
            ,sysdate
            ,ws_aplicativo
            ,ws_usuario_rede
            , ws_maquina_rede
            ,:new.pedido_venda
            ,long_aux
        );

        end if;

        if deleting then

            INSERT INTO hist_100(
                tabela  
                ,operacao
                ,data_ocorr      
                ,aplicacao
                ,usuario_rede    
                ,maquina_rede
                ,num01
            )VALUES ( 
                'PEDI_100'     
                ,'D'
                ,sysdate          
                ,ws_aplicativo
                ,ws_usuario_rede
                ,ws_maquina_rede
                ,:old.pedido_venda
            );
            
        end if;

    end if;

end inter_tr_pedi_100_log;
