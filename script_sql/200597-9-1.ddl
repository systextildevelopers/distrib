ALTER TABLE rcnb_087
ADD (
  valor_despesa_direto number(15,2) default 0,
  valor_despesa_indireto number(15,2) default 0,
  custo_minuto_direto number(19,6) default 0,
  custo_minuto_indireto number(19,6) default 0
);
