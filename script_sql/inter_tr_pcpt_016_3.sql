
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_016_3" 
     before insert
         or delete
     on pcpt_016
     for each row
declare
   v_periodo_producao   number;
begin

   if inserting or updating
   then
      begin
         select pcpt_010.periodo_producao
         into   v_periodo_producao
         from pcpt_010
         where pcpt_010.ordem_tecelagem = :new.ordem_tecelagem;
      exception when others then
         v_periodo_producao := 0;
      end;

      inter_pr_verif_per_fechado(v_periodo_producao,4);
   end if;

   if deleting
   then
      begin
         select pcpt_010.periodo_producao
         into   v_periodo_producao
         from pcpt_010
         where pcpt_010.ordem_tecelagem = :old.ordem_tecelagem;
      exception when others then
         v_periodo_producao := 0;
      end;

      inter_pr_verif_per_fechado(v_periodo_producao,4);
   end if;

end inter_tr_pcpt_016_3;

-- ALTER TRIGGER "INTER_TR_PCPT_016_3" ENABLE
 

/

exec inter_pr_recompile;

