alter table PEDI_220
add (
	LIMITE_MAX_PED1              NUMBER(12) default 0,
	LIMITE_MAX_PED2              NUMBER(12) default 0,
	LIMITE_MAX_PED4              NUMBER(12) default 0,
	LIMITE_MAX_PED7              NUMBER(12) default 0,
	UNIDADE_LIM_PED              NUMBER(1) default 0,
	DATA_VALIDADE                DATE default null,
	Ativo                        NUMBER(1) default 0 
);

  /
exec inter_pr_recompile;
