-- Create table
create table PROJ_010
(
  NUMERO_PROJETO  NUMBER(9) default 0 not null,
  DESCRICAO       VARCHAR2(30) default '',
  DATA_INICIO     DATE not null,
  DATA_FIM        DATE not null,
  STATUS          NUMBER(1) default 0,
  NIVEL           VARCHAR2(2) default '' not null,
  GRUPO           VARCHAR2(5) default '' not null,
  SUBGRUPO        VARCHAR2(3) default '' not null,
  ITEM            VARCHAR2(6) default '' not null,
  QTDE_NECESSARIA NUMBER(15,3) default 0.000,
  QTDE_PROGRAMADA NUMBER(15,3) default 0.000,
  ALTERNATIVA     NUMBER(2) default 0.000,
  ROTEIRO         NUMBER(2) default 0
);
-- Add comments to the columns 
comment on column PROJ_010.STATUS
  is '0 - Digitada, 1 - Liberada, 2 - Em processo, 3 - Fechada';
-- Create/Recreate primary, unique and foreign key constraints 
alter table PROJ_010
  add constraint PK_PROJ_010 primary key (NUMERO_PROJETO, DATA_INICIO, DATA_FIM, NIVEL, GRUPO, SUBGRUPO, ITEM)
  using index;
