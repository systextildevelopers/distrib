alter table PEDI_095 add bloq_forca_vendas numeric(1);

alter table PEDI_095 modify bloq_forca_vendas default 0;

COMMENT ON COLUMN PEDI_095.bloq_forca_vendas IS 'BLOQ FOR�A DE VENDAS que indicar� se o produto ser� ou n�o poss�vel de vender no for�a de vendas.';
/
exec inter_pr_recompile;
/

declare
nro_registro number;
cursor pedi_095_c is
   select f.rowid from pedi_095 f where bloq_forca_vendas is null;
begin
   nro_registro := 0;
   
   for reg_pedi_095_c in pedi_095_c
   loop
      update pedi_095
         set bloq_forca_vendas = 0
       where bloq_forca_vendas is null
         and pedi_095.rowid = reg_pedi_095_c.rowid;
      
      nro_registro := nro_registro + 1;
      
      if nro_registro > 1000
      then
         nro_registro := 0;
         commit;
      end if;
   end loop; 
   commit;
end;
/
exec inter_pr_recompile;
/



