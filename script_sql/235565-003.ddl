
---------------------------------------
-- Insert dos processos ambiente PRD --
---------------------------------------
ALTER TABLE FINA_201 DROP CONSTRAINT "FINA_201_CON";
ALTER TABLE FINA_201 
  ADD CONSTRAINT "FINA_201_CON" UNIQUE ("TIPO_PROCESSO", "TIPO_ORIGEM","AMBIENTE") 
  USING INDEX;  

INSERT INTO FINA_201 (TIPO_PROCESSO,TIPO_ORIGEM,DESCRICAO_PROCESSO,AMBIENTE,OBSERVACAO) values('ANTECIPACAO','SYSTEXTIL','PROCESSO DE PEDIDO DE VENDA INTERNO','PRD','pedi_f117');
INSERT INTO FINA_201 (TIPO_PROCESSO,TIPO_ORIGEM,DESCRICAO_PROCESSO,AMBIENTE,OBSERVACAO) values('PEDIDO DE VENDA','SYSTEXTIL','ANTECIPACAO DE PAGAMENTO','PRD','crec_fa02');
INSERT INTO FINA_201 (TIPO_PROCESSO,TIPO_ORIGEM,DESCRICAO_PROCESSO,AMBIENTE,OBSERVACAO) values('RENEGOCIACAO','SYSTEXTIL','RENEGOCIACAO DE TITULOS','PRD','crec_f560');
UPDATE FINA_201 SET OBSERVACAO='pedi_f117' where TIPO_PROCESSO='ANTECIPACAO' AND TIPO_ORIGEM='SYSTEXTIL';
UPDATE FINA_201 SET OBSERVACAO='crec_fa02' where TIPO_PROCESSO='PEDIDO DE VENDA' AND TIPO_ORIGEM='SYSTEXTIL';
UPDATE FINA_201 SET OBSERVACAO='crec_f560' where TIPO_PROCESSO='RENEGOCIACAO' AND TIPO_ORIGEM='SYSTEXTIL';

----------------------------------------------
-- Tabela para guarda dados temporários SPH --
----------------------------------------------
CREATE TABLE  "TMP_DEBUG" 
   (	"TMP_DESCRICAO" VARCHAR2(4000)
   );

------------------------------------------
-- Tabela para tivar/inativar trace SPH --
------------------------------------------
CREATE TABLE  FINA_204_trace_active 
   (	isactive number(1)
   ); 
insert into FINA_204_trace_active  values(0);

-------------------------------------------------------
-- Tabela de Configuração do SPH / Dados por empresa --
-------------------------------------------------------
CREATE TABLE FINA_205
   (	
	ID NUMBER(9,0), 
	INTEGRATIONID NUMBER(9,0), 
	INTEGRATION NUMBER(9,0), 
	COMPANYID NUMBER(9,0), 
	COMPANY NUMBER(9,0), 
	FORMSMETHODID NUMBER(9,0), 
	FORMSNAME VARCHAR2(30), 
	METHODDESCRIPTION VARCHAR2(30), 
	BANKCODE NUMBER(4,0), 
	BANKAGENCY NUMBER(9,0), 
	BANKACCOUNT NUMBER(9,0), 
	URLSANDBOX VARCHAR2(255), 
	URLHOMOL VARCHAR2(255), 
	URLPRODUCTION VARCHAR2(255), 
	CLIENTID VARCHAR2(255), 
	CLIENTSECRET VARCHAR2(255), 
	DEVELOPERAPPLICATIONKEY VARCHAR2(1000), 
	AGREEMENTNUMBER VARCHAR2(1000), 
	BASICKEY VARCHAR2(4000), 
	PIXKEYRANDOM VARCHAR2(255), 
	PIXKEYDOCUMENT VARCHAR2(255), 
	PIXKEYEMAIL VARCHAR2(255), 
	PIXKEYPHONE VARCHAR2(255), 
	ISACTIVE VARCHAR2(5), 
	URLTYPE VARCHAR2(3), 
	CREATEDAT DATE, 
	UPDATEDAT DATE, 
	DATEOCCURS DATE, 
	USERID VARCHAR2(60), 
	COMPANYNUMBER NUMBER(9,0), 
	CONSTRAINT FINA_205_CON UNIQUE (INTEGRATIONID,COMPANYID,FORMSMETHODID)
	USING INDEX  ENABLE
   );

	CREATE UNIQUE INDEX PK_FINA_205 ON FINA_205 (ID);

	ALTER TABLE FINA_205 ADD CONSTRAINT PK_FINA_205 PRIMARY KEY (ID);

	CREATE SEQUENCE FINA_205_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 10;

	CREATE OR REPLACE TRIGGER INTER_TR_FINA_205
	  before insert on FINA_205               
	  for each row  
	begin   
	  if :NEW.ID is null then 
		select FINA_205_SEQ.nextval into :NEW.ID from sys.dual; 
	  end if; 
	end; 
	/

	ALTER TRIGGER INTER_TR_FINA_205 ENABLE;

------------------------------------------------
-- Tabela BAckup dados de Configuração do SPH --
------------------------------------------------
CREATE TABLE FINA_205_BKP
   (	
    SEQ NUMBER(9,0), 
	INTEGRATIONID NUMBER(9,0), 
	INTEGRATION NUMBER(9,0), 
	COMPANYID NUMBER(9,0), 
	COMPANY NUMBER(9,0), 
	FORMSMETHODID NUMBER(9,0), 
	FORMSNAME VARCHAR2(30), 
	METHODDESCRIPTION VARCHAR2(30), 
	BANKCODE NUMBER(4,0), 
	BANKAGENCY NUMBER(9,0), 
	BANKACCOUNT NUMBER(9,0), 
	URLSANDBOX VARCHAR2(255), 
	URLHOMOL VARCHAR2(255), 
	URLPRODUCTION VARCHAR2(255), 
	CLIENTID VARCHAR2(255), 
	CLIENTSECRET VARCHAR2(255), 
	DEVELOPERAPPLICATIONKEY VARCHAR2(1000), 
	AGREEMENTNUMBER VARCHAR2(1000), 
	BASICKEY VARCHAR2(4000), 
	PIXKEYRANDOM VARCHAR2(255), 
	PIXKEYDOCUMENT VARCHAR2(255), 
	PIXKEYEMAIL VARCHAR2(255), 
	PIXKEYPHONE VARCHAR2(255), 
	ISACTIVE VARCHAR2(5),
	URLTYPE VARCHAR2(3),
	CREATEDAT DATE, 
	UPDATEDAT DATE,
    DATEOCCURS DATE,
    USERID VARCHAR2(60),
    COMPANYNUMBER NUMBER(9)
   );
CREATE UNIQUE INDEX pk_fina_205_BKP ON fina_205_bkp (SEQ);
ALTER TABLE fina_205_BKP ADD CONSTRAINT pk_fina_205_BKP PRIMARY KEY (SEQ) ENABLE;

---------------------------------------------------------------------
-- Tabela de Configuração do SPH / Gateway com Metodo de Pagamento --
---------------------------------------------------------------------
CREATE TABLE FINA_206
   (	
    formsMethodId NUMBER(9,0), 
	formsName varchar2(60), 
	methodDescription varchar2(60)
   );
CREATE UNIQUE INDEX PK_FINA_206 ON FINA_206 (formsMethodId);
ALTER TABLE FINA_206 ADD CONSTRAINT PK_FINA_206 PRIMARY KEY (formsMethodId);

/

----------------------------------------------------------
-- Backup Tabela de Configuração Gateway e Métodos SPH  --
----------------------------------------------------------
CREATE TABLE FINA_206_BKP 
   (
    SEQ number(9),
    FORMSMETHODID number(9),
    FORMSNAME VARCHAR2(60),
    METHODDESCRIPTION VARCHAR2(60),
    DATEOCCURS DATE,
    USERID VARCHAR2(60),
    COMPANYNUMBER NUMBER(9)
   );
  CREATE UNIQUE INDEX PK_FINA_206_BKP ON  FINA_206_BKP (seq);
ALTER TABLE FINA_206_BKP ADD CONSTRAINT PK_FINA_206_BKP PRIMARY KEY (seq);

---------------------------------------------------------------------------
-- Tabela Relacionamento Empresa com Integração SPH e Empresa do usuario --
---------------------------------------------------------------------------
CREATE TABLE FINA_207
   (
    id number(9),
    INTEGRATIONID number(9),
    COMPANYID number(9),
    COMPANY number(9),
	CONSTRAINT FINA_207_CON UNIQUE (INTEGRATIONID,COMPANYID)
   );
	CREATE UNIQUE INDEX PK_FINA_207 ON  FINA_207 (id);
	ALTER TABLE FINA_207 ADD CONSTRAINT PK_FINA_207 PRIMARY KEY (id);

	CREATE SEQUENCE FINA_207_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 10;

	CREATE OR REPLACE TRIGGER INTER_TR_FINA_207
	  before insert on FINA_207               
	  for each row  
	begin   
	  if :NEW.ID is null then 
		select FINA_205_SEQ.nextval into :NEW.ID from sys.dual; 
	  end if; 
	end;
	/

	ALTER TRIGGER INTER_TR_FINA_207 ENABLE;

-----------------------------------------------------------------------------------
-- Backup Tabela Relacionamento Empresaa com Integração SPH e Empresa do usuario --
-----------------------------------------------------------------------------------
CREATE TABLE FINA_207_BKP
   (
	SEQ NUMBER(9),
    INTEGRATIONID number(9),
    COMPANYID number(9),
    COMPANY number(9),
	DATEOCCURS DATE,
    USERID VARCHAR2(60),
    COMPANYNUMBER NUMBER(9)
   );
  CREATE UNIQUE INDEX PK_FINA_207_BKP ON  FINA_207_BKP (SEQ);
ALTER TABLE FINA_207_BKP ADD CONSTRAINT PK_FINA_207_BKP PRIMARY KEY (SEQ);

------------------------------------------------
-- Tabela de Cadastro de Integrador com o SPH --
------------------------------------------------
CREATE TABLE FINA_208
   (	
    ID number(9),
    CODIGO_EMPRESA NUMBER(9,0),
	URLTYPE VARCHAR2(3),
	clientId varchar2(255), 
	clientSecret varchar2(255),
	CONSTRAINT FINA_208_PK PRIMARY KEY (ID) USING INDEX  ENABLE, 
	CONSTRAINT FINA_208_CON UNIQUE (CODIGO_EMPRESA) USING INDEX  ENABLE   
	);
	CREATE SEQUENCE FINA_208_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 10;

	CREATE OR REPLACE TRIGGER INTER_TR_FINA_208
	  before insert on FINA_208              
	  for each row  
	begin   
	  if :NEW.ID is null then 
		select FINA_208_SEQ.nextval into :NEW.ID from sys.dual; 
	  end if; 
	end;
	/

	ALTER TRIGGER INTER_TR_FINA_208 ENABLE;

------------------------------------------------
-- Tabela de Cadastro de Integrador com o SPH --
------------------------------------------------
CREATE TABLE FINA_208_BKP
   (	
    SEQ NUMBER(9),
    CODIGO_EMPRESA NUMBER(9,0),
	URLTYPE VARCHAR2(3),
	clientId varchar2(255), 
	clientSecret varchar2(255),
	DATEOCCURS DATE,
    USERID VARCHAR2(60),
    COMPANYNUMBER NUMBER(9)
   );
CREATE UNIQUE INDEX PK_FINA_208_BKP ON FINA_208_BKP (SEQ);
ALTER TABLE FINA_208_BKP ADD CONSTRAINT PK_FINA_208_BKP PRIMARY KEY (SEQ);

---------------------------------------------------------
-- Tabela de Empresas vinculadas ao Integrador com SPH --
---------------------------------------------------------
CREATE TABLE FINA_209
   (	
    id number(9),
	ID_EMPRESA number (9),
	COMPANY NUMBER(9,0),
	COMPANYCNPJ varchar(14),
	ISACTIVE VARCHAR2(5),
	CONSTRAINT FINA_209_PK PRIMARY KEY (ID) USING INDEX  ENABLE, 
	CONSTRAINT FINA_209_CON UNIQUE (ID_EMPRESA,COMPANY)
   );
      
	CREATE SEQUENCE FINA_209_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 10;

	CREATE OR REPLACE TRIGGER INTER_TR_FINA_209
	  before insert on FINA_209               
	  for each row  
	begin   
	  if :NEW.ID is null then 
		select FINA_209_SEQ.nextval into :NEW.ID from sys.dual; 
	  end if; 
	end; 
	/

	ALTER TRIGGER INTER_TR_FINA_209 ENABLE;  
	
-------------------------------------------------
-- Tabela de Serviços SPH vinculadas a Empresa --
-------------------------------------------------
CREATE TABLE FINA_210
   (	
    id number(9),
	id_method number(9),
	INTEGRATIONID number (9),
	COMPANYID NUMBER(9,0),
	FORMSMETHODID number(9),
	SERVICEID number(9),
	SERVICEDESCRIPTION varchar2(255),
	ISACTIVE varchar2(5),
	CONSTRAINT FINA_210_PK PRIMARY KEY (ID) USING INDEX  ENABLE, 
	CONSTRAINT FINA_210_CON UNIQUE (INTEGRATIONID,COMPANYID,FORMSMETHODID,SERVICEID)
   );
   
   
	CREATE SEQUENCE FINA_210_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 10;

	CREATE OR REPLACE TRIGGER INTER_TR_FINA_210
	  before insert on FINA_210               
	  for each row  
	begin   
	  if :NEW.ID is null then 
		select FINA_210_SEQ.nextval into :NEW.ID from sys.dual; 
	  end if; 
	end; 
	/

	ALTER TRIGGER INTER_TR_FINA_210 ENABLE;  	
   
