
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_095" 
before DELETE on pedi_095
for each row
declare
  v_control               number;
  v_consiste_tabela_preco empr_002.consiste_tabela_preco%type;

begin
   if deleting
   then
      begin
      select empr_002.consiste_tabela_preco
      into   v_consiste_tabela_preco
      from empr_002;
      exception
	     when no_data_found then
		  v_control := 2;
      end ;

      if v_consiste_tabela_preco = 1
      then
         begin
            select count(pedi_100.pedido_venda) into v_control
            from pedi_100, pedi_110
            where pedi_100.pedido_venda     = pedi_110.pedido_venda
              and pedi_100.colecao_tabela   = :old.tab_col_tab
              and pedi_100.mes_tabela       = :old.tab_mes_tab
              and pedi_100.sequencia_tabela = :old.tab_seq_tab
              and pedi_110.cd_it_pe_nivel99 = :old.nivel_estrutura
              and pedi_110.cd_it_pe_grupo   = :old.grupo_estrutura
              and (pedi_110.cd_it_pe_subgrupo = :old.subgru_estrutura or :old.subgru_estrutura = '000')
              and (pedi_110.cd_it_pe_item     = :old.item_estrutura or :old.item_estrutura = '000000');
         exception
            when no_data_found then
            v_control := 0;
         end;

         if  v_control > 0
         then
            raise_application_error(-20000,'Nao pode eliminar o item da tabela, a mesma esta sendo usada em um pedido de venda.');
         end if;
      end if;
   end if;
end;
-- ALTER TRIGGER "INTER_TR_PEDI_095" ENABLE
 

/

exec inter_pr_recompile;

