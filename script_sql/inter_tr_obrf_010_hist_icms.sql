
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_010_HIST_ICMS" 
   before insert or delete or update of base_icms on obrf_010 for each row

declare
   ws_tipo_ocorr                      varchar2 (1);
   ws_usuario_rede                    varchar2(20);
   ws_maquina_rede                    varchar2(40);
   ws_aplicativo                      varchar2(20);
   ws_sid                             number(9);
   ws_empresa                         number(3);
   ws_usuario_systextil               varchar2(250);
   ws_locale_usuario                  varchar2(5);
   ws_nome_programa                   varchar2(20);

   ws_documento_old                   obrf_010.documento%type;
   ws_documento_new                   obrf_010.documento%type;
   ws_serie_old                       obrf_010.serie%type;
   ws_serie_new                       obrf_010.serie%type;
   ws_base_icms_old                   obrf_010.base_icms%type;
   ws_base_icms_new                   obrf_010.base_icms%type;


begin
   if INSERTING then

      ws_tipo_ocorr                   := 'I';

      ws_documento_old                := null;
      ws_documento_new                := :new.documento;

      ws_serie_old                    := null;
      ws_serie_new                    := :new.serie;

      ws_base_icms_old                := null;
      ws_base_icms_new                := :new.base_icms;


   elsif UPDATING then

      ws_tipo_ocorr                   := 'A';

      ws_documento_old                := :old.documento;
      ws_documento_new                := :new.documento;

      ws_serie_old                    := :old.serie;
      ws_serie_new                    := :new.serie;

      ws_base_icms_old                := :old.base_icms;
      ws_base_icms_new                := :new.base_icms;


   elsif DELETING then

      ws_tipo_ocorr                   := 'D';

      ws_documento_old                := :old.documento;
      ws_documento_new                := null;

      ws_serie_old                    := :old.serie;
      ws_serie_new                    := null;

      ws_base_icms_old                := :old.base_icms;
      ws_base_icms_new                := null;

   end if;

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ws_nome_programa := inter_fn_nome_programa(ws_sid);                            

   INSERT INTO obrf_010_hist
       (aplicacao,                  tipo_ocorr,
        data_ocorr,
        usuario_rede,               maquina_rede,
        nome_programa,              usuario_systextil,

        local_entrega_old,          local_entrega_new,
        documento_old,              documento_new,
        serie_old,                  serie_new,
        base_icms_old,              base_icms_new)
   VALUES
       (ws_aplicativo,              ws_tipo_ocorr,
        sysdate,
        ws_usuario_rede,            ws_maquina_rede,
        ws_nome_programa,           ws_usuario_systextil,

        ws_empresa,                 ws_empresa,
        ws_documento_old,           ws_documento_new,
        ws_serie_old,               ws_serie_new,
        ws_base_icms_old,           ws_base_icms_new);

end inter_tr_obrf_010_hist_icms;
-- ALTER TRIGGER "INTER_TR_OBRF_010_HIST_ICMS" ENABLE
 

/

exec inter_pr_recompile;

