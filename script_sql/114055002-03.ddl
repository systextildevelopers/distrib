alter table oper_275
add buscar_transp number(1) default 0;

alter table oper_275 add constraint oper_275_buscar_transp_ck check (buscar_transp in (0,1));

exec inter_pr_recompile;
