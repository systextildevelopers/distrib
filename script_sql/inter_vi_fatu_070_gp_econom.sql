create or replace view inter_vi_fatu_070_gp_econom as
select fatu_070."CODIGO_EMPRESA",fatu_070."CLI_DUP_CGC_CLI9",fatu_070."CLI_DUP_CGC_CLI4",fatu_070."CLI_DUP_CGC_CLI2",fatu_070."TIPO_TITULO",fatu_070."NUM_DUPLICATA",fatu_070."SEQ_DUPLICATAS",fatu_070."DATA_VENC_DUPLIC",fatu_070."VALOR_DUPLICATA",fatu_070."SITUACAO_DUPLIC",fatu_070."COD_CANC_DUPLIC",fatu_070."DATA_CANC_DUPLIC",fatu_070."PERC_JURO_DUPLIC",fatu_070."PERC_DESC_DUPLIC",fatu_070."PORTADOR_DUPLIC",fatu_070."SERIE_NOTA_FISC",fatu_070."NUMERO_BORDERO",fatu_070."QUANTIDADE",fatu_070."TECIDO_PECA",fatu_070."PERCENTUAL_COMIS",fatu_070."VALOR_COMIS",fatu_070."BASE_CALC_COMIS",fatu_070."PEDIDO_VENDA",fatu_070."NR_TITULO_BANCO",fatu_070."COD_REP_CLIENTE",fatu_070."POSICAO_DUPLIC",fatu_070."PORT_ANTERIOR",fatu_070."VENCTO_ANTERIOR",fatu_070."DUPLIC_EMITIDA",fatu_070."NR_SOLICITACAO",fatu_070."DATA_EMISSAO",fatu_070."NUMERO_TITULO",fatu_070."DATA_TRANSF_TIT",fatu_070."NUMERO_SEQUENCIA",fatu_070."VALOR_REMESSA",fatu_070."COD_HISTORICO",fatu_070."COMPL_HISTORICO",fatu_070."COD_LOCAL",fatu_070."PREVISAO",fatu_070."MOEDA_TITULO",fatu_070."VALOR_MOEDA",fatu_070."DATA_PRORROGACAO",fatu_070."CONTA_CORRENTE",fatu_070."NUMERO_REMESSA",fatu_070."PERC_COMIS_CREC",fatu_070."DUPLIC_IMPRESSA",fatu_070."COD_TRANSACAO",fatu_070."CODIGO_CONTABIL",fatu_070."TIT_RENEGOCIADO",fatu_070."SEQ_END_COBRANCA",fatu_070."NUM_CONTABIL",fatu_070."COMISSAO_LANCADA",fatu_070."REFERENTE_NF",fatu_070."TIPO_TIT_ORIGEM",fatu_070."NUM_DUP_ORIGEM",fatu_070."SEQ_DUP_ORIGEM",fatu_070."OBSERVACAO",fatu_070."CLI9RESPTIT",fatu_070."CLI4RESPTIT",fatu_070."CLI2RESPTIT",fatu_070."ORIGEM_PEDIDO",fatu_070."CGC9_ENDOSSO",fatu_070."CGC4_ENDOSSO",fatu_070."CGC2_ENDOSSO",fatu_070."CONTROLE_CHEQUE",fatu_070."TIPO_COMISSAO",fatu_070."CODIGO_ADMINISTR",fatu_070."COMISSAO_ADMINISTR",fatu_070."PERC_COMIS_CREC_ADM",fatu_070."TIT_BAIXAR",fatu_070."POSICAO_ANT",fatu_070."MENSAGEM_BOLETO",fatu_070."ATRASO_PELA_RENEGOCIACAO",fatu_070."DATA_ULT_MOVIM_PAGTO",fatu_070."DATA_ULT_MOVIM_CREDITO",fatu_070."SALDO_DUPLICATA",fatu_070."NR_CUPOM",fatu_070."COD_FORMA_PAGTO",fatu_070."NR_MTV_PRORROGACAO",fatu_070."NR_REMESSA_INADIMPLENCIA",fatu_070."NR_SOLICITACAO_INADIMPLENCIA",fatu_070."SITUACAO_INADIMPLENCIA",fatu_070."VALOR_DESP_COBR",fatu_070."COND_PAGTO_VENDOR",fatu_070."EXECUTA_TRIGGER",fatu_070."VALOR_DESCONTO_AUX",fatu_070."VALOR_JUROS_AUX",fatu_070."VALOR_SALDO_AUX",fatu_070."NR_IDENTIFICACAO",fatu_070."COD_CARTEIRA",fatu_070."VALOR_AVP",fatu_070."INDICE_MENSAL",fatu_070."INDICE_DIARIO",fatu_070."STATUS_SERASA_PEFIN",fatu_070."MOTIVO_BXA_SERASA_PEFIN",fatu_070."RESPONSAVEL_RECEB",fatu_070."SELECIONADO_CREDITO_RENEG",fatu_070."NUM_RENEGOCIACAO",fatu_070."SEQ_RENEGOCIACAO",fatu_070."CMC7_CHEQUE",fatu_070."CD_CENTRO_CUSTO",fatu_070."TIPO_TITULO_ORIGINAL",pedi_010.grupo_economico,
(
select to_number(ocorrencia) as ult_ocorrencia
from (
            select   c50.nr_duplicata,
                    c50.seq_duplicata,
                    c50.tipo_titulo,
                    c50.cgc9_sacado,
                    ocorrencia,
                    dia_pagto, mes_pagto, ano_pagto
            from crec_050 c50
            where   ( ocorrencia is  not null or trim(ocorrencia) <> '')
            and   ( dia_pagto is  not null or trim(dia_pagto) <> '')
            and   ( mes_pagto is not null  or trim(mes_pagto) <> '' )
            and   ( ano_pagto is not null  or trim(ano_pagto) <> '' )
            order by c50.ano_pagto desc, c50.mes_pagto desc,
                     c50.dia_pagto desc, c50.data_retorno desc
     ) ocorrencias
     where ocorrencias.nr_duplicata in (to_char(fatu_070.num_duplicata, 'FM0'), to_char(fatu_070.num_duplicata, 'FM00'),
                                        to_char(fatu_070.num_duplicata, 'FM000'), to_char(fatu_070.num_duplicata, 'FM0000'),
                                        to_char(fatu_070.num_duplicata, 'FM00000'), to_char(fatu_070.num_duplicata, 'FM000000'),
                                        to_char(fatu_070.num_duplicata, 'FM0000000'), to_char(fatu_070.num_duplicata, 'FM00000000'),
                                        to_char(fatu_070.num_duplicata, 'FM000000000'))
       and ocorrencias.seq_duplicata = to_char(fatu_070.seq_duplicatas, 'FM00')
       and ocorrencias.tipo_titulo = to_char(fatu_070.tipo_titulo, 'FM00')
       and ocorrencias.cgc9_sacado = to_char(fatu_070.cli_dup_cgc_cli9, 'FM000000000')
       and not exists (select 1 from pedi_038
                       where pedi_038.cod_portador = fatu_070.portador_duplic
                         and pedi_038.instrucao    = ocorrencias.ocorrencia)
       and rownum <= 1
) ult_ocorrencia,
(
select my_to_date(to_char(dia_pagto)||to_char(mes_pagto)||to_char(ano_pagto), 'ddmmyy') dt_ult_ocorrencia
from (
            select   c50.nr_duplicata,
                    c50.seq_duplicata,
                    c50.tipo_titulo,
                    c50.cgc9_sacado,
                    ocorrencia,
                    dia_pagto, mes_pagto, ano_pagto
            from crec_050 c50
           where   ( ocorrencia is  not null or trim(ocorrencia) <> '')
            and   ( dia_pagto is  not null or trim(dia_pagto) <> '')
            and   ( mes_pagto is not null  or trim(mes_pagto) <> '' )
            and   ( ano_pagto is not null  or trim(ano_pagto) <> '' )
            order by c50.ano_pagto desc, c50.mes_pagto desc,
                     c50.dia_pagto desc, c50.data_retorno desc
     ) ocorrencias
     where ocorrencias.nr_duplicata in (to_char(fatu_070.num_duplicata, 'FM0'), to_char(fatu_070.num_duplicata, 'FM00'),
                                        to_char(fatu_070.num_duplicata, 'FM000'), to_char(fatu_070.num_duplicata, 'FM0000'),
                                        to_char(fatu_070.num_duplicata, 'FM00000'), to_char(fatu_070.num_duplicata, 'FM000000'),
                                        to_char(fatu_070.num_duplicata, 'FM0000000'), to_char(fatu_070.num_duplicata, 'FM00000000'),
                                        to_char(fatu_070.num_duplicata, 'FM000000000'))
       and ocorrencias.seq_duplicata = to_char(fatu_070.seq_duplicatas, 'FM00')
       and ocorrencias.tipo_titulo = to_char(fatu_070.tipo_titulo, 'FM00')
       and ocorrencias.cgc9_sacado = to_char(fatu_070.cli_dup_cgc_cli9, 'FM000000000')
       and not exists (select 1 from pedi_038
                       where pedi_038.cod_portador = fatu_070.portador_duplic
                         and pedi_038.instrucao    = ocorrencias.ocorrencia)
       and rownum <= 1
) dt_ult_ocorrencia,

nvl( cast (	( 	select   nvl ( SUM(DECODE(cont_010.sinal_titulo, 1, fatu_075.valor_descontos, fatu_075.valor_descontos * (-1.00))), 0.00)
			  from  fatu_075, cont_010
			  where fatu_075.nr_titul_codempr = fatu_070.codigo_empresa
			  and fatu_075.nr_titul_cli_dup_cgc_cli9 =  fatu_070.cli_dup_cgc_cli9
			  and fatu_075.nr_titul_cli_dup_cgc_cli4 =  fatu_070.cli_dup_cgc_cli4
			  and fatu_075.nr_titul_cli_dup_cgc_cli2 =  fatu_070.cli_dup_cgc_cli2
			  and fatu_075.nr_titul_cod_tit = fatu_070.tipo_titulo
			  and fatu_075.nr_titul_num_dup = fatu_070.num_duplicata
			  and fatu_075.nr_titul_seq_dup = fatu_070.seq_duplicatas
			  and fatu_075.historico_pgto = cont_010.codigo_historico
			  and cont_010.sinal_titulo  IN (1,2)
			GROUP BY fatu_075.nr_titul_codempr, fatu_075.nr_titul_cli_dup_cgc_cli9, fatu_075.nr_titul_cli_dup_cgc_cli4, fatu_075.nr_titul_cli_dup_cgc_cli2, fatu_075.nr_titul_cod_tit, fatu_075.nr_titul_num_dup, fatu_075.nr_titul_seq_dup
		) as  number(20,2) 
	) 
,0.00)
+ 
nvl ( cast (	( 	select   nvl ( SUM(fatu_075.valor_pago), 0.00)
			from  fatu_075
			where fatu_075.nr_titul_codempr = fatu_070.codigo_empresa
			and fatu_075.nr_titul_cli_dup_cgc_cli9 =  fatu_070.cli_dup_cgc_cli9
			and fatu_075.nr_titul_cli_dup_cgc_cli4 =  fatu_070.cli_dup_cgc_cli4
			and fatu_075.nr_titul_cli_dup_cgc_cli2 =  fatu_070.cli_dup_cgc_cli2
			and fatu_075.nr_titul_cod_tit = fatu_070.tipo_titulo
			and fatu_075.nr_titul_num_dup = fatu_070.num_duplicata
			and fatu_075.nr_titul_seq_dup = fatu_070.seq_duplicatas
			and exists ( select 1
					 	 from crec_246
					 	 where crec_246.historico_pgto = fatu_075.historico_pgto)
			GROUP BY fatu_075.nr_titul_codempr, fatu_075.nr_titul_cli_dup_cgc_cli9, 
					 fatu_075.nr_titul_cli_dup_cgc_cli4, fatu_075.nr_titul_cli_dup_cgc_cli2, 
					 fatu_075.nr_titul_cod_tit, fatu_075.nr_titul_num_dup, fatu_075.nr_titul_seq_dup
		) as  number(20,2)
	)
,0.00) as total_descontos

from fatu_070,pedi_010
where fatu_070.cli_dup_cgc_cli9 = pedi_010.cgc_9
and   fatu_070.cli_dup_cgc_cli4 = pedi_010.cgc_4
and   fatu_070.cli_dup_cgc_cli2 = pedi_010.cgc_2;
