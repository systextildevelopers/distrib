alter table obrf_010 add
(valor_inss NUMBER(15,2),
valor_inss_imp NUMBER(15,2),
valor_pis_imp NUMBER(15,2),
valor_cofins_imp NUMBER(15,2),
valor_csl_imp NUMBER(15,2),
valor_irrf_moeda NUMBER(15,2),
valor_irrf NUMBER(15,2));

alter table obrf_010 modify
(valor_inss default 0.0,
valor_inss_imp default 0.0,
valor_pis_imp default 0.0,
valor_cofins_imp default 0.0,
valor_csl_imp default 0.0,
valor_irrf_moeda default 0.0,
valor_irrf default 0.0);

exec inter_pr_recompile;
