create or replace trigger "INTER_TR_NFOR_010_COD_NEG"
before insert on NFOR_010
 for each row

declare
  v_nr_registro number;

begin
  select ID_NFOR_010.nextval into v_nr_registro from dual;

  :new.cod_negociacao := v_nr_registro;

end INTER_TR_NFOR_010_COD_NEG;
