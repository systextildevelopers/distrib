alter table empr_100
add (
    historico_pgto        number(4) default 0,
    banco_cliente         number(4) default 0,
    conta_cliente         number(9) default 0,    
    tipo_titulo           number(2) default 0,
    hist_pgto_supp        number(4) default 0,
    cnpj_supp9            number(9) default 0,
    cnpj_supp4            number(4) default 0,
    cnpj_supp2            number(2) default 0,
    valor_lim_duplic      number(15,2) default 0
);
exec inter_pr_recompile;
