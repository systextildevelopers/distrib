--14082
alter table fatu_060
add (perc_fcp_uf_dest       number(8,4));

alter table fatu_060
add (perc_icms_uf_dest      number(8,4));

alter table fatu_060
add (perc_icms_partilha     number(8,4));

alter table fatu_060
add (val_fcp_uf_dest        number(17,5));

alter table fatu_060
add (val_icms_uf_dest       number(17,5));

alter table fatu_060
add (val_icms_uf_remet      number(17,5));

alter table fatu_060
modify (perc_fcp_uf_dest    default 0);

alter table fatu_060
modify (perc_icms_uf_dest      default 0);

alter table fatu_060
modify (perc_icms_partilha     default 0);

alter table fatu_060
modify (val_fcp_uf_dest        default 0);

alter table fatu_060
modify (val_icms_uf_dest       default 0);

alter table fatu_060
modify (val_icms_uf_remet      default 0);


comment on column fatu_060.perc_fcp_uf_dest is 'Percentual do ICMS relativo ao Fundo de Combate � Pobreza (FCP) na UF de destino';
comment on column fatu_060.perc_icms_uf_dest is 'Al�quota interna da UF do destinat�rio';
comment on column fatu_060.perc_icms_partilha is 'Percentual de partilha para a UF do destinat�rio: 40% em 2016;60% em 2017;80% em 2018;100% a partir de 2019';
comment on column fatu_060.val_fcp_uf_dest is 'Valor do ICMS relativo ao Fundo de Combate � Pobreza (FCP) da UF de destino.';
comment on column fatu_060.val_icms_uf_dest is 'Valor do ICMS de partilha para a UF do destinat�rio';
comment on column fatu_060.val_icms_uf_remet is 'Valor do ICMS de partilha para a UF do remetente.';

exec inter_pr_recompile;
/