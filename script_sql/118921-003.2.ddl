
create table PROG_055
(
    CMQOP055_NIVMQ050 VARCHAR2(1) default '' not null,
    CMQOP055_GRUMQ050 VARCHAR2(5) default '' not null,
    CMQOP055_SUBMQ050 VARCHAR2(3) default '' not null,
    CMQOP055_ITEMQ050 VARCHAR2(6) default '' not null,
    CMQOP055_NR_ALT   NUMBER(2)   default 0  not null,
    CMQOP055_NR_ROT   NUMBER(2)   default 0  not null,
    CMQOP055_SQ_OPER  NUMBER(4)   default 0  not null,
    SEQ_ESTRUTURA     NUMBER(3)   default 0  not null
);
/
exec inter_pr_recompile;
