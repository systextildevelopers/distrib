create or replace trigger inter_tr_fatu_050_evitacanc
before update of situacao_nfisc

on fatu_050
for each row
begin

   if :old.situacao_nfisc = 2 and :new.situacao_nfisc = 1
   then
      raise_application_error(-20000,'O Sistema est� tentando alterar a situa��o de uma nota j� cancelada. Favor entrar em contato com a equipe de Atendimento da Intersys.');
   end if;

end inter_tr_fatu_050_evitacanc;

/

execute inter_pr_recompile;
/* versao: 1 */


 exit;


 exit;

 exit;
