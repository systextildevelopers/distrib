alter table pcpt_020_hist
add (
    fornecedor_cgc9_old number(9),
    fornecedor_cgc9_new number(9),     
    fornecedor_cgc4_old number(4),      
    fornecedor_cgc4_new number(4),     
    fornecedor_cgc2_old number(2),    
    fornecedor_cgc2_new number(2)
);
