create table basi_544_temp(
nivel varchar2(1),
grupo varchar2(5),
programa varchar2(30),
chave varchar2(100),
codigo_usuario number(9),
codigo_empresa number(9));

create synonym systextilrpt.basi_544_temp for basi_544_temp;

exec inter_pr_recompile;
