
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_010_3" 
     before insert
         or delete
     on pcpt_010
     for each row
declare

begin

   if inserting or updating
   then
      inter_pr_verif_per_fechado(:new.periodo_producao,4);
   end if;

   if deleting
   then
      inter_pr_verif_per_fechado(:old.periodo_producao,4);
   end if;

end inter_tr_pcpt_010_3;

-- ALTER TRIGGER "INTER_TR_PCPT_010_3" ENABLE
 

/

exec inter_pr_recompile;

