
  CREATE OR REPLACE FUNCTION "INTER_FN_CALCULO_APLIC_FINANC" (valor_aplicado number, taxa_anual number, dias_ano number, carencia number)
   return number is Result number;
begin

   begin
      result := round(power(((taxa_anual / 100.00) + 1.00),(carencia / dias_ano)) * valor_aplicado, 2) ;

   exception
      when others then
         result := -1.0;
   end;

   return(Result);
end inter_fn_calculo_aplic_financ;

 

/

exec inter_pr_recompile;

