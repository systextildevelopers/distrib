
CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_020_PLAN" 
   before delete or
          update of cod_cancelamento, pedido_venda
   on pcpc_020
   for each row
declare
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);

   v_pedido_venda            number(9);
   v_total_necessario        number(6);
   v_qtde_update             number(6);

   v_executa_trigger         number;
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;


   if v_executa_trigger = 0
   then
      -- Obt�m dados do usu�rio logado.
      -- Com esta informa��o, pode-se exibir a
      -- mensagem conforme o idioma do usu�rio

      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                            ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

      v_pedido_venda        := :old.pedido_venda;

      if updating or deleting
      then
         begin
            insert into tmrp_615
              (nome_programa,
               nr_solicitacao,                           tipo_registro,
               cgc_cliente9)
            values
              ('trigger_planejamento',
               888,                                      888,
               ws_sid);
            exception when others
            then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;

         if updating and (:old.cod_cancelamento = 0 and :old.cod_cancelamento <> :new.cod_cancelamento)
         then

            begin
               delete tmrp_630
               where  tmrp_630.ordem_prod_compra   = :old.ordem_producao
                 and  tmrp_630.area_producao       = 1
                 and  tmrp_630.alternativa_produto = :old.alternativa_peca;
               exception when others
               then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         elsif updating and (:old.pedido_venda <> :new.pedido_venda)
         then
            v_pedido_venda := :new.pedido_venda;

            begin
               delete tmrp_630
               where  tmrp_630.ordem_prod_compra   = :old.ordem_producao
                 and  tmrp_630.area_producao       = 1
                 and  tmrp_630.alternativa_produto = :old.alternativa_peca;
               exception when others
               then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;

            for reg_pcpc_021 in (select pcpc_021.tamanho, pcpc_021.sortimento, pcpc_021.quantidade
                                 from pcpc_021
                                 where pcpc_021.ordem_producao = :old.ordem_producao)
            loop
               begin
                  select decode(sign(nvl(sum(tmrp_625.qtde_reserva_planejada),0) - nvl(sum(tmrp_625.qtde_reserva_programada),0)),
                                1,
                                nvl(sum(tmrp_625.qtde_reserva_planejada),0),
                                nvl(sum(tmrp_625.qtde_reserva_programada),0)) - nvl(sum(tmrp_625.qtde_areceber_programada),0) qtde_necessaria
                  into v_total_necessario
                  from tmrp_625
                  where tmrp_625.pedido_venda         = v_pedido_venda
                    and tmrp_625.nivel_produto        = '1'
                    and tmrp_625.grupo_produto        = :old.referencia_peca
                    and tmrp_625.subgrupo_produto     = reg_pcpc_021.tamanho
                    and tmrp_625.item_produto         = reg_pcpc_021.sortimento
                    and tmrp_625.alternativa_produto  = :old.alternativa_peca
                    and tmrp_625.nivel_produto_origem = '0';
                  exception when others
                  then v_total_necessario := 0;
               end;

               for reg_ordens_planej in  (select tmrp_625.ordem_planejamento,
                                                 decode(sign(nvl(sum(tmrp_625.qtde_reserva_planejada),0) - nvl(sum(tmrp_625.qtde_reserva_programada),0)),
                                                        1,
                                                        nvl(sum(tmrp_625.qtde_reserva_planejada),0),
                                                        nvl(sum(tmrp_625.qtde_reserva_programada),0)) - nvl(sum(tmrp_625.qtde_areceber_programada),0) qtde_necessaria
                                          from tmrp_625
                                          where tmrp_625.pedido_venda         = v_pedido_venda
                                            and tmrp_625.nivel_produto        = '1'
                                            and tmrp_625.grupo_produto        = :old.referencia_peca
                                            and tmrp_625.subgrupo_produto     = reg_pcpc_021.tamanho
                                            and tmrp_625.item_produto         = reg_pcpc_021.sortimento
                                            and tmrp_625.alternativa_produto  = :old.alternativa_peca
                                            and tmrp_625.nivel_produto_origem = '0'
                                            and decode(sign(tmrp_625.qtde_reserva_planejada - tmrp_625.qtde_reserva_programada),
                                                        1,
                                                        tmrp_625.qtde_reserva_planejada,
                                                        tmrp_625.qtde_reserva_programada) - tmrp_625.qtde_areceber_programada  > 0
                                          group by tmrp_625.ordem_planejamento)
               loop
                  -- Esclarecimentos:
                  --   Toma o total necess�rio pelo planejamento e obt�m o percentual relativo de cada item.
                  --   Depois extrai eset percentual da quantidade sendo programada.
                  -- Ex.:
                  --   Tenho uma necessidade de 200, dividida em 10 itens de 20 (10% para cada um).
                  --   Ao programar 300, sei que tenho que atualizar o primeiro item com 30 (10% de 300).
                  v_qtde_update := (((reg_ordens_planej.qtde_necessaria * 100) / v_total_necessario) * reg_pcpc_021.quantidade) / 100;

                  if v_qtde_update > 0 and v_total_necessario > 0
                  then
                     begin
                        insert into tmrp_630
                          (ordem_planejamento,                   area_producao,        ordem_prod_compra,
                           nivel_produto,                        grupo_produto,        subgrupo_produto,
                           item_produto,                         pedido_venda,         programado_comprado,
                           alternativa_produto)
                        values
                          (reg_ordens_planej.ordem_planejamento, 1,                    :old.ordem_producao,
                           '1',                                  :old.referencia_peca, reg_pcpc_021.tamanho,
                           reg_pcpc_021.sortimento,              v_pedido_venda,       v_qtde_update,
                           :old.alternativa_peca);
                        exception when others
                        then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                     end;
                  end if;
               end loop;
            end loop;
         elsif deleting
         then
            begin
               delete tmrp_630
               where tmrp_630.ordem_prod_compra   = :old.ordem_producao
                 and tmrp_630.area_producao       = 1
                 and tmrp_630.alternativa_produto = :old.alternativa_peca;
               exception when others
               then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end if;
      end if;
   end if;
end inter_tr_pcpc_020_plan;

-- ALTER TRIGGER "INTER_TR_PCPC_020_PLAN" ENABLE
 

/

exec inter_pr_recompile;

