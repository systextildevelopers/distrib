ALTER TABLE FATU_503 
ADD (TAG_PRE_ROMANEIO NUMBER(1) DEFAULT 0 NOT NULL);

COMMENT ON COLUMN FATU_503.TAG_PRE_ROMANEIO IS '0 - N�o permite ler no pr�-romaneio (estq_f169) tag que n�o esteja no estoque, 1 - Permite ler a tag mesmo se esta n�o existir no dep�sito. Consulte o Help.';

exec inter_pr_recompile;
/
