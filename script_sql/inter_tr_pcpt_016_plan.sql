
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_016_PLAN" 
   before insert or
          delete or
          update of tipo_destino, ordem_pedido, quantidade
   on pcpt_016
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_saldo_prog              number(15,5);
   v_alternativa             number(2);

   v_nivel_produto           varchar2(1);
   v_grupo_produto           varchar2(5);
   v_subgrupo_produto        varchar2(3);
   v_item_produto            varchar2(6);
   v_destino                 number(1);
   v_numero_destino          number(9);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting or updating
   then
      v_destino          := :new.tipo_destino;
      v_numero_destino   := :new.ordem_pedido;

      begin
         select pcpt_010.cd_pano_nivel99,  pcpt_010.cd_pano_grupo,
                pcpt_010.cd_pano_subgrupo, pcpt_010.cd_pano_item,
                pcpt_010.alternativa_item
         into   v_nivel_produto,           v_grupo_produto,
                v_subgrupo_produto,        v_item_produto,
                v_alternativa
         from pcpt_010
         where pcpt_010.ordem_tecelagem = :new.ordem_tecelagem;
      exception when no_data_found then
              v_nivel_produto := '';
              v_grupo_produto := '';
              v_subgrupo_produto := '';
              v_item_produto := '';
      end;

      if updating
      then
         v_saldo_prog := :new.quantidade - :old.quantidade;
      else -- Se estiver inserindo...
         v_saldo_prog := :new.quantidade;
      end if;

      -- Se o tipo de destino, for para ordem de beneficiamento, entao faz a leitura do planejamento
      -- com o tecido da ordem de tecelagem, alternativa e ordem de beneficiamento que a ordem de
      -- tecelagem esta sendo destinada carregando o tecido de origem(nivel 2), ordem de planejamento,
      -- pedido de venda que tem necessidade deste tecido acabado.
      if v_destino = 2
      then
         inter_pr_pcpt_016_plan (:new.ordem_tecelagem,
                                 v_nivel_produto,
                                 v_grupo_produto,
                                 v_subgrupo_produto,
                                 v_item_produto,
                                 v_alternativa,
                                 v_numero_destino,
                                 v_saldo_prog);
      end if;
   else
      -- Deletando o destino da ordem de tecelagem.
      v_destino          := :old.tipo_destino;
      v_numero_destino   := :old.ordem_pedido;

      if v_destino = 2
      then
         begin
            select pcpt_010.cd_pano_nivel99,  pcpt_010.cd_pano_grupo,
                   pcpt_010.cd_pano_subgrupo, pcpt_010.cd_pano_item,
                   pcpt_010.alternativa_item
            into   v_nivel_produto,           v_grupo_produto,
                   v_subgrupo_produto,        v_item_produto,
                   v_alternativa
            from pcpt_010
            where pcpt_010.ordem_tecelagem = :old.ordem_tecelagem;
            exception when no_data_found
            then
               v_nivel_produto    := '';
               v_grupo_produto    := '';
               v_subgrupo_produto := '';
               v_item_produto     := '';
               v_alternativa      := 0;
         end;

         for reg_tmrp_630 in (select tmrp_630.ordem_planejamento,         tmrp_630.pedido_venda,
                                     tmrp_625.nivel_produto_origem,       tmrp_625.grupo_produto_origem,
                                     tmrp_625.subgrupo_produto_origem,    tmrp_625.item_produto_origem,
                                     tmrp_625.alternativa_produto_origem,
                                     tmrp_630.area_producao,              tmrp_630.ordem_prod_compra,
                                     tmrp_625.seq_produto,                tmrp_625.seq_produto_origem
                              from tmrp_630, tmrp_625
                              where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
                                and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
                                and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
                                and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
                                and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
                                and tmrp_625.item_produto_origem        = tmrp_630.item_produto
                                and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto
                                and tmrp_625.nivel_produto              = v_nivel_produto
                                and tmrp_625.grupo_produto              = v_grupo_produto
                                and tmrp_625.subgrupo_produto           = v_subgrupo_produto
                                and tmrp_625.item_produto               = v_item_produto
                                and tmrp_625.alternativa_produto        = v_alternativa
                                and tmrp_630.area_producao              = 2
                                and tmrp_630.ordem_prod_compra          = v_numero_destino
                              group by tmrp_630.ordem_planejamento,         tmrp_630.pedido_venda,
                                       tmrp_625.nivel_produto_origem,       tmrp_625.grupo_produto_origem,
                                       tmrp_625.subgrupo_produto_origem,    tmrp_625.item_produto_origem,
                                       tmrp_625.alternativa_produto_origem,
                                       tmrp_630.area_producao,              tmrp_630.ordem_prod_compra,
                                       tmrp_625.seq_produto,                tmrp_625.seq_produto_origem)
         loop
            begin
               insert into tmrp_615
                 (ordem_prod,                               area_producao,
                  nivel,                                    grupo,
                  subgrupo,                                 item,
                  alternativa,                              ordem_planejamento,
                  pedido_venda,                             nome_programa,
                  nr_solicitacao,                           tipo_registro,
                  cgc_cliente9,

                  seq_registro,                             sequencia,
                  nivel_prod,                               grupo_prod,
                  subgrupo_prod,                            item_prod,
                  alternativa_prod)
               values
                 (reg_tmrp_630.ordem_prod_compra,           reg_tmrp_630.area_producao,
                  reg_tmrp_630.nivel_produto_origem,        reg_tmrp_630.grupo_produto_origem,
                  reg_tmrp_630.subgrupo_produto_origem,     reg_tmrp_630.item_produto_origem,
                  reg_tmrp_630.alternativa_produto_origem,  reg_tmrp_630.ordem_planejamento,
                  reg_tmrp_630.pedido_venda,                'trigger_planejamento',
                  888,                                      888,
                  ws_sid,

                  reg_tmrp_630.seq_produto,                 reg_tmrp_630.seq_produto_origem,
                  v_nivel_produto,                          v_grupo_produto,
                  v_subgrupo_produto,                       v_item_produto,
                  v_alternativa);
            exception when others then
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end loop;

         begin
            delete tmrp_630
            where tmrp_630.ordem_prod_compra    = :old.ordem_tecelagem
              and tmrp_630.area_producao        = 4
              and tmrp_630.nivel_produto        = v_nivel_produto
              and tmrp_630.grupo_produto        = v_grupo_produto
              and tmrp_630.subgrupo_produto     = v_subgrupo_produto
              and tmrp_630.item_produto         = v_item_produto
              and tmrp_630.alternativa_produto  = v_alternativa;
            exception when others
            then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;
end inter_tr_pcpt_016_plan;

-- ALTER TRIGGER "INTER_TR_PCPT_016_PLAN" ENABLE
 

/

exec inter_pr_recompile;

