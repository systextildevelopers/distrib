create or replace trigger wms_tr_volumes_log
before insert
or delete
or update of nr_volume_st, nr_pedido, peso_volume, cod_tipo_vol_st, 
	cod_embalagem_st, montador, turno, cod_usuario, 
	nome_programa, usuario_sistema, usuario_exped, flag_integracao_st, 
	timestamp_ini_mont, timestamp_fim_mont 
on inte_wms_volumes
for each row
declare

   v_nr_volume_st               inte_wms_volumes.nr_volume_st	       %type;
   v_nr_pedido_new              inte_wms_volumes.nr_pedido	       %type;
   v_nr_pedido_old              inte_wms_volumes.nr_pedido	       %type;
   v_peso_volume_new            inte_wms_volumes.peso_volume	       %type;
   v_peso_volume_old            inte_wms_volumes.peso_volume	       %type;
   v_cod_tipo_vol_st_new        inte_wms_volumes.cod_tipo_vol_st       %type;
   v_cod_tipo_vol_st_old        inte_wms_volumes.cod_tipo_vol_st       %type;
   v_cod_embalagem_st_new	inte_wms_volumes.cod_embalagem_st      %type;
   v_cod_embalagem_st_old	inte_wms_volumes.cod_embalagem_st      %type;
   v_montador_new	  	inte_wms_volumes.montador	       %type;
   v_montador_old	  	inte_wms_volumes.montador	       %type;
   v_turno_new		        inte_wms_volumes.turno 		       %type;
   v_turno_old		        inte_wms_volumes.turno 		       %type;
   v_cod_usuario_new            inte_wms_volumes.cod_usuario	       %type;
   v_cod_usuario_old            inte_wms_volumes.cod_usuario 	       %type;
   v_nome_programa_new	        inte_wms_volumes.nome_programa 	       %type;
   v_nome_programa_old	        inte_wms_volumes.nome_programa 	       %type;
   v_usuario_sistema_new	inte_wms_volumes.usuario_sistema       %type;
   v_usuario_sistema_old	inte_wms_volumes.usuario_sistema       %type;
   v_usuario_exped_new 	        inte_wms_volumes.usuario_exped 	       %type;
   v_usuario_exped_old 	        inte_wms_volumes.usuario_exped 	       %type;
   v_flag_integracao_st_new     inte_wms_volumes.flag_integracao_st    %type;
   v_flag_integracao_st_old     inte_wms_volumes.flag_integracao_st    %type;
   v_timestamp_ini_mont_new     inte_wms_volumes.timestamp_ini_mont    %type;
   v_timestamp_ini_mont_old     inte_wms_volumes.timestamp_ini_mont    %type;
   v_timestamp_fim_mont_new     inte_wms_volumes.timestamp_fim_mont    %type;
   v_timestamp_fim_mont_old     inte_wms_volumes.timestamp_fim_mont    %type;
  								       
   v_sid                        number(9);			       
   v_empresa                    number(3);			       
   v_usuario_systextil          varchar2(250);		       
   v_locale_usuario             varchar2(5);		       
   v_nome_programa              varchar2(20);		       
   v_operacao                   varchar(1);
   v_data_operacao              date;
   v_usuario_rede               varchar(20);
   v_maquina_rede               varchar(40);
   v_aplicativo                 varchar(20);
begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();
   
   --alimenta as variaveis new caso seja insert ou update
   if inserting or updating
   then
      if inserting
      then v_operacao := 'i';
      else v_operacao := 'u';
      end if;
      
      v_nr_volume_st            := :new.nr_volume_st;           
      v_nr_pedido_new           := :new.nr_pedido;         
      v_peso_volume_new         := :new.peso_volume;          
      v_cod_tipo_vol_st_new     := :new.cod_tipo_vol_st;       
      v_cod_embalagem_st_new	:= :new.cod_embalagem_st;
      v_montador_new	  	:= :new.montador;  	
      v_turno_new		:= :new.turno;		        
      v_cod_usuario_new         := :new.cod_usuario;            
      v_nome_programa_new	:= :new.nome_programa;              
      v_usuario_sistema_new	:= :new.usuario_sistema;
      v_usuario_exped_new 	:= :new.usuario_exped;              
      v_flag_integracao_st_new  := :new.flag_integracao_st;     
      v_timestamp_ini_mont_new  := :new.timestamp_ini_mont;     
      v_timestamp_fim_mont_new  := :new.timestamp_fim_mont;      
   
   end if; --fim do if inserting or updating
   
   --alimenta as variaveis old caso seja insert ou update
   if deleting or updating
   then
      if deleting
      then
         v_operacao      := 'd';
      else
         v_operacao      := 'u';
      end if;
      
      v_nr_volume_st            := :old.nr_volume_st;          
      v_nr_pedido_old           := :old.nr_pedido;         
      v_peso_volume_old         := :old.peso_volume;        
      v_cod_tipo_vol_st_old     := :old.cod_tipo_vol_st;    
      v_cod_embalagem_st_old    := :old.cod_embalagem_st;
      v_montador_old	        := :old.montador;  	
      v_turno_old		:= :old.turno;		    
      v_cod_usuario_old         := :old.cod_usuario;        
      v_nome_programa_old	:= :old.nome_programa;      
      v_usuario_sistema_old     := :old.usuario_sistema;
      v_usuario_exped_old 	:= :old.usuario_exped;      
      v_flag_integracao_st_old  := :old.flag_integracao_st; 
      v_timestamp_ini_mont_old  := :old.timestamp_ini_mont;  
      v_timestamp_fim_mont_old  := :old.timestamp_fim_mont; 
     
   end if; --fim do if deleting or updating
   
   
   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);
   
   
    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente
    
    -- insere as informa��es na tabela de log
    insert into inte_wms_volumes_log (
       nr_volume_st,                      
       nr_pedido_new, 		       nr_pedido_old,             
       peso_volume_new,                peso_volume_old,           
       cod_tipo_vol_st_new,            cod_tipo_vol_st_old,       
       cod_embalagem_st_new,	       cod_embalagem_st_old,	    
       montador_new,	  	       montador_old,	  	    
       turno_new,		       turno_old,		    
       cod_usuario_new,                cod_usuario_old,           
       nome_programa_new,	       nome_programa_old,	    
       usuario_sistema_new,	       usuario_sistema_old,	    
       usuario_exped_new, 	       usuario_exped_old, 	    
       flag_integracao_st_new,         flag_integracao_st_old,    
       timestamp_ini_mont_new,         timestamp_ini_mont_old,    
       timestamp_fim_mont_new,         timestamp_fim_mont_old,    
       operacao,                       data_operacao,             
       usuario_rede,                   maquina_rede,             
       aplicativo,                     nome_programa )
    values(
       v_nr_volume_st,                      
       v_nr_pedido_new, 	       v_nr_pedido_old,             
       v_peso_volume_new,              v_peso_volume_old,           
       v_cod_tipo_vol_st_new,          v_cod_tipo_vol_st_old,       
       v_cod_embalagem_st_new,	       v_cod_embalagem_st_old,	    
       v_montador_new,	  	       v_montador_old,	  	    
       v_turno_new,		       v_turno_old,		    
       v_cod_usuario_new,              v_cod_usuario_old,           
       v_nome_programa_new,	       v_nome_programa_old,	    
       v_usuario_sistema_new,	       v_usuario_sistema_old,	    
       v_usuario_exped_new, 	       v_usuario_exped_old, 	    
       v_flag_integracao_st_new,       v_flag_integracao_st_old,    
       v_timestamp_ini_mont_new,       v_timestamp_ini_mont_old,    
       v_timestamp_fim_mont_new,       v_timestamp_fim_mont_old,    
       v_operacao,                     v_data_operacao,             
       v_usuario_rede,                 v_maquina_rede,             
       v_aplicativo,                   v_nome_programa
    );
       
end wms_tr_volumes_log;

/

execute inter_pr_recompile;

/* versao: 2 */


 exit;
