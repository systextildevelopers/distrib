create or replace procedure inter_pr_gera_sped_0600_pc(p_cod_empresa   IN  NUMBER,
                                                       p_cod_matriz    IN  NUMBER,
                                                       p_cnpj9_empresa IN  NUMBER,
                                                       p_cnpj4_empresa IN  NUMBER,
                                                       p_cnpj2_empresa IN  NUMBER,
                                                       p_dat_inicial   IN DATE,
                                                       p_des_erro      OUT varchar2) is

w_erro                EXCEPTION;

CURSOR u_pc_0600 (p_cod_matriz      NUMBER,
                  p_dat_inicial     IN DATE) IS

   select distinct cod_c_custo,      descricao,
                   data_cadastro
   from (
       (select distinct obrf_700.cod_ccus as cod_c_custo, basi_185.descricao,
                        basi_185.data_cadastro
        from obrf_700, basi_185, fatu_500
        where fatu_500.codigo_empresa = obrf_700.cod_empresa
          and fatu_500.codigo_matriz  = p_cod_matriz
          and obrf_700.cod_ccus       = basi_185.centro_custo
          and obrf_700.cod_ccus       > 0
          and obrf_700.ind_orig_cred  < 3
          and obrf_700.ano_apur       = to_char(p_dat_inicial, 'YYYY')
          and obrf_700.mes_apur       = to_char(p_dat_inicial, 'MM'))
   union all
         (select distinct obrf_302.cod_ccus as cod_c_custo, basi_185.descricao,
                          basi_185.data_cadastro
          from obrf_302, basi_185, fatu_500
          where fatu_500.codigo_empresa = obrf_302.cod_empresa
            and fatu_500.codigo_matriz  = p_cod_matriz
            and obrf_302.cod_ccus       = basi_185.centro_custo
            and obrf_302.cod_ccus       > 0
            and obrf_302.mes_apur       = to_char(p_dat_inicial, 'MM')
            and obrf_302.ano_apur       = to_char(p_dat_inicial, 'YYYY'))
   union all
         (select distinct obrf_711.cod_ccus as cod_c_custo, basi_185.descricao,
                          basi_185.data_cadastro
          from obrf_711, basi_185, fatu_500
          where fatu_500.codigo_empresa            = obrf_711.cod_empresa
            and fatu_500.codigo_matriz             = p_cod_matriz
            and obrf_711.cod_ccus                  = basi_185.centro_custo
            and obrf_711.cod_ccus                  > 0
            and obrf_711.per_escrit_mes            = to_char(p_dat_inicial, 'MM')
            and obrf_711.per_escrit_ano            = to_char(p_dat_inicial, 'YYYY'))
   union all
         (select distinct sped_pc_c170.cod_c_custo, basi_185.descricao,
                          basi_185.data_cadastro
          from sped_pc_c170, basi_185
          where sped_pc_c170.cod_matriz     = p_cod_matriz
            and sped_pc_c170.cod_c_custo    = basi_185.centro_custo
            and sped_pc_c170.cod_c_custo    > 0));
BEGIN

   p_des_erro := NULL;

   FOR pc_0600 IN u_pc_0600 (p_cod_matriz, p_dat_inicial) LOOP

      begin
         insert into sped_pc_0600
            (cod_matriz
             ,cod_empresa
             ,num_cnpj_empr9
             ,num_cnpj_empr4
             ,num_cnpj_empr2
             ,centro_custo
             ,descricao
             ,data_cadastro)
         values
            (p_cod_matriz
             ,p_cod_empresa
             ,p_cnpj9_empresa
             ,p_cnpj4_empresa
             ,p_cnpj2_empresa
             ,pc_0600.cod_c_custo
             ,pc_0600.descricao
             ,pc_0600.data_cadastro);
      exception
          when others then
             p_des_erro := 'N�o inseriu dados na tabela sped_pc_0600' || Chr(10) || SQLERRM;
      end;
   END LOOP;

END inter_pr_gera_sped_0600_pc;
/
exec inter_pr_recompile;
