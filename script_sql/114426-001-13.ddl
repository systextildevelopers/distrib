-- Create table
create table OBRF_796
(
  NUMERO_SOLICITACAO  NUMBER(9) default 0 not null,
  PRE_ROMANEIO        NUMBER(9) default 0 not null,
  PERIODO_PRODUCAO040 NUMBER(4) default 0 not null,
  ORDEM_CONFECCAO040  NUMBER(5) default 0 not null,
  ORDEM_PRODUCAO      NUMBER(9) default 0 not null,
  SEQUENCIA           NUMBER(4) default 0 not null,
  SEQ_PRODUTO         NUMBER(5) default 0 not null,
  NIVEL               VARCHAR2(1) default ' ' not null,
  GRUPO               VARCHAR2(5) default ' ' not null,
  SUBGRUPO            VARCHAR2(3) default ' ' not null,
  ITEM                VARCHAR2(6) default ' ' not null,
  LOTE_ACOMP          NUMBER(6) default 0 not null,
  QUANTIDADE          NUMBER(15,3) default 0.000 not null,
  FLAG_MARCADO        NUMBER(1) default 0 not null,
  COD_DEPOSITO        NUMBER(3) default 0 not null
);


exec inter_pr_recompile;
/
