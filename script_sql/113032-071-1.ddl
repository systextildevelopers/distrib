CREATE TABLE OBRF_278 (
	ID_LOTE_MULT NUMBER(9),
	ID_NOTA NUMBER(9) DEFAULT 0,
	NUM_LOTE NUMBER(9) DEFAULT 0,
	QTDE_LOTE NUMBER(15,3) DEFAULT 0
);

ALTER TABLE OBRF_278
ADD CONSTRAINT PK_OBRF_278 PRIMARY KEY (ID_LOTE_MULT);

CREATE SEQUENCE ID_OBRF_278 START WITH 1 INCREMENT BY 1;

/
exec inter_pr_recompile;
