create or replace view inter_vi_obrf_015_vlr_devol
(num_nota_orig, serie_nota_orign, capa_ent_forcli9, capa_ent_forcli4, capa_ent_forcli2, documento, serie, data_emissao, total_docto, total_devolvido)
as
select obrf_015.num_nota_orig,    obrf_015.serie_nota_orig,   obrf_015.capa_ent_forcli9,
               obrf_015.capa_ent_forcli4, obrf_015.capa_ent_forcli2,  obrf_010.documento,
               obrf_010.serie,            obrf_010.data_emissao,      obrf_010.total_docto,
               sum(obrf_015.valor_total + obrf_015.rateio_despesas + obrf_015.valor_ipi) as total_devolvido
        from obrf_010, obrf_015
        where obrf_010.documento     = obrf_015.capa_ent_nrdoc
          and obrf_010.serie         = obrf_015.capa_ent_serie
          and obrf_010.cgc_cli_for_9 = obrf_015.capa_ent_forcli9
          and obrf_010.cgc_cli_for_4 = obrf_015.capa_ent_forcli4
          and obrf_010.cgc_cli_for_2 = obrf_015.capa_ent_forcli2
          and obrf_015.num_nota_orig <> 0
          and obrf_015.flag_devolucao = 1
        group by obrf_015.num_nota_orig,    obrf_015.serie_nota_orig,   obrf_015.capa_ent_forcli9,
               obrf_015.capa_ent_forcli4, obrf_015.capa_ent_forcli2,  obrf_010.documento,
               obrf_010.serie,            obrf_010.data_emissao,      obrf_010.total_docto;
