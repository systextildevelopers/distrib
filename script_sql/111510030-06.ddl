create table oper_276 (
id_registro			number(9)		default 0,
cod_empresa         number(3) 		default 0,
documento			number(9)		default 0,
serie				varchar2(3)		default '',
descr_local			varchar2(20)	default '',
descr_mensagem		varchar2(300)	default '',
usuario_rede		varchar2(20)	default '',
usuario_systextil	varchar2(20)	default '',
maquina_rede		varchar2(40)	default '',
data_hora_registro	timestamp
);

exec inter_pr_recompile;
