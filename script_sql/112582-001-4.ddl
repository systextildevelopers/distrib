CREATE TABLE CREC_462 (
    COD_EMPRESA NUMBER(3),
	COD_BANCO NUMBER(3),
	ARQUIVO VARCHAR2(40),
	NUM_TITULO NUMBER(9),
	TIPO_TITULO NUMBER(2),
	PARCELA VARCHAR2(3),
	SITUACAO NUMBER(1),
	NR_TITULO_BANCO VARCHAR2(20),
	OCORRENCIA NUMBER(3),
	DATA_PAGAMENTO DATE,
	VALOR_PAGO NUMBER(13,2),
	DESCONTO NUMBER(13,2),
	JUROS NUMBER(13,2),
	VALOR_TITULO NUMBER(13,2),
	OBS VARCHAR2(255)
);
