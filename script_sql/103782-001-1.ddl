alter table pcpt_010 add ENDERECO_ROLO     VARCHAR2(30) DEFAULT '';
alter table pcpt_010 add GRUPO_MAQ_ROLO    VARCHAR2(4) DEFAULT '';
alter table pcpt_010 add NUMERO_MAQ_ROLO   NUMBER(5,0) DEFAULT 0;
alter table pcpt_010 add SUBGRUPO_MAQ_ROLO VARCHAR2(3) DEFAULT '';

exec inter_pr_recompile;
