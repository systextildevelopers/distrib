create table OBRF_660
(
  CODIGO_EMPRESA         NUMBER(3) default 0 not null,
  ANO                    NUMBER(4) default 0 not null,
  MES                    NUMBER(2) default 0 not null,
  NIVEL_PRODUTO          VARCHAR2(1) default '' not null,
  GRUPO_PRODUTO          VARCHAR2(5) default '' not null,
  SUB_PRODUTO            VARCHAR2(3) default '' not null,
  ITEM_PRODUTO           VARCHAR2(6) default '' not null,
  ALTER_PRODUTO          NUMBER(2) default 0 not null,
  NIVEL_COMP             VARCHAR2(1) default '' not null,
  GRUPO_COMP             VARCHAR2(5) default '' not null,
  SUB_COMP               VARCHAR2(3) default '' not null,
  ITEM_COMP              VARCHAR2(6) default '' not null,
  ALTER_COMP             NUMBER(2) default 0 not null,
  CONSUMO                NUMBER(13,7) default 0.000,
  VALOR_COMPRAS_CUSTO    NUMBER(13,5) default 0.000,
  VALOR_COMPRAS_IMPORT   NUMBER(13,5) default 0.000,
  VALOR_CONSUMO_CUSTO    NUMBER(13,5) default 0.000,
  VALOR_CONSUMO_IMPORT   NUMBER(13,5) default 0.000,
  VALOR_VENDA            NUMBER(13,5) default 0.000,
  PERC_IMPORTADO         NUMBER(5,2) default 0.00,
  PERCENTUAL_VALOR_VENDA NUMBER(5,2) default 0.0,
  PERCENTUAL_QTDE_VENDA  NUMBER(5,2) default 0.0
);

create table OBRF_665
(
  CODIGO_EMPRESA     NUMBER(3) default 0 not null,
  ANO                NUMBER(4) default 0 not null,
  MES                NUMBER(2) default 0 not null,
  TIPO_REGISTRO      VARCHAR2(1) default '' not null,
  NIVEL              VARCHAR2(1) default '' not null,
  GRUPO              VARCHAR2(5) default '' not null,
  SUBGRUPO           VARCHAR2(3) default '' not null,
  ITEM               VARCHAR2(6) default '' not null,
  NUMERO_NOTA        NUMBER(9) default 0 not null,
  SERIE_NOTA         VARCHAR2(3) default '' not null,
  FORNECEDOR9        VARCHAR2(9) default '' not null,
  FORNECEDOR4        VARCHAR2(4) default '' not null,
  FORNECEDOR2        VARCHAR2(2) default '' not null,
  QUANTIDADE         NUMBER(15,3) default 0.000,
  VALOR_CONTABIL     NUMBER(15,5) default 0.000,
  VALOR_BASE_CALCULO NUMBER(15,5) default 0.000,
  PROCEDENCIA        NUMBER(1) default 0,
  PERC_IMPORTADO     NUMBER(15,3) default 0.000,
  VALOR_IMPORTADO    NUMBER(15,5) default 0.000,
  ALTERNATIVA        NUMBER(2) default 0,
  TIPO_NOTA          VARCHAR2(1) default ''
);
