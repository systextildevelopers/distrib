create or replace procedure inter_pr_lista_critica_estoque (
p_empresa IN number,
p_nivel IN varchar2,
p_grupo IN varchar2,
p_subgrupo IN varchar2,
p_item IN varchar2,
p_id_execucao IN varchar2) is

      v_qtde040      number;
      v_qtderolos    number;
      v_saldo_k      number;
      v_qtde_kardex_ant number;

      data_base            date;

      ajuste_sub           varchar2(2);
      v_id_execucao        number;
      v_ind_partida        varchar2(400);
      v_sequencia_ficha number;
      v_forma_calculo_cardex number;
      v_qtde_empenhada number;
      v_qtde_sugerida  number;
      v_preco_medio_unitario number;
      v_observacao varchar2(1000);
      v_data_diverg_estq date;
      v_existe_diferenca number;
          
      cursor basi205 (p_empresa NUMBER) is
      select b.codigo_deposito,  b.tipo_volume
      from basi_205 b
      where b.controla_ficha_cardex = 1
        and b.dep_inativo = 0
        and (b.local_deposito  = p_empresa or p_empresa = 9999)
        and b.tipo_volume in (0,1,2,4,7,9)
      order by b.codigo_deposito;

      -- Criando o loop de tag`s
      cursor pcpc330 (p_deposito_lido number,
                   p_nivel varchar2,
                   p_grupo varchar2,
                   p_subgrupo varchar2,
                   p_item varchar2) is
      select t.deposito deposito,   t.nivel nivel,   t.grupo grupo,
             t.subgrupo subgrupo, decode(s.cor_de_estoque,'000000', t.item,s.cor_de_estoque)  item,    0 lote_acomp,
             count(t.deposito) qtde_tag,
             decode(s.cor_de_estoque,'000000', t.item,s.cor_de_estoque) cor_estoque_tag
      from pcpc_330 t, basi_030 s
      where t.nivel = s.nivel_estrutura
        and t.grupo = s.referencia
        and t.deposito = p_deposito_lido
        and ( t.nivel    = p_nivel or p_nivel = 'X')
        and ( t.grupo    = p_grupo or p_grupo = 'XXXXX')
        and ( t.subgrupo = p_subgrupo or p_subgrupo = 'XXX')
        and ( t.item     = p_item or p_item = 'XXXXXX')
      group by t.deposito, t.nivel, t.grupo , t.subgrupo,
      decode(s.cor_de_estoque, '000000', t.item, s.cor_de_estoque), 0,
      decode(s.cor_de_estoque, '000000', t.item,s.cor_de_estoque);

      -- Criando o loop da tabela de rolos
      cursor pcpt020 (p_deposito_lido number,
                   p_nivel varchar2,
                   p_grupo varchar2,
                   p_subgrupo varchar2,
                   p_item varchar2) is
      select p.codigo_deposito deposito,   p.panoacab_nivel99 nivel,    p.panoacab_grupo grupo,
             p.panoacab_subgrupo subgrupo, p.panoacab_item    item,     0 lote_acomp,
             sum(p.qtde_quilos_acab) peso_rolos
      from pcpt_020 p
      where p.rolo_estoque    not in (0,2)
        and p.codigo_deposito     = p_deposito_lido
        and ( p.panoacab_nivel99  = p_nivel or p_nivel = 'X')
        and ( p.panoacab_grupo    = p_grupo or p_grupo = 'XXXXX')
        and ( p.panoacab_subgrupo = p_subgrupo or p_subgrupo = 'XXX')
        and ( p.panoacab_item     = p_item or p_item = 'XXXXXX')
        -- UTILIZAR ESTA PARA FILTRAR QUANDO A EMPRESA PASSOU A UTILIZAR PRE?O POR EMPRESA NA CARDEX
        -- SE J? INICIOU O CARDEX POR EMPRESA N?O TEM NECESSIDADE
        and p.data_entrada  >= '01-jan-2016'
      group by p.codigo_deposito, p.panoacab_nivel99, p.panoacab_grupo,
             p.panoacab_subgrupo, p.panoacab_item,0;


      -- Criando o loop da tabela de saldos

      cursor estq040 (p_deposito_lido number,
                   p_nivel varchar2,
                   p_grupo varchar2,
                   p_subgrupo varchar2,
                   p_item varchar2) is
      select * from
      (select e.cditem_nivel99 nivel, e.cditem_grupo grupo, e.cditem_subgrupo subgrupo,
             e.cditem_item item,     e.deposito deposito,  0 lote_acomp,
             decode(s.cor_de_estoque,'000000', e.cditem_item,s.cor_de_estoque) cor_estoque_tag,
             sum(e.qtde_estoque_atu) qtde_estoque, sum(e.qtde_empenhada) qtde_empenhada,
             sum(e.qtde_sugerida) qtde_sugerida,

             max(case when e.data_ult_entrada < e.data_ult_saida  and e.data_ult_entrada is not null and e.data_ult_saida is not null then e.data_ult_saida
               when e.data_ult_entrada >= e.data_ult_saida and e.data_ult_entrada is not null and e.data_ult_saida is not null then e.data_ult_entrada
               when e.data_ult_entrada is null then e.data_ult_saida
               when e.data_ult_saida is null then e.data_ult_entrada
            end) data_movimento

      from estq_040 e, basi_030 s
      where e.deposito = p_deposito_lido
        and e.cditem_nivel99    = s.nivel_estrutura
        and e.cditem_grupo      = s.referencia
        and ( e.cditem_nivel99  = p_nivel or p_nivel = 'X')
        and ( e.cditem_grupo    = p_grupo or p_grupo = 'XXXXX')
        and ( e.cditem_subgrupo = p_subgrupo or p_subgrupo = 'XXX')
        and ( e.cditem_item     = p_item or p_item = 'XXXXXX')

      group by e.cditem_nivel99, e.cditem_grupo, e.cditem_subgrupo,  e.cditem_item, e.deposito,
              0,decode(s.cor_de_estoque,'000000', e.cditem_item,s.cor_de_estoque))
      /* UTILIZAR ESTA DA PARA FILTRAR QUANDO A EMPRESA PASSOU A UTILIZAR PRE?O POR EMPRESA NA CARDEX
         SE J? INICIOU O CARDEX POR EMPRESA N?O TEM NECESSIDADE */
      where (data_movimento  >= '01-jan-2016' or (select empr_008.val_int 
                                                  from empr_008 
                                                  where empr_008.codigo_empresa = p_empresa
                                                    and empr_008.param = 'estq.formaCalcCardex') = 0);

      -- Criando loop verificando se tem estoque onde se trabalha com cor de estoque

      cursor estq040Sort (p_deposito_lido number,
                       p_nivel varchar2,
                       p_grupo varchar2,
                       p_subgrupo varchar2,
                       p_item varchar2) is
      select e.cditem_nivel99 nivel, e.cditem_grupo grupo, e.cditem_subgrupo subgrupo,
             e.cditem_item item,     e.deposito deposito,  0 lote_acomp,
             sum(e.qtde_estoque_atu) qtde_estoque,sum(e.qtde_empenhada) qtde_empenhada,
             sum(e.qtde_sugerida) qtde_sugerida
      from estq_040 e, basi_030 s
      where e.deposito = p_deposito_lido
        and s.cor_de_estoque <> '000000'
        and e.qtde_estoque_atu <> 0
        and e.cditem_nivel99    = s.nivel_estrutura
        and e.cditem_grupo      = s.referencia
        and ( e.cditem_nivel99  = p_nivel or p_nivel = 'X')
        and ( e.cditem_grupo    = p_grupo or p_grupo = 'XXXXX')
        and ( e.cditem_subgrupo = p_subgrupo or p_subgrupo = 'XXX')
        and ( e.cditem_item     = p_item or p_item = 'XXXXXX')

       group by e.cditem_nivel99, e.cditem_grupo, e.cditem_subgrupo, e.cditem_item, e.deposito,0;

       cursor estq300310 (p_deposito_lido number, p_data_base date,
                         p_nivel varchar2,
                         p_grupo varchar2,
                         p_subgrupo varchar2,
                         p_item varchar2) is
       select q.codigo_deposito deposito,
             q.nivel_estrutura nivel,
             q.grupo_estrutura grupo,
             q.subgrupo_estrutura subgrupo,
             q.item_estrutura item,
             0 lote_acomp,
             sum(decode(entrada_saida,'E',q.quantidade,(q.quantidade * -1))) v_qtde_kardex,
             avg(q.preco_medio_unitario) preco_medio_unitario
       from estq_300_estq_310 q
       where q.codigo_deposito      = p_deposito_lido
         and q.data_movimento      >= p_data_base
         and ( q.nivel_estrutura    = p_nivel or p_nivel = 'X')
         and ( q.grupo_estrutura    = p_grupo or p_grupo = 'XXXXX')
         and ( q.subgrupo_estrutura = p_subgrupo or p_subgrupo = 'XXX')
         and ( q.item_estrutura     = p_item or p_item = 'XXXXXX')
       group by q.codigo_deposito,
             q.nivel_estrutura,
             q.grupo_estrutura,
             q.subgrupo_estrutura,
             q.item_estrutura,
             0;

      -- Criando o loop de caixas
      cursor estq060 (p_deposito_lido number,
                   p_nivel varchar2,
                   p_grupo varchar2,
                   p_subgrupo varchar2,
                   p_item varchar2) is
      select e.codigo_deposito deposito,   e.prodcai_nivel99 nivel,     e.prodcai_grupo grupo,
             e.prodcai_subgrupo subgrupo,  e.prodcai_item    item,      0 lote_acomp,
             sum(e.peso_liquido) peso_rolos
      from estq_060 e
      where e.status_caixa   not in (4,6,8,9)
        and e.codigo_deposito    = p_deposito_lido
        and ( e.prodcai_nivel99  = p_nivel or p_nivel = 'X')
        and ( e.prodcai_grupo    = p_grupo or p_grupo = 'XXXXX')
        and ( e.prodcai_subgrupo = p_subgrupo or p_subgrupo = 'XXX')
        and ( e.prodcai_item     = p_item or p_item = 'XXXXXX')
      group by e.codigo_deposito  ,  e.prodcai_nivel99 ,     e.prodcai_grupo ,
               e.prodcai_subgrupo ,  e.prodcai_item    ,     0;

      -- Criando o loop de fardos
      cursor pcpf060 (p_deposito_lido number,
                   p_nivel varchar2,
                   p_grupo varchar2,
                   p_subgrupo varchar2,
                   p_item varchar2) is
      select f.deposito deposito,        f.fardo_nivel99 nivel,       f.fardo_grupo grupo,
             f.fardo_subgrupo subgrupo,  f.fardo_item    item,        0 lote_acomp,
             nvl(sum(decode(peso_real, 0, peso_medio, peso_real)), 0) peso_rolos
      from pcpf_060 f
      where f.status_fardo   not in (2,5)
        and f.deposito = p_deposito_lido
        and ( f.fardo_nivel99  = p_nivel or p_nivel = 'X')
        and ( f.fardo_grupo    = p_grupo or p_grupo = 'XXXXX')
        and ( f.fardo_subgrupo = p_subgrupo or p_subgrupo = 'XXX')
        and ( f.fardo_item     = p_item or p_item = 'XXXXXX')
      group by f.deposito,      f.fardo_nivel99,     f.fardo_grupo,
             f.fardo_subgrupo,  f.fardo_item,        0;

procedure atualiza_list_critica_estoque(pr_ind_partida          varchar2,
                           pr_id_execucao          integer,
                           pr_nivel                varchar2,
                           pr_grupo                varchar2,
                           pr_subgrupo             varchar2,
                           pr_item                 varchar2,
                           pr_deposito             integer,
                           pr_lote                 integer,
                           pr_qtde_estoque         number,
                           pr_qtde_em_vol          number,
                           pr_qtde_em_card         number,
                           pr_qtde_sugerida        number,
                           pr_qtde_empenhada       number,
                           pr_preco_medio_unitario number,
                           pr_observacao           varchar2,
               pr_tipo_volume       number) is
       
       w_cor_estoque varchar2(6);
begin
    select basi_030.cor_de_estoque
    into w_cor_estoque
    from basi_030
    where basi_030.nivel_estrutura = pr_nivel
    and basi_030.referencia        = pr_grupo;
    if w_cor_estoque is null
    then
       w_cor_estoque := '000000';
    end if;

     begin
      select count(*) into v_existe_diferenca
      from list_critica_estoque
      where nivel = pr_nivel
        and grupo = pr_grupo
        and subgrupo = pr_subgrupo
        and item = pr_item
        and deposito = pr_deposito
        and lote = pr_lote
        and qtde_estoque = pr_qtde_estoque
        and qtde_em_vol = pr_qtde_em_vol
        and qtde_em_card = pr_qtde_em_card
        and trim(ind_partida) = trim(pr_ind_partida)
        and data_diverg_estq is not null;
    end;

    if v_existe_diferenca = 0 -- PRODUTO N?O TINHA DIFEREN?A OU EST? COM DIFEREN?A NOVA
    then    
    
       insert into list_critica_estoque
         (ind_partida,
          id_execucao,
          nivel,
          grupo,
          subgrupo,
          item,
          cor_estoque,
          deposito,
          lote,
          qtde_estoque,
          qtde_em_vol,
          qtde_em_card,
          qtde_sugerida,
          qtde_empenhada,
          preco_medio_unitario,
          data_execucao,
          observacao,
          data_diverg_estq,
          difer_vol_kardex,
          difer_sld_kardex,
          difer_vol_sld   
          
          )
       select
         pr_ind_partida,
          pr_id_execucao,
          pr_nivel,
          pr_grupo,
          pr_subgrupo,
          pr_item,
          w_cor_estoque,
          pr_deposito,
          pr_lote,
          pr_qtde_estoque,
          pr_qtde_em_vol,
          pr_qtde_em_card,
          pr_qtde_sugerida,
          pr_qtde_empenhada,
          pr_preco_medio_unitario,
          sysdate,
          pr_observacao,
          sysdate,          
          (case when pr_tipo_volume > 0 then pr_qtde_em_vol - pr_qtde_em_card else 0 end),
          pr_qtde_estoque - pr_qtde_em_card,
          (case when pr_tipo_volume > 0 then pr_qtde_estoque - pr_qtde_em_vol else 0 end)
       from dual;

    end if;
    commit;
end atualiza_list_critica_estoque;

BEGIN

   v_id_execucao := p_id_execucao; -- gerar com dia mes ano hora minuto segundo
   select periodo_estoque
   into data_base
   from empr_001;

   select empr_002.forma_calculo_cardex
   into   v_forma_calculo_cardex
   from empr_002;

   -- le depositos escolhidos em paramentros
   for reg_basi205 in basi205(p_empresa)
   loop

       if reg_basi205.tipo_volume = 1
       then
          /* UTILIZAMOS ESTE CURSOR PARA QUANDO A REFERENCIA FOI PASSAD A PARA COR DE ESTOQUE
             ENQUANTO TEM ESTOQUE NA COR.
             DESTA FOR TEMO QUE TIRAR TODOS O SALDO DAS CORRES EM ESTOQUE, FAZENDO MOVIMENTA??O
          */

         v_ind_partida := 'Cor de estoque - Partindo da tabela de Estoques e comparando com Tag`s onde a cor de estoque';
         for sdo in estq040Sort (reg_basi205.codigo_deposito,
                                 p_nivel,
                                 p_grupo,
                                 p_subgrupo,
                                 p_item)
         loop

            select sum(decode(entrada_saida,'E',k.quantidade,(k.quantidade * -1)))
            into v_saldo_k
            from estq_300_estq_310 k
            where k.nivel_estrutura    = sdo.nivel
              and k.grupo_estrutura    = sdo.grupo
              and k.subgrupo_estrutura = sdo.subgrupo
              and k.item_estrutura     = sdo.item
              and k.codigo_deposito    = sdo.deposito
              and k.numero_lote        = 0
              and k.data_movimento     >= data_base;

            if v_saldo_k is null
            then
               v_saldo_k := 0;
            end if;

            select sum(nvl(f.saldo_fisico,0.000)),
            avg(f.preco_medio_unitario) preco_medio_unitario
            into v_qtde_kardex_ant, v_preco_medio_unitario
            from estq_301 f
            where f.codigo_deposito   = sdo.deposito
             and f.nivel_estrutura    = sdo.nivel
             and f.grupo_estrutura    = sdo.grupo
             and f.subgrupo_estrutura = sdo.subgrupo
             and f.item_estrutura     = sdo.item
             and f.mes_ano_movimento  = (select max(i.mes_ano_movimento) from estq_301 i
             where i.codigo_deposito    = f.codigo_deposito
               and i.nivel_estrutura    = f.nivel_estrutura
               and i.grupo_estrutura    = f.grupo_estrutura
               and i.subgrupo_estrutura = f.subgrupo_estrutura
               and i.item_estrutura     = f.item_estrutura
               and i.mes_ano_movimento  < data_base);
            if v_qtde_kardex_ant is null
            then
               v_qtde_kardex_ant := 0;
            end if;

            if v_saldo_k + v_qtde_kardex_ant = 0
            then
                v_qtde040 := sdo.qtde_estoque;
            else
                v_qtde040 := 0;
            end if;

            if v_preco_medio_unitario is null
            then
                 v_preco_medio_unitario := 0;
            end if;

            select nvl(count(p.deposito),0)
            into v_qtderolos
            from pcpc_330 p, basi_030 s
            where p.deposito          = sdo.deposito
            and   p.nivel             = sdo.nivel
            and   p.grupo             = sdo.grupo
            and   p.subgrupo          = sdo.subgrupo
            and   p.nivel             = s.nivel_estrutura
            and   p.grupo             = s.referencia
            and   decode(s.cor_de_estoque,'000000', p.item,s.cor_de_estoque) = sdo.item
            and   p.estoque_tag not in (0,4,9);

            if v_qtderolos  is null
            then
                  v_qtderolos := 0;
            end if;

            if sdo.qtde_estoque <> v_saldo_k + v_qtde_kardex_ant or sdo.qtde_estoque <> v_qtderolos
            then
                atualiza_list_critica_estoque('Referencia na cor de estoque e estq_040 nas cores',
                                               v_id_execucao,
                                               sdo.nivel,
                                               sdo.grupo,
                                               sdo.subgrupo,
                                               sdo.item,
                                               sdo.deposito,
                                               0,
                                               sdo.qtde_estoque,
                                               0,
                                               v_saldo_k + v_qtde_kardex_ant,
                                               sdo.qtde_sugerida,
                                               sdo.qtde_empenhada,
                                               v_preco_medio_unitario,
                                               '', reg_basi205.tipo_volume);

            end if;

         end loop;


         /* Partindo da tabela de Estoques e comparando com Tag`s
            A ESTQ_040, PARTE ABERTO POR

           */


          v_ind_partida := 'Partindo da tabela de Estoques e comparando com Tag`s ';

         for reg_estq040 in estq040 (reg_basi205.codigo_deposito,
                                     p_nivel,
                                     p_grupo,
                                     p_subgrupo,
                                     p_item)
         loop

            select sum(decode(entrada_saida,'E',k.quantidade,(k.quantidade * -1)))
            into v_saldo_k
            from estq_300_estq_310 k
            where k.nivel_estrutura    = reg_estq040.nivel
              and k.grupo_estrutura    = reg_estq040.grupo
              and k.subgrupo_estrutura = reg_estq040.subgrupo
              and k.item_estrutura     = reg_estq040.item
              and k.codigo_deposito    = reg_estq040.deposito
              and k.data_movimento     >= data_base;

              if v_saldo_k is null
              then
                  v_saldo_k := 0;
              end if;

              select sum(nvl(f.saldo_fisico,0.000)), avg(f.preco_medio_unitario) preco_medio_unitario
              into v_qtde_kardex_ant,                v_preco_medio_unitario
              from estq_301 f
              where f.codigo_deposito    = reg_estq040.deposito
                  and f.nivel_estrutura    = reg_estq040.nivel
                  and f.grupo_estrutura    = reg_estq040.grupo
                  and f.subgrupo_estrutura = reg_estq040.subgrupo
                  and f.item_estrutura     = reg_estq040.item
                  and f.mes_ano_movimento  = (select max(i.mes_ano_movimento) from estq_301 i
                                              where i.codigo_deposito    = f.codigo_deposito
                                              and   i.nivel_estrutura    = f.nivel_estrutura
                                              and   i.grupo_estrutura    = f.grupo_estrutura
                                              and   i.subgrupo_estrutura = f.subgrupo_estrutura
                                              and   i.item_estrutura     = f.item_estrutura
                                              and   i.mes_ano_movimento  < data_base);
            if v_qtde_kardex_ant is null
            then
               v_qtde_kardex_ant := 0;
            end if;

            if v_preco_medio_unitario is null
            then
               v_preco_medio_unitario := 0;
            end if;


            select nvl(count(p.deposito),0)
            into v_qtderolos
            from pcpc_330 p, basi_030 s
            where p.deposito          = reg_estq040.deposito
            and   p.nivel             = reg_estq040.nivel
            and   p.grupo             = reg_estq040.grupo
            and   p.subgrupo          = reg_estq040.subgrupo
            and   p.nivel             = s.nivel_estrutura
            and   p.grupo             = s.referencia
            and   decode(s.cor_de_estoque,'000000', p.item,s.cor_de_estoque) = reg_estq040.item
            and   p.estoque_tag not in (0,4,9);

            if v_qtderolos  is null
            then
                  v_qtderolos := 0;
            end if;

            select nvl(sum(qtde_estoque_atu),0), nvl(sum(e.qtde_empenhada),0), nvl(sum(e.qtde_sugerida),0)
            into v_qtde040, v_qtde_empenhada, v_qtde_sugerida
            from estq_040 e
            where e.cditem_nivel99  = reg_estq040.nivel
            and   e.cditem_grupo    = reg_estq040.grupo
            and   e.cditem_subgrupo = reg_estq040.subgrupo
            and   e.cditem_item     = reg_estq040.item
            and   e.deposito        = reg_estq040.deposito;

            if v_qtde040 is null
            then
               v_qtde040 := 0;
            end if;

            if v_qtde_empenhada is null
            then
               v_qtde_empenhada := 0;
            end if;

            if v_qtde_sugerida is null
            then
               v_qtde_sugerida := 0;
            end if;


            ajuste_sub := null;
            if length(reg_estq040.subgrupo) = 1 then
               ajuste_sub := '  ';
            end if;

            if length(reg_estq040.subgrupo) = 2 then
               ajuste_sub := ' ';
            end if;

            if v_qtde040 <> v_qtderolos or v_qtde040 <> v_saldo_k + v_qtde_kardex_ant
            then
               atualiza_list_critica_estoque(v_ind_partida,
                                             v_id_execucao,
                                             reg_estq040.nivel,
                                             reg_estq040.grupo,
                                             reg_estq040.subgrupo,
                                             reg_estq040.item,
                                             reg_estq040.deposito,
                                             reg_estq040.lote_acomp,
                                             v_qtde040,
                                             v_qtderolos,
                                             v_saldo_k + v_qtde_kardex_ant,
                                             reg_estq040.qtde_sugerida,
                                             reg_estq040.qtde_empenhada,
                                             v_preco_medio_unitario,
                                             '', reg_basi205.tipo_volume);
            end if;
         end loop;

         v_ind_partida := 'Partindo da tabela de TAG`S e comparando com Estoque';

         for reg_pcpc330 in pcpc330 (reg_basi205.codigo_deposito,
                                     p_nivel,
                                     p_grupo,
                                     p_subgrupo,
                                     p_item)
         loop

            select sum(decode(entrada_saida,'E',k.quantidade,(k.quantidade * -1)))
            into v_saldo_k
            from estq_300_estq_310 k
            where k.nivel_estrutura    = reg_pcpc330.nivel
              and k.grupo_estrutura    = reg_pcpc330.grupo
              and k.subgrupo_estrutura = reg_pcpc330.subgrupo
              and k.item_estrutura     = reg_pcpc330.cor_estoque_tag
              and k.codigo_deposito    = reg_pcpc330.deposito
              and k.data_movimento     >= data_base;

            if v_saldo_k is null
            then
              v_saldo_k := 0;
            end if;

            select sum(nvl(f.saldo_fisico,0.000)), avg(f.preco_medio_unitario) preco_medio_unitario
            into v_qtde_kardex_ant,                v_preco_medio_unitario
            from estq_301 f
            where f.codigo_deposito    = reg_pcpc330.deposito
                    and f.nivel_estrutura    = reg_pcpc330.nivel
                    and f.grupo_estrutura    = reg_pcpc330.grupo
                    and f.subgrupo_estrutura = reg_pcpc330.subgrupo
                    and f.item_estrutura     = reg_pcpc330.cor_estoque_tag
                    and f.mes_ano_movimento  = (select max(i.mes_ano_movimento) from estq_301 i
                                                where i.codigo_deposito    = f.codigo_deposito
                                                  and i.nivel_estrutura    = f.nivel_estrutura
                                                  and i.grupo_estrutura    = f.grupo_estrutura
                                                  and i.subgrupo_estrutura = f.subgrupo_estrutura
                                                  and i.item_estrutura     = f.item_estrutura
                                                  and i.mes_ano_movimento  < data_base);
            if v_qtde_kardex_ant is null
            then
                v_qtde_kardex_ant := 0;
            end if;

            if v_preco_medio_unitario is null
            then
               v_preco_medio_unitario := 0;
            end if;

            select nvl(sum(qtde_estoque_atu),0), nvl(sum(e.qtde_empenhada),0), nvl(sum(e.qtde_sugerida),0)
            into v_qtde040, v_qtde_empenhada, v_qtde_sugerida
            from estq_040 e
            where e.cditem_nivel99  = reg_pcpc330.nivel
            and   e.cditem_grupo    = reg_pcpc330.grupo
            and   e.cditem_subgrupo = reg_pcpc330.subgrupo
            and   e.cditem_item     = reg_pcpc330.cor_estoque_tag
            and   e.deposito        = reg_pcpc330.deposito;

            if v_qtde040 is null
            then
               v_qtde040 := 0;
            end if;

            if v_qtde_empenhada is null
            then
               v_qtde_empenhada := 0;
            end if;

            if v_qtde_sugerida is null
            then
               v_qtde_sugerida := 0;
            end if;

            select nvl(count(p.deposito),0)
            into v_qtderolos
            from pcpc_330 p, basi_030 s
            where p.deposito          = reg_pcpc330.deposito
            and   p.nivel             = reg_pcpc330.nivel
            and   p.grupo             = reg_pcpc330.grupo
            and   p.subgrupo          = reg_pcpc330.subgrupo
            and   p.nivel             = s.nivel_estrutura
            and   p.grupo             = s.referencia
            and   decode(s.cor_de_estoque,'000000', p.item,s.cor_de_estoque) = reg_pcpc330.item
            and   p.estoque_tag not in (0,4,9);

            if v_qtderolos  is null
            then
                  v_qtderolos := 0;
            end if;

            ajuste_sub := null;
            if length(reg_pcpc330.subgrupo) = 1 then
               ajuste_sub := '  ';
            end if;

            if length(reg_pcpc330.subgrupo) = 2 then
               ajuste_sub := ' ';
            end if;

            if v_qtderolos <> v_qtde040 or v_qtderolos <> v_saldo_k + v_qtde_kardex_ant
            then
               atualiza_list_critica_estoque(v_ind_partida,
                                             v_id_execucao,
                                             reg_pcpc330.nivel,
                                             reg_pcpc330.grupo,
                                             reg_pcpc330.subgrupo,
                                             reg_pcpc330.cor_estoque_tag,
                                             reg_pcpc330.deposito,
                                             reg_pcpc330.lote_acomp,
                                             v_qtde040,
                                             v_qtderolos,
                                             v_saldo_k + v_qtde_kardex_ant,
                                             v_qtde_sugerida,
                                             v_qtde_empenhada,
                                             v_preco_medio_unitario,
                                             '', reg_basi205.tipo_volume);
            end if;
         end loop;
       end if; --- fim tipo_volume = 1

       -- ---------- TIPO VOLUME ZERO -----------------------------------------------------------------

      if reg_basi205.tipo_volume = 0 -- SEM VOLUMES
      then
           -- Partindo da tabela de Estoques e comparando com Kardex
          v_ind_partida := 'Partindo da tabela de Estoques e comparando com Kardex ';

         for reg_estq040 in estq040 (reg_basi205.codigo_deposito,
                                     p_nivel,
                                     p_grupo,
                                     p_subgrupo,
                                     p_item)
         loop
            v_sequencia_ficha := null;
            v_qtde_kardex_ant := 0;
            v_saldo_k := 0;
            v_saldo_k := 0;
            v_qtderolos := 0;
            v_qtde040   := 0;
            v_preco_medio_unitario := 0;
            v_qtderolos  := 0.000;
            v_observacao := '';

            /* NIVEL 1 LE SEM CONSIDERAR LOTE PARA FICAR MAIS R?PIDO */


            select sum(decode(entrada_saida,'E',k.quantidade,(k.quantidade * -1)))
            into v_saldo_k
            from estq_300_estq_310 k
            where k.nivel_estrutura    = reg_estq040.nivel
              and k.grupo_estrutura    = reg_estq040.grupo
              and k.subgrupo_estrutura = reg_estq040.subgrupo
              and k.item_estrutura     = reg_estq040.item
              and k.codigo_deposito    = reg_estq040.deposito
              and k.data_movimento     >= data_base;
            if v_saldo_k is null
            then
                v_saldo_k := 0;
            end if;

            BEGIN
            select nvl(f.saldo_fisico,0.000), nvl(f.preco_medio_unitario,0)
            into v_qtde_kardex_ant,           v_preco_medio_unitario
            from estq_301 f
            where f.codigo_deposito    = reg_estq040.deposito
              and f.nivel_estrutura    = reg_estq040.nivel
              and f.grupo_estrutura    = reg_estq040.grupo
              and f.subgrupo_estrutura = reg_estq040.subgrupo
              and f.item_estrutura     = reg_estq040.item
              and f.mes_ano_movimento  = (select max(i.mes_ano_movimento) from estq_301 i
                                          where i.codigo_deposito    = f.codigo_deposito
                                            and i.nivel_estrutura    = f.nivel_estrutura
                                            and i.grupo_estrutura    = f.grupo_estrutura
                                            and i.subgrupo_estrutura = f.subgrupo_estrutura
                                            and i.item_estrutura     = f.item_estrutura
                                            and i.mes_ano_movimento  < data_base);
             EXCEPTION
                WHEN OTHERS THEN
                v_qtde_kardex_ant := 0;
                v_preco_medio_unitario :=0;
            END;

            if v_qtde_kardex_ant is null
            then
                v_qtde_kardex_ant := 0;
            end if;

            select nvl(sum(qtde_estoque_atu),0), nvl(sum(e.qtde_empenhada),0), nvl(sum(e.qtde_sugerida),0)
            into v_qtde040, v_qtde_empenhada, v_qtde_sugerida
            from estq_040 e
            where e.cditem_nivel99  = reg_estq040.nivel
            and   e.cditem_grupo    = reg_estq040.grupo
            and   e.cditem_subgrupo = reg_estq040.subgrupo
            and   e.cditem_item     = reg_estq040.item
            and   e.deposito        = reg_estq040.deposito;

            if v_qtde040 is null
            then
               v_qtde040 := 0;
            end if;

            if v_qtde_empenhada is null
            then
               v_qtde_empenhada := 0;
            end if;

            if v_qtde_sugerida is null
            then
               v_qtde_sugerida := 0;
            end if;

            ajuste_sub := null;
            if length(reg_estq040.subgrupo) = 1 then
               ajuste_sub := '  ';
            end if;

            if length(reg_estq040.subgrupo) = 2 then
               ajuste_sub := ' ';
            end if;


            if v_qtde040 <> v_saldo_k + v_qtde_kardex_ant
            then
               atualiza_list_critica_estoque(v_ind_partida,
                                             v_id_execucao,
                                             reg_estq040.nivel,
                                             reg_estq040.grupo,
                                             reg_estq040.subgrupo,
                                             reg_estq040.item,
                                             reg_estq040.deposito,
                                             reg_estq040.lote_acomp,
                                             v_qtde040,
                                             0,
                                             v_saldo_k + v_qtde_kardex_ant,
                                             v_qtde_sugerida,
                                             v_qtde_empenhada,
                                             v_preco_medio_unitario,
                                             v_observacao, reg_basi205.tipo_volume);

            end if;
         end loop;
         --Partindo da Kardex e comparando com Estoque
         v_ind_partida := 'Partindo da Kardex e comparando com Estoque';

         for reg_estq300310 in estq300310 (reg_basi205.codigo_deposito,
                                           data_base,
                                           p_nivel,
                                           p_grupo,
                                           p_subgrupo,
                                           p_item)
         loop
            v_observacao := '';





            v_sequencia_ficha := null;
            v_qtde_kardex_ant := 0;
            v_saldo_k := 0;
            v_saldo_k := 0;
            v_qtderolos := 0;
            v_qtde040   := 0;
            v_preco_medio_unitario := 0;
            v_qtderolos  := 0.000;
            v_observacao := '';


            select nvl(sum(qtde_estoque_atu),0), nvl(sum(qtde_sugerida),0), nvl(sum(qtde_empenhada),0)
            into v_qtde040,                      v_qtde_sugerida,           v_qtde_empenhada
            from estq_040 e
            where e.cditem_nivel99  = reg_estq300310.nivel
            and   e.cditem_grupo    = reg_estq300310.grupo
            and   e.cditem_subgrupo = reg_estq300310.subgrupo
            and   e.cditem_item     = reg_estq300310.item
            and   e.deposito        = reg_estq300310.deposito;

            BEGIN
            select nvl(f.saldo_fisico,0.000), nvl(f.preco_medio_unitario,0)
            into v_qtde_kardex_ant,           v_preco_medio_unitario
            from estq_301 f
            where f.codigo_deposito    = reg_estq300310.deposito
              and f.nivel_estrutura    = reg_estq300310.nivel
              and f.grupo_estrutura    = reg_estq300310.grupo
              and f.subgrupo_estrutura = reg_estq300310.subgrupo
              and f.item_estrutura     = reg_estq300310.item
              and f.mes_ano_movimento  = (select max(i.mes_ano_movimento) from estq_301 i
                                          where i.codigo_deposito    = f.codigo_deposito
                                            and i.nivel_estrutura    = f.nivel_estrutura
                                            and i.grupo_estrutura    = f.grupo_estrutura
                                            and i.subgrupo_estrutura = f.subgrupo_estrutura
                                            and i.item_estrutura     = f.item_estrutura
                                            and i.mes_ano_movimento  < data_base);
            EXCEPTION
                WHEN OTHERS THEN
                v_qtde_kardex_ant := 0;
                v_preco_medio_unitario :=0;
            END;



            if v_qtde_kardex_ant is null
            then
                v_qtde_kardex_ant := 0;
            end if;


            if reg_estq300310.v_qtde_kardex + v_qtde_kardex_ant <> v_qtde040
            then
               atualiza_list_critica_estoque(v_ind_partida,
                                             v_id_execucao,
                                             reg_estq300310.nivel,
                                             reg_estq300310.grupo,
                                             reg_estq300310.subgrupo,
                                             reg_estq300310.item,
                                             reg_estq300310.deposito,
                                             reg_estq300310.lote_acomp,
                                             v_qtde040,
                                             0,
                                             reg_estq300310.v_qtde_kardex + v_qtde_kardex_ant,
                                             v_qtde_sugerida,
                                             v_qtde_empenhada,
                                             v_preco_medio_unitario,
                                             v_observacao, reg_basi205.tipo_volume);

            end if;
         end loop;
      end if; --fim tipo volume zero

      -- --------- 2,4 = ROLOS ACABADOS -------------------------------------------------------------

      if reg_basi205.tipo_volume = 2 or
         reg_basi205.tipo_volume = 4
      then



         /* Partindo da tabela de Estoques e comparando com ROLOS  */


          v_ind_partida := 'Partindo da tabela de Estoques e comparando com Rolos ';

         for reg_estq040 in estq040 (reg_basi205.codigo_deposito,
                                     p_nivel,
                                     p_grupo,
                                     p_subgrupo,
                                     p_item)
         loop

            v_sequencia_ficha := null;
            v_qtde_kardex_ant := 0;
            v_saldo_k := 0;
            v_saldo_k := 0;


            BEGIN
            select nvl(f.saldo_fisico,0.000), nvl(f.preco_medio_unitario,0)
            into v_qtde_kardex_ant,           v_preco_medio_unitario
            from estq_301 f
            where f.codigo_deposito    = reg_estq040.deposito
              and f.nivel_estrutura    = reg_estq040.nivel
              and f.grupo_estrutura    = reg_estq040.grupo
              and f.subgrupo_estrutura = reg_estq040.subgrupo
              and f.item_estrutura     = reg_estq040.item
              and f.mes_ano_movimento  = (select max(i.mes_ano_movimento) from estq_301 i
                                          where i.codigo_deposito    = f.codigo_deposito
                                            and i.nivel_estrutura    = f.nivel_estrutura
                                            and i.grupo_estrutura    = f.grupo_estrutura
                                            and i.subgrupo_estrutura = f.subgrupo_estrutura
                                            and i.item_estrutura     = f.item_estrutura
                                            and i.mes_ano_movimento  < data_base);
             EXCEPTION
                WHEN OTHERS THEN
                v_qtde_kardex_ant := 0;
                v_preco_medio_unitario :=0;
            END;


            if v_qtde_kardex_ant is null
            then
               v_qtde_kardex_ant := 0.000;
            end if;


            select sum(decode(entrada_saida,'E',k.quantidade,(k.quantidade * -1)))
            into v_saldo_k
            from estq_300_estq_310 k
            where k.nivel_estrutura    = reg_estq040.nivel
              and k.grupo_estrutura    = reg_estq040.grupo
              and k.subgrupo_estrutura = reg_estq040.subgrupo
              and k.item_estrutura     = reg_estq040.item
              and k.codigo_deposito    = reg_estq040.deposito
              and k.data_movimento     >= data_base;

            if v_saldo_k is null
            then
               v_saldo_k := 0;
            end if;

            select nvl(sum(p.qtde_quilos_acab),0.000)
            into v_qtderolos
            from pcpt_020 p
            where p.codigo_deposito   = reg_estq040.deposito
            and   p.panoacab_nivel99  = reg_estq040.nivel
            and   p.panoacab_grupo    = reg_estq040.grupo
            and   p.panoacab_subgrupo = reg_estq040.subgrupo
            and   p.panoacab_item     = reg_estq040.item
            and   p.rolo_estoque not in (0,2);

            if v_qtderolos is null
            then
               v_qtderolos  := 0.000;
            end if;

            select nvl(sum(qtde_estoque_atu),0), nvl(sum(e.qtde_empenhada),0), nvl(sum(e.qtde_sugerida),0)
            into v_qtde040, v_qtde_empenhada, v_qtde_sugerida
            from estq_040 e
            where e.cditem_nivel99  = reg_estq040.nivel
            and   e.cditem_grupo    = reg_estq040.grupo
            and   e.cditem_subgrupo = reg_estq040.subgrupo
            and   e.cditem_item     = reg_estq040.item
            and   e.deposito        = reg_estq040.deposito;

            if v_qtde040 is null
            then
               v_qtde040 := 0;
            end if;

            if v_qtde_empenhada is null
            then
               v_qtde_empenhada := 0;
            end if;

            if v_qtde_sugerida is null
            then
               v_qtde_sugerida := 0;
            end if;

            ajuste_sub := null;

            if length(reg_estq040.subgrupo) = 1 then
               ajuste_sub := '  ';
            end if;

            if length(reg_estq040.subgrupo) = 2 then
               ajuste_sub := ' ';
            end if;


            if v_qtde040 <> v_qtderolos or v_qtde040 <> v_saldo_k + v_qtde_kardex_ant
            then
               atualiza_list_critica_estoque(v_ind_partida,
                                             v_id_execucao,
                                             reg_estq040.nivel,
                                             reg_estq040.grupo,
                                             reg_estq040.subgrupo,
                                             reg_estq040.item,
                                             reg_estq040.deposito,
                                             reg_estq040.lote_acomp,
                                             v_qtde040,
                                             v_qtderolos,
                                             v_saldo_k + v_qtde_kardex_ant,
                                             v_qtde_sugerida,
                                             v_qtde_empenhada,
                                             v_preco_medio_unitario,
                                             v_observacao, reg_basi205.tipo_volume);

            end if;
         end loop;

         v_ind_partida := 'Partindo da tabela de Rolos e comparando com Estoque';

         for reg_pcpt020 in pcpt020 (reg_basi205.codigo_deposito,
                                     p_nivel,
                                     p_grupo,
                                     p_subgrupo,
                                     p_item)
         loop

            /*
             VERIFICA SALDO DOS ROLOS SEM CONSIDERAR LOTE COMPARA COM A ESTQ_040 SEM LOTE
             PARA TRATAR ACERTOS ERRADOS COM LOTES NA ESTQ_300_ESTQ_310 */

            v_sequencia_ficha := null;
            v_qtde_kardex_ant := 0;
            v_saldo_k := 0;
            v_saldo_k := 0;

            BEGIN
            select nvl(f.saldo_fisico,0.000), nvl(f.preco_medio_unitario,0)
            into v_qtde_kardex_ant,           v_preco_medio_unitario
            from estq_301 f
            where f.codigo_deposito    = reg_pcpt020.deposito
              and f.nivel_estrutura    = reg_pcpt020.nivel
              and f.grupo_estrutura    = reg_pcpt020.grupo
              and f.subgrupo_estrutura = reg_pcpt020.subgrupo
              and f.item_estrutura     = reg_pcpt020.item
              and f.mes_ano_movimento  = (select max(i.mes_ano_movimento) from estq_301 i
                                          where i.codigo_deposito    = f.codigo_deposito
                                            and i.nivel_estrutura    = f.nivel_estrutura
                                            and i.grupo_estrutura    = f.grupo_estrutura
                                            and i.subgrupo_estrutura = f.subgrupo_estrutura
                                            and i.item_estrutura     = f.item_estrutura
                                            and i.mes_ano_movimento  < data_base);
             EXCEPTION
                WHEN OTHERS THEN
                v_qtde_kardex_ant := 0;
                v_preco_medio_unitario :=0;
            END;


            if v_qtde_kardex_ant is null
            then
               v_qtde_kardex_ant := 0.000;
            end if;

            select sum(decode(entrada_saida,'E',k.quantidade,(k.quantidade * -1)))
            into v_saldo_k
            from estq_300_estq_310 k
            where k.nivel_estrutura    = reg_pcpt020.nivel
              and k.grupo_estrutura    = reg_pcpt020.grupo
              and k.subgrupo_estrutura = reg_pcpt020.subgrupo
              and k.item_estrutura     = reg_pcpt020.item
              and k.codigo_deposito    = reg_pcpt020.deposito
              and k.data_movimento     >= data_base;


            if v_saldo_k is null
            then
              v_saldo_k := 0;
            end if;

            select nvl(sum(qtde_estoque_atu),0), nvl(sum(e.qtde_empenhada),0), nvl(sum(e.qtde_sugerida),0)
            into v_qtde040, v_qtde_empenhada, v_qtde_sugerida
            from estq_040 e
            where e.cditem_nivel99  = reg_pcpt020.nivel
            and   e.cditem_grupo    = reg_pcpt020.grupo
            and   e.cditem_subgrupo = reg_pcpt020.subgrupo
            and   e.cditem_item     = reg_pcpt020.item
            and   e.deposito        = reg_pcpt020.deposito;

            if v_qtde040 is null
            then
               v_qtde040 := 0;
            end if;

            select nvl(sum(p.qtde_quilos_acab),0.000)
            into v_qtderolos
            from pcpt_020 p
            where p.codigo_deposito   = reg_pcpt020.deposito
            and   p.panoacab_nivel99  = reg_pcpt020.nivel
            and   p.panoacab_grupo    = reg_pcpt020.grupo
            and   p.panoacab_subgrupo = reg_pcpt020.subgrupo
            and   p.panoacab_item     = reg_pcpt020.item
            and   p.rolo_estoque not in (0,2);

            if v_qtderolos is null
            then
               v_qtderolos  := 0.000;
            end if;

            ajuste_sub := null;
            if length(reg_pcpt020.subgrupo) = 1 then
               ajuste_sub := '  ';
            end if;

            if length(reg_pcpt020.subgrupo) = 2 then
               ajuste_sub := ' ';
            end if;


            if v_qtderolos <> v_qtde040 or v_qtderolos <> v_saldo_k + v_qtde_kardex_ant
            then
               atualiza_list_critica_estoque(v_ind_partida,
                                             v_id_execucao,
                                             reg_pcpt020.nivel,
                                             reg_pcpt020.grupo,
                                             reg_pcpt020.subgrupo,
                                             reg_pcpt020.item,
                                             reg_pcpt020.deposito,
                                             reg_pcpt020.lote_acomp,
                                             v_qtde040,
                                             v_qtderolos,
                                             v_saldo_k + v_qtde_kardex_ant,
                                             v_qtde_sugerida,
                                             v_qtde_empenhada,
                                             v_preco_medio_unitario,
                                             v_observacao, reg_basi205.tipo_volume);

            end if;
         end loop;
      end if;  --- fim tipo_volume = 2

      -- --------------------------  CAIXAS DE FIOS -------------------------------------------------

      if reg_basi205.tipo_volume = 7
      then

         /* Partindo da tabela de Estoques e comparando com Caixas  */


          v_ind_partida := 'Partindo da tabela de Estoques e comparando com Caixas ';

         for reg_estq040 in estq040 (reg_basi205.codigo_deposito,
                                     p_nivel,
                                     p_grupo,
                                     p_subgrupo,
                                     p_item)
         loop

            v_sequencia_ficha := null;
            v_qtde_kardex_ant := 0;
            v_saldo_k := 0;
            v_saldo_k := 0;


            BEGIN
            select nvl(f.saldo_fisico,0.000), nvl(f.preco_medio_unitario,0)
            into v_qtde_kardex_ant,           v_preco_medio_unitario
            from estq_301 f
            where f.codigo_deposito    = reg_estq040.deposito
              and f.nivel_estrutura    = reg_estq040.nivel
              and f.grupo_estrutura    = reg_estq040.grupo
              and f.subgrupo_estrutura = reg_estq040.subgrupo
              and f.item_estrutura     = reg_estq040.item
              and f.mes_ano_movimento  = (select max(i.mes_ano_movimento) from estq_301 i
                                          where i.codigo_deposito    = f.codigo_deposito
                                            and i.nivel_estrutura    = f.nivel_estrutura
                                            and i.grupo_estrutura    = f.grupo_estrutura
                                            and i.subgrupo_estrutura = f.subgrupo_estrutura
                                            and i.item_estrutura     = f.item_estrutura
                                            and i.mes_ano_movimento  < data_base);
             EXCEPTION
                WHEN OTHERS THEN
                v_qtde_kardex_ant := 0;
                v_preco_medio_unitario :=0;
            END;


            if v_qtde_kardex_ant is null
            then
               v_qtde_kardex_ant := 0.000;
            end if;


            select sum(decode(entrada_saida,'E',k.quantidade,(k.quantidade * -1)))
            into v_saldo_k
            from estq_300_estq_310 k
            where k.nivel_estrutura    = reg_estq040.nivel
              and k.grupo_estrutura    = reg_estq040.grupo
              and k.subgrupo_estrutura = reg_estq040.subgrupo
              and k.item_estrutura     = reg_estq040.item
              and k.codigo_deposito    = reg_estq040.deposito
              and k.data_movimento     >= data_base;

            if v_saldo_k is null
            then
               v_saldo_k := 0;
            end if;

            select  nvl(sum(peso_liquido), 0) into v_qtderolos from estq_060 e
            where e.codigo_deposito  = reg_estq040.deposito
              and e.prodcai_nivel99  = reg_estq040.nivel
              and e.prodcai_grupo    = reg_estq040.grupo
              and e.prodcai_subgrupo = reg_estq040.subgrupo
              and e.prodcai_item     = reg_estq040.item
              and e.status_caixa     not in (4,6,8,9);

            if v_qtderolos is null
            then
               v_qtderolos  := 0.000;
            end if;

            select nvl(sum(qtde_estoque_atu),0), nvl(sum(e.qtde_empenhada),0), nvl(sum(e.qtde_sugerida),0)
            into v_qtde040, v_qtde_empenhada, v_qtde_sugerida
            from estq_040 e
            where e.cditem_nivel99  = reg_estq040.nivel
            and   e.cditem_grupo    = reg_estq040.grupo
            and   e.cditem_subgrupo = reg_estq040.subgrupo
            and   e.cditem_item     = reg_estq040.item
            and   e.deposito        = reg_estq040.deposito;

            if v_qtde040 is null
            then
               v_qtde040 := 0;
            end if;

            if v_qtde_empenhada is null
            then
               v_qtde_empenhada := 0;
            end if;

            if v_qtde_sugerida is null
            then
               v_qtde_sugerida := 0;
            end if;

            ajuste_sub := null;

            if length(reg_estq040.subgrupo) = 1 then
               ajuste_sub := '  ';
            end if;

            if length(reg_estq040.subgrupo) = 2 then
               ajuste_sub := ' ';
            end if;


            if v_qtde040 <> v_qtderolos or v_qtde040 <> v_saldo_k + v_qtde_kardex_ant
            then
               atualiza_list_critica_estoque(v_ind_partida,
                                             v_id_execucao,
                                             reg_estq040.nivel,
                                             reg_estq040.grupo,
                                             reg_estq040.subgrupo,
                                             reg_estq040.item,
                                             reg_estq040.deposito,
                                             reg_estq040.lote_acomp,
                                             v_qtde040,
                                             v_qtderolos,
                                             v_saldo_k + v_qtde_kardex_ant,
                                             v_qtde_sugerida,
                                             v_qtde_empenhada,
                                             v_preco_medio_unitario,
                                             v_observacao, reg_basi205.tipo_volume);

            end if;
         end loop;

         v_ind_partida := 'Partindo da tabela de Caixas e comparando com Estoque';

         for reg_estq060 in estq060  (reg_basi205.codigo_deposito,
                                      p_nivel,
                                      p_grupo,
                                      p_subgrupo,
                                      p_item)
         loop

            /*
             VERIFICA SALDO DOS ROLOS SEM CONSIDERAR LOTE COMPARA COM A ESTQ_040 SEM LOTE
             PARA TRATAR ACERTOS ERRADOS COM LOTES NA ESTQ_300_ESTQ_310 */

            v_sequencia_ficha := null;
            v_qtde_kardex_ant := 0;
            v_saldo_k := 0;
            v_saldo_k := 0;

            BEGIN
            select nvl(f.saldo_fisico,0.000), nvl(f.preco_medio_unitario,0)
            into v_qtde_kardex_ant,           v_preco_medio_unitario
            from estq_301 f
            where f.codigo_deposito    = reg_estq060.deposito
              and f.nivel_estrutura    = reg_estq060.nivel
              and f.grupo_estrutura    = reg_estq060.grupo
              and f.subgrupo_estrutura = reg_estq060.subgrupo
              and f.item_estrutura     = reg_estq060.item
              and f.mes_ano_movimento  = (select max(i.mes_ano_movimento) from estq_301 i
                                          where i.codigo_deposito    = f.codigo_deposito
                                            and i.nivel_estrutura    = f.nivel_estrutura
                                            and i.grupo_estrutura    = f.grupo_estrutura
                                            and i.subgrupo_estrutura = f.subgrupo_estrutura
                                            and i.item_estrutura     = f.item_estrutura
                                            and i.mes_ano_movimento  < data_base);
             EXCEPTION
                WHEN OTHERS THEN
                v_qtde_kardex_ant := 0;
                v_preco_medio_unitario :=0;
            END;


            if v_qtde_kardex_ant is null
            then
               v_qtde_kardex_ant := 0.000;
            end if;

            select sum(decode(entrada_saida,'E',k.quantidade,(k.quantidade * -1)))
            into v_saldo_k
            from estq_300_estq_310 k
            where k.nivel_estrutura    = reg_estq060.nivel
              and k.grupo_estrutura    = reg_estq060.grupo
              and k.subgrupo_estrutura = reg_estq060.subgrupo
              and k.item_estrutura     = reg_estq060.item
              and k.codigo_deposito    = reg_estq060.deposito
              and k.data_movimento     >= data_base;


            if v_saldo_k is null
            then
              v_saldo_k := 0;
            end if;

            select nvl(sum(qtde_estoque_atu),0), nvl(sum(e.qtde_empenhada),0), nvl(sum(e.qtde_sugerida),0)
            into v_qtde040, v_qtde_empenhada, v_qtde_sugerida
            from estq_040 e
            where e.cditem_nivel99  = reg_estq060.nivel
            and   e.cditem_grupo    = reg_estq060.grupo
            and   e.cditem_subgrupo = reg_estq060.subgrupo
            and   e.cditem_item     = reg_estq060.item
            and   e.deposito        = reg_estq060.deposito;

            if v_qtde040 is null
            then
               v_qtde040 := 0;
            end if;

            select  nvl(sum(peso_liquido), 0) into v_qtderolos from estq_060 e
            where e.codigo_deposito  = reg_estq060.deposito
              and e.prodcai_nivel99  = reg_estq060.nivel
              and e.prodcai_grupo    = reg_estq060.grupo
              and e.prodcai_subgrupo = reg_estq060.subgrupo
              and e.prodcai_item     = reg_estq060.item
              and e.status_caixa     not in (4,6,8,9);

            if v_qtderolos is null
            then
               v_qtderolos  := 0.000;
            end if;

            ajuste_sub := null;
            if length(reg_estq060.subgrupo) = 1 then
               ajuste_sub := '  ';
            end if;

            if length(reg_estq060.subgrupo) = 2 then
               ajuste_sub := ' ';
            end if;


            if v_qtderolos <> v_qtde040 or v_qtderolos <> v_saldo_k + v_qtde_kardex_ant
            then
               atualiza_list_critica_estoque(v_ind_partida,
                                             v_id_execucao,
                                             reg_estq060.nivel,
                                             reg_estq060.grupo,
                                             reg_estq060.subgrupo,
                                             reg_estq060.item,
                                             reg_estq060.deposito,
                                             reg_estq060.lote_acomp,
                                             v_qtde040,
                                             v_qtderolos,
                                             v_saldo_k + v_qtde_kardex_ant,
                                             v_qtde_sugerida,
                                             v_qtde_empenhada,
                                             v_preco_medio_unitario,
                                             v_observacao, reg_basi205.tipo_volume);

            end if;
         end loop;
      end if;  ---  tipo_volume = 7

      -- --------------------------  FARDOS -------------------------------------------------

      if reg_basi205.tipo_volume = 9
      then

         /* Partindo da tabela de Estoques e comparando com FARDOS  */


          v_ind_partida := 'Partindo da tabela de Estoques e comparando com Fardos ';

         for reg_estq040 in estq040 (reg_basi205.codigo_deposito,
                                     p_nivel,
                                     p_grupo,
                                     p_subgrupo,
                                     p_item)
         loop

            v_sequencia_ficha := null;
            v_qtde_kardex_ant := 0;
            v_saldo_k := 0;
            v_saldo_k := 0;


            BEGIN
            select nvl(f.saldo_fisico,0.000), nvl(f.preco_medio_unitario,0)
            into v_qtde_kardex_ant,           v_preco_medio_unitario
            from estq_301 f
            where f.codigo_deposito    = reg_estq040.deposito
              and f.nivel_estrutura    = reg_estq040.nivel
              and f.grupo_estrutura    = reg_estq040.grupo
              and f.subgrupo_estrutura = reg_estq040.subgrupo
              and f.item_estrutura     = reg_estq040.item
              and f.mes_ano_movimento  = (select max(i.mes_ano_movimento) from estq_301 i
                                          where i.codigo_deposito    = f.codigo_deposito
                                            and i.nivel_estrutura    = f.nivel_estrutura
                                            and i.grupo_estrutura    = f.grupo_estrutura
                                            and i.subgrupo_estrutura = f.subgrupo_estrutura
                                            and i.item_estrutura     = f.item_estrutura
                                            and i.mes_ano_movimento  < data_base);
             EXCEPTION
                WHEN OTHERS THEN
                v_qtde_kardex_ant := 0;
                v_preco_medio_unitario :=0;
            END;


            if v_qtde_kardex_ant is null
            then
               v_qtde_kardex_ant := 0.000;
            end if;


            select sum(decode(entrada_saida,'E',k.quantidade,(k.quantidade * -1)))
            into v_saldo_k
            from estq_300_estq_310 k
            where k.nivel_estrutura    = reg_estq040.nivel
              and k.grupo_estrutura    = reg_estq040.grupo
              and k.subgrupo_estrutura = reg_estq040.subgrupo
              and k.item_estrutura     = reg_estq040.item
              and k.codigo_deposito    = reg_estq040.deposito
              and k.data_movimento     >= data_base;

            if v_saldo_k is null
            then
               v_saldo_k := 0;
            end if;

            select nvl(sum(decode(peso_real, 0, peso_medio, peso_real)), 0) into v_qtderolos from pcpf_060 f
            where f.deposito        = reg_estq040.deposito
              and f.fardo_nivel99   = reg_estq040.nivel
              and f.fardo_grupo     = reg_estq040.grupo
              and f.fardo_subgrupo  = reg_estq040.subgrupo
              and f.fardo_item      = reg_estq040.item
              and f.status_fardo   not in (2,5);

            if v_qtderolos is null
            then
               v_qtderolos  := 0.000;
            end if;

            select nvl(sum(qtde_estoque_atu),0), nvl(sum(e.qtde_empenhada),0), nvl(sum(e.qtde_sugerida),0)
            into v_qtde040, v_qtde_empenhada, v_qtde_sugerida
            from estq_040 e
            where e.cditem_nivel99  = reg_estq040.nivel
            and   e.cditem_grupo    = reg_estq040.grupo
            and   e.cditem_subgrupo = reg_estq040.subgrupo
            and   e.cditem_item     = reg_estq040.item
            and   e.deposito        = reg_estq040.deposito;

            if v_qtde040 is null
            then
               v_qtde040 := 0;
            end if;

            if v_qtde_empenhada is null
            then
               v_qtde_empenhada := 0;
            end if;

            if v_qtde_sugerida is null
            then
               v_qtde_sugerida := 0;
            end if;

            ajuste_sub := null;

            if length(reg_estq040.subgrupo) = 1 then
               ajuste_sub := '  ';
            end if;

            if length(reg_estq040.subgrupo) = 2 then
               ajuste_sub := ' ';
            end if;


            if v_qtde040 <> v_qtderolos or v_qtde040 <> v_saldo_k + v_qtde_kardex_ant
            then
               atualiza_list_critica_estoque(v_ind_partida,
                                             v_id_execucao,
                                             reg_estq040.nivel,
                                             reg_estq040.grupo,
                                             reg_estq040.subgrupo,
                                             reg_estq040.item,
                                             reg_estq040.deposito,
                                             reg_estq040.lote_acomp,
                                             v_qtde040,
                                             v_qtderolos,
                                             v_saldo_k + v_qtde_kardex_ant,
                                             v_qtde_sugerida,
                                             v_qtde_empenhada,
                                             v_preco_medio_unitario,
                                             v_observacao, reg_basi205.tipo_volume);

            end if;
         end loop;

         v_ind_partida := 'Partindo da tabela de Fardos e comparando com Estoque';

         for reg_pcpf060 in pcpf060  (reg_basi205.codigo_deposito,
                                      p_nivel,
                                      p_grupo,
                                      p_subgrupo,
                                      p_item)
         loop

            /*
             VERIFICA SALDO DOS ROLOS SEM CONSIDERAR LOTE COMPARA COM A ESTQ_040 SEM LOTE
             PARA TRATAR ACERTOS ERRADOS COM LOTES NA ESTQ_300_ESTQ_310 */

            v_sequencia_ficha := null;
            v_qtde_kardex_ant := 0;
            v_saldo_k := 0;
            v_saldo_k := 0;

            BEGIN
            select nvl(f.saldo_fisico,0.000), nvl(f.preco_medio_unitario,0)
            into v_qtde_kardex_ant,           v_preco_medio_unitario
            from estq_301 f
            where f.codigo_deposito    = reg_pcpf060.deposito
              and f.nivel_estrutura    = reg_pcpf060.nivel
              and f.grupo_estrutura    = reg_pcpf060.grupo
              and f.subgrupo_estrutura = reg_pcpf060.subgrupo
              and f.item_estrutura     = reg_pcpf060.item
              and f.mes_ano_movimento  = (select max(i.mes_ano_movimento) from estq_301 i
                                          where i.codigo_deposito    = f.codigo_deposito
                                            and i.nivel_estrutura    = f.nivel_estrutura
                                            and i.grupo_estrutura    = f.grupo_estrutura
                                            and i.subgrupo_estrutura = f.subgrupo_estrutura
                                            and i.item_estrutura     = f.item_estrutura
                                            and i.mes_ano_movimento  < data_base);
             EXCEPTION
                WHEN OTHERS THEN
                v_qtde_kardex_ant := 0;
                v_preco_medio_unitario :=0;
            END;


            if v_qtde_kardex_ant is null
            then
               v_qtde_kardex_ant := 0.000;
            end if;

            select sum(decode(entrada_saida,'E',k.quantidade,(k.quantidade * -1)))
            into v_saldo_k
            from estq_300_estq_310 k
            where k.nivel_estrutura    = reg_pcpf060.nivel
              and k.grupo_estrutura    = reg_pcpf060.grupo
              and k.subgrupo_estrutura = reg_pcpf060.subgrupo
              and k.item_estrutura     = reg_pcpf060.item
              and k.codigo_deposito    = reg_pcpf060.deposito
              and k.data_movimento     >= data_base;


            if v_saldo_k is null
            then
              v_saldo_k := 0;
            end if;

            select nvl(sum(qtde_estoque_atu),0), nvl(sum(e.qtde_empenhada),0), nvl(sum(e.qtde_sugerida),0)
            into v_qtde040, v_qtde_empenhada, v_qtde_sugerida
            from estq_040 e
            where e.cditem_nivel99  = reg_pcpf060.nivel
            and   e.cditem_grupo    = reg_pcpf060.grupo
            and   e.cditem_subgrupo = reg_pcpf060.subgrupo
            and   e.cditem_item     = reg_pcpf060.item
            and   e.deposito        = reg_pcpf060.deposito;

            if v_qtde040 is null
            then
               v_qtde040 := 0;
            end if;

            select nvl(sum(decode(peso_real, 0, peso_medio, peso_real)), 0) into v_qtderolos from pcpf_060 f
            where f.deposito        = reg_pcpf060.deposito
              and f.fardo_nivel99   = reg_pcpf060.nivel
              and f.fardo_grupo     = reg_pcpf060.grupo
              and f.fardo_subgrupo  = reg_pcpf060.subgrupo
              and f.fardo_item      = reg_pcpf060.item
              and f.status_fardo   not in (2,5);

            if v_qtderolos is null
            then
               v_qtderolos  := 0.000;
            end if;

            ajuste_sub := null;
            if length(reg_pcpf060.subgrupo) = 1 then
               ajuste_sub := '  ';
            end if;

            if length(reg_pcpf060.subgrupo) = 2 then
               ajuste_sub := ' ';
            end if;


            if v_qtderolos <> v_qtde040 or v_qtderolos <> v_saldo_k + v_qtde_kardex_ant
      then
               atualiza_list_critica_estoque(v_ind_partida,
                                             v_id_execucao,
                                             reg_pcpf060.nivel,
                                             reg_pcpf060.grupo,
                                             reg_pcpf060.subgrupo,
                                             reg_pcpf060.item,
                                             reg_pcpf060.deposito,
                                             reg_pcpf060.lote_acomp,
                                             v_qtde040,
                                             v_qtderolos,
                                             v_saldo_k + v_qtde_kardex_ant,
                                             v_qtde_sugerida,
                                             v_qtde_empenhada,
                                             v_preco_medio_unitario,
                                             v_observacao, reg_basi205.tipo_volume);

            end if;
         end loop;
      end if;  --tipo_volume = 9
   end loop;
END;
