create table oper_180 
(
empresa   number(3) not null,
icms      number(6,2) default 0.00,
pis       number(6,2) default 0.00,
cofins    number(6,2) default 0.00,
comissao  number(6,2) default 0.00,
lucro     number(6,2) default 0.00,
acrescimo number(6,2) default 0.00
);

alter table oper_180 add constraint PK_OPER_180 primary key (empresa);