CREATE OR REPLACE TRIGGER "INTER_TR_OPER_276" 
before insert on oper_276
for each row

declare
  v_id_registro number;
  
  ws_usuario_rede           varchar2(20);
  ws_maquina_rede           varchar2(40);
  ws_aplicativo             varchar2(20);
  ws_sid                    number(9);
  ws_empresa                number(3);
  ws_usuario_systextil      varchar2(250);
  ws_locale_usuario         varchar2(5);
   
begin

	-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
						   
   select seq_oper_276.nextval into v_id_registro from dual;
   :new.id_registro     	:= v_id_registro;
   :new.data_hora_registro 	:= sysdate;
   :new.usuario_rede 		:= ws_usuario_rede;
   :new.usuario_systextil 	:= ws_usuario_systextil;
   :new.maquina_rede 		:= ws_maquina_rede;
end;

/
exec inter_pr_recompile;
