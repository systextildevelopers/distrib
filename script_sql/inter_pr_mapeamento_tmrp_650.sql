
  CREATE OR REPLACE PROCEDURE "INTER_PR_MAPEAMENTO_TMRP_650" 
  (p_data_inicial in date,
   p_data_final   in date,
   p_codigo_recurso in varchar2)
is
   v_data_analise               date := p_data_inicial;
   v_cor_sequencia_livre        varchar2(6) := '000000';
   v_tonalidade_sequencia_livre varchar2(2) := '';
   v_sequencia                  number(3) := 0;
   v_data_termino               date;
   v_hora_termino               date;
   v_hora_analise               date;
   v_tipo_alocacao              number(1) := 2;
   v_tipo_recurso               number(1) := 1;
   v_dia_hora_inicio            date;
   v_dia_hora_termino           date;
   v_dia_tempo_disponivel       number;
   v_ordem_data_inicio          date;
   v_ordem_hora_inicio          date;
   v_ordem_data_termino         date;
   v_ordem_hora_termino         date;
   v_id_ultima_ordem            number;
   v_ordem_tempo_producao       number;
   v_ordem_cor                  varchar2(6) := '000000';
   v_id_lido                    number;
   v_tonalidade                 varchar2(2);
   v_tempo_producao             number;
   v_n_registros                number;
   v_minutos_dia_seguinte       number;
begin
   begin
      delete tmrp_651
      where trunc(tmrp_651.data_recurso) between trunc(p_data_inicial) and trunc(p_data_final)
        and tmrp_651.tipo_alocacao       = 2
        and tmrp_651.tipo_recurso        = 1
        and (tmrp_651.codigo_recurso     = p_codigo_recurso or p_codigo_recurso = 'todos');
      exception when others
      then raise_application_error(-20411,'Erro');
   end;

   for reg_recursos in (select b.codigo_recurso
                        from tmrp_660 b
                        where b.tipo_alocacao  = 2
                          and b.tipo_recurso   = 1
                          and b.data_recurso between p_data_inicial and p_data_final
                          and (b.codigo_recurso = p_codigo_recurso or p_codigo_recurso = 'todos')
                        group by b.codigo_recurso)
   loop
      v_data_analise := p_data_inicial;

      while trunc(v_data_analise) <= trunc(p_data_final)
      loop
         v_sequencia := 0;

         begin
            select b.hora_inicio,     b.hora_termino,     b.tempo_disponivel
            into   v_dia_hora_inicio, v_dia_hora_termino, v_dia_tempo_disponivel
            from tmrp_660 b
            where b.tipo_alocacao  = 2
              and b.tipo_recurso   = 1
              and b.codigo_recurso = reg_recursos.codigo_recurso
              and b.data_recurso   = v_data_analise
              and (b.codigo_recurso = p_codigo_recurso or p_codigo_recurso = 'todos');
            exception when others
            then
               v_dia_hora_inicio      := null;
               v_dia_hora_termino     := null;
               v_dia_tempo_disponivel := 0;
         end;

         if v_dia_hora_inicio is not null
         then
            v_hora_analise := to_date(to_char(v_dia_hora_inicio,'dd-mm-yyyy') || ' 00:01','dd-mm-yyyy hh24:mi');

            v_ordem_data_inicio  := null;
            v_ordem_hora_inicio  := null;
            v_ordem_data_termino := null;
            v_ordem_hora_termino := null;

            begin
               select tmrp_650.numero_id
               into   v_id_ultima_ordem
               from tmrp_650,(select (tmrp_650.data_inicio) di,(tmrp_650.hora_inicio) hi,(tmrp_650.data_termino) dt,(tmrp_650.hora_termino) ht
                              from tmrp_650
                              where  tmrp_650.tipo_alocacao  = 2
                                and  tmrp_650.tipo_recurso   = 1
                                and  tmrp_650.codigo_recurso = reg_recursos.codigo_recurso
                                and (trunc(tmrp_650.data_inicio) = trunc(v_data_analise) or trunc(tmrp_650.data_termino) = trunc(v_data_analise))
                              order by (tmrp_650.data_inicio) desc,(tmrp_650.hora_inicio) desc,(tmrp_650.data_termino) desc,(tmrp_650.hora_termino) desc) a
               where  tmrp_650.tipo_alocacao  = 2
                 and  tmrp_650.tipo_recurso   = 1
                 and  tmrp_650.codigo_recurso = reg_recursos.codigo_recurso
                 and (trunc(tmrp_650.data_inicio)= trunc(v_data_analise) or trunc(tmrp_650.data_termino) = trunc(v_data_analise))
                 and  tmrp_650.data_inicio    =  a.di
                 and  tmrp_650.hora_inicio    =  a.hi
                 and  tmrp_650.data_termino   =  a.dt
                 and  tmrp_650.hora_termino   =  a.ht
                 and  rownum                  = 1;
               exception when others
               then
                  v_id_ultima_ordem := 0;
            end;

            for reg_ordens in (select c.data_inicio,     c.hora_inicio,     c.data_termino,     c.hora_termino,     c.tempo_producao,     c.item_produto, c.numero_id
                               from tmrp_650 c
                               where  c.tipo_alocacao  = 2
                                 and  c.tipo_recurso   = 1
                                 and  c.codigo_recurso = reg_recursos.codigo_recurso
                                 and (trunc(c.data_inicio) = trunc(v_data_analise) or trunc(c.data_termino) = trunc(v_data_analise))
                               group by c.data_inicio,c.hora_inicio,c.data_termino,c.hora_termino, c.tempo_producao, c.item_produto, c.numero_id
                               order by c.data_inicio,c.hora_inicio,c.data_termino,c.hora_termino)
            loop
               v_ordem_data_inicio    := reg_ordens.data_inicio;
               v_ordem_hora_inicio    := reg_ordens.hora_inicio;
               v_ordem_data_termino   := reg_ordens.data_termino;
               v_ordem_hora_termino   := reg_ordens.hora_termino;
               v_ordem_tempo_producao := reg_ordens.tempo_producao;
               v_ordem_cor            := reg_ordens.item_produto;
               v_id_lido              := reg_ordens.numero_id;

               begin
                  select basi_100.tonalidade
                  into   v_tonalidade
                  from basi_100
                  where basi_100.cor_sortimento = v_ordem_cor;
                  exception when others
                  then v_tonalidade := '';
               end;

               if trunc(v_ordem_data_inicio) < trunc(v_data_analise)
               then
                  v_tempo_producao := abs(inter_fn_calcula_tempo(to_date(to_char(sysdate,'dd-mm-yyyy') || ' 00:00','dd-mm-yyyy hh24:mi'),to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_termino,'hh24:mi'),'dd-mm-yyyy hh24:mi')));

                  v_sequencia := v_sequencia + 1;

                  begin
                     insert into tmrp_651
                       (tipo_alocacao,               tipo_recurso,
                        codigo_recurso,              data_recurso,
                        sequencia,                   hora_inicio,
                        hora_termino,
                        minutos,
                        situacao,                    cor,
                        tonalidade)
                     values
                       (v_tipo_alocacao,             v_tipo_recurso,
                        reg_recursos.codigo_recurso, trunc(v_data_analise),
                        v_sequencia,                 to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' 00:00','dd-mm-yyyy hh24:mi'),
                        to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_termino,'hh24:mi'),'dd-mm-yyyy hh24:mi'),
                        v_tempo_producao,
                        1,                           v_ordem_cor,
                        v_tonalidade);
                  end;

                  v_hora_analise := to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_termino,'hh24:mi'),'dd-mm-yyyy hh24:mi');
               else
                  if trunc(v_ordem_data_termino) > trunc(v_data_analise)
                  then
                     v_tempo_producao := abs(inter_fn_calcula_tempo(to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi'), to_date(to_char(sysdate,'dd-mm-yyyy') || ' 23:59','dd-mm-yyyy hh24:mi')));

                     v_sequencia := v_sequencia + 1;

                     begin
                        insert into tmrp_651
                          (tipo_alocacao,               tipo_recurso,
                           codigo_recurso,              data_recurso,
                           sequencia,                   hora_inicio,
                           hora_termino,                minutos,
                           situacao,                    cor,
                           tonalidade)
                        values
                          (v_tipo_alocacao,               v_tipo_recurso,
                           reg_recursos.codigo_recurso,   trunc(v_data_analise),
                           v_sequencia,                   to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi'),
                           to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' 23:59','dd-mm-yyyy hh24:mi'),
                           v_tempo_producao,
                           1,                             v_ordem_cor,
                           v_tonalidade);
                     end;

                     v_hora_analise := to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' 23:59','dd-mm-yyyy hh24:mi');
                  else
                     if to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi') = to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_hora_analise,'hh24:mi'),'dd-mm-yyyy hh24:mi')
                     then
                        v_tempo_producao := abs(inter_fn_calcula_tempo(to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi'), to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_termino,'hh24:mi'),'dd-mm-yyyy hh24:mi')));

                        v_sequencia := v_sequencia + 1;

                        begin
                           insert into tmrp_651
                             (tipo_alocacao,               tipo_recurso,
                              codigo_recurso,              data_recurso,
                              sequencia,                   hora_inicio,
                              hora_termino,                minutos,
                              situacao,                    cor,
                              tonalidade)
                           values
                             (v_tipo_alocacao,             v_tipo_recurso,
                              reg_recursos.codigo_recurso, trunc(v_data_analise),
                              v_sequencia,                 to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi'),
                              to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_termino,'hh24:mi'),'dd-mm-yyyy hh24:mi'),
                              v_tempo_producao,
                              1,                           v_ordem_cor,
                              v_tonalidade);
                        end;

                        v_hora_analise := to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_termino,'hh24:mi'),'dd-mm-yyyy hh24:mi');
                     else
                        if to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi') > to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_hora_analise,'hh24:mi'),'dd-mm-yyyy hh24:mi')
                        then
                           v_tempo_producao := abs(inter_fn_calcula_tempo(to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_hora_analise,'hh24:mi'),'dd-mm-yyyy hh24:mi'), to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi')));

                           v_sequencia := v_sequencia + 1;

                           if v_data_analise = p_data_inicial and v_sequencia = 1
                           then
                              begin
                                 select tmrp_650.item_produto
                                 into   v_cor_sequencia_livre
                                 from tmrp_650,(select max(tmrp_650.hora_termino) hora_termino, b.dt data_termino
                                                from tmrp_650,(select max(tmrp_650.data_termino) dt
                                                               from tmrp_650
                                                               where  tmrp_650.tipo_alocacao  = 2
                                                                 and  tmrp_650.tipo_recurso   = 1
                                                                 and  tmrp_650.codigo_recurso = reg_recursos.codigo_recurso
                                                                 and  trunc(tmrp_650.data_termino) < trunc(v_data_analise)) b
                                                where  tmrp_650.tipo_alocacao  = 2
                                                  and  tmrp_650.tipo_recurso   = 1
                                                  and  tmrp_650.codigo_recurso = reg_recursos.codigo_recurso
                                                  and  trunc(tmrp_650.data_termino) = trunc(b.dt)
                                                group by b.dt ) a
                                 where tmrp_650.tipo_alocacao  = 2
                                   and tmrp_650.tipo_recurso   = 1
                                   and tmrp_650.codigo_recurso = reg_recursos.codigo_recurso
                                   and trunc(tmrp_650.data_termino) = trunc(a.data_termino)
                                   and trunc(tmrp_650.hora_termino) = trunc(a.hora_termino);
                                 exception when others
                                 then
                                    v_cor_sequencia_livre:= '000000';
                              end;
                           end if;

                           if v_cor_sequencia_livre <> '000000'
                           then
                              begin
                                 select basi_100.tonalidade
                                 into   v_tonalidade_sequencia_livre
                                 from basi_100
                                 where basi_100.cor_sortimento = v_cor_sequencia_livre;
                                 exception when others
                                 then
                                    v_tonalidade_sequencia_livre := '0';
                              end;
                           else
                              v_tonalidade_sequencia_livre := '0';
                           end if;

                           begin
                              insert into tmrp_651
                                (tipo_alocacao,               tipo_recurso,
                                 codigo_recurso,              data_recurso,
                                 sequencia,                   hora_inicio,
                                 hora_termino,                minutos,
                                 situacao,                    cor,
                                 tonalidade)
                              values
                                (v_tipo_alocacao,             v_tipo_recurso,
                                 reg_recursos.codigo_recurso, trunc(v_data_analise),
                                 v_sequencia,                 to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' ' || to_char(v_hora_analise,'hh24:mi'),'dd-mm-yyyy hh24:mi'),
                                 to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi'),
                                 v_tempo_producao,
                                 0,                           v_cor_sequencia_livre,
                                 v_tonalidade_sequencia_livre);
                           end;

                           v_tempo_producao := abs(inter_fn_calcula_tempo(to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi'), to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_termino,'hh24:mi'),'dd-mm-yyyy hh24:mi')));

                           v_sequencia := v_sequencia + 1;

                           begin
                              insert into tmrp_651
                                (tipo_alocacao,               tipo_recurso,
                                 codigo_recurso,              data_recurso,
                                 sequencia,                   hora_inicio,
                                 hora_termino,                minutos,
                                 situacao,                    cor,
                                 tonalidade)
                              values
                                (v_tipo_alocacao,             v_tipo_recurso,
                                 reg_recursos.codigo_recurso, trunc(v_data_analise),
                                 v_sequencia,                 to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_inicio,'hh24:mi'),'dd-mm-yyyy hh24:mi'),
                                 to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_termino,'hh24:mi'),'dd-mm-yyyy hh24:mi'),
                                 v_tempo_producao,
                                 1,                           v_ordem_cor,
                                 v_tonalidade);
                           end;

                           v_hora_analise := to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_termino,'hh24:mi'),'dd-mm-yyyy hh24:mi');
                        end if;
                     end if;
                  end if;
               end if;

               if  trunc(v_data_analise) = trunc(v_ordem_data_termino)
               and abs(inter_fn_calcula_tempo(to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_termino,'hh24:mi'),'dd-mm-yyyy hh24:mi'), to_date(to_char(sysdate,'dd-mm-yyyy') || ' 23:59','dd-mm-yyyy hh24:mi'))) > 0
               and v_id_ultima_ordem = v_id_lido
               then
                  v_sequencia := v_sequencia + 1;

                  v_tempo_producao := abs(inter_fn_calcula_tempo(to_date(to_char(sysdate,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_termino,'hh24:mi'),'dd-mm-yyyy hh24:mi'), to_date(to_char(sysdate,'dd-mm-yyyy') || ' 23:59','dd-mm-yyyy hh24:mi')));

                  v_minutos_dia_seguinte := 0;

                  begin
                     select tmrp_651.minutos
                     into v_minutos_dia_seguinte
                     from tmrp_651
                     where tmrp_651.tipo_alocacao       = v_tipo_alocacao
                       and tmrp_651.tipo_recurso        = v_tipo_recurso
                       and tmrp_651.codigo_recurso      = reg_recursos.codigo_recurso
                       and trunc(tmrp_651.data_recurso) = trunc(v_data_analise) + 1
                       and tmrp_651.sequencia           = 1
                       and tmrp_651.situacao            = 0;
                     exception when others
                     then v_minutos_dia_seguinte := 0;
                  end;

                  v_tempo_producao := v_tempo_producao + v_minutos_dia_seguinte;

                  begin
                     insert into tmrp_651
                      (tipo_alocacao,               tipo_recurso,
                       codigo_recurso,              data_recurso,
                       sequencia,                   hora_inicio,
                       hora_termino,                minutos,
                       situacao,                    cor,
                       tonalidade)
                     values
                       (v_tipo_alocacao,             v_tipo_recurso,
                        reg_recursos.codigo_recurso, trunc(v_data_analise),
                        v_sequencia,                 to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' ' || to_char(v_ordem_hora_termino,'hh24:mi'),'dd-mm-yyyy hh24:mi'),
                        to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' 23:59','dd-mm-yyyy hh24:mi'),
                        v_tempo_producao,
                        0,                           v_ordem_cor,
                        v_tonalidade);
                  end;
               end if;

               v_cor_sequencia_livre        := v_ordem_cor;
               v_tonalidade_sequencia_livre := v_tonalidade;
            end loop;

            begin
               select count(1)
               into v_n_registros
               from tmrp_650 c
               where  c.tipo_alocacao  = 2
                 and  c.tipo_recurso   = 1
                 and  c.codigo_recurso = reg_recursos.codigo_recurso
                 and (trunc(c.data_inicio)    = trunc(v_data_analise) or trunc(c.data_termino)   = trunc(v_data_analise));
               exception when others
               then
                  v_n_registros := 0;
            end;

            if v_n_registros = 0
            then
               begin
                  select item_produto,          tonalidade
                  into   v_cor_sequencia_livre, v_tonalidade_sequencia_livre
                  from (select tmrp_650.item_produto, basi_100.tonalidade
                        from tmrp_650, basi_100
                        where tmrp_650.item_produto   = basi_100.cor_sortimento
                          and basi_100.cor_sortimento = 1
                          and tmrp_650.tipo_alocacao  = 2
                          and tmrp_650.tipo_recurso   = 1
                          and tmrp_650.codigo_recurso = reg_recursos.codigo_recurso
                          and trunc(tmrp_650.data_termino) < trunc(v_data_analise)
                        order by tmrp_650.data_termino desc,tmrp_650.hora_termino desc)
                  where rownum                  < 2;
                  exception when others
                  then
                     v_cor_sequencia_livre := '000000';
                     v_tonalidade_sequencia_livre := '0';
               end;

               v_sequencia := 1;

               begin
                  insert into tmrp_651
                    (tipo_alocacao,               tipo_recurso,
                     codigo_recurso,              data_recurso,
                     sequencia,                   hora_inicio,
                     hora_termino,                minutos,
                     situacao,                    cor,
                     tonalidade)
                  values
                    (v_tipo_alocacao,             v_tipo_recurso,
                     reg_recursos.codigo_recurso, trunc(v_data_analise),
                     v_sequencia,                 to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' 00:01','dd-mm-yyyy hh24:mi'),
                     to_date(to_char(v_data_analise,'dd-mm-yyyy') || ' 23:59','dd-mm-yyyy hh24:mi'),
                     v_dia_tempo_disponivel,
                     0,                           v_cor_sequencia_livre,
                     v_tonalidade_sequencia_livre);
               end;
            end if;
         end if;

         v_data_analise := v_data_analise + 1;
      end loop;
   end loop;
end;
 

/

exec inter_pr_recompile;

