  CREATE OR REPLACE TRIGGER "INTER_TR_MQOP_050_HIST" 
   before insert
      or delete
      or update of codigo_operacao,
                   minutos,
                   codigo_estagio,
                   minutos_homem,
                   situacao

on mqop_050
for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   teve_alter                number(1);
   sequencia_hist            number(9);
   long_aux                  long;
   linha                     varchar(100);
   situacao_old              varchar2(15);
   situacao_new              varchar2(15);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if ws_usuario_systextil is null or ws_usuario_systextil = '' or ws_usuario_systextil = ' '
   then ws_usuario_systextil := 'NAO DEFINIDO';
   end if;

   if inserting
   then
      begin
         select nvl(max(basi_095.sequencia),0)
         into sequencia_hist
         from basi_095
         where basi_095.tipo_comentario = 10 -- Historico Roteiro.
           and basi_095.nivel_estrutura = :new.nivel_estrutura
           and basi_095.grupo_estrutura = :new.grupo_estrutura
           and basi_095.data_historico  = trunc(sysdate);
      exception when others then
         sequencia_hist := 0;
      end;

      sequencia_hist := sequencia_hist + 1;

      linha := ' ';

      teve_alter := 1;

      long_aux       := inter_fn_buscar_tag('lb00883#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)     || :new.nivel_estrutura || '.' ||
                                                             :new.grupo_estrutura || '.' ||
                                                             :new.subgru_estrutura   || '.' ||
                                                             :new.item_estrutura  || '     ' ||
                        inter_fn_buscar_tag('lb29927#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil)           || to_char(:new.numero_alternati, '00') || '     ' ||
                        inter_fn_buscar_tag('lb08191#ROTEIRO:',ws_locale_usuario,ws_usuario_systextil)               || to_char(:new.numero_roteiro, '00') || chr(10) ||
                        inter_fn_buscar_tag('lb34318#TEVE AS SEGUINTES MODIFICACOES:',ws_locale_usuario,ws_usuario_systextil)  || chr(10) ||
                        inter_fn_buscar_tag('lb34324#FOI INCLUIDA A SEQ. DE OPERACAO: ',ws_locale_usuario,ws_usuario_systextil) || to_char(:new.seq_operacao,'00000');

      begin
         INSERT INTO basi_095
           (nivel_estrutura,        grupo_estrutura,
            subgru_estrutura,       data_historico,
            tipo_comentario,        sequencia,
            descricao,              codigo_usuario,
            hora_historico,         nome_programa)
         VALUES
           (:new.nivel_estrutura,   :new.grupo_estrutura,
            :new.subgru_estrutura,  trunc(sysdate),
            10,                     sequencia_hist,
            long_aux,               ws_usuario_systextil,
            sysdate,                ws_aplicativo);
         exception when OTHERS then
            raise_application_error (-20000,inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.',
                                         'BASI_095' , SQLERRM , '' , '' , '' , '' , '' , '' , '' , '' ,ws_locale_usuario,ws_usuario_systextil));
      end;
   end if;

   if updating
   then
      begin
         select nvl(max(basi_095.sequencia),0)
         into sequencia_hist
         from basi_095
         where basi_095.tipo_comentario = 10 -- Historico Roteiro.
           and basi_095.nivel_estrutura = :new.nivel_estrutura
           and basi_095.grupo_estrutura = :new.grupo_estrutura
           and basi_095.data_historico  = trunc(sysdate);
      exception when others then
         sequencia_hist := 0;
      end;

      sequencia_hist := sequencia_hist + 1;

      linha := ' ';

      long_aux       := inter_fn_buscar_tag('lb00883#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)     || :new.nivel_estrutura || '.' ||
                                                             :new.grupo_estrutura || '.' ||
                                                             :new.subgru_estrutura   || '.' ||
                                                             :new.item_estrutura  || '     ' ||
                        inter_fn_buscar_tag('lb29927#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || to_char(:new.numero_alternati, '00') || '     ' ||
                        inter_fn_buscar_tag('lb08191#ROTEIRO:',ws_locale_usuario,ws_usuario_systextil)     || to_char(:new.numero_roteiro, '00') || chr(10) ||
                        inter_fn_buscar_tag('lb34318#TEVE AS SEGUINTES MODIFICACOES:',ws_locale_usuario,ws_usuario_systextil) ;
      teve_alter     := 0;

      if :old.codigo_operacao <> :new.codigo_operacao
      then
         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
                     inter_fn_buscar_tag('lb10566#SEQUENCIA DE OPERACAO:',ws_locale_usuario,ws_usuario_systextil) || to_char(:new.seq_operacao,'00000') || chr(10) ||
                     inter_fn_buscar_tag('lb02558#COD. OPERACAO:',ws_locale_usuario,ws_usuario_systextil) || '     ' ||
                     inter_fn_buscar_tag('lb34854#ALTERADO DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:old.codigo_operacao, '00000') ||
                     ' -> '  ||
                     inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:new.codigo_operacao, '00000');

      end if;

      if :old.minutos <> :new.minutos
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter := 1;

         long_aux := long_aux || chr(10) || linha ||
                     inter_fn_buscar_tag('lb10566#SEQUENCIA DE OPERACAO:',ws_locale_usuario,ws_usuario_systextil) || to_char(:new.seq_operacao,'00000') || chr(10) ||
                     inter_fn_buscar_tag('lb34320#TEMPO MAQUINA:',ws_locale_usuario,ws_usuario_systextil) || '     ' ||
                     inter_fn_buscar_tag('lb34854#ALTERADO DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:old.minutos, '000.0000') ||
                     ' -> '  ||
                     inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' '  || to_char(:new.minutos, '000.0000');

      end if;

      if :old.codigo_estagio <> :new.codigo_estagio
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter := 1;

         long_aux := long_aux || chr(10) || linha ||
                     inter_fn_buscar_tag('lb10566#SEQUENCIA DE OPERACAO:',ws_locale_usuario,ws_usuario_systextil) || to_char(:new.seq_operacao,'00000') || chr(10) ||
                     inter_fn_buscar_tag('lb17425#CODIGO ESTAGIO:',ws_locale_usuario,ws_usuario_systextil) || '     ' ||
                     inter_fn_buscar_tag('lb34854#ALTERADO DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:old.codigo_estagio, '99900') ||
                     ' -> ' ||
                     inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:new.codigo_estagio, '99900');

      end if;

      if :old.minutos_homem <> :new.minutos_homem
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter := 1;

         long_aux := long_aux || chr(10) || linha ||
                     inter_fn_buscar_tag('lb10566#SEQUENCIA DE OPERACAO:',ws_locale_usuario,ws_usuario_systextil) || to_char(:new.seq_operacao,'00000') || chr(10) ||
                     inter_fn_buscar_tag('lb34322#TEMPO HOMEM:',ws_locale_usuario,ws_usuario_systextil) || '     ' ||
                     inter_fn_buscar_tag('lb34854#ALTERADO DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:old.minutos_homem, '0.0000') ||
                     ' -> ' ||
                     inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:new.minutos_homem, '0.0000');
      end if;

      if :old.situacao <> :new.situacao and (:new.situacao = 0 or :new.situacao = 1)
      then

         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter := 1;

         if :old.situacao = 0
         then situacao_old := inter_fn_buscar_tag('lb19858#DIGITADO',ws_locale_usuario,ws_usuario_systextil);
         else situacao_old := inter_fn_buscar_tag('lb20317#APROVADO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :new.situacao = 0
         then situacao_new := inter_fn_buscar_tag('lb19858#DIGITADO',ws_locale_usuario,ws_usuario_systextil);
         else situacao_new := inter_fn_buscar_tag('lb20317#APROVADO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         long_aux := long_aux || chr(10) || linha ||
                     inter_fn_buscar_tag('lb00303#SITUCAO:',ws_locale_usuario,ws_usuario_systextil) || '     ' ||
                     inter_fn_buscar_tag('lb34854#ALTERADO DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || situacao_old ||
                     ' -> ' ||
                     inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || situacao_new;

      end if;

      if :new.situacao = 2
      then :new.situacao := 0;
      end if;

      if :new.situacao = 3
      then :new.situacao := 1;
      end if;

      if teve_alter = 1
      then
         begin
            INSERT INTO basi_095
              (nivel_estrutura,        grupo_estrutura,
               subgru_estrutura,       data_historico,
               tipo_comentario,        sequencia,
               descricao,              codigo_usuario,
               hora_historico,         nome_programa)
            VALUES
              (:new.nivel_estrutura,   :new.grupo_estrutura,
               :new.subgru_estrutura,  trunc(sysdate),
               10,                     sequencia_hist,
               long_aux,               ws_usuario_systextil,
               sysdate,                ws_aplicativo);
            exception when OTHERS then
               raise_application_error (-20000,inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.',
                                            'BASI_095(1)' , SQLERRM , '' , '' , '' , '' , '' , '' , '' , '' ,ws_locale_usuario,ws_usuario_systextil));
         end;
      end if;
   end if;

   if deleting
   then
      begin
         select nvl(max(basi_095.sequencia),0)
         into sequencia_hist
         from basi_095
         where basi_095.tipo_comentario = 10 -- Historico Roteiro.
           and basi_095.nivel_estrutura = :old.nivel_estrutura
           and basi_095.grupo_estrutura = :old.grupo_estrutura
           and basi_095.data_historico  = trunc(sysdate);
      exception when others then
         sequencia_hist := 0;
      end;

      sequencia_hist := sequencia_hist + 1;
      teve_alter := 1;

      long_aux       := inter_fn_buscar_tag('lb00883#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)     || :old.nivel_estrutura || '.' ||
                                                             :old.grupo_estrutura || '.' ||
                                                             :old.subgru_estrutura   || '.' ||
                                                             :old.item_estrutura  || '     ' ||
                        inter_fn_buscar_tag('lb29927#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil)           || to_char(:old.numero_alternati, '00') || '     ' ||
                        inter_fn_buscar_tag('lb08191#ROTEIRO:',ws_locale_usuario,ws_usuario_systextil)               || to_char(:old.numero_roteiro, '00') || chr(10) ||
                        inter_fn_buscar_tag('lb34318#TEVE AS SEGUINTES MODIFICACOES:',ws_locale_usuario,ws_usuario_systextil)    || chr(10) ||
                        inter_fn_buscar_tag('lb34326#FOI ELIMINADA A SEQ. DE OPERACAO: ',ws_locale_usuario,ws_usuario_systextil) || to_char(:old.seq_operacao,'00000') || chr(10) ||
                        inter_fn_buscar_tag('lb02558#COD. OPERACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:old.codigo_operacao, '00000');

      if teve_alter = 1
      then
        begin
           INSERT INTO basi_095
              (nivel_estrutura,        grupo_estrutura,
               subgru_estrutura,       data_historico,
               tipo_comentario,        sequencia,
               descricao,              codigo_usuario,
               hora_historico,         nome_programa)
            VALUES
              (:old.nivel_estrutura,   :old.grupo_estrutura,
               :old.subgru_estrutura,  trunc(sysdate),
               10,                     sequencia_hist,
               long_aux,               ws_usuario_systextil,
               sysdate,                ws_aplicativo);
            exception when OTHERS then
               raise_application_error (-20000,inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.',
                                            'BASI_095(2)' , SQLERRM , '' , '' , '' , '' , '' , '' , '' , '' ,ws_locale_usuario,ws_usuario_systextil));
         end;
      end if;
   end if;
end inter_tr_mqop_050_hist;

-- ALTER TRIGGER "INTER_TR_MQOP_050_HIST" ENABLE
 

/

exec inter_pr_recompile;

