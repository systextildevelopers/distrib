CREATE OR REPLACE TRIGGER inter_tr_fatu_060_revenda
BEFORE INSERT OR DELETE OR UPDATE of cod_cancelamento  ON fatu_060
FOR EACH ROW
DECLARE
    v_cfop            VARCHAR2(20);
    v_tipo_movimento VARCHAR2(2);
    v_tem_nat_revenda         pedi_080.nat_oper_revenda%TYPE;
    
    v_nivel_estrutura  estq_307.nivel_estrutura%TYPE;
    v_GRUPO_estrutura  estq_307.GRUPO_estrutura%TYPE;
    v_SUBGRU_estrutura estq_307.SUBGRU_estrutura%TYPE;
    v_ITEM_estrutura   estq_307.ITEM_estrutura%TYPE;
    v_num_nfis           fatu_060.ch_it_nf_num_nfis%TYPE;
    v_ser_nfis           fatu_060.ch_it_nf_ser_nfis%TYPE;
    v_qtde_item          estq_307.quantidade%TYPE;
    v_usuario_cardex     fatu_060.usuario_cardex%TYPE;
    v_nome_programa      fatu_060.nome_programa%TYPE;
    v_ch_it_nf_cd_empr   fatu_060.ch_it_nf_cd_empr%TYPE;
    
    v_nat_oper    fatu_060.natopeno_nat_oper%TYPE;
    
    v_cod_est_agrupador fatu_060.cod_estagio_agrupador_insu%TYPE;
    v_cod_est_simul fatu_060.cod_estagio_simultaneo_insu%TYPE;
    
    v_transacao fatu_060.TRANSACAO%TYPE;
    v_nat_oper_est pedi_080.estado_natoper%type;
    v_tipo_transacao estq_005.tipo_transacao%type;
    
BEGIN
      IF INSERTING OR UPDATING THEN
         v_nivel_estrutura := :NEW.nivel_estrutura;

         v_GRUPO_estrutura  := :NEW.GRUPO_estrutura;
         v_SUBGRU_estrutura := :NEW.SUBGRU_estrutura;
         v_ITEM_estrutura   := :NEW.ITEM_estrutura;
         v_num_nfis           := :NEW.ch_it_nf_num_nfis;
         v_ser_nfis           := :NEW.ch_it_nf_ser_nfis;
         v_qtde_item          := :NEW.QTDE_ITEM_FATUR;
         v_usuario_cardex     := :NEW.usuario_cardex;
         v_nome_programa      := :NEW.nome_programa;
         v_ch_it_nf_cd_empr   := :NEW.ch_it_nf_cd_empr;
        
         v_nat_oper := :NEW.natopeno_nat_oper;
         v_nat_oper_est := :NEW.Natopeno_Est_Oper;
         
         v_transacao := :NEW.TRANSACAO; 
         
         v_cod_est_agrupador := :NEW.cod_estagio_agrupador_insu;
         v_cod_est_simul := :NEW.cod_estagio_simultaneo_insu;

      ELSE
         v_nivel_estrutura  := :OLD.nivel_estrutura;
         v_GRUPO_estrutura  := :OLD.GRUPO_estrutura;
         v_SUBGRU_estrutura := :OLD.SUBGRU_estrutura;
         v_ITEM_estrutura   := :OLD.ITEM_estrutura;
         v_num_nfis           := :OLD.ch_it_nf_num_nfis;
         v_ser_nfis           := :OLD.ch_it_nf_ser_nfis;
         v_qtde_item          := :OLD.QTDE_ITEM_FATUR;
         v_usuario_cardex     := :OLD.usuario_cardex;
         v_nome_programa      := :OLD.nome_programa;
         v_ch_it_nf_cd_empr   := :OLD.ch_it_nf_cd_empr;
         
         v_nat_oper := :OLD.natopeno_nat_oper;
         v_nat_oper_est := :OLD.Natopeno_Est_Oper;
         v_transacao := :OLD.TRANSACAO;
         
         v_cod_est_agrupador := :OLD.cod_estagio_agrupador_insu;
         v_cod_est_simul := :OLD.cod_estagio_simultaneo_insu;

      END IF;

        BEGIN
         SELECT count(*) INTO v_tem_nat_revenda FROM pedi_080
          WHERE nat_oper_revenda = v_nat_oper
          AND estado_natoper = v_nat_oper_est;
        END;
        
        BEGIN
           select estq_005.tipo_transacao INTO v_tipo_transacao from estq_005 where estq_005.codigo_transacao = v_transacao;
        EXCEPTION WHEN OTHERS THEN v_tipo_transacao := 'N';
        
        END;
        
        IF  inter_fn_cc_revenda(v_ch_it_nf_cd_empr)
            AND inter_fn_valida_tipo_transacao(v_transacao)
            AND inter_fn_valida_prod_virtual(v_cod_est_agrupador, v_cod_est_simul) 
            AND v_tem_nat_revenda > 0
            and (inter_fn_cons_saldo_revenda_s(v_ch_it_nf_cd_empr,v_nivel_estrutura,v_GRUPO_estrutura,v_SUBGRU_estrutura,v_ITEM_estrutura) > 0 or v_tipo_transacao = 'D')
        THEN
            IF INSERTING THEN
               v_tipo_movimento := 'S';

            ELSIF UPDATING and :OLD.cod_cancelamento = 0 and :NEW.cod_cancelamento > 0 THEN
               v_tipo_movimento := 'E';
            ELSIF DELETING THEN
               v_tipo_movimento := 'E';
            END IF;
            
            IF INSERTING or DELETING OR (UPDATING and :OLD.cod_cancelamento = 0 and :NEW.cod_cancelamento > 0) THEN
               INSERT INTO ESTQ_307 (
                    NIVEL_ESTRUTURA,
                    GRUPO_ESTRUTURA,
                    SUBGRU_ESTRUTURA,
                    ITEM_ESTRUTURA,
                    TIPO_MOVIMENTO,
                    DOCUMENTO,
                    SERIE,
                    ORIGEM_MOVIMENTO,
                    QUANTIDADE,
                    USUARIO_SYSTEXTIL,
                    DATA_HORA_MOV,
                    PROG_GEROU_MOV,
                    SEQ_INSERCAO,
                    QTDE_SALDO,
                    COD_EMPRESA,
                    DATA_INSERCAO
                ) VALUES (
                   v_nivel_estrutura,
                   v_GRUPO_estrutura,
                   v_SUBGRU_estrutura,
                   v_ITEM_estrutura,
                   v_tipo_movimento,
                   v_num_nfis,
                   v_ser_nfis,
                   v_cfop,
                   v_qtde_item,
                   v_usuario_cardex,
                   SYSDATE,
                   v_nome_programa,
                   ESTQ_307_SEQ_INSERCAO_SEQ.nextval,
                   0,
                   v_ch_it_nf_cd_empr,
                   SYSDATE
                );
             END IF;
        END IF;
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error (-20000, SQLERRM);
END;
/
