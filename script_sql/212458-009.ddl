create table basi_969
(
  cliente9         NUMBER(9) default 0 not null,
  cliente4         NUMBER(4) default 0 not null,
  cliente2         NUMBER(2) default 0 not null,
  cod_coligado     NUMBER(4) default 0
);

alter table basi_969
add constraint PK_BASI_969 primary key (CLIENTE9, CLIENTE4, CLIENTE2);
