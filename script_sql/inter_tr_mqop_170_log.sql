
  CREATE OR REPLACE TRIGGER "INTER_TR_MQOP_170_LOG" 
after insert or delete or update
   of grupo_maquina,   subgrupo_maquina, descricao,      largura,    nivel_estrutura,
      grupo_estrutura, subgru_estrutura, item_estrutura, observacao, atalho_anexos_ap,
      atalho_anexos_acab
on mqop_170
for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   long_aux                  varchar2(2000);
   produto                   varchar2(20);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      produto := :new.nivel_estrutura   || '.' ||
                 :new.grupo_estrutura   || '.' ||
                 :new.subgru_estrutura  || '.' ||
                 :new.item_estrutura;

           INSERT INTO hist_100
               ( tabela,                 operacao,
                 data_ocorr,             aplicacao,
                 usuario_rede,           maquina_rede,
                 str02,                  str03,
                 str04,                  long01
               )
            VALUES
               ( 'MQOP_170',             'I',
                 sysdate,                ws_aplicativo,
                 ws_usuario_rede,        ws_maquina_rede,
                 :new.codigo_aparelho,   '',
                 produto,
                 '                              '||
                 inter_fn_buscar_tag('lb34876#CADASTRO DE APARELHOS',ws_locale_usuario,ws_usuario_systextil) ||
                                           chr(10)                    ||
                                           chr(10)                    ||
                 inter_fn_buscar_tag('lb34877#CODIGO DO APARELHO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :new.codigo_aparelho       ||
                                           '-'                        ||
                                           :new.descricao             ||
                                           chr(10)                    ||
                 inter_fn_buscar_tag('lb11620#MAQUINA :',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :new.grupo_maquina         || '.' ||
                                           :new.subgrupo_maquina      ||
                                           chr(10)                    ||
                 inter_fn_buscar_tag('lb08363#LARGURA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :new.largura               ||
                                           chr(10)                    ||
                 inter_fn_buscar_tag('lb00883#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           produto                    ||
                                           chr(10)                    ||
                 inter_fn_buscar_tag('lb03958#OBSERVACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :new.observacao            ||
                                           chr(10)                    ||
                 inter_fn_buscar_tag('lb34878#ANEXOS DO APARELHO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :new.atalho_anexos_ap      ||
                                           chr(10)                    ||
                 inter_fn_buscar_tag('lb34879#ANEXOS APARELHO/MAQUINA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :new.atalho_anexos_acab
                 );
   end if;

   if updating and
      (:old.codigo_aparelho    <> :new.codigo_aparelho      or
       :old.descricao          <> :new.descricao            or
       :old.largura            <> :new.largura              or
       :old.grupo_maquina      <> :new.grupo_maquina        or
       :old.subgrupo_maquina   <> :new.subgrupo_maquina     or
       :old.nivel_estrutura    <> :new.nivel_estrutura      or
       :old.grupo_estrutura    <> :new.grupo_estrutura      or
       :old.subgru_estrutura   <> :new.subgru_estrutura     or
       :old.item_estrutura     <> :new.item_estrutura       or
       :old.observacao         <> :new.observacao           or
       :old.atalho_anexos_ap   <> :new.atalho_anexos_ap     or
       :old.atalho_anexos_acab <> :new.atalho_anexos_acab
      )
   then
      produto := :new.nivel_estrutura   || '.' ||
                 :new.grupo_estrutura   || '.' ||
                 :new.subgru_estrutura  || '.' ||
                 :new.item_estrutura;

      long_aux := long_aux ||
                  '                              '||
                  inter_fn_buscar_tag('lb34876#CADASTRO DE APARELHOS',ws_locale_usuario,ws_usuario_systextil) ||
                  chr(10)                                             ||
                  chr(10)                                             ||
                  inter_fn_buscar_tag('lb34877#CODIGO DO APARELHO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                  :old.codigo_aparelho || '-' || :old.descricao ||
                  chr(10)                                          ||
                  inter_fn_buscar_tag('lb11620#MAQUINA :',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                  :old.grupo_maquina    ||
                  '.'                   ||
                  :old.subgrupo_maquina ||
                  chr(10)                                          ||
                  inter_fn_buscar_tag('lb00883#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                  :old.nivel_estrutura   || '.' ||
                  :old.grupo_estrutura   || '.' ||
                  :old.subgru_estrutura  || '.' ||
                  :old.item_estrutura    ||
                  chr(10)                                          ||
                  chr(10)                                          ||
                  '                                        '||
                  inter_fn_buscar_tag('lb34849#ALTERACOES',ws_locale_usuario,ws_usuario_systextil) ||
                  chr(10)                                          ||
                  chr(10);

      if :old.codigo_aparelho <> :new.codigo_aparelho
      then
         long_aux := long_aux ||
                 inter_fn_buscar_tag('lb34877#CODIGO DO APARELHO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :old.codigo_aparelho || ' -> ' ||
                                           :new.codigo_aparelho ||
                                           chr(10);
      end if;

      if :old.descricao <> :new.descricao
      then
         long_aux := long_aux ||
                 inter_fn_buscar_tag('lb00644#DESCRICAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :old.descricao || ' -> ' ||
                                           :new.descricao ||
                                           chr(10);
      end if;

      if :old.largura <> :new.largura
      then
         long_aux := long_aux ||
                 inter_fn_buscar_tag('lb08363#LARGURA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :old.largura || ' -> ' ||
                                           :new.largura ||
                                           chr(10);
      end if;

      if :old.grupo_maquina    <> :new.grupo_maquina    or
         :old.subgrupo_maquina <> :new.subgrupo_maquina
      then
         long_aux := long_aux ||
                 inter_fn_buscar_tag('lb11620#MAQUINA :',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :old.grupo_maquina    || '.'
                                        || :old.subgrupo_maquina || ' -> ' ||
                                           :new.grupo_maquina    || '.'
                                        || :new.subgrupo_maquina ||
                                        chr(10);
      end if;

      if :old.nivel_estrutura   <>     :new.nivel_estrutura     or
         :old.grupo_estrutura     <>     :new.grupo_estrutura     or
         :old.subgru_estrutura  <>     :new.subgru_estrutura    or
         :old.item_estrutura      <>     :new.item_estrutura
      then
         long_aux := long_aux ||
                 inter_fn_buscar_tag('lb00883#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :old.nivel_estrutura   || '.' ||
                                           :old.grupo_estrutura   || '.' ||
                                           :old.subgru_estrutura  || '.' ||
                                           :old.item_estrutura    || ' -> ' ||
                                           :new.nivel_estrutura   || '.' ||
                                           :new.grupo_estrutura   || '.' ||
                                           :new.subgru_estrutura  || '.' ||
                                           :new.item_estrutura    ||
                                           chr(10);
      end if;

      if :old.observacao <> :new.observacao
      then
         long_aux := long_aux ||
                 inter_fn_buscar_tag('lb03958#OBSERVACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :old.observacao || ' -> ' ||
                                           :new.observacao ||
                                           chr(10);
      end if;

      if :old.atalho_anexos_ap <> :new.atalho_anexos_ap
      then
         long_aux := long_aux ||
                 inter_fn_buscar_tag('lb34878#ANEXOS DO APARELHO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                          :old.atalho_anexos_ap || ' -> ' ||
                                          :new.atalho_anexos_ap ||
                                          chr(10);
      end if;

      if :old.atalho_anexos_acab <> :new.atalho_anexos_acab
      then
         long_aux := long_aux ||
                 inter_fn_buscar_tag('lb34879#ANEXOS APARELHO/MAQUINA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                          :old.atalho_anexos_acab || ' -> ' ||
                                          :new.atalho_anexos_acab ||
                                          chr(10);
      end if;


      INSERT INTO hist_100
         ( tabela,                 operacao,
           data_ocorr,             aplicacao,
           usuario_rede,           maquina_rede,
           str02,                  str03,
           str04,                  long01
         )
      VALUES
         ( 'MQOP_170',             'A',
           sysdate,                ws_aplicativo,
           ws_usuario_rede,        ws_maquina_rede,
           :new.codigo_aparelho,   '',
           produto,                long_aux
         );
   end if;

   if deleting
   then
      produto := :old.nivel_estrutura   || '.' ||
                 :old.grupo_estrutura   || '.' ||
                 :old.subgru_estrutura  || '.' ||
                 :old.item_estrutura;

       INSERT INTO hist_100
          ( tabela,                 operacao,
            data_ocorr,             aplicacao,
            usuario_rede,           maquina_rede,
            str02,                  str03,
            str04,                  long01
          )
       VALUES
          ( 'MQOP_170',             'D',
            sysdate,                ws_aplicativo,
            ws_usuario_rede,        ws_maquina_rede,
            :old.codigo_aparelho,   '',
            produto,
            inter_fn_buscar_tag('lb34877#CODIGO DO APARELHO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                || :old.codigo_aparelho || ' - ' || :old.descricao
         );

   end if;
end inter_tr_mqop_170_log;

-- ALTER TRIGGER "INTER_TR_MQOP_170_LOG" ENABLE
 

/

exec inter_pr_recompile;

