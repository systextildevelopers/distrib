CREATE OR REPLACE TRIGGER "TRIGGER_PEDI_135_HIST" 
BEFORE UPDATE of pedido_venda, seq_situacao, codigo_situacao, data_situacao,
	flag_liberacao, data_liberacao, responsavel, observacao,
	usuario_bloqueio, executa_trigger
OR DELETE
OR INSERT
on pedi_135
for each row
declare
   ws_pedido                 pedi_135.pedido_venda%type;
   ws_seq_situacao_old       pedi_135.seq_situacao%type;
   ws_seq_situacao_atu       pedi_135.seq_situacao%type;
   ws_codigo_situacao        number(2);
   ws_data_situacao_old      date;
   ws_data_situacao_atu      date;
   ws_flag_liberacao_old     varchar2(1);
   ws_flag_liberacao_atu     varchar2(1);
   ws_data_liberacao_old     date;
   ws_data_liberacao_atu     date;
   ws_responsavel_old        varchar2(20);
   ws_responsavel_atu        varchar2(20);
   ws_usuario_bloqueio_old   varchar2(250);
   ws_usuario_bloqueio_atu   varchar2(250);
   ws_tipo_ocorr             varchar2(1);
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(250);
   ws_aplicacao              varchar2(20);
   v_executa_trigger         number(1);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if INSERTING then
         ws_pedido                 := :new.pedido_venda;
         ws_seq_situacao_old    	:= 0;
         ws_seq_situacao_atu    	:= :new.seq_situacao;
         ws_codigo_situacao     	:= :new.codigo_situacao;
         ws_data_situacao_old   	:= null;
         ws_data_situacao_atu   	:= :new.data_situacao;
         ws_flag_liberacao_old  	:= '';
         ws_flag_liberacao_atu  	:= :new.flag_liberacao;
         ws_data_liberacao_old  	:= null;
         ws_data_liberacao_atu  	:= :new.data_liberacao;
         ws_responsavel_old     	:= '';
         ws_responsavel_atu     	:= :new.responsavel;
         ws_usuario_bloqueio_old	:= '';
         ws_usuario_bloqueio_atu	:= :new.usuario_bloqueio;
         ws_tipo_ocorr          	:= 'I';
      elsif UPDATING then
            ws_pedido                    := :new.pedido_venda;
            ws_seq_situacao_old    	:= :old.seq_situacao;
            ws_seq_situacao_atu    	:= :new.seq_situacao;
            ws_codigo_situacao     	:= :new.codigo_situacao;
            ws_data_situacao_old   	:= :old.data_situacao;
            ws_data_situacao_atu   	:= :new.data_situacao;
            ws_flag_liberacao_old  	:= :old.flag_liberacao;
            ws_flag_liberacao_atu  	:= :new.flag_liberacao;
            ws_data_liberacao_old  	:= :old.data_liberacao;
            ws_data_liberacao_atu  	:= :new.data_liberacao;
            ws_responsavel_old     	:= :old.responsavel;
            ws_responsavel_atu     	:= :new.responsavel;
            ws_usuario_bloqueio_old	:= :old.usuario_bloqueio;
            ws_usuario_bloqueio_atu	:= :new.usuario_bloqueio;
            ws_tipo_ocorr          	:= 'A';
         elsif DELETING then
               ws_pedido                    := :old.pedido_venda;
               ws_seq_situacao_old    	:= :old.seq_situacao;
               ws_seq_situacao_atu    	:= 0;
               ws_codigo_situacao     	:= :old.codigo_situacao;
               ws_data_situacao_old   	:= :old.data_situacao;
               ws_data_situacao_atu   	:= null;
               ws_flag_liberacao_old  	:= :old.flag_liberacao;
               ws_flag_liberacao_atu  	:= '';
               ws_data_liberacao_old  	:= :old.data_liberacao;
               ws_data_liberacao_atu  	:= null;
               ws_responsavel_old     	:= :old.responsavel;
               ws_responsavel_atu     	:= '';
               ws_usuario_bloqueio_old	:= :old.usuario_bloqueio;
               ws_usuario_bloqueio_atu	:= '';
               ws_tipo_ocorr          	:= 'D';
      end if;

      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicacao,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

      INSERT INTO pedi_135_hist
        (pedido,                  seq_situacao_old,        seq_situacao_atu,
         codigo_situacao,         data_situacao_old,       data_situacao_atu,
         flag_liberacao_old,      flag_liberacao_atu,      data_liberacao_old,
         data_liberacao_atu,      responsavel_old,         responsavel_atu,
         usuario_bloqueio_old,    usuario_bloqueio_atu,    tipo_ocorr,
         data_ocorr,              usuario_rede,            maquina_rede,
         aplicacao)
      VALUES
        (ws_pedido,               ws_seq_situacao_old,     ws_seq_situacao_atu,
         ws_codigo_situacao,      ws_data_situacao_old,    ws_data_situacao_atu,
         ws_flag_liberacao_old,   ws_flag_liberacao_atu,   ws_data_liberacao_old,
         ws_data_liberacao_atu,   ws_responsavel_old,      ws_responsavel_atu,
         ws_usuario_bloqueio_old, ws_usuario_bloqueio_atu, ws_tipo_ocorr,
         sysdate,                 ws_usuario_rede,         ws_maquina_rede,
         ws_aplicacao);

   end if;

end trigger_pedi_135_hist;

-- ALTER TRIGGER "TRIGGER_PEDI_135_HIST" ENABLE
/
