create table pcpc_400(
ordem_producao number(9),
id number(4),
constraint pcpc_400_pk primary key (ordem_producao, id),
constraint fk_pcpc_400_pcpc_020 foreign key (ordem_producao) references pcpc_020 (ordem_producao),
referencia varchar2(5),  
nivel varchar2(1),
grupo varchar2(5),  
subgrupo varchar2(3),  
item varchar2(6),  
metros number(13,4),  
quantidade number(13,2),  
larg_padrao number(13,2),
larg_real number(13,2),  
perc_custo number(13,2),  
perc_real number(13,2),  
data_registro date,  
ocorrencia_id number(4),
constraint fk_pcpc_400_pcpc_405 foreign key (ocorrencia_id) references pcpc_405 (id),
acao_id number(4),
constraint fk_pcpc_400_pcpc_406 foreign key (acao_id) references pcpc_406 (id)
);

comment on column pcpc_400.id is 'Identificador do registro do aproveitamento de encaixe';
comment on column pcpc_400.ordem_producao is 'Ordem de produção para informar o aproveitamento de encaixe dos materiais';
comment on column pcpc_400.referencia is 'Referência do material para informar o aproveitamento de encaixe';
comment on column pcpc_400.nivel is 'Nível do material para informar o aproveitamento do encaixe';
comment on column pcpc_400.grupo is 'Grupo do material para informar o aproveitamento do encaixe';
comment on column pcpc_400.subgrupo is 'Subgrupo do material para informar o aproveitamento do encaixe';
comment on column pcpc_400.item is 'Item do material para informar o aproveitamento do encaixe';
comment on column pcpc_400.metros is 'Qtde de metros necessária para atender a OP';
comment on column pcpc_400.quantidade is 'Qtde de quilos necessária para atender a OP';
comment on column pcpc_400.larg_padrao is 'Largura padrão do material';
comment on column pcpc_400.larg_real is 'Largura real do material';
comment on column pcpc_400.perc_custo is 'Percentual do custo padrão para o aproveitamento do material';
comment on column pcpc_400.perc_real is 'Percentual do custo real para o aproveitamento do material';
comment on column pcpc_400.data_registro is 'Data da gravação do registro';
comment on column pcpc_400.ocorrencia_id is 'Código da ocorrência do encaixe do material';
comment on column pcpc_400.acao_id is 'Código da ação para correção da ocorrência do encaixe do material';
comment on table pcpc_400 is 'Tabela de registro de aproveitamento de encaixe';

/
