CREATE OR REPLACE PROCEDURE "LOJA_PR_EXP_SALDOS"
  ( p_empresa number
  , p_linha   number
  , p_artigo  number
  , p_colecao number
  , p_cestoque number
  , p_produto  varchar2
  )

IS
--------------------------------------------------------------
-- Rotina principal
--------------------------------------------------------------
BEGIN

FOR saldo IN ( select estq_040.cditem_nivel99
                    , estq_040.cditem_grupo
                    , estq_040.cditem_subgrupo
                    , estq_040.cditem_item
                    , lpad(basi_205.local_deposito,3,0) local_deposito
                    , sum(estq_040.qtde_estoque_atu) qtde_estoque_atu
               from estq_040, basi_205, basi_030
               where estq_040.deposito = basi_205.codigo_deposito
                 and estq_040.cditem_nivel99 = basi_030.nivel_estrutura
                 and estq_040.cditem_grupo   = basi_030.referencia
                 and estq_040.deposito in (120,100, 150,170, 200,225, 250,270, 300,320)
                 and estq_040.cditem_nivel99 = '2'
                 and (basi_205.local_deposito = p_empresa or p_empresa = 0)
                 and (basi_030.linha_produto  = p_linha   or p_linha   = 0)
                 and (basi_030.artigo         = p_artigo  or p_artigo  = 0)
                 and (basi_030.conta_estoque  = p_cestoque or p_cestoque = 0)
                 and (basi_030.colecao        = p_colecao  or p_colecao  = 0)
                 and (basi_030.referencia     = p_produto  or p_produto  = '00000')
               group by estq_040.cditem_nivel99
                    , estq_040.cditem_grupo
                    , estq_040.cditem_subgrupo
                    , estq_040.cditem_item
                    , lpad(basi_205.local_deposito,3,0)
                  
) LOOP
     begin
       saldo.cditem_nivel99 := 2;
       --  insert into totall.ti_sal
       --  (CODEXT,
       --   CODFIL,
       --   DESEST,
       --   QTDEST)
       --  values
       --  (saldo.cditem_nivel99 || saldo.cditem_grupo || saldo.cditem_subgrupo || saldo.cditem_item,
       --   saldo.local_deposito,
       --   'PADRAO',
       --   saldo.qtde_estoque_atu
       --  );

      end;

end loop;
   commit;

end loja_pr_exp_saldos;


/

exec inter_pr_recompile;
