
  CREATE OR REPLACE TRIGGER "INTER_TR_CREC_170_LOG" 
after insert or
update of codigo_usuario, flag_sugestao, empresa, cgc9_cli,
	cgc4_cli, cgc2_cli, banco, agencia,
	conta, num_cheque, codigo_sacado, valor_cheque,
	data_recebido, data_vencto, posdata, cod_cidade,
	proprio, prazo, flag_cheque, saldo_cheque,
	sequencia, banco_corr, conta_corr, data_substituicao,
	data_devolucao, recibo_emitido, representante, compensacao,
	controle1, controle2, controle3, tipificacao,
	controle_cheque, flag_compensacao, forma_pgto, solicitacao_loja,
	guia, flag_guia, flag_cpag, agencia_descricao,
	cod_alinea_reapre, cod_alinea_devolu, valor_pagar_titulo, sel_cheque,
	valor_pago_cpag
or delete or update
on CREC_170
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuￜrio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);   

 if inserting
 then
    begin

        insert into CREC_170_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           EMPRESA_OLD,   /*8*/
           EMPRESA_NEW,   /*9*/
           CGC9_CLI_OLD,   /*10*/
           CGC9_CLI_NEW,   /*11*/
           CGC4_CLI_OLD,   /*12*/
           CGC4_CLI_NEW,   /*13*/
           CGC2_CLI_OLD,   /*14*/
           CGC2_CLI_NEW,   /*15*/
           BANCO_OLD,   /*16*/
           BANCO_NEW,   /*17*/
           AGENCIA_OLD,   /*18*/
           AGENCIA_NEW,   /*19*/
           CONTA_OLD,   /*20*/
           CONTA_NEW,   /*21*/
           NUM_CHEQUE_OLD,   /*22*/
           NUM_CHEQUE_NEW,   /*23*/
           CODIGO_SACADO_OLD,   /*24*/
           CODIGO_SACADO_NEW,   /*25*/
           VALOR_CHEQUE_OLD,   /*26*/
           VALOR_CHEQUE_NEW,   /*27*/
           DATA_RECEBIDO_OLD,   /*28*/
           DATA_RECEBIDO_NEW,   /*29*/
           DATA_VENCTO_OLD,   /*30*/
           DATA_VENCTO_NEW,   /*31*/
           POSDATA_OLD,   /*32*/
           POSDATA_NEW,   /*33*/
           COD_CIDADE_OLD,   /*34*/
           COD_CIDADE_NEW,   /*35*/
           PROPRIO_OLD,   /*36*/
           PROPRIO_NEW,   /*37*/
           PRAZO_OLD,   /*38*/
           PRAZO_NEW,   /*39*/
           FLAG_CHEQUE_OLD,   /*40*/
           FLAG_CHEQUE_NEW,   /*41*/
           SALDO_CHEQUE_OLD,   /*42*/
           SALDO_CHEQUE_NEW,   /*43*/
           SEQUENCIA_OLD,   /*44*/
           SEQUENCIA_NEW,   /*45*/
           BANCO_CORR_OLD,   /*46*/
           BANCO_CORR_NEW,   /*47*/
           CONTA_CORR_OLD,   /*48*/
           CONTA_CORR_NEW,   /*49*/
           DATA_SUBSTITUICAO_OLD,   /*50*/
           DATA_SUBSTITUICAO_NEW,   /*51*/
           DATA_DEVOLUCAO_OLD,   /*52*/
           DATA_DEVOLUCAO_NEW,   /*53*/
           RECIBO_EMITIDO_OLD,   /*54*/
           RECIBO_EMITIDO_NEW,   /*55*/
           REPRESENTANTE_OLD,   /*56*/
           REPRESENTANTE_NEW,   /*57*/
           COMPENSACAO_OLD,   /*58*/
           COMPENSACAO_NEW,   /*59*/
           CONTROLE1_OLD,   /*60*/
           CONTROLE1_NEW,   /*61*/
           CONTROLE2_OLD,   /*62*/
           CONTROLE2_NEW,   /*63*/
           CONTROLE3_OLD,   /*64*/
           CONTROLE3_NEW,   /*65*/
           TIPIFICACAO_OLD,   /*66*/
           TIPIFICACAO_NEW,   /*67*/
           CONTROLE_CHEQUE_OLD,   /*68*/
           CONTROLE_CHEQUE_NEW,   /*69*/
           FLAG_COMPENSACAO_OLD,   /*70*/
           FLAG_COMPENSACAO_NEW,   /*71*/
           FORMA_PGTO_OLD,   /*72*/
           FORMA_PGTO_NEW,   /*73*/
           SOLICITACAO_LOJA_OLD,   /*74*/
           SOLICITACAO_LOJA_NEW,   /*75*/
           GUIA_OLD,   /*76*/
           GUIA_NEW,   /*77*/
           FLAG_GUIA_OLD,   /*78*/
           FLAG_GUIA_NEW,   /*79*/
           FLAG_CPAG_OLD,   /*80*/
           FLAG_CPAG_NEW,   /*81*/
           AGENCIA_DESCRICAO_OLD,   /*82*/
           AGENCIA_DESCRICAO_NEW,   /*83*/
           COD_ALINEA_REAPRE_OLD,   /*84*/
           COD_ALINEA_REAPRE_NEW,   /*85*/
           COD_ALINEA_DEVOLU_OLD,   /*86*/
           COD_ALINEA_DEVOLU_NEW,   /*87*/
           VALOR_PAGAR_TITULO_OLD,   /*88*/
           VALOR_PAGAR_TITULO_NEW,   /*89*/
           SEL_CHEQUE_OLD,   /*90*/
           SEL_CHEQUE_NEW,   /*91*/
           VALOR_PAGO_CPAG_OLD,   /*92*/
           VALOR_PAGO_CPAG_NEW    /*93*/
        ) values (
            'I', /*1*/
            sysdate, /*2*/
            sysdate,/*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           0,/*8*/
           :new.EMPRESA, /*9*/
           0,/*10*/
           :new.CGC9_CLI, /*11*/
           0,/*12*/
           :new.CGC4_CLI, /*13*/
           0,/*14*/
           :new.CGC2_CLI, /*15*/
           0,/*16*/
           :new.BANCO, /*17*/
           0,/*18*/
           :new.AGENCIA, /*19*/
           0,/*20*/
           :new.CONTA, /*21*/
           0,/*22*/
           :new.NUM_CHEQUE, /*23*/
           '',/*24*/
           :new.CODIGO_SACADO, /*25*/
           0,/*26*/
           :new.VALOR_CHEQUE, /*27*/
           null,/*28*/
           :new.DATA_RECEBIDO, /*29*/
           null,/*30*/
           :new.DATA_VENCTO, /*31*/
           null,/*32*/
           :new.POSDATA, /*33*/
           0,/*34*/
           :new.COD_CIDADE, /*35*/
           0,/*36*/
           :new.PROPRIO, /*37*/
           0,/*38*/
           :new.PRAZO, /*39*/
           0,/*40*/
           :new.FLAG_CHEQUE, /*41*/
           0,/*42*/
           :new.SALDO_CHEQUE, /*43*/
           0,/*44*/
           :new.SEQUENCIA, /*45*/
           0,/*46*/
           :new.BANCO_CORR, /*47*/
           0,/*48*/
           :new.CONTA_CORR, /*49*/
           null,/*50*/
           :new.DATA_SUBSTITUICAO, /*51*/
           null,/*52*/
           :new.DATA_DEVOLUCAO, /*53*/
           0,/*54*/
           :new.RECIBO_EMITIDO, /*55*/
           0,/*56*/
           :new.REPRESENTANTE, /*57*/
           0,/*58*/
           :new.COMPENSACAO, /*59*/
           0,/*60*/
           :new.CONTROLE1, /*61*/
           0,/*62*/
           :new.CONTROLE2, /*63*/
           0,/*64*/
           :new.CONTROLE3, /*65*/
           '',/*66*/
           :new.TIPIFICACAO, /*67*/
           0,/*68*/
           :new.CONTROLE_CHEQUE, /*69*/
           0,/*70*/
           :new.FLAG_COMPENSACAO, /*71*/
           0,/*72*/
           :new.FORMA_PGTO, /*73*/
           0,/*74*/
           :new.SOLICITACAO_LOJA, /*75*/
           0,/*76*/
           :new.GUIA, /*77*/
           0,/*78*/
           :new.FLAG_GUIA, /*79*/
           0,/*80*/
           :new.FLAG_CPAG, /*81*/
           '',/*82*/
           :new.AGENCIA_DESCRICAO, /*83*/
           0,/*84*/
           :new.COD_ALINEA_REAPRE, /*85*/
           0,/*86*/
           :new.COD_ALINEA_DEVOLU, /*87*/
           0,/*88*/
           :new.VALOR_PAGAR_TITULO, /*89*/
           0,/*90*/
           :new.SEL_CHEQUE, /*91*/
           0,/*92*/
           :new.VALOR_PAGO_CPAG /*93*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into CREC_170_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           EMPRESA_OLD, /*8*/
           EMPRESA_NEW, /*9*/
           CGC9_CLI_OLD, /*10*/
           CGC9_CLI_NEW, /*11*/
           CGC4_CLI_OLD, /*12*/
           CGC4_CLI_NEW, /*13*/
           CGC2_CLI_OLD, /*14*/
           CGC2_CLI_NEW, /*15*/
           BANCO_OLD, /*16*/
           BANCO_NEW, /*17*/
           AGENCIA_OLD, /*18*/
           AGENCIA_NEW, /*19*/
           CONTA_OLD, /*20*/
           CONTA_NEW, /*21*/
           NUM_CHEQUE_OLD, /*22*/
           NUM_CHEQUE_NEW, /*23*/
           CODIGO_SACADO_OLD, /*24*/
           CODIGO_SACADO_NEW, /*25*/
           VALOR_CHEQUE_OLD, /*26*/
           VALOR_CHEQUE_NEW, /*27*/
           DATA_RECEBIDO_OLD, /*28*/
           DATA_RECEBIDO_NEW, /*29*/
           DATA_VENCTO_OLD, /*30*/
           DATA_VENCTO_NEW, /*31*/
           POSDATA_OLD, /*32*/
           POSDATA_NEW, /*33*/
           COD_CIDADE_OLD, /*34*/
           COD_CIDADE_NEW, /*35*/
           PROPRIO_OLD, /*36*/
           PROPRIO_NEW, /*37*/
           PRAZO_OLD, /*38*/
           PRAZO_NEW, /*39*/
           FLAG_CHEQUE_OLD, /*40*/
           FLAG_CHEQUE_NEW, /*41*/
           SALDO_CHEQUE_OLD, /*42*/
           SALDO_CHEQUE_NEW, /*43*/
           SEQUENCIA_OLD, /*44*/
           SEQUENCIA_NEW, /*45*/
           BANCO_CORR_OLD, /*46*/
           BANCO_CORR_NEW, /*47*/
           CONTA_CORR_OLD, /*48*/
           CONTA_CORR_NEW, /*49*/
           DATA_SUBSTITUICAO_OLD, /*50*/
           DATA_SUBSTITUICAO_NEW, /*51*/
           DATA_DEVOLUCAO_OLD, /*52*/
           DATA_DEVOLUCAO_NEW, /*53*/
           RECIBO_EMITIDO_OLD, /*54*/
           RECIBO_EMITIDO_NEW, /*55*/
           REPRESENTANTE_OLD, /*56*/
           REPRESENTANTE_NEW, /*57*/
           COMPENSACAO_OLD, /*58*/
           COMPENSACAO_NEW, /*59*/
           CONTROLE1_OLD, /*60*/
           CONTROLE1_NEW, /*61*/
           CONTROLE2_OLD, /*62*/
           CONTROLE2_NEW, /*63*/
           CONTROLE3_OLD, /*64*/
           CONTROLE3_NEW, /*65*/
           TIPIFICACAO_OLD, /*66*/
           TIPIFICACAO_NEW, /*67*/
           CONTROLE_CHEQUE_OLD, /*68*/
           CONTROLE_CHEQUE_NEW, /*69*/
           FLAG_COMPENSACAO_OLD, /*70*/
           FLAG_COMPENSACAO_NEW, /*71*/
           FORMA_PGTO_OLD, /*72*/
           FORMA_PGTO_NEW, /*73*/
           SOLICITACAO_LOJA_OLD, /*74*/
           SOLICITACAO_LOJA_NEW, /*75*/
           GUIA_OLD, /*76*/
           GUIA_NEW, /*77*/
           FLAG_GUIA_OLD, /*78*/
           FLAG_GUIA_NEW, /*79*/
           FLAG_CPAG_OLD, /*80*/
           FLAG_CPAG_NEW, /*81*/
           AGENCIA_DESCRICAO_OLD, /*82*/
           AGENCIA_DESCRICAO_NEW, /*83*/
           COD_ALINEA_REAPRE_OLD, /*84*/
           COD_ALINEA_REAPRE_NEW, /*85*/
           COD_ALINEA_DEVOLU_OLD, /*86*/
           COD_ALINEA_DEVOLU_NEW, /*87*/
           VALOR_PAGAR_TITULO_OLD, /*88*/
           VALOR_PAGAR_TITULO_NEW, /*89*/
           SEL_CHEQUE_OLD, /*90*/
           SEL_CHEQUE_NEW, /*91*/
           VALOR_PAGO_CPAG_OLD, /*92*/
           VALOR_PAGO_CPAG_NEW  /*93*/
        ) values (
            'A', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.EMPRESA,  /*8*/
           :new.EMPRESA, /*9*/
           :old.CGC9_CLI,  /*10*/
           :new.CGC9_CLI, /*11*/
           :old.CGC4_CLI,  /*12*/
           :new.CGC4_CLI, /*13*/
           :old.CGC2_CLI,  /*14*/
           :new.CGC2_CLI, /*15*/
           :old.BANCO,  /*16*/
           :new.BANCO, /*17*/
           :old.AGENCIA,  /*18*/
           :new.AGENCIA, /*19*/
           :old.CONTA,  /*20*/
           :new.CONTA, /*21*/
           :old.NUM_CHEQUE,  /*22*/
           :new.NUM_CHEQUE, /*23*/
           :old.CODIGO_SACADO,  /*24*/
           :new.CODIGO_SACADO, /*25*/
           :old.VALOR_CHEQUE,  /*26*/
           :new.VALOR_CHEQUE, /*27*/
           :old.DATA_RECEBIDO,  /*28*/
           :new.DATA_RECEBIDO, /*29*/
           :old.DATA_VENCTO,  /*30*/
           :new.DATA_VENCTO, /*31*/
           :old.POSDATA,  /*32*/
           :new.POSDATA, /*33*/
           :old.COD_CIDADE,  /*34*/
           :new.COD_CIDADE, /*35*/
           :old.PROPRIO,  /*36*/
           :new.PROPRIO, /*37*/
           :old.PRAZO,  /*38*/
           :new.PRAZO, /*39*/
           :old.FLAG_CHEQUE,  /*40*/
           :new.FLAG_CHEQUE, /*41*/
           :old.SALDO_CHEQUE,  /*42*/
           :new.SALDO_CHEQUE, /*43*/
           :old.SEQUENCIA,  /*44*/
           :new.SEQUENCIA, /*45*/
           :old.BANCO_CORR,  /*46*/
           :new.BANCO_CORR, /*47*/
           :old.CONTA_CORR,  /*48*/
           :new.CONTA_CORR, /*49*/
           :old.DATA_SUBSTITUICAO,  /*50*/
           :new.DATA_SUBSTITUICAO, /*51*/
           :old.DATA_DEVOLUCAO,  /*52*/
           :new.DATA_DEVOLUCAO, /*53*/
           :old.RECIBO_EMITIDO,  /*54*/
           :new.RECIBO_EMITIDO, /*55*/
           :old.REPRESENTANTE,  /*56*/
           :new.REPRESENTANTE, /*57*/
           :old.COMPENSACAO,  /*58*/
           :new.COMPENSACAO, /*59*/
           :old.CONTROLE1,  /*60*/
           :new.CONTROLE1, /*61*/
           :old.CONTROLE2,  /*62*/
           :new.CONTROLE2, /*63*/
           :old.CONTROLE3,  /*64*/
           :new.CONTROLE3, /*65*/
           :old.TIPIFICACAO,  /*66*/
           :new.TIPIFICACAO, /*67*/
           :old.CONTROLE_CHEQUE,  /*68*/
           :new.CONTROLE_CHEQUE, /*69*/
           :old.FLAG_COMPENSACAO,  /*70*/
           :new.FLAG_COMPENSACAO, /*71*/
           :old.FORMA_PGTO,  /*72*/
           :new.FORMA_PGTO, /*73*/
           :old.SOLICITACAO_LOJA,  /*74*/
           :new.SOLICITACAO_LOJA, /*75*/
           :old.GUIA,  /*76*/
           :new.GUIA, /*77*/
           :old.FLAG_GUIA,  /*78*/
           :new.FLAG_GUIA, /*79*/
           :old.FLAG_CPAG,  /*80*/
           :new.FLAG_CPAG, /*81*/
           :old.AGENCIA_DESCRICAO,  /*82*/
           :new.AGENCIA_DESCRICAO, /*83*/
           :old.COD_ALINEA_REAPRE,  /*84*/
           :new.COD_ALINEA_REAPRE, /*85*/
           :old.COD_ALINEA_DEVOLU,  /*86*/
           :new.COD_ALINEA_DEVOLU, /*87*/
           :old.VALOR_PAGAR_TITULO,  /*88*/
           :new.VALOR_PAGAR_TITULO, /*89*/
           :old.SEL_CHEQUE,  /*90*/
           :new.SEL_CHEQUE, /*91*/
           :old.VALOR_PAGO_CPAG,  /*92*/
           :new.VALOR_PAGO_CPAG  /*93*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into CREC_170_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           EMPRESA_OLD, /*8*/
           EMPRESA_NEW, /*9*/
           CGC9_CLI_OLD, /*10*/
           CGC9_CLI_NEW, /*11*/
           CGC4_CLI_OLD, /*12*/
           CGC4_CLI_NEW, /*13*/
           CGC2_CLI_OLD, /*14*/
           CGC2_CLI_NEW, /*15*/
           BANCO_OLD, /*16*/
           BANCO_NEW, /*17*/
           AGENCIA_OLD, /*18*/
           AGENCIA_NEW, /*19*/
           CONTA_OLD, /*20*/
           CONTA_NEW, /*21*/
           NUM_CHEQUE_OLD, /*22*/
           NUM_CHEQUE_NEW, /*23*/
           CODIGO_SACADO_OLD, /*24*/
           CODIGO_SACADO_NEW, /*25*/
           VALOR_CHEQUE_OLD, /*26*/
           VALOR_CHEQUE_NEW, /*27*/
           DATA_RECEBIDO_OLD, /*28*/
           DATA_RECEBIDO_NEW, /*29*/
           DATA_VENCTO_OLD, /*30*/
           DATA_VENCTO_NEW, /*31*/
           POSDATA_OLD, /*32*/
           POSDATA_NEW, /*33*/
           COD_CIDADE_OLD, /*34*/
           COD_CIDADE_NEW, /*35*/
           PROPRIO_OLD, /*36*/
           PROPRIO_NEW, /*37*/
           PRAZO_OLD, /*38*/
           PRAZO_NEW, /*39*/
           FLAG_CHEQUE_OLD, /*40*/
           FLAG_CHEQUE_NEW, /*41*/
           SALDO_CHEQUE_OLD, /*42*/
           SALDO_CHEQUE_NEW, /*43*/
           SEQUENCIA_OLD, /*44*/
           SEQUENCIA_NEW, /*45*/
           BANCO_CORR_OLD, /*46*/
           BANCO_CORR_NEW, /*47*/
           CONTA_CORR_OLD, /*48*/
           CONTA_CORR_NEW, /*49*/
           DATA_SUBSTITUICAO_OLD, /*50*/
           DATA_SUBSTITUICAO_NEW, /*51*/
           DATA_DEVOLUCAO_OLD, /*52*/
           DATA_DEVOLUCAO_NEW, /*53*/
           RECIBO_EMITIDO_OLD, /*54*/
           RECIBO_EMITIDO_NEW, /*55*/
           REPRESENTANTE_OLD, /*56*/
           REPRESENTANTE_NEW, /*57*/
           COMPENSACAO_OLD, /*58*/
           COMPENSACAO_NEW, /*59*/
           CONTROLE1_OLD, /*60*/
           CONTROLE1_NEW, /*61*/
           CONTROLE2_OLD, /*62*/
           CONTROLE2_NEW, /*63*/
           CONTROLE3_OLD, /*64*/
           CONTROLE3_NEW, /*65*/
           TIPIFICACAO_OLD, /*66*/
           TIPIFICACAO_NEW, /*67*/
           CONTROLE_CHEQUE_OLD, /*68*/
           CONTROLE_CHEQUE_NEW, /*69*/
           FLAG_COMPENSACAO_OLD, /*70*/
           FLAG_COMPENSACAO_NEW, /*71*/
           FORMA_PGTO_OLD, /*72*/
           FORMA_PGTO_NEW, /*73*/
           SOLICITACAO_LOJA_OLD, /*74*/
           SOLICITACAO_LOJA_NEW, /*75*/
           GUIA_OLD, /*76*/
           GUIA_NEW, /*77*/
           FLAG_GUIA_OLD, /*78*/
           FLAG_GUIA_NEW, /*79*/
           FLAG_CPAG_OLD, /*80*/
           FLAG_CPAG_NEW, /*81*/
           AGENCIA_DESCRICAO_OLD, /*82*/
           AGENCIA_DESCRICAO_NEW, /*83*/
           COD_ALINEA_REAPRE_OLD, /*84*/
           COD_ALINEA_REAPRE_NEW, /*85*/
           COD_ALINEA_DEVOLU_OLD, /*86*/
           COD_ALINEA_DEVOLU_NEW, /*87*/
           VALOR_PAGAR_TITULO_OLD, /*88*/
           VALOR_PAGAR_TITULO_NEW, /*89*/
           SEL_CHEQUE_OLD, /*90*/
           SEL_CHEQUE_NEW, /*91*/
           VALOR_PAGO_CPAG_OLD, /*92*/
           VALOR_PAGO_CPAG_NEW /*93*/
        ) values (
            'D', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede,/*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.EMPRESA, /*8*/
           0, /*9*/
           :old.CGC9_CLI, /*10*/
           0, /*11*/
           :old.CGC4_CLI, /*12*/
           0, /*13*/
           :old.CGC2_CLI, /*14*/
           0, /*15*/
           :old.BANCO, /*16*/
           0, /*17*/
           :old.AGENCIA, /*18*/
           0, /*19*/
           :old.CONTA, /*20*/
           0, /*21*/
           :old.NUM_CHEQUE, /*22*/
           0, /*23*/
           :old.CODIGO_SACADO, /*24*/
           '', /*25*/
           :old.VALOR_CHEQUE, /*26*/
           0, /*27*/
           :old.DATA_RECEBIDO, /*28*/
           null, /*29*/
           :old.DATA_VENCTO, /*30*/
           null, /*31*/
           :old.POSDATA, /*32*/
           null, /*33*/
           :old.COD_CIDADE, /*34*/
           0, /*35*/
           :old.PROPRIO, /*36*/
           0, /*37*/
           :old.PRAZO, /*38*/
           0, /*39*/
           :old.FLAG_CHEQUE, /*40*/
           0, /*41*/
           :old.SALDO_CHEQUE, /*42*/
           0, /*43*/
           :old.SEQUENCIA, /*44*/
           0, /*45*/
           :old.BANCO_CORR, /*46*/
           0, /*47*/
           :old.CONTA_CORR, /*48*/
           0, /*49*/
           :old.DATA_SUBSTITUICAO, /*50*/
           null, /*51*/
           :old.DATA_DEVOLUCAO, /*52*/
           null, /*53*/
           :old.RECIBO_EMITIDO, /*54*/
           0, /*55*/
           :old.REPRESENTANTE, /*56*/
           0, /*57*/
           :old.COMPENSACAO, /*58*/
           0, /*59*/
           :old.CONTROLE1, /*60*/
           0, /*61*/
           :old.CONTROLE2, /*62*/
           0, /*63*/
           :old.CONTROLE3, /*64*/
           0, /*65*/
           :old.TIPIFICACAO, /*66*/
           '', /*67*/
           :old.CONTROLE_CHEQUE, /*68*/
           0, /*69*/
           :old.FLAG_COMPENSACAO, /*70*/
           0, /*71*/
           :old.FORMA_PGTO, /*72*/
           0, /*73*/
           :old.SOLICITACAO_LOJA, /*74*/
           0, /*75*/
           :old.GUIA, /*76*/
           0, /*77*/
           :old.FLAG_GUIA, /*78*/
           0, /*79*/
           :old.FLAG_CPAG, /*80*/
           0, /*81*/
           :old.AGENCIA_DESCRICAO, /*82*/
           '', /*83*/
           :old.COD_ALINEA_REAPRE, /*84*/
           0, /*85*/
           :old.COD_ALINEA_DEVOLU, /*86*/
           0, /*87*/
           :old.VALOR_PAGAR_TITULO, /*88*/
           0, /*89*/
           :old.SEL_CHEQUE, /*90*/
           0, /*91*/
           :old.VALOR_PAGO_CPAG, /*92*/
           0 /*93*/
         );
    end;
 end if;
end inter_tr_CREC_170_log;
-- ALTER TRIGGER "INTER_TR_CREC_170_LOG" ENABLE
 

/

exec inter_pr_recompile;

