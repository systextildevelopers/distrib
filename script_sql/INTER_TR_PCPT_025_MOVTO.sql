create or replace trigger "INTER_TR_PCPT_025_MOVTO"
after insert or delete or update
on PCPT_025
for each row
declare
    ws_usuario_rede           varchar2(20) ;
    ws_maquina_rede           varchar2(40) ;
    ws_aplicativo             varchar2(20) ;
    ws_sid                    number(9) ;
    ws_empresa                number(3) ;
    ws_usuario_systextil      varchar2(20) ;
    ws_locale_usuario         varchar2(5) ;
    v_nome_programa           varchar2(20) ;
begin
    inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                            ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
    
    v_nome_programa := inter_fn_nome_programa(ws_sid);
    
    if  inserting 
    and :new.data_sai_estq is not null
    then
        insert into pcpt_025_movto (
            area_ordem,               codigo_rolo,
            ordem_producao,           data_movimento,
            qtde_movimento,           tipo_movimento,
            usuario_systextil,        processo_systextil,
            data_insercao,            usuario_rede,
            maquina_rede,             aplicativo
        ) values (
            :new.area_ordem,          :new.codigo_rolo,
            :new.ordem_producao,      :new.data_sai_estq,
            :new.qtde_kg_inicial,     'A',
            ws_usuario_systextil,     v_nome_programa,
            sysdate,                  ws_usuario_rede,
            ws_maquina_rede,          ws_aplicativo
        );
     end if;

     if  updating 
     and :new.data_sai_estq is not null
     then
        -- Esta trocando a ordem de produção do rolo 
        if :old.ordem_producao  <> :new.ordem_producao
        or :old.codigo_rolo     <> :new.codigo_rolo
        or :old.qtde_kg_inicial <> :new.qtde_kg_inicial
        then
            insert into pcpt_025_movto (
                area_ordem,               codigo_rolo,
                ordem_producao,           data_movimento,
                qtde_movimento,           tipo_movimento,
                usuario_systextil,        processo_systextil,
                data_insercao,            usuario_rede,
                maquina_rede,             aplicativo
            ) values (
                :old.area_ordem,          :old.codigo_rolo,
                :old.ordem_producao,      :old.data_sai_estq,
                :old.qtde_kg_inicial,     'D',
                ws_usuario_systextil,     v_nome_programa,
                sysdate,                  ws_usuario_rede,
                ws_maquina_rede,          ws_aplicativo
            );
            
            insert into pcpt_025_movto (
                area_ordem,               codigo_rolo,
                ordem_producao,           data_movimento,
                qtde_movimento,           tipo_movimento,
                usuario_systextil,        processo_systextil,
                data_insercao,            usuario_rede,
                maquina_rede,             aplicativo
            ) values (
                :new.area_ordem,          :new.codigo_rolo,
                :new.ordem_producao,      :new.data_sai_estq,
                :new.qtde_kg_inicial,     'A',
                ws_usuario_systextil,     v_nome_programa,
                sysdate,                  ws_usuario_rede,
                ws_maquina_rede,          ws_aplicativo
            );
        end if;
     end if;
     
     if deleting
     and :old.data_sai_estq is not null
     then
        insert into pcpt_025_movto (
            area_ordem,               codigo_rolo,
            ordem_producao,           data_movimento,
            qtde_movimento,           tipo_movimento,
            usuario_systextil,        processo_systextil,
            data_insercao,            usuario_rede,
            maquina_rede,             aplicativo
        ) values (
            :old.area_ordem,          :old.codigo_rolo,
            :old.ordem_producao,      :old.data_sai_estq,
            :old.qtde_kg_inicial,     'D',
            ws_usuario_systextil,     v_nome_programa,
            sysdate,                  ws_usuario_rede,
            ws_maquina_rede,          ws_aplicativo
        );
     end if;
end inter_tr_PCPT_025_MOVTO;

/
exec inter_pr_recompile;
