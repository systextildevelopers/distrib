
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_052_HIST" 
BEFORE DELETE OR INSERT OR
UPDATE of cod_empresa, num_nota, cod_serie_nota, cod_mensagem,
	seq_mensagem, ind_local, des_mensag_1, des_mensag_2,
	des_mensag_3, des_mensag_4, des_mensag_5, des_mensag_6,
	des_mensag_7, des_mensag_8, des_mensag_9, des_mensag_10,
	cnpj9, cnpj4, cnpj2, ind_entr_saida,
	des_mensag_11, des_mensag_12
on fatu_052
for each row

-- Finalidade: Registrar altera��es feitas na tabela fatu_052 - mensagens danf
-- Autor.....: Aline Blanski
-- Data......: 13/07/10
--
-- Hist�ricos de altera��es na trigger
--
-- Data      Autor           SS          Observa��es
--

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_long_aux               varchar2(4000);
   ws_tipo_ocorr             varchar2(1);
   ws_locale_usuario         varchar2(5);
   ws_nome_programa hdoc_090.programa%type;


begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   
   ws_nome_programa := inter_fn_nome_programa(ws_sid); 

   if INSERTING then
      ws_tipo_ocorr   := 'I';
      ws_long_aux     :=  'Inser��o na tabela  :'           ||
                          'C�digo da Emprresa : '           || :new.COD_EMPRESA     || chr(10)  || ' ' ||
                          'N�mero da Nota Fiscal : '        || :new.num_nota        || chr(10)  || ' ' ||
                          'C�digo da S�rie de Nota : '      || :new.cod_serie_nota  || chr(10)  || ' ' ||
                          'C�digo da Mensagem : '           || :new.cod_mensagem    || chr(10)  || ' ' ||
                          'Sequencia da Mensagem : '        || :new.seq_mensagem    || chr(10)  || ' ' ||
                          'Indicador de Local : '           || :new.ind_local       || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 1 : '      || :new.des_mensag_1    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 2 : '      || :new.des_mensag_2    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 3 : '      || :new.des_mensag_3    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 4 : '      || :new.des_mensag_4    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 5 : '      || :new.des_mensag_5    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 6 : '      || :new.des_mensag_6    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 7 : '      || :new.des_mensag_7    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 8 : '      || :new.des_mensag_8    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 9 : '      || :new.des_mensag_9    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 10 : '     || :new.des_mensag_10   || chr(10)  || ' ' ||
                          'CNPJ da Nota Fiscal : '          || :new.cnpj9 || '.' || :new.cnpj4 || '/' || :new.cnpj2|| chr(10)  || ' ' ||
                          'Indicador de Entrada e Sa�da : ' || :new.ind_entr_saida ;

   elsif DELETING then
      ws_tipo_ocorr   := 'D';
      ws_long_aux     :=  'Exclus�o na tabela:'             || chr(10)              || ' ' ||
                          'C�digo da Emprresa : '           || :old.COD_EMPRESA     || chr(10)  || ' ' ||
                          'N�mero da Nota Fiscal : '        || :old.num_nota        || chr(10)  || ' ' ||
                          'C�digo da S�rie de Nota : '      || :old.cod_serie_nota  || chr(10)  || ' ' ||
                          'C�digo da Mensagem : '           || :old.cod_mensagem    || chr(10)  || ' ' ||
                          'Sequencia da Mensagem : '        || :old.seq_mensagem    || chr(10)  || ' ' ||
                          'Indicador de Local : '           || :old.ind_local       || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 1 : '      || :old.des_mensag_1    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 2 : '      || :old.des_mensag_2    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 3 : '      || :old.des_mensag_3    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 4 : '      || :old.des_mensag_4    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 5 : '      || :old.des_mensag_5    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 6 : '      || :old.des_mensag_6    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 7 : '      || :old.des_mensag_7    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 8 : '      || :old.des_mensag_8    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 9 : '      || :old.des_mensag_9    || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 10 : '     || :old.des_mensag_10   || chr(10)  || ' ' ||
                          'CNPJ da Nota Fiscal : '          || :old.cnpj9 || '.' || :old.cnpj4 || '/' || :old.cnpj2|| chr(10)  || ' ' ||
                          'Indicador de Entrada e Sa�da : ' || :old.ind_entr_saida ;

   elsif UPDATING then
      ws_tipo_ocorr   := 'A';
      ws_long_aux     :=  'Altera��o na tabela:'            || chr(10)              || ' '     ||
                          'C�digo da Emprresa : '           || :new.COD_EMPRESA     || ' --> ' || :old.COD_EMPRESA     || chr(10) || ' ' ||
                          'N�mero da Nota Fiscal : '        || :new.num_nota        || ' --> ' || :old.num_nota        ||chr(10)  || ' ' ||
                          'C�digo da S�rie de Nota : '      || :new.cod_serie_nota  || ' --> ' || :old.cod_serie_nota     || ' ' ||chr(10)  || ' ' ||
                          'C�digo da Mensagem : '           || :new.cod_mensagem    || ' --> ' || :old.cod_mensagem     || ' ' ||chr(10)  || ' ' ||
                          'Sequencia da Mensagem : '        || :new.seq_mensagem    || ' --> ' || :old.seq_mensagem     || ' ' ||chr(10)  || ' ' ||
                          'Indicador de Local : '           || :new.ind_local       || ' --> ' || :old.ind_local     || ' ' || chr(10)  || ' ' ||
                          'Descri��o da Mensagem 1 : '      || :new.des_mensag_1    || ' --> ' || :old.des_mensag_1     || ' ' ||chr(10)  || ' ' ||
                          'Descri��o da Mensagem 2 : '      || :new.des_mensag_2    || ' --> ' || :old.des_mensag_2     || ' ' ||chr(10)  || ' ' ||
                          'Descri��o da Mensagem 3 : '      || :new.des_mensag_3    || ' --> ' || :old.des_mensag_3     || ' ' ||chr(10)  || ' ' ||
                          'Descri��o da Mensagem 4 : '      || :new.des_mensag_4    || ' --> ' || :old.des_mensag_4     || ' ' ||chr(10)  || ' ' ||
                          'Descri��o da Mensagem 5 : '      || :new.des_mensag_5    || ' --> ' || :old.des_mensag_5     || ' ' ||chr(10)  || ' ' ||
                          'Descri��o da Mensagem 6 : '      || :new.des_mensag_6    || ' --> ' || :old.des_mensag_6     || ' ' ||chr(10)  || ' ' ||
                          'Descri��o da Mensagem 7 : '      || :new.des_mensag_7    || ' --> ' || :old.des_mensag_7     || ' ' ||chr(10)  || ' ' ||
                          'Descri��o da Mensagem 8 : '      || :new.des_mensag_8    || ' --> ' || :old.des_mensag_8     || ' ' ||chr(10)  || ' ' ||
                          'Descri��o da Mensagem 9 : '      || :new.des_mensag_9    || ' --> ' || :old.des_mensag_9     || ' ' ||chr(10)  || ' ' ||
                          'Descri��o da Mensagem 10 : '     || :new.des_mensag_10   || ' --> ' || :old.des_mensag_10     || ' ' ||chr(10)  || ' ' ||
                          'CNPJ da Nota Fiscal : '          || :new.cnpj9 || '.' || :new.cnpj4 || '/' || :new.cnpj2||
                                                                                       ' --> ' || :old.cnpj9 || '.' || :old.cnpj4 || '/' || :old.cnpj2|| chr(10)  ||
                          'Indicador de Entrada e Sa�da : ' || :new.ind_entr_saida  || ' --> ' || :old.ind_entr_saida;
   end if;

   begin
      INSERT INTO hist_100
             ( tabela,              operacao,
               data_ocorr,          aplicacao,
               usuario_rede,        maquina_rede,
               programa,            long01
             )
       VALUES
             ( 'FATU_052',          ws_tipo_ocorr,
               sysdate,             ws_aplicativo,
               ws_usuario_rede,     ws_maquina_rede,
               ws_nome_programa,    ws_long_aux
             );
   exception when OTHERS then
      raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATEN��O! N�o inseriu {0}. Status: {1}.', 'HIST_100(013-2)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

end inter_tr_fatu_052_hist;

-- ALTER TRIGGER "INTER_TR_FATU_052_HIST" ENABLE
 

/

exec inter_pr_recompile;

