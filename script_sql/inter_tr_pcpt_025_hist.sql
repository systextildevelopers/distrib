create or replace trigger inter_tr_pcpt_025_hist
after insert or delete or update 
on pcpt_025 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    begin 
       select hdoc_090.programa 
       into v_nome_programa 
       from hdoc_090 
       where hdoc_090.sid = ws_sid 
         and rownum       = 1 
         and hdoc_090.programa not like '%menu%' 
         and hdoc_090.programa not like '%_m%'; 
       exception 
         when no_data_found then            v_nome_programa := 'sql'; 
    end; 
 
 
 
 if inserting 
 then 
    begin 
 
        insert into pcpt_025_hist (
           tipo_ocorr,   /*0*/ 
           data_ocorr,   /*1*/ 
           hora_ocorr,   /*2*/ 
           usuario_rede,   /*3*/ 
           maquina_rede,   /*4*/ 
           aplicacao,   /*5*/ 
           usuario_sistema,   /*6*/ 
           nome_programa,   /*7*/ 
           area_ordem_old,   /*8*/ 
           area_ordem_new,   /*9*/ 
           periodo_ordem_old,   /*10*/ 
           periodo_ordem_new,   /*11*/ 
           numero_ordem_old,   /*12*/ 
           numero_ordem_new,   /*13*/ 
           numrolo_per_tece_old,   /*14*/ 
           numrolo_per_tece_new,   /*15*/ 
           numrolo_nr_rolo_old,   /*16*/ 
           numrolo_nr_rolo_new,   /*17*/ 
           codigo_deposito_old,   /*18*/ 
           codigo_deposito_new,   /*19*/ 
           pano_ini_nivel99_old,   /*20*/ 
           pano_ini_nivel99_new,   /*21*/ 
           pano_ini_grupo_old,   /*22*/ 
           pano_ini_grupo_new,   /*23*/ 
           pano_ini_subgrupo_old,   /*24*/ 
           pano_ini_subgrupo_new,   /*25*/ 
           pano_ini_item_old,   /*26*/ 
           pano_ini_item_new,   /*27*/ 
           pano_fin_nivel99_old,   /*28*/ 
           pano_fin_nivel99_new,   /*29*/ 
           pano_fin_grupo_old,   /*30*/ 
           pano_fin_grupo_new,   /*31*/ 
           pano_fin_subgrupo_old,   /*32*/ 
           pano_fin_subgrupo_new,   /*33*/ 
           pano_fin_item_old,   /*34*/ 
           pano_fin_item_new,   /*35*/ 
           qtde_kg_inicial_old,   /*36*/ 
           qtde_kg_inicial_new,   /*37*/ 
           qtde_kg_final_old,   /*38*/ 
           qtde_kg_final_new,   /*39*/ 
           data_sai_estq_old,   /*40*/ 
           data_sai_estq_new,   /*41*/ 
           data_ent_estq_old,   /*42*/ 
           data_ent_estq_new,   /*43*/ 
           seq_ordem_old,   /*44*/ 
           seq_ordem_new,   /*45*/ 
           codigo_rolo_old,   /*46*/ 
           codigo_rolo_new,   /*47*/ 
           rolo_acabado_old,   /*48*/ 
           rolo_acabado_new,   /*49*/ 
           ordem_producao_old,   /*50*/ 
           ordem_producao_new,   /*51*/ 
           grupo_maquina_old,   /*52*/ 
           grupo_maquina_new,   /*53*/ 
           subgrupo_maquina_old,   /*54*/ 
           subgrupo_maquina_new,   /*55*/ 
           numero_maquina_old,   /*56*/ 
           numero_maquina_new,   /*57*/ 
           rolo_confirmado_old,   /*58*/ 
           rolo_confirmado_new,   /*59*/ 
           usuario_old,   /*60*/ 
           usuario_new,   /*61*/ 
           data_hora_conf_old,   /*62*/ 
           data_hora_conf_new,   /*63*/ 
           rolo_conferido_old,   /*64*/ 
           rolo_conferido_new,   /*65*/ 
           estagio_producao_old,   /*66*/ 
           estagio_producao_new,   /*67*/ 
           rolo_rateado_old,   /*68*/ 
           rolo_rateado_new    /*69*/
        ) values (    
            'i', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.area_ordem, /*9*/   
           0,/*10*/
           :new.periodo_ordem, /*11*/   
           0,/*12*/
           :new.numero_ordem, /*13*/   
           0,/*14*/
           :new.numrolo_per_tece, /*15*/   
           0,/*16*/
           :new.numrolo_nr_rolo, /*17*/   
           0,/*18*/
           :new.codigo_deposito, /*19*/   
           '',/*20*/
           :new.pano_ini_nivel99, /*21*/   
           '',/*22*/
           :new.pano_ini_grupo, /*23*/   
           '',/*24*/
           :new.pano_ini_subgrupo, /*25*/   
           '',/*26*/
           :new.pano_ini_item, /*27*/   
           '',/*28*/
           :new.pano_fin_nivel99, /*29*/   
           '',/*30*/
           :new.pano_fin_grupo, /*31*/   
           '',/*32*/
           :new.pano_fin_subgrupo, /*33*/   
           '',/*34*/
           :new.pano_fin_item, /*35*/   
           0,/*36*/
           :new.qtde_kg_inicial, /*37*/   
           0,/*38*/
           :new.qtde_kg_final, /*39*/   
           null,/*40*/
           :new.data_sai_estq, /*41*/   
           null,/*42*/
           :new.data_ent_estq, /*43*/   
           0,/*44*/
           :new.seq_ordem, /*45*/   
           0,/*46*/
           :new.codigo_rolo, /*47*/   
           0,/*48*/
           :new.rolo_acabado, /*49*/   
           0,/*50*/
           :new.ordem_producao, /*51*/   
           '',/*52*/
           :new.grupo_maquina, /*53*/   
           '',/*54*/
           :new.subgrupo_maquina, /*55*/   
           0,/*56*/
           :new.numero_maquina, /*57*/   
           '',/*58*/
           :new.rolo_confirmado, /*59*/   
           '',/*60*/
           :new.usuario, /*61*/   
           null,/*62*/
           :new.data_hora_conf, /*63*/   
           '',/*64*/
           :new.rolo_conferido, /*65*/   
           0,/*66*/
           :new.estagio_producao, /*67*/   
           '',/*68*/
           :new.rolo_rateado /*69*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into pcpt_025_hist (
           tipo_ocorr, /*0*/   
           data_ocorr, /*1*/   
           hora_ocorr, /*2*/   
           usuario_rede, /*3*/   
           maquina_rede, /*4*/   
           aplicacao, /*5*/   
           usuario_sistema, /*6*/   
           nome_programa, /*7*/   
           area_ordem_old, /*8*/   
           area_ordem_new, /*9*/   
           periodo_ordem_old, /*10*/   
           periodo_ordem_new, /*11*/   
           numero_ordem_old, /*12*/   
           numero_ordem_new, /*13*/   
           numrolo_per_tece_old, /*14*/   
           numrolo_per_tece_new, /*15*/   
           numrolo_nr_rolo_old, /*16*/   
           numrolo_nr_rolo_new, /*17*/   
           codigo_deposito_old, /*18*/   
           codigo_deposito_new, /*19*/   
           pano_ini_nivel99_old, /*20*/   
           pano_ini_nivel99_new, /*21*/   
           pano_ini_grupo_old, /*22*/   
           pano_ini_grupo_new, /*23*/   
           pano_ini_subgrupo_old, /*24*/   
           pano_ini_subgrupo_new, /*25*/   
           pano_ini_item_old, /*26*/   
           pano_ini_item_new, /*27*/   
           pano_fin_nivel99_old, /*28*/   
           pano_fin_nivel99_new, /*29*/   
           pano_fin_grupo_old, /*30*/   
           pano_fin_grupo_new, /*31*/   
           pano_fin_subgrupo_old, /*32*/   
           pano_fin_subgrupo_new, /*33*/   
           pano_fin_item_old, /*34*/   
           pano_fin_item_new, /*35*/   
           qtde_kg_inicial_old, /*36*/   
           qtde_kg_inicial_new, /*37*/   
           qtde_kg_final_old, /*38*/   
           qtde_kg_final_new, /*39*/   
           data_sai_estq_old, /*40*/   
           data_sai_estq_new, /*41*/   
           data_ent_estq_old, /*42*/   
           data_ent_estq_new, /*43*/   
           seq_ordem_old, /*44*/   
           seq_ordem_new, /*45*/   
           codigo_rolo_old, /*46*/   
           codigo_rolo_new, /*47*/   
           rolo_acabado_old, /*48*/   
           rolo_acabado_new, /*49*/   
           ordem_producao_old, /*50*/   
           ordem_producao_new, /*51*/   
           grupo_maquina_old, /*52*/   
           grupo_maquina_new, /*53*/   
           subgrupo_maquina_old, /*54*/   
           subgrupo_maquina_new, /*55*/   
           numero_maquina_old, /*56*/   
           numero_maquina_new, /*57*/   
           rolo_confirmado_old, /*58*/   
           rolo_confirmado_new, /*59*/   
           usuario_old, /*60*/   
           usuario_new, /*61*/   
           data_hora_conf_old, /*62*/   
           data_hora_conf_new, /*63*/   
           rolo_conferido_old, /*64*/   
           rolo_conferido_new, /*65*/   
           estagio_producao_old, /*66*/   
           estagio_producao_new, /*67*/   
           rolo_rateado_old, /*68*/   
           rolo_rateado_new  /*69*/  
        ) values (    
            'a', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.area_ordem,  /*8*/  
           :new.area_ordem, /*9*/   
           :old.periodo_ordem,  /*10*/  
           :new.periodo_ordem, /*11*/   
           :old.numero_ordem,  /*12*/  
           :new.numero_ordem, /*13*/   
           :old.numrolo_per_tece,  /*14*/  
           :new.numrolo_per_tece, /*15*/   
           :old.numrolo_nr_rolo,  /*16*/  
           :new.numrolo_nr_rolo, /*17*/   
           :old.codigo_deposito,  /*18*/  
           :new.codigo_deposito, /*19*/   
           :old.pano_ini_nivel99,  /*20*/  
           :new.pano_ini_nivel99, /*21*/   
           :old.pano_ini_grupo,  /*22*/  
           :new.pano_ini_grupo, /*23*/   
           :old.pano_ini_subgrupo,  /*24*/  
           :new.pano_ini_subgrupo, /*25*/   
           :old.pano_ini_item,  /*26*/  
           :new.pano_ini_item, /*27*/   
           :old.pano_fin_nivel99,  /*28*/  
           :new.pano_fin_nivel99, /*29*/   
           :old.pano_fin_grupo,  /*30*/  
           :new.pano_fin_grupo, /*31*/   
           :old.pano_fin_subgrupo,  /*32*/  
           :new.pano_fin_subgrupo, /*33*/   
           :old.pano_fin_item,  /*34*/  
           :new.pano_fin_item, /*35*/   
           :old.qtde_kg_inicial,  /*36*/  
           :new.qtde_kg_inicial, /*37*/   
           :old.qtde_kg_final,  /*38*/  
           :new.qtde_kg_final, /*39*/   
           :old.data_sai_estq,  /*40*/  
           :new.data_sai_estq, /*41*/   
           :old.data_ent_estq,  /*42*/  
           :new.data_ent_estq, /*43*/   
           :old.seq_ordem,  /*44*/  
           :new.seq_ordem, /*45*/   
           :old.codigo_rolo,  /*46*/  
           :new.codigo_rolo, /*47*/   
           :old.rolo_acabado,  /*48*/  
           :new.rolo_acabado, /*49*/   
           :old.ordem_producao,  /*50*/  
           :new.ordem_producao, /*51*/   
           :old.grupo_maquina,  /*52*/  
           :new.grupo_maquina, /*53*/   
           :old.subgrupo_maquina,  /*54*/  
           :new.subgrupo_maquina, /*55*/   
           :old.numero_maquina,  /*56*/  
           :new.numero_maquina, /*57*/   
           :old.rolo_confirmado,  /*58*/  
           :new.rolo_confirmado, /*59*/   
           :old.usuario,  /*60*/  
           :new.usuario, /*61*/   
           :old.data_hora_conf,  /*62*/  
           :new.data_hora_conf, /*63*/   
           :old.rolo_conferido,  /*64*/  
           :new.rolo_conferido, /*65*/   
           :old.estagio_producao,  /*66*/  
           :new.estagio_producao, /*67*/   
           :old.rolo_rateado,  /*68*/  
           :new.rolo_rateado  /*69*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into pcpt_025_hist (
           tipo_ocorr, /*0*/   
           data_ocorr, /*1*/   
           hora_ocorr, /*2*/   
           usuario_rede, /*3*/   
           maquina_rede, /*4*/   
           aplicacao, /*5*/   
           usuario_sistema, /*6*/   
           nome_programa, /*7*/   
           area_ordem_old, /*8*/   
           area_ordem_new, /*9*/   
           periodo_ordem_old, /*10*/   
           periodo_ordem_new, /*11*/   
           numero_ordem_old, /*12*/   
           numero_ordem_new, /*13*/   
           numrolo_per_tece_old, /*14*/   
           numrolo_per_tece_new, /*15*/   
           numrolo_nr_rolo_old, /*16*/   
           numrolo_nr_rolo_new, /*17*/   
           codigo_deposito_old, /*18*/   
           codigo_deposito_new, /*19*/   
           pano_ini_nivel99_old, /*20*/   
           pano_ini_nivel99_new, /*21*/   
           pano_ini_grupo_old, /*22*/   
           pano_ini_grupo_new, /*23*/   
           pano_ini_subgrupo_old, /*24*/   
           pano_ini_subgrupo_new, /*25*/   
           pano_ini_item_old, /*26*/   
           pano_ini_item_new, /*27*/   
           pano_fin_nivel99_old, /*28*/   
           pano_fin_nivel99_new, /*29*/   
           pano_fin_grupo_old, /*30*/   
           pano_fin_grupo_new, /*31*/   
           pano_fin_subgrupo_old, /*32*/   
           pano_fin_subgrupo_new, /*33*/   
           pano_fin_item_old, /*34*/   
           pano_fin_item_new, /*35*/   
           qtde_kg_inicial_old, /*36*/   
           qtde_kg_inicial_new, /*37*/   
           qtde_kg_final_old, /*38*/   
           qtde_kg_final_new, /*39*/   
           data_sai_estq_old, /*40*/   
           data_sai_estq_new, /*41*/   
           data_ent_estq_old, /*42*/   
           data_ent_estq_new, /*43*/   
           seq_ordem_old, /*44*/   
           seq_ordem_new, /*45*/   
           codigo_rolo_old, /*46*/   
           codigo_rolo_new, /*47*/   
           rolo_acabado_old, /*48*/   
           rolo_acabado_new, /*49*/   
           ordem_producao_old, /*50*/   
           ordem_producao_new, /*51*/   
           grupo_maquina_old, /*52*/   
           grupo_maquina_new, /*53*/   
           subgrupo_maquina_old, /*54*/   
           subgrupo_maquina_new, /*55*/   
           numero_maquina_old, /*56*/   
           numero_maquina_new, /*57*/   
           rolo_confirmado_old, /*58*/   
           rolo_confirmado_new, /*59*/   
           usuario_old, /*60*/   
           usuario_new, /*61*/   
           data_hora_conf_old, /*62*/   
           data_hora_conf_new, /*63*/   
           rolo_conferido_old, /*64*/   
           rolo_conferido_new, /*65*/   
           estagio_producao_old, /*66*/   
           estagio_producao_new, /*67*/   
           rolo_rateado_old, /*68*/   
           rolo_rateado_new /*69*/   
        ) values (    
            'd', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.area_ordem, /*8*/   
           0, /*9*/
           :old.periodo_ordem, /*10*/   
           0, /*11*/
           :old.numero_ordem, /*12*/   
           0, /*13*/
           :old.numrolo_per_tece, /*14*/   
           0, /*15*/
           :old.numrolo_nr_rolo, /*16*/   
           0, /*17*/
           :old.codigo_deposito, /*18*/   
           0, /*19*/
           :old.pano_ini_nivel99, /*20*/   
           '', /*21*/
           :old.pano_ini_grupo, /*22*/   
           '', /*23*/
           :old.pano_ini_subgrupo, /*24*/   
           '', /*25*/
           :old.pano_ini_item, /*26*/   
           '', /*27*/
           :old.pano_fin_nivel99, /*28*/   
           '', /*29*/
           :old.pano_fin_grupo, /*30*/   
           '', /*31*/
           :old.pano_fin_subgrupo, /*32*/   
           '', /*33*/
           :old.pano_fin_item, /*34*/   
           '', /*35*/
           :old.qtde_kg_inicial, /*36*/   
           0, /*37*/
           :old.qtde_kg_final, /*38*/   
           0, /*39*/
           :old.data_sai_estq, /*40*/   
           null, /*41*/
           :old.data_ent_estq, /*42*/   
           null, /*43*/
           :old.seq_ordem, /*44*/   
           0, /*45*/
           :old.codigo_rolo, /*46*/   
           0, /*47*/
           :old.rolo_acabado, /*48*/   
           0, /*49*/
           :old.ordem_producao, /*50*/   
           0, /*51*/
           :old.grupo_maquina, /*52*/   
           '', /*53*/
           :old.subgrupo_maquina, /*54*/   
           '', /*55*/
           :old.numero_maquina, /*56*/   
           0, /*57*/
           :old.rolo_confirmado, /*58*/   
           '', /*59*/
           :old.usuario, /*60*/   
           '', /*61*/
           :old.data_hora_conf, /*62*/   
           null, /*63*/
           :old.rolo_conferido, /*64*/   
           '', /*65*/
           :old.estagio_producao, /*66*/   
           0, /*67*/
           :old.rolo_rateado, /*68*/   
           '' /*69*/
         );    
    end;    
 end if;    
end inter_tr_pcpt_025_hist;

/

exec inter_pr_recompile;
