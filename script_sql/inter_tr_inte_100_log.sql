
  CREATE OR REPLACE TRIGGER "INTER_TR_INTE_100_LOG" 
after insert or delete or update
on inte_100
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);   

 if inserting
 then
    begin

        insert into inte_100_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           CODIGO_REGISTRO_OLD,   /*8*/
           CODIGO_REGISTRO_NEW,   /*9*/
           PEDIDO_VENDA    ,   /*11*/
           TECIDO_PECA_OLD,   /*12*/
           TECIDO_PECA_NEW,   /*13*/
           DATA_ENTREGA_OLD,   /*14*/
           DATA_ENTREGA_NEW,   /*15*/
           CLIENTE9_OLD,   /*16*/
           CLIENTE9_NEW,   /*17*/
           CLIENTE4_OLD,   /*18*/
           CLIENTE4_NEW,   /*19*/
           CLIENTE2_OLD,   /*20*/
           CLIENTE2_NEW,   /*21*/
           PEDIDO_CLIENTE_OLD,   /*22*/
           PEDIDO_CLIENTE_NEW,   /*23*/
           COLECAO_TABELA_OLD,   /*24*/
           COLECAO_TABELA_NEW,   /*25*/
           MES_TABELA_OLD,   /*26*/
           MES_TABELA_NEW,   /*27*/
           SEQUENCIA_TABELA_OLD,   /*28*/
           SEQUENCIA_TABELA_NEW,   /*29*/
           CODIGO_MOEDA_OLD,   /*30*/
           CODIGO_MOEDA_NEW,   /*31*/
           COND_PGTO_VENDA_OLD,   /*32*/
           COND_PGTO_VENDA_NEW,   /*33*/
           DESCONTO1_OLD,   /*34*/
           DESCONTO1_NEW,   /*35*/
           DESCONTO2_OLD,   /*36*/
           DESCONTO2_NEW,   /*37*/
           DESCONTO3_OLD,   /*38*/
           DESCONTO3_NEW,   /*39*/
           TRANS_RE_FORNE9_OLD,   /*40*/
           TRANS_RE_FORNE9_NEW,   /*41*/
           TRANS_RE_FORNE4_OLD,   /*42*/
           TRANS_RE_FORNE4_NEW,   /*43*/
           TRANS_RE_FORNE2_OLD,   /*44*/
           TRANS_RE_FORNE2_NEW,   /*45*/
           NAT_OPER_OLD,   /*46*/
           NAT_OPER_NEW,   /*47*/
           NAT_EST_OPER_OLD,   /*48*/
           NAT_EST_OPER_NEW,   /*49*/
           NUMERO_CONTROLE_OLD,   /*50*/
           NUMERO_CONTROLE_NEW,   /*51*/
           TIPO_REGISTRO_OLD,   /*52*/
           TIPO_REGISTRO_NEW,   /*53*/
           OBS1_OLD,   /*54*/
           OBS1_NEW,   /*55*/
           OBS2_OLD,   /*56*/
           OBS2_NEW,   /*57*/
           OBS3_OLD,   /*58*/
           OBS3_NEW,   /*59*/
           OBS4_OLD,   /*60*/
           OBS4_NEW,   /*61*/
           OBS5_OLD,   /*62*/
           OBS5_NEW,   /*63*/
           OBS6_OLD,   /*64*/
           OBS6_NEW,   /*65*/
           OBS7_OLD,   /*66*/
           OBS7_NEW,   /*67*/
           SIT_IMPORTACAO_OLD,   /*68*/
           SIT_IMPORTACAO_NEW,   /*69*/
           LOJA_OLD,   /*70*/
           LOJA_NEW,   /*71*/
           DATA_IMPORTACAO_OLD,   /*72*/
           DATA_IMPORTACAO_NEW,   /*73*/
           COD_REPRE_OLD,   /*74*/
           COD_REPRE_NEW,   /*75*/
           CODIGO_EMPRESA_OLD,   /*76*/
           CODIGO_EMPRESA_NEW,   /*77*/
           DATA_EMIS_VENDA_OLD,   /*78*/
           DATA_EMIS_VENDA_NEW,   /*79*/
           DATA_DIGIT_VENDA_OLD,   /*80*/
           DATA_DIGIT_VENDA_NEW,   /*81*/
           PERC_COMIS_VENDA_OLD,   /*82*/
           PERC_COMIS_VENDA_NEW,   /*83*/
           PERC_DESC_DUPLIC_OLD,   /*84*/
           PERC_DESC_DUPLIC_NEW,   /*85*/
           SEQ_END_ENTREGA_OLD,   /*86*/
           SEQ_END_ENTREGA_NEW,   /*87*/
           SEQ_END_COBRANCA_OLD,   /*88*/
           SEQ_END_COBRANCA_NEW,   /*89*/
           COD_VIA_TRANSP_OLD,   /*90*/
           COD_VIA_TRANSP_NEW,   /*91*/
           TRANS_PV_FORNE9_OLD,   /*92*/
           TRANS_PV_FORNE9_NEW,   /*93*/
           TRANS_PV_FORNE4_OLD,   /*94*/
           TRANS_PV_FORNE4_NEW,   /*95*/
           TRANS_PV_FORNE2_OLD,   /*96*/
           TRANS_PV_FORNE2_NEW,   /*97*/
           COD_BANCO_OLD,   /*98*/
           COD_BANCO_NEW,   /*99*/
           DATA_ARQUIVO_OLD,   /*100*/
           DATA_ARQUIVO_NEW,   /*101*/
           FRETE_VENDA_OLD,   /*102*/
           FRETE_VENDA_NEW,   /*103*/
           NUMERO_SEMANA_OLD,   /*104*/
           NUMERO_SEMANA_NEW,   /*105*/
           NOVO_PEDIDO_VENDA_OLD,   /*106*/
           NOVO_PEDIDO_VENDA_NEW,   /*107*/
           DESCONTO_ITEM1_OLD,   /*108*/
           DESCONTO_ITEM1_NEW,   /*109*/
           DESCONTO_ITEM2_OLD,   /*110*/
           DESCONTO_ITEM2_NEW,   /*111*/
           DESCONTO_ITEM3_OLD,   /*112*/
           DESCONTO_ITEM3_NEW,   /*113*/
           TIPO_COMISSAO_OLD,   /*114*/
           TIPO_COMISSAO_NEW,   /*115*/
           CLASSIFICACAO_PEDIDO_OLD,   /*116*/
           CLASSIFICACAO_PEDIDO_NEW,   /*117*/
           CODIGO_DEPOSITO_OLD,   /*118*/
           CODIGO_DEPOSITO_NEW,   /*119*/
           OBS1_NF_OLD,   /*120*/
           OBS1_NF_NEW,   /*121*/
           OBS2_NF_OLD,   /*122*/
           OBS2_NF_NEW,   /*123*/
           OBS3_NF_OLD,   /*124*/
           OBS3_NF_NEW,   /*125*/
           OBS4_NF_OLD,   /*126*/
           OBS4_NF_NEW,   /*127*/
           OBS5_NF_OLD,   /*128*/
           OBS5_NF_NEW,   /*129*/
           OBS1_PROD_OLD,   /*130*/
           OBS1_PROD_NEW,   /*131*/
           OBS2_PROD_OLD,   /*132*/
           OBS2_PROD_NEW,   /*133*/
           OBS3_PROD_OLD,   /*134*/
           OBS3_PROD_NEW,   /*135*/
           OBS4_PROD_OLD,   /*136*/
           OBS4_PROD_NEW,   /*137*/
           OBS5_PROD_OLD,   /*138*/
           OBS5_PROD_NEW,   /*139*/
           TIPO_PEDIDO_OLD,   /*140*/
           TIPO_PEDIDO_NEW,   /*141*/
           TIPO_PROD_PEDIDO_OLD,   /*142*/
           TIPO_PROD_PEDIDO_NEW,   /*143*/
           EXIGENCIA_REQ_AMBIENTAL_OLD,   /*144*/
           EXIGENCIA_REQ_AMBIENTAL_NEW,   /*145*/
           ESPECIFICAR_REQUISITO_OLD,   /*146*/
           ESPECIFICAR_REQUISITO_NEW,   /*147*/
           ORIGEM_PEDIDO_OLD,   /*148*/
           ORIGEM_PEDIDO_NEW,   /*149*/
           EXECUTA_TRIGGER_OLD,   /*150*/
           EXECUTA_TRIGGER_NEW,   /*151*/
           CRITERIO_PEDIDO_OLD,   /*152*/
           CRITERIO_PEDIDO_NEW,   /*153*/
           ADMINISTRADOR_OLD,   /*154*/
           ADMINISTRADOR_NEW,   /*155*/
           PEDIDO_COTACAO_OLD,   /*156*/
           PEDIDO_COTACAO_NEW,   /*157*/
           ID_PEDIDO_FORCA_VENDAS_OLD,   /*158*/
           ID_PEDIDO_FORCA_VENDAS_NEW,   /*159*/
           CRITERIO_QUALIDADE_OLD,   /*160*/
           CRITERIO_QUALIDADE_NEW,   /*161*/
           DESCONTO_EXTRA_OLD,   /*162*/
           DESCONTO_EXTRA_NEW,   /*163*/
           PRAZO_EXTRA_OLD,   /*164*/
           PRAZO_EXTRA_NEW,   /*165*/
           COND_PGTO_EXTRA_OLD,   /*166*/
           COND_PGTO_EXTRA_NEW,   /*167*/
           COD_LOCAL_OLD,   /*168*/
           COD_LOCAL_NEW,   /*169*/
           COD_CATALOGO_OLD,   /*170*/
           COD_CATALOGO_NEW,   /*171*/
           ACEITA_ANTECIPACAO_OLD,   /*172*/
           ACEITA_ANTECIPACAO_NEW,   /*173*/
           PERMITE_PARCIAL_OLD,   /*174*/
           PERMITE_PARCIAL_NEW,   /*175*/
           COD_PREPOSTO_OLD,   /*176*/
           COD_PREPOSTO_NEW,   /*177*/
           ANA_COMERCIAL_CONC_DESC_OLD,   /*178*/
           ANA_COMERCIAL_CONC_DESC_NEW,   /*179*/
           ANA_COMERCIAL_PRAZO_OLD,   /*180*/
           ANA_COMERCIAL_PRAZO_NEW,   /*181*/
           ANA_COMERCIAL_COMISSAO_OLD,   /*182*/
           ANA_COMERCIAL_COMISSAO_NEW,   /*183*/
           ANA_COMERCIAL_OBS_OLD,   /*184*/
           ANA_COMERCIAL_OBS_NEW,    /*185*/
           OBS_LISTA_CONTEUDO_1_OLD,    /*186*/
           OBS_LISTA_CONTEUDO_1_NEW,    /*187*/
           OBS_LISTA_CONTEUDO_2_OLD,    /*188*/
           OBS_LISTA_CONTEUDO_2_NEW,    /*189*/
           OBS_LISTA_CONTEUDO_3_OLD,    /*190*/
           OBS_LISTA_CONTEUDO_3_NEW,    /*191*/
           COD_TIPO_EMBALAGEM_OLD,    /*192*/
           COD_TIPO_EMBALAGEM_NEW    /*193*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.CODIGO_REGISTRO, /*9*/
           :new.PEDIDO_VENDA, /*11*/
           '',/*12*/
           :new.TECIDO_PECA, /*13*/
           null,/*14*/
           :new.DATA_ENTREGA, /*15*/
           0,/*16*/
           :new.CLIENTE9, /*17*/
           0,/*18*/
           :new.CLIENTE4, /*19*/
           0,/*20*/
           :new.CLIENTE2, /*21*/
           '',/*22*/
           :new.PEDIDO_CLIENTE, /*23*/
           0,/*24*/
           :new.COLECAO_TABELA, /*25*/
           0,/*26*/
           :new.MES_TABELA, /*27*/
           0,/*28*/
           :new.SEQUENCIA_TABELA, /*29*/
           0,/*30*/
           :new.CODIGO_MOEDA, /*31*/
           0,/*32*/
           :new.COND_PGTO_VENDA, /*33*/
           0,/*34*/
           :new.DESCONTO1, /*35*/
           0,/*36*/
           :new.DESCONTO2, /*37*/
           0,/*38*/
           :new.DESCONTO3, /*39*/
           0,/*40*/
           :new.TRANS_RE_FORNE9, /*41*/
           0,/*42*/
           :new.TRANS_RE_FORNE4, /*43*/
           0,/*44*/
           :new.TRANS_RE_FORNE2, /*45*/
           0,/*46*/
           :new.NAT_OPER, /*47*/
           '',/*48*/
           :new.NAT_EST_OPER, /*49*/
           0,/*50*/
           :new.NUMERO_CONTROLE, /*51*/
           0,/*52*/
           :new.TIPO_REGISTRO, /*53*/
           '',/*54*/
           :new.OBS1, /*55*/
           '',/*56*/
           :new.OBS2, /*57*/
           '',/*58*/
           :new.OBS3, /*59*/
           '',/*60*/
           :new.OBS4, /*61*/
           '',/*62*/
           :new.OBS5, /*63*/
           '',/*64*/
           :new.OBS6, /*65*/
           '',/*66*/
           :new.OBS7, /*67*/
           0,/*68*/
           :new.SIT_IMPORTACAO, /*69*/
           0,/*70*/
           :new.LOJA, /*71*/
           null,/*72*/
           :new.DATA_IMPORTACAO, /*73*/
           0,/*74*/
           :new.COD_REPRE, /*75*/
           0,/*76*/
           :new.CODIGO_EMPRESA, /*77*/
           null,/*78*/
           :new.DATA_EMIS_VENDA, /*79*/
           null,/*80*/
           :new.DATA_DIGIT_VENDA, /*81*/
           0,/*82*/
           :new.PERC_COMIS_VENDA, /*83*/
           0,/*84*/
           :new.PERC_DESC_DUPLIC, /*85*/
           0,/*86*/
           :new.SEQ_END_ENTREGA, /*87*/
           0,/*88*/
           :new.SEQ_END_COBRANCA, /*89*/
           0,/*90*/
           :new.COD_VIA_TRANSP, /*91*/
           0,/*92*/
           :new.TRANS_PV_FORNE9, /*93*/
           0,/*94*/
           :new.TRANS_PV_FORNE4, /*95*/
           0,/*96*/
           :new.TRANS_PV_FORNE2, /*97*/
           0,/*98*/
           :new.COD_BANCO, /*99*/
           null,/*100*/
           :new.DATA_ARQUIVO, /*101*/
           0,/*102*/
           :new.FRETE_VENDA, /*103*/
           0,/*104*/
           :new.NUMERO_SEMANA, /*105*/
           0,/*106*/
           :new.NOVO_PEDIDO_VENDA, /*107*/
           0,/*108*/
           :new.DESCONTO_ITEM1, /*109*/
           0,/*110*/
           :new.DESCONTO_ITEM2, /*111*/
           0,/*112*/
           :new.DESCONTO_ITEM3, /*113*/
           0,/*114*/
           :new.TIPO_COMISSAO, /*115*/
           0,/*116*/
           :new.CLASSIFICACAO_PEDIDO, /*117*/
           0,/*118*/
           :new.CODIGO_DEPOSITO, /*119*/
           '',/*120*/
           :new.OBS1_NF, /*121*/
           '',/*122*/
           :new.OBS2_NF, /*123*/
           '',/*124*/
           :new.OBS3_NF, /*125*/
           '',/*126*/
           :new.OBS4_NF, /*127*/
           '',/*128*/
           :new.OBS5_NF, /*129*/
           '',/*130*/
           :new.OBS1_PROD, /*131*/
           '',/*132*/
           :new.OBS2_PROD, /*133*/
           '',/*134*/
           :new.OBS3_PROD, /*135*/
           '',/*136*/
           :new.OBS4_PROD, /*137*/
           '',/*138*/
           :new.OBS5_PROD, /*139*/
           0,/*140*/
           :new.TIPO_PEDIDO, /*141*/
           0,/*142*/
           :new.TIPO_PROD_PEDIDO, /*143*/
           0,/*144*/
           :new.EXIGENCIA_REQUISITO_AMBIENTAL, /*145*/
           '',/*146*/
           :new.ESPECIFICAR_REQUISITO, /*147*/
           0,/*148*/
           :new.ORIGEM_PEDIDO, /*149*/
           0,/*150*/
           :new.EXECUTA_TRIGGER, /*151*/
           0,/*152*/
           :new.CRITERIO_PEDIDO, /*153*/
           0,/*154*/
           :new.ADMINISTRADOR, /*155*/
           0,/*156*/
           :new.PEDIDO_COTACAO, /*157*/
           '',/*158*/
           :new.ID_PEDIDO_FORCA_VENDAS, /*159*/
           0,/*160*/
           :new.CRITERIO_QUALIDADE, /*161*/
           0,/*162*/
           :new.DESCONTO_EXTRA, /*163*/
           0,/*164*/
           :new.PRAZO_EXTRA, /*165*/
           0,/*166*/
           :new.COND_PGTO_EXTRA, /*167*/
           0,/*168*/
           :new.COD_LOCAL, /*169*/
           0,/*170*/
           :new.COD_CATALOGO, /*171*/
           0,/*172*/
           :new.ACEITA_ANTECIPACAO, /*173*/
           0,/*174*/
           :new.PERMITE_PARCIAL, /*175*/
           0,/*176*/
           :new.COD_PREPOSTO, /*177*/
           0,/*178*/
           :new.ANA_COMERCIAL_CONCESSAO_DESC, /*179*/
           0,/*180*/
           :new.ANA_COMERCIAL_PRAZO, /*181*/
           0,/*182*/
           :new.ANA_COMERCIAL_COMISSAO, /*183*/
           '',/*184*/
           :new.ANA_COMERCIAL_OBS, /*185*/
           ' ',    /*186*/
           :new.OBS_LISTA_CONTEUDO_1,    /*187*/
           ' ',    /*188*/
           :new.OBS_LISTA_CONTEUDO_2,    /*189*/
           ' ',    /*190*/
           :new.OBS_LISTA_CONTEUDO_3,    /*191*/
           0,    /*192*/
           :new.COD_TIPO_EMBALAGEM    /*193*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into inte_100_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           CODIGO_REGISTRO_OLD, /*8*/
           CODIGO_REGISTRO_NEW, /*9*/
           PEDIDO_VENDA, /*11*/
           TECIDO_PECA_OLD, /*12*/
           TECIDO_PECA_NEW, /*13*/
           DATA_ENTREGA_OLD, /*14*/
           DATA_ENTREGA_NEW, /*15*/
           CLIENTE9_OLD, /*16*/
           CLIENTE9_NEW, /*17*/
           CLIENTE4_OLD, /*18*/
           CLIENTE4_NEW, /*19*/
           CLIENTE2_OLD, /*20*/
           CLIENTE2_NEW, /*21*/
           PEDIDO_CLIENTE_OLD, /*22*/
           PEDIDO_CLIENTE_NEW, /*23*/
           COLECAO_TABELA_OLD, /*24*/
           COLECAO_TABELA_NEW, /*25*/
           MES_TABELA_OLD, /*26*/
           MES_TABELA_NEW, /*27*/
           SEQUENCIA_TABELA_OLD, /*28*/
           SEQUENCIA_TABELA_NEW, /*29*/
           CODIGO_MOEDA_OLD, /*30*/
           CODIGO_MOEDA_NEW, /*31*/
           COND_PGTO_VENDA_OLD, /*32*/
           COND_PGTO_VENDA_NEW, /*33*/
           DESCONTO1_OLD, /*34*/
           DESCONTO1_NEW, /*35*/
           DESCONTO2_OLD, /*36*/
           DESCONTO2_NEW, /*37*/
           DESCONTO3_OLD, /*38*/
           DESCONTO3_NEW, /*39*/
           TRANS_RE_FORNE9_OLD, /*40*/
           TRANS_RE_FORNE9_NEW, /*41*/
           TRANS_RE_FORNE4_OLD, /*42*/
           TRANS_RE_FORNE4_NEW, /*43*/
           TRANS_RE_FORNE2_OLD, /*44*/
           TRANS_RE_FORNE2_NEW, /*45*/
           NAT_OPER_OLD, /*46*/
           NAT_OPER_NEW, /*47*/
           NAT_EST_OPER_OLD, /*48*/
           NAT_EST_OPER_NEW, /*49*/
           NUMERO_CONTROLE_OLD, /*50*/
           NUMERO_CONTROLE_NEW, /*51*/
           TIPO_REGISTRO_OLD, /*52*/
           TIPO_REGISTRO_NEW, /*53*/
           OBS1_OLD, /*54*/
           OBS1_NEW, /*55*/
           OBS2_OLD, /*56*/
           OBS2_NEW, /*57*/
           OBS3_OLD, /*58*/
           OBS3_NEW, /*59*/
           OBS4_OLD, /*60*/
           OBS4_NEW, /*61*/
           OBS5_OLD, /*62*/
           OBS5_NEW, /*63*/
           OBS6_OLD, /*64*/
           OBS6_NEW, /*65*/
           OBS7_OLD, /*66*/
           OBS7_NEW, /*67*/
           SIT_IMPORTACAO_OLD, /*68*/
           SIT_IMPORTACAO_NEW, /*69*/
           LOJA_OLD, /*70*/
           LOJA_NEW, /*71*/
           DATA_IMPORTACAO_OLD, /*72*/
           DATA_IMPORTACAO_NEW, /*73*/
           COD_REPRE_OLD, /*74*/
           COD_REPRE_NEW, /*75*/
           CODIGO_EMPRESA_OLD, /*76*/
           CODIGO_EMPRESA_NEW, /*77*/
           DATA_EMIS_VENDA_OLD, /*78*/
           DATA_EMIS_VENDA_NEW, /*79*/
           DATA_DIGIT_VENDA_OLD, /*80*/
           DATA_DIGIT_VENDA_NEW, /*81*/
           PERC_COMIS_VENDA_OLD, /*82*/
           PERC_COMIS_VENDA_NEW, /*83*/
           PERC_DESC_DUPLIC_OLD, /*84*/
           PERC_DESC_DUPLIC_NEW, /*85*/
           SEQ_END_ENTREGA_OLD, /*86*/
           SEQ_END_ENTREGA_NEW, /*87*/
           SEQ_END_COBRANCA_OLD, /*88*/
           SEQ_END_COBRANCA_NEW, /*89*/
           COD_VIA_TRANSP_OLD, /*90*/
           COD_VIA_TRANSP_NEW, /*91*/
           TRANS_PV_FORNE9_OLD, /*92*/
           TRANS_PV_FORNE9_NEW, /*93*/
           TRANS_PV_FORNE4_OLD, /*94*/
           TRANS_PV_FORNE4_NEW, /*95*/
           TRANS_PV_FORNE2_OLD, /*96*/
           TRANS_PV_FORNE2_NEW, /*97*/
           COD_BANCO_OLD, /*98*/
           COD_BANCO_NEW, /*99*/
           DATA_ARQUIVO_OLD, /*100*/
           DATA_ARQUIVO_NEW, /*101*/
           FRETE_VENDA_OLD, /*102*/
           FRETE_VENDA_NEW, /*103*/
           NUMERO_SEMANA_OLD, /*104*/
           NUMERO_SEMANA_NEW, /*105*/
           NOVO_PEDIDO_VENDA_OLD, /*106*/
           NOVO_PEDIDO_VENDA_NEW, /*107*/
           DESCONTO_ITEM1_OLD, /*108*/
           DESCONTO_ITEM1_NEW, /*109*/
           DESCONTO_ITEM2_OLD, /*110*/
           DESCONTO_ITEM2_NEW, /*111*/
           DESCONTO_ITEM3_OLD, /*112*/
           DESCONTO_ITEM3_NEW, /*113*/
           TIPO_COMISSAO_OLD, /*114*/
           TIPO_COMISSAO_NEW, /*115*/
           CLASSIFICACAO_PEDIDO_OLD, /*116*/
           CLASSIFICACAO_PEDIDO_NEW, /*117*/
           CODIGO_DEPOSITO_OLD, /*118*/
           CODIGO_DEPOSITO_NEW, /*119*/
           OBS1_NF_OLD, /*120*/
           OBS1_NF_NEW, /*121*/
           OBS2_NF_OLD, /*122*/
           OBS2_NF_NEW, /*123*/
           OBS3_NF_OLD, /*124*/
           OBS3_NF_NEW, /*125*/
           OBS4_NF_OLD, /*126*/
           OBS4_NF_NEW, /*127*/
           OBS5_NF_OLD, /*128*/
           OBS5_NF_NEW, /*129*/
           OBS1_PROD_OLD, /*130*/
           OBS1_PROD_NEW, /*131*/
           OBS2_PROD_OLD, /*132*/
           OBS2_PROD_NEW, /*133*/
           OBS3_PROD_OLD, /*134*/
           OBS3_PROD_NEW, /*135*/
           OBS4_PROD_OLD, /*136*/
           OBS4_PROD_NEW, /*137*/
           OBS5_PROD_OLD, /*138*/
           OBS5_PROD_NEW, /*139*/
           TIPO_PEDIDO_OLD, /*140*/
           TIPO_PEDIDO_NEW, /*141*/
           TIPO_PROD_PEDIDO_OLD, /*142*/
           TIPO_PROD_PEDIDO_NEW, /*143*/
           EXIGENCIA_REQ_AMBIENTAL_OLD, /*144*/
           EXIGENCIA_REQ_AMBIENTAL_NEW, /*145*/
           ESPECIFICAR_REQUISITO_OLD, /*146*/
           ESPECIFICAR_REQUISITO_NEW, /*147*/
           ORIGEM_PEDIDO_OLD, /*148*/
           ORIGEM_PEDIDO_NEW, /*149*/
           EXECUTA_TRIGGER_OLD, /*150*/
           EXECUTA_TRIGGER_NEW, /*151*/
           CRITERIO_PEDIDO_OLD, /*152*/
           CRITERIO_PEDIDO_NEW, /*153*/
           ADMINISTRADOR_OLD, /*154*/
           ADMINISTRADOR_NEW, /*155*/
           PEDIDO_COTACAO_OLD, /*156*/
           PEDIDO_COTACAO_NEW, /*157*/
           ID_PEDIDO_FORCA_VENDAS_OLD, /*158*/
           ID_PEDIDO_FORCA_VENDAS_NEW, /*159*/
           CRITERIO_QUALIDADE_OLD, /*160*/
           CRITERIO_QUALIDADE_NEW, /*161*/
           DESCONTO_EXTRA_OLD, /*162*/
           DESCONTO_EXTRA_NEW, /*163*/
           PRAZO_EXTRA_OLD, /*164*/
           PRAZO_EXTRA_NEW, /*165*/
           COND_PGTO_EXTRA_OLD, /*166*/
           COND_PGTO_EXTRA_NEW, /*167*/
           COD_LOCAL_OLD, /*168*/
           COD_LOCAL_NEW, /*169*/
           COD_CATALOGO_OLD, /*170*/
           COD_CATALOGO_NEW, /*171*/
           ACEITA_ANTECIPACAO_OLD, /*172*/
           ACEITA_ANTECIPACAO_NEW, /*173*/
           PERMITE_PARCIAL_OLD, /*174*/
           PERMITE_PARCIAL_NEW, /*175*/
           COD_PREPOSTO_OLD, /*176*/
           COD_PREPOSTO_NEW, /*177*/
           ANA_COMERCIAL_CONC_DESC_OLD, /*178*/
           ANA_COMERCIAL_CONC_DESC_NEW, /*179*/
           ANA_COMERCIAL_PRAZO_OLD, /*180*/
           ANA_COMERCIAL_PRAZO_NEW, /*181*/
           ANA_COMERCIAL_COMISSAO_OLD, /*182*/
           ANA_COMERCIAL_COMISSAO_NEW, /*183*/
           ANA_COMERCIAL_OBS_OLD, /*184*/
           ANA_COMERCIAL_OBS_NEW,  /*185*/
           OBS_LISTA_CONTEUDO_1_OLD,    /*186*/
           OBS_LISTA_CONTEUDO_1_NEW,    /*187*/
           OBS_LISTA_CONTEUDO_2_OLD,    /*188*/
           OBS_LISTA_CONTEUDO_2_NEW,    /*189*/
           OBS_LISTA_CONTEUDO_3_OLD,    /*190*/
           OBS_LISTA_CONTEUDO_3_NEW,    /*191*/
           COD_TIPO_EMBALAGEM_OLD,    /*192*/
           COD_TIPO_EMBALAGEM_NEW    /*193*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.CODIGO_REGISTRO,  /*8*/
           :new.CODIGO_REGISTRO, /*9*/
           :new.PEDIDO_VENDA, /*11*/
           :old.TECIDO_PECA,  /*12*/
           :new.TECIDO_PECA, /*13*/
           :old.DATA_ENTREGA,  /*14*/
           :new.DATA_ENTREGA, /*15*/
           :old.CLIENTE9,  /*16*/
           :new.CLIENTE9, /*17*/
           :old.CLIENTE4,  /*18*/
           :new.CLIENTE4, /*19*/
           :old.CLIENTE2,  /*20*/
           :new.CLIENTE2, /*21*/
           :old.PEDIDO_CLIENTE,  /*22*/
           :new.PEDIDO_CLIENTE, /*23*/
           :old.COLECAO_TABELA,  /*24*/
           :new.COLECAO_TABELA, /*25*/
           :old.MES_TABELA,  /*26*/
           :new.MES_TABELA, /*27*/
           :old.SEQUENCIA_TABELA,  /*28*/
           :new.SEQUENCIA_TABELA, /*29*/
           :old.CODIGO_MOEDA,  /*30*/
           :new.CODIGO_MOEDA, /*31*/
           :old.COND_PGTO_VENDA,  /*32*/
           :new.COND_PGTO_VENDA, /*33*/
           :old.DESCONTO1,  /*34*/
           :new.DESCONTO1, /*35*/
           :old.DESCONTO2,  /*36*/
           :new.DESCONTO2, /*37*/
           :old.DESCONTO3,  /*38*/
           :new.DESCONTO3, /*39*/
           :old.TRANS_RE_FORNE9,  /*40*/
           :new.TRANS_RE_FORNE9, /*41*/
           :old.TRANS_RE_FORNE4,  /*42*/
           :new.TRANS_RE_FORNE4, /*43*/
           :old.TRANS_RE_FORNE2,  /*44*/
           :new.TRANS_RE_FORNE2, /*45*/
           :old.NAT_OPER,  /*46*/
           :new.NAT_OPER, /*47*/
           :old.NAT_EST_OPER,  /*48*/
           :new.NAT_EST_OPER, /*49*/
           :old.NUMERO_CONTROLE,  /*50*/
           :new.NUMERO_CONTROLE, /*51*/
           :old.TIPO_REGISTRO,  /*52*/
           :new.TIPO_REGISTRO, /*53*/
           :old.OBS1,  /*54*/
           :new.OBS1, /*55*/
           :old.OBS2,  /*56*/
           :new.OBS2, /*57*/
           :old.OBS3,  /*58*/
           :new.OBS3, /*59*/
           :old.OBS4,  /*60*/
           :new.OBS4, /*61*/
           :old.OBS5,  /*62*/
           :new.OBS5, /*63*/
           :old.OBS6,  /*64*/
           :new.OBS6, /*65*/
           :old.OBS7,  /*66*/
           :new.OBS7, /*67*/
           :old.SIT_IMPORTACAO,  /*68*/
           :new.SIT_IMPORTACAO, /*69*/
           :old.LOJA,  /*70*/
           :new.LOJA, /*71*/
           :old.DATA_IMPORTACAO,  /*72*/
           :new.DATA_IMPORTACAO, /*73*/
           :old.COD_REPRE,  /*74*/
           :new.COD_REPRE, /*75*/
           :old.CODIGO_EMPRESA,  /*76*/
           :new.CODIGO_EMPRESA, /*77*/
           :old.DATA_EMIS_VENDA,  /*78*/
           :new.DATA_EMIS_VENDA, /*79*/
           :old.DATA_DIGIT_VENDA,  /*80*/
           :new.DATA_DIGIT_VENDA, /*81*/
           :old.PERC_COMIS_VENDA,  /*82*/
           :new.PERC_COMIS_VENDA, /*83*/
           :old.PERC_DESC_DUPLIC,  /*84*/
           :new.PERC_DESC_DUPLIC, /*85*/
           :old.SEQ_END_ENTREGA,  /*86*/
           :new.SEQ_END_ENTREGA, /*87*/
           :old.SEQ_END_COBRANCA,  /*88*/
           :new.SEQ_END_COBRANCA, /*89*/
           :old.COD_VIA_TRANSP,  /*90*/
           :new.COD_VIA_TRANSP, /*91*/
           :old.TRANS_PV_FORNE9,  /*92*/
           :new.TRANS_PV_FORNE9, /*93*/
           :old.TRANS_PV_FORNE4,  /*94*/
           :new.TRANS_PV_FORNE4, /*95*/
           :old.TRANS_PV_FORNE2,  /*96*/
           :new.TRANS_PV_FORNE2, /*97*/
           :old.COD_BANCO,  /*98*/
           :new.COD_BANCO, /*99*/
           :old.DATA_ARQUIVO,  /*100*/
           :new.DATA_ARQUIVO, /*101*/
           :old.FRETE_VENDA,  /*102*/
           :new.FRETE_VENDA, /*103*/
           :old.NUMERO_SEMANA,  /*104*/
           :new.NUMERO_SEMANA, /*105*/
           :old.NOVO_PEDIDO_VENDA,  /*106*/
           :new.NOVO_PEDIDO_VENDA, /*107*/
           :old.DESCONTO_ITEM1,  /*108*/
           :new.DESCONTO_ITEM1, /*109*/
           :old.DESCONTO_ITEM2,  /*110*/
           :new.DESCONTO_ITEM2, /*111*/
           :old.DESCONTO_ITEM3,  /*112*/
           :new.DESCONTO_ITEM3, /*113*/
           :old.TIPO_COMISSAO,  /*114*/
           :new.TIPO_COMISSAO, /*115*/
           :old.CLASSIFICACAO_PEDIDO,  /*116*/
           :new.CLASSIFICACAO_PEDIDO, /*117*/
           :old.CODIGO_DEPOSITO,  /*118*/
           :new.CODIGO_DEPOSITO, /*119*/
           :old.OBS1_NF,  /*120*/
           :new.OBS1_NF, /*121*/
           :old.OBS2_NF,  /*122*/
           :new.OBS2_NF, /*123*/
           :old.OBS3_NF,  /*124*/
           :new.OBS3_NF, /*125*/
           :old.OBS4_NF,  /*126*/
           :new.OBS4_NF, /*127*/
           :old.OBS5_NF,  /*128*/
           :new.OBS5_NF, /*129*/
           :old.OBS1_PROD,  /*130*/
           :new.OBS1_PROD, /*131*/
           :old.OBS2_PROD,  /*132*/
           :new.OBS2_PROD, /*133*/
           :old.OBS3_PROD,  /*134*/
           :new.OBS3_PROD, /*135*/
           :old.OBS4_PROD,  /*136*/
           :new.OBS4_PROD, /*137*/
           :old.OBS5_PROD,  /*138*/
           :new.OBS5_PROD, /*139*/
           :old.TIPO_PEDIDO,  /*140*/
           :new.TIPO_PEDIDO, /*141*/
           :old.TIPO_PROD_PEDIDO,  /*142*/
           :new.TIPO_PROD_PEDIDO, /*143*/
           :old.EXIGENCIA_REQUISITO_AMBIENTAL,  /*144*/
           :new.EXIGENCIA_REQUISITO_AMBIENTAL, /*145*/
           :old.ESPECIFICAR_REQUISITO,  /*146*/
           :new.ESPECIFICAR_REQUISITO, /*147*/
           :old.ORIGEM_PEDIDO,  /*148*/
           :new.ORIGEM_PEDIDO, /*149*/
           :old.EXECUTA_TRIGGER,  /*150*/
           :new.EXECUTA_TRIGGER, /*151*/
           :old.CRITERIO_PEDIDO,  /*152*/
           :new.CRITERIO_PEDIDO, /*153*/
           :old.ADMINISTRADOR,  /*154*/
           :new.ADMINISTRADOR, /*155*/
           :old.PEDIDO_COTACAO,  /*156*/
           :new.PEDIDO_COTACAO, /*157*/
           :old.ID_PEDIDO_FORCA_VENDAS,  /*158*/
           :new.ID_PEDIDO_FORCA_VENDAS, /*159*/
           :old.CRITERIO_QUALIDADE,  /*160*/
           :new.CRITERIO_QUALIDADE, /*161*/
           :old.DESCONTO_EXTRA,  /*162*/
           :new.DESCONTO_EXTRA, /*163*/
           :old.PRAZO_EXTRA,  /*164*/
           :new.PRAZO_EXTRA, /*165*/
           :old.COND_PGTO_EXTRA,  /*166*/
           :new.COND_PGTO_EXTRA, /*167*/
           :old.COD_LOCAL,  /*168*/
           :new.COD_LOCAL, /*169*/
           :old.COD_CATALOGO,  /*170*/
           :new.COD_CATALOGO, /*171*/
           :old.ACEITA_ANTECIPACAO,  /*172*/
           :new.ACEITA_ANTECIPACAO, /*173*/
           :old.PERMITE_PARCIAL,  /*174*/
           :new.PERMITE_PARCIAL, /*175*/
           :old.COD_PREPOSTO,  /*176*/
           :new.COD_PREPOSTO, /*177*/
           :old.ANA_COMERCIAL_CONCESSAO_DESC,  /*178*/
           :new.ANA_COMERCIAL_CONCESSAO_DESC, /*179*/
           :old.ANA_COMERCIAL_PRAZO,  /*180*/
           :new.ANA_COMERCIAL_PRAZO, /*181*/
           :old.ANA_COMERCIAL_COMISSAO,  /*182*/
           :new.ANA_COMERCIAL_COMISSAO, /*183*/
           :old.ANA_COMERCIAL_OBS,  /*184*/
           :new.ANA_COMERCIAL_OBS,  /*185*/
           :old.OBS_LISTA_CONTEUDO_1,    /*186*/
           :new.OBS_LISTA_CONTEUDO_1,    /*187*/
           :old.OBS_LISTA_CONTEUDO_2,    /*188*/
           :new.OBS_LISTA_CONTEUDO_2,    /*189*/
           :old.OBS_LISTA_CONTEUDO_3,    /*190*/
           :new.OBS_LISTA_CONTEUDO_3,    /*191*/
           :old.COD_TIPO_EMBALAGEM,    /*192*/
           :new.COD_TIPO_EMBALAGEM    /*193*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into inte_100_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           CODIGO_REGISTRO_OLD, /*8*/
           CODIGO_REGISTRO_NEW, /*9*/
           PEDIDO_VENDA, /*11*/
           TECIDO_PECA_OLD, /*12*/
           TECIDO_PECA_NEW, /*13*/
           DATA_ENTREGA_OLD, /*14*/
           DATA_ENTREGA_NEW, /*15*/
           CLIENTE9_OLD, /*16*/
           CLIENTE9_NEW, /*17*/
           CLIENTE4_OLD, /*18*/
           CLIENTE4_NEW, /*19*/
           CLIENTE2_OLD, /*20*/
           CLIENTE2_NEW, /*21*/
           PEDIDO_CLIENTE_OLD, /*22*/
           PEDIDO_CLIENTE_NEW, /*23*/
           COLECAO_TABELA_OLD, /*24*/
           COLECAO_TABELA_NEW, /*25*/
           MES_TABELA_OLD, /*26*/
           MES_TABELA_NEW, /*27*/
           SEQUENCIA_TABELA_OLD, /*28*/
           SEQUENCIA_TABELA_NEW, /*29*/
           CODIGO_MOEDA_OLD, /*30*/
           CODIGO_MOEDA_NEW, /*31*/
           COND_PGTO_VENDA_OLD, /*32*/
           COND_PGTO_VENDA_NEW, /*33*/
           DESCONTO1_OLD, /*34*/
           DESCONTO1_NEW, /*35*/
           DESCONTO2_OLD, /*36*/
           DESCONTO2_NEW, /*37*/
           DESCONTO3_OLD, /*38*/
           DESCONTO3_NEW, /*39*/
           TRANS_RE_FORNE9_OLD, /*40*/
           TRANS_RE_FORNE9_NEW, /*41*/
           TRANS_RE_FORNE4_OLD, /*42*/
           TRANS_RE_FORNE4_NEW, /*43*/
           TRANS_RE_FORNE2_OLD, /*44*/
           TRANS_RE_FORNE2_NEW, /*45*/
           NAT_OPER_OLD, /*46*/
           NAT_OPER_NEW, /*47*/
           NAT_EST_OPER_OLD, /*48*/
           NAT_EST_OPER_NEW, /*49*/
           NUMERO_CONTROLE_OLD, /*50*/
           NUMERO_CONTROLE_NEW, /*51*/
           TIPO_REGISTRO_OLD, /*52*/
           TIPO_REGISTRO_NEW, /*53*/
           OBS1_OLD, /*54*/
           OBS1_NEW, /*55*/
           OBS2_OLD, /*56*/
           OBS2_NEW, /*57*/
           OBS3_OLD, /*58*/
           OBS3_NEW, /*59*/
           OBS4_OLD, /*60*/
           OBS4_NEW, /*61*/
           OBS5_OLD, /*62*/
           OBS5_NEW, /*63*/
           OBS6_OLD, /*64*/
           OBS6_NEW, /*65*/
           OBS7_OLD, /*66*/
           OBS7_NEW, /*67*/
           SIT_IMPORTACAO_OLD, /*68*/
           SIT_IMPORTACAO_NEW, /*69*/
           LOJA_OLD, /*70*/
           LOJA_NEW, /*71*/
           DATA_IMPORTACAO_OLD, /*72*/
           DATA_IMPORTACAO_NEW, /*73*/
           COD_REPRE_OLD, /*74*/
           COD_REPRE_NEW, /*75*/
           CODIGO_EMPRESA_OLD, /*76*/
           CODIGO_EMPRESA_NEW, /*77*/
           DATA_EMIS_VENDA_OLD, /*78*/
           DATA_EMIS_VENDA_NEW, /*79*/
           DATA_DIGIT_VENDA_OLD, /*80*/
           DATA_DIGIT_VENDA_NEW, /*81*/
           PERC_COMIS_VENDA_OLD, /*82*/
           PERC_COMIS_VENDA_NEW, /*83*/
           PERC_DESC_DUPLIC_OLD, /*84*/
           PERC_DESC_DUPLIC_NEW, /*85*/
           SEQ_END_ENTREGA_OLD, /*86*/
           SEQ_END_ENTREGA_NEW, /*87*/
           SEQ_END_COBRANCA_OLD, /*88*/
           SEQ_END_COBRANCA_NEW, /*89*/
           COD_VIA_TRANSP_OLD, /*90*/
           COD_VIA_TRANSP_NEW, /*91*/
           TRANS_PV_FORNE9_OLD, /*92*/
           TRANS_PV_FORNE9_NEW, /*93*/
           TRANS_PV_FORNE4_OLD, /*94*/
           TRANS_PV_FORNE4_NEW, /*95*/
           TRANS_PV_FORNE2_OLD, /*96*/
           TRANS_PV_FORNE2_NEW, /*97*/
           COD_BANCO_OLD, /*98*/
           COD_BANCO_NEW, /*99*/
           DATA_ARQUIVO_OLD, /*100*/
           DATA_ARQUIVO_NEW, /*101*/
           FRETE_VENDA_OLD, /*102*/
           FRETE_VENDA_NEW, /*103*/
           NUMERO_SEMANA_OLD, /*104*/
           NUMERO_SEMANA_NEW, /*105*/
           NOVO_PEDIDO_VENDA_OLD, /*106*/
           NOVO_PEDIDO_VENDA_NEW, /*107*/
           DESCONTO_ITEM1_OLD, /*108*/
           DESCONTO_ITEM1_NEW, /*109*/
           DESCONTO_ITEM2_OLD, /*110*/
           DESCONTO_ITEM2_NEW, /*111*/
           DESCONTO_ITEM3_OLD, /*112*/
           DESCONTO_ITEM3_NEW, /*113*/
           TIPO_COMISSAO_OLD, /*114*/
           TIPO_COMISSAO_NEW, /*115*/
           CLASSIFICACAO_PEDIDO_OLD, /*116*/
           CLASSIFICACAO_PEDIDO_NEW, /*117*/
           CODIGO_DEPOSITO_OLD, /*118*/
           CODIGO_DEPOSITO_NEW, /*119*/
           OBS1_NF_OLD, /*120*/
           OBS1_NF_NEW, /*121*/
           OBS2_NF_OLD, /*122*/
           OBS2_NF_NEW, /*123*/
           OBS3_NF_OLD, /*124*/
           OBS3_NF_NEW, /*125*/
           OBS4_NF_OLD, /*126*/
           OBS4_NF_NEW, /*127*/
           OBS5_NF_OLD, /*128*/
           OBS5_NF_NEW, /*129*/
           OBS1_PROD_OLD, /*130*/
           OBS1_PROD_NEW, /*131*/
           OBS2_PROD_OLD, /*132*/
           OBS2_PROD_NEW, /*133*/
           OBS3_PROD_OLD, /*134*/
           OBS3_PROD_NEW, /*135*/
           OBS4_PROD_OLD, /*136*/
           OBS4_PROD_NEW, /*137*/
           OBS5_PROD_OLD, /*138*/
           OBS5_PROD_NEW, /*139*/
           TIPO_PEDIDO_OLD, /*140*/
           TIPO_PEDIDO_NEW, /*141*/
           TIPO_PROD_PEDIDO_OLD, /*142*/
           TIPO_PROD_PEDIDO_NEW, /*143*/
           EXIGENCIA_REQ_AMBIENTAL_OLD, /*144*/
           EXIGENCIA_REQ_AMBIENTAL_NEW, /*145*/
           ESPECIFICAR_REQUISITO_OLD, /*146*/
           ESPECIFICAR_REQUISITO_NEW, /*147*/
           ORIGEM_PEDIDO_OLD, /*148*/
           ORIGEM_PEDIDO_NEW, /*149*/
           EXECUTA_TRIGGER_OLD, /*150*/
           EXECUTA_TRIGGER_NEW, /*151*/
           CRITERIO_PEDIDO_OLD, /*152*/
           CRITERIO_PEDIDO_NEW, /*153*/
           ADMINISTRADOR_OLD, /*154*/
           ADMINISTRADOR_NEW, /*155*/
           PEDIDO_COTACAO_OLD, /*156*/
           PEDIDO_COTACAO_NEW, /*157*/
           ID_PEDIDO_FORCA_VENDAS_OLD, /*158*/
           ID_PEDIDO_FORCA_VENDAS_NEW, /*159*/
           CRITERIO_QUALIDADE_OLD, /*160*/
           CRITERIO_QUALIDADE_NEW, /*161*/
           DESCONTO_EXTRA_OLD, /*162*/
           DESCONTO_EXTRA_NEW, /*163*/
           PRAZO_EXTRA_OLD, /*164*/
           PRAZO_EXTRA_NEW, /*165*/
           COND_PGTO_EXTRA_OLD, /*166*/
           COND_PGTO_EXTRA_NEW, /*167*/
           COD_LOCAL_OLD, /*168*/
           COD_LOCAL_NEW, /*169*/
           COD_CATALOGO_OLD, /*170*/
           COD_CATALOGO_NEW, /*171*/
           ACEITA_ANTECIPACAO_OLD, /*172*/
           ACEITA_ANTECIPACAO_NEW, /*173*/
           PERMITE_PARCIAL_OLD, /*174*/
           PERMITE_PARCIAL_NEW, /*175*/
           COD_PREPOSTO_OLD, /*176*/
           COD_PREPOSTO_NEW, /*177*/
           ANA_COMERCIAL_CONC_DESC_OLD, /*178*/
           ANA_COMERCIAL_CONC_DESC_NEW, /*179*/
           ANA_COMERCIAL_PRAZO_OLD, /*180*/
           ANA_COMERCIAL_PRAZO_NEW, /*181*/
           ANA_COMERCIAL_COMISSAO_OLD, /*182*/
           ANA_COMERCIAL_COMISSAO_NEW, /*183*/
           ANA_COMERCIAL_OBS_OLD, /*184*/
           ANA_COMERCIAL_OBS_NEW, /*185*/
           OBS_LISTA_CONTEUDO_1_OLD,    /*186*/
           OBS_LISTA_CONTEUDO_1_NEW,    /*187*/
           OBS_LISTA_CONTEUDO_2_OLD,    /*188*/
           OBS_LISTA_CONTEUDO_2_NEW,    /*189*/
           OBS_LISTA_CONTEUDO_3_OLD,    /*190*/
           OBS_LISTA_CONTEUDO_3_NEW,    /*191*/
           COD_TIPO_EMBALAGEM_OLD,    /*192*/
           COD_TIPO_EMBALAGEM_NEW    /*193*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.CODIGO_REGISTRO, /*8*/
           0, /*9*/
           :old.PEDIDO_VENDA, /*11*/
           :old.TECIDO_PECA, /*12*/
           '', /*13*/
           :old.DATA_ENTREGA, /*14*/
           null, /*15*/
           :old.CLIENTE9, /*16*/
           0, /*17*/
           :old.CLIENTE4, /*18*/
           0, /*19*/
           :old.CLIENTE2, /*20*/
           0, /*21*/
           :old.PEDIDO_CLIENTE, /*22*/
           '', /*23*/
           :old.COLECAO_TABELA, /*24*/
           0, /*25*/
           :old.MES_TABELA, /*26*/
           0, /*27*/
           :old.SEQUENCIA_TABELA, /*28*/
           0, /*29*/
           :old.CODIGO_MOEDA, /*30*/
           0, /*31*/
           :old.COND_PGTO_VENDA, /*32*/
           0, /*33*/
           :old.DESCONTO1, /*34*/
           0, /*35*/
           :old.DESCONTO2, /*36*/
           0, /*37*/
           :old.DESCONTO3, /*38*/
           0, /*39*/
           :old.TRANS_RE_FORNE9, /*40*/
           0, /*41*/
           :old.TRANS_RE_FORNE4, /*42*/
           0, /*43*/
           :old.TRANS_RE_FORNE2, /*44*/
           0, /*45*/
           :old.NAT_OPER, /*46*/
           0, /*47*/
           :old.NAT_EST_OPER, /*48*/
           '', /*49*/
           :old.NUMERO_CONTROLE, /*50*/
           0, /*51*/
           :old.TIPO_REGISTRO, /*52*/
           0, /*53*/
           :old.OBS1, /*54*/
           '', /*55*/
           :old.OBS2, /*56*/
           '', /*57*/
           :old.OBS3, /*58*/
           '', /*59*/
           :old.OBS4, /*60*/
           '', /*61*/
           :old.OBS5, /*62*/
           '', /*63*/
           :old.OBS6, /*64*/
           '', /*65*/
           :old.OBS7, /*66*/
           '', /*67*/
           :old.SIT_IMPORTACAO, /*68*/
           0, /*69*/
           :old.LOJA, /*70*/
           0, /*71*/
           :old.DATA_IMPORTACAO, /*72*/
           null, /*73*/
           :old.COD_REPRE, /*74*/
           0, /*75*/
           :old.CODIGO_EMPRESA, /*76*/
           0, /*77*/
           :old.DATA_EMIS_VENDA, /*78*/
           null, /*79*/
           :old.DATA_DIGIT_VENDA, /*80*/
           null, /*81*/
           :old.PERC_COMIS_VENDA, /*82*/
           0, /*83*/
           :old.PERC_DESC_DUPLIC, /*84*/
           0, /*85*/
           :old.SEQ_END_ENTREGA, /*86*/
           0, /*87*/
           :old.SEQ_END_COBRANCA, /*88*/
           0, /*89*/
           :old.COD_VIA_TRANSP, /*90*/
           0, /*91*/
           :old.TRANS_PV_FORNE9, /*92*/
           0, /*93*/
           :old.TRANS_PV_FORNE4, /*94*/
           0, /*95*/
           :old.TRANS_PV_FORNE2, /*96*/
           0, /*97*/
           :old.COD_BANCO, /*98*/
           0, /*99*/
           :old.DATA_ARQUIVO, /*100*/
           null, /*101*/
           :old.FRETE_VENDA, /*102*/
           0, /*103*/
           :old.NUMERO_SEMANA, /*104*/
           0, /*105*/
           :old.NOVO_PEDIDO_VENDA, /*106*/
           0, /*107*/
           :old.DESCONTO_ITEM1, /*108*/
           0, /*109*/
           :old.DESCONTO_ITEM2, /*110*/
           0, /*111*/
           :old.DESCONTO_ITEM3, /*112*/
           0, /*113*/
           :old.TIPO_COMISSAO, /*114*/
           0, /*115*/
           :old.CLASSIFICACAO_PEDIDO, /*116*/
           0, /*117*/
           :old.CODIGO_DEPOSITO, /*118*/
           0, /*119*/
           :old.OBS1_NF, /*120*/
           '', /*121*/
           :old.OBS2_NF, /*122*/
           '', /*123*/
           :old.OBS3_NF, /*124*/
           '', /*125*/
           :old.OBS4_NF, /*126*/
           '', /*127*/
           :old.OBS5_NF, /*128*/
           '', /*129*/
           :old.OBS1_PROD, /*130*/
           '', /*131*/
           :old.OBS2_PROD, /*132*/
           '', /*133*/
           :old.OBS3_PROD, /*134*/
           '', /*135*/
           :old.OBS4_PROD, /*136*/
           '', /*137*/
           :old.OBS5_PROD, /*138*/
           '', /*139*/
           :old.TIPO_PEDIDO, /*140*/
           0, /*141*/
           :old.TIPO_PROD_PEDIDO, /*142*/
           0, /*143*/
           :old.EXIGENCIA_REQUISITO_AMBIENTAL, /*144*/
           0, /*145*/
           :old.ESPECIFICAR_REQUISITO, /*146*/
           '', /*147*/
           :old.ORIGEM_PEDIDO, /*148*/
           0, /*149*/
           :old.EXECUTA_TRIGGER, /*150*/
           0, /*151*/
           :old.CRITERIO_PEDIDO, /*152*/
           0, /*153*/
           :old.ADMINISTRADOR, /*154*/
           0, /*155*/
           :old.PEDIDO_COTACAO, /*156*/
           0, /*157*/
           :old.ID_PEDIDO_FORCA_VENDAS, /*158*/
           '', /*159*/
           :old.CRITERIO_QUALIDADE, /*160*/
           0, /*161*/
           :old.DESCONTO_EXTRA, /*162*/
           0, /*163*/
           :old.PRAZO_EXTRA, /*164*/
           0, /*165*/
           :old.COND_PGTO_EXTRA, /*166*/
           0, /*167*/
           :old.COD_LOCAL, /*168*/
           0, /*169*/
           :old.COD_CATALOGO, /*170*/
           0, /*171*/
           :old.ACEITA_ANTECIPACAO, /*172*/
           0, /*173*/
           :old.PERMITE_PARCIAL, /*174*/
           0, /*175*/
           :old.COD_PREPOSTO, /*176*/
           0, /*177*/
           :old.ANA_COMERCIAL_CONCESSAO_DESC, /*178*/
           0, /*179*/
           :old.ANA_COMERCIAL_PRAZO, /*180*/
           0, /*181*/
           :old.ANA_COMERCIAL_COMISSAO, /*182*/
           0, /*183*/
           :old.ANA_COMERCIAL_OBS, /*184*/
           '', /*185*/
           :old.OBS_LISTA_CONTEUDO_1,    /*186*/
           ' ',    /*187*/
           :old.OBS_LISTA_CONTEUDO_2,    /*188*/
           ' ',    /*189*/
           :old.OBS_LISTA_CONTEUDO_3,    /*190*/
           ' ',    /*191*/
           :old.COD_TIPO_EMBALAGEM,    /*192*/
           0    /*193*/
         );
    end;
 end if;
end inter_tr_inte_100_log;

-- ALTER TRIGGER "INTER_TR_INTE_100_LOG" ENABLE
 

/

exec inter_pr_recompile;

