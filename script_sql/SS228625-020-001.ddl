create or replace function inter_fn_calendarioDiaUtil(p_vencto in date,  p_tipoUtil in number)
return date
is

   v_data_prorrogacao     date;
   eh number;
begin
   v_data_prorrogacao := p_vencto;
   if p_tipoUtil = 0 
   then --dia utila
       begin
          select Min(basi_260.data_calendario) into v_data_prorrogacao from basi_260
          where data_calendario >= p_vencto
          and   basi_260.dia_util   = 0;
       
       exception
          when others then
             v_data_prorrogacao := p_vencto;
       end;
    else 
      
        begin
           select Min(basi_260.data_calendario) into v_data_prorrogacao from basi_260
           where data_calendario > p_vencto
           and   basi_260.dia_util_finan   >= 0;
        exception
           when others then
              v_data_prorrogacao := p_vencto;
        end;
    end if;

   return(v_data_prorrogacao);
end inter_fn_calendarioDiaUtil;

/
