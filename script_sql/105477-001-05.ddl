alter table fatu_060
add unid_med_trib varchar2(10);

alter table fatu_060
modify unid_med_trib default ' ';

comment on column fatu_060.unid_med_trib is 'Unidade de medida tributaria para exterior';

exec inter_pr_recompile;
/
