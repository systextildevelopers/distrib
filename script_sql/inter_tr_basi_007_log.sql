
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_007_LOG" 
   after insert or delete or update
       of  CODIGO_PROJETO_PAI,             SEQUENCIA_PROJETO_PAI,
           SEQUENCIA_ATIVIDADE_PAI,        CODIGO_ATIVIDADE_PAI,
           CODIGO_PROJETO_FILHO,           SEQUENCIA_PROJETO_FILHO,
           SEQUENCIA_ATIVIDADE_FILHO,      CODIGO_ATIVIDADE_FILHO
   on basi_007
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   wsp_cod_tipo_produto_n    varchar2(6);
   wsp_tipo_produto_n        varchar2(60);

   wsp_desc_atividade_n      varchar2(60);

   wsp_desc_projeto_n        varchar2(60);

   wsf_cod_tipo_produto_n    varchar2(6);
   wsf_tipo_produto_n        varchar2(60);

   wsf_desc_atividade_n      varchar2(60);

   wsf_desc_projeto_n        varchar2(60);

   wsp_cod_tipo_produto_o    varchar2(6);
   wsp_tipo_produto_o        varchar2(60);

   wsp_desc_atividade_o      varchar2(60);

   wsp_desc_projeto_o        varchar2(60);

   wsf_cod_tipo_produto_o    varchar2(6);
   wsf_tipo_produto_o        varchar2(60);

   wsf_desc_atividade_o      varchar2(60);

   wsf_desc_projeto_o        varchar2(60);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      begin
         select basi_101.codigo_tipo_produto
         into wsp_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto_pai
           and basi_101.sequencia_projeto = :new.sequencia_projeto_pai;
      exception when no_data_found then
         wsp_cod_tipo_produto_n := ' ';
      end;

      begin
         select basi_101.codigo_tipo_produto
         into wsf_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto_filho
           and basi_101.sequencia_projeto = :new.sequencia_projeto_filho;
      exception when no_data_found then
         wsf_cod_tipo_produto_n := ' ';
      end;

      begin
         select nvl(basi_003.descricao,' ')
         into wsp_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = wsp_cod_tipo_produto_n;
      exception when no_data_found then
         wsp_tipo_produto_n := ' ';
      end;

      begin
         select nvl(basi_003.descricao,' ')
         into wsf_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = wsf_cod_tipo_produto_n;
      exception when no_data_found then
         wsf_tipo_produto_n := ' ';
      end;

      begin
         select basi_002.descricao
         into wsp_desc_atividade_n
         from basi_002
         where basi_002.codigo_atividade = :new.codigo_atividade_pai;
      exception when no_data_found then
         wsp_desc_atividade_n := ' ';
      end;

      begin
         select basi_002.descricao
         into wsf_desc_atividade_n
         from basi_002
         where basi_002.codigo_atividade = :new.codigo_atividade_filho;
      exception when no_data_found then
         wsf_desc_atividade_n := ' ';
      end;

      begin
         select basi_001.descricao_projeto
         into wsp_desc_projeto_n
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto_pai;
      exception when no_data_found then
         wsp_desc_projeto_n := ' ';
      end;

      begin
         select basi_001.descricao_projeto
         into wsf_desc_projeto_n
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto_filho;
      exception when no_data_found then
         wsf_desc_projeto_n := ' ';
      end;

      begin
         INSERT INTO hist_100 (
            tabela,                 operacao,
            data_ocorr,             aplicacao,
            usuario_rede,           maquina_rede,
            str01,                  num05,
            long01
         ) VALUES (
            'BASI_007',             'I',
            sysdate,                ws_aplicativo,
            ws_usuario_rede,        ws_maquina_rede,
            :new.codigo_projeto_pai, :new.sequencia_projeto_pai,
            inter_fn_buscar_tag('lb34971#DEPENDENCIAS DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                                     chr(10)                   ||
                                     chr(10)                   ||
            '                      '                           ||
            inter_fn_buscar_tag('lb34971#lb34972#PROJETO PRINCIPAL',ws_locale_usuario,ws_usuario_systextil) ||
                                     chr(10)                   ||
            inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.codigo_projeto_pai    ||
            ' - ' || wsp_desc_projeto_n                        ||
            chr(10)                                            ||
            inter_fn_buscar_tag('lb05455#SEQUeNCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.sequencia_projeto_pai ||
            chr(10)                                            ||
            inter_fn_buscar_tag('lb01021#TIPO PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || wsp_cod_tipo_produto_n     ||
            ' - '                || wsp_tipo_produto_n         ||
            chr(10)                                            ||
            inter_fn_buscar_tag('lb05455#SEQUeNCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.sequencia_atividade_pai ||
            chr(10)                                            ||
            inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.codigo_atividade_pai  ||
            ' - ' || wsp_desc_atividade_n                      ||
                                     chr(10)                   ||
                                     chr(10)                   ||
            '                       '                          ||
            inter_fn_buscar_tag('lb34973#PROJETO DEPENDENTE',ws_locale_usuario,ws_usuario_systextil) ||
                                     chr(10)                   ||
                                     chr(10)                   ||
            inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.codigo_projeto_filho  ||
            ' - ' || wsf_desc_projeto_n                        ||
            chr(10)                                            ||
            inter_fn_buscar_tag('lb05455#SEQUeNCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.sequencia_projeto_filho ||
            chr(10)                                            ||
            inter_fn_buscar_tag('lb01021#TIPO PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || wsf_cod_tipo_produto_n     ||
            ' - '                || wsf_tipo_produto_n         ||
            chr(10)                                            ||
            inter_fn_buscar_tag('lb05455#SEQUeNCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.sequencia_atividade_filho ||
            chr(10)                                            ||
            inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.codigo_atividade_filho  ||
            ' - ' || wsf_desc_atividade_n
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(007-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   if deleting
   then
      begin
         select basi_101.codigo_tipo_produto
         into wsp_cod_tipo_produto_o
         from basi_101
         where basi_101.codigo_projeto    = :old.codigo_projeto_pai
           and basi_101.sequencia_projeto = :old.sequencia_projeto_pai;
      exception when no_data_found then
         wsp_cod_tipo_produto_o := ' ';
      end;

      begin
         select basi_101.codigo_tipo_produto
         into wsf_cod_tipo_produto_o
         from basi_101
         where basi_101.codigo_projeto    = :old.codigo_projeto_filho
           and basi_101.sequencia_projeto = :old.sequencia_projeto_filho;
      exception when no_data_found then
         wsf_cod_tipo_produto_o := ' ';
      end;

      begin
         select nvl(basi_003.descricao,' ')
         into wsp_tipo_produto_o
         from basi_003
         where basi_003.codigo_tipo_produto = wsp_cod_tipo_produto_o;
      exception when no_data_found then
         wsp_tipo_produto_o := ' ';
      end;

      begin
         select nvl(basi_003.descricao,' ')
         into wsf_tipo_produto_o
         from basi_003
         where basi_003.codigo_tipo_produto = wsf_cod_tipo_produto_o;
      exception when no_data_found then
         wsf_tipo_produto_o := ' ';
      end;

      begin
         select basi_002.descricao
         into wsp_desc_atividade_o
         from basi_002
         where basi_002.codigo_atividade = :old.codigo_atividade_pai;
      exception when no_data_found then
         wsp_desc_atividade_o := ' ';
      end;

      begin
         select basi_002.descricao
         into wsf_desc_atividade_o
         from basi_002
         where basi_002.codigo_atividade = :old.codigo_atividade_filho;
      exception when no_data_found then
         wsf_desc_atividade_o := ' ';
      end;

      begin
         select basi_001.descricao_projeto
         into wsp_desc_projeto_o
         from basi_001
         where basi_001.codigo_projeto = :old.codigo_projeto_pai;
      exception when no_data_found then
         wsp_desc_projeto_o := ' ';
      end;

      begin
         select basi_001.descricao_projeto
         into wsf_desc_projeto_o
         from basi_001
         where basi_001.codigo_projeto = :old.codigo_projeto_filho;
      exception when no_data_found then
         wsf_desc_projeto_o := ' ';
      end;

      INSERT INTO hist_100
         ( tabela,                 operacao,
           data_ocorr,             aplicacao,
           usuario_rede,           maquina_rede,
           str01,                  num05,
           long01
         )
      VALUES
         ( 'BASI_007',             'O',
           sysdate,                ws_aplicativo,
           ws_usuario_rede,        ws_maquina_rede,
          :old.codigo_projeto_pai, :old.sequencia_projeto_pai,
            '           ' ||
            inter_fn_buscar_tag('lb34971#DEPENDENCIAS DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                                    chr(10)                   ||
                                    chr(10)                   ||
            '                      '                          ||
            inter_fn_buscar_tag('lb34971#lb34972#PROJETO PRINCIPAL',ws_locale_usuario,ws_usuario_systextil) ||
                                    chr(10)                   ||
           inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                || :old.codigo_projeto_pai    ||
           ' - ' || wsp_desc_projeto_o                        ||
           chr(10)                                            ||
           inter_fn_buscar_tag('lb05455#SEQUeNCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                || :old.sequencia_projeto_pai ||
           chr(10)                                            ||
           inter_fn_buscar_tag('lb01021#TIPO PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                || wsp_cod_tipo_produto_o     ||
           ' - '                || wsp_tipo_produto_o         ||
           chr(10)                                            ||
           inter_fn_buscar_tag('lb05455#SEQUeNCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                || :old.sequencia_atividade_pai ||
           chr(10)                                            ||
           inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                || :old.codigo_atividade_pai  ||
           ' - ' || wsp_desc_atividade_o                      ||
                                    chr(10)                   ||
                                    chr(10)                   ||
           '                        '                         ||
           inter_fn_buscar_tag('lb34973#PROJETO DEPENDENTE',ws_locale_usuario,ws_usuario_systextil) ||
                                    chr(10)                   ||
                                    chr(10)                   ||
           inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                || :old.codigo_projeto_filho  ||
           ' - ' || wsf_desc_projeto_o                        ||
           chr(10)                                            ||
           inter_fn_buscar_tag('lb05455#SEQUeNCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                || :old.sequencia_projeto_filho ||
           chr(10)                                            ||
           inter_fn_buscar_tag('lb01021#TIPO PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                || wsf_cod_tipo_produto_o     ||
           ' - '                || wsf_tipo_produto_o         ||
           chr(10)                                            ||
           inter_fn_buscar_tag('lb05455#SEQUeNCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                || :old.sequencia_atividade_filho ||
           chr(10)                                            ||
           inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                || :old.codigo_atividade_filho  ||
           ' - ' || wsf_desc_atividade_o
        );
   end if;

end inter_tr_basi_007_log;

-- ALTER TRIGGER "INTER_TR_BASI_007_LOG" ENABLE
 

/

exec inter_pr_recompile;

