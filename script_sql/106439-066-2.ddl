delete from hdoc_033 where programa = 'pedi_008';
delete from hdoc_035 where codigo_programa = 'pedi_008';

commit work;

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_e008', 'Relatório de Pedidos Pendentes de Tecidos',0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_e008', 'pedi_menu' ,1, 0, 'S', 'S', 'S', 'S');

commit work;
