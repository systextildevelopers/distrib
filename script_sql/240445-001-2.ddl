alter table obrf_164
add (
    RECEB_CGC9      NUMBER(9) default 0,
    RECEB_CGC4      NUMBER(4) default 0,
    RECEB_CGC2      NUMBER(2) default 0,
    IDENT_TERMINAL  VARCHAR2(40) default ''
)
