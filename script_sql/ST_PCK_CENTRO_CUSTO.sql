create or replace package ST_PCK_CENTRO_CUSTO as
    TYPE t_dados_basi_185 IS TABLE OF basi_185%rowtype;

    function get_dados_ccusto(p_ccusto number, p_msg_erro in out varchar2) return t_dados_basi_185;

end;
