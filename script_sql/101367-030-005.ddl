ALTER TABLE obrf_089 ADD (
  estagio NUMBER(2));

ALTER TABLE obrf_089 MODIFY (
  estagio DEFAULT 0);

exec inter_pr_recompile;

/
