-- Cadastro de Grupos de Embarque por Referência

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('basi_f595', 'Cadastro de Grupos de Embarque por Referência', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'basi_f595', 'estq_menu', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'basi_f595', 'estq_menu', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Registro de grupos de envío por referencia'
 where hdoc_036.codigo_programa = 'basi_f595'
   and hdoc_036.locale          = 'es_ES';

commit;



