CREATE OR REPLACE TRIGGER inter_tr_OBRF_772_log
AFTER INSERT OR DELETE OR UPDATE
ON OBRF_772
FOR EACH ROW
DECLARE
   ws_usuario_rede      VARCHAR2(20);
   ws_maquina_rede      VARCHAR2(40);
   ws_aplicativo        VARCHAR2(20);
   ws_sid               NUMBER(9);
   ws_empresa           NUMBER(3);
   ws_usuario_systextil VARCHAR2(20);
   ws_locale_usuario    VARCHAR2(5);
   v_nome_programa      VARCHAR2(20);
BEGIN
   -- Dados do usuário logado
   inter_pr_dados_usuario(
      ws_usuario_rede,
      ws_maquina_rede,
      ws_aplicativo,
      ws_sid,
      ws_usuario_systextil,
      ws_empresa,
      ws_locale_usuario
   );

   v_nome_programa := inter_fn_nome_programa(ws_sid);

   IF INSERTING THEN
      BEGIN
         INSERT INTO OBRF_772_log (
            TIPO_OCORR,
            DATA_OCORR,
            HORA_OCORR,
            USUARIO_REDE,
            MAQUINA_REDE,
            APLICACAO,
            USUARIO_SISTEMA,
            NOME_PROGRAMA,
            CODIGO_EMPRESA_OLD,
            CODIGO_EMPRESA_NEW,
            CNPJ9_OLD,
            CNPJ9_NEW,
            CNPJ4_OLD,
            CNPJ4_NEW,
            CNPJ2_OLD,
            CNPJ2_NEW,
            NAT_OPERACAO_OLD,
            NAT_OPERACAO_NEW,
            COD_DEPOSITO_OLD,
            COD_DEPOSITO_NEW,
            FUNCIONARIO_OLD,
            FUNCIONARIO_NEW,
            COD_PROCESSO_OLD,
            COD_PROCESSO_NEW,
            ATIVO_INATIVO_OLD,
            ATIVO_INATIVO_NEW
         ) VALUES (
            'I',
            SYSDATE,
            SYSDATE,
            ws_usuario_rede,
            ws_maquina_rede,
            ws_aplicativo,
            ws_usuario_systextil,
            v_nome_programa,
            0,
            :NEW.CODIGO_EMPRESA,
            0,
            :NEW.CNPJ9,
            0,
            :NEW.CNPJ4,
            0,
            :NEW.CNPJ2,
            0,
            :NEW.NAT_OPERACAO,
            0,
            :NEW.COD_DEPOSITO,
            0,
            :NEW.FUNCIONARIO,
            0,
            :NEW.COD_PROCESSO,
            0,
            :NEW.ATIVO_INATIVO
         );
      END;
   END IF;

   IF UPDATING THEN
      BEGIN
         INSERT INTO OBRF_772_log (
            TIPO_OCORR,
            DATA_OCORR,
            HORA_OCORR,
            USUARIO_REDE,
            MAQUINA_REDE,
            APLICACAO,
            USUARIO_SISTEMA,
            NOME_PROGRAMA,
            CODIGO_EMPRESA_OLD,
            CODIGO_EMPRESA_NEW,
            CNPJ9_OLD,
            CNPJ9_NEW,
            CNPJ4_OLD,
            CNPJ4_NEW,
            CNPJ2_OLD,
            CNPJ2_NEW,
            NAT_OPERACAO_OLD,
            NAT_OPERACAO_NEW,
            COD_DEPOSITO_OLD,
            COD_DEPOSITO_NEW,
            FUNCIONARIO_OLD,
            FUNCIONARIO_NEW,
            COD_PROCESSO_OLD,
            COD_PROCESSO_NEW,
            ATIVO_INATIVO_OLD,
            ATIVO_INATIVO_NEW
         ) VALUES (
            'A',
            SYSDATE,
            SYSDATE,
            ws_usuario_rede,
            ws_maquina_rede,
            ws_aplicativo,
            ws_usuario_systextil,
            v_nome_programa,
            :OLD.CODIGO_EMPRESA,
            :NEW.CODIGO_EMPRESA,
            :OLD.CNPJ9,
            :NEW.CNPJ9,
            :OLD.CNPJ4,
            :NEW.CNPJ4,
            :OLD.CNPJ2,
            :NEW.CNPJ2,
            :OLD.NAT_OPERACAO,
            :NEW.NAT_OPERACAO,
            :OLD.COD_DEPOSITO,
            :NEW.COD_DEPOSITO,
            :OLD.FUNCIONARIO,
            :NEW.FUNCIONARIO,
            :OLD.COD_PROCESSO,
            :NEW.COD_PROCESSO,
            :OLD.ATIVO_INATIVO,
            :NEW.ATIVO_INATIVO
         );
      END;
   END IF;

   IF DELETING THEN
      BEGIN
         INSERT INTO OBRF_772_log (
            TIPO_OCORR,
            DATA_OCORR,
            HORA_OCORR,
            USUARIO_REDE,
            MAQUINA_REDE,
            APLICACAO,
            USUARIO_SISTEMA,
            NOME_PROGRAMA,
            CODIGO_EMPRESA_OLD,
            CODIGO_EMPRESA_NEW,
            CNPJ9_OLD,
            CNPJ9_NEW,
            CNPJ4_OLD,
            CNPJ4_NEW,
            CNPJ2_OLD,
            CNPJ2_NEW,
            NAT_OPERACAO_OLD,
            NAT_OPERACAO_NEW,
            COD_DEPOSITO_OLD,
            COD_DEPOSITO_NEW,
            FUNCIONARIO_OLD,
            FUNCIONARIO_NEW,
            COD_PROCESSO_OLD,
            COD_PROCESSO_NEW,
            ATIVO_INATIVO_OLD,
            ATIVO_INATIVO_NEW
         ) VALUES (
            'D',
            SYSDATE,
            SYSDATE,
            ws_usuario_rede,
            ws_maquina_rede,
            ws_aplicativo,
            ws_usuario_systextil,
            v_nome_programa,
            :OLD.CODIGO_EMPRESA,
            0,
            :OLD.CNPJ9,
            0,
            :OLD.CNPJ4,
            0,
            :OLD.CNPJ2,
            0,
            :OLD.NAT_OPERACAO,
            0,
            :OLD.COD_DEPOSITO,
            0,
            :OLD.FUNCIONARIO,
            0,
            :OLD.COD_PROCESSO,
            0,
            :OLD.ATIVO_INATIVO,
            0
         );
      END;
   END IF;
END inter_tr_OBRF_772_log;
