create table obrf_083 (
    numero_ordem    	number(6)       not null,
    seq_areceber    	number(3)       not null,
    sequencia       	number(3)       not null,
    sequencial_op       number(9)       not null,
	ordem_producao		number(9)		not null,
	periodo_producao	number(4)		not null,
    ordem_confeccao 	number(5)		not null,
    codigo_estagio      number(2)		not null,
    quantidade      	number(14,3),
	consumo_unit		number(13,7),
    
    CONSTRAINT obrf_083_pk PRIMARY KEY (numero_ordem, seq_areceber, sequencia, sequencial_op, ordem_producao, periodo_producao, ordem_confeccao, codigo_estagio)
);

CREATE INDEX obrf_083_idx1 ON obrf_083(numero_ordem, seq_areceber, sequencia, ordem_producao, periodo_producao, ordem_confeccao, codigo_estagio);

comment on table obrf_083 is 'Tabela de Produtos Enviados aberta por Ordem de Confeccao (pacote) para o BlocoK';

comment on column obrf_083.numero_ordem is 'Numero da ordem de servico';
comment on column obrf_083.seq_areceber is 'Sequencia da ordem de servico';
comment on column obrf_083.sequencia is 'Sequencia do item a enviar';
comment on column obrf_083.sequencial_op is 'Sequencial dentro da mesma producao';

comment on column obrf_083.ordem_producao is 'Ordem de producao relacionada a ordem de servico';
comment on column obrf_083.periodo_producao is 'Periodo de producao do pacote da OP relacionada a ordem de servico';
comment on column obrf_083.ordem_confeccao is 'Ordem de confeccao da OP relacionada a ordem de servico';
comment on column obrf_083.codigo_estagio is 'Codigo do estagio relacionado a ordem de servico';
comment on column obrf_083.quantidade is 'Quantidade do item a enviar para a ordem de confeccao (pacote) relacionado';
comment on column obrf_083.consumo_unit is 'Consumo unitário do item a enviar';

exec inter_pr_recompile;
