
  CREATE OR REPLACE TRIGGER "INTER_TR_MAPA_ECF" 
after insert or delete
or update of numero, data_emissao, cod_empresa, sequencia,
	cont_inicial, cont_final, total_ini, total_fim,
	cancelado, desconto, vl_contabil, ntributada,
	nincide, substit, base_calc, outras,
	perc_aliq1, perc_aliq2, perc_aliq3, perc_aliq4,
	val_aliq1, val_aliq2, val_aliq3, val_aliq4,
	num_serie_ecf, num_intervencao, num_caixa, perc_aliq5,
	perc_aliq6, val_aliq5, val_aliq6, tipo_atualizacao,
	cod_impressora_fiscal, cont_operacao
on MAPA_ECF
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


   v_nome_programa := inter_fn_nome_programa(ws_sid);   

 if inserting
 then
    begin

        insert into MAPA_ECF_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           NUMERO_OLD,   /*8*/
           NUMERO_NEW,   /*9*/
           DATA_EMISSAO_OLD,   /*10*/
           DATA_EMISSAO_NEW,   /*11*/
           COD_EMPRESA_OLD,   /*12*/
           COD_EMPRESA_NEW,   /*13*/
           SEQUENCIA_OLD,   /*14*/
           SEQUENCIA_NEW,   /*15*/
           CONT_INICIAL_OLD,   /*16*/
           CONT_INICIAL_NEW,   /*17*/
           CONT_FINAL_OLD,   /*18*/
           CONT_FINAL_NEW,   /*19*/
           TOTAL_INI_OLD,   /*20*/
           TOTAL_INI_NEW,   /*21*/
           TOTAL_FIM_OLD,   /*22*/
           TOTAL_FIM_NEW,   /*23*/
           CANCELADO_OLD,   /*24*/
           CANCELADO_NEW,   /*25*/
           DESCONTO_OLD,   /*26*/
           DESCONTO_NEW,   /*27*/
           VL_CONTABIL_OLD,   /*28*/
           VL_CONTABIL_NEW,   /*29*/
           NTRIBUTADA_OLD,   /*30*/
           NTRIBUTADA_NEW,   /*31*/
           NINCIDE_OLD,   /*32*/
           NINCIDE_NEW,   /*33*/
           SUBSTIT_OLD,   /*34*/
           SUBSTIT_NEW,   /*35*/
           BASE_CALC_OLD,   /*36*/
           BASE_CALC_NEW,   /*37*/
           OUTRAS_OLD,   /*38*/
           OUTRAS_NEW,   /*39*/
           PERC_ALIQ1_OLD,   /*40*/
           PERC_ALIQ1_NEW,   /*41*/
           PERC_ALIQ2_OLD,   /*42*/
           PERC_ALIQ2_NEW,   /*43*/
           PERC_ALIQ3_OLD,   /*44*/
           PERC_ALIQ3_NEW,   /*45*/
           PERC_ALIQ4_OLD,   /*46*/
           PERC_ALIQ4_NEW,   /*47*/
           VAL_ALIQ1_OLD,   /*48*/
           VAL_ALIQ1_NEW,   /*49*/
           VAL_ALIQ2_OLD,   /*50*/
           VAL_ALIQ2_NEW,   /*51*/
           VAL_ALIQ3_OLD,   /*52*/
           VAL_ALIQ3_NEW,   /*53*/
           VAL_ALIQ4_OLD,   /*54*/
           VAL_ALIQ4_NEW,   /*55*/
           NUM_SERIE_ECF_OLD,   /*56*/
           NUM_SERIE_ECF_NEW,   /*57*/
           NUM_INTERVENCAO_OLD,   /*58*/
           NUM_INTERVENCAO_NEW,   /*59*/
           NUM_CAIXA_OLD,   /*60*/
           NUM_CAIXA_NEW,   /*61*/
           PERC_ALIQ5_OLD,   /*62*/
           PERC_ALIQ5_NEW,   /*63*/
           PERC_ALIQ6_OLD,   /*64*/
           PERC_ALIQ6_NEW,   /*65*/
           VAL_ALIQ5_OLD,   /*66*/
           VAL_ALIQ5_NEW,   /*67*/
           VAL_ALIQ6_OLD,   /*68*/
           VAL_ALIQ6_NEW,   /*69*/
           TIPO_ATUALIZACAO_OLD,   /*70*/
           TIPO_ATUALIZACAO_NEW,   /*71*/
           COD_IMPRESSORA_FISCAL_OLD,   /*72*/
           COD_IMPRESSORA_FISCAL_NEW    /*73*/
        ) values (
            'I', /*1*/
            sysdate, /*2*/
            sysdate,/*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           0,/*8*/
           :new.NUMERO, /*9*/
           null,/*10*/
           :new.DATA_EMISSAO, /*11*/
           0,/*12*/
           :new.COD_EMPRESA, /*13*/
           0,/*14*/
           :new.SEQUENCIA, /*15*/
           0,/*16*/
           :new.CONT_INICIAL, /*17*/
           0,/*18*/
           :new.CONT_FINAL, /*19*/
           0,/*20*/
           :new.TOTAL_INI, /*21*/
           0,/*22*/
           :new.TOTAL_FIM, /*23*/
           0,/*24*/
           :new.CANCELADO, /*25*/
           0,/*26*/
           :new.DESCONTO, /*27*/
           0,/*28*/
           :new.VL_CONTABIL, /*29*/
           0,/*30*/
           :new.NTRIBUTADA, /*31*/
           0,/*32*/
           :new.NINCIDE, /*33*/
           0,/*34*/
           :new.SUBSTIT, /*35*/
           0,/*36*/
           :new.BASE_CALC, /*37*/
           0,/*38*/
           :new.OUTRAS, /*39*/
           0,/*40*/
           :new.PERC_ALIQ1, /*41*/
           0,/*42*/
           :new.PERC_ALIQ2, /*43*/
           0,/*44*/
           :new.PERC_ALIQ3, /*45*/
           0,/*46*/
           :new.PERC_ALIQ4, /*47*/
           0,/*48*/
           :new.VAL_ALIQ1, /*49*/
           0,/*50*/
           :new.VAL_ALIQ2, /*51*/
           0,/*52*/
           :new.VAL_ALIQ3, /*53*/
           0,/*54*/
           :new.VAL_ALIQ4, /*55*/
           '',/*56*/
           :new.NUM_SERIE_ECF, /*57*/
           0,/*58*/
           :new.NUM_INTERVENCAO, /*59*/
           0,/*60*/
           :new.NUM_CAIXA, /*61*/
           0,/*62*/
           :new.PERC_ALIQ5, /*63*/
           0,/*64*/
           :new.PERC_ALIQ6, /*65*/
           0,/*66*/
           :new.VAL_ALIQ5, /*67*/
           0,/*68*/
           :new.VAL_ALIQ6, /*69*/
           0,/*70*/
           :new.TIPO_ATUALIZACAO, /*71*/
           0,/*72*/
           :new.COD_IMPRESSORA_FISCAL /*73*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into MAPA_ECF_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           NUMERO_OLD, /*8*/
           NUMERO_NEW, /*9*/
           DATA_EMISSAO_OLD, /*10*/
           DATA_EMISSAO_NEW, /*11*/
           COD_EMPRESA_OLD, /*12*/
           COD_EMPRESA_NEW, /*13*/
           SEQUENCIA_OLD, /*14*/
           SEQUENCIA_NEW, /*15*/
           CONT_INICIAL_OLD, /*16*/
           CONT_INICIAL_NEW, /*17*/
           CONT_FINAL_OLD, /*18*/
           CONT_FINAL_NEW, /*19*/
           TOTAL_INI_OLD, /*20*/
           TOTAL_INI_NEW, /*21*/
           TOTAL_FIM_OLD, /*22*/
           TOTAL_FIM_NEW, /*23*/
           CANCELADO_OLD, /*24*/
           CANCELADO_NEW, /*25*/
           DESCONTO_OLD, /*26*/
           DESCONTO_NEW, /*27*/
           VL_CONTABIL_OLD, /*28*/
           VL_CONTABIL_NEW, /*29*/
           NTRIBUTADA_OLD, /*30*/
           NTRIBUTADA_NEW, /*31*/
           NINCIDE_OLD, /*32*/
           NINCIDE_NEW, /*33*/
           SUBSTIT_OLD, /*34*/
           SUBSTIT_NEW, /*35*/
           BASE_CALC_OLD, /*36*/
           BASE_CALC_NEW, /*37*/
           OUTRAS_OLD, /*38*/
           OUTRAS_NEW, /*39*/
           PERC_ALIQ1_OLD, /*40*/
           PERC_ALIQ1_NEW, /*41*/
           PERC_ALIQ2_OLD, /*42*/
           PERC_ALIQ2_NEW, /*43*/
           PERC_ALIQ3_OLD, /*44*/
           PERC_ALIQ3_NEW, /*45*/
           PERC_ALIQ4_OLD, /*46*/
           PERC_ALIQ4_NEW, /*47*/
           VAL_ALIQ1_OLD, /*48*/
           VAL_ALIQ1_NEW, /*49*/
           VAL_ALIQ2_OLD, /*50*/
           VAL_ALIQ2_NEW, /*51*/
           VAL_ALIQ3_OLD, /*52*/
           VAL_ALIQ3_NEW, /*53*/
           VAL_ALIQ4_OLD, /*54*/
           VAL_ALIQ4_NEW, /*55*/
           NUM_SERIE_ECF_OLD, /*56*/
           NUM_SERIE_ECF_NEW, /*57*/
           NUM_INTERVENCAO_OLD, /*58*/
           NUM_INTERVENCAO_NEW, /*59*/
           NUM_CAIXA_OLD, /*60*/
           NUM_CAIXA_NEW, /*61*/
           PERC_ALIQ5_OLD, /*62*/
           PERC_ALIQ5_NEW, /*63*/
           PERC_ALIQ6_OLD, /*64*/
           PERC_ALIQ6_NEW, /*65*/
           VAL_ALIQ5_OLD, /*66*/
           VAL_ALIQ5_NEW, /*67*/
           VAL_ALIQ6_OLD, /*68*/
           VAL_ALIQ6_NEW, /*69*/
           TIPO_ATUALIZACAO_OLD, /*70*/
           TIPO_ATUALIZACAO_NEW, /*71*/
           COD_IMPRESSORA_FISCAL_OLD, /*72*/
           COD_IMPRESSORA_FISCAL_NEW  /*73*/
        ) values (
            'A', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.NUMERO,  /*8*/
           :new.NUMERO, /*9*/
           :old.DATA_EMISSAO,  /*10*/
           :new.DATA_EMISSAO, /*11*/
           :old.COD_EMPRESA,  /*12*/
           :new.COD_EMPRESA, /*13*/
           :old.SEQUENCIA,  /*14*/
           :new.SEQUENCIA, /*15*/
           :old.CONT_INICIAL,  /*16*/
           :new.CONT_INICIAL, /*17*/
           :old.CONT_FINAL,  /*18*/
           :new.CONT_FINAL, /*19*/
           :old.TOTAL_INI,  /*20*/
           :new.TOTAL_INI, /*21*/
           :old.TOTAL_FIM,  /*22*/
           :new.TOTAL_FIM, /*23*/
           :old.CANCELADO,  /*24*/
           :new.CANCELADO, /*25*/
           :old.DESCONTO,  /*26*/
           :new.DESCONTO, /*27*/
           :old.VL_CONTABIL,  /*28*/
           :new.VL_CONTABIL, /*29*/
           :old.NTRIBUTADA,  /*30*/
           :new.NTRIBUTADA, /*31*/
           :old.NINCIDE,  /*32*/
           :new.NINCIDE, /*33*/
           :old.SUBSTIT,  /*34*/
           :new.SUBSTIT, /*35*/
           :old.BASE_CALC,  /*36*/
           :new.BASE_CALC, /*37*/
           :old.OUTRAS,  /*38*/
           :new.OUTRAS, /*39*/
           :old.PERC_ALIQ1,  /*40*/
           :new.PERC_ALIQ1, /*41*/
           :old.PERC_ALIQ2,  /*42*/
           :new.PERC_ALIQ2, /*43*/
           :old.PERC_ALIQ3,  /*44*/
           :new.PERC_ALIQ3, /*45*/
           :old.PERC_ALIQ4,  /*46*/
           :new.PERC_ALIQ4, /*47*/
           :old.VAL_ALIQ1,  /*48*/
           :new.VAL_ALIQ1, /*49*/
           :old.VAL_ALIQ2,  /*50*/
           :new.VAL_ALIQ2, /*51*/
           :old.VAL_ALIQ3,  /*52*/
           :new.VAL_ALIQ3, /*53*/
           :old.VAL_ALIQ4,  /*54*/
           :new.VAL_ALIQ4, /*55*/
           :old.NUM_SERIE_ECF,  /*56*/
           :new.NUM_SERIE_ECF, /*57*/
           :old.NUM_INTERVENCAO,  /*58*/
           :new.NUM_INTERVENCAO, /*59*/
           :old.NUM_CAIXA,  /*60*/
           :new.NUM_CAIXA, /*61*/
           :old.PERC_ALIQ5,  /*62*/
           :new.PERC_ALIQ5, /*63*/
           :old.PERC_ALIQ6,  /*64*/
           :new.PERC_ALIQ6, /*65*/
           :old.VAL_ALIQ5,  /*66*/
           :new.VAL_ALIQ5, /*67*/
           :old.VAL_ALIQ6,  /*68*/
           :new.VAL_ALIQ6, /*69*/
           :old.TIPO_ATUALIZACAO,  /*70*/
           :new.TIPO_ATUALIZACAO, /*71*/
           :old.COD_IMPRESSORA_FISCAL,  /*72*/
           :new.COD_IMPRESSORA_FISCAL  /*73*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into MAPA_ECF_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           NUMERO_OLD, /*8*/
           NUMERO_NEW, /*9*/
           DATA_EMISSAO_OLD, /*10*/
           DATA_EMISSAO_NEW, /*11*/
           COD_EMPRESA_OLD, /*12*/
           COD_EMPRESA_NEW, /*13*/
           SEQUENCIA_OLD, /*14*/
           SEQUENCIA_NEW, /*15*/
           CONT_INICIAL_OLD, /*16*/
           CONT_INICIAL_NEW, /*17*/
           CONT_FINAL_OLD, /*18*/
           CONT_FINAL_NEW, /*19*/
           TOTAL_INI_OLD, /*20*/
           TOTAL_INI_NEW, /*21*/
           TOTAL_FIM_OLD, /*22*/
           TOTAL_FIM_NEW, /*23*/
           CANCELADO_OLD, /*24*/
           CANCELADO_NEW, /*25*/
           DESCONTO_OLD, /*26*/
           DESCONTO_NEW, /*27*/
           VL_CONTABIL_OLD, /*28*/
           VL_CONTABIL_NEW, /*29*/
           NTRIBUTADA_OLD, /*30*/
           NTRIBUTADA_NEW, /*31*/
           NINCIDE_OLD, /*32*/
           NINCIDE_NEW, /*33*/
           SUBSTIT_OLD, /*34*/
           SUBSTIT_NEW, /*35*/
           BASE_CALC_OLD, /*36*/
           BASE_CALC_NEW, /*37*/
           OUTRAS_OLD, /*38*/
           OUTRAS_NEW, /*39*/
           PERC_ALIQ1_OLD, /*40*/
           PERC_ALIQ1_NEW, /*41*/
           PERC_ALIQ2_OLD, /*42*/
           PERC_ALIQ2_NEW, /*43*/
           PERC_ALIQ3_OLD, /*44*/
           PERC_ALIQ3_NEW, /*45*/
           PERC_ALIQ4_OLD, /*46*/
           PERC_ALIQ4_NEW, /*47*/
           VAL_ALIQ1_OLD, /*48*/
           VAL_ALIQ1_NEW, /*49*/
           VAL_ALIQ2_OLD, /*50*/
           VAL_ALIQ2_NEW, /*51*/
           VAL_ALIQ3_OLD, /*52*/
           VAL_ALIQ3_NEW, /*53*/
           VAL_ALIQ4_OLD, /*54*/
           VAL_ALIQ4_NEW, /*55*/
           NUM_SERIE_ECF_OLD, /*56*/
           NUM_SERIE_ECF_NEW, /*57*/
           NUM_INTERVENCAO_OLD, /*58*/
           NUM_INTERVENCAO_NEW, /*59*/
           NUM_CAIXA_OLD, /*60*/
           NUM_CAIXA_NEW, /*61*/
           PERC_ALIQ5_OLD, /*62*/
           PERC_ALIQ5_NEW, /*63*/
           PERC_ALIQ6_OLD, /*64*/
           PERC_ALIQ6_NEW, /*65*/
           VAL_ALIQ5_OLD, /*66*/
           VAL_ALIQ5_NEW, /*67*/
           VAL_ALIQ6_OLD, /*68*/
           VAL_ALIQ6_NEW, /*69*/
           TIPO_ATUALIZACAO_OLD, /*70*/
           TIPO_ATUALIZACAO_NEW, /*71*/
           COD_IMPRESSORA_FISCAL_OLD, /*72*/
           COD_IMPRESSORA_FISCAL_NEW /*73*/
        ) values (
            'D', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede,/*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.NUMERO, /*8*/
           0, /*9*/
           :old.DATA_EMISSAO, /*10*/
           null, /*11*/
           :old.COD_EMPRESA, /*12*/
           0, /*13*/
           :old.SEQUENCIA, /*14*/
           0, /*15*/
           :old.CONT_INICIAL, /*16*/
           0, /*17*/
           :old.CONT_FINAL, /*18*/
           0, /*19*/
           :old.TOTAL_INI, /*20*/
           0, /*21*/
           :old.TOTAL_FIM, /*22*/
           0, /*23*/
           :old.CANCELADO, /*24*/
           0, /*25*/
           :old.DESCONTO, /*26*/
           0, /*27*/
           :old.VL_CONTABIL, /*28*/
           0, /*29*/
           :old.NTRIBUTADA, /*30*/
           0, /*31*/
           :old.NINCIDE, /*32*/
           0, /*33*/
           :old.SUBSTIT, /*34*/
           0, /*35*/
           :old.BASE_CALC, /*36*/
           0, /*37*/
           :old.OUTRAS, /*38*/
           0, /*39*/
           :old.PERC_ALIQ1, /*40*/
           0, /*41*/
           :old.PERC_ALIQ2, /*42*/
           0, /*43*/
           :old.PERC_ALIQ3, /*44*/
           0, /*45*/
           :old.PERC_ALIQ4, /*46*/
           0, /*47*/
           :old.VAL_ALIQ1, /*48*/
           0, /*49*/
           :old.VAL_ALIQ2, /*50*/
           0, /*51*/
           :old.VAL_ALIQ3, /*52*/
           0, /*53*/
           :old.VAL_ALIQ4, /*54*/
           0, /*55*/
           :old.NUM_SERIE_ECF, /*56*/
           '', /*57*/
           :old.NUM_INTERVENCAO, /*58*/
           0, /*59*/
           :old.NUM_CAIXA, /*60*/
           0, /*61*/
           :old.PERC_ALIQ5, /*62*/
           0, /*63*/
           :old.PERC_ALIQ6, /*64*/
           0, /*65*/
           :old.VAL_ALIQ5, /*66*/
           0, /*67*/
           :old.VAL_ALIQ6, /*68*/
           0, /*69*/
           :old.TIPO_ATUALIZACAO, /*70*/
           0, /*71*/
           :old.COD_IMPRESSORA_FISCAL, /*72*/
           0 /*73*/
         );
    end;
 end if;

end inter_tr_MAPA_ECF;
-- ALTER TRIGGER "INTER_TR_MAPA_ECF" ENABLE
 

/

exec inter_pr_recompile;

