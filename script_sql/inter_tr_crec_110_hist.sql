
  CREATE OR REPLACE TRIGGER "INTER_TR_CREC_110_HIST" 
before update of nr_perio_nrperiod, nr_perio_divpro, codigo_repr, data_lancamento,
	sequencia_lcto, codigo_historico, complemento_hist, numero_documento,
	seq_documento, tipo_documento, cgc_cli9, cgc_cli4,
	cgc_cli2, base_calc_ir, percentual_ir, valor_ir,
	valor_deducao_ir, base_calc_comis, perc_fatu_crec, percentual_comis,
	valor_lancamento, situacao_lcto, contabilizado, num_contabil,
	gerou_juros_adt_com
       or delete or
       insert on crec_110
for each row
declare
   ws_usuario_rede         sys.gv_$session.osuser%type ;
   ws_maquina_rede         sys.gv_$session.machine%type;
   ws_aplicacao            sys.gv_$session.program%type;
   ws_sid                  sys.gv_$session.sid%type;
   ws_programa             crec_110_hist.programa%type;
   ws_usuario_systextil    crec_110_hist.usuario_systextil%type;
   ws_aplicativo             varchar2(20); 
   ws_empresa                number(3);
   ws_locale_usuario         varchar2(5);
begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
 
   ws_programa := inter_fn_nome_programa(ws_sid);

   if inserting
   then
      insert into crec_110_hist
             (nr_perio_nrperiod         ,              nr_perio_divpro              ,
              codigo_repr               ,              data_lancamento              ,
              sequencia_lcto            ,
              codigo_historico_new      ,              codigo_historico_old         ,
              numero_documento_new      ,              numero_documento_old         ,
              seq_documento_new         ,              seq_documento_old            ,
              tipo_documento_new        ,              tipo_documento_old           ,
              base_calc_comis_new       ,              base_calc_comis_old          ,
              perc_fatu_crec_new        ,              perc_fatu_crec_old           ,
              percentual_comis_new      ,              percentual_comis_old         ,
              valor_lancamento_new      ,              valor_lancamento_old         ,
              situacao_lcto_new         ,              situacao_lcto_old            ,
              programa                  ,              usuario_rede                 ,
              usuario_systextil         ,              maquina_rede                 ,
              aplicacao                 ,              tipo_operacao                ,
              data_operacao)
      VALUES (:new.nr_perio_nrperiod    ,              :new.nr_perio_divpro         ,
              :new.codigo_repr          ,              :new.data_lancamento         ,
              :new.sequencia_lcto       ,
              :new.codigo_historico     ,              0                            ,
              :new.numero_documento     ,              0                            ,
              :new.seq_documento        ,              0                            ,
              :new.tipo_documento       ,              0                            ,
              :new.base_calc_comis      ,              0                            ,
              :new.perc_fatu_crec       ,              0                            ,
              :new.percentual_comis     ,              0                            ,
              :new.valor_lancamento     ,              0                            ,
              :new.situacao_lcto        ,              ' '                          ,
              ws_programa               ,              ws_usuario_rede              ,
              ws_usuario_systextil      ,              ws_maquina_rede              ,
              ws_aplicacao              ,              'I'                          ,
              sysdate);
   end if;

   if updating
   then

      insert into crec_110_hist
             (nr_perio_nrperiod         ,              nr_perio_divpro              ,
              codigo_repr               ,              data_lancamento              ,
              sequencia_lcto            ,
              codigo_historico_new      ,              codigo_historico_old         ,
              numero_documento_new      ,              numero_documento_old         ,
              seq_documento_new         ,              seq_documento_old            ,
              tipo_documento_new        ,              tipo_documento_old           ,
              base_calc_comis_new       ,              base_calc_comis_old          ,
              perc_fatu_crec_new        ,              perc_fatu_crec_old           ,
              percentual_comis_new      ,              percentual_comis_old         ,
              valor_lancamento_new      ,              valor_lancamento_old         ,
              situacao_lcto_new         ,              situacao_lcto_old            ,
              programa                  ,              usuario_rede                 ,
              usuario_systextil         ,              maquina_rede                 ,
              aplicacao                 ,              tipo_operacao                ,
              data_operacao)
      VALUES (:new.nr_perio_nrperiod    ,              :new.nr_perio_divpro         ,
              :new.codigo_repr          ,              :new.data_lancamento         ,
              :new.sequencia_lcto       ,
              :new.codigo_historico     ,              :old.codigo_historico        ,
              :new.numero_documento     ,              :old.numero_documento        ,
              :new.seq_documento        ,              :old.seq_documento           ,
              :new.tipo_documento       ,              :old.tipo_documento          ,
              :new.base_calc_comis      ,              :old.base_calc_comis         ,
              :new.perc_fatu_crec       ,              :old.perc_fatu_crec          ,
              :new.percentual_comis     ,              :old.percentual_comis        ,
              :new.valor_lancamento     ,              :old.valor_lancamento        ,
              :new.situacao_lcto        ,              :old.situacao_lcto           ,
              ws_programa               ,              ws_usuario_rede              ,
              ws_usuario_systextil      ,              ws_maquina_rede              ,
              ws_aplicacao              ,              'U'                          ,
              sysdate);

   end if;

   if deleting
   then
      insert into crec_110_hist
             (nr_perio_nrperiod         ,              nr_perio_divpro              ,
              codigo_repr               ,              data_lancamento              ,
              sequencia_lcto            ,
              codigo_historico_new      ,              codigo_historico_old         ,
              numero_documento_new      ,              numero_documento_old         ,
              seq_documento_new         ,              seq_documento_old            ,
              tipo_documento_new        ,              tipo_documento_old           ,
              base_calc_comis_new       ,              base_calc_comis_old          ,
              perc_fatu_crec_new        ,              perc_fatu_crec_old           ,
              percentual_comis_new      ,              percentual_comis_old         ,
              valor_lancamento_new      ,              valor_lancamento_old         ,
              situacao_lcto_new         ,              situacao_lcto_old            ,
              programa                  ,              usuario_rede                 ,
              usuario_systextil         ,              maquina_rede                 ,
              aplicacao                 ,              tipo_operacao                ,
              data_operacao)
      VALUES (:old.nr_perio_nrperiod    ,              :old.nr_perio_divpro         ,
              :old.codigo_repr          ,              :old.data_lancamento         ,
              :old.sequencia_lcto       ,
              0                         ,              :old.codigo_historico        ,
              0                         ,              :old.numero_documento        ,
              0                         ,              :old.seq_documento           ,
              0                         ,              :old.tipo_documento          ,
              0                         ,              :old.base_calc_comis         ,
              0                         ,              :old.perc_fatu_crec          ,
              0                         ,              :old.percentual_comis        ,
              0                         ,              :old.valor_lancamento        ,
              ' '                       ,              :old.situacao_lcto           ,
              ws_programa               ,              ws_usuario_rede              ,
              ws_usuario_systextil      ,              ws_maquina_rede              ,
              ws_aplicacao              ,              'D'                          ,
              sysdate);
   end if;

end inter_tr_crec_110_hist;

-- ALTER TRIGGER "INTER_TR_CREC_110_HIST" ENABLE
 

/

exec inter_pr_recompile;

