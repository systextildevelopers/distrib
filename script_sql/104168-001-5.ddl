--prompt "ATEN��O! O comando abaixo devera demorar para executar, e n�o deve ser abortado."

declare
   v_contador  number (9) := 0;
begin
   for reg_estq_040 in (select rowid from estq_040 where estq_040.data_imagem is null)
   loop
      update estq_040
      set    estq_040.qtde_estoque_syst_imagem = 0.000,
             estq_040.data_imagem             = to_date('01-01-2014','dd-mm-yyyy'),
             estq_040.qtde_estoque_wms_imagem = 0.000
      where  estq_040.rowid = reg_estq_040.rowid;
      v_contador :=  v_contador + 1;

         if v_contador > 1000 then
            commit;
            v_contador := 0;
         end if;  
   end loop;  
   commit;
end;

/

exec inter_pr_recompile;
