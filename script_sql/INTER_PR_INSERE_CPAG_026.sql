create or replace PROCEDURE INTER_PR_INSERE_CPAG_026
    ( p_conta in number, p_banco in number,  p_data_movimento in date,
    p_debito_credito in varchar2 , p_codigo_historico in number,  p_valor_cheque in number,
    p_origem_lcto in number,  p_codigo_evento in number,  p_historico varchar2, p_data_pre_datado in date, p_num_lanc_contab  in number, p_nr_documento in number default 0)
is
v_sequencia_lcto number := 0;
v_sequencia_insert number := 0;
ok boolean := false;
v_valor_cheque number := 0;
v_estorno cont_010.sinal_titulo%type;
v_atualiza_conta cont_010.conta_corrente%type;
v_controla_conta cpag_020.controla_conta%type;

begin

    BEGIN
        select h.sinal_titulo, h.conta_corrente into v_estorno, v_atualiza_conta
        from cont_010 h where h.codigo_historico = p_historico;
    exception when others then
      v_estorno := 0;
      v_atualiza_conta := 0;
    END;

    BEGIN
      select controla_conta INTO v_controla_conta from cpag_020 where portador= p_banco and conta_corrente= p_conta;
    exception when others then
       v_controla_conta := 0;
    END;

   IF v_controla_conta = 1 /*AND v_estorno = 1*/ AND v_atualiza_conta = 1 --removido a validação do estorno para poder atualizar movimentação no conta corrente também quando for estorno caso atualize conta
   THEN
dbms_output.put_line('cpag_026............');
     begin
       select cpag_026.sequencia_lcto 
         into v_sequencia_lcto
         from cpag_026 
         where cpag_026.banco            =  p_banco
           and cpag_026.conta_corrente   =  p_conta
           and cpag_026.data_movimento   = p_data_movimento
           and cpag_026.codigo_historico = p_codigo_historico
           and cpag_026.debito_credito   = p_debito_credito
           and cpag_026.origem_lcto      = 2 
           and nvl(cpag_026.nr_documento,0) = p_nr_documento;
 		 dbms_output.put_line('cpag_026 v_sequencia_lcto............'||v_sequencia_lcto);
     EXCEPTION WHEN NO_DATA_FOUND then
         
         begin
             select nvl(max(cpag_026.sequencia_lcto),0) + 1
             into v_sequencia_insert
             from cpag_026 
             where cpag_026.banco            =  p_banco
               and cpag_026.conta_corrente   = p_conta
               and cpag_026.data_movimento   = p_data_movimento;
         end;
dbms_output.put_line('cpag_026 INSERT v_sequencia_lcto............'||v_sequencia_lcto);
         while ok = false 
         LOOP
             begin                          
                 insert into cpag_026 (
                 banco,                     conta_corrente, 
                 data_movimento,            sequencia_lcto, 
                 debito_credito,            codigo_historico, 
                 valor_cheque,              origem_lcto, 
                 codigo_evento,             historico, 
                 data_pre_datado,          num_contabil,
                 nr_documento
                 ) values (
                 p_banco ,                     p_conta ,
                 p_data_movimento ,     v_sequencia_insert , 
                 p_debito_credito ,         p_codigo_historico , 
                 p_valor_cheque ,          p_origem_lcto , 
                 p_codigo_evento ,        p_historico , 
                 p_data_pre_datado,      p_num_lanc_contab,
                 p_nr_documento);


                 ok := true;
            exception 
                 when others then 
                    ok := false;
                    v_sequencia_insert := v_sequencia_insert + 1;
            end;    
         end loop;
         
     end;
     
     if v_sequencia_lcto is not null then
         update cpag_026 
         set valor_cheque = valor_cheque + p_valor_cheque 
         where banco            = p_banco 
           and conta_corrente   = p_conta 
           and data_movimento   = p_data_movimento
           and sequencia_lcto   = v_sequencia_lcto
           and nvl(nr_documento,0)  = p_nr_documento;
     end if;
     if p_debito_credito = 'D' then
        v_valor_cheque := p_valor_cheque * -1; 
     else
        v_valor_cheque := p_valor_cheque;
     end if;
     
     inter_pr_insere_cpag_027(p_banco, p_conta, p_data_movimento, v_valor_cheque);

   
   END IF;
   
end INTER_PR_INSERE_CPAG_026;
