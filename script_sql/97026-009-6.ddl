  Create table obrf_740 (
    COD_EMPRESA              number(03)    default 0    not null ,
    MES                      number(02)    default 0    not null ,
    ANO                      number(04)    default 0    not null ,
    UF                       varchar2(2)   default ' '  not null,
  DT_INI                   date,
  DT_FIM                   date,
  IND_MOV_DIFAL            number(01)    default 0,
  VL_SLD_CRED_ANT_DIFAL    number(13,2)  default 0.00,
  VL_TOT_DEBITOS_DIFAL     number(13,2)  default 0.00,
  VL_OUT_DEB_DIFAL         number(13,2)  default 0.00,
  VL_TOT_DEB_FCP           number(13,2)  default 0.00,
  VL_TOT_CREDITOS_DIFAL    number(13,2)  default 0.00,
  VL_TOT_CRED_FCP          number(13,2)  default 0.00,
  VL_OUT_CRED_DIFAL        number(13,2)  default 0.00,
  VL_SLD_DEV_ANT_DIFAL     number(13,2)  default 0.00,
  VL_DEDUCOES_DIFAL        number(13,2)  default 0.00,
  VL_RECOL                 number(13,2)  default 0.00,
  VL_SLD_CRED_TRANSPORTAR  number(13,2)  default 0.00,
  DEB_ESP_DIFAL            number(13,2)  default 0.00 );
    
  alter table obrf_740
  add constraint PK_OBRF_740 primary key (COD_EMPRESA, MES, ANO,UF );
  
  comment on column obrf_740.COD_EMPRESA              is 'Codigo de empresa para registro E310 sped';  
  comment on column obrf_740.MES                      is 'Mes para registro E310 sped';  
  comment on column obrf_740.ANO                      is 'ano para registro E310 sped';  
  comment on column obrf_740.UF                       is 'Estado para registro E310 sped';  
  comment on column obrf_740.IND_MOV_DIFAL            is 'Indicador de movimento 0 � Sem opera��es com ICMS Diferencial de Aliquota da UF de Origem/Destino1 � Com opera��es de ICMS Diferencial de Al�quota da UF de Origem/Destino';
  comment on column obrf_740.VL_SLD_CRED_ANT_DIFAL    is 'Valor do Saldo credor de periodo anterior � ICMS Diferencial de Aliquota da UF de Origem/Destino';
  comment on column obrf_740.VL_TOT_DEBITOS_DIFAL     is 'Valor total dos d�bitos por Saidas e prestacoes com debito do ICMS referente ao diferencial de aliquota devido a UF do Remetente/Destinatario';
  comment on column obrf_740.VL_OUT_DEB_DIFAL         is 'Valor Total dos ajustes outros debitos ICMS Diferencial de Aliquota da UF de Origem/Destino e Estorno de creditos ICMS Diferencial de Aliquota da UF de Origem/Destino ';
  comment on column obrf_740.VL_TOT_DEB_FCP           is 'Valor total dos d�bitos FCP por Saidas e prestacoes';
  comment on column obrf_740.VL_TOT_CREDITOS_DIFAL    is 'Valor total dos creditos do ICMS referente ao diferencial de aliquota devido a UF dos Remetente/Destinatario ';
  comment on column obrf_740.VL_TOT_CRED_FCP          is 'Valor total dos cr�ditos FCP por Entradas';
  comment on column obrf_740.VL_OUT_CRED_DIFAL        is 'Valor total de Ajustes Outros creditos ICMS Diferencial de Aliquota da UF de Origem/Destino e Estorno de debitos ICMS Diferencial de Aliquota da UF de Origem/Destin.';
  comment on column obrf_740.VL_SLD_DEV_ANT_DIFAL     is '10 -Valor total de Saldo devedor ICMS Diferencial de Aliquota da UF de Origem/Destino antes das deducoes';
  comment on column obrf_740.VL_DEDUCOES_DIFAL        is '11 -Valor total dos ajustes Deducoes ICMS Diferencial de Aliquota da UF de Origem/Destino';
  comment on column obrf_740.VL_RECOL                 is '12 -Valor recolhido ou a recolher referente a FCP e Imposto do Diferencial de Al�quota da UF de Origem/Destino (10-11)';
  comment on column obrf_740.VL_SLD_CRED_TRANSPORTAR  is '13 -Saldo credor a transportar para o periodo seguinte referente a FCP e Imposto do Diferencial de Aliquota da UF de Origem/Destino'; 
  comment on column obrf_740.DEB_ESP_DIFAL            is '14 -Valores recolhidos ou a recolher, extra apuracao.';

  exec inter_pr_recompile;
