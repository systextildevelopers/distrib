ALTER TABLE haus_001
ADD codigo_historico_pgto NUMBER(4);

ALTER TABLE haus_001
ADD cod_portador NUMBER(3);

ALTER TABLE haus_001
ADD path_processar VARCHAR2(100);

ALTER TABLE haus_001
ADD path_processados VARCHAR2(100);

ALTER TABLE haus_001
ADD path_processados_erro VARCHAR2(100);

ALTER TABLE haus_002
DROP COLUMN tipo_registro;

ALTER TABLE haus_002
DROP COLUMN numero_pedido;

ALTER TABLE haus_002
DROP COLUMN data_faturamento;

ALTER TABLE haus_002
DROP COLUMN descricao_problema;

ALTER TABLE haus_003
ADD tipo_registro number(1);

/ 
exec inter_pr_recompile;
