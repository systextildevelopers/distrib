alter table fatu_060 add cod_estagio_agrupador_prod number(2);
comment on column fatu_060.cod_estagio_agrupador_prod is 'Estagio Agrupador do estagio de producao do produto produzido que foi terceirizado (codificacao de produtos intermediarios para o BlocoK)';

alter table fatu_060 add cod_estagio_agrupador_insu number(2);
comment on column fatu_060.cod_estagio_agrupador_insu is 'Estagio Agrupador do estagio anterior do insumo (nivel 1 produzido anteriormente) que foi terceirizado (codificacao de produtos intermediarios para o BlocoK)';

alter table fatu_060 add cod_estagio_simultaneo_insu number(2);
comment on column fatu_060.cod_estagio_simultaneo_insu is 'Estagio Agrupador Simultaneo do estagio anterior do insumo (nivel 1 produzido anteriormente) que foi terceirizado (codificacao de produtos intermediarios para o BlocoK)';


exec inter_pr_recompile;
