create table EMPR_008_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(20) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(20) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  CODIGO_EMPRESA_OLD NUMBER(3) ,  
  CODIGO_EMPRESA_NEW NUMBER(3) , 
  PARAM_OLD          VARCHAR2(40) ,  
  PARAM_NEW          VARCHAR2(40) , 
  VAL_STR_OLD        VARCHAR2(4000),  
  VAL_STR_NEW        VARCHAR2(4000), 
  VAL_INT_OLD        NUMBER(9),  
  VAL_INT_NEW        NUMBER(9), 
  VAL_DBL_OLD        NUMBER,  
  VAL_DBL_NEW        NUMBER, 
  VAL_DAT_OLD        DATE, 
  VAL_DAT_NEW        DATE 
); 
