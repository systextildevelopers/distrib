create table fatu_732 (
    codigo_empresa          number(3)       not null,
    num_nota_cobranca       number(9)       not null,
    serie_nota_cobranca     varchar2(3)     not null,
    num_nota_retorno        number(9)       not null,
    serie_nota_retorno      varchar2(3)     not null,
    data_hora_ins           date                            default sysdate
);

alter table fatu_732 add constraint PK_fatu_732 primary key (codigo_empresa, num_nota_cobranca, serie_nota_cobranca, num_nota_retorno, serie_nota_retorno);

comment on table fatu_732 is 'Relacionamento entre nota de cobranca e nota de retorno para o processo de faturamento de containers';

comment on column fatu_732.codigo_empresa       is 'Codigo da empresa do faturamento';
comment on column fatu_732.num_nota_cobranca    is 'Numero da nota de cobranca';
comment on column fatu_732.serie_nota_cobranca  is 'Serie da nota de cobranca';
comment on column fatu_732.num_nota_retorno     is 'Numero da nota de retorno';
comment on column fatu_732.serie_nota_retorno   is 'Serie da nota de retorno';
comment on column fatu_732.data_hora_ins        is 'Data/hora insercao registro';
