create or replace trigger inter_tr_HDOC_001_813_log
after insert or delete or update
on HDOC_001
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin

-- Dados do usu?rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);



    begin
       select hdoc_090.programa
       into v_nome_programa
       from hdoc_090
       where hdoc_090.sid = ws_sid
         and rownum       = 1
         and hdoc_090.programa not like '%menu%'
         and hdoc_090.programa not like '%_m%';
       exception
         when no_data_found then            v_nome_programa := 'SQL';
    end;



 if inserting
 then

  if :new.tipo = 313 and :new.codigo = 1
  then

    begin

      insert into HDOC_001_PEDI_F813_LOG (
         TIPO_OCORR,   /*0*/
         DATA_OCORR,   /*1*/
         HORA_OCORR,   /*2*/
         USUARIO_REDE,   /*3*/
         MAQUINA_REDE,   /*4*/
         APLICACAO,   /*5*/
         USUARIO_SISTEMA,   /*6*/
         NOME_PROGRAMA,   /*7*/
         TIPO_OLD,   /*8*/
         TIPO_NEW,   /*9*/
         CODIGO_OLD,   /*10*/
         CODIGO_NEW,   /*11*/
         DESCRICAO_OLD,   /*12*/
         DESCRICAO_NEW,   /*13*/
         COLECAO_OLD,   /*14*/
         COLECAO_NEW,   /*15*/
         LINHA_PRODUTO_OLD,   /*16*/
         LINHA_PRODUTO_NEW,   /*17*/
         NUMERO_INTERNO_OLD,   /*18*/
         NUMERO_INTERNO_NEW,   /*19*/
         CONS_PED_DESP_ADIC_OLD,   /*20*/
         CONS_PED_DESP_ADIC_NEW,   /*21*/
         COD_CANCELAMENTO_OLD,   /*22*/
         COD_CANCELAMENTO_NEW,   /*23*/
         AGRUPA_COL_DIFER_OLD,   /*24*/
         AGRUPA_COL_DIFER_NEW,   /*25*/
         VALOR_MAX_SALDO_OLD,   /*26*/
         VALOR_MAX_SALDO_NEW,   /*27*/
         TRANSF_DEP_DIFER_OLD,   /*28*/
         TRANSF_DEP_DIFER_NEW,   /*29*/
         TRANSF_TAB_PRECO_DIFER_OLD,   /*30*/
         TRANSF_TAB_PRECO_DIFER_NEW,   /*31*/
         TRANSF_NR_INT_DIFER_OLD,   /*32*/
         TRANSF_NR_INT_DIFER_NEW,   /*33*/
         TRANSF_PERC_COMIS_DIFER_OLD,   /*34*/
         TRANSF_PERC_COMIS_DIFER_NEW,   /*35*/
         ENVIA_DADOS_ADICIONAIS_OLD,   /*36*/
         ENVIA_DADOS_ADICIONAIS_NEW,   /*37*/
         ENVIA_OBSERVACAO_OLD,   /*38*/
         ENVIA_OBSERVACAO_NEW,   /*39*/
         TIPOS_DE_CLIENTE_OLD,   /*40*/
         TIPOS_DE_CLIENTE_NEW,   /*41*/
         NATUREZAS_OLD,   /*42*/
         NATUREZAS_NEW,   /*43*/
         AGRUPA_LINHA_DIFER_OLD,   /*44*/
         AGRUPA_LINHA_DIFER_NEW,   /*45*/
         AGRUPA_NAO_FATURAMENTO_OLD,   /*46*/
         AGRUPA_NAO_FATURAMENTO_NEW,   /*47*/
         CONS_PED_ORIG_DEST_OLD,   /*48*/
         CONS_PED_ORIG_DEST_NEW,   /*49*/
         AGRUPA_TIPO_FRETE_DIFER_OLD,   /*50*/
         AGRUPA_TIPO_FRETE_DIFER_NEW,   /*51*/
         AGRUPA_TAB_PRECO_DIFER_OLD,   /*52*/
         AGRUPA_TAB_PRECO_DIFER_NEW,   /*53*/
         ATU_PRECO_ITEM_INCLUSAO_OLD,   /*54*/
         ATU_PRECO_ITEM_INCLUSAO_NEW,   /*55*/
         AGRUPA_COND_PAGTO_DIFER_OLD,   /*56*/
         AGRUPA_COND_PAGTO_DIFER_NEW,   /*57*/
         AGRUPA_TIPO_CLI_DIFER_OLD,   /*58*/
         AGRUPA_TIPO_CLI_DIFER_NEW,   /*59*/
         AGRUPA_COD_FUNC_DIFER_OLD,   /*60*/
         AGRUPA_COD_FUNC_DIFER_NEW,   /*61*/
         AGRUPA_PERC_COMIS_DIFER_OLD,   /*62*/
         AGRUPA_PERC_COMIS_DIFER_NEW,   /*63*/
         UNIFIC_PERC_DESC_DIFER_OLD,   /*64*/
         UNIFIC_PERC_DESC_DIFER_NEW,   /*65*/
         TRANSF_FORMA_PAGTO_DIFER_OLD,   /*66*/
         TRANSF_FORMA_PAGTO_DIFER_NEW,   /*67*/
         TRANSF_COND_PAGTO_DIFER_OLD,   /*68*/
         TRANSF_COND_PAGTO_DIFER_NEW    /*69*/
      ) values (
        'I', /*o*/
        sysdate, /*1*/
        sysdate,/*2*/
        ws_usuario_rede,/*3*/
        ws_maquina_rede, /*4*/
        ws_aplicativo, /*5*/
        ws_usuario_systextil,/*6*/
        v_nome_programa, /*7*/
         0,/*8*/
         :new.TIPO, /*9*/
         0,/*10*/
         :new.CODIGO, /*11*/
         '',/*12*/
         :new.DESCRICAO, /*13*/
         0,/*14*/
         :new.CAMPO_NUMERICO01      , /*15*/
         0,/*16*/
         :new.CAMPO_NUMERICO02  , /*17*/
         0,/*18*/
         :new.CAMPO_NUMERICO03, /*19*/
         0,/*20*/
         :new.CAMPO_NUMERICO04, /*21*/
         0,/*22*/
         :new.CAMPO_NUMERICO05, /*23*/
         0,/*24*/
         :new.CAMPO_NUMERICO06, /*25*/
         0,/*26*/
         :new.VALOR01, /*27*/
         0,/*28*/
         :new.CAMPO_NUMERICO20, /*29*/
         0,/*30*/
         :new.CAMPO_NUMERICO21, /*31*/
         0,/*32*/
         :new.CAMPO_NUMERICO22, /*33*/
         0,/*34*/
         :new.CAMPO_NUMERICO23, /*35*/
         0,/*36*/
         :new.CAMPO_NUMERICO24, /*37*/
         0,/*38*/
         :new.CAMPO_NUMERICO25, /*39*/
         '',/*40*/
         :new.DESCRICAO7, /*41*/
         '',/*42*/
         :new.DESCRICAO9, /*43*/
         0,/*44*/
         :new.CAMPO_NUMERICO07, /*45*/
         0,/*46*/
         :new.CAMPO_NUMERICO09, /*47*/
         0,/*48*/
         :new.CAMPO_NUMERICO10, /*49*/
         0,/*50*/
         :new.CAMPO_NUMERICO11, /*51*/
         0,/*52*/
         :new.CAMPO_NUMERICO12, /*53*/
         0,/*54*/
         :new.CAMPO_NUMERICO13, /*55*/
         0,/*56*/
         :new.CAMPO_NUMERICO15, /*57*/
         0,/*58*/
         :new.CAMPO_NUMERICO16, /*59*/
         0,/*60*/
         :new.CAMPO_NUMERICO17, /*61*/
         0,/*62*/
         :new.CAMPO_NUMERICO18, /*63*/
         0,/*64*/
         :new.CAMPO_NUMERICO26, /*65*/
         0,/*66*/
         :new.CAMPO_NUMERICO27, /*67*/
         0,/*68*/
         :new.CAMPO_NUMERICO28 /*69*/
       );
    end;
  end if;
 end if;


 if updating
 then

  if (:new.tipo = 313 and :new.codigo = 1) or (:old.tipo = 313 and :old.codigo = 1)
  then
    begin
      insert into HDOC_001_PEDI_F813_LOG (
         TIPO_OCORR, /*0*/
         DATA_OCORR, /*1*/
         HORA_OCORR, /*2*/
         USUARIO_REDE, /*3*/
         MAQUINA_REDE, /*4*/
         APLICACAO, /*5*/
         USUARIO_SISTEMA, /*6*/
         NOME_PROGRAMA, /*7*/
         TIPO_OLD, /*8*/
         TIPO_NEW, /*9*/
         CODIGO_OLD, /*10*/
         CODIGO_NEW, /*11*/
         DESCRICAO_OLD, /*12*/
         DESCRICAO_NEW, /*13*/
         COLECAO_OLD, /*14*/
         COLECAO_NEW, /*15*/
         LINHA_PRODUTO_OLD, /*16*/
         LINHA_PRODUTO_NEW, /*17*/
         NUMERO_INTERNO_OLD, /*18*/
         NUMERO_INTERNO_NEW, /*19*/
         CONS_PED_DESP_ADIC_OLD, /*20*/
         CONS_PED_DESP_ADIC_NEW, /*21*/
         COD_CANCELAMENTO_OLD, /*22*/
         COD_CANCELAMENTO_NEW, /*23*/
         AGRUPA_COL_DIFER_OLD, /*24*/
         AGRUPA_COL_DIFER_NEW, /*25*/
         VALOR_MAX_SALDO_OLD, /*26*/
         VALOR_MAX_SALDO_NEW, /*27*/
         TRANSF_DEP_DIFER_OLD, /*28*/
         TRANSF_DEP_DIFER_NEW, /*29*/
         TRANSF_TAB_PRECO_DIFER_OLD, /*30*/
         TRANSF_TAB_PRECO_DIFER_NEW, /*31*/
         TRANSF_NR_INT_DIFER_OLD, /*32*/
         TRANSF_NR_INT_DIFER_NEW, /*33*/
         TRANSF_PERC_COMIS_DIFER_OLD, /*34*/
         TRANSF_PERC_COMIS_DIFER_NEW, /*35*/
         ENVIA_DADOS_ADICIONAIS_OLD, /*36*/
         ENVIA_DADOS_ADICIONAIS_NEW, /*37*/
         ENVIA_OBSERVACAO_OLD, /*38*/
         ENVIA_OBSERVACAO_NEW, /*39*/
         TIPOS_DE_CLIENTE_OLD, /*40*/
         TIPOS_DE_CLIENTE_NEW, /*41*/
         NATUREZAS_OLD, /*42*/
         NATUREZAS_NEW, /*43*/
         AGRUPA_LINHA_DIFER_OLD, /*44*/
         AGRUPA_LINHA_DIFER_NEW, /*45*/
         AGRUPA_NAO_FATURAMENTO_OLD, /*46*/
         AGRUPA_NAO_FATURAMENTO_NEW, /*47*/
         CONS_PED_ORIG_DEST_OLD, /*48*/
         CONS_PED_ORIG_DEST_NEW, /*49*/
         AGRUPA_TIPO_FRETE_DIFER_OLD, /*50*/
         AGRUPA_TIPO_FRETE_DIFER_NEW, /*51*/
         AGRUPA_TAB_PRECO_DIFER_OLD, /*52*/
         AGRUPA_TAB_PRECO_DIFER_NEW, /*53*/
         ATU_PRECO_ITEM_INCLUSAO_OLD, /*54*/
         ATU_PRECO_ITEM_INCLUSAO_NEW, /*55*/
         AGRUPA_COND_PAGTO_DIFER_OLD, /*56*/
         AGRUPA_COND_PAGTO_DIFER_NEW, /*57*/
         AGRUPA_TIPO_CLI_DIFER_OLD, /*58*/
         AGRUPA_TIPO_CLI_DIFER_NEW, /*59*/
         AGRUPA_COD_FUNC_DIFER_OLD, /*60*/
         AGRUPA_COD_FUNC_DIFER_NEW, /*61*/
         AGRUPA_PERC_COMIS_DIFER_OLD, /*62*/
         AGRUPA_PERC_COMIS_DIFER_NEW, /*63*/
         UNIFIC_PERC_DESC_DIFER_OLD, /*64*/
         UNIFIC_PERC_DESC_DIFER_NEW, /*65*/
         TRANSF_FORMA_PAGTO_DIFER_OLD, /*66*/
         TRANSF_FORMA_PAGTO_DIFER_NEW, /*67*/
         TRANSF_COND_PAGTO_DIFER_OLD, /*68*/
         TRANSF_COND_PAGTO_DIFER_NEW  /*69*/
      ) values (
        'A', /*0*/
        sysdate, /*1*/
        sysdate, /*2*/
        ws_usuario_rede,/*3*/
        ws_maquina_rede, /*4*/
        ws_aplicativo, /*5*/
        ws_usuario_systextil,/*6*/
        v_nome_programa, /*7*/
         :old.TIPO,  /*8*/
         :new.TIPO, /*9*/
         :old.CODIGO,  /*10*/
         :new.CODIGO, /*11*/
         :old.DESCRICAO,  /*12*/
         :new.DESCRICAO, /*13*/
         :old.CAMPO_NUMERICO01      ,  /*14*/
         :new.CAMPO_NUMERICO01      , /*15*/
         :old.CAMPO_NUMERICO02  ,  /*16*/
         :new.CAMPO_NUMERICO02  , /*17*/
         :old.CAMPO_NUMERICO03,  /*18*/
         :new.CAMPO_NUMERICO03, /*19*/
         :old.CAMPO_NUMERICO04,  /*20*/
         :new.CAMPO_NUMERICO04, /*21*/
         :old.CAMPO_NUMERICO05,  /*22*/
         :new.CAMPO_NUMERICO05, /*23*/
         :old.CAMPO_NUMERICO06,  /*24*/
         :new.CAMPO_NUMERICO06, /*25*/
         :old.VALOR01,  /*26*/
         :new.VALOR01, /*27*/
         :old.CAMPO_NUMERICO20,  /*28*/
         :new.CAMPO_NUMERICO20, /*29*/
         :old.CAMPO_NUMERICO21,  /*30*/
         :new.CAMPO_NUMERICO21, /*31*/
         :old.CAMPO_NUMERICO22,  /*32*/
         :new.CAMPO_NUMERICO22, /*33*/
         :old.CAMPO_NUMERICO23,  /*34*/
         :new.CAMPO_NUMERICO23, /*35*/
         :old.CAMPO_NUMERICO24,  /*36*/
         :new.CAMPO_NUMERICO24, /*37*/
         :old.CAMPO_NUMERICO25,  /*38*/
         :new.CAMPO_NUMERICO25, /*39*/
         :old.DESCRICAO7,  /*40*/
         :new.DESCRICAO7, /*41*/
         :old.DESCRICAO9,  /*42*/
         :new.DESCRICAO9, /*43*/
         :old.CAMPO_NUMERICO07,  /*44*/
         :new.CAMPO_NUMERICO07, /*45*/
         :old.CAMPO_NUMERICO09,  /*46*/
         :new.CAMPO_NUMERICO09, /*47*/
         :old.CAMPO_NUMERICO10,  /*48*/
         :new.CAMPO_NUMERICO10, /*49*/
         :old.CAMPO_NUMERICO11,  /*50*/
         :new.CAMPO_NUMERICO11, /*51*/
         :old.CAMPO_NUMERICO12,  /*52*/
         :new.CAMPO_NUMERICO12, /*53*/
         :old.CAMPO_NUMERICO13,  /*54*/
         :new.CAMPO_NUMERICO13, /*55*/
         :old.CAMPO_NUMERICO15,  /*56*/
         :new.CAMPO_NUMERICO15, /*57*/
         :old.CAMPO_NUMERICO16,  /*58*/
         :new.CAMPO_NUMERICO16, /*59*/
         :old.CAMPO_NUMERICO17,  /*60*/
         :new.CAMPO_NUMERICO17, /*61*/
         :old.CAMPO_NUMERICO18,  /*62*/
         :new.CAMPO_NUMERICO18, /*63*/
         :old.CAMPO_NUMERICO26,  /*64*/
         :new.CAMPO_NUMERICO26, /*65*/
         :old.CAMPO_NUMERICO27,  /*66*/
         :new.CAMPO_NUMERICO27, /*67*/
         :old.CAMPO_NUMERICO28,  /*68*/
         :new.CAMPO_NUMERICO28  /*69*/
       );
    end;
  end if;
 end if;


 if deleting
 then

  if (:old.tipo = 313 and :old.codigo = 1)
  then
    begin
      insert into HDOC_001_PEDI_F813_LOG (
         TIPO_OCORR, /*0*/
         DATA_OCORR, /*1*/
         HORA_OCORR, /*2*/
         USUARIO_REDE, /*3*/
         MAQUINA_REDE, /*4*/
         APLICACAO, /*5*/
         USUARIO_SISTEMA, /*6*/
         NOME_PROGRAMA, /*7*/
         TIPO_OLD, /*8*/
         TIPO_NEW, /*9*/
         CODIGO_OLD, /*10*/
         CODIGO_NEW, /*11*/
         DESCRICAO_OLD, /*12*/
         DESCRICAO_NEW, /*13*/
         COLECAO_OLD, /*14*/
         COLECAO_NEW, /*15*/
         LINHA_PRODUTO_OLD, /*16*/
         LINHA_PRODUTO_NEW, /*17*/
         NUMERO_INTERNO_OLD, /*18*/
         NUMERO_INTERNO_NEW, /*19*/
         CONS_PED_DESP_ADIC_OLD, /*20*/
         CONS_PED_DESP_ADIC_NEW, /*21*/
         COD_CANCELAMENTO_OLD, /*22*/
         COD_CANCELAMENTO_NEW, /*23*/
         AGRUPA_COL_DIFER_OLD, /*24*/
         AGRUPA_COL_DIFER_NEW, /*25*/
         VALOR_MAX_SALDO_OLD, /*26*/
         VALOR_MAX_SALDO_NEW, /*27*/
         TRANSF_DEP_DIFER_OLD, /*28*/
         TRANSF_DEP_DIFER_NEW, /*29*/
         TRANSF_TAB_PRECO_DIFER_OLD, /*30*/
         TRANSF_TAB_PRECO_DIFER_NEW, /*31*/
         TRANSF_NR_INT_DIFER_OLD, /*32*/
         TRANSF_NR_INT_DIFER_NEW, /*33*/
         TRANSF_PERC_COMIS_DIFER_OLD, /*34*/
         TRANSF_PERC_COMIS_DIFER_NEW, /*35*/
         ENVIA_DADOS_ADICIONAIS_OLD, /*36*/
         ENVIA_DADOS_ADICIONAIS_NEW, /*37*/
         ENVIA_OBSERVACAO_OLD, /*38*/
         ENVIA_OBSERVACAO_NEW, /*39*/
         TIPOS_DE_CLIENTE_OLD, /*40*/
         TIPOS_DE_CLIENTE_NEW, /*41*/
         NATUREZAS_OLD, /*42*/
         NATUREZAS_NEW, /*43*/
         AGRUPA_LINHA_DIFER_OLD, /*44*/
         AGRUPA_LINHA_DIFER_NEW, /*45*/
         AGRUPA_NAO_FATURAMENTO_OLD, /*46*/
         AGRUPA_NAO_FATURAMENTO_NEW, /*47*/
         CONS_PED_ORIG_DEST_OLD, /*48*/
         CONS_PED_ORIG_DEST_NEW, /*49*/
         AGRUPA_TIPO_FRETE_DIFER_OLD, /*50*/
         AGRUPA_TIPO_FRETE_DIFER_NEW, /*51*/
         AGRUPA_TAB_PRECO_DIFER_OLD, /*52*/
         AGRUPA_TAB_PRECO_DIFER_NEW, /*53*/
         ATU_PRECO_ITEM_INCLUSAO_OLD, /*54*/
         ATU_PRECO_ITEM_INCLUSAO_NEW, /*55*/
         AGRUPA_COND_PAGTO_DIFER_OLD, /*56*/
         AGRUPA_COND_PAGTO_DIFER_NEW, /*57*/
         AGRUPA_TIPO_CLI_DIFER_OLD, /*58*/
         AGRUPA_TIPO_CLI_DIFER_NEW, /*59*/
         AGRUPA_COD_FUNC_DIFER_OLD, /*60*/
         AGRUPA_COD_FUNC_DIFER_NEW, /*61*/
         AGRUPA_PERC_COMIS_DIFER_OLD, /*62*/
         AGRUPA_PERC_COMIS_DIFER_NEW, /*63*/
         UNIFIC_PERC_DESC_DIFER_OLD, /*64*/
         UNIFIC_PERC_DESC_DIFER_NEW, /*65*/
         TRANSF_FORMA_PAGTO_DIFER_OLD, /*66*/
         TRANSF_FORMA_PAGTO_DIFER_NEW, /*67*/
         TRANSF_COND_PAGTO_DIFER_OLD, /*68*/
         TRANSF_COND_PAGTO_DIFER_NEW /*69*/
      ) values (
        'D', /*0*/
        sysdate, /*1*/
        sysdate, /*2*/
        ws_usuario_rede,/*3*/
        ws_maquina_rede,/*4*/
        ws_aplicativo, /*5*/
        ws_usuario_systextil,/*6*/
        v_nome_programa, /*7*/
         :old.TIPO, /*8*/
         0, /*9*/
         :old.CODIGO, /*10*/
         0, /*11*/
         :old.DESCRICAO, /*12*/
         '', /*13*/
         :old.CAMPO_NUMERICO01      , /*14*/
         0, /*15*/
         :old.CAMPO_NUMERICO02  , /*16*/
         0, /*17*/
         :old.CAMPO_NUMERICO03, /*18*/
         0, /*19*/
         :old.CAMPO_NUMERICO04, /*20*/
         0, /*21*/
         :old.CAMPO_NUMERICO05, /*22*/
         0, /*23*/
         :old.CAMPO_NUMERICO06, /*24*/
         0, /*25*/
         :old.VALOR01, /*26*/
         0, /*27*/
         :old.CAMPO_NUMERICO20, /*28*/
         0, /*29*/
         :old.CAMPO_NUMERICO21, /*30*/
         0, /*31*/
         :old.CAMPO_NUMERICO22, /*32*/
         0, /*33*/
         :old.CAMPO_NUMERICO23, /*34*/
         0, /*35*/
         :old.CAMPO_NUMERICO24, /*36*/
         0, /*37*/
         :old.CAMPO_NUMERICO25, /*38*/
         0, /*39*/
         :old.DESCRICAO7, /*40*/
         '', /*41*/
         :old.DESCRICAO9, /*42*/
         '', /*43*/
         :old.CAMPO_NUMERICO07, /*44*/
         0, /*45*/
         :old.CAMPO_NUMERICO09, /*46*/
         0, /*47*/
         :old.CAMPO_NUMERICO10, /*48*/
         0, /*49*/
         :old.CAMPO_NUMERICO11, /*50*/
         0, /*51*/
         :old.CAMPO_NUMERICO12, /*52*/
         0, /*53*/
         :old.CAMPO_NUMERICO13, /*54*/
         0, /*55*/
         :old.CAMPO_NUMERICO15, /*56*/
         0, /*57*/
         :old.CAMPO_NUMERICO16, /*58*/
         0, /*59*/
         :old.CAMPO_NUMERICO17, /*60*/
         0, /*61*/
         :old.CAMPO_NUMERICO18, /*62*/
         0, /*63*/
         :old.CAMPO_NUMERICO26, /*64*/
         0, /*65*/
         :old.CAMPO_NUMERICO27, /*66*/
         0, /*67*/
         :old.CAMPO_NUMERICO28, /*68*/
         0 /*69*/
       );
    end;
  end if;
 end if;
end inter_tr_HDOC_001_813_log;
