create or replace view inter_vi_fatu_780 as
select fatu_780.PEDIDO, fatu_780.SEQUENCIA, fatu_780.NIVEL, fatu_780.GRUPO, fatu_780.SUBGRUPO, fatu_780.ITEM,
        fatu_780.DEPOSITO, fatu_780.LOTE, fatu_780.NR_SOLICITACAO, fatu_780.TIPO_REGISTRO,
        fatu_780.qtde_pedida, DECODE(NVL(fatu_780.seq_conjunto, 0), 0, fatu_780.QTDE_SUGERIDA, fatu_780.qtde_coleta) QTDE_SUGERIDA, fatu_780.QTDE_DISPONIVEL, fatu_780.QTDE_AFATURAR,
        fatu_780.NR_SUGESTAO, fatu_780.NOME_PROGRAMA, fatu_780.REF_ORIGINAL, fatu_780.EXECUTA_TRIGGER,
        nvl((select min(endereco) from estq_110
            where nivel   =fatu_780.nivel
              and grupo   =fatu_780.grupo
              and subgrupo=fatu_780.subgrupo
              and item    =fatu_780.item
              and deposito=fatu_780.deposito), 'ZZZZZ') endereco,
        basi_020.sequencia_tamanho
    from fatu_780, basi_020, pedi_100
    where fatu_780.subgrupo = basi_020.tamanho_ref
      and fatu_780.nivel = basi_020.basi030_nivel030
      and fatu_780.grupo = basi_020.basi030_referenc
      and DECODE(NVL(fatu_780.seq_conjunto, 0), 0, fatu_780.QTDE_SUGERIDA, fatu_780.qtde_coleta) > 0
      and pedi_100.pedido_venda = fatu_780.pedido
      and (   ( pedi_100.ind_coletado_cem_porcento = 0 and pedi_100.ind_col_embarque_cem_porcento = 0 )
           or ( pedi_100.ind_coletado_cem_porcento = 1
           and  exists (
           ( select 1 from pedi_110 p
             where  p.pedido_venda     = fatu_780.pedido
             and    p.cd_it_pe_nivel99 = fatu_780.nivel
             and    p.cd_it_pe_grupo   = fatu_780.grupo
             and p.cod_cancelamento = 0
             group by  p.cd_it_pe_grupo
             having sum(p.qtde_pedida - p.qtde_faturada) = sum(p.qtde_sugerida)
             and sum(p.qtde_pedida - p.qtde_faturada)  > 0)
           )) 
           or ( pedi_100.ind_col_embarque_cem_porcento = 1
           and  exists (            
           (select 1 from basi_590 g
             where g.nivel = fatu_780.nivel
               and g.grupo = fatu_780.grupo
               and g.subgrupo = fatu_780.subgrupo
               and g.item = fatu_780.item
               and exists (select i.grupo_embarque from inter_vi_grupo_emb_cod_sug_100 i 
                            where i.pedido_venda = pedi_100.pedido_venda
                              and i.grupo_embarque = g.grupo_embarque))
           )));
