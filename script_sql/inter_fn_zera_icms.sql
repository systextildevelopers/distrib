
  CREATE OR REPLACE FUNCTION "INTER_FN_ZERA_ICMS" (p_cod_empresa in number, p_nat_oper in number, p_nat_oper_est in varchar2, p_cvf_icms in number) return int is

  Result int;
  v_consiste_cvf_icms pedi_080.consiste_cvf_icms %type;
  v_ind_empr_simples          fatu_503.ind_empresa_simples%type;
begin

   /* Autor - Thiago - 728 - SS: 60446/001
      Descricao funcao: Funcao foi criada porque existem casos que o ICMS deve ser zerado, porem a p_gera_Nfe
      nao estava fazendo corretamente, por isso foi feita essa funcao para fazer igual a procedure do sped c100
   */

   -- EMPRESA DO SIMPLES NACIONAL
   begin
      select fatu_503.ind_empresa_simples
      into   v_ind_empr_simples
      from fatu_503
      where fatu_503.codigo_empresa = p_cod_empresa;
   exception
       when no_data_found then
          v_ind_empr_simples := '3';
   end;

   begin
     select trim(pedi_080.consiste_cvf_icms)
     into   v_consiste_cvf_icms
     from pedi_080
     where pedi_080.natur_operacao = p_nat_oper
       and pedi_080.estado_natoper = p_nat_oper_est;
     exception when no_data_found then
         v_consiste_cvf_icms := 0;
   end;

   if p_cvf_icms >= 30 and p_cvf_icms <> 70 and v_consiste_cvf_icms < 3 and v_ind_empr_simples <> '1'
   then
       Result := 1;
   else
       Result := 0;
   end if;

  return(Result);
end INTER_FN_ZERA_ICMS;

 

/

exec inter_pr_recompile;

