CREATE OR REPLACE VIEW PCPC_888_DET
(
  NUMERO_SOLICITACAO,
  COD_EST_IDENT_CONS,
  COD_EST_CONS,
  DATA_EMISSAO,
  NUMERO_ORDEM,
  SEQ_ORDEM_SERVICO,
  SITUACAO_SOLICITACAO,
  SITUACAO_OS,
  SITUACAO_TAG,
  TIPO_SOLICITACAO_CONSERTO,
  CODIGO_CANCELAMENTO,
  ORDEM_PRODUCAO,
  ORDEM_CONFECCAO,
  PERIODO_PRODUCAO,
  CODIGO_BARRAS,
  TAG_ATUALIZA_CONSERTO
)
AS 
select pcpc_888.numero_solicitacao               as numero_solicitacao,
       nvl(pcpc_888.cod_est_ident_cons ,0)       as cod_est_ident_cons,
       nvl(pcpc_880.estagio_conserto ,0)         as cod_est_cons,
       pcpc_888.data_emissao                     as data_emissao,
       pcpc_888.numero_ordem                     as numero_ordem,
       obrf_089.sequencia                        as seq_ordem_servico,
       pcpc_888.situacao_solicitacao             as situacao_solicitacao,
       obrf_080.situacao_ordem                   as situacao_os,
       obrf_089.situacao_item                    as situacao_tag,
       obrf_089.tipo_solicitacao_conserto        as tipo_solicitacao_conserto,
       pcpc_888.codigo_cancelamento              as codigo_cancelamento,
       obrf_089.ordem_producao                   as ordem_producao,
       obrf_089.ordem_confeccao                  as ordem_confeccao,
       obrf_089.periodo_producao                 as periodo_producao,
       obrf_089.codigo_barras                    as codigo_barras,
       obrf_089.tag_atualiza_conserto            as tag_atualiza_conserto
from pcpc_888, obrf_089, obrf_080, pcpc_880
where pcpc_888.numero_solicitacao = obrf_089.numero_solicitacao(+)
  and pcpc_888.numero_ordem       = obrf_080.numero_ordem(+)
  and pcpc_888.numero_solicitacao = pcpc_880.numero_solicitacao(+)
group by pcpc_888.numero_solicitacao,
       pcpc_888.cod_est_ident_cons,
       pcpc_880.estagio_conserto,
       pcpc_888.data_emissao,
       pcpc_888.numero_ordem,
       obrf_089.sequencia,
       pcpc_888.situacao_solicitacao,
       obrf_080.situacao_ordem,
       obrf_089.situacao_item,
       obrf_089.tipo_solicitacao_conserto,
       pcpc_888.codigo_cancelamento,
       obrf_089.ordem_producao,
       obrf_089.ordem_confeccao,
       obrf_089.periodo_producao,
       obrf_089.codigo_barras,
       obrf_089.tag_atualiza_conserto;

exec inter_pr_recompile;
