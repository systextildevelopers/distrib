create table basi_l369
(
  uf_origem         varchar2(2) default '',
  uf_destino        varchar2(2) default '',
  perc_icms_destino number(5,2) default 0.0
);

alter table basi_l369
  add primary key (uf_origem, uf_destino);
