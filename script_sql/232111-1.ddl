ALTER TABLE EMPR_050 ADD OBRIGA_ZERO NUMBER(1);
COMMENT ON COLUMN EMPR_050.OBRIGA_ZERO IS 'Com sa�da autom�tica e leitura cont�nua estabilizada, obriga a receber zero antes de nova pesagem.';
