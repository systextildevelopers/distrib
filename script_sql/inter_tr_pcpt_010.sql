
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_010" 
   before insert or
          delete or
          update of periodo_producao,  cod_cancelamento,
                    qtde_quilos_prod,  qtde_rolos_prod,
                    qtde_quilos_prog,  qtde_rolos_prog
   on pcpt_010
   for each row

declare
   t_saldo_rolos_ordem   pcpt_010.qtde_quilos_prog%type;
   t_calculo_nec_tecel   integer;
   qtde_reg_saldo_zero   number;
   v_codigo_empresa      number;
begin
   begin
      select empr_002.calculo_nec_tecel
      into t_calculo_nec_tecel
      from empr_002;
      exception when others then
         t_calculo_nec_tecel := 0;
   end;

   if inserting
   then
      v_codigo_empresa := 0;

      begin
         select pcpc_010.codigo_empresa
         into    v_codigo_empresa
         from pcpc_010
         where pcpc_010.area_periodo     = 4
           and pcpc_010.periodo_producao = :new.periodo_producao;
      exception when others then
         v_codigo_empresa := 0;
      end;

      inter_pr_valida_sit_projeto(:new.cd_pano_nivel99, :new.cd_pano_grupo, v_codigo_empresa);

      -- Insere produto para planejamento da producao.
      insert into tmrp_041
         (periodo_producao,      area_producao,
          nr_pedido_ordem,       seq_pedido_ordem,
          codigo_estagio,        nivel_estrutura,
          grupo_estrutura,       subgru_estrutura,
          item_estrutura,        qtde_reservada,
          qtde_areceber,         consumo)
      values
         (:new.periodo_producao,  4,
          :new.ordem_tecelagem,   0,
          0,                      :new.cd_pano_nivel99,
          :new.cd_pano_grupo,     :new.cd_pano_subgrupo,
          :new.cd_pano_item,      0.00,
          :new.qtde_quilos_prog,  0.00);

      -- Procedure que executa a explosao da estrutura do produto
      -- que sera produzido.
      inter_pr_explode_estrutura(:new.cd_pano_nivel99,
                                 :new.cd_pano_grupo,
                                 :new.cd_pano_subgrupo,
                                 :new.cd_pano_item,
                                 :new.alternativa_item,
                                 :new.periodo_producao,
                                 :new.qtde_quilos_prog,
                                 :new.ordem_tecelagem,
                                 0,
                                 4,
                                 0,
                                 0,
                                 0,
                                 0.00,
                                 0,
                                 'pcpb', 1            );

   end if;

   if updating
   then
      -- Se informou codigo de cancelamento para a OT deleta do
      -- planejamento da producao.
      if :new.cod_cancelamento > 0
      then
         -- Deleta o produto da ordem de tecelagem e seus componentes.
         delete tmrp_041
         where tmrp_041.periodo_producao = :old.periodo_producao
           and tmrp_041.area_producao    = 4
           and tmrp_041.nr_pedido_ordem  = :old.ordem_tecelagem;

         -- Deleta a ligacao entre a ordem de tecelagem e a ordem de planejamento
         -- So quando tiver tipo de destino = 5
         for reg2 in (select pcpt_016.ordem_pedido, pcpt_016.quantidade
                        from pcpt_016
                        where pcpt_016.ordem_tecelagem = :old.ordem_tecelagem
                          and pcpt_016.tipo_destino    = 5)
         loop
            -- Procedimento que vai executar a explosao da estrutura
            -- do produto e atualizar a quantidade programada na ordem de planejamento
            -- Para retirar a quantidade da ordem antiga
            inter_pr_ordem_planejamento('pcpt',
                                        reg2.ordem_pedido,
                                        :old.cd_pano_nivel99,
                                        :old.cd_pano_grupo,
                                        :old.cd_pano_subgrupo,
                                        :old.cd_pano_item,
                                        :old.alternativa_item,
                                        reg2.quantidade * -1);

            -- Excluir ligacao com a ordem de planejamento
            delete from tmrp_630
            where tmrp_630.ordem_planejamento    = reg2.ordem_pedido
              and tmrp_630.area_producao         = 4
              and tmrp_630.ordem_prod_compra     = :old.ordem_tecelagem
              and tmrp_630.nivel_produto         = :old.cd_pano_nivel99
              and tmrp_630.grupo_produto         = :old.cd_pano_grupo
              and tmrp_630.subgrupo_produto      = :old.cd_pano_subgrupo
              and tmrp_630.item_produto          = :old.cd_pano_item;
         end loop;
      end if;

      -- Se alterar o periodo de producao da OT.
      if :old.periodo_producao <> :new.periodo_producao
      then
         -- Altera periodo de producao do planejamento, se alterar o periodo de producao da OT
         update tmrp_041
         set tmrp_041.periodo_producao   = :new.periodo_producao
         where tmrp_041.periodo_producao = :old.periodo_producao
           and tmrp_041.area_producao    = 4
           and tmrp_041.nr_pedido_ordem  = :old.ordem_tecelagem;
      end if;

      -- Quando for atualizada a quantidade produzida da OT
      -- executa o recalculo para o planejamento.
      if :old.qtde_quilos_prod <> :new.qtde_quilos_prod
      or :old.qtde_rolos_prod  <> :new.qtde_rolos_prod
      then
         if t_calculo_nec_tecel = 0
         then t_saldo_rolos_ordem := ((:new.qtde_rolos_prod - :old.qtde_rolos_prod) * :old.peso_rolo);
         else t_saldo_rolos_ordem := :new.qtde_quilos_prod - :old.qtde_quilos_prod;
         end if;

         if :old.qtde_rolos_prog >= :new.qtde_rolos_prod and t_saldo_rolos_ordem >= 0.000
         then
            update tmrp_041
            set tmrp_041.qtde_areceber  = tmrp_041.qtde_areceber  - t_saldo_rolos_ordem
            where tmrp_041.periodo_producao    = :old.periodo_producao
              and tmrp_041.area_producao       = 4
              and tmrp_041.nr_pedido_ordem     = :old.ordem_tecelagem
              and tmrp_041.sequencia_estrutura = 0;

            update tmrp_041
            set tmrp_041.qtde_reservada = tmrp_041.qtde_reservada - (t_saldo_rolos_ordem * tmrp_041.consumo)
            where tmrp_041.periodo_producao    = :old.periodo_producao
              and tmrp_041.area_producao       = 4
              and tmrp_041.nr_pedido_ordem     = :old.ordem_tecelagem
              and tmrp_041.sequencia_estrutura > 0;
         end if;
         --Adicionado lógica para deletar registro da tmrp caso for produzido for mais que a programada, 
         --para que não seja necessário realizar o recalculo para ajustar saldos negativos no painel de planejamento.
         if :old.qtde_rolos_prog >= :new.qtde_rolos_prod and t_saldo_rolos_ordem <= 0.000
         then
           begin
             delete tmrp_041
             where tmrp_041.periodo_producao    =  :old.periodo_producao
               and tmrp_041.area_producao       =  4
               and tmrp_041.nr_pedido_ordem     =  :old.ordem_tecelagem;
           end;
         end if;
      end if;

      -- Quando for alterada a quantidade de Rolos ou Quilos programados da
      -- Ordem de Tecelagem, executa o recalculo para o planejamento.
      if :old.qtde_quilos_prog <> :new.qtde_quilos_prog
      or :old.qtde_rolos_prog  <> :new.qtde_rolos_prog
      then
         if t_calculo_nec_tecel = 0
         then t_saldo_rolos_ordem := ((:new.qtde_rolos_prog - :old.qtde_rolos_prog) * :old.peso_rolo);
         else t_saldo_rolos_ordem := :new.qtde_quilos_prog - :old.qtde_quilos_prog;
         end if;

         if t_saldo_rolos_ordem <> 0.000
         then
            update tmrp_041
            set tmrp_041.qtde_areceber  = tmrp_041.qtde_areceber  + t_saldo_rolos_ordem
            where tmrp_041.periodo_producao    = :old.periodo_producao
              and tmrp_041.area_producao       = 4
              and tmrp_041.nr_pedido_ordem     = :old.ordem_tecelagem
              and tmrp_041.sequencia_estrutura = 0;

            update tmrp_041
            set tmrp_041.qtde_reservada = tmrp_041.qtde_reservada + (t_saldo_rolos_ordem * tmrp_041.consumo)
            where tmrp_041.periodo_producao    = :old.periodo_producao
              and tmrp_041.area_producao       = 4
              and tmrp_041.nr_pedido_ordem     = :old.ordem_tecelagem
              and tmrp_041.sequencia_estrutura > 0;
         end if;
      end if;

      -- Elimina do planejamento a quantidade necessaria para ordem de tecelagem
      -- e seus componentes.
      begin
         select nvl(count(*),0)
         into qtde_reg_saldo_zero
         from tmrp_041
         where tmrp_041.periodo_producao    =  :old.periodo_producao
           and tmrp_041.area_producao       =  4
           and tmrp_041.nr_pedido_ordem     =  :old.ordem_tecelagem
           and tmrp_041.qtde_reservada      <= 0
           and tmrp_041.qtde_areceber        <= 0;
      exception when others then
         qtde_reg_saldo_zero := 0;
      end;


      if qtde_reg_saldo_zero > 0
      then
        begin
           delete tmrp_041
           where tmrp_041.periodo_producao    =  :old.periodo_producao
             and tmrp_041.area_producao       =  4
             and tmrp_041.nr_pedido_ordem     =  :old.ordem_tecelagem
             and tmrp_041.qtde_reservada      <= 0
             and tmrp_041.qtde_areceber        <= 0;
         end;
      end if;
   end if;

   if deleting
   then
      -- Elimina do planejamento a quantidade necessaria para ordem de tecelagem
      -- e seus componentes.
      delete tmrp_041
      where tmrp_041.periodo_producao    = :old.periodo_producao
        and tmrp_041.area_producao       = 4
        and tmrp_041.nr_pedido_ordem     = :old.ordem_tecelagem;

      -- Verifica se tem algum destino para Ordem de Planejamento 5
      for reg2 in (select pcpt_016.ordem_pedido, pcpt_016.quantidade
                   from pcpt_016
                   where pcpt_016.ordem_tecelagem = :old.ordem_tecelagem
                     and pcpt_016.tipo_destino    = 5)
      loop
         -- Procedimento que vai executar a explosao da estrutura
         -- do produto e atualizar a quantidade programada na ordem de planejamento
         -- Para retirar a quantidade da ordem antiga
         inter_pr_ordem_planejamento('exp1',
                                     reg2.ordem_pedido,
                                     :old.cd_pano_nivel99,
                                     :old.cd_pano_grupo,
                                     :old.cd_pano_subgrupo,
                                     :old.cd_pano_item,
                                     :old.alternativa_item,
                                     reg2.quantidade * -1);

         -- Excluir ligacao com a ordem de planejamento
         delete from tmrp_630
         where tmrp_630.ordem_planejamento    = reg2.ordem_pedido
           and tmrp_630.area_producao         = 4
           and tmrp_630.ordem_prod_compra     = :old.ordem_tecelagem
           and tmrp_630.nivel_produto         = :old.cd_pano_nivel99
           and tmrp_630.grupo_produto         = :old.cd_pano_grupo
           and tmrp_630.subgrupo_produto      = :old.cd_pano_subgrupo
           and tmrp_630.item_produto          = :old.cd_pano_item;
      end loop;
   end if;
end inter_tr_pcpt_010;

-- ALTER TRIGGER "INTER_TR_PCPT_010" ENABLE
 

/

exec inter_pr_recompile;

