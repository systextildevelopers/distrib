
  CREATE OR REPLACE PROCEDURE "INTER_PR_ESTOQUE" (p_nivel_estrutura in varchar2, p_grupo_estrutura  in varchar2, p_subgrupo_estrutura       in varchar2,
                                              p_item_estrutura  in varchar2, p_codigo_transacao in number,   p_codigo_deposito          in number,
                                              p_numero_lote     in number,   p_entrada_saida    in varchar2,
                                              p_data_movimento  in date,     p_quantidade       in number,   p_valor_movimento_unitario in number,
                                              p_flag_elimina    in number)
is
   v_nr_dias_mov_retr_e       number;
   v_nr_dias_mov_retr_s       number;

   v_entrada_saida            varchar2(1);
   v_atual_estoque_f          varchar2(1);
   v_atualiza_estoque         number;
   v_quantidade_atu           number;
   v_quantidade_mes           number;
   v_valor_total              number;
   v_ano                      number;
   v_mes                      number;
   v_quantidade               number;
   v_data_ult_entrada         date;
   v_data_ult_saida           date;
   v_final_periodo_estoque    date;


   v_comprado_fabric          number;
   v_calcula_preco            number;
   v_preco_medio              number;
   v_estoque_total            number;
   v_qtde_estoque_f_total     number;
   v_estoque_anterior         number;
   v_gera_estq_negativo_1     number;
   v_gera_estq_negativo_2     number;
   v_gera_estq_negativo_4     number;
   v_gera_estq_negativo_7     number;
   v_gera_estq_negativo_9     number;
   v_gera_estq_negativo       number;
   v_codigo_empresa           number;
   v_par_atualiza_estoque_f   number;
   v_quantidade_estoque_f     number;
   v_flag_fecha_estq          number;

   v_data_atual               date := trunc(sysdate,'DD');
   v_data_minima              date;

   erro_executando_fecham     exception;

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_processo_systextil      varchar2(20);

   v_mensagem_erro           varchar(500);

   v_pagina_apex             number;
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   begin
      select hdoc_090.programa
      into   v_processo_systextil
      from hdoc_090
      where hdoc_090.sid       = ws_sid
        and hdoc_090.instancia = userenv('INSTANCE')
        and hdoc_090.programa  not like '%menu%'
        and hdoc_090.programa  not like  '%!_m%'escape'!'
        and rownum             = 1;
   exception when no_data_found then
      v_processo_systextil := ws_aplicativo;
   end;

   -- verifica se a transacao atualiza estoque
   begin
      select atualiza_estoque,   entrada_saida,   calcula_preco,   atual_estoque_f
      into   v_atualiza_estoque, v_entrada_saida, v_calcula_preco, v_atual_estoque_f
      from estq_005
      where codigo_transacao = p_codigo_transacao;

      exception
         when no_data_found
         then
--            raise_application_error(-20000,'Transacao informada nao cadastrada.' || to_char(p_codigo_transacao,'999'));
            raise_application_error(-20000,inter_fn_buscar_tag('ds26559#ATENCAO! Transacao informada nao cadastrada.',ws_locale_usuario,ws_usuario_systextil) || to_char(p_codigo_transacao,'999'));
   end;

   if v_atualiza_estoque <> 1
   then
--      raise_application_error(-20000,'Transacao informada nao atualiza estoque.');
      raise_application_error(-20000,inter_fn_buscar_tag('ds26560#ATENCAO! Transacao informada nao atualiza estoque.',ws_locale_usuario,ws_usuario_systextil)||' Trans: '||p_codigo_transacao);
   end if;

   -- verifica se a transacao e do mesmo tipo do movimento "entrada/saida"
   if (((v_entrada_saida = 'S' or  v_entrada_saida = 'T') and p_entrada_saida <> 'S')
   or  (v_entrada_saida = 'E' and p_entrada_saida <> 'E')) and p_codigo_transacao <> 0
   then
--      raise_application_error(-20000,'Transacao nao compativel com movimento.');
      raise_application_error(-20000,inter_fn_buscar_tag('ds26561#ATENCAO! Transacao nao compativel com movimento.',ws_locale_usuario,ws_usuario_systextil));
   end if;

   -- verifica se e uma inclusao no estq_300 ou se e uma eliminacao
   -- p_flag_elimina = 0 (insercao), p_flag_elimina = 1 (eliminacao)
   if p_flag_elimina = 0
   then
      v_quantidade := p_quantidade;

      -- calcula valor total do movimento
      if p_quantidade <> 0.000
      then
         v_valor_total := p_quantidade * p_valor_movimento_unitario;
      else
         v_valor_total := p_valor_movimento_unitario;
      end if;

   else
      v_quantidade := p_quantidade * (-1.0);

      -- calcula valor total do movimento
      if p_quantidade <> 0.000
      then
         v_valor_total := p_quantidade * p_valor_movimento_unitario * (-1.0);
      else
         v_valor_total := p_valor_movimento_unitario * (-1.0);
      end if;

   end if;

   -- troca o sinal da quantidade, se esta for uma saida
   if p_entrada_saida = 'E'
   then
      v_quantidade_atu := v_quantidade;
   else

      v_quantidade_atu := v_quantidade * (-1.0);
   end if;

   -- verifica se o estoque vai ficar negativa. Se ficar, nao deixa a movimentacao se confirmar
   select fatu_502.nr_dias_mov_retr_e,         fatu_502.nr_dias_mov_retr_s,
          fatu_501.codigo_empresa,             fatu_502.atualiza_estoque_f
   into   v_nr_dias_mov_retr_e,                v_nr_dias_mov_retr_s,
          v_codigo_empresa,                    v_par_atualiza_estoque_f
   from fatu_501,fatu_502, basi_205
   where fatu_501.codigo_empresa  = basi_205.local_deposito
     and fatu_502.codigo_empresa  = basi_205.local_deposito
     and basi_205.codigo_deposito = p_codigo_deposito;

   begin
      -- verifica parametro se permite deixar o estoque negativo para o deposito/nivel
      select nvl(hdoc_001.campo_numerico01,0),      nvl(hdoc_001.campo_numerico02,0),
             nvl(hdoc_001.campo_numerico03,0),      nvl(hdoc_001.campo_numerico04,0),
             nvl(hdoc_001.campo_numerico05,0)
      into   v_gera_estq_negativo_1,         v_gera_estq_negativo_2,
             v_gera_estq_negativo_4,         v_gera_estq_negativo_7,
             v_gera_estq_negativo_9
      from hdoc_001
      where tipo   = 81
        and codigo = p_codigo_deposito;

   exception
      when no_data_found
      then begin

         -- verifica parametro se permite deixar o estoque negativo para o deposito/nivel
         select nvl(hdoc_001.campo_numerico01,0),      nvl(hdoc_001.campo_numerico02,0),
                nvl(hdoc_001.campo_numerico03,0),      nvl(hdoc_001.campo_numerico04,0),
                nvl(hdoc_001.campo_numerico05,0)
         into   v_gera_estq_negativo_1,         v_gera_estq_negativo_2,
                v_gera_estq_negativo_4,         v_gera_estq_negativo_7,
                v_gera_estq_negativo_9
         from hdoc_001
         where tipo   = 81
           and codigo = 0;

         exception
            when no_data_found then
               v_gera_estq_negativo_1 := 0;
               v_gera_estq_negativo_2 := 0;
               v_gera_estq_negativo_4 := 0;
               v_gera_estq_negativo_7 := 0;
               v_gera_estq_negativo_9 := 0;
      end;
   end;

   v_gera_estq_negativo := 0;

   if p_nivel_estrutura = '1'
   then
      v_gera_estq_negativo := v_gera_estq_negativo_1;
   end if;

   if p_nivel_estrutura = '2'
   then
      v_gera_estq_negativo := v_gera_estq_negativo_2;
   end if;

   if p_nivel_estrutura = '4'
   then
      v_gera_estq_negativo := v_gera_estq_negativo_4;
   end if;

   if p_nivel_estrutura = '7'
   then
      v_gera_estq_negativo := v_gera_estq_negativo_7;
   end if;

   if p_nivel_estrutura = '9'
   then
      v_gera_estq_negativo := v_gera_estq_negativo_9;
   end if;

   -- se o numero de dias permitido para movimentacao retroativa <> 99 e se
   -- a data do movimento for <> da data atual (sysdate),
   -- entao verifica se pode ou nao realizar a movimentacao.
   if (v_nr_dias_mov_retr_e <> 99 or v_nr_dias_mov_retr_s <> 99)
   and p_data_movimento < v_data_atual
   and v_processo_systextil <> 'pcpc_f235'
   and v_processo_systextil <> 'systextilweb'
   then

      if p_entrada_saida = 'E' and v_nr_dias_mov_retr_e <> 99
      then

         -- se o numero de dias permitidos retroativos for zero, gera mensagem de erro
         -- e nao verifica data minima, pois a data minima e a propria data_atual
         if v_nr_dias_mov_retr_e = 0
         then
            raise_application_error(-20000,inter_fn_buscar_tag('ds26563#ATENCAO! Data da movimentacao retroativa inferior a permitida.',ws_locale_usuario,ws_usuario_systextil));
         end if;

         -- acha a data minima que se pode movimentar o estoque
         v_data_minima := v_data_atual - v_nr_dias_mov_retr_e;

         -- se a data da movimentacao for menor que a minima calculada
         -- acha a data imediatamente anterior que e util
         if p_data_movimento < v_data_minima
         then
            begin

               select max(data_calendario)
               into   v_data_minima
               from basi_260
               where data_calendario <= v_data_minima
               and   dia_util        = 0;

               exception
                  when others then
                     raise_application_error(-20000,inter_fn_buscar_tag('ds26562#ATENCAO! Calendario nao encontrado.',ws_locale_usuario,ws_usuario_systextil));
            end;

            if p_data_movimento < v_data_minima
            then
               raise_application_error(-20000,inter_fn_buscar_tag('ds26563#ATENCAO! Data da movimentacao retroativa inferior a permitida.',ws_locale_usuario,ws_usuario_systextil));
            end if;
         end if;
      elsif p_entrada_saida = 'S' and v_nr_dias_mov_retr_s <> 99
      then

         -- se o numero de dias permitidos retroativos for zero, gera mensagem de erro
         -- e nao verifica data minima, pois a data minima e a propria data_atual
         if v_nr_dias_mov_retr_s = 0
         then
            raise_application_error(-20000,inter_fn_buscar_tag('ds26563#ATENCAO! Data da movimentacao retroativa inferior a permitida.',ws_locale_usuario,ws_usuario_systextil));
         end if;

         -- acha a data minima que se pode movimentar o estoque
         v_data_minima := v_data_atual - v_nr_dias_mov_retr_s;

         -- se a data da movimentacao for menor que a minima calculada
         -- acha a data imediatamente anterior que e util
         if p_data_movimento < v_data_minima
         then
            begin

               select max(data_calendario)
               into   v_data_minima
               from basi_260
               where data_calendario <= v_data_minima
               and   dia_util        = 0;

               exception
                  when others then
                     raise_application_error(-20000,inter_fn_buscar_tag('ds26562#ATENCAO! Calendario nao encontrado.',ws_locale_usuario,ws_usuario_systextil));
            end;

            if p_data_movimento < v_data_minima
            then
               raise_application_error(-20000,inter_fn_buscar_tag('ds26563#ATENCAO! Data da movimentacao retroativa inferior a permitida.',ws_locale_usuario,ws_usuario_systextil));
            end if;
         end if;
      end if;
   end if;


   if v_gera_estq_negativo = 1
   then

      if (p_entrada_saida = 'S' and p_flag_elimina = 0)
      or (p_entrada_saida = 'E' and p_flag_elimina = 1)
      then

         begin
            select qtde_estoque_atu
            into   v_estoque_total
            from estq_040
            where cditem_nivel99  = p_nivel_estrutura
            and   cditem_grupo    = p_grupo_estrutura
            and   cditem_subgrupo = p_subgrupo_estrutura
            and   cditem_item     = p_item_estrutura
            and   lote_acomp      = p_numero_lote
            and   deposito        = p_codigo_deposito;

         exception
            when NO_DATA_FOUND then
               v_estoque_total := 0.00;
         end;

         begin
            execute immediate 'select v(''APP_PAGE_ID'') into :v_pagina_apex from dual' into v_pagina_apex;
            exception when others then
               v_pagina_apex := null;
         end;

         if (v_estoque_total - p_quantidade) < 0.00 and (v_pagina_apex is null or v_pagina_apex != 437)
         then
            raise_application_error(-20101,
               inter_fn_buscar_tag('ds26564#ATENCAO! Estoque insuficiente para a movimentacao.',ws_locale_usuario,ws_usuario_systextil) ||
               (case when v_processo_systextil in ('JDBC') then ' ' else chr(10) end)          ||
               inter_fn_buscar_tag('lb08048#DEPOSITO:',ws_locale_usuario,ws_usuario_systextil)                                          ||
               to_char(p_codigo_deposito, '000')                                                                                        ||
               (case when v_processo_systextil in ('JDBC') then ' ' else chr(10) end)          ||
               inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)                                           ||
               p_nivel_estrutura     || '.'                                                                                             ||
               p_grupo_estrutura     || '.'                                                                                             ||
               p_subgrupo_estrutura  || '.'                                                                                             ||
               p_item_estrutura                                                                                                         ||
               (case when v_processo_systextil in ('JDBC') then ' ' else chr(10) end)		    ||
               inter_fn_buscar_tag('lb00236#LOTE:',ws_locale_usuario,ws_usuario_systextil)                                              ||
               to_char(p_numero_lote, '000000'));
         end if;
      end if;
   end if;

   v_quantidade_estoque_f:= 0.000;

   -- verifica se empresa e a transacao atualiza deposito ativo
   if v_par_atualiza_estoque_f = 1 and v_atual_estoque_f = 'S'
   then
      v_quantidade_estoque_f:= v_quantidade_atu;

      if (p_entrada_saida = 'S' and p_flag_elimina = 0)
      or (p_entrada_saida = 'E' and p_flag_elimina = 1)
      then

         begin
            select qtde_estoque_f
            into   v_qtde_estoque_f_total
            from estq_040
            where cditem_nivel99  = p_nivel_estrutura
            and   cditem_grupo    = p_grupo_estrutura
            and   cditem_subgrupo = p_subgrupo_estrutura
            and   cditem_item     = p_item_estrutura
            and   lote_acomp      = p_numero_lote
            and   deposito        = p_codigo_deposito;

         exception
            when NO_DATA_FOUND then
               v_qtde_estoque_f_total := 0.00;
         end;

         if (v_qtde_estoque_f_total - p_quantidade) < 0.00
         then
            raise_application_error(-20000,inter_fn_buscar_tag('ds26565#ATENCAO! Estoque insuficiente para a movimentacao gerencial.',ws_locale_usuario,ws_usuario_systextil) ||
										   (case when v_processo_systextil in ('JDBC') then ' ' else chr(10) end)          ||
										   inter_fn_buscar_tag('lb08048#DEPOSITO:',ws_locale_usuario,ws_usuario_systextil)                                          ||
										   to_char(p_codigo_deposito, '000')                                                                                        ||
										   (case when v_processo_systextil in ('JDBC') then ' ' else chr(10) end)          ||
										   inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)                                           ||
										   p_nivel_estrutura     || '.'                                                                                             ||
										   p_grupo_estrutura     || '.'                                                                                             ||
										   p_subgrupo_estrutura  || '.'                                                                                             ||
										   p_item_estrutura                                                                                                         ||
										   (case when v_processo_systextil in ('JDBC') then ' ' else chr(10) end)          ||
										   inter_fn_buscar_tag('lb00236#LOTE:',ws_locale_usuario,ws_usuario_systextil)                                              ||
										   to_char(p_numero_lote, '000000'));
         end if;
      end if;
   end if;




   -- bloco para atualizacao de saldos de estoque
   begin
      select data_ult_entrada,  data_ult_saida
      into   v_data_ult_entrada, v_data_ult_saida
      from estq_040
      where cditem_nivel99  = p_nivel_estrutura
      and   cditem_grupo    = p_grupo_estrutura
      and   cditem_subgrupo = p_subgrupo_estrutura
      and   cditem_item     = p_item_estrutura
      and   lote_acomp      = p_numero_lote
      and   deposito        = p_codigo_deposito;

      -- encontra se a data de movimentacao e maior que a ultima movimentacao
      if p_entrada_saida = 'E'
      then

         if v_data_ult_entrada is null or v_data_ult_entrada < p_data_movimento
         then
            v_data_ult_entrada := p_data_movimento;
         end if;

      else

         if v_data_ult_saida is null or v_data_ult_saida < p_data_movimento
         then
            v_data_ult_saida := p_data_movimento;
         end if;

      end if;

      exception
         when NO_DATA_FOUND
         then

            if p_entrada_saida = 'E'
            then
               v_data_ult_entrada := p_data_movimento;
               v_data_ult_saida   := null;
            else
               v_data_ult_entrada := null;
               v_data_ult_saida   := p_data_movimento;
            end if;
   end;

   begin

      loop

         select flag_fecha_estq
         into   v_flag_fecha_estq
         from empr_002;

         exit when v_flag_fecha_estq < 2;

         DBMS_LOCK.sleep(10);  -- espera 10 segundos para verificar novamente se o estoque foi liberado

      end loop;

      -- Verificando qual e o ultimo dia do mes
      select trunc(last_day(periodo_estoque),'DD')
      into v_final_periodo_estoque
      from empr_001;

      --Quando o movimento for do proximo mes nao atualiza a quantidade estoque mes.

      if p_data_movimento <= v_final_periodo_estoque
      then
         v_quantidade_mes := v_quantidade_atu;

      else
         v_quantidade_mes := 0.000;

      end if;

      update estq_040
      set qtde_estoque_atu = qtde_estoque_atu + v_quantidade_atu,
          qtde_estoque_mes = qtde_estoque_mes + v_quantidade_mes,
          data_ult_entrada = v_data_ult_entrada,
          data_ult_saida   = v_data_ult_saida,
          qtde_estoque_f   = qtde_estoque_f + v_quantidade_estoque_f
      where cditem_nivel99  = p_nivel_estrutura
      and   cditem_grupo    = p_grupo_estrutura
      and   cditem_subgrupo = p_subgrupo_estrutura
      and   cditem_item     = p_item_estrutura
      and   lote_acomp      = p_numero_lote
      and   deposito        = p_codigo_deposito;

      if SQL%notfound
      then

     
         insert into estq_040 (
            cditem_nivel99,      cditem_grupo,
            cditem_subgrupo,     cditem_item,
            lote_acomp,          deposito,
            qtde_estoque_atu,    qtde_estoque_mes,
            data_ult_entrada,    data_ult_saida,
            qtde_estoque_f,      data_imagem)
         values (
            p_nivel_estrutura,    p_grupo_estrutura,
            p_subgrupo_estrutura, p_item_estrutura,
            p_numero_lote,        p_codigo_deposito,
            v_quantidade_atu,     v_quantidade_mes,
            v_data_ult_entrada,   v_data_ult_saida,
            v_quantidade_estoque_f, trunc(sysdate) );
      end if;

      exception
         when NO_DATA_FOUND
         then
            raise_application_error(-20103,inter_fn_buscar_tag('ds26566#ATENÇÃO! Não encontrou tabela EMPR_001 / EMPR_002.',ws_locale_usuario,ws_usuario_systextil));

         when erro_executando_fecham
         then
            raise_application_error (-20102,inter_fn_buscar_tag('ds26567#ATENÇÃO! Fechamento de estoques esta sendo executado. Não pode movimentar estoques.',ws_locale_usuario,ws_usuario_systextil));

         when others
         then
            raise_application_error (-20000,inter_fn_buscar_tag('ds26568#ATENÇÃO! Não atualizou a tabela de saldos de estoques.',ws_locale_usuario,ws_usuario_systextil) 
            || ' P ' || p_nivel_estrutura || '.' || p_grupo_estrutura || '.' ||  p_subgrupo_estrutura || '.' || p_item_estrutura 
            || ' L ' || p_numero_lote  
            || ' D ' || p_codigo_deposito  
            || ' EA ' || v_quantidade_atu 
            || ' EM ' || v_quantidade_mes  
            || ' EF ' || v_quantidade_estoque_f  || Chr(10) || SQLERRM);
   end;


   -- atualiza a tabela de acumulado (estq_020)
   begin
      v_ano := to_number(to_char(p_data_movimento,'YYYY'));
      v_mes := to_number(to_char(p_data_movimento,'MM'));

      update estq_020
      set quantidade = quantidade + v_quantidade,
          valor      = valor      + v_valor_total
      where ano_consumo       = v_ano
      and   mes_consumo       = v_mes
      and   item_con_nivel99  = p_nivel_estrutura
      and   item_con_grupo    = p_grupo_estrutura
      and   item_con_subgrupo = p_subgrupo_estrutura
      and   item_con_item     = p_item_estrutura
      and   codigo_deposito   = p_codigo_deposito
      and   transacao         = p_codigo_transacao
      and   lote_acomp        = p_numero_lote;

      if SQL%notfound
      then

         insert into estq_020
           (ano_consumo,          mes_consumo,
            item_con_nivel99,     item_con_grupo,
            item_con_subgrupo,    item_con_item,
            codigo_deposito,      transacao,
            lote_acomp,           quantidade,
            valor)
         values
           (v_ano,                v_mes,
            p_nivel_estrutura,    p_grupo_estrutura,
            p_subgrupo_estrutura, p_item_estrutura,
            p_codigo_deposito,    p_codigo_transacao,
            p_numero_lote,        v_quantidade,
            v_valor_total);

      end if;

      exception
      when OTHERS
         then begin
            if v_valor_total > 99999999999.99
            then v_mensagem_erro := chr(10) ||
               'ATENÇÃO! O valor total do seguinte produto é maior que o aceitável pela tabela. Verifique se a quantidade e o valor unitário estão corretos!' ||
               chr(10) ||
               'PRODUTO: ' ||
               p_nivel_estrutura || '.' ||
               p_grupo_estrutura || '.' ||
               p_subgrupo_estrutura  || '.' ||
               p_item_estrutura ||
               chr(10) ||
               'DEPÓSITO: ' ||
               to_char(p_codigo_deposito, '000') ||
               chr(10) ||
               'QUANTIDADE: ' ||
               v_quantidade ||
               chr(10) ||
               'VALOR TOTAL: ' ||
               v_valor_total;
            else v_mensagem_erro := '';
            end if;

            raise_application_error (-20000,inter_fn_buscar_tag('ds26569#ATENÇÃO! Não atualizou a tabela de acumulados de estoques',ws_locale_usuario,ws_usuario_systextil) || v_mensagem_erro);
         end;

   end;

   if p_flag_elimina = 0
   then
      select data_ult_entrada,   data_ult_saida,   preco_medio
      into   v_data_ult_entrada, v_data_ult_saida, v_preco_medio
      from basi_010
      where nivel_estrutura  = p_nivel_estrutura
      and   grupo_estrutura  = p_grupo_estrutura
      and   subgru_estrutura = p_subgrupo_estrutura
      and   item_estrutura   = p_item_estrutura;

      -- encontra se a data de movimentacao e maior que a ultima movimentacao
      if p_entrada_saida = 'E'
      then

         if v_data_ult_entrada is null or v_data_ult_entrada < p_data_movimento
         then
            v_data_ult_entrada := p_data_movimento;
         end if;

         -- se for transacao de entrada e a transacao atualiza o preco medio e se o produto for comprado
         select comprado_fabric
         into   v_comprado_fabric
         from basi_030
         where nivel_estrutura = p_nivel_estrutura
         and   referencia      = p_grupo_estrutura;

         if v_comprado_fabric = 1  -- produto comprado
         then

            if v_calcula_preco = 1
            then
               select sum(qtde_estoque_atu)
               into   v_estoque_total
               from estq_040
               where cditem_nivel99  = p_nivel_estrutura
               and   cditem_grupo    = p_grupo_estrutura
               and   cditem_subgrupo = p_subgrupo_estrutura
               and   cditem_item     = p_item_estrutura;

               v_estoque_anterior := v_estoque_total - p_quantidade;

               if v_estoque_total > 0.00
               then
                  v_preco_medio      := ((v_estoque_anterior * v_preco_medio) +
                                          v_valor_total) / v_estoque_total;
               end if; -- if v_calcula_preco = 1 and v_estoque_total > 0.00
            end if;    -- if v_calcula_preco = 1
         end if;       -- if v_comprado_fabric = 1
      else             -- if p_entrada_saida = 'E'

         if v_data_ult_saida is null or v_data_ult_saida < p_data_movimento
         then
            v_data_ult_saida := p_data_movimento;
         end if;

      end if;          -- if p_entrada_saida = 'E'

      begin
         update basi_010
         set data_ult_entrada = v_data_ult_entrada,
             data_ult_saida   = v_data_ult_saida,
             preco_medio      = v_preco_medio
         where nivel_estrutura  = p_nivel_estrutura
         and   grupo_estrutura  = p_grupo_estrutura
         and   subgru_estrutura = p_subgrupo_estrutura
         and   item_estrutura   = p_item_estrutura;

         exception
         when OTHERS
         then begin
            if v_preco_medio > 99999999999.99
            then v_mensagem_erro := chr(10) ||
               'O preço médio do seguinte produto é maior que o aceitável pela tabela. Verifique se o valor está correto!' ||
               chr(10) ||
               'PRODUTO: ' ||
               p_nivel_estrutura || '.' ||
               p_grupo_estrutura || '.' ||
               p_subgrupo_estrutura  || '.' ||
               p_item_estrutura ||
               chr(10) ||
               'PREÇO: ' ||
               v_preco_medio ||
               chr(10) ||
               SQLERRM;
            else v_mensagem_erro := SQLERRM;
            end if;

            raise_application_error (-20000, 'ATENÇÃO! Não atualizou as informações básicas do produto.' || v_mensagem_erro);
         end;
      end;

      inter_pr_compra_direta_estoque(p_nivel_estrutura,      p_grupo_estrutura,
                                     p_subgrupo_estrutura,   p_item_estrutura,
                                     p_codigo_deposito);
   end if;

   exception
      when NO_DATA_FOUND
      then
         raise_application_error (-20000,inter_fn_buscar_tag('ds26570#ATENCAO! Produto nao encontrado na tabela de itens',ws_locale_usuario,ws_usuario_systextil));

end inter_pr_estoque;

 

/

exec inter_pr_recompile;

