ALTER TABLE rcnb_030
  MODIFY valor_base number(15,6);

ALTER TABLE basi_355
  MODIFY preco_estimado number(12,2);

ALTER TABLE basi_355
  MODIFY preco number(12,2);

ALTER TABLE basi_350
  MODIFY VALOR_MP_COMPRADA number(15,6);

ALTER TABLE basi_350
  MODIFY val_mp_comp_malh_estimado number(15,6);
  
ALTER TABLE rcnb_851
  MODIFY custo_mp number(19,6);
  
ALTER TABLE rcnb_851
  MODIFY custo_total number(19,6);
  
ALTER TABLE rcnb_852
  MODIFY custo_minuto number(19,6);

ALTER TABLE rcnb_852
  MODIFY valor_custo number(19,6);
  
ALTER TABLE rcnb_853
  MODIFY valor_consumo number(19,6);
  
ALTER TABLE rcnb_853
  MODIFY valor_total number(19,6);
  
ALTER TABLE rcnb_850
  MODIFY preco_final number(15,2);
  
ALTER TABLE rcnb_850
  MODIFY custo_fabricacao number(18,5);
  
ALTER TABLE rcnb_850
  MODIFY valor_cd number(18,5);
  
ALTER TABLE rcnb_850
  MODIFY valor_cp number(18,5);
  
ALTER TABLE rcnb_850
  MODIFY valor_mo number(18,5);
  
ALTER TABLE rcnb_850
  MODIFY valor_mp number(18,5);

ALTER TABLE basi_350
  MODIFY VALOR_MP_COMPRADA number(15,6);
  
exec inter_pr_recompile;
