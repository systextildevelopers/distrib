
CREATE OR REPLACE TRIGGER "INTER_TR_PLAN_MOV_TMRP_645" 
   after insert or delete or update
       of nivel_produto,   grupo_produto,
          subgru_produto,  item_produto,
          qtde_reserva,    minutos_reserva,
          qtde_adicional
   on tmrp_645
   for each row

declare
   v_executa_trigger   number(1);
   v_nivel             varchar2(1);
   v_grupo             varchar2(5);
   v_subgrupo          varchar2(3);
   v_item              varchar2(6);
   v_ordem_planej      number(9);
   v_numero_reserva    number(6);

   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);

begin
   v_executa_trigger := 1;

   if inserting
   then
      v_numero_reserva := :new.numero_reserva;
   else
      v_numero_reserva := :old.numero_reserva;
   end if;

   begin
      select tmrp_610.ordem_planejamento
      into   v_ordem_planej
      from tmrp_625, tmrp_610
      where   tmrp_625.pedido_reserva           = v_numero_reserva
        and   tmrp_625.ordem_planejamento       = tmrp_610.ordem_planejamento
        and   tmrp_610.situacao_ordem           < 9
        and   tmrp_625.nivel_produto_origem     = '0'
        and   tmrp_625.grupo_produto_origem     = '00000'
        and   tmrp_625.subgrupo_produto_origem  = '000'
        and   tmrp_625.item_produto_origem      = '000000'
        and ((tmrp_625.qtde_areceber_programada > 0 and exists (select 1 from tmrp_041, pcpc_020, tmrp_630
                                                                where tmrp_041.periodo_producao   = pcpc_020.periodo_producao
                                                                  and tmrp_041.area_producao      = 1
                                                                  and tmrp_041.nr_pedido_ordem    = tmrp_630.ordem_prod_compra
                                                                  and tmrp_041.codigo_estagio     = pcpc_020.ultimo_estagio
                                                                  and tmrp_041.nivel_estrutura    = tmrp_625.nivel_produto
                                                                  and tmrp_041.grupo_estrutura    = tmrp_625.grupo_produto
                                                                  and tmrp_041.subgru_estrutura   = tmrp_625.subgrupo_produto
                                                                  and tmrp_041.item_estrutura     = tmrp_625.item_produto
                                                                  and tmrp_041.qtde_areceber      > 0
                                                                  and pcpc_020.ordem_producao     = tmrp_630.ordem_prod_compra
                                                                  and pcpc_020.cod_cancelamento   = 0
                                                                  and tmrp_630.ordem_planejamento = tmrp_610.ordem_planejamento
                                                                  and tmrp_630.area_producao      = 1)) or
              tmrp_625.qtde_areceber_programada = 0)
        and rownum = 1;
      exception when no_data_found then
            v_executa_trigger := 0;
   end;

   if v_executa_trigger = 1
   then
      v_nivel    := :old.nivel_produto;
      v_grupo    := :old.grupo_produto;
      v_subgrupo := :old.subgru_produto;
      v_item     := :old.item_produto;

      -- DADOS DO LOGIN DO USUARIO

      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

      -- Alteracao do item do pedido, quantidade reservada do item e codigo de cancelamento.
      if updating
      then
         -- Alteracao do produto
         if :old.nivel_produto   <> :new.nivel_produto
         or :old.grupo_produto   <> :new.grupo_produto
         or :old.subgru_produto  <> :new.subgru_produto
         or :old.item_produto    <> :new.item_produto
         then
            inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_numero_reserva,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE RESERVA ALTERADO
                  inter_fn_buscar_tag_composta('lb38809','','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  -- Item {0} do PEDIDO DE RESERVA {1} alterado para o item {2}.
                  inter_fn_buscar_tag_composta('lb38915', :old.nivel_produto    || '.' ||
                                                          :old.grupo_produto    || '.' ||
                                                          :old.subgru_produto   || '.' ||
                                                          :old.item_produto,
                                                          to_char(:old.numero_reserva,'000000'),
                                                          :new.nivel_produto  || '.' ||
                                                          :new.grupo_produto  || '.' ||
                                                          :new.subgru_produto || '.' ||
                                                          :new.item_produto,
                                                          '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
         end if;

         -- Alteracao da quantidade pedida
         if :old.qtde_reserva <> :new.qtde_reserva
         then
            inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_numero_reserva,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE RESERVA ALTERADO
                  inter_fn_buscar_tag_composta('lb38809', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                    -- Quantidade pedida do item {0} alterada de {1} para {2}.
                    inter_fn_buscar_tag_composta('lb33049',:old.nivel_produto   || '.' ||
                                                            :old.grupo_produto  || '.' ||
                                                            :old.subgru_produto || '.' ||
                                                            :old.item_produto,
                                                            to_char(:old.qtde_reserva),
                                                            to_char(:new.qtde_reserva),
                                                            '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
         end if;
         -- Alteracao dos minutos da reserva
         if :old.minutos_reserva <> :new.minutos_reserva
         then
            inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_numero_reserva,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE RESERVA ALTERADO
                  inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                    -- Quantidade pedida do item {0} alterada de {1} para {2}.
                    inter_fn_buscar_tag_composta('lb33049',:old.nivel_produto   || '.' ||
                                                            :old.grupo_produto  || '.' ||
                                                            :old.subgru_produto || '.' ||
                                                            :old.item_produto,
                                                            to_char(:old.minutos_reserva),
                                                            to_char(:new.minutos_reserva),
                                                            '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
         end if;
         -- Alteracao da quantidade adicional
         if :old.qtde_adicional <> :new.qtde_adicional
         then
            inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_numero_reserva,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE RESERVA ALTERADO
                  inter_fn_buscar_tag_composta('lb38809', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                    -- Quantidade pedida do item {0} alterada de {1} para {2}.
                    inter_fn_buscar_tag_composta('lb33049',:old.nivel_produto   || '.' ||
                                                            :old.grupo_produto  || '.' ||
                                                            :old.subgru_produto || '.' ||
                                                            :old.item_produto,
                                                            to_char(:old.qtde_adicional),
                                                            to_char(:new.qtde_adicional),
                                                            '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
         end if;
         /*
         if :old.cod_cancelamento <> :new.cod_cancelamento
         then
            inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_numero_reserva,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE RESERVA ALTERADO
                  inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                    -- Item {0}, na sequencia {1}, cancelado ou eliminado.
                    inter_fn_buscar_tag_composta('lb38810',:old.nivel_produto  || '.' ||
                                                           :old.grupo_produto    || '.' ||
                                                           :old.subgru_produto || '.' ||
                                                           :old.item_produto,
                                                           to_char(:old.numero_reserva),
                                                           to_char(:old.seq_item_reserva),
                                                           '','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
         end if;*/

      end if;

      -- Inclusao de um item do pedido.
      if inserting
      then
         v_nivel    := :new.nivel_produto;
         v_grupo    := :new.grupo_produto;
         v_subgrupo := :new.subgru_produto;
         v_item     := :new.item_produto;

         inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_numero_reserva,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- ITEM PEDIDO DE RESERVA CRIADO
                  inter_fn_buscar_tag_composta('lb38865', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                 -- Novo item: {0}.
                 inter_fn_buscar_tag_composta('lb33051',:new.nivel_produto  || '.' ||
                                                        :new.grupo_produto    || '.' ||
                                                        :new.subgru_produto || '.' ||
                                                        :new.item_produto,
                                                        '','','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
      end if;

      -- Eliminacao de um item do pedido.
      if  deleting
      then
         inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_numero_reserva,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE RESERVA ALTERADO
                  inter_fn_buscar_tag_composta('lb38809', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                 -- Item {0}, na sequencia {1}, cancelado ou eliminado.
                 inter_fn_buscar_tag_composta('lb38810',:old.nivel_produto  || '.' ||
                                                        :old.grupo_produto    || '.' ||
                                                        :old.subgru_produto || '.' ||
                                                        :old.item_produto,
                                                        to_char(:old.numero_reserva),
                                                        to_char(:old.seq_item_reserva),
                                                        '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
      end if;
   end if;
end inter_tr_plan_mov_tmrp_645;
-- ALTER TRIGGER "INTER_TR_PLAN_MOV_TMRP_645" ENABLE
 

/

exec inter_pr_recompile;

