declare tmpInt number;
cursor parametro_c is select codigo_empresa from fatu_500;
begin
  insert into empr_007 (PARAM, TIPO) values ('pcpc.confeccao.validarordem', 0);
  
  for reg_parametro in parametro_c
  loop
    select nvl(count(*),0) into tmpInt
    from fatu_500
    where fatu_500.codigo_empresa = reg_parametro.codigo_empresa;
    if(tmpInt > 0)
    then
      begin
        insert into empr_008 (
		   codigo_empresa, param, VAL_INT 
        ) values (
		   reg_parametro.codigo_empresa, 'pcpc.confeccao.validarordem', 0
		);
      end;
    end if;
  end loop;
  
  commit;
end;
/
