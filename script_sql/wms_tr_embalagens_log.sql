create or replace trigger wms_tr_embalagens_log
  before insert or delete on inte_wms_embalagens
  for each row
declare
  -- local variables here
 
  v_cod_embalagem_st    inte_wms_embalagens.cod_embalagem_st    %type;
  v_cod_integracao_wms  inte_wms_embalagens.cod_integracao_wms  %type;
  v_desc_embalagem_st   inte_wms_embalagens.desc_embalagem_st   %type;
  v_metros_cubicos_emb  inte_wms_embalagens.metros_cubicos_emb  %type;
  v_peso_maximo         inte_wms_embalagens.peso_maximo         %type;
  v_timestamp_aimportar inte_wms_embalagens.timestamp_aimportar %type;
  v_peso_embalagem      inte_wms_embalagens.peso_embalagem      %type;
  v_perc_fator_comp     inte_wms_embalagens.perc_fator_comp     %type;

  v_sid                 number(9);
  v_empresa             number(3);
  v_usuario_systextil   varchar2(250);
  v_locale_usuario      varchar2(5);
  v_nome_programa       varchar2(20);

  v_operacao            varchar(1);
  v_data_operacao       date;
  v_usuario_rede        varchar(20);
  v_maquina_rede        varchar(40);
  v_aplicativo          varchar(20);

begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();

   --alimenta as variaveis new caso seja insert
   if inserting
   then
      v_operacao := 'i';

      v_cod_embalagem_st    := :new.cod_embalagem_st;
      v_cod_integracao_wms  := :new.cod_integracao_wms;
      v_desc_embalagem_st   := :new.desc_embalagem_st;
      v_metros_cubicos_emb  := :new.metros_cubicos_emb;
      v_peso_maximo         := :new.peso_maximo;
      v_timestamp_aimportar := :new.timestamp_aimportar;
      v_peso_embalagem      := :new.peso_embalagem;
	  v_perc_fator_comp     := :new.perc_fator_comp;
      
   end if; --fim do if inserting or updating

   --alimenta as variaveis old caso seja insert ou update
   if deleting
   then
      v_operacao      := 'd';

      v_cod_embalagem_st    := :old.cod_embalagem_st;
      v_cod_integracao_wms  := :old.cod_integracao_wms;
      v_desc_embalagem_st   := :old.desc_embalagem_st;
      v_metros_cubicos_emb  := :old.metros_cubicos_emb;
      v_peso_maximo         := :old.peso_maximo;
      v_timestamp_aimportar := :old.timestamp_aimportar;
      v_peso_embalagem      := :old.peso_embalagem;
	  v_perc_fator_comp     := :old.perc_fator_comp;

   end if; --fim do if deleting or updating


   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);


    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente

   --insere na inte_wms_embalagens_log o registro.
   insert into inte_wms_embalagens_log (
      cod_embalagem_st,    cod_integracao_wms,
      desc_embalagem_st,   metros_cubicos_emb,
      peso_maximo,         timestamp_aimportar,
      operacao,            data_operacao,
      usuario_rede,        maquina_rede,
      aplicativo,          nome_programa,
      peso_embalagem,      perc_fator_comp      
   )
   values (
      v_cod_embalagem_st,  v_cod_integracao_wms,
      v_desc_embalagem_st, v_metros_cubicos_emb,
      v_peso_maximo,       v_timestamp_aimportar,
      v_operacao,          v_data_operacao,
      v_usuario_rede,      v_maquina_rede,
      v_aplicativo,        v_nome_programa,
      v_peso_embalagem,    v_perc_fator_comp
   );

end wms_tr_embalagens_log;

/

exec inter_pr_recompile;
/* versao: 3 */


 exit;


 exit;
