create or replace trigger INTER_TR_PEDI_080_HIST
   after insert
   or delete
   or update of
	   perc_subs_interna, respeita_ipi_class_fiscal, perc_pis, perc_cofins,
	   natu_nf_ref_cupom, subtrai_icms_custo, nat_oper_entrega, data_cadastro,
	   tipo_reducao, grava_obs_nfs, cod_nat_relacionada, ipi_sobre_substituicao,
	   considera_rateio, cvf_pis, cvf_cofins, consumidor_final,
	   cod_mensagem, razao_social_natureza, flag_razao_social, exige_entrada,
	   analise_credito_pedido, exige_cod_produto, cod_csosn, tem_mov_fisica,
	   cod_cred, ind_nf_servico, ind_nat_frt, cod_cont_crec,
	   ind_nf_ciap, ind_base_icms_resp_usu, considera_suframa, nfe_atu_financeiro,
	   perc_iva_2, faturamento, gi, natur_operacao,
	   estado_natoper, descr_nat_oper, emite_duplicata, perc_icms,
	   mensagem, cod_natureza, operacao_fiscal, classif_contabil,
	   codigo_transacao, cod_trib_icms, mensagem_frete, ipi_sobre_icms,
	   tipo_natureza, perc_iss, perc_substituica, perc_reducao_icm,
	   livros_fiscais, perc_diferenc, hist_contabil, perc_iva_1,
	   consiste_cvf_icms, divisao_natur, perc_icms_isento, perc_icms_diferido,
	   dot, modelo_doc_fisc, nat_ativa, tipo_calc_sub,
	   perc_redu_sub,perc_fcp_uf_dest,perc_icms_uf_dest, red_icms_base_pis_cof
   on pedi_080
   for each row

-- Finalidade: Registrar altera￿￿es feitas na tabela pedi_080
-- Autor.....: Aline Blanski
-- Data......: 07/06/10
--
-- Hist￿ricos de altera￿￿es na trigger
--
-- Data      Autor           SS          Observa￿￿es
-- 12/07/10  Aline Blanski   58329/001   Adicionado campos na verifica￿￿o da trigger.

declare
   ws_tipo_ocorr                         varchar2 (1);
   ws_usuario_rede                       varchar2(20);
   ws_maquina_rede                       varchar2(40);
   ws_aplicativo                         varchar2(20);
   ws_sid                                number(9);
   ws_empresa                            number(3);
   ws_usuario_systextil                  varchar2(250);
   ws_locale_usuario                     varchar2(5);
   ws_nome_programa                      varchar2(20);
   ws_natur_operacao_old                 NUMBER(3)    default 0 ;
   ws_natur_operacao_new                 NUMBER(3)    default 0 ;
   ws_estado_natoper_old                 VARCHAR2(2)  default '';
   ws_estado_natoper_new                 VARCHAR2(2)  default '';
   ws_cod_natureza_old                   VARCHAR2(4)  default '';
   ws_cod_natureza_new                   VARCHAR2(4)  default '';
   ws_divisao_natur_old                  NUMBER(2)    default 0 ;
   ws_divisao_natur_new                  NUMBER(2)    default 0 ;
   ws_descr_nat_oper_old                 VARCHAR2(40) default '';
   ws_descr_nat_oper_new                 VARCHAR2(40) default '';
   ws_tipo_natureza_old                  NUMBER(1)   default 0 ;
   ws_tipo_natureza_new                  NUMBER(1)   default 0 ;
   ws_emite_duplicata_old                NUMBER(1)   default 1 ;
   ws_emite_duplicata_new                NUMBER(1)   default 1 ;
   ws_cod_nat_relacionada_old            NUMBER(3)   default 0 ;
   ws_cod_nat_relacionada_new            NUMBER(3)   default 0 ;
   ws_livros_fiscais_old                 NUMBER(1)   default 1 ;
   ws_livros_fiscais_new                 NUMBER(1)   default 1 ;
   ws_natu_nf_ref_cupom_old              NUMBER(3)   default 0 ;
   ws_natu_nf_ref_cupom_new              NUMBER(3)   default 0 ;
   ws_codigo_transacao_old               NUMBER(3)   default 0 ;
   ws_codigo_transacao_new               NUMBER(3)   default 0 ;
   ws_nat_ativa_old                      NUMBER(1)   default 0 ;
   ws_nat_ativa_new                      NUMBER(1)   default 0 ;
   ws_operacao_fiscal_old                NUMBER(3)   default 0 ;
   ws_operacao_fiscal_new                NUMBER(3)   default 0 ;
   ws_respeita_ipi_class_fiscal_o        NUMBER(1)   default 1 ;
   ws_respeita_ipi_class_fiscal_n        NUMBER(1)   default 1 ;
   ws_classif_contabil_old               NUMBER(6)   default 0 ;
   ws_classif_contabil_new               NUMBER(6)   default 0 ;
   ws_dot_old                            VARCHAR2(1) default '';
   ws_dot_new                            VARCHAR2(1) default '';
   ws_gi_old                             VARCHAR2(1) default '';
   ws_gi_new                             VARCHAR2(1) default '';
   ws_consiste_cvf_icms_old              NUMBER(1)   default 1 ;
   ws_consiste_cvf_icms_new              NUMBER(1)   default 1 ;
   ws_cod_trib_icms_old                  NUMBER(2)   default 0 ;
   ws_cod_trib_icms_new                  NUMBER(2)   default 0 ;
   ws_perc_icms_old                      NUMBER(6,2) default 0.0;
   ws_perc_icms_new                      NUMBER(6,2) default 0.0;
   ws_perc_icms_isento_old               NUMBER(6,2) default 0.00;
   ws_perc_icms_isento_new               NUMBER(6,2) default 0.00;
   ws_perc_iss_old                       NUMBER(6,2) default 0.0 ;
   ws_perc_iss_new                       NUMBER(6,2) default 0.0 ;
   ws_perc_icms_diferido_old             NUMBER(6,2) default 0.00;
   ws_perc_icms_diferido_new             NUMBER(6,2) default 0.00;
   ws_tipo_calc_sub_old                  NUMBER(1)   default 0 ;
   ws_tipo_calc_sub_new                  NUMBER(1)   default 0 ;
   ws_perc_subs_interna_old              NUMBER(5,2) default 0.00;
   ws_perc_subs_interna_new              NUMBER(5,2) default 0.00;
   ws_perc_substituica_old               NUMBER(5,2) default 0.0 ;
   ws_perc_substituica_new               NUMBER(5,2) default 0.0 ;
   ws_perc_redu_sub_old                  NUMBER(5,2) default 0.00;
   ws_perc_redu_sub_new                  NUMBER(5,2) default 0.00;
   ws_perc_reducao_icm_old               NUMBER(8,4) default 0;
   ws_perc_reducao_icm_new               NUMBER(8,4) default 0;
   ws_tipo_reducao_old                   NUMBER(1)   default 1;
   ws_tipo_reducao_new                   NUMBER(1)   default 1;
   ws_perc_iva_1_old                     NUMBER(6,2) default 0.0;
   ws_perc_iva_1_new                     NUMBER(6,2) default 0.0;
   ws_perc_diferenc_old                  NUMBER(9,6) default 0.0;
   ws_perc_diferenc_new                  NUMBER(9,6) default 0.0;
   ws_perc_iva_2_old                     NUMBER(6,2) default 0.0;
   ws_perc_iva_2_new                     NUMBER(6,2) default 0.0;
   ws_perc_pis_old                       NUMBER(6,2) default 0.0;
   ws_perc_pis_new                       NUMBER(6,2) default 0.0;
   ws_perc_cofins_old                    NUMBER(6,2) default 0.0;
   ws_perc_cofins_new                    NUMBER(6,2) default 0.0;
   ws_cvf_pis_old                        NUMBER(2)   default 0 ;
   ws_cvf_pis_new                        NUMBER(2)   default 0 ;
   ws_cvf_cofins_old                     NUMBER(2)   default 0 ;
   ws_cvf_cofins_new                     NUMBER(2)   default 0 ;
   ws_ipi_sobre_icms_old                 NUMBER(1)   default 0 ;
   ws_ipi_sobre_icms_new                 NUMBER(1)   default 0 ;
   ws_hist_contabil_old                  NUMBER(4)   default 0 ;
   ws_hist_contabil_new                  NUMBER(4)   default 0 ;
   ws_ipi_sobre_substituicao_old         NUMBER(1)   default 0 ;
   ws_ipi_sobre_substituicao_new         NUMBER(1)   default 0 ;
   ws_grava_obs_nfs_old                  VARCHAR2(1) default 'N';
   ws_grava_obs_nfs_new                  VARCHAR2(1) default 'N';
   ws_cod_mensagem_old                   NUMBER(6)   default 0;
   ws_cod_mensagem_new                   NUMBER(6)   default 0;
   ws_faturamento_old                    NUMBER(1)   default 0;
   ws_faturamento_new                    NUMBER(1)   default 0;
   ws_subtrai_icms_custo_old             NUMBER(1)   default 1;
   ws_subtrai_icms_custo_new             NUMBER(1)   default 1;
   ws_modelo_doc_fisc_old                NUMBER(2)   default 0;
   ws_modelo_doc_fisc_new                NUMBER(2)   default 0;
   ws_nat_oper_entrega_old               NUMBER(3)   default 0;
   ws_nat_oper_entrega_new               NUMBER(3)   default 0;
   ws_considera_rateio_old               NUMBER(1)   default 1;
   ws_considera_rateio_new               NUMBER(1)   default 1;
   ws_exige_entrada_old                  NUMBER(1)   default 2;
   ws_exige_entrada_new                  NUMBER(1)   default 2;
   ws_consumidor_final_old               VARCHAR2(1) default 'N';
   ws_consumidor_final_new               VARCHAR2(1) default 'N';
   ws_analise_credito_pedido_old         NUMBER(1)   default 0 ;
   ws_analise_credito_pedido_new         NUMBER(1)   default 0 ;
   ws_exige_cod_produto_old              VARCHAR2(1) default 'N';
   ws_exige_cod_produto_new              VARCHAR2(1) default 'N';
   ws_razao_social_natureza_old          VARCHAR2(40) default '';
   ws_razao_social_natureza_new          VARCHAR2(40) default '';
   
   ws_perc_fcp_uf_dest_old                number(5,2) default 0 ;
   ws_perc_fcp_uf_dest_new                number(5,2) default 0 ;
   ws_perc_icms_uf_dest_old               number(5,2) default 0 ;
   ws_perc_icms_uf_dest_new               number(5,2) default 0 ;
   ws_red_icms_base_pis_cof_old           number(1)   default 0 ;
   ws_red_icms_base_pis_cof_new           number(1)   default 0 ;

begin
   if INSERTING then

      ws_tipo_ocorr                      := 'I';
      ws_natur_operacao_old              := 0;
      ws_natur_operacao_new              := :new.natur_operacao;
      ws_estado_natoper_old              := null;
      ws_estado_natoper_new              := :new.estado_natoper;
      ws_cod_natureza_old                := null;
      ws_cod_natureza_new                := :new.cod_natureza;
      ws_divisao_natur_old               := 0;
      ws_divisao_natur_new               := :new.divisao_natur;
      ws_descr_nat_oper_old              := null;
      ws_descr_nat_oper_new              := :new.descr_nat_oper;
      ws_tipo_natureza_old               := 0;
      ws_tipo_natureza_new               := :new.tipo_natureza;
      ws_emite_duplicata_old             := 0;
      ws_emite_duplicata_new             := :new.emite_duplicata;
      ws_cod_nat_relacionada_old         := 0;
      ws_cod_nat_relacionada_new         := :new.cod_nat_relacionada;
      ws_livros_fiscais_old              := 0;
      ws_livros_fiscais_new              := :new.livros_fiscais;
      ws_natu_nf_ref_cupom_old           := 0;
      ws_natu_nf_ref_cupom_new           := :new.natu_nf_ref_cupom;
      ws_codigo_transacao_old            := 0;
      ws_codigo_transacao_new            := :new.codigo_transacao;
      ws_nat_ativa_old                   := 0;
      ws_nat_ativa_new                   := :new.nat_ativa;
      ws_operacao_fiscal_old             := 0;
      ws_operacao_fiscal_new             := :new.operacao_fiscal;
      ws_respeita_ipi_class_fiscal_o     := 0;
      ws_respeita_ipi_class_fiscal_n     := :new.respeita_ipi_class_fiscal ;
      ws_classif_contabil_old            := 0;
      ws_classif_contabil_new            := :new.classif_contabil;
      ws_dot_old                         := null;
      ws_dot_new                         := :new.dot;
      ws_gi_old                          := null;
      ws_gi_new                          := :new.gi;
      ws_consiste_cvf_icms_old           := 0;
      ws_consiste_cvf_icms_new           := :new.consiste_cvf_icms;
      ws_cod_trib_icms_old               := 0;
      ws_cod_trib_icms_new               := :new.cod_trib_icms;
      ws_perc_icms_old                   := 0;
      ws_perc_icms_new                   := :new.perc_icms;
      ws_perc_icms_isento_old            := 0;
      ws_perc_icms_isento_new            := :new.perc_icms_isento;
      ws_perc_iss_old                    := 0;
      ws_perc_iss_new                    := :new.perc_iss;
      ws_perc_icms_diferido_old          := 0;
      ws_perc_icms_diferido_new          := :new.perc_icms_diferido;
      ws_tipo_calc_sub_old               := 0;
      ws_tipo_calc_sub_new               := :new.tipo_calc_sub;
      ws_perc_subs_interna_old           := 0;
      ws_perc_subs_interna_new           := :new.perc_subs_interna;
      ws_perc_substituica_old            := 0;
      ws_perc_substituica_new            := :new.perc_substituica;
      ws_perc_redu_sub_old               := 0;
      ws_perc_redu_sub_new               := :new.perc_redu_sub;
      ws_perc_reducao_icm_old            := 0;
      ws_perc_reducao_icm_new            := :new.perc_reducao_icm;
      ws_tipo_reducao_old                := 0;
      ws_tipo_reducao_new                := :new.tipo_reducao;
      ws_perc_iva_1_old                  := 0;
      ws_perc_iva_1_new                  := :new.perc_iva_1;
      ws_perc_diferenc_old               := 0;
      ws_perc_diferenc_new               := :new.perc_diferenc;
      ws_perc_iva_2_old                  := 0;
      ws_perc_iva_2_new                  := :new.perc_iva_2;
      ws_perc_pis_old                    := 0;
      ws_perc_pis_new                    := :new.perc_pis;
      ws_perc_cofins_old                 := 0;
      ws_perc_cofins_new                 := :new.perc_cofins;
      ws_cvf_pis_old                     := 0;
      ws_cvf_pis_new                     := :new.cvf_pis;
      ws_cvf_cofins_old                  := 0;
      ws_cvf_cofins_new                  := :new.cvf_cofins;
      ws_ipi_sobre_icms_old              := 0;
      ws_ipi_sobre_icms_new              := :new.ipi_sobre_icms;
      ws_hist_contabil_old               := 0;
      ws_hist_contabil_new               := :new.hist_contabil;
      ws_ipi_sobre_substituicao_old      := 0;
      ws_ipi_sobre_substituicao_new      := :new.ipi_sobre_substituicao;
      ws_grava_obs_nfs_old               := null;
      ws_grava_obs_nfs_new               := :new.grava_obs_nfs;
      ws_cod_mensagem_old                := 0;
      ws_cod_mensagem_new                := :new.cod_mensagem ;
      ws_faturamento_old                 := 0;
      ws_faturamento_new                 := :new.faturamento ;
      ws_subtrai_icms_custo_old          := 0;
      ws_subtrai_icms_custo_new          := :new.subtrai_icms_custo;
      ws_modelo_doc_fisc_old             := 0;
      ws_modelo_doc_fisc_new             := :new.modelo_doc_fisc;
      ws_nat_oper_entrega_old            := 0;
      ws_nat_oper_entrega_new            := :new.nat_oper_entrega;
      ws_considera_rateio_old            := 0;
      ws_considera_rateio_new            := :new.considera_rateio;
      ws_exige_entrada_old               := 0;
      ws_exige_entrada_new               := :new.exige_entrada;
      ws_consumidor_final_old            := null;
      ws_consumidor_final_new            := :new.consumidor_final;
      ws_analise_credito_pedido_old      := 0;
      ws_analise_credito_pedido_new      := :new.analise_credito_pedido;
      ws_exige_cod_produto_old           := null;
      ws_exige_cod_produto_new           := :new.exige_cod_produto;
      ws_razao_social_natureza_old       := null;
      ws_razao_social_natureza_new       := :new.razao_social_natureza;
      
      ws_perc_fcp_uf_dest_old            := 0;
      ws_perc_fcp_uf_dest_new            := :new.perc_fcp_uf_dest;
      ws_perc_icms_uf_dest_old           := 0;
      ws_perc_icms_uf_dest_new           := :new.perc_icms_uf_dest;
      ws_red_icms_base_pis_cof_old       := 0;
      ws_red_icms_base_pis_cof_new       := :new.red_icms_base_pis_cof;

   elsif UPDATING then

      ws_tipo_ocorr                      := 'A';
      ws_natur_operacao_old              := :old.natur_operacao;
      ws_natur_operacao_new              := :new.natur_operacao;
      ws_estado_natoper_old              := :old.estado_natoper;
      ws_estado_natoper_new              := :new.estado_natoper;
      ws_cod_natureza_old                := :old.cod_natureza;
      ws_cod_natureza_new                := :new.cod_natureza;
      ws_divisao_natur_old               := :old.divisao_natur;
      ws_divisao_natur_new               := :new.divisao_natur;
      ws_descr_nat_oper_old              := :old.descr_nat_oper;
      ws_descr_nat_oper_new              := :new.descr_nat_oper;
      ws_tipo_natureza_old               := :old.tipo_natureza;
      ws_tipo_natureza_new               := :new.tipo_natureza;
      ws_emite_duplicata_old             := :old.emite_duplicata;
      ws_emite_duplicata_new             := :new.emite_duplicata;
      ws_cod_nat_relacionada_old         := :old.cod_nat_relacionada;
      ws_cod_nat_relacionada_new         := :new.cod_nat_relacionada;
      ws_livros_fiscais_old              := :old.livros_fiscais;
      ws_livros_fiscais_new              := :new.livros_fiscais;
      ws_natu_nf_ref_cupom_old           := :old.natu_nf_ref_cupom;
      ws_natu_nf_ref_cupom_new           := :new.natu_nf_ref_cupom;
      ws_codigo_transacao_old            := :old.codigo_transacao;
      ws_codigo_transacao_new            := :new.codigo_transacao;
      ws_nat_ativa_old                   := :old.nat_ativa;
      ws_nat_ativa_new                   := :new.nat_ativa;
      ws_operacao_fiscal_old             := :old.operacao_fiscal;
      ws_operacao_fiscal_new             := :new.operacao_fiscal;
      ws_respeita_ipi_class_fiscal_o     := :old.respeita_ipi_class_fiscal;
      ws_respeita_ipi_class_fiscal_n     := :new.respeita_ipi_class_fiscal;
      ws_classif_contabil_old            := :old.classif_contabil;
      ws_classif_contabil_new            := :new.classif_contabil;
      ws_dot_old                         := :old.dot;
      ws_dot_new                         := :new.dot;
      ws_gi_old                          := :old.gi;
      ws_gi_new                          := :new.gi;
      ws_consiste_cvf_icms_old           := :old.consiste_cvf_icms;
      ws_consiste_cvf_icms_new           := :new.consiste_cvf_icms;
      ws_cod_trib_icms_old               := :old.cod_trib_icms;
      ws_cod_trib_icms_new               := :new.cod_trib_icms;
      ws_perc_icms_old                   := :old.perc_icms;
      ws_perc_icms_new                   := :new.perc_icms;
      ws_perc_icms_isento_old            := :old.perc_icms_isento;
      ws_perc_icms_isento_new            := :new.perc_icms_isento;
      ws_perc_iss_old                    := :old.perc_iss;
      ws_perc_iss_new                    := :new.perc_iss;
      ws_perc_icms_diferido_old          := :old.perc_icms_diferido;
      ws_perc_icms_diferido_new          := :new.perc_icms_diferido;
      ws_tipo_calc_sub_old               := :old.tipo_calc_sub;
      ws_tipo_calc_sub_new               := :new.tipo_calc_sub;
      ws_perc_subs_interna_old           := :old.perc_subs_interna;
      ws_perc_subs_interna_new           := :new.perc_subs_interna;
      ws_perc_substituica_old            := :old.perc_substituica;
      ws_perc_substituica_new            := :new.perc_substituica;
      ws_perc_redu_sub_old               := :old.perc_redu_sub;
      ws_perc_redu_sub_new               := :new.perc_redu_sub;
      ws_perc_reducao_icm_old            := :old.perc_reducao_icm;
      ws_perc_reducao_icm_new            := :new.perc_reducao_icm;
      ws_tipo_reducao_old                := :old.tipo_reducao;
      ws_tipo_reducao_new                := :new.tipo_reducao;
      ws_perc_iva_1_old                  := :old.perc_iva_1;
      ws_perc_iva_1_new                  := :new.perc_iva_1;
      ws_perc_diferenc_old               := :old.perc_diferenc;
      ws_perc_diferenc_new               := :new.perc_diferenc;
      ws_perc_iva_2_old                  := :old.perc_iva_2;
      ws_perc_iva_2_new                  := :new.perc_iva_2;
      ws_perc_pis_old                    := :old.perc_pis;
      ws_perc_pis_new                    := :new.perc_pis;
      ws_perc_cofins_old                 := :old.perc_cofins;
      ws_perc_cofins_new                 := :new.perc_cofins;
      ws_cvf_pis_old                     := :old.cvf_pis;
      ws_cvf_pis_new                     := :new.cvf_pis;
      ws_cvf_cofins_old                  := :old.cvf_cofins;
      ws_cvf_cofins_new                  := :new.cvf_cofins;
      ws_ipi_sobre_icms_old              := :old.ipi_sobre_icms;
      ws_ipi_sobre_icms_new              := :new.ipi_sobre_icms;
      ws_hist_contabil_old               := :old.hist_contabil;
      ws_hist_contabil_new               := :new.hist_contabil;
      ws_ipi_sobre_substituicao_old      := :old.ipi_sobre_substituicao;
      ws_ipi_sobre_substituicao_new      := :new.ipi_sobre_substituicao;
      ws_grava_obs_nfs_old               := :old.grava_obs_nfs;
      ws_grava_obs_nfs_new               := :new.grava_obs_nfs;
      ws_cod_mensagem_old                := :old.cod_mensagem;
      ws_cod_mensagem_new                := :new.cod_mensagem;
      ws_faturamento_old                 := :old.faturamento;
      ws_faturamento_new                 := :new.faturamento;
      ws_subtrai_icms_custo_old          := :old.subtrai_icms_custo;
      ws_subtrai_icms_custo_new          := :new.subtrai_icms_custo;
      ws_modelo_doc_fisc_old             := :old.modelo_doc_fisc;
      ws_modelo_doc_fisc_new             := :new.modelo_doc_fisc;
      ws_nat_oper_entrega_old            := :old.nat_oper_entrega;
      ws_nat_oper_entrega_new            := :new.nat_oper_entrega;
      ws_considera_rateio_old            := :old.considera_rateio;
      ws_considera_rateio_new            := :new.considera_rateio;
      ws_exige_entrada_old               := :old.exige_entrada;
      ws_exige_entrada_new               := :new.exige_entrada;
      ws_consumidor_final_old            := :old.consumidor_final;
      ws_consumidor_final_new            := :new.consumidor_final;
      ws_analise_credito_pedido_old      := :old.analise_credito_pedido;
      ws_analise_credito_pedido_new      := :new.analise_credito_pedido;
      ws_exige_cod_produto_old           := :old.exige_cod_produto;
      ws_exige_cod_produto_new           := :new.exige_cod_produto;
      ws_razao_social_natureza_old       := :old.razao_social_natureza;
      ws_razao_social_natureza_new       := :new.razao_social_natureza;
      
      ws_perc_fcp_uf_dest_old            := :old.perc_fcp_uf_dest;
      ws_perc_fcp_uf_dest_new            := :new.perc_fcp_uf_dest;
      ws_perc_icms_uf_dest_old           := :old.perc_icms_uf_dest;
      ws_perc_icms_uf_dest_new           := :new.perc_icms_uf_dest;
      ws_red_icms_base_pis_cof_old       := :old.red_icms_base_pis_cof;
      ws_red_icms_base_pis_cof_new       := :new.red_icms_base_pis_cof;

   elsif DELETING then

      ws_tipo_ocorr                      := 'D';
      ws_natur_operacao_old              := :old.natur_operacao;
      ws_natur_operacao_new              := 0;
      ws_estado_natoper_old              := :old.estado_natoper;
      ws_estado_natoper_new              := null;
      ws_cod_natureza_old                := :old.cod_natureza;
      ws_cod_natureza_new                := null;
      ws_divisao_natur_old               := :old.divisao_natur;
      ws_divisao_natur_new               := 0;
      ws_descr_nat_oper_old              := :old.descr_nat_oper;
      ws_descr_nat_oper_new              := null;
      ws_tipo_natureza_old               := :old.tipo_natureza;
      ws_tipo_natureza_new               := 0;
      ws_emite_duplicata_old             := :old.emite_duplicata;
      ws_emite_duplicata_new             := 0;
      ws_cod_nat_relacionada_old         := :old.cod_nat_relacionada;
      ws_cod_nat_relacionada_new         := 0;
      ws_livros_fiscais_old              := :old.livros_fiscais;
      ws_livros_fiscais_new              := 0;
      ws_natu_nf_ref_cupom_old           := :old.natu_nf_ref_cupom;
      ws_natu_nf_ref_cupom_new           := 0;
      ws_codigo_transacao_old            := :old.codigo_transacao;
      ws_codigo_transacao_new            := 0;
      ws_nat_ativa_old                   := :old.nat_ativa;
      ws_nat_ativa_new                   := 0;
      ws_operacao_fiscal_old             := :old.operacao_fiscal;
      ws_operacao_fiscal_new             := 0;
      ws_respeita_ipi_class_fiscal_o     := :old.respeita_ipi_class_fiscal;
      ws_respeita_ipi_class_fiscal_n     := 0;
      ws_classif_contabil_old            := :old.classif_contabil;
      ws_classif_contabil_new            := 0;
      ws_dot_old                         := :old.dot;
      ws_dot_new                         := null;
      ws_gi_old                          := :old.gi;
      ws_gi_new                          := null;
      ws_consiste_cvf_icms_old           := :old.consiste_cvf_icms;
      ws_consiste_cvf_icms_new           := 0;
      ws_cod_trib_icms_old               := :old.cod_trib_icms;
      ws_cod_trib_icms_new               := 0;
      ws_perc_icms_old                   := :old.perc_icms;
      ws_perc_icms_new                   := 0;
      ws_perc_icms_isento_old            := :old.perc_icms_isento;
      ws_perc_icms_isento_new            := 0;
      ws_perc_iss_old                    := :old.perc_iss;
      ws_perc_iss_new                    := 0;
      ws_perc_icms_diferido_old          := :old.perc_icms_diferido;
      ws_perc_icms_diferido_new          := 0;
      ws_tipo_calc_sub_old               := :old.tipo_calc_sub;
      ws_tipo_calc_sub_new               := 0;
      ws_perc_subs_interna_old           := :old.perc_subs_interna;
      ws_perc_subs_interna_new           := 0;
      ws_perc_substituica_old            := :old.perc_substituica;
      ws_perc_substituica_new            := 0;
      ws_perc_redu_sub_old               := :old.perc_redu_sub;
      ws_perc_redu_sub_new               := 0;
      ws_perc_reducao_icm_old            := :old.perc_reducao_icm;
      ws_perc_reducao_icm_new            := 0;
      ws_tipo_reducao_old                := :old.tipo_reducao;
      ws_tipo_reducao_new                := 0;
      ws_perc_iva_1_old                  := :old.perc_iva_1;
      ws_perc_iva_1_new                  := 0;
      ws_perc_diferenc_old               := :old.perc_diferenc;
      ws_perc_diferenc_new               := 0;
      ws_perc_iva_2_old                  := :old.perc_iva_2;
      ws_perc_iva_2_new                  := 0;
      ws_perc_pis_old                    := :old.perc_pis;
      ws_perc_pis_new                    := 0;
      ws_perc_cofins_old                 := :old.perc_cofins;
      ws_perc_cofins_new                 := 0;
      ws_cvf_pis_old                     := :old.cvf_pis;
      ws_cvf_pis_new                     := 0;
      ws_cvf_cofins_old                  := :old.cvf_cofins;
      ws_cvf_cofins_new                  := 0;
      ws_ipi_sobre_icms_old              := :old.ipi_sobre_icms;
      ws_ipi_sobre_icms_new              := 0;
      ws_hist_contabil_old               := :old.hist_contabil;
      ws_hist_contabil_new               := 0;
      ws_ipi_sobre_substituicao_old      := :old.ipi_sobre_substituicao;
      ws_ipi_sobre_substituicao_new      := 0;
      ws_grava_obs_nfs_old               := :old.grava_obs_nfs;
      ws_grava_obs_nfs_new               := null;
      ws_cod_mensagem_old                := :old.cod_mensagem;
      ws_cod_mensagem_new                := 0;
      ws_faturamento_old                 := :old.faturamento;
      ws_faturamento_new                 := 0;
      ws_subtrai_icms_custo_old          := :old.subtrai_icms_custo;
      ws_subtrai_icms_custo_new          := 0;
      ws_modelo_doc_fisc_old             := :old.modelo_doc_fisc;
      ws_modelo_doc_fisc_new             := 0;
      ws_nat_oper_entrega_old            := :old.nat_oper_entrega;
      ws_nat_oper_entrega_new            := 0;
      ws_considera_rateio_old            := :old.considera_rateio;
      ws_considera_rateio_new            := 0;
      ws_exige_entrada_old               := :old.exige_entrada;
      ws_exige_entrada_new               := 0;
      ws_consumidor_final_old            := :old.consumidor_final;
      ws_consumidor_final_new            := null;
      ws_analise_credito_pedido_old      := :old.analise_credito_pedido;
      ws_analise_credito_pedido_new      := 0;
      ws_exige_cod_produto_old           := :old.exige_cod_produto;
      ws_exige_cod_produto_new           := null;
      ws_razao_social_natureza_old       := :old.razao_social_natureza;
      ws_razao_social_natureza_new       := null;
      
      ws_perc_fcp_uf_dest_old            := :old.perc_fcp_uf_dest;
      ws_perc_fcp_uf_dest_new            := 0.00;
      ws_perc_icms_uf_dest_old           := :old.perc_icms_uf_dest;
      ws_perc_icms_uf_dest_new           := 0.00;
      ws_red_icms_base_pis_cof_old       := :old.red_icms_base_pis_cof;
      ws_red_icms_base_pis_cof_new       := 0;

   end if;

   -- Dados do usu￿rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ws_nome_programa := inter_fn_nome_programa(ws_sid);  
  

   INSERT INTO pedi_080_hist
      ( aplicacao,                         tipo_ocorr,
        data_ocorr,
        usuario_rede,                      maquina_rede,
        nome_programa,                     usuario_systextil,
        natur_operacao_old,                natur_operacao_new,
        estado_natoper_old,                estado_natoper_new,
        cod_natureza_old,                  cod_natureza_new,
        divisao_natur_old,                 divisao_natur_new,
        descr_nat_oper_old,                descr_nat_oper_new,
        tipo_natureza_old,                 tipo_natureza_new,
        emite_duplicata_old,               emite_duplicata_new,
        cod_nat_relacionada_old,           cod_nat_relacionada_new,
        livros_fiscais_old,                livros_fiscais_new,
        natu_nf_ref_cupom_old,             natu_nf_ref_cupom_new,
        codigo_transacao_old,              codigo_transacao_new,
        nat_ativa_old,                     nat_ativa_new,
        operacao_fiscal_old,               operacao_fiscal_new,
        respeita_ipi_class_fiscal_old,     respeita_ipi_class_fiscal_new,
        classif_contabil_old,              classif_contabil_new,
        dot_old,                           dot_new,
        gi_old,                            gi_new,
        consiste_cvf_icms_old,             consiste_cvf_icms_new,
        cod_trib_icms_old,                 cod_trib_icms_new,
        perc_icms_old,                     perc_icms_new,
        perc_icms_isento_old,              perc_icms_isento_new,
        perc_iss_old,                      perc_iss_new,
        perc_icms_diferido_old,            perc_icms_diferido_new,
        tipo_calc_sub_old,                 tipo_calc_sub_new,
        perc_subs_interna_old,             perc_subs_interna_new,
        perc_substituica_old,              perc_substituica_new,
        perc_redu_sub_old,                 perc_redu_sub_new,
        perc_reducao_icm_old,              perc_reducao_icm_new,
        tipo_reducao_old,                  tipo_reducao_new,
        perc_iva_1_old,                    perc_iva_1_new,
        perc_diferenc_old,                 perc_diferenc_new,
        perc_iva_2_old,                    perc_iva_2_new,
        perc_pis_old,                      perc_pis_new,
        perc_cofins_old,                   perc_cofins_new,
        cvf_pis_old,                       cvf_pis_new,
        cvf_cofins_old,                    cvf_cofins_new,
        ipi_sobre_icms_old,                ipi_sobre_icms_new,
        hist_contabil_old,                 hist_contabil_new,
        ipi_sobre_substituicao_old,        ipi_sobre_substituicao_new,
        grava_obs_nfs_old,                 grava_obs_nfs_new,
        cod_mensagem_old,                  cod_mensagem_new,
        faturamento_old,                   faturamento_new,
        subtrai_icms_custo_old,            subtrai_icms_custo_new,
        modelo_doc_fisc_old,               modelo_doc_fisc_new,
        nat_oper_entrega_old,              nat_oper_entrega_new,
        considera_rateio_old,              considera_rateio_new,
        exige_entrada_old,                 exige_entrada_new,
        consumidor_final_old,              consumidor_final_new,
        analise_credito_pedido_old,        analise_credito_pedido_new,
        exige_cod_produto_old,             exige_cod_produto_new,
        razao_social_natureza_old,         razao_social_natureza_new,
        
        perc_fcp_uf_dest_old ,
        perc_fcp_uf_dest_new ,
        perc_icms_uf_dest_old,
        perc_icms_uf_dest_new,
        red_icms_base_pis_cof_old,
        red_icms_base_pis_cof_new
      )
   VALUES
      ( ws_aplicativo,                     ws_tipo_ocorr,
        sysdate,
        ws_usuario_rede,                   ws_maquina_rede,
        ws_nome_programa,                  ws_usuario_systextil,
        ws_natur_operacao_old,             ws_natur_operacao_new,
        ws_estado_natoper_old,             ws_estado_natoper_new,
        ws_cod_natureza_old,               ws_cod_natureza_new,
        ws_divisao_natur_old,              ws_divisao_natur_new,
        ws_descr_nat_oper_old,             ws_descr_nat_oper_new,
        ws_tipo_natureza_old,              ws_tipo_natureza_new,
        ws_emite_duplicata_old,            ws_emite_duplicata_new,
        ws_cod_nat_relacionada_old,        ws_cod_nat_relacionada_new,
        ws_livros_fiscais_old,             ws_livros_fiscais_new,
        ws_natu_nf_ref_cupom_old,          ws_natu_nf_ref_cupom_new,
        ws_codigo_transacao_old,           ws_codigo_transacao_new,
        ws_nat_ativa_old,                  ws_nat_ativa_new,
        ws_operacao_fiscal_old,            ws_operacao_fiscal_new,
        ws_respeita_ipi_class_fiscal_o,    ws_respeita_ipi_class_fiscal_n,
        ws_classif_contabil_old,           ws_classif_contabil_new,
        ws_dot_old,                        ws_dot_new,
        ws_gi_old,                         ws_gi_new,
        ws_consiste_cvf_icms_old,          ws_consiste_cvf_icms_new,
        ws_cod_trib_icms_old,              ws_cod_trib_icms_new,
        ws_perc_icms_old,                  ws_perc_icms_new,
        ws_perc_icms_isento_old,           ws_perc_icms_isento_new,
        ws_perc_iss_old,                   ws_perc_iss_new,
        ws_perc_icms_diferido_old,         ws_perc_icms_diferido_new,
        ws_tipo_calc_sub_old,              ws_tipo_calc_sub_new,
        ws_perc_subs_interna_old,          ws_perc_subs_interna_new,
        ws_perc_substituica_old,           ws_perc_substituica_new,
        ws_perc_redu_sub_old,              ws_perc_redu_sub_new,
        ws_perc_reducao_icm_old,           ws_perc_reducao_icm_new,
        ws_tipo_reducao_old,               ws_tipo_reducao_new,
        ws_perc_iva_1_old,                 ws_perc_iva_1_new,
        ws_perc_diferenc_old,              ws_perc_diferenc_new,
        ws_perc_iva_2_old,                 ws_perc_iva_2_new,
        ws_perc_pis_old,                   ws_perc_pis_new,
        ws_perc_cofins_old,                ws_perc_cofins_new,
        ws_cvf_pis_old,                    ws_cvf_pis_new,
        ws_cvf_cofins_old,                 ws_cvf_cofins_new,
        ws_ipi_sobre_icms_old,             ws_ipi_sobre_icms_new,
        ws_hist_contabil_old,              ws_hist_contabil_new,
        ws_ipi_sobre_substituicao_old,     ws_ipi_sobre_substituicao_new,
        ws_grava_obs_nfs_old,              ws_grava_obs_nfs_new,
        ws_cod_mensagem_old,               ws_cod_mensagem_new,
        ws_faturamento_old,                ws_faturamento_new,
        ws_subtrai_icms_custo_old,         ws_subtrai_icms_custo_new,
        ws_modelo_doc_fisc_old,            ws_modelo_doc_fisc_new,
        ws_nat_oper_entrega_old,           ws_nat_oper_entrega_new,
        ws_considera_rateio_old,           ws_considera_rateio_new,
        ws_exige_entrada_old,              ws_exige_entrada_new,
        ws_consumidor_final_old,           ws_consumidor_final_new,
        ws_analise_credito_pedido_old,     ws_analise_credito_pedido_new,
        ws_exige_cod_produto_old,          ws_exige_cod_produto_new,
        ws_razao_social_natureza_old,      ws_razao_social_natureza_new,
        
        ws_perc_fcp_uf_dest_old ,
        ws_perc_fcp_uf_dest_new ,
        ws_perc_icms_uf_dest_old,
        ws_perc_icms_uf_dest_new,
        ws_red_icms_base_pis_cof_old,
        ws_red_icms_base_pis_cof_new
      );
end inter_tr_pedi_080_hist;
-- ALTER TRIGGER "INTER_TR_PEDI_080_HIST" ENABLE
/
