CREATE OR REPLACE PROCEDURE "INTER_PR_VALIDA_QUANTIDADE_KG" 
   (p_valor_parametro      in number,
    p_codigo_projeto       in varchar2,
    p_sequencia_projeto    in number,
    p_nivel_item           in varchar2,
    p_grupo_item           in varchar2,
    p_sugru_item           in varchar2,
    p_item_item            in varchar2,
    p_sequencia_estrutura  in number,
    p_alternativa_produto  in number,
    p_consumo_componente   in number,
    p_subgrupo             in varchar2)
        
is
   ws_sid                  number(9);
   ws_empresa              number(3);
   ws_usuario_systextil    varchar2(20);
   ws_locale_usuario       varchar2(5);
   ws_usuario_rede         varchar2(250);
   ws_maquina_rede         varchar2(40);
   ws_aplicativo           varchar2(20);
   
   v_consumo               NUMBER(13,6);
   v_comsumo1              NUMBER(13,6);
   v_projeto_produto       varchar2(31);
   
BEGIN  
  inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                          ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
  v_consumo := 0.0;
      
  for consumo in (
  select basi_013.alternativa_produto,   basi_013.sequencia_estrutura,
         basi_013.subgru_comp,           basi_013.item_comp,
         basi_013.consumo_componente,    basi_013.nivel_item,
         basi_013.grupo_item
  from basi_013, basi_030
  where basi_013.codigo_projeto      = p_codigo_projeto
  and   basi_013.sequencia_projeto   = p_sequencia_projeto
  and   basi_013.nivel_item          = p_nivel_item
  and   basi_013.grupo_item          = p_grupo_item
  and   basi_013.subgru_item         = p_sugru_item
  and   basi_013.item_item           = p_item_item
  and   basi_013.alternativa_produto = p_alternativa_produto
  and   basi_013.utiliza_componente  = 1
  and   basi_013.sequencia_estrutura > 0
  and   basi_013.nivel_comp          = basi_030.nivel_estrutura 
  and   basi_013.grupo_comp          = basi_030.referencia
  and   basi_030.unidade_medida      = 'KG'    
  ) loop
    v_consumo := v_consumo + consumo.consumo_componente;
     
    if consumo.subgru_comp = '000' or consumo.consumo_componente = 0
    then
      begin            
        select basi_021.consumo_componente 
        into v_comsumo1 
        from basi_021
        where basi_021.codigo_projeto       = p_codigo_projeto
        and   basi_021.sequencia_projeto    = p_sequencia_projeto
        and   basi_021.alternativa_produto  = consumo.alternativa_produto
        and   basi_021.nivel_item           = consumo.nivel_item
        and   basi_021.grupo_item           = consumo.grupo_item
        and   basi_021.sequencia_estrutura  = consumo.sequencia_estrutura
        and   basi_021.utiliza_componente   = 1;
        exception
        when OTHERS then
          v_comsumo1 := 0;
      end;
      v_consumo := v_consumo + v_comsumo1;
    end if;
    
  end loop;
  
  v_consumo         := v_consumo + p_consumo_componente; 
  v_projeto_produto := p_codigo_projeto || '/' || p_nivel_item || '.' || p_grupo_item || '.' || p_sugru_item || '.' || p_item_item;   

  if v_consumo >= p_valor_parametro
  then
    /* ATENÇÃO! No projeto {0}, na alternativa {1}, a soma do consumo de todos os itens com unidade de medida em quilos excede a {2}. */   
    raise_application_error (-20000, inter_fn_buscar_tag_composta('ds34840', v_projeto_produto,  to_char(p_alternativa_produto, '99') ,p_valor_parametro, '' , '' , '' , '' , '' , '', '' , ws_locale_usuario,ws_usuario_systextil ));
  end if;                           
end inter_pr_valida_quantidade_kg;
 

/

exec inter_pr_recompile;

