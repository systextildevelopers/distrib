create or replace procedure inter_pr_gera_auxiliar_sped_pc(p_cod_matriz   IN  NUMBER,
                                                           p_dat_inicial   IN  DATE,
                                                           p_dat_final     IN  DATE,
                                                           p_gera_blocop   IN VARCHAR2,
                                                           p_des_erro      OUT varchar2) is
--
-- Finalidade: Executa as demais procedures do SPED PIS COFINS
-- Autor.....: Edson Pio
-- Data......: 21/02/11
--
-- Históricos
--
-- Data    Autor    Observações
--

--DECLARE
   w_des_erro                   VARCHAR2(2000);
   w_erro                       EXCEPTION;
   w_ind_regime_cum_pis_cofins  fatu_503.ind_regime_cum_pis_cofins%type;

CURSOR u_fatu_500 (p_cod_matriz          NUMBER) IS
   select distinct fatu_500.codigo_empresa, fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2
   from fatu_500
   where fatu_500.codigo_matriz = p_cod_matriz;

BEGIN
   w_des_erro := NULL;

   --
   -- Gera Informações da empresa
   --
   inter_pr_gera_sped_0000_pc(p_cod_matriz , w_des_erro);
   IF  w_des_erro IS NOT NULL THEN
       RAISE w_erro;
   END IF;


   --
   -- Gera Informações Bloco P
   --
   inter_pr_gera_sped_p100_pc(p_cod_matriz, p_dat_inicial, p_dat_final, w_des_erro);
   IF  w_des_erro IS NOT NULL THEN
       RAISE w_erro;
   END IF;
   
   COMMIT;   

   begin
      select fatu_503.ind_regime_cum_pis_cofins
      into   w_ind_regime_cum_pis_cofins
      from fatu_503
      where fatu_503.codigo_empresa = p_cod_matriz;
   exception
   when no_data_found then
      w_ind_regime_cum_pis_cofins := 9;
   end;

   if w_ind_regime_cum_pis_cofins = 1
   then
      FOR fatu_500 in u_fatu_500 (p_cod_matriz)
      LOOP
         --
         -- Gera Informações contábeis
         --
         inter_pr_gera_sped_0500_pc(fatu_500.codigo_empresa,p_cod_matriz,fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2, p_dat_inicial, w_des_erro);
         IF  w_des_erro IS NOT NULL THEN
             RAISE w_erro;
         END IF;
        
        
        inter_pr_gera_sped_1900_pc(fatu_500.codigo_empresa, p_cod_matriz,fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2,p_dat_inicial,p_dat_final, w_des_erro);
        IF  w_des_erro IS NOT NULL THEN
            RAISE w_erro;
        END IF;
      END LOOP;
   end if;

   if w_ind_regime_cum_pis_cofins <> 1 /*  1 - Regime de Caixa, 9 - Regime de Competencia*/     
   then
      FOR fatu_500 in u_fatu_500 (p_cod_matriz)
      LOOP
        if p_gera_blocop <> 'S' /* Gerar somente bloco P para regime cumulativo sem obrigações de notas fiscais */
        then  
           --
           -- Gera Informações das notas fiscais e dos itens das notas
           --
           inter_pr_gera_sped_c100_pc(fatu_500.codigo_empresa, p_cod_matriz,fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2,p_dat_inicial,p_dat_final, w_des_erro);
           IF  w_des_erro IS NOT NULL THEN
               RAISE w_erro;
           END IF;
           --
           -- Gera Informações de notas de deveolução de exportação
           --
           inter_pr_gera_sped_ex_pc(p_cod_matriz, p_dat_inicial, p_dat_final, w_des_erro);
           IF  w_des_erro IS NOT NULL THEN
               RAISE w_erro;
           END IF;
           --
           -- Gera Informações dos clientes
           --
           inter_pr_gera_sped_0150_pc(fatu_500.codigo_empresa,p_cod_matriz,fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2,p_dat_inicial, w_des_erro);
           IF  w_des_erro IS NOT NULL THEN
               RAISE w_erro;
           END IF;
           --
           -- Gera Informações dos produtos
           --                      Unidade de medida
           --
           inter_pr_gera_sped_0200_pc(fatu_500.codigo_empresa,p_cod_matriz,fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2, p_dat_inicial, p_dat_final, w_des_erro);
           IF  w_des_erro IS NOT NULL THEN
               RAISE w_erro;
           END IF;
           --
           -- Gera Informações da natureza de operação
           --
           inter_pr_gera_sped_0400_pc(fatu_500.codigo_empresa,p_cod_matriz,fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2,w_des_erro);
           IF  w_des_erro IS NOT NULL THEN
               RAISE w_erro;
           END IF;
           --
           -- Gera Informações de observações
           --
           inter_pr_gera_sped_0450_pc(fatu_500.codigo_empresa,p_cod_matriz,fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2, w_des_erro);
           IF  w_des_erro IS NOT NULL THEN
               RAISE w_erro;
           END IF;
           --
           -- Gera Informações contábeis
           --
           inter_pr_gera_sped_0500_pc(fatu_500.codigo_empresa,p_cod_matriz,fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2, p_dat_inicial, w_des_erro);
           IF  w_des_erro IS NOT NULL THEN
               RAISE w_erro;
           END IF;
           --
           -- Gera Informações dos Centros de Custo
           --
           inter_pr_gera_sped_0600_pc(fatu_500.codigo_empresa,p_cod_matriz,fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2, p_dat_inicial, w_des_erro);
           IF  w_des_erro IS NOT NULL THEN
               RAISE w_erro;
           END IF;
        end if;
           
        --
        -- Integra informações para o arquivo do sped
        --
        inter_pr_gera_integracao_pc(fatu_500.codigo_empresa, p_cod_matriz, p_gera_blocop, w_des_erro);
        IF  w_des_erro IS NOT NULL THEN
            RAISE w_erro;
        END IF;
        --
        -- Gera Informações dos registro 0111
        --
        inter_pr_gera_sped_0111_pc(p_cod_matriz, p_dat_inicial, p_dat_final, w_des_erro);
        IF  w_des_erro IS NOT NULL THEN
            RAISE w_erro;
        END IF;
      END LOOP;
   end if;

   --
   -- Gera Informações dos registro M100/M105
   --
   inter_pr_gera_sped_m100_pc(p_cod_matriz, p_dat_inicial, p_dat_final, w_des_erro);
   IF  w_des_erro IS NOT NULL THEN
       RAISE w_erro;
   END IF;


   COMMIT;
EXCEPTION
   WHEN w_erro THEN
      --Dbms_Output.Put_Line('Erro na procedure p_gera_auxiliar_sped ' || Chr(10) || w_des_erro);
      p_des_erro := 'Erro na procedure p_gera_auxiliar_sped ' || Chr(10) || w_des_erro;
END inter_pr_gera_auxiliar_sped_pc;

/

exec inter_pr_recompile;
