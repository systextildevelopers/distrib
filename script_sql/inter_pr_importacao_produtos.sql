
  CREATE OR REPLACE PROCEDURE "INTER_PR_IMPORTACAO_PRODUTOS" 
is

 v_reg_ok_30  varchar2(1);
 v_reg_ok_20  varchar2(1);
 v_reg_ok_10  varchar2(1);
 v_reg_ok_100 varchar2(1);

begin

   begin

   inter_pr_insere_hist_001(0, 'JOB', 0, 'i_basi_030', 0, 'INSERT', 'inter_pr_importacao_produtos', 0, 0, 'Importacao executada com sucesso!');

   v_reg_ok_30 := 's';

   insert into i_basi_030(
   tipo_atualizacao,	       comp_serra_fita,
   nivel_estrutura,	       parte_composicao_01,
   referencia,		       parte_composicao_02,
   descr_referencia,	       parte_composicao_03,
   unidade_medida,             parte_composicao_04,
   colecao,		       parte_composicao_05,
   conta_estoque,              composicao_01,
   linha_produto,	       composicao_02,
   artigo,		       composicao_03,
   artigo_cotas,	       composicao_04,
   tipo_produto,	       composicao_05,
   classific_fiscal,	       composicao_01_ingles,
   comprado_fabric,	       composicao_02_ingles,
   cor_de_estoque,	       composicao_03_ingles,
   tipo_estampa,	       composicao_04_ingles,
   codigo_contabil,	       composicao_05_ingles,
   data_exportacao,	       imp_inf_empresa_tag,
   data_importacao,	       estampa_impressa,
   flag_importacao,	       cod_tipo_aviamento,
   perc_2_qualidade,	       cod_atributo_01,
   artigo_aproveit,	       cod_atributo_02,
   percent_desconto,	       cod_atributo_03,
   variacao_cores,	       cod_atributo_04,
   tamanho_filme,	       cod_atributo_05,
   raportagem,		       cod_atributo_06,
   percent_perda_1,	       cod_atributo_07,
   percent_perda_2,	       cod_atributo_08,
   percent_perda_3,	       cod_atributo_09,
   aproveit_medio,	       cod_atributo_10,
   metros_lineares,	       referencia_agrupador,
   indice_reajuste,	       maquina_piloto,
   item_baixa_estq,	       finura_piloto,
   demanda,		       graduacao_piloto,
   pontos_por_cm,	       programa_piloto,
   observacao,		       programador_piloto,
   agulhas_falhadas,	       peso_bruto_piloto,
   formacao_agulhas,	       peso_acabamento_piloto,
   aberto_tubular,	       tecelagem_piloto,
   numero_roteiro,	       faccao_piloto,
   ref_original,	       pers_bordado_piloto,
   numero_molde,	       pers_lavacao_piloto,
   div_prod,		       pers_serigrafia_piloto,
   cor_de_producao,	       personalizacao_piloto,
   cgc_cliente_2,	       grupo_tecido_corte,
   cgc_cliente_4,	       data_mov_cilindro,
   cgc_cliente_9,	       serie_tamanho,
   perc_composicao1,	       numero_pontos_laser,
   perc_composicao2,	       percentual_amaciante,
   perc_composicao3,	       simbolo_1,
   perc_composicao4,	       simbolo_2,
   perc_composicao5,	       simbolo_3,
   estagio_altera_programado,  simbolo_4,
   risco_padrao,	       simbolo_5,
   responsavel,		       lote_estoque,
   multiplicador,	       dias_maturacao,
   num_sol_desenv,	       cod_bar_por_cli,
   sortimento_diversos,	       familia_atributo,
   grupo_agrupador,	       doc_referencial,
   sub_agrupador,	       ligamento,
   cod_embalagem,
   conf_altera_prog,	       perc_composicao6,
   observacao2,		       perc_composicao7,
   observacao3,		       perc_composicao8,
   publico_alvo,	       perc_composicao9,
   cor_de_sortido,	       perc_composicao10,
   tipo_codigo_ean,	       composicao_06,
   num_programa,	       composicao_07,
   referencia_desenv,	       composicao_08,
   tab_rendimento,	       composicao_09,
   largura_cilindro,	       composicao_10,
   largura_estampa,	       simbolo_6,
   descricao_tag1,	       simbolo_7,
   descricao_tag2,	       simbolo_8,
   descr_complementar,	       simbolo_9,
   pasta_banho,		       simbolo_10,
   perimetro_corte,	       parte_composicao_06,
   produto_prototipo,	       parte_composicao_07,
   tempo_laser,		       parte_composicao_08,
   colecao_cliente,	       parte_composicao_09,
   descr_ingles,	       parte_composicao_10,
   descr_espanhol,	       tipo_operacao,
   tamanho_amostra,	       usuario_operacao,
   peso_amostra,	       flag_integracao,
			       importado)
   select
   tipo_atualizacao,	       comp_serra_fita,
   nivel_estrutura,	       parte_composicao_01,
   referencia,		       parte_composicao_02,
   descr_referencia,	       parte_composicao_03,
   unidade_medida,	       parte_composicao_04,
   colecao,		       parte_composicao_05,
   conta_estoque,	       composicao_01,
   linha_produto,	       composicao_02,
   artigo,		       composicao_03,
   artigo_cotas,	       composicao_04,
   tipo_produto,	       composicao_05,
   classific_fiscal,	       composicao_01_ingles,
   comprado_fabric,	       composicao_02_ingles,
   cor_de_estoque,	       composicao_03_ingles,
   tipo_estampa,	       composicao_04_ingles,
   codigo_contabil,	       composicao_05_ingles,
   data_exportacao,	       imp_inf_empresa_tag,
   data_importacao,	       estampa_impressa,
   flag_importacao,	       cod_tipo_aviamento,
   perc_2_qualidade,	       cod_atributo_01,
   artigo_aproveit,	       cod_atributo_02,
   percent_desconto,	       cod_atributo_03,
   variacao_cores,	       cod_atributo_04,
   tamanho_filme,	       cod_atributo_05,
   raportagem,		       cod_atributo_06,
   percent_perda_1,	       cod_atributo_07,
   percent_perda_2,	       cod_atributo_08,
   percent_perda_3,	       cod_atributo_09,
   aproveit_medio,	       cod_atributo_10,
   metros_lineares,	       referencia_agrupador,
   indice_reajuste,	       maquina_piloto,
   item_baixa_estq,	       finura_piloto,
   demanda,		       graduacao_piloto,
   pontos_por_cm,	       programa_piloto,
   observacao,		       programador_piloto,
   agulhas_falhadas,	       peso_bruto_piloto,
   formacao_agulhas,	       peso_acabamento_piloto,
   aberto_tubular,	       tecelagem_piloto,
   numero_roteiro,	       faccao_piloto,
   ref_original,	       pers_bordado_piloto,
   numero_molde,	       pers_lavacao_piloto,
   div_prod,		       pers_serigrafia_piloto,
   cor_de_producao,	       personalizacao_piloto,
   cgc_cliente_2,	       grupo_tecido_corte,
   cgc_cliente_4,	       data_mov_cilindro,
   cgc_cliente_9,	       serie_tamanho,
   perc_composicao1,	       numero_pontos_laser,
   perc_composicao2,	       percentual_amaciante,
   perc_composicao3,	       simbolo_1,
   perc_composicao4,	       simbolo_2,
   perc_composicao5,	       simbolo_3,
   estagio_altera_programado,  simbolo_4,
   risco_padrao,	       simbolo_5,
   responsavel,		       lote_estoque,
   multiplicador,	       dias_maturacao,
   num_sol_desenv,	       cod_bar_por_cli,
   sortimento_diversos,	       familia_atributo,
   grupo_agrupador,	       doc_referencial,
   sub_agrupador,	       ligamento,
   cod_embalagem,
   conf_altera_prog,	       perc_composicao6,
   observacao2,		       perc_composicao7,
   observacao3,		       perc_composicao8,
   publico_alvo,	       perc_composicao9,
   cor_de_sortido,	       perc_composicao10,
   tipo_codigo_ean,	       composicao_06,
   num_programa,	       composicao_07,
   referencia_desenv,	       composicao_08,
   tab_rendimento,	       composicao_09,
   largura_cilindro,	       composicao_10,
   largura_estampa,	       simbolo_6,
   descricao_tag1,	       simbolo_7,
   descricao_tag2,	       simbolo_8,
   descr_complementar,	       simbolo_9,
   pasta_banho,		       simbolo_10,
   perimetro_corte,	       parte_composicao_06,
   produto_prototipo,	       parte_composicao_07,
   tempo_laser,		       parte_composicao_08,
   colecao_cliente,	       parte_composicao_09,
   descr_ingles,	       parte_composicao_10,
   descr_espanhol,	       tipo_operacao,
   tamanho_amostra,	       usuario_operacao,
   peso_amostra,	       flag_integracao,
			       importado
   from e_basi_030
  where e_basi_030.flag_integracao = 1
    and e_basi_030.importado       = 'N';

 exception
 when others
    then inter_pr_insere_hist_001(0, 'JOB', 0, 'i_basi_030', 0, 'INSERT', 'inter_pr_importacao_produtos', 0, SQLCODE, SQLERRM);
    v_reg_ok_30 := 'n';
    raise_application_error (-20000, 'Nao atualizou a tabela i_basi_030' || Chr(10) || SQLERRM);
 end;

 if v_reg_ok_30 = 's'
 then
    begin
       update e_basi_030
          set e_basi_030.importado = 'S'
        where e_basi_030.flag_integracao = 1
          and e_basi_030.importado       = 'N';
    exception
    when others
       then inter_pr_insere_hist_001(0, 'JOB', 0, 'e_basi_030', 0, 'UPDATE', 'inter_pr_importacao_produtos', 0, SQLCODE, SQLERRM);
       v_reg_ok_30 := 'n';
       raise_application_error (-20000, 'Nao atualizou o status da importacao da e_basi_030' || Chr(10) || SQLERRM);
    end;
 end if;

 if v_reg_ok_30 = 's'
 then commit;
 else rollback;
 end if;

 begin

    v_reg_ok_20 := 's';

 insert into i_basi_020(
   tipo_atualizacao,		 sol_friccao_seco_fim,
   basi030_nivel030,		 solidez_friccao_umido,
   basi030_referenc,		 sol_friccao_umido_ini,
   tamanho_ref,			 sol_friccao_umido_fim,
   descr_tam_refer,		 metros_lineares,
   tipo_produto,		 metros_cubicos,
   artigo_cotas,		 agulhas_falhadas,
   largura_1,			 num_agulhas_falhadas,
   gramatura_1,			 numero_pontos,
   peso_rolo,			 polegadas_agulha,
   data_exportacao,		 peso_mini_rolo,
   data_importacao,		 compos_comer01,
   flag_importacao,		 compos_comer02,
   lote_fabr_pecas,		 compos_comer03,
   peso_liquido,		 compos_comer04,
   qtde_max_enfesto,		 descr_ing,
   comp_max_enfesto,		 descr_ing2,
   composicao_01,		 descr_esp,
   composicao_02,		 compos_ing1,
   composicao_03,		 compos_ing2,
   composicao_04,		 compos_ing3,
   composicao_05,		 compos_ing4,
   largura_2,			 compos_esp1,
   gramatura_2,			 compos_esp2,
   gramatura_3,			 compos_esp3,
   avaliacao_toque,		 compos_esp4,
   classe_fio,			 observacao,
   cursos,			 obs_ing,
   elasticidade_aca,		 obs_esp,
   elasticidade_cru,		 gramatura_pre_estampado,
   enco_comprimento,		 peso_rolo_pre_estampado,
   enco_largura,		 largura_pre_estampado,
   fator_cobertura,		 comprim_pre_estampado,
   grau_compactacao,		 perdas_pre_estampado,
   kit_elastano,		 limite_falhas_rolo,
   mercerizado,			 limite_falhas_mini,
   nr_alimentadores,		 tato,
   numero_agulhas,		 referencia_origem,
   perc_composicao1,		 consumo_pasta_normal,
   perc_composicao2,		 largura_cilindro,
   perc_composicao3,		 largura_estampa,
   perc_composicao4,		 compos_ing5,
   perc_composicao5,		 compos_esp5,
   perc_perdas,			 compos_comer05,
   perc_umidade,		 cod_fluxo,
   relach_comprimen,		 perdas_antes_tingimento,
   relach_largura,		 peso_multiplo,
   rendimento,			 tem_dep_rolo,
   temperatura_estp,		 tem_dep_mini_rolo,
   temperatura_fixa,		 tem_dep_estamparia,
   tempo_fixacao,		 composicao_06,
   tempo_termofixar,		 perc_composicao6,
   termofixar,			 diametro,
   titulo_fio,			 grupo_encolhimento,
   torcao_acabado,		 homologacao_fornec,
   torcao_cru,			 litros_absorcao,
   tubular_aberto,		 nr_rolos_cubagem,
   variacao_fio,		 descr_marca_reg,
   fator,			 codigo_embalagem,
   ind_mt_minutos,		 desc_tam_ficha,
   ind_kg_maq_dia,		 comprimento_mini,
   ind_eficiencia,		 caract,
   ne_titulo,			 caract_ing,
   processo_unico_tec,		 caract_esp,
   gramatura_vaporizado,	 perc_difer_peso,
   cod_processo,		 sequencia_tamanho,
   grade_distribuicao,		 raport,
   largura_saida_maquina,	 num_desenvolvimento,
   gramatura_saida_maquina,	 alternativa_padrao,
   cursos_saida_maquina,	 roteiro_padrao,
   colunas_saida_maquina,	 peso_quebra,
   largura_apos_repouso,	 grupo_tecido_costurar,
   gramatura_apos_repouso,	 batidas_polegadas,
   cursos_apos_repouso,		 ordem_compra,
   colunas_apos_repouso,	 tipo_produto_global,
   perc_variacao_gramatura,	 batidas_metro,
   tam_ponto_1,			 codigo_pente,
   tam_ponto_2,			 densidade_pente,
   tam_ponto_3,			 qtde_fios,
   tam_ponto_4,			 qtde_fios_fundo,
   ne_titulo_2,			 qtde_fios_ourela,
   ne_titulo_3,			 perc_encolhimento,
   ne_titulo_4,			 passamento_fundo,
   lfa_1,			 passamento_ourela,
   lfa_2,			 puas_fundo,
   lfa_3,			 puas_ourela,
   lfa_4,			 largura_pente,
   tear_finura,			 contracao_urdume,
   tear_diametro,		 contracao_trama,
   perc_alt_dim_comp,		 oz_yd2,
   perc_alt_dim_larg,		 cor_etiqueta,
   largura_proj,		 linha_producao,
   gramatura_proj,		 titulo_fibra,
   larg_var_ini,		 um_titulo_fibra,
   larg_var_fim,		 simbolo_1,
   gar_var_ini,			 simbolo_2,
   gar_var_fim,			 simbolo_3,
   cursos_var_ini,		 simbolo_4,
   cursos_var_fim,		 simbolo_5,
   colunas_var_ini,		 cod_toler_rendim,
   colunas_var_fim,		 solidez_agua_mar,
   torcao_aca_var_ini,		 sol_agua_mar_ini,
   torcao_aca_var_fim,		 perc_retracao,
   tipo_retorno,		 altura_1,
   perc_var_largura,		 altura_2,
   perc_var_compr,		 cursos_centimetros,
   ne_titulo_1,			 tamanho_retilinea,
   alongamento,			 dias_armazenamento,
   grupo_agrupador,		 peso_medio_cone,
   sub_agrupador,		 un_med_titulo_fio,
   tipo_homologacao,		 grupo_tecido_cortar,
   nro_etiqs_homologacao,	 permite_misturar_maquina,
   solidez_lavacao,		 colunas_ercru,
   sol_lavacao_ini,		 cursos_ercru,
   sol_lavacao_fim,		 gramatura_ercru,
   solidez_suor,		 largura_ercru,
   sol_suor_ini,		 largura_pf_1,
   sol_suor_fim,		 largura_pf_2,
   solidez_agua_clorada,	 altura_pf_1,
   sol_agua_clorada_ini,	 altura_pf_2,
   sol_agua_clorada_fim,	 compri_plotagem,
   sol_agua_mar_fim,		 tipo_operacao,
   solidez_friccao_seco,	 usuario_operacao,
   sol_friccao_seco_ini,	 flag_integracao,
   	                         importado)
   select
   tipo_atualizacao,		 sol_friccao_seco_fim,
   basi030_nivel030,		 solidez_friccao_umido,
   basi030_referenc,		 sol_friccao_umido_ini,
   tamanho_ref,			 sol_friccao_umido_fim,
   descr_tam_refer,		 metros_lineares,
   tipo_produto,		 metros_cubicos,
   artigo_cotas,		 agulhas_falhadas,
   largura_1,			 num_agulhas_falhadas,
   gramatura_1,			 numero_pontos,
   peso_rolo,			 polegadas_agulha,
   data_exportacao,		 peso_mini_rolo,
   data_importacao,		 compos_comer01,
   flag_importacao,		 compos_comer02,
   lote_fabr_pecas,		 compos_comer03,
   peso_liquido,		 compos_comer04,
   qtde_max_enfesto,		 descr_ing,
   comp_max_enfesto,		 descr_ing2,
   composicao_01,		 descr_esp,
   composicao_02,		 compos_ing1,
   composicao_03,		 compos_ing2,
   composicao_04,		 compos_ing3,
   composicao_05,		 compos_ing4,
   largura_2,			 compos_esp1,
   gramatura_2,			 compos_esp2,
   gramatura_3,			 compos_esp3,
   avaliacao_toque,		 compos_esp4,
   classe_fio,			 observacao,
   cursos,			 obs_ing,
   elasticidade_aca,		 obs_esp,
   elasticidade_cru,		 gramatura_pre_estampado,
   enco_comprimento,		 peso_rolo_pre_estampado,
   enco_largura,		 largura_pre_estampado,
   fator_cobertura,		 comprim_pre_estampado,
   grau_compactacao,		 perdas_pre_estampado,
   kit_elastano,		 limite_falhas_rolo,
   mercerizado,			 limite_falhas_mini,
   nr_alimentadores,		 tato,
   numero_agulhas,		 referencia_origem,
   perc_composicao1,		 consumo_pasta_normal,
   perc_composicao2,		 largura_cilindro,
   perc_composicao3,		 largura_estampa,
   perc_composicao4,		 compos_ing5,
   perc_composicao5,		 compos_esp5,
   perc_perdas,			 compos_comer05,
   perc_umidade,		 cod_fluxo,
   relach_comprimen,		 perdas_antes_tingimento,
   relach_largura,		 peso_multiplo,
   rendimento,			 tem_dep_rolo,
   temperatura_estp,		 tem_dep_mini_rolo,
   temperatura_fixa,		 tem_dep_estamparia,
   tempo_fixacao,		 composicao_06,
   tempo_termofixar,		 perc_composicao6,
   termofixar,			 diametro,
   titulo_fio,			 grupo_encolhimento,
   torcao_acabado,		 homologacao_fornec,
   torcao_cru,			 litros_absorcao,
   tubular_aberto,		 nr_rolos_cubagem,
   variacao_fio,		 descr_marca_reg,
   fator,			 codigo_embalagem,
   ind_mt_minutos,		 desc_tam_ficha,
   ind_kg_maq_dia,		 comprimento_mini,
   ind_eficiencia,		 caract,
   ne_titulo,			 caract_ing,
   processo_unico_tec,		 caract_esp,
   gramatura_vaporizado,	 perc_difer_peso,
   cod_processo,		 sequencia_tamanho,
   grade_distribuicao,		 raport,
   largura_saida_maquina,	 num_desenvolvimento,
   gramatura_saida_maquina,	 alternativa_padrao,
   cursos_saida_maquina,	 roteiro_padrao,
   colunas_saida_maquina,	 peso_quebra,
   largura_apos_repouso,	 grupo_tecido_costurar,
   gramatura_apos_repouso,	 batidas_polegadas,
   cursos_apos_repouso,		 ordem_compra,
   colunas_apos_repouso,	 tipo_produto_global,
   perc_variacao_gramatura,	 batidas_metro,
   tam_ponto_1,			 codigo_pente,
   tam_ponto_2,			 densidade_pente,
   tam_ponto_3,			 qtde_fios,
   tam_ponto_4,			 qtde_fios_fundo,
   ne_titulo_2,			 qtde_fios_ourela,
   ne_titulo_3,			 perc_encolhimento,
   ne_titulo_4,			 passamento_fundo,
   lfa_1,			 passamento_ourela,
   lfa_2,			 puas_fundo,
   lfa_3,			 puas_ourela,
   lfa_4,			 largura_pente,
   tear_finura,			 contracao_urdume,
   tear_diametro,		 contracao_trama,
   perc_alt_dim_comp,		 oz_yd2,
   perc_alt_dim_larg,		 cor_etiqueta,
   largura_proj,		 linha_producao,
   gramatura_proj,		 titulo_fibra,
   larg_var_ini,		 um_titulo_fibra,
   larg_var_fim,		 simbolo_1,
   gar_var_ini,			 simbolo_2,
   gar_var_fim,			 simbolo_3,
   cursos_var_ini,		 simbolo_4,
   cursos_var_fim,		 simbolo_5,
   colunas_var_ini,		 cod_toler_rendim,
   colunas_var_fim,		 solidez_agua_mar,
   torcao_aca_var_ini,		 sol_agua_mar_ini,
   torcao_aca_var_fim,		 perc_retracao,
   tipo_retorno,		 altura_1,
   perc_var_largura,		 altura_2,
   perc_var_compr,		 cursos_centimetros,
   ne_titulo_1,			 tamanho_retilinea,
   alongamento,			 dias_armazenamento,
   grupo_agrupador,		 peso_medio_cone,
   sub_agrupador,		 un_med_titulo_fio,
   tipo_homologacao,		 grupo_tecido_cortar,
   nro_etiqs_homologacao,	 permite_misturar_maquina,
   solidez_lavacao,		 colunas_ercru,
   sol_lavacao_ini,		 cursos_ercru,
   sol_lavacao_fim,		 gramatura_ercru,
   solidez_suor,		 largura_ercru,
   sol_suor_ini,		 largura_pf_1,
   sol_suor_fim,		 largura_pf_2,
   solidez_agua_clorada,	 altura_pf_1,
   sol_agua_clorada_ini,	 altura_pf_2,
   sol_agua_clorada_fim,	 compri_plotagem,
   sol_agua_mar_fim,		 tipo_operacao,
   solidez_friccao_seco,	 usuario_operacao,
   sol_friccao_seco_ini,	 flag_integracao,
	                         importado
   from	e_basi_020
  where e_basi_020.flag_integracao = 1
    and e_basi_020.importado       = 'N';

 exception
 when others
    then inter_pr_insere_hist_001(0, 'JOB', 0, 'i_basi_020', 0, 'UPDATE', 'inter_pr_importacao_produtos', 0, SQLCODE, SQLERRM);
    v_reg_ok_20 := 'n';
    raise_application_error (-20000, 'Nao atualizou a tabela i_basi_020' || Chr(10) || SQLERRM);
 end;

 if v_reg_ok_20 = 's'
 then
    begin
       update e_basi_020
          set e_basi_020.importado = 'S'
        where e_basi_020.flag_integracao = 1
          and e_basi_020.importado       = 'N';
    exception
    when others
       then inter_pr_insere_hist_001(0, 'JOB', 0, 'e_basi_020', 0, 'UPDATE', 'inter_pr_importacao_produtos', 0, SQLCODE, SQLERRM);
       v_reg_ok_20 := 'n';
       raise_application_error (-20000, 'Nao atualizou o status da importacao da e_basi_020' || Chr(10) || SQLERRM);
    end;
 end if;

 if v_reg_ok_20 = 's'
 then commit;
 else rollback;
 end if;


 begin

    v_reg_ok_10 := 's';

 insert into i_basi_010(
   tipo_atualizacao,	    	tam_ponto_3,
   nivel_estrutura,	    	tam_ponto_4,
   grupo_estrutura,	    	ne_titulo,
   subgru_estrutura,	    	ne_titulo_2,
   item_estrutura,		ne_titulo_3,
   descricao_15,		ne_titulo_4,
   item_ativo,		    	lfa_1,
   classific_fiscal,	    	lfa_2,
   codigo_velho,		lfa_3,
   data_cadastro,		lfa_4,
   data_ult_compra,	    	tear_finura,
   data_ult_entrada,	    	tear_diametro,
   data_ult_saida,		perc_alt_dim_comp,
   preco_medio,		    	perc_alt_dim_larg,
   preco_ult_compra,	    	largura_proj,
   preco_custo,		    	num_agulhas,
   preco_custo_info,	    	cursos,
   preco_medio_ant,	    	colunas,
   estoque_minimo,		perc_perdas,
   tempo_reposicao,	    	fator_correcao,
   lote_multiplo,		gramatura_proj,
   consumo_medio,		ponto_reposicao,
   narrativa,		    	tipo_cor,
   narrativa_ingles,	    	codigo_cliente,
   narrativa2,		    	qtde_min_venda,
   narrativa_espanhol,	    	tonalidade,
   tipo_mat_prima,		concentracao,
   estoque_maximo,		motivo_uso,
   data_desativacao,	    	complemento,
   tipo_prod_quimico,	    	qtde_minima,
   artigo_cotas,		sequencia_tamanho,
   data_lancamento,	    	preco_contratipo,
   largura,		    	nivel_antigo,
   gramatura,		    	grupo_antigo,
   data_exportacao,	    	subgrupo_antigo,
   data_importacao,	    	item_antigo,
   flag_importacao,	    	alternativa_acabado,
   classificacao_ncm,	    	codigo_contabil,
   numero_roteiro,		prioridade_distribuicao,
   numero_alternati,	    	agrupador_linha,
   distribuicao_cor,	    	alternativa_custos,
   codigo_barras,		roteiro_custos,
   imprime_ficha,		nr_fios_fita,
   qtde_ups,		    	item_agrupador,
   qtde_elaboracao,	    	produto_integracao,
   natur_operacao,		estacao_dosagem_orgatex,
   sugere_item,		    	ind_envia_infotint,
   cgc_cliente_2,		perc_detraccion,
   cgc_cliente_4,		classificacao_ibama,
   cgc_cliente_9,		cod_servico_lst,
   grau_solidez,		qtde_min_venda_min,
   classificacao_naladi,	integrado_decisor,
   numero_grafico,		combinacao_projeto,
   cod_tipo_volume,	    	destino_projeto,
   grade_distribuicao,	    	tipo_operacao,
   perc_cor,		    	usuario_operacao,
   tam_ponto_1,		    	flag_integracao,
   tam_ponto_2,		    	importado)
 select
   tipo_atualizacao,	    	tam_ponto_3,
   nivel_estrutura,	    	tam_ponto_4,
   grupo_estrutura,	    	ne_titulo,
   subgru_estrutura,	    	ne_titulo_2,
   item_estrutura,		ne_titulo_3,
   descricao_15,		ne_titulo_4,
   item_ativo,		    	lfa_1,
   classific_fiscal,	    	lfa_2,
   codigo_velho,		lfa_3,
   data_cadastro,		lfa_4,
   data_ult_compra,	    	tear_finura,
   data_ult_entrada,	    	tear_diametro,
   data_ult_saida,		perc_alt_dim_comp,
   preco_medio,		    	perc_alt_dim_larg,
   preco_ult_compra,	    	largura_proj,
   preco_custo,		    	num_agulhas,
   preco_custo_info,	    	cursos,
   preco_medio_ant,	    	colunas,
   estoque_minimo,		perc_perdas,
   tempo_reposicao,	    	fator_correcao,
   lote_multiplo,		gramatura_proj,
   consumo_medio,		ponto_reposicao,
   narrativa,		    	tipo_cor,
   narrativa_ingles,	    	codigo_cliente,
   narrativa2,		    	qtde_min_venda,
   narrativa_espanhol,	    	tonalidade,
   tipo_mat_prima,		concentracao,
   estoque_maximo,		motivo_uso,
   data_desativacao,	    	complemento,
   tipo_prod_quimico,	    	qtde_minima,
   artigo_cotas,		sequencia_tamanho,
   data_lancamento,	    	preco_contratipo,
   largura,		    	nivel_antigo,
   gramatura,		    	grupo_antigo,
   data_exportacao,	    	subgrupo_antigo,
   data_importacao,	    	item_antigo,
   flag_importacao,	    	alternativa_acabado,
   classificacao_ncm,	    	codigo_contabil,
   numero_roteiro,		prioridade_distribuicao,
   numero_alternati,	    	agrupador_linha,
   distribuicao_cor,	    	alternativa_custos,
   codigo_barras,		roteiro_custos,
   imprime_ficha,		nr_fios_fita,
   qtde_ups,		    	item_agrupador,
   qtde_elaboracao,	    	produto_integracao,
   natur_operacao,		estacao_dosagem_orgatex,
   sugere_item,		    	ind_envia_infotint,
   cgc_cliente_2,		perc_detraccion,
   cgc_cliente_4,		classificacao_ibama,
   cgc_cliente_9,		cod_servico_lst,
   grau_solidez,		qtde_min_venda_min,
   classificacao_naladi,	integrado_decisor,
   numero_grafico,		combinacao_projeto,
   cod_tipo_volume,	    	destino_projeto,
   grade_distribuicao,	    	tipo_operacao,
   perc_cor,		    	usuario_operacao,
   tam_ponto_1,		    	flag_integracao,
   tam_ponto_2,		    	importado
  from e_basi_010
 where e_basi_010.flag_integracao = 1
   and e_basi_010.importado       = 'N';

 exception
 when others
    then inter_pr_insere_hist_001(0, 'JOB', 0, 'i_basi_010', 0, 'INSERT', 'inter_pr_importacao_produtos', 0, SQLCODE, SQLERRM);
    v_reg_ok_10 := 'n';
    raise_application_error (-20000, 'Nao atualizou a tabela i_basi_010' || Chr(10) || SQLERRM);
 end;

 if v_reg_ok_10 = 's'
 then
    begin
       update e_basi_010
          set e_basi_010.importado = 'S'
        where e_basi_010.flag_integracao = 1
          and e_basi_010.importado       = 'N';
    exception
    when others
       then inter_pr_insere_hist_001(0, 'JOB', 0, 'e_basi_010', 0, 'INSERT', 'inter_pr_importacao_produtos', 0, SQLCODE, SQLERRM);
       v_reg_ok_10 := 'n';
       raise_application_error (-20000, 'Nao atualizou o status da importacao da e_basi_010' || Chr(10) || SQLERRM);
    end;
 end if;

 if v_reg_ok_10 = 's'
 then commit;
 else rollback;
 end if;


 begin

    v_reg_ok_100 := 's';

 insert into i_basi_100(
   cor_sortimento,	 cor_pantone,
   tipo_cor,		 cor_numerica,
   descricao,		 cor_representativa,
   serie_cor,		 cod_cor_cliente,
   numero_quadro,	 desc_cor_cliente,
   cor_de_fundo,	 cgc_cli9,
   sortim_estampa,	 cgc_cli4,
   observacao1,		 cgc_cli2,
   observacao2,		 ind_envia_infotint,
   relacao_banho,	 data_importacao,
   tipo_tingimento,	 tipo_operacao,
   tonalidade,		 usuario_operacao,
   seq_degrade,		 flag_integracao,
   cor_integracao,	 importado)
 select
   cor_sortimento,	 cor_pantone,
   tipo_cor,		 cor_numerica,
   descricao,		 cor_representativa,
   serie_cor,		 cod_cor_cliente,
   numero_quadro,	 desc_cor_cliente,
   cor_de_fundo,	 cgc_cli9,
   sortim_estampa,	 cgc_cli4,
   observacao1,		 cgc_cli2,
   observacao2,		 ind_envia_infotint,
   relacao_banho,	 data_importacao,
   tipo_tingimento,	 tipo_operacao,
   tonalidade,		 usuario_operacao,
   seq_degrade,		 flag_integracao,
   cor_integracao,	 importado
 from e_basi_100
where e_basi_100.flag_integracao = 1
  and e_basi_100.importado       = 'N';

 exception
 when others
    then inter_pr_insere_hist_001(0, 'JOB', 0, 'i_basi_100', 0, 'INSERT', 'inter_pr_importacao_produtos', 0, SQLCODE, SQLERRM);
    v_reg_ok_100 := 'n';
    raise_application_error (-20000, 'Nao atualizou a tabela i_basi_100' || Chr(10) || SQLERRM);
 end;

 if v_reg_ok_100 = 's'
 then
    begin
       update e_basi_100
          set e_basi_100.importado = 'S'
        where e_basi_100.flag_integracao = 1
          and e_basi_100.importado       = 'N';
    exception
    when others
       then inter_pr_insere_hist_001(0, 'JOB', 0, 'e_basi_100', 0, 'UPDATE', 'inter_pr_importacao_produtos', 0, SQLCODE, SQLERRM);
       v_reg_ok_100 := 'n';
       raise_application_error (-20000, 'Nao atualizou o status da importacao da e_basi_100' || Chr(10) || SQLERRM);
    end;
 end if;

 if v_reg_ok_100 = 's'
 then commit;
 else rollback;
 end if;


end inter_pr_importacao_produtos;
 

/

exec inter_pr_recompile;

