alter table pedi_080 add nat_oper_ret_indus number(3) default 0;

comment on column pedi_080.nat_oper_ret_indus is 'Código da Natureza de Operação a ser utilizada para retorno de industrialização (no fatu_f051 e cálculo do faturamento)';

exec inter_pr_recompile;
