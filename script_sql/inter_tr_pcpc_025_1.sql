
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_025_1" 
  before insert or
         update of tamanho,
                   qtde_programada
  on pcpc_025
  for each row
declare
  v_referencia  varchar2(5);
  v_executa_trigger number;
begin

  if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting or updating
      then
         v_referencia := '00000';

         begin
            select pcpc_020.referencia_peca
            into v_referencia
            from pcpc_020
            where pcpc_020.ordem_producao = :new.ordem_producao;
         exception when no_data_found then
            v_referencia := '00000';
         end;

         begin
            :new.sequencia_tamanho := inter_fn_busca_seq_tamanho('1',v_referencia,:new.tamanho);
         end;

         INTER_PR_MARCA_REFERENCIA_OP('0', '00000', '000', '000000',
                                      :new.ordem_producao,
                                      1);
      end if;

      if  updating
      and (:new.qtde_programada_orig =  0
      and  :old.qtde_programada      <> 0
      and  :old.qtde_programada      <> :new.qtde_programada
      and  :new.qtde_programada      <> 0)
      then
         :new.qtde_programada_orig := :old.qtde_programada;
      end if;

      if :new.qtde_programada_orig is null
      then
         :new.qtde_programada_orig := 0;
      end if;
   end if;

end inter_tr_pcpc_025_1;

-- ALTER TRIGGER "INTER_TR_PCPC_025_1" ENABLE
 

/

exec inter_pr_recompile;

