alter table fatu_060
add (
  perc_substituicao      NUMBER(5,2)
);

comment on column FATU_060.perc_substituicao
  is 'Percentual de substituição do item da nota fiscal';

/
exec inter_pr_recompile;
/
