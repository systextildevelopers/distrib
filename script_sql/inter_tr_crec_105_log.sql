
  CREATE OR REPLACE TRIGGER "INTER_TR_CREC_105_LOG" 
after insert or delete or update
on crec_105
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid); 

 if inserting
 then
    begin

        insert into crec_105_log (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           codigo_historico_old,   /*8*/
           codigo_historico_new,   /*9*/
           descr_historico_old,   /*10*/
           descr_historico_new,   /*11*/
           tipo_historico_old,   /*12*/
           tipo_historico_new,   /*13*/
           base_de_imposto_old,   /*14*/
           base_de_imposto_new,   /*15*/
           conta_contabil_old,   /*16*/
           conta_contabil_new,   /*17*/
           historico_contab_old,   /*18*/
           historico_contab_new,   /*19*/
           codigo_contabil_old,   /*20*/
           codigo_contabil_new,   /*21*/
           gera_contab_old,   /*22*/
           gera_contab_new    /*23*/
        ) values (
            'i', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.codigo_historico, /*9*/
           '',/*10*/
           :new.descr_historico, /*11*/
           '',/*12*/
           :new.tipo_historico, /*13*/
           '',/*14*/
           :new.base_de_imposto, /*15*/
           '',/*16*/
           :new.conta_contabil, /*17*/
           0,/*18*/
           :new.historico_contab, /*19*/
           0,/*20*/
           :new.codigo_contabil, /*21*/
           0,/*22*/
           :new.gera_contab /*23*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into crec_105_log (
           tipo_ocorr, /*0*/
           data_ocorr, /*1*/
           hora_ocorr, /*2*/
           usuario_rede, /*3*/
           maquina_rede, /*4*/
           aplicacao, /*5*/
           usuario_sistema, /*6*/
           nome_programa, /*7*/
           codigo_historico_old, /*8*/
           codigo_historico_new, /*9*/
           descr_historico_old, /*10*/
           descr_historico_new, /*11*/
           tipo_historico_old, /*12*/
           tipo_historico_new, /*13*/
           base_de_imposto_old, /*14*/
           base_de_imposto_new, /*15*/
           conta_contabil_old, /*16*/
           conta_contabil_new, /*17*/
           historico_contab_old, /*18*/
           historico_contab_new, /*19*/
           codigo_contabil_old, /*20*/
           codigo_contabil_new, /*21*/
           gera_contab_old, /*22*/
           gera_contab_new  /*23*/
        ) values (
            'a', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.codigo_historico,  /*8*/
           :new.codigo_historico, /*9*/
           :old.descr_historico,  /*10*/
           :new.descr_historico, /*11*/
           :old.tipo_historico,  /*12*/
           :new.tipo_historico, /*13*/
           :old.base_de_imposto,  /*14*/
           :new.base_de_imposto, /*15*/
           :old.conta_contabil,  /*16*/
           :new.conta_contabil, /*17*/
           :old.historico_contab,  /*18*/
           :new.historico_contab, /*19*/
           :old.codigo_contabil,  /*20*/
           :new.codigo_contabil, /*21*/
           :old.gera_contab,  /*22*/
           :new.gera_contab  /*23*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into crec_105_log (
           tipo_ocorr, /*0*/
           data_ocorr, /*1*/
           hora_ocorr, /*2*/
           usuario_rede, /*3*/
           maquina_rede, /*4*/
           aplicacao, /*5*/
           usuario_sistema, /*6*/
           nome_programa, /*7*/
           codigo_historico_old, /*8*/
           codigo_historico_new, /*9*/
           descr_historico_old, /*10*/
           descr_historico_new, /*11*/
           tipo_historico_old, /*12*/
           tipo_historico_new, /*13*/
           base_de_imposto_old, /*14*/
           base_de_imposto_new, /*15*/
           conta_contabil_old, /*16*/
           conta_contabil_new, /*17*/
           historico_contab_old, /*18*/
           historico_contab_new, /*19*/
           codigo_contabil_old, /*20*/
           codigo_contabil_new, /*21*/
           gera_contab_old, /*22*/
           gera_contab_new /*23*/
        ) values (
            'd', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.codigo_historico, /*8*/
           0, /*9*/
           :old.descr_historico, /*10*/
           '', /*11*/
           :old.tipo_historico, /*12*/
           '', /*13*/
           :old.base_de_imposto, /*14*/
           '', /*15*/
           :old.conta_contabil, /*16*/
           '', /*17*/
           :old.historico_contab, /*18*/
           0, /*19*/
           :old.codigo_contabil, /*20*/
           0, /*21*/
           :old.gera_contab, /*22*/
           0 /*23*/
         );
    end;
 end if;
end inter_tr_crec_105_log;

-- ALTER TRIGGER "INTER_TR_CREC_105_LOG" ENABLE
 

/

exec inter_pr_recompile;

