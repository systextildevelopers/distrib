CREATE OR REPLACE TRIGGER TRIGGER_PCPB_030_LOG
BEFORE UPDATE OR DELETE OR INSERT on pcpb_030
for each row

declare
   ws_ordem_producao             pcpb_030.ordem_producao%type;
   ws_pano_nivel99               pcpb_030.pano_nivel99%type;
   ws_pano_grupo                 pcpb_030.pano_grupo%type;
   ws_pano_subgrupo              pcpb_030.pano_subgrupo%type;
   ws_pano_item                  pcpb_030.pano_item%type;
   ws_codigo_deposito            pcpb_030.codigo_deposito%type;
   ws_pedido_corte               pcpb_030.pedido_corte%type;
   ws_sequenci_periodo           pcpb_030.sequenci_periodo%type;
   ws_nr_pedido_ordem            pcpb_030.nr_pedido_ordem%type;
   ws_qtde_rolos_prog            pcpb_030.qtde_rolos_prog%type;
   ws_qtde_quilos_prog           pcpb_030.qtde_quilos_prog%type;
   ws_qtde_quilos_produzido      pcpb_030.qtde_quilos_produzido%type;
   ws_qtde_rolos_produzido       pcpb_030.qtde_rolos_produzido%type;
   ws_ordem_producao_old         pcpb_030.ordem_producao%type;
   ws_pano_nivel99_old           pcpb_030.pano_nivel99%type;
   ws_pano_grupo_old             pcpb_030.pano_grupo%type;
   ws_pano_subgrupo_old          pcpb_030.pano_subgrupo%type;
   ws_pano_item_old              pcpb_030.pano_item%type;
   ws_codigo_deposito_old        pcpb_030.codigo_deposito%type;
   ws_pedido_corte_old           pcpb_030.pedido_corte%type;
   ws_sequenci_periodo_old       pcpb_030.sequenci_periodo%type;
   ws_nr_pedido_ordem_old        pcpb_030.nr_pedido_ordem%type;
   ws_qtde_rolos_prog_old        pcpb_030.qtde_rolos_prog%type;
   ws_qtde_quilos_prog_old       pcpb_030.qtde_quilos_prog%type;
   ws_qtde_quilos_produzido_old  pcpb_030.qtde_quilos_produzido%type;
   ws_qtde_rolos_produzido_old   pcpb_030.qtde_rolos_produzido%type;
   ws_tipo_ocorr                 varchar2(1);
   ws_usuario_rede               varchar2(20);
   ws_maquina_rede               varchar2(40);
   ws_aplicacao                  varchar2(20);
   ws_processo_systextil         hdoc_090.programa%type;
   ws_sid                        number(9);
   ws_usuario_systextil          varchar2(250);
   ws_empresa                    varchar2(20);
   ws_locale_usuario             varchar2(5);
begin
   if INSERTING then
      ws_ordem_producao                  := :new.ordem_producao;
      ws_pano_nivel99                    := :new.pano_nivel99;
      ws_pano_grupo                      := :new.pano_grupo;
      ws_pano_subgrupo                   := :new.pano_subgrupo;
      ws_pano_item                       := :new.pano_item;
      ws_codigo_deposito                 := :new.codigo_deposito;
      ws_pedido_corte                    := :new.pedido_corte;
      ws_sequenci_periodo                := :new.sequenci_periodo;
      ws_nr_pedido_ordem                 := :new.nr_pedido_ordem;
      ws_qtde_rolos_prog                 := :new.qtde_rolos_prog;
      ws_qtde_quilos_prog                := :new.qtde_quilos_prog;
      ws_qtde_quilos_produzido           := :new.qtde_quilos_produzido;
      ws_qtde_rolos_produzido            := :new.qtde_rolos_produzido;
      ws_ordem_producao_old              := 0;
      ws_pano_nivel99_old                := '';
      ws_pano_grupo_old                  := '';
      ws_pano_subgrupo_old               := '';
      ws_pano_item_old                   := '';
      ws_codigo_deposito_old             := 0;
      ws_pedido_corte_old                := 0;
      ws_sequenci_periodo_old            := 0;
      ws_nr_pedido_ordem_old             := 0;
      ws_qtde_rolos_prog_old             := 0.00;
      ws_qtde_quilos_prog_old            := 0.00;
      ws_qtde_quilos_produzido_old       := 0.00;
      ws_qtde_rolos_produzido_old        := 0.00;
      ws_tipo_ocorr                      := 'I';
   else
      if UPDATING then
         ws_ordem_producao                  := :new.ordem_producao;
         ws_pano_nivel99                    := :new.pano_nivel99;
         ws_pano_grupo                      := :new.pano_grupo;
         ws_pano_subgrupo                   := :new.pano_subgrupo;
         ws_pano_item                       := :new.pano_item;
         ws_codigo_deposito                 := :new.codigo_deposito;
         ws_pedido_corte                    := :new.pedido_corte;
         ws_sequenci_periodo                := :new.sequenci_periodo;
         ws_nr_pedido_ordem                 := :new.nr_pedido_ordem;
         ws_qtde_rolos_prog                 := :new.qtde_rolos_prog;
         ws_qtde_quilos_prog                := :new.qtde_quilos_prog;
         ws_qtde_quilos_produzido           := :new.qtde_quilos_produzido;
         ws_qtde_rolos_produzido            := :new.qtde_rolos_produzido;
         ws_ordem_producao_old              := :old.ordem_producao;
         ws_pano_nivel99_old                := :old.pano_nivel99;
         ws_pano_grupo_old                  := :old.pano_grupo;
         ws_pano_subgrupo_old               := :old.pano_subgrupo;
         ws_pano_item_old                   := :old.pano_item;
         ws_codigo_deposito_old             := :old.codigo_deposito;
         ws_pedido_corte_old                := :old.pedido_corte;
         ws_sequenci_periodo_old            := :old.sequenci_periodo;
         ws_nr_pedido_ordem_old             := :old.nr_pedido_ordem;
         ws_qtde_rolos_prog_old             := :old.qtde_rolos_prog;
         ws_qtde_quilos_prog_old            := :old.qtde_quilos_prog;
         ws_qtde_quilos_produzido_old       := :old.qtde_quilos_produzido;
         ws_qtde_rolos_produzido_old        := :old.qtde_rolos_produzido;
         ws_tipo_ocorr                      := 'A';
      else
         ws_ordem_producao                  := 0;
         ws_pano_nivel99                    := '';
         ws_pano_grupo                      := '';
         ws_pano_subgrupo                   := '';
         ws_pano_item                       := '';
         ws_codigo_deposito                 := 0;
         ws_pedido_corte                    := 0;
         ws_sequenci_periodo                := 0;
         ws_nr_pedido_ordem                 := 0;
         ws_qtde_rolos_prog                 := 0.00;
         ws_qtde_quilos_prog                := 0.00;
         ws_qtde_quilos_produzido           := 0.00;
         ws_qtde_rolos_produzido            := 0.00;
         ws_ordem_producao_old              := :old.ordem_producao;
         ws_pano_nivel99_old                := :old.pano_nivel99;
         ws_pano_grupo_old                  := :old.pano_grupo;
         ws_pano_subgrupo_old               := :old.pano_subgrupo;
         ws_pano_item_old                   := :old.pano_item;
         ws_codigo_deposito_old             := :old.codigo_deposito;
         ws_pedido_corte_old                := :old.pedido_corte;
         ws_sequenci_periodo_old            := :old.sequenci_periodo;
         ws_nr_pedido_ordem_old             := :old.nr_pedido_ordem;
         ws_qtde_rolos_prog_old             := :old.qtde_rolos_prog;
         ws_qtde_quilos_prog_old            := :old.qtde_quilos_prog;
         ws_qtde_quilos_produzido_old       := :old.qtde_quilos_produzido;
         ws_qtde_rolos_produzido_old        := :old.qtde_rolos_produzido;
         ws_tipo_ocorr                      := 'D';
      end if;
   end if;   
   inter_pr_dados_usuario(ws_usuario_rede, ws_maquina_rede, ws_aplicacao, ws_sid, ws_usuario_systextil, ws_empresa, ws_locale_usuario);
   ws_processo_systextil := inter_fn_nome_programa(ws_sid);
   
   INSERT INTO pcpb_030_log
          (ordem_producao,                pano_nivel99,
           pano_grupo,                    pano_subgrupo,
           pano_item,                     codigo_deposito,
           pedido_corte,                  sequenci_periodo,
           nr_pedido_ordem,               qtde_rolos_prog,
           qtde_quilos_prog,              qtde_quilos_produzido,
           qtde_rolos_produzido,          ordem_producao_old,
           pano_nivel99_old,              pano_grupo_old,
           pano_subgrupo_old,             pano_item_old,
           codigo_deposito_old,           pedido_corte_old,
           sequenci_periodo_old,          nr_pedido_ordem_old,
           qtde_rolos_prog_old,           qtde_quilos_prog_old,
           qtde_quilos_produzido_old,     qtde_rolos_produzido_old,
           data_ocorr,                    tipo_ocorr,
           usuario_rede,                  maquina_rede,
           aplicacao,                     processo_systextil,
           usuario_systextil)
   VALUES (ws_ordem_producao,             ws_pano_nivel99,
           ws_pano_grupo,                 ws_pano_subgrupo,
           ws_pano_item,                  ws_codigo_deposito,
           ws_pedido_corte,               ws_sequenci_periodo,
           ws_nr_pedido_ordem,            ws_qtde_rolos_prog,
           ws_qtde_quilos_prog,           ws_qtde_quilos_produzido,
           ws_qtde_rolos_produzido,       ws_ordem_producao_old,
           ws_pano_nivel99_old,           ws_pano_grupo_old,
           ws_pano_subgrupo_old,          ws_pano_item_old,
           ws_codigo_deposito_old,        ws_pedido_corte_old,
           ws_sequenci_periodo_old,       ws_nr_pedido_ordem_old,
           ws_qtde_rolos_prog_old,        ws_qtde_quilos_prog_old,
           ws_qtde_quilos_produzido_old,  ws_qtde_rolos_produzido_old,
           sysdate,                       ws_tipo_ocorr,
           ws_usuario_rede,               ws_maquina_rede,
           ws_aplicacao,                  ws_processo_systextil,
           ws_usuario_systextil);
           
end trigger_pcpb_030_log;

-- ALTER TRIGGER TRIGGER_PCPB_030_LOG ENABLE
/
