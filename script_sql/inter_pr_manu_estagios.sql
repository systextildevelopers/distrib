CREATE OR REPLACE PROCEDURE "INTER_PR_MANU_ESTAGIOS"
    -- Recebe parametros para criacao dos estagios e operacoes.
   (v_ordem_producao   in number,  v_sequencia_tec   in number,      
    v_qtde_quilos_prog in number,  v_vip             in number default 0,
    v_tipo_ordem       in number default -1)
is 
   v_codigo_perfil        number;
   v_posicao              number;
   v_flag_antes           number;
   v_flag_depois          number;
   v_contador             number;
   v_grupo_maquinas_aux   mqop_040.grupo_maquinas%type := '####';
   v_sub_maquina_aux      mqop_040.sub_maquina%type    := '###';
   v_controle             number;

   -- ord = 1 ou -1 (ASC ou DESC)
   procedure p1(ord number, cod_perfil number, seq_operacao number) as
      v_contador number;
      v_cont     number;
      v_seq      number;
      v_cod_estagio_agrupador  pcpb_015.cod_estagio_agrupador%type;
      v_seq_operacao_agrupador pcpb_015.seq_operacao_agrupador%type;
   begin
      v_contador := 0;
      for reg_pcpb161 in (select pcpb_161.codigo_operacao,
                  pcpb_161.codigo_estagio
           from  pcpb_161
           where pcpb_161.codigo_perfil = cod_perfil
           order by ord * pcpb_161.seq_operacao)
      loop
         v_contador := v_contador + ord;
         
         -- pcpb_040 - Validac�o de estagios
         v_cont := 0;     
         v_seq  := seq_operacao + v_contador;
         
         begin
           select 1 
           into v_cont
           from pcpb_040
           where pcpb_040.ordem_producao = v_ordem_producao
             and pcpb_040.seq_operacao   = v_seq;
         exception
           when no_data_found then
             v_cont := 0;
         end;
         
         v_seq  := 0;
                 
         -- pcpb_018
         begin
           insert into pcpb_018
           (pcpb_018.ordem_producao,     pcpb_018.seq_tecido,
            pcpb_018.seq_operacao,       pcpb_018.codigo_operacao,
            pcpb_018.quantidade,         pcpb_018.minutos
           )
           values
           (v_ordem_producao,                        v_sequencia_tec,
           (seq_operacao + v_contador), reg_pcpb161.codigo_operacao,
            v_qtde_quilos_prog,                      0
           );
         exception       
            WHEN DUP_VAL_ON_INDEX THEN
            -- variavel sem uso, apenas para controle
            v_controle := 1;
         end;
                 
         -- pcpb_015
         begin
            select mqop_040.grupo_maquinas
                  ,mqop_040.sub_maquina
            into   v_grupo_maquinas_aux,        
                   v_sub_maquina_aux
            from mqop_040
            where mqop_040.codigo_operacao = reg_pcpb161.codigo_operacao;
            exception
            when others then
               v_grupo_maquinas_aux := ' ';
               v_sub_maquina_aux := ' ';
         end;  
                 
         if v_cont = 0
         then
            begin
                select pcpb_015.cod_estagio_agrupador
                     , pcpb_015.seq_operacao_agrupador
                into   v_cod_estagio_agrupador
                     , v_seq_operacao_agrupador
                from pcpb_015
                where pcpb_015.ordem_producao = v_ordem_producao
                and   pcpb_015.seq_operacao   in (
                            select nvl(min(pcpb_015.seq_operacao),0)
                            from pcpb_015
                            where pcpb_015.ordem_producao = v_ordem_producao
                            and   pcpb_015.seq_operacao   > (seq_operacao + v_contador)
                                                );
            exception when others then
                v_cod_estagio_agrupador  := reg_pcpb161.codigo_estagio;
                v_seq_operacao_agrupador := (seq_operacao + v_contador);
            end;
            
            begin            
              insert into pcpb_015 (
                pcpb_015.ordem_producao,      pcpb_015.seq_operacao,
                pcpb_015.codigo_operacao,     pcpb_015.grupo_maquina,
                pcpb_015.subgrupo_maquina,    pcpb_015.codigo_estagio,
                pcpb_015.minutos_unitario,
                pcpb_015.cod_estagio_agrupador,
                pcpb_015.seq_operacao_agrupador,
                pcpb_015.regra_estagio_agrupador,
                pcpb_015.dt_hr_regra_estagio_agrupador
              ) values (
                v_ordem_producao,            (seq_operacao + v_contador),
                reg_pcpb161.codigo_operacao,  v_grupo_maquinas_aux,
                v_sub_maquina_aux,            reg_pcpb161.codigo_estagio,
                0,
                v_cod_estagio_agrupador,
                v_seq_operacao_agrupador,
                20,
                sysdate
              );
            exception       
               WHEN DUP_VAL_ON_INDEX THEN
               -- variavel sem uso, apenas para controle
               v_controle := 1;
            end;
                    
            -- pcpb_040
            begin
              insert into pcpb_040
              (ordem_producao,    codigo_estagio,
               seq_operacao)
              values
               (v_ordem_producao, reg_pcpb161.codigo_estagio,
               (seq_operacao + v_contador));
            exception       
                WHEN DUP_VAL_ON_INDEX THEN
                -- variavel sem uso, apenas para controle
                v_controle := 1;
            end;
         end if;   
      end loop;
   end p1;

begin
   -- Busca parametros - VIP
     -- if v_cli_vip = 1
     -- then
      begin
         select pcpb_162.posicao,
                pcpb_162.codigo_perfil,
                pcpb_162.flag_antes,
                pcpb_162.flag_depois
         into   v_posicao,
                v_codigo_perfil,
                v_flag_antes,
                v_flag_depois
         from  pcpb_160, pcpb_162
         where pcpb_160.codigo_perfil = pcpb_162.codigo_perfil
           and ((pcpb_160.opcao = 3
                 and v_vip in (pcpb_162.vip1, pcpb_162.vip2, pcpb_162.vip3, pcpb_162.vip4, pcpb_162.vip5, pcpb_162.vip6)
                 and v_vip > 0) -- Vip
             or (pcpb_160.opcao = 4
                 and pcpb_162.tipo_ordem = v_tipo_ordem))
         order by opcao;
      exception
         when others then
            v_posicao := 0;
            v_codigo_perfil := 0;
            v_flag_antes := 0;
            v_flag_depois := 0;
      end;
   
      for reg_pcpb_040 in (select pcpb_040.codigo_estagio,  
                                  pcpb_040.seq_operacao
                           from pcpb_040
                           where pcpb_040.ordem_producao = v_ordem_producao
                           and   pcpb_040.codigo_estagio = v_posicao
                           order by pcpb_040.codigo_estagio,
                                    pcpb_040.seq_operacao asc)
      loop 
         -- Insere ANTES
         if v_flag_antes = 1
         then
            p1(-1, v_codigo_perfil, reg_pcpb_040.seq_operacao);
         end if;
           
         -- Insere DESPOIS
         if v_flag_depois = 1
         then
            p1(1, v_codigo_perfil, reg_pcpb_040.seq_operacao);
         end if;
      end loop;
     -- end if;
end INTER_PR_MANU_ESTAGIOS;

/

exec inter_pr_recompile;
