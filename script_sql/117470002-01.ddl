alter table i_obrf_015 add
ncm_xml varchar2(15);

alter table i_obrf_015 add
perc_icms_xml number(9,2);

alter table i_obrf_015 add
orig_icms_xml number(9);

alter table i_obrf_015 add
cst_icms_xml number(2);

alter table i_obrf_015 add
base_calc_icms_xml number(15,2);

alter table i_obrf_015 add
valor_icms_xml number(15,2);

alter table i_obrf_015 add
perc_icms_subs_xml number(5,2);

alter table i_obrf_015 add
base_icms_subs_xml number(14,2);

alter table i_obrf_015 add
valor_icms_subs_xml number(14,2);

alter table i_obrf_015 add
base_pis_cofins_xml number(15,2);

alter table i_obrf_015 add
perc_pis_xml number(6,2);

alter table i_obrf_015 add
cst_pis_xml number(2);

alter table i_obrf_015 add
valor_pis_xml number(15,2);

alter table i_obrf_015 add
perc_cofins_xml number(6,2);

alter table i_obrf_015 add
cst_cofins_xml number(2);

alter table i_obrf_015 add
valor_cofins_xml number(15,2);

alter table i_obrf_015 add
perc_ipi_xml number(9,2);

alter table i_obrf_015 add
cst_ipi_xml number(2);

alter table i_obrf_015 add
base_ipi_xml number(15,2);

alter table i_obrf_015 add
valor_ipi_xml number(15,2);

exec inter_pr_recompile;
