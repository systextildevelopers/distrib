ALTER TABLE PCPC_045 ADD (
  NUMERO_SOLICITACAO NUMBER(9),
  NUMERO_ORDEM NUMBER(6));

ALTER TABLE PCPC_045 MODIFY (
  NUMERO_SOLICITACAO DEFAULT 0,
  NUMERO_ORDEM DEFAULT 0);

COMMENT ON COLUMN PCPC_045.NUMERO_SOLICITACAO IS 'Numero solicitacao que é gerado ao criar uma solicitacao de conserto';

COMMENT ON COLUMN PCPC_045.NUMERO_ORDEM IS 'Numero ordem que é gerado ao criar uma ordem de serviço';

exec inter_pr_recompile;

/
