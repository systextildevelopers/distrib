-- Add/modify columns 
alter table BASI_050 add (AGRUP_TINGIMENTO   NUMBER(3)  default 0);

-- Add comments to the columns 
comment on column BASI_050.AGRUP_TINGIMENTO
  is 'Agrupamento de itens para tingimento.';

exec inter_pr_recompile;
/

declare
cursor basi_050_c is
   select f.rowid from basi_050 f
                  where AGRUP_TINGIMENTO is null;

begin
   for reg_basi_050_c in basi_050_c
   loop
      update basi_050
      set AGRUP_TINGIMENTO   = 0
      where AGRUP_TINGIMENTO is null
        and basi_050.rowid = reg_basi_050_c.rowid; 
      commit;
   end loop; 

   commit;
end;
/

exec inter_pr_recompile;
/
