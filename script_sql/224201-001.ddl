INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('fiscal.tipoReducaoBasePisCofins', 1, 'lb48516', ' ', null, 0, null, null);

COMMIT;

declare 

tmpInt number;
valStr empr_008.val_str%type;
valInt empr_008.val_int%type;

cursor parametro_c is select codigo_empresa from fatu_500;

begin
  for reg_parametro in parametro_c
  loop
    
    select count(*) into tmpInt
    from empr_008
    where empr_008.codigo_empresa = reg_parametro.codigo_empresa
      and empr_008.param = 'fiscal.redusIcmsBasePisCofins';
    if(tmpInt = 0)
    then
      valInt := 0;
    else
      select empr_008.val_str into valStr
      from empr_008
      where empr_008.codigo_empresa = reg_parametro.codigo_empresa
        and empr_008.param = 'fiscal.redusIcmsBasePisCofins';
      if valStr = 'S'
      then
        valInt := 1;
      else 
        valInt := 0;
      end if;
      
    end if;
    
    select count(*) into tmpInt
    from empr_008
    where empr_008.codigo_empresa = reg_parametro.codigo_empresa
      and empr_008.param = 'fiscal.tipoReducaoBasePisCofins';
    if(tmpInt = 0)
    then
      begin
        insert into empr_008 ( codigo_empresa, param, val_int) 
            values (reg_parametro.codigo_empresa, 'fiscal.tipoReducaoBasePisCofins', valInt);
      end;
    end if;
    
  end loop;
  commit;
end;
