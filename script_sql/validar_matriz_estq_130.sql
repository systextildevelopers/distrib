CREATE OR REPLACE FUNCTION validar_matriz_estq_130(p_de IN NUMBER, p_ate IN NUMBER)
  RETURN BOOLEAN
IS
  v_count NUMBER;
BEGIN
  
  SELECT COUNT(*)
  INTO v_count
  FROM ESTQ_130
  WHERE (DE < p_de AND ATE > p_de)
     OR (DE < p_ate AND ATE > p_ate)
     OR (DE = p_de AND ATE = p_ate);

  RETURN (v_count = 0);
END;
