create table basi_242 (
ncm      varchar2(15) default '',
uf       varchar2(2)  default '',
perc_fcp number(15,2) default 0.00
);

alter table basi_242
	add constraint PK_BASI_242 primary key (ncm,uf);

/
exec inter_pr_recompile;
