
  CREATE OR REPLACE FUNCTION "INTER_FN_FIOS_PRODUCAO_ANUAL" 
        (fano_fioprod varchar2)
RETURN number
IS
   ftot_qtde number(15,4);
begin
   select nvl(sum(estq_060.peso_liquido),0) into ftot_qtde from estq_060, estq_005
   where estq_060.transacao_ent                 = estq_005.codigo_transacao
     and to_char(estq_060.data_producao,'YYYY') = fano_fioprod
     and estq_005.tipo_transacao                = 'P';

   return(ftot_qtde);
end inter_fn_fios_producao_anual;


 

/

exec inter_pr_recompile;

