alter table empr_002
add ind_usa_cc_dre varchar2(1) default 'N';

comment on column empr_002.ind_usa_cc_dre is 'Indica para o DRE se usa centro de custo';

exec inter_pr_recompile;
/
