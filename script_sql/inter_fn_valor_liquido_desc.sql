
  CREATE OR REPLACE FUNCTION "INTER_FN_VALOR_LIQUIDO_DESC" (p_valor_bruto in number, p_perc_desc in number)
return number 
is
   v_valor_liquido number;
   
begin
  
  v_valor_liquido := p_valor_bruto - (p_valor_bruto * p_perc_desc / 100.0);
  
  return(v_valor_liquido);
end inter_fn_valor_liquido_desc;

 

/

exec inter_pr_recompile;

