insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('basi_f524', 'Cadastro Linha',0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'basi_f524', ' ' ,1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Cadastro Linha'
where hdoc_036.codigo_programa = 'basi_f524'
  and hdoc_036.locale          = 'es_ES';

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('basi_f523', 'Cadastro Sub Linha',0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'basi_f523', ' ' ,1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Cadastro Sub Linha'
where hdoc_036.codigo_programa = 'basi_f523'
  and hdoc_036.locale          = 'es_ES';
commit;
