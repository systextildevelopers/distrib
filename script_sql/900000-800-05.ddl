alter table pedi_080 
add conf_almox_imp_xml number(1);

alter table pedi_080 
modify conf_almox_imp_xml default 0;

declare
nro_registro number;
begin
  nro_registro := 0;

  for reg in (select rowid
              from pedi_080
              where conf_almox_imp_xml is null)
  loop
    update pedi_080
      set conf_almox_imp_xml = 0
    where rowid = reg.rowid;

    nro_registro := nro_registro + 1;

    if nro_registro > 1000
    then
       nro_registro := 0;
       commit;
    end if;
  end loop;

  commit;

end;

/

exec inter_pr_recompile;
