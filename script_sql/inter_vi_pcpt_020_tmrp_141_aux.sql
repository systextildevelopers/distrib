create or replace view inter_vi_pcpt_020_tmrp_141_aux as 
  select pcpt_020.codigo_rolo,
       pcpt_020.panoacab_nivel99   nivel_rolo,
       pcpt_020.panoacab_grupo     grupo_rolo,
       pcpt_020.panoacab_subgrupo  sub_rolo,
       pcpt_020.panoacab_item      item_rolo,
       decode(tmrp_141.codigo_rolo,null,0,1) reservado,
       pcpt_020.largura,
       pcpt_020.gramatura,
       pcpt_020.mt_lineares_prod,
       pcpt_020.lote_acomp,
       pcpt_020.numero_lote,
       pcpt_020.codigo_deposito,
       pcpt_020.qtde_quilos_acab,
       pcpt_020.peso_bruto,
       pcpt_020.tara,
       pcpt_020.data_entrada,
       pcpt_020.rolo_estoque,
       pcpt_020.endereco_rolo,
       decode(tmrp_141.codigo_rolo,null,0,tmrp_141.ordem_producao) ordem_producao,
       basi_205.local_deposito codigo_empresa,
       pcpt_020.panoacab_nivel99,
       pcpt_020.panoacab_grupo,
       pcpt_020.panoacab_subgrupo,
       pcpt_020.panoacab_item,
       decode(tmrp_141.codigo_rolo,null,pcpt_020.qtde_quilos_acab,tmrp_141.qtde_reservada) qtde_rolo,
       decode(tmrp_141.codigo_rolo,null,0.000,tmrp_141.qtde_reservada) qtde_reservada,
       decode(tmrp_141.codigo_rolo,null,1,tmrp_141.sit_reserva) situacao_reserva,
       pcpt_020.qtde_quilos_acab - (select nvl(sum(t.qtde_reservada),0)
                                      from tmrp_141 t
                                     where t.codigo_rolo = pcpt_020.codigo_rolo
                                       and t.tipo_reserva          = 4
                                       and t.area_producao_reserva = 1) as qtde_saldo_rolo,
       1 tipo_rolo,
       pcpt_020.ordem_producao ordem_rolo,
       pcpt_021.cod_nuance
from pcpt_020, tmrp_141, basi_205, pcpt_021
where pcpt_020.codigo_rolo     = tmrp_141.codigo_rolo (+)
  and pcpt_020.codigo_deposito = basi_205.codigo_deposito
  and pcpt_020.rolo_estoque in (1,3)
  and basi_205.considera_tmrp  = 1
  and not exists(select 1
                 from pcpt_025
                 where pcpt_025.codigo_rolo = pcpt_020.codigo_rolo)
  and pcpt_021.codigo_rolo    = pcpt_020.codigo_rolo
UNION ALL
select pcpt_020.codigo_rolo,
       pcpt_020.panoacab_nivel99   nivel_rolo,
       pcpt_020.panoacab_grupo     grupo_rolo,
       pcpt_020.panoacab_subgrupo  sub_rolo,
       pcpt_020.panoacab_item      item_rolo,
       decode(tmrp_141.codigo_rolo,null,0,1) reservado,
       pcpt_020.largura,
       pcpt_020.gramatura,
       pcpt_020.mt_lineares_prod,
       pcpt_020.lote_acomp,
       pcpt_020.numero_lote,
       pcpt_020.codigo_deposito,
       pcpt_020.qtde_quilos_acab,
       pcpt_020.peso_bruto,
       pcpt_020.tara,
       pcpt_020.data_entrada,
       pcpt_020.rolo_estoque,
       pcpt_020.endereco_rolo,
       0 ordem_producao,
       basi_205.local_deposito codigo_empresa,
       pcpt_020.panoacab_nivel99,
       pcpt_020.panoacab_grupo,
       pcpt_020.panoacab_subgrupo,
       pcpt_020.panoacab_item,
       decode(tmrp_141.codigo_rolo,null,pcpt_020.qtde_quilos_acab,tmrp_141.qtde_reservada) qtde_rolo,
       decode(tmrp_141.codigo_rolo,null,0.000,tmrp_141.qtde_reservada) qtde_reservada,
       decode(tmrp_141.codigo_rolo,null,1,tmrp_141.sit_reserva) situacao_reserva,
       pcpt_020.qtde_quilos_acab - (select nvl(sum(t.qtde_reservada),0)
                                      from tmrp_141 t
                                     where t.codigo_rolo = pcpt_020.codigo_rolo
                                       and t.tipo_reserva          = 4
                                       and t.area_producao_reserva = 1) as qtde_saldo_rolo,
       1 tipo_rolo,
       pcpt_020.ordem_producao ordem_rolo,
       pcpt_021.cod_nuance
from pcpt_020, tmrp_141, basi_205, pcpt_021
where pcpt_020.codigo_rolo     = tmrp_141.codigo_rolo (+)
  and pcpt_020.codigo_deposito = basi_205.codigo_deposito
  and pcpt_020.rolo_estoque in (1,3)
  and basi_205.considera_tmrp  = 1
  and pcpt_020.qtde_quilos_acab > (select sum(t.qtde_reservada)
                                     from tmrp_141 t
                                    where t.codigo_rolo = pcpt_020.codigo_rolo
                                      and t.tipo_reserva          = 4
                                      and t.area_producao_reserva = 1)
  and not exists(select 1
                 from pcpt_025
                 where pcpt_025.codigo_rolo = pcpt_020.codigo_rolo)
  and pcpt_021.codigo_rolo    = pcpt_020.codigo_rolo
UNION ALL
select pcpt_020.codigo_rolo,
       basi_020.basi030_nivel030 nivel_rolo,
       basi_020.grupo_agrupador  grupo_rolo,
       basi_020.sub_agrupador    sub_rolo,
       pcpt_020.panoacab_item    item_rolo,
       decode(tmrp_141.codigo_rolo,null,0,1) reservado,
       pcpt_020.largura,
       pcpt_020.gramatura,
       pcpt_020.mt_lineares_prod,
       pcpt_020.lote_acomp,
       pcpt_020.numero_lote,
       pcpt_020.codigo_deposito,
       pcpt_020.qtde_quilos_acab,
       pcpt_020.peso_bruto,
       pcpt_020.tara,
       pcpt_020.data_entrada,
       pcpt_020.rolo_estoque,
       pcpt_020.endereco_rolo,
       decode(tmrp_141.codigo_rolo,null,0,tmrp_141.ordem_producao) ordem_producao,
       basi_205.local_deposito codigo_empresa,
       pcpt_020.panoacab_nivel99,
       pcpt_020.panoacab_grupo,
       pcpt_020.panoacab_subgrupo,
       pcpt_020.panoacab_item,
       decode(tmrp_141.codigo_rolo,null,pcpt_020.qtde_quilos_acab,tmrp_141.qtde_reservada) qtde_rolo,
       decode(tmrp_141.codigo_rolo,null,0.000,tmrp_141.qtde_reservada) qtde_reservada,
       decode(tmrp_141.codigo_rolo,null,1,tmrp_141.sit_reserva) situacao_reserva,
       pcpt_020.qtde_quilos_acab - (select nvl(sum(t.qtde_reservada),0)
                                     from tmrp_141 t
                                    where t.codigo_rolo = pcpt_020.codigo_rolo
                                      and t.tipo_reserva          = 4
                                      and t.area_producao_reserva = 1) as qtde_saldo_rolo,
       2 tipo_rolo,
       pcpt_020.ordem_producao ordem_rolo,
       pcpt_021.cod_nuance
from basi_020, pcpt_020, tmrp_141, basi_205, pcpt_021
where pcpt_020.panoacab_nivel99  = basi_020.basi030_nivel030
  and pcpt_020.panoacab_grupo    = basi_020.basi030_referenc
  and pcpt_020.panoacab_subgrupo = basi_020.tamanho_ref
  and pcpt_020.codigo_rolo       = tmrp_141.codigo_rolo (+)
  and pcpt_020.codigo_deposito   = basi_205.codigo_deposito
  and pcpt_020.rolo_estoque in (1,3)
  and basi_205.considera_tmrp    = 1
  and not exists(select 1
                 from pcpt_025
                 where pcpt_025.codigo_rolo = pcpt_020.codigo_rolo)
  and pcpt_021.codigo_rolo       = pcpt_020.codigo_rolo
UNION ALL
select pcpt_020.codigo_rolo,
       basi_020.basi030_nivel030 nivel_rolo,
       basi_020.basi030_referenc grupo_rolo,
       basi_020.tamanho_ref      sub_rolo,
       pcpt_020.panoacab_item    item_rolo,
       decode(tmrp_141.codigo_rolo,null,0,1) reservado,
       pcpt_020.largura,
       pcpt_020.gramatura,
       pcpt_020.mt_lineares_prod,
       pcpt_020.lote_acomp,
       pcpt_020.numero_lote,
       pcpt_020.codigo_deposito,
       pcpt_020.qtde_quilos_acab,
       pcpt_020.peso_bruto,
       pcpt_020.tara,
       pcpt_020.data_entrada,
       pcpt_020.rolo_estoque,
       pcpt_020.endereco_rolo,
       decode(tmrp_141.codigo_rolo,null,0,tmrp_141.ordem_producao) ordem_producao,
       basi_205.local_deposito codigo_empresa,
       pcpt_020.panoacab_nivel99,
       pcpt_020.panoacab_grupo,
       pcpt_020.panoacab_subgrupo,
       pcpt_020.panoacab_item,
       decode(tmrp_141.codigo_rolo,null,pcpt_020.qtde_quilos_acab,tmrp_141.qtde_reservada) qtde_rolo,
       decode(tmrp_141.codigo_rolo,null,0.000,tmrp_141.qtde_reservada) qtde_reservada,
       decode(tmrp_141.codigo_rolo,null,1,tmrp_141.sit_reserva) situacao_reserva,
       pcpt_020.qtde_quilos_acab - (select nvl(sum(t.qtde_reservada),0)
                                     from tmrp_141 t
                                    where t.codigo_rolo = pcpt_020.codigo_rolo
                                      and t.tipo_reserva          = 4
                                      and t.area_producao_reserva = 1) as qtde_saldo_rolo,
       2 tipo_rolo,
       pcpt_020.ordem_producao ordem_rolo,
       pcpt_021.cod_nuance
from basi_020, pcpt_020, tmrp_141, basi_205, pcpt_021
where basi_020.grupo_agrupador  <> '00000'
  and pcpt_020.panoacab_nivel99  = basi_020.basi030_nivel030
  and pcpt_020.panoacab_grupo    = basi_020.grupo_agrupador
  and pcpt_020.panoacab_subgrupo = basi_020.sub_agrupador
  and pcpt_020.codigo_rolo       = tmrp_141.codigo_rolo (+)
  and pcpt_020.codigo_deposito   = basi_205.codigo_deposito
  and pcpt_020.rolo_estoque in (1,3)
  and basi_205.considera_tmrp    = 1
  and not exists(select 1
                 from pcpt_025
                 where pcpt_025.codigo_rolo = pcpt_020.codigo_rolo)
  and pcpt_021.codigo_rolo       = pcpt_020.codigo_rolo
UNION ALL
select pcpt_020.codigo_rolo,
       basi_020.basi030_nivel030 nivel_rolo,
       basi_020.basi030_referenc grupo_rolo,
       basi_020.tamanho_ref      sub_rolo,
       pcpt_020.panoacab_item    item_rolo,
       decode(tmrp_141.codigo_rolo,null,0,1) reservado,
       pcpt_020.largura,
       pcpt_020.gramatura,
       pcpt_020.mt_lineares_prod,
       pcpt_020.lote_acomp,
       pcpt_020.numero_lote,
       pcpt_020.codigo_deposito,
       pcpt_020.qtde_quilos_acab,
       pcpt_020.peso_bruto,
       pcpt_020.tara,
       pcpt_020.data_entrada,
       pcpt_020.rolo_estoque,
       pcpt_020.endereco_rolo,
       0 ordem_producao,
       basi_205.local_deposito codigo_empresa,
       pcpt_020.panoacab_nivel99,
       pcpt_020.panoacab_grupo,
       pcpt_020.panoacab_subgrupo,
       pcpt_020.panoacab_item,
       decode(tmrp_141.codigo_rolo,null,pcpt_020.qtde_quilos_acab,tmrp_141.qtde_reservada) qtde_rolo,
       decode(tmrp_141.codigo_rolo,null,0.000,tmrp_141.qtde_reservada) qtde_reservada,
       decode(tmrp_141.codigo_rolo,null,1,tmrp_141.sit_reserva) situacao_reserva,
       pcpt_020.qtde_quilos_acab - (select nvl(sum(t.qtde_reservada),0)
                                     from tmrp_141 t
                                    where t.codigo_rolo = pcpt_020.codigo_rolo
                                      and t.tipo_reserva          = 4
                                      and t.area_producao_reserva = 1) as qtde_saldo_rolo,
       2 tipo_rolo,
       pcpt_020.ordem_producao ordem_rolo,
       pcpt_021.cod_nuance
from basi_020, pcpt_020, tmrp_141, basi_205, pcpt_021
where basi_020.grupo_agrupador  <> '00000'
  and pcpt_020.panoacab_nivel99  = basi_020.basi030_nivel030
  and pcpt_020.panoacab_grupo    = basi_020.grupo_agrupador
  and pcpt_020.panoacab_subgrupo = basi_020.sub_agrupador
  and pcpt_020.codigo_rolo       = tmrp_141.codigo_rolo (+)
  and pcpt_020.codigo_deposito   = basi_205.codigo_deposito
  and pcpt_020.rolo_estoque in (1,3)
  and basi_205.considera_tmrp    = 1
  and not exists(select 1
                 from pcpt_025
                 where pcpt_025.codigo_rolo = pcpt_020.codigo_rolo)
  and pcpt_021.codigo_rolo       = pcpt_020.codigo_rolo
 UNION ALL
select pcpt_020.codigo_rolo,
       basi_020.basi030_nivel030 nivel_rolo,
       basi_020.grupo_agrupador  grupo_rolo,
       basi_020.sub_agrupador    sub_rolo,
       basi_010.item_agrupador    item_rolo,
       decode(tmrp_141.codigo_rolo,null,0,1) reservado,
       pcpt_020.largura,
       pcpt_020.gramatura,
       pcpt_020.mt_lineares_prod,
       pcpt_020.lote_acomp,
       pcpt_020.numero_lote,
       pcpt_020.codigo_deposito,
       pcpt_020.qtde_quilos_acab,
       pcpt_020.peso_bruto,
       pcpt_020.tara,
       pcpt_020.data_entrada,
       pcpt_020.rolo_estoque,
       pcpt_020.endereco_rolo,
       decode(tmrp_141.codigo_rolo,null,0,tmrp_141.ordem_producao) ordem_producao,
       basi_205.local_deposito codigo_empresa,
       pcpt_020.panoacab_nivel99,
       pcpt_020.panoacab_grupo,
       pcpt_020.panoacab_subgrupo,
       basi_010.item_estrutura,
       decode(tmrp_141.codigo_rolo,null,pcpt_020.qtde_quilos_acab,tmrp_141.qtde_reservada) qtde_rolo,
       decode(tmrp_141.codigo_rolo,null,0.000,tmrp_141.qtde_reservada) qtde_reservada,
       decode(tmrp_141.codigo_rolo,null,1,tmrp_141.sit_reserva) situacao_reserva,
       pcpt_020.qtde_quilos_acab - (select nvl(sum(t.qtde_reservada),0)
                                     from tmrp_141 t
                                    where t.codigo_rolo = pcpt_020.codigo_rolo
                                      and t.tipo_reserva          = 4
                                      and t.area_producao_reserva = 1) as qtde_saldo_rolo,
       2 tipo_rolo,
       pcpt_020.ordem_producao ordem_rolo,
       pcpt_021.cod_nuance
from basi_010, basi_020, pcpt_020, tmrp_141, basi_205, pcpt_021
where basi_020.grupo_agrupador  <> '00000'
  and basi_010.item_agrupador <> '000000'
  and basi_010.grupo_estrutura   = basi_020.grupo_agrupador
  and basi_010.subgru_estrutura  = basi_020.sub_agrupador
  and pcpt_020.panoacab_nivel99  = basi_020.basi030_nivel030
  and pcpt_020.panoacab_grupo    = basi_020.grupo_agrupador
  and pcpt_020.panoacab_subgrupo = basi_020.sub_agrupador
  and  pcpt_020.panoacab_item    = basi_010.item_estrutura
  and pcpt_020.panoacab_grupo    = basi_010.grupo_estrutura
  and pcpt_020.panoacab_subgrupo = basi_010.subgru_estrutura
  and pcpt_020.codigo_rolo       = tmrp_141.codigo_rolo (+)
  and pcpt_020.codigo_deposito   = basi_205.codigo_deposito
  and pcpt_020.rolo_estoque in (1,3)
  and basi_205.considera_tmrp    = 1
  and not exists(select 1
                 from pcpt_025
                 where pcpt_025.codigo_rolo = pcpt_020.codigo_rolo)
  and pcpt_021.codigo_rolo       = pcpt_020.codigo_rolo;
