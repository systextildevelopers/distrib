
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_028_HIST" 
   after insert or delete or update
       of empresa, CGC9, CGC4, CGC2, TRANSP9, TRANSP4, TRANSP2, COD_PORTADOR, DATA_CADASTRO, TRANSP_REDESP9, TRANSP_REDESP4, TRANSP_REDESP2
   on pedi_028
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_processo_systextil     hdoc_090.programa%type;

begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ws_processo_systextil := inter_fn_nome_programa(ws_sid); 
  
   if inserting
   then

      begin
         INSERT INTO PEDI_028_HIST(
            EMPRESA,             CGC9,
            CGC4,                CGC2,

            REPRESENTANTE,

            TRANSP9_OLD,         TRANSP9_NEW,
            TRANSP4_OLD,         TRANSP4_NEW,
            TRANSP2_OLD,         TRANSP2_NEW,

            COD_PORTADOR_OLD,    COD_PORTADOR_NEW,
            DATA_CADASTRO_OLD,   DATA_CADASTRO_NEW,

            TRANSP_REDESP9_OLD,  TRANSP_REDESP9_NEW,
            TRANSP_REDESP4_OLD,  TRANSP_REDESP4_NEW,
            TRANSP_REDESP2_OLD,  TRANSP_REDESP2_NEW,

            APLICACAO,           TIPO_OCORR,
            DATA_OCORR,          USUARIO_REDE,
            MAQUINA_REDE,        NOME_PROGRAMA,
            USUARIO_SYSTEXTIL
         ) VALUES (
            :new.empresa,        :new.cgc9,
            :new.cgc4,           :new.cgc2,

            :new.representante,

            0,                   :new.transp9,
            0,                   :new.transp4,
            0,                   :new.transp2,

            0,                   :new.cod_portador,
            null,                :new.data_cadastro,

            0,                   :new.transp_redesp9,
            0,                   :new.transp_redesp4,
            0,                   :new.transp_redesp2,

            ws_aplicativo,       'I',
            sysdate,             ws_usuario_rede,
            ws_maquina_rede,     ws_processo_systextil,
            ws_usuario_systextil

         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATEN��O! N�o inseriu {0}. Status: {1}.', 'PEDI_028_HIST(001-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   if updating
   then

      begin
         INSERT INTO PEDI_028_HIST(
            EMPRESA,             CGC9,
            CGC4,                CGC2,

            REPRESENTANTE,

            TRANSP9_OLD,         TRANSP9_NEW,
            TRANSP4_OLD,         TRANSP4_NEW,
            TRANSP2_OLD,         TRANSP2_NEW,

            COD_PORTADOR_OLD,    COD_PORTADOR_NEW,
            DATA_CADASTRO_OLD,   DATA_CADASTRO_NEW,

            TRANSP_REDESP9_OLD,  TRANSP_REDESP9_NEW,
            TRANSP_REDESP4_OLD,  TRANSP_REDESP4_NEW,
            TRANSP_REDESP2_OLD,  TRANSP_REDESP2_NEW,

            APLICACAO,           TIPO_OCORR,
            DATA_OCORR,          USUARIO_REDE,
            MAQUINA_REDE,        NOME_PROGRAMA,
            USUARIO_SYSTEXTIL
         ) VALUES (
            :new.empresa,        :new.cgc9,
            :new.cgc4,           :new.cgc2,

            :new.representante,

            :old.transp9,        :new.transp9,
            :old.transp4,        :new.transp4,
            :old.transp2,        :new.transp2,

            :old.cod_portador,   :new.cod_portador,
            :old.data_cadastro,  :new.data_cadastro,

            :old.transp_redesp9, :new.transp_redesp9,
            :old.transp_redesp4, :new.transp_redesp4,
            :old.transp_redesp2, :new.transp_redesp2,

            ws_aplicativo,       'A',
            sysdate,             ws_usuario_rede,
            ws_maquina_rede,     ws_processo_systextil,
            ws_usuario_systextil

         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATEN��O! N�o inseriu {0}. Status: {1}.', 'PEDI_028_HIST(001-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

   end if;

   if deleting
   then

      begin
         INSERT INTO PEDI_028_HIST(
            EMPRESA,             CGC9,
            CGC4,                CGC2,

            REPRESENTANTE,

            TRANSP9_OLD,         TRANSP9_NEW,
            TRANSP4_OLD,         TRANSP4_NEW,
            TRANSP2_OLD,         TRANSP2_NEW,

            COD_PORTADOR_OLD,    COD_PORTADOR_NEW,
            DATA_CADASTRO_OLD,   DATA_CADASTRO_NEW,

            TRANSP_REDESP9_OLD,  TRANSP_REDESP9_NEW,
            TRANSP_REDESP4_OLD,  TRANSP_REDESP4_NEW,
            TRANSP_REDESP2_OLD,  TRANSP_REDESP2_NEW,

            APLICACAO,           TIPO_OCORR,
            DATA_OCORR,          USUARIO_REDE,
            MAQUINA_REDE,        NOME_PROGRAMA,
            USUARIO_SYSTEXTIL
         ) VALUES (
            :old.empresa,        :old.cgc9,
            :old.cgc4,           :old.cgc2,

            :old.representante,

            :old.transp9,        0,
            :old.transp4,        0,
            :old.transp2,        0,

            :old.cod_portador,   0,
            :old.data_cadastro,  null,

            :old.transp_redesp9, 0,
            :old.transp_redesp4, 0,
            :old.transp_redesp2, 0,

            ws_aplicativo,       'D',
            sysdate,             ws_usuario_rede,
            ws_maquina_rede,     ws_processo_systextil,
            ws_usuario_systextil

         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATEN��O! N�o inseriu {0}. Status: {1}.', 'PEDI_028_HIST(001-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

   end if;
end inter_tr_pedi_028_hist;
-- ALTER TRIGGER "INTER_TR_PEDI_028_HIST" ENABLE
 

/

exec inter_pr_recompile;

