
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_086_LOG" 
   after insert or delete or update
        of sit_fechamento
   on obrf_086
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_usuario_systextil      varchar2(250);
   ws_empresa                number(3);
   ws_locale_usuario         varchar2(5);

begin
	-- Dados do usuario logado
    inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                            ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

	insert into obrf_086_log (
       data_ocorrencia,            /*1*/
       maquina_rede,               /*2*/
       usuario_rede,               /*3*/
       usuario_systextil,          /*4*/
       ord086_numord,              /*5*/
       ord086_seqord,              /*6*/
       seq_entrada,                /*7*/
       situacao_fechamento_old,    /*8*/
       situacao_fechamento_new,    /*9*/
       data_fechamento_old,        /*10*/
       data_fechamento_new         /*11*/
    )values(
       sysdate,                    /*1*/
	   ws_maquina_rede,            /*2*/
	   ws_usuario_rede,            /*3*/
	   ws_usuario_systextil,       /*4*/
       :new.ord086_numord,         /*5*/
	   :new.ord086_seqord,         /*6*/
	   :new.seq_entrada,           /*7*/
	   :old.sit_fechamento,        /*8*/
	   :new.sit_fechamento,        /*9*/
	   :old.data_fechamento,       /*10*/
	   :new.data_fechamento        /*11*/
    );

end inter_tr_obrf_086_log;

-- ALTER TRIGGER "INTER_TR_OBRF_086_LOG" ENABLE
 

/

exec inter_pr_recompile;

