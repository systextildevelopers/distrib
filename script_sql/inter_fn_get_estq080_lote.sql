create or replace FUNCTION "INTER_FN_GET_ESTQ080_LOTE" (v_nivel_estrutura in varchar2, v_grupo_estrutura in varchar2, 
                                                             v_sub_estrutura in varchar2,   v_item_estrutura in varchar2,
                                                             v_numero_lote in number)
	return boolean
is 
   loteBloqueado boolean;
   tmp_sql number;
begin
    loteBloqueado := false;
    tmp_sql := 0;

    if v_nivel_estrutura = '9' then
        loteBloqueado := true;

        begin
            select 1 
            into tmp_sql
            from estq_080 
            where estq_080.nivel_estrutura = v_nivel_estrutura
            and   estq_080.grupo_estrutura = v_grupo_estrutura
            and   estq_080.sub_estrutura   = v_sub_estrutura
            and   estq_080.item_estrutura  = v_item_estrutura
            and   estq_080.lote_produto    = v_numero_lote
            and   estq_080.situacao_lote   in (0,3);
        exception when OTHERS then
            loteBloqueado := false;
        end;
    else
        loteBloqueado := false;
    end if;
    

   return(loteBloqueado); 
end inter_fn_get_estq080_lote;


/

exec inter_pr_recompile;

