alter table pedi_010
add (solicitacao_emergencial number(1) default 0);

comment on column pedi_010.solicitacao_emergencial is 'identifica se a remessa eh, 0 - emergencial, 1 - nao emergencial';

exec inter_pr_recompile;
