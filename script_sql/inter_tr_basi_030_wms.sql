create or replace trigger inter_tr_basi_030_wms
before insert or
       update of nivel_estrutura,
                 referencia,
                 descr_referencia,
                 unidade_medida,
                 colecao,
                 tipo_tag_ean,
                 artigo_cotas,
                 linha_produto
on basi_030
for each row
declare
  Pragma Autonomous_Transaction;
  v_descr_colecao     varchar2(20);
  v_codigo_unico_sku  inte_wms_produtos.codigo_unico_sku   %type;
  v_utiliza_wms       number(1);
  v_descr_artigo_sku   inte_wms_produtos.descr_artigo_sku   %type;
  v_descr_linha_sku    inte_wms_produtos.descr_linha_sku    %type;
begin
   
   v_utiliza_wms := 0;
   
   begin
      select max(fatu_503.uti_integracao_wms)
      into v_utiliza_wms
      from fatu_503
      where fatu_503.uti_integracao_wms = 1;
   exception
     when no_data_found then
        v_utiliza_wms := 0;
   end;      
   
   if :new.nivel_estrutura = '1' and v_utiliza_wms = 1
   then
   
      /* Buscar descri��o da cole��o */
      begin
        select basi_140.descr_colecao
        into   v_descr_colecao
        from basi_140
        where basi_140.colecao = :new.colecao;
      exception
        when no_data_found then
          v_descr_colecao := '';
      end;
      
      /* Buscar descri��o do artigo de cota */
      begin
         select basi_295.descr_artigo 
         into v_descr_artigo_sku
         from basi_295 
         where basi_295.artigo_cotas = :new.artigo_cotas;
      exception
        when no_data_found then
          v_descr_artigo_sku := '';
      end;
                        
      /* Buscar descri��o da linha de produto */
      begin
         select basi_120.descricao_linha 
         into v_descr_linha_sku
         from basi_120 
         where basi_120.linha_produto = :new.linha_produto; 
      exception
        when no_data_found then
          v_descr_linha_sku := '';
      end;

      /* Buscar/explodir basi_020 */
      for f_tamanhos in (
        select basi_020.tamanho_ref, basi_020.peso_liquido, basi_023.cubagem,
               basi_220.descr_tamanho, basi_220.ordem_tamanho
        from basi_020, basi_023, basi_220
        where basi_020.basi030_nivel030 = basi_023.basi030_nivel030
          and basi_020.basi030_referenc = basi_023.basi030_referenc
          and basi_020.tamanho_ref      = basi_023.tamanho_ref
          and basi_020.tamanho_ref      = basi_220.tamanho_ref
          and basi_020.basi030_nivel030 = :new.nivel_estrutura
          and basi_020.basi030_referenc = :new.referencia)
      LOOP

         /* Buscar/Explodir basi_010 */
         for f_itens in (
           select basi_010.item_estrutura,
                  basi_010.codigo_barras,    basi_010.narrativa
           from basi_010
           where basi_010.nivel_estrutura  = :new.nivel_estrutura
             and basi_010.grupo_estrutura  = :new.referencia
             and basi_010.subgru_estrutura = f_tamanhos.tamanho_ref)
         LOOP

            v_codigo_unico_sku := :new.nivel_estrutura || '.' || :new.referencia || '.' || f_tamanhos.tamanho_ref || '.' || f_itens.item_estrutura;

            if (:new.ref_original = '00000')
            then
               /* Inserir na tabela de integra��o */
               begin
                  insert into inte_wms_produtos
                     (nivel_sku,
                      grupo_sku,
                      subgrupo_sku,
                      item_sku,
                      ean_sku,
                      descricao_sku,
                      descr_tam,
                      ordem_tam,
                      descr_grupo,
                      unidade_medida_sku,
                      peso_sku,
                      cubagem_sku,
                      colecao_sku,
                      desc_colecao_sku,
                      tipo_sku,
                      codigo_unico_sku,
                      artigo_cotas_sku,
                      descr_artigo_sku,
                      linha_produto_sku,
                      descr_linha_sku
                     )
                  values
                     (:new.nivel_estrutura,
                      :new.referencia,
                      f_tamanhos.tamanho_ref,
                      f_itens.item_estrutura,
                      f_itens.codigo_barras,
                      f_itens.narrativa,
                      f_tamanhos.descr_tamanho,
                      f_tamanhos.ordem_tamanho,
                      :new.descr_referencia,
                      :new.unidade_medida,
                      f_tamanhos.peso_liquido,
                      f_tamanhos.cubagem,
                      :new.colecao,
                      v_descr_colecao,
                      :new.tipo_tag_ean,
                      v_codigo_unico_sku,
                      :new.artigo_cotas,
                      v_descr_artigo_sku,
                      :new.linha_produto,
                      v_descr_linha_sku
                     );
               exception
                  when others then
                     raise_application_error(-20000, 'N�o inseriu na tabela INTE_WMS_PRODUTOS.' || Chr(10) || SQLERRM);
               end;
            end if;
         END LOOP;
      
      END LOOP; 
      
      COMMIT;

   end if;
   
end inter_tr_basi_030_wms;
/

execute inter_pr_recompile;
/* versao: 3 */


 exit;


 exit;


 exit;

 exit;
