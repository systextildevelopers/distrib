
  CREATE OR REPLACE PROCEDURE "INTER_PR_DESTINOS_PLANEJAMENTO" (p_ordem_planejamento in number,
                                                            p_ordem_producao     in number,
                                                            p_qtde_destino      in number,
                                                            p_nivel              in varchar2,
                                                            p_grupo              in varchar2,
                                                            p_subgrupo           in varchar2,
                                                            p_item               in varchar2,
                                                            p_alternativa        in number,
                                                            p_acao               in varchar2)
is

   v_pedido_venda   number;
   v_pedido_reserva number;
   cursor tmrp625 is select tmrp_625.ordem_planejamento,         tmrp_625.pedido_venda,
                            tmrp_625.pedido_reserva,             tmrp_625.qtde_reserva_planejada,
                            tmrp_625.nivel_produto_origem,       tmrp_625.grupo_produto_origem,
                            tmrp_625.subgrupo_produto_origem,    tmrp_625.item_produto_origem,
                            tmrp_625.alternativa_produto_origem, tmrp_625.seq_produto,
                            tmrp_625.seq_produto_origem
                     from tmrp_625
                     where tmrp_625.ordem_planejamento  = p_ordem_planejamento
                       and tmrp_625.nivel_produto       = p_nivel
                       and tmrp_625.grupo_produto       = p_grupo
                       and tmrp_625.subgrupo_produto    = p_subgrupo
                       and tmrp_625.item_produto        = p_item
                       and tmrp_625.alternativa_produto = p_alternativa
                     order by tmrp_625.ordem_planejamento,         tmrp_625.pedido_venda,
                              tmrp_625.pedido_reserva,             tmrp_625.qtde_reserva_planejada,
                              tmrp_625.nivel_produto_origem,       tmrp_625.grupo_produto_origem,
                              tmrp_625.subgrupo_produto_origem,    tmrp_625.item_produto_origem,
                              tmrp_625.alternativa_produto_origem, tmrp_625.seq_produto,
                              tmrp_625.seq_produto_origem;

   v_qtde_planejamento       number(9,2) := 0.00;
   v_existe_630              number(3);

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   for reg_625 in tmrp625
   loop
      begin
         insert into tmrp_615(
            ordem_prod,                               area_producao,
            nivel,                                    grupo,
            subgrupo,                                 item,
            alternativa,                              ordem_planejamento,
            pedido_venda,                             pedido_reserva,
            nome_programa,
            nr_solicitacao,                           tipo_registro,
            cgc_cliente9,
            seq_registro,                             sequencia,
            nivel_prod,                               grupo_prod,
            subgrupo_prod,                            item_prod,
            alternativa_prod)
         values
           (p_ordem_producao,                         decode(p_nivel,'2',1,'4',2,'7',4,'9',9),
            reg_625.nivel_produto_origem,             reg_625.grupo_produto_origem,
            reg_625.subgrupo_produto_origem,          reg_625.item_produto_origem,
            reg_625.alternativa_produto_origem,       p_ordem_planejamento,
            reg_625.pedido_venda,                     reg_625.pedido_reserva,
            'trigger_planejamento',
            888,                                      888,
            ws_sid,
            reg_625.seq_produto,                      reg_625.seq_produto_origem,
            p_nivel,                                  p_grupo,
            p_subgrupo,                               p_item,
            p_alternativa);
      exception when others then
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end loop;

   if p_acao = 'I' or p_acao = 'U'
   then
      begin
         select sum(tmrp_625.qtde_reserva_planejada)
         into v_qtde_planejamento
         from tmrp_625
         where tmrp_625.ordem_planejamento  = p_ordem_planejamento
           and tmrp_625.nivel_produto       = p_nivel
           and tmrp_625.grupo_produto       = p_grupo
           and tmrp_625.subgrupo_produto    = p_subgrupo
           and tmrp_625.item_produto        = p_item
           and tmrp_625.alternativa_produto = p_alternativa;
      exception
      when OTHERS then
         v_qtde_planejamento := 0.00;
      end;

      for reg_ordem_pedido in (select tmrp_625.ordem_planejamento,
                                      tmrp_625.pedido_venda,
                                      tmrp_625.pedido_reserva,
                                      nvl(sum(tmrp_625.qtde_reserva_planejada),0) qtde_reserva_planejada
                               from tmrp_625
                               where tmrp_625.ordem_planejamento  = p_ordem_planejamento
                                 and tmrp_625.nivel_produto       = p_nivel
                                 and tmrp_625.grupo_produto       = p_grupo
                                 and tmrp_625.subgrupo_produto    = p_subgrupo
                                 and tmrp_625.item_produto        = p_item
                                 and tmrp_625.alternativa_produto = p_alternativa
                               group by tmrp_625.ordem_planejamento, tmrp_625.pedido_venda, tmrp_625.pedido_reserva)
      loop
         begin
            select nvl(count(1),0)
            into v_existe_630
            from tmrp_630
            where tmrp_630.ordem_planejamento  = reg_ordem_pedido.ordem_planejamento
              and tmrp_630.pedido_venda        = reg_ordem_pedido.pedido_venda
              and tmrp_630.pedido_reserva      = reg_ordem_pedido.pedido_reserva
              and tmrp_630.area_producao       = decode(p_nivel,'2',1,'4',2,'7',4,'9',9)
              and tmrp_630.ordem_prod_compra   = p_ordem_producao
              and tmrp_630.nivel_produto       = p_nivel
              and tmrp_630.grupo_produto       = p_grupo
              and tmrp_630.subgrupo_produto    = p_subgrupo
              and tmrp_630.item_produto        = p_item
              and tmrp_630.alternativa_produto = p_alternativa;
         exception when OTHERS then
            v_existe_630 := 0;
         end;

         if v_existe_630 = 0
         then
            inter_pr_estrutura_plan(reg_ordem_pedido.ordem_planejamento,     reg_ordem_pedido.pedido_venda,
                                    reg_ordem_pedido.pedido_reserva,         p_nivel,
                                    p_grupo,                                 p_subgrupo,
                                    p_item,                                  p_alternativa,
                                    reg_ordem_pedido.qtde_reserva_planejada);

            begin
               INSERT INTO tmrp_630 (
                  ordem_planejamento,                  area_producao,
                  ordem_prod_compra,                   nivel_produto,
                  grupo_produto,                       subgrupo_produto,
                  item_produto,                        programado_comprado,
                  pedido_venda,                        pedido_reserva,
                  alternativa_produto
               )VALUES(
                  reg_ordem_pedido.ordem_planejamento, decode(p_nivel,'2',1,'4',2,'7',4,'9',9),
                  p_ordem_producao,                    p_nivel,
                  p_grupo,                             p_subgrupo,
                  p_item,                              (p_qtde_destino * ((reg_ordem_pedido.qtde_reserva_planejada * 100) / decode(v_qtde_planejamento,0,1,v_qtde_planejamento))) / 100,
                  reg_ordem_pedido.pedido_venda,       reg_ordem_pedido.pedido_reserva,
                  p_alternativa
               );
            exception
            when OTHERS then
               raise_application_error(-20000,'Nao incluiu tmrp_630 ' || sqlerrm);
            end;
         else
            begin
               UPDATE tmrp_630
               set   tmrp_630.programado_comprado = tmrp_630.programado_comprado + (p_qtde_destino * ((reg_ordem_pedido.qtde_reserva_planejada * 100) / decode(v_qtde_planejamento,0,1,v_qtde_planejamento))) / 100
               where tmrp_630.ordem_planejamento  = reg_ordem_pedido.ordem_planejamento
                 and tmrp_630.area_producao       = decode(p_nivel,'2',1,'4',2,'7',4,'9',9)
                 and tmrp_630.ordem_prod_compra   = p_ordem_producao
                 and tmrp_630.nivel_produto       = p_nivel
                 and tmrp_630.grupo_produto       = p_grupo
                 and tmrp_630.subgrupo_produto    = p_subgrupo
                 and tmrp_630.item_produto        = p_item
                 and tmrp_630.alternativa_produto = p_alternativa
                 and tmrp_630.pedido_venda        = reg_ordem_pedido.pedido_venda
                 and tmrp_630.pedido_reserva      = reg_ordem_pedido.pedido_reserva;
            exception
            when OTHERS then
               raise_application_error(-20000,'Nao incluiu tmrp_630 ' || sqlerrm);
            end;
         end if;
      end loop;
   elsif p_acao = 'D'
   then
   
--         raise_application_error(-20000,'v_area_producao   ' );
   
      begin
         DELETE tmrp_630
         where tmrp_630.ordem_planejamento  = p_ordem_planejamento
--           and tmrp_630.area_producao       = decode(p_nivel,'2',1,'4',2,'7',4,'9',9)
           and tmrp_630.ordem_prod_compra   = p_ordem_producao
           and tmrp_630.nivel_produto       = p_nivel
           and tmrp_630.grupo_produto       = p_grupo
           and tmrp_630.subgrupo_produto    = p_subgrupo
           and tmrp_630.item_produto        = p_item
           and tmrp_630.alternativa_produto = p_alternativa;
      exception  when OTHERS then
         raise_application_error(-20000,'Nao eliminou tmrp_630');
      end;

--         raise_application_error(-20000,sqlerrm);

   end if;

end inter_pr_destinos_planejamento;

 

/

exec inter_pr_recompile;

