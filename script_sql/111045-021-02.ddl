create table obrf_516_hist
( 
  TIPO_OCORR                 VARCHAR2(1)   default '',
  DATA_OCORR                 DATE 					 ,
  HORA_OCORR                 DATE 					 ,
  USUARIO_REDE               VARCHAR2(20)  default '',
  MAQUINA_REDE               VARCHAR2(40)  default '',
  APLICACAO                  VARCHAR2(20)  default '',
  USUARIO_SISTEMA            VARCHAR2(20)  default '',
  NOME_PROGRAMA              VARCHAR2(20)  default '',
  cod_empresa_OLD        	 number(3)     default 0,  
  cod_empresa_NEW        	 number(3)     default 0,  
  uf_empresa_OLD             varchar2(2)   default ' ',
  uf_empresa_NEW             varchar2(2)   default ' ',
  perc_interno_fci_OLD       number(8,5)   default 0.0,
  perc_interno_fci_NEW       number(8,5)   default 0.0,
  perc_interestadual_fci_OLD number(8,5)   default 0.0,
  perc_interestadual_fci_NEW number(8,5)   default 0.0);

exec inter_pr_recompile;
