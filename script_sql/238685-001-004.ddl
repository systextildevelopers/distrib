DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'data_prev_receb';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'data_prev_receb',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cli_ped_cgc_cli9';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cli_ped_cgc_cli9',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cli_ped_cgc_cli4';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cli_ped_cgc_cli4',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cli_ped_cgc_cli2';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cli_ped_cgc_cli2',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'classificacao_pedido';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'classificacao_pedido',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'perc_comis_vendedor';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'perc_comis_vendedor',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cod_ped_cliente';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cod_ped_cliente',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'res';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'res',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'perc_desc_duplic';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'perc_desc_duplic',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'encargos';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'encargos',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'id_pedido_forca_vendas';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'id_pedido_forca_vendas',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'nr_pedido_compra_cliente';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'nr_pedido_compra_cliente',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'seq_end_entrega';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'seq_end_entrega',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'seq_end_cobranca';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'seq_end_cobranca',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cod_catalogo';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cod_catalogo',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'tipo_frete';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'tipo_frete',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cod_via_transp';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cod_via_transp',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'trans_pv_forne9';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'trans_pv_forne9',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'trans_pv_forne4';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'trans_pv_forne4',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'trans_pv_forne2';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'trans_pv_forne2',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cod_forma_pagto';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cod_forma_pagto',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'trans_re_forne9';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'trans_re_forne9',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'trans_re_forne4';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'trans_re_forne4',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'trans_re_forne2';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'trans_re_forne2',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cod_banco';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cod_banco',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'natop_pv_nat_oper';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'natop_pv_nat_oper',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'natop_pv_est_oper';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'natop_pv_est_oper',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'numero_controle';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'numero_controle',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'cod_cancelamento';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'cod_cancelamento',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'status_pedido';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'status_pedido',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'controle';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'controle',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'status_comercial';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'status_comercial',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'num_nota_relacionada';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'num_nota_relacionada',           0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/

DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'pedi_f130';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'serie_nota_relacionada';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'serie_nota_relacionada',    0, 
            1,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/
