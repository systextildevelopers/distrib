
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_300" 
before insert or
       update
       of cod_tipo_volume,
          descricao,
          cod_integracao,
          uso_tipo_vol
on pcpc_300
for each row

begin

   if :new.cod_integracao > 0
   then

      insert into inte_wms_tipos_volumes (
         cod_tipo_volume_st,   cod_integracao_wms,
         desc_tipo_vol_st,     uso_tipo_vol,
         timestamp_aimportar
      )
      values (
         :new.cod_tipo_volume, :new.cod_integracao,
         :new.descricao,       :new.uso_tipo_vol,
         sysdate()
      );

   end if;

end inter_tr_pcpc_300;

-- ALTER TRIGGER "INTER_TR_PCPC_300" ENABLE
 

/

exec inter_pr_recompile;

