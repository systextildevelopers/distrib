CREATE OR REPLACE VIEW "INTER_VI_PEDI_100_SUGESTAO" ("TECIDO_PECA", "PEDIDO_VENDA", "CODIGO_EMPRESA", "DATA_ENTR_VENDA", "QTDE_TOTAL_PEDI", "QTDE_SALDO_PEDI", "VALOR_SALDO_PEDI", "NUMERO_CONTROLE", "SUGESTAO_IMPRESSA", "STATUS_COMERCIAL", "PRIORIDADE", "LIBERADO_COLETAR", "LIBERADO_FATURAR", "OBSERVACAO", "CLI_PED_CGC_CLI9", "CLI_PED_CGC_CLI4", "CLI_PED_CGC_CLI2", "DESCONTO1", "DESCONTO2", "DESCONTO3", "LIBERADO_CONJUNTO", "NUM_SUGESTAO", "QTDE_PC_EMB_SUG_100", "QTDE_EMB_SUG_100", "QTDE_COL_PC_EMB_100", "QTDE_SUG_CONJUNTO", "VALOR_SUG_CONJUNTO", "QTDE_FATURADA", "QTDE_SUGER", "VALOR_SUGER", "QTDE_SALDO_SUGESTAO", "QTDE_COLETADA", "QTDE_A_COLETAR", "DESCR_CLIENTE", "BLOQUEIO_COMERCIAL_PEDIDO", "VEZES_FATURADO", "IND_COLETADO_CEM_PORCENTO", "IND_COL_EMBARQUE_CEM_PORCENTO", "QTDE_PC_SUG_100", "QTDE_COL_A_100") AS 
  select pedi_100.tecido_peca,
    pedi_100.pedido_venda,
    pedi_100.codigo_empresa,
    pedi_100.data_entr_venda,
    pedi_100.qtde_total_pedi,
    pedi_100.qtde_saldo_pedi,
    pedi_100.valor_saldo_pedi,
    pedi_100.numero_controle,
    pedi_100.sugestao_impressa,
    pedi_100.status_comercial,
    pedi_100.prioridade,
    pedi_100.liberado_coletar,
    pedi_100.liberado_faturar,
    pedi_100.observacao,
    pedi_100.cli_ped_cgc_cli9,
    pedi_100.cli_ped_cgc_cli4,
    pedi_100.cli_ped_cgc_cli2,
    pedi_100.desconto1,
    pedi_100.desconto2,
    pedi_100.desconto3,
    pedi_100.liberado_conjunto,
    nvl(fatu_780a.num_sugestao,0) num_sugestao,
    nvl(embarque.qtde_sugerida,0) qtde_pc_emb_sug_100,
    nvl(embarque.qtde_embarque,0) qtde_emb_sug_100,
    decode (nvl(embarque.qtde_sugerida,0),0,0,nvl(embarque.qtde_sugerida,0) - nvl(tb_emb_qtde_col_a_100.qtde_coletada,0)) qtde_col_pc_emb_100,

    nvl((select nvl(sum(decode(NVL(fatu_780.seq_conjunto,0), 0,fatu_780.qtde_sugerida, fatu_780.qtde_coleta)),0) qtde_sug_conjunto
         from fatu_780, pedi_110 p110ft785
         where fatu_780.pedido = pedi_100.pedido_venda
           and fatu_780.pedido = p110ft785.pedido_venda
           and fatu_780.sequencia = p110ft785.seq_item_pedido
         group by fatu_780.pedido),0) qtde_sug_conjunto,

         nvl((select nvl(sum(decode(NVL(fatu_780.seq_conjunto,0), 0, fatu_780.qtde_sugerida, fatu_780.qtde_coleta) * inter_fn_calc_desconto_triplo((p110ft785.valor_unitario - ((p110ft785.valor_unitario * p110ft785.percentual_desc) / 100)), pedi_100.desconto1, pedi_100.desconto2, pedi_100.desconto3)),0) valor_sug_conjunto
         from fatu_780, pedi_110 p110ft785
         where fatu_780.pedido = pedi_100.pedido_venda
           and fatu_780.pedido = p110ft785.pedido_venda
           and fatu_780.sequencia = p110ft785.seq_item_pedido
         group by fatu_780.pedido),0) valor_sug_conjunto,

    pedi_110b.qtde_faturada,

    pedi_110a.qtde_suger,

      inter_fn_calc_desconto_triplo(pedi_110a.valor_suger, pedi_100.desconto1, pedi_100.desconto2, pedi_100.desconto3) valor_suger,

    nvl(pedi_100.qtde_saldo_pedi,0) - pedi_110a.qtde_sugestao qtde_saldo_sugestao,

    nvl(pcpc_320a.qtde_coletada,0) qtde_coletada,

    (pedi_110a.qtde_suger) - nvl(pcpc_320a.qtde_coletada,0) qtde_a_coletar,
    pedi_010.nome_cliente descr_cliente,

    nvl(pedi_135a.bloqueio_comercial_pedido,0) bloqueio_comercial_pedido,
   (select count(distinct to_char(fatu_060.ch_it_nf_num_nfis) || fatu_060.ch_it_nf_ser_nfis)
    from fatu_060
    where fatu_060.ch_it_nf_cd_empr = pedi_100.codigo_empresa
     and fatu_060.pedido_venda   = pedi_100.pedido_venda )vezes_faturado,

    pedi_100.ind_coletado_cem_porcento,
    pedi_100.ind_col_embarque_cem_porcento,
    nvl(pedi_sug_100.qtde_pc_sug_100,0) qtde_pc_sug_100,

    nvl(pedi_sug_100.qtde_pc_sug_100,0) - nvl(tb_qtde_col_a_100.qtde_coletada,0) qtde_col_a_100

from pedi_100,
     pedi_010,
     inter_vi_grupo_emb_ped_sug_100 embarque,
  (select nvl(max(fatu_780.nr_sugestao),0) num_sugestao,
      fatu_780.pedido
   from fatu_780
   group by fatu_780.pedido) fatu_780a,

  (select nvl(sum(pedi_110.qtde_sugerida),0) qtde_suger,
      sum(pedi_110.qtde_sugerida * (pedi_110.valor_unitario - ((pedi_110.valor_unitario * pedi_110.percentual_desc) / 100))) valor_suger,
      nvl(sum(pedi_110.qtde_sugerida),0) qtde_sugestao,
      pedi_110.pedido_venda
   from pedi_110
   where pedi_110.qtde_sugerida > 0.00
   group by pedi_110.pedido_venda) pedi_110a,

  --pedi_sug_100
  (select sum(qtde_pc_sug_100) qtde_pc_sug_100, pedido_venda from
      (select nvl(sum(p.qtde_sugerida),0) qtde_pc_sug_100, p.pedido_venda  from pedi_110 p
        where p.cod_cancelamento = 0
        group by p.cd_it_pe_grupo, p.pedido_venda
        having sum(p.qtde_pedida - p.qtde_faturada) = sum(p.qtde_sugerida)
        and sum(p.qtde_pedida - p.qtde_faturada) > 0)
  group by pedido_venda  ) pedi_sug_100,

  (select nvl(sum(pedi_110.qtde_faturada),0) qtde_faturada,
      pedi_110.pedido_venda
   from pedi_110
   group by pedi_110.pedido_venda) pedi_110b,

  (select nvl(sum(pcpc_325.qtde_pecas_real),0) qtde_coletada,
      pcpc_320.pedido_venda
   from pcpc_320, pcpc_325
   where pcpc_325.numero_volume  = pcpc_320.numero_volume
    and pcpc_320.situacao_volume not in (2,5)
    and not exists (select 1 from fatu_050
                    where pcpc_320.nota_fiscal = fatu_050.num_nota_fiscal
                      and fatu_050.situacao_nfisc = 4)
   group by pcpc_320.pedido_venda) pcpc_320a,

  (select max(pedi_135.codigo_situacao) bloqueio_comercial_pedido,
       pedi_135.pedido_venda
   from  pedi_135
   where  pedi_135.data_liberacao is null
   group by pedi_135.pedido_venda ) pedi_135a,

  (select nvl(sum(pcpc_325.qtde_pecas_real),0) qtde_coletada, pcpc_320.pedido_venda
     from pcpc_320, pcpc_325,

       (select nvl(sum(p.qtde_sugerida),0) qtde_pc_sug_100, p.cd_it_pe_grupo, p.pedido_venda  from pedi_110 p
          where p.cod_cancelamento = 0
          group by p.cd_it_pe_grupo, p.pedido_venda
          having sum(p.qtde_pedida - p.qtde_faturada) = sum(p.qtde_sugerida)
          and sum(p.qtde_pedida - p.qtde_faturada) > 0) ref_sug_pedido

     where pcpc_325.numero_volume   = pcpc_320.numero_volume
       and pcpc_320.situacao_volume not in (2,5)
       and pcpc_320.pedido_venda    = ref_sug_pedido.pedido_venda
       and pcpc_325.grupo           = ref_sug_pedido.cd_it_pe_grupo
     group by pcpc_320.pedido_venda) tb_qtde_col_a_100,

  (select nvl(sum(pcpc_325.qtde_pecas_real),0) qtde_coletada, pcpc_320.pedido_venda
     from pcpc_320, pcpc_325, basi_590, inter_vi_grupo_emb_cod_sug_100
     where pcpc_325.numero_volume   = pcpc_320.numero_volume
       and pcpc_320.situacao_volume not in (2,5)
       and basi_590.nivel = pcpc_325.nivel
       and basi_590.grupo = pcpc_325.grupo
       and basi_590.subgrupo = pcpc_325.sub
       and basi_590.item = pcpc_325.item
       and inter_vi_grupo_emb_cod_sug_100.pedido_venda = pcpc_320.pedido_venda
       and inter_vi_grupo_emb_cod_sug_100.grupo_embarque = basi_590.grupo_embarque
     group by pcpc_320.pedido_venda) tb_emb_qtde_col_a_100

where pedi_100.cli_ped_cgc_cli9 = pedi_010.cgc_9
 and pedi_100.cli_ped_cgc_cli4 = pedi_010.cgc_4
 and pedi_100.cli_ped_cgc_cli2 = pedi_010.cgc_2
 and pedi_100.pedido_venda   = embarque.pedido_venda (+)
 and pedi_100.pedido_venda   = fatu_780a.pedido (+)
 and pedi_100.pedido_venda   = pedi_135a.pedido_venda (+)
 and pedi_100.pedido_venda   = pedi_110a.pedido_venda (+)
 and pedi_100.pedido_venda   = pedi_110b.pedido_venda (+)
 and pedi_100.pedido_venda   = pcpc_320a.pedido_venda (+)
 and pedi_100.pedido_venda   = pedi_sug_100.pedido_venda (+)
 and pedi_100.pedido_venda   = tb_qtde_col_a_100.pedido_venda (+)
 and pedi_100.pedido_venda   = tb_emb_qtde_col_a_100.pedido_venda (+);
