CREATE OR REPLACE TRIGGER inter_tr_pcpb_810
  BEFORE INSERT ON pcpb_810
  FOR EACH ROW

declare
   ob_gerada number; 

begin
   
   begin
      select gerar_pcpb_812_id.nextval
      into   :new.id
      from   dual;
   end;
   
   begin
      inter_pr_gerar_ob(
      :new.id,                    :new.GRUPO_MAQ,
      :new.SUBGRUPO_MAQ,          :new.NUMERO_MAQ, 
      :new.OBSERVACAO1,           :new.OBSERVACAO2,
      :new.NIVEL_PRODUTO,         :new.GRUPO_PRODUTO,
      :new.SUBGRUPO_PRODUTO,      :new.ITEM_PRODUTO,
      :new.ALTERNATIVA_PRODUTO,   :new.QTDE_QUILOS,
      :new.DEPOSITO_DESTINO,      ob_gerada);
   end;
   
   :new.ob_gerada := ob_gerada;
   
end inter_tr_pcpb_810;
/
