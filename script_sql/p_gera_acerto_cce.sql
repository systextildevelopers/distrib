create or replace PROCEDURE "P_GERA_ACERTO_CCE" (p_cd_cce        in  number,
                                              p_msg_erro      out varchar2 ) is

  v_ERRO EXCEPTION;

begin

   for a_cce in(

   select obrf_150.cod_status,
          obrf_150.msg_status,
          obrf_150.nr_protocolo,
          obrf_122.cd_empresa,
          obrf_122.cd_numero_nota,
          obrf_122.cd_serie,
          obrf_122.cd_forn_9,
          obrf_122.cd_forn_4,
          obrf_122.cd_forn_2,
          obrf_122.cd_tipo_e_s
   from   obrf_122,                  obrf_150
   where  obrf_150.id_lote           = p_cd_cce
     and  obrf_122.cd_cce            = obrf_150.id_lote
     and  obrf_150.tipo_documento    = 6
)
   LOOP
   BEGIN


     /*Se o retorno do envio da CC-e for positivo*/
     if a_cce.cod_status = '135'
     then
        /*Se for CC-e de uma nota de entrada*/
        if a_cce.cd_tipo_e_s = 'E'
        then
           BEGIN
           /*Volta a situação da nota fiscal para 0 (Não alterada)*/
           update obrf_010
           set    obrf_010.st_flag_cce = 0
           where  obrf_010.documento      = a_cce.cd_numero_nota
             and  obrf_010.serie          = a_cce.cd_serie
             and  obrf_010.cgc_cli_for_9  = a_cce.cd_forn_9
             and  obrf_010.cgc_cli_for_4  = a_cce.cd_forn_4
             and  obrf_010.cgc_cli_for_2  = a_cce.cd_forn_2;
           EXCEPTION
           WHEN OTHERS THEN
              p_msg_erro := 'Não atualizou dados na tabela obrf_010' || Chr(10) || SQLERRM;
              RAISE v_ERRO;
           END;
        end if;
        /*Se for CC-e de uma nota de saida*/
        if a_cce.cd_tipo_e_s = 'S'
        then
           BEGIN
           update fatu_050
           set    fatu_050.st_flag_cce = 0
           where  fatu_050.codigo_empresa  = a_cce.cd_empresa
             and  fatu_050.num_nota_fiscal = a_cce.cd_numero_nota
             and  fatu_050.serie_nota_fisc = a_cce.cd_serie;
           EXCEPTION
           WHEN OTHERS THEN
              p_msg_erro := 'Não atualizou dados na tabela fatu_050' || Chr(10) || SQLERRM;
              RAISE v_ERRO;
           END;
        end if;
        /*Atualiza dados de retorno da tabela da CC-e */
        BEGIN
        update obrf_122
        set    obrf_122.cd_status      = a_cce.cod_status,
               obrf_122.ds_msg_status  = a_cce.msg_status,
               obrf_122.cd_nr_protocolo = a_cce.nr_protocolo,
               obrf_122.st_enviado_s_n = 'S'
        where  obrf_122.cd_cce         = p_cd_cce;
        EXCEPTION
        WHEN OTHERS THEN
           p_msg_erro := 'Não atualizou dados na tabela obrf_122' || Chr(10) || SQLERRM;
           RAISE v_ERRO;
        END;
     end if;
     /*Se o retorno do envio for negativo*/
     if a_cce.cod_status <> '135'
     then
        BEGIN
        update obrf_122
        set    obrf_122.cd_status      = decode(a_cce.cod_status,537,135,573,135,a_cce.cod_status),
               obrf_122.ds_msg_status  = a_cce.msg_status,
               obrf_122.cd_nr_protocolo = a_cce.nr_protocolo,
               obrf_122.st_enviado_s_n = decode(a_cce.cod_status,537,'S',573,'S','N')
        where  obrf_122.cd_cce         = p_cd_cce;
        EXCEPTION
        WHEN OTHERS THEN
           p_msg_erro := 'Não atualizou dados na tabela obrf_122' || Chr(10) || SQLERRM;
           RAISE v_ERRO;
        END;
     end if;
     /*Deleta todos Registros da obrf_150 ligados a integração da CC-e*/

     BEGIN
        delete from obrf_150
        where obrf_150.id_lote   = p_cd_cce
          and obrf_150.tipo_documento = 6;

      EXCEPTION
        WHEN OTHERS THEN
        p_msg_erro := 'Não apagou os dados na tabela obrf_150' || Chr(10) || SQLERRM;
        RAISE v_erro;
      END;
   end;
   END LOOP;
end p_gera_acerto_cce;

 

/

exec inter_pr_recompile;

