CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_015_HIST"
AFTER INSERT OR
      DELETE OR
      UPDATE of seq_operacao,  codigo_operacao,  motivo_rejeicao,  data_digitacao, hora_digitacao,
                grupo_maquina, subgrupo_maquina, numero_maquina,   codigo_estagio, operador_inicio,
                data_inicio,   hora_inicio,      operador_termino, data_termino,   hora_termino,
                cod_estagio_agrupador, seq_operacao_agrupador
      on pcpb_015
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_area_producao          number(1);
   ws_ordem_producao         number(9);
   ws_periodo_producao       number(4);
   ws_ordem_confeccao        number(2);
   ws_tipo_historico         number(1);

   ws_descricao_historico    varchar2(4000);
   ws_tipo_ocorr             varchar2(1);

   ws_historico_ordens       number(1);

begin
   begin
      select empr_002.historico_ordens
      into ws_historico_ordens
      from empr_002;
   end;

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- HISTORICO DE BENEFICIAMENTO ATIVO
   if ws_historico_ordens = 1
   then
      if INSERTING then
         ws_area_producao         := 2;
         ws_ordem_producao        := :new.ordem_producao;
         ws_periodo_producao      := 0;
         ws_ordem_confeccao       := 0;
         ws_tipo_historico        := 1;
         ws_descricao_historico   := inter_fn_buscar_tag('lb34858#INCLUSAO OPERACAO (PCPB_015)',ws_locale_usuario,ws_usuario_systextil)
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb06610#SEQ.OPERACAO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.seq_operacao
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb02558#COD. OPERACAO:',ws_locale_usuario,ws_usuario_systextil)  || ' ' || :new.codigo_operacao
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb17425#CODIGO ESTAGIO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.codigo_estagio
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb02646#CODIGO MAQUINA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.grupo_maquina
                                                                                                                           || '.'
                                                                                                                           || :new.subgrupo_maquina
                                                                                                                           || '.'
                                                                                                                           || :new.numero_maquina
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb02908#DATA DIGITACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_digitacao || ' ' || to_char(:new.hora_digitacao,'HH24:MI')
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36654#DATA / HORA INICIO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_inicio || ' ' || to_char(:new.hora_inicio,'HH24:MI')
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36655#DATA / HORA TERMINO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_termino || ' ' || to_char(:new.hora_termino,'HH24:MI')
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb06072#MOTIVO REJEICAO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.motivo_rejeicao
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36656#OPERADOR INICIO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.operador_inicio
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36657#OPERADOR TERMINO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.operador_termino
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36658#MINUTOS UNITARIO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.minutos_unitario
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb10247#ORDEM REPROCESSO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.ordem_reprocesso
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36659#PRIORIDADE PRODUCAO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.prioridade_producao
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb50576#COD_ESTAGIO_AGRUPADOR: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cod_estagio_agrupador
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb50600#SEQ_OPERACAO_AGRUPADOR: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.seq_operacao_agrupador
                                   ;
         ws_tipo_ocorr            := 'I';

      elsif DELETING then
            ws_area_producao         := 2;
            ws_ordem_producao        := :old.ordem_producao;
            ws_periodo_producao      := 0;
            ws_ordem_confeccao       := 0;
            ws_tipo_historico        := 1;

            ws_descricao_historico   := inter_fn_buscar_tag('lb34859#EXCLUSAO OPERACAO (PCPB_015)',ws_locale_usuario,ws_usuario_systextil)
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb06610#SEQ.OPERACAO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.seq_operacao
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb02558#COD. OPERACAO:',ws_locale_usuario,ws_usuario_systextil)  || ' ' || :old.codigo_operacao
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb17425#CODIGO ESTAGIO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.codigo_estagio
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb02646#CODIGO MAQUINA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.grupo_maquina
                                                                                                                           || '.'
                                                                                                                           || :old.subgrupo_maquina
                                                                                                                           || '.'
                                                                                                                           || :old.numero_maquina
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb02908#DATA DIGITACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.data_digitacao || ' ' || to_char(:old.hora_digitacao,'HH24:MI')
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36654#DATA / HORA INICIO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.data_inicio || ' ' || to_char(:old.hora_inicio,'HH24:MI')
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36655#DATA / HORA TERMINO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.data_termino || ' ' || to_char(:old.hora_termino,'HH24:MI')
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb06072#MOTIVO REJEICAO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.motivo_rejeicao
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36656#OPERADOR INICIO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.operador_inicio
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36657#OPERADOR TERMINO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.operador_termino
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36658#MINUTOS UNITARIO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.minutos_unitario
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb10247#ORDEM REPROCESSO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.ordem_reprocesso
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36659#PRIORIDADE PRODUCAO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.prioridade_producao
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb50576#COD_ESTAGIO_AGRUPADOR: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.cod_estagio_agrupador
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb50600#SEQ_OPERACAO_AGRUPADOR: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.seq_operacao_agrupador
                                   ;

            ws_tipo_ocorr            := 'D';

      elsif UPDATING then
            ws_area_producao         := 2;
            ws_ordem_producao        := :old.ordem_producao;
            ws_periodo_producao      := 0;
            ws_ordem_confeccao       := 0;
            ws_tipo_historico        := 1;
            ws_descricao_historico   := inter_fn_buscar_tag('lb34860#ATUALIZACAO OPERACAO (PCPB_015)',ws_locale_usuario,ws_usuario_systextil)
                                      ||chr(10)
                                   ||inter_fn_buscar_tag('lb06610#SEQ.OPERACAO:',ws_locale_usuario,ws_usuario_systextil)   || ' '  || :old.seq_operacao
                                                                                                                           || '->' || :new.seq_operacao
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb02558#COD. OPERACAO:',ws_locale_usuario,ws_usuario_systextil)  || ' '  || :old.codigo_operacao
                                                                                                                           || '->' || :new.codigo_operacao
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb17425#CODIGO ESTAGIO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.codigo_estagio
                                                                                                                           || '->' || :new.codigo_estagio
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb02646#CODIGO MAQUINA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.grupo_maquina
                                                                                                                           || '.'
                                                                                                                           || :old.subgrupo_maquina
                                                                                                                           || '.'
                                                                                                                           || :old.numero_maquina
                                                                                                                           || '->' || :new.grupo_maquina
                                                                                                                           || '.'
                                                                                                                           || :new.subgrupo_maquina
                                                                                                                           || '.'
                                                                                                                           || :new.numero_maquina
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb02908#DATA DIGITACAO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.data_digitacao || ' ' || to_char(:old.hora_digitacao,'HH24:MI')
                                                                                                                            || '->' || :new.data_digitacao || ' ' || to_char(:new.hora_digitacao,'HH24:MI')
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36654#DATA / HORA INICIO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.data_inicio || ' ' || to_char(:old.hora_inicio,'HH24:MI')
                                                                                                                                 || '->' || :new.data_inicio || ' ' || to_char(:new.hora_inicio,'HH24:MI')
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36655#DATA / HORA TERMINO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.data_termino || ' ' || to_char(:old.hora_termino,'HH24:MI')
                                                                                                                                  || '->' || :new.data_termino || ' ' || to_char(:new.hora_termino,'HH24:MI')
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb06072#MOTIVO REJEICAO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.motivo_rejeicao
                                                                                                                              || '->' || :new.motivo_rejeicao
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36656#OPERADOR INICIO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.operador_inicio
                                                                                                                              || '->' || :new.operador_inicio
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36657#OPERADOR TERMINO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.operador_termino
                                                                                                                               || '->' || :new.operador_termino
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36658#MINUTOS UNITARIO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.minutos_unitario
                                                                                                                               || '->' || :new.minutos_unitario
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb10247#ORDEM REPROCESSO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.ordem_reprocesso
                                                                                                                               || '->' || :new.ordem_reprocesso
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb36659#PRIORIDADE PRODUCAO: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.prioridade_producao
                                                                                                                               || '->' || :new.prioridade_producao
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb50576#COD_ESTAGIO_AGRUPADOR: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.cod_estagio_agrupador
                                                                                                                                   || '->' || :new.cod_estagio_agrupador
                                   ||chr(10)
                                   || inter_fn_buscar_tag('lb50600#SEQ_OPERACAO_AGRUPADOR: ',ws_locale_usuario,ws_usuario_systextil) || ' ' || :old.seq_operacao_agrupador
                                                                                                                                    || '->' || :new.seq_operacao_agrupador
                                   ;
            ws_tipo_ocorr            := 'A';
      end if;

      INSERT INTO hist_010
        (area_producao,           ordem_producao,          periodo_producao,
         ordem_confeccao,         tipo_historico,          descricao_historico,
         tipo_ocorr,              data_ocorr,              hora_ocorr,
         usuario_rede,            maquina_rede,            aplicacao,
         usuario_sistema,         tipo_ordem)
      VALUES
        (ws_area_producao,        ws_ordem_producao,       ws_periodo_producao,
         ws_ordem_confeccao,      ws_tipo_historico,       ws_descricao_historico,
         ws_tipo_ocorr,           sysdate,                 sysdate,
         ws_usuario_rede,         ws_maquina_rede,         ws_aplicativo,
         ws_usuario_systextil,    0);

   end if;
end inter_tr_pcpb_015_hist;

-- ALTER TRIGGER "INTER_TR_PCPB_015_HIST" ENABLE
 

/

exec inter_pr_recompile;

