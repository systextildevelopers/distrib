alter table obrf_971 
add seq_nf_entr number(9) default 0;

alter table obrf_971 drop constraint PK_OBRF_971;

drop index PK_OBRF_971;

alter table obrf_971 add constraint PK_OBRF_971 primary key(NIVEL, GRUPO, SUBGRUPO, ITEM, LOTE, IND_REM_RET, NUM_NF_ENTR, SERIE_NF_ENTR, SEQ_NF_ENTR, CNPJ_9_NF_ENTR, CNPJ_4_NF_ENTR, CNPJ_2_NF_ENTR) NOVALIDATE;

/

