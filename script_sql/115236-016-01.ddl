alter table obrf_318 add (
 cod_matriz           NUMBER(3) default 0,
 cod_filial           NUMBER(3) default 0,
 ano                  NUMBER(4) default 0,
 mes                  NUMBER(2) default 0);
 
alter table OBRF_318
drop constraint UNIQ_OBRF_318_APURACAO unique;
 
alter table OBRF_318
add constraint UNIQ_OBRF_318_APURACAO unique (ID_OBRF_317_APURACAO, COD_MATRIZ, COD_FILIAL, ANO, MES, NUM_NOTA_FISCAL, SERIE_NOTA, CNPJ9, CNPJ4, CNPJ2, NIVEL, GRUPO, SUBGRUPO, ITEM);
