CREATE OR REPLACE TRIGGER "INTER_TR_BASI_050_2" 
before insert or
       delete or
       update of nivel_comp, grupo_comp,
                 sub_comp,   item_comp, consumo
on basi_050
for each row

declare
   v_ano_atual              number(4);
   v_nivel_param1           varchar2(1);
   v_nivel_param2           varchar2(1);
   v_nivel_param4           varchar2(1);
   v_nivel_param7           varchar2(1);
   v_nivel_param9           varchar2(1);
   v_alternatica_custos     number;
   v_tem_produto            number;
   v_produto                varchar2(18);
   v_mes                    number(2);

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

begin
  -- Dados do usuário logado
  inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                          ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

  -- busca ano atual
  select extract(year from sysdate)
  into   v_ano_atual
  from dual;

  -- verifica parametro se possui bloqueio de atualizacao por ninel
  begin
     select nvl(hdoc_001.campo_numerico01,0),      nvl(hdoc_001.campo_numerico02,0),
            nvl(hdoc_001.campo_numerico03,0),      nvl(hdoc_001.campo_numerico04,0),
            nvl(hdoc_001.campo_numerico05,0)
     into   v_nivel_param1,         v_nivel_param2,
            v_nivel_param4,         v_nivel_param7,
            v_nivel_param9
     from hdoc_001
     where tipo   = 84
     and   codigo = ws_empresa;
  exception when others then
     v_nivel_param1 := '0';
     v_nivel_param2 := '0';
     v_nivel_param4 := '0';
     v_nivel_param7 := '0';
     v_nivel_param9 := '0';
  end;

  if v_nivel_param1 = 1
  then
    v_nivel_param1 := '1';
  end if;

  if v_nivel_param2 = 1
  then
    v_nivel_param2 := '2';
  end if;

  if v_nivel_param4 = 1
  then
    v_nivel_param4 := '4';
  end if;

  if v_nivel_param7 = 1
  then
    v_nivel_param7 := '7';
  end if;

  if v_nivel_param9 = 1
  then
    v_nivel_param9 := '9';
  end if;

  if inserting or updating
  then
     begin
        select count(*)
        into v_alternatica_custos
        from basi_010 b
        where b.nivel_estrutura    = :new.nivel_item
        and   b.grupo_estrutura    = :new.grupo_item
        and  (b.subgru_estrutura   = :new.sub_item  or :new.sub_item  = '000')
        and  (b.item_estrutura     = :new.item_item or :new.item_item = '000000')
        and   b.alternativa_custos = :new.alternativa_item;
     exception when others then
        v_alternatica_custos := 0;
     end;

     if v_alternatica_custos > 0
     then
        if :new.nivel_item in (v_nivel_param1, v_nivel_param2, v_nivel_param4, v_nivel_param7, v_nivel_param9)
        then
           begin
              select count(*)
              into v_tem_produto
              from estq_300_estq_310 a, estq_005 b
              where extract(month from data_movimento)   = extract(month from current_date) - 1
              and   extract(year from data_movimento)    = v_ano_atual
              and   a.nivel_estrutura    = :new.nivel_item
              and   a.grupo_estrutura    = :new.grupo_item
              and  (a.subgrupo_estrutura = :new.sub_item  or :new.sub_item  = '000')
              and  (a.item_estrutura     = :new.item_item or :new.item_item = '000000')
              and   a.codigo_transacao   = b.codigo_transacao
              and   b.tipo_transacao     = 'P';
           exception when others then
              v_tem_produto := 0;
           end;

           if v_tem_produto > 0
           then
              v_produto := :new.nivel_item || '.' || :new.grupo_item || '.' || :new.sub_item || '.' || :new.item_item;
              v_mes := extract(month from current_date) - 1;
              raise_application_error (-20000, 'O Item ' || v_produto || ' teve movimentação no mês ' || '<' || v_mes || '/' || v_ano_atual ||
                                               '>' || ' e não pode ser atualizado agora pois está em processo de fechamento de custos.');
           end if;
        end if;
     end if;
  end if;

  if deleting
  then
     begin
        select count(*)
        into v_alternatica_custos
        from basi_010 b
        where b.nivel_estrutura    = :old.nivel_item
        and   b.grupo_estrutura    = :old.grupo_item
        and  (b.subgru_estrutura   = :old.sub_item  or :old.sub_item  = '000')
        and  (b.item_estrutura     = :old.item_item or :old.item_item = '000000')
        and   b.alternativa_custos = :old.alternativa_item;
     exception when others then
        v_alternatica_custos := 0;
     end;

     if v_alternatica_custos > 0
     then
        if :old.nivel_item in (v_nivel_param1, v_nivel_param2, v_nivel_param4, v_nivel_param7, v_nivel_param9)
        then
           begin
              select count(*)
              into v_tem_produto
              from estq_300_estq_310 a, estq_005 b
              where extract(month from data_movimento)   = extract(month from current_date) - 1
              and   extract(year from data_movimento)    = v_ano_atual
              and   a.nivel_estrutura    = :old.nivel_item
              and   a.grupo_estrutura    = :old.grupo_item
              and  (a.subgrupo_estrutura = :old.sub_item  or :old.sub_item  = '000')
              and  (a.item_estrutura     = :old.item_item or :old.item_item = '000000')
              and   a.codigo_transacao   = b.codigo_transacao
              and   b.tipo_transacao     = 'P';
           exception when others then
              v_tem_produto := 0;
           end;

           if v_tem_produto > 0
           then
              v_produto := :old.nivel_item || '.' || :old.grupo_item || '.' || :old.sub_item || '.' || :old.item_item;
              v_mes := extract(month from current_date) - 1;
              raise_application_error (-20000, 'O Item ' || v_produto || ' teve movimentação no mês ' || '<' || v_mes || '/' || v_ano_atual ||
                                               '>' || ' e não pode ser atualizado agora pois está em processo de fechamento de custos.');
           end if;
        end if;
     end if;
  end if;
end inter_tr_basi_050_2;
 

/

exec inter_pr_recompile;

