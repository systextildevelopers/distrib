CREATE OR REPLACE PROCEDURE "INTER_PR_GET_SALDO_MOEDA_CPAG"(
   p_empresa     in number, p_titulo      in number, p_parcela     in varchar2,
   p_tipo_titulo in number, p_cnpj9       in number, p_cnpj4       in number,
   p_cnpj2       in number, p_valor_moeda in number, p_valor_irrf  in number,
   p_valor_iss   in number, p_valor_inss  in number, p_cotacao     in number, 
   p_saldo_moeda out number)
is   
  cursor pagamentos is 
  select cpag_015.valor_juros, cpag_015.valor_descontos,
        cpag_015.valor_pago, cpag_015.valor_abatido,
        cpag_015.codigo_historico, cpag_015.data_pagamento,
        cpag_015.vlr_juros_moeda, cpag_015.vlr_descontos_moeda,
        cpag_015.valor_pago_moeda
  from cpag_015
  where cpag_015.dupl_for_nrduppag = p_titulo
    and cpag_015.dupl_for_no_parc  = p_parcela
    and cpag_015.dupl_for_for_cli9 = p_cnpj9
    and cpag_015.dupl_for_for_cli4 = p_cnpj4
    and cpag_015.dupl_for_for_cli2 = p_cnpj2
    and cpag_015.dupl_for_tipo_tit = p_tipo_titulo;

  v_sinal_titulo                   cont_010.sinal_titulo%type;
  v_valor_acrescimo_moeda          cpag_015.vlr_juros_moeda%type;
  v_valor_pago_moeda               cpag_015.valor_pago_moeda%type;
  v_valor_desconto_moeda           cpag_015.vlr_descontos_moeda%type;
  v_contr_irrf                     cpag_040.controle_irrf%type;
  v_sinal_irrf                     number;

begin

   v_valor_acrescimo_moeda := 0.00;
   v_valor_pago_moeda := 0.00;
   v_valor_desconto_moeda := 0.00;

   for pagamento in pagamentos
   loop

     select cont_010.sinal_titulo
     into v_sinal_titulo
     from cont_010
     where cont_010.codigo_historico = pagamento.codigo_historico;

     if v_sinal_titulo = 1
     then
       v_valor_acrescimo_moeda := v_valor_acrescimo_moeda + pagamento.vlr_juros_moeda;
       v_valor_pago_moeda := v_valor_pago_moeda + pagamento.valor_pago_moeda;
       v_valor_desconto_moeda := v_valor_desconto_moeda + pagamento.vlr_descontos_moeda;
     else
       v_valor_acrescimo_moeda := v_valor_acrescimo_moeda - pagamento.vlr_juros_moeda;
       v_valor_pago_moeda := v_valor_pago_moeda - pagamento.valor_pago_moeda;
       v_valor_desconto_moeda := v_valor_desconto_moeda - pagamento.vlr_descontos_moeda;
     end if;

   end loop;
   
   begin
     select cpag_040.controle_irrf 
     into v_contr_irrf
     from cpag_040
     where cpag_040.tipo_titulo = p_tipo_titulo;
   exception
   when no_data_found then v_contr_irrf := 0;
   end;
        
   if v_contr_irrf = 0
   then v_sinal_irrf := 1.00;
   else v_sinal_irrf := -1.00;
   end if;

   p_saldo_moeda := p_valor_moeda - v_valor_pago_moeda - v_valor_acrescimo_moeda + v_valor_desconto_moeda 
                 + (p_valor_irrf * v_sinal_irrf) + (p_valor_iss * p_cotacao) + (p_valor_inss * p_cotacao);

end inter_pr_get_saldo_moeda_cpag;
