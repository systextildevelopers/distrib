create or replace PACKAGE ST_PCK_BASI AS
    
    TYPE t_produto IS RECORD
    (
        nivel_estrutura   basi_010.nivel_estrutura%type,
        grupo_estrutura   basi_010.grupo_estrutura%type,
        subgru_estrutura  basi_010.subgru_estrutura%type,
        item_estrutura    basi_010.item_estrutura%type
    );
    
    function exists_produto(p_nivel_estrutura varchar2, p_grupo_estrutura varchar2, p_subgru_estrutura varchar2, p_item_estrutura varchar2) return boolean;
    function get_produto_by_integracao(p_produto_integracao varchar2) return t_produto;
    
    function exists_centro_custo(p_centro_custo number, p_codigo_empresa number) return boolean;
    function is_centro_custo_atv(p_centro_custo number, p_codigo_empresa number) return boolean;
    function exists_deposito(p_codigo_deposito number) return boolean;
    function get_empresa_by_deposito(p_codigo_deposito number) return number;
    function exists_moeda_by_cod(p_codigo number) return boolean;

    function get_descr_moeda(p_codigo number) return varchar2;
    function get_simbolo_moeda(p_codigo number) return varchar2;
    
END ST_PCK_BASI;
