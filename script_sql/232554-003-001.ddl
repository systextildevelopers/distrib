CREATE OR REPLACE VIEW "VI_CARGA_TABELAS" ("DESCRICAO", "CODIGO") AS 
  select descricao, codigo
from (
            select 'pedi_010 - Clientes'						        descricao,    'pedi_010' codigo,    1 ordem       from dual
    union   select 'supr_010 - Fornecedores'					        descricao,    'supr_010' codigo,    2 ordem       from dual
    union   select 'cpag_010 - Contas a Pagar'					        descricao,    'cpag_010' codigo,    3 ordem       from dual
    union   select 'fatu_070 - Contas a Receber'				        descricao,    'fatu_070' codigo,    4 ordem       from dual
    union   select 'pedi_020 - Representantes'					        descricao,    'pedi_020' codigo,    5 ordem       from dual
    union   select 'pedi_150 - Endereços Clientes'				        descricao,    'pedi_150' codigo,    6 ordem       from dual
    union   select 'basi_010 - Produtos'						        descricao,    'basi_010' codigo,    7 ordem       from dual
    union   select 'basi_020 - Produtos'						        descricao,    'basi_020' codigo,    8 ordem       from dual
    union   select 'basi_030 - Produtos'						        descricao,    'basi_030' codigo,    9 ordem       from dual
    union   select 'cpag_015 - Movimento Títulos a Pagar'		        descricao,    'cpag_015' codigo,    10 ordem      from dual
    union   select 'fatu_075 - Movimento Títulos a Receber'		        descricao,    'fatu_075' codigo,    11 ordem      from dual
    union   select 'cpag_200 - Adiant. Clientes e Forcedores'	        descricao,    'cpag_200' codigo,    12 ordem      from dual
    union   select 'basi_240 - NCM'								        descricao,    'basi_240' codigo,    13 ordem      from dual
    union   select 'basi_050 - Estrutura'						        descricao,    'basi_050' codigo,    14 ordem      from dual
    union   select 'basi_100 - Cores'							        descricao,    'basi_100' codigo,    15 ordem      from dual
    union   select 'basi_400 - Testes Qualidade Prod'			        descricao,    'basi_400' codigo,    16 ordem      from dual
    union   select 'supr_050 - Condição Pagamento'				        descricao,    'supr_050' codigo,    17 ordem      from dual
    union   select 'crec_110 - Conta Corrente Representante'	        descricao,    'crec_110' codigo,    18 ordem      from dual
    union   select 'supr_086 - Rel. Equipes X Produtos'			        descricao,    'supr_086' codigo,    19 ordem      from dual
    union   select 'supr_055 - Condição Pagamento a Prazo'		        descricao,    'supr_055' codigo,    20 ordem      from dual
    union   select 'supr_090 - Pedidos de Compra - Capa'		        descricao,    'supr_090' codigo,    21 ordem      from dual
    union   select 'supr_100 - Pedidos de Compra - Itens'		        descricao,    'supr_100' codigo,    22 ordem      from dual
    union   select 'pedi_220 - Grupos Economicos'				        descricao,    'pedi_220' codigo,    23 ordem      from dual
    union   select 'supr_120 - Pedidos de Compra - Observacoes'	        descricao,    'supr_120' codigo,    24 ordem      from dual
    union   select 'pedi_070 - Condição Pagamentos - Capa'		        descricao,    'pedi_070' codigo,    25 ordem      from dual
    union   select 'pedi_075 - Parcelas Condição Pagamentos'	        descricao,    'pedi_075' codigo,    26 ordem      from dual
    union   select 'basi_185 - Centro de Custo'					        descricao,    'basi_185' codigo,    27 ordem      from dual
    union   select 'pedi_100 - Pedidos de Venda - Capa'			        descricao,    'pedi_100' codigo,    28 ordem      from dual
    union   select 'pedi_110 - Pedidos de Venda - Itens'		        descricao,    'pedi_110' codigo,    29 ordem      from dual
    union   select 'cont_570 - Cadastro Rateios'				        descricao,    'cont_570' codigo,    30 ordem      from dual
    union   select 'cont_575 - Cadastro Rateios Contabeis'		        descricao,    'cont_575' codigo,    31 ordem      from dual
    union   select 'cont_580 - Cadastro Criterios Rateios'		        descricao,    'cont_580' codigo,    32 ordem      from dual
    union   select 'cont_585 - Itens Criterios Rateio'			        descricao,    'cont_585' codigo,    33 ordem      from dual
    union   select 'efic_020 - Paradas'							        descricao,    'efic_020' codigo,    34 ordem      from dual
    union   select 'efic_040 - Defeitos'						        descricao,    'efic_040' codigo,    35 ordem      from dual
    union   select 'estq_300 - Movimentação de Estoque'			        descricao,    'estq_300' codigo,    36 ordem      from dual
    union   select 'hdoc_001 - Testes Qualidade'				        descricao,    'hdoc_001' codigo,    37 ordem      from dual
    union   select 'mqop_010 - Maquinas'						        descricao,    'mqop_010' codigo,    38 ordem      from dual
    union   select 'mqop_020 - Maquinas'						        descricao,    'mqop_020' codigo,    39 ordem      from dual
    union   select 'mqop_030 - Maquinas'						        descricao,    'mqop_030' codigo,    40 ordem      from dual
    union   select 'mqop_040 - Operações'						        descricao,    'mqop_040' codigo,    41 ordem      from dual
    union   select 'obrf_010 - Nota Fiscal Entrada - Capa'		        descricao,    'obrf_010' codigo,    42 ordem      from dual
    union   select 'mqop_050 - Roteiro'							        descricao,    'mqop_050' codigo,    43 ordem      from dual
    union   select 'mqop_060 - Capacidade'						        descricao,    'mqop_060' codigo,    44 ordem      from dual
    union   select 'mqop_090 - Variaveis'						        descricao,    'mqop_090' codigo,    45 ordem      from dual
    union   select 'pcpt_020 - Rolos'							        descricao,    'pcpt_020' codigo,    46 ordem      from dual
    union   select 'fatu_050 - Notas Fiscal de Saída - Capa'	        descricao,    'fatu_050' codigo,    47 ordem      from dual
    union   select 'fatu_060 - Notas Fiscal de Saída - Itens'	        descricao,    'fatu_060' codigo,    48 ordem      from dual
    union   select 'patr_010 - Grupos Patrimoniais'				        descricao,    'patr_010' codigo,    49 ordem      from dual
    union   select 'patr_020 - Bens Patrimoniais'				        descricao,    'patr_020' codigo,    50 ordem      from dual
    union   select 'patr_030 - Relacionamentos de Bens Patrimoniais'	descricao,    'patr_030' codigo,    51 ordem      from dual
    union   select 'obrf_015 - Notas Fiscais Entrada - Itens'		    descricao,    'obrf_015' codigo,    52 ordem      from dual
    union   select 'patr_050 - Depreciacões'					        descricao,    'patr_050' codigo,    53 ordem      from dual
    union   select 'patr_060 - CIAP Cálculo Coeficiente'			    descricao,    'patr_060' codigo,    54 ordem      from dual
    union   select 'obrf_874 - Mensagens'			                    descricao,    'obrf_874' codigo,    55 ordem      from dual
    union   select 'pedi_080 - Natureza de Operação'			        descricao,    'pedi_080' codigo,    56 ordem      from dual
    union   select 'patr_065 - CIAP Bens Patrimoniais'			        descricao,    'patr_065' codigo,    57 ordem      from dual
    union   select 'cont_550 - Relacionamentos Contabeis'			    descricao,    'cont_550' codigo,    58 ordem      from dual
    union   select 'cont_540 - Código Contabeis'			            descricao,    'cont_540' codigo,    59 ordem      from dual
    union   select 'basi_015 - Parametros de Compras'			        descricao,    'basi_015' codigo,    60 ordem      from dual
    union   select 'pedi_090 - Tabela de Preços'			            descricao,    'pedi_090' codigo,    61 ordem      from dual
    union   select 'pedi_095 - Produtos da Tabela de Preços'			descricao,    'pedi_095' codigo,    62 ordem      from dual
    union   select 'cont_600 - Lancamento Contabeis'			        descricao,    'cont_600' codigo,    63 ordem      from dual    
    union   select 'pcpt_020 - Rolos, com etiqueta de terceiros'        descricao,    'pcpt_020_1' codigo,  63 ordem      from dual
    union   select 'estq_301 - Atualiza Saldo de Estoque'               descricao,    'estq_301' codigo,    63 ordem      from dual
    union   select 'pcpc_010 - cadastro periodos de producao'           descricao,    'pcpc_010' codigo,    64 ordem      from dual
)
order by ordem;
