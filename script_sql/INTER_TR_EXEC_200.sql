CREATE OR REPLACE TRIGGER  INTER_TR_EXEC_200 
  before insert or update or delete on EXEC_200 
  for each row 
 
begin 
 
   if inserting or updating
   then 

   begin
    insert into EXEC_010( 
          DATA_PRODUCAO 
          ,TURNO 
          ,GRUPO_MAQUINA  
          ,SUBGRUPO_MAQUINA  
          ,NUMERO_MAQUINA 
          ,IT_ACUMU_NIVEL99  
          ,IT_ACUMU_GRUPO  
          ,IT_ACUMU_SUBGRUPO  
          ,IT_ACUMU_ITEM 
          ,VELOCIDADE_REAL 
          ,TEMPO 
          ,QTDE_PRODUZIDA 
          ,INSERT_EXEC200 
    ) 
        values ( 
          :new.DATA_PRODUCAO 
          ,:new.TURNO 
          ,:new.GRUPO_MAQUINA  
          ,:new.SUBGRUPO_MAQUINA  
          ,:new.NUMERO_MAQUINA 
          ,:new.IT_ACUMU_NIVEL99  
          ,:new.IT_ACUMU_GRUPO  
          ,:new.IT_ACUMU_SUBGRUPO  
          ,:new.IT_ACUMU_ITEM 
          ,:new.VELOCIDADE_REAL 
          ,:new.TEMPO 
          ,:new.QTDE_PRODUZIDA 
          ,1 
        );
      exception when dup_val_on_index then
        update EXEC_010 set  
          DATA_PRODUCAO = :new.DATA_PRODUCAO 
          ,TURNO = :new.TURNO 
          ,GRUPO_MAQUINA = :new.GRUPO_MAQUINA  
          ,SUBGRUPO_MAQUINA = :new.SUBGRUPO_MAQUINA  
          ,NUMERO_MAQUINA = :new.NUMERO_MAQUINA 
          ,IT_ACUMU_NIVEL99 = :new.IT_ACUMU_NIVEL99  
          ,IT_ACUMU_GRUPO = :new.IT_ACUMU_GRUPO  
          ,IT_ACUMU_SUBGRUPO = :new.IT_ACUMU_SUBGRUPO  
          ,IT_ACUMU_ITEM = :new.IT_ACUMU_ITEM 
          ,VELOCIDADE_REAL = :new.VELOCIDADE_REAL 
          ,TEMPO = :new.TEMPO 
          ,QTDE_PRODUZIDA = :new.QTDE_PRODUZIDA 
          ,INSERT_EXEC200 = 1 
        where  
          DATA_PRODUCAO = nvl(:new.DATA_PRODUCAO, :old.DATA_PRODUCAO)
          AND TURNO = nvl(:new.TURNO, :old.TURNO) 
          AND GRUPO_MAQUINA = nvl(:new.GRUPO_MAQUINA, :old.GRUPO_MAQUINA)
          AND SUBGRUPO_MAQUINA = nvl(:new.SUBGRUPO_MAQUINA, :old.SUBGRUPO_MAQUINA)
          AND NUMERO_MAQUINA = nvl(:new.NUMERO_MAQUINA, :old.NUMERO_MAQUINA)
          AND IT_ACUMU_NIVEL99 = nvl(:new.IT_ACUMU_NIVEL99, :old.IT_ACUMU_NIVEL99)
          AND IT_ACUMU_GRUPO = nvl(:new.IT_ACUMU_GRUPO, :old.IT_ACUMU_GRUPO) 
          AND IT_ACUMU_SUBGRUPO = nvl(:new.IT_ACUMU_SUBGRUPO, :old.IT_ACUMU_SUBGRUPO) 
          AND IT_ACUMU_ITEM = nvl(:new.IT_ACUMU_ITEM, :old.IT_ACUMU_ITEM); 
    end;

   end if; 
 
   if deleting 
   then 
       delete from EXEC_010 
       where  
        DATA_PRODUCAO = :old.DATA_PRODUCAO 
        AND TURNO = :old.TURNO 
        AND GRUPO_MAQUINA = :old.GRUPO_MAQUINA  
        AND SUBGRUPO_MAQUINA = :old.SUBGRUPO_MAQUINA  
        AND NUMERO_MAQUINA = :old.NUMERO_MAQUINA 
        AND IT_ACUMU_NIVEL99 = :old.IT_ACUMU_NIVEL99  
        AND IT_ACUMU_GRUPO = :old.IT_ACUMU_GRUPO  
        AND IT_ACUMU_SUBGRUPO = :old.IT_ACUMU_SUBGRUPO  
        AND IT_ACUMU_ITEM = :old.IT_ACUMU_ITEM 
        AND INSERT_EXEC200 = 1; 
   end if; 
 
end INTER_TR_EXEC_200;

/
