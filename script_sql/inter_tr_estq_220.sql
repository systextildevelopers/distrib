
  CREATE OR REPLACE TRIGGER "INTER_TR_ESTQ_220" 
before update of stats on estq_220
for each row

declare
   v_quantidade              number:=0;
   v_quantidade_considerar   number:=0;
   v_quantidade_entregue     number:=0;
   v_situacao                number:=0;
   v_quantidade_requisitada  number:=0;
   v_ha_itens_pendentes      number:=0;

   v_baixar_op               number := 0;
   v_ordem_producao          number := 0;
   v_periodo_producao        number := 0;
   v_saldo_pacote            number := 0;
   v_codigo_usuario          number := 0;
   v_sequencia_045           number := 0;
   v_codigo_estagio          number := 0;
   v_saldo_est_ant           number := 0;

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   v_proc                    varchar2(100);
   v_deposito                number(3);

   function formata_numero(valor varchar2)
   return varchar2 is
      novo_valor varchar(30);
   begin
      novo_valor:= replace(replace(valor,'.',''),',','.');

      return(novo_valor);
   end;

begin
   v_proc := '';

   /*begin
      select lower(c.what)
      into v_proc
      from v$session a, v$process b, dba_jobs c, dba_jobs_running d
      where a.paddr=b.addr and c.job=d.job and a.sid in (select sid from dba_jobs_running)
      and   lower(c.what) = 'inter_pr_empenho_almoxarifado;'
      and   rownum = 1;
      exception when others
      then v_proc := '';
   end;

   if v_proc = 'inter_pr_empenho_almoxarifado;'
   then
      BEGIN
         DBMS_LOCK.SLEEP(8);
      END;
   end if;*/

   if :new.stats = 4
   then
      :new.qtd_exec:= formata_numero(:new.qtd_exec);

      begin
         if  :new.qtd_exec is not null
         and to_number(formata_numero(:new.qtd_exec)) > 0
         then
            v_quantidade_considerar := to_number(formata_numero(:new.qtd_exec));
         else
            v_quantidade_considerar := to_number(formata_numero(:new.quant));
         end if;

         if :new.tpta = 1
         then
            v_quantidade := v_quantidade_considerar;
         else
            v_quantidade := v_quantidade_considerar * (-1);
         end if;
      end;

      begin
         update estq_210
         set   estq_210.qtde_atual       = qtde_atual + v_quantidade
         where estq_210.maquina_estoque  = :new.idmaq
           and estq_210.numero_bandeja   = to_number(:new.idgav)
           and estq_210.compartimento    = :new.idcmp;
         exception when others
         then raise_application_error(-20000,'Erro ao atualizar saldo do compartimento: ' || sqlerrm);
      end;

      if :new.requisicao_almoxarifado > 0 and :new.seq_requisicao > 0
      then
         v_quantidade_entregue := 0;
         v_quantidade_requisitada := 0;

         begin
            select supr_520.qtde_entregue, supr_520.situacao, supr_520.qtde_requisitada,
                   supr_520.deposito
            into   v_quantidade_entregue,  v_situacao,        v_quantidade_requisitada,
                   v_deposito
            from supr_520
            where supr_520.num_requisicao = :new.requisicao_almoxarifado
              and supr_520.sequencia      = :new.seq_requisicao
              and rownum = 1;
            exception when others
            then v_quantidade_entregue := 0;
                 v_situacao := 0;
                 v_quantidade_requisitada := 0;
                 v_deposito := 0;
         end;

         v_situacao := 1;
         /*SS 66431/001 - Alteracao a pedido da Lunender
         if abs(v_quantidade) + v_quantidade_entregue >= v_quantidade_requisitada
         then
            v_situacao := 6;
         else
            v_situacao := 5;
         end if;

         begin
            update supr_520
            set supr_520.situacao = v_situacao,
                supr_520.qtde_entregue = supr_520.qtde_entregue + abs(v_quantidade)
            where supr_520.num_requisicao = :new.requisicao_almoxarifado
              and supr_520.sequencia      = :new.seq_requisicao;
         end;
         */

         begin
            update supr_520
            set supr_520.situacao = v_situacao
            where supr_520.num_requisicao = :new.requisicao_almoxarifado
              and supr_520.sequencia      = :new.seq_requisicao;
         end;

         -- atualiza o estq_040.qtde_empenhada com o total que se conseguiu empenhar do
         -- ultimo produto/deposito processado e que nco entrou na quebra de produto/deposito
         begin
            update estq_040
            set qtde_empenhada = qtde_empenhada + abs(v_quantidade),
                nome_prog_040  = 'INTER_TR_ESTQ_220'
            where cditem_nivel99  = :new.nivel_produto
              and cditem_grupo    = :new.grupo_produto
              and cditem_subgrupo = :new.subgrupo_produto
              and cditem_item     = :new.item_produto
              and lote_acomp      = 0
              and deposito        = v_deposito;
            exception when others
            then raise_application_error(-20000,'Problema ao atualizar dados na tabela estq_040');
         end;

         v_ha_itens_pendentes := 0;

         begin
            select 1
            into v_ha_itens_pendentes
            from supr_520
            where supr_520.num_requisicao = :new.requisicao_almoxarifado
              and supr_520.situacao <> 6
              and rownum = 1;
            exception when others
            then v_ha_itens_pendentes := 0;
         end;

         if v_ha_itens_pendentes = 0 --sem itens pendentes
         then
            begin
               update supr_510
               set supr_510.situacao = 1
               where supr_510.num_requisicao = :new.requisicao_almoxarifado;
            end;
         end if;
      end if;

      begin
         update obrf_082
         set obrf_082.qtde_enviada = abs(v_quantidade)
         where obrf_082.numero_ordem     = :new.ordem_servico
         and   obrf_082.prodsai_nivel99  = :new.nivel_produto
         and   obrf_082.prodsai_grupo    = :new.grupo_produto
         and   obrf_082.prodsai_subgrupo = :new.subgrupo_produto
         and   obrf_082.prodsai_item     = :new.item_produto;
      end;

      v_baixar_op := 0;

      begin
         select 0
         into v_baixar_op
         from supr_520
         where supr_520.num_requisicao = :new.requisicao_almoxarifado
           and supr_520.numero_ordem   = :new.ordem_servico
           and supr_520.situacao not in (6,7)
           and rownum = 1;
         exception when others
         then v_baixar_op := 1;
      end;

      if v_baixar_op = 1
      then
         v_baixar_op := 0;

         begin
            select 0
            into v_baixar_op
            from obrf_082
            where obrf_082.numero_ordem = :new.ordem_servico
              and not exists (select 1
                              from obrf_081
                              where obrf_081.numero_ordem      = obrf_082.numero_ordem
                              and   obrf_081.prodord_nivel99   = obrf_082.prodsai_nivel99
                              and   obrf_081.prodord_grupo     = obrf_082.prodsai_grupo
                              and   obrf_081.prodord_subgrupo  = obrf_082.prodsai_subgrupo
                              and   obrf_081.prodord_item      = obrf_082.prodsai_item)
              and not exists (select 1
                              from   (select estq_210.nivel_produto, estq_210.grupo_produto, estq_210.subgrupo_produto,
                                             estq_210.item_produto,
                                             sum(estq_210.qtde_atual) qtde
                                      from estq_210
                                      where estq_210.maquina_estoque  = :new.idmaq
                                      group by estq_210.nivel_produto, estq_210.grupo_produto, estq_210.subgrupo_produto,
                                               estq_210.item_produto) quantidades
                              where quantidades.nivel_produto    =  obrf_082.prodsai_nivel99
                              and   quantidades.grupo_produto    =  obrf_082.prodsai_grupo
                              and   quantidades.subgrupo_produto =  obrf_082.prodsai_subgrupo
                              and   quantidades.item_produto     =  obrf_082.prodsai_item
                              and   quantidades.qtde             >= obrf_082.qtde_enviada)
              and rownum = 1;
            exception when others
            then v_baixar_op := 1;
         end;
      end if;

      v_codigo_estagio := 0;

      begin
         select mqop_010.codigo_estagio
         into v_codigo_estagio
         from mqop_010,mqop_020
         where mqop_010.grupo_maquina = mqop_020.grupo_maquina
         and   mqop_020.tipo_processo = 21
         and   mqop_020.subgrupo_maquina = :new.idmaq;
         exception when others
         then v_codigo_estagio := 0;
      end;

     v_ordem_producao := 0;

      begin
         select pcpc_040.ordem_producao, pcpc_040.periodo_producao
         into   v_ordem_producao,        v_periodo_producao
         from pcpc_040
         where pcpc_040.numero_ordem = :new.ordem_servico
           and rownum = 1;
         exception when others
         then
            v_ordem_producao := 0;
            v_periodo_producao := 0;
      end;

      for reg_estagio_anterior in (select inter_vi_pcpc_020_040_045.estagio_anterior
      from inter_vi_pcpc_020_040_045
      where inter_vi_pcpc_020_040_045.codigo_estagio   = v_codigo_estagio
        and inter_vi_pcpc_020_040_045.periodo_producao = v_periodo_producao
        and inter_vi_pcpc_020_040_045.ordem_producao   = v_ordem_producao
      group by inter_vi_pcpc_020_040_045.estagio_anterior)
      loop
         begin
            select nvl(sum(pcpc_040.qtde_a_produzir_pacote),0)
            into v_saldo_est_ant
            from pcpc_040
            where pcpc_040.periodo_producao = v_periodo_producao
              and pcpc_040.ordem_producao   = v_ordem_producao
              and pcpc_040.codigo_estagio   = reg_estagio_anterior.estagio_anterior;
            exception when others
            then v_saldo_est_ant := 0;
         end;

         if v_saldo_est_ant > 0
         then
            v_baixar_op := 0;
         end if;
      end loop;

      if v_baixar_op = 1
      then
         begin
            select 0
            into v_baixar_op
            from obrf_082
            where obrf_082.numero_ordem     = :new.ordem_servico
            and   obrf_082.qtde_estrutura - obrf_082.qtde_enviada > 0
            and   rownum = 1;
            exception when others
            then v_baixar_op := 1;
         end;
      end if;

      if v_baixar_op = 1
      then
          /*BAIXA ESTAGIO DO ALMOXARIFADO PARA TODOS OS PACOTES*/
         for reg_pacotes in (select pcpc_040.ordem_confeccao,
                                    pcpc_040.qtde_pecas_prog,
                                    pcpc_040.qtde_em_producao_pacote
         from pcpc_040
         where pcpc_040.ordem_producao   = v_ordem_producao
           and pcpc_040.codigo_estagio   = v_codigo_estagio
           and pcpc_040.periodo_producao = v_periodo_producao)
         loop

            /* VERIFICA QUANTO JA FOI APONTADO PARA O PACOTE
            begin
               select nvl(sum(pcpc_045.qtde_produzida + pcpc_045.qtde_pecas_2a + pcpc_045.qtde_conserto + pcpc_045.qtde_perdas),0)
               into v_saldo_pacote
               from pcpc_045
               where pcpc_045.pcpc040_ordconf = reg_pacotes.ordem_confeccao
                 and pcpc_045.pcpc040_perconf = v_periodo_producao
                 and pcpc_045.pcpc040_estconf = v_codigo_estagio
                 and pcpc_045.ordem_producao  = v_ordem_producao;
               exception when others
               then v_saldo_pacote := 0;
            end;
*/

            /* DESCONTAR A QUANTIDADE APONTADA DA PROGRAMADA PARA SE TER O SALDO */
--          v_saldo_pacote := reg_pacotes.qtde_pecas_prog - v_saldo_pacote;
            v_saldo_pacote := reg_pacotes.qtde_em_producao_pacote;

            if v_saldo_pacote > 0
            then
               inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                                       ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

               begin
                  select hdoc_030.codigo_usuario
                  into v_codigo_usuario
                  from hdoc_030
                  where hdoc_030.usuario = ws_usuario_systextil;
                  exception when others
                  then v_codigo_usuario := 0;
               end;

               begin
                  select nvl(max(pcpc_045.sequencia),0) + 1
                  into v_sequencia_045
                  from pcpc_045
                  where pcpc_045.pcpc040_ordconf = reg_pacotes.ordem_confeccao
                    and pcpc_045.pcpc040_perconf = v_periodo_producao
                    and pcpc_045.pcpc040_estconf = v_codigo_estagio;
                  exception when others
                  then v_sequencia_045 := 0;
               end;

               begin
                  INSERT INTO pcpc_045
                    (pcpc040_ordconf,              pcpc040_perconf,
                     pcpc040_estconf,              sequencia,
                     data_producao,                hora_producao,
                     qtde_produzida,               turno_producao,
                     codigo_usuario,               ordem_producao,
                     executa_trigger
                  ) VALUES (
                     reg_pacotes.ordem_confeccao,  v_periodo_producao,
                     v_codigo_estagio,             v_sequencia_045,
                     sysdate,                      sysdate,
                     v_saldo_pacote,               1,
                     v_codigo_usuario,             v_ordem_producao,
                     3
                  );
               end;

/*
               begin
                  update pcpc_040
                  set pcpc_040.qtde_pecas_prod = pcpc_040.qtde_pecas_prod + v_saldo_pacote
                  where pcpc_040.periodo_producao = v_periodo_producao
                    and pcpc_040.codigo_estagio   = v_codigo_estagio
                    and pcpc_040.ordem_confeccao  = reg_pacotes.ordem_confeccao
                    and pcpc_040.ordem_producao   = v_ordem_producao;
               end;
*/
            end if;
         end loop;

         begin
            update obrf_080
            set obrf_080.status_requisicao = 4
            where obrf_080.numero_ordem = :new.ordem_servico;
         end;

         begin
            update supr_510
            set supr_510.requisitante   = ws_usuario_systextil
            where supr_510.num_requisicao = :new.requisicao_almoxarifado;
         end;
      end if;
   end if;
end inter_tr_estq_220;


-- ALTER TRIGGER "INTER_TR_ESTQ_220" ENABLE
 

/

exec inter_pr_recompile;

