
create table pedi_092 (
   colecao_tabela_servico    number(2) not null,
   mes_tabela_servico        number(2) not null,
   sequencia_tabela_servico  number(2) not null,
   colecao_tabela_insumo     number(2) not null,
   mes_tabela_insumo         number(2) not null,
   sequencia_tabela_insumo   number(2) not null,
   nivel_insumo              varchar2(1byte) default '',
   grupo_insumo              varchar2(5byte) default '',
   subgrupo_insumo           varchar2(3byte) default '',
   item_insumo               varchar2(6byte) default '',
   natureza_insumo           number(3) not null
);

alter table pedi_092
add constraint PK_PEDI_092 primary key(colecao_tabela_servico, mes_tabela_servico, sequencia_tabela_servico)

/
