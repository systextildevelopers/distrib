CREATE OR REPLACE FORCE VIEW INTER_VI_VALORES_RETENCOES_ORI (PERIODO_APURACAO, CODIGO_EMPRESA, NR_DUPLICATA, PARCELA, TIPO_TITULO, CNPJ9_FORNECEDOR, CNPJ4_FORNECEDOR, CNPJ2_FORNECEDOR, CNPJ_CPF_FORNECEDOR, NOME_FORNECEDOR, CIDADE_FORNECEDOR, UF_FORNECEDOR, SERIE_NF, DATA_EMISSAO, DATA_VENCIMENTO, VALOR_NF, VALOR_PARCELA, COD_RETENCAO_IRRF, BASE_IRRF, ALIQUOTA_IRRF, VALOR_IRRF, DATA_VENCTO_IRRF, COD_RETENCAO_INSS, BASE_INSS, ALIQUOTA_INSS, VALOR_INSS, DATA_VENCTO_INSS, COD_RETENCAO_ISS, BASE_ISS, ALIQUOTA_ISS, VALOR_ISS, DATA_VENCTO_ISS, DATA_TRANSACAO) AS 
  select trunc(cpag_010.data_contrato, 'MM')                            as PERIODO_APURACAO,
       cpag_010.codigo_empresa                                        as CODIGO_EMPRESA,
       cpag_010.nr_duplicata                                          as NR_DUPLICATA,
       cpag_010.parcela                                               as PARCELA,
       cpag_010.tipo_titulo                                           as TIPO_TITULO,
       cpag_010.cgc_9                                                 as CNPJ9_FORNECEDOR,
       cpag_010.cgc_4                                                 as CNPJ4_FORNECEDOR,
       cpag_010.cgc_2                                                 as CNPJ2_FORNECEDOR,
       case
          when cpag_010.cgc_4 = 0 then
             trim(to_char(cpag_010.cgc_9, '000000000')) || '-' ||
             trim(to_char(cpag_010.cgc_2, '00'))
          else
             trim(to_char(cpag_010.cgc_9, '00000000'))  || '/' ||
             trim(to_char(cpag_010.cgc_4, '0000'))      || '-' ||
             trim(to_char(cpag_010.cgc_2, '00'))
       end                                                            as CNPJ_CPF_FORNECEDOR,
       supr_010.nome_fornecedor                                       as NOME_FORNECEDOR,
      (select basi_160.cidade from basi_160
       where basi_160.cod_cidade = supr_010.cod_cidade)               as CIDADE_FORNECEDOR,
      (select basi_160.estado from basi_160
       where basi_160.cod_cidade = supr_010.cod_cidade)               as UF_FORNECEDOR,
       cpag_010.serie                                                 as SERIE_NF,
       cpag_010.data_contrato                                         as DATA_EMISSAO,
       cpag_010.data_vencimento                                       as DATA_VENCIMENTO,
      (select sum(valor_parcela) from cpag_010 cpag_010_2
       where cpag_010_2.nr_duplicata = cpag_010.nr_duplicata
       and   cpag_010_2.cgc_9        = cpag_010.cgc_9
       and   cpag_010_2.cgc_4        = cpag_010.cgc_4
       and   cpag_010_2.cgc_2        = cpag_010.cgc_2
       and   cpag_010_2.tipo_titulo  = cpag_010.tipo_titulo)          as VALOR_NF,
       cpag_010.valor_parcela                                         as VALOR_PARCELA,
       cpag_010.cod_ret_irrf                                          as COD_RETENCAO_IRRF,
       cpag_010.base_irrf                                             as BASE_IRRF,
       cpag_010.aliq_irrf                                             as ALIQUOTA_IRRF,
       cpag_010.valor_irrf                                            as VALOR_IRRF,
       inter_fn_data_vencto_retencoes(cpag_010.data_contrato, 'IRRF') as DATA_VENCTO_IRRF,
       cpag_010.cod_ret_inss                                          as COD_RETENCAO_INSS,
       cpag_010.base_inss                                             as BASE_INSS,
       cpag_010.aliq_inss                                             as ALIQUOTA_INSS,
       cpag_010.valor_inss_imp                                        as VALOR_INSS,   --estava cpag_010.valor_inss
       inter_fn_data_vencto_retencoes(cpag_010.data_contrato, 'INSS') as DATA_VENCTO_INSS,
       cpag_010.cod_ret_iss                                           as COD_RETENCAO_ISS,
       cpag_010.base_iss                                              as BASE_ISS,
       cpag_010.aliq_iss                                              as ALIQUOTA_ISS,
       cpag_010.valor_iss                                             as VALOR_ISS,
       inter_fn_data_vencto_retencoes(cpag_010.data_contrato, 'ISS')  as DATA_VENCTO_ISS,
	   cpag_010.data_transacao                                        as DATA_TRANSACAO
from cpag_010, supr_010
where cpag_010.cgc_9                = supr_010.fornecedor9
    and   cpag_010.cgc_4            = supr_010.fornecedor4
    and   cpag_010.cgc_2            = supr_010.fornecedor2
    and  (cpag_010.valor_irrf       > 0.00 or cpag_010.valor_inss > 0.00 or cpag_010.valor_iss > 0.00)
    and   cpag_010.cod_cancelamento = 0;

/
