alter table pedi_080_hist
add (perc_fcp_uf_dest_old    number(5,2),
     perc_fcp_uf_dest_new    number(5,2),
     perc_icms_uf_dest_old   number(5,2),
     perc_icms_uf_dest_new   number(5,2));

exec inter_pr_recompile;
/