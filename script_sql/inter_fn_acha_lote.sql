create or replace function inter_fn_acha_lote (p_empresa_matriz IN NUMBER,
                                                p_exercicio IN NUMBER,
                                                p_origem IN NUMBER, 
                                                p_lote IN NUMBER,
                                                p_data_lancto IN DATE) 
RETURN NUMBER IS v_lote number;
BEGIN
   select cont_520.lote
   into v_lote
   from cont_520
   where cont_520.cod_empresa = p_empresa_matriz
   and cont_520.exercicio = p_exercicio
   and cont_520.origem = p_origem
   and cont_520.lote = p_lote
   and cont_520.data_lote = p_data_lancto;
EXCEPTION
WHEN NO_DATA_FOUND
   THEN
      BEGIN
         select cont_520.lote
         into v_lote
         from cont_520
         where cont_520.cod_empresa = p_empresa_matriz
         and cont_520.exercicio = p_exercicio
         and cont_520.origem = p_origem
         and cont_520.data_lote = p_data_lancto
         and cont_520.situacao = 0
         and rownum = 1
         order by cont_520.lote asc;
      EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         select nvl(max(cont_520.lote),0) + 1
         into v_lote
         from cont_520
         where cont_520.cod_empresa = p_empresa_matriz
         and cont_520.exercicio = p_exercicio
         and cont_520.origem = p_origem;
         
         insert into cont_520 (cod_empresa, exercicio,
                               origem, lote,
                               data_lote, situacao)
         values (p_empresa_matriz, p_exercicio,
                 p_origem, v_lote, p_data_lancto, 0);
      END;
      RETURN v_lote;
END;

/

exec inter_pr_recompile;
