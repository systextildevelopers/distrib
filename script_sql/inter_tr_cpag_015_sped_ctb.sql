
  CREATE OR REPLACE TRIGGER "INTER_TR_CPAG_015_SPED_CTB" 
   before insert or update on cpag_015
   for each row

declare
   v_cliente_fornec  cont_600.cliente_fornecedor_part%type;
   v_cnpj_9          cont_600.cnpj9_participante%type;
   v_cnpj_4          cont_600.cnpj4_participante%type;
   v_cnpj_2          cont_600.cnpj2_participante%type;
   v_sid             cont_601.sid%type;
   v_instancia       cont_601.instancia%type;

begin

   if inserting
   then
      v_cnpj_9 := :new.dupl_for_for_cli9;
      v_cnpj_4 := :new.dupl_for_for_cli4;
      v_cnpj_2 := :new.dupl_for_for_cli2;
   else
      v_cnpj_9 := :old.dupl_for_for_cli9;
      v_cnpj_4 := :old.dupl_for_for_cli4;
      v_cnpj_2 := :old.dupl_for_for_cli2;
   end if;

   v_cliente_fornec := 2; -- fornecedor

   select sid,   inst_id
   into   v_sid, v_instancia
   from   v_lista_sessao_banco;

   insert into cont_601
      (sid,
       instancia,
       data_insercao,
       tabela_origem,
       cnpj_9,
       cnpj_4,
       cnpj_2,
       cliente_fornec)
   values
      (v_sid,
       v_instancia,
       sysdate,
       'CPAG_015',
       v_cnpj_9,
       v_cnpj_4,
       v_cnpj_2,
       v_cliente_fornec);

   exception
      when others then
         null;
end inter_tr_cpag_015_sped_ctb;
-- ALTER TRIGGER "INTER_TR_CPAG_015_SPED_CTB" ENABLE
 

/

exec inter_pr_recompile;

