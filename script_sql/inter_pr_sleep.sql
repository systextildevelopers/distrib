
  CREATE OR REPLACE PROCEDURE "INTER_PR_SLEEP" (tempo_analise_nf in number)
is
   v_erro number := 0;

begin
   dbms_lock.sleep(tempo_analise_nf);

end inter_pr_sleep;
 

/

exec inter_pr_recompile;

