
  CREATE OR REPLACE TRIGGER "TRIGGER_PCPF_060_HIST" 
BEFORE UPDATE of uhm, codigo_fardo, fardo_nivel99, fardo_grupo,
	fardo_subgrupo, fardo_item, lote_mat_prima, deposito,
	transacao, nf_entrada, serie_nf_entrada, cgc9_forne,
	cgc4_forne, cgc2_forne, peso_medio, peso_real,
	peso_embalagem, peso_medio_fardo, status_fardo, lote_fio,
	origem, fornecedor, padrao, fibra,
	preco_custo, letra, luz_dia, luz_negra,
	mic, csp, turno, operador,
	endereco_fardo, data_movimentacao, emp_nota_sai, num_nota_sai,
	ser_nota_sai, seq_nota_sai, periodo_producao, linha_produto,
	codigo_estagio, codigo, nome_prog, padrao_algodao,
	data_entrada, procedencia, tama, tama50,
	unif, resi, alon, micr,
	sf, rd, b, comer1,
	comer2, perc_umidade, pre_romaneio, regiao_hvi,
	safra_hvi, data_insercao, data_cardex, transacao_cardex,
	usuario_cardex, valor_movto_cardex, valor_contabil_cardex, tabela_origem_cardex,
	cor_fibra, fardo_origem, origem_fardo, tipo
or DELETE
or INSERT
on pcpf_060
for each row
declare
   ws_codigo_fardo         pcpf_060.codigo_fardo%type;
   ws_nivel                pcpf_060.fardo_nivel99%type;
   ws_grupo                pcpf_060.fardo_grupo%type;
   ws_sub                  pcpf_060.fardo_subgrupo%type;
   ws_item                 pcpf_060.fardo_item%type;
   ws_lote_old             pcpf_060.lote_mat_prima%type;
   ws_lote_atu             pcpf_060.lote_mat_prima%type;
   ws_deposito_old         pcpf_060.deposito%type;
   ws_deposito_atu         pcpf_060.deposito%type;
   ws_status_old           pcpf_060.status_fardo%type;
   ws_status_atu           pcpf_060.status_fardo%type;
   ws_peso_real_old        pcpf_060.peso_real%type;
   ws_peso_real_atu        pcpf_060.peso_real%type;
   ws_peso_medio_old       pcpf_060.peso_medio%type;
   ws_peso_medio_atu       pcpf_060.peso_medio%type;
   ws_letra_old            pcpf_060.letra%type;
   ws_letra_atu            pcpf_060.letra%type;
   ws_nota_old             pcpf_060.num_nota_sai%type;
   ws_nota_atu             pcpf_060.num_nota_sai%type;
   ws_transacao_old        pcpf_060.transacao%type;
   ws_transacao_atu        pcpf_060.transacao%type;
   ws_atualiza_estq        estq_005.atualiza_estoque%type;
   ws_tipo_ocorr           pcpf_060_hist.tipo_ocorr%type;
   ws_nome_programa        pcpf_060_hist.nome_programa%type;
   ws_usuario_rede         pcpf_060_hist.usuario_rede%type;
   ws_maquina_rede         pcpf_060_hist.maquina_rede%type;
   ws_aplicacao            pcpf_060_hist.aplicacao%type;
   ws_lote_fio_old         pcpf_060.lote_fio%type;
   ws_lote_fio_atu         pcpf_060.lote_fio%type;
begin
   if UPDATING then
      ws_codigo_fardo     := :old.codigo_fardo;
      ws_nivel            := :old.fardo_nivel99;
      ws_grupo            := :old.fardo_grupo;
      ws_sub              := :old.fardo_subgrupo;
      ws_item             := :old.fardo_item;
      ws_lote_atu         := :new.lote_mat_prima;
      ws_lote_old         := :old.lote_mat_prima;
      ws_deposito_atu     := :new.deposito;
      ws_deposito_old     := :old.deposito;
      ws_status_atu       := :new.status_fardo;
      ws_status_old       := :old.status_fardo;
      ws_peso_real_atu    := :new.peso_real;
      ws_peso_real_old    := :old.peso_real;
      ws_peso_medio_atu   := :new.peso_medio;
      ws_peso_medio_old   := :old.peso_medio;
      ws_letra_atu        := :new.letra;
      ws_letra_old        := :old.letra;
      ws_nota_atu         := :new.num_nota_sai;
      ws_nota_old         := :old.num_nota_sai;
      ws_transacao_atu    := :new.transacao;
      ws_transacao_old    := :old.transacao;
      ws_tipo_ocorr       := 'A';
      ws_nome_programa    := :new.nome_prog;
      ws_lote_fio_atu     := :new.lote_fio;
      ws_lote_fio_old     := :old.lote_fio;
      select atualiza_estoque
      into   ws_atualiza_estq
      from estq_005 where codigo_transacao = ws_transacao_atu;
   elsif DELETING then
         ws_codigo_fardo     := :old.codigo_fardo;
         ws_nivel            := :old.fardo_nivel99;
         ws_grupo            := :old.fardo_grupo;
         ws_sub              := :old.fardo_subgrupo;
         ws_item             := :old.fardo_item;
         ws_lote_atu         := 0;
         ws_lote_old         := :old.lote_mat_prima;
         ws_deposito_atu     := 0;
         ws_deposito_old     := :old.deposito;
         ws_status_atu       := 0;
         ws_status_old       := :old.status_fardo;
         ws_peso_real_atu    := 0.0;
         ws_peso_real_old    := :old.peso_real;
         ws_peso_medio_atu   := 0.0;
         ws_peso_medio_old   := :old.peso_medio;
         ws_letra_atu        := '';
         ws_letra_old        := :old.letra;
         ws_nota_atu         := 0.0;
         ws_nota_old         := :old.num_nota_sai;
         ws_transacao_atu    := 0;
         ws_transacao_old    := :old.transacao;
         ws_tipo_ocorr       := 'D';
         ws_nome_programa    := :old.nome_prog;
         ws_lote_fio_atu     := 0;
         ws_lote_fio_old     := :old.lote_fio;
         select atualiza_estoque
            into ws_atualiza_estq
            from estq_005 where codigo_transacao = ws_transacao_old;
      elsif INSERTING then
            ws_codigo_fardo     := :new.codigo_fardo;
            ws_nivel            := :new.fardo_nivel99;
            ws_grupo            := :new.fardo_grupo;
            ws_sub              := :new.fardo_subgrupo;
            ws_item             := :new.fardo_item;
            ws_lote_atu         := :new.lote_mat_prima;
            ws_lote_old         := 0;
            ws_deposito_atu     := :new.deposito;
            ws_deposito_old     := 0;
            ws_status_atu       := :new.status_fardo;
            ws_status_old       := 0;
            ws_peso_real_atu    := :new.peso_real;
            ws_peso_real_old    := 0;
            ws_peso_medio_atu   := :new.peso_medio;
            ws_peso_medio_old   := 0;
            ws_letra_atu        := :new.letra;
            ws_letra_old        := '';
            ws_nota_atu         := :new.num_nota_sai;
            ws_nota_old         := 0;
            ws_transacao_atu    := :new.transacao;
            ws_transacao_old    := 0;
            ws_tipo_ocorr       := 'I';
            ws_nome_programa    := :new.nome_prog;
            ws_lote_fio_atu     := :new.lote_fio;
            ws_lote_fio_old     := 0;
            select atualiza_estoque
            into   ws_atualiza_estq
            from estq_005 where codigo_transacao = ws_transacao_atu;
   end if;

   begin
      select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
      into ws_usuario_rede, ws_maquina_rede, ws_aplicacao
      from sys.gv_$session
      where audsid  = userenv('SESSIONID')
        and inst_id = userenv('INSTANCE')
        and rownum < 2;
   end;

   INSERT INTO pcpf_060_hist
     (codigo_fardo,      nivel,             grupo,
      sub,               item,              lote_old,
      lote_atu,          deposito_old,      deposito_atu,
      status_old,        status_atu,        peso_real_old,
      peso_real_atu,     peso_medio_old,    peso_medio_atu,
      letra_old,         letra_atu,         nota_old,
      nota_atu,          transacao_old,     transacao_atu,
      atualiza_estq,     data_ocorr,        tipo_ocorr,
      nome_programa,     usuario_rede,      maquina_rede,
      aplicacao,         lote_fio_old,      lote_fio_atu)
   VALUES
     (ws_codigo_fardo,   ws_nivel,          ws_grupo,
      ws_sub,            ws_item,           ws_lote_old,
      ws_lote_atu,       ws_deposito_old,   ws_deposito_atu,
      ws_status_old,     ws_status_atu,     ws_peso_real_old,
      ws_peso_real_atu,  ws_peso_medio_old, ws_peso_medio_atu,
      ws_letra_old,      ws_letra_atu,      ws_nota_old,
      ws_nota_atu,       ws_transacao_old,  ws_transacao_atu,
      ws_atualiza_estq,  sysdate,           ws_tipo_ocorr,
      ws_nome_programa,  ws_usuario_rede,   ws_maquina_rede,
      ws_aplicacao,      ws_lote_fio_old,   ws_lote_fio_atu);

   if INSERTING or UPDATING then
      :new.nome_prog := '';
   end if;

end trigger_pcpf_060_hist;

-- ALTER TRIGGER "TRIGGER_PCPF_060_HIST" ENABLE
 

/

exec inter_pr_recompile;

