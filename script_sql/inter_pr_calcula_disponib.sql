
  CREATE OR REPLACE PROCEDURE "INTER_PR_CALCULA_DISPONIB" (par_codigo_empresa fatu_503.codigo_empresa%type) is
  cursor basi010 is
    select basi_010.nivel_estrutura, basi_010.grupo_estrutura, basi_010.subgru_estrutura, basi_010.item_estrutura
      from basi_010
     where basi_010.calc_disponibilidade > 0;

  var_perc_reducao    fatu_503.perc_red_prod%type;
  var_tot_disp         pedi_104.total_disponibilidade%type;
  var_estoque_atual    pedi_104.estoque_atual%type;
  var_prev_prod        pedi_104.prev_producao%type;
  var_pedidos_compra   pedi_104.pedidos_compra%type;
  var_pedidos_venda    pedi_104.pedidos_venda%type;
  var_pedidos_integrar pedi_104.pedidos_a_integrar%type;
  var_qtde_tags_emtrans number(9);
  var_dias_disponibilidade fatu_503.dias_disponibilidade%type;
  var_data_fim         basi_010.data_fim_disp%type;
begin

  -- busca os parametros de empresa
  begin
    select fatu_503.perc_red_prod, fatu_503.dias_disponibilidade
      into var_perc_reducao,       var_dias_disponibilidade
      from fatu_503
     where fatu_503.codigo_empresa = par_codigo_empresa;
  exception
    when others then
      var_perc_reducao    := 0;
      var_dias_disponibilidade := 0;
  end;

  if var_dias_disponibilidade > 0
  then
    -- limpa a tabela para iniciar o rec�lculo
    delete from pedi_104;

    for reg010 in basi010 loop

	    begin
          select basi_010.data_fim_disp
           into var_data_fim
           from basi_010
            where basi_010.nivel_estrutura  = reg010.nivel_estrutura
              and basi_010.grupo_estrutura  = reg010.grupo_estrutura
              and basi_010.subgru_estrutura = reg010.subgru_estrutura
              and basi_010.item_estrutura   = reg010.item_estrutura
              and basi_010.data_fim_disp is not null;
        exception
          when others then
            var_data_fim := trunc(sysdate + var_dias_disponibilidade);
	    end;

       begin
          select nvl(sum(estq_040.qtde_estoque_atu), 0)
           into var_estoque_atual
           from estq_040, basi_205
          where estq_040.deposito        = basi_205.codigo_deposito
            and estq_040.cditem_nivel99  = reg010.nivel_estrutura
            and estq_040.cditem_grupo    = reg010.grupo_estrutura
            and estq_040.cditem_subgrupo = reg010.subgru_estrutura
            and estq_040.cditem_item     = reg010.item_estrutura
            and basi_205.considera_tmrp  = 1;
       exception
         when others then
           var_estoque_atual := 0;
       end;

       var_qtde_tags_emtrans := 0;

       begin
         select count(*) into var_qtde_tags_emtrans
           from pcpc_330, basi_205, basi_030
          where   pcpc_330.nivel       = basi_030.nivel_estrutura
            and   pcpc_330.grupo       = basi_030.referencia
            and   pcpc_330.nivel       = reg010.nivel_estrutura
            and   pcpc_330.grupo       = reg010.grupo_estrutura
            and   pcpc_330.subgrupo    = reg010.subgru_estrutura
            and  (pcpc_330.item        = reg010.item_estrutura or reg010.item_estrutura = basi_030.cor_de_estoque)
            and   pcpc_330.deposito    = basi_205.codigo_deposito
            and   basi_205.considera_tmrp = 1
            and   pcpc_330.estoque_tag = 4
            and   exists (select 1 from pcpc_339 where cod_deposito = pcpc_330.deposito);
       exception
         when others then
           var_qtde_tags_emtrans := 0;
       end;


       for p_producao in
          (select periodo_producao, data_ini_fatu, data_fim_fatu
           from pcpc_010
           where pcpc_010.situacao_periodo = 0
           and   pcpc_010.area_periodo = reg010.nivel_estrutura
           and   pcpc_010.codigo_empresa = par_codigo_empresa
           and   trunc(pcpc_010.data_fim_fatu) between trunc(sysdate) and var_data_fim)
       loop
          if reg010.nivel_estrutura = '1' then
             begin
                select nvl(sum(pcpc_040.qtde_pecas_prog),0) -
                       nvl(sum(pcpc_040.qtde_pecas_prod),0) - nvl(sum(pcpc_040.qtde_pecas_2a),0) -
                       nvl(sum(pcpc_040.qtde_perdas),0)     - nvl(sum(pcpc_040.qtde_conserto),0)
                into   var_prev_prod
                from pcpc_020, pcpc_040, pcpc_010
                where pcpc_020.periodo_producao = pcpc_010.periodo_producao
                and   pcpc_010.area_periodo     = 1
                and   pcpc_020.ordem_producao   = pcpc_040.ordem_producao
                and   pcpc_020.ultimo_estagio   = pcpc_040.codigo_estagio
                and   pcpc_010.data_fim_fatu   <= p_producao.data_fim_fatu
                and   pcpc_020.cod_cancelamento = 0
                and   pcpc_040.proconf_nivel99  = reg010.nivel_estrutura
                and   pcpc_040.proconf_grupo    = reg010.grupo_estrutura
                and   pcpc_040.proconf_subgrupo = reg010.subgru_estrutura
                and   pcpc_040.proconf_item     = reg010.item_estrutura;
             exception
               when others then
                 var_prev_prod := 0;
             end;
          else
             if reg010.nivel_estrutura = '2' then
               begin
                 select nvl(sum(pcpb_020.qtde_quilos_prog - pcpb_020.qtde_quilos_prod), 0)
                   into var_prev_prod
                   from pcpb_020, pcpb_010, pcpc_010
                  where pcpb_010.periodo_producao  = pcpc_010.periodo_producao
                    and pcpc_010.area_periodo      = 2
                    and pcpc_010.data_fim_fatu    <= p_producao.data_fim_fatu
                    and pcpb_020.pano_sbg_nivel99  = reg010.nivel_estrutura
                    and pcpb_020.pano_sbg_grupo    = reg010.grupo_estrutura
                    and pcpb_020.pano_sbg_subgrupo = reg010.subgru_estrutura
                    and pcpb_020.pano_sbg_item     = reg010.item_estrutura
                    and pcpb_020.qtde_quilos_prog  > pcpb_020.qtde_quilos_prod
                    and pcpb_020.cod_cancelamento  = 0
                    and pcpb_020.ordem_producao    = pcpb_010.ordem_producao
                    and pcpb_010.cod_cancelamento  = 0;
               exception
                 when others then
                   var_prev_prod := 0;
               end;
             else
               if reg010.nivel_estrutura = '4' then
                 begin
                    select nvl(sum(pcpt_010.qtde_quilos_prog - pcpt_010.qtde_quilos_prod), 0)
                    into var_prev_prod
                    from pcpt_010, pcpc_010
                    where pcpt_010.periodo_producao = pcpc_010.periodo_producao
                    and   pcpc_010.area_periodo     = 4
                    and   pcpc_010.data_fim_fatu   <= p_producao.data_fim_fatu
                    and   pcpt_010.cd_pano_nivel99  = reg010.nivel_estrutura
                    and   pcpt_010.cd_pano_grupo    = reg010.grupo_estrutura
                    and   pcpt_010.cd_pano_subgrupo = reg010.subgru_estrutura
                    and   pcpt_010.cd_pano_item     = reg010.item_estrutura
                    and   pcpt_010.qtde_quilos_prog > pcpt_010.qtde_quilos_prod
                    and   pcpt_010.qtde_rolos_prog  > pcpt_010.qtde_rolos_prod
                    and   pcpt_010.cod_cancelamento = 0;
                 exception
                   when others then
                     var_prev_prod := 0;
                 end;
               else
                 -- n�vel 7 n�o calcula previs�o de produ��o
                 var_prev_prod := 0;
               end if;
             end if;
          end if;

          if var_prev_prod > 0 then
            var_prev_prod := var_prev_prod - (var_prev_prod * var_perc_reducao / 100);
          end if;

          begin
             select nvl(sum(supr_100.qtde_saldo_item), 0)
             into var_pedidos_compra
             from supr_090, supr_100
             where supr_090.pedido_compra     = supr_100.num_ped_compra
             and   supr_090.data_prev_entr   <= p_producao.data_fim_fatu
             and   supr_090.cod_cancelamento  = 0
             and   supr_100.cod_cancelamento  = 0
             and   supr_100.situacao_item     < 3
             and   supr_100.item_100_nivel99  = reg010.nivel_estrutura
             and   supr_100.item_100_grupo    = reg010.grupo_estrutura
             and   supr_100.item_100_subgrupo = reg010.subgru_estrutura
             and   supr_100.item_100_item     = reg010.item_estrutura;
          exception
            when others then
              var_pedidos_compra := 0;
          end;

          begin
             select nvl(sum(pedi_110.qtde_pedida - pedi_110.qtde_faturada), 0)
             into var_pedidos_venda
             from pedi_100, pedi_110, basi_205
             where pedi_100.pedido_venda      = pedi_110.pedido_venda
             and   pedi_110.codigo_deposito   = basi_205.codigo_deposito
             and   pedi_100.situacao_venda   <> 10
             and   pedi_110.cod_cancelamento  = 0
             and   basi_205.considera_tmrp    = 1
             and   pedi_110.cd_it_pe_nivel99  = reg010.nivel_estrutura
             and   pedi_110.cd_it_pe_grupo    = reg010.grupo_estrutura
             and   pedi_110.cd_it_pe_subgrupo = reg010.subgru_estrutura
             and   pedi_110.cd_it_pe_item     = reg010.item_estrutura;
             exception
             when others then
                var_pedidos_venda := 0;
          end;

          begin
             select nvl(sum(inte_110.qtde_pedida/100), 0)
             into var_pedidos_integrar
             from inte_100, inte_110, basi_205
             where inte_100.pedido_venda    = inte_110.pedido_venda
             and   inte_110.codigo_deposito = basi_205.codigo_deposito
             and   inte_100.data_entrega   <= p_producao.data_fim_fatu
             and   inte_100.sit_importacao in (1,2)
             and   inte_100.tipo_registro   = 1
             and   basi_205.considera_tmrp  = 1
             and   inte_110.item_nivel99    = reg010.nivel_estrutura
             and   inte_110.item_grupo      = reg010.grupo_estrutura
             and   inte_110.item_sub        = reg010.subgru_estrutura
             and   inte_110.item_item       = reg010.item_estrutura;
          exception
            when others then
              var_pedidos_integrar := 0;
          end;

          var_tot_disp := var_estoque_atual + var_prev_prod + var_qtde_tags_emtrans + var_pedidos_compra - var_pedidos_venda - var_pedidos_integrar;
          if reg010.nivel_estrutura = '1' then
            var_tot_disp := trunc(var_tot_disp);
          end if;

          begin
            if var_tot_disp < 0 then
              var_tot_disp:= 0;
            end if;

            insert into pedi_104
              (nivel_estrutura,
               grupo_estrutura,
               subgru_estrutura,
               item_estrutura,
               data_inicio_disponib,
               data_final_disponib,
               total_disponibilidade,
               estoque_atual,
               prev_producao,
               pedidos_compra,
               pedidos_venda,
               pedidos_a_integrar)
            values
              (reg010.nivel_estrutura,
               reg010.grupo_estrutura,
               reg010.subgru_estrutura,
               reg010.item_estrutura,
               p_producao.data_ini_fatu,
               p_producao.data_fim_fatu,
               var_tot_disp,
               var_estoque_atual,
               var_prev_prod,
               var_pedidos_compra,
               var_pedidos_venda,
               var_pedidos_integrar);
          end;
       end loop;
    end loop;
    commit;
  end if;
end inter_pr_calcula_disponib;


 

/

exec inter_pr_recompile;

