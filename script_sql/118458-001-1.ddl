delete from hdoc_033 where hdoc_033.programa in ('cont_e006','cont_f003','cont_f007','cont_f565','cpag_f120',
                       'cpag_f200','datatex','estq_f026','estq_f049','estq_f306',
                       'inte_f225','inte_p330','obrf_f092','oper_f044','oper_f077',
                       'pedi_e470','pedi_f467');

alter trigger trigger_hdoc_036_hist disable;

delete from hdoc_036 where hdoc_036.codigo_programa in ('cont_e006','cont_f003','cont_f007','cont_f565','cpag_f120',
                       'cpag_f200','datatex','estq_f026','estq_f049','estq_f306',
                       'inte_f225','inte_p330','obrf_f092','oper_f044','oper_f077',
                       'pedi_e470','pedi_f467');

alter trigger trigger_hdoc_036_hist enable;

delete from hdoc_035 where hdoc_035.codigo_programa in ('cont_e006','cont_f003','cont_f007','cont_f565','cpag_f120',
                       'cpag_f200','datatex','estq_f026','estq_f049','estq_f306',
                       'inte_f225','inte_p330','obrf_f092','oper_f044','oper_f077',
                       'pedi_e470','pedi_f467');

commit;
