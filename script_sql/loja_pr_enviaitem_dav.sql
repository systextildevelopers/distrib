
  CREATE OR REPLACE PROCEDURE "LOJA_PR_ENVIAITEM_DAV" (pCODFIL IN varchar2,
                                                    pTIPPED IN varchar2,
                                                    pNUMPED IN varchar2,
                                                    pNUMITE IN number,
                                                    pVLRCUS IN number,
                                                    pVLRPGT IN number,
                                                    pQTDITE IN number,
                                                    pVLRITE IN number,
                                                    pVLRENT IN number,
                                                    pPROEXT IN varchar2,
                                                    pPROMOC IN varchar2,
                                                    pPREUNI IN number,
                                                    pDESMUL IN varchar2,
                                                    pOBSERV IN varchar2,
                                                    pPERIPI IN number,
                                                    pNIV    IN varchar2,
                                                    pGRUPO  IN varchar2,
                                                    pSUB    IN varchar2,
                                                    pITEM   IN varchar2,
                                                    p_msg_erro out varchar2) is

begin
  loja_pr_enviaitem_dav_inte(pCODFIL,
                              pTIPPED,
                              pNUMPED,
                              pNUMITE,
                              pVLRCUS,
                              pVLRPGT,
                              pQTDITE,
                              pVLRITE,
                              pVLRENT,
                              pPROEXT,
                              pPROMOC,
                              pPREUNI,
                              pDESMUL,
                              pOBSERV,
                              pPERIPI,
                              pNIV,
                              pGRUPO,
                              pSUB,
                              pITEM,
                              p_msg_erro);
end loja_pr_enviaitem_dav;
 

/

exec inter_pr_recompile;

