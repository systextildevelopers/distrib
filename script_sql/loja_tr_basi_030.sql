
  CREATE OR REPLACE TRIGGER "LOJA_TR_BASI_030" 
BEFORE  UPDATE
of descr_referencia, unidade_medida


on basi_030
for each row

begin

  if updating and
     :new.descr_referencia <> :old.descr_referencia or
     :new.unidade_medida <> :old.unidade_medida
  then
     BEGIN
        update basi_010
           set basi_010.flag_exportacao_loja = 0
        where basi_010.nivel_estrutura  = :new.nivel_estrutura
          and basi_010.grupo_estrutura  = :new.referencia;
     EXCEPTION
        WHEN OTHERS THEN
        raise_application_error (-20000, 'Erro na atualizacao da tabela basi_010, executado na basi_030. ' || SQLERRM);
     END;

  end if;


end;
-- ALTER TRIGGER "LOJA_TR_BASI_030" ENABLE
 

/

exec inter_pr_recompile;

