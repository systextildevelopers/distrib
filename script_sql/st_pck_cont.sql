create or replace package ST_PCK_CONT as
    
    TYPE t_exercicio IS RECORD
    (
        exercicio   cont_500.exercicio%type,
        situacao    cont_500.situacao%type
    );
    TYPE t_periodo IS RECORD
    (
        periodo     cont_510.periodo%type,
        per_inicial cont_510.per_inicial%type,
        per_final   cont_510.per_final%type
    );
    
    TYPE t_conta_cpag IS RECORD
    (
        cta_irrf_cpag   cont_560.cta_irrf_cpag%type,
        cta_iss_cpag    cont_560.cta_iss_cpag%type,
        cta_inss_cpag   cont_560.cta_inss_cpag%type,
        cta_pis_cpag    cont_560.cta_pis_cpag%type,
        cta_cofins_cpag cont_560.cta_cofins_cpag%type,
        cta_csl_cpag    cont_560.cta_csl_cpag%type,
        
        hst_irrf_cpag   cont_560.hst_irrf_cpag%type,
        hst_iss_cpag    cont_560.hst_iss_cpag%type,
        hst_inss_cpag   cont_560.hst_inss_cpag%type,
        hst_pis_cpag    cont_560.hst_pis_cpag%type,
        hst_cofins_cpag cont_560.hst_cofins_cpag%type,
        hst_csl_cpag    cont_560.hst_csl_cpag%type,
        
        cta_pis_recuperar       cont_560.hst_csl_cpag%type,
        cta_confins_recuperar   cont_560.hst_csl_cpag%type,
        hst_pis_recuperar       cont_560.hst_csl_cpag%type,
        hst_confins_recuperar   cont_560.hst_csl_cpag%type
        
    );
    
    function get_exercicio(p_codigo_empresa number, p_data_lancamento date) return t_exercicio;
    
    function get_next_numero_lancamento(p_codigo_empresa number, p_exercicio number) return number;
    
    ----------------------
    -- Valida transação --
    ----------------------
    function exists_transacao(p_codigo_transacao number) return boolean;
    function get_transacao(p_documento number, p_serie number, p_cgc_9 number, p_cgc_4 number, p_cgc_2 number) return number;
    
    ---------------------------
    -- Obtem Plano de Contas --
    ---------------------------
    function get_plano_conta(p_codigo_empresa number, p_exercicio number) return number;
    
    function get_conta_contabil(p_codigo_empresa number, p_exercicio number) return number;
    function get_conta_reduzida(p_codigo_empresa number, p_exercicio number) return number;
    
    ----------------------
    -- Valida histórico --
    ----------------------
    function exists_historico_by_cod(p_codigo_historico number) return boolean;
    function get_historico_by_cod(p_codigo_historico number) return cont_010.historico_contab%type;
    
    ----------------------------
    -- Obtem período contábil --
    ----------------------------
    function get_periodo_contabil(p_codigo_empresa number, p_exercicio number, p_data_lancamento date) return t_periodo; 
    
    function exists_tipo_contabil(p_codigo_contabil number) return boolean;
    
    ----------------
    -- Obtem lote --
    ----------------
    function acha_lote(p_empresa_lt number, p_exercicio_lt number, p_origem_lt number, p_lote_lt number, p_data_lt date, p_tipo_lancto_lt number) return number; 
    
    function checa_data(p_cod_empresa number, p_data_lanc date, p_transacao number, p_programa_gerador varchar2) return number;
    
    ------------------------------------------
    -- Valida tipo de pagamento --
    ------------------------------------------
    function exists_tipo_pagamento(p_tipo_pagamento number) return boolean;
    
    ------------------------------
    -- Valida posicao do titulo --
    ------------------------------
    function exists_posicao(p_posicao number) return boolean;
    
    ------------------------------
    -- Contas CPAG --
    ------------------------------
    function get_contas_cpag(p_plano_conta number) return t_conta_cpag;
    
    function gera_contabilizacao(p_cod_empresa      IN NUMBER, p_c_custo            IN NUMBER, p_cnpj9           IN NUMBER,
                                p_cnpj2             IN NUMBER, p_codContabilForn    IN NUMBER, p_documento       IN NUMBER,
                                p_data_transacao	IN DATE,   p_valor              IN NUMBER) return varchar2;
                                
end;
