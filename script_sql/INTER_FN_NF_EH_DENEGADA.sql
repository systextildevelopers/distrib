CREATE OR REPLACE FUNCTION INTER_FN_NF_EH_DENEGADA (
      p_situacao_nfisc     number,
      p_modelo_nota     varchar2,
      p_data_exec       date)
return varchar2 is

   nf_valida varchar2(1);

begin
    if EXTRACT(YEAR FROM p_data_exec) > '2022' and (p_situacao_nfisc = 4 or p_situacao_nfisc = 5) and
       (p_modelo_nota = '55' or p_modelo_nota = '57' or p_modelo_nota = '65')
   then
        nf_valida:= 'S';
   else
      nf_valida := 'N';
   end if;

   return nf_valida;
end;
