CREATE TABLE  PROJ_070_LOG 
   (	TIPO_OCORR VARCHAR2(1), 
	DATA_OCORR DATE, 
	HORA_OCORR DATE, 
	USUARIO_REDE VARCHAR2(20), 
	MAQUINA_REDE VARCHAR2(40), 
	APLICACAO VARCHAR2(20), 
	USUARIO_SISTEMA VARCHAR2(250), 
	NOME_PROGRAMA VARCHAR2(20), 
	SEQ_IMPORTACAO_ACABAMENTO NUMBER(30,0), 
	CODIGO_EMPRESA NUMBER(3,0), 
	DATA_IMPORTACAO DATE, 
	MAQUINA VARCHAR2(25), 
	CODIGO_ARTIGO VARCHAR2(18), 
	NIVEL VARCHAR2(1), 
	GRUPO VARCHAR2(5), 
	SUBGRUPO VARCHAR2(3), 
	ITEM VARCHAR2(6), 
	QTDE_PRODUZIR_NEW NUMBER(12,0), 
	QTDE_PRODUZIR_OLD NUMBER(12,0)
   );
   
/
