ALTER TABLE supr_010 
ADD (considera_ttd number(1) default 0);

COMMENT ON COLUMN supr_010.considera_ttd IS 'Indica se o fornecedor participa das regras do TTD para produtos nacionais importados';
exec inter_pr_recompile;
/
