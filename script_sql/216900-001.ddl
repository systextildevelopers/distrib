ALTER TABLE TMRP_650 ADD PERIODO_PRODUCAO NUMBER(4) DEFAULT 0;

ALTER TABLE TMRP_650 ADD OBSERVACAO VARCHAR2(120) NULL;

create table MQOP_131
(
  nivel       VARCHAR2(1) not null,
  grupo       VARCHAR2(5) not null,
  subgru      VARCHAR2(3) not null,
  item        VARCHAR2(6) not null,
  qtde_maqs   NUMBER default 0 not null,
  qtde_dias   NUMBER default 0 not null,
  qtde_turnos NUMBER default 0 not null,
  qtde_rolos  NUMBER default 0 not null,
  capacidade  NUMBER default 0 not null,
  narrativa   VARCHAR2(65) default ''
);

alter table MQOP_131 add constraint PK_MQOP_131 primary key (NIVEL, GRUPO, SUBGRU, ITEM);
