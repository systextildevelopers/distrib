CREATE OR REPLACE TRIGGER "INTER_TR_BASI_021_1" 
   after insert
      or update
      of consumo_componente
on basi_021 -- Estrutura do produto de Projeto
for each row

declare
Pragma Autonomous_Transaction;
   v_valor_parametro             number;
   v_um                          varchar2(2);

begin
    begin    
      select empr_002.consumo_max_comp
      into v_valor_parametro
      from empr_002;
      exception
      when OTHERS then
        v_valor_parametro := 0;
    end;
   
    if inserting or updating and v_valor_parametro > 0
    then
       if :new.consumo_componente <> :old.consumo_componente
       then
         inter_pr_valida_quantidade_kg(v_valor_parametro,
                                       :new.codigo_projeto,
                                       :new.sequencia_projeto,
                                       :new.nivel_item,
                                       :new.grupo_item,
                                       :new.subgru_item,
                                       :new.item_item,
                                       :new.sequencia_estrutura,
                                       :new.alternativa_produto,
                                       :new.consumo_componente,
                                       :new.subgru_item);
      end if;
   end if;
end inter_tr_basi_021_1;

/

exec inter_pr_recompile;

