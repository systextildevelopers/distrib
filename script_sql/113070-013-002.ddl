ALTER TABLE obrf_829
DROP CONSTRAINT PK_OBRF_829;

drop index PK_OBRF_829;

ALTER TABLE obrf_829
ADD CONSTRAINT PK_OBRF_829 PRIMARY KEY (empresa,quadro_ajuste, cod_msg, cod_ajuste);
  
exec inter_pr_recompile;
