
  CREATE OR REPLACE FUNCTION "INTER_FN_BUSCA_MAIOR_PRC_VENDA" (p_empresa            in number,
                                                          p_data_saldo         in date,
                                                          p_nivel              in varchar2,
                                                          p_grupo              in varchar2,
                                                          p_subgrupo           in varchar2,
                                                          p_item               in varchar2,
                                                          p_gravar_maior_venda in varchar2,
                                                          p_tipo_val           in number, -- 1 Estagio, 2 Deposito, 3 Semi-acabado (fixo)
                                                          p_dep_est            in number)
return number is
   v_valor_aux               number;
   v_valor_nota              number;
   v_data_saldo              date;
   v_nota                    number;
   v_serie                   varchar2(3);
   v_data                    date;
   v_tipo_valorizacao        number;
   v_tipo_produto            number;
   v_mes                     number;
   v_ano                     number;

   v_perc_produto_fab        fatu_503.perc_produto_fab%type;
   v_perc_semi_acab          fatu_503.perc_semi_acab%type;

   v_nivel_acab              basi_010.nivel_estrutura%type;
   v_grupo_acab              basi_010.grupo_estrutura%type;
   v_sub_acab                basi_010.subgru_estrutura%type;
   v_item_acab               basi_010.item_estrutura%type;
   v_preco_contratipo        basi_010.preco_contratipo%type;
   v_origem_preco_informado  fatu_504.origem_preco_informado%type;
begin
   -- inicializa variaveis
   v_valor_aux   := 0.00;
   v_valor_nota  := 0.00;
   v_data_saldo  := last_day(p_data_saldo);

   v_ano         := to_number(to_char(p_data_saldo, 'YYYY'));
   v_mes         := to_number(to_char(p_data_saldo, 'MM'));

   -- le a empresa para verificar o tipo do custo utilizado (real ou previsto)
   begin
      select perc_produto_fab,   perc_semi_acab
      into v_perc_produto_fab, v_perc_semi_acab
      from fatu_503
      where codigo_empresa  = p_empresa;

      exception
         when others then
            v_perc_produto_fab := 0.00;
            v_perc_semi_acab   := 0.00;
   end;

   -- verifica se o produto e um "acabado" ou  "semi-acabado"
   if p_tipo_val = 3 --  fixo -> semi-acabado. Passado diretamente como parametro da funcao
   then
      v_tipo_valorizacao := 2;
   else

      if p_tipo_val = 1 -- Procurar tipo de valorizacao no Estagio
      then
         begin
            select mqop_005.tipo_valorizacao into v_tipo_valorizacao
            from mqop_005
            where mqop_005.codigo_estagio = p_dep_est;

         exception
            when others then
               v_tipo_valorizacao := 2;
         end;
      else              -- Procurar tipo de valorizacao no Deposito

         begin
            -- altera os codigos do tipo em valorizacao para ficar igual ao do estagio
            -- ficara assim: 2- Semi-acabados e 1- Em Elaboracao
            select  decode(basi_205.tipo_valorizacao, 3, 2, 2, 1, basi_205.tipo_valorizacao)
            into v_tipo_valorizacao
            from basi_205
            where basi_205.codigo_deposito = p_dep_est;

         exception
            when others then
               v_tipo_valorizacao := 2;
         end;
      end if;
   end if;


   -- se o produto for semi-acabado
   if v_tipo_valorizacao = 2
   then

      -- le a tabela "de/para" de produtos semi-acabados
      begin
         select decode(nivel_acab, 'X',      p_nivel,    nivel_acab),
                decode(grupo_acab, 'XXXXX',  p_grupo,    grupo_acab),
                decode(sub_acab,   'XXX',    p_subgrupo, sub_acab),
                decode(item_acab,  'XXXXXX', p_item,     item_acab)
         into   v_nivel_acab,
                v_grupo_acab,
                v_sub_acab,
                v_item_acab
         from rcnb_390
         where (nivel_semi = p_nivel     or nivel_semi = 'X')
         and   (grupo_semi = p_grupo     or grupo_semi = 'XXXXX')
         and   (sub_semi   = p_subgrupo  or sub_semi   = 'XXX')
         and   (item_semi  = p_item      or item_semi  = 'XXXXXX');

      exception
         when others then
            v_nivel_acab := p_nivel;
            v_grupo_acab := p_grupo;
            v_sub_acab   := p_subgrupo;
            v_item_acab  := p_item;
      end;

      -- le a tabela de faturamento para encontrar o mair valor de venda
      begin

         select valor_unitario,
                num_nota_fiscal,
                serie_nota_fisc,
                data_emissao
         into   v_valor_aux,
                v_nota,
                v_serie,
                v_data
         from (

            select nvl(fatu_060.valor_unitario, 0.00) VALOR_UNITARIO,
                   fatu_050.num_nota_fiscal,
                   fatu_050.serie_nota_fisc,
                   fatu_050.data_emissao
            from fatu_050, fatu_060, estq_005
            where  fatu_050.codigo_empresa        = p_empresa
              and  fatu_050.data_emissao         <= v_data_saldo
              and  fatu_050.situacao_nfisc       <> 2
              and (fatu_050.considera_inventario  = 0 or fatu_050.considera_inventario is null)
              and  fatu_060.ch_it_nf_cd_empr      = fatu_050.codigo_empresa
              and  fatu_060.ch_it_nf_num_nfis     = fatu_050.num_nota_fiscal
              and  fatu_060.ch_it_nf_ser_nfis     = fatu_050.serie_nota_fisc

              and (fatu_060.nivel_estrutura       = v_nivel_acab or v_nivel_acab = 'Z')
              and (fatu_060.grupo_estrutura       = v_grupo_acab or v_grupo_acab = 'ZZZZZ')
              and (fatu_060.subgru_estrutura      = v_sub_acab   or v_sub_acab   = 'ZZZ')
              and (fatu_060.item_estrutura        = v_item_acab  or v_item_acab  = 'ZZZZZZ')

              /*NOTAS DE VENDAS*/
              and fatu_060.transacao              = estq_005.codigo_transacao
              and estq_005.tipo_transacao         = 'V'
            order by fatu_060.valor_unitario desc)
         where rownum = 1;

      exception
         when others then
            v_valor_aux := 0.00;
            v_nota      := 0   ;
            v_serie     := ''  ;
            v_data      := null;
      end;
   else

      begin

         select valor_unitario,
                num_nota_fiscal,
                serie_nota_fisc,
                data_emissao
         into   v_valor_aux,
                v_nota,
                v_serie,
                v_data
         from (

            select nvl(fatu_060.valor_unitario, 0.00) VALOR_UNITARIO,
                   fatu_050.num_nota_fiscal,
                   fatu_050.serie_nota_fisc,
                   fatu_050.data_emissao
            from fatu_050, fatu_060, estq_005
            where fatu_050.codigo_empresa         = p_empresa
              and fatu_050.data_emissao          <= v_data_saldo
              and fatu_050.situacao_nfisc        <> 2
              and (fatu_050.considera_inventario  = 0 or fatu_050.considera_inventario is null)
              and fatu_060.ch_it_nf_cd_empr       = fatu_050.codigo_empresa
              and fatu_060.ch_it_nf_num_nfis      = fatu_050.num_nota_fiscal
              and fatu_060.ch_it_nf_ser_nfis      = fatu_050.serie_nota_fisc
              and fatu_060.nivel_estrutura        = p_nivel
              and fatu_060.grupo_estrutura        = p_grupo
              and fatu_060.subgru_estrutura       = p_subgrupo
              and fatu_060.item_estrutura         = p_item
              /*NOTAS DE VENDAS*/
              and fatu_060.transacao              = estq_005.codigo_transacao
              and estq_005.tipo_transacao         = 'V'
            order by fatu_060.valor_unitario desc)
         where rownum = 1;

      exception
         when others then
            v_valor_aux := 0.00;
            v_nota      := 0   ;
            v_serie     := ''  ;
            v_data      := null;
      end;
   end if;

   if v_valor_aux = 0.00
   then

      begin
         select basi_010.preco_custo_info, basi_010.preco_contratipo
         into   v_valor_aux,               v_preco_contratipo
         from basi_010
         where basi_010.nivel_estrutura  = p_nivel
         and   basi_010.grupo_estrutura  = p_grupo
         and   basi_010.subgru_estrutura = p_subgrupo
         and   basi_010.item_estrutura   = p_item;

      exception
         when others then
            v_valor_aux := 0.00;
            v_preco_contratipo := 0.00;
      end;
   end if;

   begin
      select fatu_504.origem_preco_informado
      into   v_origem_preco_informado
      from fatu_504
      where fatu_504.codigo_empresa = p_empresa;
   exception
       when no_data_found then
          v_origem_preco_informado := 0;
   end;

   if v_origem_preco_informado = 1
   then
      v_valor_aux := v_preco_contratipo;
   end if;

   v_valor_nota := v_valor_aux;
   if v_tipo_valorizacao = 2
   then
      v_valor_aux    := round(v_valor_aux * v_perc_semi_acab / 100.00, 5);
      v_tipo_produto := 2;
   else

      if v_tipo_valorizacao = 4
      then
         v_valor_aux    := round(v_valor_aux * v_perc_produto_fab / 100.00, 5);
         v_tipo_produto := 1;
      else

         v_valor_aux    := 0.00;
         v_tipo_produto := 1;
      end if;
   end if;

   if p_gravar_maior_venda = 'S' and v_valor_aux > 0.00000
   then
      begin

         inter_pr_gravar_maior_venda (p_empresa,
                                      p_nivel,
                                      p_grupo,
                                      p_subgrupo,
                                      p_item,
                                      v_tipo_produto,
                                      v_mes,
                                      v_ano,
                                      v_nota,
                                      v_serie,
                                      v_data,
                                      v_valor_aux,
                                      v_valor_nota);
      end;
   end if;

   return(v_valor_aux);
end inter_fn_busca_maior_prc_venda;






 

/

exec inter_pr_recompile;

