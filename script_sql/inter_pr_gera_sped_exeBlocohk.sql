create or replace procedure inter_pr_gera_sped_exeBlocohk(p_cod_empresa   in number,
                                                     p_dat_final     IN DATE,
                                                     p_des_erro    out varchar2) is


w_erro    EXCEPTION;


BEGIN

      BEGIN
          inter_pr_gera_sped_hkcardex(p_cod_empresa, p_dat_final, p_des_erro);
          IF p_des_erro != NULL AND p_des_erro != '' THEN
			RAISE W_ERRO;
		  END IF;
      EXCEPTION
        WHEN W_ERRO THEN
            RAISE W_ERRO;
        WHEN OTHERS THEN
          p_des_erro := 'Erro na execucao da procedure inter_pr_gera_sped_hkcardex' || Chr(10) ||
                        SQLERRM;
          RAISE W_ERRO;
      END;
      BEGIN
          inter_pr_gera_sped_hkterceiNf(p_cod_empresa, p_dat_final, p_des_erro);
          IF p_des_erro != NULL AND p_des_erro != '' THEN
			RAISE W_ERRO;
		  END IF;
      EXCEPTION
        WHEN W_ERRO THEN
          RAISE W_ERRO;
        WHEN OTHERS THEN
          p_des_erro := 'Erro na execucao da procedure inter_pr_gera_sped_hkterceiNf' || Chr(10) ||
                        SQLERRM;
          RAISE W_ERRO;
      END;
      BEGIN
          inter_pr_gera_sped_hkestqProp(p_cod_empresa, p_dat_final, p_des_erro);
          IF p_des_erro != NULL AND p_des_erro != '' THEN
			RAISE W_ERRO;
		  END IF; 
      EXCEPTION
        WHEN W_ERRO THEN
          RAISE W_ERRO;
        WHEN OTHERS THEN
          p_des_erro := 'Erro na execucao da procedure inter_pr_gera_sped_hkestqProp' || Chr(10) ||
                        SQLERRM;
          RAISE W_ERRO;

      END;

EXCEPTION
WHEN W_ERRO then
   p_des_erro := 'Erro na procedure inter_pr_gera_sped_exeBlocohk ' || Chr(10) || p_des_erro;
WHEN OTHERS THEN
   p_des_erro := 'Outros erros na procedure inter_pr_gera_sped_exeBlocohk ' || Chr(10) || SQLERRM;
END inter_pr_gera_sped_exeBlocohk;
