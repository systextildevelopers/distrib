CREATE OR REPLACE FUNCTION inter_fn_busca_cfop(
    p_natur_operacao IN NUMBER,
    p_estado_natoper IN VARCHAR2
) RETURN VARCHAR2
AS
    v_cfop VARCHAR2(10);
BEGIN
    SELECT p.cod_natureza || p.divisao_natur
    INTO v_cfop
    FROM pedi_080 p
    WHERE p.natur_operacao = p_natur_operacao
    AND p.estado_natoper = p_estado_natoper;

    RETURN v_cfop;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
END;
/
