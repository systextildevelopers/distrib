update obrf_601
set obrf_601.instrucao = 'Deve-se verificar na NF-e os campos que foram preenchidos com caracteres especiais espa�os em branco no in�cio e/ou fim das informa��es dos campos e quebras de linhas e remove-las ou substitu�-las. Todos os campos que aceitam como valor qualquer tipo de caractere ou textos est�o sujeitos a essa rejei��o.

S�o exemplos de caracteres especiais que podem ocasionar a rejei��o: < > & � � � � � � �

Processar o arquivo XML no validador da Receita Federal. (https://www.sefaz.rs.gov.br/nfe/NFE-VAL.aspx)'
where obrf_601.cod_status = 297 ;
