CREATE OR REPLACE PROCEDURE pr_basi_240_basi_062 IS
BEGIN
     BEGIN
    for reg_basi_062 in (select basi_062.NCM
                                from basi_062
                                where basi_062.DATA_INICIAL <= trunc(SYSDATE)
                                and basi_062.DATA_FINAL >= trunc(SYSDATE))
      loop
          BEGIN
            UPDATE basi_240
            set considera_ttd = 1
            where basi_240.classific_fiscal = reg_basi_062.NCM;
            EXCEPTION when OTHERS then raise_application_error (-20000, 'Nao atualizou a tabela de Classificação Fiscal(basi_240)');
          END;
      end loop;
      COMMIT;
  END;

  BEGIN
    for reg_basi_062 in (select basi_062.NCM
                                from basi_062
                                where basi_062.DATA_INICIAL > trunc(SYSDATE)
                                and basi_062.DATA_FINAL < trunc(SYSDATE))
      loop
          BEGIN
            UPDATE basi_240
            set considera_ttd = 0
            where basi_240.classific_fiscal = reg_basi_062.NCM;
          EXCEPTION when OTHERS then
            raise_application_error (-20000, 'Nao atualizou a tabela de Classificação Fiscal(basi_240)');
        END;
      end loop;
      COMMIT;
  END;
END pr_basi_240_basi_062;
