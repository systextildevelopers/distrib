
  CREATE OR REPLACE PROCEDURE "INTER_PR_CALCULA_2A_PERDA" (
   v_qtde_2a           out number,
   v_qtde_perda        out number,
   v_periodo_producao  in number,
   v_ordem_confeccao   in number,
   v_ordem_producao    in number,
   v_estagio_depende   in number,
   v_sequencia_estagio in number
)
is
   v_qtde_segunda_a            number;
   v_qtde_segunda_b            number;
   v_qtde_segunda_b1           number;
   v_qtde_segunda_b2           number;
   v_qtde_segunda_b3           number;
   v_qtde_segunda_b4           number;
   v_qtde_segunda_b5           number;

   v_qtde_perda_a              number;
   v_qtde_perda_b              number;
   v_qtde_perda_b1             number;
   v_qtde_perda_b2             number;
   v_qtde_perda_b3             number;
   v_qtde_perda_b4             number;
   v_qtde_perda_b5             number;

begin
   /*
      Formula:
               v_qtde_segunda_a  = maior quantidade de segunda qualidade dos estagios sem estagio dependentes
               v_qtde_segunda_b1 = quantidade de segunda qualidade dos estagios que tem estagios dependentes
               v_qtde_segunda_b2 = quantidade de segunda qualidade do 1 estagio dependentes
               v_qtde_segunda_b3 = quantidade de segunda qualidade do 2 estagio dependentes
               v_qtde_segunda_b5 = Soma das quantidades de segunda qualidade dos estagios dependentes
               v_qtde_segunda_b  = soma da quantidade dos estagios que tem estagios dependentes

      v_qtde_segunda_a + v_qtde_segunda_b1 + v_qtde_segunda_b5

   */

   -- Dos estagios que sao simultaneos, deve buscar a maior segunda qualidade dos estagios que nao tem dependentes
   select nvl(max(pcpc_040.qtde_pecas_2a),0),   nvl(max(pcpc_040.qtde_perdas),0)
   into   v_qtde_segunda_a,                      v_qtde_perda_a
   from pcpc_040
   where pcpc_040.periodo_producao  = v_periodo_producao
     and pcpc_040.ordem_confeccao   = v_ordem_confeccao
     and pcpc_040.ordem_producao    = v_ordem_producao
     and pcpc_040.estagio_depende   = v_estagio_depende
     and pcpc_040.sequencia_estagio = v_sequencia_estagio
     and not exists(select 1
                    from pcpc_040 pcpc_040a
                    where pcpc_040a.periodo_producao  = pcpc_040.periodo_producao
                      and pcpc_040a.ordem_confeccao   = pcpc_040.ordem_confeccao
                      and pcpc_040a.estagio_depende   = pcpc_040.codigo_estagio
                      and pcpc_040a.ordem_producao    = pcpc_040.ordem_producao);

   v_qtde_segunda_b := 0;
   v_qtde_perda_b   := 0;

   -- Dos estagios que sao simultaneos, deve calcular a maior segunda qualidade dos estagios que tem dependentes
   for reg_qtde_b1 in (select pcpc_040.codigo_estagio
                       from pcpc_040
                       where pcpc_040.periodo_producao  = v_periodo_producao
                         and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                         and pcpc_040.ordem_producao    = v_ordem_producao
                         and pcpc_040.estagio_depende   = 0
                         and pcpc_040.sequencia_estagio =  v_sequencia_estagio
                         and exists (select 1
                                     from pcpc_040 pcpc_040a
                                     where pcpc_040a.periodo_producao  = pcpc_040.periodo_producao
                                       and pcpc_040a.ordem_confeccao   = pcpc_040.ordem_confeccao
                                       and pcpc_040a.estagio_depende   = pcpc_040.codigo_estagio
                                       and pcpc_040a.ordem_producao    = pcpc_040.ordem_producao))
   loop
      begin
         select nvl(max(pcpc_040.qtde_pecas_2a),0),     nvl(max(pcpc_040.qtde_perdas),0)
         into   v_qtde_segunda_b1,                      v_qtde_perda_b1
         from pcpc_040
         where pcpc_040.periodo_producao  = v_periodo_producao
           and pcpc_040.ordem_confeccao   = v_ordem_confeccao
           and pcpc_040.codigo_estagio    = reg_qtde_b1.codigo_estagio
           and pcpc_040.ordem_producao    = v_ordem_producao
           and pcpc_040.sequencia_estagio = v_sequencia_estagio;
      end;

      v_qtde_segunda_b5 := 0;
      v_qtde_segunda_b2 := 0;

      v_qtde_perda_b5   := 0;
      v_qtde_perda_b2   := 0;

      -- Dos estagios que sao simultaneos e dependentes, busca a maior segunda qualidade do primeiro estagio dependentes
      for reg_filho_qtde_b2 in (select pcpc_040.codigo_estagio
                                from pcpc_040
                                where pcpc_040.periodo_producao  = v_periodo_producao
                                  and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                                  and pcpc_040.ordem_producao    = v_ordem_producao
                                  and pcpc_040.estagio_depende   = reg_qtde_b1.codigo_estagio
                                  and pcpc_040.sequencia_estagio = v_sequencia_estagio)
      loop
         begin
            select nvl(max(pcpc_040.qtde_pecas_2a),0),    nvl(max(pcpc_040.qtde_perdas),0)
            into   v_qtde_segunda_b2,                     v_qtde_perda_b2
            from pcpc_040
            where pcpc_040.periodo_producao  = v_periodo_producao
              and pcpc_040.ordem_confeccao   = v_ordem_confeccao
              and pcpc_040.codigo_estagio    = reg_filho_qtde_b2.codigo_estagio
              and pcpc_040.ordem_producao    = v_ordem_producao
              and pcpc_040.sequencia_estagio = v_sequencia_estagio;
         end;

         v_qtde_segunda_b3 := 0;
         v_qtde_segunda_b4 := 0;

         v_qtde_perda_b3 := 0;
         v_qtde_perda_b4 := 0;

         -- Dos estagios que sao simultaneos e dependentes, busca a maior segunda qualidade do segundo estagio dependente
         for reg_filho_qtde_b3 in (select pcpc_040.codigo_estagio
                                   from pcpc_040
                                   where pcpc_040.periodo_producao  = v_periodo_producao
                                     and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                                     and pcpc_040.estagio_depende   = reg_filho_qtde_b2.codigo_estagio
                                     and pcpc_040.ordem_producao    = v_ordem_producao
                                     and pcpc_040.sequencia_estagio = v_sequencia_estagio)
         loop
            begin
               select nvl(max(pcpc_040.qtde_pecas_2a),0),  nvl(max(pcpc_040.qtde_perdas),0)
               into   v_qtde_segunda_b3,                   v_qtde_perda_b3
               from pcpc_040
               where pcpc_040.periodo_producao  = v_periodo_producao
                 and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                 and pcpc_040.codigo_estagio    = reg_filho_qtde_b3.codigo_estagio
                 and pcpc_040.ordem_producao    = v_ordem_producao
                 and pcpc_040.sequencia_estagio = v_sequencia_estagio;
           end;

           v_qtde_segunda_b4 := v_qtde_segunda_b4 + v_qtde_segunda_b3;
           v_qtde_perda_b4   := v_qtde_perda_b4   + v_qtde_perda_b3;
         end loop;

         if v_qtde_segunda_b5 < v_qtde_segunda_b2 + v_qtde_segunda_b3
         then
            v_qtde_segunda_b5 := v_qtde_segunda_b2 + v_qtde_segunda_b3;
         end if;

         if v_qtde_perda_b5 < v_qtde_perda_b2 + v_qtde_perda_b3
         then
            v_qtde_perda_b5 := v_qtde_perda_b2 + v_qtde_perda_b3;
         end if;
      end loop;

      if v_qtde_segunda_b < v_qtde_segunda_b5 + v_qtde_segunda_b1
      then
         v_qtde_segunda_b := v_qtde_segunda_b5 + v_qtde_segunda_b1;
      end if;

      if v_qtde_perda_b < v_qtde_perda_b5 + v_qtde_perda_b1
      then
         v_qtde_perda_b := v_qtde_perda_b5 + v_qtde_perda_b1;
      end if;
   end loop;

   if v_qtde_segunda_a > v_qtde_segunda_b
   then
      v_qtde_2a := v_qtde_segunda_a;
   else
      v_qtde_2a := v_qtde_segunda_b;
   end if;

   if v_qtde_perda_a > v_qtde_perda_b
   then
      v_qtde_perda := v_qtde_perda_a;
   else
      v_qtde_perda := v_qtde_perda_b;
   end if;
end inter_pr_calcula_2a_perda;

 

/

exec inter_pr_recompile;

