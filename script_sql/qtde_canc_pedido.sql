
  CREATE OR REPLACE FUNCTION "QTDE_CANC_PEDIDO" 
        (pedido_informado in pedi_100.pedido_venda%type)
RETURN number
IS
   saldo_canc_pedido number;
begin
   select nvl(sum(pedi_110.qtde_pedida-pedi_110.qtde_faturada),0) into saldo_canc_pedido
   from pedi_100,pedi_110
   where pedi_110.pedido_venda = pedido_informado
     and pedi_100.pedido_venda = pedi_110.pedido_venda
     and pedi_110.cod_cancelamento > 0;

   return(saldo_canc_pedido);
end qtde_canc_pedido;

 

/

exec inter_pr_recompile;

