insert into hdoc_035 (
    codigo_programa, descricao, programa_menu, item_menu_def
) values (
    'pcpc_f052', 'Dividir Pacote', 0,1
);

insert into hdoc_033 (
    usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar
) values (
    'INTERSYS', 1, 'pcpc_f052', 'pcpc_menu', 0, 20, 'S', 'S', 'S', 'S'
);

update hdoc_036
   set hdoc_036.descricao       = 'Dividir Pacote'
 where hdoc_036.codigo_programa = 'pcpc_f052'
   and hdoc_036.locale          = 'es_ES';
commit;
