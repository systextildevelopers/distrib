create or replace trigger inter_tr_oper_tmp_data_criacao
   before insert on oper_tmp
FOR EACH ROW
begin
  if :new.data_criacao is null 
  then :new.data_criacao := sysdate;
  end if;
end inter_tr_oper_tmp_data_criacao;
