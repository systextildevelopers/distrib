create table oper_283 (
  cod_empresa    number(3)          default 0,
  cfop_remessa   varchar2(7 byte)   default ' ',
  cfop_retorno   varchar2(7 byte)   default ' ');


alter table oper_283 add constraint pk_oper_283 primary key(cod_empresa, cfop_remessa,cfop_retorno);
