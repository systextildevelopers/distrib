
  CREATE OR REPLACE TRIGGER "LOJA_TR_BASI_020" 
BEFORE  UPDATE
of descr_tam_refer


on basi_020
for each row

begin

  if  updating and :new.descr_tam_refer <> :old.descr_tam_refer
  then
     BEGIN
        update basi_010
           set basi_010.flag_exportacao_loja = 0
        where basi_010.nivel_estrutura  = :new.basi030_nivel030
          and basi_010.grupo_estrutura  = :new.basi030_referenc
          and basi_010.subgru_estrutura = :new.tamanho_ref;
     EXCEPTION
        WHEN OTHERS THEN
        raise_application_error (-20000, 'Erro na atualizacao da tabela basi_010, executado na basi_020. ' || SQLERRM);
     END;

  end if;


end;
-- ALTER TRIGGER "LOJA_TR_BASI_020" ENABLE
 

/

exec inter_pr_recompile;

