alter table obrf_158
add resp_tec_telefone_novo varchar2(14);

update obrf_158
set resp_tec_telefone_novo = to_char(resp_tec_telefone); commit;

alter table obrf_158
drop column resp_tec_telefone;

alter table obrf_158
add resp_tec_telefone varchar2(14);

update obrf_158
set resp_tec_telefone = resp_tec_telefone_novo; commit;

alter table obrf_158
drop column resp_tec_telefone_novo;

exec inter_pr_recompile;
