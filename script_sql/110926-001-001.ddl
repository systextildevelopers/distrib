alter table CREC_730
add NR_DIAS_PAGAMENTO number(2) default 0;

comment on column CREC_730.NR_DIAS_PAGAMENTO is 'Númerdo de dias para pagar o título antes de se negativar';
