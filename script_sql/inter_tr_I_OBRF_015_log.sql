create or replace trigger inter_tr_I_OBRF_015_log 
after insert or delete or update 
on I_OBRF_015 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(20) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 

	v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 if inserting 
 then 
    begin 
 
        insert into I_OBRF_015_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           id_registro_OLD,   /*8*/ 
           id_registro_NEW,   /*9*/ 
           flag_importacao_OLD,   /*10*/ 
           flag_importacao_NEW,   /*11*/ 
           tipo_atualizacao_OLD,   /*12*/ 
           tipo_atualizacao_NEW,   /*13*/ 
           data_exportacao_OLD,   /*14*/ 
           data_exportacao_NEW,   /*15*/ 
           data_importacao_OLD,   /*16*/ 
           data_importacao_NEW,   /*17*/ 
           capa_ent_nrdoc_OLD,   /*18*/ 
           capa_ent_nrdoc_NEW,   /*19*/ 
           capa_ent_serie_OLD,   /*20*/ 
           capa_ent_serie_NEW,   /*21*/ 
           capa_ent_forcli9_OLD,   /*22*/ 
           capa_ent_forcli9_NEW,   /*23*/ 
           capa_ent_forcli4_OLD,   /*24*/ 
           capa_ent_forcli4_NEW,   /*25*/ 
           capa_ent_forcli2_OLD,   /*26*/ 
           capa_ent_forcli2_NEW,   /*27*/ 
           sequencia_OLD,   /*28*/ 
           sequencia_NEW,   /*29*/ 
           coditem_nivel99_OLD,   /*30*/ 
           coditem_nivel99_NEW,   /*31*/ 
           coditem_grupo_OLD,   /*32*/ 
           coditem_grupo_NEW,   /*33*/ 
           coditem_subgrupo_OLD,   /*34*/ 
           coditem_subgrupo_NEW,   /*35*/ 
           coditem_item_OLD,   /*36*/ 
           coditem_item_NEW,   /*37*/ 
           quantidade_OLD,   /*38*/ 
           quantidade_NEW,   /*39*/ 
           lote_entrega_OLD,   /*40*/ 
           lote_entrega_NEW,   /*41*/ 
           valor_unitario_OLD,   /*42*/ 
           valor_unitario_NEW,   /*43*/ 
           unidade_medida_OLD,   /*44*/ 
           unidade_medida_NEW,   /*45*/ 
           valor_ipi_OLD,   /*46*/ 
           valor_ipi_NEW,   /*47*/ 
           valor_total_OLD,   /*48*/ 
           valor_total_NEW,   /*49*/ 
           classific_fiscal_OLD,   /*50*/ 
           classific_fiscal_NEW,   /*51*/ 
           classif_contabil_OLD,   /*52*/ 
           classif_contabil_NEW,   /*53*/ 
           cod_vlfiscal_ipi_OLD,   /*54*/ 
           cod_vlfiscal_ipi_NEW,   /*55*/ 
           pedido_compra_OLD,   /*56*/ 
           pedido_compra_NEW,   /*57*/ 
           sequencia_pedido_OLD,   /*58*/ 
           sequencia_pedido_NEW,   /*59*/ 
           descricao_item_OLD,   /*60*/ 
           descricao_item_NEW,   /*61*/ 
           percentual_ipi_OLD,   /*62*/ 
           percentual_ipi_NEW,   /*63*/ 
           centro_custo_OLD,   /*64*/ 
           centro_custo_NEW,   /*65*/ 
           codigo_deposito_OLD,   /*66*/ 
           codigo_deposito_NEW,   /*67*/ 
           preco_custo_OLD,   /*68*/ 
           preco_custo_NEW,   /*69*/ 
           natitem_nat_oper_OLD,   /*70*/ 
           natitem_nat_oper_NEW,   /*71*/ 
           natitem_est_oper_OLD,   /*72*/ 
           natitem_est_oper_NEW,   /*73*/ 
           cod_vlfiscal_icm_OLD,   /*74*/ 
           cod_vlfiscal_icm_NEW,   /*75*/ 
           valor_icms_OLD,   /*76*/ 
           valor_icms_NEW,   /*77*/ 
           codigo_transacao_OLD,   /*78*/ 
           codigo_transacao_NEW,   /*79*/ 
           percentual_icm_OLD,   /*80*/ 
           percentual_icm_NEW,   /*81*/ 
           base_calc_icm_OLD,   /*82*/ 
           base_calc_icm_NEW,   /*83*/ 
           base_diferenca_OLD,   /*84*/ 
           base_diferenca_NEW,   /*85*/ 
           cvf_icm_diferenc_OLD,   /*86*/ 
           cvf_icm_diferenc_NEW,   /*87*/ 
           procedencia_OLD,   /*88*/ 
           procedencia_NEW,   /*89*/ 
           rateio_despesas_OLD,   /*90*/ 
           rateio_despesas_NEW,   /*91*/ 
           dif_aliquota_OLD,   /*92*/ 
           dif_aliquota_NEW,   /*93*/ 
           perc_dif_aliq_OLD,   /*94*/ 
           perc_dif_aliq_NEW,   /*95*/ 
           base_ipi_OLD,   /*96*/ 
           base_ipi_NEW,   /*97*/ 
           valor_pis_OLD,   /*98*/ 
           valor_pis_NEW,   /*99*/ 
           valor_cofins_OLD,   /*100*/ 
           valor_cofins_NEW,   /*101*/ 
           base_pis_cofins_OLD,   /*102*/ 
           base_pis_cofins_NEW,   /*103*/ 
           codigo_contabil_OLD,   /*104*/ 
           codigo_contabil_NEW,   /*105*/ 
           obs_livro1_OLD,   /*106*/ 
           obs_livro1_NEW,   /*107*/ 
           obs_livro2_OLD,   /*108*/ 
           obs_livro2_NEW,   /*109*/ 
           rateio_despesas_ipi_OLD,   /*110*/ 
           rateio_despesas_ipi_NEW,   /*111*/ 
           rateio_descontos_ipi_OLD,   /*112*/ 
           rateio_descontos_ipi_NEW,   /*113*/ 
           natureza_str_OLD,   /*114*/ 
           natureza_str_NEW,   /*115*/ 
           cvf_pis_OLD,   /*116*/ 
           cvf_pis_NEW,   /*117*/ 
           cvf_cofins_OLD,   /*118*/ 
           cvf_cofins_NEW,   /*119*/ 
           perc_pis_OLD,   /*120*/ 
           perc_pis_NEW,   /*121*/ 
           perc_cofins_OLD,   /*122*/ 
           perc_cofins_NEW,   /*123*/ 
           cvf_ipi_entrada_OLD,   /*124*/ 
           cvf_ipi_entrada_NEW,   /*125*/ 
           base_icms_subs_OLD,   /*126*/ 
           base_icms_subs_NEW,   /*127*/ 
           perc_icms_subs_OLD,   /*128*/ 
           perc_icms_subs_NEW,   /*129*/ 
           valor_icms_subs_OLD,   /*130*/ 
           valor_icms_subs_NEW,   /*131*/ 
           num_nota_orig_OLD,   /*132*/ 
           num_nota_orig_NEW,   /*133*/ 
           seq_nota_orig_OLD,   /*134*/ 
           seq_nota_orig_NEW,   /*135*/ 
           motivo_devolucao_OLD,   /*136*/ 
           motivo_devolucao_NEW,   /*137*/ 
           serie_nota_orig_OLD,   /*138*/ 
           serie_nota_orig_NEW,   /*139*/ 
           flag_devolucao_OLD,   /*140*/ 
           flag_devolucao_NEW,   /*141*/ 
           empr_nf_saida_OLD,   /*142*/ 
           empr_nf_saida_NEW,   /*143*/ 
           num_nf_saida_OLD,   /*144*/ 
           num_nf_saida_NEW,   /*145*/ 
           serie_nf_saida_OLD,   /*146*/ 
           serie_nf_saida_NEW,   /*147*/ 
           perc_icms_diferido_OLD,   /*148*/ 
           perc_icms_diferido_NEW,   /*149*/ 
           valor_icms_diferido_OLD,   /*150*/ 
           valor_icms_diferido_NEW,   /*151*/ 
           data_canc_nfisc_OLD,   /*152*/ 
           data_canc_nfisc_NEW,   /*153*/ 
           transacao_canc_nfisc_OLD,   /*154*/ 
           transacao_canc_nfisc_NEW,   /*155*/ 
           tipo_doc_importacao_sped_OLD,   /*156*/ 
           tipo_doc_importacao_sped_NEW,   /*157*/ 
           valor_pis_import_sped_OLD,   /*158*/ 
           valor_pis_import_sped_NEW,   /*159*/ 
           valor_cofins_import_sped_OLD,   /*160*/ 
           valor_cofins_import_sped_NEW,   /*161*/ 
           data_desembaraco_sped_OLD,   /*162*/ 
           data_desembaraco_sped_NEW,   /*163*/ 
           valor_cif_sped_OLD,   /*164*/ 
           valor_cif_sped_NEW,   /*165*/ 
           valor_desp_sem_icms_sped_OLD,   /*166*/ 
           valor_desp_sem_icms_sped_NEW,   /*167*/ 
           valor_desp_com_icms_sped_OLD,   /*168*/ 
           valor_desp_com_icms_sped_NEW,   /*169*/ 
           valor_imp_opera_finan_sped_OLD,   /*170*/ 
           valor_imp_opera_finan_sped_NEW,   /*171*/ 
           valor_imposto_import_sped_OLD,   /*172*/ 
           valor_imposto_import_sped_NEW,   /*173*/ 
           base_ipi_importacao_OLD,   /*174*/ 
           base_ipi_importacao_NEW,   /*175*/ 
           cvf_ipi_importacao_OLD,   /*176*/ 
           cvf_ipi_importacao_NEW,   /*177*/ 
           valor_ipi_importacao_OLD,   /*178*/ 
           valor_ipi_importacao_NEW,   /*179*/ 
           base_pis_cofins_importacao_OLD,   /*180*/ 
           base_pis_cofins_importacao_NEW,   /*181*/ 
           cvf_pis_importacao_OLD,   /*182*/ 
           cvf_pis_importacao_NEW,   /*183*/ 
           valor_pis_importacao_OLD,   /*184*/ 
           valor_pis_importacao_NEW,   /*185*/ 
           cvf_cofins_importacao_OLD,   /*186*/ 
           cvf_cofins_importacao_NEW,   /*187*/ 
           valor_cofins_importacao_OLD,   /*188*/ 
           valor_cofins_importacao_NEW,   /*189*/ 
           base_calculo_importacao_OLD,   /*190*/ 
           base_calculo_importacao_NEW,   /*191*/ 
           valor_imposto_importacao_OLD,   /*192*/ 
           valor_imposto_importacao_NEW,   /*193*/ 
           valor_iof_importacao_OLD,   /*194*/ 
           valor_iof_importacao_NEW,   /*195*/ 
           valor_desp_aduan_OLD,   /*196*/ 
           valor_desp_aduan_NEW,   /*197*/ 
           perc_ipi_importacao_OLD,   /*198*/ 
           perc_ipi_importacao_NEW,   /*199*/ 
           perc_pis_importacao_OLD,   /*200*/ 
           perc_pis_importacao_NEW,   /*201*/ 
           perc_cofins_importacao_OLD,   /*202*/ 
           perc_cofins_importacao_NEW,   /*203*/ 
           cod_csosn_OLD,   /*204*/ 
           cod_csosn_NEW,   /*205*/ 
           valor_frete_OLD,   /*206*/ 
           valor_frete_NEW,   /*207*/ 
           valor_desc_OLD,   /*208*/ 
           valor_desc_NEW,   /*209*/ 
           valor_outros_OLD,   /*210*/ 
           valor_outros_NEW,   /*211*/ 
           valor_seguro_OLD,   /*212*/ 
           valor_seguro_NEW,   /*213*/ 
           numero_adicao_OLD,   /*214*/ 
           numero_adicao_NEW,   /*215*/ 
           cod_base_cred_OLD,   /*216*/ 
           cod_base_cred_NEW,   /*217*/ 
           desconto_item_OLD,   /*218*/ 
           desconto_item_NEW,   /*219*/ 
           flag_saida_OLD,   /*220*/ 
           flag_saida_NEW,   /*221*/ 
           st_flag_cce_OLD,   /*222*/ 
           st_flag_cce_NEW,   /*223*/ 
           cd_cnpj_forne_9_OLD,   /*224*/ 
           cd_cnpj_forne_9_NEW,   /*225*/ 
           cd_cnpj_forne_4_OLD,   /*226*/ 
           cd_cnpj_forne_4_NEW,   /*227*/ 
           cd_cnpj_forne_2_OLD,   /*228*/ 
           cd_cnpj_forne_2_NEW,   /*229*/ 
           cd_seq_nota_frete_OLD,   /*230*/ 
           cd_seq_nota_frete_NEW,   /*231*/ 
           qtd_pcs_caixa_rfid_OLD,   /*232*/ 
           qtd_pcs_caixa_rfid_NEW,   /*233*/ 
           numero_di_OLD,   /*234*/ 
           numero_di_NEW,   /*235*/ 
           data_declaracao_OLD,   /*236*/ 
           data_declaracao_NEW,   /*237*/ 
           local_desembaraco_OLD,   /*238*/ 
           local_desembaraco_NEW,   /*239*/ 
           uf_desembaraco_OLD,   /*240*/ 
           uf_desembaraco_NEW,   /*241*/ 
           codigo_exportador_OLD,   /*242*/ 
           codigo_exportador_NEW,   /*243*/ 
           codigo_fabricante_OLD,   /*244*/ 
           codigo_fabricante_NEW,   /*245*/ 
           sequencia_adicao_OLD,   /*246*/ 
           sequencia_adicao_NEW,   /*247*/ 
           taxa_siscomex_imp_OLD,   /*248*/ 
           taxa_siscomex_imp_NEW,   /*249*/ 
           local_desembaraco_imp_OLD,   /*250*/ 
           local_desembaraco_imp_NEW,   /*251*/ 
           uf_desembaraco_imp_OLD,   /*252*/ 
           uf_desembaraco_imp_NEW,   /*253*/ 
           valor_aduaneiro_imp_OLD,   /*254*/ 
           valor_aduaneiro_imp_NEW,   /*255*/ 
           valor_ii_imp_OLD,   /*256*/ 
           valor_ii_imp_NEW,   /*257*/ 
           num_programa_emb_OLD,   /*258*/ 
           num_programa_emb_NEW,   /*259*/ 
           cest_OLD,   /*260*/ 
           cest_NEW,   /*261*/ 
           fator_conv_OLD,   /*262*/ 
           fator_conv_NEW,   /*263*/ 
           observacao_OLD,   /*264*/ 
           observacao_NEW,   /*265*/ 
           pdif51_OLD,   /*266*/ 
           pdif51_NEW,   /*267*/ 
           perc_fcp_uf_dest_OLD,   /*268*/ 
           perc_fcp_uf_dest_NEW,   /*269*/ 
           perc_icms_partilha_OLD,   /*270*/ 
           perc_icms_partilha_NEW,   /*271*/ 
           perc_icms_uf_dest_OLD,   /*272*/ 
           perc_icms_uf_dest_NEW,   /*273*/ 
           perc_redu_icm_OLD,   /*274*/ 
           perc_redu_icm_NEW,   /*275*/ 
           perc_redu_sub_OLD,   /*276*/ 
           perc_redu_sub_NEW,   /*277*/ 
           perc_substituicao_OLD,   /*278*/ 
           perc_substituicao_NEW,   /*279*/ 
           quantidade_conv_OLD,   /*280*/ 
           quantidade_conv_NEW,   /*281*/ 
           unidade_conv_OLD,   /*282*/ 
           unidade_conv_NEW,   /*283*/ 
           valor_afrmm_OLD,   /*284*/ 
           valor_afrmm_NEW,   /*285*/ 
           valor_anti_dump_OLD,   /*286*/ 
           valor_anti_dump_NEW,   /*287*/ 
           valor_conv_OLD,   /*288*/ 
           valor_conv_NEW,   /*289*/ 
           valor_desp_acess_OLD,   /*290*/ 
           valor_desp_acess_NEW,   /*291*/ 
           valor_desp_ntrib_OLD,   /*292*/ 
           valor_desp_ntrib_NEW,   /*293*/ 
           val_fcp_uf_dest_OLD,   /*294*/ 
           val_fcp_uf_dest_NEW,   /*295*/ 
           val_icms_uf_dest_OLD,   /*296*/ 
           val_icms_uf_dest_NEW,   /*297*/ 
           val_icms_uf_remet_OLD,   /*298*/ 
           val_icms_uf_remet_NEW,   /*299*/ 
           vicmsdif51_OLD,   /*300*/ 
           vicmsdif51_NEW,   /*301*/ 
           vicmsop51_OLD,   /*302*/ 
           vicmsop51_NEW,   /*303*/ 
           produto_forn_OLD,   /*304*/ 
           produto_forn_NEW,   /*305*/ 
           descricao_item_forn_OLD,   /*306*/ 
           descricao_item_forn_NEW,   /*307*/ 
           quantidade_forn_OLD,   /*308*/ 
           quantidade_forn_NEW,   /*309*/ 
           unidade_medida_forn_OLD,   /*310*/ 
           unidade_medida_forn_NEW,   /*311*/ 
           valor_unitario_forn_OLD,   /*312*/ 
           valor_unitario_forn_NEW,   /*313*/ 
           valor_total_forn_OLD,   /*314*/ 
           valor_total_forn_NEW,   /*315*/ 
           cfop_OLD,   /*316*/ 
           cfop_NEW,   /*317*/ 
           cst_icms_xml_OLD,   /*318*/ 
           cst_icms_xml_NEW,   /*319*/ 
           perc_icms_xml_OLD,   /*320*/ 
           perc_icms_xml_NEW,   /*321*/ 
           orig_icms_xml_OLD,   /*322*/ 
           orig_icms_xml_NEW,   /*323*/ 
           base_calc_icms_xml_OLD,   /*324*/ 
           base_calc_icms_xml_NEW,   /*325*/ 
           valor_icms_xml_OLD,   /*326*/ 
           valor_icms_xml_NEW,   /*327*/ 
           perc_icms_subs_xml_OLD,   /*328*/ 
           perc_icms_subs_xml_NEW,   /*329*/ 
           base_icms_subs_xml_OLD,   /*330*/ 
           base_icms_subs_xml_NEW,   /*331*/ 
           valor_icms_subs_xml_OLD,   /*332*/ 
           valor_icms_subs_xml_NEW,   /*333*/ 
           base_pis_cofins_xml_OLD,   /*334*/ 
           base_pis_cofins_xml_NEW,   /*335*/ 
           perc_pis_xml_OLD,   /*336*/ 
           perc_pis_xml_NEW,   /*337*/ 
           cst_pis_xml_OLD,   /*338*/ 
           cst_pis_xml_NEW,   /*339*/ 
           valor_pis_xml_OLD,   /*340*/ 
           valor_pis_xml_NEW,   /*341*/ 
           perc_cofins_xml_OLD,   /*342*/ 
           perc_cofins_xml_NEW,   /*343*/ 
           cst_cofins_xml_OLD,   /*344*/ 
           cst_cofins_xml_NEW,   /*345*/ 
           valor_cofins_xml_OLD,   /*346*/ 
           valor_cofins_xml_NEW,   /*347*/ 
           perc_ipi_xml_OLD,   /*348*/ 
           perc_ipi_xml_NEW,   /*349*/ 
           cst_ipi_xml_OLD,   /*350*/ 
           cst_ipi_xml_NEW,   /*351*/ 
           base_ipi_xml_OLD,   /*352*/ 
           base_ipi_xml_NEW,   /*353*/ 
           valor_ipi_xml_OLD,   /*354*/ 
           valor_ipi_xml_NEW,   /*355*/ 
           ncm_xml_OLD,   /*356*/ 
           ncm_xml_NEW,   /*357*/ 
           flag_rateado_OLD,   /*358*/ 
           flag_rateado_NEW,   /*359*/ 
           sequencia_original_OLD,   /*360*/ 
           sequencia_original_NEW,   /*361*/ 
           qtde_original_OLD,   /*362*/ 
           qtde_original_NEW,   /*363*/ 
           situacao_liberacao_OLD,   /*364*/ 
           situacao_liberacao_NEW,   /*365*/ 
           tp_via_transp_OLD,   /*366*/ 
           tp_via_transp_NEW,   /*367*/ 
           ind_orig_cod_prod_OLD,   /*368*/ 
           ind_orig_cod_prod_NEW,   /*369*/ 
           conferencia_almox_OLD,   /*370*/ 
           conferencia_almox_NEW,   /*371*/ 
           projeto_OLD,   /*372*/ 
           projeto_NEW,   /*373*/ 
           subprojeto_OLD,   /*374*/ 
           subprojeto_NEW,   /*375*/ 
           servico_OLD,   /*376*/ 
           servico_NEW,   /*377*/ 
           codigo_ean_OLD,   /*378*/ 
           codigo_ean_NEW    /*379*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.id_registro, /*9*/   
           0,/*10*/
           :new.flag_importacao, /*11*/   
           '',/*12*/
           :new.tipo_atualizacao, /*13*/   
           null,/*14*/
           :new.data_exportacao, /*15*/   
           null,/*16*/
           :new.data_importacao, /*17*/   
           0,/*18*/
           :new.capa_ent_nrdoc, /*19*/   
           '',/*20*/
           :new.capa_ent_serie, /*21*/   
           0,/*22*/
           :new.capa_ent_forcli9, /*23*/   
           0,/*24*/
           :new.capa_ent_forcli4, /*25*/   
           0,/*26*/
           :new.capa_ent_forcli2, /*27*/   
           0,/*28*/
           :new.sequencia, /*29*/   
           '',/*30*/
           :new.coditem_nivel99, /*31*/   
           '',/*32*/
           :new.coditem_grupo, /*33*/   
           '',/*34*/
           :new.coditem_subgrupo, /*35*/   
           '',/*36*/
           :new.coditem_item, /*37*/   
           0,/*38*/
           :new.quantidade, /*39*/   
           0,/*40*/
           :new.lote_entrega, /*41*/   
           0,/*42*/
           :new.valor_unitario, /*43*/   
           '',/*44*/
           :new.unidade_medida, /*45*/   
           0,/*46*/
           :new.valor_ipi, /*47*/   
           0,/*48*/
           :new.valor_total, /*49*/   
           '',/*50*/
           :new.classific_fiscal, /*51*/   
           0,/*52*/
           :new.classif_contabil, /*53*/   
           0,/*54*/
           :new.cod_vlfiscal_ipi, /*55*/   
           0,/*56*/
           :new.pedido_compra, /*57*/   
           0,/*58*/
           :new.sequencia_pedido, /*59*/   
           '',/*60*/
           :new.descricao_item, /*61*/   
           0,/*62*/
           :new.percentual_ipi, /*63*/   
           0,/*64*/
           :new.centro_custo, /*65*/   
           0,/*66*/
           :new.codigo_deposito, /*67*/   
           0,/*68*/
           :new.preco_custo, /*69*/   
           0,/*70*/
           :new.natitem_nat_oper, /*71*/   
           '',/*72*/
           :new.natitem_est_oper, /*73*/   
           0,/*74*/
           :new.cod_vlfiscal_icm, /*75*/   
           0,/*76*/
           :new.valor_icms, /*77*/   
           0,/*78*/
           :new.codigo_transacao, /*79*/   
           0,/*80*/
           :new.percentual_icm, /*81*/   
           0,/*82*/
           :new.base_calc_icm, /*83*/   
           0,/*84*/
           :new.base_diferenca, /*85*/   
           0,/*86*/
           :new.cvf_icm_diferenc, /*87*/   
           0,/*88*/
           :new.procedencia, /*89*/   
           0,/*90*/
           :new.rateio_despesas, /*91*/   
           0,/*92*/
           :new.dif_aliquota, /*93*/   
           0,/*94*/
           :new.perc_dif_aliq, /*95*/   
           0,/*96*/
           :new.base_ipi, /*97*/   
           0,/*98*/
           :new.valor_pis, /*99*/   
           0,/*100*/
           :new.valor_cofins, /*101*/   
           0,/*102*/
           :new.base_pis_cofins, /*103*/   
           0,/*104*/
           :new.codigo_contabil, /*105*/   
           '',/*106*/
           :new.obs_livro1, /*107*/   
           '',/*108*/
           :new.obs_livro2, /*109*/   
           0,/*110*/
           :new.rateio_despesas_ipi, /*111*/   
           0,/*112*/
           :new.rateio_descontos_ipi, /*113*/   
           '',/*114*/
           :new.natureza_str, /*115*/   
           0,/*116*/
           :new.cvf_pis, /*117*/   
           0,/*118*/
           :new.cvf_cofins, /*119*/   
           0,/*120*/
           :new.perc_pis, /*121*/   
           0,/*122*/
           :new.perc_cofins, /*123*/   
           0,/*124*/
           :new.cvf_ipi_entrada, /*125*/   
           0,/*126*/
           :new.base_icms_subs, /*127*/   
           0,/*128*/
           :new.perc_icms_subs, /*129*/   
           0,/*130*/
           :new.valor_icms_subs, /*131*/   
           0,/*132*/
           :new.num_nota_orig, /*133*/   
           0,/*134*/
           :new.seq_nota_orig, /*135*/   
           0,/*136*/
           :new.motivo_devolucao, /*137*/   
           '',/*138*/
           :new.serie_nota_orig, /*139*/   
           0,/*140*/
           :new.flag_devolucao, /*141*/   
           0,/*142*/
           :new.empr_nf_saida, /*143*/   
           0,/*144*/
           :new.num_nf_saida, /*145*/   
           '',/*146*/
           :new.serie_nf_saida, /*147*/   
           0,/*148*/
           :new.perc_icms_diferido, /*149*/   
           0,/*150*/
           :new.valor_icms_diferido, /*151*/   
           null,/*152*/
           :new.data_canc_nfisc, /*153*/   
           0,/*154*/
           :new.transacao_canc_nfisc, /*155*/   
           '',/*156*/
           :new.tipo_doc_importacao_sped, /*157*/   
           0,/*158*/
           :new.valor_pis_import_sped, /*159*/   
           0,/*160*/
           :new.valor_cofins_import_sped, /*161*/   
           null,/*162*/
           :new.data_desembaraco_sped, /*163*/   
           0,/*164*/
           :new.valor_cif_sped, /*165*/   
           0,/*166*/
           :new.valor_desp_sem_icms_sped, /*167*/   
           0,/*168*/
           :new.valor_desp_com_icms_sped, /*169*/   
           0,/*170*/
           :new.valor_imposto_opera_finan_sped, /*171*/   
           0,/*172*/
           :new.valor_imposto_import_sped, /*173*/   
           0,/*174*/
           :new.base_ipi_importacao, /*175*/   
           0,/*176*/
           :new.cvf_ipi_importacao, /*177*/   
           0,/*178*/
           :new.valor_ipi_importacao, /*179*/   
           0,/*180*/
           :new.base_pis_cofins_importacao, /*181*/   
           0,/*182*/
           :new.cvf_pis_importacao, /*183*/   
           0,/*184*/
           :new.valor_pis_importacao, /*185*/   
           0,/*186*/
           :new.cvf_cofins_importacao, /*187*/   
           0,/*188*/
           :new.valor_cofins_importacao, /*189*/   
           0,/*190*/
           :new.base_calculo_importacao, /*191*/   
           0,/*192*/
           :new.valor_imposto_importacao, /*193*/   
           0,/*194*/
           :new.valor_iof_importacao, /*195*/   
           0,/*196*/
           :new.valor_desp_aduan, /*197*/   
           0,/*198*/
           :new.perc_ipi_importacao, /*199*/   
           0,/*200*/
           :new.perc_pis_importacao, /*201*/   
           0,/*202*/
           :new.perc_cofins_importacao, /*203*/   
           0,/*204*/
           :new.cod_csosn, /*205*/   
           0,/*206*/
           :new.valor_frete, /*207*/   
           0,/*208*/
           :new.valor_desc, /*209*/   
           0,/*210*/
           :new.valor_outros, /*211*/   
           0,/*212*/
           :new.valor_seguro, /*213*/   
           0,/*214*/
           :new.numero_adicao, /*215*/   
           0,/*216*/
           :new.cod_base_cred, /*217*/   
           0,/*218*/
           :new.desconto_item, /*219*/   
           0,/*220*/
           :new.flag_saida, /*221*/   
           0,/*222*/
           :new.st_flag_cce, /*223*/   
           0,/*224*/
           :new.cd_cnpj_forne_9, /*225*/   
           0,/*226*/
           :new.cd_cnpj_forne_4, /*227*/   
           0,/*228*/
           :new.cd_cnpj_forne_2, /*229*/   
           0,/*230*/
           :new.cd_seq_nota_frete, /*231*/   
           0,/*232*/
           :new.qtd_pcs_caixa_rfid, /*233*/   
           '',/*234*/
           :new.numero_di, /*235*/   
           null,/*236*/
           :new.data_declaracao, /*237*/   
           '',/*238*/
           :new.local_desembaraco, /*239*/   
           '',/*240*/
           :new.uf_desembaraco, /*241*/   
           '',/*242*/
           :new.codigo_exportador, /*243*/   
           '',/*244*/
           :new.codigo_fabricante, /*245*/   
           0,/*246*/
           :new.sequencia_adicao, /*247*/   
           0,/*248*/
           :new.taxa_siscomex_imp, /*249*/   
           '',/*250*/
           :new.local_desembaraco_imp, /*251*/   
           '',/*252*/
           :new.uf_desembaraco_imp, /*253*/   
           0,/*254*/
           :new.valor_aduaneiro_imp, /*255*/   
           0,/*256*/
           :new.valor_ii_imp, /*257*/   
           '',/*258*/
           :new.num_programa_emb, /*259*/   
           '',/*260*/
           :new.cest, /*261*/   
           0,/*262*/
           :new.fator_conv, /*263*/   
           '',/*264*/
           :new.observacao, /*265*/   
           0,/*266*/
           :new.pdif51, /*267*/   
           0,/*268*/
           :new.perc_fcp_uf_dest, /*269*/   
           0,/*270*/
           :new.perc_icms_partilha, /*271*/   
           0,/*272*/
           :new.perc_icms_uf_dest, /*273*/   
           0,/*274*/
           :new.perc_redu_icm, /*275*/   
           0,/*276*/
           :new.perc_redu_sub, /*277*/   
           0,/*278*/
           :new.perc_substituicao, /*279*/   
           0,/*280*/
           :new.quantidade_conv, /*281*/   
           '',/*282*/
           :new.unidade_conv, /*283*/   
           0,/*284*/
           :new.valor_afrmm, /*285*/   
           0,/*286*/
           :new.valor_anti_dump, /*287*/   
           0,/*288*/
           :new.valor_conv, /*289*/   
           0,/*290*/
           :new.valor_desp_acess, /*291*/   
           0,/*292*/
           :new.valor_desp_ntrib, /*293*/   
           0,/*294*/
           :new.val_fcp_uf_dest, /*295*/   
           0,/*296*/
           :new.val_icms_uf_dest, /*297*/   
           0,/*298*/
           :new.val_icms_uf_remet, /*299*/   
           0,/*300*/
           :new.vicmsdif51, /*301*/   
           0,/*302*/
           :new.vicmsop51, /*303*/   
           '',/*304*/
           :new.produto_forn, /*305*/   
           '',/*306*/
           :new.descricao_item_forn, /*307*/   
           0,/*308*/
           :new.quantidade_forn, /*309*/   
           '',/*310*/
           :new.unidade_medida_forn, /*311*/   
           0,/*312*/
           :new.valor_unitario_forn, /*313*/   
           0,/*314*/
           :new.valor_total_forn, /*315*/   
           '',/*316*/
           :new.cfop, /*317*/   
           0,/*318*/
           :new.cst_icms_xml, /*319*/   
           0,/*320*/
           :new.perc_icms_xml, /*321*/   
           0,/*322*/
           :new.orig_icms_xml, /*323*/   
           0,/*324*/
           :new.base_calc_icms_xml, /*325*/   
           0,/*326*/
           :new.valor_icms_xml, /*327*/   
           0,/*328*/
           :new.perc_icms_subs_xml, /*329*/   
           0,/*330*/
           :new.base_icms_subs_xml, /*331*/   
           0,/*332*/
           :new.valor_icms_subs_xml, /*333*/   
           0,/*334*/
           :new.base_pis_cofins_xml, /*335*/   
           0,/*336*/
           :new.perc_pis_xml, /*337*/   
           0,/*338*/
           :new.cst_pis_xml, /*339*/   
           0,/*340*/
           :new.valor_pis_xml, /*341*/   
           0,/*342*/
           :new.perc_cofins_xml, /*343*/   
           0,/*344*/
           :new.cst_cofins_xml, /*345*/   
           0,/*346*/
           :new.valor_cofins_xml, /*347*/   
           0,/*348*/
           :new.perc_ipi_xml, /*349*/   
           0,/*350*/
           :new.cst_ipi_xml, /*351*/   
           0,/*352*/
           :new.base_ipi_xml, /*353*/   
           0,/*354*/
           :new.valor_ipi_xml, /*355*/   
           '',/*356*/
           :new.ncm_xml, /*357*/   
           0,/*358*/
           :new.flag_rateado, /*359*/   
           0,/*360*/
           :new.sequencia_original, /*361*/   
           0,/*362*/
           :new.qtde_original, /*363*/   
           0,/*364*/
           :new.situacao_liberacao, /*365*/   
           0,/*366*/
           :new.tp_via_transp, /*367*/   
           '',/*368*/
           :new.ind_orig_cod_prod, /*369*/   
           0,/*370*/
           :new.conferencia_almox, /*371*/   
           0,/*372*/
           :new.projeto, /*373*/   
           0,/*374*/
           :new.subprojeto, /*375*/   
           0,/*376*/
           :new.servico, /*377*/   
           '',/*378*/
           :new.codigo_ean /*379*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into I_OBRF_015_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           id_registro_OLD, /*8*/   
           id_registro_NEW, /*9*/   
           flag_importacao_OLD, /*10*/   
           flag_importacao_NEW, /*11*/   
           tipo_atualizacao_OLD, /*12*/   
           tipo_atualizacao_NEW, /*13*/   
           data_exportacao_OLD, /*14*/   
           data_exportacao_NEW, /*15*/   
           data_importacao_OLD, /*16*/   
           data_importacao_NEW, /*17*/   
           capa_ent_nrdoc_OLD, /*18*/   
           capa_ent_nrdoc_NEW, /*19*/   
           capa_ent_serie_OLD, /*20*/   
           capa_ent_serie_NEW, /*21*/   
           capa_ent_forcli9_OLD, /*22*/   
           capa_ent_forcli9_NEW, /*23*/   
           capa_ent_forcli4_OLD, /*24*/   
           capa_ent_forcli4_NEW, /*25*/   
           capa_ent_forcli2_OLD, /*26*/   
           capa_ent_forcli2_NEW, /*27*/   
           sequencia_OLD, /*28*/   
           sequencia_NEW, /*29*/   
           coditem_nivel99_OLD, /*30*/   
           coditem_nivel99_NEW, /*31*/   
           coditem_grupo_OLD, /*32*/   
           coditem_grupo_NEW, /*33*/   
           coditem_subgrupo_OLD, /*34*/   
           coditem_subgrupo_NEW, /*35*/   
           coditem_item_OLD, /*36*/   
           coditem_item_NEW, /*37*/   
           quantidade_OLD, /*38*/   
           quantidade_NEW, /*39*/   
           lote_entrega_OLD, /*40*/   
           lote_entrega_NEW, /*41*/   
           valor_unitario_OLD, /*42*/   
           valor_unitario_NEW, /*43*/   
           unidade_medida_OLD, /*44*/   
           unidade_medida_NEW, /*45*/   
           valor_ipi_OLD, /*46*/   
           valor_ipi_NEW, /*47*/   
           valor_total_OLD, /*48*/   
           valor_total_NEW, /*49*/   
           classific_fiscal_OLD, /*50*/   
           classific_fiscal_NEW, /*51*/   
           classif_contabil_OLD, /*52*/   
           classif_contabil_NEW, /*53*/   
           cod_vlfiscal_ipi_OLD, /*54*/   
           cod_vlfiscal_ipi_NEW, /*55*/   
           pedido_compra_OLD, /*56*/   
           pedido_compra_NEW, /*57*/   
           sequencia_pedido_OLD, /*58*/   
           sequencia_pedido_NEW, /*59*/   
           descricao_item_OLD, /*60*/   
           descricao_item_NEW, /*61*/   
           percentual_ipi_OLD, /*62*/   
           percentual_ipi_NEW, /*63*/   
           centro_custo_OLD, /*64*/   
           centro_custo_NEW, /*65*/   
           codigo_deposito_OLD, /*66*/   
           codigo_deposito_NEW, /*67*/   
           preco_custo_OLD, /*68*/   
           preco_custo_NEW, /*69*/   
           natitem_nat_oper_OLD, /*70*/   
           natitem_nat_oper_NEW, /*71*/   
           natitem_est_oper_OLD, /*72*/   
           natitem_est_oper_NEW, /*73*/   
           cod_vlfiscal_icm_OLD, /*74*/   
           cod_vlfiscal_icm_NEW, /*75*/   
           valor_icms_OLD, /*76*/   
           valor_icms_NEW, /*77*/   
           codigo_transacao_OLD, /*78*/   
           codigo_transacao_NEW, /*79*/   
           percentual_icm_OLD, /*80*/   
           percentual_icm_NEW, /*81*/   
           base_calc_icm_OLD, /*82*/   
           base_calc_icm_NEW, /*83*/   
           base_diferenca_OLD, /*84*/   
           base_diferenca_NEW, /*85*/   
           cvf_icm_diferenc_OLD, /*86*/   
           cvf_icm_diferenc_NEW, /*87*/   
           procedencia_OLD, /*88*/   
           procedencia_NEW, /*89*/   
           rateio_despesas_OLD, /*90*/   
           rateio_despesas_NEW, /*91*/   
           dif_aliquota_OLD, /*92*/   
           dif_aliquota_NEW, /*93*/   
           perc_dif_aliq_OLD, /*94*/   
           perc_dif_aliq_NEW, /*95*/   
           base_ipi_OLD, /*96*/   
           base_ipi_NEW, /*97*/   
           valor_pis_OLD, /*98*/   
           valor_pis_NEW, /*99*/   
           valor_cofins_OLD, /*100*/   
           valor_cofins_NEW, /*101*/   
           base_pis_cofins_OLD, /*102*/   
           base_pis_cofins_NEW, /*103*/   
           codigo_contabil_OLD, /*104*/   
           codigo_contabil_NEW, /*105*/   
           obs_livro1_OLD, /*106*/   
           obs_livro1_NEW, /*107*/   
           obs_livro2_OLD, /*108*/   
           obs_livro2_NEW, /*109*/   
           rateio_despesas_ipi_OLD, /*110*/   
           rateio_despesas_ipi_NEW, /*111*/   
           rateio_descontos_ipi_OLD, /*112*/   
           rateio_descontos_ipi_NEW, /*113*/   
           natureza_str_OLD, /*114*/   
           natureza_str_NEW, /*115*/   
           cvf_pis_OLD, /*116*/   
           cvf_pis_NEW, /*117*/   
           cvf_cofins_OLD, /*118*/   
           cvf_cofins_NEW, /*119*/   
           perc_pis_OLD, /*120*/   
           perc_pis_NEW, /*121*/   
           perc_cofins_OLD, /*122*/   
           perc_cofins_NEW, /*123*/   
           cvf_ipi_entrada_OLD, /*124*/   
           cvf_ipi_entrada_NEW, /*125*/   
           base_icms_subs_OLD, /*126*/   
           base_icms_subs_NEW, /*127*/   
           perc_icms_subs_OLD, /*128*/   
           perc_icms_subs_NEW, /*129*/   
           valor_icms_subs_OLD, /*130*/   
           valor_icms_subs_NEW, /*131*/   
           num_nota_orig_OLD, /*132*/   
           num_nota_orig_NEW, /*133*/   
           seq_nota_orig_OLD, /*134*/   
           seq_nota_orig_NEW, /*135*/   
           motivo_devolucao_OLD, /*136*/   
           motivo_devolucao_NEW, /*137*/   
           serie_nota_orig_OLD, /*138*/   
           serie_nota_orig_NEW, /*139*/   
           flag_devolucao_OLD, /*140*/   
           flag_devolucao_NEW, /*141*/   
           empr_nf_saida_OLD, /*142*/   
           empr_nf_saida_NEW, /*143*/   
           num_nf_saida_OLD, /*144*/   
           num_nf_saida_NEW, /*145*/   
           serie_nf_saida_OLD, /*146*/   
           serie_nf_saida_NEW, /*147*/   
           perc_icms_diferido_OLD, /*148*/   
           perc_icms_diferido_NEW, /*149*/   
           valor_icms_diferido_OLD, /*150*/   
           valor_icms_diferido_NEW, /*151*/   
           data_canc_nfisc_OLD, /*152*/   
           data_canc_nfisc_NEW, /*153*/   
           transacao_canc_nfisc_OLD, /*154*/   
           transacao_canc_nfisc_NEW, /*155*/   
           tipo_doc_importacao_sped_OLD, /*156*/   
           tipo_doc_importacao_sped_NEW, /*157*/   
           valor_pis_import_sped_OLD, /*158*/   
           valor_pis_import_sped_NEW, /*159*/   
           valor_cofins_import_sped_OLD, /*160*/   
           valor_cofins_import_sped_NEW, /*161*/   
           data_desembaraco_sped_OLD, /*162*/   
           data_desembaraco_sped_NEW, /*163*/   
           valor_cif_sped_OLD, /*164*/   
           valor_cif_sped_NEW, /*165*/   
           valor_desp_sem_icms_sped_OLD, /*166*/   
           valor_desp_sem_icms_sped_NEW, /*167*/   
           valor_desp_com_icms_sped_OLD, /*168*/   
           valor_desp_com_icms_sped_NEW, /*169*/   
           valor_imp_opera_finan_sped_OLD, /*170*/   
           valor_imp_opera_finan_sped_NEW, /*171*/   
           valor_imposto_import_sped_OLD, /*172*/   
           valor_imposto_import_sped_NEW, /*173*/   
           base_ipi_importacao_OLD, /*174*/   
           base_ipi_importacao_NEW, /*175*/   
           cvf_ipi_importacao_OLD, /*176*/   
           cvf_ipi_importacao_NEW, /*177*/   
           valor_ipi_importacao_OLD, /*178*/   
           valor_ipi_importacao_NEW, /*179*/   
           base_pis_cofins_importacao_OLD, /*180*/   
           base_pis_cofins_importacao_NEW, /*181*/   
           cvf_pis_importacao_OLD, /*182*/   
           cvf_pis_importacao_NEW, /*183*/   
           valor_pis_importacao_OLD, /*184*/   
           valor_pis_importacao_NEW, /*185*/   
           cvf_cofins_importacao_OLD, /*186*/   
           cvf_cofins_importacao_NEW, /*187*/   
           valor_cofins_importacao_OLD, /*188*/   
           valor_cofins_importacao_NEW, /*189*/   
           base_calculo_importacao_OLD, /*190*/   
           base_calculo_importacao_NEW, /*191*/   
           valor_imposto_importacao_OLD, /*192*/   
           valor_imposto_importacao_NEW, /*193*/   
           valor_iof_importacao_OLD, /*194*/   
           valor_iof_importacao_NEW, /*195*/   
           valor_desp_aduan_OLD, /*196*/   
           valor_desp_aduan_NEW, /*197*/   
           perc_ipi_importacao_OLD, /*198*/   
           perc_ipi_importacao_NEW, /*199*/   
           perc_pis_importacao_OLD, /*200*/   
           perc_pis_importacao_NEW, /*201*/   
           perc_cofins_importacao_OLD, /*202*/   
           perc_cofins_importacao_NEW, /*203*/   
           cod_csosn_OLD, /*204*/   
           cod_csosn_NEW, /*205*/   
           valor_frete_OLD, /*206*/   
           valor_frete_NEW, /*207*/   
           valor_desc_OLD, /*208*/   
           valor_desc_NEW, /*209*/   
           valor_outros_OLD, /*210*/   
           valor_outros_NEW, /*211*/   
           valor_seguro_OLD, /*212*/   
           valor_seguro_NEW, /*213*/   
           numero_adicao_OLD, /*214*/   
           numero_adicao_NEW, /*215*/   
           cod_base_cred_OLD, /*216*/   
           cod_base_cred_NEW, /*217*/   
           desconto_item_OLD, /*218*/   
           desconto_item_NEW, /*219*/   
           flag_saida_OLD, /*220*/   
           flag_saida_NEW, /*221*/   
           st_flag_cce_OLD, /*222*/   
           st_flag_cce_NEW, /*223*/   
           cd_cnpj_forne_9_OLD, /*224*/   
           cd_cnpj_forne_9_NEW, /*225*/   
           cd_cnpj_forne_4_OLD, /*226*/   
           cd_cnpj_forne_4_NEW, /*227*/   
           cd_cnpj_forne_2_OLD, /*228*/   
           cd_cnpj_forne_2_NEW, /*229*/   
           cd_seq_nota_frete_OLD, /*230*/   
           cd_seq_nota_frete_NEW, /*231*/   
           qtd_pcs_caixa_rfid_OLD, /*232*/   
           qtd_pcs_caixa_rfid_NEW, /*233*/   
           numero_di_OLD, /*234*/   
           numero_di_NEW, /*235*/   
           data_declaracao_OLD, /*236*/   
           data_declaracao_NEW, /*237*/   
           local_desembaraco_OLD, /*238*/   
           local_desembaraco_NEW, /*239*/   
           uf_desembaraco_OLD, /*240*/   
           uf_desembaraco_NEW, /*241*/   
           codigo_exportador_OLD, /*242*/   
           codigo_exportador_NEW, /*243*/   
           codigo_fabricante_OLD, /*244*/   
           codigo_fabricante_NEW, /*245*/   
           sequencia_adicao_OLD, /*246*/   
           sequencia_adicao_NEW, /*247*/   
           taxa_siscomex_imp_OLD, /*248*/   
           taxa_siscomex_imp_NEW, /*249*/   
           local_desembaraco_imp_OLD, /*250*/   
           local_desembaraco_imp_NEW, /*251*/   
           uf_desembaraco_imp_OLD, /*252*/   
           uf_desembaraco_imp_NEW, /*253*/   
           valor_aduaneiro_imp_OLD, /*254*/   
           valor_aduaneiro_imp_NEW, /*255*/   
           valor_ii_imp_OLD, /*256*/   
           valor_ii_imp_NEW, /*257*/   
           num_programa_emb_OLD, /*258*/   
           num_programa_emb_NEW, /*259*/   
           cest_OLD, /*260*/   
           cest_NEW, /*261*/   
           fator_conv_OLD, /*262*/   
           fator_conv_NEW, /*263*/   
           observacao_OLD, /*264*/   
           observacao_NEW, /*265*/   
           pdif51_OLD, /*266*/   
           pdif51_NEW, /*267*/   
           perc_fcp_uf_dest_OLD, /*268*/   
           perc_fcp_uf_dest_NEW, /*269*/   
           perc_icms_partilha_OLD, /*270*/   
           perc_icms_partilha_NEW, /*271*/   
           perc_icms_uf_dest_OLD, /*272*/   
           perc_icms_uf_dest_NEW, /*273*/   
           perc_redu_icm_OLD, /*274*/   
           perc_redu_icm_NEW, /*275*/   
           perc_redu_sub_OLD, /*276*/   
           perc_redu_sub_NEW, /*277*/   
           perc_substituicao_OLD, /*278*/   
           perc_substituicao_NEW, /*279*/   
           quantidade_conv_OLD, /*280*/   
           quantidade_conv_NEW, /*281*/   
           unidade_conv_OLD, /*282*/   
           unidade_conv_NEW, /*283*/   
           valor_afrmm_OLD, /*284*/   
           valor_afrmm_NEW, /*285*/   
           valor_anti_dump_OLD, /*286*/   
           valor_anti_dump_NEW, /*287*/   
           valor_conv_OLD, /*288*/   
           valor_conv_NEW, /*289*/   
           valor_desp_acess_OLD, /*290*/   
           valor_desp_acess_NEW, /*291*/   
           valor_desp_ntrib_OLD, /*292*/   
           valor_desp_ntrib_NEW, /*293*/   
           val_fcp_uf_dest_OLD, /*294*/   
           val_fcp_uf_dest_NEW, /*295*/   
           val_icms_uf_dest_OLD, /*296*/   
           val_icms_uf_dest_NEW, /*297*/   
           val_icms_uf_remet_OLD, /*298*/   
           val_icms_uf_remet_NEW, /*299*/   
           vicmsdif51_OLD, /*300*/   
           vicmsdif51_NEW, /*301*/   
           vicmsop51_OLD, /*302*/   
           vicmsop51_NEW, /*303*/   
           produto_forn_OLD, /*304*/   
           produto_forn_NEW, /*305*/   
           descricao_item_forn_OLD, /*306*/   
           descricao_item_forn_NEW, /*307*/   
           quantidade_forn_OLD, /*308*/   
           quantidade_forn_NEW, /*309*/   
           unidade_medida_forn_OLD, /*310*/   
           unidade_medida_forn_NEW, /*311*/   
           valor_unitario_forn_OLD, /*312*/   
           valor_unitario_forn_NEW, /*313*/   
           valor_total_forn_OLD, /*314*/   
           valor_total_forn_NEW, /*315*/   
           cfop_OLD, /*316*/   
           cfop_NEW, /*317*/   
           cst_icms_xml_OLD, /*318*/   
           cst_icms_xml_NEW, /*319*/   
           perc_icms_xml_OLD, /*320*/   
           perc_icms_xml_NEW, /*321*/   
           orig_icms_xml_OLD, /*322*/   
           orig_icms_xml_NEW, /*323*/   
           base_calc_icms_xml_OLD, /*324*/   
           base_calc_icms_xml_NEW, /*325*/   
           valor_icms_xml_OLD, /*326*/   
           valor_icms_xml_NEW, /*327*/   
           perc_icms_subs_xml_OLD, /*328*/   
           perc_icms_subs_xml_NEW, /*329*/   
           base_icms_subs_xml_OLD, /*330*/   
           base_icms_subs_xml_NEW, /*331*/   
           valor_icms_subs_xml_OLD, /*332*/   
           valor_icms_subs_xml_NEW, /*333*/   
           base_pis_cofins_xml_OLD, /*334*/   
           base_pis_cofins_xml_NEW, /*335*/   
           perc_pis_xml_OLD, /*336*/   
           perc_pis_xml_NEW, /*337*/   
           cst_pis_xml_OLD, /*338*/   
           cst_pis_xml_NEW, /*339*/   
           valor_pis_xml_OLD, /*340*/   
           valor_pis_xml_NEW, /*341*/   
           perc_cofins_xml_OLD, /*342*/   
           perc_cofins_xml_NEW, /*343*/   
           cst_cofins_xml_OLD, /*344*/   
           cst_cofins_xml_NEW, /*345*/   
           valor_cofins_xml_OLD, /*346*/   
           valor_cofins_xml_NEW, /*347*/   
           perc_ipi_xml_OLD, /*348*/   
           perc_ipi_xml_NEW, /*349*/   
           cst_ipi_xml_OLD, /*350*/   
           cst_ipi_xml_NEW, /*351*/   
           base_ipi_xml_OLD, /*352*/   
           base_ipi_xml_NEW, /*353*/   
           valor_ipi_xml_OLD, /*354*/   
           valor_ipi_xml_NEW, /*355*/   
           ncm_xml_OLD, /*356*/   
           ncm_xml_NEW, /*357*/   
           flag_rateado_OLD, /*358*/   
           flag_rateado_NEW, /*359*/   
           sequencia_original_OLD, /*360*/   
           sequencia_original_NEW, /*361*/   
           qtde_original_OLD, /*362*/   
           qtde_original_NEW, /*363*/   
           situacao_liberacao_OLD, /*364*/   
           situacao_liberacao_NEW, /*365*/   
           tp_via_transp_OLD, /*366*/   
           tp_via_transp_NEW, /*367*/   
           ind_orig_cod_prod_OLD, /*368*/   
           ind_orig_cod_prod_NEW, /*369*/   
           conferencia_almox_OLD, /*370*/   
           conferencia_almox_NEW, /*371*/   
           projeto_OLD, /*372*/   
           projeto_NEW, /*373*/   
           subprojeto_OLD, /*374*/   
           subprojeto_NEW, /*375*/   
           servico_OLD, /*376*/   
           servico_NEW, /*377*/   
           codigo_ean_OLD, /*378*/   
           codigo_ean_NEW  /*379*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.id_registro,  /*8*/  
           :new.id_registro, /*9*/   
           :old.flag_importacao,  /*10*/  
           :new.flag_importacao, /*11*/   
           :old.tipo_atualizacao,  /*12*/  
           :new.tipo_atualizacao, /*13*/   
           :old.data_exportacao,  /*14*/  
           :new.data_exportacao, /*15*/   
           :old.data_importacao,  /*16*/  
           :new.data_importacao, /*17*/   
           :old.capa_ent_nrdoc,  /*18*/  
           :new.capa_ent_nrdoc, /*19*/   
           :old.capa_ent_serie,  /*20*/  
           :new.capa_ent_serie, /*21*/   
           :old.capa_ent_forcli9,  /*22*/  
           :new.capa_ent_forcli9, /*23*/   
           :old.capa_ent_forcli4,  /*24*/  
           :new.capa_ent_forcli4, /*25*/   
           :old.capa_ent_forcli2,  /*26*/  
           :new.capa_ent_forcli2, /*27*/   
           :old.sequencia,  /*28*/  
           :new.sequencia, /*29*/   
           :old.coditem_nivel99,  /*30*/  
           :new.coditem_nivel99, /*31*/   
           :old.coditem_grupo,  /*32*/  
           :new.coditem_grupo, /*33*/   
           :old.coditem_subgrupo,  /*34*/  
           :new.coditem_subgrupo, /*35*/   
           :old.coditem_item,  /*36*/  
           :new.coditem_item, /*37*/   
           :old.quantidade,  /*38*/  
           :new.quantidade, /*39*/   
           :old.lote_entrega,  /*40*/  
           :new.lote_entrega, /*41*/   
           :old.valor_unitario,  /*42*/  
           :new.valor_unitario, /*43*/   
           :old.unidade_medida,  /*44*/  
           :new.unidade_medida, /*45*/   
           :old.valor_ipi,  /*46*/  
           :new.valor_ipi, /*47*/   
           :old.valor_total,  /*48*/  
           :new.valor_total, /*49*/   
           :old.classific_fiscal,  /*50*/  
           :new.classific_fiscal, /*51*/   
           :old.classif_contabil,  /*52*/  
           :new.classif_contabil, /*53*/   
           :old.cod_vlfiscal_ipi,  /*54*/  
           :new.cod_vlfiscal_ipi, /*55*/   
           :old.pedido_compra,  /*56*/  
           :new.pedido_compra, /*57*/   
           :old.sequencia_pedido,  /*58*/  
           :new.sequencia_pedido, /*59*/   
           :old.descricao_item,  /*60*/  
           :new.descricao_item, /*61*/   
           :old.percentual_ipi,  /*62*/  
           :new.percentual_ipi, /*63*/   
           :old.centro_custo,  /*64*/  
           :new.centro_custo, /*65*/   
           :old.codigo_deposito,  /*66*/  
           :new.codigo_deposito, /*67*/   
           :old.preco_custo,  /*68*/  
           :new.preco_custo, /*69*/   
           :old.natitem_nat_oper,  /*70*/  
           :new.natitem_nat_oper, /*71*/   
           :old.natitem_est_oper,  /*72*/  
           :new.natitem_est_oper, /*73*/   
           :old.cod_vlfiscal_icm,  /*74*/  
           :new.cod_vlfiscal_icm, /*75*/   
           :old.valor_icms,  /*76*/  
           :new.valor_icms, /*77*/   
           :old.codigo_transacao,  /*78*/  
           :new.codigo_transacao, /*79*/   
           :old.percentual_icm,  /*80*/  
           :new.percentual_icm, /*81*/   
           :old.base_calc_icm,  /*82*/  
           :new.base_calc_icm, /*83*/   
           :old.base_diferenca,  /*84*/  
           :new.base_diferenca, /*85*/   
           :old.cvf_icm_diferenc,  /*86*/  
           :new.cvf_icm_diferenc, /*87*/   
           :old.procedencia,  /*88*/  
           :new.procedencia, /*89*/   
           :old.rateio_despesas,  /*90*/  
           :new.rateio_despesas, /*91*/   
           :old.dif_aliquota,  /*92*/  
           :new.dif_aliquota, /*93*/   
           :old.perc_dif_aliq,  /*94*/  
           :new.perc_dif_aliq, /*95*/   
           :old.base_ipi,  /*96*/  
           :new.base_ipi, /*97*/   
           :old.valor_pis,  /*98*/  
           :new.valor_pis, /*99*/   
           :old.valor_cofins,  /*100*/  
           :new.valor_cofins, /*101*/   
           :old.base_pis_cofins,  /*102*/  
           :new.base_pis_cofins, /*103*/   
           :old.codigo_contabil,  /*104*/  
           :new.codigo_contabil, /*105*/   
           :old.obs_livro1,  /*106*/  
           :new.obs_livro1, /*107*/   
           :old.obs_livro2,  /*108*/  
           :new.obs_livro2, /*109*/   
           :old.rateio_despesas_ipi,  /*110*/  
           :new.rateio_despesas_ipi, /*111*/   
           :old.rateio_descontos_ipi,  /*112*/  
           :new.rateio_descontos_ipi, /*113*/   
           :old.natureza_str,  /*114*/  
           :new.natureza_str, /*115*/   
           :old.cvf_pis,  /*116*/  
           :new.cvf_pis, /*117*/   
           :old.cvf_cofins,  /*118*/  
           :new.cvf_cofins, /*119*/   
           :old.perc_pis,  /*120*/  
           :new.perc_pis, /*121*/   
           :old.perc_cofins,  /*122*/  
           :new.perc_cofins, /*123*/   
           :old.cvf_ipi_entrada,  /*124*/  
           :new.cvf_ipi_entrada, /*125*/   
           :old.base_icms_subs,  /*126*/  
           :new.base_icms_subs, /*127*/   
           :old.perc_icms_subs,  /*128*/  
           :new.perc_icms_subs, /*129*/   
           :old.valor_icms_subs,  /*130*/  
           :new.valor_icms_subs, /*131*/   
           :old.num_nota_orig,  /*132*/  
           :new.num_nota_orig, /*133*/   
           :old.seq_nota_orig,  /*134*/  
           :new.seq_nota_orig, /*135*/   
           :old.motivo_devolucao,  /*136*/  
           :new.motivo_devolucao, /*137*/   
           :old.serie_nota_orig,  /*138*/  
           :new.serie_nota_orig, /*139*/   
           :old.flag_devolucao,  /*140*/  
           :new.flag_devolucao, /*141*/   
           :old.empr_nf_saida,  /*142*/  
           :new.empr_nf_saida, /*143*/   
           :old.num_nf_saida,  /*144*/  
           :new.num_nf_saida, /*145*/   
           :old.serie_nf_saida,  /*146*/  
           :new.serie_nf_saida, /*147*/   
           :old.perc_icms_diferido,  /*148*/  
           :new.perc_icms_diferido, /*149*/   
           :old.valor_icms_diferido,  /*150*/  
           :new.valor_icms_diferido, /*151*/   
           :old.data_canc_nfisc,  /*152*/  
           :new.data_canc_nfisc, /*153*/   
           :old.transacao_canc_nfisc,  /*154*/  
           :new.transacao_canc_nfisc, /*155*/   
           :old.tipo_doc_importacao_sped,  /*156*/  
           :new.tipo_doc_importacao_sped, /*157*/   
           :old.valor_pis_import_sped,  /*158*/  
           :new.valor_pis_import_sped, /*159*/   
           :old.valor_cofins_import_sped,  /*160*/  
           :new.valor_cofins_import_sped, /*161*/   
           :old.data_desembaraco_sped,  /*162*/  
           :new.data_desembaraco_sped, /*163*/   
           :old.valor_cif_sped,  /*164*/  
           :new.valor_cif_sped, /*165*/   
           :old.valor_desp_sem_icms_sped,  /*166*/  
           :new.valor_desp_sem_icms_sped, /*167*/   
           :old.valor_desp_com_icms_sped,  /*168*/  
           :new.valor_desp_com_icms_sped, /*169*/   
           :old.valor_imposto_opera_finan_sped,  /*170*/  
           :new.valor_imposto_opera_finan_sped, /*171*/   
           :old.valor_imposto_import_sped,  /*172*/  
           :new.valor_imposto_import_sped, /*173*/   
           :old.base_ipi_importacao,  /*174*/  
           :new.base_ipi_importacao, /*175*/   
           :old.cvf_ipi_importacao,  /*176*/  
           :new.cvf_ipi_importacao, /*177*/   
           :old.valor_ipi_importacao,  /*178*/  
           :new.valor_ipi_importacao, /*179*/   
           :old.base_pis_cofins_importacao,  /*180*/  
           :new.base_pis_cofins_importacao, /*181*/   
           :old.cvf_pis_importacao,  /*182*/  
           :new.cvf_pis_importacao, /*183*/   
           :old.valor_pis_importacao,  /*184*/  
           :new.valor_pis_importacao, /*185*/   
           :old.cvf_cofins_importacao,  /*186*/  
           :new.cvf_cofins_importacao, /*187*/   
           :old.valor_cofins_importacao,  /*188*/  
           :new.valor_cofins_importacao, /*189*/   
           :old.base_calculo_importacao,  /*190*/  
           :new.base_calculo_importacao, /*191*/   
           :old.valor_imposto_importacao,  /*192*/  
           :new.valor_imposto_importacao, /*193*/   
           :old.valor_iof_importacao,  /*194*/  
           :new.valor_iof_importacao, /*195*/   
           :old.valor_desp_aduan,  /*196*/  
           :new.valor_desp_aduan, /*197*/   
           :old.perc_ipi_importacao,  /*198*/  
           :new.perc_ipi_importacao, /*199*/   
           :old.perc_pis_importacao,  /*200*/  
           :new.perc_pis_importacao, /*201*/   
           :old.perc_cofins_importacao,  /*202*/  
           :new.perc_cofins_importacao, /*203*/   
           :old.cod_csosn,  /*204*/  
           :new.cod_csosn, /*205*/   
           :old.valor_frete,  /*206*/  
           :new.valor_frete, /*207*/   
           :old.valor_desc,  /*208*/  
           :new.valor_desc, /*209*/   
           :old.valor_outros,  /*210*/  
           :new.valor_outros, /*211*/   
           :old.valor_seguro,  /*212*/  
           :new.valor_seguro, /*213*/   
           :old.numero_adicao,  /*214*/  
           :new.numero_adicao, /*215*/   
           :old.cod_base_cred,  /*216*/  
           :new.cod_base_cred, /*217*/   
           :old.desconto_item,  /*218*/  
           :new.desconto_item, /*219*/   
           :old.flag_saida,  /*220*/  
           :new.flag_saida, /*221*/   
           :old.st_flag_cce,  /*222*/  
           :new.st_flag_cce, /*223*/   
           :old.cd_cnpj_forne_9,  /*224*/  
           :new.cd_cnpj_forne_9, /*225*/   
           :old.cd_cnpj_forne_4,  /*226*/  
           :new.cd_cnpj_forne_4, /*227*/   
           :old.cd_cnpj_forne_2,  /*228*/  
           :new.cd_cnpj_forne_2, /*229*/   
           :old.cd_seq_nota_frete,  /*230*/  
           :new.cd_seq_nota_frete, /*231*/   
           :old.qtd_pcs_caixa_rfid,  /*232*/  
           :new.qtd_pcs_caixa_rfid, /*233*/   
           :old.numero_di,  /*234*/  
           :new.numero_di, /*235*/   
           :old.data_declaracao,  /*236*/  
           :new.data_declaracao, /*237*/   
           :old.local_desembaraco,  /*238*/  
           :new.local_desembaraco, /*239*/   
           :old.uf_desembaraco,  /*240*/  
           :new.uf_desembaraco, /*241*/   
           :old.codigo_exportador,  /*242*/  
           :new.codigo_exportador, /*243*/   
           :old.codigo_fabricante,  /*244*/  
           :new.codigo_fabricante, /*245*/   
           :old.sequencia_adicao,  /*246*/  
           :new.sequencia_adicao, /*247*/   
           :old.taxa_siscomex_imp,  /*248*/  
           :new.taxa_siscomex_imp, /*249*/   
           :old.local_desembaraco_imp,  /*250*/  
           :new.local_desembaraco_imp, /*251*/   
           :old.uf_desembaraco_imp,  /*252*/  
           :new.uf_desembaraco_imp, /*253*/   
           :old.valor_aduaneiro_imp,  /*254*/  
           :new.valor_aduaneiro_imp, /*255*/   
           :old.valor_ii_imp,  /*256*/  
           :new.valor_ii_imp, /*257*/   
           :old.num_programa_emb,  /*258*/  
           :new.num_programa_emb, /*259*/   
           :old.cest,  /*260*/  
           :new.cest, /*261*/   
           :old.fator_conv,  /*262*/  
           :new.fator_conv, /*263*/   
           :old.observacao,  /*264*/  
           :new.observacao, /*265*/   
           :old.pdif51,  /*266*/  
           :new.pdif51, /*267*/   
           :old.perc_fcp_uf_dest,  /*268*/  
           :new.perc_fcp_uf_dest, /*269*/   
           :old.perc_icms_partilha,  /*270*/  
           :new.perc_icms_partilha, /*271*/   
           :old.perc_icms_uf_dest,  /*272*/  
           :new.perc_icms_uf_dest, /*273*/   
           :old.perc_redu_icm,  /*274*/  
           :new.perc_redu_icm, /*275*/   
           :old.perc_redu_sub,  /*276*/  
           :new.perc_redu_sub, /*277*/   
           :old.perc_substituicao,  /*278*/  
           :new.perc_substituicao, /*279*/   
           :old.quantidade_conv,  /*280*/  
           :new.quantidade_conv, /*281*/   
           :old.unidade_conv,  /*282*/  
           :new.unidade_conv, /*283*/   
           :old.valor_afrmm,  /*284*/  
           :new.valor_afrmm, /*285*/   
           :old.valor_anti_dump,  /*286*/  
           :new.valor_anti_dump, /*287*/   
           :old.valor_conv,  /*288*/  
           :new.valor_conv, /*289*/   
           :old.valor_desp_acess,  /*290*/  
           :new.valor_desp_acess, /*291*/   
           :old.valor_desp_ntrib,  /*292*/  
           :new.valor_desp_ntrib, /*293*/   
           :old.val_fcp_uf_dest,  /*294*/  
           :new.val_fcp_uf_dest, /*295*/   
           :old.val_icms_uf_dest,  /*296*/  
           :new.val_icms_uf_dest, /*297*/   
           :old.val_icms_uf_remet,  /*298*/  
           :new.val_icms_uf_remet, /*299*/   
           :old.vicmsdif51,  /*300*/  
           :new.vicmsdif51, /*301*/   
           :old.vicmsop51,  /*302*/  
           :new.vicmsop51, /*303*/   
           :old.produto_forn,  /*304*/  
           :new.produto_forn, /*305*/   
           :old.descricao_item_forn,  /*306*/  
           :new.descricao_item_forn, /*307*/   
           :old.quantidade_forn,  /*308*/  
           :new.quantidade_forn, /*309*/   
           :old.unidade_medida_forn,  /*310*/  
           :new.unidade_medida_forn, /*311*/   
           :old.valor_unitario_forn,  /*312*/  
           :new.valor_unitario_forn, /*313*/   
           :old.valor_total_forn,  /*314*/  
           :new.valor_total_forn, /*315*/   
           :old.cfop,  /*316*/  
           :new.cfop, /*317*/   
           :old.cst_icms_xml,  /*318*/  
           :new.cst_icms_xml, /*319*/   
           :old.perc_icms_xml,  /*320*/  
           :new.perc_icms_xml, /*321*/   
           :old.orig_icms_xml,  /*322*/  
           :new.orig_icms_xml, /*323*/   
           :old.base_calc_icms_xml,  /*324*/  
           :new.base_calc_icms_xml, /*325*/   
           :old.valor_icms_xml,  /*326*/  
           :new.valor_icms_xml, /*327*/   
           :old.perc_icms_subs_xml,  /*328*/  
           :new.perc_icms_subs_xml, /*329*/   
           :old.base_icms_subs_xml,  /*330*/  
           :new.base_icms_subs_xml, /*331*/   
           :old.valor_icms_subs_xml,  /*332*/  
           :new.valor_icms_subs_xml, /*333*/   
           :old.base_pis_cofins_xml,  /*334*/  
           :new.base_pis_cofins_xml, /*335*/   
           :old.perc_pis_xml,  /*336*/  
           :new.perc_pis_xml, /*337*/   
           :old.cst_pis_xml,  /*338*/  
           :new.cst_pis_xml, /*339*/   
           :old.valor_pis_xml,  /*340*/  
           :new.valor_pis_xml, /*341*/   
           :old.perc_cofins_xml,  /*342*/  
           :new.perc_cofins_xml, /*343*/   
           :old.cst_cofins_xml,  /*344*/  
           :new.cst_cofins_xml, /*345*/   
           :old.valor_cofins_xml,  /*346*/  
           :new.valor_cofins_xml, /*347*/   
           :old.perc_ipi_xml,  /*348*/  
           :new.perc_ipi_xml, /*349*/   
           :old.cst_ipi_xml,  /*350*/  
           :new.cst_ipi_xml, /*351*/   
           :old.base_ipi_xml,  /*352*/  
           :new.base_ipi_xml, /*353*/   
           :old.valor_ipi_xml,  /*354*/  
           :new.valor_ipi_xml, /*355*/   
           :old.ncm_xml,  /*356*/  
           :new.ncm_xml, /*357*/   
           :old.flag_rateado,  /*358*/  
           :new.flag_rateado, /*359*/   
           :old.sequencia_original,  /*360*/  
           :new.sequencia_original, /*361*/   
           :old.qtde_original,  /*362*/  
           :new.qtde_original, /*363*/   
           :old.situacao_liberacao,  /*364*/  
           :new.situacao_liberacao, /*365*/   
           :old.tp_via_transp,  /*366*/  
           :new.tp_via_transp, /*367*/   
           :old.ind_orig_cod_prod,  /*368*/  
           :new.ind_orig_cod_prod, /*369*/   
           :old.conferencia_almox,  /*370*/  
           :new.conferencia_almox, /*371*/   
           :old.projeto,  /*372*/  
           :new.projeto, /*373*/   
           :old.subprojeto,  /*374*/  
           :new.subprojeto, /*375*/   
           :old.servico,  /*376*/  
           :new.servico, /*377*/   
           :old.codigo_ean,  /*378*/  
           :new.codigo_ean  /*379*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into I_OBRF_015_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           id_registro_OLD, /*8*/   
           id_registro_NEW, /*9*/   
           flag_importacao_OLD, /*10*/   
           flag_importacao_NEW, /*11*/   
           tipo_atualizacao_OLD, /*12*/   
           tipo_atualizacao_NEW, /*13*/   
           data_exportacao_OLD, /*14*/   
           data_exportacao_NEW, /*15*/   
           data_importacao_OLD, /*16*/   
           data_importacao_NEW, /*17*/   
           capa_ent_nrdoc_OLD, /*18*/   
           capa_ent_nrdoc_NEW, /*19*/   
           capa_ent_serie_OLD, /*20*/   
           capa_ent_serie_NEW, /*21*/   
           capa_ent_forcli9_OLD, /*22*/   
           capa_ent_forcli9_NEW, /*23*/   
           capa_ent_forcli4_OLD, /*24*/   
           capa_ent_forcli4_NEW, /*25*/   
           capa_ent_forcli2_OLD, /*26*/   
           capa_ent_forcli2_NEW, /*27*/   
           sequencia_OLD, /*28*/   
           sequencia_NEW, /*29*/   
           coditem_nivel99_OLD, /*30*/   
           coditem_nivel99_NEW, /*31*/   
           coditem_grupo_OLD, /*32*/   
           coditem_grupo_NEW, /*33*/   
           coditem_subgrupo_OLD, /*34*/   
           coditem_subgrupo_NEW, /*35*/   
           coditem_item_OLD, /*36*/   
           coditem_item_NEW, /*37*/   
           quantidade_OLD, /*38*/   
           quantidade_NEW, /*39*/   
           lote_entrega_OLD, /*40*/   
           lote_entrega_NEW, /*41*/   
           valor_unitario_OLD, /*42*/   
           valor_unitario_NEW, /*43*/   
           unidade_medida_OLD, /*44*/   
           unidade_medida_NEW, /*45*/   
           valor_ipi_OLD, /*46*/   
           valor_ipi_NEW, /*47*/   
           valor_total_OLD, /*48*/   
           valor_total_NEW, /*49*/   
           classific_fiscal_OLD, /*50*/   
           classific_fiscal_NEW, /*51*/   
           classif_contabil_OLD, /*52*/   
           classif_contabil_NEW, /*53*/   
           cod_vlfiscal_ipi_OLD, /*54*/   
           cod_vlfiscal_ipi_NEW, /*55*/   
           pedido_compra_OLD, /*56*/   
           pedido_compra_NEW, /*57*/   
           sequencia_pedido_OLD, /*58*/   
           sequencia_pedido_NEW, /*59*/   
           descricao_item_OLD, /*60*/   
           descricao_item_NEW, /*61*/   
           percentual_ipi_OLD, /*62*/   
           percentual_ipi_NEW, /*63*/   
           centro_custo_OLD, /*64*/   
           centro_custo_NEW, /*65*/   
           codigo_deposito_OLD, /*66*/   
           codigo_deposito_NEW, /*67*/   
           preco_custo_OLD, /*68*/   
           preco_custo_NEW, /*69*/   
           natitem_nat_oper_OLD, /*70*/   
           natitem_nat_oper_NEW, /*71*/   
           natitem_est_oper_OLD, /*72*/   
           natitem_est_oper_NEW, /*73*/   
           cod_vlfiscal_icm_OLD, /*74*/   
           cod_vlfiscal_icm_NEW, /*75*/   
           valor_icms_OLD, /*76*/   
           valor_icms_NEW, /*77*/   
           codigo_transacao_OLD, /*78*/   
           codigo_transacao_NEW, /*79*/   
           percentual_icm_OLD, /*80*/   
           percentual_icm_NEW, /*81*/   
           base_calc_icm_OLD, /*82*/   
           base_calc_icm_NEW, /*83*/   
           base_diferenca_OLD, /*84*/   
           base_diferenca_NEW, /*85*/   
           cvf_icm_diferenc_OLD, /*86*/   
           cvf_icm_diferenc_NEW, /*87*/   
           procedencia_OLD, /*88*/   
           procedencia_NEW, /*89*/   
           rateio_despesas_OLD, /*90*/   
           rateio_despesas_NEW, /*91*/   
           dif_aliquota_OLD, /*92*/   
           dif_aliquota_NEW, /*93*/   
           perc_dif_aliq_OLD, /*94*/   
           perc_dif_aliq_NEW, /*95*/   
           base_ipi_OLD, /*96*/   
           base_ipi_NEW, /*97*/   
           valor_pis_OLD, /*98*/   
           valor_pis_NEW, /*99*/   
           valor_cofins_OLD, /*100*/   
           valor_cofins_NEW, /*101*/   
           base_pis_cofins_OLD, /*102*/   
           base_pis_cofins_NEW, /*103*/   
           codigo_contabil_OLD, /*104*/   
           codigo_contabil_NEW, /*105*/   
           obs_livro1_OLD, /*106*/   
           obs_livro1_NEW, /*107*/   
           obs_livro2_OLD, /*108*/   
           obs_livro2_NEW, /*109*/   
           rateio_despesas_ipi_OLD, /*110*/   
           rateio_despesas_ipi_NEW, /*111*/   
           rateio_descontos_ipi_OLD, /*112*/   
           rateio_descontos_ipi_NEW, /*113*/   
           natureza_str_OLD, /*114*/   
           natureza_str_NEW, /*115*/   
           cvf_pis_OLD, /*116*/   
           cvf_pis_NEW, /*117*/   
           cvf_cofins_OLD, /*118*/   
           cvf_cofins_NEW, /*119*/   
           perc_pis_OLD, /*120*/   
           perc_pis_NEW, /*121*/   
           perc_cofins_OLD, /*122*/   
           perc_cofins_NEW, /*123*/   
           cvf_ipi_entrada_OLD, /*124*/   
           cvf_ipi_entrada_NEW, /*125*/   
           base_icms_subs_OLD, /*126*/   
           base_icms_subs_NEW, /*127*/   
           perc_icms_subs_OLD, /*128*/   
           perc_icms_subs_NEW, /*129*/   
           valor_icms_subs_OLD, /*130*/   
           valor_icms_subs_NEW, /*131*/   
           num_nota_orig_OLD, /*132*/   
           num_nota_orig_NEW, /*133*/   
           seq_nota_orig_OLD, /*134*/   
           seq_nota_orig_NEW, /*135*/   
           motivo_devolucao_OLD, /*136*/   
           motivo_devolucao_NEW, /*137*/   
           serie_nota_orig_OLD, /*138*/   
           serie_nota_orig_NEW, /*139*/   
           flag_devolucao_OLD, /*140*/   
           flag_devolucao_NEW, /*141*/   
           empr_nf_saida_OLD, /*142*/   
           empr_nf_saida_NEW, /*143*/   
           num_nf_saida_OLD, /*144*/   
           num_nf_saida_NEW, /*145*/   
           serie_nf_saida_OLD, /*146*/   
           serie_nf_saida_NEW, /*147*/   
           perc_icms_diferido_OLD, /*148*/   
           perc_icms_diferido_NEW, /*149*/   
           valor_icms_diferido_OLD, /*150*/   
           valor_icms_diferido_NEW, /*151*/   
           data_canc_nfisc_OLD, /*152*/   
           data_canc_nfisc_NEW, /*153*/   
           transacao_canc_nfisc_OLD, /*154*/   
           transacao_canc_nfisc_NEW, /*155*/   
           tipo_doc_importacao_sped_OLD, /*156*/   
           tipo_doc_importacao_sped_NEW, /*157*/   
           valor_pis_import_sped_OLD, /*158*/   
           valor_pis_import_sped_NEW, /*159*/   
           valor_cofins_import_sped_OLD, /*160*/   
           valor_cofins_import_sped_NEW, /*161*/   
           data_desembaraco_sped_OLD, /*162*/   
           data_desembaraco_sped_NEW, /*163*/   
           valor_cif_sped_OLD, /*164*/   
           valor_cif_sped_NEW, /*165*/   
           valor_desp_sem_icms_sped_OLD, /*166*/   
           valor_desp_sem_icms_sped_NEW, /*167*/   
           valor_desp_com_icms_sped_OLD, /*168*/   
           valor_desp_com_icms_sped_NEW, /*169*/   
           valor_imp_opera_finan_sped_OLD, /*170*/   
           valor_imp_opera_finan_sped_NEW, /*171*/   
           valor_imposto_import_sped_OLD, /*172*/   
           valor_imposto_import_sped_NEW, /*173*/   
           base_ipi_importacao_OLD, /*174*/   
           base_ipi_importacao_NEW, /*175*/   
           cvf_ipi_importacao_OLD, /*176*/   
           cvf_ipi_importacao_NEW, /*177*/   
           valor_ipi_importacao_OLD, /*178*/   
           valor_ipi_importacao_NEW, /*179*/   
           base_pis_cofins_importacao_OLD, /*180*/   
           base_pis_cofins_importacao_NEW, /*181*/   
           cvf_pis_importacao_OLD, /*182*/   
           cvf_pis_importacao_NEW, /*183*/   
           valor_pis_importacao_OLD, /*184*/   
           valor_pis_importacao_NEW, /*185*/   
           cvf_cofins_importacao_OLD, /*186*/   
           cvf_cofins_importacao_NEW, /*187*/   
           valor_cofins_importacao_OLD, /*188*/   
           valor_cofins_importacao_NEW, /*189*/   
           base_calculo_importacao_OLD, /*190*/   
           base_calculo_importacao_NEW, /*191*/   
           valor_imposto_importacao_OLD, /*192*/   
           valor_imposto_importacao_NEW, /*193*/   
           valor_iof_importacao_OLD, /*194*/   
           valor_iof_importacao_NEW, /*195*/   
           valor_desp_aduan_OLD, /*196*/   
           valor_desp_aduan_NEW, /*197*/   
           perc_ipi_importacao_OLD, /*198*/   
           perc_ipi_importacao_NEW, /*199*/   
           perc_pis_importacao_OLD, /*200*/   
           perc_pis_importacao_NEW, /*201*/   
           perc_cofins_importacao_OLD, /*202*/   
           perc_cofins_importacao_NEW, /*203*/   
           cod_csosn_OLD, /*204*/   
           cod_csosn_NEW, /*205*/   
           valor_frete_OLD, /*206*/   
           valor_frete_NEW, /*207*/   
           valor_desc_OLD, /*208*/   
           valor_desc_NEW, /*209*/   
           valor_outros_OLD, /*210*/   
           valor_outros_NEW, /*211*/   
           valor_seguro_OLD, /*212*/   
           valor_seguro_NEW, /*213*/   
           numero_adicao_OLD, /*214*/   
           numero_adicao_NEW, /*215*/   
           cod_base_cred_OLD, /*216*/   
           cod_base_cred_NEW, /*217*/   
           desconto_item_OLD, /*218*/   
           desconto_item_NEW, /*219*/   
           flag_saida_OLD, /*220*/   
           flag_saida_NEW, /*221*/   
           st_flag_cce_OLD, /*222*/   
           st_flag_cce_NEW, /*223*/   
           cd_cnpj_forne_9_OLD, /*224*/   
           cd_cnpj_forne_9_NEW, /*225*/   
           cd_cnpj_forne_4_OLD, /*226*/   
           cd_cnpj_forne_4_NEW, /*227*/   
           cd_cnpj_forne_2_OLD, /*228*/   
           cd_cnpj_forne_2_NEW, /*229*/   
           cd_seq_nota_frete_OLD, /*230*/   
           cd_seq_nota_frete_NEW, /*231*/   
           qtd_pcs_caixa_rfid_OLD, /*232*/   
           qtd_pcs_caixa_rfid_NEW, /*233*/   
           numero_di_OLD, /*234*/   
           numero_di_NEW, /*235*/   
           data_declaracao_OLD, /*236*/   
           data_declaracao_NEW, /*237*/   
           local_desembaraco_OLD, /*238*/   
           local_desembaraco_NEW, /*239*/   
           uf_desembaraco_OLD, /*240*/   
           uf_desembaraco_NEW, /*241*/   
           codigo_exportador_OLD, /*242*/   
           codigo_exportador_NEW, /*243*/   
           codigo_fabricante_OLD, /*244*/   
           codigo_fabricante_NEW, /*245*/   
           sequencia_adicao_OLD, /*246*/   
           sequencia_adicao_NEW, /*247*/   
           taxa_siscomex_imp_OLD, /*248*/   
           taxa_siscomex_imp_NEW, /*249*/   
           local_desembaraco_imp_OLD, /*250*/   
           local_desembaraco_imp_NEW, /*251*/   
           uf_desembaraco_imp_OLD, /*252*/   
           uf_desembaraco_imp_NEW, /*253*/   
           valor_aduaneiro_imp_OLD, /*254*/   
           valor_aduaneiro_imp_NEW, /*255*/   
           valor_ii_imp_OLD, /*256*/   
           valor_ii_imp_NEW, /*257*/   
           num_programa_emb_OLD, /*258*/   
           num_programa_emb_NEW, /*259*/   
           cest_OLD, /*260*/   
           cest_NEW, /*261*/   
           fator_conv_OLD, /*262*/   
           fator_conv_NEW, /*263*/   
           observacao_OLD, /*264*/   
           observacao_NEW, /*265*/   
           pdif51_OLD, /*266*/   
           pdif51_NEW, /*267*/   
           perc_fcp_uf_dest_OLD, /*268*/   
           perc_fcp_uf_dest_NEW, /*269*/   
           perc_icms_partilha_OLD, /*270*/   
           perc_icms_partilha_NEW, /*271*/   
           perc_icms_uf_dest_OLD, /*272*/   
           perc_icms_uf_dest_NEW, /*273*/   
           perc_redu_icm_OLD, /*274*/   
           perc_redu_icm_NEW, /*275*/   
           perc_redu_sub_OLD, /*276*/   
           perc_redu_sub_NEW, /*277*/   
           perc_substituicao_OLD, /*278*/   
           perc_substituicao_NEW, /*279*/   
           quantidade_conv_OLD, /*280*/   
           quantidade_conv_NEW, /*281*/   
           unidade_conv_OLD, /*282*/   
           unidade_conv_NEW, /*283*/   
           valor_afrmm_OLD, /*284*/   
           valor_afrmm_NEW, /*285*/   
           valor_anti_dump_OLD, /*286*/   
           valor_anti_dump_NEW, /*287*/   
           valor_conv_OLD, /*288*/   
           valor_conv_NEW, /*289*/   
           valor_desp_acess_OLD, /*290*/   
           valor_desp_acess_NEW, /*291*/   
           valor_desp_ntrib_OLD, /*292*/   
           valor_desp_ntrib_NEW, /*293*/   
           val_fcp_uf_dest_OLD, /*294*/   
           val_fcp_uf_dest_NEW, /*295*/   
           val_icms_uf_dest_OLD, /*296*/   
           val_icms_uf_dest_NEW, /*297*/   
           val_icms_uf_remet_OLD, /*298*/   
           val_icms_uf_remet_NEW, /*299*/   
           vicmsdif51_OLD, /*300*/   
           vicmsdif51_NEW, /*301*/   
           vicmsop51_OLD, /*302*/   
           vicmsop51_NEW, /*303*/   
           produto_forn_OLD, /*304*/   
           produto_forn_NEW, /*305*/   
           descricao_item_forn_OLD, /*306*/   
           descricao_item_forn_NEW, /*307*/   
           quantidade_forn_OLD, /*308*/   
           quantidade_forn_NEW, /*309*/   
           unidade_medida_forn_OLD, /*310*/   
           unidade_medida_forn_NEW, /*311*/   
           valor_unitario_forn_OLD, /*312*/   
           valor_unitario_forn_NEW, /*313*/   
           valor_total_forn_OLD, /*314*/   
           valor_total_forn_NEW, /*315*/   
           cfop_OLD, /*316*/   
           cfop_NEW, /*317*/   
           cst_icms_xml_OLD, /*318*/   
           cst_icms_xml_NEW, /*319*/   
           perc_icms_xml_OLD, /*320*/   
           perc_icms_xml_NEW, /*321*/   
           orig_icms_xml_OLD, /*322*/   
           orig_icms_xml_NEW, /*323*/   
           base_calc_icms_xml_OLD, /*324*/   
           base_calc_icms_xml_NEW, /*325*/   
           valor_icms_xml_OLD, /*326*/   
           valor_icms_xml_NEW, /*327*/   
           perc_icms_subs_xml_OLD, /*328*/   
           perc_icms_subs_xml_NEW, /*329*/   
           base_icms_subs_xml_OLD, /*330*/   
           base_icms_subs_xml_NEW, /*331*/   
           valor_icms_subs_xml_OLD, /*332*/   
           valor_icms_subs_xml_NEW, /*333*/   
           base_pis_cofins_xml_OLD, /*334*/   
           base_pis_cofins_xml_NEW, /*335*/   
           perc_pis_xml_OLD, /*336*/   
           perc_pis_xml_NEW, /*337*/   
           cst_pis_xml_OLD, /*338*/   
           cst_pis_xml_NEW, /*339*/   
           valor_pis_xml_OLD, /*340*/   
           valor_pis_xml_NEW, /*341*/   
           perc_cofins_xml_OLD, /*342*/   
           perc_cofins_xml_NEW, /*343*/   
           cst_cofins_xml_OLD, /*344*/   
           cst_cofins_xml_NEW, /*345*/   
           valor_cofins_xml_OLD, /*346*/   
           valor_cofins_xml_NEW, /*347*/   
           perc_ipi_xml_OLD, /*348*/   
           perc_ipi_xml_NEW, /*349*/   
           cst_ipi_xml_OLD, /*350*/   
           cst_ipi_xml_NEW, /*351*/   
           base_ipi_xml_OLD, /*352*/   
           base_ipi_xml_NEW, /*353*/   
           valor_ipi_xml_OLD, /*354*/   
           valor_ipi_xml_NEW, /*355*/   
           ncm_xml_OLD, /*356*/   
           ncm_xml_NEW, /*357*/   
           flag_rateado_OLD, /*358*/   
           flag_rateado_NEW, /*359*/   
           sequencia_original_OLD, /*360*/   
           sequencia_original_NEW, /*361*/   
           qtde_original_OLD, /*362*/   
           qtde_original_NEW, /*363*/   
           situacao_liberacao_OLD, /*364*/   
           situacao_liberacao_NEW, /*365*/   
           tp_via_transp_OLD, /*366*/   
           tp_via_transp_NEW, /*367*/   
           ind_orig_cod_prod_OLD, /*368*/   
           ind_orig_cod_prod_NEW, /*369*/   
           conferencia_almox_OLD, /*370*/   
           conferencia_almox_NEW, /*371*/   
           projeto_OLD, /*372*/   
           projeto_NEW, /*373*/   
           subprojeto_OLD, /*374*/   
           subprojeto_NEW, /*375*/   
           servico_OLD, /*376*/   
           servico_NEW, /*377*/   
           codigo_ean_OLD, /*378*/   
           codigo_ean_NEW /*379*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.id_registro, /*8*/   
           0, /*9*/
           :old.flag_importacao, /*10*/   
           0, /*11*/
           :old.tipo_atualizacao, /*12*/   
           '', /*13*/
           :old.data_exportacao, /*14*/   
           null, /*15*/
           :old.data_importacao, /*16*/   
           null, /*17*/
           :old.capa_ent_nrdoc, /*18*/   
           0, /*19*/
           :old.capa_ent_serie, /*20*/   
           '', /*21*/
           :old.capa_ent_forcli9, /*22*/   
           0, /*23*/
           :old.capa_ent_forcli4, /*24*/   
           0, /*25*/
           :old.capa_ent_forcli2, /*26*/   
           0, /*27*/
           :old.sequencia, /*28*/   
           0, /*29*/
           :old.coditem_nivel99, /*30*/   
           '', /*31*/
           :old.coditem_grupo, /*32*/   
           '', /*33*/
           :old.coditem_subgrupo, /*34*/   
           '', /*35*/
           :old.coditem_item, /*36*/   
           '', /*37*/
           :old.quantidade, /*38*/   
           0, /*39*/
           :old.lote_entrega, /*40*/   
           0, /*41*/
           :old.valor_unitario, /*42*/   
           0, /*43*/
           :old.unidade_medida, /*44*/   
           '', /*45*/
           :old.valor_ipi, /*46*/   
           0, /*47*/
           :old.valor_total, /*48*/   
           0, /*49*/
           :old.classific_fiscal, /*50*/   
           '', /*51*/
           :old.classif_contabil, /*52*/   
           0, /*53*/
           :old.cod_vlfiscal_ipi, /*54*/   
           0, /*55*/
           :old.pedido_compra, /*56*/   
           0, /*57*/
           :old.sequencia_pedido, /*58*/   
           0, /*59*/
           :old.descricao_item, /*60*/   
           '', /*61*/
           :old.percentual_ipi, /*62*/   
           0, /*63*/
           :old.centro_custo, /*64*/   
           0, /*65*/
           :old.codigo_deposito, /*66*/   
           0, /*67*/
           :old.preco_custo, /*68*/   
           0, /*69*/
           :old.natitem_nat_oper, /*70*/   
           0, /*71*/
           :old.natitem_est_oper, /*72*/   
           '', /*73*/
           :old.cod_vlfiscal_icm, /*74*/   
           0, /*75*/
           :old.valor_icms, /*76*/   
           0, /*77*/
           :old.codigo_transacao, /*78*/   
           0, /*79*/
           :old.percentual_icm, /*80*/   
           0, /*81*/
           :old.base_calc_icm, /*82*/   
           0, /*83*/
           :old.base_diferenca, /*84*/   
           0, /*85*/
           :old.cvf_icm_diferenc, /*86*/   
           0, /*87*/
           :old.procedencia, /*88*/   
           0, /*89*/
           :old.rateio_despesas, /*90*/   
           0, /*91*/
           :old.dif_aliquota, /*92*/   
           0, /*93*/
           :old.perc_dif_aliq, /*94*/   
           0, /*95*/
           :old.base_ipi, /*96*/   
           0, /*97*/
           :old.valor_pis, /*98*/   
           0, /*99*/
           :old.valor_cofins, /*100*/   
           0, /*101*/
           :old.base_pis_cofins, /*102*/   
           0, /*103*/
           :old.codigo_contabil, /*104*/   
           0, /*105*/
           :old.obs_livro1, /*106*/   
           '', /*107*/
           :old.obs_livro2, /*108*/   
           '', /*109*/
           :old.rateio_despesas_ipi, /*110*/   
           0, /*111*/
           :old.rateio_descontos_ipi, /*112*/   
           0, /*113*/
           :old.natureza_str, /*114*/   
           '', /*115*/
           :old.cvf_pis, /*116*/   
           0, /*117*/
           :old.cvf_cofins, /*118*/   
           0, /*119*/
           :old.perc_pis, /*120*/   
           0, /*121*/
           :old.perc_cofins, /*122*/   
           0, /*123*/
           :old.cvf_ipi_entrada, /*124*/   
           0, /*125*/
           :old.base_icms_subs, /*126*/   
           0, /*127*/
           :old.perc_icms_subs, /*128*/   
           0, /*129*/
           :old.valor_icms_subs, /*130*/   
           0, /*131*/
           :old.num_nota_orig, /*132*/   
           0, /*133*/
           :old.seq_nota_orig, /*134*/   
           0, /*135*/
           :old.motivo_devolucao, /*136*/   
           0, /*137*/
           :old.serie_nota_orig, /*138*/   
           '', /*139*/
           :old.flag_devolucao, /*140*/   
           0, /*141*/
           :old.empr_nf_saida, /*142*/   
           0, /*143*/
           :old.num_nf_saida, /*144*/   
           0, /*145*/
           :old.serie_nf_saida, /*146*/   
           '', /*147*/
           :old.perc_icms_diferido, /*148*/   
           0, /*149*/
           :old.valor_icms_diferido, /*150*/   
           0, /*151*/
           :old.data_canc_nfisc, /*152*/   
           null, /*153*/
           :old.transacao_canc_nfisc, /*154*/   
           0, /*155*/
           :old.tipo_doc_importacao_sped, /*156*/   
           '', /*157*/
           :old.valor_pis_import_sped, /*158*/   
           0, /*159*/
           :old.valor_cofins_import_sped, /*160*/   
           0, /*161*/
           :old.data_desembaraco_sped, /*162*/   
           null, /*163*/
           :old.valor_cif_sped, /*164*/   
           0, /*165*/
           :old.valor_desp_sem_icms_sped, /*166*/   
           0, /*167*/
           :old.valor_desp_com_icms_sped, /*168*/   
           0, /*169*/
           :old.valor_imposto_opera_finan_sped, /*170*/   
           0, /*171*/
           :old.valor_imposto_import_sped, /*172*/   
           0, /*173*/
           :old.base_ipi_importacao, /*174*/   
           0, /*175*/
           :old.cvf_ipi_importacao, /*176*/   
           0, /*177*/
           :old.valor_ipi_importacao, /*178*/   
           0, /*179*/
           :old.base_pis_cofins_importacao, /*180*/   
           0, /*181*/
           :old.cvf_pis_importacao, /*182*/   
           0, /*183*/
           :old.valor_pis_importacao, /*184*/   
           0, /*185*/
           :old.cvf_cofins_importacao, /*186*/   
           0, /*187*/
           :old.valor_cofins_importacao, /*188*/   
           0, /*189*/
           :old.base_calculo_importacao, /*190*/   
           0, /*191*/
           :old.valor_imposto_importacao, /*192*/   
           0, /*193*/
           :old.valor_iof_importacao, /*194*/   
           0, /*195*/
           :old.valor_desp_aduan, /*196*/   
           0, /*197*/
           :old.perc_ipi_importacao, /*198*/   
           0, /*199*/
           :old.perc_pis_importacao, /*200*/   
           0, /*201*/
           :old.perc_cofins_importacao, /*202*/   
           0, /*203*/
           :old.cod_csosn, /*204*/   
           0, /*205*/
           :old.valor_frete, /*206*/   
           0, /*207*/
           :old.valor_desc, /*208*/   
           0, /*209*/
           :old.valor_outros, /*210*/   
           0, /*211*/
           :old.valor_seguro, /*212*/   
           0, /*213*/
           :old.numero_adicao, /*214*/   
           0, /*215*/
           :old.cod_base_cred, /*216*/   
           0, /*217*/
           :old.desconto_item, /*218*/   
           0, /*219*/
           :old.flag_saida, /*220*/   
           0, /*221*/
           :old.st_flag_cce, /*222*/   
           0, /*223*/
           :old.cd_cnpj_forne_9, /*224*/   
           0, /*225*/
           :old.cd_cnpj_forne_4, /*226*/   
           0, /*227*/
           :old.cd_cnpj_forne_2, /*228*/   
           0, /*229*/
           :old.cd_seq_nota_frete, /*230*/   
           0, /*231*/
           :old.qtd_pcs_caixa_rfid, /*232*/   
           0, /*233*/
           :old.numero_di, /*234*/   
           '', /*235*/
           :old.data_declaracao, /*236*/   
           null, /*237*/
           :old.local_desembaraco, /*238*/   
           '', /*239*/
           :old.uf_desembaraco, /*240*/   
           '', /*241*/
           :old.codigo_exportador, /*242*/   
           '', /*243*/
           :old.codigo_fabricante, /*244*/   
           '', /*245*/
           :old.sequencia_adicao, /*246*/   
           0, /*247*/
           :old.taxa_siscomex_imp, /*248*/   
           0, /*249*/
           :old.local_desembaraco_imp, /*250*/   
           '', /*251*/
           :old.uf_desembaraco_imp, /*252*/   
           '', /*253*/
           :old.valor_aduaneiro_imp, /*254*/   
           0, /*255*/
           :old.valor_ii_imp, /*256*/   
           0, /*257*/
           :old.num_programa_emb, /*258*/   
           '', /*259*/
           :old.cest, /*260*/   
           '', /*261*/
           :old.fator_conv, /*262*/   
           0, /*263*/
           :old.observacao, /*264*/   
           '', /*265*/
           :old.pdif51, /*266*/   
           0, /*267*/
           :old.perc_fcp_uf_dest, /*268*/   
           0, /*269*/
           :old.perc_icms_partilha, /*270*/   
           0, /*271*/
           :old.perc_icms_uf_dest, /*272*/   
           0, /*273*/
           :old.perc_redu_icm, /*274*/   
           0, /*275*/
           :old.perc_redu_sub, /*276*/   
           0, /*277*/
           :old.perc_substituicao, /*278*/   
           0, /*279*/
           :old.quantidade_conv, /*280*/   
           0, /*281*/
           :old.unidade_conv, /*282*/   
           '', /*283*/
           :old.valor_afrmm, /*284*/   
           0, /*285*/
           :old.valor_anti_dump, /*286*/   
           0, /*287*/
           :old.valor_conv, /*288*/   
           0, /*289*/
           :old.valor_desp_acess, /*290*/   
           0, /*291*/
           :old.valor_desp_ntrib, /*292*/   
           0, /*293*/
           :old.val_fcp_uf_dest, /*294*/   
           0, /*295*/
           :old.val_icms_uf_dest, /*296*/   
           0, /*297*/
           :old.val_icms_uf_remet, /*298*/   
           0, /*299*/
           :old.vicmsdif51, /*300*/   
           0, /*301*/
           :old.vicmsop51, /*302*/   
           0, /*303*/
           :old.produto_forn, /*304*/   
           '', /*305*/
           :old.descricao_item_forn, /*306*/   
           '', /*307*/
           :old.quantidade_forn, /*308*/   
           0, /*309*/
           :old.unidade_medida_forn, /*310*/   
           '', /*311*/
           :old.valor_unitario_forn, /*312*/   
           0, /*313*/
           :old.valor_total_forn, /*314*/   
           0, /*315*/
           :old.cfop, /*316*/   
           '', /*317*/
           :old.cst_icms_xml, /*318*/   
           0, /*319*/
           :old.perc_icms_xml, /*320*/   
           0, /*321*/
           :old.orig_icms_xml, /*322*/   
           0, /*323*/
           :old.base_calc_icms_xml, /*324*/   
           0, /*325*/
           :old.valor_icms_xml, /*326*/   
           0, /*327*/
           :old.perc_icms_subs_xml, /*328*/   
           0, /*329*/
           :old.base_icms_subs_xml, /*330*/   
           0, /*331*/
           :old.valor_icms_subs_xml, /*332*/   
           0, /*333*/
           :old.base_pis_cofins_xml, /*334*/   
           0, /*335*/
           :old.perc_pis_xml, /*336*/   
           0, /*337*/
           :old.cst_pis_xml, /*338*/   
           0, /*339*/
           :old.valor_pis_xml, /*340*/   
           0, /*341*/
           :old.perc_cofins_xml, /*342*/   
           0, /*343*/
           :old.cst_cofins_xml, /*344*/   
           0, /*345*/
           :old.valor_cofins_xml, /*346*/   
           0, /*347*/
           :old.perc_ipi_xml, /*348*/   
           0, /*349*/
           :old.cst_ipi_xml, /*350*/   
           0, /*351*/
           :old.base_ipi_xml, /*352*/   
           0, /*353*/
           :old.valor_ipi_xml, /*354*/   
           0, /*355*/
           :old.ncm_xml, /*356*/   
           '', /*357*/
           :old.flag_rateado, /*358*/   
           0, /*359*/
           :old.sequencia_original, /*360*/   
           0, /*361*/
           :old.qtde_original, /*362*/   
           0, /*363*/
           :old.situacao_liberacao, /*364*/   
           0, /*365*/
           :old.tp_via_transp, /*366*/   
           0, /*367*/
           :old.ind_orig_cod_prod, /*368*/   
           '', /*369*/
           :old.conferencia_almox, /*370*/   
           0, /*371*/
           :old.projeto, /*372*/   
           0, /*373*/
           :old.subprojeto, /*374*/   
           0, /*375*/
           :old.servico, /*376*/   
           0, /*377*/
           :old.codigo_ean, /*378*/   
           '' /*379*/
         );    
    end;    
 end if;    
end inter_tr_I_OBRF_015_log;

/

exec inter_pr_recompile;
