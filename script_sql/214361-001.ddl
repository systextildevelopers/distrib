create table supr_012 (fornecedor9 number(9) not null, fornecedor4 number(4) not null, fornecedor2 number(2) not null,
ali_irrf number(7,2), cod_ret_irrf number(6),
ali_iss number(7,2), cod_ret_iss number(6),
ali_inss number(7,2), cod_ret_inss number(6),
ali_pis number(7,2), cod_ret_pis number(6),
ali_cofins number(7,2), cod_ret_cofins number(6),
ali_csl number(7,2), cod_ret_csl number(6),
ali_csrf number(7,2), cod_ret_csrf number(6),
primary key ( fornecedor9, fornecedor4, fornecedor2 ));

alter table supr_012 add constraint fk_supr_012 foreign key (fornecedor9, fornecedor4, fornecedor2)
  references supr_010 (fornecedor9, fornecedor4, fornecedor2) on delete cascade enable novalidate ;
commit;
