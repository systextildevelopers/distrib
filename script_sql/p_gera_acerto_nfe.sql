CREATE OR REPLACE PROCEDURE "P_GERA_ACERTO_NFE" (p_cod_empresa in number,
                                              p_cod_id in number,
                                              p_str_ent_sai in varchar2,
                                              p_str_erro out varchar2) is
  cursor c_nfe is
    select * from obrf_150
    where obrf_150.cod_empresa = p_cod_empresa
      and obrf_150.cod_usuario = p_cod_id
      and ((decode(obrf_150.tipo_documento,0,'E','S') = p_str_ent_sai) or (decode(obrf_150.tipo_emissao,0,'E','S') = p_str_ent_sai));
      --and obrf_150.tipo_documento <> 2; -- Cancelamento

  v_ERRO             EXCEPTION;
  v_FLAG_PROTOCOLO   number;
  for9      supr_010.fornecedor9%type;
  for4      supr_010.fornecedor4%type;
  for2      supr_010.fornecedor2%type;

begin

  for f_nfe in c_nfe
  LOOP
     -- 3 Inutiliza��o - 2 cancelamento - 4 Consulta Status NF-e - 5 Consulta Status Servi�o NF-e
     if  p_str_ent_sai = 'E'
     then
        if  f_nfe.cod_status is not null
        then


           if (f_nfe.dest_sigla_uf = 'EX')
           then
			  BEGIN
				  select fornecedor9  , fornecedor4 , fornecedor2 
				  into for9, for4, for2
				  from supr_010
				  where nome_fornecedor = f_nfe.dest_nome
					and sit_fornecedor  = 1
					and rownum          = 1;
			  EXCEPTION
                 WHEN OTHERS THEN
                   p_str_erro := 'N�o encontrou o fornecedor ' || f_nfe.dest_nome || ' ou o mesmo est� inativo.' || Chr(10) || SQLERRM;
                RAISE v_ERRO;
              END;
  
              BEGIN

                 update obrf_010 set -- /*N�o precisa mais - John*/ nr_recibo            = decode(f_nfe.cod_status,100,(decode(f_nfe.nr_recibo,null,' ',f_nfe.nr_recibo)),102,(decode(f_nfe.nr_recibo,null,' ',f_nfe.nr_recibo)),obrf_010.nr_recibo),
                                    nr_protocolo         = decode(f_nfe.cod_status,100,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                   102,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                   563,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                   205,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                   obrf_010.nr_protocolo),
                                    justificativa        = decode(f_nfe.cod_status,256,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                   102,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                   563,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                   null),
                                    nr_final_inut        = decode(f_nfe.nr_final_inut,null,' ',f_nfe.nr_final_inut),
                                    cod_status           = decode(f_nfe.cod_status,204,'100',
                                                                                   256,'102',
                                                                                   563,'102',
                                                                                   null,' ',
                                                                                   '999','-99',
                                                                                   206,'102',
                                                                                   f_nfe.cod_status),
                                    msg_status           = decode(f_nfe.msg_status,null,' ',f_nfe.msg_status),
                                    cod_solicitacao_nfe  = decode(f_nfe.cod_usuario,null,0,f_nfe.cod_usuario),
                                    ano_inutilizacao     = decode(f_nfe.ano_inutilizacao,null,' ',f_nfe.ano_inutilizacao),
                                    numero_danf_nfe      = (decode(f_nfe.chave_acesso,null,' ',f_nfe.chave_acesso)),
                                    cod_justificativa    = decode(f_nfe.cod_status,256,1,
                                                                                   102,1,
                                                                                   563,1,
                                                                                   0),
                                    xml_nprot            = f_nfe.nprot_xml,
                                    versao_systextilnfe  = f_nfe.versao_systextilnfe,
                                    nfe_contigencia      = decode(obrf_010.nfe_contigencia,0,f_nfe.nfe_contigencia,obrf_010.nfe_contigencia),
                                    chave_contingencia   = f_nfe.chave_contingencia,
                                    data_autorizacao_nfe = f_nfe.data_autorizacao_nfe,
                                    justificativa_cont   = f_nfe.justificativa_cont,
                                    data_hora_contigencia = f_nfe.data_hora_contigencia
                 where obrf_010.local_entrega           = p_cod_empresa
                   and obrf_010.documento               = f_nfe.numero
                   and obrf_010.serie                   = trim(to_char(f_nfe.serie))
                   and for9                             = obrf_010.cgc_cli_for_9
                   and for4                             = obrf_010.cgc_cli_for_4
                   and for2                             = obrf_010.cgc_cli_for_2;
              EXCEPTION
                 WHEN OTHERS THEN
                   p_str_erro := 'N�o atualizou dados na tabela obrf_010 (1)' || Chr(10) || SQLERRM;
                RAISE v_ERRO;
              END;
           else
              BEGIN
                 update obrf_010 set -- /*N�o precisa mais - John*/ nr_recibo            = decode(f_nfe.cod_status,100,(decode(f_nfe.nr_recibo,null,' ',f_nfe.nr_recibo)),102,(decode(f_nfe.nr_recibo,null,' ',f_nfe.nr_recibo)),obrf_010.nr_recibo),
                                     nr_protocolo         = decode(f_nfe.cod_status,100,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                    102,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                    563,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                    205,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                    obrf_010.nr_protocolo),
                                     justificativa        = decode(f_nfe.cod_status,256,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                    102,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                    563,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                    null),
                                     nr_final_inut        = decode(f_nfe.nr_final_inut,null,' ',f_nfe.nr_final_inut),
                                     cod_status           = decode(f_nfe.cod_status,204,'100',
                                                                                    256,'102',
                                                                                    563,'102',
                                                                                    null,' ',
                                                                                    '999','-99',
                                                                                    101, '101',
                                                                                    102, '102',
                                                                                    f_nfe.cod_status),
                                     msg_status           = decode(f_nfe.msg_status,null,' ',f_nfe.msg_status),
                                     cod_solicitacao_nfe  = decode(f_nfe.cod_usuario,null,0,f_nfe.cod_usuario),
                                     ano_inutilizacao     = decode(f_nfe.ano_inutilizacao,null,' ',f_nfe.ano_inutilizacao),
                                     numero_danf_nfe      = (decode(f_nfe.chave_acesso,null,' ',f_nfe.chave_acesso)),
                                     cod_justificativa    = decode(f_nfe.cod_status,256,1,
                                                                                    102,1,
                                                                                    563,1,
                                                                                    0),
                                     xml_nprot            = f_nfe.nprot_xml,
                                     versao_systextilnfe  = f_nfe.versao_systextilnfe,
                                    nfe_contigencia      = decode(obrf_010.nfe_contigencia,0,f_nfe.nfe_contigencia,obrf_010.nfe_contigencia),
                                     chave_contingencia   = f_nfe.chave_contingencia,
                                     data_autorizacao_nfe = f_nfe.data_autorizacao_nfe,
                                     justificativa_cont   = f_nfe.justificativa_cont,
                                     data_hora_contigencia = f_nfe.data_hora_contigencia
                 where obrf_010.local_entrega           = p_cod_empresa
                   and obrf_010.documento               = f_nfe.numero
                   and obrf_010.serie                   = trim(to_char(f_nfe.serie))
                   and obrf_010.cgc_cli_for_9         = f_nfe.dest_cnpj9
                   and obrf_010.cgc_cli_for_4     = f_nfe.dest_cnpj4
                   and obrf_010.cgc_cli_for_2     = f_nfe.dest_cnpj2
                   and obrf_010.natoper_est_oper <> 'EX';
              EXCEPTION
                WHEN OTHERS THEN
                   p_str_erro := 'N�o atualizou dados na tabela obrf_010 (1)' || Chr(10) || SQLERRM;
                RAISE v_ERRO;
              END;
           end if;
        end if;

     end if;
     if p_str_ent_sai = 'S'
     then
        if  f_nfe.cod_status is not null
        then

           BEGIN
             update fatu_050 set -- /*N�o precisa mais - John*/ nr_recibo            = decode(f_nfe.cod_status,100,(decode(f_nfe.nr_recibo,null,' ',f_nfe.nr_recibo)),102,(decode(f_nfe.nr_recibo,null,' ',f_nfe.nr_recibo)),fatu_050.nr_recibo),
                                 nr_protocolo         = decode(f_nfe.cod_status,100,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                102,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                563,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                205,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                fatu_050.nr_protocolo),
                                 justificativa        = decode(f_nfe.cod_status,256,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                102,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                563,(decode(f_nfe.nr_protocolo,null,' ',f_nfe.nr_protocolo)),
                                                                                null),
                                 nr_final_inut        = decode(f_nfe.nr_final_inut,null,' ',f_nfe.nr_final_inut),
                                 cod_status           = decode(f_nfe.cod_status,204,100,
                                                                                256,102,
                                                                                563,102,
                                                                                null,' ',
                                                                                101,101,
                                                                                102,102,
                                                                                '999','-99',
                                                                                f_nfe.cod_status),
                                 msg_status           = decode(f_nfe.msg_status,null,' ',f_nfe.msg_status),
                                 cod_solicitacao_nfe  = decode(f_nfe.cod_usuario,null,0,f_nfe.cod_usuario),
                                 ano_inutilizacao     = decode(f_nfe.ano_inutilizacao,null,' ',f_nfe.ano_inutilizacao),
                                 numero_danf_nfe      = (decode(f_nfe.chave_acesso,null,' ',f_nfe.chave_acesso)),
                                 cod_justificativa    = decode(f_nfe.cod_status,256,1,
                                                                                563,1,
                                                                                102,1,
                                                                                0),
                                 xml_nprot            = f_nfe.nprot_xml,
                                 versao_systextilnfe  = f_nfe.versao_systextilnfe,
                                 nfe_contigencia      = decode(fatu_050.nfe_contigencia,0,f_nfe.nfe_contigencia,fatu_050.nfe_contigencia),
                                 chave_contingencia   = f_nfe.chave_contingencia,
                                 data_autorizacao_nfe = f_nfe.data_autorizacao_nfe,
                                 justificativa_cont   = f_nfe.justificativa_cont,
                                 data_hora_contigencia = f_nfe.data_hora_contigencia
             where fatu_050.codigo_empresa  = p_cod_empresa
               and fatu_050.num_nota_fiscal = f_nfe.numero
               and fatu_050.serie_nota_fisc = to_char(f_nfe.serie);
           EXCEPTION
              WHEN OTHERS THEN
                 p_str_erro := 'N�o atualizou dados na tabela fatu_050 (1)' || Chr(10) || SQLERRM;
              RAISE v_ERRO;
           END;
        end if;
     end if;

     if  f_nfe.cod_status is not null
     then

        BEGIN
          insert into obrf_400
              (obrf_400.codigo_empresa
              ,obrf_400.num_nota_fiscal
              ,obrf_400.serie_nota_fisc
              ,obrf_400.cgc_9
              ,obrf_400.cgc_4
              ,obrf_400.cgc_2
              ,obrf_400.cod_status
              ,obrf_400.msg_status
              ,obrf_400.nr_protocolo
              ,obrf_400.data_inclusao)
           values
              (f_nfe.cod_empresa
              ,f_nfe.numero
              ,trim(to_char(f_nfe.serie))
              ,f_nfe.dest_cnpj9
              ,f_nfe.dest_cnpj4
              ,f_nfe.dest_cnpj2
              ,decode(f_nfe.cod_status,null,' ',f_nfe.cod_status)
              ,decode(f_nfe.msg_status,null,' ',f_nfe.msg_status)
              ,decode(f_nfe.cod_status,null,' ',f_nfe.nr_protocolo)
              ,sysdate
           );
        EXCEPTION
           WHEN OTHERS THEN
             p_str_erro := 'N�o atualizou dados na tabela obrf_400' || Chr(10) || SQLERRM;
           RAISE v_ERRO;
        END;
     end if;

     if f_nfe.cod_status is null or f_nfe.cod_status not in('103','104','105','106','110','101')
     then

        BEGIN
          delete from obrf_150
          where cod_empresa = p_cod_empresa
            and cod_usuario = p_cod_id
            and id = f_nfe.id
            and ((obrf_150.cod_status not in('103','104','105','106','110','101')) or (obrf_150.cod_status is null));

        EXCEPTION
          WHEN OTHERS THEN
            p_str_erro := 'N�o apagou os dados na tabela obrf_150 (1)' || Chr(10) || SQLERRM;
          RAISE v_erro;
        END;

        BEGIN
           delete from obrf_150
           where cod_empresa = p_cod_empresa
             and cod_usuario = p_cod_id
             and tipo_documento = 5;

        EXCEPTION
          WHEN OTHERS THEN
            p_str_erro := 'N�o apagou os dados na tabela obrf_150 (2)' || Chr(10) || SQLERRM;
          RAISE v_erro;
        END;
     end if;

  END LOOP;

commit;

end p_gera_acerto_nfe;
 

/

exec inter_pr_recompile;

