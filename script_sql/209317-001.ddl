create table cobr_003
(
codigo_empresa  number(3),
tipoInformacao  number(2),
tipoInformacaoCancRej  number(2),
usuario         varchar2(15),
nrDiasIni       number(2),
nrDiasFim       number(2),
PercIni         number(2),
PercFim         number(2)
);

ALTER TABLE cobr_003 ADD CONSTRAINT cobr_003 PRIMARY KEY(codigo_empresa, usuario);
