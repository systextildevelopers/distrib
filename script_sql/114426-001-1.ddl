ALTER TABLE PCPC_330 
ADD (ENDERECO VARCHAR2(10));

COMMENT ON COLUMN PCPC_330.ENDERECO IS 'Utilizado para empresas que utilizam o enderešamento de tags no estoque';

exec inter_pr_recompile;
/
