
  CREATE OR REPLACE FUNCTION "INTER_FN_PM_COND_SERASA" 
  (p_pedido_venda pedi_100.pedido_venda%type)
return number
is

   v_prazo_medio number;
   v_prazo_em_relacao_ao_inicial number;
   v_soma_dias number;
   v_soma_ocorrencias number;

begin

   begin
     v_soma_ocorrencias := 0;

     v_prazo_em_relacao_ao_inicial := 0;
     v_soma_dias                   := 0;

     for r075 in (select pedi_075.vencimento from pedi_100, pedi_075
                  where pedi_075.condicao_pagto = pedi_100.cond_pgto_venda
                    and pedi_100.pedido_venda   = p_pedido_venda
                  order by pedi_075.sequencia)
     loop
       v_soma_ocorrencias            := v_soma_ocorrencias            + 1;
       v_prazo_em_relacao_ao_inicial := v_prazo_em_relacao_ao_inicial + r075.vencimento;
       v_soma_dias                   := v_soma_dias                   + v_prazo_em_relacao_ao_inicial;
     end loop;

     v_prazo_medio := v_soma_dias / v_soma_ocorrencias;
     exception when others then
         v_prazo_medio := 0;
   end;

   if sql%notfound
   then
      v_prazo_medio := 0.00;
   end if;

   return(v_prazo_medio);

end INTER_FN_PM_COND_SERASA;
 

/

exec inter_pr_recompile;

