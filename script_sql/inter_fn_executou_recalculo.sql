
  CREATE OR REPLACE FUNCTION "INTER_FN_EXECUTOU_RECALCULO" 
   return number
is
   v_tem_reg_400 number;
begin

   v_tem_reg_400 := 0;

   begin
      select nvl(count(1),0)
      into v_tem_reg_400
      from basi_400
      where basi_400.nivel              = '0'
        and basi_400.grupo              = '00000'
        and basi_400.subgrupo           = '000'
        and basi_400.item               = '000000'
        and basi_400.tipo_informacao    = 144
        and basi_400.codigo_informacao  = 144;
   exception when others then
      v_tem_reg_400 := 0;
   end;

   return v_tem_reg_400;

end inter_fn_executou_recalculo;

 

/

exec inter_pr_recompile;

