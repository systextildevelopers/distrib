create or replace PACKAGE ST_PCK_PEDI AS
    
    function exists_portador(p_cod_portador number) return boolean;
    
    function get_cond_pagto_vencimentos_str(p_cod_condicao_pagamento number) return varchar2;

    function get_data_primeiro_venc(p_cod_condicao_pagamento number, p_data_base date) return date;

END ST_PCK_PEDI;
