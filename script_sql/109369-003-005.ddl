CREATE OR REPLACE VIEW INTER_VI_ORDENS_RAMA  AS
select pcpb_020.ORDEM_PRODUCAO,
       pcpb_020.PANO_SBG_NIVEL99,
       pcpb_020.PANO_SBG_GRUPO,
       pcpb_020.PANO_SBG_SUBGRUPO,
       pcpb_020.PANO_SBG_ITEM,
       pcpb_010.grupo_atendimento,
       (select nvl(max(pcpb_665.situacao),0)
        from pcpb_665
        where pcpb_665.ordem_beneficiamento = pcpb_020.ORDEM_PRODUCAO
          and pcpb_665.situacao in (1,9))   as bloq,
       (SELECT CAST(sum(pcpt_025.qtde_kg_inicial)as FLOAT)
        FROM PCPT_025
        WHERE PCPT_025.ORDEM_PRODUCAO = PCPB_010.ORDEM_PRODUCAO
          and pcpt_025.area_ordem = 2
       ) qtde_kg,
       (SELECT nvl(count(pcpt_025.codigo_rolo),0)
        FROM PCPT_025
        WHERE PCPT_025.ORDEM_PRODUCAO = PCPB_010.ORDEM_PRODUCAO
          and pcpt_025.area_ordem = 2
       ) nro_rolo,       
       basi_030.tipo_produto as tipo_produto_030,       
       pcpb_010.situacao_bloqueio as situacaoOB,        
       (SELECT tipo_produto
        FROM basi_020
        WHERE basi_020.BASI030_NIVEL030 = pcpb_020.PANO_SBG_NIVEL99
          and basi_020.BASI030_REFERENC = pcpb_020.PANO_SBG_GRUPO
          and basi_020.TAMANHO_REF      = pcpb_020.PANO_SBG_SUBGRUPO
       ) tipo_produto_020
from pcpb_010, pcpb_020, basi_030
where basi_030.nivel_estrutura   = '2'
  and basi_030.referencia        = pcpb_020.PANO_SBG_GRUPO
  and basi_030.tipo_produto     not in (3,4,6)
  and pcpb_010.ordem_producao   = pcpb_020.ORDEM_PRODUCAO
  and exists (select 1 from pcpc_010
              where pcpc_010.area_periodo = 2
                and pcpc_010.periodo_producao = pcpb_010.periodo_producao
                and pcpc_010.situacao_periodo <> 3
               )
  and pcpb_010.cod_cancelamento = 0              
  and exists (select 1
              from pcpb_015
              where pcpb_015.ordem_producao = pcpb_020.ORDEM_PRODUCAO
              and   pcpb_015.data_termino   is not null
              and   pcpb_015.hora_termino   is not null
              and   exists (select 1 from pcpb_665
                            where pcpb_665.ordem_beneficiamento = pcpb_010.ordem_producao
                           )
              and   pcpb_015.seq_operacao   in (select nvl(max(pcpb_015.seq_operacao),0)
                                            from pcpb_015
                                            where  pcpb_015.ordem_producao = pcpb_020.ORDEM_PRODUCAO                                           
                                            and    pcpb_015.seq_operacao   < (select max(pcpb_015.seq_operacao)
                                                                              from pcpb_015
                                                                              where pcpb_015.ordem_producao = pcpb_020.ORDEM_PRODUCAO
                                                                              and   pcpb_015.codigo_estagio in (select empr_001.estagio_acabam from empr_001)
                                                                              and   pcpb_015.data_termino is null )));
