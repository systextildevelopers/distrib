create or replace trigger "INTER_TR_HAUS_005_ID"
before insert on HAUS_005
 for each row

declare
  v_nr_registro number;

begin
  select ID_HAUS_005.nextval into v_nr_registro from dual;

  :new.id := v_nr_registro;

end INTER_TR_HAUS_005_ID;
