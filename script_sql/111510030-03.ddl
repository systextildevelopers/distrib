create table oper_275 (
cod_empresa         number(3) 		default 0,
cod_natur_operacao  number(9) 		default 0,
cod_cond_pgto       number(9) 		default 0,
cod_centro_custo    number(9) 		default 0,
cod_contabil 		number(6) 		default 0,
nivel				varchar2(1)		default '',
grupo				varchar2(5)		default '',
subgrupo			varchar2(3)		default '',
item				varchar2(6)		default '',
descricao_item		varchar2(70)	default ''
);

alter table oper_275 add constraint pk_oper_275 primary key(cod_empresa);

exec inter_pr_recompile;
