create or replace procedure inter_pr_gera_ctb_sped_i050(p_cod_empresa   IN NUMBER,
                                                        p_exercicio     IN NUMBER,
                                                        p_cod_plano_cta IN NUMBER,
                                                        p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar a tabela sped_cta_i050 - Plano de contas
   --
   -- Hist�ricos
   --
   -- Data    Autor    Observa��es
   --

   cursor cont535(p_cod_plano_cta NUMBER) IS
      SELECT *
      from   cont_535
      where  cod_plano_cta = p_cod_plano_cta
     and   exists (select 1 from cont_610
                     where cont_610.cod_empresa    = p_cod_empresa
                     and   cont_610.exercicio      = p_exercicio
                   and   cont_610.conta_reduzida   = cont_535.cod_reduzido
                   
                     );

   v_erro                     EXCEPTION;

   v_conta_contabil           varchar2(30);
   v_nome_conta               varchar2(85);
   v_ind_tipo_conta           varchar2(1);
   v_cod_conta_sup            varchar2(20);
   v_descricao_conta_sup      varchar2(40);
   v_codigo_agl               varchar2(20);
   v_descricao_agl            varchar2(40);
 
BEGIN

   p_des_erro := NULL;


   -- le o plano de contas
   for reg_cont535 in cont535(p_cod_plano_cta)
   loop

      -- monta a conta contabil acrescendo a subconta, quando necessario
      -- se for uma conta de resultado ou se a conta N�O exige subconta ou se a conta for sint�tica
      -- a conta ser� a propria conta. Caso contrario, concatenea o codigo da conta e o codigo da
      -- subconta
      
       if inter_fn_valida_conta_mae( reg_cont535.cod_reduzido, p_cod_plano_cta, p_cod_empresa, p_exercicio) = 0 then continue; end if;
       
       
       
       
       if reg_cont535.patr_result = 2 or reg_cont535.exige_subconta = 2
       or reg_cont535.tipo_conta  = 2
        then
          v_conta_contabil := reg_cont535.conta_contabil;
          v_nome_conta     := reg_cont535.descricao;
       else
          v_conta_contabil := rtrim(reg_cont535.conta_contabil) ||
                              ltrim(to_char(reg_cont535.subconta, '0000'));
          v_nome_conta     := rtrim(reg_cont535.descricao) || ' ' ||
                              ltrim(reg_cont535.descr_subconta);
       end if;

       -- Verifica se a conta contabil � analitica (1) ou sintetica (2)
       if reg_cont535.tipo_conta = 2
       then
          v_ind_tipo_conta := 'S';
       else
          v_ind_tipo_conta := 'A';
       end if;

       -- encontra a conta totalizadora (conta superior)
       if reg_cont535.nivel = 1
       then
          v_cod_conta_sup := '';
       else

          begin
             select conta_contabil,  descricao
                into   v_cod_conta_sup, v_descricao_conta_sup
                from   cont_535
                where  cod_plano_cta = p_cod_plano_cta
                and    cod_reduzido  = reg_cont535.conta_mae;
             exception
                when others then
                   v_cod_conta_sup       := NULL;
                   v_descricao_conta_sup := NULL;

                   inter_pr_insere_erro_sped('C', p_cod_empresa, 'Nao encontrou a conta totalizadora da conta ' ||
                                              reg_cont535.conta_contabil);

                commit;
          end;
       end if;

       -- se a conta for analitica e de resultado, busca os codigos de aglutinacao para
       -- montar o DRE.
       if reg_cont535.tipo_conta = 1 and reg_cont535.patr_result = 2
       then

          begin

             select cont_021.conta_dre,
                    cont_021.descricao_conta
             into   v_codigo_agl,
                    v_descricao_agl
             from cont_021, cont_022
             where cont_021.cod_plano_cta    = cont_022.cod_plano_cta
             and   cont_021.conta_dre        = cont_022.conta_dre
             and   cont_021.cod_plano_cta    = p_cod_plano_cta
             and   trim(cont_022.conta_analit_ctb) = reg_cont535.cod_reduzido
			 and   rownum = 1;
          exception
             when others then
                v_codigo_agl    := NULL;
                v_descricao_agl := ' ';
          end;

       else

         -- se a conta for analitica e patrominial, busca os codigos de aglutinacao para
         -- montar o balan�o patrominial.
         if reg_cont535.tipo_conta = 1 and reg_cont535.patr_result = 1
         then

            v_codigo_agl    := v_cod_conta_sup;
            v_descricao_agl := v_descricao_conta_sup;
         else

            v_codigo_agl    := NULL;
            v_descricao_agl := ' ';
         end if;
      end if;

      begin
         insert into sped_ctb_i050
            (cod_empresa,                   exercicio,
             cod_conta,                     data_ult_alteracao,
             cod_natureza,                  ind_tipo_conta,
             nivel,                         cod_conta_sup,
             nome_conta,                    cod_cta_ref,
             cod_aglutinacao,
             descricao_aglutinacao,         codigo_reduzido,
             cont_agrup_subconta,           natur_subconta)
          values
            (p_cod_empresa,                 p_exercicio,
             v_conta_contabil,              reg_cont535.data_cadastro,
             decode(reg_cont535.indic_natu_sped, 6, 5, decode(reg_cont535.indic_natu_sped, 8, 9, reg_cont535.indic_natu_sped)),
             v_ind_tipo_conta,
             reg_cont535.nivel,             v_cod_conta_sup,
             v_nome_conta,                  reg_cont535.conta_contabil_ref,
             v_codigo_agl,
             v_descricao_agl,               reg_cont535.cod_reduzido,
             reg_cont535.grupo_subconta_sped,reg_cont535.ind_nat_subconta_sped );

      exception
         when others then
         p_des_erro := 'Erro na inclus�o da tabela sped_ctb_i050 ' || Chr(10) || SQLERRM;
         RAISE V_ERRO;
      end;
     

   end loop;

   commit;

exception
   when V_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_gera_ctb_sped_i050 ' || Chr(10) || p_des_erro;

   when others then
      p_des_erro := 'Outros erros na procedure inter_pr_gera_ctb_sped_i050 ' || Chr(10) || SQLERRM;
end inter_pr_gera_ctb_sped_i050;

/

exec inter_pr_recompile;
