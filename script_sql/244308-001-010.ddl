CREATE TABLE  PROJ_070 
   (	SEQ_IMPORTACAO_ACABAMENTO NUMBER(30,0) NOT NULL ENABLE, 
	CODIGO_EMPRESA NUMBER(3,0) NOT NULL ENABLE, 
	DATA_IMPORTACAO DATE NOT NULL ENABLE, 
	MAQUINA VARCHAR2(25) NOT NULL ENABLE, 
	CODIGO_ARTIGO VARCHAR2(18) NOT NULL ENABLE, 
	NIVEL VARCHAR2(1) NOT NULL ENABLE, 
	GRUPO VARCHAR2(5) NOT NULL ENABLE, 
	SUBGRUPO VARCHAR2(3) NOT NULL ENABLE, 
	ITEM VARCHAR2(6) NOT NULL ENABLE, 
	QTDE_PRODUZIR NUMBER(12,0), 
	USUARIO_ATUALIZACAO VARCHAR2(30) NOT NULL ENABLE, 
	DATA_ATUALIZACAO DATE NOT NULL ENABLE, 
	 CONSTRAINT PK_PROJ_070 PRIMARY KEY (SEQ_IMPORTACAO_ACABAMENTO)
  USING INDEX  ENABLE, 
	 CONSTRAINT UK_PROJ_070 UNIQUE (CODIGO_EMPRESA, DATA_IMPORTACAO, MAQUINA, CODIGO_ARTIGO)
  USING INDEX  ENABLE
   );

/

CREATE OR REPLACE EDITIONABLE TRIGGER  INTER_TR_PROJ_070 
BEFORE INSERT OR UPDATE ON PROJ_070 FOR EACH ROW
DECLARE
BEGIN
   IF inserting then 
      IF :new.seq_importacao_acabamento is null THEN 
         :new.seq_importacao_acabamento:= seq_proj_070.nextval;
      END IF;
      IF :new.nivel is null THEN 
         :new.nivel:= substr(:new.codigo_artigo,0,instr(:new.codigo_artigo,'.',1)- 1);
      END IF;
      IF :new.grupo is null THEN 
         :new.grupo:= substr(:new.codigo_artigo,instr(:new.codigo_artigo,'.',1)+1, (instr(:new.codigo_artigo,'.',1,2) - instr(:new.codigo_artigo,'.',1,1)) -1 );
      END IF;
      IF :new.subgrupo is null THEN 
         :new.subgrupo:= substr(:new.codigo_artigo,instr(:new.codigo_artigo,'.',1,2)+1, (instr(:new.codigo_artigo,'.',1,3) - instr(:new.codigo_artigo,'.',1,2)) -1 );
      END IF;
      IF :new.item is null THEN 
         :new.item:= substr(:new.codigo_artigo,instr(:new.codigo_artigo,'.',1,3)+1);
      END IF;        
   END IF;
   :NEW.USUARIO_ATUALIZACAO:= nvl(sys_context('APEX$SESSION','APP_USER'),user);
   :NEW.DATA_ATUALIZACAO:= sysdate;   
END INTER_TR_PROJ_070;

/
ALTER TRIGGER  INTER_TR_PROJ_070 ENABLE;

/
