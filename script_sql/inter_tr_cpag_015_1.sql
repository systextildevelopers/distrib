
  CREATE OR REPLACE TRIGGER "INTER_TR_CPAG_015_1" 
   after insert or
         update of data_pagamento on cpag_015
   for each row

   -- Author  : Luiz Egberto Wassmansdorf
   -- Created : 28/08/08

declare
   v_data_ult_movim_pagto cpag_010.data_ult_movim_pagto%type;
   v_executa_trigger      number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      begin
         select data_ult_movim_pagto
         into   v_data_ult_movim_pagto
         from cpag_010
         where cpag_010.cgc_9          = :new.dupl_for_for_cli9
           and cpag_010.cgc_4          = :new.dupl_for_for_cli4
           and cpag_010.cgc_2          = :new.dupl_for_for_cli2
           and cpag_010.tipo_titulo    = :new.dupl_for_tipo_tit
           and cpag_010.nr_duplicata   = :new.dupl_for_nrduppag
           and cpag_010.parcela        = :new.dupl_for_no_parc;
      end;

      if :new.data_pagamento > v_data_ult_movim_pagto
      then v_data_ult_movim_pagto := :new.data_pagamento;
      end if;

      begin
         update cpag_010
         set data_ult_movim_pagto   = v_data_ult_movim_pagto
         where cpag_010.cgc_9          = :new.dupl_for_for_cli9
           and cpag_010.cgc_4          = :new.dupl_for_for_cli4
           and cpag_010.cgc_2          = :new.dupl_for_for_cli2
           and cpag_010.tipo_titulo    = :new.dupl_for_tipo_tit
           and cpag_010.nr_duplicata   = :new.dupl_for_nrduppag
           and cpag_010.parcela        = :new.dupl_for_no_parc;
      end;
   end if;
end inter_tr_cpag_015_1;

-- ALTER TRIGGER "INTER_TR_CPAG_015_1" ENABLE
 

/

exec inter_pr_recompile;

