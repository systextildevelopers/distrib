alter table INTE_520 drop constraint PK_INTE_520;
  
  drop index PK_INTE_520;

alter table INTE_520
  add constraint PK_INTE_520 primary key (CODIGO_ROLO,nota_fiscal,serie_fiscal_sai,cliente_cgc9,cliente_cgc4,cliente_cgc2);
  
  exec inter_pr_recompile;
