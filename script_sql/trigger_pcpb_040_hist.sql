  CREATE OR REPLACE TRIGGER "TRIGGER_PCPB_040_HIST" 
BEFORE UPDATE of codigo_estagio, seq_operacao, data_inicio, hora_inicio,
		 data_termino, hora_termino, ordem_producao, codigo_usuario,
		 seq_estagio, turno_producao
or DELETE
or INSERT
on pcpb_040
for each row

/*
26/01/2010 - Trigger Oficializada
713 - Antony
*/



declare
   ws_ordem_producao               number(9);
   ws_estagio                      number(5);
   ws_usuario                      number(5);
   ws_ordem_producao_old           number(9);
   ws_sid                          number(9);
   ws_estagio_old                  number(5);
   ws_usuario_old                  number(5);
   ws_tipo_ocorr                   varchar2(1);
   ws_usuario_rede                 varchar2(20);
   ws_maquina_rede                 varchar2(40);
   ws_aplicacao                    varchar2(20); 
   ws_usuario_systextil            varchar2(250);
   ws_nome_programa                hdoc_090.programa%type; 
   ws_empresa                      number(3);
   ws_locale_usuario               varchar2(5); 
   ws_data_inicio_old              date;
   ws_hora_inicio_old              date;
   
begin
   if INSERTING  then
      ws_ordem_producao       := :new.ordem_producao;
      ws_estagio              := :new.codigo_estagio;
      ws_usuario              := :new.codigo_usuario;
      ws_ordem_producao_old   := 0;
      ws_estagio_old          := 0;
      ws_usuario_old          := 0;
      ws_tipo_ocorr           := 'I';
      ws_data_inicio_old      := :new.data_inicio;
      ws_hora_inicio_old      := :new.hora_inicio;
   elsif UPDATING then
         ws_ordem_producao       := :new.ordem_producao;
         ws_estagio              := :new.codigo_estagio;
         ws_usuario              := :new.codigo_usuario;
         ws_ordem_producao_old   := :old.ordem_producao;
         ws_estagio_old          := :old.codigo_estagio;
         ws_usuario_old          := :old.codigo_usuario;
         ws_tipo_ocorr           := 'A';
         ws_data_inicio_old      := :old.data_inicio;
         ws_hora_inicio_old      := :old.hora_inicio;
      elsif DELETING then
            ws_ordem_producao        := 0;
            ws_estagio               := 0;
            ws_usuario               := 0;
            ws_ordem_producao_old    := :old.ordem_producao;
            ws_estagio_old           := :old.codigo_estagio;
            ws_usuario_old           := :old.codigo_usuario;
            ws_tipo_ocorr            := 'D';
            ws_data_inicio_old       := :old.data_inicio;
            ws_hora_inicio_old       := :old.hora_inicio;
   end if;
   
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicacao,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
  
   ws_nome_programa:= inter_fn_nome_programa(ws_sid);   
   
   INSERT INTO pcpb_040_hist
     (ordem_producao,        estagio,        usuario,
      ordem_producao_old,    estagio_old,    usuario_old,
      data_ocorr,            tipo_ocorr,     usuario_rede,
      maquina_rede,          aplicacao,      processo_systextil,
      data_inicio_old,       hora_inicio_old)
   VALUES
     (ws_ordem_producao,     ws_estagio,     ws_usuario,
      ws_ordem_producao_old, ws_estagio_old, ws_usuario_old,
      sysdate,               ws_tipo_ocorr,  ws_usuario_rede,
      ws_maquina_rede,       ws_aplicacao,   ws_nome_programa,
      ws_data_inicio_old,    ws_hora_inicio_old);

end trigger_pcpb_040_hist;

-- ALTER TRIGGER "TRIGGER_PCPB_040_HIST" ENABLE
 

/

exec inter_pr_recompile;

