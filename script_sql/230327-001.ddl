create table estq_420 (
  id number primary key not null,
  nome_arquivo    varchar2(400) not null,
  mime_type       varchar2(255) not null,
  conteudo        blob,
  data_importacao timestamp default current_timestamp not null,
  usuario_importacao varchar2(250),
  status number(2)
);

comment on column estq_420.id is 'Chave primaria';
comment on column estq_420.nome_arquivo is 'Nome do arquivo importado';
comment on column estq_420.mime_type is 'Tipo do arquivo';
comment on column estq_420.conteudo is 'Conteudo do arquivo (binario)';
comment on column estq_420.data_importacao is 'Data de importação do arquivo';
comment on column estq_420.usuario_importacao is 'Usuário que importou o arquivo no sistema';
comment on column estq_420.usuario_importacao is 'Status';

create sequence estq_420_seq;
