
  CREATE OR REPLACE FUNCTION "INTER_FN_CHECA_DATA" (p_cod_empresa      in number,
                                               p_data_lanc        in date,
                                               p_transacao        in number)
                                               return number is v_plano_exercicio number;
   v_gera_contab_func     fatu_500.gera_contabil%type;
   v_empresa_matriz       fatu_500.codigo_matriz%type;
   v_contab_trans         estq_005.atualiza_contabi%type;
   v_exercicio            cont_500.exercicio%type;
   v_sit_exercicio        cont_500.situacao%type;
   v_periodo              cont_510.periodo%type;
   v_situacao_periodo     cont_510.situacao%type;

begin

   /*
   Funcao checa_data convertida para o Oracle.
   Motivo: Vai ser utilizado nas procedures do Sped EFD PIS/COFINS
   */

   /*
   RETORNOS POSSIVEIS:
   = 0 => Nao e para gerar contabilidade, tudo ok
   > 0 => E para gerar contabilidade, tudo ok
   < 0 => E para gerar contabilidade, mas deu algum problema
   */

   v_plano_exercicio := 0;
   v_exercicio := 0;

   begin
      select fatu_500.gera_contabil
      into   v_gera_contab_func
      from fatu_500
      where fatu_500.codigo_empresa = p_cod_empresa;
   exception
       when no_data_found then
          v_gera_contab_func := 0;
   end;

   if v_gera_contab_func = 1
   then
      begin
         select fatu_500.codigo_matriz
         into   v_empresa_matriz
         from fatu_500
         where fatu_500.codigo_empresa = p_cod_empresa;
      exception
          when no_data_found then
             v_empresa_matriz := 0;
      end;

      if p_transacao > 0
      then
         begin
            select estq_005.atualiza_contabi
            into   v_contab_trans
            from estq_005
            where estq_005.codigo_transacao = p_transacao;
         exception
             when no_data_found then
                v_contab_trans := 2;
         end;
      else
         v_contab_trans := 1;
      end if; --if p_transacao > 0

      if v_contab_trans = 1 or v_contab_trans = 3
      then

         begin
            select cont_500.exercicio,     cont_500.situacao
            into   v_exercicio,            v_sit_exercicio
            from cont_500
            where cont_500.cod_empresa   = v_empresa_matriz
              and   cont_500.per_inicial <= p_data_lanc
              and   cont_500.per_final   >= p_data_lanc
              and   rownum = 1;
         exception
             when no_data_found then
                v_exercicio := -99;
         end;

         if v_exercicio <> -99
         then

            begin
               select cont_510.periodo, cont_510.situacao
               into   v_periodo,        v_situacao_periodo
               from cont_510
               where cont_510.cod_empresa    = v_empresa_matriz
                 and   cont_510.exercicio    = v_exercicio
                 and   cont_510.per_inicial <= p_data_lanc
                 and   cont_510.per_final   >= p_data_lanc
                 and   rownum = 1;
            exception
                when no_data_found then
                   v_exercicio := -99;
            end;
         end if; --if v_exercicio <> -99
      end if; --if v_contab_trans = 1 or v_contab_trans = 3
   end if; --if v_gera_contab_func = 1

   v_plano_exercicio := v_exercicio;

   return(v_plano_exercicio);
end inter_fn_checa_data;
 

/

exec inter_pr_recompile;

