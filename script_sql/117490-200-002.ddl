alter table pcpt_021 add (peso_previsto    NUMBER(9,3));

comment on column pcpt_021.peso_previsto IS 'Peso do rolo criado como previsto (com situacao = 0) no programa pcpb_f184. Campo atualizado ao confirmar o rolo no pcpb_f668.';

exec inter_pr_recompile;
