
  CREATE OR REPLACE TRIGGER "INTER_TR_INTE_989_LOG" 
   after insert or
         delete or
         update of nivel_produto,     grupo_produto,
                   subgru_produto,    item_produto,
                   fator_1,           fator_2,
                   preco_transitorio, preco_sugerido,
                   preco_venda,       sdsobservacao,
                   nvlprecomontagem
   on inte_989
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   long_aux                  varchar2(4000);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   if inserting
   then
      INSERT INTO hist_100
         ( tabela,            operacao,
           data_ocorr,        usuario_rede,
           maquina_rede,      str01,
           aplicacao,         long01
         )
      VALUES
        ( 'INTE_989',        'I',
          sysdate,           ws_usuario_rede,
          ws_maquina_rede,   :new.nivel_produto || '.' ||
                             :new.grupo_produto || '.' ||
                             :new.subgru_produto|| '.' ||
                             :new.item_produto,
          ws_aplicativo,     'DEFINICAO DE PRECO DE VENDA: ' || chr(10) || chr(10) ||
                             'PRODUTO: ' ||
                             :new.nivel_produto || '.' ||
                             :new.grupo_produto || '.' ||
                             :new.subgru_produto|| '.' ||
                             :new.item_produto  || chr(10) ||
                             'FATOR: ' ||
                             to_char(:new.fator_1,'9990.00') || chr(10) ||
                             'FATOR 2: ' ||
                             to_char(:new.fator_2,'9990.00') || chr(10) ||
                             'PRECO COMPRADOR: ' ||
                             to_char(:new.preco_sugerido,'99999990.00000') || chr(10) ||
                             'PRECO MONTAGEM LINHA: ' ||
                             to_char(:new.nvlprecomontagem,'99999990.00000') || chr(10) ||
                             'PRECO VENDA: ' ||
                             to_char(:new.preco_venda,'99999990.00000') || chr(10) ||
                             'PRECO TRANSITORIO: ' ||
                             to_char(:new.preco_transitorio,'99999990.00000') || chr(10) ||
                             'MOTIVO: ' ||
                             :new.sdsobservacao);
   end if;

   if updating and
      (:new.nivel_produto     <> :old.nivel_produto     or
       :new.grupo_produto     <> :old.grupo_produto     or
       :new.subgru_produto    <> :old.subgru_produto    or
       :new.item_produto      <> :old.item_produto      or
       :new.fator_1           <> :old.fator_1           or
       :new.fator_2           <> :old.fator_2           or
       :new.preco_transitorio <> :old.preco_transitorio or
       :new.preco_sugerido    <> :old.preco_sugerido    or
       :new.preco_venda       <> :old.preco_venda       or
       :new.nvlprecomontagem  <> :old.nvlprecomontagem  or
       :new.sdsobservacao     <> :old.sdsobservacao     or
       :old.sdsobservacao     is null)
   then
      long_aux := '';

      if :new.nivel_produto     <> :old.nivel_produto  or
         :new.grupo_produto     <> :old.grupo_produto  or
         :new.subgru_produto    <> :old.subgru_produto or
         :new.item_produto      <> :old.item_produto
      then
         long_aux := long_aux || 'PRODUTO: ' || :old.nivel_produto || '.'
                                             || :old.grupo_produto || '.'
                                             || :old.subgru_produto|| '.'
                                             || :old.item_produto  || ' -> '
                                             || :new.nivel_produto || '.'
                                             || :new.grupo_produto || '.'
                                             || :new.subgru_produto|| '.'
                                             || :new.item_produto  || chr(10);
      end if;

      if :new.fator_1           <> :old.fator_1
      then
         long_aux := long_aux || 'FATOR: ' || to_char(:old.fator_1,'9990.00') || ' -> '
                                           || to_char(:new.fator_1,'9990.00') || chr(10);
      end if;

      if :new.fator_2           <> :old.fator_2
      then
         long_aux := long_aux || 'FATOR 2: ' || to_char(:old.fator_2,'9990.00') || ' -> '
                                             || to_char(:new.fator_2,'9990.00') || chr(10);
      end if;

      if :new.preco_sugerido <> :old.preco_sugerido
      then
         long_aux := long_aux || 'PRECO COMPRADOR: ' || to_char(:old.preco_sugerido,'99999990.00000') || '->' ||
                                                        to_char(:new.preco_sugerido,'99999990.00000') || chr(10);
      end if;

      if :new.nvlprecomontagem <> :old.nvlprecomontagem
      then
         long_aux := long_aux || 'PRECO MOTAGEM DE LINHA: ' || to_char(:old.nvlprecomontagem,'99999990.00000') || '->' ||
                                                       to_char(:new.nvlprecomontagem,'99999990.00000') || chr(10);
      end if;

      if :new.preco_venda <> :old.preco_venda
      then
         long_aux := long_aux || 'PRECO VENDA:    ' || to_char(:old.preco_venda,'99999990.00000') || '->' ||
                                                       to_char(:new.preco_venda,'99999990.00000') || chr(10);
      end if;

      if :new.preco_transitorio <> :old.preco_transitorio
      then
         long_aux := long_aux || 'PRECO TRANSITORIO: ' || to_char(:old.preco_transitorio,'99999990.00000') || '->' ||
                                                          to_char(:new.preco_transitorio,'99999990.00000') || chr(10);
      end if;

      if :new.sdsobservacao     <> :old.sdsobservacao or
         (:old.sdsobservacao is null and :new.sdsobservacao is not null)
      then
          long_aux := long_aux || 'MOTIVO:        ' || :old.sdsobservacao ||' -> '
                                                    || :new.sdsobservacao || chr(10);
     end if;

      /******* FAZ INSERT ******/
      INSERT INTO hist_100
         ( tabela,            operacao,
           data_ocorr,        usuario_rede,
           maquina_rede,      str01,
           aplicacao,         long01
         )
      VALUES
         ( 'INTE_989',        'A',
           sysdate,           ws_usuario_rede,
           ws_maquina_rede,   :new.nivel_produto || '.'
                           || :new.grupo_produto || '.'
                           || :new.subgru_produto|| '.'
                           || :new.item_produto,
           ws_aplicativo,     long_aux
        );
   end if;
   if deleting
   then
      INSERT INTO hist_100
         ( tabela,            operacao,
           data_ocorr,        usuario_rede,
           maquina_rede,      str01,
           aplicacao,         long01
         )
      VALUES
         ( 'INTE_989',        'D',
           sysdate,           ws_usuario_rede,
           ws_maquina_rede,   :old.nivel_produto || '.'
                           || :old.grupo_produto || '.'
                           || :old.subgru_produto|| '.'
                           || :old.item_produto,
           ws_aplicativo,     'DEFINICAO DE PRECO DE VENDA' || chr(10) || chr(10)
                           || :old.nivel_produto || '.'
                           || :old.grupo_produto || '.'
                           || :old.subgru_produto|| '.'
                           || :old.item_produto
        );
   end if;
end inter_tr_inte_989_log;

-- ALTER TRIGGER "INTER_TR_INTE_989_LOG" ENABLE
 

/

exec inter_pr_recompile;

