create or replace package body "ST_PCK_PERIODO_CONTABIL" is

    function exist_periodos_posteriores(p_codigo_empresa number, p_exercicio number, p_data date, p_situacao number) 
    return boolean 
    as
        tmpInt number := 0;
    begin
        select NVL(MIN(1),0) 
        into tmpInt
        from cont_510
        where cod_empresa = p_codigo_empresa
        and exercicio = p_exercicio
        and per_inicial > p_data
        and situacao = p_situacao
        and ROWNUM = 1;

        return tmpInt = 1;
    end; 

    function exist_periodos_anteriores(p_codigo_empresa number, p_exercicio number, p_data date, p_situacao number) return boolean as
             tmpInt number := 0;
    begin
        select NVL(MIN(1),0) 
        into tmpInt
        from cont_510
        where cod_empresa = p_codigo_empresa
        and exercicio = p_exercicio
        and per_inicial < p_data
        and situacao = p_situacao
        and ROWNUM = 1;

        return tmpInt = 1;
    end; 

    function update_situ_periodo(p_codigo_empresa number, p_codigo_exercicio number, p_periodo number, p_new_sit number) 
    return boolean 
    as
    begin
        update cont_510 
        set situacao = p_new_sit
        where cod_empresa = p_codigo_empresa
        and exercicio = p_codigo_exercicio
        and periodo = p_periodo;

        if SQL%ROWCOUNT > 0 then
            return true;
        else 
            return false;
        end if;
    EXCEPTION WHEN OTHERS THEN
        return false;
    end;

    function get_dados(p_codigo_empresa number, p_exercicio number, p_periodo number, p_msg_erro in out varchar2) 
    return t_dados_cont_510 
    as
        row_var t_dados_cont_510;
    begin
        begin
            SELECT * 
            BULK COLLECT into row_var
            FROM cont_510
            WHERE cod_empresa = p_codigo_empresa 
            AND   exercicio = p_exercicio
            AND   periodo = p_periodo;
        exception when others then
            p_msg_erro := 'Não foi possível encontrar dados para este periodo contabil!.  cod_empresa:' || p_codigo_empresa ||
                                                                ' exercicio:' || p_exercicio ||
                                                                ' periodo:' || p_periodo;
        end;
             
        return row_var;
    end;

    function get_by_date( p_codigo_empresa number,    p_codigo_exercicio number, 
                          p_data date,                p_msg_erro in out varchar2) 
    return t_dados_cont_510 
    as
        row_var t_dados_cont_510;
    begin
        begin
            SELECT *
            BULK COLLECT into row_var
            FROM cont_510
            WHERE cod_empresa = p_codigo_empresa
            AND   exercicio = p_codigo_exercicio
            AND   p_data BETWEEN per_inicial and per_final;
        exception when others then
            p_msg_erro := 'Exercicio não cadastrado para a data e empresa informada!. Exercicio' || p_codigo_exercicio 
                            || 'data:' || p_data
                            || 'empresa' || p_codigo_empresa;
        end;

        return row_var;
    end;            

end "ST_PCK_PERIODO_CONTABIL";
