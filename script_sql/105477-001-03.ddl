alter table basi_240 add (unid_med_trib varchar2(10) default ' ');

comment on column basi_240.unid_med_trib is 'Unidade de medida tributaria criado pelo RFB';

exec inter_pr_recompile;

/
