create or replace procedure wms_pr_inventario (p_usuario_sistema    varchar2) is
  v_encontrou           number; --0: N�o Encontrou; 1: Encontrou
  v_retorno_int         number;
  v_entrou_loop         number; --0: N�o Entrou; 1: Entrou
  
  v_estoque_tag         number;
  v_tipo_volume         number;
  
  v_periodo_prod        number;
  v_ordem_prod          number;
  v_ordem_conf          number;
  v_seq_ordem           number;
  
  v_cod_empresa         number;
  v_ano_inventario      number;
  v_mes_inventario      number;
BEGIN
   
   for f_inventario in (
      select inte_wms_inventario.num_volume,  inte_wms_inventario.codigo_barras,
             inte_wms_inventario.seq_ean,     inte_wms_inventario.nivel_sku,
             inte_wms_inventario.grupo_sku,   inte_wms_inventario.subgrupo_sku,
             inte_wms_inventario.item_sku,    inte_wms_inventario.cod_deposito
      from   inte_wms_inventario
      order by inte_wms_inventario.timestamp_insercao
      )
   LOOP

      /*verificar se o dep�sito � de tag, se for de tag, descobrir a tag e buscar o estoque_tag*/
      begin
         select basi_205.tipo_volume,  basi_205.local_deposito
         into   v_tipo_volume,         v_cod_empresa
         from basi_205
         where basi_205.codigo_deposito = f_inventario.cod_deposito;
      exception
        when no_data_found then
           raise_application_error(-20000, 'N�o encontrou o dep�sito do invent�rio informado na tabela INTE_WMS_INVENTARIO no Syst�xtil. Dep�sito: ' || to_char(f_inventario.cod_deposito) || Chr(10) || SQLERRM);
      end;

      begin
         select fatu_503.ano_inventario,  fatu_503.mes_inventario
         into   v_ano_inventario,         v_mes_inventario
         from  fatu_503
         where fatu_503.codigo_empresa = v_cod_empresa;
      exception
        when no_data_found then
           v_ano_inventario := 0;
           v_mes_inventario := 0;
      end;
      
      if v_tipo_volume <> 1 --EAN: sem volume
      then
         v_estoque_tag := 0;
      else --TAG
         
         v_periodo_prod := Substr(f_inventario.codigo_barras,  1, 4);
         v_ordem_prod   := Substr(f_inventario.codigo_barras,  5, 9);
         v_ordem_conf   := Substr(f_inventario.codigo_barras, 14, 5);
         v_seq_ordem    := Substr(f_inventario.codigo_barras, 19, 4);
         
         begin
            select pcpc_330.estoque_tag
            into   v_estoque_tag
            from pcpc_330
            where pcpc_330.periodo_producao = v_periodo_prod
              and pcpc_330.ordem_producao   = v_ordem_prod
              and pcpc_330.ordem_confeccao  = v_ordem_conf
              and pcpc_330.sequencia        = v_seq_ordem;
         exception
           when no_data_found then
              raise_application_error(-20000, 'N�o encontrou a TAG na tabela pcpc_330 informada na tabela INTE_WMS_INVENTARIO. ' || Chr(10) || SQLERRM);
         end;

      end if;
      
      begin
   
         /*gravar na tabela de ocupa��o de endere�o fatu_115*/
         insert into estq_146
         ( estq_146.mes_inventario,    estq_146.ano_inventario,
           estq_146.num_volume,        estq_146.codigo_barras,
           estq_146.seq_ean,           estq_146.nivel,
           estq_146.grupo,             estq_146.subgrupo,
           estq_146.item,              estq_146.estoque_tag,
           estq_146.deposito,          estq_146.flg_atualizacao,
           estq_146.usuario,           estq_146.data_coleta
         ) 
         values
         ( v_mes_inventario,           v_ano_inventario,
           f_inventario.num_volume,    f_inventario.codigo_barras,
           f_inventario.seq_ean,       f_inventario.nivel_sku,
           f_inventario.grupo_sku,     f_inventario.subgrupo_sku,
           f_inventario.item_sku,      v_estoque_tag,
           f_inventario.cod_deposito,  0,
           p_usuario_sistema,          sysdate
         );
      exception
         when others then
            raise_application_error(-20000, 'N�o inseriu na tabela ESTQ_146.' || Chr(10) || SQLERRM);
      end;

   END LOOP;

   begin
   
      /*limpar a tabela de integra��o de invent�rio*/
      delete from inte_wms_inventario;
   exception
      when others then
         raise_application_error(-20000, 'N�o excluiu da tabela INTE_WMS_INVENTARIO.' || Chr(10) || SQLERRM);
   end;

   commit WORK;

END wms_pr_inventario;

/

exec inter_pr_recompile;

/* versao: 1 */


 exit;
