create or replace FUNCTION INTER_FN_QTD_PROD_OB_JA_INS (
    p_ordem_producao_ob      in number
) return number is 
    --PRAGMA AUTONOMOUS_TRANSACTION;
    --Se usar o pragma e tiver algum processo que insere varios produtos sem comitar,
    --  a transacao autonoma do pragma desta funcao nao ira encontrar os produtos nao comitados
    --Entao esta function deve ser chamada no BEFORE INSERT

    v_ret_qtde_prod_ob number;
begin

    v_ret_qtde_prod_ob := 0;

    begin
        select count(1)
        into v_ret_qtde_prod_ob
        from pcpb_020
        where pcpb_020.ordem_producao = p_ordem_producao_ob;
    exception when others then
        v_ret_qtde_prod_ob := 0;
    end;


    return v_ret_qtde_prod_ob;

end;
/
