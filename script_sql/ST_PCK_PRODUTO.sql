create or replace package ST_PCK_PRODUTO as


    TYPE t_dados_basi_010 IS TABLE OF basi_010%rowtype;
    TYPE t_dados_basi_020 IS TABLE OF basi_020%rowtype;
    TYPE t_dados_basi_030 IS TABLE OF basi_030%rowtype;
    TYPE t_dados_basi_220 IS TABLE OF basi_220%rowtype;

    function get_dados_produto_completo(p_nivel varchar2, p_grupo varchar2, p_subgrupo varchar2, p_item varchar2, p_msg_erro in out varchar2) return t_dados_basi_010;

    function get_dados_tamanho(p_nivel varchar2, p_grupo varchar2, p_subgrupo varchar2, p_msg_erro in out varchar2) return t_dados_basi_020;

    function get_dados_ordem_tamanho(p_tamanho varchar2, p_msg_erro in out varchar2) return t_dados_basi_220;

    function get_dados_referencia(p_nivel varchar2, p_grupo varchar2, p_msg_erro in out varchar2) return t_dados_basi_030;

    function get_descricao_basi063 ( p_idioma number, p_ncm varchar2) return varchar2;

    FUNCTION get_dados_produto_exportacao(p_msg_erro in out varchar2) return t_dados_basi_010;
end;
/
