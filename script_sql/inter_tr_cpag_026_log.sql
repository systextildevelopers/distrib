
  CREATE OR REPLACE TRIGGER "INTER_TR_CPAG_026_LOG" 
after insert or delete or update
on CPAG_026
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid); 

 if inserting
 then
    begin

        insert into CPAG_026_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           BANCO_OLD,   /*8*/
           BANCO_NEW,   /*9*/
           CONTA_CORRENTE_OLD,   /*10*/
           CONTA_CORRENTE_NEW,   /*11*/
           DATA_MOVIMENTO_OLD,   /*12*/
           DATA_MOVIMENTO_NEW,   /*13*/
           SEQUENCIA_LCTO_OLD,   /*14*/
           SEQUENCIA_LCTO_NEW,   /*15*/
           CODIGO_EVENTO_OLD,   /*16*/
           CODIGO_EVENTO_NEW,   /*17*/
           NR_DOCUMENTO_OLD,   /*18*/
           NR_DOCUMENTO_NEW,   /*19*/
           DEBITO_CREDITO_OLD,   /*20*/
           DEBITO_CREDITO_NEW,   /*21*/
           HISTORICO_OLD,   /*22*/
           HISTORICO_NEW,   /*23*/
           VALOR_CHEQUE_OLD,   /*24*/
           VALOR_CHEQUE_NEW,   /*25*/
           VALOR_COMPENSADO_OLD,   /*26*/
           VALOR_COMPENSADO_NEW,   /*27*/
           DATA_PRE_DATADO_OLD,   /*28*/
           DATA_PRE_DATADO_NEW,   /*29*/
           DATA_COMPENSACAO_OLD,   /*30*/
           DATA_COMPENSACAO_NEW,   /*31*/
           ORIGEM_LCTO_OLD,   /*32*/
           ORIGEM_LCTO_NEW,   /*33*/
           SITUACAO_LANCTO_OLD,   /*34*/
           SITUACAO_LANCTO_NEW,   /*35*/
           TIPO_MOVIMENTO_OLD,   /*36*/
           TIPO_MOVIMENTO_NEW,   /*37*/
           CODIGO_HISTORICO_OLD,   /*38*/
           CODIGO_HISTORICO_NEW,   /*39*/
           CODIGO_TRANSACAO_OLD,   /*40*/
           CODIGO_TRANSACAO_NEW,   /*41*/
           CODIGO_CONTABIL_OLD,   /*42*/
           CODIGO_CONTABIL_NEW,   /*43*/
           NUM_CONTABIL_OLD,   /*44*/
           NUM_CONTABIL_NEW,   /*45*/
           CENTRO_CUSTO_OLD,   /*46*/
           CENTRO_CUSTO_NEW,   /*47*/
           CONTROLE_LEITURA_OLD,   /*48*/
           CONTROLE_LEITURA_NEW,   /*49*/
           CONCILIADO_OLD,   /*50*/
           CONCILIADO_NEW,   /*51*/
           MARCA_CONCILIA_OLD,   /*52*/
           MARCA_CONCILIA_NEW,   /*53*/
           COD_CONCILIACAO_OLD,   /*54*/
           COD_CONCILIACAO_NEW,   /*55*/
           DATA_CONCILIACAO_OLD,   /*56*/
           DATA_CONCILIACAO_NEW,   /*57*/
           MAQUINA_USUARIO_OLD,   /*58*/
           MAQUINA_USUARIO_NEW,   /*59*/
           USUARIO_OLD,   /*60*/
           USUARIO_NEW,   /*61*/
           DATA_OCORR_OLD,   /*62*/
           DATA_OCORR_NEW,   /*63*/
           EXECUTA_TRIGGER_OLD,   /*64*/
           EXECUTA_TRIGGER_NEW,   /*65*/
           FLAG_CONCILIACAO_OLD,   /*66*/
           FLAG_CONCILIACAO_NEW    /*67*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.BANCO, /*9*/
           0,/*10*/
           :new.CONTA_CORRENTE, /*11*/
           null,/*12*/
           :new.DATA_MOVIMENTO, /*13*/
           0,/*14*/
           :new.SEQUENCIA_LCTO, /*15*/
           0,/*16*/
           :new.CODIGO_EVENTO, /*17*/
           0,/*18*/
           :new.NR_DOCUMENTO, /*19*/
           '',/*20*/
           :new.DEBITO_CREDITO, /*21*/
           '',/*22*/
           :new.HISTORICO, /*23*/
           0,/*24*/
           :new.VALOR_CHEQUE, /*25*/
           0,/*26*/
           :new.VALOR_COMPENSADO, /*27*/
           null,/*28*/
           :new.DATA_PRE_DATADO, /*29*/
           null,/*30*/
           :new.DATA_COMPENSACAO, /*31*/
           0,/*32*/
           :new.ORIGEM_LCTO, /*33*/
           0,/*34*/
           :new.SITUACAO_LANCTO, /*35*/
           0,/*36*/
           :new.TIPO_MOVIMENTO, /*37*/
           0,/*38*/
           :new.CODIGO_HISTORICO, /*39*/
           0,/*40*/
           :new.CODIGO_TRANSACAO, /*41*/
           0,/*42*/
           :new.CODIGO_CONTABIL, /*43*/
           0,/*44*/
           :new.NUM_CONTABIL, /*45*/
           0,/*46*/
           :new.CENTRO_CUSTO, /*47*/
           0,/*48*/
           :new.CONTROLE_LEITURA, /*49*/
           '',/*50*/
           :new.CONCILIADO, /*51*/
           0,/*52*/
           :new.MARCA_CONCILIA, /*53*/
           0,/*54*/
           :new.COD_CONCILIACAO, /*55*/
           null,/*56*/
           :new.DATA_CONCILIACAO, /*57*/
           '',/*58*/
           :new.MAQUINA_USUARIO, /*59*/
           '',/*60*/
           :new.USUARIO, /*61*/
           null,/*62*/
           :new.DATA_OCORR, /*63*/
           0,/*64*/
           :new.EXECUTA_TRIGGER, /*65*/
           0,/*66*/
           :new.FLAG_CONCILIACAO /*67*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into CPAG_026_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           BANCO_OLD, /*8*/
           BANCO_NEW, /*9*/
           CONTA_CORRENTE_OLD, /*10*/
           CONTA_CORRENTE_NEW, /*11*/
           DATA_MOVIMENTO_OLD, /*12*/
           DATA_MOVIMENTO_NEW, /*13*/
           SEQUENCIA_LCTO_OLD, /*14*/
           SEQUENCIA_LCTO_NEW, /*15*/
           CODIGO_EVENTO_OLD, /*16*/
           CODIGO_EVENTO_NEW, /*17*/
           NR_DOCUMENTO_OLD, /*18*/
           NR_DOCUMENTO_NEW, /*19*/
           DEBITO_CREDITO_OLD, /*20*/
           DEBITO_CREDITO_NEW, /*21*/
           HISTORICO_OLD, /*22*/
           HISTORICO_NEW, /*23*/
           VALOR_CHEQUE_OLD, /*24*/
           VALOR_CHEQUE_NEW, /*25*/
           VALOR_COMPENSADO_OLD, /*26*/
           VALOR_COMPENSADO_NEW, /*27*/
           DATA_PRE_DATADO_OLD, /*28*/
           DATA_PRE_DATADO_NEW, /*29*/
           DATA_COMPENSACAO_OLD, /*30*/
           DATA_COMPENSACAO_NEW, /*31*/
           ORIGEM_LCTO_OLD, /*32*/
           ORIGEM_LCTO_NEW, /*33*/
           SITUACAO_LANCTO_OLD, /*34*/
           SITUACAO_LANCTO_NEW, /*35*/
           TIPO_MOVIMENTO_OLD, /*36*/
           TIPO_MOVIMENTO_NEW, /*37*/
           CODIGO_HISTORICO_OLD, /*38*/
           CODIGO_HISTORICO_NEW, /*39*/
           CODIGO_TRANSACAO_OLD, /*40*/
           CODIGO_TRANSACAO_NEW, /*41*/
           CODIGO_CONTABIL_OLD, /*42*/
           CODIGO_CONTABIL_NEW, /*43*/
           NUM_CONTABIL_OLD, /*44*/
           NUM_CONTABIL_NEW, /*45*/
           CENTRO_CUSTO_OLD, /*46*/
           CENTRO_CUSTO_NEW, /*47*/
           CONTROLE_LEITURA_OLD, /*48*/
           CONTROLE_LEITURA_NEW, /*49*/
           CONCILIADO_OLD, /*50*/
           CONCILIADO_NEW, /*51*/
           MARCA_CONCILIA_OLD, /*52*/
           MARCA_CONCILIA_NEW, /*53*/
           COD_CONCILIACAO_OLD, /*54*/
           COD_CONCILIACAO_NEW, /*55*/
           DATA_CONCILIACAO_OLD, /*56*/
           DATA_CONCILIACAO_NEW, /*57*/
           MAQUINA_USUARIO_OLD, /*58*/
           MAQUINA_USUARIO_NEW, /*59*/
           USUARIO_OLD, /*60*/
           USUARIO_NEW, /*61*/
           DATA_OCORR_OLD, /*62*/
           DATA_OCORR_NEW, /*63*/
           EXECUTA_TRIGGER_OLD, /*64*/
           EXECUTA_TRIGGER_NEW, /*65*/
           FLAG_CONCILIACAO_OLD, /*66*/
           FLAG_CONCILIACAO_NEW  /*67*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.BANCO,  /*8*/
           :new.BANCO, /*9*/
           :old.CONTA_CORRENTE,  /*10*/
           :new.CONTA_CORRENTE, /*11*/
           :old.DATA_MOVIMENTO,  /*12*/
           :new.DATA_MOVIMENTO, /*13*/
           :old.SEQUENCIA_LCTO,  /*14*/
           :new.SEQUENCIA_LCTO, /*15*/
           :old.CODIGO_EVENTO,  /*16*/
           :new.CODIGO_EVENTO, /*17*/
           :old.NR_DOCUMENTO,  /*18*/
           :new.NR_DOCUMENTO, /*19*/
           :old.DEBITO_CREDITO,  /*20*/
           :new.DEBITO_CREDITO, /*21*/
           :old.HISTORICO,  /*22*/
           :new.HISTORICO, /*23*/
           :old.VALOR_CHEQUE,  /*24*/
           :new.VALOR_CHEQUE, /*25*/
           :old.VALOR_COMPENSADO,  /*26*/
           :new.VALOR_COMPENSADO, /*27*/
           :old.DATA_PRE_DATADO,  /*28*/
           :new.DATA_PRE_DATADO, /*29*/
           :old.DATA_COMPENSACAO,  /*30*/
           :new.DATA_COMPENSACAO, /*31*/
           :old.ORIGEM_LCTO,  /*32*/
           :new.ORIGEM_LCTO, /*33*/
           :old.SITUACAO_LANCTO,  /*34*/
           :new.SITUACAO_LANCTO, /*35*/
           :old.TIPO_MOVIMENTO,  /*36*/
           :new.TIPO_MOVIMENTO, /*37*/
           :old.CODIGO_HISTORICO,  /*38*/
           :new.CODIGO_HISTORICO, /*39*/
           :old.CODIGO_TRANSACAO,  /*40*/
           :new.CODIGO_TRANSACAO, /*41*/
           :old.CODIGO_CONTABIL,  /*42*/
           :new.CODIGO_CONTABIL, /*43*/
           :old.NUM_CONTABIL,  /*44*/
           :new.NUM_CONTABIL, /*45*/
           :old.CENTRO_CUSTO,  /*46*/
           :new.CENTRO_CUSTO, /*47*/
           :old.CONTROLE_LEITURA,  /*48*/
           :new.CONTROLE_LEITURA, /*49*/
           :old.CONCILIADO,  /*50*/
           :new.CONCILIADO, /*51*/
           :old.MARCA_CONCILIA,  /*52*/
           :new.MARCA_CONCILIA, /*53*/
           :old.COD_CONCILIACAO,  /*54*/
           :new.COD_CONCILIACAO, /*55*/
           :old.DATA_CONCILIACAO,  /*56*/
           :new.DATA_CONCILIACAO, /*57*/
           :old.MAQUINA_USUARIO,  /*58*/
           :new.MAQUINA_USUARIO, /*59*/
           :old.USUARIO,  /*60*/
           :new.USUARIO, /*61*/
           :old.DATA_OCORR,  /*62*/
           :new.DATA_OCORR, /*63*/
           :old.EXECUTA_TRIGGER,  /*64*/
           :new.EXECUTA_TRIGGER, /*65*/
           :old.FLAG_CONCILIACAO,  /*66*/
           :new.FLAG_CONCILIACAO  /*67*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into CPAG_026_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           BANCO_OLD, /*8*/
           BANCO_NEW, /*9*/
           CONTA_CORRENTE_OLD, /*10*/
           CONTA_CORRENTE_NEW, /*11*/
           DATA_MOVIMENTO_OLD, /*12*/
           DATA_MOVIMENTO_NEW, /*13*/
           SEQUENCIA_LCTO_OLD, /*14*/
           SEQUENCIA_LCTO_NEW, /*15*/
           CODIGO_EVENTO_OLD, /*16*/
           CODIGO_EVENTO_NEW, /*17*/
           NR_DOCUMENTO_OLD, /*18*/
           NR_DOCUMENTO_NEW, /*19*/
           DEBITO_CREDITO_OLD, /*20*/
           DEBITO_CREDITO_NEW, /*21*/
           HISTORICO_OLD, /*22*/
           HISTORICO_NEW, /*23*/
           VALOR_CHEQUE_OLD, /*24*/
           VALOR_CHEQUE_NEW, /*25*/
           VALOR_COMPENSADO_OLD, /*26*/
           VALOR_COMPENSADO_NEW, /*27*/
           DATA_PRE_DATADO_OLD, /*28*/
           DATA_PRE_DATADO_NEW, /*29*/
           DATA_COMPENSACAO_OLD, /*30*/
           DATA_COMPENSACAO_NEW, /*31*/
           ORIGEM_LCTO_OLD, /*32*/
           ORIGEM_LCTO_NEW, /*33*/
           SITUACAO_LANCTO_OLD, /*34*/
           SITUACAO_LANCTO_NEW, /*35*/
           TIPO_MOVIMENTO_OLD, /*36*/
           TIPO_MOVIMENTO_NEW, /*37*/
           CODIGO_HISTORICO_OLD, /*38*/
           CODIGO_HISTORICO_NEW, /*39*/
           CODIGO_TRANSACAO_OLD, /*40*/
           CODIGO_TRANSACAO_NEW, /*41*/
           CODIGO_CONTABIL_OLD, /*42*/
           CODIGO_CONTABIL_NEW, /*43*/
           NUM_CONTABIL_OLD, /*44*/
           NUM_CONTABIL_NEW, /*45*/
           CENTRO_CUSTO_OLD, /*46*/
           CENTRO_CUSTO_NEW, /*47*/
           CONTROLE_LEITURA_OLD, /*48*/
           CONTROLE_LEITURA_NEW, /*49*/
           CONCILIADO_OLD, /*50*/
           CONCILIADO_NEW, /*51*/
           MARCA_CONCILIA_OLD, /*52*/
           MARCA_CONCILIA_NEW, /*53*/
           COD_CONCILIACAO_OLD, /*54*/
           COD_CONCILIACAO_NEW, /*55*/
           DATA_CONCILIACAO_OLD, /*56*/
           DATA_CONCILIACAO_NEW, /*57*/
           MAQUINA_USUARIO_OLD, /*58*/
           MAQUINA_USUARIO_NEW, /*59*/
           USUARIO_OLD, /*60*/
           USUARIO_NEW, /*61*/
           DATA_OCORR_OLD, /*62*/
           DATA_OCORR_NEW, /*63*/
           EXECUTA_TRIGGER_OLD, /*64*/
           EXECUTA_TRIGGER_NEW, /*65*/
           FLAG_CONCILIACAO_OLD, /*66*/
           FLAG_CONCILIACAO_NEW /*67*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.BANCO, /*8*/
           0, /*9*/
           :old.CONTA_CORRENTE, /*10*/
           0, /*11*/
           :old.DATA_MOVIMENTO, /*12*/
           null, /*13*/
           :old.SEQUENCIA_LCTO, /*14*/
           0, /*15*/
           :old.CODIGO_EVENTO, /*16*/
           0, /*17*/
           :old.NR_DOCUMENTO, /*18*/
           0, /*19*/
           :old.DEBITO_CREDITO, /*20*/
           '', /*21*/
           :old.HISTORICO, /*22*/
           '', /*23*/
           :old.VALOR_CHEQUE, /*24*/
           0, /*25*/
           :old.VALOR_COMPENSADO, /*26*/
           0, /*27*/
           :old.DATA_PRE_DATADO, /*28*/
           null, /*29*/
           :old.DATA_COMPENSACAO, /*30*/
           null, /*31*/
           :old.ORIGEM_LCTO, /*32*/
           0, /*33*/
           :old.SITUACAO_LANCTO, /*34*/
           0, /*35*/
           :old.TIPO_MOVIMENTO, /*36*/
           0, /*37*/
           :old.CODIGO_HISTORICO, /*38*/
           0, /*39*/
           :old.CODIGO_TRANSACAO, /*40*/
           0, /*41*/
           :old.CODIGO_CONTABIL, /*42*/
           0, /*43*/
           :old.NUM_CONTABIL, /*44*/
           0, /*45*/
           :old.CENTRO_CUSTO, /*46*/
           0, /*47*/
           :old.CONTROLE_LEITURA, /*48*/
           0, /*49*/
           :old.CONCILIADO, /*50*/
           '', /*51*/
           :old.MARCA_CONCILIA, /*52*/
           0, /*53*/
           :old.COD_CONCILIACAO, /*54*/
           0, /*55*/
           :old.DATA_CONCILIACAO, /*56*/
           null, /*57*/
           :old.MAQUINA_USUARIO, /*58*/
           '', /*59*/
           :old.USUARIO, /*60*/
           '', /*61*/
           :old.DATA_OCORR, /*62*/
           null, /*63*/
           :old.EXECUTA_TRIGGER, /*64*/
           0, /*65*/
           :old.FLAG_CONCILIACAO, /*66*/
           0 /*67*/
         );
    end;
 end if;
end inter_tr_CPAG_026_log;

-- ALTER TRIGGER "INTER_TR_CPAG_026_LOG" ENABLE
 

/

exec inter_pr_recompile;

