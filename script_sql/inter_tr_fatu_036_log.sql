
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_036_LOG" 
after insert or delete
or update of nr_solicitacao, pedido_venda, seq_pedido, nota_fiscal_entrada,
	serie_nf_entrada, fornecedor_9, fornecedor_4, fornecedor_2,
	seq_nota_entrada, quantidade_devolucao, valor_unitario_devol, valor_devolucao,
	sit_devolucao
on FATU_036
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   
   v_nome_programa := inter_fn_nome_programa(ws_sid); 


 if inserting
 then
    begin

        insert into FATU_036_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           NR_SOLICITACAO_OLD,   /*8*/
           NR_SOLICITACAO_NEW,   /*9*/
           PEDIDO_VENDA_OLD,   /*10*/
           PEDIDO_VENDA_NEW,   /*11*/
           SEQ_PEDIDO_OLD,   /*12*/
           SEQ_PEDIDO_NEW,   /*13*/
           NOTA_FISCAL_ENTRADA_OLD,   /*14*/
           NOTA_FISCAL_ENTRADA_NEW,   /*15*/
           SERIE_NF_ENTRADA_OLD,   /*16*/
           SERIE_NF_ENTRADA_NEW,   /*17*/
           FORNECEDOR_9_OLD,   /*18*/
           FORNECEDOR_9_NEW,   /*19*/
           FORNECEDOR_4_OLD,   /*20*/
           FORNECEDOR_4_NEW,   /*21*/
           FORNECEDOR_2_OLD,   /*22*/
           FORNECEDOR_2_NEW,   /*23*/
           SEQ_NOTA_ENTRADA_OLD,   /*24*/
           SEQ_NOTA_ENTRADA_NEW,   /*25*/
           QUANTIDADE_DEVOLUCAO_OLD,   /*26*/
           QUANTIDADE_DEVOLUCAO_NEW,   /*27*/
           VALOR_UNITARIO_DEVOL_OLD,   /*28*/
           VALOR_UNITARIO_DEVOL_NEW,   /*29*/
           VALOR_DEVOLUCAO_OLD,   /*30*/
           VALOR_DEVOLUCAO_NEW,   /*31*/
           SIT_DEVOLUCAO_OLD,   /*32*/
           SIT_DEVOLUCAO_NEW    /*33*/
        ) values (
            'I', /*1*/
            sysdate, /*2*/
            sysdate,/*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           0,/*8*/
           :new.NR_SOLICITACAO, /*9*/
           0,/*10*/
           :new.PEDIDO_VENDA, /*11*/
           0,/*12*/
           :new.SEQ_PEDIDO, /*13*/
           0,/*14*/
           :new.NOTA_FISCAL_ENTRADA, /*15*/
           '',/*16*/
           :new.SERIE_NF_ENTRADA, /*17*/
           0,/*18*/
           :new.FORNECEDOR_9, /*19*/
           0,/*20*/
           :new.FORNECEDOR_4, /*21*/
           0,/*22*/
           :new.FORNECEDOR_2, /*23*/
           0,/*24*/
           :new.SEQ_NOTA_ENTRADA, /*25*/
           0,/*26*/
           :new.QUANTIDADE_DEVOLUCAO, /*27*/
           0,/*28*/
           :new.VALOR_UNITARIO_DEVOL, /*29*/
           0,/*30*/
           :new.VALOR_DEVOLUCAO, /*31*/
           0,/*32*/
           :new.SIT_DEVOLUCAO /*33*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into FATU_036_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           NR_SOLICITACAO_OLD, /*8*/
           NR_SOLICITACAO_NEW, /*9*/
           PEDIDO_VENDA_OLD, /*10*/
           PEDIDO_VENDA_NEW, /*11*/
           SEQ_PEDIDO_OLD, /*12*/
           SEQ_PEDIDO_NEW, /*13*/
           NOTA_FISCAL_ENTRADA_OLD, /*14*/
           NOTA_FISCAL_ENTRADA_NEW, /*15*/
           SERIE_NF_ENTRADA_OLD, /*16*/
           SERIE_NF_ENTRADA_NEW, /*17*/
           FORNECEDOR_9_OLD, /*18*/
           FORNECEDOR_9_NEW, /*19*/
           FORNECEDOR_4_OLD, /*20*/
           FORNECEDOR_4_NEW, /*21*/
           FORNECEDOR_2_OLD, /*22*/
           FORNECEDOR_2_NEW, /*23*/
           SEQ_NOTA_ENTRADA_OLD, /*24*/
           SEQ_NOTA_ENTRADA_NEW, /*25*/
           QUANTIDADE_DEVOLUCAO_OLD, /*26*/
           QUANTIDADE_DEVOLUCAO_NEW, /*27*/
           VALOR_UNITARIO_DEVOL_OLD, /*28*/
           VALOR_UNITARIO_DEVOL_NEW, /*29*/
           VALOR_DEVOLUCAO_OLD, /*30*/
           VALOR_DEVOLUCAO_NEW, /*31*/
           SIT_DEVOLUCAO_OLD, /*32*/
           SIT_DEVOLUCAO_NEW  /*33*/
        ) values (
            'A', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.NR_SOLICITACAO,  /*8*/
           :new.NR_SOLICITACAO, /*9*/
           :old.PEDIDO_VENDA,  /*10*/
           :new.PEDIDO_VENDA, /*11*/
           :old.SEQ_PEDIDO,  /*12*/
           :new.SEQ_PEDIDO, /*13*/
           :old.NOTA_FISCAL_ENTRADA,  /*14*/
           :new.NOTA_FISCAL_ENTRADA, /*15*/
           :old.SERIE_NF_ENTRADA,  /*16*/
           :new.SERIE_NF_ENTRADA, /*17*/
           :old.FORNECEDOR_9,  /*18*/
           :new.FORNECEDOR_9, /*19*/
           :old.FORNECEDOR_4,  /*20*/
           :new.FORNECEDOR_4, /*21*/
           :old.FORNECEDOR_2,  /*22*/
           :new.FORNECEDOR_2, /*23*/
           :old.SEQ_NOTA_ENTRADA,  /*24*/
           :new.SEQ_NOTA_ENTRADA, /*25*/
           :old.QUANTIDADE_DEVOLUCAO,  /*26*/
           :new.QUANTIDADE_DEVOLUCAO, /*27*/
           :old.VALOR_UNITARIO_DEVOL,  /*28*/
           :new.VALOR_UNITARIO_DEVOL, /*29*/
           :old.VALOR_DEVOLUCAO,  /*30*/
           :new.VALOR_DEVOLUCAO, /*31*/
           :old.SIT_DEVOLUCAO,  /*32*/
           :new.SIT_DEVOLUCAO  /*33*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into FATU_036_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           NR_SOLICITACAO_OLD, /*8*/
           NR_SOLICITACAO_NEW, /*9*/
           PEDIDO_VENDA_OLD, /*10*/
           PEDIDO_VENDA_NEW, /*11*/
           SEQ_PEDIDO_OLD, /*12*/
           SEQ_PEDIDO_NEW, /*13*/
           NOTA_FISCAL_ENTRADA_OLD, /*14*/
           NOTA_FISCAL_ENTRADA_NEW, /*15*/
           SERIE_NF_ENTRADA_OLD, /*16*/
           SERIE_NF_ENTRADA_NEW, /*17*/
           FORNECEDOR_9_OLD, /*18*/
           FORNECEDOR_9_NEW, /*19*/
           FORNECEDOR_4_OLD, /*20*/
           FORNECEDOR_4_NEW, /*21*/
           FORNECEDOR_2_OLD, /*22*/
           FORNECEDOR_2_NEW, /*23*/
           SEQ_NOTA_ENTRADA_OLD, /*24*/
           SEQ_NOTA_ENTRADA_NEW, /*25*/
           QUANTIDADE_DEVOLUCAO_OLD, /*26*/
           QUANTIDADE_DEVOLUCAO_NEW, /*27*/
           VALOR_UNITARIO_DEVOL_OLD, /*28*/
           VALOR_UNITARIO_DEVOL_NEW, /*29*/
           VALOR_DEVOLUCAO_OLD, /*30*/
           VALOR_DEVOLUCAO_NEW, /*31*/
           SIT_DEVOLUCAO_OLD, /*32*/
           SIT_DEVOLUCAO_NEW /*33*/
        ) values (
            'D', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede,/*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.NR_SOLICITACAO, /*8*/
           0, /*9*/
           :old.PEDIDO_VENDA, /*10*/
           0, /*11*/
           :old.SEQ_PEDIDO, /*12*/
           0, /*13*/
           :old.NOTA_FISCAL_ENTRADA, /*14*/
           0, /*15*/
           :old.SERIE_NF_ENTRADA, /*16*/
           '', /*17*/
           :old.FORNECEDOR_9, /*18*/
           0, /*19*/
           :old.FORNECEDOR_4, /*20*/
           0, /*21*/
           :old.FORNECEDOR_2, /*22*/
           0, /*23*/
           :old.SEQ_NOTA_ENTRADA, /*24*/
           0, /*25*/
           :old.QUANTIDADE_DEVOLUCAO, /*26*/
           0, /*27*/
           :old.VALOR_UNITARIO_DEVOL, /*28*/
           0, /*29*/
           :old.VALOR_DEVOLUCAO, /*30*/
           0, /*31*/
           :old.SIT_DEVOLUCAO, /*32*/
           0 /*33*/
         );
    end;
 end if;
end inter_tr_FATU_036_log;

-- ALTER TRIGGER "INTER_TR_FATU_036_LOG" ENABLE
 

/

exec inter_pr_recompile;

