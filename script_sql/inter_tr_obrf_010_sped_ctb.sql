
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_010_SPED_CTB" 
   before insert or
   update of despesas_importacao, flag_nota_fechada, qtd_pcs_faltantes, nota_estorno,
	nota_dev, serie_dev, relac_nf_orig, moeda_nota,
	num_form_final, tipo_doc_importacao_sped, nr_recibo, ano_inutilizacao,
	cod_justificativa, valor_iss, nr_duplicata, situacao_entrada,
	num_conhecimento, val_conhecimento, tipo_frete, transpa_forne9,
	transpa_forne4, transpa_forne2, numero_volume, peso_liquido,
	peso_bruto, observacao1, observacao2, fornec_cliente,
	classif_contabil, qtde_itens, local_entrega, rol_embalagens,
	base_diferenca, terceiros, valor_desconto, valor_funrural,
	total_docto, valor_icms_sub, base_icms_sub, marca_volumes,
	especie_volumes, qtde_volumes, historico_cont, flag_devolucao,
	sit_divergencia, ser_conhecimento, tipo_conhecimento, num_contabil,
	despesas_extras, nota_rateada_estq, valor_desp_post, divisao_qualif,
	responsavel9, responsavel4, responsavel2, selo_inicial,
	selo_final, data_de_digitacao, data_digitacao, numero_di,
	tarifa_frete, sit_dup_uni, complemento_dup, msg_corpo1,
	msg_corpo2, valor_iva_1, valor_iva_2, data_nsu,
	hora_nsu, nsu, numero_formulario, num_form_inicial,
	origem, valor_indice_moeda, codigo_cai, dt_valida_cai,
	executa_trigger, tipo_valor_nf, numero_danf_nfe, tipo_valores_fiscal,
	num_processo_imp, valor_pis_import_sped, valor_cofins_import_sped, data_desembaraco_sped,
	valor_cif_sped, valor_desp_sem_icms_sped, valor_desp_com_icms_sped, valor_imposto_opera_finan_sped,
	valor_imposto_import_sped, data_registro_sisco_sped, nr_protocolo, justificativa,
	nr_final_inut, cod_status, msg_status, cod_solicitacao_nfe,
	status_impressao_danfe, nfe_contigencia, serie_original, cod_canc_nfisc,
	tipo_nf_referenciada, nota_referenciada, serie_referenciada, cnpj9_ref,
	cnpj4_ref, cnpj2_ref, doc_conta_corrente, xml_nprot,
	versao_systextilnfe, local_impressao, chave_contingencia, data_autorizacao_nfe,
	exec_processo_imp, tipo_desconto, st_flag_cce, documento,
	serie, cgc_cli_for_9, cgc_cli_for_4, cgc_cli_for_2,
	especie_docto, natoper_nat_oper, natoper_est_oper, via_transporte,
	codigo_transacao, data_transacao, data_emissao, base_icms,
	valor_itens, valor_icms, valor_total_ipi, valor_despesas,
	valor_frete, valor_seguro, icms_frete, condicao_pagto
   on obrf_010
   for each row

declare
   v_cliente_fornec  cont_600.cliente_fornecedor_part%type;
   v_cnpj_9          cont_600.cnpj9_participante%type;
   v_cnpj_4          cont_600.cnpj4_participante%type;
   v_cnpj_2          cont_600.cnpj2_participante%type;
   v_trans           estq_005.codigo_transacao%type;
   v_tipo_transacao  estq_005.tipo_transacao%type;
   v_sid             cont_601.sid%type;
   v_instancia       cont_601.instancia%type;

begin

   if inserting
   then
      v_cnpj_9 := :new.cgc_cli_for_9;
      v_cnpj_4 := :new.cgc_cli_for_4;
      v_cnpj_2 := :new.cgc_cli_for_2;
      v_trans  := :new.codigo_transacao;
   else
      v_cnpj_9 := :old.cgc_cli_for_9;
      v_cnpj_4 := :old.cgc_cli_for_4;
      v_cnpj_2 := :old.cgc_cli_for_2;
      v_trans  := :old.codigo_transacao;
   end if;

   begin
      select tipo_transacao
      into   v_tipo_transacao
      from   estq_005
      where  codigo_transacao = v_trans;

   exception
      when others then
         v_tipo_transacao := 'N';
   end;

   if v_tipo_transacao <> 'D'
   then
      v_cliente_fornec := 2; -- fornecedor
   else
      v_cliente_fornec := 1; -- cliente
   end if;


   select sid,   inst_id
   into   v_sid, v_instancia
   from   v_lista_sessao_banco;

   insert into cont_601
      (sid,
       instancia,
       data_insercao,
       tabela_origem,
       cnpj_9,
       cnpj_4,
       cnpj_2,
       cliente_fornec)
   values
      (v_sid,
       v_instancia,
       sysdate,
       'OBRF_010',
       v_cnpj_9,
       v_cnpj_4,
       v_cnpj_2,
       v_cliente_fornec);

   exception
      when others then
         null;
end inter_tr_obrf_010_sped_ctb;
-- ALTER TRIGGER "INTER_TR_OBRF_010_SPED_CTB" ENABLE
 

/

exec inter_pr_recompile;

