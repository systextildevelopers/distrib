create or replace trigger INTER_TR_GERA_NOTA_EXP
after update of situacao_nfisc
on fatu_050
for each row
declare

v_cod_re fatu_490.cod_re%type;
v_data_re fatu_490.data_re%type;
v_cod_bl fatu_490.cod_bl%type;
v_data_bl fatu_490.data_bl%type;
v_data_emissao fatu_490.data_emissao%type;
v_moeda fatu_490.moeda%type;
v_cod_sd fatu_490.cod_sd%type;
v_encontrou boolean;

begin

   if :old.situacao_nfisc <> 3 and :new.situacao_nfisc = 3 and :new.natop_nf_est_oper = 'EX'
   then   

     for itemNf in (
        select distinct classific_fiscal, pedido_venda
        from fatu_060 
        where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
          and fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
          and fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc
          and fatu_060.pedido_venda > 0
       )
     loop
       
        v_encontrou := true;
        
        /*Le processo de exportacao do pedido*/
        begin
          select substr(trim(fatu_490.cod_re),1,14),  fatu_490.data_re,
                 fatu_490.cod_bl,  fatu_490.data_bl, 
                 fatu_490.data_emissao, fatu_490.moeda, 
                 fatu_490.cod_sd
          into v_cod_re, v_data_re, v_cod_bl, v_data_bl, 
               v_data_emissao, v_moeda, v_cod_sd
          from fatu_490
          where fatu_490.pedido_venda = itemNf.pedido_venda;
          exception
            when no_data_found then
               v_encontrou := false;
        end;
        
        if v_encontrou
        then
			begin
			   insert into fatu_067
			   (codigo_empresa,         num_nota_fiscal,
				serie_nota_fisc,        seq_nota_fisc, 
				ncm,                    tipo_exp_sped,
				nr_declaracao_exp_sped, data_exp_sped,
				tipo_nat_exp_sped,      data_registro_exp_sped, 
				conhecimento_emb_sped,  data_conhecimento_emb_sped,
				tipo_inform_conhecimento_sped, codigo_pais_siscomex_sped, 
				data_averbacao_sped,           cod_moeda_exp
			   )
			   values
			   (:new.codigo_empresa,        :new.num_nota_fiscal,
				:new.serie_nota_fisc,       0, 
				itemNf.classific_fiscal,    0,
				v_cod_re,                   v_data_re,
				0,                          v_data_re,
				v_cod_bl,                   v_data_bl,
				13,                         :new.codigo_pais_siscomex_sped,
				v_data_emissao,             v_moeda);
			exception
				WHEN dup_val_on_index  then				
					null;
				end;
        end if;
        
     end loop;
   end if;
end INTER_TR_GERA_NOTA_EXP;
-- ALTER TRIGGER "INTER_TR_FATU_050_GERA_NOTA_EXP" ENABLE

/
