INSERT INTO hdoc_035
          ( codigo_programa
          , programa_menu
          , item_menu_def
          , descricao)
   VALUES ( 'pcpt_f224'
          , 0
          , 0
          , 'Ocorrências de Tecelagem');
 
INSERT INTO hdoc_033
          ( usu_prg_cdusu
          , usu_prg_empr_usu
          , programa
          , nome_menu
          , item_menu
          , ordem_menu
          , incluir
          , modificar
          , excluir
          , procurar)
    VALUES( 'INTERSYS'
          , 1
          , 'pcpt_f224'
          , 'nenhum'
          , 0
          , 0
          , 'S'
          , 'S'
          , 'S'
          , 'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Incidentes de Tejeduría'
 WHERE hdoc_036.codigo_programa = 'pcpt_f224'
   AND hdoc_036.locale          = 'es_ES';
  
commit work;
