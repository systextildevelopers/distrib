alter table basi_020 
modify (tam_ponto_1 number(5,4));

alter table basi_020 
modify (tam_ponto_2 number(5,4));

alter table basi_020 
modify (tam_ponto_3 number(5,4));

alter table mqop_060 
modify (sal_banho number(4));

alter table mqop_060 
modify (teor_absorcao number(10,4));


/
exec inter_pr_recompile;
