create or replace package inter_pc_crec_variaveis
is

   -- Author  : Claudio Renato Antonius
   -- Created : 26/01/04 11:12:32
   -- Purpose : Package do modulo de contas a receber
  
   -- Public variable declarations
   codigo_empresa    fatu_070.codigo_empresa%type;
   cli_dup_cgc_cli9  fatu_070.cli_dup_cgc_cli9%type;
   cli_dup_cgc_cli4  fatu_070.cli_dup_cgc_cli4%type;
   cli_dup_cgc_cli2  fatu_070.cli_dup_cgc_cli2%type;
   tipo_titulo       fatu_070.tipo_titulo%type;
   num_duplicata     fatu_070.num_duplicata%type;
   seq_duplicatas    fatu_070.seq_duplicatas%type;

end inter_pc_crec_variaveis;
/
/* versao: 1 */
