create or replace procedure inter_pr_insere_obrf_665 (
  p_codigo_empresa  in number,   p_ano                in number,
  p_mes             in number,   p_tipo_registro      in varchar2,
  p_nivel           in varchar2, p_grupo              in varchar2,
  p_subgrupo        in varchar2, p_item               in varchar2,
  p_numero_nota     in number,   p_serie_nota         in varchar2,
  p_fornecedor9     in number,   p_fornecedor4        in number,
  p_fornecedor2     in number,   p_quantidade         in number,
  p_valor_contabil  in number,   p_valor_base_calculo in number,
  p_procedencia     in number,   p_perc_importado     in number,
  p_valor_importado in number,   p_alternativa        in number,
  p_tipo_nota       in varchar2) is
begin
  insert into obrf_665
    (codigo_empresa,  ano,
     mes,             tipo_registro,

     nivel,           grupo,
     subgrupo,        item,

     numero_nota,     serie_nota,
     fornecedor9,     fornecedor4,
     fornecedor2,     quantidade,

     valor_contabil,  valor_base_calculo,
     procedencia,     perc_importado,
     valor_importado, alternativa,
     tipo_nota)
  values
    (p_codigo_empresa,  p_ano,
     p_mes,             p_tipo_registro,

     p_nivel,           p_grupo,
     p_subgrupo,        p_item,

     p_numero_nota,     p_serie_nota,
     p_fornecedor9,     p_fornecedor4,
     p_fornecedor2,     p_quantidade,

     p_valor_contabil,  p_valor_base_calculo,
     p_procedencia,     p_perc_importado,
     p_valor_importado, p_alternativa,
     p_tipo_nota);

   exception when others
   then raise_application_error(-20000, 'Problema ao inserir dados na tabela obrf_665. Erro banco de dados:  ' || SQLERRM);

end inter_pr_insere_obrf_665;
