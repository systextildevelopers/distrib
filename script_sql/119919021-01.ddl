alter table supr_100 add qtde_a_entregar number(15,3);
alter table supr_100 modify qtde_a_entregar default 0.00;

alter table supr_100 add flag_marcar number(1);
alter table supr_100 modify flag_marcar default 0;

alter table supr_100 add chave_nf varchar2(44);
alter table supr_100 modify chave_nf default ' ';

alter table supr_100 add sequencia_item_nf number(9);
alter table supr_100 modify sequencia_item_nf default 0;

exec inter_pr_recompile;
