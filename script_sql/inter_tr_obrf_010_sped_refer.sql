CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_010_SPED_REFER"
BEFORE  DELETE
OR UPDATE  of situacao_entrada, valor_pis_import_sped, valor_cofins_import_sped,valor_imposto_import_sped
on obrf_010
for each row

declare

   ws_encontrou_nf              varchar2(1);
   ws_cod_msg_referenciado      obrf_874.cod_mensagem%type;
   ws_descr_mensagem1           obrf_874.des_mensagem1%type;
   ws_descr_mensagem2           obrf_874.des_mensagem2%type;
   ws_descr_mensagem3           obrf_874.des_mensagem3%type;
   ws_descr_mensagem4           obrf_874.des_mensagem4%type;
   ws_descr_mensagem5           obrf_874.des_mensagem5%type;
   ws_nf                        obrf_010.documento%type;
   ws_cod_msg_imp               obrf_874.cod_mensagem%type;
   ws_seq_msg_imp               obrf_180.cod_sequencia_imp%type;
   ws_cod_local_imp             obrf_180.cod_local_imp%type;
   ws_cod_mensagem_ipi_dev      obrf_180.cod_mensagem_ipi_dev%type;
   ws_seq_mensagem_ipi_dev      obrf_180.cod_sequencia_ipi_dev%type;
   ws_cod_local_ipi_dev         obrf_180.cod_local_ipi_dev%type;
   ws_cod_mensagem_devolucao    obrf_180.cod_mensagem_devolucao%type;
   ws_cod_sequencia_devolucao   obrf_180.cod_seq_devolucao%type;
   ws_cod_local_devolucao       obrf_180.cod_local_devolucao%type;
   ws_cod_msg_nf_comp           obrf_180.cod_msg_nf_comp%type;
   ws_seq_msg_nf_comp           obrf_180.seq_msg_nf_comp%type;
   ws_loc_msg_nf_comp           obrf_180.loc_msg_nf_comp%type;
   ws_cod_msg_nf_ajuste         obrf_180.cod_msg_nf_ajuste%type;
   ws_seq_msg_nf_ajuste         obrf_180.seq_msg_nf_ajuste%type;
   ws_loc_msg_nf_ajuste         obrf_180.loc_msg_nf_ajuste%type;
   ws_data_emissao              varchar2(10);
   ws_zerou_ipi                 varchar2(1);
   ws_mensagem_ipi              varchar2(300);
   ws_mensagem_devolucao        varchar2(4000);

   w_cod_natureza                number(6);
   w_mensagem_natureza           varchar2(2000);
   w_mensagem_natureza_aux       varchar2(2000);
   v_ind_local                   varchar2(1);
   v_des_mensagem1               obrf_874.des_mensagem1%type;
   v_des_mensagem2               obrf_874.des_mensagem2%type;
   v_des_mensagem3               obrf_874.des_mensagem3%type;
   v_des_mensagem4               obrf_874.des_mensagem4%type;
   v_des_mensagem5               obrf_874.des_mensagem5%type;
   v_des_mensagem6               obrf_874.des_mensagem6%type;
   v_des_mensagem7               obrf_874.des_mensagem7%type;
   v_des_mensagem8               obrf_874.des_mensagem8%type;
   v_des_mensagem9               obrf_874.des_mensagem9%type;
   v_des_mensagem10              obrf_874.des_mensagem10%type;
   w_encontrou_msg               varchar2(1);
   w_conta_msg_t                 number(3);
   w_conta_msg_p                 number(3);
   w_conta_msg                   number(3);
   ws_qtde_fatur_cupom           obrf_015.quantidade%type;
   ws_mensagem_devolucao_p       varchar2(4000);
   ws_mensagem_devolucao_t       varchar2(4000);
   ws_mensagem_devolucao_nf       varchar2(4000);

   w_cod_msg_cest                obrf_180.cod_mensagem_cest%type;
   w_cod_seq_cest                obrf_180.cod_sequencia_cest%type;
   w_cod_local_cest              obrf_180.cod_local_cest%type;
   w_mensagem_cest               varchar2(1000);
   w_msg_nr_cest                 varchar2(1000);
   w_msg_conta_cest              number;

   w_cod_msg_consumo_final       obrf_180.cod_msg_consumo_final%type;
   w_seq_msg_consumo_final       obrf_180.seq_msg_consumo_final%type;
   w_loc_msg_consumo_final       obrf_180.loc_msg_consumo_final%type;
   w_conta_consumo_final         number;
   w_ind_local           obrf_874.ind_local%type;

   w_nome_fornecedor VARCHAR2(100);
   w_inscricao_forne VARCHAR2(30);
   w_cod_mensagem_nf_devol number;
   w_cod_local_nf_devol varchar2(3);
   w_len_msg number(4);
   w_teve_devol boolean;
   w_notas_devolvidas varchar2(4000);
   w_cod_sequencia_nf_devol   obrf_180.cod_sequencia_nf_devol%type;
   ws_data_emissao_devol      date;
   w_retorno                     varchar2(4000);
   w_sequencia                   number;

begin
   ws_encontrou_nf         := 's';
   ws_cod_msg_referenciado := 0;

   begin
      select fatu_503.cod_msg_refenciado into ws_cod_msg_referenciado
      from fatu_503
      where fatu_503.codigo_empresa = :old.local_entrega;
   exception
      when no_data_found then
        ws_cod_msg_referenciado := 0;
   end;

   if ws_cod_msg_referenciado > 0 and :new.situacao_entrada in (4,5)
   then
      begin
         select 1 into ws_nf
         from obrf_015
         where obrf_015.capa_ent_nrdoc   = :old.documento
           and obrf_015.capa_ent_serie   = :old.serie
           and obrf_015.capa_ent_forcli9 = :old.cgc_cli_for_9
           and obrf_015.capa_ent_forcli4 = :old.cgc_cli_for_4
           and obrf_015.capa_ent_forcli2 = :old.cgc_cli_for_2
           and obrf_015.num_nota_orig    > 0
           and rownum                    = 1;
      exception
         when no_data_found then
           ws_encontrou_nf := 'n';
      end;

      if ws_encontrou_nf = 's' and updating
      then

         begin
            select trim(obrf_874.des_mensagem1),       trim(obrf_874.des_mensagem2),
                   trim(obrf_874.des_mensagem3),       trim(obrf_874.des_mensagem4),
                   trim(obrf_874.des_mensagem5),      obrf_874.ind_local
                 into ws_descr_mensagem1,              ws_descr_mensagem2,
                     ws_descr_mensagem3,               ws_descr_mensagem4,
                     ws_descr_mensagem5,         w_ind_local
            from obrf_874
            where obrf_874.cod_mensagem = ws_cod_msg_referenciado;
         exception
            when no_data_found then
                ws_descr_mensagem1 := '';
                ws_descr_mensagem2 := '';
                ws_descr_mensagem3 := '';
                ws_descr_mensagem4 := '';
                ws_descr_mensagem5 := '';
         end;

         begin
            insert into fatu_052
              (cod_empresa,
               num_nota,
               cod_serie_nota,
               cod_mensagem,
               cnpj9,
               cnpj4,
               cnpj2,
               ind_local,
               ind_entr_saida,
               des_mensag_1,
               des_mensag_2,
               des_mensag_3,
               des_mensag_4,
               des_mensag_5)
            values
              (:old.local_entrega,
               :old.documento,
               :old.serie,
               ws_cod_msg_referenciado,
               :old.cgc_cli_for_9,
               :old.cgc_cli_for_4,
               :old.cgc_cli_for_2,
               w_ind_local,
               'E',
               ws_descr_mensagem1,
               ws_descr_mensagem2,
               ws_descr_mensagem3,
               ws_descr_mensagem4,
               ws_descr_mensagem5);
         EXCEPTION
            WHEN dup_val_on_index THEN
               NULL;

            WHEN OTHERS THEN
               raise_application_error(-20000,'Erro ao inserir tabela FATU_052');
         END;

   end if;

      if deleting
      then
         delete from fatu_052
         where fatu_052.cod_empresa    = :old.local_entrega
           and fatu_052.num_nota       = :old.documento
           and fatu_052.cod_serie_nota = :old.serie
           and fatu_052.cod_mensagem   = ws_cod_msg_referenciado
           and fatu_052.cnpj9          = :old.cgc_cli_for_9
           and fatu_052.cnpj4          = :old.cgc_cli_for_4
           and fatu_052.cnpj2          = :old.cgc_cli_for_2
           and fatu_052.ind_entr_saida = 'E';
      end if;
   end if;

   if ((:new.valor_pis_import_sped > 0.00 or :new.valor_cofins_import_sped > 0.00 or :new.valor_imposto_import_sped > 0.00) and :new.situacao_entrada not in (3,4,5) and updating)
   then
      begin
         select obrf_180.cod_mensagem_imp, obrf_180.cod_sequencia_imp, obrf_180.cod_local_imp
         into   ws_cod_msg_imp,            ws_seq_msg_imp,             ws_cod_local_imp
         from obrf_180
         where obrf_180.cod_empresa = :old.local_entrega;
      exception
         when no_data_found then
            ws_cod_msg_imp := 0;
            ws_seq_msg_imp := 0;
            ws_cod_local_imp := '';
      end;
         if ws_cod_msg_imp > 0
         then
             begin
                delete from fatu_052
                where fatu_052.cod_empresa    = :old.local_entrega
                  and fatu_052.num_nota       = :old.documento
                  and fatu_052.cod_serie_nota = :old.serie
                  and fatu_052.cod_mensagem   = ws_cod_msg_imp
                  and fatu_052.seq_mensagem   = ws_seq_msg_imp
                  and fatu_052.cnpj9          = :old.cgc_cli_for_9
                  and fatu_052.cnpj4          = :old.cgc_cli_for_4
                  and fatu_052.cnpj2          = :old.cgc_cli_for_2
                  and fatu_052.ind_entr_saida = 'E';
             end;
             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.local_entrega,
                        :new.documento,
                        :new.serie,
                        o.cod_mensagem,
                        :new.cgc_cli_for_9,
                        :new.cgc_cli_for_4,
                        :new.cgc_cli_for_2,
                        'E',
                        ws_seq_msg_imp,
                        ws_cod_local_imp,
                        substr(o.des_mensagem1 || '  ' || 'IMPOSTOS DE IMPORTACAO: R$' || ' ' || trim(to_char(:new.valor_imposto_import_sped,'999990.00')) ||
                                  ' - VALOR DO PIS: R$'        || ' ' || trim(to_char(:new.valor_pis_import_sped,'9999990.00'))   ||
                                  ' - VALOR DO COFINS: R$'     || ' ' || trim(to_char(:new.valor_cofins_import_sped,'9999990.00')),1,49),
                        substr(o.des_mensagem1 || '  ' || 'IMPOSTOS DE IMPORTACAO: R$' || ' ' || trim(to_char(:new.valor_imposto_import_sped,'999990.00')) ||
                                  ' - VALOR DO PIS: R$'        || ' ' || trim(to_char(:new.valor_pis_import_sped,'9999990.00'))   ||
                                  ' - VALOR DO COFINS: R$'     || ' ' || trim(to_char(:new.valor_cofins_import_sped,'9999990.00')),50,50),
                        substr(o.des_mensagem1 || '  ' || 'IMPOSTOS DE IMPORTACAO: R$' || ' ' || trim(to_char(:new.valor_imposto_import_sped,'999990.00')) ||
                                  ' - VALOR DO PIS: R$'        || ' ' || trim(to_char(:new.valor_pis_import_sped,'9999990.00'))   ||
                                  ' - VALOR DO COFINS: R$'     || ' ' || trim(to_char(:new.valor_cofins_import_sped,'9999990.00')),100,50),
                        substr(o.des_mensagem1 || '  ' || 'IMPOSTOS DE IMPORTACAO: R$' || ' ' || trim(to_char(:new.valor_imposto_import_sped,'999990.00')) ||
                                  ' - VALOR DO PIS: R$'        || ' ' || trim(to_char(:new.valor_pis_import_sped,'9999990.00'))   ||
                                  ' - VALOR DO COFINS: R$'     || ' ' || trim(to_char(:new.valor_cofins_import_sped,'9999990.00')),150,50),
                        substr(o.des_mensagem1 || '  ' || 'IMPOSTOS DE IMPORTACAO: R$' || ' ' || trim(to_char(:new.valor_imposto_import_sped,'999990.00')) ||
                                  ' - VALOR DO PIS: R$'        || ' ' || trim(to_char(:new.valor_pis_import_sped,'9999990.00'))   ||
                                  ' - VALOR DO COFINS: R$'     || ' ' || trim(to_char(:new.valor_cofins_import_sped,'9999990.00')),200,50)
                 from obrf_874 o
                 where o.cod_mensagem = ws_cod_msg_imp
                   and ws_cod_msg_imp > 0);
             exception
                WHEN dup_val_on_index THEN
                NULL;

               when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem de Importacao' || Chr(10) || SQLERRM);
             end;
         end if;
   end if;

   --Mensagens da natureza de opera??o -- Rodrigo.C SS 88857/014
   if :new.situacao_entrada not in (2,3,4,5) and updating
   then

       for mensagem_natureza in (select distinct(pedi_080.cod_mensagem) from obrf_015, pedi_080
                                 where obrf_015.capa_ent_nrdoc    = :new.documento
                                   and obrf_015.capa_ent_serie    = :new.serie
                                   and obrf_015.capa_ent_forcli9  = :new.cgc_cli_for_9
                                   and obrf_015.capa_ent_forcli4  = :new.cgc_cli_for_4
                                   and obrf_015.capa_ent_forcli2  = :new.cgc_cli_for_2
                                   and pedi_080.natur_operacao    = obrf_015.natitem_nat_oper
                                   and pedi_080.estado_natoper    = obrf_015.natitem_est_oper
                                   and pedi_080.cod_mensagem    > 0)
       loop

         begin
           insert into fatu_052
          (cod_empresa,
           num_nota,
           cod_serie_nota,
           cod_mensagem,
           cnpj9,
           cnpj4,
           cnpj2,
           ind_entr_saida,
           ind_local,
           des_mensag_1,
           des_mensag_2,
           des_mensag_3,
           des_mensag_4,
           des_mensag_5,
           des_mensag_6,
           des_mensag_7,
           des_mensag_8,
           des_mensag_9,
           des_mensag_10)
          (select :new.local_entrega,
                  :new.documento,
                  :new.serie,
                  o.cod_mensagem,
                  :new.cgc_cli_for_9,
                  :new.cgc_cli_for_4,
                  :new.cgc_cli_for_2,
                  'E',
                  o.ind_local,
                  o.des_mensagem1,
                  o.des_mensagem2,
                  o.des_mensagem3,
                  o.des_mensagem4,
                  o.des_mensagem5,
                  o.des_mensagem6,
                  o.des_mensagem7,
                  o.des_mensagem8,
                  o.des_mensagem9,
                  o.des_mensagem10
           from obrf_874 o
           where o.cod_mensagem = mensagem_natureza.cod_mensagem);
           exception
              WHEN dup_val_on_index THEN
              NULL;
              when others then
                 raise_application_error(-20000,'Erro ao inserir Mensagem default da nota' || Chr(10) || SQLERRM);
         end;
      end loop;

      for mensagem_natureza_comercial in (select distinct(pedi_080.cod_mensagem_comercial) from obrf_015, pedi_080
                                 where obrf_015.capa_ent_nrdoc    = :new.documento
                                   and obrf_015.capa_ent_serie    = :new.serie
                                   and obrf_015.capa_ent_forcli9  = :new.cgc_cli_for_9
                                   and obrf_015.capa_ent_forcli4  = :new.cgc_cli_for_4
                                   and obrf_015.capa_ent_forcli2  = :new.cgc_cli_for_2
                                   and pedi_080.natur_operacao    = obrf_015.natitem_nat_oper
                                   and pedi_080.estado_natoper    = obrf_015.natitem_est_oper
                                   and pedi_080.cod_mensagem_comercial    > 0
                                   and exists(select 1  from obrf_874
                                                    where cod_mensagem = pedi_080.cod_mensagem_comercial )
                                    order by pedi_080.cod_mensagem_comercial
                                         )
       loop

         begin
           insert into fatu_052
          (cod_empresa,
           num_nota,
           cod_serie_nota,
           cod_mensagem,
           cnpj9,
           cnpj4,
           cnpj2,
           ind_entr_saida,
           ind_local,
           des_mensag_1,
           des_mensag_2,
           des_mensag_3,
           des_mensag_4,
           des_mensag_5,
           des_mensag_6,
           des_mensag_7,
           des_mensag_8,
           des_mensag_9,
           des_mensag_10)
          (select :new.local_entrega,
                  :new.documento,
                  :new.serie,
                  o.cod_mensagem,
                  :new.cgc_cli_for_9,
                  :new.cgc_cli_for_4,
                  :new.cgc_cli_for_2,
                  'E',
                  o.ind_local,
                  o.des_mensagem1,
                  o.des_mensagem2,
                  o.des_mensagem3,
                  o.des_mensagem4,
                  o.des_mensagem5,
                  o.des_mensagem6,
                  o.des_mensagem7,
                  o.des_mensagem8,
                  o.des_mensagem9,
                  o.des_mensagem10
           from obrf_874 o
           where o.cod_mensagem = mensagem_natureza_comercial.cod_mensagem_comercial);
           exception
              WHEN dup_val_on_index THEN
              NULL;
              when others then
                 raise_application_error(-20000,'Erro ao inserir Mensagem default da nota' || Chr(10) || SQLERRM);
         end;
      end loop;
   end if;

   begin
      select obrf_180.cod_mensagem_ipi_dev, obrf_180.cod_sequencia_ipi_dev, obrf_180.cod_local_ipi_dev
      into   ws_cod_mensagem_ipi_dev,       ws_seq_mensagem_ipi_dev,        ws_cod_local_ipi_dev
      from obrf_180
      where obrf_180.cod_empresa = :old.local_entrega;
   exception
      when no_data_found then
         ws_cod_mensagem_ipi_dev := 0;
         ws_seq_mensagem_ipi_dev := 0;
         ws_cod_local_ipi_dev := ' ';
   end;

   if ws_cod_mensagem_ipi_dev > 0 and :new.valor_total_ipi > 0.00
   then

      ws_zerou_ipi := 'N';
      ws_mensagem_ipi := ' ';

      begin
         select 'S' into ws_zerou_ipi
         from estq_005
         where estq_005.codigo_transacao = :new.codigo_transacao
           and estq_005.tipo_transacao = 'D';
      exception
          when no_data_found then
             ws_zerou_ipi := 'N';
      end;

      if ws_zerou_ipi = 'S'
      then

         begin
            select obrf_874.des_mensagem1 || obrf_874.des_mensagem2 || obrf_874.des_mensagem3 ||
                   obrf_874.des_mensagem4 || obrf_874.des_mensagem5
            into ws_mensagem_ipi
            from obrf_874
            where obrf_874.cod_mensagem = ws_cod_mensagem_ipi_dev;
         exception
             when no_data_found then
                ws_mensagem_ipi := ' ';
         end;

         ws_mensagem_ipi := trim(ws_mensagem_ipi || ' ' || trim(to_char(:new.valor_total_ipi, 999999999999990.99)));

         begin
          insert into fatu_052
          (cod_empresa,
           num_nota,
           cod_serie_nota,
           cod_mensagem,
           cnpj9,
           cnpj4,
           cnpj2,
           ind_entr_saida,
           seq_mensagem,
           ind_local,
           des_mensag_1,
           des_mensag_2,
           des_mensag_3,
           des_mensag_4,
           des_mensag_5)
          (select :new.local_entrega,
                  :new.documento,
                  :new.serie,
                  o.cod_mensagem,
                  :new.cgc_cli_for_9,
                  :new.cgc_cli_for_4,
                  :new.cgc_cli_for_2,
                  'E',
                  ws_seq_mensagem_ipi_dev,
                  ws_cod_local_ipi_dev,
                  substr(ws_mensagem_ipi, 1, 55),
                  substr(ws_mensagem_ipi, 56, 55),
                  substr(ws_mensagem_ipi, 111, 55),
                  substr(ws_mensagem_ipi, 166, 55),
                  substr(ws_mensagem_ipi, 221, 55)
           from obrf_874 o
           where o.cod_mensagem = ws_cod_mensagem_ipi_dev);
       exception
          WHEN dup_val_on_index THEN
             NULL;
          when others then
            raise_application_error(-20000,'Erro ao inserir Mensagem de IPI' || Chr(10) || SQLERRM);
       end;

      end if;

      if deleting
      then
         delete from fatu_052
         where fatu_052.cod_empresa    = :old.local_entrega
           and fatu_052.num_nota       = :old.documento
           and fatu_052.cod_serie_nota = :old.serie
           and fatu_052.cod_mensagem   = ws_cod_mensagem_ipi_dev
           and fatu_052.cnpj9          = :old.cgc_cli_for_9
           and fatu_052.cnpj4          = :old.cgc_cli_for_4
           and fatu_052.cnpj2          = :old.cgc_cli_for_2
           and fatu_052.ind_entr_saida = 'E';
      end if;

   end if;

   begin
      select obrf_180.cod_mensagem_devolucao, obrf_180.cod_seq_devolucao,      obrf_180.cod_local_devolucao,
             obrf_180.cod_msg_nf_comp,        obrf_180.seq_msg_nf_comp,        obrf_180.loc_msg_nf_comp,
             obrf_180.cod_msg_nf_ajuste,      obrf_180.seq_msg_nf_ajuste,      obrf_180.loc_msg_nf_ajuste,
             obrf_180.cod_mensagem_cest,      obrf_180.cod_sequencia_cest,     obrf_180.cod_local_cest,
             obrf_180.cod_msg_consumo_final,  obrf_180.seq_msg_consumo_final,  obrf_180.loc_msg_consumo_final,
             cod_mensagem_nf_devol,           cod_sequencia_nf_devol,          obrf_180.cod_local_nf_devol
      into   ws_cod_mensagem_devolucao,       ws_cod_sequencia_devolucao,      ws_cod_local_devolucao,
             ws_cod_msg_nf_comp,              ws_seq_msg_nf_comp,              ws_loc_msg_nf_comp,
             ws_cod_msg_nf_ajuste,            ws_seq_msg_nf_ajuste,            ws_loc_msg_nf_ajuste,
             w_cod_msg_cest,                  w_cod_seq_cest,                  w_cod_local_cest,
             w_cod_msg_consumo_final,         w_seq_msg_consumo_final,         w_loc_msg_consumo_final,
             w_cod_mensagem_nf_devol,         w_cod_sequencia_nf_devol,        w_cod_local_nf_devol
      from obrf_180
      where obrf_180.cod_empresa = :old.local_entrega;
   exception
       when no_data_found then
          ws_cod_mensagem_devolucao  := 0;
          ws_cod_sequencia_devolucao := 0;
          ws_cod_local_devolucao     := 'D';
          ws_cod_msg_nf_comp         := 0;
          ws_seq_msg_nf_comp         := 0;
          ws_loc_msg_nf_comp         := 'D';
          ws_cod_msg_nf_ajuste       := 0;
          ws_seq_msg_nf_ajuste       := 0;
          ws_loc_msg_nf_ajuste       := 'D';
          w_cod_msg_cest             := 0;
          w_cod_seq_cest             := 0;
          w_cod_local_cest           := 0;
          w_cod_mensagem_nf_devol    := 0;
          w_cod_sequencia_nf_devol   := 0;
          w_cod_local_nf_devol       := 0;
   end;

   if w_cod_mensagem_nf_devol > 0 and updating
   then
      ws_data_emissao    := null;
      w_teve_devol       := false;
      w_len_msg          := 300;
      w_notas_devolvidas := '';

      if w_cod_local_nf_devol = 'C'
      then
         w_len_msg := 2000;
      end if;

      BEGIN
         select a.nome_fornecedor , a.inscr_est_forne
          into w_nome_fornecedor,  w_inscricao_forne
         from supr_010 a
         where a.fornecedor9 = :new.cgc_cli_for_9
           and a.fornecedor4 = :new.cgc_cli_for_4
           and a.fornecedor2 = :new.cgc_cli_for_2;
          exception
            when no_data_found then
              w_nome_fornecedor := '';
              w_inscricao_forne := '';
      END;

      for msg_devol in (
         select o.num_nota_orig nf_devol, o.serie_nota_orig serie_devol,
                sum(valor_total) valor_total
         from obrf_015 o, estq_005
         where o.codigo_transacao      = estq_005.codigo_transacao
           and estq_005.tipo_transacao in ('I', 'R')
           and o.capa_ent_nrdoc        = :new.documento
           and o.capa_ent_serie        = :new.serie
           and o.capa_ent_forcli9      = :new.cgc_cli_for_9
           and o.capa_ent_forcli4      = :new.cgc_cli_for_4
           and o.capa_ent_forcli2      = :new.cgc_cli_for_2
           and o.num_nota_orig         > 0
        group by o.num_nota_orig, o.serie_nota_orig
      )
      LOOP

         BEGIN
            select c.data_emissao
            into   ws_data_emissao_devol
            from fatu_060 f, fatu_050 c
            where f.ch_it_nf_cd_empr = c.codigo_empresa
              and f.ch_it_nf_num_nfis = c.num_nota_fiscal
              and f.ch_it_nf_ser_nfis = c.serie_nota_fisc
              and f.ch_it_nf_cd_empr  = :new.local_entrega
              and f.ch_it_nf_num_nfis = msg_devol.nf_devol
              and f.ch_it_nf_ser_nfis = msg_devol.serie_devol
            group by c.data_emissao;
         exception
            when no_data_found then
              ws_data_emissao_devol     := null;
         END;

         if (w_teve_devol = false) /*somente pra primeira nota  aqui*/
         then
            w_teve_devol := true;
            w_notas_devolvidas := w_nome_fornecedor || to_char(:new.cgc_cli_for_9,'000000000') || '/' ||
            to_char(:new.cgc_cli_for_4,'0000')|| '-' || to_char(:new.cgc_cli_for_2,'00') || ' REF NF(s) ';
         end if;

         if (trim(w_notas_devolvidas) is null or length(trim(w_notas_devolvidas)) < w_len_msg)
         then

            w_notas_devolvidas := w_notas_devolvidas ||
                                  to_char(msg_devol.nf_devol, '000000000') || '-' ||
                                  msg_devol.serie_devol || ' ' ||
                                  to_char(ws_data_emissao_devol,'DD/MM/YY') || ' ' ||
                                 ltrim(to_char(msg_devol.valor_total,'9999999990.00')) || ' / ';
         end if;
      END LOOP;

      if trim(w_notas_devolvidas) is not null
      then

         if w_cod_local_nf_devol = 'C'
         then
            begin
               insert into fatu_052
               (cod_empresa,
                num_nota,
                cod_serie_nota,
                cod_mensagem,
                cnpj9,
                cnpj4,
                cnpj2,
                ind_entr_saida,
                seq_mensagem,
                ind_local,
                des_mensag_1,
                des_mensag_2,
                des_mensag_3,
                des_mensag_4,
                des_mensag_5,
                des_mensag_6,
                des_mensag_7,
                des_mensag_8,
                des_mensag_9,
                des_mensag_10,
                des_mensag_11,
                des_mensag_12)
               (select :new.local_entrega,
                       :new.documento,
                       :new.serie,
                       o.cod_mensagem,
                       :new.cgc_cli_for_9,
                       :new.cgc_cli_for_4,
                       :new.cgc_cli_for_2,
                       'E',
                       w_cod_sequencia_nf_devol,
                       w_cod_local_nf_devol,
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,1,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,56,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,111,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,166,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,221,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,276,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,331,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,386,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,441,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,496,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,551,725),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,1276,725)
                from obrf_874 o
                where o.cod_mensagem = w_cod_mensagem_nf_devol);
            exception
               WHEN dup_val_on_index THEN
               NULL;

              when others then
               raise_application_error(-20000,'Erro ao inserir Mensagem de notas de devolucao' || Chr(10) || SQLERRM);
            end;
         else

            begin
               insert into fatu_052
               (cod_empresa,
                num_nota,
                cod_serie_nota,
                cod_mensagem,
                cnpj9,
                cnpj4,
                cnpj2,
                ind_entr_saida,
                seq_mensagem,
                ind_local,
                des_mensag_1,
                des_mensag_2,
                des_mensag_3,
                des_mensag_4,
                des_mensag_5)
               (select :new.local_entrega,
                       :new.documento,
                       :new.serie,
                       o.cod_mensagem,
                       :new.cgc_cli_for_9,
                       :new.cgc_cli_for_4,
                       :new.cgc_cli_for_2,
                       'E',
                       w_cod_sequencia_nf_devol,
                       w_cod_local_nf_devol,
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,1,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,56,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,111,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,166,55),
                       substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,221,55)
                from obrf_874 o
                where o.cod_mensagem = w_cod_mensagem_nf_devol);
            exception
               WHEN dup_val_on_index THEN
               NULL;

              when others then
               raise_application_error(-20000,'Erro ao inserir Mensagem de notas de devolucao' || Chr(10) || SQLERRM);
            end;
         end if;

      end if;

   end if; -- fim if w_cod_mensagem_nf_devol > 0

   if ws_cod_mensagem_devolucao > 0
   then
      begin
         select nvl(obrf_874.des_mensagem1 || obrf_874.des_mensagem2 || obrf_874.des_mensagem3 ||
                obrf_874.des_mensagem4 || obrf_874.des_mensagem5,' ')
         into ws_mensagem_devolucao
            from obrf_874
            where obrf_874.cod_mensagem = ws_cod_mensagem_devolucao;
      exception
          when no_data_found then
             ws_mensagem_devolucao := ' ';
      end;

      w_conta_msg   := 0;
      w_conta_msg_t := 0;
      w_conta_msg_p := 0;
      ws_mensagem_devolucao_p := ws_mensagem_devolucao;
      ws_mensagem_devolucao_t := ws_mensagem_devolucao;
      ws_mensagem_devolucao_nf := ws_mensagem_devolucao;

      ws_mensagem_devolucao := '';

      for msg_devol in (
      select o.nr_cupom, o.num_nota_orig, o.serie_nota_orig,o.capa_ent_serie, nvl(num_solicitacao_ecommerce,0) num_solicitacao_ecommerce,
             sum(o.quantidade) qtde_devolvida
      from obrf_015 o, estq_005
      where o.codigo_transacao      = estq_005.codigo_transacao
        and estq_005.tipo_transacao = 'D'
        and o.capa_ent_nrdoc        = :new.documento
        and o.capa_ent_serie        = :new.serie
        and o.capa_ent_forcli9      = :new.cgc_cli_for_9
        and o.capa_ent_forcli4      = :new.cgc_cli_for_4
        and o.capa_ent_forcli2      = :new.cgc_cli_for_2
        and o.num_nota_orig         > 0
      group by o.nr_cupom, o.num_nota_orig, o.serie_nota_orig,o.capa_ent_serie, num_solicitacao_ecommerce
      )
      LOOP
          if length(ws_mensagem_devolucao_p) > 3900 or
             length(ws_mensagem_devolucao_t) > 3900 or
             length(ws_mensagem_devolucao_nf) > 3900
          then
             exit;
          end if;

          BEGIN
          select round(sum(f.qtde_item_fatur),3), c.data_emissao
          into   ws_qtde_fatur_cupom,    ws_data_emissao
          from fatu_060 f, fatu_050 c
          where f.ch_it_nf_cd_empr = c.codigo_empresa
            and f.ch_it_nf_num_nfis = c.num_nota_fiscal
            and f.ch_it_nf_ser_nfis = c.serie_nota_fisc
            and f.ch_it_nf_cd_empr  = :new.local_entrega
            and f.ch_it_nf_num_nfis = msg_devol.num_nota_orig
            and f.ch_it_nf_ser_nfis = msg_devol.serie_nota_orig
            and c.nr_cupom          = msg_devol.nr_cupom
          group by c.data_emissao;
          exception
             when no_data_found then
               ws_qtde_fatur_cupom := 0.000;
               ws_data_emissao     := null;
          END;

         if msg_devol.num_solicitacao_ecommerce > 0 -- DEVOLU??O DE E-COMMERCE
         then
            if msg_devol.qtde_devolvida <> ws_qtde_fatur_cupom -- DEVOLU??O PARCIAL
            then
               if w_conta_msg_p = 0 -- SE FOR A PRIMEIRA
               then
                  ws_mensagem_devolucao_p := trim(ws_mensagem_devolucao_p) || ' Parcial, Numero NF/Serie/ Data de Emissao: ' || msg_devol.num_nota_orig || '/' ||
                  msg_devol.serie_nota_orig || ' de ' || ws_data_emissao || ' - ' || :new.msg_corpo1;
               else
                  ws_mensagem_devolucao_p := trim(ws_mensagem_devolucao_p) || ' - ' || msg_devol.num_nota_orig || '/' || msg_devol.serie_nota_orig || ' DE ' || ws_data_emissao || ' - ' || :new.msg_corpo1;
               end if;
               w_conta_msg_p := w_conta_msg_p + 1;
            else
               -- DEVOU??O TOTAL
               if w_conta_msg_t = 0 -- SE FOR A PRIMEIRA
               then
                  ws_mensagem_devolucao_t := trim(ws_mensagem_devolucao_t) || ' Total, Numero NF/Serie/ Data de Emissao: ' || msg_devol.num_nota_orig || '/' ||
                  msg_devol.serie_nota_orig || ' de ' || ws_data_emissao || ' - ' || :new.msg_corpo1;
               else
                  ws_mensagem_devolucao_t := trim(ws_mensagem_devolucao_t) || ' - ' || msg_devol.num_nota_orig || '/' || msg_devol.serie_nota_orig || ' DE ' || ws_data_emissao || ' - ' || :new.msg_corpo1;
               end if;
               w_conta_msg_t := w_conta_msg_t + 1;
            end if;
         else

            if msg_devol.qtde_devolvida <> ws_qtde_fatur_cupom -- DEVOLU??O PARCIAL DE CUPOM
            then
                if w_conta_msg = 0 and msg_devol.nr_cupom = 0 -- SE FOR A PRIMEIRA
                then
                   ws_mensagem_devolucao_nf := trim(ws_mensagem_devolucao_nf) || ' Parcial, Numero NF/Serie/ Data de Emissao: ' || msg_devol.num_nota_orig || '/' || msg_devol.serie_nota_orig || ' de ' || ws_data_emissao;
                   w_conta_msg := w_conta_msg + 1;
                else
                   if msg_devol.nr_cupom = 0
                   then
                      ws_mensagem_devolucao_nf := trim(ws_mensagem_devolucao_nf) || ' - ' || msg_devol.num_nota_orig || '/' || msg_devol.serie_nota_orig || ' DE ' || ws_data_emissao;
                      w_conta_msg := w_conta_msg + 1;
                   end if;
               end if;
            else
               if w_conta_msg = 0 and msg_devol.nr_cupom = 0 -- SE FOR A PRIMEIRA
               then
                  ws_mensagem_devolucao_nf := trim(ws_mensagem_devolucao_nf) || ' Total, Numero NF/Serie/ Data de Emissao: ' || msg_devol.num_nota_orig || '/' || msg_devol.serie_nota_orig || ' de ' || ws_data_emissao;
                  w_conta_msg := w_conta_msg + 1;
               else
                  if msg_devol.nr_cupom = 0
                  then
                     ws_mensagem_devolucao_nf := trim(ws_mensagem_devolucao_nf) || ' - ' || msg_devol.num_nota_orig || '/' || msg_devol.serie_nota_orig || ' DE ' || ws_data_emissao;
                     w_conta_msg := w_conta_msg + 1;
                  end if;
               end if;
            end if;
         end if;

        if msg_devol.nr_cupom > 0
        then
           if msg_devol.qtde_devolvida <> ws_qtde_fatur_cupom -- DEVOLU??O PARCIAL DE CUPOM
           then
              if w_conta_msg_p = 0 -- SE FOR A PRIMEIRA
              then
                 ws_mensagem_devolucao_p := trim(ws_mensagem_devolucao_p) || ' parcial ref. Cupom: ' || msg_devol.nr_cupom || ' de ' || ws_data_emissao;
              else
                 if msg_devol.nr_cupom > 0
                 then
                    ws_mensagem_devolucao_p := trim(ws_mensagem_devolucao_p) || ' - ' || msg_devol.nr_cupom || ' de ' || ws_data_emissao;
                 end if;
              end if;
              w_conta_msg_p := w_conta_msg_p + 1;
           else
              -- DEVOU??O TOTAL DE CUPOM
              if w_conta_msg_t = 0 -- SE FOR A PRIMEIRA
              then
                 ws_mensagem_devolucao_t := trim(ws_mensagem_devolucao_t) || ' total ref. Cupom: ' || msg_devol.nr_cupom || ' de ' || ws_data_emissao;
              else
                 if msg_devol.nr_cupom > 0
                 then
                    ws_mensagem_devolucao_t := trim(ws_mensagem_devolucao_t) || ' - ' || msg_devol.nr_cupom || ' de ' || ws_data_emissao;
                 end if;
              end if;
              w_conta_msg_t := w_conta_msg_t + 1;
           end if;
        end if;
      END LOOP;

      if w_conta_msg_t + w_conta_msg_p + w_conta_msg > 0
      then
         if w_conta_msg > 0
         then
            ws_mensagem_devolucao := ws_mensagem_devolucao || ' ' || ws_mensagem_devolucao_nf;
         end if;

         if w_conta_msg_t > 0
         then
            ws_mensagem_devolucao := ws_mensagem_devolucao || ' ' || ws_mensagem_devolucao_t;
         end if;

         if w_conta_msg_p > 0
         then
            ws_mensagem_devolucao := ws_mensagem_devolucao || ' ' || ws_mensagem_devolucao_p;
         end if;

         begin
            insert into fatu_052
            (cod_empresa,
             num_nota,
             cod_serie_nota,
             cod_mensagem,
             cnpj9,
             cnpj4,
             cnpj2,
             ind_entr_saida,
             seq_mensagem,
             ind_local,
             des_mensag_1,
             des_mensag_2,
             des_mensag_3,
             des_mensag_4,
             des_mensag_5)
            (select :new.local_entrega,
                    :new.documento,
                    :new.serie,
                    o.cod_mensagem,
                    :new.cgc_cli_for_9,
                    :new.cgc_cli_for_4,
                    :new.cgc_cli_for_2,
                    'E',
                    ws_cod_sequencia_devolucao,
                    ws_cod_local_devolucao,
                    substr(ws_mensagem_devolucao, 1, 55),
                    substr(ws_mensagem_devolucao, 56, 55),
                    substr(ws_mensagem_devolucao, 111, 55),
                    substr(ws_mensagem_devolucao, 166, 55),
                    substr(ws_mensagem_devolucao, 221, 55)
             from obrf_874 o
             where o.cod_mensagem = ws_cod_mensagem_devolucao);
         exception
            WHEN dup_val_on_index THEN
               NULL;
            when others then
              raise_application_error(-20000,'Erro ao inserir Mensagem de Devolucao' || Chr(10) || SQLERRM);
         end;
      end if;

   end if;

   -- Mensagem referente notas de ajuste ou complementar

   if :new.tipo_nf_referenciada = 2 /*Complementar*/
   or :new.tipo_nf_referenciada = 3 /*Ajuste*/
   then

      begin
         insert into fatu_052
         (cod_empresa,
          num_nota,
          cod_serie_nota,
          cod_mensagem,
          cnpj9,
          cnpj4,
          cnpj2,
          ind_entr_saida,
          seq_mensagem,
          ind_local,
          des_mensag_1,
          des_mensag_2,
          des_mensag_3,
          des_mensag_4,
          des_mensag_5)
         (select :new.local_entrega,
                 :new.documento,
                 :new.serie,
                 o.cod_mensagem,
                 :new.cgc_cli_for_9,
                 :new.cgc_cli_for_4,
                 :new.cgc_cli_for_2,
                 'E',
                  decode(:new.tipo_nf_referenciada, 2, ws_seq_msg_nf_comp, ws_seq_msg_nf_ajuste),
                  decode(:new.tipo_nf_referenciada, 2, ws_loc_msg_nf_comp, ws_loc_msg_nf_ajuste),
                  o.des_mensagem1,
                  o.des_mensagem2,
                  o.des_mensagem3,
                  o.des_mensagem4,
                  o.des_mensagem5 || ' N.F.: ' || :new.nota_referenciada || '/' || :new.serie_referenciada || ' ' || to_char(:new.cnpj9_ref,'000000000') || '/' || to_char(:new.cnpj4_ref,'0000') || '-' || to_char(:new.cnpj2_ref,'00')
          from obrf_874 o
          where o.cod_mensagem  = decode(:new.tipo_nf_referenciada, 2, ws_cod_msg_nf_comp, ws_cod_msg_nf_ajuste)
            and decode(:new.tipo_nf_referenciada, 2, ws_cod_msg_nf_comp, ws_cod_msg_nf_ajuste) <> 0);
      exception
         WHEN dup_val_on_index THEN
            NULL;
         when others then
           raise_application_error(-20000,'Erro ao inserir Mensagem de Ajuste/Complementar' || Chr(10) || SQLERRM);
      end;

   end if;
   if  w_cod_msg_cest > 0 and :new.situacao_entrada not in (2,3,4,5) and updating
   then
      w_msg_nr_cest := '';
      for obrf015_cest in (select distinct trim(obrf_015.cest) as cest
                           from obrf_015
      where obrf_015.capa_ent_nrdoc   = :old.documento
        and obrf_015.capa_ent_serie   = :old.serie
        and obrf_015.capa_ent_forcli9 = :old.cgc_cli_for_9
        and obrf_015.capa_ent_forcli4 = :old.cgc_cli_for_4
        and obrf_015.capa_ent_forcli2 = :old.cgc_cli_for_2)
      loop
         w_msg_nr_cest := w_msg_nr_cest || obrf015_cest.cest || ' ';
      end loop;

      select length(w_msg_nr_cest) into  w_msg_conta_cest from dual;

      if w_msg_conta_cest > 3
      then
         begin
           select obrf_874.des_mensagem1 || obrf_874.des_mensagem2 || obrf_874.des_mensagem3 ||
                  obrf_874.des_mensagem4 || obrf_874.des_mensagem5
           into w_mensagem_cest
           from obrf_874
           where obrf_874.cod_mensagem = w_cod_msg_cest;
          exception
             when no_data_found then
             w_mensagem_cest := ' ';
         end;

         w_mensagem_cest := w_mensagem_cest || ' ' ||  w_msg_nr_cest;
         begin
            insert into fatu_052
            (cod_empresa,
             num_nota,
             cod_serie_nota,
             cod_mensagem,
             cnpj9,
             cnpj4,
             cnpj2,
             ind_entr_saida,
             seq_mensagem,
             ind_local,
             des_mensag_1,
             des_mensag_2,
             des_mensag_3,
             des_mensag_4,
             des_mensag_5,
             des_mensag_6,
             des_mensag_7)
            Values
        (:new.local_entrega,
             :new.documento,
             :new.serie,
             w_cod_msg_cest,
             :new.cgc_cli_for_9,
             :new.cgc_cli_for_4,
             :new.cgc_cli_for_2,
             'E',
             w_cod_seq_cest,
             w_cod_local_cest,
             substr(w_mensagem_cest, 1, 55),
             substr(w_mensagem_cest, 56, 55),
             substr(w_mensagem_cest, 111, 55),
             substr(w_mensagem_cest, 166, 55),
             substr(w_mensagem_cest, 221, 55),
             substr(w_mensagem_cest, 276, 55),
             substr(w_mensagem_cest, 331, 55));
         exception
             WHEN dup_val_on_index THEN
             NULL;
             when others then
             raise_application_error(-20000,'Erro ao inserir Mensagem de CEST' || Chr(10) || SQLERRM);
         end;
      end if;
    end if;

-- MENSAGEM REFERENTE A VENDA DE OPERA??O FINAL (CONSUMO FINAL)

--Mensagem referente aos descontos para notas de Devolu??o
    if   :new.situacao_entrada not in (2,3,4,5) and updating
    then
       select count(1)
       into w_conta_consumo_final
       from obrf_015 o, estq_005
       where o.codigo_transacao      = estq_005.codigo_transacao
         and estq_005.tipo_transacao = 'D'
         and o.capa_ent_nrdoc        = :new.documento
         and o.capa_ent_serie        = :new.serie
         and o.capa_ent_forcli9      = :new.cgc_cli_for_9
         and o.capa_ent_forcli4      = :new.cgc_cli_for_4
         and o.capa_ent_forcli2      = :new.cgc_cli_for_2
         and o.num_nota_orig         > 0;

       if (w_cod_msg_consumo_final > 0) and (:new.VAL_ICMS_UF_DEST_NF > 0) and (w_conta_consumo_final > 0)
       then

          begin
             insert into fatu_052
             (cod_empresa,
              num_nota,
              cod_serie_nota,
              cod_mensagem,
              cnpj9,
              cnpj4,
              cnpj2,
              ind_entr_saida,
              seq_mensagem,
              ind_local,
              des_mensag_1,
              des_mensag_2,
              des_mensag_3,
              des_mensag_4,
              des_mensag_5)
             (select :new.local_entrega,
                     :new.documento,
                     :new.serie,
                     o.cod_mensagem,
                     :new.cgc_cli_for_9,
                     :new.cgc_cli_for_4,
                     :new.cgc_cli_for_2,
                     'E',
                     w_seq_msg_consumo_final,
                     w_loc_msg_consumo_final,
                     substr('ICMS CONFORME DECRECTO 93/2015. VALOR UF DESTINATARIO' || ' : ' || TRIM(to_char(:new.VAL_ICMS_UF_DEST_NF,'9999999990.00')) || ' VALOR FCP: ' || TRIM(to_char(:new.VAL_FCP_UF_DEST_NF,'9999999990.00')) || ' VALOR UF REMETENTE' || ' : ' || TRIM(to_char(:new.VAL_ICMS_UF_REMET_NF,'9999999990.00')),1,55),

                     substr('ICMS CONFORME DECRECTO 93/2015. VALOR UF DESTINATARIO' || ' : ' || TRIM(to_char(:new.VAL_ICMS_UF_DEST_NF,'9999999990.00')) || ' VALOR FCP: ' || TRIM(to_char(:new.VAL_FCP_UF_DEST_NF,'9999999990.00')) || ' VALOR UF REMETENTE' || ' : ' || TRIM(to_char(:new.VAL_ICMS_UF_REMET_NF,'9999999990.00')),56,55),

                     substr('ICMS CONFORME DECRECTO 93/2015. VALOR UF DESTINATARIO' || ' : ' || TRIM(to_char(:new.VAL_ICMS_UF_DEST_NF,'9999999990.00')) || ' VALOR FCP: ' || TRIM(to_char(:new.VAL_FCP_UF_DEST_NF,'9999999990.00')) || ' VALOR UF REMETENTE' || ' : ' || TRIM(to_char(:new.VAL_ICMS_UF_REMET_NF,'9999999990.00')),111,55),
                     '',
                     ''
              from obrf_874 o
              where o.cod_mensagem = w_cod_msg_consumo_final
                and w_cod_msg_consumo_final > 0);
          exception
             WHEN dup_val_on_index THEN
             NULL;

            when others then
             raise_application_error(-20000,'Erro ao inserir Mensagem de consumo final' || Chr(10) || SQLERRM);
          end;
       end if;
    end if;

    begin
    select nvl(max(seq_mensagem),0) +1
    into w_sequencia
    from fatu_052
    where cod_empresa = :new.local_entrega
      and num_nota = :new.documento
      and cod_serie_nota = :new.serie
      and cnpj9 = :new.cgc_cli_for_9
      and cnpj4 = :new.cgc_cli_for_4
      and cnpj2 = :new.cgc_cli_for_2;
    exception
    when no_data_found then
      w_sequencia := 1;
    end;

    --Aqui entra a regra nova de mensagem da nota, inserindo uma sequencia nova com a maior sequencia +1
    w_retorno := inter_fn_gera_msg_nfe_adic(:new.local_entrega, :new.documento, :new.serie, :new.cgc_cli_for_9, :new.cgc_cli_for_4, :new.cgc_cli_for_2, w_sequencia, 'E', 0);

end INTER_TR_OBRF_010_SPED_REFER;
/
exec inter_pr_recompile;
