alter table mqop_005 add desc_estagio varchar(20);

comment on column mqop_005.desc_estagio is 'Descricao de estagio para bloco k';

alter table mqop_050 add cod_estagio_agrupador number(2);

comment on column mqop_050.cod_estagio_agrupador is 'Codigo de estagio para bloco k';
