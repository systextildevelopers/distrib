ALTER TABLE CPAG_250
ADD ( juros_mora      number(14,2),
      tipo_desconto   number(1),
      data_desc       date,
      valor_desconto  number(14,2),
      tipo_multa      number(1),
      data_multa      date,
      valor_multa     number(14,2) );

exec inter_pr_recompile;
/
