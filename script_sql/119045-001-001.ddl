insert into pedi_016
   (cnpj9, cnpj4, cnpj2, atributo, conteudo)
 select cgc_9, cgc_4, cgc_2, 'FATURAMENTO.enviaRomaneioDANFE', 'N' from pedi_010
 where  not exists (select 1 from pedi_016 a
                    where a.cnpj9 = pedi_010.cgc_9
                    and a.cnpj4 = pedi_010.cgc_4
                    and a.cnpj2 = pedi_010.cgc_2
                    and a.atributo = 'FATURAMENTO.enviaRomaneioDANFE' );
 
 commit;
