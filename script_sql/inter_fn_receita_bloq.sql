
  CREATE OR REPLACE FUNCTION "INTER_FN_RECEITA_BLOQ" 
    -- Recebe parametros para explosao da estrutura.
   (f_nivel_explode       in  varchar2,
    f_grupo_explode       in  varchar2,
    f_subgrupo_explode    in  varchar2,
    f_item_explode        in  varchar2,
    f_alternativa_explode in  number)
    return varchar2
is
   -- Cria cursor para executar loop na basi_050 buscando todos
   -- os produtos da estrutura.
   cursor basi050 is
   select nivel_comp,       grupo_comp,
          sub_comp,         item_comp,
          sub_item,         item_item,
          alternativa_comp, sequencia
   from basi_050
   where  basi_050.nivel_item       = f_nivel_explode
     and  basi_050.grupo_item       = f_grupo_explode
     and (basi_050.sub_item         = f_subgrupo_explode or basi_050.sub_item  = '000')
     and (basi_050.item_item        = f_item_explode     or basi_050.item_item = '000000')
     and  basi_050.alternativa_item = f_alternativa_explode
     and  basi_050.nivel_comp       = '5'
   order by basi_050.sequencia;

   -- Declara as variaveis que serao utilizadas.
   f_nivel_comp050       basi_050.nivel_comp%type := '#';
   f_grupo_comp050       basi_050.grupo_comp%type := '#####';
   f_subgru_comp050      basi_050.sub_comp%type   := '###';
   f_item_comp050        basi_050.item_comp%type  := '######';
   f_sequencia050        basi_050.sequencia%type;
   f_sub_item050         basi_050.sub_item%type   := '###';
   f_item_item050        basi_050.item_item%type  := '######';
   f_alternativa_comp_050 basi_050.alternativa_comp%type;
   f_rec_bloq             number(1) := 0;
   f_tem_bloq             varchar(1) := '';


begin
   -- Inicio do loop da explosao da estrutura.
   f_tem_bloq := 'n';
   for reg_basi050 in basi050
   loop
      -- Carrega as variaveis com os valores do cursor.
      f_nivel_comp050    := reg_basi050.nivel_comp;
      f_grupo_comp050    := reg_basi050.grupo_comp;
      f_subgru_comp050   := reg_basi050.sub_comp;
      f_item_comp050     := reg_basi050.item_comp;
      f_sequencia050     := reg_basi050.sequencia;
      f_sub_item050      := reg_basi050.sub_item;
      f_item_item050     := reg_basi050.item_item;
      f_alternativa_comp_050  := reg_basi050.alternativa_comp;

      if f_subgru_comp050 = '000'
      then
         begin
            select basi_040.sub_comp into f_subgru_comp050
            from basi_040
            where basi_040.nivel_item       = f_nivel_explode
              and basi_040.grupo_item       = f_grupo_explode
              and basi_040.sub_item         = f_subgrupo_explode
              and basi_040.item_item        = f_item_item050
              and basi_040.alternativa_item = f_alternativa_explode
              and basi_040.sequencia        = f_sequencia050;
            exception
            when others then
               f_subgru_comp050 := '000';
         end;
      end if;

      if f_item_comp050 = '000000'
      then
         begin
            select basi_040.item_comp into f_item_comp050
            from basi_040
            where basi_040.nivel_item       = f_nivel_explode
              and basi_040.grupo_item       = f_grupo_explode
              and basi_040.sub_item         = f_sub_item050
              and basi_040.item_item        = f_item_explode
              and basi_040.alternativa_item = f_alternativa_explode
              and basi_040.sequencia        = f_sequencia050;
            exception
            when others then
                f_item_comp050 := '000000';
         end;
      end if;


      -- Busca se existe receita bloqueada
      begin
         select nvl(count(1),0) into f_rec_bloq
         from basi_400
         where basi_400.tipo_informacao   = 42
           and basi_400.codigo_informacao = 1
           and basi_400.nivel             = '5'
           and basi_400.grupo             = f_grupo_comp050
           and basi_400.subgrupo          = f_subgru_comp050
           and basi_400.item              = f_item_comp050;
      end;
      -- Se encontrar alguma receita bloqueada, para e retorna 's' pro programa
      if f_rec_bloq > 0
      then
         f_tem_bloq := 's';
      end if;
      if f_rec_bloq <= 0
      then
         f_tem_bloq := 'n';
      end if;

      if f_tem_bloq = 'n'
      then

          -- Chama procedure novamente para explosao da estrutura dos componentes
         f_tem_bloq := inter_fn_receita_bloq(f_nivel_comp050,
                                             f_grupo_comp050,
                                             f_subgru_comp050,
                                             f_item_comp050,
                                             f_alternativa_comp_050);
      end if;

      if f_tem_bloq = 's'
      then
         exit;
      end if;
   end loop;

   return (f_tem_bloq);

end INTER_FN_RECEITA_BLOQ;

 

/

exec inter_pr_recompile;

