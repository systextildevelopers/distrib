alter table fndc_007 
add ( usuario_sol VARCHAR2(100),
  liberador        VARCHAR2(100),
  data_liberacao   DATE,
  data_analise     DATE,
  data_remessa     DATE,
  data_retorno     DATE,
  dupl_pag NUMBER(9),
  parcela_pag VARCHAR2(2),
  tipo_pag   NUMBER(2),
  cnpj9_pag NUMBER(9),
  cnpj4_pag NUMBER(4),
  cnpj2_pag NUMBER(2));

/

exec inter_pr_recompile;
