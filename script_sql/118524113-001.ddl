alter table basi_266 drop constraint pk_basi_266;

alter table basi_266 ADD (cod_processo number (2) default 0 not null);

alter table basi_266 add constraint pk_basi_266 primary key (cod_empresa, cod_moeda, cod_processo);
