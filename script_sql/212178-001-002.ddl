insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpb_l016', 'Troca de Cor/Variante da Ordem de Tingimento', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpb_l016', 'pcpb_menu',1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpb_l016', 'pcpb_menu', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Troca de Cor/Variante da Ordem de Tingimento'
 where hdoc_036.codigo_programa = 'pcpb_l016'
   and hdoc_036.locale          = 'es_ES';
commit;
