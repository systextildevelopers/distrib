CREATE table "PCPB_140" (
    "ORDEM_PRODUCAO" NUMBER,
    "USUARIO"        VARCHAR2(244),
    "DATA"           DATE,
    "LIBERADO"       VARCHAR2(1),
    constraint  "PCPB_140_PK" primary key ("ORDEM_PRODUCAO")
)
