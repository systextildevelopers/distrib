alter table i_obrf_010 add conferencia_almox number(1);

alter table i_obrf_010 modify conferencia_almox default 0;

exec inter_pr_recompile;
