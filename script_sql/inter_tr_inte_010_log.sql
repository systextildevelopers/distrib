
  CREATE OR REPLACE TRIGGER "INTER_TR_INTE_010_LOG" 
after insert or delete or update
on inte_010
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   
   v_nome_programa := inter_fn_nome_programa(ws_sid); 

 if inserting
 then
    begin

        insert into inte_010_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           TIPO_REGISTRO_OLD,   /*8*/
           TIPO_REGISTRO_NEW,   /*9*/
           NOME_CLIENTE_OLD,   /*10*/
           NOME_CLIENTE_NEW,   /*11*/
           FANTASIA_CLIENTE_OLD,   /*12*/
           FANTASIA_CLIENTE_NEW,   /*13*/
           FISICA_JURIDICA_OLD,   /*14*/
           FISICA_JURIDICA_NEW,   /*15*/
           ENDERECO_CLIENTE_OLD,   /*16*/
           ENDERECO_CLIENTE_NEW,   /*17*/
           BAIRRO_OLD,   /*18*/
           BAIRRO_NEW,   /*19*/
           COD_CIDADE_OLD,   /*20*/
           COD_CIDADE_NEW,   /*21*/
           TELEX_CLIENTE_OLD,   /*22*/
           TELEX_CLIENTE_NEW,   /*23*/
           CXPOSTAL_CLIENTE_OLD,   /*24*/
           CXPOSTAL_CLIENTE_NEW,   /*25*/
           CEP_CLIENTE_OLD,   /*26*/
           CEP_CLIENTE_NEW,   /*27*/
           INSC_EST_CLIENTE_OLD,   /*28*/
           INSC_EST_CLIENTE_NEW,   /*29*/
           NR_SUFRAMA_CLI_OLD,   /*30*/
           NR_SUFRAMA_CLI_NEW,   /*31*/
           SUB_REGIAO_OLD,   /*32*/
           SUB_REGIAO_NEW,   /*33*/
           PERC_DESC_DUPLIC_OLD,   /*34*/
           PERC_DESC_DUPLIC_NEW,   /*35*/
           CDREPRES_CLIENTE_OLD,   /*36*/
           CDREPRES_CLIENTE_NEW,   /*37*/
           TRAN_CLI_FORNE9_OLD,   /*38*/
           TRAN_CLI_FORNE9_NEW,   /*39*/
           TRAN_CLI_FORNE4_OLD,   /*40*/
           TRAN_CLI_FORNE4_NEW,   /*41*/
           TRAN_CLI_FORNE2_OLD,   /*42*/
           TRAN_CLI_FORNE2_NEW,   /*43*/
           DATA_CAD_CLIENTE_OLD,   /*44*/
           DATA_CAD_CLIENTE_NEW,   /*45*/
           SIT_IMPORTACAO_OLD,   /*46*/
           SIT_IMPORTACAO_NEW,   /*47*/
           DATA_ARQUIVO_OLD,   /*48*/
           DATA_ARQUIVO_NEW,   /*49*/
           DATA_IMPORTACAO_OLD,   /*50*/
           DATA_IMPORTACAO_NEW,   /*51*/
           NUMERO_REG_JUNTA_OLD,   /*52*/
           NUMERO_REG_JUNTA_NEW,   /*53*/
           VALOR_FATURAMENTO_ANUAL_OLD,   /*54*/
           VALOR_FATURAMENTO_ANUAL_NEW,   /*55*/
           VALOR_COMPRAS_MENSAL_OLD,   /*56*/
           VALOR_COMPRAS_MENSAL_NEW,   /*57*/
           PREDIO_PROPRIO_OLD,   /*58*/
           PREDIO_PROPRIO_NEW,   /*59*/
           SUG_LIMITE_MAX_PED2_OLD,   /*60*/
           SUG_LIMITE_MAX_PED2_NEW,   /*61*/
           SUG_LIMITE_MAX_PED4_OLD,   /*62*/
           SUG_LIMITE_MAX_PED4_NEW,   /*63*/
           SUG_LIMITE_MAX_PED7_OLD,   /*64*/
           SUG_LIMITE_MAX_PED7_NEW,   /*65*/
           DATA_FUNDACAO_OLD,   /*66*/
           DATA_FUNDACAO_NEW,   /*67*/
           NUMERO_FILIAIS_OLD,   /*68*/
           NUMERO_FILIAIS_NEW,   /*69*/
           CAPITAL_ATUAL_OLD,   /*70*/
           CAPITAL_ATUAL_NEW,   /*71*/
           SITUACAO_CADASTRO_OLD,   /*72*/
           SITUACAO_CADASTRO_NEW,   /*73*/
           NUMERO_IMOVEL_OLD,   /*74*/
           NUMERO_IMOVEL_NEW,   /*75*/
           COMPLEMENTO_OLD,   /*76*/
           COMPLEMENTO_NEW,   /*77*/
           NFE_E_MAIL_OLD,   /*78*/
           NFE_E_MAIL_NEW,   /*79*/
           GRUPO_ECONOMICO_OLD,   /*80*/
           GRUPO_ECONOMICO_NEW,   /*81*/
           TIPO_CLIENTE_OLD,   /*82*/
           TIPO_CLIENTE_NEW,   /*83*/
           SEGMENTO_MERCADO_OLD,   /*84*/
           SEGMENTO_MERCADO_NEW,   /*85*/
           ORIGEM_OLD,   /*88*/
           ORIGEM_NEW,   /*89*/
           CELULAR_CLIENTE_OLD,   /*90*/
           CELULAR_CLIENTE_NEW,    /*91*/
           CGC_9,    /*92*/
           CGC_4,    /*93*/
           CGC_2,    /*94*/
           PORTADOR_CLIENTE_OLD,    /*95*/
           PORTADOR_CLIENTE_NEW,    /*96*/
           E_MAIL_OLD,    /*97*/
           E_MAIL_NEW,    /*98*/
           FAX_CLIENTE_OLD,    /*99*/
           FAX_CLIENTE_NEW,    /*100*/
           TELEFONE_CLIENTE_OLD,    /*101*/
           TELEFONE_CLIENTE_NEW,    /*102*/
           SUG_LIMITE_MAX_PED1_OLD,    /*103*/
           SUG_LIMITE_MAX_PED1_NEW    /*104*/

        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.TIPO_REGISTRO, /*9*/
           '',/*10*/
           :new.NOME_CLIENTE, /*11*/
           '',/*12*/
           :new.FANTASIA_CLIENTE, /*13*/
           '',/*14*/
           :new.FISICA_JURIDICA, /*15*/
           '',/*16*/
           :new.ENDERECO_CLIENTE, /*17*/
           '',/*18*/
           :new.BAIRRO, /*19*/
           0,/*20*/
           :new.COD_CIDADE, /*21*/
           0,/*22*/
           :new.TELEX_CLIENTE, /*23*/
           '',/*24*/
           :new.CXPOSTAL_CLIENTE, /*25*/
           0,/*26*/
           :new.CEP_CLIENTE, /*27*/
           '',/*28*/
           :new.INSC_EST_CLIENTE, /*29*/
           '',/*30*/
           :new.NR_SUFRAMA_CLI, /*31*/
           0,/*32*/
           :new.SUB_REGIAO, /*33*/
           0,/*34*/
           :new.PERC_DESC_DUPLIC, /*35*/
           0,/*36*/
           :new.CDREPRES_CLIENTE, /*37*/
           0,/*38*/
           :new.TRAN_CLI_FORNE9, /*39*/
           0,/*40*/
           :new.TRAN_CLI_FORNE4, /*41*/
           0,/*42*/
           :new.TRAN_CLI_FORNE2, /*43*/
           null,/*44*/
           :new.DATA_CAD_CLIENTE, /*45*/
           0,/*46*/
           :new.SIT_IMPORTACAO, /*47*/
           null,/*48*/
           :new.DATA_ARQUIVO, /*49*/
           null,/*50*/
           :new.DATA_IMPORTACAO, /*51*/
           '',/*52*/
           :new.NUMERO_REG_JUNTA, /*53*/
           0,/*54*/
           :new.VALOR_FATURAMENTO_ANUAL, /*55*/
           0,/*56*/
           :new.VALOR_COMPRAS_MENSAL, /*57*/
           '',/*58*/
           :new.PREDIO_PROPRIO, /*59*/
           0,/*60*/
           :new.SUG_LIMITE_MAX_PED2, /*61*/
           0,/*62*/
           :new.SUG_LIMITE_MAX_PED4, /*63*/
           0,/*64*/
           :new.SUG_LIMITE_MAX_PED7, /*65*/
           null,/*66*/
           :new.DATA_FUNDACAO, /*67*/
           0,/*68*/
           :new.NUMERO_FILIAIS, /*69*/
           0,/*70*/
           :new.CAPITAL_ATUAL, /*71*/
           '',/*72*/
           :new.SITUACAO_CADASTRO, /*73*/
           '',/*74*/
           :new.NUMERO_IMOVEL, /*75*/
           '',/*76*/
           :new.COMPLEMENTO, /*77*/
           '',/*78*/
           :new.NFE_E_MAIL, /*79*/
           0,/*80*/
           :new.GRUPO_ECONOMICO, /*81*/
           0,/*82*/
           :new.TIPO_CLIENTE, /*83*/
           0,/*84*/
           :new.SEGMENTO_MERCADO, /*85*/
           0,/*88*/
           :new.ORIGEM, /*89*/
           0,/*90*/
           :new.CELULAR_CLIENTE,  /*91*/
           :new.CGC_9,                /*92*/
           :new.CGC_4,                /*93*/
           :new.CGC_2,                /*94*/
           0,     /*95*/
           :new.PORTADOR_CLIENTE,     /*96*/
           '',               /*97*/
           :new.E_MAIL,               /*98*/
           0,          /*99*/
           :new.FAX_CLIENTE,          /*100*/
           0,     /*101*/
           :new.TELEFONE_CLIENTE,     /*102*/
           0,  /*103*/
           :new.SUG_LIMITE_MAX_PED1   /*104*/

         );
    end;
 end if;


 if updating
 then
    begin
        insert into inte_010_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           TIPO_REGISTRO_OLD, /*8*/
           TIPO_REGISTRO_NEW, /*9*/
           NOME_CLIENTE_OLD, /*10*/
           NOME_CLIENTE_NEW, /*11*/
           FANTASIA_CLIENTE_OLD, /*12*/
           FANTASIA_CLIENTE_NEW, /*13*/
           FISICA_JURIDICA_OLD, /*14*/
           FISICA_JURIDICA_NEW, /*15*/
           ENDERECO_CLIENTE_OLD, /*16*/
           ENDERECO_CLIENTE_NEW, /*17*/
           BAIRRO_OLD, /*18*/
           BAIRRO_NEW, /*19*/
           COD_CIDADE_OLD, /*20*/
           COD_CIDADE_NEW, /*21*/
           TELEX_CLIENTE_OLD, /*22*/
           TELEX_CLIENTE_NEW, /*23*/
           CXPOSTAL_CLIENTE_OLD, /*24*/
           CXPOSTAL_CLIENTE_NEW, /*25*/
           CEP_CLIENTE_OLD, /*26*/
           CEP_CLIENTE_NEW, /*27*/
           INSC_EST_CLIENTE_OLD, /*28*/
           INSC_EST_CLIENTE_NEW, /*29*/
           NR_SUFRAMA_CLI_OLD, /*30*/
           NR_SUFRAMA_CLI_NEW, /*31*/
           SUB_REGIAO_OLD, /*32*/
           SUB_REGIAO_NEW, /*33*/
           PERC_DESC_DUPLIC_OLD, /*34*/
           PERC_DESC_DUPLIC_NEW, /*35*/
           CDREPRES_CLIENTE_OLD, /*36*/
           CDREPRES_CLIENTE_NEW, /*37*/
           TRAN_CLI_FORNE9_OLD, /*38*/
           TRAN_CLI_FORNE9_NEW, /*39*/
           TRAN_CLI_FORNE4_OLD, /*40*/
           TRAN_CLI_FORNE4_NEW, /*41*/
           TRAN_CLI_FORNE2_OLD, /*42*/
           TRAN_CLI_FORNE2_NEW, /*43*/
           DATA_CAD_CLIENTE_OLD, /*44*/
           DATA_CAD_CLIENTE_NEW, /*45*/
           SIT_IMPORTACAO_OLD, /*46*/
           SIT_IMPORTACAO_NEW, /*47*/
           DATA_ARQUIVO_OLD, /*48*/
           DATA_ARQUIVO_NEW, /*49*/
           DATA_IMPORTACAO_OLD, /*50*/
           DATA_IMPORTACAO_NEW, /*51*/
           NUMERO_REG_JUNTA_OLD, /*52*/
           NUMERO_REG_JUNTA_NEW, /*53*/
           VALOR_FATURAMENTO_ANUAL_OLD, /*54*/
           VALOR_FATURAMENTO_ANUAL_NEW, /*55*/
           VALOR_COMPRAS_MENSAL_OLD, /*56*/
           VALOR_COMPRAS_MENSAL_NEW, /*57*/
           PREDIO_PROPRIO_OLD, /*58*/
           PREDIO_PROPRIO_NEW, /*59*/
           SUG_LIMITE_MAX_PED2_OLD, /*60*/
           SUG_LIMITE_MAX_PED2_NEW, /*61*/
           SUG_LIMITE_MAX_PED4_OLD, /*62*/
           SUG_LIMITE_MAX_PED4_NEW, /*63*/
           SUG_LIMITE_MAX_PED7_OLD, /*64*/
           SUG_LIMITE_MAX_PED7_NEW, /*65*/
           DATA_FUNDACAO_OLD, /*66*/
           DATA_FUNDACAO_NEW, /*67*/
           NUMERO_FILIAIS_OLD, /*68*/
           NUMERO_FILIAIS_NEW, /*69*/
           CAPITAL_ATUAL_OLD, /*70*/
           CAPITAL_ATUAL_NEW, /*71*/
           SITUACAO_CADASTRO_OLD, /*72*/
           SITUACAO_CADASTRO_NEW, /*73*/
           NUMERO_IMOVEL_OLD, /*74*/
           NUMERO_IMOVEL_NEW, /*75*/
           COMPLEMENTO_OLD, /*76*/
           COMPLEMENTO_NEW, /*77*/
           NFE_E_MAIL_OLD, /*78*/
           NFE_E_MAIL_NEW, /*79*/
           GRUPO_ECONOMICO_OLD, /*80*/
           GRUPO_ECONOMICO_NEW, /*81*/
           TIPO_CLIENTE_OLD, /*82*/
           TIPO_CLIENTE_NEW, /*83*/
           SEGMENTO_MERCADO_OLD, /*84*/
           SEGMENTO_MERCADO_NEW, /*85*/
           ORIGEM_OLD, /*88*/
           ORIGEM_NEW, /*89*/
           CELULAR_CLIENTE_OLD, /*90*/
           CELULAR_CLIENTE_NEW,    /*91*/
           CGC_9,    /*92*/
           CGC_4,    /*93*/
           CGC_2,    /*94*/
           PORTADOR_CLIENTE_OLD,    /*95*/
           PORTADOR_CLIENTE_NEW,    /*96*/
           E_MAIL_OLD,    /*97*/
           E_MAIL_NEW,    /*98*/
           FAX_CLIENTE_OLD,    /*99*/
           FAX_CLIENTE_NEW,    /*100*/
           TELEFONE_CLIENTE_OLD,    /*101*/
           TELEFONE_CLIENTE_NEW,    /*102*/
           SUG_LIMITE_MAX_PED1_OLD,    /*103*/
           SUG_LIMITE_MAX_PED1_NEW    /*104*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.TIPO_REGISTRO,  /*8*/
           :new.TIPO_REGISTRO, /*9*/
           :old.NOME_CLIENTE,  /*10*/
           :new.NOME_CLIENTE, /*11*/
           :old.FANTASIA_CLIENTE,  /*12*/
           :new.FANTASIA_CLIENTE, /*13*/
           :old.FISICA_JURIDICA,  /*14*/
           :new.FISICA_JURIDICA, /*15*/
           :old.ENDERECO_CLIENTE,  /*16*/
           :new.ENDERECO_CLIENTE, /*17*/
           :old.BAIRRO,  /*18*/
           :new.BAIRRO, /*19*/
           :old.COD_CIDADE,  /*20*/
           :new.COD_CIDADE, /*21*/
           :old.TELEX_CLIENTE,  /*22*/
           :new.TELEX_CLIENTE, /*23*/
           :old.CXPOSTAL_CLIENTE,  /*24*/
           :new.CXPOSTAL_CLIENTE, /*25*/
           :old.CEP_CLIENTE,  /*26*/
           :new.CEP_CLIENTE, /*27*/
           :old.INSC_EST_CLIENTE,  /*28*/
           :new.INSC_EST_CLIENTE, /*29*/
           :old.NR_SUFRAMA_CLI,  /*30*/
           :new.NR_SUFRAMA_CLI, /*31*/
           :old.SUB_REGIAO,  /*32*/
           :new.SUB_REGIAO, /*33*/
           :old.PERC_DESC_DUPLIC,  /*34*/
           :new.PERC_DESC_DUPLIC, /*35*/
           :old.CDREPRES_CLIENTE,  /*36*/
           :new.CDREPRES_CLIENTE, /*37*/
           :old.TRAN_CLI_FORNE9,  /*38*/
           :new.TRAN_CLI_FORNE9, /*39*/
           :old.TRAN_CLI_FORNE4,  /*40*/
           :new.TRAN_CLI_FORNE4, /*41*/
           :old.TRAN_CLI_FORNE2,  /*42*/
           :new.TRAN_CLI_FORNE2, /*43*/
           :old.DATA_CAD_CLIENTE,  /*44*/
           :new.DATA_CAD_CLIENTE, /*45*/
           :old.SIT_IMPORTACAO,  /*46*/
           :new.SIT_IMPORTACAO, /*47*/
           :old.DATA_ARQUIVO,  /*48*/
           :new.DATA_ARQUIVO, /*49*/
           :old.DATA_IMPORTACAO,  /*50*/
           :new.DATA_IMPORTACAO, /*51*/
           :old.NUMERO_REG_JUNTA,  /*52*/
           :new.NUMERO_REG_JUNTA, /*53*/
           :old.VALOR_FATURAMENTO_ANUAL,  /*54*/
           :new.VALOR_FATURAMENTO_ANUAL, /*55*/
           :old.VALOR_COMPRAS_MENSAL,  /*56*/
           :new.VALOR_COMPRAS_MENSAL, /*57*/
           :old.PREDIO_PROPRIO,  /*58*/
           :new.PREDIO_PROPRIO, /*59*/
           :old.SUG_LIMITE_MAX_PED2,  /*60*/
           :new.SUG_LIMITE_MAX_PED2, /*61*/
           :old.SUG_LIMITE_MAX_PED4,  /*62*/
           :new.SUG_LIMITE_MAX_PED4, /*63*/
           :old.SUG_LIMITE_MAX_PED7,  /*64*/
           :new.SUG_LIMITE_MAX_PED7, /*65*/
           :old.DATA_FUNDACAO,  /*66*/
           :new.DATA_FUNDACAO, /*67*/
           :old.NUMERO_FILIAIS,  /*68*/
           :new.NUMERO_FILIAIS, /*69*/
           :old.CAPITAL_ATUAL,  /*70*/
           :new.CAPITAL_ATUAL, /*71*/
           :old.SITUACAO_CADASTRO,  /*72*/
           :new.SITUACAO_CADASTRO, /*73*/
           :old.NUMERO_IMOVEL,  /*74*/
           :new.NUMERO_IMOVEL, /*75*/
           :old.COMPLEMENTO,  /*76*/
           :new.COMPLEMENTO, /*77*/
           :old.NFE_E_MAIL,  /*78*/
           :new.NFE_E_MAIL, /*79*/
           :old.GRUPO_ECONOMICO,  /*80*/
           :new.GRUPO_ECONOMICO, /*81*/
           :old.TIPO_CLIENTE,  /*82*/
           :new.TIPO_CLIENTE, /*83*/
           :old.SEGMENTO_MERCADO,  /*84*/
           :new.SEGMENTO_MERCADO, /*85*/
           :old.ORIGEM,  /*88*/
           :new.ORIGEM, /*89*/
           :old.CELULAR_CLIENTE,  /*90*/
           :new.CELULAR_CLIENTE,  /*91*/
           :new.CGC_9,                /*92*/
           :new.CGC_4,                /*93*/
           :new.CGC_2,                /*94*/
           :old.PORTADOR_CLIENTE,     /*95*/
           :new.PORTADOR_CLIENTE,     /*96*/
           :old.E_MAIL,               /*97*/
           :new.E_MAIL,               /*98*/
           :old.FAX_CLIENTE,          /*99*/
           :new.FAX_CLIENTE,          /*100*/
           :old.TELEFONE_CLIENTE,     /*101*/
           :new.TELEFONE_CLIENTE,     /*102*/
           :old.SUG_LIMITE_MAX_PED1,  /*103*/
           :new.SUG_LIMITE_MAX_PED1   /*104*/

         );
    end;
 end if;


 if deleting
 then
    begin
        insert into inte_010_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           TIPO_REGISTRO_OLD, /*8*/
           TIPO_REGISTRO_NEW, /*9*/
           NOME_CLIENTE_OLD, /*10*/
           NOME_CLIENTE_NEW, /*11*/
           FANTASIA_CLIENTE_OLD, /*12*/
           FANTASIA_CLIENTE_NEW, /*13*/
           FISICA_JURIDICA_OLD, /*14*/
           FISICA_JURIDICA_NEW, /*15*/
           ENDERECO_CLIENTE_OLD, /*16*/
           ENDERECO_CLIENTE_NEW, /*17*/
           BAIRRO_OLD, /*18*/
           BAIRRO_NEW, /*19*/
           COD_CIDADE_OLD, /*20*/
           COD_CIDADE_NEW, /*21*/
           TELEX_CLIENTE_OLD, /*22*/
           TELEX_CLIENTE_NEW, /*23*/
           CXPOSTAL_CLIENTE_OLD, /*24*/
           CXPOSTAL_CLIENTE_NEW, /*25*/
           CEP_CLIENTE_OLD, /*26*/
           CEP_CLIENTE_NEW, /*27*/
           INSC_EST_CLIENTE_OLD, /*28*/
           INSC_EST_CLIENTE_NEW, /*29*/
           NR_SUFRAMA_CLI_OLD, /*30*/
           NR_SUFRAMA_CLI_NEW, /*31*/
           SUB_REGIAO_OLD, /*32*/
           SUB_REGIAO_NEW, /*33*/
           PERC_DESC_DUPLIC_OLD, /*34*/
           PERC_DESC_DUPLIC_NEW, /*35*/
           CDREPRES_CLIENTE_OLD, /*36*/
           CDREPRES_CLIENTE_NEW, /*37*/
           TRAN_CLI_FORNE9_OLD, /*38*/
           TRAN_CLI_FORNE9_NEW, /*39*/
           TRAN_CLI_FORNE4_OLD, /*40*/
           TRAN_CLI_FORNE4_NEW, /*41*/
           TRAN_CLI_FORNE2_OLD, /*42*/
           TRAN_CLI_FORNE2_NEW, /*43*/
           DATA_CAD_CLIENTE_OLD, /*44*/
           DATA_CAD_CLIENTE_NEW, /*45*/
           SIT_IMPORTACAO_OLD, /*46*/
           SIT_IMPORTACAO_NEW, /*47*/
           DATA_ARQUIVO_OLD, /*48*/
           DATA_ARQUIVO_NEW, /*49*/
           DATA_IMPORTACAO_OLD, /*50*/
           DATA_IMPORTACAO_NEW, /*51*/
           NUMERO_REG_JUNTA_OLD, /*52*/
           NUMERO_REG_JUNTA_NEW, /*53*/
           VALOR_FATURAMENTO_ANUAL_OLD, /*54*/
           VALOR_FATURAMENTO_ANUAL_NEW, /*55*/
           VALOR_COMPRAS_MENSAL_OLD, /*56*/
           VALOR_COMPRAS_MENSAL_NEW, /*57*/
           PREDIO_PROPRIO_OLD, /*58*/
           PREDIO_PROPRIO_NEW, /*59*/
           SUG_LIMITE_MAX_PED2_OLD, /*60*/
           SUG_LIMITE_MAX_PED2_NEW, /*61*/
           SUG_LIMITE_MAX_PED4_OLD, /*62*/
           SUG_LIMITE_MAX_PED4_NEW, /*63*/
           SUG_LIMITE_MAX_PED7_OLD, /*64*/
           SUG_LIMITE_MAX_PED7_NEW, /*65*/
           DATA_FUNDACAO_OLD, /*66*/
           DATA_FUNDACAO_NEW, /*67*/
           NUMERO_FILIAIS_OLD, /*68*/
           NUMERO_FILIAIS_NEW, /*69*/
           CAPITAL_ATUAL_OLD, /*70*/
           CAPITAL_ATUAL_NEW, /*71*/
           SITUACAO_CADASTRO_OLD, /*72*/
           SITUACAO_CADASTRO_NEW, /*73*/
           NUMERO_IMOVEL_OLD, /*74*/
           NUMERO_IMOVEL_NEW, /*75*/
           COMPLEMENTO_OLD, /*76*/
           COMPLEMENTO_NEW, /*77*/
           NFE_E_MAIL_OLD, /*78*/
           NFE_E_MAIL_NEW, /*79*/
           GRUPO_ECONOMICO_OLD, /*80*/
           GRUPO_ECONOMICO_NEW, /*81*/
           TIPO_CLIENTE_OLD, /*82*/
           TIPO_CLIENTE_NEW, /*83*/
           SEGMENTO_MERCADO_OLD, /*84*/
           SEGMENTO_MERCADO_NEW, /*85*/
           ORIGEM_OLD, /*88*/
           ORIGEM_NEW, /*89*/
           CELULAR_CLIENTE_OLD, /*90*/
           CELULAR_CLIENTE_NEW,    /*91*/
           CGC_9,    /*92*/
           CGC_4,    /*93*/
           CGC_2,    /*94*/
           PORTADOR_CLIENTE_OLD,    /*95*/
           PORTADOR_CLIENTE_NEW,    /*96*/
           E_MAIL_OLD,    /*97*/
           E_MAIL_NEW,    /*98*/
           FAX_CLIENTE_OLD,    /*99*/
           FAX_CLIENTE_NEW,    /*100*/
           TELEFONE_CLIENTE_OLD,    /*101*/
           TELEFONE_CLIENTE_NEW,    /*102*/
           SUG_LIMITE_MAX_PED1_OLD,    /*103*/
           SUG_LIMITE_MAX_PED1_NEW    /*104*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.TIPO_REGISTRO, /*8*/
           0, /*9*/
           :old.NOME_CLIENTE, /*10*/
           '', /*11*/
           :old.FANTASIA_CLIENTE, /*12*/
           '', /*13*/
           :old.FISICA_JURIDICA, /*14*/
           '', /*15*/
           :old.ENDERECO_CLIENTE, /*16*/
           '', /*17*/
           :old.BAIRRO, /*18*/
           '', /*19*/
           :old.COD_CIDADE, /*20*/
           0, /*21*/
           :old.TELEX_CLIENTE, /*22*/
           0, /*23*/
           :old.CXPOSTAL_CLIENTE, /*24*/
           '', /*25*/
           :old.CEP_CLIENTE, /*26*/
           0, /*27*/
           :old.INSC_EST_CLIENTE, /*28*/
           '', /*29*/
           :old.NR_SUFRAMA_CLI, /*30*/
           '', /*31*/
           :old.SUB_REGIAO, /*32*/
           0, /*33*/
           :old.PERC_DESC_DUPLIC, /*34*/
           0, /*35*/
           :old.CDREPRES_CLIENTE, /*36*/
           0, /*37*/
           :old.TRAN_CLI_FORNE9, /*38*/
           0, /*39*/
           :old.TRAN_CLI_FORNE4, /*40*/
           0, /*41*/
           :old.TRAN_CLI_FORNE2, /*42*/
           0, /*43*/
           :old.DATA_CAD_CLIENTE, /*44*/
           null, /*45*/
           :old.SIT_IMPORTACAO, /*46*/
           0, /*47*/
           :old.DATA_ARQUIVO, /*48*/
           null, /*49*/
           :old.DATA_IMPORTACAO, /*50*/
           null, /*51*/
           :old.NUMERO_REG_JUNTA, /*52*/
           '', /*53*/
           :old.VALOR_FATURAMENTO_ANUAL, /*54*/
           0, /*55*/
           :old.VALOR_COMPRAS_MENSAL, /*56*/
           0, /*57*/
           :old.PREDIO_PROPRIO, /*58*/
           '', /*59*/
           :old.SUG_LIMITE_MAX_PED2, /*60*/
           0, /*61*/
           :old.SUG_LIMITE_MAX_PED4, /*62*/
           0, /*63*/
           :old.SUG_LIMITE_MAX_PED7, /*64*/
           0, /*65*/
           :old.DATA_FUNDACAO, /*66*/
           null, /*67*/
           :old.NUMERO_FILIAIS, /*68*/
           0, /*69*/
           :old.CAPITAL_ATUAL, /*70*/
           0, /*71*/
           :old.SITUACAO_CADASTRO, /*72*/
           '', /*73*/
           :old.NUMERO_IMOVEL, /*74*/
           '', /*75*/
           :old.COMPLEMENTO, /*76*/
           '', /*77*/
           :old.NFE_E_MAIL, /*78*/
           '', /*79*/
           :old.GRUPO_ECONOMICO, /*80*/
           0, /*81*/
           :old.TIPO_CLIENTE, /*82*/
           0, /*83*/
           :old.SEGMENTO_MERCADO, /*84*/
           0, /*85*/
           :old.ORIGEM, /*88*/
           0, /*89*/
           :old.CELULAR_CLIENTE, /*90*/
           0,  /*91*/
           :old.CGC_9,                /*92*/
           :old.CGC_4,                /*93*/
           :old.CGC_2,                /*94*/
           :old.PORTADOR_CLIENTE,     /*95*/
           0,     /*96*/
           :old.E_MAIL,               /*97*/
           '',               /*98*/
           :old.FAX_CLIENTE,          /*99*/
           0,          /*100*/
           :old.TELEFONE_CLIENTE,     /*101*/
           0,     /*102*/
           :old.SUG_LIMITE_MAX_PED1,  /*103*/
           0   /*104*/
         );
    end;
 end if;
end inter_tr_inte_010_log;

-- ALTER TRIGGER "INTER_TR_INTE_010_LOG" ENABLE
 

/

exec inter_pr_recompile;

