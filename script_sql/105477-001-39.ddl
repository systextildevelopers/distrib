alter table obrf_015 
add (vICMSDif51  number(15,2));

comment on column obrf_015.vICMSDif51
is 'Valor do Icms Diferido';

alter table obrf_015 
modify ( vICMSDif51  default 0.00);

/

exec inter_pr_recompile;
/
