
  CREATE OR REPLACE PROCEDURE "INTER_PR_PROCURA_DADOS_CARDEX" 
   (p_deposito              in     number,    p_nivel                 in     varchar2,
    p_grupo                 in     varchar2,  p_subgrupo              in     varchar2,
    p_item                  in     varchar2,  p_data_cardex           in     date,
    p_saldo_fisico          in out number,    p_saldo_financeiro      in out number,
    p_preco_medio_unit      in out number,    p_saldo_financeiro_proj in out number,
    p_preco_medio_unit_proj in out number,    p_saldo_financeiro_est  in out number,
    p_preco_medio_unit_est  in out number,    p_saldo_fisico_quilo    in out number)
is
BEGIN

   select estq_301.saldo_fisico,              estq_301.saldo_financeiro,
          estq_301.preco_medio_unitario,      estq_301.saldo_financeiro_proj,
          estq_301.preco_medio_unit_proj,     estq_301.saldo_financeiro_estimado,
          estq_301.preco_medio_unit_estimado, nvl(estq_301.saldo_fisico_quilo, 0)
   into   p_saldo_fisico,                     p_saldo_financeiro,
          p_preco_medio_unit,                 p_saldo_financeiro_proj,
          p_preco_medio_unit_proj,            p_saldo_financeiro_est,
          p_preco_medio_unit_est,             p_saldo_fisico_quilo
   from estq_301, (

      select max(estq_301.mes_ano_movimento) mes_ano_movimento
      from estq_301
      where estq_301.codigo_deposito    = p_deposito
        and estq_301.nivel_estrutura    = p_nivel
        and estq_301.grupo_estrutura    = p_grupo
        and estq_301.subgrupo_estrutura = p_subgrupo
        and estq_301.item_estrutura     = p_item
        and estq_301.saldo_fisico is not null
        and estq_301.mes_ano_movimento  < p_data_cardex
      ORDER BY estq_301.mes_ano_movimento asc) estq301

   where estq_301.codigo_deposito    = p_deposito
     and estq_301.nivel_estrutura    = p_nivel
     and estq_301.grupo_estrutura    = p_grupo
     and estq_301.subgrupo_estrutura = p_subgrupo
     and estq_301.item_estrutura     = p_item
     and estq_301.mes_ano_movimento  = estq301.mes_ano_movimento;

   exception
      when others then
         p_saldo_fisico          := 0.000;
         p_saldo_financeiro      := 0.000;
         p_preco_medio_unit      := 0.000;
         p_saldo_financeiro_proj := 0.000;
         p_preco_medio_unit_proj := 0.000;
         p_saldo_financeiro_est  := 0.000;
         p_preco_medio_unit_est  := 0.000;
         p_saldo_fisico_quilo    := 0.000;
end inter_pr_procura_dados_cardex;

 

/

exec inter_pr_recompile;

