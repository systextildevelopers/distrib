
  CREATE OR REPLACE FUNCTION "INTER_FN_VIEW_ATRASO_MEDIO" (
   p_cnpj_9 in number, p_cnpj_4 in number, 
   p_cnpj_2 in number, p_mes    in number, 
   p_ano    in number) 
return number 
is

   -- formula de calculo do atraso medio
   --
   -- ((acumulo_atraso * atraso_medio) + (valor_pago * dias_atraso_dupl))
   -- -------------------------------------------------------------------
   --                (acumulo_atraso + valor_pago)
   
   -- Onde: acumulo_atraso   = e' o acumulo de todos os valores pagos 
   --                          (nao considera o valor do pagamento que se esta processamdo)
   --       atraso_medio     = qtde de dias em atraso (ponderado)
   --       valor_pago       = Valor pago da duplicata
   --       dias_atraso_dupl = qtde de dias que o titulo foi pago em atraso

   cursor fatu075 (p_data_inicio date, p_data_fim date) is
      select fatu_075.nr_titul_codempr,          fatu_075.nr_titul_cli_dup_cgc_cli9,
             fatu_075.nr_titul_cli_dup_cgc_cli4, fatu_075.nr_titul_cli_dup_cgc_cli2,
             fatu_075.nr_titul_cod_tit,          fatu_075.nr_titul_num_dup,
             fatu_075.nr_titul_seq_dup,          fatu_075.data_pagamento,
             fatu_075.valor_pago,                cont_010.sinal_titulo
      from fatu_075, cont_010, cpag_040
      where fatu_075.nr_titul_cli_dup_cgc_cli9 = p_cnpj_9
      and   fatu_075.nr_titul_cli_dup_cgc_cli4 = p_cnpj_4
      and   fatu_075.nr_titul_cli_dup_cgc_cli2 = p_cnpj_2
      and   fatu_075.data_pagamento      between p_data_inicio and p_data_fim
      and   fatu_075.historico_pgto            = cont_010.codigo_historico
      and   cont_010.sinal_titulo             in (1,2)
      and   fatu_075.nr_titul_cod_tit          = cpag_040.tipo_titulo
      and   cpag_040.at_inform_fin             = 1;
   

   v_data_inicio          date;
   v_data_fim             date;
   v_data_prorrogacao     date;
   v_data_prorrogacao_aux date;   
   v_dias_atraso_dupl     number;
   v_valor_pago           number;
   v_acumulo_atraso       number := 0.00;
   v_atraso_medio         number := 0.00;
        
begin
   
   v_data_inicio := to_date('01/' || p_mes || '/' || p_ano, 'dd/mm/yyyy');
   v_data_fim    := last_day(v_data_inicio);
   
   for reg_fatu075 in fatu075 (v_data_inicio, v_data_fim)
   loop
   
      if reg_fatu075.sinal_titulo = 1
      then
         v_valor_pago := reg_fatu075.valor_pago;
      else
         v_valor_pago := reg_fatu075.valor_pago * (-1.0);
      end if;

      select data_prorrogacao into v_data_prorrogacao from fatu_070
      where codigo_empresa   = reg_fatu075.nr_titul_codempr
      and   cli_dup_cgc_cli9 = reg_fatu075.nr_titul_cli_dup_cgc_cli9
      and   cli_dup_cgc_cli4 = reg_fatu075.nr_titul_cli_dup_cgc_cli4
      and   cli_dup_cgc_cli2 = reg_fatu075.nr_titul_cli_dup_cgc_cli2
      and   tipo_titulo      = reg_fatu075.nr_titul_cod_tit
      and   num_duplicata    = reg_fatu075.nr_titul_num_dup
      and   seq_duplicatas   = reg_fatu075.nr_titul_seq_dup;

      if reg_fatu075.data_pagamento > v_data_prorrogacao
      then

         begin
            select data_calendario into v_data_prorrogacao_aux from basi_260
            where data_calendario >= v_data_prorrogacao
            and   dia_util_finan   = 0;
            
            v_data_prorrogacao := v_data_prorrogacao_aux;
            
         exception
            when others then
               v_data_prorrogacao_aux := v_data_prorrogacao;
         end;
      end if;

         
      if reg_fatu075.data_pagamento > v_data_prorrogacao
      then
         v_dias_atraso_dupl := reg_fatu075.data_pagamento - v_data_prorrogacao;
      else 
         v_dias_atraso_dupl := 0;
      end if;

      if v_acumulo_atraso + reg_fatu075.valor_pago > 0.00
      then
         v_atraso_medio   := round(((v_acumulo_atraso * v_atraso_medio)            + 
                                    (reg_fatu075.valor_pago * v_dias_atraso_dupl)) / 
                                    (v_acumulo_atraso + reg_fatu075.valor_pago),2);
                      
         v_acumulo_atraso := v_acumulo_atraso + reg_fatu075.valor_pago;
      else
         v_atraso_medio   := 0.00;
         v_acumulo_atraso := 0.00;
      end if;
   end loop;

   return(v_atraso_medio);
end inter_fn_view_atraso_medio;

 

/

exec inter_pr_recompile;

