declare
  i NUMBER := 0;

begin
   for nota in (select * from fatu_050 
                where fatu_050.nota_pendente is null)
   loop 
      update fatu_050 
        set nota_pendente = 0
      where fatu_050.codigo_empresa  = nota.codigo_empresa
        and fatu_050.num_nota_fiscal = nota.num_nota_fiscal
        and fatu_050.serie_nota_fisc = nota.serie_nota_fisc;
   
      i := i + 1;
      IF i > 1000 THEN
         COMMIT;
         i := 0;
      END IF;
   end loop;
end;
