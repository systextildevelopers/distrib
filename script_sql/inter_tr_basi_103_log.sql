
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_103_LOG" 
   after insert or delete or update
       of  CODIGO_PROJETO,            SEQUENCIA_PROJETO,
           CODIGO_ATIVIDADE,          SEQUENCIA_ATIVIDADE,
           ORDENACAO_ATIVIDADE,       TEMPO,
           CODIGO_RESPONSAVEL,        DATA_INICIO_PREVISTA,
           DATA_TERMINO_PREVISTA,     ATIVIDADE_CRITICA,
           DATA_INICIO_REALIZADA,     DATA_TERMINO_REALIZADA,
           DATA_FIXA,                 CODIGO_CARGO
   on basi_103
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_cod_tipo_produto_n     varchar2(60);
   ws_cod_tipo_produto_o     varchar2(60);

   ws_tipo_produto_n         varchar2(60);
   ws_tipo_produto_o         varchar2(60);

   ws_desc_atividade_o       varchar2(60);
   ws_desc_atividade_n       varchar2(60);

   ws_desc_projeto_n         varchar2(60);
   ws_desc_projeto_o         varchar2(60);

   ws_resp_projeto_o         number(5);
   ws_resp_projeto_n         number(5);

   ws_cod_responsavel_o      integer;
   ws_cod_responsavel_n      integer;

   ws_nome_responsavel_o     varchar2(60);
   ws_nome_responsavel_n     varchar2(60);

   ws_desc_cargo_o           varchar2(30);
   ws_desc_cargo_n           varchar2(30);

   ws_teve                   varchar2(1);

   long_aux                  varchar2(2000);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = :new.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_n := '';
      end;

      begin
         select basi_003.descricao
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n;
      exception when no_data_found then
         ws_tipo_produto_n := '';
      end;

      begin
         select basi_002.descricao
         into ws_desc_atividade_n
         from basi_002
         where basi_002.codigo_atividade = :new.codigo_atividade;
      exception when no_data_found then
         ws_desc_atividade_n := '';
      end;

      begin
         select basi_001.descricao_projeto, basi_001.codigo_responsavel
         into ws_desc_projeto_n, ws_resp_projeto_n
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_n := '';
         ws_resp_projeto_n := 0;
      end;

      if :new.codigo_responsavel = 0
      then ws_cod_responsavel_n := ws_resp_projeto_n;
      else ws_cod_responsavel_n := :new.codigo_responsavel;
      end if;

      begin
         select nvl(efic_050.nome,' ')
         into ws_nome_responsavel_n
         from efic_050
         where efic_050.cod_empresa     = ws_empresa
           and efic_050.cod_funcionario = ws_cod_responsavel_n;
      exception when no_data_found then
         ws_nome_responsavel_n := '';
      end;

      begin
         select nvl(mqop_120.descricao, ' ')
         into ws_desc_cargo_n
         from mqop_120
         where mqop_120.codigo_cargo = :new.codigo_cargo;
      exception when no_data_found then
         ws_desc_cargo_n := '';
      end;

      INSERT INTO hist_100
         ( tabela,              operacao,
           data_ocorr,          aplicacao,
           usuario_rede,        maquina_rede,
           str01,               num05,
           long01
         )
      VALUES
         ( 'BASI_103',          'I',
           sysdate,             ws_aplicativo,
           ws_usuario_rede,     ws_maquina_rede,
           :new.codigo_projeto,  :new.sequencia_projeto,
           inter_fn_buscar_tag('lb34900#ATIVIDADES DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                                    chr(10)               ||
                                    chr(10)               ||
           inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.codigo_projeto    ||
           ' - '                       || ws_desc_projeto_n      ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb05455#SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_projeto ||
           ' - '                       || ws_cod_tipo_produto_n  ||
           ' - '                       || ws_tipo_produto_n      ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_atividade    ||
           ' - '                       || :new.codigo_atividade  ||
           ' - '                       || ws_desc_atividade_n    ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb00838#ORDENACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.ordenacao_atividade    ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb06239#TEMPO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.tempo             ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb31305#RESPONSAVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_cod_responsavel_n   ||
           ' - '                       || ws_nome_responsavel_n  ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb12295#CARGO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.codigo_cargo      ||
           ' - '                       || ws_desc_cargo_n        ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb34901#DATA INICIO PREVISTA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:new.data_inicio_prevista,'DD/MM/YYYY')  ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb34902#DATA TERMINO PREVISTA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:new.data_termino_prevista,'DD/MM/YYYY')  ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb34903#DATA INICIO REALIZADA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:new.data_inicio_realizada,'DD/MM/YYYY')  ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb34904#DATA TERMINO REALIZADA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:new.data_termino_realizada,'DD/MM/YYYY')  ||
           chr(10)
        );
   end if;

   if updating
   then
      begin
         select basi_001.descricao_projeto, basi_001.codigo_responsavel
         into ws_desc_projeto_n, ws_resp_projeto_n
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_n := '';
         ws_resp_projeto_n := 0;
      end;

      if :new.codigo_responsavel = 0
      then ws_cod_responsavel_n := ws_resp_projeto_n;
      else ws_cod_responsavel_n := :new.codigo_responsavel;
      end if;

      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = :new.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_n := 0;
      end;

      begin
         select basi_003.descricao
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n;
      exception when no_data_found then
         ws_tipo_produto_n := '';
      end;

      ws_teve := 'n';

      long_aux := long_aux ||
           inter_fn_buscar_tag('lb34900#ATIVIDADES DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
           chr(10)                    ||
           chr(10);

      long_aux := long_aux ||
           inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.codigo_projeto    ||
           ' - '                       || ws_desc_projeto_n      ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb05455#SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_projeto ||
           ' - '                       || ws_cod_tipo_produto_n  ||
           ' - '                       || ws_tipo_produto_n      ||
           chr(10);

      if  :old.codigo_atividade <> :new.codigo_atividade
      then
          ws_teve := 's';

          long_aux := long_aux ||
          inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                           || :old.sequencia_atividade    ||
               ' - '                       || :old.codigo_atividade       ||
               ' - '                       || ws_desc_atividade_o         ||
               ' -> '                      ||
          inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                           || :new.sequencia_atividade    ||
               ' - '                       || :new.codigo_atividade       ||
               ' - '                       || ws_desc_atividade_n
                                           || chr(10);
      end if;

      if :old.ordenacao_atividade <> :new.ordenacao_atividade
      then
          ws_teve := 's';
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb00838#ORDENACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                           || :old.ordenacao_atividade    ||
               ' -> '                      || :new.ordenacao_atividade
                                           || chr(10);
      end if;

      if :old.tempo <> :new.tempo
      then
          ws_teve := 's';
          long_aux := long_aux ||
          inter_fn_buscar_tag('lb06239#TEMPO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                           || :old.tempo                  ||
               ' -> '                      || :new.tempo
                                           || chr(10);
      end if;

      if :old.codigo_responsavel <> :new.codigo_responsavel
      then
         ws_teve := 's';

         begin
            select basi_001.codigo_responsavel
            into ws_resp_projeto_o
            from basi_001
            where basi_001.codigo_projeto = :old.codigo_projeto;
         exception when no_data_found then
            ws_resp_projeto_o := 0;
         end;

         if :old.codigo_responsavel = 0
         then ws_cod_responsavel_o := ws_resp_projeto_o;
         else ws_cod_responsavel_o := :old.codigo_responsavel;
         end if;

         begin
            select efic_050.nome
            into ws_nome_responsavel_o
            from efic_050
            where efic_050.cod_empresa     = ws_empresa
              and efic_050.cod_funcionario = ws_cod_responsavel_o;
         exception when no_data_found then
            ws_nome_responsavel_o := '';
         end;

         begin
            select efic_050.nome
            into ws_nome_responsavel_n
            from efic_050
            where efic_050.cod_empresa     = ws_empresa
              and efic_050.cod_funcionario = ws_cod_responsavel_n;
         exception when no_data_found then
            ws_nome_responsavel_n := '';
         end;

         long_aux := long_aux ||
              inter_fn_buscar_tag('lb31305#RESPONSAVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || ws_cod_responsavel_o
                                          || ' - ' || ws_nome_responsavel_o || ' -> '
                                          || ws_cod_responsavel_n
                                          || ' - ' || ws_nome_responsavel_n
                                          || chr(10);
      end if;

      if :old.codigo_cargo <> :new.codigo_cargo
      then
         ws_teve := 's';
          begin
            select mqop_120.descricao
            into ws_desc_cargo_o
            from mqop_120
            where mqop_120.codigo_cargo = :old.codigo_cargo;
         exception when no_data_found then
            ws_desc_cargo_o := '';
         end;
          begin
            select mqop_120.descricao
            into ws_desc_cargo_n
            from mqop_120
            where mqop_120.codigo_cargo = :new.codigo_cargo;
         exception when no_data_found then
            ws_desc_cargo_n := '';
         end;
          long_aux := long_aux ||
              inter_fn_buscar_tag('lb12295#CARGO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || :old.codigo_cargo
                                          || ' - ' || ws_desc_cargo_o || ' -> '
                                          || :new.codigo_cargo
                                          || ' - ' || ws_desc_cargo_n
                                          || chr(10);
      end if;

      if (:old.data_inicio_prevista <> :new.data_inicio_prevista) or
         (:old.data_inicio_prevista is null)
      then
         ws_teve := 's';

         long_aux := long_aux ||
               inter_fn_buscar_tag('lb34901#DATA INICIO PREVISTA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                           || to_char(:old.data_inicio_prevista,'DD/MM/YYYY')  ||
               ' -> '                      || to_char(:new.data_inicio_prevista,'DD/MM/YYYY')
                                           || chr(10);
      end if;

      if (:old.data_termino_prevista <> :new.data_termino_prevista) or
         (:old.data_termino_prevista is null)
      then
          ws_teve := 's';

          long_aux := long_aux ||
               inter_fn_buscar_tag('lb34902#DATA TERMINO PREVISTA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                           || to_char(:old.data_termino_prevista,'DD/MM/YYYY')  ||
               ' -> '                      || to_char(:new.data_termino_prevista,'DD/MM/YYYY')
                                           || chr(10);
      end if;

      if (:old.data_inicio_realizada <> :new.data_inicio_realizada) or
         (:old.data_inicio_realizada is null and :new.data_inicio_realizada is not null)
      then
          ws_teve := 's';
          long_aux := long_aux ||
               inter_fn_buscar_tag('lb34903#DATA INICIO REALIZADA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                           || to_char(:old.data_inicio_realizada,'DD/MM/YYYY')  ||
               ' -> '                      || to_char(:new.data_inicio_realizada,'DD/MM/YYYY')
                                           || chr(10);
      end if;

      if (:old.data_termino_realizada <> :new.data_termino_realizada) or
         (:old.data_termino_realizada is null and :new.data_termino_realizada is not null)
      then
          ws_teve := 's';
          long_aux := long_aux ||
               inter_fn_buscar_tag('lb34904#DATA TERMINO REALIZADA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                           || to_char(:old.data_termino_realizada,'DD/MM/YYYY')  ||
                ' -> '                     || to_char(:new.data_termino_realizada,'DD/MM/YYYY')
                                           || chr(10);
      end if;

      if ws_teve = 's'
      then
         INSERT INTO hist_100
            ( tabela,               operacao,
              data_ocorr,           aplicacao,
              usuario_rede,         maquina_rede,
              str01,                num05,
              long01
            )
         VALUES
            ( 'BASI_103',           'A',
              sysdate,              ws_aplicativo,
              ws_usuario_rede,      ws_maquina_rede,
              :new.codigo_projeto,  :new.sequencia_projeto,
              long_aux
           );
      end if;
   end if;

   if deleting
   then
      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_o
         from basi_101
         where basi_101.codigo_projeto    = :old.codigo_projeto
           and basi_101.sequencia_projeto = :old.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_o := '';
      end;

      begin
         select basi_003.descricao
         into ws_tipo_produto_o
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_o;
      exception when no_data_found then
         ws_tipo_produto_o := '';
      end;

      begin
         select basi_002.descricao
         into ws_desc_atividade_o
         from basi_002
         where basi_002.codigo_atividade = :old.codigo_atividade;
      exception when no_data_found then
         ws_desc_atividade_o := '';
      end;

      begin
         select basi_001.descricao_projeto, basi_001.codigo_responsavel
         into ws_desc_projeto_o, ws_resp_projeto_o
         from basi_001
         where basi_001.codigo_projeto = :old.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_o := '';
         ws_resp_projeto_o := 0;
      end;

      if :old.codigo_responsavel = 0
      then ws_cod_responsavel_o := ws_resp_projeto_o;
      else ws_cod_responsavel_o := :old.codigo_responsavel;
      end if;

      begin
         select nvl(efic_050.nome,' ')
         into ws_nome_responsavel_o
         from efic_050
         where efic_050.cod_empresa     = ws_empresa
           and efic_050.cod_funcionario = ws_cod_responsavel_o;
      exception when no_data_found then
         ws_nome_responsavel_o := '';
      end;

      begin
         select nvl(mqop_120.descricao, ' ')
         into ws_desc_cargo_o
         from mqop_120
         where mqop_120.codigo_cargo = :old.codigo_cargo;
      exception when no_data_found then
         ws_desc_cargo_o := '';
      end;

      INSERT INTO hist_100
         ( tabela,              operacao,
           data_ocorr,          aplicacao,
           usuario_rede,        maquina_rede,
           str01,               num05,
           long01
         )
      VALUES
         ( 'BASI_103',          'D',
           sysdate,             ws_aplicativo,
           ws_usuario_rede,     ws_maquina_rede,
          :old.codigo_projeto,  :old.sequencia_projeto,
           inter_fn_buscar_tag('lb34900#ATIVIDADES DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                                    chr(10)               ||
                                    chr(10)               ||
           inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.codigo_projeto    ||
           ' - '                       || ws_desc_projeto_o      ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb05455#SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.sequencia_projeto ||
           ' - '                       || ws_cod_tipo_produto_o  ||
           ' - '                       || ws_tipo_produto_o      ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.sequencia_atividade    ||
           ' - '                       || :old.codigo_atividade  ||
           ' - '                       || ws_desc_atividade_o    ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb00838#ORDENACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.ordenacao_atividade    ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb06239#TEMPO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.tempo             ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb31305#RESPONSAVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_cod_responsavel_o   ||
           ' - '                       || ws_nome_responsavel_o  ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb12295#CARGO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.codigo_cargo      ||
           ' - '                       || ws_desc_cargo_o        ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb34901#DATA INICIO PREVISTA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.data_inicio_prevista,'DD/MM/YYYY')  ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb34902#DATA TERMINO PREVISTA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.data_termino_prevista,'DD/MM/YYYY')  ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb34903#DATA INICIO REALIZADA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.data_inicio_realizada,'DD/MM/YYYY')  ||
           chr(10)                                        ||
           inter_fn_buscar_tag('lb34904#DATA TERMINO REALIZADA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.data_termino_realizada,'DD/MM/YYYY')  ||
           chr(10)
        );
   end if;
end inter_tr_basi_103_log;

-- ALTER TRIGGER "INTER_TR_BASI_103_LOG" ENABLE
 

/

exec inter_pr_recompile;

