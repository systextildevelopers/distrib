create or replace trigger inter_tr_efic_090_log 
after insert or delete or update 
on efic_090 
for each row 
declare 
   ws_usuario_rede           varchar2(20); 
   ws_maquina_rede           varchar2(40); 
   ws_aplicativo             varchar2(20); 
   ws_sid                    number; 
   ws_empresa                number; 
   ws_usuario_systextil      varchar2(250); 
   ws_locale_usuario         varchar2(20); 
   v_nome_programa           varchar2(20); 


begin
-- dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 

   v_nome_programa := inter_fn_nome_programa(ws_sid);  
 
 if inserting 
 then 
    begin 
 
        insert into efic_090_log (
           tipo_ocorr,   /*0*/ 
           data_ocorr,   /*1*/ 
           hora_ocorr,   /*2*/ 
           usuario_rede,   /*3*/ 
           maquina_rede,   /*4*/ 
           aplicacao,   /*5*/ 
           usuario_sistema,   /*6*/ 
           nome_programa,   /*7*/ 
           data_parada_old,   /*8*/ 
           data_parada_new,   /*9*/ 
           maquina_maq_sub_grupo_mq_old,   /*10*/ 
           maquina_maq_sub_grupo_mq_new,   /*11*/ 
           maquina_maq_sub_sbgr_maq_old,   /*12*/ 
           maquina_maq_sub_sbgr_maq_new,   /*13*/ 
           maquina_nr_maq_old,   /*14*/ 
           maquina_nr_maq_new,   /*15*/ 
           parada_cd_area_old,   /*16*/ 
           parada_cd_area_new,   /*17*/ 
           parada_cd_parad_old,   /*18*/ 
           parada_cd_parad_new,   /*19*/ 
           turno_old,   /*20*/ 
           turno_new,   /*21*/ 
           equipe_old,   /*22*/ 
           equipe_new,   /*23*/ 
           hora_inicio_old,   /*24*/ 
           hora_inicio_new,   /*25*/ 
           minutos_parada_old,   /*26*/ 
           minutos_parada_new,   /*27*/ 
           observacao_old,   /*28*/ 
           observacao_new,   /*29*/ 
           hora_final_old,   /*30*/ 
           hora_final_new,   /*31*/ 
           minutos_turno_old,   /*32*/ 
           minutos_turno_new,   /*33*/ 
           data_final_old,   /*34*/ 
           data_final_new,   /*35*/ 
           operador_parada_old,   /*36*/ 
           operador_parada_new,   /*37*/ 
           numero_fusos_old,   /*38*/ 
           numero_fusos_new,   /*39*/ 
           velocidade_old,   /*40*/ 
           velocidade_new,   /*41*/ 
           area_resp_old,   /*42*/ 
           area_resp_new,   /*43*/ 
           intervalo_old,   /*44*/ 
           intervalo_new,   /*45*/ 
           ordem_producao_old,   /*46*/ 
           ordem_producao_new,   /*47*/ 
           codigo_rolo_old,   /*48*/ 
           codigo_rolo_new    /*49*/
        ) values (    
            'i', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           null,/*8*/
           :new.data_parada, /*9*/   
           '',/*10*/
           :new.maquina_maq_sub_grupo_mq, /*11*/   
           '',/*12*/
           :new.maquina_maq_sub_sbgr_maq, /*13*/   
           0,/*14*/
           :new.maquina_nr_maq, /*15*/   
           0,/*16*/
           :new.parada_cd_area, /*17*/   
           0,/*18*/
           :new.parada_cd_parad, /*19*/   
           0,/*20*/
           :new.turno, /*21*/   
           0,/*22*/
           :new.equipe, /*23*/   
           null,/*24*/
           :new.hora_inicio, /*25*/   
           0,/*26*/
           :new.minutos_parada, /*27*/   
           '',/*28*/
           :new.observacao, /*29*/   
           null,/*30*/
           :new.hora_final, /*31*/   
           0,/*32*/
           :new.minutos_turno, /*33*/   
           null,/*34*/
           :new.data_final, /*35*/   
           0,/*36*/
           :new.operador_parada, /*37*/   
           0,/*38*/
           :new.numero_fusos, /*39*/   
           0,/*40*/
           :new.velocidade, /*41*/   
           0,/*42*/
           :new.area_resp, /*43*/   
           0,/*44*/
           :new.intervalo, /*45*/   
           0,/*46*/
           :new.ordem_producao, /*47*/   
           0,/*48*/
           :new.codigo_rolo /*49*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into efic_090_log (
           tipo_ocorr, /*0*/   
           data_ocorr, /*1*/   
           hora_ocorr, /*2*/   
           usuario_rede, /*3*/   
           maquina_rede, /*4*/   
           aplicacao, /*5*/   
           usuario_sistema, /*6*/   
           nome_programa, /*7*/   
           data_parada_old, /*8*/   
           data_parada_new, /*9*/   
           maquina_maq_sub_grupo_mq_old, /*10*/   
           maquina_maq_sub_grupo_mq_new, /*11*/   
           maquina_maq_sub_sbgr_maq_old, /*12*/   
           maquina_maq_sub_sbgr_maq_new, /*13*/   
           maquina_nr_maq_old, /*14*/   
           maquina_nr_maq_new, /*15*/   
           parada_cd_area_old, /*16*/   
           parada_cd_area_new, /*17*/   
           parada_cd_parad_old, /*18*/   
           parada_cd_parad_new, /*19*/   
           turno_old, /*20*/   
           turno_new, /*21*/   
           equipe_old, /*22*/   
           equipe_new, /*23*/   
           hora_inicio_old, /*24*/   
           hora_inicio_new, /*25*/   
           minutos_parada_old, /*26*/   
           minutos_parada_new, /*27*/   
           observacao_old, /*28*/   
           observacao_new, /*29*/   
           hora_final_old, /*30*/   
           hora_final_new, /*31*/   
           minutos_turno_old, /*32*/   
           minutos_turno_new, /*33*/   
           data_final_old, /*34*/   
           data_final_new, /*35*/   
           operador_parada_old, /*36*/   
           operador_parada_new, /*37*/   
           numero_fusos_old, /*38*/   
           numero_fusos_new, /*39*/   
           velocidade_old, /*40*/   
           velocidade_new, /*41*/   
           area_resp_old, /*42*/   
           area_resp_new, /*43*/   
           intervalo_old, /*44*/   
           intervalo_new, /*45*/   
           ordem_producao_old, /*46*/   
           ordem_producao_new, /*47*/   
           codigo_rolo_old, /*48*/   
           codigo_rolo_new  /*49*/  
        ) values (    
            'a', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.data_parada,  /*8*/  
           :new.data_parada, /*9*/   
           :old.maquina_maq_sub_grupo_mq,  /*10*/  
           :new.maquina_maq_sub_grupo_mq, /*11*/   
           :old.maquina_maq_sub_sbgr_maq,  /*12*/  
           :new.maquina_maq_sub_sbgr_maq, /*13*/   
           :old.maquina_nr_maq,  /*14*/  
           :new.maquina_nr_maq, /*15*/   
           :old.parada_cd_area,  /*16*/  
           :new.parada_cd_area, /*17*/   
           :old.parada_cd_parad,  /*18*/  
           :new.parada_cd_parad, /*19*/   
           :old.turno,  /*20*/  
           :new.turno, /*21*/   
           :old.equipe,  /*22*/  
           :new.equipe, /*23*/   
           :old.hora_inicio,  /*24*/  
           :new.hora_inicio, /*25*/   
           :old.minutos_parada,  /*26*/  
           :new.minutos_parada, /*27*/   
           :old.observacao,  /*28*/  
           :new.observacao, /*29*/   
           :old.hora_final,  /*30*/  
           :new.hora_final, /*31*/   
           :old.minutos_turno,  /*32*/  
           :new.minutos_turno, /*33*/   
           :old.data_final,  /*34*/  
           :new.data_final, /*35*/   
           :old.operador_parada,  /*36*/  
           :new.operador_parada, /*37*/   
           :old.numero_fusos,  /*38*/  
           :new.numero_fusos, /*39*/   
           :old.velocidade,  /*40*/  
           :new.velocidade, /*41*/   
           :old.area_resp,  /*42*/  
           :new.area_resp, /*43*/   
           :old.intervalo,  /*44*/  
           :new.intervalo, /*45*/   
           :old.ordem_producao,  /*46*/  
           :new.ordem_producao, /*47*/   
           :old.codigo_rolo,  /*48*/  
           :new.codigo_rolo  /*49*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into efic_090_log (
           tipo_ocorr, /*0*/   
           data_ocorr, /*1*/   
           hora_ocorr, /*2*/   
           usuario_rede, /*3*/   
           maquina_rede, /*4*/   
           aplicacao, /*5*/   
           usuario_sistema, /*6*/   
           nome_programa, /*7*/   
           data_parada_old, /*8*/   
           data_parada_new, /*9*/   
           maquina_maq_sub_grupo_mq_old, /*10*/   
           maquina_maq_sub_grupo_mq_new, /*11*/   
           maquina_maq_sub_sbgr_maq_old, /*12*/   
           maquina_maq_sub_sbgr_maq_new, /*13*/   
           maquina_nr_maq_old, /*14*/   
           maquina_nr_maq_new, /*15*/   
           parada_cd_area_old, /*16*/   
           parada_cd_area_new, /*17*/   
           parada_cd_parad_old, /*18*/   
           parada_cd_parad_new, /*19*/   
           turno_old, /*20*/   
           turno_new, /*21*/   
           equipe_old, /*22*/   
           equipe_new, /*23*/   
           hora_inicio_old, /*24*/   
           hora_inicio_new, /*25*/   
           minutos_parada_old, /*26*/   
           minutos_parada_new, /*27*/   
           observacao_old, /*28*/   
           observacao_new, /*29*/   
           hora_final_old, /*30*/   
           hora_final_new, /*31*/   
           minutos_turno_old, /*32*/   
           minutos_turno_new, /*33*/   
           data_final_old, /*34*/   
           data_final_new, /*35*/   
           operador_parada_old, /*36*/   
           operador_parada_new, /*37*/   
           numero_fusos_old, /*38*/   
           numero_fusos_new, /*39*/   
           velocidade_old, /*40*/   
           velocidade_new, /*41*/   
           area_resp_old, /*42*/   
           area_resp_new, /*43*/   
           intervalo_old, /*44*/   
           intervalo_new, /*45*/   
           ordem_producao_old, /*46*/   
           ordem_producao_new, /*47*/   
           codigo_rolo_old, /*48*/   
           codigo_rolo_new /*49*/   
        ) values (    
            'd', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.data_parada, /*8*/   
           null, /*9*/
           :old.maquina_maq_sub_grupo_mq, /*10*/   
           '', /*11*/
           :old.maquina_maq_sub_sbgr_maq, /*12*/   
           '', /*13*/
           :old.maquina_nr_maq, /*14*/   
           0, /*15*/
           :old.parada_cd_area, /*16*/   
           0, /*17*/
           :old.parada_cd_parad, /*18*/   
           0, /*19*/
           :old.turno, /*20*/   
           0, /*21*/
           :old.equipe, /*22*/   
           0, /*23*/
           :old.hora_inicio, /*24*/   
           null, /*25*/
           :old.minutos_parada, /*26*/   
           0, /*27*/
           :old.observacao, /*28*/   
           '', /*29*/
           :old.hora_final, /*30*/   
           null, /*31*/
           :old.minutos_turno, /*32*/   
           0, /*33*/
           :old.data_final, /*34*/   
           null, /*35*/
           :old.operador_parada, /*36*/   
           0, /*37*/
           :old.numero_fusos, /*38*/   
           0, /*39*/
           :old.velocidade, /*40*/   
           0, /*41*/
           :old.area_resp, /*42*/   
           0, /*43*/
           :old.intervalo, /*44*/   
           0, /*45*/
           :old.ordem_producao, /*46*/   
           0, /*47*/
           :old.codigo_rolo, /*48*/   
           0 /*49*/
         );    
    end;    
 end if;    
end inter_tr_efic_090_log;
