
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_020_3" 
  before update of
	  tipo_reg_prod, data_insercao, valor_movto_cardex, valor_contabil_cardex,
	  tabela_origem_cardex, certificacao_qualidade, pedido_loja, gatilho_trg,
	  executa_trigger, posicao_rolo, codigo_analista_qualidade, data_inspecao_qualidade,
	  nr_pedido_ordem_dest, sequenci_periodo_dest, codigo_deposito_dest, pedido_corte,
	  pos_rolo_processo, rolo_externo, seq_rolo_externo, centro_custo_cardex,
	  montador, produto_cliente, panoacab_subgrupo, panoacab_item,
	  qtde_quilos_prod, qtde_quilos_acab, qtde_quilos_decl, qtde_rolos_real,
	  qtde_rolos_decla, data_inicio_prod, data_prod_tecel, hora_inicio,
	  hora_termino, grupo_maquina, sub_maquina, numero_maquina,
	  codigo_operador, qtde_voltas, turno_producao, rolo_estoque,
	  qualidade_rolo, codigo_deposito, pedido_venda, nr_solic_volume,
	  nr_volume, seq_item_pedido, cod_empresa_nota, nota_fiscal,
	  serie_fiscal_sai, seq_nota_fiscal, cliente_cgc9, cliente_cgc4,
	  cliente_cgc2, nota_fiscal_ent, seri_fiscal_ent, sequ_fiscal_ent,
	  numero_lote, largura, gramatura, periodo_origem,
	  nr_rolo_origem, transacao_ent, numero_ordem, seq_ordem_serv,
	  peso_bruto, tara, codigo_rolo, ordem_tecelagem,
	  ordem_producao, seq_ordem, area_producao, codigo_embalagem,
	  proforma, valor_entrada, valor_despesas, data_entrada,
	  endereco_rolo, nome_prog_020, largura_padrao, seq_ordem_tec,
	  codigo_tecelao, pre_romaneio, tipo_producao, agrupador,
	  mt_lineares, pistas, rolo_anterior, rolo_associado,
	  mt_lineares_prod, numero_tubete, fornecedor_cgc9, fornecedor_cgc4,
	  fornecedor_cgc2, caixa, kanban, area,
	  lote_acomp, pontos_qualidade, rolo_confirmado, ordem_estampa,
	  seq_ordem_estampa, qtde_quilos_cru, peso_real_fios_retilinea, desc_prod_cliente,
	  data_cardex, transacao_cardex, usuario_cardex, periodo_producao,
	  numero_rolo, numero_programa, panoacab_nivel99, panoacab_grupo
	 or delete or
         insert on pcpt_020
  for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   v_gatilho                 varchar2(100);
   v_motivo                  number(3);
   v_programa                varchar2(20);
   v_existe_ctrl_prog        number;

   v_executa_trigger      number;

begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      if deleting
      then
         -- Grava a data/hora da insercao do registro (log)
         begin
            select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20), sid
            into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo,       ws_sid
            from sys.gv_$session
            where audsid  = userenv('SESSIONID')
              and inst_id = userenv('INSTANCE')
              and rownum < 2;

            begin
               select hdoc_090.usuario,     hdoc_090.empresa
               into   ws_usuario_systextil, ws_empresa
               from hdoc_090
               where hdoc_090.sid       = ws_sid
                 and hdoc_090.instancia = userenv('INSTANCE')
                 and rownum             = 1;
            exception when no_data_found then
               ws_usuario_systextil := '';
               ws_empresa           := 0;
            end;

            begin
                select  nvl(hdoc_030.locale,'pt_BR')
                into   ws_locale_usuario
                from hdoc_030
                where hdoc_030.empresa  = ws_empresa
                  and hdoc_030.usuario  = ws_usuario_systextil;
            exception when no_data_found then
               ws_locale_usuario := 'pt_BR';
            end;

            select nvl(count(*),0)
            into v_existe_ctrl_prog
            from estq_888;
         end;

         -- Se o campo gatilho_trg for diferente de '' entao e por que tem um
         -- gatilho para executar
         if :old.gatilho_trg is not null and v_existe_ctrl_prog > 0
         then
             v_gatilho := substr(:old.gatilho_trg,1,14);

             if v_gatilho = 'MOTIVOEXCLUSAO'
             then
                v_motivo   := substr(:old.gatilho_trg, 16,3);
                v_programa := substr(:old.gatilho_trg, 19,20);

                insert into estq_889 (
                   codigo_volume,             periodo_producao,
                   nivel_produto,             grupo_produto,
                   subgrupo_produto,          item_produto,

                   turno_producao,            numero_lote,
                   codigo_deposito,           data_prod_tecel,
                   data_entrada,              area_producao,

                   ordem_producao,            grupo_maquina,
                   sub_maquina,               numero_maquina,
                   nome_programa,             codigo_motivo,
                   peso_bruto,                tara,

                   usuario_systextil,         usuario_rede,
                   maquina_rede,              aplicacao,
                   data_exclusao,              hora_exclusao
                  ) values (
                   :old.codigo_rolo,          :old.periodo_producao,
                   :old.panoacab_nivel99,     :old.panoacab_grupo,
                   :old.panoacab_subgrupo,    :old.panoacab_item,

                   :old.turno_producao,       :old.numero_lote,
                   :old.codigo_deposito,      :old.data_prod_tecel,
                   :old.data_entrada,         :old.area_producao,

                   :old.ordem_producao,       :old.grupo_maquina,
                   :old.sub_maquina,          :old.numero_maquina,
                   v_programa,                v_motivo,
                   :old.peso_bruto,           :old.tara,

                   ws_usuario_systextil,      ws_usuario_rede,
                   ws_maquina_rede,           ws_aplicativo,
                   sysdate,                   sysdate
                  );
             end if;
         end if;
      end if;

   end if;
end inter_tr_pcpt_020_3;

-- ALTER TRIGGER "INTER_TR_PCPT_020_3" ENABLE
 

/

exec inter_pr_recompile;

