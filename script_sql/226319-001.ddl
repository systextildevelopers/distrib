alter table PEDI_100 add
("VALOR_LIBERADO" NUMBER(15,2) default 0.0);


alter table "PEDI_100" add
("PEDIDO_MAE" VARCHAR2(1) DEFAULT 'N');   

alter table "PEDI_100" add
("PEDIDO_ORIGINAL" NUMBER(9,0));   

alter table "PEDI_110" add
("QTD_ORIGINAL_PEDIDO" NUMBER(15,3));   

alter table "PEDI_110" add
("PEDIDO_ORIGINAL" NUMBER(9,0));   

alter table "PEDI_110" add
("SEQ_ITEM_ORIGINAL" NUMBER(3,0));
