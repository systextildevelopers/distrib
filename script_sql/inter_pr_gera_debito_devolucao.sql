
  CREATE OR REPLACE PROCEDURE "INTER_PR_GERA_DEBITO_DEVOLUCAO" (p_valor_baixa    in number,
                                                            p_cgc9           in number,
                                                            p_cgc4           in number,
                                                            p_cgc2           in number,
                                                            p_cod_empresa    in number,
                                                            p_orcamento      in number,
                                                            p_observacao_dev in varchar2 ) is

   w_nivel_dev      varchar2(1);
   w_grupo_dev      varchar2(5);
   w_sub_dev        varchar2(3);
   w_item_dev       varchar2(6);
   w_documento_dev  loja_060.documento%type;
   w_deposito_dev   loja_061.deposito%type;
   w_lote_dev       loja_061.lote%type;
   w_valor_dev      loja_061.valor_unit%type;
   w_sequencia_dev  loja_061.sequencia%type;
   w_seq_entrada    loja_061.seq_saida%type;
   w_valor_baixa    loja_075.valor_parc%type;

begin

    /**
    * SS:70024/012 - Tiago.H
    *
    * Essa procedure gera uma saida na tabela loja_061 (Processo de devolucao da fatal),
    * muito cuidado ao mecher no SQL abaixo, existem muitas situacoes especificas que
    * podem dar problema, teste todas as opcoes possiveis.
    *
    * Procedure chamada dos programas:
    *   . inter_tr_loja_075
    *   . crec_f572
    */

    w_valor_baixa := p_valor_baixa;

    WHILE w_valor_baixa > 0
    LOOP
        /* BUSCA ENTRADAS COM SALDO PARA GERAR AS SAIDAS E ABATER O VALOR */

        BEGIN

          SELECT entradas.nivel,           entradas.grupo,
                 entradas.subgrupo,        entradas.item,
                 entradas.documento,       entradas.deposito,
                 entradas.lote,            entradas.sequencia,
                 nvl(entradas.total,0) - nvl(saidas.total,0)

          INTO   w_nivel_dev,             w_grupo_dev,
                 w_sub_dev,               w_item_dev,
                 w_documento_dev,         w_deposito_dev,
                 w_lote_dev,              w_seq_entrada,
                 w_valor_dev

          FROM

          /* ENTRADAS */
          (SELECT loja_061.documento, loja_061.nivel,
                  loja_061.grupo,     loja_061.subgrupo,
                  loja_061.item,      loja_061.deposito,
                  loja_061.lote,      loja_061.sequencia,
                  nvl(sum(loja_061.quantidade * loja_061.valor_unit),0) as total
           FROM loja_061, loja_060
           where loja_060.documento             = loja_061.documento
             and loja_060.cnpj9                 = loja_061.cnpj9
             and loja_060.cnpj4                 = loja_061.cnpj4
             and loja_060.cnpj2                 = loja_061.cnpj2
             and loja_060.consignacao_devolucao = 1
             and loja_061.cnpj9                 = p_cgc9
             and loja_061.cnpj4                 = p_cgc4
             and loja_061.cnpj2                 = p_cgc2
             and loja_061.entrada_saida         = 'E'
             and loja_061.situacao_item         = 1
             and loja_061.codigo_empresa        = p_cod_empresa
           GROUP BY loja_061.documento, loja_061.nivel,
                    loja_061.grupo,     loja_061.subgrupo,
                    loja_061.item,      loja_061.deposito,
                    loja_061.lote,      loja_061.sequencia) entradas,

             /* SAIDAS */
           (SELECT loja_061.documento, loja_061.nivel,
                   loja_061.grupo,     loja_061.subgrupo,
                   loja_061.item,      loja_061.deposito,
                   loja_061.lote,      loja_061.seq_saida,
                   nvl(sum(loja_061.quantidade * loja_061.valor_unit),0) as total
            FROM loja_061, loja_060
            where loja_060.documento             = loja_061.documento
              and loja_060.cnpj9                 = loja_061.cnpj9
              and loja_060.cnpj4                 = loja_061.cnpj4
              and loja_060.cnpj2                 = loja_061.cnpj2
              and loja_060.consignacao_devolucao = 1
              and loja_061.cnpj9                 = p_cgc9
              and loja_061.cnpj4                 = p_cgc4
              and loja_061.cnpj2                 = p_cgc2
              and loja_061.entrada_saida         = 'S'
              and loja_061.situacao_item         = 1
              and loja_061.codigo_empresa = p_cod_empresa
            GROUP BY loja_061.documento, loja_061.nivel,
                     loja_061.grupo,     loja_061.subgrupo,
                     loja_061.item,      loja_061.deposito,
                     loja_061.lote,      loja_061.seq_saida ) saidas
            WHERE nvl(entradas.total,0) > nvl(saidas.total,0)
              and entradas.nivel        = saidas.nivel     (+)
              and entradas.grupo        = saidas.grupo     (+)
              and entradas.subgrupo     = saidas.subgrupo  (+)
              and entradas.item         = saidas.item      (+)
              and entradas.lote         = saidas.lote      (+)
              and entradas.deposito     = saidas.deposito  (+)
              and entradas.sequencia    = saidas.seq_saida (+)
              and rownum                = 1
           order by nvl(entradas.total,0) - nvl(saidas.total,0) desc;
         EXCEPTION
             WHEN no_data_found THEN
               raise_application_error(-20000, 'Nao encontrou nenhum lancamento de entrada de devolucao para o cliente.');
               /*p_valor_baixa := 0;
               continue;*/
             WHEN others THEN
               raise_application_error(-20000, 'Nao encontrou nenhum lancamento de entrada de devolucao para o cliente. Erro: '||SQLERRM);
         END;

         if w_valor_dev > w_valor_baixa
         then
            w_valor_dev := w_valor_baixa;
         end if;

         w_sequencia_dev := 0;

         BEGIN
           select max(sequencia)
           into w_sequencia_dev
           from loja_061
           where loja_061.cnpj9     = p_cgc9
             and loja_061.cnpj4     = p_cgc4
             and loja_061.cnpj2     = p_cgc2
             and loja_061.documento = w_documento_dev;
         EXCEPTION
            WHEN OTHERS THEN
               w_sequencia_dev := 0;
         END;

         if w_sequencia_dev is null
         then
            w_sequencia_dev := 0;
         end if;

         w_sequencia_dev := w_sequencia_dev + 1;

         BEGIN
            INSERT into loja_061
            (
               documento        ,   cnpj9,
               cnpj4            ,   cnpj2,
               sequencia        ,   nivel,
               grupo            ,   subgrupo,
               item             ,   deposito,
               lote             ,   data_movto,
               observacao       ,   codigo_empresa,
               quantidade       ,   valor_unit,
               entrada_saida    ,   situacao_item,
               nr_orcamento_loja,   seq_saida
            )
            VALUES
            (
               w_documento_dev       ,   p_cgc9,
               p_cgc4                ,   p_cgc2,
               w_sequencia_dev       ,   w_nivel_dev,
               w_grupo_dev           ,   w_sub_dev,
               w_item_dev            ,   w_deposito_dev,
               w_lote_dev            ,   sysdate,
               p_observacao_dev      ,   p_cod_empresa,
               1                     ,   w_valor_dev,
               'S'                   ,   1,
               p_orcamento           ,   w_seq_entrada
            );
         EXCEPTION
            WHEN others THEN
               raise_application_error(-20000, 'Nao inseriu LOJA_061(Devolucoes). Erro: '||SQLERRM);
         END;
         w_valor_baixa := w_valor_baixa - w_valor_dev;

    END LOOP;
end inter_pr_gera_debito_devolucao;
 

/

exec inter_pr_recompile;

