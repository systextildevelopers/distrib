CREATE OR REPLACE VIEW PCPT_020_025
(codigo_rolo, periodo_producao, turno_producao, numero_lote, panoacab_nivel99, panoacab_grupo, panoacab_subgrupo, panoacab_item, codigo_deposito, qtde_quilos_acab, peso_bruto, tara, mt_lineares_prod, pedido_venda, seq_item_pedido, nr_volume, data_prod_tecel, area_producao, ordem_producao, pre_romaneio, grupo_maquina, sub_maquina, numero_maquina, largura, nr_rolo_origem, gramatura, transacao_ent, data_entrada, rolo_estoque, nota_fiscal, serie_fiscal_sai, numero_ob, lote_acomp, certificacao_qualidade, endereco_rolo, cod_nuance, pontuacao_qualidade, emenda, qtde_rolos_real, qualidade_rolo, codigo_operador, sequencia_corte, numero_lacre, ordem_tingimento_rolo, numero_embarque, sequ_fiscal_ent, procedencia, ds_procedencia, unidade_medida, codigo_barras_antigo, seq_rolo_prev_romaneio, fornecedor_cgc9, fornecedor_cgc4, fornecedor_cgc2, nota_fiscal_ent, seri_fiscal_ent, ordem_benefic_modular, grupo_atendimento)
AS
SELECT
 pcpt_020.codigo_rolo            , pcpt_020.periodo_producao          ,
 pcpt_020.turno_producao         , pcpt_020.numero_lote               ,
 pcpt_020.panoacab_nivel99       , pcpt_020.panoacab_grupo            ,
 pcpt_020.panoacab_subgrupo      , pcpt_020.panoacab_item             ,
 pcpt_020.codigo_deposito        , pcpt_020.qtde_quilos_acab          ,
 pcpt_020.peso_bruto             , pcpt_020.tara                      ,
 pcpt_020.mt_lineares_prod       , pcpt_020.pedido_venda              ,
 pcpt_020.seq_item_pedido        , pcpt_020.nr_volume                 ,
 pcpt_020.data_prod_tecel        , pcpt_020.area_producao             ,
 pcpt_020.ordem_producao         , pcpt_020.pre_romaneio              ,
 pcpt_020.grupo_maquina          , pcpt_020.sub_maquina               ,
 pcpt_020.numero_maquina         , pcpt_020.largura                   ,
 pcpt_020.nr_rolo_origem         , pcpt_020.gramatura                 ,
 pcpt_020.transacao_ent          , pcpt_020.data_entrada              ,
 pcpt_020.rolo_estoque           , pcpt_020.nota_fiscal               ,
 pcpt_020.serie_fiscal_sai       , pcpt025.ordem_producao             ,
 pcpt_020.lote_acomp             , pcpt_020.certificacao_qualidade    ,
 pcpt_020.endereco_rolo          , pcpt_021.cod_nuance                ,
 pcpt_021.pontuacao_qualidade    , pcpt_021.emenda                    ,
 pcpt_020.qtde_rolos_real        , pcpt_020.qualidade_rolo            ,
 pcpt_020.codigo_operador        , pcpt_021.sequencia_corte           ,
 pcpt_021.numero_lacre           , DECODE(pcpt_020.area_producao,2,pcpb_010.ordem_tingimento,0),
 rcnb_060a.numero_embarque       , pcpt_020.sequ_fiscal_ent           ,
 pcpt_021.procedencia            , pcpf_019.descricao                 ,
 basi_030.unidade_medida         , oper183.codigo_barras_antigo       ,
 pcpt_021.seq_rolo_prev_romaneio , pcpt_020.fornecedor_cgc9           ,
 pcpt_020.fornecedor_cgc4        , pcpt_020.fornecedor_cgc2           ,
 pcpt_020.nota_fiscal_ent        , pcpt_020.seri_fiscal_ent           ,
 pcpt_010.ordem_benefic_modular  , pcpt_021.grupo_atendimento
 from (select pcpt_025.codigo_rolo,
              pcpt_025.ordem_producao
       from pcpt_025
       where pcpt_025.area_ordem <> 1)
    pcpt025, pcpt_020, pcpt_021, pcpb_010, pcpt_010, basi_030, pcpf_019,
    (select rcnb_060.grupo_estrutura          as codigo_rolo,
            nvl(rcnb_060.campo_str_01,' ')   as numero_embarque
     from rcnb_060
     where rcnb_060.tipo_registro = 517
       and rcnb_060.nr_solicitacao = 315
       and rcnb_060.grupo_estrutura > 0
     group by rcnb_060.grupo_estrutura,nvl(rcnb_060.campo_str_01,' ')) rcnb_060a,
    (select oper_183.codigo_barras_antigo, oper_183.numero_vol_systextil
       from oper_183
      where oper_183.nivel_systextil in ('2', '4')) oper183
where pcpt_020.codigo_rolo            = pcpt025.codigo_rolo(+)
  and pcpt_020.codigo_rolo            = pcpt_021.codigo_rolo(+)
  and pcpt_020.codigo_rolo            > 0
  and pcpt_020.ordem_producao         = pcpb_010.ordem_producao (+)
  and pcpt_020.ordem_producao         = pcpt_010.ordem_tecelagem (+)
  and pcpt_020.codigo_rolo            = rcnb_060a.codigo_rolo(+)
  and pcpt_020.panoacab_nivel99       = basi_030.nivel_estrutura
  and pcpt_020.panoacab_grupo         = basi_030.referencia
  and pcpt_021.procedencia            = pcpf_019.codigo (+)
  and pcpt_020.codigo_rolo            = oper183.numero_vol_systextil (+);
/
