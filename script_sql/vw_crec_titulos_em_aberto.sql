CREATE OR REPLACE VIEW  VW_CREC_TITULOS_EM_ABERTO as
    select
        fatu_070.codigo_empresa
        ,fatu_070.cli_dup_cgc_cli9
        ,fatu_070.cli_dup_cgc_cli4
        ,fatu_070.cli_dup_cgc_cli2
        ,pedi_010.nome_cliente
        ,pedi_010.cod_cliente
        ,'(' || basi_160.ddd ||') ' || pedi_010.telefone_cliente as contato_cliente
        ,pedi_010.e_mail
        ,fatu_070.cod_transacao
        ,estq_005.descricao as desc_transacao
        ,cpag_040.tipo_titulo                   --cpag_010.tipo_titulo
        ,cpag_040.descricao as nome_tipo
        ,fatu_070.num_duplicata
        ,fatu_070.seq_duplicatas
        ,fatu_070.data_emissao
        ,fatu_070.data_prorrogacao as data_vencimento
        ,fatu_070.data_prorrogacao as data_vencimento_real
        ,fatu_070.data_venc_duplic as data_vencimento_original
        ,fatu_070.data_prorrogacao
        ,fatu_070.moeda_titulo
        ,nvl(basi_270.valor_moeda, 1) as cotacao_moeda
        ,fatu_070.valor_duplicata as valor_titulo
        ,fatu_070.saldo_duplicata as saldo_titulo
        ,fatu_070.posicao_duplic
        ,crec_010.descricao as posicao
        ,fatu_070.num_contabil
        ,fatu_070.numero_remessa
        ,fatu_070.nr_titulo_banco
        ,fatu_070.numero_bordero
        ,fatu_070.cd_centro_custo
        ,fatu_070.pedido_venda
        ,fatu_070.data_emissao as data_transacao
        ,fatu_070.codigo_contabil
        ,fatu_070.cod_historico as historico
        ,case when fatu_070.previsao = 1 then 'SIM' else 'NÃO' end as previsto
        ,fatu_070.cod_canc_duplic
        ,basi_160.cidade as municipio
        ,basi_160.estado as estado
        ,pedi_020.cod_rep_cliente ||' - '|| pedi_020.nome_rep_cliente as nome_rep_cliente
        ,'(' || basi_160.ddd ||') ' || pedi_020.fone_rep_cliente as contato_representante
        ,pedi_020.e_mail as email_representante

    from
        fatu_070
        ,pedi_010
        ,basi_160
        ,estq_005
        ,cpag_040
        --,cpag_010
        ,basi_270
        ,pedi_020
        ,crec_010

    where
            fatu_070.cli_dup_cgc_cli9    = pedi_010.cgc_9
        and fatu_070.cli_dup_cgc_cli4    = pedi_010.cgc_4
        and fatu_070.cli_dup_cgc_cli2    = pedi_010.cgc_2
        and pedi_010.cod_cidade          = basi_160.cod_cidade (+)
        and pedi_010.cgc_9               = fatu_070.cli_dup_cgc_cli9
        and pedi_010.cgc_4               = fatu_070.cli_dup_cgc_cli4
        and pedi_010.cgc_2               = fatu_070.cli_dup_cgc_cli2
        and fatu_070.cod_transacao       = estq_005.codigo_transacao
        -- and fatu_070.cli_dup_cgc_cli9 = cpag_010.cgc_9 (+)
        -- and fatu_070.cli_dup_cgc_cli4 = cpag_010.cgc_4 (+)
        -- and fatu_070.cli_dup_cgc_cli2 = cpag_010.cgc_2 (+)
        -- and fatu_070.tipo_titulo      = cpag_010.tipo_titulo
        and fatu_070.tipo_titulo         = cpag_040.tipo_titulo
        and fatu_070.moeda_titulo        = basi_270.codigo_moeda  (+)
        and fatu_070.data_emissao        = basi_270.data_moeda    (+)
        and fatu_070.cod_rep_cliente     = pedi_020.cod_rep_cliente
        and fatu_070.posicao_duplic      = crec_010.posicao_duplic
        and fatu_070.saldo_duplicata     > 0
/
