
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_110_2" 
before update of situacao_fatu_it, cod_cancelamento
on pedi_110
for each row
declare
  v_proxima_seq        i_saldos_cancelados.id_registro%type;
  v_cliente9           pedi_100.cli_ped_cgc_cli9%type;
  v_cliente4           pedi_100.cli_ped_cgc_cli4%type;
  v_cliente2           pedi_100.cli_ped_cgc_cli2%type;
  v_pedido_compra      i_pedi_110.guarda_ped_compra%type;
  v_sequencia_compra   i_pedi_110.agrupador_producao%type;

  v_cod_cancelamento   number(3);
  v_descr_cancelamento varchar2(120);

  ws_usuario_rede      varchar2(20);
  ws_maquina_rede      varchar2(40);
  ws_aplicativo        varchar2(20);
  ws_sid               number(9);
  ws_empresa           number(3);
  ws_usuario_systextil varchar2(250);
  ws_locale_usuario    varchar2(5);

  v_executa_trigger    number(1);

  v_e_mail_destino     oper_130.e_mail_destino%type;

begin

  if inserting
  then
     if :new.executa_trigger = 1
     then v_executa_trigger := 1;
     else v_executa_trigger := 0;
     end if;
  end if;

  if updating
  then
    if :old.executa_trigger = 1 or :new.executa_trigger = 1
    then v_executa_trigger := 1;
    else v_executa_trigger := 0;
    end if;
  end if;

  if deleting
  then
     if :old.executa_trigger = 1
     then v_executa_trigger := 1;
     else v_executa_trigger := 0;
     end if;
  end if;

  begin
    select oper_130.e_mail_destino
    into   v_e_mail_destino
    from oper_130
    where oper_130.nome_programa = 'inte_f984';
  exception
    when no_data_found then
      v_e_mail_destino := '';
  end;

  if v_executa_trigger = 0 and
     trim(v_e_mail_destino) is not null
  then

    -- Dados do usuario logado
    inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                            ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

    if :old.situacao_fatu_it <> :new.situacao_fatu_it or
       :old.cod_cancelamento <> :new.cod_cancelamento
    then
      if (:new.situacao_fatu_it = 1  or
          :new.cod_cancelamento > 0) and
         (:new.qtde_pedida > :new.qtde_faturada)
      then
        -- Proximo valor
        begin
          select nvl(Max(id_registro),0)+1
          into   v_proxima_seq
          from i_saldos_cancelados;
        exception
          when no_data_found then
            v_proxima_seq := 1;
        end;

        -- Cnpj do cliente
        begin
          select i_pedi_100.cliente9, i_pedi_100.cliente4,
                 i_pedi_100.cliente2
          into   v_cliente9,          v_cliente4,
                 v_cliente2
          from i_pedi_100
          where i_pedi_100.pedido_venda = :new.pedido_venda;
        exception
          when no_data_found then
             v_cliente9 := 0;
             v_cliente4 := 0;
             v_cliente2 := 0;
        end;
        -- Dados do pedido
        begin
          select i_pedi_110.guarda_ped_compra, i_pedi_110.agrupador_producao
          into   v_pedido_compra,              v_sequencia_compra
          from i_pedi_110
          where i_pedi_110.pedido_venda    = :new.pedido_venda
            and i_pedi_110.seq_item_pedido = :new.seq_item_pedido;
        exception
          when no_data_found then
            v_pedido_compra := 0;
            v_sequencia_compra := 0;
        end;

        v_cod_cancelamento := :new.cod_cancelamento;

        if :new.cod_cancelamento = 0
        then v_descr_cancelamento := 'LIQUIDACAO PEDIDO';
        else
           begin
             select pedi_140.desc_canc_pedido
             into   v_descr_cancelamento
             from pedi_140
             where pedi_140.cod_canc_pedido = :new.cod_cancelamento;
           exception
             when no_data_found then
               v_descr_cancelamento := ' ';
           end;
        end if;

        begin
          update i_saldos_cancelados
          set i_saldos_cancelados.qtde_cancelada      = i_saldos_cancelados.qtde_cancelada + :new.qtde_pedida - :new.qtde_faturada,
              i_saldos_cancelados.data_cancelamento   = sysdate,
              i_saldos_cancelados.usuario_atualizacao = ws_usuario_systextil,
              i_saldos_cancelados.codigo_cancelamento = v_cod_cancelamento,
              i_saldos_cancelados.descr_cancelamento  = v_descr_cancelamento
          where i_saldos_cancelados.cliente9         = v_cliente9
            and i_saldos_cancelados.cliente4         = v_cliente4
            and i_saldos_cancelados.cliente2         = v_cliente2
            and i_saldos_cancelados.pedido_compra    = v_pedido_compra
            and i_saldos_cancelados.sequencia_compra = v_sequencia_compra;

          if SQL%notfound
          then
            insert into i_saldos_cancelados
             (id_registro,      tipo_atualizacao,
              flag_exportacao,  data_exportacao,
              flag_importacao,  data_importacao,
              cliente9,         cliente4,
              cliente2,         pedido_compra,
              sequencia_compra, data_cancelamento,
              qtde_cancelada,   usuario_atualizacao,
              codigo_cancelamento, descr_cancelamento)
            values
              (v_proxima_seq,      'I',
               0,                  sysdate,
               3,                  null,
               v_cliente9,         v_cliente4,
               v_cliente2,         v_pedido_compra,
               v_sequencia_compra, sysdate,
               :new.qtde_pedida - :new.qtde_faturada,  ws_usuario_systextil,
               v_cod_cancelamento, v_descr_cancelamento);
          end if;
        exception
          when others then
            Raise_application_error(-20000, 'ATENCAO! Ocorreram problemas ao inserir os dados na i_saldos_cancelados.');
        end;
      end if;
    end if;
  end if;
end inter_tr_pedi_110_2;

-- ALTER TRIGGER "INTER_TR_PEDI_110_2" ENABLE
 

/

exec inter_pr_recompile;

