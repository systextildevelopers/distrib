INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'supr_f027',     0,
	1,			     'Cadastro de rota/cep para a BrasPress');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'supr_f027',   'supr_menu', 
	1,             1, 
	'S',           'S', 
	'S',           'S');
	
INSERT INTO hdoc_033
(	usu_prg_cdusu,  usu_prg_empr_usu, 
	programa,       nome_menu, 
	item_menu,      ordem_menu, 
	incluir,        modificar, 
	excluir,        procurar)
VALUES
(	'TREINAMENTO',  1, 
	'supr_f027',    'supr_menu', 
	1,              1, 
	'S',            'S', 
	'S',            'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Cadastro de rota/cep para a BrasPress'
 WHERE hdoc_036.codigo_programa = 'supr_f027'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;

/
EXEC inter_pr_recompile;
