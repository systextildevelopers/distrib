
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_001_LOG" 
   after insert or delete or update
       of CODIGO_PROJETO,          DESCRICAO_PROJETO,
          NIVEL_PRODUTO,           GRUPO_PRODUTO,
          CNPJ_CLIENTE9,           CNPJ_CLIENTE4,
          CNPJ_CLIENTE2,           PRODUTO_CLIENTE,
          CODIGO_RESPONSAVEL,      SITUACAO_PROJETO,
          DATA_CADASTRO,           DATA_INICIO_PROJETO
   on basi_001
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_nome_cliente_o         varchar2(60);
   ws_nome_cliente_n         varchar2(60);
   ws_nome_responsavel_o     varchar2(60);
   ws_nome_responsavel_n     varchar2(60);
   ws_situacao_projeto_o     varchar2(60);
   ws_situacao_projeto_n     varchar2(60);

   long_aux                  varchar2(2000);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      begin
         select nvl(pedi_010.nome_cliente,' ')
         into ws_nome_cliente_n
         from pedi_010
         where pedi_010.cgc_9 = :new.cnpj_cliente9
           and pedi_010.cgc_4 = :new.cnpj_cliente4
           and pedi_010.cgc_2 = :new.cnpj_cliente2;
      exception when no_data_found then
         ws_nome_cliente_n  := ' ';
      end;

      begin
         select nvl(efic_050.nome,' ')
         into ws_nome_responsavel_n
         from efic_050
         where efic_050.cod_empresa     = ws_empresa
           and efic_050.cod_funcionario = :new.codigo_responsavel;
      exception when no_data_found then
         ws_nome_responsavel_n  := ' ';
      end;

      if :new.situacao_projeto = 0 then ws_situacao_projeto_n := inter_fn_buscar_tag('lb29598#EM DESENVOLVIMENTO',ws_locale_usuario,ws_usuario_systextil); end if;
      if :new.situacao_projeto = 1 then ws_situacao_projeto_n := inter_fn_buscar_tag('lb29597#DESENVOLVIDO',ws_locale_usuario,ws_usuario_systextil); end if;
      if :new.situacao_projeto = 2 then ws_situacao_projeto_n := inter_fn_buscar_tag('lb20317#APROVADO',ws_locale_usuario,ws_usuario_systextil); end if;
      if :new.situacao_projeto = 3 then ws_situacao_projeto_n := inter_fn_buscar_tag('lb23367#REJEITADO',ws_locale_usuario,ws_usuario_systextil); end if;

      begin
         INSERT INTO hist_100 (
            tabela,             operacao,
            data_ocorr,         aplicacao,
            usuario_rede,       maquina_rede,
            str01,              num05,
            long01
         ) VALUES (
            'BASI_001',         'I',
            sysdate,            ws_aplicativo,
            ws_usuario_rede,    ws_maquina_rede,
            :new.codigo_projeto, 0,
             '               '||
             inter_fn_buscar_tag('lb34974#PROJETO DE DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                                     chr(10)               ||
                                     chr(10)               ||
            inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.codigo_projeto    ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb30896#DESCRICAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.descricao_projeto ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.nivel_produto     || '.'
                                 || :new.grupo_produto     ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb31254#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || to_char(:new.cnpj_cliente9,'000000000')     || '/' ||
                                    to_char(:new.cnpj_cliente4,'0000')     || '-' ||
                                    to_char(:new.cnpj_cliente2,'00')     ||
             ' - ' || ws_nome_cliente_n                     ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb11029#PROD. CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.produto_cliente   ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb31305#RESPONSaVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.codigo_responsavel ||
            ' - ' || ws_nome_responsavel_n                 ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb29847#SIT.PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || ws_situacao_projeto_n  ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb00610#DATA CADASTRO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || to_char(:new.data_cadastro,'DD/MM/YYYY')  ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb33002#DATA INICIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || to_char(:new.data_inicio_projeto,'DD/MM/YYYY') ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb40890#DATA APROVADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || to_char(:new.data_aprovado,'DD/MM/YYYY') ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb40891#DATA REPROVADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || to_char(:new.data_reprovado,'DD/MM/YYYY')
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(001-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   if updating
   then
      long_aux := long_aux ||
                 '                 '||
                 inter_fn_buscar_tag('lb34974#PROJETO DE DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                  chr(10)                    ||
                  chr(10);

      if  :old.descricao_projeto <> :new.descricao_projeto
      then
          long_aux := long_aux ||
            inter_fn_buscar_tag('lb30896#DESCRICAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                    || :old.descricao_projeto   || ' -> '
                                    || :new.descricao_projeto
                                    || chr(10);
      end if;

      if  (:old.nivel_produto <> :new.nivel_produto) or
          (:old.grupo_produto <> :new.grupo_produto)
      then
          long_aux := long_aux ||
            inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                    || :old.nivel_produto || '.'
                                    || :old.grupo_produto || ' -> '
                                    || :new.nivel_produto || '.'
                                    || :new.grupo_produto
                                    || chr(10);
      end if;

      if :old.cnpj_cliente9 <> :new.cnpj_cliente9 or
         :old.cnpj_cliente4 <> :new.cnpj_cliente4 or
         :old.cnpj_cliente2 <> :new.cnpj_cliente2
      then
         begin
             select nvl(pedi_010.nome_cliente,' ')
             into ws_nome_cliente_o
             from pedi_010
              where pedi_010.cgc_9 = :old.cnpj_cliente9
              and pedi_010.cgc_4 = :old.cnpj_cliente4
               and pedi_010.cgc_2 = :old.cnpj_cliente2;
         exception when no_data_found then
             ws_nome_cliente_o := ' ';
         end;

         begin
           select nvl(pedi_010.nome_cliente,' ')
           into ws_nome_cliente_n
           from pedi_010
            where pedi_010.cgc_9 = :new.cnpj_cliente9
            and pedi_010.cgc_4 = :new.cnpj_cliente4
             and pedi_010.cgc_2 = :new.cnpj_cliente2;
         exception when no_data_found then
             ws_nome_cliente_o := ' ';
         end;

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb31254#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                   || to_char(:old.cnpj_cliente9,'000000000')  || '/'
                                   || to_char(:old.cnpj_cliente4,'0000')       || '-'
                                   || to_char(:old.cnpj_cliente2,'00')         || ' - '
                                   || ws_nome_cliente_o                           || ' -> ' ||
                                   chr(10);
         long_aux := long_aux     ||
             '                : ' || to_char(:new.cnpj_cliente9,'000000000')  || '/'
                                  || to_char(:new.cnpj_cliente4,'0000')       || '-'
                                  || to_char(:new.cnpj_cliente2,'00')         || ' - '
                                  || ws_nome_cliente_n
                                  || chr(10);
      end if;

      if :old.produto_cliente <> :new.produto_cliente
      then
         long_aux := long_aux ||
            inter_fn_buscar_tag('lb11029#PROD. CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                   || :old.produto_cliente   || ' -> '
                                   || :new.produto_cliente
                                   || chr(10);
      end if;

      if :old.codigo_responsavel <> :new.codigo_responsavel
      then
         begin
            select nvl(efic_050.nome,' ')
            into ws_nome_responsavel_o
            from efic_050
            where efic_050.cod_empresa     = ws_empresa
              and efic_050.cod_funcionario = :old.codigo_responsavel;
         exception when no_data_found then
            ws_nome_responsavel_o := ' ';
         end;

         begin
            select nvl(efic_050.nome,' ')
            into ws_nome_responsavel_n
            from efic_050
            where efic_050.cod_empresa     = ws_empresa
              and efic_050.cod_funcionario = :new.codigo_responsavel;
         exception when no_data_found then
            ws_nome_responsavel_n := ' ';
         end;

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb31305#RESPONSaVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                   || :old.codigo_responsavel
                                   || ' - ' || ws_nome_responsavel_o || ' -> '
                                   || :new.codigo_responsavel
                                   || ' - ' || ws_nome_responsavel_n
                                   || chr(10);
      end if;

      if :old.situacao_projeto <> :new.situacao_projeto
      then
         if :new.situacao_projeto = 0 then ws_situacao_projeto_n := inter_fn_buscar_tag('lb29598#EM DESENVOLVIMENTO',ws_locale_usuario,ws_usuario_systextil); end if;
         if :new.situacao_projeto = 1 then ws_situacao_projeto_n := inter_fn_buscar_tag('lb29597#DESENVOLVIDO',ws_locale_usuario,ws_usuario_systextil); end if;
         if :new.situacao_projeto = 2 then ws_situacao_projeto_n := inter_fn_buscar_tag('lb20317#APROVADO',ws_locale_usuario,ws_usuario_systextil); end if;
         if :new.situacao_projeto = 3 then ws_situacao_projeto_n := inter_fn_buscar_tag('lb23367#REJEITADO',ws_locale_usuario,ws_usuario_systextil); end if;

         if :old.situacao_projeto = 0 then ws_situacao_projeto_o := inter_fn_buscar_tag('lb29598#EM DESENVOLVIMENTO',ws_locale_usuario,ws_usuario_systextil); end if;
         if :old.situacao_projeto = 1 then ws_situacao_projeto_o := inter_fn_buscar_tag('lb29597#DESENVOLVIDO',ws_locale_usuario,ws_usuario_systextil); end if;
         if :old.situacao_projeto = 2 then ws_situacao_projeto_o := inter_fn_buscar_tag('lb20317#APROVADO',ws_locale_usuario,ws_usuario_systextil); end if;
         if :old.situacao_projeto = 3 then ws_situacao_projeto_o := inter_fn_buscar_tag('lb23367#REJEITADO',ws_locale_usuario,ws_usuario_systextil); end if;

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb29847#SIT.PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                    || ws_situacao_projeto_o || ' -> '
                                    || ws_situacao_projeto_n
                                    || chr(10);
      end if;

      if :old.data_inicio_projeto <> :new.data_inicio_projeto
      then
          long_aux := long_aux ||
            inter_fn_buscar_tag('lb33002#DATA INICIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                    || to_char(:old.data_inicio_projeto,'DD/MM/YYYY') || ' -> '
                                    || to_char(:new.data_inicio_projeto,'DD/MM/YYYY')
                                    || chr(10);
      end if;

      if  :old.data_aprovado <> :new.data_aprovado                       or
         (:old.data_aprovado is null and :new.data_aprovado is not null) or
         (:new.data_aprovado is null and :old.data_aprovado is not null)
      then
         long_aux := long_aux ||
            inter_fn_buscar_tag('lb40890#DATA APROVADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                   || to_char(:old.data_aprovado,'DD/MM/YYYY') || ' -> '
                                   || to_char(:new.data_aprovado,'DD/MM/YYYY')
                                   || chr(10);
      end if;

      if :old.data_reprovado <> :new.data_reprovado                        or
         (:old.data_reprovado is null and :new.data_reprovado is not null) or
         (:new.data_reprovado is null and :old.data_reprovado is not null)
      then
         long_aux := long_aux ||
            inter_fn_buscar_tag('lb40891#DATA REPROVADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                   || to_char(:old.data_reprovado,'DD/MM/YYYY') || ' -> '
                                   || to_char(:new.data_reprovado,'DD/MM/YYYY')
                                   || chr(10);
      end if;

      begin
         INSERT INTO hist_100 (
            tabela,               operacao,
            data_ocorr,           aplicacao,
            usuario_rede,         maquina_rede,
            str01,                num05,
            long01
         ) VALUES (
           'BASI_001',           'A',
            sysdate,              ws_aplicativo,
            ws_usuario_rede,      ws_maquina_rede,
            :new.codigo_projeto,  0,
            long_aux
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(001-2)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));

      end;
   end if;

   if deleting
   then
      begin
         select nvl(pedi_010.nome_cliente,' ')
         into ws_nome_cliente_o
         from pedi_010
         where pedi_010.cgc_9 = :old.cnpj_cliente9
           and pedi_010.cgc_4 = :old.cnpj_cliente4
           and pedi_010.cgc_2 = :old.cnpj_cliente2;
      exception when no_data_found then
         ws_nome_cliente_o  := ' ';
      end;

      begin
         select nvl(efic_050.nome,' ')
         into ws_nome_responsavel_o
         from efic_050
         where efic_050.cod_empresa     = ws_empresa
           and efic_050.cod_funcionario = :old.codigo_responsavel;
      exception when no_data_found then
         ws_nome_responsavel_o  := ' ';
      end;

      if :old.situacao_projeto = 0 then ws_situacao_projeto_o := 'EM DESENVOLVIMENTO'; end if;
      if :old.situacao_projeto = 1 then ws_situacao_projeto_o := 'DESENVOLVIDO'; end if;
      if :old.situacao_projeto = 2 then ws_situacao_projeto_o := 'APROVADO'; end if;
      if :old.situacao_projeto = 3 then ws_situacao_projeto_o := 'REJEITADO'; end if;

      begin
         INSERT INTO hist_100 (
            tabela,             operacao,
            data_ocorr,         aplicacao,
            usuario_rede,       maquina_rede,
            str01,              num05,
            long01
         ) VALUES (
            'BASI_001',         'O',
            sysdate,            ws_aplicativo,
            ws_usuario_rede,    ws_maquina_rede,
           :old.codigo_projeto, 0,
             '               '||
             inter_fn_buscar_tag('lb34974#PROJETO DE DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                                     chr(10)               ||
                                     chr(10)               ||
            inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || :old.codigo_projeto    || chr(10) ||
            inter_fn_buscar_tag('lb30896#DESCRICAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || :old.descricao_projeto || chr(10) ||
            inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || :old.nivel_produto     || '.' || :old.grupo_produto || chr(10)                                        ||
            inter_fn_buscar_tag('lb31254#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || to_char(:old.cnpj_cliente9,'000000000')     || '/' ||
            to_char(:old.cnpj_cliente4,'0000')     || '-' ||
            to_char(:old.cnpj_cliente2,'00')     ||
            ' - ' || ws_nome_cliente_o                     ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb11029#PROD. CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || :old.produto_cliente   || chr(10) ||
            inter_fn_buscar_tag('lb31305#RESPONSaVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || :old.codigo_responsavel ||
            ' - ' || ws_nome_responsavel_o                 ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb29847#SIT.PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || ws_situacao_projeto_o  || chr(10) ||
            inter_fn_buscar_tag('lb00610#DATA CADASTRO:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || to_char(:old.data_cadastro,'DD/MM/YYYY')  || chr(10) ||
            inter_fn_buscar_tag('lb33002#DATA INICIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || to_char(:old.data_inicio_projeto,'DD/MM/YYYY') ||
            inter_fn_buscar_tag('lb40890#DATA APROVADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || to_char(:old.data_aprovado,'DD/MM/YYYY') ||
            inter_fn_buscar_tag('lb40891#DATA REPROVADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || to_char(:old.data_reprovado,'DD/MM/YYYY')
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(001-3)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;
end inter_tr_basi_001_log;

-- ALTER TRIGGER "INTER_TR_BASI_001_LOG" ENABLE
 

/

exec inter_pr_recompile;

