
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_205_LOG" 
after insert or delete or update
on BASI_205
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid); 

 if inserting
 then
    begin

        insert into BASI_205_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           CODIGO_DEPOSITO_OLD,   /*8*/
           CODIGO_DEPOSITO_NEW,   /*9*/
           DESCRICAO_OLD,   /*10*/
           DESCRICAO_NEW,   /*11*/
           LOCAL_DEPOSITO_OLD,   /*12*/
           LOCAL_DEPOSITO_NEW,   /*13*/
           PRONTA_ENTREGA_OLD,   /*14*/
           PRONTA_ENTREGA_NEW,   /*15*/
           CONSIDERA_TMRP_OLD,   /*16*/
           CONSIDERA_TMRP_NEW,   /*17*/
           NUMERO_CAIXA_OLD,   /*18*/
           NUMERO_CAIXA_NEW,   /*19*/
           TIPO_VOLUME_OLD,   /*20*/
           TIPO_VOLUME_NEW,   /*21*/
           CONFIRMA_COLETA_OLD,   /*22*/
           CONFIRMA_COLETA_NEW,   /*23*/
           ROLO_MINI_OLD,   /*24*/
           ROLO_MINI_NEW,   /*25*/
           TIPO_PROD_DEPOSITO_OLD,   /*26*/
           TIPO_PROD_DEPOSITO_NEW,   /*27*/
           DEPOSITO_ORIGEM_OLD,   /*28*/
           DEPOSITO_ORIGEM_NEW,   /*29*/
           CONSISTE_SOL_MANU_OLD,   /*30*/
           CONSISTE_SOL_MANU_NEW,   /*31*/
           DEPOSITO_DESTINO_OLD,   /*32*/
           DEPOSITO_DESTINO_NEW,   /*33*/
           DEPOSITO_TERCEIRO_OLD,   /*34*/
           DEPOSITO_TERCEIRO_NEW,   /*35*/
           ACEITA_REQ_ALMOX_OLD,   /*36*/
           ACEITA_REQ_ALMOX_NEW,   /*37*/
           CONTROLA_FICHA_CARDEX_OLD,   /*38*/
           CONTROLA_FICHA_CARDEX_NEW,   /*39*/
           DISPONIVEL_NET_OLD,   /*40*/
           DISPONIVEL_NET_NEW,   /*41*/
           DEP_INATIVO_OLD,   /*42*/
           DEP_INATIVO_NEW,   /*43*/
           DATA_LIM_INATIVO_OLD,   /*44*/
           DATA_LIM_INATIVO_NEW,   /*45*/
           ENDERECA_CAIXA_OLD,   /*46*/
           ENDERECA_CAIXA_NEW,   /*47*/
           DEP_KG_ROLO_OLD,   /*48*/
           DEP_KG_ROLO_NEW,   /*49*/
           LIMPA_PEDIDO_VOLUME_OLD,   /*50*/
           LIMPA_PEDIDO_VOLUME_NEW,   /*51*/
           TIP_PROPRIEDADE_DEPOSITO_OLD,   /*52*/
           TIP_PROPRIEDADE_DEPOSITO_NEW,   /*53*/
           SITUACAO_DEPOSITO_OLD,   /*54*/
           SITUACAO_DEPOSITO_NEW,   /*55*/
           TIPO_VALORIZACAO_OLD,   /*56*/
           TIPO_VALORIZACAO_NEW,   /*57*/
           COD_PROCESSO_OLD,   /*58*/
           COD_PROCESSO_NEW,   /*59*/
           EMPENHA_ALMOXARIFADO_OLD,   /*60*/
           EMPENHA_ALMOXARIFADO_NEW,   /*61*/
           COD_OBSERVACAO_OLD,   /*62*/
           COD_OBSERVACAO_NEW,   /*63*/
           EMPRESA_CUSTO_GERENCIAL_OLD,   /*64*/
           EMPRESA_CUSTO_GERENCIAL_NEW,   /*65*/
           DEPOSITO_LOJA_OLD,   /*66*/
           DEPOSITO_LOJA_NEW,   /*67*/
           ICDDEPDESTTERCEIROS_OLD,   /*68*/
           ICDDEPDESTTERCEIROS_NEW    /*69*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.CODIGO_DEPOSITO, /*9*/
           '',/*10*/
           :new.DESCRICAO, /*11*/
           0,/*12*/
           :new.LOCAL_DEPOSITO, /*13*/
           0,/*14*/
           :new.PRONTA_ENTREGA, /*15*/
           0,/*16*/
           :new.CONSIDERA_TMRP, /*17*/
           0,/*18*/
           :new.NUMERO_CAIXA, /*19*/
           0,/*20*/
           :new.TIPO_VOLUME, /*21*/
           0,/*22*/
           :new.CONFIRMA_COLETA, /*23*/
           0,/*24*/
           :new.ROLO_MINI, /*25*/
           0,/*26*/
           :new.TIPO_PROD_DEPOSITO, /*27*/
           0,/*28*/
           :new.DEPOSITO_ORIGEM, /*29*/
           0,/*30*/
           :new.CONSISTE_SOL_MANU, /*31*/
           0,/*32*/
           :new.DEPOSITO_DESTINO, /*33*/
           0,/*34*/
           :new.DEPOSITO_TERCEIRO, /*35*/
           0,/*36*/
           :new.ACEITA_REQ_ALMOX, /*37*/
           0,/*38*/
           :new.CONTROLA_FICHA_CARDEX, /*39*/
           0,/*40*/
           :new.DISPONIVEL_NET, /*41*/
           0,/*42*/
           :new.DEP_INATIVO, /*43*/
           null,/*44*/
           :new.DATA_LIM_INATIVO, /*45*/
           0,/*46*/
           :new.ENDERECA_CAIXA, /*47*/
           0,/*48*/
           :new.DEP_KG_ROLO, /*49*/
           0,/*50*/
           :new.LIMPA_PEDIDO_VOLUME, /*51*/
           0,/*52*/
           :new.TIP_PROPRIEDADE_DEPOSITO, /*53*/
           0,/*54*/
           :new.SITUACAO_DEPOSITO, /*55*/
           0,/*56*/
           :new.TIPO_VALORIZACAO, /*57*/
           0,/*58*/
           :new.COD_PROCESSO, /*59*/
           0,/*60*/
           :new.EMPENHA_ALMOXARIFADO, /*61*/
           0,/*62*/
           :new.COD_OBSERVACAO, /*63*/
           0,/*64*/
           :new.EMPRESA_CUSTO_GERENCIAL, /*65*/
           0,/*66*/
           :new.DEPOSITO_LOJA, /*67*/
           0,/*68*/
           :new.ICDDEPDESTTERCEIROS /*69*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into BASI_205_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           CODIGO_DEPOSITO_OLD, /*8*/
           CODIGO_DEPOSITO_NEW, /*9*/
           DESCRICAO_OLD, /*10*/
           DESCRICAO_NEW, /*11*/
           LOCAL_DEPOSITO_OLD, /*12*/
           LOCAL_DEPOSITO_NEW, /*13*/
           PRONTA_ENTREGA_OLD, /*14*/
           PRONTA_ENTREGA_NEW, /*15*/
           CONSIDERA_TMRP_OLD, /*16*/
           CONSIDERA_TMRP_NEW, /*17*/
           NUMERO_CAIXA_OLD, /*18*/
           NUMERO_CAIXA_NEW, /*19*/
           TIPO_VOLUME_OLD, /*20*/
           TIPO_VOLUME_NEW, /*21*/
           CONFIRMA_COLETA_OLD, /*22*/
           CONFIRMA_COLETA_NEW, /*23*/
           ROLO_MINI_OLD, /*24*/
           ROLO_MINI_NEW, /*25*/
           TIPO_PROD_DEPOSITO_OLD, /*26*/
           TIPO_PROD_DEPOSITO_NEW, /*27*/
           DEPOSITO_ORIGEM_OLD, /*28*/
           DEPOSITO_ORIGEM_NEW, /*29*/
           CONSISTE_SOL_MANU_OLD, /*30*/
           CONSISTE_SOL_MANU_NEW, /*31*/
           DEPOSITO_DESTINO_OLD, /*32*/
           DEPOSITO_DESTINO_NEW, /*33*/
           DEPOSITO_TERCEIRO_OLD, /*34*/
           DEPOSITO_TERCEIRO_NEW, /*35*/
           ACEITA_REQ_ALMOX_OLD, /*36*/
           ACEITA_REQ_ALMOX_NEW, /*37*/
           CONTROLA_FICHA_CARDEX_OLD, /*38*/
           CONTROLA_FICHA_CARDEX_NEW, /*39*/
           DISPONIVEL_NET_OLD, /*40*/
           DISPONIVEL_NET_NEW, /*41*/
           DEP_INATIVO_OLD, /*42*/
           DEP_INATIVO_NEW, /*43*/
           DATA_LIM_INATIVO_OLD, /*44*/
           DATA_LIM_INATIVO_NEW, /*45*/
           ENDERECA_CAIXA_OLD, /*46*/
           ENDERECA_CAIXA_NEW, /*47*/
           DEP_KG_ROLO_OLD, /*48*/
           DEP_KG_ROLO_NEW, /*49*/
           LIMPA_PEDIDO_VOLUME_OLD, /*50*/
           LIMPA_PEDIDO_VOLUME_NEW, /*51*/
           TIP_PROPRIEDADE_DEPOSITO_OLD, /*52*/
           TIP_PROPRIEDADE_DEPOSITO_NEW, /*53*/
           SITUACAO_DEPOSITO_OLD, /*54*/
           SITUACAO_DEPOSITO_NEW, /*55*/
           TIPO_VALORIZACAO_OLD, /*56*/
           TIPO_VALORIZACAO_NEW, /*57*/
           COD_PROCESSO_OLD, /*58*/
           COD_PROCESSO_NEW, /*59*/
           EMPENHA_ALMOXARIFADO_OLD, /*60*/
           EMPENHA_ALMOXARIFADO_NEW, /*61*/
           COD_OBSERVACAO_OLD, /*62*/
           COD_OBSERVACAO_NEW, /*63*/
           EMPRESA_CUSTO_GERENCIAL_OLD, /*64*/
           EMPRESA_CUSTO_GERENCIAL_NEW, /*65*/
           DEPOSITO_LOJA_OLD, /*66*/
           DEPOSITO_LOJA_NEW, /*67*/
           ICDDEPDESTTERCEIROS_OLD, /*68*/
           ICDDEPDESTTERCEIROS_NEW  /*69*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.CODIGO_DEPOSITO,  /*8*/
           :new.CODIGO_DEPOSITO, /*9*/
           :old.DESCRICAO,  /*10*/
           :new.DESCRICAO, /*11*/
           :old.LOCAL_DEPOSITO,  /*12*/
           :new.LOCAL_DEPOSITO, /*13*/
           :old.PRONTA_ENTREGA,  /*14*/
           :new.PRONTA_ENTREGA, /*15*/
           :old.CONSIDERA_TMRP,  /*16*/
           :new.CONSIDERA_TMRP, /*17*/
           :old.NUMERO_CAIXA,  /*18*/
           :new.NUMERO_CAIXA, /*19*/
           :old.TIPO_VOLUME,  /*20*/
           :new.TIPO_VOLUME, /*21*/
           :old.CONFIRMA_COLETA,  /*22*/
           :new.CONFIRMA_COLETA, /*23*/
           :old.ROLO_MINI,  /*24*/
           :new.ROLO_MINI, /*25*/
           :old.TIPO_PROD_DEPOSITO,  /*26*/
           :new.TIPO_PROD_DEPOSITO, /*27*/
           :old.DEPOSITO_ORIGEM,  /*28*/
           :new.DEPOSITO_ORIGEM, /*29*/
           :old.CONSISTE_SOL_MANU,  /*30*/
           :new.CONSISTE_SOL_MANU, /*31*/
           :old.DEPOSITO_DESTINO,  /*32*/
           :new.DEPOSITO_DESTINO, /*33*/
           :old.DEPOSITO_TERCEIRO,  /*34*/
           :new.DEPOSITO_TERCEIRO, /*35*/
           :old.ACEITA_REQ_ALMOX,  /*36*/
           :new.ACEITA_REQ_ALMOX, /*37*/
           :old.CONTROLA_FICHA_CARDEX,  /*38*/
           :new.CONTROLA_FICHA_CARDEX, /*39*/
           :old.DISPONIVEL_NET,  /*40*/
           :new.DISPONIVEL_NET, /*41*/
           :old.DEP_INATIVO,  /*42*/
           :new.DEP_INATIVO, /*43*/
           :old.DATA_LIM_INATIVO,  /*44*/
           :new.DATA_LIM_INATIVO, /*45*/
           :old.ENDERECA_CAIXA,  /*46*/
           :new.ENDERECA_CAIXA, /*47*/
           :old.DEP_KG_ROLO,  /*48*/
           :new.DEP_KG_ROLO, /*49*/
           :old.LIMPA_PEDIDO_VOLUME,  /*50*/
           :new.LIMPA_PEDIDO_VOLUME, /*51*/
           :old.TIP_PROPRIEDADE_DEPOSITO,  /*52*/
           :new.TIP_PROPRIEDADE_DEPOSITO, /*53*/
           :old.SITUACAO_DEPOSITO,  /*54*/
           :new.SITUACAO_DEPOSITO, /*55*/
           :old.TIPO_VALORIZACAO,  /*56*/
           :new.TIPO_VALORIZACAO, /*57*/
           :old.COD_PROCESSO,  /*58*/
           :new.COD_PROCESSO, /*59*/
           :old.EMPENHA_ALMOXARIFADO,  /*60*/
           :new.EMPENHA_ALMOXARIFADO, /*61*/
           :old.COD_OBSERVACAO,  /*62*/
           :new.COD_OBSERVACAO, /*63*/
           :old.EMPRESA_CUSTO_GERENCIAL,  /*64*/
           :new.EMPRESA_CUSTO_GERENCIAL, /*65*/
           :old.DEPOSITO_LOJA,  /*66*/
           :new.DEPOSITO_LOJA, /*67*/
           :old.ICDDEPDESTTERCEIROS,  /*68*/
           :new.ICDDEPDESTTERCEIROS  /*69*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into BASI_205_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           CODIGO_DEPOSITO_OLD, /*8*/
           CODIGO_DEPOSITO_NEW, /*9*/
           DESCRICAO_OLD, /*10*/
           DESCRICAO_NEW, /*11*/
           LOCAL_DEPOSITO_OLD, /*12*/
           LOCAL_DEPOSITO_NEW, /*13*/
           PRONTA_ENTREGA_OLD, /*14*/
           PRONTA_ENTREGA_NEW, /*15*/
           CONSIDERA_TMRP_OLD, /*16*/
           CONSIDERA_TMRP_NEW, /*17*/
           NUMERO_CAIXA_OLD, /*18*/
           NUMERO_CAIXA_NEW, /*19*/
           TIPO_VOLUME_OLD, /*20*/
           TIPO_VOLUME_NEW, /*21*/
           CONFIRMA_COLETA_OLD, /*22*/
           CONFIRMA_COLETA_NEW, /*23*/
           ROLO_MINI_OLD, /*24*/
           ROLO_MINI_NEW, /*25*/
           TIPO_PROD_DEPOSITO_OLD, /*26*/
           TIPO_PROD_DEPOSITO_NEW, /*27*/
           DEPOSITO_ORIGEM_OLD, /*28*/
           DEPOSITO_ORIGEM_NEW, /*29*/
           CONSISTE_SOL_MANU_OLD, /*30*/
           CONSISTE_SOL_MANU_NEW, /*31*/
           DEPOSITO_DESTINO_OLD, /*32*/
           DEPOSITO_DESTINO_NEW, /*33*/
           DEPOSITO_TERCEIRO_OLD, /*34*/
           DEPOSITO_TERCEIRO_NEW, /*35*/
           ACEITA_REQ_ALMOX_OLD, /*36*/
           ACEITA_REQ_ALMOX_NEW, /*37*/
           CONTROLA_FICHA_CARDEX_OLD, /*38*/
           CONTROLA_FICHA_CARDEX_NEW, /*39*/
           DISPONIVEL_NET_OLD, /*40*/
           DISPONIVEL_NET_NEW, /*41*/
           DEP_INATIVO_OLD, /*42*/
           DEP_INATIVO_NEW, /*43*/
           DATA_LIM_INATIVO_OLD, /*44*/
           DATA_LIM_INATIVO_NEW, /*45*/
           ENDERECA_CAIXA_OLD, /*46*/
           ENDERECA_CAIXA_NEW, /*47*/
           DEP_KG_ROLO_OLD, /*48*/
           DEP_KG_ROLO_NEW, /*49*/
           LIMPA_PEDIDO_VOLUME_OLD, /*50*/
           LIMPA_PEDIDO_VOLUME_NEW, /*51*/
           TIP_PROPRIEDADE_DEPOSITO_OLD, /*52*/
           TIP_PROPRIEDADE_DEPOSITO_NEW, /*53*/
           SITUACAO_DEPOSITO_OLD, /*54*/
           SITUACAO_DEPOSITO_NEW, /*55*/
           TIPO_VALORIZACAO_OLD, /*56*/
           TIPO_VALORIZACAO_NEW, /*57*/
           COD_PROCESSO_OLD, /*58*/
           COD_PROCESSO_NEW, /*59*/
           EMPENHA_ALMOXARIFADO_OLD, /*60*/
           EMPENHA_ALMOXARIFADO_NEW, /*61*/
           COD_OBSERVACAO_OLD, /*62*/
           COD_OBSERVACAO_NEW, /*63*/
           EMPRESA_CUSTO_GERENCIAL_OLD, /*64*/
           EMPRESA_CUSTO_GERENCIAL_NEW, /*65*/
           DEPOSITO_LOJA_OLD, /*66*/
           DEPOSITO_LOJA_NEW, /*67*/
           ICDDEPDESTTERCEIROS_OLD, /*68*/
           ICDDEPDESTTERCEIROS_NEW /*69*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.CODIGO_DEPOSITO, /*8*/
           0, /*9*/
           :old.DESCRICAO, /*10*/
           '', /*11*/
           :old.LOCAL_DEPOSITO, /*12*/
           0, /*13*/
           :old.PRONTA_ENTREGA, /*14*/
           0, /*15*/
           :old.CONSIDERA_TMRP, /*16*/
           0, /*17*/
           :old.NUMERO_CAIXA, /*18*/
           0, /*19*/
           :old.TIPO_VOLUME, /*20*/
           0, /*21*/
           :old.CONFIRMA_COLETA, /*22*/
           0, /*23*/
           :old.ROLO_MINI, /*24*/
           0, /*25*/
           :old.TIPO_PROD_DEPOSITO, /*26*/
           0, /*27*/
           :old.DEPOSITO_ORIGEM, /*28*/
           0, /*29*/
           :old.CONSISTE_SOL_MANU, /*30*/
           0, /*31*/
           :old.DEPOSITO_DESTINO, /*32*/
           0, /*33*/
           :old.DEPOSITO_TERCEIRO, /*34*/
           0, /*35*/
           :old.ACEITA_REQ_ALMOX, /*36*/
           0, /*37*/
           :old.CONTROLA_FICHA_CARDEX, /*38*/
           0, /*39*/
           :old.DISPONIVEL_NET, /*40*/
           0, /*41*/
           :old.DEP_INATIVO, /*42*/
           0, /*43*/
           :old.DATA_LIM_INATIVO, /*44*/
           null, /*45*/
           :old.ENDERECA_CAIXA, /*46*/
           0, /*47*/
           :old.DEP_KG_ROLO, /*48*/
           0, /*49*/
           :old.LIMPA_PEDIDO_VOLUME, /*50*/
           0, /*51*/
           :old.TIP_PROPRIEDADE_DEPOSITO, /*52*/
           0, /*53*/
           :old.SITUACAO_DEPOSITO, /*54*/
           0, /*55*/
           :old.TIPO_VALORIZACAO, /*56*/
           0, /*57*/
           :old.COD_PROCESSO, /*58*/
           0, /*59*/
           :old.EMPENHA_ALMOXARIFADO, /*60*/
           0, /*61*/
           :old.COD_OBSERVACAO, /*62*/
           0, /*63*/
           :old.EMPRESA_CUSTO_GERENCIAL, /*64*/
           0, /*65*/
           :old.DEPOSITO_LOJA, /*66*/
           0, /*67*/
           :old.ICDDEPDESTTERCEIROS, /*68*/
           0 /*69*/
         );
    end;
 end if;
end inter_tr_BASI_205_log;
-- ALTER TRIGGER "INTER_TR_BASI_205_LOG" ENABLE
 

/

exec inter_pr_recompile;

