ALTER TABLE obrf_076 DROP CONSTRAINT PK_OBRF_076;

ALTER TABLE obrf_076
DROP CONSTRAINT PK_OBRF_076;
create index PK_OBRF_076 on obrf_076 (cnpj_terceiro9,cnpj_terceiro4,cnpj_terceiro2,tipo_servico,col_tabela_preco,mes_tabela_preco,seq_tabela_preco);

drop index PK_OBRF_076;

ALTER TABLE obrf_076
ADD CONSTRAINT PK_OBRF_076 PRIMARY KEY
(cnpj_terceiro9,cnpj_terceiro4,cnpj_terceiro2,tipo_servico,col_tabela_preco,mes_tabela_preco,seq_tabela_preco);

commit;
