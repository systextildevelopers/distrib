create or replace package body "ST_PCK_REPRESENTANTE" is

FUNCTION get_dados_cod_rep_cliente(p_cod_rep_cliente number, p_msg_erro in out varchar2) return t_dados_pedi_020
AS
    v_dados_pedi_020 t_dados_pedi_020;
    
BEGIN
    begin
        SELECT  cod_rep_cliente,            tipo_repr,                 nome_rep_cliente,
                end_rep_cliente,            bairro,                    cod_cidade,
                cxpo_rep_cliente,           cep_rep_cliente,           numero_celular,
                numero_fax,                 fone_rep_cliente,          cgc_9,
                cgc_4,                      cgc_2,                     ines_rep_cliente,
                cod_banco_repr,             codigo_agencia,            conta_banco_rep,
                perc_comis_venda,           perc_comis_fatu,           perc_comis_crec,
                perc_ir_federal,            cod_empresa,               saldo_comissao,
                margem_cotas,               codigo_administr,          sit_rep_cliente,
                data_fechamento,            conta_contabil,            val_min_imposto,
                tipo_prod_repres,           e_mail,                    represent_isento,
                codigo_contabil,            perc_comis_administrador,  envia_mala,
                perc_indenizacao,           senha,                     fantasia_repres,
                core,                       nome_pessoal,              data_nascimento,
                nome_conjuge,               email_pessoal,             observacoes,
                envia_email,                cd_cargo_rep,              endereco_pessoal,
                numero_pessoal,             bairro_pessoal,            cep_pessoal,
                cod_cidade_pessoal,         complemento_pessoal,       identidade_pessoal,
                cpf_pessoal_9,              cpf_pessoal_2,             loja_fv,
                considera_plano_comissao,   ddd_celular,               conceito_repres,
                data_atualizacao_api
        INTO v_dados_pedi_020
        FROM pedi_020
        WHERE cod_rep_cliente = p_cod_rep_cliente;
    exception when others then
        p_msg_erro := 'Não foi possível buscar dados para o representante!. p_cod_rep_cliente:' || p_cod_rep_cliente;
        return null;
    end;

    return v_dados_pedi_020;
    
END;

end "ST_PCK_REPRESENTANTE";
