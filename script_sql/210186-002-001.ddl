create table mppm_001
( 
  nr_solicitacao       NUMBER(9) default 0 not null,
  nome_relatorio       VARCHAR2(15) default '' not null,
  sequencia            NUMBER(9) default 0 not null,
  data_criacao         DATE default sysdate,
  codigo_usuario       NUMBER(9) default 0,
  flag_gera_relatorio  NUMBER(1) default 0,
  flag_selecao         NUMBER(1) default 0,
  codigo_empresa       NUMBER(3) default 0,
  pedido_venda         NUMBER(9) default 0 not null,
  data_emis_venda      DATE,
  data_entr_venda      DATE,
  cli_ped_cgc_cli9     NUMBER(9) default 0,
  cli_ped_cgc_cli4     NUMBER(4) default 0,
  cli_ped_cgc_cli2     NUMBER(2) default 0,
  cod_rep_cliente      NUMBER(5) default 0,
  grupo_economico      NUMBER(3) default 0,
  colecao_tabela       NUMBER(2) default 0,
  mes_tabela           NUMBER(2) default 0,
  sequencia_tabela     NUMBER(2) default 0,
  cond_pgto_venda      NUMBER(3) default 0,
  classificacao_pedido NUMBER(5) default 0,
  cod_banco            NUMBER(3) default 0,
  natop_pv_nat_oper    NUMBER(3) default 0,
  valor_saldo_pedi     NUMBER(15,2) default 0.0
);

comment on column mppm_001.flag_gera_relatorio is '0 - n�o gerado, 1 - gerado ok, 2 - gerado com problema (vermelho n�o vai fazer nada)';

/

exec inter_pr_recompile;
