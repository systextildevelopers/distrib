alter table i_obrf_015 add situacao_liberacao number(1);

alter table i_obrf_015 modify situacao_liberacao default 0;

exec inter_pr_recompile;
