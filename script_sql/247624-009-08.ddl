alter table estq_420
    add tipo_processo_transf number(1);

COMMENT ON COLUMN estq_420.tipo_processo_transf IS 'Indica o tipo de transferência (com ou sem nota) utilizado pelo programa ESTQ_FA25, considerando o parâmetro "preview". (0 - O Processo Não Terá Nota de Transferência e Já Está Liberado Para Coleta., 1 - O Processo Será Por Nota de Transferência.)';

-- execute
