create or replace procedure "INTER_PR_GERA_CAPA_PED_COMPRA"
                        (p_codigo_comprador NUMBER,
                        p_codigo_empresa NUMBER,
                        p_usuario VARCHAR2,
                        p_cgc_forn9 NUMBER,
                        p_cgc_forn4 NUMBER,
                        p_cgc_forn2 NUMBER,
                        p_data_prev_entr DATE,
                        p_tab_preco NUMBER,
                        p_codigo_transacao NUMBER,
                        p_condicao_pgto NUMBER,
                        v_pedido_compra out NUMBER) is

v_nr_pedc_autom NUMERIC;
v_maior_pedido NUMERIC;
v_datetime_pedido DATE;
v_situacao_fornecedor NUMERIC;
v_email_fornecedor VARCHAR2(256);
v_tmp_int NUMERIC;
v_situacao_pedido NUMERIC;
v_email VARCHAR2(256);
v_assunto_email VARCHAR(100);
v_corpo_e_mail VARCHAR2(4000);
v_texto_email VARCHAR2(4000);
v_msg_erro VARCHAR2(4000);
v_rpt_impr_pedc VARCHAR2(100);
v_verifica_sit NUMERIC;
v_cond_pgto_compra NUMERIC;

begin

    v_msg_erro := '';
    v_pedido_compra := 0;

    if p_codigo_comprador = 0
    then
            raise_application_error(-20001,'ATEN��O! C�digo do comprador n�o pode ser 0.');
    end if;
    
    select nr_pedc_autom 
    into v_nr_pedc_autom
    from empr_001;

    if v_nr_pedc_autom = 1 
    then
        v_maior_pedido := 0;

        select nvl(max(supr_090.pedido_compra),0) 
        into v_maior_pedido
        from supr_090;

        while v_pedido_compra <= v_maior_pedido
        LOOP
            select seq_pedido_compra.nextval 
            into v_pedido_compra
            from dual;
            
            if v_pedido_compra is null
            then v_pedido_compra := 0;
            end if;
        END LOOP;
    end if;
    
    v_datetime_pedido := sysdate;

    begin
        select supr_010.sit_fornecedor, supr_010.e_mail
        into v_situacao_fornecedor, v_email_fornecedor
        from supr_010
        where supr_010.fornecedor9 = p_cgc_forn9
          and supr_010.fornecedor4 = p_cgc_forn4
          and supr_010.fornecedor2 = p_cgc_forn2;
    exception when no_data_found then
        v_situacao_fornecedor := 0;
    end;

    if v_situacao_fornecedor = 9
    then
        raise_application_error(-20001,'ATEN��O! Situa��o do Fornecedor est� como PR�-CADASTRO. Precisar� ser alterada para ATIVO por usu�rio qualificado para tal.');
    end if;

    if v_situacao_fornecedor = 2
    then
        raise_application_error(-20001,'ATEN��O! Situa��o do Fornecedor est� como INATIVO. Precisar� ser alterada para ATIVO por usu�rio qualificado para tal.');
    end if;
    
    select fatu_502.sit_inic_pedido_compra 
    into v_verifica_sit
    from fatu_502
    where fatu_502.codigo_empresa = p_codigo_empresa;

    if v_verifica_sit = 1
    then
        v_situacao_pedido := 7;
    else
        v_situacao_pedido := 1;
        
        begin
            select 1 
            into v_tmp_int
            from hdoc_100
            where hdoc_100.usubloq_empr_usu = p_codigo_empresa
              and hdoc_100.area_bloqueio = 3
              and rownum = 1;
        exception when no_data_found then
            v_tmp_int := 0;
        end;
        
        if v_tmp_int = 1
        then
            v_situacao_pedido := 9;
        end if;
    end if;

    begin
        select 1 
        into v_tmp_int
        from hdoc_100 
        where hdoc_100.situacao_liberador = 0 
          and hdoc_100.area_bloqueio = 3 
          and hdoc_100.usubloq_empr_usu = p_codigo_empresa
          and rownum = 1;
    exception when no_data_found then
        v_situacao_pedido := 1;
    end;

    if p_condicao_pgto = 0
    then
        select supr_090.cond_pgto_compra
        into v_cond_pgto_compra
        from supr_090 
        where supr_090.forn_ped_forne9 =  p_cgc_forn9
          and supr_090.forn_ped_forne4 = p_cgc_forn4
          and supr_090.forn_ped_forne2 = p_cgc_forn2
          and supr_090.codigo_empresa  = p_codigo_empresa
          and rownum = 1;
    else
        v_cond_pgto_compra := p_condicao_pgto;
    end if;

    INSERT INTO SUPR_090(
        codigo_empresa, pedido_compra,
        dt_emis_ped_comp, data_prev_entr,
        forn_ped_forne9, forn_ped_forne4,
        forn_ped_forne2, tab_preco,
        codigo_transacao, datetime_pedido,
        codigo_comprador, tipo_frete,
        cod_end_entrega, cod_end_cobranca,
        cond_pgto_compra, situacao_pedido,
        e_mail
    ) values(
        p_codigo_empresa, v_pedido_compra,
        sysdate, p_data_prev_entr,
        p_cgc_forn9, p_cgc_forn4,
        p_cgc_forn2, p_tab_preco,
        p_codigo_transacao, v_datetime_pedido,
        p_codigo_comprador, 1,
        p_codigo_empresa, p_codigo_empresa,
        v_cond_pgto_compra, v_situacao_pedido,
        v_email_fornecedor
    );
    commit;

end INTER_PR_GERA_CAPA_PED_COMPRA;
