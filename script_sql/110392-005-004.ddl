create or replace view inter_vi_contas_consolidadas  as
  select cont_k201.exercicio,
	 	 cont_k201.cod_controladora,
         cont_k201.cod_plano_controladora,
		 cont_k201.cta_controladora,
         cont_k201.cod_plano_controlada,
		 cont_k201.cod_red_controlada,
		 cont_k201.cta_controlada
  from cont_k201;  
  
  /
exec inter_pr_recompile;
