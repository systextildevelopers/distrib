drop table pedi_015;

create table pedi_015(
    CLIENTE_CGC_9 NUMBER(9,0)
    ,CLIENTE_CGC_4 NUMBER(4,0)
    ,CLIENTE_CGC_2 NUMBER(2,0)
    ,PESO_MAX_ROLO NUMBER(9,3)
    ,PERC_MINIMO   NUMBER
    ,PERC_MAXIMO   NUMBER
);

ALTER TABLE PEDI_015
   ADD CONSTRAINT PK_PEDI_015 PRIMARY KEY (CLIENTE_CGC_9, CLIENTE_CGC_4, CLIENTE_CGC_2);
