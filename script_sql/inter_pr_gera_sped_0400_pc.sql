create or replace procedure inter_pr_gera_sped_0400_pc(p_cod_empresa     IN  NUMBER,
                                                       p_cod_matriz      IN  NUMBER,
                                                       p_cnpj9_empresa   IN  NUMBER,
                                                       p_cnpj4_empresa   IN  NUMBER,
                                                       p_cnpj2_empresa   IN  NUMBER,
                                                       p_des_erro        OUT varchar2) is
--
-- Finalidade: Gerar a tabela sped_pc_0400 - Naturezas de Opera��o
-- Autor.....: C�sar Anton
-- Data......: 20/11/08
--
-- Hist�ricos
--
-- Data    Autor    Observa��es
--

CURSOR u_sped_pc_c170 (p_cod_empresa    NUMBER,
                       p_cod_matriz     NUMBER,
                       indConsolDetalhe NUMBER) IS
   SELECT DISTINCT cod_nat_oper, sig_estado_oper
   from   sped_pc_c170, sped_pc_c100
   where sped_pc_c170.cod_empresa       = sped_pc_c100.cod_empresa
     and sped_pc_c170.num_nota_fiscal   = sped_pc_c100.num_nota_fiscal
     and sped_pc_c170.cod_serie_nota    = sped_pc_c100.cod_serie_nota
     and sped_pc_c170.num_cnpj_9        = sped_pc_c100.Num_Cnpj_9
     and sped_pc_c170.num_cnpj_4        = sped_pc_c100.Num_Cnpj_4
     and sped_pc_c170.num_cnpj_2        = sped_pc_c100.Num_Cnpj_2
     and sped_pc_c100.tip_entrada_saida = sped_pc_c170.tip_entrada_saida
     and sped_pc_c100.cod_modelo not in ('00','02','06','07','08','09','10','11','21','22','27','28','57')
     and (not (sped_pc_c100.cod_modelo in ('55','65','59') and sped_pc_c100.tip_entrada_saida = 'S') or indConsolDetalhe = 2)
     and sped_pc_c100.cod_situacao_nota not in (2,3,5)
     and sped_pc_c100.cod_serie_nota    <> 'ECF'
     and sped_pc_c100.ind_nf_consumo    = 'N'
     and sped_pc_c100.cod_matriz        = p_cod_matriz
     and sped_pc_c100.cod_empresa       = p_cod_empresa;

CURSOR u_pedi_080 (p_cod_nature_opera  pedi_080.natur_operacao%TYPE,
                   p_sig_estado        pedi_080.estado_natoper%type) IS
   SELECT *
   from   pedi_080
   where  pedi_080.natur_operacao = p_cod_nature_opera
   AND    pedi_080.estado_natoper = p_sig_estado;

w_erro                 EXCEPTION;
w_ind_achou            VARCHAR2(1);
w_des_mensagem         sped_pc_0450.des_mensagem%type;
w_tem_msg              number(2);
indConsolDetalhe       number;

BEGIN
   p_des_erro          := NULL;

   indConsolDetalhe := inter_fn_get_param_int(p_cod_matriz, 'sped.indivConsol');

   FOR sped_pc_c170 IN u_sped_pc_c170 (p_cod_empresa,p_cod_matriz, indConsolDetalhe)
   LOOP
       w_ind_achou := 'N';

       FOR pedi_080 IN u_pedi_080 (sped_pc_c170.cod_nat_oper, sped_pc_c170.sig_estado_oper) LOOP
           w_ind_achou := 'S';
           BEGIN
               INSERT INTO sped_pc_0400
                 (cod_empresa
                 ,cod_matriz
                 ,num_cnpj9_empr
                 ,num_cnpj4_empr
                 ,num_cnpj2_empr
                 ,cod_natureza
                 ,des_natureza
                 ) VALUES
                 (p_cod_empresa
                 ,p_cod_matriz
                 ,p_cnpj9_empresa
                 ,p_cnpj4_empresa
                 ,p_cnpj2_empresa
                 ,pedi_080.natur_operacao || pedi_080.estado_natoper
                 ,pedi_080.descr_nat_oper);
           EXCEPTION
               WHEN Dup_Val_On_Index THEN
                  NULL;
               WHEN OTHERS THEN
                 p_des_erro := 'Erro na inclus�o da tabela sped_pc_0400 ' || Chr(10) || SQLERRM;
                 RAISE W_ERRO;
           END;

           if pedi_080.cod_mensagem > 0
           then
              begin
                 select trim(obrf_874.des_mensagem1 ||  obrf_874.des_mensagem2 ||
                        obrf_874.des_mensagem3 ||  obrf_874.des_mensagem4 ||
                        obrf_874.des_mensagem5)
                 into   w_des_mensagem
                 from obrf_874
                 where obrf_874.cod_mensagem = pedi_080.cod_mensagem;
              exception
                 when no_data_found then
                 w_des_mensagem  := ' ';
              end;

              begin
                 select count(*)
                 into w_tem_msg
                 from sped_pc_0450
                 where sped_pc_0450.cod_empresa    = p_cod_empresa
                   and sped_pc_0450.num_cnpj9_empr = p_cnpj9_empresa
                   and sped_pc_0450.num_cnpj4_empr = p_cnpj4_empresa
                   and sped_pc_0450.num_cnpj2_empr = p_cnpj2_empresa
                   and sped_pc_0450.cod_mensagem   = pedi_080.cod_mensagem;
              exception
                 when no_data_found then
                 w_tem_msg := 0;
              end;

              if w_tem_msg = 0
              then
                  BEGIN
                     INSERT INTO sped_pc_0450
                        (cod_empresa
                        ,cod_matriz
                        ,num_cnpj9_empr
                        ,num_cnpj4_empr
                        ,num_cnpj2_empr
                        ,cod_mensagem
                        ,des_mensagem
                        ) values
                        (p_cod_empresa                 -- cod_empresa
                        ,p_cod_matriz
                        ,p_cnpj9_empresa
                        ,p_cnpj4_empresa
                        ,p_cnpj2_empresa
                        ,pedi_080.cod_mensagem         -- cod_mensagem
                        ,substr(w_des_mensagem,1,255)   -- des_mesagem
                        );
                  EXCEPTION
                     WHEN OTHERS THEN
                        p_des_erro := 'Erro na inclus�o da tabela sped_pc_0450 ' || Chr(10) ||
                                      ' MENSAGEM: ' || pedi_080.cod_mensagem ||
                                      Chr(10) || SQLERRM;
                        RAISE W_ERRO;
                  END;
              end if;
           end if;

       END LOOP;

       COMMIT;

   END LOOP;

   COMMIT;

EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_pc_0400 ' || Chr(10) || p_des_erro;
      ROLLBACK;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure p_gera_sped_pc_0400 ' || Chr(10) || SQLERRM;
      ROLLBACK;
END inter_pr_gera_sped_0400_pc;

/
exec inter_pr_recompile;
