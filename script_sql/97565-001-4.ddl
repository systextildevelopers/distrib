--14091
alter table fatu_050
add (val_fcp_uf_dest_nf        number(13,2),
     val_icms_uf_dest_nf       number(13,2),
     val_icms_uf_remet_nf      number(13,2));
 
alter table fatu_050
modify (val_fcp_uf_dest_nf        default 0,
        val_icms_uf_dest_nf       default 0,
        val_icms_uf_remet_nf      default 0);

comment on column fatu_050.val_fcp_uf_dest_nf is 'Valor total do ICMS relativo ao Fundo de Combate � Pobreza (FCP) da UF de destino';
comment on column fatu_050.val_icms_uf_dest_nf is 'Valor total do ICMS de partilha para a UF do destinat�rio';
comment on column fatu_050.val_icms_uf_remet_nf is 'Valor total do ICMS de partilha para a UF do remetente';

exec inter_pr_recompile;
/