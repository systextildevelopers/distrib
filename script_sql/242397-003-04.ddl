-- Create table 
drop table PCPT_071_LOG;
create table PCPT_071_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(20) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(20) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  id_OLD               NUMBER(9) ,  
  id_NEW               NUMBER(9) , 
  id_sugestao_OLD      NUMBER(9) ,  
  id_sugestao_NEW      NUMBER(9) , 
  pedido_venda_OLD     NUMBER(9) ,  
  pedido_venda_NEW     NUMBER(9) , 
  seq_item_pedido_OLD  NUMBER(3) ,  
  seq_item_pedido_NEW  NUMBER(3) , 
  sequencia_OLD        NUMBER(9) ,  
  sequencia_NEW        NUMBER(9) , 
  codigo_rolo_OLD      NUMBER(9) ,  
  codigo_rolo_NEW      NUMBER(9) , 
  tipo_de_sugestao_OLD VARCHAR2(30) , 
  tipo_de_sugestao_NEW VARCHAR2(30)  
) ;

