alter table efic_090
add  intervalo number(2) default 0;

comment on column efic_090.intervalo is 'Intervalo de produção da parada.';

exec inter_pr_recompile;
