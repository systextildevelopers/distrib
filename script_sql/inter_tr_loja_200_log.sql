
  CREATE OR REPLACE TRIGGER "INTER_TR_LOJA_200_LOG" 
after insert or delete or update
on loja_200
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);  
    

 if inserting
 then
    begin

        insert into loja_200_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           COD_EMPRESA_OLD,   /*8*/
           COD_EMPRESA_NEW,   /*9*/
           DOCUMENTO_OLD,   /*10*/
           DOCUMENTO_NEW,   /*11*/
           CGC9_OLD,   /*12*/
           CGC9_NEW,   /*13*/
           CGC4_OLD,   /*14*/
           CGC4_NEW,   /*15*/
           CGC2_OLD,   /*16*/
           CGC2_NEW,   /*17*/
           TAB_COL_OLD,   /*18*/
           TAB_COL_NEW,   /*19*/
           TAB_MES_OLD,   /*20*/
           TAB_MES_NEW,   /*21*/
           TAB_SEQ_OLD,   /*22*/
           TAB_SEQ_NEW,   /*23*/
           COND_PAGTO_OLD,   /*24*/
           COND_PAGTO_NEW,   /*25*/
           VENDEDOR_OLD,   /*26*/
           VENDEDOR_NEW,   /*27*/
           REPRESENTANTE_OLD,   /*28*/
           REPRESENTANTE_NEW,   /*29*/
           VALOR_TOTAL_OLD,   /*30*/
           VALOR_TOTAL_NEW,   /*31*/
           VALOR_DESC_OLD,   /*32*/
           VALOR_DESC_NEW,   /*33*/
           PERC_DESC_OLD,   /*34*/
           PERC_DESC_NEW,   /*35*/
           VALOR_LIQUIDO_OLD,   /*36*/
           VALOR_LIQUIDO_NEW,   /*37*/
           TIPO_VENDA_OLD,   /*38*/
           TIPO_VENDA_NEW,   /*39*/
           SERIE_OLD,   /*40*/
           SERIE_NEW,   /*41*/
           DEPOSITO_LOJA_OLD,   /*42*/
           DEPOSITO_LOJA_NEW,   /*43*/
           NATU_LOJA_OLD,   /*44*/
           NATU_LOJA_NEW,   /*45*/
           ESTADO_NATU_OLD,   /*46*/
           ESTADO_NATU_NEW,   /*47*/
           TRANSPORT9_OLD,   /*48*/
           TRANSPORT9_NEW,   /*49*/
           TRANSPORT4_OLD,   /*50*/
           TRANSPORT4_NEW,   /*51*/
           TRANSPORT2_OLD,   /*52*/
           TRANSPORT2_NEW,   /*53*/
           SELEC_CUPOM_OLD,   /*54*/
           SELEC_CUPOM_NEW,   /*55*/
           ULTIMA_SEQUENCIA_OLD,   /*56*/
           ULTIMA_SEQUENCIA_NEW,   /*57*/
           PERC_COMIS_REPRES_OLD,   /*58*/
           PERC_COMIS_REPRES_NEW,   /*59*/
           PERC_COMIS_VENDEDOR_OLD,   /*60*/
           PERC_COMIS_VENDEDOR_NEW,   /*61*/
           BANCO_OLD,   /*62*/
           BANCO_NEW,   /*63*/
           SEQ_END_ENTREGA_OLD,   /*64*/
           SEQ_END_ENTREGA_NEW,   /*65*/
           BLOQUEIO_LIMITE_OLD,   /*66*/
           BLOQUEIO_LIMITE_NEW,   /*67*/
           CLI9RESPTIT_OLD,   /*68*/
           CLI9RESPTIT_NEW,   /*69*/
           CLI4RESPTIT_OLD,   /*70*/
           CLI4RESPTIT_NEW,   /*71*/
           CLI2RESPTIT_OLD,   /*72*/
           CLI2RESPTIT_NEW,   /*73*/
           TIPO_COMISSAO_OLD,   /*74*/
           TIPO_COMISSAO_NEW,   /*75*/
           COD_FORMA_PGTO_OLD,   /*76*/
           COD_FORMA_PGTO_NEW,   /*77*/
           TIPO_COMIS_VEND_OLD,   /*78*/
           TIPO_COMIS_VEND_NEW,   /*79*/
           SITUACAO_OLD,   /*80*/
           SITUACAO_NEW,   /*81*/
           COND_PAGTO_ORIG_OLD,   /*82*/
           COND_PAGTO_ORIG_NEW,   /*83*/
           QUANTIDADE_OLD,   /*84*/
           QUANTIDADE_NEW,   /*85*/
           QUANTIDADE_ORIG_OLD,   /*86*/
           QUANTIDADE_ORIG_NEW,   /*87*/
           GUIA_OLD,   /*88*/
           GUIA_NEW,   /*89*/
           DATA_EMISSAO_OLD,   /*90*/
           DATA_EMISSAO_NEW,   /*91*/
           TIPO_FRETE_OLD,   /*92*/
           TIPO_FRETE_NEW,   /*93*/
           VALOR_FRETE_OLD,   /*94*/
           VALOR_FRETE_NEW,   /*95*/
           DATA_FATURAMENTO_OLD,   /*96*/
           DATA_FATURAMENTO_NEW,   /*97*/
           PERC_DESC_FINAL_OLD,   /*98*/
           PERC_DESC_FINAL_NEW,   /*99*/
           VALOR_DESC_FINAL_OLD,   /*100*/
           VALOR_DESC_FINAL_NEW,   /*101*/
           PERC_DESC_COND_PGTO_OLD,   /*102*/
           PERC_DESC_COND_PGTO_NEW,   /*103*/
           PERC_ACRES_COND_PGTO_OLD,   /*104*/
           PERC_ACRES_COND_PGTO_NEW,   /*105*/
           QTDE_TROCA_OLD,   /*106*/
           QTDE_TROCA_NEW,   /*107*/
           VALOR_TROCA_OLD,   /*108*/
           VALOR_TROCA_NEW,   /*109*/
           PRAZO_MEDIO_OLD,   /*110*/
           PRAZO_MEDIO_NEW,   /*111*/
           TIPO_TITULO_OLD,   /*112*/
           TIPO_TITULO_NEW,   /*113*/
           DATA_BASE_OLD,   /*114*/
           DATA_BASE_NEW,   /*115*/
           PERCENTUAL_VENDA_OLD,   /*116*/
           PERCENTUAL_VENDA_NEW,   /*117*/
           NUM_INTERVENCAO_OLD,   /*118*/
           NUM_INTERVENCAO_NEW,   /*119*/
           NUM_CUPOM_OLD,   /*120*/
           NUM_CUPOM_NEW,   /*121*/
           MAQUINA_USUARIO_OLD,   /*122*/
           MAQUINA_USUARIO_NEW,   /*123*/
           DADOS_TEF_OLD,   /*124*/
           DADOS_TEF_NEW,   /*125*/
           NOME_CLIENTE_OLD,   /*126*/
           NOME_CLIENTE_NEW,   /*127*/
           CRED_FLAG_OLD,   /*128*/
           CRED_FLAG_NEW,   /*129*/
           CRED_VAL_SOLIC_OLD,   /*130*/
           CRED_VAL_SOLIC_NEW,   /*131*/
           CRED_VAL_LIBER_OLD,   /*132*/
           CRED_VAL_LIBER_NEW,   /*133*/
           CRED_USU_LIBER_OLD,   /*134*/
           CRED_USU_LIBER_NEW,   /*135*/
           OBS_CREDITO_OLD,   /*136*/
           OBS_CREDITO_NEW,   /*137*/
           QTDE_VOLUMES_OLD,   /*138*/
           QTDE_VOLUMES_NEW,   /*139*/
           ESPECIE_VOLUME_OLD,   /*140*/
           ESPECIE_VOLUME_NEW,   /*141*/
           NUM_DAV_OLD,   /*142*/
           NUM_DAV_NEW,   /*143*/
           PERC_DESC_FINAL2_OLD,   /*146*/
           PERC_DESC_FINAL2_NEW,   /*147*/
           SELECIONADO_BAIXA_OLD,   /*148*/
           SELECIONADO_BAIXA_NEW    /*149*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.COD_EMPRESA, /*9*/
           0,/*10*/
           :new.DOCUMENTO, /*11*/
           0,/*12*/
           :new.CGC9, /*13*/
           0,/*14*/
           :new.CGC4, /*15*/
           0,/*16*/
           :new.CGC2, /*17*/
           0,/*18*/
           :new.TAB_COL, /*19*/
           0,/*20*/
           :new.TAB_MES, /*21*/
           0,/*22*/
           :new.TAB_SEQ, /*23*/
           0,/*24*/
           :new.COND_PAGTO, /*25*/
           0,/*26*/
           :new.VENDEDOR, /*27*/
           0,/*28*/
           :new.REPRESENTANTE, /*29*/
           0,/*30*/
           :new.VALOR_TOTAL, /*31*/
           0,/*32*/
           :new.VALOR_DESC, /*33*/
           0,/*34*/
           :new.PERC_DESC, /*35*/
           0,/*36*/
           :new.VALOR_LIQUIDO, /*37*/
           0,/*38*/
           :new.TIPO_VENDA, /*39*/
           '',/*40*/
           :new.SERIE, /*41*/
           0,/*42*/
           :new.DEPOSITO_LOJA, /*43*/
           0,/*44*/
           :new.NATU_LOJA, /*45*/
           '',/*46*/
           :new.ESTADO_NATU, /*47*/
           0,/*48*/
           :new.TRANSPORT9, /*49*/
           0,/*50*/
           :new.TRANSPORT4, /*51*/
           0,/*52*/
           :new.TRANSPORT2, /*53*/
           0,/*54*/
           :new.SELEC_CUPOM, /*55*/
           0,/*56*/
           :new.ULTIMA_SEQUENCIA, /*57*/
           0,/*58*/
           :new.PERC_COMIS_REPRES, /*59*/
           0,/*60*/
           :new.PERC_COMIS_VENDEDOR, /*61*/
           0,/*62*/
           :new.BANCO, /*63*/
           0,/*64*/
           :new.SEQ_END_ENTREGA, /*65*/
           0,/*66*/
           :new.BLOQUEIO_LIMITE, /*67*/
           0,/*68*/
           :new.CLI9RESPTIT, /*69*/
           0,/*70*/
           :new.CLI4RESPTIT, /*71*/
           0,/*72*/
           :new.CLI2RESPTIT, /*73*/
           0,/*74*/
           :new.TIPO_COMISSAO, /*75*/
           0,/*76*/
           :new.COD_FORMA_PGTO, /*77*/
           0,/*78*/
           :new.TIPO_COMIS_VEND, /*79*/
           0,/*80*/
           :new.SITUACAO, /*81*/
           0,/*82*/
           :new.COND_PAGTO_ORIG, /*83*/
           0,/*84*/
           :new.QUANTIDADE, /*85*/
           0,/*86*/
           :new.QUANTIDADE_ORIG, /*87*/
           0,/*88*/
           :new.GUIA, /*89*/
           null,/*90*/
           :new.DATA_EMISSAO, /*91*/
           0,/*92*/
           :new.TIPO_FRETE, /*93*/
           0,/*94*/
           :new.VALOR_FRETE, /*95*/
           null,/*96*/
           :new.DATA_FATURAMENTO, /*97*/
           0,/*98*/
           :new.PERC_DESC_FINAL, /*99*/
           0,/*100*/
           :new.VALOR_DESC_FINAL, /*101*/
           0,/*102*/
           :new.PERC_DESC_COND_PGTO, /*103*/
           0,/*104*/
           :new.PERC_ACRES_COND_PGTO, /*105*/
           0,/*106*/
           :new.QTDE_TROCA, /*107*/
           0,/*108*/
           :new.VALOR_TROCA, /*109*/
           0,/*110*/
           :new.PRAZO_MEDIO, /*111*/
           0,/*112*/
           :new.TIPO_TITULO, /*113*/
           null,/*114*/
           :new.DATA_BASE, /*115*/
           0,/*116*/
           :new.PERCENTUAL_VENDA, /*117*/
           0,/*118*/
           :new.NUM_INTERVENCAO, /*119*/
           0,/*120*/
           :new.NUM_CUPOM, /*121*/
           '',/*122*/
           :new.MAQUINA_USUARIO, /*123*/
           '',/*124*/
           :new.DADOS_TEF, /*125*/
           '',/*126*/
           :new.NOME_CLIENTE, /*127*/
           0,/*128*/
           :new.CRED_FLAG, /*129*/
           0,/*130*/
           :new.CRED_VAL_SOLIC, /*131*/
           0,/*132*/
           :new.CRED_VAL_LIBER, /*133*/
           '',/*134*/
           :new.CRED_USU_LIBER, /*135*/
           '',/*136*/
           :new.OBS_CREDITO, /*137*/
           0,/*138*/
           :new.QTDE_VOLUMES, /*139*/
           '',/*140*/
           :new.ESPECIE_VOLUME, /*141*/
           '',/*142*/
           :new.NUM_DAV, /*143*/
           0,/*144*/
           :new.PERC_DESC_FINAL2, /*147*/
           0,/*148*/
           :new.SELECIONADO_BAIXA /*149*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into loja_200_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_EMPRESA_OLD, /*8*/
           COD_EMPRESA_NEW, /*9*/
           DOCUMENTO_OLD, /*10*/
           DOCUMENTO_NEW, /*11*/
           CGC9_OLD, /*12*/
           CGC9_NEW, /*13*/
           CGC4_OLD, /*14*/
           CGC4_NEW, /*15*/
           CGC2_OLD, /*16*/
           CGC2_NEW, /*17*/
           TAB_COL_OLD, /*18*/
           TAB_COL_NEW, /*19*/
           TAB_MES_OLD, /*20*/
           TAB_MES_NEW, /*21*/
           TAB_SEQ_OLD, /*22*/
           TAB_SEQ_NEW, /*23*/
           COND_PAGTO_OLD, /*24*/
           COND_PAGTO_NEW, /*25*/
           VENDEDOR_OLD, /*26*/
           VENDEDOR_NEW, /*27*/
           REPRESENTANTE_OLD, /*28*/
           REPRESENTANTE_NEW, /*29*/
           VALOR_TOTAL_OLD, /*30*/
           VALOR_TOTAL_NEW, /*31*/
           VALOR_DESC_OLD, /*32*/
           VALOR_DESC_NEW, /*33*/
           PERC_DESC_OLD, /*34*/
           PERC_DESC_NEW, /*35*/
           VALOR_LIQUIDO_OLD, /*36*/
           VALOR_LIQUIDO_NEW, /*37*/
           TIPO_VENDA_OLD, /*38*/
           TIPO_VENDA_NEW, /*39*/
           SERIE_OLD, /*40*/
           SERIE_NEW, /*41*/
           DEPOSITO_LOJA_OLD, /*42*/
           DEPOSITO_LOJA_NEW, /*43*/
           NATU_LOJA_OLD, /*44*/
           NATU_LOJA_NEW, /*45*/
           ESTADO_NATU_OLD, /*46*/
           ESTADO_NATU_NEW, /*47*/
           TRANSPORT9_OLD, /*48*/
           TRANSPORT9_NEW, /*49*/
           TRANSPORT4_OLD, /*50*/
           TRANSPORT4_NEW, /*51*/
           TRANSPORT2_OLD, /*52*/
           TRANSPORT2_NEW, /*53*/
           SELEC_CUPOM_OLD, /*54*/
           SELEC_CUPOM_NEW, /*55*/
           ULTIMA_SEQUENCIA_OLD, /*56*/
           ULTIMA_SEQUENCIA_NEW, /*57*/
           PERC_COMIS_REPRES_OLD, /*58*/
           PERC_COMIS_REPRES_NEW, /*59*/
           PERC_COMIS_VENDEDOR_OLD, /*60*/
           PERC_COMIS_VENDEDOR_NEW, /*61*/
           BANCO_OLD, /*62*/
           BANCO_NEW, /*63*/
           SEQ_END_ENTREGA_OLD, /*64*/
           SEQ_END_ENTREGA_NEW, /*65*/
           BLOQUEIO_LIMITE_OLD, /*66*/
           BLOQUEIO_LIMITE_NEW, /*67*/
           CLI9RESPTIT_OLD, /*68*/
           CLI9RESPTIT_NEW, /*69*/
           CLI4RESPTIT_OLD, /*70*/
           CLI4RESPTIT_NEW, /*71*/
           CLI2RESPTIT_OLD, /*72*/
           CLI2RESPTIT_NEW, /*73*/
           TIPO_COMISSAO_OLD, /*74*/
           TIPO_COMISSAO_NEW, /*75*/
           COD_FORMA_PGTO_OLD, /*76*/
           COD_FORMA_PGTO_NEW, /*77*/
           TIPO_COMIS_VEND_OLD, /*78*/
           TIPO_COMIS_VEND_NEW, /*79*/
           SITUACAO_OLD, /*80*/
           SITUACAO_NEW, /*81*/
           COND_PAGTO_ORIG_OLD, /*82*/
           COND_PAGTO_ORIG_NEW, /*83*/
           QUANTIDADE_OLD, /*84*/
           QUANTIDADE_NEW, /*85*/
           QUANTIDADE_ORIG_OLD, /*86*/
           QUANTIDADE_ORIG_NEW, /*87*/
           GUIA_OLD, /*88*/
           GUIA_NEW, /*89*/
           DATA_EMISSAO_OLD, /*90*/
           DATA_EMISSAO_NEW, /*91*/
           TIPO_FRETE_OLD, /*92*/
           TIPO_FRETE_NEW, /*93*/
           VALOR_FRETE_OLD, /*94*/
           VALOR_FRETE_NEW, /*95*/
           DATA_FATURAMENTO_OLD, /*96*/
           DATA_FATURAMENTO_NEW, /*97*/
           PERC_DESC_FINAL_OLD, /*98*/
           PERC_DESC_FINAL_NEW, /*99*/
           VALOR_DESC_FINAL_OLD, /*100*/
           VALOR_DESC_FINAL_NEW, /*101*/
           PERC_DESC_COND_PGTO_OLD, /*102*/
           PERC_DESC_COND_PGTO_NEW, /*103*/
           PERC_ACRES_COND_PGTO_OLD, /*104*/
           PERC_ACRES_COND_PGTO_NEW, /*105*/
           QTDE_TROCA_OLD, /*106*/
           QTDE_TROCA_NEW, /*107*/
           VALOR_TROCA_OLD, /*108*/
           VALOR_TROCA_NEW, /*109*/
           PRAZO_MEDIO_OLD, /*110*/
           PRAZO_MEDIO_NEW, /*111*/
           TIPO_TITULO_OLD, /*112*/
           TIPO_TITULO_NEW, /*113*/
           DATA_BASE_OLD, /*114*/
           DATA_BASE_NEW, /*115*/
           PERCENTUAL_VENDA_OLD, /*116*/
           PERCENTUAL_VENDA_NEW, /*117*/
           NUM_INTERVENCAO_OLD, /*118*/
           NUM_INTERVENCAO_NEW, /*119*/
           NUM_CUPOM_OLD, /*120*/
           NUM_CUPOM_NEW, /*121*/
           MAQUINA_USUARIO_OLD, /*122*/
           MAQUINA_USUARIO_NEW, /*123*/
           DADOS_TEF_OLD, /*124*/
           DADOS_TEF_NEW, /*125*/
           NOME_CLIENTE_OLD, /*126*/
           NOME_CLIENTE_NEW, /*127*/
           CRED_FLAG_OLD, /*128*/
           CRED_FLAG_NEW, /*129*/
           CRED_VAL_SOLIC_OLD, /*130*/
           CRED_VAL_SOLIC_NEW, /*131*/
           CRED_VAL_LIBER_OLD, /*132*/
           CRED_VAL_LIBER_NEW, /*133*/
           CRED_USU_LIBER_OLD, /*134*/
           CRED_USU_LIBER_NEW, /*135*/
           OBS_CREDITO_OLD, /*136*/
           OBS_CREDITO_NEW, /*137*/
           QTDE_VOLUMES_OLD, /*138*/
           QTDE_VOLUMES_NEW, /*139*/
           ESPECIE_VOLUME_OLD, /*140*/
           ESPECIE_VOLUME_NEW, /*141*/
           NUM_DAV_OLD, /*142*/
           NUM_DAV_NEW, /*143*/
           PERC_DESC_FINAL2_OLD, /*146*/
           PERC_DESC_FINAL2_NEW, /*147*/
           SELECIONADO_BAIXA_OLD, /*148*/
           SELECIONADO_BAIXA_NEW  /*149*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.COD_EMPRESA,  /*8*/
           :new.COD_EMPRESA, /*9*/
           :old.DOCUMENTO,  /*10*/
           :new.DOCUMENTO, /*11*/
           :old.CGC9,  /*12*/
           :new.CGC9, /*13*/
           :old.CGC4,  /*14*/
           :new.CGC4, /*15*/
           :old.CGC2,  /*16*/
           :new.CGC2, /*17*/
           :old.TAB_COL,  /*18*/
           :new.TAB_COL, /*19*/
           :old.TAB_MES,  /*20*/
           :new.TAB_MES, /*21*/
           :old.TAB_SEQ,  /*22*/
           :new.TAB_SEQ, /*23*/
           :old.COND_PAGTO,  /*24*/
           :new.COND_PAGTO, /*25*/
           :old.VENDEDOR,  /*26*/
           :new.VENDEDOR, /*27*/
           :old.REPRESENTANTE,  /*28*/
           :new.REPRESENTANTE, /*29*/
           :old.VALOR_TOTAL,  /*30*/
           :new.VALOR_TOTAL, /*31*/
           :old.VALOR_DESC,  /*32*/
           :new.VALOR_DESC, /*33*/
           :old.PERC_DESC,  /*34*/
           :new.PERC_DESC, /*35*/
           :old.VALOR_LIQUIDO,  /*36*/
           :new.VALOR_LIQUIDO, /*37*/
           :old.TIPO_VENDA,  /*38*/
           :new.TIPO_VENDA, /*39*/
           :old.SERIE,  /*40*/
           :new.SERIE, /*41*/
           :old.DEPOSITO_LOJA,  /*42*/
           :new.DEPOSITO_LOJA, /*43*/
           :old.NATU_LOJA,  /*44*/
           :new.NATU_LOJA, /*45*/
           :old.ESTADO_NATU,  /*46*/
           :new.ESTADO_NATU, /*47*/
           :old.TRANSPORT9,  /*48*/
           :new.TRANSPORT9, /*49*/
           :old.TRANSPORT4,  /*50*/
           :new.TRANSPORT4, /*51*/
           :old.TRANSPORT2,  /*52*/
           :new.TRANSPORT2, /*53*/
           :old.SELEC_CUPOM,  /*54*/
           :new.SELEC_CUPOM, /*55*/
           :old.ULTIMA_SEQUENCIA,  /*56*/
           :new.ULTIMA_SEQUENCIA, /*57*/
           :old.PERC_COMIS_REPRES,  /*58*/
           :new.PERC_COMIS_REPRES, /*59*/
           :old.PERC_COMIS_VENDEDOR,  /*60*/
           :new.PERC_COMIS_VENDEDOR, /*61*/
           :old.BANCO,  /*62*/
           :new.BANCO, /*63*/
           :old.SEQ_END_ENTREGA,  /*64*/
           :new.SEQ_END_ENTREGA, /*65*/
           :old.BLOQUEIO_LIMITE,  /*66*/
           :new.BLOQUEIO_LIMITE, /*67*/
           :old.CLI9RESPTIT,  /*68*/
           :new.CLI9RESPTIT, /*69*/
           :old.CLI4RESPTIT,  /*70*/
           :new.CLI4RESPTIT, /*71*/
           :old.CLI2RESPTIT,  /*72*/
           :new.CLI2RESPTIT, /*73*/
           :old.TIPO_COMISSAO,  /*74*/
           :new.TIPO_COMISSAO, /*75*/
           :old.COD_FORMA_PGTO,  /*76*/
           :new.COD_FORMA_PGTO, /*77*/
           :old.TIPO_COMIS_VEND,  /*78*/
           :new.TIPO_COMIS_VEND, /*79*/
           :old.SITUACAO,  /*80*/
           :new.SITUACAO, /*81*/
           :old.COND_PAGTO_ORIG,  /*82*/
           :new.COND_PAGTO_ORIG, /*83*/
           :old.QUANTIDADE,  /*84*/
           :new.QUANTIDADE, /*85*/
           :old.QUANTIDADE_ORIG,  /*86*/
           :new.QUANTIDADE_ORIG, /*87*/
           :old.GUIA,  /*88*/
           :new.GUIA, /*89*/
           :old.DATA_EMISSAO,  /*90*/
           :new.DATA_EMISSAO, /*91*/
           :old.TIPO_FRETE,  /*92*/
           :new.TIPO_FRETE, /*93*/
           :old.VALOR_FRETE,  /*94*/
           :new.VALOR_FRETE, /*95*/
           :old.DATA_FATURAMENTO,  /*96*/
           :new.DATA_FATURAMENTO, /*97*/
           :old.PERC_DESC_FINAL,  /*98*/
           :new.PERC_DESC_FINAL, /*99*/
           :old.VALOR_DESC_FINAL,  /*100*/
           :new.VALOR_DESC_FINAL, /*101*/
           :old.PERC_DESC_COND_PGTO,  /*102*/
           :new.PERC_DESC_COND_PGTO, /*103*/
           :old.PERC_ACRES_COND_PGTO,  /*104*/
           :new.PERC_ACRES_COND_PGTO, /*105*/
           :old.QTDE_TROCA,  /*106*/
           :new.QTDE_TROCA, /*107*/
           :old.VALOR_TROCA,  /*108*/
           :new.VALOR_TROCA, /*109*/
           :old.PRAZO_MEDIO,  /*110*/
           :new.PRAZO_MEDIO, /*111*/
           :old.TIPO_TITULO,  /*112*/
           :new.TIPO_TITULO, /*113*/
           :old.DATA_BASE,  /*114*/
           :new.DATA_BASE, /*115*/
           :old.PERCENTUAL_VENDA,  /*116*/
           :new.PERCENTUAL_VENDA, /*117*/
           :old.NUM_INTERVENCAO,  /*118*/
           :new.NUM_INTERVENCAO, /*119*/
           :old.NUM_CUPOM,  /*120*/
           :new.NUM_CUPOM, /*121*/
           :old.MAQUINA_USUARIO,  /*122*/
           :new.MAQUINA_USUARIO, /*123*/
           :old.DADOS_TEF,  /*124*/
           :new.DADOS_TEF, /*125*/
           :old.NOME_CLIENTE,  /*126*/
           :new.NOME_CLIENTE, /*127*/
           :old.CRED_FLAG,  /*128*/
           :new.CRED_FLAG, /*129*/
           :old.CRED_VAL_SOLIC,  /*130*/
           :new.CRED_VAL_SOLIC, /*131*/
           :old.CRED_VAL_LIBER,  /*132*/
           :new.CRED_VAL_LIBER, /*133*/
           :old.CRED_USU_LIBER,  /*134*/
           :new.CRED_USU_LIBER, /*135*/
           :old.OBS_CREDITO,  /*136*/
           :new.OBS_CREDITO, /*137*/
           :old.QTDE_VOLUMES,  /*138*/
           :new.QTDE_VOLUMES, /*139*/
           :old.ESPECIE_VOLUME,  /*140*/
           :new.ESPECIE_VOLUME, /*141*/
           :old.NUM_DAV,  /*142*/
           :new.NUM_DAV, /*143*/
           :old.PERC_DESC_FINAL2,  /*146*/
           :new.PERC_DESC_FINAL2, /*147*/
           :old.SELECIONADO_BAIXA,  /*148*/
           :new.SELECIONADO_BAIXA  /*149*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into loja_200_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_EMPRESA_OLD, /*8*/
           COD_EMPRESA_NEW, /*9*/
           DOCUMENTO_OLD, /*10*/
           DOCUMENTO_NEW, /*11*/
           CGC9_OLD, /*12*/
           CGC9_NEW, /*13*/
           CGC4_OLD, /*14*/
           CGC4_NEW, /*15*/
           CGC2_OLD, /*16*/
           CGC2_NEW, /*17*/
           TAB_COL_OLD, /*18*/
           TAB_COL_NEW, /*19*/
           TAB_MES_OLD, /*20*/
           TAB_MES_NEW, /*21*/
           TAB_SEQ_OLD, /*22*/
           TAB_SEQ_NEW, /*23*/
           COND_PAGTO_OLD, /*24*/
           COND_PAGTO_NEW, /*25*/
           VENDEDOR_OLD, /*26*/
           VENDEDOR_NEW, /*27*/
           REPRESENTANTE_OLD, /*28*/
           REPRESENTANTE_NEW, /*29*/
           VALOR_TOTAL_OLD, /*30*/
           VALOR_TOTAL_NEW, /*31*/
           VALOR_DESC_OLD, /*32*/
           VALOR_DESC_NEW, /*33*/
           PERC_DESC_OLD, /*34*/
           PERC_DESC_NEW, /*35*/
           VALOR_LIQUIDO_OLD, /*36*/
           VALOR_LIQUIDO_NEW, /*37*/
           TIPO_VENDA_OLD, /*38*/
           TIPO_VENDA_NEW, /*39*/
           SERIE_OLD, /*40*/
           SERIE_NEW, /*41*/
           DEPOSITO_LOJA_OLD, /*42*/
           DEPOSITO_LOJA_NEW, /*43*/
           NATU_LOJA_OLD, /*44*/
           NATU_LOJA_NEW, /*45*/
           ESTADO_NATU_OLD, /*46*/
           ESTADO_NATU_NEW, /*47*/
           TRANSPORT9_OLD, /*48*/
           TRANSPORT9_NEW, /*49*/
           TRANSPORT4_OLD, /*50*/
           TRANSPORT4_NEW, /*51*/
           TRANSPORT2_OLD, /*52*/
           TRANSPORT2_NEW, /*53*/
           SELEC_CUPOM_OLD, /*54*/
           SELEC_CUPOM_NEW, /*55*/
           ULTIMA_SEQUENCIA_OLD, /*56*/
           ULTIMA_SEQUENCIA_NEW, /*57*/
           PERC_COMIS_REPRES_OLD, /*58*/
           PERC_COMIS_REPRES_NEW, /*59*/
           PERC_COMIS_VENDEDOR_OLD, /*60*/
           PERC_COMIS_VENDEDOR_NEW, /*61*/
           BANCO_OLD, /*62*/
           BANCO_NEW, /*63*/
           SEQ_END_ENTREGA_OLD, /*64*/
           SEQ_END_ENTREGA_NEW, /*65*/
           BLOQUEIO_LIMITE_OLD, /*66*/
           BLOQUEIO_LIMITE_NEW, /*67*/
           CLI9RESPTIT_OLD, /*68*/
           CLI9RESPTIT_NEW, /*69*/
           CLI4RESPTIT_OLD, /*70*/
           CLI4RESPTIT_NEW, /*71*/
           CLI2RESPTIT_OLD, /*72*/
           CLI2RESPTIT_NEW, /*73*/
           TIPO_COMISSAO_OLD, /*74*/
           TIPO_COMISSAO_NEW, /*75*/
           COD_FORMA_PGTO_OLD, /*76*/
           COD_FORMA_PGTO_NEW, /*77*/
           TIPO_COMIS_VEND_OLD, /*78*/
           TIPO_COMIS_VEND_NEW, /*79*/
           SITUACAO_OLD, /*80*/
           SITUACAO_NEW, /*81*/
           COND_PAGTO_ORIG_OLD, /*82*/
           COND_PAGTO_ORIG_NEW, /*83*/
           QUANTIDADE_OLD, /*84*/
           QUANTIDADE_NEW, /*85*/
           QUANTIDADE_ORIG_OLD, /*86*/
           QUANTIDADE_ORIG_NEW, /*87*/
           GUIA_OLD, /*88*/
           GUIA_NEW, /*89*/
           DATA_EMISSAO_OLD, /*90*/
           DATA_EMISSAO_NEW, /*91*/
           TIPO_FRETE_OLD, /*92*/
           TIPO_FRETE_NEW, /*93*/
           VALOR_FRETE_OLD, /*94*/
           VALOR_FRETE_NEW, /*95*/
           DATA_FATURAMENTO_OLD, /*96*/
           DATA_FATURAMENTO_NEW, /*97*/
           PERC_DESC_FINAL_OLD, /*98*/
           PERC_DESC_FINAL_NEW, /*99*/
           VALOR_DESC_FINAL_OLD, /*100*/
           VALOR_DESC_FINAL_NEW, /*101*/
           PERC_DESC_COND_PGTO_OLD, /*102*/
           PERC_DESC_COND_PGTO_NEW, /*103*/
           PERC_ACRES_COND_PGTO_OLD, /*104*/
           PERC_ACRES_COND_PGTO_NEW, /*105*/
           QTDE_TROCA_OLD, /*106*/
           QTDE_TROCA_NEW, /*107*/
           VALOR_TROCA_OLD, /*108*/
           VALOR_TROCA_NEW, /*109*/
           PRAZO_MEDIO_OLD, /*110*/
           PRAZO_MEDIO_NEW, /*111*/
           TIPO_TITULO_OLD, /*112*/
           TIPO_TITULO_NEW, /*113*/
           DATA_BASE_OLD, /*114*/
           DATA_BASE_NEW, /*115*/
           PERCENTUAL_VENDA_OLD, /*116*/
           PERCENTUAL_VENDA_NEW, /*117*/
           NUM_INTERVENCAO_OLD, /*118*/
           NUM_INTERVENCAO_NEW, /*119*/
           NUM_CUPOM_OLD, /*120*/
           NUM_CUPOM_NEW, /*121*/
           MAQUINA_USUARIO_OLD, /*122*/
           MAQUINA_USUARIO_NEW, /*123*/
           DADOS_TEF_OLD, /*124*/
           DADOS_TEF_NEW, /*125*/
           NOME_CLIENTE_OLD, /*126*/
           NOME_CLIENTE_NEW, /*127*/
           CRED_FLAG_OLD, /*128*/
           CRED_FLAG_NEW, /*129*/
           CRED_VAL_SOLIC_OLD, /*130*/
           CRED_VAL_SOLIC_NEW, /*131*/
           CRED_VAL_LIBER_OLD, /*132*/
           CRED_VAL_LIBER_NEW, /*133*/
           CRED_USU_LIBER_OLD, /*134*/
           CRED_USU_LIBER_NEW, /*135*/
           OBS_CREDITO_OLD, /*136*/
           OBS_CREDITO_NEW, /*137*/
           QTDE_VOLUMES_OLD, /*138*/
           QTDE_VOLUMES_NEW, /*139*/
           ESPECIE_VOLUME_OLD, /*140*/
           ESPECIE_VOLUME_NEW, /*141*/
           NUM_DAV_OLD, /*142*/
           NUM_DAV_NEW, /*143*/
           PERC_DESC_FINAL2_OLD, /*146*/
           PERC_DESC_FINAL2_NEW, /*147*/
           SELECIONADO_BAIXA_OLD, /*148*/
           SELECIONADO_BAIXA_NEW /*149*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.COD_EMPRESA, /*8*/
           0, /*9*/
           :old.DOCUMENTO, /*10*/
           0, /*11*/
           :old.CGC9, /*12*/
           0, /*13*/
           :old.CGC4, /*14*/
           0, /*15*/
           :old.CGC2, /*16*/
           0, /*17*/
           :old.TAB_COL, /*18*/
           0, /*19*/
           :old.TAB_MES, /*20*/
           0, /*21*/
           :old.TAB_SEQ, /*22*/
           0, /*23*/
           :old.COND_PAGTO, /*24*/
           0, /*25*/
           :old.VENDEDOR, /*26*/
           0, /*27*/
           :old.REPRESENTANTE, /*28*/
           0, /*29*/
           :old.VALOR_TOTAL, /*30*/
           0, /*31*/
           :old.VALOR_DESC, /*32*/
           0, /*33*/
           :old.PERC_DESC, /*34*/
           0, /*35*/
           :old.VALOR_LIQUIDO, /*36*/
           0, /*37*/
           :old.TIPO_VENDA, /*38*/
           0, /*39*/
           :old.SERIE, /*40*/
           '', /*41*/
           :old.DEPOSITO_LOJA, /*42*/
           0, /*43*/
           :old.NATU_LOJA, /*44*/
           0, /*45*/
           :old.ESTADO_NATU, /*46*/
           '', /*47*/
           :old.TRANSPORT9, /*48*/
           0, /*49*/
           :old.TRANSPORT4, /*50*/
           0, /*51*/
           :old.TRANSPORT2, /*52*/
           0, /*53*/
           :old.SELEC_CUPOM, /*54*/
           0, /*55*/
           :old.ULTIMA_SEQUENCIA, /*56*/
           0, /*57*/
           :old.PERC_COMIS_REPRES, /*58*/
           0, /*59*/
           :old.PERC_COMIS_VENDEDOR, /*60*/
           0, /*61*/
           :old.BANCO, /*62*/
           0, /*63*/
           :old.SEQ_END_ENTREGA, /*64*/
           0, /*65*/
           :old.BLOQUEIO_LIMITE, /*66*/
           0, /*67*/
           :old.CLI9RESPTIT, /*68*/
           0, /*69*/
           :old.CLI4RESPTIT, /*70*/
           0, /*71*/
           :old.CLI2RESPTIT, /*72*/
           0, /*73*/
           :old.TIPO_COMISSAO, /*74*/
           0, /*75*/
           :old.COD_FORMA_PGTO, /*76*/
           0, /*77*/
           :old.TIPO_COMIS_VEND, /*78*/
           0, /*79*/
           :old.SITUACAO, /*80*/
           0, /*81*/
           :old.COND_PAGTO_ORIG, /*82*/
           0, /*83*/
           :old.QUANTIDADE, /*84*/
           0, /*85*/
           :old.QUANTIDADE_ORIG, /*86*/
           0, /*87*/
           :old.GUIA, /*88*/
           0, /*89*/
           :old.DATA_EMISSAO, /*90*/
           null, /*91*/
           :old.TIPO_FRETE, /*92*/
           0, /*93*/
           :old.VALOR_FRETE, /*94*/
           0, /*95*/
           :old.DATA_FATURAMENTO, /*96*/
           null, /*97*/
           :old.PERC_DESC_FINAL, /*98*/
           0, /*99*/
           :old.VALOR_DESC_FINAL, /*100*/
           0, /*101*/
           :old.PERC_DESC_COND_PGTO, /*102*/
           0, /*103*/
           :old.PERC_ACRES_COND_PGTO, /*104*/
           0, /*105*/
           :old.QTDE_TROCA, /*106*/
           0, /*107*/
           :old.VALOR_TROCA, /*108*/
           0, /*109*/
           :old.PRAZO_MEDIO, /*110*/
           0, /*111*/
           :old.TIPO_TITULO, /*112*/
           0, /*113*/
           :old.DATA_BASE, /*114*/
           null, /*115*/
           :old.PERCENTUAL_VENDA, /*116*/
           0, /*117*/
           :old.NUM_INTERVENCAO, /*118*/
           0, /*119*/
           :old.NUM_CUPOM, /*120*/
           0, /*121*/
           :old.MAQUINA_USUARIO, /*122*/
           '', /*123*/
           :old.DADOS_TEF, /*124*/
           '', /*125*/
           :old.NOME_CLIENTE, /*126*/
           '', /*127*/
           :old.CRED_FLAG, /*128*/
           0, /*129*/
           :old.CRED_VAL_SOLIC, /*130*/
           0, /*131*/
           :old.CRED_VAL_LIBER, /*132*/
           0, /*133*/
           :old.CRED_USU_LIBER, /*134*/
           '', /*135*/
           :old.OBS_CREDITO, /*136*/
           '', /*137*/
           :old.QTDE_VOLUMES, /*138*/
           0, /*139*/
           :old.ESPECIE_VOLUME, /*140*/
           '', /*141*/
           :old.NUM_DAV, /*142*/
           '', /*143*/
           :old.PERC_DESC_FINAL2, /*146*/
           0, /*147*/
           :old.SELECIONADO_BAIXA, /*148*/
           0 /*149*/
         );
    end;
 end if;
end inter_tr_loja_200_log;
-- ALTER TRIGGER "INTER_TR_LOJA_200_LOG" ENABLE
 

/

exec inter_pr_recompile;

