alter table fatu_030 add (nr_autorizacao_opera  varchar2(100) default ' ',
                          cod_forma_pagto       number(2)     default 0);

comment on COLUMN fatu_030.nr_autorizacao_opera is 'Numero de autorizacao do cartao para a venda';
comment on COLUMN fatu_030.cod_forma_pagto is 'Indica a forma de pagamento';
/
exec inter_pr_recompile;
