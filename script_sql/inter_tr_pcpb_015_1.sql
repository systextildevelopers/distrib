
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_015_1" 
   before  update of data_termino
   on pcpb_015
for each row

  /*Esta trigger serve para atualizar a qtde produzida ou a qtde rejeitada
    no exec_010 */


declare
  v_nivel           pcpb_020.pano_sbg_nivel99%type;
  v_grupo           pcpb_020.pano_sbg_grupo%type;
  v_sub             pcpb_020.pano_sbg_subgrupo%type;
  v_item            pcpb_020.pano_sbg_item%type;
  v_turno           number;
  v_qtde020         pcpb_020.qtde_quilos_real%type;
  v_qtde020_prog    pcpb_020.qtde_quilos_prog%type;
  v_qtde_prod       pcpb_020.qtde_quilos_prog%type;
  v_qtde_reje       pcpb_020.qtde_quilos_prog%type;
  v_ccusto           basi_185.centro_custo%type;
  v_hi1             basi_185.horaini_t1%type;
  v_ht1             basi_185.horafim_t1%type;
  v_hi2             basi_185.horaini_t2%type;
  v_ht2             basi_185.horafim_t2%type;
  v_hi3             basi_185.horaini_t3%type;
  v_ht3             basi_185.horafim_t3%type;
  v_hi4             basi_185.horaini_t4%type;
  v_ht4             basi_185.horafim_t4%type;
  v_tturno          basi_185.tempo_turno1%type;
  v_tturno1         basi_185.tempo_turno1%type;
  v_tturno2         basi_185.tempo_turno2%type;
  v_tturno3         basi_185.tempo_turno3%type;
  v_tturno4         basi_185.tempo_turno4%type;
  v_capacidade_maq  mqop_060.capac_producao%type;
  v_tempo_producao  mqop_060.tempo_producao%type;
  v_qtde_capacidade exec_010.qtde_capacidade%type;
  v_registro_ok number;

begin
   if updating
   then

      --Verifica se a data nao e nula e se houve alteracao--
      if :old.data_termino is null and :new.data_termino is not null
      then

         --Verifica qual o centro de custo da maquina--
         begin
           select mqop_030.centro_custo into v_ccusto
           from mqop_030
           where mqop_030.maq_sub_grupo_mq = :new.grupo_maquina
             and mqop_030.maq_sub_sbgr_maq = :new.subgrupo_maquina
             and mqop_030.numero_maquina   = :new.subgrupo_maquina;
           exception
              when OTHERS
              then v_ccusto := 0;
         end;
         --Verifica o turno para o centro de custo--
         v_turno := 0;
         begin
            select   basi_185.horaini_t1,   basi_185.horafim_t1,
                     basi_185.horaini_t2,   basi_185.horafim_t2,
                     basi_185.horaini_t3,   basi_185.horafim_t3,
                     basi_185.horaini_t4,   basi_185.horafim_t4,
                     basi_185.tempo_turno1, basi_185.tempo_turno2,
                     basi_185.tempo_turno3, basi_185.tempo_turno4
            into v_hi1, v_ht1,
                 v_hi2, v_ht2,
                 v_hi3, v_ht3,
                 v_hi4, v_ht4,
                 v_tturno1,v_tturno2,
                 v_tturno3,v_tturno4
            from basi_185
            where basi_185.centro_custo = v_ccusto;
            exception
               when OTHERS
               then v_turno := 1;
                    v_tturno := 0;
         end;
         if v_turno = 0
         then
            if to_char(:new.hora_inicio,'HH24:MI') >= to_char(v_hi4,'HH24:MI') and to_char(:new.hora_inicio,'HH24:MI') <= to_char(v_ht4,'HH24:MI')
            then
               v_turno  := 4;
               v_tturno := v_tturno4;

            end if;
            if to_char(:new.hora_inicio,'HH24:MI') >= to_char(v_hi3,'HH24:MI') and to_char(:new.hora_inicio,'HH24:MI') <= to_char(v_ht3,'HH24:MI')
            then
               v_turno  := 3;
               v_tturno := v_tturno3;

            end if;
            if to_char(:new.hora_inicio,'HH24:MI') >= to_char(v_hi2,'HH24:MI') and to_char(:new.hora_inicio,'HH24:MI') <= to_char(v_ht2,'HH24:MI')
            then
               v_turno  := 2;
               v_tturno := v_tturno2;

            end if;
            if to_char(:new.hora_inicio,'HH24:MI') >= to_char(v_hi1,'HH24:MI') and to_char(:new.hora_inicio,'HH24:MI') <= to_char(v_ht1,'HH24:MI')
            then
               v_turno  := 1;
               v_tturno := v_tturno1;

            end if;

         end if;

         --Verifica produto e qtde produzida--
         begin
             select pcpb_020.qtde_quilos_real, pcpb_020.pano_sbg_nivel99,
                    pcpb_020.pano_sbg_grupo  , pcpb_020.pano_sbg_subgrupo,
                    pcpb_020.pano_sbg_item ,   pcpb_020.qtde_quilos_prog
             into   v_qtde020,  v_nivel,
                    v_grupo,    v_sub,
                    v_item,     v_qtde020_prog
             from pcpb_020
             where pcpb_020.ordem_producao   = :old.ordem_producao;
             exception
                when OTHERS
                then v_qtde020      := 0.00;
                     v_qtde020_prog := 0.00;

         end;

         if  v_qtde020 > 0.00
         then
            --Atualiza as quantidades--
            v_qtde_prod  := v_qtde020;
         else
            v_qtde_prod  := v_qtde020_prog;
         end if;

         if :new.motivo_rejeicao > 0
         then
            v_qtde_reje  := v_qtde_prod;
         else
            v_qtde_reje  := 0.00;
         end if;

         if v_qtde_prod > 0.00
         then
            /*Conforme foi passado a capacidade e o tempo sao atualizados
             somente na insersao */
            begin
               select mqop_060.capac_producao,  mqop_060.tempo_producao
               into v_capacidade_maq,          v_tempo_producao
               from mqop_060
               where  mqop_060.maq_sbgr_grupo_mq = :old.grupo_maquina
                 and  mqop_060.maq_sbgr_sbgr_maq = :old.subgrupo_maquina
                 and  mqop_060.nivel_estrutura   = v_nivel
                 and (mqop_060.grupo_estrutura   = v_grupo or mqop_060.grupo_estrutura = '00000');
                exception
                   when OTHERS
                   then   v_capacidade_maq := 0;
                          v_tempo_producao:= 0;
            end;

            if v_tempo_producao > 0
            then v_qtde_capacidade  := v_capacidade_maq / v_tempo_producao;
            else  v_qtde_capacidade := 0.000;
            end if;
            --Insere na tabela de acumulados--
            v_registro_ok := 0;

            begin
               INSERT INTO exec_010
               ( data_producao,          turno,
                 grupo_maquina,          subgrupo_maquina,
                 numero_maquina,         it_acumu_nivel99,
                 it_acumu_grupo,         it_acumu_subgrupo,
                 it_acumu_item,          qtde_produzida,
                 qtde_capacidade,        minutos_turno,
                 qtde_reprocessada)
                VALUES
               ( :new.data_termino,      v_turno,
                 :new.grupo_maquina,     :new.subgrupo_maquina,
                 :new.numero_maquina,     v_nivel,
                 v_grupo,                 v_sub,
                 v_item,                  v_qtde_prod,
                 v_qtde_capacidade,       v_tturno,
                 v_qtde_reje);
                exception
                   when OTHERS
                   then v_registro_ok := 1;
            end;

            --Se nao conseguiu inserir tenta dar update--

            if v_registro_ok = 1
            then

               begin
                  update exec_010
                     set qtde_produzida    = qtde_produzida    + v_qtde_prod,
                         qtde_reprocessada = qtde_reprocessada +  v_qtde_reje
                  where data_producao    = :new.data_termino
                   and  turno            = v_turno
                   and  grupo_maquina    = :new.grupo_maquina
                   and  subgrupo_maquina = :new.subgrupo_maquina
                   and  it_acumu_nivel99 = v_nivel
                   and  it_acumu_grupo   = v_grupo
                   and  it_acumu_subgrupo = v_sub
                   and  it_acumu_item     = v_item;
                   exception
                      when OTHERS
                       then raise_application_error (-20000, 'Nao atualizou a tabela de acumulados de maquina/produto (EXEC_010).');
               end;
            end if;
         end if;
      end if;/*data*/
   end if;/*update*/
end inter_tr_pcpb_015_1;



-- ALTER TRIGGER "INTER_TR_PCPB_015_1" ENABLE
 

/

exec inter_pr_recompile;

