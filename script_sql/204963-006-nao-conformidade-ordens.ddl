create table efic_307(  
cod_nao_conf number(6),
ordem_producao number(9),
constraint efic_307_pk primary key (cod_nao_conf, ordem_producao),
constraint fk_efic_307_pcpc_020	foreign key (ordem_producao) references pcpc_020 (ordem_producao)
);

comment on column efic_307.cod_nao_conf is 'Identificador da não conformidade';
comment on column efic_307.ordem_producao is 'Numero da ordem de produção';
comment on table efic_307 is 'Tabela de relacionamento de ordens de produção a não conformidade produtiva';

/	
