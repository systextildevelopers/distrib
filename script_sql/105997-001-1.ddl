ALTER TABLE INTE_067
DROP CONSTRAINT PK_INTE_067;

create index pk_inte_067 on inte_067 (CLIFINAN_CGC_CLI9);
drop index pk_inte_067;

ALTER TABLE INTE_067
ADD CONSTRAINT PK_INTE_067 PRIMARY KEY
(CLIFINAN_CGC_CLI9, CLIFINAN_CGC_CLI4, CLIFINAN_CGC_CLI2, FONFINAN_CGC9FON, FONFINAN_CGC4FON, FONFINAN_CGC2FON, NOME);

alter table pedi_067 drop constraint REF_PEDI_067_PEDI_065;
alter table pedi_065 drop constraint pk_pedi_065;
create index pk_pedi_065 on pedi_065 (cgc_9);
drop index pk_pedi_065;
alter table pedi_065 add constraint pk_pedi_065 primary key  (cgc_9,cgc_4,cgc_2,nome_fonte_refer);

alter table pedi_067 drop constraint pk_pedi_067;
create index pk_pedi_067 on pedi_067 (FONFINAN_CGC9FON);
drop index pk_pedi_067;

ALTER TABLE PEDI_067 ADD NOME VARCHAR2(40);

begin
  for reg in (select cgc_9,cgc_4,cgc_2,nome_fonte_refer from pedi_065)
  loop
    begin
      update pedi_067
      set nome = reg.nome_fonte_refer
      where pedi_067.FONFINAN_CGC9FON = reg.cgc_9
      and   pedi_067.FONFINAN_CGC4FON = reg.cgc_4
      and   pedi_067.FONFINAN_CGC2FON = reg.cgc_2;
      exception when others then null;
    end;
  end loop;
end;
/

commit;

alter table PEDI_067
  add constraint PK_PEDI_067 primary key (CLIFINAN_CGC_CLI9, CLIFINAN_CGC_CLI4, CLIFINAN_CGC_CLI2, FONFINAN_CGC9FON, FONFINAN_CGC4FON, FONFINAN_CGC2FON, NOME);

alter table PEDI_067
  add constraint REF_PEDI_067_PEDI_065 foreign key (FONFINAN_CGC9FON, FONFINAN_CGC4FON, FONFINAN_CGC2FON, NOME)
  references PEDI_065 (CGC_9, CGC_4, CGC_2, NOME_FONTE_REFER);

ALTER TABLE PEDI_067
DROP CONSTRAINT PK_PEDI_067;

ALTER TABLE PEDI_067
ADD CONSTRAINT PK_PEDI_067
PRIMARY KEY (CLIFINAN_CGC_CLI9, CLIFINAN_CGC_CLI4, CLIFINAN_CGC_CLI2, FONFINAN_CGC9FON, FONFINAN_CGC4FON, FONFINAN_CGC2FON, NOME);

exec inter_pr_recompile;
