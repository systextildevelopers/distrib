create or replace trigger bi_estq_420
before insert on estq_420
for each row
begin
    :new.id := estq_420_seq.nextval;
end;
