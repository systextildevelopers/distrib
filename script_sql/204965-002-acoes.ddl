create table pcpc_406(
    id number(4),
    constraint pcpc_406_pk primary key (id),
    descricao varchar2(120));

comment on column pcpc_406.id is 'Identificador da ação';
comment on column pcpc_406.descricao is 'Descrição da ação';
comment on table pcpc_406 is 'Tabela de cadastro de ações das ocorrências de encaixes do enfesto';

/
