create or replace trigger inter_tr_finalidade_ob
before insert on finalidade_ob
for each row
begin
   :new.codigo_finalidade := seq_finalidade_ob.nextval;
end;
