CREATE or replace procedure inter_pr_split_module(module in varchar2, programa out varchar2, codigo_empresa out number, usuario out varchar2) is
   i number(9);
   normalized_module varchar2(40);
BEGIN
   i := instr(module, ' ');
   if i = 0 then
      programa := module;
      codigo_empresa := 1;
      usuario := module;
   else
      programa := substr(module, 1, i-1);
      normalized_module := substr(module, i+1);
      i := instr(normalized_module, '-');
      if i = 0 then
         codigo_empresa := 1;
         usuario := normalized_module;
      else
         codigo_empresa := to_number(substr(normalized_module, 1, i-1));
         usuario := substr(normalized_module, i+1);
      end if;
   end if;
END;
/
 
