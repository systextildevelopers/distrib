create table pedi_489 (
	bloqueio           Number(2) not null,
	bloqueio_a_liberar Number(2) not null
);
alter table pedi_489 add constraint PK_pedi_489 primary key (bloqueio, bloqueio_a_liberar);
exec inter_pr_recompile;
