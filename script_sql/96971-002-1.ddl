insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpc_f047', 'Conferência de Partes', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpc_f047', 'basi_menu', 1, 20, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Conferência de Partes'
 where hdoc_036.codigo_programa = 'pcpc_f047'
   and hdoc_036.locale          = 'es_ES';
commit;
