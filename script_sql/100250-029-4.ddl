INSERT INTO hdoc_035
( codigo_programa, programa_menu, 
 item_menu_def,   descricao)
VALUES
( 'pcpb_l015',     0,
 0,        		   'Cadastro de Perfis de Est�gios para Reprocesso');
 
INSERT INTO hdoc_033
( usu_prg_cdusu, usu_prg_empr_usu, 
 programa,      nome_menu, 
 item_menu,     ordem_menu, 
 incluir,       modificar, 
 excluir,       procurar)
VALUES
( 'INTERSYS',    1, 
 'pcpb_l015',   'nenhum', 
 0,             1, 
 'N',           'S', 
 'N',           'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Cadastro de Perfis de Est�gios para Reprocesso'
 WHERE hdoc_036.codigo_programa = 'pcpb_l015'
   AND hdoc_036.locale          = 'es_ES';
  
commit work;
