
  CREATE OR REPLACE TRIGGER "INTER_TR_CPAG_168_LOG" 
after insert or delete or update
on cpag_168
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);  
     

 if inserting
 then
    begin

        insert into cpag_168_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           tcre_empresa_OLD,   /*8*/
           tcre_empresa_NEW,   /*9*/
           tcre_tipo_OLD,   /*10*/
           tcre_tipo_NEW,   /*11*/
           tcre_titulo_OLD,   /*12*/
           tcre_titulo_NEW,   /*13*/
           tcre_seq_OLD,   /*14*/
           tcre_seq_NEW,   /*15*/
           tcre_cnpj9_OLD,   /*16*/
           tcre_cnpj9_NEW,   /*17*/
           tcre_cnpj4_OLD,   /*18*/
           tcre_cnpj4_NEW,   /*19*/
           tcre_cnpj2_OLD,   /*20*/
           tcre_cnpj2_NEW,   /*21*/
           tpag_titulo_OLD,   /*22*/
           tpag_titulo_NEW,   /*23*/
           tpag_tipo_OLD,   /*24*/
           tpag_tipo_NEW,   /*25*/
           tpag_seq_OLD,   /*26*/
           tpag_seq_NEW,   /*27*/
           tpag_cnpj9_OLD,   /*28*/
           tpag_cnpj9_NEW,   /*29*/
           tpag_cnpj4_OLD,   /*30*/
           tpag_cnpj4_NEW,   /*31*/
           tpag_cnpj2_OLD,   /*32*/
           tpag_cnpj2_NEW,   /*33*/
           data_associacao_OLD,   /*34*/
           data_associacao_NEW,   /*35*/
           valor_usado_OLD,   /*36*/
           valor_usado_NEW,   /*37*/
           flag_OLD,   /*38*/
           flag_NEW,   /*39*/
           tcre_usado_OLD,   /*40*/
           tcre_usado_NEW,   /*41*/
           tpag_empresa_OLD,   /*42*/
           tpag_empresa_NEW    /*43*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.tcre_empresa, /*9*/
           0,/*10*/
           :new.tcre_tipo, /*11*/
           0,/*12*/
           :new.tcre_titulo, /*13*/
           0,/*14*/
           :new.tcre_seq, /*15*/
           0,/*16*/
           :new.tcre_cnpj9, /*17*/
           0,/*18*/
           :new.tcre_cnpj4, /*19*/
           0,/*20*/
           :new.tcre_cnpj2, /*21*/
           0,/*22*/
           :new.tpag_titulo, /*23*/
           0,/*24*/
           :new.tpag_tipo, /*25*/
           '',/*26*/
           :new.tpag_seq, /*27*/
           0,/*28*/
           :new.tpag_cnpj9, /*29*/
           0,/*30*/
           :new.tpag_cnpj4, /*31*/
           0,/*32*/
           :new.tpag_cnpj2, /*33*/
           null,/*34*/
           :new.data_associacao, /*35*/
           0,/*36*/
           :new.valor_usado, /*37*/
           0,/*38*/
           :new.flag, /*39*/
           0,/*40*/
           :new.tcre_usado, /*41*/
           0,/*42*/
           :new.tpag_empresa /*43*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into cpag_168_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           tcre_empresa_OLD, /*8*/
           tcre_empresa_NEW, /*9*/
           tcre_tipo_OLD, /*10*/
           tcre_tipo_NEW, /*11*/
           tcre_titulo_OLD, /*12*/
           tcre_titulo_NEW, /*13*/
           tcre_seq_OLD, /*14*/
           tcre_seq_NEW, /*15*/
           tcre_cnpj9_OLD, /*16*/
           tcre_cnpj9_NEW, /*17*/
           tcre_cnpj4_OLD, /*18*/
           tcre_cnpj4_NEW, /*19*/
           tcre_cnpj2_OLD, /*20*/
           tcre_cnpj2_NEW, /*21*/
           tpag_titulo_OLD, /*22*/
           tpag_titulo_NEW, /*23*/
           tpag_tipo_OLD, /*24*/
           tpag_tipo_NEW, /*25*/
           tpag_seq_OLD, /*26*/
           tpag_seq_NEW, /*27*/
           tpag_cnpj9_OLD, /*28*/
           tpag_cnpj9_NEW, /*29*/
           tpag_cnpj4_OLD, /*30*/
           tpag_cnpj4_NEW, /*31*/
           tpag_cnpj2_OLD, /*32*/
           tpag_cnpj2_NEW, /*33*/
           data_associacao_OLD, /*34*/
           data_associacao_NEW, /*35*/
           valor_usado_OLD, /*36*/
           valor_usado_NEW, /*37*/
           flag_OLD, /*38*/
           flag_NEW, /*39*/
           tcre_usado_OLD, /*40*/
           tcre_usado_NEW, /*41*/
           tpag_empresa_OLD, /*42*/
           tpag_empresa_NEW  /*43*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.tcre_empresa,  /*8*/
           :new.tcre_empresa, /*9*/
           :old.tcre_tipo,  /*10*/
           :new.tcre_tipo, /*11*/
           :old.tcre_titulo,  /*12*/
           :new.tcre_titulo, /*13*/
           :old.tcre_seq,  /*14*/
           :new.tcre_seq, /*15*/
           :old.tcre_cnpj9,  /*16*/
           :new.tcre_cnpj9, /*17*/
           :old.tcre_cnpj4,  /*18*/
           :new.tcre_cnpj4, /*19*/
           :old.tcre_cnpj2,  /*20*/
           :new.tcre_cnpj2, /*21*/
           :old.tpag_titulo,  /*22*/
           :new.tpag_titulo, /*23*/
           :old.tpag_tipo,  /*24*/
           :new.tpag_tipo, /*25*/
           :old.tpag_seq,  /*26*/
           :new.tpag_seq, /*27*/
           :old.tpag_cnpj9,  /*28*/
           :new.tpag_cnpj9, /*29*/
           :old.tpag_cnpj4,  /*30*/
           :new.tpag_cnpj4, /*31*/
           :old.tpag_cnpj2,  /*32*/
           :new.tpag_cnpj2, /*33*/
           :old.data_associacao,  /*34*/
           :new.data_associacao, /*35*/
           :old.valor_usado,  /*36*/
           :new.valor_usado, /*37*/
           :old.flag,  /*38*/
           :new.flag, /*39*/
           :old.tcre_usado,  /*40*/
           :new.tcre_usado, /*41*/
           :old.tpag_empresa,  /*42*/
           :new.tpag_empresa  /*43*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into cpag_168_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           tcre_empresa_OLD, /*8*/
           tcre_empresa_NEW, /*9*/
           tcre_tipo_OLD, /*10*/
           tcre_tipo_NEW, /*11*/
           tcre_titulo_OLD, /*12*/
           tcre_titulo_NEW, /*13*/
           tcre_seq_OLD, /*14*/
           tcre_seq_NEW, /*15*/
           tcre_cnpj9_OLD, /*16*/
           tcre_cnpj9_NEW, /*17*/
           tcre_cnpj4_OLD, /*18*/
           tcre_cnpj4_NEW, /*19*/
           tcre_cnpj2_OLD, /*20*/
           tcre_cnpj2_NEW, /*21*/
           tpag_titulo_OLD, /*22*/
           tpag_titulo_NEW, /*23*/
           tpag_tipo_OLD, /*24*/
           tpag_tipo_NEW, /*25*/
           tpag_seq_OLD, /*26*/
           tpag_seq_NEW, /*27*/
           tpag_cnpj9_OLD, /*28*/
           tpag_cnpj9_NEW, /*29*/
           tpag_cnpj4_OLD, /*30*/
           tpag_cnpj4_NEW, /*31*/
           tpag_cnpj2_OLD, /*32*/
           tpag_cnpj2_NEW, /*33*/
           data_associacao_OLD, /*34*/
           data_associacao_NEW, /*35*/
           valor_usado_OLD, /*36*/
           valor_usado_NEW, /*37*/
           flag_OLD, /*38*/
           flag_NEW, /*39*/
           tcre_usado_OLD, /*40*/
           tcre_usado_NEW, /*41*/
           tpag_empresa_OLD, /*42*/
           tpag_empresa_NEW /*43*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.tcre_empresa, /*8*/
           0, /*9*/
           :old.tcre_tipo, /*10*/
           0, /*11*/
           :old.tcre_titulo, /*12*/
           0, /*13*/
           :old.tcre_seq, /*14*/
           0, /*15*/
           :old.tcre_cnpj9, /*16*/
           0, /*17*/
           :old.tcre_cnpj4, /*18*/
           0, /*19*/
           :old.tcre_cnpj2, /*20*/
           0, /*21*/
           :old.tpag_titulo, /*22*/
           0, /*23*/
           :old.tpag_tipo, /*24*/
           0, /*25*/
           :old.tpag_seq, /*26*/
           '', /*27*/
           :old.tpag_cnpj9, /*28*/
           0, /*29*/
           :old.tpag_cnpj4, /*30*/
           0, /*31*/
           :old.tpag_cnpj2, /*32*/
           0, /*33*/
           :old.data_associacao, /*34*/
           null, /*35*/
           :old.valor_usado, /*36*/
           0, /*37*/
           :old.flag, /*38*/
           0, /*39*/
           :old.tcre_usado, /*40*/
           0, /*41*/
           :old.tpag_empresa, /*42*/
           0 /*43*/
         );
    end;
 end if;
end inter_tr_cpag_168_log;

-- ALTER TRIGGER "INTER_TR_CPAG_168_LOG" ENABLE
 

/

exec inter_pr_recompile;

