alter table proj_010 add (
  NIVEL_ITEM VARCHAR2(1), 
  GRUPO_ITEM VARCHAR2(5), 
  SUBGRUPO_ITEM VARCHAR2(3), 
  ITEM_ITEM VARCHAR2(6), 
  ALT_ITEM NUMBER(2,0)
);

alter table proj_010 modify REGISTRO_ORIGEM VARCHAR2(4000);

declare
  v_existe number := 0;
begin

  rollback;
  
  begin
    select count(1) 
        into v_existe
        from all_cons_columns 
    where constraint_name = 'PK_PROJ_010'
    and UPPER(column_name) = 'ALTERNATIVA';
  exception when others then
    v_existe := 0;
  end;
  
  if v_existe = 0 then    --Não tem alternativa na PK, incluir
    
        begin
            execute immediate 'alter table proj_010 drop constraint PK_PROJ_010';
        exception when others then null; end;
        commit;
        
        begin
            execute immediate 'drop index PK_PROJ_010';
        exception when others then null; end;
        commit;

        begin
            execute immediate 'alter table proj_010 add CONSTRAINT PK_PROJ_010 PRIMARY KEY (NUMERO_PROJETO, NIVEL, GRUPO, SUBGRUPO, ITEM, ALTERNATIVA, ROTEIRO)';
        exception when others then null; end;
		    commit;
	end if;
		
	
end;
/
