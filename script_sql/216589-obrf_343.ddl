create table obrf_343 (
    id            number(9)                  not null,
    id_devolucao  number(9)                  not null,
    status        number(1)  default 0       not null,
    tipo_envio    number(1)                  not null,
    data_criacao  date       default sysdate not null,
    data_envio    date
);

alter table obrf_343 add constraint pk_obrf_343 primary key (id);
alter table obrf_343 add constraint fk_obrf_343_id_devolucao foreign key (id_devolucao) references obrf_341 (id) on delete cascade;

comment on table obrf_343 is 'Integra��o Devolu��o E-Commerce - Envio';
comment on column obrf_343.status is '0 - N�o enviado / 1 - Enviado';
comment on column obrf_343.tipo_envio is '1 - Emiss�o / 2 - Cancelamento';

create sequence id_obrf_343;

exec inter_pr_recompile;
/
