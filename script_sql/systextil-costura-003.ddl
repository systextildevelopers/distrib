insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('tmrp_f763', 'Painel de Planejamento de Produtos para Requisição de Compras', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'tmrp_f763', 'pmenu_ad46', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'tmrp_f763', 'menu_ad46', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Painel de Planejamento de Produtos para Requisição de Compras'
 where hdoc_036.codigo_programa = 'tmrp_f763'
   and hdoc_036.locale          = 'es_ES';
commit;

