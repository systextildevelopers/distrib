
  CREATE OR REPLACE PROCEDURE "INTER_PR_PEDI_845" 
(p_nivel             in pedi_110.cd_it_pe_nivel99%type,
 p_grupo             in pedi_110.cd_it_pe_grupo%type,
 p_subgrupo          in pedi_110.cd_it_pe_subgrupo%type,
 p_item              in pedi_110.cd_it_pe_item%type,
 p_lote              in pedi_110.lote_empenhado%type,
 p_deposito          in pedi_110.codigo_deposito%type,
 p_pedido            in pedi_110.pedido_venda%type,
 p_seq_pedido        in pedi_110.seq_item_pedido%type ,
 p_qtde_pedida       in pedi_110.qtde_pedida%type,
 p_qtde_afaturar     in pedi_110.qtde_afaturar%type,
 p_qtde_faturada     in pedi_110.qtde_faturada%type)
is
   v_qtde_disponivel number;
   v_qtde_excedida   number;
   v_saldo           number;
   v_qtde_empenhada  number;
   v_reg             number;
begin
   -- Se o nivel for = 2 ou 4 verifica se existe quantidade disponivel no estoque
   if p_nivel = '2' or p_nivel = '4'
   then
      begin
         select estq_040.qtde_estoque_atu - estq_040.qtde_empenhada,
                estq_040.qtde_empenhada
         into   v_qtde_disponivel,
                v_qtde_empenhada
         from estq_040
         where cditem_nivel99  = p_nivel
           and cditem_grupo    = p_grupo
           and cditem_subgrupo = p_subgrupo
           and cditem_item     = p_item
           and deposito        = p_deposito
           and lote_acomp      = p_lote;

         exception
            when no_data_found
            then
               v_qtde_disponivel := 0.000;
               v_qtde_empenhada  := 0.000;
      end;

      if Trunc(v_qtde_disponivel, 3) < Trunc(v_qtde_empenhada, 3)
      then

         v_saldo := trunc(p_qtde_pedida,3) - trunc(p_qtde_faturada,3);

         v_qtde_excedida := Trunc(p_qtde_afaturar, 3) - Trunc(v_saldo, 3);

         select count(*) into v_reg
         from pedi_845
         where pedi_845.pedido    = p_pedido
           and pedi_845.sequencia = p_seq_pedido
           and pedi_845.nivel     = p_nivel
           and pedi_845.grupo     = p_grupo
           and pedi_845.subgrupo  = p_subgrupo
           and pedi_845.item      = p_item
           and pedi_845.deposito  = p_deposito
           and pedi_845.lote      = p_lote;

         if v_reg = 0
         then

            insert into pedi_845
              (pedido,              sequencia,
	             nivel,               grupo,
               subgrupo,  	    item,
               deposito,            lote,
               qtde_excedida,       data_excesso)
            values
              (p_pedido,            p_seq_pedido,
               p_nivel,             p_grupo,
               p_subgrupo,          p_item,
               p_deposito,          p_lote,
               v_qtde_excedida,     trunc(sysdate,'DD'));

         end if;
      end if;
   end if;
end inter_pr_pedi_845;
 

/

exec inter_pr_recompile;

