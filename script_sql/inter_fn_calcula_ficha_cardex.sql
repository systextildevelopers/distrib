CREATE OR REPLACE FUNCTION inter_fn_calcula_ficha_cardex
   (p_inc_exc           in number,   p_empresa1           in number,
    p_empresa2          in number,   p_empresa3           in number,
    p_empresa4          in number,   p_empresa5           in number,
    p_nivel_prod        in varchar2, p_grupo_prod         in varchar2,
    p_nivel_prod1       in varchar2, p_grupo_prod1        in varchar2,
    p_nivel_prod2       in varchar2, p_grupo_prod2        in varchar2,
    p_nivel_prod3       in varchar2, p_grupo_prod3        in varchar2,
    p_conta_estoque     in number,   p_data_inicial_ficha in date,
    p_data_final_ficha   in date,    p_origem_custo_fabr  in number,
    p_situacao_depositos in number,  p_valorizacao        in number)
return varchar2
is
   erro_mensagem          varchar2(250) := ' ';
   v_periodo_estoque      date;
   v_forma_calculo_cardex empr_002.forma_calculo_cardex%type;
   v_estq_300_estq_310    varchar2(10);

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(20);
   ws_locale_usuario         varchar2(5);

begin


   begin
      erro_mensagem := inter_fn_calc_ficha_cardex_rec(p_inc_exc,
                                       p_empresa1,
                                       p_empresa2,
                                       p_empresa3,
                                       p_empresa4,
                                       p_empresa5,
                                       p_nivel_prod,
                                       p_grupo_prod,
                                       p_nivel_prod1,
                                       p_grupo_prod1,
                                       p_nivel_prod2,
                                       p_grupo_prod2,
                                       p_nivel_prod3,
                                       p_grupo_prod3,
                                       p_conta_estoque,
                                       p_data_inicial_ficha,
                                       p_data_final_ficha,
                                       p_origem_custo_fabr,
                                       p_situacao_depositos,
                                       p_valorizacao,
                                       'XXXX',
                                       'XXXXXXX');


      exception
         when others then
            erro_mensagem := SQLERRM;
   end;
   return(erro_mensagem);
end inter_fn_calcula_ficha_cardex;

/

exec inter_pr_recompile;

