create table estq_425 (
  id number primary key not null,
  id_arquivo number references estq_420(id) not null,
  cnpj_cpf_9 number(9) not null,
  cnpj_cpf_4 number(4) not null,
  cnpj_cpf_2 number(2) not null,
  nota_fiscal number(9) not null,
  serie_fiscal_sai varchar2(3) not null,
  nota_transferencia	NUMBER(9,0),
  serie_transferencia	VARCHAR2(3),
  codigo_rolo varchar2(40) not null,
  turno_producao number(1) not null,
  cod_nuance varchar2(3) not null,
  numero_lote varchar2(15) not null,
  panoacab_nivel99 varchar2(1) not null,
  panoacab_grupo varchar2(5) not null,
  panoacab_subgrupo varchar2(3) not null,
  panoacab_item varchar2(6) not null,
  endereco varchar2(10),
  qtde_quilos_acab number(9, 3) not null,
  peso_bruto number(9, 3) not null,
  peso_liquido_real number(9, 3) not null,
  tara number(7, 4) not null,
  qualidade_rolo number(1) check(qualidade_rolo in (1, 2)) not null,
  rolada number(2) default 0,
  seq_rolada number(2) default 0,
  sequencia_tingimento number(5) default 0,
  sequencia_corte number(3) not null,
  largura number(7, 3) not null,
  gramatura number(6, 3) not null,
  pontos_qualidade number(3) not null,
  pontos_qualidade_m2 number(18,3) default 0 not null,
  hora_inicio timestamp not null,
  hora_termino timestamp not null,
  conferido number default 0 check(conferido in (0, 1)) not null,
  data_conferencia timestamp,
  usuario_conferencia varchar2(250),
  integrado number default 0 check(integrado in (0, 1)) not null,
  data_integracao timestamp,
  usuario_integracao varchar2(250),
  codigo_rolo_integrado number(9)
);

comment on column estq_425.id is 'Chave primaria';
comment on column estq_425.id_arquivo is 'Chave estrangeira que referencia o arquivo de importação (estq_420)';
comment on column estq_425.cnpj_cpf_9 is 'Matriz do CNPJ ou CPF';
comment on column estq_425.cnpj_cpf_4 is 'Número de filial do CNPJ. Se for CPF preencher com zero.';
comment on column estq_425.cnpj_cpf_2 is 'Dígito verificador do CNPJ ou CPF';
comment on column estq_425.nota_fiscal is 'Número da nota fiscal de industrialização';
comment on column estq_425.serie_fiscal_sai is 'Série da nota fiscal de industrialização';
comment on column estq_425.codigo_rolo is 'Código do rolo gerado no fornecedor';
comment on column estq_425.turno_producao is 'Código do turno de produção';
comment on column estq_425.cod_nuance is 'Código da nuance do rolo';
comment on column estq_425.numero_lote is 'Número da ordem de produção no fornecedor';
comment on column estq_425.panoacab_nivel99 is 'Nível do produto: 2 para tecidos';
comment on column estq_425.panoacab_grupo is 'Grupo do produto na Capricornio';
comment on column estq_425.panoacab_subgrupo is 'Subgrupo do produto na Capricornio';
comment on column estq_425.panoacab_item is 'Item do produto na Capricornio';
comment on column estq_425.endereco is 'Endereço do produto na Capricornio';
comment on column estq_425.qtde_quilos_acab is 'Quantidade produzida';
comment on column estq_425.peso_bruto is 'Peso bruto do rolo com embalagem';
comment on column estq_425.peso_liquido_real is 'Peso líquido do rolo sem embalagem';
comment on column estq_425.tara is 'Peso da embalagem';
comment on column estq_425.qualidade_rolo is '1 Primeira qualidade; 2 segunda qualidade';
comment on column estq_425.rolada is 'Número da rolada do tingimento';
comment on column estq_425.seq_rolada is 'Sequência da rolada no tingimento';
comment on column estq_425.sequencia_tingimento is 'Sequência no tingimento';
comment on column estq_425.sequencia_corte is 'Sequência de corte no processo de revisão';
comment on column estq_425.largura is 'Largura do rolo no processo de revisão';
comment on column estq_425.gramatura is 'Gramatura do rolo no processo de revisão';
comment on column estq_425.pontos_qualidade is 'Quantidade de pontos conforme ABNT NBR 13484';
comment on column estq_425.pontos_qualidade_m2 is 'Pontos por M2';
comment on column estq_425.hora_inicio is 'Data e hora de início da produção do rolo';
comment on column estq_425.hora_termino is 'Data e hora do término da produção do rolo';
comment on column estq_425.conferido is 'Flag se o registro foi conferido ou não (0 - Não, 1 - Sim)';
comment on column estq_425.data_conferencia is 'Data em que o registro foi conferido';
comment on column estq_425.usuario_conferencia is 'Usuario que realizou a conferencia';
comment on column estq_425.integrado is 'Flag se o registro foi integrado nas tabelas oficiais ou não (0 - Não, 1 - Sim)';
comment on column estq_425.data_integracao is 'Data em que o registro foi integrado nas tabelas oficiais';
comment on column estq_425.usuario_integracao is 'Usuario que realizou a integração nas tabelas oficiais';
comment on column estq_425.codigo_rolo_integrado is 'Código do rolo integrado no Systêxtil (pcpt_020)';

create sequence estq_425_seq;
