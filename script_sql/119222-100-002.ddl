alter table efic_090 add ordem_producao number(9);

comment on column efic_090.ordem_producao is 'Ordem de produção que estava sendo executada quando foi feita a parada (utilizado inicialmente apenas no inte_f665)';

exec inter_pr_recompile;
