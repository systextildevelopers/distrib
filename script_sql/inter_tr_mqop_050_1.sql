
  CREATE OR REPLACE TRIGGER "INTER_TR_MQOP_050_1" 
   after insert
      or delete
      or update of MINUTOS
   on mqop_050
   for each row

begin
    if (updating or inserting) and :old.minutos <> :new.minutos
    and :new.nivel_estrutura = '1'
    then
        -- LOOP para buscar todas as ordens de producao que tenham um estagio para enderecar
        -- Cuja ordem de producao ainda nao foi produzida na maquina
        for reg_mqop_050 in (select estq_450.ordem_producao
                             from mqop_040, estq_450, mqop_005, pcpc_020
                             where mqop_040.grupo_maquinas      = estq_450.grupo_maquina
                               and mqop_040.sub_maquina         = estq_450.subgrupo_maquina
                               and mqop_040.codigo_operacao     = :new.codigo_operacao
                               and mqop_040.pede_produto        = 2
                               and mqop_005.codigo_estagio      = estq_450.estagio
                               and mqop_005.endereco_automatico =  'S'
                               and pcpc_020.ordem_producao      = estq_450.ordem_producao
                               and pcpc_020.cod_cancelamento    = 0
                               and pcpc_020.referencia_peca     = :new.grupo_estrutura
                               and pcpc_020.alternativa_peca    = :new.numero_alternati
                               and pcpc_020.roteiro_peca        = :new.numero_roteiro
                               and estq_450.area_producao       = 1
                               and estq_450.referencia          = pcpc_020.referencia_peca
                               and estq_450.estagio             = :new.codigo_estagio
                               and estq_450.grupo_maquina || '.' ||
                                   estq_450.subgrupo_maquina || '.' ||
                                   to_char(estq_450.numero_maquina,'00000') <> estq_450.endereco
                             group by estq_450.ordem_producao)
        loop
            -- Alteracao do tempo
            begin
               update estq_450
                  set estq_450.minutos_maquina = estq_450.minutos_maquina - :old.minutos + :new.minutos
                where estq_450.ordem_producao  = reg_mqop_050.ordem_producao
                  and estq_450.estagio         = :new.codigo_estagio;
            exception when others then
               raise_application_error(-20000,'Nao atulizou os dados da tabela estq_450.');
            end;
        end loop;
    end if;

    if deleting and :new.nivel_estrutura = '1'
    then
        -- LOOP para buscar todas as ordens de producao que tenham um estagio para enderecar
        -- Cuja ordem de producao ainda nao foi produzida na maquina
        for reg_mqop_050 in (select estq_450.ordem_producao
                             from mqop_040, estq_450, mqop_005, pcpc_020
                             where mqop_040.grupo_maquinas      = estq_450.grupo_maquina
                               and mqop_040.sub_maquina         = estq_450.subgrupo_maquina
                               and mqop_040.codigo_operacao     = :old.codigo_operacao
                               and mqop_040.pede_produto        = 2
                               and mqop_005.codigo_estagio      = estq_450.estagio
                               and mqop_005.endereco_automatico =  'S'
                               and pcpc_020.ordem_producao      = estq_450.ordem_producao
                               and pcpc_020.cod_cancelamento    = 0
                               and pcpc_020.referencia_peca     = :old.grupo_estrutura
                               and pcpc_020.alternativa_peca    = :old.numero_alternati
                               and pcpc_020.roteiro_peca        = :old.numero_roteiro
                               and estq_450.area_producao       = 1
                               and estq_450.referencia          = pcpc_020.referencia_peca
                               and estq_450.estagio             = :old.codigo_estagio
                               and estq_450.grupo_maquina || '.' ||
                                   estq_450.subgrupo_maquina || '.' ||
                                   to_char(estq_450.numero_maquina,'00000') <> estq_450.endereco
                             group by estq_450.ordem_producao)
        loop
            -- Alteracao do tempo
            begin
               update estq_450
                  set estq_450.minutos_maquina = estq_450.minutos_maquina - :old.minutos
                where estq_450.ordem_producao  = reg_mqop_050.ordem_producao
                  and estq_450.estagio         = :old.codigo_estagio;
            exception when others then
               raise_application_error(-20000,'Nao atulizou(1) os dados da tabela estq_450.');
            end;
        end loop;
    end if;
end inter_tr_mqop_050_1;


-- ALTER TRIGGER "INTER_TR_MQOP_050_1" ENABLE
 

/

exec inter_pr_recompile;

