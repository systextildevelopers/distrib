
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_636_ID" 
before insert on tmrp_636
  for each row

declare
   v_nr_registro number;

begin
   select seq_tmrp_636.nextval into v_nr_registro from dual;

   :new.numero_id := v_nr_registro;

end inter_tr_tmrp_636_id;

-- ALTER TRIGGER "INTER_TR_TMRP_636_ID" ENABLE
 

/

exec inter_pr_recompile;

