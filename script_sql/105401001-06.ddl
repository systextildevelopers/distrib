alter table pedi_100 add (nr_autorizacao_opera  varchar2(100));


comment on COLUMN pedi_100.nr_autorizacao_opera is 'Numero de autorizacao do cartao para a venda';

alter table pedi_100
modify nr_autorizacao_opera default ' ';
/
exec inter_pr_recompile;
