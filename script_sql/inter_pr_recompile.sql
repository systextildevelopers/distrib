
  CREATE OR REPLACE PROCEDURE "INTER_PR_RECOMPILE" 
is
   v_erro number := 0;
begin
   --verifica e compila procedures que nao estejam compiladas
   for reg_proc in  (select * from all_objects
                     where all_objects.object_type  = 'PROCEDURE'
                       and all_objects.status       = 'INVALID')
   loop
      begin
         execute immediate 'alter procedure ' || reg_proc.object_name || ' compile';
      EXCEPTION
         when others then
            v_erro := 1;
      end;
   end loop;

   --verifica e compila funcoes que nao estejam compiladas
   for reg_func in  (select * from all_objects
                     where all_objects.object_type  = 'FUNCTION'
                       and all_objects.status       = 'INVALID')
   loop
      begin
         execute immediate 'alter function ' || reg_func.object_name || ' compile';
      EXCEPTION
         when others then
            v_erro := 1;
      end;
   end loop;

   --verifica e compila triggers que nao estejam compiladas
   for reg_trig in  (select * from all_objects
                     where all_objects.object_type  = 'TRIGGER'
                       and all_objects.status       = 'INVALID')
   loop
      begin
         execute immediate 'alter trigger ' || reg_trig.object_name || ' compile';
      EXCEPTION
         when others then
            v_erro := 1;
      end;
   end loop;

   --verifica e compila views que nao estejam compiladas
   for reg_func in  (select * from all_objects
                     where all_objects.object_type  = 'VIEW'
                       and all_objects.status       = 'INVALID')
   loop
      begin
         execute immediate 'alter view ' || reg_func.object_name || ' compile';
      EXCEPTION
         when others then
            v_erro := 1;
      end;
   end loop;

   --verifica e compila materialized views que nao estejam compiladas
   for reg_func in  (select * from all_objects
                     where all_objects.object_type  = 'MATERIALIZED VIEW'
                       and all_objects.status       = 'INVALID')
   loop
      begin
         execute immediate 'alter materialized view ' || reg_func.object_name || ' compile';
      EXCEPTION
         when others then
            v_erro := 1;
      end;
   end loop;

    --verifica e compila package body que nao estejam compiladas
   for reg_func in  (select * from all_objects
                     where all_objects.object_type  = 'PACKAGE BODY'
                       and all_objects.status       = 'INVALID')
   loop
      begin
         execute immediate 'alter package ' || reg_func.object_name || ' compile body';
      EXCEPTION
         when others then
            v_erro := 1;
      end;
   end loop;

end inter_pr_recompile;
 

/

exec inter_pr_recompile;

