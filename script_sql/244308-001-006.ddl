CREATE TABLE  PROJ_050 
   (	SEQ_IMPORTACAO_TECELAGEM NUMBER(30,0) NOT NULL ENABLE, 
	CODIGO_EMPRESA NUMBER(3,0) NOT NULL ENABLE, 
	DATA_IMPORTACAO DATE NOT NULL ENABLE, 
	TECELAGEM VARCHAR2(10) NOT NULL ENABLE, 
	TEAR VARCHAR2(30) NOT NULL ENABLE, 
	FAMILIA VARCHAR2(30) NOT NULL ENABLE, 
	CODIGO_ARTIGO VARCHAR2(18) NOT NULL ENABLE, 
	NIVEL VARCHAR2(1) NOT NULL ENABLE, 
	GRUPO VARCHAR2(5) NOT NULL ENABLE, 
	SUBGRUPO VARCHAR2(3) NOT NULL ENABLE, 
	ITEM VARCHAR2(6) NOT NULL ENABLE, 
	TINGIMENTO VARCHAR2(150), 
	LIGAMENTO VARCHAR2(30), 
	QTDE_RPM NUMBER(10,0), 
	QTDE_BATIDAS NUMBER(9,2), 
	TAXA_OCUPACAO_PENTE NUMBER(5,2), 
	INICIO NUMBER(5,2), 
	FIM NUMBER(5,2), 
	MEDIA NUMBER(5,2), 
	PERCENTUAL_REAL NUMBER(5,2), 
	PERCENTUAL_NOMINAL NUMBER(5,2), 
	QTDE_METRO_MES NUMBER(9,3), 
	USUARIO_ATUALIZACAO VARCHAR2(30) NOT NULL ENABLE, 
	DATA_ATUALIZACAO DATE NOT NULL ENABLE, 
	TECELAGEM_TEAR VARCHAR2(40), 
	 CONSTRAINT PK_PROJ_050 PRIMARY KEY (SEQ_IMPORTACAO_TECELAGEM)
  USING INDEX  ENABLE, 
	 CONSTRAINT UK_PROJ_050 UNIQUE (CODIGO_EMPRESA, DATA_IMPORTACAO, TECELAGEM, TEAR, FAMILIA, CODIGO_ARTIGO)
  USING INDEX  ENABLE
   );

/

CREATE OR REPLACE EDITIONABLE TRIGGER  INTER_TR_PROJ_050 
BEFORE INSERT OR UPDATE ON PROJ_050 FOR EACH ROW
DECLARE
BEGIN
   IF inserting THEN 
      IF :new.seq_importacao_tecelagem is null THEN 
         :new.seq_importacao_tecelagem:= seq_proj_050.nextval;
      END IF;
      :new.tecelagem_tear:= :new.tecelagem||' - '||:new.tear;
   END IF;
   IF updating THEN 
      :new.tecelagem_tear:= :new.tecelagem||' - '||:new.tear;
   END IF;   
   :NEW.USUARIO_ATUALIZACAO:= nvl(sys_context('APEX$SESSION','APP_USER'),user);
   :NEW.DATA_ATUALIZACAO:= sysdate;   
END INTER_TR_PROJ_050;

/

ALTER TRIGGER  INTER_TR_PROJ_050 ENABLE;

/

CREATE OR REPLACE EDITIONABLE TRIGGER  INTER_TR_PROJ_050_LOG 
after insert or delete or update 
on proj_050 
for each row 
declare 
   ws_usuario_rede           varchar2(2000) ; 
   ws_maquina_rede           varchar2(2000) ; 
   ws_aplicativo             varchar2(2000) ; 
   ws_sid                    number; 
   ws_empresa                number; 
   ws_usuario_systextil      varchar2(2000) ; 
   ws_locale_usuario         varchar2(2000) ; 
   ws_nome_programa          varchar2(2000) ; 

begin
-- dados do usuario logado
 inter_pr_dados_usuario (ws_usuario_rede,ws_maquina_rede,ws_aplicativo,ws_sid,ws_usuario_systextil,ws_empresa,ws_locale_usuario); 
 ws_nome_programa := inter_fn_nome_programa(ws_sid);

 if inserting 
 then 
    begin 
        insert into proj_050_log (
           tipo_ocorr,   /*0*/ 
           data_ocorr,   /*1*/ 
           hora_ocorr,   /*2*/ 
           usuario_rede,   /*3*/ 
           maquina_rede,   /*4*/ 
           aplicacao,   /*5*/ 
           usuario_sistema,   /*6*/ 
           nome_programa,   /*7*/
           seq_importacao_tecelagem, /*7.1*/  
           codigo_empresa,   /*8*/ 
           data_importacao, /*9*/ 
           tecelagem, /*10*/ 
           tear, /*11*/ 
           familia, /*12*/
           codigo_artigo, /*13*/
           nivel,    /*15*/ 
           grupo,  /*16*/ 
           subgrupo,/*17*/
           item,  /*18*/ 
           tingimento,  /*19*/ 
           ligamento, /*20*/
           qtde_rpm_new,   /*21*/
           qtde_rpm_old, /*22*/
           qtde_batidas_new, /*23*/
           qtde_batidas_old, /*24*/
           taxa_ocupacao_pente_new, /*25*/
           taxa_ocupacao_pente_old, /*26*/
           inicio_new, /*27*/
           inicio_old, /*28*/ 
           fim_new, /*29*/
           fim_old, /*30*/
           media_new, /*31*/
           media_old, /*32*/
           percentual_real_new, /*33*/
           percentual_real_old, /*34*/
           percentual_nominal_new, /*35*/
           percentual_nominal_old, /*36*/
           qtde_metro_mes_new, /*37*/
           qtde_metro_mes_old, /*38*/
           tecelagem_tear /*39*/
        ) values (    
           'i', /*0*/
           sysdate, /*1*/
           sysdate,/*2*/ 
           ws_usuario_rede,/*3*/ 
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/ 
           ws_nome_programa, /*7*/
           null, /*7.1*/ 
           :new.codigo_empresa,/*8*/
           :new.data_importacao, /*9*/   
           :new.tecelagem, /*10*/ 
           :new.tear, /*11*/ 
           :new.familia, /*12*/
           :new.codigo_artigo, /*13*/
           :new.nivel,    /*15*/ 
           :new.grupo,  /*16*/ 
           :new.subgrupo,/*17*/
           :new.item,  /*18*/ 
           :new.tingimento,  /*19*/ 
           :new.ligamento, /*20*/
           :new.qtde_rpm,   /*21*/
           null, /*22*/
           :new.qtde_batidas, /*23*/
           null, /*24*/
           :new.taxa_ocupacao_pente, /*25*/
           null, /*26*/
           :new.inicio, /*27*/
           null, /*28*/ 
           :new.fim, /*29*/
           null, /*30*/
           :new.media, /*31*/
           null, /*32*/
           :new.percentual_real, /*33*/
           null, /*34*/
           :new.percentual_nominal, /*35*/
           null, /*36*/
           :new.qtde_metro_mes, /*37*/
           null, /*38*/
           :new.tecelagem_tear /*39*/
      );    
    end;    
 end if;    
  
 if updating
 then 
    begin 
        insert into proj_050_log (
           tipo_ocorr,   /*0*/ 
           data_ocorr,   /*1*/ 
           hora_ocorr,   /*2*/ 
           usuario_rede,   /*3*/ 
           maquina_rede,   /*4*/ 
           aplicacao,   /*5*/ 
           usuario_sistema,   /*6*/ 
           nome_programa,   /*7*/ 
           seq_importacao_tecelagem, /*7.1*/ 
           codigo_empresa,   /*8*/ 
           data_importacao, /*9*/ 
           tecelagem, /*10*/ 
           tear, /*11*/ 
           familia, /*12*/
           codigo_artigo, /*13*/
           nivel,    /*15*/ 
           grupo,  /*16*/ 
           subgrupo,/*17*/
           item,  /*18*/ 
           tingimento,  /*19*/ 
           ligamento, /*20*/
           qtde_rpm_new,   /*21*/
           qtde_rpm_old, /*22*/
           qtde_batidas_new, /*23*/
           qtde_batidas_old, /*24*/
           taxa_ocupacao_pente_new, /*25*/
           taxa_ocupacao_pente_old, /*26*/
           inicio_new, /*27*/
           inicio_old, /*28*/ 
           fim_new, /*29*/
           fim_old, /*30*/
           media_new, /*31*/
           media_old, /*32*/
           percentual_real_new, /*33*/
           percentual_real_old, /*34*/
           percentual_nominal_new, /*35*/
           percentual_nominal_old, /*36*/
           qtde_metro_mes_new, /*37*/
           qtde_metro_mes_old, /*38*/
           tecelagem_tear /*39*/
        ) values (    
           'a', /*0*/
           sysdate, /*1*/
           sysdate,/*2*/ 
           ws_usuario_rede,/*3*/ 
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/ 
           ws_nome_programa, /*7*/
           :new.seq_importacao_tecelagem, /*7.1*/ 
           :new.codigo_empresa,/*8*/
           :new.data_importacao, /*9*/   
           :new.tecelagem, /*10*/ 
           :new.tear, /*11*/ 
           :new.familia, /*12*/
           :new.codigo_artigo, /*13*/
           :new.nivel,    /*15*/ 
           :new.grupo,  /*16*/ 
           :new.subgrupo,/*17*/
           :new.item,  /*18*/ 
           :new.tingimento,  /*19*/ 
           :new.ligamento, /*20*/
           :new.qtde_rpm,   /*21*/
           :old.qtde_rpm, /*22*/
           :new.qtde_batidas, /*23*/
           :old.qtde_batidas, /*24*/
           :new.taxa_ocupacao_pente, /*25*/
           :old.taxa_ocupacao_pente, /*26*/
           :new.inicio, /*27*/
           :old.inicio, /*28*/ 
           :new.fim, /*29*/
           :old.fim, /*30*/
           :new.media, /*31*/
           :old.media, /*32*/
           :new.percentual_real, /*33*/
           :old.percentual_real, /*34*/
           :new.percentual_nominal, /*35*/
           :old.percentual_nominal, /*36*/
           :new.qtde_metro_mes, /*37*/
           :old.qtde_metro_mes, /*38*/
           :new.tecelagem_tear /*39*/
         );    
    end;    
 end if;    
  
 if deleting 
 then 
    begin 
        insert into proj_050_log (
           tipo_ocorr,   /*0*/ 
           data_ocorr,   /*1*/ 
           hora_ocorr,   /*2*/ 
           usuario_rede,   /*3*/ 
           maquina_rede,   /*4*/ 
           aplicacao,   /*5*/ 
           usuario_sistema,   /*6*/ 
           nome_programa,   /*7*/ 
           seq_importacao_tecelagem, /*7.1*/ 
           codigo_empresa,   /*8*/ 
           data_importacao, /*9*/ 
           tecelagem, /*10*/ 
           tear, /*11*/ 
           familia, /*12*/
           codigo_artigo, /*13*/
           nivel,    /*15*/ 
           grupo,  /*16*/ 
           subgrupo,/*17*/
           item,  /*18*/ 
           tingimento,  /*19*/ 
           ligamento, /*20*/
           qtde_rpm_new,   /*21*/
           qtde_rpm_old, /*22*/
           qtde_batidas_new, /*23*/
           qtde_batidas_old, /*24*/
           taxa_ocupacao_pente_new, /*25*/
           taxa_ocupacao_pente_old, /*26*/
           inicio_new, /*27*/
           inicio_old, /*28*/ 
           fim_new, /*29*/
           fim_old, /*30*/
           media_new, /*31*/
           media_old, /*32*/
           percentual_real_new, /*33*/
           percentual_real_old, /*34*/
           percentual_nominal_new, /*35*/
           percentual_nominal_old, /*36*/
           qtde_metro_mes_new, /*37*/
           qtde_metro_mes_old, /*38*/
           tecelagem_tear /*39*/
       ) values (    
           'd', /*0*/
           sysdate, /*1*/
           sysdate,/*2*/ 
           ws_usuario_rede,/*3*/ 
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/ 
           ws_nome_programa, /*7*/
           :old.seq_importacao_tecelagem, /*7.1*/ 
           :old.codigo_empresa,/*8*/
           :old.data_importacao, /*9*/   
           :old.tecelagem, /*10*/ 
           :old.tear, /*11*/ 
           :old.familia, /*12*/
           :old.codigo_artigo, /*13*/
           :old.nivel,    /*15*/ 
           :old.grupo,  /*16*/ 
           :old.subgrupo,/*17*/
           :old.item,  /*18*/ 
           :old.tingimento,  /*19*/ 
           :old.ligamento, /*20*/
           null,   /*21*/
           :old.qtde_rpm, /*22*/
           null, /*23*/
           :old.qtde_batidas, /*24*/
           null, /*25*/
           :old.taxa_ocupacao_pente, /*26*/
           null, /*27*/
           :old.inicio, /*28*/ 
           null, /*29*/
           :old.fim, /*30*/
           null, /*31*/
           :old.media, /*32*/
           null, /*33*/
           :old.percentual_real, /*34*/
           null, /*35*/
           :old.percentual_nominal, /*36*/
           null, /*37*/
           :old.qtde_metro_mes, /*38*/
           :old.tecelagem_tear /*39*/
         );
    end;    
 end if;
end inter_tr_proj_050_log;

/

ALTER TRIGGER  INTER_TR_PROJ_050_LOG ENABLE;
/
