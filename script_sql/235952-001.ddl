create table pedi_123 (
     codigo_empresa         number(3,0) not null
	,codigo_transacao       number(3,0) not null
	,codigo_deposito        number(3,0) not null
	,codigo_motivo_bloqueio number(3,0) not null
	,codigo_cancelamento    number(3,0) not null
	,primary key (codigo_empresa)
);
