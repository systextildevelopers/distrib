CREATE TABLE oper_111 (
codigo_empresa         number(3)   default 0,
tamanho_min_senha      number(2)   default 8,
controle_senha         number(1)   default 0,
tempo_expiracao_senha  number(2)   default 90,
nro_historico_senha    number(2)   default 4,
bloqueio_tentativas    number(2)   default 5,
tempo_tentativas       number(3)   default 5,
senha_conter_letra     varchar2(1) default 'N',
senha_conter_numeros   varchar2(1) default 'N',
senha_conter_especiais varchar2(1) default 'N');

alter table oper_111 add constraint pk_oper_111 primary key (codigo_empresa);
