
  CREATE OR REPLACE FUNCTION "INTER_FN_ENCONTRA_PRECO_CUSTO" (p_origem_preco_custo in varchar2,
                                                         p_empresa            in number,
                                                         p_nivel              in varchar2,
                                                         p_grupo              in varchar2,
                                                         p_subgrupo           in varchar2,
                                                         p_item               in varchar2,
                                                         p_deposito           in number,
                                                         p_data_movimento     in date,
                                                         p_tipo_preco_ret     in number)
   return number

   -- P_TIPO_PRECO
   --    1 = preco de custo
   --    2 = preco de custo projetado
   --    3 = preco de custo estimado

 is

   r_valor_custo      estq_300.preco_medio_unitario%type;
   r_valor_custo_proj estq_300.preco_medio_unitario%type;
   r_valor_custo_est  estq_300.preco_medio_unitario%type;

begin

   begin
      inter_pr_encontra_preco_custo(p_origem_preco_custo, p_empresa, p_nivel, p_grupo, p_subgrupo, p_item, p_deposito, p_data_movimento, r_valor_custo, r_valor_custo_proj, r_valor_custo_est);

   exception
      when others then
         r_valor_custo      := 0.00;
         r_valor_custo_proj := 0.00;
         r_valor_custo_est  := 0.00;
   end;

   if p_tipo_preco_ret = 1
   then
      return(r_valor_custo);
   else

      if p_tipo_preco_ret = 1
      then
         return(r_valor_custo_proj);
      else
         return(r_valor_custo_est);
      end if;
   end if;
end inter_fn_encontra_preco_custo;
 

/

exec inter_pr_recompile;

