----------------------------------
-- (PCPT_021) Sugestão de Rolos --
----------------------------------

alter table pcpt_021 add sugerido number(1,0) default 0 not null;

comment on column pcpt_021.sugerido is 'Sinaliza se o rolo foi sugerido ou não (0 - não 1 - sim)';
