insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('basi_c007', 'Cadastro de certificação das Divisões do produto', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'basi_c007', 'basi_menu', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Cadastro de certificação das Divisões do produto'
where hdoc_036.codigo_programa = 'basi_c007'
and hdoc_036.locale          = 'es_ES';

commit;
