create table mqop_245(
    id                   number(9)
    ,grupo_maq_tipo_6    varchar2(4)
    ,subgrupo_maq_tipo_6 varchar2(3)
    ,numero_maq_tipo_6   number(5,0)
    ,estagio             number(2)

    ,grupo_maq_tipo_1    varchar2(4)
    ,subgrupo_maq_tipo_1 varchar2(3)
    ,numero_maq_tipo_1   number(5,0)
);

alter table mqop_245
    add constraint PK_MQOP_245 primary key (id);
