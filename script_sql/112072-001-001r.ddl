alter table pedi_100
add (marca_libera number(1) );

alter table pedi_100
modify (marca_libera default 0 );

alter table pedi_100
add (solic_libera number(9) );

alter table pedi_100
modify (solic_libera default 0 );

/
exec inter_pr_recompile;
