CREATE OR REPLACE TRIGGER "INTER_TR_CPAG_JUROS"
   before insert
   on cpag_015
   for each row

declare
   v_executa_trigger   number;

   lvlr_multa          cpag_015.vlr_multa%type;
   lvlr_juros          cpag_015.vlr_juros%type;
   lvlr_despcartorio   cpag_015.vlr_desp_cartorio%type;
   lvlr_outras_desp    cpag_015.vlr_outras_desp%type;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      -- busca dados do titulo original
      begin
         select cpag_010.vlr_multa,         cpag_010.vlr_juros ,
                cpag_010.vlr_desp_cartorio, cpag_010.vlr_outras_desp
            into   lvlr_multa,               lvlr_juros,
                   lvlr_despcartorio,        lvlr_outras_desp
            from cpag_010
            where cpag_010.cgc_9        = :new.dupl_for_for_cli9
              and cpag_010.cgc_4        = :new.dupl_for_for_cli4
              and cpag_010.cgc_2        = :new.dupl_for_for_cli2
              and cpag_010.tipo_titulo  = :new.dupl_for_tipo_tit
              and cpag_010.nr_duplicata = :new.dupl_for_nrduppag
              and cpag_010.parcela      = :new.dupl_for_no_parc;
         exception
            when no_data_found then
                  lvlr_multa := 0;
                  lvlr_juros := 0;
                  lvlr_despcartorio := 0;
                  lvlr_outras_desp  := 0;
      end;

      if lvlr_multa <> 0
      then
         :new.vlr_multa := lvlr_multa;
      end if;

      if lvlr_juros <> 0
      then
         :new.vlr_juros := lvlr_juros;
      end if;

      if lvlr_despcartorio <> 0
      then
         :new.vlr_desp_cartorio := lvlr_despcartorio;
      end if;

      if lvlr_outras_desp <> 0
      then
         :new.vlr_outras_desp := lvlr_outras_desp;
      end if;

      if lvlr_multa <> 0 or lvlr_juros <> 0 or lvlr_outras_desp <> 0 or lvlr_despcartorio <> 0
      then
         update cpag_010
         set vlr_multa = 0,
             vlr_juros = 0,
             vlr_desp_cartorio = 0,
             vlr_outras_desp = 0
         where cpag_010.cgc_9        = :new.dupl_for_for_cli9
           and cpag_010.cgc_4        = :new.dupl_for_for_cli4
           and cpag_010.cgc_2        = :new.dupl_for_for_cli2
           and cpag_010.tipo_titulo  = :new.dupl_for_tipo_tit
           and cpag_010.nr_duplicata = :new.dupl_for_nrduppag
           and cpag_010.parcela      = :new.dupl_for_no_parc;
      end if;
   end if;
end INTER_TR_CPAG_JUROS;

/

exec inter_pr_recompile;
