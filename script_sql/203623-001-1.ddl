/*Caminho para gerar arquivo de remessa do contas a pagar CPAG_F705*/

INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('apagar.caminhoGerarRemessa', 0, 'fy45951', '', '', null, null, null);

/*Caminho para gerar arquivo de remessa do contas a receber COBR_F900*/

INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('apagar.caminhoBackupRemessa', 0, 'fy45952', '', '', null, null, null);

commit;
