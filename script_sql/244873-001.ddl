INSERT INTO hdoc_035
          ( codigo_programa
          , programa_menu
          , item_menu_def
          , descricao)
   VALUES ( 'empr_f817'
          , 0
          , 0
          , 'Relacionamento Motivo Bloqueio para limite');
 
INSERT INTO hdoc_033
          ( usu_prg_cdusu
          , usu_prg_empr_usu
          , programa
          , nome_menu
          , item_menu
          , ordem_menu
          , incluir
          , modificar
          , excluir
          , procurar)
    VALUES( 'INTERSYS'
          , 1
          , 'empr_f817'
          , 'nenhum'
          , 0
          , 0
          , 'S'
          , 'S'
          , 'S'
          , 'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Relación Motivo Bloqueo por límite'
 WHERE hdoc_036.codigo_programa = 'empr_f817'
   AND hdoc_036.locale          = 'es_ES';
  
commit work;
