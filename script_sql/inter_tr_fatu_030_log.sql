create or replace trigger "INTER_TR_FATU_030_LOG"
after insert or delete or update
on fatu_030
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);

 if inserting
 then
    begin

        insert into fatu_030_log (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           pedido_venda_old,   /*8*/
           pedido_venda_new,   /*9*/
           nr_solicitacao_old,   /*10*/
           nr_solicitacao_new,   /*11*/
           num_nota_fiscal_old,   /*12*/
           num_nota_fiscal_new,   /*13*/
           serie_nota_fisc_old,   /*14*/
           serie_nota_fisc_new,   /*15*/
           data_emissao_old,   /*16*/
           data_emissao_new,   /*17*/
           data_saida_old,   /*18*/
           data_saida_new,   /*19*/
           peso_liquido_old,   /*20*/
           peso_liquido_new,   /*21*/
           peso_bruto_old,   /*22*/
           peso_bruto_new,   /*23*/
           observacao_01_old,   /*24*/
           observacao_01_new,   /*25*/
           observacao_02_old,   /*26*/
           observacao_02_new,   /*27*/
           qtde_embalagens_old,   /*28*/
           qtde_embalagens_new,   /*29*/
           numero_do_volume_old,   /*30*/
           numero_do_volume_new,   /*31*/
           valor_despesas_old,   /*32*/
           valor_despesas_new,   /*33*/
           valor_frete_old,   /*34*/
           valor_frete_new,   /*35*/
           valor_seguro_old,   /*36*/
           valor_seguro_new,   /*37*/
           data_base_fatura_old,   /*38*/
           data_base_fatura_new,   /*39*/
           marca_volumes_old,   /*40*/
           marca_volumes_new,   /*41*/
           tipo_frete_old,   /*42*/
           tipo_frete_new,   /*43*/
           tipo_frete_redes_old,   /*44*/
           tipo_frete_redes_new,   /*45*/
           taxa_encargos_old,   /*46*/
           taxa_encargos_new,   /*47*/
           especie_volume_old,   /*48*/
           especie_volume_new,   /*49*/
           posicao_fatura_old,   /*50*/
           posicao_fatura_new,   /*51*/
           mensagem_corpo1_old,   /*52*/
           mensagem_corpo1_new,   /*53*/
           mensagem_corpo2_old,   /*54*/
           mensagem_corpo2_new,   /*55*/
           tipo_fatu_old,   /*56*/
           tipo_fatu_new,   /*57*/
           codigo_embalagem_old,   /*58*/
           codigo_embalagem_new,   /*59*/
           vlr_desc_especial_old,   /*60*/
           vlr_desc_especial_new,   /*61*/
           transp9_old,   /*62*/
           transp9_new,   /*63*/
           transp4_old,   /*64*/
           transp4_new,   /*65*/
           transp2_old,   /*66*/
           transp2_new,   /*67*/
           roma_volu_old,   /*68*/
           roma_volu_new,   /*69*/
           placa_veiculo_old,   /*70*/
           placa_veiculo_new,   /*71*/
           natureza_operacao_old,   /*72*/
           natureza_operacao_new,   /*73*/
           estado_natureza_old,   /*74*/
           estado_natureza_new,   /*75*/
           motivo_devolucao_old,   /*76*/
           motivo_devolucao_new,   /*77*/
           hora_saida_old,   /*78*/
           hora_saida_new,   /*79*/
           tarifa_frete_old,   /*80*/
           tarifa_frete_new,   /*81*/
           sequencia_faturamento_old,   /*82*/
           sequencia_faturamento_new,   /*83*/
           numero_re_old,   /*84*/
           numero_re_new,   /*85*/
           numero_di_old,   /*86*/
           numero_di_new,   /*87*/
           aloca_solic_old,   /*88*/
           aloca_solic_new,   /*89*/
           imp_comunic_fatur_old,   /*90*/
           imp_comunic_fatur_new,   /*91*/
           num_container_exp_old,   /*92*/
           num_container_exp_new,   /*93*/
           num_lacre_exp_old,   /*94*/
           num_lacre_exp_new,   /*95*/
           moeda_pedido_old,   /*96*/
           moeda_pedido_new,   /*97*/
           codigo_condutor_old,   /*98*/
           codigo_condutor_new,   /*99*/
           vr_indice_moeda_old,   /*100*/
           vr_indice_moeda_new,   /*101*/
           perc_negociacao_old,   /*102*/
           perc_negociacao_new,   /*103*/
           endereco_old,   /*104*/
           endereco_new,   /*105*/
           usuario_old,   /*106*/
           usuario_new,   /*107*/
           data_cadastro_old,   /*108*/
           data_cadastro_new,   /*109*/
           num_prioridade_old,   /*110*/
           num_prioridade_new,   /*111*/
           usuario_solicitacao_old,   /*112*/
           usuario_solicitacao_new,    /*113*/
           total_qtde_old,	/*114*/
           total_qtde_new,  /*115*/
           total_mercadoria_old,	/*116*/
           total_mercadoria_new,  /*117*/
           valor_nota_fatura_old, /*118*/
           valor_nota_fatura_new, /*119*/
           valor_nota_fiscal_old, /*120*/
           valor_nota_fiscal_new,  /*121*/
           cod_banco_old, /*122*/
           cod_banco_new  /*123*/
        ) values (
            'i', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.pedido_venda, /*9*/
           0,/*10*/
           :new.nr_solicitacao, /*11*/
           0,/*12*/
           :new.num_nota_fiscal, /*13*/
           '',/*14*/
           :new.serie_nota_fisc, /*15*/
           null,/*16*/
           :new.data_emissao, /*17*/
           null,/*18*/
           :new.data_saida, /*19*/
           0,/*20*/
           :new.peso_liquido, /*21*/
           0,/*22*/
           :new.peso_bruto, /*23*/
           '',/*24*/
           :new.observacao_01, /*25*/
           '',/*26*/
           :new.observacao_02, /*27*/
           0,/*28*/
           :new.qtde_embalagens, /*29*/
           '',/*30*/
           :new.numero_do_volume, /*31*/
           0,/*32*/
           :new.valor_despesas, /*33*/
           0,/*34*/
           :new.valor_frete, /*35*/
           0,/*36*/
           :new.valor_seguro, /*37*/
           null,/*38*/
           :new.data_base_fatura, /*39*/
           '',/*40*/
           :new.marca_volumes, /*41*/
           0,/*42*/
           :new.tipo_frete, /*43*/
           0,/*44*/
           :new.tipo_frete_redes, /*45*/
           0,/*46*/
           :new.taxa_encargos, /*47*/
           '',/*48*/
           :new.especie_volume, /*49*/
           0,/*50*/
           :new.posicao_fatura, /*51*/
           '',/*52*/
           :new.mensagem_corpo1, /*53*/
           '',/*54*/
           :new.mensagem_corpo2, /*55*/
           0,/*56*/
           :new.tipo_fatu, /*57*/
           0,/*58*/
           :new.codigo_embalagem, /*59*/
           0,/*60*/
           :new.vlr_desc_especial, /*61*/
           0,/*62*/
           :new.transp9, /*63*/
           0,/*64*/
           :new.transp4, /*65*/
           0,/*66*/
           :new.transp2, /*67*/
           0,/*68*/
           :new.roma_volu, /*69*/
           '',/*70*/
           :new.placa_veiculo, /*71*/
           0,/*72*/
           :new.natureza_operacao, /*73*/
           '',/*74*/
           :new.estado_natureza, /*75*/
           0,/*76*/
           :new.motivo_devolucao, /*77*/
           null,/*78*/
           :new.hora_saida, /*79*/
           0,/*80*/
           :new.tarifa_frete, /*81*/
           0,/*82*/
           :new.sequencia_faturamento, /*83*/
           0,/*84*/
           :new.numero_re, /*85*/
           0,/*86*/
           :new.numero_di, /*87*/
           0,/*88*/
           :new.aloca_solic, /*89*/
           0,/*90*/
           :new.imp_comunic_fatur, /*91*/
           '',/*92*/
           :new.num_container_exp, /*93*/
           '',/*94*/
           :new.num_lacre_exp, /*95*/
           0,/*96*/
           :new.moeda_pedido, /*97*/
           0,/*98*/
           :new.codigo_condutor, /*99*/
           0,/*100*/
           :new.vr_indice_moeda, /*101*/
           0,/*102*/
           :new.perc_negociacao, /*103*/
           '',/*104*/
           :new.endereco, /*105*/
           '',/*106*/
           :new.usuario, /*107*/
           null,/*108*/
           :new.data_cadastro, /*109*/
           0,/*110*/
           :new.num_prioridade, /*111*/
           '',/*112*/
           :new.usuario_solicitacao, /*113*/
           0,	/*114*/
           :new.total_qtde,  /*115*/
           0,	/*116*/
           :new.total_mercadoria,  /*117*/
           0, /*118*/
           :new.valor_nota_fatura, /*119*/
           0, /*120*/
           :new.valor_nota_fiscal, /*121*/
           0, /*122*/
           :new.cod_banco /*123*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into fatu_030_log (
           tipo_ocorr, /*0*/
           data_ocorr, /*1*/
           hora_ocorr, /*2*/
           usuario_rede, /*3*/
           maquina_rede, /*4*/
           aplicacao, /*5*/
           usuario_sistema, /*6*/
           nome_programa, /*7*/
           pedido_venda_old, /*8*/
           pedido_venda_new, /*9*/
           nr_solicitacao_old, /*10*/
           nr_solicitacao_new, /*11*/
           num_nota_fiscal_old, /*12*/
           num_nota_fiscal_new, /*13*/
           serie_nota_fisc_old, /*14*/
           serie_nota_fisc_new, /*15*/
           data_emissao_old, /*16*/
           data_emissao_new, /*17*/
           data_saida_old, /*18*/
           data_saida_new, /*19*/
           peso_liquido_old, /*20*/
           peso_liquido_new, /*21*/
           peso_bruto_old, /*22*/
           peso_bruto_new, /*23*/
           observacao_01_old, /*24*/
           observacao_01_new, /*25*/
           observacao_02_old, /*26*/
           observacao_02_new, /*27*/
           qtde_embalagens_old, /*28*/
           qtde_embalagens_new, /*29*/
           numero_do_volume_old, /*30*/
           numero_do_volume_new, /*31*/
           valor_despesas_old, /*32*/
           valor_despesas_new, /*33*/
           valor_frete_old, /*34*/
           valor_frete_new, /*35*/
           valor_seguro_old, /*36*/
           valor_seguro_new, /*37*/
           data_base_fatura_old, /*38*/
           data_base_fatura_new, /*39*/
           marca_volumes_old, /*40*/
           marca_volumes_new, /*41*/
           tipo_frete_old, /*42*/
           tipo_frete_new, /*43*/
           tipo_frete_redes_old, /*44*/
           tipo_frete_redes_new, /*45*/
           taxa_encargos_old, /*46*/
           taxa_encargos_new, /*47*/
           especie_volume_old, /*48*/
           especie_volume_new, /*49*/
           posicao_fatura_old, /*50*/
           posicao_fatura_new, /*51*/
           mensagem_corpo1_old, /*52*/
           mensagem_corpo1_new, /*53*/
           mensagem_corpo2_old, /*54*/
           mensagem_corpo2_new, /*55*/
           tipo_fatu_old, /*56*/
           tipo_fatu_new, /*57*/
           codigo_embalagem_old, /*58*/
           codigo_embalagem_new, /*59*/
           vlr_desc_especial_old, /*60*/
           vlr_desc_especial_new, /*61*/
           transp9_old, /*62*/
           transp9_new, /*63*/
           transp4_old, /*64*/
           transp4_new, /*65*/
           transp2_old, /*66*/
           transp2_new, /*67*/
           roma_volu_old, /*68*/
           roma_volu_new, /*69*/
           placa_veiculo_old, /*70*/
           placa_veiculo_new, /*71*/
           natureza_operacao_old, /*72*/
           natureza_operacao_new, /*73*/
           estado_natureza_old, /*74*/
           estado_natureza_new, /*75*/
           motivo_devolucao_old, /*76*/
           motivo_devolucao_new, /*77*/
           hora_saida_old, /*78*/
           hora_saida_new, /*79*/
           tarifa_frete_old, /*80*/
           tarifa_frete_new, /*81*/
           sequencia_faturamento_old, /*82*/
           sequencia_faturamento_new, /*83*/
           numero_re_old, /*84*/
           numero_re_new, /*85*/
           numero_di_old, /*86*/
           numero_di_new, /*87*/
           aloca_solic_old, /*88*/
           aloca_solic_new, /*89*/
           imp_comunic_fatur_old, /*90*/
           imp_comunic_fatur_new, /*91*/
           num_container_exp_old, /*92*/
           num_container_exp_new, /*93*/
           num_lacre_exp_old, /*94*/
           num_lacre_exp_new, /*95*/
           moeda_pedido_old, /*96*/
           moeda_pedido_new, /*97*/
           codigo_condutor_old, /*98*/
           codigo_condutor_new, /*99*/
           vr_indice_moeda_old, /*100*/
           vr_indice_moeda_new, /*101*/
           perc_negociacao_old, /*102*/
           perc_negociacao_new, /*103*/
           endereco_old, /*104*/
           endereco_new, /*105*/
           usuario_old, /*106*/
           usuario_new, /*107*/
           data_cadastro_old, /*108*/
           data_cadastro_new, /*109*/
           num_prioridade_old, /*110*/
           num_prioridade_new, /*111*/
           usuario_solicitacao_old, /*112*/
           usuario_solicitacao_new,  /*113*/
           total_qtde_old,	/*114*/
           total_qtde_new,  /*115*/
           total_mercadoria_old,	/*116*/
           total_mercadoria_new,  /*117*/
           valor_nota_fatura_old, /*118*/
           valor_nota_fatura_new, /*119*/
           valor_nota_fiscal_old, /*120*/
           valor_nota_fiscal_new, /*121*/
           cod_banco_old, /*122*/
           cod_banco_new  /*123*/
        ) values (
            'a', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.pedido_venda,  /*8*/
           :new.pedido_venda, /*9*/
           :old.nr_solicitacao,  /*10*/
           :new.nr_solicitacao, /*11*/
           :old.num_nota_fiscal,  /*12*/
           :new.num_nota_fiscal, /*13*/
           :old.serie_nota_fisc,  /*14*/
           :new.serie_nota_fisc, /*15*/
           :old.data_emissao,  /*16*/
           :new.data_emissao, /*17*/
           :old.data_saida,  /*18*/
           :new.data_saida, /*19*/
           :old.peso_liquido,  /*20*/
           :new.peso_liquido, /*21*/
           :old.peso_bruto,  /*22*/
           :new.peso_bruto, /*23*/
           :old.observacao_01,  /*24*/
           :new.observacao_01, /*25*/
           :old.observacao_02,  /*26*/
           :new.observacao_02, /*27*/
           :old.qtde_embalagens,  /*28*/
           :new.qtde_embalagens, /*29*/
           :old.numero_do_volume,  /*30*/
           :new.numero_do_volume, /*31*/
           :old.valor_despesas,  /*32*/
           :new.valor_despesas, /*33*/
           :old.valor_frete,  /*34*/
           :new.valor_frete, /*35*/
           :old.valor_seguro,  /*36*/
           :new.valor_seguro, /*37*/
           :old.data_base_fatura,  /*38*/
           :new.data_base_fatura, /*39*/
           :old.marca_volumes,  /*40*/
           :new.marca_volumes, /*41*/
           :old.tipo_frete,  /*42*/
           :new.tipo_frete, /*43*/
           :old.tipo_frete_redes,  /*44*/
           :new.tipo_frete_redes, /*45*/
           :old.taxa_encargos,  /*46*/
           :new.taxa_encargos, /*47*/
           :old.especie_volume,  /*48*/
           :new.especie_volume, /*49*/
           :old.posicao_fatura,  /*50*/
           :new.posicao_fatura, /*51*/
           :old.mensagem_corpo1,  /*52*/
           :new.mensagem_corpo1, /*53*/
           :old.mensagem_corpo2,  /*54*/
           :new.mensagem_corpo2, /*55*/
           :old.tipo_fatu,  /*56*/
           :new.tipo_fatu, /*57*/
           :old.codigo_embalagem,  /*58*/
           :new.codigo_embalagem, /*59*/
           :old.vlr_desc_especial,  /*60*/
           :new.vlr_desc_especial, /*61*/
           :old.transp9,  /*62*/
           :new.transp9, /*63*/
           :old.transp4,  /*64*/
           :new.transp4, /*65*/
           :old.transp2,  /*66*/
           :new.transp2, /*67*/
           :old.roma_volu,  /*68*/
           :new.roma_volu, /*69*/
           :old.placa_veiculo,  /*70*/
           :new.placa_veiculo, /*71*/
           :old.natureza_operacao,  /*72*/
           :new.natureza_operacao, /*73*/
           :old.estado_natureza,  /*74*/
           :new.estado_natureza, /*75*/
           :old.motivo_devolucao,  /*76*/
           :new.motivo_devolucao, /*77*/
           :old.hora_saida,  /*78*/
           :new.hora_saida, /*79*/
           :old.tarifa_frete,  /*80*/
           :new.tarifa_frete, /*81*/
           :old.sequencia_faturamento,  /*82*/
           :new.sequencia_faturamento, /*83*/
           :old.numero_re,  /*84*/
           :new.numero_re, /*85*/
           :old.numero_di,  /*86*/
           :new.numero_di, /*87*/
           :old.aloca_solic,  /*88*/
           :new.aloca_solic, /*89*/
           :old.imp_comunic_fatur,  /*90*/
           :new.imp_comunic_fatur, /*91*/
           :old.num_container_exp,  /*92*/
           :new.num_container_exp, /*93*/
           :old.num_lacre_exp,  /*94*/
           :new.num_lacre_exp, /*95*/
           :old.moeda_pedido,  /*96*/
           :new.moeda_pedido, /*97*/
           :old.codigo_condutor,  /*98*/
           :new.codigo_condutor, /*99*/
           :old.vr_indice_moeda,  /*100*/
           :new.vr_indice_moeda, /*101*/
           :old.perc_negociacao,  /*102*/
           :new.perc_negociacao, /*103*/
           :old.endereco,  /*104*/
           :new.endereco, /*105*/
           :old.usuario,  /*106*/
           :new.usuario, /*107*/
           :old.data_cadastro,  /*108*/
           :new.data_cadastro, /*109*/
           :old.num_prioridade,  /*110*/
           :new.num_prioridade, /*111*/
           :old.usuario_solicitacao,  /*112*/
           :new.usuario_solicitacao,  /*113*/
           :old.total_qtde,	/*114*/
           :new.total_qtde,  /*115*/
           :old.total_mercadoria,	/*116*/
           :new.total_mercadoria,  /*117*/
           :old.valor_nota_fatura, /*118*/
           :new.valor_nota_fatura, /*119*/
           :old.valor_nota_fiscal, /*120*/
           :new.valor_nota_fiscal,  /*121*/
           :old.cod_banco, /*122*/
           :new.cod_banco  /*123*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into fatu_030_log (
           tipo_ocorr, /*0*/
           data_ocorr, /*1*/
           hora_ocorr, /*2*/
           usuario_rede, /*3*/
           maquina_rede, /*4*/
           aplicacao, /*5*/
           usuario_sistema, /*6*/
           nome_programa, /*7*/
           pedido_venda_old, /*8*/
           pedido_venda_new, /*9*/
           nr_solicitacao_old, /*10*/
           nr_solicitacao_new, /*11*/
           num_nota_fiscal_old, /*12*/
           num_nota_fiscal_new, /*13*/
           serie_nota_fisc_old, /*14*/
           serie_nota_fisc_new, /*15*/
           data_emissao_old, /*16*/
           data_emissao_new, /*17*/
           data_saida_old, /*18*/
           data_saida_new, /*19*/
           peso_liquido_old, /*20*/
           peso_liquido_new, /*21*/
           peso_bruto_old, /*22*/
           peso_bruto_new, /*23*/
           observacao_01_old, /*24*/
           observacao_01_new, /*25*/
           observacao_02_old, /*26*/
           observacao_02_new, /*27*/
           qtde_embalagens_old, /*28*/
           qtde_embalagens_new, /*29*/
           numero_do_volume_old, /*30*/
           numero_do_volume_new, /*31*/
           valor_despesas_old, /*32*/
           valor_despesas_new, /*33*/
           valor_frete_old, /*34*/
           valor_frete_new, /*35*/
           valor_seguro_old, /*36*/
           valor_seguro_new, /*37*/
           data_base_fatura_old, /*38*/
           data_base_fatura_new, /*39*/
           marca_volumes_old, /*40*/
           marca_volumes_new, /*41*/
           tipo_frete_old, /*42*/
           tipo_frete_new, /*43*/
           tipo_frete_redes_old, /*44*/
           tipo_frete_redes_new, /*45*/
           taxa_encargos_old, /*46*/
           taxa_encargos_new, /*47*/
           especie_volume_old, /*48*/
           especie_volume_new, /*49*/
           posicao_fatura_old, /*50*/
           posicao_fatura_new, /*51*/
           mensagem_corpo1_old, /*52*/
           mensagem_corpo1_new, /*53*/
           mensagem_corpo2_old, /*54*/
           mensagem_corpo2_new, /*55*/
           tipo_fatu_old, /*56*/
           tipo_fatu_new, /*57*/
           codigo_embalagem_old, /*58*/
           codigo_embalagem_new, /*59*/
           vlr_desc_especial_old, /*60*/
           vlr_desc_especial_new, /*61*/
           transp9_old, /*62*/
           transp9_new, /*63*/
           transp4_old, /*64*/
           transp4_new, /*65*/
           transp2_old, /*66*/
           transp2_new, /*67*/
           roma_volu_old, /*68*/
           roma_volu_new, /*69*/
           placa_veiculo_old, /*70*/
           placa_veiculo_new, /*71*/
           natureza_operacao_old, /*72*/
           natureza_operacao_new, /*73*/
           estado_natureza_old, /*74*/
           estado_natureza_new, /*75*/
           motivo_devolucao_old, /*76*/
           motivo_devolucao_new, /*77*/
           hora_saida_old, /*78*/
           hora_saida_new, /*79*/
           tarifa_frete_old, /*80*/
           tarifa_frete_new, /*81*/
           sequencia_faturamento_old, /*82*/
           sequencia_faturamento_new, /*83*/
           numero_re_old, /*84*/
           numero_re_new, /*85*/
           numero_di_old, /*86*/
           numero_di_new, /*87*/
           aloca_solic_old, /*88*/
           aloca_solic_new, /*89*/
           imp_comunic_fatur_old, /*90*/
           imp_comunic_fatur_new, /*91*/
           num_container_exp_old, /*92*/
           num_container_exp_new, /*93*/
           num_lacre_exp_old, /*94*/
           num_lacre_exp_new, /*95*/
           moeda_pedido_old, /*96*/
           moeda_pedido_new, /*97*/
           codigo_condutor_old, /*98*/
           codigo_condutor_new, /*99*/
           vr_indice_moeda_old, /*100*/
           vr_indice_moeda_new, /*101*/
           perc_negociacao_old, /*102*/
           perc_negociacao_new, /*103*/
           endereco_old, /*104*/
           endereco_new, /*105*/
           usuario_old, /*106*/
           usuario_new, /*107*/
           data_cadastro_old, /*108*/
           data_cadastro_new, /*109*/
           num_prioridade_old, /*110*/
           num_prioridade_new, /*111*/
           usuario_solicitacao_old, /*112*/
           usuario_solicitacao_new, /*113*/
           total_qtde_old,	/*114*/
           total_qtde_new,  /*115*/
           total_mercadoria_old,	/*116*/
           total_mercadoria_new,  /*117*/
           valor_nota_fatura_old, /*118*/
           valor_nota_fatura_new, /*119*/
           valor_nota_fiscal_old, /*120*/
           valor_nota_fiscal_new, /*121*/
           cod_banco_old, /*122*/
           cod_banco_new  /*123*/
        ) values (
            'd', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.pedido_venda, /*8*/
           0, /*9*/
           :old.nr_solicitacao, /*10*/
           0, /*11*/
           :old.num_nota_fiscal, /*12*/
           0, /*13*/
           :old.serie_nota_fisc, /*14*/
           '', /*15*/
           :old.data_emissao, /*16*/
           null, /*17*/
           :old.data_saida, /*18*/
           null, /*19*/
           :old.peso_liquido, /*20*/
           0, /*21*/
           :old.peso_bruto, /*22*/
           0, /*23*/
           :old.observacao_01, /*24*/
           '', /*25*/
           :old.observacao_02, /*26*/
           '', /*27*/
           :old.qtde_embalagens, /*28*/
           0, /*29*/
           :old.numero_do_volume, /*30*/
           '', /*31*/
           :old.valor_despesas, /*32*/
           0, /*33*/
           :old.valor_frete, /*34*/
           0, /*35*/
           :old.valor_seguro, /*36*/
           0, /*37*/
           :old.data_base_fatura, /*38*/
           null, /*39*/
           :old.marca_volumes, /*40*/
           '', /*41*/
           :old.tipo_frete, /*42*/
           0, /*43*/
           :old.tipo_frete_redes, /*44*/
           0, /*45*/
           :old.taxa_encargos, /*46*/
           0, /*47*/
           :old.especie_volume, /*48*/
           '', /*49*/
           :old.posicao_fatura, /*50*/
           0, /*51*/
           :old.mensagem_corpo1, /*52*/
           '', /*53*/
           :old.mensagem_corpo2, /*54*/
           '', /*55*/
           :old.tipo_fatu, /*56*/
           0, /*57*/
           :old.codigo_embalagem, /*58*/
           0, /*59*/
           :old.vlr_desc_especial, /*60*/
           0, /*61*/
           :old.transp9, /*62*/
           0, /*63*/
           :old.transp4, /*64*/
           0, /*65*/
           :old.transp2, /*66*/
           0, /*67*/
           :old.roma_volu, /*68*/
           0, /*69*/
           :old.placa_veiculo, /*70*/
           '', /*71*/
           :old.natureza_operacao, /*72*/
           0, /*73*/
           :old.estado_natureza, /*74*/
           '', /*75*/
           :old.motivo_devolucao, /*76*/
           0, /*77*/
           :old.hora_saida, /*78*/
           null, /*79*/
           :old.tarifa_frete, /*80*/
           0, /*81*/
           :old.sequencia_faturamento, /*82*/
           0, /*83*/
           :old.numero_re, /*84*/
           0, /*85*/
           :old.numero_di, /*86*/
           0, /*87*/
           :old.aloca_solic, /*88*/
           0, /*89*/
           :old.imp_comunic_fatur, /*90*/
           0, /*91*/
           :old.num_container_exp, /*92*/
           '', /*93*/
           :old.num_lacre_exp, /*94*/
           '', /*95*/
           :old.moeda_pedido, /*96*/
           0, /*97*/
           :old.codigo_condutor, /*98*/
           0, /*99*/
           :old.vr_indice_moeda, /*100*/
           0, /*101*/
           :old.perc_negociacao, /*102*/
           0, /*103*/
           :old.endereco, /*104*/
           '', /*105*/
           :old.usuario, /*106*/
           '', /*107*/
           :old.data_cadastro, /*108*/
           null, /*109*/
           :old.num_prioridade, /*110*/
           0, /*111*/
           :old.usuario_solicitacao, /*112*/
           '', /*113*/
           :old.total_qtde,	/*114*/
           0,  /*115*/
           :old.total_mercadoria,	/*116*/
           0,  /*117*/
           :old.valor_nota_fatura, /*118*/
           0, /*119*/
           :old.valor_nota_fiscal, /*120*/
           0, /*121*/
           :old.cod_banco, /*122*/
           0  /*123*/
         );
    end;
 end if;
end inter_tr_fatu_030_log;

-- ALTER TRIGGER "INTER_TR_FATU_030_LOG" ENABLE


-- ALTER TRIGGER "INTER_TR_FATU_030_LOG" ENABLE
 

/

exec inter_pr_recompile;

