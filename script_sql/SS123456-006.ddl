alter table pcpc_045
add(nr_operadores_informado number(5,1));

alter table pcpc_045
add(eficiencia              number(5,2));

alter table pcpc_045
add(minutos_peca            number(9,4));

comment on column pcpc_045.nr_operadores_informado is 'Numero de operadores real na equipe no momento da produção.';
comment on column pcpc_045.eficiencia is 'Eficiência padrão da equipe.';
comment on column pcpc_045.nr_operadores is 'Numero de operadores padrão da equipe.';
comment on column pcpc_045.eficiencia_informada is 'Eficiência real da equipe no momento da produção.';
                
exec inter_pr_recompile;
