alter table pcpc_040 add ordem_confeccao_origem number(5);

comment on column pcpc_040.ordem_confeccao_origem is 'Ordem de Confeccao origem deste pacote (gravado no pcpc_f052 - Divisao de Pacotes)';
