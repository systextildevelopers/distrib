create table pcpc_179(
    codigo_ocorrencia number(3) not null,
    descricao varchar2(6) default ''
);

alter table pcpc_179
add constraint PK_OCORRENCIA_ESTORNO primary key(codigo_ocorrencia);

comment on table pcpc_179 is 'Ocorrências de estorno da ordem de confecção';
comment on column pcpc_179.codigo_ocorrencia is 'Código da ocorrência';
comment on column pcpc_179.descricao is 'Descrição da ocorrência';
/
exec inter_pr_recompile;
/
