create or replace trigger wms_tr_produtos_log
  before insert or delete on inte_wms_produtos
  for each row
declare
  -- local variables here
  v_nivel_sku                      inte_wms_produtos.nivel_sku                   %type;
  v_grupo_sku                      inte_wms_produtos.grupo_sku                   %type;
  v_subgrupo_sku                   inte_wms_produtos.subgrupo_sku                %type;
  v_item_sku                       inte_wms_produtos.item_sku                    %type;
  v_ean_sku                        inte_wms_produtos.ean_sku                     %type;
  v_descricao_sku                  inte_wms_produtos.descricao_sku               %type;
  v_descr_tam                      inte_wms_produtos.descr_tam                   %type;
  v_ordem_tam                      inte_wms_produtos.ordem_tam                   %type;
  v_descr_grupo                    inte_wms_produtos.descr_grupo                 %type;
  v_unidade_medida_sku             inte_wms_produtos.unidade_medida_sku          %type;
  v_peso_sku                       inte_wms_produtos.peso_sku                    %type;
  v_cubagem_sku                    inte_wms_produtos.cubagem_sku                 %type;
  v_timestamp_aimportar            inte_wms_produtos.timestamp_aimportar         %type;
  v_colecao_sku                    inte_wms_produtos.colecao_sku                 %type;
  v_desc_colecao_sku               inte_wms_produtos.desc_colecao_sku            %type;
  v_tipo_sku                       inte_wms_produtos.tipo_sku                    %type;
  v_codigo_unico_sku               inte_wms_produtos.codigo_unico_sku            %type;
  v_artigo_cotas_sku               inte_wms_produtos.artigo_cotas_sku            %type;
  v_descr_artigo_sku               inte_wms_produtos.descr_artigo_sku            %type;
  v_linha_produto_sku              inte_wms_produtos.linha_produto_sku           %type;
  v_descr_linha_sku                inte_wms_produtos.descr_linha_sku             %type;


  v_sid                            number(9);
  v_empresa                        number(3);
  v_usuario_systextil              varchar2(250);
  v_locale_usuario                 varchar2(5);
  v_nome_programa                  varchar2(20);
 

  v_operacao                       varchar(1);
  v_data_operacao                  date;
  v_usuario_rede                   varchar(20);
  v_maquina_rede                   varchar(40);
  v_aplicativo                     varchar(20);
begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();

   --alimenta as variaveis new caso seja insert
   if inserting
   then
      v_operacao := 'i';

      v_nivel_sku            := :new.nivel_sku;
      v_grupo_sku            := :new.grupo_sku;
      v_subgrupo_sku         := :new.subgrupo_sku;
      v_item_sku             := :new.item_sku;
      v_ean_sku              := :new.ean_sku;
      v_descricao_sku        := :new.descricao_sku;
      v_descr_tam            := :new.descr_tam;
      v_ordem_tam            := :new.ordem_tam;
      v_descr_grupo          := :new.descr_grupo;
      v_unidade_medida_sku   := :new.unidade_medida_sku;
      v_peso_sku             := :new.peso_sku;
      v_cubagem_sku          := :new.cubagem_sku;
      v_timestamp_aimportar  := :new.timestamp_aimportar;
      v_colecao_sku          := :new.colecao_sku;
      v_desc_colecao_sku     := :new.desc_colecao_sku;
      v_tipo_sku             := :new.tipo_sku;
      v_codigo_unico_sku     := :new.codigo_unico_sku;
      v_artigo_cotas_sku     := :new.artigo_cotas_sku;
      v_descr_artigo_sku     := :new.descr_artigo_sku;
      v_linha_produto_sku    := :new.linha_produto_sku;
      v_descr_linha_sku      := :new.descr_linha_sku;
      

   end if; --fim do if inserting or updating

   --alimenta as variaveis old caso seja insert ou update
   if deleting
   then
      v_operacao      := 'd';

      v_nivel_sku            := :old.nivel_sku;
      v_grupo_sku            := :old.grupo_sku;
      v_subgrupo_sku         := :old.subgrupo_sku;
      v_item_sku             := :old.item_sku;
      v_ean_sku              := :old.ean_sku;
      v_descricao_sku        := :old.descricao_sku;
      v_descr_tam            := :old.descr_tam;
      v_ordem_tam            := :old.ordem_tam;
      v_descr_grupo          := :old.descr_grupo;
      v_unidade_medida_sku   := :old.unidade_medida_sku;
      v_peso_sku             := :old.peso_sku;
      v_cubagem_sku          := :old.cubagem_sku;
      v_timestamp_aimportar  := :old.timestamp_aimportar;
      v_colecao_sku          := :old.colecao_sku;
      v_desc_colecao_sku     := :old.desc_colecao_sku;
      v_tipo_sku             := :old.tipo_sku;
      v_codigo_unico_sku     := :old.codigo_unico_sku;
      v_artigo_cotas_sku     := :old.artigo_cotas_sku;
      v_descr_artigo_sku     := :old.descr_artigo_sku;
      v_linha_produto_sku    := :old.linha_produto_sku;
      v_descr_linha_sku      := :old.descr_linha_sku;

   end if; --fim do if deleting or updating


   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);


    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente

   --insere na inte_wms_rfid_log o registro.
   insert into inte_wms_produtos_log (
      nivel_sku,               grupo_sku,
      subgrupo_sku,            item_sku,
      ean_sku,                 descricao_sku,   
      descr_tam,               ordem_tam,
      descr_grupo,
      unidade_medida_sku,      peso_sku,
      cubagem_sku,             timestamp_aimportar,
      colecao_sku,             desc_colecao_sku,
      tipo_sku,                codigo_unico_sku,
      operacao,
      data_operacao,           usuario_rede,
      maquina_rede,            aplicativo,
      nome_programa,           artigo_cotas_sku,
      descr_artigo_sku,        linha_produto_sku,
      descr_linha_sku
   )
   values (
      v_nivel_sku,             v_grupo_sku,
      v_subgrupo_sku,          v_item_sku,
      v_ean_sku,               v_descricao_sku,
      v_descr_tam,             v_ordem_tam,
      v_descr_grupo,
      v_unidade_medida_sku,    v_peso_sku,
      v_cubagem_sku,           v_timestamp_aimportar,
      v_colecao_sku,           v_desc_colecao_sku,
      v_tipo_sku,              v_codigo_unico_sku,
      v_operacao,
      v_data_operacao,         v_usuario_rede,
      v_maquina_rede,          v_aplicativo,
      v_nome_programa,         v_artigo_cotas_sku,
      v_descr_artigo_sku,      v_linha_produto_sku,
      v_descr_linha_sku
   );
   
            
end wms_tr_produtos_log;
/

execute inter_pr_recompile;

/* versao: 3 */


 exit;


 exit;


 exit;
