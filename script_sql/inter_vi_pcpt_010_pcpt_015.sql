CREATE OR REPLACE VIEW INTER_VI_PCPT_010_PCPT_015 AS
select PCPT_010.ORDEM_TECELAGEM,
       PCPT_015.FIO_NIVEL99,
       PCPT_015.FIO_GRUPO,
       PCPT_015.FIO_SUBGRUPO,
       PCPT_015.FIO_ITEM,
       PCPT_015.LOTE,
       PCPT_015.QTDE_TOTAL_FIO,
       (SELECT BASI_080.METRO_FIO_VOLTA
        from basi_080
        where basi_080.nivel_tecido     = PCPT_010.CD_PANO_NIVEL99
          and basi_080.grupo_tecido     = PCPT_010.CD_PANO_GRUPO
          and basi_080.subgrupo_tecido  = PCPT_010.CD_PANO_SUBGRUPO
          AND BASI_080.ITEM_TECIDO      = PCPT_010.CD_PANO_ITEM    
          AND BASI_080.ALTERNATIVA_TECI = PCPT_010.ALTERNATIVA_ITEM
          AND BASI_080.SEQ_ESTRU_TECIDO = PCPT_015.SEQ_ESTRUTURA    
          AND BASI_080.GRUPO_MAQ_TECIDO = PCPT_010.GRUPO_MAQUINA
          AND BASI_080.SUB_MAQ_TECIDO   = PCPT_010.SUBGRU_MAQUINA
       ) LFA
from PCPT_015, PCPT_010
where PCPT_010.ORDEM_TECELAGEM  = PCPT_015.ORDEM_TECELAGEM;
