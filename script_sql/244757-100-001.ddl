CREATE TABLE NFOR_010 (
  cod_negociacao          NUMBER(9) DEFAULT 0 NOT NULL,
  codigo_empresa          NUMBER(9),
  situacao                NUMBER(2) DEFAULT 0,
  data_negociacao         DATE,
  cnpj_forne9             NUMBER(9) DEFAULT 0,
  cnpj_forne4             NUMBER(4) DEFAULT 0,
  cnpj_forne2             NUMBER(2) DEFAULT 0,
  cond_pagamento          NUMBER(9) DEFAULT 0,
  perc_bonificado         NUMBER(5,2) DEFAULT 0.00,
  valor_frete             NUMBER(15,2) DEFAULT 0.00,
  usuario_digitacao       VARCHAR2(60) DEFAULT '',
  negociador              VARCHAR2(30) DEFAULT '',
  contato_fornecedor      VARCHAR2(60) DEFAULT '',
  observacao              VARCHAR2(4000) DEFAULT '',
  cod_cancelamento        NUMBER(2) DEFAULT 0,
  data_cancelamento       DATE,
  usuario_cancelamento    VARCHAR2(60) DEFAULT '',
  pedido_pai              NUMBER(9) DEFAULT 0
);

ALTER TABLE NFOR_010 ADD CONSTRAINT PK_NFOR_010 PRIMARY KEY (cod_negociacao);

ALTER TABLE NFOR_010
ADD CONSTRAINT REF_NFOR_010_FATU_500 FOREIGN KEY (codigo_empresa) REFERENCES FATU_500 (codigo_empresa);

ALTER TABLE NFOR_010
ADD CONSTRAINT REF_NFOR_010_SUPR_010 FOREIGN KEY (cnpj_forne9, cnpj_forne4, cnpj_forne2) REFERENCES SUPR_010 (FORNECEDOR9, FORNECEDOR4, FORNECEDOR2);

ALTER TABLE NFOR_010
ADD CONSTRAINT REF_NFOR_010_SUPR_050 FOREIGN KEY (cond_pagamento) REFERENCES SUPR_050 (COND_PGTO_COMPRA);

ALTER TABLE NFOR_010
ADD CONSTRAINT REF_NFOR_010_HDOC_030 FOREIGN KEY (usuario_digitacao, codigo_empresa) REFERENCES HDOC_030 (USUARIO, EMPRESA);

ALTER TABLE NFOR_010
ADD CONSTRAINT REF_NFOR_010_SUPR_040 FOREIGN KEY (cod_cancelamento) REFERENCES SUPR_040 (COD_CANC_COMPRA);

/

exec inter_pr_recompile;
