create or replace trigger inter_tr_basi_544
before insert or update on basi_544 for each row
declare
    ws_usuario_rede           varchar2(20);
    ws_maquina_rede           varchar2(40);
    ws_aplicativo             varchar2(20);
    ws_sid                    number(9);
    ws_empresa                number(3);
    ws_usuario_systextil      varchar2(250);
    ws_locale_usuario         varchar2(5);
    v_nome_programa           varchar2(20); 
begin
    -- Dados do usuario logado
    inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                            ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
    v_nome_programa := inter_fn_nome_programa(ws_sid);   

    if inserting
    then
        begin
            :new.data_cadastro := sysdate;
            :new.usuario_cadastro := ws_usuario_systextil;
            :new.processo_cadastro := v_nome_programa;
        end;
    end if;

    :new.data_alteracao := sysdate;
    :new.usuario_alteracao := ws_usuario_systextil;
    :new.processo_alteracao := v_nome_programa;
end;
/
