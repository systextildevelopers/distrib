CREATE TABLE EXPT_050 (
    VOLUME           NUMBER(9),
	QUANTIDADE       NUMBER(14,3) DEFAULT 0.0,
	SITUACAO         NUMBER(1) DEFAULT 0,
	DOCUMENTO        NUMBER(9),
	SERIE            VARCHAR2(3),
	SEQUENCIA        NUMBER(9) DEFAULT 0 NOT NULL,
	CGC9             NUMBER(9),
	CGC4             NUMBER(4),
	CGC2             NUMBER(2)
);

ALTER TABLE EXPT_050
ADD CONSTRAINT PK_EXPT_050 PRIMARY KEY (VOLUME);
