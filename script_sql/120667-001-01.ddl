create table BASI_071
(
  nivel              VARCHAR2(1) default '' not null,
  grupo              VARCHAR2(5) default '' not null,
  item               VARCHAR2(6) default '' not null,
  roteiro            NUMBER(2)   default 0  not null,
  subgrupo           VARCHAR2(3) default '' not null,
  alternativa        NUMBER(2)   default 0  not null,
  colecao            NUMBER(3)   default 0  not null
);
