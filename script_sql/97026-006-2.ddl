create table obrf_168 (
cod_empresa number(3) default 0,
tipo_titulo_dest number(2) default 0,
tipo_titulo_fcp number(2) default 0,
codigo_transacao number(3) default 0,
codigo_historico number(3) default 0,
cod_end_cobranca number(3) default 0,
codigo_contabil_dest  number(6) default 0,
codigo_contabil_fcp  number(6) default 0,
cnpj9 number(9) default 0,
cnpj4 number(4) default 0,
cnpj2 number(2) default 0,
gera_dup_apagar varchar2(1) default 'N');

comment on table obrf_168 is 'Par�metros destinados a gera��o de t�tulo do GNRE';
comment on column obrf_168.cod_empresa is 'C�digo da empresa que ir� gera o t�tulos GNRE';
comment on column obrf_168.tipo_titulo_dest is 'Tipo t�tulo destinatario GNRE';
comment on column obrf_168.tipo_titulo_fcp is 'Tipo t�tulo fundo de combate a probreza GNRE';
comment on column obrf_168.codigo_transacao is 'C�digo da transa��o GNRE';
comment on column obrf_168.codigo_historico is 'C�digo de Hinst�rico GNRE';
comment on column obrf_168.cod_end_cobranca is 'C�digo do endere�o de cobran�a GNRE';
comment on column obrf_168.codigo_contabil_dest is 'C�digo cont�bil destinatario GNRE';
comment on column obrf_168.codigo_contabil_fcp is 'C�digo cont�bil fundo de combate a pobreza GNRE';
comment on column obrf_168.cnpj9 is 'Cnpj para gera��o do t�tulo do GNRE (IMPOSTO A PAGAR)';
comment on column obrf_168.cnpj4 is 'Cnpj para gera��o do t�tulo do GNRE (IMPOSTO A PAGAR)';
comment on column obrf_168.cnpj2 is 'Cnpj para gera��o do t�tulo do GNRE (IMPOSTO A PAGAR)';
comment on column obrf_168.gera_dup_apagar is 'Indica se gera titulo a pagar';

exec inter_pr_recompile;
/
