Create table obrf_746 (
    COD_EMPRESA      number(03)    default 0    not null ,
    MES              number(02)    default 0    not null ,
    ANO              number(04)    default 0    not null ,
    UF               varchar2(02)  default ' '  not null,
    SEQ_LANCTO       number(9)     default 0    not null,
    COD_OR           varchar2(03)  default ' ',
    VLR_OR           number(13,2)  default 0.00,
    DT_VCTO          date,  
    COD_REC          varchar2(60)  default ' ',
    NUM_PROC         varchar2(15)  default ' ',
    IND_PROC         varchar2(1)   default ' ' ,
    PROC             varchar2(200) default ' ',
    TXT_COMPL        varchar2(200) default ' ' ,
    MES_REF          number(6)     default 0
     );
     
    
  alter table obrf_746
  add constraint PK_obrf_746 primary key (COD_EMPRESA, MES, ANO,UF,SEQ_LANCTO );
  
  comment on column obrf_746.COD_EMPRESA is 'Codigo de empresa para registro E310';
  comment on column obrf_746.MES is 'Mes para registro E316 filho 310';  
  comment on column obrf_746.ANO is 'ano para registro E316 filho 310';  
  comment on column obrf_746.UF is 'Estado para registro E316 filho 310';  
  comment on column obrf_746.SEQ_LANCTO is 'Sequencia para lancamento do registro E316 filho 310';  
  comment on column obrf_746.COD_OR     is 'Codigo da obriga��o recolhida ou a recolher, conforme a Tabela 5.4';    
  comment on column obrf_746.VLR_OR     is 'VALOR DO PROCESSO, para registo E316';
  comment on column obrf_746.DT_VCTO    is 'Data de vencimento, para registo E316';
  comment on column obrf_746.NUM_PROC   is 'Numero do processo ou auto de infracao ao qual a obrigacao esta vinculada, se houver';
  comment on column obrf_746.IND_PROC   is 'Indicador da origem do processo 0- Sefaz,1- Justi�a Federal,2- Justi�a Estadual,9- Outros';
  comment on column obrf_746.PROC       is 'Descri��o resumida do processo que embasou o lancamento';
  comment on column obrf_746.TXT_COMPL  is 'Complemento de descricao do processo';
  comment on column obrf_746.MES_REF  is 'mes ano referente ao processo';
   
  exec inter_pr_recompile ;
