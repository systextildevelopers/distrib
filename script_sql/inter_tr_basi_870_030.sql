
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_870_030" 
after insert or
       update of nivel, grupo, alternativa, sequencia,
	perc_composicao, desc_composicao, simbolo, parte_composicao
       or delete
  on basi_870
  for each row

begin

   --Quando for add um novo registro na basi_870(Ex: perc composicao, descricao, simbolo, parte_composicao,) ele estara fazendo um update na basi_030
   if inserting
   then
      begin
         UPDATE basi_030
         set basi_030.perc_composicao1     = decode(:new.sequencia,1,:new.perc_composicao,basi_030.perc_composicao1),
             basi_030.perc_composicao2     = decode(:new.sequencia,2,:new.perc_composicao,basi_030.perc_composicao2),
             basi_030.perc_composicao3     = decode(:new.sequencia,3,:new.perc_composicao,basi_030.perc_composicao3),
             basi_030.perc_composicao4     = decode(:new.sequencia,4,:new.perc_composicao,basi_030.perc_composicao4),
             basi_030.perc_composicao5     = decode(:new.sequencia,5,:new.perc_composicao,basi_030.perc_composicao5),
             basi_030.perc_composicao6     = decode(:new.sequencia,6,:new.perc_composicao,basi_030.perc_composicao6),
             basi_030.perc_composicao7     = decode(:new.sequencia,7,:new.perc_composicao,basi_030.perc_composicao7),
             basi_030.perc_composicao8     = decode(:new.sequencia,8,:new.perc_composicao,basi_030.perc_composicao8),
             basi_030.perc_composicao9     = decode(:new.sequencia,9,:new.perc_composicao,basi_030.perc_composicao9),
             basi_030.perc_composicao10    = decode(:new.sequencia,10,:new.perc_composicao,basi_030.perc_composicao10),
             basi_030.composicao_01        = decode(:new.sequencia,1,:new.desc_composicao,basi_030.composicao_01),
             basi_030.composicao_02        = decode(:new.sequencia,2,:new.desc_composicao,basi_030.composicao_02),
             basi_030.composicao_03        = decode(:new.sequencia,3,:new.desc_composicao,basi_030.composicao_03),
             basi_030.composicao_04        = decode(:new.sequencia,4,:new.desc_composicao,basi_030.composicao_04),
             basi_030.composicao_05        = decode(:new.sequencia,5,:new.desc_composicao,basi_030.composicao_05),
             basi_030.composicao_06        = decode(:new.sequencia,6,:new.desc_composicao,basi_030.composicao_06),
             basi_030.composicao_07        = decode(:new.sequencia,7,:new.desc_composicao,basi_030.composicao_07),
             basi_030.composicao_08        = decode(:new.sequencia,8,:new.desc_composicao,basi_030.composicao_08),
             basi_030.composicao_09        = decode(:new.sequencia,9,:new.desc_composicao,basi_030.composicao_09),
             basi_030.composicao_10        = decode(:new.sequencia,10,:new.desc_composicao,basi_030.composicao_10),
             basi_030.simbolo_1            = decode(:new.sequencia,1,:new.simbolo,basi_030.simbolo_1),
             basi_030.simbolo_2            = decode(:new.sequencia,2,:new.simbolo,basi_030.simbolo_2),
             basi_030.simbolo_3            = decode(:new.sequencia,3,:new.simbolo,basi_030.simbolo_3),
             basi_030.simbolo_4            = decode(:new.sequencia,4,:new.simbolo,basi_030.simbolo_4),
             basi_030.simbolo_5            = decode(:new.sequencia,5,:new.simbolo,basi_030.simbolo_5),
             basi_030.simbolo_6            = decode(:new.sequencia,6,:new.simbolo,basi_030.simbolo_6),
             basi_030.simbolo_7            = decode(:new.sequencia,7,:new.simbolo,basi_030.simbolo_7),
             basi_030.simbolo_8            = decode(:new.sequencia,8,:new.simbolo,basi_030.simbolo_8),
             basi_030.simbolo_9            = decode(:new.sequencia,9,:new.simbolo,basi_030.simbolo_9),
             basi_030.simbolo_10           = decode(:new.sequencia,10,:new.simbolo,basi_030.simbolo_10),
             basi_030.parte_composicao_01  = decode(:new.sequencia,1,:new.parte_composicao,basi_030.parte_composicao_01),
             basi_030.parte_composicao_02  = decode(:new.sequencia,2,:new.parte_composicao,basi_030.parte_composicao_02),
             basi_030.parte_composicao_03  = decode(:new.sequencia,3,:new.parte_composicao,basi_030.parte_composicao_03),
             basi_030.parte_composicao_04  = decode(:new.sequencia,4,:new.parte_composicao,basi_030.parte_composicao_04),
             basi_030.parte_composicao_05  = decode(:new.sequencia,5,:new.parte_composicao,basi_030.parte_composicao_05),
             basi_030.parte_composicao_06  = decode(:new.sequencia,6,:new.parte_composicao,basi_030.parte_composicao_06),
             basi_030.parte_composicao_07  = decode(:new.sequencia,7,:new.parte_composicao,basi_030.parte_composicao_07),
             basi_030.parte_composicao_08  = decode(:new.sequencia,8,:new.parte_composicao,basi_030.parte_composicao_08),
             basi_030.parte_composicao_09  = decode(:new.sequencia,9,:new.parte_composicao,basi_030.parte_composicao_09),
             basi_030.parte_composicao_10  = decode(:new.sequencia,10,:new.parte_composicao,basi_030.parte_composicao_10)
          where basi_030.nivel_estrutura   = :new.nivel
            and basi_030.referencia        = :new.grupo;
          EXCEPTION
             WHEN OTHERS THEN
             NULL;
      end;
   end if;

   if updating
   then
      begin
         if :new.perc_composicao <> :old.perc_composicao
         then
            begin
               UPDATE basi_030
               set  basi_030.perc_composicao1  = decode(:new.sequencia,1,:new.perc_composicao,basi_030.perc_composicao1),
                    basi_030.perc_composicao2  = decode(:new.sequencia,2,:new.perc_composicao,basi_030.perc_composicao2),
                    basi_030.perc_composicao3  = decode(:new.sequencia,3,:new.perc_composicao,basi_030.perc_composicao3),
                    basi_030.perc_composicao4  = decode(:new.sequencia,4,:new.perc_composicao,basi_030.perc_composicao4),
                    basi_030.perc_composicao5  = decode(:new.sequencia,5,:new.perc_composicao,basi_030.perc_composicao5),
                    basi_030.perc_composicao6  = decode(:new.sequencia,6,:new.perc_composicao,basi_030.perc_composicao6),
                    basi_030.perc_composicao7  = decode(:new.sequencia,7,:new.perc_composicao,basi_030.perc_composicao7),
                    basi_030.perc_composicao8  = decode(:new.sequencia,8,:new.perc_composicao,basi_030.perc_composicao8),
                    basi_030.perc_composicao9  = decode(:new.sequencia,9,:new.perc_composicao,basi_030.perc_composicao9),
                    basi_030.perc_composicao10 = decode(:new.sequencia,10,:new.perc_composicao,basi_030.perc_composicao10)
               where basi_030.nivel_estrutura    = :new.nivel
                 and basi_030.referencia         = :new.grupo;
               EXCEPTION
                  WHEN OTHERS THEN
                  NULL;
            end;
         end if;

         if :new.desc_composicao <> :new.desc_composicao
         then
            begin
               UPDATE basi_030
               set basi_030.composicao_01   = decode(:new.sequencia,1,:new.desc_composicao,basi_030.composicao_01),
                   basi_030.composicao_02   = decode(:new.sequencia,2,:new.desc_composicao,basi_030.composicao_02),
                   basi_030.composicao_03   = decode(:new.sequencia,3,:new.desc_composicao,basi_030.composicao_03),
                   basi_030.composicao_04   = decode(:new.sequencia,4,:new.desc_composicao,basi_030.composicao_04),
                   basi_030.composicao_05   = decode(:new.sequencia,5,:new.desc_composicao,basi_030.composicao_05),
                   basi_030.composicao_06   = decode(:new.sequencia,6,:new.desc_composicao,basi_030.composicao_06),
                   basi_030.composicao_07   = decode(:new.sequencia,7,:new.desc_composicao,basi_030.composicao_07),
                   basi_030.composicao_08   = decode(:new.sequencia,8,:new.desc_composicao,basi_030.composicao_08),
                   basi_030.composicao_09   = decode(:new.sequencia,9,:new.desc_composicao,basi_030.composicao_09),
                   basi_030.composicao_10   = decode(:new.sequencia,10,:new.desc_composicao,basi_030.composicao_10)
               where basi_030.nivel_estrutura = :new.nivel
                 and basi_030.referencia      = :new.grupo;
               EXCEPTION
                   WHEN OTHERS THEN
                   NULL;
            end;
         end if;

         if :new.simbolo <> :old.simbolo
         then
            begin
               UPDATE basi_030
               set basi_030.simbolo_1       = decode(:new.sequencia,1,:new.simbolo,basi_030.simbolo_1),
                   basi_030.simbolo_2       = decode(:new.sequencia,2,:new.simbolo,basi_030.simbolo_2),
                   basi_030.simbolo_3       = decode(:new.sequencia,3,:new.simbolo,basi_030.simbolo_3),
                   basi_030.simbolo_4       = decode(:new.sequencia,4,:new.simbolo,basi_030.simbolo_4),
                   basi_030.simbolo_5       = decode(:new.sequencia,5,:new.simbolo,basi_030.simbolo_5),
                   basi_030.simbolo_6       = decode(:new.sequencia,6,:new.simbolo,basi_030.simbolo_6),
                   basi_030.simbolo_7       = decode(:new.sequencia,7,:new.simbolo,basi_030.simbolo_7),
                   basi_030.simbolo_8       = decode(:new.sequencia,8,:new.simbolo,basi_030.simbolo_8),
                   basi_030.simbolo_9       = decode(:new.sequencia,9,:new.simbolo,basi_030.simbolo_9),
                   basi_030.simbolo_10      = decode(:new.sequencia,10,:new.simbolo,basi_030.simbolo_10)
               where basi_030.nivel_estrutura = :new.nivel
                 and basi_030.referencia      = :new.grupo;
               EXCEPTION
                  WHEN OTHERS THEN
                  NULL;
            end;
         end if;

         if :new.parte_composicao <> :old.parte_composicao
         then
            begin
               UPDATE basi_030
               set basi_030.parte_composicao_01 = decode(:new.sequencia,1,:new.parte_composicao,basi_030.parte_composicao_01),
                   basi_030.parte_composicao_02 = decode(:new.sequencia,2,:new.parte_composicao,basi_030.parte_composicao_02),
                   basi_030.parte_composicao_03 = decode(:new.sequencia,3,:new.parte_composicao,basi_030.parte_composicao_03),
                   basi_030.parte_composicao_04 = decode(:new.sequencia,4,:new.parte_composicao,basi_030.parte_composicao_04),
                   basi_030.parte_composicao_05 = decode(:new.sequencia,5,:new.parte_composicao,basi_030.parte_composicao_05),
                   basi_030.parte_composicao_06 = decode(:new.sequencia,6,:new.parte_composicao,basi_030.parte_composicao_06),
                   basi_030.parte_composicao_07 = decode(:new.sequencia,7,:new.parte_composicao,basi_030.parte_composicao_07),
                   basi_030.parte_composicao_08 = decode(:new.sequencia,8,:new.parte_composicao,basi_030.parte_composicao_08),
                   basi_030.parte_composicao_09 = decode(:new.sequencia,9,:new.parte_composicao,basi_030.parte_composicao_09),
                   basi_030.parte_composicao_10 = decode(:new.sequencia,10,:new.parte_composicao,basi_030.parte_composicao_10)
               where basi_030.nivel_estrutura     = :new.nivel
                 and basi_030.referencia          = :new.grupo;
               EXCEPTION
                  WHEN OTHERS THEN
                  NULL;
            end;
         end if;

      end;
   end if;

   --Ao eliminar um registro na tabela basi_870(Ex: perc composicao, descricao, simbolo, parte_composicao,) ele estara fazendo um update na basi_030
   if deleting
   then
      begin
         UPDATE basi_030
         set basi_030.perc_composicao1     = decode(:old.sequencia,1,0,basi_030.perc_composicao1),
             basi_030.perc_composicao2     = decode(:old.sequencia,2,0,basi_030.perc_composicao2),
             basi_030.perc_composicao3     = decode(:old.sequencia,3,0,basi_030.perc_composicao3),
             basi_030.perc_composicao4     = decode(:old.sequencia,4,0,basi_030.perc_composicao4),
             basi_030.perc_composicao5     = decode(:old.sequencia,5,0,basi_030.perc_composicao5),
             basi_030.perc_composicao6     = decode(:old.sequencia,6,0,basi_030.perc_composicao6),
             basi_030.perc_composicao7     = decode(:old.sequencia,7,0,basi_030.perc_composicao7),
             basi_030.perc_composicao8     = decode(:old.sequencia,8,0,basi_030.perc_composicao8),
             basi_030.perc_composicao9     = decode(:old.sequencia,9,0,basi_030.perc_composicao9),
             basi_030.perc_composicao10    = decode(:old.sequencia,10,0,basi_030.perc_composicao10),
             basi_030.composicao_01        = decode(:old.sequencia,1,'',basi_030.composicao_01),
             basi_030.composicao_02        = decode(:old.sequencia,2,'',basi_030.composicao_02),
             basi_030.composicao_03        = decode(:old.sequencia,3,'',basi_030.composicao_03),
             basi_030.composicao_04        = decode(:old.sequencia,4,'',basi_030.composicao_04),
             basi_030.composicao_05        = decode(:old.sequencia,5,'',basi_030.composicao_05),
             basi_030.composicao_06        = decode(:old.sequencia,6,'',basi_030.composicao_06),
             basi_030.composicao_07        = decode(:old.sequencia,7,'',basi_030.composicao_07),
             basi_030.composicao_08        = decode(:old.sequencia,8,'',basi_030.composicao_08),
             basi_030.composicao_09        = decode(:old.sequencia,9,'',basi_030.composicao_09),
             basi_030.composicao_10        = decode(:old.sequencia,10,'',basi_030.composicao_10),
             basi_030.simbolo_1            = decode(:old.sequencia,1,'',basi_030.simbolo_1),
             basi_030.simbolo_2            = decode(:old.sequencia,2,'',basi_030.simbolo_2),
             basi_030.simbolo_3            = decode(:old.sequencia,3,'',basi_030.simbolo_3),
             basi_030.simbolo_4            = decode(:old.sequencia,4,'',basi_030.simbolo_4),
             basi_030.simbolo_5            = decode(:old.sequencia,5,'',basi_030.simbolo_5),
             basi_030.simbolo_6            = decode(:old.sequencia,6,'',basi_030.simbolo_6),
             basi_030.simbolo_7            = decode(:old.sequencia,7,'',basi_030.simbolo_7),
             basi_030.simbolo_8            = decode(:old.sequencia,8,'',basi_030.simbolo_8),
             basi_030.simbolo_9            = decode(:old.sequencia,9,'',basi_030.simbolo_9),
             basi_030.simbolo_10           = decode(:old.sequencia,10,'',basi_030.simbolo_10),
             basi_030.parte_composicao_01  = decode(:old.sequencia,1,'',basi_030.parte_composicao_01),
             basi_030.parte_composicao_02  = decode(:old.sequencia,2,'',basi_030.parte_composicao_02),
             basi_030.parte_composicao_03  = decode(:old.sequencia,3,'',basi_030.parte_composicao_03),
             basi_030.parte_composicao_04  = decode(:old.sequencia,4,'',basi_030.parte_composicao_04),
             basi_030.parte_composicao_05  = decode(:old.sequencia,5,'',basi_030.parte_composicao_05),
             basi_030.parte_composicao_06  = decode(:old.sequencia,6,'',basi_030.parte_composicao_06),
             basi_030.parte_composicao_07  = decode(:old.sequencia,7,'',basi_030.parte_composicao_07),
             basi_030.parte_composicao_08  = decode(:old.sequencia,8,'',basi_030.parte_composicao_08),
             basi_030.parte_composicao_09  = decode(:old.sequencia,9,'',basi_030.parte_composicao_09),
             basi_030.parte_composicao_10  = decode(:old.sequencia,10,'',basi_030.parte_composicao_10)
          where basi_030.nivel_estrutura   = :old.nivel
            and basi_030.referencia        = :old.grupo;
          EXCEPTION
             WHEN OTHERS THEN
             NULL;
      end;
   end if;

end inter_tr_basi_870_030;
-- ALTER TRIGGER "INTER_TR_BASI_870_030" ENABLE
 

/

exec inter_pr_recompile;

