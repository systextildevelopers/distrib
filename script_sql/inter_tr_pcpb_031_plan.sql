
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_031_PLAN" 
   before insert or
          update of qtde_quilos_prog
   on pcpb_031
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_qtde_update             number(15,5);
   v_saldo_prog              number(15,5);
   v_alternativa             number(2);
   v_count_reg               number(7);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      v_saldo_prog    := :new.qtde_quilos_prog;
   else
      v_saldo_prog    := :new.qtde_quilos_prog - :old.qtde_quilos_prog;
   end if;

   if :new.alternativa_pano = 0
   then
   begin
      select pcpb_020.alternativa_item
      into   v_alternativa
      from pcpb_030, pcpb_020
      where pcpb_030.ordem_producao    = :new.ordem_producao
        and pcpb_030.pedido_corte      = 4
        and pcpb_030.nr_pedido_ordem   = :new.nr_pedido_ordem
        and pcpb_030.pano_nivel99      = :new.pano_nivel99
        and pcpb_030.pano_grupo        = :new.pano_grupo
        and pcpb_030.pano_subgrupo     = :new.pano_subgrupo
        and pcpb_030.pano_item         = :new.pano_item
        and pcpb_030.ordem_producao    = pcpb_020.ordem_producao
        and pcpb_030.sequencia         = pcpb_020.sequencia
        and pcpb_030.pano_nivel99      = pcpb_020.pano_sbg_nivel99
        and pcpb_030.pano_grupo        = pcpb_020.pano_sbg_grupo
        and pcpb_030.pano_subgrupo     = pcpb_020.pano_sbg_subgrupo
        and pcpb_030.pano_item         = pcpb_020.pano_sbg_item
        and rownum                     = 1;
   exception when no_data_found then
      v_alternativa := 0;
   end;
   else
      v_alternativa := :new.alternativa_pano;
   end if;

   begin
      delete tmrp_615
      where tmrp_615.nome_programa  = 'trigger_planejamento'
        and tmrp_615.nr_solicitacao = 888
        and tmrp_615.tipo_registro  = 888
        and tmrp_615.cgc_cliente9   = ws_sid;
   exception when others then
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   for reg_tmrp_630 in (select tmrp_630.ordem_planejamento,          tmrp_630.pedido_venda,
                               tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                               tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                               tmrp_625.alternativa_produto_origem,
                               tmrp_630.area_producao,               tmrp_630.ordem_prod_compra
                        from tmrp_630, tmrp_625
                        where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
                          and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
                          and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
                          and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
                          and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
                          and tmrp_625.item_produto_origem        = tmrp_630.item_produto
                          and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto

                          and tmrp_625.ordem_planejamento         = :new.ordem_planejamento
                          and tmrp_625.pedido_venda               = :new.pedido_venda

                          -- NIVEL 1
                          and tmrp_625.nivel_produto_origem       = :new.nivel_prod
                          and tmrp_625.grupo_produto_origem       = :new.grupo_prod
                          and tmrp_625.subgrupo_produto_origem    = :new.subgrupo_prod
                          and tmrp_625.item_produto_origem        = :new.item_prod

                          -- NIVEL 2
                          and tmrp_625.nivel_produto              = :new.pano_nivel99
                          and tmrp_625.grupo_produto              = :new.pano_grupo
                          and tmrp_625.subgrupo_produto           = :new.pano_subgrupo
                          and tmrp_625.item_produto               = :new.pano_item
                          and tmrp_625.alternativa_produto        = v_alternativa
                          and tmrp_630.ordem_prod_compra          = :new.nr_pedido_ordem
                          and tmrp_630.area_producao              = 1
                        group by tmrp_630.ordem_planejamento,          tmrp_630.pedido_venda,
                                 tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                 tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                 tmrp_625.alternativa_produto_origem,
                                 tmrp_630.area_producao,               tmrp_630.ordem_prod_compra)
   loop
      begin
         insert into tmrp_615 (
            ordem_prod,                               area_producao,
            nivel,                                    grupo,
            subgrupo,                                 item,
            alternativa,                              ordem_planejamento,
            pedido_venda,                             nome_programa,
            nr_solicitacao,                           tipo_registro,
            cgc_cliente9
         ) values (
            reg_tmrp_630.ordem_prod_compra,           reg_tmrp_630.area_producao,
            reg_tmrp_630.nivel_produto_origem,        reg_tmrp_630.grupo_produto_origem,
            reg_tmrp_630.subgrupo_produto_origem,     reg_tmrp_630.item_produto_origem,
            reg_tmrp_630.alternativa_produto_origem,  reg_tmrp_630.ordem_planejamento,
            reg_tmrp_630.pedido_venda,                'trigger_planejamento',
            888,                                      888,
            ws_sid
         );
      exception when others then
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end loop;

   for reg_ordens_planej in (select nvl(sum(tmrp_625.qtde_reserva_planejada),0) qtde_necessaria,
                                    tmrp_625.data_requerida
                             from tmrp_630, tmrp_625
                             where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
                               and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
                               and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
                               and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
                               and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
                               and tmrp_625.item_produto_origem        = tmrp_630.item_produto
                               and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto
                               and tmrp_625.ordem_planejamento         = :new.ordem_planejamento
                               and tmrp_625.pedido_venda               = :new.pedido_venda
                              -- NIVEL 1
                               and tmrp_625.nivel_produto_origem       = :new.nivel_prod
                               and tmrp_625.grupo_produto_origem       = :new.grupo_prod
                               and tmrp_625.subgrupo_produto_origem    = :new.subgrupo_prod
                               and tmrp_625.item_produto_origem        = :new.item_prod
                              -- NIVEL 2
                               and tmrp_625.nivel_produto              = :new.pano_nivel99
                               and tmrp_625.grupo_produto              = :new.pano_grupo
                               and tmrp_625.subgrupo_produto           = :new.pano_subgrupo
                               and tmrp_625.item_produto               = :new.pano_item
                               and tmrp_625.alternativa_produto        = v_alternativa
                               and tmrp_630.ordem_prod_compra          = :new.nr_pedido_ordem
                               and tmrp_630.area_producao              = 1
                             group by tmrp_625.data_requerida
                             order by tmrp_625.data_requerida)
   loop
      if reg_ordens_planej.qtde_necessaria > v_saldo_prog
      then
         v_qtde_update := v_saldo_prog;
         v_saldo_prog  := 0;
      else
         v_qtde_update := reg_ordens_planej.qtde_necessaria;
         v_saldo_prog  := v_saldo_prog - reg_ordens_planej.qtde_necessaria;
      end if;

      v_count_reg := 0;

      begin
         select nvl(count(1),0)
         into   v_count_reg
         from tmrp_630
         where tmrp_630.ordem_planejamento   = :new.ordem_planejamento
           and tmrp_630.pedido_venda         = :new.pedido_venda
           and tmrp_630.ordem_prod_compra    = :new.ordem_producao
           and tmrp_630.area_producao        = decode(:new.pano_nivel99,'2',2,7)
           and tmrp_630.nivel_produto        = :new.pano_nivel99
           and tmrp_630.grupo_produto        = :new.pano_grupo
           and tmrp_630.subgrupo_produto     = :new.pano_subgrupo
           and tmrp_630.item_produto         = :new.pano_item
           and tmrp_630.alternativa_produto  = v_alternativa;
      exception when others then
         v_count_reg := 0;
      end;

      if v_count_reg > 0
      then
         begin
            update tmrp_630
            set   tmrp_630.programado_comprado  = round(tmrp_630.programado_comprado + v_qtde_update,3)
            where tmrp_630.ordem_planejamento   = :new.ordem_planejamento
              and tmrp_630.pedido_venda         = :new.pedido_venda
              and tmrp_630.ordem_prod_compra    = :new.ordem_producao
              and tmrp_630.area_producao        = decode(:new.pano_nivel99,'2',2,7)
              and tmrp_630.nivel_produto        = :new.pano_nivel99
              and tmrp_630.grupo_produto        = :new.pano_grupo
              and tmrp_630.subgrupo_produto     = :new.pano_subgrupo
              and tmrp_630.item_produto         = :new.pano_item
              and tmrp_630.alternativa_produto  = v_alternativa;
         exception when others then
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      else
         begin
            insert into tmrp_630 (
               ordem_planejamento,                   area_producao,                   ordem_prod_compra,
               nivel_produto,                        grupo_produto,                   subgrupo_produto,
               item_produto,                         pedido_venda,                    programado_comprado,
               alternativa_produto
            ) values (
               :new.ordem_planejamento,              decode(:new.pano_nivel99,'2',2,7), :new.ordem_producao,
               :new.pano_nivel99,                      :new.pano_grupo,                 :new.pano_subgrupo,
               :new.pano_item,                       :new.pedido_venda,               round(v_qtde_update,3),
               v_alternativa
            );
         exception when others then
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end loop;

   if v_saldo_prog > 0
   then
      v_count_reg := 0;

      begin
         select nvl(count(1),0)
         into   v_count_reg
         from tmrp_630
         where tmrp_630.ordem_planejamento   = :new.ordem_planejamento
           and tmrp_630.pedido_venda         = :new.pedido_venda
           and tmrp_630.ordem_prod_compra    = :new.ordem_producao
           and tmrp_630.area_producao        = decode(:new.pano_nivel99,'2',2,7)
           and tmrp_630.nivel_produto        = :new.pano_nivel99
           and tmrp_630.grupo_produto        = :new.pano_grupo
           and tmrp_630.subgrupo_produto     = :new.pano_subgrupo
           and tmrp_630.item_produto         = :new.pano_item
           and tmrp_630.alternativa_produto  = v_alternativa;
      exception when others then
         v_count_reg := 0;
      end;

      if v_count_reg > 0
      then
         begin
            update tmrp_630
            set   tmrp_630.programado_comprado  = round(tmrp_630.programado_comprado + v_saldo_prog,3)
            where tmrp_630.ordem_planejamento   = :new.ordem_planejamento
              and tmrp_630.pedido_venda         = :new.pedido_venda
              and tmrp_630.ordem_prod_compra    = :new.ordem_producao
              and tmrp_630.area_producao        = decode(:new.pano_nivel99,'2',2,7)
              and tmrp_630.nivel_produto        = :new.pano_nivel99
              and tmrp_630.grupo_produto        = :new.pano_grupo
              and tmrp_630.subgrupo_produto     = :new.pano_subgrupo
              and tmrp_630.item_produto         = :new.pano_item
              and tmrp_630.alternativa_produto  = v_alternativa;
         exception when others then
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      else
         begin
            insert into tmrp_630 (
               ordem_planejamento,                   area_producao,                   ordem_prod_compra,
               nivel_produto,                        grupo_produto,                   subgrupo_produto,
               item_produto,                         pedido_venda,                    programado_comprado,
               alternativa_produto
            ) values (
               :new.ordem_planejamento,              decode(:new.pano_nivel99,'2',2,7), :new.ordem_producao,
               :new.pano_nivel99,                      :new.pano_grupo,                 :new.pano_subgrupo,
               :new.pano_item,                       :new.pedido_venda,               round(v_saldo_prog,3),
               v_alternativa
            );
         exception when others then
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;
end inter_tr_pcpb_031_plan;

-- ALTER TRIGGER "INTER_TR_PCPB_031_PLAN" ENABLE
 

/

exec inter_pr_recompile;

