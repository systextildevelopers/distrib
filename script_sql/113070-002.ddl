alter table obrf_015 add valor_cofins_importacao number(15,6);
alter table obrf_015 add cvf_cofins_importacao number(3);
alter table obrf_015 add valor_desp_aduan number(15,6);
alter table obrf_015 add perc_cofins_importacao number(15,6);
alter table obrf_015 add cvf_pis_importacao number(3);
alter table obrf_015 add base_calculo_importacao number(15,6);
alter table obrf_015 add valor_pis_importacao number(15,6);
alter table obrf_015 add st_flag_cce number(9);
alter table obrf_015 add cvf_ipi_importacao number(9);
alter table obrf_015 add valor_ipi_importacao number(15,6);
alter table obrf_015 add perc_pis_importacao number(15,6);
alter table obrf_015 add valor_unitario_novo number(15,6);
alter table obrf_015 add natitem_nat_dcip number(15,6);
alter table obrf_015 add valor_imposto_importacao number(15,6);
alter table obrf_015 add valor_iof_importacao number(15,6);
alter table obrf_015 add base_pis_cofins_importacao number(15,6);
alter table obrf_015 add base_ipi_importacao number(15,6);
alter table obrf_015 add perc_ipi_importacao number(15,6);

alter table obrf_015 modify valor_cofins_importacao default 0.00;
alter table obrf_015 modify cvf_cofins_importacao default 0;
alter table obrf_015 modify valor_desp_aduan default 0.00;
alter table obrf_015 modify perc_cofins_importacao default 0.00;
alter table obrf_015 modify cvf_pis_importacao default 0;
alter table obrf_015 modify base_calculo_importacao default 0.00;
alter table obrf_015 modify valor_pis_importacao default 0.00;
alter table obrf_015 modify st_flag_cce default 0;
alter table obrf_015 modify cvf_ipi_importacao default 0;
alter table obrf_015 modify valor_ipi_importacao default 0.00;
alter table obrf_015 modify perc_pis_importacao default 0.00;
alter table obrf_015 modify valor_unitario_novo default 0.00;
alter table obrf_015 modify natitem_nat_dcip default 0.00;
alter table obrf_015 modify valor_imposto_importacao default 0.00;
alter table obrf_015 modify valor_iof_importacao default 0.00;
alter table obrf_015 modify base_pis_cofins_importacao default 0.00;
alter table obrf_015 modify base_ipi_importacao default 0.00;
alter table obrf_015 modify perc_ipi_importacao default 0.00;

alter table fatu_050 add flag_copia_nf number(9);
alter table fatu_050 modify flag_copia_nf default 0;

alter table fatu_060 add valor_unitario_novo number(15,6);
alter table fatu_060 add cod_local  number(9);
alter table fatu_060 add pre_romaneio number(9);
alter table fatu_060 add ano_gerado number(4);
alter table fatu_060 add mes_gerado number(2);

alter table fatu_060 modify valor_unitario_novo default 0;
alter table fatu_060 modify cod_local  default 0;
alter table fatu_060 modify pre_romaneio default 0;
alter table fatu_060 modify ano_gerado default 0;
alter table fatu_060 modify mes_gerado default 0;

alter table obrf_821 add apura_est_quadro VARCHAR2(60);
alter table obrf_821 add tipo_insert varchar2(1);

alter table obrf_821 modify apura_est_quadro default '';
alter table obrf_821 modify tipo_insert default 'M';

alter table obrf_823 add tipo_insert varchar2(1);
ALTER TABLE obrf_823 MODIFY DESCR_AJUSTE53 varchar2(200) default '';

alter table obrf_823 modify tipo_insert default 'M';
   
/
exec inter_pr_recompile;
