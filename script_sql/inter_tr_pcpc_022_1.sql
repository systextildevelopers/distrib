
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_022_1" 
  before insert or
         update of qtde_programada
  on pcpc_022
  for each row
declare
   v_executa_trigger number;
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if  inserting
      or  updating
      and :new.qtde_programada_orig =  0
      and :old.qtde_programada      <> 0
      and :old.qtde_programada      <> :new.qtde_programada
      and :new.qtde_programada      <> 0
      then
         :new.qtde_programada_orig := :old.qtde_programada;
      end if;

      if :new.qtde_programada_orig is null
      then
         :new.qtde_programada_orig := 0;
      end if;

      INTER_PR_MARCA_REFERENCIA_OP('0', '00000', '000', '000000',
                                   :new.ordem_producao,
                                   1);
   end if;

end inter_tr_pcpc_022_1;


-- ALTER TRIGGER "INTER_TR_PCPC_022_1" ENABLE
 

/

exec inter_pr_recompile;

