-- Create table
create table TMRP_685
(
  inc_exc_sit_pecas   NUMBER(1),
  inc_exc_deposito    NUMBER(1),
  nr_projecao         NUMBER(9) default 0,
  inc_exc_pedidos     NUMBER(1),
  per_producao_fim    NUMBER(9),
  sit_peca1           NUMBER(2),
  sit_peca2           NUMBER(2),
  inc_exc_marca       NUMBER(1),
  sit_peca3           NUMBER(2),
  inc_exc_referencia  NUMBER(1),
  dep_est_peca2       NUMBER(3),
  tipo_dist_venda     NUMBER(1),
  qtde_minima_cor     NUMBER(9),
  rateio              NUMBER(1),
  gerar_est_peca      VARCHAR2(1),
  observacao2         VARCHAR2(60),
  qtde_estq_proc_cons NUMBER(1) default 0,
  dep_est_peca5       NUMBER(3),
  sit_peca5           NUMBER(2),
  dep_est_peca1       NUMBER(3),
  inc_exc_linha       NUMBER(1),
  per_producao_ini    NUMBER(9),
  inc_exc_natureza    NUMBER(1),
  qtde_minima_op      NUMBER(9),
  per_pedido_ini      NUMBER(9),
  nr_roteiro          NUMBER(9),
  inc_exc_atributos   NUMBER(1),
  forma_dist_ordens   NUMBER(1),
  per_ordem           NUMBER(9),
  observacao1         VARCHAR2(60),
  per_pedido_fim      NUMBER(9),
  qtde_maxima_op      NUMBER(9),
  sit_peca4           NUMBER(2),
  dep_est_peca4       NUMBER(3),
  agrup_ordens        NUMBER(1),
  nr_alternativa      NUMBER(9),
  multiplicaddor      NUMBER(2),
  inc_exc_colecao     NUMBER(1),
  agrupamento_item    NUMBER(1),
  dep_est_peca3       NUMBER(3),
  grade_colecao       NUMBER(3) default 0,
  pedidos_aimportar   NUMBER(1) default 0
);

alter table TMRP_685
  add constraint PK_TMRP_685 primary key (NR_PROJECAO);

alter table tmrp_685
add ( DEP_EST_PECA6       NUMBER(3),
      DEP_EST_PECA7       NUMBER(3),
      DEP_EST_PECA8       NUMBER(3),
      DEP_EST_PECA9       NUMBER(3),
      DEP_EST_PECA10      NUMBER(3));
/
exec inter_pr_recompile;
