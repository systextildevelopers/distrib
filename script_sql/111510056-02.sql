CREATE OR REPLACE TRIGGER "INTER_TR_OPER_277" 
before insert on oper_277
for each row

declare
  v_id number;
begin
   select SEQ_CONSULTA.nextval into v_id from dual;
   :new.id     				:= v_id;
   :new.data_hora 	:= sysdate;
end;
