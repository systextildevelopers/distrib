create table INTE_511
(
  CODIGO_EMPRESA NUMBER(3) default 0 not null,
  RESPONSAVEL9   NUMBER(9) default 0 not null,
  RESPONSAVEL4   NUMBER(4) default 0 not null,
  RESPONSAVEL2   NUMBER(2) default 0 not null,
  CENTRO_CUSTO   NUMBER(6) default 0,
  NAT_OPER       NUMBER(3) default 0,
  NAT_EST        VARCHAR2(3) default ''
);

ALTER TABLE inte_511 
ADD CONSTRAINT PK_INTE_511 PRIMARY KEY (CODIGO_EMPRESA, RESPONSAVEL9, RESPONSAVEL4, RESPONSAVEL2);
