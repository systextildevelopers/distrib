create or replace trigger inter_tr_fatu_509
   before insert
   on fatu_509
   for each row


declare
   ws_usuario_rede             varchar2(20);
   ws_maquina_rede             varchar2(40);
   ws_aplicativo               varchar2(20);
   ws_sid                      number(9);
   ws_empresa                  number(3);
   ws_usuario_systextil        varchar2(250);
   ws_locale_usuario           varchar2(5);
   ws_nome_programa            varchar2(20);

begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ws_nome_programa := inter_fn_nome_programa(ws_sid);                         
   
   :new.data_inicio := sysdate;
   :new.maquina_rede := ws_maquina_rede;
   :new.sid_oracle := ws_sid;
   :new.usuario_rede := ws_usuario_rede;
   :new.programa := ws_nome_programa;

end inter_tr_fatu_509;

/

exec inter_pr_recompile;

