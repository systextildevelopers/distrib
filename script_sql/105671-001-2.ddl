alter table mqop_050 add ind_estagio_gargalo number(1);

alter table mqop_050 modify ind_estagio_gargalo  default 0;

comment on column mqop_050.ind_estagio_gargalo is '1- Indica se o estagio sera estagio gargalo ou 0 - N�o';

exec inter_pr_recompile;
