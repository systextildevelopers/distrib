create table pcpb_162
(
  codigo_perfil NUMBER(9) default 0 not null,
  posicao       NUMBER(3) default 0,
  flag_antes    NUMBER(1) default 0,
  flag_depois   NUMBER(1) default 0,
  vip1          NUMBER(2) default 0,
  vip2          NUMBER(2) default 0,
  vip3          NUMBER(2) default 0,
  vip4          NUMBER(2) default 0,
  vip5          NUMBER(2) default 0,
  vip6          NUMBER(2) default 0
);

/

exec inter_pr_recompile;
