create table pedi_334
(
  representante NUMBER(5) default 0 not null,
  agrupador     NUMBER(4) default 0
);


alter table pedi_334
add  constraint pk_pedi_334 primary key (representante);

exec inter_pr_recompile;
