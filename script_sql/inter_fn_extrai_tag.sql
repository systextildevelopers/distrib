
  CREATE OR REPLACE FUNCTION "INTER_FN_EXTRAI_TAG" (tag in varchar2)
   return varchar2
is
   -- declara as variaveis para a extracao do tag
   i_extrai_nome_tag         number;
   tag_retorno               varchar2(255);
   tag_aux                   varchar2(1);

begin
   -- inicializa as variaveis necessarias para extracao do tag
   i_extrai_nome_tag      := 0;
   tag_retorno            := '';

   if tag is not null
   then
      -- loop para retirar o tag de toda a string que e passada pelo programa
      -- o caracter delimitador e #
      loop
         exit when Length(tag) = i_extrai_nome_tag;

         i_extrai_nome_tag := i_extrai_nome_tag + 1;

         tag_aux := '';
         tag_aux := Substr(tag,i_extrai_nome_tag,1);

         -- Quando for # ira sair do loop para, pois a tag ja esta completa.
         exit when tag_aux = '#';

         tag_retorno := tag_retorno || tag_aux;
      end loop;
   end if;

   return (tag_retorno);
end;

 

/

exec inter_pr_recompile;

