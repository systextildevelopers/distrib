INSERT INTO hdoc_035
( codigo_programa, programa_menu, 
 item_menu_def,   descricao)
VALUES
( 'basi_l400',     0,
 1,        'Ficha T�cnica de Insumos');
 
INSERT INTO hdoc_033
( usu_prg_cdusu, usu_prg_empr_usu, 
 programa,      nome_menu, 
 item_menu,     ordem_menu, 
 incluir,       modificar, 
 excluir,       procurar)
VALUES
( 'INTERSYS',    1, 
 'basi_l400',   'basi_menu', 
 1,             1, 
 'S',           'S', 
 'S',           'S');
 
INSERT INTO hdoc_033
( usu_prg_cdusu,  usu_prg_empr_usu, 
 programa,       nome_menu, 
 item_menu,      ordem_menu, 
 incluir,        modificar, 
 excluir,        procurar)
VALUES
( 'TREINAMENTO',  1, 
 'basi_l400',    'basi_menu', 
 1,              1, 
 'S',            'S', 
 'S',            'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Ficha T�cnica de Insumos'
 WHERE hdoc_036.codigo_programa = 'basi_l400'
   AND hdoc_036.locale          = 'es_ES';
   
commit;
