create or replace trigger inter_tr_pedi_421_log 
after insert or delete or update 
on PEDI_421 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    begin 
       select hdoc_090.programa 
       into v_nome_programa 
       from hdoc_090 
       where hdoc_090.sid = ws_sid 
         and rownum       = 1 
         and hdoc_090.programa not like '%menu%' 
         and hdoc_090.programa not like '%_m%'; 
       exception 
         when no_data_found then            v_nome_programa := 'SQL'; 
    end; 
 
 
 
 if inserting 
 then 
    begin 
 
        insert into pedi_421_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           pedido_venda_OLD,   /*8*/ 
           pedido_venda_NEW,   /*9*/ 
           seq_informacao_OLD,   /*10*/ 
           seq_informacao_NEW,   /*11*/ 
           tipo_informacao_OLD,   /*12*/ 
           tipo_informacao_NEW,   /*13*/ 
           data_informacao_OLD,   /*14*/ 
           data_informacao_NEW,   /*15*/ 
           informante_OLD,   /*16*/ 
           informante_NEW,   /*17*/ 
           descricao_OLD,   /*18*/ 
           descricao_NEW,   /*19*/ 
           data_complemento_OLD,   /*20*/ 
           data_complemento_NEW,   /*21*/ 
           usuario_OLD,   /*22*/ 
           usuario_NEW,   /*23*/ 
           hora_informacao_OLD,   /*24*/ 
           hora_informacao_NEW    /*25*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.pedido_venda, /*9*/   
           0,/*10*/
           :new.seq_informacao, /*11*/   
           0,/*12*/
           :new.tipo_informacao, /*13*/   
           null,/*14*/
           :new.data_informacao, /*15*/   
           '',/*16*/
           :new.informante, /*17*/   
           '',/*18*/
           :new.descricao, /*19*/   
           null,/*20*/
           :new.data_complemento, /*21*/   
           '',/*22*/
           :new.usuario, /*23*/   
           null,/*24*/
           :new.hora_informacao /*25*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into pedi_421_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           pedido_venda_OLD, /*8*/   
           pedido_venda_NEW, /*9*/   
           seq_informacao_OLD, /*10*/   
           seq_informacao_NEW, /*11*/   
           tipo_informacao_OLD, /*12*/   
           tipo_informacao_NEW, /*13*/   
           data_informacao_OLD, /*14*/   
           data_informacao_NEW, /*15*/   
           informante_OLD, /*16*/   
           informante_NEW, /*17*/   
           descricao_OLD, /*18*/   
           descricao_NEW, /*19*/   
           data_complemento_OLD, /*20*/   
           data_complemento_NEW, /*21*/   
           usuario_OLD, /*22*/   
           usuario_NEW, /*23*/   
           hora_informacao_OLD, /*24*/   
           hora_informacao_NEW  /*25*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.pedido_venda,  /*8*/  
           :new.pedido_venda, /*9*/   
           :old.seq_informacao,  /*10*/  
           :new.seq_informacao, /*11*/   
           :old.tipo_informacao,  /*12*/  
           :new.tipo_informacao, /*13*/   
           :old.data_informacao,  /*14*/  
           :new.data_informacao, /*15*/   
           :old.informante,  /*16*/  
           :new.informante, /*17*/   
           :old.descricao,  /*18*/  
           :new.descricao, /*19*/   
           :old.data_complemento,  /*20*/  
           :new.data_complemento, /*21*/   
           :old.usuario,  /*22*/  
           :new.usuario, /*23*/   
           :old.hora_informacao,  /*24*/  
           :new.hora_informacao  /*25*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into pedi_421_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           pedido_venda_OLD, /*8*/   
           pedido_venda_NEW, /*9*/   
           seq_informacao_OLD, /*10*/   
           seq_informacao_NEW, /*11*/   
           tipo_informacao_OLD, /*12*/   
           tipo_informacao_NEW, /*13*/   
           data_informacao_OLD, /*14*/   
           data_informacao_NEW, /*15*/   
           informante_OLD, /*16*/   
           informante_NEW, /*17*/   
           descricao_OLD, /*18*/   
           descricao_NEW, /*19*/   
           data_complemento_OLD, /*20*/   
           data_complemento_NEW, /*21*/   
           usuario_OLD, /*22*/   
           usuario_NEW, /*23*/   
           hora_informacao_OLD, /*24*/   
           hora_informacao_NEW /*25*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.pedido_venda, /*8*/   
           0, /*9*/
           :old.seq_informacao, /*10*/   
           0, /*11*/
           :old.tipo_informacao, /*12*/   
           0, /*13*/
           :old.data_informacao, /*14*/   
           null, /*15*/
           :old.informante, /*16*/   
           '', /*17*/
           :old.descricao, /*18*/   
           '', /*19*/
           :old.data_complemento, /*20*/   
           null, /*21*/
           :old.usuario, /*22*/   
           '', /*23*/
           :old.hora_informacao, /*24*/   
           null /*25*/
         );    
    end;    
 end if;    
end inter_tr_pedi_421_log;

/

exec inter_pr_recompile;
