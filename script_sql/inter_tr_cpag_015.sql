create or replace trigger "INTER_TR_CPAG_015" 
   before insert or
          delete or
          update of codigo_historico, valor_pago,
                    valor_juros,      valor_descontos,
                    valor_abatido,    vlr_var_cambial
   on cpag_015
   for each row

declare
   v_sinal_titulo      cont_010.sinal_titulo%type;

   v_tit_at            empr_001.ident_antecipado%type;
   v_tit_dv            empr_001.ident_devolucao%type;

   v_valor_pago        cpag_010.saldo_titulo%type;
   v_valor_pago_old    cpag_010.saldo_titulo%type;

   v_dupl_for_for_cli9 cpag_015.dupl_for_for_cli9%type;
   v_dupl_for_for_cli4 cpag_015.dupl_for_for_cli4%type;
   v_dupl_for_for_cli2 cpag_015.dupl_for_for_cli2%type;
   v_tipo_titulo       cpag_015.dupl_for_tipo_tit%type;
   v_num_titulo        cpag_015.dupl_for_nrduppag%type;
   v_parcela           cpag_015.dupl_for_no_parc%type;
   v_moeda_titulo      cpag_010.moeda_titulo%type;

   v_executa_trigger      number;
   v_lote              cpag_005.lote%type;
   v_data_lote         cpag_005.data_lote%type;
   v_saldo_juros       cpag_015.valor_juros%type;
   v_saldo_descontos   cpag_015.valor_descontos%type;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      -- busca configuracao global para identificacao do calculo
      -- conforme o tipo de titulo
      select empr_001.ident_antecipado, empr_001.ident_devolucao
      into   v_tit_at,                  v_tit_dv
      from empr_001;

      v_valor_pago     := 0.00;
      v_valor_pago_old := 0.00;

      if inserting or updating
      then
         select cont_010.sinal_titulo into v_sinal_titulo from cont_010
         where cont_010.codigo_historico = :new.codigo_historico;

         v_valor_pago := :new.valor_pago + :new.valor_abatido - :new.valor_juros + :new.valor_descontos - :new.vlr_var_cambial;

         -- SINAL TITULO: 1) pagamento / 2) estorno
         if v_sinal_titulo = 2
         then
            v_valor_pago := v_valor_pago * (-1.0);
         else
            if v_sinal_titulo = 0
            then
               v_valor_pago := 0.00;
            end if;
         end if;

         v_dupl_for_for_cli9 := :new.dupl_for_for_cli9;
         v_dupl_for_for_cli4 := :new.dupl_for_for_cli4;
         v_dupl_for_for_cli2 := :new.dupl_for_for_cli2;
         v_tipo_titulo       := :new.dupl_for_tipo_tit;
         v_num_titulo        := :new.dupl_for_nrduppag;
         v_parcela           := :new.dupl_for_no_parc;

         begin
            select cpag_010.moeda_titulo, cpag_010.lote, cpag_010.data_lote
            into   v_moeda_titulo,        v_lote,        v_data_lote
            from cpag_010
            where cpag_010.cgc_9        = v_dupl_for_for_cli9
              and cpag_010.cgc_4        = v_dupl_for_for_cli4
              and cpag_010.cgc_2        = v_dupl_for_for_cli2
              and cpag_010.tipo_titulo  = v_tipo_titulo
              and cpag_010.nr_duplicata = v_num_titulo
              and cpag_010.parcela      = v_parcela;
         exception
            when no_data_found then
              v_moeda_titulo := 0;
              v_lote         := 0;
              v_data_lote    := trunc(sysdate);
         end;

         if v_moeda_titulo = 0
         then
            if :old.vlr_juros_moeda = 0.00 or :old.vlr_juros_moeda is null
            then
               :new.vlr_juros_moeda := :new.valor_juros;
            end if;

            if :old.vlr_descontos_moeda = 0.00 or :old.vlr_descontos_moeda is null
            then
               :new.vlr_descontos_moeda := :new.valor_descontos;
            end if;

            if :old.vlr_abatido_moeda = 0.00 or :old.vlr_abatido_moeda is null
            then
               :new.vlr_abatido_moeda := :new.valor_abatido;
            end if;
         end if;

         if v_lote > 0
         then
           if inserting
           then
             BEGIN
                 update cpag_005
                 set cpag_005.valor_total_apagar = (cpag_005.valor_total_apagar - :new.valor_descontos) + :new.valor_juros
                 where cpag_005.lote = v_lote
                   and cpag_005.data_lote = v_data_lote;
              EXCEPTION
                 WHEN OTHERS THEN
                    raise_application_error(-20000,chr(10)||chr(10)||'ATENCAO! Nao atualizou cpag_005( valor do lote )');
              END;
           else
              if updating
              then
                 v_saldo_juros     := :new.valor_juros - :old.valor_juros;
                 v_saldo_descontos := :new.valor_descontos - :old.valor_descontos;

                 BEGIN
                   update cpag_005
                   set cpag_005.valor_total_apagar = (cpag_005.valor_total_apagar - v_saldo_descontos) +  v_saldo_juros
                   where cpag_005.lote = v_lote
                     and cpag_005.data_lote = v_data_lote;
                 EXCEPTION
                    WHEN OTHERS THEN
                       raise_application_error(-20000,chr(10)||chr(10)||'ATENCAO! Nao atualizou cpag_005( valor do lote )');
                 END;
              end if;
           end if;
         end if;
      end if;

      if deleting or updating
      then
         begin
            select cont_010.sinal_titulo
            into v_sinal_titulo
            from cont_010
            where cont_010.codigo_historico = :old.codigo_historico;
         end;

         v_valor_pago_old := :old.valor_pago + :old.valor_abatido - :old.valor_juros + :old.valor_descontos - :old.vlr_var_cambial;

         -- SINAL TITULO: 1) pagamento / 2) estorno
         if v_sinal_titulo = 2
         then
            v_valor_pago_old := v_valor_pago_old * (-1.0);
         else
            if v_sinal_titulo = 0
            then
               v_valor_pago_old := 0.00;
            end if;
         end if;

         v_dupl_for_for_cli9 := :old.dupl_for_for_cli9;
         v_dupl_for_for_cli4 := :old.dupl_for_for_cli4;
         v_dupl_for_for_cli2 := :old.dupl_for_for_cli2;
         v_tipo_titulo       := :old.dupl_for_tipo_tit;
         v_num_titulo        := :old.dupl_for_nrduppag;
         v_parcela           := :old.dupl_for_no_parc;


      end if;

      if v_valor_pago <> 0.00
      then
         update cpag_010
         set saldo_titulo = saldo_titulo - v_valor_pago + v_valor_pago_old
         where cpag_010.cgc_9        = v_dupl_for_for_cli9
           and cpag_010.cgc_4        = v_dupl_for_for_cli4
           and cpag_010.cgc_2        = v_dupl_for_for_cli2
           and cpag_010.tipo_titulo  = v_tipo_titulo
           and cpag_010.nr_duplicata = v_num_titulo
           and cpag_010.parcela      = v_parcela;
      end if;
   end if;
end inter_tr_cpag_015;
-- ALTER TRIGGER "INTER_TR_CPAG_015" ENABLE

 

/

exec inter_pr_recompile;

