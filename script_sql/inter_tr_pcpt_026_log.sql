CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_026_LOG"
AFTER INSERT OR
    DELETE OR
    UPDATE of rolo_fio_per_tece, rolo_fio_nr_rolo , fio_rolo_nivel99,
    fio_rolo_grupo, fio_rolo_subgrupo, fio_rolo_item,
    lote_fio, quantidade_fio, valor_fio,
    codigo_rolo, estagio, deposito,
    data_cardex, transacao_cardex, usuario_cardex,
    nome_programa, sequencia_fio, rolo_consumo,
    centro_custo, executa_trigger
    on pcpt_026
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   v_nome_programa           varchar2(20);
begin

   -- Dados do usuario logado
  inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                          ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

  v_nome_programa := inter_fn_nome_programa(ws_sid);

 if inserting
 then
    begin
        insert into pcpt_026_log (
            tipo_ocorr, /*0*/
            data_ocorr, /*1*/
            hora_ocorr, /*2*/
            usuario_rede, /*3*/
            maquina_rede, /*4*/
            aplicacao, /*5*/
            usuario_sistema, /*6*/
            nome_programa, /*7*/
            rolo_fio_per_tece_old, /*8*/
            rolo_fio_per_tece_new, /*9*/
            rolo_fio_nr_rolo_old, /*10*/
            rolo_fio_nr_rolo_new, /*11*/
            fio_rolo_nivel99_old, /*12*/
            fio_rolo_nivel99_new, /*13*/
            fio_rolo_grupo_old, /*14*/
            fio_rolo_grupo_new, /*15*/
            fio_rolo_subgrupo_old, /*16*/
            fio_rolo_subgrupo_new, /*17*/
            fio_rolo_item_old, /*18*/
            fio_rolo_item_new, /*19*/
            lote_fio_old, /*20*/
            lote_fio_new, /*21*/
            quantidade_fio_old, /*22*/
            quantidade_fio_new, /*23*/
            valor_fio_old, /*24*/
            valor_fio_new, /*25*/
            codigo_rolo_old, /*26*/
            codigo_rolo_new, /*27*/
            estagio_old, /*28*/
            estagio_new, /*29*/
            deposito_old, /*30*/
            deposito_new, /*31*/
            data_cardex_old, /*32*/
            data_cardex_new,/*33*/
            transacao_cardex_old, /*34*/
            transacao_cardex_new, /*35*/
            usuario_cardex_old, /*36*/
            usuario_cardex_new, /*37*/
            nome_programa_old, /*38*/
            nome_programa_new, /*39*/
            sequencia_fio_old, /*40*/
            sequencia_fio_new, /*41*/
            rolo_consumo_old, /*42*/
            rolo_consumo_new, /*43*/
            centro_custo_old, /*44*/
            centro_custo_new, /*45*/
            executa_trigger_old, /*46*/
            executa_trigger_new /*47*/
        ) values (
            'i', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
            null,  /*8*/
            :new.rolo_fio_per_tece, /*9*/
            null,  /*10*/
            :new.rolo_fio_nr_rolo, /*11*/
            '',  /*12*/
            :new.fio_rolo_nivel99, /*13*/
            '',  /*14*/
            :new.fio_rolo_grupo, /*15*/
            '',  /*16*/
            :new.fio_rolo_subgrupo, /*17*/
            '',  /*18*/
            :new.fio_rolo_item, /*19*/
            null,  /*20*/
            :new.lote_fio, /*21*/
            null,  /*22*/
            :new.quantidade_fio, /*23*/
            null,  /*24*/
            :new.valor_fio, /*25*/
            null,  /*26*/
            :new.codigo_rolo, /*27*/
            null,  /*28*/
            :new.estagio, /*29*/
            null,  /*30*/
            :new.deposito, /*31*/
            null,  /*32*/
            :new.data_cardex, /*33*/
            null,  /*34*/
            :new.transacao_cardex, /*35*/
            '',  /*36*/
            :new.usuario_cardex, /*37*/
            '',  /*38*/
            :new.nome_programa, /*39*/
            null,  /*40*/
            :new.sequencia_fio, /*41*/
            null,  /*42*/
            :new.rolo_consumo, /*43*/
            null,  /*44*/
            :new.centro_custo, /*45*/
            null,  /*46*/
            :new.executa_trigger /*47*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into pcpt_026_log (
            tipo_ocorr, /*0*/
            data_ocorr, /*1*/
            hora_ocorr, /*2*/
            usuario_rede, /*3*/
            maquina_rede, /*4*/
            aplicacao, /*5*/
            usuario_sistema, /*6*/
            nome_programa, /*7*/
            rolo_fio_per_tece_old, /*8*/
            rolo_fio_per_tece_new, /*9*/
            rolo_fio_nr_rolo_old, /*10*/
            rolo_fio_nr_rolo_new, /*11*/
            fio_rolo_nivel99_old, /*12*/
            fio_rolo_nivel99_new, /*13*/
            fio_rolo_grupo_old, /*14*/
            fio_rolo_grupo_new, /*15*/
            fio_rolo_subgrupo_old, /*16*/
            fio_rolo_subgrupo_new, /*17*/
            fio_rolo_item_old, /*18*/
            fio_rolo_item_new, /*19*/
            lote_fio_old, /*20*/
            lote_fio_new, /*21*/
            quantidade_fio_old, /*22*/
            quantidade_fio_new, /*23*/
            valor_fio_old, /*24*/
            valor_fio_new, /*25*/
            codigo_rolo_old, /*26*/
            codigo_rolo_new, /*27*/
            estagio_old, /*28*/
            estagio_new, /*29*/
            deposito_old, /*30*/
            deposito_new, /*31*/
            data_cardex_old, /*32*/
            data_cardex_new,/*33*/
            transacao_cardex_old, /*34*/
            transacao_cardex_new, /*35*/
            usuario_cardex_old, /*36*/
            usuario_cardex_new, /*37*/
            nome_programa_old, /*38*/
            nome_programa_new, /*39*/
            sequencia_fio_old, /*40*/
            sequencia_fio_new, /*41*/
            rolo_consumo_old, /*42*/
            rolo_consumo_new, /*43*/
            centro_custo_old, /*44*/
            centro_custo_new, /*45*/
            executa_trigger_old, /*46*/
            executa_trigger_new /*47*/
        ) values (
            'a', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.rolo_fio_per_tece,  /*8*/
           :new.rolo_fio_per_tece, /*9*/
           :old.rolo_fio_nr_rolo,  /*10*/
           :new.rolo_fio_nr_rolo, /*11*/
           :old.fio_rolo_nivel99,  /*12*/
           :new.fio_rolo_nivel99, /*13*/
           :old.fio_rolo_grupo,  /*14*/
           :new.fio_rolo_grupo, /*15*/
           :old.fio_rolo_subgrupo,  /*16*/
           :new.fio_rolo_subgrupo, /*17*/
           :old.fio_rolo_item,  /*18*/
           :new.fio_rolo_item, /*19*/
           :old.lote_fio,  /*20*/
           :new.lote_fio, /*21*/
           :old.quantidade_fio,  /*22*/
           :new.quantidade_fio, /*23*/
           :old.valor_fio,  /*24*/
           :new.valor_fio, /*25*/
           :old.codigo_rolo,  /*26*/
           :new.codigo_rolo, /*27*/
           :old.estagio,  /*28*/
           :new.estagio, /*29*/
           :old.deposito,  /*30*/
           :new.deposito, /*31*/
           :old.data_cardex,  /*32*/
           :new.data_cardex, /*33*/
           :old.transacao_cardex,  /*34*/
           :new.transacao_cardex, /*35*/
           :old.usuario_cardex,  /*36*/
           :new.usuario_cardex, /*37*/
           :old.nome_programa,  /*38*/
           :new.nome_programa, /*39*/
           :old.sequencia_fio,  /*40*/
           :new.sequencia_fio, /*41*/
           :old.rolo_consumo,  /*42*/
           :new.rolo_consumo, /*43*/
           :old.centro_custo,  /*44*/
           :new.centro_custo, /*45*/
           :old.executa_trigger,  /*46*/
           :new.executa_trigger /*47*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into pcpt_026_log (
            tipo_ocorr, /*0*/
            data_ocorr, /*1*/
            hora_ocorr, /*2*/
            usuario_rede, /*3*/
            maquina_rede, /*4*/
            aplicacao, /*5*/
            usuario_sistema, /*6*/
            nome_programa, /*7*/
            rolo_fio_per_tece_old, /*8*/
            rolo_fio_per_tece_new, /*9*/
            rolo_fio_nr_rolo_old, /*10*/
            rolo_fio_nr_rolo_new, /*11*/
            fio_rolo_nivel99_old, /*12*/
            fio_rolo_nivel99_new, /*13*/
            fio_rolo_grupo_old, /*14*/
            fio_rolo_grupo_new, /*15*/
            fio_rolo_subgrupo_old, /*16*/
            fio_rolo_subgrupo_new, /*17*/
            fio_rolo_item_old, /*18*/
            fio_rolo_item_new, /*19*/
            lote_fio_old, /*20*/
            lote_fio_new, /*21*/
            quantidade_fio_old, /*22*/
            quantidade_fio_new, /*23*/
            valor_fio_old, /*24*/
            valor_fio_new, /*25*/
            codigo_rolo_old, /*26*/
            codigo_rolo_new, /*27*/
            estagio_old, /*28*/
            estagio_new, /*29*/
            deposito_old, /*30*/
            deposito_new, /*31*/
            data_cardex_old, /*32*/
            data_cardex_new,/*33*/
            transacao_cardex_old, /*34*/
            transacao_cardex_new, /*35*/
            usuario_cardex_old, /*36*/
            usuario_cardex_new, /*37*/
            nome_programa_old, /*38*/
            nome_programa_new, /*39*/
            sequencia_fio_old, /*40*/
            sequencia_fio_new, /*41*/
            rolo_consumo_old, /*42*/
            rolo_consumo_new, /*43*/
            centro_custo_old, /*44*/
            centro_custo_new, /*45*/
            executa_trigger_old, /*46*/
            executa_trigger_new /*47*/
        ) values (
             'd', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
            :old.rolo_fio_per_tece,  /*8*/
            null, /*9*/
            :old.rolo_fio_nr_rolo,  /*10*/
            null, /*11*/
            :old.fio_rolo_nivel99,  /*12*/
            '', /*13*/
            :old.fio_rolo_grupo,  /*14*/
            '', /*15*/
            :old.fio_rolo_subgrupo,  /*16*/
            '', /*17*/
            :old.fio_rolo_item,  /*18*/
            '', /*19*/
            :old.lote_fio,  /*20*/
            null, /*21*/
            :old.quantidade_fio,  /*22*/
            null, /*23*/
            :old.valor_fio,  /*24*/
            null, /*25*/
            :old.codigo_rolo,  /*26*/
            null, /*27*/
            :old.estagio,  /*28*/
            null, /*29*/
            :old.deposito,  /*30*/
            null, /*31*/
            :old.data_cardex,  /*32*/
            null, /*33*/
            :old.transacao_cardex,  /*34*/
            null, /*35*/
            :old.usuario_cardex,  /*36*/
            '', /*37*/
            :old.nome_programa,  /*38*/
            '', /*39*/
            :old.sequencia_fio,  /*40*/
            null, /*41*/
            :old.rolo_consumo,  /*42*/
            null, /*43*/
            :old.centro_custo,  /*44*/
            null, /*45*/
            :old.executa_trigger,  /*46*/
            null /*47*/
         );
    end;
 end if;
end inter_tr_pcpt_026_log;

-- ALTER TRIGGER "INTER_TR_PCPT_026_LOG" ENABLE
 

/

exec inter_pr_recompile;

