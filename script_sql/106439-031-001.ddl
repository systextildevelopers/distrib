alter table EFIC_100 
add (NIVEL_MAT_PRIMA   VARCHAR2(1) default '0',
     GRUPO_MAT_PRIMA   VARCHAR2(5) default '00000',
     SUB_MAT_PRIMA     VARCHAR2(3) default '000',
     ITEM_MAT_PRIMA    VARCHAR2(6) default '000000');
     
/

exec inter_pr_recompile;
