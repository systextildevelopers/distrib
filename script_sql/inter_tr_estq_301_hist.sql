CREATE OR REPLACE TRIGGER INTER_TR_ESTQ_301_HIST
   after delete or
   update of
      codigo_deposito,
      nivel_estrutura,
      grupo_estrutura,
      subgrupo_estrutura,
      item_estrutura,
      mes_movimento,
      ano_movimento,
      mes_ano_movimento,
      saldo_fisico,
      saldo_financeiro,
      preco_medio_unitario,
      preco_custo_unitario,
      saldo_financeiro_estimado,
      preco_medio_unit_estimado,
      preco_custo_unit_estimado,
      saldo_financeiro_proj,
      preco_medio_unit_proj,
      preco_custo_unit_proj,
      saldo_fisico_quilo,
      conta_contabil
   on estq_301
   for each row

-- Finalidade: Registrar altera¿¿¿¿¿¿es feitas na tabela estq_301 - Resumo do cardex
-- Autor.....: Edson Pio
-- Data......: 05/06/21
--
-- Hist¿¿¿ricos de altera¿¿¿¿¿¿es na trigger
--
-- Data      Autor           SS          Observa¿¿¿¿¿¿es

declare
   v_codigo_deposito_old                        estq_301.codigo_deposito%type;
   v_codigo_deposito_new                        estq_301.codigo_deposito%type;
   v_nivel_estrutura_old                        estq_301.nivel_estrutura%type;
   v_nivel_estrutura_new                        estq_301.nivel_estrutura%type;
   v_grupo_estrutura_old                        estq_301.grupo_estrutura%type;
   v_grupo_estrutura_new                        estq_301.grupo_estrutura%type;
   v_subgrupo_estrutura_old                     estq_301.subgrupo_estrutura%type;
   v_subgrupo_estrutura_new                     estq_301.subgrupo_estrutura%type;
   v_item_estrutura_old                         estq_301.item_estrutura%type;
   v_item_estrutura_new                         estq_301.item_estrutura%type;
   v_mes_movimento_old                          estq_301.mes_movimento%type;
   v_mes_movimento_new                          estq_301.mes_movimento%type;
   v_ano_movimento_old                          estq_301.ano_movimento%type;
   v_ano_movimento_new                          estq_301.ano_movimento%type;
   v_mes_ano_movimento_old                      estq_301.mes_ano_movimento%type;
   v_mes_ano_movimento_new                      estq_301.mes_ano_movimento%type;
   v_saldo_fisico_old                           estq_301.saldo_fisico%type;
   v_saldo_fisico_new                           estq_301.saldo_fisico%type;
   v_saldo_financeiro_old                       estq_301.saldo_financeiro%type;
   v_saldo_financeiro_new                       estq_301.saldo_financeiro%type;
   v_preco_medio_unitario_old                   estq_301.preco_medio_unitario%type;
   v_preco_medio_unitario_new                   estq_301.preco_medio_unitario%type;
   v_preco_custo_unitario_old                   estq_301.preco_custo_unitario%type;
   v_preco_custo_unitario_new                   estq_301.preco_custo_unitario%type;
   v_saldo_financeiro_est_old                   estq_301.saldo_financeiro_estimado%type;
   v_saldo_financeiro_est_new                   estq_301.saldo_financeiro_estimado%type;
   v_preco_medio_unit_est_old                   estq_301.preco_medio_unit_estimado%type;
   v_preco_medio_unit_est_new                   estq_301.preco_medio_unit_estimado%type;
   v_preco_custo_unit_est_old                   estq_301.preco_custo_unit_estimado%type;
   v_preco_custo_unit_est_new                   estq_301.preco_custo_unit_estimado%type;
   v_saldo_financeiro_proj_old                  estq_301.saldo_financeiro_proj%type;
   v_saldo_financeiro_proj_new                  estq_301.saldo_financeiro_proj%type;
   v_preco_medio_unit_proj_old                  estq_301.preco_medio_unit_proj%type;
   v_preco_medio_unit_proj_new                  estq_301.preco_medio_unit_proj%type;
   v_preco_custo_unit_proj_old                  estq_301.preco_custo_unit_proj%type;
   v_preco_custo_unit_proj_new                  estq_301.preco_custo_unit_proj%type;
   v_saldo_fisico_quilo_old                     estq_301.saldo_fisico_quilo%type;
   v_saldo_fisico_quilo_new                     estq_301.saldo_fisico_quilo%type;
   v_conta_contabil_old                         estq_301.conta_contabil%type;
   v_conta_contabil_new                         estq_301.conta_contabil%type;

   v_tipo_ocorr                   varchar2 (1);
   v_usuario_rede                 varchar2(20);
   v_maquina_rede                 varchar2(40);
   v_aplicativo                   varchar2(20);
   v_sid                          number(9);
   v_empresa                      number(3);
   v_usuario_systextil            varchar2(250);
   v_locale_usuario               varchar2(5);
   v_nome_programa                varchar2(20);

begin
   if UPDATING
   then
       v_tipo_ocorr                                 := 'A';
       v_codigo_deposito_old                        := :old.codigo_deposito;
       v_codigo_deposito_new                        := :new.codigo_deposito;
       v_nivel_estrutura_old                        := :old.nivel_estrutura;
       v_nivel_estrutura_new                        := :new.nivel_estrutura;
       v_grupo_estrutura_old                        := :old.grupo_estrutura;
       v_grupo_estrutura_new                        := :new.grupo_estrutura;
       v_subgrupo_estrutura_old                     := :old.subgrupo_estrutura;
       v_subgrupo_estrutura_new                     := :new.subgrupo_estrutura;
       v_item_estrutura_old                         := :old.item_estrutura;
       v_item_estrutura_new                         := :new.item_estrutura;
       v_mes_movimento_old                          := :old.mes_movimento;
       v_mes_movimento_new                          := :new.mes_movimento;
       v_ano_movimento_old                          := :old.ano_movimento;
       v_ano_movimento_new                          := :new.ano_movimento;
       v_mes_ano_movimento_old                      := :old.mes_ano_movimento;
       v_mes_ano_movimento_new                      := :new.mes_ano_movimento;
       v_saldo_fisico_old                           := :old.saldo_fisico;
       v_saldo_fisico_new                           := :new.saldo_fisico;
       v_saldo_financeiro_old                       := :old.saldo_financeiro;
       v_saldo_financeiro_new                       := :new.saldo_financeiro;
       v_preco_medio_unitario_old                   := :old.preco_medio_unitario;
       v_preco_medio_unitario_new                   := :new.preco_medio_unitario;
       v_preco_custo_unitario_old                   := :old.preco_custo_unitario;
       v_preco_custo_unitario_new                   := :new.preco_custo_unitario;
       v_saldo_financeiro_est_old                   := :old.saldo_financeiro_estimado;
       v_saldo_financeiro_est_new                   := :new.saldo_financeiro_estimado;
       v_preco_medio_unit_est_old                   := :old.preco_medio_unit_estimado;
       v_preco_medio_unit_est_new                   := :new.preco_medio_unit_estimado;
       v_preco_custo_unit_est_old                   := :old.preco_custo_unit_estimado;
       v_preco_custo_unit_est_new                   := :new.preco_custo_unit_estimado;
       v_saldo_financeiro_proj_old                  := :old.saldo_financeiro_proj;
       v_saldo_financeiro_proj_new                  := :new.saldo_financeiro_proj;
       v_preco_medio_unit_proj_old                  := :old.preco_medio_unit_proj;
       v_preco_medio_unit_proj_new                  := :new.preco_medio_unit_proj;
       v_preco_custo_unit_proj_old                  := :old.preco_custo_unit_proj;
       v_preco_custo_unit_proj_new                  := :new.preco_custo_unit_proj;
       v_saldo_fisico_quilo_old                     := :old.saldo_fisico_quilo;
       v_saldo_fisico_quilo_new                     := :new.saldo_fisico_quilo;
       v_conta_contabil_old                         := :old.conta_contabil;
       v_conta_contabil_new                         := :new.conta_contabil;
   elsif DELETING
   then
       v_tipo_ocorr                                 := 'D';
       v_codigo_deposito_old                        := :old.codigo_deposito;
       v_codigo_deposito_new                        := 0;
       v_nivel_estrutura_old                        := :old.nivel_estrutura;
       v_nivel_estrutura_new                        := ' ';
       v_grupo_estrutura_old                        := :old.grupo_estrutura;
       v_grupo_estrutura_new                        := ' ';
       v_subgrupo_estrutura_old                     := :old.subgrupo_estrutura;
       v_subgrupo_estrutura_new                     := ' ';
       v_item_estrutura_old                         := :old.item_estrutura;
       v_item_estrutura_new                         := ' ';
       v_mes_movimento_old                          := :old.mes_movimento;
       v_mes_movimento_new                          := 0;
       v_ano_movimento_old                          := :old.ano_movimento;
       v_ano_movimento_new                          := 0;
       v_mes_ano_movimento_old                      := :old.mes_ano_movimento;
       v_mes_ano_movimento_new                      := null;
       v_saldo_fisico_old                           := :old.saldo_fisico;
       v_saldo_fisico_new                           := 0.00;
       v_saldo_financeiro_old                       := :old.saldo_financeiro;
       v_saldo_financeiro_new                       := 0.00;
       v_preco_medio_unitario_old                   := :old.preco_medio_unitario;
       v_preco_medio_unitario_new                   := 0.00;
       v_preco_custo_unitario_old                   := :old.preco_custo_unitario;
       v_preco_custo_unitario_new                   := 0.00;
       v_saldo_financeiro_est_old                   := :old.saldo_financeiro_estimado;
       v_saldo_financeiro_est_new                   := 0.00;
       v_preco_medio_unit_est_old                   := :old.preco_medio_unit_estimado;
       v_preco_medio_unit_est_new                   := 0.00;
       v_preco_custo_unit_est_old                   := :old.preco_custo_unit_estimado;
       v_preco_custo_unit_est_new                   := 0.00;
       v_saldo_financeiro_proj_old                  := :old.saldo_financeiro_proj;
       v_saldo_financeiro_proj_new                  := 0.00;
       v_preco_medio_unit_proj_old                  := :old.preco_medio_unit_proj;
       v_preco_medio_unit_proj_new                  := 0.00;
       v_preco_custo_unit_proj_old                  := :old.preco_custo_unit_proj;
       v_preco_custo_unit_proj_new                  := 0.00;
       v_saldo_fisico_quilo_old                     := :old.saldo_fisico_quilo;
       v_saldo_fisico_quilo_new                     := 0.00;
       v_conta_contabil_old                         := :old.conta_contabil;
       v_conta_contabil_new                         := 0;
   end if;

    -- Dados do usu¿¿¿rio logado
    inter_pr_dados_usuario (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);
    v_nome_programa := inter_fn_nome_programa(v_sid);

    if UPDATING or DELETING
    then
        insert into estq_301_hist
          (aplicacao,
           tipo_ocorr,
           data_ocorr,
           usuario_rede,
           maquina_rede,
           nome_programa,
           usuario_systextil,
           codigo_deposito_old,
           codigo_deposito_new,
           nivel_estrutura_old,
           nivel_estrutura_new,
           grupo_estrutura_old,
           grupo_estrutura_new,
           subgrupo_estrutura_old,
           subgrupo_estrutura_new,
           item_estrutura_old,
           item_estrutura_new,
           mes_movimento_old,
           mes_movimento_new,
           ano_movimento_old,
           ano_movimento_new,
           mes_ano_movimento_old,
           mes_ano_movimento_new,
           saldo_fisico_old,
           saldo_fisico_new,
           saldo_financeiro_old,
           saldo_financeiro_new,
           preco_medio_unitario_old,
           preco_medio_unitario_new,
           preco_custo_unitario_old,
           preco_custo_unitario_new,
           saldo_financeiro_estimado_old,
           saldo_financeiro_estimado_new,
           preco_medio_unit_estimado_old,
           preco_medio_unit_estimado_new,
           preco_custo_unit_estimado_old,
           preco_custo_unit_estimado_new,
           saldo_financeiro_proj_old,
           saldo_financeiro_proj_new,
           preco_medio_unit_proj_old,
           preco_medio_unit_proj_new,
           preco_custo_unit_proj_old,
           preco_custo_unit_proj_new,
           saldo_fisico_quilo_old,
           saldo_fisico_quilo_new,
           conta_contabil_old,
           conta_contabil_new)
        values
          (v_aplicativo,
           v_tipo_ocorr,
           sysdate,
           v_usuario_rede,
           v_maquina_rede,
           v_nome_programa,
           v_usuario_systextil,
           v_codigo_deposito_old,
           v_codigo_deposito_new,
           v_nivel_estrutura_old,
           v_nivel_estrutura_new,
           v_grupo_estrutura_old,
           v_grupo_estrutura_new,
           v_subgrupo_estrutura_old,
           v_subgrupo_estrutura_new,
           v_item_estrutura_old,
           v_item_estrutura_new,
           v_mes_movimento_old,
           v_mes_movimento_new,
           v_ano_movimento_old,
           v_ano_movimento_new,
           v_mes_ano_movimento_old,
           v_mes_ano_movimento_new,
           v_saldo_fisico_old,
           v_saldo_fisico_new,
           v_saldo_financeiro_old,
           v_saldo_financeiro_new,
           v_preco_medio_unitario_old,
           v_preco_medio_unitario_new,
           v_preco_custo_unitario_old,
           v_preco_custo_unitario_new,
           v_saldo_financeiro_est_old,
           v_saldo_financeiro_est_new,
           v_preco_medio_unit_est_old,
           v_preco_medio_unit_est_new,
           v_preco_custo_unit_est_old,
           v_preco_custo_unit_est_new,
           v_saldo_financeiro_proj_old,
           v_saldo_financeiro_proj_new,
           v_preco_medio_unit_proj_old,
           v_preco_medio_unit_proj_new,
           v_preco_custo_unit_proj_old,
           v_preco_custo_unit_proj_new,
           v_saldo_fisico_quilo_old,
           v_saldo_fisico_quilo_new,
           v_conta_contabil_old,
           v_conta_contabil_new);
   end if;
end inter_tr_estq_301_hist;

-- ALTER TRIGGER INTER_TR_ESTQ_301_HIST ENABLE


/
