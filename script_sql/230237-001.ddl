-- Add column 
alter table pcpc_040 add qtde_kg_oc number(9,3) default 0;
comment on column PCPC_040.qtde_kg_oc
is 'Peso das pe�as da ordem de confec��o em cada estagio';

alter table pcpc_040 add data_pesagem date;
comment on column PCPC_040.data_pesagem
  is 'Data / hora da �ltima pesagem';
