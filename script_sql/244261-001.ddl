INSERT INTO hdoc_035
          ( codigo_programa
          , programa_menu
          , item_menu_def
          , descricao)
   VALUES ( 'oper_f128'
          , 0
          , 0
          , 'C�pia de cadastro de Regras (de: para) para Importa��o/Exporta��o');
 
INSERT INTO hdoc_033
          ( usu_prg_cdusu
          , usu_prg_empr_usu
          , programa
          , nome_menu
          , item_menu
          , ordem_menu
          , incluir
          , modificar
          , excluir
          , procurar)
    VALUES( 'INTERSYS'
          , 1
          , 'oper_f128'
          , 'nenhum'
          , 0
          , 1
          , 'N'
          , 'N'
          , 'N'
          , 'N');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Copia de registro de Reglas (de: para) para Importaci�n/Exportaci�n'
 WHERE hdoc_036.codigo_programa = 'oper_f128'
   AND hdoc_036.locale          = 'es_ES';
  
commit work;
