create table obrf_318 (
id_obrf_317_apuracao number(9) default 0,
id_obrf_318_apuracao number(9) default 0,
num_nota_fiscal number(9) default 0,
serie_nota varchar2(3) default ' ',
cnpj9 number(9) default 0,
cnpj4 number(4) default 0,
cnpj2 number(2) default 0,
ind_nf_entr_saida varchar2(1) default ' ',
nivel varchar2(1) default ' ',
grupo varchar2(5) default ' ',
subgrupo varchar2(3) default ' ',
item varchar2(6) default ' ',
data_emis_trans date,
ind_faturmento number(1) default 0,
ind_entra_livro number(1) default 0,
valor_contabil number(17,2) default 0.0,
val_base_calc number(17,2) default 0.0,
valor_pis number(17,2) default 0.0,
valor_cofins number(17,2) default 0.0,
cst_pis number(2) default 0,
cst_cofins number(2) default 0,
perc_pis number(5,2) default 0.0,
perc_cofins number(5,2) default 0.0,
cod_nat_base_credito number(4) default 0,
cod_contribuicao number(4) default 0);


ALTER TABLE obrf_318
ADD CONSTRAINT pk_obrf_318 PRIMARY KEY (id_obrf_317_apuracao,id_obrf_318_apuracao);

ALTER TABLE obrf_318
ADD CONSTRAINT FK_obrf_318
FOREIGN KEY (id_obrf_317_apuracao) REFERENCES obrf_317(id_obrf_317_apuracao);


alter table obrf_318
  add constraint UNIQ_obrf_318_apuracao unique (id_obrf_317_apuracao, num_nota_fiscal, serie_nota, cnpj9, cnpj4,cnpj2,nivel,grupo,subgrupo,item);
  
create sequence id_obrf_318_apuracao start with 1 increment by 1;
