alter table hdoc_030 add perc_alcada_pesagem number(5,2) default 0.00;

comment on column hdoc_030.perc_alcada_pesagem is 'Percentual Alçada para Liberação Pesagem';
