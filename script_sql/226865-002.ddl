CREATE TABLE PCPB_245 (
  ORDEM_PRODUCAO NUMBER(9,0) NOT NULL,
  CODIGO_ESTAGIO NUMBER(2,0) NOT NULL,
  COD_MOTIVO_LIBERACAO NUMBER(3,0) NOT NULL,
  USUARIO_LIBERACAO VARCHAR2(250) NOT NULL,
  EMPRESA NUMBER(3,0) NOT NULL,
  DATA_HORA_LIBERACAO TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE UNIQUE INDEX  "PK_PCPB_245" ON  "PCPB_245" ("ORDEM_PRODUCAO", "CODIGO_ESTAGIO");

ALTER TABLE  "PCPB_245" ADD CONSTRAINT "PK_PCPB_245" PRIMARY KEY ("ORDEM_PRODUCAO", "CODIGO_ESTAGIO")
  USING INDEX  "PK_PCPB_245"  ENABLE;
