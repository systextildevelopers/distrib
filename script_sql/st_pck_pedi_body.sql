create or replace PACKAGE BODY ST_PCK_PEDI AS
    
    function exists_portador(p_cod_portador number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM pedi_050 
        WHERE cod_portador = p_cod_portador;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_portador;
    
    function get_cond_pagto_vencimentos_str(p_cod_condicao_pagamento number) return varchar2 is
        
        cursor c_vencimentos is
            select b.condicao_pagto,b.sequencia,b.vencimento
            from pedi_070 a, pedi_075 b
            where a.cond_pgt_cliente = p_cod_condicao_pagamento
            and   a.cond_pgt_cliente = b.condicao_pagto
            order by b.sequencia;

        v_vencimentos_str   varchar2(4000) := null;
        v_soma_venc         number := 0;

    begin

        for r_vencimentos in c_vencimentos loop

            v_soma_venc := v_soma_venc + r_vencimentos.vencimento;


            if v_vencimentos_str is null then
                v_vencimentos_str := trim(to_char(v_soma_venc));
            else
                v_vencimentos_str := trim(v_vencimentos_str || '/' || trim(to_char(v_soma_venc)));
            end if;


        end loop;

        return v_vencimentos_str;

    end get_cond_pagto_vencimentos_str;



    function get_data_primeiro_venc(p_cod_condicao_pagamento number, p_data_base date) return date is
        v_controla_vencto   number  := 0;
        v_dia_util          number  := 0;
        v_nr_dias_venc      number  := 0;
        v_data_venc         date    := null;
        v_data_venc_tmp     date    := null;
    begin

        begin
            select max(fatu_500.controla_vencto)
            into v_controla_vencto
            from fatu_500;
        exception when others then
            v_controla_vencto := 0;
        end;


        begin
            select vencimento
            into v_nr_dias_venc
            from (
                    select pedi_075.vencimento
                    from pedi_075
                    where pedi_075.condicao_pagto = p_cod_condicao_pagamento
                    order by pedi_075.sequencia
                ) regs
            where rownum <= 1;
        exception when others then
            v_nr_dias_venc := 0;
        end;


        v_data_venc := trunc(trunc(p_data_base) + v_nr_dias_venc);


        if v_controla_vencto = 1 then

            v_dia_util := 0;

            begin
                select basi_260.dia_util_finan
                into v_dia_util
                from basi_260
                where basi_260.data_calendario = v_data_venc;
            exception when others then
                v_dia_util := 0;
            end;


            if v_dia_util = 1 then  --1 = Dia nao util


                v_data_venc_tmp := null;
                begin
                    select data_calendario
                    into v_data_venc_tmp
                    from (
                            select basi_260.data_calendario
                            from basi_260
                            where basi_260.data_calendario > v_data_venc
                              and basi_260.dia_util_finan  = 0  --0 = Dia util
                            order by basi_260.data_calendario ASC
                          ) regs
                    where rownum <= 1;

                    --raise_application_error(-20000, 'v_data_venc_tmp: '||v_data_venc_tmp);

                    v_data_venc := v_data_venc_tmp;
                exception when others then
                    v_data_venc_tmp := null;
                    --raise;
                end;


            end if; --dia util


        end if; --controla vencto


        return v_data_venc;

    end get_data_primeiro_venc;


END ST_PCK_PEDI;
