  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_050_HIST" 
   after insert
      or delete
      or update
   of nivel_item,                grupo_item,             sub_item,
      item_item,                 alternativa_item,       sequencia,
      nivel_comp,                grupo_comp,             sub_comp,
      item_comp,                 alternativa_comp,       consumo,
      estagio,                   tipo_calculo,           letra_grafico,
      qtde_camadas,              percent_perdas,         numero_grafico,
      qtde_inicial,              qtde_final,             tensao,
      lfa,                       lote,                   fornecedor,
      cons_un_rec,               seq_principal,          grupo_similares,
      centro_custo,              cons_unid_med_generica, perc_cons_calc,
      calcula_composicao,        relacao_banho,          qtde_pecas_estampadas,
      tipo_tela,                 area_cobertura,         tipo_aplicacao,
      tipo_medida,               tecido_principal
on basi_050
for each row

declare

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   teve_alter                number(1);

   sequencia_hist            number(9);
   long_aux                  long;

   linha                     varchar(100);

begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      begin
         select nvl(max(basi_095.sequencia),0)
         into sequencia_hist
         from basi_095
         where basi_095.tipo_comentario = 2 -- Hist�rico.
           and basi_095.nivel_estrutura = :new.nivel_item
           and basi_095.grupo_estrutura = :new.grupo_item
           and basi_095.data_historico  = trunc(sysdate);
      exception when others then
         sequencia_hist := 0;
      end;

      sequencia_hist := sequencia_hist + 1;

      linha := ' ';

      teve_alter := 1;
      long_aux       := inter_fn_buscar_tag('lb34909#A ESTRUTURA:',ws_locale_usuario,ws_usuario_systextil) ||
                                           :new.nivel_item || '.' ||
                                           :new.grupo_item || '.' ||
                                           :new.sub_item   || '.' ||
                                           :new.item_item  || ' ' ||
                        inter_fn_buscar_tag('lb34910#NA ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           to_char(:new.alternativa_item, '00') || chr(10) ||
                        inter_fn_buscar_tag('lb34318#TEVE AS SEGUINTES MODIFICA��ES:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           chr(10) ||
                        inter_fn_buscar_tag('lb34911#FOI INCLUIDA A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           to_char(:new.sequencia,'000') || chr(10) ||
                        inter_fn_buscar_tag('lb34912#COM O COMPONENTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           :new.nivel_comp || '.' ||
                                           :new.grupo_comp || '.' ||
                                           :new.sub_comp   || '.' ||
                                           :new.item_comp  || chr(10) ||
                        inter_fn_buscar_tag('lb34913#COM A ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                           to_char(:new.alternativa_comp, '00');
      if teve_alter = 1
      then
        begin
           INSERT INTO basi_095
              (nivel_estrutura,        grupo_estrutura,
               subgru_estrutura,       data_historico,
               tipo_comentario,        sequencia,
               descricao,              codigo_usuario,
               hora_historico,         nome_programa)
           VALUES
              (:new.nivel_item,        :new.grupo_item,
               :new.sub_item,          trunc(sysdate),
               2,                      sequencia_hist,
               long_aux,               ws_usuario_systextil,
               sysdate,                ws_aplicativo);
        exception when OTHERS then
           raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATEN��O! N�o inseriu {0}. Status: {1}.', 'BASI_095(050-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
        end;
      end if;
   end if;

   if updating and (:old.nivel_item <> :new.nivel_item or
                    :old.grupo_item <> :new.grupo_item or
                    :old.sub_item <> :new.sub_item or
                    :old.item_item <> :new.item_item or
                    :old.alternativa_item <> :new.alternativa_item or
                    :old.sequencia <> :new.sequencia or
                    :old.nivel_comp <> :new.nivel_comp or
                    :old.grupo_comp <> :new.grupo_comp or
                    :old.sub_comp <> :new.sub_comp or
                    :old.item_comp <> :new.item_comp or
                    :old.alternativa_comp <> :new.alternativa_comp or
                    :old.consumo <> :new.consumo or
                    :old.estagio <> :new.estagio or
                    :old.tipo_calculo <> :new.tipo_calculo or
                    :old.letra_grafico <> :new.letra_grafico or
                    :old.qtde_camadas <> :new.qtde_camadas or
                    :old.percent_perdas <> :new.percent_perdas or
                    :old.numero_grafico <> :new.numero_grafico or
                    :old.qtde_inicial <> :new.qtde_inicial or
                    :old.qtde_final <> :new.qtde_final or
                    :old.tensao <> :new.tensao or
                    :old.lfa <> :new.lfa or
                    :old.lote <> :new.lote or
                    :old.fornecedor <> :new.fornecedor or
                    :old.cons_un_rec <> :new.cons_un_rec or
                    :old.seq_principal <> :new.seq_principal or
                    :old.grupo_similares <> :new.grupo_similares or
                    :old.centro_custo <> :new.centro_custo or
                    :old.cons_unid_med_generica <> :new.cons_unid_med_generica or
                    :old.perc_cons_calc <> :new.perc_cons_calc or
                    :old.calcula_composicao <> :new.calcula_composicao or
                    :old.relacao_banho <> :new.relacao_banho or
                    :old.qtde_pecas_estampadas <> :new.qtde_pecas_estampadas or
                    :old.tipo_tela <> :new.tipo_tela or
                    :old.area_cobertura <> :new.area_cobertura or
                    :old.tipo_aplicacao <> :new.tipo_aplicacao or
                    :old.tipo_medida <> :new.tipo_medida or
                    :old.tecido_principal <> :new.tecido_principal)
   then
      begin
         select nvl(max(basi_095.sequencia),0)
         into sequencia_hist
         from basi_095
         where basi_095.tipo_comentario = 2 -- Hist�rico.
           and basi_095.nivel_estrutura = :new.nivel_item
           and basi_095.grupo_estrutura = :new.grupo_item
           and basi_095.data_historico  = trunc(sysdate);
      exception when others then
         sequencia_hist := 0;
      end;

      sequencia_hist := sequencia_hist + 1;

      linha := ' ';

      long_aux       := inter_fn_buscar_tag('lb34909#A ESTRUTURA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                        :new.nivel_item || '.' || :new.grupo_item || '.' || :new.sub_item   || '.' || :new.item_item  || ' ' ||
                        inter_fn_buscar_tag('lb34910#NA ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                        to_char(:new.alternativa_item, '00') || chr(10) ||
                        inter_fn_buscar_tag('lb34318#TEVE AS SEGUINTES MODIFICA��ES:',ws_locale_usuario,ws_usuario_systextil);

      teve_alter     := 0;

      if :old.consumo <> :new.consumo
      then
         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
                 inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                 to_char(:new.sequencia,'000') || ' ' ||
                 inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                 :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
                 inter_fn_buscar_tag('lb34916#TEVE O CONSUMO ALTERADO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || chr(10) ||
                 inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:old.consumo, '0.0000000') ||
                 ' -> ' ||
                 inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:new.consumo, '0.0000000');
      end if;

      if :old.estagio <> :new.estagio
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:new.sequencia,'000') || ' ' ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34917#TEVE O EST�GIO ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:old.estagio, '99900') ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || to_char(:new.estagio, '99900');
      end if;

      if :old.nivel_item <> :new.nivel_item or
         :old.grupo_item <> :new.grupo_item or
         :old.sub_item   <> :new.sub_item or
         :old.item_item  <> :new.item_item
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34918#A ESTRUTURA DO PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.nivel_item || '.' || :old.grupo_item || '.' || :old.sub_item   || '.' || :old.item_item  || chr(10) ||
              inter_fn_buscar_tag('lb34919#FOI ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.nivel_item || '.' || :old.grupo_item || '.' || :old.sub_item   || '.' || :old.item_item  || chr(10) ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_item || '.' || :new.grupo_item || '.' || :new.sub_item   || '.' || :new.item_item;
      end if;

      if :old.alternativa_item <> :new.alternativa_item
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:new.sequencia,'000') || ' ' ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34920#TEVE A ALTERNATIVA ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.alternativa_item, '00') ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:new.alternativa_item, '00');
      end if;

      if :old.sequencia <> :new.sequencia
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') || ' ' ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' ||  :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34919#FOI ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia, '000') ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:new.sequencia, '000');
      end if;

      if :old.nivel_comp <> :new.nivel_comp or
         :old.grupo_comp <> :new.grupo_comp or
         :old.sub_comp   <> :new.sub_comp or
         :old.item_comp  <> :new.item_comp
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.nivel_comp || '.' || :old.grupo_comp || '.' || :old.sub_comp   || '.' || :old.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34921#TEVE SEU COMPONENTE ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.nivel_comp || '.' || :old.grupo_comp || '.' || :old.sub_comp   || '.' || :old.item_comp  || chr(10) ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp;
      end if;

      if :old.alternativa_comp <> :new.alternativa_comp
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') || ' ' ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34922#TEVE A ALTERNATIVA ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.alternativa_comp, '00') ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:new.alternativa_comp, '00');
      end if;

      if :old.tipo_calculo <> :new.tipo_calculo
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') || ' ' ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34923#TEVE O TIPO DE CALCULO ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.tipo_calculo ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.tipo_calculo;
      end if;

      if :old.letra_grafico <> :new.letra_grafico
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') || ' ' ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34925#TEVE A LETRA DO GRAFICO ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.letra_grafico ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.letra_grafico;
      end if;

      if :old.qtde_camadas <> :new.qtde_camadas
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34926#TEVE QUANTIDADE DE CAMADAS ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.qtde_camadas ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.qtde_camadas;
      end if;

      if :old.percent_perdas <> :new.percent_perdas
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34928#TEVE O PERCENTUAL DE PERDAS ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.percent_perdas ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.percent_perdas;
      end if;

      if :old.numero_grafico <> :new.numero_grafico
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34929#TEVE O N�MERO DO GR�FICO ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.numero_grafico ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.numero_grafico;
      end if;

      if :old.qtde_inicial <> :new.qtde_inicial
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34930#TEVE A QUANTIDADE INICIAL ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.qtde_inicial ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.qtde_inicial;
      end if;

      if :old.qtde_final <> :new.qtde_final
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34931#TEVE A QUANTIDADE FINAL ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.qtde_final ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.qtde_final;
      end if;

      if :old.tensao <> :new.tensao
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34932#TEVE A TENS�O ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.tensao ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.tensao;
      end if;

      if :old.lfa <> :new.lfa
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34933#TEVE O LFA ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.lfa ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.lfa;
      end if;

      if :old.lote <> :new.lote
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34934#TEVE O LOTE ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.lote ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.lote;
      end if;

      if :old.fornecedor <> :new.fornecedor
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34935#TEVE O FORNECEDOR ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.fornecedor ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.fornecedor;
      end if;

      if :old.cons_un_rec <> :new.cons_un_rec
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34936#TEVE O CONSUMO UNIT�RIO RECEITA ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.cons_un_rec ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.cons_un_rec;
      end if;

      if :old.seq_principal <> :new.seq_principal
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34937#TEVE A SEQUECIA PRINCIPAL ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.seq_principal ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.seq_principal;
      end if;

      if :old.grupo_similares <> :new.grupo_similares
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34938#TEVE O GRUPO SIMILARES ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.grupo_similares ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.grupo_similares;
      end if;

      if :old.centro_custo <> :new.centro_custo
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34939#TEVE O CENTRO DE CUSTOS ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.centro_custo ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.centro_custo;
      end if;

      if :old.cons_unid_med_generica <> :new.cons_unid_med_generica
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34940#TEVE A CONSUMO AUXILIAR ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.cons_unid_med_generica ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.cons_unid_med_generica;
      end if;

      if :old.perc_cons_calc <> :new.perc_cons_calc
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34941#TEVE O % DE CONS. PARA CALC. ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.perc_cons_calc ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.perc_cons_calc;
      end if;

      if :old.calcula_composicao <> :new.calcula_composicao
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34942#TEVE CALCULA COMPOSI��O ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.calcula_composicao ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.calcula_composicao;
      end if;

      if :old.relacao_banho <> :new.relacao_banho
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34943#TEVE A RELA��O DE BANHO ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.relacao_banho ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.relacao_banho;
      end if;

      if :old.qtde_pecas_estampadas <> :new.qtde_pecas_estampadas
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34944#TEVE A QUANTIDADE DE PECAS ESTAMPADA ALTERADA',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.qtde_pecas_estampadas ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.qtde_pecas_estampadas;
      end if;

      if :old.tipo_tela <> :new.tipo_tela
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34945#TEVE O TIPO TELA ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.tipo_tela ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.tipo_tela;
      end if;

      if :old.area_cobertura <> :new.area_cobertura
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34946#TEVE O TIPO DE COBERTURA ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.area_cobertura ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.area_cobertura;
      end if;

      if :old.tipo_aplicacao <> :new.tipo_aplicacao
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34947#TEVE O TIPO DE APLICA��O ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.tipo_aplicacao ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.tipo_aplicacao;
      end if;

      if :old.tipo_medida <> :new.tipo_medida
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34948#TEVE O TIPO DE MEDIDA ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.tipo_medida ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.tipo_medida;
      end if;

      if :old.tecido_principal <> :new.tecido_principal
      then
         if teve_alter = 1
         then
            linha := '----------------------------------------------------------' || chr(10);
         end if;

         teve_alter     := 1;

         long_aux := long_aux || chr(10) ||
              linha ||
              inter_fn_buscar_tag('lb34914#A SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              to_char(:old.sequencia,'000') ||
              inter_fn_buscar_tag('lb34915#COM PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.nivel_comp || '.' || :new.grupo_comp || '.' || :new.sub_comp   || '.' || :new.item_comp  || chr(10) ||
              inter_fn_buscar_tag('lb34949#TEVE O TECIDO PRINCIPAL ALTERADO',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
              inter_fn_buscar_tag('lb00622#DE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :old.tecido_principal ||
              ' -> ' ||
              inter_fn_buscar_tag('lb00845#PARA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              :new.tecido_principal;
      end if;

      if teve_alter = 1
      then
         begin
            INSERT INTO basi_095
               (nivel_estrutura,        grupo_estrutura,
                subgru_estrutura,       data_historico,
                tipo_comentario,        sequencia,
                descricao,              codigo_usuario,
                hora_historico,         nome_programa)
            VALUES
               (:new.nivel_item,        :new.grupo_item,
                :new.sub_item,          trunc(sysdate),
                2,                      sequencia_hist,
                long_aux,               ws_usuario_systextil,
                sysdate,                ws_aplicativo);
         exception when OTHERS then
            raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATEN��O! N�o inseriu {0}. Status: {1}.', 'BASI_095(050-2)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;

   if deleting
   then
      begin
         select nvl(max(basi_095.sequencia),0)
         into sequencia_hist
         from basi_095
         where basi_095.tipo_comentario = 2 -- Hist�rico.
           and basi_095.nivel_estrutura = :old.nivel_item
           and basi_095.grupo_estrutura = :old.grupo_item
           and basi_095.data_historico  = trunc(sysdate);
      exception when others then
         sequencia_hist := 0;
      end;

      sequencia_hist := sequencia_hist + 1;

      teve_alter := 1;

      long_aux       := inter_fn_buscar_tag('lb34909#A ESTRUTURA:',ws_locale_usuario,ws_usuario_systextil) ||
                        :old.nivel_item || '.' || :old.grupo_item || '.' || :old.sub_item   || '.' || :old.item_item  ||
                        inter_fn_buscar_tag('lb34910#NA ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                        to_char(:old.alternativa_item, '00') || chr(10) ||
                        inter_fn_buscar_tag('lb34318#TEVE AS SEGUINTES MODIFICA��ES:',ws_locale_usuario,ws_usuario_systextil) || chr(10) ||
                        inter_fn_buscar_tag('lb34950#FOI DELETADA A SEQUENCIA',ws_locale_usuario,ws_usuario_systextil) || ': ' || chr(10) ||
                        to_char(:old.sequencia,'000') || chr(10) ||
                        inter_fn_buscar_tag('lb34912#COM O COMPONENTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                        :old.nivel_comp || '.' || :old.grupo_comp || '.' || :old.sub_comp   || '.' || :old.item_comp  || chr(10) ||
                        inter_fn_buscar_tag('lb34913#COM A ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                        to_char(:old.alternativa_comp, '00');

      if teve_alter = 1
      then
        begin
           INSERT INTO basi_095
              (nivel_estrutura,        grupo_estrutura,
               subgru_estrutura,       data_historico,
               tipo_comentario,        sequencia,
               descricao,              codigo_usuario,
               hora_historico,         nome_programa)
           VALUES
              (:old.nivel_item,        :old.grupo_item,
               :old.sub_item,          trunc(sysdate),
               2,                      sequencia_hist,
               long_aux,               ws_usuario_systextil,
               sysdate,                ws_aplicativo);
        exception when OTHERS then
            raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATEN��O! N�o inseriu {0}. Status: {1}.', 'BASI_095(050-3)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
        end;
      end if;
   end if;
end inter_tr_basi_050_hist;


-- ALTER TRIGGER "INTER_TR_BASI_050_HIST" ENABLE
 

/

exec inter_pr_recompile;

