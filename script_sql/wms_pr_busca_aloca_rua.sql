create or replace procedure wms_pr_busca_aloca_rua (p_nr_volume     in number,
                                                    p_nr_pedido     in number,
                                                    p_cod_embalagem in number,

                                                    p_rua           out number,
                                                    p_box           out number) is
  v_encontrou           number; --0: N�o Encontrou; 1: Encontrou
  v_retorno_int         number;
  v_entrou_loop         number; --0: N�o Entrou; 1: Entrou

  transp9_pedido        number;
  transp4_pedido        number;
  transp2_pedido        number;

  transp9_rua           number;
  transp4_rua           number;
  transp2_rua           number;

  rua_usa               number;
  box_usa               number;
  caixa_pedido          number;
  utiliza_enderecamento number;

  rua_transp            number;
  rua_aux               number;
  box_ped               number;
  box_aux               number;
  maior_box             number;
  maior_rua             number;
  nr_embalagem          number;

  nova_emb              number;

  encontrou_box         varchar2(1);
  pode_usar_rua         varchar2(1);
  achou_nova_emb        varchar2(1);
  inseriu_ok            varchar2(1);

  entrar_loop           number; --0: N�o Entrar; 1-Entrar;
  ent_sub_loop          number; --0: N�o Entrar; 1-Entrar;
BEGIN

   p_rua        := 0;
   p_box        := 0;
   caixa_pedido := 0;
   entrar_loop  := 1;
   ent_sub_loop := 1;

   begin
      select nvl(max(cod_rua), 0), nvl(max(cod_box), 0)
      into   maior_rua,            maior_box
      from  fatu_105
      where fatu_105.cod_embalagem   = p_cod_embalagem;
   exception
     when no_data_found then
        maior_rua := 0;
        maior_box := 0;
   end;

   if p_nr_pedido = 0
   then

      transp9_pedido := 999999999;
      transp4_pedido := 9999;
      transp2_pedido := 99;

   else

      begin
         select pedi_100.trans_pv_forne9, pedi_100.trans_pv_forne4,
                pedi_100.trans_pv_forne2
         into   transp9_pedido,           transp4_pedido,
                transp2_pedido
         from pedi_100
         where pedi_100.pedido_venda = p_nr_pedido;
      exception
        when no_data_found then
           transp9_pedido := 999999999;
           transp4_pedido := 9999;
           transp2_pedido := 99;
      end;

   end if;

/*   v_encontrou   := 1;
   begin

       ---- ROTINA PARA SABER SE UTILIZA ENDERECAMENTO - SS 38058 item 3 D) ------------
      select hdoc_001.campo_numerico17
      into   utiliza_enderecamento
      from  hdoc_001
      where hdoc_001.tipo   = 66
        and hdoc_001.codigo = tipo_endereco;
   exception
     when no_data_found then
        v_encontrou   := 0;
   end;*/

/*   if v_encontrou = 0
   then
      utiliza_enderecamento  := 0;
   end if;
*/

   utiliza_enderecamento  := 1;

   if utiliza_enderecamento = 1
   then

      /* ---PEGA RUA DA NOVA TABELA DE RELACIONAMENTO  RUA x TRANSPORTADORA - SS 38058 item 3 E) ---*/
      rua_transp := 0;

      for f_ruas in (
         select fatu_125.cod_rua rua_transp
         from  fatu_125
         where fatu_125.transpor_forne9 = transp9_pedido
           and fatu_125.transpor_forne4 = transp4_pedido
           and fatu_125.transpor_forne2 = transp2_pedido
         order by fatu_125.cod_rua)
      LOOP

         rua_aux    := f_ruas.rua_transp;
         rua_transp := f_ruas.rua_transp;

         if p_rua = 0
         then

            v_encontrou := 1;

            /*Verifica se existe box para o cod da embalagem*/
            begin
               select fatu_115.cod_box
               into   box_ped
               from fatu_115
                 where fatu_115.cod_rua       = f_ruas.rua_transp
                  and  fatu_115.cod_embalagem = p_cod_embalagem
                  and  rownum                <= 1;
            exception
              when no_data_found then
                 v_encontrou   := 0;
                 box_ped       := 0;
            end;

            if v_encontrou = 0
            then
                box_aux       := 0;
                encontrou_box := 'n';
            else
                encontrou_box := 's';
                box_aux       := box_ped - 1;
            end if;

            entrar_loop  := 1;
            while entrar_loop = 1
            loop
               box_aux := box_aux + 1;
               wms_pr_gera_sem_pedido(rua_aux,box_aux,p_cod_embalagem,rua_aux,box_aux,rua_aux,box_aux,p_rua,p_box);

               EXIT WHEN box_aux = 1000 or maior_box < box_aux or p_box > 0;
            end loop;

            if p_box = 0 and encontrou_box = 's'
            then
               box_aux      := 0;
               entrar_loop  := 1;
               while entrar_loop = 1
               loop
                 box_aux     := box_aux + 1;
                 wms_pr_gera_sem_pedido(rua_aux,box_aux,p_cod_embalagem,rua_aux,box_aux,rua_aux,box_aux,p_rua,p_box);

                 EXIT WHEN box_aux = 1000 or maior_box < box_aux or p_box > 0;
               end loop;
            end if;

         else exit when p_rua <> 0;
         end if;

      END LOOP;

      if rua_transp = 0 /* ---NAO TEM RELACIONAMENTO DE RUA X TRANSPORTADORA--- */
      then              /* ---PROCESSO CONTINUA COMO ANTES DA SS -38058--- */

         /*Verifica se j� existe uma rua para a transportadora embalagem*/
         for f_runas_trans in (
            select distinct(fatu_115.cod_rua) rua_ped
            from fatu_115
            where fatu_115.transp9       = transp9_pedido
             and  fatu_115.transp4       = transp4_pedido
             and  fatu_115.transp2       = transp2_pedido
            order by fatu_115.cod_rua)
         LOOP

            rua_aux := f_runas_trans.rua_ped;

            if p_rua = 0
            then

               v_encontrou := 1;

               begin

                  /*Verifica se existe box para o cod da embalagem*/
                  select fatu_115.cod_box
                  into   box_ped
                  from  fatu_115
                  where fatu_115.cod_rua       = f_runas_trans.rua_ped
                   and  fatu_115.cod_embalagem = p_cod_embalagem
                   and  rownum                 <= 1;
               exception
                 when no_data_found then
                    v_encontrou   := 0;
                    box_ped       := 0;
               end;

               if v_encontrou = 0
               then
                  box_aux       := 0;
                  encontrou_box := 'n';
               else
                  encontrou_box := 's';
                  box_aux       := box_ped - 1;
               end if;

               entrar_loop := 1;
               while entrar_loop = 1
               loop
                  box_aux     := box_aux + 1;
                  wms_pr_gera_sem_pedido(rua_aux,box_aux,p_cod_embalagem,rua_aux,box_aux,rua_aux,box_aux,p_rua,p_box);

                  EXIT WHEN box_aux = 1000 or maior_box < box_aux or p_box > 0;
               end loop;

               if p_box = 0 and encontrou_box = 's'
               then
                  box_aux     := 0;
                  entrar_loop := 1;
                  while entrar_loop = 1
                  loop
                     box_aux     := box_aux + 1;
                     wms_pr_gera_sem_pedido(rua_aux,box_aux,p_cod_embalagem,rua_aux,box_aux,rua_aux,box_aux,p_rua,p_box);

                     EXIT WHEN box_aux = 1000 or maior_box < box_aux or p_box > 0;
                  end loop;
               end if;

            else exit when p_rua <> 0;
            end if;

         END LOOP;

      end if; /* ---FIM RUA_TRANSP = 0 ---*/

   else /* --- SE NAO UTILIZA_ENDERECAMENTO --- */

      if rua_transp = 0 /* ---SE  NAO ENCONTROU RUA NO ENDERECAMENTO,--- */
      then              /* ---PROCESSO CONTINUA COMO ANTES DA SS -38058         --- */

         /*Verifica se j� existe uma rua para a transportadora embalagem*/
         for f_ruas_dist in (
            select distinct(fatu_115.cod_rua) rua_ped
            from fatu_115
            where fatu_115.transp9       = transp9_pedido
             and  fatu_115.transp4       = transp4_pedido
             and  fatu_115.transp2       = transp2_pedido
            order by fatu_115.cod_rua)
         LOOP

            rua_aux := f_ruas_dist.rua_ped;

            if p_rua = 0
            then

               v_encontrou := 1;

               begin

                  /*Verifica se existe box para o cod da embalagem*/
                  select fatu_115.cod_box
                  into   box_ped
                  from  fatu_115
                  where fatu_115.cod_rua       = f_ruas_dist.rua_ped
                   and  fatu_115.cod_embalagem = p_cod_embalagem
                   and  rownum                <= 1;
               exception
                 when no_data_found then
                    v_encontrou   := 0;
                    box_ped       := 0;
               end;

               if v_encontrou = 0
               then
                  box_aux       := 0;
                  encontrou_box := 'n';
               else
                  encontrou_box := 's';
                  box_aux       := box_ped - 1;
               end if;

               entrar_loop := 1;
               while entrar_loop = 1
               loop
                  box_aux     := box_aux + 1;
                  wms_pr_gera_sem_pedido(rua_aux,box_aux,p_cod_embalagem,rua_aux,box_aux,rua_aux,box_aux,p_rua,p_box);

                  EXIT WHEN box_aux = 1000 or maior_box < box_aux or p_box > 0;
               end loop;

               if p_box = 0 and encontrou_box = 's'
               then
                  box_aux   := 0;
                  entrar_loop := 1;
                  while entrar_loop = 1
                  loop
                     box_aux := box_aux + 1;
                     wms_pr_gera_sem_pedido(rua_aux,box_aux,p_cod_embalagem,rua_aux,box_aux,rua_aux,box_aux,p_rua,p_box);

                     EXIT WHEN box_aux = 1000 or maior_box < box_aux or p_box > 0;
                  end loop;
               end if;

            else exit when p_rua <> 0;
            end if;

         END LOOP;

      end if; /* ---FIM RUA_TRANSP = 0 ---*/

   end if;

   /* --- SE NAO ENCONTROU RUA NA TABELA FATU_125 E FATU_115, PEGA RUA LIVRE --- */
   if p_rua = 0 and p_box = 0
   then
      rua_aux   := 0;

      entrar_loop := 1;
      while entrar_loop = 1
      loop
         rua_aux     := rua_aux + 1;

         v_encontrou := 1;
         begin

            /*verifica se a rua j� est� alocada a uma transportadora*/
            select fatu_115.transp9,              fatu_115.transp4,
                   fatu_115.transp2
            into   transp9_rua,                   transp4_rua,
                   transp2_rua
            from fatu_115
            where fatu_115.cod_rua = rua_aux
              and rownum          <= 1;
         exception
           when no_data_found then
              v_encontrou   := 0;
              transp9_rua   := 0;
              transp4_rua   := 0;
              transp2_rua   := 0;
         end;

         if v_encontrou = 0 then pode_usar_rua := 's';
         else
            if transp9_pedido = transp9_rua and
               transp4_pedido = transp4_rua and
               transp2_pedido = transp2_rua
            then
               pode_usar_rua := 's';
            else
               pode_usar_rua := 'n';
            end if;
         end if;

         box_aux   := 0;

         if pode_usar_rua = 's'
         then
            ent_sub_loop := 1;
            while ent_sub_loop = 1
            loop
               box_aux := box_aux + 1;
               wms_pr_gera_sem_pedido(rua_aux,box_aux,p_cod_embalagem,rua_aux,box_aux,rua_aux,box_aux,p_rua,p_box);

               EXIT WHEN box_aux = 1000 or maior_box < box_aux;
            end loop;
         end if;

         EXIT WHEN rua_aux = 1000 or maior_rua < rua_aux;

      end loop;
   end if;

   if (p_rua <> 0 and p_box <> 0) or utiliza_enderecamento = 0
   then

      inseriu_ok := 's';

      if utiliza_enderecamento = 1
      then

         begin

            /*gravar na tabela de ocupa��o de endere�o fatu_115*/
            insert into fatu_115
            ( cod_rua,          cod_box,
              nr_embalagem,     cod_embalagem,
              transp9,          transp4,
              transp2)
            values
            ( p_rua,            p_box,
              p_nr_volume ,     p_cod_embalagem,
              transp9_pedido,   transp4_pedido,
              transp2_pedido);
         exception
            when others then
               raise_application_error(-20000, 'N�o inseriu na tabela FATU_115.' || Chr(10) || SQLERRM);
         end;

      end if;

   end if;

END wms_pr_busca_aloca_rua;
/

exec inter_pr_recompile;

/* versao: 5 */


 exit;


 exit;

 exit;


 exit;

 exit;
