alter table pedi_051 modify seq_nr_tit_banco number(11);
alter table pedi_051 modify faixa_maxima number(11);
alter table pedi_051 modify faixa_minima number(11);
alter table pedi_051_log modify seq_nr_tit_banco_old number(11);
alter table pedi_051_log modify seq_nr_tit_banco_new number(11);
alter table pedi_051_log modify faixa_maxima_old number(11);
alter table pedi_051_log modify faixa_maxima_new number(11);
alter table pedi_051_log modify faixa_minima_old number(11);
alter table pedi_051_log modify faixa_minima_new number(11);

