alter table pedi_020
add (considera_plano_comissao varchar2(1) default 'N');

COMMENT ON COLUMN pedi_020.considera_plano_comissao IS 'Indica se o representante participa do plano de pagamento de comiss�o (S/N).';

/
exec inter_pr_recompile;

