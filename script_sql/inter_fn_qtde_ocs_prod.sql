CREATE OR REPLACE FUNCTION inter_fn_qtde_ocs_prod (p_op                in number,
                                                   p_est               in number,
                                                   p_data              in date
)
return number is
   v_ocs                number(9);

begin

   select count(1) into v_ocs
   from ( select p45.pcpc040_perconf,PCPC040_ORDCONF
          from pcpc_045 p45
          where p45.ordem_producao = p_op
          and p45.pcpc040_estconf = p_est
          and p45.data_insercao   < p_data
          group by p45.pcpc040_perconf,PCPC040_ORDCONF
          );
   return(v_ocs + 1);
   
end inter_fn_qtde_ocs_prod;

/

exec inter_pr_recompile;
