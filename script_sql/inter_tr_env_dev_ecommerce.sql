create or replace trigger inter_tr_obrf_010_env_dev_ecom
after update of cod_status, cod_canc_nfisc
on obrf_010 for each row
declare
    v_id_devolucao  number(9) := 0;
    v_tipo_envio    number(9) := 0;
    v_reg_exists    number(1) := 0;
begin
    if (:old.cod_status <> :new.cod_status and :new.cod_status = '100')
    then
        v_tipo_envio := 1; /* ENVIO EMISS�O */
    end if;

    if (:old.cod_canc_nfisc = 0 and :new.cod_canc_nfisc > 0)
    then
        v_tipo_envio := 2; /* ENVIO CANCELAMENTO */
    end if;

    if (v_tipo_envio > 0)
    then
        begin
            select id into v_id_devolucao
            from obrf_341
            where (nr_doc_entrada = :new.documento)
              and (serie_doc_entrada = :new.serie)
              and (cgc9_doc_entrada = :new.cgc_cli_for_9)
              and (cgc4_doc_entrada = :new.cgc_cli_for_4)
              and (cgc2_doc_entrada = :new.cgc_cli_for_2)
              and rownum < 2;
            exception
                when no_data_found then
                    v_id_devolucao := 0;
        end;

        if (v_id_devolucao > 0)
        then
            begin
                select 1 into v_reg_exists
                from obrf_343
                where (id_devolucao = v_id_devolucao)
                  and (status = 0)
                  and rownum < 2;
                exception
                    when no_data_found then
                        v_reg_exists := 0;
            end;

            if (v_reg_exists = 0)
            then
                begin
                    insert into obrf_343 (id_devolucao, tipo_envio, data_criacao)
                    values (v_id_devolucao, v_tipo_envio, sysdate);
                    exception when others then
                          raise_application_error(-20000, 'Erro ao inserir tabela obrf_343' || Chr(10) || SQLERRM);
                end;
            end if;

            if (v_reg_exists > 0)
            then
                begin
                    update obrf_343
                    set tipo_envio = v_tipo_envio,
                        data_criacao = sysdate
                    where (id_devolucao = v_id_devolucao)
                      and (status = 0);
                    exception when others then
                        raise_application_error(-20000, 'Erro ao alterar tabela obrf_343' || Chr(10) || SQLERRM);
                end;
            end if;
        end if;
    end if;
end inter_tr_obrf_010_env_dev_ecom;

/
exec inter_pr_recompile;
