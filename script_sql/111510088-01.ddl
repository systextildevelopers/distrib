alter table i_obrf_010 add num_contabil number(9);

alter table i_obrf_010 modify num_contabil default 0;

exec inter_pr_recompile;
