create or replace function safediv( n1 number, n2 number) return number is
begin
    if n2 = 0 then
        return 0;
    else
       return n1 / n2; 
    end if;
end safediv;
