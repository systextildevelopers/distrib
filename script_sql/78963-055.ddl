insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f443', 'Implanta saldo credor a transp.',0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f443', 'favoritos', 0, 0, 'S', 'S', 'N', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Implanta saldo credor a transp.'
where hdoc_036.codigo_programa = 'obrf_f443'
  and hdoc_036.locale          = 'Pt-Br';

commit;
