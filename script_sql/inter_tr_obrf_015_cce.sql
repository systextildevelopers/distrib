
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_015_CCE" 
before update of natitem_nat_oper,    classific_fiscal,
                unidade_medida,      procedencia,
                cvf_ipi_entrada,     cod_vlfiscal_icm,
                cod_csosn,           cvf_pis,
                cvf_cofins
on obrf_015
for each row

declare
st_situacao_entrada_aux  number(1);
v_cod_status_aux         varchar2(5);

begin
   begin
     select situacao_entrada,      cod_status
     into st_situacao_entrada_aux, v_cod_status_aux
     from obrf_010
     where obrf_010.documento      =  :new.capa_ent_nrdoc
       and obrf_010.serie          =  :new.capa_ent_serie
       and obrf_010.cgc_cli_for_9  =  :new.capa_ent_forcli9
       and obrf_010.cgc_cli_for_4  =  :new.capa_ent_forcli4
       and obrf_010.cgc_cli_for_2  =  :new.capa_ent_forcli2
       ;
   exception
     when others then
       st_situacao_entrada_aux := 0;
	   v_cod_status_aux := ' ';
   end;

   if st_situacao_entrada_aux = 1
   and (v_cod_status_aux      = '100' or v_cod_status_aux  = '990')
   then
      begin
         update obrf_010
         set obrf_010.st_flag_cce = 1
         where obrf_010.documento      =  :new.capa_ent_nrdoc
           and obrf_010.serie          =  :new.capa_ent_serie
           and obrf_010.cgc_cli_for_9  =  :new.capa_ent_forcli9
           and obrf_010.cgc_cli_for_4  =  :new.capa_ent_forcli4
           and obrf_010.cgc_cli_for_2  =  :new.capa_ent_forcli2;
      end;
   end if;
end inter_tr_obrf_015_cce;
-- ALTER TRIGGER "INTER_TR_OBRF_015_CCE" ENABLE
 

/

exec inter_pr_recompile;

