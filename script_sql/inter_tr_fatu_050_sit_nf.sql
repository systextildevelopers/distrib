create or replace trigger "INTER_TR_FATU_050_SIT_NF"
  before update of situacao_nfisc
  on fatu_050
  for each row
begin
   if (:new.situacao_nfisc = 3 and :old.situacao_nfisc = 1) and updating
   then
      :new.situacao_nfisc := 1;
   end if;
end inter_tr_fatu_050_sit_nf;

-- ALTER TRIGGER "INTER_TR_FATU_050_SIT_NF" ENABLE
 

/

exec inter_pr_recompile;
