-- Create table
create table FTEC_020
(
  ID        NUMBER(5) not null,
  DESCRICAO NVARCHAR2(50) not null
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table FTEC_020
  add constraint PK_FTEC_020 primary key (ID)
  using index;
