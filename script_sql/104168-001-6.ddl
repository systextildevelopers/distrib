alter table fatu_504 add altera_ped_per_fechado number(1) default 0;

comment on column fatu_504.altera_ped_per_fechado is ' Se configurado para 1 -Sim, o sistema ir� aceitar fazer altera��es para menor nas quantidades de itens de pedidos de venda, ou prorrogar a data de entrega';

exec inter_pr_recompile;
