ALTER TABLE EXPT_050 ADD ENDERECO VARCHAR2(40);

ALTER TABLE EXPT_050 ADD NIVEL VARCHAR2(1) DEFAULT '';

ALTER TABLE EXPT_050 ADD GRUPO VARCHAR2(5) DEFAULT '';

ALTER TABLE EXPT_050 ADD SUBGRUPO VARCHAR2(3) DEFAULT '';

ALTER TABLE EXPT_050 ADD ITEM VARCHAR2(6) DEFAULT '';

ALTER TABLE EXPT_050 ADD DESCRICAO_ITEM VARCHAR2(120) DEFAULT '';
