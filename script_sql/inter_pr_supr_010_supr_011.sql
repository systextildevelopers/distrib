CREATE OR REPLACE PROCEDURE pr_supr_010_supr_011 IS
BEGIN
     BEGIN
    for reg_supr_011 in (select supr_011.cnpj_fornecedor9, supr_011.cnpj_fornecedor4, supr_011.cnpj_fornecedor2
                                from supr_011
                                where supr_011.DATA_INICIAL <= trunc(SYSDATE)
                                and supr_011.DATA_FINAL >= trunc(SYSDATE))
      loop
          BEGIN
            UPDATE supr_010
            set considera_ttd = 1
            where supr_010.fornecedor9 = reg_supr_011.cnpj_fornecedor9
            and supr_010.fornecedor4   = reg_supr_011.cnpj_fornecedor4
            and supr_010.fornecedor2   = reg_supr_011.cnpj_fornecedor2;
            EXCEPTION when OTHERS then raise_application_error (-20000, 'Nao atualizou a tabela de Fornecedores(supr_011)');
          END;
      end loop;
      COMMIT;
  END;

  BEGIN
    for reg_supr_011 in (select supr_011.cnpj_fornecedor9, supr_011.cnpj_fornecedor4, supr_011.cnpj_fornecedor2
                                from supr_011
                                where supr_011.DATA_INICIAL > trunc(SYSDATE)
                                and supr_011.DATA_FINAL < trunc(SYSDATE))
      loop
          BEGIN
            UPDATE supr_010
            set considera_ttd = 0
            where supr_010.fornecedor9 = reg_supr_011.cnpj_fornecedor9
            and supr_010.fornecedor4   = reg_supr_011.cnpj_fornecedor4
            and supr_010.fornecedor2   = reg_supr_011.cnpj_fornecedor2;
          EXCEPTION when OTHERS then
            raise_application_error (-20000, 'Nao atualizou a tabela de Fornecedores(supr_011)');
        END;
      end loop;
      COMMIT;
  END;
END pr_supr_010_supr_011;
