create table obrf_165
(
	COD_NOTA_TEC VARCHAR2(20),
	AMBIENTE NUMBER(1),
	DATA_VIGOR DATE
);

comment on table OBRF_165
  is 'Armazena a data que a nota técnica entra em vigor.';
