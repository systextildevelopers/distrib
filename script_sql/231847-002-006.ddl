create table pcpt_073 (
  id number(9,0) not null primary key,
  pedido_venda number(9,0) not null,
  seq_item_pedido number(3, 0) not null,
  observacao varchar2(300),
  criado_por varchar2(250),
  criado_em timestamp default current_timestamp
);

create sequence pcpt_073_seq;

create or replace trigger bi_pcpt_073
before insert on pcpt_073
for each row
begin
  :new.id := pcpt_073_seq.nextval;
end;
