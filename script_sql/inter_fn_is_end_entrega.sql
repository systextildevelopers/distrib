CREATE OR REPLACE FUNCTION inter_fn_is_end_entrega (p_empresa in number, p_param_name in varchar2, p_cnpj4 in number) return number
  is param_value number;
begin
  param_value := 0;

  if p_cnpj4 = 0 /* apenas pessoa fisica */
  then
    begin
     select inter_fn_get_param_int(p_empresa, p_param_name)
     into param_value
     from dual;
    exception
      when no_data_found then
        raise_application_error(-20000, 'Erro ao validar endereço entrega');
    end;
  end if;

  return(param_value);

end inter_fn_is_end_entrega;
/

