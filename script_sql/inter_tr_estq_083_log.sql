create or replace trigger inter_tr_ESTQ_083_log 
after insert or delete or update 
on ESTQ_083 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);  
 
 
 
 if inserting 
 then 
    begin 
 
        insert into ESTQ_083_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           periodo_producao_OLD,   /*8*/ 
           periodo_producao_NEW,   /*9*/ 
           ordem_producao_OLD,   /*10*/ 
           ordem_producao_NEW,   /*11*/ 
           ordem_confeccao_OLD,   /*12*/ 
           ordem_confeccao_NEW,   /*13*/ 
           sequencia_OLD,   /*14*/ 
           sequencia_NEW,   /*15*/ 
           deposito_OLD,   /*16*/ 
           deposito_NEW,   /*17*/ 
           data_imagem_OLD,   /*18*/ 
           data_imagem_NEW,   /*19*/ 
           nivel_OLD,   /*20*/ 
           nivel_NEW,   /*21*/ 
           grupo_OLD,   /*22*/ 
           grupo_NEW,   /*23*/ 
           subgrupo_OLD,   /*24*/ 
           subgrupo_NEW,   /*25*/ 
           item_OLD,   /*26*/ 
           item_NEW,   /*27*/ 
           cor_tag_OLD,   /*28*/ 
           cor_tag_NEW,   /*29*/ 
           nro_contagem_OLD,   /*30*/ 
           nro_contagem_NEW    /*31*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.periodo_producao, /*9*/   
           0,/*10*/
           :new.ordem_producao, /*11*/   
           0,/*12*/
           :new.ordem_confeccao, /*13*/   
           0,/*14*/
           :new.sequencia, /*15*/   
           0,/*16*/
           :new.deposito, /*17*/   
           null,/*18*/
           :new.data_imagem, /*19*/   
           '',/*20*/
           :new.nivel, /*21*/   
           '',/*22*/
           :new.grupo, /*23*/   
           '',/*24*/
           :new.subgrupo, /*25*/   
           '',/*26*/
           :new.item, /*27*/   
           '',/*28*/
           :new.cor_tag, /*29*/   
           0,/*30*/
           :new.nro_contagem /*31*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into ESTQ_083_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           periodo_producao_OLD, /*8*/   
           periodo_producao_NEW, /*9*/   
           ordem_producao_OLD, /*10*/   
           ordem_producao_NEW, /*11*/   
           ordem_confeccao_OLD, /*12*/   
           ordem_confeccao_NEW, /*13*/   
           sequencia_OLD, /*14*/   
           sequencia_NEW, /*15*/   
           deposito_OLD, /*16*/   
           deposito_NEW, /*17*/   
           data_imagem_OLD, /*18*/   
           data_imagem_NEW, /*19*/   
           nivel_OLD, /*20*/   
           nivel_NEW, /*21*/   
           grupo_OLD, /*22*/   
           grupo_NEW, /*23*/   
           subgrupo_OLD, /*24*/   
           subgrupo_NEW, /*25*/   
           item_OLD, /*26*/   
           item_NEW, /*27*/   
           cor_tag_OLD, /*28*/   
           cor_tag_NEW, /*29*/   
           nro_contagem_OLD, /*30*/   
           nro_contagem_NEW  /*31*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.periodo_producao,  /*8*/  
           :new.periodo_producao, /*9*/   
           :old.ordem_producao,  /*10*/  
           :new.ordem_producao, /*11*/   
           :old.ordem_confeccao,  /*12*/  
           :new.ordem_confeccao, /*13*/   
           :old.sequencia,  /*14*/  
           :new.sequencia, /*15*/   
           :old.deposito,  /*16*/  
           :new.deposito, /*17*/   
           :old.data_imagem,  /*18*/  
           :new.data_imagem, /*19*/   
           :old.nivel,  /*20*/  
           :new.nivel, /*21*/   
           :old.grupo,  /*22*/  
           :new.grupo, /*23*/   
           :old.subgrupo,  /*24*/  
           :new.subgrupo, /*25*/   
           :old.item,  /*26*/  
           :new.item, /*27*/   
           :old.cor_tag,  /*28*/  
           :new.cor_tag, /*29*/   
           :old.nro_contagem,  /*30*/  
           :new.nro_contagem  /*31*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into ESTQ_083_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           periodo_producao_OLD, /*8*/   
           periodo_producao_NEW, /*9*/   
           ordem_producao_OLD, /*10*/   
           ordem_producao_NEW, /*11*/   
           ordem_confeccao_OLD, /*12*/   
           ordem_confeccao_NEW, /*13*/   
           sequencia_OLD, /*14*/   
           sequencia_NEW, /*15*/   
           deposito_OLD, /*16*/   
           deposito_NEW, /*17*/   
           data_imagem_OLD, /*18*/   
           data_imagem_NEW, /*19*/   
           nivel_OLD, /*20*/   
           nivel_NEW, /*21*/   
           grupo_OLD, /*22*/   
           grupo_NEW, /*23*/   
           subgrupo_OLD, /*24*/   
           subgrupo_NEW, /*25*/   
           item_OLD, /*26*/   
           item_NEW, /*27*/   
           cor_tag_OLD, /*28*/   
           cor_tag_NEW, /*29*/   
           nro_contagem_OLD, /*30*/   
           nro_contagem_NEW /*31*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.periodo_producao, /*8*/   
           0, /*9*/
           :old.ordem_producao, /*10*/   
           0, /*11*/
           :old.ordem_confeccao, /*12*/   
           0, /*13*/
           :old.sequencia, /*14*/   
           0, /*15*/
           :old.deposito, /*16*/   
           0, /*17*/
           :old.data_imagem, /*18*/   
           null, /*19*/
           :old.nivel, /*20*/   
           '', /*21*/
           :old.grupo, /*22*/   
           '', /*23*/
           :old.subgrupo, /*24*/   
           '', /*25*/
           :old.item, /*26*/   
           '', /*27*/
           :old.cor_tag, /*28*/   
           '', /*29*/
           :old.nro_contagem, /*30*/   
           0 /*31*/
         );    
    end;    
 end if;    
end inter_tr_ESTQ_083_log;

/

exec inter_pr_recompile;
