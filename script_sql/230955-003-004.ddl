INSERT INTO EMPR_007 (PARAM,             TIPO,   LABEL,     FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES              ('obrf.dataIniBlocoK',  3,   'null', 	  ' ',         null,        null,         null,       to_date(sysdate + 3365));

COMMIT;

declare tmpInt number;

cursor parametro_c is select codigo_empresa from fatu_500;

begin
   for reg_parametro in parametro_c
   loop
      select count(*)
      into tmpInt
      from empr_008
      where empr_008.codigo_empresa = reg_parametro.codigo_empresa
      and empr_008.param = 'obrf.dataIniBlocoK';
      
      if(tmpInt = 0)
      then
         begin
            insert into empr_008 ( codigo_empresa, param, val_dat)
            values (reg_parametro.codigo_empresa, 'obrf.dataIniBlocoK', to_date(sysdate + 3365));
         end;
      end if;

   end loop;
   
   commit;
end;
