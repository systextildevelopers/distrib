-- Cria a tabela SUPR_029

CREATE TABLE SUPR_029 (
FORNECEDOR9 NUMBER(9),
DELIMITADOR VARCHAR2(1),
CEP_INICIAL NUMBER(3) NOT NULL,
CEP_INICIAL_FIM NUMBER(3),
CEP_FINAL NUMBER(3),
CEP_FINAL_FIM NUMBER(3),
ROTA NUMBER(3) NOT NULL,
ROTA_FIM NUMBER(3),
FILIAL_SIGLA NUMBER(3) NOT NULL,
FILIAL_SIGLA_FIM NUMBER(3),
ID_FILIAL NUMBER(3) NOT NULL,
ID_FILIAL_FIM NUMBER(3),
CONSTRAINT PK_SUPR_029 PRIMARY KEY (FORNECEDOR9)
);
COMMENT ON TABLE SUPR_029 IS 'Defini��o da leitura do arquivo de importa��o da transportadora';
COMMENT ON COLUMN SUPR_029.FORNECEDOR9 IS 'A transportadora correspondente';
COMMENT ON COLUMN SUPR_029.DELIMITADOR IS 'O caractere delimitador, ou nulo se usar posi��es fixas';
COMMENT ON COLUMN SUPR_029.CEP_INICIAL IS 'A posi��o inicial ou coluna do CEP inicial';
COMMENT ON COLUMN SUPR_029.CEP_INICIAL_FIM IS 'A posi��o final do CEP inicial, se usar posi��es fixas';
COMMENT ON COLUMN SUPR_029.CEP_FINAL IS 'A posi��o inicial ou coluna do CEP final, se for usado';
COMMENT ON COLUMN SUPR_029.CEP_FINAL_FIM IS 'A posi��o final do CEP final, se usar posi��es fixas';
COMMENT ON COLUMN SUPR_029.ROTA IS 'A posi��o inicial ou coluna da rota';
COMMENT ON COLUMN SUPR_029.ROTA_FIM IS 'A posi��o final da rota, se usar posi��es fixas';
COMMENT ON COLUMN SUPR_029.FILIAL_SIGLA IS 'A posi��o inicial ou coluna da sigla da filial';
COMMENT ON COLUMN SUPR_029.FILIAL_SIGLA_FIM IS 'A posi��o final da sigla da filial, se usar posi��es fixas';
COMMENT ON COLUMN SUPR_029.ID_FILIAL IS 'A posi��o inicial ou coluna do ID da filial';
COMMENT ON COLUMN SUPR_029.ID_FILIAL_FIM IS 'A posi��o final do ID da filial, se usar posi��es fixas';

-- Braspress:
INSERT INTO SUPR_029 (FORNECEDOR9, CEP_INICIAL, CEP_INICIAL_FIM, ROTA, ROTA_FIM,
    FILIAL_SIGLA, FILIAL_SIGLA_FIM, ID_FILIAL, ID_FILIAL_FIM)
VALUES (48740351, 6, 13, 98, 103, 109, 112, 113, 116);

-- Altera a tabela SUPR_027

ALTER TABLE SUPR_027 DROP CONSTRAINT PK_SUPR_027;
DROP INDEX PK_SUPR_027;

ALTER TABLE SUPR_027 ADD FORNECEDOR9 NUMBER(9);
COMMENT ON COLUMN SUPR_027.FORNECEDOR9 IS 'A transportadora correspondente';

UPDATE SUPR_027 SET FORNECEDOR9 = 48740351 WHERE FORNECEDOR9 IS NULL; -- Braspress

ALTER TABLE SUPR_027 ADD CONSTRAINT FK_SUPR_027_029 FOREIGN KEY (FORNECEDOR9) REFERENCES SUPR_029;
ALTER TABLE SUPR_027 ADD CONSTRAINT PK_SUPR_027 PRIMARY KEY (FORNECEDOR9, CEP);

ALTER TABLE SUPR_027 ADD CEP_FINAL NUMBER(9);
COMMENT ON COLUMN SUPR_027.CEP_FINAL IS 'O CEP final da faixa de CEPs, se houver';

-- Cria e alimenta a tabela SUPR_028

CREATE TABLE SUPR_028 (
CODIGO_EMPRESA NUMBER(3) NOT NULL,
FORNECEDOR9    NUMBER(9),
CODIGO         NUMBER(4) NOT NULL,
CONSTRAINT FK_SUPR_028_FATU_500 FOREIGN KEY (CODIGO_EMPRESA) REFERENCES FATU_500 ON DELETE CASCADE,
CONSTRAINT PK_SUPR_028 PRIMARY KEY (CODIGO_EMPRESA, FORNECEDOR9)
);
COMMENT ON TABLE SUPR_028 IS 'Relaciona a uma empresa do ERP transportadoras com o c�digo de empresa correspondente';
COMMENT ON COLUMN SUPR_028.CODIGO_EMPRESA IS 'O c�digo de empresa do ERP que usa esta configura��o';
COMMENT ON COLUMN SUPR_028.FORNECEDOR9 IS 'A transportadora � qual se destina esta configura��o';
COMMENT ON COLUMN SUPR_028.CODIGO IS 'O c�digo de empresa associado a esta transportadora para esta empresa do ERP';

INSERT INTO SUPR_028 (CODIGO_EMPRESA, FORNECEDOR9, CODIGO)
SELECT CODIGO_EMPRESA, 48740351, VAL_INT
FROM EMPR_008
WHERE PARAM = 'faturamento.etiq.codBrasPress'
AND VAL_INT > 0
ORDER BY 1;

insert into hdoc_035 (codigo_programa, descricao, programa_menu, item_menu_def)
values ('supr_f028', 'C�digo da empresa para rotas de transportadoras', 0, 0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values ('INTERSYS', 1, 'supr_f028', 'supr_menu', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_035 (codigo_programa, descricao, programa_menu, item_menu_def)
values ('supr_f029', 'Configura��o da importa��o de rotas de transportadoras', 0, 0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values ('INTERSYS', 1, 'supr_f029', 'supr_menu', 0, 0, 'S', 'S', 'S', 'S');

