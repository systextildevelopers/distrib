create or replace trigger "INTER_TR_EFIC_100"
  before insert or
         delete or
         update of quantidade,        data_rejeicao,
                   codigo_motivo,     classificacao,
                   prod_rej_nivel99,  prod_rej_grupo,
                   prod_rej_subgrupo, codigo_estagio,
                   turno
  on efic_100
  for each row

declare
   v_rpt_pette number(1);
begin
   if ((inserting or updating) and :new.hora_inicio is null)
   then
      :new.hora_inicio := to_date('01/01/80 00:00:00','DD/MM/YY HH24:MI:SS');
   end if;
   
   -- Verifica se o rpt que esta configurado e Pettenati para tratar
   -- regras específicas de funcionamento
    v_rpt_pette := 0;
    begin
       select count(*)
       into v_rpt_pette
       from fatu_500
       where fatu_500.rpt_ordem_benef = 'pcpb_opb002';
    end;
   

   if inserting
   then
      :new.hora_inicio := sysdate;
      
      if v_rpt_pette > 0
      then
      inter_pr_efic_100_info(:new.data_rejeicao,             :new.prod_rej_nivel99,
                            :new.prod_rej_grupo,         :new.prod_rej_subgrupo,
                            :new.prod_rej_item,            :new.codigo_motivo,
                            :new.area_producao,          :new.periodo_producao,
                            :new.numero_ordem,             :new.classificacao,
                            :new.quantidade,               :new.numero_tubete,
                            :new.codigo_estagio, 		:new.estagio_digitacao,
                            :new.turno,            	 	:new.pedido_venda,
                            :new.observacao, 	 		:new.nivel_mat_prima,
                            :new.grupo_mat_prima, 	:new.sub_mat_prima,
                            :new.item_mat_prima,		:new.parte_peca,
                            :new.maq, 				:new.sub_maq,
                            :new.num_maq, 			:new.codigo_operacao,
                            :new.tipo_apont,         :new.codigo_familia,
                            :new.codigo_intervalo);
     end if;

      inter_pr_alimenta_exec_015(:new.data_rejeicao,  :new.codigo_motivo,
                                 :new.classificacao,  :new.prod_rej_nivel99,
                                 :new.prod_rej_grupo, :new.prod_rej_subgrupo,
                                 :new.codigo_estagio, :new.turno,
                                 :new.quantidade,     0.000);
   end if;

   if updating
   then

      -- Se mudar a chave da exec_015, deve subtrair o registro antigo
      -- e adicionar para o novo.
      if (:old.quantidade        <> :new.quantidade) and
         (:old.data_rejeicao     <> :new.data_rejeicao or
          :old.codigo_motivo     <> :new.codigo_motivo or
          :old.classificacao     <> :new.classificacao or
          :old.prod_rej_nivel99  <> :new.prod_rej_nivel99 or
          :old.prod_rej_grupo    <> :new.prod_rej_grupo or
          :old.prod_rej_subgrupo <> :new.prod_rej_subgrupo or
          :old.codigo_estagio    <> :new.codigo_estagio or
          :old.turno             <> :new.turno or
          :old.codigo_operacao   <> :new.codigo_operacao or
          :old.codigo_familia    <> :new.codigo_familia or
          :old.codigo_intervalo  <> :new.codigo_intervalo or
          :old.tipo_apont        <> :new.tipo_apont)
      then
         
         if v_rpt_pette > 0
         then
            inter_pr_efic_100_info(:new.data_rejeicao,     		:new.prod_rej_nivel99,
                                  :new.prod_rej_grupo, 		:new.prod_rej_subgrupo,
                                  :new.prod_rej_item,    		:new.codigo_motivo,
                                  :new.area_producao,  		:new.periodo_producao,
                                  :new.numero_ordem, 		    :new.classificacao,
                                  (:new.quantidade -:old.quantidade),       		:new.numero_tubete,
                                  :new.codigo_estagio, 		:new.estagio_digitacao,
                                  :new.turno,            	 	:new.pedido_venda,
                                  :new.observacao, 	 		:new.nivel_mat_prima,
                                  :new.grupo_mat_prima, 	:new.sub_mat_prima,
                                  :new.item_mat_prima,		:new.parte_peca,
                                  :new.maq, 				:new.sub_maq,
                                  :new.num_maq, 			:new.codigo_operacao,
                                  :new.tipo_apont,         :new.codigo_familia,
                                  :new.codigo_intervalo);
         end if;
         
         inter_pr_alimenta_exec_015(:old.data_rejeicao,  :old.codigo_motivo,
                                    :old.classificacao,  :old.prod_rej_nivel99,
                                    :old.prod_rej_grupo, :old.prod_rej_subgrupo,
                                    :old.codigo_estagio, :old.turno,
                                    (:old.quantidade * -1), 0.000);

         inter_pr_alimenta_exec_015(:new.data_rejeicao,  :new.codigo_motivo,
                                    :new.classificacao,  :new.prod_rej_nivel99,
                                    :new.prod_rej_grupo, :new.prod_rej_subgrupo,
                                    :new.codigo_estagio, :new.turno,
                                    :new.quantidade,     0.000);

      else

         -- Se alterar somente a quantidade, entao atualiza o valor
         if :old.quantidade <> :new.quantidade
         then
            
            if v_rpt_pette > 0
           then
              inter_pr_efic_100_info(:new.data_rejeicao,     		:new.prod_rej_nivel99,
                                    :new.prod_rej_grupo, 		:new.prod_rej_subgrupo,
                                    :new.prod_rej_item,    		:new.codigo_motivo,
                                    :new.area_producao,  		:new.periodo_producao,
                                    :new.numero_ordem, 		    :new.classificacao,
                                    (:new.quantidade - :old.quantidade),       		:new.numero_tubete,
                                    :new.codigo_estagio, 		:new.estagio_digitacao,
                                    :new.turno,            	 	:new.pedido_venda,
                                    :new.observacao, 	 		:new.nivel_mat_prima,
                                    :new.grupo_mat_prima, 	:new.sub_mat_prima,
                                    :new.item_mat_prima,		:new.parte_peca,
                                    :new.maq, 				:new.sub_maq,
                                    :new.num_maq, 			:new.codigo_operacao,
                                    :new.tipo_apont,         :new.codigo_familia,
                                    :new.codigo_intervalo);
           end if;  
         
            inter_pr_alimenta_exec_015(:old.data_rejeicao,  :old.codigo_motivo,
                                       :old.classificacao,  :old.prod_rej_nivel99,
                                       :old.prod_rej_grupo, :old.prod_rej_subgrupo,
                                       :old.codigo_estagio, :old.turno,
                                       (:new.quantidade - :old.quantidade), 0.000);
         end if;
      end if;
   end if;

   if deleting
   then
      if v_rpt_pette > 0
      then
         inter_pr_efic_100_info(:old.data_rejeicao,     		:old.prod_rej_nivel99,
                              :old.prod_rej_grupo, 		:old.prod_rej_subgrupo,
                              :old.prod_rej_item,    		:old.codigo_motivo,
                              :old.area_producao,  		:old.periodo_producao,
                              :old.numero_ordem, 		    :old.classificacao,
                              (:old.quantidade*-1),       		:old.numero_tubete,
                              :old.codigo_estagio, 		:old.estagio_digitacao,
                              :old.turno,            	 	:old.pedido_venda,
                              :old.observacao, 	 		:old.nivel_mat_prima,
                              :old.grupo_mat_prima, 	:old.sub_mat_prima,
                              :old.item_mat_prima,		:old.parte_peca,
                              :old.maq, 				:old.sub_maq,
                              :old.num_maq, 			:old.codigo_operacao,
                              :old.tipo_apont,         :old.codigo_familia,
                              :old.codigo_intervalo);
      end if;
      
      inter_pr_alimenta_exec_015(:old.data_rejeicao,  :old.codigo_motivo,
                                 :old.classificacao,  :old.prod_rej_nivel99,
                                 :old.prod_rej_grupo, :old.prod_rej_subgrupo,
                                 :old.codigo_estagio, :old.turno,
                                 (:old.quantidade * -1), 0.000);
   end if;

end inter_tr_efic_100;

-- ALTER TRIGGER "INTER_TR_EFIC_100" ENABLE

 

/

exec inter_pr_recompile;

