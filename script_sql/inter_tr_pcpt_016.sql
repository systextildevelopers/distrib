
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_016" 

   before insert or
          delete or
          update of ordem_pedido, quantidade, tipo_destino
   on pcpt_016
   for each row

declare
   p_cd_pano_nivel99     pcpt_010.cd_pano_nivel99%type := '#';
   p_cd_pano_grupo       pcpt_010.cd_pano_grupo%type := '#####';
   p_cd_pano_subgrupo    pcpt_010.cd_pano_subgrupo%type   := '###';
   p_cd_pano_item        pcpt_010.cd_pano_item%type  := '######';
   p_alternativa_item    pcpt_010.alternativa_item%type;

begin
   if inserting
   then
      if :new.tipo_destino = 5
      then
         -- Busca tecido cru da capa
         select pcpt_010.cd_pano_nivel99,       pcpt_010.cd_pano_grupo,
                pcpt_010.cd_pano_subgrupo,      pcpt_010.cd_pano_item,
                pcpt_010.alternativa_item
         into   p_cd_pano_nivel99,              p_cd_pano_grupo,
                p_cd_pano_subgrupo,             p_cd_pano_item,
                p_alternativa_item
         from pcpt_010
         where pcpt_010.ordem_tecelagem  = :new.ordem_tecelagem;

         -- Incluir ligacao com a ordem de planejamento
         inter_pr_destinos_planejamento(:new.ordem_pedido,
                                        :new.ordem_tecelagem,
                                        :new.quantidade,
                                        p_cd_pano_nivel99,
                                        p_cd_pano_grupo,
                                        p_cd_pano_subgrupo,
                                        p_cd_pano_item,
                                        p_alternativa_item,
                                        'I');
      end if;
   end if;

   if deleting
   then
      if :old.tipo_destino = 5
      then
        -- Busca tecido cru da capa
         select pcpt_010.cd_pano_nivel99,       pcpt_010.cd_pano_grupo,
                pcpt_010.cd_pano_subgrupo,      pcpt_010.cd_pano_item,
                pcpt_010.alternativa_item
         into  p_cd_pano_nivel99,              p_cd_pano_grupo,
               p_cd_pano_subgrupo,             p_cd_pano_item,
                p_alternativa_item
         from pcpt_010
         where pcpt_010.ordem_tecelagem  = :old.ordem_tecelagem;

         -- Procedimento que vai executar a explosao da estrutura
         -- do produto e atualizar a quantidade programada na ordem de planejamento
         -- Para retirar a quantidade da ordem antiga
         /*inter_pr_ordem_planejamento('pcpt',
                                     :old.ordem_pedido,
                                     p_cd_pano_nivel99,
                                     p_cd_pano_grupo,
                                     p_cd_pano_subgrupo,
                                     p_cd_pano_item,
                                     p_alternativa_item,
                                     :old.quantidade * -1);*/

         -- Excluir ligacao com a ordem de planejamento
         inter_pr_destinos_planejamento(:old.ordem_pedido,
                                        :old.ordem_tecelagem,
                                        :old.quantidade * (-1),
                                        p_cd_pano_nivel99,
                                        p_cd_pano_grupo,
                                        p_cd_pano_subgrupo,
                                        p_cd_pano_item,
                                        p_alternativa_item,
                                        'D');
      end if;
   end if;

   if updating
   then

      if :old.quantidade <> :new.quantidade
      then
      -- CHAMA PROCEDURE QUE INSERE NA TABELA TEMPORARIA DO RECALCULO
         inter_pr_marca_referencia_op('0', '00000',
                                      '000', '000000',
                                      :new.ordem_tecelagem, 4);
      end if;

      if :new.tipo_destino = 5 or :old.tipo_destino = 5
      then
        -- Busca tecido cru da capa
         select pcpt_010.cd_pano_nivel99,       pcpt_010.cd_pano_grupo,
                pcpt_010.cd_pano_subgrupo,      pcpt_010.cd_pano_item,
                pcpt_010.alternativa_item
         into  p_cd_pano_nivel99,              p_cd_pano_grupo,
               p_cd_pano_subgrupo,             p_cd_pano_item,
                p_alternativa_item
         from pcpt_010
         where pcpt_010.ordem_tecelagem  = :old.ordem_tecelagem;

         if :old.tipo_destino = 5 and :new.tipo_destino <> 5
         then
            -- Procedimento que vai executar a explosao da estrutura
            -- do produto e atualizar a quantidade programada na ordem de planejamento
            -- Para retirar a quantidade da ordem antiga
            /*inter_pr_ordem_planejamento('pcpt',
                                        :old.ordem_pedido,
                                        p_cd_pano_nivel99,
                                        p_cd_pano_grupo,
                                        p_cd_pano_subgrupo,
                                        p_cd_pano_item,
                                        p_alternativa_item,
                                        :old.quantidade * -1);*/

            -- Excluir ligacao com a ordem de planejamento
            inter_pr_destinos_planejamento(:old.ordem_pedido,
                                           :old.ordem_tecelagem,
                                           :old.quantidade,
                                           p_cd_pano_nivel99,
                                           p_cd_pano_grupo,
                                           p_cd_pano_subgrupo,
                                           p_cd_pano_item,
                                           p_alternativa_item,
                                           'D');
         else
            -- Se alterar a Ordem de Planejamento da Ordem de Tecelagem
            if :old.tipo_destino = :new.tipo_destino
            then
               -- Procedimento que vai executar a explosao da estrutura
               -- do produto e atualizar a quantidade programada na ordem de planejamento
               -- Para retirar a quantidade da ordem antiga
               /*inter_pr_ordem_planejamento('pcpt',
                                           :old.ordem_pedido,
                                           p_cd_pano_nivel99,
                                           p_cd_pano_grupo,
                                           p_cd_pano_subgrupo,
                                           p_cd_pano_item,
                                           p_alternativa_item,
                                           :old.quantidade * -1);*/

               -- Excluir ligacao com a ordem de planejamento
               /*delete from tmrp_630
               where tmrp_630.ordem_planejamento    = :old.ordem_pedido
                 and tmrp_630.area_producao         = 4
                 and tmrp_630.ordem_prod_compra     = :old.ordem_tecelagem
                 and tmrp_630.nivel_produto         = p_cd_pano_nivel99
                 and tmrp_630.grupo_produto         = p_cd_pano_grupo
                 and tmrp_630.subgrupo_produto      = p_cd_pano_subgrupo
                 and tmrp_630.item_produto          = p_cd_pano_item;
               */
               -- Procedimento que vai executar a explosao da estrutura
               -- do produto e atualizar a quantidade programada na ordem de planejamento
               -- Para acrescentar a quantidade da ordem nova
               inter_pr_destinos_planejamento(:old.ordem_pedido,
                                              :old.ordem_tecelagem,
                                              :new.quantidade - :old.quantidade,
                                              p_cd_pano_nivel99,
                                              p_cd_pano_grupo,
                                              p_cd_pano_subgrupo,
                                              p_cd_pano_item,
                                              p_alternativa_item,
                                              'U');
            end if;
         end if;
      end if;
   end if;
end inter_tr_pcpt_016;

-- ALTER TRIGGER "INTER_TR_PCPT_016" ENABLE
 

/

exec inter_pr_recompile;

