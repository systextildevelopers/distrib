CREATE TABLE SUPR_680
(
  ID             NUMBER(9) not null,
  FORNECEDOR9    NUMBER(9) not null,
  FORNECEDOR4    NUMBER(4) not null,
  FORNECEDOR2    NUMBER(2) not null,
  TIPO_CHAVE     NUMBER(2) not null,
  CHAVE          varchar2(100)  not null,
  PADRAO         varchar2(1) default 'N'
);

-- Add comments to the columns 
comment on column SUPR_680.TIPO_CHAVE
  is 'Temos 4 tipos de chave: 1 - CPF ou CNPJ, 2 - E-mail, 3 - Número de telefone celular ou 4 - Chave aleatoria';

comment on column SUPR_680.PADRAO
  is 'Define qual é a chave default do fornecedor';

-- Create primary key
ALTER TABLE SUPR_680 ADD CONSTRAINT PK_SUPR_680 PRIMARY KEY (ID);
ALTER TABLE SUPR_680 ADD CONSTRAINT UNIQ_PK_SUPR_680 UNIQUE(FORNECEDOR9, FORNECEDOR4, FORNECEDOR2, TIPO_CHAVE, CHAVE);

-- Create FOREIGN key
ALTER TABLE SUPR_680
  ADD CONSTRAINT FK_RELAC_PIX_FORNECEDOR 
  FOREIGN KEY (FORNECEDOR9, FORNECEDOR4, FORNECEDOR2)
  REFERENCES SUPR_010(fornecedor9, fornecedor4, fornecedor2);

/
exec inter_pr_recompile;
/
