
  CREATE OR REPLACE TRIGGER "INTER_TR_ESTQ_060_LOG" 
after insert or delete
or update of serie_nota, transacao_ent, seq_nota_fiscal, data_entrada,
	codigo_embalagem, proforma, valor_entrada, valor_despesas,
	qtde_pecas_emb, data_transacao, torcao, nota_fisc_ent,
	seri_fisc_ent, sequ_fisc_ent, nome_prog, periodo_producao,
	qualidade_fio, pre_romaneio, grupo_maquina, sub_maquina,
	numero_maquina, cnpj_fornecedor9, cnpj_fornecedor4, cnpj_fornecedor2,
	data_insercao, data_cardex, transacao_cardex, usuario_cardex,
	valor_movto_cardex, valor_contabil_cardex, tabela_origem_cardex, ordem_agrupamento,
	caixa_original, turno_original, codigo_pesador, divisao_producao,
	ordem_producao, peso_adicional, arreada, restricao,
	deposito_producao, transacao_producao, sequencia_plano, cod_empresa_sai,
	sigla_inmetro, centro_custo_cardex, numero_caixa, turno,
	codigo_deposito, lote, prodcai_nivel99, prodcai_grupo,
	prodcai_subgrupo, prodcai_item, data_producao, peso_liquido,
	peso_bruto, peso_embalagem, preco_medio, endereco_caixa,
	numero_pedido, sequencia_pedido, nr_solicitacao, status_caixa,
	nota_fiscal
on ESTQ_060
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   v_nome_programa := inter_fn_nome_programa(ws_sid);   


 if inserting
 then
    begin

        insert into ESTQ_060_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           NUMERO_CAIXA_OLD,   /*8*/
           NUMERO_CAIXA_NEW,   /*9*/
           TURNO_OLD,   /*10*/
           TURNO_NEW,   /*11*/
           CODIGO_DEPOSITO_OLD,   /*12*/
           CODIGO_DEPOSITO_NEW,   /*13*/
           LOTE_OLD,   /*14*/
           LOTE_NEW,   /*15*/
           PRODCAI_NIVEL99_OLD,   /*16*/
           PRODCAI_NIVEL99_NEW,   /*17*/
           PRODCAI_GRUPO_OLD,   /*18*/
           PRODCAI_GRUPO_NEW,   /*19*/
           PRODCAI_SUBGRUPO_OLD,   /*20*/
           PRODCAI_SUBGRUPO_NEW,   /*21*/
           PRODCAI_ITEM_OLD,   /*22*/
           PRODCAI_ITEM_NEW,   /*23*/
           DATA_PRODUCAO_OLD,   /*24*/
           DATA_PRODUCAO_NEW,   /*25*/
           PESO_LIQUIDO_OLD,   /*26*/
           PESO_LIQUIDO_NEW,   /*27*/
           PESO_BRUTO_OLD,   /*28*/
           PESO_BRUTO_NEW,   /*29*/
           PESO_EMBALAGEM_OLD,   /*30*/
           PESO_EMBALAGEM_NEW,   /*31*/
           PRECO_MEDIO_OLD,   /*32*/
           PRECO_MEDIO_NEW,   /*33*/
           ENDERECO_CAIXA_OLD,   /*34*/
           ENDERECO_CAIXA_NEW,   /*35*/
           NUMERO_PEDIDO_OLD,   /*36*/
           NUMERO_PEDIDO_NEW,   /*37*/
           SEQUENCIA_PEDIDO_OLD,   /*38*/
           SEQUENCIA_PEDIDO_NEW,   /*39*/
           NR_SOLICITACAO_OLD,   /*40*/
           NR_SOLICITACAO_NEW,   /*41*/
           STATUS_CAIXA_OLD,   /*42*/
           STATUS_CAIXA_NEW,   /*43*/
           NOTA_FISCAL_OLD,   /*44*/
           NOTA_FISCAL_NEW,   /*45*/
           SERIE_NOTA_OLD,   /*46*/
           SERIE_NOTA_NEW,   /*47*/
           TRANSACAO_ENT_OLD,   /*48*/
           TRANSACAO_ENT_NEW,   /*49*/
           SEQ_NOTA_FISCAL_OLD,   /*50*/
           SEQ_NOTA_FISCAL_NEW,   /*51*/
           DATA_ENTRADA_OLD,   /*52*/
           DATA_ENTRADA_NEW,   /*53*/
           CODIGO_EMBALAGEM_OLD,   /*54*/
           CODIGO_EMBALAGEM_NEW,   /*55*/
           PROFORMA_OLD,   /*56*/
           PROFORMA_NEW,   /*57*/
           VALOR_ENTRADA_OLD,   /*58*/
           VALOR_ENTRADA_NEW,   /*59*/
           VALOR_DESPESAS_OLD,   /*60*/
           VALOR_DESPESAS_NEW,   /*61*/
           QTDE_PECAS_EMB_OLD,   /*62*/
           QTDE_PECAS_EMB_NEW,   /*63*/
           DATA_TRANSACAO_OLD,   /*64*/
           DATA_TRANSACAO_NEW,   /*65*/
           TORCAO_OLD,   /*66*/
           TORCAO_NEW,   /*67*/
           NOTA_FISC_ENT_OLD,   /*68*/
           NOTA_FISC_ENT_NEW,   /*69*/
           SERI_FISC_ENT_OLD,   /*70*/
           SERI_FISC_ENT_NEW,   /*71*/
           SEQU_FISC_ENT_OLD,   /*72*/
           SEQU_FISC_ENT_NEW,   /*73*/
           NOME_PROG_OLD,   /*74*/
           NOME_PROG_NEW,   /*75*/
           PERIODO_PRODUCAO_OLD,   /*76*/
           PERIODO_PRODUCAO_NEW,   /*77*/
           QUALIDADE_FIO_OLD,   /*78*/
           QUALIDADE_FIO_NEW,   /*79*/
           PRE_ROMANEIO_OLD,   /*80*/
           PRE_ROMANEIO_NEW,   /*81*/
           GRUPO_MAQUINA_OLD,   /*82*/
           GRUPO_MAQUINA_NEW,   /*83*/
           SUB_MAQUINA_OLD,   /*84*/
           SUB_MAQUINA_NEW,   /*85*/
           NUMERO_MAQUINA_OLD,   /*86*/
           NUMERO_MAQUINA_NEW,   /*87*/
           CNPJ_FORNECEDOR9_OLD,   /*88*/
           CNPJ_FORNECEDOR9_NEW,   /*89*/
           CNPJ_FORNECEDOR4_OLD,   /*90*/
           CNPJ_FORNECEDOR4_NEW,   /*91*/
           CNPJ_FORNECEDOR2_OLD,   /*92*/
           CNPJ_FORNECEDOR2_NEW,   /*93*/
           DATA_INSERCAO_OLD,   /*94*/
           DATA_INSERCAO_NEW,   /*95*/
           DATA_CARDEX_OLD,   /*96*/
           DATA_CARDEX_NEW,   /*97*/
           TRANSACAO_CARDEX_OLD,   /*98*/
           TRANSACAO_CARDEX_NEW,   /*99*/
           USUARIO_CARDEX_OLD,   /*100*/
           USUARIO_CARDEX_NEW,   /*101*/
           VALOR_MOVTO_CARDEX_OLD,   /*102*/
           VALOR_MOVTO_CARDEX_NEW,   /*103*/
           VALOR_CONTABIL_CARDEX_OLD,   /*104*/
           VALOR_CONTABIL_CARDEX_NEW,   /*105*/
           TABELA_ORIGEM_CARDEX_OLD,   /*106*/
           TABELA_ORIGEM_CARDEX_NEW,   /*107*/
           ORDEM_AGRUPAMENTO_OLD,   /*108*/
           ORDEM_AGRUPAMENTO_NEW,   /*109*/
           CAIXA_ORIGINAL_OLD,   /*110*/
           CAIXA_ORIGINAL_NEW,   /*111*/
           TURNO_ORIGINAL_OLD,   /*112*/
           TURNO_ORIGINAL_NEW,   /*113*/
           CODIGO_PESADOR_OLD,   /*114*/
           CODIGO_PESADOR_NEW,   /*115*/
           DIVISAO_PRODUCAO_OLD,   /*116*/
           DIVISAO_PRODUCAO_NEW,   /*117*/
           ORDEM_PRODUCAO_OLD,   /*118*/
           ORDEM_PRODUCAO_NEW,   /*119*/
           PESO_ADICIONAL_OLD,   /*120*/
           PESO_ADICIONAL_NEW,   /*121*/
           ARREADA_OLD,   /*122*/
           ARREADA_NEW,   /*123*/
           RESTRICAO_OLD,   /*124*/
           RESTRICAO_NEW,   /*125*/
           DEPOSITO_PRODUCAO_OLD,   /*126*/
           DEPOSITO_PRODUCAO_NEW,   /*127*/
           TRANSACAO_PRODUCAO_OLD,   /*128*/
           TRANSACAO_PRODUCAO_NEW,   /*129*/
           SEQUENCIA_PLANO_OLD,   /*130*/
           SEQUENCIA_PLANO_NEW,   /*131*/
           COD_EMPRESA_SAI_OLD,   /*132*/
           COD_EMPRESA_SAI_NEW,   /*133*/
           SIGLA_INMETRO_OLD,   /*134*/
           SIGLA_INMETRO_NEW,   /*135*/
           CENTRO_CUSTO_CARDEX_OLD,   /*136*/
           CENTRO_CUSTO_CARDEX_NEW    /*137*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.NUMERO_CAIXA, /*9*/
           0,/*10*/
           :new.TURNO, /*11*/
           0,/*12*/
           :new.CODIGO_DEPOSITO, /*13*/
           0,/*14*/
           :new.LOTE, /*15*/
           '',/*16*/
           :new.PRODCAI_NIVEL99, /*17*/
           '',/*18*/
           :new.PRODCAI_GRUPO, /*19*/
           '',/*20*/
           :new.PRODCAI_SUBGRUPO, /*21*/
           '',/*22*/
           :new.PRODCAI_ITEM, /*23*/
           null,/*24*/
           :new.DATA_PRODUCAO, /*25*/
           0,/*26*/
           :new.PESO_LIQUIDO, /*27*/
           0,/*28*/
           :new.PESO_BRUTO, /*29*/
           0,/*30*/
           :new.PESO_EMBALAGEM, /*31*/
           0,/*32*/
           :new.PRECO_MEDIO, /*33*/
           '',/*34*/
           :new.ENDERECO_CAIXA, /*35*/
           0,/*36*/
           :new.NUMERO_PEDIDO, /*37*/
           0,/*38*/
           :new.SEQUENCIA_PEDIDO, /*39*/
           0,/*40*/
           :new.NR_SOLICITACAO, /*41*/
           0,/*42*/
           :new.STATUS_CAIXA, /*43*/
           0,/*44*/
           :new.NOTA_FISCAL, /*45*/
           '',/*46*/
           :new.SERIE_NOTA, /*47*/
           0,/*48*/
           :new.TRANSACAO_ENT, /*49*/
           0,/*50*/
           :new.SEQ_NOTA_FISCAL, /*51*/
           null,/*52*/
           :new.DATA_ENTRADA, /*53*/
           0,/*54*/
           :new.CODIGO_EMBALAGEM, /*55*/
           '',/*56*/
           :new.PROFORMA, /*57*/
           0,/*58*/
           :new.VALOR_ENTRADA, /*59*/
           0,/*60*/
           :new.VALOR_DESPESAS, /*61*/
           0,/*62*/
           :new.QTDE_PECAS_EMB, /*63*/
           null,/*64*/
           :new.DATA_TRANSACAO, /*65*/
           '',/*66*/
           :new.TORCAO, /*67*/
           0,/*68*/
           :new.NOTA_FISC_ENT, /*69*/
           '',/*70*/
           :new.SERI_FISC_ENT, /*71*/
           0,/*72*/
           :new.SEQU_FISC_ENT, /*73*/
           '',/*74*/
           :new.NOME_PROG, /*75*/
           0,/*76*/
           :new.PERIODO_PRODUCAO, /*77*/
           0,/*78*/
           :new.QUALIDADE_FIO, /*79*/
           0,/*80*/
           :new.PRE_ROMANEIO, /*81*/
           '',/*82*/
           :new.GRUPO_MAQUINA, /*83*/
           '',/*84*/
           :new.SUB_MAQUINA, /*85*/
           0,/*86*/
           :new.NUMERO_MAQUINA, /*87*/
           0,/*88*/
           :new.CNPJ_FORNECEDOR9, /*89*/
           0,/*90*/
           :new.CNPJ_FORNECEDOR4, /*91*/
           0,/*92*/
           :new.CNPJ_FORNECEDOR2, /*93*/
           null,/*94*/
           :new.DATA_INSERCAO, /*95*/
           null,/*96*/
           :new.DATA_CARDEX, /*97*/
           0,/*98*/
           :new.TRANSACAO_CARDEX, /*99*/
           '',/*100*/
           :new.USUARIO_CARDEX, /*101*/
           0,/*102*/
           :new.VALOR_MOVTO_CARDEX, /*103*/
           0,/*104*/
           :new.VALOR_CONTABIL_CARDEX, /*105*/
           '',/*106*/
           :new.TABELA_ORIGEM_CARDEX, /*107*/
           0,/*108*/
           :new.ORDEM_AGRUPAMENTO, /*109*/
           0,/*110*/
           :new.CAIXA_ORIGINAL, /*111*/
           0,/*112*/
           :new.TURNO_ORIGINAL, /*113*/
           0,/*114*/
           :new.CODIGO_PESADOR, /*115*/
           0,/*116*/
           :new.DIVISAO_PRODUCAO, /*117*/
           0,/*118*/
           :new.ORDEM_PRODUCAO, /*119*/
           0,/*120*/
           :new.PESO_ADICIONAL, /*121*/
           0,/*122*/
           :new.ARREADA, /*123*/
           '',/*124*/
           :new.RESTRICAO, /*125*/
           0,/*126*/
           :new.DEPOSITO_PRODUCAO, /*127*/
           0,/*128*/
           :new.TRANSACAO_PRODUCAO, /*129*/
           0,/*130*/
           :new.SEQUENCIA_PLANO, /*131*/
           0,/*132*/
           :new.COD_EMPRESA_SAI, /*133*/
           '',/*134*/
           :new.SIGLA_INMETRO, /*135*/
           0,/*136*/
           :new.CENTRO_CUSTO_CARDEX /*137*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into ESTQ_060_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           NUMERO_CAIXA_OLD, /*8*/
           NUMERO_CAIXA_NEW, /*9*/
           TURNO_OLD, /*10*/
           TURNO_NEW, /*11*/
           CODIGO_DEPOSITO_OLD, /*12*/
           CODIGO_DEPOSITO_NEW, /*13*/
           LOTE_OLD, /*14*/
           LOTE_NEW, /*15*/
           PRODCAI_NIVEL99_OLD, /*16*/
           PRODCAI_NIVEL99_NEW, /*17*/
           PRODCAI_GRUPO_OLD, /*18*/
           PRODCAI_GRUPO_NEW, /*19*/
           PRODCAI_SUBGRUPO_OLD, /*20*/
           PRODCAI_SUBGRUPO_NEW, /*21*/
           PRODCAI_ITEM_OLD, /*22*/
           PRODCAI_ITEM_NEW, /*23*/
           DATA_PRODUCAO_OLD, /*24*/
           DATA_PRODUCAO_NEW, /*25*/
           PESO_LIQUIDO_OLD, /*26*/
           PESO_LIQUIDO_NEW, /*27*/
           PESO_BRUTO_OLD, /*28*/
           PESO_BRUTO_NEW, /*29*/
           PESO_EMBALAGEM_OLD, /*30*/
           PESO_EMBALAGEM_NEW, /*31*/
           PRECO_MEDIO_OLD, /*32*/
           PRECO_MEDIO_NEW, /*33*/
           ENDERECO_CAIXA_OLD, /*34*/
           ENDERECO_CAIXA_NEW, /*35*/
           NUMERO_PEDIDO_OLD, /*36*/
           NUMERO_PEDIDO_NEW, /*37*/
           SEQUENCIA_PEDIDO_OLD, /*38*/
           SEQUENCIA_PEDIDO_NEW, /*39*/
           NR_SOLICITACAO_OLD, /*40*/
           NR_SOLICITACAO_NEW, /*41*/
           STATUS_CAIXA_OLD, /*42*/
           STATUS_CAIXA_NEW, /*43*/
           NOTA_FISCAL_OLD, /*44*/
           NOTA_FISCAL_NEW, /*45*/
           SERIE_NOTA_OLD, /*46*/
           SERIE_NOTA_NEW, /*47*/
           TRANSACAO_ENT_OLD, /*48*/
           TRANSACAO_ENT_NEW, /*49*/
           SEQ_NOTA_FISCAL_OLD, /*50*/
           SEQ_NOTA_FISCAL_NEW, /*51*/
           DATA_ENTRADA_OLD, /*52*/
           DATA_ENTRADA_NEW, /*53*/
           CODIGO_EMBALAGEM_OLD, /*54*/
           CODIGO_EMBALAGEM_NEW, /*55*/
           PROFORMA_OLD, /*56*/
           PROFORMA_NEW, /*57*/
           VALOR_ENTRADA_OLD, /*58*/
           VALOR_ENTRADA_NEW, /*59*/
           VALOR_DESPESAS_OLD, /*60*/
           VALOR_DESPESAS_NEW, /*61*/
           QTDE_PECAS_EMB_OLD, /*62*/
           QTDE_PECAS_EMB_NEW, /*63*/
           DATA_TRANSACAO_OLD, /*64*/
           DATA_TRANSACAO_NEW, /*65*/
           TORCAO_OLD, /*66*/
           TORCAO_NEW, /*67*/
           NOTA_FISC_ENT_OLD, /*68*/
           NOTA_FISC_ENT_NEW, /*69*/
           SERI_FISC_ENT_OLD, /*70*/
           SERI_FISC_ENT_NEW, /*71*/
           SEQU_FISC_ENT_OLD, /*72*/
           SEQU_FISC_ENT_NEW, /*73*/
           NOME_PROG_OLD, /*74*/
           NOME_PROG_NEW, /*75*/
           PERIODO_PRODUCAO_OLD, /*76*/
           PERIODO_PRODUCAO_NEW, /*77*/
           QUALIDADE_FIO_OLD, /*78*/
           QUALIDADE_FIO_NEW, /*79*/
           PRE_ROMANEIO_OLD, /*80*/
           PRE_ROMANEIO_NEW, /*81*/
           GRUPO_MAQUINA_OLD, /*82*/
           GRUPO_MAQUINA_NEW, /*83*/
           SUB_MAQUINA_OLD, /*84*/
           SUB_MAQUINA_NEW, /*85*/
           NUMERO_MAQUINA_OLD, /*86*/
           NUMERO_MAQUINA_NEW, /*87*/
           CNPJ_FORNECEDOR9_OLD, /*88*/
           CNPJ_FORNECEDOR9_NEW, /*89*/
           CNPJ_FORNECEDOR4_OLD, /*90*/
           CNPJ_FORNECEDOR4_NEW, /*91*/
           CNPJ_FORNECEDOR2_OLD, /*92*/
           CNPJ_FORNECEDOR2_NEW, /*93*/
           DATA_INSERCAO_OLD, /*94*/
           DATA_INSERCAO_NEW, /*95*/
           DATA_CARDEX_OLD, /*96*/
           DATA_CARDEX_NEW, /*97*/
           TRANSACAO_CARDEX_OLD, /*98*/
           TRANSACAO_CARDEX_NEW, /*99*/
           USUARIO_CARDEX_OLD, /*100*/
           USUARIO_CARDEX_NEW, /*101*/
           VALOR_MOVTO_CARDEX_OLD, /*102*/
           VALOR_MOVTO_CARDEX_NEW, /*103*/
           VALOR_CONTABIL_CARDEX_OLD, /*104*/
           VALOR_CONTABIL_CARDEX_NEW, /*105*/
           TABELA_ORIGEM_CARDEX_OLD, /*106*/
           TABELA_ORIGEM_CARDEX_NEW, /*107*/
           ORDEM_AGRUPAMENTO_OLD, /*108*/
           ORDEM_AGRUPAMENTO_NEW, /*109*/
           CAIXA_ORIGINAL_OLD, /*110*/
           CAIXA_ORIGINAL_NEW, /*111*/
           TURNO_ORIGINAL_OLD, /*112*/
           TURNO_ORIGINAL_NEW, /*113*/
           CODIGO_PESADOR_OLD, /*114*/
           CODIGO_PESADOR_NEW, /*115*/
           DIVISAO_PRODUCAO_OLD, /*116*/
           DIVISAO_PRODUCAO_NEW, /*117*/
           ORDEM_PRODUCAO_OLD, /*118*/
           ORDEM_PRODUCAO_NEW, /*119*/
           PESO_ADICIONAL_OLD, /*120*/
           PESO_ADICIONAL_NEW, /*121*/
           ARREADA_OLD, /*122*/
           ARREADA_NEW, /*123*/
           RESTRICAO_OLD, /*124*/
           RESTRICAO_NEW, /*125*/
           DEPOSITO_PRODUCAO_OLD, /*126*/
           DEPOSITO_PRODUCAO_NEW, /*127*/
           TRANSACAO_PRODUCAO_OLD, /*128*/
           TRANSACAO_PRODUCAO_NEW, /*129*/
           SEQUENCIA_PLANO_OLD, /*130*/
           SEQUENCIA_PLANO_NEW, /*131*/
           COD_EMPRESA_SAI_OLD, /*132*/
           COD_EMPRESA_SAI_NEW, /*133*/
           SIGLA_INMETRO_OLD, /*134*/
           SIGLA_INMETRO_NEW, /*135*/
           CENTRO_CUSTO_CARDEX_OLD, /*136*/
           CENTRO_CUSTO_CARDEX_NEW  /*137*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.NUMERO_CAIXA,  /*8*/
           :new.NUMERO_CAIXA, /*9*/
           :old.TURNO,  /*10*/
           :new.TURNO, /*11*/
           :old.CODIGO_DEPOSITO,  /*12*/
           :new.CODIGO_DEPOSITO, /*13*/
           :old.LOTE,  /*14*/
           :new.LOTE, /*15*/
           :old.PRODCAI_NIVEL99,  /*16*/
           :new.PRODCAI_NIVEL99, /*17*/
           :old.PRODCAI_GRUPO,  /*18*/
           :new.PRODCAI_GRUPO, /*19*/
           :old.PRODCAI_SUBGRUPO,  /*20*/
           :new.PRODCAI_SUBGRUPO, /*21*/
           :old.PRODCAI_ITEM,  /*22*/
           :new.PRODCAI_ITEM, /*23*/
           :old.DATA_PRODUCAO,  /*24*/
           :new.DATA_PRODUCAO, /*25*/
           :old.PESO_LIQUIDO,  /*26*/
           :new.PESO_LIQUIDO, /*27*/
           :old.PESO_BRUTO,  /*28*/
           :new.PESO_BRUTO, /*29*/
           :old.PESO_EMBALAGEM,  /*30*/
           :new.PESO_EMBALAGEM, /*31*/
           :old.PRECO_MEDIO,  /*32*/
           :new.PRECO_MEDIO, /*33*/
           :old.ENDERECO_CAIXA,  /*34*/
           :new.ENDERECO_CAIXA, /*35*/
           :old.NUMERO_PEDIDO,  /*36*/
           :new.NUMERO_PEDIDO, /*37*/
           :old.SEQUENCIA_PEDIDO,  /*38*/
           :new.SEQUENCIA_PEDIDO, /*39*/
           :old.NR_SOLICITACAO,  /*40*/
           :new.NR_SOLICITACAO, /*41*/
           :old.STATUS_CAIXA,  /*42*/
           :new.STATUS_CAIXA, /*43*/
           :old.NOTA_FISCAL,  /*44*/
           :new.NOTA_FISCAL, /*45*/
           :old.SERIE_NOTA,  /*46*/
           :new.SERIE_NOTA, /*47*/
           :old.TRANSACAO_ENT,  /*48*/
           :new.TRANSACAO_ENT, /*49*/
           :old.SEQ_NOTA_FISCAL,  /*50*/
           :new.SEQ_NOTA_FISCAL, /*51*/
           :old.DATA_ENTRADA,  /*52*/
           :new.DATA_ENTRADA, /*53*/
           :old.CODIGO_EMBALAGEM,  /*54*/
           :new.CODIGO_EMBALAGEM, /*55*/
           :old.PROFORMA,  /*56*/
           :new.PROFORMA, /*57*/
           :old.VALOR_ENTRADA,  /*58*/
           :new.VALOR_ENTRADA, /*59*/
           :old.VALOR_DESPESAS,  /*60*/
           :new.VALOR_DESPESAS, /*61*/
           :old.QTDE_PECAS_EMB,  /*62*/
           :new.QTDE_PECAS_EMB, /*63*/
           :old.DATA_TRANSACAO,  /*64*/
           :new.DATA_TRANSACAO, /*65*/
           :old.TORCAO,  /*66*/
           :new.TORCAO, /*67*/
           :old.NOTA_FISC_ENT,  /*68*/
           :new.NOTA_FISC_ENT, /*69*/
           :old.SERI_FISC_ENT,  /*70*/
           :new.SERI_FISC_ENT, /*71*/
           :old.SEQU_FISC_ENT,  /*72*/
           :new.SEQU_FISC_ENT, /*73*/
           :old.NOME_PROG,  /*74*/
           :new.NOME_PROG, /*75*/
           :old.PERIODO_PRODUCAO,  /*76*/
           :new.PERIODO_PRODUCAO, /*77*/
           :old.QUALIDADE_FIO,  /*78*/
           :new.QUALIDADE_FIO, /*79*/
           :old.PRE_ROMANEIO,  /*80*/
           :new.PRE_ROMANEIO, /*81*/
           :old.GRUPO_MAQUINA,  /*82*/
           :new.GRUPO_MAQUINA, /*83*/
           :old.SUB_MAQUINA,  /*84*/
           :new.SUB_MAQUINA, /*85*/
           :old.NUMERO_MAQUINA,  /*86*/
           :new.NUMERO_MAQUINA, /*87*/
           :old.CNPJ_FORNECEDOR9,  /*88*/
           :new.CNPJ_FORNECEDOR9, /*89*/
           :old.CNPJ_FORNECEDOR4,  /*90*/
           :new.CNPJ_FORNECEDOR4, /*91*/
           :old.CNPJ_FORNECEDOR2,  /*92*/
           :new.CNPJ_FORNECEDOR2, /*93*/
           :old.DATA_INSERCAO,  /*94*/
           :new.DATA_INSERCAO, /*95*/
           :old.DATA_CARDEX,  /*96*/
           :new.DATA_CARDEX, /*97*/
           :old.TRANSACAO_CARDEX,  /*98*/
           :new.TRANSACAO_CARDEX, /*99*/
           :old.USUARIO_CARDEX,  /*100*/
           :new.USUARIO_CARDEX, /*101*/
           :old.VALOR_MOVTO_CARDEX,  /*102*/
           :new.VALOR_MOVTO_CARDEX, /*103*/
           :old.VALOR_CONTABIL_CARDEX,  /*104*/
           :new.VALOR_CONTABIL_CARDEX, /*105*/
           :old.TABELA_ORIGEM_CARDEX,  /*106*/
           :new.TABELA_ORIGEM_CARDEX, /*107*/
           :old.ORDEM_AGRUPAMENTO,  /*108*/
           :new.ORDEM_AGRUPAMENTO, /*109*/
           :old.CAIXA_ORIGINAL,  /*110*/
           :new.CAIXA_ORIGINAL, /*111*/
           :old.TURNO_ORIGINAL,  /*112*/
           :new.TURNO_ORIGINAL, /*113*/
           :old.CODIGO_PESADOR,  /*114*/
           :new.CODIGO_PESADOR, /*115*/
           :old.DIVISAO_PRODUCAO,  /*116*/
           :new.DIVISAO_PRODUCAO, /*117*/
           :old.ORDEM_PRODUCAO,  /*118*/
           :new.ORDEM_PRODUCAO, /*119*/
           :old.PESO_ADICIONAL,  /*120*/
           :new.PESO_ADICIONAL, /*121*/
           :old.ARREADA,  /*122*/
           :new.ARREADA, /*123*/
           :old.RESTRICAO,  /*124*/
           :new.RESTRICAO, /*125*/
           :old.DEPOSITO_PRODUCAO,  /*126*/
           :new.DEPOSITO_PRODUCAO, /*127*/
           :old.TRANSACAO_PRODUCAO,  /*128*/
           :new.TRANSACAO_PRODUCAO, /*129*/
           :old.SEQUENCIA_PLANO,  /*130*/
           :new.SEQUENCIA_PLANO, /*131*/
           :old.COD_EMPRESA_SAI,  /*132*/
           :new.COD_EMPRESA_SAI, /*133*/
           :old.SIGLA_INMETRO,  /*134*/
           :new.SIGLA_INMETRO, /*135*/
           :old.CENTRO_CUSTO_CARDEX,  /*136*/
           :new.CENTRO_CUSTO_CARDEX  /*137*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into ESTQ_060_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           NUMERO_CAIXA_OLD, /*8*/
           NUMERO_CAIXA_NEW, /*9*/
           TURNO_OLD, /*10*/
           TURNO_NEW, /*11*/
           CODIGO_DEPOSITO_OLD, /*12*/
           CODIGO_DEPOSITO_NEW, /*13*/
           LOTE_OLD, /*14*/
           LOTE_NEW, /*15*/
           PRODCAI_NIVEL99_OLD, /*16*/
           PRODCAI_NIVEL99_NEW, /*17*/
           PRODCAI_GRUPO_OLD, /*18*/
           PRODCAI_GRUPO_NEW, /*19*/
           PRODCAI_SUBGRUPO_OLD, /*20*/
           PRODCAI_SUBGRUPO_NEW, /*21*/
           PRODCAI_ITEM_OLD, /*22*/
           PRODCAI_ITEM_NEW, /*23*/
           DATA_PRODUCAO_OLD, /*24*/
           DATA_PRODUCAO_NEW, /*25*/
           PESO_LIQUIDO_OLD, /*26*/
           PESO_LIQUIDO_NEW, /*27*/
           PESO_BRUTO_OLD, /*28*/
           PESO_BRUTO_NEW, /*29*/
           PESO_EMBALAGEM_OLD, /*30*/
           PESO_EMBALAGEM_NEW, /*31*/
           PRECO_MEDIO_OLD, /*32*/
           PRECO_MEDIO_NEW, /*33*/
           ENDERECO_CAIXA_OLD, /*34*/
           ENDERECO_CAIXA_NEW, /*35*/
           NUMERO_PEDIDO_OLD, /*36*/
           NUMERO_PEDIDO_NEW, /*37*/
           SEQUENCIA_PEDIDO_OLD, /*38*/
           SEQUENCIA_PEDIDO_NEW, /*39*/
           NR_SOLICITACAO_OLD, /*40*/
           NR_SOLICITACAO_NEW, /*41*/
           STATUS_CAIXA_OLD, /*42*/
           STATUS_CAIXA_NEW, /*43*/
           NOTA_FISCAL_OLD, /*44*/
           NOTA_FISCAL_NEW, /*45*/
           SERIE_NOTA_OLD, /*46*/
           SERIE_NOTA_NEW, /*47*/
           TRANSACAO_ENT_OLD, /*48*/
           TRANSACAO_ENT_NEW, /*49*/
           SEQ_NOTA_FISCAL_OLD, /*50*/
           SEQ_NOTA_FISCAL_NEW, /*51*/
           DATA_ENTRADA_OLD, /*52*/
           DATA_ENTRADA_NEW, /*53*/
           CODIGO_EMBALAGEM_OLD, /*54*/
           CODIGO_EMBALAGEM_NEW, /*55*/
           PROFORMA_OLD, /*56*/
           PROFORMA_NEW, /*57*/
           VALOR_ENTRADA_OLD, /*58*/
           VALOR_ENTRADA_NEW, /*59*/
           VALOR_DESPESAS_OLD, /*60*/
           VALOR_DESPESAS_NEW, /*61*/
           QTDE_PECAS_EMB_OLD, /*62*/
           QTDE_PECAS_EMB_NEW, /*63*/
           DATA_TRANSACAO_OLD, /*64*/
           DATA_TRANSACAO_NEW, /*65*/
           TORCAO_OLD, /*66*/
           TORCAO_NEW, /*67*/
           NOTA_FISC_ENT_OLD, /*68*/
           NOTA_FISC_ENT_NEW, /*69*/
           SERI_FISC_ENT_OLD, /*70*/
           SERI_FISC_ENT_NEW, /*71*/
           SEQU_FISC_ENT_OLD, /*72*/
           SEQU_FISC_ENT_NEW, /*73*/
           NOME_PROG_OLD, /*74*/
           NOME_PROG_NEW, /*75*/
           PERIODO_PRODUCAO_OLD, /*76*/
           PERIODO_PRODUCAO_NEW, /*77*/
           QUALIDADE_FIO_OLD, /*78*/
           QUALIDADE_FIO_NEW, /*79*/
           PRE_ROMANEIO_OLD, /*80*/
           PRE_ROMANEIO_NEW, /*81*/
           GRUPO_MAQUINA_OLD, /*82*/
           GRUPO_MAQUINA_NEW, /*83*/
           SUB_MAQUINA_OLD, /*84*/
           SUB_MAQUINA_NEW, /*85*/
           NUMERO_MAQUINA_OLD, /*86*/
           NUMERO_MAQUINA_NEW, /*87*/
           CNPJ_FORNECEDOR9_OLD, /*88*/
           CNPJ_FORNECEDOR9_NEW, /*89*/
           CNPJ_FORNECEDOR4_OLD, /*90*/
           CNPJ_FORNECEDOR4_NEW, /*91*/
           CNPJ_FORNECEDOR2_OLD, /*92*/
           CNPJ_FORNECEDOR2_NEW, /*93*/
           DATA_INSERCAO_OLD, /*94*/
           DATA_INSERCAO_NEW, /*95*/
           DATA_CARDEX_OLD, /*96*/
           DATA_CARDEX_NEW, /*97*/
           TRANSACAO_CARDEX_OLD, /*98*/
           TRANSACAO_CARDEX_NEW, /*99*/
           USUARIO_CARDEX_OLD, /*100*/
           USUARIO_CARDEX_NEW, /*101*/
           VALOR_MOVTO_CARDEX_OLD, /*102*/
           VALOR_MOVTO_CARDEX_NEW, /*103*/
           VALOR_CONTABIL_CARDEX_OLD, /*104*/
           VALOR_CONTABIL_CARDEX_NEW, /*105*/
           TABELA_ORIGEM_CARDEX_OLD, /*106*/
           TABELA_ORIGEM_CARDEX_NEW, /*107*/
           ORDEM_AGRUPAMENTO_OLD, /*108*/
           ORDEM_AGRUPAMENTO_NEW, /*109*/
           CAIXA_ORIGINAL_OLD, /*110*/
           CAIXA_ORIGINAL_NEW, /*111*/
           TURNO_ORIGINAL_OLD, /*112*/
           TURNO_ORIGINAL_NEW, /*113*/
           CODIGO_PESADOR_OLD, /*114*/
           CODIGO_PESADOR_NEW, /*115*/
           DIVISAO_PRODUCAO_OLD, /*116*/
           DIVISAO_PRODUCAO_NEW, /*117*/
           ORDEM_PRODUCAO_OLD, /*118*/
           ORDEM_PRODUCAO_NEW, /*119*/
           PESO_ADICIONAL_OLD, /*120*/
           PESO_ADICIONAL_NEW, /*121*/
           ARREADA_OLD, /*122*/
           ARREADA_NEW, /*123*/
           RESTRICAO_OLD, /*124*/
           RESTRICAO_NEW, /*125*/
           DEPOSITO_PRODUCAO_OLD, /*126*/
           DEPOSITO_PRODUCAO_NEW, /*127*/
           TRANSACAO_PRODUCAO_OLD, /*128*/
           TRANSACAO_PRODUCAO_NEW, /*129*/
           SEQUENCIA_PLANO_OLD, /*130*/
           SEQUENCIA_PLANO_NEW, /*131*/
           COD_EMPRESA_SAI_OLD, /*132*/
           COD_EMPRESA_SAI_NEW, /*133*/
           SIGLA_INMETRO_OLD, /*134*/
           SIGLA_INMETRO_NEW, /*135*/
           CENTRO_CUSTO_CARDEX_OLD, /*136*/
           CENTRO_CUSTO_CARDEX_NEW /*137*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.NUMERO_CAIXA, /*8*/
           0, /*9*/
           :old.TURNO, /*10*/
           0, /*11*/
           :old.CODIGO_DEPOSITO, /*12*/
           0, /*13*/
           :old.LOTE, /*14*/
           0, /*15*/
           :old.PRODCAI_NIVEL99, /*16*/
           '', /*17*/
           :old.PRODCAI_GRUPO, /*18*/
           '', /*19*/
           :old.PRODCAI_SUBGRUPO, /*20*/
           '', /*21*/
           :old.PRODCAI_ITEM, /*22*/
           '', /*23*/
           :old.DATA_PRODUCAO, /*24*/
           null, /*25*/
           :old.PESO_LIQUIDO, /*26*/
           0, /*27*/
           :old.PESO_BRUTO, /*28*/
           0, /*29*/
           :old.PESO_EMBALAGEM, /*30*/
           0, /*31*/
           :old.PRECO_MEDIO, /*32*/
           0, /*33*/
           :old.ENDERECO_CAIXA, /*34*/
           '', /*35*/
           :old.NUMERO_PEDIDO, /*36*/
           0, /*37*/
           :old.SEQUENCIA_PEDIDO, /*38*/
           0, /*39*/
           :old.NR_SOLICITACAO, /*40*/
           0, /*41*/
           :old.STATUS_CAIXA, /*42*/
           0, /*43*/
           :old.NOTA_FISCAL, /*44*/
           0, /*45*/
           :old.SERIE_NOTA, /*46*/
           '', /*47*/
           :old.TRANSACAO_ENT, /*48*/
           0, /*49*/
           :old.SEQ_NOTA_FISCAL, /*50*/
           0, /*51*/
           :old.DATA_ENTRADA, /*52*/
           null, /*53*/
           :old.CODIGO_EMBALAGEM, /*54*/
           0, /*55*/
           :old.PROFORMA, /*56*/
           '', /*57*/
           :old.VALOR_ENTRADA, /*58*/
           0, /*59*/
           :old.VALOR_DESPESAS, /*60*/
           0, /*61*/
           :old.QTDE_PECAS_EMB, /*62*/
           0, /*63*/
           :old.DATA_TRANSACAO, /*64*/
           null, /*65*/
           :old.TORCAO, /*66*/
           '', /*67*/
           :old.NOTA_FISC_ENT, /*68*/
           0, /*69*/
           :old.SERI_FISC_ENT, /*70*/
           '', /*71*/
           :old.SEQU_FISC_ENT, /*72*/
           0, /*73*/
           :old.NOME_PROG, /*74*/
           '', /*75*/
           :old.PERIODO_PRODUCAO, /*76*/
           0, /*77*/
           :old.QUALIDADE_FIO, /*78*/
           0, /*79*/
           :old.PRE_ROMANEIO, /*80*/
           0, /*81*/
           :old.GRUPO_MAQUINA, /*82*/
           '', /*83*/
           :old.SUB_MAQUINA, /*84*/
           '', /*85*/
           :old.NUMERO_MAQUINA, /*86*/
           0, /*87*/
           :old.CNPJ_FORNECEDOR9, /*88*/
           0, /*89*/
           :old.CNPJ_FORNECEDOR4, /*90*/
           0, /*91*/
           :old.CNPJ_FORNECEDOR2, /*92*/
           0, /*93*/
           :old.DATA_INSERCAO, /*94*/
           null, /*95*/
           :old.DATA_CARDEX, /*96*/
           null, /*97*/
           :old.TRANSACAO_CARDEX, /*98*/
           0, /*99*/
           :old.USUARIO_CARDEX, /*100*/
           '', /*101*/
           :old.VALOR_MOVTO_CARDEX, /*102*/
           0, /*103*/
           :old.VALOR_CONTABIL_CARDEX, /*104*/
           0, /*105*/
           :old.TABELA_ORIGEM_CARDEX, /*106*/
           '', /*107*/
           :old.ORDEM_AGRUPAMENTO, /*108*/
           0, /*109*/
           :old.CAIXA_ORIGINAL, /*110*/
           0, /*111*/
           :old.TURNO_ORIGINAL, /*112*/
           0, /*113*/
           :old.CODIGO_PESADOR, /*114*/
           0, /*115*/
           :old.DIVISAO_PRODUCAO, /*116*/
           0, /*117*/
           :old.ORDEM_PRODUCAO, /*118*/
           0, /*119*/
           :old.PESO_ADICIONAL, /*120*/
           0, /*121*/
           :old.ARREADA, /*122*/
           0, /*123*/
           :old.RESTRICAO, /*124*/
           '', /*125*/
           :old.DEPOSITO_PRODUCAO, /*126*/
           0, /*127*/
           :old.TRANSACAO_PRODUCAO, /*128*/
           0, /*129*/
           :old.SEQUENCIA_PLANO, /*130*/
           0, /*131*/
           :old.COD_EMPRESA_SAI, /*132*/
           0, /*133*/
           :old.SIGLA_INMETRO, /*134*/
           '', /*135*/
           :old.CENTRO_CUSTO_CARDEX, /*136*/
           0 /*137*/
         );
    end;
 end if;
end inter_tr_ESTQ_060_log;

-- ALTER TRIGGER "INTER_TR_ESTQ_060_LOG" ENABLE
 

/

exec inter_pr_recompile;

