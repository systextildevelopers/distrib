alter table fatu_060
add (nr_cupom number(6), nr_caixa number(3), num_serie_ecf varchar2(30));

alter table fatu_060
modify (nr_cupom default 0, nr_caixa default 0, num_serie_ecf default ' ');

exec inter_pr_recompile;
/
