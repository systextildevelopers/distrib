create table estq_191(
    deposito       number(5) not null,
    codigo_empresa number(5) not null,
    agrupador      number(5) not null
);

alter table estq_191 add constraint  pk_estq_191 primary key (deposito);

alter table estq_191 add constraint fk_estq_191_estq_190 foreign key (agrupador, codigo_empresa) references estq_190 (agrupador, codigo_empresa);
