begin
    EXECUTE IMMEDIATE 'alter table basi_003            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_004_sped       modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_013            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_030            modify (estagio_altera_programado      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_030_log        modify (estagio_altera_programado_new  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_030_log        modify (estagio_altera_programado_old  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_050            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_113            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_120            modify (estagio_critico_fio            number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_120            modify (ultimo_estagio_fio             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_137            modify (comp_estagio                   number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_170            modify (estagio_critico                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_351            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_572            modify (cod_estagio_padrao             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table basi_846            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_210          modify (cod_item_est_agrup_insu        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_210          modify (cod_item_est_simult_insu       number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_215          modify (cod_item_est_agrup_insu        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_215          modify (cod_item_est_simult_insu       number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_220          modify (cod_item_est_agrup_insu_dest   number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_220          modify (cod_item_est_agrup_insu_ori    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_220          modify (cod_item_est_simult_insu_dest  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_220          modify (cod_item_est_simult_insu_ori   number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_230          modify (cod_item_est_agrup             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_230          modify (estagio_agrupador              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_235          modify (cod_item_est_agrup_insu        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_235          modify (cod_item_est_simult_insu       number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_235          modify (estagio_agrupador              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_235          modify (estagio_agrupador_ant          number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_250          modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_250          modify (cod_item_est_agrup             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_255          modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_255          modify (cod_item_est_agrup_insu        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_255          modify (cod_item_est_simult_insu       number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_290          modify (estagio_agrupador              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_291          modify (cod_item_est_agrup             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_291          modify (cod_item_est_agrup_simult      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_291          modify (estagio_agrupador              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_292          modify (cod_item_est_agrup_insu        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_292          modify (cod_item_est_simult_insu       number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table blocok_292          modify (estagio_agrupador              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table cent_010            modify (estagio_altera_programado      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table cent_018            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table cent_019            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table cent_023            modify (estagio_altera_programado      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table cent_028            modify (estagio_aviamento              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table cent_050            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table cost_340            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table cost_340            modify (estagio_depende                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table dist_002            modify (estagio1                       number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table dist_002            modify (estagio2                       number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table dist_004            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table dist_005            modify (estagio_distribuicao           number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table dist_005            modify (estagio_servico                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table dist_050            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table e_basi_030          modify (estagio_altera_programado      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_040            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_100            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_100            modify (estagio_digitacao              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_100_info       modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_100_info       modify (estagio_digitacao              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_101            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_101            modify (estagio_digitacao              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_115            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_140            modify (estagio_digitacao              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_160            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_305            modify (cod_estagio                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_560            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_815            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table efic_815            modify (estagio_digitacao              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_001            modify (estagio_acabam                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_001            modify (estagio_confec                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_001            modify (estagio_corte                  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_001            modify (estagio_estamp                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_001            modify (estagio_filato                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_001            modify (estagio_libera                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_001            modify (estagio_prepara                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_001            modify (estagio_tecelagem              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_001            modify (estagio_tintur                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_001            modify (ult_estagio_fiacao             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_002            modify (estagio_baixa                  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_002            modify (estagio_laser                  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_002            modify (estagio_prepara_estamparia     number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_002            modify (estagio_prepara_fios           number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_002            modify (estagio_qualidade              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table empr_002            modify (estagio_tecel_urdume           number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_036            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_036            modify (estagio_ant                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_039            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_039            modify (estagio_ant                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_220            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_300            modify (cod_estagio_agrupador          number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_300            modify (estagio_op                     number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_310            modify (cod_estagio_agrupador          number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_310            modify (estagio_op                     number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_450            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_451            modify (estagio_producao               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_800            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_801            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_900            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table estq_901            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table etiq_137            modify (comp_estagio                   number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table exec_015            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table exp_basi_030        modify (estagio_altera_programado      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table exp_basi_120        modify (estagio_critico_fio            number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table exp_basi_120        modify (ultimo_estagio_fio             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table e450_aux            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table e450_aux            modify (estagio_ant                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_060            modify (cod_estagio_agrupador_insu     number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_060            modify (cod_estagio_agrupador_prod     number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_060            modify (cod_estagio_simultaneo_insu    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_062            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_501            modify (estagio_alter_destino          number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_501            modify (estagio_elab_4                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_502            modify (estagio_acabamento_fio         number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_502            modify (estagio_liberacao              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_502            modify (estagio_repasse                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_502            modify (est_req_peso_os                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_503            modify (cod_estagio_conf_rolos         number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_503            modify (estagio_ctrl_avance_parte_peca number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_503            modify (estagio_preparacao_ordem       number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_503            modify (opcao_estagio_painel_corte     number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_504            modify (estagio_devolucao_pecas        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_504            modify (estagio_estampa_digital        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_504            modify (estagio_estampa_rotativa       number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_504            modify (estagio_estampa_transfer       number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_504            modify (estagio_faturamento            number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_504            modify (estagio_preparacao_pasta       number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_735            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_737            modify (cod_estagio_agrupador          number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table fatu_737            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table ftec_001            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table ftec_005            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table ftec_005            modify (estagio_anterior               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table ftec_005            modify (estagio_depende                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table ftec_010            modify (estagio_altera_programado      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table ftec_010            modify (estagio_critico_fio_linha_prod number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table ftec_010            modify (ultimo_estagio_fio_linha_prod  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table hdoc_030            modify (estagio_dig_rejeicao           number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table hist_010            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table i_basi_030          modify (estagio_altera_programado      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table i_fatu_050          modify (cod_estagio_imp                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table inte_preactor       modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table inte_910            modify (ultimo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table inte_940            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table in68_001            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table iplm_021            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_001            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/
/*
begin
    EXECUTE IMMEDIATE 'alter table mqop_005            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_005            modify (estagio_base_fila              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_005            modify (estagio_final                  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_005            modify (est_agrup_est                  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_005_log        modify (cod_estagio_agrupador_new      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_005_log        modify (cod_estagio_agrupador_old      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_005_log        modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/
*/
begin
    EXECUTE IMMEDIATE 'alter table mqop_006            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_007            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_008            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_010            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_030            modify (estagio_principal              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_050            modify (cod_estagio_agrupador          number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_050            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_050            modify (estagio_anterior               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_050            modify (estagio_depende                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_057            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_190            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_260            modify (estagio_agrupador_servico      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_260            modify (estagio_producao               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_750            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table mqop_755            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_015            modify (cod_estagio_agrupador_insu     number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_015            modify (cod_estagio_agrupador_prod     number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_015            modify (cod_estagio_simultaneo_insu    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_070            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_070            modify (estagio_baixa                  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_070            modify (estagio_bloqueio_os            number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_070            modify (estagio_matprima               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_070            modify (estagio_recebimento            number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_082            modify (cod_estagio_agrupador_insu     number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_082            modify (cod_estagio_simultaneo_insu    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_083            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_089            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_089_log        modify (estagio_new                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_089_log        modify (estagio_old                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_096            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_300            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_430            modify (estagio_1                      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_430            modify (estagio_2                      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_430            modify (estagio_3                      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_430            modify (estagio_4                      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_430            modify (estagio_5                      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_779            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_787            modify (codigo_estagio040              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_794            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table obrf_797            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_005            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_010            modify (cod_estagio_agrupador          number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_010            modify (ultimo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_010_aux        modify (ultimo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_010_log        modify (ultimo_estagio_new             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_010_log        modify (ultimo_estagio_old             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_012            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_015            modify (cod_estagio_agrupador          number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_015            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_040            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_040_hist       modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_040_hist       modify (estagio_old                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_0402           modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_050            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_075            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_075_log        modify (codigo_estagio_new             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_075_log        modify (codigo_estagio_old             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_081            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_100            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_161            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_201            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_240            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_245            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_250            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpb_590            modify (cod_estagio                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_003            modify (cod_estagio                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_005            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_010            modify (estagio_critico                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_020            modify (estagio_leitura_bath           number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_020            modify (ultimo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_040            modify (cod_estagio_agrupador          number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_040            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_040            modify (estagio_agrupador_servico      number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_040            modify (estagio_anterior               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_040            modify (estagio_depende                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_040_hist       modify (codigo_estagio_new             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_040_hist       modify (codigo_estagio_old             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_040_hist       modify (estagio_anterior_new           number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_040_hist       modify (estagio_anterior_old           number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_0402           modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_0402           modify (estagio_anterior               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_045            modify (pcpc040_estconf                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_046            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_046            modify (estagio_anterior               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_046            modify (estagio_depende                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_055            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_070            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_090            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_090_log        modify (codigo_estagio_new             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_090_log        modify (codigo_estagio_old             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_100            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_146            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_168            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_190            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_260            modify (cod_estagio                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_261            modify (cod_estagio                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_330            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_330_backup     modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_330_log        modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_330_log        modify (estagio_old                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_331            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_341            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_360            modify (cod_estagio                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_475            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_880            modify (estagio_conserto               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_888            modify (cod_est_cons                   number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpc_888            modify (cod_est_ident_cons             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpf_050            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpf_060            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpf_070            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpf_300            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpf_300            modify (estagio_anterior               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpf_310            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpf_310            modify (estagio_anterior               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpt_021            modify (estagio_op                     number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpt_025            modify (estagio_producao               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpt_025_hist       modify (estagio_producao_new           number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpt_025_hist       modify (estagio_producao_old           number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpt_026            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpt_026_log        modify (estagio_new                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table pcpt_026_log        modify (estagio_old                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table prog_050            modify (cod_estagio_agrupador          number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table prog_050            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table prog_050            modify (estagio_anterior               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table prog_050            modify (estagio_depende                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table prog_057            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table proj_015            modify (cod_estagio                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table proj_017            modify (cod_estagio                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_021            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_125            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_150            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_155            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_160            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_500            modify (estagio_elab                   number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_851            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_852            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_853            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_859            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_859            modify (estagio_agrupador              number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_859            modify (estagio_agrupador_simul        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table rcnb_859            modify (estagio_depende                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table supr_511            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table supr_520            modify (cod_estagio                    number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table supr_525            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table supr_526            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_005            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_015            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_041            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_240            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_240            modify (estagio_anterior               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_240            modify (ultimo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_310            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_315            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_330            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_330            modify (estagio_posicao                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_330            modify (est_agrup_est                  number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_330_aux        modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_330_aux        modify (estagio_posicao                number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_370            modify (estagio_componente             number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_380            modify (estagio                        number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_381            modify (estagio_producao               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_621            modify (estagio_producao               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_625            modify (estagio_producao               number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_650            modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others 	then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_650_aux        modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/

begin
    EXECUTE IMMEDIATE 'alter table tmrp_650_hist       modify (codigo_estagio                 number(5,0) DEFAULT 0)';
exception
    when others then
        if sqlcode <> -942 then
            raise;
        end if;
end;
/
