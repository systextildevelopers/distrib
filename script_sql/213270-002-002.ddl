ALTER TABLE OBRF_010 ADD (num_fatura_cte number(9));
                                
EXEC INTER_PR_RECOMPILE;

ALTER TABLE obrf_010 MODIFY (num_fatura_cte number(9) DEFAULT 0);     
    
EXEC INTER_PR_RECOMPILE;
/
