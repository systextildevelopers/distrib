create or replace view inter_vi_compras_anuais as
select obrf_010.documento NF,
       obrf_010.serie SERIE,
       obrf_010.data_transacao DATA_TRANSACAO,
       trim(to_char(supr_010.fornecedor9,'000000000'))||'/'||trim(to_char(supr_010.fornecedor4,'0000'))||'-'||trim(to_char(supr_010.fornecedor2, '00'))||' '||supr_010.nome_fornecedor FORNECEDOR,
       basi_160.estado UF,
       decode(supr_010.considera_ttd,0,'0-Nao',1,'1-Sim',supr_010.considera_ttd) TTD,
       obrf_015.classific_fiscal NCM,
       decode(basi_240.considera_ttd,0,'0-Nao',1,'1-Sim',basi_240.considera_ttd) TTD_NCM,
       obrf_015.coditem_nivel99||'.'||obrf_015.coditem_grupo||'.'||obrf_015.coditem_subgrupo||'.'||obrf_015.coditem_item||' - '||basi_010.narrativa PRODUTO,
       obrf_015.procedencia CST,
       round(obrf_015.quantidade*obrf_015.valor_unitario,3) VALOR,
       extract(month from obrf_010.data_emissao) MES
from obrf_010, obrf_015, supr_010, basi_010, basi_160, basi_030, basi_150, pedi_080, basi_240
where obrf_010.documento         = obrf_015.capa_ent_nrdoc
  and obrf_010.serie             = obrf_015.capa_ent_serie
  and obrf_010.cgc_cli_for_9     = obrf_015.capa_ent_forcli9
  and obrf_010.cgc_cli_for_4     = obrf_015.capa_ent_forcli4
  and obrf_010.cgc_cli_for_2     = obrf_015.capa_ent_forcli2
  and obrf_015.natitem_nat_oper  = pedi_080.natur_operacao
  and obrf_015.natitem_est_oper  = pedi_080.estado_natoper
  and pedi_080.nat_ativa         = 0
  and pedi_080.tipo_natureza     not in (2)
  and obrf_015.coditem_nivel99   = basi_010.nivel_estrutura
  and obrf_015.coditem_grupo     = basi_010.grupo_estrutura
  and obrf_015.coditem_subgrupo  = basi_010.subgru_estrutura
  and obrf_015.coditem_item      = basi_010.item_estrutura
  and obrf_015.coditem_nivel99   = basi_030.nivel_estrutura
  and obrf_015.coditem_grupo     = basi_030.referencia
  and basi_030.conta_estoque     = basi_150.conta_estoque
  and basi_150.tipo_produto_sped = 1
  and obrf_010.cgc_cli_for_9     = supr_010.fornecedor9
  and obrf_010.cgc_cli_for_4     = supr_010.fornecedor4
  and obrf_010.cgc_cli_for_2     = supr_010.fornecedor2
  and extract (year from obrf_010.data_emissao) = extract (year from SYSDATE)
  and supr_010.tipo_fornecedor   = 11
  and supr_010.cod_cidade        = basi_160.cod_cidade
  and obrf_010.cod_canc_nfisc    = 0
  and obrf_015.classific_fiscal  = basi_240.CLASSIFIC_FISCAL
order by obrf_010.data_transacao;
