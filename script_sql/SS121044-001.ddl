ALTER TABLE FATU_500
ADD (controle_cotas_amostragem number(1) default 0);

/

update fatu_500 set
fatu_500.controle_cotas_amostragem = 0
where fatu_500.analisa_cotas = 0;

/

update fatu_500 set
fatu_500.controle_cotas_amostragem = 1
where fatu_500.analisa_cotas > 0;

commit;

/

exec inter_pr_recompile;
