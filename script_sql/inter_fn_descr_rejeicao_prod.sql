
  CREATE OR REPLACE FUNCTION "INTER_FN_DESCR_REJEICAO_PROD" 
        (fniv_fioprod   varchar2,
         fgru_fioprod   varchar2,
         fsub_fioprod   varchar2,
         fite_fioprod   varchar2,
         fcodigo_motivo number)
RETURN string
IS
   descr_rejeicao efic_040.descricao%type;
   serie_prod     number;
   contador       number;
begin

   begin
      select basi_100.serie_cor
      into serie_prod
      from basi_100
      where basi_100.cor_sortimento = fite_fioprod
        and basi_100.tipo_cor       = 1;
      exception
      when NO_DATA_FOUND
      then
         serie_prod := 0;
   end;

   begin
      select efic_040.descricao
      into descr_rejeicao
      from efic_040
      where  efic_040.codigo_motivo  = fcodigo_motivo
        and  efic_040.nivel          = fniv_fioprod
        and  efic_040.grupo          = fgru_fioprod
        and  efic_040.subgrupo       = fsub_fioprod
        and  efic_040.item           = fite_fioprod
        and (efic_040.serie_cor      = serie_prod or efic_040.serie_cor = 0);
      exception
      when NO_DATA_FOUND
      then begin
         select efic_040.descricao
         into descr_rejeicao
         from efic_040
         where  efic_040.codigo_motivo  = fcodigo_motivo
           and  efic_040.nivel          = fniv_fioprod
           and  efic_040.grupo          = fgru_fioprod
           and  efic_040.subgrupo       = fsub_fioprod
           and  efic_040.item           = '000000'
           and (efic_040.serie_cor      = serie_prod or efic_040.serie_cor = 0);
         exception
         when NO_DATA_FOUND
         then begin
            select efic_040.descricao
            into descr_rejeicao
            from efic_040
            where  efic_040.codigo_motivo  = fcodigo_motivo
              and  efic_040.nivel          = fniv_fioprod
              and  efic_040.grupo          = fgru_fioprod
              and  efic_040.subgrupo       = '000'
              and  efic_040.item           = '000000'
              and (efic_040.serie_cor      = serie_prod or efic_040.serie_cor = 0);
            exception
            when NO_DATA_FOUND
            then begin
               select efic_040.descricao
               into descr_rejeicao
               from efic_040
               where  efic_040.codigo_motivo  = fcodigo_motivo
                 and  efic_040.nivel          = fniv_fioprod
                 and  efic_040.grupo          = '00000'
                 and  efic_040.subgrupo       = '000'
                 and  efic_040.item           = '000000'
                 and (efic_040.serie_cor      = serie_prod or efic_040.serie_cor = 0);
               exception
               when NO_DATA_FOUND
               then begin
                  select efic_040.descricao
                  into descr_rejeicao
                  from efic_040
                  where  efic_040.codigo_motivo  = fcodigo_motivo
                    and  efic_040.nivel          = '0'
                    and  efic_040.grupo          = '00000'
                    and  efic_040.subgrupo       = '000'
                    and  efic_040.item           = '000000'
                    and (efic_040.serie_cor      = serie_prod or efic_040.serie_cor = 0);
                  exception
                  when NO_DATA_FOUND
                  then
                     descr_rejeicao:= ' ';
               end;
            end;
         end;
      end;
   end;

   return(descr_rejeicao);
end inter_fn_descr_rejeicao_prod;



 

/

exec inter_pr_recompile;

