alter table sped_c170 add(
val_fcp_uf_dest number(17,2),
val_icms_uf_dest   number(17,2),
val_icms_uf_remet  number(17,2));


comment on column sped_c170.val_fcp_uf_dest is 'Valor do ICMS relativo ao Fundo de Combate � Pobreza (FCP) da UF de destino.';
comment on column sped_c170.val_icms_uf_dest is 'Valor do ICMS de partilha para a UF do destinat�rio';
comment on column sped_c170.val_icms_uf_remet is 'Valor do ICMS de partilha para a UF do remetente.';

exec inter_pr_recompile;
