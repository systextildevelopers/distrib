CREATE OR REPLACE TRIGGER inter_tr_expt_040_atualiza
BEFORE INSERT OR UPDATE ON EXPT_040
FOR EACH ROW
DECLARE 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(20) ; 
   ws_locale_usuario         varchar2(5) ;

BEGIN

    :NEW.ATUALIZADO_EM := SYSDATE;
	
    inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                            ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 

    :NEW.ATUALIZADO_POR := ws_usuario_systextil;
	
END inter_tr_expt_040_atualiza;
/
