insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('estq_f307', 'Consulta de Saldos de Lote Fornecedor por Depósito',0,1);
insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'estq_f307', 'menu_ge30' ,1, 0, 'S', 'S', 'S', 'S');
update hdoc_036
set hdoc_036.descricao       = 'Consulta de Saldos de Lote Proveedor por Depósito'
where hdoc_036.codigo_programa = 'estq_f307'
  and hdoc_036.locale          = 'es_ES';
