alter table imp_ordens_marcacoes add (
eficiencia number(5,2),
area       number(6,1),
perimetro  number(7,2)
);

alter table pcpc_030 add (
eficiencia_opt number(5,2),
area_opt       number(6,1),
perimetro_opt  number(7,2)
);
