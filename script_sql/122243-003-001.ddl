insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('inte_f110', 'Integração de Saldos de Estoques com Totall', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'inte_f110', 'inte_menu', 1, 1, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'inte_f110', 'inte_menu', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Integração de Saldos de Estoques com Totall'
 where hdoc_036.codigo_programa = 'inte_f110'
   and hdoc_036.locale          = 'es_ES';
   
update hdoc_036
   set hdoc_036.descricao       = 'Integração de Saldos de Estoques com Totall'
 where hdoc_036.codigo_programa = 'inte_f110'
   and hdoc_036.locale          = 'pt_BR';
   
commit;
