insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('estq_f376', 'Par�metros de An�lise Diverg�ncia entre Estoque X Volume X Cardex', 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'estq_f376', ' ' , 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Par�metros de An�lisis Divergencia entre Stock X Volumen X Cardex'
where hdoc_036.codigo_programa = 'estq_f376'
  and hdoc_036.locale          = 'es_ES';
  
commit work;
