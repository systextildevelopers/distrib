insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f429', 'Itens a Enviar da Ordem de Servico - Por Pacote', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f429', ' ', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'obrf_f429', ' ', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Itens a Enviar da Ordem de Servico - Por Pacote'
 where hdoc_036.codigo_programa = 'obrf_f429'
   and hdoc_036.locale          = 'es_ES';
commit;
