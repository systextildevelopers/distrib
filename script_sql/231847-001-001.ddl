alter table pedi_156 drop column pct_menor_comp_rolo;
alter table pedi_156 drop column pct_maior_comp_rolo;
alter table pedi_156 drop column pct_menor_tot_lote;
alter table pedi_156 drop column pct_maior_tot_lote;

alter table pedi_156 add peso_max_rolo number(9,3) default 0;
