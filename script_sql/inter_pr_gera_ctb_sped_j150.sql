
  CREATE OR REPLACE PROCEDURE "INTER_PR_GERA_CTB_SPED_J150" (p_cod_empresa   IN NUMBER,
                                                        p_exercicio     IN NUMBER,
                                                        p_cod_plano_cta IN NUMBER,
                                                        p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar a tabela sped_cta_j150 - Demostracao do Resultado do Exercicio
   --
   -- Historicos
   --
   -- Data    Autor    Observacoes
   --

  -- Calcula totais para as contas do DRE Analiticas

   cursor cont021_analit(p_cod_empresa NUMBER, p_exercicio NUMBER, p_cod_plano_cta NUMBER) IS
      select cont_021.conta_dre,cont_021.descricao_conta, sum(decode(cont_600.debito_credito,'D',cont_600.valor_lancto,cont_600.valor_lancto * -1)) as saldo_linha,
             decode(sign(sum(decode(cont_600.debito_credito,'D',cont_600.valor_lancto,cont_600.valor_lancto * -1))),1,'R','D') as indicador,
             cont_021.nivel_conta, cont_021.ordem_impressao
      from cont_021, cont_022, cont_600
      where cont_021.cod_plano_cta   = p_cod_plano_cta
        and cont_021.tipo_conta      = 1
        and cont_022.cod_plano_cta   = cont_021.cod_plano_cta
        and cont_022.conta_dre       = cont_021.conta_dre
        and cont_600.cod_empresa     = p_cod_empresa
        and cont_600.exercicio       = p_exercicio
        and cont_600.conta_reduzida  = cont_022.conta_analit_ctb
        and cont_600.origem          = 20
      group by cont_021.conta_dre,cont_021.descricao_conta,cont_021.nivel_conta, cont_021.ordem_impressao;


   cursor cont021_analit_saldo(p_cod_empresa NUMBER, p_exercicio NUMBER, p_cod_plano_cta NUMBER, p_cod_conta_dre VARCHAR2) IS
      select cont_021.cod_plano_cta,
       cont_021.conta_dre,
       cont_021.descricao_conta,
       cont_021.nivel_conta,
       cont_021.tipo_conta,
       cont_021.ordem_impressao,
       cont_022.conta_analit_dre
      from cont_021, cont_022
      where cont_021.cod_plano_cta = p_cod_plano_cta
        and cont_022.cod_plano_cta = cont_021.cod_plano_cta
        and cont_022.conta_dre     = cont_021.conta_dre
        and cont_021.tipo_conta    = 2
        and cont_021.conta_dre = p_cod_conta_dre
      group by cont_021.cod_plano_cta,
             cont_021.conta_dre,
             cont_021.descricao_conta,
             cont_021.nivel_conta,
             cont_021.tipo_conta,
             cont_021.ordem_impressao,
             cont_022.conta_analit_dre
      order by cont_021.nivel_conta desc , cont_021.ordem_impressao;

  cursor gamb_oracle8(p_cod_empresa NUMBER, p_exercicio NUMBER, p_cod_plano_cta NUMBER, p_conta_analit_dre VARCHAR2) IS
     select sum(decode(cont_600.debito_credito,'D',cont_600.valor_lancto,cont_600.valor_lancto * -1)) saldo_linha
     from cont_021 c1, cont_022 c2, cont_600
     where c1.cod_plano_cta   = p_cod_plano_cta
       and c1.tipo_conta      = 1
       and c2.cod_plano_cta   = c1.cod_plano_cta
       and c2.conta_dre       = c1.conta_dre
       and cont_600.cod_empresa     = p_cod_empresa
       and cont_600.exercicio       = p_exercicio
       and cont_600.conta_reduzida  = c2.conta_analit_ctb
       and cont_600.origem          = 20
       and c2.conta_dre = p_conta_analit_dre;

  -- Calcula totais para as contas do DRE Sinteticas
   cursor cont021_sintetica(p_cod_empresa NUMBER, p_exercicio NUMBER, p_cod_plano_cta NUMBER) IS
      select cont_021.cod_plano_cta,
             cont_021.conta_dre,
             cont_021.descricao_conta,
             cont_021.nivel_conta,
             cont_021.tipo_conta,
             cont_021.ordem_impressao
      from cont_021, cont_022
      where cont_021.cod_plano_cta = p_cod_plano_cta
        and cont_022.cod_plano_cta = cont_021.cod_plano_cta
        and cont_022.conta_dre     = cont_021.conta_dre
        and cont_021.tipo_conta    = 2
      group by cont_021.cod_plano_cta,
             cont_021.conta_dre,
             cont_021.descricao_conta,
             cont_021.nivel_conta,
             cont_021.tipo_conta,
             cont_021.ordem_impressao
      order by cont_021.nivel_conta desc , cont_021.ordem_impressao;
--

   v_erro                EXCEPTION;
   v_ind_nat             varchar2(1);
   v_ind_nat_agrutinacao varchar2(1);
   v_valor_saldo         number(15,2);
   v_valor_saldo_p       number(15,2);
   v_saldo               number(15,2);



begin
   p_des_erro       := NULL;

   for reg_cont021_analit in cont021_analit(p_cod_empresa, p_exercicio, p_cod_plano_cta)
   loop


      if reg_cont021_analit.saldo_linha < 0.00
      then
         v_valor_saldo := reg_cont021_analit.saldo_linha * (-1.0);
      else
         v_valor_saldo := reg_cont021_analit.saldo_linha;
      end if;

      begin

         -- contas analiticas

         insert into sped_ctb_j100
           (cod_empresa,                      exercicio,
            tipo_demonstracao,

            cod_aglutinacao,                  nivel_aglutinacao,
            descricao_aglutinacao,            saldo_final,
            ind_nat_agrutinacao,              sequencia_dre,
            nivel_dre)
         values (
            p_cod_empresa,                     p_exercicio,
            2,
            reg_cont021_analit.conta_dre,      reg_cont021_analit.nivel_conta,
            reg_cont021_analit.descricao_conta,      v_valor_saldo,
            reg_cont021_analit.indicador,      reg_cont021_analit.ordem_impressao,
            reg_cont021_analit.nivel_conta);

      exception
         when others then
            p_des_erro := 'Erro na inclusao da tabela sped_ctb_j100 ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;
   end loop;

   for reg_cont021_sintetica in cont021_sintetica(p_cod_empresa, p_exercicio, p_cod_plano_cta)
   loop

      v_valor_saldo := 0;

      for reg_cont021_analit_saldo in cont021_analit_saldo(p_cod_empresa, p_exercicio, p_cod_plano_cta,reg_cont021_sintetica.conta_dre)
      loop

        v_saldo := 0.00;

        for reg_gamb_oracle8 in gamb_oracle8(p_cod_empresa, p_exercicio, p_cod_plano_cta,reg_cont021_analit_saldo.conta_analit_dre)
        loop
          v_saldo := v_saldo + reg_gamb_oracle8.saldo_linha;
        end loop;

        if v_saldo is null or v_saldo = 0
        then
          begin
            select decode(sped_ctb_j100.ind_nat_agrutinacao,'N',sped_ctb_j100.saldo_final * (-1),sped_ctb_j100.saldo_final), sped_ctb_j100.ind_nat_agrutinacao
              into v_valor_saldo_p,           v_ind_nat_agrutinacao
            from sped_ctb_j100
            where sped_ctb_j100.cod_empresa     = p_cod_empresa
              and sped_ctb_j100.exercicio       = p_exercicio
              and sped_ctb_j100.cod_aglutinacao = reg_cont021_analit_saldo.conta_analit_dre
              and sped_ctb_j100.tipo_demonstracao = 2;
          exception
            when no_data_found then
              v_valor_saldo_p := 0;
          end;
        else
          v_valor_saldo_p := v_saldo;
        end if;

        v_valor_saldo := v_valor_saldo + v_valor_saldo_p;

      end loop;

      if v_valor_saldo < 0.00 or v_ind_nat_agrutinacao = 'D'
      then
         v_valor_saldo := v_valor_saldo * (-1.0);
         v_ind_nat       := 'N';
      else
         v_valor_saldo := v_valor_saldo;
         v_ind_nat       := 'P';
      end if;

      begin


      insert into sped_ctb_j100
        (cod_empresa,                           exercicio,
         tipo_demonstracao,
         cod_aglutinacao,                       nivel_aglutinacao,
         descricao_aglutinacao,                 saldo_final,
         ind_nat_agrutinacao,                   sequencia_dre,
         nivel_dre)
      values (
         p_cod_empresa,                          p_exercicio,
         2,
         reg_cont021_sintetica.conta_dre,        reg_cont021_sintetica.nivel_conta,
         reg_cont021_sintetica.descricao_conta,  v_valor_saldo,
         v_ind_nat,                                reg_cont021_sintetica.ordem_impressao,
         reg_cont021_sintetica.nivel_conta);
      exception
        when others then
            p_des_erro := 'Erro na inclusao da tabela sped_ctb_j100 ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;

      commit;

   end loop;

   commit;

exception
   when V_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_gera_ctb_sped_j150 ' || Chr(10) || p_des_erro;

   when others then
      p_des_erro := 'Outros erros na procedure inter_pr_gera_ctb_sped_j150 ' || Chr(10) || SQLERRM;

end inter_pr_gera_ctb_sped_j150;
 

/

exec inter_pr_recompile;

