ALTER TABLE OBRF_010
	ADD (
	   TIPO_CTE NUMBER(1)
	);
	
ALTER TABLE OBRF_010
	MODIFY TIPO_CTE DEFAULT 0;
/

declare
  cont_reg number;
begin
  cont_reg := 0;
  
  for nf in (
    select rowid from obrf_010
    where obrf_010.tipo_cte is null
  )
  loop
     update obrf_010
     set obrf_010.tipo_cte = 0
     where rowid = nf.rowid;
     
     cont_reg := cont_reg + 1;
     
     if cont_reg = 1000
     then
        commit;
        cont_reg := 0;
     end if;
  end loop;
  commit;
end;

/

exec inter_pr_recompile;

/
