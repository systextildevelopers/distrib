insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('supr_f012', 'Detalhes do Imposto de Fornecedor', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'supr_f012', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'supr_f012', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Detalles de impuestos del proveedor'
 where hdoc_036.codigo_programa = 'supr_f012'
   and hdoc_036.locale          = 'es_ES';
commit;
