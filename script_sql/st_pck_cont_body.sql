create or replace PACKAGE BODY ST_PCK_CONT AS

    function get_exercicio(p_codigo_empresa number, p_data_lancamento date) return t_exercicio
    AS 
        v_exercicio t_exercicio;
    BEGIN
        BEGIN
            SELECT 
                exercicio,
                situacao
            INTO
                v_exercicio.exercicio,
                v_exercicio.situacao
            FROM cont_500 
            WHERE cod_empresa = p_codigo_empresa
            AND p_data_lancamento BETWEEN per_inicial AND per_final;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_exercicio.exercicio := null;
                    v_exercicio.situacao := null;
        END;
        RETURN v_exercicio;
    END get_exercicio;
    
    function get_next_numero_lancamento(p_codigo_empresa number, p_exercicio number) return number 
    AS
        v_numero_lanc number;
    BEGIN
    
        UPDATE cont_500 
        SET ultimo_lanc=ultimo_lanc+1 
        WHERE cod_empresa = p_codigo_empresa
        AND exercicio = p_exercicio
        RETURNING ultimo_lanc
        INTO v_numero_lanc;

        RETURN v_numero_lanc;
        EXCEPTION
            WHEN OTHERS THEN
                return 0;     
    END get_next_numero_lancamento;
    
    function exists_transacao(p_codigo_transacao number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM estq_005 
        WHERE codigo_transacao = p_codigo_transacao;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_transacao;
    
    function get_transacao(p_documento number, p_serie number, p_cgc_9 number, p_cgc_4 number, p_cgc_2 number) return number 
    AS
        p_codigo_transacao number;
    BEGIN
        BEGIN
            SELECT
                codigo_transacao
            INTO
                p_codigo_transacao
            FROM obrf_010
            WHERE documento = p_documento
            AND serie = p_serie
            AND cgc_cli_for_9 = p_cgc_9
            AND cgc_cli_for_4 = p_cgc_4
            AND cgc_cli_for_2 = p_cgc_2; 
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    p_codigo_transacao := null;
        END;    
        RETURN p_codigo_transacao;
    END get_transacao;
  
    function get_plano_conta(p_codigo_empresa number, p_exercicio number) return number 
    AS
        v_cod_plano_cta number;
    BEGIN
        BEGIN
            SELECT
                cod_plano_cta
            INTO
                v_cod_plano_cta
            FROM cont_500
            WHERE cod_empresa = p_codigo_empresa
            AND exercicio = p_exercicio; 
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_cod_plano_cta := null;
        END;    
        RETURN v_cod_plano_cta;
    END get_plano_conta;
    
    function get_conta_contabil(p_codigo_empresa number, p_exercicio number) return number 
    AS
        
    BEGIN
        RETURN NULL;
    END get_conta_contabil;
    
    function get_conta_reduzida(p_codigo_empresa number, p_exercicio number) return number 
    AS
        
    BEGIN
        RETURN NULL;
    END get_conta_reduzida;
  
    function get_historico_by_cod(p_codigo_historico number) return cont_010.historico_contab%type
    AS
        v_historico cont_010.historico_contab%type;
    BEGIN
    SELECT
        historico_contab
    INTO
        v_historico
    FROM cont_010 
    WHERE codigo_historico = p_codigo_historico;
    
    return v_historico;
    
    EXCEPTION 
        WHEN others THEN
            return null;
    END get_historico_by_cod;
  
    function exists_historico_by_cod(p_codigo_historico number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM cont_010 
        WHERE codigo_historico = p_codigo_historico;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_historico_by_cod;
  
    function get_periodo_contabil(p_codigo_empresa number, p_exercicio number, p_data_lancamento date) return t_periodo
    AS
        v_periodo t_periodo;
    BEGIN
        BEGIN
            SELECT
                periodo,
                per_inicial,
                per_final
            INTO
                v_periodo.periodo,
                v_periodo.per_inicial,
                v_periodo.per_final
            FROM cont_510 
            WHERE cod_empresa = p_codigo_empresa 
            AND exercicio = p_exercicio 
            AND p_data_lancamento BETWEEN per_inicial AND per_final;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_periodo.periodo := null;
                    v_periodo.per_inicial := null;
                    v_periodo.per_final := null;
        END;
        RETURN v_periodo;
    END get_periodo_contabil;
  
  function exists_tipo_contabil(p_codigo_contabil number) return boolean
  AS
    v_exists number;
  BEGIN
    SELECT
        1
    INTO
        v_exists
    FROM cont_540 
    WHERE codigo_contabil = p_codigo_contabil
    AND tipo_contabil = 4;
    
    IF v_exists = 1 THEN
        return true;
    ELSE
        return false;
    END IF;
    EXCEPTION 
        WHEN others THEN
            return false;
  END exists_tipo_contabil;
  
  function acha_lote(p_empresa_lt number, p_exercicio_lt number, p_origem_lt number, p_lote_lt number, p_data_lt date, p_tipo_lancto_lt number) return number AS
  BEGIN
    -- TODO: implementação exigida para function ST_PCK_CONT.acha_lote
    RETURN NULL;
  END acha_lote;

  function checa_data(p_cod_empresa number, p_data_lanc date, p_transacao number, p_programa_gerador varchar2) return number AS
  BEGIN
    -- TODO: implementação exigida para function ST_PCK_CONT.checa_data
    RETURN NULL;
  END checa_data;

    
    function exists_tipo_pagamento(p_tipo_pagamento number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM hdoc_001 
        WHERE tipo = p_tipo_pagamento;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_tipo_pagamento;
    
    function exists_posicao(p_posicao number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM crec_010 
        WHERE posicao_duplic = p_posicao;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_posicao;
    
    function get_contas_cpag(p_plano_conta number) return t_conta_cpag 
    AS
        v_conta_cpag t_conta_cpag;
    BEGIN
        BEGIN
            SELECT
                cta_irrf_cpag,
                cta_iss_cpag,
                cta_inss_cpag,
                cta_pis_cpag,
                cta_cofins_cpag,
                cta_csl_cpag,
                hst_irrf_cpag,
                hst_iss_cpag,
                hst_inss_cpag,
                hst_pis_cpag,
                hst_cofins_cpag,
                hst_csl_cpag,
                cta_pis_recuperar,
                cta_confins_recuperar,
                hst_pis_recuperar,
                hst_confins_recuperar
            INTO
                v_conta_cpag.cta_irrf_cpag,
                v_conta_cpag.cta_iss_cpag,
                v_conta_cpag.cta_inss_cpag,
                v_conta_cpag.cta_pis_cpag,
                v_conta_cpag.cta_cofins_cpag,
                v_conta_cpag.cta_csl_cpag,
                v_conta_cpag.hst_irrf_cpag,
                v_conta_cpag.hst_iss_cpag,
                v_conta_cpag.hst_inss_cpag,
                v_conta_cpag.hst_pis_cpag,
                v_conta_cpag.hst_cofins_cpag,
                v_conta_cpag.hst_csl_cpag,
                v_conta_cpag.cta_pis_recuperar,
                v_conta_cpag.cta_confins_recuperar,
                v_conta_cpag.hst_pis_recuperar,
                v_conta_cpag.hst_confins_recuperar
            FROM cont_560
            WHERE cod_plano_cta = p_plano_conta; 
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_conta_cpag.cta_irrf_cpag := null;
                    v_conta_cpag.cta_iss_cpag := null;
                    v_conta_cpag.cta_inss_cpag := null;
                    v_conta_cpag.cta_pis_cpag := null;
                    v_conta_cpag.cta_cofins_cpag := null;
                    v_conta_cpag.cta_csl_cpag := null;
                    v_conta_cpag.hst_irrf_cpag := null;
                    v_conta_cpag.hst_iss_cpag := null;
                    v_conta_cpag.hst_inss_cpag := null;
                    v_conta_cpag.hst_pis_cpag := null;
                    v_conta_cpag.hst_cofins_cpag := null;
                    v_conta_cpag.hst_csl_cpag := null;
                    v_conta_cpag.cta_pis_recuperar := null;
                    v_conta_cpag.cta_confins_recuperar := null;
                    v_conta_cpag.hst_pis_recuperar := null;
                    v_conta_cpag.hst_confins_recuperar := null;
        END;    
        RETURN v_conta_cpag;
    END get_contas_cpag;

    function gera_contabilizacao(p_cod_empresa      IN NUMBER, p_c_custo            IN NUMBER, p_cnpj9           IN NUMBER,
                                p_cnpj2             IN NUMBER, p_codContabilForn    IN NUMBER, p_documento       IN NUMBER,
                                p_data_transacao	IN DATE,   p_valor              IN NUMBER) return varchar2
    AS

    BEGIN
        return 'a';
    end gera_contabilizacao;
    
END ST_PCK_CONT;
