alter table OBRF_821 add constraint PK_OBRF_821 primary key (COD_EMPRESA, MES, ANO, ID, COD_MENSAGEM, COD_AJUSTE) novalidate;

alter table OBRF_823 add constraint PK_OBRF_823 primary key (COD_EMPRESA,MES,ANO,ID,COD_MENSAGEM,COD_AJUSTE53,NOTA,
							SERIE,CNPJ9,CNPJ4,CNPJ2,NIVEL,GRUPO,SUBGRUPO,ITEM,COD_AJUSTE) novalidate;

alter table obrf_823
  add constraint REF_OBRF_823_OBRF_821_1 foreign key (COD_EMPRESA, MES, ANO, ID, COD_MENSAGEM, COD_AJUSTE)
  references obrf_821 (COD_EMPRESA, MES, ANO, ID, COD_MENSAGEM, COD_AJUSTE) novalidate;

exec inter_pr_recompile;
