create or replace trigger inter_tr_obrf_874
  before delete on obrf_874
for each row
    

declare
   v_existe   number(1);
   v_natureza pedi_080.natur_operacao%type;
  
begin
     
   if deleting  
   then
       v_existe := 1;
      /*Caso a mensagem esteja relacionada a natureza nao permitir deletar */
      begin
         select c.natur_operacao into v_natureza
         from pedi_080 c
         where c.cod_mensagem   = :old.cod_mensagem
          and  ROWNUM = 1;
         exception
         when no_data_found then
            v_existe := 0;
      end;
      if v_existe = 1
      then  raise_application_error(-20000, 'ATEN��O! Mensagem j� associada natureza de opera��o. Ser� poss�vel elimin�-la. ');
      end if;      
   end if;     
end;
/
exec inter_pr_recompile;