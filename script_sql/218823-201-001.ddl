alter table PEDI_052 modify RPT_RETORNO varchar2(30);
alter table PEDI_052 modify RPT_REMESSA varchar2(30);

alter table PEDI_052_LOG modify RPT_RETORNO_OLD varchar2(30);
alter table PEDI_052_LOG modify RPT_RETORNO_NEW varchar2(30);

alter table PEDI_052_LOG modify RPT_REMESSA_OLD varchar2(30);
alter table PEDI_052_LOG modify RPT_REMESSA_NEW varchar2(30);

exec inter_pr_recompile;
