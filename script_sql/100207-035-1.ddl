alter table basi_750 add tipo_medida number(3);

comment on column basi_750.tipo_medida 
is 'Tipo de Medida.';

/
exec inter_pr_recompile;
