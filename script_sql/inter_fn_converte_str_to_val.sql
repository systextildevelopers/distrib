
  CREATE OR REPLACE FUNCTION "INTER_FN_CONVERTE_STR_TO_VAL" (
   pString  in varchar2) return varchar2 is

vRetorno number;

begin
   begin
      select to_number(pString)
      into vRetorno
      from dual;

      return (to_char(vRetorno));
   exception
   when others then
      return trim(pString);
   end;
end inter_fn_converte_str_to_val;

 

/

exec inter_pr_recompile;

