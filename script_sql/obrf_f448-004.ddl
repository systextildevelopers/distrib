-- Create table
create table OBRF_923
( ID_923      NUMBER(9) default 0 not null,
  ID_921      NUMBER(9) default 0 not null,  
  COD_PART    VARCHAR2(60) default ' ', 
  COD_MOD     VARCHAR2(2) default ' ', 
  SER         VARCHAR2(14) default ' ', 
  SUB         NUMBER(3) default 0, 
  NUM_DOC     NUMBER(9) default 0,
  DT_DOC      NUMBER(8) default 0,
  COD_ITEM    VARCHAR2(60) default ' ',
  VL_AJ_ITEM  NUMBER(15,2) default 0,
  CHV_DOCe    VARCHAR2(44) default 0,
  ind_ent_sai VARCHAR2(1) default ' ',
  cnpj9       NUMBER(9) default 0 not null,
  cnpj4       NUMBER(4) default 0 not null,
  cnpj2       NUMBER(2) default 0 not null,
  nivel       VARCHAR2(1) default ' ' not null,
  grupo       VARCHAR2(5) default ' ' not null,
  subgrupo    VARCHAR2(3) default ' ' not null,
  item        VARCHAR2(6) default ' ' not null,
  tipo_insert VARCHAR2(1) default 'M');
  
-- Add comments to the columns 
comment on column OBRF_923.COD_PART   is 'C�digo do participante (campo 02 do Registro 0150): - do emitente do documento ou do 
                                          remetente das mercadorias,no caso de entradas, - do adquirente, no caso de sa�das.';
comment on column OBRF_923.COD_MOD    is 'C�digo do modelo do documento fiscal, conforme a Tabela 4.1.1.';
comment on column OBRF_923.SER        is 'S�rie do documento fiscal.';
comment on column OBRF_923.SUB        is 'Subs�rie do documento fiscal.';
comment on column OBRF_923.NUM_DOC    is 'Data da emiss�o do documento fiscal.';
comment on column OBRF_923.DT_DOC     is 'C�digo do item (campo 02 do Registro 0200).';
comment on column OBRF_923.COD_ITEM   is 'Valor do ajuste para a opera��o/item.';
comment on column OBRF_923.VL_AJ_ITEM is 'Chave do Documento Eletr�nico.';
comment on column OBRF_923.CHV_DOCe   is 'Descri��o complementar.';

-- Create/Recreate primary, unique and foreign key constraints 
alter table OBRF_923 add constraint PK_OBRF_923 primary key (ID_923);
alter table OBRF_923 add constraint UNIQ_OBRF_923 unique (ID_921, COD_PART, COD_MOD, SER, NUM_DOC, COD_ITEM);

create synonym systextilrpt.OBRF_923 for OBRF_923; 

create sequence seq_obrf_923
minvalue 0
maxvalue 99999999999999999999
start with 1
increment by 1;

CREATE OR REPLACE TRIGGER INTER_TR_OBRF_923_SEQ
BEFORE INSERT ON OBRF_923 FOR EACH ROW
DECLARE
    next_value number;
BEGIN
    if :new.ID_923 is null or :new.ID_923 = 0 then
        select seq_obrf_923.nextval
        into next_value from dual;
        :new.ID_923 := next_value;
    end if;
END INTER_TR_OBRF_923_SEQ;

/

exec inter_pr_recompile;
