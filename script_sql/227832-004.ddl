begin

--================
--== Inventario ==  
--================

  begin
    execute immediate '
      CREATE TABLE  "ESTQ_195" 
				(	"NUMERO_CONTAGEM" NUMBER, 
				"CODIGO_VOLUME" VARCHAR2(10), 
				"TURNO" NUMBER(1,0), 
				"NIVEL" VARCHAR2(1), 
				"GRUPO" VARCHAR2(5), 
				"SUBGRUPO" VARCHAR2(3), 
				"ITEM" VARCHAR2(6), 
				"DEPOSITO_VOLUME" NUMBER(3,0), 
				"DEPOSITO_INVENTARIO" NUMBER(3,0), 
				"ENDERECO_VOLUME" VARCHAR2(10), 
				"ENDERECO_INVENTARIO" VARCHAR2(10), 
				"USUARIO_SYSTEXTIL" VARCHAR2(2000), 
				"USUARIO_REDE" VARCHAR2(2000), 
				"MAQUINA_REDE" VARCHAR2(2000), 
				"DATA_OPERACAO" TIMESTAMP (6), 
				"PROGRAMA" VARCHAR2(200), 
				"CODIGO_INVENTARIO" NUMBER(9,0), 
				"ID" NUMBER, 
				CONSTRAINT "ESTQ_195_PK" PRIMARY KEY ("ID")
				USING INDEX  ENABLE
			)';
    exception when others then
      if sqlcode != -955 then
        raise;
      end if;
  end;

  begin
    execute immediate '
      CREATE SEQUENCE "ESTQ_195_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1
    ';
    exception when others then
      if sqlcode != -955 then
        raise;
      end if;
  end;

end;
