INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('fiscal.enviaCodClientePedidoXML', 0, 'null', 'null', 'S', null, null, null);

declare 

cursor parametro_c is
  select codigo_empresa
  from fatu_500;

begin
  for reg_parametro in parametro_c
  loop
      begin
        insert into empr_008 (codigo_empresa, param, val_str) values (reg_parametro.codigo_empresa, 'fiscal.enviaCodClientePedidoXML', 'S');
      end;

  end loop;
  commit;
end;
/
