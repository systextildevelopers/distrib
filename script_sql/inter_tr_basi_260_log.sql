
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_260_LOG" 
after delete or update
on BASI_260
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);                            

 if updating
 then
    begin
        insert into BASI_260_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           DATA_CALENDARIO_OLD, /*8*/
           DATA_CALENDARIO_NEW, /*9*/
           DIA_UTIL_OLD, /*10*/
           DIA_UTIL_NEW, /*11*/
           DIA_SEMANA_OLD, /*12*/
           DIA_SEMANA_NEW, /*13*/
           NUMERO_SEMANA_OLD, /*14*/
           NUMERO_SEMANA_NEW, /*15*/
           PER_DISTR_FATU_OLD, /*16*/
           PER_DISTR_FATU_NEW, /*17*/
           DIA_UTIL_FINAN_OLD, /*18*/
           DIA_UTIL_FINAN_NEW  /*19*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.DATA_CALENDARIO,  /*8*/
           :new.DATA_CALENDARIO, /*9*/
           :old.DIA_UTIL,  /*10*/
           :new.DIA_UTIL, /*11*/
           :old.DIA_SEMANA,  /*12*/
           :new.DIA_SEMANA, /*13*/
           :old.NUMERO_SEMANA,  /*14*/
           :new.NUMERO_SEMANA, /*15*/
           :old.PER_DISTR_FATU,  /*16*/
           :new.PER_DISTR_FATU, /*17*/
           :old.DIA_UTIL_FINAN,  /*18*/
           :new.DIA_UTIL_FINAN  /*19*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into BASI_260_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           DATA_CALENDARIO_OLD, /*8*/
           DATA_CALENDARIO_NEW, /*9*/
           DIA_UTIL_OLD, /*10*/
           DIA_UTIL_NEW, /*11*/
           DIA_SEMANA_OLD, /*12*/
           DIA_SEMANA_NEW, /*13*/
           NUMERO_SEMANA_OLD, /*14*/
           NUMERO_SEMANA_NEW, /*15*/
           PER_DISTR_FATU_OLD, /*16*/
           PER_DISTR_FATU_NEW, /*17*/
           DIA_UTIL_FINAN_OLD, /*18*/
           DIA_UTIL_FINAN_NEW /*19*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.DATA_CALENDARIO, /*8*/
           null, /*9*/
           :old.DIA_UTIL, /*10*/
           0, /*11*/
           :old.DIA_SEMANA, /*12*/
           0, /*13*/
           :old.NUMERO_SEMANA, /*14*/
           0, /*15*/
           :old.PER_DISTR_FATU, /*16*/
           0, /*17*/
           :old.DIA_UTIL_FINAN, /*18*/
           0 /*19*/
         );
    end;
 end if;
end inter_tr_BASI_260_log;

-- ALTER TRIGGER "INTER_TR_BASI_260_LOG" ENABLE
 

/

exec inter_pr_recompile;

