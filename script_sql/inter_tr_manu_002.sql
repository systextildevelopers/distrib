
  CREATE OR REPLACE TRIGGER "INTER_TR_MANU_002" 
   after insert or
         update of solicitacao, cod_empresa, tipo_servico, executa_trigger
         or delete
   on manu_002
   for each row
declare
  Pragma Autonomous_Transaction;

  -- declaracao das variaveis
  v_existe_recurso        number;
  v_existe_calendario     number;
  v_codigo_recurso        varchar2(20);
  v_tempo_serv            number;
  v_total_tempo_serv      number;
  v_data_capa_solic       date;
  v_hora_capa_solic       date;
  v_existe_tmrp           number;
  v_tipo_solic_empr       number;
  v_tipo_solic_solic      number;
  v_grupo_maquina_solic   varchar(4);
  v_subgru_maquina_solic  varchar(3);
  v_numero_maquina_solic  number;
  v_tempo_disp_maquina    number;
  v_data_calc_disp        date;
  v_hora_calc_disp        date;
  v_hora_max_convert_min  number;
  v_tempo_restante_manu   number;
  v_sobra_tempo_dia       number;
  v_hora_convert_capa     number;
  v_tempo_old_serv        number;
  v_centro_custo_capa     number;
  v_status_solic_capa     number;

  v_executa_trigger      number;

begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting
      then
         -- Seleciona o tipo de manutecao preventiva cadastrada para a empresa
         -- se nao tiver, nao executara a rotina de planificacao do recurso
         -- para a manutencao
         begin
            select fatu_502.tipo_sol_preventiva
            into v_tipo_solic_empr
            from fatu_502
            where fatu_502.codigo_empresa = :new.cod_empresa;
         exception when others then
            v_tipo_solic_empr := 0;
         end;

         begin
               -- Le a capa da solicitacao de manutencao...
            select manu_001.data_solicitacao, manu_001.hora_solicitacao,
                   manu_001.grupo_maquina,    manu_001.subgru_maquina,
                   manu_001.numero_maquina,   manu_001.tipo_solicitacao,
                   to_number(to_char(manu_001.hora_solicitacao,'sssss')/60),
                   manu_001.c_custo_resp,     manu_001.sit_solicitacao
            into   v_data_capa_solic,         v_hora_capa_solic,
                   v_grupo_maquina_solic,     v_subgru_maquina_solic,
                   v_numero_maquina_solic,    v_tipo_solic_solic,
                   v_hora_convert_capa,
                   v_centro_custo_capa,       v_status_solic_capa
            from manu_001
            where manu_001.solicitacao = :new.solicitacao
              and manu_001.cod_empresa = :new.cod_empresa;
         exception
         when others then
            v_data_capa_solic      := null;
            v_hora_capa_solic      := null;
            v_grupo_maquina_solic  := '';
            v_subgru_maquina_solic := '';
            v_numero_maquina_solic := 0;
            v_centro_custo_capa    := 0;
            v_tipo_solic_solic     := 0;
            v_hora_convert_capa    := 0;
            v_status_solic_capa    := 0;
         end;

         if v_tipo_solic_solic   = v_tipo_solic_empr and v_tipo_solic_solic <> 0 and
            v_status_solic_capa <> 0
         then

            v_codigo_recurso := ltrim(rtrim(v_grupo_maquina_solic)) || '.' || ltrim(rtrim(v_subgru_maquina_solic)) || '.' || ltrim(rtrim(to_char(v_numero_maquina_solic,'00000')));

            --VALIDA SE A MAQUINA E ESTA CADASTRADA COMO RECURSO 1 - MAQUINA
            begin
               select count(*)
               into v_existe_recurso
               from tmrp_660
               where tmrp_660.codigo_recurso = v_codigo_recurso
                 and tmrp_660.tipo_recurso = 1;
            exception when others then
               v_existe_recurso := 0;
            end;

            --VALIDA SE A MAQUINA CONTEM CALEDARIO PARA TIPO DE RECURSO 1 - MAQUINA
            begin
               select nvl(count(*),0)
               into v_existe_calendario
               from tmrp_665
               where tmrp_665.tipo_recurso = 1
                 and tmrp_665.codigo_recurso = v_codigo_recurso;
            exception when others then
               v_existe_calendario := 0;
            end;

            if v_existe_recurso <> 0 and v_existe_calendario <> 0
            then
               v_total_tempo_serv := 0;

               -- le os servicos da solicitacao cadastrada
               -- para pegar os tempos em MINUTOS dos servicos
               -- no primeiro momento estes minutos vem do agendamento
               -- destes servicos.
               begin
                  for reg_manu_001 in (select manu_002.tipo_servico
                                       from manu_002
                                       where manu_002.solicitacao = :new.solicitacao
                                         and manu_002.cod_empresa = :new.cod_empresa)
                  loop
                     begin
                        select manu_005.minutos_previstos
                        into v_tempo_serv
                        from manu_005
                        where manu_005.grupo_maquina  = v_grupo_maquina_solic
                          and manu_005.subgru_maquina = v_subgru_maquina_solic
                          and manu_005.numero_maquina = v_numero_maquina_solic
                          and manu_005.tipo_servico   = reg_manu_001.tipo_servico;
                     exception when others then
                           v_tempo_serv := 0;
                     end;

                     -- Caso o tempo encontrado para o servicos seja maior que 0, entao soma.
                     if v_tempo_serv > 0
                     then
                        v_total_tempo_serv := v_total_tempo_serv + v_tempo_serv;
                     end if;
                  end loop;

                  -- Calcula o tempo do servico que esta sendo inserindo
                  -- mais os servicos ja cadastrados
                  begin
                     select manu_005.minutos_previstos
                     into v_tempo_serv
                     from manu_005
                     where manu_005.grupo_maquina  = v_grupo_maquina_solic
                       and manu_005.subgru_maquina = v_subgru_maquina_solic
                       and manu_005.numero_maquina = v_numero_maquina_solic
                       and manu_005.c_custo_resp   = v_centro_custo_capa
                       and manu_005.tipo_servico   = :new.tipo_servico;
                  exception when others then
                       v_tempo_serv := 0;
                  end;

                  if v_tempo_serv > 0
                  then
                     v_total_tempo_serv := v_total_tempo_serv + v_tempo_serv;
                  end if;

                  if v_total_tempo_serv is null
                  then
                     v_total_tempo_serv := 0;
                  end if;
               end;

               v_data_calc_disp      := v_data_capa_solic;
               v_tempo_restante_manu := v_total_tempo_serv;
               v_hora_max_convert_min := 0;

               -- Consiste hora da manuntecao com a maior data da alocada do dia da manutencao
               -- caso ja exista alguma ordem ou alguma planificacao para depois da hora prevista
               -- para inicio da manuntecao preventiva, entao nao ira gerar a alocacao da manutencao
               -- e avisara o usuarios que nao a solicitacao nao foi planificada.
               if v_hora_max_convert_min < v_hora_convert_capa
               then
                  -- Antes de comecar verifica se tera tempo disponivel, para nao ficar no loop ate estorar a data
                  begin
                      select sum(tmrp_660.tempo_disponivel)
                      into v_tempo_disp_maquina
                      from tmrp_660
                      where tmrp_660.tipo_recurso   = 1
                        and tmrp_660.codigo_recurso = v_codigo_recurso
                        and tmrp_660.data_recurso   >= v_data_calc_disp
                      having sum(tmrp_660.tempo_disponivel) >= v_tempo_restante_manu;
                  exception
                      when no_data_found then
                        raise_application_error (-20000, 'ATENCAO! Nao ha tempo disponivel para o recurso '||v_codigo_recurso||Chr(10)||'Verifique a geracao do calendario dos recursos.');
                  end;
                  begin
                     while v_tempo_restante_manu > 0
                     loop
                        -- Tempo disponivel no calendario
                        begin
                           select tmrp_660.tempo_disponivel
                           into v_tempo_disp_maquina
                           from tmrp_660
                           where tmrp_660.tipo_recurso   = 1
                             and tmrp_660.codigo_recurso = v_codigo_recurso
                             and tmrp_660.data_recurso   = v_data_calc_disp;
                        exception
                        when others then
                           v_tempo_disp_maquina := 0;
                        end;
                        -- Diminui o tempo ja alocado do dia da manutencao
                        if v_hora_max_convert_min > 0
                        then
                           v_tempo_disp_maquina := v_tempo_disp_maquina - v_hora_max_convert_min;
                           v_hora_max_convert_min := 0;
                        end if;

                        v_tempo_restante_manu := v_tempo_restante_manu - v_tempo_disp_maquina;

                        if v_tempo_restante_manu <= 0
                        then
                           v_sobra_tempo_dia := v_tempo_restante_manu * -1;
                           select to_date('14/06/1989'||to_char(v_hora_capa_solic + ((trunc((v_tempo_disp_maquina-v_sobra_tempo_dia)/v_tempo_disp_maquina)-((v_tempo_disp_maquina-v_sobra_tempo_dia)/v_tempo_disp_maquina))*-1),'hh24:mi'),'DD/MM/YYYY HH24:MI:SS')
                           into v_hora_calc_disp
                           from dual;

                        end if;

                        if v_tempo_restante_manu > 0
                        then
                           v_data_calc_disp := v_data_calc_disp + 1;
                        end if;
                     end loop;
                  end;
               end if;

               -- Caso tiver algum registro dentro do periodo e para a maquina em questao
               -- entao dara a mensagem para o usuario
               -- dizendo: ATENCAO! JA EXISTE ORDEM PLANIFICADA PARA A MAQUINA
               begin
                  select nvl(count(*),0)
                  into v_existe_tmrp
                  from tmrp_650
                  where tmrp_650.data_termino   <= v_data_capa_solic
                    and tmrp_650.hora_termino    > v_hora_capa_solic
                    and tmrp_650.codigo_recurso  = v_codigo_recurso;
               exception when others
               then
                  v_existe_tmrp := 0;
               end;

               if v_existe_tmrp = 0
               then
                  v_total_tempo_serv := v_total_tempo_serv + 0.000;

                  begin
                     update tmrp_650
                     set tmrp_650.data_termino     = v_data_calc_disp,
                         tmrp_650.hora_termino     = v_hora_calc_disp
                     where tmrp_650.tipo_alocacao  = 9
                       and tmrp_650.codigo_recurso = v_codigo_recurso
                       and tmrp_650.tipo_recurso   = 1
                       and tmrp_650.ordem_trabalho = :new.solicitacao;

                     if SQL%notfound
                     then
                        insert into tmrp_650
                        (tipo_alocacao,         tipo_recurso,
                         codigo_recurso,        ordem_trabalho,
                         data_inicio,           hora_inicio,
                         data_termino,          hora_termino,
                         qtde_recursos
                        )
                        values
                        (9,                     1,
                         v_codigo_recurso,      :new.solicitacao,
                         v_data_capa_solic,     v_hora_capa_solic,
                         v_data_calc_disp,      v_hora_calc_disp,
                         1
                        );
                     end if;
                  exception when OTHERS then
                     raise_application_error (-20000, 'Nao gerou planificacao para a manutencao preventiva.(tmrp_650)(1)');
                  end;
               end if;
            end if;
         end if;
      commit;
      end if;  -- final da atualizacao da tabela tmrp_650 quando estiver inserindo uma solicitacao de manutencao.

      if updating
      then
         begin
            select fatu_502.tipo_sol_preventiva
            into v_tipo_solic_empr
            from fatu_502
            where fatu_502.codigo_empresa = :new.cod_empresa;
         exception when others then
            v_tipo_solic_empr := 0;
         end;

         begin
            -- Le a capa da solicitacao de manutencao...
            select manu_001.data_solicitacao, manu_001.hora_solicitacao,
                   manu_001.grupo_maquina,    manu_001.subgru_maquina,
                   manu_001.numero_maquina,   manu_001.tipo_solicitacao,
                   to_number(to_char(manu_001.hora_solicitacao,'sssss')/60),
                   manu_001.c_custo_resp,     manu_001.sit_solicitacao
            into   v_data_capa_solic,         v_hora_capa_solic,
                   v_grupo_maquina_solic,     v_subgru_maquina_solic,
                   v_numero_maquina_solic,    v_tipo_solic_solic,
                   v_hora_convert_capa,
                   v_centro_custo_capa,       v_status_solic_capa
            from manu_001
            where manu_001.solicitacao = :new.solicitacao
              and manu_001.cod_empresa = :new.cod_empresa;
         exception
         when others then
            v_data_capa_solic      := null;
            v_hora_capa_solic      := null;
            v_grupo_maquina_solic  := '';
            v_subgru_maquina_solic := '';
            v_numero_maquina_solic := 0;
            v_centro_custo_capa    := 0;
            v_tipo_solic_solic     := 0;
            v_hora_convert_capa    := 0;
            v_status_solic_capa    := 0;
         end;

         if v_tipo_solic_solic   = v_tipo_solic_empr and v_tipo_solic_solic <> 0 and
            v_status_solic_capa <> 0
         then

            v_codigo_recurso := ltrim(rtrim(v_grupo_maquina_solic)) || '.' || ltrim(rtrim(v_subgru_maquina_solic)) || '.' || ltrim(rtrim(to_char(v_numero_maquina_solic,'00000')));

            --VALIDA SE A MAQUINA E ESTA CADASTRADA COMO RECURSO 1 - MAQUINA
            begin
               select count(*)
               into v_existe_recurso
               from tmrp_660
               where tmrp_660.codigo_recurso = v_codigo_recurso
                 and tmrp_660.tipo_recurso = 1;
            exception when others then
               v_existe_recurso := 0;
            end;

            --VALIDA SE A MAQUINA CONTEM CALEDARIO PARA TIPO DE RECURSO 1 - MAQUINA
            begin
               select nvl(count(*),0)
               into v_existe_calendario
               from tmrp_665
               where tmrp_665.tipo_recurso = 1
                 and tmrp_665.codigo_recurso = v_codigo_recurso;
            exception when others then
               v_existe_calendario := 0;
            end;

            if v_existe_recurso <> 0 and v_existe_calendario <> 0
            then
               v_total_tempo_serv := 0;

               -- le os servicos da solicitacao cadastrada
               -- para pegar os tempos em MINUTOS dos servicos
               -- no primeiro momento estes minutos vem do agendamento
               -- destes servicos.
               begin
                  for reg_manu_001 in (select manu_002.tipo_servico
                                       from manu_002
                                       where manu_002.solicitacao = :new.solicitacao
                                         and manu_002.cod_empresa = :new.cod_empresa)
                  loop
                     begin
                        select manu_005.minutos_previstos
                        into v_tempo_serv
                        from manu_005
                        where manu_005.grupo_maquina  = v_grupo_maquina_solic
                          and manu_005.subgru_maquina = v_subgru_maquina_solic
                          and manu_005.numero_maquina = v_numero_maquina_solic
                          and manu_005.tipo_servico   = reg_manu_001.tipo_servico;
                     exception when others then
                           v_tempo_serv := 0;
                     end;

                     -- Caso o tempo encontrado para o servicos seja maior que 0, entao soma.
                     if v_tempo_serv > 0
                     then
                        v_total_tempo_serv := v_total_tempo_serv + v_tempo_serv;
                     end if;
                  end loop;

                  -- Calcula o tempo do servico que esta sendo inserindo
                  -- mais os servicos ja cadastrados
                  begin
                     select manu_005.minutos_previstos
                     into v_tempo_serv
                     from manu_005
                     where manu_005.grupo_maquina  = v_grupo_maquina_solic
                       and manu_005.subgru_maquina = v_subgru_maquina_solic
                       and manu_005.numero_maquina = v_numero_maquina_solic
                       and manu_005.c_custo_resp   = v_centro_custo_capa
                       and manu_005.tipo_servico   = :new.tipo_servico;
                  exception when others then
                       v_tempo_serv := 0;
                  end;

                  if v_tempo_serv > 0
                  then
                     begin
                        select manu_005.minutos_previstos
                        into v_tempo_old_serv
                        from manu_005
                        where manu_005.grupo_maquina  = v_grupo_maquina_solic
                         and manu_005.subgru_maquina = v_subgru_maquina_solic
                         and manu_005.numero_maquina = v_numero_maquina_solic
                         and manu_005.tipo_servico   = :old.tipo_servico;
                     exception
                     when others then
                        v_tempo_old_serv := 0;
                     end;

                     v_total_tempo_serv := (v_total_tempo_serv - v_tempo_old_serv) + v_tempo_serv;
                  end if;

                  if v_total_tempo_serv is null
                  then
                     v_total_tempo_serv := 0;
                  end if;
               end;

               v_data_calc_disp      := v_data_capa_solic;
               v_tempo_restante_manu := v_total_tempo_serv;
               v_hora_max_convert_min := 0;

               -- Consiste hora da manuntecao com a maior data da alocada do dia da manutencao
               -- caso ja exista alguma ordem ou alguma planificacao para depois da hora prevista
               -- para inicio da manuntecao preventiva, entao nao ira gerar a alocacao da manutencao
               -- e avisara o usuarios que nao a solicitacao nao foi planificada.
               if v_hora_max_convert_min < v_hora_convert_capa
               then
                  -- Antes de comecar verifica se tera tempo disponivel, para nao ficar no loop ate estorar a data
                  begin
                      select sum(tmrp_660.tempo_disponivel)
                      into v_tempo_disp_maquina
                      from tmrp_660
                      where tmrp_660.tipo_recurso   = 1
                        and tmrp_660.codigo_recurso = v_codigo_recurso
                        and tmrp_660.data_recurso   >= v_data_calc_disp
                      having sum(tmrp_660.tempo_disponivel) >= v_tempo_restante_manu;
                  exception
                      when no_data_found then
                        raise_application_error (-20000, 'ATENCAO! Nao ha tempo disponivel para o recurso '||v_codigo_recurso||Chr(10)||'Verifique a geracao do calendario dos recursos.');
                  end;
                  begin
                     while v_tempo_restante_manu > 0
                     loop
                        -- Tempo disponivel no calendario
                        begin
                           select tmrp_660.tempo_disponivel
                           into v_tempo_disp_maquina
                           from tmrp_660
                           where tmrp_660.tipo_recurso   = 1
                             and tmrp_660.codigo_recurso = v_codigo_recurso
                             and tmrp_660.data_recurso   = v_data_calc_disp;
                        exception
                        when others then
                           v_tempo_disp_maquina := 0;
                        end;
                        -- Diminui o tempo ja alocado do dia da manutencao
                        if v_hora_max_convert_min > 0
                        then
                           v_tempo_disp_maquina := v_tempo_disp_maquina - v_hora_max_convert_min;
                           v_hora_max_convert_min := 0;
                        end if;

                        v_tempo_restante_manu := v_tempo_restante_manu - v_tempo_disp_maquina;

                        if v_tempo_restante_manu <= 0
                        then
                           v_sobra_tempo_dia := v_tempo_restante_manu * -1;
                           select to_date('14/06/1989'||to_char(v_hora_capa_solic + ((v_tempo_disp_maquina-v_sobra_tempo_dia)/v_tempo_disp_maquina),'hh24:mi'),'DD/MM/YYYY HH24:MI:SS')
                           into v_hora_calc_disp
                           from dual;

                        end if;

                        if v_tempo_restante_manu > 0
                        then
                           v_data_calc_disp := v_data_calc_disp + 1;
                        end if;
                     end loop;
                  end;
               end if;

               -- Caso tiver algum registro dentro do periodo e para a maquina em questao
               -- entao dara a mensagem para o usuario
               -- dizendo: ATENCAO! JA EXISTE ORDEM PLANIFICADA PARA A MAQUINA
               begin
                  select nvl(count(*),0)
                  into v_existe_tmrp
                  from tmrp_650
                  where tmrp_650.data_termino   <= v_data_capa_solic
                    and tmrp_650.hora_termino    > v_hora_capa_solic
                    and tmrp_650.codigo_recurso  = v_codigo_recurso;
               exception when others
               then
                  v_existe_tmrp := 0;
               end;

               if v_existe_tmrp = 0
               then
                  v_total_tempo_serv := v_total_tempo_serv + 0.000;

                  begin
                     update tmrp_650
                     set tmrp_650.data_termino = v_data_calc_disp,
                         tmrp_650.hora_termino = v_hora_calc_disp
                     where tmrp_650.tipo_alocacao  = 9
                       and tmrp_650.codigo_recurso = v_codigo_recurso
                       and tmrp_650.tipo_recurso   = 1
                       and tmrp_650.ordem_trabalho = :new.solicitacao;
                  exception when OTHERS then
                     raise_application_error (-20000, 'Nao atualizou planificacao para a manutencao preventiva.(tmrp_650)(1)');
                  end;
               end if;
            end if;
         end if;
      commit;
      end if;  -- final da atualizacao da tabela tmrp_650 quando estiver atualizando uma solicitacao de manutencao.

      if deleting
      then
         begin
            select fatu_502.tipo_sol_preventiva
            into v_tipo_solic_empr
            from fatu_502
            where fatu_502.codigo_empresa = :old.cod_empresa;
         exception when others then
            v_tipo_solic_empr := 0;
         end;

         begin
            -- Le a capa da solicitacao de manutencao...
            select manu_001.data_solicitacao, manu_001.hora_solicitacao,
                   manu_001.grupo_maquina,    manu_001.subgru_maquina,
                   manu_001.numero_maquina,   manu_001.tipo_solicitacao,
                   to_number(to_char(manu_001.hora_solicitacao,'sssss')/60),
                   manu_001.c_custo_resp,     manu_001.sit_solicitacao
            into   v_data_capa_solic,         v_hora_capa_solic,
                   v_grupo_maquina_solic,     v_subgru_maquina_solic,
                   v_numero_maquina_solic,    v_tipo_solic_solic,
                   v_hora_convert_capa,
                   v_centro_custo_capa,       v_status_solic_capa
            from manu_001
            where manu_001.solicitacao = :old.solicitacao
              and manu_001.cod_empresa = :old.cod_empresa;
         exception
         when others then
            v_data_capa_solic      := null;
            v_hora_capa_solic      := null;
            v_grupo_maquina_solic  := '';
            v_subgru_maquina_solic := '';
            v_numero_maquina_solic := 0;
            v_centro_custo_capa    := 0;
            v_tipo_solic_solic     := 0;
            v_hora_convert_capa    := 0;
            v_status_solic_capa    := 0;
         end;

         if v_tipo_solic_solic   = v_tipo_solic_empr and v_tipo_solic_solic <> 0 and
            v_status_solic_capa <> 0
         then

            v_codigo_recurso := ltrim(rtrim(v_grupo_maquina_solic)) || '.' || ltrim(rtrim(v_subgru_maquina_solic)) || '.' || ltrim(rtrim(to_char(v_numero_maquina_solic,'00000')));

            --VALIDA SE A MAQUINA E ESTA CADASTRADA COMO RECURSO 1 - MAQUINA
            begin
               select count(*)
               into v_existe_recurso
               from tmrp_660
               where tmrp_660.codigo_recurso = v_codigo_recurso
                 and tmrp_660.tipo_recurso = 1;
            exception when others then
               v_existe_recurso := 0;
            end;

            --VALIDA SE A MAQUINA CONTEM CALEDARIO PARA TIPO DE RECURSO 1 - MAQUINA
            begin
               select nvl(count(*),0)
               into v_existe_calendario
               from tmrp_665
               where tmrp_665.tipo_recurso = 1
                 and tmrp_665.codigo_recurso = v_codigo_recurso;
            exception when others then
               v_existe_calendario := 0;
            end;

            if v_existe_recurso <> 0 and v_existe_calendario <> 0
            then
               v_total_tempo_serv := 0;

               -- le os servicos da solicitacao cadastrada
               -- para pegar os tempos em MINUTOS dos servicos
               -- no primeiro momento estes minutos vem do agendamento
               -- destes servicos.
               begin
                  for reg_manu_001 in (select manu_002.tipo_servico
                                       from manu_002
                                       where manu_002.solicitacao = :old.solicitacao
                                         and manu_002.cod_empresa = :old.cod_empresa)
                  loop
                     begin
                        select manu_005.minutos_previstos
                        into v_tempo_serv
                        from manu_005
                        where manu_005.grupo_maquina  = v_grupo_maquina_solic
                          and manu_005.subgru_maquina = v_subgru_maquina_solic
                          and manu_005.numero_maquina = v_numero_maquina_solic
                          and manu_005.c_custo_resp   = v_centro_custo_capa
                          and manu_005.tipo_servico   = reg_manu_001.tipo_servico;
                     exception when others then
                           v_tempo_serv := 0;
                     end;

                     -- Caso o tempo encontrado para o servicos seja maior que 0, entao soma.
                     if v_tempo_serv > 0
                     then
                        v_total_tempo_serv := v_total_tempo_serv + v_tempo_serv;
                     end if;
                  end loop;

                  begin
                     select manu_005.minutos_previstos
                     into v_tempo_old_serv
                     from manu_005
                     where manu_005.grupo_maquina  = v_grupo_maquina_solic
                       and manu_005.subgru_maquina = v_subgru_maquina_solic
                       and manu_005.numero_maquina = v_numero_maquina_solic
                       and manu_005.tipo_servico   = :old.tipo_servico;
                  exception
                  when others then
                     v_tempo_old_serv := 0;
                  end;

                     v_total_tempo_serv := v_total_tempo_serv - v_tempo_old_serv;
               end;

               v_data_calc_disp      := v_data_capa_solic;
               v_tempo_restante_manu := v_total_tempo_serv;
               v_hora_max_convert_min := 0;

               -- Consiste hora da manuntecao com a maior data da alocada do dia da manutencao
               -- caso ja exista alguma ordem ou alguma planificacao para depois da hora prevista
               -- para inicio da manuntecao preventiva, entao nao ira gerar a alocacao da manutencao
               -- e avisara o usuarios que nao a solicitacao nao foi planificada.
               if v_hora_max_convert_min < v_hora_convert_capa
               then
                  begin
                     while v_tempo_restante_manu > 0
                     loop
                        begin
                           select min(tmrp_660.data_recurso)
                           into v_data_calc_disp
                           from tmrp_660
                           where tmrp_660.tipo_recurso    = 1
                             and tmrp_660.codigo_recurso  = v_codigo_recurso
                             and tmrp_660.data_recurso   >= v_data_calc_disp;
                        exception when OTHERS
                        then
                           raise_application_error(-20000,'ATENCAO! Verifique se ha calendario gerado para o periodo livre da maquina.');
                        end;

                        -- Tempo disponivel no calendario
                        begin
                           select tmrp_660.tempo_disponivel
                           into v_tempo_disp_maquina
                           from tmrp_660
                           where tmrp_660.tipo_recurso   = 1
                             and tmrp_660.codigo_recurso = v_codigo_recurso
                             and tmrp_660.data_recurso   = v_data_calc_disp;
                        exception
                        when others then
                           v_tempo_disp_maquina := 0;
                        end;
                        -- Diminui o tempo ja alocado do dia da manutencao
                        if v_hora_max_convert_min > 0
                        then
                           v_tempo_disp_maquina := v_tempo_disp_maquina - v_hora_max_convert_min;
                           v_hora_max_convert_min := 0;
                        end if;

                        v_tempo_restante_manu := v_tempo_restante_manu - v_tempo_disp_maquina;

                        if v_tempo_restante_manu <= 0
                        then
                           begin
                              v_sobra_tempo_dia := v_tempo_restante_manu * -1;
                              select to_date('14/06/1989'||to_char(v_hora_capa_solic + ((v_tempo_disp_maquina-v_sobra_tempo_dia)/v_tempo_disp_maquina),'hh24:mi'),'DD/MM/YYYY HH24:MI:SS')
                              into v_hora_calc_disp
                              from dual;
                           exception when OTHERS
                           then
                              raise_application_error(-20000,'ATENCAO! Verifique se ha calendario gerado para o periodo livre da maquina.');
                           end;
                        end if;

                        if v_tempo_restante_manu > 0
                        then
                           v_data_calc_disp := v_data_calc_disp + 1;
                        end if;
                     end loop;
                  end;
               end if;

               -- Caso tiver algum registro dentro do periodo e para a maquina em questao
               -- entao dara a mensagem para o usuario
               -- dizendo: ATENCAO! JA EXISTE ORDEM PLANIFICADA PARA A MAQUINA
               begin
                  select nvl(count(*),0)
                  into v_existe_tmrp
                  from tmrp_650
                  where tmrp_650.data_termino   <= v_data_capa_solic
                    and tmrp_650.hora_termino    > v_hora_capa_solic
                    and tmrp_650.codigo_recurso  = v_codigo_recurso;
               exception when others
               then
                  v_existe_tmrp := 0;
               end;

               if v_existe_tmrp = 0
               then

                  begin
                     update tmrp_650
                     set tmrp_650.data_termino = v_data_calc_disp,
                         tmrp_650.hora_termino = v_hora_calc_disp
                     where tmrp_650.tipo_alocacao  = 9
                       and tmrp_650.codigo_recurso = v_codigo_recurso
                       and tmrp_650.tipo_recurso   = 1
                       and tmrp_650.ordem_trabalho = :old.solicitacao;
                  exception when OTHERS then
                     raise_application_error (-20000, 'Nao atualizou planificacao para a manutencao preventiva.(tmrp_650)(1)');
                  end;
               end if;
            end if;
         end if;
         commit;
      end if; -- Fim da delecao do tempo do servico da tmrp_650
   end if;
end; -- Final da trigger da manu_002

-- ALTER TRIGGER "INTER_TR_MANU_002" ENABLE
 

/

exec inter_pr_recompile;

