CREATE TABLE TMRP_200
(
  NRO_SOLICITACAO   number(9) default 0 not null,
  TIPO_ADIANTAM     number(1) default 0 not null,
  NUMERO_ADIANTAM   number(6) default 0 not null,
  DATA_INSERT       DATE default sysdate,
  SEL               number(1) default 0,  
  CNPJ9             number(9) default 0,
  CNPJ4             number(4) default 0,
  CNPJ2             number(2) default 0,
  DATA_DIGITACAO    DATE,
  VALOR_ADIANT      number(13,2) default 0.0,
  CODIGO_EMPRESA    number(3) default 0
);

alter table tmrp_200 add constraint tmrp_200_pk primary key (NRO_SOLICITACAO, TIPO_ADIANTAM, NUMERO_ADIANTAM);

comment on column tmrp_200.TIPO_ADIANTAM is 'tipo do adiantamento';     
comment on column tmrp_200.NUMERO_ADIANTAM is 'numero do adiantamento';
comment on column tmrp_200.cnpj9 is 'cnpj do cliente';     
comment on column tmrp_200.cnpj4 is 'cnpj do cliente';
comment on column tmrp_200.cnpj2 is 'cnpj do cliente';
