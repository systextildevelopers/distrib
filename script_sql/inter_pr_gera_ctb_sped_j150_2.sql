create or replace procedure inter_pr_gera_ctb_sped_j150_2(p_cod_empresa IN NUMBER,
                                                        p_cod_filial    IN NUMBER,
                                                        p_tipo_dre      IN NUMBER,
                                                        p_periodo       IN NUMBER,
                                                        p_acumulado     IN NUMBER,
                                                        p_exercicio     IN NUMBER,
                                                        p_cod_plano_cta IN NUMBER,
                                                        p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar a tabela sped_cta_j150 - Demostrac?o do Resultado do Exercicio
   --
   -- Historicos
   --
   -- Data    Autor    Observac?es
   --

-- Calcula totais para as contas do DRE Analiticas
   cursor cont021_analit(p_cod_empresa   IN NUMBER, p_exercicio IN NUMBER,
                         p_mes_01        IN NUMBER, p_mes_02    IN NUMBER,
                         p_cod_plano_cta IN NUMBER, p_acumulado IN NUMBER) IS
      select contab.conta_dre,
             contab.descricao_conta,
             contab.nivel_conta,
             contab.ordem_impressao,
             sum(contab.saldo_periodo) SALDO_PERIODO,
             decode(sign(sum(contab.saldo_periodo)),1,'D','R') INDICADOR,
             conta_mae_dre
      from (select cont_021.conta_dre,
                   cont_021.descricao_conta,
                   cont_021.nivel_conta,
                   cont_021.ordem_impressao,
                   sum(decode(cont_600.debito_credito, 'D', cont_600.valor_lancto, cont_600.valor_lancto * (-1.0))) SALDO_PERIODO,
                   decode(sign(sum(decode(cont_600.debito_credito,'D',cont_600.valor_lancto,cont_600.valor_lancto * -1))),1,'D','R') INDICADOR,
                   cont_021.conta_mae_dre
            from cont_600, cont_022, cont_021
            where cont_600.cod_empresa   = p_cod_empresa
              and cont_600.exercicio     = p_exercicio
              and cont_600.periodo between p_mes_01 and p_mes_02    -- periodo escolhido
             
              and cont_600.origem       <> 20
              and cont_021.cod_plano_cta = p_cod_plano_cta
              and cont_021.cod_plano_cta = cont_022.cod_plano_cta
              and cont_021.conta_dre     = cont_022.conta_dre
              and cont_021.tipo_conta    = 1
              and cont_022.conta_analit_ctb = cont_600.conta_reduzida
              and (cont_022.centro_custo    = cont_600.centro_custo or cont_022.centro_custo = 0)
            group by cont_021.conta_dre,
                     cont_021.descricao_conta,
                     cont_021.nivel_conta,
                     cont_021.ordem_impressao,
                     cont_021.conta_mae_dre
            UNION ALL
            select cont_021.conta_dre,
                   cont_021.descricao_conta,
                   cont_021.nivel_conta,
                   cont_021.ordem_impressao,
                   sum(cont_610.saldo_periodo) SALDO_PERIODO,
                   decode(sign(sum(cont_610.saldo_periodo)),1,'D','R') INDICADOR,
                   cont_021.conta_mae_dre
            from cont_610, cont_022, cont_021
            where cont_610.cod_empresa   = p_cod_empresa
              and cont_610.exercicio     = p_exercicio
              and cont_610.periodo       = 0    -- periodo escolhido
              and (p_acumulado            = 2 and p_tipo_dre = 1)
              and cont_021.cod_plano_cta = p_cod_plano_cta
              and cont_021.cod_plano_cta = cont_022.cod_plano_cta
              and cont_021.conta_dre     = cont_022.conta_dre
              and cont_021.tipo_conta    = 1
              and cont_022.conta_analit_ctb = cont_610.conta_reduzida
            group by cont_021.conta_dre,
                     cont_021.descricao_conta,
                     cont_021.nivel_conta,
                     cont_021.ordem_impressao,
                     cont_021.conta_mae_dre) contab
      group by contab.conta_dre,
               contab.descricao_conta,
               contab.nivel_conta,
               contab.ordem_impressao,
               contab.conta_mae_dre;

   cursor cont021_analit_saldo(p_cod_empresa NUMBER, p_exercicio NUMBER, p_cod_plano_cta NUMBER, p_cod_conta_dre VARCHAR2) IS
      select cont_021.cod_plano_cta,
       cont_021.conta_dre,
       cont_021.descricao_conta,
       cont_021.nivel_conta,
       cont_021.tipo_conta,
       cont_021.ordem_impressao,
       cont_022.conta_analit_dre
      from cont_021, cont_022
      where cont_021.cod_plano_cta = p_cod_plano_cta
        and cont_022.cod_plano_cta = cont_021.cod_plano_cta
        and cont_022.conta_dre     = cont_021.conta_dre
        and cont_021.tipo_conta    = 2
        and cont_021.conta_dre = p_cod_conta_dre
      group by cont_021.cod_plano_cta,
             cont_021.conta_dre,
             cont_021.descricao_conta,
             cont_021.nivel_conta,
             cont_021.tipo_conta,
             cont_021.ordem_impressao,
             cont_022.conta_analit_dre
      order by cont_021.nivel_conta desc , cont_021.ordem_impressao;

  -- Calcula totais para as contas do DRE Sinteticas
   cursor cont021_sintetica(p_cod_empresa NUMBER, p_exercicio NUMBER, p_cod_plano_cta NUMBER) IS
      select cont_021.cod_plano_cta,
             cont_021.conta_dre,
             cont_021.descricao_conta,
             cont_021.nivel_conta,
             cont_021.tipo_conta,
             cont_021.ordem_impressao,
             cont_021.conta_mae_dre
      from cont_021, cont_022
      where cont_021.cod_plano_cta = p_cod_plano_cta
        and cont_022.cod_plano_cta = cont_021.cod_plano_cta
        and cont_022.conta_dre     = cont_021.conta_dre
        and cont_021.tipo_conta    = 2
      group by cont_021.cod_plano_cta,
             cont_021.conta_dre,
             cont_021.descricao_conta,
             cont_021.nivel_conta,
             cont_021.tipo_conta,
             cont_021.ordem_impressao,
             cont_021.conta_mae_dre
      order by cont_021.nivel_conta desc , cont_021.ordem_impressao;

--
   v_erro                EXCEPTION;
   v_ind_nat             varchar2(1);
   ind_dc_cta            varchar2(1);
   --v_ind_nat_agrutinacao varchar2(1);
   v_valor_saldo         number(15,2);
   v_valor_saldo_p       number(15,2);
   v_saldo               number(15,2);
   p_mes_01              number(2);
   p_mes_02              number(2);
   v_val_saldo_per_ant   cont_610.saldo_periodo%type;
   v_ind_saldo_per_ant   varchar2(1);
   v_ind_grp_dre         varchar2(1);
   v_ind_cod_agl         varchar2(1);
   v_per_dt_ini             date;
   v_exercicio_ant       number;
   v_saldo_final_ant_p   number(15,2);
   v_total_saldo_ant     number;
   v_cod_plano_cta_ant   number;
   v_mes_01_ant          number;
   v_mes_02_ant          number;
   v_periodo_ant         sped_ctb_j100.periodo%type;
   v_tmp                 number;
   
begin

   if p_tipo_dre = 1
   then
      p_mes_01 := 1;
      p_mes_02 := 12;
   elsif p_tipo_dre = 2 and p_periodo = 1
   then
      p_mes_01 := 1;
      p_mes_02 := 3;
   elsif p_tipo_dre = 2 and p_periodo = 2
   then
      p_mes_01 := 4;
      p_mes_02 := 6 ;

   elsif p_tipo_dre = 2 and p_periodo = 3
   then
      p_mes_01 := 7;
      p_mes_02 := 9;

   elsif p_tipo_dre = 2 and p_periodo = 4
   then
      p_mes_01 := 10;
      p_mes_02 := 12;

   elsif p_tipo_dre = 3 and p_acumulado = 1
   then
      p_mes_01 := p_periodo;
      p_mes_02 := p_periodo;

   elsif p_tipo_dre = 3 and p_acumulado = 2
   then
      p_mes_01     := 1;
      p_mes_02     := p_periodo;

   end if;

   p_des_erro       := NULL;

   for reg_cont021_analit in cont021_analit(p_cod_empresa, p_exercicio, p_mes_01, p_mes_02, p_cod_plano_cta, p_acumulado)
   loop
      if reg_cont021_analit.saldo_periodo < 0.00
      then
         v_valor_saldo := reg_cont021_analit.saldo_periodo * (-1.0);
      else
         v_valor_saldo := reg_cont021_analit.saldo_periodo;
      end if;

      if reg_cont021_analit.indicador = 'D'
      then
         ind_dc_cta := 'D';
      else
         ind_dc_cta := 'C';
      end if;

      /**verifica periodo anterior**/

      v_val_saldo_per_ant := null;
      v_ind_saldo_per_ant := null;

      v_ind_cod_agl := 'D';


      
      -- Com o exercicio atual, encontramos a data inicial do mesmo.
      begin
         select c.per_inicial
         into v_per_dt_ini
         from cont_500 c
         where c.cod_empresa = p_cod_empresa
           and c.exercicio   = p_exercicio;
      exception when no_data_found then
         v_per_dt_ini := null; 
      end;
      v_val_saldo_per_ant := 0;
      v_ind_saldo_per_ant := 'D';

      if (v_per_dt_ini is not null) then
            

         -- Tendo a data inicial, encontraremos o exercicio anterior a esta data
         select nvl(max(c.exercicio), 0) exercicio_ant, nvl(max(c.cod_plano_cta), 0) into v_exercicio_ant, v_cod_plano_cta_ant from cont_500 c
         where c.cod_empresa = p_cod_empresa
         and c.per_inicial < v_per_dt_ini;
      
         /*BALENA*/
         v_mes_01_ant := p_mes_01;
         v_mes_02_ant := p_mes_02;
         
         if p_tipo_dre = 2 and p_periodo = 1
         then
            v_mes_01_ant := 10;
            v_mes_02_ant := 12;
         elsif p_tipo_dre = 2 and p_periodo = 2
         then
            v_mes_01_ant := 1;
            v_mes_02_ant := 3;
            v_cod_plano_cta_ant := p_cod_plano_cta;
            v_exercicio_ant := p_exercicio;
         elsif p_tipo_dre = 2 and p_periodo = 3
         then
            v_mes_01_ant := 4;
            v_mes_02_ant := 6;
            v_cod_plano_cta_ant := p_cod_plano_cta;
            v_exercicio_ant := p_exercicio;
         elsif p_tipo_dre = 2 and p_periodo = 4
         then
            v_mes_01_ant := 7;
            v_mes_02_ant := 9;
            v_cod_plano_cta_ant := p_cod_plano_cta;
            v_exercicio_ant := p_exercicio;
         end if;
    
         -- Tendo o exercicio anterior, encontraremos o saldo anterior.
         begin
            select 
             nvl(sum(decode(cont_600.debito_credito, 'D', cont_600.valor_lancto, cont_600.valor_lancto * (-1.0))), 0) SALDO_PERIODO_ant,
             nvl(decode(sign(sum(decode(cont_600.debito_credito,'D',cont_600.valor_lancto,cont_600.valor_lancto * -1))),1,'D','R'), 'D') INDICADOR_ant           
            into v_val_saldo_per_ant, v_ind_saldo_per_ant
            from cont_600, cont_022, cont_021
            where cont_600.cod_empresa   = p_cod_empresa
              and cont_600.exercicio     = v_exercicio_ant
              and cont_600.periodo between v_mes_01_ant and v_mes_02_ant    -- periodo escolhido
              and cont_600.origem       <> 20
              and cont_021.cod_plano_cta = v_cod_plano_cta_ant
              and cont_021.cod_plano_cta = cont_022.cod_plano_cta
              and cont_021.conta_dre     = cont_022.conta_dre
              and cont_021.tipo_conta    = 1
              and cont_022.conta_analit_ctb = cont_600.conta_reduzida
              and cont_021.conta_dre = reg_cont021_analit.conta_dre
              and (cont_022.centro_custo    = cont_600.centro_custo or cont_022.centro_custo = 0);
         end;
      end if;
      
      if v_val_saldo_per_ant <= 0
      then
         v_val_saldo_per_ant := v_val_saldo_per_ant * (-1);
      end if;
      
       if v_ind_saldo_per_ant != 'D'
      then
         v_ind_saldo_per_ant := 'C';
      end if;
      
      begin

         -- contas analiticas

         insert into sped_ctb_j100
           (cod_empresa,                      exercicio,
            tipo_demonstracao,

            cod_aglutinacao,                  nivel_aglutinacao,
            descricao_aglutinacao,            saldo_final,
            ind_nat_agrutinacao,              sequencia_dre,
            nivel_dre,                        cod_filial,
            tipo_dre,                         periodo,
            acumulado,                        saldo_per_ant,
            ind_saldo_per_ant,                ind_cod_agl,
            cod_agl_sup,                      ind_dc_cta,
            ind_grp_dre)
         values (
            p_cod_empresa,                      p_exercicio,
            2,
            reg_cont021_analit.conta_dre,       reg_cont021_analit.nivel_conta,
            reg_cont021_analit.descricao_conta, v_valor_saldo,
            reg_cont021_analit.indicador,       reg_cont021_analit.ordem_impressao,
            reg_cont021_analit.nivel_conta,     p_cod_filial,
            p_tipo_dre,                         p_periodo,
            p_acumulado,                        v_val_saldo_per_ant,
            v_ind_saldo_per_ant,                v_ind_cod_agl,
            reg_cont021_analit.conta_mae_dre,   ind_dc_cta,
            reg_cont021_analit.indicador);

      exception
         when others then
            p_des_erro := 'Erro na inclus?o da tabela sped_ctb_j100 ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;
      commit;
   end loop;

   for reg_cont021_sintetica in cont021_sintetica(p_cod_empresa, p_exercicio, p_cod_plano_cta)
   loop

      v_valor_saldo := 0;
      v_total_saldo_ant := 0;
      
      for reg_cont021_analit_saldo in cont021_analit_saldo(p_cod_empresa, p_exercicio, p_cod_plano_cta,reg_cont021_sintetica.conta_dre)
      loop

        v_saldo := 0.00;

        if v_saldo is null or v_saldo = 0
        then
          begin
            select nvl(sum(decode(sped_ctb_j100.ind_nat_agrutinacao,'D',sped_ctb_j100.saldo_final*(-1),
                                                            'N',sped_ctb_j100.saldo_final*(-1),
                                                            sped_ctb_j100.saldo_final)),0),
                  nvl(sum(decode(sped_ctb_j100.ind_saldo_per_ant,'D',sped_ctb_j100.saldo_per_ant*(-1),
                  'N',sped_ctb_j100.saldo_per_ant*(-1),
                  sped_ctb_j100.saldo_per_ant)),0) 
                                                                                            
            into v_valor_saldo_p, v_saldo_final_ant_p
            from sped_ctb_j100
            where sped_ctb_j100.cod_empresa     = p_cod_empresa
              and sped_ctb_j100.exercicio       = p_exercicio
              and sped_ctb_j100.cod_aglutinacao = reg_cont021_analit_saldo.conta_analit_dre
              and sped_ctb_j100.tipo_demonstracao = 2
              and sped_ctb_j100.periodo           = p_periodo;
            --group by sped_ctb_j100.ind_nat_agrutinacao;
          exception
            when no_data_found then
              v_valor_saldo_p := 0;
              v_saldo_final_ant_p := 0;
          end;
        else
          v_valor_saldo_p := v_saldo;
        end if;

        v_valor_saldo := v_valor_saldo + v_valor_saldo_p;
        v_total_saldo_ant := v_total_saldo_ant + v_saldo_final_ant_p;
      end loop;
      
      if v_valor_saldo < 0.00
      then
         v_valor_saldo := v_valor_saldo * (-1.0);
         v_ind_nat       := 'N';
         ind_dc_cta       := 'D';
         v_ind_grp_dre    := 'R';
      else
         v_valor_saldo := v_valor_saldo;
         v_ind_nat       := 'P';
         ind_dc_cta       := 'C';
         v_ind_grp_dre    := 'D';
      end if;
      
      if v_total_saldo_ant < 0.00
      then
         v_total_saldo_ant := v_total_saldo_ant * (-1.0);
         v_ind_saldo_per_ant       := 'D';
      else
         v_ind_saldo_per_ant       := 'C';
      end if;

      v_ind_cod_agl := 'T';
      -- contas sinteticas
      begin

      insert into sped_ctb_j100
        (cod_empresa,                           exercicio,
         tipo_demonstracao,
         cod_aglutinacao,                       nivel_aglutinacao,
         descricao_aglutinacao,                 saldo_final,
         ind_nat_agrutinacao,                   sequencia_dre,
         nivel_dre,                             cod_filial,
         tipo_dre,                              periodo,
         acumulado,                             saldo_per_ant,
         ind_saldo_per_ant,                     ind_cod_agl,
         cod_agl_sup,                           ind_dc_cta,
         ind_grp_dre)
      values (
         p_cod_empresa,                          p_exercicio,
         2,
         reg_cont021_sintetica.conta_dre,        reg_cont021_sintetica.nivel_conta,
         reg_cont021_sintetica.descricao_conta,  v_valor_saldo,
         v_ind_nat,                              reg_cont021_sintetica.ordem_impressao,
         reg_cont021_sintetica.nivel_conta,      p_cod_filial,
         p_tipo_dre,                             p_periodo,
         p_acumulado,                            v_total_saldo_ant,
         v_ind_saldo_per_ant,                    v_ind_cod_agl,
         reg_cont021_sintetica.conta_mae_dre,    ind_dc_cta,
         v_ind_grp_dre);
      exception
        when others then
            p_des_erro := 'Erro na inclus?o da tabela sped_ctb_j100 ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;

      commit;

    end loop;
   
    -- Com o exercicio atual, encontramos a data inicial do mesmo.
    begin
       select c.per_inicial
       into v_per_dt_ini
       from cont_500 c
       where c.cod_empresa = p_cod_empresa
         and c.exercicio   = p_exercicio;
    exception when no_data_found then
       v_per_dt_ini := null; 
    end;

    if (v_per_dt_ini is not null) then

       begin
         delete from sped_ctb_j100_aux
         where sped_ctb_j100_aux.cod_empresa = p_cod_empresa
           and sped_ctb_j100_aux.exercicio = p_exercicio
           --and sped_ctb_j100_aux.tipo_demonstracao = 2
           and sped_ctb_j100_aux.periodo = p_periodo;
         
         insert into sped_ctb_j100_aux
         (cod_empresa, exercicio, tipo_demonstracao, cod_aglutinacao,
            nivel_aglutinacao, descricao_aglutinacao, saldo_final, ind_nat_agrutinacao,
            ind_saldo_aglutinacao, sequencia_dre, nivel_dre, tipo_dre,
            periodo, acumulado, cod_filial, saldo_inicial,
            ind_saldo_inicial, saldo_per_ant, ind_saldo_per_ant, ind_cod_agl,
            cod_agl_sup, ind_dc_cta, ind_grp_dre, centro_custo)
         select cod_empresa, exercicio, tipo_demonstracao, cod_aglutinacao,
            nivel_aglutinacao, descricao_aglutinacao, saldo_final, ind_nat_agrutinacao,
            ind_saldo_aglutinacao, sequencia_dre, nivel_dre, tipo_dre,
            periodo, acumulado, cod_filial, saldo_inicial,
            ind_saldo_inicial, saldo_per_ant, ind_saldo_per_ant, ind_cod_agl,
            cod_agl_sup, ind_dc_cta, ind_grp_dre, centro_custo
         from sped_ctb_j100
         where sped_ctb_j100.cod_empresa = p_cod_empresa
           and sped_ctb_j100.exercicio = p_exercicio
           --and sped_ctb_j100.tipo_demonstracao = 2
           and sped_ctb_j100.periodo = p_periodo;
             
       exception
       when dup_val_on_index then
         v_tmp := 1;
       end;

       -- Tendo a data inicial, encontraremos o exercicio anterior a esta data
       select nvl(max(c.exercicio), 0) exercicio_ant, nvl(max(c.cod_plano_cta), 0) into v_exercicio_ant, v_cod_plano_cta_ant from cont_500 c
       where c.cod_empresa = p_cod_empresa
       and c.per_inicial < v_per_dt_ini;
          
       /*BALENA*/
       v_periodo_ant := p_periodo;
             
       if p_tipo_dre = 2 and p_periodo = 1
       then
          v_periodo_ant := 4;
       elsif p_tipo_dre = 2 and p_periodo = 2
       then
          v_periodo_ant := 1;
          v_exercicio_ant := p_exercicio;
       elsif p_tipo_dre = 2 and p_periodo = 3
       then
          v_periodo_ant := 2;
          v_exercicio_ant := p_exercicio;
       elsif p_tipo_dre = 2 and p_periodo = 4
       then
          v_periodo_ant := 3;
          v_exercicio_ant := p_exercicio;
       end if;
       
       for per_ant in (
         select * from sped_ctb_j100_aux
         where sped_ctb_j100_aux.cod_empresa = p_cod_empresa
           and sped_ctb_j100_aux.exercicio = v_exercicio_ant
           and sped_ctb_j100_aux.tipo_demonstracao = 2
           and sped_ctb_j100_aux.periodo = v_periodo_ant
       )
       loop
         
         update sped_ctb_j100
         set sped_ctb_j100.saldo_per_ant = per_ant.saldo_final,
             sped_ctb_j100.ind_saldo_per_ant = per_ant.ind_dc_cta
         where sped_ctb_j100.cod_empresa = per_ant.cod_empresa
           and sped_ctb_j100.exercicio = p_exercicio
           and sped_ctb_j100.tipo_demonstracao = per_ant.tipo_demonstracao
           and sped_ctb_j100.cod_aglutinacao = per_ant.cod_aglutinacao
           and sped_ctb_j100.periodo = p_periodo
           and sped_ctb_j100.centro_custo = per_ant.centro_custo;
         
       end loop;
    end if;
   
exception
   when V_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_gera_ctb_sped_j150_n ' || Chr(10) || p_des_erro;

   when others then
      p_des_erro := 'Outros erros na procedure inter_pr_gera_ctb_sped_j150_n ' || Chr(10) || SQLERRM;

end inter_pr_gera_ctb_sped_j150_2;
