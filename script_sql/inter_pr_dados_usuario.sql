create or replace procedure inter_pr_dados_usuario (
ws_usuario_rede       out nocopy varchar2,
ws_maquina_rede       out nocopy varchar2,
ws_aplicativo         out nocopy varchar2,
ws_sid                out nocopy number,
ws_usuario_systextil  out nocopy varchar2,
ws_empresa            out nocopy number,
ws_locale_usuario     out nocopy varchar2
) is

client_info varchar2(64);
pos integer;
v_nome_programa varchar2(255);
v_module varchar2(255);
v_usuario_rede_apex varchar(255);
procedure get_data_for_oracle8
is
begin
   begin
      select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20), sid
      into   ws_usuario_rede,     ws_maquina_rede,      ws_aplicativo,        ws_sid
      from sys.gv_$session
      where audsid  = userenv('SESSIONID')
        and inst_id = userenv('INSTANCE')
        and rownum < 2;
   exception when no_data_found then
      ws_usuario_rede := '';
      ws_maquina_rede := '';
      ws_aplicativo   := '';
      ws_sid          := 0;
   end;
end;
begin
   -- DADOS DO LOGIN DO USUARIO
   begin
      ws_usuario_rede := substr(sys_context('USERENV', 'OS_USER'),1,20);

      client_info := sys_context('USERENV', 'CLIENT_INFO');
      if client_info is null
      then
         ws_maquina_rede := sys_context('USERENV', 'HOST');
      else
         begin
            pos := instr(client_info, '@');
            if pos > 1
            then
               -- ws_usuario_rede := substr(client_info, 1, pos-1);
               ws_maquina_rede := substr(client_info, pos+1);
            else
               ws_maquina_rede := client_info;
            end if;
            if ws_maquina_rede = '127.0.0.1'
               or ws_maquina_rede = '0:0:0:0:0:0:0:1'
               or ws_maquina_rede = '::0'
            then
               ws_maquina_rede := sys_context('USERENV', 'HOST');
            end if;
         end;
      end if;
      ws_maquina_rede := substr(ws_maquina_rede,1,40);
      ws_aplicativo   := substr(nvl(sys_context('USERENV', 'MODULE'),''),1,20);
      pos := instr(ws_aplicativo, ' ');
      if pos > 1 -- NOME DO PROGRAMA, ESPACO, ID DO USUARIO
      then       -- OBTER APENAS O NOME DO PROGRAMA
        ws_aplicativo := substr(ws_aplicativo, 1, pos-1);
      end if;
      ws_sid          :=     sys_context('USERENV', 'SID');
      exception when others
      then get_data_for_oracle8();
   end;
   begin
      select hdoc_090.usuario,     hdoc_090.empresa
      into   ws_usuario_systextil, ws_empresa
      from hdoc_090
      where hdoc_090.sid       = ws_sid
        and hdoc_090.instancia = userenv('INSTANCE')
        and rownum       = 1;          
   exception when no_data_found then
      ws_usuario_systextil := '';
      v_nome_programa := '';
   end;

   if client_info is not null then -- ACESSO WEB: USUARIO REDE NAO SIGNIFICA NADA
      inter_pr_split_module(sys_context('USERENV', 'MODULE'), v_nome_programa, ws_empresa, ws_usuario_systextil);
      ws_usuario_rede := substr(ws_usuario_systextil, 1, 20);
   end if;

    v_module := sys_context('USERENV', 'MODULE');

    begin
        if upper(v_module) like '%APEX%' then
            EXECUTE IMMEDIATE 'select V(''APP_USER'') from dual' into v_usuario_rede_apex;
            if trim(v_usuario_rede_apex) is not null and length(trim(v_usuario_rede_apex)) > 1 then
                ws_usuario_rede := v_usuario_rede_apex;
            end if;
        end if;
    exception
        when others then
            v_usuario_rede_apex := '';
    end;

   begin
      select  nvl(hdoc_030.locale,'pt_BR')
      into   ws_locale_usuario
      from hdoc_030
      where hdoc_030.empresa  = ws_empresa
        and hdoc_030.usuario  = ws_usuario_systextil;
   exception when no_data_found then
      ws_locale_usuario := 'pt_BR';
   end;

end inter_pr_dados_usuario;
/
