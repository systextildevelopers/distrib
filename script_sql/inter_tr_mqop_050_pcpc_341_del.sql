create or replace trigger "INTER_TR_MQOP_050_PCPC_341_DEL"
before delete on mqop_050
for each row
begin
  delete from pcpc_341
  where nivel_rot      = :old.nivel_estrutura
    and referencia_rot = :old.grupo_estrutura
    and subgrupo_rot   = :old.subgru_estrutura
    and item_rot       = :old.item_estrutura
    and alt_rot        = :old.numero_alternati
    and rot_rot        = :old.numero_roteiro
    and seq_operacao   = :old.seq_operacao;
end;
/

