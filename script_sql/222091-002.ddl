create table OBRF_063
(
  cfop           VARCHAR2(5) not null,
  descricao      VARCHAR2(400),
  entr_saida     VARCHAR2(1),
  tipo_operacao  VARCHAR2(3),
  descr_operacao VARCHAR2(100)
);

alter table OBRF_063
  add constraint OBRF_063_PK primary key (CFOP);
  
/
exec inter_pr_recompile;
