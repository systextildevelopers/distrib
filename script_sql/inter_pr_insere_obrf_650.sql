create or replace procedure inter_pr_insere_obrf_650 (
  p_mes               in number,   p_ano in number,
  p_cod_empresa       in number,   p_nivel in number,
  p_grupo             in varchar2, p_subgrupo in varchar2,
  p_item              in varchar2, p_min_part  in number,
  p_icms_interno      in number,   p_icms_intesta  in number,
  p_origem            in varchar2, p_participacao in number,
  p_vlr_med_vend      in number,   p_numero_fci in number,
  p_valor_parcela_imp in number) is
begin

  insert into obrf_650
    (mes,               ano,
     cod_empresa,

     nivel,             grupo,
     subgrupo,          item,

     min_part,          icms_interno,
     icms_intesta,      origem,
     participacao,      vlr_med_vend,
     numero_fci,        valor_parcela_imp)
  values
    (p_mes,             p_ano,
     p_cod_empresa,

     p_nivel,            p_grupo,
     p_subgrupo,         p_item,

     p_min_part,         p_icms_interno,
     p_icms_intesta,     p_origem,
     p_participacao,     p_vlr_med_vend,
     p_numero_fci,       p_valor_parcela_imp);
  exception when others
    then raise_application_error(-20000, 'Problema ao inserir dados na tabela obrf_650. Erro banco de dados:  ' || SQLERRM);

end inter_pr_insere_obrf_650;
