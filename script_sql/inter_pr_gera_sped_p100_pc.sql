create or replace procedure inter_pr_gera_sped_p100_pc(p_cod_matriz    IN NUMBER,
                                                       p_dat_inicial   IN DATE,
                                                       p_dat_final     IN DATE,
                                                       p_des_erro      OUT varchar2) is

--and    fatu_050.data_emissao                  between p_dat_inicial and p_dat_final
--and    fatu_050.situacao_nfisc                in (1,2,3,4,6)

CURSOR u_obrf_235 (p_cod_empresa     NUMBER,
                   p_dat_inicial     DATE) IS

   select * from obrf_235
   where obrf_235.cod_empresa = p_cod_empresa
     and obrf_235.mes_apur    = to_char(p_dat_inicial, 'MM')
     and obrf_235.ano_apur    = to_char(p_dat_inicial, 'YYYY')
   order by obrf_235.cod_empresa;

CURSOR u_fatu_500 (p_cod_empresa          NUMBER) IS
   select distinct fatu_500.codigo_empresa, fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2
   from fatu_500
   where fatu_500.codigo_matriz = p_cod_matriz;

    w_erro                    EXCEPTION;
    w_valor_nao_selecionado   fatu_060.valor_contabil%type;
    w_tem_contribuicao        boolean;
    w_vl_rec_tot_estq         number;
    w_vl_rec_ativ_estab       number;
    w_cod_empresa             obrf_235.cod_empresa%type;


BEGIN
   p_des_erro          := NULL;
   w_tem_contribuicao  := false;
   w_vl_rec_tot_estq   := 0.00;
   w_vl_rec_ativ_estab := 0.00;
   w_cod_empresa       := 0;

   FOR fatu_500 IN u_fatu_500 (p_cod_matriz)
   LOOP

      w_tem_contribuicao  := false;
      w_vl_rec_tot_estq   := 0.00;
      w_vl_rec_ativ_estab := 0.00;


      FOR obrf_235 IN u_obrf_235 (fatu_500.codigo_empresa, p_dat_inicial)
      LOOP
         if w_cod_empresa <> fatu_500.codigo_empresa
         then
            w_vl_rec_tot_estq   := w_vl_rec_tot_estq   + obrf_235.vl_rec_tot_estq;
            w_cod_empresa       := fatu_500.codigo_empresa;
         end if;
        
         w_tem_contribuicao := true;
         
         w_vl_rec_ativ_estab := w_vl_rec_ativ_estab + obrf_235.vl_rec_ativ_estab;

         begin
            insert into sped_pc_p100
               (cod_matriz
               ,cod_empresa
               ,num_cnpj9_empr
               ,num_cnpj4_empr
               ,num_cnpj2_empr
               ,dt_ini
               ,dt_fin
               ,vl_rec_tot_estq
               ,cod_ativ_econ
               ,vl_rec_ativ_estab
               ,vl_exc
               ,vl_bc_cont
               ,aliq_cont
               ,vl_cont_apu
               ,cod_cta
               ,info_compl
               ,cod_rec)

               VALUES
               (p_cod_matriz
               ,fatu_500.codigo_empresa
               ,fatu_500.cgc_9
               ,fatu_500.cgc_4
               ,fatu_500.cgc_2
               ,p_dat_inicial
               ,p_dat_final
               ,obrf_235.vl_rec_tot_estq
               ,obrf_235.cod_ativ_econ
               ,obrf_235.vl_rec_ativ_estab
               ,obrf_235.vl_exc
               ,obrf_235.vl_bc_cont
               ,obrf_235.aliq_cont
               ,obrf_235.vl_cont_apu
               ,obrf_235.cod_cta
               ,obrf_235.info_compl
               ,obrf_235.cod_rec);
         exception
            when others then
                p_des_erro := 'Erro na inclus�o da tabela sped_pc_p100 - Saidas ' || Chr(10) || SQLERRM;
                RAISE W_ERRO;
         end;

         commit;

      END LOOP;

      if w_tem_contribuicao
      then
         w_valor_nao_selecionado := w_vl_rec_tot_estq - w_vl_rec_ativ_estab;

         begin
            insert into sped_pc_0145
               (cod_empresa,
                cod_matriz,
                num_cnpj9_empr,
                num_cnpj4_empr,
                num_cnpj2_empr,
                vl_rec_tot,
                vl_rec_ativ,
                vl_rec_demais_ativ)
            values
               (fatu_500.codigo_empresa,
                p_cod_matriz,
                fatu_500.cgc_9,
                fatu_500.cgc_4,
                fatu_500.cgc_2,
                w_vl_rec_tot_estq,
                w_vl_rec_ativ_estab,
                w_valor_nao_selecionado);
         exception
            when others then
                p_des_erro := 'Erro na inclus�o da tabela sped_pc_p100 - Saidas ' || Chr(10) || SQLERRM;
                RAISE W_ERRO;
         end;

      end if;

   END LOOP;

END inter_pr_gera_sped_p100_pc;
/
exec inter_pr_recompile;

