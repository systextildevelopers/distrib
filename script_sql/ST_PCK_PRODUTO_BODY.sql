create or replace package body ST_PCK_PRODUTO is

    FUNCTION get_dados_produto_completo(p_nivel varchar2, p_grupo varchar2, p_subgrupo varchar2, p_item varchar2, p_msg_erro in out varchar2) return t_dados_basi_010
    AS
        v_dados_basi_010 t_dados_basi_010;
    BEGIN
        begin
            SELECT *
            BULK COLLECT
            INTO v_dados_basi_010
            FROM basi_010
            WHERE nivel_estrutura = p_nivel
            and grupo_estrutura = p_grupo
            and subgru_estrutura = p_subgrupo
            and item_estrutura = p_item;
        exception when others then
            p_msg_erro := 'N?#o foi poss?-vel buscar os dados do produto!. p_nivel: ' || p_nivel || ' p_grupo: ' || p_grupo || ' p_subgrupo: ' || p_subgrupo || ' p_item: ' || p_item;
            return null;
        end;

        return v_dados_basi_010;
    END;

    FUNCTION get_dados_tamanho(p_nivel varchar2, p_grupo varchar2, p_subgrupo varchar2, p_msg_erro in out varchar2) return t_dados_basi_020
    AS
        v_dados_basi_020 t_dados_basi_020;
    BEGIN
        begin
            SELECT *
            BULK COLLECT
            INTO v_dados_basi_020
            FROM basi_020
            WHERE BASI030_NIVEL030 = p_nivel
            AND BASI030_REFERENC = p_grupo
            AND  tamanho_ref = p_subgrupo;

        exception when others then
            p_msg_erro := 'N?#o foi poss?-vel buscar os dados do produto!. p_nivel: ' || p_nivel || ' p_grupo: ' || p_grupo || ' p_subgrupo: ' || p_subgrupo;
            return null;
        end;

        return v_dados_basi_020;

    END;

    FUNCTION get_dados_ordem_tamanho(p_tamanho varchar2, p_msg_erro in out varchar2) return t_dados_basi_220
    AS
        v_dados_basi_220 t_dados_basi_220;
    BEGIN
        begin
            SELECT *
            BULK COLLECT
            INTO v_dados_basi_220
            FROM basi_220
            WHERE tamanho_ref = p_tamanho;

        exception when others then
            p_msg_erro := 'Nao foi possivel buscar os dados da ordem tamanho do produto!. p_tamanho: ' || p_tamanho;
            return null;
        end;

        return v_dados_basi_220;

    END;


    FUNCTION get_dados_referencia(p_nivel varchar2, p_grupo varchar2, p_msg_erro in out varchar2) return t_dados_basi_030
    AS
        v_dados_basi_030 t_dados_basi_030;
    BEGIN
        begin
            SELECT *
            BULK COLLECT
            INTO v_dados_basi_030
            FROM basi_030
            WHERE NIVEL_ESTRUTURA = p_nivel
            AND referencia = p_grupo;
        exception when others then
            p_msg_erro := 'N?#o foi poss?-vel buscar os dados do produto!. p_nivel: ' || p_nivel || ' p_grupo: ' || p_grupo;
            return null;
        end;

        return v_dados_basi_030;

   END;

  function get_descricao_basi063 (p_idioma number, p_ncm varchar2) return varchar2
  is
    v_descricao basi_063.descricao%type;
  begin
    begin
    select b.descricao into v_descricao from basi_063 b, basi_165 p
    where b.cod_pais = p.codigo_pais
    and replace(b.classific_fiscal,'.') = replace(p_ncm,'.')
    and p.idioma = p_idioma
    and trim(b.descricao) is not null;
    EXCEPTION
        WHEN no_data_found THEN
            v_descricao := NULL;
    end;

    return v_descricao;

  end get_descricao_basi063;

  FUNCTION get_dados_produto_exportacao(p_msg_erro in out varchar2) return t_dados_basi_010
    AS
        v_dados_basi_010 t_dados_basi_010;
    BEGIN
        begin
            SELECT *
            BULK COLLECT
            INTO v_dados_basi_010
            FROM basi_010
            WHERE IND_INTEG_PRODUTO_NARW = 1;
        exception when others then
            p_msg_erro := 'Nao foi possivel buscar os dados do produto!';
            return null;
        end;

        return v_dados_basi_010;
    END;


end ST_PCK_PRODUTO;
/
