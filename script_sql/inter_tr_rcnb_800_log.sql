create or replace trigger INTER_TR_RCNB_800_LOG
  after insert
  or delete
  or update of
    mes_classe, ano_classe, seq_classe, classe_valor,
    valor_inicial, valor_final, situacao
  on rcnb_800
for each row

declare
  ws_usuario_rede           varchar2(20);
  ws_maquina_rede           varchar2(40);
  ws_aplicativo             varchar2(20);
  ws_sid                    number(9);
  ws_empresa                number(3);
  ws_usuario_systextil      varchar2(250);
  ws_locale_usuario         varchar2(5);
  operacao                  varchar2(2000);
  v_nome_programa           varchar2(20);

begin
-- Dados do usuario logado
  inter_pr_dados_usuario (ws_usuario_rede,      ws_maquina_rede, ws_aplicativo,     ws_sid, 
                          ws_usuario_systextil, ws_empresa,      ws_locale_usuario); 

  v_nome_programa := inter_fn_nome_programa(ws_sid);  
    
 
  if inserting 
  then 
    begin 
      insert into rcnb_800_log (
        data_ocorrencia,
        tipo_ocorrencia,
        usuario_rede,
        maquina_rede,
        aplicativo,
        empresa,
        usuario_systextil,
        nome_programa,
        mes_classe_OLD,   /*2*/ 
        mes_classe_NEW,   /*3*/ 
        ano_classe_OLD,   /*4*/ 
        ano_classe_NEW,   /*5*/ 
        seq_classe_OLD,   /*6*/ 
        seq_classe_NEW,   /*7*/ 
        classe_valor_OLD,   /*8*/ 
        classe_valor_NEW,   /*9*/ 
        valor_inicial_OLD,   /*10*/ 
        valor_inicial_NEW,   /*11*/ 
        valor_final_OLD,   /*12*/ 
        valor_final_NEW,   /*13*/ 
        situacao_OLD,   /*14*/ 
        situacao_NEW    /*15*/
      ) values (    
        sysdate, /*1*/
        'I', /*o*/
        ws_usuario_rede,/*3*/ 
        ws_maquina_rede, /*4*/
        ws_aplicativo, /*5*/
        ws_empresa,
        ws_usuario_systextil,/*6*/ 
        v_nome_programa, /*7*/
        0,/*2*/
        :new.mes_classe, /*3*/   
        0,/*4*/
        :new.ano_classe, /*5*/   
        0,/*6*/
        :new.seq_classe, /*7*/   
        0,/*8*/
        :new.classe_valor, /*9*/   
        0,/*10*/
        :new.valor_inicial, /*11*/   
        0,/*12*/
        :new.valor_final, /*13*/   
        '',/*14*/
        :new.situacao /*15*/   
      );    
    end;    
  end if;
  
  if updating 
  then 
    begin 
      insert into rcnb_800_log (
        data_ocorrencia,
        tipo_ocorrencia,
        usuario_rede,
        maquina_rede,
        aplicativo,
        empresa,
        usuario_systextil,
        nome_programa,
        mes_classe_OLD, /*2*/   
        mes_classe_NEW, /*3*/   
        ano_classe_OLD, /*4*/   
        ano_classe_NEW, /*5*/   
        seq_classe_OLD, /*6*/   
        seq_classe_NEW, /*7*/   
        classe_valor_OLD, /*8*/   
        classe_valor_NEW, /*9*/   
        valor_inicial_OLD, /*10*/   
        valor_inicial_NEW, /*11*/   
        valor_final_OLD, /*12*/   
        valor_final_NEW, /*13*/   
        situacao_OLD, /*14*/   
        situacao_NEW  /*15*/  
      ) values (    
        sysdate, /*1*/
        'A', /*o*/
        ws_usuario_rede,/*3*/ 
        ws_maquina_rede, /*4*/
        ws_aplicativo, /*5*/
        ws_empresa,
        ws_usuario_systextil,/*6*/ 
        v_nome_programa, /*7*/
        :old.mes_classe,  /*2*/  
        :new.mes_classe, /*3*/   
        :old.ano_classe,  /*4*/  
        :new.ano_classe, /*5*/   
        :old.seq_classe,  /*6*/  
        :new.seq_classe, /*7*/   
        :old.classe_valor,  /*8*/  
        :new.classe_valor, /*9*/   
        :old.valor_inicial,  /*10*/  
        :new.valor_inicial, /*11*/   
        :old.valor_final,  /*12*/  
        :new.valor_final, /*13*/   
        :old.situacao,  /*14*/  
        :new.situacao  /*15*/  
      );    
    end;    
  end if;    
  
  if deleting 
  then 
    begin 
      insert into rcnb_800_log (
        data_ocorrencia,
        tipo_ocorrencia,
        usuario_rede,
        maquina_rede,
        aplicativo,
        empresa,
        usuario_systextil,
        nome_programa,
        mes_classe_OLD, /*2*/   
        mes_classe_NEW, /*3*/   
        ano_classe_OLD, /*4*/   
        ano_classe_NEW, /*5*/   
        seq_classe_OLD, /*6*/   
        seq_classe_NEW, /*7*/   
        classe_valor_OLD, /*8*/   
        classe_valor_NEW, /*9*/   
        valor_inicial_OLD, /*10*/   
        valor_inicial_NEW, /*11*/   
        valor_final_OLD, /*12*/   
        valor_final_NEW, /*13*/   
        situacao_OLD, /*14*/   
        situacao_NEW /*15*/   
      ) values (    
        sysdate, /*1*/
        'D', /*o*/
        ws_usuario_rede,/*3*/ 
        ws_maquina_rede, /*4*/
        ws_aplicativo, /*5*/
        ws_empresa,
        ws_usuario_systextil,/*6*/ 
        v_nome_programa, /*7*/
        :old.mes_classe, /*2*/   
        0, /*3*/
        :old.ano_classe, /*4*/   
        0, /*5*/
        :old.seq_classe, /*6*/   
        0, /*7*/
        :old.classe_valor, /*8*/   
        0, /*9*/
        :old.valor_inicial, /*10*/   
        0, /*11*/
        :old.valor_final, /*12*/   
        0, /*13*/
        :old.situacao, /*14*/   
        '' /*15*/
      );    
    end;    
  end if;    
end INTER_TR_RCNB_800_LOG;
 

/

exec inter_pr_recompile;
