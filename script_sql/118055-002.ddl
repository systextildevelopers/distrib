update obrf_601 set instrucao = 'É necessário validar o XML dessa nota no portal https://www.sefaz.rs.gov.br/nfe/nfe-val.aspx. Analisar a mensagem do validador, e fazer as correções necessárias.' where cod_status = 225;
update obrf_601 set instrucao = 'O tamanho da nota está superior a 500kb. É necessário cancelar a nota e dividir o seu faturamento em duas ou mais notas.' where cod_status = 214;
update obrf_601 set instrucao = 'Essa rejeição ocorre quando algum dos produtos existentes da Nota Fiscal possui CST 60 ou CSOSN 500 e não foram enviadas as informações relativas ao ICMS ST pago anteriormente, também conhecido como ICMS retido por Substituição Tributária.' where cod_status = 938;
update obrf_601 set instrucao = 'É necessário validar o XML dessa nota no portal https://www.sefaz.rs.gov.br/nfe/nfe-val.aspx. Analisar a mensagem do validador, e fazer as correções necessárias.' where cod_status = 215;
update obrf_601 set instrucao = 'Houve algum problema com o CNPJ do destinatário, consulte esse CNPJ junto ao sintegra para maiores informações.' where cod_status = 246;
update obrf_601 set instrucao = 'Verificar o número de inscrição do suframa do cliente se está correto, caso tenha dúvidas pode ser feita uma consulta da situação cadastral da inscrição no site: http://site.suframa.gov.br' where cod_status = 235;
update obrf_601 set instrucao = 'A inscrição estadual do destinatário da nota está inválida, deve consultar o CNPJ do cliente no sintegra: http://www.sintegra.gov.br/ para corrigir o cadastro do mesmo.' where cod_status = 210;
update obrf_601 set instrucao = 'Uma nota fiscal só pode ser autorizada se a data corrente não for 30 dias superior a emissão da nota. A operação necessária é inutilizar a nota fiscal e fazer uma nova emissão.' where cod_status = 228;

commit;

/
