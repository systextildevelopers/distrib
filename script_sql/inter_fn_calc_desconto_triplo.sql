
  CREATE OR REPLACE FUNCTION "INTER_FN_CALC_DESCONTO_TRIPLO" (valor number, desconto1 number, desconto2 number, desconto3 number)
return number is
	v_retorno number := valor;
begin
	v_retorno := v_retorno - (v_retorno * desconto1 / 100.00);
	v_retorno := v_retorno - (v_retorno * desconto2 / 100.00);
	v_retorno := v_retorno - (v_retorno * desconto3 / 100.00);

	return(v_retorno);
end inter_fn_calc_desconto_triplo;
 

/

exec inter_pr_recompile;

