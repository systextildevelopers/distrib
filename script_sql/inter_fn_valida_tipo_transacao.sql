CREATE OR REPLACE FUNCTION inter_fn_valida_tipo_transacao(p_codigo_transacao NUMBER) RETURN BOOLEAN IS
    v_tipo_transacao estq_005.tipo_transacao%TYPE;
BEGIN
    SELECT tipo_transacao INTO v_tipo_transacao
    FROM estq_005
    WHERE codigo_transacao = p_codigo_transacao
    AND estq_005.atualiza_estoque = 1
    AND tipo_transacao not in ('R','I', 'P', 'E', 'S');

    RETURN TRUE;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN FALSE;
END;
/
