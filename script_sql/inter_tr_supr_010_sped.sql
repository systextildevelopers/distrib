
  CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_010_SPED" 
BEFORE  UPDATE

of nome_fornecedor,
   cod_cidade,
   inscr_est_forne,
   endereco_forne,
   numero_imovel,
   complemento,
   bairro

on supr_010
for each row

begin

  if :new.nome_fornecedor <> :old.nome_fornecedor
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'supr_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '3',
      :old.nome_fornecedor,
      :new.fornecedor9,
      :new.fornecedor4,
      :new.fornecedor2);
  end if;

  if :new.cod_cidade <> :old.cod_cidade
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'supr_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '4',
      :old.cod_cidade,
      :new.fornecedor9,
      :new.fornecedor4,
      :new.fornecedor2);
  end if;

  if :new.inscr_est_forne <> :old.inscr_est_forne
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'supr_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '7',
      :old.inscr_est_forne,
      :new.fornecedor9,
      :new.fornecedor4,
      :new.fornecedor2);
  end if;

  if :new.endereco_forne <> :old.endereco_forne
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'supr_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '10',
      :old.endereco_forne,
      :new.fornecedor9,
      :new.fornecedor4,
      :new.fornecedor2);
  end if;

  if :new.numero_imovel <> :old.numero_imovel
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'supr_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '11',
      :old.numero_imovel,
      :new.fornecedor9,
      :new.fornecedor4,
      :new.fornecedor2);
  end if;
  if :new.complemento <> :old.complemento
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'supr_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '12',
      :old.complemento,
      :new.fornecedor9,
      :new.fornecedor4,
      :new.fornecedor2);
  end if;
  if :new.bairro <> :old.bairro
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'supr_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '13',
      :old.bairro,
      :new.fornecedor9,
      :new.fornecedor4,
      :new.fornecedor2);
  end if;

end;
-- ALTER TRIGGER "INTER_TR_SUPR_010_SPED" ENABLE
 

/

exec inter_pr_recompile;

