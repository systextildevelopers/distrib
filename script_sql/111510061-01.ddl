alter table fatu_050
drop constraint CK_FATU_050_TIPO_FRETE; 

alter table fatu_050 
add constraint CK_FATU_050_TIPO_FRETE CHECK (tipo_frete in (1,2,3,4,6,7,9)) novalidate;

exec inter_pr_recompile;
