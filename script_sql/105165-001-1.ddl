alter table fatu_504 add presumido_reduzido numeric(1) default 0;

alter table obrf_050
add (estorno_cred_presumido number(15,2) default 0.0,
     segreg_cred_presumido_ent number(15,2) default 0.0,
     segreg_cred_presumido_sai number(15,2) default 0.0,
     segreg_deb_presumido_ent number(15,2) default 0.0,
     val_cred_difal_13_045 number(15,2) default 0.0);
          
comment on column obrf_050.estorno_cred_presumido is 'Estorno de Cr�dito da Entrada em Decorr�ncia da Utiliza��o de Cr�dito Presumido';
comment on column obrf_050.segreg_cred_presumido_ent is 'Segrega��o do Cr�dito Presumido Utilizado em Substitui��o aos Cr�ditos pelas Entradas';
comment on column obrf_050.segreg_cred_presumido_sai is 'Segrega��o do Cr�dito Decorrente do Pagamento Antecipado do ICMS Devido na Sa�da Subsequente � Importa��o, com Utiliza��o de Cr�dito Presumido';
comment on column obrf_050.segreg_deb_presumido_ent is 'Segrega��o dos D�bitos Relativos �s Sa�das com Cr�dito Presumido em Substitui��o aos Cr�ditos pelas Entradas';
comment on column obrf_050.val_cred_difal_13_045 is 'Cr�dito da Diferen�a de Al�quota de Opera��o ou Presta��o a Consumidor Final de Outro Estado';

/
exec inter_pr_recompile;
