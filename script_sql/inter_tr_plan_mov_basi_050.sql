
  CREATE OR REPLACE TRIGGER "INTER_TR_PLAN_MOV_BASI_050" 
   after insert
      or delete
      or update of NIVEL_COMP,        GRUPO_COMP,
                   SUB_COMP,          ITEM_COMP,
                   ALTERNATIVA_COMP,  CONS_UNID_MED_GENERICA,
                   CONSUMO, ESTAGIO
   on basi_050
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_executa_trigger         number(1);
   v_nivel                   varchar2(1);
   v_grupo                   varchar2(5);
   v_subgrupo                varchar2(3);
   v_item                    varchar2(6);
   v_alternativa             number(2);

   v_pedido                  number;
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_executa_trigger := 1;

   if inserting
   then
      v_nivel       := :new.nivel_item;
      v_grupo       := :new.grupo_item;
      v_subgrupo    := :new.sub_item;
      v_item        := :new.item_item;
      v_alternativa := :new.alternativa_item;
   else
      v_nivel       := :old.nivel_item;
      v_grupo       := :old.grupo_item;
      v_subgrupo    := :old.sub_item;
      v_item        := :old.item_item;
      v_alternativa := :old.alternativa_item;
   end if;

   if v_executa_trigger = 1
   then
      for reg_o_planej in(select tmrp_virtual.ordem_planejamento,
                                 tmrp_virtual.pedido_venda,
                                 tmrp_virtual.pedido_reserva,
                                 tmrp_virtual.nivel_produto_origem,
                                 tmrp_virtual.grupo_produto_origem
                          from (
                          select tmrp_610.ordem_planejamento,
                                 tmrp_625.pedido_venda,
                                 tmrp_625.pedido_reserva,
                                 '0'     as nivel_produto_origem,
                                 '00000' as grupo_produto_origem
                          from tmrp_625, tmrp_620, tmrp_610
                          where   tmrp_620.ordem_planejamento       = tmrp_625.ordem_planejamento
                            and   tmrp_620.pedido_venda             = tmrp_625.pedido_venda
                            and   tmrp_620.pedido_reserva           = tmrp_625.pedido_reserva
                            and   tmrp_620.nivel_produto            = tmrp_625.nivel_produto
                            and   tmrp_620.grupo_produto            = tmrp_625.grupo_produto
                            and   tmrp_620.subgrupo_produto         = tmrp_625.subgrupo_produto
                            and   tmrp_620.item_produto             = tmrp_625.item_produto
                            and   tmrp_620.alternativa_produto      = v_alternativa
                            and   tmrp_625.ordem_planejamento       = tmrp_610.ordem_planejamento
                            and   tmrp_610.situacao_ordem           < 9
                            and   tmrp_625.nivel_produto_origem     = '0'
                            and   tmrp_625.grupo_produto_origem     = '00000'
                            and   tmrp_625.subgrupo_produto_origem  = '000'
                            and   tmrp_625.item_produto_origem      = '000000'
                            and   tmrp_625.nivel_produto            = v_nivel
                            and   tmrp_625.grupo_produto            = v_grupo
                            and  (tmrp_625.subgrupo_produto         = v_subgrupo or v_subgrupo = '000')
                            and  (tmrp_625.item_produto             = v_item     or v_item     = '000000')
                            and ((tmrp_625.qtde_areceber_programada > 0
                            and  exists (select 1 from tmrp_041, pcpc_020, tmrp_630
                                         where tmrp_041.periodo_producao   = pcpc_020.periodo_producao
                                           and tmrp_041.area_producao      = 1
                                           and tmrp_041.nr_pedido_ordem    = tmrp_630.ordem_prod_compra
                                           and tmrp_041.codigo_estagio     = pcpc_020.ultimo_estagio
                                           and tmrp_041.nivel_estrutura    = tmrp_625.nivel_produto
                                           and tmrp_041.grupo_estrutura    = tmrp_625.grupo_produto
                                           and tmrp_041.subgru_estrutura   = tmrp_625.subgrupo_produto
                                           and tmrp_041.item_estrutura     = tmrp_625.item_produto
                                           and tmrp_041.qtde_areceber      > 0
                                           and pcpc_020.ordem_producao     = tmrp_630.ordem_prod_compra
                                           and pcpc_020.cod_cancelamento   = 0
                                           and tmrp_630.ordem_planejamento = tmrp_610.ordem_planejamento
                                          and tmrp_630.area_producao      = 1))
                             or tmrp_625.qtde_areceber_programada = 0)
                          group by tmrp_610.ordem_planejamento,
                                   tmrp_625.pedido_venda,
                                   tmrp_625.pedido_reserva,
                                   tmrp_625.nivel_produto_origem,
                                   tmrp_625.grupo_produto_origem
                          UNION ALL
                          select tmrp_610.ordem_planejamento,
                                 tmrp_625.pedido_venda,
                                 tmrp_625.pedido_reserva,
                                 tmrp_625.nivel_produto_origem,
                                 tmrp_625.grupo_produto_origem
                          from tmrp_625, tmrp_620, tmrp_610
                          where   tmrp_620.ordem_planejamento       = tmrp_625.ordem_planejamento
                            and   tmrp_620.pedido_venda             = tmrp_625.pedido_venda
                            and   tmrp_620.pedido_reserva           = tmrp_625.pedido_reserva
                            and   tmrp_620.nivel_produto            = tmrp_625.nivel_produto_origem
                            and   tmrp_620.grupo_produto            = tmrp_625.grupo_produto_origem
                            and   tmrp_620.subgrupo_produto         = tmrp_625.subgrupo_produto_origem
                            and   tmrp_620.item_produto             = tmrp_625.item_produto_origem
                            and   tmrp_620.alternativa_produto      = v_alternativa
                            and   tmrp_625.ordem_planejamento       = tmrp_610.ordem_planejamento
                            and   tmrp_610.situacao_ordem           < 9
                            and   tmrp_625.nivel_produto_origem     = '1'
                            and   tmrp_625.nivel_produto            = v_nivel
                            and   tmrp_625.grupo_produto            = v_grupo
                            and  (tmrp_625.subgrupo_produto         = v_subgrupo or v_subgrupo = '000')
                            and  (tmrp_625.item_produto             = v_item     or v_item     = '000000')
                            and ((tmrp_625.qtde_areceber_programada > 0
                            and  exists (select 1 from tmrp_041, pcpc_020, tmrp_630
                                         where tmrp_041.periodo_producao   = pcpc_020.periodo_producao
                                           and tmrp_041.area_producao      = 1
                                           and tmrp_041.nr_pedido_ordem    = tmrp_630.ordem_prod_compra
                                           and tmrp_041.codigo_estagio     = pcpc_020.ultimo_estagio
                                           and tmrp_041.nivel_estrutura    = tmrp_625.nivel_produto
                                           and tmrp_041.grupo_estrutura    = tmrp_625.grupo_produto
                                           and tmrp_041.subgru_estrutura   = tmrp_625.subgrupo_produto
                                           and tmrp_041.item_estrutura     = tmrp_625.item_produto
                                           and tmrp_041.qtde_areceber      > 0
                                           and pcpc_020.ordem_producao     = tmrp_630.ordem_prod_compra
                                           and pcpc_020.cod_cancelamento   = 0
                                           and tmrp_630.ordem_planejamento = tmrp_610.ordem_planejamento
                                          and tmrp_630.area_producao      = 1))
                             or tmrp_625.qtde_areceber_programada = 0)
                          group by tmrp_610.ordem_planejamento,
                                 tmrp_625.pedido_venda,
                                 tmrp_625.pedido_reserva,
                                 tmrp_625.nivel_produto_origem,
                                 tmrp_625.grupo_produto_origem) tmrp_virtual
                          group by tmrp_virtual.ordem_planejamento,
                                   tmrp_virtual.pedido_venda,
                                   tmrp_virtual.pedido_reserva,
                                   tmrp_virtual.nivel_produto_origem,
                                   tmrp_virtual.grupo_produto_origem)
      loop
        if reg_o_planej.pedido_venda > 0
        then
           v_pedido := reg_o_planej.pedido_venda;
        else
           v_pedido := reg_o_planej.pedido_reserva;
        end if;

        -- Alteracao do codigo do componente, alternativa do componente ou consumo.
        if updating
        then
           if :old.nivel_comp <> :new.nivel_comp
           or :old.grupo_comp <> :new.grupo_comp
           or :old.sub_comp   <> :new.sub_comp
           or :old.item_comp  <> :new.item_comp
           then
              inter_pr_insere_motivo_plan (
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),


                  -- Componente modificado
                  inter_fn_buscar_tag_composta('lb33412', :old.nivel_comp || '.' ||
                                                          :old.grupo_comp || '.' ||
                                                          :old.sub_comp   || '.' ||
                                                          :old.item_comp,
                                                          '','','','','','','','','', ws_locale_usuario,ws_usuario_systextil));

           end if;

           if :old.alternativa_comp <> :new.alternativa_comp
           then
              inter_pr_insere_motivo_plan (
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                      -- Alternativa do componente {0} alterada de {1} para {2}.
                 inter_fn_buscar_tag_composta('lb33413', :old.nivel_comp || '.' ||
                                                         :old.grupo_comp || '.' ||
                                                         :old.sub_comp   || '.' ||
                                                         :old.item_comp,
                                                         to_char(:old.alternativa_comp),
                                                         to_char(:new.alternativa_comp),
                                                         '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
           end if;

           if :old.consumo <> :new.consumo
           then
              inter_pr_insere_motivo_plan (
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  -- Consumo do componente {0} alterado de {1} para {2}.
                  inter_fn_buscar_tag_composta('lb33414', :old.nivel_comp || '.' ||
                                                          :old.grupo_comp || '.' ||
                                                          :old.sub_comp   || '.' ||
                                                          :old.item_comp,
                                                          to_char(:old.consumo),
                                                          to_char(:new.consumo),
                                                          '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
           end if;

           if :old.cons_unid_med_generica <> :new.cons_unid_med_generica
           then
              inter_pr_insere_motivo_plan (
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  -- Consumo do componente {0} alterado de {1} para {2}.
                  inter_fn_buscar_tag_composta('lb34234', :old.nivel_comp || '.' ||
                                                          :old.grupo_comp || '.' ||
                                                          :old.sub_comp   || '.' ||
                                                          :old.item_comp,
                                                          to_char(:old.cons_unid_med_generica),
                                                          to_char(:new.cons_unid_med_generica),
                                                          '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
           end if;

           if :old.estagio <> :new.estagio
           then
              inter_pr_insere_motivo_plan (
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  -- Estagio do componente {0} foi alterado de {1} para {2}.
                  inter_fn_buscar_tag_composta('lb34229', :old.nivel_comp || '.' ||
                                                          :old.grupo_comp || '.' ||
                                                          :old.sub_comp   || '.' ||
                                                          :old.item_comp,
                                                          to_char(:old.estagio),
                                                          to_char(:new.estagio),
                                                          '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
           end if;
        end if;

        -- Eliminacao de um componente.
        if deleting
        then
           inter_pr_insere_motivo_plan (
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  -- Componente {0} excluido.
                  inter_fn_buscar_tag_composta('lb33415', :old.nivel_comp || '.' ||
                                                          :old.grupo_comp || '.' ||
                                                          :old.sub_comp   || '.' ||
                                                          :old.item_comp,
                                                          '','','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
        end if;

        -- Inclusao de um novo componente.
        if inserting
        then
           inter_pr_insere_motivo_plan (
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  -- Novo componente: {0}
                  inter_fn_buscar_tag_composta('lb33416',:new.nivel_comp || '.' ||
                                                         :new.grupo_comp || '.' ||
                                                         :new.sub_comp   || '.' ||
                                                         :new.item_comp,
                                                         '','','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
        end if;
      end loop;
   end if;

end inter_tr_plan_mov_basi_050;

-- ALTER TRIGGER "INTER_TR_PLAN_MOV_BASI_050" ENABLE
 

/

exec inter_pr_recompile;

