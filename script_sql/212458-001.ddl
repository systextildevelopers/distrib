alter table hdoc_001 add status number(1)default 0;
alter table hdoc_001 add data_atualizacao date;
alter table hdoc_001 add nome_usuario varchar2(100);
alter table hdoc_001 add natop number(3) default 0;
alter table hdoc_001 add nivel varchar2(1);
alter table hdoc_001 add grupo varchar2(5);
alter table hdoc_001 add subgrupo varchar2(3);
alter table hdoc_001 add item varchar2(6);
alter table hdoc_001 add deposito numeric(3) default 0;
