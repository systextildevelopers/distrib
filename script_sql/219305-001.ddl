create table SUPR_113
(
 cod_condicao number(9) not null,
 descricao varchar2(20) not null,
 permite_req_compra varchar2(1) default 'S' not null
 );

comment on table SUPR_113
  is 'Cadastro de ciclo de vida de produto';

alter table SUPR_113
  add constraint PK_SUPR_113 primary key (cod_condicao);

insert into SUPR_113
(cod_condicao,descricao)
values
(0,'Liberado');
