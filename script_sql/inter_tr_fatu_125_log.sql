
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_125_LOG" 
after insert or delete
or update of cod_rua, transpor_forne9, transpor_forne4, transpor_forne2
on FATU_125
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);                         

 if inserting
 then
    begin

        insert into FATU_125_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           COD_RUA_OLD,   /*8*/
           COD_RUA_NEW,   /*9*/
           TRANSPOR_FORNE9_OLD,   /*10*/
           TRANSPOR_FORNE9_NEW,   /*11*/
           TRANSPOR_FORNE4_OLD,   /*12*/
           TRANSPOR_FORNE4_NEW,   /*13*/
           TRANSPOR_FORNE2_OLD,   /*14*/
           TRANSPOR_FORNE2_NEW    /*15*/
        ) values (
            'I', /*1*/
            sysdate, /*2*/
            sysdate,/*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           0,/*8*/
           :new.COD_RUA, /*9*/
           0,/*10*/
           :new.TRANSPOR_FORNE9, /*11*/
           0,/*12*/
           :new.TRANSPOR_FORNE4, /*13*/
           0,/*14*/
           :new.TRANSPOR_FORNE2 /*15*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into FATU_125_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_RUA_OLD, /*8*/
           COD_RUA_NEW, /*9*/
           TRANSPOR_FORNE9_OLD, /*10*/
           TRANSPOR_FORNE9_NEW, /*11*/
           TRANSPOR_FORNE4_OLD, /*12*/
           TRANSPOR_FORNE4_NEW, /*13*/
           TRANSPOR_FORNE2_OLD, /*14*/
           TRANSPOR_FORNE2_NEW  /*15*/
        ) values (
            'A', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.COD_RUA,  /*8*/
           :new.COD_RUA, /*9*/
           :old.TRANSPOR_FORNE9,  /*10*/
           :new.TRANSPOR_FORNE9, /*11*/
           :old.TRANSPOR_FORNE4,  /*12*/
           :new.TRANSPOR_FORNE4, /*13*/
           :old.TRANSPOR_FORNE2,  /*14*/
           :new.TRANSPOR_FORNE2  /*15*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into FATU_125_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_RUA_OLD, /*8*/
           COD_RUA_NEW, /*9*/
           TRANSPOR_FORNE9_OLD, /*10*/
           TRANSPOR_FORNE9_NEW, /*11*/
           TRANSPOR_FORNE4_OLD, /*12*/
           TRANSPOR_FORNE4_NEW, /*13*/
           TRANSPOR_FORNE2_OLD, /*14*/
           TRANSPOR_FORNE2_NEW /*15*/
        ) values (
            'D', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede,/*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.COD_RUA, /*8*/
           0, /*9*/
           :old.TRANSPOR_FORNE9, /*10*/
           0, /*11*/
           :old.TRANSPOR_FORNE4, /*12*/
           0, /*13*/
           :old.TRANSPOR_FORNE2, /*14*/
           0 /*15*/
         );
    end;
 end if;
end inter_tr_FATU_125_log;
-- ALTER TRIGGER "INTER_TR_FATU_125_LOG" ENABLE
 

/

exec inter_pr_recompile;

