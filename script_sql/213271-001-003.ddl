alter table pcpc_331
add qualidade_baixa number(1);

--Add comments to the columns 
comment on column pcpc_331.qualidade_baixa is '1 - Primeira, 2 - Segunda, 3 - Conserto, 4 - Perda e 5 - Reprocesso.';


/

exec inter_pr_recompile;
