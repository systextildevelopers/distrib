create or replace view inter_vi_estq_040_080 as
select es040.deposito,
       es040.cditem_nivel99,
       es040.cditem_grupo,
       es040.cditem_subgrupo,
       es040.cditem_item,
       es040.lote_acomp,
       es040.qtde_estoque_atu,
       es040.qtde_empenhada,
       es040.qtde_sugerida,
       es080.lote_fornecedor,
       es080.cgc_9,
       es080.cgc_4,
       es080.cgc_2,
       es080.cnpj9_fabr,
       es080.cnpj4_fabr,
       es080.cnpj2_fabr
  from estq_040 es040, estq_080 es080
 where es040.cditem_nivel99 = es080.nivel_estrutura
   and es040.cditem_grupo = es080.grupo_estrutura
   and es040.cditem_subgrupo = es080.sub_estrutura
   and es040.cditem_item = es080.item_estrutura
   and es040.lote_acomp = es080.lote_produto
   order by es040.deposito, es040.lote_acomp;
