
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_045_PMS" 
  after insert on pcpc_045
  for each row

declare
  v_estagio_programado   basi_030.estagio_altera_programado%type;
  v_proconf_grupo        pcpc_040.proconf_grupo%type;
  v_proconf_item         pcpc_040.proconf_item%type;
  v_executa_trigger      number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger = 0
   then
      -- INICIO - Atualizacao da quantidade produzida na ordem de planejamento.
      if inserting
      then
         begin
             select basi_030.estagio_altera_programado, pcpc_040.proconf_grupo,
                    pcpc_040.proconf_item
             into   v_estagio_programado,               v_proconf_grupo,
                    v_proconf_item
             from pcpc_040, basi_030
             where pcpc_040.periodo_producao = :new.pcpc040_perconf
               and pcpc_040.ordem_confeccao  = :new.pcpc040_ordconf
               and pcpc_040.codigo_estagio   = :new.pcpc040_estconf
               and pcpc_040.ordem_producao   = :new.ordem_producao -- Neylor - Pette.
               and pcpc_040.proconf_nivel99  = basi_030.nivel_estrutura
               and pcpc_040.proconf_grupo    = basi_030.referencia
               and pcpc_040.codigo_estagio   = basi_030.estagio_altera_programado;
             exception
             when no_data_found then
                v_estagio_programado  := 0;
                v_proconf_grupo       := ' ';
                v_proconf_item        := ' ';
         end;

          if :new.pcpc040_estconf = v_estagio_programado
          then
             update pcpc_040
             set pcpc_040.exportado_pms = 'N'
             where pcpc_040.periodo_producao = :new.pcpc040_perconf
               and pcpc_040.ordem_producao   = :new.ordem_producao
               and pcpc_040.proconf_grupo    = v_proconf_grupo
               and pcpc_040.proconf_item     = v_proconf_item
               and pcpc_040.codigo_estagio   = v_estagio_programado;
          end if;
      end if;
   end if;
end inter_tr_pcpc_045_pms;

-- ALTER TRIGGER "INTER_TR_PCPC_045_PMS" ENABLE
 

/

exec inter_pr_recompile;

