create table NFOR_020
(
	cod_negociacao    	number(9)    not null,
	sequencia			number(3)    not null,
	cod_empresa			number(3)    not null,
	cod_deposito		number(3)    not null,
	quantidade			number(15,3) not null,
	fator_conv			number(15,3)
);

ALTER TABLE NFOR_020 ADD CONSTRAINT PK_NFOR_020 PRIMARY KEY (cod_negociacao, sequencia, cod_empresa, cod_deposito);

ALTER TABLE NFOR_020
ADD CONSTRAINT REF_NFOR_020_NFOR_015 FOREIGN KEY (cod_negociacao, sequencia) REFERENCES NFOR_015 (cod_negociacao, sequencia);

ALTER TABLE NFOR_020
ADD CONSTRAINT REF_NFOR_020_FATU_500 FOREIGN KEY (cod_empresa) REFERENCES FATU_500 (codigo_empresa);

ALTER TABLE NFOR_020
ADD CONSTRAINT REF_NFOR_020_BASI_205 FOREIGN KEY (cod_deposito) REFERENCES BASI_205 (codigo_deposito);


/

exec inter_pr_recompile;
