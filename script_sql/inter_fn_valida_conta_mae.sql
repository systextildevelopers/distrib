CREATE OR REPLACE FUNCTION inter_fn_valida_conta_mae(
    p_conta_reduzida IN NUMBER,
    p_cod_plano in number,
    p_cod_empresa in number,
    p_exercicio in number
) RETURN NUMBER IS
v_retorno number;
tmpInt number;
BEGIN
    v_retorno := 0;

    select count(*) into tmpInt from cont_610
    where cont_610.cod_empresa    = p_cod_empresa
    and   cont_610.exercicio      = p_exercicio
    and   cont_610.conta_reduzida   = p_conta_reduzida
    and  (cont_610.total_debito  <> 0.00
    or    cont_610.total_credito <> 0.00
    or    cont_610.saldo_periodo <> 0.00
    );
    if tmpInt > 0 then return 1;
    end if;

    FOR a IN (SELECT cod_reduzido, cont_535.tipo_conta, cont_535.conta_contabil FROM cont_535 WHERE conta_mae = p_conta_reduzida and cont_535.cod_plano_cta = p_cod_plano) LOOP

          select count(*) into tmpInt from cont_610
          where cont_610.cod_empresa    = p_cod_empresa
          and   cont_610.exercicio      = p_exercicio
          and   cont_610.conta_reduzida   = a.cod_reduzido
          and  (cont_610.total_debito  <> 0.00
          or    cont_610.total_credito <> 0.00
          or    cont_610.saldo_periodo <> 0.00
          );

          if tmpInt > 0 then return 1; exit;
          else        --if a.tipo_conta = 2 and tmpInt = 0
             v_retorno := inter_fn_valida_conta_mae(a.cod_reduzido, p_cod_plano, p_cod_empresa, p_exercicio);
             if v_retorno = 1
               then exit;
               return v_retorno;
             end if;
          end if;
    END LOOP;
    
    return 0;

    
END inter_fn_valida_conta_mae;


/

exec inter_pr_recompile;
