insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('cont_f118', 'Painel de Conglomerados Econômicos (Bloco K)', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'cont_f118', 'menu_ad03', 0, 1, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'cont_f118', 'menu_ad03', 0, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Painel de Conglomerados Econômicos (Bloco K)'
 where hdoc_036.codigo_programa = 'cont_f118'
   and hdoc_036.locale          = 'es_ES';
commit;
/
