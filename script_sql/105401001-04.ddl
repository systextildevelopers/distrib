alter table fatu_050 add (nr_autorizacao_opera  varchar2(100),  cod_forma_pagto  number(2));

alter table fatu_050 modify (nr_autorizacao_opera default '  ', cod_forma_pagto default 0 );

comment on COLUMN fatu_050.nr_autorizacao_opera is 'Numero de autorizacao quando venda a cartão';
comment on COLUMN fatu_050.cod_forma_pagto is 'Código da forma de pagamento da venda';
/
exec inter_pr_recompile;

