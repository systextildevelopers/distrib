create table obrf_453
(
  cod_gia    NUMBER(5)     default 0   not null,
  ocorrencia VARCHAR2(200) default ' ' not null,
  base_legal VARCHAR2(200) default ' ' not null,
  operacao   VARCHAR2(100) default 0   not null,
  quadro     NUMBER(2)     default 0   not null,
  altera     VARCHAR2(1)   default ' ' not null
);

alter table obrf_453 add constraint pk_obrf_453 primary key (cod_gia, quadro);
