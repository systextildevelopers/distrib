
  CREATE OR REPLACE FUNCTION "INTER_FN_NIVEL_PROD_SERASA" 
  (p_pedido_venda pedi_100.pedido_venda%type)
return varchar2
is

   v_nivel_pedido pedi_100.tecido_peca%type;

begin

   begin
      select pedi_100.tecido_peca
        into v_nivel_pedido
        from pedi_100
       where pedi_100.pedido_venda = p_pedido_venda;
      exception when others then
         v_nivel_pedido := 0;
   end;

   if sql%notfound
   then
      v_nivel_pedido := '';
   end if;

   return(v_nivel_pedido);

end INTER_FN_NIVEL_PROD_SERASA;
 

/

exec inter_pr_recompile;

