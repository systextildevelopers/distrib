alter table basi_351
add (faixa_etaria_custos number(3),
     qtde_marcacoes number(4,1));

alter table basi_351
modify (faixa_etaria_custos default 0,
        qtde_marcacoes default 0);

execute inter_pr_recompile;     
/

declare
cursor basi_351_c is
   select f.rowid from basi_351 f where faixa_etaria_custos is null;
   
contaRegistro number;

begin
   contaRegistro := 0;
   
   for reg_basi_351_c in basi_351_c
   loop
      update basi_351
      set faixa_etaria_custos   = 0,
          qtde_marcacoes        = 0
      where faixa_etaria_custos is null
        and basi_351.rowid = reg_basi_351_c.rowid;
      
      contaRegistro := contaRegistro + 1;
      
      if contaRegistro = 1000
      then
         contaRegistro := 0;
         commit;
      end if;
   end loop; 

   commit;
end;
/
alter table basi_351
  drop constraint PK_BASI_351;

drop index PK_BASI_351;

alter table BASI_351
  add constraint PK_BASI_351 primary key (CODIGO_EMPRESA, MES, ANO, NIVEL_ESTRUTURA, GRUPO_ESTRUTURA, SUBGRU_ESTRUTURA, ITEM_ESTRUTURA, TIPO_CUSTO, CODIGO_CUSTO, FAIXA_ETARIA_CUSTOS);
  
execute inter_pr_recompile;     
