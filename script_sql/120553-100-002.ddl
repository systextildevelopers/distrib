alter table fatu_036 add nat_oper_retorno number(3) default 0;

comment on column fatu_036.nat_oper_retorno is 'Código da Natureza de Operação para o retorno de industrialização deste item (utilizado no cálculo do faturamento para o item da nota a ser emitido)';

exec inter_pr_recompile;
