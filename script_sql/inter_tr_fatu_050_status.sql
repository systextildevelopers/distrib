
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_050_STATUS" 
before update of cod_status, nr_protocolo, numero_danf_nfe

on fatu_050
for each row
begin

   if :old.cod_status = '100' and :new.cod_status = '999'
   then
      :new.cod_status      := '100';
      :new.msg_status      := 'Autorizado o uso da NF-e';
      :new.nr_protocolo    := :old.nr_protocolo;
      :new.numero_danf_nfe := :old.numero_danf_nfe;

   end if;

end inter_tr_fatu_050_status;

-- ALTER TRIGGER "INTER_TR_FATU_050_STATUS" ENABLE
 

/

exec inter_pr_recompile;

