create or replace view vw_formas_de_pagamento as
select
  forma_pgto as id,
  descricao as descricao,
  tarifa as tarifa,
  taxa as taxa
from loja_010
