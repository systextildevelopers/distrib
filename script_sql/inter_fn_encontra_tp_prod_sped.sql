create or replace function inter_fn_encontra_tp_prod_sped(p_cod_empresa in number,
                                                           p_conta_estoque in number,
                                                           p_nivel in varchar2,
                                                           p_grupo in varchar2,
                                                           p_subgrupo in varchar2,
                                                           p_item in varchar2,
                                                           p_tipo_produto_sped in number) return number is w_tipo_produto_sped number;


begin
    
    
    return inter_fn_tp_prod_sped_recur(p_cod_empresa,
                                       p_conta_estoque,
                                       p_nivel,
                                       p_grupo,
                                       p_subgrupo,
                                       p_item,
                                       p_tipo_produto_sped, 
                                       0);

end inter_fn_encontra_tp_prod_sped;
/
