create or replace trigger inter_tr_finalidade_ob_perm
before insert on finalidade_ob_permissoes
for each row
begin
   :new.id := seq_finalidade_ob_permissoes.nextval;
end;
