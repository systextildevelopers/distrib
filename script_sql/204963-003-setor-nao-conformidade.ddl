create table efic_302(
    setor_id number(3),
	tipo_id number(4),
    constraint efic_302_pk primary key (setor_id, tipo_id),
	constraint fk_efic_302_basi_006 foreign key (setor_id) references basi_006 (setor_responsavel),
	constraint fk_efic_302_efic_300 foreign key (tipo_id) references efic_300 (id));

comment on column efic_302.setor_id is 'Identificador do setor';
comment on column efic_302.tipo_id is 'Identificador do tipo de não conformidade';
comment on table efic_302 is 'Tabela de relacionamento de setores x tipos de não conformidades';

/
