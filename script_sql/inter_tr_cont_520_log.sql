create or replace trigger inter_tr_CONT_520_log 
after insert or delete or update 
on CONT_520 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(20) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 

   v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 if inserting 
 then 
    begin 
 
        insert into CONT_520_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           cod_empresa_OLD,   /*8*/ 
           cod_empresa_NEW,   /*9*/ 
           exercicio_OLD,   /*10*/ 
           exercicio_NEW,   /*11*/ 
           origem_OLD,   /*12*/ 
           origem_NEW,   /*13*/ 
           lote_OLD,   /*14*/ 
           lote_NEW,   /*15*/ 
           data_lote_OLD,   /*16*/ 
           data_lote_NEW,   /*17*/ 
           situacao_OLD,   /*18*/ 
           situacao_NEW    /*19*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.cod_empresa, /*9*/   
           0,/*10*/
           :new.exercicio, /*11*/   
           0,/*12*/
           :new.origem, /*13*/   
           0,/*14*/
           :new.lote, /*15*/   
           null,/*16*/
           :new.data_lote, /*17*/   
           0,/*18*/
           :new.situacao /*19*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into CONT_520_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           cod_empresa_OLD, /*8*/   
           cod_empresa_NEW, /*9*/   
           exercicio_OLD, /*10*/   
           exercicio_NEW, /*11*/   
           origem_OLD, /*12*/   
           origem_NEW, /*13*/   
           lote_OLD, /*14*/   
           lote_NEW, /*15*/   
           data_lote_OLD, /*16*/   
           data_lote_NEW, /*17*/   
           situacao_OLD, /*18*/   
           situacao_NEW  /*19*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.cod_empresa,  /*8*/  
           :new.cod_empresa, /*9*/   
           :old.exercicio,  /*10*/  
           :new.exercicio, /*11*/   
           :old.origem,  /*12*/  
           :new.origem, /*13*/   
           :old.lote,  /*14*/  
           :new.lote, /*15*/   
           :old.data_lote,  /*16*/  
           :new.data_lote, /*17*/   
           :old.situacao,  /*18*/  
           :new.situacao  /*19*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into CONT_520_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           cod_empresa_OLD, /*8*/   
           cod_empresa_NEW, /*9*/   
           exercicio_OLD, /*10*/   
           exercicio_NEW, /*11*/   
           origem_OLD, /*12*/   
           origem_NEW, /*13*/   
           lote_OLD, /*14*/   
           lote_NEW, /*15*/   
           data_lote_OLD, /*16*/   
           data_lote_NEW, /*17*/   
           situacao_OLD, /*18*/   
           situacao_NEW /*19*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.cod_empresa, /*8*/   
           0, /*9*/
           :old.exercicio, /*10*/   
           0, /*11*/
           :old.origem, /*12*/   
           0, /*13*/
           :old.lote, /*14*/   
           0, /*15*/
           :old.data_lote, /*16*/   
           null, /*17*/
           :old.situacao, /*18*/   
           0 /*19*/
         );    
    end;    
 end if;    
end inter_tr_CONT_520_log;

/

exec inter_pr_recompile;
