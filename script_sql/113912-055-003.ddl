
alter table rcnb_750 modify (
  custo_base          number(19,5),
  custo_anterior1     number(19,5),
  custo_anterior2     number(19,5),
  custo_anterior3     number(19,5),
  custo_anterior4     number(19,5),
  custo_intervalo_ini number(19,5),
  custo_intervalo_fim number(19,5));
  


alter table rcnb_755 add custo_base_s_icms number(19,5) default 0.00000;
  
alter table rcnb_755 modify (
  custo_base          number(19,5) default 0.00000,
  custo_anterior1     number(19,5) default 0.00000,
  custo_anterior2     number(19,5) default 0.00000,
  custo_anterior3     number(19,5) default 0.00000,
  custo_anterior4     number(19,5) default 0.00000,
  custo_base_icms     number(19,5) default 0.00000,
  custo_base_s_icms   number(19,5) default 0.00000,
  custo_base_mp_icms  number(19,5));
  
  exec inter_pr_recompile; 
  
