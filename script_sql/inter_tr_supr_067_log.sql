
  CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_067_LOG" 
before insert or
       delete or
       update of ccusto_aplicacao, num_requisicao, seq_item_req, item_req_nivel99,
		 item_req_grupo, item_req_subgrupo, item_req_item, descricao_item,
		 unidade_medida, qtde_requisitada, data_prev_entr, observacao_item,
		 cod_cancelamento, dt_cancelamento, numero_cotacao, numero_pedido,
		 situacao, codigo_comprador, numero_coleta, grupo_maquina,
		 subgru_maquina, numero_maquina, codigo_agrupador, solicitacao,
		 requis_almox, seq_requis_almox, projeto, subprojeto,
		 servico, data_requisicao, hora_requisicao, controle_exportacao,
		 usuario_requisitante, executa_trigger
   on supr_067
   for each row

declare
  V_NUM_REQUISICAO_OLD    supr_067.NUM_REQUISICAO%type;
  V_SEQ_ITEM_REQ_OLD      supr_067.SEQ_ITEM_REQ%type;
  V_ITEM_REQ_NIVEL99_OLD  supr_067.ITEM_REQ_NIVEL99%type;
  V_ITEM_REQ_GRUPO_OLD    supr_067.ITEM_REQ_GRUPO%type;
  V_ITEM_REQ_SUBGRUPO_OLD supr_067.ITEM_REQ_SUBGRUPO%type;
  V_ITEM_REQ_ITEM_OLD     supr_067.ITEM_REQ_ITEM%type;
  V_DESCRICAO_ITEM_OLD    supr_067.DESCRICAO_ITEM%type;
  V_UNIDADE_MEDIDA_OLD    supr_067.UNIDADE_MEDIDA%type;
  V_QTDE_REQUISITADA_OLD  supr_067.QTDE_REQUISITADA%type;
  V_DATA_PREV_ENTR_OLD    supr_067.DATA_PREV_ENTR%type;
  V_OBSERVACAO_ITEM_OLD   supr_067.OBSERVACAO_ITEM%type;
  V_COD_CANCELAMENTO_OLD  supr_067.COD_CANCELAMENTO%type;
  V_DT_CANCELAMENTO_OLD   supr_067.DT_CANCELAMENTO%type;
  V_NUMERO_COTACAO_OLD    supr_067.NUMERO_COTACAO%type;
  V_NUMERO_PEDIDO_OLD     supr_067.NUMERO_PEDIDO%type;
  V_SITUACAO_OLD          supr_067.SITUACAO%type;
  V_CODIGO_COMPRADOR_OLD  supr_067.CODIGO_COMPRADOR%type;
  V_NUMERO_COLETA_OLD     supr_067.NUMERO_COLETA%type;
  V_GRUPO_MAQUINA_OLD     supr_067.GRUPO_MAQUINA%type;
  V_SUBGRU_MAQUINA_OLD    supr_067.SUBGRU_MAQUINA%type;
  V_NUMERO_MAQUINA_OLD    supr_067.NUMERO_MAQUINA%type;
  V_CODIGO_AGRUPADOR_OLD  supr_067.CODIGO_AGRUPADOR%type;
  V_SOLICITACAO_OLD       supr_067.SOLICITACAO%type;
  V_REQUIS_ALMOX_OLD      supr_067.REQUIS_ALMOX%type;
  V_SEQ_REQUIS_ALMOX_OLD  supr_067.SEQ_REQUIS_ALMOX%type;
  V_PROJETO_OLD           supr_067.PROJETO%type;
  V_SUBPROJETO_OLD        supr_067.SUBPROJETO%type;
  V_SERVICO_OLD           supr_067.SERVICO%type;
  V_DATA_REQUISICAO_OLD   supr_067.DATA_REQUISICAO%type;
  V_HORA_REQUISICAO_OLD   supr_067.HORA_REQUISICAO%type;
  V_CCUSTO_APLICACAO_OLD  supr_067.CCUSTO_APLICACAO%type;
  V_NUM_REQUISICAO_NEW    supr_067.NUM_REQUISICAO%type;
  V_SEQ_ITEM_REQ_NEW      supr_067.SEQ_ITEM_REQ%type;
  V_ITEM_REQ_NIVEL99_NEW  supr_067.ITEM_REQ_NIVEL99%type;
  V_ITEM_REQ_GRUPO_NEW    supr_067.ITEM_REQ_GRUPO%type;
  V_ITEM_REQ_SUBGRUPO_NEW supr_067.ITEM_REQ_SUBGRUPO%type;
  V_ITEM_REQ_ITEM_NEW     supr_067.ITEM_REQ_ITEM%type;
  V_DESCRICAO_ITEM_NEW    supr_067.DESCRICAO_ITEM%type;
  V_UNIDADE_MEDIDA_NEW    supr_067.UNIDADE_MEDIDA%type;
  V_QTDE_REQUISITADA_NEW  supr_067.QTDE_REQUISITADA%type;
  V_DATA_PREV_ENTR_NEW    supr_067.DATA_PREV_ENTR%type;
  V_OBSERVACAO_ITEM_NEW   supr_067.OBSERVACAO_ITEM%type;
  V_COD_CANCELAMENTO_NEW  supr_067.COD_CANCELAMENTO%type;
  V_DT_CANCELAMENTO_NEW   supr_067.DT_CANCELAMENTO%type;
  V_NUMERO_COTACAO_NEW    supr_067.NUMERO_COTACAO%type;
  V_NUMERO_PEDIDO_NEW     supr_067.NUMERO_PEDIDO%type;
  V_SITUACAO_NEW          supr_067.SITUACAO%type;
  V_CODIGO_COMPRADOR_NEW  supr_067.CODIGO_COMPRADOR%type;
  V_NUMERO_COLETA_NEW     supr_067.NUMERO_COLETA%type;
  V_GRUPO_MAQUINA_NEW     supr_067.GRUPO_MAQUINA%type;
  V_SUBGRU_MAQUINA_NEW    supr_067.SUBGRU_MAQUINA%type;
  V_NUMERO_MAQUINA_NEW    supr_067.NUMERO_MAQUINA%type;
  V_CODIGO_AGRUPADOR_NEW  supr_067.CODIGO_AGRUPADOR%type;
  V_SOLICITACAO_NEW       supr_067.SOLICITACAO%type;
  V_REQUIS_ALMOX_NEW      supr_067.REQUIS_ALMOX%type;
  V_SEQ_REQUIS_ALMOX_NEW  supr_067.SEQ_REQUIS_ALMOX%type;
  V_PROJETO_NEW           supr_067.PROJETO%type;
  V_SUBPROJETO_NEW        supr_067.SUBPROJETO%type;
  V_SERVICO_NEW           supr_067.SERVICO%type;
  V_DATA_REQUISICAO_NEW   supr_067.DATA_REQUISICAO%type;
  V_HORA_REQUISICAO_NEW   supr_067.HORA_REQUISICAO%type;
  V_CCUSTO_APLICACAO_NEW  supr_067.CCUSTO_APLICACAO%type;
  V_OPERACAO              varchar(1);
  V_DATA_OPERACAO         date;
  V_USUARIO_REDE          varchar(20);
  V_MAQUINA_REDE          varchar(40);
  V_APLICATIVO            varchar(20);
  ws_sid                    number(9);
  ws_empresa                number(3);
  ws_usuario_systextil      varchar2(250);
  ws_locale_usuario         varchar2(5);

   v_executa_trigger      number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      begin
         -- Grava a data/hora da insercao do registro (log)
         V_DATA_OPERACAO := sysdate;


         -- Dados do usuario logado
        inter_pr_dados_usuario (V_USUARIO_REDE,        V_MAQUINA_REDE,   V_APLICATIVO,     ws_sid,
                                ws_usuario_systextil,  ws_empresa,       ws_locale_usuario);

         --Alimenta as variaveis NEW caso seja insert ou update
         if  inserting or updating
         then
            if inserting
            then
               V_OPERACAO := 'I';
            else
               V_OPERACAO := 'U';
            end if;

            V_NUM_REQUISICAO_NEW    := :new.NUM_REQUISICAO;
            V_SEQ_ITEM_REQ_NEW      := :new.SEQ_ITEM_REQ;
            V_ITEM_REQ_NIVEL99_NEW  := :new.ITEM_REQ_NIVEL99;
            V_ITEM_REQ_GRUPO_NEW    := :new.ITEM_REQ_GRUPO;
            V_ITEM_REQ_SUBGRUPO_NEW := :new.ITEM_REQ_SUBGRUPO;
            V_ITEM_REQ_ITEM_NEW     := :new.ITEM_REQ_ITEM;
            V_DESCRICAO_ITEM_NEW    := :new.DESCRICAO_ITEM;
            V_UNIDADE_MEDIDA_NEW    := :new.UNIDADE_MEDIDA;
            V_QTDE_REQUISITADA_NEW  := :new.QTDE_REQUISITADA;
            V_DATA_PREV_ENTR_NEW    := :new.DATA_PREV_ENTR;
            V_OBSERVACAO_ITEM_NEW   := :new.OBSERVACAO_ITEM;
            V_COD_CANCELAMENTO_NEW  := :new.COD_CANCELAMENTO;
            V_DT_CANCELAMENTO_NEW   := :new.DT_CANCELAMENTO;
            V_NUMERO_COTACAO_NEW    := :new.NUMERO_COTACAO;
            V_NUMERO_PEDIDO_NEW     := :new.NUMERO_PEDIDO;
            V_SITUACAO_NEW          := :new.SITUACAO;
            V_CODIGO_COMPRADOR_NEW  := :new.CODIGO_COMPRADOR;
            V_NUMERO_COLETA_NEW     := :new.NUMERO_COLETA;
            V_GRUPO_MAQUINA_NEW     := :new.GRUPO_MAQUINA;
            V_SUBGRU_MAQUINA_NEW    := :new.SUBGRU_MAQUINA;
            V_NUMERO_MAQUINA_NEW    := :new.NUMERO_MAQUINA;
            V_CODIGO_AGRUPADOR_NEW  := :new.CODIGO_AGRUPADOR;
            V_SOLICITACAO_NEW       := :new.SOLICITACAO;
            V_REQUIS_ALMOX_NEW      := :new.REQUIS_ALMOX;
            V_SEQ_REQUIS_ALMOX_NEW  := :new.SEQ_REQUIS_ALMOX;
            V_PROJETO_NEW           := :new.PROJETO;
            V_SUBPROJETO_NEW        := :new.SUBPROJETO;
            V_SERVICO_NEW           := :new.SERVICO;
            V_DATA_REQUISICAO_NEW   := :new.DATA_REQUISICAO;
            V_HORA_REQUISICAO_NEW   := :new.HORA_REQUISICAO;
	    V_CCUSTO_APLICACAO_NEW  := :new.CCUSTO_APLICACAO;

         end if; -- Fim do inserting or updating

         -- Alimenta as variaveis OLD caso seja insert ou update
         if deleting or updating
         then
            if deleting
            then
               V_OPERACAO := 'D';
            else
               V_OPERACAO := 'U';
            end if;

            V_NUM_REQUISICAO_OLD    := :old.NUM_REQUISICAO;
            V_SEQ_ITEM_REQ_OLD      := :old.SEQ_ITEM_REQ;
            V_ITEM_REQ_NIVEL99_OLD  := :old.ITEM_REQ_NIVEL99;
            V_ITEM_REQ_GRUPO_OLD    := :old.ITEM_REQ_GRUPO;
            V_ITEM_REQ_SUBGRUPO_OLD := :old.ITEM_REQ_SUBGRUPO;
            V_ITEM_REQ_ITEM_OLD     := :old.ITEM_REQ_ITEM;
            V_DESCRICAO_ITEM_OLD    := :old.DESCRICAO_ITEM;
            V_UNIDADE_MEDIDA_OLD    := :old.UNIDADE_MEDIDA;
            V_QTDE_REQUISITADA_OLD  := :old.QTDE_REQUISITADA;
            V_DATA_PREV_ENTR_OLD    := :old.DATA_PREV_ENTR;
            V_OBSERVACAO_ITEM_OLD   := :old.OBSERVACAO_ITEM;
            V_COD_CANCELAMENTO_OLD  := :old.COD_CANCELAMENTO;
            V_DT_CANCELAMENTO_OLD   := :old.DT_CANCELAMENTO;
            V_NUMERO_COTACAO_OLD    := :old.NUMERO_COTACAO;
            V_NUMERO_PEDIDO_OLD     := :old.NUMERO_PEDIDO;
            V_SITUACAO_OLD          := :old.SITUACAO;
            V_CODIGO_COMPRADOR_OLD  := :old.CODIGO_COMPRADOR;
            V_NUMERO_COLETA_OLD     := :old.NUMERO_COLETA;
            V_GRUPO_MAQUINA_OLD     := :old.GRUPO_MAQUINA;
            V_SUBGRU_MAQUINA_OLD    := :old.SUBGRU_MAQUINA;
            V_NUMERO_MAQUINA_OLD    := :old.NUMERO_MAQUINA;
            V_CODIGO_AGRUPADOR_OLD  := :old.CODIGO_AGRUPADOR;
            V_SOLICITACAO_OLD       := :old.SOLICITACAO;
            V_REQUIS_ALMOX_OLD      := :old.REQUIS_ALMOX;
            V_SEQ_REQUIS_ALMOX_OLD  := :old.SEQ_REQUIS_ALMOX;
            V_PROJETO_OLD           := :old.PROJETO;
            V_SUBPROJETO_OLD        := :old.SUBPROJETO;
            V_SERVICO_OLD           := :old.SERVICO;
            V_DATA_REQUISICAO_OLD   := :old.DATA_REQUISICAO;
            V_HORA_REQUISICAO_OLD   := :old.HORA_REQUISICAO;
			V_CCUSTO_APLICACAO_OLD  := :old.CCUSTO_APLICACAO;

         end if; -- Fim do deleting or updating

         -- Insere o registro na supr_067_log
         insert into supr_067_log (
           NUM_REQUISICAO_OLD,
           SEQ_ITEM_REQ_OLD,
           ITEM_REQ_NIVEL99_OLD,
           ITEM_REQ_GRUPO_OLD,
           ITEM_REQ_SUBGRUPO_OLD,
           ITEM_REQ_ITEM_OLD,
           DESCRICAO_ITEM_OLD,
           UNIDADE_MEDIDA_OLD,
           QTDE_REQUISITADA_OLD,
           DATA_PREV_ENTR_OLD,
           OBSERVACAO_ITEM_OLD,
           COD_CANCELAMENTO_OLD,
           DT_CANCELAMENTO_OLD,
           NUMERO_COTACAO_OLD,
           NUMERO_PEDIDO_OLD,
           SITUACAO_OLD,
           CODIGO_COMPRADOR_OLD,
           NUMERO_COLETA_OLD,
           GRUPO_MAQUINA_OLD,
           SUBGRU_MAQUINA_OLD,
           NUMERO_MAQUINA_OLD,
           CODIGO_AGRUPADOR_OLD,
           SOLICITACAO_OLD,
           REQUIS_ALMOX_OLD,
           SEQ_REQUIS_ALMOX_OLD,
           PROJETO_OLD,
           SUBPROJETO_OLD,
           SERVICO_OLD,
           DATA_REQUISICAO_OLD,
           HORA_REQUISICAO_OLD,
           CCUSTO_APLICACAO_OLD,
           NUM_REQUISICAO_NEW,
           SEQ_ITEM_REQ_NEW,
           ITEM_REQ_NIVEL99_NEW,
           ITEM_REQ_GRUPO_NEW,
           ITEM_REQ_SUBGRUPO_NEW,
           ITEM_REQ_ITEM_NEW,
           DESCRICAO_ITEM_NEW,
           UNIDADE_MEDIDA_NEW,
           QTDE_REQUISITADA_NEW,
           DATA_PREV_ENTR_NEW,
           OBSERVACAO_ITEM_NEW,
           COD_CANCELAMENTO_NEW,
           DT_CANCELAMENTO_NEW,
           NUMERO_COTACAO_NEW,
           NUMERO_PEDIDO_NEW,
           SITUACAO_NEW,
           CODIGO_COMPRADOR_NEW,
           NUMERO_COLETA_NEW,
           GRUPO_MAQUINA_NEW,
           SUBGRU_MAQUINA_NEW,
           NUMERO_MAQUINA_NEW,
           CODIGO_AGRUPADOR_NEW,
           SOLICITACAO_NEW,
           REQUIS_ALMOX_NEW,
           SEQ_REQUIS_ALMOX_NEW,
           PROJETO_NEW,
           SUBPROJETO_NEW,
           SERVICO_NEW,
           DATA_REQUISICAO_NEW,
           HORA_REQUISICAO_NEW,
	   CCUSTO_APLICACAO_NEW,
           OPERACAO,
           DATA_OPERACAO,
           USUARIO_REDE,
           MAQUINA_REDE,
           APLICATIVO)
        values (
           V_NUM_REQUISICAO_OLD,
           V_SEQ_ITEM_REQ_OLD,
           V_ITEM_REQ_NIVEL99_OLD,
           V_ITEM_REQ_GRUPO_OLD,
           V_ITEM_REQ_SUBGRUPO_OLD,
           V_ITEM_REQ_ITEM_OLD,
           V_DESCRICAO_ITEM_OLD,
           V_UNIDADE_MEDIDA_OLD,
           V_QTDE_REQUISITADA_OLD,
           V_DATA_PREV_ENTR_OLD,
           V_OBSERVACAO_ITEM_OLD,
           V_COD_CANCELAMENTO_OLD,
           V_DT_CANCELAMENTO_OLD,
           V_NUMERO_COTACAO_OLD,
           V_NUMERO_PEDIDO_OLD,
           V_SITUACAO_OLD,
           V_CODIGO_COMPRADOR_OLD,
           V_NUMERO_COLETA_OLD,
           V_GRUPO_MAQUINA_OLD,
           V_SUBGRU_MAQUINA_OLD,
           V_NUMERO_MAQUINA_OLD,
           V_CODIGO_AGRUPADOR_OLD,
           V_SOLICITACAO_OLD,
           V_REQUIS_ALMOX_OLD,
           V_SEQ_REQUIS_ALMOX_OLD,
           V_PROJETO_OLD,
           V_SUBPROJETO_OLD,
           V_SERVICO_OLD,
           V_DATA_REQUISICAO_OLD,
           V_HORA_REQUISICAO_OLD,
           V_CCUSTO_APLICACAO_OLD,
           V_NUM_REQUISICAO_NEW,
           V_SEQ_ITEM_REQ_NEW,
           V_ITEM_REQ_NIVEL99_NEW,
           V_ITEM_REQ_GRUPO_NEW,
           V_ITEM_REQ_SUBGRUPO_NEW,
           V_ITEM_REQ_ITEM_NEW,
           V_DESCRICAO_ITEM_NEW,
           V_UNIDADE_MEDIDA_NEW,
           V_QTDE_REQUISITADA_NEW,
           V_DATA_PREV_ENTR_NEW,
           V_OBSERVACAO_ITEM_NEW,
           V_COD_CANCELAMENTO_NEW,
           V_DT_CANCELAMENTO_NEW,
           V_NUMERO_COTACAO_NEW,
           V_NUMERO_PEDIDO_NEW,
           V_SITUACAO_NEW,
           V_CODIGO_COMPRADOR_NEW,
           V_NUMERO_COLETA_NEW,
           V_GRUPO_MAQUINA_NEW,
           V_SUBGRU_MAQUINA_NEW,
           V_NUMERO_MAQUINA_NEW,
           V_CODIGO_AGRUPADOR_NEW,
           V_SOLICITACAO_NEW,
           V_REQUIS_ALMOX_NEW,
           V_SEQ_REQUIS_ALMOX_NEW,
           V_PROJETO_NEW,
           V_SUBPROJETO_NEW,
           V_SERVICO_NEW,
           V_DATA_REQUISICAO_NEW,
           V_HORA_REQUISICAO_NEW,
           V_CCUSTO_APLICACAO_NEW,
           V_OPERACAO,
           V_DATA_OPERACAO,
           V_USUARIO_REDE,
           V_MAQUINA_REDE,
           V_APLICATIVO);
         exception
            when OTHERS
            then raise_application_error (-20000, 'Nao atualizou a tabela de log supr_067_log.');
      end;
   end if;
end inter_tr_supr_067_log;

-- ALTER TRIGGER "INTER_TR_SUPR_067_LOG" ENABLE
 

/

exec inter_pr_recompile;

