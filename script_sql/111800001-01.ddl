INSERT INTO hdoc_035
( codigo_programa, programa_menu, 
 item_menu_def,   descricao)
VALUES
( 'pcpb_l104',     1,
 1,        'Revisao de Metragem e Largura dos Rolos da OB');
 
INSERT INTO hdoc_033
( usu_prg_cdusu, usu_prg_empr_usu, 
 programa,      nome_menu, 
 item_menu,     ordem_menu, 
 incluir,       modificar, 
 excluir,       procurar)
VALUES
( 'INTERSYS',    1, 
 'pcpb_l104',   'pcpb_menu', 
 1,             1, 
 'S',           'S', 
 'S',           'S');
 
INSERT INTO hdoc_033
( usu_prg_cdusu,  usu_prg_empr_usu, 
 programa,       nome_menu, 
 item_menu,      ordem_menu, 
 incluir,        modificar, 
 excluir,        procurar)
VALUES
( 'TREINAMENTO',  1, 
 'pcpb_l104',    'pcpb_menu', 
 1,              1, 
 'S',            'S', 
 'S',            'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Revisao de Metragem e Largura dos Rolos da OB'
 WHERE hdoc_036.codigo_programa = 'pcpb_l104'
   AND hdoc_036.locale          = 'es_ES';
   
commit;

exec inter_pr_recompile;
