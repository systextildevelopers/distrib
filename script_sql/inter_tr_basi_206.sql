
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_206" 
   before insert or update on basi_206
   for each row
declare
   v_achou number(1);
begin
   select count(*) into v_achou from basi_205
   where basi_205.codigo_deposito = :new.deposito_acerto
     and basi_205.tipo_volume    in (2,4,7,9);
   if v_achou = 0
   then
      raise_application_error(-20000,'Deposito informado nao e do tipo volume ou nao existe.');
   end if;

   select count(*) into v_achou from estq_005
   where estq_005.codigo_transacao = :new.transacao_acerto_saida
     and estq_005.entrada_saida    = 'S';

   if v_achou = 0
   then
      raise_application_error(-20000,'Transacao informada para acerto de saida, nao e de saida ou nao existe.');
   end if;

   select count(*) into v_achou from estq_005
   where estq_005.codigo_transacao = :new.transacao_acerto_entrada
     and estq_005.entrada_saida    = 'E';

   if v_achou = 0
   then
      raise_application_error(-20000,'Transacao informada para acerto de entrada, nao e de entrada ou nao existe.');
   end if;

end inter_tr_basi_206;
-- ALTER TRIGGER "INTER_TR_BASI_206" ENABLE
 

/

exec inter_pr_recompile;

