CREATE OR REPLACE TRIGGER inter_tr_fatu_060
   before insert or delete or
   update of num_nota_ecf_ipi, observacao, pedido_venda, valor_iss,
  formulario_num, formulario_ser, formulario_seq, formulario_ori,
  um_faturamento_um, um_faturamento_qtde, um_faturamento_valor_unit, flag_guia_remissao,
  requisicao, seq_requisicao, executa_trigger, valor_pis,
  valor_cofins, perc_iss, cvf_pis, cvf_cofins,
  basi_pis_cofins, perc_pis, perc_cofins, cvf_ipi_saida,
  nota_ajuste, serie_ajuste, cod_csosn, valor_frete,
  valor_desc, valor_outros, valor_seguro, base_ipi,
  cvf_ipi, valor_icms, peso_liquido, descricao_item,
  centro_custo, unidade_medida, num_ordem_serv, seq_nota_orig,
  cgc9_origem, codigo_contabil, perc_iva_2, rateio_desc_propaganda,
  base_icms_difer, valor_icms_diferido, empresa_nota_reemissao, seq_nota_reemissao,
  ch_it_nf_cd_empr, ch_it_nf_num_nfis, ch_it_nf_ser_nfis, seq_item_nfisc,
  seq_item_pedido, nivel_estrutura, grupo_estrutura, subgru_estrutura,
  item_estrutura, lote_acomp, deposito, transacao,
  data_emissao, qtde_item_fatur, valor_unitario, valor_faturado,
  valor_contabil, desconto_item, natopeno_nat_oper, natopeno_est_oper,
  perc_ipi, valor_ipi, base_icms, perc_icms,
  cvf_icms, valor_icms_difer, cod_cancelamento, nr_solicitacao,
  atualizou_estq, classific_fiscal, classif_contabil, deposito_transf,
  cen_custo_transf, rateio_despesa, procedencia, seq_item_ordem,
  num_nota_orig, serie_nota_orig, motivo_devolucao, qtde_fatu_empe,
  cgc4_origem, cgc2_origem, flag_devolucao, obs_livro1,
  obs_livro2, perc_iva_1, valor_iva_1, valor_iva_2,
  atualizou_plano, sequencia_mao_obra, perc_mao_obra, valor_negociacao,
  perc_icms_diferido, usuario_cardex, nome_programa, data_canc_nfisc,
  transacao_canc_nfisc, numero_nota_reemissao, serie_nota_reemissao, nro_rolo_peca,
  rateio_despesas_ipi, rateio_descontos_ipi, processo_contabil_estoque
   on fatu_060
   for each row
declare
   v_atualiza_estoque      estq_005.atualiza_estoque    %type;
   v_tipo_volume           basi_205.tipo_volume         %type;
   v_cnpj_9                fatu_050.cgc_9               %type;
   v_cnpj_4                fatu_050.cgc_4               %type;
   v_cnpj_2                fatu_050.cgc_2               %type;
   v_pedido_venda_060      fatu_050.pedido_venda        %type;
   v_pedido_venda_020      fatu_050.pedido_venda        %type;
   v_pedido_venda_capa     fatu_050.pedido_venda        %type;
   v_nr_solicitacao        fatu_050.nr_solicitacao      %type;
   v_nr_solicitacao_020    fatu_050.nr_solicitacao      %type;
   v_emp_nota_fiscal       fatu_050.codigo_empresa      %type;
   v_num_nota_fiscal       fatu_050.num_nota_fiscal     %type;
   v_ser_nota_fiscal       fatu_050.serie_nota_fisc     %type;
   v_seq_nota_fiscal       fatu_060.seq_item_nfisc      %type;
   v_seq_item_pedido       fatu_060.seq_item_pedido     %type;
   v_tabela_origem         pcpt_020.tabela_origem_cardex%type;
   v_tipo_natureza         pedi_080.tipo_natureza       %type;
   v_origem_nota           fatu_050.origem_nota         %type;
   v_especie_docto         fatu_050.especie_docto       %type;
   v_tipo_transacao        estq_005.tipo_transacao      %type;
   v_tipo_transacao_005    estq_005.tipo_transacao      %type;
   v_trans_terceiro        estq_005.transac_entrada     %type;
   v_ent_sai_terc          estq_005.entrada_saida       %type;
   v_trans_terceiro_canc   estq_005.transac_entrada     %type;
   v_deposito_consig       basi_205.codigo_deposito     %type;
   v_deposito_terceiro     basi_205.icddepdestterceiros %type;
   v_atu_estoque_terceiro  estq_005.atualiza_estoque    %type;
   v_processo_nota         obrf_510.processo_nota       %type;
   v_cod_emp_benef         obrf_510.cod_emp_benef       %type;
   v_cod_emp_origem        obrf_510.cod_emp_origem      %type;
   v_centro_custo_terceiro hdoc_001.campo_numerico32    %type;

   v_sit_rolo                                    number;
   v_sit_rolo_ped                                number;
   v_sit_caixa                                   number;
   v_nr_registro_300                             number;
   v_situacao_nfisc                              number;
   v_valor_contabil                              number;
   v_par_atualiza_estoque_f                      number;
   v_atual_estoque_f                             varchar2(1);
   v_permite_fatura_atu_esq                      varchar2(1);

   /* zequiel */
   v_cod_canc_item_ped                           number;
   v_moeda                                       number;
   v_indice_moeda                                number;
   v_data_emis        fatu_050.data_emissao      %type;
   v_qtde             fatu_060.qtde_item_fatur   %type;
   v_cidadeempresa                               number;
   v_paisempresa                                 number;
   v_valor_unitario                              number;
   v_qtde_item_fatur                             number;
   v_pedido_nota_capa   fatu_050.pedido_venda    %type;

   v_peso_rolo_kg                                number;
   v_deposito_transicao                          number;
   v_limpa_pedido                                number;
   empresa_nota                                  number;
   produto_elab                                  number;
   outra_empresa                                 boolean;
   v_cont_processo                               number;
   v_tipo_processo                               number;

   /* Ricardo */
   v_dep_entrada_terc                            number;
   v_dep_entrada_terc_1                          number;
   v_dep_entrada_terc_2                          number;
   v_dep_entrada_terc_4                          number;
   v_dep_entrada_terc_7                          number;
   v_dep_entrada_terc_9                          number;
   v_cest                                        basi_010.cest%type;
   v_dep_terceiro_572                            basi_572.cod_dep%type;

   v_nfe_ambiente                                obrf_158.nfe_ambiente%type;
   v_data_legislacao                             obrf_158.data_legislacao%type;

   v_unid_med_trib_tmp                           fatu_060.unid_med_trib%type;

--   v_novo_pedido_venda                         number;
--   v_nova_seq_item_pedido                      number;

   v_executa_trigger                             number(1);
   v_data_canc_nfisc                             fatu_060.data_canc_nfisc%type;
   v_periodo_estoque                             empr_001.periodo_estoque%type;
   v_atualiza_estq_terceiro         estq_005.atualiza_estq_terceiro%type;


begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if not deleting  and trim(:new.cest) is null
      then
         begin
       select obrf_158.nfe_ambiente, obrf_158.data_legislacao
       into v_nfe_ambiente, v_data_legislacao
       from obrf_158
       where obrf_158.cod_empresa =  :new.ch_it_nf_cd_empr;
     exception
             when no_data_found then
         v_nfe_ambiente    := 0;
         v_data_legislacao := null;
         end;

         :new.cest := ' ';
         if v_nfe_ambiente = 2 or to_date(v_data_legislacao) <= to_date(sysdate)
         then
           begin
            select basi_010.cest into v_cest
            from basi_010
            where basi_010.nivel_estrutura  = :new.nivel_estrutura
             and basi_010.grupo_estrutura  = :new.grupo_estrutura
              and basi_010.subgru_estrutura = :new.subgru_estrutura
              and basi_010.item_estrutura   = :new.item_estrutura;
           exception
             when no_data_found then
             v_cest := ' ';
           end;
            :new.cest := v_cest;
         end if;
      end if;

      v_indice_moeda := 1;
      if inserting
      then
         -- le parametro de empresa que atualiza deposito ativo.
         select fatu_502.atualiza_estoque_f, fatu_502.permite_fatura_atu_esq
         into v_par_atualiza_estoque_f,      v_permite_fatura_atu_esq
         from fatu_502
         where fatu_502.codigo_empresa  = :new.ch_it_nf_cd_empr;

         -- le a transacao para verificar se ela atualiza estoque
         select atualiza_estoque, atual_estoque_f
         into v_atualiza_estoque, v_atual_estoque_f
         from estq_005
         where codigo_transacao = :new.transacao;

         if v_par_atualiza_estoque_f = 1 and v_atual_estoque_f = 'N' and v_atualiza_estoque = 1
         then
            raise_application_error(-20000,'Quando a empresa atualiza estoque gerencial, a transac?o tambem deve atualizar estoque gerencial.');
         end if;

         -- caso a transacao atualise estoque, checa se o deposito foi informado
         if v_atualiza_estoque = 1 and :new.deposito = 0
         then
            raise_application_error(-20000,'Se a transacao atualizar estoque, o deposito deve ser informado');
         end if;

         -- caso a transacao nao atualise estoque, checa se o deposito foi informado
         if v_atualiza_estoque = 2 and :new.deposito <> 0
         then
            raise_application_error(-20000,'Se a transacao n?o atualizar estoque, o deposito n?o deve ser informado');
         end if;

         -- caso o deposito foi informado e o produto e sem codigo, gera mensagem de erro
         if :new.deposito <> 0 and :new.nivel_estrutura = 0
         then
            raise_application_error(-20000,'Se informar um deposito, deve-se informar um produto de estoque');
         end if;

         select inter_fn_unid_med_tributaria(:new.natopeno_nat_oper,
                                             :new.natopeno_est_oper,
                                             decode(trim(:new.um_faturamento_um),null,:new.unidade_medida,:new.um_faturamento_um),
                                             :new.classific_fiscal,
                                             :new.nivel_estrutura,
                                             :new.grupo_estrutura,
                                             :new.subgru_estrutura,
                                             :new.item_estrutura
                                             )
         into v_unid_med_trib_tmp from dual;

         -- l? os par?metros de depositos de entrada em poder de terceiros
         v_dep_entrada_terc_1 := inter_fn_get_param_int(:new.ch_it_nf_cd_empr, 'estoque.depTercEntrada1');
         v_dep_entrada_terc_2 := inter_fn_get_param_int(:new.ch_it_nf_cd_empr, 'estoque.depTercEntrada2');
         v_dep_entrada_terc_4 := inter_fn_get_param_int(:new.ch_it_nf_cd_empr, 'estoque.depTercEntrada4');
         v_dep_entrada_terc_7 := inter_fn_get_param_int(:new.ch_it_nf_cd_empr, 'estoque.depTercEntrada7');
         v_dep_entrada_terc_9 := inter_fn_get_param_int(:new.ch_it_nf_cd_empr, 'estoque.depTercEntrada9');

         if :new.nivel_estrutura = '1'
         then v_dep_entrada_terc := v_dep_entrada_terc_1;
         elsif :new.nivel_estrutura = '2'
         then v_dep_entrada_terc := v_dep_entrada_terc_2;
         elsif :new.nivel_estrutura = '4'
         then v_dep_entrada_terc := v_dep_entrada_terc_4;
         elsif :new.nivel_estrutura = '7'
         then v_dep_entrada_terc := v_dep_entrada_terc_7;
         elsif :new.nivel_estrutura = '9'
         then v_dep_entrada_terc := v_dep_entrada_terc_9;
         end if;

         select cgc_9,    cgc_4,    cgc_2,    nr_solicitacao,   pedido_venda,        origem_nota,
                moeda_nota, situacao_nfisc
         into   v_cnpj_9, v_cnpj_4, v_cnpj_2, v_nr_solicitacao, v_pedido_venda_capa, v_origem_nota,
                v_moeda, v_situacao_nfisc
         from fatu_050
         where codigo_empresa  = :new.ch_it_nf_cd_empr
           and num_nota_fiscal = :new.ch_it_nf_num_nfis
           and serie_nota_fisc = :new.ch_it_nf_ser_nfis;

         -- Consistencias para controlar a atualizacao do estoque via pcpt_020
         -- Se o produto for em elaboracao
         -- Se a empresa da nota e a mesma na qual esta movimentando
         -- Se existe deposito de mercadoria em transito
         -- Nao deve atualizar estoque (estq_300) por esta trigger
         -- Mas sim pela trigger da pcpt_020
         produto_elab := 1;

         begin
             select 1
             into   produto_elab
             from pedi_860
             where pedi_860.niv_prod_elab  = :new.nivel_estrutura
               and pedi_860.gru_prod_elab  = :new.grupo_estrutura
               and pedi_860.sub_prod_elab  = :new.subgru_estrutura
               and pedi_860.item_prod_elab = :new.item_estrutura;
           exception
             when no_data_found
             then produto_elab := 0;
         end;

         begin
             select min(fatu_500.codigo_empresa)
             into   empresa_nota
             from fatu_500
             where fatu_500.cgc_9 = v_cnpj_9
               and fatu_500.cgc_4 = v_cnpj_4
               and fatu_500.cgc_2 = v_cnpj_2;
         end;

         if empresa_nota is null
         then empresa_nota := 0;
         end if;

         if empresa_nota <> :new.ch_it_nf_cd_empr and
            empresa_nota  > 0
         then outra_empresa := true;
         else outra_empresa := false;
         end if;

         select empr_002.deposito_transicao, empr_002.limpa_pedido
         into   v_deposito_transicao, v_limpa_pedido
         from empr_002;

        /*Atualiza o valor para a moeda corrente*/
         if v_moeda <> 0
         then
            begin
               select valor_moeda into v_indice_moeda
               from basi_270
               where basi_270.data_moeda   = :new.data_emissao
                 and basi_270.codigo_moeda = v_moeda;
               exception
                  when others then
                    v_indice_moeda := 1;
            end;
          else
             v_indice_moeda := 1;
          end if;



         if v_pedido_venda_capa > 0
         and (:new.pedido_venda is null or :new.pedido_venda = 0)
         then
            :new.pedido_venda := v_pedido_venda_capa;
         end if;

         if :new.deposito > 0
         then
            select tipo_volume into v_tipo_volume from basi_205
            where codigo_deposito = :new.deposito;

            -- le a natureza de operacao para verificar se a mesma e de servico (tipo_natureza = 1)
            begin
               select tipo_natureza into v_tipo_natureza from pedi_080
               where natur_operacao = :new.natopeno_nat_oper
               and   estado_natoper = :new.natopeno_est_oper;

               exception
                  when others then
                     v_tipo_natureza := 0;
            end;

            if :new.qtde_item_fatur = 0.00
            then v_qtde := 1.00;
            else v_qtde := :new.qtde_item_fatur;
            end if;

            -- LOGICA PARA PROCURAR O PAIS DA EMPRESA DA NOTA
            v_valor_contabil := :new.valor_contabil /  v_qtde;
            v_valor_unitario := :new.valor_unitario;
            v_qtde_item_fatur := :new.qtde_item_fatur;

            begin
               select fatu_500.codigo_cidade into v_cidadeempresa from fatu_500
               where fatu_500.codigo_empresa = :new.ch_it_nf_cd_empr;
               exception
                  when others then
                     v_cidadeempresa := 0;
            end;

            begin
               select basi_160.codigo_pais into v_paisempresa from basi_160
               where basi_160.cod_cidade = v_cidadeempresa;
               exception
                  when others then
                     v_paisempresa := 1;
            end;

            if v_paisempresa <> 1
            then
               if v_especie_docto = 'FAC' -- 1 FAC - NOTA FATURA
               then
                  if not(v_atualiza_estoque = 1 and v_permite_fatura_atu_esq = 'S')
                  then
                      v_qtde_item_fatur := 0.000;
                  else
                      v_qtde_item_fatur := :new.qtde_item_fatur;
                  end if;
               else
                  v_qtde_item_fatur := :new.qtde_item_fatur;
               end if;

               if v_especie_docto = 'GRE' -- 5 GRE - NOTA REMISSAO
               then
                  v_valor_unitario := 0.00;
                  v_valor_contabil := 0.00;
               else
                  v_valor_unitario := :new.valor_unitario;
               end if;
            end if;

            -- se o tipo da natureza for de servico e o nivel for '2' ou '4' e o tipo volume for '2' ou '4'
            -- NAO atualiza o estoque, pois o mesmo e atualizado pela baixa do rolo, que e feita abaixo pela
            -- trigger

            select estq_005.entrada_saida into v_tipo_transacao
            from estq_005
            where codigo_transacao = :new.transacao;

            if not (v_tipo_natureza = 1 and :new.nivel_estrutura in ('2','4') and v_tipo_volume in ('2','4') and
               not (produto_elab    = 1 and outra_empresa = true and v_deposito_transicao > 0) and
              :new.pedido_venda  > 0 and :new.numero_nota_reemissao = 0) and v_tipo_transacao <> 'T'
            then

               ---------------------------------------------------------------------------------------
               -- INICIO DA ROTINA DE CALCULO DO PESO DOS ROLOS
               ---------------------------------------------------------------------------------------

               -- inicializa a variavel de leitura dos rolos para a busca dos pesos para gravar no
               -- movimento de estoque
               v_peso_rolo_kg := 0.00;

               -- se o deposito for de rolos, encontra o peso dos volumes
               if :new.nivel_estrutura in ('2','4') and v_tipo_volume in (2,4)
               then

                  -- faturamento por loja
                  if  :new.pedido_venda          = 0 and v_nr_solicitacao > 0
                  and :new.numero_nota_reemissao = 0 and v_origem_nota    = 3
                  then

                     select sum(nvl(peso_bruto, 0)) into v_peso_rolo_kg
                     from pcpt_020
                     where pedido_loja        = v_nr_solicitacao
                     and   seq_item_pedido    = :new.seq_item_pedido
                     and   panoacab_nivel99   = :new.nivel_estrutura
                     and   panoacab_grupo     = :new.grupo_estrutura
                     and   panoacab_subgrupo  = :new.subgru_estrutura
                     and   panoacab_item      = :new.item_estrutura
                     and   codigo_deposito    = :new.deposito
                     and   lote_acomp         = :new.lote_acomp
                     and   cod_empresa_nota   = :new.ch_it_nf_cd_empr
                     and   rolo_estoque       = 3;      -- coletado para faturamento/ loja
                  else

                     -- faturamento
                     if:new.pedido_venda > 0 and :new.numero_nota_reemissao = 0
                     then

                        if v_tipo_transacao <> 'T'
                        then

                            select sum(nvl(peso_bruto, 0)) into v_peso_rolo_kg
                            from pcpt_020
                            where pedido_venda    = :new.pedido_venda
                            and   nr_solic_volume = v_nr_solicitacao
                            and   seq_item_pedido = :new.seq_item_pedido
                            and   rolo_estoque    = 3;      -- coletado para faturamento
                         else

                            /*NOVO UPDATE*/

                            select cgc_9,       cgc_4,
                                   cgc_2
                            into   v_cnpj_9,    v_cnpj_4,
                                   v_cnpj_2
                            from fatu_050
                            where codigo_empresa  = :new.ch_it_nf_cd_empr
                              and num_nota_fiscal = :new.ch_it_nf_num_nfis
                              and serie_nota_fisc = :new.ch_it_nf_ser_nfis;

                            /*a partir do clinete, busca o deposito de consignacao.
                              se nao achar, nao faz o update. (obrf_f206)*/
                            begin
                               select obrf_206.deposito into v_deposito_consig
                               from obrf_206
                               where obrf_206.cnpj9_cli      = v_cnpj_9
                                 and obrf_206.cnpj4_cli      = v_cnpj_4
                                 and obrf_206.cnpj2_cli      = v_cnpj_2
                                 and obrf_206.natur_operacao = :new.natopeno_nat_oper
                                 and obrf_206.natur_estado   = :new.natopeno_est_oper;
                               exception
                               when others then
                                  v_deposito_consig := 0;
                            end;

                            if v_deposito_consig > 0
                            then

                               select sum(nvl(peso_bruto, 0)) into v_peso_rolo_kg
                               from pcpt_020
                               where pedido_venda    =:new.pedido_venda
                               and   nr_solic_volume = v_nr_solicitacao
                               and   seq_item_pedido = :new.seq_item_pedido;
                            end if;
                         end if;

                     else

                        --- Nota de OBRF relacionada a ordem de servico
                        if :new.num_ordem_serv > 0 and :new.nivel_estrutura = '4' and :new.numero_nota_reemissao = 0
                        then

                           select sum(nvl(peso_bruto, 0)) into v_peso_rolo_kg
                           from pcpt_020
                           where ordem_producao        = :new.num_ordem_serv
                             and seq_ordem             > 0
                             and rolo_estoque          = 7  -- Relacionado a ordem de servico
                             and panoacab_nivel99      = :new.nivel_estrutura
                             and panoacab_grupo        = :new.grupo_estrutura
                             and panoacab_subgrupo     = :new.subgru_estrutura
                             and panoacab_item         = :new.item_estrutura
                             and codigo_deposito       = :new.deposito
                             and lote_acomp            = :new.lote_acomp;

                          else
                             -- Obrigacoes fiscais ou Reemissao de nota gerando de nova nota

                             if :new.numero_nota_reemissao = 0 -- Obrigacoes fiscais
                             then
                                v_sit_rolo           := 1; -- Rolo em estoque
                                v_seq_nota_fiscal    := 0;
                             else -- Reemissao de nota gerando de nova nota
                                v_sit_rolo           := 8; -- Rolo com nota reemitida
                                v_seq_nota_fiscal    := :new.seq_item_nfisc;
                             end if;

                             select sum(nvl(peso_bruto, 0)) into v_peso_rolo_kg
                             from pcpt_020
                             where cod_empresa_nota   = :new.ch_it_nf_cd_empr
                             and   nota_fiscal        = :new.ch_it_nf_num_nfis
                             and   serie_fiscal_sai   = :new.ch_it_nf_ser_nfis
                             and   seq_nota_fiscal    = v_seq_nota_fiscal
                             and   rolo_estoque       = v_sit_rolo;

                         end if;   -- if :new.num_ordem_serv > 0 and :new.nivel_estrutura = '4' --- Nota de OBRF relacionada a ordem de servico
                      end if;      -- faturamento
                  end if;          -- faturamento por loja
               end if;

               ---------------------------------------------------------------------------------------
               -- FIM DA ROTINA DE CALCULO DO PESO DOS ROLOS
               ---------------------------------------------------------------------------------------

               if v_situacao_nfisc <> 2
               then
                    inter_pr_insere_estq_300_recur (:new.deposito,         :new.nivel_estrutura,   :new.grupo_estrutura,
                                         :new.subgru_estrutura, :new.item_estrutura,    :new.data_emissao,
                                         :new.lote_acomp,       :new.ch_it_nf_num_nfis, :new.ch_it_nf_ser_nfis,
                                         v_cnpj_9,              v_cnpj_4,                 v_cnpj_2,
                                         :new.seq_item_nfisc,
                                         :new.transacao,        'S',                    :new.centro_custo,
                                         v_qtde_item_fatur,      v_valor_unitario * v_indice_moeda,    v_valor_contabil * v_indice_moeda,
                                         :new.usuario_cardex,   'FATU_060',             :new.nome_programa,
                                         v_peso_rolo_kg,         0,                       0,
                                         :new.ch_it_nf_num_nfis, 0,                       0);
               end if;
            end if;

            if v_tipo_volume <> 0
            then
               -- :new.numero_nota_reemissao - Este campo identifica se estamos reemitindo a
               --                              nota fiscal (fatu_e254) com o cancelamento da
               --                              mesma e gerando uma nova nota.
               --                              Este campo e atualizado da seguinte forma:
               --                              1) Na nota cancelada: Neste caso atualizamos com o
               --                                 numero da nova nota fiscal que sera gerada
               --                                 na reemissao.
               --                              2) Na nota nova: Neste caso atualizamos com o numero
               --                                 da nota fiscal cancelada que gerou a nova nota.
               --                              Depois de utiliza-lo para update e insert este campo e zerado.


               if :new.nivel_estrutura in ('2','4') and v_tipo_volume in (2,4)
               then

                  -- faturamento por loja
                  if  :new.pedido_venda          = 0 and v_nr_solicitacao > 0
                  and :new.numero_nota_reemissao = 0 and v_origem_nota    = 3
                  then

                     update pcpt_020
                     set rolo_estoque         = 2,
                         nota_fiscal          = :new.ch_it_nf_num_nfis,
                         serie_fiscal_sai     = :new.ch_it_nf_ser_nfis,
                         cod_empresa_nota     = :new.ch_it_nf_cd_empr,
                         seq_nota_fiscal      = :new.seq_item_nfisc,
                         data_cardex          = :new.data_emissao,
                         transacao_cardex     = :new.transacao,
                         usuario_cardex       = :new.usuario_cardex,
                         nome_prog_020        = :new.nome_programa,
                         tabela_origem_cardex = 'FATU_060',
                         pedido_venda         = 0,
                         dep_origem_faturamento = decode(dep_origem_faturamento,0,:new.deposito,dep_origem_faturamento)
                     where pedido_loja        = v_nr_solicitacao
                     and   seq_item_pedido    = :new.seq_item_pedido
                     and   panoacab_nivel99   = :new.nivel_estrutura
                     and   panoacab_grupo     = :new.grupo_estrutura
                     and   panoacab_subgrupo  = :new.subgru_estrutura
                     and   panoacab_item      = :new.item_estrutura
                     and   codigo_deposito    = :new.deposito
                     and   lote_acomp         = :new.lote_acomp
                     and   cod_empresa_nota   = :new.ch_it_nf_cd_empr
                     and   rolo_estoque       = 3;      -- coletado para faturamento/ loja
                  else
                     -- faturamento
                     if:new.pedido_venda > 0 and :new.numero_nota_reemissao = 0
                     then

                        if v_tipo_transacao <> 'T'
                        then
                            -- se a natureza de operacao nao for de servico, seta a a variavel de controle
                            -- v_tabela_origem para 'FATU_060', para que a trigger INTER_TR_PCPT_020 nao atualize
                            -- o estoque, pois o mesmo foi atualizado pelo registro do fatu_060.
                            -- Caso contrario, seta a a variavel de controle v_tabela_origem para
                            -- 'PCPT_020', para que a trigger INTER_TR_PCPT_020 atualize o estoque, pois quando
                            -- a natureza de operacao for de servico, o estoque deve ser baixado pelo peso real do rolo
                            -- e nao pela nota fiscal.
                            if v_tipo_natureza = 0 or
                               v_tipo_natureza = 2
                            then
                               v_tabela_origem := 'FATU_060';
                            else
                               v_tabela_origem := 'PCPT_020';
                            end if;

                            update pcpt_020
                            set rolo_estoque         = 2,
                                nota_fiscal          = :new.ch_it_nf_num_nfis,
                                serie_fiscal_sai     = :new.ch_it_nf_ser_nfis,
                                cod_empresa_nota     = :new.ch_it_nf_cd_empr,
                                seq_nota_fiscal      = :new.seq_item_nfisc,
                                data_cardex          = :new.data_emissao,
                                transacao_cardex     = :new.transacao,
                                usuario_cardex       = :new.usuario_cardex,
                                nome_prog_020        = :new.nome_programa,
                                tabela_origem_cardex = v_tabela_origem,
                                dep_origem_faturamento = decode(dep_origem_faturamento,0,:new.deposito,dep_origem_faturamento)
                            where pedido_venda    =:new.pedido_venda
                            and   nr_solic_volume = v_nr_solicitacao
                            and   seq_item_pedido = :new.seq_item_pedido
                            and   rolo_estoque    = 3;      -- coletado para faturamento
                         else
                            /*NOVO UPDATE*/

                            select cgc_9,       cgc_4,
                                   cgc_2
                            into   v_cnpj_9,    v_cnpj_4,
                                   v_cnpj_2
                            from fatu_050
                            where codigo_empresa  = :new.ch_it_nf_cd_empr
                              and num_nota_fiscal = :new.ch_it_nf_num_nfis
                              and serie_nota_fisc = :new.ch_it_nf_ser_nfis;

                            /*a partir do clinete, busca o deposito de consignacao.
                              se nao achar, nao faz o update. (obrf_f206)*/
                            begin
                               select obrf_206.deposito into v_deposito_consig
                               from obrf_206
                               where obrf_206.cnpj9_cli      = v_cnpj_9
                                 and obrf_206.cnpj4_cli      = v_cnpj_4
                                 and obrf_206.cnpj2_cli      = v_cnpj_2
                                 and obrf_206.natur_operacao = :new.natopeno_nat_oper
                                 and obrf_206.natur_estado   = :new.natopeno_est_oper;
                               exception
                               when others then
                                  v_deposito_consig := 0;
                            end;

                            if v_deposito_consig > 0
                            then

                                update pcpt_020
                                set rolo_estoque         = 1,
                                    nota_fiscal          = :new.ch_it_nf_num_nfis,
                                    serie_fiscal_sai     = :new.ch_it_nf_ser_nfis,
                                    cod_empresa_nota     = :new.ch_it_nf_cd_empr,
                                    seq_nota_fiscal      = :new.seq_item_nfisc,
                                    data_cardex          = :new.data_emissao,
                                    transacao_cardex     = :new.transacao,
                                    usuario_cardex       = :new.usuario_cardex,
                                    nome_prog_020        = :new.nome_programa,
                                    codigo_deposito      = v_deposito_consig,
                                    tabela_origem_cardex = 'PCPT_020',
                                    dep_origem_faturamento = decode(dep_origem_faturamento,0,:new.deposito,dep_origem_faturamento)
                                where pedido_venda    =:new.pedido_venda
                                and   nr_solic_volume = v_nr_solicitacao
                                and   seq_item_pedido = :new.seq_item_pedido;
                            end if;
                         end if;

                     else

                        --- Nota de OBRF relacionada a ordem de servico
                        if :new.num_ordem_serv > 0 and :new.nivel_estrutura = '4' and :new.numero_nota_reemissao = 0
                        then

                           update pcpt_020
                           set rolo_estoque            = 2,
                               nota_fiscal             = :new.ch_it_nf_num_nfis,
                               serie_fiscal_sai        = :new.ch_it_nf_ser_nfis,
                               cod_empresa_nota        = :new.ch_it_nf_cd_empr,
                               seq_nota_fiscal         = :new.seq_item_nfisc,
                               data_cardex             = :new.data_emissao,
                               transacao_cardex        = :new.transacao,
                               usuario_cardex          = :new.usuario_cardex,
                               nome_prog_020           = :new.nome_programa,
                               tabela_origem_cardex    = 'FATU_060',
                               dep_origem_faturamento = decode(dep_origem_faturamento,0,:new.deposito,dep_origem_faturamento)
                           where ordem_producao        = :new.num_ordem_serv
                             and seq_ordem             > 0
                             and rolo_estoque          = 7  -- Relacionado a ordem de servico
                             and panoacab_nivel99      = :new.nivel_estrutura
                             and panoacab_grupo        = :new.grupo_estrutura
                             and panoacab_subgrupo     = :new.subgru_estrutura
                             and panoacab_item         = :new.item_estrutura
                             and codigo_deposito       = :new.deposito
                             and lote_acomp            = :new.lote_acomp;

                          else
                             -- Obrigacoes fiscais ou Reemissao de nota gerando de nova nota

                             -- Consistencias para controlar a atualizacao do estoque via pcpt_020
                             -- Se o produto for em elaboracao
                             -- Se a empresa da nota e a mesma na qual esta movimentando
                             -- Se existe deposito de mercadoria em transito
                             -- Nao deve atualizar estoque (estq_300) por esta trigger
                             -- Mas sim pela trigger da pcpt_020
                             if produto_elab  = 1 and
                                outra_empresa = true and
                                v_deposito_transicao > 0
                             then

                                 update pcpt_020
                                 set seq_nota_fiscal      = :new.seq_item_nfisc,
                                     data_cardex          = :new.data_emissao,
                                     codigo_deposito      = v_deposito_transicao,
                                     transacao_cardex     = :new.transacao,
                                     usuario_cardex       = :new.usuario_cardex,
                                     nome_prog_020        = :new.nome_programa,
                                     tabela_origem_cardex = 'PCPT_020',
                                     dep_origem_faturamento = decode(dep_origem_faturamento,0,:new.deposito,dep_origem_faturamento)
                                 where cod_empresa_nota   = :new.ch_it_nf_cd_empr
                                 and   nota_fiscal        = :new.ch_it_nf_num_nfis
                                 and   serie_fiscal_sai   = :new.ch_it_nf_ser_nfis
                                 and   seq_nota_fiscal    = 0
                                 and   rolo_estoque      in (1,3);

                             else
                                 if :new.numero_nota_reemissao = 0 -- Obrigacoes fiscais
                                 then
                                    v_sit_rolo           := 1; -- Rolo em estoque
                                    v_sit_rolo_ped       := 3;       
                                    v_seq_nota_fiscal    := 0;
                                    v_pedido_venda_020   := 0;
                                    v_nr_solicitacao_020 := 0;
                                 else -- Reemissao de nota gerando de nova nota
                                    v_sit_rolo           := 8; -- Rolo com nota reemitida
                                    v_sit_rolo_ped       := 8;       
                                    v_seq_nota_fiscal    := :new.seq_item_nfisc;
                                    v_pedido_venda_020   := :new.pedido_venda;
                                    v_nr_solicitacao_020 := v_nr_solicitacao;
                                 end if;

                                 update pcpt_020
                                 set rolo_estoque         = 2,
                                     pedido_venda         = decode(pre_romaneio,0,v_pedido_venda_020,pedido_venda),
                                     nr_solic_volume      = v_nr_solicitacao_020,
                                     seq_nota_fiscal      = :new.seq_item_nfisc,
                                     data_cardex          = :new.data_emissao,
                                     transacao_cardex     = :new.transacao,
                                     usuario_cardex       = :new.usuario_cardex,
                                     nome_prog_020        = :new.nome_programa,
                                     tabela_origem_cardex = 'FATU_060',
                                     dep_origem_faturamento = decode(dep_origem_faturamento,0,:new.deposito,dep_origem_faturamento)
                                 where cod_empresa_nota   = :new.ch_it_nf_cd_empr
                                 and   nota_fiscal        = :new.ch_it_nf_num_nfis
                                 and   serie_fiscal_sai   = :new.ch_it_nf_ser_nfis
                                 and   seq_nota_fiscal    = v_seq_nota_fiscal
                                 and   rolo_estoque       in(v_sit_rolo,decode(v_limpa_pedido,1,v_sit_rolo_ped,v_sit_rolo));
                             end if;
                         end if;   -- if :new.num_ordem_serv > 0 and :new.nivel_estrutura = '4' --- Nota de OBRF relacionada a ordem de servico
                      end if;      -- faturamento
                  end if;          -- faturamento por loja
               end if;             -- if (:new.nivel_estrutura = '2' or :new.nivel_estrutura = '4') and v_tipo_volume in (2,4)

               if :new.nivel_estrutura = '1' and v_tipo_volume = 1
               then

                  -- faturamento por loja
                  if  :new.pedido_venda          = 0 and v_nr_solicitacao > 0
                  and :new.numero_nota_reemissao = 0 and v_origem_nota    = 3
                  then
                     update pcpc_330
                     set estoque_tag          = 4,
                         nota_inclusao        = :new.ch_it_nf_num_nfis,
                         serie_nota           = :new.ch_it_nf_ser_nfis,
                         emp_saida            = :new.ch_it_nf_cd_empr,
                         seq_saida            = :new.seq_item_nfisc,
                         data_cardex          = :new.data_emissao,
                         transacao_cardex     = :new.transacao,
                         usuario_cardex       = :new.usuario_cardex,
                         nome_prog_050        = :new.nome_programa,
                         tabela_origem_cardex = 'FATU_060',
                         pedido_venda         = 0
                     where pedido_venda       = v_nr_solicitacao
                     and   seq_item_pedido    = :new.seq_item_pedido
                     and   estoque_tag        = 3;      -- coletado para faturamento/LOJA

                  else
                     -- faturamento
                     if:new.pedido_venda > 0 and :new.numero_nota_reemissao = 0
                     then

                        -- se a natureza de operacao nao for de servico, seta a a variavel de controle
                        -- v_tabela_origem para 'FATU_060', para que a trigger INTER_TR_PCPC_330 nao atualize
                        -- o estoque, pois o mesmo foi atualizado pelo registro do fatu_060.
                        -- Caso contrario, seta a a variavel de controle v_tabela_origem para
                        -- 'PCPC_330', para que a trigger INTER_TR_PCPC_330 atualize o estoque, pois quando
                        if v_tipo_natureza = 0 or
                           v_tipo_natureza = 2
                        then
                           v_tabela_origem := 'FATU_060';
                        else
                           v_tabela_origem := 'PCPC_330';
                        end if;

                        update pcpc_330
                        set estoque_tag          = 4,
                            nota_inclusao        = :new.ch_it_nf_num_nfis,
                            serie_nota           = :new.ch_it_nf_ser_nfis,
                            emp_saida            = :new.ch_it_nf_cd_empr,
                            seq_saida            = :new.seq_item_nfisc,
                            data_cardex          = :new.data_emissao,
                            transacao_cardex     = :new.transacao,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog_050        = :new.nome_programa,
                            tabela_origem_cardex = v_tabela_origem
                        where pedido_venda       = :new.pedido_venda
                        and   seq_item_pedido    = :new.seq_item_pedido
                        and   estoque_tag        = 3       -- coletado para faturamento
                        and  nvl(sit_faturamento, 0) <> 1; -- volume suspenso
                     else

                        -- Obrigacoes fiscais ou Reemissao de nota gerando de nova nota

                        if :new.numero_nota_reemissao = 0 -- Obrigacoes fiscais
                        then
                           v_sit_caixa       := 1; -- TAG em estoque
                           v_seq_nota_fiscal := 0;
                        else -- Reemissao de nota gerando de nova nota
                           v_sit_caixa       := 8; -- TAG com nota reemitida
                           v_seq_nota_fiscal := :new.seq_item_nfisc;
                        end if;

                        update pcpc_330
                        set estoque_tag          = 4,
                            seq_saida            = :new.seq_item_nfisc,
                            data_cardex          = :new.data_emissao,
                            transacao_cardex     = :new.transacao,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog_050        = :new.nome_programa,
                            tabela_origem_cardex = 'FATU_060'
                        where emp_saida          = :new.ch_it_nf_cd_empr
                     and   nota_inclusao      = :new.ch_it_nf_num_nfis
                        and   serie_nota         = :new.ch_it_nf_ser_nfis
                        and   seq_saida          = v_seq_nota_fiscal
                        and   estoque_tag        = v_sit_caixa;
                     end if;      -- faturamento
                  end if;         -- faturamento por loja
               end if;            -- if (:new.nivel_estrutura = '1' and v_tipo_volume  = 1


               if :new.nivel_estrutura = '7' and v_tipo_volume = 7
               then

                  -- faturamento
                  if:new.pedido_venda > 0 and :new.numero_nota_reemissao = 0
                  then

                     update estq_060
                     set status_caixa         = 4,
                         cod_empresa_sai      = :new.ch_it_nf_cd_empr,
                         nota_fiscal          = :new.ch_it_nf_num_nfis,
                         serie_nota           = :new.ch_it_nf_ser_nfis,
                         seq_nota_fiscal      = :new.seq_item_nfisc,
                         data_cardex          = :new.data_emissao,
                         transacao_cardex     = :new.transacao,
                         usuario_cardex       = :new.usuario_cardex,
                         nome_prog            = :new.nome_programa,
                         tabela_origem_cardex = 'FATU_060'
                     where numero_pedido    =:new.pedido_venda
                     and   nr_solicitacao   = v_nr_solicitacao
                     and   sequencia_pedido = :new.seq_item_pedido
                     and   lote             = :new.lote_acomp
                     and   status_caixa     = 3;      -- coletado para faturamento
                  else             -- obrigacoes fiscais

                     -- Obrigacoes fiscais ou Reemissao de nota gerando de nova nota

                     if :new.numero_nota_reemissao = 0 -- Obrigacoes fiscais
                     then
                        v_sit_caixa       := 1; -- Caixa em estoque
                        v_seq_nota_fiscal := 0;
                     else -- Reemissao de nota gerando de nova nota
                        v_sit_caixa       := 7; -- Caixa com nota reemitida
                        v_seq_nota_fiscal := :new.seq_item_nfisc;
                     end if;

                     update estq_060
                     set status_caixa         = 4,
                         cod_empresa_sai      = :new.ch_it_nf_cd_empr,
                         seq_nota_fiscal      = :new.seq_item_nfisc,
                         data_cardex          = :new.data_emissao,
                         transacao_cardex     = :new.transacao,
                         usuario_cardex       = :new.usuario_cardex,
                         nome_prog            = :new.nome_programa,
                         tabela_origem_cardex = 'FATU_060'
                     where nota_fiscal      = :new.ch_it_nf_num_nfis
                     and   serie_nota       = :new.ch_it_nf_ser_nfis
                     and   seq_nota_fiscal  = v_seq_nota_fiscal
                     and   prodcai_nivel99  = :new.nivel_estrutura
                     and   prodcai_grupo    = :new.grupo_estrutura
                     and   prodcai_subgrupo = :new.subgru_estrutura
                     and   prodcai_item     = :new.item_estrutura
                     and   lote             = :new.lote_acomp
                     and   status_caixa     = v_sit_caixa;
                  end if;          -- faturamento
               end if;             -- if :new.nivel_estrutura = '7' and v_tipo_volume = 7

               if :new.nivel_estrutura = '9' and v_tipo_volume = 9
               then

                  -- fardo so tem na obrigacao fiscal
                  update pcpf_060
                  set status_fardo     = 2,
                      seq_nota_sai     = :new.seq_item_nfisc,
                      data_cardex      = :new.data_emissao,
                      transacao_cardex = :new.transacao,
                      usuario_cardex   = :new.usuario_cardex,
                      nome_prog        = :new.nome_programa,
                      tabela_origem_cardex = 'FATU_060'
                  where emp_nota_sai = :new.ch_it_nf_cd_empr
                  and   num_nota_sai = :new.ch_it_nf_num_nfis
                  and   ser_nota_sai = :new.ch_it_nf_ser_nfis
                  and   status_fardo = 1;      -- fardo em estoque
              end if;              -- if :new.nivel_estrutura = '9' and v_tipo_volume = 9
            end if;                -- if v_tipo_volume <> 0
         end if;                   -- if :new.deposito > 0

         /*INICIO MOVIMENTACAO DE ESTOQUE NO DEPOSITO PROPRIO EM PODER DE TERCEIROS
           SS: 60598/001 - THIAGO.FELIPE*/
         begin
            select estq_005.icdtransdepterceiros,    estq_005.tipo_transacao,
                   estq_005.entrada_saida,           estq_005.atualiza_estq_terceiro
            into v_trans_terceiro,                   v_tipo_transacao_005,
     v_ent_sai_terc,                     v_atualiza_estq_terceiro
            from estq_005
            where estq_005.codigo_transacao = :new.transacao;
         exception
             when no_data_found then
                v_trans_terceiro     := 0;
    v_ent_sai_terc       := '';
                v_tipo_transacao_005 := 'V';
                v_atualiza_estq_terceiro := 0;
         end;

         /*Verifica deposito relacionado ao terceiro*/
         begin
            select basi_572.cod_dep
            into v_dep_terceiro_572
            from basi_572
            where basi_572.cod_empresa = :new.ch_it_nf_cd_empr
              and basi_572.terc_cnpj9  = v_cnpj_9
              and basi_572.terc_cnpj4  = v_cnpj_4
              and basi_572.terc_cnpj2  = v_cnpj_2;
            exception
            when no_data_found then
                 v_dep_terceiro_572     := 0;
         end;

         if ( v_dep_terceiro_572 = 0 and  (v_tipo_transacao_005 = 'E' or v_tipo_transacao_005 = 'D' or
                                           v_tipo_transacao_005 = 'I' or v_tipo_transacao_005 = 'N' or v_atualiza_estq_terceiro = 1) )
            or
            ( v_dep_terceiro_572 <> 0 and  (v_tipo_transacao_005 = 'R' or v_tipo_transacao_005 = 'E' or
                                            v_tipo_transacao_005 = 'I' or v_tipo_transacao_005 = 'D' or v_atualiza_estq_terceiro = 1) )
         then

            begin
               select estq_005.atualiza_estoque, estq_005.entrada_saida
               into v_atu_estoque_terceiro, v_ent_sai_terc
               from estq_005
               where estq_005.codigo_transacao = v_trans_terceiro;
            exception
                when no_data_found then
                   v_atu_estoque_terceiro := 0;
       v_ent_sai_terc         := '';
            end;

            if v_dep_terceiro_572 = 0
            then
               if :new.deposito = 0 and (v_trans_terceiro > 0 and v_dep_entrada_terc >0)
               then
                  v_deposito_terceiro := v_dep_entrada_terc;
               elsif :new.deposito > 0
               then
                  begin
                     select basi_205.icddepdestterceiros into v_deposito_terceiro from basi_205
                     where basi_205.codigo_deposito = :new.deposito;
                  exception
                      when no_data_found then
                         v_deposito_terceiro := 0;
                  end;
               else
                  begin
                     select 1 into v_cont_processo from hdoc_001
                     where hdoc_001.tipo = 350
                       and hdoc_001.codigo > 0
                       and hdoc_001.codigo2 > 0;
                  exception
                      when no_data_found then
                         v_cont_processo := 0;
                  end;

                  if v_cont_processo > 0
                  then
                     begin
                        select obrf_510.processo_nota, obrf_510.cod_emp_benef,
                               obrf_510.cod_emp_origem
                        into   v_processo_nota,        v_cod_emp_benef,
                               v_cod_emp_origem
                        from obrf_510, (select obrf_510.cod_emp_benef, obrf_510.cod_emp_origem, obrf_510.seq_exec_rotina, max(obrf_510.seq_nota) seq_nota
                                        from obrf_510
                                        where obrf_510.cod_empresa_nota  = :new.ch_it_nf_cd_empr
                                          and obrf_510.numero_nota       = :new.ch_it_nf_num_nfis
                                          and obrf_510.serie_nota        = :new.ch_it_nf_ser_nfis
                                          and obrf_510.cgc9_cli_for_nota = v_cnpj_9
                                          and obrf_510.cgc4_cli_for_nota = v_cnpj_4
                                          and obrf_510.cgc2_cli_for_nota = v_cnpj_2
                                        group by obrf_510.cod_emp_benef, obrf_510.cod_emp_origem, obrf_510.seq_exec_rotina) aux_obrf_510
                         where obrf_510.cod_emp_benef   = aux_obrf_510.cod_emp_benef
                           and obrf_510.cod_emp_origem  = aux_obrf_510.cod_emp_origem
                           and obrf_510.seq_exec_rotina = aux_obrf_510.seq_exec_rotina
                           and obrf_510.seq_nota        = aux_obrf_510.seq_nota;
                     exception
                         when no_data_found then
                            v_processo_nota  := 0;
                            v_cod_emp_benef  := 0;
                            v_cod_emp_origem := 0;
                     end;

                     if v_processo_nota > 0 and v_cod_emp_benef > 0 and v_cod_emp_origem > 0
                     then

                        if v_processo_nota = 6
                        then
                           v_tipo_processo := 356;
                        end if;

                        if v_processo_nota = 12
                        then
                           v_tipo_processo := 362;
                        end if;

                        if v_processo_nota = 16
                        then
                           v_tipo_processo := 366;
                        end if;

                        begin
                           select hdoc_001.campo_numerico31,  hdoc_001.campo_numerico32
                           into   v_deposito_terceiro,        v_centro_custo_terceiro
                           from hdoc_001
                           where hdoc_001.tipo    = v_tipo_processo
                             and hdoc_001.codigo  = v_cod_emp_benef
                             and hdoc_001.codigo2 = v_cod_emp_origem;
                        exception
                            when no_data_found then
                               v_deposito_terceiro := 0;
                               v_centro_custo_terceiro := 0;
                        end;
                     end if;
                  end if; --if v_cont_processo > 0
               end if;
            else
               v_deposito_terceiro := v_dep_terceiro_572 ;
            end if;

            if v_trans_terceiro > 0 and v_deposito_terceiro > 0
            then
               if :new.qtde_item_fatur = 0.00
               then v_qtde := 1.00;
               else v_qtde := :new.qtde_item_fatur;
               end if;
               -- LOGICA PARA PROCURAR O PAIS DA EMPRESA DA NOTA
               v_valor_contabil := :new.valor_contabil /  v_qtde;
               v_valor_unitario := :new.valor_unitario;
               v_qtde_item_fatur := :new.qtde_item_fatur;

               begin
                  select fatu_500.codigo_cidade into v_cidadeempresa from fatu_500
                  where fatu_500.codigo_empresa = :new.ch_it_nf_cd_empr;
               exception
                   when others then
                      v_cidadeempresa := 0;
               end;

               begin
                  select basi_160.codigo_pais into v_paisempresa from basi_160
                  where basi_160.cod_cidade = v_cidadeempresa;
               exception
                   when others then
                      v_paisempresa := 1;
               end;

               if v_paisempresa <> 1
               then
                  if v_especie_docto = 'FAC' -- 1 FAC - NOTA FATURA
                  then
                     if not(v_atu_estoque_terceiro = 1 and v_permite_fatura_atu_esq = 'S')
                     then
                        v_qtde_item_fatur := 0.000;
                     else
                        v_qtde_item_fatur := :new.qtde_item_fatur;
                     end if;
                  else
                     v_qtde_item_fatur := :new.qtde_item_fatur;
                  end if;

                  if v_especie_docto = 'GRE' -- 5 GRE - NOTA REMISSAO
                  then
                     v_valor_unitario := 0.00;
                     v_valor_contabil := 0.00;
                  else
                     v_valor_unitario := :new.valor_unitario;
                  end if;
               end if;

               if v_centro_custo_terceiro <= 0
               then
                  v_centro_custo_terceiro := :new.centro_custo;
               end if;

               inter_pr_insere_estq_300_recur (v_deposito_terceiro,          :new.nivel_estrutura,               :new.grupo_estrutura,
                                        :new.subgru_estrutura,        :new.item_estrutura,                :new.data_emissao,
                                        :new.lote_acomp,              :new.ch_it_nf_num_nfis,             :new.ch_it_nf_ser_nfis,
                                        v_cnpj_9,                     v_cnpj_4,                           v_cnpj_2,
                                        :new.seq_item_nfisc,
                                        v_trans_terceiro         ,     v_ent_sai_terc,                     v_centro_custo_terceiro,
                                        v_qtde_item_fatur,             v_valor_unitario * v_indice_moeda, v_valor_contabil * v_indice_moeda,
                                        :new.usuario_cardex,          'FATU_060',                         :new.nome_programa,
                                        0.00,                          0,                                 0,
                                        :new.ch_it_nf_num_nfis,        0,                                 0);
            end if;                   -- if v_trans_terceiro > 0 and v_deposito_terceiro > 0
         end if;                   -- if v_tipo_transacao_005 = 'E' or v_tipo_transacao_005 = 'D'
      end if;                      -- if inserting


      if updating
      then
         if :new.cod_cancelamento = 0
         then
            select inter_fn_unid_med_tributaria(:new.natopeno_nat_oper,
                                                :new.natopeno_est_oper,
                                                decode(trim(:new.um_faturamento_um),null,:new.unidade_medida,:new.um_faturamento_um),
                                                :new.classific_fiscal,
                                                :new.nivel_estrutura,
                                                :new.grupo_estrutura,
                                                :new.subgru_estrutura,
                                                :new.item_estrutura
                                                )
            into v_unid_med_trib_tmp from dual;
         end if;

         -- le parametro de empresa que atualiza deposito ativo.
         select fatu_502.atualiza_estoque_f, fatu_502.permite_fatura_atu_esq
         into v_par_atualiza_estoque_f,      v_permite_fatura_atu_esq
         from fatu_502
         where fatu_502.codigo_empresa  = :new.ch_it_nf_cd_empr;

         BEGIN
         select e.periodo_estoque into v_periodo_estoque from empr_001 e;
         END;

         if :new.deposito         <> :old.deposito
         or :new.nivel_estrutura  <> :old.nivel_estrutura
         or :new.grupo_estrutura  <> :old.grupo_estrutura
         or :new.subgru_estrutura <> :old.subgru_estrutura
         or :new.item_estrutura   <> :old.item_estrutura
         or :new.lote_acomp       <> :old.lote_acomp
         or :new.qtde_item_fatur  <> :old.qtde_item_fatur
         or :new.transacao        <> :old.transacao
         then
            raise_application_error(-20000,'Nao se pode alterar dados de estoque na nota.');
         end if;

         if :new.cod_cancelamento = 0 and :old.cod_cancelamento > 0
         then
            raise_application_error(-20000,'Nao se pode descancelar uma nota fiscal.');
         end if;

         if :new.cod_cancelamento > 0 and :old.cod_cancelamento = 0 -- cancelou a nota fiscal
         then

            if :new.transacao_canc_nfisc is null or :new.transacao_canc_nfisc = 0
            then
               raise_application_error(-20000,'Para cancelar a nota fiscal, e obrigatorio que a transacao de estoque da natureza de operacao que
                     foi usada na emissao da mesma, tenha uma TRANSACAO DE CANCELAMENTO cadastrada.
                Identifique qual foi a transacao e providencie o cadastro.');
            end if;

            select atualiza_estoque, atual_estoque_f
            into v_atualiza_estoque, v_atual_estoque_f
            from estq_005
            where codigo_transacao = :new.transacao_canc_nfisc;

            -- quando a empresa consiste deposito ativo toda transacao tem que atualizar "deposito ativo" nao
            if v_par_atualiza_estoque_f = 1 and v_atual_estoque_f = 'N' and v_atualiza_estoque = 1
            then
               raise_application_error(-20000,'Quando a empresa atualiza estoque gerencial, a transacao tambem deve atualizar estoque gerencial.');
            end if;

            -- caso a transacao atualise estoque, checa se o deposito foi informado
            if v_atualiza_estoque = 1 and :new.deposito = 0
            then
               raise_application_error(-20000,'Movimento de saida nao atualizou o estoque. A transacao de cancelamento nao devera atualizar tambem');
            end if;

            -- caso a transacao nao atualise estoque, checa se o deposito foi informado
            if v_atualiza_estoque = 2 and :new.deposito <> 0
            then
               raise_application_error(-20000,'Movimento de saida atualizou o estoque. A transacao de cancelamento deve atualizar tambem.');
            end if;

            v_data_canc_nfisc := :new.data_canc_nfisc;

            if :new.data_emissao > v_periodo_estoque
            then
               v_data_canc_nfisc := :new.data_emissao;
            end if;

            -- l? os par?metros de depositos de entrada em poder de terceiros
            v_dep_entrada_terc_1 := inter_fn_get_param_int(:new.ch_it_nf_cd_empr, 'estoque.depTercEntrada1');
            v_dep_entrada_terc_2 := inter_fn_get_param_int(:new.ch_it_nf_cd_empr, 'estoque.depTercEntrada2');
            v_dep_entrada_terc_4 := inter_fn_get_param_int(:new.ch_it_nf_cd_empr, 'estoque.depTercEntrada4');
            v_dep_entrada_terc_7 := inter_fn_get_param_int(:new.ch_it_nf_cd_empr, 'estoque.depTercEntrada7');
            v_dep_entrada_terc_9 := inter_fn_get_param_int(:new.ch_it_nf_cd_empr, 'estoque.depTercEntrada9');

            if :new.nivel_estrutura = '1'
            then v_dep_entrada_terc := v_dep_entrada_terc_1;
            elsif :new.nivel_estrutura = '2'
            then v_dep_entrada_terc := v_dep_entrada_terc_2;
            elsif :new.nivel_estrutura = '4'
            then v_dep_entrada_terc := v_dep_entrada_terc_4;
            elsif :new.nivel_estrutura = '7'
            then v_dep_entrada_terc := v_dep_entrada_terc_7;
            elsif :new.nivel_estrutura = '9'
            then v_dep_entrada_terc := v_dep_entrada_terc_9;
            end if;

            select cgc_9,    cgc_4,    cgc_2,    nr_solicitacao,   origem_nota,  moeda_nota, fatu_050.data_emissao,
                   fatu_050.pedido_venda
            into   v_cnpj_9, v_cnpj_4, v_cnpj_2, v_nr_solicitacao, v_origem_nota,v_moeda,    v_data_emis,
                   v_pedido_nota_capa
            from fatu_050
            where codigo_empresa  = :new.ch_it_nf_cd_empr
            and   num_nota_fiscal = :new.ch_it_nf_num_nfis
            and   serie_nota_fisc = :new.ch_it_nf_ser_nfis;

            if :new.deposito > 0
            then
               -- le a natureza de operacao para verificar se a mesma e de servico (tipo_natureza = 1)
               begin
                  select tipo_natureza into v_tipo_natureza from pedi_080
                  where natur_operacao = :new.natopeno_nat_oper
                  and   estado_natoper = :new.natopeno_est_oper;

                  exception
                     when others then
                        v_tipo_natureza := 0;
               end;

               select tipo_volume into v_tipo_volume from basi_205
               where codigo_deposito = :new.deposito;

               /*Atualiza o valor para a moeda corrente*/
               if v_moeda <> 0
               then
                  begin
                     select valor_moeda into v_indice_moeda
                     from basi_270
                     where basi_270.data_moeda   = v_data_emis
                       and basi_270.codigo_moeda = v_moeda;
                     exception
                        when others then
                          v_indice_moeda := 1;
                  end;
               else
                  v_indice_moeda := 1;
               end if;


               if :new.qtde_item_fatur = 0.00
               then v_qtde := 1.00;
               else v_qtde := :new.qtde_item_fatur;
               end if;

               -- LOGICA PARA PROCURAR O PAIS DA EMPRESA DA NOTA
               v_valor_contabil := :new.valor_contabil / v_qtde;
               v_valor_unitario := :new.valor_unitario;
               v_qtde_item_fatur := :new.qtde_item_fatur;

               begin
                  select fatu_500.codigo_cidade into v_cidadeempresa from fatu_500
                  where fatu_500.codigo_empresa = :new.ch_it_nf_cd_empr;
                  exception
                     when others then
                        v_cidadeempresa := 0;
               end;

               begin
                  select basi_160.codigo_pais into v_paisempresa from basi_160
                  where basi_160.cod_cidade = v_cidadeempresa;
                  exception
                     when others then
                        v_paisempresa := 1;
               end;

               if v_paisempresa <> 1
               then

                  if v_especie_docto = 'FAC' -- 1 FAC - NOTA FATURA
                  then
                     if not(v_atualiza_estoque = 1 and v_permite_fatura_atu_esq = 'S')
                     then
                         v_qtde_item_fatur := 0.000;
                     else
                         v_qtde_item_fatur := :new.qtde_item_fatur;
                     end if;
                  else
                     v_qtde_item_fatur := :new.qtde_item_fatur;
                  end if;

                  if v_especie_docto = 'GRE' -- 5 GRE - NOTA REMISSAO
                  then
                     v_valor_unitario := 0.00;
                     v_valor_contabil := 0.00;
                  else
                     v_valor_unitario := :new.valor_unitario;
                 end if;

               end if;


               -- se o tipo da natureza for de servico e o nivel for '2' ou '4' e o tipo volume for '2' ou '4'
               -- NAO atualiza o estoque, pois o mesmo e atualizado pela baixa do rolo, que e feita abaixo pela
               -- trigger
               if not (v_tipo_natureza = 1 and :new.nivel_estrutura in ('2','4') and v_tipo_volume in ('2','4')
               and    :new.pedido_venda  > 0 and :new.numero_nota_reemissao = 0)
               then
                  v_tabela_origem := 'FATU_060';

                  ---------------------------------------------------------------------------------------
                  -- INICIO DA ROTINA DE CALCULO DO PESO DOS ROLOS
                  ---------------------------------------------------------------------------------------

                  -- inicializa a variavel de leitura dos rolos para a busca dos pesos para gravar no
                  -- movimento de estoque
                  v_peso_rolo_kg := 0.00;

                  -- se o deposito for de rolos, encontra o peso dos volumes
                  if :new.nivel_estrutura in ('2','4') and v_tipo_volume in (2,4)
                  then
                     select sum(nvl(peso_bruto, 0)) into v_peso_rolo_kg
                     from pcpt_020
                     where cod_empresa_nota   = :new.ch_it_nf_cd_empr
                     and   nota_fiscal        = :new.ch_it_nf_num_nfis
                     and   serie_fiscal_sai   = :new.ch_it_nf_ser_nfis
                     and   seq_nota_fiscal    = :new.seq_item_nfisc
                     and   rolo_estoque       = 2      -- faturado
                     and   pedido_venda       = :new.pedido_venda
                     and   seq_item_pedido    = :new.seq_item_pedido
                     and  (pedido_loja = v_nr_solicitacao or v_origem_nota <> 3);
                  end if;             -- if :new.nivel_estrutura in ('2','4') and v_tipo_volume in (2,4)

                  ---------------------------------------------------------------------------------------
                  -- FIM DA ROTINA DE CALCULO DO PESO DOS ROLOS
                  ---------------------------------------------------------------------------------------

                  inter_pr_insere_estq_300_recur (:new.deposito,             :new.nivel_estrutura,   :new.grupo_estrutura,
                                            :new.subgru_estrutura,     :new.item_estrutura,    v_data_canc_nfisc,
                                            :new.lote_acomp,           :new.ch_it_nf_num_nfis, :new.ch_it_nf_ser_nfis,
                                            v_cnpj_9,                  v_cnpj_4,               v_cnpj_2,
                                            :new.seq_item_nfisc,
                                            :new.transacao_canc_nfisc, 'E',                    :new.centro_custo,
                                            v_qtde_item_fatur,         v_valor_unitario * v_indice_moeda,    v_valor_contabil * v_indice_moeda,
                                            :new.usuario_cardex,       'FATU_060',             :new.nome_programa,
                                            v_peso_rolo_kg,            0,                       0,
                                            :new.ch_it_nf_num_nfis,    0,                       0);

               else
                  v_tabela_origem := 'PCPT_020';
               end if;

               if v_tipo_volume <> 0
               then

                  if (:new.nivel_estrutura = '2' and v_tipo_volume = 2)
                  or (:new.nivel_estrutura = '4' and v_tipo_volume = 4)
                  then

                     if :new.numero_nota_reemissao > 0 -- Cancelamento de nota na reemissao
                     then
                        v_sit_rolo              := 8;
                        v_emp_nota_fiscal       := :new.empresa_nota_reemissao;
                        v_num_nota_fiscal       := :new.numero_nota_reemissao;
                        v_ser_nota_fiscal       := :new.serie_nota_reemissao;
                        v_seq_nota_fiscal       := :new.seq_item_nfisc;
                        :new.seq_nota_reemissao := :new.seq_item_nfisc;
                     else
                       v_emp_nota_fiscal        := 0;
                       v_num_nota_fiscal        := 0;
                       v_ser_nota_fiscal        := ' ';
                       v_seq_nota_fiscal        := 0;

                       if:new.pedido_venda > 0          --- faturamento
                       then

                          /* zequiel
                          A L T E R A C :1 O  P A R A  S E R  F E I T A
                            . incluir leitura da tabela pedi_110, para ver se a sequencia
                              do pedido relacionado ao rolo, esta cancelada.

                              se estiver
                              entao:
                                . rolo_estoque    = 1 (v_sit_rolo)
                                . pedido_venda    = 0
                                . seq_item_pedido = 0
                              senao
                                . rolo_estoque    = 3 (v_sit_rolo)
                              fim
                          */

                          begin
                             select cod_cancelamento
                             into v_cod_canc_item_ped from pedi_110
                             where pedido_venda    =:new.pedido_venda
                               and seq_item_pedido = :new.seq_item_pedido;

                             exception
                                when no_data_found then
                                   v_cod_canc_item_ped := 99;
                          end;

                          if v_cod_canc_item_ped > 0
                          then
                             v_sit_rolo             := 1;
--                           v_novo_pedido_venda    := 0;
--                           v_nova_seq_item_pedido := 0;
                          else
                             v_sit_rolo             := 3;
                          end if;

                       else
                          if :new.num_ordem_serv > 0  --- ordem de servico
                          then
                              v_sit_rolo := 7;
                          else
                             if  v_origem_nota    = 3
                             and v_nr_solicitacao > 0 /* Rolo associado a solicit. de loja */
                             then
                                v_sit_rolo := 3;
                                v_emp_nota_fiscal        := :new.ch_it_nf_cd_empr;
                             else
                                v_sit_rolo := 1;
                             end if;
                          end if;
                       end if;
                     end if;

                     update pcpt_020
                     set rolo_estoque         = v_sit_rolo,
                         cod_empresa_nota     = v_emp_nota_fiscal,
                         nota_fiscal          = v_num_nota_fiscal,
                         serie_fiscal_sai     = v_ser_nota_fiscal,
                         seq_nota_fiscal      = v_seq_nota_fiscal,
                         rolo_confirmado      = 1,
                         data_cardex          = v_data_canc_nfisc,
                         transacao_cardex     = :new.transacao_canc_nfisc,
                         usuario_cardex       = :new.usuario_cardex,
                         nome_prog_020        = :new.nome_programa,
                         tabela_origem_cardex = v_tabela_origem,
                         codigo_deposito      = :new.deposito
                     where cod_empresa_nota   = :new.ch_it_nf_cd_empr
                     and   nota_fiscal        = :new.ch_it_nf_num_nfis
                     and   serie_fiscal_sai   = :new.ch_it_nf_ser_nfis
                     and   seq_nota_fiscal    = :new.seq_item_nfisc
                     and   rolo_estoque       = 2      -- faturado
                     and   panoacab_nivel99   = :new.nivel_estrutura
                     and   panoacab_grupo     = :new.grupo_estrutura
                     and   panoacab_subgrupo  = :new.subgru_estrutura
                     and   panoacab_item      = :new.item_estrutura
                     and   seq_item_pedido    = :new.seq_item_pedido
                     and  (pedido_loja = v_nr_solicitacao or v_origem_nota <> 3);

                     update pcpt_020
                        set rolo_estoque         = 0,
                        cod_empresa_nota     = v_emp_nota_fiscal,
                        nota_fiscal          = v_num_nota_fiscal,
                        serie_fiscal_sai     = v_ser_nota_fiscal,
                        seq_nota_fiscal      = v_seq_nota_fiscal,
                        rolo_confirmado      = 1,
                        data_cardex          = v_data_canc_nfisc,
                        transacao_cardex     = :new.transacao_canc_nfisc,
                        usuario_cardex       = :new.usuario_cardex,
                        nome_prog_020        = :new.nome_programa,
                        codigo_deposito      = :new.deposito,
                        tabela_origem_cardex = v_tabela_origem
                    where   cod_empresa_nota   = :new.ch_it_nf_cd_empr
                      and   nota_fiscal        = :new.ch_it_nf_num_nfis
                      and   serie_fiscal_sai   = :new.ch_it_nf_ser_nfis
                      and   seq_nota_fiscal    = :new.seq_item_nfisc
                      and   rolo_estoque       = 2      -- faturado
                    and   seq_item_pedido    = :new.seq_item_pedido
                      and  (pedido_loja = v_nr_solicitacao or v_origem_nota <> 3);
                  end if;             -- if :new.nivel_estrutura in ('2','4') and v_tipo_volume in (2,4)


                  if :new.nivel_estrutura = '7' and v_tipo_volume = 7
                  then
                     if :new.numero_nota_reemissao > 0 -- Cancelamento de nota na reemissao
                     then
                        v_sit_caixa             := 7;
                        v_emp_nota_fiscal       := :new.ch_it_nf_cd_empr;
                        v_num_nota_fiscal       := :new.numero_nota_reemissao;
                        v_ser_nota_fiscal       := :new.serie_nota_reemissao;
                        v_seq_nota_fiscal       := :new.seq_item_nfisc;
                        :new.seq_nota_reemissao := :new.seq_item_nfisc;
                        v_pedido_venda_060      := :new.pedido_venda;
                        v_seq_item_pedido       := :new.seq_item_pedido;
                     else
                        v_sit_caixa              := 1;
                        v_emp_nota_fiscal        := 0;
                        v_num_nota_fiscal        := 0;
                        v_ser_nota_fiscal        := ' ';
                        v_seq_nota_fiscal        := 0;
                        v_pedido_venda_060       := 0;
                        v_seq_item_pedido        := 0;
                        v_nr_solicitacao         := 0;
                     end if;

                     if v_pedido_nota_capa > 0 -- faturamento
                     then
                        update estq_060
                        set status_caixa         = v_sit_caixa,
                            cod_empresa_sai      = v_emp_nota_fiscal,
                            nota_fiscal          = v_num_nota_fiscal,
                            serie_nota           = v_ser_nota_fiscal,
                            seq_nota_fiscal      = v_seq_nota_fiscal,
                            numero_pedido        = v_pedido_venda_060,
                            sequencia_pedido     = v_seq_item_pedido,
                            nr_solicitacao       = v_nr_solicitacao,
                            data_cardex          = v_data_canc_nfisc,
                            transacao_cardex     = :new.transacao_canc_nfisc,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog            = :new.nome_programa,
                            tabela_origem_cardex = 'FATU_060',
                            codigo_deposito      = :new.deposito
                        where status_caixa     = 4      -- faturado
--                      and   numero_pedido    = :new.pedido_venda  (Comentado conforme definido na SS:64889.001 entre rafaels e Osmar.
--                      and   sequencia_pedido = :new.seq_item_pedido
                        and   cod_empresa_sai  = :new.ch_it_nf_cd_empr
                        and   nota_fiscal      = :new.ch_it_nf_num_nfis
                        and   serie_nota       = :new.ch_it_nf_ser_nfis
                        and   seq_nota_fiscal  = :new.seq_item_nfisc;
                     else
                        update estq_060
                        set status_caixa         = v_sit_caixa,
                            cod_empresa_sai      = v_emp_nota_fiscal,
                            nota_fiscal          = v_num_nota_fiscal,
                            serie_nota           = v_ser_nota_fiscal,
                            seq_nota_fiscal      = v_seq_nota_fiscal,
                            numero_pedido        = v_pedido_venda_060,
                            sequencia_pedido     = v_seq_item_pedido,
                            nr_solicitacao       = v_nr_solicitacao,
                            data_cardex          = v_data_canc_nfisc,
                            transacao_cardex     = :new.transacao_canc_nfisc,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog            = :new.nome_programa,
                            tabela_origem_cardex = 'FATU_060',
                            codigo_deposito      = :new.deposito
                        where status_caixa     = 4      -- faturado
                        and   cod_empresa_sai  = :new.ch_it_nf_cd_empr
                        and   nota_fiscal      = :new.ch_it_nf_num_nfis
                        and   serie_nota       = :new.ch_it_nf_ser_nfis
                        and   seq_nota_fiscal  = :new.seq_item_nfisc
                        and   codigo_deposito  = :new.deposito;
                     end if;
                  end if;             -- if :new.nivel_estrutura = '7' and v_tipo_volume = 7

                  if :new.nivel_estrutura = '1' and v_tipo_volume = 1
                  then
                     if :new.numero_nota_reemissao > 0 -- Cancelamento de nota na reemissao aqui
                     then
                        v_sit_caixa             := 8;
                        v_num_nota_fiscal       := :new.numero_nota_reemissao;
                        v_ser_nota_fiscal       := :new.serie_nota_reemissao;
                        v_seq_nota_fiscal       := :new.seq_item_nfisc;
                        :new.seq_nota_reemissao := :new.seq_item_nfisc;
                        v_pedido_venda_060      := :new.pedido_venda;
                        v_seq_item_pedido       := :new.seq_item_pedido;
                     else
                       if :new.pedido_venda = 0
                        then
                           v_sit_caixa              := 1;
                           v_pedido_venda_060       := 0;
                           v_seq_item_pedido        := 0;
                        else
                           v_sit_caixa             := 3;
                           v_pedido_venda_060      := :new.pedido_venda;
                           v_seq_item_pedido       := :new.seq_item_pedido;
                        end if;

                        v_nr_solicitacao         := 0;
                        v_num_nota_fiscal        := 0;
                        v_ser_nota_fiscal        := ' ';
                        v_seq_nota_fiscal        := 0;
                     end if;


                     update pcpc_330
                     set estoque_tag          = v_sit_caixa,
                         pedido_venda         = v_pedido_venda_060,
                         seq_item_pedido      = v_seq_item_pedido,
                         nota_inclusao        = v_num_nota_fiscal,
                         serie_nota           = v_ser_nota_fiscal,
                         emp_saida            = :new.ch_it_nf_cd_empr,
                         seq_saida            = v_seq_nota_fiscal,
                         data_cardex          = v_data_canc_nfisc,
                         transacao_cardex     = :new.transacao_canc_nfisc,
                         usuario_cardex       = :new.usuario_cardex,
                         nome_prog_050        = :new.nome_programa,
                         tabela_origem_cardex = 'FATU_060',
                         deposito             = :new.deposito
                     where pedido_venda       = :new.pedido_venda
                     and   seq_item_pedido    = :new.seq_item_pedido
                     and   estoque_tag        = 4      -- faturado
                     and   nota_inclusao      = :new.ch_it_nf_num_nfis
                     and   serie_nota         = :new.ch_it_nf_ser_nfis
                     and   seq_saida          = :new.seq_item_nfisc;
                  end if;             -- if :new.nivel_estrutura = '1' and v_tipo_volume = 1


                  if :new.nivel_estrutura = '9' and v_tipo_volume = 9
                  then

                     -- fardo so tem na obrigacao fiscal
                     update pcpf_060
                     set status_fardo     = 1,
                         num_nota_sai     = 0,
                         ser_nota_sai     = ' ',
                         seq_nota_sai     = 0,
                         emp_nota_sai     = 0,
                         data_cardex      = v_data_canc_nfisc,
                         transacao_cardex = :new.transacao_canc_nfisc,
                         usuario_cardex   = :new.usuario_cardex,
                         nome_prog        = :new.nome_programa,
                         tabela_origem_cardex = 'FATU_060',
                         deposito             = :new.deposito
                     where emp_nota_sai = :new.ch_it_nf_cd_empr
                     and   num_nota_sai = :new.ch_it_nf_num_nfis
                     and   ser_nota_sai = :new.ch_it_nf_ser_nfis
                     and   seq_nota_sai = :new.seq_item_nfisc
                     and   status_fardo = 2;      -- fardo em estoque
                  end if;             -- if :new.nivel_estrutura = '9' and v_tipo_volume = 9
               end if;                -- if v_tipo_volume <> 0
            end if;                   -- if :new.deposito > 0

            /*INICIO MOVIMENTACAO DE ESTOQUE NO DEPOSITO PROPRIO EM PODER DE TERCEIROS
              SS: 60598/001 - THIAGO.FELIPE*/
            begin
               select estq_005.icdtransdepterceiros,    estq_005.tipo_transacao,
                      estq_005.atualiza_estq_terceiro
               into v_trans_terceiro,                   v_tipo_transacao_005,
                    v_atualiza_estq_terceiro
               from estq_005
               where estq_005.codigo_transacao = :new.transacao;
            exception
                when no_data_found then
                   v_trans_terceiro     := 0;
                   v_tipo_transacao_005 := 'V';
                   v_atualiza_estq_terceiro := 0;
            end;

            /*Verifica deposito relacionado ao terceiro*/
            begin
              select basi_572.cod_dep
              into v_dep_terceiro_572
              from basi_572
              where basi_572.cod_empresa = :new.ch_it_nf_cd_empr
                and basi_572.terc_cnpj9  = v_cnpj_9
                and basi_572.terc_cnpj4  = v_cnpj_4
                and basi_572.terc_cnpj2  = v_cnpj_2;
              exception
              when no_data_found then
                   v_dep_terceiro_572     := 0;
            end;

            if ( v_dep_terceiro_572 = 0 and  (v_tipo_transacao_005 = 'E' or v_tipo_transacao_005 = 'D' or
                                              v_tipo_transacao_005 = 'I' or v_tipo_transacao_005 = 'N' or v_atualiza_estq_terceiro = 1) )
               or
               ( v_dep_terceiro_572 <> 0 and  (v_tipo_transacao_005 = 'R' or v_tipo_transacao_005 = 'E' or
                                                v_tipo_transacao_005 = 'I' or v_tipo_transacao_005 = 'D' or v_atualiza_estq_terceiro = 1) )
            then
               if v_dep_terceiro_572 = 0
               then
                  if :new.deposito = 0 and (v_trans_terceiro > 0 and v_dep_entrada_terc >0)
                  then
                     v_deposito_terceiro := v_dep_entrada_terc;
                  elsif :new.deposito > 0
                  then
                     begin
                        select basi_205.icddepdestterceiros into v_deposito_terceiro from basi_205
                        where basi_205.codigo_deposito = :new.deposito;
                     exception
                         when no_data_found then
                            v_deposito_terceiro := 0;
                     end;
                  else
                     begin
                        select 1 into v_cont_processo from hdoc_001
                        where hdoc_001.tipo = 350
                          and hdoc_001.codigo > 0
                          and hdoc_001.codigo2 > 0;
                     exception
                         when no_data_found then
                            v_cont_processo := 0;
                     end;

                     if v_cont_processo > 0
                     then
                        begin
                           select obrf_510.processo_nota, obrf_510.cod_emp_benef,
                                  obrf_510.cod_emp_origem
                           into   v_processo_nota,        v_cod_emp_benef,
                                  v_cod_emp_origem
                           from obrf_510, (select obrf_510.cod_emp_benef, obrf_510.cod_emp_origem, obrf_510.seq_exec_rotina, max(obrf_510.seq_nota) seq_nota
                                            from obrf_510
                                         where obrf_510.cod_empresa_nota  = :new.ch_it_nf_cd_empr
                                           and obrf_510.numero_nota       = :new.ch_it_nf_num_nfis
                                           and obrf_510.serie_nota        = :new.ch_it_nf_ser_nfis
                                           and obrf_510.cgc9_cli_for_nota = v_cnpj_9
                                           and obrf_510.cgc4_cli_for_nota = v_cnpj_4
                                           and obrf_510.cgc2_cli_for_nota = v_cnpj_2
                                         group by obrf_510.cod_emp_benef, obrf_510.cod_emp_origem, obrf_510.seq_exec_rotina) aux_obrf_510
                           where obrf_510.cod_emp_benef   = aux_obrf_510.cod_emp_benef
                             and obrf_510.cod_emp_origem  = aux_obrf_510.cod_emp_origem
                             and obrf_510.seq_exec_rotina = aux_obrf_510.seq_exec_rotina
                             and obrf_510.seq_nota        = aux_obrf_510.seq_nota;
                          exception
                              when no_data_found then
                                v_processo_nota  := 0;
                                v_cod_emp_benef  := 0;
                                v_cod_emp_origem := 0;
                         end;

                         if v_processo_nota > 0 and v_cod_emp_benef > 0 and v_cod_emp_origem > 0
                         then

                            if v_processo_nota = 6
                            then
                               v_tipo_processo := 356;
                            end if;

                            if v_processo_nota = 12
                            then
                               v_tipo_processo := 362;
                            end if;

                            begin
                               select hdoc_001.campo_numerico31,  hdoc_001.campo_numerico32
                               into   v_deposito_terceiro,        v_centro_custo_terceiro
                               from hdoc_001
                               where hdoc_001.tipo    = v_tipo_processo
                                 and hdoc_001.codigo  = v_cod_emp_benef
                                and hdoc_001.codigo2 = v_cod_emp_origem;
                           exception
                               when no_data_found then
                                  v_deposito_terceiro := 0;
                                  v_centro_custo_terceiro := 0;
                           end;
                        end if;
                     end if; --if v_cont_processo > 0
                  end if;
                  else -- v_dep_terceiro_572 <> 0
                     v_deposito_terceiro := v_dep_terceiro_572;
                  end if;

               if v_trans_terceiro > 0 and v_deposito_terceiro > 0
               then

                  begin
                     select estq_005.trans_cancelamento
           into v_trans_terceiro_canc
           from estq_005
                     where estq_005.codigo_transacao = v_trans_terceiro;
                  exception
                      when no_data_found then
                         v_trans_terceiro_canc := 0;
                  end;

                  begin
                     select estq_005.atualiza_estoque , estq_005.entrada_saida
           into v_atu_estoque_terceiro , v_ent_sai_terc
           from estq_005
                     where estq_005.codigo_transacao = v_trans_terceiro_canc;
                  exception
                      when no_data_found then
                         v_atu_estoque_terceiro := 0;
             v_ent_sai_terc         := '';
                  end;

                  if v_trans_terceiro_canc > 0 and v_atu_estoque_terceiro = 1
                  then
                     if :new.qtde_item_fatur = 0.00
                     then v_qtde := 1.00;
                     else v_qtde := :new.qtde_item_fatur;
                     end if;

                     -- LOGICA PARA PROCURAR O PAIS DA EMPRESA DA NOTA
                     v_valor_contabil := :new.valor_contabil /  v_qtde;
                     v_valor_unitario := :new.valor_unitario;
                     v_qtde_item_fatur := :new.qtde_item_fatur;

                     begin
                        select fatu_500.codigo_cidade into v_cidadeempresa from fatu_500
                        where fatu_500.codigo_empresa = :new.ch_it_nf_cd_empr;
                        exception
                           when others then
                              v_cidadeempresa := 0;
                     end;

                     begin
                        select basi_160.codigo_pais into v_paisempresa from basi_160
                        where basi_160.cod_cidade = v_cidadeempresa;
                        exception
                           when others then
                              v_paisempresa := 1;
                     end;

                     if v_paisempresa <> 1
                     then
                        if v_especie_docto = 'FAC' -- 1 FAC - NOTA FATURA
                        then
                           if not(v_atu_estoque_terceiro = 1 and v_permite_fatura_atu_esq = 'S')
                           then
                               v_qtde_item_fatur := 0.000;
                           else
                               v_qtde_item_fatur := :new.qtde_item_fatur;
                           end if;
                        else
                           v_qtde_item_fatur := :new.qtde_item_fatur;
                        end if;

                        if v_especie_docto = 'GRE' -- 5 GRE - NOTA REMISSAO
                        then
                           v_valor_unitario := 0.00;
                           v_valor_contabil := 0.00;
                        else
                           v_valor_unitario := :new.valor_unitario;
                        end if;
                     end if;

                     if v_centro_custo_terceiro <= 0
                     then
                        v_centro_custo_terceiro := :new.centro_custo;
                     end if;

                     inter_pr_insere_estq_300_recur (v_deposito_terceiro,          :new.nivel_estrutura,               :new.grupo_estrutura,
                                               :new.subgru_estrutura,        :new.item_estrutura,                v_data_canc_nfisc,
                                               :new.lote_acomp,              :new.ch_it_nf_num_nfis,             :new.ch_it_nf_ser_nfis,
                                               v_cnpj_9,                     v_cnpj_4,                           v_cnpj_2,
                                               :new.seq_item_nfisc,
                                               v_trans_terceiro_canc,        v_ent_sai_terc,                                v_centro_custo_terceiro,
                                               v_qtde_item_fatur,             v_valor_unitario * v_indice_moeda, v_valor_contabil * v_indice_moeda,
                                               :new.usuario_cardex,          'FATU_060',                         :new.nome_programa,
                                               0.00,                          0,                                 0,
                                               :new.ch_it_nf_num_nfis,        0,                                 0);

                  end if;                 -- if v_trans_terceiro_canc > 0 and v_atu_estoque_terceiro = 1
               end if;                    -- if v_trans_terceiro > 0 and v_deposito_terceiro > 0
            end if;                   -- if v_tipo_transacao_005 = 'E' or v_tipo_transacao_005 = 'D'
         end if;                      -- if :new.cod_cancelamento > 0 and :old.cod_cancelamento = 0
      end if;                         -- if updating


      if deleting
      then

         v_dep_entrada_terc_1 := inter_fn_get_param_int(:old.ch_it_nf_cd_empr, 'estoque.depTercEntrada1');
         v_dep_entrada_terc_2 := inter_fn_get_param_int(:old.ch_it_nf_cd_empr, 'estoque.depTercEntrada2');
         v_dep_entrada_terc_4 := inter_fn_get_param_int(:old.ch_it_nf_cd_empr, 'estoque.depTercEntrada4');
         v_dep_entrada_terc_7 := inter_fn_get_param_int(:old.ch_it_nf_cd_empr, 'estoque.depTercEntrada7');
         v_dep_entrada_terc_9 := inter_fn_get_param_int(:old.ch_it_nf_cd_empr, 'estoque.depTercEntrada9');

         if :old.nivel_estrutura = '1'
         then v_dep_entrada_terc := v_dep_entrada_terc_1;
         elsif :old.nivel_estrutura = '2'
         then v_dep_entrada_terc := v_dep_entrada_terc_2;
         elsif :old.nivel_estrutura = '4'
         then v_dep_entrada_terc := v_dep_entrada_terc_4;
         elsif :old.nivel_estrutura = '7'
         then v_dep_entrada_terc := v_dep_entrada_terc_7;
         elsif :old.nivel_estrutura = '9'
         then v_dep_entrada_terc := v_dep_entrada_terc_9;
         end if;

         select cgc_9,    cgc_4,    cgc_2,    nr_solicitacao,   situacao_nfisc,   moeda_nota
         into   v_cnpj_9, v_cnpj_4, v_cnpj_2, v_nr_solicitacao, v_situacao_nfisc, v_moeda
         from fatu_050
         where codigo_empresa  = :old.ch_it_nf_cd_empr
         and   num_nota_fiscal = :old.ch_it_nf_num_nfis
         and   serie_nota_fisc = :old.ch_it_nf_ser_nfis;

         if :old.deposito > 0 -- eliminou o item da nota fiscal
         then
            select count(*) into v_nr_registro_300 from estq_300
            where numero_documento    = :old.ch_it_nf_num_nfis
            and   serie_documento     = :old.ch_it_nf_ser_nfis
            and   cnpj_9              = v_cnpj_9
            and   cnpj_4              = v_cnpj_4
            and   cnpj_2              = v_cnpj_2
            and   sequencia_documento = :old.seq_item_nfisc
            and   codigo_deposito     = :old.deposito
            and   tabela_origem       = 'FATU_060';

            -- le a natureza de operacao para verificar se a mesma e de servico (tipo_natureza = 1)
            begin
               select tipo_natureza into v_tipo_natureza from pedi_080
               where natur_operacao = :old.natopeno_nat_oper
               and   estado_natoper = :old.natopeno_est_oper;

               exception
                  when others then
                     v_tipo_natureza := 0;
            end;

            select tipo_volume into v_tipo_volume from basi_205
            where codigo_deposito = :old.deposito;

            -- Quando encontrou registro no cardex e a nota esta aberta, atualizamos o cardex com a exclusao do item
            if v_nr_registro_300 > 0 and v_situacao_nfisc in (0,5)
            then
               update  estq_300 set flag_elimina = 1
               where estq_300.numero_documento    = :old.ch_it_nf_num_nfis
               and   estq_300.serie_documento     = :old.ch_it_nf_ser_nfis
               and   estq_300.cnpj_9              = v_cnpj_9
               and   estq_300.cnpj_4              = v_cnpj_4
               and   estq_300.cnpj_2              = v_cnpj_2
               and   estq_300.sequencia_documento = :old.seq_item_nfisc
               and   estq_300.codigo_deposito     = :old.deposito
               and   estq_300.tabela_origem       = 'FATU_060';

               delete estq_300
               where numero_documento    = :old.ch_it_nf_num_nfis
               and   serie_documento     = :old.ch_it_nf_ser_nfis
               and   cnpj_9              = v_cnpj_9
               and   cnpj_4              = v_cnpj_4
               and   cnpj_2              = v_cnpj_2
               and   sequencia_documento = :old.seq_item_nfisc
               and   codigo_deposito     = :old.deposito
               and   tabela_origem       = 'FATU_060';
           end if;

            if v_tipo_volume <> 0
            then
               if :old.nivel_estrutura in ('2','4') and v_tipo_volume in (2,4)
               then

                  if :old.pedido_venda > 0          --- faturamento
                  then
                     v_sit_rolo := 3;
                  else
                     if :old.num_ordem_serv > 0  --- ordem de servico
                     then
                         v_sit_rolo := 7;
                     else
                         v_sit_rolo := 1;
                     end if;
                  end if;

                  if v_tipo_natureza = 1 and :old.pedido_venda > 0
                  then
                     v_tabela_origem := 'PCPT_020';
                  else
                     v_tabela_origem := 'FATU_060';
                  end if;

                  update pcpt_020
                  set rolo_estoque         = v_sit_rolo,
                      nota_fiscal          = 0,
                      serie_fiscal_sai     = ' ',
                      cod_empresa_nota     = 0,
                      seq_nota_fiscal      = 0,
                      data_cardex          = sysdate,
                      usuario_cardex       = 'DELETE',
                      tabela_origem_cardex = v_tabela_origem
                  where pedido_venda     = :old.pedido_venda
                  and   nr_solic_volume  = v_nr_solicitacao
                  and   cod_empresa_nota = :old.ch_it_nf_cd_empr
                  and   nota_fiscal      = :old.ch_it_nf_num_nfis
                  and   serie_fiscal_sai = :old.ch_it_nf_ser_nfis
                  and   seq_nota_fiscal  = :old.seq_item_nfisc
                  and   rolo_estoque     = 2;      -- faturado

               end if;             -- if (:new.nivel_estrutura = '2' or :new.nivel_estrutura = '4') and v_tipo_volume in (2,4)

               if :old.nivel_estrutura = '7' and v_tipo_volume = 7
               then
                  if :old.pedido_venda > 0          --- faturamento
                  then
                     v_sit_caixa := 3;
                  else
                     v_sit_caixa := 1;
                  end if;

                  update estq_060
                  set status_caixa         = v_sit_caixa,
                      cod_empresa_sai      = 0,
                      nota_fiscal          = 0,
                      serie_nota           = ' ',
                      seq_nota_fiscal      = 0,
                      data_cardex          = sysdate,
                      usuario_cardex       = 'DELETE',
                      tabela_origem_cardex = 'FATU_060'
                  where numero_pedido    = :old.pedido_venda
                  and   sequencia_pedido = :old.seq_item_pedido
                  and   status_caixa     = 4      -- faturado
                  and   cod_empresa_sai  = :old.ch_it_nf_cd_empr
                  and   nota_fiscal      = :old.ch_it_nf_num_nfis
                  and   serie_nota       = :old.ch_it_nf_ser_nfis
                  and   seq_nota_fiscal  = :old.seq_item_nfisc;
               end if;             -- if :old.nivel_estrutura = '7' and v_tipo_volume = 7


               if :old.nivel_estrutura = '1' and v_tipo_volume = 1
               then
                  if :old.pedido_venda > 0          --- faturamento
                  then
                     v_sit_caixa := 3;
                  else
                     v_sit_caixa := 1;
                  end if;

                  update pcpc_330
                    set estoque_tag          =  v_sit_caixa,
                        nota_inclusao        = 0,
                        serie_nota           = ' ',
                        seq_saida            = 0,
                        data_cardex          = sysdate,
                        usuario_cardex       = 'DELETE',
                        tabela_origem_cardex = 'FATU_060'
                    where pedido_venda     = :old.pedido_venda
                    and   seq_item_pedido  = :old.seq_item_pedido
                    and   estoque_tag      = 4      -- faturado
                    and   nota_inclusao    = :old.ch_it_nf_num_nfis
                    and   serie_nota       = :old.ch_it_nf_ser_nfis
                    and   seq_saida        = :old.seq_item_nfisc;
               end if;             -- if :old.nivel_estrutura = '1' and v_tipo_volume = 1

               if :old.nivel_estrutura = '9' and v_tipo_volume = 9
               then

                  -- fardo so tem na obrigacao fiscal
                  update pcpf_060
                  set status_fardo         = 1,
                      num_nota_sai         = 0,
                      ser_nota_sai         = ' ',
                      seq_nota_sai         = 0,
                      emp_nota_sai         = 0,
                      data_cardex          =  sysdate,
                      usuario_cardex       = 'DELETE',
                      tabela_origem_cardex = 'FATU_060'
                  where emp_nota_sai = :old.ch_it_nf_cd_empr
                  and   num_nota_sai = :old.ch_it_nf_num_nfis
                  and   ser_nota_sai = :old.ch_it_nf_ser_nfis
                  and   seq_nota_sai = :old.seq_item_nfisc
                  and   status_fardo = 2;      -- fardo em estoque
               end if;              -- if :old.nivel_estrutura = '9' and v_tipo_volume = 9
            end if;                 -- if v_tipo_volume <> 0
         end if;                    -- if :old.deposito > 0


         begin
            select cgc_9,    cgc_4,    cgc_2
            into   v_cnpj_9, v_cnpj_4, v_cnpj_2
            from fatu_050
            where codigo_empresa  = :old.ch_it_nf_cd_empr
              and num_nota_fiscal = :old.ch_it_nf_num_nfis
              and serie_nota_fisc = :old.ch_it_nf_ser_nfis;
         exception
         when no_data_found then
             v_cnpj_9 := 0;
             v_cnpj_4 := 0;
             v_cnpj_2 := 0;
         end;

         /*REMOVE TRANSACAO DE ENTRADA NO DEPOSITO DE PODER DE TERCEIROS*/
         begin
            select estq_005.icdtransdepterceiros,     estq_005.tipo_transacao,
                   estq_005.atualiza_estq_terceiro
            into   v_trans_terceiro,                  v_tipo_transacao_005,
                   v_atualiza_estq_terceiro
            from estq_005
            where codigo_transacao = :old.transacao;
         exception
             when no_data_found then
                v_trans_terceiro     := 0;
                v_tipo_transacao_005 := 'V';
                v_atualiza_estq_terceiro := 0;
         end;

         /*Verifica deposito relacionado ao terceiro*/
         begin
            select basi_572.cod_dep
            into v_dep_terceiro_572
            from basi_572
            where basi_572.cod_empresa = :old.ch_it_nf_cd_empr
              and basi_572.terc_cnpj9  = v_cnpj_9
              and basi_572.terc_cnpj4  = v_cnpj_4
              and basi_572.terc_cnpj2  = v_cnpj_2;
            exception
            when no_data_found then
                 v_dep_terceiro_572     := 0;
         end;

         if ( v_dep_terceiro_572 = 0 and  (v_tipo_transacao_005 = 'E' or v_tipo_transacao_005 = 'D' or
                                           v_tipo_transacao_005 = 'I' or v_tipo_transacao_005 = 'N' or v_atualiza_estq_terceiro = 1) )
            or
            ( v_dep_terceiro_572 <> 0 and  (v_tipo_transacao_005 = 'R' or v_tipo_transacao_005 = 'E' or
                                            v_tipo_transacao_005 = 'I' or v_tipo_transacao_005 = 'D' or v_atualiza_estq_terceiro = 1) )
         then
            if v_dep_terceiro_572 = 0
            then
               if :old.deposito = 0 and (v_trans_terceiro > 0 and v_dep_entrada_terc >0)
               then
                  v_deposito_terceiro := v_dep_entrada_terc;
               elsif :old.deposito > 0
               then
                  begin
                     select basi_205.icddepdestterceiros into v_deposito_terceiro from basi_205
                     where basi_205.codigo_deposito = :old.deposito;
                  exception
                      when no_data_found then
                         v_deposito_terceiro := 0;
                  end;
               else
               begin
                  select cgc_9,    cgc_4,    cgc_2
                  into   v_cnpj_9, v_cnpj_4, v_cnpj_2
                  from fatu_050
                  where codigo_empresa    = :old.ch_it_nf_cd_empr
                    and   num_nota_fiscal = :old.ch_it_nf_num_nfis
                    and   serie_nota_fisc = :old.ch_it_nf_ser_nfis;
               exception
                   when no_data_found then
                      v_cnpj_9 := 0;
                      v_cnpj_4 := 0;
                      v_cnpj_2 := 0;
               end;

                  begin
                     select 1 into v_cont_processo from hdoc_001
                     where hdoc_001.tipo = 350
                       and hdoc_001.codigo > 0
                       and hdoc_001.codigo2 > 0;
                  exception
                      when no_data_found then
                         v_cont_processo := 0;
                  end;

                  if v_cont_processo > 0
                  then
                     begin
                        select obrf_510.processo_nota, obrf_510.cod_emp_benef,
                               obrf_510.cod_emp_origem
                        into   v_processo_nota,        v_cod_emp_benef,
                               v_cod_emp_origem
                        from obrf_510, (select obrf_510.cod_emp_benef, obrf_510.cod_emp_origem, obrf_510.seq_exec_rotina, max(obrf_510.seq_nota) seq_nota
                                        from obrf_510
                                        where obrf_510.cod_empresa_nota  = :new.ch_it_nf_cd_empr
                                          and obrf_510.numero_nota       = :new.ch_it_nf_num_nfis
                                          and obrf_510.serie_nota        = :new.ch_it_nf_ser_nfis
                                          and obrf_510.cgc9_cli_for_nota = v_cnpj_9
                                          and obrf_510.cgc4_cli_for_nota = v_cnpj_4
                                          and obrf_510.cgc2_cli_for_nota = v_cnpj_2
                                        group by obrf_510.cod_emp_benef, obrf_510.cod_emp_origem, obrf_510.seq_exec_rotina) aux_obrf_510
                         where obrf_510.cod_emp_benef   = aux_obrf_510.cod_emp_benef
                           and obrf_510.cod_emp_origem  = aux_obrf_510.cod_emp_origem
                           and obrf_510.seq_exec_rotina = aux_obrf_510.seq_exec_rotina
                           and obrf_510.seq_nota        = aux_obrf_510.seq_nota;
                     exception
                         when no_data_found then
                            v_processo_nota  := 0;
                            v_cod_emp_benef  := 0;
                            v_cod_emp_origem := 0;
                     end;

                     if v_processo_nota > 0 and v_cod_emp_benef > 0 and v_cod_emp_origem > 0
                     then

                        if v_processo_nota = 6
                        then
                           v_tipo_processo := 356;
                        end if;

                        if v_processo_nota = 12
                        then
                           v_tipo_processo := 362;
                        end if;

                        if v_processo_nota = 16
                        then
                           v_tipo_processo := 366;
                        end if;

                        begin
                           select hdoc_001.campo_numerico31
                           into   v_deposito_terceiro
                           from hdoc_001
                           where hdoc_001.tipo    = v_tipo_processo
                             and hdoc_001.codigo  = v_cod_emp_benef
                             and hdoc_001.codigo2 = v_cod_emp_origem;
                        exception
                            when no_data_found then
                               v_deposito_terceiro := 0;
                        end;
                     end if;
                  end if; --if v_cont_processo > 0
               end if;
            else
               v_deposito_terceiro := v_dep_terceiro_572;
            end if;

            if v_deposito_terceiro > 0 and v_trans_terceiro > 0
            then
               begin
                  select fatu_050.cgc_9, fatu_050.cgc_4,     fatu_050.cgc_2,    fatu_050.situacao_nfisc
                  into   v_cnpj_9,       v_cnpj_4,           v_cnpj_2,          v_situacao_nfisc
                  from fatu_050
                  where fatu_050.codigo_empresa  = :old.ch_it_nf_cd_empr
                    and   fatu_050.num_nota_fiscal = :old.ch_it_nf_num_nfis
                    and   fatu_050.serie_nota_fisc = :old.ch_it_nf_ser_nfis;
               exception
                   when no_data_found then
                      v_cnpj_2 := 0;
                      v_cnpj_4 := 0;
                      v_cnpj_9 := 0;
                      v_situacao_nfisc := 0;
               end;

               v_nr_registro_300 := 0;
               begin
                  select count(*) into v_nr_registro_300 from estq_300
                  where estq_300.numero_documento    = :old.ch_it_nf_num_nfis
                    and   estq_300.serie_documento     = :old.ch_it_nf_ser_nfis
                    and   estq_300.cnpj_9              = v_cnpj_9
                    and   estq_300.cnpj_4              = v_cnpj_4
                    and   estq_300.cnpj_2              = v_cnpj_2
                    and   estq_300.sequencia_documento = :old.seq_item_nfisc
                    and   estq_300.codigo_deposito     = v_deposito_terceiro
                    and   estq_300.tabela_origem       = 'FATU_060';
               exception
                   when no_data_found then
                      v_nr_registro_300 := 0;
               end;

               if v_nr_registro_300 > 0 and v_situacao_nfisc in (0,5)
               then

             -- para atualizar o estq_040 a flag_elimina tem que ser 1 para teste

                  update  estq_300 set flag_elimina = 1
                  where estq_300.numero_documento    = :old.ch_it_nf_num_nfis
                    and   estq_300.serie_documento     = :old.ch_it_nf_ser_nfis
                    and   estq_300.cnpj_9              = v_cnpj_9
                    and   estq_300.cnpj_4              = v_cnpj_4
                    and   estq_300.cnpj_2              = v_cnpj_2
                    and   estq_300.sequencia_documento = :old.seq_item_nfisc
                    and   estq_300.codigo_deposito     = v_deposito_terceiro
                    and   estq_300.tabela_origem       = 'FATU_060';

                  delete estq_300
                  where estq_300.numero_documento    = :old.ch_it_nf_num_nfis
                    and   estq_300.serie_documento     = :old.ch_it_nf_ser_nfis
                    and   estq_300.cnpj_9              = v_cnpj_9
                    and   estq_300.cnpj_4              = v_cnpj_4
                    and   estq_300.cnpj_2              = v_cnpj_2
                    and   estq_300.sequencia_documento = :old.seq_item_nfisc
                    and   estq_300.codigo_deposito     = v_deposito_terceiro
                    and   estq_300.tabela_origem       = 'FATU_060';
               end if;                 -- if v_nr_registro_300 > 0 and v_situacao_nfisc in (0,5)
            end if;                    -- if v_deposito_terceiro > 0 and v_trans_terceiro > 0
         end if;                    -- if v_tipo_transacao_005 = 'E' or v_tipo_transacao_005 = 'D'
      end if;                       -- if deleting

      -- Zerando estas informacoes pois somente sao utilizadas
      -- temporariamente enquanto nao termina o processo de
      -- Reemissao de nota fiscal com geracao de nova nota

      if inserting or updating
      then
         :new.empresa_nota_reemissao := 0;
         :new.numero_nota_reemissao  := 0;
         :new.serie_nota_reemissao   := ' ';
         :new.seq_nota_reemissao     := 0;
      end if;

   end if;

end inter_tr_fatu_060;


/
