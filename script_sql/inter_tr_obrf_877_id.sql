
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_877_ID" 
before insert on OBRF_877
  for each row

declare
   v_nr_registro number;

begin
   select seq_OBRF_877.nextval into v_nr_registro from dual;

   :new.id := v_nr_registro;

end inter_tr_OBRF_877_id;

--create sequence SEQ_OBRF_877 minvalue 1 maxvalue 999999 start with 1 increment by 1 cache 20 cycle;

-- ALTER TRIGGER "INTER_TR_OBRF_877_ID" ENABLE
 

/

exec inter_pr_recompile;

