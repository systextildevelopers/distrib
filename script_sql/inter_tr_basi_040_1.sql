
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_040_1" 
before insert or
      delete or
      update of consumo,  nivel_item, grupo_item,
                sub_item, item_item
on basi_040
for each row

begin
   if inserting or updating
   then

      -- CHAMA PROCEDURE QUE INSERE NA TABELA TEMPORARIA DO RECALCULO
      inter_pr_marca_referencia_op(:new.nivel_item, :new.grupo_item,
                                   :new.sub_item,   :new.item_item,
                                   0,               0);

   end if;

   if deleting
   then
      -- CHAMA PROCEDURE QUE INSERE NA TABELA TEMPORARIA DO RECALCULO
      inter_pr_marca_referencia_op(:old.nivel_item, :old.grupo_item,
                                   :old.sub_item,   :old.item_item,
                                   0,               0);
   end if;
end inter_tr_basi_040_1;

-- ALTER TRIGGER "INTER_TR_BASI_040_1" ENABLE
 

/

exec inter_pr_recompile;

