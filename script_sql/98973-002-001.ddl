create table cont_605 (
cod_empresa             number(3) default 0 not null,
filial_lancto           number(3) default 0,
exercicio               number(4) default 0 not null,
numero_lanc             number(9) default 0 not null,
origem                  number(2) default 0,
conta_reduzida          number(5) default 0,
conta_contabil          varchar2(20) default 0,
centro_custo            number(6) default 0,
debito_credito          varchar2(1) default 0,
cnpj9_participante      number(9) default 0,
cnpj4_participante      number(4) default 0,
cnpj2_participante      number(2) default 0,
data_lancto        date,
valor_lancto       number(13,2) default 0.00,
cod_patr_item      varchar2(10) default ' ',
qtd                number(15,2) default 0.0,
ident_item         varchar2(30) default ' ',
descr_item         varchar2(50) default ' ',
data_rect_ini      date,
sld_item_ini       number(15,2) default 0.0,
ind_sld_item_ini   varchar2(1)  default ' ',
real_item          number(15,2) default 0.0,
ind_sld_real_item      varchar2(1)  default ' ',
sld_item_fin       number(15,2) default 0.0,
ind_sld_item_fin   varchar2(1)  default ' ',
sld_scnt_ini       number(15,2) default 0.0,
ind_sld_scnt_ini   varchar2(1)  default ' ',
deb_scnt_valor     number(15,2) default 0.0,
cred_scnt_valor    number(15,2) default 0.0,
sld_scnt_fin       number(15,2) default 0.0,
ind_sld_scnt_fin   varchar2(1)  default ' ',
ind_vlr_lcto       varchar2(1)  default ' ',
ind_adoc_ini       varchar2(1)  default ' ');


comment on column cont_605.cod_empresa is 'C�digo da empresa matriz do lan�amento';
comment on column cont_605.valor_lancto is 'valor lan�amento contabil';
comment on column cont_605.filial_lancto is 'C�digo da empresa do lan�amento';
comment on column cont_605.exercicio is 'C�digo do execicio cont�bil';
comment on column cont_605.numero_lanc is 'N�mero do lan�amento cont�bil';
comment on column cont_605.conta_reduzida is 'N�mero do lan�amento cont�bil';
comment on column cont_605.conta_contabil is 'N�mero do lan�amento cont�bil';
comment on column cont_605.origem is 'Origem do lan�amento cont�bil';
comment on column cont_605.centro_custo is 'Centro do custo do lan�amento cont�bil';
comment on column cont_605.debito_credito is 'Debito ou cr�dito do lan�amento cont�bil';
comment on column cont_605.cnpj9_participante is 'Cnpj do do lan�amento cont�bil';
comment on column cont_605.cnpj4_participante is 'Cnpj do do lan�amento cont�bil';
comment on column cont_605.cnpj2_participante is 'Cnpj do do lan�amento cont�bil';
comment on column cont_605.cod_patr_item is 'c�digo definido pela pessoa jur�dica para identificar o item (ativo/passivo). observa��o: no caso de avj reflexo, ser� a identifica��o do item na pessoa jur�dica investida';
comment on column cont_605.qtd is 'quantidade inicial do item, na mesma precis�o utilizada pela metodologia cont�bil. quando se tratarem de ativos/passivos de mesmas caracter�sticas/qualidades, como ativos biol�gicos e contratos de opera��es em bolsa de valores, � poss�vel agruparem todos pelas caracter�sticas comuns';
comment on column cont_605.ident_item is 'conjunto de caracteres utilizado para individualizar o bem, conforme sua natureza. exemplos: placa para ve�culos automotores, matr�cula do cart�rio para im�veis, marca/modelo para equipamentos, ra�a/idade/sexo para animais vivos, s�rie do derivativo em bolsa';
comment on column cont_605.descr_item is 'descri��o resumida do item. observa��o: no caso de avj reflexo, ser� a descri��o do item pertencente ao patrim�nio da investida.';
comment on column cont_605.data_rect_ini is 'data do reconhecimento cont�bil do item (ddmmaaaa). � a data em que ocorreu o registro inicial no sistema cont�bil, mesmo que anterior ao exerc�cio corrente. em situa��es justific�veis pela complexidade na identifica��o da informa��o, poder� ser informado um registro aproximado constando 31/12 do ano. exemplo: 31122004 (31/12/2004)';
comment on column cont_605.sld_item_ini is 'saldo inicial da conta cont�bil que registra o item (ativo/passivo) ao qual a subconta esteja vinculada. o saldo a ser informado corresponde ao saldo antes de ocorrer o evento origin�rio do lan�amento na subconta demonstrado no livro raz�o auxiliar. observa��o: no caso de avj reflexo, ser� o saldo inicial da subconta avj na investida';
comment on column cont_605.ind_sld_item_ini is 'indicador do saldo inicial da conta cont�bil: d - devedor c - credor'; 
comment on column cont_605.real_item is 'parcela da realiza��o do item registrado no patrim�nio da investida/emitente ao qual a subconta esteja vinculada.  exemplo: deprecia��o, aliena��o, integralizar capital de outra pessoa jur�dica';
comment on column cont_605.ind_sld_real_item is 'indicador da realiza��o do item';
comment on column cont_605.sld_item_fin is 'saldo final da conta cont�bil que registra a conta (ativo/passivo) ao qual a subconta esteja vinculada. corresponde ao saldo ap�s ocorrer o evento origin�rio do lan�amento na subconta demonstrado no livro raz�o auxiliar, como por exemplo a baixa do bem, quando seu valor ser� zerado. observa��o: no caso de avj reflexo, ser� o saldo inicial da subconta avj na investida';
comment on column cont_605.ind_sld_item_fin is 'indicador do saldo final da conta cont�bil: d - devedor c - credor';
comment on column cont_605.sld_scnt_ini is 'saldo inicial representativo do item na subconta antes do lan�amento a ser demonstrado neste registro. este valor pode n�o coincidir com o saldo apresentado na ecd para este instante, uma vez que demonstra apenas a parcela atribu�vel a este item';
comment on column cont_605.ind_sld_scnt_ini is 'indicador do saldo inicial da subconta: d - devedor c - credor';
comment on column cont_605.deb_scnt_valor is 'valor registrado a d�bito na subconta correspondente apenas a participa��o do item na composi��o de um lan�amento resumido na ecd, que pode englobar v�rios eventos sobre itens distintos.';
comment on column cont_605.cred_scnt_valor is 'valor registrado a cr�dito na subconta correspondente apenas a participa��o do item na composi��o de um lan�amento resumido na ecd, que pode englobar v�rios eventos sobre itens distintos.';
comment on column cont_605.sld_scnt_fin is 'saldo final representativo deste item na subconta ap�s o lan�amento demonstrado neste registro. este valor pode n�o coincidir com o saldo apresentado na ecd para este instante, uma vez que demonstra apenas a parcela atribu�vel a este item';
comment on column cont_605.ind_sld_scnt_fin is 'indicador do saldo final da subconta: d - devedor c - credor';
comment on column cont_605.ind_vlr_lcto is 'indicador do lan�amento: d - d�bito c - cr�dito';
comment on column cont_605.ind_adoc_ini is 'indicador de registro de relativo a ado��o inicial. 1 - sim 2 - n�o';


alter table sped_ctb_i050
add (cont_agrup_subconta number(5) default 0,
     natur_subconta      varchar2(2) default ' ');

comment on column sped_ctb_i050.cont_agrup_subconta is 'Conta agrupadora da subconta';
comment on column sped_ctb_i050.natur_subconta is 'Natureza da Conta agrupadora da subconta';

alter table cont_535 add (
  ind_nat_subconta_sped number(2) default 0,
  grupo_subconta_sped   number(5) default 0);

comment on column cont_535.grupo_subconta_sped is 'Conta agrupadora da subconta';
comment on column cont_535.ind_nat_subconta_sped is 'Naturea da Conta agrupadora da subconta';

exec inter_pr_recompile;
/
