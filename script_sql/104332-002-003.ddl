create table obrf_821
(
  COD_EMPRESA     NUMBER(3) default 0 not null,
  MES             NUMBER(2) default 0 not null,
  ANO             NUMBER(4) default 0 not null,
  ID              NUMBER(9) default 0 not null,
  COD_MENSAGEM    NUMBER(6) default 0 not null,
  DESCR_MENSAGEM  VARCHAR2(2000) default ' ',
  COD_AJUSTE      VARCHAR2(10) default ' ',
  DESCR_AJUSTE    VARCHAR2(400) default ' ',
  VALOR           NUMBER(12,2) default 0.00
 );
  
 alter table OBRF_821 add constraint PK_OBRF_821 primary key (COD_EMPRESA, MES, ANO, ID, COD_MENSAGEM);
 
create synonym systextilrpt.OBRF_821 for OBRF_821; 
