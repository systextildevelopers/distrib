create or replace PROCEDURE "INTER_PR_MSG_ERRO_TITULO" (
        p_nr_duplicata IN number,          p_parcela IN varchar2,
        p_cgc9 IN number,                  p_cgc4 IN number,
        p_cgc2 IN number,                  p_tipo_titulo IN number,
        p_codigo_empresa IN number,        p_cod_end_cobranca IN number,
        p_emitente_titulo IN varchar2,     p_documento IN number,
        p_serie IN varchar2,               p_data_contrato IN varchar2,
        p_data_vencimento IN varchar2,     p_data_transacao IN varchar2,
        p_codigo_depto IN number,          p_cod_portador IN number,
        p_codigo_historico IN number,      p_codigo_transacao IN number,
        p_posicao_titulo IN number,        p_codigo_contabil IN number,
        p_tipo_pagamento IN number,        p_moeda_titulo IN number,
        p_origem_debito IN varchar2,       p_previsao IN number,
        p_valor_parcela IN number,         p_valor_parcela_moeda IN number, 
        p_base_irrf_moeda IN varchar2,     p_base_irrf IN varchar2,--INICIO IMPOSTO
        p_cod_ret_irrf IN varchar2,        p_aliq_irrf IN varchar2,
        p_valor_irrf_moeda IN number,      p_valor_irrf IN number,
        p_base_iss IN number,              p_cod_ret_iss IN number,
        p_aliq_iss IN number,              p_valor_iss IN number,
        p_base_inss IN number,             p_cod_ret_inss IN number,
        p_aliq_inss IN number,             p_valor_inss_imp IN number,
        p_base_pis IN number,              p_cod_ret_pis IN number,
        p_aliq_pis IN number,              p_valor_pis_imp IN number,
        p_base_cofins IN number,           p_cod_ret_cofins IN number,
        p_aliq_cofins IN number,           p_valor_cofins_imp IN number,
        p_base_csl IN number,              p_cod_ret_csl IN number,
        p_aliq_csl IN number,              p_valor_csl_imp IN number,
        p_base_csrf IN number,             p_cod_ret_csrf IN number,
        p_aliq_csrf IN number,             p_valor_csrf_imp IN number,--FIM IMPOSTO
        p_codigo_barras IN varchar2,       p_projeto IN number,
        p_subprojeto IN number,            p_servico IN number,
        p_nr_processo_export IN number,    p_nr_processo_import IN number,
        p_cod_cancelamento IN number,      p_data_canc_tit IN varchar2,
        p_has_error OUT boolean,           p_mensagem_retorno OUT varchar2)
is
    v_has_error boolean;
    v_mensagem_erro varchar2(4000);
    v_tmp_codigo_transacao  number;
begin
    /*
        Objetivo: Validações para inserir o Adiantamento.
        
        Padrão de defaults para não validar:
        number 0
        varchar2 null

        varchar2 pl/sql já trata vazio como null, se tiver precisar tratar com trim,
        adicione o tratamento na procedure.

        retorna a mensagem de erro.
    */

    v_has_error := false;
    
    --Obrigatório
    IF (p_nr_duplicata = 0) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Número do título é obrigatório.';
    END IF;

    --Obrigatório
    IF p_parcela is null THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Parcela do título é obrigatório.';
    END IF;

    --Obrigatório
    IF ( (p_cgc9 is null OR p_cgc4 is null OR p_cgc2 is null) ) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'CNPJ do fornecedor é obrigatório.';
    END IF;
    
    IF ST_PCK_SUPR.exists_fornecedor (p_cgc9,p_cgc4,p_cgc2) = false THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'CNPJ do Fornecedor não cadatrado.';
    END IF;

    --Obrigatório
    IF (p_tipo_titulo = 0) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código do tipo do titulo é obrigatório.';
    END IF;

    --Obrigatório
    IF (p_codigo_empresa = 0) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código da empresaque percente o débito é obrigatório.';
    END IF;

    --Obrigatório
    IF (p_cod_end_cobranca = 0) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código de endereço de cobrança é obrigatório.';
    END IF;

    --Obrigatório
    IF p_emitente_titulo is null THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Nome do fornecedor(emitente do título) é obrigatório.';
    END IF;

    --Obrigatório
    IF (p_documento = 0) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Número do documento é obrigatório.';
    END IF;

    --IF (p_serie is not null and exists_serie) THEN
    --    v_has_error := true;
    --    v_mensagem_erro :=  v_mensagem_erro || 'Número do documento é obrigatório.';
    --END IF;

    --Obrigatório
    IF p_data_contrato is null THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Data de emissão é obrigatório.';
    END IF;

    --Obrigatório
    IF p_data_vencimento is null THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Data de vencimento do título é obrigatório.';
    ELSIF to_date(p_data_vencimento) < to_date(p_data_contrato) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Data de vencimento (' || p_data_vencimento || ') deve ser maior ou igual a data de emissão )' || p_data_contrato || ') do título.';
    END IF;

    --Obrigatório
    IF p_data_transacao is null THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Data da entrada contábil(transação) é obrigatório.';
    END IF;

    --Obrigatório
    IF (p_codigo_depto = 0) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código do centro de custo é obrigatório.';
    END IF;

    --Portador

    --Obrigatório
    IF (p_codigo_historico = 0) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código do histórico é obrigatório.';
    END IF;

    --Obrigatório
    IF (p_codigo_transacao = 0) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código da transação é obrigatório.';
    END IF;

    --Posição

    --Obrigatório
    IF p_codigo_contabil is null THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código contábil é obrigatório.';
    END IF;

    --IF (p_tipo_pagamento > 0 and exists_tipo_pagamento) THEN
        --v_has_error := true;
        --v_mensagem_erro :=  v_mensagem_erro || 'Tipo de pagamento é obrigatório.';
    --END IF;

    --moeda

    --observacao

    --Opcional
    IF (p_previsao > 0 and p_previsao not in(1,2) ) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Tipo de pagamento é obrigatório.';
    END IF;

    --Obrigatório
    IF (p_valor_parcela = 0) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor da parcela na moeda local é obrigatório.';
    END IF;

    --Obrigatório
    IF (p_valor_parcela_moeda = 0)  THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor da parcela na moeda indicada é obrigatório.';
    END IF;

    ------------------------
    -- Validações do IRRF --
    ------------------------
    IF p_base_irrf_moeda is null THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor da base de cálculo do IRRF na moeda indicada é obrigatório.';
    END IF;

    IF p_base_irrf is null THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor da base de cálculo do IRFF é obrigatório.';
    END IF;

    IF p_cod_ret_irrf is null THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código da retenção do IRRF é obrigatório.';
    END IF;

    IF p_aliq_irrf is null THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Alíquota do IRRF é obrigatório.';
    END IF;

    IF p_valor_irrf_moeda is null THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor do IRRF na moeda indicada é obrigatório.';
    END IF;

    IF (p_valor_irrf is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor do IRRF é obrigatório.';
    END IF;

    IF (p_base_iss is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor da base de cálculo do ISS é obrigatório.';
    END IF;

    IF (p_cod_ret_iss is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código da retenção do ISS é obrigatório.';
    END IF;

    IF (p_aliq_iss is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Alíquota do ISS é obrigatório.';
    END IF;

    IF (p_valor_iss is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor do ISS é obrigatório.';
    END IF;

    ------------------------
    -- Validações do INSS --
    ------------------------
    IF (p_base_inss is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor da base de cálculo do INSS é obrigatório.';
    END IF;

    IF (p_cod_ret_inss is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código da retenção do INSS é obrigatório.';
    END IF;

    IF (p_aliq_inss is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Alíquota do INSS é obrigatório.';
    END IF;

    IF (p_valor_inss_imp is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor do INSS é obrigatório.';
    END IF;

    ------------------------
    -- Validações do PIS --
    ------------------------
    IF (p_base_pis is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor da base de cálculo do PIS é obrigatório.';
    END IF;

    IF (p_cod_ret_pis is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código da retenção do PIS é obrigatório.';
    END IF;

    IF (p_aliq_pis is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Alíquota do PIS é obrigatório.';
    END IF;

    IF (p_valor_pis_imp is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor do PIS é obrigatório.';
    END IF;

    ------------------------
    -- Validações do COFINS --
    ------------------------
    IF (p_base_cofins is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor da base de cálculo do COFINS é obrigatório.';
    END IF;

    IF (p_cod_ret_cofins is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código da retenção do COFINS é obrigatório.';
    END IF;

    IF (p_aliq_cofins is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Alíquota do COFINS é obrigatório.';
    END IF;

    IF (p_valor_cofins_imp is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor do COFINS é obrigatório.';
    END IF;

    ------------------------
    -- Validações do CSL --
    ------------------------
    IF (p_base_csl is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor da base de cálculo do CSL é obrigatório.';
    END IF;

    IF (p_cod_ret_csl is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código da retenção do CSL é obrigatório.';
    END IF;

    IF (p_aliq_csl is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Alíquota do CSL é obrigatório.';
    END IF;

    IF (p_valor_csl_imp is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor do CSL é obrigatório.';
    END IF;

    ------------------------
    -- Validações do CSRF --
    ------------------------
    IF (p_base_csrf is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor da base de cálculo do CSRF é obrigatório.';
    END IF;

    IF (p_cod_ret_csrf is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código da retenção do CSRF é obrigatório.';
    END IF;

    IF (p_aliq_csrf is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Alíquota do CSRF é obrigatório.';
    END IF;

    IF (p_valor_csrf_imp is null) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Valor do CSRF é obrigatório.';
    END IF;

    -----------------------------------------------
    -- Processamento e validações mais complexas --
    -----------------------------------------------

    /*
    IF ST_PCK_FATU.exists_empresa(p_codigo_empresa) = false THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Empresa não existe.';
    ELSE
        p_gera_contabil := ST_PCK_FATU.is_empresa_gera_contab(p_codigo_empresa);
    END IF;
    */

    IF ST_PCK_CPAG.exists_tipo_titulo(p_tipo_titulo) = false THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Tipo de titulo não encontrado.';
    END IF;

    IF p_cod_portador > 0 AND ST_PCK_PEDI.exists_portador(p_cod_portador) = false THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código do portador não encontrado.';
    END IF;
    
    IF p_codigo_depto > 0 AND ST_PCK_BASI.is_centro_custo_atv(p_codigo_depto, p_codigo_empresa) = false THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código do depto(centro de custo) não encontrado ou inativo para a empresa informada.';
    END IF;

    IF p_moeda_titulo > 0 AND ST_PCK_BASI.exists_moeda_by_cod(p_moeda_titulo) = false THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código da moeda não encontrado.';
    END IF;

    IF ST_PCK_CONT.exists_tipo_contabil(p_codigo_contabil) = false THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código contábil não encontrado.';
    END IF;

    IF ST_PCK_CONT.exists_historico_by_cod(p_codigo_historico) = false THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código de histórico não encontrado.';
    END IF;

    IF p_tipo_pagamento > 0 AND ST_PCK_CONT.exists_tipo_pagamento(p_tipo_pagamento) THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código de tipo de pagamento não encontrado.';
    END IF;

    IF p_projeto > 0 AND p_subprojeto > 0 AND p_servico > 0 AND ST_PCK_CPAG.exists_projeto(p_projeto, p_subprojeto, p_servico) = false THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Projeto, subprojeto e serviço não encontrados.';
    END IF;

    IF ST_PCK_CONT.exists_posicao(p_posicao_titulo) = false THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Posição do titulo não encontrada.';
    END IF;

    IF p_cod_cancelamento > 0 AND ST_PCK_CPAG.exists_cancelamento(p_cod_cancelamento) = false THEN
        v_has_error := true;
        v_mensagem_erro :=  v_mensagem_erro || 'Código de cancelamento não encontrada.';

        p_has_error := v_has_error;
        p_mensagem_retorno := v_mensagem_erro;
    END IF;
    
    p_has_error := v_has_error;
    p_mensagem_retorno := v_mensagem_erro;
end inter_pr_msg_erro_titulo;

/

exec inter_pr_recompile;
