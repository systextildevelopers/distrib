declare
  i NUMBER := 0;

begin
   for registro in (select * from oper_tmp 
                    where oper_tmp.data_criacao is null)
   loop 
      update oper_tmp
      set oper_tmp.data_criacao = sysdate
      where oper_tmp.nr_solicitacao = registro.nr_solicitacao
        and oper_tmp.nome_relatorio = registro.nome_relatorio
        and oper_tmp.sequencia      = registro.sequencia;
   
      i := i + 1;
      IF i >= 1000 THEN
         COMMIT;
         i := 0;
      END IF;
   end loop;
end;
