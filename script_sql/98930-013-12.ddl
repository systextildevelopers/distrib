insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('crec_f378', 'Processo de Associação de Títulos à Cheques', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'crec_f378', 'Nenhum', 0, 0, 'N', 'S', 'N', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'crec_f378', 'Nenhum', 0, 0, 'N', 'S', 'N', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Processo de Associação de Títulos a Cheques'
 where hdoc_036.codigo_programa = 'crec_f378'
   and hdoc_036.locale          = 'es_ES';
commit;
/
