
  CREATE OR REPLACE FUNCTION "INTER_FN_BUSCA_SEQ_TAMANHO" (p_nivel    in varchar2,
                                                      p_grupo    in varchar2,
                                                      p_subgrupo in varchar2)

return number is
   v_seq_tamanho number;
begin
   -- inicializa variavel
   v_seq_tamanho   := 0;

   -- le a SEQUENCIA_TAMANHO da BASI_020
   begin
      select nvl(basi_020.sequencia_tamanho,0)
      into v_seq_tamanho
      from basi_020
      where basi_020.basi030_nivel030 = p_nivel
        and basi_020.basi030_referenc = p_grupo
        and basi_020.tamanho_ref      = p_subgrupo;
   exception when no_data_found then
      v_seq_tamanho := 0;
   end;

   -- verifica se a SEQUENCIA_TAMANHO e valida, ou seja, IGUAL A ZERO
   if v_seq_tamanho = 0
   then
      begin
         select nvl(basi_220.ordem_tamanho,0)
         into v_seq_tamanho
         from basi_220
         where basi_220.tamanho_ref = p_subgrupo;
      exception when no_data_found then
         v_seq_tamanho := 0;
      end;
   end if;

   return(v_seq_tamanho);

end inter_fn_busca_seq_tamanho;

 

/

exec inter_pr_recompile;

