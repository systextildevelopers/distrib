CREATE OR REPLACE TRIGGER inter_tr_obrf_015
before insert or
       delete or
       update of desconto_item, flag_saida, cd_cnpj_forne_9, cd_cnpj_forne_4,
    cd_cnpj_forne_2, cd_seq_nota_frete, observacao, qtd_pcs_caixa_rfid,
    valor_desp_com_icms_sped, valor_imposto_opera_finan_sped, valor_imposto_import_sped, cvf_ipi_entrada,
    base_subtituicao, perc_subtituicao, valor_subtituicao, cod_csosn,
    valor_frete, valor_desc, valor_outros, valor_seguro,
    numero_adicao, cod_base_cred, descricao_item, codigo_deposito,
    cod_vlfiscal_icm, percentual_icm, cvf_icm_diferenc, seq_nota_orig,
    dif_aliquota, flag_devolucao, obs_livro1, endereco,
    valor_icms_diferido, transacao_canc_nfisc, base_pis_cofins, formulario_num,
    cnpj_emis_edi2, perc_icms_subs, capa_ent_nrdoc, capa_ent_serie,
    capa_ent_forcli9, capa_ent_forcli4, capa_ent_forcli2, sequencia,
    coditem_nivel99, coditem_grupo, coditem_subgrupo, coditem_item,
    quantidade, lote_entrega, valor_unitario, unidade_medida,
    valor_ipi, valor_total, classific_fiscal, classif_contabil,
    cod_vlfiscal_ipi, pedido_compra, sequencia_pedido, percentual_ipi,
    centro_custo, preco_custo, natitem_nat_oper, natitem_est_oper,
    valor_icms, codigo_transacao, base_calc_icm, base_diferenca,
    procedencia, rateio_despesas, num_nota_orig, motivo_devolucao,
    serie_nota_orig, situacao_industr, codigo_contabil, empr_nf_saida,
    num_nf_saida, serie_nf_saida, obs_livro2, perc_dif_aliq,
    flag_orcamento, gramatura, perc_icms_diferido, usuario_cardex,
    nome_programa, data_canc_nfisc, ordem_producao, rateio_despesas_ipi,
    rateio_descontos_ipi, base_ipi, valor_pis, valor_cofins,
    projeto, subprojeto, servico, perc_iva_1,
    perc_iva_2, valor_iva_1, valor_iva_2, formulario_ser,
    formulario_seq, formulario_ori, flag_guia_remissao, tipo_nota_edi,
    cnpj_emis_edi9, cnpj_emis_edi4, executa_trigger, cvf_pis,
    cvf_cofins, perc_pis, perc_cofins, base_icms_subs,
    valor_icms_subs, tipo_doc_importacao_sped, valor_pis_import_sped, valor_cofins_import_sped,
    data_desembaraco_sped, valor_cif_sped, valor_desp_sem_icms_sped
   on obrf_015
   for each row

declare
   v_atualiza_estoque                estq_005.atualiza_estoque%type;
   v_tipo_transacao                  estq_005.tipo_transacao%type;
   v_tipo_transacao_005              estq_005.tipo_transacao%type;
   v_calcula_preco                   estq_005.calcula_preco%type;
   v_trans_terceiro                  estq_005.codigo_transacao%type;
   v_ent_sai_terc                    estq_005.entrada_saida%type;
   v_trans_terceiro_canc             estq_005.codigo_transacao%type;

   v_tipo_volume                     basi_205.tipo_volume%type;
   v_confirma_coleta                 basi_205.confirma_coleta%type;
   v_deposito_terceiro               basi_205.codigo_deposito%type;

   v_data_transacao                  obrf_010.data_transacao%type;
   v_situacao_entrada                obrf_010.situacao_entrada%type;
   v_local_entrega                   obrf_010.local_entrega%type;
   v_especie_docto                   obrf_010.especie_docto%type;
   v_moeda                           obrf_010.moeda_nota%type;
   v_valor_total                     obrf_015.valor_total%type;
   v_rateio_despesas                 obrf_015.rateio_despesas%type;

   v_pedido_venda_nfs                fatu_060.pedido_venda%type;
   v_seq_item_pedido_nfs             fatu_060.seq_item_pedido%type;
   v_pedido_venda_orig               fatu_060.pedido_venda%type;
   v_pedido_venda_ent_conf           fatu_060.pedido_venda%type;
   v_seq_item_pedido_conf            fatu_060.seq_item_pedido%type;

   v_cod_empresa_nfs                 fatu_500.codigo_empresa%type;
   v_cod_empresa_par                 fatu_500.codigo_empresa%type;
   v_cod_empresa_nfs050              fatu_500.codigo_empresa%type;
   v_deposito_saida                  fatu_060.deposito%type;
   v_lote_saida                      fatu_060.lote_acomp%type;

   v_processo_nota                   obrf_510.processo_nota%type;
   v_cod_emp_benef                   obrf_510.cod_emp_benef%type;
   v_cod_emp_origem                  obrf_510.cod_emp_origem%type;
   v_centro_custo_terceiro           hdoc_001.campo_numerico32%type;

   v_atual_estoque_f                 varchar2(1);
   v_permite_fatura_atu_esq          varchar2(1);
   v_atualiza_ncm_entrada            number;

   v_sit_rolo                        number;
   v_sit_caixa                       number;
   v_sit_fardo                       number;
   v_nr_registro_300                 number;
   v_valor_contabil                  number;
   v_valor_unitario                  number;
   v_tran_cancel_nfe                 number;
   v_par_atualiza_estoque_f          number;
   v_respeita_confirmacao            number;
   v_indice_moeda                    number;
   v_limpa_pedido_volume             number;
   v_cidadeempresa                   number;
   v_paisempresa                     number;
   v_quantidade_nf                   number;
   v_preco_custo                     number;
   v_local_entrega1                  number;
   v_doc_conta_corrente              number;
   v_qtde_kilo                       number;
   v_cont_processo                   number;
   v_tipo_processo                   number;
   v_atualiza_estoque_terceiro       estq_005.atualiza_estoque%type;

   v_executa_trigger                 number;
   v_verifica                        number;
   p_nota_entrega                    number;
   p_nota_fatura                     number;

   produto_elab number;
   empresa_nota number;
   outra_empresa boolean;
   v_deposito_transicao number;

   v_codigo_empresa                 fatu_503.codigo_empresa%type;
   v_transacao_financeira           pedi_080.nfe_atu_financeiro%type;
   v_istajustefinanceiro1           estq_005.istajustefinanceiro%type;
   v_istajustefinanceiro2           estq_005.istajustefinanceiro%type;
   v_istajustefinanceiro3           estq_005.istajustefinanceiro%type;
   v_istajustefinanceiro4           estq_005.istajustefinanceiro%type;

   v_devolucao_novos_rolos          number;
   v_qtde_rolos_nota                number;
   p_opcao_conf_volume              number;

   v_dep_saida_terc                 number;
   v_dep_saida_terc_1               number;
   v_dep_saida_terc_2               number;
   v_dep_saida_terc_4               number;
   v_dep_saida_terc_7               number;
   v_dep_saida_terc_9               number;
   v_dep_terceiro_572               basi_572.cod_dep%type;
   v_cest                           basi_010.cest%type;
   v_nfe_ambiente                   obrf_158.nfe_ambiente%type;
   v_data_legislacao                obrf_158.data_legislacao%type;

   v_unid_med_trib_tmp              obrf_015.unid_med_trib%type;
   v_qtde_movimentada_rolos         number;
   v_data_canc_nfisc                obrf_015.data_canc_nfisc%type;
   v_periodo_estoque                empr_001.periodo_estoque%type;
   v_atualiza_estq_terceiro         estq_005.atualiza_estq_terceiro%type;

   v_responsavel9                   obrf_010.responsavel9%type;
   v_responsavel4                   obrf_010.responsavel4%type;
   v_responsavel2                   obrf_010.responsavel2%type;
   v_endereco_rolo                  varchar2(30);

   v_comprado_fabric                number;
   v_classif_fisc                   basi_010.classific_fiscal%type;
   v_class_fisc_a_classificar       empr_002.classific_fiscal%type;

begin
   -- INICIO - L?gica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementa??o executada para limpeza da base de dados do cliente
   -- e replica??o dos dados para base hist?rico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if not deleting
      then
         select obrf_010.local_entrega, obrf_010.situacao_entrada,
                obrf_010.responsavel9, obrf_010.responsavel4,
                obrf_010.responsavel2
         into   v_cod_empresa_nfs,      v_situacao_entrada,
                v_responsavel9, v_responsavel4,
                v_responsavel2
         from obrf_010
         where documento     = :new.capa_ent_nrdoc
           and serie         = :new.capa_ent_serie
           and cgc_cli_for_9 = :new.capa_ent_forcli9
           and cgc_cli_for_4 = :new.capa_ent_forcli4
           and cgc_cli_for_2 = :new.capa_ent_forcli2;
      end if;

      if deleting
      then
         select obrf_010.local_entrega, obrf_010.situacao_entrada,
                obrf_010.responsavel9, obrf_010.responsavel4,
                obrf_010.responsavel2
         into   v_cod_empresa_nfs,      v_situacao_entrada,
                v_responsavel9, v_responsavel4,
                v_responsavel2
         from obrf_010
         where documento     = :old.capa_ent_nrdoc
           and serie         = :old.capa_ent_serie
           and cgc_cli_for_9 = :old.capa_ent_forcli9
           and cgc_cli_for_4 = :old.capa_ent_forcli4
           and cgc_cli_for_2 = :old.capa_ent_forcli2;
           
      end if;

      if not deleting  and trim(:new.cest) is null
      then
         :new.cest := ' ';

         begin
           select obrf_158.nfe_ambiente, obrf_158.data_legislacao
                 into v_nfe_ambiente, v_data_legislacao
                 from obrf_158
                 where obrf_158.cod_empresa = v_cod_empresa_nfs;
         exception
            when no_data_found then
            v_nfe_ambiente := null;
            v_data_legislacao:= null;
         end;


         if v_nfe_ambiente = 2 or to_date(v_data_legislacao) <= to_date(sysdate)
         then begin
              select basi_010.cest into v_cest
              from basi_010
              where basi_010.nivel_estrutura  = :new.coditem_nivel99
                and basi_010.grupo_estrutura  = :new.coditem_grupo
                and basi_010.subgru_estrutura = :new.coditem_subgrupo
                and basi_010.item_estrutura   = :new.coditem_item;
             exception
                when no_data_found then
                v_cest := ' ';
             end;
             :new.cest := v_cest;
         end if;
      end if;

      if inserting
      then
         -- Eliane In?cio
         select obrf_010.local_entrega
         into   v_codigo_empresa
         from obrf_010
         where documento     = :new.capa_ent_nrdoc
           and serie         = :new.capa_ent_serie
           and cgc_cli_for_9 = :new.capa_ent_forcli9
           and cgc_cli_for_4 = :new.capa_ent_forcli4
           and cgc_cli_for_2 = :new.capa_ent_forcli2;

         begin
            inter_pr_entr_doc_fiscal_item (v_cod_empresa_nfs,

                                           :new.coditem_nivel99,
                                           :new.coditem_grupo,
                                           :new.coditem_subgrupo,
                                           :new.coditem_item);
         end;
         -- Eliane Fim

         begin
            select fatu_502.atualiza_estoque_f , fatu_502.respeita_confirmacao, fatu_502.permite_fatura_atu_esq, fatu_502.atualiza_ncm_entrada
            into v_par_atualiza_estoque_f, v_respeita_confirmacao, v_permite_fatura_atu_esq, v_atualiza_ncm_entrada
            from obrf_010, fatu_502
            where obrf_010.local_entrega = fatu_502.codigo_empresa
              and documento     = :new.capa_ent_nrdoc
              and serie         = :new.capa_ent_serie
              and cgc_cli_for_9 = :new.capa_ent_forcli9
              and cgc_cli_for_4 = :new.capa_ent_forcli4
              and cgc_cli_for_2 = :new.capa_ent_forcli2;
         end;


         -- verifica se a classificacao fiscal esta a classificar
         if :new.coditem_nivel99 <> '0'and :new.coditem_grupo <> '0'
         then
            begin
               select  empr_002.classific_fiscal  into v_class_fisc_a_classificar   
               from empr_002;
            exception
               when no_data_found then
               v_class_fisc_a_classificar := ' ';
            end;
                  
            begin
               select basi_010.classific_fiscal into v_classif_fisc
               from basi_010
               where basi_010.nivel_estrutura  = :new.coditem_nivel99
                 and basi_010.grupo_estrutura  = :new.coditem_grupo
                 and basi_010.subgru_estrutura = :new.coditem_subgrupo
                 and basi_010.item_estrutura   = :new.coditem_item;
            exception
               when no_data_found then
               v_classif_fisc := ' ';
            end;
             
             
            if v_classif_fisc = v_class_fisc_a_classificar
            then
               begin
                  update basi_010
                  set basi_010.classific_fiscal = :new.classific_fiscal
                  where nivel_estrutura  = :new.coditem_nivel99
                    and   grupo_estrutura  = :new.coditem_grupo
                    and   subgru_estrutura = :new.coditem_subgrupo
                    and   item_estrutura   = :new.coditem_item;
               exception     
                  when others then
                  raise_application_error(-20000,'ATEN??O! A classifica??o fiscal n?o foi atualizada.');
               end;
            end if;

            if v_atualiza_ncm_entrada = 1
            then
               begin
                  update basi_010
                  set basi_010.classific_fiscal = :new.classific_fiscal
                  where nivel_estrutura  = :new.coditem_nivel99
                    and   grupo_estrutura  = :new.coditem_grupo
                    and   subgru_estrutura = :new.coditem_subgrupo
                    and   item_estrutura   = :new.coditem_item
                    and   classific_fiscal   <> :new.classific_fiscal;
               exception     
                  when others then
                  raise_application_error(-20000,'ATEN??O! A classifica??o fiscal n?o foi atualizada.');
               end;
            end if;
         end if;            
         --fim    
       
        
         v_quantidade_nf:= :new.quantidade;
         v_preco_custo  := :new.preco_custo;

         if trim(:new.unidade_conv) is null
         then
            :new.unidade_conv := :new.unidade_medida;
         end if;

         -- le a transacao para verificar se ela atualiza estoque. Busca tambem o tipo de transacao.
         select estq_005.atualiza_estoque,   estq_005.tipo_transacao,
                estq_005.atual_estoque_f,    estq_005.calcula_preco,
                estq_005.istajustefinanceiro
         into   v_atualiza_estoque,          v_tipo_transacao,
                v_atual_estoque_f,           v_calcula_preco,
                v_istajustefinanceiro1
         from estq_005
         where codigo_transacao = :new.codigo_transacao;

         -- le os parametros de deposito de saida em pdoer de terceiros
         v_dep_saida_terc_1 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida1');
         v_dep_saida_terc_2 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida2');
         v_dep_saida_terc_4 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida4');
         v_dep_saida_terc_7 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida7');
         v_dep_saida_terc_9 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida9');

         if :new.coditem_nivel99 = '1'
         then v_dep_saida_terc := v_dep_saida_terc_1;
         elsif :new.coditem_nivel99 = '2'
         then v_dep_saida_terc := v_dep_saida_terc_2;
         elsif :new.coditem_nivel99 = '4'
         then v_dep_saida_terc := v_dep_saida_terc_4;
         elsif :new.coditem_nivel99 = '7'
         then v_dep_saida_terc := v_dep_saida_terc_7;
         elsif :new.coditem_nivel99 = '9'
         then v_dep_saida_terc := v_dep_saida_terc_9;
         end if;

         if v_par_atualiza_estoque_f = 1 and v_atual_estoque_f = 'N' and v_atualiza_estoque = 1
         then
            raise_application_error(-20000,'Quando a empresa atualiza estoque gerencial, a transacao tambem deve atualizar estoque gerencial.');
         end if;

         -- caso a transacao atualise estoque, checa se o deposito foi informado
         if v_atualiza_estoque = 1 and :new.codigo_deposito = 0
         then
            raise_application_error(-20000,'ATEN??O! O dep?sito deve ser informado quando a transa??o atualizar estoque.');
         end if;

         -- caso a transacao nao atualise estoque, checa se o deposito foi informado
         if v_atualiza_estoque = 2 and :new.codigo_deposito <> 0
         then
            raise_application_error(-20000,'ATEN??O! O dep?sito n?o deve ser informado quando a transa??o n?o atualizar estoque.');
         end if;

         -- caso o deposito foi informado e o produto e sem codigo, gera mensagem de erro
         if :new.codigo_deposito <> 0 and :new.coditem_nivel99 = 0
         then
            raise_application_error(-20000,'Se informar um deposito, deve-se informar um produto de estoque');
         end if;

         if v_situacao_entrada not in (2,4,5) and v_tipo_transacao = 'D'
         then
            select inter_fn_unid_med_tributaria(:new.natitem_nat_oper,
                                                :new.natitem_est_oper,
                                                decode(trim(:new.unidade_conv),null,:new.unidade_medida,:new.unidade_conv),
                                                :new.classific_fiscal,
                                                :new.coditem_nivel99,
                                                :new.coditem_grupo,
                                                :new.coditem_subgrupo,
                                                :new.coditem_item)
            into v_unid_med_trib_tmp from dual;
         end if;

         -- Buscando a data da entrada e empresa da nota fiscal
         select data_transacao, local_entrega, moeda_nota,
                obrf_010.especie_docto
         into v_data_transacao, v_local_entrega, v_moeda,
              v_especie_docto
         from obrf_010
         where documento     = :new.capa_ent_nrdoc
           and serie         = :new.capa_ent_serie
           and cgc_cli_for_9 = :new.capa_ent_forcli9
           and cgc_cli_for_4 = :new.capa_ent_forcli4
           and cgc_cli_for_2 = :new.capa_ent_forcli2;

        if v_tipo_transacao = 'D'
         then
            begin
              select endereco_rolo_devolvido
              into v_endereco_rolo
              from fatu_504
              where codigo_empresa = v_local_entrega;
              exception when others then
                v_endereco_rolo := '';
            end;
         end if;

         if :new.codigo_deposito > 0
         then

            -- Verificando o tipo de volume e se o deposito precisa confirmar coleta
            select tipo_volume,       confirma_coleta
            into   v_tipo_volume,     v_confirma_coleta
            from basi_205
            where codigo_deposito = :new.codigo_deposito;

            /*Atualiza o valor para a moeda corrente*/

            if v_moeda <> 0
            then
               begin
                  select valor_moeda into v_indice_moeda
                  from basi_270
                  where basi_270.data_moeda   = v_data_transacao
                    and basi_270.codigo_moeda = v_moeda;
                  exception
                     when others then
                       v_indice_moeda := 1;
               end;
            else
               v_indice_moeda := 1;
            end if;

            v_valor_total     := :new.valor_total * v_indice_moeda;
            v_rateio_despesas := :new.rateio_despesas * v_indice_moeda;

            -- Calculando o valor contabil unitario para gravacao no Cardex
            if :new.quantidade > 0.000
            then
               v_valor_contabil := (v_valor_total + v_rateio_despesas) / :new.quantidade;
               v_valor_unitario := :new.preco_custo / :new.quantidade;
            else
               v_valor_contabil := v_valor_total + v_rateio_despesas;
               v_valor_unitario := :new.preco_custo;
            end if;

             -- LOGICA PARA PROCURA O PAIS DA EMPRESA DA NOTA
            begin
               select fatu_500.codigo_cidade into v_cidadeempresa from fatu_500
               where fatu_500.codigo_empresa = v_local_entrega;

               exception
                  when others then
                     v_cidadeempresa := 0;
            end;

            begin
               select basi_160.codigo_pais into v_paisempresa from basi_160
               where basi_160.cod_cidade = v_cidadeempresa;
               exception
                  when others then
                     v_paisempresa := 1;
            end;

            if v_paisempresa <> 1
            then
               if v_especie_docto = 'FAC' -- NOTA FATURA
               then
                   if not(v_atualiza_estoque = 1 and v_permite_fatura_atu_esq = 'S')
                   then
                       v_quantidade_nf := 0.000;
                   end if;
               end if;

               if v_especie_docto = 'GRE' -- NOTA REMISSAO
               then
                  v_valor_contabil := 0.00;
                  v_preco_custo := 0.00;
               end if;
            end if;

--- alteracao para inventario (peso do rolo) INICIO


            -- se for tecido e for volume, calcula o peso do rolo
            if :new.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)
            then
               if :new.nome_programa not in('obrf_f200', 'obrf_f200_live', 'obrf_e147') -- Confirmacao de nota de entrada
               then
                  if v_tipo_transacao not in ('D','R')
                  then
                     begin
                        -- le o total de quilos dos rolos (para controle de rolos em metros)
                        select nvl(sum(peso_bruto), 0) into v_qtde_kilo from pcpt_020
                        where cliente_cgc9       = :new.capa_ent_forcli9
                          and cliente_cgc4       = :new.capa_ent_forcli4
                          and cliente_cgc2       = :new.capa_ent_forcli2
                          and nota_fiscal_ent    = :new.capa_ent_nrdoc
                          and seri_fiscal_ent    = :new.capa_ent_serie
                          and sequ_fiscal_ent    = 0  -- Nao relacionado a item de nota entrada
                          and rolo_estoque       = 0
                          and panoacab_nivel99   = :new.coditem_nivel99
                          and panoacab_grupo     = :new.coditem_grupo
                          and panoacab_subgrupo  = :new.coditem_subgrupo
                          and panoacab_item      = :new.coditem_item
                          and codigo_deposito    = :new.codigo_deposito;

                     exception
                        when others then
                           v_qtde_kilo := 0.00;
                     end;
                  else

                     begin
                        -- le o total de quilos dos rolos (para controle de rolos em metros)
                        select nvl(sum(peso_bruto), 0) into v_qtde_kilo from pcpt_020
                        where cod_empresa_nota = v_local_entrega
                          and nota_fiscal      = :new.num_nota_orig
                          and serie_fiscal_sai = :new.serie_nota_orig
                          and seq_nota_fiscal  = :new.seq_nota_orig
                          and rolo_estoque     = 2;   -- Rolo faturado

                     exception
                        when others then
                          v_qtde_kilo := 0.00;
                     end;
                  end if;  -- if v_tipo_transacao not in ('D','R')
               else

                  begin
                     -- Busca o codigo da empresa da nota fiscal de saida
                     -- pelo CNPJ do fornecedor da nota de entrada
                     select min(codigo_empresa)
                     into v_cod_empresa_nfs
                     from fatu_500
                     where cgc_9 = :new.capa_ent_forcli9
                       and cgc_4 = :new.capa_ent_forcli4
                       and cgc_2 = :new.capa_ent_forcli2;

                     -- le o total de quilos dos rolos (para controle de rolos em metros)
                     select nvl(sum(peso_bruto), 0) into v_qtde_kilo from pcpt_020
                     where cod_empresa_nota   = v_cod_empresa_nfs
                       and nota_fiscal        = :new.capa_ent_nrdoc
                       and serie_fiscal_sai   = :new.capa_ent_serie
                       and seq_nota_fiscal    = :new.sequencia
                       and panoacab_nivel99   = :new.coditem_nivel99
                       and panoacab_grupo     = :new.coditem_grupo
                       and panoacab_subgrupo  = :new.coditem_subgrupo
                       and panoacab_item      = :new.coditem_item
                       and rolo_estoque       = 2;   -- Rolo faturado

                  exception
                    when others then
                       v_qtde_kilo := 0.00;
                  end;
               end if;  -- if :new.nome_programa <> 'obrf_f200'
            end if;     -- if :new.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)

            -- Consist?ncias para controlar a atualiza??o do estoque via pcpt_020
            -- Se o produto for em elabora??o
            -- Se a empresa da nota ? a mesma na qual est? movimentando
            -- Se existe dep?sito de mercadoria em tr?nsito
            -- N?o deve atualizar estoque (estq_300) por esta trigger
            -- Mas sim pela trigger da pcpt_020
            produto_elab := 1;

            begin
                select 1
                into   produto_elab
                from pedi_860
                where pedi_860.niv_prod_elab  = :new.coditem_nivel99
                  and pedi_860.gru_prod_elab  = :new.coditem_grupo
                  and pedi_860.sub_prod_elab  = :new.coditem_subgrupo
                  and pedi_860.item_prod_elab = :new.coditem_item;
              exception
                when no_data_found
                then produto_elab := 0;
            end;

            begin
                select min(fatu_500.codigo_empresa)
                into   empresa_nota
                from fatu_500
                where fatu_500.cgc_9 = :new.capa_ent_forcli9
                  and fatu_500.cgc_4 = :new.capa_ent_forcli4
                  and fatu_500.cgc_2 = :new.capa_ent_forcli2;
            end;

            if empresa_nota is null
            then empresa_nota := 0;
            end if;

            if empresa_nota <> :new.lote_entrega and
               empresa_nota  > 0
            then outra_empresa := true;
            else outra_empresa := false;
            end if;

            select empr_002.deposito_transicao
            into   v_deposito_transicao
            from empr_002;

            if not(produto_elab  = 1 and outra_empresa = true and v_deposito_transicao > 0)  and v_situacao_entrada <> 2
            then
               if v_istajustefinanceiro1 = 1
               then v_quantidade_nf := 0.00;
               end if;

               -- Enviando informacoes para atualizacao do Cardex
               inter_pr_insere_estq_300_recur (:new.codigo_deposito,  :new.coditem_nivel99,   :new.coditem_grupo,
                                         :new.coditem_subgrupo, :new.coditem_item,       v_data_transacao,
                                         :new.lote_entrega,     :new.capa_ent_nrdoc,    :new.capa_ent_serie,
                                         :new.capa_ent_forcli9, :new.capa_ent_forcli4,  :new.capa_ent_forcli2,
                                         :new.sequencia,
                                         :new.codigo_transacao, 'E',                    :new.centro_custo,
                                         v_quantidade_nf,       v_preco_custo,          v_valor_contabil,
                                         :new.usuario_cardex,   'OBRF_015',             :new.nome_programa,
                                         v_qtde_kilo,            0,                       0,
                                         :new.capa_ent_nrdoc,    0,                       0);
            end if;



            -- Verificando se precisamos atualizar tag's sem volumes 77567.001
            if v_tipo_volume = 0 and v_istajustefinanceiro1 <> 1 and :new.coditem_nivel99  = '1' and :new.nome_programa = 'obrf_f200'
            then

               v_pedido_venda_ent_conf := 0;
               v_seq_item_pedido_conf  := 0;

                update pcpc_330
                   set deposito             = :new.codigo_deposito,
                       flag_controle        = 0,
                       pcpc_330.lote        = :new.lote_entrega,
                       pedido_venda         = v_pedido_venda_ent_conf,
                       seq_item_pedido      = v_seq_item_pedido_conf,
                       forn9_entr           = :new.capa_ent_forcli9,
                       forn4_entr           = :new.capa_ent_forcli4,
                       forn2_entr           = :new.capa_ent_forcli2,
                       nota_entr            = :new.capa_ent_nrdoc,
                       serie_entr           = :new.capa_ent_serie,
                       seq_entr             = :new.sequencia,
                       usuario_cardex       = :new.usuario_cardex,
                       nome_prog_050        = :new.nome_programa,
                       tabela_origem_cardex = 'OBRF_015'
                where nota_inclusao      = :new.capa_ent_nrdoc
                  and serie_nota         = :new.capa_ent_serie
                  and flag_controle      = 1;   -- Tag faturado  entrei
                end if;

            -- Verificando se precisamos atualizar volumes
            if v_tipo_volume <> 0 and v_istajustefinanceiro1 <> 1
            then
               if :new.nome_programa not in('obrf_f200', 'obrf_f200_live', 'obrf_e147')  -- Confirmac?o de nota de entrada
               then

            if v_tipo_volume =  1
      then
         raise_application_error(-20000,'Para dar entrada em dep?sito de tags dever? ser criada uma O.P.cujo roteiro ter? somente o ?ltimo est?gio e a entrada deve ser na Entrada de Produ??o por Pe?a pcpc_f140, se a tag existir reativ?-la na Reativa??o de Tags estq_f205.');
      end if;

                  -- Quando o transacao da nota fiscal de entrada n?o for de devoluc?o ou Rertono de Terceiro
                  if v_tipo_transacao not in ('D','R')
                  then

                     if :new.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)
                     then

                        if :new.nome_programa <> 'inte_f260' -- Confirmacao de nota de entrada via integra??o
                        then
                            select nvl(sum(pcpt_020.qtde_quilos_acab), 0) /*cra1*/
                            into   v_qtde_movimentada_rolos
                            from pcpt_020
                            where pcpt_020.cliente_cgc9       = :new.capa_ent_forcli9
                              and pcpt_020.cliente_cgc4       = :new.capa_ent_forcli4
                              and pcpt_020.cliente_cgc2       = :new.capa_ent_forcli2
                              and pcpt_020.nota_fiscal_ent    = :new.capa_ent_nrdoc
                              and pcpt_020.seri_fiscal_ent    = :new.capa_ent_serie
                              and pcpt_020.sequ_fiscal_ent    = 0  -- Nao relacionado a item de nota entrada
                              and pcpt_020.rolo_estoque       = 0
                              and pcpt_020.panoacab_nivel99   = :new.coditem_nivel99
                              and pcpt_020.panoacab_grupo     = :new.coditem_grupo
                              and pcpt_020.panoacab_subgrupo  = :new.coditem_subgrupo
                              and pcpt_020.panoacab_item      = :new.coditem_item
                              and pcpt_020.codigo_deposito    = :new.codigo_deposito;

                            if v_qtde_movimentada_rolos <> :new.quantidade
                            then
                               raise_application_error(-20000,'ATEN??O! Quantidade acumulada dos rolos n?o ? igual a quantidade informada no item da nota fiscal.');
                            end if;
                        end if;

                        if v_confirma_coleta = 1
                        then
                            v_sit_caixa := 4;
                        else
                            v_sit_caixa := 1;
                        end if;

                        -- Atualizando as informacoes do rolo
                        update pcpt_020
                        set rolo_estoque         = v_sit_caixa,
                            sequ_fiscal_ent      = :new.sequencia,
                            data_cardex          = v_data_transacao,
                            transacao_cardex     = :new.codigo_transacao,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog_020        = :new.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                        where cliente_cgc9       = :new.capa_ent_forcli9
                          and cliente_cgc4       = :new.capa_ent_forcli4
                          and cliente_cgc2       = :new.capa_ent_forcli2
                          and nota_fiscal_ent    = :new.capa_ent_nrdoc
                          and seri_fiscal_ent    = :new.capa_ent_serie
                          and sequ_fiscal_ent    = 0  -- Nao relacionado a item de nota entrada
                          and rolo_estoque       = 0
                          and panoacab_nivel99   = :new.coditem_nivel99
                          and panoacab_grupo     = :new.coditem_grupo
                          and panoacab_subgrupo  = :new.coditem_subgrupo
                          and panoacab_item      = :new.coditem_item
                          and codigo_deposito    = :new.codigo_deposito;
                          -- Comentado o campo lote_acomp do where para que sempre seja atualizado a situa??o do rolo
                          -- para estoque (1) de todos os rolos do item da nota. Isto foi necess?rio, pois nem todos os rolos,
                          -- possuem o mesmo lote, e desta forma, n?o gravava rolos que n?o estivesse no mesmo lote.
                          --and lote_acomp         = :new.lote_entrega;
                     end if;             --  if :new.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)

                     if :new.coditem_nivel99 = '7' and v_tipo_volume = 7
                     then

                        if v_confirma_coleta = 1
                        then
                            v_sit_caixa := 0;
                        else
                            v_sit_caixa := 1;
                        end if;


                        -- Atualizando as informacoes da caixa
                        update estq_060
                        set status_caixa         = v_sit_caixa,
                            sequ_fisc_ent        = :new.sequencia,
                            data_cardex          = v_data_transacao,
                            transacao_cardex     = :new.codigo_transacao,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog            = :new.nome_programa,
                            tabela_origem_cardex = 'OBRF_015',
                            numero_pedido        = 0,
                            sequencia_pedido     = 0
                        where cnpj_fornecedor9   = :new.capa_ent_forcli9
                          and cnpj_fornecedor4   = :new.capa_ent_forcli4
                          and cnpj_fornecedor2   = :new.capa_ent_forcli2
                          and nota_fisc_ent      = :new.capa_ent_nrdoc
                          and seri_fisc_ent      = :new.capa_ent_serie
                          and sequ_fisc_ent      = 0 -- Nao relacionado a item de nota entrada
                          and status_caixa       = 8
                          and prodcai_nivel99    = :new.coditem_nivel99
                          and prodcai_grupo      = :new.coditem_grupo
                          and prodcai_subgrupo   = :new.coditem_subgrupo
                          and prodcai_item       = :new.coditem_item
                          and codigo_deposito    = :new.codigo_deposito
                          and lote               = :new.lote_entrega
                          and numero_pedido      = :new.pedido_compra
                          and sequencia_pedido   = :new.sequencia_pedido;
                     end if;          -- if :new.coditem_nivel99 = '7' and v_tipo_volume = 7

                  else /*? devolucao*/

                     if  :new.coditem_nivel99 in ('2','4')
                     and v_tipo_volume        in (2,4)
                     and :new.num_nota_orig   > 0
                     and :new.seq_nota_orig   > 0
                     and v_tipo_transacao     in ('D','R') -- Devolucao
                     then

                        -- Atualizando as informacoes do rolo

                        /* verificar se existe o rolo */
                        begin
                          select 1
                          into v_verifica
                          from pcpt_020
                          where cod_empresa_nota = v_local_entrega
                            and nota_fiscal      = :new.num_nota_orig
                            and serie_fiscal_sai = :new.serie_nota_orig
                            and seq_nota_fiscal  = :new.seq_nota_orig
                            and rolo_estoque     = 2
                            and rownum = 1;   -- Rolo faturado
                        exception when no_data_found then
                           raise_application_error(-20000,'Rolo n?o encontrado para NF de origem informada.Verifique!');
                        end;

                        /* SS 74612/002 - DEVOLU??O COM NOVOS ROLOS JCEL */
                        v_devolucao_novos_rolos := 0; --N?o ? devolu??o de novos rolos

                        begin

                           select nvl(max(0), 1)
                           into v_devolucao_novos_rolos
                           from pcpt_020
                           where pcpt_020.cod_empresa_nota = v_local_entrega
                             and pcpt_020.nota_fiscal      = :new.num_nota_orig
                             and pcpt_020.serie_fiscal_sai = :new.serie_nota_orig
                             and pcpt_020.seq_nota_fiscal  = :new.seq_nota_orig
                             and pcpt_020.rolo_estoque     = 2 -- Rolo faturado
                             and not exists (select 1 from rcnb_060
                                             where rcnb_060.tipo_registro       = 41
                                               and rcnb_060.item_estrutura      = pcpt_020.codigo_rolo
                                               and rcnb_060.campo_numerico      = pcpt_020.cod_empresa_nota
                                               and rcnb_060.nivel_estrutura     = pcpt_020.nota_fiscal
                                               and rcnb_060.nivel_estrutura_str = pcpt_020.serie_fiscal_sai
                                               and rcnb_060.grupo_estrutura     = pcpt_020.seq_nota_fiscal
                                               and rcnb_060.tipo_registro_rel   = 0);
                        exception when no_data_found then
                           v_devolucao_novos_rolos := 1; --? devolu??o de novos rolos, porque nenhum rolo foi selecionado na obrf_f041
                        end;

                        /* BUSCAR QUANTOS ROLOS EST?O ASSOCIADOS A ESTA NOTA JCEL */
                        v_qtde_rolos_nota := 0;

                        begin

                           select count(0)
                           into v_qtde_rolos_nota
                           from pcpt_020
                           where cliente_cgc9       = :new.capa_ent_forcli9
                             and cliente_cgc4       = :new.capa_ent_forcli4
                             and cliente_cgc2       = :new.capa_ent_forcli2
                             and nota_fiscal_ent    = :new.capa_ent_nrdoc
                             and seri_fiscal_ent    = :new.capa_ent_serie
                             and sequ_fiscal_ent    = 0  -- Nao relacionado a item de nota entrada
                             and rolo_estoque       = 0
                             and panoacab_nivel99   = :new.coditem_nivel99
                             and panoacab_grupo     = :new.coditem_grupo
                             and panoacab_subgrupo  = :new.coditem_subgrupo
                             and panoacab_item      = :new.coditem_item
                             and codigo_deposito    = :new.codigo_deposito;
                        exception when no_data_found then
                           v_qtde_rolos_nota := 0; --? devolu??o de novos rolos, porque nenhum rolo foi selecionado na obrf_f041
                        end;

                        /* Se for devolu??o de novos rolos, fazer o update da sequencia do item da nota jcel */
                        if v_devolucao_novos_rolos = 1
                        then

                           if v_qtde_rolos_nota <> 0
                           then

                              if v_confirma_coleta = 1
                              then
                                  v_sit_caixa := 4;
                                 else
                                  v_sit_caixa := 1;
                              end if;

                select nvl(sum(pcpt_020.qtde_quilos_acab), 0)
                              into   v_qtde_movimentada_rolos
                              from pcpt_020
                              where cliente_cgc9       = :new.capa_ent_forcli9
                                and cliente_cgc4       = :new.capa_ent_forcli4
                                and cliente_cgc2       = :new.capa_ent_forcli2
                                and nota_fiscal_ent    = :new.capa_ent_nrdoc
                                and seri_fiscal_ent    = :new.capa_ent_serie
                                and sequ_fiscal_ent    = 0  -- Nao relacionado a item de nota entrada
                                and rolo_estoque       = 0
                                and panoacab_nivel99   = :new.coditem_nivel99
                                and panoacab_grupo     = :new.coditem_grupo
                                and panoacab_subgrupo  = :new.coditem_subgrupo
                                and panoacab_item      = :new.coditem_item
                                and codigo_deposito    = :new.codigo_deposito;

                              if v_qtde_movimentada_rolos <> :new.quantidade
                              then
                                 raise_application_error(-20000,'ATEN??O! Quantidade acumulada dos rolos n?o ? gual a quantidade informada no item da nota fiscal.');
                              end if;

                              -- Atualizando as informacoes do rolo
                              update pcpt_020
                              set rolo_estoque         = v_sit_caixa,
                                  sequ_fiscal_ent      = :new.sequencia,
                                  data_cardex          = v_data_transacao,
                                  transacao_cardex     = :new.codigo_transacao,
                                  usuario_cardex       = :new.usuario_cardex,
                                  nome_prog_020        = :new.nome_programa,
                                  tabela_origem_cardex = 'OBRF_015'
                              where cliente_cgc9       = :new.capa_ent_forcli9
                                and cliente_cgc4       = :new.capa_ent_forcli4
                                and cliente_cgc2       = :new.capa_ent_forcli2
                                and nota_fiscal_ent    = :new.capa_ent_nrdoc
                                and seri_fiscal_ent    = :new.capa_ent_serie
                                and sequ_fiscal_ent    = 0  -- Nao relacionado a item de nota entrada
                                and rolo_estoque       = 0
                                and panoacab_nivel99   = :new.coditem_nivel99
                                and panoacab_grupo     = :new.coditem_grupo
                                and panoacab_subgrupo  = :new.coditem_subgrupo
                                and panoacab_item      = :new.coditem_item
                                and codigo_deposito    = :new.codigo_deposito;
                                -- Comentado o campo lote_acomp do where para que sempre seja atualizado a situa??o do rolo
                                -- para estoque (1) de todos os rolos do item da nota. Isto foi necess?rio, pois nem todos os rolos,
                                -- possuem o mesmo lote, e desta forma, n?o gravava rolos que n?o estivesse no mesmo lote.
                                --and lote_acomp         = :new.lote_entrega;
                                if v_tipo_transacao = 'D' and trim(v_endereco_rolo) is not null
                                then
                                 update pcpt_020
                                    set endereco_rolo = v_endereco_rolo
                                  where cliente_cgc9       = :new.capa_ent_forcli9
                                    and cliente_cgc4       = :new.capa_ent_forcli4
                                    and cliente_cgc2       = :new.capa_ent_forcli2
                                    and nota_fiscal_ent    = :new.capa_ent_nrdoc
                                    and seri_fiscal_ent    = :new.capa_ent_serie
                                    and sequ_fiscal_ent    = :new.sequencia
                                    and rolo_estoque       = v_sit_caixa
                                    and panoacab_nivel99   = :new.coditem_nivel99
                                    and panoacab_grupo     = :new.coditem_grupo
                                    and panoacab_subgrupo  = :new.coditem_subgrupo
                                    and panoacab_item      = :new.coditem_item
                                    and codigo_deposito    = :new.codigo_deposito;
                                end if;
                           end if;

                        else --Devolu??o com rolos j? existentes

                           select nvl(sum(pcpt_020.qtde_quilos_acab), 0)
                           into   v_qtde_movimentada_rolos
                           from pcpt_020
                           where pcpt_020.cod_empresa_nota = v_local_entrega
                             and pcpt_020.nota_fiscal      = :new.num_nota_orig
                             and pcpt_020.serie_fiscal_sai = :new.serie_nota_orig
                             and pcpt_020.seq_nota_fiscal  = :new.seq_nota_orig
                             and pcpt_020.rolo_estoque     = 2 -- Rolo faturado
                             and not exists (select 1 from rcnb_060
                                             where rcnb_060.tipo_registro       = 41
                                               and rcnb_060.item_estrutura      = pcpt_020.codigo_rolo
                                               and rcnb_060.campo_numerico      = pcpt_020.cod_empresa_nota
                                               and rcnb_060.nivel_estrutura     = pcpt_020.nota_fiscal
                                               and rcnb_060.nivel_estrutura_str = pcpt_020.serie_fiscal_sai
                                               and rcnb_060.grupo_estrutura     = pcpt_020.seq_nota_fiscal
                                               and rcnb_060.tipo_registro_rel   = 0); --Rolo est? marcado para n?o devolver (obrf_f041)

                           if v_qtde_movimentada_rolos <> :new.quantidade
                           then
                              raise_application_error(-20000,'ATEN??O! Quantidade acumulada dos rolos n?o ? igual a quantidade informada no item da nota fiscal.');
                           end if;
                           update pcpt_020
                           set pcpt_020.pedido_venda         = 0,
                               pcpt_020.seq_item_pedido      = 0,
                               pcpt_020.nr_solic_volume      = 0,
                               pcpt_020.cod_empresa_nota     = 0,
                               pcpt_020.nota_fiscal          = 0,
                               pcpt_020.serie_fiscal_sai     = ' ',
                               pcpt_020.seq_nota_fiscal      = 0,
                               pcpt_020.data_entrada         = v_data_transacao,
                               pcpt_020.rolo_estoque         = 1,
                               pcpt_020.codigo_deposito      = :new.codigo_deposito,
                               pcpt_020.lote_acomp           = :new.lote_entrega,
                               pcpt_020.transacao_ent        = :new.codigo_transacao,
                               pcpt_020.cliente_cgc9         = :new.capa_ent_forcli9,
                               pcpt_020.cliente_cgc4         = :new.capa_ent_forcli4,
                               pcpt_020.cliente_cgc2         = :new.capa_ent_forcli2,
                               pcpt_020.nota_fiscal_ent      = :new.capa_ent_nrdoc,
                               pcpt_020.seri_fiscal_ent      = :new.capa_ent_serie,
                               pcpt_020.sequ_fiscal_ent      = :new.sequencia,
                               pcpt_020.data_cardex          = v_data_transacao,
                               pcpt_020.transacao_cardex     = :new.codigo_transacao,
                               pcpt_020.usuario_cardex       = :new.usuario_cardex,
                               pcpt_020.nome_prog_020        = :new.nome_programa,
                               pcpt_020.tabela_origem_cardex = 'OBRF_015',
                               pcpt_020.endereco_rolo        = DECODE(trim(v_endereco_rolo), null, pcpt_020.endereco_rolo, v_endereco_rolo)
                           where pcpt_020.cod_empresa_nota = v_local_entrega
                             and pcpt_020.nota_fiscal      = :new.num_nota_orig
                             and pcpt_020.serie_fiscal_sai = :new.serie_nota_orig
                             and pcpt_020.seq_nota_fiscal  = :new.seq_nota_orig
                             and pcpt_020.rolo_estoque     = 2 -- Rolo faturado
                             and not exists (select 1 from rcnb_060
                                             where rcnb_060.tipo_registro       = 41
                                               and rcnb_060.item_estrutura      = pcpt_020.codigo_rolo
                                               and rcnb_060.campo_numerico      = pcpt_020.cod_empresa_nota
                                               and rcnb_060.nivel_estrutura     = pcpt_020.nota_fiscal
                                               and rcnb_060.nivel_estrutura_str = pcpt_020.serie_fiscal_sai
                                               and rcnb_060.grupo_estrutura     = pcpt_020.seq_nota_fiscal
                                               and rcnb_060.tipo_registro_rel   = 0); --Rolo est? marcado para n?o devolver (obrf_f041)

                        end if;

                     end if; -- if :new.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)

                     if :new.coditem_nivel99 = '7'
                     and v_tipo_volume       = 7
                     and :new.num_nota_orig  > 0
                     and :new.seq_nota_orig  > 0
                     and v_tipo_transacao    = 'D' -- Devolucao
                     and :new.nome_programa <> 'inte_f260' -- Integra??o de notas entre bases.
                     then
                        if v_confirma_coleta = 1
                        then
                            v_sit_caixa := 0;
                        else
                            v_sit_caixa := 1;
                        end if;

                        -- Atualizando as informacoes da caixa
                        update estq_060
                        set numero_pedido        = 0,
                            sequencia_pedido     = 0,
                            nr_solicitacao       = 0,
                            endereco_caixa       = ' ',
                            nota_fiscal          = 0,
                            serie_nota           = ' ',
                            seq_nota_fiscal      = 0,
                            data_transacao       = v_data_transacao,
                            status_caixa         = v_sit_caixa,
                            codigo_deposito      = :new.codigo_deposito,
                            transacao_ent        = :new.codigo_transacao,
                            cnpj_fornecedor9     = :new.capa_ent_forcli9,
                            cnpj_fornecedor4     = :new.capa_ent_forcli4,
                            cnpj_fornecedor2     = :new.capa_ent_forcli2,
                            nota_fisc_ent        = :new.capa_ent_nrdoc,
                            seri_fisc_ent        = :new.capa_ent_serie,
                            sequ_fisc_ent        = :new.sequencia,
                            data_cardex          = v_data_transacao,
                            transacao_cardex     = :new.codigo_transacao,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog            = :new.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                        where nota_fiscal     = :new.num_nota_orig
                          and serie_nota      = :new.serie_nota_orig
                          and seq_nota_fiscal = :new.seq_nota_orig
                          and status_caixa    = 4  --- Caixa Faturada
                          and not exists (select 1 from rcnb_060
                                          where rcnb_060.tipo_registro       = 41
                                            and rcnb_060.item_estrutura      = estq_060.numero_caixa
                                            and rcnb_060.nivel_estrutura     = estq_060.nota_fiscal
                                            and rcnb_060.nivel_estrutura_str = estq_060.serie_nota
                                            and rcnb_060.grupo_estrutura     = estq_060.seq_nota_fiscal
                                            and rcnb_060.tipo_registro_rel   = 0); --caixa est? marcado para n?o devolver (obrf_f041)

                     end if;          -- if :new.coditem_nivel99 = '7' and v_tipo_volume = 7

                     if :new.coditem_nivel99 = '9'
                     and v_tipo_volume       = 9
                     and :new.num_nota_orig  > 0
                     and :new.seq_nota_orig  > 0
                     and v_tipo_transacao    = 'D' -- Devolucao
                     then
                        update pcpf_060
                        set emp_nota_sai         = 0,
                            num_nota_sai         = 0,
                            ser_nota_sai         = ' ',
                            seq_nota_sai         = 0,
                            data_movimentacao    = v_data_transacao,
                            nf_entrada           = :new.capa_ent_nrdoc,
                            serie_nf_entrada     = :new.capa_ent_serie,
                            cgc9_forne           = :new.capa_ent_forcli9,
                            cgc4_forne           = :new.capa_ent_forcli4,
                            cgc2_forne           = :new.capa_ent_forcli2,
                            status_fardo         = 1,
                            deposito             = :new.codigo_deposito,
                            transacao            = :new.codigo_transacao,
                            data_cardex          = v_data_transacao,
                            transacao_cardex     = :new.codigo_transacao,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog            = :new.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                        where num_nota_sai = :new.num_nota_orig
                          and ser_nota_sai = :new.serie_nota_orig
                          and seq_nota_sai = :new.seq_nota_orig
                          and status_fardo = 2 --- Fardo faturado
                          and not exists (select 1 from rcnb_060
                                          where rcnb_060.tipo_registro       = 41
                                            and rcnb_060.item_estrutura      = pcpf_060.codigo_fardo
                                            and rcnb_060.nivel_estrutura     = pcpf_060.num_nota_sai
                                            and rcnb_060.nivel_estrutura_str = pcpf_060.ser_nota_sai
                                            and rcnb_060.grupo_estrutura     = pcpf_060.seq_nota_sai
                                            and rcnb_060.tipo_registro_rel   = 0); --fardo est? marcado para n?o devolver (obrf_f041)

                     end if;          -- if :new.coditem_nivel99 = '9'
                  end if;             -- if v_tipo_transacao not in ('D','R')
               else                   -- if :new.nome_programa <> 'OBRF_F200'
                  -- Le parametro opcao_conf_volume na fatu_504
                  select opcao_conf_volume
                  into p_opcao_conf_volume
                  from fatu_504
                  where fatu_504.codigo_empresa = v_local_entrega;

                  -- Valida??o do parametro opcao_conf_volume
                  if p_opcao_conf_volume = 1
                  then
                     BEGIN
                        select nota_entrega
                        into p_nota_entrega
                        from fatu_050
                        where fatu_050.num_nota_fiscal = :new.num_nota_orig
                          and fatu_050.serie_nota_fisc = :new.serie_nota_orig
                          and fatu_050.codigo_empresa  = v_cod_empresa_nfs;
                     EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           p_nota_entrega := 0;
                     END;
                  end if;

                  if :new.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4) and ((p_opcao_conf_volume = 1 and p_nota_entrega = 0) or p_opcao_conf_volume = 0)
                  then
                     begin
                        -- Busca o codigo da empresa da nota fiscal de saida
                        -- pelo CNPJ do fornecedor da nota de entrada
                        select min(codigo_empresa)
                        into v_cod_empresa_nfs
                        from fatu_500
                        where cgc_9 = :new.capa_ent_forcli9
                          and cgc_4 = :new.capa_ent_forcli4
                          and cgc_2 = :new.capa_ent_forcli2;


                        -- Busca o pedido de venda da nota fiscal de saida
                        -- para achar o pedido original
                        -- Neste caso estamos levando em consideracao que a
                        -- sequencia do pedido destino e igual a sequencia do pedido original
                        select pedido_venda,      seq_item_pedido
                        into v_pedido_venda_nfs,  v_seq_item_pedido_nfs
                        from fatu_060
                        where ch_it_nf_cd_empr  = v_cod_empresa_nfs
                          and ch_it_nf_num_nfis = :new.capa_ent_nrdoc
                          and ch_it_nf_ser_nfis = :new.capa_ent_serie
                          and seq_item_nfisc    = :new.sequencia;

                        v_pedido_venda_orig := 0;

                        if v_pedido_venda_nfs > 0
                        then
                           -- Buscando o pedido original do pedido faturado pelo
                           -- fornecedor
                           select pedido_original
                           into v_pedido_venda_orig
                           from pedi_102
                           where pedido_destino = v_pedido_venda_nfs;
                        else
                           v_pedido_venda_orig := 0;
                        end if;

                     exception
                        when others then
                           v_pedido_venda_orig := 0;
                     end; -- do begin

                     if v_confirma_coleta = 1
                     then
                         v_sit_rolo := 4;
                     else
                         v_sit_rolo := 1;
                     end if;


                     -- Quando existir pedido original, os rolos sao automaticamente
                     -- atualizados com o numero do pedido e ja sao associados ao
                     -- mesmo.
                     if v_pedido_venda_orig > 0
                     then
                        if v_confirma_coleta <> 1
                        then
                           v_sit_rolo := 3;
                        end if;
                        v_pedido_venda_ent_conf := v_pedido_venda_orig;
                        v_seq_item_pedido_conf  := v_seq_item_pedido_nfs;
                     else
                        v_pedido_venda_ent_conf := 0;
                        v_seq_item_pedido_conf  := 0;
                     end if;

                     if v_pedido_venda_ent_conf = 0 and
                     v_respeita_confirmacao = 1       -- sempre que o tipo for 1 deve confirmar
                     then
                        v_sit_rolo := 1;
                     end if;


                     if  v_respeita_confirmacao = 2 -- sempre que o tipo for 2 deve ficar em transito
                     then
                        v_sit_rolo := 4;
                     end if;

                     -- v_limpa_pedido_volume
                     -- =====================
                     -- Esse campo serve para indicar se quando o volume for incluido em um deposito via
                     -- o processo de confirmacao de notas de entrada (obrf_f200), deve ou nao limpar o
                     -- numero e sequencia do pedido de venda originais do volume.

                     -- 1-Deve limpar o numero/sequencia do pedido
                     -- 2-Nao deve limpar, deixando o conteudo como esta.

                     --(comeco pcpt_020)

                     begin
                        select limpa_pedido_volume
                        into v_limpa_pedido_volume
                        from basi_205
                        where codigo_deposito  = :new.codigo_deposito;
                        exception when no_data_found
                        then v_limpa_pedido_volume := 1;
                     end;

                     -- Consist?ncias para controlar a atualiza??o do estoque via pcpt_020
                     -- Se o produto for em elabora??o
                     -- Se a empresa da nota ? a mesma na qual est? movimentando
                     -- Se existe dep?sito de mercadoria em tr?nsito
                     -- N?o deve atualizar estoque (estq_300) por esta trigger
                     -- Mas sim pela trigger da pcpt_020
                     if produto_elab  = 1 and
                        outra_empresa = true and
                        v_deposito_transicao > 0
                     then
                        update pcpt_020
                        set codigo_deposito      = :new.codigo_deposito,
                            lote_acomp           = :new.lote_entrega,
                            nr_solic_volume      = 0,
                            nr_volume            = 0,
                            transacao_ent        = :new.codigo_transacao,
                            data_entrada         = v_data_transacao,
                            cliente_cgc9         = :new.capa_ent_forcli9,
                            cliente_cgc4         = :new.capa_ent_forcli4,
                            cliente_cgc2         = :new.capa_ent_forcli2,
                            nota_fiscal_ent      = :new.capa_ent_nrdoc,
                            seri_fiscal_ent      = :new.capa_ent_serie,
                            sequ_fiscal_ent      = :new.sequencia,
                            data_cardex          = v_data_transacao,
                            transacao_cardex     = :new.codigo_transacao,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog_020        = :new.nome_programa,
                            tabela_origem_cardex = 'PCPT_020'
                        where cod_empresa_nota   = v_cod_empresa_nfs
                          and nota_fiscal        = :new.capa_ent_nrdoc
                          and serie_fiscal_sai   = :new.capa_ent_serie
                          and seq_nota_fiscal    = :new.sequencia
                          and panoacab_nivel99   = :new.coditem_nivel99
                          and panoacab_grupo     = :new.coditem_grupo
                          and panoacab_subgrupo  = :new.coditem_subgrupo
                          and panoacab_item      = :new.coditem_item;
                     else
                         if v_limpa_pedido_volume = 1
                         then
                            update pcpt_020
                            set codigo_deposito      = :new.codigo_deposito,
                                lote_acomp           = :new.lote_entrega,
                                pedido_venda         = v_pedido_venda_ent_conf,
                                seq_item_pedido      = v_seq_item_pedido_conf,
                                nr_solic_volume      = 0,
                                nr_volume            = 0,
                                rolo_estoque         = decode(:new.limpa_pedido, 'S', decode(pedido_venda, 0, v_sit_rolo, rolo_estoque), v_sit_rolo),
                                transacao_ent        = :new.codigo_transacao,
                                data_entrada         = v_data_transacao,
                                cliente_cgc9         = :new.capa_ent_forcli9,
                                cliente_cgc4         = :new.capa_ent_forcli4,
                                cliente_cgc2         = :new.capa_ent_forcli2,
                                nota_fiscal_ent      = :new.capa_ent_nrdoc,
                                seri_fiscal_ent      = :new.capa_ent_serie,
                                sequ_fiscal_ent      = :new.sequencia,
                                data_cardex          = v_data_transacao,
                                transacao_cardex     = :new.codigo_transacao,
                                usuario_cardex       = :new.usuario_cardex,
                                nome_prog_020        = :new.nome_programa,
                                tabela_origem_cardex = 'OBRF_015',
                                endereco_rolo        = DECODE(trim(v_endereco_rolo), null, pcpt_020.endereco_rolo, v_endereco_rolo)
                            where cod_empresa_nota   = v_cod_empresa_nfs
                              and nota_fiscal        = :new.capa_ent_nrdoc
                              and serie_fiscal_sai   = :new.capa_ent_serie
                              and seq_nota_fiscal    = :new.sequencia
                              and panoacab_nivel99   = :new.coditem_nivel99
                              and panoacab_grupo     = :new.coditem_grupo
                              and panoacab_subgrupo  = :new.coditem_subgrupo
                              and panoacab_item      = :new.coditem_item
                              and rolo_estoque       = 2;   -- Rolo faturado
                         else
                            update pcpt_020
                            set codigo_deposito      = :new.codigo_deposito,
                                lote_acomp           = :new.lote_entrega,
                                nr_solic_volume      = 0,
                                nr_volume            = 0,
                                rolo_estoque         = decode(:new.limpa_pedido,'S', decode(pedido_venda,0,v_sit_rolo,3),v_sit_rolo),
                                transacao_ent        = :new.codigo_transacao,
                                data_entrada         = v_data_transacao,
                                cliente_cgc9         = :new.capa_ent_forcli9,
                                cliente_cgc4         = :new.capa_ent_forcli4,
                                cliente_cgc2         = :new.capa_ent_forcli2,
                                nota_fiscal_ent      = :new.capa_ent_nrdoc,
                                seri_fiscal_ent      = :new.capa_ent_serie,
                                sequ_fiscal_ent      = :new.sequencia,
                                data_cardex          = v_data_transacao,
                                transacao_cardex     = :new.codigo_transacao,
                                usuario_cardex       = :new.usuario_cardex,
                                nome_prog_020        = :new.nome_programa,
                                tabela_origem_cardex = 'OBRF_015',
                                endereco_rolo        = DECODE(trim(v_endereco_rolo), null, pcpt_020.endereco_rolo, v_endereco_rolo)
                            where cod_empresa_nota   = v_cod_empresa_nfs
                              and nota_fiscal        = :new.capa_ent_nrdoc
                              and serie_fiscal_sai   = :new.capa_ent_serie
                              and seq_nota_fiscal    = :new.sequencia
                              and panoacab_nivel99   = :new.coditem_nivel99
                              and panoacab_grupo     = :new.coditem_grupo
                              and panoacab_subgrupo  = :new.coditem_subgrupo
                              and panoacab_item      = :new.coditem_item
                              and rolo_estoque       = 2;   -- Rolo faturado
                         end if;
                         --(fim pcpt_020)
                     end if;

                  end if; -- if :new.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)

                  if :new.coditem_nivel99 = '1' and v_tipo_volume = 1
                  then
                     begin
                        -- Busca o codigo da empresa da nota fiscal de saida
                        -- pelo CNPJ do fornecedor da nota de entrada
                        select min(codigo_empresa)
                        into v_cod_empresa_nfs
                        from fatu_500
                        where cgc_9 = :new.capa_ent_forcli9
                          and cgc_4 = :new.capa_ent_forcli4
                          and cgc_2 = :new.capa_ent_forcli2;

                        -- Busca o pedido de venda da nota fiscal de saida
                        -- para achar o pedido original
                        -- Neste caso estamos levando em consideracao que a
                        -- sequencia do pedido destino e igual a sequencia do pedido original
                        select pedido_venda,      seq_item_pedido
                        into v_pedido_venda_nfs,  v_seq_item_pedido_nfs
                        from fatu_060
                        where ch_it_nf_cd_empr  = v_cod_empresa_nfs
                          and ch_it_nf_num_nfis = :new.capa_ent_nrdoc
                          and ch_it_nf_ser_nfis = :new.capa_ent_serie
                          and seq_item_nfisc    = :new.sequencia;

                        v_pedido_venda_orig := 0;

                        if v_pedido_venda_nfs > 0
                        then
                           -- Buscando o pedido original do pedido faturado pelo
                           -- fornecedor
                           select pedido_original
                           into v_pedido_venda_orig
                           from pedi_102
                           where pedido_destino = v_pedido_venda_nfs;
                        else
                           v_pedido_venda_orig := 0;
                        end if;

                     exception
                        when others then
                           v_pedido_venda_orig := 0;
                     end; -- do begin

                     -- Quando existir pedido original, as tags sao automaticamente
                     -- atualizados com o numero do pedido e ja sao associados ao
                     -- mesmo.
                     if v_pedido_venda_orig > 0
                     then
                        if v_confirma_coleta = 1
                        then
                            v_sit_rolo := 1;
                            v_pedido_venda_ent_conf := 0;
                            v_seq_item_pedido_conf  := 0;
                        else
                            v_sit_rolo := 3;
                            v_pedido_venda_ent_conf := v_pedido_venda_orig;
                            v_seq_item_pedido_conf  := v_seq_item_pedido_nfs;
                        end if;
                     else
                        v_sit_rolo := 1;
                        v_pedido_venda_ent_conf := 0;
                        v_seq_item_pedido_conf  := 0;
                     end if;

                     update pcpc_330
                      set deposito             = :new.codigo_deposito,
                          flag_controle        = 0,
                          pcpc_330.lote        = :new.lote_entrega,
                          pedido_venda         = v_pedido_venda_ent_conf,
                          seq_item_pedido      = v_seq_item_pedido_conf,
                          estoque_tag          = v_sit_rolo,
                          transacao_ent        = :new.codigo_transacao,
                          data_entrada         = v_data_transacao,
                          forn9_entr           = :new.capa_ent_forcli9,
                          forn4_entr           = :new.capa_ent_forcli4,
                          forn2_entr           = :new.capa_ent_forcli2,
                          nota_entr            = :new.capa_ent_nrdoc,
                          serie_entr           = :new.capa_ent_serie,
                          seq_entr             = :new.sequencia,
                          data_cardex          = v_data_transacao,
                          transacao_cardex     = :new.codigo_transacao,
                          usuario_cardex       = :new.usuario_cardex,
                          nome_prog_050        = :new.nome_programa,
                          tabela_origem_cardex = 'OBRF_015'
                      where emp_saida          = v_cod_empresa_nfs
                        and nota_inclusao      = :new.capa_ent_nrdoc
                        and serie_nota         = :new.capa_ent_serie
                        and seq_saida          = :new.sequencia
                        and estoque_tag        = 4;   -- Tag faturado
                  end if; -- if :new.coditem_nivel99 = '1' and v_tipo_volume = 1


                  if :new.coditem_nivel99 = '7' and v_tipo_volume = 7 and ((p_opcao_conf_volume = 1 and p_nota_entrega = 0) or p_opcao_conf_volume = 0)
                  then
                     begin
                        -- Busca o codigo da empresa da nota fiscal de saida
                        -- pelo CNPJ do fornecedor da nota de entrada
                        select min(codigo_empresa)
                        into v_cod_empresa_nfs
                        from fatu_500
                        where cgc_9 = :new.capa_ent_forcli9
                          and cgc_4 = :new.capa_ent_forcli4
                          and cgc_2 = :new.capa_ent_forcli2;

                        -- Busca o pedido de venda da nota fiscal de saida
                        -- para achar o pedido original
                        -- Neste caso estamos levando em consideracao que a
                        -- sequencia do pedido destino e igual a sequencia do pedido original
                        select pedido_venda,      seq_item_pedido
                        into v_pedido_venda_nfs,  v_seq_item_pedido_nfs
                        from fatu_060
                        where ch_it_nf_cd_empr  = v_cod_empresa_nfs
                          and ch_it_nf_num_nfis = :new.capa_ent_nrdoc
                          and ch_it_nf_ser_nfis = :new.capa_ent_serie
                          and seq_item_nfisc    = :new.sequencia;

                        v_pedido_venda_orig := 0;

                        if v_pedido_venda_nfs > 0
                        then
                           -- Buscando o pedido original do pedido faturado pelo
                           -- fornecedor
                           select pedido_original
                           into v_pedido_venda_orig
                           from pedi_102
                           where pedido_destino = v_pedido_venda_nfs;
                        else
                           v_pedido_venda_orig := 0;
                        end if;

                     exception
                        when others then
                           v_pedido_venda_orig := 0;
                     end; -- do begin

                     if v_confirma_coleta = 1
                     then
                         v_sit_caixa := 0;
                     else
                         v_sit_caixa := 1;
                     end if;

                     -- Quando existir pedido original, as caixas sao automaticamente
                     -- atualizados com o numero do pedido e ja sao associados ao
                     -- mesmo.
                     if v_pedido_venda_orig > 0
                     then
                        if v_confirma_coleta <> 1
                        then
                           v_sit_caixa := 3;
                        end if;

                        v_pedido_venda_ent_conf := v_pedido_venda_orig;
                        v_seq_item_pedido_conf  := v_seq_item_pedido_nfs;
                     else
                        v_pedido_venda_ent_conf := 0;
                        v_seq_item_pedido_conf  := 0;
                     end if;

                     if v_pedido_venda_ent_conf = 0 and
                     v_respeita_confirmacao = 1       -- sempre que o tipo for 1 deve confirmar
                     then
                        v_sit_caixa := 1;
                     end if;

                  if  v_respeita_confirmacao = 2 -- sempre que o tipo for 2 deve ficar em transito
                     then
                        v_sit_caixa := 0;
                     end if;


   --                  v_limpa_pedido_volume
   --                  =====================
   --                  Esse campo serve para indicar se quando o volume for incluido em um deposito via
   --                  o processo de confirmacao de notas de entrada (obrf_f200), deve ou nao limpar o
   --                  numero e sequencia do pedido de venda originais do volume.

   --                  1-Deve limpar o numero/sequencia do pedido
   --                  2-Nao deve limpar, deixando o conteudo como esta.

   --(comeco basi_205)

                     begin
                        select limpa_pedido_volume
                        into v_limpa_pedido_volume
                        from basi_205
                        where codigo_deposito  = :new.codigo_deposito;
                        exception when no_data_found
                        then v_limpa_pedido_volume := 1;
                     end;

                     if v_limpa_pedido_volume = 1
                     then
                        update estq_060
                        set numero_pedido        = v_pedido_venda_ent_conf,
                            sequencia_pedido     = v_seq_item_pedido_conf,
                            nr_solicitacao       = 0,
                            endereco_caixa       = ' ',
                            nota_fiscal          = 0,
                            serie_nota           = ' ',
                            seq_nota_fiscal      = 0,
                            data_transacao       = v_data_transacao,
                            transacao_ent        = :new.codigo_transacao,
                            status_caixa         = v_sit_caixa,
                            codigo_deposito      = :new.codigo_deposito,
                            lote                 = :new.lote_entrega,
                            data_entrada         = v_data_transacao,
                            cnpj_fornecedor9     = :new.capa_ent_forcli9,
                            cnpj_fornecedor4     = :new.capa_ent_forcli4,
                            cnpj_fornecedor2     = :new.capa_ent_forcli2,
                            nota_fisc_ent        = :new.capa_ent_nrdoc,
                            seri_fisc_ent        = :new.capa_ent_serie,
                            sequ_fisc_ent        = :new.sequencia,
                            data_cardex          = v_data_transacao,
                            transacao_cardex     = :new.codigo_transacao,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog            = :new.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                         where cod_empresa_sai    = v_cod_empresa_nfs
                           and nota_fiscal        = :new.capa_ent_nrdoc
                           and serie_nota         = :new.capa_ent_serie
                           and seq_nota_fiscal    = :new.sequencia
                           and prodcai_nivel99    = :new.coditem_nivel99
                           and prodcai_grupo      = :new.coditem_grupo
                           and prodcai_subgrupo   = :new.coditem_subgrupo
                           and prodcai_item       = :new.coditem_item
                           and status_caixa       = 4;  --- Caixa Faturada
                     else
                        update estq_060
                        set nr_solicitacao       = 0,
                            endereco_caixa       = ' ',
                            nota_fiscal          = 0,
                            serie_nota           = ' ',
                            seq_nota_fiscal      = 0,
                            data_transacao       = v_data_transacao,
                            transacao_ent        = :new.codigo_transacao,
                            status_caixa         = v_sit_caixa,
                            codigo_deposito      = :new.codigo_deposito,
                            lote                 = :new.lote_entrega,
                            data_entrada         = v_data_transacao,
                            cnpj_fornecedor9     = :new.capa_ent_forcli9,
                            cnpj_fornecedor4     = :new.capa_ent_forcli4,
                            cnpj_fornecedor2     = :new.capa_ent_forcli2,
                            nota_fisc_ent        = :new.capa_ent_nrdoc,
                            seri_fisc_ent        = :new.capa_ent_serie,
                            sequ_fisc_ent        = :new.sequencia,
                            data_cardex          = v_data_transacao,
                            transacao_cardex     = :new.codigo_transacao,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog            = :new.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                         where cod_empresa_sai    = v_cod_empresa_nfs
                           and nota_fiscal        = :new.capa_ent_nrdoc
                           and serie_nota         = :new.capa_ent_serie
                           and seq_nota_fiscal    = :new.sequencia
                           and prodcai_nivel99    = :new.coditem_nivel99
                           and prodcai_grupo      = :new.coditem_grupo
                           and prodcai_subgrupo   = :new.coditem_subgrupo
                           and prodcai_item       = :new.coditem_item
                           and status_caixa       = 4;  --- Caixa Faturada
                     end if;
      --(fim basi_205)

                  end if;          -- if :new.coditem_nivel99 = '7' and v_tipo_volume = 7

                  if :new.coditem_nivel99 = '9' and v_tipo_volume = 9
                  then

                     -- Busca o codigo da empresa da nota fiscal de saida
                     -- pelo CNPJ do fornecedor da nota de entrada
                     select min(codigo_empresa)
                     into v_cod_empresa_nfs
                     from fatu_500
                     where cgc_9 = :new.capa_ent_forcli9
                       and cgc_4 = :new.capa_ent_forcli4
                       and cgc_2 = :new.capa_ent_forcli2;

                     if v_confirma_coleta = 1
                     then
                         v_sit_fardo := 4;
                     else
                         v_sit_fardo := 1;
                     end if;

                     update pcpf_060
                     set endereco_fardo       = ' ',
                         emp_nota_sai         = 0,
                         num_nota_sai         = 0,
                         ser_nota_sai         = ' ',
                         seq_nota_sai         = 0,
                         data_movimentacao    = v_data_transacao,
                         transacao            = :new.codigo_transacao,
                         status_fardo         = v_sit_fardo,
                         deposito             = :new.codigo_deposito,
                         lote_mat_prima       = :new.lote_entrega,
                         data_cardex          = v_data_transacao,
                         transacao_cardex     = :new.codigo_transacao,
                         usuario_cardex       = :new.usuario_cardex,
                         nome_prog            = :new.nome_programa,
                         tabela_origem_cardex = 'OBRF_015'
                     where emp_nota_sai       = v_cod_empresa_nfs
                       and num_nota_sai       = :new.capa_ent_nrdoc
                       and ser_nota_sai       = :new.capa_ent_serie
                       and seq_nota_sai       = :new.sequencia
                       and status_fardo       = 2; -- Fardo faturado

                  end if; -- if :new.coditem_nivel99 = '9' and v_tipo_volume = 9
               end if;             -- if new:nome_programa <> 'OBRF_F200' -- Confirmacao de nota de entrada
            end if;                -- if v_tipo_volume <> 0
         end if;                   -- if :new.codigo_deposito > 0

         /*IN?CIO MOVIMENTA??O DE ESTOQUE NO DEP?SITO PR?PRIO EM PODER DE TERCEIROS
           SS: 60598/001 - THIAGO.FELIPE*/
         begin
            select estq_005.icdtransdepterceiros,   estq_005.tipo_transacao,
                   estq_005.atualiza_estq_terceiro
            into   v_trans_terceiro,                v_tipo_transacao_005,
                   v_atualiza_estq_terceiro
            from estq_005
            where estq_005.codigo_transacao = :new.codigo_transacao
            and ROWNUM = 1;
         exception
             when no_data_found then
                v_trans_terceiro       := 0;
                v_tipo_transacao_005   := 'N';
                v_atualiza_estq_terceiro := 0;
         end;

         /*Verifica deposito relacionado ao terceiro*/
         begin
           select basi_572.cod_dep
           into v_dep_terceiro_572
           from basi_572
           where basi_572.cod_empresa = v_cod_empresa_nfs
             and basi_572.terc_cnpj9  = v_responsavel9
             and basi_572.terc_cnpj4  = v_responsavel4
             and basi_572.terc_cnpj2  = v_responsavel2
             and ROWNUM = 1;
           exception
           when no_data_found then
                v_dep_terceiro_572     := 0;
         end;

         if ( v_dep_terceiro_572 = 0 and  (v_tipo_transacao_005 = 'R' or v_tipo_transacao_005 = 'D' or
              v_tipo_transacao_005 = 'I' or v_tipo_transacao_005 = 'N' or v_atualiza_estq_terceiro = 1) )
            or
            ( v_dep_terceiro_572 <> 0 and  (v_tipo_transacao_005 = 'R' or v_tipo_transacao_005 = 'E' or
              v_tipo_transacao_005 = 'I' or v_atualiza_estq_terceiro = 1) )
         then
            if v_dep_terceiro_572 = 0
            then

               if :new.codigo_deposito = 0 and (v_trans_terceiro > 0 and v_dep_saida_terc >0)
               then
                  v_deposito_terceiro := v_dep_saida_terc;
               elsif :new.codigo_deposito > 0
               then
                  begin
                     select basi_205.icddepdestterceiros into v_deposito_terceiro from basi_205
                     where basi_205.codigo_deposito = :new.codigo_deposito;
                  exception
                      when no_data_found then
                         v_deposito_terceiro := 0;
                  end;
               else
                  begin
                     select 1 into v_cont_processo from hdoc_001
                     where hdoc_001.tipo = 350
                       and hdoc_001.codigo > 0
                       and hdoc_001.codigo2 > 0;
                  exception
                      when no_data_found then
                         v_cont_processo := 0;
                  end;

                  if v_cont_processo > 0
                  then
                     begin
                        select obrf_510.processo_nota, obrf_510.cod_emp_benef,
                               obrf_510.cod_emp_origem
                        into   v_processo_nota,        v_cod_emp_benef,
                               v_cod_emp_origem
                        from obrf_510, (select obrf_510.cod_emp_benef, obrf_510.cod_emp_origem, obrf_510.seq_exec_rotina, max(obrf_510.seq_nota) seq_nota
                                        from obrf_510
                                        where obrf_510.numero_nota       = :new.capa_ent_nrdoc
                                          and obrf_510.serie_nota        = :new.capa_ent_serie
                                          and obrf_510.cgc9_cli_for_nota = :new.capa_ent_forcli9
                                          and obrf_510.cgc4_cli_for_nota = :new.capa_ent_forcli4
                                          and obrf_510.cgc2_cli_for_nota = :new.capa_ent_forcli2
                                        group by obrf_510.cod_emp_benef, obrf_510.cod_emp_origem, obrf_510.seq_exec_rotina) aux_obrf_510
                         where obrf_510.cod_emp_benef   = aux_obrf_510.cod_emp_benef
                           and obrf_510.cod_emp_origem  = aux_obrf_510.cod_emp_origem
                           and obrf_510.seq_exec_rotina = aux_obrf_510.seq_exec_rotina
                           and obrf_510.seq_nota        = aux_obrf_510.seq_nota;
                     exception
                         when no_data_found then
                            v_processo_nota  := 0;
                            v_cod_emp_benef  := 0;
                            v_cod_emp_origem := 0;
                     end;

                     if v_processo_nota > 0 and v_cod_emp_benef > 0 and v_cod_emp_origem > 0
                     then
                        if v_processo_nota = 4
                        then
                           v_tipo_processo := 354;
                        end if;

                        if v_processo_nota = 10
                        then
                           v_tipo_processo := 360;
                        end if;

                        if v_processo_nota = 16
                        then
                           v_tipo_processo := 366;
                        end if;

                        begin
                           select hdoc_001.campo_numerico25,  hdoc_001.campo_numerico27
                           into   v_deposito_terceiro,        v_centro_custo_terceiro
                           from hdoc_001
                           where hdoc_001.tipo    = v_tipo_processo
                             and hdoc_001.codigo  = v_cod_emp_benef
                             and hdoc_001.codigo2 = v_cod_emp_origem;
                        exception
                            when no_data_found then
                               v_deposito_terceiro := 0;
                               v_centro_custo_terceiro := 0;
                        end;
                     end if; --if v_processo_nota > 0 and v_cod_emp_benef > 0 and v_cod_emp_origem > 0
                  end if; --if v_cont_processo > 0
               end if; --if :new.codigo_deposito > 0
            else
              v_deposito_terceiro := v_dep_terceiro_572;
            end if;

            if v_trans_terceiro > 0 and v_deposito_terceiro > 0
            then

               v_centro_custo_terceiro := :new.centro_custo;

               begin
                  select estq_005.atualiza_estoque,   estq_005.istajustefinanceiro,
                         estq_005.entrada_saida
                  into   v_atualiza_estoque_terceiro, v_istajustefinanceiro2,
                         v_ent_sai_terc
                  from estq_005
                  where estq_005.codigo_transacao = v_trans_terceiro;
               exception
                   when no_data_found then
                      v_atualiza_estoque_terceiro := 0;
                      v_ent_sai_terc              := '';
                      v_istajustefinanceiro2      := 0;
               end;

               if v_atualiza_estoque_terceiro = 1
               then
                  -- Buscando a data da entrada e empresa da nota fiscal
                  begin
                     select obrf_010.data_transacao,  obrf_010.local_entrega,
                            obrf_010.moeda_nota,      obrf_010.especie_docto
                     into   v_data_transacao,         v_local_entrega,
                            v_moeda,                  v_especie_docto
                     from obrf_010
                     where obrf_010.documento     = :new.capa_ent_nrdoc
                       and obrf_010.serie         = :new.capa_ent_serie
                       and obrf_010.cgc_cli_for_9 = :new.capa_ent_forcli9
                       and obrf_010.cgc_cli_for_4 = :new.capa_ent_forcli4
                       and obrf_010.cgc_cli_for_2 = :new.capa_ent_forcli2;
                  exception
                      when no_data_found then
                         v_data_transacao := trunc(sysdate);
                         v_local_entrega := 0;
                         v_moeda := 0;
                         v_especie_docto := ' ';
                  end;

                  -- Verificando o tipo de volume e se o deposito precisa confirmar coleta
                  begin
                     select basi_205.tipo_volume, basi_205.confirma_coleta
                     into v_tipo_volume, v_confirma_coleta
                     from basi_205
                     where basi_205.codigo_deposito = :new.codigo_deposito;
                  exception
                     when no_data_found then
                        null;
                  end;

                  /*Atualiza o valor para a moeda corrente*/

                  if v_moeda <> 0
                  then
                     begin
                        select basi_270.valor_moeda into v_indice_moeda
                        from basi_270
                        where basi_270.data_moeda   = v_data_transacao
                          and basi_270.codigo_moeda = v_moeda;
                     exception
                         when others then
                            v_indice_moeda := 1;
                     end;
                  else
                     v_indice_moeda := 1;
                  end if;

                  v_valor_total     := :new.valor_total * v_indice_moeda;
                  v_rateio_despesas := :new.rateio_despesas * v_indice_moeda;

                  -- Calculando o valor contabil unitario para gravacao no Cardex
                  if :new.quantidade > 0.000
                  then
                     v_valor_contabil := (v_valor_total + v_rateio_despesas) / :new.quantidade;
                     v_valor_unitario := :new.preco_custo / :new.quantidade;
                  else
                     v_valor_contabil := v_valor_total + v_rateio_despesas;
                     v_valor_unitario := :new.preco_custo;
                  end if;

                  -- LOGICA PARA PROCURA O PAIS DA EMPRESA DA NOTA
                  begin
                     select fatu_500.codigo_cidade into v_cidadeempresa from fatu_500
                     where fatu_500.codigo_empresa = v_local_entrega;
                     exception
                         when others then
                            v_cidadeempresa := 0;
                  end;

                  begin
                     select basi_160.codigo_pais into v_paisempresa from basi_160
                     where basi_160.cod_cidade = v_cidadeempresa;
                     exception
                        when others then
                           v_paisempresa := 1;
                  end;

                  if v_paisempresa <> 1
                  then
                     if v_especie_docto = 'FAC' -- NOTA FATURA
                     then
                        if not(v_atualiza_estoque = 1 and v_permite_fatura_atu_esq = 'S')
                        then
                           v_quantidade_nf := 0.000;
                        end if;
                     end if;

                     if v_especie_docto = 'GRE' -- NOTA REMISSAO
                     then
                        v_valor_contabil := 0.00;
                        v_preco_custo := 0.00;
                     end if;
                  end if;

                  if v_centro_custo_terceiro <= 0 or v_centro_custo_terceiro is null
                  then
                     v_centro_custo_terceiro := :new.centro_custo;
                  end if;

                  -- Vanderlei
                  --verifica consist?ncias da natureza x transa??o em rela??o a op??o de ajuste financeiro
                  begin
                     select pedi_080.nfe_atu_financeiro
                     into   v_transacao_financeira
                     from pedi_080
                     where pedi_080.natur_operacao = :new.natitem_nat_oper
                       and pedi_080.estado_natoper = :new.natitem_est_oper;
                  exception
                     when others then
                     v_transacao_financeira := 'N';
                  end;

                  if v_transacao_financeira = 'N'
                  then
                     if v_istajustefinanceiro2 = 1
                     then
                        raise_application_error (-20000,'ATEN??O! N?o ? poss?vel utilizar uma transa??o de Ajuste Financeiro se natureza de opera??o n?o permite somente ajuste financeiro.');
                     end if;
                  end if;
                  -- Fim da consist?ncia natureza x transa??o

                  if v_istajustefinanceiro2 = 1
                  then v_quantidade_nf := 0.00;
                  end if;

                  if v_centro_custo_terceiro is null
                  then v_centro_custo_terceiro := :new.centro_custo;
                  end if;

                  inter_pr_insere_estq_300_recur (v_deposito_terceiro,   :new.coditem_nivel99,   :new.coditem_grupo,
                                            :new.coditem_subgrupo, :new.coditem_item,       v_data_transacao,
                                            :new.lote_entrega,     :new.capa_ent_nrdoc,    :new.capa_ent_serie,
                                            :new.capa_ent_forcli9, :new.capa_ent_forcli4,  :new.capa_ent_forcli2,
                                            :new.sequencia,
                                            v_trans_terceiro,      v_ent_sai_terc,         v_centro_custo_terceiro,
                                            v_quantidade_nf,       v_preco_custo,          v_valor_contabil,
                                            :new.usuario_cardex,   'OBRF_015',             :new.nome_programa,
                                            0.00,                   0,                       0,
                                            :new.capa_ent_nrdoc,    0,                       0);
               end if;                -- if v_atualiza_estoque_terceiro = 1
            end if;                   -- if v_trans_terceiro > 0 and v_deposito_terceiro > 0
         end if;                      -- if v_tipo_transacao_005 = 'R' or v_tipo_transacao_005 = 'D'

         -- atualiza o pre?o de custo
         if  :new.coditem_nivel99 <> '0'   and v_calcula_preco = 1
         and v_quantidade_nf       > 0.00  and v_preco_custo   > 0.00
         then

            select basi_030.comprado_fabric
            into   v_comprado_fabric
            from basi_030
            where basi_030.nivel_estrutura = :new.coditem_nivel99
            and   basi_030.referencia      = :new.coditem_grupo;
           
            if v_comprado_fabric = 1  -- comprado
            then

                update basi_010
                set preco_custo = round(v_preco_custo / v_quantidade_nf, 5)
                where nivel_estrutura  = :new.coditem_nivel99
                and   grupo_estrutura  = :new.coditem_grupo
                and   subgru_estrutura = :new.coditem_subgrupo
                and   item_estrutura   = :new.coditem_item;
                
            end if;
         end if;

      end if;                      -- if inserting
----------
      if updating
      then
         BEGIN
         select e.periodo_estoque into v_periodo_estoque from empr_001 e;
         END;

         if trim(:new.unidade_conv) is null
         then
           :new.unidade_conv := :new.unidade_medida;
         end if;

         select fatu_502.atualiza_estoque_f,obrf_010.local_entrega,       obrf_010.especie_docto, fatu_502.permite_fatura_atu_esq
         into v_par_atualiza_estoque_f,     v_local_entrega1,             v_especie_docto,        v_permite_fatura_atu_esq
         from obrf_010, fatu_502
         where obrf_010.local_entrega = fatu_502.codigo_empresa
           and documento     = :new.capa_ent_nrdoc
           and serie         = :new.capa_ent_serie
           and cgc_cli_for_9 = :new.capa_ent_forcli9
           and cgc_cli_for_4 = :new.capa_ent_forcli4
           and cgc_cli_for_2 = :new.capa_ent_forcli2;

         -- le a transacao para verificar se ela atualiza estoque.
         select estq_005.atualiza_estoque, estq_005.tipo_transacao,
                estq_005.atualiza_estq_terceiro
         into   v_atualiza_estoque,        v_tipo_transacao,
                v_atualiza_estoque_terceiro
         from estq_005
         where codigo_transacao = :new.codigo_transacao;

         -- Verificando se estamos alterando alguma informacao
         -- que influencia no estoques
         if ((:new.codigo_deposito  <> :old.codigo_deposito
         or :new.coditem_nivel99  <> :old.coditem_nivel99
         or :new.coditem_grupo    <> :old.coditem_grupo
         or :new.coditem_subgrupo <> :old.coditem_subgrupo
         or :new.coditem_item     <> :old.coditem_item
         or :new.lote_entrega     <> :old.lote_entrega
         or :new.quantidade       <> :old.quantidade
         or :new.codigo_transacao <> :old.codigo_transacao)
         and (v_atualiza_estoque = 1 or
              v_atualiza_estoque_terceiro = 1))
         then
            raise_application_error(-20000,'Nao se pode alterar dados de estoque na nota.');
         end if;

         -- Nao podemos descancelar um item de nota fiscal
         if :new.data_canc_nfisc is null and :old.data_canc_nfisc is not null
         then
            raise_application_error(-20000,'Nao se pode descancelar uma nota fiscal.');
         end if;

         if v_situacao_entrada not in (2,4,5) and v_tipo_transacao = 'D'
         then
            select inter_fn_unid_med_tributaria(:new.natitem_nat_oper,
                                                :new.natitem_est_oper,
                                                decode(trim(:new.unidade_conv),null,:new.unidade_medida,:new.unidade_conv),
                                                :new.classific_fiscal,
                                                :new.coditem_nivel99,
                                                :new.coditem_grupo,
                                                :new.coditem_subgrupo,
                                                :new.coditem_item)
            into v_unid_med_trib_tmp from dual;
         end if;

         -- le os parametros de deposito de saida em pdoer de terceiros
         select obrf_010.local_entrega
         into   v_codigo_empresa
         from obrf_010
         where documento     = :new.capa_ent_nrdoc
           and serie         = :new.capa_ent_serie
           and cgc_cli_for_9 = :new.capa_ent_forcli9
           and cgc_cli_for_4 = :new.capa_ent_forcli4
           and cgc_cli_for_2 = :new.capa_ent_forcli2;

         v_dep_saida_terc_1 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida1');
         v_dep_saida_terc_2 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida2');
         v_dep_saida_terc_4 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida4');
         v_dep_saida_terc_7 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida7');
         v_dep_saida_terc_9 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida9');

         if :new.coditem_nivel99 = '1'
         then v_dep_saida_terc := v_dep_saida_terc_1;
         elsif :new.coditem_nivel99 = '2'
         then v_dep_saida_terc := v_dep_saida_terc_2;
         elsif :new.coditem_nivel99 = '4'
         then v_dep_saida_terc := v_dep_saida_terc_4;
         elsif :new.coditem_nivel99 = '7'
         then v_dep_saida_terc := v_dep_saida_terc_7;
         elsif :new.coditem_nivel99 = '9'
         then v_dep_saida_terc := v_dep_saida_terc_9;
         end if;

         -- se estiver sendo alterado o preco de custo do item da nota fiscal,
         -- entao atualiza o preco da ficha cardex
         if  :new.preco_custo <> :old.preco_custo and :new.codigo_deposito > 0
         and :new.data_canc_nfisc is null
         then
            -- Buscando a data da entrada da nota fiscal
            select data_transacao, moeda_nota
            into v_data_transacao, v_moeda
            from obrf_010
            where documento     = :new.capa_ent_nrdoc
              and serie         = :new.capa_ent_serie
              and cgc_cli_for_9 = :new.capa_ent_forcli9
              and cgc_cli_for_4 = :new.capa_ent_forcli4
              and cgc_cli_for_2 = :new.capa_ent_forcli2;

            --Atualiza o valor para a moeda corrente--
            if v_moeda <> 0
            then
               begin
                  select valor_moeda into v_indice_moeda
                  from basi_270
                  where basi_270.data_moeda   = v_data_transacao
                    and basi_270.codigo_moeda = v_moeda;
                  exception
                     when others then
                       v_indice_moeda := 1;
                end;
            else
               v_indice_moeda := 1;
            end if;


             v_valor_total     := :new.valor_total * v_indice_moeda;
             v_rateio_despesas := :new.rateio_despesas * v_indice_moeda;


            -- Calculando o valor contabil unitario para gravacao no Cardex
            if :new.quantidade > 0.000
            then
               v_valor_contabil := (v_valor_total + v_rateio_despesas) / :new.quantidade;
               v_valor_unitario := :new.preco_custo / :new.quantidade;
            else
               v_valor_contabil := v_valor_total + v_rateio_despesas;
               v_valor_unitario := :new.preco_custo;
            end if;

            v_preco_custo  := :new.preco_custo;

             -- LOGICA PARA PROCURA O PAIS DA EMPRESA DA NOTA
            begin
               select fatu_500.codigo_cidade into v_cidadeempresa from fatu_500
               where fatu_500.codigo_empresa = v_local_entrega;
               exception
                  when others then
                     v_cidadeempresa := 0;
            end;

            begin
               select basi_160.codigo_pais into v_paisempresa from basi_160
               where basi_160.cod_cidade = v_cidadeempresa;
               exception
                  when others then
                     v_paisempresa := 1;
            end;

            if v_paisempresa <> 1
            then
               if v_especie_docto = 'GRE' -- NOTA REMISSAO
               then
                  v_valor_contabil := 0.00;
                  v_preco_custo    := 0.00;
                  v_valor_unitario := 0.00;
               end if;
            end if;

            update estq_300
            set valor_movimento_unitario = v_valor_unitario,
                valor_contabil_unitario  = v_valor_contabil,
                valor_total              = v_preco_custo,
                sequencia_ficha          = 0
            where numero_documento    = :old.capa_ent_nrdoc
            and   serie_documento     = :old.capa_ent_serie
            and   cnpj_9              = :old.capa_ent_forcli9
            and   cnpj_4              = :old.capa_ent_forcli4
            and   cnpj_2              = :old.capa_ent_forcli2
            and   sequencia_documento = :old.sequencia
            and   codigo_deposito     = :old.codigo_deposito
            and   tabela_origem       = 'OBRF_015';
         end if;

         -- Quando estamos cancelando o item de nota fiscal
         if :new.data_canc_nfisc is not null and :old.data_canc_nfisc is null
         then

            if :new.transacao_canc_nfisc is null or :new.transacao_canc_nfisc = 0
            then
               raise_application_error(-20000,'Ao cancelar uma nota fiscal, deve-se informar a transacao de cancelamento.');
            end if;

            select estq_005.atualiza_estoque,   estq_005.atual_estoque_f,
                   estq_005.istajustefinanceiro
            into   v_atualiza_estoque,          v_atual_estoque_f,
                   v_istajustefinanceiro3
            from estq_005
            where estq_005.codigo_transacao = :new.transacao_canc_nfisc;

            if v_par_atualiza_estoque_f = 1 and v_atual_estoque_f = 'N' and v_atualiza_estoque = 1
            then
               raise_application_error(-20000,'Quando a empresa atualiza estoque gerencial, a transacao tambem deve atualizar estoque gerencial.');
            end if;

            -- caso a transacao atualise estoque, checa se o deposito foi informado
            if v_atualiza_estoque = 1 and :new.codigo_deposito = 0
            then
               raise_application_error(-20000,'Movimento de entrada nao atualizou o estoque. A transacao de cancelamento nao devera atualizar tambem');
            end if;

            -- caso a transacao nao atualise estoque, checa se o deposito foi informado
            if v_atualiza_estoque = 2 and :new.codigo_deposito <> 0
            then
               raise_application_error(-20000,'Movimento de entrada atualizou o estoque. A transacao de cancelamento deve atualizar tambem.');
            end if;

            v_data_canc_nfisc := :new.data_canc_nfisc;

            if v_data_transacao > v_periodo_estoque
            then
               v_data_canc_nfisc := v_data_transacao;
            end if;

            -- Buscando a empresa da nota fiscal
            select local_entrega, moeda_nota,data_transacao into v_local_entrega, v_moeda, v_data_transacao
            from obrf_010
            where documento     = :new.capa_ent_nrdoc
              and serie         = :new.capa_ent_serie
              and cgc_cli_for_9 = :new.capa_ent_forcli9
              and cgc_cli_for_4 = :new.capa_ent_forcli4
              and cgc_cli_for_2 = :new.capa_ent_forcli2;

            if :new.codigo_deposito > 0
            then
               -- Verificando o tipo de volume do deposito
               select tipo_volume into v_tipo_volume from basi_205
               where codigo_deposito = :new.codigo_deposito;

               -- Verificando se a transacao da nota fiscal de entrada foi
               -- devolucao
               select tipo_transacao into v_tipo_transacao from estq_005
               where codigo_transacao = :new.codigo_transacao;

               --Atualiza o valor para a moeda corrente--
               if v_moeda <> 0
               then
                  begin
                     select valor_moeda into v_indice_moeda
                     from basi_270
                     where basi_270.data_moeda   = v_data_transacao
                       and basi_270.codigo_moeda = v_moeda;
                     exception
                        when others then
                          v_indice_moeda := 1;
                  end;
               else
                  v_indice_moeda := 1;
               end if;


               v_valor_total     := :old.valor_total * v_indice_moeda;
               v_rateio_despesas := :old.rateio_despesas * v_indice_moeda;

               -- Calculando o valor contabil unitario para gravacao no Cardex
               if :old.quantidade > 0.000
               then
                  v_valor_contabil := (v_valor_total + v_rateio_despesas) / :old.quantidade;
               else
                  v_valor_contabil :=  v_valor_total + v_rateio_despesas;
               end if;

            v_quantidade_nf  := :new.quantidade;
            v_preco_custo    := :old.preco_custo;

             -- LOGICA PARA PROCURA O PAIS DA EMPRESA DA NOTA
            begin
               select fatu_500.codigo_cidade into v_cidadeempresa from fatu_500
               where fatu_500.codigo_empresa = v_local_entrega;
               exception
                  when others then
                     v_cidadeempresa := 0;
            end;

            begin
               select basi_160.codigo_pais into v_paisempresa from basi_160
               where basi_160.cod_cidade = v_cidadeempresa;
               exception
                  when others then
                     v_paisempresa := 1;
            end;

            if v_paisempresa <> 1
            then
               if v_especie_docto = 'FAC' -- NOTA FATURA
               then
                    if not(v_atualiza_estoque = 1 and v_permite_fatura_atu_esq = 'S')
                    then
                       v_quantidade_nf := 0.000;
                    end if;
               end if;

               if v_especie_docto = 'GRE' -- NOTA REMISSAO
               then
                  v_valor_contabil := 0.00;
                  v_preco_custo    := 0.00;
               end if;
            end if;


--- alteracao para inventario (peso do rolo) INICIO
            v_qtde_kilo := 0.00;

            -- se for tecido e for volume, calcula o peso do rolo
            if :new.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)
            then
               begin
                  -- le o total de quilos dos rolos (para controle de rolos em metros)
                  select nvl(sum(peso_bruto), 0) into v_qtde_kilo from pcpt_020
                  where cliente_cgc9       = :new.capa_ent_forcli9
                    and cliente_cgc4       = :new.capa_ent_forcli4
                    and cliente_cgc2       = :new.capa_ent_forcli2
                    and nota_fiscal_ent    = :new.capa_ent_nrdoc
                    and seri_fiscal_ent    = :new.capa_ent_serie
                    and sequ_fiscal_ent    = :new.sequencia;


               exception
                  when others then
                     v_qtde_kilo := 0.00;
               end;
            end if;  -- if :new.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)

--- alteracao para inventario (peso do rolo) FIM

            -- Vanderlei
            --verifica consist?ncias da natureza x transa??o em rela??o a op??o de ajuste financeiro
            begin
               select pedi_080.nfe_atu_financeiro
               into   v_transacao_financeira
               from pedi_080
               where pedi_080.natur_operacao = :new.natitem_nat_oper
                 and pedi_080.estado_natoper = :new.natitem_est_oper;
            exception
               when others then
                  v_transacao_financeira := 'N';
            end;

            if v_transacao_financeira = 'N'
            then
               if v_istajustefinanceiro3 = 1
               then
                  raise_application_error (-20000,'ATEN??O! n?o ? poss?vel utilizar uma transa??o de Ajuste Financeiro se natureza de opera??o n?o permite somente ajuste financeiro.');
               end if;
            end if;
            -- Fim da consist?ncia natureza x transa??o

             if v_istajustefinanceiro3 = 1
             then v_quantidade_nf := 0.00;
             end if;
             -- Enviando informacoes para atualizacao do Cardex

             inter_pr_insere_estq_300_recur (:new.codigo_deposito,       :new.coditem_nivel99,   :new.coditem_grupo,
                                       :new.coditem_subgrupo,      :new.coditem_item,      v_data_canc_nfisc,
                                       :new.lote_entrega,          :new.capa_ent_nrdoc,    :new.capa_ent_serie,
                                       :new.capa_ent_forcli9,      :new.capa_ent_forcli4,  :new.capa_ent_forcli2,
                                       :new.sequencia,
                                       :new.transacao_canc_nfisc,  'S',                    :new.centro_custo,
                                       v_quantidade_nf,            v_preco_custo,          v_valor_contabil,
                                       :new.usuario_cardex,        'OBRF_015',             :new.nome_programa,
                                       v_qtde_kilo,                0,                       0,
                                       :new.capa_ent_nrdoc,        0,                       0);

               -- Verificando se precisamos atualizar volumes
               if v_tipo_volume <> 0 and v_istajustefinanceiro3 <> 1
               then
                  -- Quando o transacao da nota fiscal de entrada nao for de devolucao ou Rertono de Terceiro
                  if v_tipo_transacao not in ('D','R')
                  then
                     if :old.coditem_nivel99 = '1' and v_tipo_volume = 1
                     then
                        update pcpc_330
                        set pcpc_330.estoque_tag = 4,
                            pcpc_330.forn9_entr  = 0,
                            pcpc_330.forn4_entr  = 0,
                            pcpc_330.forn2_entr  = 0,
                            pcpc_330.nota_entr   = 0,
                            pcpc_330.serie_entr  = ' ',
                            pcpc_330.seq_entr    = 0,
                            pcpc_330.usuario_cardex = :old.usuario_cardex,
                            pcpc_330.nome_prog_050  = :old.nome_programa,
                            pcpc_330.tabela_origem_cardex = 'OBRF_015'
                        where pcpc_330.forn9_entr = :old.capa_ent_forcli9
                        and   pcpc_330.forn4_entr = :old.capa_ent_forcli4
                        and   pcpc_330.forn2_entr = :old.capa_ent_forcli2
                        and   pcpc_330.nota_entr  = :old.capa_ent_nrdoc
                        and   pcpc_330.serie_entr = :old.capa_ent_serie
                        and   pcpc_330.seq_entr   = :old.sequencia;

                     end if;             -- if :old.coditem_nivel99 = '1' and v_tipo_volume = 1

                     if :new.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)
                     then

                        -- Foi decidido alterar a situacao do rolo e nao elimina-lo como
                        -- o programa de nfe fazia. Isto porque quando eliminamos o rolo nao sabemos
                        -- a origem da eliminacao e com isso a trigger do rolo seria disparada
                        -- e atualizaria duas vezes a entrada do mesmo.
                        update pcpt_020
                        set rolo_estoque         = 2,
                            data_cardex          = v_data_canc_nfisc,
                            transacao_cardex     = :new.transacao_canc_nfisc,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog_020        = :new.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                        where cliente_cgc9       = :new.capa_ent_forcli9
                          and cliente_cgc4       = :new.capa_ent_forcli4
                          and cliente_cgc2       = :new.capa_ent_forcli2
                          and nota_fiscal_ent    = :new.capa_ent_nrdoc
                          and seri_fiscal_ent    = :new.capa_ent_serie
                          and sequ_fiscal_ent    = :new.sequencia;

                     end if;             -- if :new.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)

                     if :new.coditem_nivel99 = '7' and v_tipo_volume = 7
                     then
                        -- Foi decidido alterar a situacao do caixa e nao elimina-la como
                        -- o programa de nfe fazia. Isto porque quando eliminamos a caixa nao sabemos
                        -- a origem da eliminacao e com isso a trigger da caixa seria disparada
                        -- e atualizaria duas vezes a entrada da mesmo.
                        update estq_060
                        set status_caixa         = 9,  -- Caixa eliminada
                            data_cardex          = v_data_canc_nfisc,
                            transacao_cardex     = :new.transacao_canc_nfisc,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog            = :new.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                        where cnpj_fornecedor9   = :new.capa_ent_forcli9
                          and cnpj_fornecedor4   = :new.capa_ent_forcli4
                          and cnpj_fornecedor2   = :new.capa_ent_forcli2
                          and nota_fisc_ent      = :new.capa_ent_nrdoc
                          and seri_fisc_ent      = :new.capa_ent_serie
                          and sequ_fisc_ent      = :new.sequencia;
                     end if;             -- if :new.nivel_estrutura = '7' and v_tipo_volume = 7
                  else
                     if  :new.coditem_nivel99 in ('2','4')
                     and v_tipo_volume        in (2,4)
                     and v_tipo_transacao      = 'D' -- Devolucao
                     then

                        update pcpt_020
                        set cod_empresa_nota     = v_local_entrega,
                            nota_fiscal          = :new.num_nota_orig,
                            serie_fiscal_sai     = :new.serie_nota_orig,
                            seq_nota_fiscal      = :new.seq_nota_orig,
                            rolo_estoque         = 2,
                            cliente_cgc9         = 0,
                            cliente_cgc4         = 0,
                            cliente_cgc2         = 0,
                            nota_fiscal_ent      = 0,
                            seri_fiscal_ent      = ' ',
                            sequ_fiscal_ent      = 0,
                            data_cardex          = v_data_canc_nfisc,
                            transacao_cardex     = :new.transacao_canc_nfisc,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog_020        = :new.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                        where cliente_cgc9       = :new.capa_ent_forcli9
                          and cliente_cgc4       = :new.capa_ent_forcli4
                          and cliente_cgc2       = :new.capa_ent_forcli2
                          and nota_fiscal_ent    = :new.capa_ent_nrdoc
                          and seri_fiscal_ent    = :new.capa_ent_serie
                          and sequ_fiscal_ent    = :new.sequencia;

                     end if; -- if :new.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)

                     if  :new.coditem_nivel99 = '7'
                     and v_tipo_volume        = 7
                     and v_tipo_transacao     = 'D' -- Devolucao
                     then

                        update estq_060
                        set nota_fiscal          = :new.num_nota_orig,
                            serie_nota           = :new.serie_nota_orig,
                            seq_nota_fiscal      = :new.seq_nota_orig,
                            status_caixa         = 4,
                            cnpj_fornecedor9     = 0,
                            cnpj_fornecedor4     = 0,
                            cnpj_fornecedor2     = 0,
                            nota_fisc_ent        = 0,
                            seri_fisc_ent        = ' ',
                            sequ_fisc_ent        = 0,
                            data_cardex          = v_data_canc_nfisc,
                            transacao_cardex     = :new.transacao_canc_nfisc,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog            = :new.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                        where cnpj_fornecedor9   = :new.capa_ent_forcli9
                          and cnpj_fornecedor4   = :new.capa_ent_forcli4
                          and cnpj_fornecedor2   = :new.capa_ent_forcli2
                          and nota_fisc_ent      = :new.capa_ent_nrdoc
                          and seri_fisc_ent      = :new.capa_ent_serie
                          and sequ_fisc_ent      = :new.sequencia;
                     end if;           --  if :new.coditem_nivel99 = '7' and v_tipo_volume = 7

                     if :new.coditem_nivel99 = '9'
                     and v_tipo_volume = 9
                     and v_tipo_transacao = 'D' -- devolucao
                     then
                        update pcpf_060
                        set num_nota_sai         = :new.num_nota_orig,
                            ser_nota_sai         = :new.serie_nota_orig,
                            seq_nota_sai         = :new.seq_nota_orig,
                            cgc9_forne           = 0,
                            cgc4_forne           = 0,
                            cgc2_forne           = 0,
                            nf_entrada           = 0,
                            serie_nf_entrada     = ' ',
                            status_fardo         = 2,
                            data_cardex          = v_data_canc_nfisc,
                            transacao_cardex     = :new.transacao_canc_nfisc,
                            usuario_cardex       = :new.usuario_cardex,
                            nome_prog            = :new.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                        where fardo_nivel99    = :new.coditem_nivel99
                          and fardo_grupo      = :new.coditem_grupo
                          and fardo_subgrupo   = :new.coditem_subgrupo
                          and fardo_item       = :new.coditem_item
                          and cgc9_forne       = :new.capa_ent_forcli9
                          and cgc4_forne       = :new.capa_ent_forcli4
                          and cgc2_forne       = :new.capa_ent_forcli2
                          and nf_entrada       = :new.capa_ent_nrdoc
                          and serie_nf_entrada = :new.capa_ent_serie;
                     end if;

                  end if;              -- if v_tipo_transacao not in ('D','R')
               end if;                 -- if v_tipo_volume <> 0
            end if;                    -- if :new.codigo_deposito > 0

            /*IN?CIO MOVIMENTA??O DE ESTOQUE NO DEP?SITO PR?PRIO EM PODER DE TERCEIROS
              SS: 60598/001 - THIAGO.FELIPE*/
            begin
               select estq_005.icdtransdepterceiros, estq_005.tipo_transacao,
                      estq_005.atualiza_estq_terceiro
               into   v_trans_terceiro,              v_tipo_transacao_005,
                      v_atualiza_estq_terceiro
               from estq_005
               where estq_005.codigo_transacao = :new.codigo_transacao;
            exception
                when no_data_found then
                   v_trans_terceiro     := 0;
                   v_tipo_transacao_005 := 'N';
                   v_atualiza_estq_terceiro := 0;
            end;

            /*Verifica deposito relacionado ao terceiro*/
            begin
              select basi_572.cod_dep
              into v_dep_terceiro_572
              from basi_572
              where basi_572.cod_empresa = v_local_entrega
                and basi_572.terc_cnpj9  = v_responsavel9
                and basi_572.terc_cnpj4  = v_responsavel4
                and basi_572.terc_cnpj2  = v_responsavel2;
              exception
              when no_data_found then
                   v_dep_terceiro_572     := 0;
            end;

            if ( v_dep_terceiro_572 = 0 and  (v_tipo_transacao_005 = 'R' or v_tipo_transacao_005 = 'D' or
                 v_tipo_transacao_005 = 'I' or v_tipo_transacao_005 = 'N' or v_atualiza_estq_terceiro = 1) )
               or
               ( v_dep_terceiro_572 <> 0 and  (v_tipo_transacao_005 = 'R' or v_tipo_transacao_005 = 'E' or
                 v_tipo_transacao_005 = 'I' or v_atualiza_estq_terceiro = 1) )
            then
               if v_dep_terceiro_572 = 0
               then

                  if :new.codigo_deposito = 0 and (v_trans_terceiro > 0 and v_dep_saida_terc >0)
                  then
                     v_deposito_terceiro := v_dep_saida_terc;
                  elsif :new.codigo_deposito > 0
                  then
                     begin
                        select basi_205.icddepdestterceiros into v_deposito_terceiro from basi_205
                        where basi_205.codigo_deposito = :new.codigo_deposito;
                     exception
                         when no_data_found then
                            v_deposito_terceiro := 0;
                     end;
                  else
                     begin
                        select 1 into v_cont_processo from hdoc_001
                        where hdoc_001.tipo = 350
                          and hdoc_001.codigo > 0
                          and hdoc_001.codigo2 > 0;
                     exception
                      when no_data_found then
                         v_cont_processo := 0;
                  end;

                  if v_cont_processo > 0
                  then
                     begin
                        select obrf_510.processo_nota, obrf_510.cod_emp_benef,
                               obrf_510.cod_emp_origem
                        into   v_processo_nota,        v_cod_emp_benef,
                               v_cod_emp_origem
                        from obrf_510, (select obrf_510.cod_emp_benef, obrf_510.cod_emp_origem, obrf_510.seq_exec_rotina, max(obrf_510.seq_nota) seq_nota
                                        from obrf_510
                                        where obrf_510.numero_nota       = :new.capa_ent_nrdoc
                                          and obrf_510.serie_nota        = :new.capa_ent_serie
                                          and obrf_510.cgc9_cli_for_nota = :new.capa_ent_forcli9
                                          and obrf_510.cgc4_cli_for_nota = :new.capa_ent_forcli4
                                          and obrf_510.cgc2_cli_for_nota = :new.capa_ent_forcli2
                                        group by obrf_510.cod_emp_benef, obrf_510.cod_emp_origem, obrf_510.seq_exec_rotina) aux_obrf_510
                         where obrf_510.cod_emp_benef   = aux_obrf_510.cod_emp_benef
                           and obrf_510.cod_emp_origem  = aux_obrf_510.cod_emp_origem
                           and obrf_510.seq_exec_rotina = aux_obrf_510.seq_exec_rotina
                           and obrf_510.seq_nota        = aux_obrf_510.seq_nota;
                     exception
                         when no_data_found then
                            v_processo_nota  := 0;
                            v_cod_emp_benef  := 0;
                            v_cod_emp_origem := 0;
                     end;

                     if v_processo_nota > 0 and v_cod_emp_benef > 0 and v_cod_emp_origem > 0
                     then
                        if v_processo_nota = 4
                        then
                           v_tipo_processo := 354;
                        end if;

                        if v_processo_nota = 10
                        then
                           v_tipo_processo := 360;
                        end if;

                        if v_processo_nota = 16
                        then
                           v_tipo_processo := 366;
                        end if;

                           begin
                              select hdoc_001.campo_numerico25,  hdoc_001.campo_numerico27
                              into   v_deposito_terceiro,        v_centro_custo_terceiro
                              from hdoc_001
                              where hdoc_001.tipo    = v_tipo_processo
                                and hdoc_001.codigo  = v_cod_emp_benef
                                and hdoc_001.codigo2 = v_cod_emp_origem;
                           exception
                               when no_data_found then
                                  v_deposito_terceiro := 0;
                                  v_centro_custo_terceiro := 0;
                           end;
                        end if; --if v_processo_nota > 0 and v_cod_emp_benef > 0 and v_cod_emp_origem > 0
                     end if; --if v_cont_processo > 0
                  end if; --if :new.codigo_deposito > 0
               else
                  v_deposito_terceiro := v_dep_terceiro_572;
               end if;

               if v_trans_terceiro > 0 and v_deposito_terceiro > 0
               then
                  v_centro_custo_terceiro := :new.centro_custo;

                  begin
                     select estq_005.trans_cancelamento into v_trans_terceiro_canc from estq_005
                     where estq_005.codigo_transacao = v_trans_terceiro;
                  exception
                      when no_data_found then
                         v_trans_terceiro_canc := 0;
                  end;


                  begin
                     select estq_005.atualiza_estoque, estq_005.istajustefinanceiro,
                            estq_005.entrada_saida
                     into v_atualiza_estoque_terceiro, v_istajustefinanceiro4,
                          v_ent_sai_terc
                     from estq_005
                     where estq_005.codigo_transacao = v_trans_terceiro_canc;
                  exception
                      when no_data_found then
                         v_atualiza_estoque_terceiro := 0;
                         v_ent_sai_terc              := '';
                         v_istajustefinanceiro4      := 0;
                  end;

                  if v_trans_terceiro_canc > 0 and v_atualiza_estoque_terceiro = 1
                  then
                     -- Buscando a data da entrada da nota fiscal
                     select obrf_010.data_transacao, obrf_010.moeda_nota
                     into   v_data_transacao,        v_moeda
                     from obrf_010
                     where obrf_010.documento     = :new.capa_ent_nrdoc
                       and obrf_010.serie         = :new.capa_ent_serie
                       and obrf_010.cgc_cli_for_9 = :new.capa_ent_forcli9
                       and obrf_010.cgc_cli_for_4 = :new.capa_ent_forcli4
                       and obrf_010.cgc_cli_for_2 = :new.capa_ent_forcli2;

                     --Atualiza o valor para a moeda corrente--
                     if v_moeda <> 0
                     then
                        begin
                           select basi_270.valor_moeda into v_indice_moeda
                           from basi_270
                           where basi_270.data_moeda   = v_data_transacao
                             and basi_270.codigo_moeda = v_moeda;
                        exception
                            when others then
                               v_indice_moeda := 1;
                        end;
                     else
                        v_indice_moeda := 1;
                     end if;

                     v_valor_total     := :new.valor_total * v_indice_moeda;
                     v_rateio_despesas := :new.rateio_despesas * v_indice_moeda;

                     -- Calculando o valor contabil unitario para gravacao no Cardex
                     if :new.quantidade > 0.000
                     then
                        v_valor_contabil := (v_valor_total + v_rateio_despesas) / :new.quantidade;
                        v_valor_unitario := :new.preco_custo / :new.quantidade;
                     else
                        v_valor_contabil := v_valor_total + v_rateio_despesas;
                        v_valor_unitario := :new.preco_custo;
                     end if;

                     v_preco_custo  := :new.preco_custo;

                      -- LOGICA PARA PROCURA O PAIS DA EMPRESA DA NOTA
                     begin
                        select fatu_500.codigo_cidade into v_cidadeempresa from fatu_500
                        where fatu_500.codigo_empresa = v_local_entrega;
                     exception
                         when others then
                            v_cidadeempresa := 0;
                     end;

                     begin
                        select basi_160.codigo_pais into v_paisempresa from basi_160
                        where basi_160.cod_cidade = v_cidadeempresa;
                     exception
                         when others then
                            v_paisempresa := 1;
                     end;

                     if v_paisempresa <> 1
                     then
                        if v_especie_docto = 'GRE' -- NOTA REMISSAO
                        then
                           v_valor_contabil := 0.00;
                           v_preco_custo    := 0.00;
                           v_valor_unitario := 0.00;
                        end if;
                     end if;

                     begin
                        update estq_300
                        set estq_300.valor_movimento_unitario = v_valor_unitario,
                            estq_300.valor_contabil_unitario  = v_valor_contabil,
                            estq_300.valor_total              = v_preco_custo,
                            estq_300.sequencia_ficha          = 0
                        where estq_300.numero_documento    = :old.capa_ent_nrdoc
                          and   estq_300.serie_documento     = :old.capa_ent_serie
                          and   estq_300.cnpj_9              = :old.capa_ent_forcli9
                          and   estq_300.cnpj_4              = :old.capa_ent_forcli4
                          and   estq_300.cnpj_2              = :old.capa_ent_forcli2
                          and   estq_300.sequencia_documento = :old.sequencia
                          and   estq_300.codigo_deposito     = :old.codigo_deposito
                          and   estq_300.tabela_origem       = 'OBRF_015';
                     exception
                         when others then
                            raise_application_error(-20000, 'N?o atualizou tabela estq_300.' || Chr(10) || SQLERRM);
                     end;

                     if v_centro_custo_terceiro <= 0 or v_centro_custo_terceiro is null
                     then
                        v_centro_custo_terceiro := :new.centro_custo;
                     end if;

                     if v_istajustefinanceiro4 = 1
                     then v_quantidade_nf := 0.00;
                     end if;

                     inter_pr_insere_estq_300_recur (v_deposito_terceiro,        :new.coditem_nivel99,   :new.coditem_grupo,
                                               :new.coditem_subgrupo,      :new.coditem_item,      v_data_canc_nfisc,
                                               :new.lote_entrega,          :new.capa_ent_nrdoc,    :new.capa_ent_serie,
                                               :new.capa_ent_forcli9,      :new.capa_ent_forcli4,  :new.capa_ent_forcli2,
                                               :new.sequencia,
                                               v_trans_terceiro_canc,      v_ent_sai_terc,         v_centro_custo_terceiro,
                                               v_quantidade_nf,            v_preco_custo,          v_valor_contabil,
                                               :new.usuario_cardex,        'OBRF_015',             :new.nome_programa,
                                               0.00,                       0,                       0,
                                               :new.capa_ent_nrdoc,        0,                       0);
                  end if;                 -- if v_trans_terceiro_canc > 0 and v_atualiza_estoque_terceiro = 1
               end if;                    -- if v_trans_terceiro > 0 and v_deposito_terceiro > 0
            end if;                    -- if v_tipo_transacao_005 = 'R' or v_tipo_transacao_005 = 'D'
         end if;                       -- if :new.data_canc_nfisc is not null and :old.data_canc_nfisc is null
      end if;                          -- if updating

      if deleting
      then

         begin
            -- verificar se existe documento do conta corrente para este item
            select estq_360.documento into v_doc_conta_corrente
            from estq_360
            where estq_360.nota_entrada       = :old.capa_ent_nrdoc
              and estq_360.serie_nota_entrada = :old.capa_ent_serie
              and estq_360.seq_nota_entrada   = :old.sequencia
              and estq_360.cnpj9_cliente      = :old.capa_ent_forcli9
              and estq_360.cnpj4_cliente      = :old.capa_ent_forcli4
              and estq_360.cnpj2_cliente      = :old.capa_ent_forcli2
              and rownum = 1;
            exception
               when others then
                  v_doc_conta_corrente := 0;
         end;

         if v_doc_conta_corrente > 0
         then

            -- se existir documento do conta corrente para este item, remover a referencia
            begin
               update estq_360
               set estq_360.nota_entrada         = 0,
                   estq_360.serie_nota_entrada   = '',
                   estq_360.seq_nota_entrada     = 0
               where estq_360.nota_entrada       = :old.capa_ent_nrdoc
                 and estq_360.serie_nota_entrada = :old.capa_ent_serie
                 and estq_360.seq_nota_entrada   = :old.sequencia
                 and estq_360.cnpj9_cliente      = :old.capa_ent_forcli9
                 and estq_360.cnpj4_cliente      = :old.capa_ent_forcli4
                 and estq_360.cnpj2_cliente      = :old.capa_ent_forcli2;
               exception
                  when others then
                     null;
            end;

            /* Ao Eliminar uma nota com conta corrente deve desrelacionar os rolos da NF*/
            begin
               update pcpt_020
               set pcpt_020.nota_fiscal_ent  = 0,
                   pcpt_020.seri_fiscal_ent  = null,
                   pcpt_020.sequ_fiscal_ent  = 0,
                   pcpt_020.fornecedor_cgc9  = 0,
                   pcpt_020.fornecedor_cgc4  = 0,
                   pcpt_020.fornecedor_cgc2  = 0
               where pcpt_020.nota_fiscal_ent = :old.capa_ent_nrdoc
                and pcpt_020.seri_fiscal_ent  = :old.capa_ent_serie
                and pcpt_020.sequ_fiscal_ent  = :old.sequencia
                and pcpt_020.fornecedor_cgc9  = :old.capa_ent_forcli9
                and pcpt_020.fornecedor_cgc4  = :old.capa_ent_forcli4
                and pcpt_020.fornecedor_cgc2  = :old.capa_ent_forcli2;
               exception
                  when others then
                     null;
            end;

         end if;

         -- le os parametros de deposito de saida em pdoer de terceiros
         select obrf_010.local_entrega
         into   v_codigo_empresa
         from obrf_010
         where documento     = :old.capa_ent_nrdoc
           and serie         = :old.capa_ent_serie
           and cgc_cli_for_9 = :old.capa_ent_forcli9
           and cgc_cli_for_4 = :old.capa_ent_forcli4
           and cgc_cli_for_2 = :old.capa_ent_forcli2;

         v_dep_saida_terc_1 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida1');
         v_dep_saida_terc_2 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida2');
         v_dep_saida_terc_4 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida4');
         v_dep_saida_terc_7 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida7');
         v_dep_saida_terc_9 := inter_fn_get_param_int(v_codigo_empresa, 'estoque.depTercSaida9');

         if :old.coditem_nivel99 = '1'
         then v_dep_saida_terc := v_dep_saida_terc_1;
         elsif :old.coditem_nivel99 = '2'
         then v_dep_saida_terc := v_dep_saida_terc_2;
         elsif :old.coditem_nivel99 = '4'
         then v_dep_saida_terc := v_dep_saida_terc_4;
         elsif :old.coditem_nivel99 = '7'
         then v_dep_saida_terc := v_dep_saida_terc_7;
         elsif :old.coditem_nivel99 = '9'
         then v_dep_saida_terc := v_dep_saida_terc_9;
         end if;

         if :old.codigo_deposito > 0 -- eliminou o item da nota fiscal
         then
            -- Buscando a data da entrada e situacao da nota fiscal
            select data_transacao,    situacao_entrada,
                   local_entrega
            into   v_data_transacao,  v_situacao_entrada,
                   v_local_entrega
            from obrf_010
            where documento     = :old.capa_ent_nrdoc
              and serie         = :old.capa_ent_serie
              and cgc_cli_for_9 = :old.capa_ent_forcli9
              and cgc_cli_for_4 = :old.capa_ent_forcli4
              and cgc_cli_for_2 = :old.capa_ent_forcli2;

            -- Verificando se existe informacoes no cardex para a nota fiscal
            begin
               select count(*)
               into v_nr_registro_300
               from estq_300
               where numero_documento    = :old.capa_ent_nrdoc
                 and serie_documento     = :old.capa_ent_serie
                 and cnpj_9              = :old.capa_ent_forcli9
                 and cnpj_4              = :old.capa_ent_forcli4
                 and cnpj_2              = :old.capa_ent_forcli2
                 and sequencia_documento = :old.sequencia
                 and codigo_deposito     = :old.codigo_deposito
                 and tabela_origem       = 'OBRF_015';
            end;

            -- Quando encontrou registro no cardex e a nota esta aberta ou e de fornecedor
            if v_nr_registro_300 > 0 and v_situacao_entrada in (3,4,5)
            then
               -- Verificando o tipo de volume do deposito
               begin
                  select tipo_volume
                  into v_tipo_volume
                  from basi_205
                  where codigo_deposito = :old.codigo_deposito;
               end;

               -- Verificando se a transacao da nota fiscal de entrada foi
               -- devolucao
               -- Buscando a transacao de cancelamento referente a tranasacao da NF
               begin
                  select tipo_transacao,   trans_cancelamento
                  into   v_tipo_transacao, v_tran_cancel_nfe
                  from estq_005
                  where codigo_transacao = :old.codigo_transacao;
               end;

              update  estq_300 set flag_elimina = 1
              where numero_documento    = :old.capa_ent_nrdoc
              and serie_documento     = :old.capa_ent_serie
              and cnpj_9              = :old.capa_ent_forcli9
              and cnpj_4              = :old.capa_ent_forcli4
              and cnpj_2              = :old.capa_ent_forcli2
              and sequencia_documento = :old.sequencia
              and codigo_deposito     = :old.codigo_deposito
              and tabela_origem       = 'OBRF_015';

               -- Enviando informac?es para atualizac?o do Cardex
               delete estq_300
               where numero_documento    = :old.capa_ent_nrdoc
                 and serie_documento     = :old.capa_ent_serie
                 and cnpj_9              = :old.capa_ent_forcli9
                 and cnpj_4              = :old.capa_ent_forcli4
                 and cnpj_2              = :old.capa_ent_forcli2
                 and sequencia_documento = :old.sequencia
                 and codigo_deposito     = :old.codigo_deposito
                 and tabela_origem       = 'OBRF_015';

               -- Verificando se precisamos atualizar volumes
               if v_tipo_volume <> 0
               then
                  -- Quando o transacao da nota fiscal de entrada nao for de devolucao ou Rertono de Terceiro
                  if v_tipo_transacao not in ('D','R')
                  then
                     if :old.coditem_nivel99 = '1' and v_tipo_volume = 1
                     then
                        update pcpc_330
                        set pcpc_330.estoque_tag = 4,
                            pcpc_330.forn9_entr  = 0,
                            pcpc_330.forn4_entr  = 0,
                            pcpc_330.forn2_entr  = 0,
                            pcpc_330.nota_entr   = 0,
                            pcpc_330.serie_entr  = ' ',
                            pcpc_330.seq_entr    = 0,
                            pcpc_330.usuario_cardex = :old.usuario_cardex,
                            pcpc_330.nome_prog_050  = :old.nome_programa,
                            pcpc_330.tabela_origem_cardex = 'OBRF_015'
                        where pcpc_330.forn9_entr = :old.capa_ent_forcli9
                        and   pcpc_330.forn4_entr = :old.capa_ent_forcli4
                        and   pcpc_330.forn2_entr = :old.capa_ent_forcli2
                        and   pcpc_330.nota_entr  = :old.capa_ent_nrdoc
                        and   pcpc_330.serie_entr = :old.capa_ent_serie
                        and   pcpc_330.seq_entr   = :old.sequencia;

                     end if;             -- if :old.coditem_nivel99 = '1' and v_tipo_volume = 1

                     if :old.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)
                     then
                        -- Foi decidido alterar a situacao do rolo e nao elimina-lo como
                        -- o programa de nfe fazia. Isto porque quando eliminamos o rolo nao sabemos
                        -- a origem da eliminacao e com isso a trigger do rolo seria disparada
                        -- e atualizaria duas vezes a entrada do mesmo.
                        update pcpt_020
                        set rolo_estoque         = 2,
                            cliente_cgc9         = 0,
                            cliente_cgc4         = 0,
                            cliente_cgc2         = 0,
                            nota_fiscal_ent      = 0,
                            seri_fiscal_ent      = ' ',
                            sequ_fiscal_ent      = 0,
                            usuario_cardex       = :old.usuario_cardex,
                            nome_prog_020        = :old.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                        where cliente_cgc9       = :old.capa_ent_forcli9
                          and cliente_cgc4       = :old.capa_ent_forcli4
                          and cliente_cgc2       = :old.capa_ent_forcli2
                          and nota_fiscal_ent    = :old.capa_ent_nrdoc
                          and seri_fiscal_ent    = :old.capa_ent_serie
                          and sequ_fiscal_ent    = :old.sequencia;

                     end if;             -- if :old.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)

                     if :old.coditem_nivel99 = '7' and v_tipo_volume = 7
                     then
                        -- Foi decidido alterar a situacao do caixa e nao elimina-la como
                        -- o programa de nfe fazia. Isto porque quando eliminamos a caixa nao sabemos
                        -- a origem da eliminacao e com isso a trigger da caixa seria disparada
                        -- e atualizaria duas vezes a entrada da mesmo.

                        -- Busca o codigo da empresa da nota fiscal de saida
                        -- pelo CNPJ do fornecedor da nota de entrada
                        begin
                           select min(codigo_empresa)
                           into v_cod_empresa_nfs050
                           from fatu_500
                           where cgc_9 = :old.capa_ent_forcli9
                             and cgc_4 = :old.capa_ent_forcli4
                             and cgc_2 = :old.capa_ent_forcli2;
                           exception
                           when no_data_found then
                              v_cod_empresa_nfs050 := 0;
                        end;

                        /* procura se a nota de entrada foi uma entrada por confirma??o.
                           caso for volta a situa??o da caixa para 4 e para o deposito de saida,
                           tamb?m volta a situa??o da nota de saida para 1. */
                        begin
                           select fatu_060.deposito, fatu_060.lote_acomp
                           into v_deposito_saida, v_lote_saida
                           from fatu_060, fatu_050
                           where fatu_060.ch_it_nf_cd_empr  = fatu_050.codigo_empresa
                             and fatu_060.ch_it_nf_num_nfis = fatu_050.num_nota_fiscal
                             and fatu_060.ch_it_nf_ser_nfis = fatu_050.serie_nota_fisc
                             and fatu_050.situacao_nfisc    = 4
                             and fatu_060.ch_it_nf_cd_empr  = v_cod_empresa_nfs050
                             and fatu_060.ch_it_nf_num_nfis = :old.capa_ent_nrdoc
                             and fatu_060.ch_it_nf_ser_nfis = :old.capa_ent_serie
                             and fatu_060.seq_item_nfisc    = :old.sequencia;
                           exception
                           when no_data_found then
                              v_deposito_saida := 0;
                        end;

                        if v_deposito_saida > 0
                        then
                           update estq_060
                           set status_caixa         = 4,  -- Caixa faturada
                               cnpj_fornecedor9     = 0,
                               cnpj_fornecedor4     = 0,
                               cnpj_fornecedor2     = 0,
                               nota_fisc_ent        = 0,
                               seri_fisc_ent        = ' ',
                               sequ_fisc_ent        = 0,
                               nota_fiscal          = :old.capa_ent_nrdoc,
                               serie_nota           = :old.capa_ent_serie,
                               seq_nota_fiscal      = :old.sequencia,
                               codigo_deposito      = v_deposito_saida,
                               lote                 = v_lote_saida,
                               usuario_cardex       = :old.usuario_cardex,
                               nome_prog            = :new.nome_programa,
                               tabela_origem_cardex = 'OBRF_015'
                           where cnpj_fornecedor9   = :old.capa_ent_forcli9
                             and cnpj_fornecedor4   = :old.capa_ent_forcli4
                             and cnpj_fornecedor2   = :old.capa_ent_forcli2
                             and nota_fisc_ent      = :old.capa_ent_nrdoc
                             and seri_fisc_ent      = :old.capa_ent_serie
                             and sequ_fisc_ent      = :old.sequencia;

                           /*update fatu_050
                           set fatu_050.situacao_nfisc = 1
                           where fatu_050.codigo_empresa  = v_cod_empresa_nfs050
                             and fatu_050.num_nota_fiscal = :old.capa_ent_nrdoc
                             and fatu_050.serie_nota_fisc = :old.capa_ent_serie;

               Agora a situa??o ? alterada no after delete do programa obrf_010
               */
                        else
                           update estq_060
                           set status_caixa         = 9,  -- Caixa eliminada
                               cnpj_fornecedor9     = 0,
                               cnpj_fornecedor4     = 0,
                               cnpj_fornecedor2     = 0,
                               nota_fisc_ent        = 0,
                               seri_fisc_ent        = ' ',
                               sequ_fisc_ent        = 0,
                               usuario_cardex       = :old.usuario_cardex,
                               nome_prog            = :old.nome_programa,
                               tabela_origem_cardex = 'OBRF_015'
                           where cnpj_fornecedor9   = :old.capa_ent_forcli9
                             and cnpj_fornecedor4   = :old.capa_ent_forcli4
                             and cnpj_fornecedor2   = :old.capa_ent_forcli2
                             and nota_fisc_ent      = :old.capa_ent_nrdoc
                             and seri_fisc_ent      = :old.capa_ent_serie
                             and sequ_fisc_ent      = :old.sequencia;
                        end if;
                     end if;             -- if :old.nivel_estrutura = '7' and v_tipo_volume = 7
                  else

                    if :old.coditem_nivel99 = '1' and v_tipo_volume = 1
                     then
                        update pcpc_330
                        set pcpc_330.estoque_tag = 4,
                            pcpc_330.forn9_entr  = 0,
                            pcpc_330.forn4_entr  = 0,
                            pcpc_330.forn2_entr  = 0,
                            pcpc_330.nota_entr   = 0,
                            pcpc_330.serie_entr  = ' ',
                            pcpc_330.seq_entr    = 0,
                            pcpc_330.usuario_cardex = :old.usuario_cardex,
                            pcpc_330.nome_prog_050  = :old.nome_programa,
                            pcpc_330.tabela_origem_cardex = 'OBRF_015'
                        where pcpc_330.forn9_entr = :old.capa_ent_forcli9
                        and   pcpc_330.forn4_entr = :old.capa_ent_forcli4
                        and   pcpc_330.forn2_entr = :old.capa_ent_forcli2
                        and   pcpc_330.nota_entr  = :old.capa_ent_nrdoc
                        and   pcpc_330.serie_entr = :old.capa_ent_serie
                        and   pcpc_330.seq_entr   = :old.sequencia;

                     end if;             -- if :old.coditem_nivel99 = '1' and v_tipo_volume = 1

                     if  :old.coditem_nivel99 in ('2','4')
                     and v_tipo_volume        in (2,4)
                     and v_tipo_transacao      = 'D' -- Devolucao
                     then
                        update pcpt_020
                        set cod_empresa_nota     = v_local_entrega,
                            nota_fiscal          = :old.num_nota_orig,
                            serie_fiscal_sai     = :old.serie_nota_orig,
                            seq_nota_fiscal      = :old.seq_nota_orig,
                            rolo_estoque         = 2,
                            cliente_cgc9         = 0,
                            cliente_cgc4         = 0,
                            cliente_cgc2         = 0,
                            nota_fiscal_ent      = 0,
                            seri_fiscal_ent      = ' ',
                            sequ_fiscal_ent      = 0,
                            usuario_cardex       = :old.usuario_cardex,
                            nome_prog_020        = :old.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                        where cliente_cgc9       = :old.capa_ent_forcli9
                          and cliente_cgc4       = :old.capa_ent_forcli4
                          and cliente_cgc2       = :old.capa_ent_forcli2
                          and nota_fiscal_ent    = :old.capa_ent_nrdoc
                          and seri_fiscal_ent    = :old.capa_ent_serie
                          and sequ_fiscal_ent    = :old.sequencia;

                     end if; -- if :old.coditem_nivel99 in ('2','4') and v_tipo_volume in (2,4)

                     if  :old.coditem_nivel99 = '7'
                     and v_tipo_volume        = 7
                     and v_tipo_transacao     = 'D' -- Devolucao
                     then

                        update estq_060
                        set nota_fiscal          = :old.num_nota_orig,
                            serie_nota           = :old.serie_nota_orig,
                            seq_nota_fiscal      = :old.seq_nota_orig,
                            status_caixa         = 4,
                            cnpj_fornecedor9     = 0,
                            cnpj_fornecedor4     = 0,
                            cnpj_fornecedor2     = 0,
                            nota_fisc_ent        = 0,
                            seri_fisc_ent        = ' ',
                            sequ_fisc_ent        = 0,
                            usuario_cardex       = :old.usuario_cardex,
                            nome_prog            = :old.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                        where cnpj_fornecedor9   = :old.capa_ent_forcli9
                          and cnpj_fornecedor4   = :old.capa_ent_forcli4
                          and cnpj_fornecedor2   = :old.capa_ent_forcli2
                          and nota_fisc_ent      = :old.capa_ent_nrdoc
                          and seri_fisc_ent      = :old.capa_ent_serie
                          and sequ_fisc_ent      = :old.sequencia;
                     end if;           --  if :old.coditem_nivel99 = '7' and v_tipo_volume = 7

                     if :old.coditem_nivel99 = '9'
                     and v_tipo_volume = 9
                     and v_tipo_transacao = 'D' -- devolucao
                     then

                        update pcpf_060
                        set num_nota_sai         = :old.num_nota_orig,
                            ser_nota_sai         = :old.serie_nota_orig,
                            seq_nota_sai         = :old.seq_nota_orig,
                            cgc9_forne           = 0,
                            cgc4_forne           = 0,
                            cgc2_forne           = 0,
                            nf_entrada           = 0,
                            serie_nf_entrada     = ' ',
                            status_fardo         = 2,
                            usuario_cardex       = :old.usuario_cardex,
                            nome_prog            = :old.nome_programa,
                            tabela_origem_cardex = 'OBRF_015'
                        where fardo_nivel99    = :old.coditem_nivel99
                          and fardo_grupo      = :old.coditem_grupo
                          and fardo_subgrupo   = :old.coditem_subgrupo
                          and fardo_item       = :old.coditem_item
                          and cgc9_forne       = :old.capa_ent_forcli9
                          and cgc4_forne       = :old.capa_ent_forcli4
                          and cgc2_forne       = :old.capa_ent_forcli2
                          and nf_entrada       = :old.capa_ent_nrdoc
                          and serie_nf_entrada = :old.capa_ent_serie;
                     end if;

                  end if;              -- if v_tipo_transacao not in ('D','R')
               end if;                 -- if v_tipo_volume <> 0
            end if;                    -- if v_nr_registro_300 > 0
         end if;                       -- if :old.deposito > 0

         /*IN?CIO MOVIMENTA??O DE ESTOQUE NO DEP?SITO PR?PRIO EM PODER DE TERCEIROS
           SS: 60598/001 - THIAGO.FELIPE*/
         begin
            select estq_005.icdtransdepterceiros, estq_005.tipo_transacao,
                   estq_005.atualiza_estq_terceiro
            into   v_trans_terceiro,              v_tipo_transacao_005,
                   v_atualiza_estq_terceiro
            from estq_005
            where estq_005.codigo_transacao = :old.codigo_transacao
            and ROWNUM = 1;
         exception
             when no_data_found then
                v_trans_terceiro     := 0;
                v_tipo_transacao_005 := 'N';
                v_atualiza_estq_terceiro := 0;
         end;

         /*Verifica deposito relacionado ao terceiro*/
         begin
           select basi_572.cod_dep
           into v_dep_terceiro_572
           from basi_572
           where basi_572.cod_empresa = v_codigo_empresa
             and basi_572.terc_cnpj9  = v_responsavel9
             and basi_572.terc_cnpj4  = v_responsavel4
             and basi_572.terc_cnpj2  = v_responsavel2
             and ROWNUM = 1;
           exception
           when no_data_found then
                v_dep_terceiro_572     := 0;
         end;

         if ( v_dep_terceiro_572 = 0 and  (v_tipo_transacao_005 = 'R' or v_tipo_transacao_005 = 'D' or
              v_tipo_transacao_005 = 'I' or v_tipo_transacao_005 = 'N' or v_atualiza_estq_terceiro = 1) )
            or
            ( v_dep_terceiro_572 <> 0 and  (v_tipo_transacao_005 = 'R' or v_tipo_transacao_005 = 'E' or
              v_tipo_transacao_005 = 'I' or v_atualiza_estq_terceiro = 1) )
         then
            if v_dep_terceiro_572 = 0
            then
               if :old.codigo_deposito = 0 and (v_trans_terceiro > 0 and v_dep_saida_terc >0)
               then
                  v_deposito_terceiro := v_dep_saida_terc;
               elsif :old.codigo_deposito > 0
               then
                  begin
                     select basi_205.icddepdestterceiros into v_deposito_terceiro from basi_205
                     where basi_205.codigo_deposito = :old.codigo_deposito;
                  exception
                      when no_data_found then
                         v_deposito_terceiro := 0;
                  end;
               else
                  begin
                     select 1 into v_cont_processo from hdoc_001
                     where hdoc_001.tipo = 350
                       and hdoc_001.codigo > 0
                       and hdoc_001.codigo2 > 0;
                  exception
                      when no_data_found then
                         v_cont_processo := 0;
                  end;

                  if v_cont_processo > 0
                  then
                     begin
                        select obrf_510.processo_nota, obrf_510.cod_emp_benef,
                               obrf_510.cod_emp_origem
                        into   v_processo_nota,        v_cod_emp_benef,
                               v_cod_emp_origem
                        from obrf_510, (select obrf_510.cod_emp_benef, obrf_510.cod_emp_origem, obrf_510.seq_exec_rotina, max(obrf_510.seq_nota) seq_nota
                                        from obrf_510
                                        where obrf_510.numero_nota       = :new.capa_ent_nrdoc
                                          and obrf_510.serie_nota        = :new.capa_ent_serie
                                          and obrf_510.cgc9_cli_for_nota = :new.capa_ent_forcli9
                                          and obrf_510.cgc4_cli_for_nota = :new.capa_ent_forcli4
                                          and obrf_510.cgc2_cli_for_nota = :new.capa_ent_forcli2
                                        group by obrf_510.cod_emp_benef, obrf_510.cod_emp_origem, obrf_510.seq_exec_rotina) aux_obrf_510
                         where obrf_510.cod_emp_benef   = aux_obrf_510.cod_emp_benef
                           and obrf_510.cod_emp_origem  = aux_obrf_510.cod_emp_origem
                           and obrf_510.seq_exec_rotina = aux_obrf_510.seq_exec_rotina
                           and obrf_510.seq_nota        = aux_obrf_510.seq_nota;
                     exception
                         when no_data_found then
                            v_processo_nota  := 0;
                            v_cod_emp_benef  := 0;
                            v_cod_emp_origem := 0;
                     end;

                     if v_processo_nota > 0 and v_cod_emp_benef > 0 and v_cod_emp_origem > 0
                     then
                        if v_processo_nota = 4
                        then
                           v_tipo_processo := 354;
                        end if;

                        if v_processo_nota = 10
                        then
                           v_tipo_processo := 360;
                        end if;

                        if v_processo_nota = 16
                        then
                           v_tipo_processo := 366;
                        end if;

                        begin
                           select hdoc_001.campo_numerico25,  hdoc_001.campo_numerico27
                           into   v_deposito_terceiro,        v_centro_custo_terceiro
                           from hdoc_001
                           where hdoc_001.tipo    = v_tipo_processo
                             and hdoc_001.codigo  = v_cod_emp_benef
                             and hdoc_001.codigo2 = v_cod_emp_origem;
                        exception
                            when no_data_found then
                               v_deposito_terceiro := 0;
                        end;
                     end if; --if v_processo_nota > 0 and v_cod_emp_benef > 0 and v_cod_emp_origem > 0
                  end if; --if v_cont_processo > 0
               end if; --if :old.codigo_deposito > 0
             else  --  v_dep_terceiro_572 = 0
             v_deposito_terceiro := v_dep_terceiro_572;
             end if;

            if v_trans_terceiro > 0 and v_deposito_terceiro > 0
            then
               begin
                  -- Buscando a data da entrada e situacao da nota fiscal
                  select obrf_010.situacao_entrada
                  into   v_situacao_entrada
                  from obrf_010
                  where obrf_010.documento     = :old.capa_ent_nrdoc
                    and obrf_010.serie         = :old.capa_ent_serie
                    and obrf_010.cgc_cli_for_9 = :old.capa_ent_forcli9
                    and obrf_010.cgc_cli_for_4 = :old.capa_ent_forcli4
                    and obrf_010.cgc_cli_for_2 = :old.capa_ent_forcli2;
               exception
                   when no_data_found then
               v_nr_registro_300 := 0;
                      v_situacao_entrada := 0;
               end;

               v_nr_registro_300 := 0;

               -- Verificando se existe informacoes no cardex para a nota fiscal
               begin
                  select count(*)
                  into v_nr_registro_300
                  from estq_300
                  where numero_documento    = :old.capa_ent_nrdoc
                    and serie_documento     = :old.capa_ent_serie
                    and cnpj_9              = :old.capa_ent_forcli9
                    and cnpj_4              = :old.capa_ent_forcli4
                    and cnpj_2              = :old.capa_ent_forcli2
                    and sequencia_documento = :old.sequencia
                    and codigo_deposito     = v_deposito_terceiro
                    and tabela_origem       = 'OBRF_015';
               exception
                   when no_data_found then
                      v_nr_registro_300 := 0;
               end;

               if v_nr_registro_300 > 0 and v_situacao_entrada in (3,4,5)
               then

                  update  estq_300 set flag_elimina = 1
                  where numero_documento    = :old.capa_ent_nrdoc
                    and serie_documento     = :old.capa_ent_serie
                    and cnpj_9              = :old.capa_ent_forcli9
                    and cnpj_4              = :old.capa_ent_forcli4
                    and cnpj_2              = :old.capa_ent_forcli2
                    and sequencia_documento = :old.sequencia
                    and codigo_deposito     = v_deposito_terceiro
                    and tabela_origem       = 'OBRF_015';

                  -- Enviando informacoes para atualizacao do Cardex
                  delete estq_300
                  where numero_documento    = :old.capa_ent_nrdoc
                    and serie_documento     = :old.capa_ent_serie
                    and cnpj_9              = :old.capa_ent_forcli9
                    and cnpj_4              = :old.capa_ent_forcli4
                    and cnpj_2              = :old.capa_ent_forcli2
                    and sequencia_documento = :old.sequencia
                    and codigo_deposito     = v_deposito_terceiro
                    and tabela_origem       = 'OBRF_015';
               end if;
            end if;                       -- if v_trans_terceiro > 0 and v_deposito_terceiro > 0
         end if;                       -- if v_tipo_transacao_005 = 'R' or v_tipo_transacao_005 = 'D'
      end if;                          -- if deleting
      if :new.nome_programa = 'obrf_f200' -- Confirmacao de nota de entrada
      then
         if inserting and :new.codigo_transacao > 0 and :new.nr_patrimonio_str is null
         then

            -- le a transacao para verificar se ? devolu??o ou retorno, se for dever? zerar as informa??es de nota origem,
            -- para que o fiscal fique correto
            select estq_005.tipo_transacao
            into   v_tipo_transacao
            from estq_005
            where codigo_transacao = :new.codigo_transacao;
            if v_tipo_transacao not in ('D','R')
            then
               :new.num_nota_orig    := 0;
               :new.serie_nota_orig  := '';
               :new.seq_nota_orig    := 0;
            end if;
         end if;
      end if;
   end if;

end inter_tr_obrf_015;
