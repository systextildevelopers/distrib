
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_010_HIST" 
   after insert or delete or update
   on obrf_010
   for each row

-- Finalidade: Registrar alteracoes feitas na tabela obrf_010 - notas fiscais de entrada
-- Autor.....: Aline Blanski
-- Data......: 07/06/10
--
-- Historicos de alteracoes na trigger
--
-- Data      Autor           SS          Observacoes
-- 12/07/10  Aline Blanski   58329/001   Adicionado campos na verificacao da trigger.

declare
   ws_tipo_ocorr                   varchar2 (1);
   ws_usuario_rede                 varchar2(20);
   ws_maquina_rede                 varchar2(40);
   ws_aplicativo                   varchar2(20);
   ws_sid                          number(9);
   ws_empresa                      number(3);
   ws_usuario_systextil            varchar2(250);
   ws_locale_usuario               varchar2(5);
   ws_nome_programa                varchar2(20);
   ws_local_entrega_old            NUMBER(3)    default 0;
   ws_local_entrega_new            NUMBER(3)    default 0 ;
   ws_documento_old                NUMBER(9)    default 0 ;
   ws_documento_new                NUMBER(9)    default 0 ;
   ws_serie_old                    VARCHAR2(3)  default '';
   ws_serie_new                    VARCHAR2(3)  default '';
   ws_especie_docto_old            VARCHAR2(5)  default '';
   ws_especie_docto_new            VARCHAR2(5)  default '';
   ws_data_emissao_old             DATE                   ;
   ws_data_emissao_new             DATE                   ;
   ws_cgc_cli_for_9_old            NUMBER(9)    default 0 ;
   ws_cgc_cli_for_9_new            NUMBER(9)    default 0 ;
   ws_cgc_cli_for_4_old            NUMBER(4)    default 0 ;
   ws_cgc_cli_for_4_new            NUMBER(4)    default 0 ;
   ws_cgc_cli_for_2_old            NUMBER(2)    default 0 ;
   ws_cgc_cli_for_2_new            NUMBER(2)    default 0 ;
   ws_responsavel9_old             NUMBER(9)    default 0 ;
   ws_responsavel9_new             NUMBER(9)    default 0 ;
   ws_responsavel4_old             NUMBER(4)    default 0 ;
   ws_responsavel4_new             NUMBER(4)    default 0 ;
   ws_responsavel2_old             NUMBER(2)    default 0 ;
   ws_responsavel2_new             NUMBER(2)    default 0 ;
   ws_tipo_conhecimento_old        NUMBER(1)    default 0 ;
   ws_tipo_conhecimento_new        NUMBER(1)    default 0 ;
   ws_transpa_forne9_old           NUMBER(9)    default 0 ;
   ws_transpa_forne9_new           NUMBER(9)    default 0 ;
   ws_transpa_forne4_old           NUMBER(4)    default 0 ;
   ws_transpa_forne4_new           NUMBER(4)    default 0 ;
   ws_transpa_forne2_old           NUMBER(2)    default 0 ;
   ws_transpa_forne2_new           NUMBER(2)    default 0 ;
   ws_tarifa_frete_old             NUMBER(5)    default 0 ;
   ws_tarifa_frete_new             NUMBER(5)    default 0 ;
   ws_marca_volumes_old            VARCHAR2(15) default '';
   ws_marca_volumes_new            VARCHAR2(15) default '';
   ws_especie_volumes_old          VARCHAR2(10) default '';
   ws_especie_volumes_new          VARCHAR2(10) default '';
   ws_numero_volume_old            VARCHAR2(18) default '';
   ws_numero_volume_new            VARCHAR2(18) default '';
   ws_qtde_volumes_old             NUMBER(4)    default 0 ;
   ws_qtde_volumes_new             NUMBER(4)    default 0 ;
   ws_peso_bruto_old               NUMBER(12,3) default 0.0;
   ws_peso_bruto_new               NUMBER(12,3) default 0.0;
   ws_peso_liquido_old             NUMBER(12,3) default 0.0;
   ws_peso_liquido_new             NUMBER(12,3) default 0.0;
   ws_tipo_frete_old               NUMBER(1)    default 1  ;
   ws_tipo_frete_new               NUMBER(1)    default 1  ;
   ws_via_transporte_old           NUMBER(1)    default 1  ;
   ws_via_transporte_new           NUMBER(1)    default 1  ;
   ws_condicao_pagto_old           NUMBER(3)    default 0  ;
   ws_condicao_pagto_new           NUMBER(3)    default 0  ;
   ws_selo_inicial_old             NUMBER(12,1) default 0.0;
   ws_selo_inicial_new             NUMBER(12,1) default 0.0;
   ws_selo_final_old               NUMBER(12,1) default 0.0;
   ws_selo_final_new               NUMBER(12,1) default 0.0;
   ws_natoper_nat_oper_old         NUMBER(3)    default 0  ;
   ws_natoper_nat_oper_new         NUMBER(3)    default 0  ;
   ws_natoper_est_oper_old         VARCHAR2(2)  default '' ;
   ws_natoper_est_oper_new         VARCHAR2(2)  default '' ;
   ws_codigo_transacao_old         NUMBER(3)    default 0  ;
   ws_codigo_transacao_new         NUMBER(3)    default 0  ;
   ws_numero_di_old                NUMBER(12)   default 0  ;
   ws_numero_di_new                NUMBER(12)   default 0  ;
   ws_total_docto_old              NUMBER(15,2) default 0.0;
   ws_total_docto_new              NUMBER(15,2) default 0.0;
   ws_valor_total_ipi_old          NUMBER(15,2) default 0.0;
   ws_valor_total_ipi_new          NUMBER(15,2) default 0.0;
   ws_valor_frete_old              NUMBER(15,2) default 0.0;
   ws_valor_frete_new              NUMBER(15,2) default 0.0;
   ws_valor_seguro_old             NUMBER(15,2) default 0.0;
   ws_valor_seguro_new             NUMBER(15,2) default 0.0;
   ws_valor_despesas_old           NUMBER(15,2) default 0.0;
   ws_valor_despesas_new           NUMBER(15,2) default 0.0;
   ws_valor_desconto_old           NUMBER(15,2) default 0.0;
   ws_valor_desconto_new           NUMBER(15,2) default 0.0;
   ws_valor_itens_old              NUMBER(15,2) default 0.0;
   ws_valor_itens_new              NUMBER(15,2) default 0.0;
   ws_base_icms_old                NUMBER(15,2) default 0.0;
   ws_base_icms_new                NUMBER(15,2) default 0.0;
   ws_valor_icms_old               NUMBER(15,2) default 0.0;
   ws_valor_icms_new               NUMBER(15,2) default 0.0;
   ws_base_diferenca_old           NUMBER(15,2) default 0.0;
   ws_base_diferenca_new           NUMBER(15,2) default 0.0;
   ws_historico_cont_old           NUMBER(4)    default 0  ;
   ws_historico_cont_new           NUMBER(4)    default 0  ;
   ws_num_contabil_old             NUMBER(9)    default 0  ;
   ws_num_contabil_new             NUMBER(9)    default 0  ;
   ws_numero_danf_nfe_old          VARCHAR2(44) default '' ;
   ws_numero_danf_nfe_new          VARCHAR2(44) default '' ;
   ws_divisao_qualif_old           VARCHAR2(1)  default '' ;
   ws_divisao_qualif_new           VARCHAR2(1)  default '' ;
   ws_situacao_entrada_old         NUMBER(1)    default 0  ;
   ws_situacao_entrada_new         NUMBER(1)    default 0  ;
   ws_tipo_nf_referenciada_old     NUMBER(1)    default 0  ;
   ws_tipo_nf_referenciada_new     NUMBER(1)    default 0  ;
   ws_nota_referenciada_old        NUMBER(9)    default 0  ;
   ws_nota_referenciada_new        NUMBER(9)    default 0  ;
   ws_serie_referenciada_old       VARCHAR2(3)  default '' ;
   ws_serie_referenciada_new       VARCHAR2(3)  default '' ;
   ws_cod_status_new               VARCHAR2(5)  default ' ';
   ws_cod_status_old               VARCHAR2(5)  default ' ';
   ws_msg_status_new               VARCHAR2(4000)default' ';
   ws_msg_status_old               VARCHAR2(4000)default' ';
   ws_cod_solicitacao_nfe_new      NUMBER(9)     default 0 ;
   ws_cod_solicitacao_nfe_old      NUMBER(9)     default 0 ;
   ws_nr_recibo_new                VARCHAR2(20)  default ' ';
   ws_nr_recibo_old                VARCHAR2(20)  default ' ';
   ws_nr_protocolo_new             VARCHAR2(20)  default ' ';
   ws_nr_protocolo_old             VARCHAR2(20)  default ' ';
   ws_versao_systextilnfe_new      VARCHAR2(15)  default '';
   ws_versao_systextilnfe_old      VARCHAR2(15)  default '';
   ws_chave_contingencia_new       VARCHAR2(36)  default '';
   ws_chave_contingencia_old       VARCHAR2(36)  default '';


begin
   if INSERTING then
      ws_tipo_ocorr                 := 'I';
      ws_local_entrega_old          := 0;
      ws_local_entrega_new          := :new.local_entrega;
      ws_documento_old              := 0;
      ws_documento_new              := :new.documento;
      ws_serie_old                  := null;
      ws_serie_new                  := :new.serie;
      ws_especie_docto_old          := null;
      ws_especie_docto_new          := :new.especie_docto;
      ws_data_emissao_old           := null;
      ws_data_emissao_new           := :new.data_emissao;
      ws_cgc_cli_for_9_old          := 0;
      ws_cgc_cli_for_9_new          := :new.cgc_cli_for_9;
      ws_cgc_cli_for_4_old          := 0;
      ws_cgc_cli_for_4_new          := :new.cgc_cli_for_4;
      ws_cgc_cli_for_2_old          := 0;
      ws_cgc_cli_for_2_new          := :new.cgc_cli_for_2;
      ws_responsavel9_old           := 0;
      ws_responsavel9_new           := :new.responsavel9;
      ws_responsavel4_old           := 0;
      ws_responsavel4_new           := :new.responsavel4;
      ws_responsavel2_old           := 0;
      ws_responsavel2_new           := :new.responsavel2;
      ws_tipo_conhecimento_old      := 0;
      ws_tipo_conhecimento_new      := :new.tipo_conhecimento;
      ws_transpa_forne9_old         := 0;
      ws_transpa_forne9_new         := :new.transpa_forne9;
      ws_transpa_forne4_old         := 0;
      ws_transpa_forne4_new         := :new.transpa_forne4;
      ws_transpa_forne2_old         := 0;
      ws_transpa_forne2_new         := :new.transpa_forne2;
      ws_tarifa_frete_old           := 0;
      ws_tarifa_frete_new           := :new.tarifa_frete;
      ws_marca_volumes_old          := null;
      ws_marca_volumes_new          := :new.marca_volumes;
      ws_especie_volumes_old        := null;
      ws_especie_volumes_new        := :new.especie_volumes;
      ws_numero_volume_old          := null;
      ws_numero_volume_new          := :new.numero_volume;
      ws_qtde_volumes_old           := 0;
      ws_qtde_volumes_new           := :new.qtde_volumes;
      ws_peso_bruto_old             := 0;
      ws_peso_bruto_new             := :new.peso_bruto;
      ws_peso_liquido_old           := 0;
      ws_peso_liquido_new           := :new.peso_liquido;
      ws_tipo_frete_old             := 1;
      ws_tipo_frete_new             := :new.tipo_frete;
      ws_via_transporte_old         := 1;
      ws_via_transporte_new         := :new.via_transporte;
      ws_condicao_pagto_old         := 0;
      ws_condicao_pagto_new         := :new.condicao_pagto;
      ws_selo_inicial_old           := 0;
      ws_selo_inicial_new           := :new.selo_inicial;
      ws_selo_final_old             := 0;
      ws_selo_final_new             := :new.selo_final;
      ws_natoper_nat_oper_old       := 0;
      ws_natoper_nat_oper_new       := :new.natoper_nat_oper;
      ws_natoper_est_oper_old       := null;
      ws_natoper_est_oper_new       := :new.natoper_est_oper;
      ws_codigo_transacao_old       := 0;
      ws_codigo_transacao_new       := :new.codigo_transacao;
      ws_numero_di_old              := 0;
      ws_numero_di_new              := :new.numero_di;
      ws_total_docto_old            := 0;
      ws_total_docto_new            := :new.total_docto;
      ws_valor_total_ipi_old        := 0;
      ws_valor_total_ipi_new        := :new.valor_total_ipi;
      ws_valor_frete_old            := 0;
      ws_valor_frete_new            := :new.valor_frete;
      ws_valor_seguro_old           := 0;
      ws_valor_seguro_new           := :new.valor_seguro;
      ws_valor_despesas_old         := 0;
      ws_valor_despesas_new         := :new.valor_despesas;
      ws_valor_desconto_old         := 0;
      ws_valor_desconto_new         := :new.valor_desconto;
      ws_valor_itens_old            := 0;
      ws_valor_itens_new            := :new.valor_itens;
      ws_base_icms_old              := 0;
      ws_base_icms_new              := :new.base_icms;
      ws_valor_icms_old             := 0;
      ws_valor_icms_new             := :new.valor_icms;
      ws_base_diferenca_old         := 0;
      ws_base_diferenca_new         := :new.base_diferenca;
      ws_historico_cont_old         := 0;
      ws_historico_cont_new         := :new.historico_cont;
      ws_num_contabil_old           := 0;
      ws_num_contabil_new           := :new.num_contabil;
      ws_numero_danf_nfe_old        := null;
      ws_numero_danf_nfe_new        := :new.numero_danf_nfe;
      ws_divisao_qualif_old         := null;
      ws_divisao_qualif_new         := :new.divisao_qualif;
      ws_situacao_entrada_old       := 0;
      ws_situacao_entrada_new       := :new.situacao_entrada;
      ws_tipo_nf_referenciada_old   := 0;
      ws_tipo_nf_referenciada_new   := :new.tipo_nf_referenciada;
      ws_nota_referenciada_old      := 0;
      ws_nota_referenciada_new      := :new.nota_referenciada;
      ws_serie_referenciada_old     := null;
      ws_serie_referenciada_new     := :new.serie_referenciada;
      ws_cod_status_old             := null;
      ws_cod_status_new             := :new.cod_status;
      ws_msg_status_old             := null;
      ws_msg_status_new             := :new.msg_status;
      ws_cod_solicitacao_nfe_old    := 0;
      ws_cod_solicitacao_nfe_new    := :new.cod_solicitacao_nfe;
      ws_nr_recibo_old              := null;
      ws_nr_recibo_new              := :new.nr_recibo;
      ws_nr_protocolo_old           := null;
      ws_nr_protocolo_new           := :new.nr_protocolo;
      ws_versao_systextilnfe_old    := null;
      ws_versao_systextilnfe_new    := :new.versao_systextilnfe;
      ws_chave_contingencia_old     := null;
      ws_chave_contingencia_new     := :new.chave_contingencia;

   elsif UPDATING then

      ws_tipo_ocorr                 := 'A';
      ws_local_entrega_old          := :old.local_entrega;
      ws_local_entrega_new          := :new.local_entrega;
      ws_documento_old              := :old.documento;
      ws_documento_new              := :new.documento;
      ws_serie_old                  := :old.serie;
      ws_serie_new                  := :new.serie;
      ws_especie_docto_old          := :old.especie_docto;
      ws_especie_docto_new          := :new.especie_docto;
      ws_data_emissao_old           := :old.data_emissao;
      ws_data_emissao_new           := :new.data_emissao;
      ws_cgc_cli_for_9_old          := :old.cgc_cli_for_9;
      ws_cgc_cli_for_9_new          := :new.cgc_cli_for_9;
      ws_cgc_cli_for_4_old          := :old.cgc_cli_for_4;
      ws_cgc_cli_for_4_new          := :new.cgc_cli_for_4;
      ws_cgc_cli_for_2_old          := :old.cgc_cli_for_2;
      ws_cgc_cli_for_2_new          := :new.cgc_cli_for_2;
      ws_responsavel9_old           := :old.responsavel9;
      ws_responsavel9_new           := :new.responsavel9;
      ws_responsavel4_old           := :old.responsavel4;
      ws_responsavel4_new           := :new.responsavel4;
      ws_responsavel2_old           := :old.responsavel2;
      ws_responsavel2_new           := :new.responsavel2;
      ws_tipo_conhecimento_old      := :old.tipo_conhecimento;
      ws_tipo_conhecimento_new      := :new.tipo_conhecimento;
      ws_transpa_forne9_old         := :old.transpa_forne9;
      ws_transpa_forne9_new         := :new.transpa_forne9;
      ws_transpa_forne4_old         := :old.transpa_forne4;
      ws_transpa_forne4_new         := :new.transpa_forne4;
      ws_transpa_forne2_old         := :old.transpa_forne2;
      ws_transpa_forne2_new         := :new.transpa_forne2;
      ws_tarifa_frete_old           := :old.tarifa_frete;
      ws_tarifa_frete_new           := :new.tarifa_frete;
      ws_marca_volumes_old          := :old.marca_volumes;
      ws_marca_volumes_new          := :new.marca_volumes;
      ws_especie_volumes_old        := :old.especie_volumes;
      ws_especie_volumes_new        := :new.especie_volumes;
      ws_numero_volume_old          := :old.numero_volume;
      ws_numero_volume_new          := :new.numero_volume;
      ws_qtde_volumes_old           := :old.qtde_volumes;
      ws_qtde_volumes_new           := :new.qtde_volumes;
      ws_peso_bruto_old             := :old.peso_bruto;
      ws_peso_bruto_new             := :new.peso_bruto;
      ws_peso_liquido_old           := :old.peso_liquido;
      ws_peso_liquido_new           := :new.peso_liquido;
      ws_tipo_frete_old             := :old.tipo_frete;
      ws_tipo_frete_new             := :new.tipo_frete;
      ws_via_transporte_old         := :old.via_transporte;
      ws_via_transporte_new         := :new.via_transporte;
      ws_condicao_pagto_old         := :old.condicao_pagto;
      ws_condicao_pagto_new         := :new.condicao_pagto;
      ws_selo_inicial_old           := :old.selo_inicial;
      ws_selo_inicial_new           := :new.selo_inicial;
      ws_selo_final_old             := :old.selo_final;
      ws_selo_final_new             := :new.selo_final;
      ws_natoper_nat_oper_old       := :old.natoper_nat_oper;
      ws_natoper_nat_oper_new       := :new.natoper_nat_oper;
      ws_natoper_est_oper_old       := :old.natoper_est_oper;
      ws_natoper_est_oper_new       := :new.natoper_est_oper;
      ws_codigo_transacao_old       := :old.codigo_transacao;
      ws_codigo_transacao_new       := :new.codigo_transacao;
      ws_numero_di_old              := :old.numero_di;
      ws_numero_di_new              := :new.numero_di;
      ws_total_docto_old            := :old.total_docto;
      ws_total_docto_new            := :new.total_docto;
      ws_valor_total_ipi_old        := :old.valor_total_ipi;
      ws_valor_total_ipi_new        := :new.valor_total_ipi;
      ws_valor_frete_old            := :old.valor_frete;
      ws_valor_frete_new            := :new.valor_frete;
      ws_valor_seguro_old           := :old.valor_seguro;
      ws_valor_seguro_new           := :new.valor_seguro;
      ws_valor_despesas_old         := :old.valor_despesas;
      ws_valor_despesas_new         := :new.valor_despesas;
      ws_valor_desconto_old         := :old.valor_desconto;
      ws_valor_desconto_new         := :new.valor_desconto;
      ws_valor_itens_old            := :old.valor_itens;
      ws_valor_itens_new            := :new.valor_itens;
      ws_base_icms_old              := :old.base_icms;
      ws_base_icms_new              := :new.base_icms;
      ws_valor_icms_old             := :old.valor_icms;
      ws_valor_icms_new             := :new.valor_icms;
      ws_base_diferenca_old         := :old.base_diferenca;
      ws_base_diferenca_new         := :new.base_diferenca;
      ws_historico_cont_old         := :old.historico_cont;
      ws_historico_cont_new         := :new.historico_cont;
      ws_num_contabil_old           := :old.num_contabil;
      ws_num_contabil_new           := :new.num_contabil;
      ws_numero_danf_nfe_old        := :old.numero_danf_nfe;
      ws_numero_danf_nfe_new        := :new.numero_danf_nfe;
      ws_divisao_qualif_old         := :old.divisao_qualif;
      ws_divisao_qualif_new         := :new.divisao_qualif;
      ws_situacao_entrada_old       := :old.situacao_entrada;
      ws_situacao_entrada_new       := :new.situacao_entrada;
      ws_tipo_nf_referenciada_old   := :old.tipo_nf_referenciada;
      ws_tipo_nf_referenciada_new   := :new.tipo_nf_referenciada;
      ws_nota_referenciada_old      := :old.nota_referenciada;
      ws_nota_referenciada_new      := :new.nota_referenciada;
      ws_serie_referenciada_old     := :old.serie_referenciada;
      ws_serie_referenciada_new     := :new.serie_referenciada;
      ws_cod_status_old             := :old.cod_status;
      ws_cod_status_new             := :new.cod_status;
      ws_msg_status_old             := :old.msg_status;
      ws_msg_status_new             := :new.msg_status;
      ws_cod_solicitacao_nfe_old    := :old.cod_solicitacao_nfe;
      ws_cod_solicitacao_nfe_new    := :new.cod_solicitacao_nfe;
      ws_nr_recibo_old              := :old.nr_recibo;
      ws_nr_recibo_new              := :new.nr_recibo;
      ws_nr_protocolo_old           := :old.nr_protocolo;
      ws_nr_protocolo_new           := :new.nr_protocolo;
      ws_versao_systextilnfe_old    := :old.versao_systextilnfe;
      ws_versao_systextilnfe_new    := :new.versao_systextilnfe;
      ws_chave_contingencia_old     := :old.chave_contingencia;
      ws_chave_contingencia_new     := :new.chave_contingencia;

   elsif DELETING then
      ws_tipo_ocorr                 := 'D';
      ws_local_entrega_old          := :old.local_entrega;
      ws_local_entrega_new          := 0;
      ws_documento_old              := :old.documento;
      ws_documento_new              := 0;
      ws_serie_old                  := :old.serie;
      ws_serie_new                  := null;
      ws_especie_docto_old          := :old.especie_docto;
      ws_especie_docto_new          := null;
      ws_data_emissao_old           := :old.data_emissao;
      ws_data_emissao_new           := null;
      ws_cgc_cli_for_9_old          := :old.cgc_cli_for_9;
      ws_cgc_cli_for_9_new          := 0;
      ws_cgc_cli_for_4_old          := :old.cgc_cli_for_4;
      ws_cgc_cli_for_4_new          := 0;
      ws_cgc_cli_for_2_old          := :old.cgc_cli_for_2;
      ws_cgc_cli_for_2_new          := 0;
      ws_responsavel9_old           := :old.responsavel9;
      ws_responsavel9_new           := 0;
      ws_responsavel4_old           := :old.responsavel4;
      ws_responsavel4_new           := 0;
      ws_responsavel2_old           := :old.responsavel2;
      ws_responsavel2_new           := 0;
      ws_tipo_conhecimento_old      := :old.tipo_conhecimento;
      ws_tipo_conhecimento_new      := 0;
      ws_transpa_forne9_old         := :old.transpa_forne9;
      ws_transpa_forne9_new         := 0;
      ws_transpa_forne4_old         := :old.transpa_forne4;
      ws_transpa_forne4_new         := 0;
      ws_transpa_forne2_old         := :old.transpa_forne2;
      ws_transpa_forne2_new         := 0;
      ws_tarifa_frete_old           := :old.tarifa_frete;
      ws_tarifa_frete_new           := 0;
      ws_marca_volumes_old          := :old.marca_volumes;
      ws_marca_volumes_new          := null;
      ws_especie_volumes_old        := :old.especie_volumes;
      ws_especie_volumes_new        := null;
      ws_numero_volume_old          := :old.numero_volume;
      ws_numero_volume_new          := null;
      ws_qtde_volumes_old           := :old.qtde_volumes;
      ws_qtde_volumes_new           := 0;
      ws_peso_bruto_old             := :old.peso_bruto;
      ws_peso_bruto_new             := 0;
      ws_peso_liquido_old           := :old.peso_liquido;
      ws_peso_liquido_new           := 0;
      ws_tipo_frete_old             := :old.tipo_frete;
      ws_tipo_frete_new             := 1;
      ws_via_transporte_old         := :old.via_transporte;
      ws_via_transporte_new         := 1;
      ws_condicao_pagto_old         := :old.condicao_pagto;
      ws_condicao_pagto_new         := 0;
      ws_selo_inicial_old           := :old.selo_inicial;
      ws_selo_inicial_new           := 0;
      ws_selo_final_old             := :old.selo_final;
      ws_selo_final_new             := 0;
      ws_natoper_nat_oper_old       := :old.natoper_nat_oper;
      ws_natoper_nat_oper_new       := 0;
      ws_natoper_est_oper_old       := :old.natoper_est_oper;
      ws_natoper_est_oper_new       := null;
      ws_codigo_transacao_old       := :old.codigo_transacao;
      ws_codigo_transacao_new       := 0;
      ws_numero_di_old              := :old.numero_di;
      ws_numero_di_new              := 0;
      ws_total_docto_old            := :old.total_docto;
      ws_total_docto_new            := 0;
      ws_valor_total_ipi_old        := :old.valor_total_ipi;
      ws_valor_total_ipi_new        := 0;
      ws_valor_frete_old            := :old.valor_frete;
      ws_valor_frete_new            := 0;
      ws_valor_seguro_old           := :old.valor_seguro;
      ws_valor_seguro_new           := 0;
      ws_valor_despesas_old         := :old.valor_despesas;
      ws_valor_despesas_new         := 0;
      ws_valor_desconto_old         := :old.valor_desconto;
      ws_valor_desconto_new         := 0;
      ws_valor_itens_old            := :old.valor_itens;
      ws_valor_itens_new            := 0;
      ws_base_icms_old              := :old.base_icms;
      ws_base_icms_new              := 0;
      ws_valor_icms_old             := :old.valor_icms;
      ws_valor_icms_new             := 0;
      ws_base_diferenca_old         := :old.base_diferenca;
      ws_base_diferenca_new         := 0;
      ws_historico_cont_old         := :old.historico_cont;
      ws_historico_cont_new         := 0;
      ws_num_contabil_old           := :old.num_contabil;
      ws_num_contabil_new           := 0;
      ws_numero_danf_nfe_old        := :old.numero_danf_nfe;
      ws_numero_danf_nfe_new        := null;
      ws_divisao_qualif_old         := :old.divisao_qualif;
      ws_divisao_qualif_new         := 0;
      ws_situacao_entrada_old       := :old.situacao_entrada;
      ws_situacao_entrada_new       := 0;
      ws_tipo_nf_referenciada_old   := :old.tipo_nf_referenciada;
      ws_tipo_nf_referenciada_new   := 0;
      ws_nota_referenciada_old      := :old.nota_referenciada;
      ws_nota_referenciada_new      := 0;
      ws_serie_referenciada_old     := :old.serie_referenciada;
      ws_serie_referenciada_new     := null;
      ws_cod_status_old             := :old.cod_status;
      ws_cod_status_new             := null;
      ws_msg_status_old             := :old.msg_status;
      ws_msg_status_new             := null;
      ws_cod_solicitacao_nfe_old    := :old.cod_solicitacao_nfe;
      ws_cod_solicitacao_nfe_new    := 0;
      ws_nr_recibo_old              := :old.nr_recibo;
      ws_nr_recibo_new              := null;
      ws_nr_protocolo_old           := :old.nr_protocolo;
      ws_nr_protocolo_new           := null;
      ws_versao_systextilnfe_old    := :old.versao_systextilnfe;
      ws_versao_systextilnfe_new    := null;
      ws_chave_contingencia_old     := :old.chave_contingencia;
      ws_chave_contingencia_new     := null;

   end if;

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ws_nome_programa := inter_fn_nome_programa(ws_sid);                         

   INSERT INTO obrf_010_hist
      ( aplicacao,                      tipo_ocorr,
        data_ocorr,
        usuario_rede,                   maquina_rede,
        nome_programa,                  usuario_systextil,
        local_entrega_old,              local_entrega_new,
        documento_old,                  documento_new,
        serie_old,                      serie_new,
        especie_docto_old,              especie_docto_new,
        data_emissao_old,               data_emissao_new,
        cgc_cli_for_9_old,              cgc_cli_for_9_new,
        cgc_cli_for_4_old,              cgc_cli_for_4_new,
        cgc_cli_for_2_old,              cgc_cli_for_2_new,
        responsavel9_old,               responsavel9_new,
        responsavel4_old,               responsavel4_new,
        responsavel2_old,               responsavel2_new,
        tipo_conhecimento_old,          tipo_conhecimento_new,
        transpa_forne9_old,             transpa_forne9_new,
        transpa_forne4_old,             transpa_forne4_new,
        transpa_forne2_old,             transpa_forne2_new,
        tarifa_frete_old,               tarifa_frete_new,
        marca_volumes_old,              marca_volumes_new,
        especie_volumes_old,            especie_volumes_new,
        numero_volume_old,              numero_volume_new,
        qtde_volumes_old,               qtde_volumes_new,
        peso_bruto_old,                 peso_bruto_new,
        peso_liquido_old,               peso_liquido_new,
        tipo_frete_old,                 tipo_frete_new,
        via_transporte_old,             via_transporte_new,
        condicao_pagto_old,             condicao_pagto_new,
        selo_inicial_old,               selo_inicial_new,
        selo_final_old,                 selo_final_new,
        natoper_nat_oper_old,           natoper_nat_oper_new,
        natoper_est_oper_old,           natoper_est_oper_new,
        codigo_transacao_old,           codigo_transacao_new,
        numero_di_old,                  numero_di_new,
        total_docto_old,                total_docto_new,
        valor_total_ipi_old,            valor_total_ipi_new,
        valor_frete_old,                valor_frete_new,
        valor_seguro_old,               valor_seguro_new,
        valor_despesas_old,             valor_despesas_new,
        valor_desconto_old,             valor_desconto_new,
        valor_itens_old,                valor_itens_new,
        base_icms_old,                  base_icms_new,
        valor_icms_old,                 valor_icms_new,
        base_diferenca_old,             base_diferenca_new,
        historico_cont_old,             historico_cont_new,
        num_contabil_old,               num_contabil_new,
        numero_danf_nfe_old,            numero_danf_nfe_new,
        divisao_qualif_old,             divisao_qualif_new,
        situacao_entrada_old,           situacao_entrada_new,
        tipo_nf_referenciada_old,       tipo_nf_referenciada_new,
        nota_referenciada_old,          nota_referenciada_new,
        serie_referenciada_old,         serie_referenciada_new,
        cod_status_old,                 cod_status_new,
        msg_status_old,                 msg_status_new,
        cod_solicitacao_nfe_old,        cod_solicitacao_nfe_new,
        nr_recibo_old,                  nr_recibo_new,
        nr_protocolo_old,               nr_protocolo_new,
        versao_systextilnfe_old,        versao_systextilnfe_new,
        chave_contingencia_old,         chave_contingencia_new
      )
   VALUES
      ( ws_aplicativo,                     ws_tipo_ocorr,
        sysdate,
        ws_usuario_rede,                   ws_maquina_rede,
        ws_nome_programa,                  ws_usuario_systextil,
        ws_local_entrega_old,              ws_local_entrega_new,
        ws_documento_old,                  ws_documento_new,
        ws_serie_old,                      ws_serie_new,
        ws_especie_docto_old,              ws_especie_docto_new,
        ws_data_emissao_old,               ws_data_emissao_new,
        ws_cgc_cli_for_9_old,              ws_cgc_cli_for_9_new,
        ws_cgc_cli_for_4_old,              ws_cgc_cli_for_4_new,
        ws_cgc_cli_for_2_old,              ws_cgc_cli_for_2_new,
        ws_responsavel9_old,               ws_responsavel9_new,
        ws_responsavel4_old,               ws_responsavel4_new,
        ws_responsavel2_old,               ws_responsavel2_new,
        ws_tipo_conhecimento_old,          ws_tipo_conhecimento_new,
        ws_transpa_forne9_old,             ws_transpa_forne9_new,
        ws_transpa_forne4_old,             ws_transpa_forne4_new,
        ws_transpa_forne2_old,             ws_transpa_forne2_new,
        ws_tarifa_frete_old,               ws_tarifa_frete_new,
        ws_marca_volumes_old,              ws_marca_volumes_new,
        ws_especie_volumes_old,            ws_especie_volumes_new,
        ws_numero_volume_old,              ws_numero_volume_new,
        ws_qtde_volumes_old,               ws_qtde_volumes_new,
        ws_peso_bruto_old,                 ws_peso_bruto_new,
        ws_peso_liquido_old,               ws_peso_liquido_new,
        ws_tipo_frete_old,                 ws_tipo_frete_new,
        ws_via_transporte_old,             ws_via_transporte_new,
        ws_condicao_pagto_old,             ws_condicao_pagto_new,
        ws_selo_inicial_old,               ws_selo_inicial_new,
        ws_selo_final_old,                 ws_selo_final_new,
        ws_natoper_nat_oper_old,           ws_natoper_nat_oper_new,
        ws_natoper_est_oper_old,           ws_natoper_est_oper_new,
        ws_codigo_transacao_old,           ws_codigo_transacao_new,
        ws_numero_di_old,                  ws_numero_di_new,
        ws_total_docto_old,                ws_total_docto_new,
        ws_valor_total_ipi_old,            ws_valor_total_ipi_new,
        ws_valor_frete_old,                ws_valor_frete_new,
        ws_valor_seguro_old,               ws_valor_seguro_new,
        ws_valor_despesas_old,             ws_valor_despesas_new,
        ws_valor_desconto_old,             ws_valor_desconto_new,
        ws_valor_itens_old,                ws_valor_itens_new,
        ws_base_icms_old,                  ws_base_icms_new,
        ws_valor_icms_old,                 ws_valor_icms_new,
        ws_base_diferenca_old,             ws_base_diferenca_new,
        ws_historico_cont_old,             ws_historico_cont_new,
        ws_num_contabil_old,               ws_num_contabil_new,
        ws_numero_danf_nfe_old,            ws_numero_danf_nfe_new,
        ws_divisao_qualif_old,             ws_divisao_qualif_new,
        ws_situacao_entrada_old,           ws_situacao_entrada_new,
        ws_tipo_nf_referenciada_old,       ws_tipo_nf_referenciada_new,
        ws_nota_referenciada_old,          ws_nota_referenciada_new,
        ws_serie_referenciada_old,         ws_serie_referenciada_new,
        ws_cod_status_old,                 ws_cod_status_new,
        ws_msg_status_old,                 ws_msg_status_new,
        ws_cod_solicitacao_nfe_old,        ws_cod_solicitacao_nfe_new,
        ws_nr_recibo_old,                  ws_nr_recibo_new,
        ws_nr_protocolo_old,               ws_nr_protocolo_new,
        ws_versao_systextilnfe_old,        ws_versao_systextilnfe_new,
        ws_chave_contingencia_old,         ws_chave_contingencia_new

      );

end inter_tr_obrf_010_hist;
-- ALTER TRIGGER "INTER_TR_OBRF_010_HIST" ENABLE
 
/

exec inter_pr_recompile;

