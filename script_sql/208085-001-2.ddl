-- Create Table
create table PEDI_807
(
  COD_REPRESENTANTE NUMBER(5) not null,
  COND_PGT_CLIENTE        NUMBER(3) not null,
  CGC_9             NUMBER(9) default 0 not null,
  CGC_4             NUMBER(4) default 0 not null,
  CGC_2             NUMBER(2) default 0 not null
);

-- Create/Recreate primary, unique and foreign key constraints 
alter table PEDI_807
add constraint PK_PEDI_807 primary key (COD_REPRESENTANTE, COND_PGT_CLIENTE, CGC_9, CGC_4, CGC_2);

alter table PEDI_807
  add constraint REF_PEDI_807_PEDI_020 foreign key (COD_REPRESENTANTE)
  references PEDI_020 (COD_REP_CLIENTE);
  
alter table PEDI_807
  add constraint REF_PEDI_807_PEDI_010 foreign key (CGC_9, CGC_4, CGC_2)
  references PEDI_010 (CGC_9, CGC_4, CGC_2);
  
alter table PEDI_807
  add constraint REF_PEDI_807_PEDI_070 foreign key (COND_PGT_CLIENTE)
  references PEDI_070 (COND_PGT_CLIENTE);

  exec inter_pr_recompile;
