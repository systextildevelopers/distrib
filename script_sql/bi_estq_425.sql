create or replace trigger bi_estq_425
before insert on estq_425
for each row
begin
    :new.id := estq_425_seq.nextval;
end;
