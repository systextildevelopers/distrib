create table obrf_062
(
CODIGO_EMPRESA                 NUMBER(3)     default 0,                                                                                                                                                
MES_REFERENCIA                 NUMBER(2)     default 0,     
ANO_REFERENCIA                 NUMBER(4)     default 0,                                                              
II_TIPO_DECLARACAO             NUMBER(1)     default 1,  
TIPO_REGISTRO                  VARCHAR2(2)   default '',
QUADRO                         VARCHAR2(2)   default '',
soma_VLR_DEVI_FUMDES           NUMBER(17,2)  default 0,
SALDO_CRED_FUMDES_ANT        NUMBER(17,2)  default 0,
SOMA_VLR_FUMDES_DEVOL          NUMBER(17,2)  default 0,
SALDO_CRED_FUMDES_SEG        NUMBER(17,2)  default 0,
FUMDES_RECOLHER                NUMBER(17,2)  default 0,
SOMA_VLR_DEVI_FUNDO_SOCIAL     NUMBER(17,2)  default 0,
SALDO_CRED_FUNDO_SOCIAL_ANT  NUMBER(17,2)  default 0,
SOMA_VLR_FUNDO_SOCIAL_DEVOL    NUMBER(17,2)  default 0,
SALDO_CRED_FUNDO_SOCIAL_SEG  NUMBER(17,2)  default 0,
FUNDO_SOCIAL_RECOLHER          NUMBER(17,2)  default 0);

ALTER TABLE obrf_062
ADD CONSTRAINT OBRF_062_PK PRIMARY KEY (CODIGO_EMPRESA,MES_REFERENCIA,ANO_REFERENCIA,II_TIPO_DECLARACAO,TIPO_REGISTRO,QUADRO);

COMMENT ON COLUMN obrf_062.CODIGO_EMPRESA IS 'código da empresa';  
COMMENT ON COLUMN obrf_062.MES_REFERENCIA IS 'mes de referência';                                                                                                                                            
COMMENT ON COLUMN obrf_062.ANO_REFERENCIA IS 'ano de referência';    
