create table cont_k201(
  exercicio 				number(9) 		default 0	not null ,
  cod_controladora 			number(9) 		default 0	not null ,
  cod_plano_controladora 	number(9) 		default 0	not null ,
  cta_controladora 			varchar2(20) 				default '',
  cod_plano_controlada 		number(9) 		default 0	not null ,
  cod_red_controlada 		number(9) 		default 0	not null ,
  cta_controlada 			varchar2(20) 				default '');

alter table cont_k201
   ADD CONSTRAINT PK_cont_k201 PRIMARY KEY (exercicio,cod_controladora,cod_plano_controladora,cta_controladora,cod_plano_controlada,cod_red_controlada);
