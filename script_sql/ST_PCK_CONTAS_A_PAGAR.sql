create or replace package ST_PCK_CONTAS_A_PAGAR as 

function baixar_titulo(p_nr_duplicata     in number,     p_parcela          in varchar2, p_tipo_titulo     in number,
                       p_cgc_9            in number,     p_cgc_4            in number,   p_cgc_2           in number,
                       p_transacao        in out number, p_cod_portador     in number,   p_nr_conta_porta  in number,
                       p_cod_contab_deb   in out number, p_empresa          in number,   p_centro_custo    in number,
                       p_pago_adiantam    in number,     p_data_baixa       in date,     p_valor_pago      in number,   
                       p_cod_historico    in number,     p_numero_documento in number,   p_data_pgmt       in date,     
                       p_sequencia_pagto  in number,     p_prg_gerador      in varchar2, p_usuario         in varchar2) return varchar2;
                       
procedure criar_titulo(
        p_nr_duplicata IN number,          p_parcela IN varchar2,             p_cgc9 IN number,                  
        p_cgc4 IN number,                  p_cgc2 IN number,                  p_tipo_titulo IN number,      
        p_codigo_empresa IN number,        p_cod_end_cobranca IN number,      p_emitente_titulo IN varchar2,     
        p_documento IN number,             p_serie IN varchar2,               p_data_contrato IN date,
        p_data_vencimento IN date,         p_data_transacao IN date,          p_codigo_depto IN number,          
        p_cod_portador IN number,          p_codigo_historico IN number,      p_codigo_transacao IN OUT number,
        p_posicao_titulo IN number,        p_codigo_contabil IN number,       p_tipo_pagamento IN number,        
        p_moeda_titulo IN number,          p_origem_debito IN varchar2,       p_previsao IN number,
        p_valor_parcela IN number,         p_valor_parcela_moeda IN number,   p_base_irrf_moeda IN varchar2,     
        --INICIO IMPOSTO
        p_base_irrf IN varchar2,           p_cod_ret_irrf IN varchar2,        p_aliq_irrf IN varchar2,
        p_valor_irrf_moeda IN number,      p_valor_irrf IN number,            p_base_iss IN number,              
        p_cod_ret_iss IN number,           p_aliq_iss IN number,              p_valor_iss IN number,
        p_base_inss IN number,             p_cod_ret_inss IN number,          p_aliq_inss IN number,             
        p_valor_inss_imp IN number,        p_base_pis IN number,              p_cod_ret_pis IN number,
        p_aliq_pis IN number,              p_valor_pis_imp IN number,         p_base_cofins IN number,           
        p_cod_ret_cofins IN number,        p_aliq_cofins IN number,           p_valor_cofins_imp IN number,
        p_base_csl IN number,              p_cod_ret_csl IN number,           p_aliq_csl IN number,              
        p_valor_csl_imp IN number,         p_base_csrf IN number,             p_cod_ret_csrf IN number,
        p_aliq_csrf IN number,             p_valor_csrf_imp IN number,
        --FIM IMPOSTO
        p_codigo_barras IN varchar2,       p_projeto IN number,               p_subprojeto IN number,            
        p_servico IN number,               p_nr_processo_export IN number,    p_nr_processo_import IN number,
        p_cod_cancelamento IN number,      p_data_canc_tit IN varchar2,       p_num_lcmt IN OUT number,        
        p_usuario_logado IN varchar2,      P_CGC9_FAVORECIDO IN NUMBER,       P_CGC4_FAVORECIDO IN NUMBER,
        P_CGC2_FAVORECIDO IN NUMBER,       P_BANCO_SISPAG    IN NUMBER,       P_AGENCIA_SISPAG  IN NUMBER,
        P_DIG_AGE_SISPAG  IN NUMBER,       P_CONTA_SISPAG    IN NUMBER,       P_DIG_CTA_SISPAG2 IN VARCHAR2,
        p_num_importacao IN varchar2,      p_prg_gerador     IN VARCHAR2,     p_usuario         IN VARCHAR2,
        p_success IN OUT varchar2,         p_des_erro        OUT varchar2);
   
   
   procedure criar_titulo_juros(
        p_nr_duplicata IN number,          p_parcela IN varchar2,             p_cgc9 IN number,                  
        p_cgc4 IN number,                  p_cgc2 IN number,                  p_tipo_titulo IN number,      
        p_codigo_empresa IN number,        p_cod_end_cobranca IN number,      p_emitente_titulo IN varchar2,     
        p_documento IN number,             p_serie IN varchar2,               p_data_contrato IN date,
        p_data_vencimento IN date,         p_data_transacao IN date,          p_codigo_depto IN number,          
        p_cod_portador IN number,          p_codigo_historico IN number,      p_codigo_transacao IN OUT number,
        p_posicao_titulo IN number,        p_codigo_contabil IN number,       p_tipo_pagamento IN number,        
        p_moeda_titulo IN number,          p_origem_debito IN varchar2,       p_previsao IN number,
        p_valor_parcela IN number,         p_valor_parcela_moeda IN number,   p_base_irrf_moeda IN varchar2,     
        --INICIO IMPOSTO
        p_base_irrf IN varchar2,           p_cod_ret_irrf IN varchar2,        p_aliq_irrf IN varchar2,
        p_valor_irrf_moeda IN number,      p_valor_irrf IN number,            p_base_iss IN number,              
        p_cod_ret_iss IN number,           p_aliq_iss IN number,              p_valor_iss IN number,
        p_base_inss IN number,             p_cod_ret_inss IN number,          p_aliq_inss IN number,             
        p_valor_inss_imp IN number,        p_base_pis IN number,              p_cod_ret_pis IN number,
        p_aliq_pis IN number,              p_valor_pis_imp IN number,         p_base_cofins IN number,           
        p_cod_ret_cofins IN number,        p_aliq_cofins IN number,           p_valor_cofins_imp IN number,
        p_base_csl IN number,              p_cod_ret_csl IN number,           p_aliq_csl IN number,              
        p_valor_csl_imp IN number,         p_base_csrf IN number,             p_cod_ret_csrf IN number,
        p_aliq_csrf IN number,             p_valor_csrf_imp IN number,
        --FIM IMPOSTO
        p_codigo_barras IN varchar2,       p_projeto IN number,               p_subprojeto IN number,            
        p_servico IN number,               p_nr_processo_export IN number,    p_nr_processo_import IN number,
        p_cod_cancelamento IN number,      p_data_canc_tit IN varchar2,       p_num_lcmt IN OUT number,        
        p_usuario_logado IN varchar2,      P_CGC9_FAVORECIDO IN NUMBER,       P_CGC4_FAVORECIDO IN NUMBER,
        P_CGC2_FAVORECIDO IN NUMBER,       P_BANCO_SISPAG    IN NUMBER,       P_AGENCIA_SISPAG  IN NUMBER,
        P_DIG_AGE_SISPAG  IN NUMBER,       P_CONTA_SISPAG    IN NUMBER,       P_DIG_CTA_SISPAG2 IN VARCHAR2,
        p_num_importacao IN varchar2,      p_prg_gerador     IN VARCHAR2,     p_usuario         IN VARCHAR2,
        p_juros in number,                 p_success IN OUT varchar2,         p_des_erro        OUT varchar2);

   TYPE t_valida_titulo IS RECORD (
        validar_fornecedor BOOLEAN,
        validar_tipo_titulo BOOLEAN,
        validar_cotacao BOOLEAN,
        validar_codigo_moeda BOOLEAN,
        validar_valor BOOLEAN,
        validar_empresa BOOLEAN
    );
   
   procedure criar_titulo_cpag_010(
        p_nr_duplicata IN number,          p_parcela IN varchar2,             p_cgc9 IN number,                  
        p_cgc4 IN number,                  p_cgc2 IN number,                  p_tipo_titulo IN number,      
        p_codigo_empresa IN number,        p_cod_end_cobranca IN number,      p_emitente_titulo IN varchar2,     
        p_documento IN number,             p_serie IN varchar2,               p_data_contrato IN date,
        p_data_vencimento IN date,         p_data_transacao IN date,          p_codigo_depto IN number,          
        p_cod_portador IN number,          p_codigo_historico IN number,      p_codigo_transacao IN OUT number,
        p_posicao_titulo IN number,        p_codigo_contabil IN number,       p_tipo_pagamento IN number,        
        p_moeda_titulo IN number,          p_origem_debito IN varchar2,       p_previsao IN number,
        p_valor_parcela IN number,         p_valor_parcela_moeda IN number,   p_base_irrf_moeda IN varchar2,     
        --INICIO IMPOSTO
        p_base_irrf IN varchar2,           p_cod_ret_irrf IN varchar2,        p_aliq_irrf IN varchar2,
        p_valor_irrf_moeda IN number,      p_valor_irrf IN number,            p_base_iss IN number,              
        p_cod_ret_iss IN number,           p_aliq_iss IN number,              p_valor_iss IN number,
        p_base_inss IN number,             p_cod_ret_inss IN number,          p_aliq_inss IN number,             
        p_valor_inss_imp IN number,        p_base_pis IN number,              p_cod_ret_pis IN number,
        p_aliq_pis IN number,              p_valor_pis_imp IN number,         p_base_cofins IN number,           
        p_cod_ret_cofins IN number,        p_aliq_cofins IN number,           p_valor_cofins_imp IN number,
        p_base_csl IN number,              p_cod_ret_csl IN number,           p_aliq_csl IN number,              
        p_valor_csl_imp IN number,         p_base_csrf IN number,             p_cod_ret_csrf IN number,
        p_aliq_csrf IN number,             p_valor_csrf_imp IN number,
        --FIM IMPOSTO
        p_codigo_barras IN varchar2,       p_projeto IN number,               p_subprojeto IN number,            
        p_servico IN number,               p_nr_processo_export IN number,    p_nr_processo_import IN number,
        p_cod_cancelamento IN number,      p_data_canc_tit IN varchar2,       p_num_lcmt IN OUT number,        
        p_usuario_logado IN varchar2,      P_CGC9_FAVORECIDO IN NUMBER,       P_CGC4_FAVORECIDO IN NUMBER,
        P_CGC2_FAVORECIDO IN NUMBER,       P_BANCO_SISPAG    IN NUMBER,       P_AGENCIA_SISPAG  IN NUMBER,
        P_DIG_AGE_SISPAG  IN NUMBER,       P_CONTA_SISPAG    IN NUMBER,       P_DIG_CTA_SISPAG2 IN VARCHAR2,
        p_num_importacao IN varchar2,      p_prg_gerador     IN VARCHAR2,     p_usuario         IN VARCHAR2,
        p_juros in number,                 p_id_titulo_cpag_narwal IN NUMBER, p_success IN OUT varchar2,         
        p_des_erro        OUT varchar2);

    
    FUNCTION valida_titulo(p_valida_titulo t_valida_titulo, p_codigo_empresa int, p_nr_duplicata int, p_seq_duplicata int, p_tipo_titulo int, p_cnpj9 int, p_cnpj4 int, p_cnpj2 int, p_cotacao int, p_valor int, p_codigo_moeda int, p_campo_erro_valida out varchar2) RETURN VARCHAR2;

   
   FUNCTION next_duplicata(p_cnpj9 IN NUMBER, p_cnpj4 IN NUMBER, p_cnpj2 IN NUMBER, p_tipo_titulo IN NUMBER) RETURN number;
end;
