create or replace trigger "INTER_TR_PCPT_026" 
   before insert or delete
   or update of
            rolo_fio_per_tece, rolo_fio_nr_rolo, fio_rolo_nivel99,    fio_rolo_grupo,
            fio_rolo_subgrupo, fio_rolo_item,    lote_fio,            quantidade_fio,
            valor_fio,         codigo_rolo,      estagio,             deposito,
            data_cardex,       transacao_cardex, usuario_cardex,      nome_programa,
            sequencia_fio,     rolo_consumo,     centro_custo,        executa_trigger
   on pcpt_026
   for each row

declare
   v_entrada_saida        estq_005.entrada_saida%type;
   v_transacao_cancel     estq_005.codigo_transacao%type;

   v_numero_documento     estq_300.numero_documento   %type := 0;
   v_serie_documento      estq_300.serie_documento    %type := ' ';
   v_sequencia_documento  estq_300.sequencia_documento%type := 0;
   v_qtde_movimento       estq_300.quantidade         %type := 0;
   v_cnpj_9               estq_300.cnpj_9             %type := 0;
   v_cnpj_4               estq_300.cnpj_4             %type := 0;
   v_cnpj_2               estq_300.cnpj_2             %type := 0;
   v_tabela_origem_cardex estq_300.tabela_origem      %type := 'PCPT_026';
   v_valor_unitario_fio                               number:= 0.00;
   v_executa_trigger      number;
   
   v_ordem_producao_sped  estq_300.numero_op%type := 0;
   v_estagio_op_sped      estq_300.estagio_op%type := 0;
   v_tipo_ordem           estq_300.tipo_ordem%type := 0;
   
   v_data_cardex          pcpt_020.data_cardex%type;
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting or updating
      then
         if  updating                                       --- Alterando registro
         and :old.valor_fio > 0.00                          --- Executada uma baixa anterior
         and (:new.quantidade_fio < :old.quantidade_fio)    --- Tentando diminuir qtde ja movimentada
         and :new.nome_programa <> 'pcpt_f200'
         then
            raise_application_error(-20000,'N?o podemos diminuir quantidades j? movimentadas de fios.');
         end if;

         -- Atualizando o numero do documento da movimenta??o de baixa de fios com
         -- numero do rolo que foi utilizado para esta baixa.
         v_numero_documento  := :new.codigo_rolo;
         
         if :new.valor_fio > 0.00
         then
            if  updating                                       --- Alterando registro
            and :old.valor_fio > 0.00                          --- Executada uma baixa anterior
            and (:new.quantidade_fio > :old.quantidade_fio)    --- Atualizado a quantidade do movimento
            and :new.nome_programa <> 'pcpt_f200'
            then
               v_qtde_movimento := :new.quantidade_fio - :old.quantidade_fio;
            else
               v_qtde_movimento := :new.quantidade_fio;
            end if;  

            if :new.transacao_cardex = 0 and :new.nome_programa <> 'pcpt_f200'
            then
               raise_application_error(-20000,'Deve-se informar a transa??o de movimenta??o.');
            else
               select entrada_saida into v_entrada_saida from estq_005
               where codigo_transacao = :new.transacao_cardex; 

               if v_entrada_saida <> 'S'
               and :new.nome_programa <> 'pcpt_f200'
               then
                  raise_application_error(-20000,'Transacao deve ser de sa?da.');
               end if;
            end if;

            if :new.data_cardex is null and :new.nome_programa <> 'pcpt_f200'
            then
               raise_application_error(-20000,'Deve-se informar a data de movimenta??o.');
            end if;

           -- Calculando o valor unitario da movimenta??o para grava??o no Cardex
            if :new.quantidade_fio > 0
            then
               v_valor_unitario_fio := :new.valor_fio / :new.quantidade_fio;
            else
               v_valor_unitario_fio := 0;
            end if;
            
            v_ordem_producao_sped := 0;
            v_estagio_op_sped := 0;
            v_tipo_ordem := 0;
            v_data_cardex :=  :new.data_cardex;
            
            if :new.nome_programa is null 
            or :new.nome_programa <> 'pcpt_f200' then
               begin
                  select pcpt_020.ordem_producao,  pcpt_020.data_prod_tecel
                  into   v_ordem_producao_sped,    v_data_cardex
                  from pcpt_020
                  where pcpt_020.codigo_rolo = :new.codigo_rolo;
               exception when no_data_found then
                  v_ordem_producao_sped := 0;
               end;
               
               begin
                  select ESTAGIO_TECELAGEM 
                  into   v_estagio_op_sped
                  from empr_001;
                  
                  if v_estagio_op_sped is null 
                  or v_estagio_op_sped = 0
                  then
                     raise_application_error(-20000,'O estágio da tecelagem não está configurado nos parâmentros de empresa da tecelagem.');
                  end if;
               end;
               
               v_tipo_ordem := 4;
            end if;
            
            if v_data_cardex is null then
                v_data_cardex := sysdate;
            end if;
            
            if :new.nome_programa is null or :new.nome_programa <> 'pcpt_f200'
            then
               inter_pr_insere_estq_300_recur (:new.deposito,          :new.fio_rolo_nivel99,   :new.fio_rolo_grupo,
                                               :new.fio_rolo_subgrupo, :new.fio_rolo_item,      v_data_cardex,
                                               :new.lote_fio,          v_numero_documento,      v_serie_documento,
                                               v_cnpj_9,               v_cnpj_4,                v_cnpj_2,
                                               v_sequencia_documento,
                                               :new.transacao_cardex,  'S',                     :new.centro_custo,
                                               v_qtde_movimento,       v_valor_unitario_fio,    v_valor_unitario_fio,
                                               :new.usuario_cardex,    v_tabela_origem_cardex,  :new.nome_programa,
                                               0.00,                   v_ordem_producao_sped,   0,
                                               0,                      v_tipo_ordem,            v_estagio_op_sped);
            end if;
         end if; -- if :new.valor_fio > 0
      end if;     -- if inserting or updating

      if deleting
      then
         if :old.valor_fio > 0.00
         then
            select tran_est_matpri4 
            into v_transacao_cancel 
            from empr_001;
            
            -- Atualizando o numero do documento da movimenta??o de baixa de fios com
            -- numero do rolo que foi utilizado para esta baixa.
            v_numero_documento  := :old.codigo_rolo;

            -- Calculando o valor unitario da movimenta??o para grava??o no Cardex
            if :old.quantidade_fio > 0
            then
               v_valor_unitario_fio := :old.valor_fio / :old.quantidade_fio;
            else
               v_valor_unitario_fio := 0;
            end if;
            
            v_ordem_producao_sped := 0;
            v_estagio_op_sped     := 0;
            v_tipo_ordem          := 0;
            v_data_cardex         := sysdate;
            
            if :new.nome_programa is null 
            or :new.nome_programa <> 'pcpt_f200' then
               begin
                  select pcpt_020.ordem_producao,  pcpt_020.data_prod_tecel
                  into   v_ordem_producao_sped,    v_data_cardex
                  from pcpt_020
                  where pcpt_020.codigo_rolo = :old.codigo_rolo;
               exception when no_data_found then
                  v_ordem_producao_sped := 0;
               end;
               
               begin
                  select ESTAGIO_TECELAGEM 
                  into   v_estagio_op_sped
                  from empr_001;
                  
                  if v_estagio_op_sped is null 
                  or v_estagio_op_sped = 0
                  then
                     raise_application_error(-20000,'O estágio da tecelagem não está configurado nos parâmentros de empresa da tecelagem.');
                  end if;
               end;
               
               v_tipo_ordem := 4;
            end if;
            
            if v_data_cardex is null then
                v_data_cardex := sysdate;
            end if;
            
            inter_pr_insere_estq_300_recur (:old.deposito,          :old.fio_rolo_nivel99,   :old.fio_rolo_grupo,
                                            :old.fio_rolo_subgrupo, :old.fio_rolo_item,      v_data_cardex,
                                            :old.lote_fio,          v_numero_documento,      v_serie_documento,
                                            v_cnpj_9,               v_cnpj_4,                v_cnpj_2,
                                            v_sequencia_documento,
                                            v_transacao_cancel,     'E',                     :old.centro_custo,
                                            :old.quantidade_fio,    v_valor_unitario_fio,    v_valor_unitario_fio,
                                            :new.usuario_cardex,    v_tabela_origem_cardex,  :new.nome_programa,
                                            0.00,                   v_ordem_producao_sped,   0,
                                            0,                      v_tipo_ordem,            v_estagio_op_sped);
         end if;  -- if :new.valor_fio > 0.00
      end if;     -- if deleting
      
      -- zera campos de controle da ficha cardex, assim n?o h? perigo da trigger atualizar
      -- o estoque com dados antigos, caso o usuario n?o passar estes parametros
      if inserting or updating
      then
         :new.transacao_cardex     := 0;
         :new.usuario_cardex       := ' ';
         :new.nome_programa        := ' ';
         :new.data_cardex          := null;
      end if;
   end if;
end;
