declare
cursor basi030 is 
       select f.rowid from basi_030 f
       where unidade_medida_comercial is null;
       
 contaRegistro number;

begin
       contaRegistro := 0;
       
       for reg_basi030 in basi030
       loop
           update basi_030
           set unidade_medida_comercial = basi_030.unidade_medida
           where basi_030.rowid = reg_basi030.rowid;
           
           contaRegistro := contaRegistro +1;
                    
          if contaRegistro = 100
          then
              contaRegistro := 0 ;
              commit;
          end if;           
       end loop;
       
       commit;
end;

/

exec inter_pr_recompile;
