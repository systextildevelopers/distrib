CREATE OR REPLACE PACKAGE BODY ST_PCK_ENCONTRA_CONTA AS

    function executar(  p_nome_form varchar,            p_cod_empresa number,
                        p_data_lanc date,               p_tipo_contab number,
                        p_cod_contabil number,          p_transacao number,
                        p_c_custo number,               p_data_lcto_doc date,
                        p_mdi_empresa number,           p_mdi_usuario varchar2,
                        p_mdi_codigo_usuario varchar2,  p_msg_erro in out varchar2) return number
    AS
        v_empresa_matriz number := 0;
        v_retorno_get_conta number;
        v_array_empr ST_PCK_EMPRESAS.t_dados_fatu_500;
        v_array_exercicio ST_PCK_EXERCICIO.t_dados_exercicio;
        v_exist_array boolean;
        v_msg_erro varchar2(4000);
    BEGIN

    --verifica se a empresa gera contabilizacao
        v_array_empr := ST_PCK_EMPRESAS.get_fatu_500(p_cod_empresa, v_msg_erro);
        if not v_array_empr.exists(1) then
            v_msg_erro := 'Empresa n?o gera contabilizac?o: Empresa-'|| p_cod_empresa;
            p_msg_erro := v_msg_erro;
            return -99;
        end if;

        if v_array_empr(1).gera_contabil <> 1 then
            v_msg_erro := 'Empresa n?o gera contabilizac?o: Empresa-'|| p_cod_empresa || ' Parametro contabil: '
                                                                    || v_array_empr(1).gera_contabil;
            p_msg_erro := v_msg_erro;
            return -99;
        end if;

        if not ST_PCK_TRANSACAO.transacao_gera_contabilizacao(p_transacao, true, p_nome_form) then
            v_msg_erro := 'Transac?o n?o gera contabilizac?o!' || ' p_transacao '|| p_transacao;
            p_msg_erro := v_msg_erro;
            return -99;
        end if;

        --Daqui para a frente so quando a empresa gera contabilizac?o e a transac?o tambem.
        --acha a matriz da empresa CODIGO_HISTORICO
        if ( v_array_empr(1).codigo_empresa <> 0 ) then
            v_empresa_matriz := v_array_empr(1).codigo_matriz;
        end if;

        --encontra o exercicio do lancamento atual
        if p_nome_form = 'cpag_f089' and p_data_lanc = null then
            v_msg_erro := 'N?o encontrou data de lancamento atual! e necessario para o processo cpag_f089';
            p_msg_erro := v_msg_erro;
            return -99;
        end if;

        v_array_exercicio := ST_PCK_EXERCICIO.valida_exercicio(     v_empresa_matriz,
                                                                    p_data_lanc,
                                                                    ST_PCK_CHECA_DATA.get_regras(p_nome_form),
                                                                    v_msg_erro);

        if not v_array_exercicio.exists(1) then
            p_msg_erro := v_msg_erro;
            return -99;
        end if;

        v_msg_erro := '';
        v_retorno_get_conta := -99;

        begin
            v_retorno_get_conta := get_conta(   p_nome_form,          p_cod_empresa,
                                                v_array_exercicio,    p_tipo_contab,
                                                p_cod_contabil,       p_transacao,
                                                p_c_custo,            p_mdi_empresa,
                                                p_mdi_usuario,        p_mdi_codigo_usuario,
                                                v_msg_erro);
             p_msg_erro := v_msg_erro;
        exception when others then
            p_msg_erro := SQLERRM;
        end;

        return v_retorno_get_conta;
    exception when others then
        p_msg_erro := SQLERRM;
        return -99;
    END executar;

  function get_conta( p_nome_form varchar2,                                   p_cod_empresa number,
                        p_array_exercicio ST_PCK_EXERCICIO.t_dados_exercicio,   p_tipo_contab number,
                        p_cod_contabil number,                                  p_transacao number,
                        p_c_custo number,                                       p_mdi_empresa number,
                        p_mdi_usuario varchar2,                                 p_mdi_codigo_usuario varchar2,
                        p_msg_erro in out varchar2) return number
    AS
        v_exists number;
        v_conta_contab number;
        v_tmp_int number;
        v_exibir_mensagens boolean;
        v_msg_erro varchar2(4000) := '';
    BEGIN
        v_exibir_mensagens := exibe_mensagens_erro(p_nome_form, p_tipo_contab);
        v_conta_contab := procurar_relac_contabil(p_tipo_contab, p_cod_contabil, p_transacao, p_array_exercicio, v_msg_erro);

        if v_msg_erro is not null or v_conta_contab = 0 then
            if v_exibir_mensagens = true then
                inter_pr_insere_hist_001(   p_mdi_empresa,          p_mdi_usuario,
                                            p_mdi_codigo_usuario,   'CONT_550',
                                            0,                      'INSERT (1)',
                                            p_nome_form,            0,
                                            0,                      v_msg_erro);
            end if;

            p_msg_erro := v_msg_erro;
        end if;

        if v_conta_contab > 0 then
            begin
                v_msg_erro := '';
                v_tmp_int := validar_pelo_plano_de_contas( p_cod_empresa,  v_conta_contab,
                                                        p_c_custo,      p_array_exercicio,
                                                        v_msg_erro);
                p_msg_erro := v_msg_erro;
            exception when
            others then
                p_msg_erro := SQLERRM;
            end;
        end if;

        return v_conta_contab;
    END get_conta;

  function procurar_relac_contabil(p_tipo_contab number,  p_cod_contabil number,
                                     p_transacao number,    p_array_exercicio ST_PCK_EXERCICIO.t_dados_exercicio,
                                     p_msg_erro in out varchar) return number
    AS
        v_exists number;
        v_custom_error varchar2(4000);
        v_msg_erro varchar2(4000);
        v_array_relac_contab ST_PCK_RELAC_CONTABIL.t_dados_cont_550;
        v_array_plano_contas_item ST_PCK_PLANO_CONTAS_ITEM.t_dados_cont_535;
    BEGIN
        v_msg_erro := '';

        v_array_relac_contab := ST_PCK_RELAC_CONTABIL.get(  p_array_exercicio(1).cod_plano_cta,     p_tipo_contab,
                                                            p_cod_contabil,                             p_transacao,
                                                            v_msg_erro);

        if v_array_relac_contab.exists(1) then
            return v_array_relac_contab(1).conta_contabil;
        end if;

        v_array_relac_contab := ST_PCK_RELAC_CONTABIL.get(  p_array_exercicio(1).cod_plano_cta,     p_tipo_contab,
                                                            999999,                                     p_transacao,
                                                            v_msg_erro);
        if v_array_relac_contab.exists(1) then
            if v_array_relac_contab(1).conta_contabil = 99999 then
                v_array_plano_contas_item := ST_PCK_PLANO_CONTAS_ITEM.get_by_cod_reduzido(  p_array_exercicio(1).cod_plano_cta,
                                                                                            p_cod_contabil, v_msg_erro);
                if not v_array_plano_contas_item.exists(1) then
                    return v_array_plano_contas_item(1).cod_reduzido;
                end if;

                p_msg_erro := p_msg_erro || 'Conta contabil n?o consta no plano de contas!';
            end if;
        end if;

        p_msg_erro := p_msg_erro || 'Parametros do exercicio contabil n?o encontrado \n
                                            ANOTE AS MENSAGENS A SEGUIR E PASSE PARA O CONTADOR \n
                                            EMPRESA: ' || p_array_exercicio(1).cod_empresa ||'\n'||
                                            'EXERCICIO: ' || p_array_exercicio(1).exercicio ||'\n' ||
                                            'PLANO DE CONTAS: ' || p_array_exercicio(1).cod_plano_cta ||'\n'||
                                            'TIPO CONTABIL: '|| p_tipo_contab || '\n' ||
                                            'CODIGO CONTABIL: ' || p_cod_contabil || '\n' ||
                                            'TRANSACAO: ' || p_transacao;

        return 0;
    exception when others then
        p_msg_erro := SQLERRM;
        return 0;
    END procurar_relac_contabil;

  function esta_ok(p_c_custo number, p_array_plano_contas_item ST_PCK_PLANO_CONTAS_ITEM.t_dados_cont_535) return boolean
    AS
        v_exists number;
        p_array_centro_custo ST_PCK_CENTRO_CUSTO.t_dados_basi_185;
        v_msg_erro varchar2(4000);
    BEGIN
    --NATUREZA DA CONTA E C.CUSTO NAO SAO DE DESPESAS NEM CUSTOS
        if p_array_plano_contas_item(1).tipo_natureza <> 1 and p_array_plano_contas_item(1).tipo_natureza <> 2 then
            return true;
        end if;

        p_array_centro_custo := ST_PCK_CENTRO_CUSTO.get_dados_ccusto(p_c_custo, v_msg_erro);

        if p_array_centro_custo.exists(1) then
            return (p_array_plano_contas_item(1).tipo_natureza = 1 and p_array_centro_custo(1).custo_despesa = 1)
                or--NATUREZA DA CONTA E C.CUSTO SAO DE CUSTOS
                (p_array_plano_contas_item(1).tipo_natureza = 2 and p_array_centro_custo(1).custo_despesa = 2);
                --NATUREZA DA CONTA E C.CUSTO SAO DE DESPESAS
        else
            return false;
        end if;


    END esta_ok;

  function validar_tipo_natu_conta(   p_cod_empresa number, p_conta_contab number,
                                        p_c_custo number,     p_array_plano_contas_item ST_PCK_PLANO_CONTAS_ITEM.t_dados_cont_535,
                                        p_msg_erro in out varchar2 ) return boolean
    AS
        v_exists number;
        v_custom_error varchar2(4000);
        v_array_fatu_503 ST_PCK_EMPRESAS.t_dados_fatu_503;
        v_consiste_natureza boolean := true;
        v_msg_erro varchar2(4000) := '';
    BEGIN
        v_array_fatu_503 := ST_PCK_EMPRESAS.get_fatu_503(p_cod_empresa, v_msg_erro);

        if v_array_fatu_503.exists(1) then
            if v_array_fatu_503(1).consiste_tipo_natureza <> 0 then
                v_consiste_natureza := false;
            end if;
        end if;

        if  (   ( v_consiste_natureza = false )
        or      (esta_ok(p_c_custo, p_array_plano_contas_item) = false ) ) then
            p_msg_erro := 'ATENC?O! Tipo da natureza da conta contabil e diferente do centro de custos. ' ||
                                'CONTA ENCONTRADA: '|| p_conta_contab || '\n' ||
                                'CENTRO_CUSTO: ' || p_c_custo || '\n';
        end if;

    return true;
    END validar_tipo_natu_conta;

    function validar_pelo_plano_de_contas(  p_cod_empresa number,   p_conta_contab number,
                                            p_c_custo number,       p_array_exercicio ST_PCK_EXERCICIO.t_dados_exercicio,
                                            p_msg_erro in out varchar) return number
    AS
        v_exists number;
        v_c_custo_lanc number;
        v_cod_reduzido number := 0;
        v_conta_mae number := 0;
        v_conta_tem_relac boolean := false;
        v_registro_ok boolean := false;
        v_tem_retorno boolean := false;
        v_msg_erro varchar2(4000) := '';

        v_array_plano_contas_item ST_PCK_PLANO_CONTAS_ITEM.t_dados_cont_535;
        v_array_centro_custo ST_PCK_RELAC_CONTABIL_CC.t_dados_cont_555;
    BEGIN
        v_array_plano_contas_item := ST_PCK_PLANO_CONTAS_ITEM.get_by_cod_reduzido(  p_array_exercicio(1).cod_plano_cta,
                                                                                    p_conta_contab, v_msg_erro);
        if not v_array_plano_contas_item.exists(1) then
            p_msg_erro := 'A conta contabil encontrada n?o esta no plano de contas. \n'||
                            ' Conta encontrada: ' || p_conta_contab;
            return -99;
        end if;

        v_msg_erro := '';
        v_registro_ok := validar_tipo_natu_conta(   p_cod_empresa,  p_conta_contab,
                                                    p_c_custo,      v_array_plano_contas_item,
                                                    v_msg_erro);
        if v_msg_erro is not null then
            p_msg_erro := v_msg_erro;
            return -99;
        end if;

        if v_array_plano_contas_item(1).tipo_conta = 2 then
            p_msg_erro := 'A conta contabil encontrada n?o e analitica. \n'||
                            ' Conta encontrada: ' || p_conta_contab;
            return -99;
        end if;

        if v_array_plano_contas_item(1).patr_result <> 2 then
            return NULL;
        end if;

        if v_array_plano_contas_item(1).exige_subconta = 2 then
            v_c_custo_lanc := 0;
        else
            v_c_custo_lanc := p_c_custo;
        end if;

        v_registro_ok := false;
        v_conta_tem_relac := false;
        v_cod_reduzido := p_conta_contab;

        while v_cod_reduzido <> 0 and v_registro_ok = false
        loop
            v_msg_erro := '';
            v_array_plano_contas_item := ST_PCK_PLANO_CONTAS_ITEM.get_by_cod_reduzido(  p_array_exercicio(1).cod_plano_cta,
                                                                                        v_cod_reduzido, v_msg_erro);
            if v_msg_erro is null then
                v_cod_reduzido := 0;
                v_conta_mae := 0;
                v_tem_retorno := false;
            else
                v_cod_reduzido := v_array_plano_contas_item(1).cod_reduzido;
                v_conta_mae := v_array_plano_contas_item(1).conta_mae;
                v_tem_retorno := true;
            end if;

            if v_cod_reduzido > 0 then
                v_tem_retorno := false;

                v_array_centro_custo := ST_PCK_RELAC_CONTABIL_CC.get_dados_cont_555(p_array_exercicio(1).cod_plano_cta,
                                                                                    v_cod_reduzido,
                                                                                    v_msg_erro);

                for i in 1..v_array_centro_custo.count loop
                    v_tem_retorno := true;

                    if v_array_centro_custo(i).centro_custo = v_c_custo_lanc then
                        v_registro_ok := true;
                    else
                        v_conta_tem_relac := true;
                    end if;
                end loop;
            end if;

            if v_tem_retorno = false or v_conta_tem_relac = true then
                v_cod_reduzido := v_conta_mae;
            end if;
        end loop;

        if v_registro_ok = false
        and v_conta_tem_relac = true then
            p_msg_erro := 'Conta Contabil n?o relacionada ao Centro de Custo informado!. '||
                            'parametro: ' || v_c_custo_lanc;
            return -99;
        end if;

    return 0;
    END validar_pelo_plano_de_contas;

  function exibe_mensagens_erro(p_nome_form varchar2, p_tipo_contab number) return boolean
    AS
        v_exists number;
    BEGIN
    return p_nome_form is not null  and p_nome_form not like '%inte%'
                                        and p_nome_form not in ('cont_f820', 'obrf_f135', 'estq_e142', 'cont_f660', 'estq_e325')
                                        and p_tipo_contab <> 12;
    END exibe_mensagens_erro;

END ST_PCK_ENCONTRA_CONTA;
/
