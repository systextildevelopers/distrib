
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_750_LOG" 
   after insert
      or delete
      or update
         of CODIGO_PROJETO,
            NIVEL,                   GRUPO,
            SUBGRUPO,                ITEM,
            SEQUENCIA,               CAMINHO_IMAGEM,
            TIPO_IMAGEM,             ALTERNATIVA,
            IMAGEM_DEFAULT
   on basi_750
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_desc_prod_n            varchar2(120);
   ws_narrativa              basi_010.narrativa%type;
   ws_desc_projeto_n         varchar2(60);

   long_aux                  varchar2(2000);

   flag_controle             number(1);
   imagem_def                varchar2(3);
   imagem_def_old            varchar2(3);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting or updating
   then

      if inserting
      then
         -- DESCRICAO DO PROJETO
         begin
            select basi_001.descricao_projeto
            into   ws_desc_projeto_n
            from basi_001
            where basi_001.codigo_projeto = :new.codigo_projeto;
         exception when no_data_found then
            ws_desc_projeto_n := ' ';
         end;
      end if;

      -- DESCRICAO DO PRODUTO
      inter_pr_desc_produto(:new.nivel,
                            :new.grupo,
                            :new.subgrupo,
                            :new.item,
                            ws_desc_prod_n,
                            ws_narrativa);

   end if;

   if inserting
   then
      if :new.imagem_default = 0
      then
         imagem_def := 'NAO';
      else
         imagem_def :='SIM';
      end if;
      begin
         INSERT INTO hist_100 (
            tabela,             operacao,
            data_ocorr,         aplicacao,
            usuario_rede,       maquina_rede,
            str01,              num05,
            str07,              str08,
            str09,              str10,
            long01
         ) VALUES (
            'BASI_750',         'I',
            sysdate,            ws_aplicativo,
            ws_usuario_rede,    ws_maquina_rede,
            :new.codigo_projeto, 0,
            :new.nivel,         :new.grupo,
            :new.subgrupo,      :new.item,
             '               '||
            inter_fn_buscar_tag('lb39204#CADASTRO / VISUALIZACAO DA IMAGEM DO PROJETO',ws_locale_usuario,ws_usuario_systextil) ||
            chr(10)                                          ||
            inter_fn_buscar_tag('lb39197#SEQUENCIA',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || to_char(:new.sequencia)  ||
            chr(10)                                          ||
            inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.codigo_projeto    ||
              ' - '                       || ws_desc_projeto_n      ||
            chr(10)                                                 ||
            inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.nivel             ||
              '.'                         || :new.grupo             ||
              '.'                         || :new.subgrupo          ||
              '.'                         || :new.item              ||
              ' - '                       || ws_desc_prod_n         ||
              chr(10)                                               ||
            inter_fn_buscar_tag('lb29927#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || to_char(:new.alternativa) ||
            chr(10)                                           ||
            inter_fn_buscar_tag('lb39198#CAMINHO DA IMAGEM:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.caminho_imagem    ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb39199#IMAGEM DEFAULT:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || imagem_def             ||
            chr(10)                                              ||
            inter_fn_buscar_tag('lb39201#TIPO IMAGEM:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || to_char(:new.tipo_imagem)
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(001-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   if updating
   then
      long_aux := long_aux ||
        '                 '||
      inter_fn_buscar_tag('lb39204#CADASTRO / VISUALIZACAO DA IMAGEM DO PROJETO',ws_locale_usuario,ws_usuario_systextil) ||
      chr(10)                    ||
      chr(10);

      flag_controle := 0;

      if  :old.alternativa <> :new.alternativa
      then
         flag_controle := 1;
         long_aux := long_aux ||
           inter_fn_buscar_tag('lb29927#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                   || :old.alternativa   || ' -> '
                                   || :new.alternativa
                                   || chr(10);
      end if;

      if  (:old.nivel    <> :new.nivel)    or
          (:old.grupo    <> :new.grupo)    or
          (:old.subgrupo <> :new.subgrupo) or
          (:old.item     <> :new.item)
      then
         flag_controle := 1;
         long_aux := long_aux ||
            inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                    || :old.nivel    || '.'
                                    || :old.grupo    || '.'
                                    || :old.subgrupo || '.'
                                    || :old.item     || ' -> '
                                    || :new.nivel    || '.'
                                    || :new.grupo    || '.'
                                    || :new.subgrupo || '.'
                                    || :new.item
                                    || chr(10);
      end if;

      if :old.caminho_imagem <> :new.caminho_imagem
      then
         flag_controle := 1;
         long_aux := long_aux ||
            inter_fn_buscar_tag('lb39198#CAMINHO DA IMAGEM:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                   || :old.caminho_imagem   || ' -> '
                                   || :new.caminho_imagem
                                   || chr(10);
      end if;

      if :old.imagem_default <> :new.imagem_default
      then
         flag_controle := 1;

      if :new.imagem_default = 0
      then
         imagem_def := 'NAO';
         imagem_def_old := 'SIM';
      else
         imagem_def :='SIM';
         imagem_def_old := 'NAO';
      end if;

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb39199#IMAGEM DEFAULT:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                   || imagem_def_old || ' -> '
                                   || imagem_def
                                   || chr(10);
      end if;

      if :old.tipo_imagem <> :new.tipo_imagem
      then
         flag_controle := 1;
         long_aux := long_aux ||
            inter_fn_buscar_tag('lb39201#TIPO IMAGEM:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                   || :old.tipo_imagem || ' -> '
                                   || :new.tipo_imagem
                                   || chr(10);
      end if;

      if flag_controle = 1
      then
         begin
            INSERT INTO hist_100 (
               tabela,               operacao,
               data_ocorr,           aplicacao,
               usuario_rede,         maquina_rede,
               str01,                num05,
               str07,                str08,
               str09,                str10,
               long01
            ) VALUES (
              'BASI_750',           'A',
               sysdate,              ws_aplicativo,
               ws_usuario_rede,      ws_maquina_rede,
               :new.codigo_projeto,  0,
               :new.nivel,         :new.grupo,
               :new.subgrupo,      :new.item,
               long_aux
            );
         exception when OTHERS then
            raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(001-2)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;

   if deleting
   then
      -- DESCRICAO DO PROJETO
      begin
         select basi_001.descricao_projeto
         into   ws_desc_projeto_n
         from basi_001
         where basi_001.codigo_projeto = :old.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_n := '';
      end;

      if :old.imagem_default = 0
      then
         imagem_def := 'NAO';
      else
         imagem_def :='SIM';
      end if;

      -- DESCRICAO DO PRODUTO
      inter_pr_desc_produto(:old.nivel,
                            :old.grupo,
                            :old.subgrupo,
                            :old.item,
                            ws_desc_prod_n,
                            ws_narrativa);

      begin
         INSERT INTO hist_100 (
            tabela,             operacao,
            data_ocorr,         aplicacao,
            usuario_rede,       maquina_rede,
            str01,              num05,
            str07,              str08,
            str09,              str10,
            long01
         ) VALUES (
            'BASI_750',         'D',
            sysdate,            ws_aplicativo,
            ws_usuario_rede,    ws_maquina_rede,
            :old.codigo_projeto, 0,
            :old.nivel,         :old.grupo,
            :old.subgrupo,      :old.item,
             '               '||
            inter_fn_buscar_tag('lb39204#CADASTRO / VISUALIZACAO DA IMAGEM DO PROJETO',ws_locale_usuario,ws_usuario_systextil) ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb39197#SEQUENCIA',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || to_char(:old.sequencia)  ||
            chr(10)                                        ||
              inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :old.codigo_projeto    ||
              ' - '                       || ws_desc_projeto_n      ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :old.nivel             ||
              '.'                         || :old.grupo             ||
              '.'                         || :old.subgrupo          ||
              '.'                         || :old.item              ||
              ' - '                       || ws_desc_prod_n         ||
              chr(10)                                               ||
            inter_fn_buscar_tag('lb29927#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || to_char(:old.alternativa) ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb39198#CAMINHO DA IMAGEM:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :old.caminho_imagem    ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb39199#IMAGEM DEFAULT:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || imagem_def ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb39201#TIPO IMAGEM:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || to_char(:old.tipo_imagem)
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(001-3)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;
end inter_tr_basi_750_log;
-- ALTER TRIGGER "INTER_TR_BASI_750_LOG" ENABLE
 

/

exec inter_pr_recompile;

