create table rcnb_856
(
  CODIGO_EMPRESA        NUMBER(3) default 0 not null,
  MES                   NUMBER(2) default 0 not null,
  ANO                   NUMBER(4) default 0 not null,
  NIVEL_ESTRUTURA       VARCHAR2(1) default ' ' not null,
  GRUPO_ESTRUTURA       VARCHAR2(5) default ' ' not null,
  SUBGRU_ESTRUTURA      VARCHAR2(3) default ' ' not null,
  ITEM_ESTRUTURA        VARCHAR2(6) default ' ' not null,
  ORDEM_LEITURA         NUMBER(3) default 0 not null,
  NIVEL_EXPLOSAO        NUMBER(1) default 0 not null,
  NIVEL_COMPONENTE      VARCHAR2(1) default ' ' not null,
  GRUPO_COMPONENTE      VARCHAR2(5) default ' ' not null,
  SUBGRU_COMPONENTE     VARCHAR2(3) default ' ' not null,
  ITEM_COMPONENTE       VARCHAR2(6) default ' ' not null,
  NR_VERSAO             NUMBER(6) default 0 not null,
  CONSUMO               NUMBER(17,10) default 0.00,
  PRECO_COMP            NUMBER(15,6) default 0.00,
  CUSTO_MP              NUMBER(15,6) default 0.00,
  ALTERNATIVA_ESTRUTURA NUMBER(2) default 1 not null,
  flag_gerou_tabela     NUMBER(1) default 0
);


alter table rcnb_856
  add constraint PK_RCNB_856 primary key (MES, ANO, CODIGO_EMPRESA, NIVEL_ESTRUTURA, GRUPO_ESTRUTURA, SUBGRU_ESTRUTURA, ITEM_ESTRUTURA, NIVEL_EXPLOSAO, ORDEM_LEITURA, NR_VERSAO, NIVEL_COMPONENTE, GRUPO_COMPONENTE, SUBGRU_COMPONENTE, ITEM_COMPONENTE, ALTERNATIVA_ESTRUTURA);

create synonym systextilrpt.rcnb_856 for rcnb_856; 

exec inter_pr_recompile;
/
