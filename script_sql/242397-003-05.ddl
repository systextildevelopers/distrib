-- Create table 
drop table PCPT_072_LOG;
create table PCPT_072_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(20) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(20) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  id_OLD                       NUMBER ,  
  id_NEW                       NUMBER , 
  definicao_OLD                VARCHAR2(200) ,  
  definicao_NEW                VARCHAR2(200) , 
  qtde_metros_de_OLD           NUMBER,  
  qtde_metros_de_NEW           NUMBER, 
  qtde_metros_ate_OLD          NUMBER,  
  qtde_metros_ate_NEW          NUMBER, 
  unidade_medida_embalagem_OLD VARCHAR2(10) ,  
  unidade_medida_embalagem_NEW VARCHAR2(10) , 
  descricao_da_embalagem_OLD   VARCHAR2(400), 
  descricao_da_embalagem_NEW   VARCHAR2(400) 
) ;

