alter table supr_580 add situacao number(1) default 1;
alter table supr_580 add perc_icms number(6,2) default 0.00;
alter table supr_580 add perc_pis number(6,2) default 0.00;
alter table supr_580 add perc_cofins number(6,2) default 0.00;
alter table supr_580 add perc_st number(6,2) default 0.00;
alter table supr_580 add imposto_unico number(6,2) default 0.00;
