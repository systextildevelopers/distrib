
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_013" 
   after insert
      or delete
      or update
      of nivel_comp,        grupo_comp,
         subgru_comp,       item_comp,
         codigo_projeto,    sequencia_subprojeto,
         nivel_item,        grupo_item,
         subgru_item,       item_item,
         alternativa_produto
on basi_013 -- Estrutura do produto de Projeto
for each row

declare
   cliente9              basi_001.cnpj_cliente9%type;
   cliente4              basi_001.cnpj_cliente4%type;
   cliente2              basi_001.cnpj_cliente2%type;
   v_nro_reg             number;

begin
/*   if updating
   then
      if :new.subgru_comp <> :old.subgru_comp
      then
         begin
            select nvl(count(1),0)
            into v_nro_reg
            from basi_021
            where basi_021.codigo_projeto      = :new.codigo_projeto
              and basi_021.nivel_item          = :new.nivel_item
              and basi_021.grupo_item          = :new.grupo_item
              and basi_021.item_item           = :new.item_item
              and basi_021.alternativa_produto = :new.alternativa_produto
              and basi_021.sequencia_estrutura = :new.sequencia_estrutura;
         end;

         if  :new.subgru_comp <> '000'
         and :old.subgru_comp =  '000'
         and v_nro_reg        > 0
         then
            begin
               delete from basi_021
               where basi_021.codigo_projeto      = :new.codigo_projeto
                 and basi_021.nivel_item          = :new.nivel_item
                 and basi_021.grupo_item          = :new.grupo_item
                 and basi_021.item_item           = :new.item_item
                 and basi_021.alternativa_produto = :new.alternativa_produto
                 and basi_021.sequencia_estrutura = :new.sequencia_estrutura;
            end;
         end if;
      end if;

      if :new.subgru_comp <> :old.subgru_comp
      then
         begin
            select nvl(count(1),0)
            into v_nro_reg
            from basi_021
            where basi_021.codigo_projeto      = :new.codigo_projeto
              and basi_021.nivel_item          = :new.nivel_item
              and basi_021.grupo_item          = :new.grupo_item
              and basi_021.subgru_item         = :new.subgru_item
              and basi_021.alternativa_produto = :new.alternativa_produto
              and basi_021.sequencia_estrutura = :new.sequencia_estrutura;
         end;

         if  :new.item_comp <> '000000'
         and :old.item_comp =  '000000'
         and v_nro_reg        > 0
         then
            begin
               delete from basi_021
               where basi_021.codigo_projeto      = :new.codigo_projeto
                 and basi_021.nivel_item          = :new.nivel_item
                 and basi_021.grupo_item          = :new.grupo_item
                 and basi_021.subgru_item         = :new.subgru_item
                 and basi_021.alternativa_produto = :new.alternativa_produto
                 and basi_021.sequencia_estrutura = :new.sequencia_estrutura;
            end;
         end if;
      end if;
   end if;
*/
   /*
      Quando for iserido um novo componente na estrutura do desenvolvimento de produtos
      sera validado o componente e sera inserido na tabela de situacao do componente
   */
   if inserting or updating
   then
      begin
         /*
            Seleciona o cliente da capa do projeto
         */
         select nvl(basi_001.cnpj_cliente9,0),   nvl(basi_001.cnpj_cliente4,0),
                nvl(basi_001.cnpj_cliente2,0)
         into   cliente9,                        cliente4,
                cliente2
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when OTHERS then
         raise_application_error (-20000, 'Projeto nao cadastrado');
      end;

      if :new.seq_cor <> ' '
      or :new.codigo_desenho <> ' '
      then
         inter_pr_limpar_aprov_comp(:new.codigo_projeto,         :new.sequencia_subprojeto,
                                    :new.nivel_item,             :new.grupo_item,
                                    :new.subgru_item,            :new.item_item,
                                    :new.alternativa_produto,
                                    cliente9,                    cliente4,
                                    cliente2);

         /*
            Se for informada a sequencia de cor e a estrutura ainda nao tem destino...
         */
         if :new.seq_cor <> ' '
         then
--         raise_application_error (-20000, 'inter_pr_exp_seq_cor');

            inter_pr_exp_seq_cor(:new.codigo_projeto,         :new.sequencia_subprojeto,
                                 :new.nivel_item,             :new.grupo_item,
                                 :new.subgru_item,            :new.item_item,
                                 :new.alternativa_produto,
                                 :new.nivel_comp,             :new.grupo_comp,
                                 :new.subgru_comp,            :new.item_comp,
                                 cliente9,                    cliente4,
                                 cliente2,
                                 :new.seq_cor);
         end if;

         /*
            Se for informado o codigo do desenho e a estrutura ainda nao tem destino...
         */
         if :new.codigo_desenho <> ' '
         then
            inter_pr_exp_desenho(:new.codigo_projeto,         :new.sequencia_subprojeto,
                                 :new.nivel_item,             :new.grupo_item,
                                 :new.subgru_item,            :new.item_item,
                                 :new.alternativa_produto,
                                 :new.nivel_comp,             :new.grupo_comp,
                                 :new.subgru_comp,            :new.item_comp,
                                 cliente9,                    cliente4,
                                 cliente2,
                                 :new.codigo_desenho,
                                 :new.sequencia_estrutura,    :new.sequencia_variacao);
         end if;
      else
         /*
             Somente para estruturas com o componente completo.
         */
         if  :new.nivel_comp  is not null and :new.nivel_comp  <> '0'
         and :new.grupo_comp  is not null and :new.grupo_comp  <> '00000'
         and :new.subgru_comp is not null and :new.subgru_comp <> '000'
         and :new.item_comp   is not null and :new.item_comp   <> '000000'
         then
            inter_pr_inserir_basi_027_028 (:new.nivel_comp,
                                           :new.grupo_comp,
                                           :new.subgru_comp,
                                           :new.item_comp,
                                           :new.codigo_projeto,
                                           :new.sequencia_subprojeto,
                                           :new.nivel_item,
                                           :new.grupo_item,
                                           :new.subgru_item,
                                           :new.item_item,
                                           :new.alternativa_produto,
                                           cliente9,
                                           cliente4,
                                           cliente2);
         end if;
      end if;
   end if;
end inter_tr_basi_013;

-- ALTER TRIGGER "INTER_TR_BASI_013" ENABLE
 

/

exec inter_pr_recompile;

