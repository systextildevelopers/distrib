alter table oper_001 add TITULO varchar2(255);

comment on column oper_001.TITULO is 'Título do processo agendado. Se for deixado em branco será usado o nome da tela.';
