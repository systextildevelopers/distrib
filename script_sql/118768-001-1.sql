create or replace view inter_vi_pedi_100_faturamento as
select pedi_100.codigo_empresa,
       pedi_100.pedido_venda,
       pedi_100.data_entr_venda,
       pedi_100.qtde_total_pedi,
       pedi_100.valor_liq_itens as valor_pedido,
       pedi_100.qtde_saldo_pedi,
       pedi_100.valor_saldo_pedi,
       pedi_100.numero_controle,
       pedi_100.sugestao_impressa,
       pedi_100.prioridade,
       pedi_100.status_comercial,
       pedi_100.liberado_faturar,
       pedi_100.observacao,
       pedi_100.natop_pv_nat_oper,
       pedi_100.natop_pv_est_oper,
       pedi_100.cli_ped_cgc_cli9,
       pedi_100.cli_ped_cgc_cli4,
       pedi_100.cli_ped_cgc_cli2,
       pedi_010.nome_cliente as descr_cliente,
       pedi_100.desconto1,
       pedi_100.desconto2,
       pedi_100.desconto3,
       pedi_100.trans_pv_forne9,
       pedi_100.trans_pv_forne4,
       pedi_100.trans_pv_forne2,
       supr_010.nome_fornecedor as nome_transportadora,
       pedi_100.cod_rep_cliente,
       pedi_020.nome_rep_cliente as nome_representante,
       pedi_100.codigo_administr,
       pedi_020a.nome_rep_cliente as nome_gerente,
       pedi_100.colecao_tabela,
       pedi_100.mes_tabela,
       pedi_100.sequencia_tabela,
       pedi_090.descricao as descricao_tabela,
       pedi_100.cond_pgto_venda,
       pedi_070.descr_pg_cliente as descr_condicao_pagto,
       decode(pedi_100.permite_parcial,0,'N�O', 'SIM') permite_parcial,
       decode(pedi_100.aceita_antecipacao,0,'N�O', 'SIM') aceita_antecipacao,
       nvl(pedi_110a.qtde_faturada,0) as qtde_faturada,
       inter_fn_calc_desconto_triplo((pedi_110a.valor_faturad),pedi_100.desconto1,pedi_100.desconto2,pedi_100.desconto3) as valor_faturado,
       nvl(pcpc_320a.qtde_coletada,0) as qtde_coletada,
      (select nvl(inter_fn_calc_desconto_triplo(nvl(sum(pcpc_325.qtde_pecas_real*( sum(pedi_110.valor_unitario - ((pedi_110.valor_unitario * pedi_110.percentual_desc) / 100)))),0),pedi_100.desconto1,pedi_100.desconto2, pedi_100.desconto3),0)
       from pedi_110,pcpc_320,pcpc_325
        where pcpc_320.numero_volume     = pcpc_325.numero_volume
          and pedi_110.pedido_venda      = pcpc_320.pedido_venda
          and pedi_110.cd_it_pe_nivel99  = pcpc_325.nivel
          and pedi_110.seq_item_pedido   = pcpc_325.seq_item_pedido
          and pedi_110.cd_it_pe_grupo    = pcpc_325.grupo
          and pedi_110.cd_it_pe_subgrupo = pcpc_325.sub
          and pedi_110.cd_it_pe_item     = pcpc_325.item
          and pcpc_320.situacao_volume not in (2,5)
          and pedi_110.pedido_venda      = pedi_100.pedido_venda group by pcpc_325.qtde_pecas_real) as valor_coletado,
       nvl(pedi_100.qtde_saldo_pedi,0) - nvl(pcpc_320a.qtde_coletada,0) as qtde_a_coletar,
       nvl(decode(nvl(pedi_100.qtde_saldo_pedi,0) - nvl(pcpc_320a.qtde_coletada,0),0,0.00,( pedi_100.valor_saldo_pedi - ((select nvl(inter_fn_calc_desconto_triplo(nvl(sum(pcpc_325.qtde_pecas_real*( sum(pedi_110.valor_unitario - ((pedi_110.valor_unitario * pedi_110.percentual_desc) / 100)))),0),pedi_100.desconto1,pedi_100.desconto2, pedi_100.desconto3),0)
                                         from pedi_110,pcpc_320,pcpc_325
                                         where pcpc_320.numero_volume     = pcpc_325.numero_volume
                                         and pedi_110.pedido_venda      = pcpc_320.pedido_venda
                                         and pedi_110.cd_it_pe_nivel99  = pcpc_325.nivel
                                         and pedi_110.seq_item_pedido   = pcpc_325.seq_item_pedido
                                         and pedi_110.cd_it_pe_grupo    = pcpc_325.grupo
                                         and pedi_110.cd_it_pe_subgrupo = pcpc_325.sub
                                         and pedi_110.cd_it_pe_item     = pcpc_325.item
                                         and pcpc_320.situacao_volume not in (2,5)
                                         and pedi_110.pedido_venda      = pedi_100.pedido_venda group by pcpc_325.qtde_pecas_real)))),0) as valor_a_coletar,
       nvl(pedi_135a.bloqueio_comercial_pedido,0) as bloqueio_comercial_pedido,
      (select count(distinct to_char(fatu_060.ch_it_nf_num_nfis) || fatu_060.ch_it_nf_ser_nfis)from fatu_060
       where fatu_060.ch_it_nf_cd_empr = pedi_100.codigo_empresa
         and fatu_060.pedido_venda     = pedi_100.pedido_venda ) as vezes_faturado,
      round((nvl(pcpc_320a.qtde_coletada,0)) / pedi_100.qtde_saldo_pedi * 100) porc_quantidade_coletado,
      round((select nvl(inter_fn_calc_desconto_triplo(nvl(sum(pcpc_325.qtde_pecas_real*( sum(pedi_110.valor_unitario - ((pedi_110.valor_unitario * pedi_110.percentual_desc) / 100)))),0),pedi_100.desconto1,pedi_100.desconto2, pedi_100.desconto3),0)
       from pedi_110,pcpc_320,pcpc_325
        where pcpc_320.numero_volume     = pcpc_325.numero_volume
          and pedi_110.pedido_venda      = pcpc_320.pedido_venda
          and pedi_110.cd_it_pe_nivel99  = pcpc_325.nivel
          and pedi_110.seq_item_pedido   = pcpc_325.seq_item_pedido
          and pedi_110.cd_it_pe_grupo    = pcpc_325.grupo
          and pedi_110.cd_it_pe_subgrupo = pcpc_325.sub
          and pedi_110.cd_it_pe_item     = pcpc_325.item
          and pcpc_320.situacao_volume not in (2,5)
          and pedi_110.pedido_venda      = pedi_100.pedido_venda group by pcpc_325.qtde_pecas_real) / pedi_100.valor_saldo_pedi *100) porc_valor_coletado,
       nvl(pedi_110b.qtde_suger,0) qtde_suger,
       nvl(pedi_110b.valor_suger,0) valor_suger
from pedi_100,
     pedi_010,
     supr_010,
     pedi_020,
     pedi_020 pedi_020a,
     pedi_090,
     pedi_070,
     (select nvl(sum(pcpc_325.qtde_pecas_real),0) qtde_coletada,
             pcpc_320.pedido_venda,
             pcpc_320.cod_tipo_volume
      from pcpc_320, pcpc_325
      where pcpc_325.numero_volume  = pcpc_320.numero_volume
        and pcpc_320.situacao_volume not in (2,5)
      group by pcpc_320.pedido_venda, pcpc_320.cod_tipo_volume) pcpc_320a,
     (select max(pedi_135.codigo_situacao) bloqueio_comercial_pedido,
             pedi_135.pedido_venda
      from  pedi_135
      where  pedi_135.data_liberacao is null
      group by pedi_135.pedido_venda ) pedi_135a ,
     (select nvl(sum(pedi_110.qtde_sugerida),0) qtde_suger,
      sum(pedi_110.qtde_sugerida * (pedi_110.valor_unitario - ((pedi_110.valor_unitario * pedi_110.percentual_desc) / 100))) valor_suger,
      pedi_110.pedido_venda
      from pedi_110
      where pedi_110.qtde_sugerida > 0.00
      group by pedi_110.pedido_venda) pedi_110b,
     (select nvl(sum(pedi_110.qtde_faturada),0) qtde_faturada,
             sum(pedi_110.qtde_faturada * (pedi_110.valor_unitario - ((pedi_110.valor_unitario * pedi_110.percentual_desc) / 100))) valor_faturad,
             pedi_110.pedido_venda
      from pedi_110
      --where pedi_110.qtde_faturada > 0
      group by pedi_110.pedido_venda) pedi_110a
where pedi_100.pedido_venda     = pedi_110a.pedido_venda
  and pedi_100.pedido_venda     = pedi_110b.pedido_venda (+)
  and pedi_100.cli_ped_cgc_cli9 = pedi_010.cgc_9
  and pedi_100.cli_ped_cgc_cli4 = pedi_010.cgc_4
  and pedi_100.cli_ped_cgc_cli2 = pedi_010.cgc_2
  and pedi_100.trans_pv_forne9  = supr_010.fornecedor9
  and pedi_100.trans_pv_forne4  = supr_010.fornecedor4
  and pedi_100.trans_pv_forne2  = supr_010.fornecedor2
  and pedi_100.cod_rep_cliente  = pedi_020.cod_rep_cliente  (+)
  and pedi_100.codigo_administr = pedi_020a.cod_rep_cliente (+)
  and pedi_100.pedido_venda     = pcpc_320a.pedido_venda
  and pedi_100.pedido_venda     = pedi_135a.pedido_venda (+)
  and pedi_100.colecao_tabela   = pedi_090.col_tabela_preco (+)
  and pedi_100.mes_tabela       = pedi_090.mes_tabela_preco (+)
  and pedi_100.sequencia_tabela = pedi_090.seq_tabela_preco (+)
  and pedi_100.cond_pgto_venda  = pedi_070.cond_pgt_cliente
  and pedi_100.tecido_peca      = '1'
  and pcpc_320a.cod_tipo_volume = 7 /* pedidos normais coletados na m�quina */
  and pcpc_320a.qtde_coletada   > 0
  and pedi_100.qtde_saldo_pedi  > 0
  and pedi_100.valor_saldo_pedi > 0
  and not exists (select 1 from inte_wms_pedidos
                  where inte_wms_pedidos.numero_pedido = pedi_100.pedido_venda
                    and inte_wms_pedidos.flag_importacao_st <> 1)
  and not exists (select 1 from fatu_030
                  where fatu_030.pedido_venda = pedi_100.pedido_venda);

exec inter_pr_recompile;


