
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_010_HIST" 
   after insert or
         update of numero_receita, peso_tingimento, relacao_banho, qtde_quilos_prog,
		observacao, ordem_preparacao, situacao_ordem, cod_cancelamento,
		dt_cancelamento, ultimo_estagio, observacao2, ordem_producao,
		situacao_fal, nivel_estrutura, tipo_ordem, ordem_processo,
		ordem_reprocesso, situacao_ot, situacao_estamparia, ordem_servico,
		seq_ordem_serv, grupo_atendimento, programa_externo, data_envio_preactor,
		data_retorno_preactor, codigo_roteiro, situacao_pre_ob, tipo_programacao,
		sit_ordem_sal, numero_programa, sit_prov, data_prevista_original,
		ordem_teste, qtde_rolos_teste, qtde_quilos_teste, cor_imp_teste,
		situacao_bloqueio, numero_fusos, codigo_motivo, situacao_reposicao,
		posicao_integracao, situacao_integracao, observacao3, volume_banho_2,
		volume_banho, qtde_rolos_prog, periodo_producao, ordem_tingimento,
		data_programa, previsao_termino, grupo_maquina, subgrupo_maquina,
		numero_maquina
   on pcpb_010
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number;
   ws_area_producao          number(9);
   ws_ordem_producao         number(9);
   ws_periodo_producao       number(9);
   ws_ordem_confeccao        number(9);
   ws_tipo_historico         number(9);
   ws_descricao_historico    varchar2(4000);
   ws_tipo_ocorr             varchar2(1);   
   ws_usuario_systextil      varchar(250);
   ws_empresa                number;
   ws_locale_usuario         varchar(20);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      ws_area_producao         := 2;
      ws_ordem_producao        := :new.ordem_producao;
      ws_periodo_producao      := :new.periodo_producao;
      ws_ordem_confeccao       := 0;
      ws_tipo_historico        := 1;
      ws_descricao_historico   := 'INCLUSAO ORDEM DE BENEFICIAMENTO (PCPB_010)'
                                  ||chr(10)
                                  ||'ORDEM TINGIMENTO:' || ' ' || :new.ordem_tingimento;
      ws_tipo_ocorr            := 'I';

      INSERT INTO hist_010
        (area_producao,           ordem_producao,          periodo_producao,
         ordem_confeccao,         tipo_historico,          descricao_historico,
         tipo_ocorr,              data_ocorr,              hora_ocorr,
         usuario_rede,            maquina_rede,            aplicacao,
         tipo_ordem)
      VALUES
        (ws_area_producao,        ws_ordem_producao,       ws_periodo_producao,
         ws_ordem_confeccao,      ws_tipo_historico,       ws_descricao_historico,
         ws_tipo_ocorr,           sysdate,                 sysdate,
         ws_usuario_rede,         ws_maquina_rede,         ws_aplicativo,
         0);
   end if;

   if updating
   then
      if :old.ordem_tingimento <> :new.ordem_tingimento
      then
         ws_area_producao         := 2;
         ws_ordem_producao        := :old.ordem_producao;
         ws_periodo_producao      := :old.periodo_producao;
         ws_ordem_confeccao       := 0;
         ws_tipo_historico        := 1;
         ws_descricao_historico   := 'ATUALIZACAO ORDEM DE TINGIMENTO (PCPB_010)'
                                     ||chr(10)
                                     ||'ORDEM TINGIMENTO:' || ' ' || :old.ordem_tingimento
                                     ||'  PARA' ||' -> '|| :new.ordem_tingimento;
         ws_tipo_ocorr            := 'A';

         INSERT INTO hist_010
           (area_producao,           ordem_producao,          periodo_producao,
            ordem_confeccao,         tipo_historico,          descricao_historico,
            tipo_ocorr,              data_ocorr,              hora_ocorr,
            usuario_rede,            maquina_rede,            aplicacao,
            tipo_ordem)
         VALUES
           (ws_area_producao,        ws_ordem_producao,       ws_periodo_producao,
            ws_ordem_confeccao,      ws_tipo_historico,       ws_descricao_historico,
            ws_tipo_ocorr,           sysdate,                 sysdate,
            ws_usuario_rede,         ws_maquina_rede,         ws_aplicativo,
            0);
       end if;

       if :old.cod_cancelamento <> :new.cod_cancelamento
       then
         ws_area_producao         := 2;
         ws_ordem_producao        := :old.ordem_producao;
         ws_periodo_producao      := :old.periodo_producao;
         ws_ordem_confeccao       := 0;
         ws_tipo_historico        := 1;
         ws_descricao_historico   := 'ATUALIZACAO CODIGO DE CANCELAMENTO (PCPB_010)'
                                     ||chr(10)
                                     ||'COD. CANCELAMENTO:' || ' ' || :old.cod_cancelamento
                                     ||'  PARA' ||' -> '|| :new.cod_cancelamento;
         ws_tipo_ocorr            := 'A';

         INSERT INTO hist_010
           (area_producao,           ordem_producao,          periodo_producao,
            ordem_confeccao,         tipo_historico,          descricao_historico,
            tipo_ocorr,              data_ocorr,              hora_ocorr,
            usuario_rede,            maquina_rede,            aplicacao,
            tipo_ordem)
         VALUES
           (ws_area_producao,        ws_ordem_producao,       ws_periodo_producao,
            ws_ordem_confeccao,      ws_tipo_historico,       ws_descricao_historico,
            ws_tipo_ocorr,           sysdate,                 sysdate,
            ws_usuario_rede,         ws_maquina_rede,         ws_aplicativo,
            0);
       end if;

       if :old.situacao_fal = 5 and :new.situacao_fal = 0
       then
         ws_area_producao         := 2;
         ws_ordem_producao        := :old.ordem_producao;
         ws_periodo_producao      := :old.periodo_producao;
         ws_ordem_confeccao       := 0;
         ws_tipo_historico        := 1;
         ws_descricao_historico   := 'LIBEROU REIMPRESSAO ETIQUETA ACABAMENTO';
         ws_tipo_ocorr            := 'A';

         INSERT INTO hist_010
           (area_producao,           ordem_producao,          periodo_producao,
            ordem_confeccao,         tipo_historico,          descricao_historico,
            tipo_ocorr,              data_ocorr,              hora_ocorr,
            usuario_rede,            maquina_rede,            aplicacao,
            tipo_ordem)
         VALUES
           (ws_area_producao,        ws_ordem_producao,       ws_periodo_producao,
            ws_ordem_confeccao,      ws_tipo_historico,       ws_descricao_historico,
            ws_tipo_ocorr,           sysdate,                 sysdate,
            ws_usuario_rede,         ws_maquina_rede,         ws_aplicativo,
            0);
       end if;
       
       if :old.periodo_producao <> :new.periodo_producao
       then
         ws_area_producao         := 2;
         ws_ordem_producao        := :old.ordem_producao;
         ws_periodo_producao      := :old.periodo_producao;
         ws_ordem_confeccao       := 0;
         ws_tipo_historico        := 1;
         ws_descricao_historico   := 'ALTERACAO PERIODO DE PRODUCAO'
                                     ||chr(10)
                                     ||'PERIODO PRODUCAO:' || ' ' || :old.periodo_producao
                                     ||'  PARA' ||' -> '|| :new.periodo_producao;
         ws_tipo_ocorr            := 'A';

         INSERT INTO hist_010
           (area_producao,           ordem_producao,          periodo_producao,
            ordem_confeccao,         tipo_historico,          descricao_historico,
            tipo_ocorr,              data_ocorr,              hora_ocorr,
            usuario_rede,            maquina_rede,            aplicacao,
            tipo_ordem)
         VALUES
           (ws_area_producao,        ws_ordem_producao,       ws_periodo_producao,
            ws_ordem_confeccao,      ws_tipo_historico,       ws_descricao_historico,
            ws_tipo_ocorr,           sysdate,                 sysdate,
            ws_usuario_rede,         ws_maquina_rede,         ws_aplicativo,
            0);
       end if;

   end if;
end inter_tr_pcpb_010_hist;

-- ALTER TRIGGER "INTER_TR_PCPB_010_HIST" ENABLE
 

/

exec inter_pr_recompile;

