CREATE OR REPLACE TRIGGER inter_tr_obrf_516_hist
after insert or delete or update on obrf_516 
for each row 

declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9)    ; 
   ws_empresa                number(3)    ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5)  ; 
   ws_nome_programa          varchar2(20) ; 


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


   ws_nome_programa := inter_fn_nome_programa(ws_sid);
 
 
 if inserting 
 then 
    begin 
 
        insert into obrf_516_hist (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/  
           cod_empresa_NEW,   /*9*/ 
           uf_empresa_NEW,   /*11*/
           perc_interno_fci_NEW,   /*13*/ 
           perc_interestadual_fci_NEW   /*15*/ 
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            ws_nome_programa, /*7*/
            :new.cod_empresa, /*9*/   
            :new.uf_empresa, /*11*/   
            :new.perc_interno_fci, /*13*/   
            :new.perc_interestadual_fci /*15*/            
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into obrf_516_hist (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           cod_empresa_OLD, /*8*/   
           cod_empresa_NEW, /*9*/   
           uf_empresa_OLD,   /*11*/
           uf_empresa_NEW,
           perc_interno_fci_OLD,   /*13*/ 
           perc_interno_fci_NEW,
           perc_interestadual_fci_OLD,   /*15*/ 
           perc_interestadual_fci_NEW
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            ws_nome_programa, /*7*/     
            :old.cod_empresa, /*9*/  
            :new.cod_empresa,
            :old.uf_empresa, /*11*/   
            :new.uf_empresa,
            :old.perc_interno_fci, /*13*/
            :new.perc_interno_fci,
            :old.perc_interestadual_fci, /*15*/
            :new.perc_interestadual_fci  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into obrf_516_hist (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/ 
           cod_empresa_OLD, /*8*/ 
           uf_empresa_OLD,           
           perc_interno_fci_OLD,
           perc_interestadual_fci_OLD
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            ws_nome_programa, /*7*/
            :old.cod_empresa, /*9*/   
            :old.uf_empresa, /*11*/   
            :old.perc_interno_fci, /*13*/   
            :old.perc_interestadual_fci /*15*/
         );    
    end;    
 end if;    
end inter_tr_obrf_516_hist;
/
exec inter_pr_recompile;
