create or replace trigger inter_tr_pcpb_070_log 
after insert or delete or update 
on PCPB_070 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(20) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu?rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    begin 
       select hdoc_090.programa 
       into v_nome_programa 
       from hdoc_090 
       where hdoc_090.sid = ws_sid 
         and rownum       = 1 
         and hdoc_090.programa not like '%menu%' 
         and hdoc_090.programa not like '%_m%'; 
       exception 
         when no_data_found then            v_nome_programa := 'SQL'; 
    end; 
 
 
 
 if inserting 
 then 
    begin 
 
        insert into PCPB_070_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           TIPO_INFORMACAO_OLD,   /*8*/ 
           TIPO_INFORMACAO_NEW,   /*9*/ 
           ORDEM_PRODUCAO_OLD,   /*10*/ 
           ORDEM_PRODUCAO_NEW,   /*11*/ 
           NIVEL_ESTRUTURA_OLD,   /*12*/ 
           NIVEL_ESTRUTURA_NEW,   /*13*/ 
           GRUPO_ESTRUTURA_OLD,   /*14*/ 
           GRUPO_ESTRUTURA_NEW,   /*15*/ 
           SUBGRU_ESTRUTURA_OLD,   /*16*/ 
           SUBGRU_ESTRUTURA_NEW,   /*17*/ 
           ITEM_ESTRUTURA_OLD,   /*18*/ 
           ITEM_ESTRUTURA_NEW,   /*19*/ 
           SEQUENCIA_OLD,   /*20*/ 
           SEQUENCIA_NEW,   /*21*/ 
           CODIGO_INFORMANTE_OLD,   /*22*/ 
           CODIGO_INFORMANTE_NEW,   /*23*/ 
           DATA_INFORMACAO_OLD,   /*24*/ 
           DATA_INFORMACAO_NEW,   /*25*/ 
           HORA_INFORMACAO_OLD,   /*26*/ 
           HORA_INFORMACAO_NEW,   /*27*/ 
           SITUACAO_OLD,   /*28*/ 
           SITUACAO_NEW,   /*29*/ 
           ORIGEM_OLD,   /*30*/ 
           ORIGEM_NEW,   /*31*/ 
           NATUREZA_OLD,   /*32*/ 
           NATUREZA_NEW,   /*33*/ 
           ARREADA_OLD,   /*34*/ 
           ARREADA_NEW,   /*35*/ 
           LOTE_PRODUTO_OLD,   /*36*/ 
           LOTE_PRODUTO_NEW,   /*37*/ 
           TONALIDADE_LIVRE_OLD,   /*38*/ 
           TONALIDADE_LIVRE_NEW,   /*39*/ 
           TONALIDADE_BR_CR_OLD,   /*40*/ 
           TONALIDADE_BR_CR_NEW,   /*41*/ 
           TONALIDADE_CLARA_OLD,   /*42*/ 
           TONALIDADE_CLARA_NEW,   /*43*/ 
           TONALIDADE_MEDIA_OLD,   /*44*/ 
           TONALIDADE_MEDIA_NEW,   /*45*/ 
           TONALIDADE_ESCURA_OLD,   /*46*/ 
           TONALIDADE_ESCURA_NEW,   /*47*/ 
           TONALIDADE_PRETA_OLD,   /*48*/ 
           TONALIDADE_PRETA_NEW   /*49*/ 
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.TIPO_INFORMACAO, /*9*/   
           0,/*10*/
           :new.ORDEM_PRODUCAO, /*11*/   
           '',/*12*/
           :new.NIVEL_ESTRUTURA, /*13*/   
           '',/*14*/
           :new.GRUPO_ESTRUTURA, /*15*/   
           '',/*16*/
           :new.SUBGRU_ESTRUTURA, /*17*/   
           '',/*18*/
           :new.ITEM_ESTRUTURA, /*19*/   
           0,/*20*/
           :new.SEQUENCIA, /*21*/   
           0,/*22*/
           :new.CODIGO_INFORMANTE, /*23*/   
           null,/*24*/
           :new.DATA_INFORMACAO, /*25*/   
           null,/*26*/
           :new.HORA_INFORMACAO, /*27*/   
           0,/*28*/
           :new.SITUACAO, /*29*/   
           0,/*30*/
           :new.ORIGEM, /*31*/   
           0,/*32*/
           :new.NATUREZA, /*33*/   
           0,/*34*/
           :new.ARREADA, /*35*/   
           0,/*36*/
           :new.LOTE_PRODUTO, /*37*/   
           0,/*38*/
           :new.TONALIDADE_LIVRE, /*39*/   
           0,/*40*/
           :new.TONALIDADE_BR_CR, /*41*/   
           0,/*42*/
           :new.TONALIDADE_CLARA, /*43*/   
           0,/*44*/
           :new.TONALIDADE_MEDIA, /*45*/   
           0,/*46*/
           :new.TONALIDADE_ESCURA, /*47*/   
           0,/*48*/
           :new.TONALIDADE_PRETA /*49*/
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into PCPB_070_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           TIPO_INFORMACAO_OLD, /*8*/   
           TIPO_INFORMACAO_NEW, /*9*/   
           ORDEM_PRODUCAO_OLD, /*10*/   
           ORDEM_PRODUCAO_NEW, /*11*/   
           NIVEL_ESTRUTURA_OLD, /*12*/   
           NIVEL_ESTRUTURA_NEW, /*13*/   
           GRUPO_ESTRUTURA_OLD, /*14*/   
           GRUPO_ESTRUTURA_NEW, /*15*/   
           SUBGRU_ESTRUTURA_OLD, /*16*/   
           SUBGRU_ESTRUTURA_NEW, /*17*/   
           ITEM_ESTRUTURA_OLD, /*18*/   
           ITEM_ESTRUTURA_NEW, /*19*/   
           SEQUENCIA_OLD, /*20*/   
           SEQUENCIA_NEW, /*21*/   
           CODIGO_INFORMANTE_OLD, /*22*/   
           CODIGO_INFORMANTE_NEW, /*23*/   
           DATA_INFORMACAO_OLD, /*24*/   
           DATA_INFORMACAO_NEW, /*25*/   
           HORA_INFORMACAO_OLD, /*26*/   
           HORA_INFORMACAO_NEW, /*27*/   
           SITUACAO_OLD, /*28*/   
           SITUACAO_NEW, /*29*/   
           ORIGEM_OLD, /*30*/   
           ORIGEM_NEW, /*31*/   
           NATUREZA_OLD, /*32*/   
           NATUREZA_NEW, /*33*/   
           ARREADA_OLD, /*34*/   
           ARREADA_NEW, /*35*/   
           LOTE_PRODUTO_OLD, /*36*/   
           LOTE_PRODUTO_NEW, /*37*/   
           TONALIDADE_LIVRE_OLD, /*38*/   
           TONALIDADE_LIVRE_NEW, /*39*/   
           TONALIDADE_BR_CR_OLD, /*40*/   
           TONALIDADE_BR_CR_NEW, /*41*/   
           TONALIDADE_CLARA_OLD, /*42*/   
           TONALIDADE_CLARA_NEW, /*43*/   
           TONALIDADE_MEDIA_OLD, /*44*/   
           TONALIDADE_MEDIA_NEW, /*45*/   
           TONALIDADE_ESCURA_OLD, /*46*/   
           TONALIDADE_ESCURA_NEW, /*47*/   
           TONALIDADE_PRETA_OLD, /*48*/   
           TONALIDADE_PRETA_NEW /*49*/   
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.TIPO_INFORMACAO,  /*8*/  
           :new.TIPO_INFORMACAO, /*9*/   
           :old.ORDEM_PRODUCAO,  /*10*/  
           :new.ORDEM_PRODUCAO, /*11*/   
           :old.NIVEL_ESTRUTURA,  /*12*/  
           :new.NIVEL_ESTRUTURA, /*13*/   
           :old.GRUPO_ESTRUTURA,  /*14*/  
           :new.GRUPO_ESTRUTURA, /*15*/   
           :old.SUBGRU_ESTRUTURA,  /*16*/  
           :new.SUBGRU_ESTRUTURA, /*17*/   
           :old.ITEM_ESTRUTURA,  /*18*/  
           :new.ITEM_ESTRUTURA, /*19*/   
           :old.SEQUENCIA,  /*20*/  
           :new.SEQUENCIA, /*21*/   
           :old.CODIGO_INFORMANTE,  /*22*/  
           :new.CODIGO_INFORMANTE, /*23*/   
           :old.DATA_INFORMACAO,  /*24*/  
           :new.DATA_INFORMACAO, /*25*/   
           :old.HORA_INFORMACAO,  /*26*/  
           :new.HORA_INFORMACAO, /*27*/   
           :old.SITUACAO,  /*28*/  
           :new.SITUACAO, /*29*/   
           :old.ORIGEM,  /*30*/  
           :new.ORIGEM, /*31*/   
           :old.NATUREZA,  /*32*/  
           :new.NATUREZA, /*33*/   
           :old.ARREADA,  /*34*/  
           :new.ARREADA, /*35*/   
           :old.LOTE_PRODUTO,  /*36*/  
           :new.LOTE_PRODUTO, /*37*/   
           :old.TONALIDADE_LIVRE,  /*38*/  
           :new.TONALIDADE_LIVRE, /*39*/   
           :old.TONALIDADE_BR_CR,  /*40*/  
           :new.TONALIDADE_BR_CR, /*41*/   
           :old.TONALIDADE_CLARA,  /*42*/  
           :new.TONALIDADE_CLARA, /*43*/   
           :old.TONALIDADE_MEDIA,  /*44*/  
           :new.TONALIDADE_MEDIA, /*45*/   
           :old.TONALIDADE_ESCURA,  /*46*/  
           :new.TONALIDADE_ESCURA, /*47*/   
           :old.TONALIDADE_PRETA,  /*48*/  
           :new.TONALIDADE_PRETA /*49*/   
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into PCPB_070_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           TIPO_INFORMACAO_OLD, /*8*/   
           TIPO_INFORMACAO_NEW, /*9*/   
           ORDEM_PRODUCAO_OLD, /*10*/   
           ORDEM_PRODUCAO_NEW, /*11*/   
           NIVEL_ESTRUTURA_OLD, /*12*/   
           NIVEL_ESTRUTURA_NEW, /*13*/   
           GRUPO_ESTRUTURA_OLD, /*14*/   
           GRUPO_ESTRUTURA_NEW, /*15*/   
           SUBGRU_ESTRUTURA_OLD, /*16*/   
           SUBGRU_ESTRUTURA_NEW, /*17*/   
           ITEM_ESTRUTURA_OLD, /*18*/   
           ITEM_ESTRUTURA_NEW, /*19*/   
           SEQUENCIA_OLD, /*20*/   
           SEQUENCIA_NEW, /*21*/   
           CODIGO_INFORMANTE_OLD, /*22*/   
           CODIGO_INFORMANTE_NEW, /*23*/   
           DATA_INFORMACAO_OLD, /*24*/   
           DATA_INFORMACAO_NEW, /*25*/   
           HORA_INFORMACAO_OLD, /*26*/   
           HORA_INFORMACAO_NEW, /*27*/   
           SITUACAO_OLD, /*28*/   
           SITUACAO_NEW, /*29*/   
           ORIGEM_OLD, /*30*/   
           ORIGEM_NEW, /*31*/   
           NATUREZA_OLD, /*32*/   
           NATUREZA_NEW, /*33*/   
           ARREADA_OLD, /*34*/   
           ARREADA_NEW, /*35*/   
           LOTE_PRODUTO_OLD, /*36*/   
           LOTE_PRODUTO_NEW, /*37*/   
           TONALIDADE_LIVRE_OLD, /*38*/   
           TONALIDADE_LIVRE_NEW, /*39*/   
           TONALIDADE_BR_CR_OLD, /*40*/   
           TONALIDADE_BR_CR_NEW, /*41*/   
           TONALIDADE_CLARA_OLD, /*42*/   
           TONALIDADE_CLARA_NEW, /*43*/   
           TONALIDADE_MEDIA_OLD, /*44*/   
           TONALIDADE_MEDIA_NEW, /*45*/   
           TONALIDADE_ESCURA_OLD, /*46*/   
           TONALIDADE_ESCURA_NEW, /*47*/   
           TONALIDADE_PRETA_OLD, /*48*/   
           TONALIDADE_PRETA_NEW /*49*/    
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.TIPO_INFORMACAO, /*8*/   
           0, /*9*/
           :old.ORDEM_PRODUCAO, /*10*/   
           0, /*11*/
           :old.NIVEL_ESTRUTURA, /*12*/   
           '', /*13*/
           :old.GRUPO_ESTRUTURA, /*14*/   
           '', /*15*/
           :old.SUBGRU_ESTRUTURA, /*16*/   
           '', /*17*/
           :old.ITEM_ESTRUTURA, /*18*/   
           '', /*19*/
           :old.SEQUENCIA, /*20*/   
           0, /*21*/
           :old.CODIGO_INFORMANTE, /*22*/   
           0, /*23*/
           :old.DATA_INFORMACAO, /*24*/   
           null, /*25*/
           :old.HORA_INFORMACAO, /*26*/   
           null, /*27*/
           :old.SITUACAO, /*28*/   
           0, /*29*/
           :old.ORIGEM, /*30*/   
           0, /*31*/
           :old.NATUREZA, /*32*/   
           0, /*33*/
           :old.ARREADA, /*34*/   
           0, /*35*/
           :old.LOTE_PRODUTO, /*36*/   
           0, /*37*/
           :old.TONALIDADE_LIVRE, /*38*/   
           0, /*39*/
           :old.TONALIDADE_BR_CR, /*40*/   
           0, /*41*/
           :old.TONALIDADE_CLARA, /*42*/   
           0, /*43*/
           :old.TONALIDADE_MEDIA, /*44*/   
           0, /*45*/
           :old.TONALIDADE_ESCURA, /*46*/   
           0, /*47*/
           :old.TONALIDADE_PRETA, /*48*/   
           0 /*49*/  
         );    
    end;    
 end if;    
end inter_tr_pcpb_070_log;
