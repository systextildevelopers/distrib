
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_650_1" 
after delete
      on tmrp_650
      for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);


begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if deleting
   then
      if :old.tipo_reserva > 0
      then
         begin
            update tmrp_640
            set   tmrp_640.status         = 2
            where tmrp_640.numero_reserva = :new.pedido_venda;
         exception when OTHERS then
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_640(inter_tr_tmrp_650_1(3))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;

end inter_tr_tmrp_650_1;

-- ALTER TRIGGER "INTER_TR_TMRP_650_1" ENABLE
 

/

exec inter_pr_recompile;

