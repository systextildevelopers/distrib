---------------------
-- Rolos Sugeridos --
---------------------

create table pcpt_071 (
  id number(9, 0) primary key not null,
  id_sugestao number(9, 0) not null references pcpt_070(id),
  pedido_venda number(9,0) not null,
  seq_item_pedido number(3, 0) not null,
  sequencia number(9, 0) not null,
  codigo_rolo number(9, 0) not null,
  tipo_de_sugestao varchar2 (30) not null check( tipo_de_sugestao in ('Por Nuance', 'Por Ordem de Produção'))
);

alter table pcpt_071 add constraint uni_pcpt_071 unique(pedido_venda, seq_item_pedido, codigo_rolo, tipo_de_sugestao); 

comment on table pcpt_071 is 'Itens da sugestão de alocação de rolos';
comment on column pcpt_071.id is 'Chave primária';
comment on column pcpt_071.id_sugestao is 'Chave estrangeira da sugestão';
comment on column pcpt_071.pedido_venda is 'Numero do pedido';
comment on column pcpt_071.seq_item_pedido is 'Sequencia do pedido';
comment on column pcpt_071.sequencia is 'Sequencia da alocação de rolos';
comment on column pcpt_071.codigo_rolo is 'Código do rolo na pcpt_020';
comment on column pcpt_071.tipo_de_sugestao is 'Tipo da sugestão que está sendo feita (Por Nuance / Por Ordem de Produção)';

create sequence pcpt_071_seq;

create or replace trigger bi_pcpt_071
before insert on pcpt_071
for each row
begin
  :new.id := pcpt_071_seq.nextval;
end;
