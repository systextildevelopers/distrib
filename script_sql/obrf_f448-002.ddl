-- Create table
create table OBRF_921
( ID_921                    NUMBER(9) default 0 not null,
  ID_920                    NUMBER(9) default 0 not null,
  COD_QUADRO                NUMBER(9) default 0 not null,  
  COD_AJ_APUR               VARCHAR2(8) default ' ', 
  DESCR_COMPL_AJ            VARCHAR2(4000) default ' ', 
  VL_AJ_APUR                NUMBER(15,2) default 0.00,
  tipo_insert               VARCHAR2(1) default 'M');
  
-- Add comments to the columns 
comment on column OBRF_921.COD_AJ_APUR    is 'C�digo do ajuste da SUB-APURA��O e dedu��o, conforme a Tabela indicada no item 5.1.1.';
comment on column OBRF_921.DESCR_COMPL_AJ is 'Descri��o complementar do ajuste da apura��o.';
comment on column OBRF_921.VL_AJ_APUR     is 'Valor do ajuste da apura��o.';

-- Create/Recreate primary, unique and foreign key constraints 
alter table OBRF_921 add constraint PK_OBRF_921 primary key (ID_921);
alter table OBRF_921 add constraint UNIQ_OBRF_921 unique (ID_920, COD_QUADRO, COD_AJ_APUR);

create synonym systextilrpt.OBRF_921 for OBRF_921; 

create sequence seq_obrf_921
minvalue 0
maxvalue 99999999999999999999
start with 1
increment by 1;

CREATE OR REPLACE TRIGGER INTER_TR_OBRF_921_SEQ
BEFORE INSERT ON OBRF_921 FOR EACH ROW
DECLARE
    next_value number;
BEGIN
    if :new.ID_921 is null or :new.ID_921 = 0 then
        select seq_obrf_921.nextval
        into next_value from dual;
        :new.ID_921 := next_value;
    end if;
END INTER_TR_OBRF_921_SEQ;

/

exec inter_pr_recompile;
