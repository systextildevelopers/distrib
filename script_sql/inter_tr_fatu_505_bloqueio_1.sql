create or replace trigger "INTER_TR_FATU_505_BLOQUEIO_1"
   before update of ultima_nf_impres
   on fatu_505
   for each row
begin
   if UPDATING then
      if :new.ultima_nf_impres = 0 or :new.ultima_nf_impres is null then
         raise_application_error(-20000,'ATEN��O! N�o � permitido zerar a numera��o da nota fiscal, entre em contato com o TI. �ltimo N�mero anterior: ' || to_char(:old.ultima_nf_impres) || '. Iria atuaizar �ltimo n�mero para ' || to_char(:new.ultima_nf_impres) || '.');
      end if;
   end if;
end INTER_TR_FATU_505_BLOQUEIO_1;
/

exec inter_pr_recompile;

