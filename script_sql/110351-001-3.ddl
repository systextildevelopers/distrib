create table BASI_461 (
    id                      number(9)      not null,
    codigo_regiao           number(2)      default (0) ,
    descricao               VARCHAR2(40)   default (''));

ALTER TABLE BASI_461 ADD CONSTRAINT pk_BASI_461 PRIMARY KEY (id);
ALTER TABLE BASI_461 ADD CONSTRAINT UNIQ_BASI_461 UNIQUE(codigo_regiao, descricao);

CREATE SEQUENCE ID_BASI_461 START WITH 1 INCREMENT BY 1;

exec inter_pr_recompile;
