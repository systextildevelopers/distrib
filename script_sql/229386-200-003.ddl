create table blocok_220 (
	COD_EMPRESA             number(3),
	ANO_PERIODO_APUR        number(4),
	MES_PERIODO_APUR        number(2),
	
	DATA_MOVIMENTO          date,
	
	DT_MOV                  varchar2(8),
	COD_ITEM_ORI            varchar2(60),
	COD_ITEM_DEST           varchar2(60),
	QTD_ORI                 number(15,6),
	QTD_DEST                number(15,6),
	
	data_hora_ins           date            default sysdate
);
