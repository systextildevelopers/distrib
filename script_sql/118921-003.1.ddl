create table PROG_050
(
    NIVEL_ESTRUTURA         VARCHAR2(1)    default '' not null,
    GRUPO_ESTRUTURA         VARCHAR2(5)    default '' not null,
    SUBGRU_ESTRUTURA        VARCHAR2(3)    default '' not null,
    ITEM_ESTRUTURA          VARCHAR2(6)    default '' not null,
    NUMERO_ALTERNATI        NUMBER(2)      default 0  not null,
    NUMERO_ROTEIRO          NUMBER(2)      default 0  not null,
    SEQ_OPERACAO            NUMBER(4)      default 0  not null,
    CODIGO_OPERACAO         NUMBER(5)      default 0,
    MINUTOS                 NUMBER(9, 4)   default 0.0,
    CODIGO_ESTAGIO          NUMBER(2)      default 0,
    CENTRO_CUSTO            NUMBER(6)      default 0,
    SEQUENCIA_ESTAGIO       NUMBER(4)      default 0,
    ESTAGIO_ANTERIOR        NUMBER(2)      default 0,
    ESTAGIO_DEPENDE         NUMBER(2)      default 0,
    SEPARA_OPERACAO         VARCHAR2(1)    default '',
    MINUTOS_HOMEM           NUMBER(9, 4)   default 0.00,
    CCUSTO_HOMEM            NUMBER(6)      default 0,
    NUMERO_CORDAS           NUMBER(3)      default 0,
    NUMERO_ROLOS            NUMBER(5)      default 0,
    VELOCIDADE              NUMBER(8, 2)   default 0.00,
    CODIGO_FAMILIA          NUMBER(6)      default 0,
    OBSERVACAO              VARCHAR2(4000) default '',
    TIPO_PROCESSO           NUMBER(1)      default 0,
    PECAS_1_HORA            NUMBER(9)      default 0,
    PECAS_8_HORAS           NUMBER(9)      default 0,
    CUSTO_MINUTO            NUMBER(6, 3)   default 0.00,
    PERC_EFICIENCIA         NUMBER(6, 3)   default 0.00,
    TEMPERATURA             NUMBER(7, 3)   default 0.00,
    TEMPO_LOTE_PRODUCAO     NUMBER(4)      default 0,
    PECAS_LOTE_PRODUCAO     NUMBER(10, 2)  default 0.00,
    TIME_CELULA             NUMBER(4)      default 0,
    CODIGO_APARELHO         VARCHAR2(4)    default '',
    PERC_EFIC_ROT           NUMBER(6, 3)   default 0.000,
    NUMERO_OPERADORAS       NUMBER(3)      default 0,
    CONSIDERA_EFIC          NUMBER(1)      default 0,
    SEQ_OPERACAO_AGRUPADORA NUMBER(4)      default 0,
    CODIGO_PARTE_PECA       VARCHAR2(5)    default '',
    SEQ_JUNCAO_PARTE_PECA   NUMBER(4)      default 0,
    SITUACAO                NUMBER(1)      default 0,
    PERC_PERDAS             NUMBER(5, 2)   default 0.00,
    PERC_CUSTOS             NUMBER(5, 2)   default 0.00,
    IND_ESTAGIO_GARGALO     NUMBER(1)      default 0,
    NR_AGULHAS              NUMBER(3)      default 0
);
/

exec inter_pr_recompile;
