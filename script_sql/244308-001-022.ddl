begin
  execute immediate 'CREATE TABLE MQOP_062
      (ID NUMBER(10),
      MAQ_SBGR_GRUPO_MQ VARCHAR2(4),
      MAQ_SBGR_SBGR_MAQ VARCHAR2(3),
      NIVEL_ESTRUTURA 	VARCHAR2(1),
      GRUPO_ESTRUTURA VARCHAR2(5),
      SUBGRU_ESTRUTURA VARCHAR2(3),
      ITEM_ESTRUTURA VARCHAR2(6),
      ID_CARA NUMBER(9),
      QTDE_ROLADA NUMBER(2))';
  exception when others then null;
end;
/
begin
  execute immediate 'ALTER TABLE mqop_062 ADD CONSTRAINT PK_mqop_062 PRIMARY KEY (ID)';
  exception when others then null;
end;
/
begin
  execute immediate 'CREATE SEQUENCE   ID_MQOP_062';
  exception when others then null;
end;
/
