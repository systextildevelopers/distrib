alter table pcpt_021
add peso_rolo_real number(9,3) default 0;

comment on column pcpt_021.peso_rolo_real
  is '� o peso do rolo real que vem da balan�a, para produtos com unidade de medida diferente de kg.';
  
/
exec inter_pr_recompile;
