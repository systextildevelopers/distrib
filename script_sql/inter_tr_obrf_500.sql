
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_500" 

  before insert or
         update of cod_emp_benef, cod_emp_origem, seq_exec_rotina, usuario_systextil_inicio,
		usuario_rede_inicio, maquina_rede_inicio, data_inicio, data_ultima_atualizacao,
		posicao_atual_rolos, etapa_atual, processo_atual, funcao_atual,
		mensagem, rotina_cancelada
         on obrf_500
  for each row

declare
     ws_usuario_rede           varchar2(20);
     ws_maquina_rede           varchar2(40);
     ws_aplicativo             varchar2(20);
     ws_sid                    number(9);
     ws_empresa                number(3);
     ws_usuario_systextil      varchar2(250);
     ws_locale_usuario         varchar2(5);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   :new.usuario_rede_inicio := ws_usuario_rede;
   :new.maquina_rede_inicio := ws_maquina_rede;
end;
-- ALTER TRIGGER "INTER_TR_OBRF_500" ENABLE
 

/

exec inter_pr_recompile;

