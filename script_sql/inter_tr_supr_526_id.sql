CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_526_ID" 
before insert on SUPR_526
  for each row

declare
   v_nr_registro number;

begin
   select ID_SUPR_526.nextval into v_nr_registro from dual;

   :new.id := v_nr_registro;

end INTER_TR_SUPR_526_ID;

/

exec inter_pr_recompile;
