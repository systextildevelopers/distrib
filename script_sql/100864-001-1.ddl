alter table obrf_995
add (aliquota_estadual      number(13,2) default 0,
     aliquota_municipal     number(13,2) default 0,
     final_vigecia          date);
     
exec inter_pr_recompile;
/
