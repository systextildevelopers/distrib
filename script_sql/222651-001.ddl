alter table pedi_100 add sit_conferencia number(1);
alter table pedi_100 modify sit_conferencia default 0;

comment on column pedi_100.sit_conferencia     is '0 - Não conferido, 1 - Conferência iniciada, 2 - Conferência sem divergência e 3 - Conferência com divergência';
