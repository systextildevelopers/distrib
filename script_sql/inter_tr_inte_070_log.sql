create or replace trigger "INTER_TR_INTE_070_LOG"
after insert or delete or update
on inte_070
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);

 if inserting
 then
    begin
        insert into INTE_070_LOG (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           numero_nf_old,   /*8*/
           numero_nf_new,   /*9*/
           serie_nf_old,   /*10*/
           serie_nf_new,   /*11*/
           cnpj_cliente9_old,   /*12*/
           cnpj_cliente9_new,   /*13*/
           cnpj_cliente4_old,   /*14*/
           cnpj_cliente4_new,   /*15*/
           cnpj_cliente2_old,   /*16*/
           cnpj_cliente2_new,   /*17*/
           nome_cliente_old,   /*18*/
           nome_cliente_new,   /*19*/
           cep_cliente_old,   /*20*/
           cep_cliente_new,   /*21*/
           cidade_cliente_old,   /*22*/
           cidade_cliente_new,   /*23*/
           uf_cliente_old,   /*24*/
           uf_cliente_new,   /*25*/
           valor_total_nf_old,   /*26*/
           valor_total_nf_new,   /*27*/
           peso_total_liq_old,   /*28*/
           peso_total_liq_new,   /*29*/
           peso_total_bru_old,   /*30*/
           peso_total_bru_new,   /*31*/
           volume_old,   /*32*/
           volume_new,   /*33*/
           qtd_volumes_old,   /*34*/
           qtd_volumes_new,   /*35*/
           tipo_volume_old,   /*36*/
           tipo_volume_new,   /*37*/
           cnpj_trans_pedi9_old,   /*38*/
           cnpj_trans_pedi9_new,   /*39*/
           cnpj_trans_pedi4_old,   /*40*/
           cnpj_trans_pedi4_new,   /*41*/
           cnpj_trans_pedi2_old,   /*42*/
           cnpj_trans_pedi2_new,   /*43*/
           data_remessa_old,   /*44*/
           data_remessa_new,   /*45*/
           cnpj_transp9_old,   /*46*/
           cnpj_transp9_new,   /*47*/
           cnpj_transp4_old,   /*48*/
           cnpj_transp4_new,   /*49*/
           cnpj_transp2_old,   /*50*/
           cnpj_transp2_new,   /*51*/
           data_retorno_old,   /*52*/
           data_retorno_new,   /*53*/
           situacao_old,   /*54*/
           situacao_new,   /*55*/
           pedido_venda_old,   /*56*/
           pedido_venda_new,   /*57*/
           situacao_pedido_old,   /*58*/
           situacao_pedido_new   /*59*/
           ) values (
           'i', /*o*/
           sysdate, /*1*/
           sysdate,/*2*/
           ws_usuario_rede,/*3*/
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/
           v_nome_programa, /*7*/
           null,/*8*/
           :new.numero_nf, /*9*/
           null,/*10*/
           :new.serie_nf, /*11*/
           null,/*12*/
           :new.cnpj_cliente9, /*13*/
           null,/*14*/
           :new.cnpj_cliente4, /*15*/
           null,/*16*/
           :new.cnpj_cliente2, /*17*/
           null,/*18*/
           :new.nome_cliente, /*19*/
           null,/*20*/
           :new.cep_cliente, /*21*/
           null,/*22*/
           :new.cidade_cliente, /*23*/
           null,/*24*/
           :new.uf_cliente, /*25*/
           null,/*26*/
           :new.valor_total_nf, /*27*/
           null,/*28*/
           :new.peso_total_liq, /*29*/
           null,/*30*/
           :new.peso_total_bru, /*31*/
           null,/*32*/
           :new.volume, /*33*/
           null,/*34*/
           :new.qtd_volumes, /*35*/
           null,/*36*/
           :new.tipo_volume, /*37*/
           null,/*38*/
           :new.cnpj_trans_pedi9, /*39*/
           null,/*40*/
           :new.cnpj_trans_pedi4, /*41*/
           null,/*42*/
           :new.cnpj_trans_pedi2, /*43*/
           null,/*44*/
           :new.data_remessa, /*45*/
           null,/*46*/
           :new.cnpj_transp9, /*47*/
           null,/*48*/
           :new.cnpj_transp4, /*49*/
           null,/*50*/
           :new.cnpj_transp2, /*51*/
           null,/*52*/
           :new.data_retorno, /*53*/
           null,/*54*/
           :new.situacao, /*55*/
           null,/*56*/
           :new.pedido_venda, /*57*/
           null,/*58*/
           :new.situacao_pedido /*59*/
        );
    end;
 end if;

 if updating
 then
    begin
       insert into INTE_070_LOG (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           numero_nf_old,   /*8*/
           numero_nf_new,   /*9*/
           serie_nf_old,   /*10*/
           serie_nf_new,   /*11*/
           cnpj_cliente9_old,   /*12*/
           cnpj_cliente9_new,   /*13*/
           cnpj_cliente4_old,   /*14*/
           cnpj_cliente4_new,   /*15*/
           cnpj_cliente2_old,   /*16*/
           cnpj_cliente2_new,   /*17*/
           nome_cliente_old,   /*18*/
           nome_cliente_new,   /*19*/
           cep_cliente_old,   /*20*/
           cep_cliente_new,   /*21*/
           cidade_cliente_old,   /*22*/
           cidade_cliente_new,   /*23*/
           uf_cliente_old,   /*24*/
           uf_cliente_new,   /*25*/
           valor_total_nf_old,   /*26*/
           valor_total_nf_new,   /*27*/
           peso_total_liq_old,   /*28*/
           peso_total_liq_new,   /*29*/
           peso_total_bru_old,   /*30*/
           peso_total_bru_new,   /*31*/
           volume_old,   /*32*/
           volume_new,   /*33*/
           qtd_volumes_old,   /*34*/
           qtd_volumes_new,   /*35*/
           tipo_volume_old,   /*36*/
           tipo_volume_new,   /*37*/
           cnpj_trans_pedi9_old,   /*38*/
           cnpj_trans_pedi9_new,   /*39*/
           cnpj_trans_pedi4_old,   /*40*/
           cnpj_trans_pedi4_new,   /*41*/
           cnpj_trans_pedi2_old,   /*42*/
           cnpj_trans_pedi2_new,   /*43*/
           data_remessa_old,   /*44*/
           data_remessa_new,   /*45*/
           cnpj_transp9_old,   /*46*/
           cnpj_transp9_new,   /*47*/
           cnpj_transp4_old,   /*48*/
           cnpj_transp4_new,   /*49*/
           cnpj_transp2_old,   /*50*/
           cnpj_transp2_new,   /*51*/
           data_retorno_old,   /*52*/
           data_retorno_new,   /*53*/
           situacao_old,   /*54*/
           situacao_new,   /*55*/
           pedido_venda_old,   /*56*/
           pedido_venda_new,   /*57*/
           situacao_pedido_old,   /*58*/
           situacao_pedido_new   /*59*/
           )values (
           'a', /*o*/
           sysdate, /*1*/
           sysdate,/*2*/
           ws_usuario_rede,/*3*/
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/
           v_nome_programa, /*7*/
           :old.numero_nf,/*8*/
           :new.numero_nf, /*9*/
           :old.serie_nf,/*10*/
           :new.serie_nf, /*11*/
           :old.cnpj_cliente9,/*12*/
           :new.cnpj_cliente9, /*13*/
           :old.cnpj_cliente4,/*14*/
           :new.cnpj_cliente4, /*15*/
           :old.cnpj_cliente2,/*16*/
           :new.cnpj_cliente2, /*17*/
           :old.nome_cliente,/*18*/
           :new.nome_cliente, /*19*/
           :old.cep_cliente,/*20*/
           :new.cep_cliente, /*21*/
           :old.cidade_cliente,/*22*/
           :new.cidade_cliente, /*23*/
           :old.uf_cliente,/*24*/
           :new.uf_cliente, /*25*/
           :old.valor_total_nf,/*26*/
           :new.valor_total_nf, /*27*/
           :old.peso_total_liq,/*28*/
           :new.peso_total_liq, /*29*/
           :old.peso_total_bru,/*30*/
           :new.peso_total_bru, /*31*/
           :old.volume,/*32*/
           :new.volume, /*33*/
           :old.qtd_volumes,/*34*/
           :new.qtd_volumes, /*35*/
           :old.tipo_volume,/*36*/
           :new.tipo_volume, /*37*/
           :old.cnpj_trans_pedi9,/*38*/
           :new.cnpj_trans_pedi9, /*39*/
           :old.cnpj_trans_pedi4,/*40*/
           :new.cnpj_trans_pedi4, /*41*/
           :old.cnpj_trans_pedi2,/*42*/
           :new.cnpj_trans_pedi2, /*43*/
           :old.data_remessa,/*44*/
           :new.data_remessa, /*45*/
           :old.cnpj_transp9,/*46*/
           :new.cnpj_transp9, /*47*/
           :old.cnpj_transp4,/*48*/
           :new.cnpj_transp4, /*49*/
           :old.cnpj_transp2,/*50*/
           :new.cnpj_transp2, /*51*/
           :old.data_retorno,/*52*/
           :new.data_retorno, /*53*/
           :old.situacao,/*54*/
           :new.situacao, /*55*/
           :old.pedido_venda,/*56*/
           :new.pedido_venda, /*57*/
           :old.situacao_pedido,/*58*/
           :new.situacao_pedido /*59*/
         );
    end;
 end if;

 if deleting
 then
    begin
       insert into INTE_070_LOG (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           numero_nf_old,   /*8*/
           numero_nf_new,   /*9*/
           serie_nf_old,   /*10*/
           serie_nf_new,   /*11*/
           cnpj_cliente9_old,   /*12*/
           cnpj_cliente9_new,   /*13*/
           cnpj_cliente4_old,   /*14*/
           cnpj_cliente4_new,   /*15*/
           cnpj_cliente2_old,   /*16*/
           cnpj_cliente2_new,   /*17*/
           nome_cliente_old,   /*18*/
           nome_cliente_new,   /*19*/
           cep_cliente_old,   /*20*/
           cep_cliente_new,   /*21*/
           cidade_cliente_old,   /*22*/
           cidade_cliente_new,   /*23*/
           uf_cliente_old,   /*24*/
           uf_cliente_new,   /*25*/
           valor_total_nf_old,   /*26*/
           valor_total_nf_new,   /*27*/
           peso_total_liq_old,   /*28*/
           peso_total_liq_new,   /*29*/
           peso_total_bru_old,   /*30*/
           peso_total_bru_new,   /*31*/
           volume_old,   /*32*/
           volume_new,   /*33*/
           qtd_volumes_old,   /*34*/
           qtd_volumes_new,   /*35*/
           tipo_volume_old,   /*36*/
           tipo_volume_new,   /*37*/
           cnpj_trans_pedi9_old,   /*38*/
           cnpj_trans_pedi9_new,   /*39*/
           cnpj_trans_pedi4_old,   /*40*/
           cnpj_trans_pedi4_new,   /*41*/
           cnpj_trans_pedi2_old,   /*42*/
           cnpj_trans_pedi2_new,   /*43*/
           data_remessa_old,   /*44*/
           data_remessa_new,   /*45*/
           cnpj_transp9_old,   /*46*/
           cnpj_transp9_new,   /*47*/
           cnpj_transp4_old,   /*48*/
           cnpj_transp4_new,   /*49*/
           cnpj_transp2_old,   /*50*/
           cnpj_transp2_new,   /*51*/
           data_retorno_old,   /*52*/
           data_retorno_new,   /*53*/
           situacao_old,   /*54*/
           situacao_new,   /*55*/
           pedido_venda_old,   /*56*/
           pedido_venda_new,   /*57*/
           situacao_pedido_old,   /*58*/
           situacao_pedido_new   /*59*/
           ) values (
           'd', /*o*/
           sysdate, /*1*/
           sysdate,/*2*/
           ws_usuario_rede,/*3*/
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/
           v_nome_programa, /*7*/
           :old.numero_nf,/*8*/
           null, /*9*/
           :old.serie_nf,/*10*/
           null, /*11*/
           :old.cnpj_cliente9,/*12*/
           null, /*13*/
           :old.cnpj_cliente4,/*14*/
           null, /*15*/
           :old.cnpj_cliente2,/*16*/
           null, /*17*/
           :old.nome_cliente,/*18*/
           null, /*19*/
           :old.cep_cliente,/*20*/
           null, /*21*/
           :old.cidade_cliente,/*22*/
           null, /*23*/
           :old.uf_cliente,/*24*/
           null, /*25*/
           :old.valor_total_nf,/*26*/
           null, /*27*/
           :old.peso_total_liq,/*28*/
           null, /*29*/
           :old.peso_total_bru,/*30*/
           null, /*31*/
           :old.volume,/*32*/
           null, /*33*/
           :old.qtd_volumes,/*34*/
           null, /*35*/
           :old.tipo_volume,/*36*/
           null, /*37*/
           :old.cnpj_trans_pedi9,/*38*/
           null, /*39*/
           :old.cnpj_trans_pedi4,/*40*/
           null, /*41*/
           :old.cnpj_trans_pedi2,/*42*/
           null, /*43*/
           :old.data_remessa,/*44*/
           null, /*45*/
           :old.cnpj_transp9,/*46*/
           null, /*47*/
           :old.cnpj_transp4,/*48*/
           null, /*49*/
           :old.cnpj_transp2,/*50*/
           null, /*51*/
           :old.data_retorno,/*52*/
           null, /*53*/
           :old.situacao,/*54*/
           null, /*55*/
           :old.pedido_venda,/*56*/
           null, /*57*/
           :old.situacao_pedido,/*58*/
           null /*59*/
         );
    end;
 end if;
end INTER_TR_INTE_070_LOG;

-- ALTER TRIGGER "INTER_TR_INTE_070_LOG" ENABLE
