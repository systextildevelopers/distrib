alter table fatu_504 add ( opcao_conf_volume  numeric(1) default 0 );

comment on column fatu_504.opcao_conf_volume
  is 'Este par�metro indica se a forma de confirmacao dos volumes sera efetuada por (0) na Nf de faturamento ou (1) na Nf de Entrega.';
/
exec inter_pr_recompile;
/
