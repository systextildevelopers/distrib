-- Inclusão de valores default
ALTER TABLE pcpc_400 MODIFY ocorrencia_id DEFAULT 0;
ALTER TABLE pcpc_400 MODIFY acao_id DEFAULT 0;

/

-- Inicialização de tabelas
INSERT INTO pcpc_405 (id, descricao) VALUES (0, '.'); 
INSERT INTO pcpc_406 (id, descricao) VALUES (0, '.');

COMMIT;

/
