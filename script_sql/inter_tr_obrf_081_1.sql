
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_081_1" 
before update of sit_areceber
    on obrf_081
for each row

declare
  v_executa_trigger      number;
  v_nro_reg              number;
begin
   -- INICIO - L�gica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementa��o executada para limpeza da base de dados do cliente
   -- e replica��o dos dados para base hist�rico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - L�gica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   v_nro_reg := 0;

   if  v_executa_trigger = 0
   and updating
   then
      if :new.sit_areceber = 4
      and :new.numero_ordem > 0
      then
         begin
            select nvl(count(1),0)
            into v_nro_reg
            from obrf_089
            where obrf_089.ordem_servico = :new.numero_ordem
              and obrf_089.sequencia     = :new.sequencia
              and obrf_089.situacao_item = 0;
         exception when others then
            v_nro_reg := 0;
         end;

         -- Neylor: Validacoes GB
         --if v_nro_reg > 0
         --then
         --   raise_application_error (-20000, 'ATEN��O! Deve ser feito a baixa da TAG de conserto antes de atender total o item da ordem de servi�o.');
         --end if;
      end if;
   end if;


end inter_tr_obrf_081_1;

-- ALTER TRIGGER "INTER_TR_OBRF_081_1" ENABLE
 

/

exec inter_pr_recompile;

