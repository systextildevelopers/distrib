
  CREATE OR REPLACE PROCEDURE "INTER_PR_ATU_PROD_ORDEM_PLANEJ" 
    -- Recebe parametros para atualizacao da quantidade produzida e/ou comprada.
   (p_area_producao            in number,
    p_ordem_producao_compra    in number,
    p_pedido_venda             in number,
    p_nivel_produto            in varchar2,
    p_grupo_produto            in varchar2,
    p_subgrupo_produto         in varchar2,
    p_item_produto             in varchar2,
    p_qtde_conserto            in number,
    p_qtde_2qualidade          in number,
    p_qtde_perdas              in number,
    p_qtde_produzida_comprada  in number,
    p_tipo_atualizacao         in varchar2)  -- "P" de producao ou "E" de estorno de producao.
is
   -- Cria cursor para executar loop na tmrp_630 buscando a ordem de planejamento e todos
   -- os produtos desta ordem de producao.
   cursor tmrp625 is
   select tmrp_625.ordem_planejamento,       tmrp_625.pedido_venda,
          tmrp_625.pedido_reserva,           tmrp_625.tipo_produto_origem,
          tmrp_625.seq_produto_origem,       tmrp_625.nivel_produto_origem,
          tmrp_625.grupo_produto_origem,     tmrp_625.subgrupo_produto_origem,
          tmrp_625.item_produto_origem,      tmrp_625.alternativa_produto_origem,
          tmrp_625.tipo_produto,             tmrp_625.seq_produto,
          tmrp_625.nivel_produto,            tmrp_625.grupo_produto,
          tmrp_625.subgrupo_produto,         tmrp_625.item_produto,
          tmrp_625.alternativa_produto,
          tmrp_625.qtde_areceber_programada, tmrp_625.qtde_produzida_comprada,
          tmrp_625.qtde_2qualidade,          tmrp_625.qtde_conserto,
          tmrp_625.qtde_perdas
   from tmrp_625, (select tmrp_630.ordem_planejamento,
                          tmrp_630.pedido_venda,
                          tmrp_630.nivel_produto,
                          tmrp_630.grupo_produto,
                          tmrp_630.subgrupo_produto,
                          tmrp_630.item_produto,
                          tmrp_630.alternativa_produto
                   from tmrp_630
                   where  tmrp_630.area_producao      = p_area_producao
                     and (tmrp_630.pedido_venda       = p_pedido_venda
                     or   p_pedido_venda              = 0)
                     and  tmrp_630.ordem_prod_compra  = p_ordem_producao_compra
                     and  tmrp_630.nivel_produto      = p_nivel_produto
                     and  tmrp_630.grupo_produto      = p_grupo_produto
                     and  tmrp_630.subgrupo_produto   = p_subgrupo_produto
                     and  tmrp_630.item_produto       = p_item_produto
                   group by tmrp_630.ordem_planejamento,
                            tmrp_630.pedido_venda,
                            tmrp_630.nivel_produto,
                            tmrp_630.grupo_produto,
                            tmrp_630.subgrupo_produto,
                            tmrp_630.item_produto,
                            tmrp_630.alternativa_produto) tmrp_630a
   where  tmrp_625.ordem_planejamento   = tmrp_630a.ordem_planejamento
     and  tmrp_625.pedido_venda         = tmrp_630a.pedido_venda
     and  tmrp_625.nivel_produto        = tmrp_630a.nivel_produto
     and  tmrp_625.grupo_produto        = tmrp_630a.grupo_produto
     and  tmrp_625.subgrupo_produto     = tmrp_630a.subgrupo_produto
     and  tmrp_625.item_produto         = tmrp_630a.item_produto
     and (tmrp_625.alternativa_produto  = tmrp_630a.alternativa_produto
     or   tmrp_630a.alternativa_produto = 0);

   -- Declara as variaveis que serao utilizadas.
   p_ordem_planejamento630         tmrp_630.ordem_planejamento%type;
   p_area_producao630              tmrp_630.area_producao%type;
   p_ordem_producao630             tmrp_630.ordem_prod_compra%type;
   p_nivel_produto630              tmrp_630.nivel_produto%type      := '#';
   p_grupo_produto630              tmrp_630.grupo_produto%type      := '#####';
   p_subgrupo_produto630           tmrp_630.subgrupo_produto%type   := '###';
   p_item_produto630               tmrp_630.item_produto%type       := '######';

   p_ordem_planejamento625         tmrp_625.ordem_planejamento%type;
   p_pedido_venda625               tmrp_625.pedido_venda%type;
   p_pedido_reserva625             tmrp_625.pedido_reserva%type;
   p_tipo_produto_origem625        tmrp_625.tipo_produto_origem%type;
   p_seq_produto_origem625         tmrp_625.seq_produto_origem%type;
   p_nivel_produto_origem625       tmrp_625.nivel_produto_origem%type;
   p_grupo_produto_origem625       tmrp_625.grupo_produto_origem%type;
   p_subgrupo_produto_origem625    tmrp_625.subgrupo_produto_origem%type;
   p_item_produto_origem625        tmrp_625.item_produto_origem%type;
   p_alt_produto_origem625         tmrp_625.alternativa_produto_origem%type;
   p_tipo_produto625               tmrp_625.tipo_produto%type;
   p_seq_produto625                tmrp_625.seq_produto%type;
   p_nivel_produto625              tmrp_625.nivel_produto%type      := '#';
   p_grupo_produto625              tmrp_625.grupo_produto%type      := '#####';
   p_subgrupo_produto625           tmrp_625.subgrupo_produto%type   := '###';
   p_item_produto625               tmrp_625.item_produto%type       := '######';
   p_alternativa_produto625        tmrp_625.alternativa_produto%type;
   p_qtde_programada625            tmrp_625.qtde_areceber_programada%type;
   p_qtde_prod_prim625             tmrp_625.qtde_produzida_comprada%type;
   p_qtde_prod_segu625             tmrp_625.qtde_2qualidade%type;
   p_qtde_prod_cons625             tmrp_625.qtde_conserto%type;
   p_qtde_prod_perd625             tmrp_625.qtde_perdas%type;

   p_qtde_ja_atualizada625         tmrp_625.qtde_produzida_comprada%type;
   p_qtde_atualizar625             tmrp_625.qtde_produzida_comprada%type;
   p_saldo_atualizar625            tmrp_625.qtde_produzida_comprada%type;
   p_achou_1_registro625           number;

   p_saldo_conserto_atu            tmrp_625.qtde_conserto%type;
   p_saldo_2qualidade_atu          tmrp_625.qtde_2qualidade%type;
   p_saldo_perdas_atu              tmrp_625.qtde_perdas%type;
   p_saldo_produzida_comprada_atu  tmrp_625.qtde_produzida_comprada%type;

   p_qtde_conserto_atu             tmrp_625.qtde_conserto%type;
   p_qtde_2qualidade_atu           tmrp_625.qtde_2qualidade%type;
   p_qtde_perdas_atu               tmrp_625.qtde_perdas%type;
   p_qtde_produzida_comprada_atu   tmrp_625.qtde_produzida_comprada%type;

begin
   -- Inicializa as variaveis de controle do saldo com a quantidade a ser atualizada.
   p_saldo_conserto_atu            := p_qtde_conserto;
   p_saldo_2qualidade_atu          := p_qtde_2qualidade;
   p_saldo_perdas_atu              := p_qtde_perdas;
   p_saldo_produzida_comprada_atu  := p_qtde_produzida_comprada;

   if p_tipo_atualizacao = 'E' and p_nivel_produto = '1'
   then
      p_saldo_conserto_atu           := (p_qtde_conserto           * -1);
      p_saldo_2qualidade_atu         := (p_qtde_2qualidade         * -1);
      p_saldo_perdas_atu             := (p_qtde_perdas             * -1);
      p_saldo_produzida_comprada_atu := (p_qtde_produzida_comprada * -1);
   end if;

   -- Verifica se e "P" de producao ou "E" de estorno de producao.
   if p_tipo_atualizacao = 'P'
   then
      if p_saldo_conserto_atu           <> 0.000 or
         p_saldo_2qualidade_atu         <> 0.000 or
         p_saldo_perdas_atu             <> 0.000 or
         p_saldo_produzida_comprada_atu <> 0.000
      then
         -- 0 - Nao achou e 1 - Achou.
         p_achou_1_registro625 := 0;

         -- Inicio do loop da atualizacao proporcionalmente a quantidade
         -- vendida por pedido da ordem de planejamento.
         for reg_tmrp625 in tmrp625
         loop
            -- Se encontrar pelo menos 1 registro, atualiza variavel.
            p_achou_1_registro625 := 1;

            -- Carrega as variaveis com os valores do cursor.
            p_ordem_planejamento625         := reg_tmrp625.ordem_planejamento;
            p_pedido_venda625               := reg_tmrp625.pedido_venda;
            p_pedido_reserva625             := reg_tmrp625.pedido_reserva;
            p_tipo_produto_origem625        := reg_tmrp625.tipo_produto_origem;
            p_seq_produto_origem625         := reg_tmrp625.seq_produto_origem;
            p_nivel_produto_origem625       := reg_tmrp625.nivel_produto_origem;
            p_grupo_produto_origem625       := reg_tmrp625.grupo_produto_origem;
            p_subgrupo_produto_origem625    := reg_tmrp625.subgrupo_produto_origem;
            p_item_produto_origem625        := reg_tmrp625.item_produto_origem;
            p_alt_produto_origem625         := reg_tmrp625.alternativa_produto_origem;
            p_tipo_produto625               := reg_tmrp625.tipo_produto;
            p_seq_produto625                := reg_tmrp625.seq_produto;
            p_nivel_produto625              := reg_tmrp625.nivel_produto;
            p_grupo_produto625              := reg_tmrp625.grupo_produto;
            p_subgrupo_produto625           := reg_tmrp625.subgrupo_produto;
            p_item_produto625               := reg_tmrp625.item_produto;
            p_alternativa_produto625        := reg_tmrp625.alternativa_produto;
            p_qtde_programada625            := reg_tmrp625.qtde_areceber_programada;
            p_qtde_prod_prim625             := reg_tmrp625.qtde_produzida_comprada;
            p_qtde_prod_segu625             := reg_tmrp625.qtde_2qualidade;
            p_qtde_prod_cons625             := reg_tmrp625.qtde_conserto;
            p_qtde_prod_perd625             := reg_tmrp625.qtde_perdas;

            p_ordem_planejamento630         := reg_tmrp625.ordem_planejamento;
            p_area_producao630              := p_area_producao;
            p_ordem_producao630             := p_ordem_producao_compra;
            p_nivel_produto630              := reg_tmrp625.nivel_produto;
            p_grupo_produto630              := reg_tmrp625.grupo_produto;
            p_subgrupo_produto630           := reg_tmrp625.subgrupo_produto;
            p_item_produto630               := reg_tmrp625.item_produto;

            -- Calcula a quantidade ja atualizada deste pedido.
            p_qtde_ja_atualizada625 := (p_qtde_prod_prim625 + p_qtde_prod_segu625 + p_qtde_prod_cons625 + p_qtde_prod_perd625);

            -- Calcula o saldo a atualizar deste pedido.
            p_saldo_atualizar625    := (p_qtde_programada625 - p_qtde_ja_atualizada625);

            -- Se o pedido tiver saldo para ser atualizado comparado com o programado, tenta atualizar este pedido.
            if p_saldo_atualizar625 <> 0.000
            then
               -- Calcula a quantidade a ser atualizada para comparar com o saldo do pedido.
               p_qtde_atualizar625 := (p_saldo_produzida_comprada_atu + p_saldo_2qualidade_atu + p_saldo_conserto_atu + p_saldo_perdas_atu);

               -- Encontra o pedido que deve ser atualizado, se ja esta todo atendido, procura o proximo pedido.
               if p_qtde_atualizar625 <= p_saldo_atualizar625
               then
                  -- Se a quantidade a atualizar for menor que o saldo a atualizar,
                  -- faz a atualizacao com o quantidade a atualizar.
                  if p_saldo_conserto_atu <> 0.000
                  then
                     p_qtde_conserto_atu  := p_qtde_atualizar625;
                  else
                     p_qtde_conserto_atu  := 0.000;
                  end if;

                  if p_saldo_2qualidade_atu <> 0.000
                  then
                     p_qtde_2qualidade_atu  := p_qtde_atualizar625;
                  else
                     p_qtde_2qualidade_atu  := 0.000;
                  end if;

                  if p_saldo_perdas_atu <> 0.000
                  then
                     p_qtde_perdas_atu  := p_qtde_atualizar625;
                  else
                     p_qtde_perdas_atu  := 0.000;
                  end if;

                  if p_saldo_produzida_comprada_atu <> 0.000
                  then
                     p_qtde_produzida_comprada_atu  := p_qtde_atualizar625;
                  else
                     p_qtde_produzida_comprada_atu  := 0.000;
                  end if;
               else
                  -- Se a quantidade a atualizar for maior que o saldo a atualizar,
                  -- faz a atualizacao somente do saldo a atualizar.
                  if p_saldo_conserto_atu <> 0.000
                  then
                     p_qtde_conserto_atu  := p_saldo_atualizar625;
                  else
                     p_qtde_conserto_atu  := 0.000;
                  end if;

                  if p_saldo_2qualidade_atu <> 0.000
                  then
                     p_qtde_2qualidade_atu  := p_saldo_atualizar625;
                  else
                     p_qtde_2qualidade_atu  := 0.000;
                  end if;

                  if p_saldo_perdas_atu <> 0.000
                  then
                     p_qtde_perdas_atu  := p_saldo_atualizar625;
                  else
                     p_qtde_perdas_atu  := 0.000;
                  end if;

                  if p_saldo_produzida_comprada_atu <> 0.000
                  then
                     p_qtde_produzida_comprada_atu  := p_saldo_atualizar625;
                  else
                     p_qtde_produzida_comprada_atu  := 0.000;
                  end if;
               end if;

               if p_qtde_conserto_atu           <> 0.000 or
                  p_qtde_2qualidade_atu         <> 0.000 or
                  p_qtde_perdas_atu             <> 0.000 or
                  p_qtde_produzida_comprada_atu <> 0.000
               then
                  update tmrp_625
                  set tmrp_625.qtde_conserto           = tmrp_625.qtde_conserto           + p_qtde_conserto_atu,
                      tmrp_625.qtde_2qualidade         = tmrp_625.qtde_2qualidade         + p_qtde_2qualidade_atu,
                      tmrp_625.qtde_perdas             = tmrp_625.qtde_perdas             + p_qtde_perdas_atu,
                      tmrp_625.qtde_produzida_comprada = tmrp_625.qtde_produzida_comprada + p_qtde_produzida_comprada_atu
                  where tmrp_625.ordem_planejamento         = p_ordem_planejamento625
                    and tmrp_625.pedido_venda               = p_pedido_venda625
                    and tmrp_625.pedido_reserva             = p_pedido_reserva625
                    and tmrp_625.tipo_produto_origem        = p_tipo_produto_origem625
                    and tmrp_625.seq_produto_origem         = p_seq_produto_origem625
                    and tmrp_625.nivel_produto_origem       = p_nivel_produto_origem625
                    and tmrp_625.grupo_produto_origem       = p_grupo_produto_origem625
                    and tmrp_625.subgrupo_produto_origem    = p_subgrupo_produto_origem625
                    and tmrp_625.item_produto_origem        = p_item_produto_origem625
                    and tmrp_625.alternativa_produto_origem = p_alt_produto_origem625
                    and tmrp_625.tipo_produto               = p_tipo_produto625
                    and tmrp_625.seq_produto                = p_seq_produto625
                    and tmrp_625.nivel_produto              = p_nivel_produto625
                    and tmrp_625.grupo_produto              = p_grupo_produto625
                    and tmrp_625.subgrupo_produto           = p_subgrupo_produto625
                    and tmrp_625.item_produto               = p_item_produto625
                    and tmrp_625.alternativa_produto        = p_alternativa_produto625;

                  -- Atualiza a quantidade produzida para o produto e pedido.
                  update tmrp_630
                  set tmrp_630.qtde_conserto           = tmrp_630.qtde_conserto           + p_qtde_conserto_atu,
                      tmrp_630.qtde_2qualidade         = tmrp_630.qtde_2qualidade         + p_qtde_2qualidade_atu,
                      tmrp_630.qtde_perdas             = tmrp_630.qtde_perdas             + p_qtde_perdas_atu,
                      tmrp_630.qtde_produzida_comprada = tmrp_630.qtde_produzida_comprada + p_qtde_produzida_comprada_atu
                  where tmrp_630.ordem_planejamento = p_ordem_planejamento630
                    and tmrp_630.pedido_venda       = p_pedido_venda625
                    and tmrp_630.area_producao      = p_area_producao630
                    and tmrp_630.ordem_prod_compra  = p_ordem_producao630
                    and tmrp_630.nivel_produto      = p_nivel_produto630
                    and tmrp_630.grupo_produto      = p_grupo_produto630
                    and tmrp_630.subgrupo_produto   = p_subgrupo_produto630
                    and tmrp_630.item_produto       = p_item_produto630;

                  if SQL%notfound
                  then
                     -- Atualiza a quantidade produzida para o produto, ja que o pedido esta zerado.
                     update tmrp_630
                     set tmrp_630.qtde_conserto           = tmrp_630.qtde_conserto           + p_qtde_conserto_atu,
                         tmrp_630.qtde_2qualidade         = tmrp_630.qtde_2qualidade         + p_qtde_2qualidade_atu,
                         tmrp_630.qtde_perdas             = tmrp_630.qtde_perdas             + p_qtde_perdas_atu,
                         tmrp_630.qtde_produzida_comprada = tmrp_630.qtde_produzida_comprada + p_qtde_produzida_comprada_atu
                     where tmrp_630.ordem_planejamento = p_ordem_planejamento630
                       and tmrp_630.area_producao      = p_area_producao630
                       and tmrp_630.ordem_prod_compra  = p_ordem_producao630
                       and tmrp_630.nivel_produto      = p_nivel_produto630
                       and tmrp_630.grupo_produto      = p_grupo_produto630
                       and tmrp_630.subgrupo_produto   = p_subgrupo_produto630
                       and tmrp_630.item_produto       = p_item_produto630;
                  end if;

                  -- Se atualizou conserto.
                  if p_qtde_conserto_atu <> 0.000
                  then
                     p_saldo_conserto_atu  := p_saldo_conserto_atu - p_qtde_conserto_atu;
                  end if;

                  -- Se atualizou segunda qualidade.
                  if p_qtde_2qualidade_atu <> 0.000
                  then
                     p_saldo_2qualidade_atu  := p_saldo_2qualidade_atu - p_qtde_2qualidade_atu;
                  end if;

                  -- Se atualizou perdas.
                  if p_qtde_perdas_atu <> 0.000
                  then
                     p_saldo_perdas_atu  := p_saldo_perdas_atu - p_qtde_perdas_atu;
                  end if;

                  -- Se atualizou primeira qualidade.
                  if p_qtde_produzida_comprada_atu <> 0.000
                  then
                     p_saldo_produzida_comprada_atu  := p_saldo_produzida_comprada_atu - p_qtde_produzida_comprada_atu;
                  end if;
               end if;
            end if;
         end loop;

         -- Se nao encontrou registro para atualizar a quantidade produzida ou
         --  tem saldo a atualizar, faz atualiza no ultimo registro encontrado.
         if  p_achou_1_registro625           = 1     and
            (p_saldo_conserto_atu           <> 0.000 or
             p_saldo_2qualidade_atu         <> 0.000 or
             p_saldo_perdas_atu             <> 0.000 or
             p_saldo_produzida_comprada_atu <> 0.000)
         then
            update tmrp_625
            set tmrp_625.qtde_conserto           = tmrp_625.qtde_conserto           + p_saldo_conserto_atu,
                tmrp_625.qtde_2qualidade         = tmrp_625.qtde_2qualidade         + p_saldo_2qualidade_atu,
                tmrp_625.qtde_perdas             = tmrp_625.qtde_perdas             + p_saldo_perdas_atu,
                tmrp_625.qtde_produzida_comprada = tmrp_625.qtde_produzida_comprada + p_saldo_produzida_comprada_atu
            where tmrp_625.ordem_planejamento         = p_ordem_planejamento625
              and tmrp_625.pedido_venda               = p_pedido_venda625
              and tmrp_625.pedido_reserva             = p_pedido_reserva625
              and tmrp_625.tipo_produto_origem        = p_tipo_produto_origem625
              and tmrp_625.seq_produto_origem         = p_seq_produto_origem625
              and tmrp_625.nivel_produto_origem       = p_nivel_produto_origem625
              and tmrp_625.grupo_produto_origem       = p_grupo_produto_origem625
              and tmrp_625.subgrupo_produto_origem    = p_subgrupo_produto_origem625
              and tmrp_625.item_produto_origem        = p_item_produto_origem625
              and tmrp_625.alternativa_produto_origem = p_alt_produto_origem625
              and tmrp_625.tipo_produto               = p_tipo_produto625
              and tmrp_625.seq_produto                = p_seq_produto625
              and tmrp_625.nivel_produto              = p_nivel_produto625
              and tmrp_625.grupo_produto              = p_grupo_produto625
              and tmrp_625.subgrupo_produto           = p_subgrupo_produto625
              and tmrp_625.item_produto               = p_item_produto625
              and tmrp_625.alternativa_produto        = p_alternativa_produto625;

            -- Atualiza a quantidade produzida para o produto e pedido.
            update tmrp_630
            set tmrp_630.qtde_conserto           = tmrp_630.qtde_conserto           + p_saldo_conserto_atu,
                tmrp_630.qtde_2qualidade         = tmrp_630.qtde_2qualidade         + p_saldo_2qualidade_atu,
                tmrp_630.qtde_perdas             = tmrp_630.qtde_perdas             + p_saldo_perdas_atu,
                tmrp_630.qtde_produzida_comprada = tmrp_630.qtde_produzida_comprada + p_saldo_produzida_comprada_atu
            where tmrp_630.ordem_planejamento = p_ordem_planejamento630
              and tmrp_630.pedido_venda       = p_pedido_venda625
              and tmrp_630.area_producao      = p_area_producao630
              and tmrp_630.ordem_prod_compra  = p_ordem_producao630
              and tmrp_630.nivel_produto      = p_nivel_produto630
              and tmrp_630.grupo_produto      = p_grupo_produto630
              and tmrp_630.subgrupo_produto   = p_subgrupo_produto630
              and tmrp_630.item_produto       = p_item_produto630;

            if SQL%notfound
            then
               -- Atualiza a quantidade produzida para o produto, ja que o pedido esta zerado.
               update tmrp_630
               set tmrp_630.qtde_conserto           = tmrp_630.qtde_conserto           + p_saldo_conserto_atu,
                   tmrp_630.qtde_2qualidade         = tmrp_630.qtde_2qualidade         + p_saldo_2qualidade_atu,
                   tmrp_630.qtde_perdas             = tmrp_630.qtde_perdas             + p_saldo_perdas_atu,
                   tmrp_630.qtde_produzida_comprada = tmrp_630.qtde_produzida_comprada + p_saldo_produzida_comprada_atu
               where tmrp_630.ordem_planejamento = p_ordem_planejamento630
                 and tmrp_630.area_producao      = p_area_producao630
                 and tmrp_630.ordem_prod_compra  = p_ordem_producao630
                 and tmrp_630.nivel_produto      = p_nivel_produto630
                 and tmrp_630.grupo_produto      = p_grupo_produto630
                 and tmrp_630.subgrupo_produto   = p_subgrupo_produto630
                 and tmrp_630.item_produto       = p_item_produto630;
            end if;
         end if;
      end if;
   end if;

   -- Verifica se e "P" de producao ou "E" de estorno de producao.
   if p_tipo_atualizacao = 'E'
   then
      if p_saldo_conserto_atu           <> 0.000 or
         p_saldo_2qualidade_atu         <> 0.000 or
         p_saldo_perdas_atu             <> 0.000 or
         p_saldo_produzida_comprada_atu <> 0.000
      then
         -- 0 - Nao achou e 1 - Achou.
         p_achou_1_registro625 := 0;

         -- Inicio do loop da atualizacao proporcionalmente a quantidade
         -- vendida por pedido da ordem de planejamento.
         for reg_tmrp625 in tmrp625
         loop
            -- Se encontrar pelo menos 1 registro, atualiza variavel.
            p_achou_1_registro625 := 1;

            -- Carrega as variaveis com os valores do cursor.
            p_ordem_planejamento625         := reg_tmrp625.ordem_planejamento;
            p_pedido_venda625               := reg_tmrp625.pedido_venda;
            p_pedido_reserva625             := reg_tmrp625.pedido_reserva;
            p_tipo_produto_origem625        := reg_tmrp625.tipo_produto_origem;
            p_seq_produto_origem625         := reg_tmrp625.seq_produto_origem;
            p_nivel_produto_origem625       := reg_tmrp625.nivel_produto_origem;
            p_grupo_produto_origem625       := reg_tmrp625.grupo_produto_origem;
            p_subgrupo_produto_origem625    := reg_tmrp625.subgrupo_produto_origem;
            p_item_produto_origem625        := reg_tmrp625.item_produto_origem;
            p_alt_produto_origem625         := reg_tmrp625.alternativa_produto_origem;
            p_tipo_produto625               := reg_tmrp625.tipo_produto;
            p_seq_produto625                := reg_tmrp625.seq_produto;
            p_nivel_produto625              := reg_tmrp625.nivel_produto;
            p_grupo_produto625              := reg_tmrp625.grupo_produto;
            p_subgrupo_produto625           := reg_tmrp625.subgrupo_produto;
            p_item_produto625               := reg_tmrp625.item_produto;
            p_alternativa_produto625        := reg_tmrp625.alternativa_produto;
            p_qtde_programada625            := reg_tmrp625.qtde_areceber_programada;
            p_qtde_prod_prim625             := reg_tmrp625.qtde_produzida_comprada;
            p_qtde_prod_segu625             := reg_tmrp625.qtde_2qualidade;
            p_qtde_prod_cons625             := reg_tmrp625.qtde_conserto;
            p_qtde_prod_perd625             := reg_tmrp625.qtde_perdas;

            p_ordem_planejamento630         := reg_tmrp625.ordem_planejamento;
            p_area_producao630              := p_area_producao;
            p_ordem_producao630             := p_ordem_producao_compra;
            p_nivel_produto630              := reg_tmrp625.nivel_produto;
            p_grupo_produto630              := reg_tmrp625.grupo_produto;
            p_subgrupo_produto630           := reg_tmrp625.subgrupo_produto;
            p_item_produto630               := reg_tmrp625.item_produto;

            -- Calcula a quantidade ja atualizada deste pedido.
            p_qtde_ja_atualizada625 := (p_qtde_prod_prim625 + p_qtde_prod_segu625 + p_qtde_prod_cons625 + p_qtde_prod_perd625);

            -- Encontra o pedido que deve ser estornada a quantidade, nao podera estornar do pedido que nao tem
            -- a quantidade atualizada como produzida para ser estornada.
            if p_qtde_ja_atualizada625 <> 0.000
            then
               -- Calcula a quantidade a ser atualizada para comparar com o saldo do pedido.
               p_qtde_atualizar625 := (p_saldo_produzida_comprada_atu + p_saldo_2qualidade_atu + p_saldo_conserto_atu + p_saldo_perdas_atu);

               -- Encontra a quantidade que deve ser estornada para este pedido.
               if p_qtde_atualizar625 <= p_qtde_ja_atualizada625
               then
                  -- Se a quantidade a atualizar for menor que a quantidade ja atualizada,
                  -- faz a atualizacao do estorno com a quantidade a atualizar.
                  if p_saldo_conserto_atu <> 0.000
                  then
                     p_qtde_conserto_atu  := p_qtde_atualizar625;
                  else
                     p_qtde_conserto_atu  := 0.000;
                  end if;

                  if p_saldo_2qualidade_atu <> 0.000
                  then
                     p_qtde_2qualidade_atu  := p_qtde_atualizar625;
                  else
                     p_qtde_2qualidade_atu  := 0.000;
                  end if;

                  if p_saldo_perdas_atu <> 0.000
                  then
                     p_qtde_perdas_atu  := p_qtde_atualizar625;
                  else
                     p_qtde_perdas_atu  := 0.000;
                  end if;

                  if p_saldo_produzida_comprada_atu <> 0.000
                  then
                     p_qtde_produzida_comprada_atu  := p_qtde_atualizar625;
                  else
                     p_qtde_produzida_comprada_atu  := 0.000;
                  end if;
               else
                  -- Se a quantidade a atualizar for maior que a quantidade ja atualizada,
                  -- faz a atualizacao do estorno com a quantidade ja atualizada.
                  if p_saldo_conserto_atu <> 0.000
                  then
                     p_qtde_conserto_atu  := p_qtde_ja_atualizada625;
                  else
                     p_qtde_conserto_atu  := 0.000;
                  end if;

                  if p_saldo_2qualidade_atu <> 0.000
                  then
                     p_qtde_2qualidade_atu  := p_qtde_ja_atualizada625;
                  else
                     p_qtde_2qualidade_atu  := 0.000;
                  end if;

                  if p_saldo_perdas_atu <> 0.000
                  then
                     p_qtde_perdas_atu  := p_qtde_ja_atualizada625;
                  else
                     p_qtde_perdas_atu  := 0.000;
                  end if;

                  if p_saldo_produzida_comprada_atu <> 0.000
                  then
                     p_qtde_produzida_comprada_atu  := p_qtde_ja_atualizada625;
                  else
                     p_qtde_produzida_comprada_atu  := 0.000;
                  end if;
               end if;

               if p_qtde_conserto_atu           <> 0.000 or
                  p_qtde_2qualidade_atu         <> 0.000 or
                  p_qtde_perdas_atu             <> 0.000 or
                  p_qtde_produzida_comprada_atu <> 0.000
               then
                  update tmrp_625
                  set tmrp_625.qtde_conserto           = tmrp_625.qtde_conserto           - p_qtde_conserto_atu,
                      tmrp_625.qtde_2qualidade         = tmrp_625.qtde_2qualidade         - p_qtde_2qualidade_atu,
                      tmrp_625.qtde_perdas             = tmrp_625.qtde_perdas             - p_qtde_perdas_atu,
                      tmrp_625.qtde_produzida_comprada = tmrp_625.qtde_produzida_comprada - p_qtde_produzida_comprada_atu
                  where tmrp_625.ordem_planejamento         = p_ordem_planejamento625
                    and tmrp_625.pedido_venda               = p_pedido_venda625
                    and tmrp_625.pedido_reserva             = p_pedido_reserva625
                    and tmrp_625.tipo_produto_origem        = p_tipo_produto_origem625
                    and tmrp_625.seq_produto_origem         = p_seq_produto_origem625
                    and tmrp_625.nivel_produto_origem       = p_nivel_produto_origem625
                    and tmrp_625.grupo_produto_origem       = p_grupo_produto_origem625
                    and tmrp_625.subgrupo_produto_origem    = p_subgrupo_produto_origem625
                    and tmrp_625.item_produto_origem        = p_item_produto_origem625
                    and tmrp_625.alternativa_produto_origem = p_alt_produto_origem625
                    and tmrp_625.tipo_produto               = p_tipo_produto625
                    and tmrp_625.seq_produto                = p_seq_produto625
                    and tmrp_625.nivel_produto              = p_nivel_produto625
                    and tmrp_625.grupo_produto              = p_grupo_produto625
                    and tmrp_625.subgrupo_produto           = p_subgrupo_produto625
                    and tmrp_625.item_produto               = p_item_produto625
                    and tmrp_625.alternativa_produto        = p_alternativa_produto625;

                  -- Atualiza a quantidade produzida para o produto e pedido.
                  update tmrp_630
                  set tmrp_630.qtde_conserto           = tmrp_630.qtde_conserto           - p_qtde_conserto_atu,
                      tmrp_630.qtde_2qualidade         = tmrp_630.qtde_2qualidade         - p_qtde_2qualidade_atu,
                      tmrp_630.qtde_perdas             = tmrp_630.qtde_perdas             - p_qtde_perdas_atu,
                      tmrp_630.qtde_produzida_comprada = tmrp_630.qtde_produzida_comprada - p_qtde_produzida_comprada_atu
                  where tmrp_630.ordem_planejamento = p_ordem_planejamento630
                    and tmrp_630.pedido_venda       = p_pedido_venda625
                    and tmrp_630.area_producao      = p_area_producao630
                    and tmrp_630.ordem_prod_compra  = p_ordem_producao630
                    and tmrp_630.nivel_produto      = p_nivel_produto630
                    and tmrp_630.grupo_produto      = p_grupo_produto630
                    and tmrp_630.subgrupo_produto   = p_subgrupo_produto630
                    and tmrp_630.item_produto       = p_item_produto630;

                  if SQL%notfound
                  then
                     -- Atualiza a quantidade produzida para o produto, ja que o pedido esta zerado.
                     update tmrp_630
                     set tmrp_630.qtde_conserto           = tmrp_630.qtde_conserto           - p_qtde_conserto_atu,
                         tmrp_630.qtde_2qualidade         = tmrp_630.qtde_2qualidade         - p_qtde_2qualidade_atu,
                         tmrp_630.qtde_perdas             = tmrp_630.qtde_perdas             - p_qtde_perdas_atu,
                         tmrp_630.qtde_produzida_comprada = tmrp_630.qtde_produzida_comprada - p_qtde_produzida_comprada_atu
                     where tmrp_630.ordem_planejamento = p_ordem_planejamento630
                       and tmrp_630.area_producao      = p_area_producao630
                       and tmrp_630.ordem_prod_compra  = p_ordem_producao630
                       and tmrp_630.nivel_produto      = p_nivel_produto630
                       and tmrp_630.grupo_produto      = p_grupo_produto630
                       and tmrp_630.subgrupo_produto   = p_subgrupo_produto630
                       and tmrp_630.item_produto       = p_item_produto630;
                  end if;

                  -- Se atualizou conserto.
                  if p_qtde_conserto_atu <> 0.000
                  then
                     p_saldo_conserto_atu  := p_saldo_conserto_atu - p_qtde_conserto_atu;
                  end if;

                  -- Se atualizou segunda qualidade.
                  if p_qtde_2qualidade_atu <> 0.000
                  then
                     p_saldo_2qualidade_atu  := p_saldo_2qualidade_atu - p_qtde_2qualidade_atu;
                  end if;

                  -- Se atualizou perdas.
                  if p_qtde_perdas_atu <> 0.000
                  then
                     p_saldo_perdas_atu  := p_saldo_perdas_atu - p_qtde_perdas_atu;
                  end if;

                  -- Se atualizou primeira qualidade.
                  if p_qtde_produzida_comprada_atu <> 0.000
                  then
                     p_saldo_produzida_comprada_atu  := p_saldo_produzida_comprada_atu - p_qtde_produzida_comprada_atu;
                  end if;
               end if;
            end if;
         end loop;

         -- Se nao encontrou registro para atualizar a quantidade produzida ou
         --  tem saldo a atualizar, faz atualiza no ultimo registro encontrado.
         if  p_achou_1_registro625           = 1     and
            (p_saldo_conserto_atu           <> 0.000 or
             p_saldo_2qualidade_atu         <> 0.000 or
             p_saldo_perdas_atu             <> 0.000 or
             p_saldo_produzida_comprada_atu <> 0.000)
         then
            update tmrp_625
            set tmrp_625.qtde_conserto           = tmrp_625.qtde_conserto           - p_saldo_conserto_atu,
                tmrp_625.qtde_2qualidade         = tmrp_625.qtde_2qualidade         - p_saldo_2qualidade_atu,
                tmrp_625.qtde_perdas             = tmrp_625.qtde_perdas             - p_saldo_perdas_atu,
                tmrp_625.qtde_produzida_comprada = tmrp_625.qtde_produzida_comprada - p_saldo_produzida_comprada_atu
            where tmrp_625.ordem_planejamento         = p_ordem_planejamento625
              and tmrp_625.pedido_venda               = p_pedido_venda625
              and tmrp_625.pedido_reserva             = p_pedido_reserva625
              and tmrp_625.tipo_produto_origem        = p_tipo_produto_origem625
              and tmrp_625.seq_produto_origem         = p_seq_produto_origem625
              and tmrp_625.nivel_produto_origem       = p_nivel_produto_origem625
              and tmrp_625.grupo_produto_origem       = p_grupo_produto_origem625
              and tmrp_625.subgrupo_produto_origem    = p_subgrupo_produto_origem625
              and tmrp_625.item_produto_origem        = p_item_produto_origem625
              and tmrp_625.alternativa_produto_origem = p_alt_produto_origem625
              and tmrp_625.tipo_produto               = p_tipo_produto625
              and tmrp_625.seq_produto                = p_seq_produto625
              and tmrp_625.nivel_produto              = p_nivel_produto625
              and tmrp_625.grupo_produto              = p_grupo_produto625
              and tmrp_625.subgrupo_produto           = p_subgrupo_produto625
              and tmrp_625.item_produto               = p_item_produto625
              and tmrp_625.alternativa_produto        = p_alternativa_produto625;

            -- Atualiza a quantidade produzida para o produto e pedido.
            update tmrp_630
            set tmrp_630.qtde_conserto           = tmrp_630.qtde_conserto           - p_saldo_conserto_atu,
                tmrp_630.qtde_2qualidade         = tmrp_630.qtde_2qualidade         - p_saldo_2qualidade_atu,
                tmrp_630.qtde_perdas             = tmrp_630.qtde_perdas             - p_saldo_perdas_atu,
                tmrp_630.qtde_produzida_comprada = tmrp_630.qtde_produzida_comprada - p_saldo_produzida_comprada_atu
            where tmrp_630.ordem_planejamento = p_ordem_planejamento630
              and tmrp_630.pedido_venda       = p_pedido_venda625
              and tmrp_630.area_producao      = p_area_producao630
              and tmrp_630.ordem_prod_compra  = p_ordem_producao630
              and tmrp_630.nivel_produto      = p_nivel_produto630
              and tmrp_630.grupo_produto      = p_grupo_produto630
              and tmrp_630.subgrupo_produto   = p_subgrupo_produto630
              and tmrp_630.item_produto       = p_item_produto630;

            if SQL%notfound
            then
               -- Atualiza a quantidade produzida para o produto, ja que o pedido esta zerado.
               update tmrp_630
               set tmrp_630.qtde_conserto           = tmrp_630.qtde_conserto           - p_saldo_conserto_atu,
                   tmrp_630.qtde_2qualidade         = tmrp_630.qtde_2qualidade         - p_saldo_2qualidade_atu,
                   tmrp_630.qtde_perdas             = tmrp_630.qtde_perdas             - p_saldo_perdas_atu,
                   tmrp_630.qtde_produzida_comprada = tmrp_630.qtde_produzida_comprada - p_saldo_produzida_comprada_atu
               where tmrp_630.ordem_planejamento = p_ordem_planejamento630
                 and tmrp_630.area_producao      = p_area_producao630
                 and tmrp_630.ordem_prod_compra  = p_ordem_producao630
                 and tmrp_630.nivel_produto      = p_nivel_produto630
                 and tmrp_630.grupo_produto      = p_grupo_produto630
                 and tmrp_630.subgrupo_produto   = p_subgrupo_produto630
                 and tmrp_630.item_produto       = p_item_produto630;
            end if;
         end if;
      end if;
   end if;

end INTER_PR_ATU_PROD_ORDEM_PLANEJ;

 

/

exec inter_pr_recompile;

