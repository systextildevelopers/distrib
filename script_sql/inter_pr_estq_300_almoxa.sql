CREATE OR REPLACE PROCEDURE "INTER_PR_ESTQ_300_ALMOXA" 
   (p_codigo_deposito         in number,   p_nivel_estrutura          in varchar2,
    p_grupo_estrutura         in varchar2, p_subgrupo_estrutura       in varchar2,
    p_item_estrutura          in varchar2, p_data_movimento           in date,
    p_numero_lote             in number,   p_numero_documento         in number,
    p_serie_documento         in varchar2, p_cnpj_9                   in number,
    p_cnpj_4                  in number,   p_cnpj_2                   in number,
    p_sequencia_documento     in number,   p_codigo_transacao         in number,
    p_entrada_saida           in varchar2, p_centro_custo             in number,
    p_quantidade              in number,   p_valor_movimento_unitario in number,
    p_valor_contabil_unitario in number,   p_usuario_systextil        in varchar2,
    p_tabela_origem           in varchar2, p_processo_systextil       in varchar2,
    p_quantidade_quilo        in number,   p_numero_op				  in number,
    p_estagio_op			  in number)
is


   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuï¿¿rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid); 

   if  p_processo_systextil is not null 
   then 
      v_nome_programa := p_processo_systextil;
   end if;
   
   if  p_usuario_systextil is not null 
   then 
     ws_usuario_systextil:= p_usuario_systextil;   
   end if;
  
   insert into estq_300
     (codigo_deposito,           nivel_estrutura,            grupo_estrutura,
      subgrupo_estrutura,        item_estrutura,             data_movimento,
      numero_lote,               numero_documento,           serie_documento,
      cnpj_9,                    cnpj_4,                     cnpj_2,
      sequencia_documento,
      codigo_transacao,          entrada_saida,              centro_custo,
      quantidade,                valor_movimento_unitario,   valor_contabil_unitario,
      usuario_systextil,         tabela_origem,              processo_systextil,
      quantidade_quilo,			 numero_op,					 estagio_op)
   values
      (p_codigo_deposito,        p_nivel_estrutura,          p_grupo_estrutura,
       p_subgrupo_estrutura,     p_item_estrutura,           p_data_movimento,
       p_numero_lote,            p_numero_documento,         p_serie_documento,
       p_cnpj_9,                 p_cnpj_4,                   p_cnpj_2,
       p_sequencia_documento,
       p_codigo_transacao,       p_entrada_saida,            p_centro_custo,
       p_quantidade,             p_valor_movimento_unitario, p_valor_contabil_unitario,
       ws_usuario_systextil,     p_tabela_origem,            SUBSTR(v_nome_programa,1,15),
       p_quantidade_quilo,		 p_numero_op,				 p_estagio_op);

   exception when others 
   then raise_application_error(-20000,'Problema ao inserir dados na tabela estq_300. Erro banco de dados:  '||SQLERRM);
end inter_pr_estq_300_almoxa;

/
exec inter_pr_recompile;
