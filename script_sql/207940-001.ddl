alter table pedi_051
add priorizar_num_end_cliente  VARCHAR2(1) default 'N';

comment on column pedi_051.priorizar_num_end_cliente
  is 'Utilizado para priorizar n�mero im�vel/complemento no endere�o do cliente.';

/
exec inter_pr_recompile;
