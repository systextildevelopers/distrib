INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'obrf_e275',     1,
	1,			     'Execu��o autom�tica de leitura de XML');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'obrf_e275',   'menu_mp05', 
	1,             1, 
	'S',           'S', 
	'S',           'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Execu��o autom�tica de leitura de XML'
 WHERE hdoc_036.codigo_programa = 'obrf_e275'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;
