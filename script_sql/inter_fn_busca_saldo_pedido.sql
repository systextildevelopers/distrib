create or replace FUNCTION "INTER_FN_BUSCA_SALDO_PEDIDO" ( p_pedido_compra in number)
return
  number
is
  v_return number;
  v_valor_saldo_pedido number;
  v_valor_liq number;
  v_valor_desc_prod number;
  v_valor_ipi_prod number;
  v_valor_enc_prod number;
  v_valor_iva number;
  v_valor_subs_prod number;
begin
    v_valor_saldo_pedido := 0;
    v_valor_liq := 0;
    v_valor_desc_prod := 0;
    v_valor_ipi_prod := 0;
    v_valor_enc_prod := 0;
    v_valor_iva := 0;
    v_valor_subs_prod := 0;
    
    for qtdeSaldo in (  select supr_100.qtde_pedida_item,       supr_100.qtde_saldo_item,
                                supr_100.preco_item_comp,       supr_100.cod_cancelamento,
                                nvl(supr_100.fator_conv, 0) as fatorConv,    nvl(supr_100.valor_conv, 0) as valorConv,
                                supr_100.percentual_desc,       supr_100.percentual_ipi,
                                supr_100.perc_enc_finan,        supr_100.perc_iva,
                                nvl(supr_100.percentual_subs, 0) as percentual_subs

                        from supr_100
                        where supr_100.num_ped_compra = p_pedido_compra
                        and supr_100.cod_cancelamento = 0 )
    LOOP
        if qtdeSaldo.cod_cancelamento = 0 then
            if qtdeSaldo.valorConv > 0 and qtdeSaldo.fatorConv > 0 then
                v_valor_saldo_pedido := v_valor_saldo_pedido + ( (qtdeSaldo.qtde_saldo_item / qtdeSaldo.fatorConv) * qtdeSaldo.valorConv ) ;
            else
                v_valor_desc_prod := (qtdeSaldo.preco_item_comp * qtdeSaldo.percentual_desc / 100.000);
                v_valor_ipi_prod := ((qtdeSaldo.preco_item_comp - v_valor_desc_prod) * qtdeSaldo.percentual_ipi) / 100.000;
                v_valor_enc_prod := (qtdeSaldo.preco_item_comp * qtdeSaldo.perc_enc_finan / 100.000);
                v_valor_iva := (qtdeSaldo.preco_item_comp * qtdeSaldo.perc_iva / 100.000);
                v_valor_subs_prod := ((qtdeSaldo.preco_item_comp - v_valor_desc_prod) * qtdeSaldo.percentual_subs) / 100.000;

                v_valor_liq := qtdeSaldo.preco_item_comp - v_valor_desc_prod + v_valor_ipi_prod + v_valor_enc_prod + v_valor_iva + v_valor_subs_prod;
                v_valor_saldo_pedido := v_valor_saldo_pedido + (qtdeSaldo.qtde_saldo_item * v_valor_liq);
            end if;
        end if;
    END LOOP;

    if v_valor_saldo_pedido is null then
        v_valor_saldo_pedido := 0;
    end if;
    return v_valor_saldo_pedido;
end;

/

exec inter_pr_recompile;
/
