
  CREATE OR REPLACE VIEW "ESTQ_060_PCPT_028" ("NR_CAIXA", "TURNO", "NIVEL", "GRUPO", "SUBGRUPO", "ITEM", "LOTE", "PESO_LIQUIDO", "PESO_EMBALAGEM", "PESO_BRUTO", "CODIGO_DEPOSITO", "DATA_PRODUCAO", "ENDERECO_CAIXA", "STATUS_CAIXA", "QUALIDADE_FIO", "TRANSACAO_ENT", "NUMERO_PEDIDO", "SEQUENCIA_PEDIDO", "NOTA_FISCAL", "SERIE_NOTA", "PRE_ROMANEIO", "GRUPO_MAQUINA", "SUB_MAQUINA", "NUMERO_MAQUINA", "NUMERO_OP", "ORDEM_AGRUPAMENTO", "CAIXA_ORIGINAL", "TURNO_ORIGINAL", "PESO_ADICIONAL", "NOTA_FISC_ENT", "SERI_FISC_ENT", "ARREADA", "LOTE_PREPARACAO", "RESTRICAO", "DEPOSITO_PRODUCAO", "TRANSACAO_PRODUCAO", "CODIGO_EMBALAGEM", "QTDE_PECAS_EMB", "SEQU_FISC_ENT", "CNPJ_FORNECEDOR9", "CNPJ_FORNECEDOR4", "CNPJ_FORNECEDOR2", "PERIODO_PRODUCAO") AS
  SELECT
 estq_060.numero_caixa      , estq_060.turno             ,
 estq_060.prodcai_nivel99   , estq_060.prodcai_grupo     ,
 estq_060.prodcai_subgrupo  , estq_060.prodcai_item      ,
 estq_060.lote              , estq_060.peso_liquido      ,
 estq_060.peso_embalagem    , estq_060.peso_bruto        ,
 estq_060.codigo_deposito   , estq_060.data_producao     ,
 estq_060.endereco_caixa    , estq_060.status_caixa      ,
 estq_060.qualidade_fio     , estq_060.transacao_ent     ,
 estq_060.numero_pedido     , estq_060.sequencia_pedido  ,
 estq_060.nota_fiscal       , estq_060.serie_nota        ,
 estq_060.pre_romaneio      , estq_060.grupo_maquina     ,
 estq_060.sub_maquina       , estq_060.numero_maquina    ,
 estq_060.ordem_producao    , estq_060.ordem_agrupamento ,
 estq_060.caixa_original    , estq_060.turno_original    ,
 estq_060.peso_adicional    , estq_060.nota_fisc_ent     ,
 estq_060.seri_fisc_ent     , estq_060.arreada           ,
 pcpt_028.ordem_producao    , estq_060.restricao         ,
 estq_060.deposito_producao , estq_060.transacao_producao,
 estq_060.codigo_embalagem  , estq_060.qtde_pecas_emb    ,
 estq_060.sequ_fisc_ent     , estq_060.cnpj_fornecedor9  ,
 estq_060.cnpj_fornecedor4  , estq_060.cnpj_fornecedor2  ,
 estq_060.periodo_producao
 from pcpt_028,estq_060
 where estq_060.numero_caixa    = pcpt_028.codigo_caixa(+)
   and estq_060.turno           = pcpt_028.turno_caixa(+)
   and estq_060.codigo_deposito = pcpt_028.codigo_deposito(+);
