ALTER TABLE OBRF_180
ADD (COD_LOCAL_CLASS_PEDIDO   VARCHAR2(1) default 'D',
    COD_MSG_CLASS_PEDIDO      NUMBER(6) default 0,
    COD_SEQ_CLASS_PEDIDO      NUMBER(2) default 0);

/

exec inter_pr_recompile;
