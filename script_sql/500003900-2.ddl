alter table basi_010 add fator_umed_trib number(14,5);
alter table basi_010 modify fator_umed_trib default 0.0;
comment on column basi_010.fator_umed_trib is 'Fator de multiplicacao para se obter o tag de quantidade tributavel no xml da nota';
exec inter_pr_recompile;
