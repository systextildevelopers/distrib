
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_900_LOG" 
before insert or
       delete or
       update on pedi_900
for each row

declare

  v_origem_pedido_old   pedi_900.origem_pedido%type;
  v_motivo_bloqueio_old pedi_900.motivo_bloqueio%type;
  v_origem_pedido_new   pedi_900.origem_pedido%type;
  v_motivo_bloqueio_new pedi_900.motivo_bloqueio%type;

  v_usuario_systextil hdoc_030.usuario%type;
  v_operacao          varchar(1);
  v_data_operacao     date;
  v_usuario_rede      varchar(20);
  v_maquina_rede      varchar(40);
  v_aplicativo        varchar(20);
  v_empresa           number(3);
  v_locale_usuario   varchar2(5);

  v_sid               sys.gv_$session.sid%type;

  v_executa_trigger number(1);

begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      v_data_operacao := sysdate();

      -- Dados do usu�rio logado
      inter_pr_dados_usuario (v_usuario_rede,      v_maquina_rede, v_aplicativo,     v_sid,
                              v_usuario_systextil, v_empresa,      v_locale_usuario);

      if inserting or updating
      then
         if inserting
         then v_operacao := 'I';
         else v_operacao := 'U';
         end if;

         v_origem_pedido_new   := :new.origem_pedido;
         v_motivo_bloqueio_new := :new.motivo_bloqueio;

      end if;

      if deleting or updating
      then
         if deleting
         then v_operacao := 'D';
         else v_operacao := 'U';
         end if;

         v_origem_pedido_old   := :old.origem_pedido;
         v_motivo_bloqueio_old := :old.motivo_bloqueio;

      end if;

      begin
         insert into pedi_900_log
           (operacao,                data_operacao,
            usuario_rede,            maquina_rede,
            aplicativo,              usuario_systextil,

            origem_pedido_old,       motivo_bloqueio_old,
            origem_pedido_new,       motivo_bloqueio_new
            )
         values
           (v_operacao,              v_data_operacao,
            v_usuario_rede,          v_maquina_rede,
            v_aplicativo,            v_usuario_systextil,

            v_origem_pedido_old,     v_motivo_bloqueio_old,
            v_origem_pedido_new,     v_motivo_bloqueio_new
            );

         exception
            when OTHERS
            then raise_application_error (-20000, 'N�o atualizou a tabela de log da pedi_900.');

      end;
   end if;
end inter_tr_pedi_900_log;

-- ALTER TRIGGER "INTER_TR_PEDI_900_LOG" ENABLE
 

/

exec inter_pr_recompile;

