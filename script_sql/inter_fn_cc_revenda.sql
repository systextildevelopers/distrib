CREATE OR REPLACE FUNCTION inter_fn_cc_revenda(p_codigo_empresa NUMBER) RETURN BOOLEAN IS
    v_valor empr_008.val_str%TYPE;
BEGIN

    SELECT val_str INTO v_valor
    FROM empr_008
    WHERE codigo_empresa = p_codigo_empresa
    AND param = 'obrf.controlaCcComprados';

    IF v_valor = 'S' THEN
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN FALSE;
END;
/
