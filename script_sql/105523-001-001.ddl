create table fatu_510
( CODIGO_EMPRESA               NUMBER(3) default 0 not null,
  BUSCA_PRECO_TAB_MOV          NUMBER(1) default 0 not null,
  NATUR_OPER_FUNC_DIGISAT      NUMBER(3) default 0,
  TIPO_TITULO_FUNC_DIGISAT     NUMBER(2) default 0,
  ORIGEM_PRECO_CUSTO           NUMBER(1) default 0,
  CALCULA_PREV_95              NUMBER(1) default 0,
  LIBERA_INV_FISCAL            VARCHAR2(1) default 'N',
  IND_ATUAL_PRC_MEDIO_BASI_010 VARCHAR2(1) default ' ',
  IND_BLOQ_QTDE_PACK_DIF_PROG  VARCHAR2(1) default 'N',
  IND_BLOQ_QTDE_PACK_DIF_ENTR  VARCHAR2(1) default 'N',
  IND_BLOQ_QTDE_PACK_DIF_REST  VARCHAR2(1) default 'N',
  IND_BLOQ_QTDE_PACK_DIF_SEGU  VARCHAR2(1) default 'N');
alter table fatu_510  add constraint PK_FATU_510 primary key (CODIGO_EMPRESA);
exec inter_pr_recompile;
