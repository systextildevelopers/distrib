create or replace PACKAGE BODY ST_PCK_LANCAMENTO_CONTAB as

    FUNCTION last_seq( p_cod_empresa NUMBER, p_exercicio NUMBER, p_numero_lanc NUMBER) RETURN NUMBER AS
        v_seq_lanc NUMBER;   

    BEGIN
        SELECT MAX(seq_lanc) 
        INTO v_seq_lanc
        FROM cont_600
        WHERE cod_empresa = p_cod_empresa
            AND exercicio = p_exercicio
            AND numero_lanc = p_numero_lanc;
        RETURN v_seq_lanc;
        
    END last_seq;

    FUNCTION list( p_cod_empresa NUMBER, p_exercicio NUMBER, p_numero_lanc NUMBER) RETURN v_dados_cont_600 AS
        row_var v_dados_cont_600;
    BEGIN
        SELECT * 
        BULK COLLECT INTO row_var
        FROM cont_600 
        WHERE cod_empresa = p_cod_empresa
            AND exercicio  = p_exercicio
            AND numero_lanc = numero_lanc;
            
        RETURN row_var;
    END list;

    FUNCTION update_proximo_lancamento( p_codigo_empresa NUMBER, p_codigo_exercicio NUMBER) RETURN NUMBER AS
        v_ultimo_lanc NUMBER;
    BEGIN
        UPDATE cont_500 
        SET ultimo_lanc = ultimo_lanc + 1 
        WHERE cod_empresa = p_codigo_empresa 
        AND exercicio = p_codigo_exercicio;

        BEGIN
            SELECT ultimo_lanc INTO v_ultimo_lanc
            FROM cont_500 
            WHERE cod_empresa = p_codigo_empresa AND exercicio = p_codigo_exercicio;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_ultimo_lanc := 0;
        END;

        RETURN v_ultimo_lanc;
    END update_proximo_lancamento;

END ST_PCK_LANCAMENTO_CONTAB;
