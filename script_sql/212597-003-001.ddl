ALTER TABLE SUPR_090 ADD (REUTILIZA_SDCV_OBC varchar2(1));

alter table SUPR_090
modify (REUTILIZA_SDCV_OBC varchar2(1) default 'N');

comment on column SUPR_090.REUTILIZA_SDCV_OBC IS 'Campo informa se reutiliza ou n�o req. ligada ao pedido (S reutiliza, N n�o), para informar na integra��o OBC';
