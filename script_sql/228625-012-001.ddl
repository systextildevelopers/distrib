alter table oper_021 add cod_empresa number(3) default 0 ;

alter table OPER_021
  add constraint PK_OPER_021 primary key (COD_EMPRESA, CODIGO_CONTABIL,  CODIGO_TRANSACAO,  CODIGO_HISTORICO_CONT,  
                                          POSICAO_TITULO,  COD_PORTADOR,  CODIGO_HISTORICO_PGTO,  CONTA_CORRENTE);
                                          
exec inter_pr_recompile;
/
