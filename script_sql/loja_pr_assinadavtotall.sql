
  CREATE OR REPLACE PROCEDURE "LOJA_PR_ASSINADAVTOTALL" (pTipo   in varchar2, -- Capa ou Item (C,I)
                                                    pCodFil in varchar2,
                                                    pTipPed in varchar2,
                                                    pNumPed in varchar2,
                                                    pVlrTot in numeric,
                                                    pNumIte in number,
                                                    pMsgErro out varchar2) is
BEGIN
  loja_pr_AssinaDAVTotall_inte(pTipo,
                          pCodFil,
                          pTipPed,
                          pNumPed,
                          pVlrTot,
                          pNumIte,
                          pMsgErro);
END loja_pr_AssinaDAVTotall;

 

/

exec inter_pr_recompile;

