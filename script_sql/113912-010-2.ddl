ALTER TABLE rcnb_030
  MODIFY valor_base number(15,6);
  
ALTER TABLE basi_351
  MODIFY valor_custo number(15,6);  
  
ALTER TABLE basi_350
  MODIFY valor_mp number(15,6);  
  
ALTER TABLE basi_350
 MODIFY valor_mo number(15,6); 
 
ALTER TABLE basi_350
 MODIFY valor_cp number(15,6); 

ALTER TABLE basi_350
 MODIFY valor_cd number(15,6);  

ALTER TABLE basi_350
 MODIFY VALOR_MP_COMP_ESTIMADO number(15,6);  
 
ALTER TABLE basi_351
 MODIFY VALOR_custo_estimado number(15,6);  
 
ALTER TABLE basi_351
 MODIFY valor_custo_previsto number(15,6); 

exec inter_pr_recompile; 
