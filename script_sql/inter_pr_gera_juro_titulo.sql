
  CREATE OR REPLACE PROCEDURE "INTER_PR_GERA_JURO_TITULO" ( p_data_prorrogacao in date,
                                                        p_empresa in number,
                                                        p_numero_titulo in number,
                                                        p_tipo_titulo in number,
                                                        p_parcela_titulo in number,
                                                        p_cgc9 in number,
                                                        p_cgc4 in number,
                                                        p_cgc2 in number,
                                                        p_cgc9_cheque in number,
                                                        p_cgc4_cheque in number,
                                                        p_cgc2_cheque in number,
                                                        p_portador_duplic in number,
                                                        p_conta_duplic in number,
                                                        p_codigo_usuario in number,
                                                        p_nome_usuario in varchar2,
                                                        p_taxa_juros   in number,
                                                        p_valor_titulo in number,
                                                        p_historico_baixa in number,
                                                        msg_erro out varchar2
                                                        ) is

valor_base crec_180.valor_usado%type;
valor_ultimo_cheque crec_180.valor_usado%type;
dias_atraso number(6);
valor_juros crec_180.valor_usado%type;
maior_data_atraso date;
v_seq_pagto fatu_075.seq_pagamento%type;
data_ultimo_cheque date;

begin

valor_base := p_valor_titulo;
valor_ultimo_cheque := 0;
dias_atraso := 0;
valor_juros := 0;
msg_erro := '';
maior_data_atraso := null;
data_ultimo_cheque := null;

for cheques in (select crec_170.data_vencto,   crec_180.valor_usado
                from crec_170,crec_180
                where crec_170.data_vencto    > p_data_prorrogacao
                  and crec_170.empresa        = crec_180.empresa
                  and crec_170.cgc9_cli       = crec_180.cgc9_cli
                  and crec_170.cgc4_cli       = crec_180.cgc4_cli
                  and crec_170.cgc2_cli       = crec_180.cgc2_cli
                  and crec_170.banco          = crec_180.banco
                  and crec_170.agencia        = crec_180.agencia
                  and crec_170.conta          = crec_180.conta
                  and crec_170.num_cheque     = crec_180.num_cheque
                  and crec_170.sequencia      = crec_180.sequencia

                  and crec_180.empresa        = p_empresa
                   and ((    crec_180.cgc9_cli      = p_cgc9
                        and crec_180.cgc4_cli       = p_cgc4
                        and crec_180.cgc2_cli       = p_cgc2) or
                       (    crec_180.cgc9_cli       = p_cgc9_cheque
                        and crec_180.cgc4_cli       = p_cgc4_cheque
                        and crec_180.cgc2_cli       = p_cgc2_cheque))

                  and crec_180.tipo_titulo    = p_tipo_titulo
                  and crec_180.numero_titulo  = p_numero_titulo
                  and crec_180.parcela_titulo = p_parcela_titulo

                  /* Cheques sendo associados no momento */
                  and exists (select 1 from oper_tmp
                              where oper_tmp.nome_relatorio = 'crec_f399_1'
                                and oper_tmp.nr_solicitacao = p_codigo_usuario
                                and oper_tmp.str_61         = p_nome_usuario
                                and oper_tmp.int_02         = crec_170.empresa
                                and oper_tmp.int_03         = crec_170.num_cheque
                                and oper_tmp.int_04         = crec_170.banco
                                and oper_tmp.int_05         = crec_170.conta
                                and oper_tmp.int_06         = crec_170.cgc9_cli
                                and oper_tmp.int_07         = crec_170.cgc4_cli
                                and oper_tmp.int_08         = crec_170.cgc2_cli
                                and oper_tmp.int_09         = crec_170.sequencia
                                and oper_tmp.int_10         = crec_170.agencia)
          order by crec_170.data_vencto)
   loop
       valor_base := valor_base - valor_ultimo_cheque;
       if data_ultimo_cheque is null
       then
          dias_atraso := trunc(cheques.data_vencto) - trunc(p_data_prorrogacao);
       else
          dias_atraso := trunc(cheques.data_vencto) - trunc(data_ultimo_cheque);
       end if;

       valor_juros := valor_juros + (p_taxa_juros/3000) * dias_atraso * valor_base;
       valor_ultimo_cheque := cheques.valor_usado;
       maior_data_atraso := cheques.data_vencto;

       data_ultimo_cheque := cheques.data_vencto;
   end loop;

   /* Encontrou registros */
   if valor_juros > 0
   then

      BEGIN
        update fatu_070
        set data_prorrogacao = maior_data_atraso
        where fatu_070.codigo_empresa   = p_empresa
          and fatu_070.cli_dup_cgc_cli9 = p_cgc9
          and fatu_070.cli_dup_cgc_cli4 = p_cgc4
          and fatu_070.cli_dup_cgc_cli2 = p_cgc2
          and fatu_070.tipo_titulo      = p_tipo_titulo
          and fatu_070.seq_duplicatas   = p_parcela_titulo;
      EXCEPTION
         when others then
            msg_erro := sqlerrm||' (FATU_070).';
      END;

      BEGIN
        select nvl(max(fatu_075.seq_pagamento),0)+1 into v_seq_pagto
        from fatu_075
        where fatu_075.nr_titul_codempr          = p_empresa
          and fatu_075.nr_titul_cli_dup_cgc_cli9 = p_cgc9
          and fatu_075.nr_titul_cli_dup_cgc_cli4 = p_cgc4
          and fatu_075.nr_titul_cli_dup_cgc_cli2 = p_cgc2
          and fatu_075.nr_titul_cod_tit          = p_tipo_titulo
          and fatu_075.nr_titul_num_dup          = p_numero_titulo
          and fatu_075.nr_titul_seq_dup          = p_parcela_titulo;

        insert into fatu_075
             (nr_titul_codempr,                     nr_titul_cli_dup_cgc_cli9,
              nr_titul_cli_dup_cgc_cli4,            nr_titul_cli_dup_cgc_cli2,
              nr_titul_cod_tit,                     nr_titul_num_dup,
              nr_titul_seq_dup,                     seq_pagamento,
              data_pagamento,                       valor_pago,
              historico_pgto,                       portador,
              conta_corrente,                       data_credito)
           values
             (p_empresa,                            p_cgc9,
              p_cgc4,                               p_cgc2,
              p_tipo_titulo,                        p_numero_titulo,
              p_parcela_titulo,                     v_seq_pagto,
              sysdate,                              valor_juros,
              p_historico_baixa,                    p_portador_duplic,
              p_conta_duplic,                       sysdate);
      EXCEPTION
         when others then
            msg_erro := sqlerrm||' (FATU_075).';
      END;

      BEGIN
         delete from oper_tmp
         where oper_tmp.nr_solicitacao = p_codigo_usuario
           and oper_tmp.nome_relatorio ='crec_f399_1'
           and oper_tmp.int_01         = p_codigo_usuario
           and oper_tmp.str_61         = p_nome_usuario;
       EXCEPTION
         when no_data_found then
           null;
        when others then
           msg_erro := sqlerrm||' (OPER_TMP).';
       END;

   end if;
end;
 

/

exec inter_pr_recompile;

