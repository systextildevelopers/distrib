CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_015_HIST" 
   after insert or delete or
   update of desconto_item, flag_saida, cd_cnpj_forne_9, cd_cnpj_forne_4,
		cd_cnpj_forne_2, cd_seq_nota_frete, observacao, qtd_pcs_caixa_rfid,
		valor_desp_com_icms_sped, valor_imposto_opera_finan_sped, valor_imposto_import_sped, cvf_ipi_entrada,
		base_subtituicao, perc_subtituicao, valor_subtituicao, cod_csosn,
		valor_frete, valor_desc, valor_outros, valor_seguro,
		numero_adicao, cod_base_cred, descricao_item, codigo_deposito,
		cod_vlfiscal_icm, percentual_icm, cvf_icm_diferenc, seq_nota_orig,
		dif_aliquota, flag_devolucao, obs_livro1, endereco,
		valor_icms_diferido, transacao_canc_nfisc, base_pis_cofins, formulario_num,
		cnpj_emis_edi2, perc_icms_subs, capa_ent_nrdoc, capa_ent_serie,
		capa_ent_forcli9, capa_ent_forcli4, capa_ent_forcli2, sequencia,
		coditem_nivel99, coditem_grupo, coditem_subgrupo, coditem_item,
		quantidade, lote_entrega, valor_unitario, unidade_medida,
		valor_ipi, valor_total, classific_fiscal, classif_contabil,
		cod_vlfiscal_ipi, pedido_compra, sequencia_pedido, percentual_ipi,
		centro_custo, preco_custo, natitem_nat_oper, natitem_est_oper,
		valor_icms, codigo_transacao, base_calc_icm, base_diferenca,
		procedencia, rateio_despesas, num_nota_orig, motivo_devolucao,
		serie_nota_orig, situacao_industr, codigo_contabil, empr_nf_saida,
		num_nf_saida, serie_nf_saida, obs_livro2, perc_dif_aliq,
		flag_orcamento, gramatura, perc_icms_diferido, usuario_cardex,
		nome_programa, data_canc_nfisc, ordem_producao, rateio_despesas_ipi,
		rateio_descontos_ipi, base_ipi, valor_pis, valor_cofins,
		projeto, subprojeto, servico, perc_iva_1,
		perc_iva_2, valor_iva_1, valor_iva_2, formulario_ser,
		formulario_seq, formulario_ori, flag_guia_remissao, tipo_nota_edi,
		cnpj_emis_edi9, cnpj_emis_edi4, executa_trigger, cvf_pis,
		cvf_cofins, perc_pis, perc_cofins, base_icms_subs,
		valor_icms_subs, tipo_doc_importacao_sped, valor_pis_import_sped, valor_cofins_import_sped,
		data_desembaraco_sped, valor_cif_sped, valor_desp_sem_icms_sped
   on obrf_015
   for each row

-- Finalidade: Registrar alteraï¿¿ï¿¿es feitas na tabela obrf_015 - itens das notas fiscais de entrada
-- Autor.....: Aline Blanski
-- Data......: 09/06/10
--
-- Histï¿¿ricos de alteraï¿¿ï¿¿es na trigger
--
-- Data      Autor           SS          Observaï¿¿ï¿¿es
-- 12/07/10  Aline Blanski   58329/001   Adicionado campos na verificaï¿¿ï¿¿o da trigger.
-- 29/07/10  Aline Blanski   58331/001   Adicionado campos na verificaï¿¿ï¿¿o da trigger.

declare
   ws_tipo_ocorr                     varchar2 (1);
   ws_usuario_rede                   varchar2(20);
   ws_maquina_rede                   varchar2(40);
   ws_aplicativo                     varchar2(20);
   ws_sid                            number(9);
   ws_empresa                        number(3);
   ws_usuario_systextil              varchar2(250);
   ws_locale_usuario                 varchar2(5);
   ws_nome_programa                  varchar2(20);
   ws_sequencia_old                  NUMBER(9)    default 0 ;
   ws_sequencia_new                  NUMBER(9)    default 0 ;
   ws_pedido_compra_old              NUMBER(9)    default 0 ;
   ws_pedido_compra_new              NUMBER(9)    default 0 ;
   ws_sequencia_pedido_old           NUMBER(9)    default 0 ;
   ws_sequencia_pedido_new           NUMBER(9)    default 0 ;
   ws_num_nota_orig_old              NUMBER(9)    default 0 ;
   ws_num_nota_orig_new              NUMBER(9)    default 0 ;
   ws_serie_nota_orig_old            VARCHAR2(3)  default '';
   ws_serie_nota_orig_new            VARCHAR2(3)  default '';
   ws_seq_nota_orig_old              NUMBER(9)    default 0 ;
   ws_seq_nota_orig_new              NUMBER(9)    default 0 ;
   ws_motivo_devolucao_old           NUMBER(3)    default 0 ;
   ws_motivo_devolucao_new           NUMBER(3)    default 0 ;
   ws_coditem_nivel99_old            VARCHAR2(1)  default '';
   ws_coditem_nivel99_new            VARCHAR2(1)  default '';
   ws_coditem_grupo_old              VARCHAR2(5)  default '';
   ws_coditem_grupo_new              VARCHAR2(5)  default '';
   ws_coditem_subgrupo_old           VARCHAR2(3)  default '';
   ws_coditem_subgrupo_new           VARCHAR2(3)  default '';
   ws_coditem_item_old               VARCHAR2(6)  default '';
   ws_coditem_item_new               VARCHAR2(6)  default '';
   ws_descricao_item_old             VARCHAR2(120) default '';
   ws_descricao_item_new             VARCHAR2(120) default '';
   ws_unidade_medida_old             VARCHAR2(2)  default '';
   ws_unidade_medida_new             VARCHAR2(2)  default '';
   ws_natitem_nat_oper_old           NUMBER(9)    default 0 ;
   ws_natitem_nat_oper_new           NUMBER(9)    default 0 ;
   ws_codigo_transacao_old           NUMBER(9)    default 0 ;
   ws_codigo_transacao_new           NUMBER(9)    default 0 ;
   ws_classif_contabil_old           NUMBER(9)    default 0 ;
   ws_classif_contabil_new           NUMBER(9)    default 0 ;
   ws_classific_fiscal_old           VARCHAR2(15) default '';
   ws_classific_fiscal_new           VARCHAR2(15) default '';
   ws_lote_entrega_old               NUMBER(9)    default 0 ;
   ws_lote_entrega_new               NUMBER(9)    default 0 ;
   ws_codigo_deposito_old            NUMBER(9)    default 0 ;
   ws_codigo_deposito_new            NUMBER(9)    default 0 ;
   ws_centro_custo_old               NUMBER(9)    default 0 ;
   ws_centro_custo_new               NUMBER(9)    default 0 ;
   ws_quantidade_old                 NUMBER(14,3) default 0.0;
   ws_quantidade_new                 NUMBER(14,3) default 0.0;   
   ws_valor_unitario_old             NUMBER(20,5) default 0.0;
   ws_valor_unitario_new             NUMBER(20,5) default 0.0;   
   ws_valor_total_old                NUMBER(15,2) default 0.0;
   ws_valor_total_new                NUMBER(15,2) default 0.0;
   ws_rateio_despesas_old            NUMBER(15,2) default 0.0;
   ws_rateio_despesas_new            NUMBER(15,2) default 0.0;
   ws_percentual_ipi_old             NUMBER(9,2)  default 0.0;
   ws_percentual_ipi_new             NUMBER(9,2)  default 0.0;
   ws_cvf_ipi_entrada_old            NUMBER(2)    default 99 ;
   ws_cvf_ipi_entrada_new            NUMBER(2)    default 99 ;
   ws_base_ipi_old                   NUMBER(15,2) default 0.0;
   ws_base_ipi_new                   NUMBER(15,2) default 0.0;
   ws_valor_ipi_old                  NUMBER(15,2) default 0.0;
   ws_valor_ipi_new                  NUMBER(15,2) default 0.0;
   ws_percentual_icm_old             NUMBER(9,2)  default 0.0;
   ws_percentual_icm_new             NUMBER(9,2)  default 0.0;
   ws_procedencia_old                NUMBER(9)    default 0  ;
   ws_procedencia_new                NUMBER(9)    default 0  ;
   ws_cod_vlfiscal_icm_old           NUMBER(9)    default 0  ;
   ws_cod_vlfiscal_icm_new           NUMBER(9)    default 0  ;
   ws_base_calc_icm_old              NUMBER(15,2) default 0.0;
   ws_base_calc_icm_new              NUMBER(15,2) default 0.0;
   ws_valor_icms_old                 NUMBER(15,2) default 0.0;
   ws_valor_icms_new                 NUMBER(15,2) default 0.0;
   ws_cvf_icm_diferenc_old           NUMBER(9)    default 0  ;
   ws_cvf_icm_diferenc_new           NUMBER(9)    default 0  ;
   ws_base_diferenca_old             NUMBER(15,2) default 0.0;
   ws_base_diferenca_new             NUMBER(15,2) default 0.0;
   ws_cvf_pis_old                    NUMBER(2)    default 0  ;
   ws_cvf_pis_new                    NUMBER(2)    default 0  ;
   ws_cvf_cofins_old                 NUMBER(2)    default 0  ;
   ws_cvf_cofins_new                 NUMBER(2)    default 0  ;
   ws_base_pis_cofins_old            NUMBER(15,2) default 0.00;
   ws_base_pis_cofins_new            NUMBER(15,2) default 0.00;
   ws_valor_pis_old                  NUMBER(15,2) default 0.00;
   ws_valor_pis_new                  NUMBER(15,2) default 0.00;
   ws_valor_cofins_old               NUMBER(15,2) default 0.00;
   ws_valor_cofins_new               NUMBER(15,2) default 0.00;
   ws_codigo_contabil_old            NUMBER(9)    default 0   ;
   ws_codigo_contabil_new            NUMBER(9)    default 0   ;
   ws_perc_dif_aliq_old              NUMBER(16,6) default 0.00;
   ws_perc_dif_aliq_new              NUMBER(16,6) default 0.00;
   ws_dif_aliquota_old               NUMBER(15,2) default 0.00;
   ws_dif_aliquota_new               NUMBER(15,2) default 0.00;
   ws_projeto_old                    NUMBER(4)    default 0   ;
   ws_projeto_new                    NUMBER(4)    default 0   ;
   ws_subprojeto_old                 NUMBER(5)    default 0   ;
   ws_subprojeto_new                 NUMBER(5)    default 0   ;
   ws_servico_old                    NUMBER(4)    default 0   ;
   ws_servico_new                    NUMBER(4)    default 0   ;   
   ws_preco_custo_old                NUMBER(20,4) default  0.0 ;
   ws_preco_custo_new                NUMBER(20,4) default  0.0 ;   
   ws_capa_ent_nrdoc_new             number(9) default 0 ;
   ws_capa_ent_nrdoc_old             number(9) default 0 ;
   ws_capa_ent_serie_new             varchar2(3) default '' ;
   ws_capa_ent_serie_old             varchar2(3) default '' ;
   ws_capa_ent_forcli9_new           number(9)   default 0  ;
   ws_capa_ent_forcli9_old           number(9)   default 0  ;
   ws_capa_ent_forcli4_new           number(9)   default 0  ;
   ws_capa_ent_forcli4_old           number(9)   default 0  ;
   ws_capa_ent_forcli2_new           number(9)   default 0  ;
   ws_capa_ent_forcli2_old           number(9)   default 0  ;
   ws_natitem_est_oper_old           VARCHAR2(2)  default '';
   ws_natitem_est_oper_new           VARCHAR2(2)  default '';

begin

   if INSERTING then
      ws_tipo_ocorr                 := 'I';
      ws_sequencia_old              := 0;
      ws_sequencia_new              := :new.sequencia;
      ws_pedido_compra_old          := 0;
      ws_pedido_compra_new          := :new.pedido_compra;
      ws_sequencia_pedido_old       := 0;
      ws_sequencia_pedido_new       := :new.sequencia_pedido;
      ws_num_nota_orig_old          := 0;
      ws_num_nota_orig_new          := :new.num_nota_orig;
      ws_serie_nota_orig_old        := null;
      ws_serie_nota_orig_new        := :new.serie_nota_orig;
      ws_seq_nota_orig_old          := 0;
      ws_seq_nota_orig_new          := :new.seq_nota_orig;
      ws_motivo_devolucao_old       := 0;
      ws_motivo_devolucao_new       := :new.motivo_devolucao;
      ws_coditem_nivel99_old        := null;
      ws_coditem_nivel99_new        := :new.coditem_nivel99;
      ws_coditem_grupo_old          := null;
      ws_coditem_grupo_new          := :new.coditem_grupo;
      ws_coditem_subgrupo_old       := null;
      ws_coditem_subgrupo_new       := :new.coditem_subgrupo;
      ws_coditem_item_old           := null;
      ws_coditem_item_new           := :new.coditem_item;
      ws_descricao_item_old         := null;
      ws_descricao_item_new         := :new.descricao_item;
      ws_unidade_medida_old         := null;
      ws_unidade_medida_new         := :new.unidade_medida;
      ws_natitem_nat_oper_old       := 0;
      ws_natitem_nat_oper_new       := :new.natitem_nat_oper;
      ws_codigo_transacao_old       := 0;
      ws_codigo_transacao_new       := :new.codigo_transacao;
      ws_classif_contabil_old       := 0;
      ws_classif_contabil_new       := :new.classif_contabil;
      ws_classific_fiscal_old       := null;
      ws_classific_fiscal_new       := :new.classific_fiscal;
      ws_lote_entrega_old           := 0;
      ws_lote_entrega_new           := :new.lote_entrega;
      ws_codigo_deposito_old        := 0;
      ws_codigo_deposito_new        := :new.codigo_deposito;
      ws_centro_custo_old           := 0;
      ws_centro_custo_new           := :new.centro_custo;
      ws_quantidade_old             := 0;
      ws_quantidade_new             := :new.quantidade;
      ws_valor_unitario_old         := 0;
      ws_valor_unitario_new         := :new.valor_unitario;
      ws_valor_total_old            := 0;
      ws_valor_total_new            := :new.valor_total;
      ws_rateio_despesas_old        := 0;
      ws_rateio_despesas_new        := :new.rateio_despesas;
      ws_percentual_ipi_old         := 0;
      ws_percentual_ipi_new         := :new.percentual_ipi;
      ws_cvf_ipi_entrada_old        := 0;
      ws_cvf_ipi_entrada_new        := :new.cvf_ipi_entrada;
      ws_base_ipi_old               := 0;
      ws_base_ipi_new               := :new.base_ipi;
      ws_valor_ipi_old              := 0;
      ws_valor_ipi_new              := :new.valor_ipi;
      ws_percentual_icm_old         := 0;
      ws_percentual_icm_new         := :new.percentual_icm;
      ws_procedencia_old            := 0;
      ws_procedencia_new            := :new.procedencia;
      ws_cod_vlfiscal_icm_old       := 0;
      ws_cod_vlfiscal_icm_new       := :new.cod_vlfiscal_icm;
      ws_base_calc_icm_old          := 0;
      ws_base_calc_icm_new          := :new.base_calc_icm;
      ws_valor_icms_old             := 0;
      ws_valor_icms_new             := :new.valor_icms;
      ws_cvf_icm_diferenc_old       := 0;
      ws_cvf_icm_diferenc_new       := :new.cvf_icm_diferenc;
      ws_base_diferenca_old         := 0;
      ws_base_diferenca_new         := :new.base_diferenca;
      ws_cvf_pis_old                := 0;
      ws_cvf_pis_new                := :new.cvf_pis;
      ws_cvf_cofins_old             := 0;
      ws_cvf_cofins_new             := :new.cvf_cofins;
      ws_base_pis_cofins_old        := 0;
      ws_base_pis_cofins_new        := :new.base_pis_cofins;
      ws_valor_pis_old              := 0;
      ws_valor_pis_new              := :new.valor_pis;
      ws_valor_cofins_old           := 0;
      ws_valor_cofins_new           := :new.valor_cofins;
      ws_codigo_contabil_old        := 0;
      ws_codigo_contabil_new        := :new.codigo_contabil;
      ws_perc_dif_aliq_old          := 0;
      ws_perc_dif_aliq_new          := :new.perc_dif_aliq;
      ws_dif_aliquota_old           := 0;
      ws_dif_aliquota_new           := :new.dif_aliquota;
      ws_projeto_old                := 0;
      ws_projeto_new                := :new.projeto;
      ws_subprojeto_old             := 0;
      ws_subprojeto_new             := :new.subprojeto;
      ws_servico_old                := 0;
      ws_servico_new                := :new.servico ;
      ws_preco_custo_old            := 0;
      ws_preco_custo_new            := :new.preco_custo;
      ws_capa_ent_nrdoc_new         := :new.capa_ent_nrdoc;
      ws_capa_ent_nrdoc_old         := 0;
      ws_capa_ent_serie_new         := :new.capa_ent_serie;
      ws_capa_ent_serie_old         := null;
      ws_capa_ent_forcli9_new       := :new.capa_ent_forcli9;
      ws_capa_ent_forcli9_old       := 0;
      ws_capa_ent_forcli4_new       := :new.capa_ent_forcli4;
      ws_capa_ent_forcli4_old       := 0;
      ws_capa_ent_forcli2_new       := :new.capa_ent_forcli2;
      ws_capa_ent_forcli2_old       := 0;
      ws_natitem_est_oper_new       := :new.natitem_est_oper;
      ws_natitem_est_oper_old       := null;

   elsif UPDATING then

      ws_tipo_ocorr                 := 'A';
      ws_sequencia_old              := :old.sequencia;
      ws_sequencia_new              := :new.sequencia;
      ws_pedido_compra_old          := :old.pedido_compra;
      ws_pedido_compra_new          := :new.pedido_compra;
      ws_sequencia_pedido_old       := :old.sequencia_pedido;
      ws_sequencia_pedido_new       := :new.sequencia_pedido;
      ws_num_nota_orig_old          := :old.num_nota_orig;
      ws_num_nota_orig_new          := :new.num_nota_orig;
      ws_serie_nota_orig_old        := :old.serie_nota_orig;
      ws_serie_nota_orig_new        := :new.serie_nota_orig;
      ws_seq_nota_orig_old          := :old.seq_nota_orig;
      ws_seq_nota_orig_new          := :new.seq_nota_orig;
      ws_motivo_devolucao_old       := :old.motivo_devolucao;
      ws_motivo_devolucao_new       := :new.motivo_devolucao;
      ws_coditem_nivel99_old        := :old.coditem_nivel99;
      ws_coditem_nivel99_new        := :new.coditem_nivel99;
      ws_coditem_grupo_old          := :old.coditem_grupo;
      ws_coditem_grupo_new          := :new.coditem_grupo;
      ws_coditem_subgrupo_old       := :old.coditem_subgrupo;
      ws_coditem_subgrupo_new       := :new.coditem_subgrupo;
      ws_coditem_item_old           := :old.coditem_item;
      ws_coditem_item_new           := :new.coditem_item;
      ws_descricao_item_old         := :old.descricao_item;
      ws_descricao_item_new         := :new.descricao_item;
      ws_unidade_medida_old         := :old.unidade_medida;
      ws_unidade_medida_new         := :new.unidade_medida;
      ws_natitem_nat_oper_old       := :old.natitem_nat_oper;
      ws_natitem_nat_oper_new       := :new.natitem_nat_oper;
      ws_codigo_transacao_old       := :old.codigo_transacao;
      ws_codigo_transacao_new       := :new.codigo_transacao;
      ws_classif_contabil_old       := :old.classif_contabil;
      ws_classif_contabil_new       := :new.classif_contabil;
      ws_classific_fiscal_old       := :old.classific_fiscal;
      ws_classific_fiscal_new       := :new.classific_fiscal;
      ws_lote_entrega_old           := :old.lote_entrega;
      ws_lote_entrega_new           := :new.lote_entrega;
      ws_codigo_deposito_old        := :old.codigo_deposito;
      ws_codigo_deposito_new        := :new.codigo_deposito;
      ws_centro_custo_old           := :old.centro_custo;
      ws_centro_custo_new           := :new.centro_custo;
      ws_quantidade_old             := :old.quantidade;
      ws_quantidade_new             := :new.quantidade;
      ws_valor_unitario_old         := :old.valor_unitario;
      ws_valor_unitario_new         := :new.valor_unitario;
      ws_valor_total_old            := :old.valor_total;
      ws_valor_total_new            := :new.valor_total;
      ws_rateio_despesas_old        := :old.rateio_despesas;
      ws_rateio_despesas_new        := :new.rateio_despesas;
      ws_percentual_ipi_old         := :old.percentual_ipi;
      ws_percentual_ipi_new         := :new.percentual_ipi;
      ws_cvf_ipi_entrada_old        := :old.cvf_ipi_entrada;
      ws_cvf_ipi_entrada_new        := :new.cvf_ipi_entrada;
      ws_base_ipi_old               := :old.base_ipi;
      ws_base_ipi_new               := :new.base_ipi;
      ws_valor_ipi_old              := :old.valor_ipi;
      ws_valor_ipi_new              := :new.valor_ipi;
      ws_percentual_icm_old         := :old.percentual_icm;
      ws_percentual_icm_new         := :new.percentual_icm;
      ws_procedencia_old            := :old.procedencia;
      ws_procedencia_new            := :new.procedencia;
      ws_cod_vlfiscal_icm_old       := :old.cod_vlfiscal_icm;
      ws_cod_vlfiscal_icm_new       := :new.cod_vlfiscal_icm;
      ws_base_calc_icm_old          := :old.base_calc_icm;
      ws_base_calc_icm_new          := :new.base_calc_icm;
      ws_valor_icms_old             := :old.valor_icms;
      ws_valor_icms_new             := :new.valor_icms;
      ws_cvf_icm_diferenc_old       := :old.cvf_icm_diferenc;
      ws_cvf_icm_diferenc_new       := :new.cvf_icm_diferenc;
      ws_base_diferenca_old         := :old.base_diferenca;
      ws_base_diferenca_new         := :new.base_diferenca;
      ws_cvf_pis_old                := :old.cvf_pis;
      ws_cvf_pis_new                := :new.cvf_pis;
      ws_cvf_cofins_old             := :old.cvf_cofins;
      ws_cvf_cofins_new             := :new.cvf_cofins;
      ws_base_pis_cofins_old        := :old.base_pis_cofins;
      ws_base_pis_cofins_new        := :new.base_pis_cofins;
      ws_valor_pis_old              := :old.valor_pis;
      ws_valor_pis_new              := :new.valor_pis;
      ws_valor_cofins_old           := :old.valor_cofins;
      ws_valor_cofins_new           := :new.valor_cofins;
      ws_codigo_contabil_old        := :old.codigo_contabil;
      ws_codigo_contabil_new        := :new.codigo_contabil;
      ws_perc_dif_aliq_old          := :old.perc_dif_aliq;
      ws_perc_dif_aliq_new          := :new.perc_dif_aliq;
      ws_dif_aliquota_old           := :old.dif_aliquota;
      ws_dif_aliquota_new           := :new.dif_aliquota;
      ws_projeto_old                := :old.projeto;
      ws_projeto_new                := :new.projeto;
      ws_subprojeto_old             := :old.subprojeto;
      ws_subprojeto_new             := :new.subprojeto;
      ws_servico_old                := :old.servico;
      ws_servico_new                := :new.servico;
      ws_preco_custo_old            := :old.preco_custo;
      ws_preco_custo_new            := :new.preco_custo;
      ws_capa_ent_nrdoc_new         := :new.capa_ent_nrdoc;
      ws_capa_ent_nrdoc_old         := :old.capa_ent_nrdoc;
      ws_capa_ent_serie_new         := :new.capa_ent_serie;
      ws_capa_ent_serie_old         := :old.capa_ent_serie;
      ws_capa_ent_forcli9_new       := :new.capa_ent_forcli9;
      ws_capa_ent_forcli9_old       := :old.capa_ent_forcli9;
      ws_capa_ent_forcli4_new       := :new.capa_ent_forcli4;
      ws_capa_ent_forcli4_old       := :old.capa_ent_forcli4;
      ws_capa_ent_forcli2_new       := :new.capa_ent_forcli2;
      ws_capa_ent_forcli2_old       := :old.capa_ent_forcli2;
      ws_natitem_est_oper_new       := :new.natitem_est_oper;
      ws_natitem_est_oper_old       := :old.natitem_est_oper;

   elsif DELETING then

      ws_tipo_ocorr                 := 'D';
      ws_sequencia_old              := :old.sequencia;
      ws_sequencia_new              := 0;
      ws_pedido_compra_old          := :old.pedido_compra;
      ws_pedido_compra_new          := 0;
      ws_sequencia_pedido_old       := :old.sequencia_pedido;
      ws_sequencia_pedido_new       := 0;
      ws_num_nota_orig_old          := :old.num_nota_orig;
      ws_num_nota_orig_new          := 0;
      ws_serie_nota_orig_old        := :old.serie_nota_orig;
      ws_serie_nota_orig_new        := null;
      ws_seq_nota_orig_old          := :old.seq_nota_orig;
      ws_seq_nota_orig_new          := 0;
      ws_motivo_devolucao_old       := :old.motivo_devolucao;
      ws_motivo_devolucao_new       := 0;
      ws_coditem_nivel99_old        := :old.coditem_nivel99;
      ws_coditem_nivel99_new        := null;
      ws_coditem_grupo_old          := :old.coditem_grupo;
      ws_coditem_grupo_new          := null;
      ws_coditem_subgrupo_old       := :old.coditem_subgrupo;
      ws_coditem_subgrupo_new       := null;
      ws_coditem_item_old           := :old.coditem_item;
      ws_coditem_item_new           := null;
      ws_descricao_item_old         := :old.descricao_item;
      ws_descricao_item_new         := null;
      ws_unidade_medida_old         := :old.unidade_medida;
      ws_unidade_medida_new         := null;
      ws_natitem_nat_oper_old       := :old.natitem_nat_oper;
      ws_natitem_nat_oper_new       := 0;
      ws_codigo_transacao_old       := :old.codigo_transacao;
      ws_codigo_transacao_new       := 0;
      ws_classif_contabil_old       := :old.classif_contabil;
      ws_classif_contabil_new       := 0;
      ws_classific_fiscal_old       := :old.classific_fiscal;
      ws_classific_fiscal_new       := null;
      ws_lote_entrega_old           := :old.lote_entrega;
      ws_lote_entrega_new           := 0;
      ws_codigo_deposito_old        := :old.codigo_deposito;
      ws_codigo_deposito_new        := 0;
      ws_centro_custo_old           := :old.centro_custo;
      ws_centro_custo_new           := 0;
      ws_quantidade_old             := :old.quantidade;
      ws_quantidade_new             := 0;
      ws_valor_unitario_old         := :old.valor_unitario;
      ws_valor_unitario_new         := 0;
      ws_valor_total_old            := :old.valor_total;
      ws_valor_total_new            := 0;
      ws_rateio_despesas_old        := :old.rateio_despesas;
      ws_rateio_despesas_new        := 0;
      ws_percentual_ipi_old         := :old.percentual_ipi;
      ws_percentual_ipi_new         := 0;
      ws_cvf_ipi_entrada_old        := :old.cvf_ipi_entrada;
      ws_cvf_ipi_entrada_new        := 0;
      ws_base_ipi_old               := :old.base_ipi;
      ws_base_ipi_new               := 0;
      ws_valor_ipi_old              := :old.valor_ipi;
      ws_valor_ipi_new              := 0;
      ws_percentual_icm_old         := :old.percentual_icm;
      ws_percentual_icm_new         := 0;
      ws_procedencia_old            := :old.procedencia;
      ws_procedencia_new            := 0;
      ws_cod_vlfiscal_icm_old       := :old.cod_vlfiscal_icm;
      ws_cod_vlfiscal_icm_new       := 0;
      ws_base_calc_icm_old          := :old.base_calc_icm;
      ws_base_calc_icm_new          := 0;
      ws_valor_icms_old             := :old.valor_icms;
      ws_valor_icms_new             := 0;
      ws_cvf_icm_diferenc_old       := :old.cvf_icm_diferenc;
      ws_cvf_icm_diferenc_new       := 0;
      ws_base_diferenca_old         := :old.base_diferenca;
      ws_base_diferenca_new         := 0;
      ws_cvf_pis_old                := :old.cvf_pis;
      ws_cvf_pis_new                := 0;
      ws_cvf_cofins_old             := :old.cvf_cofins;
      ws_cvf_cofins_new             := 0;
      ws_base_pis_cofins_old        := :old.base_pis_cofins;
      ws_base_pis_cofins_new        := 0;
      ws_valor_pis_old              := :old.valor_pis;
      ws_valor_pis_new              := 0;
      ws_valor_cofins_old           := :old.valor_cofins;
      ws_valor_cofins_new           := 0;
      ws_codigo_contabil_old        := :old.codigo_contabil;
      ws_codigo_contabil_new        := 0;
      ws_perc_dif_aliq_old          := :old.perc_dif_aliq;
      ws_perc_dif_aliq_new          := 0;
      ws_dif_aliquota_old           := :old.dif_aliquota;
      ws_dif_aliquota_new           := 0;
      ws_projeto_old                := :old.projeto;
      ws_projeto_new                := 0;
      ws_subprojeto_old             := :old.subprojeto;
      ws_subprojeto_new             := 0;
      ws_servico_old                := :old.servico ;
      ws_servico_new                := 0;
      ws_preco_custo_old            := :old.preco_custo;
      ws_preco_custo_new            := 0;
      ws_capa_ent_nrdoc_new         := 0;
      ws_capa_ent_nrdoc_old         := :old.capa_ent_nrdoc;
      ws_capa_ent_serie_new         := null;
      ws_capa_ent_serie_old         := :old.capa_ent_serie;
      ws_capa_ent_forcli9_new       := 0;
      ws_capa_ent_forcli9_old       := :old.capa_ent_forcli9;
      ws_capa_ent_forcli4_new       := 0;
      ws_capa_ent_forcli4_old       := :old.capa_ent_forcli4;
      ws_capa_ent_forcli2_new       := 0;
      ws_capa_ent_forcli2_old       := :old.capa_ent_forcli2;
      ws_natitem_est_oper_new       := null;
      ws_natitem_est_oper_old       := :old.natitem_est_oper;

   end if;

   -- Dados do usuï¿¿rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


   ws_nome_programa := inter_fn_nome_programa(ws_sid);   
   
   INSERT INTO obrf_015_hist
      ( aplicacao,                     tipo_ocorr,
        data_ocorr,
        usuario_rede,                  maquina_rede,
        nome_programa,                 usuario_systextil,
        sequencia_old,                 sequencia_new,
        pedido_compra_old,             pedido_compra_new,
        sequencia_pedido_old,          sequencia_pedido_new,
        num_nota_orig_old,             num_nota_orig_new,
        serie_nota_orig_old,           serie_nota_orig_new,
        seq_nota_orig_old,             seq_nota_orig_new,
        motivo_devolucao_old,          motivo_devolucao_new,
        coditem_nivel99_old,           coditem_nivel99_new,
        coditem_grupo_old,             coditem_grupo_new,
        coditem_subgrupo_old,          coditem_subgrupo_new,
        coditem_item_old,              coditem_item_new,
        descricao_item_old,            descricao_item_new,
        unidade_medida_old,            unidade_medida_new,
        natitem_nat_oper_old,          natitem_nat_oper_new,
        codigo_transacao_old,          codigo_transacao_new,
        classif_contabil_old,          classif_contabil_new,
        classific_fiscal_old,          classific_fiscal_new,
        lote_entrega_old,              lote_entrega_new,
        codigo_deposito_old,           codigo_deposito_new,
        centro_custo_old,              centro_custo_new,
        quantidade_old,                quantidade_new,
        valor_unitario_old,            valor_unitario_new,
        valor_total_old,               valor_total_new,
        rateio_despesas_old,           rateio_despesas_new,
        percentual_ipi_old,            percentual_ipi_new,
        cvf_ipi_entrada_old,           cvf_ipi_entrada_new,
        base_ipi_old,                  base_ipi_new,
        valor_ipi_old,                 valor_ipi_new,
        percentual_icm_old,            percentual_icm_new,
        procedencia_old,               procedencia_new,
        cod_vlfiscal_icm_old,          cod_vlfiscal_icm_new,
        base_calc_icm_old,             base_calc_icm_new,
        valor_icms_old,                valor_icms_new,
        cvf_icm_diferenc_old,          cvf_icm_diferenc_new,
        base_diferenca_old,            base_diferenca_new,
        cvf_pis_old,                   cvf_pis_new,
        cvf_cofins_old,                cvf_cofins_new,
        base_pis_cofins_old,           base_pis_cofins_new,
        valor_pis_old,                 valor_pis_new,
        valor_cofins_old,              valor_cofins_new,
        codigo_contabil_old,           codigo_contabil_new,
        perc_dif_aliq_old,             perc_dif_aliq_new,
        dif_aliquota_old,              dif_aliquota_new,
        projeto_old,                   projeto_new,
        subprojeto_old,                subprojeto_new,
        servico_old,                   servico_new,
        preco_custo_old,               preco_custo_new,
        capa_ent_nrdoc_new,            capa_ent_nrdoc_old,
        capa_ent_serie_new,            capa_ent_serie_old,
        capa_ent_forcli9_new,          capa_ent_forcli9_old,
        capa_ent_forcli4_new,          capa_ent_forcli4_old,
        capa_ent_forcli2_new,          capa_ent_forcli2_old,
        natitem_est_oper_new,          natitem_est_oper_old
      )
   VALUES
      ( ws_aplicativo,                 ws_tipo_ocorr,
        sysdate,
        ws_usuario_rede,               ws_maquina_rede,
        ws_nome_programa,              ws_usuario_systextil,
        ws_sequencia_old,              ws_sequencia_new,
        ws_pedido_compra_old,          ws_pedido_compra_new,
        ws_sequencia_pedido_old,       ws_sequencia_pedido_new,
        ws_num_nota_orig_old,          ws_num_nota_orig_new,
        ws_serie_nota_orig_old,        ws_serie_nota_orig_new,
        ws_seq_nota_orig_old,          ws_seq_nota_orig_new,
        ws_motivo_devolucao_old,       ws_motivo_devolucao_new,
        ws_coditem_nivel99_old,        ws_coditem_nivel99_new,
        ws_coditem_grupo_old,          ws_coditem_grupo_new,
        ws_coditem_subgrupo_old,       ws_coditem_subgrupo_new,
        ws_coditem_item_old,           ws_coditem_item_new,
        ws_descricao_item_old,         ws_descricao_item_new,
        ws_unidade_medida_old,         ws_unidade_medida_new,
        ws_natitem_nat_oper_old,       ws_natitem_nat_oper_new,
        ws_codigo_transacao_old,       ws_codigo_transacao_new,
        ws_classif_contabil_old,       ws_classif_contabil_new,
        ws_classific_fiscal_old,       ws_classific_fiscal_new,
        ws_lote_entrega_old,           ws_lote_entrega_new,
        ws_codigo_deposito_old,        ws_codigo_deposito_new,
        ws_centro_custo_old,           ws_centro_custo_new,
        ws_quantidade_old,             ws_quantidade_new,
        ws_valor_unitario_old,         ws_valor_unitario_new,
        ws_valor_total_old,            ws_valor_total_new,
        ws_rateio_despesas_old,        ws_rateio_despesas_new,
        ws_percentual_ipi_old,         ws_percentual_ipi_new,
        ws_cvf_ipi_entrada_old,        ws_cvf_ipi_entrada_new,
        ws_base_ipi_old,               ws_base_ipi_new,
        ws_valor_ipi_old,              ws_valor_ipi_new,
        ws_percentual_icm_old,         ws_percentual_icm_new,
        ws_procedencia_old,            ws_procedencia_new,
        ws_cod_vlfiscal_icm_old,       ws_cod_vlfiscal_icm_new,
        ws_base_calc_icm_old,          ws_base_calc_icm_new,
        ws_valor_icms_old,             ws_valor_icms_new,
        ws_cvf_icm_diferenc_old,       ws_cvf_icm_diferenc_new,
        ws_base_diferenca_old,         ws_base_diferenca_new,
        ws_cvf_pis_old,                ws_cvf_pis_new,
        ws_cvf_cofins_old,             ws_cvf_cofins_new,
        ws_base_pis_cofins_old,        ws_base_pis_cofins_new,
        ws_valor_pis_old,              ws_valor_pis_new,
        ws_valor_cofins_old,           ws_valor_cofins_new,
        ws_codigo_contabil_old,        ws_codigo_contabil_new,
        ws_perc_dif_aliq_old,          ws_perc_dif_aliq_new,
        ws_dif_aliquota_old,           ws_dif_aliquota_new,
        ws_projeto_old,                ws_projeto_new,
        ws_subprojeto_old,             ws_subprojeto_new,
        ws_servico_old,                ws_servico_new,
        ws_preco_custo_old,            ws_preco_custo_new,
        ws_capa_ent_nrdoc_new,         ws_capa_ent_nrdoc_old,
        ws_capa_ent_serie_new,         ws_capa_ent_serie_old,
        ws_capa_ent_forcli9_new,       ws_capa_ent_forcli9_old,
        ws_capa_ent_forcli4_new,       ws_capa_ent_forcli4_old,
        ws_capa_ent_forcli2_new,       ws_capa_ent_forcli2_old,
        ws_natitem_est_oper_new,       ws_natitem_est_oper_old
      );

end inter_tr_obrf_015_hist;

-- ALTER TRIGGER "INTER_TR_OBRF_015_HIST" ENABLE
 


/
