-- Create table
create table PCPT_223
(
  codigo_ocorrencia NUMBER(4) default 0 not null,
  descricao         VARCHAR2(60) default ''
);
comment on table PCPT_223 is 'Cadastro de Ocorrencias para Tecelagem';
comment on column PCPT_223.codigo_ocorrencia  is 'Codigo de ocorrencia.';
comment on column PCPT_223.descricao  is 'Descricao da ocorrencia.';
alter table PCPT_223 add constraint PK_PCPT_223 primary key (CODIGO_OCORRENCIA)
;
