create or replace trigger "INTER_TR_LOJA_810_LOG"
after insert or delete or update
on loja_810
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);

 if inserting
 then
    begin

        insert into loja_810_log (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           empresa_old,   /*8*/
           empresa_new,   /*9*/
           codigo_caixa_old,   /*10*/
           codigo_caixa_new,   /*11*/
           data_caixa_old,   /*12*/
           data_caixa_new,   /*13*/
           saldo_inicial_old,   /*14*/
           saldo_inicial_new,   /*15*/
           saldo_final_old,   /*16*/
           saldo_final_new,   /*17*/
           situacao_old,   /*18*/
           situacao_new,   /*19*/
           observacao_old,   /*20*/
           observacao_new   /*21*/
        ) values (
            'i', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           null,/*8*/
           :new.empresa, /*9*/
           null,/*10*/
           :new.codigo_caixa, /*11*/
           null,/*12*/
           :new.data_caixa, /*13*/
           null,/*14*/
           :new.saldo_inicial, /*15*/
           null,/*16*/
           :new.saldo_final, /*17*/
           null,/*18*/
           :new.situacao, /*19*/
           null,/*20*/
           :new.observacao /*21*/
         );
    end;
 end if;


 if updating
 then
    begin
           insert into loja_810_log (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           empresa_old,   /*8*/
           empresa_new,   /*9*/
           codigo_caixa_old,   /*10*/
           codigo_caixa_new,   /*11*/
           data_caixa_old,   /*12*/
           data_caixa_new,   /*13*/
           saldo_inicial_old,   /*14*/
           saldo_inicial_new,   /*15*/
           saldo_final_old,   /*16*/
           saldo_final_new,   /*17*/
           situacao_old,   /*18*/
           situacao_new,   /*19*/
           observacao_old,   /*20*/
           observacao_new   /*21*/
        ) values (
            'u', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.empresa,/*8*/
           :new.empresa, /*9*/
           :old.codigo_caixa,/*10*/
           :new.codigo_caixa, /*11*/
           :old.data_caixa,/*12*/
           :new.data_caixa, /*13*/
           :old.saldo_inicial,/*14*/
           :new.saldo_inicial, /*15*/
           :old.saldo_final,/*16*/
           :new.saldo_final, /*17*/
           :old.situacao,/*18*/
           :new.situacao, /*19*/
           :old.observacao,/*20*/
           :new.observacao /*21*/
         );
    end;
 end if;


 if deleting
 then
    begin
           insert into loja_810_log (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           empresa_old,   /*8*/
           empresa_new,   /*9*/
           codigo_caixa_old,   /*10*/
           codigo_caixa_new,   /*11*/
           data_caixa_old,   /*12*/
           data_caixa_new,   /*13*/
           saldo_inicial_old,   /*14*/
           saldo_inicial_new,   /*15*/
           saldo_final_old,   /*16*/
           saldo_final_new,   /*17*/
           situacao_old,   /*18*/
           situacao_new,   /*19*/
           observacao_old,   /*20*/
           observacao_new   /*21*/
        ) values (
            'd', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.empresa,/*8*/
           null, /*9*/
           :old.codigo_caixa,/*10*/
           null, /*11*/
           :old.data_caixa,/*12*/
           null, /*13*/
           :old.saldo_inicial,/*14*/
           null, /*15*/
           :old.saldo_final,/*16*/
           null, /*17*/
           :old.situacao,/*18*/
           null, /*19*/
           :old.observacao,/*20*/
           null /*21*/
         );
    end;
 end if;
end inter_tr_loja_810_log;

-- ALTER TRIGGER "INTER_TR_LOJA_810_LOG" ENABLE


/

exec inter_pr_recompile;
