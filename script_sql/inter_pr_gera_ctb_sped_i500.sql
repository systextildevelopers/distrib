create or replace procedure inter_pr_gera_ctb_sped_i500(p_cod_empresa   IN NUMBER,
                                                        p_exercicio     IN NUMBER,
                                                        p_layout        IN VARCHAR2,
                                                        p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar as tabelas sped_cta_i500 - Definicao do layout do Razao/diario de clientes e fornecedores
   --
   -- Historicos
   --
   -- Data    Autor    Observacoes
   --

   v_erro           EXCEPTION;

   v_desc_campo     sped_ctb_i500.desc_campo%type;

begin
   p_des_erro       := NULL;

   -- primeiro campo: CNPJ
   begin

      if p_layout = 'C'
      then
         v_desc_campo := 'CNPJ CLIENTE';
      else
         v_desc_campo := 'CNPJ FORNECEDOR';
      end if;

      insert into sped_ctb_i500
        (cod_empresa,          exercicio,
         tipo_livro_aux,       tam_fonte,
         nom_campo,            desc_campo,
         tipo_campo,           tam_campo,
         dec_campo,            larg_col_campo,
         seq_campo)
      values
        (p_cod_empresa,        p_exercicio,
         p_layout,             8,
         'CNPJ',               v_desc_campo,
         'C',                  17,
         0,                    17,
         0);

   exception
      when others then
         p_des_erro := 'Erro na inclusao da tabela sped_i500 (1) ' || Chr(10) || SQLERRM;
         RAISE V_ERRO;
   end;

   -- segundo campo: Nome do Cliente ou Fornecedor
   begin
      if p_layout = 'C'
      then
         v_desc_campo := 'NOME CLIENTE';
      else
         v_desc_campo := 'NOME FORNECEDOR';
      end if;

      insert into sped_ctb_i500
        (cod_empresa,          exercicio,
         tipo_livro_aux,       tam_fonte,
         nom_campo,            desc_campo,
         tipo_campo,           tam_campo,
         dec_campo,            larg_col_campo,
         seq_campo)
      values
        (p_cod_empresa,        p_exercicio,
         p_layout,             8,
         'NOME',               v_desc_campo,
         'C',                  100,
         0,                    100,
         1);

   exception
      when others then
         p_des_erro := 'Erro na inclusao da tabela sped_i500 (2) ' || Chr(10) || SQLERRM;
         RAISE V_ERRO;
   end;

   -- terceiro campo: DATA MOVIMENTO
   begin

      insert into sped_ctb_i500
        (cod_empresa,          exercicio,
         tipo_livro_aux,       tam_fonte,
         nom_campo,            desc_campo,
         tipo_campo,           tam_campo,
         dec_campo,            larg_col_campo,
         seq_campo)
      values
        (p_cod_empresa,        p_exercicio,
         p_layout,             8,
         'DATA',               'DATA MOVTO',
         'C',                  8,
         0,                    9,
         2);

   exception
      when others then
         p_des_erro := 'Erro na inclusao da tabela sped_i500 (3) ' || Chr(10) || SQLERRM;
         RAISE V_ERRO;
   end;

   -- quarto campo: DESCRICAO DO HISTORICO
   begin

      insert into sped_ctb_i500
        (cod_empresa,          exercicio,
         tipo_livro_aux,       tam_fonte,
         nom_campo,            desc_campo,
         tipo_campo,           tam_campo,
         dec_campo,            larg_col_campo,
         seq_campo)
      values
        (p_cod_empresa,        p_exercicio,
         p_layout,             8,
         'HISTORICO',          'HISTORICO',
         'C',                  30,
         0,                    31,
         3);

   exception
      when others then
         p_des_erro := 'Erro na inclusao da tabela sped_i500 (4) ' || Chr(10) || SQLERRM;
         RAISE V_ERRO;
   end;

   -- quinto campo: NUMERO DO DOCUMENTO
   begin

      insert into sped_ctb_i500
        (cod_empresa,          exercicio,
         tipo_livro_aux,       tam_fonte,
         nom_campo,            desc_campo,
         tipo_campo,           tam_campo,
         dec_campo,            larg_col_campo,
         seq_campo)
      values
        (p_cod_empresa,        p_exercicio,
         p_layout,             8,
         'NR_DOCTO',          'NR.DOCUMENTO',
         'C',                  13,
         0,                    14,
         4);

   exception
      when others then
         p_des_erro := 'Erro na inclusao da tabela sped_i500 (5) ' || Chr(10) || SQLERRM;
         RAISE V_ERRO;
   end;

   -- sexto campo: VALOR DEBITO
   begin

      insert into sped_ctb_i500
        (cod_empresa,          exercicio,
         tipo_livro_aux,       tam_fonte,
         nom_campo,            desc_campo,
         tipo_campo,           tam_campo,
         dec_campo,            larg_col_campo,
         seq_campo)
      values
        (p_cod_empresa,        p_exercicio,
         p_layout,             8,
         'V_DEBITO',           'VALOR DEBITO',
         'N',                  14,
         2,                    15,
         5);

   exception
      when others then
         p_des_erro := 'Erro na inclusao da tabela sped_i500 (6) ' || Chr(10) || SQLERRM;
         RAISE V_ERRO;
   end;

   -- setimo campo: VALOR CREDITO
   begin

      insert into sped_ctb_i500
        (cod_empresa,          exercicio,
         tipo_livro_aux,       tam_fonte,
         nom_campo,            desc_campo,
         tipo_campo,           tam_campo,
         dec_campo,            larg_col_campo,
         seq_campo)
      values
        (p_cod_empresa,        p_exercicio,
         p_layout,             8,
         'V_CREDITO',          'VALOR CREDITO',
         'N',                  14,
         2,                    15,
         6);

   exception
      when others then
         p_des_erro := 'Erro na inclusao da tabela sped_i500 (7) ' || Chr(10) || SQLERRM;
         RAISE V_ERRO;
   end;

   commit;

exception
   when V_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_i500 ' || Chr(10) || p_des_erro;

   when others then
      p_des_erro := 'Outros erros na procedure p_gera_sped_i500 ' || Chr(10) || SQLERRM;

end inter_pr_gera_ctb_sped_i500;
 

/

exec inter_pr_recompile;

