alter table obrf_150
add (
	intermediador numeric(1),
	cgc9_intermediador numeric(9),
	cgc4_intermediador numeric(4),
	cgc2_intermediador numeric(2),
	id_cad_int_tran varchar2(60)
);
