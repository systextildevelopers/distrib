
  CREATE OR REPLACE TRIGGER "LOJA_TR_PEDI_150" 
BEFORE  UPDATE
of seq_endereco,end_entr_cobr,numero_imovel,bairro_entr_cobr,
   complemento_endereco,cep_entr_cobr

on pedi_150
for each row

begin

  if updating
  then
     :new.flag_exportacao_loja := 0;
  end if;


end loja_tr_pedi_150;
-- ALTER TRIGGER "LOJA_TR_PEDI_150" ENABLE
 

/

exec inter_pr_recompile;

