--228438 - etiq_fa02
alter table "PCPB_010" add
("PENDRIVE" VARCHAR2(6));

alter table "PCPB_010" add
("NTC" NUMBER(6,0));

alter table "PCPB_010" add
("BATIDAS" NUMBER(4,0));

alter table "PCPB_010" add
("MATRIZ" VARCHAR2(8));

alter table "PCPB_010" add
("MDC" VARCHAR2(8));

alter table "PCPB_010" add
("FATOR" NUMBER); 
