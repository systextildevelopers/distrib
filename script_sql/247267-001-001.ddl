create table pedi_813_log(
	id number(38),
	nr_solicitacao number,
	codigo_relato varchar2(255),
	data_executacao date,
	usuario_execucao varchar2(255),
	tp_processo number,
	descr_processo varchar2(255),
	codigo_cancelamento number,
	status number,
	status_obs clob,
	pedido_origem number,
	pedido_destino number,
	dt_embarque_origem date,
	dt_embarque_destino date,
	tabela_preco_origem varchar2(255),
	tabela_preco_destino varchar2(255),
	sequencia_origem number,
	sequencia_destino number,
	produto_origem varchar2(255),
	produto_destino varchar2(255),
	deposito_origem number,
	deposito_destino number,
	qtde_saldo_origem number,
	quantidade_origem number,
	quantidade_destino number,
	natureza_origem number,
	natureza_destino number,
	classificacao_origem number,
	classificacao_dest number,
	primary key(id)
);

COMMENT ON COLUMN pedi_813_log.nr_solicitacao IS 'Número da solicitacao agendamento';
COMMENT ON COLUMN pedi_813_log.codigo_relato IS 'Codigo relato agendamento';
COMMENT ON COLUMN pedi_813_log.data_executacao IS 'Data da execução agendamento';
COMMENT ON COLUMN pedi_813_log.usuario_execucao IS 'Usuário que executou';
COMMENT ON COLUMN pedi_813_log.tp_processo IS 'Tipo Processo: 0 agrupamento ou 1 transferencia';
COMMENT ON COLUMN pedi_813_log.descr_processo IS 'Descrição Processo para caso abrir um novo pedido';
COMMENT ON COLUMN pedi_813_log.pedido_origem IS 'Pedido origem transferencia (pedi_100.pedido_venda)';
COMMENT ON COLUMN pedi_813_log.dt_embarque_origem IS 'Data de embarque origem.(date pedi_100.data_entr_venda)';
COMMENT ON COLUMN pedi_813_log.tabela_preco_origem IS 'tabela preço pedi_100(colecao_tabela, mes_tabela, sequencia_tabela)';
COMMENT ON COLUMN pedi_813_log.sequencia_origem IS 'Sequência origem item pedido (pedi_110.seq_item_pedido)';
COMMENT ON COLUMN pedi_813_log.produto_origem IS 'Produto origem (pedi_110 CD_IT_PE_NIVEL99, CD_IT_PE_GRUPO, CD_IT_PE_SUBGRUPO, CD_IT_PE_ITEM)';
COMMENT ON COLUMN pedi_813_log.deposito_origem IS 'Depósito origem.(pedi_110.codigo_deposito)';
COMMENT ON COLUMN pedi_813_log.qtde_saldo_origem IS 'Quantidade origem (saldo do item).(pedi_110 qtde_pedida - qtde_faturada - qtde_afaturar)';
COMMENT ON COLUMN pedi_813_log.quantidade_origem IS 'Quantidade após o processamento (pedi_110 qtde_pedida - qtde_faturada - qtde_afaturar)';
COMMENT ON COLUMN pedi_813_log.codigo_cancelamento IS 'Codigo cancelament da tela(pedi_110.cod_cancelamento)';
COMMENT ON COLUMN pedi_813_log.natureza_origem IS 'Natureza Operacao Origem (pedi_110.cod_nat_op)';
COMMENT ON COLUMN pedi_813_log.classificacao_origem IS 'Classificação Origem (pedi_100.classificacao_pedido)';
COMMENT ON COLUMN pedi_813_log.pedido_destino IS 'Pedido destino selecionado na tela';
COMMENT ON COLUMN pedi_813_log.dt_embarque_destino IS 'Data de embarque destino.';
COMMENT ON COLUMN pedi_813_log.tabela_preco_destino IS 'Tabela de preço destino.';
COMMENT ON COLUMN pedi_813_log.sequencia_destino IS 'Tabela de preço destino.';
COMMENT ON COLUMN pedi_813_log.produto_destino IS 'Produto destino.';
COMMENT ON COLUMN pedi_813_log.deposito_destino IS 'Depósito destino.';
COMMENT ON COLUMN pedi_813_log.quantidade_destino IS 'Quantidade destino.';
COMMENT ON COLUMN pedi_813_log.natureza_destino IS 'Natureza Operacao destino.';
COMMENT ON COLUMN pedi_813_log.classificacao_dest IS 'Classificação destino.';
COMMENT ON COLUMN pedi_813_log.status IS 'Status (0 - operação concluída com sucesso, 1 - falha na operação).';
COMMENT ON COLUMN pedi_813_log.status_obs IS 'Descricao processo passo a passo';

create sequence ID_PEDI_813
minvalue 0
maxvalue 999999999
start with 1
increment by 1
cache 20;
