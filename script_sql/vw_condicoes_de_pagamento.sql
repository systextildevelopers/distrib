create or replace view vw_condicoes_de_pagamento as
select
  pedi_070.cond_pgt_cliente as id,
  pedi_070.descr_pg_cliente as descricao,
  count(1) as qtde_parcelas,
  fina_070.tarifa_emissao,
  fina_070.tarifa_liquidacao,
  fina_070.taxa,
  fina_070.id_forma_de_pagamento as id_forma_de_pagamento
from pedi_070
inner join pedi_075 on pedi_070.cond_pgt_cliente = pedi_075.condicao_pagto
left join fina_070 on fina_070.id_condicao_pagamento = pedi_070.cond_pgt_cliente
group by
  pedi_070.cod_forma_pagto,
  pedi_070.cond_pgt_cliente,
  pedi_070.descr_pg_cliente,
  fina_070.tarifa_emissao,
  fina_070.tarifa_liquidacao,
  fina_070.taxa,
  fina_070.id_forma_de_pagamento
