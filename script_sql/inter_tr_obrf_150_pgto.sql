CREATE OR REPLACE TRIGGER inter_tr_obrf_150_pgto
before delete on OBRF_150
    for each row

    declare
    
    begin
     delete from obrf_164 where obrf_164.id_nfe = :old.id;
     delete from obrf_151 where obrf_151.nfe_id = :old.id;
     
 end inter_tr_obrf_150_pgto;
/
exec inter_pr_recompile;
