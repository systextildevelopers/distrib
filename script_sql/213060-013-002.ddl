DECLARE
    cursor i is select * from fatu_070
    where  fatu_070.saldo_duplicata > 0
      and  fatu_070.situacao_duplic != 2
      and  fatu_070.FND_TEM_ESPELHO is null;
begin
    for j in i
    loop
        update fatu_070
        set    fatu_070.FND_TEM_ESPELHO = 0
        where  fatu_070.codigo_empresa   = j.codigo_empresa
          and  fatu_070.cli_dup_cgc_cli9 = j.cli_dup_cgc_cli9
          and  fatu_070.cli_dup_cgc_cli4 = j.cli_dup_cgc_cli4
          and  fatu_070.cli_dup_cgc_cli2 = j.cli_dup_cgc_cli2
          and  fatu_070.tipo_titulo      = j.tipo_titulo
          and  fatu_070.num_duplicata    = j.num_duplicata 
          and  fatu_070.seq_duplicatas   = j.seq_duplicatas;
        commit;
    end loop;
end;
/
