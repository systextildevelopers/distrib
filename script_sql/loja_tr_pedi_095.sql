
  CREATE OR REPLACE TRIGGER "LOJA_TR_PEDI_095" 
BEFORE  UPDATE
of val_tabela_preco

on pedi_095
for each row

begin

  if updating and :new.val_tabela_preco <> :old.val_tabela_preco
  then
     :new.flag_exportacao_loja := 0;
  end if;


end;
-- ALTER TRIGGER "LOJA_TR_PEDI_095" ENABLE
 

/

exec inter_pr_recompile;

