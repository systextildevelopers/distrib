
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_015_SPED" 
BEFORE INSERT
or UPDATE of cvf_ipi_entrada, cod_vlfiscal_ipi
on obrf_015
for each row

begin
  if :new.cvf_ipi_entrada = 0
  then
     :new.cod_vlfiscal_ipi := 1;
  end if;
  if :new.cvf_ipi_entrada = 2
  then
     :new.cod_vlfiscal_ipi := 2;
  end if;
  if :new.cvf_ipi_entrada = 1 or :new.cvf_ipi_entrada = 3
  or :new.cvf_ipi_entrada = 4 or :new.cvf_ipi_entrada = 5
  or :new.cvf_ipi_entrada = 49
  then
     :new.cod_vlfiscal_ipi := 3;
  end if;

  if :new.cvf_ipi_entrada = 99
  then
     if :new.cod_vlfiscal_ipi = 1
     then
       :new.cvf_ipi_entrada := 0;
     end if;
     if :new.cod_vlfiscal_ipi = 2
     then
       :new.cvf_ipi_entrada := 2;
     end if;
     if :new.cod_vlfiscal_ipi = 3
     then
       :new.cvf_ipi_entrada := 49;
     end if;
  end if;

end inter_tr_obrf_015_sped;
-- ALTER TRIGGER "INTER_TR_OBRF_015_SPED" ENABLE
 

/

exec inter_pr_recompile;

