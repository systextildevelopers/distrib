alter table estq_040
add (data_imagem              date);

comment on column estq_040.data_imagem
 is 'Este campo serve para guardar a data de geracao do estoque congelado do Systextil para executar a sugest�o de faturamento.';

/

exec inter_pr_recompile;
