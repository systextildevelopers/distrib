alter table pcpb_015 add codigo_perfil number(9);
comment on column pcpb_015.codigo_perfil is 'Codigo do perfil de manutenção de estágios que adicionou este registro';

exec inter_pr_recompile;
