create table finalidade_ob_permissoes(
    id                 number(5)
    ,codigo_finalidade number(3)
    ,codigo_empresa    number(3)
    ,usuario           varchar2(30)
);

alter table finalidade_ob_permissoes
add constraint pk_finalidade_ob_permissoes primary key (id);

alter table finalidade_ob_permissoes
add constraint fk_finalidade_ob_permissoes foreign key (codigo_finalidade) references finalidade_ob (codigo_finalidade);

alter table finalidade_ob_permissoes
add constraint fk_finalidade_ob_perm_ft500 foreign key (codigo_empresa) references fatu_500 (codigo_empresa);

alter table finalidade_ob_permissoes
add constraint uk_finalidade_ob_permissoes unique (usuario ,codigo_empresa ,codigo_finalidade);
