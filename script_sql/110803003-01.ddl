create table inte_087_lnd (
	cod_empresa			number(9)	default 0,
	cod_natu_oper		number(9)	default 0,
	cod_deposito		number(9)	default 0,
	cod_origem_pedido 	number(9)	default 0
);

alter table inte_087_lnd add constraint pk_inte_087_lnd primary key(cod_empresa);

exec inter_pr_recompile;
