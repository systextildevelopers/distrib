
  CREATE OR REPLACE TRIGGER "LOJA_TR_BASI_010" 
BEFORE  UPDATE
of narrativa, descricao_15, item_ativo, codigo_barras

on basi_010
for each row

begin

  if updating and
     :new.narrativa <> :old.narrativa or
     :new.descricao_15 <> :old.descricao_15 or
     :new.item_ativo <> :old.item_ativo
  then
     :new.flag_exportacao_loja := 0;
  end if;


end;
-- ALTER TRIGGER "LOJA_TR_BASI_010" ENABLE
 

/

exec inter_pr_recompile;

