  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_070" 
   before insert or
          update of valor_duplicata, saldo_duplicata,
                    cod_canc_duplic, situacao_duplic,
                    duplic_emitida,  nr_titulo_banco
   on fatu_070
   for each row

declare
   v_executa_trigger      number;
   v_empresa_matriz       number;
   v_cod_posicao_titulo_desc fatu_504.cod_posicao_titulo_desc%type;
   v_cod_forma_pagto         fatu_050.cod_forma_pagto%type;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   -- SS 55642 - Atualiza o campo resposavel_receb quando ele vier zerado ou nulo
   if inserting
   then
      if :new.responsavel_receb = 0 or :new.responsavel_receb is null
      then
         begin
            select fatu_500.codigo_matriz into v_empresa_matriz
            from fatu_500
            where  fatu_500.codigo_empresa = :new.codigo_empresa;
         exception
            when others then
               v_empresa_matriz := 0;
         end;

         :new.responsavel_receb := v_empresa_matriz;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      -- data_ult_movim_pagto    -> maior data de pagamento da duplicata
      -- data_ult_movim_creditoo -> maior data de credito da duplicata
      if inserting
      then
         :new.data_ult_movim_pagto   := :new.data_emissao;
         :new.data_ult_movim_credito := :new.data_emissao;
         :new.saldo_duplicata        := :new.valor_duplicata;
         :new.situacao_duplic        := 0;
         :new.cod_canc_duplic        := 0;
         :new.data_canc_duplic       := null;
         
         if :new.cod_forma_pagto is null or :new.cod_forma_pagto = 0
         then
            begin
               select fatu_050.cod_forma_pagto into v_cod_forma_pagto from fatu_050
               where fatu_050.codigo_empresa  = :new.codigo_empresa
                 and fatu_050.num_nota_fiscal = :new.num_duplicata
                 and fatu_050.serie_nota_fisc = :new.serie_nota_fisc
                 and fatu_050.origem_nota    <> 3
                 and fatu_050.cod_forma_pagto > 0;
               exception
               when no_data_found then
                  v_cod_forma_pagto := 0;
            end;
            
            if v_cod_forma_pagto > 0
            then
               :new.cod_forma_pagto := v_cod_forma_pagto;
            end if;
            
         end if;
      end if;

      if updating
      then
         /* UTILIZADO PARA REGIME DE CAIXA - SPED PIS/COFINS - TITULOS DESCONTADOS NO BANCO DEVEM FAZER PARTE DA BASE DE CALCULO */
         BEGIN
            select fatu_504.cod_posicao_titulo_desc into v_cod_posicao_titulo_desc from fatu_504
            where fatu_504.codigo_empresa = :new.codigo_empresa;
            exception
            when others then
               v_cod_posicao_titulo_desc := null;
         END;

         if v_cod_posicao_titulo_desc > 0                            and
            :new.nr_titulo_banco      is not null                    and
            v_cod_posicao_titulo_desc = :new.posicao_duplic          and
            :old.duplic_emitida      <> :new.duplic_emitida          and
            :new.duplic_emitida       = 3
         then
            :new.data_aceite_banco := sysdate;
         end if;

        -- se a nova data_ult_movim_pagto for nula, esta sera a data_emissao da duplicata
         if :new.data_ult_movim_pagto is null
         then
            :new.data_ult_movim_pagto   := :new.data_emissao;
         end if;

         -- se a nova data_ult_movim_credito for nula, esta sera a data_emissao da duplicata
         if :new.data_ult_movim_credito is null
         then
            :new.data_ult_movim_credito := :new.data_emissao;
         end if;

         -- se o valor_duplicata for alterado, recalcula o saldo da duplicata
         if :old.valor_duplicata <> :new.valor_duplicata
         then
            :new.saldo_duplicata := :old.saldo_duplicata - (:old.valor_duplicata - :new.valor_duplicata);
         end if;

         -- altera a situacao_duplic, conforme abaixo

         -- se o titulo for cancelado, a situacao_duplic passa para 2 (cancelada)
         if :new.cod_canc_duplic > 0
         then
            :new.situacao_duplic := 2;
         else

            -- se o saldo da duplicata for >= ao valor_duplicata, a situacao_duplic passa para 0 (aberto)
            if :new.saldo_duplicata >= :new.valor_duplicata
            then
               :new.situacao_duplic := 0;
            else

               -- se o saldo da duplicata for = 0.00, a situacao_duplic passa para 1 (Pago total)
               if :new.saldo_duplicata = 0.00
               then
                  :new.situacao_duplic := 1;
               else

                  -- se o saldo da duplicata for < 0.00, a situacao_duplic passa para 4 (Pago a maior)
                  if :new.saldo_duplicata < 0.00
                  then
                     :new.situacao_duplic := 4;
                  else
                     -- senao a situacao_duplic passa para 3 (Pago a menor)
                     :new.situacao_duplic := 3;
                  end if;
               end if;
            end if;
         end if;
      end if;
   end if;
end inter_tr_fatu_070;
/
exec inter_pr_recompile;
