-- Create table
create table PEDI_178
(
  PERIODO_COTAS   NUMBER(4) default 0 not null,
  CODIGO_REGIAO   NUMBER(3) default 0 not null,
  CODIGO_REPR     NUMBER(5) default 0 not null,
  ARTIGO_COTAS    NUMBER(4) default 0 not null,
  QTDE_COTAS      NUMBER(15,4) default 0.0,
  VALOR_COTAS     NUMBER(14,2) default 0.0,
  QTDE_VENDIDA    NUMBER(15,3) default 0.0,
  VALOR_VENDIDO   NUMBER(14,2) default 0.0,
  QTDE_CANCELADA  NUMBER(15,3) default 0.0,
  VALOR_CANCELADO NUMBER(14,2) default 0.0,
  UN_MEDIDA       VARCHAR2(2),
  CLIENTE9        NUMBER(9) default 0 not null,
  CLIENTE4        NUMBER(4) default 0 not null,
  CLIENTE2        NUMBER(2) default 0 not null,
  REFERENCIA      VARCHAR2(5) default ' ' not null,
  NIVEL           VARCHAR2(1) default ' ' not null,
  GRUPO           VARCHAR2(5) default ' ' not null,
  SUBGRUPO        VARCHAR2(3) default ' ' not null,
  ITEM            VARCHAR2(6) default ' ' not null,
  TIPO_CONTROLE   VARCHAR2(100) DEFAULT ' ',
  NIVEL_CONTROLE  VARCHAR2(100) DEFAULT ' ',
  GRUPO_ECONOMICO varchar2(100) default ' ',
  TIPO_ESTAMPA	number(1)		default 0 not null
);

alter table pedi_178
add constraint PK_PEDI_178 primary key (PERIODO_COTAS, CODIGO_REGIAO, CODIGO_REPR, ARTIGO_COTAS, CLIENTE9, CLIENTE4, 
										CLIENTE2, REFERENCIA, NIVEL, GRUPO, SUBGRUPO, ITEM, NIVEL_CONTROLE, 
										TIPO_CONTROLE, GRUPO_ECONOMICO, TIPO_ESTAMPA);
  
alter table PEDI_178
  add constraint REF_PEDI_178_BASI_295 foreign key (ARTIGO_COTAS)
  references BASI_295 (ARTIGO_COTAS);
alter table PEDI_178
  add constraint REF_PEDI_178_PEDI_040 foreign key (CODIGO_REGIAO)
  references PEDI_040 (CODIGO_REGIAO);
