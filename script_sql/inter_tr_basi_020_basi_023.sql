create or replace trigger inter_tr_basi_020_basi_023
   after insert
   on basi_020
   for each row

-- Finalidade: Manter a tabela basi_023 atualizada conforme cada inserção da tabela basi_020 (SS 54335/002)
-- Autor.....: Aline Blanski
-- Data......: 08/09/10

declare

v_fator number := 0.00000;
v_flag varchar2(1);
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
 

begin
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting and ws_aplicativo <> 'basi_f920'
   then
      begin
         insert into basi_023( basi030_nivel030,
                               basi030_referenc,
                               tamanho_ref,
                               fator_conversao_un_para_kg)
                       values( :new.basi030_nivel030,
                               :new.basi030_referenc,
                               :new.tamanho_ref,
                               v_fator);

      exception when OTHERS then
          v_flag := 'P';
      end;
   end if;

end inter_tr_basi_020_basi_023;
/

exec inter_pr_recompile;
