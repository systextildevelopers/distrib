
  CREATE OR REPLACE FUNCTION "INTER_FN_FECHA_ESTOQUE" 
return varchar2
is
   erro_mensagem varchar2(250) := ' ';
   
begin
   begin
      inter_pr_fecha_estoque;

      exception
         when others then
            erro_mensagem := SQLERRM;
   end;  
   return(erro_mensagem);
end inter_fn_fecha_estoque;

 

/

exec inter_pr_recompile;

