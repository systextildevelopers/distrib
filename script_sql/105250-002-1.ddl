alter table fatu_504 add bloq_fatu_representante number(1) default 0;

comment on column fatu_504.bloq_fatu_representante 
is 'Indica se pode ser faturado um pedido para quando o cnpj do representante for o mesmo do cliente';

/
exec inter_pr_recompile;
