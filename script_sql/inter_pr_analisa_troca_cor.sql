CREATE OR REPLACE PROCEDURE "INTER_PR_ANALISA_TROCA_COR"
(
     p_colecao_1 number,
     p_colecao_2 number,
     p_colecao_3 number,
     p_colecao_4 number,
     p_data_ini date,
     p_data_fim date,
     p_considera_grupo number,
     p_grupo number,
     p_grupo_1 number,
     p_grupo_2 number,
     p_grupo_3 number,
     p_grupo_4 number,
     p_grupo_5 number,
     p_tipo_cliente number,
     p_tipo_cliente_1 number,
     p_tipo_cliente_2 number,
     p_tipo_cliente_3 number,
     p_tipo_cliente_4 number,
     p_tipo_cliente_5 number,
     p_tipo_cliente_6 number,
     p_analisa_sortido varchar2,
     p_codigo_usuario number,
     p_codigo_empresa number,
     p_inc_exc_cliente number,
     p_numero_interno_ini number,
     p_numero_interno_fim number,
     p_descr_erro         OUT varchar2
  )
is
   v_erro EXCEPTION;

begin
declare
   v_tem_saldo_ref   number(9);
   v_cor_atu         varchar2(6);
   v_registro        varchar2(4000);
   v_conta_sortido   number(9);
   v_qtde_trocas     number(9);
   v_qtde_trocado    number(9);

begin
     for reg_pedidos in (select pedi_100.pedido_venda,
                                pedi_110.seq_item_pedido,
                                pedi_110.cd_it_pe_nivel99 nivel,
                                pedi_110.cd_it_pe_grupo ref,
                                pedi_110.cd_it_pe_subgrupo tam,
                                pedi_110.cd_it_pe_item cor,
                                pedi_110.qtde_pedida,
                                pedi_100.cod_ped_cliente
                         from pedi_100,
                              pedi_110,
                              pedi_010,
                              basi_030
                         where pedi_100.pedido_venda      = pedi_110.pedido_venda
                         
                         -- Pega os pedidos informados no cadastro temporario, considerando todos ou especifico.
                         and ((pedi_100.pedido_venda in (select rcnb_060.grupo_estrutura 
                                                        from rcnb_060
                                                        where rcnb_060.tipo_registro = 179
                                                        and rcnb_060.nr_solicitacao = 994
                                                        and rcnb_060.subgru_estrutura = p_codigo_usuario
                                                        and rcnb_060.grupo_estrutura != 9999999) 
                          ) or exists (select rcnb_060.grupo_estrutura 
                                  from rcnb_060
                                  where rcnb_060.tipo_registro = 179
                                  and rcnb_060.nr_solicitacao = 994
                                  and rcnb_060.subgru_estrutura = p_codigo_usuario
                                  and rcnb_060.grupo_estrutura = 9999999)
                          )
                          
                         -- Pega os clientes informados no cadastro temporario, considerando inclusao e exclusao.
                          and (
                               (p_inc_exc_cliente = 1

                                 and exists (
                                   select rcnb_060.grupo_estrutura, rcnb_060.item_estrutura, rcnb_060.nivel_estrutura
                                   from rcnb_060
                                   where rcnb_060.tipo_registro = 60
                                   and rcnb_060.nr_solicitacao = 994
                                   and rcnb_060.grupo_estrutura = pedi_100.cli_ped_cgc_cli9
                                   and rcnb_060.item_estrutura = pedi_100.cli_ped_cgc_cli4
                                   and rcnb_060.nivel_estrutura = pedi_100.cli_ped_cgc_cli2
                                 )
                               ) or (p_inc_exc_cliente = 2
                                 and not exists (
                                   select rcnb_060.grupo_estrutura, rcnb_060.item_estrutura, rcnb_060.nivel_estrutura
                                   from rcnb_060
                                   where rcnb_060.tipo_registro = 60
                                   and rcnb_060.nr_solicitacao = 994
                                   and rcnb_060.grupo_estrutura = pedi_100.cli_ped_cgc_cli9
                                   and rcnb_060.item_estrutura = pedi_100.cli_ped_cgc_cli4
                                   and rcnb_060.nivel_estrutura = pedi_100.cli_ped_cgc_cli2
                                   and rcnb_060.grupo_estrutura <> '00000000'
                                   and rcnb_060.item_estrutura <> '0000'
                                   and rcnb_060.nivel_estrutura <> '00'
                                 )
                              )
                         )
                         and pedi_100.cli_ped_cgc_cli9  = pedi_010.cgc_9
                         and pedi_100.cli_ped_cgc_cli4  = pedi_010.cgc_4
                         and pedi_100.cli_ped_cgc_cli2  = pedi_010.cgc_2
                         and basi_030.nivel_estrutura   = pedi_110.cd_it_pe_nivel99
                         and basi_030.referencia        = pedi_110.cd_it_pe_grupo
                         and pedi_100.tecido_peca       = '1' -- Considerar somente pedidos de confeccao.
                         and pedi_100.cod_cancelamento  = 0 -- Nao cancelado CAPA.
                         and pedi_110.cod_cancelamento  = 0 -- Nao cancelado ITENS.
                         and pedi_110.qtde_sugerida     = 0 -- Nao sugerido.
                         and pedi_110.qtde_faturada     = 0 -- Nao faturado.
                         and pedi_110.qtde_afaturar     = 0 -- Sem volume montado.
                         and pedi_100.cli_ped_cgc_cli9  not in (select fatu_500.cgc_9
                                                                from fatu_500
                                                                where fatu_500.codigo_empresa = p_codigo_empresa)
                         and pedi_100.data_entr_venda   between p_data_ini and p_data_fim
                         and pedi_100.qtde_saldo_pedi   =  pedi_100.qtde_total_pedi
                           
                         --pega os grupos economicos informados na tela, considerando inclusao e exclusao
                         and  ((p_considera_grupo = 1
                               and (pedi_010.grupo_economico > 0
                                    and p_grupo = 1
                                    and pedi_010.grupo_economico in (p_grupo_1, p_grupo_2, p_grupo_3, p_grupo_4, p_grupo_5)
                                    or (p_grupo = 2
                                        and (pedi_010.grupo_economico not in (p_grupo_1, p_grupo_2, p_grupo_3, p_grupo_4, p_grupo_5)))
                                   )
                               ) or (p_considera_grupo = 2 and pedi_010.grupo_economico = 0)
                         )
                         
                         --pega o tipo de cliente informados na tela, considerando inclusao e exclusao
                         and ((p_tipo_cliente = 1 
                               and pedi_010.tipo_cliente in (p_tipo_cliente_1, p_tipo_cliente_2, p_tipo_cliente_3, p_tipo_cliente_4, p_tipo_cliente_5, p_tipo_cliente_6)
                              ) 
                              or (p_tipo_cliente = 2 
                                 and pedi_010.tipo_cliente not in (p_tipo_cliente_1, p_tipo_cliente_2, p_tipo_cliente_3, p_tipo_cliente_4, p_tipo_cliente_5, p_tipo_cliente_6)
                              )
                         )
                         and pedi_100.numero_controle  between p_numero_interno_ini and p_numero_interno_fim
                         and basi_030.colecao           in (p_colecao_1,p_colecao_2,p_colecao_3,p_colecao_4)
                          
                         -- Só analisa itens que o saldo do planejamento esteja negativo.
                         -- Este item do pedido que o sistema irá tentar fazer a troca por outra cor.
                         and exists (select 1 from pedi_994
                                     where pedi_994.nr_solicitacao = 1
                                       and pedi_994.nivel          = pedi_110.cd_it_pe_nivel99
                                       and pedi_994.grupo          = pedi_110.cd_it_pe_grupo
                                       and pedi_994.subgrupo       = pedi_110.cd_it_pe_subgrupo
                                       and pedi_994.item           = pedi_110.cd_it_pe_item
                                       and pedi_994.qtde_saldo    <= 0)

                         -- Só vai fazer a troca se o item do pedido já não foi trocado.
                         and not exists (select 1 from pedi_994
                                         where pedi_994.nr_solicitacao = 2
                                           and pedi_994.pedido_venda = pedi_100.pedido_venda
                                           and pedi_994.sequencia    = pedi_110.seq_item_pedido)
                         order by pedi_100.pedido_venda,
                                  pedi_110.seq_item_pedido
                         )
   loop

      
    v_conta_sortido := 0;

    -- p_analisa_sortido - Quando for S, sistema vai verificar se o produto entrou no sortido para sortimento automatico,
    -- se sim, faz a troca de cor no pedido.
    -- Quando for N, sistema não verifica e tenta trocar a cor mesmo que o pedido tenha sido feito na
    -- cor.

    if p_analisa_sortido = 'S'
    then
       select nvl(count(*),0)
       into v_conta_sortido
       from inte_110_log,
            basi_030
       where inte_110_log.item_item_new      = basi_030.cor_de_sortido
         and inte_110_log.item_nivel99_new   = basi_030.nivel_estrutura
         and inte_110_log.item_grupo_new     = basi_030.referencia
         and inte_110_log.pedido_cliente_new = reg_pedidos.cod_ped_cliente
         and inte_110_log.tipo_ocorr         = 'I'
         and inte_110_log.item_nivel99_new   = reg_pedidos.nivel
         and inte_110_log.item_grupo_new     = reg_pedidos.ref
         and inte_110_log.item_sub_new       = reg_pedidos.tam;

    end if;

    -- Só executa se é cor sortida ou se não valida o sortido.
    if v_conta_sortido > 0 or p_analisa_sortido = 'N'
    then

    -- Primeiro vamos ver se para o produto do pedido que esta negativo,
    -- tem alguma outra cor positiva...
    v_tem_saldo_ref := 0;

    begin
       select nvl(count(*),0)
       into v_tem_saldo_ref
       from pedi_994
       where pedi_994.nr_solicitacao = 1
         and pedi_994.nivel          = reg_pedidos.nivel
         and pedi_994.grupo          = reg_pedidos.ref
         and pedi_994.subgrupo       = reg_pedidos.tam
         and pedi_994.qtde_saldo     > 0;
    end;


    if v_tem_saldo_ref > 0
    then
       begin
          -- Busca a primeira cor com saldo para trocar no pedido de venda
          -- tentando por uma cor que n?o esteja no pedido.
          v_cor_atu := '000000';

          select pedi_994.item
          into v_cor_atu
          from pedi_994
          where pedi_994.nr_solicitacao = 1
            and pedi_994.nivel          = reg_pedidos.nivel
            and pedi_994.grupo          = reg_pedidos.ref
            and pedi_994.subgrupo       = reg_pedidos.tam
            and pedi_994.qtde_saldo     > 0
            and rownum                       = 1
            and not exists (select 1 from pedi_110
                            where pedi_110.pedido_venda      = reg_pedidos.pedido_venda
                              and pedi_110.cd_it_pe_nivel99  = reg_pedidos.nivel
                              and pedi_110.cd_it_pe_grupo    = reg_pedidos.ref
                              and pedi_110.cd_it_pe_subgrupo = reg_pedidos.tam
                              and pedi_110.seq_item_pedido  <> reg_pedidos.seq_item_pedido
                              and pedi_110.cd_it_pe_item    <> pedi_994.item);
          exception
          when others then
               v_cor_atu := '000000';

       end;


       -- Se nao achou a cor, busca a primeira com maior saldo...
       if v_cor_atu = '000000'
       then
          begin
             select pedi_994.item
             into v_cor_atu
             from pedi_994
             where pedi_994.nr_solicitacao = 1
               and pedi_994.nivel          = reg_pedidos.nivel
               and pedi_994.grupo          = reg_pedidos.ref
               and pedi_994.subgrupo       = reg_pedidos.tam
               and pedi_994.qtde_saldo     > 0
               and rownum                       = 1
               order by pedi_994.qtde_saldo desc;
             exception
             when others then
                  v_cor_atu := '000000';
          end;
       end if;


       if v_cor_atu <> '000000'
       then
          -- Troca a cor do item do pedido de venda.
          begin
             update pedi_110
             set pedi_110.cd_it_pe_item  = v_cor_atu
             where pedi_110.pedido_venda    = reg_pedidos.pedido_venda
               and pedi_110.seq_item_pedido = reg_pedidos.seq_item_pedido;
          end;


          -- Atualiza a tabela tempor?ria para termos o saldo atualizado.
          begin
             update pedi_994
             set pedi_994.qtde_reserva = pedi_994.qtde_reserva + 1,
                 pedi_994.qtde_saldo   = pedi_994.qtde_saldo - 1
             where pedi_994.nr_solicitacao = 1
               and pedi_994.nivel          = reg_pedidos.nivel
               and pedi_994.grupo          = reg_pedidos.ref
               and pedi_994.subgrupo       = reg_pedidos.tam
               and pedi_994.item           = v_cor_atu; -- Cor atual que estamos trocando no pedido.
          end;


          begin
             update pedi_994
             set pedi_994.qtde_reserva = pedi_994.qtde_reserva - 1,
                 pedi_994.qtde_saldo   = pedi_994.qtde_saldo + 1
             where pedi_994.nr_solicitacao = 1
               and pedi_994.nivel          = reg_pedidos.nivel
               and pedi_994.grupo          = reg_pedidos.ref
               and pedi_994.subgrupo       = reg_pedidos.tam
               and pedi_994.item           = reg_pedidos.cor; -- Cor que estava no pedido de venda.
          end;


          begin
             insert into pedi_994 (
               nr_solicitacao,
               pedido_venda,
               sequencia,
               nivel,
               grupo,
               subgrupo,
               item,
               item_prod,
               data_troca)
             VALUES
               (2,
               reg_pedidos.pedido_venda,
               reg_pedidos.seq_item_pedido,
               reg_pedidos.nivel,
               reg_pedidos.ref,
               reg_pedidos.tam,
               reg_pedidos.cor,
               v_cor_atu,
               sysdate
             );

          end;
          commit;
       end if;
      end if;
    end if;

   end loop;
end;

end INTER_PR_ANALISA_TROCA_COR;

/

exec inter_pr_recompile;
