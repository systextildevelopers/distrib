INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'pcpc_f151',     0,
	1,			     'Depósitos para Apontamento de Produção');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'pcpc_f151',   'pcpc_menu', 
	1,             0, 
	'S',           'S', 
	'S',           'S');
	
UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Depósitos para Apontamento de Produção'
 WHERE hdoc_036.codigo_programa = 'pcpc_f151'
   AND hdoc_036.locale          = 'es_ES';
   
COMMIT;
