-- Create table
create table PROJ_001
(
  ID        NUMBER not null,
  DESCRICAO VARCHAR2(100) not null
);

-- Create/Recreate primary, unique and foreign key constraints 
alter table PROJ_001
  add constraint PROJ_001_PK primary key (ID)
  using index;
