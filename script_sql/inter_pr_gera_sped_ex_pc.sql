create or replace procedure inter_pr_gera_sped_ex_pc(p_cod_matriz    IN NUMBER,
                                                       p_dat_inicial   IN DATE,
                                                       p_dat_final     IN DATE,
                                                       p_des_erro      OUT varchar2) is

--
-- Finalidade: Executa informa��es de notas de devolu��o de exporta��o
-- Autor.....: Edson Pio
-- Data......: 07/10/11


w_erro             EXCEPTION;
w_conta_registro   number;

begin
   begin
      delete from obrf_700
      where exists (select 1 from fatu_500 
                    where fatu_500.codigo_empresa = obrf_700.cod_empresa 
                      and fatu_500.codigo_matriz  = p_cod_matriz)
        and obrf_700.mes_apur    = to_char(p_dat_inicial,'MM')
        and obrf_700.ano_apur    = to_char(p_dat_inicial,'YYYY')
        and obrf_700.nome_programa = 'inter_pr_gera_devol_exp_pc'
        and obrf_700.ind_oper      = 3
        and obrf_700.ind_orig_cred = 1;
   end;
   commit;

   for nf_devol in (
      select obrf_010.local_entrega cod_empresa_orig, obrf_015.num_nota_orig, obrf_015.serie_nota_orig,
            obrf_010.total_docto valor_nota_orig
      from obrf_015, obrf_010, pedi_080, fatu_500
      where obrf_015.num_nota_orig    > 0
       and obrf_015.capa_ent_nrdoc   = obrf_010.documento
       and obrf_015.capa_ent_serie   = obrf_010.serie
       and obrf_015.capa_ent_forcli9 = obrf_010.cgc_cli_for_9
       and obrf_015.capa_ent_forcli4 = obrf_010.cgc_cli_for_4
       and obrf_015.capa_ent_forcli2 = obrf_010.cgc_cli_for_2
       and obrf_010.data_transacao   between p_dat_inicial and p_dat_final
       and obrf_010.situacao_entrada in (1,4)
       and obrf_010.local_entrega    = fatu_500.codigo_empresa
       and fatu_500.codigo_matriz    = p_cod_matriz
       and obrf_015.natitem_nat_oper = pedi_080.natur_operacao
       and obrf_015.natitem_est_oper = pedi_080.estado_natoper
       and pedi_080.livros_fiscais  = 1  -- 1 - Entra nos livros fiscais
       and pedi_080.ind_nf_ciap      < 2 -- 2 - n�o entra nos livros
       and pedi_080.cod_natureza || pedi_080.divisao_natur in ('3.201','3.202','3.503','1.503','2.503')
                                    /* DEVEOLU��O DE EXPORTA��O REDUZU A RECEITA DE EPORTA��O*/
      group by obrf_010.local_entrega, obrf_015.num_nota_orig, obrf_015.serie_nota_orig, obrf_010.total_docto
   )
   loop
      for nf_venda in (
         /* SOMA OS VALORES PARA INSERIR NA TABELA AJUSTES COMO REDU��O DE BASE/RECEITA */
         select pedi_080.cod_cred,
                fatu_060.cvf_pis,                             fatu_060.cvf_cofins,
                fatu_060.perc_pis,                            fatu_060.perc_cofins,
                sum(fatu_060.valor_faturado) vlr_faturado,    sum(fatu_060.basi_pis_cofins) base_pis_cofins,
                sum(fatu_060.valor_pis) vlr_pis,              sum(fatu_060.perc_cofins) vlr_cofins,
                pedi_080.cod_natureza || pedi_080.divisao_natur CFOP
         from fatu_060, pedi_080
         where fatu_060.ch_it_nf_cd_empr  = nf_devol.cod_empresa_orig
           and fatu_060.ch_it_nf_num_nfis = nf_devol.num_nota_orig
           and fatu_060.ch_it_nf_ser_nfis = nf_devol.serie_nota_orig
           and fatu_060.natopeno_nat_oper = pedi_080.natur_operacao
           and fatu_060.natopeno_est_oper = pedi_080.estado_natoper
         group by pedi_080.cod_cred,
                  fatu_060.cvf_pis,        fatu_060.cvf_cofins,
                  fatu_060.perc_pis,       fatu_060.perc_cofins,
                  pedi_080.cod_natureza || pedi_080.divisao_natur
      )
      loop
         w_conta_registro := 0;
         
         begin
         select count(*) into w_conta_registro  from obrf_700
            where obrf_700.cod_empresa   = nf_devol.cod_empresa_orig
              and obrf_700.mes_apur      = to_char(p_dat_inicial,'MM')
              and obrf_700.ano_apur      = to_char(p_dat_inicial,'YYYY')
              and obrf_700.nome_programa = 'inter_pr_gera_devol_exp_pc'
              and obrf_700.ind_oper      = 3
              and obrf_700.ind_orig_cred = 1;
         exception
            when no_data_found then
            w_conta_registro := 0;
         end;
         
         if w_conta_registro > 0
         then
            begin
               update obrf_700
               set obrf_700.vl_bc_pis = obrf_700.vl_bc_pis + nf_venda.base_pis_cofins,
                   obrf_700.vl_oper   = obrf_700.vl_oper   + nf_venda.vlr_faturado,
                   obrf_700.vl_pis    = obrf_700.vl_pis    + nf_venda.vlr_pis,
                   obrf_700.vl_cofins = obrf_700.vl_cofins + nf_venda.vlr_cofins
               where obrf_700.cod_empresa   = nf_devol.cod_empresa_orig
                 and obrf_700.mes_apur      = to_char(p_dat_inicial,'MM')
                 and obrf_700.ano_apur      = to_char(p_dat_inicial,'YYYY')
                 and obrf_700.nome_programa = 'inter_pr_gera_devol_exp_pc'
                 and obrf_700.ind_oper      = 3
                 and obrf_700.ind_orig_cred = 1;
            exception
            when others then
                  p_des_erro := 'ao atualiza��o da tabela obrf_700 - AJUSTES DE EXPORTA��O.' || Chr(10) || SQLERRM;
                  RAISE W_ERRO;
            end;
            commit;
         else
            begin
               insert into obrf_700 (
                   cod_empresa,     mes_apur,
                   ano_apur,
                   ind_oper,        vl_oper,

                   vl_bc_pis,       cst_pis,
                   cst_cofins,      vl_pis,
                   vl_cofins,       aliq_pis,

                   aliq_cofins,     ind_orig_cred,
                   nome_programa,   receita_operacional)
               values
                  (nf_devol.cod_empresa_orig,    to_char(p_dat_inicial,'MM'),
                   to_char(p_dat_inicial,'YYYY'),
                   3,                             nf_venda.vlr_faturado,

                   nf_venda.base_pis_cofins,     nf_venda.cvf_pis,
                   nf_venda.cvf_cofins,          nf_venda.vlr_pis,
                   nf_venda.vlr_cofins,          nf_venda.perc_pis,
                   nf_venda.perc_cofins,         1,
                   'inter_pr_gera_devol_exp_pc', 1
                  );
            exception
            when others then
               p_des_erro := 'Erro na inclus�o da tabela obrf_700 - AJUSTES DE EXPORTA��O.' || Chr(10) || SQLERRM;
               RAISE W_ERRO;
            end;
            commit;
         end if;
      end loop;
   end loop;
   
   EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_gera_sped_ex_pc ' || Chr(10) || p_des_erro;
end inter_pr_gera_sped_ex_pc;
/
exec inter_pr_recompile;
