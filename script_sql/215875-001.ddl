-- Add/modify columns 
alter table ESTQ_080 add peso_medio_caixa NUMBER(9,3) default 0.000;
-- Add comments to the columns 
comment on column ESTQ_080.peso_medio_caixa
  is 'Movimentar o estoque de fios informado apenas a quantidade de caixas';
