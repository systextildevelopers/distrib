create or replace trigger inter_tr_obrf_318
before insert on obrf_318 for each row
declare
    proximo_valor number;
begin
    if :new.id_obrf_318_apuracao is null then
        select id_obrf_318_apuracao.nextval into proximo_valor from dual;
        :new.id_obrf_318_apuracao := proximo_valor;
    end if;
end inter_tr_obrf_318;
