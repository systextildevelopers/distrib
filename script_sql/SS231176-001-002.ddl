create table pedi_074(
cnpj9 number (9) not null,
cnpj4 number (4) not null,
cnpj2 number (2) not null,
cod_marca number (9) not null,
cod_conceito number (9),
data_conceito date
);

alter table  pedi_074
add constraint PK_pedi_074 primary key (cnpj9,cnpj4,cnpj2,cod_marca );

EXEC inter_pr_recompile;
