create or replace trigger inter_tr_estq_110
before delete or update
on estq_110
for each row
declare
   vTmp number;
   v_endereca number;
   v_tipo_volume number;


begin

   v_endereca := 0;
   BEGIN
      select basi_205.endereca_caixa, basi_205.tipo_volume
      into v_endereca, v_tipo_volume
      from basi_205
      where basi_205.codigo_deposito = :old.deposito;
   EXCEPTION
      when others then
        v_endereca := 0;
   END;

   if (v_endereca=1) and (v_tipo_volume=1)
   then



      BEGIN
        select nvl(max(1),0)
        into vTmp
        from pcpc_330
        where :old.deposito = pcpc_330.deposito
          and :old.endereco = pcpc_330.endereco
          and (:old.nivel    = pcpc_330.nivel or :old.nivel = '0')
          and (:old.grupo    = pcpc_330.grupo or :old.grupo = '00000')
          and (:old.subgrupo = pcpc_330.subgrupo or :old.subgrupo = '000')
          and (:old.item     =  decode((select basi_030.cor_de_estoque
                                        from basi_030
                                        where basi_030.nivel_estrutura  = pcpc_330.nivel
                                          and basi_030.referencia       = pcpc_330.grupo),'009999','009999',pcpc_330.item)
                                           or :old.item    = '000000');
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            vTmp:=0;
      END;

      if (vTmp>0)
      then
         raise_application_error(-20000,'ATENCAO! Não e possivel eliminar/alterar um endereço com TAGs enderecadas');
      end if;
   end if;

end inter_tr_estq_110;



/

exec inter_pr_recompile;
