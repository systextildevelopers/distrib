
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_110_INTE" 
before insert or
       delete or
       update
       of pedido_venda,         seq_item_pedido,       cd_it_pe_nivel99,    cd_it_pe_grupo,
          cd_it_pe_subgrupo,    cd_it_pe_item,         codigo_deposito,     lote_empenhado,
          qtde_distribuida,     qtde_pedida,           valor_unitario,      qtde_faturada,
          nr_solicitacao,       qtde_afaturar,         situacao_fatu_it,    cod_cancelamento,
          dt_cancelamento,      dt_inclusao,           qtde_rolos,          nr_pedido_sub,
          seq_item_sub,         deposito_sub,          lote_sub,            seq_principal,
          qtde_pedida_loja,     qtde_sugerida,         observacao1,         observacao2,
          caixa_ped_ob,         kanban_ped_ob,         montador_ped_ob,     area_ped_ob,
          data_ent_prog,        caixa_pedi_ob,         kanban_pedi_ob,      codigo_acomp,
          data_empenho,         data_solicitacao,      item_ativo,          expedidor,
          data_leitura_coletor, ja_atualizado,         empenho_automatico,  corredor,
          box,                  data_sugestao,         perc_mao_obra,       caract,
          caract_ing,           caract_esp,            nr_sugestao,         agrupador_producao,
          codigo_embalagem,     um_faturamento_um,     um_faturamento_qtde, um_faturamento_valor,
          largura,              gramatura,             numero_reserva,      executa_trigger,
          cod_ped_cliente,      seq_original,          qtde_volumes,        nr_alternativa,
          nr_roteiro,           tipo_servico,          grupo_maquina,       subgrupo_maquina,
          numero_maquina,       motivo_reprocesso,     destino_projeto,     qtde_pecas_atend,
          produto_integracao,   prod_grade_integracao, seq_item_reserva,    cor_formulario_fal,
          seq_ped_compra,       percentual_desc,       acrescimo
on pedi_110
for each row

declare

   v_qtde_item_dentro  inte_wms_itens_ped.qtd_pedida%type;
   v_seq_item_pedido   pedi_110.seq_item_pedido%type;
   v_pedido_venda      pedi_100.pedido_venda%type;
   v_permite_alterar   pedi_110.permite_atu_wms%type;
   v_tem_pedido        number(1);

   v_utiliza_wms       number(1);
   
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(120) ;
begin

   v_utiliza_wms := 0;

   begin
      select max(fatu_503.uti_integracao_wms)
      into v_utiliza_wms
      from fatu_503
      where fatu_503.uti_integracao_wms = 1;
   exception
     when no_data_found then
        v_utiliza_wms := 0;
   end;
   
   -- Dados do usu?rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   

   if v_utiliza_wms = 1 and ws_aplicativo <> 'pedi_f194'
   then

      if inserting or updating
      then
         v_pedido_venda     := :new.pedido_venda;
         v_permite_alterar  := :new.permite_atu_wms;
         v_seq_item_pedido  := :new.seq_item_pedido;
      else
         v_pedido_venda     := :old.pedido_venda;
         v_permite_alterar  := :old.permite_atu_wms;
         v_seq_item_pedido  := :old.seq_item_pedido;
      end if;

      begin

         /* BUSCAR QUANTO EST� DENTRO DO WMS PARA ESTE PEDIDO E ITEM */
         select nvl(sum(inte_wms_itens_ped.qtd_pedida), 0)
         into   v_qtde_item_dentro
         from inte_wms_itens_ped
         where inte_wms_itens_ped.numero_pedido     = v_pedido_venda
           and inte_wms_itens_ped.seq_item          = v_seq_item_pedido;
         exception
            when others then
               v_qtde_item_dentro := 0;

      end;

      begin

         select max(1)
         into   v_tem_pedido
         from inte_wms_pedidos
         where inte_wms_pedidos.numero_pedido      = v_pedido_venda
           and inte_wms_pedidos.flag_importacao_st <> 1;
         exception
            when others then
               v_tem_pedido := 0;

      end;

      if :old.pedido_venda                                <> :new.pedido_venda
      or :old.seq_item_pedido                             <> :new.seq_item_pedido
      or :old.cd_it_pe_nivel99                            <> :new.cd_it_pe_nivel99
      or :old.cd_it_pe_grupo                              <> :new.cd_it_pe_grupo
      or :old.cd_it_pe_subgrupo                           <> :new.cd_it_pe_subgrupo
      or :old.cd_it_pe_item                               <> :new.cd_it_pe_item
      or :old.codigo_deposito                             <> :new.codigo_deposito
      or :old.lote_empenhado                              <> :new.lote_empenhado
      or :old.qtde_distribuida                            <> :new.qtde_distribuida
      or :old.qtde_pedida                                 <> :new.qtde_pedida
--      or :old.valor_unitario                              <> :new.valor_unitario
--      or :old.qtde_faturada                               <> :new.qtde_faturada
--      or :old.nr_solicitacao                              <> :new.nr_solicitacao
--      or :old.qtde_afaturar                               <> :new.qtde_afaturar
--      or :old.situacao_fatu_it                            <> :new.situacao_fatu_it
      or :old.cod_cancelamento                            <> :new.cod_cancelamento
      or :old.dt_cancelamento                             <> :new.dt_cancelamento
      or :old.dt_inclusao                                 <> :new.dt_inclusao
      or :old.qtde_rolos                                  <> :new.qtde_rolos
      or :old.nr_pedido_sub                               <> :new.nr_pedido_sub
      or :old.seq_item_sub                                <> :new.seq_item_sub
      or :old.deposito_sub                                <> :new.deposito_sub
      or :old.lote_sub                                    <> :new.lote_sub
      or :old.seq_principal                               <> :new.seq_principal
      or :old.qtde_pedida_loja                            <> :new.qtde_pedida_loja

      or :old.observacao1                                 <> :new.observacao1
      or :old.observacao2                                 <> :new.observacao2
      or :old.caixa_ped_ob                                <> :new.caixa_ped_ob
      or :old.kanban_ped_ob                               <> :new.kanban_ped_ob
      or :old.montador_ped_ob                             <> :new.montador_ped_ob
      or :old.area_ped_ob                                 <> :new.area_ped_ob
      or :old.data_ent_prog                               <> :new.data_ent_prog
      or :old.caixa_pedi_ob                               <> :new.caixa_pedi_ob
      or :old.kanban_pedi_ob                              <> :new.kanban_pedi_ob
      or :old.codigo_acomp                                <> :new.codigo_acomp
      or :old.data_empenho                                <> :new.data_empenho
      or :old.data_solicitacao                            <> :new.data_solicitacao
      or :old.item_ativo                                  <> :new.item_ativo
      or :old.expedidor                                   <> :new.expedidor
      or :old.data_leitura_coletor                        <> :new.data_leitura_coletor
      or :old.ja_atualizado                               <> :new.ja_atualizado
      or :old.empenho_automatico                          <> :new.empenho_automatico
      or :old.corredor                                    <> :new.corredor
      or :old.box                                         <> :new.box
--      or :old.data_sugestao                               <> :new.data_sugestao
      or :old.perc_mao_obra                               <> :new.perc_mao_obra
      or :old.caract                                      <> :new.caract
      or :old.caract_ing                                  <> :new.caract_ing
      or :old.caract_esp                                  <> :new.caract_esp
--      or :old.nr_sugestao                                 <> :new.nr_sugestao
      or :old.agrupador_producao                          <> :new.agrupador_producao
      or :old.codigo_embalagem                            <> :new.codigo_embalagem
--      or :old.um_faturamento_um                           <> :new.um_faturamento_um
      or :old.um_faturamento_qtde                         <> :new.um_faturamento_qtde
      or :old.um_faturamento_valor                        <> :new.um_faturamento_valor
      or :old.largura                                     <> :new.largura
      or :old.gramatura                                   <> :new.gramatura
      or :old.numero_reserva                              <> :new.numero_reserva
      or :old.executa_trigger                             <> :new.executa_trigger
      or :old.cod_ped_cliente                             <> :new.cod_ped_cliente
      or :old.seq_original                                <> :new.seq_original
      or :old.qtde_volumes                                <> :new.qtde_volumes
      or :old.nr_alternativa                              <> :new.nr_alternativa
      or :old.nr_roteiro                                  <> :new.nr_roteiro
      or :old.tipo_servico                                <> :new.tipo_servico
      or :old.grupo_maquina                               <> :new.grupo_maquina
      or :old.subgrupo_maquina                            <> :new.subgrupo_maquina
      or :old.numero_maquina                              <> :new.numero_maquina
      or :old.motivo_reprocesso                           <> :new.motivo_reprocesso
      or :old.destino_projeto                             <> :new.destino_projeto
      or :old.qtde_pecas_atend                            <> :new.qtde_pecas_atend
      or :old.produto_integracao                          <> :new.produto_integracao
      or :old.prod_grade_integracao                       <> :new.prod_grade_integracao
      or :old.seq_item_reserva                            <> :new.seq_item_reserva
      or :old.cor_formulario_fal                          <> :new.cor_formulario_fal
      or :old.seq_ped_compra                              <> :new.seq_ped_compra
--      or :old.percentual_desc                             <> :new.percentual_desc
      or :old.acrescimo                                   <> :new.acrescimo

      or (:old.qtde_sugerida                              > :new.qtde_sugerida and :new.qtde_sugerida < v_qtde_item_dentro)   --S� permite diminuir a quantidade sugerida at� o limite do que est� dentro do WMS
      then
         if v_tem_pedido = 1 and (v_permite_alterar is null or v_permite_alterar = 0 )/* Nao permite alterar informacoes */
         then
            raise_application_error(-20000, 'Esse pedido j� foi importado para o WMS.');
         end if;
      end if;
   end if;

end inter_tr_pedi_110_inte;

-- ALTER TRIGGER "INTER_TR_PEDI_110_INTE" ENABLE
 

/

exec inter_pr_recompile;

