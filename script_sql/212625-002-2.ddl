create table basi_063 
(
   classific_fiscal varchar2(15) not null,
   cod_pais number(3) not null,
   descricao varchar2(250)
);

ALTER TABLE BASI_063
ADD CONSTRAINT PK_BASI_063 PRIMARY KEY (classific_fiscal, cod_pais);
