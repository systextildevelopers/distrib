create table ESTQ_250
(
  id         NUMBER(9) not null,
  endereco   VARCHAR2(25) not null,
  empresa    NUMBER(3),
  criado_por VARCHAR2(250),
  criado_em  TIMESTAMP(6) default current_timestamp
);
comment on table ESTQ_250
  is 'Cadastro de endere�os a serem ignorados na sugest�o de rolos (fatu_fa02)';
comment on column ESTQ_250.id
  is 'Chave prim�ria';
comment on column ESTQ_250.endereco
  is 'Endere�o';
comment on column ESTQ_250.empresa
  is 'Empresa do onde o registro foi cadastrado';
comment on column ESTQ_250.criado_por
  is 'Usu�rio que cadastrou';
comment on column ESTQ_250.criado_em
  is 'A data em que foi cadsatrado';
alter table ESTQ_250 add primary key (ID);
alter table ESTQ_250
  add constraint ESTQ_250_UNIQUE unique (ENDERECO, EMPRESA);
