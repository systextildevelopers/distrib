
  CREATE OR REPLACE PROCEDURE "INTER_PR_UPDATE_TMRP_625_FIC" 
   (p_ordem_planejamento    in number,      p_pedido_venda              in number,
    p_pedido_reserva        in number,      p_nivel_produto_origem      in varchar2,
    p_grupo_produto_origem  in varchar2,    p_subgrupo_produto_origem   in varchar2,
    p_item_produto_origem   in varchar2,    p_nivel_produto             in varchar2,
    p_grupo_produto         in varchar2,    p_subgrupo_produto          in varchar2,
    p_item_produto          in varchar2,    p_qtde_tecido_real          in number,
    p_seq_produto_origem    in number)
is
begin

   begin
      update tmrp_625
      set tmrp_625.qtde_reserva_planejada   = tmrp_625.qtde_reserva_planejada - p_qtde_tecido_real
      where tmrp_625.ordem_planejamento     = p_ordem_planejamento
       and tmrp_625.pedido_venda            = p_pedido_venda
       and tmrp_625.pedido_reserva          = p_pedido_reserva
       and tmrp_625.seq_produto_origem      = p_seq_produto_origem
       and tmrp_625.nivel_produto_origem    = p_nivel_produto_origem
       and tmrp_625.grupo_produto_origem    = p_grupo_produto_origem
       and tmrp_625.subgrupo_produto_origem = p_subgrupo_produto_origem
       and tmrp_625.item_produto_origem     = p_item_produto_origem
       and tmrp_625.nivel_produto           = p_nivel_produto
       and tmrp_625.grupo_produto           = p_grupo_produto
       and tmrp_625.subgrupo_produto        = p_subgrupo_produto
       and tmrp_625.item_produto            = p_item_produto;
   exception when
   OTHERS then
      raise_application_error(-20000,'Nao atualizou tmrp_625 ' || sqlerrm);
   end;
end inter_pr_update_tmrp_625_fic;

 

/

exec inter_pr_recompile;

