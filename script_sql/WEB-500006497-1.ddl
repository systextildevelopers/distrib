create table BASI_201
(
  UNIDADE_MEDIDA_FORN  VARCHAR2(3) default '' not null,
  DESCRICAO_UN_MED_FORN   VARCHAR2(20) default ''
);

alter table BASI_201
  add constraint PK_BASI_201 primary key (UNIDADE_MEDIDA_FORN);
