CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_100"
   before insert or
          delete or
          update of centro_custo,    codigo_contabil, qtde_saldo_item,
                    preco_item_comp, percentual_ipi,  percentual_desc,
                    data_prev_entr,  cod_cancelamento, situacao_item,
                    percentual_subs
   on supr_100
   for each row

declare
   v_tipo_orcamento         fatu_500.controla_orcam%type;
   v_numero_pedido          supr_090.pedido_compra%type;
   v_codigo_empresa         supr_090.codigo_empresa%type;
   v_codigo_transacao       supr_090.codigo_transacao%type;
   v_codigo_matriz          fatu_500.codigo_matriz%type;
   v_situacao_orcm          cont_500.situacao_orcm%type;
   v_cod_plano_cta          cont_500.cod_plano_cta%type;
   v_valor_produto          orcm_010.valor_empenhado%type;
   v_valor_lancto           orcm_010.valor_empenhado%type;
   v_conta_reduzida         number;   --cra
   v_tipo_orcamento_plano   cont_535.tipo_orcamento%type;
   v_debito_credito         cont_535.debito_credito%type;
   v_patr_result            cont_535.patr_result%type;
   v_exige_subconta         cont_535.exige_subconta%type;
   v_centro_custo           supr_100.centro_custo%type;
   v_codigo_contabil_fornec supr_010.codigo_contabil%type;
   v_controle_orcm          cont_500.controle_orcamento%type;
   v_qtde_saldo_item        supr_100.qtde_saldo_item%type;
   v_data_emissao_pedido    supr_090.dt_emis_ped_comp%type;
   v_saldo_orcm             orcm_010.valor_orcado_atual%type;
   v_real_emp               orcm_010.valor_orcado_atual%type;

   v_mes_new                number;
   v_mes_old                number;
   v_ano_new                number;
   v_ano_old                number;
--   v_ano_orcm               number;
   v_ano_emis               number;
   v_data_contr_orcm        number;
   v_nr_registros           number;
--   v_reg_cont_500           number;
   v_tem_lib_ormc           number;

   v_processa_soma          boolean := true;
   v_processa_subt          boolean := true;
   v_deleta_orcm_050        boolean := false;
   v_so_atual_saldo         boolean := false;

   erro_sem_exercicio       exception;
   erro_sem_relacionamento  exception;
   erro_sem_conta           exception;

   v_executa_trigger        number;
   v_periodo_cont           number;
   v_exercicio_doc          number;
   v_exercicio              number;


begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      begin
         -- seta variaveis, conforme situacao: insert/update ou delete
         if inserting or updating
         then
            begin
               select supr_090.codigo_empresa, supr_090.codigo_transacao, supr_010.codigo_contabil,
                      trunc(supr_090.dt_emis_ped_comp)
               into   v_codigo_empresa,        v_codigo_transacao,        v_codigo_contabil_fornec,
                      v_data_emissao_pedido
               from supr_090, supr_010
               where supr_090.pedido_compra   = :new.num_ped_compra
               and   supr_090.forn_ped_forne9 = supr_010.fornecedor9
               and   supr_090.forn_ped_forne4 = supr_010.fornecedor4
               and   supr_090.forn_ped_forne2 = supr_010.fornecedor2;

        if :new.num_requisicao <> 0 and :new.situacao_item <> 3
        then
           begin
          update supr_067
            set numero_pedido = :new.num_ped_compra
          where supr_067.num_requisicao = :new.num_requisicao
              and supr_067.seq_item_req = :new.seq_item_req
              and supr_067.numero_pedido = 0;
          end;
        end if;

               if :new.situacao_item = 3
               then
                  begin
                     update supr_067
                        set supr_067.situacao = 8 -- Atendida
                      where supr_067.num_requisicao = :new.num_requisicao
                        and supr_067.seq_item_req   = :new.seq_item_req ;
                     exception
                       when others then null;
                  end;
               end if;
            end ;
			
			if :new.codigo_transacao > 0
			then 
			   v_codigo_transacao := :new.codigo_transacao;
			end if;
			
         else
            select supr_090.codigo_empresa, supr_090.codigo_transacao, supr_010.codigo_contabil,
                   supr_090.dt_emis_ped_comp
            into   v_codigo_empresa,        v_codigo_transacao,        v_codigo_contabil_fornec,
                   v_data_emissao_pedido
            from supr_090, supr_010
            where supr_090.pedido_compra   = :old.num_ped_compra
            and   supr_090.forn_ped_forne9 = supr_010.fornecedor9
            and   supr_090.forn_ped_forne4 = supr_010.fornecedor4
            and   supr_090.forn_ped_forne2 = supr_010.fornecedor2;
			
			if :old.codigo_transacao > 0
			then 
			   v_codigo_transacao := :old.codigo_transacao;
			end if;
			
         end if;

        if (updating and (:old.situacao_item = 3 and :new.situacao_item <> 3 ))
        then
          begin
             update supr_067
                set supr_067.situacao = 1 
              where supr_067.num_requisicao = :new.num_requisicao
                and supr_067.seq_item_req   = :new.seq_item_req ;
             exception
               when others then null;
          end;
        end if;


         -- verifica se a empresa utiliza o modulo orcamentario
         select codigo_matriz into   v_codigo_matriz from fatu_500
         where codigo_empresa = v_codigo_empresa;

         -- seta a empresa como sendo a matriz, pois a contabilidade
         -- e feita pela matriz
         v_codigo_empresa := v_codigo_matriz;

         -- verifica se a matriz utiliza o modulo orcamentario
         select controla_orcam into v_tipo_orcamento from fatu_500
         where codigo_empresa = v_codigo_empresa;

         if v_tipo_orcamento = 1
         then
        v_exercicio_doc := inter_fn_checa_data(v_codigo_empresa, v_data_emissao_pedido, 0);

            v_exercicio := inter_fn_checa_data(v_codigo_empresa, v_data_emissao_pedido,0);
            if v_exercicio_doc < 0
            then
               v_exercicio_doc := v_exercicio;
            end if;

            if v_exercicio_doc = 0
            then 
                v_exercicio_doc  := to_number(to_char(v_data_emissao_pedido, 'YYYY'));
            end if;
            

            v_ano_emis  := v_exercicio_doc;

            begin

            select cont_500.data_emp_orcamento into v_data_contr_orcm
               from cont_500
               where cod_empresa = v_codigo_empresa
               and   exercicio   = v_ano_emis;

               exception
               when no_data_found
               then raise erro_sem_exercicio;

            end;

            if inserting
            then
               -- se estiver inserindo registro ja cancelado, seta variaveis para nao processar
               if :new.cod_cancelamento > 0
               then
                  v_processa_soma := false;
               end if;

               v_processa_subt := false;

               if v_data_contr_orcm = 1
               then
            v_exercicio_doc := inter_fn_checa_data(v_codigo_empresa, :new.data_prev_entr, 0);

                  v_exercicio := inter_fn_checa_data(v_codigo_empresa, :new.data_prev_entr,0);
                  if v_exercicio_doc < 0
                  then
                     v_exercicio_doc := v_exercicio;
                  end if;
                  

                  if v_exercicio_doc = 0
                  then 
                     v_exercicio_doc  := to_number(to_char(:new.data_prev_entr, 'YYYY'));
                  end if;
                  

          begin
            select c.periodo into v_periodo_cont from cont_510 c
            where c.cod_empresa = v_codigo_empresa
            and c.exercicio   = v_exercicio_doc
            and :new.data_prev_entr between c.per_inicial and c.per_final;
            exception
              when no_data_found then
               v_periodo_cont := 0;
          end;
                  -- Se usar controle por DATA DE ENTREGA
                  v_mes_new := v_periodo_cont;
                  v_ano_new := v_exercicio_doc;

               else
            v_exercicio_doc := inter_fn_checa_data(v_codigo_empresa, v_data_emissao_pedido, 0);

                  v_exercicio := inter_fn_checa_data(v_codigo_empresa, v_data_emissao_pedido,0);
                  if v_exercicio_doc < 0
                  then
                     v_exercicio_doc := v_exercicio;
                  end if;
                  
                  if v_exercicio_doc = 0
                  then 
                     v_exercicio_doc  := to_number(to_char(v_data_emissao_pedido, 'YYYY'));
                  end if;                  

          begin
            select c.periodo into v_periodo_cont from cont_510 c
            where c.cod_empresa = v_codigo_empresa
            and c.exercicio   = v_exercicio_doc
            and v_data_emissao_pedido between c.per_inicial and c.per_final;
            exception
              when no_data_found then
               v_periodo_cont := 0;
          end;
                  -- Se usar controle por DATA DE EMISSAO
                  v_mes_new := v_periodo_cont;
                  v_ano_new := v_exercicio_doc;
               end if;

               v_numero_pedido := :new.num_ped_compra;

               :new.qtde_saldo_item := :new.qtde_pedida_item;
            end if;

            if updating
            then
               -- nao permite descancelar um item do pedido de compra
               if :old.cod_cancelamento > 0 and :new.cod_cancelamento = 0
               then
                  raise_application_error(-20000,'Nao se pode descancelar um item de pedido de compra.');
               end if;

               -- se o item ja estava cancelado, seta variaveis para nao processar
               if :old.cod_cancelamento > 0
               then
                  v_processa_soma := false;
                  v_processa_subt := false;
               else

                  if v_data_contr_orcm = 1
                  then
             v_exercicio_doc := inter_fn_checa_data(v_codigo_empresa, :old.data_prev_entr, 0);

           v_exercicio := inter_fn_checa_data(v_codigo_empresa, :old.data_prev_entr,0);
           if v_exercicio_doc < 0
           then
           v_exercicio_doc := v_exercicio;
           end if;
           
           if v_exercicio_doc = 0
           then 
              v_exercicio_doc  := to_number(to_char( :old.data_prev_entr, 'YYYY'));
           end if;              

           begin
            select c.periodo into v_periodo_cont from cont_510 c
            where c.cod_empresa = v_codigo_empresa
            and c.exercicio   = v_exercicio_doc
            and :old.data_prev_entr between c.per_inicial and c.per_final;
           exception
              when no_data_found then
              v_periodo_cont := 0;
           end;

                     -- Se usar controle por DATA DE ENTREGA
                     v_mes_old := v_periodo_cont;
                     v_ano_old := v_exercicio_doc;




           v_exercicio_doc := inter_fn_checa_data(v_codigo_empresa, :new.data_prev_entr, 0);

           v_exercicio := inter_fn_checa_data(v_codigo_empresa, :new.data_prev_entr,0);
           if v_exercicio_doc < 0
           then
           v_exercicio_doc := v_exercicio;
           end if;


           if v_exercicio_doc = 0
           then 
              v_exercicio_doc  := to_number(to_char( :new.data_prev_entr, 'YYYY'));
           end if;              

           begin
            select c.periodo into v_periodo_cont from cont_510 c
            where c.cod_empresa = v_codigo_empresa
            and c.exercicio   = v_exercicio_doc
            and :new.data_prev_entr between c.per_inicial and c.per_final;
           exception
              when no_data_found then
              v_periodo_cont := 0;
           end;

                     v_mes_new := v_periodo_cont;
                     v_ano_new := v_exercicio_doc;
                  else
             v_exercicio_doc := inter_fn_checa_data(v_codigo_empresa, v_data_emissao_pedido, 0);

           v_exercicio := inter_fn_checa_data(v_codigo_empresa, v_data_emissao_pedido,0);
           if v_exercicio_doc < 0
           then
              v_exercicio_doc := v_exercicio;
           end if;
           

           if v_exercicio_doc = 0
           then 
              v_exercicio_doc  := to_number(to_char(v_data_emissao_pedido, 'YYYY'));
           end if;            

           begin
            select c.periodo into v_periodo_cont from cont_510 c
            where c.cod_empresa = v_codigo_empresa
            and c.exercicio   = v_exercicio_doc
            and v_data_emissao_pedido between c.per_inicial and c.per_final;
            exception
              when no_data_found then
                v_periodo_cont := 0;
           end;
                     -- Se usar controle por DATA DE EMISSAO
                     v_mes_old := v_periodo_cont;
                     v_ano_old := v_exercicio_doc;
                     v_mes_new := v_periodo_cont;
                     v_ano_new := v_exercicio_doc;
                  end if;

                  -- se estiver cancelando o item, so processa a retirada do orcamento
                  if :new.cod_cancelamento > 0
                  then
                     v_processa_soma  := false;
                     v_processa_subt   := true;

                     v_deleta_orcm_050 := true;

                     v_qtde_saldo_item := :old.qtde_saldo_item;
                  else

                     -- se as variaveis usadas no orcamento nao foram alteradas, seta variaveis para
                     -- nao processar
                     if  :old.centro_custo    = :new.centro_custo
                     and :old.codigo_contabil = :new.codigo_contabil
                     and :old.qtde_saldo_item = :new.qtde_saldo_item
                     and :old.preco_item_comp = :new.preco_item_comp
                     and :old.percentual_desc = :new.percentual_desc
                     and :old.percentual_ipi  = :new.percentual_ipi
                     and :old.codigo_contabil = :new.codigo_contabil
                     and (v_mes_old = v_mes_new and v_ano_old = v_ano_new)
                     and :old.percentual_subs = :new.percentual_subs
                     then
                        v_processa_soma := false;
                        v_processa_subt := false;
                     else

                        if :old.centro_custo    <> :new.centro_custo
                        or :old.codigo_contabil <> :new.codigo_contabil
                        or :old.preco_item_comp <> :new.preco_item_comp
                        or :old.percentual_desc <> :new.percentual_desc
                        or :old.percentual_ipi  <> :new.percentual_ipi
                        or :old.codigo_contabil <> :new.codigo_contabil
                        or (v_mes_old <> v_mes_new or v_ano_old <> v_ano_new)
                        or :old.percentual_subs <> :new.percentual_subs
                        then
                           v_deleta_orcm_050 := true;
                           v_qtde_saldo_item := :old.qtde_saldo_item;
                        else
                           -- entrega do produto
                           if :old.qtde_saldo_item > :new.qtde_saldo_item
                           then
                              v_processa_soma   := false;
                              v_so_atual_saldo  := true;

                              v_qtde_saldo_item := :old.qtde_saldo_item - :new.qtde_saldo_item;
                           else  -- cancelamento da entrega (eliminação da NF)
                              v_so_atual_saldo  := true;

                              v_qtde_saldo_item := :old.qtde_saldo_item;
                           end if;

                           if v_qtde_saldo_item < 0.00
                           then
                              v_qtde_saldo_item := 0.00;
                           end if;
                        end if;
                     end if;
                  end if;
               end if;

               v_numero_pedido := :new.num_ped_compra;
            end if;

            if deleting
            then
               -- se o item ja estava cancelado, seta variaveis para nao processar
               if :old.cod_cancelamento > 0
               then
                  v_processa_subt := false;
               end if;

               v_processa_soma := false;

               if v_data_contr_orcm = 1
               then
            v_exercicio_doc := inter_fn_checa_data(v_codigo_empresa, :old.data_prev_entr, 0);

         v_exercicio := inter_fn_checa_data(v_codigo_empresa, :old.data_prev_entr,0);
         if v_exercicio_doc < 0
         then
         v_exercicio_doc := v_exercicio;
         end if;

         if v_exercicio_doc = 0
         then 
             v_exercicio_doc  := to_number(to_char(:old.data_prev_entr, 'YYYY'));
         end if;
         

         begin
          select c.periodo into v_periodo_cont from cont_510 c
          where c.cod_empresa = v_codigo_empresa
          and c.exercicio   = v_exercicio_doc
          and :old.data_prev_entr between c.per_inicial and c.per_final;
         exception
          when no_data_found then
            v_periodo_cont := 0;
         end;

                  -- Se usar controle por DATA DE ENTREGA
                  v_mes_old := v_periodo_cont;
                  v_ano_old := v_exercicio_doc;
               else
            v_exercicio_doc := inter_fn_checa_data(v_codigo_empresa, v_data_emissao_pedido, 0);

         v_exercicio := inter_fn_checa_data(v_codigo_empresa, v_data_emissao_pedido,0);
         if v_exercicio_doc < 0
         then
         v_exercicio_doc := v_exercicio;
         end if;
         
         
         if v_exercicio_doc = 0
         then 
             v_exercicio_doc  := to_number(to_char(v_data_emissao_pedido, 'YYYY'));
         end if;


         begin
          select c.periodo into v_periodo_cont from cont_510 c
          where c.cod_empresa = v_codigo_empresa
          and c.exercicio   = v_exercicio_doc
          and v_data_emissao_pedido between c.per_inicial and c.per_final;
         exception
          when no_data_found then
            v_periodo_cont := 0;
         end;
                  -- Se usar controle por DATA DE EMISSAO
                  v_mes_old := v_periodo_cont;
                  v_ano_old := v_exercicio_doc;
               end if;

               v_numero_pedido   := :old.num_ped_compra;
               v_qtde_saldo_item := :old.qtde_saldo_item;
            end if;

            -- verifica se deve processar a soma do orcamento (insert/update)
            if v_processa_soma = true
            then

               -- le o exercicio contabil para verificar a situacao e pegar o plano de contas
               begin
                  select situacao_orcm,   cod_plano_cta,   controle_orcamento
                  into   v_situacao_orcm, v_cod_plano_cta, v_controle_orcm
                  from cont_500
                  where cod_empresa = v_codigo_empresa
                  and   exercicio   = v_ano_new;

               exception
                  when no_data_found then
                     raise erro_sem_exercicio;
               end;

               if v_controle_orcm = 3 or (v_controle_orcm = 1 and v_so_atual_saldo = false)
               then

                  -- se a situacao nao poder aceitar lancamentos contabeis, gera erro
                  if v_situacao_orcm not in (1,2,4)  -- utilizado, revisao, bloqueado pela revisao
                  then
                     Raise_application_error(-20100, 'Situacao do orcamento no exercicio nao permite manutencao.');
                  end if;

                  -- encontra o valor do lancamento
                  v_valor_produto := :new.qtde_saldo_item * (:new.preco_item_comp - (:new.preco_item_comp * :new.percentual_desc / 100.00));
                  v_valor_produto := v_valor_produto + (v_valor_produto * (:new.percentual_ipi / 100.00)) + (v_valor_produto * (:new.percentual_subs / 100.00));

                  -- GERA O LANCAMENTO DE DEBITO (ESTOQUE/DESPESAS)

                  -- encontra a conta contabil pelo relacionamento contabil
                  begin
                     select conta_contabil into v_conta_reduzida from cont_550
                     where cod_plano_cta   = v_cod_plano_cta
                     and   tipo_contabil   = 3
                     and   codigo_contabil = :new.codigo_contabil
                     and   transacao       = v_codigo_transacao;
                  exception
                     when no_data_found then
                        begin
                           select conta_contabil into v_conta_reduzida from cont_550
                           where cod_plano_cta   = v_cod_plano_cta
                           and   tipo_contabil   = 3
                           and   codigo_contabil = 999999
                           and   transacao       = v_codigo_transacao
                           and   conta_contabil  = 99999;

                           v_conta_reduzida := :new.codigo_contabil;

                        exception
                           when no_data_found then
                              --raise erro_sem_relacionamento;
                              raise_application_error(-20103, 'Relacionamento contabil nao cadastrado.'|| chr(10) ||
                                                      'Tipo contabil: 3'                        || chr(10) ||
                                                      'Codigo Contabil: '||:new.codigo_contabil || chr(10) ||
                                                      'Codigo Transacao: '|| v_codigo_transacao || chr(10) ||
                                                      'Plano de Contas: '||v_cod_plano_cta      || chr(10) ||
                                                      'Codigo Exercicio: '||v_ano_new           || chr(10) ||
                                                      'Codigo Empresa: '||v_codigo_empresa);
                        end;
                  end;
                  -- le a conta contabil, se nao encontrar gera erro
                  begin
                     select tipo_orcamento,         debito_credito,   patr_result,   exige_subconta
                     into   v_tipo_orcamento_plano, v_debito_credito, v_patr_result, v_exige_subconta
                     from cont_535
                     where cod_plano_cta = v_cod_plano_cta
                     and   cod_reduzido  = v_conta_reduzida;

                  exception
                     when no_data_found then
                        raise erro_sem_conta;
                  end;

                  -- se a conta controlar orcamento, entao atualiza saldo orcamentario
                  if v_tipo_orcamento_plano in (1,2)
                  then

                     -- se a conta contabil for patrimonial ou se nao exigir centro de custo
                     -- deve-se zerar o centro de custo para o orcamento
                     if v_patr_result = 1 or v_exige_subconta = 2
                     then
                        v_centro_custo := 0;
                     else
                        v_centro_custo := :new.centro_custo;
                     end if;

                     -- verifica se o lancamento contabil e do mesmo tipo da conta contabil
                     -- (debito/credito). Se for, o lancamento deve ser somado, senao, o
                     -- lancamento deve ser subtraido
                     if v_debito_credito = 'D'
                     then
                        v_valor_lancto := v_valor_produto;
                     else
                        v_valor_lancto := v_valor_produto * (-1.0);
                     end if;

                     -- verifica se existe usuario liberador orcamentario
                     select count(*) into v_tem_lib_ormc
                     from hdoc_100
                     where area_bloqueio      = 3
                       and usubloq_empr_usu   = v_codigo_empresa
                       and lib_orcament       = 1
                       and situacao_liberador = 0;

                     -- se estourar o orcamento bloqueia o item do pedido e nao atualiza orcamento
                     select sum(orcm_010.valor_orcado_atual + orcm_010.valor_suplementar +
                                orcm_010.valor_transferido  + orcm_010.valor_justificado),
                            sum(orcm_010.valor_realizado    + orcm_010.valor_empenhado)
                     into v_saldo_orcm,
                          v_real_emp
                     from orcm_010
                     where cod_empresa    = v_codigo_empresa
                     and   exercicio      = v_ano_new
                     and   mes            = v_mes_new
                     and   centro_custo   = v_centro_custo
                     and   conta_reduzida = v_conta_reduzida;

                     v_real_emp := v_real_emp + v_valor_lancto;

                     if (v_saldo_orcm < v_real_emp   or
                         v_saldo_orcm is null)      and
                         v_tipo_orcamento_plano = 2 and
                         v_tem_lib_ormc         > 0
                     then
                        update supr_090
                        set situacao_pedido = 8
                        where pedido_compra = v_numero_pedido;

                        :new.flag_orcamento := 1;
                     end if;

                     -- atualiza valor empenhado
                     update orcm_010
                     set valor_empenhado = valor_empenhado + v_valor_lancto
                     where cod_empresa    = v_codigo_empresa
                     and   exercicio      = v_ano_new
                     and   mes            = v_mes_new
                     and   centro_custo   = v_centro_custo
                     and   conta_reduzida = v_conta_reduzida;

                     -- se nao encontrar, insere um registro no orcamento
                     if SQL%notfound
                     then
                        insert into orcm_010
                          (cod_empresa,         exercicio,
                           mes,                 centro_custo,
                           conta_reduzida,      valor_empenhado)
                        values
                          (v_codigo_empresa,    v_ano_new,
                           v_mes_new,           v_centro_custo,
                           v_conta_reduzida,    v_valor_lancto);
                     end if;

                     begin
                        insert into orcm_050
                          (cod_empresa,            exercicio,
                           periodo,                centro_custo,
                           conta_reduzida,         pedido_compra,
                           sequencia_pedido,       debito_credito)
                        values
                          (v_codigo_empresa,       v_ano_new,
                           v_mes_new,              v_centro_custo,
                           v_conta_reduzida,       v_numero_pedido,
                           :new.seq_item_pedido,   v_debito_credito);

                     exception
                         when dup_val_on_index then
                           null;
                     end;
                  end if;

                  -- GERA O LANCAMENTO DE CREDITO (FORNECEDOR/ CONTAS A PAGAR)

                  -- encontra a conta contabil pelo relacionamento contabil
                  begin
                     select conta_contabil into v_conta_reduzida from cont_550
                     where cod_plano_cta   = v_cod_plano_cta
                     and   tipo_contabil   = 2
                     and   codigo_contabil = v_codigo_contabil_fornec
                     and   transacao       = v_codigo_transacao;
                  exception
                     when no_data_found then
                        begin
                           select conta_contabil into v_conta_reduzida from cont_550
                           where cod_plano_cta   = v_cod_plano_cta
                           and   tipo_contabil   = 2
                           and   codigo_contabil = 999999
                           and   transacao       = v_codigo_transacao
                           and   conta_contabil  = 99999;
                        
                           v_conta_reduzida := v_codigo_contabil_fornec;

                        exception
                           when no_data_found then
                              --raise erro_sem_relacionamento;
                           raise_application_error(-20103, 'Relacionamento contabil nao cadastrado.' || chr(10) ||
                                                          'Tipo contabil: 2'                            || chr(10) ||
                                                          'Codigo Contabil: '||v_codigo_contabil_fornec || chr(10) ||
                                                          'Codigo Transacao: '||v_codigo_transacao      || chr(10) ||
                                                          'Plano de Contas: '||v_cod_plano_cta          || chr(10) ||
                                                          'Codigo Exercicio: '||v_ano_new               || chr(10) ||
                                                          'Codigo Empresa: '||v_codigo_empresa);

                        end;
                  end;
                  -- le a conta contabil, se nao encontrar gera erro
                  begin
                     select tipo_orcamento,         debito_credito,   patr_result,   exige_subconta
                     into   v_tipo_orcamento_plano, v_debito_credito, v_patr_result, v_exige_subconta
                     from cont_535
                     where cod_plano_cta = v_cod_plano_cta
                     and   cod_reduzido  = v_conta_reduzida;

                  exception
                     when no_data_found then
                        raise erro_sem_conta;
                  end;

                  -- se a conta controlar orcamento, entao atualiza saldo orcamentario
                  if v_tipo_orcamento_plano in (1,2)
                  then
                     -- se a conta contabil for patrimonial ou se nao exigir centro de custo
                     -- deve-se zerar o centro de custo para o orcamento
                     if v_patr_result = 1 or v_exige_subconta = 2
                     then
                        v_centro_custo := 0;
                     else
                        v_centro_custo := :new.centro_custo;
                     end if;

                     -- verifica se o lancamento contabil e do mesmo tipo da conta contabil
                     -- (debito/credito). Se for, o lancamento deve ser somado, senao, o
                     -- lancamento deve ser subtraido
                     if v_debito_credito = 'C'
                     then
                        v_valor_lancto := v_valor_produto;
                     else
                        v_valor_lancto := v_valor_produto * (-1.0);
                     end if;

                     -- verifica se existe usuario liberador orcamentario
                     select count(*) into v_tem_lib_ormc
                     from hdoc_100
                     where area_bloqueio      = 3
                       and usubloq_empr_usu   = v_codigo_empresa
                       and lib_orcament       = 1
                                           and situacao_liberador = 0;

                     -- se estourar o orcamento bloqueia o item do pedido e nao atualiza orcamento
                     select sum(orcm_010.valor_orcado_atual + orcm_010.valor_suplementar +
                                orcm_010.valor_transferido  + orcm_010.valor_justificado),
                            sum(orcm_010.valor_realizado    + orcm_010.valor_empenhado)
                     into v_saldo_orcm,
                          v_real_emp
                     from orcm_010
                     where cod_empresa    = v_codigo_empresa
                     and   exercicio      = v_ano_new
                     and   mes            = v_mes_new
                     and   centro_custo   = v_centro_custo
                     and   conta_reduzida = v_conta_reduzida;

                     v_real_emp := v_real_emp + v_valor_lancto;

                     if (v_saldo_orcm < v_real_emp   or
                         v_saldo_orcm is null)      and
                         v_tipo_orcamento_plano = 2 and
                         v_tem_lib_ormc         > 0
                     then
                        update supr_090
                        set situacao_pedido = 8
                        where pedido_compra = v_numero_pedido;

                        :new.flag_orcamento := 1;
                     end if;

                     -- atualiza valor empenhado
                     update orcm_010
                     set valor_empenhado = valor_empenhado + v_valor_lancto
                     where cod_empresa    = v_codigo_empresa
                     and   exercicio      = v_ano_new
                     and   mes            = v_mes_new
                     and   centro_custo   = v_centro_custo
                     and   conta_reduzida = v_conta_reduzida;

                     -- se nao encontrar, insere im registro no orcamento
                     if SQL%notfound
                     then
                        insert into orcm_010
                          (cod_empresa,         exercicio,
                           mes,                 centro_custo,
                           conta_reduzida,      valor_empenhado)
                        values
                          (v_codigo_empresa,    v_ano_new,
                           v_mes_new,           v_centro_custo,
                           v_conta_reduzida,    v_valor_lancto);
                     end if;  -- if SQL%notfound

                     begin
                       insert into orcm_050
                          (cod_empresa,            exercicio,
                           periodo,                centro_custo,
                           conta_reduzida,         pedido_compra,
                           sequencia_pedido,       debito_credito)
                        values
                          (v_codigo_empresa,       v_ano_new,
                           v_mes_new,              v_centro_custo,
                           v_conta_reduzida,       v_numero_pedido,
                           :new.seq_item_pedido,   v_debito_credito);
                     exception
                        when dup_val_on_index then
                           null;
                     end;
                  end if;     -- if v_tipo_orcamento_plano in (1,2)
               end if;        -- if v_controle_orcm in (1,3)
            end if;           -- if processa_soma = true

            -- *******************  GERA O ESTORNO DO ORCAMENTO *******************

            -- verifica se deve processar a subtrair do orcamento (delete/update)
            if v_processa_subt = true
            then
               -- le o exercicio contabil para verificar a situacao e pegar o plano de contas
               begin
                  select situacao_orcm,   cod_plano_cta,   controle_orcamento
                  into   v_situacao_orcm, v_cod_plano_cta, v_controle_orcm
                  from cont_500
                  where cod_empresa = v_codigo_empresa
                  and   exercicio   = v_ano_old;

               exception
                  when no_data_found then
                     raise erro_sem_exercicio;
               end;

               if v_controle_orcm = 3 or (v_controle_orcm = 1 and v_so_atual_saldo = false)
               then

                  -- se a situacao nao poder aceitar lancamentos contabeis, gera erro
                  if v_situacao_orcm not in (1,2,4)  -- utilizado, revisao, bloqueado pela revisao
                  then
                     Raise_application_error(-20100, 'Situacao do orcamento no exercicio nao permite manutencao.');
                  end if;

                  -- encontra o valor do lancamento
                  v_valor_produto := v_qtde_saldo_item * (:old.preco_item_comp - (:old.preco_item_comp * :old.percentual_desc / 100.00));
                  v_valor_produto := v_valor_produto + (v_valor_produto * (:old.percentual_ipi / 100.00)) + (v_valor_produto * (:old.percentual_subs / 100.00));

                  -- GERA O LANCAMEMENTO DE DEBITO (ESTOQUE/DESPESAS)

                  -- encontra a conta contabil pelo relacionamento contabil
                  begin
                     select conta_contabil into v_conta_reduzida from cont_550
                     where cod_plano_cta   = v_cod_plano_cta
                     and   tipo_contabil   = 3
                     and   codigo_contabil = :old.codigo_contabil
                     and   transacao       = v_codigo_transacao;
                  exception
                     when no_data_found then
                        begin
                           select conta_contabil into v_conta_reduzida from cont_550
                           where cod_plano_cta   = v_cod_plano_cta
                           and   tipo_contabil   = 3
                           and   codigo_contabil = 999999
                           and   transacao       = v_codigo_transacao
                           and   conta_contabil  = 99999;
                     
                           v_conta_reduzida := :old.codigo_contabil;

                        exception
                           when no_data_found then
                              --raise erro_sem_relacionamento;
                              raise_application_error(-20103, 'Relacionamento contabil nao cadastrado.'     || chr(10) ||
                                                              'Tipo contabil: 3'                            || chr(10) ||
                                                              'Codigo Contabil: '||:old.codigo_contabil      || chr(10) ||
                                                              'Codigo Transacao: '||v_codigo_transacao      || chr(10) ||
                                                              'Plano de Contas: '||v_cod_plano_cta          || chr(10) ||
                                                              'Codigo Exercicio: '||v_ano_new               || chr(10) ||
                                                              'Codigo Empresa: '||v_codigo_empresa);
                        end;
                  end;
                  -- le a conta contabil, se nao encontrar gera erro
                  begin
                     select tipo_orcamento,         debito_credito,   patr_result,   exige_subconta
                     into   v_tipo_orcamento_plano, v_debito_credito, v_patr_result, v_exige_subconta
                     from cont_535
                     where cod_plano_cta = v_cod_plano_cta
                     and   cod_reduzido  = v_conta_reduzida;

                  exception
                     when no_data_found then
                        raise erro_sem_conta;
                  end;

                  -- se a conta controlar orcamento, entao atualiza saldo orcamentario
                  if v_tipo_orcamento_plano in (1,2)
                  then
                     -- se a conta contabil for patrimonial ou se nao exigir centro de custo
                     -- deve-se zerar o centro de custo para o orcamento
                     if v_patr_result = 1 or v_exige_subconta = 2
                     then
                        v_centro_custo := 0;
                     else
                        v_centro_custo := :old.centro_custo;
                     end if;

                     select count(*) into v_nr_registros from orcm_050
                     where cod_empresa      = v_codigo_empresa
                     and   exercicio        = v_ano_old
                     and   periodo          = v_mes_old
                     and   centro_custo     = v_centro_custo
                     and   conta_reduzida   = v_conta_reduzida
                     and   pedido_compra    = v_numero_pedido
                     and   sequencia_pedido = :old.seq_item_pedido
                     and   debito_credito   = v_debito_credito;

                     if v_nr_registros > 0
                     then

                        -- verifica se o lancamento contabil e do mesmo tipo da conta contabil
                        -- (debito/credito). Se for, o lancamento deve ser somado, senao, o
                        -- lancamento deve ser subtraido
                        if v_debito_credito = 'D'
                        then
                           v_valor_lancto := v_valor_produto;
                        else
                           v_valor_lancto := v_valor_produto * (-1.0);
                        end if;

                        -- atualiza valor empenhado
                        update orcm_010
                        set valor_empenhado = valor_empenhado - v_valor_lancto
                        where cod_empresa    = v_codigo_empresa
                        and   exercicio      = v_ano_old
                        and   mes            = v_mes_old
                        and   centro_custo   = v_centro_custo
                        and   conta_reduzida = v_conta_reduzida;

                        -- se nao encontrar, insere im registro no orcamento
                        if SQL%notfound
                        then
                           insert into orcm_010
                             (cod_empresa,         exercicio,
                              mes,                centro_custo,
                              conta_reduzida,      valor_empenhado)
                           values
                             (v_codigo_empresa,    v_ano_old,
                              v_mes_old,           v_centro_custo,
                              v_conta_reduzida,    v_valor_lancto * (-1.0));
                        end if;

                        if (updating and (:new.qtde_saldo_item <= 0.00 or v_deleta_orcm_050 = true))
                        or  deleting
                        then

                           delete orcm_050
                           where cod_empresa      = v_codigo_empresa
                           and   exercicio        = v_ano_old
                           and   periodo          = v_mes_old
                           and   centro_custo     = v_centro_custo
                           and   conta_reduzida   = v_conta_reduzida
                           and   pedido_compra    = v_numero_pedido
                           and   sequencia_pedido = :old.seq_item_pedido
                           and   debito_credito   = v_debito_credito;
                        end if;
                     end if;
                  end if;

                  -- GERA O LANCAMEMENTO DE CREDITO (FORNECEDOR/ CONTAS A PAGAR)

                  -- encontra a conta contabil pelo relacionamento contabil
                  begin
                     select conta_contabil into v_conta_reduzida from cont_550
                     where cod_plano_cta   = v_cod_plano_cta
                     and   tipo_contabil   = 2
                     and   codigo_contabil = v_codigo_contabil_fornec
                     and   transacao       = v_codigo_transacao;
                  exception
                     when no_data_found then
                        begin
                           select conta_contabil into v_conta_reduzida from cont_550
                           where cod_plano_cta   = v_cod_plano_cta
                           and   tipo_contabil   = 2
                           and   codigo_contabil = 999999
                           and   transacao       = v_codigo_transacao
                           and   conta_contabil  = 99999;

                           v_conta_reduzida := v_codigo_contabil_fornec;

                        exception
                           when no_data_found then
                              --raise erro_sem_relacionamento;
                              raise_application_error(-20103, 'Relacionamento contabil nao cadastrado.'     || chr(10) ||
                                                              'Tipo contabil: 2'                            || chr(10) ||
                                                              'Codigo Contabil: '||v_codigo_contabil_fornec || chr(10) ||
                                                              'Codigo Transacao: '||v_codigo_transacao      || chr(10) ||
                                                              'Plano de Contas: '||v_cod_plano_cta          || chr(10) ||
                                                              'Codigo Exercicio: '||v_ano_new               || chr(10) ||
                                                              'Codigo Empresa: '||v_codigo_empresa);
                        end;
                  end;
                  -- le a conta contabil, se nao encontrar gera erro
                  begin
                     select tipo_orcamento,         debito_credito,   patr_result,   exige_subconta
                     into   v_tipo_orcamento_plano, v_debito_credito, v_patr_result, v_exige_subconta
                     from cont_535
                     where cod_plano_cta = v_cod_plano_cta
                     and   cod_reduzido  = v_conta_reduzida;

                  exception
                     when no_data_found then
                        raise erro_sem_conta;
                  end;

                  -- se a conta controlar orcamento, entao atualiza saldo orcamentario
                  if v_tipo_orcamento_plano in (1,2)
                  then
                     -- se a conta contabil for patrimonial ou se nao exigir centro de custo
                     -- deve-se zerar o centro de custo para o orcamento
                     if v_patr_result = 1 or v_exige_subconta = 2
                     then
                        v_centro_custo := 0;
                     else
                        v_centro_custo := :old.centro_custo;
                     end if;

                     select count(*) into v_nr_registros from orcm_050
                     where cod_empresa      = v_codigo_empresa
                     and   exercicio        = v_ano_old
                     and   periodo          = v_mes_old
                     and   centro_custo     = v_centro_custo
                     and   conta_reduzida   = v_conta_reduzida
                     and   pedido_compra    = v_numero_pedido
                     and   sequencia_pedido = :old.seq_item_pedido
                     and   debito_credito   = v_debito_credito;

                     if v_nr_registros > 0
                     then

                        -- verifica se o lancamento contabil e do mesmo tipo da conta contabil
                        -- (debito/credito). Se for, o lancamento deve ser somado, senao, o
                        -- lancamento deve ser subtraido
                        if v_debito_credito = 'C'
                        then
                           v_valor_lancto := v_valor_produto;
                        else
                           v_valor_lancto := v_valor_produto * (-1.0);
                        end if;

                        -- atualiza valor empenhado
                        update orcm_010
                        set valor_empenhado = valor_empenhado - v_valor_lancto
                        where cod_empresa    = v_codigo_empresa
                        and   exercicio      = v_ano_old
                        and   mes            = v_mes_old
                        and   centro_custo   = v_centro_custo
                        and   conta_reduzida = v_conta_reduzida;

                        -- se nao encontrar, insere im registro no orcamento
                        if SQL%notfound
                        then
                           insert into orcm_010
                             (cod_empresa,         exercicio,
                              mes,                 centro_custo,
                              conta_reduzida,      valor_empenhado)
                           values
                             (v_codigo_empresa,    v_ano_old,
                              v_mes_old,           v_centro_custo,
                              v_conta_reduzida,    v_valor_lancto * (-1.0));
                        end if;  -- if SQL%notfound

                        if (updating and (:new.qtde_saldo_item <= 0.00 or v_deleta_orcm_050 = true))
                        or  deleting
                        then

                           delete orcm_050
                           where cod_empresa      = v_codigo_empresa
                           and   exercicio        = v_ano_old
                           and   periodo          = v_mes_old
                           and   centro_custo     = v_centro_custo
                           and   conta_reduzida   = v_conta_reduzida
                           and   pedido_compra    = v_numero_pedido
                           and   sequencia_pedido = :old.seq_item_pedido
                           and   debito_credito   = v_debito_credito;
                        end if;
                     end if;
                  end if;     -- if v_tipo_orcamento_plano in (1,2)
               end if;        -- if v_controle_orcm in (1,3)
            end if;           -- if processa_subr = true
         end if;              -- if v_tipo_orcamento = 1

         exception
            when erro_sem_conta then
               raise_application_error(-20101,'Conta Contabil nao cadastrada no plano de contas.');
            when erro_sem_exercicio then
               raise_application_error(-20102,'Exercicio nao cadastrado.');
            when erro_sem_relacionamento then
               raise_application_error(-20110,'Relacionamentos contabil nao encontrado..');
      end;
   end if;
end inter_tr_supr_100;
