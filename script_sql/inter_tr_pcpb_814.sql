CREATE OR REPLACE TRIGGER inter_tr_pcpb_814
  BEFORE INSERT ON pcpb_814
  FOR EACH ROW

declare
   ot_gerada number; 
   
begin
   
   begin
      select gerar_pcpb_812_id.nextval
      into   :new.id
      from   dual;
   end;
   
    begin
       inter_pr_gerar_ot(
	   :new.id,			:new.tipo_ordem,
	   :new.lista_ob, 	:new.observacao, 			
	   ot_gerada);
	end;
	
	:new.ot_gerada := ot_gerada;

end inter_tr_pcpb_814;
/
