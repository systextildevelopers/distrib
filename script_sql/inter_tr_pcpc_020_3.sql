
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_020_3" 
     before insert
         or delete
         or update of qtde_programada, alternativa_peca
     on pcpc_020
     for each row
declare
   v_executa_trigger   number;
   v_codigo_empresa    number;
begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :new.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :old.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger = 0
   then
      if inserting
      then
         inter_pr_verif_reg_em_producao (0,:new.executa_trigger);

         inter_pr_verif_per_fechado(:new.periodo_producao,1);
      end if;

      if deleting
      then
         inter_pr_verif_reg_em_producao (0,:old.executa_trigger);

         inter_pr_verif_per_fechado(:old.periodo_producao,1);
      end if;
   end if;

   /*
      SS 82559/001 - Valida��o Situa��o Projeto.
   */
   if inserting
   then
      v_codigo_empresa := 0;

      begin
         select pcpc_010.codigo_empresa
         into   v_codigo_empresa
         from pcpc_010
         where pcpc_010.area_periodo     = 1
           and pcpc_010.periodo_producao = :new.periodo_producao;
      exception when others then
         v_codigo_empresa := 0;
      end;

      inter_pr_valida_sit_projeto('1', :new.referencia_peca,v_codigo_empresa);
   end if;

end inter_tr_pcpc_020_3;

-- ALTER TRIGGER "INTER_TR_PCPC_020_3" ENABLE
 

/

exec inter_pr_recompile;

