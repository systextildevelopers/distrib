create table obrf_317 (
id_obrf_317_apuracao number(9) default 0,
cod_matriz number(3) default 0,
cod_filial number(3) default 0,
ano number(4) default 0,
mes number(2) default 0,
val_credito number(17,2) default 0.0,
val_outros_creditos number(17,2) default 0.0,
val_credito_imobilizado number(17,2) default 0.0,
val_deducoes_creditos number(17,2) default 0.0,
val_total_creditos number(17,2) default 0.0,
val_debito number(17,2) default 0.0,
val_outros_debitos number(17,2) default 0.0,
val_reduc_acres_debitos number(17,2) default 0.0,
val_deducoes_debitos number(17,2) default 0.0,
val_total_debitos number(17,2) default 0.0,
val_saldos_ant number(17,2) default 0.0,
val_saldo_creddor_devedor number(17,2) default 0.0);


ALTER TABLE obrf_317
ADD CONSTRAINT pk_obrf_317 PRIMARY KEY (id_obrf_317_apuracao);

alter table obrf_317
  add constraint UNIQ_obrf_317_apuracao unique (cod_matriz, cod_filial, ano, mes);
  
create sequence id_obrf_317_apuracao start with 1 increment by 1;
