ALTER TABLE OPER_TMP 
MODIFY (FLO_33 NUMBER(22,7),
        FLO_36 NUMBER(22,7),
        FLO_34 NUMBER(22,7),
        FLO_27 NUMBER(22,7),
        FLO_30 NUMBER(22,7));

/
exec inter_pr_recompile;
