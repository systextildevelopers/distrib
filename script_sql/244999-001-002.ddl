create table ftec_300 (
  nivel_estrutura    varchar2(1),
  grupo_estrutura    varchar2(5),
  subgru_estrutura   varchar2(4),
  item_estrutura     varchar2(6),
  data_de_elaboracao date,
  data_de_revisao    date,

  peso_em_oncas      number,
  peso_em_metros     number,
  peso_tolerancia    number,
  peso_metodo        varchar2(2000),
  peso_condicao      varchar2(2000),

  largura_total_em_cm number,
  largura_util_em_cm  number,
  largura_tolerancia  number,
  largura_metodo      varchar2(2000),
  largura_condicao    varchar2(2000),

  encolhimento_trama             number,
  encolhimento_trama_tolerancia  number,
  encolhimento_urdume            number,
  encolhimento_urdume_tolerancia number,
  encolhimento_metodo            varchar2(2000),
  encolhimento_condicao          varchar2(2000),

  stretch_trama            varchar2(2000),
  stretch_trama_tolerancia number,
  stretch_trama_metodo     varchar2(2000),
  stretch_trama_condicao   varchar2(2000),
  
  movimento          number,
  movimento_metodo   varchar2(2000),
  movimento_condicao varchar2(2000),

  resist_rasgo_trama               number,
  resist_rasgo_urdume              number,
  resist_rasgo_trama_tolerancia    number,
  resist_rasgo_urdume_tolerancia   number,
  resist_rasgo_metodo              varchar2(2000),
  resist_rasgo_condicao            varchar2(2000),

  orientacao_lavanderia varchar2(2000),
  observacao_lavanderia varchar2(2000),
  observacao_costura    varchar2(2000),

  orientacao_fundo_1 varchar2(2000),
  orientacao_fundo_2 varchar2(2000),
  orientacao_fundo_3 varchar2(2000),
  orientacao_fundo_4 varchar2(2000),

  orientacao_reacao_quimi_1 varchar2(2000),
  orientacao_reacao_quimi_2 varchar2(2000),
  orientacao_reacao_quimi_3 varchar2(2000),
  orientacao_reacao_quimi_4 varchar2(2000),

  orientacao_efeitos_man_1 varchar2(2000),
  orientacao_efeitos_man_2 varchar2(2000),
  orientacao_efeitos_man_3 varchar2(2000),
  orientacao_efeitos_man_4 varchar2(2000),

  orientacao_laser_1 varchar2(2000),
  orientacao_laser_2 varchar2(2000),
  orientacao_laser_3 varchar2(2000),
  orientacao_laser_4 varchar2(2000),

  orientacao_amaciantes_1 varchar2(2000),
  orientacao_amaciantes_2 varchar2(2000),
  orientacao_amaciantes_3 varchar2(2000),
  orientacao_amaciantes_4 varchar2(2000),

  orientacao_reta_1 varchar2(2000),
  orientacao_reta_2 varchar2(2000),
  orientacao_reta_3 varchar2(2000),
  orientacao_reta_4 varchar2(2000),

  orientacao_overlock_1 varchar2(2000),
  orientacao_overlock_2 varchar2(2000),
  orientacao_overlock_3 varchar2(2000),
  orientacao_overlock_4 varchar2(2000),

  orientacao_fechadeira_1 varchar2(2000),
  orientacao_fechadeira_2 varchar2(2000),
  orientacao_fechadeira_3 varchar2(2000),
  orientacao_fechadeira_4 varchar2(2000),

  orientacao_travete_1 varchar2(2000),
  orientacao_travete_2 varchar2(2000),
  orientacao_travete_3 varchar2(2000),
  orientacao_travete_4 varchar2(2000),

  imagem_simbologia blob,
  imagem_rage_1 blob,
  imagem_rage_2 blob,
  imagem_rage_3 blob
);
/

alter table ftec_300 add constraint ftec_300_fk primary key (nivel_estrutura, grupo_estrutura, subgru_estrutura, item_estrutura)
/
