alter table sped_ctb_j100
add centro_custo number default 0;

comment on column sped_ctb_j100.centro_custo is 'centro de custo de despesa da conta contabil';

alter table sped_ctb_j100
  drop constraint PK_SPED_CTB_J1505;

prompt ATENCAO! Ao excluir este index se aparecer a mensagem "ORA-01418: specified index does not exist" favor ignorar.
drop index PK_SPED_CTB_J1505;


alter table sped_ctb_j100
  add constraint PK_SPED_CTB_J1505 primary key (COD_EMPRESA, EXERCICIO, TIPO_DEMONSTRACAO, COD_AGLUTINACAO, PERIODO, CENTRO_CUSTO) novalidate;
  
exec inter_pr_recompile;
