create or replace procedure wms_pr_atu_item_pedido (f_icdPedidoVenda  in number,
                                                    f_icdSeqPedido    in number,
                                                    f_scdNivelEstoque in varchar2,
                                                    f_scdGrupoEstoque in varchar2,
                                                    f_nqtPedida       in out number) is
  v_encontrou_reg               number; --0: N�o. 1: Sim
  f_cli_ped_cgc_cli9            number;
  f_cli_ped_cgc_cli4            number;
  f_cli_ped_cgc_cli2            number;
  f_data_digit_venda            date;
  f_codigo_empresa              number;
  f_cod_rep_cliente             number;
  f_cd_it_pe_nivel99            varchar2(1);
  f_cd_it_pe_grupo              varchar2(5);
  f_cd_it_pe_subgrupo           varchar2(3);
  f_cd_it_pe_item               varchar2(6);
  f_qtde_pedida                 number;
  f_codigo_deposito             number;
  f_lote_empenhado              number;
  f_valor_unitario              number;
  f_percentual_desc             number;
  f_seq_principal               number;
  f_codigo_embalagem            number;
  f_cod_nat_op                  number;
  f_est_nat_op                  varchar2(2);
  f_acrescimo                   number;
  f_seq_item_aux                number;

  f_mes_venda                   number;
  f_dia_venda                   number;
  f_ano_venda                   number;
begin

   v_encontrou_reg := 1;

   begin

      select    pedi_100.cli_ped_cgc_cli9,  pedi_100.cli_ped_cgc_cli4,
                pedi_100.cli_ped_cgc_cli2,  pedi_100.data_digit_venda,
                pedi_100.codigo_empresa,    pedi_100.cod_rep_cliente,
                pedi_110.cd_it_pe_nivel99,  pedi_110.cd_it_pe_grupo,
                pedi_110.cd_it_pe_subgrupo, pedi_110.cd_it_pe_item,
                pedi_110.qtde_pedida,       pedi_110.codigo_deposito,
                pedi_110.lote_empenhado,    pedi_110.valor_unitario,
                pedi_110.percentual_desc,   pedi_110.seq_principal,
                pedi_110.codigo_embalagem,  pedi_110.cod_nat_op,
                pedi_110.est_nat_op,        pedi_110.acrescimo
      into      f_cli_ped_cgc_cli9,         f_cli_ped_cgc_cli4,
                f_cli_ped_cgc_cli2,         f_data_digit_venda,
                f_codigo_empresa,           f_cod_rep_cliente,
                f_cd_it_pe_nivel99,         f_cd_it_pe_grupo,
                f_cd_it_pe_subgrupo,        f_cd_it_pe_item,
                f_qtde_pedida,              f_codigo_deposito,
                f_lote_empenhado,           f_valor_unitario,
                f_percentual_desc,          f_seq_principal,
                f_codigo_embalagem,         f_cod_nat_op,
                f_est_nat_op,               f_acrescimo
      from pedi_100, pedi_110
      where pedi_110.pedido_venda    = f_icdPedidoVenda
        and pedi_110.seq_item_pedido = f_icdSeqPedido
        and pedi_100.pedido_venda    = pedi_110.pedido_venda
        and rownum <= 1;
   exception
   when no_data_found then
      v_encontrou_reg := 0;
   end;

   if v_encontrou_reg = 1
   then

      if f_nqtPedida > f_qtde_pedida
      then f_nqtPedida := f_qtde_pedida;
      end if;

      begin
         UPDATE pedi_110
         set pedi_110.qtde_pedida     = pedi_110.qtde_pedida - f_nqtPedida,
             pedi_110.permite_atu_wms = 1
         where pedi_110.pedido_venda    = f_icdPedidoVenda
           and pedi_110.seq_item_pedido = f_icdSeqPedido;
      exception
      when others then
         raise_application_error(-20000, 'N�o atualizou a tabela PEDI_110.' || Chr(10) || SQLERRM);
      end;

      begin
         UPDATE pedi_110
         set pedi_110.permite_atu_wms   = 0
         where pedi_110.pedido_venda    = f_icdPedidoVenda
           and pedi_110.seq_item_pedido = f_icdSeqPedido;
      exception
      when others then
         raise_application_error(-20000, 'N�o atualizou a tabela PEDI_110.' || Chr(10) || SQLERRM);
      end;

      begin
         select min(pedi_110.seq_item_pedido)
         into   f_seq_item_aux
         from pedi_110
         where pedi_110.pedido_venda      = f_icdPedidoVenda
           and pedi_110.cd_it_pe_nivel99  = f_scdNivelEstoque
           and pedi_110.cd_it_pe_grupo    = f_scdGrupoEstoque
           and pedi_110.cd_it_pe_subgrupo = f_cd_it_pe_subgrupo
           and pedi_110.cd_it_pe_item     = f_cd_it_pe_item;
      exception
      when no_data_found then

         begin
            select nvl(max(pedi_110.seq_item_pedido),0) + 1
            into f_seq_item_aux
            from pedi_110
            where pedi_110.pedido_venda = f_icdPedidoVenda;
         exception
         when no_data_found then
            f_seq_item_aux := 1;
         end;

      end;

      if f_seq_item_aux is null
      then f_seq_item_aux := 0;
      end if;

      if f_seq_item_aux > 0
      then

         --Verificar se existe o registro
         v_encontrou_reg := 1;

         begin
            select 1
            into   v_encontrou_reg
            from pedi_110
            where pedi_110.pedido_venda    = f_icdPedidoVenda
              and pedi_110.seq_item_pedido = f_seq_item_aux;
         exception
         when no_data_found then
            v_encontrou_reg := 0;
         end;

         if v_encontrou_reg = 1
         then

            begin
               UPDATE pedi_110
               set pedi_110.qtde_pedida     = pedi_110.qtde_pedida + f_nqtPedida,
                   pedi_110.permite_atu_wms = 1
               where pedi_110.pedido_venda    = f_icdPedidoVenda
                 and pedi_110.seq_item_pedido = f_seq_item_aux;
            exception
            when others then
               raise_application_error(-20000, 'N�o atualizou a tabela PEDI_110.' || Chr(10) || SQLERRM);
            end;

            begin
               UPDATE pedi_110
               set pedi_110.permite_atu_wms   = 0
               where pedi_110.pedido_venda    = f_icdPedidoVenda
                 and pedi_110.seq_item_pedido = f_seq_item_aux;
            exception
            when others then
               raise_application_error(-20000, 'N�o atualizou a tabela PEDI_110.' || Chr(10) || SQLERRM);
            end;

         else

            begin

              INSERT INTO pedi_110
                (pedido_venda,                  seq_item_pedido,
                 cd_it_pe_nivel99,              cd_it_pe_grupo,
                 cd_it_pe_subgrupo,             cd_it_pe_item,
                 qtde_pedida,                   codigo_deposito,
                 lote_empenhado,                valor_unitario,
                 percentual_desc,               seq_principal,
                 codigo_embalagem,              cod_nat_op,
                 est_nat_op,                    acrescimo,
                 permite_atu_wms)
              VALUES
                (f_icdPedidoVenda,              f_seq_item_aux,
                 f_scdNivelEstoque,             f_scdGrupoEstoque,
                 f_cd_it_pe_subgrupo,           f_cd_it_pe_item,
                 f_nqtPedida,                   f_codigo_deposito,
                 f_lote_empenhado,              f_valor_unitario,
                 f_percentual_desc,             f_seq_principal,
                 f_codigo_embalagem,            f_cod_nat_op,
                 f_est_nat_op,                  f_acrescimo,
                 1);
            exception
            when others then
               raise_application_error(-20000, 'N�o inseriu na tabela PEDI_110.' || Chr(10) || SQLERRM);
            end;

            begin
               UPDATE pedi_110
               set pedi_110.permite_atu_wms   = 0
               where pedi_110.pedido_venda    = f_icdPedidoVenda
                 and pedi_110.seq_item_pedido = f_seq_item_aux;
            exception
            when others then
               raise_application_error(-20000, 'N�o atualizou a tabela PEDI_110.' || Chr(10) || SQLERRM);
            end;

         end if;

         begin
            select to_number(to_char(f_data_digit_venda, 'DD')) dia, to_number(to_char(f_data_digit_venda, 'MM')) mes, to_number(to_char(f_data_digit_venda, 'YYYY')) ano
            into   f_dia_venda,                                   f_mes_venda,                                   f_ano_venda
            from dual;
         exception
         when no_data_found then
            f_dia_venda := 0;

            f_mes_venda := 0;
            f_ano_venda := 0;
         end;

         /* ATUALIZA.PEDI_160 (VENDAS LIQUIDAS ACUMULADAS - POR CLIENTE) */

         --Verificar se existe o registro
         v_encontrou_reg := 1;

         begin
            select 1
            into   v_encontrou_reg
            from pedi_160
            where pedi_160.codigo_empresa  = f_codigo_empresa
              and pedi_160.mes_venda       = f_mes_venda
              and pedi_160.ano_venda       = f_ano_venda
              and pedi_160.ve_cli_cgc_cli9 = f_cli_ped_cgc_cli9
              and pedi_160.ve_cli_cgc_cli4 = f_cli_ped_cgc_cli4
              and pedi_160.ve_cli_cgc_cli2 = f_cli_ped_cgc_cli2
              and pedi_160.codigo_repr     = f_cod_rep_cliente
              and pedi_160.nivel_estrutura = f_cd_it_pe_nivel99
              and pedi_160.grupo_estrutura = f_cd_it_pe_grupo
              and rownum <= 1;
         exception
         when no_data_found then
            v_encontrou_reg := 0;
         end;

         if v_encontrou_reg = 1
         then

            begin

               UPDATE pedi_160
               set pedi_160.qtde_vendida  = pedi_160.qtde_vendida  - to_number(f_nqtPedida),
                   pedi_160.valor_vendido = pedi_160.valor_vendido - to_number((f_nqtPedida * f_valor_unitario))
               where pedi_160.codigo_empresa  = f_codigo_empresa
                 and pedi_160.mes_venda       = f_mes_venda
                 and pedi_160.ano_venda       = f_ano_venda
                 and pedi_160.ve_cli_cgc_cli9 = f_cli_ped_cgc_cli9
                 and pedi_160.ve_cli_cgc_cli4 = f_cli_ped_cgc_cli4
                 and pedi_160.ve_cli_cgc_cli2 = f_cli_ped_cgc_cli2
                 and pedi_160.codigo_repr     = f_cod_rep_cliente
                 and pedi_160.nivel_estrutura = f_cd_it_pe_nivel99
                 and pedi_160.grupo_estrutura = f_cd_it_pe_grupo;
            exception
            when others then
               raise_application_error(-20000, 'N�o atualizou a tabela PEDI_160.' || Chr(10) || SQLERRM);
            end;

         end if;

         --Verificar se existe o registro
         v_encontrou_reg := 1;

         begin
            select 1
            into   v_encontrou_reg
            from pedi_160
            where pedi_160.codigo_empresa  = f_codigo_empresa
              and pedi_160.mes_venda       = f_mes_venda
              and pedi_160.ano_venda       = f_ano_venda
              and pedi_160.ve_cli_cgc_cli9 = f_cli_ped_cgc_cli9
              and pedi_160.ve_cli_cgc_cli4 = f_cli_ped_cgc_cli4
              and pedi_160.ve_cli_cgc_cli2 = f_cli_ped_cgc_cli2
              and pedi_160.codigo_repr     = f_cod_rep_cliente
              and pedi_160.nivel_estrutura = f_scdNivelEstoque
              and pedi_160.grupo_estrutura = f_scdGrupoEstoque
              and rownum <= 1;
         exception
         when no_data_found then
            v_encontrou_reg := 0;
         end;

         if v_encontrou_reg = 0
         then

            begin
               INSERT INTO pedi_160
                  (codigo_empresa,               mes_venda,
                   ano_venda,                    ve_cli_cgc_cli9,
                   ve_cli_cgc_cli4,              ve_cli_cgc_cli2,
                   codigo_repr,                  nivel_estrutura,
                   grupo_estrutura,              qtde_vendida,
                   valor_vendido)
                VALUES
                  (f_codigo_empresa,             f_mes_venda,
                   f_ano_venda,                  f_cli_ped_cgc_cli9,
                   f_cli_ped_cgc_cli4,           f_cli_ped_cgc_cli2,
                   f_cod_rep_cliente,            f_scdNivelEstoque,
                   f_scdGrupoEstoque,            f_nqtPedida,
                   to_number(f_nqtPedida * f_valor_unitario));
            exception
            when others then
               raise_application_error(-20000, 'N�o inseriu na tabela PEDI_160.' || Chr(10) || SQLERRM);
            end;

         else

            begin
              UPDATE pedi_160
              set pedi_160.qtde_vendida  = pedi_160.qtde_vendida  + to_number(f_nqtPedida),
                  pedi_160.valor_vendido = pedi_160.valor_vendido + to_number((f_nqtPedida * f_valor_unitario))
              where pedi_160.codigo_empresa  = f_codigo_empresa
                and pedi_160.mes_venda       = f_mes_venda
                and pedi_160.ano_venda       = f_ano_venda
                and pedi_160.ve_cli_cgc_cli9 = f_cli_ped_cgc_cli9
                and pedi_160.ve_cli_cgc_cli4 = f_cli_ped_cgc_cli4
                and pedi_160.ve_cli_cgc_cli2 = f_cli_ped_cgc_cli2
                and pedi_160.codigo_repr     = f_cod_rep_cliente
                and pedi_160.nivel_estrutura = f_scdNivelEstoque
                and pedi_160.grupo_estrutura = f_scdGrupoEstoque;
            exception
            when others then
               raise_application_error(-20000, 'N�o atualizou a tabela PEDI_160.' || Chr(10) || SQLERRM);
            end;

         end if;

         /* ATUALIZA.PEDI_165 (VENDAS LIQUIDAS ACUMULADAS - POR PRODUTO) */

         --Verificar se existe o registro
         v_encontrou_reg := 1;

         begin
            select 1
            into   v_encontrou_reg
            from pedi_165
            where pedi_165.codigo_empresa  = f_codigo_empresa
              and pedi_165.mes_venda_prod  = f_mes_venda
              and pedi_165.ano_venda_prod  = f_ano_venda
              and pedi_165.prod_nivel99    = f_cd_it_pe_nivel99
              and pedi_165.prod_grupo      = f_cd_it_pe_grupo
              and pedi_165.prod_subgrupo   = f_cd_it_pe_subgrupo
              and pedi_165.prod_item       = f_cd_it_pe_item
              and rownum <= 1;
         exception
         when no_data_found then
            v_encontrou_reg := 0;
         end;

         if v_encontrou_reg = 1
         then

            begin
               UPDATE pedi_165
               set pedi_165.qtde_vendida  = pedi_165.qtde_vendida  - to_number(f_nqtPedida),
                   pedi_165.valor_vendido = pedi_165.valor_vendido - to_number((f_nqtPedida * f_valor_unitario))
               where pedi_165.codigo_empresa  = f_codigo_empresa
                 and pedi_165.mes_venda_prod  = f_mes_venda
                 and pedi_165.ano_venda_prod  = f_ano_venda
                 and pedi_165.prod_nivel99    = f_cd_it_pe_nivel99
                 and pedi_165.prod_grupo      = f_cd_it_pe_grupo
                 and pedi_165.prod_subgrupo   = f_cd_it_pe_subgrupo
                 and pedi_165.prod_item       = f_cd_it_pe_item;
            exception
            when others then
               raise_application_error(-20000, 'N�o atualizou a tabela PEDI_165.' || Chr(10) || SQLERRM);
            end;

         end if;

         --Verificar se existe o registro
         v_encontrou_reg := 1;

         begin
            select 1
            into   v_encontrou_reg
            from pedi_165
            where pedi_165.codigo_empresa  = f_codigo_empresa
              and pedi_165.mes_venda_prod  = f_mes_venda
              and pedi_165.ano_venda_prod  = f_ano_venda
              and pedi_165.prod_nivel99    = f_scdNivelEstoque
              and pedi_165.prod_grupo      = f_scdGrupoEstoque
              and pedi_165.prod_subgrupo   = f_cd_it_pe_subgrupo
              and pedi_165.prod_item       = f_cd_it_pe_item
              and rownum <= 1;
         exception
         when no_data_found then
            v_encontrou_reg := 0;
         end;

         if v_encontrou_reg = 0
         then

            begin
               INSERT INTO pedi_165
                  (codigo_empresa,               mes_venda_prod,
                   ano_venda_prod,               prod_nivel99,
                   prod_grupo,                   prod_subgrupo,
                   prod_item,                    qtde_vendida,
                   valor_vendido)
               VALUES
                  (f_codigo_empresa,             f_mes_venda,
                   f_ano_venda,                  f_scdNivelEstoque,
                   f_scdGrupoEstoque,            f_cd_it_pe_subgrupo,
                   f_cd_it_pe_item,              f_nqtPedida,
                   to_number((f_nqtPedida * f_valor_unitario)));
            exception
            when others then
               raise_application_error(-20000, 'N�o inseriu na tabela PEDI_165.' || Chr(10) || SQLERRM);
            end;

         else

            begin
              UPDATE pedi_165
              set pedi_165.qtde_vendida  = pedi_165.qtde_vendida  + to_number(f_nqtPedida),
                  pedi_165.valor_vendido = pedi_165.valor_vendido + to_number((f_nqtPedida * f_valor_unitario))
              where pedi_165.codigo_empresa  = f_codigo_empresa
                and pedi_165.mes_venda_prod  = f_mes_venda
                and pedi_165.ano_venda_prod  = f_ano_venda
                and pedi_165.prod_nivel99    = f_scdNivelEstoque
                and pedi_165.prod_grupo      = f_scdGrupoEstoque
                and pedi_165.prod_subgrupo   = f_cd_it_pe_subgrupo
                and pedi_165.prod_item       = f_cd_it_pe_item;
            exception
            when others then
               raise_application_error(-20000, 'N�o atualizou a tabela PEDI_165.' || Chr(10) || SQLERRM);
            end;

         end if;

      end if;

   end if;

end wms_pr_atu_item_pedido;
/

execute inter_pr_recompile;

/* versao: 1 */


 exit;

 exit;
