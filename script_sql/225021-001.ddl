alter table fatu_502 add estagio_liberacao number(2) default 0;

comment on column fatu_502.estagio_liberacao     is 'Parâmetro criado para substituir parâmetro global empr_001.estagio_libera';

update fatu_502 
set estagio_liberacao = (select estagio_libera from empr_001);
