CREATE OR REPLACE PROCEDURE inter_pr_gera_baixa_post_haus(p_programa in varchar2, p_usuario in varchar2, p_data_inicio in date,p_data_fim in date, p_saldo_a_pagar in number, p_desc_erro in out varchar2, p_flagAchaReg in out number) is

   v_codigo_historico_cont number;
   v_codigo_contabil_cc number;
   v_gera_contabil_cc number;

   v_codigo_historico_pgto number;
   v_cod_portador number;
   v_conta_corrente number;
   
   v_saldo number;
   v_saldo_a_pagar number;

   r_num_lcto number;
   r_des_erro varchar2(4000);
   r_flagAchaReg number;
   

BEGIN

   r_flagAchaReg := p_flagAchaReg;
   v_saldo_a_pagar := p_saldo_a_pagar;
  
   FOR adto in (
        select 
      fatu_070.codigo_empresa,                                          fatu_070.num_duplicata,
       fatu_070.seq_duplicatas,                                          fatu_070.tipo_titulo, 
       fatu_070.cli_dup_cgc_cli9,                                        fatu_070.cli_dup_cgc_cli4,
       fatu_070.cli_dup_cgc_cli2,                                        nvl(fatu_070.saldo_duplicata,0) saldo_titulos,
       fatu_070.data_prorrogacao,                                        fatu_070.codigo_contabil,
       fatu_070.cod_transacao
    from fatu_070, haus_001
    where fatu_070.saldo_duplicata > 0
    and fatu_070.cod_canc_duplic = 0
    and exists (select 1 from haus_003 where haus_003.pedido_gerado = fatu_070.pedido_venda and haus_003.pedido_gerado > 0)
    and fatu_070.codigo_empresa = haus_001.cod_empresa
    and fatu_070.cli_dup_cgc_cli9 = haus_001.cgc9
    and fatu_070.cli_dup_cgc_cli4 = haus_001.cgc4
    and fatu_070.cli_dup_cgc_cli2 = haus_001.cgc2
    and (fatu_070.data_emissao between P_DATA_INICIO and P_DATA_FIM or P_DATA_INICIO is null or P_DATA_FIM is null)
    order by fatu_070.data_prorrogacao
   )
   LOOP
        
        if v_saldo_a_pagar > 0
        then
                
                r_flagAchaReg :=1;
        
               BEGIN
                   select codigo_historico_pgto, cod_portador, conta_corrente
                   into v_codigo_historico_pgto, v_cod_portador, v_conta_corrente
                   from haus_001 h
                   where h.cod_empresa = adto.codigo_empresa
                   and h.cgc9 = adto.cli_dup_cgc_cli9
                   and h.cgc4 = adto.cli_dup_cgc_cli4
                   and h.cgc2 = adto.cli_dup_cgc_cli2;
               EXCEPTION
                 WHEN no_data_found THEN
                    v_conta_corrente :=0;
                    v_codigo_historico_pgto :=0;
                    v_cod_portador :=0;
               END;
          
               BEGIN
                  select c.codigo_contabil, c.gera_contabil
                    into v_codigo_contabil_cc, v_gera_contabil_cc
                    from cpag_020 c
                    where c.portador = v_cod_portador
                    and c.conta_corrente = v_conta_corrente;

                  EXCEPTION
                      WHEN no_data_found THEN
                          v_codigo_contabil_cc := 0;
                          v_gera_contabil_cc := 0;
               END;
         
               r_num_lcto := 0;
               r_des_erro := NULL;
               
               if v_gera_contabil_cc = 1 
               then      
                 inter_pr_contabilizacao_prg_02(adto.codigo_empresa,
                                    0, -- p_c_custo
                                    adto.cli_dup_cgc_cli9,
                                    adto.cli_dup_cgc_cli4,
                                    adto.cli_dup_cgc_cli2,
                                    
                                    adto.cod_transacao,
                                    
                                    v_codigo_historico_cont,
                                    v_conta_corrente,
                                               
                                    v_codigo_contabil_cc, --p_cod_contab_deb
                                    adto.codigo_contabil,       --p_cod_contab_cre
                                    
                                    adto.num_duplicata,
                                    to_date(sysdate),
                                    adto.saldo_titulos,
                                    4, --p_origem    baixa titulos,
                                    20, -- p_tp_cta_deb       IN NUMBER,
                                    1, --p_tp_cta_cre       IN NUMBER,
                                    p_programa,
                                    p_usuario,
                                    adto.num_duplicata, --|| '-' || adto.nome_cliente, alterar
                                    adto.tipo_titulo,
                                    r_num_lcto,
                                    r_des_erro);
                                    
                    if r_des_erro is not null then
                             rollback;
                             raise_application_error(-20001, r_des_erro || Chr(10) || SQLERRM);
                             
                    end if;
                end if;
            
            BEGIN  
            
            INTER_PR_INSERE_CPAG_026
            ( v_conta_corrente, v_cod_portador,  to_date(sysdate),
            'C', v_codigo_historico_pgto,  adto.saldo_titulos,
            2,  1,  'BAIXA OUTRAS RECEITAS', to_date(sysdate), r_num_lcto);
            
            EXCEPTION WHEN OTHERS THEN
              rollback;
              raise_application_error(-20001, r_des_erro || ' Erro ao inserir conta corrente - cpag_026' || Chr(10) || SQLERRM);
            END;
            
            if v_saldo_a_pagar < adto.saldo_titulos
            then
               v_saldo := v_saldo_a_pagar;
            else
               v_saldo := adto.saldo_titulos;
            end if;
            
            BEGIN
            INTER_PR_GERA_FATU_075(
             adto.cli_dup_cgc_cli9,
             adto.cli_dup_cgc_cli4,
             adto.cli_dup_cgc_cli2,
             adto.tipo_titulo, -- LIDO
             adto.num_duplicata, -- TITULO LIDO
             adto.codigo_empresa,
             adto.seq_duplicatas,  -- SEQUENCIA LIDA
             to_date(sysdate),
             v_codigo_historico_pgto,
             v_cod_portador,  
             v_conta_corrente,  
             to_date(sysdate),
             0, --p_pago_adiantamento
             0, -- adto.numero_adiantam, 
             r_num_lcto,
             v_saldo, -- VALOR DO SALDO LIDO
             0, --p_tipo_lanc_comissao
             r_des_erro);
         
             EXCEPTION WHEN OTHERS THEN
               rollback;
               raise_application_error(-20001, r_des_erro || ' Erro ao inserir conta corrente - cpag_026' || Chr(10) || SQLERRM);
            END;
            
            BEGIN
             update cpag_200 
             set valor_saldo = 0
             where tipo_adiantam = 2 
             and numero_adiantam = adto.num_duplicata;
            EXCEPTION WHEN OTHERS THEN
                rollback;
               raise_application_error(-20001, r_des_erro || ' Erro ao atualziar conta corrente - cpag_200' || Chr(10) || SQLERRM);
            END;
            
            if r_des_erro is null then
               v_saldo_a_pagar := v_saldo_a_pagar - v_saldo;
            end if;
            
       end if;
      
   END LOOP;
   
   p_desc_erro := r_des_erro;
   p_flagAchaReg := r_flagAchaReg;
   
   if r_des_erro is not null then
      rollback;
      raise_application_error(-20001, r_des_erro || Chr(10) || SQLERRM);
   else
      commit;
   end if;
   
  
END inter_pr_gera_baixa_post_haus;
/
