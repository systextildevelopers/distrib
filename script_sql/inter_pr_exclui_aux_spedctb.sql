CREATE OR REPLACE PROCEDURE INTER_PR_EXCLUI_AUX_SPEDCTB(p_cod_empresa         IN NUMBER,
                                                         p_exercicio           IN NUMBER,
                                                         p_des_erro           OUT varchar2) is
--
-- Finalidade: Eliminar as tabelas auxiliares geradas para o sped contabil
-- Autor.....:
-- Data......:
--
-- Historicos
--
-- Data    Autor    Observacoes
--

w_erro           EXCEPTION;

BEGIN
   p_des_erro          := NULL;

   -- elimina registros da tabela SPED_0000 (dados da empresa)
   begin
      delete sped_0000
      where sped_0000.cod_empresa         = p_cod_empresa
        and sped_0000.tip_contabil_fiscal = 'C';
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_0000 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;

   begin
      delete sped_ctb_0035
      where sped_ctb_0035.cod_empresa  =  p_cod_empresa;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_ctb_0035 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;
   commit;

   -- elimina registros da tabela SPED_0150 (paticipantes)
   begin
      delete sped_0150
      where sped_0150.cod_empresa         = p_cod_empresa
        and sped_0150.tip_contabil_fiscal = 'C';
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_0150 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;

   -- elimina registros da tabela SPED_CTB_I012 (termos de abertura dos livros)
   begin
      delete sped_ctb_i012
      where sped_ctb_i012.cod_empresa         = p_cod_empresa
        and sped_ctb_i012.exercicio           = p_exercicio;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_ctb_I012 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;

   -- elimina registros da tabela SPED_CTB_I050 (plano de contas)
   begin
      delete sped_ctb_i050
      where sped_ctb_i050.cod_empresa         = p_cod_empresa
        and sped_ctb_i050.exercicio           = p_exercicio;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_ctb_I050 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;

   -- elimina registros da tabela SPED_CTB_I155 (saldos por periodos)
   begin
      delete sped_ctb_i155
      where sped_ctb_i155.cod_empresa         = p_cod_empresa
        and sped_ctb_i155.exercicio           = p_exercicio;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_ctb_I155 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;

   -- elimina registros da tabela SPED_CTB_I200 (Capa dos lancamentos contabeis)
   begin
      delete sped_ctb_i200
      where sped_ctb_i200.cod_empresa         = p_cod_empresa
        and sped_ctb_i200.exercicio           = p_exercicio;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_ctb_I200 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;

   -- elimina registros da tabela SPED_CTB_I250 (Saldos das Contas de Resultado antes do zeramento)
   begin
      delete sped_ctb_i250
      where sped_ctb_i250.cod_empresa         = p_cod_empresa
        and sped_ctb_i250.exercicio           = p_exercicio;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_ctb_I250 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;

   -- elimina registros da tabela SPED_CTB_I355 (Saldos das Contas de Resultado antes do zeramento)
   begin
      delete sped_ctb_i355
      where sped_ctb_i355.cod_empresa         = p_cod_empresa
        and sped_ctb_i355.exercicio           = p_exercicio;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_ctb_I355 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;

   -- elimina registros da tabela SPED_CTB_I500 (Definicao do layout do Razao/diario de clientes e fornecedores)
   begin
      delete sped_ctb_i500
      where sped_ctb_i500.cod_empresa         = p_cod_empresa
        and sped_ctb_i500.exercicio           = p_exercicio;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_ctb_I500 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;

   -- elimina registros da tabela sped_CTB_I550 (Definicao do layout do Razao/diario de clientes e fornecedores)
   begin
      delete sped_ctb_i550
      where sped_ctb_i550.cod_empresa         = p_cod_empresa
        and sped_ctb_i550.exercicio           = p_exercicio
        and sped_ctb_i550.nome_programa       = 'cont_f001';
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_ctb_I550 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;

   -- elimina registros da tabela sped_CTB_J100 (Registros do Razao/diario de clientes e fornecedores)
   begin
      delete sped_ctb_j100
      where sped_ctb_j100.cod_empresa         = p_cod_empresa
        and sped_ctb_j100.exercicio           = p_exercicio;
   EXCEPTION
      WHEN OTHERS THEN
         p_des_erro := 'Erro na exclusao da tabela sped_ctb_j100 ' || Chr(10) || SQLERRM;
         RAISE W_ERRO;
   END;


   COMMIT;
EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_exclui_sped ' || Chr(10) || p_des_erro;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure p_exclu_sped ' || Chr(10) || SQLERRM;
END inter_pr_exclui_aux_spedctb; 
/

exec inter_pr_recompile;

