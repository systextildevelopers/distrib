create or replace trigger inter_tr_EMPR_007_log 
after insert or delete or update 
on EMPR_007 
for each row 
declare 
   ws_usuario_rede           varchar2(250) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ;
 
 
begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
 
 
   v_nome_programa :=  inter_fn_nome_programa(ws_sid); 
  

if inserting 
then 
    begin 
        insert into EMPR_007_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           PARAM_OLD,   /*8*/ 
           PARAM_NEW,   /*9*/ 
           TIPO_OLD,   /*10*/ 
           TIPO_NEW,   /*11*/ 
           LABEL_OLD,   /*12*/ 
           LABEL_NEW,   /*13*/ 
           FYI_MESSAGE_OLD,   /*14*/ 
           FYI_MESSAGE_NEW,   /*15*/ 
           DEFAULT_STR_OLD,   /*16*/ 
           DEFAULT_STR_NEW,   /*17*/ 
           DEFAULT_INT_OLD,   /*18*/ 
           DEFAULT_INT_NEW,   /*19*/ 
           DEFAULT_DBL_OLD,   /*20*/ 
           DEFAULT_DBL_NEW,   /*21*/ 
           DEFAULT_DAT_OLD,   /*22*/ 
           DEFAULT_DAT_NEW    /*23*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           '',/*8*/
           :new.PARAM, /*9*/   
           0,/*10*/
           :new.TIPO, /*11*/   
           '',/*12*/
           :new.LABEL, /*13*/   
           '',/*14*/
           :new.FYI_MESSAGE, /*15*/   
           '',/*16*/
           :new.DEFAULT_STR, /*17*/   
           0,/*18*/
           :new.DEFAULT_INT, /*19*/   
           0,/*20*/
           :new.DEFAULT_DBL, /*21*/   
           null,/*22*/
           :new.DEFAULT_DAT /*23*/   
         );    
    end;    
end if;    

if updating 
then 
    begin 
        insert into EMPR_007_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           PARAM_OLD, /*8*/   
           PARAM_NEW, /*9*/   
           TIPO_OLD, /*10*/   
           TIPO_NEW, /*11*/   
           LABEL_OLD, /*12*/   
           LABEL_NEW, /*13*/   
           FYI_MESSAGE_OLD, /*14*/   
           FYI_MESSAGE_NEW, /*15*/   
           DEFAULT_STR_OLD, /*16*/   
           DEFAULT_STR_NEW, /*17*/   
           DEFAULT_INT_OLD, /*18*/   
           DEFAULT_INT_NEW, /*19*/   
           DEFAULT_DBL_OLD, /*20*/   
           DEFAULT_DBL_NEW, /*21*/   
           DEFAULT_DAT_OLD, /*22*/   
           DEFAULT_DAT_NEW  /*23*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.PARAM,  /*8*/  
           :new.PARAM, /*9*/   
           :old.TIPO,  /*10*/  
           :new.TIPO, /*11*/   
           :old.LABEL,  /*12*/  
           :new.LABEL, /*13*/   
           :old.FYI_MESSAGE,  /*14*/  
           :new.FYI_MESSAGE, /*15*/   
           :old.DEFAULT_STR,  /*16*/  
           :new.DEFAULT_STR, /*17*/   
           :old.DEFAULT_INT,  /*18*/  
           :new.DEFAULT_INT, /*19*/   
           :old.DEFAULT_DBL,  /*20*/  
           :new.DEFAULT_DBL, /*21*/   
           :old.DEFAULT_DAT,  /*22*/  
           :new.DEFAULT_DAT  /*23*/  
         );    
    end;    
end if;    

if deleting 
then 
    begin 
        insert into EMPR_007_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           PARAM_OLD, /*8*/   
           PARAM_NEW, /*9*/   
           TIPO_OLD, /*10*/   
           TIPO_NEW, /*11*/   
           LABEL_OLD, /*12*/   
           LABEL_NEW, /*13*/   
           FYI_MESSAGE_OLD, /*14*/   
           FYI_MESSAGE_NEW, /*15*/   
           DEFAULT_STR_OLD, /*16*/   
           DEFAULT_STR_NEW, /*17*/   
           DEFAULT_INT_OLD, /*18*/   
           DEFAULT_INT_NEW, /*19*/   
           DEFAULT_DBL_OLD, /*20*/   
           DEFAULT_DBL_NEW, /*21*/   
           DEFAULT_DAT_OLD, /*22*/   
           DEFAULT_DAT_NEW /*23*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.PARAM, /*8*/   
           '', /*9*/
           :old.TIPO, /*10*/   
           0, /*11*/
           :old.LABEL, /*12*/   
           '', /*13*/
           :old.FYI_MESSAGE, /*14*/   
           '', /*15*/
           :old.DEFAULT_STR, /*16*/   
           '', /*17*/
           :old.DEFAULT_INT, /*18*/   
           0, /*19*/
           :old.DEFAULT_DBL, /*20*/   
           0, /*21*/
           :old.DEFAULT_DAT, /*22*/   
           null /*23*/
         );    
    end;    
end if;    
end inter_tr_EMPR_007_log;
