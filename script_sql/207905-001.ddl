CREATE TABLE supr_796
(
   motivo        number(3)  NOT NULL,
   desc_motivo   varchar(60) DEFAULT '',
   ativo_inativo number(1)  DEFAULT 0
);

COMMENT ON COLUMN supr_796.ativo_inativo
	IS '0 ativo e 1 inativo';

/

exec inter_pr_recompile;
