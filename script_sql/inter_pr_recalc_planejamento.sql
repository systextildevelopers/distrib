create or replace procedure inter_pr_recalc_planejamento(
   periodo_inicial    in integer,
   periodo_final      in integer,
   opcao_recalculo    in integer,
   recalc_referencias in integer)
IS
--      raiseMarcio 10092018 Teste 19_09 -atz
   v_existe_registro    integer;
   consiste_emp         varchar2(1);
   v_tipo_produto       integer;
   v_calculo_nec_tecel  integer;
   zerou_qtde           boolean;
   atualizar_areceber   boolean;
   v_deposito_conserto  number := 0;
   qtde                 number := 0.000;
   saldo_quilos         NUMBER := 0.000;
   saldo_rolos          NUMBER := 0.000;
   peso_rolo_ordem      NUMBER := 0.000;
   seq_pcpf_310         integer;
   alternativa_item     integer;
   periodo_compras      integer;
   data_req             date;


   saldo_quilos_acab    NUMBER := 0.000;
   saldo_rolos_acab     NUMBER := 0.000;
   saldo_plan           NUMBER := 0.00;
   atz_saldo            NUMBER := 0.000;

   qtde_programada    pcpc_040.qtde_pecas_prog%type;
   qtde_2a_ant        pcpc_040.qtde_pecas_2a%type;
   qtde_perdas_ant    pcpc_040.qtde_perdas%type;
   qtde_conserto_ant  pcpc_040.qtde_conserto%type;

   ordem_tingimento   pcpb_010.ordem_tingimento%type;
   relacao_banho_part pcpb_010.relacao_banho%type;
   relacao_banho_empr_001 empr_001.relacao_banho%type;

   qtde_cons pcpc_040.qtde_conserto%type;
   qtde_em_producao pcpc_040.qtde_pecas_prod%type;
   qtde_pecas_prod  pcpc_040.qtde_pecas_prod%type;

   ws_usuario_rede                       varchar2(20);
   ws_maquina_rede                       varchar2(40);
   ws_aplicativo                         varchar2(20);
   ws_sid                                number(9);
   ws_empresa                            number(3);
   tem_tmrp_650                          number(3);
   ws_usuario_systextil                  varchar2(250);
   ws_locale_usuario                     varchar2(5);

   atu_estagios_seguintes                number(3);
  
   v_qtde_reg_pedi_100                   number(9);
   v_qtde_reg_pcpc_040                   number(9);
   v_qtde_reg_pcpb_030                   number(9);
   v_qtde_reg_pcpt_010                   number(9);
   v_qtde_reg_pcpf_310                   number(9);
   v_qtde_reg_supr_100                   number(9);
   v_tem_reg_400                         number;
   obplanej_empr_002                     integer;
   qtde_saldo_kg                         number(13,3);
   codigoEmpresaAnt                      number;
   
   v_qtde_atualizar           pcpt_025.qtde_kg_final%type;
   v_saldo_qtde_kg_final      pcpt_025.qtde_kg_final%type;

PROCEDURE deleteTmrp655  
IS
   v_tem_tmrp_655       number;
BEGIN
   if recalc_referencias = 0
   then
      begin
         select nvl(count(1),0)
         into v_tem_tmrp_655
         from tmrp_655
         where tmrp_655.status = 1;
      end;
      
      if v_tem_tmrp_655 > 0
      then
         begin
            delete from tmrp_655
            where tmrp_655.status = 1;
         end;
      end if;
   end if;
end deleteTmrp655;

PROCEDURE temTmrp655 (
   PArea_producao           in number,
   POrdem_producao          in number,
   PNivel                   in varchar2,
   PGrupo                   in varchar2
) IS
   v_tem_tmrp_655       number;
BEGIN
   if recalc_referencias = 0
   then
      begin
         select nvl(count(*),0)
         into v_tem_tmrp_655
         from tmrp_655
         where tmrp_655.status           = 0
           and ((tmrp_655.area_producao  = PArea_producao
           and   tmrp_655.ordem_producao = POrdem_producao)
           or   (tmrp_655.nivel          = PNivel
           and   tmrp_655.grupo          = PGrupo));
      end;
   
      if v_tem_tmrp_655 > 0
      then
         begin
            update tmrp_655
            set   tmrp_655.status           = 1
            where tmrp_655.status           = 0
              and ((tmrp_655.area_producao  = PArea_producao
              and   tmrp_655.ordem_producao = POrdem_producao)
              or   (tmrp_655.nivel          = PNivel
              and   tmrp_655.grupo          = PGrupo));
         end;
      end if;
   end if;
END temTmrp655;

BEGIN

   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   begin
      select fatu_502.atu_estagios_seguintes
      into atu_estagios_seguintes
      from fatu_502
      where fatu_502.codigo_empresa = ws_empresa;
   exception when others then
      inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26374', 'fatu_502' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#',1);
--      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'fatu_502' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
   end;

   begin
      select 1
      into tem_tmrp_650
      from tmrp_650;
   exception when others then
      tem_tmrp_650 := 0;
   end;

   zerou_qtde := true;
   
   if recalc_referencias = 1
   then
      for reg_tmrp_041 in (select periodo_producao 
                           from tmrp_041
                           where tmrp_041.periodo_producao between periodo_inicial and periodo_final
                           group by periodo_producao)
      loop
         begin
            delete from tmrp_041
            where tmrp_041.periodo_producao = reg_tmrp_041.periodo_producao;
         exception when others then
            zerou_qtde := false;
            inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds22943', sqlerrm , '' , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil||'#'),1);
         end;
         
         COMMIT;
      end loop;
      
      COMMIT;
      
   else
      for reg_tmrp_655 in (select tmrp_655.nivel,         tmrp_655.grupo,
                                  tmrp_655.area_producao, tmrp_655.ordem_producao
                           from tmrp_655
                           where tmrp_655.status = 0)
      loop
         if reg_tmrp_655.ordem_producao > 0
         then
            begin
               delete from tmrp_041
               where tmrp_041.periodo_producao between periodo_inicial and periodo_final
                 and tmrp_041.nr_pedido_ordem = reg_tmrp_655.ordem_producao
                 and tmrp_041.area_producao   = reg_tmrp_655.area_producao;
            exception when others then
                 zerou_qtde := false;
            end;
         else
             for reg_tmrp_041 in (select tmrp_041.nr_pedido_ordem, tmrp_041.area_producao,
                                         tmrp_041.seq_pedido_ordem
                                  from tmrp_041
                                  where tmrp_041.periodo_producao between periodo_inicial and periodo_final
                                    and tmrp_041.nivel_estrutura = reg_tmrp_655.nivel
                                    and tmrp_041.grupo_estrutura = reg_tmrp_655.grupo
                                  group by tmrp_041.nr_pedido_ordem,
                                           tmrp_041.area_producao,
                                           tmrp_041.seq_pedido_ordem)
             loop
                begin
                  delete from tmrp_041
                  where tmrp_041.periodo_producao between periodo_inicial and periodo_final
                    and tmrp_041.area_producao    = reg_tmrp_041.area_producao
                    and tmrp_041.nr_pedido_ordem  = reg_tmrp_041.nr_pedido_ordem
                    and tmrp_041.seq_pedido_ordem = reg_tmrp_041.seq_pedido_ordem ;
                exception when others then
                   zerou_qtde := false;
                end;
             end loop;
         end if;
      end loop;
   end if;
   
   COMMIT;
   
   if zerou_qtde = true
   then
      v_qtde_reg_pedi_100 := 0;

      for reg_pedi_100 in (select pedi_100.pedido_venda,  pedi_100.num_periodo_prod,
                                  pedi_110.cd_it_pe_nivel99,   pedi_110.cd_it_pe_grupo,
                                  pedi_110.cd_it_pe_subgrupo,  pedi_110.cd_it_pe_item,
                                  pedi_110.qtde_pedida,        pedi_110.seq_item_pedido
                           from pcpc_010, pedi_110, pedi_100
                           where recalc_referencias            = 1
                             
                             and  pcpc_010.area_periodo        = pedi_100.tecido_peca
                             and  pcpc_010.periodo_producao    = pedi_100.num_periodo_prod
                             and  pcpc_010.situacao_periodo    < 3
                             
                             and  pedi_110.pedido_venda        = pedi_100.pedido_venda
                             and  pedi_110.cod_cancelamento    = 0
                             and (pedi_110.situacao_fatu_it   <> 1 or pedi_110.qtde_afaturar > 0.000)
                             and  pedi_100.num_periodo_prod    between periodo_inicial and periodo_final
                             and  pedi_100.num_periodo_prod    > 0
                             and  pedi_100.cod_cancelamento    = 0
                             and  pedi_100.situacao_venda not in (10)
                           UNION ALL
                           select pedi_100.pedido_venda,  pedi_100.num_periodo_prod,
                                  pedi_110.cd_it_pe_nivel99,   pedi_110.cd_it_pe_grupo,
                                  pedi_110.cd_it_pe_subgrupo,  pedi_110.cd_it_pe_item,
                                  pedi_110.qtde_pedida,        pedi_110.seq_item_pedido
                           from pcpc_010, pedi_110, pedi_100
                           where recalc_referencias            = 0
                           
                             and exists (select 1 from tmrp_655
                                         where ((tmrp_655.area_producao  = 6
                                           and   tmrp_655.ordem_producao = pedi_100.pedido_venda)
                                            or  (tmrp_655.nivel          = pedi_110.cd_it_pe_nivel99
                                           and   tmrp_655.grupo          = pedi_110.cd_it_pe_grupo)))
                             
                             and  pcpc_010.area_periodo        = pedi_100.tecido_peca
                             and  pcpc_010.periodo_producao    = pedi_100.num_periodo_prod
                             and  pcpc_010.situacao_periodo    < 3
                             
                             and  pedi_110.pedido_venda        = pedi_100.pedido_venda
                             and  pedi_110.cod_cancelamento    = 0
                             and (pedi_110.situacao_fatu_it   <> 1 or pedi_110.qtde_afaturar > 0.000)
                             and  pedi_100.num_periodo_prod    between periodo_inicial and periodo_final
                             and  pedi_100.num_periodo_prod    > 0
                             and  pedi_100.cod_cancelamento    = 0
                             and  pedi_100.situacao_venda not in (10)
                          )
      loop
         v_qtde_reg_pedi_100 := v_qtde_reg_pedi_100 + 1;

         if tem_tmrp_650 = 1
         then
            inter_pr_find_data_requerida(0, 0, 7, 1, data_req);
         else
            data_req := null;
         end if;
         
         temTmrp655 (6, reg_pedi_100.pedido_venda, reg_pedi_100.cd_it_pe_nivel99, reg_pedi_100.cd_it_pe_nivel99);
         
         begin
            insert into tmrp_041
                   (periodo_producao,               area_producao,
                    nr_pedido_ordem,                nivel_estrutura,
                    grupo_estrutura,                subgru_estrutura,
                    item_estrutura,                 qtde_planejada,
                    seq_pedido_ordem)
            values (reg_pedi_100.num_periodo_prod,  6,
                    reg_pedi_100.pedido_venda,      reg_pedi_100.cd_it_pe_nivel99,
                    reg_pedi_100.cd_it_pe_grupo,    reg_pedi_100.cd_it_pe_subgrupo,
                    reg_pedi_100.cd_it_pe_item,     reg_pedi_100.qtde_pedida,
                    reg_pedi_100.seq_item_pedido);
         exception when others then
            data_req := null;
         end;
         
         if v_qtde_reg_pedi_100 = 250
         then
            COMMIT;
            v_qtde_reg_pedi_100 := 0;
         end if;
      end loop; --FIM reg_pedi_100
      
      COMMIT;

      begin
         /* DEP?SITO CONSERTO */
         select nvl(fatu_501.deposito_conserto,0)
         into   v_deposito_conserto
         from fatu_501
         where fatu_501.codigo_empresa = ws_empresa;
      exception when others then
         v_deposito_conserto := 0;
      end;
      
      v_qtde_reg_pcpc_040 :=0;

      v_tem_reg_400 := nvl(inter_fn_executou_recalculo(),0);
      
      for reg_pcpc_040 in (select pcpc_020.ordem_producao,    pcpc_020.codigo_risco,
                                  pcpc_020.ultimo_estagio,    pcpc_020.alternativa_peca,
                                  pcpc_020.ordem_origem,      pcpc_020.periodo_producao as periodo_producao_020,
                                  pcpc_040.qtde_pecas_prog,   pcpc_040.qtde_pecas_prod,
                                  pcpc_040.qtde_conserto,     pcpc_040.qtde_perdas,
                                  pcpc_040.qtde_pecas_2a,     pcpc_040.proconf_nivel99,
                                  pcpc_040.proconf_grupo,     pcpc_040.proconf_subgrupo,
                                  pcpc_040.proconf_item,      pcpc_040.codigo_estagio,
                                  pcpc_040.ordem_confeccao,   pcpc_040.estagio_anterior,
                                  pcpc_040.estagio_depende,   pcpc_040.periodo_producao  as periodo_producao_040,
                                  pcpc_040.qtde_a_produzir_pacote
                           from pcpc_040, pcpc_020, pcpc_010
                           where recalc_referencias         = 1
                             and pcpc_040.ordem_producao    = pcpc_020.ordem_producao
                             
                             and pcpc_020.periodo_producao  = pcpc_010.periodo_producao
                             and pcpc_020.cod_cancelamento  = 0
                             
                             and pcpc_010.area_periodo      = 1
                             and pcpc_010.periodo_producao  between periodo_inicial and periodo_final
                             and pcpc_010.situacao_periodo  < 3
                           UNION ALL                             
                           select pcpc_020.ordem_producao,    pcpc_020.codigo_risco,
                                  pcpc_020.ultimo_estagio,    pcpc_020.alternativa_peca,
                                  pcpc_020.ordem_origem,      pcpc_020.periodo_producao as periodo_producao_020,
                                  pcpc_040.qtde_pecas_prog,   pcpc_040.qtde_pecas_prod,
                                  pcpc_040.qtde_conserto,     pcpc_040.qtde_perdas,
                                  pcpc_040.qtde_pecas_2a,     pcpc_040.proconf_nivel99,
                                  pcpc_040.proconf_grupo,     pcpc_040.proconf_subgrupo,
                                  pcpc_040.proconf_item,      pcpc_040.codigo_estagio,
                                  pcpc_040.ordem_confeccao,   pcpc_040.estagio_anterior,
                                  pcpc_040.estagio_depende,   pcpc_040.periodo_producao  as periodo_producao_040,
                                  pcpc_040.qtde_a_produzir_pacote
                           from pcpc_040, pcpc_020, pcpc_010
                           where recalc_referencias         = 0
                             
                             and exists (select 1 from tmrp_655
                                         where ((tmrp_655.area_producao  = 1
                                           and   tmrp_655.ordem_producao = pcpc_040.ordem_producao)
                                           or   (tmrp_655.nivel          = pcpc_040.proconf_nivel99
                                           and   tmrp_655.grupo          = pcpc_040.proconf_grupo)))
                             
                             and pcpc_040.ordem_producao    = pcpc_020.ordem_producao
                             
                             and pcpc_020.periodo_producao  = pcpc_010.periodo_producao
                             and pcpc_020.cod_cancelamento  = 0
                             
                             and pcpc_010.area_periodo      = 1
                             and pcpc_010.periodo_producao  between periodo_inicial and periodo_final
                             and pcpc_010.situacao_periodo  < 3
                          )
      loop
         if  opcao_recalculo <> 2
         and reg_pcpc_040.codigo_estagio = reg_pcpc_040.ultimo_estagio
         then
            qtde_programada := reg_pcpc_040.qtde_pecas_prog;

            if reg_pcpc_040.estagio_anterior > 0
            then
               if reg_pcpc_040.estagio_depende > 0
               then
                  begin
                     select nvl(sum(pcpc_040.qtde_pecas_2a),0),  nvl(sum(pcpc_040.qtde_perdas),0),
                            nvl(sum(pcpc_040.qtde_conserto),0)
                     into   qtde_2a_ant,                         qtde_perdas_ant,
                            qtde_conserto_ant
                     from pcpc_040
                     where pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao_040
                       and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                       and pcpc_040.codigo_estagio   = reg_pcpc_040.estagio_depende;
                  exception when others then
                     qtde_2a_ant       := 0;
                     qtde_perdas_ant   := 0;
                     qtde_conserto_ant := 0;
                  end;

                  if qtde_conserto_ant > 0 and v_deposito_conserto > 0
                  then
                     qtde_conserto_ant := qtde_conserto_ant;
                  else
                     qtde_conserto_ant := 0;
                  end if;

                  if atu_estagios_seguintes = 0
                  then qtde_programada := qtde_programada - (qtde_2a_ant + qtde_perdas_ant + qtde_conserto_ant);
                  else qtde_programada := qtde_programada - (qtde_conserto_ant);
                  end if;
               else -- else estagio depende
                  begin
                     select nvl(sum(pcpc_040.qtde_pecas_2a),0),  nvl(sum(pcpc_040.qtde_perdas),0),
                            nvl(sum(pcpc_040.qtde_conserto),0)
                     into   qtde_2a_ant,                         qtde_perdas_ant,
                            qtde_conserto_ant
                     from pcpc_040
                     where pcpc_040.periodo_producao = reg_pcpc_040.periodo_producao_040
                       and pcpc_040.ordem_confeccao  = reg_pcpc_040.ordem_confeccao
                       and pcpc_040.codigo_estagio   = reg_pcpc_040.estagio_anterior;
                  exception when others then
                     qtde_2a_ant       := 0;
                     qtde_perdas_ant   := 0;
                     qtde_conserto_ant := 0;
                  end;
                  
                  if qtde_conserto_ant > 0 and v_deposito_conserto > 0
                  then
                     qtde_conserto_ant := qtde_conserto_ant;
                  else
                     qtde_conserto_ant := 0;
                  end if;

                  if atu_estagios_seguintes = 0
                  then qtde_programada := qtde_programada - (qtde_2a_ant + qtde_perdas_ant + qtde_conserto_ant);
                  else qtde_programada := qtde_programada - (qtde_conserto_ant);
                  end if;
               end if; -- fim if estagio depende > 0
            else
               qtde_programada := reg_pcpc_040.qtde_pecas_prog;
            end if; -- FIM IF ESTAGIO ANTERIOR

            qtde_cons       := reg_pcpc_040.qtde_conserto;

            if  v_deposito_conserto = 0
            and reg_pcpc_040.qtde_conserto > 0
            then
               qtde_cons := 0.00;
            end if;

            qtde_em_producao:= qtde_programada - (reg_pcpc_040.qtde_pecas_prod +
                                                  reg_pcpc_040.qtde_pecas_2a +
                                                  reg_pcpc_040.qtde_perdas +
                                                  qtde_cons);
            
            if tem_tmrp_650 = 1
            then
               inter_pr_find_data_requerida(reg_pcpc_040.ordem_producao,
                                            reg_pcpc_040.ultimo_estagio,
                                            1,
                                            5,
                                            data_req);
            else
               data_req := null;
            end if;

            temTmrp655 (1, reg_pcpc_040.ordem_producao, reg_pcpc_040.proconf_nivel99, reg_pcpc_040.proconf_grupo);

            v_qtde_reg_pcpc_040 := v_qtde_reg_pcpc_040 + 1;

            if v_tem_reg_400 <> 0
            then
               qtde_em_producao := reg_pcpc_040.qtde_a_produzir_pacote;
            end if;

            if (qtde_em_producao <= 0)
            then
              continue;
            end if;

            begin
               insert into tmrp_041 (
                  periodo_producao,                    area_producao,
                  nr_pedido_ordem,                     seq_pedido_ordem,
                  codigo_estagio,                      nivel_estrutura,
                  grupo_estrutura,                     subgru_estrutura,
                  item_estrutura,                      qtde_reservada,
                  qtde_areceber,                       consumo,
                  data_requirida
               ) values (
                  reg_pcpc_040.periodo_producao_020,   1,
                  reg_pcpc_040.ordem_producao,         reg_pcpc_040.ordem_confeccao,
                  reg_pcpc_040.ultimo_estagio,         reg_pcpc_040.proconf_nivel99,
                  reg_pcpc_040.proconf_grupo,          reg_pcpc_040.proconf_subgrupo,
                  reg_pcpc_040.proconf_item,           0.000,
                  qtde_em_producao,                    0.000,
                  data_req
               );
            exception when others then
               data_req := null;
            end;
         end if; -- if  opcao_recalculo <> 2 and reg_pcpc_040.codigo_estagio = reg_pcpc_040.ultimo_estagio

         qtde_pecas_prod := reg_pcpc_040.qtde_pecas_prod;

         if  v_deposito_conserto <> 0
         and reg_pcpc_040.codigo_estagio = reg_pcpc_040.ultimo_estagio
         then
            qtde_pecas_prod := qtde_pecas_prod + qtde_cons;
         end if;

         qtde_pecas_prod := qtde_pecas_prod + reg_pcpc_040.qtde_pecas_2a + reg_pcpc_040.qtde_perdas;

         qtde            := reg_pcpc_040.qtde_pecas_prog - qtde_pecas_prod;

         if v_tem_reg_400 <> 0
         then
            qtde := reg_pcpc_040.qtde_a_produzir_pacote;
         end if;

         if opcao_recalculo <> 1
         and qtde > 0
         then
            inter_pr_explode_estrutura(reg_pcpc_040.proconf_nivel99,  reg_pcpc_040.proconf_grupo,
                                       reg_pcpc_040.proconf_subgrupo, reg_pcpc_040.proconf_item,
                                       reg_pcpc_040.alternativa_peca, reg_pcpc_040.periodo_producao_020,
                                       qtde,                          reg_pcpc_040.ordem_producao,
                                       reg_pcpc_040.codigo_risco,     1,
                                       reg_pcpc_040.ordem_confeccao,  reg_pcpc_040.codigo_estagio,
                                       reg_pcpc_040.codigo_estagio,
                                       0.00,                          qtde,
                                       'pcpc',                        0);

         end if;

         if v_qtde_reg_pcpc_040 = 400
         then
            COMMIT;
            v_qtde_reg_pcpc_040 := 0;
         end if;

      end loop;
      
      COMMIT;
      
      
      -- Se o rolo for confirmado, atualiza a reserva do planejamento diminuindo da reserva a quantidade confirmada
      for reg_pcpt_025 in (select pcpt_025.area_ordem,         pcpt_025.ordem_producao,
                                  pcpt_025.pano_fin_nivel99,   pcpt_025.pano_fin_grupo,
                                  pcpt_025.pano_fin_subgrupo,  pcpt_025.pano_fin_item,
                                  pcpt_025.qtde_kg_final
                           from pcpt_025, pcpc_020, pcpc_010
                           where recalc_referencias         = 1
                             and pcpt_025.area_ordem        = 1
                             and pcpt_025.ordem_producao    = pcpc_020.ordem_producao
                             and pcpt_025.rolo_confirmado   = 'S'
                             and exists (select 1 from tmrp_041
                                         where tmrp_041.area_producao    = pcpt_025.area_ordem
                                           and tmrp_041.nr_pedido_ordem  = pcpt_025.ordem_producao
                                           and tmrp_041.nivel_estrutura  = pcpt_025.pano_fin_nivel99
                                           and tmrp_041.grupo_estrutura  = pcpt_025.pano_fin_grupo
                                           and tmrp_041.subgru_estrutura = pcpt_025.pano_fin_subgrupo
                                           and tmrp_041.item_estrutura   = pcpt_025.pano_fin_item)
                             and pcpc_020.cod_cancelamento  = 0
                             and pcpc_020.periodo_producao  between periodo_inicial and periodo_final
                             and pcpc_010.area_periodo      = 1
                             and pcpc_010.periodo_producao  = pcpc_020.periodo_producao
                             and pcpc_010.situacao_periodo  < 3
                           UNION ALL
                           select pcpt_025.area_ordem,         pcpt_025.ordem_producao,
                                  pcpt_025.pano_fin_nivel99,   pcpt_025.pano_fin_grupo,
                                  pcpt_025.pano_fin_subgrupo,  pcpt_025.pano_fin_item,
                                  pcpt_025.qtde_kg_final
                           from pcpt_025, pcpc_020, pcpc_010                           
                           where (recalc_referencias         = 0
                             and   exists (select 1 from tmrp_655
                                           where ((tmrp_655.area_producao  = 1
                                             and   tmrp_655.ordem_producao = pcpc_020.ordem_producao)
                                             or   (tmrp_655.nivel          = '1'
                                             and   tmrp_655.grupo          = pcpc_020.referencia_peca))))
                             and pcpc_020.cod_cancelamento  = 0
                             and pcpc_020.periodo_producao  between periodo_inicial and periodo_final
                             and pcpc_010.area_periodo      = 1
                             and pcpc_010.periodo_producao  = pcpc_020.periodo_producao
                             and pcpc_010.situacao_periodo  < 3
                          )
      loop
         v_saldo_qtde_kg_final := reg_pcpt_025.qtde_kg_final;
         
         for reg_tmrp_041 in (select tmrp_041.periodo_producao,   tmrp_041.area_producao,
                                     tmrp_041.nr_pedido_ordem,    tmrp_041.seq_pedido_ordem,
                                     tmrp_041.codigo_estagio,     tmrp_041.nivel_estrutura,
                                     tmrp_041.grupo_estrutura,    tmrp_041.subgru_estrutura,
                                     tmrp_041.item_estrutura,     tmrp_041.sequencia_estrutura,
                                     tmrp_041.qtde_reservada
                              from tmrp_041
                              where tmrp_041.area_producao    = reg_pcpt_025.area_ordem
                                and tmrp_041.nr_pedido_ordem  = reg_pcpt_025.ordem_producao
                                and tmrp_041.nivel_estrutura  = reg_pcpt_025.pano_fin_nivel99
                                and tmrp_041.grupo_estrutura  = reg_pcpt_025.pano_fin_grupo
                                and tmrp_041.subgru_estrutura = reg_pcpt_025.pano_fin_subgrupo
                                and tmrp_041.item_estrutura   = reg_pcpt_025.pano_fin_item
                             )
         loop
            if v_saldo_qtde_kg_final > 0.000000
            then
               if v_saldo_qtde_kg_final > reg_tmrp_041.qtde_reservada
               then
                  v_qtde_atualizar      := reg_tmrp_041.qtde_reservada;
                  v_saldo_qtde_kg_final := v_saldo_qtde_kg_final - v_qtde_atualizar;
               else
                  v_qtde_atualizar      := v_saldo_qtde_kg_final;
                  v_saldo_qtde_kg_final := v_saldo_qtde_kg_final - v_qtde_atualizar;
               end if;
               
               begin
                  update tmrp_041
                  set tmrp_041.qtde_reservada = tmrp_041.qtde_reservada - v_qtde_atualizar
                  where tmrp_041.periodo_producao    = reg_tmrp_041.periodo_producao
                    and tmrp_041.area_producao       = reg_tmrp_041.area_producao
                    and tmrp_041.nr_pedido_ordem     = reg_tmrp_041.nr_pedido_ordem
                    and tmrp_041.seq_pedido_ordem    = reg_tmrp_041.seq_pedido_ordem
                    and tmrp_041.codigo_estagio      = reg_tmrp_041.codigo_estagio
                    and tmrp_041.nivel_estrutura     = reg_tmrp_041.nivel_estrutura
                    and tmrp_041.grupo_estrutura     = reg_tmrp_041.grupo_estrutura
                    and tmrp_041.subgru_estrutura    = reg_tmrp_041.subgru_estrutura
                    and tmrp_041.item_estrutura      = reg_tmrp_041.item_estrutura
                    and tmrp_041.sequencia_estrutura = reg_tmrp_041.sequencia_estrutura;
               --exception
               --when NO_DATA_FOUND then
               --   raise_application_error(-20000,'ATEN??O! N?o atualizou reserva TMRP_041 (2). Verifique.');
               end;
            end if;
         end loop;
      end loop;
      
      
   end if; --END if zerou_qtde = true

   begin
      select empr_001.relacao_banho
      into relacao_banho_empr_001
      from empr_001;
   exception when others then
      relacao_banho_empr_001 := 0.000;
   end;

   begin
      select empr_002.ob_finalizada_planej
      into obplanej_empr_002
      from empr_002;
   exception when others then
      obplanej_empr_002 := 0;
   end;

   v_qtde_reg_pcpb_030 := 0;
   
   for reg_pcpb_030 in (select pcpbBenefic.periodo_producao,      pcpbBenefic.ordem_producao,
                               pcpbBenefic.nivel_estrutura,       pcpbBenefic.relacao_banho,
                               pcpbBenefic.data_programa,         pcpbBenefic.situacao_ordem,
                               pcpbBenefic.pano_nivel99,          pcpbBenefic.pano_grupo,
                               pcpbBenefic.pano_subgrupo,         pcpbBenefic.pano_item,
                               pcpbBenefic.alternativa_item,
                               pcpbBenefic.sequencia,             pcpbBenefic.codigo_acomp,
                               pcpbBenefic.lote_acomp,            pcpbBenefic.codigo_embalagem,
                               pcpbBenefic.qtde_quilos_prog,      pcpbBenefic.qtde_quilos_prod
                        from (select pcpb_010.periodo_producao,      pcpb_010.ordem_producao,
                                     pcpb_010.nivel_estrutura,       pcpb_010.relacao_banho,
                                     pcpb_010.data_programa,         pcpb_010.situacao_ordem,
                                     pcpb_030.pano_nivel99,          pcpb_030.pano_grupo,
                                     pcpb_030.pano_subgrupo,         pcpb_030.pano_item,
                                     pcpb_020.alternativa_item,
                                     pcpb_020.sequencia,             pcpb_020.codigo_acomp,
                                     pcpb_020.lote_acomp,            pcpb_030.codigo_embalagem,
                                     sum(pcpb_030.qtde_quilos_prog) qtde_quilos_prog,
                                     sum(pcpb_030.qtde_quilos_produzido) qtde_quilos_prod
                              from pcpb_030, pcpb_020, pcpb_010, pcpc_010
                              where recalc_referencias         = 1
                                
                                and ((obplanej_empr_002 = 0 and
                                      not exists (select 1 
                                                  from pcpt_020
                                                  where pcpt_020.area_producao  = 2
                                                    and pcpt_020.ordem_producao = pcpb_010.ordem_producao)
                                     )
                                 or  (obplanej_empr_002  = 1 and
                                      exists (select 1 from pcpb_040
                                              where pcpb_040.ordem_producao = pcpb_010.ordem_producao
                                                and pcpb_040.data_termino  is null)
                                     )
                                    )
                                
                                and pcpb_030.ordem_producao    = pcpb_020.ordem_producao
                                and pcpb_030.pano_nivel99      = pcpb_020.pano_sbg_nivel99
                                and pcpb_030.pano_grupo        = pcpb_020.pano_sbg_grupo
                                and pcpb_030.pano_subgrupo     = pcpb_020.pano_sbg_subgrupo
                                and pcpb_030.pano_item         = pcpb_020.pano_sbg_item
                                and pcpb_030.sequencia         = pcpb_020.sequencia
                                
                                and pcpb_020.ordem_producao    = pcpb_010.ordem_producao
                                and pcpb_020.cod_cancelamento  = 0
                                
                                and pcpb_010.periodo_producao  between periodo_inicial and periodo_final
                                and pcpb_010.cod_cancelamento  = 0
                                and pcpb_010.tipo_ordem   not in (4)
                                and ((pcpb_010.nivel_estrutura  = '7' and
                                      not exists (select 1 
                                                  from estq_060
                                                  where estq_060.lote = pcpb_010.ordem_producao)
                                     )
                                     or
                                     ((pcpb_010.nivel_estrutura = '2' or pcpb_010.nivel_estrutura = '9') and
                                       not exists (select 1 from pcpb_035
                                                   where pcpb_035.pb35pb20_ordpro20 = pcpb_010.ordem_producao)
                                     )
                                    )
                                
                                and pcpc_010.area_periodo      = decode(pcpb_010.nivel_estrutura,'2',2,7)
                                and pcpc_010.periodo_producao  = pcpb_010.periodo_producao
                                and pcpc_010.situacao_periodo  < 3
                                
                              group by pcpb_010.periodo_producao,      pcpb_010.ordem_producao,
                                       pcpb_010.nivel_estrutura,       pcpb_010.relacao_banho,
                                       pcpb_010.data_programa,         pcpb_010.situacao_ordem,
                                       pcpb_030.pano_nivel99,          pcpb_030.pano_grupo,
                                       pcpb_030.pano_subgrupo,         pcpb_030.pano_item,
                                       pcpb_020.alternativa_item,      pcpb_020.sequencia,
                                       pcpb_020.codigo_acomp,          pcpb_020.lote_acomp,
                                       pcpb_030.codigo_embalagem
                              having (obplanej_empr_002 = 0
                                  or (obplanej_empr_002 = 1
                                  and sum(pcpb_030.qtde_quilos_prog) > sum(pcpb_030.qtde_quilos_produzido)))
                              UNION ALL
                              select pcpb_010.periodo_producao,      pcpb_010.ordem_producao,
                                     pcpb_010.nivel_estrutura,       pcpb_010.relacao_banho,
                                     pcpb_010.data_programa,         pcpb_010.situacao_ordem,
                                     pcpb_030.pano_nivel99,          pcpb_030.pano_grupo,
                                     pcpb_030.pano_subgrupo,         pcpb_030.pano_item,
                                     pcpb_020.alternativa_item,
                                     pcpb_020.sequencia,             pcpb_020.codigo_acomp,
                                     pcpb_020.lote_acomp,            pcpb_030.codigo_embalagem,
                                     sum(pcpb_030.qtde_quilos_prog) qtde_quilos_prog,
                                     sum(pcpb_030.qtde_quilos_produzido) qtde_quilos_prod
                              from pcpb_030, pcpb_020, pcpc_010, pcpb_010
                              where recalc_referencias         = 0
                                and exists (select 1 from tmrp_655
                                            where ((tmrp_655.area_producao  = 2
                                              and  tmrp_655.ordem_producao = pcpb_010.ordem_producao)
                                               or (tmrp_655.nivel          = pcpb_020.pano_sbg_nivel99
                                              and  tmrp_655.grupo          = pcpb_020.pano_sbg_grupo)))
                                
                                and ((obplanej_empr_002 = 0 and
                                      not exists (select 1 
                                                  from pcpt_020
                                                  where pcpt_020.area_producao  = 2
                                                    and pcpt_020.ordem_producao = pcpb_010.ordem_producao)
                                     )
                                 or  (obplanej_empr_002  = 1 and
                                      exists (select 1 from pcpb_040
                                              where pcpb_040.ordem_producao = pcpb_010.ordem_producao
                                                and pcpb_040.data_termino  is null)
                                     )
                                    )
                                
                                and pcpb_030.ordem_producao    = pcpb_020.ordem_producao
                                and pcpb_030.pano_nivel99      = pcpb_020.pano_sbg_nivel99
                                and pcpb_030.pano_grupo        = pcpb_020.pano_sbg_grupo
                                and pcpb_030.pano_subgrupo     = pcpb_020.pano_sbg_subgrupo
                                and pcpb_030.pano_item         = pcpb_020.pano_sbg_item
                                and pcpb_030.sequencia         = pcpb_020.sequencia
                                 
                                and pcpb_020.ordem_producao    = pcpb_010.ordem_producao
                                and pcpb_020.cod_cancelamento  = 0
                                
                                and pcpb_010.periodo_producao  between periodo_inicial and periodo_final
                                and pcpb_010.cod_cancelamento  = 0
                                and pcpb_010.tipo_ordem   not in (4)
                                and ((pcpb_010.nivel_estrutura  = '7' and
                                      not exists (select 1 
                                                  from estq_060
                                                  where estq_060.lote = pcpb_010.ordem_producao)
                                     )
                                     or
                                     ((pcpb_010.nivel_estrutura = '2' or pcpb_010.nivel_estrutura = '9') and
                                       not exists (select 1 from pcpb_035
                                                   where pcpb_035.pb35pb20_ordpro20 = pcpb_010.ordem_producao)
                                     )
                                    )
                                
                                and pcpc_010.area_periodo      = decode(pcpb_010.nivel_estrutura,'2',2,7)
                                and pcpc_010.periodo_producao  = pcpb_010.periodo_producao
                                and pcpc_010.situacao_periodo  < 3
                                
                              group by pcpb_010.periodo_producao,      pcpb_010.ordem_producao,
                                       pcpb_010.nivel_estrutura,       pcpb_010.relacao_banho,
                                       pcpb_010.data_programa,         pcpb_010.situacao_ordem,
                                       pcpb_030.pano_nivel99,          pcpb_030.pano_grupo,
                                       pcpb_030.pano_subgrupo,         pcpb_030.pano_item,
                                       pcpb_020.alternativa_item,      pcpb_020.sequencia,
                                       pcpb_020.codigo_acomp,          pcpb_020.lote_acomp,
                                       pcpb_030.codigo_embalagem
                              having (obplanej_empr_002 = 0
                                  or (obplanej_empr_002 = 1
                                  and sum(pcpb_030.qtde_quilos_prog) > sum(pcpb_030.qtde_quilos_produzido)))
                            ) pcpbBenefic
                        order by pcpbBenefic.periodo_producao,      pcpbBenefic.ordem_producao,
                                 pcpbBenefic.nivel_estrutura,       pcpbBenefic.relacao_banho,
                                 pcpbBenefic.data_programa,         pcpbBenefic.situacao_ordem,
                                 pcpbBenefic.pano_nivel99,          pcpbBenefic.pano_grupo,
                                 pcpbBenefic.pano_subgrupo,         pcpbBenefic.pano_item,
                                 pcpbBenefic.alternativa_item,      pcpbBenefic.sequencia,
                                 pcpbBenefic.codigo_acomp,          pcpbBenefic.lote_acomp,
                                 pcpbBenefic.codigo_embalagem
                       )
   loop
      qtde_saldo_kg := reg_pcpb_030.qtde_quilos_prog - reg_pcpb_030.qtde_quilos_prod;

      if (qtde_saldo_kg <= 0)
      then
        continue;
      end if;

      begin
         select pcpb_010.ordem_tingimento
         into   ordem_tingimento
         from pcpb_010
         where pcpb_010.ordem_producao = reg_pcpb_030.ordem_producao;
      exception when others then
         ordem_tingimento := 0;
      end;

      relacao_banho_part := reg_pcpb_030.relacao_banho;

      if relacao_banho_part = 0.00
      then
         relacao_banho_part := relacao_banho_empr_001;
      end if;

      if relacao_banho_part = 0.00
      then
         relacao_banho_part := 1.00;
      end if;

      if opcao_recalculo = 1
      or opcao_recalculo = 3
      then
         atualizar_areceber := true;

         if obplanej_empr_002 = 0
         then
            begin
               select nvl(count(1),0)
               into v_existe_registro
               from pcpb_040
               where pcpb_040.ordem_producao = reg_pcpb_030.ordem_producao
                 and pcpb_040.data_termino   is null;
            exception when others then
               v_existe_registro  := 0;
               atualizar_areceber := false;
            end;
            
            if v_existe_registro = 0
            then
              atualizar_areceber := false;
            end if;
         end if;

         if atualizar_areceber
         then
            begin
               select nvl(count(1),0)
               into v_existe_registro
               from tmrp_041
               where tmrp_041.periodo_producao  = reg_pcpb_030.periodo_producao
                 and tmrp_041.area_producao     = 2
                 and tmrp_041.nr_pedido_ordem   = reg_pcpb_030.ordem_producao
                 and tmrp_041.seq_pedido_ordem  = reg_pcpb_030.sequencia
                 and tmrp_041.codigo_estagio    = 0
                 and tmrp_041.nivel_estrutura   = reg_pcpb_030.pano_nivel99
                 and tmrp_041.grupo_estrutura   = reg_pcpb_030.pano_grupo
                 and tmrp_041.subgru_estrutura  = reg_pcpb_030.pano_subgrupo
                 and tmrp_041.item_estrutura    = reg_pcpb_030.pano_item;
            exception when others then
               v_existe_registro  := 0;
               atualizar_areceber := false;
            end;

            if v_existe_registro > 0
            then
               atualizar_areceber := false;
            end if;
         end if;

         if atualizar_areceber
         then
            if tem_tmrp_650 = 1
            then
               inter_pr_find_data_requerida(reg_pcpb_030.ordem_producao,
                                            0,
                                            2, /* Beneficiamento */
                                            1, /* M?quina */
                                            data_req);
            else
               data_req := null;
            end if;
            
            temTmrp655 (2, reg_pcpb_030.ordem_producao, reg_pcpb_030.pano_nivel99, reg_pcpb_030.pano_subgrupo);

            begin
               insert into tmrp_041 (
                  periodo_producao,               area_producao,
                  nr_pedido_ordem,                seq_pedido_ordem,
                  codigo_estagio,                 nivel_estrutura,
                  grupo_estrutura,                subgru_estrutura,
                  item_estrutura,                 qtde_reservada,
                  qtde_areceber,                  consumo,
                  data_requirida
               ) values (
                   reg_pcpb_030.periodo_producao,  2,
                   reg_pcpb_030.ordem_producao,    reg_pcpb_030.sequencia,
                   0,                              reg_pcpb_030.pano_nivel99,
                   reg_pcpb_030.pano_grupo,        reg_pcpb_030.pano_subgrupo,
                   reg_pcpb_030.pano_item,         0.000,
                   qtde_saldo_kg,                  0.000,
                   data_req
               );
            exception when others then
--               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
               inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#',1);
            end;
         end if; -- FIM if atualizar_areceber = true
      end if; -- FIM if opcao_recalculo = 1 or opcao_recalculo = 3

      if opcao_recalculo = 2
      or opcao_recalculo = 3
      then
         for reg_pcpb_040 in (select pcpb_040.codigo_estagio
                              from pcpb_040
                              where pcpb_040.ordem_producao = reg_pcpb_030.ordem_producao
                                and pcpb_040.data_termino  is null
                              group by pcpb_040.codigo_estagio
                              order by pcpb_040.codigo_estagio)
         loop
            inter_pr_explode_estrutura(reg_pcpb_030.pano_nivel99,     reg_pcpb_030.pano_grupo,
                                       reg_pcpb_030.pano_subgrupo,    reg_pcpb_030.pano_item,
                                       reg_pcpb_030.alternativa_item, reg_pcpb_030.periodo_producao,
                                       qtde_saldo_kg,                 reg_pcpb_030.ordem_producao,
                                       0,                             2,
                                       reg_pcpb_030.sequencia,        reg_pcpb_040.codigo_estagio,
                                       reg_pcpb_040.codigo_estagio,
                                       0.00,                          reg_pcpb_030.codigo_embalagem,
                                       'pcpb',                         0);
         end loop;

         begin
            select nvl(count(1),0)
            into v_existe_registro
            from pcpb_010
            where pcpb_010.ordem_producao = reg_pcpb_030.ordem_producao
              and not exists (select 1
                              from pcpb_040
                              where pcpb_040.ordem_producao = pcpb_010.ordem_producao);
         exception when others then
            v_existe_registro := 0;
         end;

         if v_existe_registro > 0
         then
            for reg_basi_050 in (select basi_050.estagio
                                 from  basi_050
                                 where  basi_050.nivel_item       = reg_pcpb_030.pano_nivel99
                                   and  basi_050.grupo_item       = reg_pcpb_030.pano_grupo
                                   and (basi_050.sub_item         = reg_pcpb_030.pano_subgrupo  or basi_050.sub_item  = '000')
                                   and (basi_050.item_item        = reg_pcpb_030.pano_item or basi_050.item_item = '000000')
                                   and  basi_050.alternativa_item =  reg_pcpb_030.alternativa_item
                                 group by basi_050.estagio
                                 order by basi_050.estagio)
            loop
               inter_pr_explode_estrutura(reg_pcpb_030.pano_nivel99,     reg_pcpb_030.pano_grupo,
                                          reg_pcpb_030.pano_subgrupo,    reg_pcpb_030.pano_item,
                                          reg_pcpb_030.alternativa_item, reg_pcpb_030.periodo_producao,
                                          qtde_saldo_kg,                 reg_pcpb_030.ordem_producao,
                                          0,                             2,
                                          reg_pcpb_030.sequencia,        reg_basi_050.estagio,
                                          reg_basi_050.estagio,
                                          0.00,                          reg_pcpb_030.codigo_embalagem,
                                          'pcpb', 0);
            end loop;
         end if;

         v_qtde_reg_pcpb_030 := v_qtde_reg_pcpb_030 + 1;

         if v_qtde_reg_pcpb_030 = 200
         then
            COMMIT;
            v_qtde_reg_pcpb_030 := 0;
         end if;

      end if; --FIM if opcao_recalculo = 2 or opcao_recalculo = 3
   end loop;--FIM LOOP REG_PCPB_030
   
   COMMIT;
   
   begin
      select empr_002.calculo_nec_tecel
      into   v_calculo_nec_tecel
      from empr_002;
   exception when others then
      v_calculo_nec_tecel := 0;
   end;

   v_qtde_reg_pcpt_010 := 0;
   
   for reg_pcpt_010 in (select pcpt_010.ordem_tecelagem,   pcpt_010.qtde_rolos_prog,
                               pcpt_010.qtde_rolos_prod,   pcpt_010.cd_pano_nivel99,
                               pcpt_010.cd_pano_grupo,     pcpt_010.cd_pano_subgrupo,
                               pcpt_010.cd_pano_item,      pcpt_010.alternativa_item,
                               pcpt_010.peso_rolo,         pcpt_010.periodo_producao,
                               pcpt_010.data_digitacao,    pcpt_010.nivel_acab,
                               pcpt_010.grupo_acab,        pcpt_010.sub_acab,
                               pcpt_010.item_acab,         pcpt_010.quilos_prog_acab,
                               pcpt_010.quilos_prod_acab,  pcpt_010.rolos_prod_acab,
                               pcpt_010.rolos_prog_acab,   pcpt_010.qtde_quilos_prog,
                               pcpt_010.qtde_quilos_prod
                        from pcpt_010, pcpc_010
                        where recalc_referencias          = 1 
                          and pcpt_010.cd_pano_nivel99    in ('2','4')
                          and (pcpt_010.qtde_rolos_prog   > pcpt_010.qtde_rolos_prod
                           or  pcpt_010.rolos_prog_acab   > pcpt_010.rolos_prod_acab)
                          and  pcpt_010.cod_cancelamento  = 0
                          and  pcpt_010.periodo_producao  between periodo_inicial and periodo_final
                          and  pcpc_010.area_periodo      = 4
                          and  pcpc_010.periodo_producao  = pcpt_010.periodo_producao
                          and  pcpc_010.situacao_periodo  < 3
                        UNION ALL
                        select pcpt_010.ordem_tecelagem,   pcpt_010.qtde_rolos_prog,
                               pcpt_010.qtde_rolos_prod,   pcpt_010.cd_pano_nivel99,
                               pcpt_010.cd_pano_grupo,     pcpt_010.cd_pano_subgrupo,
                               pcpt_010.cd_pano_item,      pcpt_010.alternativa_item,
                               pcpt_010.peso_rolo,         pcpt_010.periodo_producao,
                               pcpt_010.data_digitacao,    pcpt_010.nivel_acab,
                               pcpt_010.grupo_acab,        pcpt_010.sub_acab,
                               pcpt_010.item_acab,         pcpt_010.quilos_prog_acab,
                               pcpt_010.quilos_prod_acab,  pcpt_010.rolos_prod_acab,
                               pcpt_010.rolos_prog_acab,   pcpt_010.qtde_quilos_prog,
                               pcpt_010.qtde_quilos_prod
                        from pcpt_010, pcpc_010
                        where ((recalc_referencias         = 0
                          and exists (select 1 from tmrp_655
                                      where (tmrp_655.area_producao  = 4
                                        and  tmrp_655.ordem_producao = pcpt_010.ordem_tecelagem
                                         or (tmrp_655.nivel          = pcpt_010.cd_pano_nivel99
                                        and  tmrp_655.grupo          = pcpt_010.cd_pano_grupo))))) 
                          and pcpt_010.cd_pano_nivel99  in ('2','4')
                          and (pcpt_010.qtde_rolos_prog   > pcpt_010.qtde_rolos_prod
                           or  pcpt_010.rolos_prog_acab   > pcpt_010.rolos_prod_acab)
                          and  pcpt_010.cod_cancelamento  = 0
                          and  pcpt_010.periodo_producao  between periodo_inicial and periodo_final
                          and  pcpc_010.area_periodo      = 4
                          and  pcpc_010.periodo_producao  = pcpt_010.periodo_producao
                          and  pcpc_010.situacao_periodo  < 3)
   loop
      begin
         select basi_030.tipo_produto
         into   v_tipo_produto
         from basi_030
         where basi_030.nivel_estrutura = reg_pcpt_010.cd_pano_nivel99
           and basi_030.referencia      = reg_pcpt_010.cd_pano_grupo;
      exception when others then
         v_tipo_produto := 0;
      end;

      if reg_pcpt_010.qtde_rolos_prog > reg_pcpt_010.qtde_rolos_prod
      then
         saldo_quilos     := 0.000000;

         saldo_rolos      := reg_pcpt_010.qtde_rolos_prog  - reg_pcpt_010.qtde_rolos_prod;
         peso_rolo_ordem  := reg_pcpt_010.qtde_quilos_prog / reg_pcpt_010.qtde_rolos_prog;


         if v_calculo_nec_tecel = 0
         then
            if v_tipo_produto = 3 or v_tipo_produto = 4 or v_tipo_produto = 6
            then
              saldo_quilos := saldo_rolos * (reg_pcpt_010.qtde_quilos_prog / reg_pcpt_010.qtde_rolos_prog);
            else
              saldo_quilos := saldo_rolos * peso_rolo_ordem;
            end if;
         else
            saldo_quilos := reg_pcpt_010.qtde_quilos_prog - reg_pcpt_010.qtde_quilos_prod;
         end if;
         
         temTmrp655 (4, reg_pcpt_010.ordem_tecelagem, reg_pcpt_010.cd_pano_nivel99, reg_pcpt_010.cd_pano_grupo);

         if (saldo_quilos <= 0)
         then
           continue;
         end if;

         if opcao_recalculo = 1
         or opcao_recalculo = 3
         then
            begin
               insert into tmrp_041 (
                  periodo_producao,              area_producao,
                  nr_pedido_ordem,               seq_pedido_ordem,
                  codigo_estagio,                nivel_estrutura,
                  grupo_estrutura,               subgru_estrutura,
                  item_estrutura,                qtde_reservada,
                  qtde_areceber,                 consumo
               ) values (
                  reg_pcpt_010.periodo_producao, 4,
                  reg_pcpt_010.ordem_tecelagem,  0,
                  0,                             reg_pcpt_010.cd_pano_nivel99,
                  reg_pcpt_010.cd_pano_grupo,    reg_pcpt_010.cd_pano_subgrupo,
                  reg_pcpt_010.cd_pano_item,     0.000,
                  saldo_quilos,                  0.000
              );
            exception when others then
--                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
                  inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#',1);
            end;
         end if;--fim if opcao_recalculo = 1 or opcao_recalculo = 3

         if opcao_recalculo = 2
         or opcao_recalculo = 3
         then
            inter_pr_explode_estrutura(reg_pcpt_010.cd_pano_nivel99,  reg_pcpt_010.cd_pano_grupo,
                                       reg_pcpt_010.cd_pano_subgrupo, reg_pcpt_010.cd_pano_item,
                                       reg_pcpt_010.alternativa_item, reg_pcpt_010.periodo_producao,
                                       saldo_quilos,                  reg_pcpt_010.ordem_tecelagem,
                                       0,                             4,
                                       0,                             0,
                                       0,
                                       0.00,                          0,
                                       'pcpt', 0);
         end if;
      end if; -- FIM if reg_pcpt_010.qtde_rolos_prog > reg_pcpt_010.qtde_rolos_prod

      if reg_pcpt_010.rolos_prog_acab > reg_pcpt_010.rolos_prod_acab
      then
         saldo_quilos_acab  := 0.000000;

         saldo_rolos_acab   := reg_pcpt_010.rolos_prog_acab  - reg_pcpt_010.rolos_prod_acab;
         peso_rolo_ordem    := reg_pcpt_010.quilos_prog_acab / reg_pcpt_010.rolos_prog_acab;

         if v_calculo_nec_tecel = 0
         then
            if v_tipo_produto = 3 or v_tipo_produto = 4 or v_tipo_produto = 6
            then
              saldo_quilos_acab := saldo_rolos_acab * (reg_pcpt_010.quilos_prog_acab / reg_pcpt_010.rolos_prog_acab);
            else
              saldo_quilos_acab := saldo_rolos_acab * peso_rolo_ordem;
            end if;
         else
            saldo_quilos_acab := reg_pcpt_010.quilos_prog_acab - reg_pcpt_010.quilos_prod_acab;
         end if;

         if (saldo_quilos_acab <= 0)
         then
           continue;
         end if;
         
         temTmrp655 (4, reg_pcpt_010.ordem_tecelagem, reg_pcpt_010.cd_pano_nivel99, reg_pcpt_010.cd_pano_grupo);

         if opcao_recalculo = 1
         or opcao_recalculo = 3
         then
            begin
               insert into tmrp_041 (
                  periodo_producao,      area_producao,
                  nr_pedido_ordem,       seq_pedido_ordem,
                  codigo_estagio,        nivel_estrutura,
                  grupo_estrutura,       subgru_estrutura,
                  item_estrutura,        qtde_reservada,
                  qtde_areceber,         consumo
                ) values (
                  reg_pcpt_010.periodo_producao,  4,
                  reg_pcpt_010.ordem_tecelagem,   0,
                  0,                              reg_pcpt_010.nivel_acab,
                  reg_pcpt_010.grupo_acab,        reg_pcpt_010.sub_acab,
                  reg_pcpt_010.item_acab,         0.000,
                  saldo_quilos_acab,             0.000);
            exception when others then
--               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
               inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#',1);
            end;
         end if; -- FIM if opcao_recalculo = 1 or opcao_recalculo = 3

         if opcao_recalculo = 2
         or opcao_recalculo = 3
         then
            inter_pr_explode_estrutura(reg_pcpt_010.nivel_acab,       reg_pcpt_010.grupo_acab,
                                       reg_pcpt_010.sub_acab,         reg_pcpt_010.item_acab,
                                       reg_pcpt_010.alternativa_item, reg_pcpt_010.periodo_producao,
                                       saldo_quilos_acab,             reg_pcpt_010.ordem_tecelagem,
                                       0,                             4,
                                       0,                             0,0,
                                       0.00,                          0,
                                       'pcpt', 0);
         end if; -- fim if opcao_recalculo = 2 or opcao_recalculo = 3
      end if; -- FIM if reg_pcpt_010.rolos_prog_acab > reg_pcpt_010.rolos_prod_acab

      v_qtde_reg_pcpt_010 := v_qtde_reg_pcpt_010 + 1;
      if v_qtde_reg_pcpt_010 = 200
      then
         COMMIT;
         v_qtde_reg_pcpt_010 := 0;
      end if;
   end loop; -- FIM REG_PCPT_010
   
   COMMIT;
   
   begin
      select nvl(pcpf_310.sequencia_plano,0)
      into seq_pcpf_310
      from pcpf_300,pcpf_310,pcpc_010
      where pcpf_300.periodo_producao  = pcpf_310.periodo_producao
        and pcpf_300.sequencia_plano   = pcpf_310.sequencia_plano
        and pcpf_300.linha_produto     = pcpf_310.linha_produto
        and pcpf_300.codigo_estagio    = pcpf_310.codigo_estagio
        and pcpf_300.grupo_maquina     = pcpf_310.grupo_maquina
        and pcpf_300.subgrupo_maquina  = pcpf_310.subgrupo_maquina
        and pcpf_310.qtde_planejada    > pcpf_310.qtde_produzida
        and pcpf_310.periodo_producao  = pcpc_010.periodo_producao
        and pcpc_010.area_periodo      = 7
        and pcpc_010.periodo_producao between periodo_inicial and periodo_final
        and pcpc_010.situacao_periodo  < 3
        and rownum                     = 1  -- print
      order by nvl(pcpf_300.data_aprovacao,'01-jan-80') DESC,
               pcpf_300.sequencia_plano DESC;
   exception when others then
      seq_pcpf_310 := 0;
   end;

   v_qtde_reg_pcpf_310 := 0;
   
   for reg_pcpf_310 in (select pcpf_310.nivel_estrutura,   pcpf_310.grupo_estrutura,
                               pcpf_310.subgru_estrutura,  pcpf_310.item_estrutura,
                               pcpf_310.qtde_planejada,    pcpf_310.qtde_produzida,
                               pcpf_310.periodo_producao,  pcpf_310.sequencia_plano
                        from pcpf_310, pcpc_010
                        where recalc_referencias         = 1
                          and pcpf_310.qtde_planejada    > pcpf_310.qtde_produzida
                          and pcpf_310.periodo_producao  between periodo_inicial and periodo_final
                          and pcpc_010.area_periodo      = 7
                          and pcpc_010.periodo_producao  = pcpf_310.periodo_producao
                          and pcpc_010.situacao_periodo  < 3
                          and (pcpf_310.sequencia_plano  = seq_pcpf_310
                          or   seq_pcpf_310              = 0)
                        UNION ALL
                        select pcpf_310.nivel_estrutura,   pcpf_310.grupo_estrutura,
                               pcpf_310.subgru_estrutura,  pcpf_310.item_estrutura,
                               pcpf_310.qtde_planejada,    pcpf_310.qtde_produzida,
                               pcpf_310.periodo_producao,  pcpf_310.sequencia_plano
                        from pcpf_310, pcpc_010
                        where ((recalc_referencias         = 0
                          and   exists (select 1 from tmrp_655
                                        where (tmrp_655.area_producao  = 7
                                          and  tmrp_655.ordem_producao = seq_pcpf_310)
                                          or  (tmrp_655.nivel          = pcpf_310.nivel_estrutura
                                          and  tmrp_655.grupo          = pcpf_310.grupo_estrutura))))
                          and pcpf_310.qtde_planejada    > pcpf_310.qtde_produzida
                          and pcpf_310.periodo_producao  between periodo_inicial and periodo_final
                          and pcpc_010.area_periodo      = 7
                          and pcpc_010.periodo_producao  = pcpf_310.periodo_producao
                          and pcpc_010.situacao_periodo  < 3
                          and (pcpf_310.sequencia_plano  = seq_pcpf_310
                          or   seq_pcpf_310              = 0))
   loop
      saldo_plan := 0.000000;
      saldo_plan := reg_pcpf_310.qtde_planejada - reg_pcpf_310.qtde_produzida;

      if opcao_recalculo = 1
      or opcao_recalculo = 3
      then
         begin
            select nvl(count(1),0)
            into v_existe_registro
            from tmrp_041
            where tmrp_041.periodo_producao  = reg_pcpf_310.periodo_producao
              and tmrp_041.area_producao     = 7
              and tmrp_041.nr_pedido_ordem   = reg_pcpf_310.sequencia_plano
              and tmrp_041.seq_pedido_ordem  = 0
              and tmrp_041.codigo_estagio    = 0
              and tmrp_041.nivel_estrutura   = reg_pcpf_310.nivel_estrutura
              and tmrp_041.grupo_estrutura   = reg_pcpf_310.grupo_estrutura
              and tmrp_041.subgru_estrutura  = reg_pcpf_310.subgru_estrutura
              and tmrp_041.item_estrutura    =  reg_pcpf_310.item_estrutura;
         exception when others then
            v_existe_registro  := 0;
         end;

         if v_existe_registro = 0
         then
            if tem_tmrp_650 = 1
            then
               inter_pr_find_data_requerida(reg_pcpf_310.sequencia_plano,
                                            0,
                                            7, /* Fia??o */
                                            1, /* M?quina */
                                            data_req);
            else
               data_req := null;
            end if;
            
            temTmrp655 (7, reg_pcpf_310.sequencia_plano, reg_pcpf_310.nivel_estrutura, reg_pcpf_310.grupo_estrutura);
            
            begin
               insert into tmrp_041 (
                  periodo_producao,              area_producao,
                  nr_pedido_ordem,               seq_pedido_ordem,
                  codigo_estagio,                nivel_estrutura,
                  grupo_estrutura,               subgru_estrutura,
                  item_estrutura,                qtde_reservada,
                  qtde_areceber,                 consumo,
                  data_requirida
               ) values (
                  reg_pcpf_310.periodo_producao, 7, /* VER COM GIBA, POIS NA MINASA, VIA AUTOMASOFT GRAVA COM 2 */
                  reg_pcpf_310.sequencia_plano,  0,
                  0,                             reg_pcpf_310.nivel_estrutura,
                  reg_pcpf_310.grupo_estrutura,  reg_pcpf_310.subgru_estrutura,
                  reg_pcpf_310.item_estrutura,   0.000,
                  saldo_plan,                    0.000,
                  data_req
               );
            exception when others then
--               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
               inter_pr_relatorio_recalculo(inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#',1);
            end;
         end if;
      end if; -- fim if opcao_recalculo = 1 or opcao_recalculo = 3

      if opcao_recalculo = 2
      or opcao_recalculo = 3
      then
         begin
            select basi_010.numero_alternati
            into   alternativa_item
            from basi_010
            where basi_010.nivel_estrutura  = reg_pcpf_310.nivel_estrutura
              and basi_010.grupo_estrutura  = reg_pcpf_310.grupo_estrutura
              and basi_010.subgru_estrutura = reg_pcpf_310.subgru_estrutura
              and basi_010.item_estrutura   = reg_pcpf_310.item_estrutura;
         exception when others then
            alternativa_item := 1;
         end;

         inter_pr_explode_estrutura(reg_pcpf_310.nivel_estrutura,  reg_pcpf_310.grupo_estrutura,
                                    reg_pcpf_310.subgru_estrutura, reg_pcpf_310.item_estrutura,
                                    alternativa_item,              reg_pcpf_310.periodo_producao,
                                    saldo_plan,                    seq_pcpf_310,
                                    0,                             7,
                                    0,                             0,0,
                                    0.00,                          0,
                                    'pcpc', 0);
      end if; -- if opcao_recalculo = 2 or opcao_recalculo = 3

      v_qtde_reg_pcpf_310 := v_qtde_reg_pcpf_310 + 1;
      if v_qtde_reg_pcpf_310 = 200
      then
         COMMIT;
         v_qtde_reg_pcpf_310 := 0;
      end if;

   end loop; -- fim loop reg_pcpt_310
   
   COMMIT;
   
   if opcao_recalculo = 1
   or opcao_recalculo = 3
   then
      v_qtde_reg_supr_100 := 0;
      codigoEmpresaAnt := -1;
         
      for reg_supr_100 in (select supr100.qtde_saldo_item, supr100.num_ped_compra,
                                  supr100.seq_item_pedido, supr100.situacao_item,
                                  supr100.data_prev_entr,  supr100.item_100_nivel99,
                                  supr100.item_100_grupo,  supr100.item_100_subgrupo,
                                  supr100.item_100_item,   supr100.codigo_empresa,
                                  supr100.periodo_compras
                           from (select supr_100.qtde_saldo_item, supr_100.num_ped_compra,
                                        supr_100.seq_item_pedido, supr_100.situacao_item,
                                        supr_100.data_prev_entr,  supr_100.item_100_nivel99,
                                        supr_100.item_100_grupo,  supr_100.item_100_subgrupo,
                                        supr_100.item_100_item,   supr_090.codigo_empresa,
                                        supr_100.periodo_compras
                                 from supr_100, supr_090
                                 where recalc_referencias             = 1
                                   and supr_090.pedido_compra         = supr_100.num_ped_compra
                                   and supr_090.cod_cancelamento      = 0                                
                                   and supr_090.ordem_servico         = 0                                   
                                   and supr_100.seq_item_pedido       > 0
                                   and supr_100.qtde_saldo_item       > 0.000000
                                   and supr_100.cod_cancelamento      = 0
                                   and supr_100.item_100_nivel99 not in ('0')
                                   and supr_100.situacao_item    not in (3)
                                 UNION ALL
                                 select supr_100.qtde_saldo_item, supr_100.num_ped_compra,
                                        supr_100.seq_item_pedido, supr_100.situacao_item,
                                        supr_100.data_prev_entr,  supr_100.item_100_nivel99,
                                        supr_100.item_100_grupo,  supr_100.item_100_subgrupo,
                                        supr_100.item_100_item,   supr_090.codigo_empresa,
                                        supr_100.periodo_compras
                                 from supr_090, supr_100
                                 where recalc_referencias             = 0
                                   and exists (select 1 from tmrp_655
                                               where ((tmrp_655.area_producao  = 9
                                                 and   tmrp_655.ordem_producao = supr_090.pedido_compra)
                                                  or  (tmrp_655.nivel          = supr_100.item_100_nivel99
                                                 and   tmrp_655.grupo          = supr_100.item_100_grupo)))
                                   and supr_090.pedido_compra         = supr_100.num_ped_compra
                                   and supr_090.cod_cancelamento      = 0
                                   and supr_090.ordem_servico         = 0
                                   and supr_100.seq_item_pedido       > 0
                                   and supr_100.qtde_saldo_item       > 0.000000
                                   and supr_100.cod_cancelamento      = 0
                                   and supr_100.item_100_nivel99 not in ('0')
                                   and supr_100.situacao_item    not in (3)
                                   ) supr100
                           order by  supr100.codigo_empresa,  supr100.num_ped_compra, supr100.seq_item_pedido)
      loop
         if codigoEmpresaAnt != reg_supr_100.codigo_empresa
         then
            codigoEmpresaAnt := reg_supr_100.codigo_empresa;
            
            begin
               select fatu_503.consiste_empresa_compras
               into   consiste_emp
               from fatu_503
               where fatu_503.codigo_empresa = reg_supr_100.codigo_empresa;
            exception when others then
                  consiste_emp := 'N';
            end;
         end if;

         begin
            select pcpc_010.periodo_producao
            into   periodo_compras
            from pcpc_010
            where pcpc_010.area_periodo     = 9
              and pcpc_010.situacao_periodo < 3
              and (pcpc_010.codigo_empresa  = reg_supr_100.codigo_empresa
              or   consiste_emp             = 'N')
              and pcpc_010.data_ini_periodo <= reg_supr_100.data_prev_entr
              and pcpc_010.data_fim_periodo >= reg_supr_100.data_prev_entr
              and rownum = 1;
         exception when others then
            periodo_compras := 0;
         end;
         
         if reg_supr_100.qtde_saldo_item < 0.000000
         or reg_supr_100.qtde_saldo_item = 0.000000
         then
            atz_saldo := 0.0000000;
         else
            atz_saldo := reg_supr_100.qtde_saldo_item;
         end if;
         
         if tem_tmrp_650 = 1
         then
            inter_pr_find_data_requerida(0,
                                         0,
                                         7, /* Fia??o */
                                         1, /* M?quina */
                                         data_req);
         else
            data_req := null;
         end if;
         
         temTmrp655 (9, reg_supr_100.num_ped_compra, reg_supr_100.item_100_nivel99, reg_supr_100.item_100_grupo);
         
         begin
            insert into tmrp_041 (
               periodo_producao,              area_producao,
               nr_pedido_ordem,               seq_pedido_ordem,
               codigo_estagio,                nivel_estrutura,
               grupo_estrutura,               subgru_estrutura,
               item_estrutura,                qtde_reservada,
               qtde_areceber,                 consumo,
               data_requirida
            ) values (
               periodo_compras,              9,
               reg_supr_100.num_ped_compra,  reg_supr_100.seq_item_pedido,
               0,                            reg_supr_100.item_100_nivel99,
               reg_supr_100.item_100_grupo,  reg_supr_100.item_100_subgrupo,
               reg_supr_100.item_100_item,   0.000,
               atz_saldo,                    0.000,
               data_req
            );
         exception when others then
            --raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_041' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )||'#');
            data_req := null;
         end;
         
         v_qtde_reg_supr_100 := v_qtde_reg_supr_100 + 1;
         
         if v_qtde_reg_supr_100 = 50
         then
            COMMIT;
            v_qtde_reg_supr_100 := 0;
         end if;
      end loop; -- fim reg_supr_100
   end if;
   
   COMMIT;
   
   deleteTmrp655 ();
   
   commit;
END INTER_PR_RECALC_PLANEJAMENTO;
 
/
exec inter_pr_recompile;


