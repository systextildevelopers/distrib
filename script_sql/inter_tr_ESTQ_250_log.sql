create or replace trigger inter_tr_ESTQ_250_log 
after insert or delete or update 
on ESTQ_250 
for each row 
declare 
   ws_usuario_rede           varchar2(250); 
   ws_maquina_rede           varchar2(40); 
   ws_aplicativo             varchar2(20); 
   ws_sid                    number(9); 
   ws_empresa                number(3); 
   ws_usuario_systextil      varchar2(250); 
   ws_locale_usuario         varchar2(5); 
   v_nome_programa           varchar2(20); 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    -- begin 
    --    select hdoc_090.programa 
    --    into v_nome_programa 
    --    from hdoc_090 
    --    where hdoc_090.sid = ws_sid 
    --      and rownum       = 1 
    --      and hdoc_090.programa not like '%menu%' 
    --      and hdoc_090.programa not like '%_m%'; 
    --    exception 
    --      when no_data_found then            v_nome_programa := 'SQL'; 
    -- end; 
 
    v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 
 if inserting 
 then 
    begin 
 
        insert into ESTQ_250_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           id_OLD,   /*8*/ 
           id_NEW,   /*9*/ 
           endereco_OLD,   /*10*/ 
           endereco_NEW,   /*11*/ 
           empresa_OLD,   /*12*/ 
           empresa_NEW,   /*13*/ 
           criado_por_OLD,   /*14*/ 
           criado_por_NEW,   /*15*/ 
           criado_em_OLD,   /*16*/ 
           criado_em_NEW    /*17*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.id, /*9*/   
           '',/*10*/
           :new.endereco, /*11*/   
           0,/*12*/
           :new.empresa, /*13*/   
           '',/*14*/
           :new.criado_por, /*15*/
           '' ,
           :new.criado_em /*17*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into ESTQ_250_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           id_OLD, /*8*/   
           id_NEW, /*9*/   
           endereco_OLD, /*10*/   
           endereco_NEW, /*11*/   
           empresa_OLD, /*12*/   
           empresa_NEW, /*13*/   
           criado_por_OLD, /*14*/   
           criado_por_NEW, /*15*/   
           criado_em_OLD, /*16*/   
           criado_em_NEW  /*17*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.id,  /*8*/  
           :new.id, /*9*/   
           :old.endereco,  /*10*/  
           :new.endereco, /*11*/   
           :old.empresa,  /*12*/  
           :new.empresa, /*13*/   
           :old.criado_por,  /*14*/  
           :new.criado_por, /*15*/   
           :old.criado_em,  /*16*/  
           :new.criado_em  /*17*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into ESTQ_250_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           id_OLD, /*8*/   
           id_NEW, /*9*/   
           endereco_OLD, /*10*/   
           endereco_NEW, /*11*/   
           empresa_OLD, /*12*/   
           empresa_NEW, /*13*/   
           criado_por_OLD, /*14*/   
           criado_por_NEW, /*15*/   
           criado_em_OLD, /*16*/   
           criado_em_NEW /*17*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.id, /*8*/   
           0, /*9*/
           :old.endereco, /*10*/   
           '', /*11*/
           :old.empresa, /*12*/   
           0, /*13*/
           :old.criado_por, /*14*/   
           '', /*15*/
           :old.criado_em,
           ''/*16*/   
         );    
    end;    
 end if;    
end inter_tr_ESTQ_250_log;
