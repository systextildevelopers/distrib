ALTER TABLE estq_060 
MODIFY NUMERO_PEDIDO number(9);

ALTER TABLE estq_060_hist 
MODIFY PEDIDO_OLD number(9);

ALTER TABLE estq_060_hist 
MODIFY PEDIDO_ATU  number(9);

ALTER TABLE pedi_109 
MODIFY PEDIDO_VENDA  number(9);

/
exec inter_pr_recompile;
