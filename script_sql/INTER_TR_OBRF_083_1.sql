create or replace TRIGGER INTER_TR_OBRF_083_1
  after delete on obrf_082

  for each row
begin
      if deleting
      then
         begin
            delete from obrf_083
            where obrf_083.numero_ordem = :old.numero_ordem
            and obrf_083.seq_areceber = :old.seq_areceber
            and obrf_083.sequencia = :old.sequencia;
         exception when others then
            null;
         end;
      end if;
end inter_tr_obrf_083_1;
/
