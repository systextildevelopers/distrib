create or replace PROCEDURE "INTER_PR_CALC_VAR_CAMB_CPAG"(
   p_tipo_calculo in number,    p_empresa     in number,    p_filial      in number,
   p_periodo_mes in varchar2,    p_periodo_ano in varchar2,    
   p_titulo       in number,    p_parcela     in varchar2,  p_tipo_titulo in number,
   p_cnpj9        in number,    p_cnpj4       in number,    p_cnpj2       in number, 
   p_moeda        in number,    p_cotacao     in number)
is
   cursor titulos is
   select * from cpag_010
   where (cpag_010.codigo_empresa    = p_filial
     or (p_filial = 9999
         and exists (select 1 from fatu_500
                     where fatu_500.codigo_matriz  = p_empresa
                       and fatu_500.codigo_empresa = cpag_010.codigo_empresa)))
     and ((
         cpag_010.nr_duplicata         = p_titulo
         and cpag_010.parcela          = p_parcela
         and cpag_010.tipo_titulo      = p_tipo_titulo
         and cpag_010.cgc_9            = p_cnpj9
         and cpag_010.cgc_4            = p_cnpj4
         and cpag_010.cgc_2            = p_cnpj2
         and p_tipo_calculo            = 2)
         or p_tipo_calculo = 1
     )
     and cpag_010.situacao     <> 2
     and cpag_010.saldo_titulo <> 0
     and cpag_010.moeda_titulo = p_moeda;

   v_data_ajuste                  cpag_019.data_ajuste%type;
   v_sld_reais_antes              cpag_019.sld_reais_antes%type;
   v_vlr_ajuste                   cpag_010.saldo_titulo%type;
   v_num_contabil                 cpag_010.num_contabil%type;
   v_conta_credito                cont_535.cod_reduzido%type;
   v_conta_debito                 cont_535.cod_reduzido%type;
   v_ativa_passiva                number;
   v_exercicio                    cont_500.exercicio%type;
   v_cod_plano_cta                cont_500.cod_plano_cta%type;
   v_cta_var_camb_ativ_cpag       cont_560.cta_var_camb_ativ_cpag%type;
   v_cta_var_camb_pass_cpag       cont_560.cta_var_camb_pass_cpag%type;
   v_hst_var_camb_cpag            cont_560.hst_var_camb_cpag%type;
   v_ccusto_var_camb_cpag         cont_560.ccusto_var_camb_cpag%type;
   v_compl_histor1                cont_600.compl_histor1%type;
   v_cod_contab_for               supr_010.codigo_contabil%type;
   v_valor_saldo_moeda            cpag_010.saldo_titulo%type;
   v_saldo_duplicata_old          cpag_010.saldo_titulo%type;
   v_saldo_duplicata_new          cpag_010.saldo_titulo%type;
   v_valor_duplicata_new          cpag_010.valor_parcela%type;
   v_gera_contabil                fatu_500.gera_contabil%type;
   v_trans_atualiza_contab        estq_005.atualiza_contabi%type;
   v_vlr_ajuste_contab            cpag_010.saldo_titulo%type;
   v_des_erro                     varchar2(1000);
   v_executou_processo            boolean;
   v_teve_cpag_019                boolean;

begin
    
   if p_tipo_calculo = 1
    then
        v_data_ajuste := last_day(TO_DATE( p_periodo_mes || '01' || p_periodo_ano, 'MMDDYY'));
    else
        v_data_ajuste := sysdate;
        
    end if;

   begin
     select fatu_500.gera_contabil
     into v_gera_contabil
     from fatu_500
     where fatu_500.codigo_empresa = p_empresa;
   end;
   
   v_executou_processo := FALSE;

   for titulo in titulos
   loop
      begin
        
         v_saldo_duplicata_old := titulo.saldo_titulo;

         begin
           select estq_005.atualiza_contabi
           into v_trans_atualiza_contab
           from estq_005
           where estq_005.codigo_transacao = titulo.codigo_transacao;
         exception
           when others then v_trans_atualiza_contab := 0;
         end;
         
         v_teve_cpag_019 := TRUE;
         begin
           select cpag_019.sld_reais_antes into v_sld_reais_antes
           from cpag_019
           where cpag_019.cod_empresa = titulo.codigo_empresa
             and cpag_019.num_titulo  = titulo.nr_duplicata
             and cpag_019.parcela     = titulo.parcela
             and cpag_019.tipo_titulo = titulo.tipo_titulo
             and cpag_019.cnpj9       = titulo.cgc_9
             and cpag_019.cnpj4       = titulo.cgc_4
             and cpag_019.cnpj2       = titulo.cgc_2
             and cpag_019.data_ajuste = v_data_ajuste
             and cpag_019.situacao    = 0;
         exception
           when others then 
              v_sld_reais_antes := 0.00;
              v_teve_cpag_019   := FALSE;
         end;


         inter_pr_get_saldo_moeda_cpag(titulo.codigo_empresa, titulo.nr_duplicata, titulo.parcela, titulo.tipo_titulo, titulo.cgc_9,
            titulo.cgc_4, titulo.cgc_2, titulo.valor_moeda, titulo.valor_irrf, titulo.valor_iss, titulo.valor_inss, p_cotacao, 
            v_valor_saldo_moeda);

         v_saldo_duplicata_new := v_valor_saldo_moeda * p_cotacao;
         v_vlr_ajuste := v_saldo_duplicata_new - v_saldo_duplicata_old;
         v_valor_duplicata_new := titulo.valor_parcela + v_vlr_ajuste;

         if v_vlr_ajuste >= 0
         then v_ativa_passiva := 0;
         else v_ativa_passiva := 1;
         end if;
         
         

         if v_vlr_ajuste <> 0
         then
           v_executou_processo := TRUE;

           if v_sld_reais_antes <> 0
           then
              begin
                 delete from cpag_019
                 where cpag_019.cod_empresa = titulo.codigo_empresa
                   and cpag_019.num_titulo  = titulo.nr_duplicata
                   and cpag_019.parcela     = titulo.parcela
                   and cpag_019.tipo_titulo = titulo.tipo_titulo
                   and cpag_019.cnpj9       = titulo.cgc_9
                   and cpag_019.cnpj4       = titulo.cgc_4
                   and cpag_019.cnpj2       = titulo.cgc_2
                   and cpag_019.data_ajuste = v_data_ajuste;


                 if v_gera_contabil = 1 and v_trans_atualiza_contab = 1 
                 then
                   inter_pr_gera_lanc_cont(titulo.codigo_empresa,
                                   22,
                                   titulo.num_contabil,
                                   v_ccusto_var_camb_cpag,
                                   v_data_ajuste,
                                   v_hst_var_camb_cpag,
                                   v_compl_histor1,
                                   3,
                                   titulo.codigo_transacao,
                                   0,
                                   0.00,
                                   0,
                                   0.00,
                                   0,
                                   titulo.conta_pagto,
                                   v_data_ajuste,
                                   titulo.nr_duplicata,
                                   titulo.cgc_9,
                                   titulo.cgc_4,
                                   titulo.cgc_2,
                                   1,
                                   titulo.nr_duplicata,
                                   '1',
                                   1,
                                   1,
                                   0,
                                   0,
                                   0,
                                   0,
                                   v_des_erro);
                   if v_des_erro is not null
                   then raise_application_error(-20001, v_des_erro);
                   end if;
                 end if;
              end;
           end if;

           v_sld_reais_antes := titulo.saldo_titulo;

           if v_gera_contabil = 1 and v_trans_atualiza_contab = 1 
           then
             v_exercicio := inter_fn_checa_data(p_empresa, v_data_ajuste,0);

             begin
               select cod_plano_cta
               into v_cod_plano_cta
               from cont_500
               where exercicio = v_exercicio
               and cod_empresa = p_empresa;
             exception
                when others then v_cod_plano_cta := 0;
             end;

             begin
               select cta_var_camb_ativ_cpag, cta_var_camb_pass_cpag,
                      hst_var_camb_cpag, ccusto_var_camb_cpag
               into v_cta_var_camb_ativ_cpag, v_cta_var_camb_pass_cpag,
                    v_hst_var_camb_cpag, v_ccusto_var_camb_cpag
               from cont_560
               where cont_560.cod_plano_cta = v_cod_plano_cta;
             exception
                when others then 
                   v_cta_var_camb_ativ_cpag   := -1;
                   v_cta_var_camb_pass_cpag   := -1;
                   v_hst_var_camb_cpag        := 0;
                   v_ccusto_var_camb_cpag     := 0;
             end;
             
             if v_cta_var_camb_ativ_cpag = 0 then 
                v_cta_var_camb_ativ_cpag := -1;
             end if;
             if v_cta_var_camb_pass_cpag = 0 then 
                v_cta_var_camb_pass_cpag := -1;
             end if;

             begin
               select supr_010.codigo_contabil
               into v_cod_contab_for
               from supr_010
               where supr_010.fornecedor9 = titulo.cgc_9
                 and supr_010.fornecedor4 = titulo.cgc_4
                 and supr_010.fornecedor2 = titulo.cgc_2;
               exception
                  when others then v_cod_contab_for := 0;
             end;

             v_compl_histor1 := titulo.nr_duplicata || '/' || titulo.parcela || ' - ' || titulo.tipo_titulo || ' - '
                           || titulo.cgc_9 || '/' || titulo.cgc_4 || '-' || titulo.cgc_2;

             if v_ativa_passiva = 0
             then
               v_conta_debito := v_cta_var_camb_ativ_cpag;
               v_conta_credito := INTER_FN_ENCONTRA_CONTA(titulo.codigo_empresa,
                                                       2,
                                                       v_cod_contab_for,
                                                       titulo.codigo_transacao,
                                                       v_ccusto_var_camb_cpag,
                                                       v_exercicio,
                                                       v_exercicio);
               if v_conta_credito < 0
               THEN
                 v_des_erro := 'Conta contabil de débito não encontrada. ' ||
                               'Esta transação não será confirmada. Contate o contador. ' ||
                               'Tipo contábil: 1, Transação: ' || titulo.codigo_transacao || ' ' ||
                               'Código contábil: ' || v_cod_contab_for;
                 raise_application_error(-20001,  v_des_erro);
               END IF;
             else
               v_conta_debito := INTER_FN_ENCONTRA_CONTA(titulo.codigo_empresa,
                                                       2,
                                                       v_cod_contab_for,
                                                       titulo.codigo_transacao,
                                                       v_ccusto_var_camb_cpag,
                                                       v_exercicio,
                                                       v_exercicio);
               v_conta_credito := v_cta_var_camb_pass_cpag;
               if v_conta_debito < 0
               THEN
                
                 v_des_erro := 'Conta contabil de débito não encontrada. ' ||
                               'Esta transação não será confirmada. Contate o contador. ' ||
                               'Tipo contábil: 1, Transação: ' || titulo.codigo_transacao || ' ' ||
                               'Código contábil: ' || v_cod_contab_for;
                 raise_application_error(-20001, v_des_erro);
               END IF;
             end if;


             v_num_contabil := 0;

             if v_vlr_ajuste >= 0
             then v_vlr_ajuste_contab := v_vlr_ajuste;
             else v_vlr_ajuste_contab := v_vlr_ajuste * -1;
             end if;

             if v_conta_debito > 0 and v_conta_credito > 0 
             then
               inter_pr_gera_lanc_cont(titulo.codigo_empresa,
                                   22,
                                   v_num_contabil,
                                   v_ccusto_var_camb_cpag,
                                   v_data_ajuste,
                                   v_hst_var_camb_cpag,
                                   v_compl_histor1,
                                   1,
                                   titulo.codigo_transacao,
                                   v_conta_debito,
                                   v_vlr_ajuste_contab,
                                   v_conta_credito,
                                   v_vlr_ajuste_contab,
                                   0,
                                   titulo.conta_pagto,
                                   v_data_ajuste,
                                   titulo.nr_duplicata,
                                   titulo.cgc_9,
                                   titulo.cgc_4,
                                   titulo.cgc_2,
                                   1,
                                   titulo.nr_duplicata,
                                   '1',
                                   1,
                                   1,
                                   0,
                                   0,
                                   0,
                                   0,
                                   v_des_erro);
                    if v_des_erro is not null
                    then raise_application_error(-20001,  v_des_erro);
                    end if;
            end if;
           end if;

           if v_num_contabil = 0
           then v_num_contabil := titulo.num_contabil;
           end if;
        
           begin
             update cpag_010
             set saldo_titulo = v_saldo_duplicata_new,
                 num_contabil    = v_num_contabil,
                 valor_parcela = v_valor_duplicata_new
             where cpag_010.codigo_empresa = titulo.codigo_empresa
               and cpag_010.cgc_9          = titulo.cgc_9
               and cpag_010.cgc_4          = titulo.cgc_4
               and cpag_010.cgc_2          = titulo.cgc_2
               and cpag_010.tipo_titulo    = titulo.tipo_titulo
               and cpag_010.nr_duplicata   = titulo.nr_duplicata
               and cpag_010.parcela        = titulo.parcela;
           end;

           begin
             insert into cpag_019
               (cod_empresa,      data_ajuste, num_titulo,    parcela,        tipo_titulo,
                cnpj9,            cnpj4,       cnpj2,         cod_moeda,      cotacao,
                data_emissao,     data_vencto, vlr_tit_moeda, sld_tit_moeda,  sld_reais_antes,
                slr_reais_depois, vlr_ajuste,  ativa_passiva, chave_contabil, conta_debito,
                conta_credito,    situacao, ind_pagar_receb)
             values
               (titulo.codigo_empresa,   v_data_ajuste,           titulo.nr_duplicata,    titulo.parcela,         titulo.tipo_titulo,
                titulo.cgc_9,            titulo.cgc_4,            titulo.cgc_2,           titulo.moeda_titulo,    p_cotacao,
                titulo.data_transacao,   titulo.data_vencimento,  titulo.valor_moeda,      v_valor_saldo_moeda,   v_sld_reais_antes,
                v_saldo_duplicata_new,   v_vlr_ajuste,            v_ativa_passiva,         v_num_contabil,        v_conta_debito,
                v_conta_credito,         0, 'P');
           end;
            
           commit;

        end if;

     EXCEPTION
        WHEN OTHERS
        THEN
           if p_tipo_calculo = 2 -- por titulo
           then
             raise_application_error(-20001, 'Erro ao processar inter_pr_contabilização ' || SQLERRM);
           else -- por periodo
             if v_teve_cpag_019
             then
                 
                update cpag_019
                set situacao = 1,    observacao = v_des_erro
                where cpag_019.cod_empresa = titulo.codigo_empresa
                  and cpag_019.num_titulo  = titulo.nr_duplicata
                  and cpag_019.parcela     = titulo.parcela
                  and cpag_019.tipo_titulo = titulo.tipo_titulo
                  and cpag_019.cnpj9       = titulo.cgc_9
                  and cpag_019.cnpj4       = titulo.cgc_4
                  and cpag_019.cnpj2       = titulo.cgc_2
                  and cpag_019.data_ajuste = v_data_ajuste;

             else
             
                insert into cpag_019
                  (cod_empresa,      data_ajuste, num_titulo,    parcela,        tipo_titulo,
                   cnpj9,            cnpj4,       cnpj2,         cod_moeda,      cotacao,
                   data_emissao,     data_vencto, vlr_tit_moeda, sld_tit_moeda,  sld_reais_antes,
                   slr_reais_depois, vlr_ajuste,  ativa_passiva, chave_contabil, conta_debito,
                   conta_credito,    situacao,    observacao, ind_pagar_receb)
                values
                  (titulo.codigo_empresa,   v_data_ajuste,           titulo.nr_duplicata,    titulo.parcela,         titulo.tipo_titulo,
                   titulo.cgc_9,            titulo.cgc_4,            titulo.cgc_2,           titulo.moeda_titulo,    p_cotacao,
                   titulo.data_transacao,   titulo.data_vencimento,  titulo.valor_moeda,      v_valor_saldo_moeda,   v_sld_reais_antes,
                   v_saldo_duplicata_new,   v_vlr_ajuste,            v_ativa_passiva,         v_num_contabil,        v_conta_debito,
                   v_conta_credito,         1, v_des_erro,           'P');
                
              end if;
             commit;
           end if;
     END;
   
   end loop;
    if not v_executou_processo
    then
       raise_application_error(-20001, 'Nao foi alterado valor cambial de nenhum titulo.');
    end if;
EXCEPTION WHEN NO_DATA_FOUND
then
    if p_tipo_calculo = 1
    then
        raise_application_error(-20001, 'Nao foi encontrado registros para o periodo. ');
    end if;
end inter_pr_calc_var_camb_cpag;
