create table adps_001
(
       nr_lote_adps   number(9) default 0,
       seq_leitura    number(9) default 0,
       endereco       varchar2(30) default '',
       pedido_venda   number(9) default 0,
       volumes_pedido number(9) default 0,
       volumes_saldo  number(9) default 0,
       volumes_lidos  number(9) default 0,
       situacao       number(1) default 0,
       tipo_coleta    number(1) default 0
);

create table adps_004
(
       nr_lote_adps number(9) default 0,
       seq_leitura number(9) default 0,
       endereco varchar2(30) default '',
       pedido_venda number(9) default 0,
       usuario_resp varchar2(40) default '',
       situacao number(1) default 0
);

create table adps_003
(
       nr_lote_adps   number(9) default 0,
       pedido_venda   number(9) default 0,
       numero_volume  number(9) default 0,
       situacao       number(1) default 0
);
