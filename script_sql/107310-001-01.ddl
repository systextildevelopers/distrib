    alter table pedi_010_hist   modify limite_max_ped2_new number(9);
    alter table pedi_010_hist   modify limite_max_ped1_new number(9);
	alter table pedi_010_hist   modify limite_max_ped4_new number(9);
	alter table pedi_010_hist   modify limite_max_ped7_new number(9);
    alter table pedi_010_hist   modify limite_max_ped2_old number(9);
    alter table pedi_010_hist   modify limite_max_ped1_old number(9);
	alter table pedi_010_hist   modify limite_max_ped4_old number(9);
	alter table pedi_010_hist   modify limite_max_ped7_old number(9);
    alter table pedi_010   modify limite_max_ped2 number(9);
    alter table pedi_010   modify limite_max_ped1 number(9);
	alter table pedi_010   modify limite_max_ped4 number(9);
	alter table pedi_010   modify limite_max_ped7 number(9);
    alter table i_pedi_010   modify limite_max_ped1 number(9);
    alter table i_pedi_010   modify limite_max_ped2 number(9);
	alter table i_pedi_010   modify limite_max_ped4 number(9);
	alter table i_pedi_010   modify limite_max_ped7 number(9);
