
delete hdoc_033 where programa = 'pcpc_f059';
delete hdoc_035 where codigo_programa = 'pcpc_f059';

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpc_f059', 'Ordens de Produção de destino dos Rolos / Tecido', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpc_f059', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpc_f059', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Órdenes de producción de destino de rollos / telas'
 where hdoc_036.codigo_programa = 'pcpc_f059'
   and hdoc_036.locale          = 'es_ES';
commit;
