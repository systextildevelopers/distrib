
  CREATE OR REPLACE FUNCTION "PROXIMOCODIGO" (Tabela IN VARCHAR2 ,
                                          Filial IN CHAR )
RETURN VARCHAR2 IS
-- Objetivo
-- 1) Obter o proximo valor sequencial de uma tabela
--
  proximo      Number;

-- Principal
begin

   -- Obtem o proximo codigo
   IF    Tabela = 'TT_SQL' THEN SELECT SQSQL.NEXTVAL  INTO proximo FROM DUAL;
   ELSIF Tabela = 'TT_TAR' THEN SELECT SQTAR.NEXTVAL  INTO proximo FROM DUAL;
   ELSIF Tabela = 'TT_MAI' THEN SELECT SQMAI.NEXTVAL  INTO proximo FROM DUAL;
   ELSIF Tabela = 'TT_HND' THEN SELECT SQHND.NEXTVAL  INTO proximo FROM DUAL;
   ELSIF Tabela = 'TT_ANX' THEN SELECT SQANX.NEXTVAL  INTO proximo FROM DUAL;
   ELSIF Tabela = 'TD_SET' THEN SELECT SQDSET.NEXTVAL INTO proximo FROM DUAL;
   END IF;

   IF proximo IS NOT NULL THEN
      return LPAD(RTRIM(proximo),10,' ');
   END IF;

END ProximoCodigo;



 

/

exec inter_pr_recompile;

