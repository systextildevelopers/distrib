CREATE OR REPLACE TRIGGER "INTER_TR_BASI_013_3" 
   after insert
      or update
      of consumo_componente
on basi_013 -- Estrutura do produto de Projeto
for each row

declare
Pragma Autonomous_Transaction;
   v_valor_parametro             number;
   v_um                          varchar2(2);
   v_tamanho                     varchar2(3);

begin
    begin    
      select empr_002.consumo_max_comp
      into v_valor_parametro
      from empr_002;
      exception
      when OTHERS then
        v_valor_parametro := 0;
    end;
   
    begin
      select basi_030.unidade_medida 
      into v_um
      from basi_030
      where basi_030.nivel_estrutura = :new.nivel_comp
      and   basi_030.referencia      = :new.grupo_comp;
      exception
      when OTHERS then
        v_um := '';
    end;
    
    begin
      select basi_020.tamanho_ref 
      into v_tamanho
      from basi_020
      where basi_020.basi030_nivel030 = :new.nivel_item
      and   basi_020.basi030_referenc = :new.grupo_item;
      exception
      when OTHERS then
        v_tamanho := '';
    end;
      
    if inserting or updating and v_valor_parametro > 0 and v_um = 'KG'
    then
       if :new.consumo_componente <> :old.consumo_componente
       then
         inter_pr_valida_quantidade_kg(v_valor_parametro,
                                       :new.codigo_projeto,
                                       :new.sequencia_projeto,
                                       :new.nivel_item,
                                       :new.grupo_item,
                                       :new.subgru_item,
                                       :new.item_item,
                                       :new.sequencia_estrutura,
                                       :new.alternativa_produto,
                                       :new.consumo_componente,
                                       v_tamanho);
      end if;
   end if;
end inter_tr_basi_013_3;

/

exec inter_pr_recompile;

