create or replace function inter_fn_trata_EX_str(p_uf       in varchar,
                                                 p_campo1   in varchar) 
                                                 return number is
   begin
  /* TRATA UF EX, SE FOR EX, RETORNA NULL, CASO CONTRARIO O PROPRIO VALOR
   */  
   
   -- utilizada para numericos


       if p_uf = 'EX'  
       then 
          return null;
       else
          return p_campo1;
       end if;
end inter_fn_trata_EX_str;
/
