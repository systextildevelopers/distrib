create or replace trigger inter_tr_pedi_095_1
before update on pedi_095
for each row
begin
  -- verifica se o valor da tabela de pre�o foi alterada
  if :new.val_tabela_preco <> :old.val_tabela_preco 
  then
    :new.data_form_preco := trunc(sysdate);
  end if;
end;

/
