
  CREATE OR REPLACE FUNCTION "INTER_FN_FIOS_PROD_MES_LINHA" 
        (fniv_parametro varchar2,
         fgru_parametro varchar2,
         fdat_parametro varchar2)
RETURN number
IS
   flin_prod number(2);
   ftot_qtde number(15,4);
begin
   select basi_030.linha_produto into flin_prod from basi_030
   where basi_030.nivel_estrutura = fniv_parametro
     and basi_030.referencia      = fgru_parametro;

   select nvl(sum(estq_060.peso_liquido),0) into ftot_qtde from estq_060, estq_005, basi_030
   where estq_060.prodcai_nivel99 = basi_030.nivel_estrutura
     and estq_060.prodcai_grupo   = basi_030.referencia
     and basi_030.linha_produto   = flin_prod
     and estq_060.transacao_ent                    = estq_005.codigo_transacao
     and to_char(estq_060.data_producao,'YYYY/MM') = fdat_parametro
     and estq_005.tipo_transacao                   = 'P';

   return(ftot_qtde);
end inter_fn_fios_prod_mes_linha;


 

/

exec inter_pr_recompile;

