CREATE OR REPLACE TRIGGER INTER_TR_ESTQ_307
BEFORE INSERT ON ESTQ_307
FOR EACH ROW
DECLARE
    v_saldo NUMBER;
     v_saldo_emp NUMBER;
    v_char varchar2(40);
BEGIN
    v_char := :NEW.ORIGEM_MOVIMENTO;

    BEGIN
        SELECT SALDO_QTDE, SALDO_QTDE
        INTO v_saldo,      v_saldo_emp
        FROM ESTQ_308
        WHERE COD_EMPRESA = :NEW.COD_EMPRESA
          AND NIVEL_ESTRUTURA = :NEW.NIVEL_ESTRUTURA
          AND GRUPO_ESTRUTURA = :NEW.GRUPO_ESTRUTURA
          AND SUBGRU_ESTRUTURA = :NEW.SUBGRU_ESTRUTURA
          AND ITEM_ESTRUTURA = :NEW.ITEM_ESTRUTURA;
          exception
            when others then v_saldo := NULL; v_saldo_emp := null;
    END;
    
    IF v_saldo IS NOT NULL AND v_saldo_emp IS NOT NULL THEN
               
       IF :NEW.TIPO_MOVIMENTO = 'E' THEN
          UPDATE ESTQ_308
          SET SALDO_QTDE = SALDO_QTDE + :NEW.QUANTIDADE
          WHERE COD_EMPRESA = :NEW.COD_EMPRESA
          AND NIVEL_ESTRUTURA = :NEW.NIVEL_ESTRUTURA
          AND GRUPO_ESTRUTURA = :NEW.GRUPO_ESTRUTURA
          AND SUBGRU_ESTRUTURA = :NEW.SUBGRU_ESTRUTURA
          AND ITEM_ESTRUTURA = :NEW.ITEM_ESTRUTURA;
          
          :NEW.qtde_saldo := v_saldo + :NEW.QUANTIDADE;
          
       ELSE
           if v_saldo_emp < :NEW.QUANTIDADE
           then
              raise_application_error(-20000,'Saldo insuficiente para o produto Conta Corrente Revenda desta empresa: ' || 
                                              ' Empr: ' || :NEW.COD_EMPRESA ||
                                              ' Prod: ' || :NEW.NIVEL_ESTRUTURA || '.'
                                                       || :NEW.GRUPO_ESTRUTURA || '.'
                                                       || :NEW.SUBGRU_ESTRUTURA || '.'
                                                       || :NEW.ITEM_ESTRUTURA 
                                                       || ' qtde necess. '  || :NEW.QUANTIDADE 
                                                       || ' Saldo: ' || v_saldo_emp
                                                                 );
           end if;
           :NEW.qtde_saldo := v_saldo - :NEW.QUANTIDADE;
           
           UPDATE ESTQ_308
           SET SALDO_QTDE = SALDO_QTDE - :NEW.QUANTIDADE
           WHERE COD_EMPRESA = :NEW.COD_EMPRESA
              AND NIVEL_ESTRUTURA = :NEW.NIVEL_ESTRUTURA
              AND GRUPO_ESTRUTURA = :NEW.GRUPO_ESTRUTURA
              AND SUBGRU_ESTRUTURA = :NEW.SUBGRU_ESTRUTURA
              AND ITEM_ESTRUTURA = :NEW.ITEM_ESTRUTURA;
            
       END IF;
    ELSE
       IF :NEW.TIPO_MOVIMENTO = 'E' THEN
            :NEW.qtde_saldo := :NEW.QUANTIDADE;
            
            INSERT INTO ESTQ_308 (
                COD_EMPRESA,
                NIVEL_ESTRUTURA,
                GRUPO_ESTRUTURA,
                SUBGRU_ESTRUTURA,
                ITEM_ESTRUTURA,
                SALDO_QTDE,
                QTDE_EMPENHADA
            ) VALUES (
                :NEW.COD_EMPRESA,
                :NEW.NIVEL_ESTRUTURA,
                :NEW.GRUPO_ESTRUTURA,
                :NEW.SUBGRU_ESTRUTURA,
                :NEW.ITEM_ESTRUTURA,
                :NEW.QUANTIDADE,
                0 
            );
        END IF;
        
    END IF;
END INTER_TR_ESTQ_307;
