create or replace view stview_exp_contabil_sci as
select
       nf_itens_cc.empresa
       ,nf_itens_cc.identificador
       ,nf_itens_cc.tipo_movimento
       ,nf_itens_cc.cnpj_cpf_participante
       ,nf_itens_cc.inscricao_estadual
       ,nf_itens_cc.numero_do_documento 
       ,nf_itens_cc.data_do_lancamento
       ,nf_itens_cc.serie
       ,nf_itens_cc.especie
       ,nf_itens_cc.modelo       
       ,nf_itens_cc.natureza_de_operacao
       ,nf_itens_cc.tipo_documento
       ,decode(-99, nf_itens_cc.conta_debito, '', nf_itens_cc.conta_debito) conta_debito
       ,nf_itens_cc.conta_credito
       ,nf_itens_cc.sequencial
       ,nf_itens_cc.nome    
       , to_char((nf_itens_cc.valor_tot_item / nf_capa_cc.valor_total_nf_cc) * 100,'990.00')  as percentual
       ,nf_itens_cc.cnpj_empresa
  from 
  
    (select obrf_010.local_entrega                                                        as empresa
           ,'CCUSTO_SCI'                                                                 as identificador
           ,'ENT'                                                                        as tipo_movimento
           ,decode(obrf_010.cgc_cli_for_2
                  ,0
                  ,LPad(obrf_010.cgc_cli_for_9,9,0) || '-' || LPad(obrf_010.cgc_cli_for_2,2,0)
                  ,LPad(obrf_010.cgc_cli_for_9,8,0) || '/' || LPad(obrf_010.cgc_cli_for_4,4,0) || '-' || LPad(obrf_010.cgc_cli_for_2,2,0))     as cnpj_cpf_participante
           ,obrf_010.cgc_cli_for_9
           ,obrf_010.cgc_cli_for_4
           ,obrf_010.cgc_cli_for_2
           ,supr_010.inscr_est_forne                                                     as inscricao_estadual
           ,obrf_010.documento                                                           as numero_do_documento 
           ,obrf_010.data_transacao                                                      as data_do_lancamento
           ,obrf_010.serie                                                               as serie
           ,obrf_010.especie_docto                                                       as especie
           ,obrf_200.modelo_documento                                                    as modelo       
           ,pedi_080.cod_natureza || pedi_080.divisao_natur                              as natureza_de_operacao
           ,0                                                                            as tipo_documento
           ,inter_fn_encontra_conta(obrf_010.local_entrega, 3,
                                        obrf_015.codigo_contabil,
                                        obrf_015.codigo_transacao,
                                        obrf_015.centro_custo,
                                        inter_fn_checa_data(fatu_500.codigo_matriz, obrf_010.data_transacao, obrf_015.codigo_transacao),
                                        inter_fn_checa_data(fatu_500.codigo_matriz, obrf_010.data_transacao, 0)) as conta_debito
          , ''                                                                           as conta_credito
          ,0 as sequencial
          ,basi_185.centro_custo                                                         as nome    

        ,sum(obrf_015.valor_total) valor_tot_item
    
          ,LPad(fatu_500.cgc_9,8,0) || '/' || LPad(fatu_500.cgc_4,4,0) || '-' || LPad(fatu_500.cgc_2,2,0)              as cnpj_empresa
     from  obrf_015, obrf_010, supr_010, fatu_500, basi_185, fatu_505, obrf_200, pedi_080
     where obrf_015.empr_nf_saida     = obrf_010.local_entrega 
     and   obrf_015.capa_ent_nrdoc    = obrf_010.documento 
     and   obrf_015.capa_ent_serie    = obrf_010.serie 
     and   obrf_010.cgc_cli_for_9     = obrf_015.CAPA_ENT_FORCLI9
     and   obrf_010.cgc_cli_for_4     = obrf_015.CAPA_ENT_FORCLI4
     and   obrf_010.cgc_cli_for_2     = obrf_015.CAPA_ENT_FORCLI2
     and   fatu_505.codigo_empresa    = obrf_010.local_entrega 
     and   fatu_505.serie_nota_fisc   = obrf_010.serie 
     and   obrf_200.codigo_especie    = trim(fatu_505.codigo_especie) 
     and   obrf_010.situacao_entrada  in (1,4)
     and   obrf_010.cgc_cli_for_9     = supr_010.fornecedor9
     and   obrf_010.cgc_cli_for_4     = supr_010.fornecedor4
     and   obrf_010.cgc_cli_for_2     = supr_010.fornecedor2
     and   obrf_010.local_entrega     = fatu_500.codigo_empresa
     and   obrf_015.centro_custo      = basi_185.centro_custo
     and   obrf_015.natitem_nat_oper  = pedi_080.natur_operacao
     and   obrf_015.natitem_est_oper  = pedi_080.estado_natoper
     group by obrf_010.local_entrega
             ,decode(obrf_010.cgc_cli_for_2
             ,0
             ,LPad(obrf_010.cgc_cli_for_9,9,0) || '-' || LPad(obrf_010.cgc_cli_for_2,2,0)
             ,LPad(obrf_010.cgc_cli_for_9,8,0) || '/' || LPad(obrf_010.cgc_cli_for_4,4,0) || '-' || LPad(obrf_010.cgc_cli_for_2,2,0))
             ,obrf_010.cgc_cli_for_9
             ,obrf_010.cgc_cli_for_4
             ,obrf_010.cgc_cli_for_2
             ,supr_010.inscr_est_forne
             ,obrf_010.documento
             ,obrf_010.data_transacao
             ,obrf_010.serie
             ,obrf_010.especie_docto  
             ,obrf_200.modelo_documento
             ,pedi_080.cod_natureza || pedi_080.divisao_natur
             ,inter_fn_encontra_conta(obrf_010.local_entrega, 3,
                                      obrf_015.codigo_contabil,
                                      obrf_015.codigo_transacao,
                                      obrf_015.centro_custo,
                                      inter_fn_checa_data(fatu_500.codigo_matriz, obrf_010.data_transacao, obrf_015.codigo_transacao),
                                      inter_fn_checa_data(fatu_500.codigo_matriz, obrf_010.data_transacao, 0))
             ,basi_185.centro_custo   
             ,LPad(fatu_500.cgc_9,8,0) || '/' || LPad(fatu_500.cgc_4,4,0) || '-' || LPad(fatu_500.cgc_2,2,0) ) nf_itens_cc,
         
    ( select obrf010.local_entrega, obrf010.documento, obrf010.serie, 
          obrf010.cgc_cli_for_9, obrf010.cgc_cli_for_4,obrf010.cgc_cli_for_2,
        sum(obrf015.valor_total) as valor_total_nf_cc,
                    inter_fn_encontra_conta(obrf010.local_entrega, 3,
                                                     obrf015.codigo_contabil,
                                                     obrf015.codigo_transacao,
                                                     obrf015.centro_custo,
                                                     inter_fn_checa_data(fatu500.codigo_matriz, obrf010.data_transacao, obrf015.codigo_transacao),
                                                     inter_fn_checa_data(fatu500.codigo_matriz, obrf010.data_transacao, 0))	conta_c_nf
                    from obrf_015 obrf015, obrf_010 obrf010, fatu_500 fatu500
                    where obrf015.empr_nf_saida  = obrf010.local_entrega
                    and   obrf015.capa_ent_nrdoc = obrf010.documento
                    and   obrf015.capa_ent_serie = obrf010.serie
                    and   obrf015.CAPA_ENT_FORCLI9 = obrf010.cgc_cli_for_9     
                    and   obrf015.CAPA_ENT_FORCLI4 = obrf010.cgc_cli_for_4     
                    and   obrf015.CAPA_ENT_FORCLI2 = obrf010.cgc_cli_for_2
                    and   obrf010.local_entrega    = fatu500.codigo_empresa
          group by   obrf010.local_entrega, obrf010.documento, obrf010.serie, 
          obrf010.cgc_cli_for_9, obrf010.cgc_cli_for_4,obrf010.cgc_cli_for_2,
          inter_fn_encontra_conta(obrf010.local_entrega, 3,
                                                     obrf015.codigo_contabil,
                                                     obrf015.codigo_transacao,
                                                     obrf015.centro_custo,
                                                     inter_fn_checa_data(fatu500.codigo_matriz, obrf010.data_transacao, obrf015.codigo_transacao),
                                                     inter_fn_checa_data(fatu500.codigo_matriz, obrf010.data_transacao, 0)) ) nf_capa_cc


where  nf_itens_cc.empresa             = nf_capa_cc.local_entrega 
 and   nf_itens_cc.numero_do_documento = nf_capa_cc.documento
 and   nf_itens_cc.serie               = nf_capa_cc.serie
 and   nf_itens_cc.cgc_cli_for_9       = nf_capa_cc.cgc_cli_for_9
 and   nf_itens_cc.cgc_cli_for_4       = nf_capa_cc.cgc_cli_for_4
 and   nf_itens_cc.cgc_cli_for_2       = nf_capa_cc.cgc_cli_for_2
 and   nf_itens_cc.conta_debito        = nf_capa_cc.conta_c_nf;
/
