
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_101_LOG" 
   after insert or delete or update
       of CODIGO_PROJETO,            SEQUENCIA_PROJETO,
          CODIGO_TIPO_PRODUTO,       DATA_CADASTRO,
          DATA_INICIO,               DATA_TERMINO,
          LEAD_TIME_FOLGA,           SITUACAO_ATIVIDADES,
          SEQ_PROJETO_PAI
   on basi_101
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_tipo_produtol_o        varchar2(60);
   ws_tipo_produtol_n        varchar2(60);
   ws_situacao_atividade_o   varchar2(60);
   ws_situacao_atividade_n   varchar2(60);

   ws_teve                   varchar2(1);

   long_aux                  varchar2(2000);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produtol_n
         from basi_003
         where basi_003.codigo_tipo_produto = :new.codigo_tipo_produto;
      exception when no_data_found then
         ws_tipo_produtol_n := ' ';
      end;

      if :new.situacao_atividades = 0
      then
         ws_situacao_atividade_n := inter_fn_buscar_tag('lb29598#EM DESENVOLVIMENTO',ws_locale_usuario,ws_usuario_systextil);
      end if;

      if :new.situacao_atividades = 1
      then
         ws_situacao_atividade_n := inter_fn_buscar_tag('lb30577#FINALIZADA',ws_locale_usuario,ws_usuario_systextil);
      end if;

      if :new.situacao_atividades = 2
      then
         ws_situacao_atividade_n := inter_fn_buscar_tag('lb29884#REABERTO',ws_locale_usuario,ws_usuario_systextil);
      end if;

      INSERT INTO hist_100
         ( tabela,              operacao,
           data_ocorr,          aplicacao,
           usuario_rede,        maquina_rede,
           str01,               num05,
           long01
         )
      VALUES
         ( 'BASI_101',          'I',
           sysdate,             ws_aplicativo,
           ws_usuario_rede,     ws_maquina_rede,
          :new.codigo_projeto,  :new.sequencia_projeto,
           inter_fn_buscar_tag('lb34908#SUB-PROJETOS DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil)
           || chr(10) || chr(10) ||
           inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
           || :new.codigo_projeto    || chr(10) ||
           inter_fn_buscar_tag('lb05455#SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
           || :new.sequencia_projeto || chr(10) ||
           inter_fn_buscar_tag('lb01021#TIPO PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
           || :new.codigo_tipo_produto || ' - '
           || ws_tipo_produtol_n       || chr(10) ||
           inter_fn_buscar_tag('lb34905#SEQUENCIA RELAC.:',ws_locale_usuario,ws_usuario_systextil) || ' '
           || :new.seq_projeto_pai   || chr(10) ||
           inter_fn_buscar_tag('lb00610#DATA CADASTRO:',ws_locale_usuario,ws_usuario_systextil) || ' '
           || to_char(:new.data_cadastro,'DD/MM/YYYY')     || chr(10) ||
           inter_fn_buscar_tag('lb33002#DATA INICIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
           || to_char(:new.data_inicio,'DD/MM/YYYY')       || chr(10) ||
           inter_fn_buscar_tag('lb33003#DATA TERMINO:',ws_locale_usuario,ws_usuario_systextil) || ' '
           || to_char(:new.data_termino,'DD/MM/YYYY')      || chr(10) ||
           inter_fn_buscar_tag('lb34907#DIAS DE FOLGA:',ws_locale_usuario,ws_usuario_systextil) || ' '
           || :new.lead_time_folga || chr(10) ||
           inter_fn_buscar_tag('lb34906#SITUACAO ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
           || ws_situacao_atividade_n
        );
   end if;

   if updating
   then
      ws_teve := 'n';

      long_aux := long_aux ||
           inter_fn_buscar_tag('lb34908#SUB-PROJETOS DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil)
           || chr(10) || chr(10);

      if  :old.codigo_tipo_produto <> :new.codigo_tipo_produto
      then
         ws_teve := 's';

         begin
            select nvl(basi_003.descricao,' ')
            into ws_tipo_produtol_n
            from basi_003
            where basi_003.codigo_tipo_produto = :new.codigo_tipo_produto;
         end;

         begin
            select nvl(basi_003.descricao,' ')
            into ws_tipo_produtol_o
            from basi_003
            where basi_003.codigo_tipo_produto = :old.codigo_tipo_produto;
         end;

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb01021#TIPO PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || :old.codigo_tipo_produto
            || ws_tipo_produtol_o || ' -> '
            || :new.codigo_tipo_produto
            || ws_tipo_produtol_n
            || chr(10);
      end if;

      if  :old.seq_projeto_pai <> :new.seq_projeto_pai
      then
         ws_teve := 's';
         long_aux := long_aux ||
            inter_fn_buscar_tag('lb34905#SEQUENCIA RELAC.:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || :old.seq_projeto_pai   || ' -> '
            || :new.seq_projeto_pai
            || chr(10);
      end if;

      if (:old.data_inicio <> :new.data_inicio) or
         (:old.data_inicio is null)
      then
         ws_teve := 's';
         long_aux := long_aux ||
            inter_fn_buscar_tag('lb33002#DATA INICIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || to_char(:old.data_inicio,'DD/MM/YYYY') || ' -> '
            || to_char(:new.data_inicio,'DD/MM/YYYY')
            || chr(10);
      end if;

      if (:old.data_termino <> :new.data_termino) or
         (:old.data_termino is null)
      then
         ws_teve := 's';
         long_aux := long_aux ||
            inter_fn_buscar_tag('lb33003#DATA TERMINO:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || to_char(:old.data_termino,'DD/MM/YYYY') || ' -> '
            || to_char(:new.data_termino,'DD/MM/YYYY')
            || chr(10);
      end if;

      if :old.lead_time_folga <> :new.lead_time_folga
      then
         ws_teve := 's';
         long_aux := long_aux ||
            inter_fn_buscar_tag('lb34907#DIAS DE FOLGA:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || :old.lead_time_folga || ' -> '
            || :new.lead_time_folga
            || chr(10);
      end if;

      if :old.situacao_atividades <> :new.situacao_atividades
      then
         ws_teve := 's';

         if :old.situacao_atividades = 0
         then
            ws_situacao_atividade_o := inter_fn_buscar_tag('lb29598#EM DESENVOLVIMENTO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :new.situacao_atividades = 0
         then
            ws_situacao_atividade_n := inter_fn_buscar_tag('lb29598#EM DESENVOLVIMENTO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :old.situacao_atividades = 1
         then
            ws_situacao_atividade_o := inter_fn_buscar_tag('lb30577#FINALIZADA',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :new.situacao_atividades = 1
         then
            ws_situacao_atividade_n := inter_fn_buscar_tag('lb30577#FINALIZADA',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :old.situacao_atividades = 2
         then
            ws_situacao_atividade_o := inter_fn_buscar_tag('lb29884#REABERTO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :new.situacao_atividades = 2
         then
            ws_situacao_atividade_n := inter_fn_buscar_tag('lb29884#REABERTO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb34906#SITUACAO ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
            || ws_situacao_atividade_o || ' -> ' || ws_situacao_atividade_n || chr(10);
      end if;

      if ws_teve = 's'
      then
         INSERT INTO hist_100
            ( tabela,               operacao,
              data_ocorr,           aplicacao,
              usuario_rede,         maquina_rede,
              str01,                num05,
              long01
            )
         VALUES
            ( 'BASI_101',           'A',
              sysdate,              ws_aplicativo,
              ws_usuario_rede,      ws_maquina_rede,
              :new.codigo_projeto,  :new.sequencia_projeto,
              long_aux
            );
      end if;
   end if;

   if deleting
   then
      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produtol_o
         from basi_003
         where basi_003.codigo_tipo_produto = :old.codigo_tipo_produto;
      exception when no_data_found then
         ws_tipo_produtol_o := ' ';
      end;

         if :old.situacao_atividades = 0
         then
            ws_situacao_atividade_o := inter_fn_buscar_tag('lb29598#EM DESENVOLVIMENTO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :old.situacao_atividades = 1
         then
            ws_situacao_atividade_o := inter_fn_buscar_tag('lb30577#FINALIZADA',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :old.situacao_atividades = 2
         then
            ws_situacao_atividade_o := inter_fn_buscar_tag('lb29884#REABERTO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         INSERT INTO hist_100
            ( tabela,              operacao,
              data_ocorr,          aplicacao,
              usuario_rede,        maquina_rede,
              str01,               num05,
              long01
            )
         VALUES
            ( 'BASI_101',          'D',
              sysdate,             ws_aplicativo,
              ws_usuario_rede,     ws_maquina_rede,
              :old.codigo_projeto,  :old.sequencia_projeto,
              inter_fn_buscar_tag('lb34908#SUB-PROJETOS DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
              chr(10) || chr(10) ||
              inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
              || :old.codigo_projeto || chr(10) ||
              inter_fn_buscar_tag('lb05455#SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
              || :old.sequencia_projeto || chr(10) ||
              inter_fn_buscar_tag('lb01021#TIPO PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
              || :old.codigo_tipo_produto
              || ' - '
              || ws_tipo_produtol_o || chr(10) ||
              inter_fn_buscar_tag('lb34905#SEQUENCIA RELAC.:',ws_locale_usuario,ws_usuario_systextil) || ' '
              || :old.seq_projeto_pai   || chr(10) ||
              inter_fn_buscar_tag('lb00610#DATA CADASTRO:',ws_locale_usuario,ws_usuario_systextil) || ' '
              || to_char(:old.data_cadastro,'DD/MM/YYYY') || chr(10) ||
              inter_fn_buscar_tag('lb33002#DATA INICIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
              || to_char(:old.data_inicio,'DD/MM/YYYY') || chr(10) ||
              inter_fn_buscar_tag('lb33003#DATA TERMINO:',ws_locale_usuario,ws_usuario_systextil) || ' '
              || to_char(:old.data_termino,'DD/MM/YYYY') || chr(10) ||
              inter_fn_buscar_tag('lb34907#DIAS DE FOLGA:',ws_locale_usuario,ws_usuario_systextil) || ' '
              || :old.lead_time_folga   || chr(10) ||
              inter_fn_buscar_tag('lb34906#SITUACAO ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
              || ws_situacao_atividade_o
           );
   end if;
end inter_tr_basi_101_log;

-- ALTER TRIGGER "INTER_TR_BASI_101_LOG" ENABLE
 

/

exec inter_pr_recompile;

