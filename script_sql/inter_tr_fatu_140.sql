
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_140" 
   after update
       of cond_pgto_venda
   on fatu_140
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_descr_cond_pgto_o      varchar2(40);
   ws_descr_cond_pgto_n      varchar2(40);

   v_cond_pgto_venda         pedi_100.cond_pgto_venda%type;
   v_cond_pgto_venda_new     pedi_100.cond_pgto_venda%type;
   v_cond_pgto_venda_old     pedi_100.cond_pgto_venda%type;

   long_aux                  varchar2(2000);
   v_executa_trigger         number(1);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if  updating           and
         :new.num_pedido > 0 and
        (:old.cond_pgto_venda is null or
         :new.cond_pgto_venda is null or
         :old.cond_pgto_venda <> :new.cond_pgto_venda)
      then
         long_aux := long_aux ||
                    '                              '||
                    inter_fn_buscar_tag('lb40632#CAPA DO ORCAMENTO DE PRESTACAO DE SERVICO',ws_locale_usuario,ws_usuario_systextil) ||
                    chr(10)                    ||
                    'ORCAMENTO: '              || :new.num_orcamento ||
                    chr(10)                    ||
                    chr(10);

         if (:old.cond_pgto_venda is null or
             :new.cond_pgto_venda is null or
             :old.cond_pgto_venda <> :new.cond_pgto_venda)
         then
             begin
               select pedi_100.cond_pgto_venda
               into   v_cond_pgto_venda
               from pedi_100
               where pedi_100.pedido_venda = :new.num_pedido;
               exception when others
                 then v_cond_pgto_venda := 0;
             end;

             if :new.cond_pgto_venda is not null
             then v_cond_pgto_venda_new := :new.cond_pgto_venda;
             else v_cond_pgto_venda_new := v_cond_pgto_venda;
             end if;

             if :old.cond_pgto_venda is not null
             then v_cond_pgto_venda_old := :old.cond_pgto_venda;
             else v_cond_pgto_venda_old := v_cond_pgto_venda;
             end if;

             select nvl(pedi_070.descr_pg_cliente, ' ') into ws_descr_cond_pgto_o from pedi_070
             where pedi_070.cond_pgt_cliente = v_cond_pgto_venda_old;

             select nvl(pedi_070.descr_pg_cliente, ' ') into ws_descr_cond_pgto_n from pedi_070
             where pedi_070.cond_pgt_cliente = v_cond_pgto_venda_new;

             long_aux := long_aux ||
             inter_fn_buscar_tag('lb02139#CONDICAO PAGTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                      || v_cond_pgto_venda_old     || ' - '
                                      || ws_descr_cond_pgto_o      || ' -> ' ||
                                      chr(10);
             long_aux := long_aux     ||
                '                  '  || v_cond_pgto_venda_new     || ' - '
                                      || ws_descr_cond_pgto_n
                                      || chr(10);
         end if;

         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        aplicacao,
              usuario_rede,      maquina_rede,
              num01,             long01
            )
         VALUES
            ( 'PEDI_100',        'A',
              sysdate,           ws_aplicativo,
              ws_usuario_rede,   ws_maquina_rede,
              :new.num_pedido,   long_aux
           );
      end if;
   end if;
end inter_tr_fatu_140;

-- ALTER TRIGGER "INTER_TR_FATU_140" ENABLE
 

/

exec inter_pr_recompile;

