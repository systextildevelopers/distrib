
  CREATE OR REPLACE PROCEDURE "INTER_PR_INSERE_HIST_001" (p_codigo_empresa numeric,  p_nome_usuario varchar2,
                                                     p_codigo_usuario numeric,
                                                     p_nome_tabela    varchar2, p_seq_operacao numeric,

                                                     p_tipo_operacao  varchar2,
                                                                                p_nome_programa varchar2,
                                                     p_status_sistema numeric,  p_status_banco  varchar2,
                                                     p_instrucao      long) is
--
-- Finalidade: Inserir historico na hist_001
-- Autor.....: Jean Carlos
-- Data......: 21/01/09
--

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

BEGIN

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   begin
      insert into hist_001
                 (hist_001.codigo_empresa,         hist_001.nome_usuario,
                  hist_001.codigo_usuario,         hist_001.login_so,
                  hist_001.nome_tabela,            hist_001.seq_operacao,

                  hist_001.tipo_operacao,          hist_001.data_operacao,
                  hist_001.hora_operacao,          hist_001.nome_programa,
                  hist_001.status_sistema,         hist_001.status_banco,
                  hist_001.instrucao)
             values
                 (p_codigo_empresa,                p_nome_usuario,
                  p_codigo_usuario,                ws_usuario_rede,
                  p_nome_tabela,                   p_seq_operacao,

                  p_tipo_operacao,                 trunc(sysdate),
                  sysdate,                         p_nome_programa,
                  p_status_sistema,                p_status_banco,
                  p_instrucao);
   end;

   COMMIT;

EXCEPTION
   WHEN OTHERS THEN
      raise_application_error(-20000,'Erro na procedure inter_pr_insere_hist_001 ' || Chr(10) || SQLERRM);
END inter_pr_insere_hist_001;
 

/

exec inter_pr_recompile;

