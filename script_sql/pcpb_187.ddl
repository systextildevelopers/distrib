create table PCPB_187 (
ORDEM_PRODUCAO number(9) NOT NULL,
CODIGO_ROLO number(9) NOT NULL,
NIVEL varchar2(1) NOT NULL,
GRUPO varchar2(5) NOT NULL,
SUBGRUPO varchar2(3) NOT NULL,
ITEM varchar2(6) NOT NULL,
LOTE number(6) NOT NULL,
METROS number(9,3) NOT NULL
);

ALTER TABLE PCPB_187
ADD CONSTRAINT PK_PCPB_187 PRIMARY KEY (ORDEM_PRODUCAO, CODIGO_ROLO, NIVEL, GRUPO, SUBGRUPO, ITEM, LOTE);
