create table PROG_090
(
    CODIGO_VARIAVEL    NUMBER(4)    default 0  not null,
    LIMITE_MAXIMO      NUMBER(8, 3) default 0.0,
    LIMITE_MINIMO      NUMBER(8, 3) default 0.0,
    MQOP050_GRUMQ050   VARCHAR2(5)  default '' not null,
    MQOP050_ITEMQ050   VARCHAR2(6)  default '' not null,
    MQOP050_NIVMQ050   VARCHAR2(1)  default '' not null,
    MQOP050_NR_ALT     NUMBER(2)    default 0  not null,
    MQOP050_NR_ROT     NUMBER(2)    default 0  not null,
    MQOP050_SQ_OPER    NUMBER(4)    default 0  not null,
    MQOP050_SUBMQ050   VARCHAR2(3)  default '' not null,
    LIMITE_PADRAO      NUMBER(8, 3) default 0.0,
    OBSERVACAO         VARCHAR2(30) default '',
    NIVEL_VARIAVEL_OP  VARCHAR2(1)  default '' not null,
    GRUPO_VARIAVEL_OP  VARCHAR2(5)  default '' not null,
    SUBGRU_VARIAVEL_OP VARCHAR2(3)  default '' not null,
    ITEM_VARIAVEL_OP   VARCHAR2(6)  default '' not null,
    GRUPO_MAQUINA      VARCHAR2(4)  default '' not null,
    SUB_MAQUINA        VARCHAR2(3)  default '' not null,
    NUMERO_MAQUINA     NUMBER       default 0  not null
);
/

exec inter_pr_recompile;
