create table cpvd_135
(
  codigo_empresa           NUMBER(3) default 0 not null,
  pedido_venda             NUMBER(9) default 0 not null,
  data_eliminacao          DATE not null,
  seq_situacao     		   NUMBER(2) default 0 not null,
  codigo_situacao          NUMBER(2) default 0,
  data_situacao            DATE,
  flag_liberacao           VARCHAR2(1) default '',
  data_liberacao           DATE,
  responsavel              VARCHAR2(20) default '',
  observacao               VARCHAR2(2000) default ' ',
  usuario_bloqueio         VARCHAR2(15) default '',
  executa_trigger          NUMBER(1)
);

alter table CPVD_135
  add constraint PK_CPVD_135 primary key (codigo_empresa, pedido_venda, data_eliminacao, seq_situacao);

/

exec inter_pr_recompile;
