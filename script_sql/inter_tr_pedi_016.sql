create or replace trigger inter_tr_pedi_016
before insert on pedi_016 for each row
declare
    proximo_valor number;
begin
    if :new.id is null then
        select id_pedi_016.nextval into proximo_valor from dual;
        :new.id := proximo_valor;
    end if;
end inter_tr_pedi_016;
/
exec inter_pr_recompile;
