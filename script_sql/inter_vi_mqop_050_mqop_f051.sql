create or replace view inter_vi_mqop_050_mqop_f051 (
    nivel_estrutura,
    grupo_estrutura,
    subgru_estrutura,
    item_estrutura,
    numero_alternati,
    numero_roteiro,
    seq_operacao,
    codigo_operacao,
    codigo_estagio,
    centro_custo,
    sequencia_estagio,
    estagio_anterior,
    estagio_depende,
    ccusto_homem,
    cod_estagio_agrupador,
	
	ordem_producao,
	periodo_producao,
	ordem_confeccao,
	estagios_anteriores
) as

    select  
            mqop_050.nivel_estrutura,
            mqop_050.grupo_estrutura,
            mqop_050.subgru_estrutura,
            mqop_050.item_estrutura,
            mqop_050.numero_alternati,
            mqop_050.numero_roteiro,
            mqop_050.seq_operacao,
            mqop_050.codigo_operacao,
            mqop_050.codigo_estagio,
            mqop_050.centro_custo,
            mqop_050.sequencia_estagio,
            mqop_050.estagio_anterior,
            mqop_050.estagio_depende,
            mqop_050.ccusto_homem,
            mqop_050.cod_estagio_agrupador,

            cast(null as number(9))	as ordem_producao,
	        cast(null as number(4))	as periodo_producao,
	        cast(null as number(5))	as ordem_confeccao,
			
			mqop_050.estagios_anteriores

            --##############################################################################################################################################################
            --####                                                              ATENCAO!!!!                                                                             #### 
            --####                                                                                                                                                      #### 
            --####  SE INCLUIR OU ALTERAR OU REMOVER ALGUM CAMPO NESTA VIEW, DEVE FAZER O MESMO NA VIEW inter_vi_pcpc_040_mqop_f051 PARA AMBAS TEREM OS MESMOS CAMPOS   ####
            --##############################################################################################################################################################

    from mqop_050, mqop_040
    where mqop_040.codigo_operacao  = mqop_050.codigo_operacao
    and   mqop_040.pede_produto     <> 1

    and   not exists (  select 1
                        from mqop_045
                        where mqop_045.codigo_operacao = mqop_040.codigo_operacao
                    )

    --Nao trazer o estagio caso na seq_operacao anterior tenha sido o mesmo estagio que o atual (regra do mqop_f051)
    and 

        nvl(
                (
                    select m50_2.codigo_estagio     --Estagio da sequencia de operacao anterior a atual
                    from mqop_050 m50_2
                    where m50_2.nivel_estrutura     = mqop_050.nivel_estrutura
                    and   m50_2.grupo_estrutura     = mqop_050.grupo_estrutura
                    and   m50_2.subgru_estrutura    = mqop_050.subgru_estrutura
                    and   m50_2.item_estrutura      = mqop_050.item_estrutura
                    and   m50_2.numero_alternati    = mqop_050.numero_alternati
                    and   m50_2.numero_roteiro      = mqop_050.numero_roteiro
                    and   m50_2.seq_operacao        =   (
                                                            select max(m50_1.seq_operacao)      --Pega sequencia de operação anterior a atual
                                                            from mqop_050 m50_1
                                                            where m50_1.nivel_estrutura     = mqop_050.nivel_estrutura
                                                            and   m50_1.grupo_estrutura     = mqop_050.grupo_estrutura
                                                            and   m50_1.subgru_estrutura    = mqop_050.subgru_estrutura
                                                            and   m50_1.item_estrutura      = mqop_050.item_estrutura
                                                            and   m50_1.numero_alternati    = mqop_050.numero_alternati
                                                            and   m50_1.numero_roteiro      = mqop_050.numero_roteiro
                                                            and   m50_1.seq_operacao        < mqop_050.seq_operacao
                                                        )
                
                )

            , 0) <> mqop_050.codigo_estagio    --Codigo de estagio do atual tem que ser diferente do estagio da sequencia de operacao anterior (conforme mqop_f051)

;
