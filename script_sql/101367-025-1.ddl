alter table pcpc_045 add solicitacao_conserto number(9);
comment on column pcpc_045.solicitacao_conserto is 'N�mero da solicita��o de conserto em que o apontamento est� associado.';
alter table pcpc_045 modify solicitacao_conserto default(0);
exec inter_pr_recompile;
