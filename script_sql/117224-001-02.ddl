create table OBRF_827
( COD_EMPRESA          NUMBER(3)    default 0 not null,
  MES                  NUMBER(2)    default 0 not null,
  ANO                  NUMBER(4)    default 0 not null,
  COD_AJUSTE_OR        VARCHAR2(3) default ' ' not null,
  COD_RECEITA          VARCHAR2(40) default ' ' not null,
  ICMS_VALOR           NUMBER(13,2) default 0.00,
  DATA_VENCIMENTO      DATE,  
  ORIGEM_PROCESSO      VARCHAR2(1)  default ' ',
  NR_DOC_PROCESSO      VARCHAR2(40) default ' ',
  DESCR_PROCESSO       VARCHAR2(250) default ' ',
  DESCR_COMPLEMENTAR   VARCHAR2(250) default ' ');
  
alter table OBRF_827 add constraint PK_OBRF_827 primary key (COD_EMPRESA,MES,ANO,COD_AJUSTE_OR,COD_RECEITA);
 
create synonym systextilrpt.OBRF_827 for OBRF_827; 
