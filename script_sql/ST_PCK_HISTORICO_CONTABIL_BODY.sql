create or replace package body "ST_PCK_HISTORICO_CONTABIL" is

    FUNCTION busca_hist_contabil(p_codigo_historico NUMBER, p_msg_erro in out varchar2) RETURN t_dados_cont_010 
    AS
        row_var t_dados_cont_010;
    BEGIN
        begin
            SELECT *  
            BULK COLLECT into row_var
            FROM cont_010
            WHERE codigo_historico = p_codigo_historico;
        exception when others then
            p_msg_erro := 'Não foi possível buscar histórico contabil!. codigo_historico:' || p_codigo_historico;
        end;

        RETURN row_var;
    END;

    function exists_hist_contab(p_codigo_historico number) return boolean 
    as
        v_tmpInt number := 0;
    begin
        select NVL(MIN(1),0) 
        into v_tmpInt
        from cont_010
        where codigo_historico = p_codigo_historico 
        and ROWNUM = 1;

        return v_tmpInt = 1;
    end;

    function exists_by_sinal(p_codigo_historico number, p_sinal_titulo number) return boolean 
    as
        v_tmpInt number := 0;
    begin
        select NVL(MIN(1),0) 
        into v_tmpInt
        from cont_010
        where codigo_historico = p_codigo_historico
        and sinal_titulo = p_sinal_titulo
        and ROWNUM = 1;
        
        return v_tmpInt = 1;
    end;
end "ST_PCK_HISTORICO_CONTABIL";
