create or replace view inter_vi_grupo_emb_cod_sug_100 as
select pedidos.pedido_venda,
       pedidos.grupo_embarque,
       sum(pedidos.qtde_sugerida) qtde_sugerida
from
(select a.pedido_venda,
       b.grupo_embarque,
       a.cd_it_pe_nivel99,
       a.cd_it_pe_grupo,
       a.cd_it_pe_subgrupo,
       a.cd_it_pe_item,
       sum(a.qtde_pedida - a.qtde_faturada) qtde_saldo,
       sum(a.qtde_sugerida) qtde_sugerida,
       decode(sum(a.qtde_pedida - a.qtde_faturada),sum(a.qtde_sugerida),1,0) sugerido100
from pedi_110 a, basi_590 b
where a.cod_cancelamento = 0
  and b.nivel = a.cd_it_pe_nivel99
  and b.grupo = a.cd_it_pe_grupo
  and b.subgrupo = a.cd_it_pe_subgrupo
  and b.item = a.cd_it_pe_item
group by a.pedido_venda, a.cd_it_pe_nivel99, a.cd_it_pe_grupo, a.cd_it_pe_subgrupo, a.cd_it_pe_item, b.grupo_embarque
having sum(a.qtde_pedida - a.qtde_faturada) > 0) pedidos
group by pedidos.pedido_venda, pedidos.grupo_embarque
having min(pedidos.sugerido100) = 1;
