
  CREATE OR REPLACE PROCEDURE "INTER_PR_GERA_INTEGRACAO_PC" (p_cod_empresa    IN NUMBER,
                                                        p_cod_matriz     IN NUMBER,
                                                        p_gera_blocop    IN VARCHAR2,
                                                        p_des_erro      OUT varchar2) is

/*CURSOR u_obrf_235 (p_cod_empresa          NUMBER) IS
   select * from obrf_235@lnlproducao
   where obrf_235.cod_empresa = p_cod_empresa;

CURSOR u_sped_pc_c100 (p_cod_empresa          NUMBER) IS
   select * from sped_pc_c100@lnlproducao
   where sped_pc_c100.cod_empresa = p_cod_empresa;

CURSOR u_sped_pc_c170 (p_cod_empresa          NUMBER) IS
   select * from sped_pc_c170@lnlproducao
   where sped_pc_c170.cod_empresa = p_cod_empresa;

CURSOR u_sped_pc_0450 (p_cod_empresa          NUMBER) IS
   select * from sped_pc_0450@lnlproducao
   where sped_pc_0450.cod_empresa = p_cod_empresa;

CURSOR u_sped_pc_056 (p_cod_empresa          NUMBER) IS
   select * from sped_pc_056@lnlproducao
   where exists (select 1 from fatu_500@lnlproducao
                 where fatu_500.codigo_empresa = p_cod_empresa
                   and fatu_500.cgc_9 = sped_pc_056.num_cnpj9_empr
                   and fatu_500.cgc_4 = sped_pc_056.num_cnpj4_empr
                   and fatu_500.cgc_2 = sped_pc_056.num_cnpj2_empr);

CURSOR u_sped_pc_0000 (p_cod_empresa          NUMBER) IS
   select * from sped_pc_0000@lnlproducao
   where sped_pc_0000.cod_empresa = p_cod_empresa;

CURSOR u_sped_pc_0150 (p_cod_empresa          NUMBER) IS
   select * from sped_pc_0150@lnlproducao
   where sped_pc_0150.cod_empresa = p_cod_empresa;

CURSOR u_sped_pc_0190 (p_cod_empresa          NUMBER) IS
   select * from sped_pc_0190@lnlproducao
   where sped_pc_0190.cod_empresa = p_cod_empresa;

CURSOR u_sped_pc_0200 (p_cod_empresa          NUMBER) IS
   select * from sped_pc_0200@lnlproducao
   where sped_pc_0200.cod_empresa = p_cod_empresa;

CURSOR u_sped_pc_0400 (p_cod_empresa          NUMBER) IS
   select * from sped_pc_0400@lnlproducao
   where sped_pc_0400.cod_empresa = p_cod_empresa;

CURSOR u_sped_pc_0500 (p_cod_empresa          NUMBER) IS
   select * from sped_pc_0500@lnlproducao
   where sped_pc_0500.cod_empresa = p_cod_empresa;

CURSOR u_sped_pc_0600 (p_cod_empresa          NUMBER) IS
   select * from sped_pc_0600@lnlproducao
   where sped_pc_0600.cod_empresa = p_cod_empresa;

CURSOR u_sped_pc_hist (p_cod_empresa          NUMBER) IS
   select * from sped_pc_hist@lnlproducao
   where sped_pc_hist.cod_empresa = p_cod_empresa;*/

w_erro                    EXCEPTION;

BEGIN
   p_des_erro          := NULL;

   /*if p_gera_blocop <> 'S'
   then
      FOR sped_pc_c100 in u_sped_pc_c100(p_cod_empresa)
      LOOP
         begin
            insert into sped_pc_c100
              (cod_empresa,
               cod_matriz,
               num_cnpj9_empr,
               num_cnpj4_empr,
               num_cnpj2_empr,
               num_nota_fiscal,
               cod_serie_nota,
               num_cnpj_9,
               num_cnpj_4,
               num_cnpj_2,
               tip_entrada_saida,
               tip_emissao_prop,
               cod_modelo,
               cod_situacao_nota,
               num_nota_eletric,
               dat_saida,
               dat_emissao,
               val_nota,
               tip_pagto,
               val_descon,
               val_itens,
               tip_frete,
               val_frete,
               val_seguro,
               val_despesas,
               val_base_icms,
               val_icms,
               val_base_icms_sub,
               val_icms_sub,
               val_base_ipi,
               val_ipi,
               val_pis,
               val_cofins,
               val_pis_sub,
               val_cofins_sub,
               val_iss,
               num_doc_referen,
               cod_serie_doc_referen,
               sig_estado,
               cod_natur_oper,
               cod_cidade_ibge,
               num_cnpj_9_transp,
               num_cnpj_4_transp,
               num_cnpj_2_transp,
               num_serie_ecf,
               cod_impressora_fiscal,
               num_seq_modelo,
               tip_nota_cancel,
               val_base_pis_cofins,
               val_pis_ret,
               val_cofins_ret,
               ind_nf_servico,
               cod_mensagem)
               values
              (p_cod_empresa,
               p_cod_matriz,
               sped_pc_c100.num_cnpj9_empr,
               sped_pc_c100.num_cnpj4_empr,
               sped_pc_c100.num_cnpj2_empr,
               sped_pc_c100.num_nota_fiscal,
               sped_pc_c100.cod_serie_nota,
               sped_pc_c100.num_cnpj_9,
               sped_pc_c100.num_cnpj_4,
               sped_pc_c100.num_cnpj_2,
               sped_pc_c100.tip_entrada_saida,
               sped_pc_c100.tip_emissao_prop,
               sped_pc_c100.cod_modelo,
               sped_pc_c100.cod_situacao_nota,
               sped_pc_c100.num_nota_eletric,
               sped_pc_c100.dat_saida,
               sped_pc_c100.dat_emissao,
               sped_pc_c100.val_nota,
               sped_pc_c100.tip_pagto,
               sped_pc_c100.val_descon,
               sped_pc_c100.val_itens,
               sped_pc_c100.tip_frete,
               sped_pc_c100.val_frete,
               sped_pc_c100.val_seguro,
               sped_pc_c100.val_despesas,
               sped_pc_c100.val_base_icms,
               sped_pc_c100.val_icms,
               sped_pc_c100.val_base_icms_sub,
               sped_pc_c100.val_icms_sub,
               sped_pc_c100.val_base_ipi,
               sped_pc_c100.val_ipi,
               sped_pc_c100.val_pis,
               sped_pc_c100.val_cofins,
               sped_pc_c100.val_pis_sub,
               sped_pc_c100.val_cofins_sub,
               sped_pc_c100.val_iss,
               sped_pc_c100.num_doc_referen,
               sped_pc_c100.cod_serie_doc_referen,
               sped_pc_c100.sig_estado,
               sped_pc_c100.cod_natur_oper,
               sped_pc_c100.cod_cidade_ibge,
               sped_pc_c100.num_cnpj_9_transp,
               sped_pc_c100.num_cnpj_4_transp,
               sped_pc_c100.num_cnpj_2_transp,
               sped_pc_c100.num_serie_ecf,
               sped_pc_c100.cod_impressora_fiscal,
               sped_pc_c100.num_seq_modelo,
               sped_pc_c100.tip_nota_cancel,
               sped_pc_c100.val_base_pis_cofins,
               sped_pc_c100.val_pis_ret,
               sped_pc_c100.val_cofins_ret,
               sped_pc_c100.ind_nf_servico,
               sped_pc_c100.cod_mensagem);
         exception
             when DUP_VAL_ON_INDEX then
                p_des_erro := '';
             when others then
                p_des_erro := 'N�o inseriu dados na tabela sped_pc_c100' || Chr(10) || SQLERRM;
         end;

         commit;

      END LOOP;

      FOR sped_pc_c170 in u_sped_pc_c170(p_cod_empresa)
      LOOP
         begin
            insert into sped_pc_c170
              (cod_empresa,
               cod_matriz,
               num_nota_fiscal,
               cod_serie_nota,
               seq_item_nota,
               num_cnpj_9,
               num_cnpj_4,
               num_cnpj_2,
               tip_entrada_saida,
               cod_nivel,
               cod_grupo,
               cod_subgru,
               cod_item,
               des_item,
               qtde_item_fatur,
               cod_unid_medida,
               val_faturado,
               tip_transacao,
               val_unitario,
               val_desconto,
               val_contabil,
               cvf_icms,
               cod_nat_oper,
               num_cfop,
               sig_estado_oper,
               cod_mensagem_nat_oper,
               val_base_icms,
               perc_icms,
               val_icms,
               val_base_icms_sub_trib,
               perc_icms_sub_trib,
               val_base_red_sub_trib,
               val_icms_sub_trib,
               tip_apuracao,
               cod_enquadramento_ipi,
               val_base_ipi,
               perc_ipi,
               val_ipi,
               cvf_ipi,
               val_base_ipi_isento,
               val_base_ipi_outros,
               val_ipi_nao_debitado,
               val_basi_pis_cofins,
               val_pis,
               perc_pis,
               cvf_pis,
               cvf_cofins,
               perc_cofins,
               val_cofins,
               perc_iss,
               dat_emissao,
               tip_trib_federal,
               tip_trib_estadual,
               tip_trib_icms,
               val_iss,
               num_conta_contabil,
               cod_empresa_refer,
               num_nota_refer,
               cod_serie_refer,
               seq_nota_refer,
               num_cnpj9_refer,
               num_cnpj4_refer,
               num_cnpj2_refer,
               tipo_transacao,
               cod_transacao,
               cod_contabil,
               cod_mensagem_fiscal,
               cod_c_custo,
               cod_classific_fiscal,
               data_alter_cc,
               cod_nat_cc,
               ind_cta,
               nivel_cc,
               nome_cta,
               cod_cta_ref,
               cod_crec,
               cod_cont_crec,
               ind_nat_frt,
               cod_doc_imp,
               num_doc_imp,
               vl_pis_imp,
               vl_cofins_imp,
               num_acdraw,
               cod_modelo,
               cod_situacao_nota,
               nota_devolucao_compra)
            values
              (p_cod_empresa,
               p_cod_matriz,
               sped_pc_c170.num_nota_fiscal,
               sped_pc_c170.cod_serie_nota,
               sped_pc_c170.seq_item_nota,
               sped_pc_c170.num_cnpj_9,
               sped_pc_c170.num_cnpj_4,
               sped_pc_c170.num_cnpj_2,
               sped_pc_c170.tip_entrada_saida,
               sped_pc_c170.cod_nivel,
               sped_pc_c170.cod_grupo,
               sped_pc_c170.cod_subgru,
               sped_pc_c170.cod_item,
               sped_pc_c170.des_item,
               sped_pc_c170.qtde_item_fatur,
               sped_pc_c170.cod_unid_medida,
               sped_pc_c170.val_faturado,
               sped_pc_c170.tip_transacao,
               sped_pc_c170.val_unitario,
               sped_pc_c170.val_desconto,
               sped_pc_c170.val_contabil,
               sped_pc_c170.cvf_icms,
               sped_pc_c170.cod_nat_oper,
               sped_pc_c170.num_cfop,
               sped_pc_c170.sig_estado_oper,
               sped_pc_c170.cod_mensagem_nat_oper,
               sped_pc_c170.val_base_icms,
               sped_pc_c170.perc_icms,
               sped_pc_c170.val_icms,
               sped_pc_c170.val_base_icms_sub_trib,
               sped_pc_c170.perc_icms_sub_trib,
               sped_pc_c170.val_base_red_sub_trib,
               sped_pc_c170.val_icms_sub_trib,
               sped_pc_c170.tip_apuracao,
               sped_pc_c170.cod_enquadramento_ipi,
               sped_pc_c170.val_base_ipi,
               sped_pc_c170.perc_ipi,
               sped_pc_c170.val_ipi,
               sped_pc_c170.cvf_ipi,
               sped_pc_c170.val_base_ipi_isento,
               sped_pc_c170.val_base_ipi_outros,
               sped_pc_c170.val_ipi_nao_debitado,
               sped_pc_c170.val_basi_pis_cofins,
               sped_pc_c170.val_pis,
               sped_pc_c170.perc_pis,
               sped_pc_c170.cvf_pis,
               sped_pc_c170.cvf_cofins,
               sped_pc_c170.perc_cofins,
               sped_pc_c170.val_cofins,
               sped_pc_c170.perc_iss,
               sped_pc_c170.dat_emissao,
               sped_pc_c170.tip_trib_federal,
               sped_pc_c170.tip_trib_estadual,
               sped_pc_c170.tip_trib_icms,
               sped_pc_c170.val_iss,
               sped_pc_c170.num_conta_contabil,
               sped_pc_c170.cod_empresa_refer,
               sped_pc_c170.num_nota_refer,
               sped_pc_c170.cod_serie_refer,
               sped_pc_c170.seq_nota_refer,
               sped_pc_c170.num_cnpj9_refer,
               sped_pc_c170.num_cnpj4_refer,
               sped_pc_c170.num_cnpj2_refer,
               sped_pc_c170.tipo_transacao,
               sped_pc_c170.cod_transacao,
               sped_pc_c170.cod_contabil,
               sped_pc_c170.cod_mensagem_fiscal,
               sped_pc_c170.cod_c_custo,
               sped_pc_c170.cod_classific_fiscal,
               sped_pc_c170.data_alter_cc,
               sped_pc_c170.cod_nat_cc,
               sped_pc_c170.ind_cta,
               sped_pc_c170.nivel_cc,
               sped_pc_c170.nome_cta,
               sped_pc_c170.cod_cta_ref,
               sped_pc_c170.cod_crec,
               sped_pc_c170.cod_cont_crec,
               sped_pc_c170.ind_nat_frt,
               sped_pc_c170.cod_doc_imp,
               sped_pc_c170.num_doc_imp,
               sped_pc_c170.vl_pis_imp,
               sped_pc_c170.vl_cofins_imp,
               sped_pc_c170.num_acdraw,
               sped_pc_c170.cod_modelo,
               sped_pc_c170.cod_situacao_nota,
               sped_pc_c170.nota_devolucao_compra);
            exception
               when dup_val_on_index then
                  p_des_erro := '';
               when others then
                  p_des_erro := 'N�o inseriu dados na tabela sped_pc_c170' || Chr(10) || SQLERRM;
            end;
      END LOOP;

      FOR sped_pc_0450 in u_sped_pc_0450(p_cod_empresa)
      LOOP
         begin
            insert into sped_pc_0450
              (cod_empresa,
               cod_matriz,
               num_cnpj9_empr,
               num_cnpj4_empr,
               num_cnpj2_empr,
               cod_mensagem,
               des_mensagem,
               num_nota,
               cod_serie_nota,
               cnpj9,
               cnpj4,
               cnpj2,
               ind_entr_saida)
            values
              (p_cod_empresa,
               p_cod_matriz,
               sped_pc_0450.num_cnpj9_empr,
               sped_pc_0450.num_cnpj4_empr,
               sped_pc_0450.num_cnpj2_empr,
               sped_pc_0450.cod_mensagem,
               sped_pc_0450.des_mensagem,
               sped_pc_0450.num_nota,
               sped_pc_0450.cod_serie_nota,
               sped_pc_0450.cnpj9,
               sped_pc_0450.cnpj4,
               sped_pc_0450.cnpj2,
               sped_pc_0450.ind_entr_saida);
         exception
            when dup_val_on_index then
              p_des_erro := '';
         end;
      END LOOP;

      FOR sped_pc_056 in u_sped_pc_056(p_cod_empresa)
      LOOP
         begin
            insert into sped_pc_056
              (cod_matriz,
               num_cnpj9_empr,
               num_cnpj4_empr,
               num_cnpj2_empr,
               capa_ent_nrdoc,
               capa_ent_serie,
               capa_ent_forcli9,
               capa_ent_forcli4,
               capa_ent_forcli2,
               sequencia,
               tipo_declaracao,
               numero_di,
               data_declaracao,
               local_desembaraco,
               uf_desembaraco,
               data_desembaraco,
               codigo_exportador,
               numero_adicao,
               sequencia_adicao,
               codigo_fabricante,
               valor_desconto_item,
               base_calculo_ii,
               valor_desp_adu,
               valor_ii,
               valor_iof,
               valor_pis_imp,
               valor_cofins_imp,
               loc_exe_serv,
               base_pis_cofins)
            values
              (p_cod_matriz,
               sped_pc_056.num_cnpj9_empr,
               sped_pc_056.num_cnpj4_empr,
               sped_pc_056.num_cnpj2_empr,
               sped_pc_056.capa_ent_nrdoc,
               sped_pc_056.capa_ent_serie,
               sped_pc_056.capa_ent_forcli9,
               sped_pc_056.capa_ent_forcli4,
               sped_pc_056.capa_ent_forcli2,
               sped_pc_056.sequencia,
               sped_pc_056.tipo_declaracao,
               sped_pc_056.numero_di,
               sped_pc_056.data_declaracao,
               sped_pc_056.local_desembaraco,
               sped_pc_056.uf_desembaraco,
               sped_pc_056.data_desembaraco,
               sped_pc_056.codigo_exportador,
               sped_pc_056.numero_adicao,
               sped_pc_056.sequencia_adicao,
               sped_pc_056.codigo_fabricante,
               sped_pc_056.valor_desconto_item,
               sped_pc_056.base_calculo_ii,
               sped_pc_056.valor_desp_adu,
               sped_pc_056.valor_ii,
               sped_pc_056.valor_iof,
               sped_pc_056.valor_pis_imp,
               sped_pc_056.valor_cofins_imp,
               sped_pc_056.loc_exe_serv,
               sped_pc_056.base_pis_cofins);
         exception
             when dup_val_on_index then
                p_des_erro := '';
             when others then
                p_des_erro := 'N�o inseriu dados na tabela sped_pc_056' || Chr(10) || SQLERRM;
         end;

         commit;

      END LOOP;

      FOR sped_pc_0000 in u_sped_pc_0000(p_cod_empresa)
      LOOP
         begin
            insert into sped_pc_0000
              (cod_empresa,
               cod_matriz,
               num_cnpj9,
               num_cnpj4,
               num_cnpj2,
               nom_empresa,
               nom_fantasia,
               num_cep,
               des_endereco,
               num_endereco,
               des_compl_endereco,
               nom_bairro,
               num_ddd_empresa,
               num_fone,
               num_fax,
               des_email,
               sig_estado,
               num_inscri_estadual,
               cod_cidade,
               num_inscri_municipal,
               num_suframa,
               nom_contabilista,
               num_cpf_contabilista,
               num_reg_contabilista,
               num_cep_contabilista,
               des_ender_contabilista,
               num_ender_contabilista,
               des_compl_contabilista,
               des_bairro_contabilista,
               num_ddd_contabilista,
               num_fone_contabilista,
               num_fax_contabilista,
               des_email_contabilista,
               cod_cidade_contabilista,
               cod_inc_trib,
               ind_apro_cred)
            values
              (p_cod_empresa,
               p_cod_matriz,
               sped_pc_0000.num_cnpj9,
               sped_pc_0000.num_cnpj4,
               sped_pc_0000.num_cnpj2,
               sped_pc_0000.nom_empresa,
               sped_pc_0000.nom_fantasia,
               sped_pc_0000.num_cep,
               sped_pc_0000.des_endereco,
               sped_pc_0000.num_endereco,
               sped_pc_0000.des_compl_endereco,
               sped_pc_0000.nom_bairro,
               sped_pc_0000.num_ddd_empresa,
               sped_pc_0000.num_fone,
               sped_pc_0000.num_fax,
               sped_pc_0000.des_email,
               sped_pc_0000.sig_estado,
               sped_pc_0000.num_inscri_estadual,
               sped_pc_0000.cod_cidade,
               sped_pc_0000.num_inscri_municipal,
               sped_pc_0000.num_suframa,
               sped_pc_0000.nom_contabilista,
               sped_pc_0000.num_cpf_contabilista,
               sped_pc_0000.num_reg_contabilista,
               sped_pc_0000.num_cep_contabilista,
               sped_pc_0000.des_ender_contabilista,
               sped_pc_0000.num_ender_contabilista,
               sped_pc_0000.des_compl_contabilista,
               sped_pc_0000.des_bairro_contabilista,
               sped_pc_0000.num_ddd_contabilista,
               sped_pc_0000.num_fone_contabilista,
               sped_pc_0000.num_fax_contabilista,
               sped_pc_0000.des_email_contabilista,
               sped_pc_0000.cod_cidade_contabilista,
               sped_pc_0000.cod_inc_trib,
               sped_pc_0000.ind_apro_cred);
         exception
             when dup_val_on_index then
                p_des_erro := '';
             when others then
                p_des_erro := 'N�o inseriu dados na tabela sped_pc_0000' || Chr(10) || SQLERRM;
         end;
      END LOOP;

      FOR sped_pc_0150 in u_sped_pc_0150(p_cod_empresa)
      LOOP
         begin
            insert into sped_pc_0150
              (cod_empresa,
               cod_matriz,
               num_cnpj9,
               num_cnpj4,
               num_cnpj2,
               num_cnpj9_empr,
               num_cnpj4_empr,
               num_cnpj2_empr,
               nom_participante,
               cod_pais_particip,
               num_inscri_estadual,
               cod_cidade_ibge,
               num_suframa,
               des_endereco,
               num_endereco,
               des_complemento,
               nom_bairro,
               sig_estado_particip,
               cod_estado_ibge)
            values
              (sped_pc_0150.cod_empresa,
               sped_pc_0150.cod_matriz,
               sped_pc_0150.num_cnpj9,
               sped_pc_0150.num_cnpj4,
               sped_pc_0150.num_cnpj2,
               sped_pc_0150.num_cnpj9_empr,
               sped_pc_0150.num_cnpj4_empr,
               sped_pc_0150.num_cnpj2_empr,
               sped_pc_0150.nom_participante,
               sped_pc_0150.cod_pais_particip,
               sped_pc_0150.num_inscri_estadual,
               sped_pc_0150.cod_cidade_ibge,
               sped_pc_0150.num_suframa,
               sped_pc_0150.des_endereco,
               sped_pc_0150.num_endereco,
               sped_pc_0150.des_complemento,
               sped_pc_0150.nom_bairro,
               sped_pc_0150.sig_estado_particip,
               sped_pc_0150.cod_estado_ibge);
         exception
             when dup_val_on_index then
                p_des_erro := '';
             when others then
                p_des_erro := 'N�o inseriu dados na tabela sped_pc_0150' || Chr(10) || SQLERRM;
         end;

         commit;

      END LOOP;

      FOR sped_pc_0190 in u_sped_pc_0190(p_cod_empresa)
      LOOP
         begin
            insert into sped_pc_0190
              (cod_empresa,
               cod_matriz,
               num_cnpj9_empr,
               num_cnpj4_empr,
               num_cnpj2_empr,
               cod_unid_medida,
               des_unid_medida)
            values
              (sped_pc_0190.cod_empresa,
               sped_pc_0190.cod_matriz,
               sped_pc_0190.num_cnpj9_empr,
               sped_pc_0190.num_cnpj4_empr,
               sped_pc_0190.num_cnpj2_empr,
               sped_pc_0190.cod_unid_medida,
               sped_pc_0190.des_unid_medida);
         exception
             when dup_val_on_index then
                p_des_erro := '';
             when others then
                p_des_erro := 'N�o inseriu dados na tabela sped_pc_0190' || Chr(10) || SQLERRM;
         end;

         commit;

      END LOOP;


      FOR sped_pc_0200 in u_sped_pc_0200(p_cod_empresa)
      LOOP
         begin
            insert into sped_pc_0200
              (cod_empresa,
               cod_matriz,
               num_cnpj9_empr,
               num_cnpj4_empr,
               num_cnpj2_empr,
               cod_nivel,
               cod_grupo,
               cod_subgrupo,
               cod_item,
               des_narrativa,
               cod_barra,
               cod_unid_medida,
               tip_destinacao_item,
               num_classifi_fiscal,
               cod_export_ipi,
               cod_genero_item,
               per_icms,
               cod_lst)
            values
              (p_cod_empresa,
               p_cod_matriz,
               sped_pc_0200.num_cnpj9_empr,
               sped_pc_0200.num_cnpj4_empr,
               sped_pc_0200.num_cnpj2_empr,
               sped_pc_0200.cod_nivel,
               sped_pc_0200.cod_grupo,
               sped_pc_0200.cod_subgrupo,
               sped_pc_0200.cod_item,
               sped_pc_0200.des_narrativa,
               sped_pc_0200.cod_barra,
               sped_pc_0200.cod_unid_medida,
               sped_pc_0200.tip_destinacao_item,
               sped_pc_0200.num_classifi_fiscal,
               sped_pc_0200.cod_export_ipi,
               sped_pc_0200.cod_genero_item,
               sped_pc_0200.per_icms,
               sped_pc_0200.cod_lst);
         exception
             when dup_val_on_index then
                p_des_erro := '';
             when others then
                p_des_erro := 'N�o inseriu dados na tabela sped_pc_0200' || Chr(10) || SQLERRM;
         end;

         commit;

      END LOOP;

      FOR sped_pc_0400 in u_sped_pc_0400(p_cod_empresa)
      LOOP
         begin
            insert into sped_pc_0400
              (cod_empresa,
               cod_matriz,
               num_cnpj9_empr,
               num_cnpj4_empr,
               num_cnpj2_empr,
               cod_natureza,
               des_natureza)
            values
              (sped_pc_0400.cod_empresa,
               sped_pc_0400.cod_matriz,
               sped_pc_0400.num_cnpj9_empr,
               sped_pc_0400.num_cnpj4_empr,
               sped_pc_0400.num_cnpj2_empr,
               sped_pc_0400.cod_natureza,
               sped_pc_0400.des_natureza);
         exception
             when dup_val_on_index then
                p_des_erro := '';
             when others then
                p_des_erro := 'N�o inseriu dados na tabela sped_pc_0400' || Chr(10) || SQLERRM;
         end;

         commit;

      END LOOP;

      FOR sped_pc_0500 in u_sped_pc_0500(p_cod_empresa)
      LOOP
         begin
            insert into sped_pc_0500
              (cod_matriz,
               cod_empresa,
               num_cnpj_empr9,
               num_cnpj_empr4,
               num_cnpj_empr2,
               data_alter_cc,
               cod_nat_cc,
               ind_cta,
               nivel_cc,
               num_conta_contabil,
               cod_cta_ref,
               nome_cta)
            values
              (sped_pc_0500.cod_matriz,
               sped_pc_0500.cod_empresa,
               sped_pc_0500.num_cnpj_empr9,
               sped_pc_0500.num_cnpj_empr4,
               sped_pc_0500.num_cnpj_empr2,
               sped_pc_0500.data_alter_cc,
               sped_pc_0500.cod_nat_cc,
               sped_pc_0500.ind_cta,
               sped_pc_0500.nivel_cc,
               sped_pc_0500.num_conta_contabil,
               sped_pc_0500.cod_cta_ref,
               sped_pc_0500.nome_cta);
         exception
             when dup_val_on_index then
                p_des_erro := '';
             when others then
                p_des_erro := 'N�o inseriu dados na tabela sped_pc_0500' || Chr(10) || SQLERRM;
         end;
      END LOOP;

      FOR sped_pc_0600 in u_sped_pc_0600(p_cod_empresa)
      LOOP
         begin
            insert into sped_pc_0600
              (cod_matriz,
               cod_empresa,
               num_cnpj_empr9,
               num_cnpj_empr4,
               num_cnpj_empr2,
               centro_custo,
               descricao,
               data_cadastro)
            values
              (sped_pc_0600.cod_matriz,
               sped_pc_0600.cod_empresa,
               sped_pc_0600.num_cnpj_empr9,
               sped_pc_0600.num_cnpj_empr4,
               sped_pc_0600.num_cnpj_empr2,
               sped_pc_0600.centro_custo,
               sped_pc_0600.descricao,
               sped_pc_0600.data_cadastro);
         exception
             when dup_val_on_index then
                p_des_erro := '';
             when others then
                p_des_erro := 'N�o inseriu dados na tabela sped_pc_0600' || Chr(10) || SQLERRM;
         end;
      END LOOP;

      FOR sped_pc_hist in u_sped_pc_hist(p_cod_empresa)
      LOOP
         begin
            insert into sped_pc_hist
              (cod_matriz,
               cod_empresa,
               num_cnpj9_empr,
               num_cnpj4_empr,
               num_cnpj2_empr,
               nivel,
               grupo,
               subgrupo,
               item,
               narrativa_anterior,
               data_alteracao,
               data_alteracao_ant)
            values
              (sped_pc_hist.cod_matriz,
               sped_pc_hist.cod_empresa,
               sped_pc_hist.num_cnpj9_empr,
               sped_pc_hist.num_cnpj4_empr,
               sped_pc_hist.num_cnpj2_empr,
               sped_pc_hist.nivel,
               sped_pc_hist.grupo,
               sped_pc_hist.subgrupo,
               sped_pc_hist.item,
               sped_pc_hist.narrativa_anterior,
               sped_pc_hist.data_alteracao,
               sped_pc_hist.data_alteracao_ant);
         exception
             when dup_val_on_index then
                p_des_erro := '';
             when others then
                p_des_erro := 'N�o inseriu dados na tabela sped_pc_hist' || Chr(10) || SQLERRM;
         end;
      END LOOP;
   end if;

   FOR obrf_235 in u_obrf_235(p_cod_empresa)
   LOOP
      begin
         insert into obrf_235
           (cod_matriz,
            cod_empresa,
            mes_apur,
            ano_apur,
            vl_rec_tot_estq,
            cod_ativ_econ,
            vl_rec_ativ_estab,
            vl_exc,
            vl_bc_cont,
            aliq_cont,
            vl_cont_apu,
            cod_cta,
            info_compl,
            cod_rec)
         values
           (obrf_235.cod_matriz,
            obrf_235.cod_empresa,
            obrf_235.mes_apur,
            obrf_235.ano_apur,
            obrf_235.vl_rec_tot_estq,
            obrf_235.cod_ativ_econ,
            obrf_235.vl_rec_ativ_estab,
            obrf_235.vl_exc,
            obrf_235.vl_bc_cont,
            obrf_235.aliq_cont,
            obrf_235.vl_cont_apu,
            obrf_235.cod_cta,
            obrf_235.info_compl,
            obrf_235.cod_rec);
      exception
          when dup_val_on_index then
             p_des_erro := '';
          when others then
             p_des_erro := 'N�o inseriu dados na tabela sped_pc_hist' || Chr(10) || SQLERRM;
      end;
   END LOOP;   */

END inter_pr_gera_integracao_pc;

 

/

exec inter_pr_recompile;

