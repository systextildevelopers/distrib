create or replace procedure INTER_PR_CONTABILIZACAO_PRG_03(p_cod_empresa       IN NUMBER,
                                                      p_c_custo          IN NUMBER,
                                                      p_cnpj9            IN NUMBER,
                                                      p_cnpj4            IN NUMBER,
                                                      p_cnpj2            IN NUMBER,
                                                      p_transacao        IN OUT NUMBER,
                                                      p_codigo_historico IN NUMBER,
                                                      p_conta            IN NUMBER,
                                                      p_cod_contab_deb   IN NUMBER,
                                                      p_cod_contab_cre   IN NUMBER,
                                                      p_documento        IN NUMBER,
                                                      p_data_transacao    IN DATE,
                                                      p_valor            IN NUMBER,
                                                      p_origem           IN NUMBER,
                                                      p_tp_cta_deb       IN NUMBER,
                                                      p_tp_cta_cre       IN NUMBER,
                                                      p_prg_gerador      IN VARCHAR2,
                                                      p_usuario          IN VARCHAR2,
                                                      p_compl_historico  IN VARCHAR2,
                                                      p_tipo_titulo      IN NUMBER,
                                                      p_tp_insere_estorno IN NUMBER,
                                                      p_num_lcto         IN OUT number,
                                                      p_des_erro          OUT varchar2
                                                      ) AS

    v_empresa_matriz number;
    v_transacao number;
    v_tp_cta_cre number;
    v_exercicio number(4);
    v_codigo_historico number;
    v_exercicio_doc number(4);
    cta_debito number;
    cta_credito number;
     v_compl_histor1 varchar2(100);
    v_valor_deb  number;
    v_valor_cred number;
    v_gera_contab_func number;
    v_agrupa_lanc_cont_chave varchar2(1);
    v_atualiza_contabi estq_005.atualiza_contabi%type;

BEGIN
    DBMS_OUTPUT.put_line('entrou contabil');
    BEGIN
       v_codigo_historico := p_codigo_historico;

       begin
          select fatu_500.codigo_matriz, fatu_500.gera_contabil
          into v_empresa_matriz, v_gera_contab_func
          from fatu_500
          where fatu_500.codigo_empresa = p_cod_empresa;
      exception
          when no_data_found then
          v_empresa_matriz := 0;
      end;
 DBMS_OUTPUT.put_line('v_gera_contab_func ' || v_gera_contab_func);
       IF v_empresa_matriz > 0 and v_gera_contab_func = 1
       THEN

           v_exercicio_doc := inter_fn_checa_data(v_empresa_matriz, p_data_transacao, 0);
           v_exercicio := inter_fn_checa_data(v_empresa_matriz, p_data_transacao,0);
            DBMS_OUTPUT.put_line('exercicio ' || v_exercicio);
           if v_exercicio_doc < 0
           then
               v_exercicio_doc := v_exercicio;
           end if;

           if v_exercicio <= 0
           then
               p_des_erro := 'Exercicio e/ou periodo contabil nao encontrado ou fechado.
                             Nao sera permitido realizar movimentacoes nesta data. Contate o contador. '
                             ||' Data transacao: '|| p_data_transacao
                             ||' Exercicio: '|| v_exercicio
                             ||' Exercicio doc: '|| v_exercicio_doc;
               return;
           end if;

           IF v_exercicio > 0 THEN


               --Se for a origem 6 a transac?o vai ser 0.
               --Passar o campo p_tp_cta_cre 20
               if p_tp_cta_deb = 20 then
                    v_transacao := 0;
               else
                    v_transacao := p_transacao;
               end if;

                cta_debito := ST_PCK_ENCONTRA_CONTA.executar(p_prg_gerador,            p_cod_empresa,
                        p_data_transacao,               p_tp_cta_deb,
                        p_cod_contab_deb,          v_transacao,
                        p_c_custo,               p_data_transacao,
                        p_cod_empresa,           p_usuario,
                        0, p_des_erro);

                DBMS_OUTPUT.put_line('cta_debito ' || cta_debito);

               if trim(p_des_erro) is not null or cta_debito <= 0
               then
                  return;
               end if;


               v_valor_deb   := p_valor;
               v_valor_cred  := p_valor;

               v_tp_cta_cre := p_tp_cta_cre; -- fica com o que veio

               if p_tp_cta_deb = 20 then
                  BEGIN
                     select t.atualiza_contabi into v_atualiza_contabi from estq_005 t where t.codigo_transacao = p_transacao;
                    DBMS_OUTPUT.put_line('v_atualiza_contabi ' || v_atualiza_contabi);

                     exception
                     when no_data_found then
                     v_atualiza_contabi := 2;
                  END;
                  if v_atualiza_contabi <> 1
                  then
                     v_tp_cta_cre := 5;
                  else
                     v_tp_cta_cre := p_tp_cta_cre; -- fica com o que veio
                  end if;
               end if;

               -- para estorno adiantamento
                cta_credito := ST_PCK_ENCONTRA_CONTA.executar(p_prg_gerador,            p_cod_empresa,
                                        p_data_transacao,               v_tp_cta_cre,
                                        p_cod_contab_cre,          p_transacao,
                                        p_c_custo,               p_data_transacao,
                                        p_cod_empresa,           p_usuario,
                                        0, p_des_erro);


                DBMS_OUTPUT.put_line('cta_credito ' || cta_credito);

               if trim(p_des_erro) is not null or cta_credito <= 0
               then
                  return;
               end if;



               if cta_debito > 0 and cta_credito > 0 --(cta_credito > 0 or v_valor_cred = 0)
               then
                  if p_num_lcto = 0
                  then
                     if v_agrupa_lanc_cont_chave = 'S'
                     then
                         begin
                            select val_str
                            into v_agrupa_lanc_cont_chave
                            from EMPR_008
                            where PARAM = 'contab.agrupaLancamentoContabilChave'
                            and CODIGO_EMPRESA = p_cod_empresa;
                         exception when others then
                             v_agrupa_lanc_cont_chave := 'N';
                         end;

                         if v_agrupa_lanc_cont_chave = 'N' and p_num_lcto > 0 then
                             p_num_lcto := 0;
                         end if;
                     end if;

                  end if;

                  if trim(p_compl_historico) is null
                  then
                     v_compl_histor1 := p_documento;
                  else
                     v_compl_histor1 := p_compl_historico;
                  end if;

                  inter_pr_gera_lanc_cont_prg(p_cod_empresa,
                                        p_origem,
                                        p_num_lcto,
                                        p_c_custo,
                                        p_data_transacao,
                                        v_codigo_historico,
                                        v_compl_histor1,
                                        p_tp_insere_estorno,
                                        p_transacao,
                                        cta_debito,
                                        v_valor_deb,
                                        cta_credito,
                                        v_valor_cred,
                                        0,
                                        p_conta,
                                        p_data_transacao,
                                        p_documento,
                                        p_cnpj9,
                                        p_cnpj4,
                                        p_cnpj2,
                                        2,
                                        p_documento,
                                        '1',
                                        p_tipo_titulo,
                                        1,
                                        0,
                                        0,
                                        0,
                                        0,
                                        p_prg_gerador,
                                        p_usuario,
                                        p_des_erro);
                                                        DBMS_OUTPUT.put_line('p_num_lcto ' || p_num_lcto);

               end if;
           END IF;
       END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
           raise_application_error(-20001, 'Erro ao processar inter_pr_contabilizacao ' ||
                                           ' p_cod_empresa ' || p_cod_empresa ||
                                           ' p_origem ' || 0 ||
                                           ' p_num_lcto ' || p_num_lcto ||
                                           ' p_c_custo ' || p_c_custo ||
                                           ' v_codigo_historico ' || v_codigo_historico ||
                                           ' v_compl_histor1 ' || v_compl_histor1 ||
                                           ' p_transacao ' || p_transacao ||
                                           ' cta_debito ' || cta_debito ||
                                           ' cta_credito ' || cta_credito ||
                                           ' p_valor ' || p_valor ||
                                           ' p_conta ' || p_conta ||
                                           ' p_data_transacao ' || p_data_transacao ||
                                           ' p_documento ' || p_documento ||
                                           ' p_cnpj9 ' || p_cnpj9 ||
                                           ' p_cnpj4 ' || p_cnpj4 ||
                                           ' p_cnpj2 ' || p_cnpj2
           || SQLERRM);
    END;
END INTER_PR_CONTABILIZACAO_PRG_03;
/
