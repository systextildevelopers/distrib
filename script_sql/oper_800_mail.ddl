CREATE TABLE oper_800_mail
( cod_empresa                   NUMBER(3) default 0 not null,
  cod_processo                  NUMBER(5) default 0 not null,
  nome_processo					VARCHAR2(60),
  
  tipo_processo					VARCHAR2(50),
  
  opcao_conta_envio_email		NUMBER(1),
  email_remetente				VARCHAR2(100),
  email_id_remetente			VARCHAR2(100),
  senha_email_remetente			VARCHAR2(50),
  
  assunto_email					VARCHAR2(60),
  emails_com_copia				VARCHAR2(500),
  endereco_img_cabecalho		VARCHAR2(4000),
  
  corpo_email					CLOB,
  endereco_img_rodape			VARCHAR2(4000)
);
  
COMMENT ON TABLE oper_800_mail IS 'Cadastro de Emails de Processos';
  
COMMENT ON COLUMN oper_800_mail.cod_empresa      			IS 'Código da empresa';
COMMENT ON COLUMN oper_800_mail.cod_processo     			IS 'Código do processo';

COMMENT ON COLUMN oper_800_mail.nome_processo     			IS 'Nome do processo';
COMMENT ON COLUMN oper_800_mail.tipo_processo     			IS 'Tipo do processo (ex.: Imagens por email)';

COMMENT ON COLUMN oper_800_mail.opcao_conta_envio_email     IS 'Qual conta de email utilizar (1: do cadastro do usuário; 2: conta informada neste processo)';
COMMENT ON COLUMN oper_800_mail.email_remetente      		IS 'Endereço de email do remetente';
COMMENT ON COLUMN oper_800_mail.email_id_remetente      	IS 'ID do email do remetente';
COMMENT ON COLUMN oper_800_mail.senha_email_remetente      	IS 'Senha do email do remetente';
COMMENT ON COLUMN oper_800_mail.assunto_email      			IS 'Assunto do email';
COMMENT ON COLUMN oper_800_mail.emails_com_copia      		IS 'Endereços de email para enviar com cópia (CC)';
COMMENT ON COLUMN oper_800_mail.endereco_img_cabecalho      IS 'Endereço da imagem para exibir no início do corpo do email';
COMMENT ON COLUMN oper_800_mail.corpo_email      			IS 'Corpo do email';
COMMENT ON COLUMN oper_800_mail.endereco_img_rodape      	IS 'Endereço da imagem para exibir no final do corpo do email';

alter table oper_800_mail add constraint pk_oper_800_mail primary key (cod_empresa, cod_processo);

exec inter_pr_recompile;
