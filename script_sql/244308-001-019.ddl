CREATE TABLE  "PROJ_102" 
   (	"SEQ_DISTRIBUICAO" NUMBER, 
	"CODIGO_EMPRESA" NUMBER(3,0), 
	"SEQ_EXECUCAO" NUMBER, 
	"MEMORIA_CALCULO" VARCHAR2(4000), 
	 CONSTRAINT "PK_PROJ_102" PRIMARY KEY ("CODIGO_EMPRESA", "SEQ_DISTRIBUICAO", "SEQ_EXECUCAO")
  USING INDEX  ENABLE
   )
/
ALTER TABLE  "PROJ_102" ADD CONSTRAINT "FK_PROJ_100" FOREIGN KEY ("SEQ_DISTRIBUICAO", "CODIGO_EMPRESA")
	  REFERENCES  "PROJ_100" ("SEQ_DISTRIBUICAO", "CODIGO_EMPRESA") ENABLE
/
