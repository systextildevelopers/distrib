
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_021_1" 
  before insert or
         update of tamanho,
                   quantidade
  on pcpc_021
  for each row
declare
  v_referencia  varchar2(5);
  v_executa_trigger  number;
begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if  inserting
   or  updating
   and :new.executa_trigger = 0
   then
      v_referencia := '00000';

      begin
         select pcpc_020.referencia_peca
         into v_referencia
         from pcpc_020
         where pcpc_020.ordem_producao = :new.ordem_producao;
      exception when no_data_found then
         v_referencia := '00000';
      end;

      begin
         :new.sequencia_tamanho := inter_fn_busca_seq_tamanho('1',v_referencia,:new.tamanho);
      end;
   end if;

   if  inserting
   or  updating
   and v_executa_trigger         =  0
   and :new.qtde_programada_orig =  0
   and :old.quantidade           <> 0
   and :new.quantidade           <> 0
   and :old.quantidade           <> :new.quantidade
   then
      :new.qtde_programada_orig := :old.quantidade;
   end if;

   if :new.qtde_programada_orig is null
   then
      :new.qtde_programada_orig := 0;
   end if;

   INTER_PR_MARCA_REFERENCIA_OP('0', '00000', '000', '000000',
                                :new.ordem_producao,
                                1);

end inter_tr_pcpc_021_1;

-- ALTER TRIGGER "INTER_TR_PCPC_021_1" ENABLE
 

/

exec inter_pr_recompile;

