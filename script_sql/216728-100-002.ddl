insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('cont_f658', 'Estorno de Lançamentos Contábeis', 0, 0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'cont_f658', ' ', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'cont_f658', ' ', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Estorno de Lançamentos Contábeis'
where hdoc_036.codigo_programa = 'cont_f658'
  and hdoc_036.locale          = 'pt_BR';

commit;
