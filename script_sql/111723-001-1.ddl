alter table fatu_504 add conf_rolo_baixa_separacao varchar2(1) default 'N';

comment on column fatu_504.conf_rolo_baixa_separacao is 'Estágio que irá verificar os rolos que foram alocados.';
