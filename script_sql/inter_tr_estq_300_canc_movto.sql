create or replace trigger inter_tr_estq_300_canc_movto
before delete on estq_300
for each row

declare

v_entrada_saida estq_005.entrada_saida%type;
ws_usuario_rede           varchar2(20);
ws_maquina_rede           varchar2(40);
ws_aplicativo             varchar2(20);
ws_sid                    number(9);
ws_empresa                number(3);
ws_usuario_systextil      varchar2(250);
ws_locale_usuario         varchar2(5);
v_nome_programa varchar2(20);
v_gera_estq_negativo_1     number;
v_gera_estq_negativo_2     number;
v_gera_estq_negativo_4     number;
v_gera_estq_negativo_7     number;
v_gera_estq_negativo_9     number;
v_gera_estq_negativo       number;
v_estoque_total            number;

begin
   
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   v_nome_programa := inter_fn_nome_programa(ws_sid);

   if v_nome_programa = 'estq_f015'
   then
   
      begin
          -- verifica parametro se permite deixar o estoque negativo para o deposito/nivel
         select nvl(hdoc_001.campo_numerico01,0),      nvl(hdoc_001.campo_numerico02,0),
                nvl(hdoc_001.campo_numerico03,0),      nvl(hdoc_001.campo_numerico04,0),
                nvl(hdoc_001.campo_numerico05,0)
         into   v_gera_estq_negativo_1,         v_gera_estq_negativo_2,
                v_gera_estq_negativo_4,         v_gera_estq_negativo_7,
                v_gera_estq_negativo_9
         from hdoc_001
         where tipo   = 81
           and codigo = :old.codigo_deposito;
           
      exception
         when no_data_found
         then begin
            -- verifica parametro se permite deixar o estoque negativo para o deposito/nivel
            select nvl(hdoc_001.campo_numerico01,0),      nvl(hdoc_001.campo_numerico02,0),
                   nvl(hdoc_001.campo_numerico03,0),      nvl(hdoc_001.campo_numerico04,0),
                   nvl(hdoc_001.campo_numerico05,0)
            into   v_gera_estq_negativo_1,         v_gera_estq_negativo_2,
                   v_gera_estq_negativo_4,         v_gera_estq_negativo_7,
                   v_gera_estq_negativo_9
            from hdoc_001
            where tipo   = 81
              and codigo = 0;

            exception
               when no_data_found then
                  v_gera_estq_negativo_1 := 0;
                  v_gera_estq_negativo_2 := 0;
                  v_gera_estq_negativo_4 := 0;
                  v_gera_estq_negativo_7 := 0;
                  v_gera_estq_negativo_9 := 0;
         end;
      end;
      
      v_gera_estq_negativo := 0;

      if :old.nivel_estrutura = '1'
      then
         v_gera_estq_negativo := v_gera_estq_negativo_1;
      end if;

      if :old.nivel_estrutura = '2'
      then
         v_gera_estq_negativo := v_gera_estq_negativo_2;
      end if; 

      if :old.nivel_estrutura = '4'
      then
         v_gera_estq_negativo := v_gera_estq_negativo_4;
      end if;

      if :old.nivel_estrutura = '7'
      then
         v_gera_estq_negativo := v_gera_estq_negativo_7;
      end if;

      if :old.nivel_estrutura = '9'
      then
         v_gera_estq_negativo := v_gera_estq_negativo_9;
      end if;
      
      if v_gera_estq_negativo = 1
      then
         if (:old.entrada_saida = 'S' /*and :old.flag_elimina = 0*/)
         or (:old.entrada_saida = 'E' /*and :old.flag_elimina = 1*/)
         then

            begin
               select qtde_estoque_atu
               into   v_estoque_total
               from estq_040
               where cditem_nivel99  = :old.nivel_estrutura
               and   cditem_grupo    = :old.grupo_estrutura
               and   cditem_subgrupo = :old.subgrupo_estrutura
               and   cditem_item     = :old.item_estrutura
               and   lote_acomp      = :old.numero_lote
               and   deposito        = :old.codigo_deposito;

            exception
               when NO_DATA_FOUND then
                  v_estoque_total := 0.00;
            end;

            if (v_estoque_total - :old.quantidade) < 0.00
            then
               raise_application_error(-20101,
                  inter_fn_buscar_tag('ds26564#ATENCAO! Estoque insuficiente para a movimentacao.',ws_locale_usuario,ws_usuario_systextil) ||
                  chr(10)                                                                                                                  ||
                  inter_fn_buscar_tag('lb08048#DEPOSITO:',ws_locale_usuario,ws_usuario_systextil)                                          ||
                  to_char(:old.codigo_deposito, '000')                                                                                        ||
                  chr(10)                                                                                                                  ||
                  inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)                                           ||
                  :old.nivel_estrutura     || '.'                                                                                             ||
                  :old.grupo_estrutura     || '.'                                                                                             ||
                  :old.subgrupo_estrutura  || '.'                                                                                             ||
                  :old.item_estrutura                                                                                                         ||
                  chr(10)                                    ||
                  inter_fn_buscar_tag('lb00236#LOTE:',ws_locale_usuario,ws_usuario_systextil)                                              ||
                  to_char(:old.numero_lote, '000000'));
            end if;
         end if;
      end if;
   
      select estq_005.entrada_saida
      into v_entrada_saida
      from estq_005
      where estq_005.codigo_transacao = :old.codigo_transacao;
      
      if (v_entrada_saida = 'T') 
      then
         if (:old.entrada_saida = 'S')
         then
            update estq_040
            set estq_040.qtde_estoque_atu  = estq_040.qtde_estoque_atu + :old.quantidade
            where estq_040.cditem_nivel99  = :old.nivel_estrutura
              and estq_040.cditem_grupo    = :old.grupo_estrutura
              and estq_040.cditem_subgrupo = :old.subgrupo_estrutura
              and estq_040.cditem_item     = :old.item_estrutura
              and estq_040.deposito        = :old.codigo_deposito
              and estq_040.lote_acomp      = :old.numero_lote;
         else
            update estq_040
            set estq_040.qtde_estoque_atu  = estq_040.qtde_estoque_atu - :old.quantidade
            where estq_040.cditem_nivel99  = :old.nivel_estrutura
              and estq_040.cditem_grupo    = :old.grupo_estrutura
              and estq_040.cditem_subgrupo = :old.subgrupo_estrutura
              and estq_040.cditem_item     = :old.item_estrutura
              and estq_040.deposito        = :old.codigo_deposito
              and estq_040.lote_acomp      = :old.numero_lote;
         end if;
      else
         if (v_entrada_saida = 'S')
         then
            update estq_040
            set estq_040.qtde_estoque_atu  = estq_040.qtde_estoque_atu + :old.quantidade
            where estq_040.cditem_nivel99  = :old.nivel_estrutura
              and estq_040.cditem_grupo    = :old.grupo_estrutura
              and estq_040.cditem_subgrupo = :old.subgrupo_estrutura
              and estq_040.cditem_item     = :old.item_estrutura
              and estq_040.deposito        = :old.codigo_deposito
              and estq_040.lote_acomp      = :old.numero_lote;
         else
            if (v_entrada_saida = 'E') 
            then
               update estq_040
               set estq_040.qtde_estoque_atu  = estq_040.qtde_estoque_atu - :old.quantidade
               where estq_040.cditem_nivel99  = :old.nivel_estrutura
                 and estq_040.cditem_grupo    = :old.grupo_estrutura
                 and estq_040.cditem_subgrupo = :old.subgrupo_estrutura
                 and estq_040.cditem_item     = :old.item_estrutura
                 and estq_040.deposito        = :old.codigo_deposito
                 and estq_040.lote_acomp      = :old.numero_lote;
            end if;
         end if;
      end if;
   end if;
end;

/

exec inter_pr_recompile;
