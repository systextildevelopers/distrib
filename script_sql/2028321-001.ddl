CREATE TABLE SUPR_061 (
	REQ_COMPRA				NUMBER(6) NOT NULL,
	SEQ_REQ_COMPRA 			NUMBER(2) NOT NULL,
	PEDIDO_VENDA 			NUMBER(6) DEFAULT 0 NOT NULL 
);

ALTER TABLE SUPR_061
ADD CONSTRAINT PK_SUPR_061 PRIMARY KEY (REQ_COMPRA, SEQ_REQ_COMPRA);

COMMENT ON COLUMN SUPR_061.REQ_COMPRA IS 'Requisição de compra que é alimentado pela tela (tmrp_f763)';
COMMENT ON COLUMN SUPR_061.SEQ_REQ_COMPRA IS 'Sequência da requisição de compra que vem é alimentado pela tela (tmrp_f763)';
COMMENT ON COLUMN SUPR_061.PEDIDO_VENDA IS 'Pedido de venda que foi originado a requisição de compra (tmrp_f763)';

/

exec inter_pr_recompile;
