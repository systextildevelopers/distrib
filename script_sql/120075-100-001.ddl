alter table fatu_504 add consiste_lote_entradas number(1) default 0;

comment on column fatu_504.consiste_lote_entradas is '0: Nao consiste o lote nas entradas de documentos. 1: Fazer consistência do lote nas entradas de documentos (obrf_f015, obrf_f055, obrf_f885).';

exec inter_pr_recompile;
