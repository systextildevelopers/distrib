alter table i_obrf_015 add
produto_forn varchar2(200) default '';

alter table i_obrf_015 add
descricao_item_forn varchar2(70) default '';

alter table i_obrf_015 add
quantidade_forn number(14,3) default 0.00;

alter table i_obrf_015 add
unidade_medida_forn varchar2(2) default '';

alter table i_obrf_015 add
valor_unitario_forn number(15,5) default 0.00;

alter table i_obrf_015 add
valor_total_forn number(15,2) default 0.00;

alter table i_obrf_015 add
cfop varchar2(10) default '';

exec inter_pr_recompile;
