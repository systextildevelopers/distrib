create table pcpt_085_hist (
codigo_rolo number(9) not null,
usuario varchar2(15) not null,
perc_variacao number(5,2) not null,
perc_usuario number(5,2)not null,
PRIMARY KEY (codigo_rolo)
);

comment on table pcpt_085_hist is 'Tabela para controle de liberação de pesagem pela oper_f085 por Percentual de Alçada do usuário';
comment on column pcpt_085_hist.perc_variacao is 'Percentual de variação liberada pelo usuário para rolo';
comment on column pcpt_085_hist.perc_usuario is 'Percentual de Alçada do usuário';
