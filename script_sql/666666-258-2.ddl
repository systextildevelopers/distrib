declare
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'cobr_f002';

   v_usuario varchar2(15);
   
begin
   for reg_programa in programa
   loop
      begin
         
         insert into hdoc_033
         (usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
         values
         (reg_programa.usu_prg_cdusu, reg_programa.usu_prg_empr_usu, 'cobr_f004', 'pedi_menu', 1, 0, 'S', 'S', 'S', 'S');
         
         commit;
      end;
   end loop;
   
  update hdoc_033
  set hdoc_033.nome_menu = ' ', 
      hdoc_033.item_menu = 0
  where hdoc_033.programa = 'cobr_f002';
  
  commit;
end;
