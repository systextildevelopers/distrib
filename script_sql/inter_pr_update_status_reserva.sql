
  CREATE OR REPLACE PROCEDURE "INTER_PR_UPDATE_STATUS_RESERVA" (
      p_pedido_reserva in number,p_tipo_reserva in number,p_codigo_empresa in number)
is
   v_geracao_desenv_produto  number(1);
   v_status_reserva          number(1);
   v_estagio_costura         number(5);
begin
   v_geracao_desenv_produto := 0;
   v_status_reserva := 4;

   begin
      select fatu_503.geracao_desenv_produto
      into v_geracao_desenv_produto
      from fatu_503
      where fatu_503.codigo_empresa = p_codigo_empresa;
      exception
         when others then
            v_geracao_desenv_produto:= 0;
   end;

   begin
      select empr_001.estagio_confec
      into v_estagio_costura
      from empr_001;
      exception
         when others then
            v_estagio_costura := 0;
   end;

   <<produtos_reserva>>
   for reg_produtos in (select tmrp_645.nivel_produto nivel, tmrp_645.grupo_produto grupo, tmrp_645.subgru_produto subgrupo, tmrp_645.item_produto item
                        from tmrp_645
                        where tmrp_645.numero_reserva = p_pedido_reserva
                        group by tmrp_645.nivel_produto, tmrp_645.grupo_produto, tmrp_645.subgru_produto, tmrp_645.item_produto)
   loop
      begin
         select 4
         into   v_status_reserva
         from tmrp_620
         where tmrp_620.pedido_reserva = p_pedido_reserva
           and tmrp_620.nivel_produto = reg_produtos.nivel
           and tmrp_620.grupo_produto = reg_produtos.grupo
           and tmrp_620.subgrupo_produto = reg_produtos.subgrupo
           and tmrp_620.item_produto = reg_produtos.item
           and rownum = 1;
         exception
            when others then
               v_status_reserva := 2;
      end;

      if v_status_reserva = 2
      then
         exit produtos_reserva;
      end if;

      if v_geracao_desenv_produto = 2
      then
         begin
            select 2
            into   v_status_reserva
            from tmrp_650
            where tmrp_650.pedido_venda = p_pedido_reserva
              and tmrp_650.tipo_reserva = p_tipo_reserva
              and tmrp_650.nivel_produto = reg_produtos.nivel
              and tmrp_650.grupo_produto = reg_produtos.grupo
              and tmrp_650.codigo_estagio = v_estagio_costura
              and tmrp_650.data_inicio is null
              and rownum = 1;
            exception
               when others then
                  v_status_reserva := 4;
         end;

         if v_status_reserva = 2
         then
            exit produtos_reserva;
         end if;
      end if;
   end loop;

   begin
      update tmrp_640
      set tmrp_640.status = v_status_reserva
      where tmrp_640.numero_reserva = p_pedido_reserva;
   end;
end;
 

/

exec inter_pr_recompile;

