CREATE OR REPLACE PROCEDURE "INTER_PR_GET_SALDO_MOEDA_CREC"(
   p_empresa     in number, p_titulo      in number, p_parcela     in varchar2,  
   p_tipo_titulo in number, p_cnpj9       in number, p_cnpj4       in number,    
   p_cnpj2       in number, p_valor_moeda in number, p_saldo_moeda out number)
is  

   cursor pagamentos is 
   select fatu_075.historico_pgto, fatu_075.valor_pago,
        fatu_075.valor_juros, fatu_075.valor_descontos,
        fatu_075.data_pagamento, fatu_075.vlr_desconto_moeda,
        fatu_075.vlr_juros_moeda,fatu_075.valor_pago_moeda from fatu_075
   where fatu_075.nr_titul_codempr = p_empresa
     and fatu_075.nr_titul_cli_dup_cgc_cli9 = p_cnpj9
     and fatu_075.nr_titul_cli_dup_cgc_cli4 = p_cnpj4
     and fatu_075.nr_titul_cli_dup_cgc_cli2 = p_cnpj2
     and fatu_075.nr_titul_cod_tit = p_tipo_titulo
     and fatu_075.nr_titul_num_dup = p_titulo
     and fatu_075.nr_titul_seq_dup = p_parcela;
     
  v_sinal_titulo                   cont_010.sinal_titulo%type;
  v_valor_acrescimo_moeda          fatu_075.vlr_juros_moeda%type;
  v_valor_pago_moeda               fatu_075.valor_pago_moeda%type;
  v_valor_desconto_moeda           fatu_075.vlr_desconto_moeda%type;
     
begin
   
   v_valor_acrescimo_moeda := 0.00;
   v_valor_pago_moeda := 0.00;
   v_valor_desconto_moeda := 0.00;

   for pagamento in pagamentos
   loop
     
     select cont_010.sinal_titulo 
     into v_sinal_titulo
     from cont_010
     where cont_010.codigo_historico = pagamento.historico_pgto;  
   
     if v_sinal_titulo = 1
     then
       v_valor_acrescimo_moeda := v_valor_acrescimo_moeda + pagamento.vlr_juros_moeda;
       v_valor_pago_moeda := v_valor_pago_moeda + pagamento.valor_pago_moeda;
       v_valor_desconto_moeda := v_valor_desconto_moeda + pagamento.vlr_desconto_moeda;
     else 
       v_valor_acrescimo_moeda := v_valor_acrescimo_moeda - pagamento.vlr_juros_moeda;
       v_valor_pago_moeda := v_valor_pago_moeda - pagamento.valor_pago_moeda;
       v_valor_desconto_moeda := v_valor_desconto_moeda - pagamento.vlr_desconto_moeda;
     end if;
   
   end loop;

   p_saldo_moeda := p_valor_moeda - v_valor_pago_moeda - v_valor_acrescimo_moeda + v_valor_desconto_moeda;

end inter_pr_get_saldo_moeda_crec;
/
