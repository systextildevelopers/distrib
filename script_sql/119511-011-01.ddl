alter table SUPR_522
  drop constraint PK_SUPR_522;
  
alter table SUPR_522 modify TIPO_MATERIA_PRIMA default 0;

alter table SUPR_522
  add constraint PK_SUPR_522 primary key (COD_EMPRESA, CENTRO_CUSTO, TRANSACAO, TIPO_MATERIA_PRIMA) novalidate;
  
  
exec inter_pr_recompile;
