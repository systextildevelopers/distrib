alter table obrf_015 add
num_solicitacao_ecommerce number(9);

alter table obrf_015 add
seq_solicitacao_ecommerce number(9);

alter table obrf_015 modify
num_solicitacao_ecommerce default 0;

alter table obrf_015 modify
seq_solicitacao_ecommerce default 0;
