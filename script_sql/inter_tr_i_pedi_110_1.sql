
  CREATE OR REPLACE TRIGGER "INTER_TR_I_PEDI_110_1" 
before insert or
update of liquida_saldo_aprogramar, permite_atu_wms, grade_item, gramatura,
	numero_reserva, executa_trigger, cod_ped_cliente, seq_original,
	qtde_volumes, nr_alternativa, nr_roteiro, cor_formulario_fal,
	tipo_servico, grupo_maquina, subgrupo_maquina, numero_maquina,
	motivo_reprocesso, destino_projeto, produto_integracao, prod_grade_integracao,
	qtde_pecas_atend, seq_item_reserva, seq_ped_compra, seq_item_sub,
	seq_principal, observacao1, kanban_ped_ob, kanban_pedi_ob,
	item_ativo, empenho_automatico, caract, caract_esp,
	codigo_embalagem, pedido_venda, seq_item_pedido, cd_it_pe_nivel99,
	cd_it_pe_grupo, cd_it_pe_subgrupo, cd_it_pe_item, codigo_deposito,
	lote_empenhado, qtde_distribuida, qtde_pedida, valor_unitario,
	percentual_desc, qtde_faturada, nr_solicitacao, qtde_afaturar,
	situacao_fatu_it, cod_cancelamento, dt_cancelamento, dt_inclusao,
	qtde_rolos, nr_pedido_sub, deposito_sub, lote_sub,
	qtde_pedida_loja, qtde_sugerida, observacao2, caixa_ped_ob,
	montador_ped_ob, area_ped_ob, data_ent_prog, caixa_pedi_ob,
	codigo_acomp, data_empenho, data_solicitacao, acrescimo,
	expedidor, data_leitura_coletor, ja_atualizado, corredor,
	box, data_sugestao, perc_mao_obra, caract_ing,
	nr_sugestao, agrupador_producao, cod_nat_op, est_nat_op,
	um_faturamento_um, um_faturamento_qtde, um_faturamento_valor, largura
on pedi_110
for each row

declare
   Pragma autonomous_transaction;

  perc_acompanhamento number;
  qtde_principal      number;
  codigo_empresa_lido number;
  tipo_calculo_acomp  number;

begin
   begin
      select pedi_100.codigo_empresa
      into codigo_empresa_lido
      from pedi_100
      where pedi_100.pedido_venda = :new.pedido_venda;
      exception when others
      then codigo_empresa_lido := 1;
   end;

   begin
      select fatu_503.calculo_perc_acompanhamento
      into tipo_calculo_acomp
      from fatu_503
      where fatu_503.codigo_empresa = codigo_empresa_lido;
      exception when others
      then tipo_calculo_acomp := 0;
   end;

   if tipo_calculo_acomp = 1 and ((inserting and :new.codigo_acomp = 0) or updating)
   then
      if :new.seq_principal > 0
      then
         begin
            select pedi_110.qtde_pedida
            into qtde_principal
            from pedi_110
            where pedi_110.pedido_venda = :new.pedido_venda
              and pedi_110.seq_item_pedido = :new.seq_principal;
            exception when others
            then qtde_principal := 0;
         end;

         if qtde_principal > 0
         then
            perc_acompanhamento := ceil((:new.qtde_pedida * 100) / qtde_principal);
         else
            perc_acompanhamento := 0;
         end if;

         if perc_acompanhamento > 100
         then
            perc_acompanhamento := 0;
         end if;


         :new.codigo_acomp := perc_acompanhamento;
      end if;
   end if;
end;

-- ALTER TRIGGER "INTER_TR_I_PEDI_110_1" ENABLE
 

/

exec inter_pr_recompile;

