create or replace package ST_PCK_CLIENTE as

    TYPE t_dados_pedi_010 IS TABLE OF pedi_010%rowtype;

    TYPE t_dados_pcpt_020 IS TABLE OF pcpt_020%rowtype;

    TYPE t_dados_fatu_100 IS TABLE OF fatu_100%rowtype;

    TYPE t_dados_pcpc_320 IS TABLE OF pcpc_320%rowtype;

    function get_dados_cliente( p_cnpj_9 number,  p_cnpj_4 number,  p_cnpj_2 number, p_msg_erro in out varchar2) return t_dados_pedi_010;

    function get_dados_embalagem( p_pedido_venda number,  p_seq_item_pedido number,  p_rolo_estoque number, p_msg_erro in out varchar2) return t_dados_pcpt_020;

    FUNCTION get_dados_embalagem_fatu_100( p_codigo_embalagem number,  p_msg_erro in out varchar2) return t_dados_fatu_100;

    FUNCTION get_dados_embalagem_pcpc_320( p_nr_volume number,  p_msg_erro in out varchar2) return t_dados_pcpc_320;
    
    function get_dados_cliente_narwal(  p_msg_erro in out varchar2) return t_dados_pedi_010;

end;
/
