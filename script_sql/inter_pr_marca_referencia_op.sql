
  CREATE OR REPLACE PROCEDURE "INTER_PR_MARCA_REFERENCIA_OP" (
 p_nivel_produto         in     varchar2,
 p_grupo_produto         in     varchar2,
 p_subgrupo_produto      in     varchar2,
 p_item_produto          in     varchar2,
 p_ordem_producao               number,
 p_area_ordem                   number
 )
IS
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_existe_reg              number(1);
   v_existe_reg_2            number(1);
   v_tmrp_655_status_1       number;

   v_recalcular_referencia   fatu_503.recalcular_referencia%type;

BEGIN
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   begin
      select fatu_503.recalcular_referencia
      into   v_recalcular_referencia
      from fatu_503
      where fatu_503.codigo_empresa = ws_empresa;
      exception when others then
         v_recalcular_referencia := 1;
   end;

   if p_ordem_producao <> 0 and v_recalcular_referencia = 0
   then
      v_tmrp_655_status_1 := 0;

      begin
         select nvl(count(1),0)
         into v_tmrp_655_status_1
         from tmrp_655
         where tmrp_655.area_producao  = p_area_ordem
           and tmrp_655.ordem_producao = p_ordem_producao
           and tmrp_655.status         = 1
           and rownum                  = 1;
      exception when others then
         v_tmrp_655_status_1 := 0;
      end;

      if v_tmrp_655_status_1 > 0
      then
         begin
            delete from tmrp_655
            where tmrp_655.area_producao  = p_area_ordem
              and tmrp_655.ordem_producao = p_ordem_producao
              and tmrp_655.status         = 1;
         end;
      end if;

      begin
         select 1
         into v_existe_reg
         from tmrp_041
         where tmrp_041.area_producao    = p_area_ordem
           and tmrp_041.nr_pedido_ordem  = p_ordem_producao
           and rownum                    = 1;
      exception when others then
         v_existe_reg := 0;
      end;

      begin
         select 1
         into v_existe_reg_2
         from tmrp_655
         where tmrp_655.area_producao  = p_area_ordem
           and tmrp_655.ordem_producao = p_ordem_producao
           and tmrp_655.status         = 0
           and rownum                  = 1;
      exception when others then
         v_existe_reg_2 := 0;
      end;


      if v_existe_reg > 0 and v_existe_reg_2 = 0
      then
         begin
            -- INSERE NA TABELA TEMPORARIO DO RECALCULO COM STATUS 0
            insert into tmrp_655(
               nivel,           grupo,
               area_producao,   ordem_producao,
               status,          usuario
            )values(
               '0',             '0',
               p_area_ordem,    p_ordem_producao,
               0,               ws_usuario_systextil);
         exception when others then
            -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_655' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if; --fim if p_ordem_producao <> 0

   if p_nivel_produto <> '0' and v_recalcular_referencia = 0
   then
      begin
         select 1
         into v_existe_reg
         from tmrp_041
         where tmrp_041.nivel_estrutura    = p_nivel_produto
           and tmrp_041.grupo_estrutura    = p_grupo_produto
--           and (tmrp_041.subgru_estrutura  = p_subgrupo_produto
--            or  p_subgrupo_produto         = '000')
--           and (tmrp_041.item_estrutura    = p_item_produto
--            or  p_item_produto             = '000000')
           and rownum                      = 1;
      exception when others then
         v_existe_reg := 0;
      end;

      v_tmrp_655_status_1 := 0;
      begin
         select nvl(count(1),0)
         into v_tmrp_655_status_1
         from tmrp_655
         where tmrp_655.nivel  = p_nivel_produto
           and tmrp_655.grupo  = p_grupo_produto
           and tmrp_655.status = 1;
      exception when others then
         v_tmrp_655_status_1 := 0;
      end;

      if v_tmrp_655_status_1 > 0
      then
         begin
            delete from tmrp_655
            where tmrp_655.nivel  = p_nivel_produto
              and tmrp_655.grupo  = p_grupo_produto
              and tmrp_655.status = 1;
         exception when others then
            v_tmrp_655_status_1 := 0;
         end;
      end if;

      begin
         select 1
         into v_existe_reg_2
         from tmrp_655
         where tmrp_655.nivel  = p_nivel_produto
           and tmrp_655.grupo  = p_grupo_produto
           and tmrp_655.status = 0
           and rownum          = 1;
      exception when others then
         v_existe_reg_2 := 0;
      end;
      if v_existe_reg > 0 and v_existe_reg_2 = 0
      then
--        raise_application_error(-20000,p_nivel_produto||'.'||p_grupo_produto);
         begin
            -- INSERE NA TABELA TEMPORARIO DO RECALCULO COM STATUS 0
            insert into tmrp_655(
               nivel,           grupo,
               area_producao,   ordem_producao,
               status,          usuario
            )values(
               p_nivel_produto, p_grupo_produto,
               0,               0,
               0,               ws_usuario_systextil);
         exception when others then
            -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_655' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;
END INTER_PR_MARCA_REFERENCIA_OP;

 

/

exec inter_pr_recompile;

