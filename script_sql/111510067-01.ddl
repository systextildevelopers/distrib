INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'obrf_f793',     1,
	1,			     'Processo de Geração CNP');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'obrf_f793',   'obrf_menu', 
	1,             1, 
	'S',           'S', 
	'S',           'S');
	
INSERT INTO hdoc_033
(	usu_prg_cdusu,  usu_prg_empr_usu, 
	programa,       nome_menu, 
	item_menu,      ordem_menu, 
	incluir,        modificar, 
	excluir,        procurar)
VALUES
(	'TREINAMENTO',  1, 
	'obrf_f793',    'obrf_menu', 
	1,              1, 
	'S',            'S', 
	'S',            'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Processo de Geração CNP'
 WHERE hdoc_036.codigo_programa = 'obrf_f793'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;

/
EXEC inter_pr_recompile;
