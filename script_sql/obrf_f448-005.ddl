-- Create table
create table OBRF_925
( ID_925         NUMBER(9) default 0 not null,  
  ID_920         NUMBER(9) default 0 not null,  
  COD_QUADRO     NUMBER(9) default 0 not null, 
  COD_INF_ADIC   VARCHAR2(8) default ' ', 
  VL_INF_ADIC    NUMBER(15,2) default 0, 
  DESCR_COMPL_AJ VARCHAR2(4000) default ' ');
  
-- Add comments to the columns 
comment on column OBRF_925.COD_INF_ADIC   is 'C�digo da informa��o adicional conforme tabela a ser definida pelas SEFAZ, conforme tabela definida no item 5.2.';
comment on column OBRF_925.VL_INF_ADIC    is 'Valor referente � informa��o adicional.';
comment on column OBRF_925.DESCR_COMPL_AJ is 'Descri��o complementar do ajuste.';

-- Create/Recreate primary, unique and foreign key constraints 
alter table OBRF_925 add constraint PK_OBRF_925 primary key (ID_925);
alter table OBRF_925 add constraint UNIQ_OBRF_925 unique (ID_920, COD_QUADRO, COD_INF_ADIC);

create synonym systextilrpt.OBRF_925 for OBRF_925; 

create sequence seq_obrf_925
minvalue 0
maxvalue 99999999999999999999
start with 1
increment by 1;

CREATE OR REPLACE TRIGGER INTER_TR_OBRF_925_SEQ
BEFORE INSERT ON OBRF_925 FOR EACH ROW
DECLARE
    next_value number;
BEGIN
    if :new.ID_925 is null or :new.ID_925 = 0 then
        select seq_obrf_925.nextval
        into next_value from dual;
        :new.ID_925 := next_value;
    end if;
END INTER_TR_OBRF_925_SEQ;

/

exec inter_pr_recompile;
