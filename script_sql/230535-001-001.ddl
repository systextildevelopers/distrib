create table oper_284
(
	cod_empresa	number(3)		not null, 
	natureza_saida	number(3) 		not null,
	divisao	varchar2(1)		not null,
	natureza_cte	number(3) 		not null,
	cod_contabil	number(6) 		not null,
	centro_custo	number(6) 		not null
);
