
  CREATE OR REPLACE TRIGGER "INTER_TR_CONT_600_SPED_CTB" 
   before insert on cont_600
   for each row

declare
   v_cgc_9           cont_600.cnpj9_participante%type;
   v_cgc_4           cont_600.cnpj4_participante%type;
   v_cgc_2           cont_600.cnpj2_participante%type;
   v_cliente_fornec  cont_600.cliente_fornecedor_part%type;
   v_sid             cont_601.sid%type;
   v_instancia       cont_601.instancia%type;

   v_tabela_origem   varchar2(10);

begin

   -- se for uma origem de NFS, NFE, CPAG, CREC, tenta achar o CNPJ do participante
   if   :new.prg_gerador <> 'cont_f655'
   and (:new.origem = 1 or :new.origem = 2 or :new.origem = 3
   or   :new.origem = 4 or :new.origem = 5 or :new.origem = 6)

   then

      -- seta a tabela origem pela origem do lancamento
      if :new.origem = 1
      then
         v_tabela_origem := 'FATU_050';
      else

         if :new.origem = 3
         then
            v_tabela_origem := 'FATU_070';
         else

            if :new.origem = 4
            then
               v_tabela_origem := 'FATU_075';
            else

               if :new.origem = 2
               then
                  v_tabela_origem := 'OBRF_010';
               else

                  if :new.origem = 5
                  then
                     v_tabela_origem := 'CPAG_010';
                  else

                     if :new.origem = 6
                     then
                        v_tabela_origem := 'CPAG_015';
                     end if;
                  end if;
               end if;
            end if;
         end if;
      end if;

      -- busca a idetificacao da conexao com o banco de dados
      select sid,   inst_id
      into   v_sid, v_instancia
      from   v_lista_sessao_banco;

      -- encontra o CNPJ do participante na tabela cont_601
      begin
         select cnpj_9,         cnpj_4,       cnpj_2,
                cliente_fornec
         into   v_cgc_9,        v_cgc_4,      v_cgc_2,
                v_cliente_fornec
         from cont_601
         where sid           = v_sid
         and   instancia     = v_instancia
         and   data_insercao = (select max(data_insercao) from cont_601
                                where sid           = v_sid
                                and   instancia     = v_instancia
                                and   tabela_origem = v_tabela_origem)
         and   tabela_origem = v_tabela_origem;

         :new.cnpj9_participante      := v_cgc_9;
         :new.cnpj4_participante      := v_cgc_4;
         :new.cnpj2_participante      := v_cgc_2;
         :new.cliente_fornecedor_part := v_cliente_fornec;

         -- elimina os registros anteriores
         delete cont_601
         where trunc(data_insercao, 'DD') < trunc(sysdate, 'DD');

      exception
         when others then
            null;
      end;
   end if;
end inter_tr_cont_600_sped_ctb;
-- ALTER TRIGGER "INTER_TR_CONT_600_SPED_CTB" ENABLE

/

drop trigger INTER_TR_CONT_600_SPED_CTB; 

/

exec inter_pr_recompile;

