insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('oper_f920', 'Cadastro de Estágios para Conferência dos rolos confirmados', 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'oper_f920', 'nenhum' , 0, 20, 'S', 'S', 'S', 'S');

insert into hdoc_036
(codigo_programa, locale, descricao, flag_repassado, data_criacao)
values
('oper_f920', 'pt_BR', 'Cadastro de Estágios para Conferência dos rolos confirmados', 0, sysdate);

commit;

exec inter_pr_recompile;
/
