
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_025" 
before insert on pcpt_025
for each row

declare

   v_area          pcpt_025.area_ordem%type;
   v_numero_ordem  pcpt_025.numero_ordem%type;
   v_rolo_estoque  pcpt_020.rolo_estoque%type;
   v_alocado       number := 1;

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

begin

      -- Dados do usu�rio logado
      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                              ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
      begin
         select area_ordem, ordem_producao
         into   v_area,     v_numero_ordem
         from pcpt_025
         where pcpt_025.codigo_rolo = :new.codigo_rolo
           and rownum               = 1;
      exception when no_data_found then
         v_alocado := 0;
      end;

      begin
         select pcpt_020.rolo_estoque
         into   v_rolo_estoque
         from pcpt_020
         where pcpt_020.codigo_rolo = :new.codigo_rolo;
      exception when no_data_found then
         v_rolo_estoque := 0;
      end;

      if v_alocado > 0 and v_rolo_estoque > 0
      then
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds28618', to_char(v_area) ,  to_char(v_numero_ordem) , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end if;
end;


-- ALTER TRIGGER "INTER_TR_PCPT_025" ENABLE
 

/

exec inter_pr_recompile;

