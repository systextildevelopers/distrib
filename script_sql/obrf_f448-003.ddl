-- Create table
create table OBRF_922
( ID_922       NUMBER(9) default 0 not null,
  ID_921       NUMBER(9) default 0 not null,  
  NUM_DA       VARCHAR2(9) default ' ', 
  NUM_PROC     VARCHAR2(15) default ' ', 
  IND_PROC     VARCHAR2(1) default ' ', 
  PROC         VARCHAR2(4000) default ' ', 
  TXT_COMPL    VARCHAR2(4000) default ' ');
  
-- Add comments to the columns 
comment on column OBRF_922.NUM_DA    is 'N�mero do documento de arrecada��o estadual, se houver.';
comment on column OBRF_922.NUM_PROC  is 'N�mero do processo ao qual o ajuste est� vinculado, se houver.';
comment on column OBRF_922.IND_PROC  is 'Indicador da origem do processo: 0- SEFAZ, 1- Justi�a Federal, 2- Justi�a Estadual, 9- Outros.';
comment on column OBRF_922.PROC      is 'Descri��o resumida do processo que embasou o lan�amento.';
comment on column OBRF_922.TXT_COMPL is 'Descri��o complementar.';

-- Create/Recreate primary, unique and foreign key constraints 
alter table OBRF_922 add constraint PK_OBRF_922 primary key (ID_922);
alter table OBRF_922 add constraint UNIQ_OBRF_922 unique (ID_921);


create synonym systextilrpt.OBRF_922 for OBRF_922; 

create sequence seq_obrf_922
minvalue 0
maxvalue 99999999999999999999
start with 1
increment by 1;

CREATE OR REPLACE TRIGGER INTER_TR_OBRF_922_SEQ
BEFORE INSERT ON OBRF_922 FOR EACH ROW
DECLARE
    next_value number;
BEGIN
    if :new.ID_922 is null or :new.ID_922 = 0 then
        select seq_obrf_922.nextval
        into next_value from dual;
        :new.ID_922 := next_value;
    end if;
END INTER_TR_OBRF_922_SEQ;

/

exec inter_pr_recompile;
