insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('crec_f374', 'Processo de Associação de Títulos a Cheques', 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'crec_f374', 'menu_ad22', 1, 0, 'S', 'N', 'N', 'N');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'crec_f374', 'menu_ad22', 1, 0, 'S', 'N', 'N', 'N');

update hdoc_036
   set hdoc_036.descricao       = 'Processo de Associação de Títulos a Cheques'
 where hdoc_036.codigo_programa = 'crec_f374'
   and hdoc_036.locale          = 'es_ES';
commit;
/
