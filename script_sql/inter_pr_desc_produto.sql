
  CREATE OR REPLACE PROCEDURE "INTER_PR_DESC_PRODUTO" (
 p_nivel_produto         in     varchar2,
 p_grupo_produto         in     varchar2,
 p_subgrupo_produto      in     varchar2,
 p_item_produto          in     varchar2,
 r_desc_produto          out    varchar2,
 r_narrativa_item        out    varchar2
)
is
   ws_desc_grupo             basi_030.descr_referencia%type;
   ws_desc_subgrupo          basi_020.descr_tam_refer%type;
   ws_desc_item              basi_010.descricao_15%type;
begin
   begin
      select nvl(basi_030.descr_referencia,' ')
      into ws_desc_grupo
      from basi_030
      where basi_030.nivel_estrutura = p_nivel_produto
        and basi_030.referencia      = p_grupo_produto;
   exception when no_data_found then
      ws_desc_grupo := ' ';
   end;

   begin
      select nvl(basi_020.descr_tam_refer,' ')
      into ws_desc_subgrupo
      from basi_020
      where basi_020.basi030_nivel030 = p_nivel_produto
        and basi_020.basi030_referenc = p_grupo_produto
        and basi_020.tamanho_ref      = p_subgrupo_produto;
   exception when no_data_found then
      ws_desc_subgrupo := ' ';
   end;

   begin
      select nvl(basi_010.descricao_15,' '), nvl(basi_010.narrativa,' ')
      into ws_desc_item,                     r_narrativa_item
      from basi_010
      where basi_010.nivel_estrutura  = p_nivel_produto
        and basi_010.grupo_estrutura  = p_grupo_produto
        and basi_010.subgru_estrutura = p_subgrupo_produto
        and basi_010.item_estrutura   = p_item_produto;
   exception when no_data_found then
      ws_desc_item     := ' ';
      r_narrativa_item := ' ';
   end;

   r_desc_produto := ws_desc_grupo || ' ' || ws_desc_subgrupo  || ' ' || ws_desc_item;

end inter_pr_desc_produto;

 

/

exec inter_pr_recompile;

