alter table i_obrf_010 add situacao_liberacao number(1);

alter table i_obrf_010 modify situacao_liberacao default 0;

exec inter_pr_recompile;
