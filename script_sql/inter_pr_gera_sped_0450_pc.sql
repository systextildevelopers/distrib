create or replace procedure inter_pr_gera_sped_0450_pc(p_cod_empresa   IN NUMBER,
                                                       p_cod_matriz    IN  NUMBER,
                                                       p_cnpj9_empresa IN  NUMBER,
                                                       p_cnpj4_empresa IN  NUMBER,
                                                       p_cnpj2_empresa IN  NUMBER,
                                                       p_des_erro      OUT varchar2) is
--
-- Finalidade: Gerar a tabela sped_0450_pc - gerar cadastro de mensagens
-- Autor.....: Edson Pio
-- Data......: 23/02/11
--
-- OBSERVAÇÕES DE NOTAS FISCAIS
--
-- Data    Autor    Observações
--

w_ind_achou           varchar2(1);
w_erro                EXCEPTION;
indConsolDetalhe      number;


CURSOR u_fatu_052 (p_cod_empresa     NUMBER,
                   p_cod_matriz      NUMBER,
                   indConsolDetalhe NUMBER) IS
   select distinct fatu_052.num_nota,        fatu_052.cod_serie_nota, 
                   fatu_052.cnpj9,           fatu_052.cnpj4, 
                   fatu_052.cnpj2,           fatu_052.cod_mensagem, 
                   fatu_052.cod_empresa,     fatu_052.ind_entr_saida
   from fatu_052, sped_pc_c100
   where fatu_052.cod_empresa    = sped_pc_c100.cod_empresa
     and fatu_052.num_nota       = sped_pc_c100.num_nota_fiscal
     and fatu_052.cod_serie_nota = sped_pc_c100.cod_serie_nota
     and fatu_052.cnpj9          = sped_pc_c100.num_cnpj_9
     and fatu_052.cnpj4          = sped_pc_c100.num_cnpj_4
     and fatu_052.cnpj2          = sped_pc_c100.num_cnpj_2
     and fatu_052.ind_entr_saida = sped_pc_c100.tip_entrada_saida
     and sped_pc_c100.cod_serie_nota <> 'ECF'
     and sped_pc_c100.ind_nf_consumo = 'N'
     and fatu_052.cod_mensagem   > 0
     and sped_pc_c100.cod_modelo not in ('06','07','08','09','10','11','22','27','28','57')
     and (not (sped_pc_c100.cod_modelo in ('55','65','59') and sped_pc_c100.tip_entrada_saida = 'S') or indConsolDetalhe = 2)
     and sped_pc_c100.cod_situacao_nota not in (2,3,5)
     and fatu_052.cod_empresa      = p_cod_empresa
     and sped_pc_c100.cod_matriz   = p_cod_matriz;

CURSOR u_obrf_874 (p_cod_mensagem NUMBER) IS
   select obrf_874.cod_mensagem, obrf_874.tip_mensagem,
             substr(trim((obrf_874.des_mensagem1 || '' || obrf_874.des_mensagem2 || '' ||
                          obrf_874.des_mensagem3 || '' || obrf_874.des_mensagem4 || '' ||
                          obrf_874.des_mensagem5 || '' || obrf_874.des_mensagem6 || '' ||
                          obrf_874.des_mensagem7 || '' || obrf_874.des_mensagem8 || '' ||
                          obrf_874.des_mensagem9 || '' || obrf_874.des_mensagem10)),1,255) descr_mensangem
   FROM   obrf_874
   WHERE  obrf_874.cod_mensagem = p_cod_mensagem
     AND  obrf_874.cod_mensagem > 0;

BEGIN

   p_des_erro := NULL;
   
   indConsolDetalhe := inter_fn_get_param_int(p_cod_matriz, 'sped.indivConsol');
   
   FOR fatu_052 IN u_fatu_052 (p_cod_empresa,p_cod_matriz,indConsolDetalhe) LOOP
       w_ind_achou := 'N';

          FOR obrf_874 IN u_obrf_874 (fatu_052.cod_mensagem) LOOP
              w_ind_achou := 'S';

              BEGIN
                 INSERT INTO sped_pc_0450
                    (cod_empresa
                    ,cod_matriz
                    ,num_nota
                    ,cod_serie_nota
                    ,num_cnpj9_empr
                    ,num_cnpj4_empr
                    ,num_cnpj2_empr
                    ,cod_mensagem
                    ,des_mensagem
                    ,cnpj9
                    ,cnpj4
                    ,cnpj2
                    ,ind_entr_saida
                    ) values
                    (p_cod_empresa                                               -- cod_empresa
                    ,p_cod_matriz
                    ,fatu_052.num_nota
                    ,fatu_052.cod_serie_nota
                    ,p_cnpj9_empresa
                    ,p_cnpj4_empresa
                    ,p_cnpj2_empresa
                    ,fatu_052.cod_mensagem                                       -- cod_mensagem
                    ,decode(obrf_874.descr_mensangem,null,'',obrf_874.descr_mensangem)
                    ,fatu_052.cnpj9
                    ,fatu_052.cnpj4
                    ,fatu_052.cnpj2
                    ,fatu_052.ind_entr_saida);
              EXCEPTION
                 WHEN dup_val_on_index THEN
                    NULL;

                 WHEN OTHERS THEN
                    p_des_erro := 'Erro na inclusão da tabela sped_pc_0450 ' || Chr(10) ||
                                  ' MENSAGEM: ' || fatu_052.cod_mensagem ||
                                  Chr(10) || SQLERRM;
                    RAISE W_ERRO;
              END;
              COMMIT;

          END LOOP;
   END LOOP;

   COMMIT;

EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_pc_0450 ' || Chr(10) || p_des_erro;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure p_gera_sped_pc_0450 ' || Chr(10) || SQLERRM;
END inter_pr_gera_sped_0450_pc;
/
exec inter_pr_recompile;
