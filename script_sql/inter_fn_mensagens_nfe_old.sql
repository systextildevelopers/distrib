create or replace function inter_fn_mensagens_nfe_old(p_empresa in number, p_numero_nota in number, p_serie_nota in  varchar2, p_natureza in number, p_estado in varchar2, p_tipo_mensagem in varchar2)
return varchar2
is
   v_mensagem varchar2(4000);
   qtde_caracter_msg_somando number;
   total_qtde_caracter_msg number;
   total_qtde_mensagens number;
   nr_registro_msg number;

   w_erro                  varchar2(2000);
begin 

      total_qtde_caracter_msg := 901;
      qtde_caracter_msg_somando := 0;
      nr_registro_msg           := 0;
      
      for ad_fatu052 in (select fatu_052.cod_mensagem,length(fatu_052.des_mensag_1 || fatu_052.des_mensag_2 || fatu_052.des_mensag_3 ||
                                fatu_052.des_mensag_4 || fatu_052.des_mensag_5 || fatu_052.des_mensag_6 ||
                                fatu_052.des_mensag_7 || fatu_052.des_mensag_8 || fatu_052.des_mensag_9 ||
                                fatu_052.des_mensag_10) tam_msg
                         from fatu_052
                         where fatu_052.cod_empresa    = p_empresa 
                         and   fatu_052.num_nota       = p_numero_nota 
                         and   fatu_052.cod_serie_nota = p_serie_nota 
                         and   fatu_052.ind_local = 'D'
                         order by fatu_052.seq_mensagem)
      loop
         qtde_caracter_msg_somando := qtde_caracter_msg_somando + ad_fatu052.tam_msg;

         nr_registro_msg := nr_registro_msg + 1;

         if qtde_caracter_msg_somando > total_qtde_caracter_msg or nr_registro_msg > 10
         then
            nr_registro_msg := nr_registro_msg - 1;
            qtde_caracter_msg_somando := qtde_caracter_msg_somando - ad_fatu052.tam_msg;

            BEGIN
              update fatu_052
              set fatu_052.ind_local_msg_danfe = 'C',
                  fatu_052.ind_local = 'C',
                  fatu_052.desc_ind_adicionais = 'CONTINUACAO INF. ADICIONAIS'
              where fatu_052.cod_empresa    = p_empresa
                and fatu_052.num_nota       = p_numero_nota 
                and fatu_052.cod_serie_nota = p_serie_nota
                and fatu_052.cod_mensagem   = ad_fatu052.cod_mensagem;
            EXCEPTION
                 WHEN OTHERS THEN
                   w_erro := 'Erro na altera��o da tabela fatu_052 - Mensagens notas fiscai ' || Chr(10) || SQLERRM;

             END;
         end if;
      end loop;

      
      v_mensagem := '';

      for reg_fatu052 in (select nvl(trim(des_mensag_1) || trim(des_mensag_2) || trim(des_mensag_3) ||
                                     trim(des_mensag_4) || trim(des_mensag_5) || trim(des_mensag_6) ||
                                     trim(des_mensag_7) || trim(des_mensag_8) || trim(des_mensag_9) ||
                                     trim(des_mensag_10 || trim(des_mensag_11) || trim(des_mensag_12)), ' ') MENSAGEM
                          from fatu_052, obrf_874
                          where fatu_052.cod_empresa    = p_empresa
                            and fatu_052.num_nota       = p_numero_nota
                            and fatu_052.cod_serie_nota = p_serie_nota
                            and fatu_052.cod_mensagem   = obrf_874.cod_mensagem
                            and obrf_874.tip_mensagem   = p_tipo_mensagem
                          order by fatu_052.seq_mensagem)

      loop
         v_mensagem := v_mensagem || ' ' || reg_fatu052.mensagem;
      end loop;
   return(v_mensagem);
end INTER_FN_MENSAGENS_NFE_OLD;

