create or replace function inter_fn_mensagens_nfe(p_empresa in number, p_numero_nota in number, p_serie_nota in  varchar2, p_natureza in number, p_estado in varchar2, p_tipo_mensagem in varchar2)
return varchar2
is
   v_mensagem varchar2(4000);
   PARAM_CARACTER_LINHA number;
   PARAM_QTDE_MENSAGENS number;
   saldo_mensagens number;
   saldo_caracteres_linha number;
   qtde_linhas_msg number;
   
   w_erro                  varchar2(2000);
begin
	  
	  begin
		select max_linhas_dados_adic, max_carac_linhas_dados_adic
		into PARAM_QTDE_MENSAGENS, PARAM_CARACTER_LINHA
		from fatu_505 
		where fatu_505.codigo_empresa  = p_empresa
		  and fatu_505.serie_nota_fisc = p_serie_nota;
	  exception
		when others then
			PARAM_CARACTER_LINHA := 0;
			PARAM_QTDE_MENSAGENS := 0;
	  end;
	  
	  IF PARAM_CARACTER_LINHA > 0 AND PARAM_QTDE_MENSAGENS > 0
	  THEN
	  
		  saldo_mensagens := PARAM_QTDE_MENSAGENS;
		  
		  for ad_fatu052 in (select fatu_052.cod_mensagem,length(fatu_052.des_mensag_1 || fatu_052.des_mensag_2 || fatu_052.des_mensag_3 ||
									fatu_052.des_mensag_4 || fatu_052.des_mensag_5 || fatu_052.des_mensag_6 ||
									fatu_052.des_mensag_7 || fatu_052.des_mensag_8 || fatu_052.des_mensag_9 ||
									fatu_052.des_mensag_10) tam_msg
							 from fatu_052
							 where fatu_052.cod_empresa    = p_empresa 
							 and   fatu_052.num_nota       = p_numero_nota 
							 and   fatu_052.cod_serie_nota = p_serie_nota 
							 and   fatu_052.ind_local = 'D'
							 order by fatu_052.seq_mensagem)
		  loop
			 if saldo_mensagens > 0 
			 then 
				qtde_linhas_msg := trunc((ad_fatu052.tam_msg / 100), 0) + 1;
				
				saldo_caracteres_linha := saldo_mensagens * PARAM_CARACTER_LINHA;
				
				if saldo_caracteres_linha >= ad_fatu052.tam_msg
				then 
					saldo_mensagens := saldo_mensagens - qtde_linhas_msg;
				else 
					BEGIN
					  update fatu_052
					  set fatu_052.ind_local_msg_danfe = 'C',
						  fatu_052.ind_local = 'C',
						  fatu_052.desc_ind_adicionais = 'CONTINUACAO INF. ADICIONAIS'
					  where fatu_052.cod_empresa    = p_empresa
						and fatu_052.num_nota       = p_numero_nota 
						and fatu_052.cod_serie_nota = p_serie_nota
						and fatu_052.cod_mensagem   = ad_fatu052.cod_mensagem;
					EXCEPTION
						 WHEN OTHERS THEN
						   w_erro := 'Erro na altera��o da tabela fatu_052 - Mensagens notas fiscai ' || Chr(10) || SQLERRM;
					END;
				end if;
			 else
				BEGIN
				  update fatu_052
				  set fatu_052.ind_local_msg_danfe = 'C',
					  fatu_052.ind_local = 'C',
					  fatu_052.desc_ind_adicionais = 'CONTINUACAO INF. ADICIONAIS'
				  where fatu_052.cod_empresa    = p_empresa
					and fatu_052.num_nota       = p_numero_nota 
					and fatu_052.cod_serie_nota = p_serie_nota
					and fatu_052.cod_mensagem   = ad_fatu052.cod_mensagem;
				EXCEPTION
					 WHEN OTHERS THEN
					   w_erro := 'Erro na altera��o da tabela fatu_052 - Mensagens notas fiscai ' || Chr(10) || SQLERRM;
				END;
			 end if;
		  end loop;
		  
		  v_mensagem := '';

		  for reg_fatu052 in (select nvl(trim(des_mensag_1) || trim(des_mensag_2) || trim(des_mensag_3) ||
										 trim(des_mensag_4) || trim(des_mensag_5) || trim(des_mensag_6) ||
										 trim(des_mensag_7) || trim(des_mensag_8) || trim(des_mensag_9) ||
										 trim(des_mensag_10 || trim(des_mensag_11) || trim(des_mensag_12)), ' ') MENSAGEM
							  from fatu_052, obrf_874
							  where fatu_052.cod_empresa    = p_empresa
								and fatu_052.num_nota       = p_numero_nota
								and fatu_052.cod_serie_nota = p_serie_nota
								and fatu_052.cod_mensagem   = obrf_874.cod_mensagem
								and obrf_874.tip_mensagem   = p_tipo_mensagem
							  order by fatu_052.seq_mensagem)

		  loop
			 v_mensagem := v_mensagem || ' ' || reg_fatu052.mensagem;
		  end loop;
		return(v_mensagem);
	ELSE
		RETURN inter_fn_mensagens_nfe_old(p_empresa, p_numero_nota, p_serie_nota, p_natureza, p_estado, p_tipo_mensagem);
	END IF;
end inter_fn_mensagens_nfe;

