CREATE TABLE supr_797
(
   num_ped_compra  NUMBER(6) DEFAULT 0 not null,
   seq_item_pedido NUMBER(2) DEFAULT 0 not null,
   data_prev_entr  DATE not null,
   data_prev_old   DATE not null,
   cod_motivo      NUMBER(3) DEFAULT 0,
   data_ocorr      DATE,
   usuario         VARCHAR2(100) DEFAULT ' ',
   obs             VARCHAR2(1000) DEFAULT ' '
);

/

exec inter_pr_recompile;
