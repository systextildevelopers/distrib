
  CREATE OR REPLACE TRIGGER "INTER_TR_ESTQ_301" 
before insert
on estq_301
for each row

declare
  v_codigo_contabil  number;
  v_local_deposito   number;
  v_tran_baixa_estq  number;
  v_conta_contab_inv number;
  v_exercicio        number;
  v_plano_conta      number;
begin

   begin
     select basi_205.local_deposito
       into v_local_deposito
       from basi_205
      where basi_205.codigo_deposito = :new.codigo_deposito;
   exception when others
      then null;
   end;

   begin
     select fatu_504.conta_contab_inv
       into v_conta_contab_inv
       from fatu_504
      where fatu_504.codigo_empresa = v_local_deposito;
   exception when others
      then null;
   end;

   if v_conta_contab_inv = 1
   then

     begin
       select basi_010.codigo_contabil
         into v_codigo_contabil
         from basi_010
        where basi_010.nivel_estrutura  = :new.nivel_estrutura
          and basi_010.grupo_estrutura  = :new.grupo_estrutura
          and basi_010.subgru_estrutura = :new.subgrupo_estrutura
          and basi_010.item_estrutura   = :new.item_estrutura;
     exception when others
        then v_codigo_contabil := 0;
     end;

     if v_codigo_contabil = 0
     then
       begin
         select basi_030.codigo_contabil
           into v_codigo_contabil
           from basi_030
          where basi_030.nivel_estrutura = :new.nivel_estrutura
            and basi_030.referencia      = :new.grupo_estrutura;
       exception when others
          then null;
       end;
     end if;

     begin
       select cont_500.exercicio, cont_500.cod_plano_cta
       into   v_exercicio,        v_plano_conta
       from cont_500
       where cont_500.cod_empresa  = v_local_deposito
         and :new.mes_ano_movimento between cont_500.per_inicial and cont_500.per_final;
     exception when others
        then null;
     end;

     begin
       select cont_560.tran_baixa_estq
         into v_tran_baixa_estq
         from cont_560
        where cont_560.cod_plano_cta = v_plano_conta;
     exception when others
        then null;
     end;

     :new.conta_contabil := inter_fn_encontra_conta(v_local_deposito, 3, v_codigo_contabil, v_tran_baixa_estq, 0, v_exercicio, v_exercicio);

   end if;

end inter_tr_estq_301;

-- ALTER TRIGGER "INTER_TR_ESTQ_301" ENABLE
 

/

exec inter_pr_recompile;

