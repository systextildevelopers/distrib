
delete hdoc_033 where programa = 'mqop_f020';
delete hdoc_033 where programa = 'mqop_f021';
delete hdoc_035 where codigo_programa = 'mqop_f020';
delete hdoc_035 where codigo_programa = 'mqop_f021';

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('mqop_f021', 'Incluir componentes das partes', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'mqop_f021', 'menu_mp00', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'mqop_f021', 'menu_mp00', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Incluir componentes de piezas'
 where hdoc_036.codigo_programa = 'mqop_f021'
   and hdoc_036.locale          = 'es_ES';
commit;


insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('mqop_f020', 'Incluir partes das maquinas', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'mqop_f020', 'menu_mp00', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'mqop_f020', 'menu_mp00', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Incluir piezas de la máquina'
 where hdoc_036.codigo_programa = 'mqop_f020'
   and hdoc_036.locale          = 'es_ES';
commit;
