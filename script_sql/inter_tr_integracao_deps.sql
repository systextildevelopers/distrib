
  CREATE OR REPLACE TRIGGER "INTER_TR_INTEGRACAO_DEPS" 
before insert
on INTEGRACAO_DEPS
for each row

-- Finalidade: Atualiza Situacao de bloqueio dos Pedidos Vendas atraves de integracao com o sistema ACPJ fornecida pela DEPS Tecnologia.
-- www.deps.com.br
-- Autor.....: Rodrigo F.
-- Data......: 22/06/2009
--
--
declare
   v_seq_motivos     number(2);
   v_seq_pedido      number(9);
   v_qtde_motivos    number(2);
   v_flag_liberacao  varchar2(1);
   v_qtde_situacao   number(9);
   v_cgc_9           number(9);
   v_cgc_4           number(9);
   v_cgc_2           number(9);
   v_data            date;

begin
   begin
      select nvl(max(integracao_deps.seq_situacao),0) + 1
      into v_seq_pedido
      from integracao_deps
      where integracao_deps.pedido_venda = :new.pedido_venda;

      :new.seq_situacao := v_seq_pedido;
   end;

   /*Se for motivo de bloqueio, o pedido sera passado para 5-Suspenso*/
   if :new.flag_liberacao = 'B' /*BLOQUEIO*/
   then
      begin
         update pedi_100
            set pedi_100.situacao_venda = 5,
                pedi_100.status_pedido  = 0
          where pedi_100.pedido_venda = :new.pedido_venda;
          exception when others then null;
      end;
   end if;

   v_qtde_situacao := 0;

   begin
     select count(*)
     into v_qtde_situacao
     from pedi_135
      where pedi_135.pedido_venda    = :new.pedido_venda
        and pedi_135.codigo_situacao = :new.codigo_situacao;
   end;

   if v_qtde_situacao = 0 /*INSERT*/
   then
      begin
         select nvl(max(pedi_135.seq_situacao),0) + 1 into v_seq_motivos
         from pedi_135
         where pedi_135.pedido_venda = :new.pedido_venda;

         if v_seq_motivos is null
         then v_seq_motivos := 0;
         end if;

         if :new.flag_liberacao = 'L'
         then v_flag_liberacao := 'M'; /*LIBERA*/
         else v_flag_liberacao := 'N'; /*BLOQUEIA*/
         end if;

         if :new.flag_liberacao = 'L'
         then v_data := sysdate;
         else v_data := null;
         end if;

         insert into pedi_135 (pedido_venda,            seq_situacao,
                               codigo_situacao,         data_situacao,
                               flag_liberacao,          data_liberacao,
                               responsavel,             observacao,
                               usuario_bloqueio,        executa_trigger)
                       values (:new.pedido_venda,       v_seq_motivos,
                               :new.codigo_situacao,    trunc(sysdate,'DD'),
                               v_flag_liberacao,        v_data,
                               'SISTEMA DEPS',          '',
                               'SISTEMA DEPS',          0);
      end;
   end if;

   /*Verifica se e para liberar o bloqueio em questao*/
   if v_qtde_situacao > 0 /*UPDATE*/
   then
      begin
         if :new.flag_liberacao = 'L'  /*LIBERAR*/
         then
            begin
               update pedi_135
                  set pedi_135.flag_liberacao = 'M' ,
                      pedi_135.data_liberacao = sysdate,
                      pedi_135.responsavel    = 'SISTEMA DEPS'
                where pedi_135.pedido_venda    = :new.pedido_venda
                  and pedi_135.codigo_situacao = :new.codigo_situacao ;
                exception when others then null;
            end;
         else                         /*BLOQUEAR*/
            begin
               update pedi_135
                  set pedi_135.flag_liberacao = 'N' ,
                      pedi_135.data_liberacao = null
                where pedi_135.pedido_venda    = :new.pedido_venda
                  and pedi_135.codigo_situacao = :new.codigo_situacao ;
            end;
         end if;
      end;
   end if;

   /*Se o motivo for liberado, atualiza as situacoes*/
   if :new.flag_liberacao = 'L' /*LIBERADO*/
   then
      begin
         select count(*)
         into v_qtde_motivos
         from pedi_135
         where pedi_135.pedido_venda   = :new.pedido_venda
           and pedi_135.flag_liberacao = 'N';

         if v_qtde_motivos is null
         then v_qtde_motivos := 0;
         end if;

         if v_qtde_motivos = 0
         then
            begin
               update pedi_100
                  set pedi_100.situacao_venda = 0,
                      pedi_100.status_pedido  = 0
                where pedi_100.pedido_venda   = :new.pedido_venda;
                exception when others then null;
            end;
         end if;
      end;
   end if;

   /*Atualizar o Valor de Credito do Cliente*/
   if :new.valor_credito > 0
   then
      begin

         select pedi_100.cli_ped_cgc_cli9,
                pedi_100.cli_ped_cgc_cli4,
                pedi_100.cli_ped_cgc_cli2
           into v_cgc_9,
                v_cgc_4,
                v_cgc_2
         from pedi_100
         where pedi_100.pedido_venda = :new.pedido_venda;
         exception
            when no_data_found
            then begin
               v_cgc_9 := 0;
               v_cgc_4 := 0;
               v_cgc_2 := 0;
            end;

         begin
            update pedi_012
               set pedi_012.limite2  = :new.valor_credito
             where pedi_012.empresa  = :new.cod_empresa
               and pedi_012.cliente9 = v_cgc_9
               and pedi_012.cliente4 = v_cgc_4
               and pedi_012.cliente2 = v_cgc_2;
            exception when others then null;
         end;

      end;
   end if;
end;

-- ALTER TRIGGER "INTER_TR_INTEGRACAO_DEPS" ENABLE
 

/

exec inter_pr_recompile;

