
  CREATE OR REPLACE TRIGGER "TRIGGER_EMPR_500_LOG" 
BEFORE UPDATE of codigo_empresa, usuario, driver, padrao,
		 saida_default, temp_imp, tipo, descricao,
		 caminho
or DELETE
or INSERT on empr_500
for each row

declare
   ws_sid                          number(9);
   ws_empresa                      number(3);
   ws_tipo_ocorr                   varchar2(1);
   ws_usuario_rede                 varchar2(20);
   ws_maquina_rede                 varchar2(40);
   ws_aplicacao                    varchar2(20);
   ws_locale_usuario               varchar2(50);
   ws_nome_programa                hdoc_090.programa%type;
   ws_tipo                         empr_500_log.tipo%type;
   ws_tipo_old                     empr_500_log.tipo_old%type;
   ws_descricao                    empr_500_log.descricao%type;
   ws_descricao_old                empr_500_log.descricao_old%type;
   ws_caminho                      empr_500_log.caminho%type;
   ws_caminho_old                  empr_500_log.caminho_old%type;
   ws_codigo_empresa               empr_500_log.codigo_empresa%type;
   ws_codigo_empresa_old           empr_500_log.codigo_empresa_old%type;
   ws_usuario                      empr_500_log.usuario%type;
   ws_usuario_old                  empr_500_log.usuario_old%type;
   ws_driver                       empr_500_log.driver%type;
   ws_driver_old                   empr_500_log.driver_old%type;
   ws_padrao                       empr_500_log.padrao%type;
   ws_padrao_old                   empr_500_log.padrao_old%type;
   ws_saida_default                empr_500_log.saida_default%type;
   ws_saida_default_old            empr_500_log.saida_default_old%type;
   ws_usuario_systextil            varchar2(250);

begin
   if INSERTING  then
      ws_tipo                    := :new.tipo;
      ws_descricao               := :new.descricao;
      ws_caminho                 := :new.caminho;
      ws_codigo_empresa          := :new.codigo_empresa;
      ws_usuario                 := :new.usuario;
      ws_driver                  := :new.driver;
      ws_padrao                  := :new.padrao;
      ws_saida_default           := :new.saida_default;

      ws_tipo_old                := '';
      ws_descricao_old           := '';
      ws_caminho_old             := '';
      ws_codigo_empresa_old      := 0;
      ws_usuario_old             := '';
      ws_driver_old              := '';
      ws_padrao_old              := 0;
      ws_saida_default_old       := 0;

      ws_tipo_ocorr              := 'I';
   elsif UPDATING then

      ws_tipo                    := :new.tipo;
      ws_descricao               := :new.descricao;
      ws_caminho                 := :new.caminho;
      ws_codigo_empresa          := :new.codigo_empresa;
      ws_usuario                 := :new.usuario;
      ws_driver                  := :new.driver;
      ws_padrao                  := :new.padrao;
      ws_saida_default           := :new.saida_default;

      ws_tipo_old                := :old.tipo;
      ws_descricao_old           := :old.descricao;
      ws_caminho_old             := :old.caminho;
      ws_codigo_empresa_old      := :old.codigo_empresa;
      ws_usuario_old             := :old.usuario;
      ws_driver_old              := :old.driver;
      ws_padrao_old              := :old.padrao;
      ws_saida_default_old       := :old.saida_default;

      ws_tipo_ocorr              := 'A';

      elsif DELETING then

         ws_tipo                    := '';
         ws_descricao               := '';
         ws_caminho                 := '';
         ws_codigo_empresa          := 0;
         ws_usuario                 := '';
         ws_driver                  := '';
         ws_padrao                  := 0;
         ws_saida_default           := 0;

         ws_tipo_old                := :old.tipo;
         ws_descricao_old           := :old.descricao;
         ws_caminho_old             := :old.caminho;
         ws_codigo_empresa_old      := :old.codigo_empresa;
         ws_usuario_old             := :old.usuario;
         ws_driver_old              := :old.driver;
         ws_padrao_old              := :old.padrao;
         ws_saida_default_old       := :old.saida_default;

         ws_tipo_ocorr              := 'D';
   end if;

   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicacao,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ws_nome_programa := inter_fn_nome_programa(ws_sid);                            

   INSERT INTO empr_500_log
   ( tipo,                   tipo_old,              descricao,
     descricao_old,          caminho,               caminho_old,
     codigo_empresa,         codigo_empresa_old,    usuario,
     usuario_old,            driver,                driver_old,
     padrao,                 padrao_old,            saida_default,
     saida_default_old,      data_ocorr,            tipo_ocorr,
     usuario_rede,           maquina_rede,          aplicacao,
     processo_systextil,     usuario_systextil )

   VALUES
   ( ws_tipo,                ws_tipo_old,           ws_descricao,
     ws_descricao_old,       ws_caminho,            ws_caminho_old,
     ws_codigo_empresa,       ws_codigo_empresa_old, ws_usuario,
     ws_usuario_old,         ws_driver,             ws_driver_old,
     ws_padrao,              ws_padrao_old,         ws_saida_default,
     ws_saida_default_old,   sysdate,               ws_tipo_ocorr,
     ws_usuario_rede,        ws_maquina_rede,       ws_aplicacao,
     ws_nome_programa,       ws_usuario_systextil);

end trigger_empr_500_log;


-- ALTER TRIGGER "TRIGGER_EMPR_500_LOG" ENABLE
 

/

exec inter_pr_recompile;

