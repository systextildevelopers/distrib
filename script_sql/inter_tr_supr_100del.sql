create or replace trigger inter_tr_supr_100del
   before delete on supr_100
   for each row

declare
  v_situacao_pedido supr_090.situacao_pedido%type;
  v_cod_cancel      supr_090.cod_cancelamento%type;
   
begin
   if deleting 
   then   
      select supr_090.situacao_pedido,  supr_090.cod_cancelamento
      into   v_situacao_pedido,         v_cod_cancel       
      from supr_090
      where supr_090.pedido_compra = :old.num_ped_compra;      
      
      -- se o iem estiver cancelado n�o deixa eliminar o item
      if v_cod_cancel > 0
      then 
          raise_application_error(-20100,'Pedido j� cancelado n�o permite eliminar o item.');
      end if;     
     
      -- se o iem estiver cancelado n�o deixa eliminar o item     
      if v_situacao_pedido in (2,3,4) 
      then 
          raise_application_error(-20101,'Situa��o do pedido n�o permite eliminar o item.');
      end if;
   end if;         
end inter_tr_supr_100;
/
/* versao: 1 */
