alter table empr_100
add (   conta_ctb_tarifa       number(6) default 0,
        historico_estorno      number(4) default 0,
        historico_devolucao    number(4) default 0,
        hist_dev_parcial       number(4) default 0, 
        conta_ctb_dev_parcial  number(6) default 0,
        historico_bonificacao  number(4) default 0,                
        conta_ctb_bonificacao  number(6) default 0,
        tipo_titulo_tarifa     number(4) default 0);

EXEC inter_pr_recompile;
/
