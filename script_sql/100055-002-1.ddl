alter table pedi_080
add ind_natur_cupom_ref number(1) default 0;

comment on column pedi_080.ind_natur_cupom_ref is '0 - Indica natureza N�o utilizada a cupom relacionado a NF-e (Normal) Ou 1 - Indica natureza utilizada a cupom relacionado a NF-e';

exec inter_pr_recompile;
/
