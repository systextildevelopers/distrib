
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_030_DEL_MENS" 
AFTER  DELETE

on fatu_030

for each row

declare
  cd_empresa number(3);
  cd_msg_tab fatu_502.cod_mensagem_tabela%type;
  cd_msg_box fatu_502.cod_mensagem_tabela%type;
  cd_msg_age fatu_502.cod_mensagem_tabela%type;

begin

   select pedi_100.codigo_empresa
     into cd_empresa
   from pedi_100
   where pedi_100.pedido_venda = :old.pedido_venda;

   select fatu_502.cod_mensagem_tabela, fatu_502.cod_mensagem_box, fatu_502.cod_mensagem_agend
     into cd_msg_tab,                   cd_msg_box,                 cd_msg_age
   from fatu_502
   where fatu_502.codigo_empresa = cd_empresa;

   delete from pedi_101
   where pedi_101.num_pedido    = :old.pedido_venda
     and pedi_101.cod_mensagem  in (cd_msg_tab,cd_msg_box,cd_msg_age);
	 
   delete from pedi_101
   where pedi_101.num_pedido    = :old.pedido_venda
     and pedi_101.ind_msg_pedido_solicitacao = 1;

end INTER_TR_FATU_030_DEL_MENS;

-- ALTER TRIGGER "INTER_TR_FATU_030_DEL_MENS" ENABLE
 

/

exec inter_pr_recompile;

