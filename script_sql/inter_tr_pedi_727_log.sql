
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_727_LOG" 
  before insert
  on pedi_727
  for each row
declare

  v_usuario_systextil    varchar2(250);
  v_usuario_rede         varchar2(20);
  v_sid                  number(9);
  v_empresa              number(3);
  v_locale_usuario       varchar2(5);
  v_maquina_rede         varchar(40);
  v_aplicativo           varchar(20);

  v_reg                  number(9);

begin

   select count(*) into v_reg
   from pedi_727_log
   where pedi_727_log.codigo_empresa  = :new.codigo_empresa
     and pedi_727_log.codigo_segmento = :new.codigo_segmento
     and pedi_727_log.codigo_repres   = :new.codigo_repres;

   if v_reg = 0
   then

      -- Dados do usu�rio logado
      inter_pr_dados_usuario (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                              v_usuario_systextil,   v_empresa,        v_locale_usuario);

      insert into pedi_727_log (
          codigo_empresa,          codigo_segmento,
          codigo_repres,           data_ocorrencia,
          situacao,                usuario_rede,
          usuario_systextil,       cod_motivo
          )
        values (
          :new.codigo_empresa,     :new.codigo_segmento,
          :new.codigo_repres,      trunc(sysdate(),'DD'),
          :new.situacao,           v_usuario_rede,
          v_usuario_systextil,     1
          );
   end if;

end inter_tr_pedi_727_log;

-- ALTER TRIGGER "INTER_TR_PEDI_727_LOG" ENABLE
 

/

exec inter_pr_recompile;

