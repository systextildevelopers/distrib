
  CREATE OR REPLACE FUNCTION "INTER_FN_ATUALIZA_SIT_COBRANCA" (p_empresa in number, p_nova_sit_cobranca number)
return varchar2
is
   erro_mensagem varchar2(250) := ' ';

begin
  begin
     inter_pr_atualiza_sit_cobranca(p_empresa, p_nova_sit_cobranca);

     exception
        when others then
           erro_mensagem := SQLERRM;
  end;

  return(erro_mensagem);
end inter_fn_atualiza_sit_cobranca;


 

/

exec inter_pr_recompile;

