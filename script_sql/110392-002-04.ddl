create table cont_k200 (
COD_CONTROLADORA    	NUMBER(9) 		DEFAULT 0,
EXERCICIO           	NUMBER(9) 		DEFAULT 0,
COD_NAT 				VARCHAR2(2) 	DEFAULT ' ',
IND_CTA 				VARCHAR2(1) 	CHECK(IND_CTA IN ('S','A')) NOT NULL,
NIVEL 					NUMBER(9) 		DEFAULT 0,
COD_CTA 				VARCHAR2(20) 	DEFAULT ' ',
COD_CTA_SUP 			VARCHAR2(20) 	DEFAULT ' ',
CTA 					VARCHAR2(400) 	DEFAULT ' ',
VAL_AG               	NUMBER(19,2) 	DEFAULT 0,
IND_VAL_AG           	VARCHAR2(1) 	CHECK(IND_VAL_AG IN ('D','C')) NOT NULL,
VAL_EL               	NUMBER(19,2)	DEFAULT 0,
IND_VAL_EL           	VARCHAR2(1) 	CHECK(IND_VAL_EL IN ('D','C')) NOT NULL,
VAL_CS               	NUMBER(19,2) 	DEFAULT 0,
IND_VAL_CS           	VARCHAR2(1) 	CHECK(IND_VAL_CS IN ('D','C')) NOT NULL
);

alter table cont_k200
	add constraint PK_cont_k200 primary key (COD_CONTROLADORA,EXERCICIO,COD_CTA);
  
comment on table cont_k200 is 'Plano de Contas Consolidado';
comment on column cont_k200.COD_CONTROLADORA 	is 'código da Empresa Controladora';
comment on column cont_k200.EXERCICIO 			is 'Exercicio a que se refere as informações da empresa no período';
comment on column cont_k200.COD_NAT 			is 'Código da Natureza da Conta';
comment on column cont_k200.IND_CTA 			is 'Indicador do Tipo de Conta';
comment on column cont_k200.NIVEL 				is 'Nível da Conta';
comment on column cont_k200.COD_CTA 			is 'Código da Conta';
comment on column cont_k200.COD_CTA_SUP 		is 'Código da Conta de Nível Superior';
comment on column cont_k200.CTA 				is 'Nome da Conta';
comment on column cont_k200.VAL_AG       		is 'Valor absoluto aglutinado';
comment on column cont_k200.IND_VAL_AG   		is 'Indicador da situacao do valor aglutinado D - Devedor C - Credor';
comment on column cont_k200.VAL_EL       		is 'Valor absoluto das eliminacoes';
comment on column cont_k200.IND_VAL_EL   		is 'Indicador da situacao do valor eliminado : D - devedor C - credor';
comment on column cont_k200.VAL_CS       		is 'Valor absoluto consolidado: VAL_CS = VAL_AG - VAL_EL';
comment on column cont_k200.IND_VAL_CS   		is 'Indicador da situacao do valor consolidado: D - Devedor C - Credor';
