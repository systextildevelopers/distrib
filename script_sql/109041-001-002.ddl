insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('oper_f111', 'Controle de Senhas', 0,1);
insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'oper_f111', 'oper_menu', 1, 20, 'S', 'S', 'S', 'S');
update hdoc_036
   set hdoc_036.descricao       = 'Controle de Senhas'
 where hdoc_036.codigo_programa = 'oper_f111'
   and hdoc_036.locale          = 'es_ES';
commit;
