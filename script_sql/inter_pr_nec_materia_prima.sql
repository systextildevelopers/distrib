
  CREATE OR REPLACE PROCEDURE "INTER_PR_NEC_MATERIA_PRIMA" (
    p_empresa_filtro  in number,
    p_data_ini_venda  in date,
    p_data_fim_venda  in date,
    p_nivel_material  in varchar2,
    p_tipo_material   in number,
    p_inc_exc_dep     in number,
    p_codigo_usuario  in number,
    p_usuario         in varchar2,
    p_empresa_logada  in number,
    p_nr_solicitacao  in number,
    p_cons_transito   in number,
    p_data_ini_compra in date,
    p_data_fim_compra in date,
    p_inc_exc_aplic   in number,
    p_inc_exc_produto in number,
    p_visualiza_mp    in number
) is
   /*Declara��o das vari�veis usadas para componentes de projeto*/
   TYPE projNivelComp IS TABLE OF basi_030.nivel_estrutura%TYPE;
   proj_nivel_comp projNivelComp;

   TYPE projGrupoComp IS TABLE OF basi_030.referencia%TYPE;
   proj_grupo_comp projGrupoComp;

   TYPE projSubComp IS TABLE OF basi_020.tamanho_ref%TYPE;
   proj_sub_comp projSubComp;

   TYPE projItemComp IS TABLE OF basi_010.item_estrutura%TYPE;
   proj_item_comp projItemComp;

   TYPE projConsumo IS TABLE OF basi_101.consumo%TYPE;
   proj_consumo projConsumo;

   /*Declara��o das vari�veis usadas para pedido*/
   TYPE pediCodProj IS TABLE OF basi_001.codigo_projeto%TYPE;
   pedi_cod_proj pediCodProj;

   TYPE nivelPedido IS TABLE OF basi_030.nivel_estrutura%TYPE;
   pedi_nivel nivelPedido;

   TYPE grupoPedido IS TABLE OF basi_030.referencia%TYPE;
   pedi_grupo grupoPedido;

   TYPE subgrupoPedido IS TABLE OF basi_020.tamanho_ref%TYPE;
   pedi_subgrupo subgrupoPedido;

   TYPE itemPedido IS TABLE OF basi_010.item_estrutura%TYPE;
   pedi_item itemPedido;

   TYPE qtdePedida IS TABLE OF pedi_110.qtde_pedida%TYPE;
   qtde_pedida qtdePedida;

   /*Declara��o das vari�veis usadas para componentes de pedido*/
   TYPE pediNivelComp IS TABLE OF basi_030.nivel_estrutura%TYPE;
   pedi_nivel_comp pediNivelComp;

   TYPE pediGrupoComp IS TABLE OF basi_030.referencia%TYPE;
   pedi_grupo_comp pediGrupoComp;

   TYPE pediSubComp IS TABLE OF basi_020.tamanho_ref%TYPE;
   pedi_sub_comp pediSubComp;

   TYPE pediItemComp IS TABLE OF basi_010.item_estrutura%TYPE;
   pedi_item_comp pediItemComp;

   TYPE pediConsumo IS TABLE OF basi_101.consumo%TYPE;
   pedi_consumo pediConsumo;

   TYPE pedidoVenda IS TABLE OF pedi_110.pedido_venda%TYPE;
   pedido_venda_lido pedidoVenda;

   TYPE situacaoProjPedi IS TABLE OF basi_001.situacao_projeto%TYPE;
   situacao_proj_pedi situacaoProjPedi;

   flag_estr_proj        varchar2(1);
   flag_estr_pedi        varchar2(1);
   tem_basi050           number;
   pendente              varchar2(1);
   flag_visualiza_mp     number(1);
   subgrupo_615          basi_020.tamanho_ref%TYPE;
   pedi_qtde_total       number(15,5);
   proj_qtde_total       number(15,5);
   alternativa_aux       basi_050.alternativa_item%TYPE;
   qtde_reservada_pedido tmrp_630.qtde_produzida_comprada%TYPE;
   responsavel           basi_030.responsavel%TYPE;
   proj_nivel_item       basi_030.nivel_estrutura%TYPE;
   proj_grupo_item       basi_030.referencia%TYPE;

PROCEDURE gerar_registro_aplicacao(
    p_codigo_usuario  in number,
    p_usuario         in varchar2,
    p_nr_solicitacao  in number,
    p_nome_programa   in varchar2,
    p_empresa_logada in number)
IS

BEGIN
   begin
      insert into /*+APPEND*/ tmrp_615 (
              nome_programa,                nr_solicitacao,
              codigo_usuario,               tipo_registro,
              usuario,                      codigo_empresa,
              nivel,                        grupo,
              subgrupo,                     item,
              sequencia_projeto,            observacao2)
      select  'tmrp_f025',                  p_nr_solicitacao,
              p_codigo_usuario,             345,
              p_usuario,                    p_empresa_logada,
              basi_400.nivel,               basi_400.grupo,
              basi_400.subgrupo,            basi_400.item,
              basi_400.codigo_informacao,   hdoc_001.descricao
      from basi_400, hdoc_001
      where basi_400.tipo_informacao   = 11
        and basi_400.nivel             = '2'
        and basi_400.tipo_informacao   = hdoc_001.tipo
        and basi_400.codigo_informacao = hdoc_001.codigo;
   end;

   begin
      insert into /*+APPEND*/ tmrp_615 (
              nome_programa,                nr_solicitacao,
              codigo_usuario,               tipo_registro,
              usuario,                      codigo_empresa,
              nivel,                        grupo,
              subgrupo,                     item,
              sequencia_projeto,            observacao2)
      select  p_nome_programa,              p_nr_solicitacao,
              p_codigo_usuario,             345,
              p_usuario,                    p_empresa_logada,
              basi_020.basi030_nivel030,    basi_020.basi030_referenc,
              basi_020.tamanho_ref,         '000000',
              0,                            'PADR�O'
      from basi_020
      where basi_020.basi030_nivel030 = '2'
        and not exists (select 1
                        from basi_400
                        where basi_400.tipo_informacao = 11
                          and basi_400.nivel           = basi_020.basi030_nivel030
                          and basi_400.grupo           = basi_020.basi030_referenc
                          and basi_400.subgrupo        = basi_020.tamanho_ref);
   end;

   begin
      insert into /*+APPEND*/ tmrp_615 (
              nome_programa,                nr_solicitacao,
              codigo_usuario,               tipo_registro,
              usuario,                      codigo_empresa,
              nivel,                        grupo,
              subgrupo,                     item,
              sequencia_projeto,            observacao2)
      select  p_nome_programa,              p_nr_solicitacao,
              p_codigo_usuario,             345,
              p_usuario,                    p_empresa_logada,
              basi_020.basi030_nivel030,    basi_020.basi030_referenc,
              basi_020.tamanho_ref,         '000000',
              0,                            'PADR�O'
      from basi_020
      where basi_020.basi030_nivel030 = '9';
   end;

end;

/*
   INICIO
*/
BEGIN
   gerar_registro_aplicacao(p_codigo_usuario,
                            p_usuario,
                            p_nr_solicitacao,
                            'tmrp_f025',
                            p_empresa_logada);

   flag_visualiza_mp := 1;

   if p_visualiza_mp = 3
   then
      flag_visualiza_mp := p_visualiza_mp;
   end if;
   FOR reg_dados IN (/* 1
                     L� os projetos, cujas refer�ncias n�o estejam em pedidos de venda dentro do per�odo informado
                     mas que tenha uma solicita��o de desenvolvimento de produto dentro do per�odo informado,
                     A qtde vem da combina��o n�o mais das solicita��o de desenvolvimento de produto
                     e que n�o tenham datas informadas (basi_866). */
                     select /*parallel(pedi_666, pedi_667, basi_001, basi_843, 20)*/
                            basi_001.codigo_projeto,        basi_001.situacao_projeto,
                            basi_001.nivel_produto,         basi_001.grupo_produto,
                            basi_843.combinacao_item,
                            nvl(sum(basi_843.qtde_reservada),0)    as qtde_necessaria
                     from pedi_666, pedi_667, basi_001, basi_843
                     where pedi_666.cod_solicitacao_dp       = pedi_667.cod_solicitacao_dp
                       and pedi_666.seq_solicitacao_dp       = pedi_667.seq_solicitacao_dp
                       and pedi_666.data_requerida_comercial between p_data_ini_venda and p_data_fim_venda
                       and pedi_666.codigo_projeto           = basi_001.codigo_projeto
                       and basi_843.codigo_projeto           = basi_001.codigo_projeto
                       and basi_843.situacao_comb            in (0,2) /* 0-ATIVO     1-INATIVO        2-LAN�AMENTO */
                       and basi_843.qtde_reservada           > 0
                       and basi_001.codigo_projeto           is not null
                       and basi_001.codigo_projeto           <> ' '
                       and basi_001.nivel_produto            = '1'
                       and basi_001.situacao_projeto         <> 3
                       and not exists (select 1  /*parallel(pedi_110, pedi_100, 20)*/
                                       from pedi_110, pedi_100
                                       where pedi_100.codigo_empresa    = p_empresa_filtro
                                         and pedi_110.pedido_venda      = pedi_100.pedido_venda
                                         and pedi_100.data_entr_venda   between p_data_ini_venda and p_data_fim_venda
                                         and pedi_110.cd_it_pe_nivel99  = basi_001.nivel_produto
                                         and pedi_110.cd_it_pe_grupo    = basi_001.grupo_produto
                                         and pedi_110.cd_it_pe_item     = basi_843.combinacao_item
                                         and pedi_110.cod_cancelamento  = 0)
                       and exists (select 1 from efic_050
                                    where efic_050.cod_empresa     = p_empresa_filtro
                                      and efic_050.cod_funcionario = basi_001.codigo_responsavel)
                       and not exists (select 1
                                      from basi_866
                                      where basi_866.codigo_projeto   = basi_843.codigo_projeto
                                        and basi_866.combinacao_item  = basi_843.combinacao_item)
                    group by basi_001.codigo_projeto,    basi_001.situacao_projeto,
                             basi_001.nivel_produto,     basi_001.grupo_produto,
                             basi_843.combinacao_item
                    UNION ALL
                    /* 2
                       L� os projetos, cujas refer�ncias n�o estejam em pedidos de venda dentro do per�odo informado
                       a qtde vem da combina��o n�o mais das solicita��o de desenvolvimento de produto
                       e que TENHAM datas informadas (basi_866).
                    */
                    select  /*parallel(pedi_666, pedi_667, basi_001, 20)*/
                           basi_001.codigo_projeto,        basi_001.situacao_projeto,
                           basi_001.nivel_produto,         basi_001.grupo_produto,
                           basi_843.combinacao_item,
                           nvl(sum(basi_866.quantidade),0)      as qtde_necessaria
                    from basi_001, basi_843, basi_866
                    where basi_001.codigo_projeto           is not null
                      and basi_001.codigo_projeto           <> ' '
                      and basi_001.nivel_produto            = '1'
                      and basi_001.situacao_projeto         <> 3
                      and basi_843.codigo_projeto           = basi_001.codigo_projeto
                      and basi_843.situacao_comb            in (0,2) /* 0-ATIVO     1-INATIVO        2-LAN�AMENTO */
                      and basi_843.qtde_reservada           > 0
                      and basi_866.codigo_projeto           = basi_843.codigo_projeto
                      and basi_866.combinacao_item          = basi_843.combinacao_item
                      and basi_866.data_entrega             between p_data_ini_venda and p_data_fim_venda
                      and not exists (select 1  /*parallel(pedi_110, pedi_100, 20)*/
                                      from pedi_110, pedi_100
                                      where pedi_100.codigo_empresa    = p_empresa_filtro
                                        and pedi_110.pedido_venda      = pedi_100.pedido_venda
                                        and pedi_100.data_entr_venda   between p_data_ini_venda and p_data_fim_venda
                                        and pedi_110.cd_it_pe_nivel99  = basi_001.nivel_produto
                                        and pedi_110.cd_it_pe_grupo    = basi_001.grupo_produto
                                        and pedi_110.cd_it_pe_item     = basi_843.combinacao_item
                                        and pedi_110.cod_cancelamento  = 0)
                      and exists (select 1 from efic_050
                                   where efic_050.cod_empresa     = p_empresa_filtro
                                     and efic_050.cod_funcionario = basi_001.codigo_responsavel)
                    group by basi_001.codigo_projeto, basi_001.situacao_projeto,
                             basi_001.nivel_produto,  basi_001.grupo_produto,
                             basi_843.combinacao_item
                    UNION ALL
                    /* 3
                      L� os projetos, cujas refer�ncias estejam em pedidos de venda dentro do per�odo informado
                      a qtde vem da combina��o(BASI_866) n�o mais das solicita��o de desenvolvimento de produto
                      e que TENHAM datas informadas (basi_866) que sejam maior que a maior data de pedido.
                    */
                    select  /*parallel(pedi_666, pedi_667, basi_001, 20)*/
                           basi_001.codigo_projeto,        basi_001.situacao_projeto,
                           basi_001.nivel_produto,         basi_001.grupo_produto,
                           basi_843.combinacao_item,
                           nvl(sum(basi_866.quantidade),0)      as qtde_necessaria
                    from basi_001, basi_843, basi_866
                    where basi_001.codigo_projeto           is not null
                      and basi_001.codigo_projeto           <> ' '
                      and basi_001.nivel_produto            = '1'
                      and basi_001.situacao_projeto         <> 3
                      and basi_843.codigo_projeto           = basi_001.codigo_projeto
                      and basi_843.situacao_comb            in (0,2) /* 0-ATIVO     1-INATIVO        2-LAN�AMENTO */
                      and basi_843.qtde_reservada           > 0
                      and basi_866.codigo_projeto           = basi_843.codigo_projeto
                      and basi_866.combinacao_item          = basi_843.combinacao_item
                      and basi_866.data_entrega             between p_data_ini_venda and p_data_fim_venda
                      and exists (select 1  /*parallel(pedi_110, pedi_100, 20)*/
                                  from pedi_110, pedi_100
                                  where pedi_100.codigo_empresa    = p_empresa_filtro
                                    and pedi_110.pedido_venda      = pedi_100.pedido_venda
                                    and pedi_100.data_entr_venda   between p_data_ini_venda and p_data_fim_venda
                                    and basi_866.data_entrega      > pedi_100.data_entr_venda
                                    and pedi_110.cd_it_pe_nivel99  = basi_001.nivel_produto
                                    and pedi_110.cd_it_pe_grupo    = basi_001.grupo_produto
                                    and pedi_110.cd_it_pe_item     = basi_843.combinacao_item
                                    and pedi_110.cod_cancelamento  = 0)
                      and exists (select 1 from efic_050
                                   where efic_050.cod_empresa     = p_empresa_filtro
                                     and efic_050.cod_funcionario = basi_001.codigo_responsavel)
                   group by basi_001.codigo_projeto, basi_001.situacao_projeto,
                            basi_001.nivel_produto,  basi_001.grupo_produto,
                            basi_843.combinacao_item)
   LOOP
      /*
         Verifica se a refer�ncia est� PENDENTE
      */
      begin
         select basi_030.responsavel
         into   responsavel
         from basi_030
         where  basi_030.nivel_estrutura = reg_dados.nivel_produto
           and  basi_030.referencia      = reg_dados.grupo_produto
           and (basi_030.responsavel like '%pendente%'
            or  basi_030.responsavel like '%PENDENTE%');
      exception when others then
         pendente := 'n';
      end;

      if sql%found
      then
         pendente := 's';
      else
         pendente := 'n';
      end if;
      /*Fim - Pendente*/

      flag_estr_proj := 'n';

      if  reg_dados.grupo_produto       <> '00000'
      and pendente            = 'n'
      and reg_dados.situacao_projeto = 2
      then
         begin
            select basi_050.sub_item, basi_050.alternativa_item
            into   subgrupo_615,      alternativa_aux
            from   basi_050
            where  basi_050.nivel_item = reg_dados.nivel_produto
              and  basi_050.grupo_item = reg_dados.grupo_produto
              and (basi_050.item_item  = reg_dados.combinacao_item
               or  basi_050.item_item  = '000000');
         exception when others then
            subgrupo_615 := '';
         end;

         if  sql%found
         and subgrupo_615 <> '000'
         then
            inter_pr_estrutura(p_empresa_logada,
                               reg_dados.nivel_produto,
                               reg_dados.grupo_produto,
                               subgrupo_615,
                               reg_dados.combinacao_item,
                               alternativa_aux,
                               reg_dados.qtde_necessaria,
                               p_nivel_material,
                               p_tipo_material,
                               p_codigo_usuario,
                               p_usuario,
                               p_nr_solicitacao,
                               flag_estr_proj,
                               'tmrp_f025',
                               0,
                               reg_dados.codigo_projeto,
                               p_inc_exc_aplic,
                               p_inc_exc_produto,
                               flag_visualiza_mp);
         else
            begin
               select count(*)
               into   tem_basi050
               from basi_050
               where basi_050.nivel_item = reg_dados.nivel_produto
                 and basi_050.grupo_item = reg_dados.grupo_produto
                 and basi_050.sub_item   = '000'
                 and basi_050.item_item  = '000000';
            exception when others then
               tem_basi050 := 0;
            end;

            if sql%found and tem_basi050 > 0
            then
               inter_pr_estrutura(p_empresa_logada,
                                  reg_dados.nivel_produto,
                                  reg_dados.grupo_produto,
                                  '000',
                                  '000000',
                                  alternativa_aux,
                                  reg_dados.qtde_necessaria,
                                  p_nivel_material,
                                  p_tipo_material,
                                  p_codigo_usuario,
                                  p_usuario,
                                  p_nr_solicitacao,
                                  flag_estr_proj,
                                  'tmrp_f025',
                                  0,
                                  reg_dados.codigo_projeto,
                                  p_inc_exc_aplic,
                                  p_inc_exc_produto,
                                  flag_visualiza_mp);
            end if;
         end if;
      end if;

      if flag_estr_proj = 'n'
      then
         begin
             select /*parallel(basi_850, basi_030, 20)*/
                   basi_850.niv_combinacao, basi_850.gru_combinacao,
                   basi_850.sub_combinacao, basi_850.ite_combinacao,
                   basi_850.consumo_comb
            BULK COLLECT INTO
                   proj_nivel_comp,         proj_grupo_comp,
                   proj_sub_comp,           proj_item_comp,
                   proj_consumo
            from basi_850, basi_030, tmrp_615
            where  basi_850.codigo_projeto   = reg_dados.codigo_projeto
              and  basi_850.combinacao_item  = reg_dados.combinacao_item
              and  basi_850.niv_combinacao   = basi_030.nivel_estrutura
              and  basi_850.gru_combinacao   = basi_030.referencia
              and (basi_030.comprado_fabric  = p_tipo_material or p_tipo_material = 3)
              and ((basi_850.niv_combinacao  = p_nivel_material)
              or   ((basi_850.niv_combinacao = '2'
              or     basi_850.niv_combinacao = '9')
              and    p_nivel_material        = '0'))
              /* PRODUTO */
              and ((p_inc_exc_produto = 1
              and   exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060       /* INCLUS�O */
                            where rcnb_060.tipo_registro        = 599
                              and rcnb_060.nr_solicitacao       = 25
                              and rcnb_060.subgru_estrutura     = p_codigo_usuario
                              and rcnb_060.usuario_rel          = p_usuario
                              and rcnb_060.empresa_rel          = p_empresa_logada
                              and rcnb_060.relatorio_rel        = 'tmrp_f025'
                              and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                              and rcnb_060.nivel_estrutura_str  in (basi_850.niv_combinacao, 'X')
                              and rcnb_060.grupo_estrutura_str  in (basi_850.gru_combinacao, 'XXXXXX')))
              or  (p_inc_exc_produto = 2
              and  not exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060   /* EXCLUS�O */
                               where rcnb_060.tipo_registro        = 599
                                 and rcnb_060.nr_solicitacao       = 25
                                 and rcnb_060.subgru_estrutura     = p_codigo_usuario
                                 and rcnb_060.usuario_rel          = p_usuario
                                 and rcnb_060.empresa_rel          = p_empresa_logada
                                 and rcnb_060.relatorio_rel        = 'tmrp_f025'
                                 and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                                 and rcnb_060.nivel_estrutura_str  in (basi_850.niv_combinacao,'X')
                                 and (rcnb_060.grupo_estrutura_str = basi_850.gru_combinacao
                                 or   rcnb_060.grupo_estrutura_str = 'XXXXXX'))
                                  or exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060
                         where rcnb_060.tipo_registro        = 599
                           and rcnb_060.nr_solicitacao       = 25
                           and rcnb_060.subgru_estrutura     = p_codigo_usuario
                           and rcnb_060.usuario_rel          = p_usuario
                           and rcnb_060.empresa_rel          = p_empresa_logada
                           and rcnb_060.relatorio_rel        = 'tmrp_f025'
                           and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                           and rcnb_060.nivel_estrutura_str  in (basi_850.niv_combinacao,'X')
                           and rcnb_060.grupo_estrutura_str  = 'XXXXXX')))
              /* APLICA��O */
              and tmrp_615.nr_solicitacao = p_nr_solicitacao
              and tmrp_615.codigo_usuario = p_codigo_usuario
              and tmrp_615.usuario        = p_usuario
              and tmrp_615.codigo_empresa = p_empresa_logada
              and tmrp_615.nome_programa  = 'tmrp_f025'
              and tmrp_615.tipo_registro  = 345
              and tmrp_615.nivel          = basi_850.niv_combinacao
              and tmrp_615.grupo          = basi_850.gru_combinacao
              and tmrp_615.subgrupo       = basi_850.sub_combinacao
              and tmrp_615.item           = '000000'
              and ((p_inc_exc_aplic = 1
              and   tmrp_615.sequencia_projeto in (select /*parallel(rcnb_060, 20)*/
                                                          rcnb_060.grupo_estrutura
                                                   from rcnb_060
                                                   where rcnb_060.tipo_registro     = 598
                                                     and rcnb_060.nr_solicitacao    = 25
                                                     and rcnb_060.subgru_estrutura  = p_codigo_usuario
                                                     and rcnb_060.usuario_rel       = p_usuario
                                                     and rcnb_060.empresa_rel       = p_empresa_logada
                                                     and rcnb_060.relatorio_rel     = 'tmrp_f025'
                                                     and rcnb_060.tipo_registro_rel = p_nr_solicitacao))
              or  (p_inc_exc_aplic = 2
              and  tmrp_615.sequencia_projeto not in (select /*parallel(rcnb_060, 20)*/
                                                             rcnb_060.grupo_estrutura
                                                      from rcnb_060
                                                      where rcnb_060.tipo_registro     = 598
                                                        and rcnb_060.nr_solicitacao    = 25
                                                        and rcnb_060.subgru_estrutura  = p_codigo_usuario
                                                        and rcnb_060.usuario_rel       = p_usuario
                                                        and rcnb_060.empresa_rel       = p_empresa_logada
                                                        and rcnb_060.relatorio_rel     = 'tmrp_f025'
                                                        and rcnb_060.tipo_registro_rel = p_nr_solicitacao)));
         end;

         if sql%found
         then
            FOR j IN proj_nivel_comp.FIRST .. proj_nivel_comp.LAST
            LOOP
               begin
                  select basi_001.nivel_produto,   basi_001.grupo_produto
                  into   proj_nivel_item,          proj_grupo_item
                  from basi_001
                  where basi_001.codigo_projeto = reg_dados.codigo_projeto;
               exception when others then
                  proj_nivel_item := '0';
                  proj_grupo_item := '00000';
               end;

               proj_qtde_total := proj_consumo(j) * reg_dados.qtde_necessaria;

               inter_pr_grava_tmrp_615_item(p_empresa_logada,
                                            proj_nivel_item,
                                            proj_grupo_item,
                                            reg_dados.combinacao_item,
                                            0,
                                            reg_dados.qtde_necessaria,
                                            proj_nivel_comp(j),
                                            proj_grupo_comp(j),
                                            proj_sub_comp(j),
                                            proj_item_comp(j),
                                            p_codigo_usuario,
                                            p_usuario,
                                            p_nr_solicitacao,
                                            'tmrp_f026',
                                            256,
                                            'basi_850',
                                            proj_consumo(j),
                                            0,
                                            reg_dados.codigo_projeto,
                                            1);

                inter_pr_grava_tmrp_615(p_empresa_logada,
                                       proj_nivel_comp(j),
                                       proj_grupo_comp(j),
                                       proj_sub_comp(j),
                                       proj_item_comp(j),
                                       proj_qtde_total,
                                       p_codigo_usuario,
                                       p_usuario,
                                       p_nr_solicitacao,
                                       'tmrp_f025',
                                       255,
                                       1,
                                       flag_visualiza_mp);
            END LOOP;

            /*
               Se n�o encontra nas combina��es busca dos componentes do projeto (BASI_101)
            */
         else
            begin
               select /*parallel(basi_101, basi_030, 20)*/
                      basi_101.nivel_produto,    basi_101.grupo_produto,
                      basi_101.subgrupo_produto, basi_101.item_produto,
                      basi_101.consumo
               BULK COLLECT INTO
                      proj_nivel_comp,           proj_grupo_comp,
                      proj_sub_comp,             proj_item_comp,
                      proj_consumo
                from basi_101, basi_030, tmrp_615
                where  basi_101.codigo_projeto   = reg_dados.codigo_projeto
                  and  basi_101.nivel_produto    = basi_030.nivel_estrutura
                  and  basi_101.grupo_produto    = basi_030.referencia
                  and (basi_030.comprado_fabric  = p_tipo_material or p_tipo_material = 3)
                  and ((basi_101.nivel_produto   = p_nivel_material)
                  or   ((basi_101.nivel_produto  = '2'
                  or     basi_101.nivel_produto  = '9')
                  and    p_nivel_material        = '0'))
                  and  basi_101.grupo_produto    is not null
                  and  basi_101.grupo_produto    <> ' '
                  and  basi_101.grupo_produto    <> '00000'
                  and  basi_101.subgrupo_produto is not null
                  and  basi_101.subgrupo_produto <> ' '
                  and  basi_101.subgrupo_produto <> '000'
                  and  basi_101.item_produto     is not null
                  and  basi_101.item_produto     <> ' '
                  and  basi_101.item_produto     <> '000000'
                  /* PRODUTO */
                  and ((p_inc_exc_produto = 1
                  and   exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060       /* INCLUS�O */
                                where rcnb_060.tipo_registro        = 599
                                  and rcnb_060.nr_solicitacao       = 25
                                  and rcnb_060.subgru_estrutura     = p_codigo_usuario
                                  and rcnb_060.usuario_rel          = p_usuario
                                  and rcnb_060.empresa_rel          = p_empresa_logada
                                  and rcnb_060.relatorio_rel        = 'tmrp_f025'
                                  and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                                  and rcnb_060.nivel_estrutura_str  in (basi_101.nivel_produto, 'X')
                                  and rcnb_060.grupo_estrutura_str  in (basi_101.grupo_produto, 'XXXXXX')))
                  or  (p_inc_exc_produto = 2
                  and  not exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060   /* EXCLUS�O */
                                   where rcnb_060.tipo_registro        = 599
                                     and rcnb_060.nr_solicitacao       = 25
                                     and rcnb_060.subgru_estrutura     = p_codigo_usuario
                                     and rcnb_060.usuario_rel          = p_usuario
                                     and rcnb_060.empresa_rel          = p_empresa_logada
                                     and rcnb_060.relatorio_rel        = 'tmrp_f025'
                                     and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                                     and rcnb_060.nivel_estrutura_str  in (basi_101.nivel_produto,'X')
                                     and (rcnb_060.grupo_estrutura_str = basi_101.grupo_produto
                                     or   rcnb_060.grupo_estrutura_str = 'XXXXXX'))
                  or exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060
                              where rcnb_060.tipo_registro        = 599
                                and rcnb_060.nr_solicitacao       = 25
                                and rcnb_060.subgru_estrutura     = p_codigo_usuario
                                and rcnb_060.usuario_rel          = p_usuario
                                and rcnb_060.empresa_rel          = p_empresa_logada
                                and rcnb_060.relatorio_rel        = 'tmrp_f025'
                                and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                                and rcnb_060.nivel_estrutura_str  in (basi_101.nivel_produto,'X')
                                and rcnb_060.grupo_estrutura_str  = 'XXXXXX')))
                  /* APLICA��O */
                  and tmrp_615.nr_solicitacao = p_nr_solicitacao
                  and tmrp_615.codigo_usuario = p_codigo_usuario
                  and tmrp_615.usuario        = p_usuario
                  and tmrp_615.codigo_empresa = p_empresa_logada
                  and tmrp_615.nome_programa  = 'tmrp_f025'
                  and tmrp_615.tipo_registro  = 345
                  and tmrp_615.nivel          = basi_101.nivel_produto
                  and tmrp_615.grupo          = basi_101.grupo_produto
                  and tmrp_615.subgrupo       = basi_101.subgrupo_produto
                  and tmrp_615.item           = '000000'
                  and ((p_inc_exc_aplic = 1
                  and   tmrp_615.sequencia_projeto in (select /*parallel(rcnb_060, 20)*/
                                                              rcnb_060.grupo_estrutura
                                                       from rcnb_060
                                                       where rcnb_060.tipo_registro     = 598
                                                         and rcnb_060.nr_solicitacao    = 25
                                                         and rcnb_060.subgru_estrutura  = p_codigo_usuario
                                                         and rcnb_060.usuario_rel       = p_usuario
                                                         and rcnb_060.empresa_rel       = p_empresa_logada
                                                         and rcnb_060.relatorio_rel     = 'tmrp_f025'
                                                         and rcnb_060.tipo_registro_rel = p_nr_solicitacao))
                  or  (p_inc_exc_aplic = 2
                  and  tmrp_615.sequencia_projeto not in (select /*parallel(rcnb_060, 20)*/
                                                                 rcnb_060.grupo_estrutura
                                                          from rcnb_060
                                                          where rcnb_060.tipo_registro     = 598
                                                            and rcnb_060.nr_solicitacao    = 25
                                                            and rcnb_060.subgru_estrutura  = p_codigo_usuario
                                                            and rcnb_060.usuario_rel       = p_usuario
                                                            and rcnb_060.empresa_rel       = p_empresa_logada
                                                            and rcnb_060.relatorio_rel     = 'tmrp_f025'
                                                            and rcnb_060.tipo_registro_rel = p_nr_solicitacao)));
            end;

            if sql%found
            then
               FOR k IN proj_nivel_comp.FIRST .. proj_nivel_comp.LAST
               LOOP
                  begin
                     select basi_001.nivel_produto,   basi_001.grupo_produto
                     into   proj_nivel_item,          proj_grupo_item
                     from basi_001
                     where basi_001.codigo_projeto = reg_dados.codigo_projeto;
                  exception when others then
                     proj_nivel_item := '0';
                     proj_grupo_item := '00000';
                  end;

                  proj_qtde_total := proj_consumo(k) * reg_dados.qtde_necessaria;

                  inter_pr_grava_tmrp_615_item(p_empresa_logada,
                                               proj_nivel_item,
                                               proj_grupo_item,
                                               reg_dados.combinacao_item,
                                               0,
                                               reg_dados.qtde_necessaria,
                                               proj_nivel_comp(k),
                                               proj_grupo_comp(k),
                                               proj_sub_comp(k),
                                               proj_item_comp(k),
                                               p_codigo_usuario,
                                               p_usuario,
                                               p_nr_solicitacao,
                                               'tmrp_f026',
                                               256,
                                               'basi_101',
                                               proj_consumo(k),
                                               0,
                                               reg_dados.codigo_projeto,
                                               12);

                  inter_pr_grava_tmrp_615(p_empresa_logada,
                                          proj_nivel_comp(k),
                                          proj_grupo_comp(k),
                                          proj_sub_comp(k),
                                          proj_item_comp(k),
                                          proj_qtde_total,
                                          p_codigo_usuario,
                                          p_usuario,
                                          p_nr_solicitacao,
                                          'tmrp_f025',
                                          255,
                                          12,
                                          flag_visualiza_mp);
               END LOOP;
            end if;
         end if;
      end if;
   END LOOP;





   /*
      L� os itens do pedido para buscar seus materiais na estrutura.
   */
   begin
        select /*parallel(pedi_100, pedi_110, basi_001, 20)*/
               pedi_110.cd_it_pe_nivel99,  pedi_110.cd_it_pe_grupo,
               pedi_110.cd_it_pe_subgrupo, pedi_110.cd_it_pe_item,
               basi_001.codigo_projeto,    sum(pedi_110.qtde_pedida),
               pedi_110.pedido_venda,      basi_001.situacao_projeto
        BULK COLLECT INTO
               pedi_nivel,                 pedi_grupo,
               pedi_subgrupo,              pedi_item,
               pedi_cod_proj,              qtde_pedida,
               pedido_venda_lido,          situacao_proj_pedi
         from pedi_100, pedi_110, basi_001
        where pedi_110.cd_it_pe_nivel99  = basi_001.nivel_produto
          and pedi_110.cd_it_pe_grupo    = basi_001.grupo_produto
          and pedi_110.cod_cancelamento  = 0
          and pedi_110.pedido_venda      = pedi_100.pedido_venda
          and pedi_100.codigo_empresa    = p_empresa_filtro
          and pedi_100.data_entr_venda between p_data_ini_venda and p_data_fim_venda
          and basi_001.nivel_produto     = '1'
          and basi_001.situacao_projeto  <> 3
          and exists (select 1 from efic_050
                        where efic_050.cod_empresa     = p_empresa_filtro
                          and efic_050.cod_funcionario = basi_001.codigo_responsavel)
        group by pedi_110.cd_it_pe_nivel99,  pedi_110.cd_it_pe_grupo,
                 pedi_110.cd_it_pe_subgrupo, pedi_110.cd_it_pe_item,
                 basi_001.codigo_projeto,    pedi_110.pedido_venda,
                 basi_001.situacao_projeto;
   end;

   if sql%found
   then

      FOR i IN pedi_nivel.FIRST .. pedi_nivel.LAST
      LOOP

         /*
            Busca alternativa do planejamento, sen�o passa zero pra fun��o buscar da basi_010
         */
         begin
            select tmrp_620.alternativa_produto
            into alternativa_aux
            from tmrp_620
            where tmrp_620.nivel_produto    = pedi_nivel(i)
              and tmrp_620.grupo_produto    = pedi_grupo(i)
              and tmrp_620.subgrupo_produto = pedi_subgrupo(i)
              and tmrp_620.item_produto     = pedi_item(i)
              and tmrp_620.pedido_venda     = pedido_venda_lido(i)
              and rownum                    = 1;
         exception when others then
            alternativa_aux := 0;
         end;

         if sql%notfound
         then
            alternativa_aux := 0;
         end if;


         if alternativa_aux = 0
         then
            begin
               select basi_010.numero_alternati
               into alternativa_aux
               from basi_010
               where basi_010.nivel_estrutura  = pedi_nivel(i)
                 and basi_010.grupo_estrutura  = pedi_grupo(i)
                 and basi_010.subgru_estrutura = pedi_subgrupo(i)
                 and basi_010.item_estrutura   = pedi_item(i);
            exception when others then
               alternativa_aux := 0;
            end;

            if sql%notfound
            then
               alternativa_aux := 0;
            end if;
         end if;

         /*
           Verifica se j� existem pe�as programadas para o pedido.
           Caso houver, desconta do A PROGRAMAR para n�o somar
           duas vezes a quantidade reservada do componente
         */
         begin
            select nvl(sum(tmrp_630.qtde_produzida_comprada),0)
              into qtde_reservada_pedido
              from tmrp_630, pcpc_020
             where tmrp_630.ordem_prod_compra   = pcpc_020.ordem_producao
               and tmrp_630.pedido_venda        = pedido_venda_lido(i)
               and tmrp_630.nivel_produto       = pedi_nivel(i)
               and tmrp_630.grupo_produto       = pedi_grupo(i)
               and tmrp_630.subgrupo_produto    = pedi_subgrupo(i)
               and tmrp_630.item_produto        = pedi_item(i)
               and tmrp_630.area_producao       = 1
               and tmrp_630.alternativa_produto = alternativa_aux
               and pcpc_020.cod_cancelamento    = 0;
         exception when others then
            qtde_reservada_pedido := 0.0000;
         end;

         if sql%notfound
         then
            qtde_reservada_pedido := 0.0000;
         end if;
         /*Fim - Pe�as programadas para o pedido*/

         if qtde_reservada_pedido <> 0
         then
            qtde_pedida(i) := qtde_pedida(i) - qtde_reservada_pedido;
         end if;

         flag_estr_pedi := 'n';

         if situacao_proj_pedi(i) = 2
         then
            /*
               Busca componentes e quantidades da estrutura (BASI_050)
           */
            inter_pr_estrutura(p_empresa_logada,
                               pedi_nivel(i),
                               pedi_grupo(i),
                               pedi_subgrupo(i),
                               pedi_item(i),
                               alternativa_aux,
                               qtde_pedida(i),
                               p_nivel_material,
                               p_tipo_material,
                               p_codigo_usuario,
                               p_usuario,
                               p_nr_solicitacao,
                               flag_estr_pedi,
                               'tmrp_f025',
                               pedido_venda_lido(i),
                               pedi_cod_proj(i),
                               p_inc_exc_aplic,
                               p_inc_exc_produto,
                               flag_visualiza_mp);

         end if;

         /*
            Se n�o encontra na estrutura, busca componentes/qtdes do cadastro
            de combina��es de projeto (BASI_850)
         */
         if flag_estr_pedi = 'n'
         then
            begin
               select /*parallel(basi_850, basi_030, 20)*/
                      basi_850.niv_combinacao, basi_850.gru_combinacao,
                      basi_850.sub_combinacao, basi_850.ite_combinacao,
                      basi_850.consumo_comb
               BULK COLLECT INTO
                      pedi_nivel_comp,         pedi_grupo_comp,
                      pedi_sub_comp,           pedi_item_comp,
                      pedi_consumo
               from basi_850, basi_030, tmrp_615
               where  basi_850.codigo_projeto   = pedi_cod_proj(i)
                 and  basi_850.combinacao_item  = pedi_item(i)
                 and  basi_850.niv_combinacao   = basi_030.nivel_estrutura
                 and  basi_850.gru_combinacao   = basi_030.referencia
                 and (basi_030.comprado_fabric  = p_tipo_material or p_tipo_material = 3)
                 and ((basi_850.niv_combinacao  = p_nivel_material)
                  or  ((basi_850.niv_combinacao = '2'
                  or    basi_850.niv_combinacao = '9')
                 and    p_nivel_material        = '0'))
                 /* PRODUTO */
                 and ((p_inc_exc_produto = 1
                 and   exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060       /* INCLUS�O */
                               where rcnb_060.tipo_registro        = 599
                                 and rcnb_060.nr_solicitacao       = 25
                                 and rcnb_060.subgru_estrutura     = p_codigo_usuario
                                 and rcnb_060.usuario_rel          = p_usuario
                                 and rcnb_060.empresa_rel          = p_empresa_logada
                                 and rcnb_060.relatorio_rel        = 'tmrp_f025'
                                 and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                                 and rcnb_060.nivel_estrutura_str  in (basi_850.niv_combinacao, 'X')
                                 and rcnb_060.grupo_estrutura_str  in (basi_850.gru_combinacao, 'XXXXXX')))
                  or  (p_inc_exc_produto = 2
                  and  not exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060   /* EXCLUS�O */
                                   where rcnb_060.tipo_registro        = 599
                                     and rcnb_060.nr_solicitacao       = 25
                                     and rcnb_060.subgru_estrutura     = p_codigo_usuario
                                     and rcnb_060.usuario_rel          = p_usuario
                                     and rcnb_060.empresa_rel          = p_empresa_logada
                                     and rcnb_060.relatorio_rel        = 'tmrp_f025'
                                     and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                                     and rcnb_060.nivel_estrutura_str  in (basi_850.niv_combinacao,'X')
                                     and (rcnb_060.grupo_estrutura_str = basi_850.gru_combinacao
                                     or   rcnb_060.grupo_estrutura_str = 'XXXXXX'))
                  or exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060
                             where rcnb_060.tipo_registro        = 599
                               and rcnb_060.nr_solicitacao       = 25
                               and rcnb_060.subgru_estrutura     = p_codigo_usuario
                               and rcnb_060.usuario_rel          = p_usuario
                               and rcnb_060.empresa_rel          = p_empresa_logada
                               and rcnb_060.relatorio_rel        = 'tmrp_f025'
                               and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                               and rcnb_060.nivel_estrutura_str  in (basi_850.niv_combinacao,'X')
                               and rcnb_060.grupo_estrutura_str  = 'XXXXXX')))
                 /* APLICA��O */
                 and tmrp_615.nr_solicitacao = p_nr_solicitacao
                 and tmrp_615.codigo_usuario = p_codigo_usuario
                 and tmrp_615.usuario        = p_usuario
                 and tmrp_615.codigo_empresa = p_empresa_logada
                 and tmrp_615.nome_programa  = 'tmrp_f025'
                 and tmrp_615.tipo_registro  = 345
                 and tmrp_615.nivel          = basi_850.niv_combinacao
                 and tmrp_615.grupo          = basi_850.gru_combinacao
                 and tmrp_615.subgrupo       = basi_850.sub_combinacao
                 and tmrp_615.item           = '000000'
                 and ((p_inc_exc_aplic = 1
                 and   tmrp_615.sequencia_projeto in (select rcnb_060.grupo_estrutura
                                                      from rcnb_060
                                                      where rcnb_060.tipo_registro     = 598
                                                        and rcnb_060.nr_solicitacao    = 25
                                                        and rcnb_060.subgru_estrutura  = p_codigo_usuario
                                                        and rcnb_060.usuario_rel       = p_usuario
                                                        and rcnb_060.empresa_rel       = p_empresa_logada
                                                        and rcnb_060.relatorio_rel     = 'tmrp_f025'
                                                        and rcnb_060.tipo_registro_rel = p_nr_solicitacao))
                 or  (p_inc_exc_aplic = 2
                 and  tmrp_615.sequencia_projeto not in (select rcnb_060.grupo_estrutura
                                                         from rcnb_060
                                                         where rcnb_060.tipo_registro     = 598
                                                           and rcnb_060.nr_solicitacao    = 25
                                                           and rcnb_060.subgru_estrutura  = p_codigo_usuario
                                                           and rcnb_060.usuario_rel       = p_usuario
                                                           and rcnb_060.empresa_rel       = p_empresa_logada
                                                           and rcnb_060.relatorio_rel     = 'tmrp_f025'
                                                           and rcnb_060.tipo_registro_rel = p_nr_solicitacao)));
            end;

            if sql%found
            then
               FOR j IN pedi_nivel_comp.FIRST .. pedi_nivel_comp.LAST
               LOOP
                  pedi_qtde_total := pedi_consumo(j) * qtde_pedida(i);

                  inter_pr_grava_tmrp_615_item(p_empresa_logada,
                                               pedi_nivel(i),
                                               pedi_grupo(i),
                                               pedi_item(i),
                                               alternativa_aux,
                                               qtde_pedida(i),
                                               pedi_nivel_comp(j),
                                               pedi_grupo_comp(j),
                                               pedi_sub_comp(j),
                                               pedi_item_comp(j),
                                               p_codigo_usuario,
                                               p_usuario,
                                               p_nr_solicitacao,
                                               'tmrp_f026',
                                               256,
                                               'basi_850�',
                                               pedi_consumo(j),
                                               pedido_venda_lido(i),
                                               pedi_cod_proj(i),
                                               39);

                  inter_pr_grava_tmrp_615(p_empresa_logada,
                                          pedi_nivel_comp(j),
                                          pedi_grupo_comp(j),
                                          pedi_sub_comp(j),
                                          pedi_item_comp(j),
                                          pedi_qtde_total,
                                          p_codigo_usuario,
                                          p_usuario,
                                          p_nr_solicitacao,
                                          'tmrp_f025',
                                          255,
                                          39,
                                          flag_visualiza_mp);
               END LOOP;
            else
            /*Se n�o encontra nas combina��es busca dos componentes do projeto (BASI_101)*/
               begin

                  select /*parallel(basi_101, basi_030, 20)*/
                         basi_101.nivel_produto,    basi_101.grupo_produto,
                         basi_101.subgrupo_produto, basi_101.item_produto,
                         basi_101.consumo
                  BULK COLLECT INTO
                         pedi_nivel_comp,           pedi_grupo_comp,
                         pedi_sub_comp,             pedi_item_comp,
                         pedi_consumo
                    from basi_101, basi_030, tmrp_615
                   where  basi_101.codigo_projeto   = pedi_cod_proj(i)
                     and  basi_101.nivel_produto    = basi_030.nivel_estrutura
                     and  basi_101.grupo_produto    = basi_030.referencia
                     and (basi_030.comprado_fabric  = p_tipo_material or p_tipo_material = 3)
                     and ((basi_101.nivel_produto   = p_nivel_material)
                      or  ((basi_101.nivel_produto  = '2'
                      or    basi_101.nivel_produto  = '9')
                     and    p_nivel_material        = '0'))
                     and  basi_101.grupo_produto    is not null
                     and  basi_101.grupo_produto    <> ' '
                     and  basi_101.grupo_produto    <> '00000'
                     and  basi_101.subgrupo_produto is not null
                     and  basi_101.subgrupo_produto <> ' '
                     and  basi_101.subgrupo_produto <> '000'
                     and  basi_101.item_produto     is not null
                     and  basi_101.item_produto     <> ' '
                     and  basi_101.item_produto     <> '000000'
                     /* PRODUTO */
                     and ((p_inc_exc_produto = 1
                     and   exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060       /* INCLUS�O */
                                   where rcnb_060.tipo_registro        = 599
                                     and rcnb_060.nr_solicitacao       = 25
                                     and rcnb_060.subgru_estrutura     = p_codigo_usuario
                                     and rcnb_060.usuario_rel          = p_usuario
                                     and rcnb_060.empresa_rel          = p_empresa_logada
                                     and rcnb_060.relatorio_rel        = 'tmrp_f025'
                                     and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                                     and rcnb_060.nivel_estrutura_str  in (basi_101.nivel_produto, 'X')
                                     and rcnb_060.grupo_estrutura_str  in (basi_101.grupo_produto, 'XXXXXX')))
                      or  (p_inc_exc_produto = 2
                      and  not exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060   /* EXCLUS�O */
                                       where rcnb_060.tipo_registro        = 599
                                         and rcnb_060.nr_solicitacao       = 25
                                         and rcnb_060.subgru_estrutura     = p_codigo_usuario
                                         and rcnb_060.usuario_rel          = p_usuario
                                         and rcnb_060.empresa_rel          = p_empresa_logada
                                         and rcnb_060.relatorio_rel        = 'tmrp_f025'
                                         and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                                         and rcnb_060.nivel_estrutura_str  in (basi_101.nivel_produto,'X')
                                         and (rcnb_060.grupo_estrutura_str = basi_101.grupo_produto
                                         or   rcnb_060.grupo_estrutura_str = 'XXXXXX'))
                      or exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060
                                 where rcnb_060.tipo_registro        = 599
                                   and rcnb_060.nr_solicitacao       = 25
                                   and rcnb_060.subgru_estrutura     = p_codigo_usuario
                                   and rcnb_060.usuario_rel          = p_usuario
                                   and rcnb_060.empresa_rel          = p_empresa_logada
                                   and rcnb_060.relatorio_rel        = 'tmrp_f025'
                                   and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                                   and rcnb_060.nivel_estrutura_str  in (basi_101.nivel_produto,'X')
                                   and rcnb_060.grupo_estrutura_str  = 'XXXXXX')))
                     /* APLICA��O */
                     and tmrp_615.nr_solicitacao = p_nr_solicitacao
                     and tmrp_615.codigo_usuario = p_codigo_usuario
                     and tmrp_615.usuario        = p_usuario
                     and tmrp_615.codigo_empresa = p_empresa_logada
                     and tmrp_615.nome_programa  = 'tmrp_f025'
                     and tmrp_615.tipo_registro  = 345
                     and tmrp_615.nivel          = basi_101.nivel_produto
                     and tmrp_615.grupo          = basi_101.grupo_produto
                     and tmrp_615.subgrupo       = basi_101.subgrupo_produto
                     and tmrp_615.item           = '000000'
                     and ((p_inc_exc_aplic = 1
                     and   tmrp_615.sequencia_projeto in (select /*parallel(rcnb_060, 20)*/
                                                                 rcnb_060.grupo_estrutura
                                                          from rcnb_060
                                                          where rcnb_060.tipo_registro     = 598
                                                            and rcnb_060.nr_solicitacao    = 25
                                                            and rcnb_060.subgru_estrutura  = p_codigo_usuario
                                                            and rcnb_060.usuario_rel       = p_usuario
                                                            and rcnb_060.empresa_rel       = p_empresa_logada
                                                            and rcnb_060.relatorio_rel     = 'tmrp_f025'
                                                            and rcnb_060.tipo_registro_rel = p_nr_solicitacao))
                     or  (p_inc_exc_aplic = 2
                     and  tmrp_615.sequencia_projeto not in (select /*parallel(rcnb_060, 20)*/
                                                                    rcnb_060.grupo_estrutura
                                                             from rcnb_060
                                                             where rcnb_060.tipo_registro     = 598
                                                               and rcnb_060.nr_solicitacao    = 25
                                                               and rcnb_060.subgru_estrutura  = p_codigo_usuario
                                                               and rcnb_060.usuario_rel       = p_usuario
                                                               and rcnb_060.empresa_rel       = p_empresa_logada
                                                               and rcnb_060.relatorio_rel     = 'tmrp_f025'
                                                               and rcnb_060.tipo_registro_rel = p_nr_solicitacao)));

               end;

               if sql%found
               then
                  FOR y IN pedi_nivel_comp.FIRST .. pedi_nivel_comp.LAST
                  LOOP
                     pedi_qtde_total := pedi_consumo(y) * qtde_pedida(i);

                     inter_pr_grava_tmrp_615_item(p_empresa_logada,
                                                  proj_nivel_item,
                                                  proj_grupo_item,
                                                  '000000',
                                                  0,
                                                  qtde_pedida(i),
                                                  proj_nivel_comp(y),
                                                  proj_grupo_comp(y),
                                                  proj_sub_comp(y),
                                                  proj_item_comp(y),
                                                  p_codigo_usuario,
                                                  p_usuario,
                                                  p_nr_solicitacao,
                                                  'tmrp_f026',
                                                  256,
                                                  'basi_101',
                                                  pedi_consumo(y),
                                                  pedido_venda_lido(i),
                                                  pedi_cod_proj(i),
                                                  74);

                     inter_pr_grava_tmrp_615(p_empresa_logada,
                                             pedi_nivel_comp(y),
                                             pedi_grupo_comp(y),
                                             pedi_sub_comp(y),
                                             pedi_item_comp(y),
                                             pedi_qtde_total,
                                             p_codigo_usuario,
                                             p_usuario,
                                             p_nr_solicitacao,
                                             'tmrp_f025',
                                             255,
                                             74,
                                             flag_visualiza_mp);
                  END LOOP;
               end if;
            end if;
         end if;
      END LOOP;
   end if;

   /*Busca mat�ria prima que n�o est� relacionada a projetos nem pedidos*/
   if p_visualiza_mp = 2
   or p_visualiza_mp = 3
   then

      inter_pr_mp_sem_projetos(p_nr_solicitacao,
                               p_codigo_usuario,
                               p_usuario,
                               p_empresa_logada,
                               p_empresa_filtro,
                               'tmrp_f025',
                               p_visualiza_mp,
                               p_inc_exc_dep,
                               p_cons_transito,
                               p_nivel_material,
                               p_tipo_material,
                               p_inc_exc_produto);

   end if;

   /*Busca as quantidades*/
   inter_pr_nec_quantidades(p_empresa_logada,
                            p_inc_exc_dep,
                            p_codigo_usuario,
                            p_usuario,
                            p_nr_solicitacao,
                            p_empresa_filtro,
                            p_cons_transito,
                            p_data_ini_compra,
                            p_data_fim_compra);

END inter_pr_nec_materia_prima;

 

/

exec inter_pr_recompile;

