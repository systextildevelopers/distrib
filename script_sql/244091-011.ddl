-- Create table
create table PCPT_224
(
  ordem_producao       NUMBER(9) default 0 not null,
  codigo_ocorrencia    NUMBER(4) default 0 not null,
  sequencia_lancamento NUMBER(4) default 0 not null,
  seq_operacao         NUMBER(4) default 0,
  usuario_lancamento   VARCHAR2(250),
  data_lancamento      DATE,
  hora_lancamento      DATE
);
comment on table PCPT_224 is 'Ocorrencias de Tecelagem';
comment on column PCPT_224.ordem_producao is 'Ordem de produção.';
comment on column PCPT_224.codigo_ocorrencia is 'Ocorrências de Tecelagem.';
comment on column PCPT_224.seq_operacao is 'Sequencia de operacão do pacote.';
comment on column PCPT_224.usuario_lancamento is 'Usuário que esta lançando a ocorrência.';
comment on column PCPT_224.data_lancamento is 'Data que esta lançando a ocorrência.';
comment on column PCPT_224.hora_lancamento is 'Hora que esta lançando a ocorrência.';
alter table PCPT_224
  add constraint PK_PCPT_224 primary key (ORDEM_PRODUCAO, CODIGO_OCORRENCIA, SEQUENCIA_LANCAMENTO);
alter table PCPT_224
  add constraint REF_PCPT_224_PCPT_223 foreign key (CODIGO_OCORRENCIA)
  references PCPT_223 (CODIGO_OCORRENCIA);
