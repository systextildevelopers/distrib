create or replace view stview_pcpb_080 as
select decode(p80.ordem_producao, 0, p80.ordem_reprocesso,p80.ordem_producao) ordem_producao
     , p80.nivel_comp
     , p80.grupo_comp
     , p80.subgrupo_comp
     , p80.item_comp
     , p80.grupo_receita
     , p80.subgrupo_receita
     , p80.item_receita
     , p80.letra
     , p80.grupo_receita_principal
     , p80.subgru_receita_principal
     , p80.item_receita_principal
     , p80.nivel_receita_principal
     , p80.numero_grafico
     , p80.tipo_calculo
     , p80.consumo
     , p80.peso_previsto
     , p80.sequencia_estrutura
     , p80.alternativa_receita
     , p80.seq_operacao
     , min(p80.sequencia_nivel5) sequencia_nivel5
from pcpb_080 p80
group by decode(p80.ordem_producao, 0, p80.ordem_reprocesso,p80.ordem_producao)
       , p80.nivel_comp
       , p80.grupo_comp
       , p80.subgrupo_comp
       , p80.item_comp
       , p80.grupo_receita
       , p80.subgrupo_receita
       , p80.item_receita
       , p80.letra
       , p80.grupo_receita_principal
       , p80.subgru_receita_principal
       , p80.item_receita_principal
       , p80.nivel_receita_principal
       , p80.numero_grafico
       , p80.tipo_calculo
       , p80.consumo
       , p80.peso_previsto
       , p80.sequencia_estrutura
       , p80.alternativa_receita
       , p80.seq_operacao;
/

exec inter_pr_recompile;
