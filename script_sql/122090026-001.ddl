alter table obrf_180 add cod_msg_fat_consig number(6) default 0;
alter table obrf_180 add seq_msg_fat_consig number(2) default 0;
alter table obrf_180 add loc_msg_fat_consig varchar2(1) default ' ';

comment on column obrf_180.cod_msg_fat_consig is 'Código da mensagem referente a nota de origem da venda consignada ';
comment on column obrf_180.seq_msg_fat_consig is 'Sequencia da mensagem referente a nota de origem da venda consignada';
comment on column obrf_180.loc_msg_fat_consig is 'Local da mensagem referente a nota de origem da venda consignada';

exec inter_pr_recompile;
