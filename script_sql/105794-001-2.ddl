alter table cont_560
add cod_origem_regi_caixa number(2) default 0;

comment on column cont_560.cod_origem_regi_caixa is 'Código de origem utilizado para contabilizar pis cofins no regime de caixa';

exec inter_pr_recompile;
/
