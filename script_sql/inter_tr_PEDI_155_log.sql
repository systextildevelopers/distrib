create or replace trigger inter_tr_PEDI_155_log 
after insert or delete or update 
on PEDI_155 
for each row 
declare 
   ws_usuario_rede           varchar2(250) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usuário logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    -- begin 
    --    select hdoc_090.programa 
    --    into v_nome_programa 
    --    from hdoc_090 
    --    where hdoc_090.sid = ws_sid 
    --      and rownum       = 1 
    --      and hdoc_090.programa not like '%menu%' 
    --      and hdoc_090.programa not like '%_m%'; 
    --    exception 
    --      when no_data_found then            v_nome_programa := 'SQL'; 
    -- end; 
 
    v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 
 if inserting 
 then 
    begin 
 
        insert into PEDI_155_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           metros_de_OLD,   /*8*/ 
           metros_de_NEW,   /*9*/ 
           metros_ate_OLD,   /*10*/ 
           metros_ate_NEW,   /*11*/ 
           qtde_nuances_OLD,   /*12*/ 
           qtde_nuances_NEW,   /*13*/ 
           id_OLD,   /*14*/ 
           id_NEW,   /*15*/ 
           qtde_prmtda_nuance_minima_OLD,   /*16*/ 
           qtde_prmtda_nuance_minima_NEW,   /*17*/ 
           qtde_minima_metros_nuance_OLD,   /*18*/ 
           qtde_minima_metros_nuance_NEW,   /*19*/ 
           qtde_maxima_de_ordens_OLD,   /*22*/ 
           qtde_maxima_de_ordens_NEW,   /*23*/ 
           qtde_minima_metros_ordem_OLD,   /*24*/ 
           qtde_minima_metros_ordem_NEW    /*25*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.metros_de, /*9*/   
           0,/*10*/
           :new.metros_ate, /*11*/   
           0,/*12*/
           :new.qtde_nuances, /*13*/   
           0,/*14*/
           :new.id, /*15*/   
           0,/*16*/
           :new.qtde_permitida_nuance_minima, /*17*/   
           0,/*18*/
           :new.qtde_minima_metros_nuance, /*19*/   
           0,/*20*/
           :new.qtde_maxima_de_ordens, /*23*/   
           0,/*24*/
           :new.qtde_minima_metros_ordem /*25*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into PEDI_155_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           metros_de_OLD, /*8*/   
           metros_de_NEW, /*9*/   
           metros_ate_OLD, /*10*/   
           metros_ate_NEW, /*11*/   
           qtde_nuances_OLD, /*12*/   
           qtde_nuances_NEW, /*13*/   
           id_OLD, /*14*/   
           id_NEW, /*15*/   
           qtde_prmtda_nuance_minima_OLD, /*16*/   
           qtde_prmtda_nuance_minima_NEW, /*17*/   
           qtde_minima_metros_nuance_OLD, /*18*/   
           qtde_minima_metros_nuance_NEW, /*19*/   
           qtde_maxima_de_ordens_OLD, /*22*/   
           qtde_maxima_de_ordens_NEW, /*23*/   
           qtde_minima_metros_ordem_OLD, /*24*/   
           qtde_minima_metros_ordem_NEW  /*25*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.metros_de,  /*8*/  
           :new.metros_de, /*9*/   
           :old.metros_ate,  /*10*/  
           :new.metros_ate, /*11*/   
           :old.qtde_nuances,  /*12*/  
           :new.qtde_nuances, /*13*/   
           :old.id,  /*14*/  
           :new.id, /*15*/   
           :old.qtde_permitida_nuance_minima,  /*16*/  
           :new.qtde_permitida_nuance_minima, /*17*/   
           :old.qtde_minima_metros_nuance,  /*18*/  
           :new.qtde_minima_metros_nuance, /*19*/   
           :old.qtde_maxima_de_ordens,  /*22*/  
           :new.qtde_maxima_de_ordens, /*23*/   
           :old.qtde_minima_metros_ordem,  /*24*/  
           :new.qtde_minima_metros_ordem  /*25*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into PEDI_155_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           metros_de_OLD, /*8*/   
           metros_de_NEW, /*9*/   
           metros_ate_OLD, /*10*/   
           metros_ate_NEW, /*11*/   
           qtde_nuances_OLD, /*12*/   
           qtde_nuances_NEW, /*13*/   
           id_OLD, /*14*/   
           id_NEW, /*15*/   
           qtde_prmtda_nuance_minima_OLD, /*16*/   
           qtde_prmtda_nuance_minima_NEW, /*17*/   
           qtde_minima_metros_nuance_OLD, /*18*/   
           qtde_minima_metros_nuance_NEW, /*19*/   
           qtde_maxima_de_ordens_OLD, /*22*/   
           qtde_maxima_de_ordens_NEW, /*23*/   
           qtde_minima_metros_ordem_OLD, /*24*/   
           qtde_minima_metros_ordem_NEW /*25*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.metros_de, /*8*/   
           0, /*9*/
           :old.metros_ate, /*10*/   
           0, /*11*/
           :old.qtde_nuances, /*12*/   
           0, /*13*/
           :old.id, /*14*/   
           0, /*15*/
           :old.qtde_permitida_nuance_minima, /*16*/   
           0, /*17*/
           :old.qtde_minima_metros_nuance, /*18*/   
           0, /*19*/
           :old.qtde_maxima_de_ordens, /*22*/   
           0, /*23*/
           :old.qtde_minima_metros_ordem, /*24*/   
           0 /*25*/
         );    
    end;    
 end if;    
end inter_tr_PEDI_155_log;
