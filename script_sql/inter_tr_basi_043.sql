
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_043" 
before insert on BASI_043
for each row

declare
  v_id_registro number;
begin
   select SEQ_BASI_043.nextval into v_id_registro from dual;
   :new.codigo_container := v_id_registro;
end inter_tr_basi_043;
-- ALTER TRIGGER "INTER_TR_BASI_043" ENABLE
 

/

exec inter_pr_recompile;

