create table fatu_062 (
    ch_it_nf_cd_empr	number(3)		not null,
	ch_it_nf_num_nfis	number(9)		not null,
	ch_it_nf_ser_nfis	varchar2(3)		not null,
	seq_item_nfisc		number(5)		not null,
    sequencial_op       number(9)       not null,
	ordem_producao		number(9)		not null,
	periodo_producao	number(4)		not null,
    ordem_confeccao 	number(5)		not null,
    codigo_estagio      number(2)		not null,
    quantidade      	number(14,3),
	consumo_unit		number(13,7),
    
    CONSTRAINT fatu_062_pk PRIMARY KEY (ch_it_nf_cd_empr, ch_it_nf_num_nfis, ch_it_nf_ser_nfis, seq_item_nfisc, 
		sequencial_op, ordem_producao, periodo_producao, ordem_confeccao, codigo_estagio)
);

CREATE INDEX fatu_062_idx1 ON fatu_062(ch_it_nf_cd_empr, ch_it_nf_num_nfis, ch_it_nf_ser_nfis, seq_item_nfisc, 
	ordem_producao, periodo_producao, ordem_confeccao, codigo_estagio);

comment on table fatu_062 is 'Tabela de Produtos Enviados por item da nota fiscal fatu_060 aberta por Ordem de Confeccao (pacote) para o BlocoK';

comment on column fatu_062.ch_it_nf_cd_empr is 'Codigo da empresa da nota fiscal';
comment on column fatu_062.ch_it_nf_num_nfis is 'Numero da nota fiscal';
comment on column fatu_062.ch_it_nf_ser_nfis is 'Serie da nota fiscal';
comment on column fatu_062.seq_item_nfisc is 'Sequencia do item da nota fiscal';

comment on column fatu_062.sequencial_op is 'Sequencial dentro da mesma producao';

comment on column fatu_062.ordem_producao is 'Ordem de producao relacionada a ordem de servico';
comment on column fatu_062.periodo_producao is 'Periodo de producao do pacote da OP relacionada a ordem de servico';
comment on column fatu_062.ordem_confeccao is 'Ordem de confeccao da OP relacionada a ordem de servico';
comment on column fatu_062.codigo_estagio is 'Codigo do estagio relacionado a ordem de servico';
comment on column fatu_062.quantidade is 'Quantidade do item a enviar para a ordem de confeccao (pacote) relacionado';
comment on column fatu_062.consumo_unit is 'Consumo unitário do item a enviar';

exec inter_pr_recompile;
