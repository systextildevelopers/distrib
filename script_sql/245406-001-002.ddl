CREATE TABLE EXPE_007 (
  nota_fiscal             NUMBER(9) NOT NULL,
  serie                   VARCHAR2(3) NOT NULL,
  cod_empresa             NUMBER(3) NOT NULL,
  sequencia               NUMBER(3) DEFAULT 0 NOT NULL,
  data                    DATE,
  descricao               VARCHAR2(60) DEFAULT ''
);

ALTER TABLE EXPE_007 ADD CONSTRAINT PK_EXPE_007 PRIMARY KEY (nota_fiscal, serie, cod_empresa, sequencia);

ALTER TABLE EXPE_007
ADD CONSTRAINT REF_EXPE_007_EXPE_006 FOREIGN KEY (nota_fiscal, serie, cod_empresa) REFERENCES EXPE_006 (nota_fiscal, serie, cod_empresa);

/

exec inter_pr_recompile;
