create or replace PACKAGE BODY ST_PCK_TRAMAS_LOTE AS

    FUNCTION getTramaLote(v_codigo_rolo NUMBER, v_ordem_producao NUMBER)
        RETURN tramas_table
        PIPELINED IS

        rec            tramas;

    BEGIN
        select

            ba10.narrativa  trama1,
            ba10.narrativa_ingles as trama1_eng,
            trama2.trama as trama2 ,
            trama2.tramaEng as trama2_eng,
            es80.lote_fornecedor as lote1 ,
            trama2.lote as lote2

          INTO rec
          from pcpb_187 pb187,
               basi_010 ba10,
               estq_080 es80,
               table(getTramaLote2(pb187.codigo_rolo, pb187.ordem_producao, pb187.grupo, pb187.subgrupo, pb187.item)) trama2
         where pb187.ordem_producao = v_ordem_producao
           and pb187.codigo_rolo = v_codigo_rolo
           and pb187.nivel = es80.nivel_estrutura
           and pb187.grupo = es80.grupo_estrutura
           and pb187.subgrupo = es80.sub_estrutura
           and pb187.item = es80.item_estrutura
           and pb187.lote = es80.lote_produto
           and pb187.nivel = ba10.nivel_estrutura
           and pb187.grupo = ba10.grupo_estrutura
           and pb187.subgrupo = ba10.subgru_estrutura
           and pb187.item = ba10.item_estrutura
           and pb187.qtde_consumo_comp = (select max(qtde_consumo_comp) from pcpb_187 where pcpb_187.ordem_producao = pb187.ordem_producao and pcpb_187.codigo_rolo = pb187.codigo_rolo)
           and (ba10.tipo_prod_quimico = (select tipo_prod_quimico from basi_315 where basi_315.tipo_prod_quimico = ba10.tipo_prod_quimico and basi_315.imp_etiq = 'S' ));

        PIPE ROW (rec);

        RETURN;
            exception when no_data_found then
        begin
            select '' as trama1, '' as trama1_eng ,'' as lote1,'' as trama2, '' as trama2_eng ,'' as lote2
            into rec
            from dual;

            PIPE ROW (rec);

            RETURN;
        end;
    END getTramaLote;

    FUNCTION getTramaLote2(v_codigo_rolo NUMBER, v_ordem_producao NUMBER, v_grupo VARCHAR2, v_subgrupo VARCHAR2, v_item VARCHAR2)
        RETURN trama2_table
        PIPELINED IS

        record            trama2;

    BEGIN

    select
        ba10.narrativa as trama,
        ba10.narrativa_ingles as trama_eng,
        es80.lote_fornecedor as lote
      INTO record

      from pcpb_187 pb187,
           basi_010 ba10 ,
           estq_080 es80
     where pb187.ordem_producao = v_ordem_producao
       and pb187.codigo_rolo = v_codigo_rolo
       and pb187.nivel = es80.nivel_estrutura
       and pb187.grupo = es80.grupo_estrutura
       and pb187.subgrupo = es80.sub_estrutura
       and pb187.item = es80.item_estrutura
       and pb187.lote = es80.lote_produto
       and pb187.nivel = ba10.nivel_estrutura
       and pb187.grupo = ba10.grupo_estrutura
       and pb187.subgrupo = ba10.subgru_estrutura
       and pb187.item = ba10.item_estrutura
       and pb187.qtde_consumo_comp = (
         select max(qtde_consumo_comp)
         from pcpb_187
         where pcpb_187.ordem_producao = pb187.ordem_producao
         and pcpb_187.codigo_rolo = pb187.codigo_rolo
         and pcpb_187.grupo <> v_grupo
         and pcpb_187.subgrupo <> v_subgrupo
         and pcpb_187.item <> v_item
       )
       and (ba10.tipo_prod_quimico = (select tipo_prod_quimico from basi_315 where basi_315.tipo_prod_quimico = ba10.tipo_prod_quimico and basi_315.imp_etiq = 'S' ));

       PIPE ROW (record);
       RETURN;
    exception when no_data_found then
        begin
            select '' as trama1, '' as trama1_eng ,'' as lote1
            into record
            from dual;

            PIPE ROW (record);

            RETURN;
        end;
    end;
END;
