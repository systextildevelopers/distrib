
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_100" 
before insert or
       update
       of codigo_embalagem,
          descricao,
          cod_integracao,
          metros_cubicos_emb,
          peso_maximo,
          peso_embalagem,
		  perc_fator_comp
on fatu_100
for each row

begin

   if :new.cod_integracao > 0
   then

      insert into inte_wms_embalagens (
         cod_embalagem_st,      cod_integracao_wms,
         desc_embalagem_st,     metros_cubicos_emb,
         peso_maximo,           timestamp_aimportar,
         peso_embalagem,        perc_fator_comp
      )
      values (
         :new.codigo_embalagem, :new.cod_integracao,
         :new.descricao,        :new.metros_cubicos_emb,
         :new.peso_maximo,      sysdate(),
         :new.peso_embalagem,   :new.perc_fator_comp
      );

   end if;

end inter_tr_fatu_100;

-- ALTER TRIGGER "INTER_TR_FATU_100" ENABLE
 

/

exec inter_pr_recompile;

