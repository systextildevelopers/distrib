create table oper_279
 (cod_empresa number(3) default 0,
     tp_prod_sped number(2) default 0,
     cfop varchar2(5) default ' ',
     regime_fornec varchar2(1) default ' ',
     nat_oper number(3) default 0);
     
     
alter table oper_279
add constraint PK_oper_279 primary key (cod_empresa,tp_prod_sped,cfop,regime_fornec);

alter table oper_279
  add constraint REF_oper_279_oper_275 foreign key (cod_empresa)
  references oper_275 (cod_empresa);

