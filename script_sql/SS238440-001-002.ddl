begin
  execute immediate 'create table fina_070 (
                        id_forma_de_pagamento number,
                        id_condicao_pagamento number,
                        tarifa_emissao number (9,2) default 0,
                        tarifa_liquidacao number (9,2) default 0,
                        taxa number (9,2) default 0
                      )';
  exception when others then
    if sqlcode != -955 then
      raise;
    end if;
end;
/
begin
  execute immediate 'alter table fina_070 add constraint pk_fina_070 primary key (id_forma_de_pagamento, id_condicao_pagamento)';
  exception when others then
    if sqlcode != -2260 then
      raise;
    end if;
end;
/
