
  CREATE OR REPLACE PROCEDURE "INTER_PR_EXEC_APONTAMENTOS" (
   p_empresa              in number,
   p_codigo_usuario       in number,
   p_ordem_producao       in number,
   p_ordem_confeccao      in number,
   p_codigo_estagio_ini   in number,
   p_codigo_estagio_fim   in number,
   p_qualidade            in number,
   p_qtde_pecas           in number
)
is
   ws_usuario_rede                 varchar2(20);
   ws_maquina_rede                 varchar2(40);
   ws_aplicativo                   varchar2(20);
   ws_sid                          number(9);
   ws_empresa                      number(3);
   ws_usuario_systextil            varchar2(250);
   ws_locale_usuario               varchar2(5);

   v_periodo_producao              number;
   v_referencia_peca               varchar2(5);
   v_alternativa_peca              number;
   v_roteiro_peca                  number;
   v_seq_operacao_ini              number;
   v_seq_operacao_fim              number;

/*
   #################################################################################################
   Busca o �ltimo nro da ordem de confe��o no per�odo
   #################################################################################################
*/
PROCEDURE f_get_seq_operacao_estagio (
    p_ordem_producao_seq     in number,
    p_ordem_confeccao_seq    in number,
    p_codigo_estagio_seq     in number,
    v_seq_operacao_proc      out number)
IS
BEGIN
   v_seq_operacao_proc := 0;
   begin
      select nvl(min(pcpc_040.seq_operacao),0)
      into v_seq_operacao_proc
      from pcpc_040
      where pcpc_040.ordem_producao  = p_ordem_producao_seq
        and pcpc_040.ordem_confeccao = p_ordem_confeccao_seq
        and pcpc_040.codigo_estagio  = p_codigo_estagio_seq;
   exception when others then
      v_seq_operacao_ini := 0;
   end;

end f_get_seq_operacao_estagio;

/*
   #######################
   PROCESSAMENTO PRINCIPAL
   #######################
*/
begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if  p_qualidade          = 4
   and p_codigo_estagio_ini <> p_codigo_estagio_fim
   then
      raise_application_error(-20000,'ATEN��O! Esta sendo executado um apontamento de perda, o est�gio inicial deve ser igual ao final.');
   end if;

   begin
      select   pcpc_020.periodo_producao,            pcpc_020.referencia_peca,
               pcpc_020.alternativa_peca,            pcpc_020.roteiro_peca
      into     v_periodo_producao,                   v_referencia_peca,
               v_alternativa_peca,                   v_roteiro_peca
      from pcpc_020
      where pcpc_020.ordem_producao = p_ordem_producao;
   exception when others then
      raise_application_error(-20000,'ATEN��O! Ordem de produ��o n�o cadastrada.');
   end;

   v_seq_operacao_ini := 0;
   f_get_seq_operacao_estagio (p_ordem_producao,       p_ordem_confeccao,
                               p_codigo_estagio_ini,   v_seq_operacao_ini);
   v_seq_operacao_fim := 0;
   f_get_seq_operacao_estagio (p_ordem_producao,       p_ordem_confeccao,
                               p_codigo_estagio_fim,   v_seq_operacao_fim);


   -- Gera apontamento dos pacotes
   for reg_apont in (select pcpc_040.codigo_estagio
                     from pcpc_040
                     where pcpc_040.ordem_producao  = p_ordem_producao
                       and pcpc_040.ordem_confeccao = p_ordem_confeccao
                       and ((p_qualidade             = 4
                       and   pcpc_040.seq_operacao   = v_seq_operacao_ini)
                       or   (p_qualidade             in (1,2,3)
                       and pcpc_040.seq_operacao    >= v_seq_operacao_ini
                       and pcpc_040.seq_operacao    <  v_seq_operacao_fim))
                     order by pcpc_040.seq_operacao      ASC,
                              pcpc_040.sequencia_estagio ASC,
                              pcpc_040.estagio_depende   ASC)
   loop
      inter_pr_aponta_pacote (p_empresa,                    p_codigo_usuario,
                              p_ordem_producao,             p_ordem_confeccao,
                              reg_apont.codigo_estagio,     p_qualidade,
                              p_qtde_pecas);

   end loop;

end inter_pr_exec_apontamentos;

 

/

exec inter_pr_recompile;

