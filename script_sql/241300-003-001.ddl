CREATE TABLE EXPT_020 (
    ENDERECO         VARCHAR2(40),
	DESCRICAO        VARCHAR2(40)
);

ALTER TABLE EXPT_020
ADD CONSTRAINT PK_EXPT_020 PRIMARY KEY (ENDERECO);
