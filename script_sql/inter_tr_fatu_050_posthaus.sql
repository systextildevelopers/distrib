create or replace trigger "INTER_TR_FATU_050_POSTHAUS"
before update of cod_status, msg_status, situacao_nfisc
on fatu_050
for each row
declare
  eh_nota_posthaus number(1);

begin
  if :new.pedido_venda > 0
  then

    begin
      select 1 into eh_nota_posthaus
      from haus_003
      where haus_003.codigo_empresa = :new.codigo_empresa
        and haus_003.pedido_gerado  = :new.pedido_venda
        and haus_003.solicitacao_gerada = :new.nr_solicitacao;
    exception
      when no_data_found then 
           eh_nota_posthaus := 0;
    end;

    if eh_nota_posthaus = 1
    then
      if :new.situacao_nfisc = 2
      then
        begin
          update haus_006 
          set situacao = 1, --PROCESSADO
              observacao = 'CANCELADA'
          where numero_nota = :new.num_nota_fiscal;
        end;
      elsif :new.cod_status = '100' and :new.situacao_nfisc = 1
      then
        begin
          update haus_003
          set num_nota_gerada  = :new.num_nota_fiscal,
             serie_nota_gerada = :new.serie_nota_fisc,
             cgc9_nota_gerada  = :new.cgc_9,
             cgc4_nota_gerada  = :new.cgc_4,
             cgc2_nota_gerada  = :new.cgc_2,
             situacao          = 1, --PROCESSADO
             observacao        = :new.msg_status
          where haus_003.codigo_empresa     = :new.codigo_empresa
            and haus_003.pedido_gerado      = :new.pedido_venda
            and haus_003.solicitacao_gerada = :new.nr_solicitacao;
        end;
      else
        begin
          update haus_003
          set num_nota_gerada  = :new.num_nota_fiscal,
             serie_nota_gerada = :new.serie_nota_fisc,
             cgc9_nota_gerada  = :new.cgc_9,
             cgc4_nota_gerada  = :new.cgc_4,
             cgc2_nota_gerada  = :new.cgc_2,
             situacao          = 4, --ERRO_GERACAO_NOTA
             observacao        = :new.msg_status
          where haus_003.codigo_empresa     = :new.codigo_empresa
            and haus_003.pedido_gerado      = :new.pedido_venda
            and haus_003.solicitacao_gerada = :new.nr_solicitacao;
        end;
      end if;
    end if;
  end if;
end inter_tr_fatu_050_posthaus;
