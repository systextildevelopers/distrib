create or replace trigger inter_tr_fatu_050_1
  before insert or update of situacao_nfisc
  on fatu_050
  for each row
declare
   Pragma Autonomous_Transaction;
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   v_atualiza                varchar2(1);

   nro_reg                   number;

   v_nr_volume       fatu_050.nr_volume%type;
   v_qtde_embalagens fatu_050.qtde_embalagens%type;
   v_peso_liquido    fatu_050.peso_liquido%type;
   v_peso_bruto      fatu_050.peso_bruto%type;
    
   v_val_fcp_uf_dest   fatu_050.val_fcp_uf_dest_nf%type;
   v_val_icms_uf_dest  fatu_050.val_icms_uf_dest_nf%type;
   v_val_icms_uf_remet fatu_050.val_icms_uf_remet_nf%type;
   
   v_perc_icms_uf_dest pedi_080.perc_icms_uf_dest%type;
   v_perc_icms pedi_080.perc_icms%type;
   v_consumidor_final pedi_080.consumidor_final%type;
   v_perc_icms_isento pedi_080.perc_icms_isento%type;
   v_insc_est_cliente pedi_010.insc_est_cliente%type; 
   
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
       if :new.val_fcp_uf_dest_nf is null
       then :new.val_fcp_uf_dest_nf        := 0;
       end if;

       if :new.val_icms_uf_dest_nf is null
       then :new.val_icms_uf_dest_nf       := 0;
       end if;

       if :new.val_icms_uf_remet_nf is null
       then :new.val_icms_uf_remet_nf      := 0;       
       end if;
       
       if :new.usuario_digitacao is null or :new.usuario_digitacao = ' '
     then :new.usuario_digitacao := ws_usuario_systextil;
     end if;
     
     if :new.pedido_venda > 0 and :new.nota_fatura = 0
       then
          select pedi_100.cod_forma_pagto, pedi_100.nr_autorizacao_opera
          into :new.cod_forma_pagto, :new.nr_autorizacao_opera
          from pedi_100
          where pedi_100.pedido_venda = :new.pedido_venda;
       end if;

       if :new.local_impressao = 0
       then
          select nvl(count(*),0)
          into nro_reg
          from hdoc_060
          where (hdoc_060.codigo_empresa  = :new.codigo_empresa
            or   hdoc_060.codigo_empresa  = 999)
            and hdoc_060.nome_usuario    = ws_usuario_systextil
            and hdoc_060.tipo_operacao   = 31;
          if nro_reg > 0
          then
             begin
                  select hdoc_060.chave_numerica
                  into :new.local_impressao
                  from hdoc_060
                  where (hdoc_060.codigo_empresa  = :new.codigo_empresa
                    or   hdoc_060.codigo_empresa  = 999)
                    and hdoc_060.nome_usuario    = ws_usuario_systextil
                    and hdoc_060.tipo_operacao   = 31
                    and rownum                   = 1
                  order by hdoc_060.codigo_empresa;
             end;
          end if;
       end if;
    
       if :new.nota_fatura > 0
       then
         v_atualiza := 'S';
    
          begin
             select fatu_050.nr_volume,fatu_050.qtde_embalagens,
                    fatu_050.peso_liquido, fatu_050.peso_bruto
             into   v_nr_volume, v_qtde_embalagens,
                    v_peso_liquido, v_peso_bruto
             from fatu_050
             where fatu_050.codigo_empresa  = :new.codigo_empresa
               and fatu_050.num_nota_fiscal = :new.nota_fatura
               and fatu_050.serie_nota_fisc = :new.serie_nota_fisc;
          exception
            when no_data_found then
              v_atualiza := 'N';
          end;
    
          if v_atualiza = 'S'
          then
            :new.nr_volume       := v_nr_volume;
            :new.qtde_embalagens := v_qtde_embalagens;
            :new.peso_liquido    := v_peso_liquido;
            :new.peso_bruto      := v_peso_bruto;
         end if;
      end if;
   end if;
   
   if  updating
   then 
       if :new.nota_fatura > 0
       then
         v_atualiza := 'S';
    
          begin
             select fatu_050.nr_volume,fatu_050.qtde_embalagens,
                    fatu_050.peso_liquido, fatu_050.peso_bruto
             into   v_nr_volume, v_qtde_embalagens,
                    v_peso_liquido, v_peso_bruto
             from fatu_050
             where fatu_050.codigo_empresa  = :new.codigo_empresa
               and fatu_050.num_nota_fiscal = :new.nota_fatura
               and fatu_050.serie_nota_fisc = :new.serie_nota_fisc;
          exception
            when no_data_found then
              v_atualiza := 'N';
          end;
    
          if v_atualiza = 'S'
          then
            :new.nr_volume       := v_nr_volume;
            :new.qtde_embalagens := v_qtde_embalagens;
            :new.peso_liquido    := v_peso_liquido;
            :new.peso_bruto      := v_peso_bruto;
         end if;
      end if;   
    
     if (:new.situacao_nfisc in (0,1,3)) 
     then    
    
        begin
           select pedi_010.insc_est_cliente into v_insc_est_cliente from pedi_010
           where pedi_010.cgc_9 = :new.cgc_9
             and pedi_010.cgc_4 = :new.cgc_4
             and pedi_010.cgc_2 = :new.cgc_2;
         exception
         when no_data_found then
            v_insc_est_cliente := '';
         end;
       
         begin
            select pedi_080.perc_icms_uf_dest, pedi_080.perc_icms,
                   pedi_080.consumidor_final,  pedi_080.perc_icms_isento
            into   v_perc_icms_uf_dest,        v_perc_icms,
                   v_consumidor_final,         v_perc_icms_isento
            from pedi_080
            where pedi_080.natur_operacao   = :new.natop_nf_nat_oper
              and pedi_080.estado_natoper   = :new.natop_nf_est_oper;
         exception
         when no_data_found then
               v_perc_icms_uf_dest  := 0;
               v_perc_icms          := 0;
               v_perc_icms_isento   := 0;
               v_consumidor_final   := 'N';
         end;
       
         if v_insc_est_cliente = 'ISENTO'
         then
            v_perc_icms := v_perc_icms_isento;
         end if;

         -- SE FOR OPERA��O FINAL OU CONSUMIDOR FINAL FAZ PARTILHA
         if (v_consumidor_final = 'S' or v_insc_est_cliente = 'ISENTO') and v_perc_icms_uf_dest-v_perc_icms > 0
         then
            select sum(fatu_060.val_fcp_uf_dest), sum(fatu_060.val_icms_uf_dest),
                   sum(fatu_060.val_icms_uf_remet)
            into   v_val_fcp_uf_dest,           v_val_icms_uf_dest,
                   v_val_icms_uf_remet
            from fatu_060
            where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
              and fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
              and fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc;
            
            if v_val_fcp_uf_dest is null
            then
               v_val_fcp_uf_dest := 0;
            end if;
          
            if v_val_icms_uf_dest is null
            then
               v_val_icms_uf_dest := 0;
            end if;
          
            if v_val_icms_uf_remet  is null
            then
               v_val_icms_uf_remet := 0;
            end if;
          
            :new.val_fcp_uf_dest_nf        := v_val_fcp_uf_dest;
            :new.val_icms_uf_dest_nf       := v_val_icms_uf_dest;
            :new.val_icms_uf_remet_nf      := v_val_icms_uf_remet;
         end if;
          
       end if;
   end if; 
end inter_tr_fatu_050_1;

/

exec inter_pr_recompile;

