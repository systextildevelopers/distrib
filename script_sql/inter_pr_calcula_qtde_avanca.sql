
  CREATE OR REPLACE PROCEDURE "INTER_PR_CALCULA_QTDE_AVANCA" (
   v_qtde_avanca        out number,
   v_periodo_producao   in number,
   v_ordem_confeccao    in number,
   v_ordem_producao     in number,
   v_sequencia_estagio  in number,
   v_codigo_estagio     in number
) is
   v_atu_estagios_seguintes    number;
begin
   /*
           AS OPCOES SAO:
               0 - Nao atualiza - Somente atualizara o proximo estagio se as quantidades forem lancadas como
                    primeira qualidade
               1 - Atualiza - Atualiza para o proximo estagio a quantidade lancada independente da qualidade.
   */
   begin
      select fatu_502.atu_estagios_seguintes
      into   v_atu_estagios_seguintes
      from fatu_502, pcpc_010
      where fatu_502.codigo_empresa   =  pcpc_010.codigo_empresa
        and pcpc_010.area_periodo     = 1
        and pcpc_010.periodo_producao = v_periodo_producao;
   exception when OTHERS then
      v_atu_estagios_seguintes := 0;
   end;

   -- Dos estagios que sao simultaneos, deve buscar a menor quantidade(1a + 2a(se avanca)) dos estagios que nao tem dependentes
   if v_sequencia_estagio <> 0
   then
      begin
         select nvl(min(pcpc_040.qtde_pecas_prod + decode(v_atu_estagios_seguintes,0,0,pcpc_040.qtde_pecas_2a)),0)
         into   v_qtde_avanca
         from pcpc_040
         where pcpc_040.periodo_producao  = v_periodo_producao
           and pcpc_040.ordem_confeccao   = v_ordem_confeccao
           and pcpc_040.ordem_producao    = v_ordem_producao
           and pcpc_040.sequencia_estagio = v_sequencia_estagio;
      end;
   else
      begin
         select nvl(min(pcpc_040.qtde_pecas_prod + decode(v_atu_estagios_seguintes,0,0,pcpc_040.qtde_pecas_2a)),0)
         into   v_qtde_avanca
         from pcpc_040
         where pcpc_040.periodo_producao  = v_periodo_producao
           and pcpc_040.ordem_confeccao   = v_ordem_confeccao
           and pcpc_040.codigo_estagio    = v_codigo_estagio
           and pcpc_040.ordem_producao    = v_ordem_producao;
      end;
   end if;

end inter_pr_calcula_qtde_avanca;

 

/

exec inter_pr_recompile;

