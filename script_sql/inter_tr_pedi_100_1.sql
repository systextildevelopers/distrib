
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_100_1" 
AFTER INSERT on pedi_100
for each row

declare
  v_sequencia       number(6);
  v_qtde_registros  number(1);
  v_executa_trigger number(1);
  v_gera_bloqueio   varchar2(1);
  v_tem_origem      varchar2(1);

begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      select count(*)
      into v_qtde_registros
      from pedi_135
      where pedido_venda = :new.pedido_venda
        and codigo_situacao = 25;

      if v_qtde_registros = 0
      then

         v_tem_origem := 'n';

         begin
            select 's'
            into v_tem_origem
            from pedi_900
            where pedi_900.origem_pedido = :new.origem_pedido
              and rownum = 1;
            exception when no_data_found then
               v_tem_origem := 'n';
         end;
         if v_tem_origem = 's'
         then
            v_gera_bloqueio := 'n';
            begin
               select 's'
               into v_gera_bloqueio
               from pedi_900
               where pedi_900.origem_pedido   = :new.origem_pedido
                 and pedi_900.motivo_bloqueio = 25;
               exception
                  when no_data_found then
                      v_gera_bloqueio := 'n';
            end;
         else
            v_gera_bloqueio := 's';
         end if;

         if v_gera_bloqueio = 's'
         then
            select nvl(max(seq_situacao),0) + 1
            into  v_sequencia
            from pedi_135
            where pedido_venda = :new.pedido_venda;

            INSERT INTO pedi_135
               (pedido_venda,      seq_situacao,
                codigo_situacao,   data_situacao,
                flag_liberacao,    usuario_bloqueio)
            VALUES
               (:new.pedido_venda, v_sequencia,
                25,                trunc(sysdate,'DD'),
                'N',               :new.usuario_cadastro);
          end if;
      end if;
   end if;
end inter_tr_pedi_100_1;

-- ALTER TRIGGER "INTER_TR_PEDI_100_1" ENABLE
 

/

exec inter_pr_recompile;

