create or replace view vw_crec_titulos_emitidos as
    select
        fatu_070.codigo_empresa
        ,fatu_070.cli_dup_cgc_cli9  as cli9
        ,fatu_070.cli_dup_cgc_cli4 as cli4
        ,fatu_070.cli_dup_cgc_cli2 as cli2
        ,pedi_010.nome_cliente as cliente
        ,pedi_010.cod_cliente
        ,fatu_070.cod_transacao as codigo_transacao
        ,estq_005.descricao as desc_transacao
        ,cpag_040.tipo_titulo as tipo_titulo --cpag_010.tipo_titulo as tipo_titulo
        ,cpag_040.descricao desc_titulo
        ,fatu_070.num_duplicata
        ,fatu_070.seq_duplicatas
        ,fatu_070.data_emissao as data_emissao
        ,fatu_070.data_prorrogacao as data_venc
        ,fatu_070.data_prorrogacao as data_prorrogacao
        ,fatu_070.moeda_titulo
        ,basi_270.valor_moeda
        ,fatu_070.valor_duplicata
        ,fatu_070.portador_duplic
        ,fatu_070.pedido_venda
        ,fatu_070.cd_centro_custo
        ,fatu_070.data_emissao as data_transacao
        ,fatu_070.codigo_contabil
        ,fatu_070.cod_historico
        ,case when fatu_070.previsao = 1 then 'SIM' else 'NÃO' end as previsto
        ,fatu_070.cod_canc_duplic

    from
        fatu_070
        ,pedi_010
        ,estq_005
        ,cpag_040
        ,basi_270
        --,cpag_010

    where
             fatu_070.cli_dup_cgc_cli9 = pedi_010.cgc_9
        and fatu_070.cli_dup_cgc_cli4  = pedi_010.cgc_4
        and fatu_070.cli_dup_cgc_cli2  = pedi_010.cgc_2
        and fatu_070.cod_transacao     = estq_005.codigo_transacao
        and fatu_070.tipo_titulo       = cpag_040.tipo_titulo
        and fatu_070.moeda_titulo      = basi_270.codigo_moeda  (+)
        and fatu_070.data_emissao      = basi_270.data_moeda    (+)
        --  and fatu_070.cli_dup_cgc_cli9 = cpag_010.cgc_9
        --  and fatu_070.cli_dup_cgc_cli4 = cpag_010.cgc_4
        --  and fatu_070.cli_dup_cgc_cli2 = cpag_010.cgc_2
        --  and fatu_070.tipo_titulo = cpag_010.tipo_titulo
/
