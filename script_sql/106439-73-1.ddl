INSERT INTO EMPR_007 (PARAM, TIPO, LABEL,  FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT) VALUES ('faturamento.tokenSintegra', 0, 'lb48487', 'fy00010', ' ', null, null, null);
commit;


declare tmpInt number;
cursor parametro_c is select codigo_empresa from fatu_500;
begin
  for reg_parametro in parametro_c
  loop
    select nvl(count(*),0) into tmpInt
    from fatu_500
    where fatu_500.codigo_empresa = reg_parametro.codigo_empresa;
    if(tmpInt > 0)
    then
      begin
        insert into empr_008 ( codigo_empresa, param, VAL_STR)
            values (reg_parametro.codigo_empresa, 'faturamento.tokenSintegra', ' ');
      end;
    end if;
  end loop;
  commit;
end;
/
