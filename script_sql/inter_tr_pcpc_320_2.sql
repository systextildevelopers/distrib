create or replace trigger "INTER_TR_PCPC_320_2"
before insert
    on pcpc_320
for each row
declare
  v_tem_nota_problema number(1);

begin
   if inserting and :new.pedido_venda is not null and :new.pedido_venda > 0
   then
     begin
      select 1
      into v_tem_nota_problema
      from fatu_050
      where fatu_050.pedido_venda = :new.pedido_venda
      and fatu_050.situacao_nfisc in (0,3)
      and rownum = 1;
      exception
      when no_data_found
           then v_tem_nota_problema := 0;
      end;

      if v_tem_nota_problema > 0 and :new.executa_trigger = 0
      then
        raise_application_error(-20000,'N�o pode gerar volumes para esse pedido, existem notas que precisam ser finalizadas no programa fatu_f190 com op��o 2 - reprocesso');
      end if;
    end if;
end INTER_TR_PCPC_320_2;

-- ALTER TRIGGER "INTER_TR_PCPC_320_2" ENABLE

 

/

exec inter_pr_recompile;

