create or replace view obrf_889_aberta_pacote
(
  ordem_servico,
  situacao_item,
  numero_solicitacao,
  tipo_solicitacao_conserto,
  tag_atualiza_conserto,
  nivel_estrutura,
  grupo_estrutura,
  subgrupo_estrutura,
  item_estrutura,
  ordem_producao,
  ordem_confeccao,
  periodo_producao,
  qtde_tag,
  qtde_conserto_original
)
as 
select obrf_889.ordem_servico,
       obrf_889.situacao_item, 
       obrf_889.numero_solicitacao,
       obrf_889.tipo_solicitacao_conserto,
       obrf_889.tag_atualiza_conserto,
       obrf_889.nivel_estrutura,
       obrf_889.grupo_estrutura,
       obrf_889.subgrupo_estrutura,
       obrf_889.item_estrutura,
       obrf_889.ordem_producao,
       obrf_889.ordem_confeccao,
       obrf_889.periodo_producao,
       count(*)   as qtde_tag,
       max(obrf_889.qtde_cons_original_pacote) qtde_cons_original_pacote
from obrf_889
group by obrf_889.ordem_servico,
         obrf_889.situacao_item, 
         obrf_889.numero_solicitacao,
         obrf_889.tipo_solicitacao_conserto,
         obrf_889.tag_atualiza_conserto,
         obrf_889.nivel_estrutura,
         obrf_889.grupo_estrutura,
         obrf_889.subgrupo_estrutura,
         obrf_889.item_estrutura,
         obrf_889.ordem_producao,
         obrf_889.ordem_confeccao,
         obrf_889.periodo_producao;
/
exec inter_pr_recompile;
/
