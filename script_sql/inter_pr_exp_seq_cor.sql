
  CREATE OR REPLACE PROCEDURE "INTER_PR_EXP_SEQ_COR" 
(
     p_codigo_projeto        in varchar2,         p_sequencia_subprojeto  in number,
     p_nivel_item            in varchar2,         p_grupo_item            in varchar2,
     p_subgru_item           in varchar2,         p_item_item             in varchar2,
     p_alternativa_produto   in number,
     p_nivel_comp            in varchar2,         p_grupo_comp            in varchar2,
     p_subgru_comp           in varchar2,         p_item_comp             in varchar2,
     p_cliente9              in number,           p_cliente4              in number,
     p_cliente2              in number,
     p_seq_cor               in varchar2
)
is
   v_total_027          number;
   v_total_028          number;
begin
   for reg_basi_847 in (select basi_847.cor
                        from basi_847
                        where basi_847.codigo_projeto  = p_codigo_projeto
                          and basi_847.cor             is not null
                          and basi_847.seq_cor         = p_seq_cor
                        group by basi_847.cor)
   loop
      if p_nivel_comp     is not null and p_nivel_comp      <> '0'      and
         p_grupo_comp     is not null and p_grupo_comp      <> '00000'  and
         p_subgru_comp    is not null and p_subgru_comp     <> '000'    and
         reg_basi_847.cor is not null and reg_basi_847.cor  <> '000000' and
         reg_basi_847.cor  <> ' '
      then
         --Count para verficar se ja existe o componente
         begin
            select count(*)
            into v_total_027
            from basi_027
            where basi_027.nivel_componente    = p_nivel_comp
              and basi_027.grupo_componente    = p_grupo_comp
              and basi_027.subgrupo_componente = p_subgru_comp
              and basi_027.item_componente     = reg_basi_847.cor
              and basi_027.codigo_projeto      = p_codigo_projeto
              and basi_027.sequencia_projeto   = p_sequencia_subprojeto
              and basi_027.nivel_item          = p_nivel_item
              and basi_027.grupo_item          = p_grupo_item
              and basi_027.subgrupo_item       = p_subgru_item
              and basi_027.item_item           = p_item_item
              and basi_027.alt_item            = p_alternativa_produto;
         end;

         if v_total_027 = 0
         then
            begin
               INSERT INTO basi_027 (
                     nivel_componente,         grupo_componente,
                     subgrupo_componente,      item_componente,
                     codigo_projeto,           sequencia_projeto,
                     cnpj_cliente9,
                     cnpj_cliente4,            cnpj_cliente2,
                     nivel_item,               grupo_item,
                     subgrupo_item,            item_item,
                     alt_item
               ) VALUES (
                     p_nivel_comp,             p_grupo_comp,
                     p_subgru_comp,            reg_basi_847.cor,
                     p_codigo_projeto,         p_sequencia_subprojeto,
                     p_cliente9,
                     p_cliente4,               p_cliente2,
                     p_nivel_item,             p_grupo_item,
                     p_subgru_item,            p_item_item,
                     p_alternativa_produto
                );
            exception when OTHERS then
              raise_application_error (-20000, 'Nao atualizou a tabela de Relacionamento da Situacao(basi_027)');
            end;
         end if;

     	   if p_nivel_comp = '2'
         then
            -- Consiste se ha registro do componente na tabela de componente aprovado.
            begin
               select count(*)
               into v_total_028
               from basi_028
               where basi_028.nivel_componente    = p_nivel_comp
                 and basi_028.grupo_componente    = p_grupo_comp
                 and basi_028.subgrupo_componente = p_subgru_comp
                 and basi_028.item_componente     = reg_basi_847.cor
                 and basi_028.cnpj_cliente9       = p_cliente9
                 and basi_028.cnpj_cliente4       = p_cliente4
                 and basi_028.cnpj_cliente2       = p_cliente2
                 and basi_028.tipo_aprovacao      = 1;   --APROVACAO POR COMPONENTE
            end;

            if v_total_028 = 0
            then
               begin
                  INSERT INTO basi_028 (
                     nivel_componente,                grupo_componente,
                     subgrupo_componente,             item_componente,
                     cnpj_cliente9,                   cnpj_cliente4,
                     cnpj_cliente2,                   codigo_projeto,
                     sequencia_projeto,               situacao_componente,
                     tipo_aprovacao
                  ) VALUES (
                     p_nivel_comp,                    p_grupo_comp,
                     p_subgru_comp,                   reg_basi_847.cor,
                     p_cliente9,                      p_cliente4,
                     p_cliente2,                      p_codigo_projeto,
                     p_sequencia_subprojeto,          0,
                     1
                  );
               exception when OTHERS then
                  raise_application_error (-20000, 'Nao atualizou a tabela de Situacao do Item(basi_028)');
               end;
            end if;
         else
            -- Consiste se ha registro do componente na tabela de componente aprovado.
            select count(*)
            into v_total_028
            from basi_028
            where basi_028.nivel_componente    = p_nivel_comp
              and basi_028.grupo_componente    = p_grupo_comp
              and basi_028.subgrupo_componente = p_subgru_comp
              and basi_028.item_componente     = reg_basi_847.cor
              and basi_028.codigo_projeto      = p_codigo_projeto
              and basi_028.sequencia_projeto   = p_sequencia_subprojeto
              and basi_028.tipo_aprovacao      = 1;   --APROVACAO POR COMPONENTE
            if v_total_028 = 0
            then
               begin
                  INSERT INTO basi_028 (
                     nivel_componente,                grupo_componente,
                     subgrupo_componente,             item_componente,
                     cnpj_cliente9,                   cnpj_cliente4,
                     cnpj_cliente2,                   codigo_projeto,
                     sequencia_projeto,               situacao_componente,
                     tipo_aprovacao
                  ) VALUES (
                     p_nivel_comp,                    p_grupo_comp,
                     p_subgru_comp,                   reg_basi_847.cor,
                     p_cliente9,                      p_cliente4,
                     p_cliente2,                      p_codigo_projeto,
                     p_sequencia_subprojeto,          0,
                     1
                  );
               exception when OTHERS then
                  raise_application_error (-20000, 'Nao atualizou a tabela de Situacao do Item(basi_028)');
               end;
            end if;
         end if;
      end if;
   end loop;
end inter_pr_exp_seq_cor;

 

/

exec inter_pr_recompile;

