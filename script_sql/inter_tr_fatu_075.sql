create or replace trigger "INTER_TR_FATU_075" 
   before insert or
          delete or
          update of historico_pgto,  valor_pago,
                    valor_juros,     valor_descontos,
					vlr_var_cambial
   on fatu_075
   for each row

declare
   v_valor_pago_new fatu_075.valor_pago%type  := 0.00;
   v_valor_pago_old fatu_075.valor_pago%type  := 0.00;
   v_valor_pago     fatu_075.valor_pago%type  := 0.00;
   v_sinal_titulo   cont_010.sinal_titulo%type;

   v_codigo_empresa   fatu_075.nr_titul_codempr%type;
   v_cli_dup_cgc_cli9 fatu_075.nr_titul_cli_dup_cgc_cli9%type;
   v_cli_dup_cgc_cli4 fatu_075.nr_titul_cli_dup_cgc_cli4%type;
   v_cli_dup_cgc_cli2 fatu_075.nr_titul_cli_dup_cgc_cli2%type;
   v_tipo_titulo      fatu_075.nr_titul_cod_tit%type;
   v_num_duplicata    fatu_075.nr_titul_num_dup%type;
   v_seq_duplicatas   fatu_075.nr_titul_seq_dup%type;
   v_moeda_titulo     fatu_070.moeda_titulo%type;
   v_tipo_juros       empr_001.ident_juros%type;
   v_registro_ok      empr_001.ident_juros%type;

   v_executa_trigger  number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting or updating
      then
         begin
            select sinal_titulo
            into v_sinal_titulo
            from cont_010
            where codigo_historico = :new.historico_pgto;
         exception 
           when no_data_found then 
             raise_application_error('-20111','N?o encontrados dados do hist?rico de pagamento '||:old.historico_pgto);
         end;

         if v_sinal_titulo in (1,2)   -- 1) Pagamento ou 2) estorno
         then
            v_valor_pago_new := :new.valor_pago - :new.valor_juros + :new.valor_descontos - :new.vlr_var_cambial;

            -- se for estorno, multiplica por -1.0
            if v_sinal_titulo = 2
            then
               v_valor_pago_new := v_valor_pago_new * (-1.0);
            end if;
         end if;

         v_codigo_empresa   := :new.nr_titul_codempr;
         v_cli_dup_cgc_cli9 := :new.nr_titul_cli_dup_cgc_cli9;
         v_cli_dup_cgc_cli4 := :new.nr_titul_cli_dup_cgc_cli4;
         v_cli_dup_cgc_cli2 := :new.nr_titul_cli_dup_cgc_cli2;
         v_tipo_titulo      := :new.nr_titul_cod_tit;
         v_num_duplicata    := :new.nr_titul_num_dup;
         v_seq_duplicatas   := :new.nr_titul_seq_dup;

         begin
            select fatu_070.moeda_titulo into v_moeda_titulo
            from fatu_070
            where codigo_empresa   = v_codigo_empresa
            and   cli_dup_cgc_cli9 = v_cli_dup_cgc_cli9
            and   cli_dup_cgc_cli4 = v_cli_dup_cgc_cli4
            and   cli_dup_cgc_cli2 = v_cli_dup_cgc_cli2
            and   tipo_titulo      = v_tipo_titulo
            and   num_duplicata    = v_num_duplicata
            and   seq_duplicatas   = v_seq_duplicatas;
         exception
            when no_data_found then
              v_moeda_titulo := 0;
         end;

         if v_moeda_titulo = 0
         then
            if :old.vlr_desconto_moeda = 0.00 or :old.vlr_desconto_moeda is null
            then
              :new.vlr_desconto_moeda := :new.valor_descontos;
            end if;

            if :old.valor_pago_moeda = 0.00 or :old.valor_pago_moeda is null
            then
               :new.valor_pago_moeda   := :new.valor_pago;
            end if;

            if :old.vlr_juros_moeda = 0.00 or :old.vlr_juros_moeda is null
            then
              :new.vlr_juros_moeda    := :new.valor_juros;
            end if;

         end if;
      end if;

      if deleting or updating
      then

         if deleting or (:old.historico_pgto <> :new.historico_pgto)
         then
            BEGIN 
              select sinal_titulo into v_sinal_titulo from cont_010
              where codigo_historico = :old.historico_pgto;
            EXCEPTION 
            WHEN NO_DATA_FOUND THEN             
              raise_application_error('-20111','N?o encontrados dados do hist?rico de pagamento '||:old.historico_pgto);
            END;
         end if;

         if v_sinal_titulo in (1,2)   -- 1) Pagamento ou 2) estorno
         then
            v_valor_pago_old := :old.valor_pago - :old.valor_juros + :old.valor_descontos - :old.vlr_var_cambial;

            -- se for estorno, multiplica por -1.0
            if v_sinal_titulo = 2
            then
               v_valor_pago_old := v_valor_pago_old * (-1.0);
            end if;
         end if;

         if deleting
         then
            v_codigo_empresa   := :old.nr_titul_codempr;
            v_cli_dup_cgc_cli9 := :old.nr_titul_cli_dup_cgc_cli9;
            v_cli_dup_cgc_cli4 := :old.nr_titul_cli_dup_cgc_cli4;
            v_cli_dup_cgc_cli2 := :old.nr_titul_cli_dup_cgc_cli2;
            v_tipo_titulo      := :old.nr_titul_cod_tit;
            v_num_duplicata    := :old.nr_titul_num_dup;
            v_seq_duplicatas   := :old.nr_titul_seq_dup;
         end if;
      end if;

      v_valor_pago := v_valor_pago_new - v_valor_pago_old;

      if v_valor_pago <> 0.00
      then

        -- Verifica se o titulo que esta sendo estornado possui titulo de juros associado,
        -- se existir , deve deletar o titulo se ele nao foi pago .. Somene quando for estorno.



        if v_sinal_titulo = 2
        then
          begin

             select empr_001.ident_juros into v_tipo_juros from empr_001;
             -- Verifica se existe titulos de juros.
             begin
                select 1 into v_registro_ok
                from fatu_070
                where codigo_empresa   = v_codigo_empresa
                  and cli_dup_cgc_cli9 = v_cli_dup_cgc_cli9
                  and cli_dup_cgc_cli4 = v_cli_dup_cgc_cli4
                  and cli_dup_cgc_cli2 = v_cli_dup_cgc_cli2
                  and tipo_titulo      = v_tipo_juros
                  and num_duplicata    = v_num_duplicata
                  and seq_duplicatas   = v_seq_duplicatas;
             exception
             when no_data_found then
                  v_registro_ok := 0;
             end;


             if v_registro_ok = 1 and v_tipo_juros <> v_tipo_juros
             then
                -- se nao houver pagamento para o titulo de juros, deletar o pagamento.
                begin
                select 1 into v_registro_ok
                from fatu_075
                where fatu_075.nr_titul_codempr          = v_codigo_empresa
                  and fatu_075.nr_titul_cli_dup_cgc_cli9 = v_cli_dup_cgc_cli9
                  and fatu_075.nr_titul_cli_dup_cgc_cli4 = v_cli_dup_cgc_cli4
                  and fatu_075.nr_titul_cli_dup_cgc_cli2 = v_cli_dup_cgc_cli2
                  and fatu_075.nr_titul_cod_tit          = v_tipo_juros
                  and fatu_075.nr_titul_num_dup          = v_num_duplicata
                  and fatu_075.nr_titul_seq_dup          = v_seq_duplicatas;
                exception
                when no_data_found then
                  v_registro_ok := 0;
                end;
                -- se registro_ok = 0 deletar titulo de juros .

                if v_registro_ok = 1
                then
                   raise_application_error(-20000, 'ATENCAO! Nao sera possivel estornar o titulo, existe(m) titulos de juros associados ja pagos. Verifique! ');
                end if;

                if v_registro_ok = 0
                then
                   begin
                      delete from fatu_070
                      where codigo_empresa   = v_codigo_empresa
                        and cli_dup_cgc_cli9 = v_cli_dup_cgc_cli9
                        and cli_dup_cgc_cli4 = v_cli_dup_cgc_cli4
                        and cli_dup_cgc_cli2 = v_cli_dup_cgc_cli2
                        and tipo_titulo      = v_tipo_juros
                        and num_duplicata    = v_num_duplicata
                        and seq_duplicatas   = v_seq_duplicatas;
                   exception when OTHERS then
                      raise_application_error (-20000, 'Nao excluiu da tabela (FATU_070-inter_tr_fatu_075-estorno)');
                   end;
                end if;
             end if;
          end;
        end if;


         update fatu_070
         set saldo_duplicata = saldo_duplicata - v_valor_pago
         where codigo_empresa   = v_codigo_empresa
         and   cli_dup_cgc_cli9 = v_cli_dup_cgc_cli9
         and   cli_dup_cgc_cli4 = v_cli_dup_cgc_cli4
         and   cli_dup_cgc_cli2 = v_cli_dup_cgc_cli2
         and   tipo_titulo      = v_tipo_titulo
         and   num_duplicata    = v_num_duplicata
         and   seq_duplicatas   = v_seq_duplicatas;
      end if;
   end if;
end inter_tr_fatu_075;
/
--ALTER TRIGGER  "INTER_TR_FATU_075" ENABLE
--/
