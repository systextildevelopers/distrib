CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_829_HIST"
   after insert or delete or update
   on obrf_829
   for each row

-- Finalidade: Registrar alteracoes feitas na tabela obrf_829 - Configurações para Apuração dos Impostos (ICMS)
-- Autor.....: Luana Giese
-- Data......: 08/11/21
--
-- Historicos de alteracoes na trigger
--
-- Data      Autor           SS          Observacoes
-- 08/11/21  Luana Giese     208562-011  Adicionado campos na verificacao da trigger.

declare	
   ws_tipo_ocorr                   varchar2 (1);
   ws_usuario_rede                 varchar2(20);
   ws_maquina_rede                 varchar2(40);
   ws_aplicativo                   varchar2(20);
   ws_sid                          number(9);
   ws_empresa                      number(3);
   ws_usuario_systextil            varchar2(250);
   ws_locale_usuario               varchar2(5);
   ws_nome_programa                varchar2(20);
   ws_empresa_old				           obrf_829.empresa%type;
   ws_empresa_new				           obrf_829.empresa%type;
   ws_tipo_ajuste_old			         obrf_829.tipo_ajuste%type;
   ws_tipo_ajuste_new			         obrf_829.tipo_ajuste%type;
   ws_quadro_ajuste_old			       obrf_829.quadro_ajuste%type;
   ws_quadro_ajuste_new 		       obrf_829.quadro_ajuste%type;
   ws_cod_msg_old 			           obrf_829.cod_msg%type;
   ws_cod_msg_new			             obrf_829.cod_msg%type;
   ws_cod_ajuste_old 			         obrf_829.cod_ajuste%type;
   ws_cod_ajuste_new			         obrf_829.cod_ajuste%type;
   ws_nome_objeto_manual_old	     obrf_829.nome_objeto_manual%type;
   ws_nome_objeto_manual_new	     obrf_829.nome_objeto_manual%type;
   ws_indicacao_old				         obrf_829.indicacao%type;
   ws_indicacao_new				         obrf_829.indicacao%type;
   ws_codigo_sql_old 			         obrf_829.codigo_sql%type;
   ws_codigo_sql_new 			         obrf_829.codigo_sql%type;
   ws_reg_c197_old				         obrf_829.reg_c197%type;
   ws_reg_c197_new				         obrf_829.reg_c197%type;
   ws_req_e111_old				         obrf_829.reg_e111%type;
   ws_req_e111_new				         obrf_829.reg_e111%type;
   ws_req_e115_old				         obrf_829.reg_e115%type;
   ws_req_e115_new				         obrf_829.reg_e115%type;
   ws_periodo_obrf_apuracao_i_old  obrf_829.periodo_obrf_apuracao_i%type;
   ws_periodo_obrf_apuracao_i_new  obrf_829.periodo_obrf_apuracao_i%type;
   ws_periodo_obrf_apuracao_f_old  obrf_829.periodo_obrf_apuracao_f%type;
   ws_periodo_obrf_apuracao_f_new  obrf_829.periodo_obrf_apuracao_f%type;
   ws_ativo_inativo_old			       obrf_829.ativo_inativo%type;
   ws_ativo_inativo_new			       obrf_829.ativo_inativo%type;

begin
   if INSERTING then
   
      ws_tipo_ocorr                   := 'I';          
  	  ws_empresa_old					        := 0;		 
  	  ws_empresa_new					        := :new.empresa;
  	  ws_tipo_ajuste_old				      := 0;
  	  ws_tipo_ajuste_new				      := :new.tipo_ajuste;
  	  ws_quadro_ajuste_old			      := 0;
  	  ws_quadro_ajuste_new 		        := :new.quadro_ajuste;
  	  ws_cod_msg_old 			    	      := 0;
  	  ws_cod_msg_new			   		      := :new.cod_msg;
  	  ws_cod_ajuste_old 				      := 0;
  	  ws_cod_ajuste_new			 		      := :new.cod_ajuste;
  	  ws_nome_objeto_manual_old	 		  := null;
  	  ws_nome_objeto_manual_new	 		  := :new.nome_objeto_manual;
  	  ws_indicacao_old					      := 0;
  	  ws_indicacao_new					      := :new.indicacao;
  	  ws_codigo_sql_old 				      := 0;
  	  ws_codigo_sql_new 				      := :new.codigo_sql;
  	  ws_reg_c197_old					        := 0;
  	  ws_reg_c197_new					        := :new.reg_c197;
  	  ws_req_e111_old					        := 0;
  	  ws_req_e111_new					        := :new.reg_e111;
  	  ws_req_e115_old					        := 0;
  	  ws_req_e115_new					        := :new.reg_e115;
  	  ws_periodo_obrf_apuracao_i_old	:= null;
  	  ws_periodo_obrf_apuracao_i_new	:= :new.periodo_obrf_apuracao_i;
  	  ws_periodo_obrf_apuracao_f_old	:= null;
  	  ws_periodo_obrf_apuracao_f_new  := :new.periodo_obrf_apuracao_f;
  	  ws_ativo_inativo_old			      := 0;
  	  ws_ativo_inativo_new			      := :new.ativo_inativo;
	 
   elsif UPDATING then

      ws_tipo_ocorr              	    := 'A';          
	    ws_empresa_old					        := :old.empresa;		 
	    ws_empresa_new					        := :new.empresa;
	    ws_tipo_ajuste_old				      := :old.tipo_ajuste;
	    ws_tipo_ajuste_new				      := :new.tipo_ajuste;
	    ws_quadro_ajuste_old				    := :old.quadro_ajuste;
	    ws_quadro_ajuste_new 		  		  := :new.quadro_ajuste;
	    ws_cod_msg_old 			    	      := :old.cod_msg;
	    ws_cod_msg_new			   		      := :new.cod_msg;
	    ws_cod_ajuste_old 				      := :old.cod_ajuste;
	    ws_cod_ajuste_new			 		      := :new.cod_ajuste;
	    ws_nome_objeto_manual_old	 		  := :old.nome_objeto_manual;
	    ws_nome_objeto_manual_new	 		  := :new.nome_objeto_manual;
	    ws_indicacao_old					      := :old.indicacao;
	    ws_indicacao_new					      := :new.indicacao;
	    ws_codigo_sql_old 				      := :old.codigo_sql;
	    ws_codigo_sql_new 				      := :new.codigo_sql;
	    ws_reg_c197_old					        := :old.reg_c197;
	    ws_reg_c197_new					        := :new.reg_c197;
	    ws_req_e111_old					        := :old.reg_e111;
	    ws_req_e111_new					        := :new.reg_e111;
  	  ws_req_e115_old					        := :old.reg_e115;
	    ws_req_e115_new					        := :new.reg_e115;
	    ws_periodo_obrf_apuracao_i_old	:= :old.periodo_obrf_apuracao_i;
	    ws_periodo_obrf_apuracao_i_new	:= :new.periodo_obrf_apuracao_i;
	    ws_periodo_obrf_apuracao_f_old	:= :old.periodo_obrf_apuracao_f;
	    ws_periodo_obrf_apuracao_f_new  := :new.periodo_obrf_apuracao_f;
	    ws_ativo_inativo_old			      := :old.ativo_inativo;
	    ws_ativo_inativo_new			      := :new.ativo_inativo;

   elsif DELETING then
   
      ws_tipo_ocorr                 	:= 'D';    
	    ws_empresa_new					        := 0;		 
	    ws_empresa_old					        := :old.empresa;
	    ws_tipo_ajuste_new				      := 0;
	    ws_tipo_ajuste_old				      := :old.tipo_ajuste;
	    ws_quadro_ajuste_new				    := 0;
	    ws_quadro_ajuste_old 		  		  := :old.quadro_ajuste;
	    ws_cod_msg_new			    	      := 0;
	    ws_cod_msg_old			   		      := :old.cod_msg;
	    ws_cod_ajuste_new 				      := 0;
	    ws_cod_ajuste_old		 			      := :old.cod_ajuste;
	    ws_nome_objeto_manual_new 		  := null;
	    ws_nome_objeto_manual_old	 		  := :old.nome_objeto_manual;
	    ws_indicacao_new					      := 0;
	    ws_indicacao_old					      := :old.indicacao;
	    ws_codigo_sql_new 				      := 0;
	    ws_codigo_sql_old					      := :old.codigo_sql;
	    ws_reg_c197_new					        := 0;
	    ws_reg_c197_old					        := :old.reg_c197;
	    ws_req_e111_new					        := 0;
	    ws_req_e111_old					        := :old.reg_e111;
	    ws_req_e115_new					        := 0;
	    ws_req_e115_old					        := :old.reg_e115;
	    ws_periodo_obrf_apuracao_i_new	:= null;
  	  ws_periodo_obrf_apuracao_i_old	:= :old.periodo_obrf_apuracao_i;
	    ws_periodo_obrf_apuracao_f_new	:= null;
	    ws_periodo_obrf_apuracao_f_old  := :old.periodo_obrf_apuracao_f;
	    ws_ativo_inativo_new			      := 0;
	    ws_ativo_inativo_old			      := :old.ativo_inativo;

   end if;

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ws_nome_programa := inter_fn_nome_programa(ws_sid);

   INSERT INTO obrf_829_hist
   (
  	APLICACAO,					          TIPO_OCORR,
  	DATA_OCORR, 					        USUARIO_REDE,
  	MAQUINA_REDE,
  	EMPRESA_OLD,					        EMPRESA_NEW,
  	TIPO_AJUSTE_OLD,  		        TIPO_AJUSTE_NEW,
  	QUADRO_AJUSTE_OLD,  	  	    QUADRO_AJUSTE_NEW,
  	COD_MSG_OLD,  				        COD_MSG_NEW,
  	COD_AJUSTE_OLD,  				      COD_AJUSTE_NEW,
  	NOME_OBJETO_MANUAL_OLD,       NOME_OBJETO_MANUAL_NEW,
  	INDICACAO_OLD,  				      INDICACAO_NEW,
  	CODIGO_SQL_OLD,  				      CODIGO_SQL_NEW,
  	REG_C197_OLD,  				        REG_C197_NEW,
  	REQ_E111_OLD,  				        REQ_E111_NEW,
  	REQ_E115_OLD,  				        REQ_E115_NEW,
  	PERIODO_OBRF_APURACAO_I_OLD,  PERIODO_OBRF_APURACAO_I_NEW,
  	PERIODO_OBRF_APURACAO_F_OLD,  PERIODO_OBRF_APURACAO_F_NEW,
  	ATIVO_INATIVO_OLD,  			    ATIVO_INATIVO_NEW
   )
   VALUES
      ( ws_aplicativo,                    ws_tipo_ocorr,
        sysdate,						              ws_usuario_rede,                   
		    ws_maquina_rede,
  		  ws_empresa_old, 					        ws_empresa_new,				  
  		  ws_tipo_ajuste_old,  				      ws_tipo_ajuste_new,			  
  		  ws_quadro_ajuste_old,				      ws_quadro_ajuste_new,		  
  		  ws_cod_msg_old, 		 			        ws_cod_msg_new,			      
  		  ws_cod_ajuste_old, 					      ws_cod_ajuste_new,			  
  		  ws_nome_objeto_manual_old,	  		ws_nome_objeto_manual_new,	  
  		  ws_indicacao_old,					        ws_indicacao_new,				  
  		  ws_codigo_sql_old, 					      ws_codigo_sql_new, 			  
  		  ws_reg_c197_old,				 	        ws_reg_c197_new,				  
  		  ws_req_e111_old,				  	      ws_req_e111_new,				  
  		  ws_req_e115_old,				  	      ws_req_e115_new,				  
  		  ws_periodo_obrf_apuracao_i_old,  	ws_periodo_obrf_apuracao_i_new, 
  		  ws_periodo_obrf_apuracao_f_old, 	ws_periodo_obrf_apuracao_f_new, 
  		  ws_ativo_inativo_old,			  	    ws_ativo_inativo_new			  
	);
	
end inter_tr_obrf_829_hist;
-- ALTER TRIGGER "INTER_TR_OBRF_829_HIST" ENABLE





/
