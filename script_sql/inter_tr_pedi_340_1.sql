create or replace trigger inter_tr_pedi_340_1
before update on pedi_340
for each row

declare
  v_cod_empr               pedi_100.codigo_empresa%type;
  v_niv_pedido             pedi_100.tecido_peca%type;
  v_sit_pedido             pedi_100.situacao_venda%type;
  v_cnpj9                  pedi_100.cli_ped_cgc_cli9%type;
  v_cnpj4                  pedi_100.cli_ped_cgc_cli4%type;
  v_cnpj2                  pedi_100.cli_ped_cgc_cli2%type;
  v_flag_lib               pedi_135.flag_liberacao%type;
  v_flag_retorno           number(01);
  v_seq_info               pedi_245.sequencia%type;
  v_seq_bloq               pedi_135.seq_situacao%type;
  v_limite1                pedi_010.limite_max_ped1%type;
  v_limite2                pedi_010.limite_max_ped2%type;
  v_limite4                pedi_010.limite_max_ped4%type;
  v_limite7                pedi_010.limite_max_ped7%type;
  v_nr_dias_lim            fatu_502.nr_dias_serasa%type;
  v_bloqueio               hdoc_100.sit_bloqueio%type;
  v_tp_info                pedi_240.tipo_informacao%type;
  v_data_limite            pedi_010.val_lim_credito%type;
  v_msg_pedido             varchar2(61);
  v_limite_atual           number(08);
  v_gera_bloqueio          varchar2(1);
  v_tem_origem             varchar2(1);
  v_saldo_pedi             number(14,2);
  v_origem_pedido          pedi_100.origem_pedido%type;
  v_pedido_concat          varchar2(1000);
  v_acum_cred_corporacao   number(1);
  v_grupo_economico        number(5);
  v_grupo_economico_ativo  number(1);
  
    -- SYP: (71616)
  v_concepto        pedi_010.conceito_cliente%type;
  v_ndias           pedi_086.nr_dias_validade_limite%type;

  -- extrai pedido
  v_num_pedido     varchar2(9);

  -- motivo rejeicao
  v_bloq_rej       number(02);

  --Rej com limite sugerido verifica se considera cheques
  v_par_cheque     fatu_502.cons_cheque_ana_credito%type;

  --Rej com lim sugerido verifica se considera lim por empresa
  v_ana_empresa   fatu_503.limite_credito_empresa%type;

  --Rej com lim sugerido verifica se considera lim por cliente ou grupo
  v_ana_cred_corp   fatu_500.acum_cred_corporacao%type;

  --Rej com lim sugerido valor cheque
  v_acum_cheque   number(15,2);

  --Rej com lim sugerido valor pedido
  v_acum_ped      number(15,2);

  --Rej com lim sugerido valor titulos
  v_acum_tit       number(15,2);

  --Rej com lim sugerido valor disponivel para liberaÃ§Ã¢o
  v_disp_lib       number(15,2);

  --Rej com lim sugerido grupo_economico
   v_grupo_econ    pedi_010.grupo_economico%type;

 --Rej com lim SALDO PEDIDO
   v_val_sld_pedi    pedi_100.valor_saldo_pedi%type;

  cursor contaPedido(numeros varchar2) is
    SELECT trim(regexp_substr(numeros, '[^,]+', 1, LEVEL)) pedido
      FROM dual
   CONNECT BY LEVEL <= (select sum(length(numeros)-length(replace(numeros,',','')))/length(',') from dual ) +1; --regexp_count(numeros, ',')+1;
   

  cursor ordemPedido(numeros varchar2) is
    SELECT pedi_100.pedido_venda pedido
      FROM pedi_100
   where pedi_100.pedido_venda in (numeros)
   order by pedi_100.data_entr_venda;


begin

   if updating
   then
      v_flag_retorno := :new.flag_retorno;
      
      if(:old.flag_retorno = 0 or :old.flag_retorno = 5) and
        (:new.flag_retorno = 1 or :new.flag_retorno = 2) -- and :new.limite_ofertado = 0.00))
      then

         /*
            pega o campo que tem os pedidos concatenados e quebra
            em uma lista para verificar cada pedido contigo na lista.
         */
         v_pedido_concat := '';
         for regContaPedido in contaPedido(:new.numeros_pedidos)
         loop
           v_num_pedido := regContaPedido.Pedido;

           if v_pedido_concat = '' then
              v_pedido_concat := v_pedido_concat ||','|| v_num_pedido;
           else
              v_pedido_concat := v_num_pedido;
           end if;   

           /*GRAVA REGISTRO NAS INFORMACOES GERAIS*/
           /*Verifica os dados do pedido de venda*/
           select pedi_100.codigo_empresa,   pedi_100.cli_ped_cgc_cli9,
                  pedi_100.cli_ped_cgc_cli4, pedi_100.cli_ped_cgc_cli2,
                  pedi_100.situacao_venda,   pedi_100.tecido_peca,
                  pedi_100.valor_saldo_pedi
             into v_cod_empr,                v_cnpj9,
                  v_cnpj4,                   v_cnpj2,
                  v_sit_pedido,              v_niv_pedido,
                  v_saldo_pedi
             from pedi_100
            where pedi_100.pedido_venda = v_num_pedido;
            
           /*verifica se possui analise por grupo*/
           begin
             select fatu_500.acum_cred_corporacao
             into v_acum_cred_corporacao
             from fatu_500
             where fatu_500.codigo_empresa = v_cod_empr;
             exception
                when OTHERS then v_acum_cred_corporacao := 0;
           end; 
               
           begin  
             select pedi_010.grupo_economico
             into   v_grupo_economico
             from pedi_010
             where pedi_010.cgc_9      = v_cnpj9
             and pedi_010.cgc_4      = v_cnpj4
             and pedi_010.cgc_2      = v_cnpj2;
             exception
                when OTHERS then v_grupo_economico := 0;
           end; 
                  
           begin
             select pedi_220.ativo
             into v_grupo_economico_ativo
             from pedi_220
             where pedi_220.codigo = v_grupo_economico
             and   pedi_220.tipo   = 8;
             exception
                when OTHERS then v_grupo_economico_ativo := 0;
           end;
               
           if not (v_acum_cred_corporacao = 4 and v_grupo_economico > 0 and v_grupo_economico_ativo = 1)
           then 

              /*VERIFICA O LIMITE DE CREDITO ATUAL DO CLIENTE*/

              -- SYP: (71616) Se adiciona la variable v_concepto
              select pedi_010.limite_max_ped1, pedi_010.limite_max_ped2,
                     pedi_010.limite_max_ped4, pedi_010.limite_max_ped7,
                     pedi_010.conceito_cliente, pedi_010.grupo_economico
              into   v_limite1,                v_limite2,
                     v_limite4,                v_limite7,
                     v_concepto,               v_grupo_economico
                from pedi_010
               where pedi_010.cgc_9 = v_cnpj9
                 and pedi_010.cgc_4 = v_cnpj4
                 and pedi_010.cgc_2 = v_cnpj2;

              /*Seleciona valor para limite atual de acordo com nivel*/
              if v_niv_pedido = '1'
              then v_limite_atual := v_limite1;
              end if;

              if v_niv_pedido = '2'
              then v_limite_atual := v_limite2;
              end if;

              if v_niv_pedido = '4'
              then v_limite_atual := v_limite4;
              end if;

              if v_niv_pedido = '7'
              then v_limite_atual := v_limite7;
              end if;

              -- SYP: (71616) Se selecciona el nro de dias registrado para el concepto
              select pedi_086.nr_dias_validade_limite
                into v_ndias
                from pedi_086
               where pedi_086.codigo_conceito = v_concepto;

              /*VERIFICA SE A CAPA JÃ? FOI INSERIDA*/
              begin
                 select pedi_240.tipo_informacao
                   into v_tp_info
                   from pedi_240
                  where pedi_240.infocli_cgc_cli9  = v_cnpj9
                    and pedi_240.infocli_cgc_cli4  = v_cnpj4
                    and pedi_240.infocli_cgc_cli2  = v_cnpj2
                    and pedi_240.tipo_informacao   = 97
                    and pedi_240.data_informacao   = :new.data_retorno
                    and pedi_240.informante        = 'SERASA';
              exception
                when OTHERS then
                   INSERT INTO pedi_240
                         (infocli_cgc_cli9,
                          infocli_cgc_cli4,
                          infocli_cgc_cli2,
                          tipo_informacao,
                          data_informacao,
                          informante,
                          status_inf,
                          historico_inf,
                          acesso_inf)
                       VALUES
                         (v_cnpj9,
                          v_cnpj4,
                          v_cnpj2,
                          97,
                          :new.data_retorno,
                          'SERASA',
                          0,
                          0,
                          2);
              end;

              /*Acha proxima sequencia*/
              select nvl(max(pedi_245.sequencia),0) + 1
                into v_seq_info
                from pedi_245
               where pedi_245.informe_infocli_cgc_cli9 = v_cnpj9
                 and pedi_245.informe_infocli_cgc_cli4 = v_cnpj4
                 and pedi_245.informe_infocli_cgc_cli2 = v_cnpj2
                 and pedi_245.informe_tipo_inf         = 97
                 and pedi_245.informe_datainfo         = :new.data_retorno
                 and pedi_245.informe_informte         = 'SERASA'
                 and pedi_245.informe_historico_inf    = 0;

              /*InclusÃ£o para o Pedido*/
              v_msg_pedido := ' VL'      || to_char(v_saldo_pedi,'0000000.00') ||
                              ' '        || to_char(:new.data_consulta,'DD/MM/YY')  ||
                              ' LIM:ATU' || to_char(v_limite_atual,'00000000') ||
                              ' NOVO'    || to_char(:new.novo_limite,'00000000');

              INSERT INTO pedi_245
                   (informe_infocli_cgc_cli9,
                    informe_infocli_cgc_cli4,
                    informe_infocli_cgc_cli2,
                    informe_tipo_inf,
                    informe_datainfo,
                    informe_informte,
                    sequencia,
                    informacao,
                    informe_historico_inf)
                VALUES
                   (v_cnpj9,
                    v_cnpj4,
                    v_cnpj2,
                    97,
                    :new.data_retorno,
                    'SERASA',
                    v_seq_info,
                    substr(v_msg_pedido,2,61),
                    0);

              /*InclusÃ£o para a mensagem*/
              v_seq_info := v_seq_info + 1;

              INSERT INTO pedi_245
                   (informe_infocli_cgc_cli9,
                    informe_infocli_cgc_cli4,
                    informe_infocli_cgc_cli2,
                    informe_tipo_inf,
                    informe_datainfo,
                    informe_informte,
                    sequencia,
                    informacao,
                    informe_historico_inf)
                VALUES
                   (v_cnpj9,
                    v_cnpj4,
                    v_cnpj2,
                    97,
                    :new.data_retorno,
                    'SERASA',
                    v_seq_info,
                    substr(:new.mensagem,1,60),
                    0);

              if :new.flag_retorno = 1   -- Aprovado
              then
                 v_flag_retorno := 3;

                 /*Somente se novo limite > zero*/
                 if :new.novo_limite <= 0
                 then
                    v_limite1 :=  v_limite1;
                    v_limite2 :=  v_limite2;
                    v_limite4 :=  v_limite4;
                    v_limite7 :=  v_limite7;
                 else
                    /*Seleciona valor para limite de acordo com nivel*/
                    if v_niv_pedido = '1'
                    then v_limite1 := :new.novo_limite;
                    end if;

                    if v_niv_pedido = '2'
                    then v_limite2 := :new.novo_limite;
                    end if;

                    if v_niv_pedido = '4'
                    then v_limite4 := :new.novo_limite;
                    end if;

                    if v_niv_pedido = '7'
                    then v_limite7 := :new.novo_limite;
                    end if;
                 end if;

                 /*Verifica o nr de dias para o serasa*/
                 select fatu_502.nr_dias_serasa
                   into v_nr_dias_lim
                   from fatu_502
                  where fatu_502.codigo_empresa = v_cod_empr;

                 -- SYP: (71616) Verifica si el nro de dÃ­as del concepto es mayor a cero. Si es si, toma ese nro de dÃ­as,
                 --              caso contrario, toma el de la configuraciÃ³n.
                 if v_ndias > 0
                 then v_data_limite := :new.data_retorno + v_ndias;
                 else
                    if v_nr_dias_lim > 0
                    then  v_data_limite := :new.data_retorno + v_nr_dias_lim;
                    else
                      select pedi_010.val_lim_credito
                        into v_data_limite
                        from pedi_010
                       where pedi_010.cgc_9 = v_cnpj9
                         and pedi_010.cgc_4 = v_cnpj4
                         and pedi_010.cgc_2 = v_cnpj2;
                    end if;
                 end if;

                 /*Altera o limite de credito e a dta de validade do limite*/
                 update pedi_010
                    set val_lim_credito = v_data_limite,
                        limite_max_ped1 = v_limite1,
                        limite_max_ped2 = v_limite2,
                        limite_max_ped4 = v_limite4,
                        limite_max_ped7 = v_limite7
                  where pedi_010.cgc_9  = v_cnpj9
                    and pedi_010.cgc_4  = v_cnpj4
                    and pedi_010.cgc_2  = v_cnpj2;
                    
                 /*ROTINA PARA LIBERAÃ¿Ã¿O DOS BLOQUEIOS COM CONSULTA AO SERASA*/
                 update pedi_135 set
                        flag_liberacao = 'M',
                        responsavel    = 'SERASA',
                        data_liberacao =  :new.data_retorno
                  where pedi_135.pedido_venda = v_num_pedido
                    and pedi_135.flag_liberacao = 'N'
                    and ( (pedi_135.codigo_situacao = 74) or  
                          exists (select *
                                  from hdoc_100
                                 where hdoc_100.sit_bloqueio       = pedi_135.codigo_situacao
                                   and hdoc_100.consulta_serasa    = 1
                                   and hdoc_100.situacao_liberador = 0)
                                    );
              end if;

              if :new.flag_retorno = 2  -- Rejeitado
              then
                 v_flag_retorno := 4;

                 if :old.flag_retorno = 0 then -- rejeitado pela primeira vez
                    v_bloq_rej := 45;
                 else
                    v_bloq_rej := 38;
                 end if;

                   /*Verifica se existe liberador para o bloqueio do Serasa*/
                   begin
                      select hdoc_100.sit_bloqueio
                        into v_bloqueio
                        from hdoc_100
                       where hdoc_100.usubloq_empr_usu   = v_cod_empr
                         and hdoc_100.area_bloqueio      = 1
                         and hdoc_100.sit_bloqueio       = v_bloq_rej
                         and hdoc_100.situacao_liberador = 0
                       group by hdoc_100.sit_bloqueio;
                   exception
                     when OTHERS then
                       v_bloqueio := 0;
                   end;

                   if  v_bloqueio > 0
                   then

                      /*Verifica os dados do pedido de venda*/
                      select pedi_100.situacao_venda
                        into v_sit_pedido
                        from pedi_100
                       where pedi_100.pedido_venda = v_num_pedido;

                      if v_sit_pedido <> 10  -- 10 pedido faturado
                      then

                        /*Somente se houver liberador os bloq. serÃ£o liberados */
                        /*ROTINA PARA LIBERAÃ¿Ã¿O DOS BLOQUEIOS COM CONSULTA AO SERASA*/
                        update pedi_135 set
                               flag_liberacao = 'M',
                               responsavel    = 'SERASA',
                               data_liberacao = :new.data_retorno
                         where pedi_135.pedido_venda = v_num_pedido
                           and pedi_135.flag_liberacao = 'N'
                           and ( ( pedi_135.codigo_situacao = 74) or  
                            exists (select *
                                  from hdoc_100
                                 where hdoc_100.sit_bloqueio       = pedi_135.codigo_situacao
                                   and hdoc_100.consulta_serasa    = 1
                                   and hdoc_100.situacao_liberador = 0) );
                        begin
                          select pedi_100.origem_pedido
                            into v_origem_pedido
                            from pedi_100
                           where pedi_100.pedido_venda = v_num_pedido;
                        exception
                          when no_data_found then
                             v_origem_pedido := 0;
                        end;

                        v_tem_origem := 'n';

                        begin
                          select 's'
                            into v_tem_origem
                            from pedi_900
                           where pedi_900.origem_pedido = v_origem_pedido
                             and rownum = 1;
                        exception
                          when no_data_found then
                            v_tem_origem := 'n';
                        end;

                        if v_tem_origem = 's'
                        then
                           v_gera_bloqueio := 'n';
                           begin
                             select 's'
                               into v_gera_bloqueio
                               from pedi_900
                              where pedi_900.origem_pedido   = v_origem_pedido
                                and pedi_900.motivo_bloqueio = v_bloq_rej;
                           exception
                              when no_data_found then
                                 v_gera_bloqueio := 'n';
                           end;
                        else
                           v_gera_bloqueio := 's';
                        end if;

                        if v_gera_bloqueio = 's'
                        then
                           select nvl(max(pedi_135.seq_situacao),0) + 1
                             into v_seq_bloq
                             from pedi_135
                            where pedi_135.pedido_venda = v_num_pedido;

                           /*GERA BLOQUEIO SERASA*/
                           insert into pedi_135
                                (pedido_venda,
                                 seq_situacao,
                                 codigo_situacao,
                                 data_situacao,
                                 flag_liberacao,
                                 observacao)
                               VALUES
                                (v_num_pedido,
                                 v_seq_bloq,
                                 v_bloq_rej,
                                 :new.data_retorno,
                                 'N',
                                 :new.mensagem);
                        end if;
                      end if; --sit pedido
                   end if; -- limite ofertado > 0
              end if; -- Rejeitado

              select pedi_100.situacao_venda
                into v_sit_pedido
                from pedi_100
               where pedi_100.pedido_venda = v_num_pedido;

              if v_sit_pedido <> 10 and v_sit_pedido <> 9
              then
                begin
                  select pedi_135.flag_liberacao
                    into v_flag_lib
                    from pedi_135
                   where pedi_135.pedido_venda   = v_num_pedido
                     and pedi_135.flag_liberacao = 'N'
                   group by pedi_135.flag_liberacao;
                exception
                   when OTHERS then
                      v_flag_lib := 'M';
                end;

                if v_flag_lib = 'N' then v_sit_pedido := 5;
                else v_sit_pedido := 0;
                end if;

                update pedi_100 set
                       situacao_venda = v_sit_pedido
                 where pedi_100.pedido_venda = v_num_pedido
                   and pedi_100.situacao_venda <> 10;
              end if;
           end if;
         END LOOP;/* fim looping que monta linha pedidos*/

         /*ALTERA A FLAG DE RETORNO*/
         --:new.flag_retorno := v_flag_retorno;

      end if;
            
      if (:new.flag_retorno = 2) --and :new.limite_ofertado > 0.00)
      then
         v_flag_retorno := 4;
          
         /*
            pega o campo que tem os pedidos concatenados e ordena
            por data de entrega para tentar liberar.
         */
         for regOrdemPedido in ordemPedido(v_pedido_concat) --:new.numeros_pedidos)
         loop
            v_num_pedido := regOrdemPedido.Pedido;
          
           /*Verifica os dados do pedido de venda*/
           select pedi_100.codigo_empresa,   pedi_100.cli_ped_cgc_cli9,
                  pedi_100.cli_ped_cgc_cli4, pedi_100.cli_ped_cgc_cli2,
                  pedi_100.tecido_peca,      pedi_010.grupo_economico
             into v_cod_empr,                v_cnpj9,
                  v_cnpj4,                   v_cnpj2,
                  v_niv_pedido,              v_grupo_econ
             from pedi_100, pedi_010
            where pedi_100.pedido_venda = v_num_pedido --:new.pedido_venda
              and pedi_010.cgc_9        = pedi_100.cli_ped_cgc_cli9
              and pedi_010.cgc_4        = pedi_100.cli_ped_cgc_cli4
              and pedi_010.cgc_2        = pedi_100.cli_ped_cgc_cli2;

     /*verifica se possui analise por grupo*/
           begin
             select fatu_500.acum_cred_corporacao
             into v_acum_cred_corporacao
             from fatu_500
             where fatu_500.codigo_empresa = v_cod_empr;
             exception
                when OTHERS then v_acum_cred_corporacao := 0;
           end;  
           
           begin  
	         select pedi_010.grupo_economico
	         into   v_grupo_economico
	         from pedi_010
	         where pedi_010.cgc_9      = v_cnpj9
	         and pedi_010.cgc_4      = v_cnpj4
	         and pedi_010.cgc_2      = v_cnpj2;
	         exception
	            when OTHERS then v_grupo_economico := 0;
	       end;
	           
	       begin
	         select pedi_220.ativo
	         into v_grupo_economico_ativo
	         from pedi_220
	         where pedi_220.codigo = v_grupo_economico
	         and   pedi_220.tipo   = 8;
	         exception
	            when OTHERS then v_grupo_economico_ativo := 0;
           end;
           
           if not (v_acum_cred_corporacao = 4 and v_grupo_economico > 0 and v_grupo_economico_ativo = 1)
           then

              --Rej com limite sugerido verifica se considera cheques

              begin
                select fatu_502.cons_cheque_ana_credito
                  into v_par_cheque
                  from fatu_502
                 where fatu_502.codigo_empresa = v_cod_empr;
                 exception
                    when no_data_found then v_par_cheque := 'N';
              end;

              --Rej com lim sugerido verifica se considera lim por empresa
              begin
                select fatu_503.limite_credito_empresa
                  into v_ana_empresa
                  from fatu_503
                 where fatu_503.codigo_empresa = v_cod_empr;
                 exception
                   when no_data_found then v_ana_empresa := 0;
              end;

              --Rej com lim sugerido verifica se considera lim por cliente ou grupo
              begin
                select fatu_500.acum_cred_corporacao
                into v_ana_cred_corp
                from fatu_500
                where fatu_500.codigo_empresa = v_cod_empr;
                exception
                   when no_data_found then v_ana_cred_corp := 0;
              end;

              -- verifica o valor jÃ¡ utilizado para o limite
              --Rej com lim sugerido valor pedido
              begin
                select nvl(sum(pedi_100.valor_saldo_pedi), 0.00)
                  into v_acum_ped
                  from pedi_100
                 where pedi_100.codigo_empresa   = decode(v_ana_empresa,1, pedi_100.codigo_empresa,v_cod_empr)
                   and pedi_100.situacao_venda     not in (10,5)
                   and pedi_100.cod_cancelamento = 0
                   and pedi_100.tecido_peca      =  v_niv_pedido
                   and ((pedi_100.cli_ped_cgc_cli9 = v_cnpj9 and
                         pedi_100.cli_ped_cgc_cli4 = v_cnpj4 and
                         pedi_100.cli_ped_cgc_cli2 = v_cnpj2) or
                        (v_ana_cred_corp = 1 and pedi_100.cli_ped_cgc_cli9 = v_cnpj9) or
                        (v_ana_cred_corp = 3 and v_grupo_econ > 0 and
                         exists (select 1 from pedi_010
                                  where pedi_010.cgc_9 =  pedi_100.cli_ped_cgc_cli9
                                    and pedi_010.cgc_4 =  pedi_100.cli_ped_cgc_cli4
                                    and pedi_010.cgc_2 =  pedi_100.cli_ped_cgc_cli2
                                    and pedi_010.grupo_economico  = v_grupo_econ)
                         ));
              exception
                when no_data_found then
                     v_acum_ped := 0.00;
              end;

              --Rej com lim sugerido valor titulos
              begin
                select nvl( sum(fatu_070.saldo_duplicata), 0.00)
                  into v_acum_tit
                  from fatu_070
                 where fatu_070.codigo_empresa  = decode(v_ana_empresa,1, fatu_070.codigo_empresa,v_cod_empr)
                   and fatu_070.situacao_duplic not in (1,2)
                   and fatu_070.tecido_peca     = v_niv_pedido
                   and ((fatu_070.cli_dup_cgc_cli9 = v_cnpj9 and
                         fatu_070.cli_dup_cgc_cli4 = v_cnpj4 and
                         fatu_070.cli_dup_cgc_cli2 = v_cnpj2) or
                        (v_ana_cred_corp = 1 and fatu_070.cli_dup_cgc_cli9 = v_cnpj9) or
                        (v_ana_cred_corp = 3 and v_grupo_econ > 0 and
                         exists (select 1 from pedi_010
                                  where pedi_010.cgc_9 = fatu_070.cli_dup_cgc_cli9
                                    and pedi_010.cgc_4 = fatu_070.cli_dup_cgc_cli4
                                    and pedi_010.cgc_2 = fatu_070.cli_dup_cgc_cli2
                                    and pedi_010.grupo_economico  = v_grupo_econ)
                        ));
              exception
                when no_data_found then
                     v_acum_tit := 0.00;
              end;

              --Rej com lim sugerido valor cheque
              v_acum_cheque := 0.00;

              if v_par_cheque = 'S'
              then
                begin
                  select nvl( sum(crec_180.valor_usado), 0.00)
                    into v_acum_cheque
                    from crec_180
                   where crec_180.empresa = decode(v_ana_empresa, 1, crec_180.empresa, v_cod_empr)
                     and ((crec_180.cgc9_cli = v_cnpj9 and
                           crec_180.cgc4_cli = v_cnpj4 and
                           crec_180.cgc2_cli = v_cnpj2) or
                          (v_ana_cred_corp = 1 and crec_180.cgc9_cli = v_cnpj9) or
                          (v_ana_cred_corp = 3 and v_grupo_econ > 0 and
                           exists (select 1 from pedi_010
                                    where pedi_010.cgc_9 = crec_180.cgc9_cli
                                      and pedi_010.cgc_4 = crec_180.cgc4_cli
                                      and pedi_010.cgc_2 = crec_180.cgc2_cli
                                      and pedi_010.grupo_economico  = v_grupo_econ)
                          ))
                     and crec_180.flag_reg       = 0
                     and exists (select 1 from fatu_070
                                  where fatu_070.codigo_empresa = crec_180.empresa
                                    and fatu_070.cli_dup_cgc_cli9 = crec_180.cgc9_cli
                                    and fatu_070.cli_dup_cgc_cli4 = crec_180.cgc4_cli
                                    and fatu_070.cli_dup_cgc_cli2 = crec_180.cgc2_cli
                                    and fatu_070.tipo_titulo      = crec_180.tipo_titulo
                                    and fatu_070.num_duplicata    = crec_180.numero_titulo
                                    and fatu_070.seq_duplicatas   = crec_180.parcela_titulo
                                    and fatu_070.tecido_peca      =  v_niv_pedido);
                exception
                  when no_data_found then
                       v_acum_cheque := 0.00;
                end;
              end if;

              --Rej com lim sugerido valor total utilizado
              v_disp_lib :=  :new.limite_ofertado - ( v_acum_ped + v_acum_tit - v_acum_cheque) ;

              /*GRAVA REGISTRO NAS INFORMACOES GERAIS*/
              /*Verifica os dados do pedido de venda*/
              select pedi_100.codigo_empresa,   pedi_100.cli_ped_cgc_cli9,
                     pedi_100.cli_ped_cgc_cli4, pedi_100.cli_ped_cgc_cli2,
                     pedi_100.situacao_venda,   pedi_100.tecido_peca,
                     pedi_100.valor_saldo_pedi
                into v_cod_empr,                v_cnpj9,
                     v_cnpj4,                   v_cnpj2,
                     v_sit_pedido,              v_niv_pedido,
                     v_val_sld_pedi
                from pedi_100
               where pedi_100.pedido_venda = v_num_pedido;

              /*VERIFICA O LIMITE DE CREDITO ATUAL DO CLIENTE*/
              -- SYP: (71616) Se adiciona la variable v_concepto
              select pedi_010.limite_max_ped1, pedi_010.limite_max_ped2,
                     pedi_010.limite_max_ped4, pedi_010.limite_max_ped7,
                     pedi_010.conceito_cliente
                into v_limite1,                v_limite2,
                     v_limite4,                v_limite7,
                     v_concepto
                from pedi_010
               where pedi_010.cgc_9 = v_cnpj9
                 and pedi_010.cgc_4 = v_cnpj4
                 and pedi_010.cgc_2 = v_cnpj2;

              /*Seleciona valor para limite atual de acordo com nivel*/
              if v_niv_pedido = '1' then
                v_limite_atual := v_limite1;
              end if;

              if v_niv_pedido = '2' then
                v_limite_atual := v_limite2;
              end if;

              if v_niv_pedido = '4' then
                v_limite_atual := v_limite4;
              end if;

              if v_niv_pedido = '7' then 
                v_limite_atual := v_limite7;
              end if;

              -- SYP: (71616) Se selecciona el nro de dias registrado para el concepto
              select pedi_086.nr_dias_validade_limite
                into v_ndias
                from pedi_086
               where pedi_086.codigo_conceito = v_concepto;

              /*VERIFICA SE A CAPA JÃ? FOI INSERIDA*/
              begin
                select pedi_240.tipo_informacao
                  into v_tp_info
                  from pedi_240
                 where pedi_240.infocli_cgc_cli9  = v_cnpj9
                   and pedi_240.infocli_cgc_cli4  = v_cnpj4
                   and pedi_240.infocli_cgc_cli2  = v_cnpj2
                   and pedi_240.tipo_informacao   = 97
                   and pedi_240.data_informacao   = :new.data_retorno
                   and pedi_240.informante        = 'SERASA';
              exception
                when OTHERS then
                   INSERT INTO pedi_240
                          (infocli_cgc_cli9,
                           infocli_cgc_cli4,
                           infocli_cgc_cli2,
                           tipo_informacao,
                           data_informacao,
                           informante,
                           status_inf,
                           historico_inf,
                           acesso_inf)
                        VALUES
                          (v_cnpj9,
                           v_cnpj4,
                           v_cnpj2,
                           97,
                           :new.data_retorno,
                           'SERASA',
                           0,
                           0,
                           2);
              end;

              /*Acha proxima sequencia*/
              select nvl(max(pedi_245.sequencia),0) + 1
                into v_seq_info
                from pedi_245
               where pedi_245.informe_infocli_cgc_cli9 = v_cnpj9
                 and pedi_245.informe_infocli_cgc_cli4 = v_cnpj4
                 and pedi_245.informe_infocli_cgc_cli2 = v_cnpj2
                 and pedi_245.informe_tipo_inf         = 97
                 and pedi_245.informe_datainfo         = :new.data_retorno
                 and pedi_245.informe_informte         = 'SERASA'
                 and pedi_245.informe_historico_inf    = 0;

              /*InclusÃ£o para o Pedido*/
              v_msg_pedido := ' VL'       || to_char(v_saldo_pedi,'0000000.00')||
                              ' '         || to_char(:new.data_consulta,'DD/MM/YY') ||
                              ' LIM:ATU'  || to_char(v_limite_atual,'00000000')     ||
                              ' OFERTADO' || to_char(:new.limite_ofertado,'00000000');

              INSERT INTO pedi_245
                   (informe_infocli_cgc_cli9,
                    informe_infocli_cgc_cli4,
                    informe_infocli_cgc_cli2,
                    informe_tipo_inf,
                    informe_datainfo,
                    informe_informte,
                    sequencia,
                    informacao,
                    informe_historico_inf)
                VALUES
                   (v_cnpj9,
                    v_cnpj4,
                    v_cnpj2,
                    97,
                    :new.data_retorno,
                    'SERASA',
                    v_seq_info,
                    substr(v_msg_pedido,2,61),
                    0);

              /*InclusÃ£o para a mensagem*/
              v_seq_info := v_seq_info + 1;

              INSERT INTO pedi_245
                   (informe_infocli_cgc_cli9,
                    informe_infocli_cgc_cli4,
                    informe_infocli_cgc_cli2,
                    informe_tipo_inf,
                    informe_datainfo,
                    informe_informte,
                    sequencia,
                    informacao,
                    informe_historico_inf)
                VALUES
                   (v_cnpj9,
                    v_cnpj4,
                    v_cnpj2,
                    97,
                    :new.data_retorno,
                    'SERASA',
                    v_seq_info,
                    substr(:new.mensagem,1,60),
                    0);
              if v_val_sld_pedi <= v_disp_lib then  -- Aprovado
                v_disp_lib := v_disp_lib - v_val_sld_pedi;
                /*Somente se novo limite > zero*/
                if :new.novo_limite <= 0 then
                   v_limite1 :=  v_limite1;
                   v_limite2 :=  v_limite2;
                   v_limite4 :=  v_limite4;
                   v_limite7 :=  v_limite7;
                else
                  /*Seleciona valor para limite de acordo com nivel*/
                  if v_niv_pedido = '1' then
                    v_limite1 := :new.limite_ofertado;
                  end if;

                  if v_niv_pedido = '2' then
                    v_limite2 := :new.limite_ofertado;
                  end if;

                  if v_niv_pedido = '4' then
                    v_limite4 := :new.limite_ofertado;
                  end if;

                  if v_niv_pedido = '7' then
                    v_limite7 := :new.limite_ofertado;
                  end if;
                end if;

                /*Verifica o nr de dias para o serasa*/
                select fatu_502.nr_dias_serasa
                  into v_nr_dias_lim
                  from fatu_502
                 where fatu_502.codigo_empresa = v_cod_empr;

                -- SYP: (71616) Verifica si el nro de dÃ­as del concepto es mayor a cero. Si es si, toma ese nro de dÃ­as,
                --              caso contrario, toma el de la configuraciÃ³n.
                if v_ndias > 0 then
                  v_data_limite := :new.data_retorno + v_ndias;
                else
                  if v_nr_dias_lim > 0 then
                    v_data_limite := :new.data_retorno + v_nr_dias_lim;
                  else
                    select pedi_010.val_lim_credito
                      into v_data_limite
                      from pedi_010
                     where pedi_010.cgc_9 = v_cnpj9
                       and pedi_010.cgc_4 = v_cnpj4
                       and pedi_010.cgc_2 = v_cnpj2;
                  end if;
                end if;

                /*Altera o limite de credito e a dta de validade do limite*/
                update pedi_010 set
                       val_lim_credito = v_data_limite,
                       limite_max_ped1 = v_limite1,
                       limite_max_ped2 = v_limite2,
                       limite_max_ped4 = v_limite4,
                       limite_max_ped7 = v_limite7
                 where pedi_010.cgc_9  = v_cnpj9
                   and pedi_010.cgc_4  = v_cnpj4
                   and pedi_010.cgc_2  = v_cnpj2;
                   

                /*ROTINA PARA LIBERAÃ¿Ã¿O DOS BLOQUEIOS COM CONSULTA AO SERASA*/
                update pedi_135 set
                       flag_liberacao = 'M',
                       responsavel    = 'SERASA',
                       data_liberacao = :new.data_retorno
                 where pedi_135.pedido_venda = v_num_pedido
                   and pedi_135.flag_liberacao = 'N'
                    and ( ( pedi_135.codigo_situacao = 74) or  
                            exists (select *
                                  from hdoc_100
                                 where hdoc_100.sit_bloqueio       = pedi_135.codigo_situacao
                                   and hdoc_100.consulta_serasa    = 1
                                   and hdoc_100.situacao_liberador = 0) );

              else  -- Rejeitado
                if :old.flag_retorno = 0 then -- rejeitado pela primeira vez
                  v_bloq_rej := 45;
                else
                  v_bloq_rej := 38;

                  /*Verifica se existe liberador para o bloqueio do Serasa*/
                  begin
                    select hdoc_100.sit_bloqueio
                      into v_bloqueio
                      from hdoc_100
                     where hdoc_100.usubloq_empr_usu   = v_cod_empr
                       and hdoc_100.area_bloqueio      = 1
                       and hdoc_100.sit_bloqueio       = v_bloq_rej
                       and hdoc_100.situacao_liberador = 0
                     group by hdoc_100.sit_bloqueio;
                  exception
                    when OTHERS then
                       v_bloqueio := 0;
                  end;

                  if  v_bloqueio > 0 then
                    /*Verifica os dados do pedido de venda*/
                    select pedi_100.situacao_venda
                      into v_sit_pedido
                      from pedi_100
                     where pedi_100.pedido_venda = v_num_pedido;

                    if v_sit_pedido <> 10 then -- 10 pedido faturado
                      /*Somente se houver liberador os bloq. serÃ£o liberados */
                      /*ROTINA PARA LIBERAÃ¿Ã¿O DOS BLOQUEIOS COM CONSULTA AO SERASA*/
                      update pedi_135 set
                             flag_liberacao = 'M',
                             responsavel    = 'SERASA',
                             data_liberacao = :new.data_retorno
                       where pedi_135.pedido_venda = v_num_pedido
                         and pedi_135.flag_liberacao = 'N'
                         and ( ( pedi_135.codigo_situacao = 74) or  
                            exists (select *
                                  from hdoc_100
                                 where hdoc_100.sit_bloqueio       = pedi_135.codigo_situacao
                                   and hdoc_100.consulta_serasa    = 1
                                   and hdoc_100.situacao_liberador = 0) );
                      begin
                        select pedi_100.origem_pedido
                          into v_origem_pedido
                          from pedi_100
                         where pedi_100.pedido_venda = v_num_pedido;
                      exception
                        when no_data_found then
                             v_origem_pedido := 0;
                      end;

                      v_tem_origem := 'n';

                      begin
                        select 's'
                          into v_tem_origem
                          from pedi_900
                         where pedi_900.origem_pedido = v_origem_pedido
                           and rownum = 1;
                      exception
                        when no_data_found then
                            v_tem_origem := 'n';
                      end;

                      if v_tem_origem = 's' then 
                        v_gera_bloqueio := 'n';
                      
                        begin
                          select 's'
                            into v_gera_bloqueio
                            from pedi_900
                           where pedi_900.origem_pedido   = v_origem_pedido
                             and pedi_900.motivo_bloqueio = v_bloq_rej;
                        exception
                          when no_data_found then
                               v_gera_bloqueio := 'n';
                        end;
                      else
                        v_gera_bloqueio := 's';
                      end if;

                      if v_gera_bloqueio = 's' then
                        select nvl(max(pedi_135.seq_situacao),0) + 1
                          into v_seq_bloq
                          from pedi_135
                         where pedi_135.pedido_venda = v_num_pedido;

                        /*GERA BLOQUEIO SERASA*/
                        insert into pedi_135
                              (pedido_venda,
                               seq_situacao,
                               codigo_situacao,
                               data_situacao,
                               flag_liberacao,
                               observacao)
                             VALUES
                              (v_num_pedido,
                               v_seq_bloq,
                               v_bloq_rej,
                               :new.data_retorno,
                               'N',
                               :new.mensagem);
                      end if;
                    end if; --sit pedido
                  end if; -- limite ofertado > 0
                end if; -- Pedido nÃ£o Faturado
              end if; -- Rejeitado

              select pedi_100.situacao_venda
                into v_sit_pedido
                from pedi_100
               where pedi_100.pedido_venda = v_num_pedido;

              if v_sit_pedido <> 10 and v_sit_pedido <> 9 then
                begin
                  select pedi_135.flag_liberacao
                    into v_flag_lib
                    from pedi_135
                   where pedi_135.pedido_venda   = v_num_pedido
                     and pedi_135.flag_liberacao = 'N'
                   group by pedi_135.flag_liberacao;
                exception
                   when OTHERS then
                      v_flag_lib := 'M';
                end;

                if v_flag_lib = 'N' then 
                   v_sit_pedido := 5;
                else 
                  v_sit_pedido := 0;
                end if;

                update pedi_100 set
                       situacao_venda = v_sit_pedido
                 where pedi_100.pedido_venda = v_num_pedido
                   and pedi_100.situacao_venda <> 10;
              end if;
           end if;
        END LOOP;/* fim looping que ordena pedidos pedidos*/
      end if; /*rejeita com sugerido*/
 
      /*ALTERA A FLAG DE RETORNO*/
      :new.flag_retorno := v_flag_retorno;
   end if; /*updating*/

end inter_tr_pedi_340_1;
/

exec inter_pr_recompile;

/* versao: 12 */


 exit;


 exit;
