
  CREATE OR REPLACE TRIGGER "INTER_TR_INTE_100_ADMIN" 
before insert on inte_100
for each row

declare
  v_administrador pedi_020.codigo_administr%type;
begin

  select pedi_020.codigo_administr
  into v_administrador
  from pedi_020
  where pedi_020.cod_rep_cliente = :new.cod_repre;

  :new.administrador := v_administrador;

end;

-- ALTER TRIGGER "INTER_TR_INTE_100_ADMIN" ENABLE
 

/

exec inter_pr_recompile;

