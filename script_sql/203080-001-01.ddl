CREATE TABLE obrf_017
(
	documento		NUMBER(9) 		NOT NULL,
	serie			VARCHAR(3)		NOT NULL,
	cgc_cli_for_9	NUMBER(9) 		NOT NULL,
	cgc_cli_for_4	NUMBER(4) 		NOT NULL,
	cgc_cli_for_2	NUMBER(2) 		NOT NULL,
	sequencia		NUMBER(4) 		NOT NULL,
	centro_custo	NUMBER(6) 		NOT NULL,
	proporcao		NUMBER(15,2) 	NOT NULL
);

COMMENT ON COLUMN obrf_017.documento IS 'Identifica o documento fiscal';
COMMENT ON COLUMN obrf_017.serie IS 'Identifica a série do documento fiscal';
COMMENT ON COLUMN obrf_017.cgc_cli_for_9 IS 'Identifica o CNPJ do cliente do documento fiscal';
COMMENT ON COLUMN obrf_017.cgc_cli_for_4 IS 'Identifica o CNPJ do cliente do documento fiscal';
COMMENT ON COLUMN obrf_017.cgc_cli_for_2 IS 'Identifica o CNPJ do cliente do documento fiscal';
COMMENT ON COLUMN obrf_017.sequencia IS 'Identifica a sequência do item do documento fiscal';
COMMENT ON COLUMN obrf_017.centro_custo IS 'Identifica o centro de custo de rateio';
COMMENT ON COLUMN obrf_017.proporcao IS 'Identifica a proporção de rateiro para o centro de custo';

ALTER TABLE obrf_017 ADD CONSTRAINT pk_obrf_017 primary key (documento, serie, cgc_cli_for_9, cgc_cli_for_4, cgc_cli_for_2, sequencia, centro_custo);

exec inter_pr_recompile;
/
