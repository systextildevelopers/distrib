CREATE OR REPLACE TRIGGER INTER_TR_PCPC_101 --titulo da trigger
   before insert --define que vai ser utilizada a trigger ao inserir na tabela pcpc_100
       or delete --define que vai ser utilizada a trigger ao excluir na tabela pcpc_100
       or update --define que vai ser utilizada a trigger ao atualizar na tabela pcpc_100
     on pcpc_101
   for each row

-- Históricos de alterações na trigger
--
-- Data       Autor           Motivo         Observações
-- 08/07/13   Estefânio   SS-74078/255      Trigger para atualização da tabela pcpc_100.

declare    --declaração das variáveis
 v_situacao_pcpc_100            pcpc_100.situacao%type;
 v_qtde_pecas_programada        pcpc_040.qtde_pecas_prog%type;
 v_estagio_pcpc_100             pcpc_100.codigo_estagio%type;
 v_qtde_produzida_ant           pcpc_100.qtde_produzida%type;
 v_qtde_produzida_pcpc_100      pcpc_100.qtde_produzida%type;

begin
  if inserting or updating--quando estamos inserindo ou atualizando o registro
  then
  v_situacao_pcpc_100 := 0;

  begin
    select pcpc_100.codigo_estagio, pcpc_100.qtde_produzida
    into   v_estagio_pcpc_100, v_qtde_produzida_ant
    from   pcpc_100
    where pcpc_100.ordem_producao = :new.ordem_producao
    and pcpc_100.ordem_confeccao   = :new.ordem_confeccao
    and pcpc_100.seq_operacao      = :new.seq_operacao;
  exception
    when no_data_found then
    v_estagio_pcpc_100 := 0;
    v_qtde_produzida_ant := 0;
  end;

  begin
    select sum(pcpc_040.qtde_pecas_prog)
    into v_qtde_pecas_programada
    from pcpc_040
    where pcpc_040.ordem_producao  = :new.ordem_producao
    and pcpc_040.codigo_estagio    = v_estagio_pcpc_100
    and pcpc_040.ordem_confeccao   = :new.ordem_confeccao;
  exception
    when no_data_found then
    v_qtde_pecas_programada := 0;
  end;
 
  if inserting and :new.qtde_produzida > 0
  then
    if (:new.qtde_produzida + v_qtde_produzida_ant)       = v_qtde_pecas_programada
    then v_situacao_pcpc_100    := 2;   --baixa total
      else v_situacao_pcpc_100  := 1;   --baixa parcial
    end if;

    update pcpc_100
    set pcpc_100.situacao          = v_situacao_pcpc_100,
        pcpc_100.qtde_produzida    = pcpc_100.qtde_produzida + :new.qtde_produzida,
        pcpc_100.data_baixa        = :new.data_producao,
        pcpc_100.hora_baixa        = :new.hora_producao
    where pcpc_100.ordem_producao  = :new.ordem_producao
    and pcpc_100.ordem_confeccao   = :new.ordem_confeccao
    and pcpc_100.seq_operacao      = :new.seq_operacao;
  end if;

  if updating and :new.qtde_produzida <> :old.qtde_produzida
  then
      /* antony */
      if (:new.qtde_produzida < :old.qtde_produzida) -- estorno
      then 
        v_qtde_produzida_pcpc_100 := v_qtde_produzida_ant - (:old.qtde_produzida-:new.qtde_produzida);
      else
        v_qtde_produzida_pcpc_100 := v_qtde_produzida_ant + (:old.qtde_produzida+:new.qtde_produzida);
      end if;
      
      /* antony */
  
  
      if v_qtde_produzida_pcpc_100 = 0
      then v_situacao_pcpc_100 := 0; --pendente
      else
        if v_qtde_produzida_pcpc_100   = v_qtde_pecas_programada
        then v_situacao_pcpc_100  := 2;   --baixa total
        else v_situacao_pcpc_100  := 1;   --baixa parcial
      end if;
      end if;

      update pcpc_100
      set pcpc_100.situacao          = v_situacao_pcpc_100,
          pcpc_100.qtde_produzida    = v_qtde_produzida_pcpc_100
      where pcpc_100.ordem_producao  = :new.ordem_producao
      and pcpc_100.ordem_confeccao   = :new.ordem_confeccao
      and pcpc_100.seq_operacao      = :new.seq_operacao;
  end if; --if updating
  end if; --if inserting or updating

  if deleting
  then
    begin
     select nvl(sum(pcpc_100.qtde_produzida),0)
     into v_qtde_produzida_pcpc_100
     from pcpc_100
     where pcpc_100.ordem_producao  = :old.ordem_producao
     and pcpc_100.ordem_confeccao   = :old.ordem_confeccao
     and pcpc_100.seq_operacao      = :old.seq_operacao;
     exception
       when no_data_found then
       v_qtde_produzida_pcpc_100 := 0;

    end;

    if v_qtde_produzida_pcpc_100 > 0
    then

      if (v_qtde_produzida_pcpc_100 - :old.qtde_produzida)       = 0
      then v_situacao_pcpc_100    := 0;   --pendente
        else v_situacao_pcpc_100  := 1;   --baixa parcial
      end if;

      update pcpc_100
      set pcpc_100.situacao          = v_situacao_pcpc_100,
          pcpc_100.qtde_produzida    = pcpc_100.qtde_produzida - :old.qtde_produzida
      where pcpc_100.ordem_producao  = :old.ordem_producao
      and pcpc_100.ordem_confeccao   = :old.ordem_confeccao
      and pcpc_100.seq_operacao      = :old.seq_operacao;
    end if;
  end if;

end inter_tr_pcpc_101;       --fim da trigger

/

exec inter_pr_recompile;
