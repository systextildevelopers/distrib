create table FNDC_006
(
  cod_empresa     NUMBER(3) not null,
  cgc9            NUMBER(9),
  cgc4            NUMBER(4),
  cgc2            NUMBER(2),
  transacao       NUMBER(4),
  historico       NUMBER(4),
  tp_tit_recompra NUMBER(2),
  c_custo         NUMBER(4),
  transacao_jur   NUMBER(4)
);

alter table FNDC_006
  add constraint PK_FNDC_006 primary key (COD_EMPRESA);
 
 /
 
 exec inter_pr_recompile;
