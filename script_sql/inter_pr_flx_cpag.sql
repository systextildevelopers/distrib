CREATE OR REPLACE PROCEDURE "INTER_PR_FLX_CPAG" (p_cod_usuario          IN NUMBER,
                                               p_nom_usuario          IN VARCHAR2,
                                               p_data_lida            IN DATE,
                                               p_data_ini_consulta    IN DATE,
                                               p_data_fim_consulta    IN DATE,
                                               p_inclui_port_rec      IN NUMBER,
                                               p_filtro_empresa       IN NUMBER,
                                               p_incl_exce            IN NUMBER,
                                               p_opcao_posic          IN NUMBER,
                                               p_posic01              IN NUMBER,
                                               p_posic02              IN NUMBER,
                                               p_posic03              IN NUMBER,
                                               p_posic04              IN NUMBER,
                                               p_posic05              IN NUMBER,
                                               p_cod_emp1             IN NUMBER,
                                               p_cod_emp2             IN NUMBER,
                                               p_cod_emp3             IN NUMBER,
                                               p_cod_emp4             IN NUMBER,
                                               p_cod_emp5             IN NUMBER,
                                               p_cpag_confir_vencido  IN NUMBER,
                                               p_cpag_previs_vencido  IN NUMBER,
                                               p_origem_dados_lh_tp   IN NUMBER,
                                               p_programa             IN VARCHAR2,
                                               p_data_ini             IN NUMBER,
                                               p_des_erro             OUT varchar2) is
--
-- Finalidade: Gerar a tabela oper_flx - Fluxo de caixa
-- Autor.....: Edson Pio
-- Data......: 20/10/10

w_erro                 EXCEPTION;
w_tipo_historico       cont_010.tipo_historico%type;
w_fluxo                cont_010.codigo_historico%type;
w_nome_empr            fatu_500.nome_empresa%type;
w_controla_vencto      fatu_500.controla_vencto%type;
w_nome_for             supr_010.nome_fornecedor%type;
w_tp_fornec            supr_010.tipo_fornecedor%type;
w_tipo_agrupador       number(2);
w_cod_agrupador        supr_130.cod_agrupador%type;
w_descricao_grupo      supr_130.descricao%type;
w_descr_tipo_fornec    rcnb_060.descricao%type;
w_nome_tipo            cpag_040.descricao%type;
w_entra_fluxo_caixa    cpag_040.entra_fluxo_caixa%type;
w_nome_portador        pedi_050.nome_banco%type;
w_nome_moeda           basi_265.descricao%type;
w_sequencia            oper_flx.sequencia%type;
w_indice_moeda_titulo  basi_270.valor_moeda%type;
w_he_dia_util_3        basi_260.dia_util_finan%type;
w_data_util_3          basi_260.data_calendario%type;
w_tipo_registro_flxc   varchar2(20);
w_letra                varchar2(1);
w_descr_conta_fluxo    pedi_085.descr_tipo_clien%type;
w_ja_inseriu           number(1);
w_achou_registro       number(1);
w_vencimento_aux       cpag_010.data_vencimento%type;
BEGIN
   w_ja_inseriu := 0;
   FOR cpag in (
      select
            cpag_010.data_vencimento,  cpag_010.valor_parcela,
             cpag_010.codigo_historico, cpag_010.nr_duplicata,
             cpag_010.parcela,          cpag_010.cgc_9,
             cpag_010.cgc_4,            cpag_010.cgc_2,
             cpag_010.tipo_titulo,      cpag_010.valor_irrf,
             cpag_010.valor_iss,        cpag_010.valor_inss,
             cpag_010.codigo_empresa ,  cpag_010.valor_moeda,
             cpag_010.moeda_titulo,
             cpag_010.previsao,         cpag_010.cod_portador,
             cpag_010.posicao_titulo,   cpag_010.sit_bloqueio,
             cpag_010.serie,            cpag_010.saldo_titulo
   from cpag_010
   where (cpag_010.data_vencimento  = p_data_lida or
          (p_data_ini = 1                          and
          cpag_010.data_vencimento < p_data_ini_consulta      and
          cpag_010.saldo_titulo > 0)

          )
     and   cpag_010.cod_cancelamento = 0
     and  (cpag_010.situacao         = 0 or cpag_010.situacao = 3 )
     and ((p_inclui_port_rec = 1
     and   exists (select 1 from rcnb_060
                   where rcnb_060.tipo_registro    = 139
                    and rcnb_060.nr_solicitacao    = 810
                     and rcnb_060.subgru_estrutura = p_cod_usuario
                     and rcnb_060.item_estrutura   = 2
                     and rcnb_060.grupo_estrutura  = cpag_010.cod_portador))
      or  (p_inclui_port_rec = 2
     and   not exists (select 1 from rcnb_060
                       where rcnb_060.tipo_registro    = 139
                         and rcnb_060.nr_solicitacao   = 810
                         and rcnb_060.subgru_estrutura = p_cod_usuario
                         and rcnb_060.item_estrutura   = 2
                         and rcnb_060.grupo_estrutura  = cpag_010.cod_portador
                         and rcnb_060.grupo_estrutura  <> 9999)))
     and (p_filtro_empresa = 1
           and (p_incl_exce = 1
                and cpag_010.codigo_empresa in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
           or  (p_incl_exce = 2
                and cpag_010.codigo_empresa not in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
      or (p_filtro_empresa = 2
            and (p_incl_exce = 1
                and cpag_010.cod_end_cobranca in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
            or  (p_incl_exce = 2 )
                and cpag_010.cod_end_cobranca in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5)))

     and (   (p_opcao_posic = 1 and cpag_010.posicao_titulo in (p_posic01, p_posic02,p_posic03,p_posic04,p_posic05))
          or (p_opcao_posic = 2 and cpag_010.posicao_titulo not in (p_posic01, p_posic02,p_posic03,p_posic04,p_posic05))
          )

     order by cpag_010.previsao, cpag_010.data_vencimento asc

   )
   LOOP
      w_vencimento_aux := cpag.data_vencimento;
      w_achou_registro := 1;
      BEGIN
         select cont_010.tipo_historico,    cont_010.fluxo
         into   w_tipo_historico,           w_fluxo
         from cont_010
         where cont_010.codigo_historico = cpag.codigo_historico;
      EXCEPTION
         when no_data_found then
            w_tipo_historico := 0;
            w_fluxo          := 0;
      END;

      if w_tipo_historico <> 'NC' and w_fluxo = 1
      then
         BEGIN
            select fatu_500.nome_empresa, fatu_500.controla_vencto
            into   w_nome_empr,           w_controla_vencto
            from fatu_500
            where fatu_500.codigo_empresa = cpag.codigo_empresa;
         EXCEPTION
            when no_data_found then
               w_nome_empr        := ' ';
               w_controla_vencto  := 0;
         END;

         BEGIN
            select supr_010.nome_fornecedor, supr_010.tipo_fornecedor
            into   w_nome_for,               w_tp_fornec
            from supr_010
            where supr_010.fornecedor9 = cpag.cgc_9
              and supr_010.fornecedor4 = cpag.cgc_4
              and supr_010.fornecedor2 = cpag.cgc_2;
         EXCEPTION
            when no_data_found then
               w_tipo_historico := 0 ;
               w_fluxo          := 0;
         END;

         BEGIN
            select supr_130.tipo_agrupador, supr_130.cod_agrupador,
                   supr_130.descricao
            into   w_tipo_agrupador,        w_cod_agrupador,
                   w_descricao_grupo
            from supr_130
            where supr_130.tp_forne = w_tp_fornec;
         EXCEPTION
            when no_data_found then
               w_descr_tipo_fornec := ' ';
               w_tipo_agrupador    := 0;
               w_cod_agrupador     := 0;
         END;

         if w_tipo_agrupador = 1 and w_cod_agrupador <> 0
         then
            BEGIN
               select rcnb_060.descricao into w_descr_tipo_fornec from rcnb_060
               where rcnb_060.tipo_registro  = 128
                 and rcnb_060.nr_solicitacao = w_cod_agrupador;
            EXCEPTION
               when no_data_found then
                  w_descr_tipo_fornec := ' ';
            END;
         else
            w_descr_tipo_fornec := w_descricao_grupo;
            w_cod_agrupador    := w_tp_fornec;
         end if;

         BEGIN
            select cpag_040.descricao, cpag_040.entra_fluxo_caixa
            into   w_nome_tipo,        w_entra_fluxo_caixa
            from cpag_040
            where cpag_040.tipo_titulo = cpag.tipo_titulo;
         EXCEPTION
            when no_data_found then
               w_nome_tipo         := ' ';
               w_entra_fluxo_caixa := 0;
         END;

         if w_entra_fluxo_caixa = 1
         then
            w_descr_tipo_fornec := w_nome_tipo;
            w_cod_agrupador    := cpag.tipo_titulo;
         end if;

         BEGIN
            select pedi_050.nome_banco into w_nome_portador from pedi_050
            where pedi_050.cod_portador = cpag.cod_portador;
         EXCEPTION
            when no_data_found then
               w_descr_tipo_fornec := 0;
         END;

         BEGIN
            select basi_270.valor_moeda into w_indice_moeda_titulo from basi_270
            where basi_270.codigo_moeda = cpag.moeda_titulo
              and basi_270.data_moeda   <= cpag.data_vencimento
              and rownum                 = 1
            order by basi_270.data_moeda desc;
         EXCEPTION
            when no_data_found then
               w_indice_moeda_titulo := 0;
         END;

         if w_indice_moeda_titulo = 0.000
         then
            w_indice_moeda_titulo := 1.0000;
         end if;

         if cpag.moeda_titulo > 0 and  cpag.saldo_titulo <> 0.00
         then
            cpag.saldo_titulo := round(cpag.saldo_titulo * w_indice_moeda_titulo);
         end if;

         w_letra              := 'B';
         w_tipo_registro_flxc := 'pagtos';

         if cpag.previsao = 0
         then
            -- TIPO DE TITULOS DE CONFIRMADOS


            if cpag.saldo_titulo > 0.00 and cpag.data_vencimento < to_date(sysdate, 'DD/MM/YY')
            then
               /*
                 1- Descartar Titulo;
                 2- Trazer a data de vencimento para a data atual;
                 3- Trazer a data de vencimento para o primeiro dia util apos a data corrente;
                 4- Trazer a data de vencimento para o ultimo dia do mes de vencimento;
               */
               if p_cpag_confir_vencido = 1
               then
                  cpag.saldo_titulo := 0.00;
               else
                  if p_cpag_confir_vencido = 2
                  then
                     cpag.data_vencimento := to_date(sysdate, 'DD/MM/YY');
                  else
                     if p_cpag_confir_vencido = 3
                     then
                        /* PROCURA DIA UTIL FINAL */
                        BEGIN
                           select  basi_260.dia_util_finan into w_he_dia_util_3 from basi_260
                           where basi_260.data_calendario = to_date(sysdate, 'DD/MM/YY') + 1;
                        EXCEPTION
                           when no_data_found then
                              w_he_dia_util_3 := null;
                        END;

                        if w_he_dia_util_3 is null
                        then
                           w_he_dia_util_3 := 1;
                        end if;

                        if w_he_dia_util_3 = 1
                        then
                           BEGIN
                              select data_calendario into w_data_util_3 from basi_260
                              where basi_260.data_calendario >= to_date(sysdate, 'DD/MM/YY') + 1
                                and basi_260.dia_util_finan   = 0
                                and rownum                    = 1
                              order by basi_260.data_calendario asc;
                           EXCEPTION
                              when no_data_found then
                                 w_data_util_3 := null;
                           END;
                        else
                           w_data_util_3 := to_date(sysdate, 'DD/MM/YY') + 1;
                        end if;

                        cpag.data_vencimento := w_data_util_3;
                     else
                        if p_cpag_confir_vencido = 4
                        then
                           if to_number(to_char(cpag.data_vencimento, 'MM')) < to_number(to_char(sysdate, 'MM'))
                           then
                              cpag.data_vencimento := to_date(sysdate, 'DD/MM/YY');
                           end if;

                           /* CASO O ULTIMO DIA SEJA O DIA CORRENTE, VAI PARA O ULTIMO DIA DO MES SUBSEQUENTE */
                           if Last_day(to_date(cpag.data_vencimento, 'DD/MM/YY')) = to_date(sysdate, 'DD/MM/YY')
                           then
                              cpag.data_vencimento := ADD_MONTHS(cpag.data_vencimento, 1);
                           end if;

                            BEGIN
                               select  data_calendario into w_data_util_3 from basi_260
                               where basi_260.data_calendario <= Last_day(to_date(cpag.data_vencimento, 'DD/MM/YY'))
                                 and basi_260.dia_util_finan   = 0
                                 and rownum                    = 1
                               order by basi_260.data_calendario desc;
                            EXCEPTION
                               when no_data_found then
                                  w_data_util_3 := Last_day(to_date(sysdate, 'DD/MM/YY'));
                            END;

                            cpag.data_vencimento := w_data_util_3;
                         end if;
                     end if;
                  end if;
               end if;
            end if;
         else

            -- TIPO DE TITULOS DE PREVISTOS



            if cpag.saldo_titulo > 0.00 and cpag.data_vencimento < to_date(sysdate, 'DD/MM/YY')
            then
               /*
                 1- Descartar Titulo;
                 2- Trazer a data de vencimento para a data atual;
                 3- Trazer a data de vencimento para o primeiro dia util apos a data corrente;
                 4- Trazer a data de vencimento para o ultimo dia do mes de vencimento;
               */
               if p_cpag_previs_vencido = 1
               then
                  cpag.saldo_titulo := 0.00;
               else
                  if p_cpag_previs_vencido = 2
                  then
                     cpag.data_vencimento := to_date(sysdate, 'DD/MM/YY');
                  else
                     if p_cpag_previs_vencido = 3
                     then
                        /* PROCURA DIA UTIL FINAL */
                        BEGIN
                           select basi_260.dia_util_finan into w_he_dia_util_3 from basi_260
                           where basi_260.data_calendario = to_date(sysdate, 'DD/MM/YY') + 1;
                        EXCEPTION
                           when no_data_found then
                              w_he_dia_util_3 := null;
                        END;

                        if w_he_dia_util_3 is not null
                        then
                           w_he_dia_util_3 := 1;
                        end if;

                        if w_he_dia_util_3 = 1
                        then
                           BEGIN
                              select data_calendario into w_data_util_3 from basi_260
                              where basi_260.data_calendario >= to_date(sysdate, 'DD/MM/YY') + 1
                                and basi_260.dia_util_finan   = 0
                                and rownum                    = 1
                              order by basi_260.data_calendario asc;
                           EXCEPTION
                              when no_data_found then
                                 w_data_util_3 := null;
                           END;
                        else
                           w_data_util_3 := to_date(sysdate, 'DD/MM/YY') + 1;
                        end if;

                        cpag.data_vencimento := w_data_util_3;
                     else
                        if p_cpag_previs_vencido = 4
                        then

                           if to_number(to_char(cpag.data_vencimento, 'MM')) < to_number(to_char(sysdate, 'MM'))
                           then
                              cpag.data_vencimento := to_date(sysdate, 'DD/MM/YY');
                           end if;

                           /* CASO O ULTIMO DIA SEJA O DIA CORRENTE, VAI PARA O ULTIMO DIA DO MES SUBSEQUENTE */
                           if Last_day(to_date(cpag.data_vencimento, 'DD/MM/YY')) = to_date(sysdate, 'DD/MM/YY')
                           then
                              cpag.data_vencimento := ADD_MONTHS(cpag.data_vencimento, 1);
                           end if;

                            BEGIN
                               select data_calendario into w_data_util_3 from basi_260
                               where basi_260.data_calendario <= Last_day(to_date(cpag.data_vencimento, 'DD/MM/YY'))
                                 and basi_260.dia_util_finan   = 0
                                 and rownum                    = 1
                               order by basi_260.data_calendario desc;
                            EXCEPTION
                               when no_data_found then
                                  w_data_util_3 := Last_day(to_date(sysdate, 'DD/MM/YY'));
                            END;

                            cpag.data_vencimento := w_data_util_3;
                        end if;
                     end if;
                  end if;
               end if;
            end if;
         end if;

         BEGIN
            select basi_265.descricao into w_nome_moeda from basi_265
            where basi_265.codigo_moeda = cpag.moeda_titulo;
         EXCEPTION
            when no_data_found then
               w_nome_moeda := ' ';
         END;

         -- CASO ESTIVER PARAMETRIZADO PARA LER OS DADOS DIA DO FLUXO NA TABELA DE CADASTRO
         if w_vencimento_aux = to_date(sysdate, 'DD/MM/YY') and
            p_origem_dados_lh_tp = 2                        and
            w_ja_inseriu                                    = 0
         then
            w_ja_inseriu := 1;
            FOR dados_lh_tp in (
                select * from cpag_825
                where cpag_825.data_fluxo     = p_data_lida
                  and cpag_825.valor          > 0.00
                  and cpag_825.tipo           = 2
                  and ((p_incl_exce = 1
                     and cpag_825.codigo_empresa in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
                   or  (p_incl_exce = 2
                     and cpag_825.codigo_empresa not in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5)))
             )
            LOOP
                w_tipo_registro_flxc := 'pagtos';
                w_letra              := 'C';

                BEGIN
                select rcnb_060.descricao into w_descr_conta_fluxo
                from rcnb_060
                where rcnb_060.tipo_registro  = 128
                  and rcnb_060.nr_solicitacao =  dados_lh_tp.conta_fluxo;
                EXCEPTION
                   when no_data_found then
                      w_descr_conta_fluxo := ' ';
                END;

                BEGIN
                    select
                     nvl(max(oper_flx.sequencia),0) + 1 into w_sequencia from oper_flx
                    where oper_flx.int_01           = p_cod_usuario
                      and oper_flx.int_12           = 1
                      and oper_flx.nr_solicitacao   = p_cod_usuario
                      and oper_flx.nome_relatorio   = p_programa
                      and oper_flx.str_61           = p_nom_usuario;
                END;

                BEGIN
                   INSERT /*+APPEND*/ INTO oper_flx
                       (dat_01,                        int_02,
                        int_03,                        int_04,
                        int_05,                        str_01, --parcela

                        int_06,                        str_92,
                        int_07,                        int_08,
                        int_09,                        str_60, -- w_nome_for

                        int_10,                        str_06,
                        int_11,                        str_07,
                        flo_01,                        flo_02, -- saldo_titulo

                        flo_03,                        str_08,
                        str_62,                        int_13,
                        int_01,                        int_12, -- destalhado

                        int_14,                        int_15,
                        int_16,                        int_17,
                        int_18,

                        str_63,                        dat_02,
                        dat_03,                        dat_04,

                        nr_solicitacao,                nome_relatorio,
                        sequencia,                     data_criacao,
                        str_61)
                    VALUES
                       (dados_lh_tp.data_fluxo,        0,
                        0,                             dados_lh_tp.conta_fluxo,
                        0,                             '0',

                        0,                             w_descr_conta_fluxo,
                        0,                             0,
                        0,                             ' ',

                        0,                             ' ',
                        0,                             ' ',
                        dados_lh_tp.valor,             dados_lh_tp.valor,

                        0.00,                          w_tipo_registro_flxc,
                        w_nome_empr,                   dados_lh_tp.codigo_empresa,
                        p_cod_usuario,                 1, -- destalhado

                        p_cod_emp1,                    p_cod_emp2,
                        p_cod_emp3,                    p_cod_emp4,
                        p_cod_emp5,

                        w_letra,                       p_data_fim_consulta,
                        p_data_ini_consulta,           sysdate,

                        p_cod_usuario,                p_programa,
                        w_sequencia,                   sysdate,
                        p_nom_usuario);
                EXCEPTION
                WHEN OTHERS THEN
                    p_des_erro := 'Erro na atualizacao da tabela oper_flx - CONTA A PAGAR 1 ' || Chr(10) || SQLERRM;
                    RAISE W_ERRO;
                END;
                COMMIT;
            END LOOP;
        end if;

        /* if cpag.data_vencimento > to_date(sysdate, 'DD/MM/YY') or
           (cpag.data_vencimento = to_date(sysdate, 'DD/MM/YY') and p_origem_dados_lh_tp = 1)*/
         if w_vencimento_aux > to_date(sysdate, 'DD/MM/YY') or
           (w_vencimento_aux = to_date(sysdate, 'DD/MM/YY') and p_origem_dados_lh_tp = 1)
         then
              BEGIN
                  select  basi_270.valor_moeda into w_indice_moeda_titulo from basi_270
                  where basi_270.codigo_moeda = cpag.moeda_titulo
                    and basi_270.data_moeda   <= p_data_lida
                    and rownum                 = 1
                  order by basi_270.data_moeda desc;
               EXCEPTION
               when no_data_found then
                    w_indice_moeda_titulo := 1.00;
               END;

               if w_indice_moeda_titulo = 0.00
               then
                  w_indice_moeda_titulo := 1.00;
               end if;
               FOR baixa in (
                select
                       sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_pago * (-1)),cpag_015.valor_pago))+
                       sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_abatido  * (-1)),cpag_015.valor_abatido)) +
                       sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_descontos  * (-1)),cpag_015.valor_descontos)) -
                       sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_juros  * (-1)),cpag_015.valor_juros)) -
                       sum(decode(cont_010.sinal_titulo,2,(cpag_015.vlr_var_cambial * (-1)), cpag_015.vlr_var_cambial)) valor_pago
                from cpag_015,
                     cont_010
                where cpag_015.codigo_historico  = cont_010.codigo_historico
                  and cont_010.fluxo             = 2
                  and cpag_015.dupl_for_nrduppag = cpag.nr_duplicata
                  and cpag_015.dupl_for_no_parc  = cpag.parcela
                  and cpag_015.dupl_for_for_cli9 = cpag.cgc_9
                  and cpag_015.dupl_for_for_cli4 = cpag.cgc_4
                  and cpag_015.dupl_for_for_cli2 = cpag.cgc_2
                  and cpag_015.dupl_for_tipo_tit = cpag.tipo_titulo
                  --and cpag_015.data_pagamento    <= p_data_lida
                  --and cpag.saldo_titulo > 0
                having
                  (sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_pago * (-1)),cpag_015.valor_pago))+
                  sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_abatido  * (-1)),cpag_015.valor_abatido)) +
                  sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_descontos  * (-1)),cpag_015.valor_descontos)) -
                  sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_juros  * (-1)),cpag_015.valor_juros)) -
                  sum(decode(cont_010.sinal_titulo,2,(cpag_015.vlr_var_cambial * (-1)), cpag_015.vlr_var_cambial))) <> 0
                  )
                LOOP
                  if baixa.valor_pago is not null and
                     baixa.valor_pago <> 0
                  then

                       if baixa.valor_pago > 0.00
                       then
                          baixa.valor_pago := round(baixa.valor_pago * w_indice_moeda_titulo,2);
                          if baixa.valor_pago < 0
                          then
                             baixa.valor_pago := 0;
                          end if;
                          cpag.saldo_titulo  := cpag.saldo_titulo + baixa.valor_pago;
                       end if;
                    end if;
                END LOOP;

                if cpag.saldo_titulo > 0
                then
                      BEGIN
                         select
                         nvl(max(oper_flx.sequencia),0) + 1 into w_sequencia from oper_flx
                         where oper_flx.int_01           = p_cod_usuario
                           and oper_flx.int_12           = 1
                           and oper_flx.nr_solicitacao   = p_cod_usuario
                           and oper_flx.nome_relatorio   = p_programa
                           and oper_flx.str_61           = p_nom_usuario;
                      END;

                      BEGIN
                         INSERT /*+APPEND*/ INTO oper_flx
                             (dat_01,                        int_02,
                              int_03,                        int_04,
                              int_05,                        str_01, --parcela

                              int_06,                        str_92,
                              int_07,                        int_08,
                              int_09,                        str_60, -- w_nome_for

                              int_10,                        str_06,
                              int_11,                        str_07,
                              flo_01,                        flo_02, -- saldo_titulo

                              flo_03,                        str_08,
                              str_62,                        int_13,
                              int_01,                        int_12, -- destalhado

                              int_14,                        int_15,
                              int_16,                        int_17,
                              int_18,

                              str_63,                        dat_02,
                              dat_03,                        dat_04,

                              nr_solicitacao,                nome_relatorio,
                              sequencia,                     data_criacao,
                              str_61)
                          VALUES
                             (cpag.data_vencimento,          cpag.previsao,
                              0,                             w_cod_agrupador,
                              cpag.nr_duplicata,             cpag.parcela,

                              cpag.tipo_titulo,              w_descr_tipo_fornec,
                              cpag.cgc_9,                    cpag.cgc_4,
                              cpag.cgc_2,                    w_nome_for ,

                              cpag.cod_portador,             w_nome_portador,
                              cpag.moeda_titulo,             w_nome_moeda,
                              cpag.valor_parcela,            cpag.saldo_titulo,

                              0.00,                          w_tipo_registro_flxc,
                              w_nome_empr,                   cpag.codigo_empresa,
                              p_cod_usuario,                 1, -- destalhado

                              p_cod_emp1,                    p_cod_emp2,
                              p_cod_emp3,                    p_cod_emp4,
                              p_cod_emp5,

                              'C',                           p_data_fim_consulta,
                              p_data_ini_consulta,           w_vencimento_aux,

                              p_cod_usuario,                p_programa,
                              w_sequencia,                   sysdate,
                              p_nom_usuario);
                      EXCEPTION
                      WHEN OTHERS THEN
                          p_des_erro := 'Erro na atualizacao da tabela oper_flx - CONTA A PAGAR 2 ' || Chr(10) || SQLERRM;
                          RAISE W_ERRO;
                      END;
                      COMMIT;
                  end if;
           end if;
         -- DATA LIDA MENOR QUE A LINHA DO TEMPO (DATA EM QUE FOI GERADO O FLUXO DE CAIXA)
         if w_vencimento_aux < to_date(sysdate, 'DD/MM/YY')
         then
              BEGIN
                  select  basi_270.valor_moeda into w_indice_moeda_titulo from basi_270
                  where basi_270.codigo_moeda = cpag.moeda_titulo
                    and basi_270.data_moeda   <= p_data_lida
                    and rownum                 = 1
                  order by basi_270.data_moeda desc;
               EXCEPTION
               when no_data_found then
                    w_indice_moeda_titulo := 1.00;
               END;

               if w_indice_moeda_titulo = 0.00
               then
                  w_indice_moeda_titulo := 1.00;
               end if;
              FOR baixa in (
              select
                     sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_pago * (-1)),cpag_015.valor_pago))+
                     sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_abatido  * (-1)),cpag_015.valor_abatido)) +
                     sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_descontos  * (-1)),cpag_015.valor_descontos)) -
                     sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_juros  * (-1)),cpag_015.valor_juros)) -
                     sum(decode(cont_010.sinal_titulo,2,(cpag_015.vlr_var_cambial * (-1)), cpag_015.vlr_var_cambial)) valor_pago
              from cpag_015,
                   cont_010
              where cpag_015.codigo_historico  = cont_010.codigo_historico
                and cont_010.fluxo             = 2
                and cpag_015.dupl_for_nrduppag = cpag.nr_duplicata
                and cpag_015.dupl_for_no_parc  = cpag.parcela
                and cpag_015.dupl_for_for_cli9 = cpag.cgc_9
                and cpag_015.dupl_for_for_cli4 = cpag.cgc_4
                and cpag_015.dupl_for_for_cli2 = cpag.cgc_2
                and cpag_015.dupl_for_tipo_tit = cpag.tipo_titulo
                --and cpag_015.data_pagamento    = p_data_lida
                --and cpag.saldo_titulo > 0
              having
                 (sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_pago * (-1)),cpag_015.valor_pago))+
                 sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_abatido  * (-1)),cpag_015.valor_abatido)) +
                 sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_descontos  * (-1)),cpag_015.valor_descontos)) -
                 sum(decode(cont_010.sinal_titulo,2,(cpag_015.valor_juros  * (-1)),cpag_015.valor_juros)) -
                 sum(decode(cont_010.sinal_titulo,2,(cpag_015.vlr_var_cambial * (-1)), cpag_015.vlr_var_cambial))) <> 0
                )
              LOOP
                if baixa.valor_pago is not null and
                   baixa.valor_pago <> 0
                then

                     if baixa.valor_pago > 0.00
                     then
                         baixa.valor_pago := round(baixa.valor_pago * w_indice_moeda_titulo,2);
                         if baixa.valor_pago < 0
                         then
                            baixa.valor_pago := 0;
                         end if;
                         cpag.saldo_titulo := cpag.saldo_titulo + baixa.valor_pago;
                     end if;
                 end if;
             END LOOP;


             if cpag.saldo_titulo > 0.00
             then
                 BEGIN
                    select
                     nvl(max(oper_flx.sequencia),0) + 1 into w_sequencia from oper_flx
                    where oper_flx.int_01           = p_cod_usuario
                      and oper_flx.int_12           = 1
                      and oper_flx.nr_solicitacao   = p_cod_usuario
                      and oper_flx.nome_relatorio   = p_programa
                      and oper_flx.str_61           = p_nom_usuario;
                 EXCEPTION
                    when no_data_found then
                        w_sequencia := 0;
                 END;

                 BEGIN
                      INSERT /*+APPEND*/ INTO oper_flx
                         (dat_01,                        int_02,
                          int_03,                        int_04,
                          int_05,                        str_01, --parcela

                          int_06,                        str_92,
                          int_07,                        int_08,
                          int_09,                        str_60, -- w_nome_for

                          int_10,                        str_06,
                          int_11,                        str_07,
                          flo_01,                        flo_02, -- saldo_titulo

                          flo_03,                        str_08,
                          str_62,                        int_13,
                          int_01,                        int_12, -- destalhado

                          int_14,                        int_15,
                          int_16,                        int_17,
                          int_18,

                          str_63,                        dat_02,
                          dat_03,                        dat_04,

                          nr_solicitacao,                nome_relatorio,
                          sequencia,                     data_criacao,
                          str_61)
                      VALUES
                         (cpag.data_vencimento,          cpag.previsao,
                          0,                             w_cod_agrupador,
                          cpag.nr_duplicata,             cpag.parcela,

                          cpag.tipo_titulo,              w_descr_tipo_fornec,
                          cpag.cgc_9,                    cpag.cgc_4,
                          cpag.cgc_2,                    w_nome_for ,

                          cpag.cod_portador,             w_nome_portador,
                          cpag.moeda_titulo,             w_nome_moeda,
                          cpag.valor_parcela,            cpag.saldo_titulo,

                          0.00,                          w_tipo_registro_flxc,
                          w_nome_empr,                   cpag.codigo_empresa,
                          p_cod_usuario,                 1, -- destalhado

                          p_cod_emp1,                    p_cod_emp2,
                          p_cod_emp3,                    p_cod_emp4,
                          p_cod_emp5,

                          'C',                           p_data_fim_consulta,
                          p_data_ini_consulta,           w_vencimento_aux,

                          p_cod_usuario,                p_programa,
                          w_sequencia,                   sysdate,
                          p_nom_usuario);
                 EXCEPTION
                 WHEN OTHERS THEN
                     p_des_erro := 'Erro na atualizacao da tabela oper_flx - CONTA A PAGAR 3 ' || Chr(10) || SQLERRM;
                     RAISE W_ERRO;
                 END;
                 COMMIT;
             end if;
          end if;
      end if;   /*NC*/
   END LOOP; /* fim.do.executing_begin.do.CPAG_010 */

   if w_achou_registro is null and p_origem_dados_lh_tp = 2 /*and p_data_lida = to_date(sysdate, 'DD/MM/YY')*/ -- Caso nao ache nenhum registro, procura na tabela para o dia atual
   then
      FOR dados_lh_tp in (
        select  * from cpag_825
        where cpag_825.data_fluxo     = to_date(sysdate, 'DD/MM/YY')
          and cpag_825.valor          > 0.00
          and cpag_825.tipo           = 2
          and p_data_lida             = to_date(sysdate, 'DD/MM/YY')
          and ((p_incl_exce = 1
                and cpag_825.codigo_empresa in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
           or  (p_incl_exce = 2
                and cpag_825.codigo_empresa not in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5)))
      )
      LOOP
        w_tipo_registro_flxc := 'pagtos';
        w_letra              := 'C';

        BEGIN
        select rcnb_060.descricao into w_descr_conta_fluxo
        from rcnb_060
        where rcnb_060.tipo_registro  = 128
          and rcnb_060.nr_solicitacao =  dados_lh_tp.conta_fluxo;
        EXCEPTION
           when no_data_found then
              w_descr_conta_fluxo := ' ';
        END;

        BEGIN
            select
             nvl(max(oper_flx.sequencia),0) + 1 into w_sequencia from oper_flx
            where oper_flx.int_01           = p_cod_usuario
              and oper_flx.int_12           = 1
              and oper_flx.nr_solicitacao   = p_cod_usuario
              and oper_flx.nome_relatorio   = p_programa
              and oper_flx.str_61           = p_nom_usuario;
        END;

        BEGIN
           INSERT /*+APPEND*/ INTO oper_flx
               (dat_01,                        int_02,
                int_03,                        int_04,
                int_05,                        str_01, --parcela

                int_06,                        str_92,
                int_07,                        int_08,
                int_09,                        str_60, -- w_nome_for

                int_10,                        str_06,
                int_11,                        str_07,
                flo_01,                        flo_02, -- saldo_titulo

                flo_03,                        str_08,
                str_62,                        int_13,
                int_01,                        int_12, -- destalhado

                int_14,                        int_15,
                int_16,                        int_17,
                int_18,

                str_63,                        dat_02,
                dat_03,                        dat_04,

                nr_solicitacao,                nome_relatorio,
                sequencia,                     data_criacao,
                str_61)
            VALUES
               (dados_lh_tp.data_fluxo,        0,
                0,                             dados_lh_tp.conta_fluxo,
                0,                             '0',

                0,                             w_descr_conta_fluxo,
                0,                             0,
                0,                             ' ',

                0,                             ' ',
                0,                             ' ',
                dados_lh_tp.valor,             dados_lh_tp.valor,

                0.00,                          w_tipo_registro_flxc,
                w_nome_empr,                   dados_lh_tp.codigo_empresa,
                p_cod_usuario,                 1, -- destalhado

                p_cod_emp1,                    p_cod_emp2,
                p_cod_emp3,                    p_cod_emp4,
                p_cod_emp5,

                w_letra,                       p_data_fim_consulta,
                p_data_ini_consulta,           sysdate,

                p_cod_usuario,                p_programa,
                w_sequencia,                   sysdate,
                p_nom_usuario);
        EXCEPTION
        WHEN OTHERS THEN
            p_des_erro := 'Erro na atualizacao da tabela oper_flx - CONTA A PAGAR 1 ' || Chr(10) || SQLERRM;
            RAISE W_ERRO;
        END;
        COMMIT;
      END LOOP;
   end if;

EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_flx_crec ' || Chr(10) || p_des_erro;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure inter_pr_flx_cpag ' || Chr(10) || SQLERRM;

END inter_pr_flx_cpag;
 

/

exec inter_pr_recompile;

