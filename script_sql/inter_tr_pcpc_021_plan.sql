
CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_021_PLAN" 
   before insert or
          delete or
          update of quantidade
   on pcpc_021
   for each row

declare
   v_ordem_producao          number(9);
   v_referencia_op           varchar2(5);
   v_tamanho                 varchar2(3);
   v_cor                     varchar2(6);
   v_pedido_venda            number(9);
   v_qtde_update             number(6);
   v_nr_registro_630         number(9);
   v_alternativa_produto     number(2);
   v_saldo_prog              number(9);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   v_executa_trigger         number;


begin
   -- Inicializa��o das vari�veis
   v_pedido_venda        := 0;
   v_ordem_producao      := 0;
   v_qtde_update         := 0;
   v_alternativa_produto := 0;
   v_tamanho             := '';
   v_cor                 := '';

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting or updating
   then
      v_ordem_producao  := :new.ordem_producao;
      v_tamanho         := :new.tamanho;
      v_cor             := :new.sortimento;
      v_nr_registro_630 := 0;

      v_executa_trigger := :new.executa_trigger;

      if inserting
      then
         v_saldo_prog      := :new.quantidade;
      else
         v_saldo_prog      := :new.quantidade - :old.quantidade;
      end if;
   else
      v_ordem_producao  := :old.ordem_producao;
      v_tamanho         := :old.tamanho;
      v_cor             := :old.sortimento;
      v_saldo_prog      := :old.quantidade;

      v_executa_trigger := :old.executa_trigger;
   end if;

   if v_executa_trigger is null
   then
      v_executa_trigger := 0;
   end if;

   if v_executa_trigger <> 1
   then

      -- Obt�m o pedido de venda ao qual a ordem � desti-
      -- nada e a derivada ordem de planejamento. A prio-
      -- ridade de procura � a capa da ordem de corte.
      begin
         select pcpc_020.pedido_venda,  pcpc_020.referencia_peca, pcpc_020.alternativa_peca
         into   v_pedido_venda,         v_referencia_op,          v_alternativa_produto
         from pcpc_020
         where pcpc_020.ordem_producao = v_ordem_producao;
         exception when no_data_found then
            v_pedido_venda        := 0;
            v_referencia_op       := '';
            v_alternativa_produto := 0;
      end;

      -- Se a capa da ordem de corte n�o contiver pedido,
      -- verifica  se  a  ordem j� n�o est�  associada ao
      -- planejamento (tmrp_630). Isso pode ocorrer quan-
      -- do, por exemplo, a ordem tiver sido gerada  pelo
      -- painel de planejamento de Corte (tmrp_f007), que
      -- n�o informa o pedido na capa da ordem de corte.
      if v_pedido_venda = 0
      and 1=2
      then
         begin
            select tmrp_630.pedido_venda,  pcpc_020.referencia_peca
            into   v_pedido_venda,         v_referencia_op
            from pcpc_020, tmrp_630
            where pcpc_020.ordem_producao = tmrp_630.ordem_prod_compra
              and pcpc_020.ordem_producao = v_ordem_producao
              and tmrp_630.area_producao  = 1
              and rownum                  = 1;
            exception when no_data_found then
               v_pedido_venda  := 0;
               v_referencia_op := '';
         end;
      end if;

      -- Tendo obtido o pedido de venda, busca a ordem de
      -- planejamento.
      if v_pedido_venda > 0
      then
         for reg_ordem_plan in (select tmrp_625.ordem_planejamento, tmrp_625.pedido_venda
                                from tmrp_625
                                where tmrp_625.pedido_venda            = v_pedido_venda
                                  and tmrp_625.nivel_produto           = '1'
                                  and tmrp_625.grupo_produto           = v_referencia_op
                                  and tmrp_625.subgrupo_produto        = v_tamanho
                                  and tmrp_625.item_produto            = v_cor
                                  and tmrp_625.alternativa_produto     = v_alternativa_produto
                                  and tmrp_625.nivel_produto_origem    = '0'
                                group by tmrp_625.ordem_planejamento, tmrp_625.pedido_venda)
         loop
            begin
               insert into tmrp_615
                 (nome_programa,                            area_producao,
                  nr_solicitacao,                           tipo_registro,
                  cgc_cliente9,                             alternativa,
                  nivel,                                    grupo,
                  subgrupo,                                 item,
                  ordem_planejamento,                       pedido_venda)
               values
                 ('trigger_planejamento',                   1,
                  888,                                      888,
                  ws_sid,                                   0,
                  '0',                                      '00000',
                  '000',                                    '000000',
                  reg_ordem_plan.ordem_planejamento,        reg_ordem_plan.pedido_venda);

               exception when others
               then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end loop;
      end if;
   end if;

   if v_executa_trigger <> 1
   then
      if v_pedido_venda > 0
      then
         for reg_ordens_planej in (select tmrp_625.ordem_planejamento,
                                          decode(sign(nvl(sum(tmrp_625.qtde_reserva_planejada),0) - nvl(sum(tmrp_625.qtde_reserva_programada),0)),
                                                 1,
                                                 nvl(sum(tmrp_625.qtde_reserva_planejada),0),
                                                 nvl(sum(tmrp_625.qtde_reserva_programada),0)) - nvl(sum(tmrp_625.qtde_areceber_programada),0) qtde_necessaria
                                   from tmrp_625
                                   where tmrp_625.pedido_venda            = v_pedido_venda
                                     and tmrp_625.nivel_produto           = '1'
                                     and tmrp_625.grupo_produto           = v_referencia_op
                                     and tmrp_625.subgrupo_produto        = v_tamanho
                                     and tmrp_625.item_produto            = v_cor
                                     and tmrp_625.alternativa_produto     = v_alternativa_produto
                                     and tmrp_625.nivel_produto_origem    = '0'
                                   group by tmrp_625.ordem_planejamento)
         loop

            v_qtde_update := v_saldo_prog;

            begin
               select nvl(count(*),0)
               into   v_nr_registro_630
               from tmrp_630
               where tmrp_630.ordem_planejamento = reg_ordens_planej.ordem_planejamento
                 and tmrp_630.area_producao      = 1 -- Confec��o
                 and tmrp_630.ordem_prod_compra  = v_ordem_producao
                 and tmrp_630.nivel_produto      = '1'
                 and tmrp_630.grupo_produto      = v_referencia_op
                 and tmrp_630.subgrupo_produto   = v_tamanho
                 and tmrp_630.item_produto       = v_cor
                 and tmrp_630.pedido_venda       = v_pedido_venda;
               exception when others then
                  v_nr_registro_630 := 0;
            end;

            if inserting and v_qtde_update > 0
            then
               if v_nr_registro_630 = 0
               then
                  begin
                     insert into tmrp_630
                       (ordem_planejamento,                   area_producao,     ordem_prod_compra,
                        nivel_produto,                        grupo_produto,     subgrupo_produto,
                        item_produto,                         pedido_venda,      programado_comprado,
                        alternativa_produto)
                     values
                       (reg_ordens_planej.ordem_planejamento, 1,                 v_ordem_producao,
                        '1',                                  v_referencia_op,   v_tamanho,
                        v_cor,                                v_pedido_venda,    v_qtde_update,
                        v_alternativa_produto);
                     exception when others
                     then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               else
                  begin
                     update tmrp_630
                     set programado_comprado = programado_comprado + v_qtde_update
                     where tmrp_630.ordem_planejamento = reg_ordens_planej.ordem_planejamento
                       and tmrp_630.pedido_venda       = v_pedido_venda
                       and tmrp_630.ordem_prod_compra  = v_ordem_producao
                       and tmrp_630.area_producao      = 1
                       and tmrp_630.nivel_produto      = '1'
                       and tmrp_630.grupo_produto      = v_referencia_op
                       and tmrp_630.subgrupo_produto   = v_tamanho
                       and tmrp_630.item_produto       = v_cor;
                     exception when others
                     then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               end if;
            elsif updating
            then

               if v_nr_registro_630 = 0
               then
                   if v_qtde_update > 0
                   then
                      begin
                          insert into tmrp_630
                          (ordem_planejamento,                   area_producao,     ordem_prod_compra,
                           nivel_produto,                        grupo_produto,     subgrupo_produto,
                           item_produto,                         pedido_venda,      programado_comprado,
                           alternativa_produto)
                          values
                          (reg_ordens_planej.ordem_planejamento, 1,                 v_ordem_producao,
                           '1',                                  v_referencia_op,   v_tamanho,
                           v_cor,                                v_pedido_venda,    v_qtde_update,
                           v_alternativa_produto);
                         exception when others
                         then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                      end;
                   end if;
               elsif v_qtde_update <> 0
               then
                  begin
                     update tmrp_630
                     set programado_comprado = programado_comprado + v_qtde_update
                     where tmrp_630.ordem_planejamento  = reg_ordens_planej.ordem_planejamento
                       and tmrp_630.pedido_venda        = v_pedido_venda
                       and tmrp_630.ordem_prod_compra   = v_ordem_producao
                       and tmrp_630.area_producao       = 1
                       and tmrp_630.nivel_produto       = '1'
                       and tmrp_630.grupo_produto       = v_referencia_op
                       and tmrp_630.subgrupo_produto    = v_tamanho
                       and tmrp_630.item_produto        = v_cor
                       and tmrp_630.alternativa_produto = v_alternativa_produto;
                     exception when others
                     then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               end if;
            elsif deleting
            then
               if v_nr_registro_630 > 0
               then
                  begin
                     delete tmrp_630
                     where tmrp_630.ordem_planejamento  = reg_ordens_planej.ordem_planejamento
                       and tmrp_630.pedido_venda        = v_pedido_venda
                       and tmrp_630.ordem_prod_compra   = v_ordem_producao
                       and tmrp_630.area_producao       = 1
                       and tmrp_630.nivel_produto       = '1'
                       and tmrp_630.grupo_produto       = v_referencia_op
                       and tmrp_630.subgrupo_produto    = v_tamanho
                       and tmrp_630.item_produto        = v_cor
                       and tmrp_630.alternativa_produto = v_alternativa_produto;
                     exception when others
                     then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               end if;
            end if;
         end loop;
      end if;
   else
      if inserting or updating
      then
         :new.executa_trigger := 0;
      end if;
   end if;
end inter_tr_pcpc_021_plan;

-- ALTER TRIGGER "INTER_TR_PCPC_021_PLAN" ENABLE
 

/

exec inter_pr_recompile;

