create or replace PROCEDURE pr_gera_log_param_error(
    p_param             in varchar2,
    p_processo          in varchar2,
    p_mensagem          in varchar2,
    p_tipo_alerta       in number,
    p_cod_empresa       in number,
    p_usuario           in varchar2
) is
    v_cod_usuario number;
    v_desc_param varchar2(255);
begin
    select CODIGO_USUARIO
        into v_cod_usuario
    from HDOC_030
    where USUARIO = p_usuario
    and EMPRESA = p_cod_empresa;

    SELECT DEFAULT_STR
        into v_desc_param
    from EMPR_007
    where param = p_param;

    insert into empr_015 (
        param               , tipo_alerta
        , processo          , codigo_empresa
        , codigo_usuario    , nome_usuario
        , mensagem          , DATA_OCORRIDO
        , desc_param
    ) values (
        p_param         , p_tipo_alerta
        , p_processo    , p_cod_empresa
        , v_cod_usuario , p_usuario
        , p_mensagem    , sysdate
        , v_desc_param
    );
end pr_gera_log_param_error;
