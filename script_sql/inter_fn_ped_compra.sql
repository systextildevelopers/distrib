
  CREATE OR REPLACE FUNCTION "INTER_FN_PED_COMPRA" 
(         pnivel         varchar2,
          pgrupo         varchar2,
          psubgrupo      varchar2,
          pitem          varchar2)
return number is ppedido number;
begin
   begin
      select supr100.num_ped_compra into ppedido
      from (
         select supr_100.num_ped_compra 
         from supr_100, supr_110
         where supr_100.item_100_nivel99  = pnivel
         and   supr_100.item_100_grupo    = pgrupo
         and   supr_100.item_100_subgrupo = psubgrupo
         and   supr_100.item_100_item     = pitem
         and   supr_100.num_ped_compra    = supr_110.ch_100_num_ped (+)
         and   supr_100.seq_item_pedido   = supr_110.ch_100_seq_ped (+)
         and  (supr_110.data_entrega > sysdate - 2000 or supr_110.data_entrega is null)
         and  (supr_100.cod_cancelamento = 0 or supr_100.cod_cancelamento is null)
         and  (supr_100.situacao_item  <> 1 or supr_100.situacao_item is null)
         order by supr_110.data_entrega desc) supr100
      where rownum = 1; 
 
      exception
         when others then
            ppedido := 9999999;
   end;
 
   return(ppedido);
end inter_fn_ped_compra;

 

/

exec inter_pr_recompile;

