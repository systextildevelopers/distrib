CREATE OR REPLACE TYPE BigStringConcatST 
AS OBJECT
(
    runningStr      CLOB,
    
    STATIC FUNCTION ODCIAggregateInitialize (actx IN OUT BigStringConcatST)
        RETURN NUMBER,
        
    MEMBER FUNCTION ODCIAggregateIterate (
                                            self   IN OUT BigStringConcatST,
                                            value  IN     VARCHAR2)
        RETURN NUMBER,
        
    MEMBER FUNCTION ODCIAggregateTerminate (
                                            self          IN     BigStringConcatST,
                                            returnValue   OUT    CLOB,
                                            flags         IN     NUMBER)
        RETURN NUMBER,
        
    MEMBER FUNCTION ODCIAggregateMerge (self   IN OUT BigStringConcatST,
                                        ctx2   IN     BigStringConcatST)
        RETURN NUMBER
);
/

CREATE OR REPLACE TYPE BODY BigStringConcatST 
AS     
    STATIC FUNCTION ODCIAggregateInitialize (actx IN OUT BigStringConcatST)
        RETURN NUMBER
    IS 
    BEGIN
        actx := BigStringConcatST(EMPTY_CLOB());
        RETURN ODCIConst.Success;
    END;
        
    MEMBER FUNCTION ODCIAggregateIterate (
                                            self   IN OUT BigStringConcatST,
                                            value  IN     VARCHAR2)
        RETURN NUMBER
    IS
    BEGIN
        self.runningStr := self.runningStr || value || ',';
        RETURN ODCIConst.Success;
    END;    
        
    MEMBER FUNCTION ODCIAggregateTerminate (
                                            self          IN     BigStringConcatST,
                                            returnValue   OUT    CLOB,
                                            flags         IN     NUMBER)
        RETURN NUMBER
    IS
    BEGIN
        returnValue := RTRIM(self.runningStr, ',');
        RETURN ODCIConst.Success;
    END;
        
    MEMBER FUNCTION ODCIAggregateMerge (self   IN OUT BigStringConcatST,
                                        ctx2   IN     BigStringConcatST)
        RETURN NUMBER
    IS
    BEGIN
        self.runningStr := self.runningStr || ctx2.runningStr;
        RETURN ODCIConst.Success;
    END;


END;
/


CREATE FUNCTION wm_concat_st (INPUT VARCHAR2) RETURN CLOB
PARALLEL_ENABLE AGGREGATE USING BigStringConcatST;
