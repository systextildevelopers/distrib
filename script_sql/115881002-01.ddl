alter table obrf_158 add
resp_tec_cgc9 number(9);

alter table obrf_158 add
resp_tec_cgc4 number(9);

alter table obrf_158 add
resp_tec_cgc2 number(9);

alter table obrf_158 add
resp_tec_nome varchar2(40);

alter table obrf_158 add
resp_tec_email varchar2(40);

alter table obrf_158 add
resp_tec_telefone number(9);

alter table obrf_158 add
resp_tec_idCSRT number(2);

alter table obrf_158 add
resp_tec_hashCSRT varchar2(28);

comment on column obrf_158.resp_tec_cgc9 is 'CNPJ da pessoa jurídica responsável pelo sistema utilizado na emissão do documento fiscal eletrônico';
comment on column obrf_158.resp_tec_nome is 'Nome da pessoa a ser contatada';
comment on column obrf_158.resp_tec_email is 'E-mail da pessoa jurídica a ser contatada';
comment on column obrf_158.resp_tec_telefone is 'Telefone da pessoa jurídica/física a ser contatada';
comment on column obrf_158.resp_tec_idCSRT is 'Identificador do CSRT utilizado para montar o hash do CSRT';
comment on column obrf_158.resp_tec_hashCSRT is 'O hashCSRT é o resultado da função hash (SHA-1 – Base64) do CSRT fornecido pelo fisco mais a Chave de Acesso da NFe';
