create or replace trigger inter_tr_pcpc_601_log
   after insert or delete or update
       of codigo_familia,     ano,
          mes,                valor_minuto
   on pcpc_601
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_desc_cod_familia       varchar2(20);
   
   long_aux                  varchar2(2000);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      begin
         select basi_180.descricao
         into ws_desc_cod_familia
         from basi_180
         where basi_180.divisao_producao = :new.codigo_familia;
      exception when no_data_found then
         ws_desc_cod_familia  := ' ';
      end;

      begin
         INSERT INTO hist_100 (
            tabela,             operacao,
            data_ocorr,         aplicacao,
            usuario_rede,       maquina_rede,
            str01,              num05,
            long01
         ) VALUES (
            'pcpc_601',         'I',
            sysdate,            ws_aplicativo,
            ws_usuario_rede,    ws_maquina_rede,
            '',                 :new.codigo_familia,
             '               '||
             inter_fn_buscar_tag('lb44202#REGRAS PARA PR�MIOS',ws_locale_usuario,ws_usuario_systextil) ||
                                     chr(10)               ||
                                     chr(10)               ||
            inter_fn_buscar_tag('lb02633#C�DIGO FAM�LIA/C�LULA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.codigo_familia    || ' - ' 
                                 || ws_desc_cod_familia    ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb00782#M�S/ANO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || to_char(:new.mes,'00')     || '/' ||
                                    to_char(:new.ano,'0000')   ||
            chr(10)                                        ||
            inter_fn_buscar_tag('lb37675#PR�MIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                 || :new.valor_minuto   ||
            chr(10)
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENC?O! N?o inseriu {0}. Status: {1}.', 'HIST_100(001-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   if updating
   then
      long_aux := long_aux ||
                 '                 '||
                 inter_fn_buscar_tag('lb44202#REGRAS PARA PR�MIOS',ws_locale_usuario,ws_usuario_systextil) ||
                 chr(10)            ||
                 chr(10)            ||
                 inter_fn_buscar_tag('lb02633#C�DIGO FAM�LIA/C�LULA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                    || :new.codigo_familia    || ' - ' 
                                    || ws_desc_cod_familia    ||
                 chr(10)                                        ||
                 inter_fn_buscar_tag('lb00782#M�S/ANO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                    || to_char(:new.mes,'00')     || '/' ||
                                    to_char(:new.ano,'0000')   ||
                 chr(10);

      if  :old.valor_minuto <> :new.valor_minuto
      then
          long_aux := long_aux ||
                      inter_fn_buscar_tag('lb37675#PR�MIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                    || :old.valor_minuto   || ' -> '
                                    || :new.valor_minuto
                                    || chr(10);
      end if;
      
      begin
         INSERT INTO hist_100 (
            tabela,               operacao,
            data_ocorr,           aplicacao,
            usuario_rede,         maquina_rede,
            str01,                num05,
            long01
         ) VALUES (
           'pcpc_601',           'A',
            sysdate,              ws_aplicativo,
            ws_usuario_rede,      ws_maquina_rede,
            '',                   :new.codigo_familia,
            long_aux
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENC?O! N?o inseriu {0}. Status: {1}.', 'HIST_100(001-2)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));

      end;
   end if;

   if deleting
   then
      begin
         INSERT INTO hist_100 (
            tabela,             operacao,
            data_ocorr,         aplicacao,
            usuario_rede,       maquina_rede,
            str01,              num05,
            long01
         ) VALUES (
            'pcpc_601',         'O',
            sysdate,            ws_aplicativo,
            ws_usuario_rede,    ws_maquina_rede,
            '',                 :old.codigo_familia,
             '               '||
            inter_fn_buscar_tag('lb37675#PR�MIO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                     chr(10)               ||
                                     chr(10)               ||
            inter_fn_buscar_tag('lb02633#C�DIGO FAM�LIA/C�LULA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                    || :old.codigo_familia    || ' - ' 
                                    || ws_desc_cod_familia    ||
                                    chr(10)                   ||
            inter_fn_buscar_tag('lb00782#M�S/ANO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                    || to_char(:old.mes,'00')     || '/' ||
                                    to_char(:old.ano,'0000')   ||
                                    chr(10)                    ||
            inter_fn_buscar_tag('lb37675#PR�MIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                    || :old.valor_minuto
                                    || chr(10)
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENC?O! N?o inseriu {0}. Status: {1}.', 'HIST_100(001-3)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;
end inter_tr_pcpc_601_log;

/

execute inter_pr_recompile;
