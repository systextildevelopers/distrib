create or replace procedure inter_pr_gera_auxiliar_spedctb (p_cod_empresa   IN  NUMBER,
                                                            p_exercicio     IN  NUMBER,
                                                            p_situacao_esp  IN  NUMBER,
                                                            p_per_inicial   IN  NUMBER,
                                                            p_per_final     IN  NUMBER,
                                                            p_mensal_anual  IN  NUMBER,
                                                            p_layout_arq    IN  NUMBER,
                                                            p_des_erro      OUT varchar2) is

   v_erro           EXCEPTION;
   v_des_erro       VARCHAR2(2000);
   v_cod_plano_cta  NUMBER(4);
   v_per_final      NUMBER(2);
   v_tem_expurgo    NUMBER(1);
   p_data_inicial   date;
   p_data_final     date;
   v_data_inicial_exec date;
   v_executa_proced boolean;
   v_form_apur     cont_003.form_apur%type;
   v_form_apur_exercicio varchar2(1);
   tp_apuracao_dre number(1);
   indAcumulado number(1);
   vIndUsaCcDre varchar2(1);

BEGIN

   v_des_erro := NULL;

   -- Faz a limpeza das tabelas auxiliares existentes
   inter_pr_exclui_aux_spedctb (p_cod_empresa, p_exercicio, v_des_erro);

   IF  v_des_erro IS NOT NULL
   THEN
       RAISE v_erro;
   END IF;
   COMMIT;
   
   begin
      select ind_usa_cc_dre INTO vIndUsaCcDre from empr_002;
   end;
   
   begin
   select cont_003.form_apur into v_form_apur   from cont_003
   where cont_003.cod_empresa = p_cod_empresa
     and cont_003.exercicio   = p_exercicio;
   exception
       when no_data_found then
          v_form_apur  := 'A';
       when others then
          p_des_erro := 'Erro na leitura na leitura de parametros ' || Chr(10) || SQLERRM;
       RAISE V_ERRO;
    end;

    -- le o exercicio para buscar o periodo de busca dos movimentos
    begin
       select cont_500.form_apur
       into   v_form_apur_exercicio
       from cont_500
       where cod_empresa = p_cod_empresa
         and   exercicio   = p_exercicio;

    exception
       when others then
          p_des_erro := 'Erro na leitura do exercicio contabil ' || Chr(10) || SQLERRM;
       RAISE V_ERRO;
    end;

    if p_layout_arq = 0 -- SPED CONT�?BIL
    then
       v_form_apur  := v_form_apur_exercicio;
    end if;

    if v_form_apur_exercicio = 'A'
    then
       tp_apuracao_dre := 1;
    else
       tp_apuracao_dre := 2;
    end if;

    -- verifica se o periodo passado e igual ao ultimo periodo
    -- pois tem procedures que deve ser executada somente no ultimo periodo
    -- p_mensal_anual = 3 - Trimestral.
	
	begin
       select cont_510.periodo 
       into  v_per_final
       from cont_510
       where cont_510.cod_empresa = p_cod_empresa
       and   cont_510.exercicio   = p_exercicio
       
       and   cont_510.per_final = (select max(cont510.per_final) from cont_510 cont510
                                   where cont510.cod_empresa = p_cod_empresa
                                   and   cont510.exercicio   = p_exercicio);

    exception
       when others then
          p_des_erro := 'Erro na leitura do per�odo contabil ' || Chr(10) || SQLERRM;
          RAISE V_ERRO;
    end;

    if v_per_final = p_per_final or p_mensal_anual = 2 or p_mensal_anual = 3
    then
       v_executa_proced := true;
    else
       v_executa_proced := false;
    end if;

   -- Gera Informações da empresa (SPED_0000)

	begin
       select cont_510.per_inicial 
       into   p_data_inicial
       from cont_510
       where cont_510.cod_empresa = p_cod_empresa
       and   cont_510.exercicio   = p_exercicio
       and   cont_510.periodo     = p_per_inicial;

       select cont_510.per_final 
       into   p_data_final
       from cont_510
       where cont_510.cod_empresa = p_cod_empresa
       and   cont_510.exercicio   = p_exercicio
       and   cont_510.periodo     = p_per_final;

    exception
       when others then
          p_des_erro := 'Erro na leitura do per�odo contabil ' || Chr(10) || SQLERRM;
          RAISE V_ERRO;
    end;


   inter_pr_gera_ctb_sped_0000 (p_cod_empresa, p_exercicio, p_situacao_esp, v_cod_plano_cta,p_data_inicial, p_data_final, v_des_erro);
   IF  v_des_erro IS NOT NULL
   THEN
       RAISE v_erro;
   END IF;


   -- Gera Informações dos termos de abertura dos livros (SPED_I012)
   inter_pr_gera_ctb_sped_I012 (p_cod_empresa, p_exercicio, v_cod_plano_cta, v_des_erro);
   IF  v_des_erro IS NOT NULL
   THEN
       RAISE v_erro;
   END IF;


   -- Gera Informações Saldos por periodos (SPED_I050)
   inter_pr_gera_ctb_sped_I050 (p_cod_empresa, p_exercicio, v_cod_plano_cta, v_des_erro);
   IF  v_des_erro IS NOT NULL
   THEN
       RAISE v_erro;
   END IF;


   -- Gera Informações Saldos por periodos (SPED_I155)
   inter_pr_gera_ctb_sped_I155 (p_cod_empresa, p_exercicio, v_cod_plano_cta, p_per_inicial, p_per_final, v_des_erro);
   IF  v_des_erro IS NOT NULL
   THEN
       RAISE v_erro;
   END IF;


   -- Gera Informações Lançamentos Contabeis (SPED_I200 / SPED_I250)
   if p_layout_arq <> 2
   then
      v_tem_expurgo := 1;
   else
      v_tem_expurgo := 0;

      begin
         select 1 into v_tem_expurgo
         from   cont_600, cont_535, cont_050
         where  cont_600.cod_empresa    = p_cod_empresa
         and    cont_600.exercicio      = p_exercicio
         and    cont_600.periodo        between p_per_inicial and p_per_final
         and    cont_535.cod_plano_cta  = v_cod_plano_cta
         and    cont_535.conta_contabil = cont_600.conta_contabil
         and    cont_535.subconta       = cont_600.subconta
         and    cont_600.origem         = cont_050.origem
         and    cont_050.ajuste         > 0
         and    rownum                  = 1;
      exception
         when no_data_found then
            v_tem_expurgo  := 0;
      end;

   end if;

   if v_tem_expurgo = 1
   then
      inter_pr_gera_ctb_sped_I200 (p_cod_empresa, p_exercicio, v_cod_plano_cta,p_per_inicial, p_per_final,p_layout_arq, v_des_erro);
      IF  v_des_erro IS NOT NULL
      THEN
         RAISE v_erro;
      END IF;
   end if;

   -- Gera Informações de fornecedores (SPED_0150)
   inter_pr_gera_ctb_sped_0150 (p_cod_empresa, p_exercicio, 1, v_des_erro);
   IF  v_des_erro IS NOT NULL
   THEN
       RAISE v_erro;
   END IF;


   -- Gera Informações de Saldos das Contas de Resultado Antes do Zeramento (SPED_I355)
   if v_executa_proced  -- executa anual ou somente no ultimo periodo
   then
      inter_pr_gera_ctb_sped_I355 (p_cod_empresa, p_exercicio,v_cod_plano_cta,v_form_apur,v_des_erro);
      IF  v_des_erro IS NOT NULL
      THEN
          RAISE v_erro;
      END IF;
    
    if vIndUsaCcDre = 'S' 
    then
       indAcumulado := 1;
    else
       indAcumulado := 2;
    end if;

      for per in (select decode(v_form_apur, 'T',periodo_tri, periodo_anual) periodo_tri,periodo_anual from (
          select distinct decode(v_form_apur,'T', decode(cont_510.periodo,1,'1',
                       decode(cont_510.periodo,2,'1',
                       decode(cont_510.periodo,3,'1',
                       decode(cont_510.periodo,4,'2',
                       decode(cont_510.periodo,5,'2',
                       decode(cont_510.periodo,6,'2',
                       decode(cont_510.periodo,7,'3',
                       decode(cont_510.periodo,8,'3',
                       decode(cont_510.periodo,9,'3',
                       decode(cont_510.periodo,10,'4',
                       decode(cont_510.periodo,11,'4',
                       decode(cont_510.periodo,12,'4')))))))))))),1) periodo_tri ,max(cont_510.periodo) periodo_anual

          from cont_510
          where cont_510.cod_empresa = p_cod_empresa
          and   cont_510.exercicio   = p_exercicio
          group by decode(v_form_apur,'T', decode(cont_510.periodo,1,'1',
                       decode(cont_510.periodo,2,'1',
                       decode(cont_510.periodo,3,'1',
                       decode(cont_510.periodo,4,'2',
                       decode(cont_510.periodo,5,'2',
                       decode(cont_510.periodo,6,'2',
                       decode(cont_510.periodo,7,'3',
                       decode(cont_510.periodo,8,'3',
                       decode(cont_510.periodo,9,'3',
                       decode(cont_510.periodo,10,'4',
                       decode(cont_510.periodo,11,'4',
                       decode(cont_510.periodo,12,'4')))))))))))),1))
		order by 1)
      loop
          -- Gera Informações do Balanço Patrimonial (SPED_J100)

          inter_pr_gera_ctb_sped_J100(p_cod_empresa, p_exercicio, v_cod_plano_cta, per.periodo_anual,per.periodo_tri,v_form_apur, v_des_erro);
          IF  v_des_erro IS NOT NULL
          THEN
              RAISE v_erro;
          END IF;


          -- Gera Informações do Demonstrativo de Resultados (DRE) (SPED_J100)
          inter_pr_gera_ctb_sped_J150_2(p_cod_empresa,0,tp_apuracao_dre,per.periodo_tri,indAcumulado,
                                       p_exercicio, v_cod_plano_cta, v_des_erro);
          IF  v_des_erro IS NOT NULL
          THEN
              RAISE v_erro;
          END IF;
      end loop;

   end if;

   -- Gera Informações dos movimentos do Razao/diario de clientes (SPED_I550)
   inter_pr_gera_ctb_sped_cli (p_cod_empresa, p_exercicio,p_data_inicial, p_data_final,'cont_f001', 0,0, v_des_erro);
   IF  v_des_erro IS NOT NULL
   THEN
      p_des_erro := v_des_erro;
      RAISE v_erro;

   END IF;

   -- Gera Informações dos movimentos do Razao/diario de fornecedores (SPED_I550)

   inter_pr_gera_ctb_sped_for (p_cod_empresa, p_exercicio,p_data_inicial, p_data_final,'cont_f001', 0,0, v_des_erro);
   IF  v_des_erro IS NOT NULL
   THEN
      p_des_erro := v_des_erro;
      RAISE v_erro;

   END IF;
   COMMIT;

EXCEPTION
   WHEN v_erro THEN
      p_des_erro := 'Erro na procedure p_gera_auxiliar_sped ' || Chr(10) || v_des_erro;
      commit;

END inter_pr_gera_auxiliar_spedctb;
/

exec inter_pr_recompile;

