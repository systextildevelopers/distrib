alter table obrf_015 
add (vICMSOp51   number(15,2));

comment on column obrf_015.vICMSOp51 
 is 'CST 51 - Valor como se nao tivesse diferimento';

alter table obrf_015 
modify ( vICMSOp51   default 0.00);

/

exec inter_pr_recompile;
/
