create or replace procedure inter_pr_gera_ctb_sped_0000(p_cod_empresa    IN NUMBER,
                                                        p_exercicio      IN NUMBER,
                                                        p_ind_sit_esp    IN NUMBER,
                                                        p_cod_plano_cta OUT NUMBER,
                                                        p_per_inicial   IN DATE,
                                                        p_per_final     IN DATE,
                                                        p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar a tabela sped_0000 - Dados da empresa
   --
   -- Historicos
   --
   -- Data    Autor    Observac?es
   --

   cursor fatu500(p_cod_empresa NUMBER) IS
      SELECT *
      from   fatu_500
      where  fatu_500.codigo_empresa = p_cod_empresa;

   v_erro EXCEPTION;
   v_sig_estado_empresa     basi_160.estado%TYPE;
   v_num_ddd_empresa        basi_160.ddd%TYPE;
   v_des_cidade             basi_160.cidade%type;
   v_codigo_fiscal_uf       basi_167.codigo_fiscal_uf%type;
   v_codigo_cidade_ibge     basi_160.codigo_fiscal%type;
   v_cod_plano_cta          cont_500.cod_plano_cta%type;
   v_ident_escr_contab_sped cont_530.ident_escr_contab_sped%type;
   v_identif_qualificacao   fatu_502.identif_qualificacao%type;
   v_cod_qualificacao       fatu_502.cod_qualificacao%type;
   v_nome_empresa           fatu_503.nm_emp_livro_fiscal%type;   
   v_des_email_cont         fatu_500.e_mail_contador%type;
   v_uf_cont                fatu_500.cod_cidade_contador%type;
   v_num_fone_cont          fatu_500.telefone_contador%type;
BEGIN

   p_des_erro := NULL;

   for reg_fatu500 in fatu500(p_cod_empresa)
   loop

      v_nome_empresa := ' ';
      
      begin
         select fatu_503.nm_emp_livro_fiscal
         into v_nome_empresa
         from fatu_503
         where fatu_503.codigo_empresa = p_cod_empresa;
      exception
          when no_data_found then
             v_nome_empresa := ' ';
      end;

      if trim(v_nome_empresa) is null
      then 
         v_nome_empresa := reg_fatu500.nome_empresa;
      end if;

      -- le a cidade da empresa informada pelo usuario
      begin
         select basi_160.estado,          basi_160.ddd,
                basi_160.codigo_fiscal,   basi_160.cidade,
                basi_167.codigo_fiscal_uf
         into   v_sig_estado_empresa,     v_num_ddd_empresa,
                v_codigo_cidade_ibge,     v_des_cidade,
                v_codigo_fiscal_uf
         from   basi_160, basi_167, basi_165
         where  basi_160.cod_cidade  = reg_fatu500.codigo_cidade
         and    basi_160.estado      = basi_167.estado
         and    basi_160.codigo_pais = basi_167.codigo_pais
         and    basi_160.codigo_pais = basi_165.codigo_pais;

      exception
         when others then
            v_sig_estado_empresa := NULL;
            v_num_ddd_empresa    := NULL;
            v_codigo_cidade_ibge := NULL;
            v_codigo_fiscal_uf   := NULL;

            inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura da cidade da empresa ' || p_cod_empresa || Chr(10) ||
                                       '   Cidade: ' || reg_fatu500.codigo_cidade);

            commit;
      end;

      begin
         select cod_plano_cta
         into   v_cod_plano_cta
         from   cont_500
         where  cod_empresa = p_cod_empresa
         and    exercicio   = p_exercicio;

      exception
         when others then
            v_cod_plano_cta := 0;

            inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura do exercicio ' || p_exercicio ||
                                       '   Empresa ' || p_cod_empresa || Chr(10) ||
                                       '   Cidade: ' || reg_fatu500.codigo_cidade);

            commit;

            RAISE V_ERRO;
      end;

      -- seta a variavel de retorno do codigo do plano de contas
      p_cod_plano_cta := v_cod_plano_cta;

      begin
         select ident_escr_contab_sped
         into   v_ident_escr_contab_sped
         from   cont_530
         where  cod_plano_cta = v_cod_plano_cta;

      exception
         when others then
            v_ident_escr_contab_sped := NULL;

            inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura do plano de contas ' ||
                                       v_cod_plano_cta);

            commit;

            RAISE V_ERRO;
      end;

      -- le a idetificacao e codigo da qualidicacao do signatario (contador)
      begin
         select identif_qualificacao,   cod_qualificacao
         into   v_identif_qualificacao, v_cod_qualificacao
         from   fatu_502
         where  codigo_empresa = p_cod_empresa;

      exception
         when others then
            v_identif_qualificacao := null;
            v_cod_qualificacao     := null;

            inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura da qualificacao do contador.');

            commit;

            RAISE V_ERRO;
      end;
      
       -- le o email, cidade e telefone do contabilista
      begin
         select e_mail_contador, cod_cidade_contador, telefone_contador 
         into   v_des_email_cont, v_uf_cont, v_num_fone_cont
         from   fatu_500
         where  codigo_empresa = p_cod_empresa;

      exception
         when others then
            v_des_email_cont := null;
            v_uf_cont := null;
            v_num_fone_cont := null;
            inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura de dados do contador.');

            commit;

            RAISE V_ERRO;
      end;

      begin
         insert into sped_0000
            (tip_contabil_fiscal,         cod_empresa,                  exercicio,
             num_cnpj9,                   num_cnpj4,                    num_cnpj2,
             nom_empresa,                 nom_fantasia,                 num_cep,
             des_endereco,                num_endereco,                 des_compl_endereco,
             nom_bairro,                  num_ddd_empresa,              num_fone,
             num_fax,                     des_email,                    per_inicial_contab,
             per_final_contab,            sig_estado,                   ind_sit_esp_contab,
             ident_escr_contab_sped,      num_junta_comercial,          des_cidade,
             num_inscri_estadual,
             cod_cidade,
             num_inscri_municipal,
             nom_contabilista,            num_cpf_contabilista,         identif_qualificacao,
             cod_qualificacao,            num_reg_contabilista,         des_email_contabilista,
             cod_cidade_contabilista,     num_fone_contabilista)
         values
            ('C',                         reg_fatu500.codigo_empresa,   p_exercicio,
             reg_fatu500.cgc_9,           reg_fatu500.cgc_4,            reg_fatu500.cgc_2,
             v_nome_empresa,              reg_fatu500.nome_fantasia,    reg_fatu500.cep_empresa,
             reg_fatu500.endereco,        reg_fatu500.numero,           reg_fatu500.complemento,
             reg_fatu500.bairro,          v_num_ddd_empresa,            reg_fatu500.telefone,
             reg_fatu500.fax_empresa,     reg_fatu500.e_mail,           p_per_inicial,
             p_per_final,                 v_sig_estado_empresa,         p_ind_sit_esp,
             v_ident_escr_contab_sped,    reg_fatu500.numero_reg_junta, v_des_cidade,
             substr(reg_fatu500.inscr_estadual, 1, 14),
             lpad(v_codigo_fiscal_uf, 2, 0) || lpad(v_codigo_cidade_ibge, 5, 0),
             substr(reg_fatu500.inscr_municipal, 1, 14),

             reg_fatu500.nome_contador,   substr(lpad(reg_fatu500.cpf_contador,11,0),1,11),     v_identif_qualificacao,
             v_cod_qualificacao,          substr(reg_fatu500.crc_contador, 1, 11),              v_des_email_cont,
             v_uf_cont,                   v_num_fone_cont);

      exception
         when others then
            p_des_erro := 'Erro na inclus?o da tabela sped_0000 ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;
   end loop;

   commit;

exception
   when V_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_0000 ' || Chr(10) || p_des_erro;

   when others then
      p_des_erro := 'Outros erros na procedure p_gera_sped_0000 ' || Chr(10) || SQLERRM;
END inter_pr_gera_ctb_sped_0000;
/
exec inter_pr_recompile;

