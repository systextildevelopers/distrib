
  CREATE OR REPLACE PROCEDURE "INTER_PR_VERIF_ORDENS" (p_nivel varchar2,
                                                  p_grupo varchar2,
                                                  p_subgrupo varchar2,
                                                  p_item varchar2)
is

begin

      for reg_pcpc_020 in (select pcpc_020.referencia_peca
      from tmrp_041, pcpc_020
      where tmrp_041.nivel_estrutura  = p_nivel
        and tmrp_041.grupo_estrutura  = p_grupo
        and tmrp_041.subgru_estrutura = p_subgrupo
        and tmrp_041.item_estrutura   = p_item
        and tmrp_041.area_producao    = 1
        and pcpc_020.ordem_producao   = tmrp_041.nr_pedido_ordem
      group by pcpc_020.referencia_peca)
      loop
         -- CHAMA PROCEDURE QUE INSERE NA TABELA TEMPORARIA DO RECALCULO
         inter_pr_marca_referencia_op('1',    reg_pcpc_020.referencia_peca,
                                      '000',  '000000',
                                      0,      0);
      end loop;

      for reg_pcpb_020 in( select pcpb_020.pano_sbg_nivel99,   pcpb_020.pano_sbg_grupo
      from tmrp_041, pcpb_020
      where tmrp_041.nivel_estrutura  = p_nivel
        and tmrp_041.grupo_estrutura  = p_grupo
        and tmrp_041.subgru_estrutura = p_subgrupo
        and tmrp_041.item_estrutura   = p_item
        and tmrp_041.area_producao    = 2
        and pcpb_020.ordem_producao   = tmrp_041.nr_pedido_ordem
      group by pcpb_020.pano_sbg_nivel99,   pcpb_020.pano_sbg_grupo)
      loop
         -- CHAMA PROCEDURE QUE INSERE NA TABELA TEMPORARIA DO RECALCULO
         inter_pr_marca_referencia_op('2',     reg_pcpb_020.pano_sbg_grupo,
                                      '000',   '000000',
                                      0,       0);
      end loop;

      for reg_pcpt_010 in(select tmrp_041.nivel_estrutura, tmrp_041.grupo_estrutura
      from tmrp_041, pcpt_010
      where tmrp_041.nivel_estrutura  = p_nivel
        and tmrp_041.grupo_estrutura  = p_grupo
        and tmrp_041.subgru_estrutura = p_subgrupo
        and tmrp_041.item_estrutura   = p_item
        and tmrp_041.area_producao    = 4
        and tmrp_041.nr_pedido_ordem  = pcpt_010.ordem_tecelagem
      group by tmrp_041.nivel_estrutura, tmrp_041.grupo_estrutura)
      loop
         -- CHAMA PROCEDURE QUE INSERE NA TABELA TEMPORARIA DO RECALCULO
         inter_pr_marca_referencia_op('4',     reg_pcpt_010.grupo_Estrutura,
                                      '000',   '000000',
                                      0,       0);
      end loop;

      for reg_pcpf_310 in(select pcpf_310.nivel_estrutura, pcpf_310.grupo_estrutura
      from pcpf_310, tmrp_041
      where pcpf_310.sequencia_plano  = tmrp_041.nr_pedido_ordem
        and tmrp_041.nivel_estrutura  = p_nivel
        and tmrp_041.grupo_estrutura  = p_grupo
        and tmrp_041.subgru_estrutura = p_subgrupo
        and tmrp_041.item_estrutura   = p_item
      group by pcpf_310.nivel_estrutura, pcpf_310.grupo_estrutura)
      loop
         -- CHAMA PROCEDURE QUE INSERE NA TABELA TEMPORARIA DO RECALCULO
         inter_pr_marca_referencia_op('4',     reg_pcpf_310.grupo_Estrutura,
                                      '000',   '000000',
                                      0,       0);
      end loop;
end inter_pr_verif_ordens;


 

/

exec inter_pr_recompile;

