create or replace function "INTER_FN_RELACIONA_MARCA" (v_codigo_empresa in NUMBER,
                                                        v_codigo_usuario in NUMBER,
                                                        v_item_nivel in VARCHAR2,
                                                        v_item_grupo in VARCHAR2,
                                                        v_item_subgru in VARCHAR2,
                                                        v_item_item in VARCHAR2,
                                                        v_cgc_forn9 in NUMBER,
                                                        v_cgc_forn4 in NUMBER,
                                                        v_cgc_forn2 in NUMBER,
                                                        v_nr_solic in NUMBER,
                                                        v_num_req in NUMBER,
                                                        v_seq_req in NUMBER)
return boolean is erro boolean;

v_codigo_embalagem        SUPR_063.MARCA%TYPE;
v_codigo_embalagem_ant    NUMERIC;
v_tipo_reg_usuario        NUMERIC;
v_tmp_int                 NUMERIC;

cursor c_marcas(c_item_nivel in VARCHAR2, c_item_grupo in VARCHAR2,
		   c_item_subgru in VARCHAR2, c_item_item in VARCHAR2,
		   c_cgc_forn9 in NUMBER, c_cgc_forn4 in NUMBER,
		   c_cgc_forn2 in NUMBER) is
	select supr_063.marca
	    from supr_063 
	    where supr_063.nivel = c_item_nivel
	    and supr_063.grupo = c_item_grupo
	    and supr_063.subgrupo = c_item_subgru
	    and supr_063.item = c_item_item
	    and (supr_063.cgc_9 = c_cgc_forn9
	        or supr_063.cgc_9 = 00000000) 
	    and (supr_063.cgc_4 = c_cgc_forn4
	        or supr_063.cgc_4 = 0000) 
	    and (supr_063.cgc_2 = c_cgc_forn2
	        or supr_063.cgc_2 = 00);

BEGIN

v_codigo_embalagem := 0;
v_codigo_embalagem_ant := 0;
v_tipo_reg_usuario := 0;
erro := false;
    
    FOR f_marcas IN c_marcas(v_item_nivel, v_item_grupo, v_item_subgru,
                v_item_item, v_cgc_forn9, v_cgc_forn4, v_cgc_forn2)
    LOOP
        v_codigo_embalagem := f_marcas.marca;
        select dbms_utility.get_hash_value(to_char(dbms_utility.get_time),100,99999)         
        into v_tipo_reg_usuario
        from dual;
        if v_codigo_embalagem_ant <> v_codigo_embalagem
        then
            v_codigo_embalagem_ant := v_codigo_embalagem;
            
            begin
            select count(*)
            into v_tmp_int
            from rcnb_060
            where rcnb_060.tipo_registro = 185
              and rcnb_060.nr_solicitacao = v_nr_solic
              and rcnb_060.subgru_estrutura = v_codigo_usuario
              and rcnb_060.grupo_estrutura = v_codigo_empresa
              and rcnb_060.nivel_estrutura_str = v_item_nivel
              and rcnb_060.grupo_estrutura_str = v_item_grupo
              and rcnb_060.subgru_estrutura_str = v_item_subgru
              and rcnb_060.item_estrutura_str = v_item_item
              and rcnb_060.codigo_embalagem = v_codigo_embalagem
              and rcnb_060.nivel_estrutura = v_num_req
              and rcnb_060.item_estrutura = v_seq_req;
            end;
            
            if v_tmp_int = 0
            then
                begin
                insert into rcnb_060 (               
                        rcnb_060.tipo_registro, rcnb_060.nr_solicitacao, 
                        rcnb_060.tipo_registro_rel, rcnb_060.grupo_estrutura, 
                        rcnb_060.subgru_estrutura, rcnb_060.codigo_embalagem, 
                        rcnb_060.campo_str_01, rcnb_060.nivel_estrutura_str,
                        rcnb_060.grupo_estrutura_str, rcnb_060.subgru_estrutura_str,
                        rcnb_060.item_estrutura_str, rcnb_060.nivel_estrutura,
                        rcnb_060.item_estrutura) 
                values (185 , v_nr_solic , 
                        v_tipo_reg_usuario , v_codigo_empresa , 
                        v_codigo_usuario , v_codigo_embalagem, 
                        'Y', v_item_nivel, 
                        v_item_grupo, v_item_subgru, 
                        v_item_item,v_num_req,
                        v_seq_req);
                exception when others then
                    raise_application_error(-20001, SQLERRM);
                end;
            end if;
        end if;
    END LOOP;

    if erro = false
    then commit;
    else rollback;
    end if;
    return (erro);

END INTER_FN_RELACIONA_MARCA;
