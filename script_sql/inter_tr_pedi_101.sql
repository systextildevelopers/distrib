
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_101" 
before insert on pedi_101
for each row

declare
  v_id_registro number;
begin
   select nvl(max(pedi_101.seq_mensagem),0) + 1 into v_id_registro  from pedi_101
   where pedi_101.num_pedido   = :new.num_pedido;

   :new.seq_mensagem := v_id_registro;

end;
-- ALTER TRIGGER "INTER_TR_PEDI_101" ENABLE
 

/

exec inter_pr_recompile;

