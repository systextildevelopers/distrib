 -- Create new columns
alter table pedi_806
ADD(CGC_9 number(9) DEFAULT 0, CGC_4 number(4) DEFAULT 0, CGC_2 number(2) DEFAULT 0);

 -- Drop old PK
ALTER TABLE pedi_806
 DROP CONSTRAINT PK_PEDI_806;

 -- Drop old index
drop index PK_PEDI_806;
  
 -- Create primary key
ALTER TABLE pedi_806 ADD CONSTRAINT PK_PEDI_806 PRIMARY KEY (COD_REPRESENTANTE, COL_TABELA, MES_TABELA, SEQ_TABELA, CGC_9, CGC_4, CGC_2); 
  
exec inter_pr_recompile;
