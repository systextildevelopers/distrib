CREATE OR REPLACE TRIGGER "INTER_TR_INTE_110_LOG" 
after insert or delete or update
on inte_110
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   v_nome_programa  := inter_fn_nome_programa(ws_sid);  
   
 if inserting
 then
    begin

        insert into inte_110_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           CODIGO_REGISTRO_OLD,   /*8*/
           CODIGO_REGISTRO_NEW,   /*9*/
           PEDIDO_VENDA    ,   /*10*/
           SEQ_PRINCIPAL_OLD,   /*12*/
           SEQ_PRINCIPAL_NEW,   /*13*/
           SEQ_ITEM_PEDIDO_OLD,   /*14*/
           SEQ_ITEM_PEDIDO_NEW,   /*15*/
           ITEM_NIVEL99_OLD,   /*16*/
           ITEM_NIVEL99_NEW,   /*17*/
           ITEM_GRUPO_OLD,   /*18*/
           ITEM_GRUPO_NEW,   /*19*/
           ITEM_SUB_OLD,   /*20*/
           ITEM_SUB_NEW,   /*21*/
           ITEM_ITEM_OLD,   /*22*/
           ITEM_ITEM_NEW,   /*23*/
           CODIGO_DEPOSITO_OLD,   /*24*/
           CODIGO_DEPOSITO_NEW,   /*25*/
           LOTE_OLD,   /*26*/
           LOTE_NEW,   /*27*/
           QTDE_PEDIDA_OLD,   /*28*/
           QTDE_PEDIDA_NEW,   /*29*/
           VALOR_UNITARIO_OLD,   /*30*/
           VALOR_UNITARIO_NEW,   /*31*/
           PERCENTUAL_DESC_OLD,   /*32*/
           PERCENTUAL_DESC_NEW,   /*33*/
           TIPO_REGISTRO_OLD,   /*34*/
           TIPO_REGISTRO_NEW,   /*35*/
           LOJA_OLD,   /*36*/
           LOJA_NEW,   /*37*/
           SIT_IMPORTACAO_OLD,   /*38*/
           SIT_IMPORTACAO_NEW,   /*39*/
           CLIENTE9_OLD,   /*40*/
           CLIENTE9_NEW,   /*41*/
           CLIENTE4_OLD,   /*42*/
           CLIENTE4_NEW,   /*43*/
           CLIENTE2_OLD,   /*44*/
           CLIENTE2_NEW,   /*45*/
           PEDIDO_CLIENTE_OLD,   /*46*/
           PEDIDO_CLIENTE_NEW,   /*47*/
           NOVO_PEDIDO_VENDA_OLD,   /*48*/
           NOVO_PEDIDO_VENDA_NEW,   /*49*/
           COD_NAT_OP_OLD,   /*50*/
           COD_NAT_OP_NEW,   /*51*/
           EST_NAT_OP_OLD,   /*52*/
           EST_NAT_OP_NEW,   /*53*/
           CODIGO_EMBALAGEM_OLD,   /*54*/
           CODIGO_EMBALAGEM_NEW,   /*55*/
           ACRESCIMO_OLD,   /*56*/
           ACRESCIMO_NEW,   /*57*/
           EXECUTA_TRIGGER_OLD,   /*58*/
           EXECUTA_TRIGGER_NEW,   /*59*/
           QTDE_DISTRIBUIDA_OLD,   /*60*/
           QTDE_DISTRIBUIDA_NEW,   /*61*/
           GRADE_ITEM_OLD,   /*62*/
           GRADE_ITEM_NEW    /*63*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.CODIGO_REGISTRO, /*9*/
           :new.PEDIDO_VENDA, /*11*/
           0,/*12*/
           :new.SEQ_PRINCIPAL, /*13*/
           0,/*14*/
           :new.SEQ_ITEM_PEDIDO, /*15*/
           '',/*16*/
           :new.ITEM_NIVEL99, /*17*/
           '',/*18*/
           :new.ITEM_GRUPO, /*19*/
           '',/*20*/
           :new.ITEM_SUB, /*21*/
           '',/*22*/
           :new.ITEM_ITEM, /*23*/
           0,/*24*/
           :new.CODIGO_DEPOSITO, /*25*/
           0,/*26*/
           :new.LOTE, /*27*/
           0,/*28*/
           :new.QTDE_PEDIDA, /*29*/
           0,/*30*/
           :new.VALOR_UNITARIO, /*31*/
           0,/*32*/
           :new.PERCENTUAL_DESC, /*33*/
           0,/*34*/
           :new.TIPO_REGISTRO, /*35*/
           0,/*36*/
           :new.LOJA, /*37*/
           0,/*38*/
           :new.SIT_IMPORTACAO, /*39*/
           0,/*40*/
           :new.CLIENTE9, /*41*/
           0,/*42*/
           :new.CLIENTE4, /*43*/
           0,/*44*/
           :new.CLIENTE2, /*45*/
           '',/*46*/
           :new.PEDIDO_CLIENTE, /*47*/
           0,/*48*/
           :new.NOVO_PEDIDO_VENDA, /*49*/
           0,/*50*/
           :new.COD_NAT_OP, /*51*/
           '',/*52*/
           :new.EST_NAT_OP, /*53*/
           0,/*54*/
           :new.CODIGO_EMBALAGEM, /*55*/
           0,/*56*/
           :new.ACRESCIMO, /*57*/
           0,/*58*/
           :new.EXECUTA_TRIGGER, /*59*/
           0,/*60*/
           :new.QTDE_DISTRIBUIDA, /*61*/
           0,/*62*/
           :new.GRADE_ITEM /*63*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into inte_110_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           CODIGO_REGISTRO_OLD, /*8*/
           CODIGO_REGISTRO_NEW, /*9*/
           PEDIDO_VENDA   , /*10*/
           SEQ_PRINCIPAL_OLD, /*12*/
           SEQ_PRINCIPAL_NEW, /*13*/
           SEQ_ITEM_PEDIDO_OLD, /*14*/
           SEQ_ITEM_PEDIDO_NEW, /*15*/
           ITEM_NIVEL99_OLD, /*16*/
           ITEM_NIVEL99_NEW, /*17*/
           ITEM_GRUPO_OLD, /*18*/
           ITEM_GRUPO_NEW, /*19*/
           ITEM_SUB_OLD, /*20*/
           ITEM_SUB_NEW, /*21*/
           ITEM_ITEM_OLD, /*22*/
           ITEM_ITEM_NEW, /*23*/
           CODIGO_DEPOSITO_OLD, /*24*/
           CODIGO_DEPOSITO_NEW, /*25*/
           LOTE_OLD, /*26*/
           LOTE_NEW, /*27*/
           QTDE_PEDIDA_OLD, /*28*/
           QTDE_PEDIDA_NEW, /*29*/
           VALOR_UNITARIO_OLD, /*30*/
           VALOR_UNITARIO_NEW, /*31*/
           PERCENTUAL_DESC_OLD, /*32*/
           PERCENTUAL_DESC_NEW, /*33*/
           TIPO_REGISTRO_OLD, /*34*/
           TIPO_REGISTRO_NEW, /*35*/
           LOJA_OLD, /*36*/
           LOJA_NEW, /*37*/
           SIT_IMPORTACAO_OLD, /*38*/
           SIT_IMPORTACAO_NEW, /*39*/
           CLIENTE9_OLD, /*40*/
           CLIENTE9_NEW, /*41*/
           CLIENTE4_OLD, /*42*/
           CLIENTE4_NEW, /*43*/
           CLIENTE2_OLD, /*44*/
           CLIENTE2_NEW, /*45*/
           PEDIDO_CLIENTE_OLD, /*46*/
           PEDIDO_CLIENTE_NEW, /*47*/
           NOVO_PEDIDO_VENDA_OLD, /*48*/
           NOVO_PEDIDO_VENDA_NEW, /*49*/
           COD_NAT_OP_OLD, /*50*/
           COD_NAT_OP_NEW, /*51*/
           EST_NAT_OP_OLD, /*52*/
           EST_NAT_OP_NEW, /*53*/
           CODIGO_EMBALAGEM_OLD, /*54*/
           CODIGO_EMBALAGEM_NEW, /*55*/
           ACRESCIMO_OLD, /*56*/
           ACRESCIMO_NEW, /*57*/
           EXECUTA_TRIGGER_OLD, /*58*/
           EXECUTA_TRIGGER_NEW, /*59*/
           QTDE_DISTRIBUIDA_OLD, /*60*/
           QTDE_DISTRIBUIDA_NEW, /*61*/
           GRADE_ITEM_OLD, /*62*/
           GRADE_ITEM_NEW  /*63*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.CODIGO_REGISTRO,  /*8*/
           :new.CODIGO_REGISTRO, /*9*/
           :new.PEDIDO_VENDA, /*11*/
           :old.SEQ_PRINCIPAL,  /*12*/
           :new.SEQ_PRINCIPAL, /*13*/
           :old.SEQ_ITEM_PEDIDO,  /*14*/
           :new.SEQ_ITEM_PEDIDO, /*15*/
           :old.ITEM_NIVEL99,  /*16*/
           :new.ITEM_NIVEL99, /*17*/
           :old.ITEM_GRUPO,  /*18*/
           :new.ITEM_GRUPO, /*19*/
           :old.ITEM_SUB,  /*20*/
           :new.ITEM_SUB, /*21*/
           :old.ITEM_ITEM,  /*22*/
           :new.ITEM_ITEM, /*23*/
           :old.CODIGO_DEPOSITO,  /*24*/
           :new.CODIGO_DEPOSITO, /*25*/
           :old.LOTE,  /*26*/
           :new.LOTE, /*27*/
           :old.QTDE_PEDIDA,  /*28*/
           :new.QTDE_PEDIDA, /*29*/
           :old.VALOR_UNITARIO,  /*30*/
           :new.VALOR_UNITARIO, /*31*/
           :old.PERCENTUAL_DESC,  /*32*/
           :new.PERCENTUAL_DESC, /*33*/
           :old.TIPO_REGISTRO,  /*34*/
           :new.TIPO_REGISTRO, /*35*/
           :old.LOJA,  /*36*/
           :new.LOJA, /*37*/
           :old.SIT_IMPORTACAO,  /*38*/
           :new.SIT_IMPORTACAO, /*39*/
           :old.CLIENTE9,  /*40*/
           :new.CLIENTE9, /*41*/
           :old.CLIENTE4,  /*42*/
           :new.CLIENTE4, /*43*/
           :old.CLIENTE2,  /*44*/
           :new.CLIENTE2, /*45*/
           :old.PEDIDO_CLIENTE,  /*46*/
           :new.PEDIDO_CLIENTE, /*47*/
           :old.NOVO_PEDIDO_VENDA,  /*48*/
           :new.NOVO_PEDIDO_VENDA, /*49*/
           :old.COD_NAT_OP,  /*50*/
           :new.COD_NAT_OP, /*51*/
           :old.EST_NAT_OP,  /*52*/
           :new.EST_NAT_OP, /*53*/
           :old.CODIGO_EMBALAGEM,  /*54*/
           :new.CODIGO_EMBALAGEM, /*55*/
           :old.ACRESCIMO,  /*56*/
           :new.ACRESCIMO, /*57*/
           :old.EXECUTA_TRIGGER,  /*58*/
           :new.EXECUTA_TRIGGER, /*59*/
           :old.QTDE_DISTRIBUIDA,  /*60*/
           :new.QTDE_DISTRIBUIDA, /*61*/
           :old.GRADE_ITEM,  /*62*/
           :new.GRADE_ITEM  /*63*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into inte_110_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           CODIGO_REGISTRO_OLD, /*8*/
           CODIGO_REGISTRO_NEW, /*9*/
           PEDIDO_VENDA    , /*10*/
           SEQ_PRINCIPAL_OLD, /*12*/
           SEQ_PRINCIPAL_NEW, /*13*/
           SEQ_ITEM_PEDIDO_OLD, /*14*/
           SEQ_ITEM_PEDIDO_NEW, /*15*/
           ITEM_NIVEL99_OLD, /*16*/
           ITEM_NIVEL99_NEW, /*17*/
           ITEM_GRUPO_OLD, /*18*/
           ITEM_GRUPO_NEW, /*19*/
           ITEM_SUB_OLD, /*20*/
           ITEM_SUB_NEW, /*21*/
           ITEM_ITEM_OLD, /*22*/
           ITEM_ITEM_NEW, /*23*/
           CODIGO_DEPOSITO_OLD, /*24*/
           CODIGO_DEPOSITO_NEW, /*25*/
           LOTE_OLD, /*26*/
           LOTE_NEW, /*27*/
           QTDE_PEDIDA_OLD, /*28*/
           QTDE_PEDIDA_NEW, /*29*/
           VALOR_UNITARIO_OLD, /*30*/
           VALOR_UNITARIO_NEW, /*31*/
           PERCENTUAL_DESC_OLD, /*32*/
           PERCENTUAL_DESC_NEW, /*33*/
           TIPO_REGISTRO_OLD, /*34*/
           TIPO_REGISTRO_NEW, /*35*/
           LOJA_OLD, /*36*/
           LOJA_NEW, /*37*/
           SIT_IMPORTACAO_OLD, /*38*/
           SIT_IMPORTACAO_NEW, /*39*/
           CLIENTE9_OLD, /*40*/
           CLIENTE9_NEW, /*41*/
           CLIENTE4_OLD, /*42*/
           CLIENTE4_NEW, /*43*/
           CLIENTE2_OLD, /*44*/
           CLIENTE2_NEW, /*45*/
           PEDIDO_CLIENTE_OLD, /*46*/
           PEDIDO_CLIENTE_NEW, /*47*/
           NOVO_PEDIDO_VENDA_OLD, /*48*/
           NOVO_PEDIDO_VENDA_NEW, /*49*/
           COD_NAT_OP_OLD, /*50*/
           COD_NAT_OP_NEW, /*51*/
           EST_NAT_OP_OLD, /*52*/
           EST_NAT_OP_NEW, /*53*/
           CODIGO_EMBALAGEM_OLD, /*54*/
           CODIGO_EMBALAGEM_NEW, /*55*/
           ACRESCIMO_OLD, /*56*/
           ACRESCIMO_NEW, /*57*/
           EXECUTA_TRIGGER_OLD, /*58*/
           EXECUTA_TRIGGER_NEW, /*59*/
           QTDE_DISTRIBUIDA_OLD, /*60*/
           QTDE_DISTRIBUIDA_NEW, /*61*/
           GRADE_ITEM_OLD, /*62*/
           GRADE_ITEM_NEW /*63*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.CODIGO_REGISTRO, /*8*/
		   0, /*9*/
           :old.PEDIDO_VENDA, /*10*/
           :old.SEQ_PRINCIPAL, /*12*/
           0, /*13*/
           :old.SEQ_ITEM_PEDIDO, /*14*/
           0, /*15*/
           :old.ITEM_NIVEL99, /*16*/
           '', /*17*/
           :old.ITEM_GRUPO, /*18*/
           '', /*19*/
           :old.ITEM_SUB, /*20*/
           '', /*21*/
           :old.ITEM_ITEM, /*22*/
           '', /*23*/
           :old.CODIGO_DEPOSITO, /*24*/
           0, /*25*/
           :old.LOTE, /*26*/
           0, /*27*/
           :old.QTDE_PEDIDA, /*28*/
           0, /*29*/
           :old.VALOR_UNITARIO, /*30*/
           0, /*31*/
           :old.PERCENTUAL_DESC, /*32*/
           0, /*33*/
           :old.TIPO_REGISTRO, /*34*/
           0, /*35*/
           :old.LOJA, /*36*/
           0, /*37*/
           :old.SIT_IMPORTACAO, /*38*/
           0, /*39*/
           :old.CLIENTE9, /*40*/
           0, /*41*/
           :old.CLIENTE4, /*42*/
           0, /*43*/
           :old.CLIENTE2, /*44*/
           0, /*45*/
           :old.PEDIDO_CLIENTE, /*46*/
           '', /*47*/
           :old.NOVO_PEDIDO_VENDA, /*48*/
           0, /*49*/
           :old.COD_NAT_OP, /*50*/
           0, /*51*/
           :old.EST_NAT_OP, /*52*/
           '', /*53*/
           :old.CODIGO_EMBALAGEM, /*54*/
           0, /*55*/
           :old.ACRESCIMO, /*56*/
           0, /*57*/
           :old.EXECUTA_TRIGGER, /*58*/
           0, /*59*/
           :old.QTDE_DISTRIBUIDA, /*60*/
           0, /*61*/
           :old.GRADE_ITEM, /*62*/
           0 /*63*/
         );
    end;
 end if;
end inter_tr_inte_110_log;

-- ALTER TRIGGER "INTER_TR_INTE_110_LOG" ENABLE
 

/

exec inter_pr_recompile;

