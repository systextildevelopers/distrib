
  CREATE OR REPLACE PROCEDURE "INTER_PR_REC_COMBINACOES" 
   (p_empresa_logada in number,
    p_qtde_total     in number,
    p_cod_projeto    in varchar2,
    p_codigo_usuario in number,
    p_usuario        in varchar2,
    p_nr_solicitacao in number) is

   TYPE qtdeReservada IS TABLE OF basi_843.qtde_reservada%TYPE;
   qtde_reservada qtdeReservada;

   TYPE combinacaoItem IS TABLE OF basi_843.combinacao_item%TYPE;
   combinacao_item combinacaoItem;

   p_mensagem          varchar2(200);
   p_usuario_rede      varchar2(20);
   p_maquina_rede      varchar2(40);
   p_aplicativo        varchar2(20);
   p_sid               number(9);
   p_usuario_systextil varchar2(250);
   p_locale_usuario    varchar2(5);
   p_empresa_usu       number(3);

   nr_combin_zero  integer(10);
   soma_qtde_843   tmrp_615.qtde_necessaria%TYPE;
   qtde_total_comb tmrp_615.qtde_necessaria%TYPE;
   qtde_necessaria tmrp_615.qtde_necessaria%TYPE;

BEGIN

   inter_pr_dados_usuario (p_usuario_rede,
                           p_maquina_rede,
                           p_aplicativo,
                           p_sid,
                           p_usuario_systextil,
                           p_empresa_usu,
                           p_locale_usuario);

   /*Utiliza a tabela tempor�ria tmrp_615 para calcular as quantidades
     das combina��es (BASI_843)*/

   begin

      /*Busca a soma das quantidades das combina��es para um projeto*/
      select sum(basi_843.qtde_reservada)
        into soma_qtde_843
        from basi_843
      where basi_843.codigo_projeto = p_cod_projeto;
      exception
         when others then
            soma_qtde_843 := 0;
   end;

   if sql%notfound
   then
      soma_qtde_843 := 0;
   end if;

   begin

      /*Busca a quantia de combina��es com a qtde_reservada zerada*/
      select count(*)
        into nr_combin_zero
        from basi_843
       where basi_843.codigo_projeto = p_cod_projeto
         and basi_843.qtde_reservada = 0;
      exception
         when others then
            nr_combin_zero := 1;
   end;

   if sql%notfound or nr_combin_zero = 0
   then
      nr_combin_zero := 1;
   end if;

   /*F�rmula para ratear a quantidade da solicita��o (PEDI_667) pelas combina��es que est�o zeradas.*/
   qtde_total_comb := (p_qtde_total - soma_qtde_843) / nr_combin_zero;

   /*Explos�o das combina��es para gravar na tabela tempor�ria (TMRP_615) as quantias j� calculadas*/
   begin

      select basi_843.qtde_reservada, basi_843.combinacao_item
      BULK COLLECT INTO
             qtde_reservada,          combinacao_item
        from basi_843
       where basi_843.codigo_projeto = p_cod_projeto
         and basi_843.situacao_comb  in (0,2); /* 0-ATIVO     1-INATIVO        2-LAN�AMENTO */
   end;

   if sql%found
   then

      FOR i IN qtde_reservada.FIRST .. qtde_reservada.LAST
      LOOP

         if qtde_reservada(i) = 0
         then
            qtde_necessaria := qtde_total_comb;
         else
            qtde_necessaria := qtde_reservada(i);
         end if;

         if qtde_necessaria > 0
         then

            begin
               insert into /*+APPEND*/ tmrp_615
                  (nome_programa,   nr_solicitacao,
                   codigo_usuario,  tipo_registro,
                   usuario,         codigo_empresa,
                   codigo_projeto,  item,
                   qtde_necessaria)
               values
                  ('inter_pr_rec_combinacoes', p_nr_solicitacao,
                   p_codigo_usuario,           255,
                   p_usuario,                  p_empresa_logada,
                   p_cod_projeto,              combinacao_item(i),
                   qtde_necessaria);
               exception
                  when others then
                     --ATEN��O! N�o inseriu TMRP_615 {0}. Status: {1}
                     p_mensagem := inter_fn_buscar_tag_composta('ds21459',
                                                                'TMRP_615',
                                                                sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',
                                                                p_locale_usuario,
                                                                p_usuario_systextil);

                     raise_application_error(-20000, p_mensagem);
            end;
         end if;
      END LOOP;
   end if;

END inter_pr_rec_combinacoes;
 

/

exec inter_pr_recompile;

