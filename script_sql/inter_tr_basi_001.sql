
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_001" 
   after insert or update
       of  CODIGO_PROJETO, NIVEL_PRODUTO, GRUPO_PRODUTO
   on basi_001
   for each row

declare

marca_aux number;
tmpInt number;

begin

   -- Atualiza a marca do produto
   if (:new.grupo_produto <> '00000' and :new.nivel_produto = '1')
   then
      begin
       select pedi_666.marca
       into marca_aux
       from pedi_666
       where pedi_666.codigo_projeto = :new.codigo_projeto;
      exception
        when no_data_found then
          null;
      end;
      if SQL%found   --- Quando encontrou registro
      then
        begin
         select basi_400.codigo_informacao
         into tmpInt
         from basi_400
         where basi_400.nivel = :new.nivel_produto
           and   basi_400.grupo = :new.grupo_produto
           and   basi_400.subgrupo = '000'
           and   basi_400.item = '000000'
           and   basi_400.tipo_informacao = 17;
        exception
          when no_data_found then
            null;
        end;
        if SQL%found
        then
           if (tmpInt = 0)
           then
              begin
                 update basi_400
                 set basi_400.codigo_informacao = marca_aux
                 where basi_400.nivel = :new.nivel_produto
                   and basi_400.grupo = :new.grupo_produto
                   and basi_400.subgrupo = '000'
                   and basi_400.item = '000000'
                   and basi_400.tipo_informacao = 17;
               exception
                 when others then
                   null;
               end;
             end if;
          else
             begin
                INSERT into basi_400(
                   nivel,               grupo,
                   subgrupo,            item,
                   tipo_informacao,     codigo_informacao)
                values(
                   :new.nivel_produto,  :new.grupo_produto,
                   '000',               '000000',
                   17,                  marca_aux);
             exception
                when others then
                null;
             end;
          end if;
       end if;
  end if;



  if updating
  then
   -- Trigger criada para atualizar a imagem cadastrada para o projeto.
   begin
      UPDATE basi_750
      set basi_750.nivel          = :new.nivel_produto,
          basi_750.grupo          = :new.grupo_produto
      where basi_750.codigo_projeto = :new.codigo_projeto;
       exception
                      when others then
                       null;

   end;
end if;
end inter_tr_basi_001;


-- ALTER TRIGGER "INTER_TR_BASI_001" ENABLE
 

/

exec inter_pr_recompile;

