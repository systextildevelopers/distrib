-- Create table
create table HDOC_038
(
  CODIGO_EMPRESA     NUMBER(3) default 0 not null,
  MATRICULA          NUMBER(9) not null,
  FUNCIONARIO        VARCHAR2(60) default '' not null,
  CENTRO_CUSTO       NUMBER(6) not null,
  FUNCAO             VARCHAR2(60) default '' not null,
  E_MAIL             VARCHAR2(40) default '',
  SUBCONTA           NUMBER(4) default 0 not null,
  USUARIO            VARCHAR2(15) default '',
  ATUALIZADO_EM      DATE,
  CRIADO_EM          DATE not null
);

-- Create/Recreate primary, unique and foreign key constraints
alter table HDOC_038
  add constraint PK_HDOC_038 PRIMARY KEY (MATRICULA);

CREATE OR REPLACE TRIGGER TR_HDOC_038_INSERT
BEFORE INSERT ON HDOC_038
FOR EACH ROW
BEGIN
SELECT sysdate
INTO :new.CRIADO_EM
FROM dual;
END;

/
commit;

CREATE OR REPLACE TRIGGER TR_HDOC_038_UPDATE
BEFORE UPDATE ON HDOC_038
FOR EACH ROW
BEGIN
SELECT sysdate
INTO :new.ATUALIZADO_EM
FROM dual;
END;

/
commit;

ALTER TABLE HDOC_030
ADD
(
  PERFIL_BASE                NUMBER(1) default 0
);
comment on column HDOC_030.PERFIL_BASE
  is 'Parametro que determina se e perfil base para novos usuarios - 1: SIM OU 0: NAO';
/
commit;

ALTER TABLE ORCM_001
ADD
(
  FLAG_CADASTRA_USUARIO NUMBER(1) default 0
);
comment on column ORCM_001.FLAG_CADASTRA_USUARIO
  is 'Parametro que determina se este centro de custos pode cadastrar novos usuarios - 1: SIM OU 0: NAO';
/
commit;
