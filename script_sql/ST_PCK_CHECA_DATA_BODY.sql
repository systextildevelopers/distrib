create or replace package body ST_PCK_CHECA_DATA is

    -- Função para Obter Regras
    FUNCTION get_regras(p_nome_form VARCHAR2) RETURN t_regras AS
            regras t_regras;
    BEGIN
        -- Inicializa regras padrão
        ST_PCK_CHECA_DATA.inicializa_excecoes;
        
        regras := REGRAS_PADRAO;
        -- Verifica se há uma exceção para o formulário específico
        IF EXCECOES.EXISTS(p_nome_form) THEN
            regras := EXCECOES(p_nome_form);
        END IF;
        RETURN regras;
    END get_regras;

    -- Procedimento para Inicializar Exceções 
    -- Poderia ser diferente mas precisa ser assim para clientes oracle 11
    PROCEDURE inicializa_excecoes IS
        v_regras t_regras;
    BEGIN
        v_regras.validarPorContabCardex := 0;
        v_regras.validarExercicio       := 1;
        v_regras.validarPeriodo         := 0;
        
        -- Inicializa regras padrão
        REGRAS_PADRAO := v_regras;
        
        -- Inicializa exceções
        v_regras.validarPorContabCardex := 1;
        v_regras.validarExercicio       := 1;
        v_regras.validarPeriodo         := 1;

        EXCECOES('cont_f820') := v_regras;
        
        v_regras.validarPorContabCardex := 0;
        v_regras.validarExercicio       := 0;
        v_regras.validarPeriodo         := 0;

        EXCECOES('obrf_f135') := v_regras;
        EXCECOES('obrf_f038') := v_regras;
        EXCECOES('obrf_f240') := v_regras;
        EXCECOES('cpag_f015') := v_regras;
        EXCECOES('crec_f567') := v_regras;
        
        v_regras.validarPorContabCardex := 0;
        v_regras.validarExercicio       := 1;
        v_regras.validarPeriodo         := 0;
        
        EXCECOES('estq_e142') := v_regras;
        
    END inicializa_excecoes;


    FUNCTION executar(p_nome_form VARCHAR2, p_cod_empresa NUMBER, p_data_lac DATE, p_transacao NUMBER) 
    RETURN number 
    AS
        v_array_regras ST_PCK_CHECA_DATA.t_regras;
        v_array_exercicio ST_PCK_EXERCICIO.t_dados_exercicio;
        v_array_empresa ST_PCK_EMPRESAS.t_dados_fatu_500;
        v_array_transacao ST_PCK_TRANSACAO.t_dados_estq_005;
        v_msg_error varchar2 (4000) := ''; 
        v_gera_contabilizacao boolean := TRUE; 
        
    BEGIN
        v_array_empresa := ST_PCK_EMPRESAS.get_fatu_500(p_cod_empresa, v_msg_error);
        IF NOT v_array_empresa.exists(1) OR (v_array_empresa(1).gera_contabil != 1 AND p_nome_form NOT IN ('obrf_f135', 'obrf_f240')) THEN
            RETURN 0;
        END IF;

        v_msg_error := '';
        v_array_regras := ST_PCK_CHECA_DATA.get_regras(p_nome_form);
        v_array_transacao := ST_PCK_TRANSACAO.get(p_transacao, v_msg_error);

        IF v_gera_contabilizacao = FALSE OR NOT v_array_transacao.exists(1) THEN  
            RETURN 0;
        END IF;

        IF v_array_regras.validarPorContabCardex = 1 THEN 
            v_gera_contabilizacao := v_array_transacao(1).contab_cardex = 1;
        ELSE 
            v_gera_contabilizacao := v_array_transacao(1).atualiza_contabi IN (1,3);
        END IF;

        v_array_exercicio := ST_PCK_EXERCICIO.valida_exercicio(p_cod_empresa, p_data_lac, v_array_regras, v_msg_error);
        IF NOT v_array_exercicio.exists(1)  THEN 
            RETURN NVL(v_array_exercicio(1).exercicio, -99);
        END IF;

        RETURN NVL(v_array_exercicio(1).exercicio, 0);
    END executar;

end ST_PCK_CHECA_DATA;
