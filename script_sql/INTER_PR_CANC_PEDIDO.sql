create or replace PROCEDURE inter_pr_canc_pedido (
    p_opcao IN NUMBER DEFAULT 0,
    p_pedido_venda IN VARCHAR2 DEFAULT '0'
)
is
    c0 SYS_REFCURSOR;
    v_cod_canc           number;
    v_cod_canc_tmp       number;
    v_texto              varchar2(4000);
    tmpInt               number;
    v_seq_dup_origem     number;
    v_sql                clob;
    v_pedido_venda       number; 
    v_cgc9               number;
    v_cgc4               number;
    v_cgc2               number;
    v_codigo_empresa     number;
    v_cod_rep_cliente    number;
    v_qtde_pedida        number;
    v_nr_titulo          number;
    v_numero_referencia  varchar2(60);
    v_ind                number;
    v_opcao              number;
    v_reg                number;
    v_mdi_usuario        varchar2(250);
    v_mdi_empresa        number;
    ws_usuario_rede      varchar2(20);
    ws_maquina_rede      varchar2(40);
    ws_aplicativo        varchar2(20);
    ws_sid               number;
    ws_locale_usuario    varchar2(5);
    

begin
    -- 08/2024 implementado inclusão de párâmetros para atender cancelamento de um pedido específico e cancelamento somente do Adiantamento
    v_ind := 0;
    v_opcao := p_opcao;
    if (v_opcao = 0) then
        v_opcao := 2; -- Quando não tiver opção passa a fazer completo
    end if;
    begin
        -- Dados do usuario logado
        inter_pr_dados_usuario (ws_usuario_rede,       ws_maquina_rede,     ws_aplicativo,    ws_sid,
                                v_mdi_usuario,         v_mdi_empresa,       ws_locale_usuario);

    end;
    v_sql := 'select PEDIDO_VENDA, CGC9,';
    v_sql := v_sql || ' CGC4,CGC2,'; 
    v_sql := v_sql || ' CODIGO_EMPRESA,COD_REP_CLIENTE,';
    v_sql := v_sql || ' QTDE_PEDIDA,NR_TITULO,';
    v_sql := v_sql || ' NUMERO_REFERENCIA';  --Inserido p/ cancelar Transação de Pagamento SPH
    v_sql := v_sql || ' from (select distinct'; 
    v_sql := v_sql || ' pedi_100.PEDIDO_VENDA'; 
    v_sql := v_sql || ' ,pedi_100.CODIGO_EMPRESA'; 
    v_sql := v_sql || ' ,pedi_100.DATA_EMIS_VENDA as data_emissao';
    v_sql := v_sql || ' ,pedi_100.DATA_ENTR_VENDA as DATA_ENTREGA';
    v_sql := v_sql || ' ,pedi_100.CLI_PED_CGC_CLI9 AS CGC9';
    v_sql := v_sql || ' ,pedi_100.CLI_PED_CGC_CLI4 AS CGC4';
    v_sql := v_sql || ' ,pedi_100.CLI_PED_CGC_CLI2 AS CGC2';
    v_sql := v_sql || ' ,pedi_100.COD_REP_CLIENTE';
    v_sql := v_sql || ' ,pedi_070.COND_PGT_CLIENTE ' || '||' || '''' || '-' || '''' || '||' || 'pedi_070.DESCR_PG_CLIENTE as condicao_pgto'; 
    v_sql := v_sql || ' ,pedi_121.NR_TITULO';
    v_sql := v_sql || ' ,case when pedi_100.situacao_venda = 9 then pedi_100.QTDE_SALDO_PEDI else pedi_100.QTDE_TOTAL_PEDI end AS QTDE_PEDIDA';
    v_sql := v_sql || ' ,case';
    v_sql := v_sql || ' when pedi_117.sit_boleto != 2 and (select count(1) as data_venc from FATU_070 fatu, FATU_504';
    v_sql := v_sql || ' where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA';
    v_sql := v_sql || ' and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9';
    v_sql := v_sql || ' and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4';
    v_sql := v_sql || ' and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2';
    v_sql := v_sql || ' and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO'; 
    v_sql := v_sql || ' and fatu.PEDIDO_VENDA = pedi_100.pedido_venda'; 
    v_sql := v_sql || ' and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO';
    v_sql := v_sql || ' and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA'; 
    v_sql := v_sql || ' and fatu.DATA_PRORROGACAO >= to_date(sysdate)';
    v_sql := v_sql || ' and fatu.SALDO_DUPLICATA > 0 ) >= 1 then ' || '''' || 'AGUARDANDO PAGAMENTO' || '''';
    v_sql := v_sql || ' when pedi_117.sit_boleto != 2 and (select count(1) as data_venc from FATU_070 fatu, FATU_504';
    v_sql := v_sql || ' where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA';
    v_sql := v_sql || ' and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9';
    v_sql := v_sql || ' and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4';
    v_sql := v_sql || ' and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2';
    v_sql := v_sql || ' and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO';
    v_sql := v_sql || ' and fatu.PEDIDO_VENDA = pedi_100.pedido_venda';
    v_sql := v_sql || ' and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO';
    v_sql := v_sql || ' and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA';
    v_sql := v_sql || ' and fatu.DATA_PRORROGACAO < to_date(sysdate)';
    v_sql := v_sql || ' and fatu.duplic_emitida <> 3';
    v_sql := v_sql || ' and fatu.SALDO_DUPLICATA > 0) >= 1 then ' || '''' || 'ANTECIPAÇÃO EM ATRASO' || '''';
    v_sql := v_sql || ' when pedi_117.sit_boleto != 2 and (select count(1) as data_venc from FATU_070 fatu, FATU_504';
    v_sql := v_sql || ' where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA';
    v_sql := v_sql || ' and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9';
    v_sql := v_sql || ' and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4';
    v_sql := v_sql || ' and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2';
    v_sql := v_sql || ' and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO';
    v_sql := v_sql || ' and fatu.PEDIDO_VENDA = pedi_100.pedido_venda';
    v_sql := v_sql || ' and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO';
    v_sql := v_sql || ' and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA';
    v_sql := v_sql || ' and fatu.SALDO_DUPLICATA = 0) >= 1 then ' || '''' || 'ANTECIPAÇÃO RECEBIDA' || '''';
    v_sql := v_sql || ' when pedi_117.sit_boleto = 2 then ' || '''' || 'ANTECIPAÇÃO CANCELADA' || '''';
    v_sql := v_sql || ' when pedi_117.sit_boleto in (0,1) and pedi_100.pedido_venda = pedi_118.pedido_venda then ' || '''' || 'PROCESSO DE ANTECIPAÇÃO REALIZADO' || '''';
    v_sql := v_sql || ' when ((select nvl(sum(QTDE_SUGERIDA),0) from pedi_110 where pedido_venda = pedi_100.pedido_venda) > 0) and pedi_118.pedido_venda is null then ' || '''' || 'PEDIDO SUGERIDO' || '''';
    v_sql := v_sql || ' else ' || '''' || 'PEDIDO DIGITADO' || '''';
    v_sql := v_sql || ' end as situacao_antec';
    v_sql := v_sql || ' , pedi_010.cod_cidade';
    v_sql := v_sql || ' , pedi_100.numero_referencia'; --Inserido p/ cancelar Transação de Pagamento SPH
    v_sql := v_sql || ' from  pedi_100';
    v_sql := v_sql || ' ,pedi_110';
    v_sql := v_sql || ' ,pedi_070'; --condição de pagamento
    v_sql := v_sql || ' ,pedi_010'; --cliente
    v_sql := v_sql || ' ,pedi_075';
    v_sql := v_sql || ' ,pedi_118';
    v_sql := v_sql || ' ,pedi_117';
    v_sql := v_sql || ' ,pedi_121';
    v_sql := v_sql || ' ,loja_010';--adicionado SPH
    v_sql := v_sql || ' where   pedi_100.COND_PGTO_VENDA =  pedi_070.COND_PGT_CLIENTE';
    v_sql := v_sql || ' and     pedi_100.pedido_venda = pedi_110.pedido_venda';
    v_sql := v_sql || ' and     pedi_100.cod_forma_pagto = loja_010.forma_pgto(+) ';  -- Adicionado SPH
    v_sql := v_sql || ' and     pedi_100.CLI_PED_CGC_CLI9 = pedi_010.CGC_9 (+)';
    v_sql := v_sql || ' and     pedi_100.CLI_PED_CGC_CLI4 = pedi_010.CGC_4 (+)';
    v_sql := v_sql || ' and     pedi_100.CLI_PED_CGC_CLI2 = pedi_010.CGC_2 (+)';
    v_sql := v_sql || ' and     pedi_100.cond_pgto_venda = pedi_075.condicao_pagto';
    v_sql := v_sql || ' and     pedi_100.CLI_PED_CGC_CLI9 = pedi_118.CGC9 (+)';
    v_sql := v_sql || ' and     pedi_100.CLI_PED_CGC_CLI4 = pedi_118.CGC4 (+)';
    v_sql := v_sql || ' and     pedi_100.CLI_PED_CGC_CLI2 = pedi_118.CGC2 (+)';
    v_sql := v_sql || ' and     pedi_100.pedido_venda = pedi_118.pedido_venda  (+)';
    v_sql := v_sql || ' and     pedi_118.CGC9 = pedi_117.CGC9 (+)';
    v_sql := v_sql || ' and     pedi_118.CGC4 = pedi_117.CGC4 (+)';
    v_sql := v_sql || ' and     pedi_118.CGC2 = pedi_117.CGC2 (+)';
    v_sql := v_sql || ' and     pedi_118.SEQUENCIA = pedi_117.SEQUENCIA (+)';
    v_sql := v_sql || ' and     pedi_118.CGC9 = pedi_121.CGC9 (+)';
    v_sql := v_sql || ' and     pedi_118.CGC4 = pedi_121.CGC4 (+)';
    v_sql := v_sql || ' and     pedi_118.CGC2 = pedi_121.CGC2 (+)';
    v_sql := v_sql || ' and     pedi_118.SEQUENCIA = pedi_121.seq_deposito (+)';
    v_sql := v_sql || ' and     nvl(pedi_117.sit_boleto,0) != 5';
    v_sql := v_sql || ' and    (pedi_075.vencimento = 0 or (loja_010.codigo_servico_sph > 0))';  -- condiserar venda prazo quando for SPH
    v_sql := v_sql || ' and     pedi_100.cod_cancelamento = 0';
    v_sql := v_sql || ' and     pedi_110.situacao_fatu_it <> 1';
    v_sql := v_sql || ' and     pedi_110.cod_cancelamento  = 0';
    v_sql := v_sql || ' and     pedi_100.SITUACAO_VENDA != 10';
    v_sql := v_sql || ' and     (pedi_110.QTDE_SUGERIDA > 0';
    v_sql := v_sql || ' or (((select count(1) from pedi_118 pedi118';
    v_sql := v_sql || ' where pedi118.pedido_venda = pedi_100.pedido_venda) > 0)';
    v_sql := v_sql || ' and pedi_100.situacao_venda <> 9)))';
    v_sql := v_sql || ' where SITUACAO_ANTEC is not null';
    v_sql := v_sql || ' and SITUACAO_ANTEC not in ' || '''' || 'ANTECIPAÇÃO RECEBIDA' || '''';
    if (p_pedido_venda <> '0') then
        v_sql := v_sql || ' and PEDIDO_VENDA in( ' || p_pedido_venda || ')';
    end if;
    v_sql := v_sql || ' order by pedido_venda ';    
    EXECUTE IMMEDIATE v_sql;
    OPEN c0 FOR v_sql;
    LOOP
        FETCH c0 INTO 
            v_pedido_venda, 
            v_cgc9,
            v_cgc4,
            v_cgc2, 
            v_codigo_empresa,
            v_cod_rep_cliente,
            v_qtde_pedida,
            v_nr_titulo,
            v_numero_referencia;
        EXIT WHEN c0%NOTFOUND;

        v_ind := v_ind + 1;
        v_cod_canc      := 0;
        v_cod_canc_tmp  := 0;

        -- Buscando Código de Cancelamento
        begin
            select  COD_CANCEL_PEDIDO
            into    v_cod_canc_tmp
            from    PEDI_117_TEMP
            where   CODIGO_EMPRESA = v_codigo_empresa;
        exception when no_data_found then
            select  COD_CANCEL_PEDIDO
            into    v_cod_canc_tmp
            from    PEDI_117_TEMP
            where   CODIGO_EMPRESA = 3;
        end;

        -- Implementação feita para atender o crec_fa02
        if (v_cod_canc_tmp >= 0) then
            begin
                select  nvl(pedi_140.cod_canc_pedido,0) 
                into    v_cod_canc
                from    pedi_140
                where   pedi_140.cod_canc_pedido = v_cod_canc_tmp;
            end;
        end if;

        if (v_cod_canc = 0) then
            insert into fina_211(ID,DATA_MOVIMENTO,NOME_PROCESSO,TABELA,TIPO_PROCESSO,USUARIO,EMPRESA,DADOS) 
            values(FINA_211_SEQ.nextval, SYSDATE,'CANCELAMENTO PEDIDO', 'inter_pr_canc_pedido', 'ERRO',v_mdi_usuario, v_mdi_empresa,'Não foi encontrado Código de Cancelamento, verifique!');
            --commit;
        else

            insert into fina_211(ID,DATA_MOVIMENTO,NOME_PROCESSO,TABELA,TIPO_PROCESSO,USUARIO,EMPRESA,DADOS) 
            values(FINA_211_SEQ.nextval, SYSDATE,'CANCELAMENTO PEDIDO', 'inter_pr_canc_pedido', 'PROCESSO',v_mdi_usuario, v_mdi_empresa,'Processo de cancelamento pedido ' || v_pedido_venda);
            --commit;

            -- Imnplementação do crec_fa02 para bloqueio usuário sem direitro de acesso
            /*
            begin
                select  1 
                into    tmpInt
                from    hdoc_030
                where   hdoc_030.usuario = v('MDI_USUARIO')
                and     hdoc_030.empresa = v('MDI_CODIGO_EMPRESA')
                and     hdoc_030.agrupa_canc_ped = v_cod_canc;
            exception when others then
                insert into fina_211(ID,DATA_MOVIMENTO,NOME_PROCESSO,TABELA,TIPO_PROCESSO,USUARIO,EMPRESA,DADOS) 
                values(FINA_211_SEQ.nextval, SYSDATE,'CANCELAMENTO PEDIDO', 'inter_pr_canc_pedido', 'ERRO',v('MDI_USUARIO'), v('MDI_CODIGO_EMPRESA'),'Usuario não possui permissão para utilizar este código de cancelamento., verifique!');
                --commit;
                raise_application_error(-20001, 'Usuario não possui permissão para utilizar este código de cancelamento.');
            end;
            */

            if (v_numero_referencia is not null) then

                insert into fina_211(ID,DATA_MOVIMENTO,NOME_PROCESSO,TABELA,TIPO_PROCESSO,USUARIO,EMPRESA,DADOS) 
                values(FINA_211_SEQ.nextval, SYSDATE,'CANCELAMENTO PEDIDO', 'inter_pr_canc_pedido', 'PROCESSO',v_mdi_usuario, v_mdi_empresa,'Pedido com numero_referencia ' || v_numero_referencia );
              --  commit;

                -- Cancelar Transação Pagamento SPH
                UPDATE  FINA_204 
                SET     FINA_204.SITUACAO_SPH = 'CANCELADO' 
                WHERE   FINA_204.NUMERO_REFERENCIA = v_numero_referencia;

                --buscar o numero da antecipação da fina_204
                begin
                    select documento_origem
                    into   v_seq_dup_origem
                    from   fina_204
                    where  numero_referencia = v_numero_referencia;
                exception when others then
                    v_seq_dup_origem := 0;
                end;

                begin
                    UPDATE  FATU_070
                    SET     COD_CANC_DUPLIC     = v_cod_canc,
                            SALDO_DUPLICATA     = 0
                    WHERE   CODIGO_EMPRESA      = v_codigo_empresa
                    AND     NUM_DUPLICATA       = v_nr_titulo
                    AND     CLI_DUP_CGC_CLI9    = v_cgc9
                    AND     CLI_DUP_CGC_CLI4    = v_cgc4
                    AND     CLI_DUP_CGC_CLI2    = v_cgc2
                    AND     PEDIDO_VENDA        = v_pedido_venda;
                    --and     numero_referencia   = v_numero_referencia; --acrecentado o numero de referencia
                exception when others
                then
                    continue;
                end;                    


            else

                insert into fina_211(ID,DATA_MOVIMENTO,NOME_PROCESSO,TABELA,TIPO_PROCESSO,USUARIO,EMPRESA,DADOS) 
                values(FINA_211_SEQ.nextval, SYSDATE,'CANCELAMENTO PEDIDO', 'inter_pr_canc_pedido', 'PROCESSO',v_mdi_usuario, v_mdi_empresa,'Pedido sem numero referencia <PADRAO>');
                --commit;

                -- Buscar a antecipação para atender antecipação que não foram gerados pelo processo SPH
                begin
                    select  SEQ_DUP_ORIGEM
                    into    v_seq_dup_origem
                    from    FATU_070
                    WHERE   CODIGO_EMPRESA      = v_codigo_empresa
                    AND     NUM_DUPLICATA       = v_nr_titulo
                    AND     CLI_DUP_CGC_CLI9    = v_cgc9
                    AND     CLI_DUP_CGC_CLI4    = v_cgc4
                    AND     CLI_DUP_CGC_CLI2    = v_cgc2
                    AND     PEDIDO_VENDA        = v_PEDIDO_VENDA
                    and     rownum = 1;
                exception when others then
                    v_seq_dup_origem := 0;
                end;

                begin
                    UPDATE FATU_070 
                    SET     COD_CANC_DUPLIC     = v_cod_canc,
                            SALDO_DUPLICATA     = 0
                    WHERE   CODIGO_EMPRESA      = v_codigo_empresa
                    AND     NUM_DUPLICATA       = v_nr_titulo
                    AND     CLI_DUP_CGC_CLI9    = v_cgc9
                    AND     CLI_DUP_CGC_CLI4    = v_cgc4
                    AND     CLI_DUP_CGC_CLI2    = v_cgc2
                    AND     PEDIDO_VENDA        = v_pedido_venda;
                    exception when others then
                        continue;
                    end;                
            end if;
        
            update  pedi_117
            set     sit_boleto  = 2
            where   cgc9        = v_cgc9
            and     cgc4        = v_cgc4
            and     cgc2        = v_cgc2
            and     sequencia   = v_seq_dup_origem;

            begin
                UPDATE pedi_135 
                SET FLAG_LIBERACAO = 'N', --N - não liberado, M manual, A automatico
                    DATA_LIBERACAO = null, 
                    RESPONSAVEL = 'SPH'
                WHERE PEDIDO_VENDA = v_pedido_venda
                AND   CODIGO_SITUACAO in (28,61);
            end;

            begin
                select count(*) into v_reg from pedi_135 where pedi_135.PEDIDO_VENDA = v_pedido_venda and pedi_135.FLAG_LIBERACAO in ('M','N');
            exception when no_data_found then
                v_reg := 0;
            end;

            if (v_reg = 0) then
                begin
                    update  pedi_100 
                    set     pedi_100.SITUACAO_VENDA = 5 
                    where   pedi_100.PEDIDO_VENDA = v_pedido_venda;
                end;
            end if;

            -- Faz o cancelamento Geral
            if (v_opcao = 2) then

                insert into fina_211(ID,DATA_MOVIMENTO,NOME_PROCESSO,TABELA,TIPO_PROCESSO,USUARIO,EMPRESA,DADOS) 
                values(FINA_211_SEQ.nextval, SYSDATE,'CANCELAMENTO PEDIDO', 'inter_pr_canc_pedido', 'PROCESSO',v_mdi_usuario, v_mdi_empresa,'Pedido com opção = 2 <GERAL>');
                --commit;

                UPDATE  pcpt_020
                set     ROLO_ESTOQUE = 1,
                        pedido_venda = 0
                where   pedido_venda = v_pedido_venda;

                update  pedi_100
                set     COD_CANCELAMENTO = v_cod_canc,
                        DATA_CANC_VENDA = SYSDATE
                where   pedido_venda = v_pedido_venda;

                update  pedi_110
                set     COD_CANCELAMENTO = v_cod_canc,
                        DT_CANCELAMENTO = SYSDATE
                where   pedido_venda = v_pedido_venda;
            end if;
            commit;
        end if;
    end loop;

    insert into fina_211(ID,DATA_MOVIMENTO,NOME_PROCESSO,TABELA,TIPO_PROCESSO,USUARIO,EMPRESA,DADOS) 
    values(FINA_211_SEQ.nextval, SYSDATE,'CANCELAMENTO PEDIDO', 'inter_pr_canc_pedido', 'PROCESSO',v_mdi_usuario, v_mdi_empresa,v_sql);
    --commit;

    if (v_ind = 0) then
        raise_application_error(-20001, '<Erro> verifique a situação do Pedido de venda se está correto para este processo.');
    end if;
end inter_pr_canc_pedido;
/
