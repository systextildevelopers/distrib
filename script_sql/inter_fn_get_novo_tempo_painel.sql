CREATE OR REPLACE function inter_fn_get_novo_tempo_painel(p_ordem_producao     	in number,
                                                     	  p_periodo_confeccao  	in number,
                                                     	  p_pacote			  	in number,
														  p_estagio_conf 		in number)
return number is
   v_tempo_unitario         number;
   tmpInt number;
   id_snapshot_painel number;
begin

	--Está function só existe para compilar a primeira vez o objeto
	--o que vale está no systextil-costura
   return 0;
end inter_fn_get_novo_tempo_painel;
 

/

exec inter_pr_recompile;

