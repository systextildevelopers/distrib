insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('menu_ap55', 'Menu Apex', 1, 1, null);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'menu_ap55', 'menu_ad55', 1, -1, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'menu_ap55', 'menu_ad55', 1,-1, 'S', 'S', 'S', 'S');

commit;

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('pcpc_fa01', 'Rateio de rolos para ordens produção', 0, 1, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpc_fa01', 'menu_ap55', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpc_fa01', 'menu_ap55', 1, 0, 'S', 'S', 'S', 'S');

commit;
