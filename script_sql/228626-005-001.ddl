create table oper_027
(
CODIGO_HISTORICO_PGTO NUMBER(4) default 0,
COD_PORTADOR NUMBER(3) default 0, 
CONTA_CORRENTE NUMBER(9) default 0
);
alter table oper_027
  add constraint PK_oper_027 primary key (CODIGO_HISTORICO_PGTO, COD_PORTADOR, CONTA_CORRENTE);
  
/
exec inter_pr_recompile;
