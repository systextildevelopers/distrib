
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_640_LOG" 
   after insert or delete or update
       of NUMERO_RESERVA       , DATA_EMBARQUE_PLAN   ,
          CNPJ_CLIENTE9        , TIPO_RESERVA         ,
          CNPJ_CLIENTE4        , RESERVA_ORIGINAL     ,
          CNPJ_CLIENTE2        ,
          DATA_CADASTRO        , PROJETO_ASSOCIADO    ,
          DATA_VALIDADE        , COD_CANCELAMENTO     ,
          DATA_EMBARQUE_CLI    , DATA_CANCELAMENTO    ,
          USUARIO_CANCELAMENTO , USUARIO_CADASTRO     ,
          STATUS
   on tmrp_640
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_nome_cliente_o         varchar2(60);
   ws_nome_cliente_n         varchar2(60);

   v_desc_tipo_reserva_n     varchar2(100);
   v_desc_tipo_reserva_o     varchar2(100);

   long_aux                  varchar2(2000);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   begin
      select hdoc_001.descricao
      into v_desc_tipo_reserva_n
      from hdoc_001
      where hdoc_001.tipo = 341
        and hdoc_001.codigo = :new.tipo_reserva;
      exception when no_data_found then
         v_desc_tipo_reserva_n := '(' || inter_fn_buscar_tag('lb34881#Nao encontrado',ws_locale_usuario,ws_usuario_systextil) || ')';
   end;

   if inserting
   then
      begin
         select pedi_010.nome_cliente
         into ws_nome_cliente_n
         from pedi_010
         where pedi_010.cgc_9 = :new.cnpj_cliente9
           and pedi_010.cgc_4 = :new.cnpj_cliente4
           and pedi_010.cgc_2 = :new.cnpj_cliente2;
         exception when no_data_found then
            ws_nome_cliente_n  := ' ';
      end;

      INSERT INTO hist_100
         (tabela       ,         operacao,
          data_ocorr   ,         aplicacao,
          usuario_rede ,         maquina_rede,
          num01        ,         num05 ,
          long01
         )
      VALUES
         ( 'TMRP_640',         'I',
           sysdate,            ws_aplicativo,
           ws_usuario_rede,    ws_maquina_rede,
           :new.numero_reserva, 0,
            inter_fn_buscar_tag('lb34882#CADASTRO DE RESERVAS DE PLANEJAMENTO',ws_locale_usuario,ws_usuario_systextil)||chr(10) ||
                                                                                          chr(10) ||
            inter_fn_buscar_tag('lb29657#OPERACAO:',ws_locale_usuario,ws_usuario_systextil) ||
            inter_fn_buscar_tag('lb06326#INCLUSAO',ws_locale_usuario,ws_usuario_systextil) ||
            chr(10) ||
            chr(10) ||
            inter_fn_buscar_tag('lb31252#NUMERO RESERVA:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            to_char(:new.numero_reserva,'000000')            ||chr(10) ||
            inter_fn_buscar_tag('lb17660#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            to_char(:new.cnpj_cliente9,'000000000')          ||'/'     ||
            to_char(:new.cnpj_cliente4,'0000')               ||'-'     ||
            to_char(:new.cnpj_cliente2,'00')                 ||' :: '  ||
            ws_nome_cliente_n                                ||chr(10) ||
            inter_fn_buscar_tag('lb00610#DATA CADASTRO:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            to_char(:new.data_cadastro , 'DD/MM/YYYY')       ||chr(10) ||
            inter_fn_buscar_tag('lb31255#DATA VALIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            to_char(:new.data_validade , 'DD/MM/YYYY')       ||chr(10) ||
            inter_fn_buscar_tag('lb31256#DATA EMBARQUE CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            to_char(:new.data_embarque_cli  , 'DD/MM/YYYY')  ||chr(10) ||
            inter_fn_buscar_tag('lb31257#DATA EMBARQUE PLAN.:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            to_char(:new.data_embarque_plan , 'DD/MM/YYYY')  ||chr(10) ||
            inter_fn_buscar_tag('lb31258#TIPO RESERVA:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            to_char(:new.tipo_reserva,'00')                  ||' :: '  ||
            v_desc_tipo_reserva_n                            ||chr(10) ||
            inter_fn_buscar_tag('lb31259#RESERVA ORIGINAL:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            to_char(:new.reserva_original,'000000')          ||chr(10) ||
            inter_fn_buscar_tag('lb31261#PROJETO ASSOCIADO:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            :new.projeto_associado                           ||chr(10) ||
            inter_fn_buscar_tag('lb27910#COD. CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            to_char(:new.cod_cancelamento,'000')             ||chr(10) ||
            inter_fn_buscar_tag('lb02861#DATA CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            to_char(:new.data_cancelamento , 'DD/MM/YYYY')   ||chr(10) ||
            inter_fn_buscar_tag('lb31284#USUARIO CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            :new.usuario_cancelamento                        ||chr(10) ||
            inter_fn_buscar_tag('lb31285#USUARIO CADASTRO:',ws_locale_usuario,ws_usuario_systextil) || ' '||
            :new.usuario_cadastro
         );
   end if;

   if updating
   then
      begin
         select pedi_010.nome_cliente
         into ws_nome_cliente_n
         from pedi_010
         where pedi_010.cgc_9 = :new.cnpj_cliente9
           and pedi_010.cgc_4 = :new.cnpj_cliente4
           and pedi_010.cgc_2 = :new.cnpj_cliente2;
         exception when no_data_found then
            ws_nome_cliente_n  := ' ';
      end;

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb34882#CADASTRO DE RESERVAS DE PLANEJAMENTO',ws_locale_usuario,ws_usuario_systextil) ||
         chr(10) ||
         chr(10) ||
         inter_fn_buscar_tag('lb29657#OPERACAO:',ws_locale_usuario,ws_usuario_systextil) ||
         inter_fn_buscar_tag('lb21927#ALTERACAO',ws_locale_usuario,ws_usuario_systextil) ||
         chr(10);

       if :new.numero_reserva        <>  :old.numero_reserva
       then
          long_aux := long_aux ||
             inter_fn_buscar_tag('lb31252#NUMERO RESERVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || to_char(:old.numero_reserva,'000000')   || '] >> '
                                        || to_char(:new.numero_reserva,'000000')   || chr(10);
       end if;

       if :new.cnpj_cliente9         <>  :old.cnpj_cliente9
       or :new.cnpj_cliente4         <>  :old.cnpj_cliente4
       or :new.cnpj_cliente2         <>  :old.cnpj_cliente2
       then
           begin
              select nvl(pedi_010.nome_cliente,' ')
              into ws_nome_cliente_o
              from pedi_010
              where pedi_010.cgc_9 = :old.cnpj_cliente9
                and pedi_010.cgc_4 = :old.cnpj_cliente4
                and pedi_010.cgc_2 = :old.cnpj_cliente2;
              exception when no_data_found then
                 ws_nome_cliente_o  := ' ';
           end;

           long_aux := long_aux ||
              inter_fn_buscar_tag('lb17660#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                         || to_char(:old.cnpj_cliente9,'000000000')         ||'/'     ||
                                            to_char(:old.cnpj_cliente4,'0000')              ||'-'     ||
                                            to_char(:old.cnpj_cliente2,'00')                ||' :: '  ||
                                            ws_nome_cliente_o                               ||'  >> ' ||chr(10) ||

                '                     ' || to_char(:new.cnpj_cliente9,'000000000')          ||'/'     ||
                                           to_char(:new.cnpj_cliente4,'0000')               ||'-'     ||
                                           to_char(:new.cnpj_cliente2,'00')                 ||' :: '  ||
                                           ws_nome_cliente_n                                ||chr(10) ;

       end if;

       if :new.data_cadastro         <>  :old.data_cadastro
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb00610#DATA CADASTRO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || to_char(:old.data_cadastro,'DD/MM/YYYY')   || '  >> '
                                          || to_char(:new.data_cadastro,'DD/MM/YYYY')   || chr(10);
       end if;

       if :new.data_validade         <>  :old.data_validade
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb31255#DATA VALIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                         || to_char(:old.data_validade,'DD/MM/YYYY')   || '  >> '
                                         || to_char(:new.data_validade,'DD/MM/YYYY')   || chr(10);
       end if;

       if :new.data_embarque_cli     <>  :old.data_embarque_cli
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb31256#DATA EMBARQUE CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                         || to_char(:old.data_embarque_cli,'DD/MM/YYYY')   || '  >> '
                                         || to_char(:new.data_embarque_cli,'DD/MM/YYYY')   || chr(10);
       end if;

       if :new.data_embarque_plan    <>  :old.data_embarque_plan
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb31257#DATA EMBARQUE PLAN.:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                         || to_char(:old.data_embarque_plan,'DD/MM/YYYY')   || '  >> '
                                         || to_char(:new.data_embarque_plan,'DD/MM/YYYY')   || chr(10);
       end if;

       if :new.tipo_reserva          <>  :old.tipo_reserva
       then
          begin
             select hdoc_001.descricao
             into v_desc_tipo_reserva_o
             from hdoc_001
             where hdoc_001.tipo = 341
               and hdoc_001.codigo = :old.tipo_reserva;
             exception when no_data_found then
               v_desc_tipo_reserva_o := '(' || inter_fn_buscar_tag('lb34881#Nao encontrado',ws_locale_usuario,ws_usuario_systextil) || ')';
          end;

          long_aux := long_aux ||
             inter_fn_buscar_tag('lb31258#TIPO RESERVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || to_char(:old.tipo_reserva,'00')                  ||' :: '  ||
                                          v_desc_tipo_reserva_o                             ||'  >> ' ||
                                          to_char(:new.tipo_reserva,'00')                   ||' :: '  ||
                                          v_desc_tipo_reserva_n                             ||chr(10) ;
       end if;

       if :new.reserva_original      <>  :old.reserva_original
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb31259#RESERVA ORIGINAL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                         || to_char(:old.reserva_original,'000000')   || '  >> '
                                         || to_char(:new.reserva_original,'000000')   || chr(10);
       end if;

       if :new.projeto_associado     <>  :old.projeto_associado
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb31261#PROJETO ASSOCIADO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                         || :old.projeto_associado   || '  >> '
                                         || :new.projeto_associado   || chr(10);
       end if;

       if :new.cod_cancelamento      <>  :old.cod_cancelamento
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb27910#COD. CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                         || to_char(:old.cod_cancelamento,'000')   || '  >> '
                                         || to_char(:new.cod_cancelamento,'000')   || chr(10);
       end if;

       if :new.data_cancelamento     <>  :old.data_cancelamento
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb02861#DATA CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || to_char(:old.data_cancelamento,'DD/MM/YYYY')   || ' >> '
                                        || to_char(:new.data_cancelamento,'DD/MM/YYYY')   || chr(10);
       end if;

       if :new.usuario_cancelamento  <>  :old.usuario_cancelamento
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb31284#USUARIO CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                         || :old.usuario_cancelamento   || '  >> '
                                         || :new.usuario_cancelamento   || chr(10);
       end if;

       if :new.usuario_cadastro      <>  :old.usuario_cadastro
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb31285#USUARIO CADASTRO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                         || :old.usuario_cadastro    || '  >> '
                                         || :new.usuario_cadastro    || chr(10);
       end if;

       if :new.status      <>  :old.status
       then
           long_aux := long_aux ||
              inter_fn_buscar_tag('lb06632#STATUS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                         ||  to_char(:old.status,'00')    || '  >> '
                                         ||  to_char(:new.status,'00')    || chr(10);
       end if;

       INSERT INTO hist_100
          ( tabela,               operacao,
            data_ocorr,           aplicacao,
            usuario_rede,         maquina_rede,
            num01,                num05,
            long01
          )
       VALUES
          ( 'TMRP_640',           'A',
            sysdate,              ws_aplicativo,
            ws_usuario_rede,      ws_maquina_rede,
            :new.numero_reserva,  0,
            long_aux
         );
   end if;

   if deleting
   then
      begin
         select nvl(pedi_010.nome_cliente,' ')
         into ws_nome_cliente_o
         from pedi_010
         where pedi_010.cgc_9 = :old.cnpj_cliente9
           and pedi_010.cgc_4 = :old.cnpj_cliente4
           and pedi_010.cgc_2 = :old.cnpj_cliente2;
      exception when no_data_found then
         ws_nome_cliente_o  := ' ';
      end;

      begin
         select hdoc_001.descricao
         into v_desc_tipo_reserva_o
         from hdoc_001
         where hdoc_001.tipo = 341
           and hdoc_001.codigo = :old.tipo_reserva;
         exception when no_data_found then
            v_desc_tipo_reserva_o := '(' || inter_fn_buscar_tag('lb34881#Nao encontrado',ws_locale_usuario,ws_usuario_systextil) || ')';
      end;

      INSERT INTO hist_100
         ( tabela,             operacao,
           data_ocorr,         aplicacao,
           usuario_rede,       maquina_rede,
           num01,              num05,
           long01
         )
      VALUES
         ( 'TMRP_640',         'O',
           sysdate,            ws_aplicativo,
           ws_usuario_rede,    ws_maquina_rede,
           :old.numero_reserva, 0,
           inter_fn_buscar_tag('lb34882#CADASTRO DE RESERVAS DE PLANEJAMENTO',ws_locale_usuario,ws_usuario_systextil) ||
           chr(10) ||
           chr(10) ||
           inter_fn_buscar_tag('lb29657#OPERACAO:',ws_locale_usuario,ws_usuario_systextil) ||
           inter_fn_buscar_tag('lb21928#EXCLUSAO',ws_locale_usuario,ws_usuario_systextil) ||
           chr(10) ||
           inter_fn_buscar_tag('lb31252#NUMERO RESERVA:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           to_char(:old.numero_reserva,'000000')            ||chr(10) ||
           inter_fn_buscar_tag('lb17660#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           to_char(:old.cnpj_cliente9,'000000000')          ||'/'     ||
           to_char(:old.cnpj_cliente4,'0000')               ||'-'     ||
           to_char(:old.cnpj_cliente2,'00')                 ||' :: '  ||
           ws_nome_cliente_o                                ||chr(10) ||
           inter_fn_buscar_tag('lb00610#DATA CADASTRO:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           to_char(:old.data_cadastro , 'DD/MM/YYYY')       ||chr(10) ||
           inter_fn_buscar_tag('lb31255#DATA VALIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           to_char(:old.data_validade , 'DD/MM/YYYY')       ||chr(10) ||
           inter_fn_buscar_tag('lb31256#DATA EMBARQUE CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           to_char(:old.data_embarque_cli  , 'DD/MM/YYYY')  ||chr(10) ||
           inter_fn_buscar_tag('lb31257#DATA EMBARQUE PLAN.:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           to_char(:old.data_embarque_plan , 'DD/MM/YYYY')  ||chr(10) ||
           inter_fn_buscar_tag('lb31258#TIPO RESERVA:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           to_char(:old.tipo_reserva,'00')                  ||' :: '  ||
           v_desc_tipo_reserva_o                            ||chr(10) ||
           inter_fn_buscar_tag('lb31259#RESERVA ORIGINAL:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           to_char(:old.reserva_original,'000000')          ||chr(10) ||
           inter_fn_buscar_tag('lb31261#PROJETO ASSOCIADO:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           :old.projeto_associado                           ||chr(10) ||
           inter_fn_buscar_tag('lb27910#COD. CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           to_char(:old.cod_cancelamento,'000')             ||chr(10) ||
           inter_fn_buscar_tag('lb02861#DATA CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           to_char(:old.data_cancelamento , 'DD/MM/YYYY')   ||chr(10) ||
           inter_fn_buscar_tag('lb31284#USUARIO CANCELAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           :old.usuario_cancelamento                        ||chr(10) ||
           inter_fn_buscar_tag('lb31285#USUARIO CADASTRO:',ws_locale_usuario,ws_usuario_systextil) || ' '||
           :old.usuario_cadastro
      );
   end if;
end inter_tr_tmrp_640_log;

-- ALTER TRIGGER "INTER_TR_TMRP_640_LOG" ENABLE
 

/

exec inter_pr_recompile;

