alter table PEDI_022 drop constraint PK_pedi_022;
drop index PK_pedi_022;

ALTER TABLE PEDI_022
ADD (CODIGO_INFORMACAO1 NUMBER(9) DEFAULT 9999  not null,
     CODIGO_INFORMACAO2 NUMBER(9) DEFAULT 99999 not null,
     FLAG_VENDA         NUMBER(1) DEFAULT 0);

comment on column PEDI_022.CODIGO_INFORMACAO1 is 'Indica a Linha';
comment on column PEDI_022.CODIGO_INFORMACAO2 is 'Indica a Sub Linha';
comment on column PEDI_022.FLAG_VENDA is 'Indica se considera a venda';

alter table pedi_022 
add constraint PK_pedi_022 primary key (COD_REPRES, TIPO_INFORMACAO, CODIGO_INFORMACAO, CODIGO_INFORMACAO1,CODIGO_INFORMACAO2);   
exec inter_pr_recompile;
