create or replace trigger inter_tr_pcpc_045_1
     before insert
         or delete
         or update of qtde_produzida, qtde_pecas_2a, qtde_conserto, qtde_perdas
     on pcpc_045
     for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_executa_trigger                  number;
   v_seq_operacao                     pcpc_040.seq_operacao%type;
   v_sequencia_estagio                pcpc_040.sequencia_estagio%type;
   v_prox_seq_operacao                pcpc_040.seq_operacao%type;
   v_prox_sequencia_estagio           pcpc_040.sequencia_estagio%type;
   v_sequencia_estagio_dep            pcpc_040.sequencia_estagio%type;
   v_ordem_producao                   pcpc_040.ordem_producao%type;
   v_qtde_produzida                   pcpc_045.qtde_produzida%type;
   v_qtde_conserto                    pcpc_045.qtde_conserto%type;
   v_qtde_perdas                      pcpc_045.qtde_perdas%type;
   v_qtde_pecas_2a                    pcpc_045.qtde_pecas_2a%type;
   v_atu_estagios_seguintes           fatu_502.atu_estagios_seguintes%type;
   v_deposito_conserto                fatu_501.deposito_conserto%type;
   v_codigo_estagio_depende           pcpc_040.estagio_depende%type;
   v_periodo_producao                 pcpc_045.pcpc040_perconf%type;
   v_ordem_confeccao                  pcpc_045.pcpc040_ordconf%type;
   v_codigo_estagio                   pcpc_045.pcpc040_estconf%type;
   v_qt_pc_avanca_seq_old             pcpc_040.qtde_pecas_prod%type;
   v_qt_pc_avanca_seq_new             pcpc_040.qtde_pecas_prod%type;
   v_qt_pc_avanca_seq_sld             pcpc_040.qtde_pecas_prod%type;
   v_processo_systextil               pcpc_045.processo_systextil%type;
   v_atz_em_prod                      pcpc_045.atz_em_prod%type;
   v_atz_a_prod                       pcpc_045.atz_a_prod%type;
   v_atz_pode_produzir                pcpc_045.atz_pode_produzir%type;
   v_solicitacao_conserto			     pcpc_045.solicitacao_conserto%type;

   v_estagio_corte                    pcpc_040.codigo_estagio%type;
   v_seq_operacao_alt_programado      pcpc_040.seq_operacao%type;
   v_processo_systextil_guarda        pcpc_045.processo_systextil%type;
   
   v_tem_registro                     number;
   v_tem_reg_400                      number;
   v_atz_conserto                     number;
   v_tem_seq_estagio                  number;
   e_estagio_silmutaneo               number;
   e_prox_estagio_silmutaneo          number;
   v_qtde_perda_avanca_old            number;
   v_qtde_perda_avanca_new            number;
   v_qtde_perda_avanca_sld            number;
   v_codigo_empresa                   number;
   v_proconf_grupo                    varchar2(5);
   v_qtde_a_produzir                  number;
   
   procedure atualiza_estagios_dependentes(v_estagio_dependido      in number, 
                                           v_periodo_producao       in number, 
                                           v_ordem_confeccao        in number, 
                                           v_ordem_producao         in number,
                                           v_sequencia_estagio      in number,
                                           v_atu_estagios_seguintes in number) 
   is
     v_houve_estagios number;
   begin
       
      v_houve_estagios := 1; 
        
      begin
         update pcpc_040
         set   pcpc_040.executa_trigger         = 3,
               pcpc_040.qtde_a_produzir_pacote  = pcpc_040.qtde_a_produzir_pacote -
                                                  decode(v_atz_a_prod,        0,
                                                                              v_qtde_perdas +
                                                                              decode(v_atu_estagios_seguintes,0,v_qtde_pecas_2a,0),
                                                                              0)
         where pcpc_040.periodo_producao = v_periodo_producao
           and pcpc_040.ordem_confeccao  = v_ordem_confeccao
           and pcpc_040.ordem_producao   = v_ordem_producao -- Neylor - Pette.
           and pcpc_040.codigo_estagio   in (select pcpc_040.codigo_estagio
                                            from pcpc_040
                                            where pcpc_040.ordem_producao    = v_ordem_producao
                                              and pcpc_040.periodo_producao  = v_periodo_producao
                                              and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                                              and pcpc_040.sequencia_estagio = v_sequencia_estagio
                                              and pcpc_040.estagio_depende   = v_estagio_dependido);
      exception 
         when no_data_found then
            v_houve_estagios := 0;
         when others then
            -- ATENC?O! N?o atualizou registro na tabela {0}. Status: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(992)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
     
      if v_houve_estagios = 1
      then 
         for estagios in (select pcpc_040.codigo_estagio codigo
                          from pcpc_040
                          where pcpc_040.ordem_producao    = v_ordem_producao
                            and pcpc_040.periodo_producao  = v_periodo_producao
                            and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                            and pcpc_040.sequencia_estagio = v_sequencia_estagio
                            and pcpc_040.estagio_depende   = v_estagio_dependido)
         loop
            atualiza_estagios_dependentes(estagios.codigo, 
                                          v_periodo_producao,
                                          v_ordem_confeccao,
                                          v_ordem_producao,
                                          v_sequencia_estagio,
                                          v_atu_estagios_seguintes);
         end loop;
      end if;      

   end atualiza_estagios_dependentes;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementac?o executada para limpeza da base de dados do cliente
   -- e replicac?o dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :new.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1
      or :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :old.executa_trigger = 3
         or :new.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :old.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_qtde_produzida       := 0;
   v_qtde_pecas_2a        := 0;
   v_qtde_conserto        := 0;
   v_qtde_perdas          := 0;
   v_periodo_producao     := 0;
   v_ordem_confeccao      := 0;
   v_codigo_estagio       := 0;
   v_qtde_a_produzir      := 0;
   v_solicitacao_conserto := 0;
   v_processo_systextil   := '';
   v_processo_systextil_guarda   := '';
   
   v_processo_systextil := inter_fn_nome_programa(ws_sid);
   v_processo_systextil_guarda   := v_processo_systextil;
   
   if upper(v_processo_systextil) = 'PLSQLDEV.EXE'
   then
      v_processo_systextil := 'pcpc_f144';
   end if;

       -- Neylor - Pettenati
       -- Se o programa for o "baca_f001" vamos nomear para pcpc_f045
       -- para que o programa execute os apontamentos necessários.
       if v_processo_systextil = 'BACA_F001'
       or v_processo_systextil = 'baca_f001'
       then 
          v_processo_systextil := 'pcpc_f045';
          v_processo_systextil_guarda := 'pcpc_f045';
       end if;   

   if inserting
   then
      v_periodo_producao   := :new.pcpc040_perconf;
      v_ordem_confeccao    := :new.pcpc040_ordconf;
      v_codigo_estagio     := :new.pcpc040_estconf;
      v_ordem_producao     := :new.ordem_producao;
      v_atz_em_prod        := nvl(:new.atz_em_prod,0);
      v_atz_a_prod         := nvl(:new.atz_a_prod,0);
      v_atz_pode_produzir  := nvl(:new.atz_pode_produzir,0);
      v_solicitacao_conserto  := :new.solicitacao_conserto;

      if :new.processo_systextil <> 'pcpc_f235'
      or :new.processo_systextil is null
      then
         :new.processo_systextil := v_processo_systextil_guarda;
         :new.usuario_systextil  := ws_usuario_systextil;
      end if;

      if :new.qtde_produzida <> 0
      then
         v_qtde_produzida := :new.qtde_produzida;
      end if;

      if :new.qtde_pecas_2a <> 0
      then
         v_qtde_pecas_2a := :new.qtde_pecas_2a;
      end if;

      if :new.qtde_conserto <> 0
      then
         v_qtde_conserto  := :new.qtde_conserto;
      end if;

      if :new.qtde_perdas <> 0
      then
         v_qtde_perdas := :new.qtde_perdas;
      end if;
   end if;

   if updating
   and v_processo_systextil <> 'pcpc_f355'
   and v_processo_systextil <> 'pcpc_f144'
   and v_processo_systextil <> 'tmrp_f070'
   and v_processo_systextil <> 'pcpc_f350'
   and v_processo_systextil <> 'pcpc_f052'
   then
      v_periodo_producao   := :new.pcpc040_perconf;
      v_ordem_confeccao    := :new.pcpc040_ordconf;
      v_codigo_estagio     := :new.pcpc040_estconf;
      v_ordem_producao     := :new.ordem_producao;
      v_atz_em_prod        := nvl(:new.atz_em_prod,0);
      v_atz_a_prod         := nvl(:new.atz_a_prod,0);
      v_atz_pode_produzir  := nvl(:new.atz_pode_produzir,0);
      v_solicitacao_conserto := :new.solicitacao_conserto;

      if  :new.qtde_produzida <> 0
      and :old.qtde_produzida <> :new.qtde_produzida
      then
         v_qtde_produzida := :new.qtde_produzida - :old.qtde_produzida;
      end if;

      if  :new.qtde_pecas_2a <> 0
      and :old.qtde_pecas_2a <> :new.qtde_pecas_2a
      then
         v_qtde_pecas_2a := :new.qtde_pecas_2a - :old.qtde_pecas_2a;
      end if;

      if  :new.qtde_conserto <> 0
      and :old.qtde_conserto <> :new.qtde_conserto
      then
         v_qtde_conserto  := :new.qtde_conserto - :old.qtde_conserto;
      end if;

      if  :new.qtde_perdas <> 0
      and :old.qtde_perdas <> :new.qtde_perdas
      then
         v_qtde_perdas := :new.qtde_perdas - :old.qtde_perdas;
      end if;
   end if;

   if updating
   and (v_processo_systextil = 'pcpc_f355'
   or   v_processo_systextil = 'pcpc_f144'
   or   v_processo_systextil = 'tmrp_f070'
   or   v_processo_systextil = 'pcpc_f350'
   or   v_processo_systextil = 'pcpc_f052')
   then
      v_periodo_producao   := :new.pcpc040_perconf;
      v_ordem_confeccao    := :new.pcpc040_ordconf;
      v_codigo_estagio     := :new.pcpc040_estconf;
      v_ordem_producao     := :new.ordem_producao;
      v_atz_em_prod        := nvl(:new.atz_em_prod,0);
      v_atz_a_prod         := nvl(:new.atz_a_prod,0);
      v_atz_pode_produzir  := nvl(:new.atz_pode_produzir,0);

      if  :new.qtde_produzida <> 0
      then
         v_qtde_produzida := :new.qtde_produzida;
      end if;

      if  :new.qtde_pecas_2a <> 0
      then
         v_qtde_pecas_2a := :new.qtde_pecas_2a;
      end if;

      if  :new.qtde_conserto <> 0
      then
         v_qtde_conserto  := :new.qtde_conserto;
      end if;

      if  :new.qtde_perdas <> 0
      then
         v_qtde_perdas := :new.qtde_perdas;
      end if;
   end if;

   if deleting
   then
      v_periodo_producao   := :old.pcpc040_perconf;
      v_ordem_confeccao    := :old.pcpc040_ordconf;
      v_codigo_estagio     := :old.pcpc040_estconf;
      v_ordem_producao     := :old.ordem_producao;
      v_atz_em_prod        := nvl(:old.atz_em_prod,0);
      v_atz_a_prod         := nvl(:old.atz_a_prod,0);
      v_atz_pode_produzir  := nvl(:old.atz_pode_produzir,0);

      if :old.qtde_produzida <> 0
      then
         v_qtde_produzida := (:old.qtde_produzida * -1);
      end if;
       if :old.qtde_pecas_2a <> 0
      then
         v_qtde_pecas_2a := (:old.qtde_pecas_2a * -1);
      end if;

      if :old.qtde_conserto <> 0
      then
         v_qtde_conserto  := (:old.qtde_conserto * -1);
      end if;

      if :old.qtde_perdas <> 0
      then
         v_qtde_perdas := (:old.qtde_perdas * -1);
      end if;
   end if;

   v_tem_reg_400 := nvl(inter_fn_executou_recalculo(),0);

   if (inserting
   or  deleting
   or  updating)
   and v_executa_trigger     <> 1
   and (v_tem_reg_400        <> 0
   or   v_processo_systextil = 'pcpc_f144'
   or   v_processo_systextil = 'tmrp_f070'
   or   v_processo_systextil = 'pcpc_f350'
   or   v_processo_systextil = 'pcpc_f052')
   then
      begin
         select nvl(count(1),0)
         into   v_tem_registro
         from pcpc_040
         where pcpc_040.periodo_producao  = v_periodo_producao
           and pcpc_040.ordem_confeccao   = v_ordem_confeccao
           and pcpc_040.ordem_producao    = v_ordem_producao -- Neylor - Pette.
           and pcpc_040.codigo_estagio    = v_codigo_estagio
           and pcpc_040.ordem_producao    = v_ordem_producao;
      exception when others then
         raise_application_error(-20000,'ATEN��O! Existe duas ordens de produ��o para os apontamentos de produ��o do mesmo pacote de produ��o. O processo ser� abortado!');
      end;

      if v_tem_registro = 0
      then
         raise_application_error(-20000,'ATEN��O! Existe duas ordens de produ��o para os apontamentos de produ��o do mesmo pacote de produ��o. O processo ser� abortado!');
      end if;

      begin
         select nvl(basi_030.estagio_altera_programado,0)
         into   v_estagio_corte
         from basi_030, pcpc_020
         where pcpc_020.ordem_producao = v_ordem_producao
           and basi_030.nivel_estrutura = '1'
           and basi_030.referencia      = pcpc_020.referencia_peca;
      exception when OTHERS then
         v_estagio_corte := 0;
      end;

      if v_estagio_corte is null
      or v_estagio_corte = 0
      then
         begin
            select nvl(empr_001.estagio_corte,0)
            into   v_estagio_corte
            from empr_001;
         exception when OTHERS then
            v_estagio_corte := 0;
         end;
      end if;

      v_atz_conserto := 0; -- N?o desconta a quantidade apontada em conserto nas colunas em produc?o e a produzir

      begin
         select nvl(fatu_501.deposito_conserto,0)
         into   v_deposito_conserto
         from fatu_501, pcpc_010
         where fatu_501.codigo_empresa   =  pcpc_010.codigo_empresa
           and pcpc_010.area_periodo     = 1
           and pcpc_010.periodo_producao = v_periodo_producao;
      exception when OTHERS then
         v_deposito_conserto := 0;
      end;

      v_tem_registro:= 0;

      begin
         select nvl(count(*),0)
         into   v_tem_registro
         from pcpc_020
         where pcpc_020.ordem_producao = v_ordem_producao
           and pcpc_020.ultimo_estagio = v_codigo_estagio;
      exception when OTHERS then
         v_tem_registro:= 0;
      end;

      if  v_tem_registro            > 0
      and v_deposito_conserto       > 0
      then
         v_atz_conserto := 1; -- Desconta ou soma a quantidade apontada em conserto nas colunas em produc?o e a produzir
      end if;

      begin
         select nvl(count(*),0)
         into   v_tem_registro
         from pcpc_888_det
         where pcpc_888_det.ordem_producao     = v_ordem_producao
           and pcpc_888_det.cod_est_cons       = v_codigo_estagio
           and pcpc_888_det.ordem_confeccao    = v_ordem_confeccao
           and pcpc_888_det.numero_solicitacao <> 0;
      exception when OTHERS then
         v_tem_registro := 0;
      end;

      /*
            AS OPC?ES S?O:
                0 - N?o atualiza - Somente atualizara o proximo estagio se as quantidades forem lancadas como
                    primeira qualidade
                1 - Atualiza - Atualiza para o proximo estagio a quantidade lancada independente da qualidade.
      */
      begin
         select fatu_502.atu_estagios_seguintes
         into   v_atu_estagios_seguintes
         from fatu_502, pcpc_010
         where fatu_502.codigo_empresa   =  pcpc_010.codigo_empresa
           and pcpc_010.area_periodo     = 1
           and pcpc_010.periodo_producao = v_periodo_producao;
      exception when OTHERS then
         v_atu_estagios_seguintes := 0;
      end;

      -- Qual a seq. de operac?o do estagio atual
      begin
         select pcpc_040.seq_operacao
         into  v_seq_operacao
         from pcpc_040
         where pcpc_040.periodo_producao = v_periodo_producao
           and pcpc_040.ordem_confeccao  = v_ordem_confeccao
           and pcpc_040.ordem_producao   = v_ordem_producao -- Neylor - Pette.
           and pcpc_040.codigo_estagio   = v_codigo_estagio;
      exception when OTHERS then
         v_seq_operacao := 0;
      end;

      -- Verifica se tem ou n?o seq. de estagio
      -- pcpc_040.qtde_pecas_prod <> 0 tem seq. de estagio
      -- pcpc_040.qtde_pecas_prod =  0 n?o tem seq. de estagio
      begin
         select nvl(sum(pcpc_040.sequencia_estagio),0)
         into  v_tem_seq_estagio
         from pcpc_040
         where pcpc_040.periodo_producao = v_periodo_producao
           and pcpc_040.ordem_confeccao  = v_ordem_confeccao
           and pcpc_040.ordem_producao   = v_ordem_producao; -- Neylor - Pette.
      exception when OTHERS then
         v_sequencia_estagio := 0;
      end;

      -- Qual a seq. de estagio do estagio atual
      begin
         select pcpc_040.sequencia_estagio
         into  v_sequencia_estagio
         from pcpc_040
         where pcpc_040.periodo_producao = v_periodo_producao
           and pcpc_040.ordem_confeccao  = v_ordem_confeccao
           and pcpc_040.ordem_producao   = v_ordem_producao -- Neylor - Pette.
           and pcpc_040.codigo_estagio   = v_codigo_estagio;
      exception when OTHERS then
         v_sequencia_estagio := 0;
      end;

      -- Qual a proxima seq. de operac?o que vem apos o estagio atual
      if v_tem_seq_estagio <> 0
      then
         begin
            select nvl(min(pcpc_040.seq_operacao),0)
            into  v_prox_seq_operacao
            from pcpc_040
            where pcpc_040.periodo_producao  =  v_periodo_producao
              and pcpc_040.ordem_confeccao   =  v_ordem_confeccao
              and pcpc_040.ordem_producao    = v_ordem_producao -- Neylor - Pette.
              and pcpc_040.sequencia_estagio <> v_sequencia_estagio
              and pcpc_040.seq_operacao      >  v_seq_operacao;
         exception when OTHERS then
            v_prox_seq_operacao := 0;
         end;
      else
         begin
            select nvl(min(pcpc_040.seq_operacao),0)
            into  v_prox_seq_operacao
            from pcpc_040
            where pcpc_040.periodo_producao  =  v_periodo_producao
              and pcpc_040.ordem_confeccao   =  v_ordem_confeccao
              and pcpc_040.ordem_producao    = v_ordem_producao -- Neylor - Pette.
              and pcpc_040.seq_operacao      >  v_seq_operacao;
         exception when OTHERS then
            v_prox_seq_operacao := 0;
         end;
      end if;

      -- Qual a proxima seq. de estagio que vem apos o estagio atual
      begin
         select pcpc_040.sequencia_estagio
         into  v_prox_sequencia_estagio
         from pcpc_040
         where pcpc_040.periodo_producao = v_periodo_producao
           and pcpc_040.ordem_confeccao  = v_ordem_confeccao
           and pcpc_040.ordem_producao   = v_ordem_producao -- Neylor - Pette.
           and pcpc_040.seq_operacao     = v_prox_seq_operacao;
      exception when OTHERS then
         v_prox_sequencia_estagio := 0;
      end;

      -- So verifica se tem algum estagio que depende do estagio atual?
      begin
         select nvl(min(pcpc_040.estagio_depende),0)
         into  v_codigo_estagio_depende
         from pcpc_040
         where pcpc_040.periodo_producao = v_periodo_producao
           and pcpc_040.ordem_confeccao  = v_ordem_confeccao
           and pcpc_040.ordem_producao   = v_ordem_producao -- Neylor - Pette.
           and pcpc_040.codigo_estagio   = v_codigo_estagio;
      exception when OTHERS then
         v_codigo_estagio_depende := 0;
      end;

      -- Verifica se o estagio atual e simultaneo
      if v_tem_seq_estagio <> 0
      then
         begin
            select nvl(count(*),1)
            into  e_estagio_silmutaneo
            from pcpc_040
            where pcpc_040.periodo_producao  = v_periodo_producao
              and pcpc_040.ordem_confeccao   = v_ordem_confeccao
              and pcpc_040.ordem_producao    = v_ordem_producao -- Neylor - Pette.
              and pcpc_040.sequencia_estagio = v_sequencia_estagio;
         exception when OTHERS then
           e_estagio_silmutaneo := 1;
         end;
      else
         e_estagio_silmutaneo := 1;
      end if;

       -- Verifica se o proximo estagio e simultaneo
      if v_tem_seq_estagio <> 0
      then
         begin
            select nvl(count(*),1)
            into  e_prox_estagio_silmutaneo
            from pcpc_040
            where pcpc_040.periodo_producao  = v_periodo_producao
              and pcpc_040.ordem_confeccao   = v_ordem_confeccao
              and pcpc_040.ordem_producao    = v_ordem_producao -- Neylor - Pette.
              and pcpc_040.sequencia_estagio = v_prox_sequencia_estagio;
         exception when OTHERS then
           e_prox_estagio_silmutaneo := 1;
         end;
      else
         e_prox_estagio_silmutaneo := 1;
      end if;

      -- buscar a menor quantidade de pecas da seq. de estagio atual, conforme parametro de empresa, se soma ou n?o segunda qualidade.
      v_qt_pc_avanca_seq_old     := 0;
      v_qt_pc_avanca_seq_new     := 0;
      v_qt_pc_avanca_seq_sld     := 0;

      -- Qtde de perda que avanca do estagio
      inter_pr_calcula_perda (v_qtde_perda_avanca_old,
                              v_periodo_producao,
                              v_ordem_confeccao,
                              v_ordem_producao,
                              v_sequencia_estagio,
                              v_codigo_estagio);

      -- O que pode avancar para a proxima seq. de estagio
      inter_pr_calcula_qtde_avanca (v_qt_pc_avanca_seq_old,
                                    v_periodo_producao,
                                    v_ordem_confeccao,
                                    v_ordem_producao,
                                    v_sequencia_estagio,
                                    v_codigo_estagio);

      -- Atualizar as quantidades do estagio atual
      begin
         update pcpc_040
         set   pcpc_040.executa_trigger         = 3,
               pcpc_040.qtde_pecas_prod         = pcpc_040.qtde_pecas_prod         + v_qtde_produzida,
               pcpc_040.qtde_pecas_2a           = pcpc_040.qtde_pecas_2a           + v_qtde_pecas_2a,
               pcpc_040.qtde_perdas             = pcpc_040.qtde_perdas             + v_qtde_perdas,
               pcpc_040.qtde_conserto           = pcpc_040.qtde_conserto           + v_qtde_conserto,
               pcpc_040.qtde_em_producao_pacote = (pcpc_040.qtde_em_producao_pacote -
                                                   decode(v_atz_em_prod,          0,(v_qtde_produzida +
                                                                                     v_qtde_pecas_2a +
                                                                                     v_qtde_perdas +
                                                                                     decode(v_atz_conserto,1,v_qtde_conserto,0)),
                                                                                  1,(v_qtde_produzida +
                                                                                     v_qtde_pecas_2a +
                                                                                     v_qtde_perdas +
                                                                                     decode(v_atz_conserto,1,v_qtde_conserto,0)) * -1,
                                                                                  2,(v_qtde_produzida +
                                                                                     v_qtde_pecas_2a +
                                                                                     v_qtde_perdas +
                                                                                     v_qtde_conserto),
                                                                                  3,(v_qtde_produzida +
                                                                                     v_qtde_pecas_2a +
                                                                                     v_qtde_perdas +
                                                                                     v_qtde_conserto) * -1,
                                                                                  4,0,
                                                                                  5,0,
                                                                                  0)),
               pcpc_040.qtde_disponivel_baixa   = (pcpc_040.qtde_disponivel_baixa -
                                                   decode(v_atz_pode_produzir,   0,(v_qtde_produzida +
                                                                                    v_qtde_pecas_2a +
                                                                                    v_qtde_perdas +
                                                                                    v_qtde_conserto),
                                                                                 2, (v_qtde_produzida +
                                                                                    v_qtde_pecas_2a +
                                                                                    v_qtde_perdas +
                                                                                    v_qtde_conserto),
                                                                                    0)),
               pcpc_040.qtde_a_produzir_pacote  = ((pcpc_040.qtde_a_produzir_pacote  -
                                                    decode(v_atz_a_prod,           0,(v_qtde_produzida +
                                                                                      v_qtde_pecas_2a +
                                                                                      v_qtde_perdas  +
                                                                                      decode(v_atz_conserto,1,v_qtde_conserto,0)),
                                                                                   1,(v_qtde_produzida +
                                                                                      v_qtde_pecas_2a +
                                                                                      v_qtde_perdas  +
                                                                                      decode(v_atz_conserto,1,v_qtde_conserto,0)) * -1,
                                                                                   2,(v_qtde_produzida +
                                                                                      v_qtde_pecas_2a +
                                                                                      v_qtde_perdas  +
                                                                                      v_qtde_conserto),
                                                                                   3,(v_qtde_produzida +
                                                                                      v_qtde_pecas_2a +
                                                                                      v_qtde_perdas  +
                                                                                      v_qtde_conserto) * -1,
                                                                                   0)))
         where pcpc_040.periodo_producao        = v_periodo_producao
           and pcpc_040.ordem_confeccao         = v_ordem_confeccao
           and pcpc_040.ordem_producao          = v_ordem_producao -- Neylor - Pette.
           and pcpc_040.codigo_estagio          = v_codigo_estagio;
      exception when others then
         -- ATENC?O! N?o atualizou registro na tabela {0}. Status: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(1)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      --
      -- Procedure para guardar a quantidade original
      --
      inter_pr_armazena_qtde_orig_op (v_ordem_producao);

      -- Qtde de perda que avanca do estagio
      inter_pr_calcula_perda (v_qtde_perda_avanca_new,
                              v_periodo_producao,
                              v_ordem_confeccao,
                              v_ordem_producao,
                              v_sequencia_estagio,
                              v_codigo_estagio);

      -- O que pode avancar para a proxima seq. de estagio
      inter_pr_calcula_qtde_avanca (v_qt_pc_avanca_seq_new,
                                    v_periodo_producao,
                                    v_ordem_confeccao,
                                    v_ordem_producao,
                                    v_sequencia_estagio,
                                    v_codigo_estagio);

      v_qtde_perda_avanca_sld := 0;

      if v_qtde_perda_avanca_old <> v_qtde_perda_avanca_new
      then
         v_qtde_perda_avanca_sld := v_qtde_perda_avanca_new - v_qtde_perda_avanca_old;
      end if;


      v_qt_pc_avanca_seq_sld := 0;
      if  v_qt_pc_avanca_seq_old <> v_qt_pc_avanca_seq_new
      then
         v_qt_pc_avanca_seq_sld := v_qt_pc_avanca_seq_new - v_qt_pc_avanca_seq_old;
      end if;

      if  v_tem_seq_estagio   <> 0
      then
         if v_sequencia_estagio <> v_prox_sequencia_estagio
         then
            -- Verifica se o em produc?o ou o produzir est?o negativos
            -- Se est?o negativos n?o e o apontamento que esta sendo feito e igual ao negativo n?o avanca estagios
            begin
               update pcpc_040
               set   pcpc_040.executa_trigger         = 3,
                     pcpc_040.qtde_em_producao_pacote = pcpc_040.qtde_em_producao_pacote +
                                                        decode(v_atz_em_prod,0,v_qt_pc_avanca_seq_sld,0) +
                                                        decode(v_atz_em_prod,5,v_qt_pc_avanca_seq_sld,0),
                     pcpc_040.qtde_disponivel_baixa   = pcpc_040.qtde_disponivel_baixa +
                                                        decode(v_atz_pode_produzir,0,v_qt_pc_avanca_seq_sld,
                                                                                   2,v_qt_pc_avanca_seq_sld,
                                                                                   0)
               where pcpc_040.periodo_producao        = v_periodo_producao
                 and pcpc_040.ordem_confeccao         = v_ordem_confeccao
                 and pcpc_040.ordem_producao          = v_ordem_producao -- Neylor - Pette
                 and pcpc_040.sequencia_estagio       = v_prox_sequencia_estagio
                 and (e_prox_estagio_silmutaneo       = 1
                 or   (e_prox_estagio_silmutaneo      > 1
                 and   pcpc_040.estagio_depende       = 0));
            exception when others then
               -- ATENC?O! N?o atualizou registro na tabela {0}. Status: {1}
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(3)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;

            -- Verifica se na proxima seq. de estagio tem estagios depende
            -- Se estes estagios depende est?o na mesma seq., se n?o est?o devem ser atz com a qtde
            for reg_qtde_b1 in (select pcpc_040.codigo_estagio, pcpc_040.estagio_depende
                                from pcpc_040
                                where pcpc_040.ordem_producao    = v_ordem_producao
                                  and pcpc_040.periodo_producao  = v_periodo_producao
                                  and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                                  and pcpc_040.sequencia_estagio = v_prox_sequencia_estagio
                                  and e_prox_estagio_silmutaneo  > 1
                                  and pcpc_040.estagio_depende   <> 0)
            loop
               begin
                  select pcpc_040.sequencia_estagio
                  into v_sequencia_estagio_dep
                  from pcpc_040
                  where pcpc_040.ordem_producao   = v_ordem_producao
                    and pcpc_040.periodo_producao = v_periodo_producao
                    and pcpc_040.ordem_confeccao  = v_ordem_confeccao
                    and pcpc_040.codigo_estagio   = reg_qtde_b1.estagio_depende;
               exception when others then
                  v_sequencia_estagio_dep := 0;
               end;

               if v_sequencia_estagio_dep <> v_prox_sequencia_estagio
               then
                  begin
                     update pcpc_040
                     set   pcpc_040.executa_trigger         = 3,
                           pcpc_040.qtde_em_producao_pacote = pcpc_040.qtde_em_producao_pacote +
                                                              decode(v_atz_em_prod,0,v_qt_pc_avanca_seq_sld,0) +
                                                              decode(v_atz_em_prod,5,v_qt_pc_avanca_seq_sld,0),
                           pcpc_040.qtde_disponivel_baixa   = pcpc_040.qtde_disponivel_baixa +
                                                              decode(v_atz_pode_produzir,0,v_qt_pc_avanca_seq_sld,
                                                                                         2,v_qt_pc_avanca_seq_sld,
                                                                                         0)
                     where pcpc_040.periodo_producao        = v_periodo_producao
                       and pcpc_040.ordem_confeccao         = v_ordem_confeccao
                       and pcpc_040.ordem_producao          = v_ordem_producao -- Neylor - Pette.
                       and pcpc_040.codigo_estagio          = reg_qtde_b1.codigo_estagio;
                  exception when others then
                     -- ATENC?O! N?o atualizou registro na tabela {0}. Status: {1}
                     raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(3)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               end if;
            end loop;
         end if;
      else
         if v_seq_operacao <> v_prox_seq_operacao
         then
            -- Verifica se o em produc?o ou o produzir est?o negativos
            -- Se est?o negativos n?o e o apontamento que esta sendo feito e igual ao negativo n?o avanca estagios
            begin
               update pcpc_040
               set   pcpc_040.executa_trigger         = 3,
                     pcpc_040.qtde_em_producao_pacote = pcpc_040.qtde_em_producao_pacote +
                                                        decode(v_atz_em_prod,0,v_qt_pc_avanca_seq_sld,0)+
                                                        decode(v_atz_em_prod,5,v_qt_pc_avanca_seq_sld,0),
                     pcpc_040.qtde_disponivel_baixa   = pcpc_040.qtde_disponivel_baixa +
                                                        decode(v_atz_pode_produzir,0,v_qt_pc_avanca_seq_sld,
                                                                                   2,v_qt_pc_avanca_seq_sld,
                                                                                   0)
               where pcpc_040.periodo_producao        = v_periodo_producao
                 and pcpc_040.ordem_confeccao         = v_ordem_confeccao
                 and pcpc_040.ordem_producao          = v_ordem_producao -- Neylor - Pette.
                 and pcpc_040.seq_operacao            = v_prox_seq_operacao;
            exception when others then
               -- ATENC?O! N?o atualizou registro na tabela {0}. Status: {1}
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(3)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end if;
      end if;

      -- So verifica se tem algum estagio que depende do estagio atual?
      if v_tem_seq_estagio     <> 0
      and e_estagio_silmutaneo >  1
      then
         begin
            select nvl(min(pcpc_040.codigo_estagio),0)
            into  v_codigo_estagio_depende
            from pcpc_040
            where pcpc_040.periodo_producao  =  v_periodo_producao
              and pcpc_040.ordem_confeccao   =  v_ordem_confeccao
              and pcpc_040.ordem_producao    = v_ordem_producao -- Neylor - Pette.
              and pcpc_040.estagio_depende   =  v_codigo_estagio
              and pcpc_040.sequencia_estagio =  v_sequencia_estagio;
         exception when OTHERS then
            v_codigo_estagio_depende := 0;
         end;

         if v_codigo_estagio_depende <> 0
         then
            if v_atz_em_prod = 5
            then
               v_atz_em_prod := 0;
            end if;
            
            begin
               update pcpc_040
               set   pcpc_040.executa_trigger         = 3,
                     pcpc_040.qtde_em_producao_pacote = pcpc_040.qtde_em_producao_pacote +
                                                        decode(v_atz_em_prod,0,v_qtde_produzida,0) +
                                                        decode(v_atu_estagios_seguintes,0,0,v_qtde_pecas_2a),
                     pcpc_040.qtde_disponivel_baixa   = pcpc_040.qtde_disponivel_baixa +
                                                        decode(v_atz_pode_produzir,0,v_qtde_produzida,
                                                                                   2,v_qtde_produzida,
                                                                                   0)  +
                                                        decode(v_atu_estagios_seguintes,0,0,v_qtde_pecas_2a),
                     pcpc_040.qtde_a_produzir_pacote  = pcpc_040.qtde_a_produzir_pacote -
                                                        decode(v_atz_a_prod,0,
                                                                            v_qtde_perdas +
                                                                            decode(v_atu_estagios_seguintes,0,v_qtde_pecas_2a,0),
                                                                            0)
               where pcpc_040.periodo_producao        = v_periodo_producao
                 and pcpc_040.ordem_confeccao         = v_ordem_confeccao
                 and pcpc_040.ordem_producao          = v_ordem_producao -- Neylor - Pette.
                 and pcpc_040.sequencia_estagio       = v_sequencia_estagio
                 and pcpc_040.estagio_depende         = v_codigo_estagio;
            exception when others then
               -- ATENC?O! N?o atualizou registro na tabela {0}. Status: {1}
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(4)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;

            for estagios in (select pcpc_040.codigo_estagio codigo
                                from pcpc_040
                                where pcpc_040.ordem_producao    = v_ordem_producao
                                  and pcpc_040.periodo_producao  = v_periodo_producao
                                  and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                                  and pcpc_040.sequencia_estagio = v_sequencia_estagio
                                  and pcpc_040.estagio_depende   = v_codigo_estagio)
            loop
               atualiza_estagios_dependentes(estagios.codigo, 
                                             v_periodo_producao,
                                             v_ordem_confeccao,
                                             v_ordem_producao,
                                             v_sequencia_estagio,
                                             v_atu_estagios_seguintes);
            end loop;
         end if;
      end if;

      if v_tem_seq_estagio <> 0
      then
         if v_sequencia_estagio       <> v_prox_sequencia_estagio
         and v_prox_sequencia_estagio <> 0
         then
            begin
               update pcpc_040
               set   pcpc_040.executa_trigger         = 3,
                     pcpc_040.qtde_a_produzir_pacote  = pcpc_040.qtde_a_produzir_pacote -
                                                        decode(v_atz_a_prod,0,v_qtde_perda_avanca_sld,0)
               where pcpc_040.periodo_producao        = v_periodo_producao
                 and pcpc_040.ordem_confeccao         = v_ordem_confeccao
                 and pcpc_040.ordem_producao          = v_ordem_producao -- Neylor - Pette.
                 and pcpc_040.sequencia_estagio       <> v_sequencia_estagio
                 and pcpc_040.seq_operacao            > v_seq_operacao;
            exception when others then
               -- ATENC?O! N?o atualizou registro na tabela {0}. Status: {1}
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(9)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end if;
      else
         if v_seq_operacao       <> v_prox_seq_operacao
         and v_prox_seq_operacao <> 0
         then
            begin
               update pcpc_040
               set   pcpc_040.executa_trigger         = 3,
                     pcpc_040.qtde_a_produzir_pacote  = pcpc_040.qtde_a_produzir_pacote -
                                                        decode(v_atz_a_prod,0,v_qtde_perda_avanca_sld,0)
               where pcpc_040.periodo_producao        = v_periodo_producao
                 and pcpc_040.ordem_confeccao         = v_ordem_confeccao
                 and pcpc_040.ordem_producao          = v_ordem_producao -- Neylor - Pette.
                 and pcpc_040.seq_operacao            > v_seq_operacao;
            exception when others then
               -- ATENC?O! N?o atualizou registro na tabela {0}. Status: {1}
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds22628', 'PCPC_040(9)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end if;
      end if;
   end if;

   if  (inserting
   or   updating)
   then
     :new.atz_pode_produzir  := 0;
     :new.atz_em_prod        := 0;
     :new.atz_a_prod         := 0;
   end if;

   begin
      select nvl(min(pcpc_040.seq_operacao),0)
      into  v_seq_operacao_alt_programado
      from pcpc_040
      where pcpc_040.periodo_producao = :new.pcpc040_perconf
        and pcpc_040.ordem_confeccao  = :new.pcpc040_ordconf
        and pcpc_040.ordem_producao   = v_ordem_producao -- Neylor - Pette.
        and pcpc_040.codigo_estagio   = v_estagio_corte;
   exception when OTHERS then
      v_seq_operacao_alt_programado := 0;
   end;

   /*
      72065/001 - N?o permitir apontar 2a ou Perda antes do corte
   */
   if  (inserting
   or   updating)
   and v_executa_trigger    = 3
   and (:new.qtde_pecas_2a  <> 0
   or   :new.qtde_perdas    <> 0)
   and v_processo_systextil <> 'pcpc_f144'
   and v_processo_systextil <> 'tmrp_f070'
   and v_processo_systextil <> 'pcpc_f350'
   and v_processo_systextil <> 'pcpc_f052'
   then
      v_tem_registro := 0;
      begin
         select nvl(count(*),0)
         into  v_tem_registro
         from pcpc_040
         where pcpc_040.periodo_producao = :new.pcpc040_perconf
           and pcpc_040.ordem_confeccao  = :new.pcpc040_ordconf
           and pcpc_040.ordem_producao   = v_ordem_producao -- Neylor - Pette.
           and pcpc_040.codigo_estagio   = :new.pcpc040_estconf
           and pcpc_040.seq_operacao     < v_seq_operacao_alt_programado;
      exception when OTHERS then
         v_tem_registro := 0;
      end;

      if v_tem_registro > 0
      then
         raise_application_error(-20000,'ATENC?O! N?o pode apontar segunda qualidade ou perda antes do estagio que altera o programado.');
      end if;
   end if;

   v_tem_registro := 0;

   if  inserting
   then
      /*
         SS 82559/001 - Valida��o Situa��o Projeto.
      */
      begin
         select nvl(count(*),0)
         into  v_tem_registro
         from pcpc_040
         where pcpc_040.periodo_producao = :new.pcpc040_perconf
           and pcpc_040.ordem_confeccao  = :new.pcpc040_ordconf
           and pcpc_040.ordem_producao   = v_ordem_producao -- Neylor - Pette.
           and pcpc_040.codigo_estagio   = :new.pcpc040_estconf
           and pcpc_040.seq_operacao     < v_seq_operacao_alt_programado;
      exception when OTHERS then
         v_tem_registro := 0;
      end;

      if v_tem_registro > 0
      then
         v_codigo_empresa := 0;

         begin
            select pcpc_010.codigo_empresa
            into   v_codigo_empresa
            from pcpc_010
            where pcpc_010.area_periodo     = 1
              and pcpc_010.periodo_producao = :new.pcpc040_perconf;
         exception when others then
            v_codigo_empresa := 0;
         end;

         begin
            select pcpc_040.proconf_grupo
            into   v_proconf_grupo
            from pcpc_040
            where pcpc_040.periodo_producao    = :new.pcpc040_perconf
              and pcpc_040.ordem_confeccao     = :new.pcpc040_ordconf
              and pcpc_040.ordem_producao      = v_ordem_producao -- Neylor - Pette.
              and pcpc_040.codigo_estagio      = :new.pcpc040_estconf
              and pcpc_040.ordem_producao      = :new.ordem_producao
              and rownum                       < 2;
         exception when OTHERS then
            v_proconf_grupo := '';
         end;

         inter_pr_valida_sit_projeto('1',v_proconf_grupo,v_codigo_empresa,1);
         
      end if;
   end if;
   
   if v_solicitacao_conserto > 0
   then
      begin
	      select pcpc_040.qtde_em_producao_pacote
	      into v_qtde_a_produzir
          from pcpc_040
          where pcpc_040.periodo_producao = :new.pcpc040_perconf
            and pcpc_040.ordem_confeccao  = :new.pcpc040_ordconf
            and pcpc_040.ordem_producao   = v_ordem_producao -- Neylor - Pette.
            and pcpc_040.codigo_estagio   = :new.pcpc040_estconf;
      exception when OTHERS then
         v_qtde_a_produzir := 0;
      end;
      
      if v_qtde_a_produzir = 0
      then
         begin
            update pcpc_880
            set pcpc_880.situacao_estagio_solicitacao = 3
            where pcpc_880.numero_solicitacao = v_solicitacao_conserto
              and pcpc_880.sequencia_operacao = v_seq_operacao
              and pcpc_880.sequencia_estagio  = v_sequencia_estagio;
           exception when OTHERS then
             null;
         end;   
  	      
         begin
	         update pcpc_880
	         set pcpc_880.situacao_estagio_solicitacao = 1
       	   where pcpc_880.numero_solicitacao = v_solicitacao_conserto
	         and pcpc_880.sequencia_estagio  = v_prox_sequencia_estagio;
   	   exception when OTHERS then
	         null;
	      end; 
      end if;
   end if;
   
   v_tem_registro := 0;

   begin
      select nvl(count(1),0)
      into v_tem_registro
      from pcpc_040
      where pcpc_040.ordem_producao          = v_ordem_producao
        and pcpc_040.situacao_em_prod_a_prod = 2;
   exception when OTHERS then
      v_tem_registro := 0;
   end;

   if  v_tem_reg_400        <> 0
   and v_tem_registro       <> 0
   and v_processo_systextil <> 'pcpc_f144'
   and v_processo_systextil <> 'tmrp_f070'
   and v_processo_systextil <> 'pcpc_f350'
   and v_processo_systextil <> 'pcpc_f150'
   and v_processo_systextil <> 'pcpc_f052'
   and v_executa_trigger    <> 1
   then
      raise_application_error(-20000,'ATENC?O! Ordem de produc?o esta com erro, n?o pode ser apontada, contate o suporte tecnico.');
   end if;

end inter_tr_pcpc_045_1;

/

exec inter_pr_recompile;
