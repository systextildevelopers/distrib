alter table fatu_501 add bloq_estagio_liberacao varchar2(1) default 'N';

comment on column fatu_501.bloq_estagio_liberacao is  'Um parâmetro de empresa que indica se devemos ou não bloquear a manutenção do estágio de liberação quando a ordem de produção conter rolos produzidos (pesados).';
