CREATE OR REPLACE PACKAGE ST_PCK_TRAMAS_LOTE AS

    TYPE tramas IS RECORD(
       trama1 VARCHAR2(250),
       trama1Eng VARCHAR2(250),
       trama2 VARCHAR2(250),
       trama2Eng VARCHAR2(250),
       lote1 VARCHAR2(25),
       lote2 VARCHAR2(25));

    TYPE tramas_table IS TABLE OF tramas;

    TYPE trama2 IS RECORD(
       trama VARCHAR2(250),
       tramaEng VARCHAR2(250),
       lote VARCHAR2(25));

    TYPE trama2_table IS TABLE OF trama2;

    FUNCTION getTramaLote(v_codigo_rolo NUMBER, v_ordem_producao NUMBER)
        RETURN tramas_table
        PIPELINED;

    FUNCTION getTramaLote2(v_codigo_rolo NUMBER, v_ordem_producao NUMBER, v_grupo VARCHAR2, v_subgrupo VARCHAR2, v_item VARCHAR2)
        RETURN trama2_table
        PIPELINED;
END ST_PCK_TRAMAS_LOTE;
