create table rcnb_890(
 EMPRESA    NUMBER(9)   default 0 not null,
 NIVEL      VARCHAR2(1) default '' not null,
 GRUPO      VARCHAR2(5) default '' not null,
 SUBGRUPO   VARCHAR2(3) default '' not null,
 ITEM       VARCHAR2(6) default '' not null);

alter table rcnb_890 add constraint rcnb_890_pk primary key (EMPRESA,NIVEL,GRUPO,SUBGRUPO,ITEM);

 exec inter_pr_recompile;
 / 
