create table inte_025 (
       nr_solicitacao   number(9) not null,
       codigo_relatorio varchar2(9) not null,
       solicitante      varchar2(250) not null,
       codigo_empresa   number(3) not null,
       nome_conemb      varchar2(60) default '',
       nome_doccob      varchar2(60) default '',
       situacao         number(1) default 0,
       observacao       varchar2(4000) default ''
);

create table INTE_026
(
  NR_SOLICITACAO   NUMBER(9) not null,
  CODIGO_RELATORIO VARCHAR2(9) not null,
  SOLICITANTE      VARCHAR2(250) not null,
  CODIGO_EMPRESA   NUMBER(3) not null,
  SEQUENCIA        NUMBER(9),
  DESCRICAO        VARCHAR2(4000)
);

create table inte_027 (
       nr_solicitacao   number(9) not null,
       codigo_relatorio varchar2(9) not null,
       solicitante      varchar2(250) not null,
       codigo_empresa   number(3) not null,
       empresa_titulo   number(3), 
       nr_duplicata     number(9),
       parcela          varchar2(3),
       cgc9             number(9),
       cgc4             number(9),
       cgc2             number(9),
       tipo_titulo      number(3),
	   data_vencimento	date,
	   valor_parcela	number(15,3)
);

create table inte_028 (
       nr_solicitacao   number(9) not null,
       codigo_relatorio varchar2(9) not null,
       solicitante      varchar2(250) not null,
       codigo_empresa   number(3) not null,
       documento        number(9),
       serie            varchar2(3),
       cgc9             number(9),
       cgc4             number(4),
       cgc2             number(2),
	   data_emissao		date,
	   base_icms		number(15,3),
	   valor_icms		number(15,3),
	   valor_itens		number(15,3),
	   total_docto		number(15,3),
	   valor_desconto	number(15,3)
);
