
  CREATE OR REPLACE PROCEDURE "INTER_PR_INSERIR_BASI_027_028" 
(  vp_nivel_comp         in     varchar2,
   vp_grupo_comp         in     varchar2,
   vp_subgrupo_comp      in     varchar2,
   vp_item_comp          in     varchar2,
   vp_codigo_projeto     in     varchar2,
   vp_sequencia_projeto  in     number,
   vp_nivel_item         in     varchar2,
   vp_grupo_item         in     varchar2,
   vp_subgrupo_item      in     varchar2,
   vp_item_item          in     varchar2,
   vp_alternativa_item   in     number,
   vp_cliente9           in     number,
   vp_cliente4           in     number,
   vp_cliente2           in     number
) IS
   v_total_027          number;
   v_total_028          number;
BEGIN
   if  vp_nivel_comp    is not null
   and vp_nivel_comp    <> '0'
   and vp_grupo_comp    is not null
   and vp_grupo_comp    <> '00000'
   and vp_subgrupo_comp is not null
   and vp_subgrupo_comp <> '000'
   and vp_item_comp     is not null
   and vp_item_comp     <> '000000'
   then
      --Count para verficar se ja existe o componente
      begin
         select count(1)
         into v_total_027
         from basi_027
         where basi_027.nivel_componente    = vp_nivel_comp
           and basi_027.grupo_componente    = vp_grupo_comp
           and basi_027.subgrupo_componente = vp_subgrupo_comp
           and basi_027.item_componente     = vp_item_comp
           and basi_027.codigo_projeto      = vp_codigo_projeto
           and basi_027.sequencia_projeto   = vp_sequencia_projeto
           and basi_027.nivel_item          = vp_nivel_item
           and basi_027.grupo_item          = vp_grupo_item
           and basi_027.subgrupo_item       = vp_subgrupo_item
           and basi_027.item_item           = vp_item_item
           and basi_027.alt_item            = vp_alternativa_item;
      end;

      if v_total_027 = 0
      then
         begin
            INSERT INTO basi_027 (
               nivel_componente,           grupo_componente,
               subgrupo_componente,        item_componente,
               codigo_projeto,             sequencia_projeto,
               cnpj_cliente9,
               cnpj_cliente4,              cnpj_cliente2,
               nivel_item,                 grupo_item,
               subgrupo_item,              item_item,
               alt_item
            ) VALUES (
               vp_nivel_comp,              vp_grupo_comp,
               vp_subgrupo_comp,           vp_item_comp,
               vp_codigo_projeto,          vp_sequencia_projeto,
               vp_cliente9,
               vp_cliente4,                vp_cliente2,
               vp_nivel_item,              vp_grupo_item,
               vp_subgrupo_item,           vp_item_item,
               vp_alternativa_item
            );
         exception when OTHERS then
            raise_application_error (-20000, 'Nao atualizou a tabela de Relacionamento da Situacao(basi_027)');
         end;
      end if;

      if vp_nivel_comp = '2'
      then
         -- Consiste se ha registro do componente na tabela de componente aprovado.
         select count(1)
         into v_total_028
         from basi_028
         where basi_028.nivel_componente    = vp_nivel_comp
           and basi_028.grupo_componente    = vp_grupo_comp
           and basi_028.subgrupo_componente = vp_subgrupo_comp
           and basi_028.item_componente     = vp_item_comp
           and basi_028.cnpj_cliente9       = vp_cliente9
           and basi_028.cnpj_cliente4       = vp_cliente4
           and basi_028.cnpj_cliente2       = vp_cliente2
           and basi_028.tipo_aprovacao      = 1;   --APROVACAO POR COMPONENTE
         if v_total_028 = 0
         then
            begin
               INSERT INTO basi_028 (
                  nivel_componente,                grupo_componente,
                  subgrupo_componente,             item_componente,
                  cnpj_cliente9,                   cnpj_cliente4,
                  cnpj_cliente2,                   codigo_projeto,
                  sequencia_projeto,               situacao_componente,
                  tipo_aprovacao
               ) VALUES (
                  vp_nivel_comp,                    vp_grupo_comp,
                  vp_subgrupo_comp,                 vp_item_comp,
                  vp_cliente9,                      vp_cliente4,
                  vp_cliente2,                      vp_codigo_projeto,
                  vp_sequencia_projeto,             0,
                  1
               );
            exception when OTHERS then
               raise_application_error (-20000, 'Nao atualizou a tabela de Situacao do Item(basi_028)');
            end;
         end if;
      else
         -- Consiste se ha registro do componente na tabela de componente aprovado.
         select count(1)
         into v_total_028
         from basi_028
         where basi_028.nivel_componente    = vp_nivel_comp
           and basi_028.grupo_componente    = vp_grupo_comp
           and basi_028.subgrupo_componente = vp_subgrupo_comp
           and basi_028.item_componente     = vp_item_comp
           and basi_028.codigo_projeto      = vp_codigo_projeto
           and basi_028.sequencia_projeto   = vp_sequencia_projeto
           and basi_028.tipo_aprovacao      = 1;   --APROVACAO POR COMPONENTE
         if v_total_028 = 0
         then
            begin
               INSERT INTO basi_028 (
                  nivel_componente,                grupo_componente,
                  subgrupo_componente,             item_componente,
                  cnpj_cliente9,                   cnpj_cliente4,
                  cnpj_cliente2,                   codigo_projeto,
                  sequencia_projeto,               situacao_componente,
                  tipo_aprovacao
               ) VALUES (
                  vp_nivel_comp,                   vp_grupo_comp,
                  vp_subgrupo_comp,                vp_item_comp,
                  vp_cliente9,                     vp_cliente4,
                  vp_cliente2,                     vp_codigo_projeto,
                  vp_sequencia_projeto,            0,
                  1
               );
            exception when OTHERS then
               raise_application_error (-20000, 'Nao atualizou a tabela de Situacao do Item(basi_028)');
            end;
         end if;
      end if;
   end if;

END inter_pr_inserir_basi_027_028;

 

/

exec inter_pr_recompile;

