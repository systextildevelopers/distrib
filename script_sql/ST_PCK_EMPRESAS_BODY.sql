create or replace package body ST_PCK_EMPRESAS as

    function get_fatu_500(p_codigo_empresa number, p_msg_erro in out varchar2) return t_dados_fatu_500 
    as 
        v_dados_empresa t_dados_fatu_500;
    begin
        begin
            select *
            bulk collect into v_dados_empresa
            from fatu_500
            where codigo_empresa = p_codigo_empresa;
        exception when others then
            p_msg_erro := 'Não foi possível buscar parâmetros de empresa fatu_500:' || p_codigo_empresa;
        end;

        return v_dados_empresa;
    end get_fatu_500;
    
    function exists_by_cod_empresa(p_codigo_empresa number) return boolean 
        as tmpInt number;
    begin
        begin
            select 1 
            into tmpInt
            from fatu_500
            where codigo_empresa = p_codigo_empresa;
            
            return true;
        EXCEPTION WHEN NO_DATA_FOUND then
            return false;
        end;
    end;   

    function get_fatu_503(p_codigo_empresa number, p_msg_erro in out varchar2) return t_dados_fatu_503 
    as
        v_dados_503 t_dados_fatu_503;
    begin
        begin
        select *
        bulk collect into v_dados_503
        from fatu_503
        where codigo_empresa = p_codigo_empresa;
        
        exception when others then
            p_msg_erro := 'Não foi possível buscar parâmetros de empresa fatu_503:' || p_codigo_empresa;
        end;

        return v_dados_503;
    end get_fatu_503;
	

end ST_PCK_EMPRESAS;
