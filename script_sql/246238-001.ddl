create table OBRF_806
(
  COD_EMPRESA                    NUMBER(4) default (9999) not null,
  COD_AJUSTE                     VARCHAR2(50) default ('') not null,
  PERC_ICMS_ORIGEM               NUMBER(6,2) default (0) not null,             
  PERC_CRED_PRESUMIDO            NUMBER(6,2) default (0) not null
);

alter table fatu_060
add(
perc_cred_presumido  number(6,2),
valor_cred_presumido number(15,2),
cod_cbnef            varchar2(50)
);


alter table obrf_152
add(
perc_cred_presumido  number(6,2),
valor_cred_presumido number(15,2),
cod_cbnef            varchar2(50)
);
