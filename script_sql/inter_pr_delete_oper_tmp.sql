
  CREATE OR REPLACE PROCEDURE "INTER_PR_DELETE_OPER_TMP" 
 (p_num_aleatorio           in numeric,
  p_nome_relatorio          in varchar2)

IS
BEGIN
  DELETE /*+parallel(oper_tmp, 100)*/
  FROM   oper_tmp
  WHERE  nr_solicitacao = p_num_aleatorio
  AND    nome_relatorio = p_nome_relatorio;

  COMMIT;
END INTER_PR_DELETE_OPER_TMP;

 

/

exec inter_pr_recompile;

