
  CREATE OR REPLACE TRIGGER "INTER_TR_HIST_200" 
before insert on hist_200
  for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   v_nr_registro             number;
   v_nome_programa           varchar2(60);
begin
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   v_nome_programa := inter_fn_nome_programa(ws_sid);   

   :new.programa         := v_nome_programa;
   :new.aplicacao        := ws_aplicativo;
   :new.usuario          := ws_usuario_systextil;
   :new.usuario_rede     := ws_usuario_rede;
   :new.data_ocorr       := sysdate;
   :new.maquina_rede     := ws_maquina_rede;

   begin
      select seq_hist_200.nextval
      into v_nr_registro
      from dual;
   end;

   :new.sequencia := v_nr_registro;

end inter_tr_hist_200;

-- ALTER TRIGGER "INTER_TR_HIST_200" ENABLE
 

/

exec inter_pr_recompile;

