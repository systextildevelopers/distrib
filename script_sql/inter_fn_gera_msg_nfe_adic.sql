CREATE OR REPLACE FUNCTION inter_fn_gera_msg_nfe_adic (
    p_empresa IN NUMBER,
    p_numero_nota IN NUMBER,
    p_serie IN VARCHAR2,
    p_cgc9 IN NUMBER,
    p_cgc4 IN NUMBER,
    p_cgc2 IN NUMBER,
    p_sequencia IN NUMBER,
    p_entrada_saida IN VARCHAR2,
    p_codigo IN NUMBER
) RETURN VARCHAR2
IS
    type v_tp_cursor is REF CURSOR;
    
    PRAGMA AUTONOMOUS_TRANSACTION;
    
    v_mensagem VARCHAR2(4000);
    v_script_sql VARCHAR2(4000);
    v_resultado_sql VARCHAR2(4000);
    v_mensagem_codigo VARCHAR2(4000);
    v_ind_local VARCHAR2(2);
    v_existe NUMBER;
    v_primeiro_resultado BOOLEAN := TRUE; -- Vari?vel para controlar o primeiro resultado
    v_cursor         v_tp_cursor;

BEGIN
    
    if p_sequencia = 0
    then
       SELECT obrf_182.script_sql
         INTO v_script_sql
         FROM obrf_182
        WHERE obrf_182.codigo = p_codigo;
        v_script_sql := replace(v_script_sql,'#codigo_empresa#',p_empresa);
        v_script_sql := replace(v_script_sql,'#num_nota_fiscal#',p_numero_nota);
        v_script_sql := replace(v_script_sql,'#serie_nota_fisc#','''' || p_serie || '''');
        v_script_sql := replace(v_script_sql,'#cgc_9#',p_cgc9);
        v_script_sql := replace(v_script_sql,'#cgc_4#',p_cgc4);
        v_script_sql := replace(v_script_sql,'#cgc_2#',p_cgc2);
        
        -- Executa o script SQL          
        begin
          open v_cursor for v_script_sql;

          loop
            fetch v_cursor into v_resultado_sql;

            exit when v_cursor%notfound;

            v_mensagem := v_mensagem || ' ' || trim(v_resultado_sql);
              
          end loop;

          -- fecha o cursor do SQL
          close v_cursor;

        exception
          when others then
             raise_application_error(-20000,'Erro na execucao do SQL' || sqlerrm);
        end;
        
    end if;

    if p_entrada_saida = 'E' then
       -- SQL PEGAR MENSAGEM ENTRADA
       FOR paraMsgEntra IN (SELECT obrf_182.script_sql, obrf_181.cod_msg FROM obrf_182, obrf_181
                            WHERE obrf_182.codigo = obrf_181.codigo_sql
                              AND obrf_181.empresa = p_empresa
                              AND obrf_181.inicio_vigencia < sysdate
                              AND (obrf_181.fim_vigencia > sysdate OR obrf_181.fim_vigencia IS NULL)
                              AND obrf_181.ativo_inativo = 1
                              AND obrf_182.tipo_nota = 1 -- 0 = sa?da, 1 = entrada
                              AND (obrf_182.codigo = p_codigo or p_codigo = 0)
                              ORDER BY obrf_181.cod_msg)
       LOOP
          -- Substitui par?metros no script SQL
          v_script_sql := replace(paraMsgEntra.script_sql,'#codigo_empresa#',p_empresa);
          v_script_sql := replace(v_script_sql,'#num_nota_fiscal#',p_numero_nota);
          v_script_sql := replace(v_script_sql,'#serie_nota_fisc#','''' || p_serie || '''');
          v_script_sql := replace(v_script_sql,'#cgc_9#',p_cgc9);
          v_script_sql := replace(v_script_sql,'#cgc_4#',p_cgc4);
          v_script_sql := replace(v_script_sql,'#cgc_2#',p_cgc2);
          
          -- le a tabela de mensagens e monta a vari vel
          begin
             select nvl(trim(obrf_874.des_mensagem1)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem2)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem3)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem4)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem5)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem6)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem7)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem8)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem9)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem10),''),
                    ind_local
             into v_mensagem_codigo, v_ind_local
             from obrf_874
             where obrf_874.cod_mensagem = paraMsgEntra.cod_msg;

          exception
             when dup_val_on_index then
                null;
             when others then
                raise_application_error(-20000,'Erro ao buscar a mensagem' || sqlerrm);
          end;

          -- Concatena a mensagem do codigo com o retorno do SQL
          v_mensagem := v_mensagem_codigo ;

          -- Executa o script SQL          
          begin
            open v_cursor for v_script_sql;

            loop
              fetch v_cursor into v_resultado_sql;

              exit when v_cursor%notfound;

              v_mensagem := v_mensagem || ' ' || trim(v_resultado_sql);
              
            end loop;

            -- fecha o cursor do SQL
            close v_cursor;

          exception
            when others then
               raise_application_error(-20000,'Erro na execucao do SQL' || sqlerrm);
          end;
          
          -- nao insere codigo da msg duplicado
          begin
           select count(1)
           into v_existe
           from fatu_052
           where cod_empresa = p_empresa
             and num_nota = p_numero_nota
             and cod_serie_nota = p_serie
             and cnpj9 = p_cgc9
             and cnpj4 = p_cgc4
             and cnpj2 = p_cgc2
             and cod_mensagem = paraMsgEntra.cod_msg;
           exception
           when no_data_found then
             v_existe := 0;
         end;
          
          -- grava os dados na tabela de mensagem da nota fiscal
          if p_sequencia > 0 and length(trim(v_mensagem)) > 0 and length(trim(v_resultado_sql)) > 0 and v_existe = 0 
          then 
             begin
               insert into fatu_052 (
                  cod_empresa,
                  num_nota,
                  cod_serie_nota,
                  cod_mensagem,
                  cnpj9,
                  cnpj4,
                  cnpj2,
                  ind_entr_saida,
                  seq_mensagem,
                  ind_local,
                  des_mensag_1,
                  des_mensag_2,
                  des_mensag_3,
                  des_mensag_4,
                  des_mensag_5,
                  des_mensag_6,
                  des_mensag_7,
                  des_mensag_8,
                  des_mensag_9,
                  des_mensag_10)
               values (
                  p_empresa,
                  p_numero_nota,
                  p_serie,
                  paraMsgEntra.cod_msg,
                  p_cgc9,
                  p_cgc4,
                  p_cgc2,
                  'E',
                  p_sequencia,
                  v_ind_local,
                  substr(v_mensagem,   1,  110),
                  substr(v_mensagem, 111,  220),
                  substr(v_mensagem, 221,  230),
                  substr(v_mensagem, 231,  340),
                  substr(v_mensagem, 341,  450),
                  substr(v_mensagem, 451,  560),
                  substr(v_mensagem, 561,  670),
                  substr(v_mensagem, 671,  780),
                  substr(v_mensagem, 781,  890),
                  substr(v_mensagem, 891, 1000)
                );
                
              exception
                when dup_val_on_index then
                   null;
                when others then
                   raise_application_error(-20000,'Erro ao inserir mensagem' || sqlerrm);
              end;
              commit;
              
           end if;
       END LOOP;
      
    ELSE
       -- SQL PEGAR MENSAGEM SA?DA
       FOR paraMsgSaida IN (SELECT obrf_182.script_sql, obrf_181.cod_msg FROM obrf_182, obrf_181
                            WHERE obrf_182.codigo = obrf_181.codigo_sql
                              AND obrf_181.empresa = p_empresa
                              AND obrf_181.inicio_vigencia < sysdate
                              AND (obrf_181.fim_vigencia > sysdate OR obrf_181.fim_vigencia IS NULL)
                              AND obrf_181.ativo_inativo = 1
                              AND obrf_182.tipo_nota = 0 -- 0 = sa?da, 1 = entrada
                              AND obrf_182.local_mensagem = 0
                              AND (obrf_182.codigo = p_codigo or p_codigo = 0)
                              ORDER BY obrf_181.cod_msg)
       LOOP
          -- Substitui par?metros no script SQL
          v_script_sql := replace(paraMsgSaida.script_sql,'#codigo_empresa#',p_empresa);
          v_script_sql := replace(v_script_sql,'#num_nota_fiscal#',p_numero_nota);
          v_script_sql := replace(v_script_sql,'#serie_nota_fisc#','''' || p_serie || '''');
          
          
          -- le a tabela de mensagens e monta a vari vel
          begin
             select nvl(trim(obrf_874.des_mensagem1)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem2)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem3)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem4)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem5)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem6)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem7)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem8)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem9)  ||-- ' ' ||
                    trim(obrf_874.des_mensagem10),''),
                    ind_local
             into v_mensagem_codigo, v_ind_local
             from obrf_874
             where obrf_874.cod_mensagem = paraMsgSaida.cod_msg;

          exception
             when dup_val_on_index then
                null;
             when others then
                raise_application_error(-20000,'Erro ao buscar a mensagem' || sqlerrm);
          end;
          
          -- Concatena a mensagem do codigo com o retorno do SQL
          v_mensagem := v_mensagem_codigo ;

          -- Executa o script SQL          
          begin
            open v_cursor for v_script_sql;

            loop
              fetch v_cursor into v_resultado_sql;

              exit when v_cursor%notfound;

              v_mensagem := v_mensagem || ' ' || trim(v_resultado_sql);
              
            end loop;

            -- fecha o cursor do SQL
            close v_cursor;

          exception
            when others then
               raise_application_error(-20000,'Erro na execucao do SQL' || sqlerrm);
          end;
          
          -- nao insere codigo da msg duplicado
          begin
           select count(1)
           into v_existe
           from fatu_052
           where cod_empresa = p_empresa
             and num_nota = p_numero_nota
             and cod_serie_nota = p_serie
             and cnpj9 = p_cgc9
             and cnpj4 = p_cgc4
             and cnpj2 = p_cgc2
             and cod_mensagem = paraMsgSaida.cod_msg;
           exception
           when no_data_found then
             v_existe := 0;
          end;
          
          -- grava os dados na tabela de mensagem da nota fiscal
          if p_sequencia > 0 and length(trim(v_mensagem)) > 0 and length(trim(v_resultado_sql)) > 0 and v_existe = 0 
          then 
             begin
               insert into fatu_052 (
                  cod_empresa,
                  num_nota,
                  cod_serie_nota,
                  cod_mensagem,
                  cnpj9,
                  cnpj4,
                  cnpj2,
                  ind_entr_saida,
                  seq_mensagem,
                  ind_local,
                  des_mensag_1,
                  des_mensag_2,
                  des_mensag_3,
                  des_mensag_4,
                  des_mensag_5,
                  des_mensag_6,
                  des_mensag_7,
                  des_mensag_8,
                  des_mensag_9,
                  des_mensag_10)
               values (
                  p_empresa,
                  p_numero_nota,
                  p_serie,
                  paraMsgSaida.cod_msg,
                  p_cgc9,
                  p_cgc4,
                  p_cgc2,
                  'S',
                  v_existe,
                  v_ind_local,
                  substr(v_mensagem,   1,  110),
                  substr(v_mensagem, 111,  220),
                  substr(v_mensagem, 221,  230),
                  substr(v_mensagem, 231,  340),
                  substr(v_mensagem, 341,  450),
                  substr(v_mensagem, 451,  560),
                  substr(v_mensagem, 561,  670),
                  substr(v_mensagem, 671,  780),
                  substr(v_mensagem, 781,  890),
                  substr(v_mensagem, 891, 1000)
                );
              exception
                when dup_val_on_index then
                   null;
                when others then
                   raise_application_error(-20000,'Erro ao inserir mensagem' || sqlerrm);

              end;
              commit;

           end if;
       END LOOP;
    END IF;

    -- Retorna a mensagem gerada
    RETURN v_mensagem;
END inter_fn_gera_msg_nfe_adic;
/
