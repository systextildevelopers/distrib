create or replace trigger "INTER_TR_MQOP_050_LOG" 
  before insert or delete or
  update of time_celula,           codigo_aparelho, 
            perc_efic_rot,         numero_operadoras,
            considera_efic,        seq_operacao_agrupadora, 
            codigo_parte_peca,     seq_juncao_parte_peca,
            situacao,              perc_perdas, 
            observacao,            temperatura,
            nivel_estrutura,       grupo_estrutura, 
            subgru_estrutura,      item_estrutura,
            numero_alternati,      numero_roteiro, 
            seq_operacao,          codigo_operacao,
            minutos,               codigo_estagio, 
            centro_custo,          sequencia_estagio,
            estagio_anterior,      estagio_depende, 
            separa_operacao,       minutos_homem,
            ccusto_homem,          numero_cordas, 
            numero_rolos,          velocidade,
            codigo_familia,        tipo_processo, 
            pecas_1_hora,          pecas_8_horas,
            custo_minuto,          perc_eficiencia, 
            tempo_lote_producao,   pecas_lote_producao,
            cod_estagio_agrupador  
         on mqop_050
  for each row
declare
  v_estagio_old            mqop_050.codigo_estagio%type;
  v_tempo_homem_old        mqop_050.minutos_homem%type;
  v_tempo_maquina_old      mqop_050.minutos%type;
  v_cc_homem_old           mqop_050.ccusto_homem%type;
  v_cc_maquina_old         mqop_050.centro_custo%type;
  v_estagio_new            mqop_050.codigo_estagio%type;
  v_tempo_homem_new        mqop_050.minutos_homem%type;
  v_tempo_maquina_new      mqop_050.minutos%type;
  v_cc_homem_new           mqop_050.ccusto_homem%type;
  v_cc_maquina_new         mqop_050.centro_custo%type;
  v_nivel_estrutura        mqop_050.nivel_estrutura%type;
  v_grupo_estrutura        mqop_050.grupo_estrutura%type;
  v_subgru_estrutura       mqop_050.subgru_estrutura%type;
  v_item_estrutura         mqop_050.item_estrutura%type;
  v_numero_alternati       mqop_050.numero_alternati%type;
  v_numero_roteiro         mqop_050.numero_roteiro%type;
  v_seq_operacao           mqop_050.seq_operacao%type;
  v_cod_estagio_agrupador_new     mqop_050.cod_estagio_agrupador%type;
  v_cod_estagio_agrupador_old     mqop_050.cod_estagio_agrupador%type;
  v_seq_operacao_agrupador_new    mqop_050.seq_operacao_agrupador%type;
  v_seq_operacao_agrupador_old    mqop_050.seq_operacao_agrupador%type;  
  
  v_operacao              varchar(1);
  v_data_operacao         date;
  ws_usuario_rede         hdoc_030.usuario%type;
  ws_maquina_rede         varchar(40);
  ws_aplicativo           varchar(20);
  ws_sid                  number(9);
  ws_nome_programa        hdoc_090.programa%type;
  ws_empresa              number(3);
  ws_usuario_systextil    varchar2(250);
  ws_locale_usuario       varchar2(5);

begin
    -- Grava a data/hora da inser??o do registro (log)
   v_data_operacao := sysdate();
   
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   
   ws_nome_programa := inter_fn_nome_programa(ws_sid);
   
   --alimenta as vari?veis new caso seja insert ou update
   if inserting or updating
   then
     if inserting then 
          v_operacao := 'I';
     else 
          v_operacao := 'U';
     end if;
     
     v_estagio_new                  := :new.codigo_estagio;
     v_tempo_homem_new              := :new.minutos_homem;
     v_tempo_maquina_new            := :new.minutos;
     v_cc_homem_new                 := :new.ccusto_homem;
     v_cc_maquina_new               := :new.centro_custo;
     v_nivel_estrutura              := :new.nivel_estrutura;
     v_grupo_estrutura              := :new.grupo_estrutura;
     v_subgru_estrutura             := :new.subgru_estrutura;
     v_item_estrutura               := :new.item_estrutura;
     v_numero_alternati             := :new.numero_alternati;
     v_numero_roteiro               := :new.numero_roteiro;
     v_seq_operacao                 := :new.seq_operacao;
     v_cod_estagio_agrupador_new    := :new.cod_estagio_agrupador;
     v_seq_operacao_agrupador_new   := :new.seq_operacao_agrupador;
   end if; --fim do if inserting or updating
   
   --alimenta as vari?veis old caso seja insert ou update
   if deleting or updating
   then
      if deleting then 
           v_operacao := 'D';
      else 
           v_operacao := 'U';
      end if;
      
      v_estagio_old                 := :old.codigo_estagio;
      v_tempo_homem_old             := :old.minutos_homem;
      v_tempo_maquina_old           := :old.minutos;
      v_cc_homem_old                := :old.ccusto_homem;
      v_cc_maquina_old              := :old.centro_custo;
      v_nivel_estrutura             := :old.nivel_estrutura;
      v_grupo_estrutura             := :old.grupo_estrutura;
      v_subgru_estrutura            := :old.subgru_estrutura;
      v_item_estrutura              := :old.item_estrutura;
      v_numero_alternati            := :old.numero_alternati;
      v_numero_roteiro              := :old.numero_roteiro;
      v_seq_operacao                := :old.seq_operacao;
      v_cod_estagio_agrupador_old   := :old.cod_estagio_agrupador;
      v_seq_operacao_agrupador_old  := :old.seq_operacao_agrupador;
   end if; --fim do if inserting or updating
   
   insert into mqop_050_log (
     estagio_new,                   tempo_homem_new,
     tempo_maquina_new,             cc_homem_new,
     cc_maquina_new,                estagio_old,
     tempo_homem_old,               tempo_maquina_old,
     cc_homem_old,                  cc_maquina_old,
     cod_estagio_agrupador_old,     cod_estagio_agrupador_new,
     seq_operacao_agrupador_old,    seq_operacao_agrupador_new,
     operacao,                      data_operacao,
     usuario_rede,                  maquina_rede,
     aplicativo,                    nivel_estrutura,
     grupo_estrutura,               subgru_estrutura,
     item_estrutura,                numero_alternati,
     numero_roteiro,                seq_operacao,
     programa
   ) values (
     v_estagio_new,                 v_tempo_homem_new,
     v_tempo_maquina_new,           v_cc_homem_new,
     v_cc_maquina_new,              v_estagio_old,
     v_tempo_homem_old,             v_tempo_maquina_old,
     v_cc_homem_old,                v_cc_maquina_old,
     v_cod_estagio_agrupador_old,   v_cod_estagio_agrupador_new,
     v_seq_operacao_agrupador_old,  v_seq_operacao_agrupador_new,   
     v_operacao,                    v_data_operacao,
     ws_usuario_rede,               ws_maquina_rede,
     ws_aplicativo,                 v_nivel_estrutura,
     v_grupo_estrutura,             v_subgru_estrutura,
     v_item_estrutura,              v_numero_alternati,
     v_numero_roteiro,              v_seq_operacao,
     ws_nome_programa
   );
   exception
      when OTHERS
      then raise_application_error (-20000, 'N?o atualizou a tabela de log da mqop_050.');

end inter_tr_mqop_050_log;

/

exec inter_pr_recompile;

