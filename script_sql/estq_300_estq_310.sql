create or replace view estq_300_estq_310 as
select
 codigo_deposito,               nivel_estrutura,              grupo_estrutura,
 subgrupo_estrutura,            item_estrutura,               data_movimento,
 sequencia_ficha,               sequencia_insercao,           numero_lote,
 numero_documento,              serie_documento,              cnpj_9,
 cnpj_4,                        cnpj_2,                       sequencia_documento,
 codigo_transacao,              entrada_saida,                centro_custo,
 quantidade,                    saldo_fisico,                 valor_movimento_unitario,
 valor_movimento_unitario_proj, valor_contabil_unitario,      preco_medio_unitario,
 preco_medio_unitario_proj,     saldo_financeiro,             saldo_financeiro_proj,
 grupo_maquina,                 subgru_maquina,               numero_maquina,
 ordem_servico,                 contabilizado,                usuario_systextil,
 processo_systextil,            data_insercao,                usuario_rede,
 maquina_rede,                  aplicativo,                   tabela_origem,
 valor_movto_unit_estimado,     preco_medio_unit_estimado,    saldo_financeiro_estimado,
 valor_total,                   projeto,                      subprojeto,
 servico,                       trunc(data_movimento,'MM') as periodo_movimento,
 tipo_sped_transacao,           nvl(numero_op,0)  numero_op,  nvl(numero_os,0) numero_os,
 nvl(numero_nf,0) numero_nf,    nvl(tipo_ordem,0) tipo_ordem, nvl(estagio_op,0) estagio_op,
 nvl(pacote_op,0) pacote_op
from estq_300
UNION ALL select
 codigo_deposito,               nivel_estrutura,              grupo_estrutura,
 subgrupo_estrutura,            item_estrutura,               data_movimento,
 sequencia_ficha,               sequencia_insercao,           numero_lote,
 numero_documento,              serie_documento,              cnpj_9,
 cnpj_4,                        cnpj_2,                       sequencia_documento,
 codigo_transacao,              entrada_saida,                centro_custo,
 quantidade,                    saldo_fisico,                 valor_movimento_unitario,
 valor_movimento_unitario_proj, valor_contabil_unitario,      preco_medio_unitario,
 preco_medio_unitario_proj,     saldo_financeiro,             saldo_financeiro_proj,
 grupo_maquina,                 subgru_maquina,               numero_maquina,
 ordem_servico,                 contabilizado,                usuario_systextil,
 processo_systextil,            data_insercao,                usuario_rede,
 maquina_rede,                  aplicativo,                   tabela_origem,
 valor_movto_unit_estimado,     preco_medio_unit_estimado,    saldo_financeiro_estimado,
 valor_total,                   projeto,                      subprojeto,
 servico,                       trunc(data_movimento,'MM') as periodo_movimento,
 tipo_sped_transacao,           nvl(numero_op,0) numero_op,   nvl(numero_os,0) numero_os,
 nvl(numero_nf,0) numero_nf,    nvl(tipo_ordem,0) tipo_ordem, nvl(estagio_op,0) estagio_op,
 nvl(pacote_op,0) pacote_op
from estq_310;

exec inter_pr_recompile;
/
