create or replace trigger inter_tr_cert_003_log 
after insert or delete or update 
on cert_003 
for each row 
declare 
   ws_usuario_rede           varchar2(20); 
   ws_maquina_rede           varchar2(40); 
   ws_aplicativo             varchar2(20); 
   ws_sid                    number(9); 
   ws_empresa                number(3); 
   ws_usuario_systextil      varchar2(250); 
   ws_locale_usuario         varchar2(5); 
   v_nome_programa           varchar2(20); 
begin
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 

   v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 if inserting 
 then 
    begin 
 
        insert into cert_003_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           codigo_divisao_producao_OLD,   /*8*/ 
           codigo_divisao_producao_NEW,   /*9*/ 
           codigo_certificacao_OLD,   /*10*/ 
           codigo_certificacao_NEW,   /*11*/ 
           data_inicio_OLD,   /*12*/ 
           data_inicio_NEW,   /*13*/ 
           data_fim_OLD,   /*14*/ 
           data_fim_NEW   /*15*/
        ) values (    
           'I', /*o*/
           sysdate, /*1*/
           sysdate,/*2*/ 
           ws_usuario_rede,/*3*/ 
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/ 
           v_nome_programa, /*7*/
           0,/*8*/
           :new.codigo_divisao_producao, /*9*/   
           0,/*10*/
           :new.codigo_certificacao, /*11*/   
           null,/*12*/
           :new.data_inicio, /*13*/   
           null, /*14*/
           :new.data_fim /*15*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into cert_003_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           codigo_divisao_producao_OLD, /*8*/   
           codigo_divisao_producao_NEW, /*9*/   
           codigo_certificacao_OLD, /*10*/   
           codigo_certificacao_NEW, /*11*/   
           data_inicio_OLD, /*12*/   
           data_inicio_NEW, /*13*/   
           data_fim_OLD, /*14*/   
           data_fim_NEW /*15*/   
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.codigo_divisao_producao,  /*8*/  
           :new.codigo_divisao_producao, /*9*/   
           :old.codigo_certificacao,  /*10*/  
           :new.codigo_certificacao, /*11*/   
           :old.data_inicio,  /*12*/  
           :new.data_inicio, /*13*/   
           :old.data_fim,  /*14*/  
           :new.data_fim /*15*/   
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into cert_003_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           codigo_divisao_producao_OLD, /*8*/   
           codigo_divisao_producao_NEW, /*9*/   
           codigo_certificacao_OLD, /*10*/   
           codigo_certificacao_NEW, /*11*/   
           data_inicio_OLD, /*12*/   
           data_inicio_NEW, /*13*/   
           data_fim_OLD, /*14*/   
           data_fim_NEW /*15*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.codigo_divisao_producao, /*8*/   
           0, /*9*/
           :old.codigo_certificacao, /*10*/   
           0, /*11*/
           :old.data_inicio, /*12*/   
           null, /*13*/
           :old.data_fim, /*14*/   
           null /*15*/

         );    
    end;    
 end if;    
end inter_tr_cert_003_log;

/
