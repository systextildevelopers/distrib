create or replace view obrf_089_aberta_pacote as 
  select obrf_089.ordem_servico,
       obrf_089.situacao_item,
       obrf_089.qualidade_retorno,
       obrf_089.numero_solicitacao,
       obrf_089.tipo_solicitacao_conserto,
       obrf_089.tag_atualiza_conserto,
       obrf_089.nivel_estrutura,
       obrf_089.grupo_estrutura,
       obrf_089.subgrupo_estrutura,
       obrf_089.item_estrutura,
       obrf_089.ordem_producao,
       obrf_089.ordem_confeccao,
       obrf_089.periodo_producao,
       obrf_089.estagio,
       count(*)   as qtde_tag
from obrf_089
group by obrf_089.ordem_servico,
         obrf_089.situacao_item,
         obrf_089.qualidade_retorno,
         obrf_089.numero_solicitacao,
         obrf_089.tipo_solicitacao_conserto,
         obrf_089.tag_atualiza_conserto,
         obrf_089.nivel_estrutura,
         obrf_089.grupo_estrutura,
         obrf_089.subgrupo_estrutura,
         obrf_089.item_estrutura,
         obrf_089.ordem_producao,
         obrf_089.ordem_confeccao,
         obrf_089.periodo_producao,
         obrf_089.estagio;
 
/
