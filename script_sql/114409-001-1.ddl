create table LOJA_810_LOG
(
  TIPO_OCORR                VARCHAR2(1) default '',
  DATA_OCORR                DATE,
  HORA_OCORR                DATE,
  USUARIO_REDE              VARCHAR2(20) default '',
  MAQUINA_REDE              VARCHAR2(40) default '',
  APLICACAO                 VARCHAR2(20) default '',
  USUARIO_SISTEMA           VARCHAR2(20) default '',
  NOME_PROGRAMA             VARCHAR2(20) default '',
  EMPRESA_OLD               NUMBER(3) default 0,
  EMPRESA_NEW               NUMBER(3) default 0, 
  CODIGO_CAIXA_OLD          NUMBER(3) default 0,
  CODIGO_CAIXA_NEW          NUMBER(3) default 0,
  DATA_CAIXA_OLD            DATE,
  DATA_CAIXA_NEW            DATE, 
  SALDO_INICIAL_OLD         NUMBER(12,3) default 0.0,
  SALDO_INICIAL_NEW         NUMBER(12,3) default 0.0, 
  SALDO_FINAL_OLD           NUMBER(12,3) default 0.0,
  SALDO_FINAL_NEW           NUMBER(12,3) default 0.0,
  SITUACAO_OLD              VARCHAR2(1) default 'A', 
  SITUACAO_NEW              VARCHAR2(1) default 'A', 
  OBSERVACAO_OLD            VARCHAR2(800),
  OBSERVACAO_NEW            VARCHAR2(800)
);
exec inter_pr_recompile;
