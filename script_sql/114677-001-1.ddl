insert into empr_007 (param, tipo, label, fyi_message, default_str, default_int, default_dbl, default_dat)
VALUES ( 'sped.cprb', 1, 'lb47298', 'fy45381', null, 1, null, null);
commit;
/

declare tmpInt number;


cursor parametro_c is select codigo_empresa from fatu_500;

begin
  for reg_parametro in parametro_c
  loop
    select nvl(count(*),0) into tmpInt
    from fatu_500
    where fatu_500.codigo_empresa = reg_parametro.codigo_empresa;

    if(tmpInt > 0)
    then
      begin
        insert into empr_008 ( codigo_empresa, param, val_int) 
            values (reg_parametro.codigo_empresa, 'sped.cprb', 0);
      end;
    end if;

  end loop;
  commit;
end;
/
