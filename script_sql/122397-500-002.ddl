alter table supr_010 add permite_substit number(2) default 0;

comment on column supr_010.permite_substit is 'Indica se o fornecedor pode ser utilizado na substituicao de titulos onde ele nao faca parte de um grupo de empresas com o mesmo CNPJ9 (Padrão 0=Não)';

exec inter_pr_recompile;
