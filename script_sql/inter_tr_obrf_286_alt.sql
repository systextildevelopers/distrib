create or replace trigger inter_tr_obrf_286_alt
before delete or insert or update on obrf_286
for each row

declare
  PRAGMA Autonomous_Transaction;
  enviado number(1);
  id_var number(9);

begin

    BEGIN
      select enviado
        into enviado
      from obrf_285
      where id = :NEW.id_obrf_285;
      id_var := :NEW.id_obrf_285;
    EXCEPTION
    WHEN OTHERS THEN
      BEGIN
        select enviado
          into enviado
        from obrf_285
        where id = :OLD.id_obrf_285;
        id_var := :OLD.id_obrf_285;
      EXCEPTION
      WHEN OTHERS THEN
        enviado := 0;
      END;
    END;

    if enviado = 1
    then
      BEGIN
        update obrf_285 set ENVIADO = 0, indretif = 2
        where obrf_285.id = id_var;
      EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
      END;
    end if;

    COMMIT;
end;
