-- Create table
create table OBRF_920
( ID_920                    NUMBER(9) default 0 not null,
  COD_EMPRESA               NUMBER(3) default 0 not null,
  MES                       NUMBER(2) default 0 not null,
  ANO                       NUMBER(4) default 0 not null,  
  TIPO_APURACAO             NUMBER(2) default 0 not null,  
  VL_TOT_TRANSF_DEBITOS_OA  NUMBER(15,2) default 0.00,  
  VL_TOT_AJ_DEBITOS_OA      NUMBER(15,2) default 0.00,
  VL_ESTORNOS_CRED_OA       NUMBER(15,2) default 0.00,
  VL_TOT_TRANSF_CREDITOS_OA NUMBER(15,2) default 0.00,
  VL_TOT_AJ_CREDITOS_OA     NUMBER(15,2) default 0.00,
  VL_ESTORNOS_DEB_OA        NUMBER(15,2) default 0.00,
  VL_SLD_CREDOR_ANT_OA      NUMBER(15,2) default 0.00,
  VL_SLD_APURADO_OA         NUMBER(15,2) default 0.00,      
  VL_TOT_DED                NUMBER(15,2) default 0.00,
  VL_ICMS_RECOLHER_OA       NUMBER(15,2) default 0.00,
  VL_SLD_CREDOR_TRANSP_OA   NUMBER(15,2) default 0.00,
  DEB_ESP_OA                NUMBER(15,2) default 0.00);
  
-- Add comments to the columns 
comment on column OBRF_920.COD_EMPRESA               is 'Identifica a empresa';
comment on column OBRF_920.MES                       is 'Mes da apuracao';
comment on column OBRF_920.ANO                       is 'Ano apuracao';
comment on column OBRF_920.VL_TOT_TRANSF_DEBITOS_OA  is '02 Valor total dos d�bitos por �Sa�das e presta��es com d�bito do imposto�.';
comment on column OBRF_920.VL_TOT_AJ_DEBITOS_OA      is '03 Valor total de �Ajustes a d�bito�.';
comment on column OBRF_920.VL_ESTORNOS_CRED_OA       is '04 Valor total de Ajustes �Estornos de cr�ditos�.';
comment on column OBRF_920.VL_TOT_TRANSF_CREDITOS_OA is '05 Valor total dos cr�ditos por �Entradas e aquisi��es com cr�dito do imposto�.';
comment on column OBRF_920.VL_TOT_AJ_CREDITOS_OA     is '06 Valor total de �Ajustes a cr�dito�.';
comment on column OBRF_920.VL_ESTORNOS_DEB_OA        is '07 Valor total de Ajustes �Estornos de D�bitos�.';
comment on column OBRF_920.VL_SLD_CREDOR_ANT_OA      is '08 Valor total de �Saldo credor do per�odo anterior�.';
comment on column OBRF_920.VL_SLD_APURADO_OA         is '09 Valor do saldo devedor apurado.';
comment on column OBRF_920.VL_TOT_DED                is '10 Valor total de �Dedu��es�.';
comment on column OBRF_920.VL_ICMS_RECOLHER_OA       is '11 Valor total de "ICMS a recolher (09-10).';
comment on column OBRF_920.VL_SLD_CREDOR_TRANSP_OA   is '12 Valor total de �Saldo credor a transportar para o per�odo seguinte�.';
comment on column OBRF_920.DEB_ESP_OA                is '13 Valores recolhidos ou a recolher, extraapura��o.';

-- Create/Recreate primary, unique and foreign key constraints 
alter table OBRF_920 add constraint PK_OBRF_920 primary key (ID_920);
alter table OBRF_920 add constraint UNIQ_OBRF_920 unique (COD_EMPRESA, ANO, MES, TIPO_APURACAO);

create synonym systextilrpt.OBRF_920 for OBRF_920; 

create sequence seq_obrf_920
minvalue 0
maxvalue 99999999999999999999
start with 1
increment by 1;

CREATE OR REPLACE TRIGGER INTER_TR_OBRF_920_SEQ
BEFORE INSERT ON OBRF_920 FOR EACH ROW
DECLARE
    next_value number;
BEGIN
    if :new.ID_920 is null or :new.ID_920 = 0 then
        select seq_obrf_920.nextval
        into next_value from dual;
        :new.ID_920 := next_value;
    end if;
END INTER_TR_OBRF_920_SEQ;

/

exec inter_pr_recompile;
