create table estq_049 (data_imagem date, 
                       nivel varchar2(1),
                       grupo varchar2(5),
                       subgrupo varchar2(3),
                       item varchar2(6),
                       cod_deposito number(3),
                       numero_lote number(3),
                       cod_criterio_ciclico number(1) default 0,
                       desc_criterio_ciclico varchar2(100),
		       nr_imagem_ciclico number(9),
                       constraint estq_049_pk primary key (data_imagem, nivel, grupo, subgrupo, item, cod_deposito, numero_lote));

comment on column estq_049.data_imagem is 'Data da imagem do estoque do produto';
comment on column estq_049.nivel is 'Nivel do produto';
comment on column estq_049.grupo is 'Grupo do produto';
comment on column estq_049.subgrupo is 'Subgrupo do produto';
comment on column estq_049.item is 'Item do produto';
comment on column estq_049.cod_deposito is 'Depósito do produto';
comment on column estq_049.numero_lote is 'Numero do lote do produto';
comment on column estq_049.cod_criterio_ciclico is 'Código do criterio da imagem';
comment on column estq_049.desc_criterio_ciclico is 'Descrição do criterio da imagem';
comment on column estq_049.nr_imagem_ciclico is 'Numero da imagem do inventário cíclico';

comment on table estq_049 is 'Imagem do inventário cíclico';

/
