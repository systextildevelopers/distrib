CREATE TABLE  BLOCOK_255 (
	COD_EMPRESA NUMBER(3), 
	ANO_PERIODO_APUR NUMBER(4), 
	MES_PERIODO_APUR NUMBER(2), 
	
	INF_ORIG VARCHAR2(200), 
	CENARIO VARCHAR2(5), 
	ORIGEM VARCHAR2(20), 
	
	COD_ITEM_PROD_ORIG VARCHAR2(60), 
	ID_DOC_ORIG VARCHAR2(200), 
	COD_EMP_DOC_ORIG NUMBER(3), 
	SERIE_DOC_ORIG VARCHAR2(3), 
	NUM_DOC_ORIG NUMBER(9), 
	CGC9_DOC_ORIG NUMBER(9), 
	CGC4_DOC_ORIG NUMBER(4), 
	CGC2_DOC_ORIG NUMBER(2), 
	SEQ_ITE_DOC_ORIG NUMBER(9), 
	
	ORDEM_PRODUCAO NUMBER(9), 
	PERIODO NUMBER(4), 
	PACOTE NUMBER(5), 
	CODIGO_ESTAGIO NUMBER(2), 
	
	DT_CONS VARCHAR2(8), 
	COD_ITEM VARCHAR2(60), 
	QTD NUMBER(15,6), 
	COD_INS_SUBST VARCHAR2(60), 
	
	DATA_HORA_INS DATE DEFAULT sysdate,
	
	COD_ITEM_NIVEL VARCHAR2(1), 
	COD_ITEM_GRUPO VARCHAR2(5), 
	COD_ITEM_SUBGRUPO VARCHAR2(3), 
	COD_ITEM_ITEM VARCHAR2(6), 
	COD_ITEM_EST_AGRUP_INSU NUMBER(2), 
	COD_ITEM_EST_SIMULT_INSU NUMBER(2), 
	
	DT_PROD_ORIG VARCHAR2(8), 
	COD_ITEM_ORIG VARCHAR2(60)
);
