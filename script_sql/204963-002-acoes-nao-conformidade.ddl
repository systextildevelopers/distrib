create table efic_301(
    id number(4),
    constraint efic_301_pk primary key (id),
    descricao varchar2(120));

comment on column efic_301.id is 'Identificador da ação';
comment on column efic_301.descricao is 'Descrição da ação';
comment on table efic_301 is 'Tabela de cadastro de ações para não conformidades';

/
