
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_635_ID" 
before insert on tmrp_635
  for each row

declare
   v_nr_registro number;

begin
   select seq_tmrp_635.nextval into v_nr_registro from dual;

   :new.numero_id := v_nr_registro;

end inter_tr_tmrp_635_id;



-- ALTER TRIGGER "INTER_TR_TMRP_635_ID" ENABLE
 

/

exec inter_pr_recompile;

