alter table obrf_150 add
resp_tec_cgc9 number(9);

alter table obrf_150 add
resp_tec_cgc4 number(9);

alter table obrf_150 add
resp_tec_cgc2 number(9);

alter table obrf_150 add
resp_tec_nome varchar2(40);

alter table obrf_150 add
resp_tec_email varchar2(40);

alter table obrf_150 add
resp_tec_telefone number(9);

alter table obrf_150 add
resp_tec_idCSRT number(2);

alter table obrf_150 add
resp_tec_hashCSRT varchar2(28);

alter table obrf_150 add
entr_nome_receb varchar2(60);

alter table obrf_150 add
entr_cod_cep number(8);

alter table obrf_150 add
entr_cod_pais number(4);

alter table obrf_150 add
entr_nome_pais varchar2(60);

alter table obrf_150 add
entr_telefone varchar2(14);

alter table obrf_150 add
entr_email_receb varchar2(60);

alter table obrf_150 add
entr_inscr_estadual_receb varchar2(14);

comment on column obrf_150.resp_tec_cgc9 is 'CNPJ da pessoa jurídica responsável pelo sistema utilizado na emissão do documento fiscal eletrônico';
comment on column obrf_150.resp_tec_nome is 'Nome da pessoa a ser contatada';
comment on column obrf_150.resp_tec_email is 'E-mail da pessoa jurídica a ser contatada';
comment on column obrf_150.resp_tec_telefone is 'Telefone da pessoa jurídica/física a ser contatada';
comment on column obrf_150.resp_tec_idCSRT is 'Identificador do CSRT utilizado para montar o hash do CSRT';
comment on column obrf_150.resp_tec_hashCSRT is 'O hashCSRT é o resultado da função hash (SHA-1 – Base64) do CSRT fornecido pelo fisco mais a Chave de Acesso da NFe';

comment on column obrf_150.entr_nome_receb is 'Razão Social ou Nome do Recebedor';
comment on column obrf_150.entr_cod_cep is 'Código do CEP';
comment on column obrf_150.entr_cod_pais is 'Código do País';
comment on column obrf_150.entr_nome_pais is 'Nome do País';
comment on column obrf_150.entr_telefone is 'Telefone';
comment on column obrf_150.entr_email_receb is 'Endereço de e-mail do Recebedor';
comment on column obrf_150.entr_inscr_estadual_receb is 'Inscrição Estadual do Estabelecimento Recebedor';
