alter table cobr_003
add (agrupador number(5) default 0);

/

alter table cobr_003
add (libera_baixa number(1) default 0);

comment on column cobr_003.libera_baixa is '0 - permite e 1 - não permite';

exec inter_pr_recompile;
/
