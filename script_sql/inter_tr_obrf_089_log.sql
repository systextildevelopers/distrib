create or replace trigger inter_tr_obrf_089_log
after insert or delete or update
on obrf_089
for each row

declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;

begin

   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                          ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   begin
      select hdoc_090.programa
      into v_nome_programa
      from hdoc_090
      where hdoc_090.sid = ws_sid
        and rownum       = 1
        and hdoc_090.programa not like '%menu%'
        and hdoc_090.programa not like '%!_m%' escape'!';
      exception
        when no_data_found then            v_nome_programa := 'SQL';
   end;

   if inserting
   then
   begin
      insert into obrf_089_log (
    TIPO_OCORR,
    DATA_OCORR
    ,HORA_OCORR
    ,USUARIO_REDE
    ,MAQUINA_REDE
    ,APLICACAO
    ,USUARIO_SISTEMA
    ,NOME_PROGRAMA
    ,ORDEM_SERVICO_OLD
    ,ORDEM_SERVICO_NEW
    ,CODIGO_BARRAS_OLD
    ,CODIGO_BARRAS_NEW
    ,SITUACAO_ITEM_OLD
    ,SITUACAO_ITEM_NEW
    ,QUALIDADE_RETORNO_OLD
    ,QUALIDADE_RETORNO_NEW
    ,SEQUENCIA_OLD
    ,SEQUENCIA_NEW
    ,NUMERO_SOLICITACAO_OLD
    ,NUMERO_SOLICITACAO_NEW
    ,TIPO_SOLICITACAO_CONSER_OLD
    ,TIPO_SOLICITACAO_CONSER_NEW
    ,TAG_ATUALIZA_CONSERTO_OLD
    ,TAG_ATUALIZA_CONSERTO_NEW
    ,NIVEL_ESTRUTURA_OLD
    ,NIVEL_ESTRUTURA_NEW
    ,GRUPO_ESTRUTURA_OLD
    ,GRUPO_ESTRUTURA_NEW
    ,SUBGRUPO_ESTRUTURA_OLD
    ,SUBGRUPO_ESTRUTURA_NEW
    ,ITEM_ESTRUTURA_OLD
    ,ITEM_ESTRUTURA_NEW
    ,ORDEM_PRODUCAO_OLD
    ,ORDEM_PRODUCAO_NEW
    ,ORDEM_CONFECCAO_OLD
    ,ORDEM_CONFECCAO_NEW
    ,PERIODO_PRODUCAO_OLD
    ,PERIODO_PRODUCAO_NEW
    ,SEQ_ORDEM_CONFECCAO_OLD
    ,SEQ_ORDEM_CONFECCAO_NEW
    ,ESTAGIO_OLD
    ,ESTAGIO_NEW
        ) values (
'I',
            sysdate, /*2*/
            sysdate,/*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa /*8*/
,0
,:new.ORDEM_SERVICO
,''
,:new.CODIGO_BARRAS
,0
,:new.SITUACAO_ITEM
,0
,:new.QUALIDADE_RETORNO
,0
,:new.SEQUENCIA
,0
,:new.NUMERO_SOLICITACAO
,0
,:new.TIPO_SOLICITACAO_CONSERTO
,0
,:new.TAG_ATUALIZA_CONSERTO
,''
,:new.NIVEL_ESTRUTURA
,''
,:new.GRUPO_ESTRUTURA
,''
,:new.SUBGRUPO_ESTRUTURA
,''
,:new.ITEM_ESTRUTURA
,0
,:new.ORDEM_PRODUCAO
,0
,:new.ORDEM_CONFECCAO
,0
,:new.PERIODO_PRODUCAO
,0
,:new.SEQ_ORDEM_CONFECCAO
,0
,:new.ESTAGIO
);
end;
end if;
    if updating
    then
    begin
      insert into obrf_089_log (
    TIPO_OCORR,
    DATA_OCORR
    ,HORA_OCORR
    ,USUARIO_REDE
    ,MAQUINA_REDE
    ,APLICACAO
    ,USUARIO_SISTEMA
    ,NOME_PROGRAMA
    ,ORDEM_SERVICO_OLD
    ,ORDEM_SERVICO_NEW
    ,CODIGO_BARRAS_OLD
    ,CODIGO_BARRAS_NEW
    ,SITUACAO_ITEM_OLD
    ,SITUACAO_ITEM_NEW
    ,QUALIDADE_RETORNO_OLD
    ,QUALIDADE_RETORNO_NEW
    ,SEQUENCIA_OLD
    ,SEQUENCIA_NEW
    ,NUMERO_SOLICITACAO_OLD
    ,NUMERO_SOLICITACAO_NEW
    ,TIPO_SOLICITACAO_CONSER_OLD
    ,TIPO_SOLICITACAO_CONSER_NEW
    ,TAG_ATUALIZA_CONSERTO_OLD
    ,TAG_ATUALIZA_CONSERTO_NEW
    ,NIVEL_ESTRUTURA_OLD
    ,NIVEL_ESTRUTURA_NEW
    ,GRUPO_ESTRUTURA_OLD
    ,GRUPO_ESTRUTURA_NEW
    ,SUBGRUPO_ESTRUTURA_OLD
    ,SUBGRUPO_ESTRUTURA_NEW
    ,ITEM_ESTRUTURA_OLD
    ,ITEM_ESTRUTURA_NEW
    ,ORDEM_PRODUCAO_OLD
    ,ORDEM_PRODUCAO_NEW
    ,ORDEM_CONFECCAO_OLD
    ,ORDEM_CONFECCAO_NEW
    ,PERIODO_PRODUCAO_OLD
    ,PERIODO_PRODUCAO_NEW
    ,SEQ_ORDEM_CONFECCAO_OLD
    ,SEQ_ORDEM_CONFECCAO_NEW
    ,ESTAGIO_OLD
    ,ESTAGIO_NEW
        ) values (
'A',
            sysdate, /*2*/
            sysdate,/*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa /*8*/
,:old.ORDEM_SERVICO
,:new.ORDEM_SERVICO
,:old.CODIGO_BARRAS
,:new.CODIGO_BARRAS
,:old.SITUACAO_ITEM
,:new.SITUACAO_ITEM
,:old.QUALIDADE_RETORNO
,:new.QUALIDADE_RETORNO
,:old.SEQUENCIA
,:new.SEQUENCIA
,:old.NUMERO_SOLICITACAO
,:new.NUMERO_SOLICITACAO
,:old.TIPO_SOLICITACAO_CONSERTO
,:new.TIPO_SOLICITACAO_CONSERTO
,:old.TAG_ATUALIZA_CONSERTO
,:new.TAG_ATUALIZA_CONSERTO
,:old.NIVEL_ESTRUTURA
,:new.NIVEL_ESTRUTURA
,:old.GRUPO_ESTRUTURA
,:new.GRUPO_ESTRUTURA
,:old.SUBGRUPO_ESTRUTURA
,:new.SUBGRUPO_ESTRUTURA
,:old.ITEM_ESTRUTURA
,:new.ITEM_ESTRUTURA
,:old.ORDEM_PRODUCAO
,:new.ORDEM_PRODUCAO
,:old.ORDEM_CONFECCAO
,:new.ORDEM_CONFECCAO
,:old.PERIODO_PRODUCAO
,:new.PERIODO_PRODUCAO
,:old.SEQ_ORDEM_CONFECCAO
,:new.SEQ_ORDEM_CONFECCAO
,:old.ESTAGIO
,:new.ESTAGIO
);
end;
end if;
    if deleting
    then
    begin
      insert into obrf_089_log (
    TIPO_OCORR,
    DATA_OCORR
    ,HORA_OCORR
    ,USUARIO_REDE
    ,MAQUINA_REDE
    ,APLICACAO
    ,USUARIO_SISTEMA
    ,NOME_PROGRAMA
    ,ORDEM_SERVICO_OLD
    ,ORDEM_SERVICO_NEW
    ,CODIGO_BARRAS_OLD
    ,CODIGO_BARRAS_NEW
    ,SITUACAO_ITEM_OLD
    ,SITUACAO_ITEM_NEW
    ,QUALIDADE_RETORNO_OLD
    ,QUALIDADE_RETORNO_NEW
    ,SEQUENCIA_OLD
    ,SEQUENCIA_NEW
    ,NUMERO_SOLICITACAO_OLD
    ,NUMERO_SOLICITACAO_NEW
    ,TIPO_SOLICITACAO_CONSER_OLD
    ,TIPO_SOLICITACAO_CONSER_NEW
    ,TAG_ATUALIZA_CONSERTO_OLD
    ,TAG_ATUALIZA_CONSERTO_NEW
    ,NIVEL_ESTRUTURA_OLD
    ,NIVEL_ESTRUTURA_NEW
    ,GRUPO_ESTRUTURA_OLD
    ,GRUPO_ESTRUTURA_NEW
    ,SUBGRUPO_ESTRUTURA_OLD
    ,SUBGRUPO_ESTRUTURA_NEW
    ,ITEM_ESTRUTURA_OLD
    ,ITEM_ESTRUTURA_NEW
    ,ORDEM_PRODUCAO_OLD
    ,ORDEM_PRODUCAO_NEW
    ,ORDEM_CONFECCAO_OLD
    ,ORDEM_CONFECCAO_NEW
    ,PERIODO_PRODUCAO_OLD
    ,PERIODO_PRODUCAO_NEW
    ,SEQ_ORDEM_CONFECCAO_OLD
    ,SEQ_ORDEM_CONFECCAO_NEW
    ,ESTAGIO_OLD
    ,ESTAGIO_NEW
        ) values (
'D',
            sysdate, /*2*/
            sysdate,/*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa /*8*/
,:old.ORDEM_SERVICO
,0
,:old.CODIGO_BARRAS
,''
,:old.SITUACAO_ITEM
,0
,:old.QUALIDADE_RETORNO
,0
,:old.SEQUENCIA
,0
,:old.NUMERO_SOLICITACAO
,0
,:old.TIPO_SOLICITACAO_CONSERTO
,0
,:old.TAG_ATUALIZA_CONSERTO
,0
,:old.NIVEL_ESTRUTURA
,''
,:old.GRUPO_ESTRUTURA
,''
,:old.SUBGRUPO_ESTRUTURA
,''
,:old.ITEM_ESTRUTURA
,''
,:old.ORDEM_PRODUCAO
,0
,:old.ORDEM_CONFECCAO
,0
,:old.PERIODO_PRODUCAO
,0
,:old.SEQ_ORDEM_CONFECCAO
,0
,:old.ESTAGIO
,0
);
      end;
   end if;
end inter_tr_obrf_089_log;
