DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa = 'cpag_f002';
   v_usuario varchar2(200);
   
begin
   for reg_programa in programa
   loop
   
      begin
         select oper_550.usuario 
         into   v_usuario 
         from oper_550 
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'data_pagamento';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa, 
            nome_programa,               nome_subprograma, 
            nome_field,                  requerido, 
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu, 
            reg_programa.programa,       ' ', 
            'data_pagamento',        	 0, 
            0,                           0
         );
         
         commit;
      end;
    
  end loop;
end;

/
