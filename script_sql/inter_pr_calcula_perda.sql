CREATE OR REPLACE PROCEDURE "INTER_PR_CALCULA_PERDA" (
   v_qtde_perda        out number,
   v_periodo_producao  in number,
   v_ordem_confeccao   in number,
   v_ordem_producao    in number,
   v_sequencia_estagio in number,
   v_codigo_estagio    in number
)
is
   v_qtde_perda_a            number;
   v_qtde_perda_b            number;
   v_qtde_perda_b1           number;
   v_qtde_perda_b2           number;
   v_qtde_perda_b3           number;
   v_qtde_perda_b4           number;
   v_qtde_perda_b5           number;
   v_qtde_perda_b6          number;

   v_atu_estagios_seguintes    number;
begin
         /*
               AS OPCOES SAO:
                   0 - Nao atualiza - Somente atualizara o proximo estagio se as quantidades forem lancadas como
                       primeira qualidade
                   1 - Atualiza - Atualiza para o proximo estagio a quantidade lancada independente da qualidade.
         */
         begin
            select fatu_502.atu_estagios_seguintes
            into   v_atu_estagios_seguintes
            from fatu_502, pcpc_010
            where fatu_502.codigo_empresa   =  pcpc_010.codigo_empresa
              and pcpc_010.area_periodo     = 1
              and pcpc_010.periodo_producao = v_periodo_producao;
         exception when OTHERS then
            v_atu_estagios_seguintes := 0;
         end;
/*
   v_periodo_producao  := 106;
   v_ordem_confeccao   := 1321;
   v_ordem_producao    := 13685;
   v_estagio_depende   := 0;
   v_sequencia_estagio := 3;

   v_periodo_producao  := 20;
   v_ordem_confeccao   := 236;
   v_ordem_producao    := 000013679;
   v_estagio_depende   := 0;
   v_sequencia_estagio := 3;
*/

   /*
      Formula:
               v_qtde_perda_a  = maior quantidade de segunda qualidade dos estagios sem estagio dependentes
               v_qtde_perda_b1 = quantidade de segunda qualidade dos estagios que tem estagios dependentes
               v_qtde_perda_b2 = quantidade de segunda qualidade do 1 estagio dependentes
               v_qtde_perda_b3 = quantidade de segunda qualidade do 2 estagio dependentes
               v_qtde_perda_b5 = Soma das quantidades de segunda qualidade dos estagios dependentes
               v_qtde_perda_b  = soma da quantidade dos estagios que tem estagios dependentes

      v_qtde_perda_a + v_qtde_perda_b1 + v_qtde_perda_b5

   */

   if v_sequencia_estagio <> 0
   then
      -- Dos estagios que sao simultaneos, deve buscar a maior segunda qualidade dos estagios que nao tem dependentes
      begin
         select nvl(max(pcpc_040.qtde_perdas + decode(v_atu_estagios_seguintes,0,pcpc_040.qtde_pecas_2a,0)),0)
         into   v_qtde_perda_a
         from pcpc_040
         where pcpc_040.periodo_producao  = v_periodo_producao
           and pcpc_040.ordem_confeccao   = v_ordem_confeccao
           and pcpc_040.ordem_producao    = v_ordem_producao
           and pcpc_040.sequencia_estagio = v_sequencia_estagio
           and (pcpc_040.estagio_depende  = 0
           or   exists (select 1
                        from pcpc_040 pcpc_040a
                        where pcpc_040a.periodo_producao  = pcpc_040.periodo_producao
                          and pcpc_040a.ordem_confeccao   = pcpc_040.ordem_confeccao
                          and pcpc_040a.ordem_producao    = pcpc_040.ordem_producao
                          and pcpc_040a.sequencia_estagio <> pcpc_040.sequencia_estagio
                          and pcpc_040a.codigo_estagio    = pcpc_040.estagio_depende));

      end;

    --  raise_application_error(-20000,'QTDE PERDA -> '||to_char(v_qtde_perda_a));
      v_qtde_perda_b := 0;

      -- Dos estagios que sao simultaneos, deve calcular a maior segunda qualidade dos estagios que tem dependentes
      for reg_qtde_b1 in (select pcpc_040.codigo_estagio
                          from pcpc_040
                          where pcpc_040.periodo_producao  = v_periodo_producao
                            and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                            and pcpc_040.ordem_producao    = v_ordem_producao
                            and pcpc_040.sequencia_estagio = v_sequencia_estagio
                            and (pcpc_040.estagio_depende  = 0
                            or   exists (select 1
                                         from pcpc_040 pcpc_040a
                                         where pcpc_040a.periodo_producao  = pcpc_040.periodo_producao
                                           and pcpc_040a.ordem_confeccao   = pcpc_040.ordem_confeccao
                                           and pcpc_040a.ordem_producao    = pcpc_040.ordem_producao
                                           and pcpc_040a.sequencia_estagio <> pcpc_040.sequencia_estagio
                                           and pcpc_040a.codigo_estagio    = pcpc_040.estagio_depende))
                            and exists (select 1
                                        from pcpc_040 pcpc_040a
                                        where pcpc_040a.periodo_producao  = pcpc_040.periodo_producao
                                          and pcpc_040a.ordem_confeccao   = pcpc_040.ordem_confeccao
                                          and pcpc_040a.estagio_depende   = pcpc_040.codigo_estagio
                                          and pcpc_040a.ordem_producao    = pcpc_040.ordem_producao))
      loop
         begin
            select nvl(max(pcpc_040.qtde_perdas + decode(v_atu_estagios_seguintes,0,pcpc_040.qtde_pecas_2a,0)),0)
            into   v_qtde_perda_b1
            from pcpc_040
            where pcpc_040.periodo_producao  = v_periodo_producao
              and pcpc_040.ordem_confeccao   = v_ordem_confeccao
              and pcpc_040.codigo_estagio    = reg_qtde_b1.codigo_estagio
              and pcpc_040.ordem_producao    = v_ordem_producao
              and pcpc_040.sequencia_estagio = v_sequencia_estagio;
         end;

         v_qtde_perda_b5 := inter_fn_recursiva_perda(v_periodo_producao,v_ordem_confeccao, v_ordem_producao, v_sequencia_estagio,reg_qtde_b1.codigo_estagio,v_atu_estagios_seguintes, v_qtde_perda_b1);



         if v_qtde_perda_b < v_qtde_perda_b5
         then
            v_qtde_perda_b := v_qtde_perda_b5;
         end if;

         /*v_qtde_perda_b5 := 0;
         v_qtde_perda_b2 := 0;

         -- Dos estagios que sao simultaneos e dependentes, busca a maior segunda qualidade do primeiro estagio dependentes
         for reg_filho_qtde_b2 in (select pcpc_040.codigo_estagio
                                   from pcpc_040
                                   where pcpc_040.periodo_producao  = v_periodo_producao
                                     and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                                     and pcpc_040.ordem_producao    = v_ordem_producao
                                     and pcpc_040.estagio_depende   = reg_qtde_b1.codigo_estagio
                                     and pcpc_040.sequencia_estagio = v_sequencia_estagio)
         loop
            begin
               select nvl(max(pcpc_040.qtde_perdas + decode(v_atu_estagios_seguintes,0,pcpc_040.qtde_pecas_2a,0)),0)
               into   v_qtde_perda_b2
               from pcpc_040
               where pcpc_040.periodo_producao  = v_periodo_producao
                 and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                 and pcpc_040.codigo_estagio    = reg_filho_qtde_b2.codigo_estagio
                 and pcpc_040.ordem_producao    = v_ordem_producao
                 and pcpc_040.sequencia_estagio = v_sequencia_estagio;
            end;

            v_qtde_perda_b3 := 0;
            v_qtde_perda_b6 := 0;

            -- Dos estagios que sao simultaneos e dependentes, busca a maior segunda qualidade do segundo estagio dependente
            for reg_filho_qtde_b3 in (select pcpc_040.codigo_estagio
                                      from pcpc_040
                                      where pcpc_040.periodo_producao  = v_periodo_producao
                                        and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                                        and pcpc_040.estagio_depende   = reg_filho_qtde_b2.codigo_estagio
                                        and pcpc_040.ordem_producao    = v_ordem_producao
                                        and pcpc_040.sequencia_estagio = v_sequencia_estagio)
            loop
               begin
                  select nvl(max(pcpc_040.qtde_perdas + decode(v_atu_estagios_seguintes,0,pcpc_040.qtde_pecas_2a,0)),0)
                  into   v_qtde_perda_b3
                  from pcpc_040
                  where pcpc_040.periodo_producao  = v_periodo_producao
                    and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                    and pcpc_040.codigo_estagio    = reg_filho_qtde_b3.codigo_estagio
                    and pcpc_040.ordem_producao    = v_ordem_producao
                    and pcpc_040.sequencia_estagio = v_sequencia_estagio;
              end;

              v_qtde_perda_b4 := 0;


              for reg_filho_qtde_b4 in (select pcpc_040.codigo_estagio
                                        from pcpc_040
                                        where pcpc_040.periodo_producao  = v_periodo_producao
                                          and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                                          and pcpc_040.estagio_depende   = reg_filho_qtde_b3.codigo_estagio
                                          and pcpc_040.ordem_producao    = v_ordem_producao
                                          and pcpc_040.sequencia_estagio = v_sequencia_estagio)
              loop
                 begin
                    select nvl(max(pcpc_040.qtde_perdas + decode(v_atu_estagios_seguintes,0,pcpc_040.qtde_pecas_2a,0)),0)
                    into   v_qtde_perda_b4
                    from pcpc_040
                    where pcpc_040.periodo_producao  = v_periodo_producao
                      and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                      and pcpc_040.codigo_estagio    = reg_filho_qtde_b4.codigo_estagio
                      and pcpc_040.ordem_producao    = v_ordem_producao
                      and pcpc_040.sequencia_estagio = v_sequencia_estagio;
                end;

                 if v_qtde_perda_b6 < v_qtde_perda_b4
                 then
                  v_qtde_perda_b6 :=  v_qtde_perda_b4;
                 end if;

               end loop;

               if v_qtde_perda_b6 < v_qtde_perda_b3
               then
                  v_qtde_perda_b6 :=  v_qtde_perda_b3;
               end if;
            end loop;

            if v_qtde_perda_b5 < v_qtde_perda_b2 + v_qtde_perda_b6
            then
               v_qtde_perda_b5 := v_qtde_perda_b2 + v_qtde_perda_b6;
            end if;
         end loop;

         if v_qtde_perda_b < v_qtde_perda_b5 + v_qtde_perda_b1
         then
            v_qtde_perda_b := v_qtde_perda_b5 + v_qtde_perda_b1;
         end if;*/
      end loop;

      if v_qtde_perda_a > v_qtde_perda_b
      then
         v_qtde_perda := v_qtde_perda_a;
      else
         v_qtde_perda := v_qtde_perda_b;
      end if;
   else
      v_qtde_perda := 0;

      begin
         select nvl(max(pcpc_040.qtde_perdas + decode(v_atu_estagios_seguintes,0,pcpc_040.qtde_pecas_2a,0)),0)
         into   v_qtde_perda
         from pcpc_040
         where pcpc_040.periodo_producao  = v_periodo_producao
           and pcpc_040.ordem_confeccao   = v_ordem_confeccao
           and pcpc_040.codigo_estagio    = v_codigo_estagio
           and pcpc_040.ordem_producao    = v_ordem_producao;
      exception when OTHERS then
         v_qtde_perda := 0;
      end;
   end if;
end inter_pr_calcula_perda;

 

/

exec inter_pr_recompile;
