create or replace TRIGGER inter_tr_pedi_110_log
    after insert or delete or update of
        codigo_deposito
        ,valor_unitario
        ,situacao_fatu_it   
        ,percentual_desc
        ,lote_empenhado     
        ,seq_principal
        ,codigo_acomp        
        ,qtde_pedida
        ,qtde_afaturar      
        ,qtde_faturada
        ,qtde_sugerida      
        ,qtde_distribuida
        ,cod_cancelamento   
        ,um_faturamento_um
        ,um_faturamento_qtde 
        ,um_faturamento_valor
        ,cod_nat_op 
        ,liquida_saldo_aprogramar
        ,centro_custo         
        ,COD_PED_CLIENTE 

    on PEDI_110

for each row

declare

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_descr_situ_fatu_it_o   varchar2(30);
   ws_descr_situ_fatu_it_n   varchar2(30);
   ws_descr_cancelamento_o   varchar2(40);
   ws_descr_cancelamento_n   varchar2(40);
   long_aux                  varchar2(2000);
   v_executa_trigger         number(1);
   ws_sid                    number(9);
   ws_empresa                number(3);

begin

    -- Dados do usuário logado
    inter_pr_dados_usuario (
        ws_usuario_rede
        ,ws_maquina_rede
        ,ws_aplicativo
        ,ws_sid
        ,ws_usuario_systextil
        ,ws_empresa
        ,ws_locale_usuario
    );

    if inserting then
    
        if :new.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;

    end if;

    if updating then

        if :old.executa_trigger = 1 or :new.executa_trigger = 1 then

            v_executa_trigger := 1;

        else
            v_executa_trigger := 0;

        end if;

    end if;

    if deleting then

        if :old.executa_trigger = 1 then

            v_executa_trigger := 1;

        else

            v_executa_trigger := 0;

        end if;
        
    end if;

    if v_executa_trigger = 0 then

        if inserting then

            insert into hist_100(
                tabela
                ,operacao
                ,data_ocorr     
                ,usuario_rede
                ,maquina_rede   
                ,num01
                ,aplicacao        
                ,long01
            )values(
                'PEDI_110'
                ,'I'
                ,sysdate
                ,ws_usuario_rede
                ,ws_maquina_rede
                ,:new.pedido_venda
                ,ws_aplicativo
                ,'                              ' ||(

                    inter_fn_buscar_tag(
                        'lb34980#ITENS DO PEDIDO DE VENDA'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )||chr(10)

                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb05455#SEQUÊNCIA:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.seq_item_pedido
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb00949#SEQ PRINCIPAL:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' ||:new.seq_principal
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb34982#CÓDIGO ACOMP.:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.codigo_acomp
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb00251#NÍVEL:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.cd_it_pe_nivel99

                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb06313#GRUPO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.cd_it_pe_grupo
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb28401#SUBGRUPO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.cd_it_pe_subgrupo
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb03904#ITEM:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.cd_it_pe_item
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb00236#LOTE:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.lote_empenhado

                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb00151#DEPÓSITO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.codigo_deposito
                
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb11173#VALOR UNITÁRIO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.valor_unitario

                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb07806#SITUAÇÃO ITEM:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.situacao_fatu_it
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb34984#%DESC. ITEM:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.percentual_desc
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb15127#QTDE PEDIDA:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.qtde_pedida
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb07732#QTDE. AFATURAR:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.qtde_afaturar
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb10374#QTDE FATURADA:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.qtde_faturada
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb34986#QTDE SUGERIDA:'
                        ,ws_locale_usuario,ws_usuario_systextil
                    )|| ' ' || :new.qtde_sugerida
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb34987#QTDE DISTRIBUIDA:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.qtde_distribuida
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb31282#COD. CANCELAMENTO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.cod_cancelamento
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb36996#UM_UNID.FATUR:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.um_faturamento_um
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb36997#UM_VALOR FATUR:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.um_faturamento_valor
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb36998#UM_QTDE FATUR:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.um_faturamento_qtde
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb11320#NATUREZA OPER.:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )|| ' ' || :new.cod_nat_op
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb41510#LIQUIDA SALDO A PROG.:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :new.liquida_saldo_aprogramar
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb09298#PEDIDO CLIENTE:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :new.COD_PED_CLIENTE
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb00040#CENTRO DE CUSTO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' ' || :new.centro_custo

                )

            );

        end if;

        if updating and (
            :new.codigo_deposito          <> :old.codigo_deposito          or
            :new.valor_unitario           <> :old.valor_unitario           or
            :new.situacao_fatu_it         <> :old.situacao_fatu_it         or
            :new.lote_empenhado           <> :old.lote_empenhado           or
            :new.percentual_desc          <> :old.percentual_desc          or
            :new.seq_principal            <> :old.seq_principal            or
            :new.codigo_acomp             <> :old.codigo_acomp             or
            :new.qtde_pedida              <> :old.qtde_pedida              or
            :new.qtde_afaturar            <> :old.qtde_afaturar            or
            :new.qtde_faturada            <> :old.qtde_faturada            or
            :new.qtde_sugerida            <> :old.qtde_sugerida            or
            :new.qtde_distribuida         <> :old.qtde_distribuida         or
            :new.cod_cancelamento         <> :old.cod_cancelamento         or
            :new.COD_PED_CLIENTE          <> :old.COD_PED_CLIENTE          or
            :new.um_faturamento_um        <> :old.um_faturamento_um        or
            :new.um_faturamento_valor     <> :old.um_faturamento_valor     or
            :new.um_faturamento_qtde      <> :old.um_faturamento_qtde      or
            :new.cod_nat_op               <> :old.cod_nat_op               or
            :new.liquida_saldo_aprogramar <> :old.liquida_saldo_aprogramar or
            :new.centro_custo             <> :old.centro_custo             or
            (:old.cod_ped_cliente is null and :new.cod_ped_cliente is not null)
        ) then

            long_aux := long_aux || '                              ' ||
                inter_fn_buscar_tag(
                    'lb34980#ITENS DO PEDIDO DE VENDA
                    ',ws_locale_usuario
                    ,ws_usuario_systextil
                ) || chr(10) || chr(10);

            long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb05455#SEQUÊNCIA:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :new.seq_item_pedido || chr(10);

            if :new.seq_principal <> :old.seq_principal then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb00949#SEQ PRINCIPAL:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.seq_principal || ' -> ' || :new.seq_principal || chr(10);

            end if;

            if :new.codigo_acomp <> :old.codigo_acomp then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb34982#CÓDIGO ACOMP.:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.codigo_acomp || ' -> ' || :new.codigo_acomp || chr(10);

            end if;

           if ((:old.COD_PED_CLIENTE <> :new.COD_PED_CLIENTE) or (:old.cod_ped_cliente is null and :new.cod_ped_cliente is not null)) then

                long_aux := long_aux ||
                inter_fn_buscar_tag('lb09298#PEDIDO CLIENTE:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.COD_PED_CLIENTE || ' -> ' || :new.COD_PED_CLIENTE      || chr(10);

            end if;

            if :new.codigo_deposito  <> :old.codigo_deposito then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                        'lb00151#DEPÓSITO:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                ) || ' ' || :old.codigo_deposito || ' -> ' || :new.codigo_deposito || chr(10);

            end if;

            if :new.valor_unitario   <> :old.valor_unitario then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb11173#VALOR UNITÁRIO:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.valor_unitario || ' -> ' || :new.valor_unitario || chr(10);

            end if;

            if :new.situacao_fatu_it <> :old.situacao_fatu_it then

                if :old.situacao_fatu_it = 0 then

                    ws_descr_situ_fatu_it_o :=
                        inter_fn_buscar_tag('lb19811#ABERTO'
                            ,ws_locale_usuario
                            ,ws_usuario_systextil
                        );

                end if;

                if :old.situacao_fatu_it = 1 then

                    ws_descr_situ_fatu_it_o :=
                        inter_fn_buscar_tag(
                            'lb24784#FATURADO TOTAL'
                            ,ws_locale_usuario
                            ,ws_usuario_systextil
                        );

                end if;


                if :old.situacao_fatu_it = 2 then
                
                    ws_descr_situ_fatu_it_o :=
                        inter_fn_buscar_tag(
                            'lb24783#FATURADO PARCIAL'
                            ,ws_locale_usuario
                            ,ws_usuario_systextil
                        );

                end if;

                if :new.situacao_fatu_it = 0 then

                    ws_descr_situ_fatu_it_n :=
                        inter_fn_buscar_tag(
                            'lb19811#ABERTO'
                            ,ws_locale_usuario
                            ,ws_usuario_systextil
                        );

                end if;

                if :new.situacao_fatu_it = 1 then

                    ws_descr_situ_fatu_it_n :=
                        inter_fn_buscar_tag(
                            'lb24784#FATURADO TOTAL'
                            ,ws_locale_usuario
                            ,ws_usuario_systextil
                        );

                end if;

                if :new.situacao_fatu_it = 2 then

                    ws_descr_situ_fatu_it_n :=
                        inter_fn_buscar_tag(
                            'lb24783#FATURADO PARCIAL'
                            ,ws_locale_usuario
                            ,ws_usuario_systextil
                        );

                end if;

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb07806#SITUAÇÃO ITEM:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.situacao_fatu_it || ' - ' || ws_descr_situ_fatu_it_o || ' -> ' || chr(10);

                long_aux := long_aux || '                  '  || :new.situacao_fatu_it || ' - '|| ws_descr_situ_fatu_it_n || chr(10);

            end if;

            if :new.percentual_desc <> :old.percentual_desc then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb34984#%DESC. ITEM:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.percentual_desc || ' -> ' || :new.percentual_desc || chr(10);

            end if;

            if :new.lote_empenhado <> :old.lote_empenhado then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb00236#LOTE:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.lote_empenhado || ' -> ' || :new.lote_empenhado || chr(10);

            end if;

            if :new.qtde_pedida <> :old.qtde_pedida then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb15127#QTDE PEDIDA:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.qtde_pedida || ' -> ' || :new.qtde_pedida || chr(10);

            end if;

            if :new.qtde_afaturar <> :old.qtde_afaturar then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb07732#QTDE. AFATURAR:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.qtde_afaturar || ' -> ' || :new.qtde_afaturar     || chr(10);

            end if;

            if :new.qtde_faturada <> :old.qtde_faturada then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb10374#QTDE FATURADA:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.qtde_faturada || ' -> ' || :new.qtde_faturada || chr(10);

            end if;

            if :new.qtde_sugerida <> :old.qtde_sugerida then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb34986#QTDE SUGERIDA:
                    ',ws_locale_usuario,ws_usuario_systextil
                ) || ' ' || :old.qtde_sugerida || ' -> ' || :new.qtde_sugerida || chr(10);

            end if;

            if :new.qtde_distribuida <> :old.qtde_distribuida then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb34987#QTDE DISTRIBUIDA:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.qtde_distribuida || ' -> ' || :new.qtde_distribuida  || chr(10);

            end if;

            if :new.um_faturamento_um <> :old.um_faturamento_um then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb36996#UM_UNID.FATUR:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.um_faturamento_um || ' -> ' || :new.um_faturamento_um  || chr(10);

            end if;

            if :new.um_faturamento_valor <> :old.um_faturamento_valor then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb36997#UM_VALOR FATUR:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.um_faturamento_valor || ' -> ' || :new.um_faturamento_valor || chr(10);

            end if;

            if :new.um_faturamento_qtde <> :old.um_faturamento_qtde then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb36998#UM_QTDE FATUR:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.um_faturamento_qtde || ' -> ' || :new.um_faturamento_qtde || chr(10);

            end if;

            if :new.cod_cancelamento <> :old.cod_cancelamento then

                select
                    nvl(pedi_140.desc_canc_pedido,' ')
                into
                    ws_descr_cancelamento_o
                from pedi_140
                where pedi_140.cod_canc_pedido = :old.cod_cancelamento;

                select
                    nvl(pedi_140.desc_canc_pedido,' ')
                into ws_descr_cancelamento_n
                from pedi_140
                where pedi_140.cod_canc_pedido = :new.cod_cancelamento;

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb31282#COD. CANCELAMENTO:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.cod_cancelamento || ' - ' || ws_descr_cancelamento_o || ' -> ' || chr(10);

                long_aux := long_aux || '                  '  || :new.cod_cancelamento   || ' - '|| ws_descr_cancelamento_n;

            end if;

            if :new.cod_nat_op <> :old.cod_nat_op
            then
                long_aux := long_aux      ||
                inter_fn_buscar_tag('lb11320#NATUREZA OPER.:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.cod_nat_op  || ' -> '
                                        || :new.cod_nat_op  ||
                                        chr(10);
            end if;

            if :new.liquida_saldo_aprogramar <> :old.liquida_saldo_aprogramar then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb41510#LIQUIDA SALDO A PROG.:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' '|| :old.liquida_saldo_aprogramar  || ' -> '|| :new.liquida_saldo_aprogramar  ||chr(10);

            end if;
            
            if :new.centro_custo <> :old.centro_custo then

                long_aux := long_aux ||
                inter_fn_buscar_tag(
                    'lb00040#CENTRO DE CUSTO:'
                    ,ws_locale_usuario
                    ,ws_usuario_systextil
                ) || ' ' || :old.centro_custo || ' -> ' || :new.centro_custo  || chr(10);

            end if;

            /******* FAZ INSERT ******/

            INSERT INTO hist_100(
                tabela
                ,operacao
                ,data_ocorr   
                ,usuario_rede
                ,maquina_rede
                ,num01
                ,aplicacao     
                ,long01
            )VALUES (
                'PEDI_110'      
                ,'A'
                ,sysdate        
                ,ws_usuario_rede
                ,ws_maquina_rede
                ,:new.pedido_venda
                ,ws_aplicativo 
                ,long_aux
            );

        end if;

        if deleting then

            INSERT INTO hist_100(
                tabela        
                ,operacao
                ,data_ocorr  
                ,usuario_rede
                ,maquina_rede  
                ,num01
                ,aplicacao       
                ,long01
            )VALUES (
                'PEDI_110'     
                ,'D'
                ,sysdate         
                ,ws_usuario_rede
                ,ws_maquina_rede
                ,:old.pedido_venda
                ,ws_aplicativo
                ,'                                ' ||(
                    inter_fn_buscar_tag(
                        'lb34980#ITENS DO PEDIDO DE VENDA'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    )||chr(10)
                    
                    ||chr(10)||

                    inter_fn_buscar_tag(
                        'lb05455#SEQUÊNCIA:'
                        ,ws_locale_usuario
                        ,ws_usuario_systextil
                    ) || ' '|| :old.seq_item_pedido
                )
            );

        end if;

   end if;

end inter_tr_pedi_110_log;
