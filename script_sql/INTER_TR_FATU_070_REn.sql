create or replace trigger "INTER_TR_FATU_070_REn"
   before update of num_renegociacao
   on fatu_070
   for each row


begin
  
  :new.ren_num_renegocia := :new.num_renegociacao ;
        
   if :new.num_renegociacao = 0
   then
     :new.ren_sit_renegocia := 0;
  
   else 
      :new.ren_sit_renegocia := 3;
   end if;

end INTER_TR_FATU_070_REn;
/

exec inter_pr_recompile;

