insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('supr_f113', 'Cadastro de ciclo de vida de produtos', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'supr_f113', 'menu_mp00', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'supr_f113', 'menu_mp00', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Registro del ciclo de vida del producto'
 where hdoc_036.codigo_programa = 'supr_f113'
   and hdoc_036.locale          = 'es_ES';
commit;
