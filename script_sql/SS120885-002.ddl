update obrf_601 
set obrf_601.instrucao = 'Causa: Este problema ocorre quando h� um grande volume de consultas no servidor da Sefaz em um per�odo de tempo muito curto.

Como resolver: Para solu��o desse problema, deve-se aguardar por alguns minutos e enviar novamente a nota fiscal.'
where obrf_601.cod_status = 656 ;

commit work;
