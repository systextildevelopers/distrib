create or replace trigger inter_tr_pedi_110_3
  before insert
  on pedi_110
  for each row
declare

  v_usuario_systextil    varchar2(250);
  v_usuario_rede         varchar2(20);
  v_sid                  number(9);
  v_empresa              number(3);
  v_locale_usuario       varchar2(5);
  v_maquina_rede         varchar(40);
  v_aplicativo           varchar(20);

  v_cliente9             pedi_100.cli_ped_cgc_cli9%type;
  v_cliente4             pedi_100.cli_ped_cgc_cli4%type;
  v_cliente2             pedi_100.cli_ped_cgc_cli2%type;
  v_repres               pedi_100.cod_rep_cliente%type;
  v_cod_empresa          pedi_100.codigo_empresa%type;
  v_segmento             pedi_726.codigo_segmento%type;

  v_reg                  number(9);

  v_repres9              pedi_020.cgc_9%type;
  v_repres4              pedi_020.cgc_4%type;
  v_repres2              pedi_020.cgc_2%type;
  v_marca                basi_400.codigo_informacao%type;
  v_catalogo             pedi_100.cod_catalogo%type;

begin

   select count(*) into v_reg
   from pedi_723;

   if v_reg > 0
   then

      -- Dados do usu rio logado
      inter_pr_dados_usuario (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                              v_usuario_systextil,   v_empresa,        v_locale_usuario);

      -- Inf. Pedido
      begin
        select pedi_100.cli_ped_cgc_cli9, pedi_100.cli_ped_cgc_cli4,
               pedi_100.cli_ped_cgc_cli2, pedi_100.cod_rep_cliente,
               pedi_100.codigo_empresa,   pedi_100.cod_catalogo
        into   v_cliente9,                v_cliente4,
               v_cliente2,                v_repres,
               v_cod_empresa,             v_catalogo
        from pedi_100
        where pedi_100.pedido_venda = :new.pedido_venda;
      exception
        when no_data_found then
           v_cliente9 := 0;
           v_cliente4 := 0;
           v_cliente2 := 0;
           v_catalogo := 0;
      end;

      -- Segmento
      begin
        select pedi_726.codigo_segmento
        into   v_segmento
        from pedi_726
        where pedi_726.nivel_produto = :new.cd_it_pe_nivel99
          and pedi_726.grupo_produto = :new.cd_it_pe_grupo
          and pedi_726.subgr_produto = :new.cd_it_pe_subgrupo;
      exception
        when no_data_found then
           v_segmento := 0;
      end;

      if v_segmento > 0
      then   
    
        /*marca*/
        begin 
           select  case when (basi_400.codigo_informacao = 1 and (select basi_544.conteudo_atributo from basi_544 where
                                                                  substr(basi_544.chave_acesso, 1, 1) = basi_400.nivel
                                                                  and substr(basi_544.chave_acesso, 2, 5) = basi_400.grupo
                                                                  and basi_544.codigo_atributo = 25) = 'MAIS MULHER') then 998                                                          
                    when (basi_400.codigo_informacao = 1 and (select basi_544.conteudo_atributo from basi_544 where
                                                              substr(basi_544.chave_acesso, 1, 1) = basi_400.nivel
                                                              and substr(basi_544.chave_acesso, 2, 5) = basi_400.grupo
                                                              and basi_544.codigo_atributo = 25) = 'HITS') then 997
          else basi_400.codigo_informacao end          into v_marca
          from basi_400
          where  basi_400.nivel             = :new.cd_it_pe_nivel99 
             and basi_400.grupo             = :new.cd_it_pe_grupo
             and basi_400.tipo_informacao   = 17;
        exception
        when no_data_found then
           v_marca := 0;
        end;

        begin
          select pedi_020.cgc_9, pedi_020.cgc_4, pedi_020.cgc_2
          into   v_repres9,      v_repres4,      v_repres2
          from pedi_020
          where pedi_020.cod_rep_cliente = v_repres;
        exception
        when no_data_found then
           v_repres9 := 0;
           v_repres4 := 0;
           v_repres2 := 0;
        end;

        if v_repres9 <> v_cliente9 or v_repres4 <> v_cliente4 or v_repres2 <> v_cliente2
        then
          begin
            select count(*) into v_reg
            from pedi_728
            where pedi_728.codigo_segmento = v_segmento
              and pedi_728.cnpj9_cliente   = v_cliente9
              and pedi_728.cnpj4_cliente   = v_cliente4
              and pedi_728.cnpj2_cliente   = v_cliente2
              and pedi_728.marca           = v_marca;

            if v_reg = 0
            then
              begin
                insert into pedi_728 (
                    codigo_empresa,          cnpj9_cliente,
                    cnpj4_cliente,           cnpj2_cliente,
                    codigo_segmento,         codigo_repres,
                    situacao,                data_situacao,
                    marca,                   pedido,
                    cod_catalogo
                    )
                  values (
                    v_cod_empresa,           v_cliente9,
                    v_cliente4,              v_cliente2,
                    v_segmento,              v_repres,
                    'A',                     trunc(sysdate(),'DD'),
                    v_marca,                 :new.pedido_venda,
                    v_catalogo);
              exception
                when others then
                  null;
              end;
            end if;

            select count(*) into v_reg
            from pedi_728
            where pedi_728.codigo_segmento = v_segmento
              and pedi_728.cnpj9_cliente   = v_cliente9
              and pedi_728.cnpj4_cliente   = v_cliente4
              and pedi_728.cnpj2_cliente   = v_cliente2
              and pedi_728.marca           = v_marca
              and pedi_728.situacao        = 'I';

            if v_reg > 0
            then
              begin
                update pedi_728
                set pedi_728.situacao = 'A'
                where pedi_728.codigo_segmento = v_segmento
                  and pedi_728.cnpj9_cliente   = v_cliente9
                  and pedi_728.cnpj4_cliente   = v_cliente4
                  and pedi_728.cnpj2_cliente   = v_cliente2
                  and pedi_728.marca           = v_marca ;

                insert into pedi_728_log (
                   codigo_empresa,          cnpj9_cliente,
                   cnpj4_cliente,           cnpj2_cliente,
                   codigo_segmento,         codigo_repres,
                   data_ocorrencia,         situacao,
                   usuario_rede,            usuario_systextil,
                   cod_motivo,              marca
                   )
                 values (
                   v_cod_empresa,           v_cliente9,
                   v_cliente4,              v_cliente2,
                   v_segmento,              v_repres,
                   trunc(sysdate(),'DD'),   'A',
                   v_usuario_rede,          v_usuario_systextil,
                   1,                       v_marca
                   );
              exception
                when others then
                  null;
              end;
            end if;
          end;
        end if;
      end if;
   end if;

end inter_tr_pedi_110_3;

-- ALTER TRIGGER "INTER_TR_PEDI_110_3" ENABLE
 

/

exec inter_pr_recompile;

