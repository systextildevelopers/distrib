create table NFOR_015
(
  cod_negociacao      NUMBER(9)    default 0 not null,
  sequencia           NUMBER(3)    default 0 not null,
  nivel               VARCHAR2(1)  default '',
  grupo               VARCHAR2(5)  default '',
  subgrupo            VARCHAR2(3)  default '',
  item                VARCHAR2(6)  default '',
  descricao_item      VARCHAR2(60) default '',
  unidade_medida      VARCHAR2(2)  default '',
  cod_fornecedor      VARCHAR2(60) default '',
  unidade_FORNECEDOR  VARCHAR2(2)  default '',
  quantidade          NUMBER(15,3) default 0.0,
  fator_conv          NUMBER(14,6) default 0.000000,
  valor_unitario      NUMBER(15,5) default 0.00000,
  percentual_ipi      NUMBER(6,2)  default 0.0,
  percentual_subs     NUMBER(6,2)  default 0.0,
  rateio_frete        NUMBER(15,2) default 0.0,
  valor_s_Imp         NUMBER(18,3) default 0.00000,
  valor_c_Imp         NUMBER(18,3) default 0.00000,
  cod_barras          VARCHAR2(20) default '',
  bonificado          varchar2(1)  default 'N'
);

ALTER TABLE NFOR_015 ADD CONSTRAINT PK_NFOR_015 PRIMARY KEY (cod_negociacao, sequencia);

ALTER TABLE NFOR_015
ADD CONSTRAINT REF_NFOR_015_NFOR_010 FOREIGN KEY (cod_negociacao) REFERENCES NFOR_010 (cod_negociacao);

ALTER TABLE NFOR_015
ADD CONSTRAINT REF_NFOR_015_BASI_010 FOREIGN KEY (nivel, grupo, subgrupo, item) REFERENCES BASI_010 (NIVEL_ESTRUTURA, GRUPO_ESTRUTURA, SUBGRU_ESTRUTURA, ITEM_ESTRUTURA);


/

exec inter_pr_recompile;
