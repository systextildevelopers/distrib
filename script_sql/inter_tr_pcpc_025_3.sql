
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_025_3" 
     before insert
         or delete
         or update of
         ordem_producao, tamanho, qtde_programada, qtde_marcacoes,
         sequencia_tamanho, qtde_programada_orig
     on pcpc_025
     for each row
declare
   v_periodo_producao  number;
   v_executa_trigger   number;
begin

      if inserting
      then
         if :new.executa_trigger = 1
         then v_executa_trigger := 1;
         else v_executa_trigger := 0;
         end if;
      end if;

      if updating
      then
         if :old.executa_trigger = 1 or :new.executa_trigger = 1
         then v_executa_trigger := 1;
         else v_executa_trigger := 0;
         end if;
      end if;

      if deleting
      then
         if :old.executa_trigger = 1
         then v_executa_trigger := 1;
         else v_executa_trigger := 0;
         end if;
      end if;

      if v_executa_trigger = 0
      then
         if inserting
         then
            begin
               select pcpc_020.periodo_producao
               into v_periodo_producao
               from pcpc_020
               where pcpc_020.ordem_producao = :new.ordem_producao;
            exception when others then
               v_periodo_producao := 0;
            end;

            inter_pr_verif_reg_em_producao (0,0);

            inter_pr_verif_per_fechado(v_periodo_producao,1);
         end if;

         if deleting
         then
            begin
               select pcpc_020.periodo_producao
               into v_periodo_producao
               from pcpc_020
               where pcpc_020.ordem_producao = :old.ordem_producao;
            exception when others then
               v_periodo_producao := 0;
            end;

            inter_pr_verif_reg_em_producao (0,0);

            inter_pr_verif_per_fechado(v_periodo_producao,1);
         end if;
      end if;

end inter_tr_pcpc_025_3;

-- ALTER TRIGGER "INTER_TR_PCPC_025_3" ENABLE
 

/

exec inter_pr_recompile;

