
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_072_1" 
   before delete on pcpc_072
   for each row

declare
   Pragma Autonomous_Transaction;
   v_cont_pack  integer;
   v_tipo_ordem integer;

begin
   if deleting
   then

      begin
         select pcpc_070.tipo_ordem into v_tipo_ordem
         from pcpc_070
         where pcpc_070.ordem_agrupamento = :old.ordem_agrupamento;
         exception
           when others then
             null;
      end;

      begin
         select count(*) into v_cont_pack
         from pcpc_072
         where pcpc_072.ordem_agrupamento = :old.ordem_agrupamento
           and pcpc_072.ordem_producao    = :old.ordem_producao;
      end;

      if v_cont_pack <= 1 and v_tipo_ordem = 4
      then
         begin
            update pcpc_020
               set pcpc_020.ordem_agrup_corte = 0
            where pcpc_020.ordem_producao     = :old.ordem_producao;
            exception
              when others then
                null;
         end;
      end if;
   end if;
   commit;
end inter_tr_pcpc_072_1;
-- ALTER TRIGGER "INTER_TR_PCPC_072_1" ENABLE
 

/

exec inter_pr_recompile;

