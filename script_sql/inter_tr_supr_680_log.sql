CREATE OR REPLACE TRIGGER inter_tr_supr_680_log
AFTER INSERT OR DELETE OR UPDATE
ON supr_680
FOR EACH ROW
DECLARE
    ws_usuario_rede      VARCHAR2(20);
    ws_maquina_rede      VARCHAR2(40);
    ws_aplicativo        VARCHAR2(20);
    ws_sid               NUMBER(9);
    ws_empresa           NUMBER(3);
    ws_usuario_systextil VARCHAR2(20);
    ws_locale_usuario    VARCHAR2(5);
    v_nome_programa      VARCHAR2(20);

BEGIN
    -- Dados do usuário logado
    inter_pr_dados_usuario (
        ws_usuario_rede,
        ws_maquina_rede,
        ws_aplicativo,
        ws_sid,
        ws_usuario_systextil,
        ws_empresa,
        ws_locale_usuario
    );

    BEGIN
        SELECT hdoc_090.programa
        INTO v_nome_programa
        FROM hdoc_090
        WHERE hdoc_090.sid = ws_sid
        AND ROWNUM = 1
        AND hdoc_090.programa NOT LIKE '%menu%'
        AND hdoc_090.programa NOT LIKE '%_m%';
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_nome_programa := 'SQL';
    END;

    IF INSERTING THEN
        BEGIN
            INSERT INTO supr_680_log (
                TIPO_OCORR,        /*0*/
                DATA_OCORR,        /*1*/
                HORA_OCORR,        /*2*/
                USUARIO_REDE,      /*3*/
                MAQUINA_REDE,      /*4*/
                APLICACAO,         /*5*/
                USUARIO_SISTEMA,   /*6*/
                NOME_PROGRAMA,     /*7*/
                id_OLD,            /*8*/
                id_NEW,            /*9*/
                fornecedor9_OLD,   /*10*/
                fornecedor9_NEW,   /*11*/
                fornecedor4_OLD,   /*12*/
                fornecedor4_NEW,   /*13*/
                fornecedor2_OLD,   /*14*/
                fornecedor2_NEW,   /*15*/
                tipo_chave_OLD,    /*16*/
                tipo_chave_NEW,    /*17*/
                chave_OLD,         /*18*/
                chave_NEW,         /*19*/
                padrao_OLD,        /*20*/
                padrao_NEW         /*21*/
            ) VALUES (
                'I',               /*0*/
                SYSDATE,           /*1*/
                SYSDATE,           /*2*/
                ws_usuario_rede,   /*3*/
                ws_maquina_rede,   /*4*/
                ws_aplicativo,     /*5*/
                ws_usuario_systextil,/*6*/
                v_nome_programa,   /*7*/
                0,                 /*8*/
                :NEW.id,           /*9*/
                0,                 /*10*/
                :NEW.fornecedor9,  /*11*/
                0,                 /*12*/
                :NEW.fornecedor4,  /*13*/
                0,                 /*14*/
                :NEW.fornecedor2,  /*15*/
                0,                 /*16*/
                :NEW.tipo_chave,   /*17*/
                '',                /*18*/
                :NEW.chave,        /*19*/
                '',                /*20*/
                :NEW.padrao        /*21*/
            );
        END;
    END IF;

    IF UPDATING THEN
        BEGIN
            INSERT INTO supr_680_log (
                TIPO_OCORR,        /*0*/
                DATA_OCORR,        /*1*/
                HORA_OCORR,        /*2*/
                USUARIO_REDE,      /*3*/
                MAQUINA_REDE,      /*4*/
                APLICACAO,         /*5*/
                USUARIO_SISTEMA,   /*6*/
                NOME_PROGRAMA,     /*7*/
                id_OLD,            /*8*/
                id_NEW,            /*9*/
                fornecedor9_OLD,   /*10*/
                fornecedor9_NEW,   /*11*/
                fornecedor4_OLD,   /*12*/
                fornecedor4_NEW,   /*13*/
                fornecedor2_OLD,   /*14*/
                fornecedor2_NEW,   /*15*/
                tipo_chave_OLD,    /*16*/
                tipo_chave_NEW,    /*17*/
                chave_OLD,         /*18*/
                chave_NEW,         /*19*/
                padrao_OLD,        /*20*/
                padrao_NEW         /*21*/
            ) VALUES (
                'A',               /*0*/
                SYSDATE,           /*1*/
                SYSDATE,           /*2*/
                ws_usuario_rede,   /*3*/
                ws_maquina_rede,   /*4*/
                ws_aplicativo,     /*5*/
                ws_usuario_systextil,/*6*/
                v_nome_programa,   /*7*/
                :OLD.id,           /*8*/
                :NEW.id,           /*9*/
                :OLD.fornecedor9,  /*10*/
                :NEW.fornecedor9,  /*11*/
                :OLD.fornecedor4,  /*12*/
                :NEW.fornecedor4,  /*13*/
                :OLD.fornecedor2,  /*14*/
                :NEW.fornecedor2,  /*15*/
                :OLD.tipo_chave,   /*16*/
                :NEW.tipo_chave,   /*17*/
                :OLD.chave,        /*18*/
                :NEW.chave,        /*19*/
                :OLD.padrao,       /*20*/
                :NEW.padrao        /*21*/
            );
        END;
    END IF;

    IF DELETING THEN
        BEGIN
            INSERT INTO supr_680_log (
                TIPO_OCORR,        /*0*/
                DATA_OCORR,        /*1*/
                HORA_OCORR,        /*2*/
                USUARIO_REDE,      /*3*/
                MAQUINA_REDE,      /*4*/
                APLICACAO,         /*5*/
                USUARIO_SISTEMA,   /*6*/
                NOME_PROGRAMA,     /*7*/
                id_OLD,            /*8*/
                id_NEW,            /*9*/
                fornecedor9_OLD,   /*10*/
                fornecedor9_NEW,   /*11*/
                fornecedor4_OLD,   /*12*/
                fornecedor4_NEW,   /*13*/
                fornecedor2_OLD,   /*14*/
                fornecedor2_NEW,   /*15*/
                tipo_chave_OLD,    /*16*/
                tipo_chave_NEW,    /*17*/
                chave_OLD,         /*18*/
                chave_NEW,         /*19*/
                padrao_OLD,        /*20*/
                padrao_NEW         /*21*/
            ) VALUES (
                'D',               /*0*/
                SYSDATE,           /*1*/
                SYSDATE,           /*2*/
                ws_usuario_rede,   /*3*/
                ws_maquina_rede,   /*4*/
                ws_aplicativo,     /*5*/
                ws_usuario_systextil,/*6*/
                v_nome_programa,   /*7*/
                :OLD.id,           /*8*/
                0,                 /*9*/
                :OLD.fornecedor9,  /*10*/
                0,                 /*11*/
                :OLD.fornecedor4,  /*12*/
                0,                 /*13*/
                :OLD.fornecedor2,  /*14*/
                0,                 /*15*/
                :OLD.tipo_chave,   /*16*/
                0,                 /*17*/
                :OLD.chave,        /*18*/
                '',                /*19*/
                :OLD.padrao,       /*20*/
                ''                 /*21*/
            );
        END;
    END IF;

END inter_tr_supr_680_log;
