  CREATE OR REPLACE TRIGGER "INTER_TR_MQOP_005_LOG" 
  before insert or delete or
  update of desc_estagio,codigo_estagio,cod_estagio_agrupador
  on mqop_005
  for each row
declare
  -- local variables here

  v_desc_estagio_new				mqop_005.desc_estagio%type;
  v_desc_estagio_old				mqop_005.desc_estagio%type;
  v_cod_estagio_agrupador_new		mqop_005.cod_estagio_agrupador%type;
  v_cod_estagio_agrupador_old		mqop_005.cod_estagio_agrupador%type;
  v_codigo_estagio					mqop_005.codigo_estagio%type;

  v_operacao              varchar(1);
  v_data_operacao         date;
  v_usuario_rede          hdoc_030.usuario%type;
  v_maquina_rede          varchar(40);
  v_aplicativo            varchar(20);
  ws_sid                  number(9);
  ws_nome_programa        hdoc_090.programa%type;


begin
    -- Grava a data/hora da inser��o do registro (log)
   v_data_operacao := sysdate();

   -- Encontra dados do usu�rio da rede, m�quina e aplicativo que esta atualizando a ficha
   select substr(osuser,1,20),
          substr(machine,1,40),
          substr(program,1,20)
   into v_usuario_rede,
        v_maquina_rede,
        v_aplicativo
   from sys.gv_$session
   where audsid  = userenv('SESSIONID')
     and inst_id = userenv('INSTANCE')
     and rownum < 2;

   select sid
   into ws_sid
   from sys.v_$session
   where audsid = userenv('SESSIONID');

   ws_nome_programa := inter_fn_nome_programa(ws_sid); 

   --alimenta as vari�veis new caso seja insert ou update
   if inserting or updating
   then
      if inserting
      then v_operacao := 'I';
      else v_operacao := 'U';
      end if;

     v_desc_estagio_new       		:= :new.desc_estagio;
	 v_cod_estagio_agrupador_new	:= :new.cod_estagio_agrupador;
	 v_codigo_estagio				:= :new.codigo_estagio;

   end if; --fim do if inserting or updating

   --alimenta as vari�veis old caso seja insert ou update
   if deleting or updating
   then
      if deleting
      then v_operacao := 'D';
      else v_operacao := 'U';
      end if;

      v_desc_estagio_old        	:= :old.desc_estagio;
	  v_cod_estagio_agrupador_old	:= :old.cod_estagio_agrupador;
	  v_codigo_estagio				:= :old.codigo_estagio;
	  
   end if; --fim do if inserting or updating



   --insere na mqop_005_log o registro.

   insert into mqop_005_log (
     desc_estagio_old,		   		desc_estagio_new,			
	 cod_estagio_agrupador_old,		cod_estagio_agrupador_new,
     operacao,                 		data_operacao,
     usuario_rede,             		maquina_rede,
     aplicativo,               		programa,
	 codigo_estagio)
   values (
     v_desc_estagio_old,       		v_desc_estagio_new,
	 v_cod_estagio_agrupador_old,	v_cod_estagio_agrupador_new,		
     v_operacao,               		v_data_operacao,
     v_usuario_rede,           		v_maquina_rede,
     v_aplicativo,                 ws_nome_programa,
   v_codigo_estagio);
   exception
      when OTHERS
      then raise_application_error (-20000, 'N�o atualizou a tabela de log da mqop_005.');

end inter_tr_mqop_005_log;
/

exec inter_pr_recompile;

