CREATE OR REPLACE FUNCTION "INTER_FN_COD_PRODUTO_SINTEGRA"
        (coditem_nivel99  varchar2,
         coditem_grupo    varchar2,
         coditem_subgrupo varchar2,
         coditem_item     varchar2,
         pcod_empresa      obrf_158.cod_empresa%type,
         pserie            varchar2,
         pdata_emissao   date)
RETURN varchar2
IS
   produto                  varchar2(18);
   rpt_saida                fatu_505.rpt_nota_saida%type;
   rpt_entrada              fatu_505.rpt_nota_entrada%type;
   tp_impressao_cod_produto obrf_158.tp_impressao_cod_prod%type;
   v_dtiniformtprod              date;
begin
   tp_impressao_cod_produto := 1;
   begin
       select fatu_505.rpt_nota_saida
       into rpt_saida
       from fatu_505
       where fatu_505.codigo_empresa = pcod_empresa
         and fatu_505.serie_nota_fisc = pserie
         and rownum = 1;

       select fatu_505.rpt_nota_entrada
       into rpt_entrada
       from fatu_505
       where fatu_505.codigo_empresa = pcod_empresa
         and fatu_505.serie_nota_fisc = pserie
         and rownum = 1;
      
       begin    
         select val_dat
         into v_dtiniformtprod
         from empr_008
         where empr_008.codigo_empresa = pcod_empresa
         and empr_008.param = 'obrf.dataIniFormtProd';
       exception
       when no_data_found then
         v_dtiniformtprod := sysdate + 3365;
       end;
         
       exception                     
           when others then          
              rpt_entrada := ' ';    
              rpt_saida   := ' ';    
              
   end;
   begin
       select obrf_158.tp_impressao_cod_prod
       into tp_impressao_cod_produto
       from obrf_158
       where obrf_158.cod_empresa = pcod_empresa
         and rownum = 1;
   end;
   if rpt_saida = 'obrf_nfs047' or  rpt_entrada = 'obrf_nfe047' --Minasa
   then
      produto       := coditem_nivel99 || coditem_grupo           || coditem_subgrupo      || substr(coditem_item,2,6);
   elsif tp_impressao_cod_produto = 1 or v_dtiniformtprod <= pdata_emissao --Divide codigo do produto com pontos
   then
      produto       := coditem_nivel99 ||'.'|| coditem_grupo ||'.'|| coditem_subgrupo ||'.'|| coditem_item;
   else --N?o divide codigo com pontos
      produto       := coditem_nivel99 || coditem_grupo || coditem_subgrupo || coditem_item;
   end if;
   return(produto);
end inter_fn_cod_produto_sintegra;
 
/
