
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_010_1"
  before insert
  on obrf_010
  for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   nro_reg                   number;
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if :new.usuario_digitacao is null or :new.usuario_digitacao = ' '
   then :new.usuario_digitacao := ws_usuario_systextil;
   end if;

   if :new.local_impressao = 0
   then
      select nvl(count(*),0)
      into nro_reg
      from hdoc_060
      where (hdoc_060.codigo_empresa = :new.local_entrega
        or   hdoc_060.codigo_empresa = 999)
        and hdoc_060.nome_usuario    = ws_usuario_systextil
        and hdoc_060.tipo_operacao   = 31;

      if nro_reg > 0
      then
         begin
            select hdoc_060.chave_numerica
            into :new.local_impressao
            from hdoc_060
            where (hdoc_060.codigo_empresa = :new.local_entrega
              or   hdoc_060.codigo_empresa = 999)
              and hdoc_060.nome_usuario    = ws_usuario_systextil
              and hdoc_060.tipo_operacao   = 31
              and rownum                   = 1
            order by hdoc_060.codigo_empresa;
         end;
      end if;
   end if;
end inter_tr_obrf_010_1;

-- ALTER TRIGGER "INTER_TR_OBRF_010_1" ENABLE


/

exec inter_pr_recompile;
