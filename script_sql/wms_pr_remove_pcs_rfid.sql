create or replace procedure wms_pr_remove_pcs_rfid (p_cod_rfid         in  varchar2,
                                                    p_limpar           in  number,
                                                    p_periodo_tag      in  number,
                                                    p_ordem_prod_tag   in  number,
                                                    p_ordem_conf_tag   in  number,
                                                    p_sequencia_tag    in  number,
                                                    p_quantidade       in  number,
                                                    p_liberar_st       in  number) is
  v_resultado_int   number;
  v_tmp_int         number;
begin

   -- valida RFID se existe
   begin
      select 1
      into   v_tmp_int
      from inte_wms_rfid
      where inte_wms_rfid.rfid_caixa = p_cod_rfid
        and rownum                   <= 1;
   exception
      when no_data_found then
         raise_application_error(-20000, 'ATEN��O! O c�digo de RFID n�o existe na tabela INTE_WMS_RFID.');
   end;

   --verifica se o tag existe na tablea inte_wms_rfid_tag
   if p_periodo_tag <> 0 and p_ordem_prod_tag <> 0 and p_ordem_conf_tag <> 0 and p_sequencia_tag <> 0
   then
      begin
         select 1
         into   v_tmp_int
         from inte_wms_rfid_tags
         where inte_wms_rfid_tags.rfid_caixa     = p_cod_rfid
           and inte_wms_rfid_tags.periodo_tag    = p_periodo_tag
           and inte_wms_rfid_tags.ordem_prod_tag = p_ordem_prod_tag
           and inte_wms_rfid_tags.ordem_conf_tag = p_ordem_conf_tag
           and inte_wms_rfid_tags.sequencia_tag  = p_sequencia_tag
           and rownum                            <= 1;
      exception
         when no_data_found then
            --N�o pode limpar e informar a quantidade
            raise_application_error(-20000, 'ATEN��O! N�o foi encontrado esta tag para a caixa RFID na integra��o.');
      end;
   end if;

   if p_limpar <> 0 and p_quantidade <> 0
   then
      --N�o pode limpar e informar a quantidade
      raise_application_error(-20000, 'ATEN��O! OPERA��O INV�LIDA! N�o � poss�vel LIMPAR a Caixa RFID informando o Par�metro Quantidade. O mesmo deve ser Zero, quando utilizar a op��o LIMPAR Caixa.');
   end if;

   if p_periodo_tag <> 0 and p_ordem_prod_tag <> 0 and p_ordem_conf_tag <> 0 and p_sequencia_tag <> 0 and
      p_quantidade   > 1
   then
      --N�o pode informar Tag e Quantidade maior que 1 (tem que ser Zero ou Um)
      raise_application_error(-20000, 'ATEN��O! OPERA��O INV�LIDA! N�o � poss�vel informar uma TAG espec�fica a ser removida da Caixa RFID com o Par�metro Quantidade maior que 1. J� que a Tag � unit�ria, o par�metro Quantidade deve ser Zerado ou 1 quando se informar uma Tag.');
   end if;

   if p_liberar_st = 0
   then
/*
   '0'-Default
      Quando for recebida esta op��o n�o deve ser feito nada, no entanto caso tenha sido informado LIMPAR e a
      caixa for de TAGs ou caso tenha sido informada uma Tag nos par�metros, n�o deve permitir utilizar o
      default, gerando uma exce��o. Neste caso deve ser informado apenas '1' ou '2' quando for Tag. Quando
      n�o for Tag deve ser informado sempre '0'-Default.
   '1'-N�o Libera Tag
      Quando for recebida esta op��o ao remover uma Tag da caixa RFID, antes de excluir a Tag da integra��o
      (INTE_WMS_RFID_TAGS), deve atualizar o campo LIBERAR_TAG_ST da tabela para 1. Desta forma, a trigger
      n�o limpar� a informa��o da caixa RFID da Tag na pcpc_330.
   '2'-Libera Tag ST
      Quando for recebida esta op��o ao remover uma Tag da caixa RFID, antes de excluir a Tag da integra��o
      (INTE_WMS_RFID_TAGS), deve atualizar o campo LIBERAR_TAG_ST da tabela para 2. Desta forma, a trigger
      limpar� a informa��o da caixa RFID da Tag na pcpc_330, permitindo que a Tag seja utilizada/movimentada
      no Syst�xtil.
*/

      --Se for remover pe�as e existir Tag, deve ser informado o campo p_liberar_st para que se saiba o que fazer

      begin
         select 1
         into   v_resultado_int
         from inte_wms_rfid_tags
         where inte_wms_rfid_tags.rfid_caixa = p_cod_rfid
           and rownum                       <= 1;
      exception
         when no_data_found then
            v_resultado_int := 0;
      end;

      if v_resultado_int = 1  --Se encontrou Tag, deve usar liberar_tag_st = 1 ou 2.
      then
         raise_application_error(-20000, 'ATEN��O! OPERA��O INV�LIDA! Esta Caixa RFID � de Tags, deve ser informado no par�metro p_liberar_st = 1 ou 2, indicando a a��o a ser realizada.');
      end if;

   end if; --fim do p_liberar_st = 0

   if p_limpar > 0 --Limpar a Caixa RFID
   then

      --Atualizar a a��o da Libera��o das Tags
      begin
         update inte_wms_rfid_tags
         set    inte_wms_rfid_tags.liberar_tag_st = p_liberar_st
         where  inte_wms_rfid_tags.rfid_caixa     = p_cod_rfid;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela INTE_WMS_RFID_TAGS.' || Chr(10) || SQLERRM);
      end;

      --Limpar as Tags da Caixa (inte_wms_rfid_tags)
      begin
         delete inte_wms_rfid_tags
         where  inte_wms_rfid_tags.rfid_caixa = p_cod_rfid;
      exception
          when others then
             raise_application_error(-20000, 'N�o eliminou tabela INTE_WMS_RFID_TAGS.' || Chr(10) || SQLERRM);
      end;

      --Limpar a Caixa (inte_wms_rfid) --Excluir se n�o tem mais nada dentro devido a RFIDs inv�lidos.
      begin
         delete inte_wms_rfid
         where  inte_wms_rfid.rfid_caixa      = p_cod_rfid;
      exception
          when others then
             raise_application_error(-20000, 'N�o eliminou tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);
      end;

   end if; --fim do p_limpar > 0

   if p_limpar = 0 --N�o limpar a Caixa RFID
   then

      --Verificar se � para tirar uma Tag ou 1 Pe�a (EAN)
      if p_periodo_tag <> 0 and p_ordem_prod_tag <> 0 and p_ordem_conf_tag <> 0 and p_sequencia_tag <> 0
      then

         --Remover uma Tag
         --Atualizar a a��o da Libera��o das Tags
         begin
            update inte_wms_rfid_tags
            set    inte_wms_rfid_tags.liberar_tag_st = p_liberar_st
            where  inte_wms_rfid_tags.rfid_caixa     = p_cod_rfid
              and  inte_wms_rfid_tags.periodo_tag    = p_periodo_tag
              and  inte_wms_rfid_tags.ordem_prod_tag = p_ordem_prod_tag
              and  inte_wms_rfid_tags.ordem_conf_tag = p_ordem_conf_tag
              and  inte_wms_rfid_tags.sequencia_tag  = p_sequencia_tag;
         exception
             when others then
                raise_application_error(-20000, 'N�o atualizou tabela INTE_WMS_RFID_TAGS.' || Chr(10) || SQLERRM);
         end;

         --Remover a Tag da Caixa (inte_wms_rfid_tags)
         begin
            delete inte_wms_rfid_tags
            where  inte_wms_rfid_tags.rfid_caixa     = p_cod_rfid
              and  inte_wms_rfid_tags.periodo_tag    = p_periodo_tag
              and  inte_wms_rfid_tags.ordem_prod_tag = p_ordem_prod_tag
              and  inte_wms_rfid_tags.ordem_conf_tag = p_ordem_conf_tag
              and  inte_wms_rfid_tags.sequencia_tag  = p_sequencia_tag;
         exception
             when others then
                raise_application_error(-20000, 'N�o eliminou tabela INTE_WMS_RFID_TAGS.' || Chr(10) || SQLERRM);
         end;

         --Atualizar a Caixa (inte_wms_rfid)
         begin
            update inte_wms_rfid
            set    inte_wms_rfid.qtd_pecas        = inte_wms_rfid.qtd_pecas - 1
            where  inte_wms_rfid.rfid_caixa       = p_cod_rfid;
         exception
             when others then
                raise_application_error(-20000, 'N�o atualizou tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);
         end;

      else

         --Remover uma Pe�a (EAN)

         --Atualizar a Caixa (inte_wms_rfid)
         begin
            update inte_wms_rfid
            set    inte_wms_rfid.qtd_pecas        = inte_wms_rfid.qtd_pecas - p_quantidade
            where  inte_wms_rfid.rfid_caixa       = p_cod_rfid;
         exception
             when others then
                raise_application_error(-20000, 'N�o atualizou tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);
         end;

      end if;

      --Verificar se a caixa ficou zerada. Se ficou, excluir.
      v_resultado_int := 0;
      begin
         select inte_wms_rfid.qtd_pecas
         into   v_resultado_int
         from inte_wms_rfid
         where inte_wms_rfid.rfid_caixa = p_cod_rfid
           and rownum                  <= 1;
      exception
         when no_data_found then
            v_resultado_int := 0;
      end;

      if v_resultado_int <= 0
      then

         --Limpar a Caixa (inte_wms_rfid) --Excluir se n�o tem mais nada dentro devido a RFIDs inv�lidos.
         begin
            delete inte_wms_rfid
            where  inte_wms_rfid.rfid_caixa  = p_cod_rfid;
         exception
             when others then
                raise_application_error(-20000, 'N�o eliminou tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);
         end;

      end if; --fim do v_resultado_int <= 0

   end if; --fim do p_limpar = 0

   -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
   -- commit;

end wms_pr_remove_pcs_rfid;

/

exec inter_pr_recompile;

/* versao: 4 */


 exit;


 exit;


 exit;

 exit;
