create table pedi_066 (
  banco	     		      number(3)      default 0,
  ocorrencia	      	  number(2)      default 0.0,     
  descricao    		  	  varchar2(60)   default ' '
);

alter table pedi_066 
	add constraint pk_pedi_066 primary key (banco,ocorrencia);
		
create synonym systextilrpt.pedi_066 for pedi_066; 

comment on table pedi_066
	is 'Tabela Para Registro de Ocorrência de Exceção';
	
comment on column pedi_066.banco
	is 'Código do banco';
	
comment on column pedi_066.ocorrencia
	is 'Código da ocorrência';
	
comment on column pedi_066.descricao
	is 'Descrição da ocorrência';

exec inter_pr_recompile;
