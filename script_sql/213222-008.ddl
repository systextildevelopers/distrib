alter table manu_010 add (cod_parte number(4) default 0 not null, cod_comp number(4) default 0 not null);

ALTER TABLE manu_010 DROP CONSTRAINT REF_MANU_010_MANU_005;

ALTER TABLE manu_010
DROP CONSTRAINT PK_MANU_010;
create index PK_MANU_010 on manu_010 (grupo_maquina,subgru_maquina,numero_maquina,c_custo_resp,tipo_problema,tipo_servico);

drop index PK_MANU_010;

ALTER TABLE manu_010
ADD CONSTRAINT PK_MANU_010 PRIMARY KEY
(grupo_maquina,subgru_maquina,numero_maquina,c_custo_resp,tipo_problema,tipo_servico,cod_parte,cod_comp);

alter table manu_005 add (cod_parte number(4) default 0 not null, cod_comp number(4) default 0 not null);

ALTER TABLE manu_005
DROP CONSTRAINT PK_MANU_005;
create index PK_MANU_005 on manu_005 (grupo_maquina,subgru_maquina,numero_maquina,c_custo_resp,tipo_problema,tipo_servico);

drop index PK_MANU_005;

ALTER TABLE manu_005
ADD CONSTRAINT PK_MANU_005 PRIMARY KEY
(grupo_maquina,subgru_maquina,numero_maquina,c_custo_resp,tipo_problema,tipo_servico,cod_parte,cod_comp);


alter table manu_010 add constraint REF_MANU_010_MANU_005 foreign key (grupo_maquina,subgru_maquina,numero_maquina,c_custo_resp,tipo_problema,tipo_servico,cod_parte,cod_comp)
references manu_005 (grupo_maquina,subgru_maquina,numero_maquina,c_custo_resp,tipo_problema,tipo_servico,cod_parte,cod_comp);
