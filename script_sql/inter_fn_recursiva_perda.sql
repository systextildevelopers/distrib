CREATE OR REPLACE FUNCTION "INTER_FN_RECURSIVA_PERDA" (
   v_periodo_producao  in number,
   v_ordem_confeccao   in number,
   v_ordem_producao    in number,
   v_sequencia_estagio in number,
   v_codigo_estagio    in number,
   v_atu_estagios_seguintes  in  number,
   v_qtde_perda_ant in number
     )
   return number
is
    v_qtde_perda          number;
    v_qtde_perda_result   number;
    v_qtde_perda_est      number;
    v_qtde_perda_reg      number;
begin

   v_qtde_perda_result := v_qtde_perda_ant;

   for reg_filho_qtde in (select pcpc_040.codigo_estagio
                          from pcpc_040
                          where pcpc_040.periodo_producao  = v_periodo_producao
                            and pcpc_040.ordem_confeccao   = v_ordem_confeccao
                            and pcpc_040.ordem_producao    = v_ordem_producao
                            and pcpc_040.estagio_depende   = v_codigo_estagio
                            and pcpc_040.sequencia_estagio = v_sequencia_estagio)
   loop
      begin
         select nvl(max(pcpc_040.qtde_perdas + decode(v_atu_estagios_seguintes,0,pcpc_040.qtde_pecas_2a,0)),0)
         into   v_qtde_perda_reg
         from pcpc_040
         where pcpc_040.periodo_producao  = v_periodo_producao
           and pcpc_040.ordem_confeccao   = v_ordem_confeccao
           and pcpc_040.codigo_estagio    = reg_filho_qtde.codigo_estagio
           and pcpc_040.ordem_producao    = v_ordem_producao
           and pcpc_040.sequencia_estagio = v_sequencia_estagio;
       end;

       v_qtde_perda_est := v_qtde_perda_reg + v_qtde_perda_ant;

       v_qtde_perda := inter_fn_recursiva_perda(v_periodo_producao, v_ordem_confeccao, v_ordem_producao, v_sequencia_estagio, reg_filho_qtde.codigo_estagio, v_atu_estagios_seguintes, v_qtde_perda_est);

       if v_qtde_perda_result < v_qtde_perda
       then v_qtde_perda_result := v_qtde_perda;
       end if;

    end loop;


   return v_qtde_perda_result;
end;

/

exec inter_pr_recompile;
