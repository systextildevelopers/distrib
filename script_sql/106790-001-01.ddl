create table crec_730
(
  COD_EMPRESA              NUMBER(3)    default 0 not null,
  COD_EMP_SCPC             NUMBER(8)    default 0,  
  NR_REMESSA               NUMBER(9)    default 0,
  DIAS_ATRASO              NUMBER(3)    default 99,
  AMBIENTE                 NUMBER(1)    default 1);
  
comment on column crec_730.COD_EMPRESA   is 'Código da empresa no sistema';
comment on column crec_730.COD_EMP_SCPC  is 'Código da empresa no SCPC';
comment on column crec_730.NR_REMESSA    is 'Contador do Numero da remessa  para SCPC';
comment on column crec_730.DIAS_ATRASO   is 'Numero de dias de atraso para que o título seja remetido a SCPC';
comment on column crec_730.AMBIENTE      is 'Ambiente para SCPC 0 - producao ou 1 - testes';

ALTER TABLE crec_730 ADD CONSTRAINT PK_crec_730 PRIMARY KEY (COD_EMPRESA);
  
create synonym systextilrpt.crec_730 for crec_730; 


alter table fatu_070 add( 
scpc_situacao    number(1) ,
scpc_nr_remessa  number(9) 
); 

alter table fatu_070
modify scpc_situacao default 0;

alter table fatu_070
modify scpc_nr_remessa default 0;

comment on column fatu_070.scpc_situacao     is 'Situacao do titulo junto a SCPC,  0 - Nao enviado , 1 - Selecionado, 5 - Enviado Inclusão ou 4 - Gerada remessa';
comment on column fatu_070.scpc_nr_remessa   is 'Numero da remessa  para SCPC';
