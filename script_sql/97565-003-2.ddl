alter table fatu_060_hist add(
        perc_fcp_uf_dest_old    number(8,4),
        perc_fcp_uf_dest_new    number(8,4),
        perc_icms_uf_dest_old   number(8,4),
        perc_icms_uf_dest_new   number(8,4),
        perc_icms_partilha_old  number(8,4),
        perc_icms_partilha_new  number(8,4),
        val_fcp_uf_dest_old     number(15,5),
        val_fcp_uf_dest_new     number(15,5),
        val_icms_uf_dest_old    number(15,5),
        val_icms_uf_dest_new    number(15,5),
        val_icms_uf_remet_old   number(15,5),
        val_icms_uf_remet_new   number(15,5));

exec inter_pr_recompile;
/