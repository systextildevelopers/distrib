create table oper_281
(
   cod_empresa    		number(3)     default 0,
   cod_tipo_aviamento   varchar2(2)   default ''
);

alter table oper_281 add constraint pk_oper_281 primary key (cod_empresa,cod_tipo_aviamento);

exec inter_pr_recompile;
