create or replace trigger inter_tr_pedi_100_inte 
    before delete or  
           update of
  			cod_preposto, aceita_antecipacao, permite_parcial, usuario_cadastro,
  			cli9resptit, cli4resptit, cli2resptit, desconto_item1,
  			desconto_item2, desconto_item3, observacao, calculo_comissao,
  			coleta_gerada, tipo_pedido, criterio_pedido, status_expedicao,
  			obs_nota_fiscal, tipo_comissao, encargos, codigo_administr,
  			comissao_administr, expedidor, data_sugestao, tipo_prod_pedido,
  			promocao, status_homologacao, bloqueio_rolo, status_pedido,
  			com_criterio_empenhado, volumes_tecido, volumes_acomp, obs_producao,
  			nome_coletor, classificacao_pedido, tp_frete_redesp, status_comercial,
  			valor_despesas_pedido, valor_frete_pedido, valor_seguro_pedido, nr_sugestao,
  			controle, fatura_comercial, liquida_saldo_pedido, sugestao_impressa,
  			coleta_caixa_aberta, data_liber_exped, hora_liber_exped, pedido_origem_desdobr,
  			exigencia_requisito_ambiental, especificar_requisito, criterio_qualidade, data_entr_plan,
  			data_entr_interna, executa_trigger, data_prev_receb, id_pedido_forca_vendas,
  			cod_funcionario, desconto_especial, incoterm, cod_negociacao,
  			prazo_extra, desconto_extra, data_original_entrega, colecao,
  			pedido_operativo, flag_marcado, nr_solicitacao, cod_local,
  			prioridade, liberado_coletar, liberado_faturar, data_hora_distribuicao,
  			coletor, cod_catalogo, natop_pv_nat_oper, cod_cancelamento,
  			valor_saldo_pedi, seq_recebimento, numero_semana, bonus_comissao,
  			pedido_venda, tecido_peca, data_emis_venda, data_entr_venda,
  			cli_ped_cgc_cli9, cli_ped_cgc_cli4, cli_ped_cgc_cli2, cod_rep_cliente,
  			perc_comis_venda, cod_ped_cliente, colecao_tabela, mes_tabela,
  			sequencia_tabela, codigo_moeda, cond_pgto_venda, desconto1,
  			desconto2, desconto3, seq_end_entrega, seq_end_cobranca,
  			trans_pv_forne9, trans_pv_forne4, trans_pv_forne2, trans_re_forne9,
  			trans_re_forne4, trans_re_forne2, cod_via_transp, frete_venda,
  			cidade_cif, cod_banco, natop_pv_est_oper, numero_controle,
  			num_periodo_prod, codigo_empresa, data_canc_venda, situacao_venda,
  			data_digit_venda, data_liberacao, data_base_fatur, qtde_total_pedi,
  			valor_total_pedi, qtde_saldo_pedi, pedido_sujerido, sit_aloc_pedi,
  			valor_liq_itens, perc_desc_duplic, codigo_vendedor, perc_comis_vendedor,
  			tipo_desconto, tipo_frete, origem_pedido, sit_coletor,
  			numero_carga, situacao_coleta, obs_coleta, pedido_impresso
    on pedi_100                      
    for each row
declare

   v_tem_pedido        number(1);

   v_utiliza_wms             number(1);
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(120) ;
begin

   v_utiliza_wms := 0;

   begin
      select max(fatu_503.uti_integracao_wms)
      into v_utiliza_wms
      from fatu_503
      where fatu_503.uti_integracao_wms = 1;
   exception
     when no_data_found then
        v_utiliza_wms := 0;
   end;

   -- Dados do usu?rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);




   if v_utiliza_wms = 1 and ws_aplicativo <> 'pedi_f194'
   then

      begin

         select max(1)
         into   v_tem_pedido
         from inte_wms_pedidos
         where inte_wms_pedidos.numero_pedido      = :old.pedido_venda
           and inte_wms_pedidos.flag_importacao_st <> 1;
         exception
            when others then
               v_tem_pedido := 0;

      end;

      if v_tem_pedido = 1
      then

         /*Esse campos podem ser alterados quando o pedido estiver no WMS com o flag_importacao_st <> 1:
           % desconto, tipo desconto, cond. pagto., natureza de operac?o, funcionario (pedi_100.cod_funcionario),
           tipo de frete, comiss?o, bloqueios de pedido (situac?o do pedido quando for diferente de 1, 9, 10, 15).
           Todos os outros campos da PEDI_100 n?o podem ser alterados.
           Alterados tambem os campos status_expedicao, status_pedido, situacao_venda, para permitirem alterac?o (Solicitac?o de Jean em Loco ss 65853-030).

           Obs: A trigger n?o permite o uso de campos LONG, por isso o campo observac?o foi retirado dessa validac?o.*/

         if :old.pedido_venda                  <> :new.pedido_venda
         or :old.tecido_peca                   <> :new.tecido_peca
         or :old.data_emis_venda               <> :new.data_emis_venda
         or :old.data_entr_venda               <> :new.data_entr_venda
         or :old.cli_ped_cgc_cli9              <> :new.cli_ped_cgc_cli9
         or :old.cli_ped_cgc_cli4              <> :new.cli_ped_cgc_cli4
         or :old.cli_ped_cgc_cli2              <> :new.cli_ped_cgc_cli2
         or :old.cod_rep_cliente               <> :new.cod_rep_cliente
         or :old.cod_ped_cliente               <> :new.cod_ped_cliente
         or :old.codigo_moeda                  <> :new.codigo_moeda
         or :old.seq_end_entrega               <> :new.seq_end_entrega
         or :old.seq_end_cobranca              <> :new.seq_end_cobranca
         or :old.trans_pv_forne9               <> :new.trans_pv_forne9
         or :old.trans_pv_forne4               <> :new.trans_pv_forne4
         or :old.trans_pv_forne2               <> :new.trans_pv_forne2
         or :old.trans_re_forne9               <> :new.trans_re_forne9
         or :old.trans_re_forne4               <> :new.trans_re_forne4
         or :old.trans_re_forne2               <> :new.trans_re_forne2
         or :old.cod_via_transp                <> :new.cod_via_transp
         or :old.frete_venda                   <> :new.frete_venda
         or :old.cidade_cif                    <> :new.cidade_cif
         or :old.cod_banco                     <> :new.cod_banco
         or :old.num_periodo_prod              <> :new.num_periodo_prod
         or :old.codigo_empresa                <> :new.codigo_empresa
         or :old.cod_cancelamento              <> :new.cod_cancelamento
         or :old.data_canc_venda               <> :new.data_canc_venda
         or :old.data_digit_venda              <> :new.data_digit_venda
         or :old.data_liberacao                <> :new.data_liberacao
         or :old.qtde_total_pedi               <> :new.qtde_total_pedi
         or :old.sit_aloc_pedi                 <> :new.sit_aloc_pedi
         or :old.seq_recebimento               <> :new.seq_recebimento
         or :old.codigo_vendedor               <> :new.codigo_vendedor
         or :old.numero_semana                 <> :new.numero_semana
         or :old.origem_pedido                 <> :new.origem_pedido
         or :old.numero_carga                  <> :new.numero_carga
         or :old.obs_coleta                    <> :new.obs_coleta
         or :old.pedido_impresso               <> :new.pedido_impresso
         or :old.usuario_cadastro              <> :new.usuario_cadastro
         or :old.cli9resptit                   <> :new.cli9resptit
         or :old.cli4resptit                   <> :new.cli4resptit
         or :old.cli2resptit                   <> :new.cli2resptit
         or :old.coleta_gerada                 <> :new.coleta_gerada
         or :old.tipo_pedido                   <> :new.tipo_pedido
         or :old.criterio_pedido               <> :new.criterio_pedido
         or :old.obs_nota_fiscal               <> :new.obs_nota_fiscal
         or :old.encargos                      <> :new.encargos
         or :old.codigo_administr              <> :new.codigo_administr
         or :old.tipo_prod_pedido              <> :new.tipo_prod_pedido
         or :old.promocao                      <> :new.promocao
         or :old.status_homologacao            <> :new.status_homologacao
         or :old.bloqueio_rolo                 <> :new.bloqueio_rolo
         or :old.com_criterio_empenhado        <> :new.com_criterio_empenhado
         or :old.obs_producao                  <> :new.obs_producao
         or :old.classificacao_pedido          <> :new.classificacao_pedido
         or :old.status_comercial              <> :new.status_comercial
         or :old.valor_despesas_pedido         <> :new.valor_despesas_pedido
         or :old.valor_frete_pedido            <> :new.valor_frete_pedido
         or :old.valor_seguro_pedido           <> :new.valor_seguro_pedido
         or :old.controle                      <> :new.controle
         or :old.fatura_comercial              <> :new.fatura_comercial
         or :old.coleta_caixa_aberta           <> :new.coleta_caixa_aberta
         or :old.data_liber_exped              <> :new.data_liber_exped
         or :old.hora_liber_exped              <> :new.hora_liber_exped
         or :old.pedido_origem_desdobr         <> :new.pedido_origem_desdobr
         or :old.exigencia_requisito_ambiental <> :new.exigencia_requisito_ambiental
         or :old.especificar_requisito         <> :new.especificar_requisito
         or :old.criterio_qualidade            <> :new.criterio_qualidade
         or :old.data_entr_plan                <> :new.data_entr_plan
         or :old.data_entr_interna             <> :new.data_entr_interna
         or :old.executa_trigger               <> :new.executa_trigger
         or :old.data_prev_receb               <> :new.data_prev_receb
         or :old.id_pedido_forca_vendas        <> :new.id_pedido_forca_vendas
         or :old.incoterm                      <> :new.incoterm
         or :old.cod_negociacao                <> :new.cod_negociacao
--         or :old.prazo_extra                   <> :new.prazo_extra
         or :old.data_original_entrega         <> :new.data_original_entrega
         or :old.colecao                       <> :new.colecao
         or :old.pedido_operativo              <> :new.pedido_operativo
         or :old.flag_marcado                  <> :new.flag_marcado
         or :old.nr_solicitacao                <> :new.nr_solicitacao
         or :old.cod_local                     <> :new.cod_local
         or :old.prioridade                    <> :new.prioridade
         or :old.liberado_coletar              <> :new.liberado_coletar
         or :old.liberado_faturar              <> :new.liberado_faturar
         or :old.data_hora_distribuicao        <> :new.data_hora_distribuicao
         or :old.coletor                       <> :new.coletor
         or :old.cod_catalogo                  <> :new.cod_catalogo
--         or :old.aceita_antecipacao            <> :new.aceita_antecipacao
--         or :old.permite_parcial               <> :new.permite_parcial

         or deleting
         then
            raise_application_error(-20000, 'Alteração não permitida. O pedido ' || :old.pedido_venda || ' já foi importado para o WMS.');
         end if;

      end if;

   end if;

end inter_tr_pedi_100_inte;

/

exec inter_pr_recompile;
