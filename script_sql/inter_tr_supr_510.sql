
  CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_510" 
   before delete or
          update of num_requisicao, solicitacao
   on supr_510
   for each row

declare
v_tmp    number(9);
begin
   -- trigger temporaria para controle de Integridade entre as tabelas SUPR_510 X SUPR_520

   if updating
   then
      select sum(1) into v_tmp
      from supr_520
      where supr_520.num_requisicao = :old.num_requisicao
        and supr_520.solicitacao    = :old.solicitacao;

      if v_tmp is not null and v_tmp > 0
      then raise_application_error(-20000,'Ha itens para esta requisicao, nao se pode modificar. (SUPR_520 - child record found)');
      end if;
   end if;

   if deleting
   then
      select sum(1) into v_tmp
      from supr_520
      where supr_520.num_requisicao = :old.num_requisicao
        and supr_520.solicitacao    = :old.solicitacao;

      if v_tmp is not null and v_tmp > 0
      then raise_application_error(-20000,'Ha itens para esta requisicao, nao se pode modificar. (SUPR_520 - child record found)');
      end if;
   end if;

end inter_tr_supr_510;
-- ALTER TRIGGER "INTER_TR_SUPR_510" ENABLE
 

/

exec inter_pr_recompile;

