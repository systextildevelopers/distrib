alter table ftec_201 add    processo number(1);
alter table ftec_201 modify processo number(1) default 0;
comment on column ftec_201.processo is 'Processo da estamparia: 0-Estampa (padrão), 1-Corrida, 2-Digital';

update ftec_201 set processo = 0;
commit;

exec inter_pr_recompile;
