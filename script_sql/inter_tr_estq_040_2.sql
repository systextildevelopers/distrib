create or replace trigger inter_tr_estq_040_2
before insert or
       delete or
       update of qtde_estoque_atu, qtde_empenhada
on estq_040 for each row
declare
   v_local_deposito     basi_205.local_deposito%type;
   v_pronta_entrega     basi_205.pronta_entrega%type;
   v_tipo_prod_deposito basi_205.tipo_prod_deposito%type;

   v_cgc_9              fatu_500.cgc_9%type;
   v_cgc_4              fatu_500.cgc_4%type;
   v_cgc_2              fatu_500.cgc_2%type;

   v_proxima_seq_pr     e_estq_040_pr.id_registro%type;
   v_proxima_seq_2q     e_estq_040_2q.id_registro%type;
   v_proxima_seq_030    e_estq_040_2q.id_registro%type;

   v_descr_referencia   basi_030.descr_referencia%type;
   v_familia_atributo   basi_030.familia_atributo%type;

   v_descr_tam_refer    basi_020.descr_tam_refer%type;

   v_descricao_15       basi_010.descricao_15%type;
   v_classificacao_ncm  basi_010.classificacao_ncm%type;

   v_descr_produto      varchar2(60);

   v_cnpj_fabrica       e_estq_040_pr.cnpj_fabrica%type;

   v_produto_integracao      i_prod_st_2.produto_integracao%type;
   v_codigo_2qual_integracao i_prod_st_2.codigo_2qual_integracao%type;

   v_preco_venda        inte_989.preco_venda%type;
   v_preco_custo_pr     inte_989.preco_venda%type;
   v_preco_custo_2q     inte_989.preco_venda%type;
   
   v_e_mail_destino     oper_130.e_mail_destino%type;

   v_item_faturado      number;

begin
   begin
     select oper_130.e_mail_destino
     into   v_e_mail_destino
     from oper_130
     where oper_130.nome_programa = 'inte_f984';
   exception
     when no_data_found then
       v_e_mail_destino := '';
   end;
   
   if trim(v_e_mail_destino) is not null
   then
     begin
       select basi_205.local_deposito, basi_205.pronta_entrega,
              basi_205.tipo_prod_deposito
       into   v_local_deposito,        v_pronta_entrega,
              v_tipo_prod_deposito
       from basi_205
       where basi_205.codigo_deposito = nvl(:new.deposito,:old.deposito);
     exception
       when no_data_found then
         v_local_deposito     := 0;
         v_pronta_entrega     := 0;
         v_tipo_prod_deposito := 0;
     end;
  
     begin
       select fatu_500.cgc_9, fatu_500.cgc_4,
              fatu_500.cgc_2
       into   v_cgc_9,        v_cgc_4,
              v_cgc_2
       from fatu_500
       where fatu_500.codigo_empresa = v_local_deposito;
     exception
       when no_data_found then
         v_cgc_9 := 0;
         v_cgc_4 := 0;
         v_cgc_2 := 0;
     end;
  
     if v_cgc_4 > 0
     then v_cnpj_fabrica := To_number(substr(trim(to_char(v_cgc_9,'000000000')) ||
                                             trim(to_char(v_cgc_4,'0000')) ||
                                             trim(to_char(v_cgc_2,'00')),2,15));
     else v_cnpj_fabrica := To_number(substr(trim(to_char(v_cgc_9,'0000000000000')) ||
                                             trim(to_char(v_cgc_2,'00')),2,15));
     end if;
  
     begin
       select basi_030.descr_referencia, basi_030.familia_atributo
       into   v_descr_referencia,        v_familia_atributo
       from   basi_030
       where basi_030.nivel_estrutura = nvl(:new.cditem_nivel99,:old.cditem_nivel99)
         and basi_030.referencia      = nvl(:new.cditem_grupo,:old.cditem_grupo);
     exception
       when no_data_found then
         v_descr_referencia := ' ';
     end;
  
     begin
       select basi_020.descr_tam_refer
       into   v_descr_tam_refer
       from   basi_020
       where basi_020.basi030_nivel030 = nvl(:new.cditem_nivel99,:old.cditem_nivel99)
         and basi_020.basi030_referenc = nvl(:new.cditem_grupo,:old.cditem_grupo)
         and basi_020.tamanho_ref      = nvl(:new.cditem_subgrupo,:old.cditem_subgrupo);
     exception
       when no_data_found then
         v_descr_tam_refer := ' ';
     end;
  
     begin
       select basi_010.descricao_15, basi_010.classificacao_ncm
       into   v_descricao_15,        v_classificacao_ncm
       from   basi_010
       where basi_010.nivel_estrutura  = nvl(:new.cditem_nivel99,:old.cditem_nivel99)
         and basi_010.grupo_estrutura  = nvl(:new.cditem_grupo,:old.cditem_grupo)
         and basi_010.subgru_estrutura = nvl(:new.cditem_subgrupo,:old.cditem_subgrupo)
         and basi_010.item_estrutura   = nvl(:new.cditem_item,:old.cditem_item);
     exception
       when no_data_found then
         v_descricao_15      := ' ';
         v_classificacao_ncm := ' ';
     end;
  
     -- Concatena as descricoes
     v_descr_produto := trim(substr(v_descr_referencia||' '||v_descr_tam_refer||' '||v_descricao_15,1,60));
  
     -- Busca o c�digo do produto da integracao
     begin
       select i_prod_st_2.produto_integracao,
              i_prod_st_2.codigo_2qual_integracao
       into   v_produto_integracao,
              v_codigo_2qual_integracao
       from   i_prod_st_2
       where i_prod_st_2.nivel_systextil    = nvl(:new.cditem_nivel99,:old.cditem_nivel99)
         and i_prod_st_2.grupo_systextil    = nvl(:new.cditem_grupo,:old.cditem_grupo)
         and i_prod_st_2.subgrupo_systextil = nvl(:new.cditem_subgrupo,:old.cditem_subgrupo)
         and i_prod_st_2.item_systextil     = nvl(:new.cditem_item,:old.cditem_item);
     exception
       when no_data_found then
         v_produto_integracao := ' ';
     end;
  
     -- Busca o Preco de Venda e o Preco de Custo
     begin
       select inte_989.preco_venda, inte_989.preco_venda * (inte_989.fator_1 / 100.00),
              inte_989.preco_venda * (inte_989.fator_1 / 100.00) * (inte_989.fator_2 / 100.00)
       into   v_preco_venda,        v_preco_custo_pr,
              v_preco_custo_2q
       from inte_989
       where inte_989.nivel_produto  = nvl(:new.cditem_nivel99,:old.cditem_nivel99)
         and inte_989.grupo_produto  = nvl(:new.cditem_grupo,:old.cditem_grupo)
         and inte_989.subgru_produto = nvl(:new.cditem_subgrupo,:old.cditem_subgrupo)
         and inte_989.item_produto   = nvl(:new.cditem_item,:old.cditem_item);
     exception
       when no_data_found then
         v_preco_venda    := 0.00;
         v_preco_custo_pr := 0.00;
         v_preco_custo_2q := 0.00;
     end;
  
     -- Verifica se o produto ja foi fautrado em um pedido programado
     v_item_faturado := 0;
  
     begin
       select max(1)
       into   v_item_faturado
       from pedi_100, pedi_110
       where pedi_100.pedido_venda      = pedi_110.pedido_venda
         and pedi_100.tipo_pedido       = 0
         and pedi_100.situacao_venda    in (9,10)
         and pedi_110.cd_it_pe_nivel99  = nvl(:new.cditem_nivel99,:old.cditem_nivel99)
         and pedi_110.cd_it_pe_grupo    = nvl(:new.cditem_grupo,:old.cditem_grupo)
         and pedi_110.cd_it_pe_subgrupo = nvl(:new.cditem_subgrupo,:old.cditem_subgrupo)
         and pedi_110.cd_it_pe_item     = nvl(:new.cditem_item,:old.cditem_item);
     exception
       when no_data_found then
         v_item_faturado := 0;
     end;
  
     -- atualiza a tabela de Pack Resto (e_estq_040_pr)
     if nvl(:new.cditem_nivel99,:old.cditem_nivel99) = '1' and
        v_pronta_entrega     = 1   and
        v_tipo_prod_deposito = 1   and
        v_item_faturado      = 1   and
        trim(v_produto_integracao) is not null
     then
       -- Pr�ximo valor
       begin
         select nvl(Max(id_registro),0)+1
         into   v_proxima_seq_pr
         from e_estq_040_pr;
       exception
         when no_data_found then
           v_proxima_seq_pr := 1;
       end;
  
       begin
         update e_estq_040_pr
         set data_exportacao = sysdate,
             flag_exportacao = 0,
             qtde_saldo      = nvl(:new.qtde_estoque_atu,0) - nvl(:new.qtde_empenhada,0),
             preco_venda     = v_preco_venda,
             preco_custo     = v_preco_custo_pr
         where e_estq_040_pr.niv_produto  = nvl(:new.cditem_nivel99,:old.cditem_nivel99)
           and e_estq_040_pr.ref_produto  = nvl(:new.cditem_grupo,:old.cditem_grupo)
           and e_estq_040_pr.tam_produto  = nvl(:new.cditem_subgrupo,:old.cditem_subgrupo)
           and e_estq_040_pr.cor_produto  = nvl(:new.cditem_item,:old.cditem_item)
           and e_estq_040_pr.cnpj_fabrica = v_cnpj_fabrica;
  
         if SQL%notfound
         then
           insert into e_estq_040_pr
            (id_registro,           tipo_atualizacao,
             data_exportacao,       flag_exportacao,
             data_importacao,       flag_importacao,
             usuario_atualizacao,   obs_recusa,
             cnpj_fabrica,
             niv_produto,
             ref_produto,
             tam_produto,
             cor_produto,
             ref_integracao,
             descr_produto,         qtde_saldo,
             preco_venda,           preco_custo)
           values
            (v_proxima_seq_pr,      'I',
             sysdate,               0,
             null,                  3,
             'INTEGRACAO',          '',
             v_cnpj_fabrica,
             nvl(:new.cditem_nivel99,:old.cditem_nivel99),
             nvl(:new.cditem_grupo,:old.cditem_grupo),
             nvl(:new.cditem_subgrupo,:old.cditem_subgrupo),
             nvl(:new.cditem_item,:old.cditem_item),
             v_produto_integracao,
             v_descr_produto,       nvl(:new.qtde_estoque_atu,0) - nvl(:new.qtde_empenhada,0),
             v_preco_venda,         v_preco_custo_pr);
         end if;
  
       exception
         when others then
           Raise_application_error(-20000, 'ATEN��O! Ocorreram problemas ao inserir os dados na e_estq_040_pr');
       end;
     end if;
  
     -- Se n�o houver o codigo de segunda qualidade, envia registro na e_basi_030_2q
     if nvl(:new.cditem_nivel99,:old.cditem_nivel99)  = '1' and
        v_pronta_entrega     = 1   and
        v_tipo_prod_deposito = 2   and
        trim(v_codigo_2qual_integracao) is null and
        trim(v_produto_integracao)      is not null
     then
       -- Pr�ximo valor
       begin
         select nvl(Max(id_registro),0)+1
         into   v_proxima_seq_030
         from e_basi_030_2q;
       exception
         when no_data_found then
           v_proxima_seq_030 := 1;
       end;
  
       begin
         update e_basi_030_2q
         set e_basi_030_2q.data_exportacao    = sysdate,
             e_basi_030_2q.flag_exportacao    = 0,
             e_basi_030_2q.cnpj_fabrica       = v_cnpj_fabrica,
             e_basi_030_2q.ncm_produto        = v_classificacao_ncm,
             e_basi_030_2q.preco_venda_loja   = v_preco_venda,
             e_basi_030_2q.preco_custo        = v_preco_custo_2q,
             e_basi_030_2q.codigo_produto     = v_produto_integracao,
             e_basi_030_2q.descricao_resumida = substr(v_descr_referencia,1,10),
             e_basi_030_2q.descricao          = substr(v_descr_produto,1,35)
         where e_basi_030_2q.niv_produto = nvl(:new.cditem_nivel99,:old.cditem_nivel99)
           and e_basi_030_2q.ref_produto = nvl(:new.cditem_grupo,:old.cditem_grupo);
  
         if SQL%notfound
         then
           insert into e_basi_030_2q
            (id_registro,            tipo_atualizacao,
             data_exportacao,        flag_exportacao,
             data_importacao,        flag_importacao,
             usuario_atualizacao,    cnpj_fabrica,
             ncm_produto,            preco_venda_loja,
             preco_custo,            codigo_produto,
             niv_produto,
             ref_produto,
             descricao_resumida,     descricao)
           values
            (v_proxima_seq_030,     'I',
             sysdate,               0,
             null,                  3,
             'INTEGRACAO',          v_cnpj_fabrica,
             v_classificacao_ncm,   v_preco_venda,
             v_preco_custo_2q,      v_produto_integracao,
             nvl(:new.cditem_nivel99,:old.cditem_nivel99),
             nvl(:new.cditem_grupo,:old.cditem_grupo),
             substr(v_descr_referencia,1,10),
             substr(v_descr_produto,1,35));
         end if;
  
       exception
         when others then
           Raise_application_error(-20000, 'ATEN��O! Ocorreram problemas ao inserir os dados na e_basi_030_2q');
       end;
     end if;
  
     -- atualiza a tabela de Pack Resto (e_estq_040_2q)
     if nvl(:new.cditem_nivel99,:old.cditem_nivel99)  = '1' and
        v_pronta_entrega     = 1   and
        v_tipo_prod_deposito = 2   and
        trim(v_familia_atributo)        is not null and
        trim(v_codigo_2qual_integracao) is not null
     then
       -- Pr�ximo valor
       begin
         select nvl(Max(id_registro),0)+1
         into   v_proxima_seq_2q
         from e_estq_040_2q;
       exception
         when no_data_found then
           v_proxima_seq_2q := 1;
       end;
  
       begin
         update e_estq_040_2q
         set data_exportacao = sysdate,
             flag_exportacao = 0,
             qtde_saldo      = nvl(:new.qtde_estoque_atu,0) - nvl(:new.qtde_empenhada,0)
         where e_estq_040_2q.cnpj_fabrica     = v_cnpj_fabrica
           and e_estq_040_2q.grupo_mercadoria = v_familia_atributo;
         
         if SQL%notfound
         then
           insert into e_estq_040_2q
            (id_registro,           tipo_atualizacao,
             data_exportacao,       flag_exportacao,
             data_importacao,       flag_importacao,
             usuario_atualizacao,
             cnpj_fabrica,          grupo_mercadoria,
             qtde_saldo)
           values
            (v_proxima_seq_2q,      'I',
             sysdate,               0,
             null,                  3,
             'INTEGRACAO',
             v_cnpj_fabrica,        v_familia_atributo,
             nvl(:new.qtde_estoque_atu,0) - nvl(:new.qtde_empenhada,0));
         end if;
  
       exception
         when others then
           Raise_application_error(-20000, 'ATEN��O! Ocorreram problemas ao inserir os dados na e_estq_040_2q');
       end;
     end if;
   end if;
end inter_tr_estq_040_2;

/

exec inter_pr_recompile;
/* versao: 1 */
