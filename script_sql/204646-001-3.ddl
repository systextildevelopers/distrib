
insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_f813', 'Painel de Agrupamento de Pedidos', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_f813', 'pedi_menu', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pedi_f813', 'pedi_menu', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Painel de Agrupamento de Pedidos'
 where hdoc_036.codigo_programa = 'pedi_f813'
   and hdoc_036.locale          = 'es_ES';
commit;

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_f793', 'Configuração Agrupamento Pedidos - Coleção', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_f793', ' ', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pedi_f793', ' ', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Configuração Agrupamento Pedidos - Coleção'
 where hdoc_036.codigo_programa = 'pedi_f793'
   and hdoc_036.locale          = 'es_ES';
commit;

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_f794', 'Configuração Agrupamento Pedidos - Linha de Produto', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_f794', ' ', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pedi_f794', ' ', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Configuração Agrupamento Pedidos - Linha de Produto'
 where hdoc_036.codigo_programa = 'pedi_f794'
   and hdoc_036.locale          = 'es_ES';
commit;

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_f798', 'Configuração Agrupamento Pedidos - Número Interno', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_f798', ' ', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pedi_f798', ' ', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Configuração Agrupamento Pedidos - Número Interno'
 where hdoc_036.codigo_programa = 'pedi_f798'
   and hdoc_036.locale          = 'es_ES';
commit;







