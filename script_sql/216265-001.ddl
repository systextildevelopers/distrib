alter table fatu_503 add valida_prod_quimico varchar2(1) default 'N';

comment on column fatu_503.valida_prod_quimico  is 'Validação se empresa usa leito optico para scanear produtos quimicos';
