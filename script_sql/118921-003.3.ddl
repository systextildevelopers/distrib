create table PROG_056
(
    NIVEL_ESTRUTURA  VARCHAR2(1)  default '' not null,
    GRUPO_ESTRUTURA  VARCHAR2(5)  default '' not null,
    SUBGRU_ESTRUTURA VARCHAR2(3)  default '' not null,
    ITEM_ESTRUTURA   VARCHAR2(6)  default '' not null,
    NUMERO_ALTERNATI NUMBER(2)    default 0  not null,
    NUMERO_ROTEIRO   NUMBER(2)    default 0  not null,
    SEQ_OPERACAO     NUMBER(4)    default 0  not null,
    CODIGO_OPERADOR  NUMBER(3)    default 0  not null,
    TEMPO_OPERADOR   NUMBER(6, 2) default 0.00,
    QTDE_PC_OPERADOR NUMBER(8, 2) default 0,
    CODIGO_FAMILIA   NUMBER(4)    default 0
);
/
exec inter_pr_recompile;
