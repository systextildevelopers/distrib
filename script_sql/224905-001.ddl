alter table fatu_503 add bloq_apont_cons_fio varchar2(1) default 'N';

comment on column fatu_503.bloq_apont_cons_fio is 'Mecanismo criado para bloquear a apontamentos de rolos (pcpt_f085) quando existirem ordens de urdume sem apontamento de consumo de fio';
