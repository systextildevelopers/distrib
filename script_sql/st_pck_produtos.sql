create or replace package st_pck_produtos as

  ----------------------------------
  -- Obtêm a narrativa do produto --
  ----------------------------------

  function narrativa (
    p_nivel    varchar2,
    p_grupo    varchar2,
    p_subgrupo varchar2,
    p_item     varchar2
  ) return varchar2;

  ------------------------------------------
  -- Obtêm a unidade de medida do produto --
  ------------------------------------------

  function get_unidade_medida (
    p_nivel    varchar2,
    p_grupo    varchar2
  ) return varchar2;

  -----------------------------------------------
  -- Faz o cadastro da estrutura de um produto --
  -----------------------------------------------

  procedure cadastrar_estrutura (
    p_estrutura    basi_050%rowtype
  );

    -----------------------------
  -- Obtêm o preço de um fio --
  -----------------------------
  function get_preco_fio(
    p_nivel     varchar2
    ,p_grupo    varchar2
    ,p_subgrupo varchar2
    ,p_item     varchar2
    ,p_empresa  number
    ,p_mes      number
    ,p_ano      number
  ) return number;

end st_pck_produtos;

/

create or replace package body st_pck_produtos as

  ----------------------------------
  -- Obtêm a narrativa do produto --
  ----------------------------------
  
  function narrativa (
    p_nivel    varchar2,
    p_grupo    varchar2,
    p_subgrupo varchar2,
    p_item     varchar2
  ) return varchar2
  is
    v_narrativa basi_010.narrativa%type;
  begin

    select
      basi_010.narrativa
    into
      v_narrativa
    from basi_010
    where 
        basi_010.nivel_estrutura  = p_nivel
    and basi_010.grupo_estrutura  = p_grupo
    and basi_010.subgru_estrutura = p_subgrupo
    and basi_010.item_estrutura   = p_item;

    return v_narrativa;

  end narrativa;

  ------------------------------------------
  -- Obtêm a unidade de medida do produto --
  ------------------------------------------

  function get_unidade_medida (
    p_nivel    varchar2,
    p_grupo    varchar2
  ) return varchar2
  is
    v_unidade_medida basi_030.unidade_medida%type;
  begin

    select 
      unidade_medida
    into
      v_unidade_medida
    from basi_030 
    where nivel_estrutura	= p_nivel
    and referencia = p_grupo;

    return v_unidade_medida;

  end get_unidade_medida;

  -----------------------------------------------
  -- Faz o cadastro da estrutura de um produto --
  -----------------------------------------------

  procedure cadastrar_estrutura (
    p_estrutura    basi_050%rowtype
  ) is
  begin
    savepoint inicio;

    -- function produto_existe
    declare
      v_existe number;
    begin
      select count(1) into v_existe
        from basi_010
        where NIVEL_ESTRUTURA = p_estrutura.NIVEL_ITEM
        and GRUPO_ESTRUTURA   = p_estrutura.GRUPO_ITEM
        and SUBGRU_ESTRUTURA  = p_estrutura.SUB_ITEM
        and ITEM_ESTRUTURA    = p_estrutura.ITEM_ITEM
        and NUMERO_ALTERNATI  = p_estrutura.ALTERNATIVA_ITEM;

      if v_existe = 0 then 
        raise_application_error(-20000, 'Produto não cadastrado');
      end if;
    end;

    insert into basi_050 values p_estrutura;


    raise_application_error(-20000, 'Produto cadastrado');
  exception when others then
    rollback to inicio;
  end;

  -----------------------------
  -- Obtêm o preço de um fio --
  -----------------------------
  function get_preco_fio(
     p_nivel     varchar2
    ,p_grupo    varchar2
    ,p_subgrupo varchar2
    ,p_item     varchar2
    ,p_empresa  number
    ,p_mes      number
    ,p_ano      number
  ) return number
  is
    v_preco_fio number;
  begin

    select
        preco_fio
    into v_preco_fio
    from (
        select
            preco_medio_unitario as preco_fio
        from estq_301

        inner join basi_205 on
                basi_205.codigo_deposito = estq_301.codigo_deposito
            and basi_205.local_deposito in (1,2)

        where   estq_301.nivel_estrutura    = p_nivel   
            and estq_301.grupo_estrutura    = p_grupo   
            and estq_301.subgrupo_estrutura = p_subgrupo
            and estq_301.item_estrutura     = p_item    
            and estq_301.ano_movimento      = p_ano
            and estq_301.mes_movimento      = p_mes

        order by MES_ANO_MOVIMENTO desc
    )
    where rownum = 1;
   
   return v_preco_fio;

  end get_preco_fio;

end st_pck_produtos;

/
