create or replace FUNCTION inter_fn_get_param_int (p_empresa in number, p_param_name in varchar2) return number
  is param_value number;
begin

  begin
    select nvl2(empr_008.codigo_empresa, empr_008.val_int, empr_007.default_int)
      into param_value
      from empr_007
        left join empr_008 ON empr_008.param = empr_007.param
          and empr_008.codigo_empresa = p_empresa
    where empr_007.param = p_param_name;
  exception
    when no_data_found then
      raise_application_error(-20000, 'O parâmetro ' || p_param_name || ' não foi configurado corretamente.');
  end;

  return(param_value);

end inter_fn_get_param_int;

