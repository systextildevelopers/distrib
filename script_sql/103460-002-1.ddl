create table pedi_181 (
  cod_empresa number(3) default 0,
  cnpj_9 number(9) default 0,
  cnpj_4 number(4) default 0,
  cnpj_2 number(2) default 0,
  nr_dias number(3) default 0
);

alter table pedi_181 add constraint pk_cod_empresa primary key (cod_empresa, cnpj_9, cnpj_4, cnpj_2);

exec inter_pr_recompile;
/
