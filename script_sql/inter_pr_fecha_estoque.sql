CREATE OR REPLACE PROCEDURE "INTER_PR_FECHA_ESTOQUE"
is
   v_data_inicial   empr_001.periodo_estoque%type;
   v_data_final     empr_001.periodo_estoque%type;
   v_dia_fecha      empr_001.periodo_estoque%type;

   erro_periodo_estoque exception;

begin
   -- le a data inicial
   begin
      select periodo_estoque into v_data_inicial from empr_001;

      exception
         when others then
            Raise erro_periodo_estoque;
   end;

   -- inicializa variaveis
   v_data_final := last_day(v_data_inicial);
   v_dia_fecha  := v_data_inicial;

   -- atualiza os campos de controle para informar ao sistema a data que n�o pode ser movimentadao
   -- bem como o est�gio do fechamento:
   --    0- N�o esta sendo realizado o fechamento de estoque;
   --    1- Esta sendo feito o fechamento de estoque, por�m ainda � poss�vel movimentar o estoque
   --       do per�odo em aberto;
   --    2- Esta sendo feito o fechamento de estoque, por�m ainda � poss�vel movimentar o estoque
   --       do per�odo em aberto;

   update empr_002
   set dtfim_fecha_estq = v_data_final,
       flag_fecha_estq  = 1;
   commit;

   -- verifica se a algum registro com o flag_elimina = 1
   -- se houver faz um insert deste registro na estq_305
   -- em seguida atualiza os registros com flag_elimina de 1 para 0

   insert into estq_305
         (codigo_deposito,     nivel_estrutura,
          grupo_estrutura,     subgrupo_estrutura,
          item_estrutura,      data_movimento,
          sequencia_ficha,     sequencia_insercao)
   select codigo_deposito,     nivel_estrutura,
          grupo_estrutura,     subgrupo_estrutura,
          item_estrutura,      data_movimento,
          sequencia_ficha,     sequencia_insercao
   from estq_300
   where data_movimento between  v_data_inicial and v_data_final
   and   flag_elimina = 1;

   update estq_300 set flag_elimina = 0
   where data_movimento between  v_data_inicial and v_data_final
   and   flag_elimina = 1;

   commit;

   while v_dia_fecha <= v_data_final
   loop
      begin
        -- copia registro do estq_300 para o estq_310
        insert into estq_310
              (codigo_deposito,              nivel_estrutura,           grupo_estrutura,
               subgrupo_estrutura,           item_estrutura,            data_movimento,
               sequencia_ficha,              sequencia_insercao,        numero_lote,
               numero_documento,             serie_documento,           cnpj_9,
               cnpj_4,                       cnpj_2,                    sequencia_documento,
               codigo_transacao,             entrada_saida,             centro_custo,
               quantidade,                   saldo_fisico,              valor_movimento_unitario,
               valor_contabil_unitario,      preco_medio_unitario,      saldo_financeiro,
               grupo_maquina,                subgru_maquina,            numero_maquina,
               ordem_servico,                contabilizado,             usuario_systextil,
               processo_systextil,           data_insercao,             usuario_rede,
               maquina_rede,                 aplicativo,                tabela_origem,
               valor_contabil_unitario_proj, preco_medio_unitario_proj, saldo_financeiro_proj,
               valor_movto_unit_estimado,    preco_medio_unit_estimado, saldo_financeiro_estimado,
               valor_total,                  projeto,                   subprojeto,
               servico,                      quantidade_quilo,          saldo_fisico_quilo,
               numero_op,                    estagio_op,                pacote_op,
               tipo_ordem,                   ordem_agrupamento,         seq_operacao_agrupador,
               cod_estagio_agrupador)
        select codigo_deposito,              nivel_estrutura,           grupo_estrutura,
               subgrupo_estrutura,           item_estrutura,            data_movimento,
               sequencia_ficha,              sequencia_insercao,        numero_lote,
               numero_documento,             serie_documento,           cnpj_9,
               cnpj_4,                       cnpj_2,                    sequencia_documento,
               codigo_transacao,             entrada_saida,             centro_custo,
               quantidade,                   saldo_fisico,              valor_movimento_unitario,
               valor_contabil_unitario,      preco_medio_unitario,      saldo_financeiro,
               grupo_maquina,                subgru_maquina,            numero_maquina,
               ordem_servico,                contabilizado,             usuario_systextil,
               processo_systextil,           data_insercao,             usuario_rede,
               maquina_rede,                 aplicativo,                tabela_origem,
               valor_contabil_unitario_proj, preco_medio_unitario_proj, saldo_financeiro_proj,
               valor_movto_unit_estimado,    preco_medio_unit_estimado, saldo_financeiro_estimado,
               valor_total,                  projeto,                   subprojeto,
               servico,                      quantidade_quilo,          saldo_fisico_quilo,
               numero_op,                    estagio_op,                pacote_op,
               tipo_ordem,                   ordem_agrupamento,         seq_operacao_agrupador,
               cod_estagio_agrupador
        from estq_300
        where data_movimento = v_dia_fecha;
        
      exception       
             
        WHEN DUP_VAL_ON_INDEX THEN

        -- elimina registros copiados do estq_300
        delete estq_300
        where data_movimento = v_dia_fecha;

      end;
      
      -- elimina registros copiados do estq_300
      delete estq_300
      where data_movimento = v_dia_fecha;
      
      commit;

      -- acrescenta 1 dia, na data de controle
      v_dia_fecha := v_dia_fecha + 1;
   end loop;

   update basi_205
   set basi_205.situacao_deposito = 0;

   update empr_002
   set flag_fecha_estq = 2;
   commit;

   -- atualiza estoque_ant e estoque_mes do estq_040
   update estq_040
   set qtde_estoque_ant = qtde_estoque_mes,
       qtde_estoque_mes = qtde_estoque_atu
   where qtde_estoque_ant <> qtde_estoque_mes
   or    qtde_estoque_mes <> qtde_estoque_atu;

   -- atualiza periodo de estoque
   update empr_001
   set periodo_estoque = add_months(periodo_estoque, 1);

   commit;

   update empr_002
   set flag_fecha_estq = 0;

   commit;

   exception
      when erro_periodo_estoque then
         raise_application_error(-20000,'N�o encontrou periodo de estoque');

      when others then
         rollback;

         update empr_002
         set flag_fecha_estq  = 0,
             dtfim_fecha_estq = v_data_inicial - 1;
         commit;

         raise_application_error(-20000,'Houve problemas no fechamento de estoque. Verifique' || Chr(10) || SQLERRM);

end inter_pr_fecha_estoque;
/
