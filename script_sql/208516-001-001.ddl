create table manu_007
(
  codigo           NUMBER(5) default 0 not null,
  descricao        VARCHAR2(60) default '',
  cod_empresa      NUMBER(3) default 0 not null
);

alter table manu_007 add constraint pk_manu_007 primary key (codigo);

/
exec inter_pr_recompile;
