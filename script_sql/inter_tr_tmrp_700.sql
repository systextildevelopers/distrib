
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_700" 
   before insert or
          delete or
          update of qtde_previsao
   on tmrp_700
   for each row

declare

   v_perc_distribuicao tmrp_705.perc_distribuicao%type;
   v_qtde_previsao     tmrp_700.qtde_previsao%type;
   v_existe_710 integer;

begin

   if updating
   then
      for tmrp705 in (select tmrp_705.periodo_producao,
                             tmrp_705.perc_distribuicao
                  from tmrp_705
                  where tmrp_705.nivel    = :new.nivel
                    and tmrp_705.grupo    = :new.grupo
                    and tmrp_705.subgrupo = :new.subgrupo
                    and tmrp_705.item     = :new.item
                    and tmrp_705.mes      = :new.mes
                    and tmrp_705.ano      = :new.ano
                 )
      loop
         for basi010 in (select basi_010.nivel_estrutura,
                                basi_010.grupo_estrutura,
                                basi_010.subgru_estrutura,
                                basi_010.item_estrutura,
                                basi_010.numero_alternati
                         from basi_010
                         where basi_010.nivel_estrutura   = :new.nivel
                           and basi_010.grupo_estrutura   = :new.grupo
                           and (basi_010.subgru_estrutura = :new.subgrupo or :new.subgrupo = '000')
                           and (basi_010.item_estrutura   = :new.item or :new.item = '000000')
                         )
         loop
            v_qtde_previsao := (((:old.qtde_previsao * tmrp705.perc_distribuicao)/100) * -1);
            inter_pr_expl_estr_tmrp_710(basi010.nivel_estrutura,
                                        basi010.grupo_estrutura,
                                        basi010.subgru_estrutura,
                                        basi010.item_estrutura,
                                        basi010.numero_alternati,
                                        tmrp705.periodo_producao,
                                        v_qtde_previsao,
                                        0,
                                        0,
                                        2,
                                        0,
                                        0,
                                        0.00,
                                        0,
                                       'pcpb');


            v_qtde_previsao := ((:new.qtde_previsao * tmrp705.perc_distribuicao)/100);
            inter_pr_expl_estr_tmrp_710(basi010.nivel_estrutura,
                                        basi010.grupo_estrutura,
                                        basi010.subgru_estrutura,
                                        basi010.item_estrutura,
                                        basi010.numero_alternati,
                                        tmrp705.periodo_producao,
                                        v_qtde_previsao,
                                        0,
                                        0,
                                        2,
                                        0,
                                        0,
                                        0.00,
                                        0,
                                        'pcpb');


            select count(*)
            into v_existe_710
            from tmrp_710
            where tmrp_710.periodo_producao    = tmrp705.periodo_producao
              and tmrp_710.area_producao       = 2
              and tmrp_710.nivel_estrutura     = basi010.nivel_estrutura
              and tmrp_710.grupo_estrutura     = basi010.grupo_estrutura
              and tmrp_710.subgru_estrutura    = basi010.subgru_estrutura
              and tmrp_710.item_estrutura      = basi010.item_estrutura
              and tmrp_710.sequencia_estrutura = 0;
            if v_existe_710 > 0
            then
               update tmrp_710
               set tmrp_710.qtde_reservada = ((:new.qtde_previsao * tmrp705.perc_distribuicao)/100)
               where tmrp_710.periodo_producao    = tmrp705.periodo_producao
                 and tmrp_710.area_producao       = 2
                 and tmrp_710.nivel_estrutura     = basi010.nivel_estrutura
                 and tmrp_710.grupo_estrutura     = basi010.grupo_estrutura
                 and tmrp_710.subgru_estrutura    = basi010.subgru_estrutura
                 and tmrp_710.item_estrutura      = basi010.item_estrutura
                 and tmrp_710.sequencia_estrutura = 0;
            end if;
         end loop;
      end loop;
   end if;

   if deleting
   then

      for tmrp705 in (select tmrp_705.periodo_producao,
                             tmrp_705.perc_distribuicao
                  from tmrp_705
                  where tmrp_705.nivel    = :old.nivel
                    and tmrp_705.grupo    = :old.grupo
                    and tmrp_705.subgrupo = :old.subgrupo
                    and tmrp_705.item     = :old.item
                    and tmrp_705.mes      = :old.mes
                    and tmrp_705.ano      = :old.ano
                 )
      loop
         for basi010 in (select basi_010.nivel_estrutura,
                                basi_010.grupo_estrutura,
                                basi_010.subgru_estrutura,
                                basi_010.item_estrutura,
                                basi_010.numero_alternati
                         from basi_010
                         where basi_010.nivel_estrutura   = :old.nivel
                           and basi_010.grupo_estrutura   = :old.grupo
                           and (basi_010.subgru_estrutura = :old.subgrupo or :old.subgrupo = '000')
                           and (basi_010.item_estrutura   = :old.item or :old.item = '000000')
                         )
         loop

            v_qtde_previsao := (((:old.qtde_previsao * tmrp705.perc_distribuicao)/100) * -1);

            inter_pr_expl_estr_tmrp_710(basi010.nivel_estrutura,
                                        basi010.grupo_estrutura,
                                        basi010.subgru_estrutura,
                                        basi010.item_estrutura,
                                        basi010.numero_alternati,
                                        tmrp705.periodo_producao,
                                        v_qtde_previsao,
                                        0,
                                        0,
                                        2,
                                        0,
                                        0,
                                        0.00,
                                        0,
                                        'pcpb');


            select count(*)
            into v_existe_710
            from tmrp_710
            where tmrp_710.periodo_producao    = tmrp705.periodo_producao
              and tmrp_710.area_producao       = 2
              and tmrp_710.nivel_estrutura     = basi010.nivel_estrutura
              and tmrp_710.grupo_estrutura     = basi010.grupo_estrutura
              and tmrp_710.subgru_estrutura    = basi010.subgru_estrutura
              and tmrp_710.item_estrutura      = basi010.item_estrutura
              and tmrp_710.sequencia_estrutura = 0;
            if v_existe_710 > 0
            then
               update tmrp_710
               set tmrp_710.qtde_reservada = (((:old.qtde_previsao * tmrp705.perc_distribuicao)/100) * -1)
               where tmrp_710.periodo_producao    = tmrp705.periodo_producao
                 and tmrp_710.area_producao       = 2
                 and tmrp_710.nivel_estrutura     = basi010.nivel_estrutura
                 and tmrp_710.grupo_estrutura     = basi010.grupo_estrutura
                 and tmrp_710.subgru_estrutura    = basi010.subgru_estrutura
                 and tmrp_710.item_estrutura      = basi010.item_estrutura
                 and tmrp_710.sequencia_estrutura = 0;
            end if;
         end loop;
      end loop;
   end if;
end inter_tr_tmrp_700;

-- ALTER TRIGGER "INTER_TR_TMRP_700" ENABLE
 

/

exec inter_pr_recompile;

