CREATE OR REPLACE PROCEDURE "INTER_PR_GET_BASE_PIS_COFINS"(
       p_empresa            in number, p_perc_pis    in number, p_perc_cofins     in number,
       p_valor_faturado     in number, p_valor_icms  in number, p_desconto        in number,
       p_base_pis_cofins out number
)
is

  v_tipo_reducao_param      empr_008.val_int%type;
  v_perc_pis_e_cofins       fatu_060.perc_pis%type;
  v_valor_pis_e_cofins      fatu_060.valor_pis%type;
  v_valor_liquido           fatu_060.valor_faturado%type;

begin

  v_tipo_reducao_param := inter_fn_get_param_int(p_empresa, 'fiscal.tipoReducaoBasePisCofins');
  
  if v_tipo_reducao_param = 0
  then
    p_base_pis_cofins := p_valor_faturado;
  else 
    if v_tipo_reducao_param = 1
    then
      if p_valor_icms is not null
      then
        p_base_pis_cofins := p_valor_faturado - p_valor_icms;
      else 
        p_base_pis_cofins := p_valor_faturado;
      end if;
    else 
      if v_tipo_reducao_param = 2
      then
        v_perc_pis_e_cofins := p_perc_pis + p_perc_cofins;
        v_valor_pis_e_cofins := p_valor_faturado * (v_perc_pis_e_cofins / 100);
        if p_valor_icms is not null
        then
          v_valor_liquido := p_valor_faturado - p_valor_icms - v_valor_pis_e_cofins;
        else 
          v_valor_liquido := p_valor_faturado - v_valor_pis_e_cofins;
        end if;
        p_base_pis_cofins := v_valor_liquido / (1 - v_perc_pis_e_cofins / 100);
      end if;
    end if;
  end if;
  
  p_base_pis_cofins := p_base_pis_cofins * ((100 - p_desconto) / 100);
   
end inter_pr_get_base_pis_cofins;
