create table pedi_266_LOG  
( 
  TIPO_OCORR       VARCHAR2(1) default '' null,
  DATA_OCORR       DATE null,
  HORA_OCORR       DATE null,
  USUARIO_REDE     VARCHAR2(20) default '' null,
  MAQUINA_REDE     VARCHAR2(40) default '' null,
  APLICACAO        VARCHAR2(20) default '' null,
  USUARIO_SISTEMA  VARCHAR2(20) default '' null,
  NOME_PROGRAMA    VARCHAR2(20) default '' null,
  pedido_OLD       NUMBER(9) default 0 ,  
  pedido_NEW       NUMBER(9) default 0 , 
  sequencia_OLD    NUMBER(3) default 0 ,  
  sequencia_NEW    NUMBER(3) default 0 , 
  desconto1_OLD    NUMBER(6,2) default 0.0,  
  desconto1_NEW    NUMBER(6,2) default 0.0, 
  desconto2_OLD    NUMBER(6,2) default 0.0,  
  desconto2_NEW    NUMBER(6,2) default 0.0, 
  desconto3_OLD    NUMBER(6,2) default 0.0,  
  desconto3_NEW    NUMBER(6,2) default 0.0, 
  desc_calc_OLD    NUMBER(6,2) default 0.0, 
  desc_calc_NEW    NUMBER(6,2) default 0.0 
);

/

exec inter_pr_recompile;
