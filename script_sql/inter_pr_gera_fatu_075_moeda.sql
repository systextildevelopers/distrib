create or replace PROCEDURE inter_pr_gera_fatu_075_moeda (
   p_cnpj9 in number,       
   p_cnpj4 in number,           
   p_cnpj2 in number,
   p_tipo_titulo in number, 
   p_nr_titulo in number,
   p_empresa in number,
   p_seq_duplicata number,
   p_data_emissao in date,
   p_historico in  number,
   p_portador_dupl in number,  
   p_conta_corrente in number,  
   p_data_credito in date,
   p_pago_adiantamento in number,
   p_docto_pagto in number,
   p_num_lcto in number,
   p_valor_pago in number,
   p_tipo_lanc_comissao in number,
   p_valor_pago_moe in number,
   r_des_erro out varchar2) is
    
   
    v_seq_pag number;
    
begin


        select nvl(MAX(fatu_075.seq_pagamento), 0)+1
        into v_seq_pag
        from fatu_075
        where fatu_075.nr_titul_codempr = p_empresa
        and fatu_075.nr_titul_cli_dup_cgc_cli9 = p_cnpj9
        and fatu_075.nr_titul_cli_dup_cgc_cli4 = p_cnpj4
        and fatu_075.nr_titul_cli_dup_cgc_cli2 = p_cnpj2
        and fatu_075.nr_titul_cod_tit = p_tipo_titulo
        and fatu_075.nr_titul_num_dup = p_nr_titulo
        and fatu_075.nr_titul_seq_dup = p_seq_duplicata;

        BEGIN
            insert into fatu_075 (
                nr_titul_cli_dup_cgc_cli9,  nr_titul_cli_dup_cgc_cli4,
                nr_titul_cli_dup_cgc_cli2,  nr_titul_cod_tit,
                nr_titul_num_dup,           nr_titul_seq_dup,
                nr_titul_codempr,           seq_pagamento,
                data_pagamento,             valor_pago,
                historico_pgto,             portador,
                conta_corrente,             data_credito,
                comissao_lancada,           num_contabil,
                pago_adiantamento,          docto_pagto,
                valor_pago_moeda
            ) values (
                p_cnpj9 ,               p_cnpj4 ,
                p_cnpj2 ,               p_tipo_titulo ,
                p_nr_titulo ,           p_seq_duplicata ,
                p_empresa ,             v_seq_pag ,
                p_data_emissao ,        p_valor_pago ,
                p_historico ,           p_portador_dupl ,
                p_conta_corrente ,      p_data_credito ,
                p_tipo_lanc_comissao ,  p_num_lcto,
                p_pago_adiantamento ,   p_docto_pagto,
                p_valor_pago_moe
            );
        EXCEPTION WHEN OTHERS THEN
            r_des_erro := 'Erro ao inserir baixa de titulos - fatu_075' || Chr(10) || SQLERRM;
        END;

end inter_pr_gera_fatu_075_moeda;
