create table ftec_720
(
  NIVEL_PRODUTO          VARCHAR2(1) default '0',
  GRUPO_PRODUTO          VARCHAR2(5) default '00000',
  SUBGRUPO_PRODUTO       VARCHAR2(3) default '000',
  ITEM_PRODUTO           VARCHAR2(6) default '000000',
  ALTERNATIVA_PRODUTO    NUMBER(2)   default 0,
  ROTEIRO_PRODUTO        NUMBER(2)   default 0, 
  GRADE_COLECAO          NUMBER(4)   default 0
);

alter table ftec_720
add constraint pk_ftec_720 primary key (NIVEL_PRODUTO,GRUPO_PRODUTO,SUBGRUPO_PRODUTO,ITEM_PRODUTO, GRADE_COLECAO);
