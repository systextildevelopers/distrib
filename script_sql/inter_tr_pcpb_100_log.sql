CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_100_LOG" 
   after insert or
         delete or
         update of sequencia_entrada_maquina, data_prevista_maquina, grupo_maquina,
                   subgrupo_maquina,          numero_maquina,        data_digitacao,
                   data_prev_termino,         cod_cancelamento,      data_cancelamento,
                   ordem_agrupamento,         ordem_producao
   on pcpb_100
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_area_producao          number(9);
   ws_ordem_producao         number(9);
   ws_periodo_producao       number(9);
   ws_ordem_confeccao        number(9);
   ws_tipo_historico         number(9);
   ws_descricao_historico    varchar2(4000);
   ws_tipo_ocorr             varchar2(1);
   ws_nome_programa          hdoc_090.programa%type;
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);


begin
 -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ws_nome_programa := inter_fn_nome_programa(ws_sid); 
     
   if inserting
   then

      ws_area_producao         := 2;
      ws_ordem_producao        := :new.ordem_agrupamento;
      ws_periodo_producao      := :new.periodo_producao;
      ws_ordem_confeccao       := 0;
      ws_tipo_historico        := 1;
      ws_descricao_historico   := 'REGISTRO INSERIDO - ORDEM DE BENEFICIAMENTO (PCPB_100)'
                                      ||chr(10)
                                      ||'DATA DIGITACAO:' || ' ' || :new.data_digitacao
                                      ||chr(10)
                                      ||'ORDEM PRODUC�O:' || ' ' || :new.ordem_producao;


      ws_tipo_ocorr            := 'I';
   end if;

   if updating
   then

      ws_area_producao         := 2;
      ws_ordem_producao        := :old.ordem_agrupamento;
      ws_periodo_producao      := :old.periodo_producao;
      ws_ordem_confeccao       := 0;
      ws_tipo_historico        := 1;
      ws_descricao_historico   := 'ATUALIZACAO RELACIONAMENTO DE ORDENS DE PRODUCAO (PCPB_100)'
                                      ||chr(10)
                                      ||'GRUPO MAQUINA:' || ' ' || :old.grupo_maquina
                                      ||'  PARA' ||' -> '|| :new.grupo_maquina
                                      ||chr(10)
                                      ||'SUBGRUPO MAQUINA:' || ' ' || :old.subgrupo_maquina
                                      ||'  PARA' ||' -> '|| :new.subgrupo_maquina
                                      ||chr(10)
                                      ||'NUM. MAQUINA:' || ' ' || :old.numero_maquina
                                      ||'  PARA' ||' -> '|| :new.numero_maquina
                                      ||chr(10)
                                      ||'SEQ. ENTRADA:' || ' ' || :old.sequencia_entrada_maquina
                                      ||'  PARA' ||' -> '|| :new.sequencia_entrada_maquina
                                      ||chr(10)
                                      ||'DATA MAQUINA:' || ' ' || :old.data_prevista_maquina
                                      ||'  PARA' ||' -> '|| :new.data_prevista_maquina
                                      ||chr(10)
                                      ||'DATA DIGITACAO:' || ' ' || :old.data_digitacao
                                      ||'  PARA' ||' -> '|| :new.data_digitacao
                                      ||chr(10)
                                      ||'DATA PREVISTA:' || ' ' || :old.data_prev_termino
                                      ||'  PARA' ||' -> '|| :new.data_prev_termino
                                      ||chr(10)
                                      ||'ORDEM PRODUCAO:' || ' ' || :old.ordem_producao
                                      ||'  PARA' ||' -> '|| :new.ordem_producao
                                      ||chr(10)
                                      ||'COD. CANCELAMENTO:' || ' ' || :old.cod_cancelamento
                                      ||'  PARA' ||' -> '|| :new.cod_cancelamento
                                      ||chr(10)
                                      ||'DATA CANCELAMENTO:' || ' ' || :old.data_cancelamento
                                      ||'  PARA' ||' -> '|| :new.data_cancelamento;

      ws_tipo_ocorr            := 'A';
   end if;

   if deleting
   then
      ws_area_producao         := 2;
      ws_ordem_producao        := :old.ordem_agrupamento;
      ws_periodo_producao      := :old.periodo_producao;
      ws_ordem_confeccao       := 0;
      ws_tipo_historico        := 1;
      ws_descricao_historico   := 'ELIMINACAO DO RELACIONAMENTO DE ORDENS DE PRODUCAO (PCPB_100)'
                                      ||chr(10)
                                      ||'GRUPO MAQUINA:' || ' ' || :old.grupo_maquina
                                      ||chr(10)
                                      ||'SUBGRUPO MAQUINA:' || ' ' || :old.subgrupo_maquina
                                      ||chr(10)
                                      ||'NUM. MAQUINA:' || ' ' || :old.numero_maquina
                                      ||chr(10)
                                      ||'SEQ. ENTRADA:' || ' ' || :old.sequencia_entrada_maquina
                                      ||chr(10)
                                      ||'DATA MAQUINA:' || ' ' || :old.data_prevista_maquina
                                      ||chr(10)
                                      ||'DATA DIGITACAO:' || ' ' || :old.data_digitacao
                                      ||chr(10)
                                      ||'DATA PREVISTA:' || ' ' || :old.data_prev_termino
                                      ||chr(10)
                                      ||'ORDEM PRODUCAO:' || ' ' || :old.ordem_producao
                                      ||chr(10)
                                      ||'COD. CANCELAMENTO:' || ' ' || :old.cod_cancelamento
                                      ||chr(10)
                                      ||'DATA CANCELAMENTO:' || ' ' || :old.data_cancelamento;

      ws_tipo_ocorr            := 'D';
   end if;

   if updating or deleting  or inserting
   then
      INSERT INTO hist_010
        (area_producao,           ordem_producao,          periodo_producao,
         ordem_confeccao,         tipo_historico,          descricao_historico,
         tipo_ocorr,              data_ocorr,              hora_ocorr,
         usuario_rede,            maquina_rede,            aplicacao,
         tipo_ordem,              nome_programa)
      VALUES
        (ws_area_producao,        ws_ordem_producao,       ws_periodo_producao,
         ws_ordem_confeccao,      ws_tipo_historico,       ws_descricao_historico,
         ws_tipo_ocorr,           sysdate,                 sysdate,
         ws_usuario_rede,         ws_maquina_rede,         ws_aplicativo,
         :new.tipo_ordem,         ws_nome_programa);
   end if;
end inter_tr_pcpb_100_log;
-- ALTER TRIGGER "INTER_TR_PCPB_100_LOG" ENABLE
 

/

exec inter_pr_recompile;

