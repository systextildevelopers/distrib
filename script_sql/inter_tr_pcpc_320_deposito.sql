
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_320_DEPOSITO" 
   before update of deposito_entrada on pcpc_320
   for each row
begin
   if updating
   then
      update pcpc_325
      set pcpc_325.deposito_entrada_item = :new.deposito_entrada
      where pcpc_325.numero_volume       = :new.numero_volume;
   end if;
end inter_tr_pcpc_320_deposito;

-- ALTER TRIGGER "INTER_TR_PCPC_320_DEPOSITO" ENABLE
 

/

exec inter_pr_recompile;

