
  CREATE OR REPLACE TRIGGER "INTER_TR_LOJA_210_PERMISSAO" 
  before insert or update of perc_desc, perc_acres,valor_total_item on loja_210
  for each row
declare
   v_perc_acres_perm       loja_003.acrescimo%type;
   v_perc_desc_perm        loja_003.desconto%type;
   v_val_acres_perm        loja_003.val_acrescimo%type;
   v_val_desc_perm         loja_003.val_desconto%type;

   v_DescAcres               number(13,2);
   v_PercDesc                number(13,2);
   v_TotalItem               number(13,2);

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   begin
     select loja_003.desconto, loja_003.acrescimo, loja_003.val_desconto, loja_003.val_acrescimo
       into v_perc_desc_perm , v_perc_acres_perm,  v_val_desc_perm , v_val_acres_perm
     from loja_003
     where loja_003.empresa = :new.cod_empresa
       and loja_003.usuario = ws_usuario_systextil;
   exception
     when no_data_found then
        v_perc_desc_perm  := 0;
        v_perc_acres_perm := 0;
   end;

   v_PercDesc := :new.perc_desc;

   if inserting
   then
     if :new.perc_desc > 0
     then v_PercDesc := :new.perc_desc;
     else v_PercDesc := :old.perc_desc; -- nao sei porque essa porra precisa ser old, quando esta inserindo, pelo menos esta funcionando
     end if;
   end if;

   if :new.valor_total_item = 0
   then v_TotalItem := :new.preco_tabela * (v_PercDesc /100);
   else v_TotalItem := :new.valor_total_item;
   end if;

   v_DescAcres := (v_TotalItem - :new.preco_tabela);

   if v_perc_acres_perm > 0 and :new.perc_acres > v_perc_acres_perm
   then
      raise_application_error(-20000,inter_fn_buscar_tag('ds30479#ATENCAO! O valor de acrescimo e superior ao limite informado no cadastro de Permissoes de usuarios por empresa',ws_locale_usuario,ws_usuario_systextil));
   end if;
   if v_perc_desc_perm > 0 and v_PercDesc > v_perc_desc_perm
   then
      raise_application_error(-20000,inter_fn_buscar_tag('ds30480#ATENCAO! O valor de desconto e superior ao limite informado no cadastro de Permissoes de usuarios por empresa' ,ws_locale_usuario,ws_usuario_systextil));
   end if;
   if v_val_desc_perm > 0 and v_DescAcres < 0
   then
     v_DescAcres := v_DescAcres *(-1);
     if v_DescAcres > v_val_desc_perm
     then
        raise_application_error(-20000,inter_fn_buscar_tag('ds30480#ATENCAO! O valor de desconto e superior ao limite informado no cadastro de Permissoes de usuarios por empresa' ,ws_locale_usuario,ws_usuario_systextil));
     end if;
   end if;
   if v_val_acres_perm > 0 and v_DescAcres > 0
   then
     if v_DescAcres > v_val_acres_perm
     then
      raise_application_error(-20000,inter_fn_buscar_tag('ds30479#ATENCAO! O valor de acrescimo e superior ao limite informado no cadastro de Permissoes de usuarios por empresa',ws_locale_usuario,ws_usuario_systextil));
     end if;
   end if;

end inter_tr_loja_210_permissao;

-- ALTER TRIGGER "INTER_TR_LOJA_210_PERMISSAO" ENABLE
 

/

exec inter_pr_recompile;

