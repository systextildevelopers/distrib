
  CREATE OR REPLACE FUNCTION "INTER_FN_CALCULA_PRAZO_MED" (pCondPgto   IN number)

  return number is

  totparcven       number(6);
  total_dias       number(6);
  acum_dias        number(6);
  pPrazoMedio      number(6,3);


BEGIN

  totparcven := 0;
  total_dias := 0;
  acum_dias  := 0;

  pPrazoMedio := 0;

  for LPVencimentos in (
                        select pedi_075.vencimento from pedi_075
                        where pedi_075.condicao_pagto = pCondPgto
                        order by pedi_075.sequencia ASC)
  LOOP
     totparcven := totparcven + 1;
     total_dias := total_dias + lpvencimentos.vencimento;
     acum_dias  := acum_dias  + total_dias;

  END LOOP;

  if totparcven > 0 then
    pPrazoMedio := acum_dias / totparcven;
  else
    pPrazoMedio := 0;
  end if;

  return(pPrazoMedio);

END inter_fn_calcula_prazo_med;
 

/

exec inter_pr_recompile;

