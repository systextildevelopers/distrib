
  CREATE OR REPLACE PROCEDURE "INTER_PR_LIMPAR_APROV_COMP" 
(
    p_codigo_projeto        in varchar2,         p_sequencia_subprojeto  in number,
    p_nivel_item            in varchar2,         p_grupo_item            in varchar2,
    p_subgru_item           in varchar2,         p_item_item             in varchar2,
    p_alternativa_produto   in number,
    p_cliente9              in number,           p_cliente4              in number,
    p_cliente2              in number
) is
   v_total_028          number;

begin
      for reg_basi_027 in (select basi_027.nivel_componente,     basi_027.grupo_componente,
                                  basi_027.subgrupo_componente,  basi_027.item_componente
                           from basi_027
                           where basi_027.codigo_projeto    = p_codigo_projeto
                             and basi_027.nivel_item        = p_nivel_item
                             and basi_027.grupo_item        = p_grupo_item
                             and basi_027.subgrupo_item     = p_subgru_item
                             and basi_027.item_item         = p_item_item
                             and basi_027.alt_item          = p_alternativa_produto
                             and basi_027.sequencia_projeto = p_sequencia_subprojeto)
      loop
         if reg_basi_027.nivel_componente    is not null and reg_basi_027.nivel_componente    <> '0'      and
            reg_basi_027.grupo_componente    is not null and reg_basi_027.grupo_componente    <> '00000'  and
            reg_basi_027.subgrupo_componente is not null and reg_basi_027.subgrupo_componente <> '000'    and
            reg_basi_027.item_componente     is not null and reg_basi_027.item_componente     <> '000000'
         then
            if reg_basi_027.nivel_componente = '2'
            then
               -- Consiste se ha registro do componente na tabela de componente aprovado.
               select count(1)
               into v_total_028
               from basi_028
               where basi_028.nivel_componente    = reg_basi_027.nivel_componente
                 and basi_028.grupo_componente    = reg_basi_027.grupo_componente
                 and basi_028.subgrupo_componente = reg_basi_027.subgrupo_componente
                 and basi_028.item_componente     = reg_basi_027.item_componente
                 and basi_028.cnpj_cliente9       = p_cliente9
                 and basi_028.cnpj_cliente4       = p_cliente4
                 and basi_028.cnpj_cliente2       = p_cliente2
                 and basi_028.tipo_aprovacao      = 1      --APROVACAO POR COMPONENTE
                 and basi_028.situacao_componente = 0;

               if v_total_028 > 0
               then
--               raise_application_error (-20000, 'Entrei'|| to_char(v_total_028)||reg_basi_027.nivel_componente||reg_basi_027.grupo_componente||reg_basi_027.subgrupo_componente||reg_basi_027.item_componente);
                  begin
                     delete basi_027
                     where basi_027.codigo_projeto      = p_codigo_projeto
                       and basi_027.nivel_item          = p_nivel_item
                       and basi_027.grupo_item          = p_grupo_item
                       and basi_027.subgrupo_item       = p_subgru_item
                       and basi_027.item_item           = p_item_item
                       and basi_027.alt_item            = p_alternativa_produto
                       and basi_027.sequencia_projeto   = p_sequencia_subprojeto
                       and basi_027.nivel_componente    = reg_basi_027.nivel_componente
                       and basi_027.grupo_componente    = reg_basi_027.grupo_componente
                       and basi_027.subgrupo_componente = reg_basi_027.subgrupo_componente
                       and basi_027.item_componente     = reg_basi_027.item_componente;
                  exception when OTHERS then
                     raise_application_error (-20000, 'Nao atualizou a tabela de Situacao do Componente(basi_027)');
                  end;
               end if;
            else
               -- Consiste se ha registro do componente na tabela de componente aprovado.
               select count(1)
               into v_total_028
               from basi_028
               where basi_028.nivel_componente    = reg_basi_027.nivel_componente
                 and basi_028.grupo_componente    = reg_basi_027.grupo_componente
                 and basi_028.subgrupo_componente = reg_basi_027.subgrupo_componente
                 and basi_028.item_componente     = reg_basi_027.item_componente
                 and basi_028.codigo_projeto      = p_codigo_projeto
                 and basi_028.sequencia_projeto   = p_sequencia_subprojeto
                 and basi_028.tipo_aprovacao      = 1    --APROVACAO POR COMPONENTE
                 and basi_028.situacao_componente = 0;
               if v_total_028 > 0
               then
                  begin
                     delete basi_028
                     where basi_028.nivel_componente    = reg_basi_027.nivel_componente
                       and basi_028.grupo_componente    = reg_basi_027.grupo_componente
                       and basi_028.subgrupo_componente = reg_basi_027.subgrupo_componente
                       and basi_028.item_componente     = reg_basi_027.item_componente
                       and basi_028.codigo_projeto      = p_codigo_projeto
                       and basi_028.sequencia_projeto   = p_sequencia_subprojeto
                       and basi_028.tipo_aprovacao      = 1    --APROVACAO POR COMPONENTE
                       and basi_028.situacao_componente = 0;
                  exception when OTHERS then
                     raise_application_error (-20000, 'Nao atualizou a tabela de Situacao do Item(basi_028)');
                  end;

                  begin
                     delete basi_027
                     where basi_027.codigo_projeto      = p_codigo_projeto
                       and basi_027.nivel_item          = p_nivel_item
                       and basi_027.grupo_item          = p_grupo_item
                       and basi_027.subgrupo_item       = p_subgru_item
                       and basi_027.item_item           = p_item_item
                       and basi_027.alt_item            = p_alternativa_produto
                       and basi_027.sequencia_projeto   = p_sequencia_subprojeto
                       and basi_027.nivel_componente    = reg_basi_027.nivel_componente
                       and basi_027.grupo_componente    = reg_basi_027.grupo_componente
                       and basi_027.subgrupo_componente = reg_basi_027.subgrupo_componente
                       and basi_027.item_componente     = reg_basi_027.item_componente;
                  exception when OTHERS then
                     raise_application_error (-20000, 'Nao atualizou a tabela de Situacao do Componente(basi_027)');
                  end;
               end if;
            end if;
         end if;
      end loop;

end inter_pr_limpar_aprov_comp;




 

/

exec inter_pr_recompile;

