create or replace trigger inter_tr_PCPT_071_log 
after insert or delete or update 
on PCPT_071 
for each row 
declare 
   ws_usuario_rede           varchar2(250) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    -- begin 
    --    select hdoc_090.programa 
    --    into v_nome_programa 
    --    from hdoc_090 
    --    where hdoc_090.sid = ws_sid 
    --      and rownum       = 1 
    --      and hdoc_090.programa not like '%menu%' 
    --      and hdoc_090.programa not like '%_m%'; 
    --    exception 
    --      when no_data_found then            v_nome_programa := 'SQL'; 
    -- end; 
 
    v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 
 
 if inserting 
 then 
    begin 
 
        insert into PCPT_071_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           id_OLD,   /*8*/ 
           id_NEW,   /*9*/ 
           id_sugestao_OLD,   /*10*/ 
           id_sugestao_NEW,   /*11*/ 
           pedido_venda_OLD,   /*12*/ 
           pedido_venda_NEW,   /*13*/ 
           seq_item_pedido_OLD,   /*14*/ 
           seq_item_pedido_NEW,   /*15*/ 
           sequencia_OLD,   /*16*/ 
           sequencia_NEW,   /*17*/ 
           codigo_rolo_OLD,   /*18*/ 
           codigo_rolo_NEW,   /*19*/ 
           tipo_de_sugestao_OLD,   /*20*/ 
           tipo_de_sugestao_NEW    /*21*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.id, /*9*/   
           0,/*10*/
           :new.id_sugestao, /*11*/   
           0,/*12*/
           :new.pedido_venda, /*13*/   
           0,/*14*/
           :new.seq_item_pedido, /*15*/   
           0,/*16*/
           :new.sequencia, /*17*/   
           0,/*18*/
           :new.codigo_rolo, /*19*/   
           '',/*20*/
           :new.tipo_de_sugestao /*21*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into PCPT_071_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           id_OLD, /*8*/   
           id_NEW, /*9*/   
           id_sugestao_OLD, /*10*/   
           id_sugestao_NEW, /*11*/   
           pedido_venda_OLD, /*12*/   
           pedido_venda_NEW, /*13*/   
           seq_item_pedido_OLD, /*14*/   
           seq_item_pedido_NEW, /*15*/   
           sequencia_OLD, /*16*/   
           sequencia_NEW, /*17*/   
           codigo_rolo_OLD, /*18*/   
           codigo_rolo_NEW, /*19*/   
           tipo_de_sugestao_OLD, /*20*/   
           tipo_de_sugestao_NEW  /*21*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.id,  /*8*/  
           :new.id, /*9*/   
           :old.id_sugestao,  /*10*/  
           :new.id_sugestao, /*11*/   
           :old.pedido_venda,  /*12*/  
           :new.pedido_venda, /*13*/   
           :old.seq_item_pedido,  /*14*/  
           :new.seq_item_pedido, /*15*/   
           :old.sequencia,  /*16*/  
           :new.sequencia, /*17*/   
           :old.codigo_rolo,  /*18*/  
           :new.codigo_rolo, /*19*/   
           :old.tipo_de_sugestao,  /*20*/  
           :new.tipo_de_sugestao  /*21*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into PCPT_071_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           id_OLD, /*8*/   
           id_NEW, /*9*/   
           id_sugestao_OLD, /*10*/   
           id_sugestao_NEW, /*11*/   
           pedido_venda_OLD, /*12*/   
           pedido_venda_NEW, /*13*/   
           seq_item_pedido_OLD, /*14*/   
           seq_item_pedido_NEW, /*15*/   
           sequencia_OLD, /*16*/   
           sequencia_NEW, /*17*/   
           codigo_rolo_OLD, /*18*/   
           codigo_rolo_NEW, /*19*/   
           tipo_de_sugestao_OLD, /*20*/   
           tipo_de_sugestao_NEW /*21*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.id, /*8*/   
           0, /*9*/
           :old.id_sugestao, /*10*/   
           0, /*11*/
           :old.pedido_venda, /*12*/   
           0, /*13*/
           :old.seq_item_pedido, /*14*/   
           0, /*15*/
           :old.sequencia, /*16*/   
           0, /*17*/
           :old.codigo_rolo, /*18*/   
           0, /*19*/
           :old.tipo_de_sugestao, /*20*/   
           '' /*21*/
         );    
    end;    
 end if;    
end inter_tr_PCPT_071_log;
