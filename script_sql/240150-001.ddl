create table OBRF_183
(
  empresa         number(3) default 0 not null,
  codigo_sql      number(5) default 0,
  nivel           varchar2(1),
  grupo           varchar2(5),
  subgrupo        varchar2(3),
  item            varchar2(6),
  colecao         number(3),
  marca           number(3),
  local_resultado number(1) default 0
);
 
 
alter table obrf_182
add local_mensagem number(1) default 0;
 
 
 
insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f234', 'Cadastro de relacionamento de SQL de descricao de item da nota.', 0,1);
 
insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f234', 'obrf_menu', 1, 20, 'S', 'S', 'S', 'S');
