create table OBRF_166
(
  NFE_ID           NUMBER(9) default 0 not null,
  COD_EMPRESA      NUMBER(9) default 0 not null,
  COD_USUARIO      NUMBER(9) default 0 not null,
  CGC              VARCHAR2(44) default ''
);


create table PEDI_232
(
  CGC_9             NUMBER(9) default 0 not null,
  CGC_4             NUMBER(4) default 0 not null,
  CGC_2             NUMBER(2) default 0 not null,
  TIPO              NUMBER(2) default 0 not null,
  CGC_9_RELACIONADO NUMBER(9) default 0 not null,
  CGC_4_RELACIONADO NUMBER(4) default 0 not null,
  CGC_2_RELACIONADO NUMBER(2) default 0 not null,
  EMPRESA           NUMBER(3) default 0 not null
);


