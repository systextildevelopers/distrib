alter table cont_012 add (ind_pais_a_pais varchar2(1) default 'N');
comment on column cont_012.ind_pais_a_pais is 'A pessoa jur�dica � entidade integrante de grupo multinacional, nos termos da Instru��o Normativa RFB no 1.681/2016: Sim ou N�o.';

exec inter_pr_recompile;
