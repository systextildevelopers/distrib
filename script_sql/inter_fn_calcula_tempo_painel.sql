
  CREATE OR REPLACE FUNCTION "INTER_FN_CALCULA_TEMPO_PAINEL" (p_tipo_alocacao                 in number,      p_tipo_recurso                  in number,
                                                          p_codigo_recurso                in varchar2,    p_qtde_produto                  in number,
                                                          p_perc_curva_aprendizagem_01    in number,      p_perc_curva_aprendizagem_02    in number,
                                                          p_perc_curva_aprendizagem_03    in number,      p_perc_curva_aprendizagem_04    in number,
                                                          p_perc_curva_aprendizagem_05    in number,      p_setup                         in number,
                                                          p_minutos_unitario_recurso      in number,      p_numero_recursos               in number,
                                                          p_fator_correcao                in number,      p_perc_eficiencia               in number,
                                                          p_data_inicio                   in date,        p_ordem_producao                in number,
                                                          p_tipo_ot                       in varchar2)
RETURN number is
-- Variavel a ser retornada no calculo da funcao
   v_minutos_total                 number (15,4);

-- Variaveis a serem utilizadas no calculo do tempo
   v_contador_dia                  number;
   v_total_minutos_cont            number;
   v_diferenca_minutos             number;
   v_tamanho_barra                 number;

   v_total_minutos_dia_01          tmrp_680.minutos_pecas%type;
   v_total_minutos_dia_02          tmrp_680.minutos_pecas%type;
   v_total_minutos_dia_03          tmrp_680.minutos_pecas%type;
   v_total_minutos_dia_04          tmrp_680.minutos_pecas%type;
   v_total_minutos_dia_05          tmrp_680.minutos_pecas%type;
   v_tempo_disponivel              tmrp_660.tempo_disponivel%type;
   v_data_recurso                  tmrp_660.data_recurso%type;
   v_tempo_total_recurso           number;
   v_tempo_disp_ult_dia            number;
   v_controle_dias                 number;
   v_mensagem_erro                 varchar2(300);
   v_ultima_data_recurso           date;
   v_dif_tempo_cosid_eficiencia    number;
   v_minutos_total_sem_eficiencia  number;

   v_grupo_maquina                 varchar2(4);
   v_subgrupo_maquina              varchar2(3);

begin
   v_grupo_maquina:= inter_fn_string_tokenizer(p_codigo_recurso, 1);
   v_subgrupo_maquina:= inter_fn_string_tokenizer(p_codigo_recurso, 2);

   -- Pesquisando o tempo disponivel para o recurso no calendario
   if (p_numero_recursos > 0 and p_minutos_unitario_recurso > 0)
   or (p_ordem_producao > 0)
   then
      -- Se mandar a ordem de tingimento, calculamos o tempo a partir dela...
      if p_ordem_producao > 0 and p_tipo_alocacao = 2 -- Beneficiamento
      then v_minutos_total := inter_fn_calc_tempo_ot(p_tipo_ot, p_ordem_producao, v_grupo_maquina, v_subgrupo_maquina);
      else
         -- Se nao receber uma ordem de tingimento, calcula baseado nos recursos...
         -- Calculando o tempo total da reserva considerando a qtde,tempo peca eficiencia e fator de correcao
         v_minutos_total_sem_eficiencia := p_qtde_produto * p_minutos_unitario_recurso;

         --O valor de v_dif_tempo_cosiderando_eficiencia pode ser positivo ou negativo.
         v_dif_tempo_cosid_eficiencia := (v_minutos_total_sem_eficiencia / (p_perc_eficiencia / 100) ) - v_minutos_total_sem_eficiencia;

         v_minutos_total := ((v_minutos_total_sem_eficiencia + v_dif_tempo_cosid_eficiencia) * (p_fator_correcao/100.00)) / p_numero_recursos;
      end if;

      if p_tipo_alocacao = 0
      or p_tipo_alocacao = 1
      then
         -- Fazendo um loop para achar quantos dias devemos trabalhar
         -- Para os cinco primeiros dias o tempo disponivel por dia considera a curva de aprendizagem.
         v_contador_dia        := 0;
         v_total_minutos_cont  := 0;
         v_diferenca_minutos   := 0;
         v_data_recurso        := p_data_inicio;
         v_tempo_total_recurso := 0;
         v_tempo_disp_ult_dia  := 0;
         v_controle_dias       := 0;

         while v_total_minutos_cont <= v_minutos_total
         loop
            begin
               select tmrp_660.tempo_disponivel
               into v_tempo_disponivel
               from tmrp_660
               where tmrp_660.tipo_alocacao  = p_tipo_alocacao
                 and tmrp_660.tipo_recurso   = p_tipo_recurso
                 and tmrp_660.codigo_recurso = p_codigo_recurso
                 and tmrp_660.data_recurso   = v_data_recurso;
               exception
               when others then
                  v_tempo_disponivel := 0.00000;
            end;

            -- Somente considera dias que tenham o tempo disponivel.
            if v_tempo_disponivel > 0.00000
            then
               v_contador_dia        := v_contador_dia + 1;
               v_tempo_total_recurso := v_tempo_total_recurso + v_tempo_disponivel;
               v_tempo_disp_ult_dia  := v_tempo_disponivel;
               v_ultima_data_recurso := v_data_recurso;

               if v_contador_dia = 1
               then
                  v_total_minutos_dia_01 := v_tempo_disponivel * (p_perc_curva_aprendizagem_01/100);
                  v_total_minutos_cont   := v_total_minutos_cont + v_total_minutos_dia_01;
               end if;

               if v_contador_dia = 2
               then
                  v_total_minutos_dia_02 := v_tempo_disponivel * (p_perc_curva_aprendizagem_02/100);
                  v_total_minutos_cont   := v_total_minutos_cont + v_total_minutos_dia_02;
               end if;

               if v_contador_dia = 3
               then
                  v_total_minutos_dia_03 := v_tempo_disponivel * (p_perc_curva_aprendizagem_03/100);
                  v_total_minutos_cont   := v_total_minutos_cont + v_total_minutos_dia_03;
               end if;

               if v_contador_dia = 4
               then
                  v_total_minutos_dia_04 := v_tempo_disponivel * (p_perc_curva_aprendizagem_04/100);
                  v_total_minutos_cont   := v_total_minutos_cont + v_total_minutos_dia_04;
               end if;

               if v_contador_dia = 5
               then
                  v_total_minutos_dia_05 := v_tempo_disponivel * (p_perc_curva_aprendizagem_05/100);
                  v_total_minutos_cont   := v_total_minutos_cont + v_total_minutos_dia_05;
               end if;

               if v_contador_dia > 5
               then
                  v_total_minutos_cont := v_total_minutos_cont + v_tempo_disponivel;
               end if;

               if v_minutos_total > v_total_minutos_cont
               then
                  v_diferenca_minutos := v_minutos_total - v_total_minutos_cont;
               else
                  if v_contador_dia = 1 --- Somente se nao completou o primeiro dia
                  then
                     v_diferenca_minutos := v_minutos_total;
                  end if;
            end if;
            else
               v_controle_dias := v_controle_dias + 1;

            end if;

            v_data_recurso := v_data_recurso + 1; --- Acumula 1 dia na data para achar o proximo dia.

            -- Se um recurso nao conter calendario para no minimo 6 meses, retornara uma mensagem
            -- para o usuario avisando que devera gerar o calendario do recurso.
            if v_controle_dias > 180
            then
               v_mensagem_erro := 'Nao existe calendario do recurso suficiente para a planificacao da Ordem. Ultima data do caledario para o recurso = ' || to_char(v_ultima_data_recurso, 'DD/MM/YY');
               raise_application_error(-20000,v_mensagem_erro);
            end if;
         end loop;

         -- calculando o tamanho da barra
         -- Desconta sempre o ultimo dia pois o ultimo dia seria o resto do calculo (diferenca)
         v_tamanho_barra := p_setup + (v_tempo_total_recurso - v_tempo_disp_ult_dia)  + v_diferenca_minutos;
      else
         v_tamanho_barra := v_minutos_total;
      end if;
   end if;

   return(v_tamanho_barra);

end inter_fn_calcula_tempo_painel;
 

/

exec inter_pr_recompile;

