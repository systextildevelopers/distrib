
  CREATE OR REPLACE TRIGGER "INTER_TR_LOJA_210_LOG" 
after insert or delete or update
on LOJA_210
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


   v_nome_programa := inter_fn_nome_programa(ws_sid);  
      
 if inserting
 then
    begin

        insert into LOJA_210_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           COD_EMPRESA    ,   /*9*/
           DOCUMENTO    ,   /*11*/
           SEQUENCIA    ,   /*13*/
           NIVEL_OLD,   /*14*/
           NIVEL_NEW,   /*15*/
           GRUPO_OLD,   /*16*/
           GRUPO_NEW,   /*17*/
           SUB_OLD,   /*18*/
           SUB_NEW,   /*19*/
           ITEM_OLD,   /*20*/
           ITEM_NEW,   /*21*/
           QTDE_OLD,   /*22*/
           QTDE_NEW,   /*23*/
           LOTE_OLD,   /*24*/
           LOTE_NEW,   /*25*/
           PRECO_UNITARIO_OLD,   /*26*/
           PRECO_UNITARIO_NEW,   /*27*/
           PRECO_BRUTO_OLD,   /*28*/
           PRECO_BRUTO_NEW,   /*29*/
           PERC_DESC_OLD,   /*30*/
           PERC_DESC_NEW,   /*31*/
           DEPOSITO_OLD,   /*32*/
           DEPOSITO_NEW,   /*33*/
           DOC_CONSIGNACAO_OLD,   /*34*/
           DOC_CONSIGNACAO_NEW,   /*35*/
           AGRUPADOR_OLD,   /*36*/
           AGRUPADOR_NEW,   /*37*/
           VENDA_TROCA_OLD,   /*38*/
           VENDA_TROCA_NEW,   /*39*/
           TAB_COL_OLD,   /*40*/
           TAB_COL_NEW,   /*41*/
           TAB_MES_OLD,   /*42*/
           TAB_MES_NEW,   /*43*/
           TAB_SEQ_OLD,   /*44*/
           TAB_SEQ_NEW,   /*45*/
           PRECO_INF_MANUAL_OLD,   /*46*/
           PRECO_INF_MANUAL_NEW,   /*47*/
           PRECO_CUSTO_OLD,   /*48*/
           PRECO_CUSTO_NEW,   /*49*/
           MOTIVO_TROCA_OLD,   /*50*/
           MOTIVO_TROCA_NEW,   /*51*/
           VALOR_ACRESC_UNIT_ITEM_OLD,   /*54*/
           VALOR_ACRESC_UNIT_ITEM_NEW,   /*55*/
           VALOR_DESC_TOTAL_ITEM_OLD,   /*56*/
           VALOR_DESC_TOTAL_ITEM_NEW,   /*57*/
           NATU_LOJA_OLD,   /*58*/
           NATU_LOJA_NEW,   /*59*/
           ESTADO_NATU_OLD,   /*60*/
           ESTADO_NATU_NEW,   /*61*/
           FLAG_CANCELAMENTO_OLD,   /*62*/
           FLAG_CANCELAMENTO_NEW,   /*63*/
           TRANSACAO_OLD,   /*64*/
           TRANSACAO_NEW,   /*65*/
           NUMERO_VOLUME_OLD,   /*66*/
           NUMERO_VOLUME_NEW,   /*67*/
           PRECO_TABELA_OLD,   /*68*/
           PRECO_TABELA_NEW,   /*69*/
           PERC_ACRES_OLD,   /*72*/
           PERC_ACRES_NEW,   /*73*/
           VALOR_TOTAL_ITEM_OLD,   /*74*/
           VALOR_TOTAL_ITEM_NEW    /*75*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :new.COD_EMPRESA, /*9*/
           :new.DOCUMENTO, /*11*/
           :new.SEQUENCIA, /*13*/
           '',/*14*/
           :new.NIVEL, /*15*/
           '',/*16*/
           :new.GRUPO, /*17*/
           '',/*18*/
           :new.SUB, /*19*/
           '',/*20*/
           :new.ITEM, /*21*/
           0,/*22*/
           :new.QTDE, /*23*/
           0,/*24*/
           :new.LOTE, /*25*/
           0,/*26*/
           :new.PRECO_UNITARIO, /*27*/
           0,/*28*/
           :new.PRECO_BRUTO, /*29*/
           0,/*30*/
           :new.PERC_DESC, /*31*/
           0,/*32*/
           :new.DEPOSITO, /*33*/
           0,/*34*/
           :new.DOC_CONSIGNACAO, /*35*/
           0,/*36*/
           :new.AGRUPADOR, /*37*/
           '',/*38*/
           :new.VENDA_TROCA, /*39*/
           0,/*40*/
           :new.TAB_COL, /*41*/
           0,/*42*/
           :new.TAB_MES, /*43*/
           0,/*44*/
           :new.TAB_SEQ, /*45*/
           0,/*46*/
           :new.PRECO_INF_MANUAL, /*47*/
           0,/*48*/
           :new.PRECO_CUSTO, /*49*/
           0,/*50*/
           :new.MOTIVO_TROCA, /*51*/
           0,/*54*/
           :new.VALOR_ACRESC_UNIT_ITEM, /*55*/
           0,/*56*/
           :new.VALOR_DESC_TOTAL_ITEM, /*57*/
           0,/*58*/
           :new.NATU_LOJA, /*59*/
           '',/*60*/
           :new.ESTADO_NATU, /*61*/
           0,/*62*/
           :new.FLAG_CANCELAMENTO, /*63*/
           0,/*64*/
           :new.TRANSACAO, /*65*/
           0,/*66*/
           :new.NUMERO_VOLUME, /*67*/
           0,/*68*/
           :new.PRECO_TABELA, /*69*/
           0,/*72*/
           :new.PERC_ACRES, /*73*/
           0,/*74*/
           :new.VALOR_TOTAL_ITEM /*75*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into LOJA_210_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_EMPRESA    , /*9*/
           DOCUMENTO    , /*11*/
           SEQUENCIA    , /*13*/
           NIVEL_OLD, /*14*/
           NIVEL_NEW, /*15*/
           GRUPO_OLD, /*16*/
           GRUPO_NEW, /*17*/
           SUB_OLD, /*18*/
           SUB_NEW, /*19*/
           ITEM_OLD, /*20*/
           ITEM_NEW, /*21*/
           QTDE_OLD, /*22*/
           QTDE_NEW, /*23*/
           LOTE_OLD, /*24*/
           LOTE_NEW, /*25*/
           PRECO_UNITARIO_OLD, /*26*/
           PRECO_UNITARIO_NEW, /*27*/
           PRECO_BRUTO_OLD, /*28*/
           PRECO_BRUTO_NEW, /*29*/
           PERC_DESC_OLD, /*30*/
           PERC_DESC_NEW, /*31*/
           DEPOSITO_OLD, /*32*/
           DEPOSITO_NEW, /*33*/
           DOC_CONSIGNACAO_OLD, /*34*/
           DOC_CONSIGNACAO_NEW, /*35*/
           AGRUPADOR_OLD, /*36*/
           AGRUPADOR_NEW, /*37*/
           VENDA_TROCA_OLD, /*38*/
           VENDA_TROCA_NEW, /*39*/
           TAB_COL_OLD, /*40*/
           TAB_COL_NEW, /*41*/
           TAB_MES_OLD, /*42*/
           TAB_MES_NEW, /*43*/
           TAB_SEQ_OLD, /*44*/
           TAB_SEQ_NEW, /*45*/
           PRECO_INF_MANUAL_OLD, /*46*/
           PRECO_INF_MANUAL_NEW, /*47*/
           PRECO_CUSTO_OLD, /*48*/
           PRECO_CUSTO_NEW, /*49*/
           MOTIVO_TROCA_OLD, /*50*/
           MOTIVO_TROCA_NEW, /*51*/
           VALOR_ACRESC_UNIT_ITEM_OLD, /*54*/
           VALOR_ACRESC_UNIT_ITEM_NEW, /*55*/
           VALOR_DESC_TOTAL_ITEM_OLD, /*56*/
           VALOR_DESC_TOTAL_ITEM_NEW, /*57*/
           NATU_LOJA_OLD, /*58*/
           NATU_LOJA_NEW, /*59*/
           ESTADO_NATU_OLD, /*60*/
           ESTADO_NATU_NEW, /*61*/
           FLAG_CANCELAMENTO_OLD, /*62*/
           FLAG_CANCELAMENTO_NEW, /*63*/
           TRANSACAO_OLD, /*64*/
           TRANSACAO_NEW, /*65*/
           NUMERO_VOLUME_OLD, /*66*/
           NUMERO_VOLUME_NEW, /*67*/
           PRECO_TABELA_OLD, /*68*/
           PRECO_TABELA_NEW, /*69*/
           PERC_ACRES_OLD, /*72*/
           PERC_ACRES_NEW, /*73*/
           VALOR_TOTAL_ITEM_OLD, /*74*/
           VALOR_TOTAL_ITEM_NEW  /*75*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :new.COD_EMPRESA, /*9*/
           :new.DOCUMENTO, /*11*/
           :new.SEQUENCIA, /*13*/
           :old.NIVEL,  /*14*/
           :new.NIVEL, /*15*/
           :old.GRUPO,  /*16*/
           :new.GRUPO, /*17*/
           :old.SUB,  /*18*/
           :new.SUB, /*19*/
           :old.ITEM,  /*20*/
           :new.ITEM, /*21*/
           :old.QTDE,  /*22*/
           :new.QTDE, /*23*/
           :old.LOTE,  /*24*/
           :new.LOTE, /*25*/
           :old.PRECO_UNITARIO,  /*26*/
           :new.PRECO_UNITARIO, /*27*/
           :old.PRECO_BRUTO,  /*28*/
           :new.PRECO_BRUTO, /*29*/
           :old.PERC_DESC,  /*30*/
           :new.PERC_DESC, /*31*/
           :old.DEPOSITO,  /*32*/
           :new.DEPOSITO, /*33*/
           :old.DOC_CONSIGNACAO,  /*34*/
           :new.DOC_CONSIGNACAO, /*35*/
           :old.AGRUPADOR,  /*36*/
           :new.AGRUPADOR, /*37*/
           :old.VENDA_TROCA,  /*38*/
           :new.VENDA_TROCA, /*39*/
           :old.TAB_COL,  /*40*/
           :new.TAB_COL, /*41*/
           :old.TAB_MES,  /*42*/
           :new.TAB_MES, /*43*/
           :old.TAB_SEQ,  /*44*/
           :new.TAB_SEQ, /*45*/
           :old.PRECO_INF_MANUAL,  /*46*/
           :new.PRECO_INF_MANUAL, /*47*/
           :old.PRECO_CUSTO,  /*48*/
           :new.PRECO_CUSTO, /*49*/
           :old.MOTIVO_TROCA,  /*50*/
           :new.MOTIVO_TROCA, /*51*/
           :old.VALOR_ACRESC_UNIT_ITEM,  /*54*/
           :new.VALOR_ACRESC_UNIT_ITEM, /*55*/
           :old.VALOR_DESC_TOTAL_ITEM,  /*56*/
           :new.VALOR_DESC_TOTAL_ITEM, /*57*/
           :old.NATU_LOJA,  /*58*/
           :new.NATU_LOJA, /*59*/
           :old.ESTADO_NATU,  /*60*/
           :new.ESTADO_NATU, /*61*/
           :old.FLAG_CANCELAMENTO,  /*62*/
           :new.FLAG_CANCELAMENTO, /*63*/
           :old.TRANSACAO,  /*64*/
           :new.TRANSACAO, /*65*/
           :old.NUMERO_VOLUME,  /*66*/
           :new.NUMERO_VOLUME, /*67*/
           :old.PRECO_TABELA,  /*68*/
           :new.PRECO_TABELA, /*69*/
           :old.PERC_ACRES,  /*72*/
           :new.PERC_ACRES, /*73*/
           :old.VALOR_TOTAL_ITEM,  /*74*/
           :new.VALOR_TOTAL_ITEM  /*75*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into LOJA_210_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_EMPRESA    , /*9*/
           DOCUMENTO    , /*11*/
           SEQUENCIA    , /*13*/
           NIVEL_OLD, /*14*/
           NIVEL_NEW, /*15*/
           GRUPO_OLD, /*16*/
           GRUPO_NEW, /*17*/
           SUB_OLD, /*18*/
           SUB_NEW, /*19*/
           ITEM_OLD, /*20*/
           ITEM_NEW, /*21*/
           QTDE_OLD, /*22*/
           QTDE_NEW, /*23*/
           LOTE_OLD, /*24*/
           LOTE_NEW, /*25*/
           PRECO_UNITARIO_OLD, /*26*/
           PRECO_UNITARIO_NEW, /*27*/
           PRECO_BRUTO_OLD, /*28*/
           PRECO_BRUTO_NEW, /*29*/
           PERC_DESC_OLD, /*30*/
           PERC_DESC_NEW, /*31*/
           DEPOSITO_OLD, /*32*/
           DEPOSITO_NEW, /*33*/
           DOC_CONSIGNACAO_OLD, /*34*/
           DOC_CONSIGNACAO_NEW, /*35*/
           AGRUPADOR_OLD, /*36*/
           AGRUPADOR_NEW, /*37*/
           VENDA_TROCA_OLD, /*38*/
           VENDA_TROCA_NEW, /*39*/
           TAB_COL_OLD, /*40*/
           TAB_COL_NEW, /*41*/
           TAB_MES_OLD, /*42*/
           TAB_MES_NEW, /*43*/
           TAB_SEQ_OLD, /*44*/
           TAB_SEQ_NEW, /*45*/
           PRECO_INF_MANUAL_OLD, /*46*/
           PRECO_INF_MANUAL_NEW, /*47*/
           PRECO_CUSTO_OLD, /*48*/
           PRECO_CUSTO_NEW, /*49*/
           MOTIVO_TROCA_OLD, /*50*/
           MOTIVO_TROCA_NEW, /*51*/
           VALOR_ACRESC_UNIT_ITEM_OLD, /*54*/
           VALOR_ACRESC_UNIT_ITEM_NEW, /*55*/
           VALOR_DESC_TOTAL_ITEM_OLD, /*56*/
           VALOR_DESC_TOTAL_ITEM_NEW, /*57*/
           NATU_LOJA_OLD, /*58*/
           NATU_LOJA_NEW, /*59*/
           ESTADO_NATU_OLD, /*60*/
           ESTADO_NATU_NEW, /*61*/
           FLAG_CANCELAMENTO_OLD, /*62*/
           FLAG_CANCELAMENTO_NEW, /*63*/
           TRANSACAO_OLD, /*64*/
           TRANSACAO_NEW, /*65*/
           NUMERO_VOLUME_OLD, /*66*/
           NUMERO_VOLUME_NEW, /*67*/
           PRECO_TABELA_OLD, /*68*/
           PRECO_TABELA_NEW, /*69*/
           PERC_ACRES_OLD, /*72*/
           PERC_ACRES_NEW, /*73*/
           VALOR_TOTAL_ITEM_OLD, /*74*/
           VALOR_TOTAL_ITEM_NEW /*75*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.COD_EMPRESA, /*9*/
           :old.DOCUMENTO, /*11*/
           :old.SEQUENCIA, /*13*/
           :old.NIVEL, /*14*/
           '', /*15*/
           :old.GRUPO, /*16*/
           '', /*17*/
           :old.SUB, /*18*/
           '', /*19*/
           :old.ITEM, /*20*/
           '', /*21*/
           :old.QTDE, /*22*/
           0, /*23*/
           :old.LOTE, /*24*/
           0, /*25*/
           :old.PRECO_UNITARIO, /*26*/
           0, /*27*/
           :old.PRECO_BRUTO, /*28*/
           0, /*29*/
           :old.PERC_DESC, /*30*/
           0, /*31*/
           :old.DEPOSITO, /*32*/
           0, /*33*/
           :old.DOC_CONSIGNACAO, /*34*/
           0, /*35*/
           :old.AGRUPADOR, /*36*/
           0, /*37*/
           :old.VENDA_TROCA, /*38*/
           '', /*39*/
           :old.TAB_COL, /*40*/
           0, /*41*/
           :old.TAB_MES, /*42*/
           0, /*43*/
           :old.TAB_SEQ, /*44*/
           0, /*45*/
           :old.PRECO_INF_MANUAL, /*46*/
           0, /*47*/
           :old.PRECO_CUSTO, /*48*/
           0, /*49*/
           :old.MOTIVO_TROCA, /*50*/
           0, /*51*/
           :old.VALOR_ACRESC_UNIT_ITEM, /*54*/
           0, /*55*/
           :old.VALOR_DESC_TOTAL_ITEM, /*56*/
           0, /*57*/
           :old.NATU_LOJA, /*58*/
           0, /*59*/
           :old.ESTADO_NATU, /*60*/
           '', /*61*/
           :old.FLAG_CANCELAMENTO, /*62*/
           0, /*63*/
           :old.TRANSACAO, /*64*/
           0, /*65*/
           :old.NUMERO_VOLUME, /*66*/
           0, /*67*/
           :old.PRECO_TABELA, /*68*/
           0, /*69*/
           :old.PERC_ACRES, /*72*/
           0, /*73*/
           :old.VALOR_TOTAL_ITEM, /*74*/
           0 /*75*/
         );
    end;
 end if;
end inter_tr_LOJA_210_log;

-- ALTER TRIGGER "INTER_TR_LOJA_210_LOG" ENABLE
 

/

exec inter_pr_recompile;

