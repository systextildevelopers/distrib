create or replace procedure "PR_INSERE_SUPR_002"
(v_opc_selecao IN NUMBER,
v_inclui_req IN NUMBER,
v_cgc9 IN NUMBER,
v_cgc4 IN NUMBER,
v_cgc2 IN NUMBER,
v_cod_tabela IN NUMBER,
v_nivel IN VARCHAR2 default '',
v_grupo IN VARCHAR2 default '',
v_sub IN VARCHAR2 default '',
v_item IN VARCHAR2 default '',
v_num_req IN NUMBER default 0,
v_cod_comprador IN NUMBER,
v_ccusto IN NUMBER,
v_cod_transacao IN NUMBER,
v_cod_contabil IN NUMBER,
v_usuario IN VARCHAR2,
v_cod_empresa IN NUMBER,
v_msg_erro OUT VARCHAR2)
is

    v_retorno_rel_marca BOOLEAN;
    v_valor_unitario_aux NUMBER(18,5);
    v_nivel_aux VARCHAR2(1);
    v_grupo_aux VARCHAR2(5);
    v_sub_aux VARCHAR2(3);
    v_item_aux VARCHAR2(6);
    v_data_validade_aux DATE;
    v_qtde_requisitada_aux FLOAT;
    v_seq_item_req_aux NUMBER;
    v_num_requisicao_aux NUMBER;
    v_desc_item_aux VARCHAR2(60);
    v_pedido_compra_aux NUMBER;
    v_valor_unitario_auxiliar FLOAT;
    v_tmp_int NUMBER;
    v_perc_ipi_prod FLOAT;
    v_class_fisc_prod varchar2(15);
    v_cont NUMBER;
    v_fator_conv NUMBER;
    v_qtde_ped_conv NUMBER(15,5);
    v_qtde_req_conv NUMBER(15,5);
    v_valor_unit_conv NUMBER(15,5);
    v_cod_usuario NUMBER;
    v_cod_empresa_requis NUMBER;

    --  BUSCAR OS PRODUTOS DA REQUISICAO SELECIONADA NO GRID 
    cursor opc1(inclui_requis in NUMBER, cgc9_cursor in NUMBER,
                cgc4_cursor in NUMBER, cgc2_cursor in NUMBER,
                cod_tabela_cursor in NUMBER, nivel_cursor in VARCHAR2,
                grupo_cursor in VARCHAR2, sub_cursor in VARCHAR2,
                item_cursor in VARCHAR2, num_req_cursor in NUMBER,
                cod_comprador_cursor in NUMBER) is 
    select item_req_nivel99, item_req_grupo,
        item_req_subgrupo, item_req_item,
        qtde_requisitada, seq_item_req,
        num_requisicao, descricao_item,
        codigo_empresa
    from (
        select 
            supr_067.item_req_nivel99, supr_067.item_req_grupo,
            supr_067.item_req_subgrupo, supr_067.item_req_item,
            supr_067.qtde_requisitada, supr_067.seq_item_req,
            supr_067.num_requisicao, supr_067.descricao_item,
            supr_065.codigo_empresa
        from supr_067, supr_580, supr_065
        where supr_067.item_req_nivel99 = supr_580.NIVEL(+)
          and supr_067.item_req_grupo = supr_580.GRUPO (+)
          and supr_067.item_req_subgrupo = supr_580.SUB (+)
          and supr_067.item_req_item = supr_580.ITEM (+)
          and (cod_tabela_cursor > 0 and (supr_580.cgc_forn9 = cgc9_cursor
                                    and supr_580.cgc_forn4 = cgc4_cursor
                                    and supr_580.cgc_forn2 = cgc2_cursor
                                    and supr_580.cod_tabela = cod_tabela_cursor)
          or cod_tabela_cursor = 0)
          and supr_067.num_requisicao = supr_065.num_requisicao
          and supr_067.cod_cancelamento = 0
          and supr_067.numero_pedido = 0
          and (supr_067.numero_coleta = 0 or inclui_requis = 1)
          and supr_067.situacao <> 9
          and supr_067.num_requisicao = num_req_cursor
          and supr_067.codigo_comprador = cod_comprador_cursor
        
        union

        select 
            supr_067.item_req_nivel99, supr_067.item_req_grupo,
            supr_067.item_req_subgrupo, supr_067.item_req_item,
            supr_067.qtde_requisitada, supr_067.seq_item_req,
            supr_067.num_requisicao, supr_067.descricao_item,
            supr_065.codigo_empresa
        from supr_067, supr_580, supr_065
        where supr_067.item_req_nivel99 = supr_580.NIVEL(+)
          and supr_067.item_req_grupo = supr_580.GRUPO (+)
          and (supr_067.item_req_subgrupo = supr_580.SUB or supr_580.SUB = '000')
          and (supr_067.item_req_item = supr_580.ITEM or supr_580.ITEM = '000000')
          and (cod_tabela_cursor > 0 and (supr_580.cgc_forn9 = cgc9_cursor
                                    and supr_580.cgc_forn4 = cgc4_cursor
                                    and supr_580.cgc_forn2 = cgc2_cursor
                                    and supr_580.cod_tabela = cod_tabela_cursor)
          or cod_tabela_cursor = 0)
          and supr_067.num_requisicao = supr_065.num_requisicao
          and supr_067.cod_cancelamento = 0
          and supr_067.numero_pedido = 0
          and (supr_067.numero_coleta = 0 or inclui_requis = 1)
          and supr_067.situacao <> 9
          and supr_067.num_requisicao = num_req_cursor
          and supr_067.codigo_comprador = cod_comprador_cursor
    ) opc1;
    
    -- BUSCAR TODAS AS REQUISI��ES DO PRODUTO SELECIONADO

    cursor opc2(inclui_requis in NUMBER, cgc9_cursor in NUMBER,
                cgc4_cursor in NUMBER, cgc2_cursor in NUMBER,
                cod_tabela_cursor in NUMBER, nivel_cursor in VARCHAR2,
                grupo_cursor in VARCHAR2, sub_cursor in VARCHAR2,
                item_cursor in VARCHAR2, cod_comprador_cursor in NUMBER) is
    select item_req_nivel99, item_req_grupo,
            item_req_subgrupo, item_req_item,
            qtde_requisitada, seq_item_req,
            num_requisicao, descricao_item,
            codigo_empresa
    from (
        select supr_067.item_req_nivel99, supr_067.item_req_grupo,
            supr_067.item_req_subgrupo, supr_067.item_req_item,
            supr_067.qtde_requisitada, supr_067.seq_item_req,
            supr_067.num_requisicao, supr_067.descricao_item,
            supr_065.codigo_empresa
        from supr_067, supr_580, supr_065
        where supr_067.item_req_nivel99 = supr_580.NIVEL(+)
          and supr_067.item_req_grupo = supr_580.GRUPO (+)
          and supr_067.item_req_subgrupo = supr_580.SUB (+)
          and supr_067.item_req_item = supr_580.ITEM (+)
          and (cod_tabela_cursor > 0 and (supr_580.cgc_forn9 = cgc9_cursor
                                    and supr_580.cgc_forn4 = cgc4_cursor
                                    and supr_580.cgc_forn2 = cgc2_cursor
                                    and supr_580.cod_tabela = cod_tabela_cursor)
          or cod_tabela_cursor = 0)
          and supr_067.item_req_nivel99 = nivel_cursor
          and supr_067.item_req_grupo = grupo_cursor
          and supr_067.item_req_subgrupo = sub_cursor
          and supr_067.item_req_item = item_cursor
          and supr_067.cod_cancelamento = 0
          and supr_067.numero_pedido = 0
          and (supr_067.numero_coleta = 0 or inclui_requis = 1)
          and supr_067.situacao = 0
          and supr_067.codigo_comprador = cod_comprador_cursor

        union

        select supr_067.item_req_nivel99, supr_067.item_req_grupo,
            supr_067.item_req_subgrupo, supr_067.item_req_item,
            supr_067.qtde_requisitada, supr_067.seq_item_req,
            supr_067.num_requisicao, supr_067.descricao_item,
            supr_065.codigo_empresa
        from supr_067, supr_580, supr_065
        where supr_067.item_req_nivel99 = supr_580.NIVEL(+)
          and supr_067.item_req_grupo = supr_580.GRUPO (+)
          and (supr_067.item_req_subgrupo = supr_580.SUB or supr_580.SUB = '000')
          and (supr_067.item_req_item = supr_580.ITEM or supr_580.ITEM = '000000')
          and (cod_tabela_cursor > 0 and (supr_580.cgc_forn9 = cgc9_cursor
                                    and supr_580.cgc_forn4 = cgc4_cursor
                                    and supr_580.cgc_forn2 = cgc2_cursor
                                    and supr_580.cod_tabela = cod_tabela_cursor)
          or cod_tabela_cursor = 0)
          and supr_067.num_requisicao = supr_065.num_requisicao
          and supr_067.item_req_nivel99 = nivel_cursor
          and supr_067.item_req_grupo = grupo_cursor
          and supr_067.item_req_subgrupo = sub_cursor
          and supr_067.item_req_item = item_cursor
          and supr_067.cod_cancelamento = 0
          and supr_067.numero_pedido = 0
          and (supr_067.numero_coleta = 0 or inclui_requis = 1)
          and supr_067.situacao = 0
          and supr_067.codigo_comprador = cod_comprador_cursor
    ) opc2;
    
    -- Busca todos os produtos de todas as requisi��es aprovadas no grid para o comprador selecionado.

    cursor opc3(inclui_requis in NUMBER, cgc9_cursor in NUMBER,
                cgc4_cursor in NUMBER, cgc2_cursor in NUMBER,
                cod_tabela_cursor in NUMBER, cod_comprador_cursor in NUMBER) is
    select item_req_nivel99, item_req_grupo,
            item_req_subgrupo, item_req_item,
            qtde_requisitada, seq_item_req,
            num_requisicao, descricao_item,
            codigo_empresa
    from (
        select supr_067.item_req_nivel99, supr_067.item_req_grupo,
            supr_067.item_req_subgrupo, supr_067.item_req_item,
            supr_067.qtde_requisitada, supr_067.seq_item_req,
            supr_067.num_requisicao, supr_067.descricao_item,
            supr_065.codigo_empresa
        from supr_067, supr_580, supr_065                       
        where supr_067.cod_cancelamento = 0
          and supr_067.numero_pedido = 0
          and (supr_067.numero_coleta = 0 or inclui_requis = 1)
          and supr_067.situacao = 0
          and supr_067.codigo_comprador = cod_comprador_cursor
          and supr_067.item_req_nivel99 = supr_580.NIVEL(+)
          and supr_067.item_req_grupo = supr_580.GRUPO (+)
          and supr_067.item_req_subgrupo = supr_580.SUB (+)
          and supr_067.item_req_item = supr_580.ITEM (+)
          and supr_067.num_requisicao = supr_065.num_requisicao
          and (cod_tabela_cursor > 0 and (supr_580.cgc_forn9 = cgc9_cursor
                        and supr_580.cgc_forn4 = cgc4_cursor
                        and supr_580.cgc_forn2 = cgc2_cursor
                        and supr_580.cod_tabela = cod_tabela_cursor)
          or cod_tabela_cursor = 0)
        
        union

        select supr_067.item_req_nivel99, supr_067.item_req_grupo,
            supr_067.item_req_subgrupo, supr_067.item_req_item,
            supr_067.qtde_requisitada, supr_067.seq_item_req,
            supr_067.num_requisicao, supr_067.descricao_item,
            supr_065.codigo_empresa
        from supr_067, supr_580, supr_065
        where supr_067.cod_cancelamento = 0
          and supr_067.numero_pedido = 0
          and (supr_067.numero_coleta = 0 or inclui_requis = 1)
          and supr_067.situacao = 0
          and supr_067.codigo_comprador = cod_comprador_cursor
          and supr_067.item_req_nivel99 = supr_580.NIVEL(+)
          and supr_067.item_req_grupo = supr_580.GRUPO (+)
          and (supr_067.item_req_subgrupo = supr_580.SUB or supr_580.SUB = '000')
          and (supr_067.item_req_item = supr_580.ITEM or supr_580.ITEM = '000000')
          and supr_067.num_requisicao = supr_065.num_requisicao
          and (cod_tabela_cursor > 0 and (supr_580.cgc_forn9 = cgc9_cursor
                        and supr_580.cgc_forn4 = cgc4_cursor
                        and supr_580.cgc_forn2 = cgc2_cursor
                        and supr_580.cod_tabela = cod_tabela_cursor)
          or cod_tabela_cursor = 0)
    ) opc3;

begin
    
    v_cod_empresa_requis := 1;

    select codigo_usuario
    into v_cod_usuario
    from hdoc_030
    where usuario = v_usuario
      and empresa = v_cod_empresa;

    if v_opc_selecao = 1
    then
        
        FOR opcao1 IN opc1(v_inclui_req, v_cgc9, v_cgc4,
                        v_cgc2, v_cod_tabela, v_nivel,
                        v_grupo, v_sub, v_item, 
                        v_num_req, v_cod_comprador)
        LOOP
            v_retorno_rel_marca := false;
            v_valor_unitario_aux := 0.00;
            v_nivel_aux := opcao1.item_req_nivel99;
            v_grupo_aux := opcao1.item_req_grupo;
            v_sub_aux := opcao1.item_req_subgrupo;
            v_item_aux := opcao1.item_req_item;
            v_qtde_requisitada_aux := opcao1.qtde_requisitada;
            v_seq_item_req_aux := opcao1.seq_item_req;
            v_num_requisicao_aux := opcao1.num_requisicao;
            v_desc_item_aux := opcao1.descricao_item;
            v_pedido_compra_aux := 0;
            v_valor_unitario_auxiliar := 0.00;
            v_tmp_int := 0;
            v_cod_empresa_requis := opcao1.codigo_empresa;

            if v_cod_tabela <> 0
            then
                begin
                    select supr_580.valor_preco, supr_580.data_validade 
                    into v_valor_unitario_aux, v_data_validade_aux
                    from supr_580
                    where supr_580.cod_tabela = v_cod_tabela
                      and supr_580.nivel = v_nivel_aux
                      and supr_580.grupo = v_grupo_aux
                      and (supr_580.sub = v_sub_aux or supr_580.sub = '000')
                      and (supr_580.item = v_item_aux or supr_580.item = '000000')
                      and supr_580.cgc_forn9 = v_cgc9
                      and supr_580.cgc_forn4 = v_cgc4
                      and supr_580.cgc_forn2 = v_cgc2 
                      and supr_580.valor_preco > 0.00;
                exception when OTHERS then
                    v_msg_erro := 'Erro primeiro sql: '||SQLERRM||chr(10);
                end;
                
                if v_data_validade_aux is null
                then 
                    v_data_validade_aux := sysdate;
                end if;
                if v_data_validade_aux < sysdate
                then 
                    raise_application_error(-20001,'ATEN��O! A tabela de pre�o informada est� com a data de validade expirada.');
                end if;
            end if;

            if v_cod_tabela = 0
            then
                begin
                select max(nvl(supr_090.pedido_compra,0)) 
                into v_pedido_compra_aux
                from supr_090, supr_100 
                where supr_090.pedido_compra = supr_100.num_ped_compra 
                  and supr_100.item_100_nivel99 = v_nivel_aux 
                  and supr_100.item_100_grupo = v_grupo_aux
                  and supr_100.item_100_subgrupo = v_sub_aux
                  and supr_100.item_100_item = v_item_aux
                  and supr_090.forn_ped_forne9 = v_cgc9
                  and supr_090.forn_ped_forne4 = v_cgc4
                  and supr_090.forn_ped_forne2 = v_cgc2 
                  and supr_090.cod_cancelamento = 0;
                exception when OTHERS then
                    v_msg_erro := v_msg_erro ||'Erro segundo sql: ' ||SQLERRM||chr(10);
                end;
                if v_pedido_compra_aux > 0
                then
                    begin
                    select nvl(supr_100.preco_item_comp,0) 
                    into v_valor_unitario_auxiliar
                    from supr_090, supr_100 
                    where supr_090.pedido_compra = supr_100.num_ped_compra 
                      and supr_100.item_100_nivel99 = v_nivel_aux
                      and supr_100.item_100_grupo = v_grupo_aux
                      and supr_100.item_100_subgrupo = v_sub_aux
                      and supr_100.item_100_item = v_item_aux
                      and supr_090.forn_ped_forne9 = v_cgc9
                      and supr_090.forn_ped_forne4 = v_cgc4
                      and supr_090.forn_ped_forne2 = v_cgc2
                      and supr_090.pedido_compra = v_pedido_compra_aux;
                    exception when OTHERS then
                        v_msg_erro := v_msg_erro || 'Erro terceiro sql: ' ||SQLERRM||chr(10);
                    end;

                    if v_valor_unitario_auxiliar <> v_valor_unitario_aux
                    then v_valor_unitario_aux := v_valor_unitario_auxiliar;
                    end if;
                end if;
            end if;
            

            if v_nivel_aux <> '0'
            then
                begin
                select 1
                into v_tmp_int 
                from supr_580
                where (supr_580.cod_tabela = v_cod_tabela or v_cod_tabela = 0)
                  and supr_580.nivel = v_nivel_aux 
                  and supr_580.grupo = v_grupo_aux 
                  and (supr_580.sub = v_sub_aux or supr_580.sub = '000') 
                  and (supr_580.item = v_item_aux or supr_580.item = '000000') 
                  and (supr_580.cgc_forn9 = v_cgc9 or supr_580.cgc_forn9 = 0) 
                  and (supr_580.cgc_forn4 = v_cgc4 or supr_580.cgc_forn4 = 0) 
                  and (supr_580.cgc_forn2 = v_cgc2 or supr_580.cgc_forn2 = 0) 
                  and supr_580.valor_preco > 0.00
                  and rownum = 1;

                exception 
                    when no_data_found then
                        v_tmp_int := 0;
                    when OTHERS then
                        v_msg_erro := v_msg_erro || 'Erro quarto sql: ' ||SQLERRM||chr(10);
                        v_tmp_int := 0;
                end;
                
                if v_tmp_int <> 1
                then
                    v_tmp_int := 0;

                    begin
                        select 1
                        into v_tmp_int 
                        from basi_010
                        where basi_010.nivel_estrutura = v_nivel_aux
                          and basi_010.grupo_estrutura = v_grupo_aux
                          and basi_010.subgru_estrutura = v_sub_aux
                          and basi_010.item_estrutura = v_item_aux
                          and rownum = 1;
                    exception when OTHERS then
                        v_msg_erro := v_msg_erro || 'Erro quinto sql: ' ||SQLERRM||chr(10);
                        v_tmp_int := 0;
                    end;

                    if v_tmp_int <> 1
                    then
                        v_perc_ipi_prod := 0;
                    else
                        begin
                        select basi_010.classific_fiscal 
                        into v_class_fisc_prod
                        from basi_010
                        where basi_010.nivel_estrutura = v_nivel_aux
                          and basi_010.grupo_estrutura = v_grupo_aux
                          and basi_010.subgru_estrutura = v_sub_aux
                          and basi_010.item_estrutura = v_item_aux;
                        exception when OTHERS then
                            v_msg_erro := v_msg_erro || 'Erro sexto sql: ' ||SQLERRM||chr(10);
                        end;

                        begin
                        select basi_240.aliquota_ipi 
                        into v_perc_ipi_prod
                        from basi_240 
                        where basi_240.classific_fiscal = v_class_fisc_prod;
                        exception when OTHERS then
                            v_msg_erro := v_msg_erro || 'Erro s�timo sql: ' ||SQLERRM||chr(10);
                        end;
                    end if;

                else
                    begin
                    select supr_580.percent_ipi 
                    into v_perc_ipi_prod
                    from supr_580
                    where (supr_580.cod_tabela = v_cod_tabela or v_cod_tabela = 0)
                      and supr_580.nivel = v_nivel_aux 
                      and supr_580.grupo = v_grupo_aux 
                      and (supr_580.sub = v_sub_aux or supr_580.sub = '000') 
                      and (supr_580.item = v_item_aux or supr_580.item = '000000') 
                      and (supr_580.cgc_forn9 = v_cgc9 or supr_580.cgc_forn9 = 0) 
                      and (supr_580.cgc_forn4 = v_cgc4 or supr_580.cgc_forn4 = 0) 
                      and (supr_580.cgc_forn2 = v_cgc2 or supr_580.cgc_forn2 = 0) 
                      and supr_580.valor_preco > 0.00;
                    exception when OTHERS then
                        v_msg_erro := v_msg_erro || 'Erro oitavo sql: ' ||SQLERRM||chr(10);
                    end;
                end if;
            end if;

            v_cont := v_cont + 1;

            begin
                select fator_conv
                into v_fator_conv
                from supr_060
                where item_060_nivel99 = v_nivel_aux
                  and item_060_grupo = v_grupo_aux
                  and item_060_subgrupo = v_sub_aux
                  and item_060_item = v_item_aux
                  and forn_060_forne9 = v_cgc9
                  and forn_060_forne4 = v_cgc4
                  and forn_060_forne2 = v_cgc2;
            exception when no_data_found then
                v_fator_conv := 1;
            end;

            if v_fator_conv > 0
            then
                v_qtde_ped_conv := ROUND(v_qtde_requisitada_aux / v_fator_conv,5);
                v_qtde_req_conv := ROUND(v_qtde_requisitada_aux / v_fator_conv,5);
                v_valor_unit_conv := ROUND(v_valor_unitario_aux * v_fator_conv, 5);
            else
                v_qtde_ped_conv := 0;
                v_qtde_req_conv := 0;
                v_valor_unit_conv := 0;
            end if;
            
            begin
                INSERT INTO SUPR_002
                (nr_solicitacao, tipo_registro,
                usuario, codigo_empresa,
                num_requisicao, seq_item_req,
                valor_unitario, cgc9_forn,
                cgc4_forn, cgc2_forn,
                nivel, grupo,
                subgrupo, item,
                descricao_item, qtde_requisitada,
                cod_tabela, perc_ipi_prod, 
                qtde_pedido, centro_custo, 
                codigo_transacao, codigo_contabil,
                qtde_ped_conv, qtde_req_conv,
                valor_unit_conv, cod_empresa_requis)
                VALUES
                (184, 185,
                v_usuario, v_cod_empresa,
                v_num_requisicao_aux, v_seq_item_req_aux,
                v_valor_unitario_aux, v_cgc9,
                v_cgc4, v_cgc2,
                v_nivel_aux, v_grupo_aux,
                v_sub_aux, v_item_aux,
                v_desc_item_aux, v_qtde_requisitada_aux,
                v_cod_tabela, v_perc_ipi_prod, 
                v_qtde_requisitada_aux, v_ccusto, 
                v_cod_transacao, v_cod_contabil,
                v_qtde_ped_conv, v_qtde_req_conv,
                v_valor_unit_conv, v_cod_empresa_requis);
            exception when OTHERS then
                v_msg_erro := v_msg_erro || 'Erro nono sql: ' ||SQLERRM||chr(10);
            end;

            v_retorno_rel_marca := INTER_FN_RELACIONA_MARCA (v_cod_empresa, v_cod_usuario, v_nivel_aux,
                                    v_grupo_aux, v_sub_aux, v_item_aux,
                                    v_cgc9, v_cgc4, v_cgc2, 184, v_num_requisicao_aux,
                                    v_seq_item_req_aux);

            if v_retorno_rel_marca = true
            then
                v_retorno_rel_marca := false;
                rollback;
            else
                commit;
            end if;        
        END LOOP;
    end if;

    if v_opc_selecao = 2
    then
        v_cont := 0;
        
        FOR opcao2 IN opc2(v_inclui_req, v_cgc9, v_cgc4, v_cgc2, 
                        v_cod_tabela, v_nivel, v_grupo, 
                        v_sub, v_item, v_cod_comprador)
        LOOP

            v_retorno_rel_marca := false;
            v_valor_unitario_aux := 0;
            v_nivel_aux := opcao2.item_req_nivel99;
            v_grupo_aux := opcao2.item_req_grupo;
            v_sub_aux := opcao2.item_req_subgrupo;
            v_item_aux := opcao2.item_req_item;
            v_qtde_requisitada_aux := opcao2.qtde_requisitada;
            v_seq_item_req_aux := opcao2.seq_item_req;
            v_num_requisicao_aux := opcao2.num_requisicao;
            v_desc_item_aux := opcao2.descricao_item;

            if v_cod_tabela <> 0
            then
                select supr_580.valor_preco, supr_580.data_validade 
                into v_valor_unitario_aux, v_data_validade_aux
                from supr_580
                where supr_580.cod_tabela = v_cod_tabela
                  and supr_580.nivel = v_nivel_aux
                  and supr_580.grupo = v_grupo_aux
                  and (supr_580.sub = v_sub_aux or supr_580.sub = '000')
                  and (supr_580.item = v_item_aux or supr_580.item = '000000')
                  and supr_580.cgc_forn9 = v_cgc9
                  and supr_580.cgc_forn4 = v_cgc4
                  and supr_580.cgc_forn2 = v_cgc2
                  and supr_580.valor_preco > 0.00;

                if v_valor_unitario_aux is null
                then
                    v_valor_unitario_aux := 0;
                end if;

                if v_data_validade_aux is null
                then 
                    v_data_validade_aux := sysdate;
                end if;

                if v_data_validade_aux < sysdate
                then 
                    raise_application_error(-20001,'ATEN��O! A tabela de pre�o informada est� com a data de validade expirada.');
                end if;
            end if;

            if v_nivel_aux <> '0'
            then
                begin
                    select 1
                    into v_tmp_int 
                    from supr_580
                    where (supr_580.cod_tabela = v_cod_tabela or v_cod_tabela = 0)
                      and supr_580.nivel = v_nivel_aux 
                      and supr_580.grupo = v_grupo_aux 
                      and (supr_580.sub = v_sub_aux or supr_580.sub = '000') 
                      and (supr_580.item = v_item_aux or supr_580.item = '000000') 
                      and (supr_580.cgc_forn9 = v_cgc9 or supr_580.cgc_forn9 = 0) 
                      and (supr_580.cgc_forn4 = v_cgc4 or supr_580.cgc_forn4 = 0) 
                      and (supr_580.cgc_forn2 = v_cgc2 or supr_580.cgc_forn2 = 0) 
                      and supr_580.valor_preco > 0.00
                      and rownum = 1;
                exception when no_data_found then
                    v_tmp_int := 0;
                end;
                
                if v_tmp_int <> 1
                then
                    v_tmp_int := 0;

                    begin
                        select 1
                        into v_tmp_int 
                        from basi_010
                        where basi_010.nivel_estrutura = v_nivel_aux
                          and basi_010.grupo_estrutura = v_grupo_aux
                          and basi_010.subgru_estrutura = v_sub_aux 
                          and basi_010.item_estrutura = v_item_aux
                          and rownum = 1;
                    exception when no_data_found then
                        v_tmp_int := 0;
                    end;
                    
                    if v_tmp_int <> 1
                    then
                        v_perc_ipi_prod := 0;
                    else
                        select basi_010.classific_fiscal 
                        into v_class_fisc_prod
                        from basi_010
                        where basi_010.nivel_estrutura = v_nivel_aux
                          and basi_010.grupo_estrutura = v_grupo_aux
                          and basi_010.subgru_estrutura = v_sub_aux
                          and basi_010.item_estrutura = v_item_aux;

                        select basi_240.aliquota_ipi 
                        into v_perc_ipi_prod
                        from basi_240 
                        where basi_240.classific_fiscal = v_class_fisc_prod;
                    end if;
                
                else
                    select supr_580.percent_ipi 
                    into v_perc_ipi_prod
                    from supr_580
                    where (supr_580.cod_tabela = v_cod_tabela or v_cod_tabela = 0)
                      and supr_580.nivel = v_nivel_aux 
                      and supr_580.grupo = v_grupo_aux 
                      and (supr_580.sub = v_sub_aux or supr_580.sub = '000') 
                      and (supr_580.item = v_item_aux or supr_580.item = '000000') 
                      and (supr_580.cgc_forn9 = v_cgc9 or supr_580.cgc_forn9 = 0) 
                      and (supr_580.cgc_forn4 = v_cgc4 or supr_580.cgc_forn4 = 0) 
                      and (supr_580.cgc_forn2 = v_cgc2 or supr_580.cgc_forn2 = 0) 
                      and supr_580.valor_preco > 0.00;
                end if;
            end if;

            v_cont := v_cont + 1;

            begin
                select fator_conv
                into v_fator_conv
                from supr_060
                where item_060_nivel99 = v_nivel_aux
                  and item_060_grupo = v_grupo_aux
                  and item_060_subgrupo = v_sub_aux
                  and item_060_item = v_item_aux
                  and forn_060_forne9 = v_cgc9
                  and forn_060_forne4 = v_cgc4
                  and forn_060_forne2 = v_cgc2;
            exception when no_data_found then
                v_fator_conv := 1;
            end;
            
            if v_fator_conv > 0
            then
                v_qtde_ped_conv := ROUND(v_qtde_requisitada_aux / v_fator_conv,5);
                v_qtde_req_conv := ROUND(v_qtde_requisitada_aux / v_fator_conv,5);
                v_valor_unit_conv := ROUND(v_valor_unitario_aux * v_fator_conv, 5);
            else
                v_qtde_ped_conv := 0;
                v_qtde_req_conv := 0;
                v_valor_unit_conv := 0;
            end if;
            
            INSERT INTO SUPR_002
            (nr_solicitacao, tipo_registro,
            usuario, codigo_empresa,
            num_requisicao, seq_item_req,
            valor_unitario, cgc9_forn,
            cgc4_forn, cgc2_forn,
            nivel, grupo,
            subgrupo, item,
            descricao_item, qtde_requisitada,
            cod_tabela, perc_ipi_prod, 
            qtde_pedido, centro_custo, 
            codigo_transacao,codigo_contabil,
            qtde_ped_conv, qtde_req_conv,
            valor_unit_conv)
            VALUES
            (184, 185,
            v_usuario, v_cod_empresa,
            v_num_requisicao_aux, v_seq_item_req_aux,
            v_valor_unitario_aux, v_cgc9,
            v_cgc4, v_cgc2,
            v_nivel_aux, v_grupo_aux,
            v_sub_aux, v_item_aux,
            v_desc_item_aux, v_qtde_requisitada_aux,
            v_cod_tabela, v_perc_ipi_prod, 
            v_qtde_requisitada_aux, v_ccusto, 
            v_cod_transacao, v_cod_contabil,
            v_qtde_ped_conv, v_qtde_req_conv,
            v_valor_unit_conv);

            v_retorno_rel_marca := INTER_FN_RELACIONA_MARCA (v_cod_empresa, v_cod_usuario, v_nivel_aux,
                                    v_grupo_aux, v_sub_aux, v_item_aux,
                                    v_cgc9, v_cgc4, v_cgc2, 184, v_num_requisicao_aux,
                                    v_seq_item_req_aux);

            if v_retorno_rel_marca = true
            then
                v_retorno_rel_marca := false;
                rollback;
            else
                commit;
            end if;
        END LOOP;
    end if;

    if v_opc_selecao = 3
    then

        v_cont := 0;
        FOR opcao3 IN opc3(v_inclui_req, v_cgc9, v_cgc4,
                        v_cgc2, v_cod_tabela, v_cod_comprador)
        LOOP
            
            v_retorno_rel_marca := false;
            v_valor_unitario_aux := 0;
            v_nivel_aux := opcao3.item_req_nivel99;
            v_grupo_aux := opcao3.item_req_grupo;
            v_sub_aux := opcao3.item_req_subgrupo;
            v_item_aux := opcao3.item_req_item;
            v_qtde_requisitada_aux := opcao3.qtde_requisitada;
            v_seq_item_req_aux := opcao3.seq_item_req;
            v_num_requisicao_aux := opcao3.num_requisicao;
            v_desc_item_aux := opcao3.descricao_item;

            if v_cod_tabela <> 0
            then
                select supr_580.valor_preco, supr_580.data_validade 
                into v_valor_unitario_aux, v_data_validade_aux
                from supr_580
                where supr_580.cod_tabela = v_cod_tabela
                  and supr_580.nivel = v_nivel_aux
                  and supr_580.grupo = v_grupo_aux
                  and (supr_580.sub = v_sub_aux or supr_580.sub = '000')
                  and (supr_580.item = v_item_aux or supr_580.item = '000000')
                  and supr_580.cgc_forn9 = v_cgc9
                  and supr_580.cgc_forn4 = v_cgc4
                  and supr_580.cgc_forn2 = v_cgc2
                  and supr_580.valor_preco > 0.00;

                if v_valor_unitario_aux is null or v_valor_unitario_aux = 0
                then
                    v_valor_unitario_aux := 0;
                end if;

                if v_data_validade_aux is null
                then 
                    v_data_validade_aux := sysdate;
                end if;

                if v_data_validade_aux < sysdate
                then 
                    raise_application_error(-20001,'ATEN��O! A tabela de pre�o informada est� com a data de validade expirada.');
                end if;
            end if;

            if v_nivel_aux <> '0'
            then
                begin
                    select 1
                    into v_tmp_int 
                    from supr_580
                    where (supr_580.cod_tabela = v_cod_tabela or v_cod_tabela = 0)
                      and supr_580.nivel = v_nivel_aux 
                      and supr_580.grupo = v_grupo_aux 
                      and (supr_580.sub = v_sub_aux or supr_580.sub = '000') 
                      and (supr_580.item = v_item_aux or supr_580.item = '000000') 
                      and (supr_580.cgc_forn9 = v_cgc9 or supr_580.cgc_forn9 = 0) 
                      and (supr_580.cgc_forn4 = v_cgc4 or supr_580.cgc_forn4 = 0) 
                      and (supr_580.cgc_forn2 = v_cgc2 or supr_580.cgc_forn2 = 0) 
                      and supr_580.valor_preco > 0.00
                      and rownum = 1;
                exception when no_data_found then
                    v_tmp_int := 0;
                end;

                if v_tmp_int <> 1
                then
                    v_tmp_int := 0;

                    begin
                        select 1
                        into v_tmp_int 
                        from basi_010
                        where basi_010.nivel_estrutura = v_nivel_aux
                          and basi_010.grupo_estrutura = v_grupo_aux
                          and basi_010.subgru_estrutura = v_sub_aux 
                          and basi_010.item_estrutura = v_item_aux
                          and rownum = 1;
                    exception when no_data_found then
                        v_tmp_int := 0;
                    end;
                    
                    if v_tmp_int <> 1
                    then
                        v_perc_ipi_prod := 0;
                    else
                        select basi_010.classific_fiscal 
                        into v_class_fisc_prod
                        from basi_010
                        where basi_010.nivel_estrutura = v_nivel_aux
                          and basi_010.grupo_estrutura = v_grupo_aux
                          and basi_010.subgru_estrutura = v_sub_aux 
                          and basi_010.item_estrutura = v_item_aux;
                        
                        select basi_240.aliquota_ipi 
                        into v_perc_ipi_prod
                        from basi_240 
                        where basi_240.classific_fiscal = v_class_fisc_prod;
                    end if;

                else
                    select supr_580.percent_ipi 
                    into v_perc_ipi_prod
                    from supr_580
                    where (supr_580.cod_tabela = v_cod_tabela or v_cod_tabela = 0)
                      and supr_580.nivel = v_nivel_aux 
                      and supr_580.grupo = v_grupo_aux 
                      and (supr_580.sub = v_sub_aux or supr_580.sub = '000') 
                      and (supr_580.item = v_item_aux or supr_580.item = '000000') 
                      and (supr_580.cgc_forn9 = v_cgc9 or supr_580.cgc_forn9 = 0) 
                      and (supr_580.cgc_forn4 = v_cgc4 or supr_580.cgc_forn4 = 0) 
                      and (supr_580.cgc_forn2 = v_cgc2 or supr_580.cgc_forn2 = 0) 
                      and supr_580.valor_preco > 0.00
                      and rownum = 1;
                end if;
            end if;

            v_cont := v_cont + 1;
            begin
                select fator_conv
                into v_fator_conv
                from supr_060
                where item_060_nivel99 = v_nivel_aux
                  and item_060_grupo = v_grupo_aux
                  and item_060_subgrupo = v_sub_aux
                  and item_060_item = v_item_aux
                  and forn_060_forne9 = v_cgc9
                  and forn_060_forne4 = v_cgc4
                  and forn_060_forne2 = v_cgc2;
            exception when no_data_found then
                v_fator_conv := 1;
            end;
            if v_fator_conv > 0
            then
                v_qtde_ped_conv := ROUND(v_qtde_requisitada_aux / v_fator_conv,5);
                v_qtde_req_conv := ROUND(v_qtde_requisitada_aux / v_fator_conv,5);
                v_valor_unit_conv := ROUND(v_valor_unitario_aux * v_fator_conv, 5);
            else
                v_qtde_ped_conv := 0;
                v_qtde_req_conv := 0;
                v_valor_unit_conv := 0;
            end if;

            INSERT INTO SUPR_002
            (nr_solicitacao, tipo_registro,
            usuario, codigo_empresa,
            num_requisicao, seq_item_req,
            valor_unitario, cgc9_forn,
            cgc4_forn, cgc2_forn,
            nivel, grupo,
            subgrupo, item,
            descricao_item, qtde_requisitada,
            cod_tabela, perc_ipi_prod, 
            qtde_pedido, centro_custo, 
            codigo_transacao, codigo_contabil,
            qtde_ped_conv, qtde_req_conv,
            valor_unit_conv)
            VALUES
            (184, 185,
            v_usuario, v_cod_empresa,
            v_num_requisicao_aux, v_seq_item_req_aux,
            v_valor_unitario_aux, v_cgc9,
            v_cgc4, v_cgc2,
            v_nivel_aux, v_grupo_aux,
            v_sub_aux, v_item_aux,
            v_desc_item_aux, v_qtde_requisitada_aux,
            v_cod_tabela, v_perc_ipi_prod, 
            v_qtde_requisitada_aux, v_ccusto, 
            v_cod_transacao, v_cod_contabil,
            v_qtde_ped_conv, v_qtde_req_conv,
            v_valor_unit_conv);

            v_retorno_rel_marca := INTER_FN_RELACIONA_MARCA (v_cod_empresa, v_cod_usuario, v_nivel_aux,
                                    v_grupo_aux, v_sub_aux, v_item_aux,
                                    v_cgc9, v_cgc4, v_cgc2, 184, v_num_requisicao_aux,
                                    v_seq_item_req_aux);

            commit;
        END LOOP;
    end if;
end;
