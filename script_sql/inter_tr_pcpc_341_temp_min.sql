
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_341_TEMP_MIN" 
before insert or update
       of tempo, valor_minuto
on pcpc_341
for each row

begin

   if inserting then
      :new.valor_operacao := :new.tempo * :new.valor_minuto;
   else
     if :old.tempo        <> :new.tempo
     or :old.valor_minuto <> :new.valor_minuto
     then
        :new.valor_operacao := :new.tempo * :new.valor_minuto;
     end if;
   end if;

end inter_tr_pcpc_341_temp_min;

-- ALTER TRIGGER "INTER_TR_PCPC_341_TEMP_MIN" ENABLE
 

/

exec inter_pr_recompile;

