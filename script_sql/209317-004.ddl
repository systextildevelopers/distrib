insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('cobr_f002', 'Liberação/Cancelamento das Solicitações', 1, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'cobr_f002', 'menu_ad34', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao         = 'Liberação/Cancelamento das Solicitações'
where hdoc_036.codigo_programa = 'cobr_f002'
  and hdoc_036.locale          = 'pt_BR';

commit;
