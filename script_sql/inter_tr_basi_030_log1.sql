
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_030_LOG1" 
   after delete or update
       of div_prod
   on basi_030
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_nivel                   varchar2(1);
   v_grupo                   varchar2(5);
   v_subgrupo                varchar2(3);
   v_descricao               varchar2(42);
   v_descricao_ref           varchar2(30);

   v_log                     varchar2(2000);
   v_sequencia_historico     number(9);
   v_tipo_comentario         number(9);

begin

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      v_nivel    := :new.nivel_estrutura;
      v_grupo    := :new.referencia;
      v_descricao:= :new.descr_referencia;
   else
      v_nivel    := :old.nivel_estrutura;
      v_grupo    := :old.referencia;
      v_descricao:= :old.descr_referencia;
   end if;

   v_tipo_comentario := 13;
   v_subgrupo := '000';

   begin
      select nvl(max(basi_095.sequencia),0)
      into v_sequencia_historico
      from basi_095
      where basi_095.tipo_comentario  = v_tipo_comentario -- Historico Divisao de Producao.
        and basi_095.nivel_estrutura  = v_nivel
        and basi_095.grupo_estrutura  = v_grupo
        --and basi_095.subgru_estrutura = v_subgrupo
        and basi_095.data_historico   = trunc(sysdate);
   exception when others then
      v_sequencia_historico := 0;
   end;

   v_sequencia_historico:= v_sequencia_historico + 1;

   v_log := '';

   if updating
   then
      if :new.div_prod <> :old.div_prod
      then
         v_log := v_log || inter_fn_buscar_tag('lb23737#Divisao de Producao',ws_locale_usuario,ws_usuario_systextil) ||
                  ': ' || inter_fn_buscar_tag('lb12222#DE',ws_locale_usuario,ws_usuario_systextil)|| ' ' ||
                  to_char(:old.div_prod) || ' '||
                  inter_fn_buscar_tag('lb06475#PARA',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                  to_char(:new.div_prod) || chr(10);
      end if;


      if Length(v_log) > 0
      then
         v_log:= inter_fn_buscar_tag_composta('lb34366#O produto {0} sofreu alteracoes:',v_nivel    || '.'   ||
                                                                                         v_grupo    || '.'   ||
                                                                                         v_subgrupo || ' - ' ||
                                                                                         v_descricao,'','','','','','','','','', ws_locale_usuario,ws_usuario_systextil) || chr(10) || v_log;

         begin
            INSERT INTO basi_095
              (nivel_estrutura,        grupo_estrutura,
               subgru_estrutura,       data_historico,
               tipo_comentario,        sequencia,
               descricao,              codigo_usuario,
               hora_historico)
            VALUES
              (v_nivel,                v_grupo,
               v_subgrupo,             trunc(sysdate),
               v_tipo_comentario,      v_sequencia_historico,
               v_log,                  ws_usuario_systextil,
               sysdate);
            exception when OTHERS then
               raise_application_error (-20000, inter_fn_buscar_tag('lb34370#Nao inseriu registro de historico.',ws_locale_usuario,ws_usuario_systextil));
         end;
      end if;
   end if;

   if deleting
   then
      v_log:= inter_fn_buscar_tag_composta('lb34371#O produto {0} foi excluido.',v_nivel    || '.'   ||
                                                                                 v_grupo    || '.'   ||
                                                                                 v_subgrupo || ' - ' ||
                                                                                 v_descricao,'','','','','','','','','', ws_locale_usuario,ws_usuario_systextil) || chr(10);

      v_log := v_log || inter_fn_buscar_tag('lb23737#Divisao de Producao',ws_locale_usuario, ws_usuario_systextil) ||
                           ': ' || to_char(:old.div_prod)  || chr(10);
      begin
         INSERT INTO basi_095
           (nivel_estrutura,        grupo_estrutura,
            subgru_estrutura,       data_historico,
            tipo_comentario,        sequencia,
            descricao,              codigo_usuario,
            hora_historico)
         VALUES
           (v_nivel,                v_grupo,
            v_subgrupo,             trunc(sysdate),
            v_tipo_comentario,      v_sequencia_historico,
            v_log,                  ws_usuario_systextil,
            sysdate);
         exception when OTHERS then
            raise_application_error (-20000, inter_fn_buscar_tag('lb34370#Nao inseriu registro de historico.',ws_locale_usuario,ws_usuario_systextil));
      end;
   end if;

end inter_tr_basi_030_log1;

-- ALTER TRIGGER "INTER_TR_BASI_030_LOG1" ENABLE
 

/

exec inter_pr_recompile;

