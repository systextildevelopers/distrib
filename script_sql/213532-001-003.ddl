UPDATE pedi_080
set red_icms_base_pis_cof = 0
where pedi_080.codigo_transacao in (select codigo_transacao
                                    from estq_005
                                    where estq_005.entrada_saida = 'S');

UPDATE pedi_080
set red_icms_base_pis_cof = 1
where pedi_080.codigo_transacao in (select codigo_transacao
                                    from estq_005
                                    where estq_005.entrada_saida = 'S'
									and estq_005.tipo_transacao <> 'D');

commit;
