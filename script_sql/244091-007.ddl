-- Create table
create table PCPB_223
(
  codigo_ocorrencia NUMBER(4) default 0 not null,
  descricao         VARCHAR2(60) default ''
);
comment on table PCPB_223 is 'Cadastro de Ocorrencias para Beneficiamento';
comment on column PCPB_223.codigo_ocorrencia  is 'Codigo de ocorrencia.';
comment on column PCPB_223.descricao  is 'Descricao da ocorrencia.';
alter table PCPB_223 add constraint PK_PCPB_223 primary key (CODIGO_OCORRENCIA)
;
