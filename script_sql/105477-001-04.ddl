alter table basi_200 add (unid_med_trib varchar2(10) default ' ');

comment on column basi_200.unid_med_trib is 'Unidade de medida tributaria criado pela RFB';

exec inter_pr_recompile;

/
