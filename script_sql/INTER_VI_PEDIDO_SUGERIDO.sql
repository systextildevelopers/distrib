------------------------------------------------------------------
-- View que lista pedidos Sugeridos para Atiantamento crec_fa02 --
------------------------------------------------------------------
CREATE OR REPLACE VIEW  "INTER_VI_PEDIDO_SUGERIDO" AS 
  (
    select PEDIDO_VENDA 
            ,CGC9 
            ,CGC4 
            ,CGC2 
            ,CODIGO_EMPRESA 
            ,COD_REP_CLIENTE 
            ,VALOR_SUGERIDO 
            ,COD_FORMA_PAGTO 
            ,COND_PGT_CLIENTE 
            ,codigo_servico_sph
        from (select distinct pedi_100.PEDIDO_VENDA 
            ,pedi_100.CODIGO_EMPRESA 
            ,pedi_100.DATA_EMIS_VENDA as data_emissao 
            ,pedi_100.DATA_ENTR_VENDA as DATA_ENTREGA 
            ,pedi_100.CLI_PED_CGC_CLI9 AS CGC9 
            ,pedi_100.CLI_PED_CGC_CLI4 AS CGC4 
            ,pedi_100.CLI_PED_CGC_CLI2 AS CGC2 
            ,pedi_100.COD_REP_CLIENTE 
            ,pedi_070.COND_PGT_CLIENTE
            ,loja_010.codigo_servico_sph 
            ,(select round(sum(round((VALOR_UNITARIO - (VALOR_UNITARIO * PERCENTUAL_DESC / 100.00)), 2) * QTDE_SUGERIDA), 2) from pedi_110 where pedido_venda = pedi_100.pedido_venda) as valor_sugerido 
            ,case 
                when pedi_117.sit_boleto != 2 and (select count(1) as data_venc from FATU_070 fatu, FATU_504 
                    where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA 
                    and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9 
                    and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4 
                    and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2 
                    and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO 
                    and fatu.PEDIDO_VENDA = pedi_100.pedido_venda 
                    and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO 
                    and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA 
                    and fatu.DATA_PRORROGACAO >= to_date(sysdate) 
                    and fatu.SALDO_DUPLICATA > 0 ) = 1 then 'AGUARDANDO PAGAMENTO' 
                when pedi_117.sit_boleto != 2 and (select count(1) as data_venc from FATU_070 fatu, FATU_504 
                    where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA 
                    and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9 
                    and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4 
                    and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2 
                    and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO 
                    and fatu.PEDIDO_VENDA = pedi_100.pedido_venda 
                    and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO 
                    and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA 
                    and fatu.DATA_PRORROGACAO < to_date(sysdate) 
                    and fatu.SALDO_DUPLICATA > 0) = 1 then 'ANTECIPAÇÃO EM ATRASO' 
                when pedi_117.sit_boleto != 2 and (select count(1) as data_venc from FATU_070 fatu, FATU_504 
                    where fatu.CODIGO_EMPRESA = pedi_100.CODIGO_EMPRESA 
                    and fatu.CLI_DUP_CGC_CLI9 = pedi_121.CGC9 
                    and fatu.CLI_DUP_CGC_CLI4 = pedi_121.CGC4 
                    and fatu.CLI_DUP_CGC_CLI2 = pedi_121.CGC2 
                    and fatu.NUM_DUPLICATA = pedi_121.NR_TITULO 
                    and fatu.PEDIDO_VENDA = pedi_100.pedido_venda 
                    and fatu.TIPO_TITULO = FATU_504.COD_TP_TITULO 
                    and fatu.CODIGO_EMPRESA = FATU_504.CODIGO_EMPRESA 
                    and fatu.SALDO_DUPLICATA = 0) = 1 then 'ANTECIPAÇÃO RECEBIDA' 
                when pedi_117.sit_boleto = 2 then 'ANTECIPAÇÃO CANCELADA' 
                when pedi_117.sit_boleto in (0,1) and pedi_100.pedido_venda = pedi_118.pedido_venda   then 'PROCESSO DE ANTECIPAÇÃO REALIZADO' 
                when ((select nvl(sum(QTDE_SUGERIDA),0) from pedi_110 where pedido_venda = pedi_100.pedido_venda) > 0) and pedi_118.pedido_venda is null then 'PEDIDO SUGERIDO' 
                else 'PEDIDO DIGITADO' 
            end as situacao_antec 
            , pedi_010.cod_cidade 
            , pedi_100.cod_forma_pagto 
            from  pedi_100 
                ,pedi_110 
                ,pedi_070 
                ,pedi_010 
                ,pedi_075 
                ,pedi_118 
                ,pedi_117 
                ,pedi_121 
                ,LOJA_010
                where pedi_100.COND_PGTO_VENDA =  pedi_070.COND_PGT_CLIENTE
                and pedi_100.cod_forma_pagto = loja_010.forma_pgto (+) 
                and pedi_100.pedido_venda = pedi_110.pedido_venda 
                and pedi_100.CLI_PED_CGC_CLI9 = pedi_010.CGC_9 (+) 
                and pedi_100.CLI_PED_CGC_CLI4 = pedi_010.CGC_4 (+) 
                and pedi_100.CLI_PED_CGC_CLI2 = pedi_010.CGC_2 (+) 
                and pedi_100.cond_pgto_venda = pedi_075.condicao_pagto 
                and pedi_100.CLI_PED_CGC_CLI9 = pedi_118.CGC9 (+) 
                and pedi_100.CLI_PED_CGC_CLI4 = pedi_118.CGC4 (+) 
                and pedi_100.CLI_PED_CGC_CLI2 = pedi_118.CGC2 (+) 
                and pedi_100.pedido_venda = pedi_118.pedido_venda  (+) 
                and pedi_118.CGC9 = pedi_117.CGC9 (+) 
                and pedi_118.CGC4 = pedi_117.CGC4 (+) 
                and pedi_118.CGC2 = pedi_117.CGC2 (+) 
                and pedi_118.SEQUENCIA = pedi_117.SEQUENCIA (+) 
                and pedi_118.CGC9 = pedi_121.CGC9 (+) 
                and pedi_118.CGC4 = pedi_121.CGC4 (+) 
                and pedi_118.CGC2 = pedi_121.CGC2 (+) 
                and pedi_118.SEQUENCIA = pedi_121.seq_deposito (+) 
                and nvl(pedi_117.sit_boleto,0) != 5 
                and (pedi_075.vencimento = 0 or (loja_010.codigo_servico_sph > 0))
                and pedi_100.cod_cancelamento = 0 
                and pedi_110.situacao_fatu_it <> 1 
                and pedi_110.cod_cancelamento  = 0 
                and pedi_100.SITUACAO_VENDA != 10 
                and (pedi_110.QTDE_SUGERIDA > 0 
                or (((select count(1) from pedi_118 pedi118 
                    where pedi118.pedido_venda = pedi_100.pedido_venda) > 0) 
                    and pedi_100.situacao_venda <> 9)) 
                    ) 
        where SITUACAO_ANTEC is not null 
        and     SITUACAO_ANTEC = 'PEDIDO SUGERIDO' 
)
/
