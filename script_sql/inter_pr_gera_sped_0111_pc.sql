create or replace procedure inter_pr_gera_sped_0111_pc(p_cod_matriz    IN NUMBER,
                                                       p_dat_inicial   IN DATE,
                                                       p_dat_final     IN DATE,
                                                       p_des_erro      OUT varchar2) is

CURSOR u_pc_notas (p_cod_matriz      NUMBER,
                  p_dat_inicial     IN DATE,
                  p_dat_final       IN DATE) IS
   select  sum(val_total) val_total,                                                 cvf_pis,
           perc_pis,
           tip_trubutacao
   from (
   (select sum(sped_pc_c170.val_contabil) as val_total,       sped_pc_c170.cvf_pis,
          sped_pc_c170.perc_pis,
          decode(substr(sped_pc_c170.num_cfop,1,1),'7','EXP',
          decode(sped_pc_c170.num_cfop,'6501','EXP',
          decode(sped_pc_c170.num_cfop,'6502','EXP',
          decode(sped_pc_c170.num_cfop,'5501','EXP',
          decode(sped_pc_c170.num_cfop,'5502','EXP',
          decode(sped_pc_c170.val_pis,0.00,'MINT','MI')))))) tip_trubutacao
   from sped_pc_c170, pedi_080
   where sped_pc_c170.cod_matriz           =  p_cod_matriz
     and sped_pc_c170.dat_emissao          between p_dat_inicial and p_dat_final
     and sped_pc_c170.tip_entrada_saida    = 'S'
     and sped_pc_c170.cod_situacao_nota   in (0,6)
     and pedi_080.natur_operacao           = sped_pc_c170.cod_nat_oper
     and pedi_080.estado_natoper           = sped_pc_c170.sig_estado_oper
     and pedi_080.faturamento              = 1
     and pedi_080.ind_nf_ciap              = 0
   group by sped_pc_c170.cvf_pis,          sped_pc_c170.perc_pis,
            sped_pc_c170.num_cfop,
            sped_pc_c170.perc_pis,
          decode(substr(sped_pc_c170.num_cfop,1,1),'7','EXP',
          decode(sped_pc_c170.num_cfop,'6501','EXP',
          decode(sped_pc_c170.num_cfop,'6502','EXP',
          decode(sped_pc_c170.num_cfop,'5501','EXP',
          decode(sped_pc_c170.num_cfop,'5502','EXP',
          decode(sped_pc_c170.val_pis,0.00,'MINT','MI')))))))
   union all -- 3 - REDUZ A RECEITA
   (select sum(decode(obrf_700.ind_oper, 3,-obrf_700.vl_oper, obrf_700.vl_oper)) as val_total,      obrf_700.cst_pis cvf_pis,
           obrf_700.aliq_pis perc_pis,
           decode(obrf_700.ind_orig_cred,1,'EXP',decode(obrf_700.vl_pis,0.00,'MINT','MI')) tip_trubutacao
    from obrf_700
    where obrf_700.cod_empresa         = p_cod_matriz
      and obrf_700.mes_apur            = to_char(p_dat_inicial,'MM')
      and obrf_700.ano_apur            = to_char(p_dat_inicial,'YYYY')
      and obrf_700.cst_pis             < 50
      and obrf_700.receita_operacional = 1 /*1 - � receita operacional 2 - N�o */
    group by obrf_700.cst_pis, obrf_700.aliq_pis,
           decode(obrf_700.ind_orig_cred,1,'EXP',decode(obrf_700.vl_pis,0.00,'MINT','MI')))
    )
    group by cvf_pis,
           perc_pis,
           tip_trubutacao;


begin

   begin
      delete from sped_pc_0111
      where sped_pc_0111.cod_matriz = p_cod_matriz;
   exception
       WHEN OTHERS THEN
           p_des_erro := 'N�o removeu dados na tabela sped_pc_0111' || Chr(10) || SQLERRM;
   end;

   FOR pc_c170 IN u_pc_notas (p_cod_matriz, p_dat_inicial, p_dat_final) LOOP

      begin
         insert into sped_pc_0111
           (sped_pc_0111.cod_matriz, /*001*/

            sped_pc_0111.cst_pis_cofins, /*003*/
            sped_pc_0111.perc_pis_cofins, /*004*/
            sped_pc_0111.valor_receita, /*005*/
            sped_pc_0111.tip_tributacao) /*006*/
         values
           (p_cod_matriz, /*001*/

            pc_c170.cvf_pis, /*003*/
            pc_c170.perc_pis, /*004*/
            pc_c170.val_total, /*005*/
            pc_c170.tip_trubutacao); /*006*/
      exception
         WHEN OTHERS THEN
             p_des_erro := 'N�o inseriu dados na tabela sped_pc_0111' || Chr(10) || SQLERRM;
      end;
      commit;
   END LOOP;

end inter_pr_gera_sped_0111_pc;
/
exec inter_pr_recompile;
