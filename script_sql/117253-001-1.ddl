ALTER TABLE pedi_010 ADD DDD_CELULAR NUMBER(4) ;

update pedi_010 a
set a.ddd_celular = (select b.ddd
                     from basi_160 b 
                     where ( a.cod_cidade = b.cod_cidade));
commit;
