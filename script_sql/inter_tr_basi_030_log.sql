
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_030_LOG" 
after insert or delete or update
on basi_030
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid); 

 if inserting
 then
    begin

        insert into basi_030_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           NIVEL_ESTRUTURA_OLD,   /*8*/
           NIVEL_ESTRUTURA_NEW,   /*9*/
           REFERENCIA_OLD,   /*10*/
           REFERENCIA_NEW,   /*11*/
           DESCR_REFERENCIA_OLD,   /*12*/
           DESCR_REFERENCIA_NEW,   /*13*/
           UNIDADE_MEDIDA_OLD,   /*14*/
           UNIDADE_MEDIDA_NEW,   /*15*/
           PERC_2_QUALIDADE_OLD,   /*16*/
           PERC_2_QUALIDADE_NEW,   /*17*/
           COLECAO_OLD,   /*18*/
           COLECAO_NEW,   /*19*/
           CONTA_ESTOQUE_OLD,   /*20*/
           CONTA_ESTOQUE_NEW,   /*21*/
           LINHA_PRODUTO_OLD,   /*22*/
           LINHA_PRODUTO_NEW,   /*23*/
           ARTIGO_OLD,   /*24*/
           ARTIGO_NEW,   /*25*/
           ARTIGO_COTAS_OLD,   /*26*/
           ARTIGO_COTAS_NEW,   /*27*/
           ARTIGO_APROVEIT_OLD,   /*28*/
           ARTIGO_APROVEIT_NEW,   /*29*/
           PERCENT_DESCONTO_OLD,   /*30*/
           PERCENT_DESCONTO_NEW,   /*31*/
           TIPO_PRODUTO_OLD,   /*32*/
           TIPO_PRODUTO_NEW,   /*33*/
           VARIACAO_CORES_OLD,   /*34*/
           VARIACAO_CORES_NEW,   /*35*/
           TAMANHO_FILME_OLD,   /*36*/
           TAMANHO_FILME_NEW,   /*37*/
           RAPORTAGEM_OLD,   /*38*/
           RAPORTAGEM_NEW,   /*39*/
           PERCENT_PERDA_1_OLD,   /*40*/
           PERCENT_PERDA_1_NEW,   /*41*/
           PERCENT_PERDA_2_OLD,   /*42*/
           PERCENT_PERDA_2_NEW,   /*43*/
           PERCENT_PERDA_3_OLD,   /*44*/
           PERCENT_PERDA_3_NEW,   /*45*/
           APROVEIT_MEDIO_OLD,   /*46*/
           APROVEIT_MEDIO_NEW,   /*47*/
           METROS_LINEARES_OLD,   /*48*/
           METROS_LINEARES_NEW,   /*49*/
           INDICE_REAJUSTE_OLD,   /*50*/
           INDICE_REAJUSTE_NEW,   /*51*/
           ITEM_BAIXA_ESTQ_OLD,   /*52*/
           ITEM_BAIXA_ESTQ_NEW,   /*53*/
           DEMANDA_OLD,   /*54*/
           DEMANDA_NEW,   /*55*/
           PONTOS_POR_CM_OLD,   /*56*/
           PONTOS_POR_CM_NEW,   /*57*/
           OBSERVACAO_OLD,   /*58*/
           OBSERVACAO_NEW,   /*59*/
           AGULHAS_FALHADAS_OLD,   /*60*/
           AGULHAS_FALHADAS_NEW,   /*61*/
           FORMACAO_AGULHAS_OLD,   /*62*/
           FORMACAO_AGULHAS_NEW,   /*63*/
           CLASSIFIC_FISCAL_OLD,   /*64*/
           CLASSIFIC_FISCAL_NEW,   /*65*/
           COMPRADO_FABRIC_OLD,   /*66*/
           COMPRADO_FABRIC_NEW,   /*67*/
           ABERTO_TUBULAR_OLD,   /*68*/
           ABERTO_TUBULAR_NEW,   /*69*/
           NUMERO_ROTEIRO_OLD,   /*70*/
           NUMERO_ROTEIRO_NEW,   /*71*/
           REF_ORIGINAL_OLD,   /*72*/
           REF_ORIGINAL_NEW,   /*73*/
           NUMERO_MOLDE_OLD,   /*74*/
           NUMERO_MOLDE_NEW,   /*75*/
           COR_DE_ESTOQUE_OLD,   /*76*/
           COR_DE_ESTOQUE_NEW,   /*77*/
           DIV_PROD_OLD,   /*78*/
           DIV_PROD_NEW,   /*79*/
           COR_DE_PRODUCAO_OLD,   /*80*/
           COR_DE_PRODUCAO_NEW,   /*81*/
           CODIGO_CONTABIL_OLD,   /*82*/
           CODIGO_CONTABIL_NEW,   /*83*/
           CGC_CLIENTE_2_OLD,   /*84*/
           CGC_CLIENTE_2_NEW,   /*85*/
           CGC_CLIENTE_4_OLD,   /*86*/
           CGC_CLIENTE_4_NEW,   /*87*/
           CGC_CLIENTE_9_OLD,   /*88*/
           CGC_CLIENTE_9_NEW,   /*89*/
           COMPOSICAO_01_OLD,   /*90*/
           COMPOSICAO_01_NEW,   /*91*/
           COMPOSICAO_02_OLD,   /*92*/
           COMPOSICAO_02_NEW,   /*93*/
           COMPOSICAO_03_OLD,   /*94*/
           COMPOSICAO_03_NEW,   /*95*/
           COMPOSICAO_04_OLD,   /*96*/
           COMPOSICAO_04_NEW,   /*97*/
           COMPOSICAO_05_OLD,   /*98*/
           COMPOSICAO_05_NEW,   /*99*/
           PERC_COMPOSICAO1_OLD,   /*100*/
           PERC_COMPOSICAO1_NEW,   /*101*/
           PERC_COMPOSICAO2_OLD,   /*102*/
           PERC_COMPOSICAO2_NEW,   /*103*/
           PERC_COMPOSICAO3_OLD,   /*104*/
           PERC_COMPOSICAO3_NEW,   /*105*/
           PERC_COMPOSICAO4_OLD,   /*106*/
           PERC_COMPOSICAO4_NEW,   /*107*/
           PERC_COMPOSICAO5_OLD,   /*108*/
           PERC_COMPOSICAO5_NEW,   /*109*/
           ESTAGIO_ALTERA_PROGRAMADO_OLD,   /*110*/
           ESTAGIO_ALTERA_PROGRAMADO_NEW,   /*111*/
           RISCO_PADRAO_OLD,   /*112*/
           RISCO_PADRAO_NEW,   /*113*/
           RESPONSAVEL_OLD,   /*114*/
           RESPONSAVEL_NEW,   /*115*/
           MULTIPLICADOR_OLD,   /*116*/
           MULTIPLICADOR_NEW,   /*117*/
           NUM_SOL_DESENV_OLD,   /*118*/
           NUM_SOL_DESENV_NEW,   /*119*/
           SORTIMENTO_DIVERSOS_OLD,   /*120*/
           SORTIMENTO_DIVERSOS_NEW,   /*121*/
           GRUPO_AGRUPADOR_OLD,   /*122*/
           GRUPO_AGRUPADOR_NEW,   /*123*/
           SUB_AGRUPADOR_OLD,   /*124*/
           SUB_AGRUPADOR_NEW,   /*125*/
           COD_EMBALAGEM_OLD,   /*126*/
           COD_EMBALAGEM_NEW,   /*127*/
           CONF_ALTERA_PROG_OLD,   /*128*/
           CONF_ALTERA_PROG_NEW,   /*129*/
           OBSERVACAO2_OLD,   /*130*/
           OBSERVACAO2_NEW,   /*131*/
           OBSERVACAO3_OLD,   /*132*/
           OBSERVACAO3_NEW,   /*133*/
           PUBLICO_ALVO_OLD,   /*134*/
           PUBLICO_ALVO_NEW,   /*135*/
           COR_DE_SORTIDO_OLD,   /*136*/
           COR_DE_SORTIDO_NEW,   /*137*/
           TIPO_CODIGO_EAN_OLD,   /*138*/
           TIPO_CODIGO_EAN_NEW,   /*139*/
           NUM_PROGRAMA_OLD,   /*140*/
           NUM_PROGRAMA_NEW,   /*141*/
           REFERENCIA_DESENV_OLD,   /*142*/
           REFERENCIA_DESENV_NEW,   /*143*/
           TAB_RENDIMENTO_OLD,   /*144*/
           TAB_RENDIMENTO_NEW,   /*145*/
           LARGURA_CILINDRO_OLD,   /*146*/
           LARGURA_CILINDRO_NEW,   /*147*/
           LARGURA_ESTAMPA_OLD,   /*148*/
           LARGURA_ESTAMPA_NEW,   /*149*/
           DESCRICAO_TAG1_OLD,   /*150*/
           DESCRICAO_TAG1_NEW,   /*151*/
           DESCRICAO_TAG2_OLD,   /*152*/
           DESCRICAO_TAG2_NEW,   /*153*/
           DESCR_COMPLEMENTAR_OLD,   /*154*/
           DESCR_COMPLEMENTAR_NEW,   /*155*/
           PASTA_BANHO_OLD,   /*156*/
           PASTA_BANHO_NEW,   /*157*/
           PERIMETRO_CORTE_OLD,   /*158*/
           PERIMETRO_CORTE_NEW,   /*159*/
           PRODUTO_PROTOTIPO_OLD,   /*160*/
           PRODUTO_PROTOTIPO_NEW,   /*161*/
           TEMPO_LASER_OLD,   /*162*/
           TEMPO_LASER_NEW,   /*163*/
           COLECAO_CLIENTE_OLD,   /*164*/
           COLECAO_CLIENTE_NEW,   /*165*/
           DESCR_INGLES_OLD,   /*166*/
           DESCR_INGLES_NEW,   /*167*/
           DESCR_ESPANHOL_OLD,   /*168*/
           DESCR_ESPANHOL_NEW,   /*169*/
           TAMANHO_AMOSTRA_OLD,   /*170*/
           TAMANHO_AMOSTRA_NEW,   /*171*/
           PESO_AMOSTRA_OLD,   /*172*/
           PESO_AMOSTRA_NEW,   /*173*/
           COMP_SERRA_FITA_OLD,   /*174*/
           COMP_SERRA_FITA_NEW,   /*175*/
           PARTE_COMPOSICAO_01_OLD,   /*176*/
           PARTE_COMPOSICAO_01_NEW,   /*177*/
           PARTE_COMPOSICAO_02_OLD,   /*178*/
           PARTE_COMPOSICAO_02_NEW,   /*179*/
           PARTE_COMPOSICAO_03_OLD,   /*180*/
           PARTE_COMPOSICAO_03_NEW,   /*181*/
           PARTE_COMPOSICAO_04_OLD,   /*182*/
           PARTE_COMPOSICAO_04_NEW,   /*183*/
           PARTE_COMPOSICAO_05_OLD,   /*184*/
           PARTE_COMPOSICAO_05_NEW,   /*185*/
           COMPOSICAO_01_INGLES_OLD,   /*186*/
           COMPOSICAO_01_INGLES_NEW,   /*187*/
           COMPOSICAO_02_INGLES_OLD,   /*188*/
           COMPOSICAO_02_INGLES_NEW,   /*189*/
           COMPOSICAO_03_INGLES_OLD,   /*190*/
           COMPOSICAO_03_INGLES_NEW,   /*191*/
           COMPOSICAO_04_INGLES_OLD,   /*192*/
           COMPOSICAO_04_INGLES_NEW,   /*193*/
           COMPOSICAO_05_INGLES_OLD,   /*194*/
           COMPOSICAO_05_INGLES_NEW,   /*195*/
           TIPO_ESTAMPA_OLD,   /*196*/
           TIPO_ESTAMPA_NEW,   /*197*/
           IMP_INF_EMPRESA_TAG_OLD,   /*198*/
           IMP_INF_EMPRESA_TAG_NEW,   /*199*/
           ESTAMPA_IMPRESSA_OLD,   /*200*/
           ESTAMPA_IMPRESSA_NEW,   /*201*/
           COD_TIPO_AVIAMENTO_OLD,   /*202*/
           COD_TIPO_AVIAMENTO_NEW,   /*203*/
           COD_ATRIBUTO_01_OLD,   /*204*/
           COD_ATRIBUTO_01_NEW,   /*205*/
           COD_ATRIBUTO_02_OLD,   /*206*/
           COD_ATRIBUTO_02_NEW,   /*207*/
           COD_ATRIBUTO_03_OLD,   /*208*/
           COD_ATRIBUTO_03_NEW,   /*209*/
           COD_ATRIBUTO_04_OLD,   /*210*/
           COD_ATRIBUTO_04_NEW,   /*211*/
           COD_ATRIBUTO_05_OLD,   /*212*/
           COD_ATRIBUTO_05_NEW,   /*213*/
           COD_ATRIBUTO_06_OLD,   /*214*/
           COD_ATRIBUTO_06_NEW,   /*215*/
           COD_ATRIBUTO_07_OLD,   /*216*/
           COD_ATRIBUTO_07_NEW,   /*217*/
           COD_ATRIBUTO_08_OLD,   /*218*/
           COD_ATRIBUTO_08_NEW,   /*219*/
           COD_ATRIBUTO_09_OLD,   /*220*/
           COD_ATRIBUTO_09_NEW,   /*221*/
           COD_ATRIBUTO_10_OLD,   /*222*/
           COD_ATRIBUTO_10_NEW,   /*223*/
           REFERENCIA_AGRUPADOR_OLD,   /*224*/
           REFERENCIA_AGRUPADOR_NEW,   /*225*/
           MAQUINA_PILOTO_OLD,   /*226*/
           MAQUINA_PILOTO_NEW,   /*227*/
           FINURA_PILOTO_OLD,   /*228*/
           FINURA_PILOTO_NEW,   /*229*/
           GRADUACAO_PILOTO_OLD,   /*230*/
           GRADUACAO_PILOTO_NEW,   /*231*/
           PROGRAMA_PILOTO_OLD,   /*232*/
           PROGRAMA_PILOTO_NEW,   /*233*/
           PROGRAMADOR_PILOTO_OLD,   /*234*/
           PROGRAMADOR_PILOTO_NEW,   /*235*/
           PESO_BRUTO_PILOTO_OLD,   /*236*/
           PESO_BRUTO_PILOTO_NEW,   /*237*/
           PESO_ACABAMENTO_PILOTO_OLD,   /*238*/
           PESO_ACABAMENTO_PILOTO_NEW,   /*239*/
           TECELAGEM_PILOTO_OLD,   /*240*/
           TECELAGEM_PILOTO_NEW,   /*241*/
           FACCAO_PILOTO_OLD,   /*242*/
           FACCAO_PILOTO_NEW,   /*243*/
           PERS_BORDADO_PILOTO_OLD,   /*244*/
           PERS_BORDADO_PILOTO_NEW,   /*245*/
           PERS_LAVACAO_PILOTO_OLD,   /*246*/
           PERS_LAVACAO_PILOTO_NEW,   /*247*/
           PERS_SERIGRAFIA_PILOTO_OLD,   /*248*/
           PERS_SERIGRAFIA_PILOTO_NEW,   /*249*/
           GRUPO_TECIDO_CORTE_OLD,   /*250*/
           GRUPO_TECIDO_CORTE_NEW,   /*251*/
           PERSONALIZACAO_PILOTO_OLD,   /*252*/
           PERSONALIZACAO_PILOTO_NEW,   /*253*/
           DATA_MOV_CILINDRO_OLD,   /*254*/
           DATA_MOV_CILINDRO_NEW,   /*255*/
           SERIE_TAMANHO_OLD,   /*256*/
           SERIE_TAMANHO_NEW,   /*257*/
           NUMERO_PONTOS_LASER_OLD,   /*258*/
           NUMERO_PONTOS_LASER_NEW,   /*259*/
           PERCENTUAL_AMACIANTE_OLD,   /*260*/
           PERCENTUAL_AMACIANTE_NEW,   /*261*/
           SIMBOLO_1_OLD,   /*262*/
           SIMBOLO_1_NEW,   /*263*/
           SIMBOLO_2_OLD,   /*264*/
           SIMBOLO_2_NEW,   /*265*/
           SIMBOLO_3_OLD,   /*266*/
           SIMBOLO_3_NEW,   /*267*/
           SIMBOLO_4_OLD,   /*268*/
           SIMBOLO_4_NEW,   /*269*/
           SIMBOLO_5_OLD,   /*270*/
           SIMBOLO_5_NEW,   /*271*/
           LOTE_ESTOQUE_OLD,   /*272*/
           LOTE_ESTOQUE_NEW,   /*273*/
           DIAS_MATURACAO_OLD,   /*274*/
           DIAS_MATURACAO_NEW,   /*275*/
           COD_BAR_POR_CLI_OLD,   /*276*/
           COD_BAR_POR_CLI_NEW,   /*277*/
           FAMILIA_ATRIBUTO_OLD,   /*278*/
           FAMILIA_ATRIBUTO_NEW,   /*279*/
           DOC_REFERENCIAL_OLD,   /*280*/
           DOC_REFERENCIAL_NEW,   /*281*/
           LIGAMENTO_OLD,   /*282*/
           LIGAMENTO_NEW,   /*283*/
           PERC_COMPOSICAO6_OLD,   /*284*/
           PERC_COMPOSICAO6_NEW,   /*285*/
           PERC_COMPOSICAO7_OLD,   /*286*/
           PERC_COMPOSICAO7_NEW,   /*287*/
           PERC_COMPOSICAO8_OLD,   /*288*/
           PERC_COMPOSICAO8_NEW,   /*289*/
           PERC_COMPOSICAO9_OLD,   /*290*/
           PERC_COMPOSICAO9_NEW,   /*291*/
           PERC_COMPOSICAO10_OLD,   /*292*/
           PERC_COMPOSICAO10_NEW,   /*293*/
           COMPOSICAO_06_OLD,   /*294*/
           COMPOSICAO_06_NEW,   /*295*/
           COMPOSICAO_07_OLD,   /*296*/
           COMPOSICAO_07_NEW,   /*297*/
           COMPOSICAO_08_OLD,   /*298*/
           COMPOSICAO_08_NEW,   /*299*/
           COMPOSICAO_09_OLD,   /*300*/
           COMPOSICAO_09_NEW,   /*301*/
           COMPOSICAO_10_OLD,   /*302*/
           COMPOSICAO_10_NEW,   /*303*/
           SIMBOLO_6_OLD,   /*304*/
           SIMBOLO_6_NEW,   /*305*/
           SIMBOLO_7_OLD,   /*306*/
           SIMBOLO_7_NEW,   /*307*/
           SIMBOLO_8_OLD,   /*308*/
           SIMBOLO_8_NEW,   /*309*/
           SIMBOLO_9_OLD,   /*310*/
           SIMBOLO_9_NEW,   /*311*/
           SIMBOLO_10_OLD,   /*312*/
           SIMBOLO_10_NEW,   /*313*/
           PARTE_COMPOSICAO_06_OLD,   /*314*/
           PARTE_COMPOSICAO_06_NEW,   /*315*/
           PARTE_COMPOSICAO_07_OLD,   /*316*/
           PARTE_COMPOSICAO_07_NEW,   /*317*/
           PARTE_COMPOSICAO_08_OLD,   /*318*/
           PARTE_COMPOSICAO_08_NEW,   /*319*/
           PARTE_COMPOSICAO_09_OLD,   /*320*/
           PARTE_COMPOSICAO_09_NEW,   /*321*/
           PARTE_COMPOSICAO_10_OLD,   /*322*/
           PARTE_COMPOSICAO_10_NEW,   /*323*/
           PRODUTO_VENDA_OLD,   /*324*/
           PRODUTO_VENDA_NEW,   /*325*/
           NVLPRECOMONTAGEM_OLD,   /*326*/
           NVLPRECOMONTAGEM_NEW,   /*327*/
           OPCAO_VENDA_COR_OLD,   /*328*/
           OPCAO_VENDA_COR_NEW,   /*329*/
           ULTIMA_COR_SORTIDA_OLD,   /*330*/
           ULTIMA_COR_SORTIDA_NEW,   /*331*/
           TIPO_TAG_EAN_OLD,   /*332*/
           TIPO_TAG_EAN_NEW,   /*333*/
           MODULAR_OLD,   /*334*/
           MODULAR_NEW    /*335*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           '',/*8*/
           :new.NIVEL_ESTRUTURA, /*9*/
           '',/*10*/
           :new.REFERENCIA, /*11*/
           '',/*12*/
           :new.DESCR_REFERENCIA, /*13*/
           '',/*14*/
           :new.UNIDADE_MEDIDA, /*15*/
           0,/*16*/
           :new.PERC_2_QUALIDADE, /*17*/
           0,/*18*/
           :new.COLECAO, /*19*/
           0,/*20*/
           :new.CONTA_ESTOQUE, /*21*/
           0,/*22*/
           :new.LINHA_PRODUTO, /*23*/
           0,/*24*/
           :new.ARTIGO, /*25*/
           0,/*26*/
           :new.ARTIGO_COTAS, /*27*/
           0,/*28*/
           :new.ARTIGO_APROVEIT, /*29*/
           0,/*30*/
           :new.PERCENT_DESCONTO, /*31*/
           0,/*32*/
           :new.TIPO_PRODUTO, /*33*/
           0,/*34*/
           :new.VARIACAO_CORES, /*35*/
           0,/*36*/
           :new.TAMANHO_FILME, /*37*/
           0,/*38*/
           :new.RAPORTAGEM, /*39*/
           0,/*40*/
           :new.PERCENT_PERDA_1, /*41*/
           0,/*42*/
           :new.PERCENT_PERDA_2, /*43*/
           0,/*44*/
           :new.PERCENT_PERDA_3, /*45*/
           0,/*46*/
           :new.APROVEIT_MEDIO, /*47*/
           0,/*48*/
           :new.METROS_LINEARES, /*49*/
           0,/*50*/
           :new.INDICE_REAJUSTE, /*51*/
           '',/*52*/
           :new.ITEM_BAIXA_ESTQ, /*53*/
           0,/*54*/
           :new.DEMANDA, /*55*/
           0,/*56*/
           :new.PONTOS_POR_CM, /*57*/
           '',/*58*/
           :new.OBSERVACAO, /*59*/
           0,/*60*/
           :new.AGULHAS_FALHADAS, /*61*/
           0,/*62*/
           :new.FORMACAO_AGULHAS, /*63*/
           '',/*64*/
           :new.CLASSIFIC_FISCAL, /*65*/
           0,/*66*/
           :new.COMPRADO_FABRIC, /*67*/
           0,/*68*/
           :new.ABERTO_TUBULAR, /*69*/
           0,/*70*/
           :new.NUMERO_ROTEIRO, /*71*/
           '',/*72*/
           :new.REF_ORIGINAL, /*73*/
           '',/*74*/
           '',/*75*/
           '',/*76*/
           :new.COR_DE_ESTOQUE, /*77*/
           0,/*78*/
           :new.DIV_PROD, /*79*/
           '',/*80*/
           :new.COR_DE_PRODUCAO, /*81*/
           0,/*82*/
           :new.CODIGO_CONTABIL, /*83*/
           0,/*84*/
           :new.CGC_CLIENTE_2, /*85*/
           0,/*86*/
           :new.CGC_CLIENTE_4, /*87*/
           0,/*88*/
           :new.CGC_CLIENTE_9, /*89*/
           '',/*90*/
           :new.COMPOSICAO_01, /*91*/
           '',/*92*/
           :new.COMPOSICAO_02, /*93*/
           '',/*94*/
           :new.COMPOSICAO_03, /*95*/
           '',/*96*/
           :new.COMPOSICAO_04, /*97*/
           '',/*98*/
           :new.COMPOSICAO_05, /*99*/
           0,/*100*/
           :new.PERC_COMPOSICAO1, /*101*/
           0,/*102*/
           :new.PERC_COMPOSICAO2, /*103*/
           0,/*104*/
           :new.PERC_COMPOSICAO3, /*105*/
           0,/*106*/
           :new.PERC_COMPOSICAO4, /*107*/
           0,/*108*/
           :new.PERC_COMPOSICAO5, /*109*/
           0,/*110*/
           :new.ESTAGIO_ALTERA_PROGRAMADO, /*111*/
           0,/*112*/
           :new.RISCO_PADRAO, /*113*/
           '',/*114*/
           :new.RESPONSAVEL, /*115*/
           0,/*116*/
           :new.MULTIPLICADOR, /*117*/
           '',/*118*/
           :new.NUM_SOL_DESENV, /*119*/
           '',/*120*/
           :new.SORTIMENTO_DIVERSOS, /*121*/
           '',/*122*/
           :new.GRUPO_AGRUPADOR, /*123*/
           '',/*124*/
           :new.SUB_AGRUPADOR, /*125*/
           0,/*126*/
           :new.COD_EMBALAGEM, /*127*/
           0,/*128*/
           :new.CONF_ALTERA_PROG, /*129*/
           '',/*130*/
           :new.OBSERVACAO2, /*131*/
           '',/*132*/
           :new.OBSERVACAO3, /*133*/
           0,/*134*/
           :new.PUBLICO_ALVO, /*135*/
           '',/*136*/
           :new.COR_DE_SORTIDO, /*137*/
           0,/*138*/
           :new.TIPO_CODIGO_EAN, /*139*/
           '',/*140*/
           :new.NUM_PROGRAMA, /*141*/
           '',/*142*/
           :new.REFERENCIA_DESENV, /*143*/
           0,/*144*/
           :new.TAB_RENDIMENTO, /*145*/
           0,/*146*/
           :new.LARGURA_CILINDRO, /*147*/
           0,/*148*/
           :new.LARGURA_ESTAMPA, /*149*/
           '',/*150*/
           :new.DESCRICAO_TAG1, /*151*/
           '',/*152*/
           :new.DESCRICAO_TAG2, /*153*/
           '',/*154*/
           :new.DESCR_COMPLEMENTAR, /*155*/
           0,/*156*/
           :new.PASTA_BANHO, /*157*/
           0,/*158*/
           :new.PERIMETRO_CORTE, /*159*/
           '',/*160*/
           :new.PRODUTO_PROTOTIPO, /*161*/
           0,/*162*/
           :new.TEMPO_LASER, /*163*/
           '',/*164*/
           :new.COLECAO_CLIENTE, /*165*/
           '',/*166*/
           :new.DESCR_INGLES, /*167*/
           '',/*168*/
           :new.DESCR_ESPANHOL, /*169*/
           '',/*170*/
           :new.TAMANHO_AMOSTRA, /*171*/
           0,/*172*/
           :new.PESO_AMOSTRA, /*173*/
           '',/*174*/
           :new.COMP_SERRA_FITA, /*175*/
           '',/*176*/
           :new.PARTE_COMPOSICAO_01, /*177*/
           '',/*178*/
           :new.PARTE_COMPOSICAO_02, /*179*/
           '',/*180*/
           :new.PARTE_COMPOSICAO_03, /*181*/
           '',/*182*/
           :new.PARTE_COMPOSICAO_04, /*183*/
           '',/*184*/
           :new.PARTE_COMPOSICAO_05, /*185*/
           '',/*186*/
           :new.COMPOSICAO_01_INGLES, /*187*/
           '',/*188*/
           :new.COMPOSICAO_02_INGLES, /*189*/
           '',/*190*/
           :new.COMPOSICAO_03_INGLES, /*191*/
           '',/*192*/
           :new.COMPOSICAO_04_INGLES, /*193*/
           '',/*194*/
           :new.COMPOSICAO_05_INGLES, /*195*/
           0,/*196*/
           :new.TIPO_ESTAMPA, /*197*/
           '',/*198*/
           :new.IMP_INF_EMPRESA_TAG, /*199*/
           0,/*200*/
           :new.ESTAMPA_IMPRESSA, /*201*/
           '',/*202*/
           :new.COD_TIPO_AVIAMENTO, /*203*/
           0,/*204*/
           :new.COD_ATRIBUTO_01, /*205*/
           0,/*206*/
           :new.COD_ATRIBUTO_02, /*207*/
           0,/*208*/
           :new.COD_ATRIBUTO_03, /*209*/
           0,/*210*/
           :new.COD_ATRIBUTO_04, /*211*/
           0,/*212*/
           :new.COD_ATRIBUTO_05, /*213*/
           0,/*214*/
           :new.COD_ATRIBUTO_06, /*215*/
           0,/*216*/
           :new.COD_ATRIBUTO_07, /*217*/
           0,/*218*/
           :new.COD_ATRIBUTO_08, /*219*/
           0,/*220*/
           :new.COD_ATRIBUTO_09, /*221*/
           0,/*222*/
           :new.COD_ATRIBUTO_10, /*223*/
           '',/*224*/
           :new.REFERENCIA_AGRUPADOR, /*225*/
           '',/*226*/
           :new.MAQUINA_PILOTO, /*227*/
           '',/*228*/
           :new.FINURA_PILOTO, /*229*/
           '',/*230*/
           :new.GRADUACAO_PILOTO, /*231*/
           '',/*232*/
           :new.PROGRAMA_PILOTO, /*233*/
           '',/*234*/
           :new.PROGRAMADOR_PILOTO, /*235*/
           0,/*236*/
           :new.PESO_BRUTO_PILOTO, /*237*/
           0,/*238*/
           :new.PESO_ACABAMENTO_PILOTO, /*239*/
           '',/*240*/
           :new.TECELAGEM_PILOTO, /*241*/
           '',/*242*/
           :new.FACCAO_PILOTO, /*243*/
           0,/*244*/
           :new.PERS_BORDADO_PILOTO, /*245*/
           0,/*246*/
           :new.PERS_LAVACAO_PILOTO, /*247*/
           0,/*248*/
           :new.PERS_SERIGRAFIA_PILOTO, /*249*/
           0,/*250*/
           :new.GRUPO_TECIDO_CORTE, /*251*/
           '',/*252*/
           :new.PERSONALIZACAO_PILOTO, /*253*/
           null,/*254*/
           :new.DATA_MOV_CILINDRO, /*255*/
           0,/*256*/
           :new.SERIE_TAMANHO, /*257*/
           0,/*258*/
           :new.NUMERO_PONTOS_LASER, /*259*/
           0,/*260*/
           :new.PERCENTUAL_AMACIANTE, /*261*/
           '',/*262*/
           :new.SIMBOLO_1, /*263*/
           '',/*264*/
           :new.SIMBOLO_2, /*265*/
           '',/*266*/
           :new.SIMBOLO_3, /*267*/
           '',/*268*/
           :new.SIMBOLO_4, /*269*/
           '',/*270*/
           :new.SIMBOLO_5, /*271*/
           0,/*272*/
           :new.LOTE_ESTOQUE, /*273*/
           0,/*274*/
           :new.DIAS_MATURACAO, /*275*/
           '',/*276*/
           :new.COD_BAR_POR_CLI, /*277*/
           '',/*278*/
           :new.FAMILIA_ATRIBUTO, /*279*/
           '',/*280*/
           :new.DOC_REFERENCIAL, /*281*/
           '',/*282*/
           :new.LIGAMENTO, /*283*/
           0,/*284*/
           :new.PERC_COMPOSICAO6, /*285*/
           0,/*286*/
           :new.PERC_COMPOSICAO7, /*287*/
           0,/*288*/
           :new.PERC_COMPOSICAO8, /*289*/
           0,/*290*/
           :new.PERC_COMPOSICAO9, /*291*/
           0,/*292*/
           :new.PERC_COMPOSICAO10, /*293*/
           '',/*294*/
           :new.COMPOSICAO_06, /*295*/
           '',/*296*/
           :new.COMPOSICAO_07, /*297*/
           '',/*298*/
           :new.COMPOSICAO_08, /*299*/
           '',/*300*/
           :new.COMPOSICAO_09, /*301*/
           '',/*302*/
           :new.COMPOSICAO_10, /*303*/
           '',/*304*/
           :new.SIMBOLO_6, /*305*/
           '',/*306*/
           :new.SIMBOLO_7, /*307*/
           '',/*308*/
           :new.SIMBOLO_8, /*309*/
           '',/*310*/
           :new.SIMBOLO_9, /*311*/
           '',/*312*/
           :new.SIMBOLO_10, /*313*/
           '',/*314*/
           :new.PARTE_COMPOSICAO_06, /*315*/
           '',/*316*/
           :new.PARTE_COMPOSICAO_07, /*317*/
           '',/*318*/
           :new.PARTE_COMPOSICAO_08, /*319*/
           '',/*320*/
           :new.PARTE_COMPOSICAO_09, /*321*/
           '',/*322*/
           :new.PARTE_COMPOSICAO_10, /*323*/
           '',/*324*/
           :new.PRODUTO_VENDA, /*325*/
           0,/*326*/
           :new.NVLPRECOMONTAGEM, /*327*/
           0,/*328*/
           :new.OPCAO_VENDA_COR, /*329*/
           '',/*330*/
           :new.ULTIMA_COR_SORTIDA, /*331*/
           '',/*332*/
           :new.TIPO_TAG_EAN, /*333*/
           0,/*334*/
           :new.MODULAR /*335*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into basi_030_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           NIVEL_ESTRUTURA_OLD, /*8*/
           NIVEL_ESTRUTURA_NEW, /*9*/
           REFERENCIA_OLD, /*10*/
           REFERENCIA_NEW, /*11*/
           DESCR_REFERENCIA_OLD, /*12*/
           DESCR_REFERENCIA_NEW, /*13*/
           UNIDADE_MEDIDA_OLD, /*14*/
           UNIDADE_MEDIDA_NEW, /*15*/
           PERC_2_QUALIDADE_OLD, /*16*/
           PERC_2_QUALIDADE_NEW, /*17*/
           COLECAO_OLD, /*18*/
           COLECAO_NEW, /*19*/
           CONTA_ESTOQUE_OLD, /*20*/
           CONTA_ESTOQUE_NEW, /*21*/
           LINHA_PRODUTO_OLD, /*22*/
           LINHA_PRODUTO_NEW, /*23*/
           ARTIGO_OLD, /*24*/
           ARTIGO_NEW, /*25*/
           ARTIGO_COTAS_OLD, /*26*/
           ARTIGO_COTAS_NEW, /*27*/
           ARTIGO_APROVEIT_OLD, /*28*/
           ARTIGO_APROVEIT_NEW, /*29*/
           PERCENT_DESCONTO_OLD, /*30*/
           PERCENT_DESCONTO_NEW, /*31*/
           TIPO_PRODUTO_OLD, /*32*/
           TIPO_PRODUTO_NEW, /*33*/
           VARIACAO_CORES_OLD, /*34*/
           VARIACAO_CORES_NEW, /*35*/
           TAMANHO_FILME_OLD, /*36*/
           TAMANHO_FILME_NEW, /*37*/
           RAPORTAGEM_OLD, /*38*/
           RAPORTAGEM_NEW, /*39*/
           PERCENT_PERDA_1_OLD, /*40*/
           PERCENT_PERDA_1_NEW, /*41*/
           PERCENT_PERDA_2_OLD, /*42*/
           PERCENT_PERDA_2_NEW, /*43*/
           PERCENT_PERDA_3_OLD, /*44*/
           PERCENT_PERDA_3_NEW, /*45*/
           APROVEIT_MEDIO_OLD, /*46*/
           APROVEIT_MEDIO_NEW, /*47*/
           METROS_LINEARES_OLD, /*48*/
           METROS_LINEARES_NEW, /*49*/
           INDICE_REAJUSTE_OLD, /*50*/
           INDICE_REAJUSTE_NEW, /*51*/
           ITEM_BAIXA_ESTQ_OLD, /*52*/
           ITEM_BAIXA_ESTQ_NEW, /*53*/
           DEMANDA_OLD, /*54*/
           DEMANDA_NEW, /*55*/
           PONTOS_POR_CM_OLD, /*56*/
           PONTOS_POR_CM_NEW, /*57*/
           OBSERVACAO_OLD, /*58*/
           OBSERVACAO_NEW, /*59*/
           AGULHAS_FALHADAS_OLD, /*60*/
           AGULHAS_FALHADAS_NEW, /*61*/
           FORMACAO_AGULHAS_OLD, /*62*/
           FORMACAO_AGULHAS_NEW, /*63*/
           CLASSIFIC_FISCAL_OLD, /*64*/
           CLASSIFIC_FISCAL_NEW, /*65*/
           COMPRADO_FABRIC_OLD, /*66*/
           COMPRADO_FABRIC_NEW, /*67*/
           ABERTO_TUBULAR_OLD, /*68*/
           ABERTO_TUBULAR_NEW, /*69*/
           NUMERO_ROTEIRO_OLD, /*70*/
           NUMERO_ROTEIRO_NEW, /*71*/
           REF_ORIGINAL_OLD, /*72*/
           REF_ORIGINAL_NEW, /*73*/
           NUMERO_MOLDE_OLD, /*74*/
           NUMERO_MOLDE_NEW, /*75*/
           COR_DE_ESTOQUE_OLD, /*76*/
           COR_DE_ESTOQUE_NEW, /*77*/
           DIV_PROD_OLD, /*78*/
           DIV_PROD_NEW, /*79*/
           COR_DE_PRODUCAO_OLD, /*80*/
           COR_DE_PRODUCAO_NEW, /*81*/
           CODIGO_CONTABIL_OLD, /*82*/
           CODIGO_CONTABIL_NEW, /*83*/
           CGC_CLIENTE_2_OLD, /*84*/
           CGC_CLIENTE_2_NEW, /*85*/
           CGC_CLIENTE_4_OLD, /*86*/
           CGC_CLIENTE_4_NEW, /*87*/
           CGC_CLIENTE_9_OLD, /*88*/
           CGC_CLIENTE_9_NEW, /*89*/
           COMPOSICAO_01_OLD, /*90*/
           COMPOSICAO_01_NEW, /*91*/
           COMPOSICAO_02_OLD, /*92*/
           COMPOSICAO_02_NEW, /*93*/
           COMPOSICAO_03_OLD, /*94*/
           COMPOSICAO_03_NEW, /*95*/
           COMPOSICAO_04_OLD, /*96*/
           COMPOSICAO_04_NEW, /*97*/
           COMPOSICAO_05_OLD, /*98*/
           COMPOSICAO_05_NEW, /*99*/
           PERC_COMPOSICAO1_OLD, /*100*/
           PERC_COMPOSICAO1_NEW, /*101*/
           PERC_COMPOSICAO2_OLD, /*102*/
           PERC_COMPOSICAO2_NEW, /*103*/
           PERC_COMPOSICAO3_OLD, /*104*/
           PERC_COMPOSICAO3_NEW, /*105*/
           PERC_COMPOSICAO4_OLD, /*106*/
           PERC_COMPOSICAO4_NEW, /*107*/
           PERC_COMPOSICAO5_OLD, /*108*/
           PERC_COMPOSICAO5_NEW, /*109*/
           ESTAGIO_ALTERA_PROGRAMADO_OLD, /*110*/
           ESTAGIO_ALTERA_PROGRAMADO_NEW, /*111*/
           RISCO_PADRAO_OLD, /*112*/
           RISCO_PADRAO_NEW, /*113*/
           RESPONSAVEL_OLD, /*114*/
           RESPONSAVEL_NEW, /*115*/
           MULTIPLICADOR_OLD, /*116*/
           MULTIPLICADOR_NEW, /*117*/
           NUM_SOL_DESENV_OLD, /*118*/
           NUM_SOL_DESENV_NEW, /*119*/
           SORTIMENTO_DIVERSOS_OLD, /*120*/
           SORTIMENTO_DIVERSOS_NEW, /*121*/
           GRUPO_AGRUPADOR_OLD, /*122*/
           GRUPO_AGRUPADOR_NEW, /*123*/
           SUB_AGRUPADOR_OLD, /*124*/
           SUB_AGRUPADOR_NEW, /*125*/
           COD_EMBALAGEM_OLD, /*126*/
           COD_EMBALAGEM_NEW, /*127*/
           CONF_ALTERA_PROG_OLD, /*128*/
           CONF_ALTERA_PROG_NEW, /*129*/
           OBSERVACAO2_OLD, /*130*/
           OBSERVACAO2_NEW, /*131*/
           OBSERVACAO3_OLD, /*132*/
           OBSERVACAO3_NEW, /*133*/
           PUBLICO_ALVO_OLD, /*134*/
           PUBLICO_ALVO_NEW, /*135*/
           COR_DE_SORTIDO_OLD, /*136*/
           COR_DE_SORTIDO_NEW, /*137*/
           TIPO_CODIGO_EAN_OLD, /*138*/
           TIPO_CODIGO_EAN_NEW, /*139*/
           NUM_PROGRAMA_OLD, /*140*/
           NUM_PROGRAMA_NEW, /*141*/
           REFERENCIA_DESENV_OLD, /*142*/
           REFERENCIA_DESENV_NEW, /*143*/
           TAB_RENDIMENTO_OLD, /*144*/
           TAB_RENDIMENTO_NEW, /*145*/
           LARGURA_CILINDRO_OLD, /*146*/
           LARGURA_CILINDRO_NEW, /*147*/
           LARGURA_ESTAMPA_OLD, /*148*/
           LARGURA_ESTAMPA_NEW, /*149*/
           DESCRICAO_TAG1_OLD, /*150*/
           DESCRICAO_TAG1_NEW, /*151*/
           DESCRICAO_TAG2_OLD, /*152*/
           DESCRICAO_TAG2_NEW, /*153*/
           DESCR_COMPLEMENTAR_OLD, /*154*/
           DESCR_COMPLEMENTAR_NEW, /*155*/
           PASTA_BANHO_OLD, /*156*/
           PASTA_BANHO_NEW, /*157*/
           PERIMETRO_CORTE_OLD, /*158*/
           PERIMETRO_CORTE_NEW, /*159*/
           PRODUTO_PROTOTIPO_OLD, /*160*/
           PRODUTO_PROTOTIPO_NEW, /*161*/
           TEMPO_LASER_OLD, /*162*/
           TEMPO_LASER_NEW, /*163*/
           COLECAO_CLIENTE_OLD, /*164*/
           COLECAO_CLIENTE_NEW, /*165*/
           DESCR_INGLES_OLD, /*166*/
           DESCR_INGLES_NEW, /*167*/
           DESCR_ESPANHOL_OLD, /*168*/
           DESCR_ESPANHOL_NEW, /*169*/
           TAMANHO_AMOSTRA_OLD, /*170*/
           TAMANHO_AMOSTRA_NEW, /*171*/
           PESO_AMOSTRA_OLD, /*172*/
           PESO_AMOSTRA_NEW, /*173*/
           COMP_SERRA_FITA_OLD, /*174*/
           COMP_SERRA_FITA_NEW, /*175*/
           PARTE_COMPOSICAO_01_OLD, /*176*/
           PARTE_COMPOSICAO_01_NEW, /*177*/
           PARTE_COMPOSICAO_02_OLD, /*178*/
           PARTE_COMPOSICAO_02_NEW, /*179*/
           PARTE_COMPOSICAO_03_OLD, /*180*/
           PARTE_COMPOSICAO_03_NEW, /*181*/
           PARTE_COMPOSICAO_04_OLD, /*182*/
           PARTE_COMPOSICAO_04_NEW, /*183*/
           PARTE_COMPOSICAO_05_OLD, /*184*/
           PARTE_COMPOSICAO_05_NEW, /*185*/
           COMPOSICAO_01_INGLES_OLD, /*186*/
           COMPOSICAO_01_INGLES_NEW, /*187*/
           COMPOSICAO_02_INGLES_OLD, /*188*/
           COMPOSICAO_02_INGLES_NEW, /*189*/
           COMPOSICAO_03_INGLES_OLD, /*190*/
           COMPOSICAO_03_INGLES_NEW, /*191*/
           COMPOSICAO_04_INGLES_OLD, /*192*/
           COMPOSICAO_04_INGLES_NEW, /*193*/
           COMPOSICAO_05_INGLES_OLD, /*194*/
           COMPOSICAO_05_INGLES_NEW, /*195*/
           TIPO_ESTAMPA_OLD, /*196*/
           TIPO_ESTAMPA_NEW, /*197*/
           IMP_INF_EMPRESA_TAG_OLD, /*198*/
           IMP_INF_EMPRESA_TAG_NEW, /*199*/
           ESTAMPA_IMPRESSA_OLD, /*200*/
           ESTAMPA_IMPRESSA_NEW, /*201*/
           COD_TIPO_AVIAMENTO_OLD, /*202*/
           COD_TIPO_AVIAMENTO_NEW, /*203*/
           COD_ATRIBUTO_01_OLD, /*204*/
           COD_ATRIBUTO_01_NEW, /*205*/
           COD_ATRIBUTO_02_OLD, /*206*/
           COD_ATRIBUTO_02_NEW, /*207*/
           COD_ATRIBUTO_03_OLD, /*208*/
           COD_ATRIBUTO_03_NEW, /*209*/
           COD_ATRIBUTO_04_OLD, /*210*/
           COD_ATRIBUTO_04_NEW, /*211*/
           COD_ATRIBUTO_05_OLD, /*212*/
           COD_ATRIBUTO_05_NEW, /*213*/
           COD_ATRIBUTO_06_OLD, /*214*/
           COD_ATRIBUTO_06_NEW, /*215*/
           COD_ATRIBUTO_07_OLD, /*216*/
           COD_ATRIBUTO_07_NEW, /*217*/
           COD_ATRIBUTO_08_OLD, /*218*/
           COD_ATRIBUTO_08_NEW, /*219*/
           COD_ATRIBUTO_09_OLD, /*220*/
           COD_ATRIBUTO_09_NEW, /*221*/
           COD_ATRIBUTO_10_OLD, /*222*/
           COD_ATRIBUTO_10_NEW, /*223*/
           REFERENCIA_AGRUPADOR_OLD, /*224*/
           REFERENCIA_AGRUPADOR_NEW, /*225*/
           MAQUINA_PILOTO_OLD, /*226*/
           MAQUINA_PILOTO_NEW, /*227*/
           FINURA_PILOTO_OLD, /*228*/
           FINURA_PILOTO_NEW, /*229*/
           GRADUACAO_PILOTO_OLD, /*230*/
           GRADUACAO_PILOTO_NEW, /*231*/
           PROGRAMA_PILOTO_OLD, /*232*/
           PROGRAMA_PILOTO_NEW, /*233*/
           PROGRAMADOR_PILOTO_OLD, /*234*/
           PROGRAMADOR_PILOTO_NEW, /*235*/
           PESO_BRUTO_PILOTO_OLD, /*236*/
           PESO_BRUTO_PILOTO_NEW, /*237*/
           PESO_ACABAMENTO_PILOTO_OLD, /*238*/
           PESO_ACABAMENTO_PILOTO_NEW, /*239*/
           TECELAGEM_PILOTO_OLD, /*240*/
           TECELAGEM_PILOTO_NEW, /*241*/
           FACCAO_PILOTO_OLD, /*242*/
           FACCAO_PILOTO_NEW, /*243*/
           PERS_BORDADO_PILOTO_OLD, /*244*/
           PERS_BORDADO_PILOTO_NEW, /*245*/
           PERS_LAVACAO_PILOTO_OLD, /*246*/
           PERS_LAVACAO_PILOTO_NEW, /*247*/
           PERS_SERIGRAFIA_PILOTO_OLD, /*248*/
           PERS_SERIGRAFIA_PILOTO_NEW, /*249*/
           GRUPO_TECIDO_CORTE_OLD, /*250*/
           GRUPO_TECIDO_CORTE_NEW, /*251*/
           PERSONALIZACAO_PILOTO_OLD, /*252*/
           PERSONALIZACAO_PILOTO_NEW, /*253*/
           DATA_MOV_CILINDRO_OLD, /*254*/
           DATA_MOV_CILINDRO_NEW, /*255*/
           SERIE_TAMANHO_OLD, /*256*/
           SERIE_TAMANHO_NEW, /*257*/
           NUMERO_PONTOS_LASER_OLD, /*258*/
           NUMERO_PONTOS_LASER_NEW, /*259*/
           PERCENTUAL_AMACIANTE_OLD, /*260*/
           PERCENTUAL_AMACIANTE_NEW, /*261*/
           SIMBOLO_1_OLD, /*262*/
           SIMBOLO_1_NEW, /*263*/
           SIMBOLO_2_OLD, /*264*/
           SIMBOLO_2_NEW, /*265*/
           SIMBOLO_3_OLD, /*266*/
           SIMBOLO_3_NEW, /*267*/
           SIMBOLO_4_OLD, /*268*/
           SIMBOLO_4_NEW, /*269*/
           SIMBOLO_5_OLD, /*270*/
           SIMBOLO_5_NEW, /*271*/
           LOTE_ESTOQUE_OLD, /*272*/
           LOTE_ESTOQUE_NEW, /*273*/
           DIAS_MATURACAO_OLD, /*274*/
           DIAS_MATURACAO_NEW, /*275*/
           COD_BAR_POR_CLI_OLD, /*276*/
           COD_BAR_POR_CLI_NEW, /*277*/
           FAMILIA_ATRIBUTO_OLD, /*278*/
           FAMILIA_ATRIBUTO_NEW, /*279*/
           DOC_REFERENCIAL_OLD, /*280*/
           DOC_REFERENCIAL_NEW, /*281*/
           LIGAMENTO_OLD, /*282*/
           LIGAMENTO_NEW, /*283*/
           PERC_COMPOSICAO6_OLD, /*284*/
           PERC_COMPOSICAO6_NEW, /*285*/
           PERC_COMPOSICAO7_OLD, /*286*/
           PERC_COMPOSICAO7_NEW, /*287*/
           PERC_COMPOSICAO8_OLD, /*288*/
           PERC_COMPOSICAO8_NEW, /*289*/
           PERC_COMPOSICAO9_OLD, /*290*/
           PERC_COMPOSICAO9_NEW, /*291*/
           PERC_COMPOSICAO10_OLD, /*292*/
           PERC_COMPOSICAO10_NEW, /*293*/
           COMPOSICAO_06_OLD, /*294*/
           COMPOSICAO_06_NEW, /*295*/
           COMPOSICAO_07_OLD, /*296*/
           COMPOSICAO_07_NEW, /*297*/
           COMPOSICAO_08_OLD, /*298*/
           COMPOSICAO_08_NEW, /*299*/
           COMPOSICAO_09_OLD, /*300*/
           COMPOSICAO_09_NEW, /*301*/
           COMPOSICAO_10_OLD, /*302*/
           COMPOSICAO_10_NEW, /*303*/
           SIMBOLO_6_OLD, /*304*/
           SIMBOLO_6_NEW, /*305*/
           SIMBOLO_7_OLD, /*306*/
           SIMBOLO_7_NEW, /*307*/
           SIMBOLO_8_OLD, /*308*/
           SIMBOLO_8_NEW, /*309*/
           SIMBOLO_9_OLD, /*310*/
           SIMBOLO_9_NEW, /*311*/
           SIMBOLO_10_OLD, /*312*/
           SIMBOLO_10_NEW, /*313*/
           PARTE_COMPOSICAO_06_OLD, /*314*/
           PARTE_COMPOSICAO_06_NEW, /*315*/
           PARTE_COMPOSICAO_07_OLD, /*316*/
           PARTE_COMPOSICAO_07_NEW, /*317*/
           PARTE_COMPOSICAO_08_OLD, /*318*/
           PARTE_COMPOSICAO_08_NEW, /*319*/
           PARTE_COMPOSICAO_09_OLD, /*320*/
           PARTE_COMPOSICAO_09_NEW, /*321*/
           PARTE_COMPOSICAO_10_OLD, /*322*/
           PARTE_COMPOSICAO_10_NEW, /*323*/
           PRODUTO_VENDA_OLD, /*324*/
           PRODUTO_VENDA_NEW, /*325*/
           NVLPRECOMONTAGEM_OLD, /*326*/
           NVLPRECOMONTAGEM_NEW, /*327*/
           OPCAO_VENDA_COR_OLD, /*328*/
           OPCAO_VENDA_COR_NEW, /*329*/
           ULTIMA_COR_SORTIDA_OLD, /*330*/
           ULTIMA_COR_SORTIDA_NEW, /*331*/
           TIPO_TAG_EAN_OLD, /*332*/
           TIPO_TAG_EAN_NEW, /*333*/
           MODULAR_OLD, /*334*/
           MODULAR_NEW  /*335*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.NIVEL_ESTRUTURA,  /*8*/
           :new.NIVEL_ESTRUTURA, /*9*/
           :old.REFERENCIA,  /*10*/
           :new.REFERENCIA, /*11*/
           :old.DESCR_REFERENCIA,  /*12*/
           :new.DESCR_REFERENCIA, /*13*/
           :old.UNIDADE_MEDIDA,  /*14*/
           :new.UNIDADE_MEDIDA, /*15*/
           :old.PERC_2_QUALIDADE,  /*16*/
           :new.PERC_2_QUALIDADE, /*17*/
           :old.COLECAO,  /*18*/
           :new.COLECAO, /*19*/
           :old.CONTA_ESTOQUE,  /*20*/
           :new.CONTA_ESTOQUE, /*21*/
           :old.LINHA_PRODUTO,  /*22*/
           :new.LINHA_PRODUTO, /*23*/
           :old.ARTIGO,  /*24*/
           :new.ARTIGO, /*25*/
           :old.ARTIGO_COTAS,  /*26*/
           :new.ARTIGO_COTAS, /*27*/
           :old.ARTIGO_APROVEIT,  /*28*/
           :new.ARTIGO_APROVEIT, /*29*/
           :old.PERCENT_DESCONTO,  /*30*/
           :new.PERCENT_DESCONTO, /*31*/
           :old.TIPO_PRODUTO,  /*32*/
           :new.TIPO_PRODUTO, /*33*/
           :old.VARIACAO_CORES,  /*34*/
           :new.VARIACAO_CORES, /*35*/
           :old.TAMANHO_FILME,  /*36*/
           :new.TAMANHO_FILME, /*37*/
           :old.RAPORTAGEM,  /*38*/
           :new.RAPORTAGEM, /*39*/
           :old.PERCENT_PERDA_1,  /*40*/
           :new.PERCENT_PERDA_1, /*41*/
           :old.PERCENT_PERDA_2,  /*42*/
           :new.PERCENT_PERDA_2, /*43*/
           :old.PERCENT_PERDA_3,  /*44*/
           :new.PERCENT_PERDA_3, /*45*/
           :old.APROVEIT_MEDIO,  /*46*/
           :new.APROVEIT_MEDIO, /*47*/
           :old.METROS_LINEARES,  /*48*/
           :new.METROS_LINEARES, /*49*/
           :old.INDICE_REAJUSTE,  /*50*/
           :new.INDICE_REAJUSTE, /*51*/
           :old.ITEM_BAIXA_ESTQ,  /*52*/
           :new.ITEM_BAIXA_ESTQ, /*53*/
           :old.DEMANDA,  /*54*/
           :new.DEMANDA, /*55*/
           :old.PONTOS_POR_CM,  /*56*/
           :new.PONTOS_POR_CM, /*57*/
           :old.OBSERVACAO,  /*58*/
           :new.OBSERVACAO, /*59*/
           :old.AGULHAS_FALHADAS,  /*60*/
           :new.AGULHAS_FALHADAS, /*61*/
           :old.FORMACAO_AGULHAS,  /*62*/
           :new.FORMACAO_AGULHAS, /*63*/
           :old.CLASSIFIC_FISCAL,  /*64*/
           :new.CLASSIFIC_FISCAL, /*65*/
           :old.COMPRADO_FABRIC,  /*66*/
           :new.COMPRADO_FABRIC, /*67*/
           :old.ABERTO_TUBULAR,  /*68*/
           :new.ABERTO_TUBULAR, /*69*/
           :old.NUMERO_ROTEIRO,  /*70*/
           :new.NUMERO_ROTEIRO, /*71*/
           :old.REF_ORIGINAL,  /*72*/
           :new.REF_ORIGINAL, /*73*/
           :old.NUMERO_MOLDE,  /*74*/
           :old.NUMERO_MOLDE,  /*75*/
           :old.COR_DE_ESTOQUE,  /*76*/
           :new.COR_DE_ESTOQUE, /*77*/
           :old.DIV_PROD,  /*78*/
           :new.DIV_PROD, /*79*/
           :old.COR_DE_PRODUCAO,  /*80*/
           :new.COR_DE_PRODUCAO, /*81*/
           :old.CODIGO_CONTABIL,  /*82*/
           :new.CODIGO_CONTABIL, /*83*/
           :old.CGC_CLIENTE_2,  /*84*/
           :new.CGC_CLIENTE_2, /*85*/
           :old.CGC_CLIENTE_4,  /*86*/
           :new.CGC_CLIENTE_4, /*87*/
           :old.CGC_CLIENTE_9,  /*88*/
           :new.CGC_CLIENTE_9, /*89*/
           :old.COMPOSICAO_01,  /*90*/
           :new.COMPOSICAO_01, /*91*/
           :old.COMPOSICAO_02,  /*92*/
           :new.COMPOSICAO_02, /*93*/
           :old.COMPOSICAO_03,  /*94*/
           :new.COMPOSICAO_03, /*95*/
           :old.COMPOSICAO_04,  /*96*/
           :new.COMPOSICAO_04, /*97*/
           :old.COMPOSICAO_05,  /*98*/
           :new.COMPOSICAO_05, /*99*/
           :old.PERC_COMPOSICAO1,  /*100*/
           :new.PERC_COMPOSICAO1, /*101*/
           :old.PERC_COMPOSICAO2,  /*102*/
           :new.PERC_COMPOSICAO2, /*103*/
           :old.PERC_COMPOSICAO3,  /*104*/
           :new.PERC_COMPOSICAO3, /*105*/
           :old.PERC_COMPOSICAO4,  /*106*/
           :new.PERC_COMPOSICAO4, /*107*/
           :old.PERC_COMPOSICAO5,  /*108*/
           :new.PERC_COMPOSICAO5, /*109*/
           :old.ESTAGIO_ALTERA_PROGRAMADO,  /*110*/
           :new.ESTAGIO_ALTERA_PROGRAMADO, /*111*/
           :old.RISCO_PADRAO,  /*112*/
           :new.RISCO_PADRAO, /*113*/
           :old.RESPONSAVEL,  /*114*/
           :new.RESPONSAVEL, /*115*/
           :old.MULTIPLICADOR,  /*116*/
           :new.MULTIPLICADOR, /*117*/
           :old.NUM_SOL_DESENV,  /*118*/
           :new.NUM_SOL_DESENV, /*119*/
           :old.SORTIMENTO_DIVERSOS,  /*120*/
           :new.SORTIMENTO_DIVERSOS, /*121*/
           :old.GRUPO_AGRUPADOR,  /*122*/
           :new.GRUPO_AGRUPADOR, /*123*/
           :old.SUB_AGRUPADOR,  /*124*/
           :new.SUB_AGRUPADOR, /*125*/
           :old.COD_EMBALAGEM,  /*126*/
           :new.COD_EMBALAGEM, /*127*/
           :old.CONF_ALTERA_PROG,  /*128*/
           :new.CONF_ALTERA_PROG, /*129*/
           :old.OBSERVACAO2,  /*130*/
           :new.OBSERVACAO2, /*131*/
           :old.OBSERVACAO3,  /*132*/
           :new.OBSERVACAO3, /*133*/
           :old.PUBLICO_ALVO,  /*134*/
           :new.PUBLICO_ALVO, /*135*/
           :old.COR_DE_SORTIDO,  /*136*/
           :new.COR_DE_SORTIDO, /*137*/
           :old.TIPO_CODIGO_EAN,  /*138*/
           :new.TIPO_CODIGO_EAN, /*139*/
           :old.NUM_PROGRAMA,  /*140*/
           :new.NUM_PROGRAMA, /*141*/
           :old.REFERENCIA_DESENV,  /*142*/
           :new.REFERENCIA_DESENV, /*143*/
           :old.TAB_RENDIMENTO,  /*144*/
           :new.TAB_RENDIMENTO, /*145*/
           :old.LARGURA_CILINDRO,  /*146*/
           :new.LARGURA_CILINDRO, /*147*/
           :old.LARGURA_ESTAMPA,  /*148*/
           :new.LARGURA_ESTAMPA, /*149*/
           :old.DESCRICAO_TAG1,  /*150*/
           :new.DESCRICAO_TAG1, /*151*/
           :old.DESCRICAO_TAG2,  /*152*/
           :new.DESCRICAO_TAG2, /*153*/
           :old.DESCR_COMPLEMENTAR,  /*154*/
           :new.DESCR_COMPLEMENTAR, /*155*/
           :old.PASTA_BANHO,  /*156*/
           :new.PASTA_BANHO, /*157*/
           :old.PERIMETRO_CORTE,  /*158*/
           :new.PERIMETRO_CORTE, /*159*/
           :old.PRODUTO_PROTOTIPO,  /*160*/
           :new.PRODUTO_PROTOTIPO, /*161*/
           :old.TEMPO_LASER,  /*162*/
           :new.TEMPO_LASER, /*163*/
           :old.COLECAO_CLIENTE,  /*164*/
           :new.COLECAO_CLIENTE, /*165*/
           :old.DESCR_INGLES,  /*166*/
           :new.DESCR_INGLES, /*167*/
           :old.DESCR_ESPANHOL,  /*168*/
           :new.DESCR_ESPANHOL, /*169*/
           :old.TAMANHO_AMOSTRA,  /*170*/
           :new.TAMANHO_AMOSTRA, /*171*/
           :old.PESO_AMOSTRA,  /*172*/
           :new.PESO_AMOSTRA, /*173*/
           :old.COMP_SERRA_FITA,  /*174*/
           :new.COMP_SERRA_FITA, /*175*/
           :old.PARTE_COMPOSICAO_01,  /*176*/
           :new.PARTE_COMPOSICAO_01, /*177*/
           :old.PARTE_COMPOSICAO_02,  /*178*/
           :new.PARTE_COMPOSICAO_02, /*179*/
           :old.PARTE_COMPOSICAO_03,  /*180*/
           :new.PARTE_COMPOSICAO_03, /*181*/
           :old.PARTE_COMPOSICAO_04,  /*182*/
           :new.PARTE_COMPOSICAO_04, /*183*/
           :old.PARTE_COMPOSICAO_05,  /*184*/
           :new.PARTE_COMPOSICAO_05, /*185*/
           :old.COMPOSICAO_01_INGLES,  /*186*/
           :new.COMPOSICAO_01_INGLES, /*187*/
           :old.COMPOSICAO_02_INGLES,  /*188*/
           :new.COMPOSICAO_02_INGLES, /*189*/
           :old.COMPOSICAO_03_INGLES,  /*190*/
           :new.COMPOSICAO_03_INGLES, /*191*/
           :old.COMPOSICAO_04_INGLES,  /*192*/
           :new.COMPOSICAO_04_INGLES, /*193*/
           :old.COMPOSICAO_05_INGLES,  /*194*/
           :new.COMPOSICAO_05_INGLES, /*195*/
           :old.TIPO_ESTAMPA,  /*196*/
           :new.TIPO_ESTAMPA, /*197*/
           :old.IMP_INF_EMPRESA_TAG,  /*198*/
           :new.IMP_INF_EMPRESA_TAG, /*199*/
           :old.ESTAMPA_IMPRESSA,  /*200*/
           :new.ESTAMPA_IMPRESSA, /*201*/
           :old.COD_TIPO_AVIAMENTO,  /*202*/
           :new.COD_TIPO_AVIAMENTO, /*203*/
           :old.COD_ATRIBUTO_01,  /*204*/
           :new.COD_ATRIBUTO_01, /*205*/
           :old.COD_ATRIBUTO_02,  /*206*/
           :new.COD_ATRIBUTO_02, /*207*/
           :old.COD_ATRIBUTO_03,  /*208*/
           :new.COD_ATRIBUTO_03, /*209*/
           :old.COD_ATRIBUTO_04,  /*210*/
           :new.COD_ATRIBUTO_04, /*211*/
           :old.COD_ATRIBUTO_05,  /*212*/
           :new.COD_ATRIBUTO_05, /*213*/
           :old.COD_ATRIBUTO_06,  /*214*/
           :new.COD_ATRIBUTO_06, /*215*/
           :old.COD_ATRIBUTO_07,  /*216*/
           :new.COD_ATRIBUTO_07, /*217*/
           :old.COD_ATRIBUTO_08,  /*218*/
           :new.COD_ATRIBUTO_08, /*219*/
           :old.COD_ATRIBUTO_09,  /*220*/
           :new.COD_ATRIBUTO_09, /*221*/
           :old.COD_ATRIBUTO_10,  /*222*/
           :new.COD_ATRIBUTO_10, /*223*/
           :old.REFERENCIA_AGRUPADOR,  /*224*/
           :new.REFERENCIA_AGRUPADOR, /*225*/
           :old.MAQUINA_PILOTO,  /*226*/
           :new.MAQUINA_PILOTO, /*227*/
           :old.FINURA_PILOTO,  /*228*/
           :new.FINURA_PILOTO, /*229*/
           :old.GRADUACAO_PILOTO,  /*230*/
           :new.GRADUACAO_PILOTO, /*231*/
           :old.PROGRAMA_PILOTO,  /*232*/
           :new.PROGRAMA_PILOTO, /*233*/
           :old.PROGRAMADOR_PILOTO,  /*234*/
           :new.PROGRAMADOR_PILOTO, /*235*/
           :old.PESO_BRUTO_PILOTO,  /*236*/
           :new.PESO_BRUTO_PILOTO, /*237*/
           :old.PESO_ACABAMENTO_PILOTO,  /*238*/
           :new.PESO_ACABAMENTO_PILOTO, /*239*/
           :old.TECELAGEM_PILOTO,  /*240*/
           :new.TECELAGEM_PILOTO, /*241*/
           :old.FACCAO_PILOTO,  /*242*/
           :new.FACCAO_PILOTO, /*243*/
           :old.PERS_BORDADO_PILOTO,  /*244*/
           :new.PERS_BORDADO_PILOTO, /*245*/
           :old.PERS_LAVACAO_PILOTO,  /*246*/
           :new.PERS_LAVACAO_PILOTO, /*247*/
           :old.PERS_SERIGRAFIA_PILOTO,  /*248*/
           :new.PERS_SERIGRAFIA_PILOTO, /*249*/
           :old.GRUPO_TECIDO_CORTE,  /*250*/
           :new.GRUPO_TECIDO_CORTE, /*251*/
           :old.PERSONALIZACAO_PILOTO,  /*252*/
           :new.PERSONALIZACAO_PILOTO, /*253*/
           :old.DATA_MOV_CILINDRO,  /*254*/
           :new.DATA_MOV_CILINDRO, /*255*/
           :old.SERIE_TAMANHO,  /*256*/
           :new.SERIE_TAMANHO, /*257*/
           :old.NUMERO_PONTOS_LASER,  /*258*/
           :new.NUMERO_PONTOS_LASER, /*259*/
           :old.PERCENTUAL_AMACIANTE,  /*260*/
           :new.PERCENTUAL_AMACIANTE, /*261*/
           :old.SIMBOLO_1,  /*262*/
           :new.SIMBOLO_1, /*263*/
           :old.SIMBOLO_2,  /*264*/
           :new.SIMBOLO_2, /*265*/
           :old.SIMBOLO_3,  /*266*/
           :new.SIMBOLO_3, /*267*/
           :old.SIMBOLO_4,  /*268*/
           :new.SIMBOLO_4, /*269*/
           :old.SIMBOLO_5,  /*270*/
           :new.SIMBOLO_5, /*271*/
           :old.LOTE_ESTOQUE,  /*272*/
           :new.LOTE_ESTOQUE, /*273*/
           :old.DIAS_MATURACAO,  /*274*/
           :new.DIAS_MATURACAO, /*275*/
           :old.COD_BAR_POR_CLI,  /*276*/
           :new.COD_BAR_POR_CLI, /*277*/
           :old.FAMILIA_ATRIBUTO,  /*278*/
           :new.FAMILIA_ATRIBUTO, /*279*/
           :old.DOC_REFERENCIAL,  /*280*/
           :new.DOC_REFERENCIAL, /*281*/
           :old.LIGAMENTO,  /*282*/
           :new.LIGAMENTO, /*283*/
           :old.PERC_COMPOSICAO6,  /*284*/
           :new.PERC_COMPOSICAO6, /*285*/
           :old.PERC_COMPOSICAO7,  /*286*/
           :new.PERC_COMPOSICAO7, /*287*/
           :old.PERC_COMPOSICAO8,  /*288*/
           :new.PERC_COMPOSICAO8, /*289*/
           :old.PERC_COMPOSICAO9,  /*290*/
           :new.PERC_COMPOSICAO9, /*291*/
           :old.PERC_COMPOSICAO10,  /*292*/
           :new.PERC_COMPOSICAO10, /*293*/
           :old.COMPOSICAO_06,  /*294*/
           :new.COMPOSICAO_06, /*295*/
           :old.COMPOSICAO_07,  /*296*/
           :new.COMPOSICAO_07, /*297*/
           :old.COMPOSICAO_08,  /*298*/
           :new.COMPOSICAO_08, /*299*/
           :old.COMPOSICAO_09,  /*300*/
           :new.COMPOSICAO_09, /*301*/
           :old.COMPOSICAO_10,  /*302*/
           :new.COMPOSICAO_10, /*303*/
           :old.SIMBOLO_6,  /*304*/
           :new.SIMBOLO_6, /*305*/
           :old.SIMBOLO_7,  /*306*/
           :new.SIMBOLO_7, /*307*/
           :old.SIMBOLO_8,  /*308*/
           :new.SIMBOLO_8, /*309*/
           :old.SIMBOLO_9,  /*310*/
           :new.SIMBOLO_9, /*311*/
           :old.SIMBOLO_10,  /*312*/
           :new.SIMBOLO_10, /*313*/
           :old.PARTE_COMPOSICAO_06,  /*314*/
           :new.PARTE_COMPOSICAO_06, /*315*/
           :old.PARTE_COMPOSICAO_07,  /*316*/
           :new.PARTE_COMPOSICAO_07, /*317*/
           :old.PARTE_COMPOSICAO_08,  /*318*/
           :new.PARTE_COMPOSICAO_08, /*319*/
           :old.PARTE_COMPOSICAO_09,  /*320*/
           :new.PARTE_COMPOSICAO_09, /*321*/
           :old.PARTE_COMPOSICAO_10,  /*322*/
           :new.PARTE_COMPOSICAO_10, /*323*/
           :old.PRODUTO_VENDA,  /*324*/
           :new.PRODUTO_VENDA, /*325*/
           :old.NVLPRECOMONTAGEM,  /*326*/
           :new.NVLPRECOMONTAGEM, /*327*/
           :old.OPCAO_VENDA_COR,  /*328*/
           :new.OPCAO_VENDA_COR, /*329*/
           :old.ULTIMA_COR_SORTIDA,  /*330*/
           :new.ULTIMA_COR_SORTIDA, /*331*/
           :old.TIPO_TAG_EAN,  /*332*/
           :new.TIPO_TAG_EAN, /*333*/
           :old.MODULAR,  /*334*/
           :new.MODULAR  /*335*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into basi_030_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           NIVEL_ESTRUTURA_OLD, /*8*/
           NIVEL_ESTRUTURA_NEW, /*9*/
           REFERENCIA_OLD, /*10*/
           REFERENCIA_NEW, /*11*/
           DESCR_REFERENCIA_OLD, /*12*/
           DESCR_REFERENCIA_NEW, /*13*/
           UNIDADE_MEDIDA_OLD, /*14*/
           UNIDADE_MEDIDA_NEW, /*15*/
           PERC_2_QUALIDADE_OLD, /*16*/
           PERC_2_QUALIDADE_NEW, /*17*/
           COLECAO_OLD, /*18*/
           COLECAO_NEW, /*19*/
           CONTA_ESTOQUE_OLD, /*20*/
           CONTA_ESTOQUE_NEW, /*21*/
           LINHA_PRODUTO_OLD, /*22*/
           LINHA_PRODUTO_NEW, /*23*/
           ARTIGO_OLD, /*24*/
           ARTIGO_NEW, /*25*/
           ARTIGO_COTAS_OLD, /*26*/
           ARTIGO_COTAS_NEW, /*27*/
           ARTIGO_APROVEIT_OLD, /*28*/
           ARTIGO_APROVEIT_NEW, /*29*/
           PERCENT_DESCONTO_OLD, /*30*/
           PERCENT_DESCONTO_NEW, /*31*/
           TIPO_PRODUTO_OLD, /*32*/
           TIPO_PRODUTO_NEW, /*33*/
           VARIACAO_CORES_OLD, /*34*/
           VARIACAO_CORES_NEW, /*35*/
           TAMANHO_FILME_OLD, /*36*/
           TAMANHO_FILME_NEW, /*37*/
           RAPORTAGEM_OLD, /*38*/
           RAPORTAGEM_NEW, /*39*/
           PERCENT_PERDA_1_OLD, /*40*/
           PERCENT_PERDA_1_NEW, /*41*/
           PERCENT_PERDA_2_OLD, /*42*/
           PERCENT_PERDA_2_NEW, /*43*/
           PERCENT_PERDA_3_OLD, /*44*/
           PERCENT_PERDA_3_NEW, /*45*/
           APROVEIT_MEDIO_OLD, /*46*/
           APROVEIT_MEDIO_NEW, /*47*/
           METROS_LINEARES_OLD, /*48*/
           METROS_LINEARES_NEW, /*49*/
           INDICE_REAJUSTE_OLD, /*50*/
           INDICE_REAJUSTE_NEW, /*51*/
           ITEM_BAIXA_ESTQ_OLD, /*52*/
           ITEM_BAIXA_ESTQ_NEW, /*53*/
           DEMANDA_OLD, /*54*/
           DEMANDA_NEW, /*55*/
           PONTOS_POR_CM_OLD, /*56*/
           PONTOS_POR_CM_NEW, /*57*/
           OBSERVACAO_OLD, /*58*/
           OBSERVACAO_NEW, /*59*/
           AGULHAS_FALHADAS_OLD, /*60*/
           AGULHAS_FALHADAS_NEW, /*61*/
           FORMACAO_AGULHAS_OLD, /*62*/
           FORMACAO_AGULHAS_NEW, /*63*/
           CLASSIFIC_FISCAL_OLD, /*64*/
           CLASSIFIC_FISCAL_NEW, /*65*/
           COMPRADO_FABRIC_OLD, /*66*/
           COMPRADO_FABRIC_NEW, /*67*/
           ABERTO_TUBULAR_OLD, /*68*/
           ABERTO_TUBULAR_NEW, /*69*/
           NUMERO_ROTEIRO_OLD, /*70*/
           NUMERO_ROTEIRO_NEW, /*71*/
           REF_ORIGINAL_OLD, /*72*/
           REF_ORIGINAL_NEW, /*73*/
           NUMERO_MOLDE_OLD, /*74*/
           NUMERO_MOLDE_NEW, /*75*/
           COR_DE_ESTOQUE_OLD, /*76*/
           COR_DE_ESTOQUE_NEW, /*77*/
           DIV_PROD_OLD, /*78*/
           DIV_PROD_NEW, /*79*/
           COR_DE_PRODUCAO_OLD, /*80*/
           COR_DE_PRODUCAO_NEW, /*81*/
           CODIGO_CONTABIL_OLD, /*82*/
           CODIGO_CONTABIL_NEW, /*83*/
           CGC_CLIENTE_2_OLD, /*84*/
           CGC_CLIENTE_2_NEW, /*85*/
           CGC_CLIENTE_4_OLD, /*86*/
           CGC_CLIENTE_4_NEW, /*87*/
           CGC_CLIENTE_9_OLD, /*88*/
           CGC_CLIENTE_9_NEW, /*89*/
           COMPOSICAO_01_OLD, /*90*/
           COMPOSICAO_01_NEW, /*91*/
           COMPOSICAO_02_OLD, /*92*/
           COMPOSICAO_02_NEW, /*93*/
           COMPOSICAO_03_OLD, /*94*/
           COMPOSICAO_03_NEW, /*95*/
           COMPOSICAO_04_OLD, /*96*/
           COMPOSICAO_04_NEW, /*97*/
           COMPOSICAO_05_OLD, /*98*/
           COMPOSICAO_05_NEW, /*99*/
           PERC_COMPOSICAO1_OLD, /*100*/
           PERC_COMPOSICAO1_NEW, /*101*/
           PERC_COMPOSICAO2_OLD, /*102*/
           PERC_COMPOSICAO2_NEW, /*103*/
           PERC_COMPOSICAO3_OLD, /*104*/
           PERC_COMPOSICAO3_NEW, /*105*/
           PERC_COMPOSICAO4_OLD, /*106*/
           PERC_COMPOSICAO4_NEW, /*107*/
           PERC_COMPOSICAO5_OLD, /*108*/
           PERC_COMPOSICAO5_NEW, /*109*/
           ESTAGIO_ALTERA_PROGRAMADO_OLD, /*110*/
           ESTAGIO_ALTERA_PROGRAMADO_NEW, /*111*/
           RISCO_PADRAO_OLD, /*112*/
           RISCO_PADRAO_NEW, /*113*/
           RESPONSAVEL_OLD, /*114*/
           RESPONSAVEL_NEW, /*115*/
           MULTIPLICADOR_OLD, /*116*/
           MULTIPLICADOR_NEW, /*117*/
           NUM_SOL_DESENV_OLD, /*118*/
           NUM_SOL_DESENV_NEW, /*119*/
           SORTIMENTO_DIVERSOS_OLD, /*120*/
           SORTIMENTO_DIVERSOS_NEW, /*121*/
           GRUPO_AGRUPADOR_OLD, /*122*/
           GRUPO_AGRUPADOR_NEW, /*123*/
           SUB_AGRUPADOR_OLD, /*124*/
           SUB_AGRUPADOR_NEW, /*125*/
           COD_EMBALAGEM_OLD, /*126*/
           COD_EMBALAGEM_NEW, /*127*/
           CONF_ALTERA_PROG_OLD, /*128*/
           CONF_ALTERA_PROG_NEW, /*129*/
           OBSERVACAO2_OLD, /*130*/
           OBSERVACAO2_NEW, /*131*/
           OBSERVACAO3_OLD, /*132*/
           OBSERVACAO3_NEW, /*133*/
           PUBLICO_ALVO_OLD, /*134*/
           PUBLICO_ALVO_NEW, /*135*/
           COR_DE_SORTIDO_OLD, /*136*/
           COR_DE_SORTIDO_NEW, /*137*/
           TIPO_CODIGO_EAN_OLD, /*138*/
           TIPO_CODIGO_EAN_NEW, /*139*/
           NUM_PROGRAMA_OLD, /*140*/
           NUM_PROGRAMA_NEW, /*141*/
           REFERENCIA_DESENV_OLD, /*142*/
           REFERENCIA_DESENV_NEW, /*143*/
           TAB_RENDIMENTO_OLD, /*144*/
           TAB_RENDIMENTO_NEW, /*145*/
           LARGURA_CILINDRO_OLD, /*146*/
           LARGURA_CILINDRO_NEW, /*147*/
           LARGURA_ESTAMPA_OLD, /*148*/
           LARGURA_ESTAMPA_NEW, /*149*/
           DESCRICAO_TAG1_OLD, /*150*/
           DESCRICAO_TAG1_NEW, /*151*/
           DESCRICAO_TAG2_OLD, /*152*/
           DESCRICAO_TAG2_NEW, /*153*/
           DESCR_COMPLEMENTAR_OLD, /*154*/
           DESCR_COMPLEMENTAR_NEW, /*155*/
           PASTA_BANHO_OLD, /*156*/
           PASTA_BANHO_NEW, /*157*/
           PERIMETRO_CORTE_OLD, /*158*/
           PERIMETRO_CORTE_NEW, /*159*/
           PRODUTO_PROTOTIPO_OLD, /*160*/
           PRODUTO_PROTOTIPO_NEW, /*161*/
           TEMPO_LASER_OLD, /*162*/
           TEMPO_LASER_NEW, /*163*/
           COLECAO_CLIENTE_OLD, /*164*/
           COLECAO_CLIENTE_NEW, /*165*/
           DESCR_INGLES_OLD, /*166*/
           DESCR_INGLES_NEW, /*167*/
           DESCR_ESPANHOL_OLD, /*168*/
           DESCR_ESPANHOL_NEW, /*169*/
           TAMANHO_AMOSTRA_OLD, /*170*/
           TAMANHO_AMOSTRA_NEW, /*171*/
           PESO_AMOSTRA_OLD, /*172*/
           PESO_AMOSTRA_NEW, /*173*/
           COMP_SERRA_FITA_OLD, /*174*/
           COMP_SERRA_FITA_NEW, /*175*/
           PARTE_COMPOSICAO_01_OLD, /*176*/
           PARTE_COMPOSICAO_01_NEW, /*177*/
           PARTE_COMPOSICAO_02_OLD, /*178*/
           PARTE_COMPOSICAO_02_NEW, /*179*/
           PARTE_COMPOSICAO_03_OLD, /*180*/
           PARTE_COMPOSICAO_03_NEW, /*181*/
           PARTE_COMPOSICAO_04_OLD, /*182*/
           PARTE_COMPOSICAO_04_NEW, /*183*/
           PARTE_COMPOSICAO_05_OLD, /*184*/
           PARTE_COMPOSICAO_05_NEW, /*185*/
           COMPOSICAO_01_INGLES_OLD, /*186*/
           COMPOSICAO_01_INGLES_NEW, /*187*/
           COMPOSICAO_02_INGLES_OLD, /*188*/
           COMPOSICAO_02_INGLES_NEW, /*189*/
           COMPOSICAO_03_INGLES_OLD, /*190*/
           COMPOSICAO_03_INGLES_NEW, /*191*/
           COMPOSICAO_04_INGLES_OLD, /*192*/
           COMPOSICAO_04_INGLES_NEW, /*193*/
           COMPOSICAO_05_INGLES_OLD, /*194*/
           COMPOSICAO_05_INGLES_NEW, /*195*/
           TIPO_ESTAMPA_OLD, /*196*/
           TIPO_ESTAMPA_NEW, /*197*/
           IMP_INF_EMPRESA_TAG_OLD, /*198*/
           IMP_INF_EMPRESA_TAG_NEW, /*199*/
           ESTAMPA_IMPRESSA_OLD, /*200*/
           ESTAMPA_IMPRESSA_NEW, /*201*/
           COD_TIPO_AVIAMENTO_OLD, /*202*/
           COD_TIPO_AVIAMENTO_NEW, /*203*/
           COD_ATRIBUTO_01_OLD, /*204*/
           COD_ATRIBUTO_01_NEW, /*205*/
           COD_ATRIBUTO_02_OLD, /*206*/
           COD_ATRIBUTO_02_NEW, /*207*/
           COD_ATRIBUTO_03_OLD, /*208*/
           COD_ATRIBUTO_03_NEW, /*209*/
           COD_ATRIBUTO_04_OLD, /*210*/
           COD_ATRIBUTO_04_NEW, /*211*/
           COD_ATRIBUTO_05_OLD, /*212*/
           COD_ATRIBUTO_05_NEW, /*213*/
           COD_ATRIBUTO_06_OLD, /*214*/
           COD_ATRIBUTO_06_NEW, /*215*/
           COD_ATRIBUTO_07_OLD, /*216*/
           COD_ATRIBUTO_07_NEW, /*217*/
           COD_ATRIBUTO_08_OLD, /*218*/
           COD_ATRIBUTO_08_NEW, /*219*/
           COD_ATRIBUTO_09_OLD, /*220*/
           COD_ATRIBUTO_09_NEW, /*221*/
           COD_ATRIBUTO_10_OLD, /*222*/
           COD_ATRIBUTO_10_NEW, /*223*/
           REFERENCIA_AGRUPADOR_OLD, /*224*/
           REFERENCIA_AGRUPADOR_NEW, /*225*/
           MAQUINA_PILOTO_OLD, /*226*/
           MAQUINA_PILOTO_NEW, /*227*/
           FINURA_PILOTO_OLD, /*228*/
           FINURA_PILOTO_NEW, /*229*/
           GRADUACAO_PILOTO_OLD, /*230*/
           GRADUACAO_PILOTO_NEW, /*231*/
           PROGRAMA_PILOTO_OLD, /*232*/
           PROGRAMA_PILOTO_NEW, /*233*/
           PROGRAMADOR_PILOTO_OLD, /*234*/
           PROGRAMADOR_PILOTO_NEW, /*235*/
           PESO_BRUTO_PILOTO_OLD, /*236*/
           PESO_BRUTO_PILOTO_NEW, /*237*/
           PESO_ACABAMENTO_PILOTO_OLD, /*238*/
           PESO_ACABAMENTO_PILOTO_NEW, /*239*/
           TECELAGEM_PILOTO_OLD, /*240*/
           TECELAGEM_PILOTO_NEW, /*241*/
           FACCAO_PILOTO_OLD, /*242*/
           FACCAO_PILOTO_NEW, /*243*/
           PERS_BORDADO_PILOTO_OLD, /*244*/
           PERS_BORDADO_PILOTO_NEW, /*245*/
           PERS_LAVACAO_PILOTO_OLD, /*246*/
           PERS_LAVACAO_PILOTO_NEW, /*247*/
           PERS_SERIGRAFIA_PILOTO_OLD, /*248*/
           PERS_SERIGRAFIA_PILOTO_NEW, /*249*/
           GRUPO_TECIDO_CORTE_OLD, /*250*/
           GRUPO_TECIDO_CORTE_NEW, /*251*/
           PERSONALIZACAO_PILOTO_OLD, /*252*/
           PERSONALIZACAO_PILOTO_NEW, /*253*/
           DATA_MOV_CILINDRO_OLD, /*254*/
           DATA_MOV_CILINDRO_NEW, /*255*/
           SERIE_TAMANHO_OLD, /*256*/
           SERIE_TAMANHO_NEW, /*257*/
           NUMERO_PONTOS_LASER_OLD, /*258*/
           NUMERO_PONTOS_LASER_NEW, /*259*/
           PERCENTUAL_AMACIANTE_OLD, /*260*/
           PERCENTUAL_AMACIANTE_NEW, /*261*/
           SIMBOLO_1_OLD, /*262*/
           SIMBOLO_1_NEW, /*263*/
           SIMBOLO_2_OLD, /*264*/
           SIMBOLO_2_NEW, /*265*/
           SIMBOLO_3_OLD, /*266*/
           SIMBOLO_3_NEW, /*267*/
           SIMBOLO_4_OLD, /*268*/
           SIMBOLO_4_NEW, /*269*/
           SIMBOLO_5_OLD, /*270*/
           SIMBOLO_5_NEW, /*271*/
           LOTE_ESTOQUE_OLD, /*272*/
           LOTE_ESTOQUE_NEW, /*273*/
           DIAS_MATURACAO_OLD, /*274*/
           DIAS_MATURACAO_NEW, /*275*/
           COD_BAR_POR_CLI_OLD, /*276*/
           COD_BAR_POR_CLI_NEW, /*277*/
           FAMILIA_ATRIBUTO_OLD, /*278*/
           FAMILIA_ATRIBUTO_NEW, /*279*/
           DOC_REFERENCIAL_OLD, /*280*/
           DOC_REFERENCIAL_NEW, /*281*/
           LIGAMENTO_OLD, /*282*/
           LIGAMENTO_NEW, /*283*/
           PERC_COMPOSICAO6_OLD, /*284*/
           PERC_COMPOSICAO6_NEW, /*285*/
           PERC_COMPOSICAO7_OLD, /*286*/
           PERC_COMPOSICAO7_NEW, /*287*/
           PERC_COMPOSICAO8_OLD, /*288*/
           PERC_COMPOSICAO8_NEW, /*289*/
           PERC_COMPOSICAO9_OLD, /*290*/
           PERC_COMPOSICAO9_NEW, /*291*/
           PERC_COMPOSICAO10_OLD, /*292*/
           PERC_COMPOSICAO10_NEW, /*293*/
           COMPOSICAO_06_OLD, /*294*/
           COMPOSICAO_06_NEW, /*295*/
           COMPOSICAO_07_OLD, /*296*/
           COMPOSICAO_07_NEW, /*297*/
           COMPOSICAO_08_OLD, /*298*/
           COMPOSICAO_08_NEW, /*299*/
           COMPOSICAO_09_OLD, /*300*/
           COMPOSICAO_09_NEW, /*301*/
           COMPOSICAO_10_OLD, /*302*/
           COMPOSICAO_10_NEW, /*303*/
           SIMBOLO_6_OLD, /*304*/
           SIMBOLO_6_NEW, /*305*/
           SIMBOLO_7_OLD, /*306*/
           SIMBOLO_7_NEW, /*307*/
           SIMBOLO_8_OLD, /*308*/
           SIMBOLO_8_NEW, /*309*/
           SIMBOLO_9_OLD, /*310*/
           SIMBOLO_9_NEW, /*311*/
           SIMBOLO_10_OLD, /*312*/
           SIMBOLO_10_NEW, /*313*/
           PARTE_COMPOSICAO_06_OLD, /*314*/
           PARTE_COMPOSICAO_06_NEW, /*315*/
           PARTE_COMPOSICAO_07_OLD, /*316*/
           PARTE_COMPOSICAO_07_NEW, /*317*/
           PARTE_COMPOSICAO_08_OLD, /*318*/
           PARTE_COMPOSICAO_08_NEW, /*319*/
           PARTE_COMPOSICAO_09_OLD, /*320*/
           PARTE_COMPOSICAO_09_NEW, /*321*/
           PARTE_COMPOSICAO_10_OLD, /*322*/
           PARTE_COMPOSICAO_10_NEW, /*323*/
           PRODUTO_VENDA_OLD, /*324*/
           PRODUTO_VENDA_NEW, /*325*/
           NVLPRECOMONTAGEM_OLD, /*326*/
           NVLPRECOMONTAGEM_NEW, /*327*/
           OPCAO_VENDA_COR_OLD, /*328*/
           OPCAO_VENDA_COR_NEW, /*329*/
           ULTIMA_COR_SORTIDA_OLD, /*330*/
           ULTIMA_COR_SORTIDA_NEW, /*331*/
           TIPO_TAG_EAN_OLD, /*332*/
           TIPO_TAG_EAN_NEW, /*333*/
           MODULAR_OLD, /*334*/
           MODULAR_NEW /*335*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.NIVEL_ESTRUTURA, /*8*/
           '', /*9*/
           :old.REFERENCIA, /*10*/
           '', /*11*/
           :old.DESCR_REFERENCIA, /*12*/
           '', /*13*/
           :old.UNIDADE_MEDIDA, /*14*/
           '', /*15*/
           :old.PERC_2_QUALIDADE, /*16*/
           0, /*17*/
           :old.COLECAO, /*18*/
           0, /*19*/
           :old.CONTA_ESTOQUE, /*20*/
           0, /*21*/
           :old.LINHA_PRODUTO, /*22*/
           0, /*23*/
           :old.ARTIGO, /*24*/
           0, /*25*/
           :old.ARTIGO_COTAS, /*26*/
           0, /*27*/
           :old.ARTIGO_APROVEIT, /*28*/
           0, /*29*/
           :old.PERCENT_DESCONTO, /*30*/
           0, /*31*/
           :old.TIPO_PRODUTO, /*32*/
           0, /*33*/
           :old.VARIACAO_CORES, /*34*/
           0, /*35*/
           :old.TAMANHO_FILME, /*36*/
           0, /*37*/
           :old.RAPORTAGEM, /*38*/
           0, /*39*/
           :old.PERCENT_PERDA_1, /*40*/
           0, /*41*/
           :old.PERCENT_PERDA_2, /*42*/
           0, /*43*/
           :old.PERCENT_PERDA_3, /*44*/
           0, /*45*/
           :old.APROVEIT_MEDIO, /*46*/
           0, /*47*/
           :old.METROS_LINEARES, /*48*/
           0, /*49*/
           :old.INDICE_REAJUSTE, /*50*/
           0, /*51*/
           :old.ITEM_BAIXA_ESTQ, /*52*/
           '', /*53*/
           :old.DEMANDA, /*54*/
           0, /*55*/
           :old.PONTOS_POR_CM, /*56*/
           0, /*57*/
           :old.OBSERVACAO, /*58*/
           '', /*59*/
           :old.AGULHAS_FALHADAS, /*60*/
           0, /*61*/
           :old.FORMACAO_AGULHAS, /*62*/
           0, /*63*/
           :old.CLASSIFIC_FISCAL, /*64*/
           '', /*65*/
           :old.COMPRADO_FABRIC, /*66*/
           0, /*67*/
           :old.ABERTO_TUBULAR, /*68*/
           0, /*69*/
           :old.NUMERO_ROTEIRO, /*70*/
           0, /*71*/
           :old.REF_ORIGINAL, /*72*/
           '', /*73*/
           :old.NUMERO_MOLDE, /*74*/
           :old.NUMERO_MOLDE, /*75*/
           :old.COR_DE_ESTOQUE, /*76*/
           '', /*77*/
           :old.DIV_PROD, /*78*/
           0, /*79*/
           :old.COR_DE_PRODUCAO, /*80*/
           '', /*81*/
           :old.CODIGO_CONTABIL, /*82*/
           0, /*83*/
           :old.CGC_CLIENTE_2, /*84*/
           0, /*85*/
           :old.CGC_CLIENTE_4, /*86*/
           0, /*87*/
           :old.CGC_CLIENTE_9, /*88*/
           0, /*89*/
           :old.COMPOSICAO_01, /*90*/
           '', /*91*/
           :old.COMPOSICAO_02, /*92*/
           '', /*93*/
           :old.COMPOSICAO_03, /*94*/
           '', /*95*/
           :old.COMPOSICAO_04, /*96*/
           '', /*97*/
           :old.COMPOSICAO_05, /*98*/
           '', /*99*/
           :old.PERC_COMPOSICAO1, /*100*/
           0, /*101*/
           :old.PERC_COMPOSICAO2, /*102*/
           0, /*103*/
           :old.PERC_COMPOSICAO3, /*104*/
           0, /*105*/
           :old.PERC_COMPOSICAO4, /*106*/
           0, /*107*/
           :old.PERC_COMPOSICAO5, /*108*/
           0, /*109*/
           :old.ESTAGIO_ALTERA_PROGRAMADO, /*110*/
           0, /*111*/
           :old.RISCO_PADRAO, /*112*/
           0, /*113*/
           :old.RESPONSAVEL, /*114*/
           '', /*115*/
           :old.MULTIPLICADOR, /*116*/
           0, /*117*/
           :old.NUM_SOL_DESENV, /*118*/
           '', /*119*/
           :old.SORTIMENTO_DIVERSOS, /*120*/
           '', /*121*/
           :old.GRUPO_AGRUPADOR, /*122*/
           '', /*123*/
           :old.SUB_AGRUPADOR, /*124*/
           '', /*125*/
           :old.COD_EMBALAGEM, /*126*/
           0, /*127*/
           :old.CONF_ALTERA_PROG, /*128*/
           0, /*129*/
           :old.OBSERVACAO2, /*130*/
           '', /*131*/
           :old.OBSERVACAO3, /*132*/
           '', /*133*/
           :old.PUBLICO_ALVO, /*134*/
           0, /*135*/
           :old.COR_DE_SORTIDO, /*136*/
           '', /*137*/
           :old.TIPO_CODIGO_EAN, /*138*/
           0, /*139*/
           :old.NUM_PROGRAMA, /*140*/
           '', /*141*/
           :old.REFERENCIA_DESENV, /*142*/
           '', /*143*/
           :old.TAB_RENDIMENTO, /*144*/
           0, /*145*/
           :old.LARGURA_CILINDRO, /*146*/
           0, /*147*/
           :old.LARGURA_ESTAMPA, /*148*/
           0, /*149*/
           :old.DESCRICAO_TAG1, /*150*/
           '', /*151*/
           :old.DESCRICAO_TAG2, /*152*/
           '', /*153*/
           :old.DESCR_COMPLEMENTAR, /*154*/
           '', /*155*/
           :old.PASTA_BANHO, /*156*/
           0, /*157*/
           :old.PERIMETRO_CORTE, /*158*/
           0, /*159*/
           :old.PRODUTO_PROTOTIPO, /*160*/
           '', /*161*/
           :old.TEMPO_LASER, /*162*/
           0, /*163*/
           :old.COLECAO_CLIENTE, /*164*/
           '', /*165*/
           :old.DESCR_INGLES, /*166*/
           '', /*167*/
           :old.DESCR_ESPANHOL, /*168*/
           '', /*169*/
           :old.TAMANHO_AMOSTRA, /*170*/
           '', /*171*/
           :old.PESO_AMOSTRA, /*172*/
           0, /*173*/
           :old.COMP_SERRA_FITA, /*174*/
           '', /*175*/
           :old.PARTE_COMPOSICAO_01, /*176*/
           '', /*177*/
           :old.PARTE_COMPOSICAO_02, /*178*/
           '', /*179*/
           :old.PARTE_COMPOSICAO_03, /*180*/
           '', /*181*/
           :old.PARTE_COMPOSICAO_04, /*182*/
           '', /*183*/
           :old.PARTE_COMPOSICAO_05, /*184*/
           '', /*185*/
           :old.COMPOSICAO_01_INGLES, /*186*/
           '', /*187*/
           :old.COMPOSICAO_02_INGLES, /*188*/
           '', /*189*/
           :old.COMPOSICAO_03_INGLES, /*190*/
           '', /*191*/
           :old.COMPOSICAO_04_INGLES, /*192*/
           '', /*193*/
           :old.COMPOSICAO_05_INGLES, /*194*/
           '', /*195*/
           :old.TIPO_ESTAMPA, /*196*/
           0, /*197*/
           :old.IMP_INF_EMPRESA_TAG, /*198*/
           '', /*199*/
           :old.ESTAMPA_IMPRESSA, /*200*/
           0, /*201*/
           :old.COD_TIPO_AVIAMENTO, /*202*/
           '', /*203*/
           :old.COD_ATRIBUTO_01, /*204*/
           0, /*205*/
           :old.COD_ATRIBUTO_02, /*206*/
           0, /*207*/
           :old.COD_ATRIBUTO_03, /*208*/
           0, /*209*/
           :old.COD_ATRIBUTO_04, /*210*/
           0, /*211*/
           :old.COD_ATRIBUTO_05, /*212*/
           0, /*213*/
           :old.COD_ATRIBUTO_06, /*214*/
           0, /*215*/
           :old.COD_ATRIBUTO_07, /*216*/
           0, /*217*/
           :old.COD_ATRIBUTO_08, /*218*/
           0, /*219*/
           :old.COD_ATRIBUTO_09, /*220*/
           0, /*221*/
           :old.COD_ATRIBUTO_10, /*222*/
           0, /*223*/
           :old.REFERENCIA_AGRUPADOR, /*224*/
           '', /*225*/
           :old.MAQUINA_PILOTO, /*226*/
           '', /*227*/
           :old.FINURA_PILOTO, /*228*/
           '', /*229*/
           :old.GRADUACAO_PILOTO, /*230*/
           '', /*231*/
           :old.PROGRAMA_PILOTO, /*232*/
           '', /*233*/
           :old.PROGRAMADOR_PILOTO, /*234*/
           '', /*235*/
           :old.PESO_BRUTO_PILOTO, /*236*/
           0, /*237*/
           :old.PESO_ACABAMENTO_PILOTO, /*238*/
           0, /*239*/
           :old.TECELAGEM_PILOTO, /*240*/
           '', /*241*/
           :old.FACCAO_PILOTO, /*242*/
           '', /*243*/
           :old.PERS_BORDADO_PILOTO, /*244*/
           0, /*245*/
           :old.PERS_LAVACAO_PILOTO, /*246*/
           0, /*247*/
           :old.PERS_SERIGRAFIA_PILOTO, /*248*/
           0, /*249*/
           :old.GRUPO_TECIDO_CORTE, /*250*/
           0, /*251*/
           :old.PERSONALIZACAO_PILOTO, /*252*/
           '', /*253*/
           :old.DATA_MOV_CILINDRO, /*254*/
           null, /*255*/
           :old.SERIE_TAMANHO, /*256*/
           0, /*257*/
           :old.NUMERO_PONTOS_LASER, /*258*/
           0, /*259*/
           :old.PERCENTUAL_AMACIANTE, /*260*/
           0, /*261*/
           :old.SIMBOLO_1, /*262*/
           '', /*263*/
           :old.SIMBOLO_2, /*264*/
           '', /*265*/
           :old.SIMBOLO_3, /*266*/
           '', /*267*/
           :old.SIMBOLO_4, /*268*/
           '', /*269*/
           :old.SIMBOLO_5, /*270*/
           '', /*271*/
           :old.LOTE_ESTOQUE, /*272*/
           0, /*273*/
           :old.DIAS_MATURACAO, /*274*/
           0, /*275*/
           :old.COD_BAR_POR_CLI, /*276*/
           '', /*277*/
           :old.FAMILIA_ATRIBUTO, /*278*/
           '', /*279*/
           :old.DOC_REFERENCIAL, /*280*/
           '', /*281*/
           :old.LIGAMENTO, /*282*/
           '', /*283*/
           :old.PERC_COMPOSICAO6, /*284*/
           0, /*285*/
           :old.PERC_COMPOSICAO7, /*286*/
           0, /*287*/
           :old.PERC_COMPOSICAO8, /*288*/
           0, /*289*/
           :old.PERC_COMPOSICAO9, /*290*/
           0, /*291*/
           :old.PERC_COMPOSICAO10, /*292*/
           0, /*293*/
           :old.COMPOSICAO_06, /*294*/
           '', /*295*/
           :old.COMPOSICAO_07, /*296*/
           '', /*297*/
           :old.COMPOSICAO_08, /*298*/
           '', /*299*/
           :old.COMPOSICAO_09, /*300*/
           '', /*301*/
           :old.COMPOSICAO_10, /*302*/
           '', /*303*/
           :old.SIMBOLO_6, /*304*/
           '', /*305*/
           :old.SIMBOLO_7, /*306*/
           '', /*307*/
           :old.SIMBOLO_8, /*308*/
           '', /*309*/
           :old.SIMBOLO_9, /*310*/
           '', /*311*/
           :old.SIMBOLO_10, /*312*/
           '', /*313*/
           :old.PARTE_COMPOSICAO_06, /*314*/
           '', /*315*/
           :old.PARTE_COMPOSICAO_07, /*316*/
           '', /*317*/
           :old.PARTE_COMPOSICAO_08, /*318*/
           '', /*319*/
           :old.PARTE_COMPOSICAO_09, /*320*/
           '', /*321*/
           :old.PARTE_COMPOSICAO_10, /*322*/
           '', /*323*/
           :old.PRODUTO_VENDA, /*324*/
           '', /*325*/
           :old.NVLPRECOMONTAGEM, /*326*/
           0, /*327*/
           :old.OPCAO_VENDA_COR, /*328*/
           0, /*329*/
           :old.ULTIMA_COR_SORTIDA, /*330*/
           '', /*331*/
           :old.TIPO_TAG_EAN, /*332*/
           '', /*333*/
           :old.MODULAR, /*334*/
           0 /*335*/
         );
    end;
 end if;
end inter_tr_basi_030_log;

-- ALTER TRIGGER "INTER_TR_BASI_030_LOG" ENABLE
 

/

exec inter_pr_recompile;

