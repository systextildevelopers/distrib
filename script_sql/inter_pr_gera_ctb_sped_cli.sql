create or replace procedure inter_pr_gera_ctb_sped_cli(p_cod_empresa   IN NUMBER,
                                                       p_exercicio     IN NUMBER,
                                                       p_per_inicial   IN DATE,
                                                       p_per_final     IN DATE,
                                                       p_nome_programa IN VARCHAR2,
                                                       p_inclu_exce    IN NUMBER,
                                                       p_cod_usuario   IN NUMBER,
                                                       p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar as tabelas sped_cta_i550 - Razao/diario de clientes
   --
   -- Historicos
   --
   -- Data    Autor    Observacoes
   --


   -- busca clietne para saldos anteriores
   cursor clientes is
      select pedi_010.cgc_9, pedi_010.cgc_4,
             pedi_010.cgc_2, pedi_010.codigo_contabil,
             pedi_010.nome_cliente
      from pedi_010
           -- FILTRA O CODIGO CONTABIL
      where (((exists (select r.grupo_estrutura
                  from rcnb_060 r,
                       cont_002 c
                  where r.tipo_registro       = 546
                   and r.nr_solicitacao       = 2
                   and r.item_estrutura       = p_cod_empresa  --cod_empresa
                   and r.item_estrutura_str   = 'C'
                   and r.campo_numerico       = 1 -- inclusao
                   and r.grupo_estrutura      = pedi_010.codigo_contabil
                   and c.codigo_empresa       = r.item_estrutura
                   and c.inc_exc              = r.campo_numerico
                   and r.item_estrutura_str   = c.tipo_cli_for
                   and c.ind_opcao_parametro  = 1))
           or
             (not exists (select r.grupo_estrutura
                  from rcnb_060 r,
                       cont_002 c
                  where r.tipo_registro       = 546
                   and r.nr_solicitacao       = 2
                   and r.item_estrutura       = p_cod_empresa  --cod_empresa
                   and r.item_estrutura_str   = 'C'
                   and r.campo_numerico       = 2  -- exclu
                   and r.grupo_estrutura      = pedi_010.codigo_contabil
                   and c.codigo_empresa       = r.item_estrutura
                   and c.inc_exc              = r.campo_numerico
                   and r.item_estrutura_str   = c.tipo_cli_for
                   and c.ind_opcao_parametro  = 1)
                   and exists (select 1  from cont_002
                               where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                                 and cont_002.tipo_cli_for        = 'C'
                                 and cont_002.ind_opcao_parametro = 1
                                 and cont_002.inc_exc             = 2))
           )
       or not exists (select 1  from cont_002
           where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
             and cont_002.tipo_cli_for        = 'C'
             and cont_002.ind_opcao_parametro = 1))

       -- FILTRA CLIENTE
      and (((p_inclu_exce = 1
      and exists (select 1 from rcnb_060
                  where rcnb_060.tipo_registro    = 60
                    and rcnb_060.nr_solicitacao   = 455
                    and rcnb_060.subgru_estrutura = p_cod_usuario
                    and rcnb_060.grupo_estrutura  = pedi_010.cgc_9
                    and rcnb_060.item_estrutura   = pedi_010.cgc_4
                    and rcnb_060.nivel_estrutura  = pedi_010.cgc_2))
        or  (p_inclu_exce = 2
      and not exists (select 1 from rcnb_060
                       where rcnb_060.tipo_registro    = 60
                         and rcnb_060.nr_solicitacao   = 455
                         and rcnb_060.subgru_estrutura = p_cod_usuario
                         and rcnb_060.grupo_estrutura  = pedi_010.cgc_9
                         and rcnb_060.item_estrutura   = pedi_010.cgc_4
                         and rcnb_060.nivel_estrutura  = pedi_010.cgc_2)))
          or p_nome_programa = 'cont_f001');



   -- busca saldos anteriores
   cursor saldos(p_cod_empresa NUMBER, p_data_ini DATE, p_cgc9 NUMBER, p_cgc4 NUMBER, p_cgc2 NUMBER) IS
      select fatu_070.codigo_empresa,      fatu_070.num_duplicata,
             fatu_070.seq_duplicatas,      fatu_070.cli_dup_cgc_cli9,
             fatu_070.cli_dup_cgc_cli4,    fatu_070.cli_dup_cgc_cli2,
             fatu_070.tipo_titulo,         fatu_070.data_emissao,
             fatu_070.valor_duplicata,     fatu_070.cod_historico,
             cont_010.historico_contab

      from fatu_070,
           cont_010
      where fatu_070.codigo_empresa in (select codigo_empresa from fatu_500
                                        where fatu_500.codigo_matriz = p_cod_empresa)

             -- FILTRA O TIPO TITULO
       and   (((exists (select r.subgru_estrutura
                        from rcnb_060 r,
                             cont_002 c
                        where r.grupo_estrutura     = 815
                          and r.item_estrutura       = p_cod_empresa -- cod_empresa
                          and r.item_estrutura_str   = 'C'
                          and r.nivel_estrutura      = 1 --inclu
                          and fatu_070.tipo_titulo   = r.subgru_estrutura
                          and c.codigo_empresa       = r.item_estrutura
                          and c.inc_exc              = r.nivel_estrutura
                          and r.item_estrutura_str   = c.tipo_cli_for
                          and c.ind_opcao_parametro  = 0))


       or   (not exists (select r.subgru_estrutura
                         from rcnb_060 r,
                              cont_002 c
                         where r.grupo_estrutura     = 815
                           and r.item_estrutura       = p_cod_empresa -- cod_empresa
                           and r.item_estrutura_str   = 'C'
                           and r.nivel_estrutura      = 2 -- exclu
                           and r.subgru_estrutura    = fatu_070.tipo_titulo
                           and c.codigo_empresa       = r.item_estrutura
                           and c.inc_exc              = r.nivel_estrutura
                           and r.item_estrutura_str   = c.tipo_cli_for
                           and c.ind_opcao_parametro  = 0)
                           and exists (select 1  from cont_002
                                       where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                                         and cont_002.tipo_cli_for        = 'C'
                                         and cont_002.ind_opcao_parametro = 0
                                         and cont_002.inc_exc             = 2))
       )
      or not exists (select 1  from cont_002
                  where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                    and cont_002.tipo_cli_for        = 'C'
                    and cont_002.ind_opcao_parametro = 0)) -- ou sem cadastro


      and   fatu_070.data_emissao      < p_data_ini
      and   fatu_070.situacao_duplic  <> 2
      and   fatu_070.previsao          = 0
      and   cont_010.codigo_historico  = fatu_070.cod_historico
      and   cont_010.tipo_historico   <> 'NC'
      and   cont_010.diario            = 1
      and   fatu_070.cli_dup_cgc_cli9  = p_cgc9
      and   fatu_070.cli_dup_cgc_cli4  = p_cgc4
      and   fatu_070.cli_dup_cgc_cli2  = p_cgc2
      order by fatu_070.cli_dup_cgc_cli9, fatu_070.cli_dup_cgc_cli4,
               fatu_070.cli_dup_cgc_cli2;

   -- entrada de titulos
   cursor fatu070(p_cod_empresa NUMBER, p_data_ini DATE, p_data_fim DATE, p_cgc9 NUMBER, p_cgc4 NUMBER, p_cgc2 NUMBER) IS
      select fatu_070.codigo_empresa,      fatu_070.num_duplicata,
             fatu_070.seq_duplicatas,      fatu_070.cli_dup_cgc_cli9,
             fatu_070.cli_dup_cgc_cli4,    fatu_070.cli_dup_cgc_cli2,
             fatu_070.tipo_titulo,         fatu_070.data_emissao,
             fatu_070.valor_duplicata,     fatu_070.cod_historico,
             cont_010.historico_contab

      from fatu_070,
           cont_010
      where fatu_070.codigo_empresa in (select codigo_empresa from fatu_500
                                        where fatu_500.codigo_matriz = p_cod_empresa)

             -- FILTRA O TIPO TITULO
       and   (((exists (select r.subgru_estrutura
                        from rcnb_060 r,
                             cont_002 c
                        where r.grupo_estrutura     = 815
                          and r.item_estrutura       = p_cod_empresa -- cod_empresa
                          and r.item_estrutura_str   = 'C'
                          and r.nivel_estrutura      = 1 --inclu
                          and fatu_070.tipo_titulo   = r.subgru_estrutura
                          and c.codigo_empresa       = r.item_estrutura
                          and c.inc_exc              = r.nivel_estrutura
                          and r.item_estrutura_str   = c.tipo_cli_for
                          and c.ind_opcao_parametro  = 0))


       or   (not exists (select r.subgru_estrutura
                         from rcnb_060 r,
                              cont_002 c
                         where r.grupo_estrutura     = 815
                           and r.item_estrutura       = p_cod_empresa -- cod_empresa
                           and r.item_estrutura_str   = 'C'
                           and r.nivel_estrutura      = 2 -- exclu
                           and r.subgru_estrutura    = fatu_070.tipo_titulo
                           and c.codigo_empresa       = r.item_estrutura
                           and c.inc_exc              = r.nivel_estrutura
                           and r.item_estrutura_str   = c.tipo_cli_for
                           and c.ind_opcao_parametro  = 0)
                           and exists (select 1  from cont_002
                                       where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                                         and cont_002.tipo_cli_for        = 'C'
                                         and cont_002.ind_opcao_parametro = 0
                                         and cont_002.inc_exc             = 2))
       )
      or not exists (select 1  from cont_002
                  where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                    and cont_002.tipo_cli_for        = 'C'
                    and cont_002.ind_opcao_parametro = 0)) -- ou sem cadastro

      and   fatu_070.data_emissao     >= p_data_ini
      and   fatu_070.data_emissao     <= p_data_fim
      and   fatu_070.situacao_duplic  <> 2
      and   fatu_070.previsao          = 0
      and   cont_010.codigo_historico  = fatu_070.cod_historico
      and   cont_010.tipo_historico   <> 'NC'
      and   cont_010.diario            = 1
      and   fatu_070.cli_dup_cgc_cli9  = p_cgc9
      and   fatu_070.cli_dup_cgc_cli4  = p_cgc4
      and   fatu_070.cli_dup_cgc_cli2  = p_cgc2;




   -- baixa de titulos
   cursor fatu075(p_cod_empresa NUMBER, p_data_ini DATE, p_data_fim DATE, p_data_contab_crec NUMBER, p_cgc9 NUMBER, p_cgc4 NUMBER, p_cgc2 NUMBER) IS
      select fatu_070.codigo_empresa,      fatu_070.num_duplicata,
             fatu_070.seq_duplicatas,      fatu_070.cli_dup_cgc_cli9,
             fatu_070.cli_dup_cgc_cli4,    fatu_070.cli_dup_cgc_cli2,
             fatu_070.tipo_titulo,         fatu_075.seq_pagamento,
             fatu_075.data_pagamento,      fatu_075.data_credito,
             fatu_075.valor_pago,          fatu_075.valor_juros,
             fatu_075.valor_descontos,     fatu_075.historico_pgto,
             fatu_075.vlr_var_cambial,

             cont_010.historico_contab,    cont_010.sinal_titulo

      from fatu_075,
           fatu_070,
           cont_010,
           cont_010 cont_010_c
      where ((fatu_075.data_credito    >= p_data_ini
      and     fatu_075.data_credito    <= p_data_fim
      and     p_data_contab_crec = 1)
      or     (fatu_075.data_pagamento  >= p_data_ini
      and     fatu_075.data_pagamento  <= p_data_fim
      and     p_data_contab_crec = 2))
      and     fatu_075.nr_titul_codempr in (select codigo_empresa from fatu_500
                                            where fatu_500.codigo_matriz = p_cod_empresa)

             -- FILTRA O TIPO TITULO
       and   (((exists (select r.subgru_estrutura
                        from rcnb_060 r,
                             cont_002 c
                        where r.grupo_estrutura     = 815
                          and r.item_estrutura       = p_cod_empresa -- cod_empresa
                          and r.item_estrutura_str   = 'C'
                          and r.nivel_estrutura      = 1 --inclu
                          and fatu_070.tipo_titulo   = r.subgru_estrutura
                          and c.codigo_empresa       = r.item_estrutura
                          and c.inc_exc              = r.nivel_estrutura
                          and r.item_estrutura_str   = c.tipo_cli_for
                          and c.ind_opcao_parametro  = 0))


       or   (not exists (select r.subgru_estrutura
                         from rcnb_060 r,
                              cont_002 c
                         where r.grupo_estrutura     = 815
                           and r.item_estrutura       = p_cod_empresa -- cod_empresa
                           and r.item_estrutura_str   = 'C'
                           and r.nivel_estrutura      = 2 -- exclu
                           and r.subgru_estrutura    = fatu_070.tipo_titulo
                           and c.codigo_empresa       = r.item_estrutura
                           and c.inc_exc              = r.nivel_estrutura
                           and r.item_estrutura_str   = c.tipo_cli_for
                           and c.ind_opcao_parametro  = 0)
                           and exists (select 1  from cont_002
                                       where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                                         and cont_002.tipo_cli_for        = 'C'
                                         and cont_002.ind_opcao_parametro = 0
                                         and cont_002.inc_exc             = 2))
       )
      or not exists (select 1  from cont_002
                  where cont_002.codigo_empresa      = p_cod_empresa -- cod_empresa
                    and cont_002.tipo_cli_for        = 'C'
                    and cont_002.ind_opcao_parametro = 0)) -- ou sem cadastro

      and     fatu_070.codigo_empresa        = fatu_075.nr_titul_codempr
      and     fatu_070.cli_dup_cgc_cli9      = fatu_075.nr_titul_cli_dup_cgc_cli9
      and     fatu_070.cli_dup_cgc_cli4      = fatu_075.nr_titul_cli_dup_cgc_cli4
      and     fatu_070.cli_dup_cgc_cli2      = fatu_075.nr_titul_cli_dup_cgc_cli2
      and     fatu_070.tipo_titulo           = fatu_075.nr_titul_cod_tit
      and     fatu_070.num_duplicata         = fatu_075.nr_titul_num_dup
      and     fatu_070.seq_duplicatas        = fatu_075.nr_titul_seq_dup
      and     cont_010.codigo_historico      = fatu_075.historico_pgto
      and     fatu_070.cod_historico         = cont_010_c.codigo_historico
      and     cont_010.tipo_historico       <> 'NC'
      and     cont_010_c.diario              = 1
      and     cont_010.sinal_titulo         in (1,2)
      and   fatu_070.cli_dup_cgc_cli9        = p_cgc9
      and   fatu_070.cli_dup_cgc_cli4        = p_cgc4
      and   fatu_070.cli_dup_cgc_cli2        = p_cgc2;


   v_erro           EXCEPTION;

   v_registro_livro_razao     sped_ctb_i550.registro_livro_razao%type;
   v_per_anterior             cont_500.per_final%type;
   v_data_contab_crec         fatu_500.data_contab_crec%type;
   v_data_movimento           fatu_075.data_pagamento%type;
   v_valor_movimento          fatu_075.valor_pago%type;
   v_valor_movimento_deb      fatu_075.valor_pago%type;
   v_valor_movimento_cre      fatu_075.valor_pago%type;

   v_gerou_capa_livro_cliente number(4);
   v_saldo_inicial_cliente    number(13,2);
   v_saldo_inicial_cliente_livro  number(13,2);
   v_total_pago               number(13,2);
   encontrou_movimento        boolean;

begin
   p_des_erro       := NULL;

   select count(*)
   into   v_gerou_capa_livro_cliente
   from sped_ctb_i012
   where cod_empresa    = p_cod_empresa
   and   exercicio      = p_exercicio
   and   tipo_livro_aux = 'C';

   -- quando executado atravez do programa cont_e455 deve ser eliminado so registro para serem carregados novamente
   if p_nome_programa = 'cont_e455'
   then
      delete from sped_ctb_i550 s
      where s.cod_empresa    = p_cod_empresa
        and s.tipo_livro_aux = 'C'
        and s.nome_programa  = p_nome_programa;
   end if;

   if v_gerou_capa_livro_cliente > 0 or p_nome_programa = 'cont_e455'
   then
      if p_nome_programa = 'cont_f001'
      then

         -- Gera as Informacoes da Definicao do layout do Razao/diario de clientes (SPED_I500)
         inter_pr_gera_ctb_sped_i500 (p_cod_empresa, p_exercicio, 'C', p_des_erro);
         IF  p_des_erro IS NOT NULL
         THEN
            RAISE v_erro;
         END IF;
      end if;

      -- le o parametro de empresa que indica qual a data que e utilizada na contabiizacao das
      -- baixas de titulos (1- data de credito ou 2- data de pagamento)
      begin
         select data_contab_crec
         into   v_data_contab_crec
         from fatu_500
         where codigo_empresa = p_cod_empresa;
      exception
         when others then
            p_des_erro := 'Erro na leitura dos parametros da contabilidade ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;

      ---------------------- INICIO ROTINA DE CALCULO DOS OS SALDOS INICIAIS ---------------------

      -- seta a data do final do exercicio anterior
      v_per_anterior := p_per_inicial - 1;

      -- inicializa saldos anteriioores
      v_saldo_inicial_cliente := 0.00;

      for reg_clientes in clientes
      loop

         encontrou_movimento := false;

         ---------------------- INICIO ROTINA DE CALCULO DAS ENTADAS DOS TITULOS (COLUNA DEBITO) ----

         -- entrada de titulos
         for reg_fatu070 in fatu070(p_cod_empresa, p_per_inicial, p_per_final,reg_clientes.cgc_9,reg_clientes.cgc_4,reg_clientes.cgc_2)
         loop
            encontrou_movimento := true;

            -- monta a linha que vai ser utilizada na exportacao dos dados
            begin
               v_valor_movimento_deb := reg_fatu070.valor_duplicata;
               v_valor_movimento_cre := 0.00;


               v_registro_livro_razao := ltrim(to_char(reg_fatu070.cli_dup_cgc_cli9, '900000000'))  || '/' ||
                                         ltrim(to_char(reg_fatu070.cli_dup_cgc_cli4, '0000'))       || '-' ||
                                         ltrim(to_char(reg_fatu070.cli_dup_cgc_cli2, '00'))         || '|' ||
                                         ltrim(reg_clientes.nome_cliente)                            || '|' ||
                                         ltrim(to_char(reg_fatu070.data_emissao, 'DD/MM/YY'))       || '|' ||
                                         ltrim(reg_fatu070.historico_contab)                        || '|' ||
                                         ltrim(to_char(reg_fatu070.num_duplicata, '000000000'))     || '/' ||
                                         ltrim(to_char(reg_fatu070.seq_duplicatas, '00'))           || '|' ||
                                         ltrim(replace(replace(replace(to_char(v_valor_movimento_deb, '999999990.00'), '.', ';'), ',', '.'), ';', ',')) || '|' ||
                                         ltrim(replace(replace(replace(to_char(v_valor_movimento_cre, '999999990.00'), '.', ';'), ',', '.'), ';', ','));



            exception
               when others then
                  p_des_erro := 'Erro na montagem do registro do Razao de Clientes (3) ' || Chr(10) || SQLERRM;
                  RAISE V_ERRO;
            end;

            begin
               insert into sped_ctb_i550
                 (cod_empresa,                   exercicio,
                  tipo_livro_aux,                codigo_empresa_titulo,
                  numero_titulo,                 parcela_titulo,
                  cgc_9_titulo,                  cgc_4_titulo,
                  cgc_2_titulo,                  tipo_titulo,
                  seq_pagamento,                 nome_cliente_fornec,
                  cod_historico,                 desc_historico,
                  data_movimento,                valor_movimento_deb,
                  valor_movimento_cre,           tabela_origem,
                  registro_livro_razao,          nome_programa,
                  codigo_contabil)
               values
                 (p_cod_empresa,                 p_exercicio,
                  'C',                           reg_fatu070.codigo_empresa,
                  reg_fatu070.num_duplicata,     ltrim(to_char(reg_fatu070.seq_duplicatas, '000')),
                  reg_fatu070.cli_dup_cgc_cli9,  reg_fatu070.cli_dup_cgc_cli4,
                  reg_fatu070.cli_dup_cgc_cli2,  reg_fatu070.tipo_titulo,
                   -1,                           reg_clientes.nome_cliente,
                   reg_fatu070.cod_historico,    reg_fatu070.historico_contab,
                   reg_fatu070.data_emissao,     v_valor_movimento_deb,
                   v_valor_movimento_cre,        'FATU_070',
                   v_registro_livro_razao,        p_nome_programa,
                   reg_clientes.codigo_contabil);

            exception
               when others then
                  p_des_erro := 'Erro na inclusao da tabela sped_ctb_i550 (3) ' || Chr(10) || SQLERRM;
                  RAISE V_ERRO;
            end;
            commit;

         end loop;

         ---------------------- FIM DA ROTINA DE CALCULO DAS ENTADAS DOS TITULOS (COLUNA DEBITO) ----


         ---------------------- INICIO ROTINA DE CALCULO DOS PAGAMENTOS DOS TITULOS (COLUNAS CREDITO E DEBITO) ----

         -- baixa de titulos
         for reg_fatu075 in fatu075(p_cod_empresa, p_per_inicial, p_per_final, v_data_contab_crec,reg_clientes.cgc_9,reg_clientes.cgc_4,reg_clientes.cgc_2)
         loop
            encontrou_movimento := true;

            -- encontra a data de contabilizacao, conforme parametro no cadastro de empresa
            if v_data_contab_crec = 1
            then
               v_data_movimento := reg_fatu075.data_credito;
            else
               v_data_movimento := reg_fatu075.data_pagamento;
            end if;

            -- encontra o valor liquido do movimento (valor contabil do movimento)
            v_valor_movimento := reg_fatu075.valor_pago - reg_fatu075.valor_juros + reg_fatu075.valor_descontos - reg_fatu075.vlr_var_cambial ;

            -- se o valor for zero, nao exporta o lancamento
            if v_valor_movimento <> 0.00
            then

               -- seta o valor para as colunas corretas (debito/credito), sendo:
               -- se for baixa de titulo e o valor for positivo e porque se esta baixando um titulo e tem valor pago
               -- ou se for estorno, mas o valor e negativo, quer dizer que e um estorno de provisao de juros.
               -- nestes dois casos, esta se subtraindo o valor da conta de clientes, ou seja, se esta gerando um
               -- credito.
               -- Caso contrario esta se aumentando o valor da conta de clientes, ou seja, esta gerando um debito.

               if (reg_fatu075.sinal_titulo = 1 and v_valor_movimento > 0.00)
               or (reg_fatu075.sinal_titulo = 2 and v_valor_movimento < 0.00)
               then
                  v_valor_movimento_deb := 0.00;
                  v_valor_movimento_cre := v_valor_movimento;
               else
                  v_valor_movimento_deb := v_valor_movimento;
                  v_valor_movimento_cre := 0.00;
               end if;

               -- transforma o valor em positivo, no caso de o valor de juros for maior que o do valor pago
               if v_valor_movimento_deb < 0.00
               then
                  v_valor_movimento_deb := v_valor_movimento_deb * (-1.00);
               end if;

               -- transforma o valor em positivo, no caso de o valor de juros for maior que o do valor pago
               if v_valor_movimento_cre < 0.00
               then
                  v_valor_movimento_cre := v_valor_movimento_cre * (-1.00);
               end if;


               -- monta a linha que vai ser utilizada na exportacao dos dados
               begin
                  v_registro_livro_razao := ltrim(to_char(reg_fatu075.cli_dup_cgc_cli9, '900000000'))  || '/' ||
                                            ltrim(to_char(reg_fatu075.cli_dup_cgc_cli4, '0000'))       || '-' ||
                                            ltrim(to_char(reg_fatu075.cli_dup_cgc_cli2, '00'))         || '|' ||
                                            ltrim(reg_clientes.nome_cliente)                            || '|' ||
                                            ltrim(to_char(v_data_movimento, 'DD/MM/YY'))               || '|' ||
                                            ltrim(reg_fatu075.historico_contab)                        || '|' ||
                                            ltrim(to_char(reg_fatu075.num_duplicata, '000000000'))     || '/' ||
                                            ltrim(to_char(reg_fatu075.seq_duplicatas, '00'))           || '|' ||
                                            ltrim(replace(replace(replace(to_char(v_valor_movimento_deb, '999999990.00'), '.', ';'), ',', '.'), ';', ',')) || '|' ||
                                            ltrim(replace(replace(replace(to_char(v_valor_movimento_cre, '999999990.00'), '.', ';'), ',', '.'), ';', ','));

               exception
                  when others then
                     p_des_erro := 'Erro na montagem do registro do Razao de Clientes (4) ' || Chr(10) || SQLERRM;
                     RAISE V_ERRO;
               end;

               begin
                  insert into sped_ctb_i550
                    (cod_empresa,                   exercicio,
                     tipo_livro_aux,                codigo_empresa_titulo,
                     numero_titulo,                 parcela_titulo,
                     cgc_9_titulo,                  cgc_4_titulo,
                     cgc_2_titulo,                  tipo_titulo,
                     seq_pagamento,                 nome_cliente_fornec,
                     cod_historico,                 desc_historico,
                     data_movimento,                valor_movimento_deb,
                     valor_movimento_cre,           tabela_origem,
                     registro_livro_razao,          nome_programa,
                     codigo_contabil)
                  values
                    (p_cod_empresa,                 p_exercicio,
                     'C',                           reg_fatu075.codigo_empresa,
                     reg_fatu075.num_duplicata,     ltrim(to_char(reg_fatu075.seq_duplicatas, '000')),
                     reg_fatu075.cli_dup_cgc_cli9,  reg_fatu075.cli_dup_cgc_cli4,
                     reg_fatu075.cli_dup_cgc_cli2,  reg_fatu075.tipo_titulo,
                     reg_fatu075.seq_pagamento,     reg_clientes.nome_cliente,
                     reg_fatu075.historico_pgto,    reg_fatu075.historico_contab,
                     v_data_movimento,              v_valor_movimento_deb,
                     v_valor_movimento_cre,         'FATU_075',
                     v_registro_livro_razao,        p_nome_programa,
                     reg_clientes.codigo_contabil);

               exception
                  when others then
                     p_des_erro := 'Erro na inclusao da tabela sped_ctb_i550 (4) ' || Chr(10) ||
                     to_char(reg_fatu075.num_duplicata,'000000000') || Chr(10) ||
                     to_char(reg_fatu075.seq_duplicatas,'000') || Chr(10) ||
                     to_char(reg_fatu075.seq_pagamento,'000') || Chr(10) ||
                     to_char(reg_fatu075.tipo_titulo,'000') || Chr(10) ||
                     to_char(reg_fatu075.cli_dup_cgc_cli9,'000000000000') || Chr(10) ||
                     to_char(reg_fatu075.cli_dup_cgc_cli4,'0000') || Chr(10) ||
                     to_char(reg_fatu075.cli_dup_cgc_cli2,'00') || Chr(10) ||
                     p_nome_programa || Chr(10) ||
                     SQLERRM;
                     RAISE V_ERRO;
               end;
               commit;
            end if;
         end loop;

         ---------------------- FIM DA ROTINA DE CALCULO DOS PAGAMENTOS DOS TITULOS (COLUNAS CREDITO E DEBITO) ----

         if p_nome_programa = 'cont_e455'
         then
               v_saldo_inicial_cliente_livro := 0.00;
         end if;


         for reg_saldos in saldos(p_cod_empresa, p_per_inicial, reg_clientes.cgc_9,reg_clientes.cgc_4,reg_clientes.cgc_2)
         loop

            -- le a tabela de pagamentos para encontrar o saldos da duplicata
            begin
               select nvl(sum(decode(cont_010.sinal_titulo, 1, (fatu_075.valor_pago - fatu_075.valor_juros + fatu_075.valor_descontos - fatu_075.vlr_var_cambial),
                                                                       (fatu_075.valor_pago - fatu_075.valor_juros + fatu_075.valor_descontos - fatu_075.vlr_var_cambial) * (-1.0))),0.00)
               into   v_total_pago
               from fatu_075, cont_010
               where fatu_075.nr_titul_codempr          = reg_saldos.codigo_empresa
               and   fatu_075.nr_titul_num_dup          = reg_saldos.num_duplicata
               and   fatu_075.nr_titul_seq_dup          = reg_saldos.seq_duplicatas
               and   fatu_075.nr_titul_cli_dup_cgc_cli9 = reg_saldos.cli_dup_cgc_cli9
               and   fatu_075.nr_titul_cli_dup_cgc_cli4 = reg_saldos.cli_dup_cgc_cli4
               and   fatu_075.nr_titul_cli_dup_cgc_cli2 = reg_saldos.cli_dup_cgc_cli2
               and   fatu_075.nr_titul_cod_tit          = reg_saldos.tipo_titulo
               and ((fatu_075.data_credito              < p_per_inicial
               and   v_data_contab_crec                 = 1)
               or   (fatu_075.data_pagamento            < p_per_inicial
               and   v_data_contab_crec                 = 2))
               and   cont_010.codigo_historico          = fatu_075.historico_pgto
               and   cont_010.sinal_titulo             in (1,2);

            exception
               when no_data_found then
                  v_total_pago := 0.00;
            end;

            -- acumula valores do cliente
            v_saldo_inicial_cliente := v_saldo_inicial_cliente + (reg_saldos.valor_duplicata - v_total_pago);
            v_saldo_inicial_cliente_livro := v_saldo_inicial_cliente_livro + (reg_saldos.valor_duplicata - v_total_pago);
         end loop;

         if p_nome_programa = 'cont_e455' and (encontrou_movimento or v_saldo_inicial_cliente_livro <> 0.00)
         then
            if v_saldo_inicial_cliente_livro > 0.00
            then
               v_valor_movimento_deb := v_saldo_inicial_cliente_livro;
               v_valor_movimento_cre := 0.00;
            else
               v_valor_movimento_deb := 0.00;
               v_valor_movimento_cre := v_saldo_inicial_cliente_livro * (-1.0);
            end if;

            -- grava os dados do saldo inicial do cliente
            begin
               insert into sped_ctb_i550
                 (cod_empresa,                   exercicio,
                  tipo_livro_aux,                codigo_empresa_titulo,
                  numero_titulo,                 parcela_titulo,

                  cgc_9_titulo,                  cgc_4_titulo,
                  cgc_2_titulo,                  tipo_titulo,
                  seq_pagamento,                 nome_cliente_fornec,

                  cod_historico,                 desc_historico,
                  data_movimento,                valor_movimento_deb,
                  valor_movimento_cre,           tabela_origem,
                  registro_livro_razao,          codigo_contabil,
                  nome_programa)
               values
                 (p_cod_empresa,                 p_exercicio,
                  'C',                           0,
                  0,                             ' ',

                  reg_clientes.cgc_9,            reg_clientes.cgc_4,
                  reg_clientes.cgc_2,            0,
                  -1,                            reg_clientes.nome_cliente,

                  0,                             'SALDO INICIAL',
                  v_per_anterior,                v_valor_movimento_deb,
                  v_valor_movimento_cre,         'SALDO',
                  v_registro_livro_razao,        reg_clientes.codigo_contabil,
                  p_nome_programa);

            exception
               when others then
                  p_des_erro := 'Erro na montagem do registro dos Saldos Iniciais do Razao de Clientes (2) ' || Chr(10) || SQLERRM;
                  RAISE V_ERRO;
            end;
            commit;
         end if;


      end loop; -- FIM LOOP CLIENTES



      -- grava os dados do ultimo cliente lido
      -- monta a linha que vai ser utilizada na exportacao dos dados
      begin
         if v_saldo_inicial_cliente > 0.00
         then
            v_valor_movimento_deb := v_saldo_inicial_cliente;
            v_valor_movimento_cre := 0.00;
         else
            v_valor_movimento_deb := 0.00;
            v_valor_movimento_cre := v_saldo_inicial_cliente * (-1.0);
         end if;

         if p_nome_programa = 'cont_f001'
         then

            v_registro_livro_razao := 'SALDO INICIAL:'                         || '|' ||  -- CNPJ
                                      ' '                                      || '|' ||  -- NOME DO CLIENTE
                                      to_char(v_per_anterior, 'DD/MM/YY')      || '|' ||  -- DATA DO MOVIMENTO
                                      ' '                                      || '|' ||  -- HISTORICO
                                      ' '                                      || '|' ||  -- NUMERO_DOCUMENTO
                                      ltrim(replace(replace(replace(to_char(v_valor_movimento_deb, '999999990.00'), '.', ';'), ',', '.'), ';', ',')) || '|' ||
                                      ltrim(replace(replace(replace(to_char(v_valor_movimento_cre, '999999990.00'), '.', ';'), ',', '.'), ';', ','));
         end if;
      exception
         when others then
            p_des_erro := 'Erro na montagem do registro de saldos inciciais de Clientes (1) ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;

      -- grava os dados do saldo inicial do cliente
      begin
         insert into sped_ctb_i550
           (cod_empresa,                   exercicio,
            tipo_livro_aux,                codigo_empresa_titulo,
            numero_titulo,                 parcela_titulo,
            cgc_9_titulo,                  cgc_4_titulo,
            cgc_2_titulo,                  tipo_titulo,
            seq_pagamento,                 nome_cliente_fornec,
            cod_historico,                 desc_historico,
            data_movimento,                valor_movimento_deb,
            valor_movimento_cre,           tabela_origem,
            registro_livro_razao,          nome_programa)
         values
           (p_cod_empresa,                 p_exercicio,
            'C',                           0,
            0,                             ' ',
            0,                             0,
            0,                             0,
            0,                             'SALDO INICIAL',
            0,                             'SALDO INICIAL',
            v_per_anterior,                v_valor_movimento_deb,
            v_valor_movimento_cre,         'SALDO',
            v_registro_livro_razao,        p_nome_programa);

      exception
         when others then
            p_des_erro := 'Erro na montagem do registro dos Saldos Iniciais do Razao de Clientes (2) ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;
      commit;


      ---------------------- FIM DA ROTINA DE CALCULO DOS OS SALDOS INICIAIS ---------------------
   end if;

   commit;

exception
   when V_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_cli ' || Chr(10) || p_des_erro;

   when others then
      p_des_erro := 'Outros erros na procedure p_gera_sped_cli ' || Chr(10) || SQLERRM;

end inter_pr_gera_ctb_sped_cli;
 

/

exec inter_pr_recompile;

