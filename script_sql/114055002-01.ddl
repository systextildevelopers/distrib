create table oper_794 (
	company_cad			varchar2(15),
	usuario_cnp		 	varchar2(60),
	senha_cnp 			varchar2(15),
	client_id 			varchar2(50),
	client_secret 		varchar2(50),
	ambiente 			number(1)
);

alter table oper_794 add constraint oper_794_pk unique (company_cad);

alter table oper_794 add constraint oper_794_ambiente_ck check (ambiente in (0,1));
