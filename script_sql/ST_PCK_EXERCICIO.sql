create or replace package ST_PCK_EXERCICIO as

    type t_dados_exercicio is table of cont_500%rowtype;

    function get_exercicio(p_codigo_empresa number, p_codigo_exercicio number, p_msg_erro in out varchar2) return t_dados_exercicio;
        
    function get_exercicio_by_date(p_codigo_empresa number, p_data Date, p_msg_erro in out varchar2) return t_dados_exercicio;

     FUNCTION valida_exercicio(p_empresa_matriz NUMBER, p_data_lac DATE, p_regras ST_PCK_CHECA_DATA.t_regras, p_msg_error in out varchar2) RETURN ST_PCK_EXERCICIO.t_dados_exercicio;

end ST_PCK_EXERCICIO;
