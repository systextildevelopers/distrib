create or replace trigger inter_tr_pcpt_010_1
   before delete or
         update of cod_cancelamento
   on pcpt_010
   for each row
declare

   v_qtde_prod_pedido number;
   v_qtde_negativa    number;

begin
   -- Esta trigger far� executar� com base no pedido de compra a atualiza��o
   -- das quantidades programadas na ordem de planejamento (tmrp_630).
   -- Estas atualiza��es ir�o acontecer quando cancelado um pedido ou excluido
   -- por este motivo n�o temos INSERT.
   if updating and :new.cod_cancelamento <> 0 and :new.cod_cancelamento <> :old.cod_cancelamento
   then

      for reg_tmrp in (select tmrp_630.ordem_planejamento,            tmrp_630.pedido_venda,
                              tmrp_630.nivel_produto,                 tmrp_630.grupo_produto,
                              tmrp_630.subgrupo_produto,              tmrp_630.item_produto,
                              tmrp_625.seq_produto_origem,            tmrp_625.nivel_produto_origem,
                              tmrp_625.grupo_produto_origem,          tmrp_625.subgrupo_produto_origem,
                              tmrp_625.item_produto_origem,           tmrp_625.alternativa_produto_origem,
                              tmrp_625.seq_produto,                   tmrp_625.qtde_areceber_programada,
                              tmrp_625.qtde_reserva_programada,       tmrp_630.ordem_prod_compra
                       from tmrp_630, tmrp_625
                       where tmrp_630.ordem_planejamento      = tmrp_625.ordem_planejamento
                         and tmrp_630.nivel_produto           = tmrp_625.nivel_produto
                         and tmrp_630.grupo_produto           = tmrp_625.grupo_produto
                         and tmrp_630.subgrupo_produto        = tmrp_625.subgrupo_produto
                         and tmrp_630.item_produto            = tmrp_625.item_produto
                         and tmrp_630.nivel_produto           = '4'
                         and tmrp_630.grupo_produto           = :new.cd_pano_grupo
                         and tmrp_630.subgrupo_produto        = :new.cd_pano_subgrupo
                         and tmrp_630.item_produto            = :new.cd_pano_item
                         and tmrp_630.area_producao           = 4 --Beneficiamento
                         and tmrp_630.ordem_prod_compra       = :new.ordem_tecelagem)
      loop 
         -- A quantidade a receber do produto principal
         v_qtde_prod_pedido := 0.00;
         
         begin
            select sum(tmrp_041_data.qtde_areceber)
            into v_qtde_prod_pedido
            from tmrp_041_data
            where tmrp_041_data.area_producao   = 4
              and tmrp_041_data.nr_pedido_ordem = reg_tmrp.ordem_prod_compra
              and tmrp_041_data.nivel_prod      = reg_tmrp.nivel_produto
              and tmrp_041_data.grupo_prod      = reg_tmrp.grupo_produto
              and tmrp_041_data.subgrupo_prod   = reg_tmrp.subgrupo_produto
              and tmrp_041_data.item_prod       = reg_tmrp.item_produto;
         exception
         when OTHERS then
            v_qtde_prod_pedido := 0.00;
         end;
         
         v_qtde_negativa := (reg_tmrp.qtde_areceber_programada - v_qtde_prod_pedido);
         
         if v_qtde_negativa < 0.00
         then 
            v_qtde_prod_pedido := reg_tmrp.qtde_areceber_programada;
         end if;
         
         inter_pr_atu_planej_reves(reg_tmrp.ordem_planejamento,
                                   reg_tmrp.pedido_venda,            reg_tmrp.nivel_produto,
                                   reg_tmrp.grupo_produto,           reg_tmrp.subgrupo_produto,
                                   reg_tmrp.item_produto,            v_qtde_prod_pedido,
                                   reg_tmrp.qtde_reserva_programada,
                                   reg_tmrp.nivel_produto_origem,    reg_tmrp.grupo_produto_origem,
                                   reg_tmrp.subgrupo_produto_origem, reg_tmrp.item_produto_origem,
                                   reg_tmrp.seq_produto_origem,      reg_tmrp.alternativa_produto_origem,
                                   reg_tmrp.seq_produto,             'pcpt_010',
                                   :new.ordem_tecelagem);
         
         -- Ap�s atualiza��o de todos os componentes do produto ir� deletar o link entre
         -- o Systextil e as ordens de planejamento (tmrp_630)
         begin
            delete from tmrp_630
            where tmrp_630.ordem_planejamento = reg_tmrp.ordem_planejamento
              and tmrp_630.area_producao      = 4
              and tmrp_630.ordem_prod_compra  = :new.ordem_tecelagem
              and tmrp_630.nivel_produto      = reg_tmrp.nivel_produto
              and tmrp_630.grupo_produto      = reg_tmrp.grupo_produto
              and tmrp_630.subgrupo_produto   = reg_tmrp.subgrupo_produto
              and tmrp_630.item_produto       = reg_tmrp.item_produto;
         exception
         when OTHERS then
            raise_application_error(-20000,'N�o excluiu tmpr_630');
         end;
      end loop;
   end if;

   if deleting
   then
      for reg_tmrp in (select tmrp_630.ordem_planejamento,            tmrp_630.pedido_venda,
                              tmrp_630.nivel_produto,                 tmrp_630.grupo_produto,
                              tmrp_630.subgrupo_produto,              tmrp_630.item_produto,
                              tmrp_625.seq_produto_origem,            tmrp_625.nivel_produto_origem,
                              tmrp_625.grupo_produto_origem,          tmrp_625.subgrupo_produto_origem,
                              tmrp_625.item_produto_origem,           tmrp_625.alternativa_produto_origem,
                              tmrp_625.seq_produto,                   tmrp_625.qtde_areceber_programada,
                              tmrp_625.qtde_reserva_programada,       tmrp_630.ordem_prod_compra
                       from tmrp_630, tmrp_625
                       where tmrp_630.ordem_planejamento      = tmrp_625.ordem_planejamento
                         and tmrp_630.nivel_produto           = tmrp_625.nivel_produto
                         and tmrp_630.grupo_produto           = tmrp_625.grupo_produto
                         and tmrp_630.subgrupo_produto        = tmrp_625.subgrupo_produto
                         and tmrp_630.item_produto            = tmrp_625.item_produto
                         and tmrp_630.nivel_produto           = '4'
                         and tmrp_630.grupo_produto           = :old.cd_pano_grupo
                         and tmrp_630.subgrupo_produto        = :old.cd_pano_subgrupo
                         and tmrp_630.item_produto            = :old.cd_pano_item
                         and tmrp_630.area_producao           = 4 --Beneficiamento
                         and tmrp_630.ordem_prod_compra       = :old.ordem_tecelagem)
      loop
         v_qtde_prod_pedido := 0.00;
         
         begin
            select sum(tmrp_041_data.qtde_areceber)
            into v_qtde_prod_pedido
            from tmrp_041_data
            where tmrp_041_data.area_producao   = 4
              and tmrp_041_data.nr_pedido_ordem = reg_tmrp.ordem_prod_compra
              and tmrp_041_data.nivel_prod      = reg_tmrp.nivel_produto
              and tmrp_041_data.grupo_prod      = reg_tmrp.grupo_produto
              and tmrp_041_data.subgrupo_prod   = reg_tmrp.subgrupo_produto
              and tmrp_041_data.item_prod       = reg_tmrp.item_produto;
         exception
         when OTHERS then
            v_qtde_prod_pedido := 0.00;
         end;
         
         v_qtde_negativa := (reg_tmrp.qtde_areceber_programada - v_qtde_prod_pedido);
         
         if v_qtde_negativa < 0.00
         then 
            v_qtde_prod_pedido := reg_tmrp.qtde_areceber_programada;
         end if;
         
         inter_pr_atu_planej_reves( reg_tmrp.ordem_planejamento,
                                    reg_tmrp.pedido_venda,            reg_tmrp.nivel_produto,
                                   reg_tmrp.grupo_produto,           reg_tmrp.subgrupo_produto,
                                    reg_tmrp.item_produto,            (:old.qtde_quilos_prog - :old.qtde_quilos_prod),
                                    0.00,
                                    reg_tmrp.nivel_produto_origem,    reg_tmrp.grupo_produto_origem,
                                    reg_tmrp.subgrupo_produto_origem, reg_tmrp.item_produto_origem,
                                    reg_tmrp.seq_produto_origem,      reg_tmrp.alternativa_produto_origem,
                                    reg_tmrp.seq_produto,             'pcpt_010',
                                    :old.ordem_tecelagem);
                                    
         -- Ap�s atualiza��o de todos os componentes do produto ir� deletar o link entre
         -- o Systextil e as ordens de planejamento (tmrp_630)
         begin
            delete from tmrp_630
            where tmrp_630.ordem_planejamento = reg_tmrp.ordem_planejamento
              and tmrp_630.area_producao      = 4
              and tmrp_630.ordem_prod_compra  = :old.ordem_tecelagem
              and tmrp_630.nivel_produto      = reg_tmrp.nivel_produto
              and tmrp_630.grupo_produto      = reg_tmrp.grupo_produto
              and tmrp_630.subgrupo_produto   = reg_tmrp.subgrupo_produto
              and tmrp_630.item_produto       = reg_tmrp.item_produto;
         exception
         when OTHERS then
            raise_application_error(-20000,'N�o excluiu tmpr_630');
         end;
      end loop;
   end if;
end;

/

execute inter_pr_recompile;
/* versao: 2 */
