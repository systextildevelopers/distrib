
  CREATE OR REPLACE TRIGGER "INTER_TR_HIST_100" 
before insert on hist_100
  for each row

declare
   v_nr_registro   number;
   v_nome_programa hist_100.programa%type;
   v_sid           number;
   v_osuser        varchar2(20);
   v_nome_usuario  hist_100.usuario%type;

   ws_empresa      number;
   ws_maquina_rede varchar(40);
   ws_aplicativo   varchar(40);
   ws_locale_usuario varchar(20);

begin
   begin
      select seq_hist_100.nextval
      into v_nr_registro
      from dual;
   end;

   :new.sequencia := v_nr_registro;

   -- Dados do usuario logado
   if :new.tabela <> 'HDOC_030'
   then
      inter_pr_dados_usuario (v_osuser,        ws_maquina_rede,  ws_aplicativo,     v_sid,
                              v_nome_usuario,  ws_empresa,       ws_locale_usuario);

      v_nome_programa := inter_fn_nome_programa(v_sid); 

      :new.programa := v_nome_programa;
      :new.usuario  := v_nome_usuario;
      :new.data_ocorr := sysdate;
   end if;
   
end inter_tr_hist_100;

-- ALTER TRIGGER "INTER_TR_HIST_100" ENABLE
 

/

exec inter_pr_recompile;

