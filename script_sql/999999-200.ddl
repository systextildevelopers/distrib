create table OBRF_108
(
  num_nota       NUMBER(9) default 0 not null,
  serie          VARCHAR2(3) default '' not null,
  empresa        NUMBER(3) default 0 not null,
  cod_barras_ean VARCHAR2(47) default '' not null,
  nivel          VARCHAR2(1) default '',
  grupo          VARCHAR2(5) default '',
  subgrupo       VARCHAR2(3) default '',
  item           VARCHAR2(6) default '',
  num_pedido     NUMBER(9) default 0,
  num_danfe      VARCHAR2(44) default '',
  qtde_lida      NUMBER(9) default 0,
  usuario_logado VARCHAR2(15) default '',
  data_leitura   DATE
 );
 
 alter table OBRF_108
  add constraint PK_OBRF_108 primary key (EMPRESA, NUM_NOTA, SERIE, COD_BARRAS_EAN);
  
 exec inter_pr_recompile;
 /
