declare 

begin
  for i in 1..4 loop
    for progUser in (
      select hdoc_033.programa, hdoc_033.usu_prg_cdusu, hdoc_033.usu_prg_empr_usu, count(1) from hdoc_033, hdoc_035
      where /*hdoc_033.programa        = 'pcpb_f115'
        and */hdoc_035.codigo_programa = hdoc_033.programa
        and hdoc_035.item_menu_def   = 0
        and (hdoc_033.item_menu      <> 0 or hdoc_033.nome_menu = 'NENHUM')
      group by hdoc_033.programa, hdoc_033.usu_prg_cdusu, hdoc_033.usu_prg_empr_usu
      having count(1) > 1)
    loop
      delete from hdoc_033
      where hdoc_033.usu_prg_cdusu    = progUser.Usu_Prg_Cdusu
        and hdoc_033.usu_prg_empr_usu = progUser.Usu_Prg_Empr_Usu
        and hdoc_033.programa         = progUser.Programa
        and rownum                    = 1; 
    end loop;
	commit;
  end loop;
end;
