
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_020_1"

  -- TRIGGER PARA CONTROLE DE ETIQUETAS PARA HOMOLOGACAO
  before insert or
         delete or
         update of pedido_venda, seq_item_pedido on pcpt_020
  for each row


declare
   -- Identifica de adiciona ou remove etiqueta.
   -- A -> Adiciona / R -> Remove
   t_controle_etiqueta     varchar2(1);

   v_executa_trigger      number;

begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      if updating
      then
         -- Identifica se e estorno, desalocando rolo do Pedido de Venda
         if  :old.pedido_venda    > 0 and :new.pedido_venda    = 0
         and :old.seq_item_pedido > 0 and :new.seq_item_pedido = 0
         then
            t_controle_etiqueta := 'R';

            inter_pr_atualiza_homologacao(:old.panoacab_nivel99,
                                          :old.panoacab_grupo,
                                          :old.panoacab_subgrupo,
                                          :old.panoacab_item,
                                          :old.pedido_venda,
                                          t_controle_etiqueta,
                                          :old.qtde_quilos_acab);
         end if;

         -- Identifica se esta alocando o rolo ao Pedido de Venda
         if  :old.pedido_venda    = 0 and :new.pedido_venda    > 0
         and :old.seq_item_pedido = 0 and :new.seq_item_pedido > 0
         then
            t_controle_etiqueta := 'A';

            inter_pr_atualiza_homologacao(:new.panoacab_nivel99,
                                          :new.panoacab_grupo,
                                          :new.panoacab_subgrupo,
                                          :new.panoacab_item,
                                          :new.pedido_venda,
                                          t_controle_etiqueta,
                                          :new.qtde_quilos_acab);
         end if;
      end if;

      if inserting
      then
         if :new.panoacab_nivel99 = '2'
         then
           if :new.hora_termino is null then
             :new.hora_termino := to_date(to_char(sysdate,'DD/MM/YYYY') || ' ' || to_char(sysdate,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS');
           end if;
         end if;

         -- Identifica se esta alocando o rolo ao Pedido de Venda
         if  :new.pedido_venda    > 0
         and :new.seq_item_pedido >  0
         then
            t_controle_etiqueta := 'A';

            inter_pr_atualiza_homologacao(:new.panoacab_nivel99,
                                          :new.panoacab_grupo,
                                          :new.panoacab_subgrupo,
                                          :new.panoacab_item,
                                          :new.pedido_venda,
                                          t_controle_etiqueta,
                                          :new.qtde_quilos_acab);
         end if;
      end if;

   end if;
end;

-- ALTER TRIGGER "INTER_TR_PCPT_020_1" ENABLE


/

exec inter_pr_recompile;
