CREATE TABLE CONT_321
( COD_EMPRESA            NUMBER(3) not null,
  COD_EXERCICIO          NUMBER(4) not null,
  IND_REPES              VARCHAR2(1) default 'N',
  IND_RECAP              VARCHAR2(1) default 'N',
  IND_PADIS              VARCHAR2(1) default 'N',
  IND_PATVD              VARCHAR2(1) default 'N',
  IND_REIDI              VARCHAR2(1) default 'N',
  IND_REPENEC            VARCHAR2(1) default 'N',
  IND_REICOMP            VARCHAR2(1) default 'N',
  IND_RETAERO            VARCHAR2(1) default 'N',
  IND_RECINE             VARCHAR2(1) default 'N',
  IND_RESIDUOS_SOLID     VARCHAR2(1) default 'N',
  IND_RECOPA             VARCHAR2(1) default 'N',
  IND_COPA_DO_MUND       VARCHAR2(1) default 'N',
  IND_RETID              VARCHAR2(1) default 'N',
  IND_REPNBL_REDES       VARCHAR2(1) default 'N',
  IND_REIF               VARCHAR2(1) default 'N',
  IND_OLIMPIADAS         VARCHAR2(1) default 'N'
);
ALTER TABLE CONT_321 ADD CONSTRAINT PK_CONT_321 PRIMARY KEY (COD_EMPRESA,COD_EXERCICIO );
  
COMMENT ON TABLE CONT_321  is 'Parametros para bloco 21 do sped contabil';  
COMMENT ON COLUMN CONT_321.COD_EMPRESA   is 'Codigo da empresa correspondente ao valor deste parametro';
COMMENT ON COLUMN CONT_321.COD_EXERCICIO is 'Exercicio correspondente ao valor deste parametro';
COMMENT ON COLUMN CONT_321.IND_REPES     is '02 Regime Especial de Tributacao para a Plataforma de Exportacao de Servicos de Tecnologia da Informacao (Repes): S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_RECAP     is '03 Regime Especial de Aquisicao de Bens de Capital para Empresas Exportadoras (Recap):S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_PADIS     is '04 Programa de Apoio ao Desenvolvimento Tecnologico da Industria de Semicondutores (Padis):S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_PATVD     is '05 Programa de Apoio ao Desenvolvimento Tecnologico da Industria de Equipamentos para TV Digital (PATVD):S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_REIDI     is '06 Regime Especial de Incentivos para o Desenvolvimento da IncInfraestrutura (Reidi):S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_REPENEC   is '07 Regime Especial de Incentivos para o Desenvolvimento da Infraestrutura da Industria Petrolifera das Regioes Norte, Nordeste e Centro-Oeste (Repenec): S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_REICOMP   is '08 Regime Especial de Incentivo a Computadores para Uso Educacional(Reicomp):S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_RETAERO            is '09 Regime Especial para a Industria Aeronautica Brasileira (Retaero):S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_RECINE             is '10 Regime Especial de Tributacao para Desenvolvimento da Atividade de Exibição Cinematográfica (Recine):S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_RESIDUOS_SOLID     is '11 Estabelecimentos industriais facam jus a credito presumido do IPI na aquisicao de residuos solidos, de que trata a Lei n 12.375, de 30 de dezembro 2010:S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_RECOPA             is '12 Regime Especial de Tributacao para construcao, ampliacao, reforma ou modernizacao de estadios de futebol (Recopa):S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_COPA_DO_MUND       is '13 Habilitada para fins de fruicao dos beneficios fiscais, nao abrangidos na alinea anterior, relativos a realizacao, no Brasil, da Copa das Confederacoes FIFA 2013 e da Copa do Mundo FIFA 2014, de que trata a Lei n 12.350, de 2010, regulamentada pelo Decreto n 7.578, e 11 de outubro de 2011:S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_RETID              is '14 Regime Especial Tributario para a Industria de Defesa (Retid):S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_REPNBL_REDES       is '15 Regime Especial de Tributacao do Programa Nacional de Banda Larga para Implantação de Redes de Telecomunicacoes (REPNBL-Redes) S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_REIF               is ' 16 Regime Especial de Incentivo ao Desenvolvimento da Infraestrutura da Industria de Fertilizantes (REIF): S - Sim N - Nao';
COMMENT ON COLUMN CONT_321.IND_OLIMPIADAS        is '17 Habilitada para fins de fruicao dos beneficios fiscais, relativos a realizacao, no Brasil, dos Jogos Olimpicos de 2016 e dos Jogos Paraolimpicos de 2016, de que trata a Lei n 12.780, de 2013: S - Sim N - Nao';
