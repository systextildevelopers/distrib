create table fndc_002
( COD_EMPRESA             NUMBER(3)    default 0 not null,
 NR_ATRIBUTO              NUMBER(8)    default 0 not null,
 OCORPGTO                NUMBER(2)    default 0,
 NR_REMESSA               NUMBER(9)    default 0,
 STATUS                   NUMBER(1)    default 0,
 ENVIA_NR_DANFE           NUMBER(1)    default 0
);

comment on table fndc_002 is 'Cadastro de atributos do Fundo Fidic. Somente 1 atributo de empresa pode ficar ativo';

comment on column fndc_002.COD_EMPRESA   is 'C�digo da empresa no sistema';
 
comment on column fndc_002.NR_ATRIBUTO  is 'Nr do atributo banc�rio no Fidic';

comment on column fndc_002.OCORPGTO     is 'Ocorr�ncia do pagamento para o Fidic';

comment on column fndc_002.NR_REMESSA    is 'Contador do N�mero da remessa para Fidic';

comment on column fndc_002.STATUS    is 'Status do processo 0 - INATIVO ou 1 ATIVO - somente 1 atributo de empresa pode ficar ativo';

comment on column fndc_002.ENVIA_NR_DANFE    is 'Envia nr DANFE 0 - N�O 1 - SIM';

ALTER TABLE fndc_002 ADD CONSTRAINT pk_fndc_002 PRIMARY KEY (COD_EMPRESA, NR_ATRIBUTO);
