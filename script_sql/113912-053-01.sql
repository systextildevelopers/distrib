alter table rcnb_750 add data_exe_processo date;
alter table rcnb_755 add data_exe_processo date;

alter table rcnb_750 add id_execucao varchar2(20);
alter table rcnb_755 add id_execucao varchar2(20);

alter table rcnb_750 modify id_execucao default 0;
alter table rcnb_755 modify id_execucao default 0;

exec inter_pr_recompile;
