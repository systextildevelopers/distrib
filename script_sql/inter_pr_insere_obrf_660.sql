create or replace procedure inter_pr_insere_obrf_660 (
  p_codigo_empresa         in number,   p_ano                     in number,
  p_mes                    in number,   p_nivel_produto           in varchar2,
  p_grupo_produto          in varchar2, p_sub_produto             in varchar2,
  p_item_produto           in varchar2, p_alter_produto           in number,
  p_nivel_comp             in varchar2, p_grupo_comp              in varchar2,
  p_sub_comp               in varchar2, p_item_comp               in varchar2,
  p_alter_comp             in number,   p_consumo                 in number,
  p_valor_compras_custo    in number,   p_valor_compras_import    in number,
  p_valor_consumo_custo    in number,   p_valor_consumo_import    in number,
  p_valor_venda            in number,   p_perc_importado          in number,
  p_perc_venda             in number,   p_perc_qtde_venda         in number) is
begin
  insert into obrf_660
    (codigo_empresa,           ano,
     mes,
     nivel_produto,            grupo_produto,
     sub_produto,              item_produto,
     alter_produto,
     nivel_comp,               grupo_comp,
     sub_comp,                 item_comp,
     alter_comp,               consumo,

     valor_compras_custo,      valor_compras_import,
     valor_consumo_custo,      valor_consumo_import,
     valor_venda,              perc_importado,
     percentual_valor_venda,   percentual_qtde_venda)
  values
    (p_codigo_empresa, p_ano,
     p_mes,

     p_nivel_produto,          p_grupo_produto,
     p_sub_produto,            p_item_produto,
     p_alter_produto,

     p_nivel_comp,             p_grupo_comp,
     p_sub_comp,               p_item_comp,
     p_alter_comp,             p_consumo,

     p_valor_compras_custo,    p_valor_compras_import,
     p_valor_consumo_custo,    p_valor_consumo_import,
     p_valor_venda,            p_perc_importado,
     p_perc_venda,             p_perc_qtde_venda);

  exception when others
    then raise_application_error(-20000, 'Problema ao inserir dados na tabela obrf_660. Erro banco de dados:  ' || SQLERRM);

end inter_pr_insere_obrf_660;
