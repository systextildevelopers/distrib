
  CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_110" 
   before insert or
          delete or
          update of qtde_entregue
   on supr_110
   for each row

declare
  v_nivel_produto      supr_100.item_100_nivel99%type;
  v_grupo_produto      supr_100.item_100_grupo%type;
  v_subgrupo_produto   supr_100.item_100_subgrupo%type;
  v_item_produto       supr_100.item_100_item%type;

  v_executa_trigger    number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      -- INICIO - Atualizacao da quantidade recebida na ordem de planejamento.
      if inserting or updating or deleting
      then
         if inserting
         then
            begin
               select supr_100.item_100_nivel99,  supr_100.item_100_grupo,
                      supr_100.item_100_subgrupo, supr_100.item_100_item
               into   v_nivel_produto,            v_grupo_produto,
                      v_subgrupo_produto,         v_item_produto
               from supr_100
               where supr_100.num_ped_compra  = :new.ch_100_num_ped
                 and supr_100.seq_item_pedido = :new.ch_100_seq_ped;
            end;

            -- Atualiza a quantidade recebida sempre como primeira qualidade.
            inter_pr_atu_prod_ordem_planej(9,
                                           :new.ch_100_num_ped,
                                           0,
                                           v_nivel_produto,
                                           v_grupo_produto,
                                           v_subgrupo_produto,
                                           v_item_produto,
                                           0,
                                           0,
                                           0,
                                           :new.qtde_entregue,
                                           'P');
         end if;

         if updating
         then
            begin
               select supr_100.item_100_nivel99,  supr_100.item_100_grupo,
                      supr_100.item_100_subgrupo, supr_100.item_100_item
               into   v_nivel_produto,            v_grupo_produto,
                      v_subgrupo_produto,         v_item_produto
               from supr_100
               where supr_100.num_ped_compra  = :new.ch_100_num_ped
                 and supr_100.seq_item_pedido = :new.ch_100_seq_ped;
            end;

            -- Atualizando a nova quantidade recebida.
            if :old.qtde_entregue <> :new.qtde_entregue
            then
               -- Atualizando como estorno quando estiver diminuindo a quantidade entregue.
               if :old.qtde_entregue > :new.qtde_entregue
               then
                  inter_pr_atu_prod_ordem_planej(9,
                                                 :new.ch_100_num_ped,
                                                 0,
                                                 v_nivel_produto,
                                                 v_grupo_produto,
                                                 v_subgrupo_produto,
                                                 v_item_produto,
                                                 0,
                                                 0,
                                                 0,
                                                 (:old.qtde_entregue - :new.qtde_entregue),
                                                 'E');
               end if;

               -- Atualizando como recebimento quando estiver aumentando a quantidade entregue.
               if :old.qtde_entregue < :new.qtde_entregue
               then
                  inter_pr_atu_prod_ordem_planej(9,
                                                 :new.ch_100_num_ped,
                                                 0,
                                                 v_nivel_produto,
                                                 v_grupo_produto,
                                                 v_subgrupo_produto,
                                                 v_item_produto,
                                                 0,
                                                 0,
                                                 0,
                                                 (:new.qtde_entregue - :old.qtde_entregue),
                                                 'P');
               end if;
            end if;
         end if;

         if deleting
         then
            begin
               select supr_100.item_100_nivel99,  supr_100.item_100_grupo,
                      supr_100.item_100_subgrupo, supr_100.item_100_item
               into   v_nivel_produto,            v_grupo_produto,
                      v_subgrupo_produto,         v_item_produto
               from supr_100
               where supr_100.num_ped_compra  = :old.ch_100_num_ped
                 and supr_100.seq_item_pedido = :old.ch_100_seq_ped;
            end;

            inter_pr_atu_prod_ordem_planej(9,
                                           :old.ch_100_num_ped,
                                           0,
                                           v_nivel_produto,
                                           v_grupo_produto,
                                           v_subgrupo_produto,
                                           v_item_produto,
                                           0,
                                           0,
                                           0,
                                           :old.qtde_entregue,
                                           'E');
         end if;
      end if;
      -- FIM - Atualizacao da quantidade recebida na ordem de planejamento.
   end if;
end;

-- ALTER TRIGGER "INTER_TR_SUPR_110" ENABLE

/

exec inter_pr_recompile;
