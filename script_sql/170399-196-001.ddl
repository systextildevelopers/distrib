alter table fatu_030_log 
add (
    total_qtde_old        NUMBER(17,2),
    total_qtde_new        NUMBER(17,2),
    total_mercadoria_old  NUMBER(17,2),
    total_mercadoria_new  NUMBER(17,2),
    valor_nota_fatura_old NUMBER(17,2),
    valor_nota_fatura_new NUMBER(17,2),
    valor_nota_fiscal_old NUMBER(17,2),
    valor_nota_fiscal_new NUMBER(17,2)
)
