
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_030_3" 
     before insert
         or delete
     on pcpb_030
     for each row
declare
   v_periodo_producao   number;

begin
   if inserting or updating
   then
      begin
         select pcpb_010.periodo_producao
         into   v_periodo_producao
         from pcpb_010
         where pcpb_010.ordem_producao = :new.ordem_producao;
      exception when others then
         v_periodo_producao := 0;
      end;

      inter_pr_verif_per_fechado(v_periodo_producao,2);

   end if;

   if deleting
   then
      begin
         select pcpb_010.periodo_producao
         into   v_periodo_producao
         from pcpb_010
         where pcpb_010.ordem_producao = :old.ordem_producao;
      exception when others then
         v_periodo_producao := 0;
      end;

      inter_pr_verif_per_fechado(v_periodo_producao,2);

   end if;

end inter_tr_pcpb_030_3;

-- ALTER TRIGGER "INTER_TR_PCPB_030_3" ENABLE
 

/

exec inter_pr_recompile;

