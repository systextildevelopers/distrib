create table obrf_022(
     nivel     varchar2(2) not null
    ,grupo     varchar2(5) not null
    ,subgrupo  varchar2(3) default '0'
    ,item      varchar2(6) default '0'
    ,descricao varchar2(255)
);


ALTER TABLE obrf_022
ADD CONSTRAINT PK_OBRF_022 PRIMARY KEY (nivel, grupo, subgrupo, item);
