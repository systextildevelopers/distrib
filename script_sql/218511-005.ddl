-- Add/modify columns 
alter table EFIC_040 add cod_motivo_externo number(3) default 0;

-- Add comments to the columns 
comment on column EFIC_040.cod_motivo_externo
  is 'Codigo do motivo correspondente em outros sistema';

