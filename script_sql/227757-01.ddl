create table estq_190 (
    agrupador       number(5) not null,
    descricao       varchar2(240) not null,
    codigo_empresa  number(5) not null,
    deposito_ajuste number(5) not null
);

alter table estq_190 add constraint pk_estq_190 primary key (agrupador,codigo_empresa);
