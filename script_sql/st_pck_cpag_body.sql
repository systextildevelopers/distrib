create or replace PACKAGE BODY ST_PCK_CPAG AS

    function exists_tipo_titulo(p_tipo_titulo number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM cpag_040 
        WHERE tipo_titulo = p_tipo_titulo;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_tipo_titulo;
    
    function exists_cancelamento(v_codigo_cancelamento number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM cpag_045 
        WHERE cod_cancelamento = v_codigo_cancelamento;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_cancelamento;
    
    function exists_projeto(p_projeto number, p_subprojeto number, p_servico number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM cpag_350 
        WHERE projeto = p_projeto
        AND subprojeto = p_subprojeto
        AND servico = p_servico;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
            WHEN others THEN
                return false;
    END exists_projeto;
    
    function get_data_validade_projeto(p_projeto number, p_subprojeto number, p_servico number) return t_projeto
    AS 
        v_projeto t_projeto;
    BEGIN
        BEGIN
            SELECT 
                data_ini_validade,
                data_fim_validade
            INTO
                v_projeto.data_ini_validade,
                v_projeto.data_fim_validade
            FROM cpag_350 
            WHERE projeto = p_projeto
            AND subprojeto = p_subprojeto
            AND servico = p_servico;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_projeto.data_ini_validade := null;
                    v_projeto.data_fim_validade := null;
        END;
        RETURN v_projeto;
    END get_data_validade_projeto;

    function exists_adiantamento_imp(p_num_importacao varchar2, p_cgc_9 number, p_cgc_4 number, p_cgc_2 number) return boolean
    AS
        v_exists number;
    BEGIN
        SELECT
            1
        INTO
            v_exists
        FROM cpag_200 
        WHERE cgc_9 = p_cgc_9
          AND cgc_4 = p_cgc_4
          AND cgc_2 = p_cgc_2
          AND num_importacao = p_num_importacao
          and rownum = 1;
        
        IF v_exists = 1 THEN
            return true;
        ELSE
            return false;
        END IF;
        EXCEPTION 
           WHEN others THEN
               return false;
    END exists_adiantamento_imp;

    function cria_adiantamento(p_cgc9 			number, 	p_cgc4 		number, p_cgc2 				number, p_empresa 			number, p_centro_custo 	number, 
                               p_nome 			varchar2, 	p_valor 	number, p_data_contrato 	date, 	p_data_vencimento 	date,
                               p_cod_portador 		number, p_moeda 		varchar2,
                               p_num_importacao varchar2,   v_num_adiantam in out number) return varchar2
    AS
      --  v_num_adiantam number;
        v_moeda number;
        p_des_erro varchar2(1000);
    BEGIN
        
        
        /*select nvl(max(cpag_200.numero_adiantam),0) + 1 
        into  v_num_adiantam
        from  cpag_200
        where cpag_200.tipo_adiantam = 1;*/
        v_num_adiantam := 30000;

        if p_moeda = 'BRL'
        THEN
            v_moeda := 0;
        end if;
        
        BEGIN
            insert into cpag_200 (
                tipo_adiantam, numero_adiantam, 
                cgc_9, cgc_4, 
                cgc_2, data_digitacao, 
                valor_adiant, valor_saldo, 
                situacao, data_remessa, 
                codigo_empresa, data_prev_pgto, 
                cod_moeda, cod_portador, 
                data_moeda, origem_adiantam,
                num_importacao
            ) values (
                1, v_num_adiantam,
                p_cgc9, p_cgc4, 
                p_cgc2, p_data_contrato,
                p_valor, p_valor,
                1, p_data_contrato,
                p_empresa, p_data_vencimento,
                v_moeda, p_cod_portador,
                p_data_contrato, 'ADIANTAMENTO FUNCIONARIO',
                p_num_importacao
            );
        EXCEPTION
        WHEN OTHERS
            THEN
                p_des_erro := 'Erro ao inserir adiantamento. Nome: ' || p_nome || ' Nr. Adiant.: ' || v_num_adiantam || ' ERRO: ' || SQLERRM;
        END;
    
        RETURN p_des_erro;
    END cria_adiantamento;

END ST_PCK_CPAG;
