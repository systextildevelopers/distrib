alter table pedi_015
add A_PARTIR_DE_MTS NUMBER(9,3);

alter table pedi_015
add PECA_MEDIA NUMBER(1) DEFAULT 0 CHECK(PECA_MEDIA IN (0,1));
