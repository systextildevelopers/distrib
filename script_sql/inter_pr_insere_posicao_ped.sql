
  CREATE OR REPLACE PROCEDURE "INTER_PR_INSERE_POSICAO_PED" 
   (p_pedido_venda      in number,   p_data_digitacao   in date)
is

   cursor hdoc001 is
      select codigo,valor01
      from hdoc_001
      where hdoc_001.tipo = 99
      order by hdoc_001.codigo ASC;

   v_data_prev      date;
   v_data_prev_lida date;
begin

   for reg_hdoc001 in hdoc001
   loop
      if v_data_prev is null
      then
         v_data_prev := p_data_digitacao + reg_hdoc001.valor01;
      else
         v_data_prev := v_data_prev_lida + reg_hdoc001.valor01;
      end if;

      insert into pedi_620
         (pedido,
          posicao,
          data_prevista,
          data_realizada,
          liberador)
      VALUES (
          p_pedido_venda,
          reg_hdoc001.codigo,
          v_data_prev,
          '',
          '');

      v_data_prev_lida := v_data_prev;
   end loop;
end inter_pr_insere_posicao_ped;

 

/

exec inter_pr_recompile;

