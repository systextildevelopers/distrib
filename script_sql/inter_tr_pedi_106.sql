CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_106"
BEFORE INSERT ON PEDI_106 FOR EACH ROW
DECLARE
    proximo_valor number;
BEGIN
    if :new.id is null then
        select ID_PEDI_106.nextval into proximo_valor from dual;
        :new.id := proximo_valor;
    end if;
END INTER_TR_PEDI_106;

/

exec inter_pr_recompile;
