
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_020_PLAN" 
   before delete or
          update of cod_cancelamento, alternativa_item
   on pcpb_020
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_count                   number;
   v_existe615               number(1);
   v_nro_reg                 number;

   cursor tmrp625 is select tmrp_625.ordem_planejamento,         tmrp_625.pedido_venda,
                            tmrp_625.pedido_reserva,             tmrp_625.qtde_reserva_planejada,
                            tmrp_625.nivel_produto_origem,       tmrp_625.grupo_produto_origem,
                            tmrp_625.subgrupo_produto_origem,    tmrp_625.item_produto_origem,
                            tmrp_625.alternativa_produto_origem, tmrp_625.seq_produto,
                            tmrp_625.seq_produto_origem
                     from tmrp_625
                     where tmrp_625.ordem_planejamento in (
                                         select pcpb_030.nr_pedido_ordem
                                         from pcpb_030
                                         where pcpb_030.ordem_producao = :new.ordem_producao
                                           and pcpb_030.pano_nivel99   = :new.pano_sbg_nivel99
                                           and pcpb_030.pano_grupo     = :new.pano_sbg_grupo
                                           and pcpb_030.pano_subgrupo  = :new.pano_sbg_subgrupo
                                           and pcpb_030.pano_item      = :new.pano_sbg_item
                                           and pcpb_030.pedido_corte   = 8
                                         group by pcpb_030.nr_pedido_ordem

                                         union all

                                         select pcpb_031.ordem_planejamento
                                         from pcpb_031
                                         where pcpb_031.ordem_producao = :new.ordem_producao
                                           and pcpb_031.pano_nivel99   = :new.pano_sbg_nivel99
                                           and pcpb_031.pano_grupo     = :new.pano_sbg_grupo
                                           and pcpb_031.pano_subgrupo  = :new.pano_sbg_subgrupo
                                           and pcpb_031.pano_item      = :new.pano_sbg_item
                                         group by pcpb_031.ordem_planejamento)
                       and tmrp_625.nivel_produto              = :new.pano_sbg_nivel99
                       and tmrp_625.grupo_produto              = :new.pano_sbg_grupo
                       and tmrp_625.subgrupo_produto           = :new.pano_sbg_subgrupo
                       and tmrp_625.item_produto               = :new.pano_sbg_item
                       and tmrp_625.alternativa_produto        = :old.alternativa_item
                     order by tmrp_625.ordem_planejamento,         tmrp_625.pedido_venda,
                              tmrp_625.pedido_reserva,             tmrp_625.qtde_reserva_planejada,
                              tmrp_625.nivel_produto_origem,       tmrp_625.grupo_produto_origem,
                              tmrp_625.subgrupo_produto_origem,    tmrp_625.item_produto_origem,
                              tmrp_625.alternativa_produto_origem, tmrp_625.seq_produto,
                              tmrp_625.seq_produto_origem;


begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   begin
      select nvl(count(*),0)
      into v_nro_reg
      from pcpb_031
      where pcpb_031.ordem_producao = :new.ordem_producao;
   exception when others then
      v_nro_reg := 0;
   end;

   if  updating
   and v_nro_reg = 0
   and :new.alternativa_item <> :old.alternativa_item
   then

-- raise_application_error(-20000,'AQUI');

      -- Deleta a tmrp_637 da ordem de planejamento
      begin
         delete from tmrp_637
         where tmrp_637.ordem_prod_compra = :new.ordem_producao
           and tmrp_637.area_producao     = 2;
      exception when others then
         -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_637' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      -- Cria umm backup da tmrp_630 desta ordem de planejamento
      -- tmrp_637 e uma copia da tmrp_630, todos os campso que tiver na tmrp_630 deve ter na tmrp_637
      begin
         insert into tmrp_637 (
                ordem_planejamento,                      area_producao,
                ordem_prod_compra,
                nivel_produto,                           grupo_produto,
                subgrupo_produto,                        item_produto,
                qtde_produzida_comprada,                 qtde_conserto,
                qtde_2qualidade,                         qtde_perdas,
                pedido_venda,                            programado_comprado,
                alternativa_produto,                     qtde_atendida_estoque)
         select tmrp_630.ordem_planejamento,             tmrp_630.area_producao,
                tmrp_630.ordem_prod_compra,
                tmrp_630.nivel_produto,                  tmrp_630.grupo_produto,
                tmrp_630.subgrupo_produto,               tmrp_630.item_produto,
                tmrp_630.qtde_produzida_comprada,        tmrp_630.qtde_conserto,
                tmrp_630.qtde_2qualidade,                tmrp_630.qtde_perdas,
                tmrp_630.pedido_venda,                   tmrp_630.programado_comprado,
                tmrp_630.alternativa_produto,            tmrp_630.qtde_atendida_estoque
         from tmrp_630
         where tmrp_630.ordem_prod_compra = :new.ordem_producao
           and tmrp_630.area_producao     = 2;
      exception when others then
         -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_637' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      for reg_625 in tmrp625
      loop
         begin
            insert into tmrp_615(
               ordem_prod,                               area_producao,
               nivel,                                    grupo,
               subgrupo,                                 item,
               alternativa,                              ordem_planejamento,
               pedido_venda,                             pedido_reserva,
               nome_programa,
               nr_solicitacao,                           tipo_registro,
               cgc_cliente9,
               seq_registro,                             sequencia,
               nivel_prod,                               grupo_prod,
               subgrupo_prod,                            item_prod,
               alternativa_prod)
            values
              (:new.ordem_producao,                      decode(:new.pano_sbg_nivel99,'2',1,'4',2,9),
               reg_625.nivel_produto_origem,             reg_625.grupo_produto_origem,
               reg_625.subgrupo_produto_origem,          reg_625.item_produto_origem,
               reg_625.alternativa_produto_origem,       reg_625.ordem_planejamento,
               reg_625.pedido_venda,                     reg_625.pedido_reserva,
               'trigger_planejamento',
               888,                                      888,
               ws_sid,
               reg_625.seq_produto,                      reg_625.seq_produto_origem,
               :new.pano_sbg_nivel99,                    :new.pano_sbg_grupo,
               :new.pano_sbg_subgrupo,                   :new.pano_sbg_item,
               :old.alternativa_item
            );
         exception when others then
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end loop;

      -- Deleta a tmrp_630 desta ordem de planejamento
      begin
         delete from tmrp_630
         where tmrp_630.ordem_prod_compra = :new.ordem_producao
           and tmrp_630.area_producao     = 2;
      exception when others then
         -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      begin
         insert into tmrp_621 (
                ordem_planejamento,             pedido_venda,
                ordem_prod_compra,              area_producao,
                pedido_reserva,                 tipo_produto_origem,

                seq_produto_origem,             nivel_produto_origem,
                grupo_produto_origem,           subgrupo_produto_origem,
                item_produto_origem,            alternativa_produto_origem,

                tipo_produto,                   seq_produto,
                nivel_produto,                  grupo_produto,
                subgrupo_produto,               item_produto,

                alternativa_produto,            qtde_areceber_programada,
                alternativa_produto_old)
         select tmrp_637.ordem_planejamento,    tmrp_637.pedido_venda,
                tmrp_637.ordem_prod_compra,     tmrp_637.area_producao,
                tmrp_625.pedido_reserva,        ' ',

                tmrp_625.seq_produto_origem,    tmrp_625.nivel_produto_origem,
                tmrp_625.grupo_produto_origem,  tmrp_625.subgrupo_produto_origem,
                tmrp_625.item_produto_origem,   tmrp_625.alternativa_produto_origem,

                ' ',                            tmrp_625.seq_produto,
                tmrp_637.nivel_produto,         tmrp_637.grupo_produto,
                tmrp_637.subgrupo_produto,      tmrp_637.item_produto,

                :new.alternativa_item,          tmrp_637.programado_comprado,
                tmrp_637.alternativa_produto
         from tmrp_637, tmrp_625
         where tmrp_637.ordem_prod_compra   = :new.ordem_producao
           and tmrp_637.area_producao       = 2
           and tmrp_625.ordem_planejamento  = tmrp_637.ordem_planejamento
           and tmrp_625.pedido_venda        = tmrp_637.pedido_venda
           and tmrp_625.nivel_produto       = tmrp_637.nivel_produto
           and tmrp_625.grupo_produto       = tmrp_637.grupo_produto
           and tmrp_625.subgrupo_produto    = tmrp_637.subgrupo_produto
           and tmrp_625.item_produto        = tmrp_637.item_produto
           and tmrp_625.alternativa_produto = tmrp_637.alternativa_produto;
      end;
/*
      begin
         update pcpb_030
         set    pcpb_030.qtde_quilos_prog = pcpb_030.qtde_quilos_prog,
                pcpb_030.alternativa      = :new.alternativa_item
         where  pcpb_030.ordem_producao   = :new.ordem_producao;
      end;
*/
   else
      -- Deleta a tmrp_637 da ordem de planejamento
      begin
         delete from tmrp_637
         where tmrp_637.ordem_prod_compra = :new.ordem_producao
           and tmrp_637.area_producao     = 2;
      exception when others then
         -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_637' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      -- Cria umm backup da tmrp_630 desta ordem de planejamento
      -- tmrp_637 e uma copia da tmrp_630, todos os campso que tiver na tmrp_630 deve ter na tmrp_637
      begin
         insert into tmrp_637 (
                ordem_planejamento,                      area_producao,
                ordem_prod_compra,
                nivel_produto,                           grupo_produto,
                subgrupo_produto,                        item_produto,
                qtde_produzida_comprada,                 qtde_conserto,
                qtde_2qualidade,                         qtde_perdas,
                pedido_venda,                            programado_comprado,
                alternativa_produto,                     qtde_atendida_estoque)
         select tmrp_630.ordem_planejamento,             tmrp_630.area_producao,
                tmrp_630.ordem_prod_compra,
                tmrp_630.nivel_produto,                  tmrp_630.grupo_produto,
                tmrp_630.subgrupo_produto,               tmrp_630.item_produto,
                tmrp_630.qtde_produzida_comprada,        tmrp_630.qtde_conserto,
                tmrp_630.qtde_2qualidade,                tmrp_630.qtde_perdas,
                tmrp_630.pedido_venda,                   tmrp_630.programado_comprado,
                tmrp_630.alternativa_produto,            tmrp_630.qtde_atendida_estoque
         from tmrp_630
         where tmrp_630.ordem_prod_compra = :new.ordem_producao
           and tmrp_630.area_producao     = 2;
      exception when others then
         -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_637' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      for reg_625 in tmrp625
      loop
         begin
            insert into tmrp_615(
               ordem_prod,                               area_producao,
               nivel,                                    grupo,
               subgrupo,                                 item,
               alternativa,                              ordem_planejamento,
               pedido_venda,                             pedido_reserva,
               nome_programa,
               nr_solicitacao,                           tipo_registro,
               cgc_cliente9,
               seq_registro,                             sequencia,
               nivel_prod,                               grupo_prod,
               subgrupo_prod,                            item_prod,
               alternativa_prod)
            values
              (:new.ordem_producao,                      decode(:new.pano_sbg_nivel99,'2',1,'4',2,9),
               reg_625.nivel_produto_origem,             reg_625.grupo_produto_origem,
               reg_625.subgrupo_produto_origem,          reg_625.item_produto_origem,
               reg_625.alternativa_produto_origem,       reg_625.ordem_planejamento,
               reg_625.pedido_venda,                     reg_625.pedido_reserva,
               'trigger_planejamento',
               888,                                      888,
               ws_sid,
               reg_625.seq_produto,                      reg_625.seq_produto_origem,
               :new.pano_sbg_nivel99,                    :new.pano_sbg_grupo,
               :new.pano_sbg_subgrupo,                   :new.pano_sbg_item,
               :old.alternativa_item
            );
         exception when others then
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end loop;

      -- Cria um backup da pcpb_031 desta ordem de planejamento
      -- pcpb_033 e uma copia da pcpb_031, todos os campos que tiver na pcpb_031 deve ter na pcpb_033
      begin
         insert into pcpb_033 (
                ordem_planejamento,                pedido_venda,
                ordem_producao,                    pano_nivel99,
                pano_grupo,                        pano_subgrupo,
                pano_item,                         sequenci_periodo,
                nr_pedido_ordem,                   nivel_prod,
                grupo_prod,                        subgrupo_prod,
                item_prod,                         qtde_quilos_prog,
                qtde_unidades_prog,                tipo_programacao)
         select pcpb_031.ordem_planejamento,       pcpb_031.pedido_venda,
                pcpb_031.ordem_producao,           pcpb_031.pano_nivel99,
                pcpb_031.pano_grupo,               pcpb_031.pano_subgrupo,
                pcpb_031.pano_item,                pcpb_031.sequenci_periodo,
                pcpb_031.nr_pedido_ordem,          pcpb_031.nivel_prod,
                pcpb_031.grupo_prod,               pcpb_031.subgrupo_prod,
                pcpb_031.item_prod,                pcpb_031.qtde_quilos_prog,
                pcpb_031.qtde_unidades_prog,       pcpb_031.tipo_programacao
         from pcpb_031
         where pcpb_031.ordem_producao = :new.ordem_producao;
      exception when others then
         -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPB_033' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      -- Deleta a pcpb_033 da ordem de planejamento
      begin
         delete from pcpb_031
         where pcpb_031.ordem_producao = :new.ordem_producao;
      exception when others then
         -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'PCPB_031' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      -- Deleta a tmrp_630 desta ordem de planejamento
      begin
         delete from tmrp_630
         where tmrp_630.ordem_prod_compra = :new.ordem_producao
           and tmrp_630.area_producao     = 2;
      exception when others then
         -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      begin
         insert into tmrp_621 (
                ordem_planejamento,             pedido_venda,
                ordem_prod_compra,              area_producao,
                pedido_reserva,                 tipo_produto_origem,

                seq_produto_origem,             nivel_produto_origem,
                grupo_produto_origem,           subgrupo_produto_origem,
                item_produto_origem,            alternativa_produto_origem,

                tipo_produto,                   seq_produto,
                nivel_produto,                  grupo_produto,
                subgrupo_produto,               item_produto,

                alternativa_produto,            qtde_areceber_programada,
                alternativa_produto_old)
         select tmrp_637.ordem_planejamento,    tmrp_637.pedido_venda,
                tmrp_637.ordem_prod_compra,     tmrp_637.area_producao,
                tmrp_625.pedido_reserva,        ' ',

                tmrp_625.seq_produto_origem,    tmrp_625.nivel_produto_origem,
                tmrp_625.grupo_produto_origem,  tmrp_625.subgrupo_produto_origem,
                tmrp_625.item_produto_origem,   tmrp_625.alternativa_produto_origem,

                ' ',                            tmrp_625.seq_produto,
                tmrp_637.nivel_produto,         tmrp_637.grupo_produto,
                tmrp_637.subgrupo_produto,      tmrp_637.item_produto,

                :new.alternativa_item,          tmrp_637.programado_comprado,
                tmrp_637.alternativa_produto
         from tmrp_637, tmrp_625
         where tmrp_637.ordem_prod_compra   = :new.ordem_producao
           and tmrp_637.area_producao       = 2
           and tmrp_625.ordem_planejamento  = tmrp_637.ordem_planejamento
           and tmrp_625.pedido_venda        = tmrp_637.pedido_venda
           and tmrp_625.nivel_produto       = tmrp_637.nivel_produto
           and tmrp_625.grupo_produto       = tmrp_637.grupo_produto
           and tmrp_625.subgrupo_produto    = tmrp_637.subgrupo_produto
           and tmrp_625.item_produto        = tmrp_637.item_produto
           and tmrp_625.alternativa_produto = tmrp_637.alternativa_produto;
      end;

      -- Volta o backup da pcpb_031, assim vai criar a tmrp_630 para estas ordens de producao
      -- Os dados do produto origem (tmrp_615) sera criada na trigger da pcpb_031
      begin
         insert into pcpb_031 (
                ordem_planejamento,                pedido_venda,
                ordem_producao,                    pano_nivel99,
                pano_grupo,                        pano_subgrupo,
                pano_item,                         sequenci_periodo,
                nr_pedido_ordem,                   nivel_prod,
                grupo_prod,                        subgrupo_prod,
                item_prod,                         qtde_quilos_prog,
                qtde_unidades_prog,                tipo_programacao,
                alternativa_pano)
         select pcpb_033.ordem_planejamento,       pcpb_033.pedido_venda,
                pcpb_033.ordem_producao,           pcpb_033.pano_nivel99,
                pcpb_033.pano_grupo,               pcpb_033.pano_subgrupo,
                pcpb_033.pano_item,                pcpb_033.sequenci_periodo,
                pcpb_033.nr_pedido_ordem,          pcpb_033.nivel_prod,
                pcpb_033.grupo_prod,               pcpb_033.subgrupo_prod,
                pcpb_033.item_prod,                pcpb_033.qtde_quilos_prog,
                pcpb_033.qtde_unidades_prog,       pcpb_033.tipo_programacao,
                :new.alternativa_item
         from pcpb_033
         where pcpb_033.ordem_producao = :new.ordem_producao;
      exception when others then
         -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPB_031(01)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
/*
      begin
         update pcpb_030
         set    pcpb_030.qtde_quilos_prog = pcpb_030.qtde_quilos_prog,
                pcpb_030.alternativa      = :new.alternativa_item
         where  pcpb_030.ordem_producao   = :new.ordem_producao;
      end;
*/
   end if;

   v_existe615 := 0;

   if (updating
   and (:old.cod_cancelamento =  0
   and  :old.cod_cancelamento <> :new.cod_cancelamento))
   or deleting
   then
      for reg_tmrp_630 in (select tmrp_630.ordem_planejamento,          tmrp_630.pedido_venda,
                                  tmrp_630.pedido_reserva,
                                  tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                  tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                  tmrp_625.alternativa_produto_origem,
                                  tmrp_630.area_producao,               tmrp_630.ordem_prod_compra,
                                  tmrp_625.seq_produto,                 tmrp_625.seq_produto_origem
                           from tmrp_630, tmrp_625, pcpb_030
                           where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
                             and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
                             and tmrp_625.pedido_reserva             = tmrp_630.pedido_reserva
                             and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
                             and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
                             and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
                             and tmrp_625.item_produto_origem        = tmrp_630.item_produto
                             and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto
                             and tmrp_625.nivel_produto              = :old.pano_sbg_nivel99
                             and tmrp_625.grupo_produto              = :old.pano_sbg_grupo
                             and tmrp_625.subgrupo_produto           = :old.pano_sbg_subgrupo
                             and tmrp_625.item_produto               = :old.pano_sbg_item
                             and tmrp_625.alternativa_produto        = :old.alternativa_item
                             and tmrp_630.area_producao              = decode(:old.pano_sbg_nivel99,'2',1,4)
                             and tmrp_630.ordem_prod_compra          = pcpb_030.nr_pedido_ordem
                             and pcpb_030.pedido_corte               = 4
                             and pcpb_030.ordem_producao             = :old.ordem_producao
                             and pcpb_030.sequencia                  = :old.sequencia
                        group by  tmrp_630.ordem_planejamento,          tmrp_630.pedido_venda,
                                  tmrp_630.pedido_reserva,
                                  tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                  tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                  tmrp_625.alternativa_produto_origem,
                                  tmrp_630.area_producao,               tmrp_630.ordem_prod_compra,
                                  tmrp_625.seq_produto,                 tmrp_625.seq_produto_origem)
      loop
         v_existe615 := 1;
         begin
            insert into tmrp_615
              (ordem_prod,                               area_producao,
               nivel,                                    grupo,
               subgrupo,                                 item,
               alternativa,                              ordem_planejamento,
               pedido_venda,                             pedido_reserva,
               nome_programa,
               nr_solicitacao,                           tipo_registro,
               cgc_cliente9,

               seq_registro,                             sequencia,
               nivel_prod,                               grupo_prod,
               subgrupo_prod,                            item_prod,
               alternativa_prod)
            values
              (reg_tmrp_630.ordem_prod_compra,           reg_tmrp_630.area_producao,
               reg_tmrp_630.nivel_produto_origem,        reg_tmrp_630.grupo_produto_origem,
               reg_tmrp_630.subgrupo_produto_origem,     reg_tmrp_630.item_produto_origem,
               reg_tmrp_630.alternativa_produto_origem,  reg_tmrp_630.ordem_planejamento,
               reg_tmrp_630.pedido_venda,                reg_tmrp_630.pedido_reserva,
               'trigger_planejamento',
               888,                                      888,
               ws_sid,

               reg_tmrp_630.seq_produto,                 reg_tmrp_630.seq_produto_origem,
               :old.pano_sbg_nivel99,                    :old.pano_sbg_grupo,
               :old.pano_sbg_subgrupo,                   :old.pano_sbg_item,
               :old.alternativa_item);

            exception when others
            then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end loop;
   end if;

   if v_existe615 = 1
   then
      if deleting
      then
         begin
            select 1
            into v_count
            from pcpb_030
            where pcpb_030.ordem_producao = :old.ordem_producao
              and pcpb_030.pano_nivel99   = :old.pano_sbg_nivel99
              and pcpb_030.pano_grupo     = :old.pano_sbg_grupo
              and pcpb_030.pano_subgrupo  = :old.pano_sbg_subgrupo
              and pcpb_030.pano_item      = :old.pano_sbg_item
              and pcpb_030.sequencia      = :old.sequencia;
            exception when others
            then v_count := 0;
         end;
      end if;

      if (deleting and v_count > 0) or updating
      then
         begin
            delete tmrp_630
            where tmrp_630.ordem_prod_compra   = :old.ordem_producao
              and tmrp_630.area_producao       = decode(:old.pano_sbg_nivel99,'2',2,7)
              and tmrp_630.nivel_produto       = :old.pano_sbg_nivel99
              and tmrp_630.grupo_produto       = :old.pano_sbg_grupo
              and tmrp_630.subgrupo_produto    = :old.pano_sbg_subgrupo
              and tmrp_630.item_produto        = :old.pano_sbg_item
              and tmrp_630.alternativa_produto = :old.alternativa_item;
            exception when others
            then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   elsif (((updating
   and     (:old.cod_cancelamento = 0
   and      :old.cod_cancelamento <> :new.cod_cancelamento))
   or    deleting)
   and   v_existe615 = 0)
   then
      for reg30 in (select pcpb_030.nr_pedido_ordem,   pcpb_030.qtde_quilos_prog
                    from pcpb_030
                    where pcpb_030.ordem_producao = :old.ordem_producao
                      and pcpb_030.pano_nivel99   = :old.pano_sbg_nivel99
                      and pcpb_030.pano_grupo     = :old.pano_sbg_grupo
                      and pcpb_030.pano_subgrupo  = :old.pano_sbg_subgrupo
                      and pcpb_030.pano_item      = :old.pano_sbg_item
                      and pcpb_030.alternativa    = :old.alternativa_item
                      and pcpb_030.sequencia      = :old.sequencia
                      and pcpb_030.pedido_corte   = 9)
      loop
         inter_pr_destinos_planejamento(reg30.nr_pedido_ordem,
                                        :old.ordem_producao,
                                        reg30.qtde_quilos_prog,
                                        :old.pano_sbg_nivel99,
                                        :old.pano_sbg_grupo,
                                        :old.pano_sbg_subgrupo,
                                        :old.pano_sbg_item,
                                        :old.alternativa_item,
                                        'D');
      end loop;
   end if;
end inter_tr_pcpb_020_plan;


-- ALTER TRIGGER "INTER_TR_PCPB_020_PLAN" ENABLE
 

/

exec inter_pr_recompile;

