create or replace view inter_vi_pedi_100_prt_entrega as
with vi_pedi_100 as (select
pedi_100.pedido_venda ,
pedi_100.coletor,
pedi_100.liberado_coletar,
pedi_100.status_expedicao,
pedi_100.tipo_pedido,
pedi_100.prioridade,
pedi_100.qtde_saldo_pedi,
pedi_100.observacao,
pedi_100.data_entr_venda,
(select nvl(sum(pcpc_325.qtde_pecas_real),0) from pcpc_320, pcpc_325 where pcpc_320.numero_volume = pcpc_325.numero_volume and pcpc_320.pedido_venda = pedi_100.pedido_venda) as qtde_coletada
from pedi_100) select vi_pedi_100.*, (vi_pedi_100.qtde_saldo_pedi - vi_pedi_100.qtde_coletada) as qtde_a_coletar  from vi_pedi_100
