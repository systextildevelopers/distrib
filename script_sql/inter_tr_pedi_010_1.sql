
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_010_1" 
before update of data_maior_atr,   data_maior_fatur,
                 data_maior_tit,   data_ult_compra,
                 data_ult_fatur,   data_ult_titulo,
                 dt_maior_acumulo, dt_maior_pedido on pedi_010
for each row



declare

  v_mes_ano_old   number(02);
  v_mes_ano_new   number(02);

begin
   if UPDATING
   then

      if :old.data_maior_atr is null
      then
         :new.data_maior_atr_ant := :new.data_maior_atr;
         :new.maior_atraso_ant   := :new.maior_atraso;
      else
         begin
            if :new.data_maior_atr   <> :old.data_maior_atr
            then
               v_mes_ano_new  := to_number(to_char(:new.data_maior_atr,'MM'));
               v_mes_ano_old  := to_number(to_char(:old.data_maior_atr,'MM'));
               if v_mes_ano_new <> v_mes_ano_old
               then
                  :new.data_maior_atr_ant := :old.data_maior_atr;
                  :new.maior_atraso_ant   := :old.maior_atraso;
               end if;
            end if;
         end;
      end if;

      if :old.data_maior_fatur is null
      then
         :new.data_maior_fatur_ant := :new.data_maior_fatur;
         :new.val_maior_fatur_ant  := :new.val_maior_fatur;
      else
         begin
            if :new.data_maior_fatur <> :old.data_maior_fatur
            then
               v_mes_ano_new  := to_number(to_char(:new.data_maior_fatur,'MM'));
               v_mes_ano_old  := to_number(to_char(:old.data_maior_fatur,'MM'));
               if v_mes_ano_new <> v_mes_ano_old
               then
                  :new.data_maior_fatur_ant := :old.data_maior_fatur;
                  :new.val_maior_fatur_ant  := :old.val_maior_fatur;
               end if;
            end if;
         end;
      end if;

      if :old.data_maior_tit is null
      then
         :new.data_maior_tit_ant         := :new.data_maior_tit;
         :new.maior_titulo_ant           := :new.maior_titulo;
      else
         begin
            if :new.data_maior_tit   <> :old.data_maior_tit
            then
               v_mes_ano_new  := to_number(to_char(:new.data_maior_tit,'MM'));
               v_mes_ano_old  := to_number(to_char(:old.data_maior_tit,'MM'));
               if v_mes_ano_new <> v_mes_ano_old
               then
                  :new.data_maior_tit_ant         := :old.data_maior_tit;
                  :new.maior_titulo_ant           := :old.maior_titulo;
               end if;
            end if;
         end;
      end if ;

      if :old.data_ult_compra is null
      then
         :new.data_ult_compra_ant            := :new.data_ult_compra;
         :new.valor_ult_compra_ant           := :new.valor_ult_compra;
      else
         begin
            if :new.data_ult_compra  <> :old.data_ult_compra
            then
               v_mes_ano_new  := to_number(to_char(:new.data_ult_compra,'MM'));
               v_mes_ano_old  := to_number(to_char(:old.data_ult_compra,'MM'));
               if v_mes_ano_new <> v_mes_ano_old
               then
                  :new.data_ult_compra_ant            := :old.data_ult_compra;
                  :new.valor_ult_compra_ant           := :old.valor_ult_compra;
               end if;
            end if;
         end;
      end if ;

      if :old.data_ult_fatur is null
      then
         :new.data_ult_fatur_ant       := :new.data_ult_fatur;
         :new.valor_ult_fatur_ant      := :new.valor_ult_fatur;
      else
         begin
            if :new.data_ult_fatur   <> :old.data_ult_fatur
            then
               v_mes_ano_new  := to_number(to_char(:new.data_ult_fatur,'MM'));
               v_mes_ano_old  := to_number(to_char(:old.data_ult_fatur,'MM'));
               if v_mes_ano_new <> v_mes_ano_old
               then
                  :new.data_ult_fatur_ant       := :old.data_ult_fatur;
                  :new.valor_ult_fatur_ant      := :old.valor_ult_fatur;
               end if;
            end if;
         end;
      end if;


      if :old.data_ult_titulo is null
      then
         :new.data_ult_titulo_ant      := :new.data_ult_titulo;
         :new.valor_ult_titulo_ant     := :new.valor_ult_titulo;
      else
         begin
            if :new.data_ult_titulo  <> :old.data_ult_titulo
            then
               v_mes_ano_new  := to_number(to_char(:new.data_ult_titulo,'MM'));
               v_mes_ano_old  := to_number(to_char(:old.data_ult_titulo,'MM'));
               if v_mes_ano_new <> v_mes_ano_old
               then
                  :new.data_ult_titulo_ant      := :old.data_ult_titulo;
                  :new.valor_ult_titulo_ant     := :old.valor_ult_titulo;
               end if;
            end if ;
         end;
      end if;

      if :old.dt_maior_acumulo is null
      then
         :new.dt_maior_acumulo_ant      := :new.dt_maior_acumulo;
         :new.maior_acumulo_ant         := :new.maior_acumulo;
      else
         begin
            if :new.dt_maior_acumulo <> :old.dt_maior_acumulo
            then
               v_mes_ano_new  := to_number(to_char(:new.dt_maior_acumulo,'MM'));
               v_mes_ano_old  := to_number(to_char(:old.dt_maior_acumulo,'MM'));
               if v_mes_ano_new <> v_mes_ano_old
               then
                  :new.dt_maior_acumulo_ant      := :old.dt_maior_acumulo;
                  :new.maior_acumulo_ant         := :old.maior_acumulo;
               end if;
            end if ;
         end;
      end if;

      if :old.dt_maior_pedido is null
      then
         :new.dt_maior_pedido_ant   := :new.dt_maior_pedido;
         :new.vl_maior_pedido_ant   := :new.vl_maior_pedido;
      else
         begin
            if :new.dt_maior_pedido  <> :old.dt_maior_pedido
            then
               v_mes_ano_new  := to_number(to_char(:new.dt_maior_pedido,'MM'));
               v_mes_ano_old  := to_number(to_char(:old.dt_maior_pedido,'MM'));
               if v_mes_ano_new <> v_mes_ano_old
               then
                  :new.dt_maior_pedido_ant   := :old.dt_maior_pedido;
                  :new.vl_maior_pedido_ant   := :old.vl_maior_pedido;
               end if;
            end if ;
         end;
      end if;
   end if;

end inter_tr_pedi_010_1;


-- ALTER TRIGGER "INTER_TR_PEDI_010_1" ENABLE
 

/

exec inter_pr_recompile;

