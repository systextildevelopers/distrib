create  table cont_k310 (
COD_CONTROLADORA    	NUMBER(9) 		DEFAULT 0,
EXERCICIO           	NUMBER(9) 		DEFAULT 0,
COD_CTA 				VARCHAR2(20) 	DEFAULT ' ',
EMP_COD_PARTE			NUMBER(4)		DEFAULT 0,
VALOR					NUMBER(19,2)	DEFAULT 0.0,
IND_VALOR				VARCHAR2(1)		CHECK(IND_VALOR IN ('D','C')) NOT NULL
);

alter table cont_k310
       add constraint PK_cont_k310 primary key (COD_CONTROLADORA,EXERCICIO,COD_CTA,EMP_COD_PARTE);
       
alter table cont_k310 
      add constraint FK_cont_k310 FOREIGN KEY(COD_CONTROLADORA,EXERCICIO,COD_CTA) 
	  REFERENCES cont_k200 (COD_CONTROLADORA,EXERCICIO,COD_CTA);
	  
comment on table cont_k310              		is 'EMPRESAS DETENTORAS DAS PARCELAS DO VALOR ELIMINADO TOTAL';
comment on column cont_k310.COD_CONTROLADORA 	is 'código da Empresa Controladora';
comment on column cont_k310.EXERCICIO 			is 'Exercicio a que se refere as informações da empresa no período';
comment on column cont_k310.COD_CTA 			is 'Código da Conta';
comment on column cont_k310.EMP_COD_PARTE 		is 'Código da Empresa Detentora do Valor Aglutinado que Foi Eliminado';
comment on column cont_k310.VALOR 				is 'Parcela do Valor Eliminado Total';
comment on column cont_k310.IND_VALOR 			is 'Indicador da Situação do Valor Eliminado';
