-- copia molde da basi_082 para a basi_070
update basi_070
set molde = (select basi_082.molde from basi_082 
    where 
    basi_082.molde <> ''
    and basi_082.nivel = basi_070.nivel
    and ((basi_082.item = 'XXXXXX' and basi_070.item = '000000')
        or (basi_082.item = basi_070.item))
    and ((basi_082.grupo = 'XXXXX' and basi_070.grupo = '00000')
        or (basi_082.grupo = basi_070.grupo))
    and ((basi_082.subgrupo = 'XXX' and basi_070.subgrupo = '000')
        or (basi_082.subgrupo = basi_070.subgrupo))
    and basi_082.alternativa = basi_070.alternativa
)
where EXISTS(select basi_082.molde from basi_082 
    where 
    basi_082.molde <> ''
    and basi_082.nivel = basi_070.nivel
    and ((basi_082.item = 'XXXXXX' and basi_070.item = '000000')
        or (basi_082.item = basi_070.item))
    and ((basi_082.grupo = 'XXXXX' and basi_070.grupo = '00000')
        or (basi_082.grupo = basi_070.grupo))
    and ((basi_082.subgrupo = 'XXX' and basi_070.subgrupo = '000')
        or (basi_082.subgrupo = basi_070.subgrupo))
    and basi_082.alternativa = basi_070.alternativa
    ) ;
