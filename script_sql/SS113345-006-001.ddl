ALTER TABLE SUPR_090 
MODIFY (val_enc_finan NUMBER(15,2));
/

ALTER TABLE SUPR_580 
MODIFY (valor_preco NUMBER(18,5));
/

exec inter_pr_recompile;
