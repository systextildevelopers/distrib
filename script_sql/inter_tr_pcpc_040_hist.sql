CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_040_HIST" 
AFTER INSERT OR
      DELETE OR
      UPDATE OF periodo_producao,    ordem_confeccao,     codigo_estagio,      proconf_nivel99,     proconf_grupo,
                proconf_subgrupo,    proconf_item,        qtde_pecas_prog,     qtde_pecas_prod,     qtde_conserto,
                qtde_pecas_2a,       estagio_anterior,    situacao_ordem,      numero_ordem,        seq_ordem_serv,
                qtde_perdas,         qtde_programada,     sequencia_estagio,   estagio_depende,     usuario,
                codigo_familia,      seq_operacao,        qtde_pessoas_prev,   codigo_balanceio,    numero_id_tmrp_650,
                sequencia_enfesto,   exclui_pacote,       seq_quebra,          situacao_em_prod_a_prod,
                qtde_em_producao_pacote,                  qtde_a_produzir_pacote,
                qtde_disponivel_baixa, codigo_empresa, cod_estagio_agrupador
             ON pcpc_040
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_area_producao          number(1);
   ws_ordem_producao         number(9);
   ws_periodo_producao       number(4);
   ws_ordem_confeccao        number(9);
   ws_tipo_historico         number(1);

   ws_descricao_historico    varchar2(4000);
   ws_tipo_ocorr             varchar2(1);

   ws_historico_ordens       number(1);

   ws_nome_programa hdoc_090.programa%type;
   
   ws_codigo_empresa         number(3);
   ws_codigo_estagio         number(5);
begin

   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   begin
      select empr_002.historico_ordens_conf
      into ws_historico_ordens
      from empr_002;
   end;

   ws_nome_programa := inter_fn_nome_programa(ws_sid); 

   -- TABELA DE HISTORICO DE PRODUCAO (EST�GIOS): PCPC_040
   if ws_historico_ordens = 1
   then
      if INSERTING then
         ws_area_producao         := 1;
         ws_ordem_producao        := :new.ordem_producao;
         ws_periodo_producao      := :new.periodo_producao;
         ws_ordem_confeccao       := :new.ordem_confeccao;
         ws_tipo_historico        := 1;
         ws_codigo_empresa        := :new.codigo_empresa;
         ws_codigo_estagio        := :new.codigo_estagio;
         
         ws_descricao_historico   := inter_fn_buscar_tag('lb34861#INCLUS�O EST�GIO (PCPC_040)',ws_locale_usuario,ws_usuario_systextil)
                                   ||chr(10)
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                   ||:new.proconf_nivel99 || '.' || :new.proconf_grupo || '.' || :new.proconf_subgrupo  || '.' || :new.proconf_item
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb10253#PERIODO PRODU��O:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.periodo_producao
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb06122#ORDEM CONFEC��O:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.ordem_confeccao
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb17425#C�DIGO EST�GIO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.codigo_estagio
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb00127#CODIGO EMPRESA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.codigo_empresa
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb34863#QTDE PE�AS PROG:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.qtde_pecas_prog
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39584#QTDE PE�AS PROD:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.qtde_pecas_prod
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39585#QTDE CONSERTO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.qtde_conserto
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39587#2a QUALIDADE:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.qtde_pecas_2a
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb34862#EST�GIO ANTERIOR:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.estagio_anterior
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb09724#SITUA��O ORDEM:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.situacao_ordem
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb10901#N�MERO DA ORDEM:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.numero_ordem
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39593#SEQ. ORDEM SERV:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.seq_ordem_serv
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb10122#QTDE PERDAS:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.qtde_perdas
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb10328#QTDE. PROGRAMADA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.qtde_programada
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39588#SEQU�NCIA EST�GIO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.sequencia_estagio
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39589#EST�GIO DEPENDE:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.estagio_depende
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb09752#USUARIO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.usuario
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb04530#C�DIGO FAM�LIA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.codigo_familia
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb06610#SEQ.OPERA��O:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.seq_operacao
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39592#QTDE PESSOAS PREVISTAS:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.qtde_pessoas_prev
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb17422#NR BALANCEIO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.codigo_balanceio
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39591#N�MERO ID TMRP_650:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.numero_id_tmrp_650
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb38815#SEQ. ENFESTO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.sequencia_enfesto
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39590#EXCLUIR PACOTE:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.exclui_pacote
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb05430#QUEBRA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.seq_quebra
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb41905#QTDE EM PRODU��O:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.qtde_em_producao_pacote
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb10106#QTDE A PRODUZIR:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.qtde_a_produzir_pacote
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb42243#QTDE DISP. BAIXA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.qtde_disponivel_baixa
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb41910#SITUA��O EP/AP:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.situacao_em_prod_a_prod
                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb50576#COD ESTAGIO AGRUPADOR:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :new.cod_estagio_agrupador;
         ws_tipo_ocorr            := 'I';

      elsif DELETING then
            ws_area_producao         := 1;
            ws_ordem_producao        := :old.ordem_producao;
            ws_periodo_producao      := :old.periodo_producao;
            ws_ordem_confeccao       := :old.ordem_confeccao;
            ws_tipo_historico        := 1;
            ws_codigo_empresa        := :old.codigo_empresa;
            ws_codigo_estagio        := :old.codigo_estagio;

            ws_descricao_historico   := inter_fn_buscar_tag('lb34866#EXCLUS�O EST�GIO (PCPC_040)',ws_locale_usuario,ws_usuario_systextil)
                                   ||chr(10)
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                   ||:old.proconf_nivel99 || '.' || :old.proconf_grupo || '.' || :old.proconf_subgrupo  || '.' || :old.proconf_item
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb10253#PERIODO PRODU��O:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.periodo_producao
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb06122#ORDEM CONFEC��O:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.ordem_confeccao
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb17425#C�DIGO EST�GIO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.codigo_estagio
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb00127#CODIGO EMPRESA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.codigo_empresa
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb34863#QTDE PE�AS PROG:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.qtde_pecas_prog
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39584#QTDE PE�AS PROD:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.qtde_pecas_prod
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39585#QTDE CONSERTO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.qtde_conserto
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39587#2a QUALIDADE:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.qtde_pecas_2a
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb34862#EST�GIO ANTERIOR:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.estagio_anterior
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb09724#SITUA��O ORDEM:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.situacao_ordem
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb10901#N�MERO DA ORDEM:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.numero_ordem
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39593#SEQ. ORDEM SERV:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.seq_ordem_serv
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb10122#QTDE PERDAS:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.qtde_perdas
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb10328#QTDE. PROGRAMADA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.qtde_programada
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39588#SEQU�NCIA EST�GIO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.sequencia_estagio
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39589#EST�GIO DEPENDE:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.estagio_depende
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb09752#USUARIO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.usuario
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb04530#C�DIGO FAM�LIA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.codigo_familia
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb06610#SEQ.OPERA��O:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.seq_operacao
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39592#QTDE PESSOAS PREVISTAS:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.qtde_pessoas_prev
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb17422#NR BALANCEIO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.codigo_balanceio
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39591#N�MERO ID TMRP_650:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.numero_id_tmrp_650
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb38815#SEQ. ENFESTO:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.sequencia_enfesto
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb39590#EXCLUIR PACOTE:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.exclui_pacote
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb05430#QUEBRA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.seq_quebra
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb41905#QTDE EM PRODU��O:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.qtde_em_producao_pacote
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb10106#QTDE A PRODUZIR:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.qtde_a_produzir_pacote
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb42243#QTDE DISP. BAIXA:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.qtde_disponivel_baixa
                                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb41910#SITUA��O EP/AP:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.situacao_em_prod_a_prod
                   ||chr(10)
                                   ||inter_fn_buscar_tag('lb50576#COD ESTAGIO AGRUPADOR:',ws_locale_usuario,ws_usuario_systextil)   || ' ' || :old.cod_estagio_agrupador;
                                   
            ws_tipo_ocorr         := 'D';

      elsif UPDATING then
            ws_area_producao         := 1;
            ws_ordem_producao        := :old.ordem_producao;
            ws_periodo_producao      := :old.periodo_producao;
            ws_ordem_confeccao       := :old.ordem_confeccao;
            
            ws_tipo_historico        := 1;
            if :new.codigo_empresa <> :old.codigo_empresa
            then
               ws_tipo_historico        := 3;
            end if;
            
            ws_codigo_empresa        := :new.codigo_empresa;
            ws_codigo_estagio        := :new.codigo_estagio;
            
            if (ws_tipo_historico != 3)
            then
               ws_descricao_historico   := inter_fn_buscar_tag('lb34867#ALTERA��O EST�GIO (PCPC_040)',ws_locale_usuario,ws_usuario_systextil)
                                      ||chr(10);
   
               ws_descricao_historico := ws_descricao_historico     || chr(10)
                                      || inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                      || :new.proconf_nivel99 || '.' || :new.proconf_grupo || '.' || :new.proconf_subgrupo  || '.' || :new.proconf_item;
           end if;
           
           if :old.periodo_producao <> :new.periodo_producao
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb10253#PERIODO PRODU��O:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.periodo_producao
                                   || '->' || :new.periodo_producao;
           end if;

           if :old.ordem_confeccao  <> :new.ordem_confeccao
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb06122#ORDEM CONFEC��O:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.ordem_confeccao
                                   || '->' || :new.ordem_confeccao;
           end if;

            if (ws_tipo_historico != 3)
            then
                ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb17425#C�DIGO EST�GIO:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.codigo_estagio;
           end if;
           
           if :old.codigo_empresa <> :new.codigo_empresa
           then 
              if (ws_tipo_historico != 3)
              then
                 ws_descricao_historico := ws_descricao_historico     || chr(10);
              end if;
              
              ws_descricao_historico := ws_descricao_historico
                                   ||inter_fn_buscar_tag('lb00127#CODIGO EMPRESA:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.codigo_empresa
                                   || '->' || :new.codigo_empresa;
           end if;


           if :old.qtde_pecas_prog <> :new.qtde_pecas_prog
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb34863#QTDE PE�AS PROG:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.qtde_pecas_prog
                                   || '->' || :new.qtde_pecas_prog;
           end if;

           if :old.qtde_pecas_prod <> :new.qtde_pecas_prod
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb39584#QTDE PE�AS PROD:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.qtde_pecas_prod
                                   || '->' || :new.qtde_pecas_prod;
           end if;

           if :old.qtde_conserto <> :new.qtde_conserto
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb39585#QTDE CONSERTO:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.qtde_conserto
                                   || '->' || :new.qtde_conserto;
           end if;

           if :old.qtde_pecas_2a <> :new.qtde_pecas_2a
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb39587#2a QUALIDADE:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.qtde_pecas_2a
                                   || '->' || :new.qtde_pecas_2a;
           end if;

           if :old.estagio_anterior <> :new.estagio_anterior
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb34862#EST�GIO ANTERIOR:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.estagio_anterior
                                   || '->' || :new.estagio_anterior;
           end if;

           if :old.situacao_ordem <> :new.situacao_ordem
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb09724#SITUA��O ORDEM:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.situacao_ordem
                                   || '->' || :new.situacao_ordem;
            end if;

           if :old.numero_ordem <> :new.numero_ordem
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb10901#N�MERO DA ORDEM:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.numero_ordem
                                   || '->' || :new.numero_ordem;
           end if;

           if :old.seq_ordem_serv <> :new.seq_ordem_serv
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb39593#SEQ. ORDEM SERV:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.seq_ordem_serv
                                   || '->' || :new.seq_ordem_serv;
           end if;

           if :old.qtde_perdas <> :new.qtde_perdas
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb10122#QTDE PERDAS:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.qtde_perdas
                                   || '->' || :new.qtde_perdas;
           end if;

           if :old.qtde_programada <> :new.qtde_programada
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb10328#QTDE. PROGRAMADA:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.qtde_programada
                                   || '->' || :new.qtde_programada;
           end if;

           if :old.sequencia_estagio <> :new.sequencia_estagio
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb39588#SEQU�NCIA EST�GIO:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.sequencia_estagio
                                   || '->' || :new.sequencia_estagio;
           end if;

           if :old.estagio_depende <> :new.estagio_depende
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb39589#EST�GIO DEPENDE:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.estagio_depende
                                   || '->' || :new.estagio_depende;
           end if;

           if :old.usuario <> :new.usuario
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb09752#USUARIO:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.usuario
                                   || '->' || :new.usuario;
           end if;

           if :old.codigo_familia <> :new.codigo_familia
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb04530#C�DIGO FAM�LIA:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.codigo_familia
                                   || '->' || :new.codigo_familia;
           end if;

           if :old.seq_operacao <> :new.seq_operacao
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb06610#SEQ.OPERA��O:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.seq_operacao
                                   || '->' || :new.seq_operacao;
           end if;

           if :old.qtde_pessoas_prev <> :new.qtde_pessoas_prev
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb39592#QTDE PESSOAS PREVISTAS:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.qtde_pessoas_prev
                                   || '->' || :new.qtde_pessoas_prev;
           end if;

           if :old.codigo_balanceio <> :new.codigo_balanceio
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb17422#NR BALANCEIO:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.codigo_balanceio
                                   || '->' || :new.codigo_balanceio;
           end if;

           if :old.numero_id_tmrp_650 <> :new.numero_id_tmrp_650
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb39591#N�MERO ID TMRP_650:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.numero_id_tmrp_650
                                   || '->' || :new.numero_id_tmrp_650;
           end if;

           if :old.sequencia_enfesto <> :new.sequencia_enfesto
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb38815#SEQ. ENFESTO:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.sequencia_enfesto
                                   || '->' || :new.sequencia_enfesto;
           end if;

           if :old.exclui_pacote <> :new.exclui_pacote
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                   ||inter_fn_buscar_tag('lb39590#EXCLUIR PACOTE:',ws_locale_usuario,ws_usuario_systextil)
                                   || ' '  || :old.exclui_pacote
                                   || '->' || :new.exclui_pacote;
           end if;

           if :old.seq_quebra <> :new.seq_quebra
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                       ||inter_fn_buscar_tag('lb05430#QUEBRA:',ws_locale_usuario,ws_usuario_systextil)
                                       || ' '  || :old.seq_quebra
                                       || '->' || :new.seq_quebra;
           end if;


           if :old.qtde_em_producao_pacote <> :new.qtde_em_producao_pacote
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                       ||inter_fn_buscar_tag('lb41905#QTDE EM PRODU��O:',ws_locale_usuario,ws_usuario_systextil)
                                       || ' '  || :old.qtde_em_producao_pacote
                                       || '->' || :new.qtde_em_producao_pacote;
           end if;

           if :old.qtde_a_produzir_pacote <> :new.qtde_a_produzir_pacote
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                       ||inter_fn_buscar_tag('lb10106#QTDE A PRODUZIR:',ws_locale_usuario,ws_usuario_systextil)
                                       || ' '  || :old.qtde_a_produzir_pacote
                                       || '->' || :new.qtde_a_produzir_pacote;
           end if;

           if :old.situacao_em_prod_a_prod <> :new.situacao_em_prod_a_prod
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                       ||inter_fn_buscar_tag('lb41910#SITUA��O EP/AP:',ws_locale_usuario,ws_usuario_systextil)
                                       || ' '  || :old.situacao_em_prod_a_prod
                                       || '->' || :new.situacao_em_prod_a_prod;
           end if;

           if :old.qtde_disponivel_baixa <> :new.qtde_disponivel_baixa
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                       ||inter_fn_buscar_tag('lb42243#QTDE DISP. BAIXA:',ws_locale_usuario,ws_usuario_systextil)
                                       || ' '  || :old.qtde_disponivel_baixa
                                       || '->' || :new.qtde_disponivel_baixa;
           end if;
       
       if :old.cod_estagio_agrupador <> :new.cod_estagio_agrupador
           then ws_descricao_historico := ws_descricao_historico     || chr(10)
                                       ||inter_fn_buscar_tag('lb50576#COD ESTAGIO AGRUPADOR:',ws_locale_usuario,ws_usuario_systextil)
                                       || ' '  || :old.cod_estagio_agrupador
                                       || '->' || :new.cod_estagio_agrupador;
           end if;
                                       
           ws_tipo_ocorr         := 'A';
      end if;

      INSERT INTO hist_010 (
         area_producao,           ordem_producao,          periodo_producao,
         ordem_confeccao,         tipo_historico,          descricao_historico,
         tipo_ocorr,              data_ocorr,              hora_ocorr,
         usuario_rede,            maquina_rede,            aplicacao,
         usuario_sistema,         nome_programa,           codigo_empresa,
         codigo_estagio
      ) VALUES (
         ws_area_producao,        ws_ordem_producao,       ws_periodo_producao,
         ws_ordem_confeccao,      ws_tipo_historico,       ws_descricao_historico,
         ws_tipo_ocorr,           sysdate,                 sysdate,
         ws_usuario_rede,         ws_maquina_rede,         ws_aplicativo,
         ws_usuario_systextil,    ws_nome_programa,        ws_codigo_empresa,
         ws_codigo_estagio
      );

   end if;
end inter_tr_pcpc_040_hist;






-- ALTER TRIGGER "INTER_TR_PCPC_040_HIST" ENABLE

/

exec inter_pr_recompile;

