create or replace function inter_fn_data_vencto_retencoes(p_data_base in date,
                                                          p_retencao  in varchar2)
return date
is
   v_dia_vencto_irrf   number;
   v_dia_vencto_inss   number;
   v_dia_vencto_iss    number;
   v_dia_vencto_pis    number;
   v_dia_vencto_cofins number;
   v_dia_vencto_csl    number;
   v_dia_vencto_csrf   number;
   
   v_data_vencto       date;
   v_data_vencto_final date;
   
begin

   begin
      select nvl(empr_002.dia_vencto_irrf, 1),   nvl(empr_002.dia_vencto_inss, 1),
             nvl(empr_002.dia_vencto_iss, 1),    nvl(empr_002.dia_vencto_pis, 1),
             nvl(empr_002.dia_vencto_cofins, 1), nvl(empr_002.dia_vencto_csl, 1),
             nvl(empr_002.dia_vencto_csrf, 1)
      into   v_dia_vencto_irrf,        v_dia_vencto_inss,          v_dia_vencto_iss,
             v_dia_vencto_pis,         v_dia_vencto_cofins,        v_dia_vencto_csl,
             v_dia_vencto_csrf
      from empr_002;

   exception
      when others then
         v_dia_vencto_irrf   := 1;
         v_dia_vencto_inss   := 1;
         v_dia_vencto_iss    := 1;
         v_dia_vencto_pis    := 1;
         v_dia_vencto_cofins := 1;
         v_dia_vencto_csl    := 1;
         v_dia_vencto_csrf   := 1;
   end;

   case
      when p_retencao = 'IRRF'   then v_data_vencto := add_months(trunc(p_data_base, 'MM'), 1) + v_dia_vencto_irrf - 1;
      when p_retencao = 'INSS'   then v_data_vencto := add_months(trunc(p_data_base, 'MM'), 1) + v_dia_vencto_inss - 1;
      when p_retencao = 'ISS'    then v_data_vencto := add_months(trunc(p_data_base, 'MM'), 1) + v_dia_vencto_iss - 1;
      when p_retencao = 'PIS'    then v_data_vencto := add_months(trunc(p_data_base, 'MM'), 1) + v_dia_vencto_pis - 1;
      when p_retencao = 'COFINS' then v_data_vencto := add_months(trunc(p_data_base, 'MM'), 1) + v_dia_vencto_cofins - 1;
      when p_retencao = 'CSL'    then v_data_vencto := add_months(trunc(p_data_base, 'MM'), 1) + v_dia_vencto_csl - 1;
      when p_retencao = 'CSRF'   then v_data_vencto := add_months(trunc(p_data_base, 'MM'), 1) + v_dia_vencto_csrf - 1;
      else v_data_vencto := p_data_base;
   end case;

   begin
      select data_calendario into v_data_vencto_final from (

         select basi_260.data_calendario from basi_260
         where basi_260.data_calendario <= v_data_vencto
         and   basi_260.dia_util_finan   = 0
         order by data_calendario desc)
      where rownum = 1;

   exception
      when others then v_data_vencto_final := v_data_vencto;
   end;

   return(v_data_vencto_final);
end;

/
