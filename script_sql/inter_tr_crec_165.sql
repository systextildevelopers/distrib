
  CREATE OR REPLACE TRIGGER "INTER_TR_CREC_165" 
  before insert on crec_165
  for each row

declare
  v_id_registro number;
begin
   select seq_crec_165.nextval into v_id_registro from dual;
   :new.lote_cheque := v_id_registro;
end;

-- ALTER TRIGGER "INTER_TR_CREC_165" ENABLE
 

/

exec inter_pr_recompile;

