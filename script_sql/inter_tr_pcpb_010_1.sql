
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_010_1" 
   before delete or
          update of cod_cancelamento
   on pcpb_010
   for each row

declare
   v_nr_registro_141 number;
   v_nr_registro_031 number;

begin
   -- tmrp_141.tipo_reserva = 7 - USADO SOMENTE PARA REVESTIDORAS
   -- NAO VAI TER TMRP_140 (RESUMO)
   if updating
   then
      if :new.cod_cancelamento <> 0
      then
         select count(*)
         into v_nr_registro_141
         from tmrp_141
         where tmrp_141.ordem_producao  = :old.ordem_producao
           and tmrp_141.tipo_reserva    = 7;

         if v_nr_registro_141 > 0
         then
            delete tmrp_141
            where tmrp_141.ordem_producao  = :old.ordem_producao
              and tmrp_141.tipo_reserva    = 7;
         end if;

         select count(1)
         into v_nr_registro_031
         from pcpb_031
         where pcpb_031.ordem_producao = :old.ordem_producao;
         if v_nr_registro_031 > 0
         then
            delete pcpb_031
            where pcpb_031.ordem_producao = :old.ordem_producao;
         end if;
      end if;
   end if;

   if deleting
   then
      select count(*)
      into v_nr_registro_141
      from tmrp_141
      where tmrp_141.ordem_producao  = :old.ordem_producao
        and tmrp_141.tipo_reserva    = 7;

      if v_nr_registro_141 > 0
      then
         delete tmrp_141
         where tmrp_141.ordem_producao  = :old.ordem_producao
           and tmrp_141.tipo_reserva    = 7;
      end if;

      select count(1)
      into v_nr_registro_031
      from pcpb_031
      where pcpb_031.ordem_producao = :old.ordem_producao;
      if v_nr_registro_031 > 0
      then
         delete pcpb_031
         where pcpb_031.ordem_producao = :old.ordem_producao;
      end if;
   end if;
end inter_tr_pcpb_010_1;

-- ALTER TRIGGER "INTER_TR_PCPB_010_1" ENABLE
 

/

exec inter_pr_recompile;

