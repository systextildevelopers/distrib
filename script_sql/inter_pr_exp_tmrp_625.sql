
  CREATE OR REPLACE PROCEDURE "INTER_PR_EXP_TMRP_625" (
   p_ordem_planejamento       in number,
   p_pedido_venda             in number,
   p_pedido_reserva           in number,
   p_nivel_produto_princ      in varchar2,
   p_grupo_produto_princ      in varchar2,
   p_seq_estrutura_exp        in number,
   p_nivel_produto_exp        in varchar2,
   p_grupo_produto_exp        in varchar2,
   p_subgrupo_produto_exp     in varchar2,
   p_item_produto_exp         in varchar2,
   p_alternativa_produto_exp  in number,
   p_qtde_planej              in number
)
is

begin
   for reg_tmrp_625 in (select tmrp_625.ordem_planejamento,
                               tmrp_625.pedido_venda,
                               tmrp_625.pedido_reserva,
                               tmrp_625.seq_projeto_origem,
                               tmrp_625.nivel_produto_origem,
                               tmrp_625.grupo_produto_origem,
                               tmrp_625.subgrupo_produto_origem,
                               tmrp_625.item_produto_origem,
                               tmrp_625.alternativa_produto_origem,
                               tmrp_625.seq_produto,
                               tmrp_625.nivel_produto,
                               tmrp_625.grupo_produto,
                               tmrp_625.subgrupo_produto,
                               tmrp_625.item_produto,
                               tmrp_625.alternativa_produto,
                               tmrp_625.consumo
                        from tmrp_625
                        where tmrp_625.ordem_planejamento         = p_ordem_planejamento
                          and tmrp_625.pedido_venda               = p_pedido_venda
                          and tmrp_625.pedido_reserva             = p_pedido_reserva
                          and tmrp_625.seq_produto_origem         = p_seq_estrutura_exp
                          and tmrp_625.nivel_produto_origem       = p_nivel_produto_exp
                          and tmrp_625.grupo_produto_origem       = p_grupo_produto_exp
                          and tmrp_625.subgrupo_produto_origem    = p_subgrupo_produto_exp
                          and tmrp_625.item_produto_origem        = p_item_produto_exp
                          and tmrp_625.alternativa_produto_origem = p_alternativa_produto_exp)
   loop
      begin
         insert into tmrp_622 (
            ordem_planejamento,                        pedido_venda,
            pedido_reserva,
            nivel_produto_princ,                       grupo_produto_princ,
            nivel_produto_origem,                      grupo_produto_origem,
            subgrupo_produto_origem,                   item_produto_origem,
            alternativa_produto_origem,                seq_produto,
            nivel_produto,                             grupo_produto,
            subgrupo_produto,                          item_produto,
            alternativa_produto,                       consumo,
            qtde_reserva_planejada
         ) values (
            reg_tmrp_625.ordem_planejamento,           reg_tmrp_625.pedido_venda,
            reg_tmrp_625.pedido_reserva,
            p_nivel_produto_princ,                     p_grupo_produto_princ,
            reg_tmrp_625.nivel_produto_origem,         reg_tmrp_625.grupo_produto_origem,
            reg_tmrp_625.subgrupo_produto_origem,      reg_tmrp_625.item_produto_origem,
            reg_tmrp_625.alternativa_produto_origem,   reg_tmrp_625.seq_produto,
            reg_tmrp_625.nivel_produto,                reg_tmrp_625.grupo_produto,
            reg_tmrp_625.subgrupo_produto,             reg_tmrp_625.item_produto,
            reg_tmrp_625.alternativa_produto,          reg_tmrp_625.consumo,
            p_qtde_planej * reg_tmrp_625.consumo
         );
      end;

      if  reg_tmrp_625.nivel_produto = '7'
      or  reg_tmrp_625.nivel_produto = '4'
      or  reg_tmrp_625.nivel_produto = '2'
      then
         inter_pr_exp_tmrp_625 (reg_tmrp_625.ordem_planejamento,
                                reg_tmrp_625.pedido_venda,
                                reg_tmrp_625.pedido_reserva,
                                p_nivel_produto_princ,
                                p_grupo_produto_princ,
                                reg_tmrp_625.seq_produto,
                                reg_tmrp_625.nivel_produto,
                                reg_tmrp_625.grupo_produto,
                                reg_tmrp_625.subgrupo_produto,
                                reg_tmrp_625.item_produto,
                                reg_tmrp_625.alternativa_produto,
                                p_qtde_planej * reg_tmrp_625.consumo);
      end if;
   end loop;
end inter_pr_exp_tmrp_625;

 

/

exec inter_pr_recompile;

