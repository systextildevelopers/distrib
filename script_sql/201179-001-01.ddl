CREATE TABLE OBRF_119 (
COD_DEPOSITO  NUMBER(4),
COD_REP       NUMBER(6),
ORIGEM_PED    NUMBER(3),
SERIE         VARCHAR2(4),
TIPO_VOL      NUMBER(7)
);


comment on table obrf_119 is ' Configuração de Notas para Danfe Simplificada'; 
/

exec inter_pr_recompile;
