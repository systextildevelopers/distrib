create or replace trigger inter_tr_pedi_305_log 
after insert or delete or update 
on pedi_305 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    begin 
       select hdoc_090.programa 
       into v_nome_programa 
       from hdoc_090 
       where hdoc_090.sid = ws_sid 
         and rownum       = 1 
         and hdoc_090.programa not like '%menu%' 
         and hdoc_090.programa not like '%_m%'; 
       exception 
         when no_data_found then            v_nome_programa := 'SQL'; 
    end; 
 
 
 
 if inserting 
 then 
    begin 
 
        insert into pedi_305_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           nivel_OLD,   /*8*/ 
           nivel_NEW,   /*9*/ 
           grupo_OLD,   /*10*/ 
           grupo_NEW,   /*11*/ 
           subgrupo_OLD,   /*12*/ 
           subgrupo_NEW,   /*13*/ 
           item_OLD,   /*14*/ 
           item_NEW,   /*15*/ 
           tipo_deposito_OLD,   /*16*/ 
           tipo_deposito_NEW,   /*17*/ 
           dataini_OLD,   /*18*/ 
           dataini_NEW,   /*19*/ 
           datafim_OLD,   /*20*/ 
           datafim_NEW,   /*21*/ 
           por_grade_OLD,   /*22*/ 
           por_grade_NEW,   /*23*/ 
           codigo_empresa_OLD,   /*24*/ 
           codigo_empresa_NEW    /*25*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           '',/*8*/
           :new.nivel, /*9*/   
           '',/*10*/
           :new.grupo, /*11*/   
           '',/*12*/
           :new.subgrupo, /*13*/   
           '',/*14*/
           :new.item, /*15*/   
           0,/*16*/
           :new.tipo_deposito, /*17*/   
           null,/*18*/
           :new.dataini, /*19*/   
           null,/*20*/
           :new.datafim, /*21*/   
           0,/*22*/
           :new.por_grade, /*23*/   
           0,/*24*/
           :new.codigo_empresa /*25*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into pedi_305_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           nivel_OLD, /*8*/   
           nivel_NEW, /*9*/   
           grupo_OLD, /*10*/   
           grupo_NEW, /*11*/   
           subgrupo_OLD, /*12*/   
           subgrupo_NEW, /*13*/   
           item_OLD, /*14*/   
           item_NEW, /*15*/   
           tipo_deposito_OLD, /*16*/   
           tipo_deposito_NEW, /*17*/   
           dataini_OLD, /*18*/   
           dataini_NEW, /*19*/   
           datafim_OLD, /*20*/   
           datafim_NEW, /*21*/   
           por_grade_OLD, /*22*/   
           por_grade_NEW, /*23*/   
           codigo_empresa_OLD, /*24*/   
           codigo_empresa_NEW  /*25*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.nivel,  /*8*/  
           :new.nivel, /*9*/   
           :old.grupo,  /*10*/  
           :new.grupo, /*11*/   
           :old.subgrupo,  /*12*/  
           :new.subgrupo, /*13*/   
           :old.item,  /*14*/  
           :new.item, /*15*/   
           :old.tipo_deposito,  /*16*/  
           :new.tipo_deposito, /*17*/   
           :old.dataini,  /*18*/  
           :new.dataini, /*19*/   
           :old.datafim,  /*20*/  
           :new.datafim, /*21*/   
           :old.por_grade,  /*22*/  
           :new.por_grade, /*23*/   
           :old.codigo_empresa,  /*24*/  
           :new.codigo_empresa  /*25*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into pedi_305_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           nivel_OLD, /*8*/   
           nivel_NEW, /*9*/   
           grupo_OLD, /*10*/   
           grupo_NEW, /*11*/   
           subgrupo_OLD, /*12*/   
           subgrupo_NEW, /*13*/   
           item_OLD, /*14*/   
           item_NEW, /*15*/   
           tipo_deposito_OLD, /*16*/   
           tipo_deposito_NEW, /*17*/   
           dataini_OLD, /*18*/   
           dataini_NEW, /*19*/   
           datafim_OLD, /*20*/   
           datafim_NEW, /*21*/   
           por_grade_OLD, /*22*/   
           por_grade_NEW, /*23*/   
           codigo_empresa_OLD, /*24*/   
           codigo_empresa_NEW /*25*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.nivel, /*8*/   
           '', /*9*/
           :old.grupo, /*10*/   
           '', /*11*/
           :old.subgrupo, /*12*/   
           '', /*13*/
           :old.item, /*14*/   
           '', /*15*/
           :old.tipo_deposito, /*16*/   
           0, /*17*/
           :old.dataini, /*18*/   
           null, /*19*/
           :old.datafim, /*20*/   
           null, /*21*/
           :old.por_grade, /*22*/   
           0, /*23*/
           :old.codigo_empresa, /*24*/   
           0 /*25*/
         );    
    end;    
 end if;    
end inter_tr_pedi_305_log;
