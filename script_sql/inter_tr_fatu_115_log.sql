
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_115_LOG" 
after insert or delete
or update of cod_rua, cod_box, nr_embalagem, cod_embalagem,
	transp9, transp4, transp2
on FATU_115
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid); 

 if inserting
 then
    begin

        insert into FATU_115_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           COD_RUA_OLD,   /*8*/
           COD_RUA_NEW,   /*9*/
           COD_BOX_OLD,   /*10*/
           COD_BOX_NEW,   /*11*/
           NR_EMBALAGEM_OLD,   /*12*/
           NR_EMBALAGEM_NEW,   /*13*/
           COD_EMBALAGEM_OLD,   /*14*/
           COD_EMBALAGEM_NEW,   /*15*/
           TRANSP9_OLD,   /*16*/
           TRANSP9_NEW,   /*17*/
           TRANSP4_OLD,   /*18*/
           TRANSP4_NEW,   /*19*/
           TRANSP2_OLD,   /*20*/
           TRANSP2_NEW    /*21*/
        ) values (
            'I', /*1*/
            sysdate, /*2*/
            sysdate,/*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           0,/*8*/
           :new.COD_RUA, /*9*/
           0,/*10*/
           :new.COD_BOX, /*11*/
           0,/*12*/
           :new.NR_EMBALAGEM, /*13*/
           0,/*14*/
           :new.COD_EMBALAGEM, /*15*/
           0,/*16*/
           :new.TRANSP9, /*17*/
           0,/*18*/
           :new.TRANSP4, /*19*/
           0,/*20*/
           :new.TRANSP2 /*21*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into FATU_115_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_RUA_OLD, /*8*/
           COD_RUA_NEW, /*9*/
           COD_BOX_OLD, /*10*/
           COD_BOX_NEW, /*11*/
           NR_EMBALAGEM_OLD, /*12*/
           NR_EMBALAGEM_NEW, /*13*/
           COD_EMBALAGEM_OLD, /*14*/
           COD_EMBALAGEM_NEW, /*15*/
           TRANSP9_OLD, /*16*/
           TRANSP9_NEW, /*17*/
           TRANSP4_OLD, /*18*/
           TRANSP4_NEW, /*19*/
           TRANSP2_OLD, /*20*/
           TRANSP2_NEW  /*21*/
        ) values (
            'A', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.COD_RUA,  /*8*/
           :new.COD_RUA, /*9*/
           :old.COD_BOX,  /*10*/
           :new.COD_BOX, /*11*/
           :old.NR_EMBALAGEM,  /*12*/
           :new.NR_EMBALAGEM, /*13*/
           :old.COD_EMBALAGEM,  /*14*/
           :new.COD_EMBALAGEM, /*15*/
           :old.TRANSP9,  /*16*/
           :new.TRANSP9, /*17*/
           :old.TRANSP4,  /*18*/
           :new.TRANSP4, /*19*/
           :old.TRANSP2,  /*20*/
           :new.TRANSP2  /*21*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into FATU_115_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_RUA_OLD, /*8*/
           COD_RUA_NEW, /*9*/
           COD_BOX_OLD, /*10*/
           COD_BOX_NEW, /*11*/
           NR_EMBALAGEM_OLD, /*12*/
           NR_EMBALAGEM_NEW, /*13*/
           COD_EMBALAGEM_OLD, /*14*/
           COD_EMBALAGEM_NEW, /*15*/
           TRANSP9_OLD, /*16*/
           TRANSP9_NEW, /*17*/
           TRANSP4_OLD, /*18*/
           TRANSP4_NEW, /*19*/
           TRANSP2_OLD, /*20*/
           TRANSP2_NEW /*21*/
        ) values (
            'D', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede,/*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.COD_RUA, /*8*/
           0, /*9*/
           :old.COD_BOX, /*10*/
           0, /*11*/
           :old.NR_EMBALAGEM, /*12*/
           0, /*13*/
           :old.COD_EMBALAGEM, /*14*/
           0, /*15*/
           :old.TRANSP9, /*16*/
           0, /*17*/
           :old.TRANSP4, /*18*/
           0, /*19*/
           :old.TRANSP2, /*20*/
           0 /*21*/
         );
    end;
 end if;
end inter_tr_FATU_115_log;
-- ALTER TRIGGER "INTER_TR_FATU_115_LOG" ENABLE
 

/

exec inter_pr_recompile;

