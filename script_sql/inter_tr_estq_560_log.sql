
  CREATE OR REPLACE TRIGGER "INTER_TR_ESTQ_560_LOG" 
   after insert or
         delete or
         update of nivel_produto, grupo_produto,  subgru_produto,
                   item_produto,  sequencia_prog, qtde_prog_futura,
                   observacao,    data_hora_ins,  inativo
   on estq_560
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   long_aux                  varchar2(4000);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   if inserting
   then
      INSERT INTO hist_100
         ( tabela,            operacao,
           data_ocorr,        usuario_rede,
           maquina_rede,      str01,
           aplicacao,         long01
         )
      VALUES
        ( 'ESTQ_560',        'I',
          sysdate,           ws_usuario_rede,
          ws_maquina_rede,   :new.nivel_produto || '.' ||
                             :new.grupo_produto || '.' ||
                             :new.subgru_produto|| '.' ||
                             :new.item_produto,
          ws_aplicativo,     'NOVA PROGRAMAÇÃO: ' || chr(10) || chr(10) ||
                             'PRODUTO: ' ||
                             :new.nivel_produto || '.' ||
                             :new.grupo_produto || '.' ||
                             :new.subgru_produto|| '.' ||
                             :new.item_produto  || chr(10) ||
                             'SEQ DE PROG: ' ||
                             to_char(:new.sequencia_prog,'999999000') || chr(10) ||
                             'QTDE PROGRAMADA: ' ||
                             to_char(:new.qtde_prog_futura,'9999999990000.000') || chr(10) ||
                             'ATIVO/INATIVO: ' ||
                             to_char(:new.inativo,'9') || ' (0 - Ativo, 1 - Inativo) ' || chr(10) ||
                             'OBSERVAÇÃO: ' ||
                             :new.observacao);
   end if;

   if updating and
      (:new.nivel_produto     <> :old.nivel_produto     or
       :new.grupo_produto     <> :old.grupo_produto     or
       :new.subgru_produto    <> :old.subgru_produto    or
       :new.item_produto      <> :old.item_produto      or
       :new.sequencia_prog    <> :old.sequencia_prog    or
       :new.qtde_prog_futura  <> :old.qtde_prog_futura  or
       :new.inativo           <> :old.inativo           or
       :new.observacao        <> :old.observacao        or
       :old.observacao     is null)
   then
      long_aux := '';
      long_aux := long_aux || 'ALTERAÇÃO DE PROGRAMAÇÃO: ' || chr(10) || chr(10);
      if :new.nivel_produto     <> :old.nivel_produto  or
         :new.grupo_produto     <> :old.grupo_produto  or
         :new.subgru_produto    <> :old.subgru_produto or
         :new.item_produto      <> :old.item_produto
      then
         long_aux := long_aux || 'PRODUTO: '         || :old.nivel_produto || '.'
                                                     || :old.grupo_produto || '.'
                                                     || :old.subgru_produto|| '.'
                                                     || :old.item_produto  || ' -> '
                                                     || :new.nivel_produto || '.'
                                                     || :new.grupo_produto || '.'
                                                     || :new.subgru_produto|| '.'
                                                     || :new.item_produto  || chr(10);
      end if;

      if :new.sequencia_prog    <> :old.sequencia_prog
      then
         long_aux := long_aux || 'SEQ PROG: '        || to_char(:old.sequencia_prog,'999999000') || ' -> '
                                                     || to_char(:new.sequencia_prog,'999999000') || chr(10);
      end if;

      if :new.qtde_prog_futura  <> :old.qtde_prog_futura
      then
         long_aux := long_aux || 'QTDE PROGRAMADA: ' || to_char(:old.qtde_prog_futura,'9999999990000.000') || ' -> '
                                                     || to_char(:new.qtde_prog_futura,'9999999990000.000') || chr(10);
      end if;

      if :new.inativo           <> :old.inativo
      then
         long_aux := long_aux || 'ATIVO/INATIVO '    || to_char(:old.inativo,'9') || ' -> '
                                                     || to_char(:new.inativo,'9') || chr(10);
      end if;

      if :new.observacao     <> :old.observacao or
         (:old.observacao is null and :new.observacao is not null)
      then
          long_aux := long_aux || 'OBS.: '           || :old.observacao ||' -> '
                                                     || :new.observacao || chr(10);
     end if;

      /******* FAZ INSERT ******/
      INSERT INTO hist_100
         ( tabela,            operacao,
           data_ocorr,        usuario_rede,
           maquina_rede,      str01,
           aplicacao,         long01
         )
      VALUES
         ( 'ESTQ_560',        'A',
           sysdate,           ws_usuario_rede,
           ws_maquina_rede,   :new.nivel_produto || '.'
                           || :new.grupo_produto || '.'
                           || :new.subgru_produto|| '.'
                           || :new.item_produto,
           ws_aplicativo,     long_aux
        );
   end if;
   if deleting
   then
      INSERT INTO hist_100
         ( tabela,            operacao,
           data_ocorr,        usuario_rede,
           maquina_rede,      str01,
           aplicacao,         long01
         )
      VALUES
         ( 'ESTQ_560',        'D',
           sysdate,           ws_usuario_rede,
           ws_maquina_rede,   :old.nivel_produto || '.'
                           || :old.grupo_produto || '.'
                           || :old.subgru_produto|| '.'
                           || :old.item_produto,
           ws_aplicativo,     'EXCLUSÃO DE PROGRAMAÇÃO' || chr(10) || chr(10)
                           || :old.nivel_produto || '.'
                           || :old.grupo_produto || '.'
                           || :old.subgru_produto|| '.'
                           || :old.item_produto  || chr(10)
                           || 'SEQ DE PROG: '
                           ||  to_char(:old.sequencia_prog,'999999000') || chr(10)
                           || 'QTDE PROGRAMADA: '
                           ||  to_char(:old.qtde_prog_futura,'9999999990000.000') || chr(10)
                           ||  'ATIVO/INATIVO: '
                           ||  to_char(:old.inativo,'9') || '(0 - Ativo, 1 - Inativo) ' || chr(10)
                           || 'OBSERVAÇÃO: ' || :old.observacao);
   end if;
end inter_tr_estq_560_log;

-- ALTER TRIGGER "INTER_TR_ESTQ_560_LOG" ENABLE
 

/

exec inter_pr_recompile;

