
  CREATE OR REPLACE FUNCTION "INTER_FN_DEB_PEDI_INTE_FATU" (p_empresa in number, p_cgc9 in number, p_cgc4 in number, p_cgc2 in number) return number is
  Result number;
begin

 declare

 acum_cred_corporacao_empr fatu_500.acum_cred_corporacao%type;
 est_nat_analise basi_160.estado%type;
 un_lim_max_ped pedi_012.tipo_limite%type;


 saldo_qtde_pedi  number;
 saldo_vlr_pedi   number;

 saldo_qtde_inte  number;
 saldo_vlr_inte   number;


 valor_liq110_total number;
 valor_unitario     number;
 qtde_pedida        number;
 qtde_totpedida110  number;
 valor              number;
 qtde_pedido_total  number;
 valor_saldo_100    number;
 qtde_saldo         number;
 qtde_saldo_100     number;
 valor_saldo        number;



 saldo_qtde_dupli number;
 saldo_vlr_dupli  number;

 begin


    acum_cred_corporacao_empr := 0;
    est_nat_analise           := '';
    un_lim_max_ped            := 0;


    saldo_qtde_pedi    :=0;
    saldo_vlr_pedi     :=0;

    saldo_qtde_inte    :=0;
    saldo_vlr_inte     :=0;


    valor_liq110_total :=0;
    valor_unitario     :=0;
    qtde_pedida        :=0;
    qtde_totpedida110  :=0;
    valor              :=0;
    qtde_pedido_total  :=0;
    valor_saldo_100    :=0;
    qtde_saldo         :=0;
    qtde_saldo_100     :=0;
    valor_saldo        :=0;

    saldo_qtde_dupli   :=0;
    saldo_vlr_dupli    :=0;



    --LE PARAMETROS DA FATU
    select f.acum_cred_corporacao into acum_cred_corporacao_empr
      from fatu_500 f
     where f.codigo_empresa = p_empresa;

	  --LE ESTADO DO CLIENTE SE NAO ACHAR NA PEDI LE NA INTE
    SELECT basi_160.estado into est_nat_analise
      FROM pedi_010, basi_160
     WHERE pedi_010.cgc_9 = p_cgc9
       AND pedi_010.cgc_4 = p_cgc4
       AND pedi_010.cgc_2 = p_cgc2
       AND pedi_010.cod_cidade = basi_160.cod_cidade;

    --LE tipo de credito do cliente se valor ou quantidade
    begin
       select pedi_012.tipo_limite into un_lim_max_ped
         from pedi_012
        where pedi_012.empresa  = p_empresa
          and pedi_012.cliente9 = p_cgc9
          and pedi_012.cliente4 = p_cgc4
          and pedi_012.cliente2 = p_cgc2;
    exception
    when no_data_found then
       begin
          select pedi_010.unidade_lim_ped into un_lim_max_ped
            from  pedi_010
           where pedi_010.cgc_9 = p_cgc9
             and pedi_010.cgc_4 = p_cgc4
             and pedi_010.cgc_2 = p_cgc2;
       exception
       when no_data_found then
          un_lim_max_ped := 2;
       end;
    end;


    if est_nat_analise = 'EX' or acum_cred_corporacao_empr = 2 then


       --PEDIDOS SYSTEXTIL
        select SUM(pedi_100.qtde_saldo_pedi), SUM(pedi_100.valor_saldo_pedi)
          into saldo_qtde_pedi, saldo_vlr_pedi
          from pedi_100
         where pedi_100.codigo_empresa   = p_empresa
           and pedi_100.cli_ped_cgc_cli9 = p_cgc9
           and pedi_100.cli_ped_cgc_cli4 = p_cgc4
           and pedi_100.cli_ped_cgc_cli2 = p_cgc2
           and pedi_100.situacao_venda <> 10
           and pedi_100.cod_cancelamento = 0
         group by pedi_100.cli_ped_cgc_cli9,
                  pedi_100.cli_ped_cgc_cli4,
                  pedi_100.cli_ped_cgc_cli2;



        --PEDIDOS SYSTEXTILNET
        for r_inte_100 in
           (SELECT pedido_venda, tecido_peca, desconto1, desconto2, desconto3
             from inte_100
            where inte_100.cliente9 =  p_cgc9
              and inte_100.cliente4 =  p_cgc4
              and inte_100.cliente2 =  p_cgc2
              and inte_100.codigo_empresa   = p_empresa
              and inte_100.tipo_registro  = 1)
        loop

            valor_liq110_total := 0;
						valor_unitario     := 0;
						qtde_pedida        := 0;
						qtde_totpedida110  := 0;
						valor              := 0;
						qtde_pedido_total  := 0;

            for r_inte_110 in
               (select inte_110.valor_unitario, inte_110.qtde_pedida,
                      round(round((inte_110.valor_unitario/100 * (100 - inte_110.percentual_desc/100)/100), 2) * (inte_110.qtde_pedida / 100), 2) vlr_liq_item
                 from inte_100, inte_110
                where inte_100.pedido_venda = r_inte_100.pedido_venda
                  and inte_110.pedido_venda = inte_100.pedido_venda)

            loop
                   valor_liq110_total := valor_liq110_total + r_inte_110.vlr_liq_item;
						    	 valor_unitario := r_inte_110.valor_unitario/100;
								   qtde_pedida := r_inte_110.qtde_pedida/100;
								   qtde_totpedida110 := qtde_totpedida110 + qtde_pedida;
							     valor := valor+(valor_unitario * qtde_pedida);
								   qtde_pedido_total := qtde_pedido_total + qtde_pedida;

						end loop;

						valor_liq110_total := valor_liq110_total - (valor_liq110_total*r_inte_100.desconto1)/100;
						valor_liq110_total := valor_liq110_total - (valor_liq110_total*r_inte_100.desconto2)/100;
						valor_liq110_total := valor_liq110_total - (valor_liq110_total*r_inte_100.desconto3)/100;

						valor_saldo_100 := valor_liq110_total;
						qtde_saldo_100  := qtde_totpedida110;

					  qtde_saldo := qtde_saldo + qtde_saldo_100;
						valor_saldo := valor_saldo + valor_saldo_100;




       end loop;
       saldo_qtde_inte := qtde_saldo;
       saldo_vlr_inte  := valor_saldo;

       --TITULOS
       begin
          select SUM(F.QUANTIDADE * DECODE(valor_duplicata, 0.00, 0.00, (saldo_duplicata * 100) / valor_duplicata) / 100),
                 SUM(F.SALDO_DUPLICATA)
                 into saldo_qtde_dupli,  saldo_vlr_dupli
            from fatu_070 F
           WHERE F.CODIGO_EMPRESA = p_empresa
             and f.cli_dup_cgc_cli9 = p_cgc9
             and f.cli_dup_cgc_cli4 = p_cgc4
             and f.cli_dup_cgc_cli2 = p_cgc2
           GROUP BY F.CLI_DUP_CGC_CLI9, F.CLI_DUP_CGC_CLI4, F.CLI_DUP_CGC_CLI2;
       exception
       when no_data_found then
          saldo_qtde_dupli := 0.00;
          saldo_vlr_dupli := 0.00;
       end;

       if un_lim_max_ped = 1 then
          Result:=saldo_qtde_pedi + saldo_qtde_inte + saldo_qtde_dupli;
       else
          Result:=saldo_vlr_pedi + saldo_vlr_inte + saldo_vlr_dupli;
       end if;
       return(Result);
    elsif est_nat_analise <> 'EX' and acum_cred_corporacao_empr = 1 then

       --PEDIDOS SYSTEXTIL
        select SUM(pedi_100.qtde_saldo_pedi), SUM(pedi_100.valor_saldo_pedi)
          into saldo_qtde_pedi, saldo_vlr_pedi
          from pedi_100
         where pedi_100.codigo_empresa   = p_empresa
           and pedi_100.cli_ped_cgc_cli9 = p_cgc9
           and pedi_100.situacao_venda <> 10
           and pedi_100.cod_cancelamento = 0
         group by pedi_100.cli_ped_cgc_cli9;

       --PEDIDOS SYSTEXTILNET
        for r_inte_100 in
           (SELECT pedido_venda, tecido_peca, desconto1, desconto2, desconto3
             from inte_100
            where inte_100.cliente9 =  p_cgc9
              and inte_100.cliente4 =  p_cgc4
              and inte_100.cliente2 =  p_cgc2
              and inte_100.codigo_empresa   = p_empresa
              and inte_100.tipo_registro  = 1)
        loop

            valor_liq110_total := 0;
						valor_unitario     := 0;
						qtde_pedida        := 0;
						qtde_totpedida110  := 0;
						valor              := 0;
						qtde_pedido_total  := 0;

            for r_inte_110 in
               (select inte_110.valor_unitario, inte_110.qtde_pedida,
                      round(round((inte_110.valor_unitario/100 * (100 - inte_110.percentual_desc/100)/100), 2) * (inte_110.qtde_pedida / 100), 2) vlr_liq_item
                 from inte_100, inte_110
                where inte_100.pedido_venda = r_inte_100.pedido_venda
                  and inte_110.pedido_venda = inte_100.pedido_venda)

            loop
                   valor_liq110_total := valor_liq110_total + r_inte_110.vlr_liq_item;
						    	 valor_unitario := r_inte_110.valor_unitario/100;
								   qtde_pedida := r_inte_110.qtde_pedida/100;
								   qtde_totpedida110 := qtde_totpedida110 + qtde_pedida;
							     valor := valor+(valor_unitario * qtde_pedida);
								   qtde_pedido_total := qtde_pedido_total + qtde_pedida;

						end loop;

						valor_liq110_total := valor_liq110_total - (valor_liq110_total*r_inte_100.desconto1)/100;
						valor_liq110_total := valor_liq110_total - (valor_liq110_total*r_inte_100.desconto2)/100;
						valor_liq110_total := valor_liq110_total - (valor_liq110_total*r_inte_100.desconto3)/100;

						valor_saldo_100 := valor_liq110_total;
						qtde_saldo_100  := qtde_totpedida110;

					  qtde_saldo := qtde_saldo + qtde_saldo_100;
						valor_saldo := valor_saldo + valor_saldo_100;

       end loop;
       saldo_qtde_inte := qtde_saldo;
       saldo_vlr_inte  := valor_saldo;


       --TITULOS
       begin
          select SUM(F.QUANTIDADE * DECODE(valor_duplicata, 0.00, 0.00, (saldo_duplicata * 100) / valor_duplicata) / 100),
                 SUM(F.SALDO_DUPLICATA)
                 into saldo_qtde_dupli,  saldo_vlr_dupli
            from fatu_070 F
           WHERE F.CODIGO_EMPRESA = p_empresa
             and f.cli_dup_cgc_cli9 = p_cgc9
             and f.cli_dup_cgc_cli4 = p_cgc4
             and f.cli_dup_cgc_cli2 = p_cgc2
           GROUP BY F.CLI_DUP_CGC_CLI9, F.CLI_DUP_CGC_CLI4, F.CLI_DUP_CGC_CLI2;
       exception
       when no_data_found then
          saldo_qtde_dupli := 0.00;
          saldo_vlr_dupli := 0.00;
       end;

        if un_lim_max_ped = 1 then
           Result:=saldo_qtde_pedi + saldo_qtde_inte + saldo_qtde_dupli;
        else
           Result:=saldo_vlr_pedi + saldo_vlr_inte + saldo_vlr_dupli;
        end if;
        return(Result);
     end if;


 end;


end inter_fn_deb_pedi_inte_fatu;
 

/

exec inter_pr_recompile;

