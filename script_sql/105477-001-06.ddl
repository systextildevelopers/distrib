alter table obrf_015
add unid_med_trib varchar2(10);

alter table obrf_015
modify unid_med_trib default ' ';

comment on column obrf_015.unid_med_trib is 'Unidade de medida tributaria para exterior';

exec inter_pr_recompile;
/
