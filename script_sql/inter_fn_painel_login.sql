
  CREATE OR REPLACE FUNCTION "INTER_FN_PAINEL_LOGIN" (
   p_usuario        in varchar2,
   p_codigo_empresa in number,
   p_nome_tela      in varchar2,
   p_setor          in number)
return number
is
   v_nro_reg        number;

   v_sid            number;
   v_status_conexao number;
   v_osuser         varchar2(30);
   v_logon_time_str varchar2(30);
   v_retorno        number;
   v_instancia_bd   number(9);

   v_h095_usuario          varchar2(250);
   v_h095_empresa          number;
   v_h095_sid              number;
   v_h095_usuario_rede     varchar2(30);
   v_h095_data_conexao_str varchar2(20);
   v_h095_instancia_bd     number(9);
begin
   for reg_hdoc_095 in (select hdoc_095.usuario,          hdoc_095.empresa,
                               hdoc_095.sid,              hdoc_095.usuario_rede,
                               hdoc_095.data_conexao_str, hdoc_095.instancia_bd
                        from hdoc_095
                        where  nome_tela = p_nome_tela
                          and  empresa   = p_codigo_empresa
                          and (((setor in (p_setor,999) and p_setor <> 999) or (setor <> 999 and p_setor = 999)) or p_nome_tela <> 'plan_confe'))
   loop
      v_status_conexao := 1;

      begin
         select v_lista_usuario_banco.status
         into v_status_conexao
         from v_lista_usuario_banco
         where v_lista_usuario_banco.sid            = reg_hdoc_095.sid
           and v_lista_usuario_banco.osuser         = reg_hdoc_095.usuario_rede
           and v_lista_usuario_banco.logon_time_str = reg_hdoc_095.data_conexao_str
           and v_lista_usuario_banco.inst_id        = reg_hdoc_095.instancia_bd;
      exception when others then
         v_status_conexao := 0;
      end;

      if v_status_conexao = 0
      then
         delete hdoc_095
         where hdoc_095.usuario           = reg_hdoc_095.usuario
           and hdoc_095.empresa           = reg_hdoc_095.empresa
           and hdoc_095.sid               = reg_hdoc_095.sid
           and hdoc_095.usuario_rede      = reg_hdoc_095.usuario_rede
           and hdoc_095.nome_tela         = p_nome_tela
           and hdoc_095.data_conexao_str  = reg_hdoc_095.data_conexao_str
           and hdoc_095.instancia_bd      = reg_hdoc_095.instancia_bd;
      end if;
   end loop;

   v_retorno := 1;

   begin
      select hdoc_095.usuario,          hdoc_095.empresa,
             hdoc_095.sid,              hdoc_095.usuario_rede,
             hdoc_095.data_conexao_str, hdoc_095.instancia_bd
      into   v_h095_usuario,            v_h095_empresa,
             v_h095_sid,                v_h095_usuario_rede,
             v_h095_data_conexao_str,   v_h095_instancia_bd
      from hdoc_095
      where  nome_tela = p_nome_tela
        and  empresa   = p_codigo_empresa
        and (((setor in (p_setor,999) and p_setor <> 999) or (setor <> 999 and p_setor = 999)) or p_nome_tela <> 'plan_confe')
        and  rownum    = 1;
      exception when others then
         v_retorno := 0;
   end;

   begin
      select v_lista_sessao_banco.sid,            v_lista_sessao_banco.osuser,
             v_lista_sessao_banco.logon_time_str, v_lista_sessao_banco.inst_id
      into   v_sid,                               v_osuser,
             v_logon_time_str,                    v_instancia_bd
      from v_lista_sessao_banco;
   end;

   begin
      select count(*)
      into v_nro_reg
      from hdoc_095
      where  hdoc_095.usuario      = p_usuario
        and  hdoc_095.empresa      = p_codigo_empresa
        and  hdoc_095.sid          = v_sid
        and  hdoc_095.usuario_rede = v_osuser
        and  hdoc_095.nome_tela    = p_nome_tela
        and  hdoc_095.instancia_bd = v_instancia_bd
        and (((setor in (p_setor,999) and p_setor <> 999) or (setor <> 999 and p_setor = 999)) or p_nome_tela <> 'plan_confe');
   end;

   if v_nro_reg = 0
   then
      begin
         insert into hdoc_095
            (usuario,           empresa,
             sid,               usuario_rede,
             data_conexao_str,  nome_tela,
             setor,             instancia_bd)
         values
             (p_usuario,        p_codigo_empresa,
              v_sid,            v_osuser,
              v_logon_time_str, p_nome_tela,
              p_setor,          v_instancia_bd);
      exception
         when others then
         v_retorno := 1;
      end;
   end if;

   return(v_retorno);

end inter_fn_painel_login;
 

/

exec inter_pr_recompile;

