create or replace TRIGGER INTER_TR_ESTQ_040_LOG
before insert or delete or update
of cditem_nivel99,
   cditem_grupo,
   cditem_subgrupo,
   cditem_item,
   lote_acomp,
   deposito,
   qtde_empenhada,
   qtde_estoque_atu,
   qtde_estoque_ant,
   qtde_estoque_mes,
   qtde_sugerida   on estq_040
for each row
declare
   -- local variables here
   v_cditem_nivel99_old    estq_040.cditem_nivel99%type;
   v_cditem_grupo_old      estq_040.cditem_grupo%type;
   v_cditem_subgrupo_old   estq_040.cditem_subgrupo%type;
   v_cditem_item_old       estq_040.cditem_item%type;
   v_lote_acomp_old        estq_040.lote_acomp%type;
   v_deposito_old          estq_040.deposito%type;
   v_qtde_empenhada_old    estq_040.qtde_empenhada%type;
   v_qtde_estoque_atu_old  estq_040.qtde_estoque_atu%type;
   v_qtde_estoque_ant_old  estq_040.qtde_estoque_ant%type;
   v_qtde_estoque_mes_old  estq_040.qtde_estoque_mes%type;
   v_qtde_estoque_bal_old  estq_040.qtde_estoque_bal%type;
   v_controle_balanco_old  estq_040.controle_balanco%type;
   v_data_ult_entrada_old  estq_040.data_ult_entrada%type;
   v_data_ult_saida_old    estq_040.data_ult_saida%type;
   v_qtde_sugerida_old     estq_040.qtde_sugerida%type;
   v_qtde_reserva_cor_old  estq_040.qtde_reserva_cor%type;
   v_cditem_nivel99_new    estq_040.cditem_nivel99%type;
   v_cditem_grupo_new 	   estq_040.cditem_grupo%type;
   v_cditem_subgrupo_new   estq_040.cditem_subgrupo%type;
   v_cditem_item_new	      estq_040.cditem_item%type;
   v_lote_acomp_new        estq_040.lote_acomp%type;
   v_deposito_new	         estq_040.deposito%type;
   v_qtde_empenhada_new    estq_040.qtde_empenhada%type;
   v_qtde_estoque_atu_new  estq_040.qtde_estoque_atu%type;
   v_qtde_estoque_ant_new  estq_040.qtde_estoque_ant%type;
   v_qtde_estoque_mes_new  estq_040.qtde_estoque_mes%type;
   v_qtde_estoque_bal_new  estq_040.qtde_estoque_bal%type;
   v_controle_balanco_new  estq_040.controle_balanco%type;
   v_data_ult_entrada_new  estq_040.data_ult_entrada%type;
   v_data_ult_saida_new	   estq_040.data_ult_saida%type;
   v_qtde_sugerida_new     estq_040.qtde_sugerida%type;
   v_qtde_reserva_cor_new  estq_040.qtde_reserva_cor%type;

   v_operacao              varchar(1);
   v_data_operacao         date;
   v_usuario_rede          varchar(20);
   v_maquina_rede          varchar(40);
   v_aplicativo            varchar(20);
   v_processo_systextil    estq_040_log.processo_systextil%type;
   v_sid                   number(9);
   v_empresa               number(3);
   v_usuario_systextil     varchar2(250);
   v_locale_usuario        varchar2(5);


begin

   -- Grava a data/hora da inserï¿¿ï¿¿o do registro (log)
   v_data_operacao := sysdate;

   -- Encontra dados do usuï¿¿rio da rede, mï¿¿quina e aplicativo que esta atualizando a ficha
   inter_pr_dados_usuario (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                           v_usuario_systextil,   v_empresa,        v_locale_usuario);

   v_processo_systextil := substr(inter_fn_nome_programa(v_sid), 1, 20); --inter_fn_nome_programa(v_sid);

   --alimenta as variï¿¿veis new caso seja insert ou update
   if inserting or updating
   then
      if inserting
      then v_operacao := 'I';
      else v_operacao := 'U';
      end if;

      v_cditem_nivel99_new    := :new.cditem_nivel99;
      v_cditem_grupo_new 	   := :new.cditem_grupo;
      v_cditem_subgrupo_new   := :new.cditem_subgrupo;
      v_cditem_item_new	      := :new.cditem_item;
      v_lote_acomp_new        := :new.lote_acomp;
      v_deposito_new	         := :new.deposito;
      v_qtde_empenhada_new    := :new.qtde_empenhada;
      v_qtde_estoque_atu_new  := :new.qtde_estoque_atu;
      v_qtde_estoque_ant_new  := :new.qtde_estoque_ant;
      v_qtde_estoque_mes_new  := :new.qtde_estoque_mes;
      v_qtde_estoque_bal_new  := :new.qtde_estoque_bal;
      v_controle_balanco_new  := :new.controle_balanco;
      v_data_ult_entrada_new  := :new.data_ult_entrada;
      v_data_ult_saida_new	   := :new.data_ult_saida;
      v_qtde_sugerida_new     := :new.qtde_sugerida;
      v_qtde_reserva_cor_new  := :new.qtde_reserva_cor;

	  :new.nome_prog_040 := ' ';

   end if; --fim do if inserting or updating


   --alimenta as variï¿¿veis old caso seja insert ou update
   if deleting or updating
   then
      if deleting
      then v_operacao := 'D';
      else v_operacao := 'U';
      end if;

      v_cditem_nivel99_old    := :old.cditem_nivel99;
      v_cditem_grupo_old 	   := :old.cditem_grupo;
      v_cditem_subgrupo_old   := :old.cditem_subgrupo;
      v_cditem_item_old	      := :old.cditem_item;
      v_lote_acomp_old        := :old.lote_acomp;
      v_deposito_old	         := :old.deposito;
      v_qtde_empenhada_old    := :old.qtde_empenhada;
      v_qtde_estoque_atu_old  := :old.qtde_estoque_atu;
      v_qtde_estoque_ant_old  := :old.qtde_estoque_ant;
      v_qtde_estoque_mes_old  := :old.qtde_estoque_mes;
      v_qtde_estoque_bal_old  := :old.qtde_estoque_bal;
      v_controle_balanco_old  := :old.controle_balanco;
      v_data_ult_entrada_old  := :old.data_ult_entrada;
      v_data_ult_saida_old	   := :old.data_ult_saida;
      v_qtde_sugerida_old     := :old.qtde_sugerida;
      v_qtde_reserva_cor_old  := :old.qtde_reserva_cor;

   end if; --fim do if inserting or updating

   --insere na estq_040_log o registro.
   if :new.nome_prog_040 <> 'INTER_PR_EMPENHO_ALMOXARIFADO'
   then
      begin
         insert into estq_040_log (
            cditem_nivel99_old,      cditem_grupo_old,
            cditem_subgrupo_old,     cditem_item_old,
            lote_acomp_old,          deposito_old,
            qtde_empenhada_old,      qtde_estoque_atu_old,
            qtde_estoque_ant_old,    qtde_estoque_mes_old,
            qtde_estoque_bal_old,    controle_balanco_old,
            data_ult_entrada_old,    data_ult_saida_old,
            qtde_sugerida_old,       qtde_reserva_cor_old,
            cditem_nivel99_new,      cditem_grupo_new,
            cditem_subgrupo_new,     cditem_item_new,
            lote_acomp_new,          deposito_new,
            qtde_empenhada_new,      qtde_estoque_atu_new,
            qtde_estoque_ant_new,    qtde_estoque_mes_new,
            qtde_estoque_bal_new,    controle_balanco_new,
            data_ult_entrada_new,    data_ult_saida_new,
            qtde_sugerida_new,       qtde_reserva_cor_new,
            operacao,                data_operacao,
            usuario_rede,            maquina_rede,
            aplicativo,              processo_systextil)
         values (
            v_cditem_nivel99_old,    v_cditem_grupo_old,
            v_cditem_subgrupo_old,   v_cditem_item_old,
            v_lote_acomp_old,        v_deposito_old,
            v_qtde_empenhada_old,    v_qtde_estoque_atu_old,
            v_qtde_estoque_ant_old,  v_qtde_estoque_mes_old,
            v_qtde_estoque_bal_old,  v_controle_balanco_old,
            v_data_ult_entrada_old,  v_data_ult_saida_old,
            v_qtde_sugerida_old,     v_qtde_reserva_cor_old,
            v_cditem_nivel99_new,    v_cditem_grupo_new,
            v_cditem_subgrupo_new,   v_cditem_item_new,
            v_lote_acomp_new,        v_deposito_new,
            v_qtde_empenhada_new,    v_qtde_estoque_atu_new,
            v_qtde_estoque_ant_new,  v_qtde_estoque_mes_new,
            v_qtde_estoque_bal_new,  v_controle_balanco_new,
            v_data_ult_entrada_new,  v_data_ult_saida_new,
            v_qtde_sugerida_new,     v_qtde_reserva_cor_new,
            v_operacao,              v_data_operacao,
            v_usuario_rede,          v_maquina_rede,
            v_aplicativo,            v_processo_systextil);

         exception
            when OTHERS
            then raise_application_error (-20000, 'Não atualizou a tabela de log da estq_040.'
            || chr(10)
            || sqlerrm);
      end;
   end if;



end inter_tr_estq_040_log;

-- ALTER TRIGGER INTER_TR_ESTQ_040_LOG ENABLE
