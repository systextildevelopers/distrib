create or replace package ST_PCK_COND_PGTO as

    TYPE t_dados_pedi_070 IS TABLE OF pedi_070%rowtype;

    function get_dados_cond_pgt_cliente(p_cond_pgt_cliente number, p_msg_erro in out varchar2) return t_dados_pedi_070;

end;
