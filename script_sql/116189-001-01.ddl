-- Create table
create table FATU_785
(
  pedido            NUMBER(6),
  sequencia_item    NUMBER(3),
  nivel             VARCHAR2(1),
  grupo             VARCHAR2(5),
  subgrupo          VARCHAR2(3),
  item              VARCHAR2(6),
  lote              NUMBER(6),
  deposito          NUMBER(3),
  tipo_registro     NUMBER(1),
  qtde_pedida       NUMBER(15,3),
  valor_pedido      NUMBER(15,3),
  qtde_sugerida     NUMBER(12,3),
  valor_sugerido    NUMBER(15,3),
  qtde_disponivel   NUMBER(17,4),
  nr_sugestao       NUMBER(9),
  nome_programa     VARCHAR2(30),
  nr_solicitacao    NUMBER(5),
  ref_original      VARCHAR2(5),
  flag_controle_wms NUMBER(2)
);
