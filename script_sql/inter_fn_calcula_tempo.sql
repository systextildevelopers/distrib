
  CREATE OR REPLACE FUNCTION "INTER_FN_CALCULA_TEMPO" 
(p_hora_inicio  in date,
 p_hora_termino in date)
return number is
   v_minutos number;
begin
   v_minutos := 0;

   if trunc(p_hora_inicio,'MI') > trunc(p_hora_termino,'MI')
   then
      v_minutos := trunc(to_date('23:59','HH24:MI'),'MI') - trunc(p_hora_inicio,'MI');
      v_minutos := v_minutos + (trunc(p_hora_termino,'MI') - trunc(to_date('00:00','HH24:MI'),'MI'));
   else
      v_minutos := trunc(p_hora_termino,'MI') - trunc(p_hora_inicio,'MI');
   end if;

   v_minutos := v_minutos * 1440;

   return(v_minutos);

end inter_fn_calcula_tempo;
 

/

exec inter_pr_recompile;

