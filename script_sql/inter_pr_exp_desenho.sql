
  CREATE OR REPLACE PROCEDURE "INTER_PR_EXP_DESENHO" 
(
     p_codigo_projeto        in varchar2,         p_sequencia_subprojeto  in number,
     p_nivel_item            in varchar2,         p_grupo_item            in varchar2,
     p_subgru_item           in varchar2,         p_item_item             in varchar2,
     p_alternativa_produto   in number,
     p_nivel_comp            in varchar2,         p_grupo_comp            in varchar2,
     p_subgru_comp           in varchar2,         p_item_comp             in varchar2,
     p_cliente9              in number,           p_cliente4              in number,
     p_cliente2              in number,
     p_codigo_desenho        in varchar2,
     p_sequencia_estrutura   in number,           p_sequencia_variacao    in number
)
is
   v_item_comp          varchar(6);

begin
   for reg_basi_847 in (select basi_847.comb_desenho
                        from basi_847, basi_039
                        where basi_847.codigo_projeto      = p_codigo_projeto
                          and basi_847.codigo_desenho      is not null
                          and basi_847.codigo_desenho      <> ' '
                          and basi_847.codigo_desenho      = p_codigo_desenho
                          and basi_039.codigo_projeto      = basi_847.codigo_projeto
                          and basi_039.combinacao_item     = basi_847.combinacao_item
                          and basi_039.alternativa_prod    = basi_847.alternativa_prod
                          and basi_039.codigo_desenho      = basi_847.codigo_desenho
                          and basi_039.comb_desenho        = basi_847.comb_desenho
                          and basi_039.sequencia_estrutura = p_sequencia_estrutura
                          and basi_039.sequencia_variacao  = p_sequencia_variacao
                          and basi_039.alternativa_produto = p_alternativa_produto
                          and basi_039.selecionado         = 1
                        group by basi_847.comb_desenho)
   loop
      v_item_comp := p_codigo_desenho || reg_basi_847.comb_desenho;

      if  p_nivel_comp   is not null
      and p_nivel_comp   <> '0'
      and p_grupo_comp   is not null
      and p_grupo_comp   <> '00000'
      and p_subgru_comp  is not null
      and p_subgru_comp  <> '000'
      and v_item_comp    is not null
      and v_item_comp    <> '000000'
      and v_item_comp  <> ' '
      then
         inter_pr_inserir_basi_027_028 (p_nivel_comp,
                                        p_grupo_comp,
                                        p_subgru_comp,
                                        v_item_comp,
                                        p_codigo_projeto,
                                        p_sequencia_subprojeto,
                                        p_nivel_item,
                                        p_grupo_item,
                                        p_subgru_item,
                                        p_item_item,
                                        p_alternativa_produto,
                                        p_cliente9,
                                        p_cliente4,
                                        p_cliente2);
      end if;
   end loop;
end inter_pr_exp_desenho;

 

/

exec inter_pr_recompile;

