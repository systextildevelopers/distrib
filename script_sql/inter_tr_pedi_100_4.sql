
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_100_4" 
AFTER DELETE on pedi_100
for each row
declare
   v_executa_trigger   number(1);
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      delete fatu_071
      where pedido_nota   =:old.pedido_venda;
   end if;
end inter_tr_pedi_100_4;
-- ALTER TRIGGER "INTER_TR_PEDI_100_4" ENABLE
 

/

exec inter_pr_recompile;

