create or replace PROCEDURE INTER_PR_GERA_CONT_CREC( 
   p_empresa in number,             p_titulo in number,         p_seq_duplicata number, 
   p_parcela in varchar2,           p_tipo_titulo in number,    p_cnpj9 in number, 
   p_cnpj4 in number,               p_cnpj2 in number,          p_data_cred in date, 
   p_des_erro in out varchar2,      p_tp_cta_deb in number,     p_cod_contab_deb in number, 
   p_transacao in out number,       p_c_custo in number,        p_valor_adiantamento in number, 
   p_tp_cta_cre in number,          p_cod_contab_cre in number, p_documento in number, 
   p_historico in  number,          p_conta_corrente in number, p_portador_dupl in number, 
   p_data_ctb_crec in date,         p_data_credito in date,     p_data_emissao in date, 
   p_transacao_credito in number,   num_lcto_ctb in out number, p_usuario in varchar2, 
   p_prg_gerador in varchar2 default null, p_docto_pagto in number default 0 , 
   p_cli9resptit in number  default null, p_cli4resptit in number default null, p_cli2resptit in number default null) 
is 
    v_data_ajuste                  cpag_019.data_ajuste%type; 
    v_sld_reais_antes              cpag_019.sld_reais_antes%type; 
    v_vlr_ajuste                   cpag_010.saldo_titulo%type; 
    v_num_contabil                 cpag_010.num_contabil%type; 
    v_conta_credito                cont_535.cod_reduzido%type; 
    v_conta_debito                 cont_535.cod_reduzido%type; 
    v_ativa_passiva                number; 
    v_exercicio                    cont_500.exercicio%type; 
    v_cod_plano_cta                cont_500.cod_plano_cta%type; 
    v_cta_var_camb_ativ_cpag       cont_560.cta_var_camb_ativ_cpag%type; 
    v_cta_var_camb_pass_cpag       cont_560.cta_var_camb_pass_cpag%type; 
    v_hst_var_camb_cpag            cont_560.hst_var_camb_cpag%type; 
    v_ccusto_var_camb_cpag         cont_560.ccusto_var_camb_cpag%type; 
    v_compl_histor1                cont_600.compl_histor1%type; 
    v_cod_contab_for               supr_010.codigo_contabil%type; 
    v_valor_saldo_moeda            cpag_010.saldo_titulo%type; 
    v_saldo_duplicata_old          cpag_010.saldo_titulo%type; 
    v_saldo_duplicata_new          cpag_010.saldo_titulo%type; 
    v_valor_duplicata_new          cpag_010.valor_parcela%type; 
    v_gera_contabil                fatu_500.gera_contabil%type; 
    v_trans_atualiza_contab        estq_005.atualiza_contabi%type; 
    v_vlr_ajuste_contab            cpag_010.saldo_titulo%type; 
 
    v_base_calc_comis              crec_110.base_calc_comis%type; 
    v_serie_nota                   fatu_070.serie_nota_fisc%type; 
    v_encarg                       fatu_050.encargos%type; 
    v_valor_comissao_adm           fatu_070.valor_duplicata%type; 
    v_percentualComiss             fatu_070.percentual_comis%type; 
    v_perc_comissao_crec           fatu_070.perc_comis_crec%type; 
    v_perc_comis_crec_adm          fatu_070.perc_comis_crec_adm%type; 
    v_cod_rep_cliente              fatu_070.cod_rep_cliente%type; 
    v_tecido_peca                  fatu_070.tecido_peca%type; 
    v_codigo_administr             pedi_020.codigo_administr%type; 
    v_comissao_administr           fatu_070.comissao_administr%type; 
    v_hist_comis                   empr_001.hist_cred_comis%type; 
    v_encargo_incide_comissao      fatu_501.encargo_incide_comissao%type; 
    v_serie_duplic                 fatu_070.serie_nota_fisc%type; 
    v_valor_comissao               crec_110.valor_lancamento%type; 
    v_hist_cred_comis              empr_001.hist_cred_comis%type; 
    v_perc_comis_administrador     pedi_020.perc_comis_administrador%type; 
    -- 
    v_periodo_financeiro           fatu_501.PERIODO_FINANCEIRO%type; 
    v_periodo_cta_rec              fatu_501.PERIODO_CTA_REC%type; 
 
    v_des_erro                     varchar2(4000); 
    v_executou_processo            boolean; 
    v_teve_cpag_019                boolean; 
 
    v_empresa_matriz number; 
    v_exercicio_doc number;  
    v_cta_debito number; 
    v_valor_cred number; 
    v_cta_credito number; 
    v_valor_deb number; 
    v_compl_cliente varchar2(4000); 
    v_estorno number; 
 
    v_agrupa_lanc_cont_chave varchar2(1); 
    v_nome_cli varchar2(4000); 
    v_cta_juros number; 
    v_cta_deb number; 
    v_hst_juros varchar2(4000); 
    v_hist_desp_cobr varchar2(4000); 
    v_cta_desp_cobr number; 
    v_cta_cobr_custas number; 
    v_seq_pag number; 
    v_tipo_lanc_comissao number; 
    v_valor_desp_cobr number; 
    v_ind_cred_deb varchar2(4000); 
    v_valida_conta number; 
    v_historico number; 
    v_atualiza_comissao number; 
begin 
 
    begin 
        select  fatu_500.gera_contabil,     fatu_500.codigo_matriz, 
                fatu_500.tipo_lanc_comissao 
        into    v_gera_contabil, v_empresa_matriz, 
                v_tipo_lanc_comissao 
        from fatu_500 
        where fatu_500.codigo_empresa = p_empresa; 
    exception when no_data_found then 
        v_empresa_matriz := 0; 
        v_tipo_lanc_comissao := 0; 
    end; 
 
    begin 
        select  fatu_501.encargo_incide_comissao,  
                fatu_501.PERIODO_FINANCEIRO,  
                fatu_501.PERIODO_CTA_REC  
        into    v_encargo_incide_comissao 
               ,v_periodo_financeiro 
               ,v_periodo_cta_rec  
        from fatu_501 
        where fatu_501.codigo_empresa = p_empresa; 
    exception when no_data_found then 
        v_encargo_incide_comissao := 0; 
        v_periodo_financeiro := sysdate; 
        v_periodo_cta_rec := sysdate; 
    end; 
 
    -- Valida períodos contabeis  
    if p_data_ctb_crec < v_periodo_financeiro then  
        p_des_erro := 'Exercício e/ou período financeiro não encontrado ou fechado. 
                        Não será permitido realizar movimentações nesta data. Contate o contador. ' 
                        ||' Data transação: '|| p_data_ctb_crec 
                        ||' Exercicio: '|| v_periodo_financeiro; 
        return; 
    end if; 
 
    if p_data_ctb_crec <  v_periodo_cta_rec then  
        p_des_erro := 'Exercício e/ou período recebimento não encontrado ou fechado. 
                        Não será permitido realizar movimentações nesta data. Contate o contador. ' 
                        ||' Data transação: '|| p_data_ctb_crec 
                        ||' Exercicio: '|| v_periodo_cta_rec; 
        return; 
    end if;     
 
    v_executou_processo := FALSE; 
 
    begin 
        begin 
            select cont_010.sinal_titulo, cont_010.atualiza_comis 
            into v_estorno, v_atualiza_comissao 
            from cont_010 
            where cont_010.codigo_historico = p_historico; 
        exception when others then 
            v_estorno := 0; 
            v_atualiza_comissao :=0; 
        end; 
 
        v_exercicio_doc := inter_fn_checa_data(v_empresa_matriz, p_data_cred, 0); --data_transacao 
        v_exercicio := inter_fn_checa_data(v_empresa_matriz, p_data_cred,0); --data_transacao 
 
        if v_exercicio_doc < 0 
        then 
            v_exercicio_doc := v_exercicio; 
        end if; 
 
        if v_exercicio <= 0 
        then 
            p_des_erro := 'Exercício e/ou período contábil não encontrado ou fechado. 
                            Não será permitido realizar movimentações nesta data. Contate o contador. ' 
                            ||' Data transação: '|| p_transacao 
                            ||' Exercicio: '|| v_exercicio 
                            ||' Exercicio doc: '|| v_exercicio_doc; 
            return; 
        end if; 
 
        v_cta_debito := INTER_FN_ENCONTRA_CONTA(    p_empresa, 
                                                    p_tp_cta_deb, 
                                                    p_cod_contab_deb, 
                                                    p_transacao, 
                                                    p_c_custo, 
                                                    v_exercicio, 
                                                    v_exercicio_doc); 
 
        if v_cta_debito < 0 then 
            p_des_erro := 'Conta contabil de débito não encontrada. ' || 
                        'Esta transação não será confirmada. Contate o contador. ' || 
                        ' ,Tipo contábil: ' || p_tp_cta_deb || ' ,Transação: ' || p_transacao || ' ' || 
                        ' ,Código contábil: ' || p_cod_contab_deb || ' ,Centro de Custo: ' || p_c_custo ||  
                        ' ,Exercício: ' || v_exercicio || ' ,Exerício doc: ' || v_exercicio_doc; 
            return; 
         end if; 
 
        v_cta_credito := INTER_FN_ENCONTRA_CONTA( p_empresa, 
                                                p_tp_cta_cre, 
                                                p_cod_contab_cre, 
                                                p_transacao_credito, 
                                                p_c_custo, 
                                                v_exercicio, 
                                                v_exercicio_doc); 
 
        if v_cta_credito < 0 then 
            p_des_erro := 'Conta contabil de crédito não encontrada. ' || 
                        'Esta transação não será confirmada. Contate o contador. ' || 
                        'Tipo contábil: ' || p_tp_cta_cre || ' , Transação: ' || p_transacao_credito || ' ' || 
                        'Código contábil: ' || p_cod_contab_cre; 
            return; 
        end if; 
DBMS_OUTPUT.PUT_LINE('PASSA...........'); 
        if v_gera_contabil = 1 and v_cta_debito > 0 and v_cta_credito > 0 then 
            begin 
                select cod_plano_cta 
                into v_cod_plano_cta 
                from cont_500 
                where exercicio = v_exercicio 
                and cod_empresa = v_empresa_matriz; 
            exception 
                when others then v_cod_plano_cta := 0; 
            end; 
 
            v_compl_histor1 := p_documento; 
 
            --centro custo -> passa 0 
            --documento -> passa 0 
            --seq_pagamento -> passa 0 
            --cod_imposto -> passa 0 
            --projeto -> passa 0 
            --subprojeto -> passa 0 
            --servico -> passa 0 
 
            begin 
                select cont_600.numero_lanc 
                into num_lcto_ctb 
                from cont_600 
                where cont_600.cod_empresa = p_empresa 
                and cont_600.exercicio = v_exercicio 
                and cont_600.origem = 4 
                and cont_600.banco = p_portador_dupl 
                and cont_600.conta_corrente = p_conta_corrente 
                and cont_600.data_controle = p_data_ctb_crec 
                and cont_600.documento = 0; 
            exception when others 
            then 
                num_lcto_ctb := 0; 
            end; 
DBMS_OUTPUT.PUT_LINE('PASSA 1'); 
            begin 
                select val_str 
                into v_agrupa_lanc_cont_chave 
                from EMPR_008 
                where PARAM = 'contab.agrupaLancamentoContabilChave' 
                and CODIGO_EMPRESA = p_empresa; 
            exception when others then 
                v_agrupa_lanc_cont_chave := 'N'; 
            end; 
 
            select pedi_010.nome_cliente 
            into v_nome_cli 
            from pedi_010 
            where pedi_010.cgc_9 = p_cnpj9 
            and pedi_010.cgc_4 = p_cnpj4 
            and pedi_010.cgc_2 = p_cnpj2; 
 
            select LPAD(p_titulo, 9, 0) || '/' || LPAD(p_seq_duplicata, 2, 0) || '-' || v_nome_cli 
            into v_compl_cliente 
            from dual; 
 
            if v_agrupa_lanc_cont_chave = 'N' then 
                num_lcto_ctb := 0; 
            end if; 
 
            --v_cta_credito invertido com a debito porque a capri utiliza assim. 
            v_historico := 0; 
DBMS_OUTPUT.PUT_LINE('PASSA 2 -- '||p_historico);             
            v_historico := p_historico; 
             
            IF p_prg_gerador = 'CREC_FA07' THEN  
DBMS_OUTPUT.PUT_LINE('PASSA 3');         
            declare
              v_participante_lanc_cont number(1);
              v_cnpj9 number(9);
              v_cnpj4 number(4);
              v_cnpj2 number(2);
            begin
              
              select participante_lanc_cont into v_participante_lanc_cont from fatu_502 where codigo_empresa = p_empresa;

              -- 0/Representante 1/cliente
              if v_participante_lanc_cont = 0 and p_cli9resptit is not null then
                v_cnpj9 := p_cli9resptit;
                v_cnpj4 := p_cli4resptit;
                v_cnpj2 := p_cli2resptit;
              elsif v_participante_lanc_cont = 1 and p_cnpj9 is not null then
                v_cnpj9 := p_cnpj9;
                v_cnpj4 := p_cnpj4;
                v_cnpj2 := p_cnpj2;
              else
                v_cnpj9 := nvl(p_cli9resptit,p_cnpj9);
                v_cnpj4 := nvl(p_cli4resptit,p_cnpj4);
                v_cnpj2 := nvl(p_cli2resptit,p_cnpj2);
              end if;

              INTER_PR_GERA_LANC_CONT_PRG(p_empresa,        --p_cod_empresa IN NUMBER, 
                                  4 ,                --p_origem IN NUMBER, 
                                  num_lcto_ctb ,       --p_num_lanc IN OUT NUMBER, 
                                  p_c_custo ,                   --p_centro_custo  IN NUMBER, 
                                  p_data_ctb_crec ,   --p_data_lancto IN DATE, 
                                  v_historico ,         --p_hist_contabil IN OUT NUMBER, 
                                  v_compl_cliente ,        --p_compl_histor1 IN VARCHAR2, 
                                  0 ,                   --p_estorno IN NUMBER, 
                                  p_transacao ,                   --p_transacao IN NUMBER, 
                                  v_cta_credito ,         --p_conta_debito IN NUMBER, 
                                  p_valor_adiantamento ,             --p_valor_debito IN NUMBER, 
                                  v_cta_debito ,        --p_conta_credito IN NUMBER, 
                                  p_valor_adiantamento ,             --p_valor_credito IN NUMBER, 
                                  p_portador_dupl ,             --p_banco_func IN NUMBER, 
                                  p_conta_corrente ,             --p_conta_func IN NUMBER, 
                                  p_data_ctb_crec ,         --p_data_func IN DATE, 
                                  p_titulo,                    --p_docto_func IN NUMBER, 
                                  v_cnpj9 ,                   --p_cnpj9 IN NUMBER, 
                                  v_cnpj4,                    --p_cnpj4 IN NUMBER, 
                                  v_cnpj2 ,                   --p_cnpj2 IN NUMBER, 
                                  1,                    --  p_tipo_cnpj IN NUMBER, 
                                  p_titulo,                    --p_num_documento IN NUMBER, 
                                  p_parcela ,                   --p_parcela_serie IN VARCHAR2, 
                                  p_tipo_titulo ,                   --p_tipo_titulo IN NUMBER, 
                                  0,                    -- p_seq_pagamento IN NUMBER, 
                                  0 ,                   --   p_cod_imposto IN NUMBER, 
                                  0 ,                   -- p_projeto IN NUMBER, 
                                  0 ,                   -- p_subprojeto IN NUMBER, 
                                  0 ,                   -- p_servico IN NUMBER, 
                                  p_prg_gerador,  -- p_prg_gerador IN VARCHAR2 
                                  p_usuario, -- 
                                  v_des_erro);         --Retorna Mensagem de erro 
            end;
DBMS_OUTPUT.PUT_LINE('PASSA 4 '||v_des_erro);                                     
 
            ELSE  
                INTER_PR_GERA_LANC_CONT_PRG(p_empresa,        --p_cod_empresa IN NUMBER, 
                                    4 ,                --p_origem IN NUMBER, 
                                    num_lcto_ctb ,       --p_num_lanc IN OUT NUMBER, 
                                    0 ,                   --p_centro_custo  IN NUMBER, 
                                    p_data_ctb_crec ,   --p_data_lancto IN DATE, 
                                    v_historico ,         --p_hist_contabil IN OUT NUMBER, 
                                    v_compl_cliente ,        --p_compl_histor1 IN VARCHAR2, 
                                    0 ,                   --p_estorno IN NUMBER, 
                                    0 ,                   --p_transacao IN NUMBER, 
                                    v_cta_credito ,         --p_conta_debito IN NUMBER, 
                                    p_valor_adiantamento ,             --p_valor_debito IN NUMBER, 
                                    v_cta_debito ,        --p_conta_credito IN NUMBER, 
                                    p_valor_adiantamento ,             --p_valor_credito IN NUMBER, 
                                    p_portador_dupl ,             --p_banco_func IN NUMBER, 
                                    p_conta_corrente ,             --p_conta_func IN NUMBER, 
                                    p_data_ctb_crec ,         --p_data_func IN DATE, 
                                    p_titulo,                    --p_docto_func IN NUMBER, 
                                    nvl(p_cli9resptit,p_cnpj9) ,                   --p_cnpj9 IN NUMBER, 
                                    nvl(p_cli4resptit,p_cnpj4),                    --p_cnpj4 IN NUMBER, 
                                    nvl(p_cli2resptit,p_cnpj2) ,                   --p_cnpj2 IN NUMBER, 
                                    1,                    --  p_tipo_cnpj IN NUMBER, 
                                    p_titulo,                    --p_num_documento IN NUMBER, 
                                    p_parcela ,                   --p_parcela_serie IN VARCHAR2, 
                                    p_tipo_titulo ,                   --p_tipo_titulo IN NUMBER, 
                                    0,                    -- p_seq_pagamento IN NUMBER, 
                                    0 ,                   --   p_cod_imposto IN NUMBER, 
                                    0 ,                   -- p_projeto IN NUMBER, 
                                    0 ,                   -- p_subprojeto IN NUMBER, 
                                    0 ,                   -- p_servico IN NUMBER, 
                                    'CREC_FA02', 
                                    p_usuario, -- 
                                    v_des_erro);         --Retorna Mensagem de erro 
            end if; 
DBMS_OUTPUT.PUT_LINE('PASSA 5 ');                                     
            if v_des_erro is not null then 
                raise_application_error(-20001, v_des_erro); 
            end if; 
        end if; 
                       
        select nvl(MAX(fatu_075.seq_pagamento), 0)+1 
        into v_seq_pag 
        from fatu_075 
        where fatu_075.nr_titul_codempr = p_empresa 
        and fatu_075.nr_titul_cli_dup_cgc_cli9 = p_cnpj9 
        and fatu_075.nr_titul_cli_dup_cgc_cli4 = p_cnpj4 
        and fatu_075.nr_titul_cli_dup_cgc_cli2 = p_cnpj2 
        and fatu_075.nr_titul_cod_tit = p_tipo_titulo 
        and fatu_075.nr_titul_num_dup = p_titulo 
        and fatu_075.nr_titul_seq_dup = p_seq_duplicata; 
DBMS_OUTPUT.PUT_LINE('PASSA 6 ');                                     
        --raise_application_error(-20001, 'p-Historico ' || p_historico); 
 
        insert into fatu_075 ( 
            nr_titul_cli_dup_cgc_cli9,  nr_titul_cli_dup_cgc_cli4, 
            nr_titul_cli_dup_cgc_cli2,  nr_titul_cod_tit, 
            nr_titul_num_dup,           nr_titul_seq_dup, 
            nr_titul_codempr,           seq_pagamento, 
            data_pagamento,             valor_pago, 
            historico_pgto,             portador, 
            conta_corrente,             data_credito, 
            comissao_lancada,           num_contabil, 
            docto_pagto 
        ) values ( 
            p_cnpj9 ,               p_cnpj4 , 
            p_cnpj2 ,               p_tipo_titulo , 
            p_titulo ,              p_seq_duplicata , 
            p_empresa ,             v_seq_pag , 
            p_data_emissao ,        p_valor_adiantamento , 
            p_historico ,           p_portador_dupl , 
            p_conta_corrente ,      p_data_credito , 
            v_tipo_lanc_comissao ,  num_lcto_ctb, 
            p_docto_pagto 
        );         
                      
        --raise_application_error(-20001, 'Inseriu baixa '); 
 
DBMS_OUTPUT.PUT_LINE('PASSA 7 ');                                     
 
        if ( v_atualiza_comissao = 1 and  (v_estorno = 1 or v_estorno = 2) and v_tipo_lanc_comissao = 1) 
        then  
           v_base_calc_comis := p_valor_adiantamento; 
           
           begin 
              select fatu_070.serie_nota_fisc,  fatu_070.cod_rep_cliente, 
                         fatu_070.percentual_comis, fatu_070.perc_comis_crec, 
                         fatu_070.perc_comis_crec_adm, fatu_070.tecido_peca, 
                         fatu_070.comissao_administr 
              INTO v_serie_duplic,v_cod_rep_cliente, 
                       v_percentualComiss, v_perc_comissao_crec, 
                       v_perc_comis_crec_adm, v_tecido_peca, 
                       v_comissao_administr 
                   from fatu_070 
                  where fatu_070.codigo_empresa = p_empresa 
                   and  fatu_070.cli_dup_cgc_cli9 = p_cnpj9 
                   and fatu_070.cli_dup_cgc_cli4 = p_cnpj4 
                   and fatu_070.cli_dup_cgc_cli2 = p_cnpj2 
                   and fatu_070.tipo_titulo = p_tipo_titulo 
                   and fatu_070.num_duplicata = p_titulo 
                   and fatu_070.seq_duplicatas = p_seq_duplicata        ; 
           exception when others then 
                  v_serie_duplic        := '0'; 
                  v_cod_rep_cliente     := 0; 
                  v_percentualComiss    := 0.00; 
                  v_perc_comissao_crec  := 0.00; 
                  v_perc_comis_crec_adm := 0.00; 
                  v_tecido_peca         := '0'; 
                  v_comissao_administr  := 0.00; 
           end; 
 
        --raise_application_error(-20001, 'Inseriu Comissao'); 
 
           if ( v_encargo_incide_comissao = 1 and v_serie_duplic !='0') 
           then  
 
               begin 
                  select fatu_050.encargos into v_encarg 
                  from fatu_050 
                  where fatu_050.codigo_empresa = p_empresa 
                    and fatu_050.num_nota_fiscal = p_titulo 
                    and fatu_050.serie_nota_fisc = v_serie_duplic ; 
               exception when others then 
                       v_encarg := 0.00; 
               end; 
 
               v_base_calc_comis := v_base_calc_comis * ((100.00 + v_encarg) / 100.00); 
           end if; 
 
           v_perc_comissao_crec :=  v_perc_comissao_crec; 
           v_perc_comis_crec_adm :=  v_perc_comis_crec_adm; 
 
           v_valor_comissao := v_base_calc_comis * (v_percentualComiss / 100); 
 
           v_valor_comissao := v_valor_comissao *( v_perc_comissao_crec / 100); 
 
           select empr_001.hist_cred_comis into v_hist_cred_comis from  empr_001; 
DBMS_OUTPUT.PUT_LINE('PASSA 8 ');                                                
           if ( v_valor_comissao != 0) 
           then  
              
     
                   INTER_PR_AtualizaPerc_comis(  p_empresa,             --codigo da empresa 
                                                  v_tecido_peca,         -- tipo de produto 
                                                  v_cod_rep_cliente,     -- codigo do representante 
                                                  p_data_credito,        -- dta do pagmento do título 
                                                  v_hist_cred_comis,           -- historico de pagamento 
                                                  p_titulo ,             -- numero do título 
                                                  p_seq_duplicata ,      -- parcela 
                                                  p_tipo_titulo,    -- tipo de título 
                                                  p_cnpj9 , 
                                                  p_cnpj4 , 
                                                  p_cnpj2 , 
                                                  v_base_calc_comis ,  -- base da comissao 
                                                  v_perc_comissao_crec , -- percentual 
                                                  v_percentualComiss ,   -- parcentual 
                                                  v_valor_comissao ,    -- valor comissão 
                                                  v_des_erro ); 
 
           end if; 
 
           begin 
              select pedi_020.codigo_administr 
              into v_codigo_administr 
              from pedi_020 
              where pedi_020.cod_rep_cliente = v_cod_rep_cliente; 
           exception when others then 
               v_codigo_administr := 0; 
           end; 
 
 
           if v_codigo_administr > 0 
           then  
              begin 
                select pedi_020.perc_comis_administrador 
                into v_perc_comis_administrador 
                 from pedi_020 
                 where pedi_020.cod_rep_cliente = v_codigo_administr; 
              exception when others then 
               v_perc_comis_administrador := 0.00; 
              end; 
 
              /* Atualiza comissao Administrador */ 
              v_valor_comissao_adm := v_base_calc_comis * (v_comissao_administr / 100); 
              v_valor_comissao_adm := v_valor_comissao_adm *( v_perc_comis_administrador / 100); 
 
              if ( v_valor_comissao_adm > 0) 
              then  
 
                    INTER_PR_AtualizaPerc_comis(  p_empresa,             --codigo da empresa 
                                                  v_tecido_peca,         -- tipo de produto 
                                                  v_codigo_administr,    -- codigo do representante 
                                                  p_data_credito,        -- dta do pagmento do título 
                                                  v_hist_cred_comis,           -- historico de pagamento 
                                                  p_titulo ,             -- numero do título 
                                                  p_seq_duplicata ,      -- parcela 
                                                  p_tipo_titulo,    -- tipo de título 
                                                  p_cnpj9 , 
                                                  p_cnpj4 , 
                                                  p_cnpj2 , 
                                                  v_valor_comissao_adm ,  -- base da comissao 
                                                  v_perc_comis_crec_adm , -- percentual 
                                                  v_comissao_administr ,   -- parcentual 
                                                  v_valor_comissao_adm ,    -- valor comissão 
                                                  v_des_erro ); 
 
 
              end if; 
           end if; 
 
        end if; 
        commit; 
    EXCEPTION WHEN OTHERS THEN 
        rollback; 
        p_des_erro := 'Ocorreu um durante o processo de gerar contabilização: '||SQLERRM||' - '||dbms_utility.format_error_backtrace; 
    END; 
EXCEPTION WHEN OTHERS 
then 
    rollback; 
    p_des_erro := 'Ocorreu um durante o processo de gerar contabilização: '||SQLERRM||' - '||dbms_utility.format_error_backtrace; 
end inter_pr_gera_cont_crec; 
/
