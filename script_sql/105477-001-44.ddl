alter table i_fatu_060 add (perc_substituicao      NUMBER(5,2));

alter table i_fatu_060 add (perc_redu_sub          NUMBER(5,2));

alter table i_fatu_060 add (perc_redu_icm          NUMBER(5,2));


comment on column I_FATU_060.perc_substituicao
  is 'Percentual de substitui��o do item da nota fiscal';

comment on column I_FATU_060.perc_redu_sub
  is 'Percentual de redu��o de substitui��o.';

comment on column I_FATU_060.perc_redu_icm
  is 'Percentual de redu��o de ICMD';

/
exec inter_pr_recompile;
/
