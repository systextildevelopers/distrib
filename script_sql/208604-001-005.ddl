insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('inte_e345', 'Eliminação de pedidos', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'inte_e345', 'inte_menu', 1, 1, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'inte_e345', 'inte_menu', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Eliminação de pedidos'
 where hdoc_036.codigo_programa = 'inte_e345'
   and hdoc_036.locale          = 'es_ES';
commit;

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('inte_f345', 'Consulta de pedidos eliminados', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'inte_f345', 'inte_menu', 1, 1, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'inte_f345', 'inte_menu', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Consulta de pedidos eliminados'
 where hdoc_036.codigo_programa = 'inte_f345'
   and hdoc_036.locale          = 'es_ES';
commit;
