alter table fatu_504 add perm_alter_qtde_req_mat number(1) default 0;

comment on column fatu_504.perm_alter_qtde_req_mat is 'Indica se a empresa permite alterar a quantidade requisitada na Requisicao de Materiais (supr_520), onde 0 nao permite e 1 permite ';
exec inter_pr_recompile;
/
exit;
