create table blocok_cfop_saida (
    id             number (9)
   ,codigo_empresa number (4)
   ,cfop           varchar2 (5)
);

alter table blocok_cfop_saida 
    add constraint pk_blocok_cfop_saida  primary key (id);
