
  CREATE OR REPLACE PROCEDURE "INTER_PR_GERA_CALEND_ESTAGIOS" 
   (p_data_inicial in date,   p_data_final in date)
is
--  Criando um Cursor para gerar os calendarios dos estagios
   cursor mqop005 is
      select mqop_005.codigo_estagio codigo_recurso, mqop_005.descricao descricao_recurso
      from mqop_005
      where mqop_005.seq_planejamento > 0;

   v_resultado       number;
begin
   -- Loop do estagios

   for reg_mqop_005 in mqop005
   loop
      -- atualiza a tabela de cadastro de recurso
      begin

         update tmrp_665
         set tmrp_665.qtde_recursos = 0  --- Inicia qtde recursos
         where tmrp_665.tipo_recurso   = 6   -- Estagios
           and tmrp_665.tipo_alocacao  = 0   -- PLanejamento
           and tmrp_665.codigo_recurso = reg_mqop_005.codigo_recurso;

         if SQL%notfound
         then
            -- Atualizando o estagios como um Recurso
            insert into tmrp_665
               (tipo_recurso,               qtde_recursos,
                codigo_recurso,             descricao,
                tipo_alocacao)
            values
               (6,                            0,
                reg_mqop_005.codigo_recurso,  reg_mqop_005.descricao_recurso,
                0);
         end if;

         exception
         when OTHERS
            then
               raise_application_error (-20000, 'Nao atualizou a tabela de Cadastros dos Recursos');
      end;

      -- Criando um loop para as maquinas do estagios
      for reg_mqop_030 in (select mqop_030.centro_custo
                           from mqop_030
                           where mqop_030.estagio_principal = reg_mqop_005.codigo_recurso)
      loop
         -- Mandando para a procedure gerar o calendario
         inter_pr_gera_calend_recurso(p_data_inicial   => p_data_inicial,
                                      p_data_final     => p_data_final,
                                      p_tipo_alocacao  => 0, -- PLanejamento
                                      p_tipo_recurso   => 6, -- Estagio
                                      p_codigo_recurso => reg_mqop_005.codigo_recurso,
                                      p_centro_custo   => reg_mqop_030.centro_custo,
                                      p_qtde_recursos  => 1,
                                      p_resultado      => v_resultado);

         if v_resultado >0
         then
            update tmrp_665
            set tmrp_665.qtde_recursos = tmrp_665.qtde_recursos + 1  --- Inicia qtde recursos
            where tmrp_665.tipo_recurso   = 6   -- Estagios
              and tmrp_665.tipo_alocacao  = 0
              and tmrp_665.codigo_recurso = reg_mqop_005.codigo_recurso;
         end if;

      end loop;
   end loop;
end inter_pr_gera_calend_estagios;



 

/

exec inter_pr_recompile;

