CREATE OR REPLACE TRIGGER INTER_TR_TMRP_625 
before insert or
       update of qtde_reserva_planejada, qtde_reserva_programada, qtde_areceber_programada
  on tmrp_625
  for each row

begin
  -- Trigger criada para controle de valores/ quantidades atualizadas no planejamento,
  -- que geram diferenca de arredondamento, ou seja, valores pequenos de "saldo"
  -- abaixo de 3 casas decimais, serao zerados para nao comprometer os calculos e
  -- consistencias dos programas / script's de banco de dados.
  if updating or inserting
  then
     if :new.qtde_reserva_planejada < 0.001
     then
        :new.qtde_reserva_planejada := 0.000000;
     end if;

     if :new.qtde_reserva_programada < 0.001
     then
        :new.qtde_reserva_programada := 0.000000;
     end if;

     if :new.qtde_areceber_programada < 0.001
     then
        :new.qtde_areceber_programada := 0.000000;
     end if;
  end if;
end inter_tr_tmrp_625;

-- ALTER TRIGGER "INTER_TR_TMRP_625" ENABLE
 

/

exec inter_pr_recompile;

