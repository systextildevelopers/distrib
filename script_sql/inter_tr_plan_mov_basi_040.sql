
  CREATE OR REPLACE TRIGGER "INTER_TR_PLAN_MOV_BASI_040" 
   after insert or delete or update
       of SUB_COMP, ITEM_COMP, CONS_UNID_MED_GENERICA, CONSUMO, ALTERNATIVA_COMP
   on basi_040
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_executa_trigger         number(1);
   v_nivel                   varchar2(1);
   v_grupo                   varchar2(5);
   v_subgrupo                varchar2(3);
   v_item                    varchar2(6);
   v_alternativa             number(2);

   v_pedido                  number;
begin

   v_executa_trigger := 1;

   if inserting
   then
      v_nivel       := :new.nivel_item;
      v_grupo       := :new.grupo_item;
      v_subgrupo    := :new.sub_item;
      v_item        := :new.item_item;
      v_alternativa := :new.alternativa_item;
   else
      v_nivel       := :old.nivel_item;
      v_grupo       := :old.grupo_item;
      v_subgrupo    := :old.sub_item;
      v_item        := :old.item_item;
      v_alternativa := :old.alternativa_item;
   end if;

   if v_executa_trigger = 1
   then
      -- Dados do usuario logado
      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                              ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


      for reg_o_planej in(select tmrp_virtual.ordem_planejamento,
                                 tmrp_virtual.pedido_venda,
                                 tmrp_virtual.pedido_reserva,
                                 tmrp_virtual.nivel_produto_origem,
                                 tmrp_virtual.grupo_produto_origem
                          from (
                          select tmrp_610.ordem_planejamento,
                                 tmrp_625.pedido_venda,
                                 tmrp_625.pedido_reserva,
                                 '0'     as nivel_produto_origem,
                                 '00000' as grupo_produto_origem
                          from tmrp_625, tmrp_620, tmrp_610
                          where   tmrp_620.ordem_planejamento       = tmrp_625.ordem_planejamento
                            and   tmrp_620.pedido_venda             = tmrp_625.pedido_venda
                            and   tmrp_620.pedido_reserva           = tmrp_625.pedido_reserva
                            and   tmrp_620.nivel_produto            = tmrp_625.nivel_produto
                            and   tmrp_620.grupo_produto            = tmrp_625.grupo_produto
                            and   tmrp_620.subgrupo_produto         = tmrp_625.subgrupo_produto
                            and   tmrp_620.item_produto             = tmrp_625.item_produto
                            and   tmrp_620.alternativa_produto      = v_alternativa
                            and   tmrp_625.ordem_planejamento       = tmrp_610.ordem_planejamento
                            and   tmrp_610.situacao_ordem           < 9
                            and   tmrp_625.nivel_produto_origem     = '0'
                            and   tmrp_625.grupo_produto_origem     = '00000'
                            and   tmrp_625.subgrupo_produto_origem  = '000'
                            and   tmrp_625.item_produto_origem      = '000000'
                            and   tmrp_625.nivel_produto            = v_nivel
                            and   tmrp_625.grupo_produto            = v_grupo
                            and  (tmrp_625.subgrupo_produto         = v_subgrupo or v_subgrupo = '000')
                            and  (tmrp_625.item_produto             = v_item     or v_item     = '000000')
                            and ((tmrp_625.qtde_areceber_programada > 0
                            and  exists (select 1 from tmrp_041, pcpc_020, tmrp_630
                                         where tmrp_041.periodo_producao   = pcpc_020.periodo_producao
                                           and tmrp_041.area_producao      = 1
                                           and tmrp_041.nr_pedido_ordem    = tmrp_630.ordem_prod_compra
                                           and tmrp_041.codigo_estagio     = pcpc_020.ultimo_estagio
                                           and tmrp_041.nivel_estrutura    = tmrp_625.nivel_produto
                                           and tmrp_041.grupo_estrutura    = tmrp_625.grupo_produto
                                           and tmrp_041.subgru_estrutura   = tmrp_625.subgrupo_produto
                                           and tmrp_041.item_estrutura     = tmrp_625.item_produto
                                           and tmrp_041.qtde_areceber      > 0
                                           and pcpc_020.ordem_producao     = tmrp_630.ordem_prod_compra
                                           and pcpc_020.cod_cancelamento   = 0
                                           and tmrp_630.ordem_planejamento = tmrp_610.ordem_planejamento
                                          and tmrp_630.area_producao      = 1))
                             or tmrp_625.qtde_areceber_programada = 0)
                          group by tmrp_610.ordem_planejamento , tmrp_625.pedido_venda, tmrp_625.pedido_reserva
                          UNION ALL
                          select tmrp_610.ordem_planejamento,
                                 tmrp_625.pedido_venda,
                                 tmrp_625.pedido_reserva,
                                 tmrp_625.nivel_produto_origem,
                                 tmrp_625.grupo_produto_origem
                          from tmrp_625, tmrp_620, tmrp_610
                          where   tmrp_620.ordem_planejamento       = tmrp_625.ordem_planejamento
                            and   tmrp_620.pedido_venda             = tmrp_625.pedido_venda
                            and   tmrp_620.pedido_reserva           = tmrp_625.pedido_reserva
                            and   tmrp_620.nivel_produto            = tmrp_625.nivel_produto_origem
                            and   tmrp_620.grupo_produto            = tmrp_625.grupo_produto_origem
                            and   tmrp_620.subgrupo_produto         = tmrp_625.subgrupo_produto_origem
                            and   tmrp_620.item_produto             = tmrp_625.item_produto_origem
                            and   tmrp_620.alternativa_produto      = v_alternativa
                            and   tmrp_625.ordem_planejamento       = tmrp_610.ordem_planejamento
                            and   tmrp_610.situacao_ordem           < 9
                            and   tmrp_625.nivel_produto_origem     = '1'
                            and   tmrp_625.nivel_produto            = v_nivel
                            and   tmrp_625.grupo_produto            = v_grupo
                            and  (tmrp_625.subgrupo_produto         = v_subgrupo or v_subgrupo = '000')
                            and  (tmrp_625.item_produto             = v_item     or v_item     = '000000')
                            and ((tmrp_625.qtde_areceber_programada > 0
                            and  exists (select 1 from tmrp_041, pcpc_020, tmrp_630
                                         where tmrp_041.periodo_producao   = pcpc_020.periodo_producao
                                           and tmrp_041.area_producao      = 1
                                           and tmrp_041.nr_pedido_ordem    = tmrp_630.ordem_prod_compra
                                           and tmrp_041.codigo_estagio     = pcpc_020.ultimo_estagio
                                           and tmrp_041.nivel_estrutura    = tmrp_625.nivel_produto
                                           and tmrp_041.grupo_estrutura    = tmrp_625.grupo_produto
                                           and tmrp_041.subgru_estrutura   = tmrp_625.subgrupo_produto
                                           and tmrp_041.item_estrutura     = tmrp_625.item_produto
                                           and tmrp_041.qtde_areceber      > 0
                                           and pcpc_020.ordem_producao     = tmrp_630.ordem_prod_compra
                                           and pcpc_020.cod_cancelamento   = 0
                                           and tmrp_630.ordem_planejamento = tmrp_610.ordem_planejamento
                                          and tmrp_630.area_producao      = 1))
                             or tmrp_625.qtde_areceber_programada = 0)
                          group by tmrp_610.ordem_planejamento,
                                   tmrp_625.pedido_venda,
                                   tmrp_625.pedido_reserva,
                                   tmrp_625.nivel_produto_origem,
                                   tmrp_625.GRUPO_produto_origem) tmrp_virtual
                          group by tmrp_virtual.ordem_planejamento,
                                   tmrp_virtual.pedido_venda,
                                   tmrp_virtual.pedido_reserva,
                                   tmrp_virtual.nivel_produto_origem,
                                   tmrp_virtual.grupo_produto_origem)
      loop
        if reg_o_planej.pedido_venda > 0
        then v_pedido := reg_o_planej.pedido_venda;
        else v_pedido := reg_o_planej.pedido_reserva;
        end if;

        -- Alteracao do codigo do componente, consumo ou alternativa do componente.
        if updating
        then
           if :old.sub_comp   <> :new.sub_comp
           or :old.item_comp  <> :new.item_comp
           then
              inter_pr_insere_motivo_plan(
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  --Nova especificacao tamanho/item: {0}.
                  inter_fn_buscar_tag_composta('lb33705', :new.sub_comp   || '.' ||
                                                           :new.item_comp,
                                                           '','','','','','','','','', ws_locale_usuario,ws_usuario_systextil));

                  /*Sequencia {0} da estrutura {1}, na alternativa {2}, alterada.
                  inter_fn_buscar_tag_composta('lb33702', to_char(:old.sequencia),
                                                          v_nivel    || '.' ||
                                                          v_grupo    || '.' ||
                                                          v_subgrupo || '.' ||
                                                          v_item,
                                                          to_char(:old.alternativa_item),
                                                          '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
                  */
           end if;

           if :old.alternativa_comp <> :new.alternativa_comp
           then
              inter_pr_insere_motivo_plan(
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  --Sequencia {0} da estrutura {1}, na alternativa {2}, alterada.
                  inter_fn_buscar_tag_composta('lb33702', to_char(:old.sequencia),
                                                          v_nivel    || '.' ||
                                                          v_grupo    || '.' ||
                                                          v_subgrupo || '.' ||
                                                          v_item,
                                                          to_char(:old.alternativa_item),
                                                          '','','','','','','', ws_locale_usuario,ws_usuario_systextil));

           end if;

           if :old.consumo <> :new.consumo
           then
              inter_pr_insere_motivo_plan(
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  --Consumo da sequencia {0} alterado de {1} para {2}.
                  inter_fn_buscar_tag_composta('lb33703', to_char(:old.sequencia),
                                                          to_char(:old.consumo),
                                                          to_char(:new.consumo),
                                                          '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
           end if;

           if :old.cons_unid_med_generica <> :new.cons_unid_med_generica
           then
              inter_pr_insere_motivo_plan(
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  -- O consumo auxiliar da sequencia {0} alterado de {1} para {2}.
                  inter_fn_buscar_tag_composta('lb34234', to_char(:old.sequencia),
                                                          to_char(:old.cons_unid_med_generica),
                                                          to_char(:new.cons_unid_med_generica),
                                                          '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
           end if;

        end if;

        -- Eliminacao
        if deleting
        then
           inter_pr_insere_motivo_plan(
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                   --Sequencia {0}: especificacao tamanho/item excluida.
                   inter_fn_buscar_tag_composta('lb33704', to_char(:old.sequencia),
                                                           '','','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
        end if;

        -- Inclusao
        if inserting
        then
           inter_pr_insere_motivo_plan(
                  reg_o_planej.ordem_planejamento,        v_pedido,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                                Estrutura
                  0,                                      2,
                  -- ESTRUTURA DE PRODUTOS ALTERADA
                  inter_fn_buscar_tag_composta('lb34584', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  --Nova especificacao tamanho/item: {0}.
                  inter_fn_buscar_tag_composta('lb33705', :new.sub_comp   || '.' ||
                                                           :new.item_comp,
                                                           '','','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
        end if;
      end loop;
   end if;

end inter_tr_plan_mov_basi_040;

-- ALTER TRIGGER "INTER_TR_PLAN_MOV_BASI_040" ENABLE
 

/

exec inter_pr_recompile;

