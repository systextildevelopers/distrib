create or replace trigger wms_tr_pedidos_log
  before insert
  or delete
  or update of numero_pedido, data_entrega, data_emissao, data_base_fatur, 
	cgc9_cli, cgc4_cli, cgc2_cli, nome_cli, 
	rua_end_entr, num_imovel_end_entr, comp_end_entr, bairro_end_entr, 
	cep_end_entr, cod_cidade_end_entr, nome_cidade_end_entr, uf_end_entr, 
	cod_pais_end_entr, nome_pais_end_entr, cgc9_transportadora, cgc4_transportadora, 
	cgc2_transportadora, nome_transportadora, numero_interno, perc_min_fatur, 
	valor_min_saldo, prioridade_lote, permite_parcial, aceita_antecipacao, 
	timestamp_ini_mont, timestamp_fim_mont, timestamp_insercao, flag_importacao_wms, 
	flag_importacao_st, onda, mensagem, bloqueio_coleta, qtde_min_pecas,
  auditar_volume, obs_lista_conteudo_1, obs_lista_conteudo_2, obs_lista_conteudo_3,
  cod_tipo_embalagem, rota_entrega, sigla_filial, cod_tipo_volume,
  tipo_pedido
  on inte_wms_pedidos
  for each row
declare
  -- local variables here
  v_numero_pedido                inte_wms_pedidos.numero_pedido       %type;
  v_data_entrega_old             inte_wms_pedidos.data_entrega        %type;
  v_data_entrega_new             inte_wms_pedidos.data_entrega        %type;
  v_data_emissao_old             inte_wms_pedidos.data_emissao        %type;
  v_data_emissao_new             inte_wms_pedidos.data_emissao        %type;
  v_data_base_fatur_old          inte_wms_pedidos.data_base_fatur     %type;
  v_data_base_fatur_new          inte_wms_pedidos.data_base_fatur     %type;
  v_cgc9_cli_old                 inte_wms_pedidos.cgc9_cli            %type;
  v_cgc9_cli_new                 inte_wms_pedidos.cgc9_cli            %type;
  v_cgc4_cli_old                 inte_wms_pedidos.cgc4_cli            %type;
  v_cgc4_cli_new                 inte_wms_pedidos.cgc4_cli            %type;
  v_cgc2_cli_old                 inte_wms_pedidos.cgc2_cli            %type;
  v_cgc2_cli_new                 inte_wms_pedidos.cgc2_cli            %type;
  v_nome_cli_old                 inte_wms_pedidos.nome_cli            %type;
  v_nome_cli_new                 inte_wms_pedidos.nome_cli            %type;
  v_rua_end_entr_old             inte_wms_pedidos.rua_end_entr        %type;
  v_rua_end_entr_new             inte_wms_pedidos.rua_end_entr        %type;
  v_num_imovel_end_entr_old      inte_wms_pedidos.num_imovel_end_entr %type;
  v_num_imovel_end_entr_new      inte_wms_pedidos.num_imovel_end_entr %type;
  v_comp_end_entr_old            inte_wms_pedidos.comp_end_entr       %type;
  v_comp_end_entr_new            inte_wms_pedidos.comp_end_entr       %type;
  v_bairro_end_entr_old          inte_wms_pedidos.bairro_end_entr     %type;
  v_bairro_end_entr_new          inte_wms_pedidos.bairro_end_entr     %type;
  v_cep_end_entr_old             inte_wms_pedidos.cep_end_entr        %type;
  v_cep_end_entr_new             inte_wms_pedidos.cep_end_entr        %type;
  v_cod_cidade_end_entr_old      inte_wms_pedidos.cod_cidade_end_entr %type;
  v_cod_cidade_end_entr_new      inte_wms_pedidos.cod_cidade_end_entr %type;
  v_nome_cidade_end_entr_old     inte_wms_pedidos.nome_cidade_end_entr%type;
  v_nome_cidade_end_entr_new     inte_wms_pedidos.nome_cidade_end_entr%type;
  v_uf_end_entr_old              inte_wms_pedidos.uf_end_entr         %type;
  v_uf_end_entr_new              inte_wms_pedidos.uf_end_entr         %type;
  v_cod_pais_end_entr_old        inte_wms_pedidos.cod_pais_end_entr   %type;
  v_cod_pais_end_entr_new        inte_wms_pedidos.cod_pais_end_entr   %type;
  v_nome_pais_end_entr_old       inte_wms_pedidos.nome_pais_end_entr  %type;
  v_nome_pais_end_entr_new       inte_wms_pedidos.nome_pais_end_entr  %type;
  v_cgc9_transportadora_old      inte_wms_pedidos.cgc9_transportadora %type;
  v_cgc9_transportadora_new      inte_wms_pedidos.cgc9_transportadora %type;
  v_cgc4_transportadora_old      inte_wms_pedidos.cgc4_transportadora %type;
  v_cgc4_transportadora_new      inte_wms_pedidos.cgc4_transportadora %type;
  v_cgc2_transportadora_old      inte_wms_pedidos.cgc2_transportadora %type;
  v_cgc2_transportadora_new      inte_wms_pedidos.cgc2_transportadora %type;
  v_nome_transportadora_old      inte_wms_pedidos.nome_transportadora %type;
  v_nome_transportadora_new      inte_wms_pedidos.nome_transportadora %type;
  v_numero_interno_old           inte_wms_pedidos.numero_interno      %type;
  v_numero_interno_new           inte_wms_pedidos.numero_interno      %type;
  v_perc_min_fatur_old           inte_wms_pedidos.perc_min_fatur      %type;
  v_perc_min_fatur_new           inte_wms_pedidos.perc_min_fatur      %type;
  v_valor_min_saldo_old          inte_wms_pedidos.valor_min_saldo     %type;
  v_valor_min_saldo_new          inte_wms_pedidos.valor_min_saldo     %type;
  v_prioridade_lote_old          inte_wms_pedidos.prioridade_lote     %type;
  v_prioridade_lote_new          inte_wms_pedidos.prioridade_lote     %type;
  v_permite_parcial_old          inte_wms_pedidos.permite_parcial     %type;
  v_permite_parcial_new          inte_wms_pedidos.permite_parcial     %type;
  v_aceita_antecipacao_old       inte_wms_pedidos.aceita_antecipacao  %type;
  v_aceita_antecipacao_new       inte_wms_pedidos.aceita_antecipacao  %type;
  v_timestamp_ini_mont_old       inte_wms_pedidos.timestamp_ini_mont  %type;
  v_timestamp_ini_mont_new       inte_wms_pedidos.timestamp_ini_mont  %type;
  v_timestamp_fim_mont_old       inte_wms_pedidos.timestamp_fim_mont  %type;
  v_timestamp_fim_mont_new       inte_wms_pedidos.timestamp_fim_mont  %type;
  v_timestamp_insercao_old       inte_wms_pedidos.timestamp_insercao  %type;
  v_timestamp_insercao_new       inte_wms_pedidos.timestamp_insercao  %type;
  v_flag_importacao_wms_old      inte_wms_pedidos.flag_importacao_wms %type;
  v_flag_importacao_wms_new      inte_wms_pedidos.flag_importacao_wms %type;
  v_flag_importacao_st_old       inte_wms_pedidos.flag_importacao_st  %type;
  v_flag_importacao_st_new       inte_wms_pedidos.flag_importacao_st  %type;
  v_onda_old                     inte_wms_pedidos.onda                %type;
  v_onda_new                     inte_wms_pedidos.onda                %type;
  v_mensagem_old                 inte_wms_pedidos.mensagem            %type;
  v_mensagem_new                 inte_wms_pedidos.mensagem            %type;
  v_bloqueio_coleta_old          inte_wms_pedidos.bloqueio_coleta     %type;
  v_bloqueio_coleta_new          inte_wms_pedidos.bloqueio_coleta     %type;
  v_qtde_min_pecas_old           inte_wms_pedidos.qtde_min_pecas      %type;
  v_qtde_min_pecas_new           inte_wms_pedidos.qtde_min_pecas      %type;

  v_auditar_volume_old           inte_wms_pedidos.auditar_volume            %type;
  v_auditar_volume_new           inte_wms_pedidos.auditar_volume            %type;
  v_obs_lista_conteudo_1_old     inte_wms_pedidos.obs_lista_conteudo_1      %type;
  v_obs_lista_conteudo_1_new     inte_wms_pedidos.obs_lista_conteudo_1      %type;
  v_obs_lista_conteudo_2_old     inte_wms_pedidos.obs_lista_conteudo_2      %type;
  v_obs_lista_conteudo_2_new     inte_wms_pedidos.obs_lista_conteudo_2      %type;
  v_obs_lista_conteudo_3_old     inte_wms_pedidos.obs_lista_conteudo_3      %type;
  v_obs_lista_conteudo_3_new     inte_wms_pedidos.obs_lista_conteudo_3      %type;
  v_cod_tipo_embalagem_old       inte_wms_pedidos.cod_tipo_embalagem        %type;
  v_cod_tipo_embalagem_new       inte_wms_pedidos.cod_tipo_embalagem        %type;
  v_rota_entrega_old             inte_wms_pedidos.rota_entrega              %type;
  v_rota_entrega_new             inte_wms_pedidos.rota_entrega              %type;
  v_sigla_filial_old             inte_wms_pedidos.sigla_filial              %type;
  v_sigla_filial_new             inte_wms_pedidos.sigla_filial              %type;
  v_cod_tipo_volume_old          inte_wms_pedidos.cod_tipo_volume           %type;
  v_cod_tipo_volume_new          inte_wms_pedidos.cod_tipo_volume           %type;
  v_tipo_pedido_old              inte_wms_pedidos.tipo_pedido               %type;
  v_tipo_pedido_new              inte_wms_pedidos.tipo_pedido               %type;

  v_sid                            number(9);
  v_empresa                        number(3);
  v_usuario_systextil              varchar2(250);
  v_locale_usuario                 varchar2(5);
  v_nome_programa                  varchar2(20);


  v_operacao
         varchar(1);
  v_data_operacao                  date;
  v_usuario_rede                   varchar(20);
  v_maquina_rede                   varchar(40);
  v_aplicativo                     varchar(20);
begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();

   --alimenta as variaveis new caso seja insert
   if inserting or updating
   then

      if inserting
      then v_operacao := 'i';
      else v_operacao := 'u';
      end if;

      v_numero_pedido                := :new.numero_pedido;
      v_data_entrega_new             := :new.data_entrega;
      v_data_emissao_new             := :new.data_emissao;
      v_data_base_fatur_new          := :new.data_base_fatur;
      v_cgc9_cli_new                 := :new.cgc9_cli;
      v_cgc4_cli_new                 := :new.cgc4_cli;
      v_cgc2_cli_new                 := :new.cgc2_cli;
      v_nome_cli_new                 := :new.nome_cli;
      v_rua_end_entr_new             := :new.rua_end_entr;
      v_num_imovel_end_entr_new      := :new.num_imovel_end_entr;
      v_comp_end_entr_new            := :new.comp_end_entr;
      v_bairro_end_entr_new          := :new.bairro_end_entr;
      v_cep_end_entr_new             := :new.cep_end_entr;
      v_cod_cidade_end_entr_new      := :new.cod_cidade_end_entr;
      v_nome_cidade_end_entr_new     := :new.nome_cidade_end_entr;
      v_uf_end_entr_new              := :new.uf_end_entr;
      v_cod_pais_end_entr_new        := :new.cod_pais_end_entr;
      v_nome_pais_end_entr_new       := :new.nome_pais_end_entr;
      v_cgc9_transportadora_new      := :new.cgc9_transportadora;
      v_cgc4_transportadora_new      := :new.cgc4_transportadora;
      v_cgc2_transportadora_new      := :new.cgc2_transportadora;
      v_nome_transportadora_new      := :new.nome_transportadora;
      v_numero_interno_new           := :new.numero_interno;
      v_perc_min_fatur_new           := :new.perc_min_fatur;
      v_valor_min_saldo_new          := :new.valor_min_saldo;
      v_prioridade_lote_new          := :new.prioridade_lote;
      v_permite_parcial_new          := :new.permite_parcial;
      v_aceita_antecipacao_new       := :new.aceita_antecipacao;
      v_timestamp_ini_mont_new       := :new.timestamp_ini_mont;
      v_timestamp_fim_mont_new       := :new.timestamp_fim_mont;
      v_timestamp_insercao_new       := :new.timestamp_insercao;
      v_flag_importacao_wms_new      := :new.flag_importacao_wms;
      v_flag_importacao_st_new       := :new.flag_importacao_st;
      v_onda_new                     := :new.onda;
      v_mensagem_new                 := :new.mensagem;
      v_bloqueio_coleta_new          := :new.bloqueio_coleta;
      v_qtde_min_pecas_new           := :new.qtde_min_pecas; 

      v_auditar_volume_new           := :new.auditar_volume;
      v_obs_lista_conteudo_1_new     := :new.obs_lista_conteudo_1;
      v_obs_lista_conteudo_2_new     := :new.obs_lista_conteudo_2;
      v_obs_lista_conteudo_3_new     := :new.obs_lista_conteudo_3;
      v_cod_tipo_embalagem_new       := :new.cod_tipo_embalagem;
      v_rota_entrega_new             := :new.rota_entrega;
      v_sigla_filial_new             := :new.sigla_filial;
      v_cod_tipo_volume_new          := :new.cod_tipo_volume;
      v_tipo_pedido_new              := :new.tipo_pedido;

   end if; --fim do if inserting or updating

   --alimenta as variaveis old caso seja insert ou update
   if deleting or updating
   then
      if deleting
      then v_operacao := 'd';
      else v_operacao := 'u';
      end if;

      v_numero_pedido                := :old.numero_pedido;
      v_data_entrega_old             := :old.data_entrega;
      v_data_emissao_old             := :old.data_emissao;
      v_data_base_fatur_old          := :old.data_base_fatur;
      v_cgc9_cli_old                 := :old.cgc9_cli;
      v_cgc4_cli_old                 := :old.cgc4_cli;
      v_cgc2_cli_old                 := :old.cgc2_cli;
      v_nome_cli_old                 := :old.nome_cli;
      v_rua_end_entr_old             := :old.rua_end_entr;
      v_num_imovel_end_entr_old      := :old.num_imovel_end_entr;
      v_comp_end_entr_old            := :old.comp_end_entr;
      v_bairro_end_entr_old          := :old.bairro_end_entr;
      v_cep_end_entr_old             := :old.cep_end_entr;
      v_cod_cidade_end_entr_old      := :old.cod_cidade_end_entr;
      v_nome_cidade_end_entr_old     := :old.nome_cidade_end_entr;
      v_uf_end_entr_old              := :old.uf_end_entr;
      v_cod_pais_end_entr_old        := :old.cod_pais_end_entr;
      v_nome_pais_end_entr_old       := :old.nome_pais_end_entr;
      v_cgc9_transportadora_old      := :old.cgc9_transportadora;
      v_cgc4_transportadora_old      := :old.cgc4_transportadora;
      v_cgc2_transportadora_old      := :old.cgc2_transportadora;
      v_nome_transportadora_old      := :old.nome_transportadora;
      v_numero_interno_old           := :old.numero_interno;
      v_perc_min_fatur_old           := :old.perc_min_fatur;
      v_valor_min_saldo_old          := :old.valor_min_saldo;
      v_prioridade_lote_old          := :old.prioridade_lote;
      v_permite_parcial_old          := :old.permite_parcial;
      v_aceita_antecipacao_old       := :old.aceita_antecipacao;
      v_timestamp_ini_mont_old       := :old.timestamp_ini_mont;
      v_timestamp_fim_mont_old       := :old.timestamp_fim_mont;
      v_timestamp_insercao_old       := :old.timestamp_insercao;
      v_flag_importacao_wms_old      := :old.flag_importacao_wms;
      v_flag_importacao_st_old       := :old.flag_importacao_st;
      v_onda_old                     := :old.onda;
      v_mensagem_old                 := :old.mensagem;
      v_bloqueio_coleta_old          := :old.bloqueio_coleta;
      v_qtde_min_pecas_old           := :old.qtde_min_pecas;

      v_auditar_volume_old           := :old.auditar_volume;
      v_obs_lista_conteudo_1_old     := :old.obs_lista_conteudo_1;
      v_obs_lista_conteudo_2_old     := :old.obs_lista_conteudo_2;
      v_obs_lista_conteudo_3_old     := :old.obs_lista_conteudo_3;
      v_cod_tipo_embalagem_old       := :old.cod_tipo_embalagem;
      v_rota_entrega_old             := :old.rota_entrega;
      v_sigla_filial_old             := :old.sigla_filial;
      v_cod_tipo_volume_old          := :old.cod_tipo_volume;
      v_tipo_pedido_old              := :old.tipo_pedido;

   end if; --fim do if deleting or updating


   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);


    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente

   --insere na inte_wms_rfid_log o registro.
   insert into inte_wms_pedidos_log (
      numero_pedido,
      data_entrega_old,            data_entrega_new,
      data_emissao_old,            data_emissao_new,
      data_base_fatur_old,         data_base_fatur_new,
      cgc9_cli_old,                cgc9_cli_new,
      cgc4_cli_old,                cgc4_cli_new,
      cgc2_cli_old,                cgc2_cli_new,
      nome_cli_old,                nome_cli_new,
      rua_end_entr_old,            rua_end_entr_new,
      num_imovel_end_entr_old,     num_imovel_end_entr_new,
      comp_end_entr_old,           comp_end_entr_new,
      bairro_end_entr_old,         bairro_end_entr_new,
      cep_end_entr_old,            cep_end_entr_new,
      cod_cidade_end_entr_old,     cod_cidade_end_entr_new,
      nome_cidade_end_entr_old,    nome_cidade_end_entr_new,
      uf_end_entr_old,             uf_end_entr_new,
      cod_pais_end_entr_old,       cod_pais_end_entr_new,
      nome_pais_end_entr_old,      nome_pais_end_entr_new,
      cgc9_transportadora_old,     cgc9_transportadora_new,
      cgc4_transportadora_old,     cgc4_transportadora_new,
      cgc2_transportadora_old,     cgc2_transportadora_new,
      nome_transportadora_old,     nome_transportadora_new,
      numero_interno_old,          numero_interno_new,
      perc_min_fatur_old,          perc_min_fatur_new,
      valor_min_saldo_old,         valor_min_saldo_new,
      prioridade_lote_old,         prioridade_lote_new,
      permite_parcial_old,         permite_parcial_new,
      aceita_antecipacao_old,      aceita_antecipacao_new,
      timestamp_ini_mont_old,      timestamp_ini_mont_new,
      timestamp_fim_mont_old,      timestamp_fim_mont_new,
      timestamp_insercao_old,      timestamp_insercao_new,
      flag_importacao_wms_old,     flag_importacao_wms_new,
      flag_importacao_st_old,      flag_importacao_st_new,
      onda_old,                    onda_new,
      mensagem_old,                mensagem_new,
      bloqueio_coleta_old,         bloqueio_coleta_new,
      qtde_min_pecas_old,          qtde_min_pecas_new,

      auditar_volume_old,          auditar_volume_new,
      obs_lista_conteudo_1_old,    obs_lista_conteudo_1_new,
      obs_lista_conteudo_2_old,    obs_lista_conteudo_2_new,
      obs_lista_conteudo_3_old,    obs_lista_conteudo_3_new,
      cod_tipo_embalagem_old,      cod_tipo_embalagem_new,
      rota_entrega_old,            rota_entrega_new,
      sigla_filial_old,            sigla_filial_new,
      cod_tipo_volume_old,         cod_tipo_volume_new,
      tipo_pedido_old,             tipo_pedido_new,
      
      operacao,
      data_operacao,               usuario_rede,
      maquina_rede,                aplicativo,
      nome_programa
   )
   values (
      v_numero_pedido,
      v_data_entrega_old,          v_data_entrega_new,
      v_data_emissao_old,          v_data_emissao_new,
      v_data_base_fatur_old,       v_data_base_fatur_new,
      v_cgc9_cli_old,              v_cgc9_cli_new,
      v_cgc4_cli_old,              v_cgc4_cli_new,
      v_cgc2_cli_old,              v_cgc2_cli_new,
      v_nome_cli_old,              v_nome_cli_new,
      v_rua_end_entr_old,          v_rua_end_entr_new,
      v_num_imovel_end_entr_old,   v_num_imovel_end_entr_new,
      v_comp_end_entr_old,         v_comp_end_entr_new,
      v_bairro_end_entr_old,       v_bairro_end_entr_new,
      v_cep_end_entr_old,          v_cep_end_entr_new,
      v_cod_cidade_end_entr_old,   v_cod_cidade_end_entr_new,
      v_nome_cidade_end_entr_old,  v_nome_cidade_end_entr_new,
      v_uf_end_entr_old,           v_uf_end_entr_new,
      v_cod_pais_end_entr_old,     v_cod_pais_end_entr_new,
      v_nome_pais_end_entr_old,    v_nome_pais_end_entr_new,
      v_cgc9_transportadora_old,   v_cgc9_transportadora_new,
      v_cgc4_transportadora_old,   v_cgc4_transportadora_new,
      v_cgc2_transportadora_old,   v_cgc2_transportadora_new,
      v_nome_transportadora_old,   v_nome_transportadora_new,
      v_numero_interno_old,        v_numero_interno_new,
      v_perc_min_fatur_old,        v_perc_min_fatur_new,
      v_valor_min_saldo_old,       v_valor_min_saldo_new,
      v_prioridade_lote_old,       v_prioridade_lote_new,
      v_permite_parcial_old,       v_permite_parcial_new,
      v_aceita_antecipacao_old,    v_aceita_antecipacao_new,
      v_timestamp_ini_mont_old,    v_timestamp_ini_mont_new,
      v_timestamp_fim_mont_old,    v_timestamp_fim_mont_new,
      v_timestamp_insercao_old,    v_timestamp_insercao_new,
      v_flag_importacao_wms_old,   v_flag_importacao_wms_new,
      v_flag_importacao_st_old,    v_flag_importacao_st_new,
      v_onda_old,                  v_onda_new,
      v_mensagem_old,              v_mensagem_new,
      v_bloqueio_coleta_old,       v_bloqueio_coleta_new,
      v_qtde_min_pecas_old,        v_qtde_min_pecas_new,

      v_auditar_volume_old,        v_auditar_volume_new,
      v_obs_lista_conteudo_1_old,  v_obs_lista_conteudo_1_new,
      v_obs_lista_conteudo_2_old,  v_obs_lista_conteudo_2_new,
      v_obs_lista_conteudo_3_old,  v_obs_lista_conteudo_3_new,
      v_cod_tipo_embalagem_old,    v_cod_tipo_embalagem_new,
      v_rota_entrega_old,          v_rota_entrega_new,
      v_sigla_filial_old,          v_sigla_filial_new,
      v_cod_tipo_volume_old,       v_cod_tipo_volume_new,
      v_tipo_pedido_old,           v_tipo_pedido_new,
     
      v_operacao,
      v_data_operacao,             v_usuario_rede,
      v_maquina_rede,              v_aplicativo,
      v_nome_programa
   );

end wms_tr_pedidos_log;

/

exec inter_pr_recompile;
/* versao: 5 */


 exit;


 exit;

 exit;


 exit;

 exit;
