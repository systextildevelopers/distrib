
  CREATE OR REPLACE TRIGGER "INTER_TR_ESTQ_080" 
after insert
    or update of nivel_estrutura,grupo_estrutura,sub_estrutura, item_estrutura, lote_produto, agrupador
    or delete
on estq_080
for each row

begin
   if deleting
   then

       update estq_200
       set agrupador  = 0
       where nivel    = :old.nivel_estrutura
         and grupo    = :old.grupo_estrutura
         and subgrupo = :old.sub_estrutura
         and item     = :old.item_estrutura
         and lote     = :old.lote_produto;

   end if;

   if updating
   then

       update estq_200
       set agrupador  = 0
       where nivel    = :old.nivel_estrutura
         and grupo    = :old.grupo_estrutura
         and subgrupo = :old.sub_estrutura
         and item     = :old.item_estrutura
         and lote     = :old.lote_produto;

       update estq_200
       set agrupador  = :new.agrupador
       where nivel    = :new.nivel_estrutura
         and grupo    = :new.grupo_estrutura
         and subgrupo = :new.sub_estrutura
         and item     = :new.item_estrutura
         and lote     = :new.lote_produto;

   end if;

   if inserting
   then

       update estq_200
       set agrupador  = :new.agrupador
       where nivel    = :new.nivel_estrutura
         and grupo    = :new.grupo_estrutura
         and subgrupo = :new.sub_estrutura
         and item     = :new.item_estrutura
         and lote     = :new.lote_produto;

   end if;

end inter_tr_estq_080;
-- ALTER TRIGGER "INTER_TR_ESTQ_080" ENABLE
 

/

exec inter_pr_recompile;

