create or replace trigger "INTER_TR_PEDI_728_LOG" 
  before insert
  on pedi_728
  for each row
declare

  v_usuario_systextil    varchar2(250);
  v_usuario_rede         varchar2(20);
  v_sid                  number(9);
  v_empresa              number(3);
  v_locale_usuario       varchar2(5);
  v_maquina_rede         varchar(40);
  v_aplicativo           varchar(20);

  v_reg                  number(9);

begin

   select count(*) into v_reg
   from pedi_728_log
   where pedi_728_log.codigo_empresa  = :new.codigo_empresa
     and pedi_728_log.cnpj9_cliente   = :new.cnpj9_cliente
     and pedi_728_log.cnpj4_cliente   = :new.cnpj4_cliente
     and pedi_728_log.cnpj2_cliente   = :new.cnpj2_cliente
     and pedi_728_log.codigo_segmento = :new.codigo_segmento
     and pedi_728_log.codigo_repres   = :new.codigo_repres
     and pedi_728_log.marca           = :new.marca;

   if v_reg = 0
   then

      -- Dados do usu�rio logado
      inter_pr_dados_usuario (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                              v_usuario_systextil,   v_empresa,        v_locale_usuario);

      insert into pedi_728_log (
          codigo_empresa,          cnpj9_cliente,
          cnpj4_cliente,           cnpj2_cliente,
          codigo_segmento,         codigo_repres,
          data_ocorrencia,         situacao,
          usuario_rede,            usuario_systextil,
          cod_motivo,              marca
          )
        values (
          :new.codigo_empresa,     :new.cnpj9_cliente,
          :new.cnpj4_cliente,      :new.cnpj2_cliente,
          :new.codigo_segmento,    :new.codigo_repres,
          trunc(sysdate(),'DD'),   :new.situacao,
          v_usuario_rede,           v_usuario_systextil,
          1,                       :new.marca
          );
   end if;

end inter_tr_pedi_728_log;

-- ALTER TRIGGER "INTER_TR_PEDI_728_LOG" ENABLE
 

/

exec inter_pr_recompile;

