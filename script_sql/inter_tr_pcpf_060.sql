create or replace trigger inter_tr_pcpf_060
  before insert or delete or update of peso_real, peso_medio, deposito, status_fardo on pcpf_060
  for each row

declare
   v_entrada_saida        estq_005.entrada_saida%type;
   v_transac_entrada      estq_005.transac_entrada%type;
   v_transacao_cancel     estq_005.codigo_transacao%type;
   v_peso_fardo           pcpf_060.peso_medio%type;
   v_lote_mat_prima       pcpf_060.lote_mat_prima%type;
   v_tipo_volume          basi_205.tipo_volume%type;

   v_numero_documento     estq_300.numero_documento%type;
   v_serie_documento      estq_300.serie_documento%type;
   v_sequencia_documento  estq_300.sequencia_documento%type;
   v_cnpj_9               estq_300.cnpj_9%type;
   v_cnpj_4               estq_300.cnpj_4%type;
   v_cnpj_2               estq_300.cnpj_2%type;
   v_centro_custo         estq_300.centro_custo%type;
   v_periodo_estoque      empr_001.periodo_estoque%type;
   v_tabela_origem_cardex estq_300.tabela_origem%type := 'PCPF_060';

   v_data_cancelamento    date;

begin
   if inserting or updating
   then
      -- se nao for informada a tabela origem, assume que a tabela e a propria PCPF_060
      if :new.tabela_origem_cardex is null or :new.tabela_origem_cardex = ' '
      then
         :new.tabela_origem_cardex := 'PCPF_060';
      end if;
   end if;

   if (:new.tabela_origem_cardex not in ('FATU_060', 'OBRF_015') or deleting)
   then

      if inserting or updating
      then
         -- verifica se e necessario fazer alguma conferencia
         if  (updating
         and  :new.status_fardo      <> 6
         and ((:old.status_fardo not in (2,5,6) and :new.status_fardo     in (2,5)) -- Fardo estava em estoque e nao esta mais
         or   (:old.status_fardo     in (2,5,6) and :new.status_fardo not in (2,5)) -- Fardo nao estava em estoque e agora esta
         or  ((:old.deposito         <> :new.deposito                               -- deposito alterado
         or    :old.lote_mat_prima   <> :new.lote_mat_prima                         -- lote alterado
         or    :old.peso_medio       <> :new.peso_medio                             -- peso medio do fardo alterado
         or    :old.peso_real        <> :new.peso_real)                             -- peso real do fardo alterado
         and   :old.status_fardo not in (2,5,6))))                                  -- Fardo estava em estoque

         or inserting
         then
            if (:new.transacao_cardex = 0)
            then
               raise_application_error(-20000,'Deve-se informar a transacao de movimentacao.');
            else
               select entrada_saida, transac_entrada
               into v_entrada_saida, v_transac_entrada
               from estq_005
               where codigo_transacao = :new.transacao_cardex;

               if updating                                -- Alterando o fardo
               and :old.deposito         <> :new.deposito -- Se foi alterado o deposito
               and :old.status_fardo not in (2,5)         -- Fardo nao estava em estoque
               and  v_entrada_saida      <> 'T'           -- Consiste transacao transferencia
               then
                  raise_application_error(-20000,'Deposito alterado, deve-se usar uma transacao de transferencia.');
               end if;
            end if;

            if :new.data_cardex is null
            then
               raise_application_error(-20000,'Deve-se informar a data de movimentacao.');
            end if;
         end if;
      end if;

      if inserting
      then

         -- le se o deposito controla por volume
         select tipo_volume into v_tipo_volume from basi_205 b
         where b.codigo_deposito = :new.deposito;

         if :new.status_fardo not in (2,5) or v_tipo_volume not in (9)
         then

            if v_entrada_saida <> 'E'
            then
               raise_application_error(-20000,'Na insercao de um fardo novo, a transacao deve ser de entrada.');
            end if;

            -- se o deposito for por quilo, entao o fardo entra como faturado
            if v_tipo_volume not in (9)
            then
               :new.status_fardo := 2;
            end if;

            -- verifica qual o documento e que deve ser enviado para a estq_300,
            -- se e a nota fiscal ou se e o numero do fardo
            if  :new.nf_entrada       is not null and :new.nf_entrada        > 0
            and :new.serie_nf_entrada is not null and :new.serie_nf_entrada <> ' '
            and :new.serie_nf_entrada <> ''
            then
               v_numero_documento    := :new.nf_entrada;
               v_serie_documento     := :new.serie_nf_entrada;
               v_sequencia_documento := 0;
               v_cnpj_9              := :new.cgc9_forne;
               v_cnpj_4              := :new.cgc4_forne;
               v_cnpj_2              := :new.cgc2_forne;
            else
               v_numero_documento    := :new.codigo_fardo;
               v_serie_documento     := ' ';
               v_sequencia_documento := 0;
               v_cnpj_9              := 0;
               v_cnpj_4              := 0;
               v_cnpj_2              := 0;
            end if;

            if :new.peso_real > 0
            then
               v_peso_fardo := :new.peso_real;
            else
               v_peso_fardo := :new.peso_medio;
            end if;

            if v_peso_fardo > 0
            then
               -- atualiza a data de insercao com a data corrente (sysdate)
               :new.data_insercao := trunc(sysdate,'dd');

               if :new.centro_custo is not null
               then v_centro_custo := :new.centro_custo;
               else v_centro_custo := 0;
               end if;

               inter_pr_insere_estq_300 (:new.deposito,         :new.fardo_nivel99,      :new.fardo_grupo,
                                         :new.fardo_subgrupo,   :new.fardo_item,         :new.data_cardex,
                                         :new.lote_mat_prima,   v_numero_documento,      v_serie_documento,
                                         v_cnpj_9,              v_cnpj_4,                v_cnpj_2,
                                         v_sequencia_documento,
                                         :new.transacao_cardex, 'E',                     v_centro_custo,
                                         v_peso_fardo,          :new.valor_movto_cardex, :new.valor_contabil_cardex,
                                         :new.usuario_cardex,   v_tabela_origem_cardex,  :new.nome_prog,
                                         0.00);
            end if;
         end if;
      end if;     -- if inserting


      if deleting
      then

         if :old.status_fardo not in (2,5)
         then

            if :old.peso_real > 0
            then
               v_peso_fardo := :old.peso_real;
            else
               v_peso_fardo := :old.peso_medio;
            end if;

            -- encontra a transacao de cancelamento de producao

            if :old.origem_fardo = 'PERDA'
            then
               select tran_sai_subp, periodo_estoque into v_transacao_cancel, v_periodo_estoque from empr_001;
            else
               select tran_est_produca, periodo_estoque into v_transacao_cancel, v_periodo_estoque from empr_001;
            end if;
            
            if :old.data_entrada < v_periodo_estoque
            then
                raise_application_error(-20000,'Fardo com data de entrada menor que a data do período de estoque. Este Fardo deve ser cancelada, não pode ser excluída. Fardo: ' || 
                :old.codigo_fardo ||  ' - Data entrada Fardo: '  ||  :old.data_entrada || ' - Data período estoque: ' || v_periodo_estoque);
            end if;


            if :new.centro_custo is not null
            then v_centro_custo := :new.centro_custo;
            else v_centro_custo := 0;
            end if;

            inter_pr_insere_estq_300 (:old.deposito,       :old.fardo_nivel99,      :old.fardo_grupo,
                                      :old.fardo_subgrupo, :old.fardo_item,         :old.data_entrada,
                                      :old.lote_mat_prima, :old.codigo_fardo,       ' ',
                                      0,                   0,                        0,
                                      0,
                                      v_transacao_cancel,  'S',                      v_centro_custo,
                                      v_peso_fardo,        :old.valor_movto_cardex,  :old.valor_contabil_cardex,
                                      'DELECAO',           v_tabela_origem_cardex,   ' ',
                                      0.00);
         end if;
      end if;     -- if deleting

      if updating
      then

         -- se o novo status do fardo for 6, ele nao pode atualizar o estoque, pois
         -- o status 6 e de analisado e deve ser pesado para a atualizacao do estoque
         if :new.status_fardo <> 6
         then

            -- movimento de saida
            if   :old.status_fardo not in (2,5)                  -- fardo estava em estoque
            and (:old.deposito         <> :new.deposito          -- deposito alterado
            or   :old.peso_medio        > :new.peso_medio        -- peso do fardo alterado
            or   :old.peso_real         > :new.peso_real         -- peso real do fardo alterado
            or   :old.lote_mat_prima   <> :new.lote_mat_prima    -- lote alterado
            or   :new.status_fardo     in (2,5)                  -- fardo tirado do estoque
            or   :old.status_fardo      = 6)                     -- fardo analisado deve retirado do estoque
            then

               if v_entrada_saida <> 'S' and v_entrada_saida <> 'T'
               then
                  raise_application_error(-20000,'Nao se pode realizar uma movimentacao de saida com uma transacao de entrada.');
               end if;

               if :old.status_fardo = 6
               then
                  v_lote_mat_prima := 0;
               else
                  v_lote_mat_prima := :old.lote_mat_prima;
               end if;

               if  :old.deposito          = :new.deposito        -- mesmo deposito
               and v_lote_mat_prima       = :new.lote_mat_prima  -- mesmo lote
               and :new.status_fardo not in (2,5)                -- fardo continua no estoque
               and :old.status_fardo    <> 6
               then


                  if :old.peso_real = 0 and :new.peso_real = 0
                  then
                     v_peso_fardo := :old.peso_medio - :new.peso_medio;
                  end if;

                  if :old.peso_real = 0 and :new.peso_real > 0
                  then
                     v_peso_fardo := :old.peso_medio - :new.peso_real;
                  end if;

                  if :old.peso_real > 0
                  then
                     v_peso_fardo := :old.peso_real - :new.peso_real;
                  end if;
               else

                  if :old.peso_real > 0
                  then
                     v_peso_fardo := :old.peso_real;
                  else
                     v_peso_fardo := :old.peso_medio;
                  end if;

               end if;

               if v_peso_fardo < 0
               then
                  raise_application_error(-20000,'Nao se pode aumentar o peso do fardo.');
               end if;

               if  :new.num_nota_sai is not null and :new.num_nota_sai  > 0
               and :new.ser_nota_sai is not null and :new.ser_nota_sai <> ' '
               and :new.ser_nota_sai <> ''
               and :new.seq_nota_sai is not null and :new.seq_nota_sai  > 0
               then
                  v_numero_documento    := :new.num_nota_sai;
                  v_serie_documento     := :new.ser_nota_sai;
                  v_sequencia_documento := :new.seq_nota_sai;
                  v_cnpj_9              := 0;
                  v_cnpj_4              := 0;
                  v_cnpj_2              := 0;
               else
                  v_numero_documento    := :new.codigo_fardo;
                  v_serie_documento     := ' ';
                  v_sequencia_documento := 0;
                  v_cnpj_9              := 0;
                  v_cnpj_4              := 0;
                  v_cnpj_2              := 0;
               end if;

               if :new.centro_custo is not null
               then v_centro_custo := :new.centro_custo;
               else v_centro_custo := 0;
               end if;

               inter_pr_insere_estq_300 (:old.deposito,         :old.fardo_nivel99,     :old.fardo_grupo,
                                         :old.fardo_subgrupo,   :old.fardo_item,        :new.data_cardex,
                                         v_lote_mat_prima,      v_numero_documento,      v_serie_documento,
                                         v_cnpj_9,              v_cnpj_4,                v_cnpj_2,
                                         v_sequencia_documento,
                                         :new.transacao_cardex, 'S',                     v_centro_custo,
                                         v_peso_fardo,          :new.valor_movto_cardex, :new.valor_contabil_cardex,
                                         :new.usuario_cardex,   v_tabela_origem_cardex,  :new.nome_prog,
                                         0.00);
            end if;

            if v_entrada_saida = 'T'
            then
               select tipo_volume into v_tipo_volume from basi_205 b
               where b.codigo_deposito = :new.deposito;
            end if;

            -- movimento de entrada
            if   (:new.status_fardo not in (2,5) or              -- fardo esta em estoque
                 (:new.status_fardo = 2   and                    -- Regra de Transferencia de fardos entre deposito de volume para um que nao seja volume,
                   v_entrada_saida  = 'T' and                    -- nesse caso, o status do fardo passara de 1 - Em estoque para o 2 - Faturado.
                   v_tipo_volume    not in (9)))
            and (:old.deposito         <> :new.deposito          -- deposito alterado
            or   :old.lote_mat_prima   <> :new.lote_mat_prima    -- lote alterado
            or   :old.status_fardo     in (2,5,6)                -- fardo nao estava em estoque
            or   (:old.peso_real = 0.00  and :new.peso_real  > 0.00 and -- SS 28530
                  :old.peso_medio = 0.00 and :new.peso_medio > 0.00))
            then

               if v_entrada_saida <> 'T'
               then
                  if v_entrada_saida <> 'E'
                  then
                     raise_application_error(-20000,'Nao se pode realizar uma movimentacao de entrada com uma transacao de saida.');
                  end if;

                  v_transac_entrada := :new.transacao_cardex;
               end if;

               if  :new.nf_entrada       is not null and :new.nf_entrada        > 0
               and :new.serie_nf_entrada is not null and :new.serie_nf_entrada <> ' '
               and :new.serie_nf_entrada <> ''
               then
                  v_numero_documento    := :new.nf_entrada;
                  v_serie_documento     := :new.serie_nf_entrada;
                  v_sequencia_documento := 0;
                  v_cnpj_9              := :new.cgc9_forne;
                  v_cnpj_4              := :new.cgc4_forne;
                  v_cnpj_2              := :new.cgc2_forne;
               else
                  v_numero_documento    := :new.codigo_fardo;
                  v_serie_documento     := ' ';
                  v_sequencia_documento := 0;
                  v_cnpj_9              := 0;
                  v_cnpj_4              := 0;
                  v_cnpj_2              := 0;
               end if;

               if :new.peso_real > 0
               then
                  v_peso_fardo := :new.peso_real;
               else
                  v_peso_fardo := :new.peso_medio;
               end if;

               if :new.centro_custo is not null
               then v_centro_custo := :new.centro_custo;
               else v_centro_custo := 0;
               end if;

               inter_pr_insere_estq_300 (:new.deposito,         :new.fardo_nivel99,      :new.fardo_grupo,
                                         :new.fardo_subgrupo,   :new.fardo_item,         :new.data_cardex,
                                         :new.lote_mat_prima,   v_numero_documento,      v_serie_documento,
                                         v_cnpj_9,              v_cnpj_4,                v_cnpj_2,
                                         v_sequencia_documento,
                                         v_transac_entrada,     'E',                     v_centro_custo,
                                         v_peso_fardo,          :new.valor_movto_cardex, :new.valor_contabil_cardex,
                                         :new.usuario_cardex,   v_tabela_origem_cardex,  :new.nome_prog,
                                         0.00);
             end if;
          end if; -- if statua_fardo <> 6
      end if;     -- if updating
   end if;        -- if (:new.tabela_origem_cardex not in ('FATU_060', 'OBRF_015') or deleting)

   -- zera campos de controle da ficha cardex, assim nao ha perigo da trigger atualizar
   -- o estoque com dados antigos, caso o usuario nao passar estes parametros
   if inserting or updating
   then
      :new.transacao_cardex     := 0;
      :new.usuario_cardex       := ' ';
      :new.tabela_origem_cardex := ' ';
      :new.data_cardex          := null;

   end if;
end;

/

exec inter_pr_recompile;

