
  CREATE OR REPLACE FUNCTION "INTER_FN_AGG_LOTE_FIO" (p_codigo_rolo in number)
return varchar2
is
   lote_fio_concat varchar2(4000) := ' ';

begin
   lote_fio_concat := ' ';

   for l1 in (select pcpt_026.lote_fio lote from pcpt_026
              where pcpt_026.codigo_rolo = p_codigo_rolo)
   loop
      if lote_fio_concat = ' '
      then
           lote_fio_concat := to_char(l1.lote);
      else
           lote_fio_concat := lote_fio_concat || ',' || l1.lote;
      end if;
   end loop;

   return(lote_fio_concat);
end inter_fn_agg_lote_fio;

 

/

exec inter_pr_recompile;

