alter table obrf_164 drop CONSTRAINT obrf_164_PK;

drop index obrf_164_PK;

alter table obrf_164
	add CONSTRAINT obrf_164_PK PRIMARY KEY (id_nfe,tipo_pagamento);
