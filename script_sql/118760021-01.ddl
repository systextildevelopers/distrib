alter table rcnb_067_tmp add valor_unitario number(17,4) default 0.0000;
alter table rcnb_067_tmp add valor_ipi number(15,2) default 0.0;
alter table rcnb_067_tmp add valor_desconto number(15,2) default 0.0;

exec inter_pr_recompile;
