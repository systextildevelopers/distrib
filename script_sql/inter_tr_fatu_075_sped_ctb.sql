
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_075_SPED_CTB" 
   before insert or
   update of nr_titul_codempr, nr_titul_cli_dup_cgc_cli9, nr_titul_cli_dup_cgc_cli4, nr_titul_cli_dup_cgc_cli2,
	nr_titul_cod_tit, nr_titul_num_dup, nr_titul_seq_dup, seq_pagamento,
	data_pagamento, valor_pago, historico_pgto, numero_documento,
	valor_juros, valor_descontos, portador, conta_corrente,
	data_credito, docto_pagto, num_contabil, comissao_lancada,
	alinea, pago_adiantamento, nr_contrato_acc, processo_export_acc,
	valor_pago_moeda, executa_trigger, vlr_desconto_moeda, vlr_juros_moeda
   on fatu_075
   for each row

declare
   v_cliente_fornec  cont_600.cliente_fornecedor_part%type;
   v_cnpj_9          cont_600.cnpj9_participante%type;
   v_cnpj_4          cont_600.cnpj4_participante%type;
   v_cnpj_2          cont_600.cnpj2_participante%type;
   v_sid             cont_601.sid%type;
   v_instancia       cont_601.instancia%type;

begin

   if inserting
   then
      v_cnpj_9 := :new.nr_titul_cli_dup_cgc_cli9;
      v_cnpj_4 := :new.nr_titul_cli_dup_cgc_cli4;
      v_cnpj_2 := :new.nr_titul_cli_dup_cgc_cli2;
   else
      v_cnpj_9 := :old.nr_titul_cli_dup_cgc_cli9;
      v_cnpj_4 := :old.nr_titul_cli_dup_cgc_cli4;
      v_cnpj_2 := :old.nr_titul_cli_dup_cgc_cli2;
   end if;

   v_cliente_fornec := 1; -- cliente

   select sid,   inst_id
   into   v_sid, v_instancia
   from   v_lista_sessao_banco;

   insert into cont_601
      (sid,
       instancia,
       data_insercao,
       tabela_origem,
       cnpj_9,
       cnpj_4,
       cnpj_2,
       cliente_fornec)
   values
      (v_sid,
       v_instancia,
       sysdate,
       'FATU_075',
       v_cnpj_9,
       v_cnpj_4,
       v_cnpj_2,
       v_cliente_fornec);

   exception
      when others then
         null;
end inter_tr_fatu_075_sped_ctb;
-- ALTER TRIGGER "INTER_TR_FATU_075_SPED_CTB" ENABLE
 

/

exec inter_pr_recompile;

