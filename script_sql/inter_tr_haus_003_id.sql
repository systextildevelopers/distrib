create or replace trigger "INTER_TR_HAUS_003_ID"
before insert on HAUS_003
 for each row

declare
  v_nr_registro number;

begin
  select ID_HAUS_003.nextval into v_nr_registro from dual;

  :new.id := v_nr_registro;

end INTER_TR_HAUS_003_ID;
