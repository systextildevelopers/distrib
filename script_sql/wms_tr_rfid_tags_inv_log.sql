create or replace trigger wms_tr_rfid_tags_inv_log
  before insert
  or delete
  or update of codigo_unico_tag, rfid_caixa, periodo_tag, ordem_prod_tag,
  ordem_conf_tag, sequencia_tag, liberar_tag_st, timestamp_insercao, situacao_tag_inv_aut
  on inte_wms_rfid_tags_inv
  for each row
declare
  -- local variables here
  v_rfid_caixa                     inte_wms_rfid_tags_inv.rfid_caixa                   %type;
  v_periodo_tag_new                inte_wms_rfid_tags_inv.periodo_tag                  %type;
  v_periodo_tag_old                inte_wms_rfid_tags_inv.periodo_tag                  %type;
  v_ordem_prod_tag_new             inte_wms_rfid_tags_inv.ordem_prod_tag               %type;
  v_ordem_prod_tag_old             inte_wms_rfid_tags_inv.ordem_prod_tag               %type;
  v_ordem_conf_tag_new             inte_wms_rfid_tags_inv.ordem_conf_tag               %type;
  v_ordem_conf_tag_old             inte_wms_rfid_tags_inv.ordem_conf_tag               %type;
  v_sequencia_tag_new              inte_wms_rfid_tags_inv.sequencia_tag                %type;
  v_sequencia_tag_old              inte_wms_rfid_tags_inv.sequencia_tag                %type;
  v_liberar_tag_st_new             inte_wms_rfid_tags_inv.liberar_tag_st               %type;
  v_liberar_tag_st_old             inte_wms_rfid_tags_inv.liberar_tag_st               %type;

  v_timestamp_insercao_new         inte_wms_rfid_tags_inv.timestamp_insercao           %type;
  v_timestamp_insercao_old         inte_wms_rfid_tags_inv.timestamp_insercao           %type;
  v_situacao_tag_inv_aut_new       inte_wms_rfid_tags_inv.situacao_tag_inv_aut         %type;
  v_situacao_tag_inv_aut_old       inte_wms_rfid_tags_inv.situacao_tag_inv_aut         %type;

  v_codigo_unico_tag_new           inte_wms_rfid_tags_inv.codigo_unico_tag             %type;
  v_codigo_unico_tag_old           inte_wms_rfid_tags_inv.codigo_unico_tag             %type;

  v_sid                            number(9);
  v_empresa                        number(3);
  v_usuario_systextil              varchar2(250);
  v_locale_usuario                 varchar2(5);
  v_nome_programa                  varchar2(20);


  v_operacao                       varchar(1);
  v_data_operacao                  date;
  v_usuario_rede                   varchar(20);
  v_maquina_rede                   varchar(40);
  v_aplicativo                     varchar(20);
begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();

   --alimenta as variaveis new caso seja insert ou update
   if inserting or updating
   then
      if inserting
      then v_operacao := 'i';
      else v_operacao := 'u';
      end if;

      v_rfid_caixa                  := :new.rfid_caixa;
      v_periodo_tag_new             := :new.periodo_tag;
      v_ordem_prod_tag_new          := :new.ordem_prod_tag;
      v_ordem_conf_tag_new          := :new.ordem_conf_tag;
      v_sequencia_tag_new           := :new.sequencia_tag;
      v_liberar_tag_st_new          := :new.liberar_tag_st;
      v_codigo_unico_tag_new        := :new.codigo_unico_tag;
      v_timestamp_insercao_new      := :new.timestamp_insercao;
      v_situacao_tag_inv_aut_new    := :new.situacao_tag_inv_aut;

   end if; --fim do if inserting or updating

   --alimenta as variaveis old caso seja insert ou update
   if deleting or updating
   then
      if deleting
      then
         v_operacao      := 'd';
      else
         v_operacao      := 'u';
      end if;

      v_rfid_caixa                  := :old.rfid_caixa;
      v_periodo_tag_old             := :old.periodo_tag;
      v_ordem_prod_tag_old          := :old.ordem_prod_tag;
      v_ordem_conf_tag_old          := :old.ordem_conf_tag;
      v_sequencia_tag_old           := :old.sequencia_tag;
      v_liberar_tag_st_old          := :old.liberar_tag_st;
      v_codigo_unico_tag_old        := :old.codigo_unico_tag;
      v_timestamp_insercao_old      := :old.timestamp_insercao;
      v_situacao_tag_inv_aut_old    := :old.situacao_tag_inv_aut;

   end if; --fim do if deleting or updating


   -- Dados do usuario logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);


    v_nome_programa := ''; --Deixado de fora por quest?es de performance, a pedido do cliente

   --insere na inte_wms_rfid_tags_inv_log o registro.
   insert into inte_wms_rfid_tags_inv_log (
      rfid_caixa,
      periodo_tag_new,                periodo_tag_old,
      ordem_prod_tag_new,             ordem_prod_tag_old,
      ordem_conf_tag_new,             ordem_conf_tag_old,
      sequencia_tag_new,              sequencia_tag_old,
      liberar_tag_st_new,             liberar_tag_st_old,
      codigo_unico_tag_new,           codigo_unico_tag_old,
      
      timestamp_insercao_new,         timestamp_insercao_old,
      situacao_tag_inv_aut_new,       situacao_tag_inv_aut_old,
      
      operacao,
      data_operacao,                  usuario_rede,
      maquina_rede,                   aplicativo,
      nome_programa
   )
   values (
      v_rfid_caixa,
      v_periodo_tag_new,              v_periodo_tag_old,
      v_ordem_prod_tag_new,           v_ordem_prod_tag_old,
      v_ordem_conf_tag_new,           v_ordem_conf_tag_old,
      v_sequencia_tag_new,            v_sequencia_tag_old,
      v_liberar_tag_st_new,           v_liberar_tag_st_old,
      v_codigo_unico_tag_new,         v_codigo_unico_tag_old,
      
      v_timestamp_insercao_new,       v_timestamp_insercao_old,
      v_situacao_tag_inv_aut_new,     v_situacao_tag_inv_aut_old,
      
      v_operacao,
      v_data_operacao,                v_usuario_rede,
      v_maquina_rede,                 v_aplicativo,
      v_nome_programa
   );

end wms_tr_rfid_tags_inv_log;
/

execute inter_pr_recompile;

/* versao: 1 */

exit;


 exit;

 exit;
