create table FINA_043
(
  id_contrato     VARCHAR2(14),
  numero_contrato NUMBER(14) not null,
  cod_empresa     NUMBER(4) not null,
  data_fechamento DATE not null,
  data_cred       DATE not null,
  banco           VARCHAR2(30),
  agencia         VARCHAR2(6),
  conta           NUMBER(20),
  modalidade      VARCHAR2(10),
  moeda_contrato  VARCHAR2(2),
  valor_contrato  NUMBER(17,2),
  observacoes     VARCHAR2(50),
  valor           NUMBER(17,2) not null,
  conta_cred      NUMBER(9),
  conta_deb       NUMBER(9),
  historico       NUMBER(9),
  descricao       VARCHAR2(50) not null,
  ind_cred_deb    VARCHAR2(1),
  movimentou_reg  VARCHAR2(1) default 'N'
);

alter table FINA_043
  add constraint PK_FINA_043 primary key (NUMERO_CONTRATO, COD_EMPRESA, DATA_FECHAMENTO, DATA_CRED, VALOR, DESCRICAO);


exec inter_pr_recompile;
