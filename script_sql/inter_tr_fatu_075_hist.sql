create or replace trigger "INTER_TR_FATU_075_HIST"
after insert or delete or update
on fatu_075
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);

 if inserting
 then
    begin
        insert into fatu_075_hist (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           nr_titul_codempr_old,   /*8*/
           nr_titul_codempr_new,   /*9*/
           nr_titul_cli_dup_cgc_cli9_old,   /*10*/
           nr_titul_cli_dup_cgc_cli9_new,   /*11*/
           nr_titul_cli_dup_cgc_cli4_old,   /*12*/
           nr_titul_cli_dup_cgc_cli4_new,   /*13*/
           nr_titul_cli_dup_cgc_cli2_old,   /*14*/
           nr_titul_cli_dup_cgc_cli2_new,   /*15*/
           nr_titul_cod_tit_old,   /*16*/
           nr_titul_cod_tit_new,   /*17*/
           nr_titul_num_dup_old,   /*18*/
           nr_titul_num_dup_new,   /*19*/
           nr_titul_seq_dup_old,   /*20*/
           nr_titul_seq_dup_new,   /*21*/
           seq_pagamento_old,   /*22*/
           seq_pagamento_new,   /*23*/
           data_pagamento_old,   /*24*/
           data_pagamento_new,   /*25*/
           valor_pago_old,   /*26*/
           valor_pago_new,   /*27*/
           historico_pgto_old,   /*28*/
           historico_pgto_new,   /*29*/
           numero_documento_old,   /*30*/
           numero_documento_new,   /*31*/
           valor_juros_old,   /*32*/
           valor_juros_new,   /*33*/
           valor_descontos_old,   /*34*/
           valor_descontos_new,   /*35*/
           portador_old,   /*36*/
           portador_new,   /*37*/
           conta_corrente_old,   /*38*/
           conta_corrente_new,   /*39*/
           data_credito_old,   /*40*/
           data_credito_new,   /*41*/
           docto_pagto_old,   /*42*/
           docto_pagto_new,   /*43*/
           num_contabil_old,   /*44*/
           num_contabil_new,   /*45*/
           comissao_lancada_old,   /*46*/
           comissao_lancada_new,   /*47*/
           alinea_old,   /*48*/
           alinea_new,   /*49*/
           pago_adiantamento_old,   /*50*/
           pago_adiantamento_new,   /*51*/
           nr_contrato_acc_old,   /*52*/
           nr_contrato_acc_new,   /*53*/
           processo_export_acc_old,   /*54*/
           processo_export_acc_new,   /*55*/
           valor_pago_moeda_old,   /*56*/
           valor_pago_moeda_new,   /*57*/
           executa_trigger_old,   /*58*/
           executa_trigger_new,   /*59*/
           vlr_desconto_moeda_old,   /*60*/
           vlr_desconto_moeda_new,   /*61*/
           vlr_juros_moeda_old,   /*62*/
           vlr_juros_moeda_new,   /*63*/
           nr_mtv_abatimento_old,   /*64*/
           nr_mtv_abatimento_new,   /*65*/
           flag_dev_consignado_old,   /*66*/
           flag_dev_consignado_new,   /*67*/
           vlr_var_cambial_old,   /*68*/
           vlr_var_cambial_new   /*69*/
           )
           values (
           'i', /*o*/
           sysdate, /*1*/
           sysdate,/*2*/
           ws_usuario_rede,/*3*/
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/
           v_nome_programa, /*7*/
           null,/*8*/
           :new.nr_titul_codempr, /*9*/
           null,/*10*/
           :new.nr_titul_cli_dup_cgc_cli9, /*11*/
           null,/*12*/
           :new.nr_titul_cli_dup_cgc_cli4, /*13*/
           null,/*14*/
           :new.nr_titul_cli_dup_cgc_cli2, /*15*/
           null,/*16*/
           :new.nr_titul_cod_tit, /*17*/
           null,/*18*/
           :new.nr_titul_num_dup, /*19*/
           null,/*20*/
           :new.nr_titul_seq_dup, /*21*/
           null,/*22*/
           :new.seq_pagamento, /*23*/
           null,/*24*/
           :new.data_pagamento, /*25*/
           null,/*26*/
           :new.valor_pago, /*27*/
           null,/*28*/
           :new.historico_pgto, /*29*/
           null,/*30*/
           :new.numero_documento, /*31*/
           null,/*32*/
           :new.valor_juros, /*33*/
           null,/*34*/
           :new.valor_descontos, /*35*/
           null,/*36*/
           :new.portador, /*37*/
           null,/*38*/
           :new.conta_corrente, /*39*/
           null,/*40*/
           :new.data_credito, /*41*/
           null,/*42*/
           :new.docto_pagto, /*43*/
           null,/*44*/
           :new.num_contabil, /*45*/
           null,/*46*/
           :new.comissao_lancada, /*47*/
           null,/*48*/
           :new.alinea, /*49*/
           null,/*50*/
           :new.pago_adiantamento, /*51*/
           null,/*52*/
           :new.nr_contrato_acc, /*53*/
           null,/*54*/
           :new.processo_export_acc, /*55*/
           null,/*56*/
           :new.valor_pago_moeda, /*57*/
           null,/*58*/
           :new.executa_trigger, /*59*/
           null,/*60*/
           :new.vlr_desconto_moeda, /*61*/
           null,/*62*/
           :new.vlr_juros_moeda, /*63*/
           null,/*64*/
           :new.nr_mtv_abatimento, /*65*/
           null,/*66*/
           :new.flag_dev_consignado, /*67*/
           null, /*68*/
           :new.vlr_var_cambial  /*69*/
         );
    end;
 end if;

 if updating
 then
    begin
        insert into fatu_075_hist (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           nr_titul_codempr_old,   /*8*/
           nr_titul_codempr_new,   /*9*/
           nr_titul_cli_dup_cgc_cli9_old,   /*10*/
           nr_titul_cli_dup_cgc_cli9_new,   /*11*/
           nr_titul_cli_dup_cgc_cli4_old,   /*12*/
           nr_titul_cli_dup_cgc_cli4_new,   /*13*/
           nr_titul_cli_dup_cgc_cli2_old,   /*14*/
           nr_titul_cli_dup_cgc_cli2_new,   /*15*/
           nr_titul_cod_tit_old,   /*16*/
           nr_titul_cod_tit_new,   /*17*/
           nr_titul_num_dup_old,   /*18*/
           nr_titul_num_dup_new,   /*19*/
           nr_titul_seq_dup_old,   /*20*/
           nr_titul_seq_dup_new,   /*21*/
           seq_pagamento_old,   /*22*/
           seq_pagamento_new,   /*23*/
           data_pagamento_old,   /*24*/
           data_pagamento_new,   /*25*/
           valor_pago_old,   /*26*/
           valor_pago_new,   /*27*/
           historico_pgto_old,   /*28*/
           historico_pgto_new,   /*29*/
           numero_documento_old,   /*30*/
           numero_documento_new,   /*31*/
           valor_juros_old,   /*32*/
           valor_juros_new,   /*33*/
           valor_descontos_old,   /*34*/
           valor_descontos_new,   /*35*/
           portador_old,   /*36*/
           portador_new,   /*37*/
           conta_corrente_old,   /*38*/
           conta_corrente_new,   /*39*/
           data_credito_old,   /*40*/
           data_credito_new,   /*41*/
           docto_pagto_old,   /*42*/
           docto_pagto_new,   /*43*/
           num_contabil_old,   /*44*/
           num_contabil_new,   /*45*/
           comissao_lancada_old,   /*46*/
           comissao_lancada_new,   /*47*/
           alinea_old,   /*48*/
           alinea_new,   /*49*/
           pago_adiantamento_old,   /*50*/
           pago_adiantamento_new,   /*51*/
           nr_contrato_acc_old,   /*52*/
           nr_contrato_acc_new,   /*53*/
           processo_export_acc_old,   /*54*/
           processo_export_acc_new,   /*55*/
           valor_pago_moeda_old,   /*56*/
           valor_pago_moeda_new,   /*57*/
           executa_trigger_old,   /*58*/
           executa_trigger_new,   /*59*/
           vlr_desconto_moeda_old,   /*60*/
           vlr_desconto_moeda_new,   /*61*/
           vlr_juros_moeda_old,   /*62*/
           vlr_juros_moeda_new,   /*63*/
           nr_mtv_abatimento_old,   /*64*/
           nr_mtv_abatimento_new,   /*65*/
           flag_dev_consignado_old,   /*66*/
           flag_dev_consignado_new,   /*67*/
           vlr_var_cambial_old,   /*68*/
           vlr_var_cambial_new   /*69*/
           )
            values (
           'a', /*o*/
           sysdate, /*1*/
           sysdate,/*2*/
           ws_usuario_rede,/*3*/
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/
           v_nome_programa, /*7*/
           :old.nr_titul_codempr,/*8*/
           :new.nr_titul_codempr, /*9*/
           :old.nr_titul_cli_dup_cgc_cli9,/*10*/
           :new.nr_titul_cli_dup_cgc_cli9, /*11*/
           :old.nr_titul_cli_dup_cgc_cli4,/*12*/
           :new.nr_titul_cli_dup_cgc_cli4, /*13*/
           :old.nr_titul_cli_dup_cgc_cli2,/*14*/
           :new.nr_titul_cli_dup_cgc_cli2, /*15*/
           :old.nr_titul_cod_tit,/*16*/
           :new.nr_titul_cod_tit, /*17*/
           :old.nr_titul_num_dup,/*18*/
           :new.nr_titul_num_dup, /*19*/
           :old.nr_titul_seq_dup,/*20*/
           :new.nr_titul_seq_dup, /*21*/
           :old.seq_pagamento,/*22*/
           :new.seq_pagamento, /*23*/
           :old.data_pagamento,/*24*/
           :new.data_pagamento, /*25*/
           :old.valor_pago,/*26*/
           :new.valor_pago, /*27*/
           :old.historico_pgto,/*28*/
           :new.historico_pgto, /*29*/
           :old.numero_documento,/*30*/
           :new.numero_documento, /*31*/
           :old.valor_juros,/*32*/
           :new.valor_juros, /*33*/
           :old.valor_descontos,/*34*/
           :new.valor_descontos, /*35*/
           :old.portador,/*36*/
           :new.portador, /*37*/
           :old.conta_corrente,/*38*/
           :new.conta_corrente, /*39*/
           :old.data_credito,/*40*/
           :new.data_credito, /*41*/
           :old.docto_pagto,/*42*/
           :new.docto_pagto, /*43*/
           :old.num_contabil,/*44*/
           :new.num_contabil, /*45*/
           :old.comissao_lancada,/*46*/
           :new.comissao_lancada, /*47*/
           :old.alinea,/*48*/
           :new.alinea, /*49*/
           :old.pago_adiantamento,/*50*/
           :new.pago_adiantamento, /*51*/
           :old.nr_contrato_acc,/*52*/
           :new.nr_contrato_acc, /*53*/
           :old.processo_export_acc,/*54*/
           :new.processo_export_acc, /*55*/
           :old.valor_pago_moeda,/*56*/
           :new.valor_pago_moeda, /*57*/
           :old.executa_trigger,/*58*/
           :new.executa_trigger, /*59*/
           :old.vlr_desconto_moeda,/*60*/
           :new.vlr_desconto_moeda, /*61*/
           :old.vlr_juros_moeda,/*62*/
           :new.vlr_juros_moeda, /*63*/
           :old.nr_mtv_abatimento,/*64*/
           :new.nr_mtv_abatimento, /*65*/
           :old.flag_dev_consignado,/*66*/
           :new.flag_dev_consignado, /*67*/
           :old.vlr_var_cambial, /*68*/
           :new.vlr_var_cambial  /*69*/
         );
    end;
 end if;

 if deleting
 then
    begin
        insert into fatu_075_hist (
           tipo_ocorr,   /*0*/
           data_ocorr,   /*1*/
           hora_ocorr,   /*2*/
           usuario_rede,   /*3*/
           maquina_rede,   /*4*/
           aplicacao,   /*5*/
           usuario_sistema,   /*6*/
           nome_programa,   /*7*/
           nr_titul_codempr_old,   /*8*/
           nr_titul_codempr_new,   /*9*/
           nr_titul_cli_dup_cgc_cli9_old,   /*10*/
           nr_titul_cli_dup_cgc_cli9_new,   /*11*/
           nr_titul_cli_dup_cgc_cli4_old,   /*12*/
           nr_titul_cli_dup_cgc_cli4_new,   /*13*/
           nr_titul_cli_dup_cgc_cli2_old,   /*14*/
           nr_titul_cli_dup_cgc_cli2_new,   /*15*/
           nr_titul_cod_tit_old,   /*16*/
           nr_titul_cod_tit_new,   /*17*/
           nr_titul_num_dup_old,   /*18*/
           nr_titul_num_dup_new,   /*19*/
           nr_titul_seq_dup_old,   /*20*/
           nr_titul_seq_dup_new,   /*21*/
           seq_pagamento_old,   /*22*/
           seq_pagamento_new,   /*23*/
           data_pagamento_old,   /*24*/
           data_pagamento_new,   /*25*/
           valor_pago_old,   /*26*/
           valor_pago_new,   /*27*/
           historico_pgto_old,   /*28*/
           historico_pgto_new,   /*29*/
           numero_documento_old,   /*30*/
           numero_documento_new,   /*31*/
           valor_juros_old,   /*32*/
           valor_juros_new,   /*33*/
           valor_descontos_old,   /*34*/
           valor_descontos_new,   /*35*/
           portador_old,   /*36*/
           portador_new,   /*37*/
           conta_corrente_old,   /*38*/
           conta_corrente_new,   /*39*/
           data_credito_old,   /*40*/
           data_credito_new,   /*41*/
           docto_pagto_old,   /*42*/
           docto_pagto_new,   /*43*/
           num_contabil_old,   /*44*/
           num_contabil_new,   /*45*/
           comissao_lancada_old,   /*46*/
           comissao_lancada_new,   /*47*/
           alinea_old,   /*48*/
           alinea_new,   /*49*/
           pago_adiantamento_old,   /*50*/
           pago_adiantamento_new,   /*51*/
           nr_contrato_acc_old,   /*52*/
           nr_contrato_acc_new,   /*53*/
           processo_export_acc_old,   /*54*/
           processo_export_acc_new,   /*55*/
           valor_pago_moeda_old,   /*56*/
           valor_pago_moeda_new,   /*57*/
           executa_trigger_old,   /*58*/
           executa_trigger_new,   /*59*/
           vlr_desconto_moeda_old,   /*60*/
           vlr_desconto_moeda_new,   /*61*/
           vlr_juros_moeda_old,   /*62*/
           vlr_juros_moeda_new,   /*63*/
           nr_mtv_abatimento_old,   /*64*/
           nr_mtv_abatimento_new,   /*65*/
           flag_dev_consignado_old,   /*66*/
           flag_dev_consignado_new,   /*67*/
           vlr_var_cambial_old,   /*68*/
           vlr_var_cambial_new   /*69*/
           )
           values (
           'd', /*o*/
           sysdate, /*1*/
           sysdate,/*2*/
           ws_usuario_rede,/*3*/
           ws_maquina_rede, /*4*/
           ws_aplicativo, /*5*/
           ws_usuario_systextil,/*6*/
           v_nome_programa, /*7*/
           :old.nr_titul_codempr,/*8*/
           null, /*9*/
           :old.nr_titul_cli_dup_cgc_cli9,/*10*/
           null, /*11*/
           :old.nr_titul_cli_dup_cgc_cli4,/*12*/
           null, /*13*/
           :old.nr_titul_cli_dup_cgc_cli2,/*14*/
           null, /*15*/
           :old.nr_titul_cod_tit,/*16*/
           null, /*17*/
           :old.nr_titul_num_dup,/*18*/
           null, /*19*/
           :old.nr_titul_seq_dup,/*20*/
           null, /*21*/
           :old.seq_pagamento,/*22*/
           null, /*23*/
           :old.data_pagamento,/*24*/
           null, /*25*/
           :old.valor_pago,/*26*/
           null, /*27*/
           :old.historico_pgto,/*28*/
           null, /*29*/
           :old.numero_documento,/*30*/
           null, /*31*/
           :old.valor_juros,/*32*/
           null, /*33*/
           :old.valor_descontos,/*34*/
           null, /*35*/
           :old.portador,/*36*/
           null, /*37*/
           :old.conta_corrente,/*38*/
           null, /*39*/
           :old.data_credito,/*40*/
           null, /*41*/
           :old.docto_pagto,/*42*/
           null, /*43*/
           :old.num_contabil,/*44*/
           null, /*45*/
           :old.comissao_lancada,/*46*/
           null, /*47*/
           :old.alinea,/*48*/
           null, /*49*/
           :old.pago_adiantamento,/*50*/
           null, /*51*/
           :old.nr_contrato_acc,/*52*/
           null, /*53*/
           :old.processo_export_acc,/*54*/
           null, /*55*/
           :old.valor_pago_moeda,/*56*/
           null, /*57*/
           :old.executa_trigger,/*58*/
           null, /*59*/
           :old.vlr_desconto_moeda,/*60*/
           null, /*61*/
           :old.vlr_juros_moeda,/*62*/
           null, /*63*/
           :old.nr_mtv_abatimento,/*64*/
           null, /*65*/
           :old.flag_dev_consignado,/*66*/
           null, /*67*/
           :old.vlr_var_cambial,/*68*/
           null /*69*/
         );
    end;
 end if;
end inter_tr_fatu_075_hist;

-- ALTER TRIGGER "INTER_TR_FATU_075_HIST" ENABLE
 
/

exec inter_pr_recompile;

