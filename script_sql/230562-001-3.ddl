alter table fatu_050 
add status_expedicao_adps number(1);

alter table fatu_050 modify 
status_expedicao_adps default 0;

update fatu_050 
set status_expedicao_adps = 0;

commit;
