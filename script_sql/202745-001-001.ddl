CREATE TABLE PEDI_266
(
   PEDIDO    NUMBER(9)   DEFAULT 0 NOT NULL,
   SEQUENCIA NUMBER(3)   DEFAULT 0 NOT NULL,
   DESCONTO1 NUMBER(6,2) DEFAULT 0.0,
   DESCONTO2 NUMBER(6,2) DEFAULT 0.0,
   DESCONTO3 NUMBER(6,2) DEFAULT 0.0,
   DESC_CALC NUMBER(6,2) DEFAULT 0.0
);

-- Create/Recreate primary, unique and foreign key constraints 
alter table PEDI_266 
  add constraint PK_PEDI_266  primary key (PEDIDO,SEQUENCIA);
  
/

exec inter_pr_recompile;
