alter table rcnb_755
add (faixa_etaria_custos number(3));

alter table rcnb_755
modify (faixa_etaria_custos default 0);

/
declare
cursor rcnb_755_c is
   select f.rowid from rcnb_755 f where faixa_etaria_custos is null;
   
contaRegistro number;

begin
   contaRegistro := 0;
   
   for reg_rcnb_755_c in rcnb_755_c
   loop
      update rcnb_755
      set faixa_etaria_custos   = 0
      where faixa_etaria_custos is null
        and rcnb_755.rowid = reg_rcnb_755_c.rowid;
      
      contaRegistro := contaRegistro + 1;
      
      if contaRegistro = 1000
      then
         contaRegistro := 0;
         commit;
      end if;
   end loop; 

   commit;
end;
/
alter table rcnb_755
  drop constraint PK_RCNB_755;

drop index PK_RCNB_755;

alter table rcnb_755
add  constraint PK_RCNB_755
primary key (CODIGO_EMPRESA, NIVEL_ESTRUTURA, GRUPO_ESTRUTURA, SUBGRU_ESTRUTURA, ITEM_ESTRUTURA, NOME_USUARIO, COLECAO, CODIGO_AGRUPADOR_CC, DESCRICAO_AGRUPADOR, ALTERNATIVA, TIPO_ANALISE,faixa_etaria_custos);

execute inter_pr_recompile;     
