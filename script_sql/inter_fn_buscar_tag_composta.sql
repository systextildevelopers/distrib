
  CREATE OR REPLACE FUNCTION "INTER_FN_BUSCAR_TAG_COMPOSTA" (
      par_tag_programa  in varchar2,
      v_par_tag_0       in varchar2,
      v_par_tag_1       in varchar2,
      v_par_tag_2       in varchar2,
      v_par_tag_3       in varchar2,
      v_par_tag_4       in varchar2,
      v_par_tag_5       in varchar2,
      v_par_tag_6       in varchar2,
      v_par_tag_7       in varchar2,
      v_par_tag_8       in varchar2,
      v_par_tag_9       in varchar2,
      codigo_idioma     in varchar2,
      usuario_systextil in varchar2)
   return varchar2
is
   -- Declara as variaveis para a extracao do tag;
   valor_tag_comp_aux      varchar2(4000);

   x_par_tag_0             varchar2(4000);
   x_par_tag_1             varchar2(4000);
   x_par_tag_2             varchar2(4000);
   x_par_tag_3             varchar2(4000);
   x_par_tag_4             varchar2(4000);
   x_par_tag_5             varchar2(4000);
   x_par_tag_6             varchar2(4000);
   x_par_tag_7             varchar2(4000);
   x_par_tag_8             varchar2(4000);
   x_par_tag_9             varchar2(4000);

begin
   valor_tag_comp_aux := inter_fn_buscar_tag(inter_fn_extrai_tag(par_tag_programa),codigo_idioma,usuario_systextil);

   x_par_tag_0 := v_par_tag_0;
   x_par_tag_1 := v_par_tag_1;
   x_par_tag_2 := v_par_tag_2;
   x_par_tag_3 := v_par_tag_3;
   x_par_tag_4 := v_par_tag_4;
   x_par_tag_5 := v_par_tag_5;
   x_par_tag_6 := v_par_tag_6;
   x_par_tag_7 := v_par_tag_7;
   x_par_tag_8 := v_par_tag_8;
   x_par_tag_9 := v_par_tag_9;

   valor_tag_comp_aux := replace(valor_tag_comp_aux,'{0}',x_par_tag_0);
   valor_tag_comp_aux := replace(valor_tag_comp_aux,'{1}',x_par_tag_1);
   valor_tag_comp_aux := replace(valor_tag_comp_aux,'{2}',x_par_tag_2);
   valor_tag_comp_aux := replace(valor_tag_comp_aux,'{3}',x_par_tag_3);
   valor_tag_comp_aux := replace(valor_tag_comp_aux,'{4}',x_par_tag_4);
   valor_tag_comp_aux := replace(valor_tag_comp_aux,'{5}',x_par_tag_5);
   valor_tag_comp_aux := replace(valor_tag_comp_aux,'{6}',x_par_tag_6);
   valor_tag_comp_aux := replace(valor_tag_comp_aux,'{7}',x_par_tag_7);
   valor_tag_comp_aux := replace(valor_tag_comp_aux,'{8}',x_par_tag_8);
   valor_tag_comp_aux := replace(valor_tag_comp_aux,'{9}',x_par_tag_9);

   return (valor_tag_comp_aux);
end;

 

/

exec inter_pr_recompile;

