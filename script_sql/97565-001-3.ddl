--14083
alter table obrf_157
add (vBCUFDest         number(15,2),
     pFCPUFDest        number(5,2),
     pICMSUFDest       number(5,2),
     pICMSInter        number(5,2),
     pICMSInterPart    number(5,2),
     vFCPUFDest        number(15,2),
     vICMSUFDest       number(15,2),
     vICMSUFRemet      number(15,2));


exec inter_pr_recompile;
/