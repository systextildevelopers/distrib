-- Create table
create table PEDI_224
(
  pedido_venda       NUMBER(9) default 0 not null,
  codigo_ocorrencia    NUMBER(4) default 0 not null,
  sequencia_lancamento NUMBER(4) default 0 not null,
  seq_operacao         NUMBER(4) default 0,
  usuario_lancamento   VARCHAR2(250),
  data_lancamento      DATE,
  hora_lancamento      DATE
);
comment on table PEDI_224 is 'Ocorrencias de Pedido de Venda';
comment on column PEDI_224.pedido_venda is 'Pedido de Venda.';
comment on column PEDI_224.codigo_ocorrencia is 'Ocorrências de Pedido de Venda.';
comment on column PEDI_224.seq_operacao is 'Sequencia de operacão do pacote.';
comment on column PEDI_224.usuario_lancamento is 'Usuário que esta lançando a ocorrência.';
comment on column PEDI_224.data_lancamento is 'Data que esta lançando a ocorrência.';
comment on column PEDI_224.hora_lancamento is 'Hora que esta lançando a ocorrência.';
alter table PEDI_224
  add constraint PK_PEDI_224 primary key (PEDIDO_VENDA, CODIGO_OCORRENCIA, SEQUENCIA_LANCAMENTO);
alter table PEDI_224
  add constraint REF_PEDI_224_PEDI_223 foreign key (CODIGO_OCORRENCIA)
  references PEDI_223 (CODIGO_OCORRENCIA);
