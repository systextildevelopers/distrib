ALTER TABLE haus_004
DROP CONSTRAINT fk_haus_004_id_haus_002;

ALTER TABLE haus_004
ADD CONSTRAINT fk_haus_004_id_haus_003
FOREIGN KEY (id_haus_003)
REFERENCES haus_003 (id);

alter table haus_001
add serie varchar2(3) default '';

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('lune_e196', 'Integracao PostHaus', 0, 1, 0);

commit;
