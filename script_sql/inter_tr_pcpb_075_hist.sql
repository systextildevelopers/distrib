CREATE OR REPLACE TRIGGER INTER_TR_PCPB_075_HIST

BEFORE DELETE OR INSERT OR
UPDATE of grupo_estrutura, subgru_estrutura, item_estrutura, sequencia,
	codigo_informacao, observacao, valor_01, valor_02,
	situacao, codigo_estagio, data_informacao, data_alt,
	hora_alt, valor_03, valor_04, valor_05,
	valor_06, valor_07, valor_08, valor_09,
	valor_10, valor_11, exige_baixa, lote_produto,
	codigo_motivo, tipo_informacao, ordem_producao, nivel_estrutura
on pcpb_075
for each row

declare

   ws_ordem_producao    pcpb_075.ordem_producao%type;


   ws_tipo_historico         number(1);
   ws_sid                    number;

   ws_descricao_historico    varchar2(4000);
   ws_tipo_ocorr             varchar2(1);
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_nome_programa          hdoc_090.programa%type;
   ws_aplicacao              varchar2(20);
   ws_usuario_sistema        varchar2(250);

   ws_empresa                number(5);

   ws_locale_usuario         varchar2(20);
   v_processo_systextil      varchar2(40);
begin
      if INSERTING then
         ws_ordem_producao        := :new.ordem_producao;

         ws_tipo_historico        := 1;
         ws_descricao_historico   := 'INCLUSAO (PCPB_075) '
                                   || chr(10)
                                   || chr(10)
                                   ||'  TIPO INFORMA��O: '
                                   || :new.tipo_informacao
                                   || chr(10)
                                   ||'  ITEM: '
                                   || :new.nivel_estrutura
                                   ||'.'
                                   || :new.grupo_estrutura
                                   ||'.'
                                   || :new.subgru_estrutura
                                   ||'.'
                                   || :new.item_estrutura
                                   || chr(10)
                                   ||'  SEQU�NCIA: '
                                   || :new.sequencia
                                   || chr(10)
                                   ||'  COD. INFORMA��O: '
                                   || :new.codigo_informacao
                                   || chr(10)
                                   ||'  OBSERVA��O: '
                                   || :new.observacao
                                   || chr(10)
                                   ||'  CODIGO MOTIVO: '
                                   || :new.codigo_motivo
                                   || chr(10)
                                   ||'  LOTE PRODU��O: '
                                   || :new.lote_produto
                                   || chr(10)
                                   ||'  DATA ALT: '
                                   || :new.data_alt
                                   || chr(10)
                                   ||'  HORA ALT: '
                                   ||to_char(:new.hora_alt,'hh24:mi:ss')
                                   || chr(10)
                                   ||'  DATA INFORMA��O: '
                                   || :new.data_informacao;


         ws_tipo_ocorr            := 'I';
         ws_usuario_sistema       := '';
      elsif DELETING then

            ws_ordem_producao        := :old.ordem_producao;

            ws_tipo_historico        := 1;

            ws_descricao_historico   := 'ELIMINA��O (PCPB_075) '
                                        || chr(10)
                                        || chr(10)
                                        ||'  TIPO INFORMA��O: '
                                        || :old.tipo_informacao
                                        || chr(10)
                                        ||'  ITEM: '
                                        || :old.nivel_estrutura
                                        ||'.'
                                        || :old.grupo_estrutura
                                        ||'.'
                                        || :old.subgru_estrutura
                                        ||'.'
                                        || :old.item_estrutura
                                        || chr(10)
                                        ||'  SEQU�NCIA: '
                                        || :old.sequencia
                                        || chr(10)
                                        ||'  COD. INFORMA��O: '
                                        || :old.codigo_informacao
                                        || chr(10)
                                        ||'  OBSERVA��O: '
                                        || :old.observacao
                                        || chr(10)
                                        ||'  CODIGO MOTIVO: '
                                        || :old.codigo_motivo
                                        || chr(10)
                                        ||'  LOTE PRODU��O: '
                                        || :old.lote_produto
                                        || chr(10)
                                        ||'  DATA ALT: '
                                        || :old.data_alt
                                        || chr(10)
                                        ||'  HORA ALT: '
                                        || to_char(:old.hora_alt,'hh24:mi:ss')
                                        || chr(10)
                                        ||'  DATA INFORMA��O: '
                                        || :old.data_informacao;


            ws_tipo_ocorr            := 'D';
            ws_usuario_sistema       := '';
       end if;

       if UPDATING then

            ws_tipo_historico        := 1;

            ws_ordem_producao := :old.ordem_producao;

            ws_descricao_historico   := 'ALTERA��O (PCPB_075) '
                                        || chr(10);
            if (:new.ordem_producao <>  :old.ordem_producao)
            then ws_descricao_historico   := ws_descricao_historico
                                             || chr(10)
                                             ||' ORDEM PRODU��O: '
                                             ||:old.ordem_producao
                                             ||'    ->   '
                                             ||:new.ordem_producao;
            end if;

            if (:new.tipo_informacao <>  :old.tipo_informacao)
            then ws_descricao_historico   := ws_descricao_historico
                                             || chr(10)
                                             ||' TIPO INFORMA��O: '
                                             ||:old.tipo_informacao
                                             ||'    ->   '
                                             ||:new.tipo_informacao;
            end if;

            if ((:new.nivel_estrutura <> :old.nivel_estrutura)
             or (:new.grupo_estrutura  <> :old.grupo_estrutura)
             or (:new.subgru_estrutura <> :old.subgru_estrutura)
             or (:new.item_estrutura   <> :old.item_estrutura))
            then ws_descricao_historico   := ws_descricao_historico
                                             || chr(10)
                                             ||' ITEM: '
                                             || :old.nivel_estrutura
                                             ||'.'
                                             || :old.grupo_estrutura
                                             ||'.'
                                             || :old.subgru_estrutura
                                             ||'.'
                                             || :old.item_estrutura
                                             ||'    ->   '
                                             || :new.nivel_estrutura
                                             ||'.'
                                             || :new.grupo_estrutura
                                             ||'.'
                                             || :new.subgru_estrutura
                                             ||'.'
                                             || :new.item_estrutura;
            end if;

            if (:new.sequencia <>  :old.sequencia)
            then ws_descricao_historico   := ws_descricao_historico
                                              || chr(10)
                                              ||' SEQU�NCIA: '
                                              ||:old.sequencia
                                              ||'    ->   '
                                              ||:new.sequencia;
            end if;

            if (:new.codigo_informacao <>  :old.codigo_informacao)
            then ws_descricao_historico   := ws_descricao_historico
                                              || chr(10)
                                              ||' COD. INFORMA��O: '
                                              ||:old.codigo_informacao
                                              ||'    ->   '
                                              ||:new.codigo_informacao;
            end if;

            if (:new.data_informacao <>  :old.data_informacao)
            then ws_descricao_historico   := ws_descricao_historico
                                              || chr(10)
                                              ||' DATA INFORMA��O: '
                                              ||:old.data_informacao
                                              ||'    ->   '
                                              ||:new.data_informacao;
            end if;

            if (:new.lote_produto <>  :old.lote_produto)
            then ws_descricao_historico   := ws_descricao_historico
                                             || chr(10)
                                             ||' LOTE PRODUTO: '
                                             ||:old.lote_produto
                                             ||'    ->   '
                                             ||:new.lote_produto;
            end if;

            if (:new.codigo_motivo <>  :old.codigo_motivo)
            then ws_descricao_historico   := ws_descricao_historico
                                             || chr(10)
                                             ||' C�DIGO MOTIVO: '
                                             ||:old.codigo_motivo
                                             ||'    ->   '
                                             ||:new.codigo_motivo;
            end if;

            if (:new.data_alt <>  :old.data_alt)
            then ws_descricao_historico   := ws_descricao_historico
                                             || chr(10)
                                             ||' DATA ALT: '
                                             ||:old.data_alt
                                             ||'    ->   '
                                             ||:new.data_alt;
            end if;

            if (:new.hora_alt <>  :old.hora_alt)
            then ws_descricao_historico   := ws_descricao_historico
                                             || chr(10)
                                             ||' HORA ALT: '
                                             ||to_char(:old.hora_alt,'hh24:mi:ss')
                                             ||'    ->   '
                                             ||to_char(:new.hora_alt,'hh24:mi:ss');
            end if;


            ws_tipo_ocorr            := 'A';
            ws_usuario_sistema       := '';
      end if;

      -- Encontra dados do usu�rio da rede, m�quina e aplicativo que esta atualizando a ficha
      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicacao,     ws_sid,
                              ws_usuario_sistema,     ws_empresa,        ws_locale_usuario);

      v_processo_systextil := inter_fn_nome_programa(ws_sid);

      begin
         INSERT INTO hist_010
            (area_producao,           ordem_producao,          periodo_producao,
             ordem_confeccao,         tipo_historico,          descricao_historico,
             tipo_ocorr,              data_ocorr,              hora_ocorr,
             usuario_rede,            maquina_rede,            aplicacao,
             usuario_sistema,         nome_programa )
         VALUES
            ('2',                     ws_ordem_producao,       0,
             0,                       ws_tipo_historico,       ws_descricao_historico,
             ws_tipo_ocorr,           sysdate,                 sysdate,
             ws_usuario_rede,         ws_maquina_rede,         ws_aplicacao,
             ws_usuario_sistema,      ws_nome_programa);
      end;

end inter_tr_pcpb_075_hist;

-- ALTER TRIGGER INTER_TR_PCPB_075_HIST ENABLE


/
