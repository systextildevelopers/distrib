CREATE OR REPLACE FUNCTION "INTER_FN_BASI_400_VALOR_4" (nivel_estrutura       in varchar2, 
                                                        grupo_estrutura       in varchar2, 
                                                        subgru_estrutura      in varchar2, 
                                                        item_estrutura        in varchar2,
                                                        tipo_informacao_var   in number,
                                                        codigo_informacao_var in number)
                                                        
return number is
   valor04 number;
begin

    valor04 := 0;
    
    begin
      select valor_04
      into valor04
      from (select (CASE WHEN basi_400.nivel   = nivel_estrutura
                        and  basi_400.grupo    = grupo_estrutura
                        and  basi_400.subgrupo = subgru_estrutura
                        and  basi_400.item     = item_estrutura THEN 1
                       WHEN  basi_400.nivel    = nivel_estrutura
                        and  basi_400.grupo    = grupo_estrutura
                        and  basi_400.subgrupo = subgru_estrutura
                        and  basi_400.item     = '000000' THEN 2
                       WHEN  basi_400.nivel    = nivel_estrutura
                        and  basi_400.grupo    = grupo_estrutura
                        and  basi_400.subgrupo = '000'
                        and  basi_400.item     = '000000' THEN 3
                       WHEN  basi_400.nivel    = nivel_estrutura
                        and  basi_400.grupo    = '00000' 
                        and  basi_400.subgrupo = '000'
                        and  basi_400.item     = '000000' THEN 4
                       WHEN  basi_400.nivel    = nivel_estrutura
                        and  basi_400.grupo    = '00000' 
                        and  basi_400.subgrupo = '000'
                        and  basi_400.item     = item_estrutura THEN 5
                       ELSE 0
                    END) as seq, 
                    basi_400.valor_04
            from basi_400
            where basi_400.nivel             = nivel_estrutura
              and basi_400.grupo             in (grupo_estrutura,'00000')
              and basi_400.subgrupo          in (subgru_estrutura,'000')
              and basi_400.item              in (item_estrutura,'000000')
              and basi_400.tipo_informacao   = tipo_informacao_var
              and basi_400.codigo_informacao = codigo_informacao_var
              order by seq) basi400
      where rownum = 1
      order by seq;
      exception 
        when no_data_found
        then valor04 := 0;
    
    end;
    return(valor04);
    
end inter_fn_basi_400_valor_4;
/
