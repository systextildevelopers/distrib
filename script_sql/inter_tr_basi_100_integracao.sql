
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_100_INTEGRACAO" 
before insert or
       delete or
       update of cor_sortimento,     tipo_cor,        descricao,          serie_cor,
                 numero_quadro,      cor_de_fundo,    sortim_estampa,     observacao1,
                 observacao2,        relacao_banho,   tipo_tingimento,    tonalidade,
                 seq_degrade,        cor_integracao,  cor_pantone,        cor_numerica,
                 cor_representativa, complemento_descricao
       on basi_100
for each row

declare
   v_cor_sortimento             e_basi_100.cor_sortimento%type;
   v_tipo_cor           	      e_basi_100.tipo_cor%type;
   v_descricao          	      e_basi_100.descricao%type;
   v_serie_cor          	      e_basi_100.serie_cor%type;
   v_numero_quadro      	      e_basi_100.numero_quadro%type;
   v_cor_de_fundo       	      e_basi_100.cor_de_fundo%type;
   v_sortim_estampa     	      e_basi_100.sortim_estampa%type;
   v_observacao1        	      e_basi_100.observacao1%type;
   v_observacao2        	      e_basi_100.observacao2%type;
   v_relacao_banho      	      e_basi_100.relacao_banho%type;
   v_tipo_tingimento    	      e_basi_100.tipo_tingimento%type;
   v_tonalidade         	      e_basi_100.tonalidade%type;
   v_seq_degrade        	      e_basi_100.seq_degrade%type;
   v_cor_integracao     	      e_basi_100.cor_integracao%type;
   v_cor_pantone        	      e_basi_100.cor_pantone%type;
   v_cor_numerica       	      e_basi_100.cor_numerica%type;
   v_cor_representativa 	      e_basi_100.cor_representativa%type;
   v_cod_cor_cliente    	      e_basi_100.cod_cor_cliente%type;
   v_desc_cor_cliente   	      e_basi_100.desc_cor_cliente%type;
   v_cgc_cli9           	      e_basi_100.cgc_cli9%type;
   v_cgc_cli4           	      e_basi_100.cgc_cli4%type;
   v_cgc_cli2           	      e_basi_100.cgc_cli2%type;
   v_ind_envia_infotint 	      e_basi_100.ind_envia_infotint%type;

   v_data_importacao    	      e_basi_100.data_importacao%type;
   v_tipo_operacao      	      e_basi_100.tipo_operacao%type;
   ws_usuario_systextil   	      e_basi_100.usuario_operacao%type;

   ws_usuario_rede                    varchar2(20);
   ws_maquina_rede                    varchar2(40);
   ws_aplicativo                      varchar2(20);
   ws_sid                             number(9);
   ws_empresa                         number(3);
   ws_locale_usuario                  varchar2(5);


begin


   inter_pr_dados_usuario (ws_usuario_rede, ws_maquina_rede, ws_aplicativo, ws_sid, ws_usuario_systextil, ws_empresa, ws_locale_usuario);

   v_cor_sortimento         := :new.cor_sortimento;
   v_tipo_cor           	:= :new.tipo_cor;
   v_descricao          	:= :new.descricao;
   v_serie_cor          	:= :new.serie_cor;
   v_numero_quadro      	:= :new.numero_quadro;
   v_cor_de_fundo       	:= :new.cor_de_fundo;
   v_sortim_estampa     	:= :new.sortim_estampa;
   v_observacao1        	:= :new.observacao1;
   v_observacao2        	:= :new.observacao2;
   v_relacao_banho      	:= :new.relacao_banho;
   v_tipo_tingimento    	:= :new.tipo_tingimento;
   v_tonalidade         	:= :new.tonalidade;
   v_seq_degrade        	:= :new.seq_degrade;
   v_cor_integracao     	:= :new.cor_integracao;
   v_cor_pantone        	:= :new.cor_pantone;
   v_cor_numerica       	:= :new.cor_numerica;
   v_cor_representativa 	:= :new.cor_representativa;
   v_cod_cor_cliente    	:= '';
   v_desc_cor_cliente   	:= '';
   v_cgc_cli9           	:= 0;
   v_cgc_cli4           	:= 0;
   v_cgc_cli2           	:= 0;
   v_ind_envia_infotint 	:= 'N';

   v_data_importacao    	:= sysdate();


if inserting or updating
then
   if inserting
   then v_tipo_operacao := 'I';
   else v_tipo_operacao := 'A';
   end if;

   if ((v_tipo_operacao  = 'I') or
       ( v_tipo_operacao = 'A'  and
      (:old.cor_sortimento        <> :new.cor_sortimento  or
       :old.tipo_cor              <> :new.tipo_cor        or
       :old.descricao             <> :new.descricao       or
       :old.serie_cor             <> :new.serie_cor       or
       :old.numero_quadro         <> :new.numero_quadro   or
       :old.cor_de_fundo          <> :new.cor_de_fundo    or
       :old.sortim_estampa        <> :new.sortim_estampa  or
       :old.observacao1           <> :new.observacao1     or
       :old.observacao2           <> :new.observacao2     or
       :old.relacao_banho         <> :new.relacao_banho   or
       :old.tipo_tingimento       <> :new.tipo_tingimento or
       :old.tonalidade            <> :new.tonalidade      or
       :old.seq_degrade           <> :new.seq_degrade     or
       :old.cor_integracao        <> :new.cor_integracao  or
       :old.cor_pantone           <> :new.cor_pantone     or
       :old.cor_numerica          <> :new.cor_numerica    or
       :old.cor_representativa    <> :new.cor_representativa or
       :old.complemento_descricao <> :new.complemento_descricao)))
   then

    begin

      insert into e_basi_100
      (cor_sortimento,         cor_integracao,
       tipo_cor,          	    cor_pantone,
       descricao,         	    cor_numerica,
       serie_cor,         	    cor_representativa,
       numero_quadro,     	    cod_cor_cliente,
       cor_de_fundo,      	    desc_cor_cliente,
       sortim_estampa,    	    cgc_cli9,
       observacao1,       	    cgc_cli4,
       observacao2,       	    cgc_cli2,
       relacao_banho,     	    ind_envia_infotint,
       tipo_tingimento,   	    data_importacao,
       tonalidade,        	    tipo_operacao,
       seq_degrade,       	    usuario_operacao,
       flag_integracao)
       VALUES
      (v_cor_sortimento,               v_cor_integracao,
       v_tipo_cor,           	    v_cor_pantone,
       v_descricao,          	    v_cor_numerica,
       v_serie_cor,          	    v_cor_representativa,
       v_numero_quadro,      	    v_cod_cor_cliente,
       v_cor_de_fundo,       	    v_desc_cor_cliente,
       v_sortim_estampa,     	    v_cgc_cli9,
       v_observacao1,        	    v_cgc_cli4,
       v_observacao2,        	    v_cgc_cli2,
       v_relacao_banho,      	    v_ind_envia_infotint,
       v_tipo_tingimento,   	    v_data_importacao,
       v_tonalidade,         	    v_tipo_operacao,
       v_seq_degrade,        	    ws_usuario_systextil,
       1);

    exception
    when OTHERS
       then raise_application_error (-20000, 'N�o atualizou a tabela e_basi_100' || Chr(10) || SQLERRM);
    end;

   if v_tipo_operacao = 'A'                      and
      :new.cor_sortimento <> :old.cor_sortimento or
      :new.tipo_cor       <> :old.tipo_cor
   then
      begin

         v_tipo_operacao := 'E';

         v_cor_sortimento       := :old.cor_sortimento;
         v_tipo_cor           	:= :old.tipo_cor;
       	 v_descricao          	:= :old.descricao;
      	 v_serie_cor          	:= :old.serie_cor;
  	     v_numero_quadro      	:= :old.numero_quadro;
         v_cor_de_fundo       	:= :old.cor_de_fundo;
         v_sortim_estampa     	:= :old.sortim_estampa;
         v_observacao1        	:= :old.observacao1;
         v_observacao2        	:= :old.observacao2;
         v_relacao_banho      	:= :old.relacao_banho;
         v_tipo_tingimento    	:= :old.tipo_tingimento;
         v_tonalidade         	:= :old.tonalidade;
         v_seq_degrade        	:= :old.seq_degrade;
         v_cor_integracao     	:= :old.cor_integracao;
         v_cor_pantone        	:= :old.cor_pantone;
         v_cor_numerica       	:= :old.cor_numerica;
         v_cor_representativa 	:= :old.cor_representativa;
         v_cod_cor_cliente    	:= :old.cod_cor_cliente;
         v_desc_cor_cliente   	:= :old.desc_cor_cliente;
         v_cgc_cli9           	:= :old.cgc_cli9;
         v_cgc_cli4           	:= :old.cgc_cli4;
         v_cgc_cli2           	:= :old.cgc_cli2;
         v_ind_envia_infotint 	:= :old.ind_envia_infotint;
         v_data_importacao    	:= sysdate();


         insert into e_basi_100
                    (cor_sortimento,          cor_integracao,
                     tipo_cor,           	    cor_pantone,
                     descricao,          	    cor_numerica,
                     serie_cor,          	    cor_representativa,
                     numero_quadro,      	    cod_cor_cliente,
                     cor_de_fundo,       	    desc_cor_cliente,
                     sortim_estampa,     	    cgc_cli9,
                     observacao1,        	    cgc_cli4,
                     observacao2,        	    cgc_cli2,
                     relacao_banho,      	    ind_envia_infotint,
                     tipo_tingimento,    	    data_importacao,
                     tonalidade,         	    tipo_operacao,
                     seq_degrade,        	    usuario_operacao,
                     flag_integracao)
   	           VALUES
                    (v_cor_sortimento,        v_cor_integracao,
                     v_tipo_cor,              v_cor_pantone,
                     v_descricao,             v_cor_numerica,
                     v_serie_cor,             v_cor_representativa,
                     v_numero_quadro,         v_cod_cor_cliente,
                     v_cor_de_fundo,          v_desc_cor_cliente,
                     v_sortim_estampa,        v_cgc_cli9,
                     v_observacao1,           v_cgc_cli4,
                     v_observacao2,           v_cgc_cli2,
                     v_relacao_banho,         v_ind_envia_infotint,
                     v_tipo_tingimento,       v_data_importacao,
                     v_tonalidade,            v_tipo_operacao,
                     v_seq_degrade,           ws_usuario_systextil,
                     1);
      exception
      when OTHERS
         then raise_application_error (-20000, 'N�o atualizou a tabela e_basi_100' || Chr(10) || SQLERRM);
      end;
   end if;
  end if;
end if;


if deleting
then

  begin

  v_tipo_operacao := 'E';

  v_cor_sortimento      := :old.cor_sortimento;
  v_tipo_cor           	:= :old.tipo_cor;
  v_descricao          	:= :old.descricao;
  v_serie_cor          	:= :old.serie_cor;
  v_numero_quadro      	:= :old.numero_quadro;
  v_cor_de_fundo       	:= :old.cor_de_fundo;
  v_sortim_estampa     	:= :old.sortim_estampa;
  v_observacao1        	:= :old.observacao1;
  v_observacao2        	:= :old.observacao2;
  v_relacao_banho      	:= :old.relacao_banho;
  v_tipo_tingimento    	:= :old.tipo_tingimento;
  v_tonalidade         	:= :old.tonalidade;
  v_seq_degrade        	:= :old.seq_degrade;
  v_cor_integracao     	:= :old.cor_integracao;
  v_cor_pantone        	:= :old.cor_pantone;
  v_cor_numerica       	:= :old.cor_numerica;
  v_cor_representativa 	:= :old.cor_representativa;
  v_cod_cor_cliente    	:= :old.cod_cor_cliente;
  v_desc_cor_cliente   	:= :old.desc_cor_cliente;
  v_cgc_cli9           	:= :old.cgc_cli9;
  v_cgc_cli4           	:= :old.cgc_cli4;
  v_cgc_cli2           	:= :old.cgc_cli2;
  v_ind_envia_infotint 	:= :old.ind_envia_infotint;

  v_data_importacao    	:= sysdate();


  insert into e_basi_100
  (cor_sortimento,          cor_integracao,
   tipo_cor,           	    cor_pantone,
   descricao,          	    cor_numerica,
   serie_cor,          	    cor_representativa,
   numero_quadro,      	    cod_cor_cliente,
   cor_de_fundo,       	    desc_cor_cliente,
   sortim_estampa,     	    cgc_cli9,
   observacao1,        	    cgc_cli4,
   observacao2,        	    cgc_cli2,
   relacao_banho,      	    ind_envia_infotint,
   tipo_tingimento,    	    data_importacao,
   tonalidade,         	    tipo_operacao,
   seq_degrade,        	    usuario_operacao,
   flag_integracao)
   VALUES
  (v_cor_sortimento,        v_cor_integracao,
   v_tipo_cor,              v_cor_pantone,
   v_descricao,             v_cor_numerica,
   v_serie_cor,             v_cor_representativa,
   v_numero_quadro,         v_cod_cor_cliente,
   v_cor_de_fundo,          v_desc_cor_cliente,
   v_sortim_estampa,        v_cgc_cli9,
   v_observacao1,           v_cgc_cli4,
   v_observacao2,           v_cgc_cli2,
   v_relacao_banho,         v_ind_envia_infotint,
   v_tipo_tingimento,       v_data_importacao,
   v_tonalidade,            v_tipo_operacao,
   v_seq_degrade,           ws_usuario_systextil,
   1);

    exception
    when OTHERS
      then raise_application_error (-20000, 'N�o atualizou a tabela e_basi_100' || Chr(10) || SQLERRM);
    end;
  end if;
end inter_tr_basi_100_integracao;
-- ALTER TRIGGER "INTER_TR_BASI_100_INTEGRACAO" DISABLE
 

/

exec inter_pr_recompile;

