
  CREATE OR REPLACE TRIGGER "INTER_TR_HIST_999" 
before insert on hist_999
  for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_nome_programa           hdoc_090.programa%type;
   v_nr_registro             number;
begin
   -- Busca a pr�xima seq��ncia
   begin
      select seq_hist_999.nextval
      into v_nr_registro
      from dual;
   end;

   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


   v_nome_programa := inter_fn_nome_programa(ws_sid);  
     

   :new.sequencia          := v_nr_registro;
   :new.data_ocorrencia    := sysdate;
   :new.hora_ocorrencia    := sysdate;
   :new.usuario            := ws_usuario_systextil;
   :new.usuario_rede       := ws_usuario_rede;
   :new.maquina_rede       := ws_maquina_rede;
   :new.aplicacao          := ws_aplicativo;
   :new.programa           := v_nome_programa;

end inter_tr_hist_999;

-- ALTER TRIGGER "INTER_TR_HIST_999" ENABLE
 

/

exec inter_pr_recompile;

