create or replace FUNCTION PROXIMO_DIA_UTIL (p_dia_atual IN DATE, p_qtd_dias IN NUMBER)
RETURN DATE
IS
  v_data_resultado DATE := p_dia_atual;
  v_contador NUMBER := 0;
BEGIN
  -- Busca os dias úteis financeiros a partir da data atual
  FOR x IN (
    SELECT DIA_UTIL_FINAN
    FROM basi_260
    WHERE DATA_CALENDARIO >= TRUNC(p_dia_atual) + 1
    ORDER BY DATA_CALENDARIO
  ) LOOP
    -- Verifica se há dias úteis após a data fornecida
    EXIT WHEN v_contador = p_qtd_dias;

    -- Adiciona um dia à data atual
    v_data_resultado := v_data_resultado + 1;

    -- Verifica se o dia resultante é útil (segunda a sexta-feira)
    IF 0 = x.DIA_UTIL_FINAN THEN
      v_contador := v_contador + 1;
    END IF;
  END LOOP;

  RETURN v_data_resultado;
END PROXIMO_DIA_UTIL;
