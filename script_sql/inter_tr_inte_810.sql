
  CREATE OR REPLACE TRIGGER "INTER_TR_INTE_810" 
before insert on inte_810
  for each row





declare
   v_nr_registro number;

begin
   select seq_inte_810.nextval into v_nr_registro from dual;

   :new.nr_registro := v_nr_registro;

end inter_tr_inte_810;

-- ALTER TRIGGER "INTER_TR_INTE_810" ENABLE
 

/

exec inter_pr_recompile;

