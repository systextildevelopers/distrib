alter table fatu_075 add     NR_MTV_ABATIMENTO  NUMBER(2);
alter table fatu_075 modify  NR_MTV_ABATIMENTO  default 0;

alter table fatu_075 add     FLAG_DEV_CONSIGNADO NUMBER(1);
alter table fatu_075 modify  FLAG_DEV_CONSIGNADO default 0;

alter table cont_010 add     CONSIDERA_REGIME_CAIXA VARCHAR2(1);
alter table cont_010 modify  CONSIDERA_REGIME_CAIXA default 'S';

alter table exec_010 add     FAMILIA_MAQ       NUMBER(4);
alter table exec_010 modify  FAMILIA_MAQ       default 0;

alter table exec_010 add     TEMPO_FUSOS       NUMBER(5,2);
alter table exec_010 modify  TEMPO_FUSOS       default 0;

alter table exec_010 add     FUSOS_UTILIZADOS  NUMBER(5);
alter table exec_010 modify  FUSOS_UTILIZADOS  default 0;

alter table obrf_015 add NATITEM_NAT_DCIP    NUMBER(9);
alter table obrf_015 add PERC_REDU_ICM       NUMBER(5,2);
alter table obrf_015 add PERC_REDU_SUB       NUMBER(5,2);
alter table obrf_015 add PERC_SUBSTITUICAO   NUMBER(5,2);
alter table obrf_015 add VALOR_AFRMM         NUMBER(15,6);
alter table obrf_015 modify VALOR_AFRMM default 0;
alter table obrf_015 add VALOR_ANTI_DUMP     NUMBER(15,6);
alter table obrf_015 modify VALOR_ANTI_DUMP default 0;
alter table obrf_015 add VALOR_DESP_NTRIB    NUMBER(15,6);
alter table obrf_015 modify VALOR_DESP_NTRIB default 0;
alter table obrf_015 add VALOR_DESP_ACESS    NUMBER(15,6);
alter table obrf_015 modify VALOR_DESP_ACESS default 0;

alter table supr_010 add PERM_MULT_GENERICO         NUMBER(1);
alter table supr_010 modify PERM_MULT_GENERICO      default 0;

alter table supr_010 add FORN_ISENTO_TITULO         NUMBER(1);
alter table supr_010 modify FORN_ISENTO_TITULO      default 0;

alter table fatu_503 add OPC_ANALISE_GRADE NUMBER(1) default 0;

alter table fatu_505 add DEP_CONFECCIONADOS_EMPRODUCAO NUMBER(3);
alter table fatu_505 add IND_SERIE_EXCLUS_INTE         VARCHAR2(1) default 'N';

create table FATU_014 /* confirmar */
(
  UF                  VARCHAR2(2) default ' ',
  COD_RECEITA         VARCHAR2(20) default ' ',
  COD_DOC_ORIGEM      VARCHAR2(10) default ' ',
  COD_EXTRA           VARCHAR2(10) default ' ',
  TIP_EXTRA           VARCHAR2(10) default ' ',
  OPCAO_CAMPO_EXTRA   NUMBER(1) default 0,
  DETALHE_DESTIN      VARCHAR2(1) default 'S',
  DETALHE_RECEITA     VARCHAR2(10) default ' ',
  INF_PERIODO         VARCHAR2(1) default 'S',
  COD_RECEITA_FCP     VARCHAR2(20) default ' ',
  DETALHE_RECEITA_FCP VARCHAR2(10) default ' ',
  PRODUTO_REC         VARCHAR2(20) default ' ',
  IND_VALOR_TOTAL     VARCHAR2(1) default ' ',
  VALOR_TOTAL         VARCHAR2(1) default ' '
);

alter table pcpb_020 add NRO_PECAS    NUMBER(5);
alter table pcpb_020 add COD_NUANCE   VARCHAR2(3);
alter table pcpb_020 add DATA_APONT_NUANCE  DATE;
alter table pcpb_020 add USUARIO_APONT_NUANCE  VARCHAR2(20);

alter table pcpc_045 add USUARIO_SYSTEXTIL      VARCHAR2(30);
alter table pcpc_045 modify USUARIO_SYSTEXTIL default ' ';
alter table pcpc_045 add COD_OCORRENCIA_ESTORNO NUMBER(3);
alter table pcpc_045 modify COD_OCORRENCIA_ESTORNO default 0;
alter table pcpc_045 add CODIGO_OCORRENCIA      NUMBER(3);
alter table pcpc_045 modify CODIGO_OCORRENCIA default 0;

alter table supr_090 add E_MAIL VARCHAR2(4000);
alter table supr_090 add ASSUNTO_EMAIL VARCHAR2(100);
alter table supr_090 add TEXTO_EMAIL VARCHAR2(4000);


alter table pedi_010 add CREDITO_TROCA NUMBER(10,2);
alter table pedi_010 modify CREDITO_TROCA default 0;
alter table pedi_010 add IND_BLOQ_QTDE_PACK_DIF VARCHAR2(1);
alter table pedi_010 modify IND_BLOQ_QTDE_PACK_DIF default 'N';


alter table basi_023 add FAIXA_ETARIA NUMBER(3);
alter table basi_023 modify FAIXA_ETARIA default 0;
alter table basi_023 add FAIXA_ETARIA_CUSTOS NUMBER(3);
alter table basi_023 modify FAIXA_ETARIA_CUSTOS default 0;

alter table loja_200 add CODIGO_CAIXA NUMBER(3);

alter table fatu_060 add VICMSDIF51 NUMBER(15,2);
alter table fatu_060 modify VICMSDIF51 default 0;

alter table loja_210 add FLAG_TROCA NUMBER(1);
alter table loja_210 modify FLAG_TROCA default 0;

alter table fatu_070 add NUMERO_CAIXA NUMBER(3);
alter table fatu_070 modify NUMERO_CAIXA default 0;

alter table pedi_080 add DECLARA_DCIP NUMBER(1);
alter table pedi_080 modify DECLARA_DCIP default 0;
alter table pedi_080 add TIPO_BENEFICIO_FISCAL NUMBER(1);
alter table pedi_080 modify TIPO_BENEFICIO_FISCAL default 0;
alter table pedi_080 add REGRA_NAT_EXCECAO  NUMBER(1);
alter table pedi_080 modify REGRA_NAT_EXCECAO default 0;
alter table pedi_080 add NATUREZA_INDUSTRIALIZACAO NUMBER(1);
alter table pedi_080 modify NATUREZA_INDUSTRIALIZACAO default 0;
alter table pedi_080 add REFERENTE_NF_CUPOM_FISCAL VARCHAR2(1);
alter table pedi_080 modify REFERENTE_NF_CUPOM_FISCAL default 'N';
alter table pedi_080 add CODIGO_CAIXA  NUMBER(3);
alter table pedi_080 add NATU_NF_REF_CUPOM  NUMBER(1);
alter table pedi_080 modify NATU_NF_REF_CUPOM default 0;

create table OBRF_PARTILHA
(
  ANO_BASE      NUMBER(5) default 0,
  PERC_PARTILHA NUMBER(5,2) default 0
);


alter table obrf_050 add VL_DEBITO_DIFAL NUMBER(15,2);
alter table obrf_050 modify VL_DEBITO_DIFAL default 0;
alter table obrf_050 add EVA_TRIB_TRANSFER_PRECO_CUSTO NUMBER(15,2);
alter table obrf_050 modify EVA_TRIB_TRANSFER_PRECO_CUSTO default 0;


-- Create table
create table PCPT_032
(
  ORDEM_TECELAGEM NUMBER(6) default 0 not null,
  OPERADOR        NUMBER(5) default 0 not null,
  CODIGO_TECELAO  NUMBER(5) default 0 not null
);
-- Add comments to the columns
comment on column PCPT_032.ORDEM_TECELAGEM
  is 'Ordem de tecelagem.';
comment on column PCPT_032.OPERADOR
  is 'Operador da tecelagem.';
comment on column PCPT_032.CODIGO_TECELAO
  is 'Codigo do tecelao da ordem de tecelagem.';
-- Create/Recreate primary, unique and foreign key constraints
alter table PCPT_032
  add constraint PK_PCPT_032 primary key (ORDEM_TECELAGEM, OPERADOR)
  using index;

alter table sped_ctb_i050 add NATUR_SUBCONTA  VARCHAR2(2);
alter table sped_ctb_i050 modify NATUR_SUBCONTA  default ' ';

-- Create table
create table CONT_605 /* Confirmar */
(
  COD_EMPRESA        NUMBER(3) default 0 not null,
  FILIAL_LANCTO      NUMBER(3) default 0,
  EXERCICIO          NUMBER(4) default 0 not null,
  NUMERO_LANC        NUMBER(9) default 0 not null,
  ORIGEM             NUMBER(2) default 0,
  CONTA_REDUZIDA     NUMBER(5) default 0,
  CONTA_CONTABIL     VARCHAR2(20) default 0,
  CENTRO_CUSTO       NUMBER(6) default 0,
  DEBITO_CREDITO     VARCHAR2(1) default 0,
  CNPJ9_PARTICIPANTE NUMBER(9) default 0,
  CNPJ4_PARTICIPANTE NUMBER(4) default 0,
  CNPJ2_PARTICIPANTE NUMBER(2) default 0,
  DATA_LANCTO        DATE,
  VALOR_LANCTO       NUMBER(13,2) default 0.00,
  COD_PATR_ITEM      VARCHAR2(10) default ' ',
  QTD                NUMBER(15,2) default 0.0,
  IDENT_ITEM         VARCHAR2(30) default ' ',
  DESCR_ITEM         VARCHAR2(50) default ' ',
  DATA_RECT_INI      DATE,
  SLD_ITEM_INI       NUMBER(15,2) default 0.0,
  IND_SLD_ITEM_INI   VARCHAR2(1) default ' ',
  REAL_ITEM          NUMBER(15,2) default 0.0,
  IND_SLD_REAL_ITEM  VARCHAR2(1) default ' ',
  SLD_ITEM_FIN       NUMBER(15,2) default 0.0,
  IND_SLD_ITEM_FIN   VARCHAR2(1) default ' ',
  SLD_SCNT_INI       NUMBER(15,2) default 0.0,
  IND_SLD_SCNT_INI   VARCHAR2(1) default ' ',
  DEB_SCNT_VALOR     NUMBER(15,2) default 0.0,
  CRED_SCNT_VALOR    NUMBER(15,2) default 0.0,
  SLD_SCNT_FIN       NUMBER(15,2) default 0.0,
  IND_SLD_SCNT_FIN   VARCHAR2(1) default ' ',
  IND_VLR_LCTO       VARCHAR2(1) default ' ',
  IND_ADOC_INI       VARCHAR2(1) default ' '
);
-- Add comments to the columns
comment on column CONT_605.COD_EMPRESA
  is 'Código da empresa matriz do lançamento';
comment on column CONT_605.FILIAL_LANCTO
  is 'Código da empresa do lançamento';
comment on column CONT_605.EXERCICIO
  is 'Código do execicio contábil';
comment on column CONT_605.NUMERO_LANC
  is 'Número do lançamento contábil';
comment on column CONT_605.ORIGEM
  is 'Origem do lançamento contábil';
comment on column CONT_605.CONTA_REDUZIDA
  is 'Número do lançamento contábil';
comment on column CONT_605.CONTA_CONTABIL
  is 'Número do lançamento contábil';
comment on column CONT_605.CENTRO_CUSTO
  is 'Centro do custo do lançamento contábil';
comment on column CONT_605.DEBITO_CREDITO
  is 'Debito ou crédito do lançamento contábil';
comment on column CONT_605.CNPJ9_PARTICIPANTE
  is 'Cnpj do do lançamento contábil';
comment on column CONT_605.CNPJ4_PARTICIPANTE
  is 'Cnpj do do lançamento contábil';
comment on column CONT_605.CNPJ2_PARTICIPANTE
  is 'Cnpj do do lançamento contábil';
comment on column CONT_605.VALOR_LANCTO
  is 'valor lançamento contabil';
comment on column CONT_605.COD_PATR_ITEM
  is 'código definido pela pessoa jurídica para identificar o item (ativo/passivo). observação: no caso de avj reflexo, será a identificação do item na pessoa jurídica investida';
comment on column CONT_605.QTD
  is 'quantidade inicial do item, na mesma precisão utilizada pela metodologia contábil. quando se tratarem de ativos/passivos de mesmas características/qualidades, como ativos biológicos e contratos de operações em bolsa de valores, é possível agruparem todos pelas características comuns';
comment on column CONT_605.IDENT_ITEM
  is 'conjunto de caracteres utilizado para individualizar o bem, conforme sua natureza. exemplos: placa para veículos automotores, matrícula do cartório para imóveis, marca/modelo para equipamentos, raça/idade/sexo para animais vivos, série do derivativo em bolsa';
comment on column CONT_605.DESCR_ITEM
  is 'descrição resumida do item. observação: no caso de avj reflexo, será a descrição do item pertencente ao patrimônio da investida.';
comment on column CONT_605.DATA_RECT_INI
  is 'data do reconhecimento contábil do item (ddmmaaaa). é a data em que ocorreu o registro inicial no sistema contábil, mesmo que anterior ao exercício corrente. em situações justificáveis pela complexidade na identificação da informação, poderá ser informado um registro aproximado constando 31/12 do ano. exemplo: 31122004 (31/12/2004)';
comment on column CONT_605.SLD_ITEM_INI
  is 'saldo inicial da conta contábil que registra o item (ativo/passivo) ao qual a subconta esteja vinculada. o saldo a ser informado corresponde ao saldo antes de ocorrer o evento originário do lançamento na subconta demonstrado no livro razão auxiliar. observação: no caso de avj reflexo, será o saldo inicial da subconta avj na investida';
comment on column CONT_605.IND_SLD_ITEM_INI
  is 'indicador do saldo inicial da conta contábil: d - devedor c - credor';
comment on column CONT_605.REAL_ITEM
  is 'parcela da realização do item registrado no patrimônio da investida/emitente ao qual a subconta esteja vinculada.  exemplo: depreciação, alienação, integralizar capital de outra pessoa jurídica';
comment on column CONT_605.IND_SLD_REAL_ITEM
  is 'indicador da realização do item';
comment on column CONT_605.SLD_ITEM_FIN
  is 'saldo final da conta contábil que registra a conta (ativo/passivo) ao qual a subconta esteja vinculada. corresponde ao saldo após ocorrer o evento originário do lançamento na subconta demonstrado no livro razão auxiliar, como por exemplo a baixa do bem, quando seu valor será zerado. observação: no caso de avj reflexo, será o saldo inicial da subconta avj na investida';
comment on column CONT_605.IND_SLD_ITEM_FIN
  is 'indicador do saldo final da conta contábil: d - devedor c - credor';
comment on column CONT_605.SLD_SCNT_INI
  is 'saldo inicial representativo do item na subconta antes do lançamento a ser demonstrado neste registro. este valor pode não coincidir com o saldo apresentado na ecd para este instante, uma vez que demonstra apenas a parcela atribuível a este item';
comment on column CONT_605.IND_SLD_SCNT_INI
  is 'indicador do saldo inicial da subconta: d - devedor c - credor';
comment on column CONT_605.DEB_SCNT_VALOR
  is 'valor registrado a débito na subconta correspondente apenas a participação do item na composição de um lançamento resumido na ecd, que pode englobar vários eventos sobre itens distintos.';
comment on column CONT_605.CRED_SCNT_VALOR
  is 'valor registrado a crédito na subconta correspondente apenas a participação do item na composição de um lançamento resumido na ecd, que pode englobar vários eventos sobre itens distintos.';
comment on column CONT_605.SLD_SCNT_FIN
  is 'saldo final representativo deste item na subconta após o lançamento demonstrado neste registro. este valor pode não coincidir com o saldo apresentado na ecd para este instante, uma vez que demonstra apenas a parcela atribuível a este item';
comment on column CONT_605.IND_SLD_SCNT_FIN
  is 'indicador do saldo final da subconta: d - devedor c - credor';
comment on column CONT_605.IND_VLR_LCTO
  is 'indicador do lançamento: d - débito c - crédito';
comment on column CONT_605.IND_ADOC_INI
  is 'indicador de registro de relativo a adoção inicial. 1 - sim 2 - não';


alter table sped_c100 add TIPO_CTE  NUMBER(1);
alter table sped_c100 modify TIPO_CTE default 0;

-- Create table
create table I_OBRF_016 /* confirmar */
(
  ID_REGISTRO      NUMBER(9) not null,
  FLAG_IMPORTACAO  NUMBER(1) default 0,
  TIPO_ATUALIZACAO VARCHAR2(1),
  DATA_EXPORTACAO  DATE,
  DATA_IMPORTACAO  DATE,
  NUM_CONHECIMENTO NUMBER(9) default 0 not null,
  SER_CONHECIMENTO VARCHAR2(3) default '' not null,
  TRANSPORTADORA9  NUMBER(9) default 0 not null,
  TRANSPORTADORA4  NUMBER(4) default 0 not null,
  TRANSPORTADORA2  NUMBER(2) default 0 not null,
  NUMERO_NOTA      NUMBER(9) default 0 not null,
  SERIE_NOTA       VARCHAR2(3) default '' not null,
  FORNECEDOR9      NUMBER(9) default 0 not null,
  FORNECEDOR4      NUMBER(4) default 0 not null,
  FORNECEDOR2      NUMBER(2) default 0 not null,
  COD_TRANSACAO    NUMBER(3) default 0,
  COD_DEPOSITO     NUMBER(3) default 0
);


alter table cont_500 add FORM_APUR VARCHAR2(1);
alter table cont_500 modify FORM_APUR default 'A';

alter table cpag_005 add TEVE_REJEICAO VARCHAR2(1);
alter table cpag_005 modify TEVE_REJEICAO default 'N';


-- Create table
create table PEDI_013
(
  CGC9                    NUMBER(9) not null,
  CGC4                    NUMBER(4) not null,
  CGC2                    NUMBER(2) not null,
  TOTAL_LIMITE            NUMBER(14,2) default 0.0,
  SALDO_LIMITE            NUMBER(14,2) default 0.0,
  DIAS_ATRASO             NUMBER(5) default 0,
  STATUS_CARTAO           VARCHAR2(1) default '',
  STATUS_CLIENTE          VARCHAR2(1) default '',
  DATA_RETORNO            DATE,
  DATA_REJEICAO           DATE,
  MOTIVO_REJEICAO         VARCHAR2(100) default '',
  ARQUIVO_RETORNO         VARCHAR2(25),
  COD_REJEICAO            VARCHAR2(3),
  OBSERVACAO              VARCHAR2(255),
  FLAG_REMESSA            NUMBER(1) default 0,
  SOLICITACAO_EMERGENCIAL NUMBER(1) default 0,
  LIMITE_SUGERIDO         NUMBER(14,2) default 0.0,
  SALDO_LIMITE_INTERNO    NUMBER(14,2) default 0.00,
  NUMERO_CARTAO           VARCHAR2(14) default '',
  TIPO_CLIENTE            VARCHAR2(5) default 'C',
  SLD_ABERTO_CARTAO       NUMBER(15,2) default 0.00
);
-- Add comments to the columns
comment on column PEDI_013.TOTAL_LIMITE
  is 'Total do limite de crÃ©dito do cliente na suppliercard';
comment on column PEDI_013.SALDO_LIMITE
  is 'Saldo do limite de crÃ©dito do cliente na suppliercard';
comment on column PEDI_013.DIAS_ATRASO
  is 'Dias atraso do cliente na suppliercard';
comment on column PEDI_013.STATUS_CARTAO
  is '1-Atendida, 2-Encaminhada, 3-Rejeitada';
comment on column PEDI_013.STATUS_CLIENTE
  is '0-Ativo, 1-Bloqueado por CrÃ©dito, 2-Bloqueado por Atraso, 3-Cancelado';
comment on column PEDI_013.DATA_RETORNO
  is 'Data de retorno da suppliercard';
comment on column PEDI_013.DATA_REJEICAO
  is 'Data da rejeiÃ§Ã£o pela suppliercard';
comment on column PEDI_013.MOTIVO_REJEICAO
  is 'Motivo da rejeiÃ§Ã£o pela suppliercard';

-- Create table
create table PEDI_392
(
  CODIGO_EMPRESA NUMBER(3) default 0 not null,
  ESTADO         VARCHAR2(2) default '' not null,
  ORIG_PROD      NUMBER(1) default 0 not null,
  TIPO_DESCONTO  NUMBER(3) default 0,
  PERC_DESCONTO  NUMBER(6,2) default 0.00
);
-- Add comments to the columns
comment on column PEDI_392.CODIGO_EMPRESA
  is 'Tipo do desconto';
comment on column PEDI_392.ESTADO
  is 'Estado destino para excecao';
comment on column PEDI_392.ORIG_PROD
  is 'ORIGEM DO PRODUTO 0 - proprio, 1 - importado, 2 - comprado';
comment on column PEDI_392.PERC_DESCONTO
  is 'PERCENTUAL DE DESCONTO';
-- Create/Recreate primary, unique and foreign key constraints
alter table PEDI_392
  add constraint PK_PEDI_392 primary key (CODIGO_EMPRESA, ESTADO, ORIG_PROD)
  using index;


alter table BASI_040 add QTDE_PECAS NUMBER(3,0);
alter table basi_040 modify QTDE_PECAS DEFAULT 0;

alter table BASI_050 add QTDE_PECAS NUMBER(3,0);
alter table basi_050 modify QTDE_PECAS DEFAULT 0;
