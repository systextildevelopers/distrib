
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_325_LOG" 
  before insert or delete or
  update of ordem_producao, sequencia_pack, numero_volume, nivel,
	grupo, sub, item, periodo_ordem,
	ordem_confeccao, qtde_pecas_real, qtde_pecas_sug, numero_kit,
	nivel_estoque, grupo_estoque, sub_estoque, item_estoque,
	qtde_quilos_estoque, artigo_produto, seq_item_pedido, qtde_quilos_real,
	executa_trigger, deposito_entrada_item, qtde_pecas_atend, deposito_saida_item
  on pcpc_325
  for each row
declare
  -- local variables here
  v_numero_volume           pcpc_325.numero_volume%type;
  v_nivel		                pcpc_325.nivel%type;
  v_grupo	                  pcpc_325.grupo%type;
  v_sub		   	              pcpc_325.sub%type;
  v_item    		            pcpc_325.item%type;
  v_periodo_ordem	          pcpc_325.periodo_ordem%type;
  v_ordem_confeccao	        pcpc_325.ordem_confeccao%type;
  v_numero_kit		          pcpc_325.numero_kit%type;
  v_nivel_estoque	          pcpc_325.nivel_estoque%type;
  v_grupo_estoque	          pcpc_325.grupo_estoque%type;
  v_sub_estoque		          pcpc_325.sub_estoque%type;
  v_item_estoque	          pcpc_325.item_estoque%type;
  v_qtde_quilos_estoque	    pcpc_325.qtde_quilos_estoque%type;
  v_artigo_produto	        pcpc_325.artigo_produto%type;
  v_seq_item_pedido	        pcpc_325.seq_item_pedido%type;
  v_qtde_quilos_real_old    pcpc_325.qtde_quilos_real%type;
  v_qtde_pecas_real_old	    pcpc_325.qtde_pecas_real%type;
  v_qtde_pecas_sug_old	    pcpc_325.qtde_pecas_sug%type;
  v_qtde_pecas_sug_new	    pcpc_325.qtde_pecas_sug%type;
  v_qtde_pecas_real_new     pcpc_325.qtde_pecas_real%type;
  v_qtde_quilos_real_new    pcpc_325.qtde_quilos_real%type;
  v_nome_programa           hdoc_090.programa%type;
  v_deposito_entrada_item   pcpc_325.deposito_entrada_item%type;
  v_sid                     number;

  v_operacao              varchar(1);
  v_data_operacao         date;
  v_usuario_rede          varchar(20);
  v_maquina_rede          varchar(40);
  v_aplicativo            varchar(20);
  v_executa_trigger       number(1);
  ws_empresa              number(3);
  ws_locale_usuario       varchar(40);
  v_nome_usuario          varchar(250);
begin
   -- Dados do usu￿rio logado
   inter_pr_dados_usuario (v_usuario_rede,  v_maquina_rede,   v_aplicativo,     v_sid,
                           v_nome_usuario,  ws_empresa,       ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(v_sid);      

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      -- grava a data/hora da insercao do registro (log)
      v_data_operacao := sysdate();

      --alimenta as variaveis new caso seja insert ou update
      if inserting or updating
      then
         if inserting
         then v_operacao := 'i';
         else v_operacao := 'u';
         end if;

         v_numero_volume	  := :new.numero_volume;
         v_nivel		  := :new.nivel;
         v_grupo		  := :new.grupo;
         v_sub			  := :new.sub;
         v_item		  	  := :new.item;
         v_periodo_ordem	  := :new.periodo_ordem;
         v_ordem_confeccao        := :new.ordem_confeccao;
         v_numero_kit		          := :new.numero_kit;
         v_nivel_estoque	        := :new.nivel_estoque;
         v_grupo_estoque	        := :new.grupo_estoque;
         v_sub_estoque	       	  := :new.sub_estoque;
         v_item_estoque	  	      := :new.item_estoque;
         v_qtde_quilos_estoque	  := :new.qtde_quilos_estoque;
         v_artigo_produto	        := :new.artigo_produto;
         v_seq_item_pedido	      := :new.seq_item_pedido;
         v_qtde_pecas_sug_new	    := :new.qtde_pecas_sug;
         v_qtde_pecas_real_new	  := :new.qtde_pecas_real;
         v_qtde_quilos_real_new	  := :new.qtde_quilos_real;
         v_deposito_entrada_item  := :new.deposito_entrada_item;

         if updating
         then
           v_qtde_pecas_sug_old	    := :old.qtde_pecas_sug;
           v_qtde_pecas_real_old    := :old.qtde_pecas_real;
           v_qtde_quilos_real_old   := :old.qtde_quilos_real;
           v_deposito_entrada_item  := :old.deposito_entrada_item;
         end if;

      end if; --fim do if inserting or updating

      --alimenta as variaveis old caso seja insert ou update
      if deleting
      then
           v_operacao      := 'd';

         v_numero_volume    	    := :old.numero_volume;
         v_nivel       	  	      := :old.nivel;
         v_grupo		              := :old.grupo;
         v_sub			              := :old.sub;
         v_item		  	            := :old.item;
         v_periodo_ordem          := :old.periodo_ordem;
         v_ordem_confeccao 	      := :old.ordem_confeccao;
         v_numero_kit		          := :old.numero_kit;
         v_nivel_estoque	        := :old.nivel_estoque;
         v_grupo_estoque          := :old.grupo_estoque;
         v_sub_estoque		        := :old.sub_estoque;
         v_item_estoque	          := :old.item_estoque;
         v_qtde_quilos_estoque    := :old.qtde_quilos_estoque;
         v_artigo_produto         := :old.artigo_produto;
         v_seq_item_pedido	      := :old.seq_item_pedido;
         v_qtde_pecas_sug_old	    := :old.qtde_pecas_sug;
         v_qtde_pecas_real_old    := :old.qtde_pecas_real;
         v_qtde_quilos_real_old   := :old.qtde_quilos_real;
         v_deposito_entrada_item  := :old.deposito_entrada_item;
      end if; --fim do if inserting or updating

      --insere na pcpc_325_log o registro.

      insert into pcpc_325_log
        (numero_volume,		            nivel,
         grupo,			                  sub,
         item,			                  periodo_ordem,
         ordem_confeccao,	            qtde_pecas_real_old,
         qtde_pecas_real_new,	        qtde_pecas_sug_old,
         qtde_pecas_sug_new,	        numero_kit,
         nivel_estoque,	 	            grupo_estoque,
         sub_estoque,		              item_estoque,
         qtde_quilos_estoque,	        artigo_produto,
         seq_item_pedido,	            qtde_quilos_real_old,
         qtde_quilos_real_new,        operacao,
         data_operacao,		            usuario_rede,
         maquina_rede,		            aplicativo,
         nome_programa,               usuario_sistema,
         deposito_entrada_item)
         values (
         v_numero_volume,	             v_nivel,
         v_grupo,		                   v_sub,
         v_item,		                   v_periodo_ordem,
         v_ordem_confeccao,	           v_qtde_pecas_real_old,
         v_qtde_pecas_real_new,	       v_qtde_pecas_sug_old,
         v_qtde_pecas_sug_new,	       v_numero_kit,
         v_nivel_estoque,	             v_grupo_estoque,
         v_sub_estoque,		             v_item_estoque,
         v_qtde_quilos_estoque,	       v_artigo_produto,
         v_seq_item_pedido,	           v_qtde_quilos_real_old,
         v_qtde_quilos_real_new,       v_operacao,
         v_data_operacao,	             v_usuario_rede,
         v_maquina_rede,	             v_aplicativo,
         v_nome_programa,              v_nome_usuario,
         v_deposito_entrada_item);
   end if;
end inter_tr_pcpc_325_log;

-- ALTER TRIGGER "INTER_TR_PCPC_325_LOG" ENABLE
 

/

exec inter_pr_recompile;

