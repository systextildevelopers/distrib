create table CPAG_200_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(20) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(20) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  tipo_adiantam_OLD     NUMBER(1) default 0 ,  
  tipo_adiantam_NEW     NUMBER(1) default 0 , 
  numero_adiantam_OLD   NUMBER(6) default 0 ,  
  numero_adiantam_NEW   NUMBER(6) default 0 , 
  cgc_9_OLD             NUMBER(9) default 0,  
  cgc_9_NEW             NUMBER(9) default 0, 
  cgc_4_OLD             NUMBER(4) default 0,  
  cgc_4_NEW             NUMBER(4) default 0, 
  cgc_2_OLD             NUMBER(2) default 0,  
  cgc_2_NEW             NUMBER(2) default 0, 
  data_digitacao_OLD    DATE,  
  data_digitacao_NEW    DATE, 
  valor_adiant_OLD      NUMBER(13,2) default 0.0,  
  valor_adiant_NEW      NUMBER(13,2) default 0.0, 
  valor_saldo_OLD       NUMBER(13,2) default 0.0,  
  valor_saldo_NEW       NUMBER(13,2) default 0.0, 
  situacao_OLD          NUMBER(1) default 0,  
  situacao_NEW          NUMBER(1) default 0, 
  data_remessa_OLD      DATE,  
  data_remessa_NEW      DATE, 
  cod_cancelamento_OLD  NUMBER(3) default 0,  
  cod_cancelamento_NEW  NUMBER(3) default 0, 
  data_cancelamento_OLD DATE,  
  data_cancelamento_NEW DATE, 
  codigo_empresa_OLD    NUMBER(3) default 0,  
  codigo_empresa_NEW    NUMBER(3) default 0, 
  data_prev_pgto_OLD    DATE,  
  data_prev_pgto_NEW    DATE, 
  origem_adiantam_OLD   VARCHAR2(60) default '',  
  origem_adiantam_NEW   VARCHAR2(60) default '', 
  nr_adiantam_geral_OLD NUMBER(6) default 0,  
  nr_adiantam_geral_NEW NUMBER(6) default 0, 
  pedido_compra_OLD     NUMBER(6) default 0,  
  pedido_compra_NEW     NUMBER(6) default 0, 
  percentual_OLD        NUMBER(5,2) default 0,  
  percentual_NEW        NUMBER(5,2) default 0, 
  cod_moeda_OLD         NUMBER(2) default 0,  
  cod_moeda_NEW         NUMBER(2) default 0, 
  cod_portador_OLD      NUMBER(3) default 0,  
  cod_portador_NEW      NUMBER(3) default 0, 
  data_moeda_OLD        DATE,  
  data_moeda_NEW        DATE, 
  num_importacao_OLD    VARCHAR2(20) default ' ',  
  num_importacao_NEW    VARCHAR2(20) default ' ', 
  valor_liquidacao_OLD  NUMBER(13,2) default 0.00,  
  valor_liquidacao_NEW  NUMBER(13,2) default 0.00, 
  data_liquidacao_OLD   DATE,  
  data_liquidacao_NEW   DATE, 
  banco_liquidacao_OLD  NUMBER(3) default 0,  
  banco_liquidacao_NEW  NUMBER(3) default 0, 
  conta_liquidacao_OLD  NUMBER(9) default 0,  
  conta_liquidacao_NEW  NUMBER(9) default 0, 
  cotacao_moeda_OLD     NUMBER(16,6) default 0.0, 
  cotacao_moeda_NEW     NUMBER(16,6) default 0.0 
);
