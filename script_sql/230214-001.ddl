create table PCPB_666
(
  SEQUENCIA_NUM        NUMBER(9) default 0 not null,
  ORDEM_BENEFICIAMENTO NUMBER(9) default 0 not null,
  LARGURA_MINIMA       NUMBER(6,3) default 0.00,
  LARGURA_MEDIA        NUMBER(6,3) default 0.00,
  COMPRIMENTO          NUMBER(7,3) default 0.00,
  DATA_ATUALIZACAO     DATE,
  STATUS_IMPRESSAO     NUMBER(1) default 0,
  SITUACAO             NUMBER(2) default 0,
  MOTIVO_APROVACAO     NUMBER(2) default 0,
  OBS_MOTIVO_APROVACAO VARCHAR2(120) default 0,
  QUALIDADE_ROLO       NUMBER(1) default 0,
  CODIGO_MOTIVO        NUMBER(9) default 0,
  ESTAGIO              NUMBER(3) default 0
);

comment on column PCPB_666.SEQUENCIA_NUM
  is 'Sequencia numerica do tubete';
comment on column PCPB_666.ORDEM_BENEFICIAMENTO
  is 'Ordem de beneficiamento';
comment on column PCPB_666.LARGURA_MINIMA
  is 'Largura do tubete';
comment on column PCPB_666.LARGURA_MEDIA
  is 'Largura do tubete';
comment on column PCPB_666.COMPRIMENTO
  is 'Comprimento do tubete';
comment on column PCPB_666.DATA_ATUALIZACAO
  is 'Data e hora de atualizac?o';
comment on column PCPB_666.SITUACAO
  is 'Situacao do Tubete';
comment on column PCPB_666.MOTIVO_APROVACAO
  is 'Codigo do motivo de aprovac?o';
comment on column PCPB_666.OBS_MOTIVO_APROVACAO
  is 'Observac?o do motivo de aprovac?o';
  
alter table PCPB_666
  add constraint PK_PCPB_666 primary key (ORDEM_BENEFICIAMENTO, SEQUENCIA_NUM)
  ;
