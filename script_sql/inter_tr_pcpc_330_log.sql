create or replace trigger "INTER_TR_PCPC_330_LOG" 
  before insert or
         delete or
         update of forn4_entr, forn2_entr, nota_entr, serie_entr,
		seq_entr, nome_prog_050, data_cardex, transacao_cardex,
		usuario_cardex, valor_movto_cardex, valor_contabil_cardex, tabela_origem_cardex,
		seq_operacao, pedido_compra, seq_compra, executa_trigger,
		cod_caixa_rfid, periodo_producao, ordem_producao, ordem_confeccao,
		sequencia, estagio, nr_volume, pedido_venda,
		seq_item_pedido, nivel, grupo, subgrupo,
		item, nota_inclusao, serie_nota, data_inclusao,
		usuario_estq, usuario_exped, flag_controle, estoque_tag,
		lote, deposito, transacao_ent, data_entrada,
		pre_romaneio, emp_saida, seq_saida, flag_inventario,
		forn9_entr, endereco
         on pcpc_330
  for each row

declare
  v_periodo_producao          pcpc_330.periodo_producao     %type;
  v_ordem_producao              pcpc_330.ordem_producao      %type;
  v_ordem_confeccao            pcpc_330.ordem_confeccao     %type;
  v_sequencia                  pcpc_330.sequencia           %type;
  v_estagio_old               pcpc_330.estagio             %type;
  v_estagio                    pcpc_330.estagio             %type;
  v_nr_volume_old              pcpc_330.nr_volume           %type;
  v_nr_volume                  pcpc_330.nr_volume           %type;
  v_nivel                      pcpc_330.nivel               %type;
  v_grupo                      pcpc_330.grupo               %type;
  v_subgrupo                  pcpc_330.subgrupo             %type;
  v_item                      pcpc_330.item                 %type;
  v_usuario_estq              pcpc_330.usuario_estq         %type;
  v_usuario_exped              pcpc_330.usuario_exped       %type;
  v_flag_controle              pcpc_330.flag_controle       %type;
  v_pedido_venda_old          pcpc_330.pedido_venda         %type;
  v_seq_item_pedido_old        pcpc_330.seq_item_pedido      %type;
  v_nota_inclusao_old          pcpc_330.nota_inclusao       %type;
  v_serie_nota_old            pcpc_330.serie_nota           %type;
  v_data_inclusao_old          pcpc_330.data_inclusao       %type;
  v_data_inclusao_new         pcpc_330.data_inclusao        %type;
  v_pedido_venda_new          pcpc_330.pedido_venda         %type;
  v_seq_item_pedido_new        pcpc_330.seq_item_pedido     %type;
  v_nota_inclusao_new          pcpc_330.nota_inclusao       %type;
  v_serie_nota_new            pcpc_330.serie_nota           %type;
  v_estoque_tag_old           pcpc_330.estoque_tag         %type;
  v_estoque_tag_new           pcpc_330.estoque_tag         %type;
  v_lote_old                  pcpc_330.lote                %type;
  v_lote_new                  pcpc_330.lote                %type;
  v_deposito_old              pcpc_330.deposito            %type;
  v_deposito_new              pcpc_330.deposito            %type;
  v_transacao_ent_old         pcpc_330.transacao_ent       %type;
  v_transacao_ent_new         pcpc_330.transacao_ent       %type;
  v_data_entrada_old          pcpc_330.data_entrada        %type;
  v_data_entrada_new          pcpc_330.data_entrada        %type;
  v_pre_romaneio_old          pcpc_330.pre_romaneio        %type;
  v_pre_romaneio_new          pcpc_330.pre_romaneio        %type;
  v_emp_saida_old             pcpc_330.emp_saida           %type;
  v_emp_saida_new             pcpc_330.emp_saida           %type;
  v_seq_saida_old             pcpc_330.seq_saida           %type;
  v_seq_saida_new             pcpc_330.seq_saida           %type;
  v_flag_inventario_old       pcpc_330.flag_inventario     %type;
  v_flag_inventario_new       pcpc_330.flag_inventario     %type;
  v_forn9_entr_old            pcpc_330.forn9_entr          %type;
  v_forn9_entr_new            pcpc_330.forn9_entr          %type;
  v_forn4_entr_old            pcpc_330.forn4_entr          %type;
  v_forn4_entr_new            pcpc_330.forn4_entr          %type;
  v_forn2_entr_old            pcpc_330.forn2_entr          %type;
  v_forn2_entr_new            pcpc_330.forn2_entr          %type;
  v_nota_entr_old             pcpc_330.nota_entr           %type;
  v_nota_entr_new             pcpc_330.nota_entr           %type;
  v_serie_entr_old            pcpc_330.serie_entr          %type;
  v_serie_entr_new            pcpc_330.serie_entr          %type;
  v_seq_entr_old              pcpc_330.seq_entr            %type;
  v_seq_entr_new              pcpc_330.seq_entr            %type;
  v_nome_prog_050_old         pcpc_330.nome_prog_050       %type;
  v_nome_prog_050_new         pcpc_330.nome_prog_050       %type;
  v_pedido_compra_old         pcpc_330.pedido_compra       %type;
  v_pedido_compra_new         pcpc_330.pedido_compra       %type;
  v_seq_compra_old            pcpc_330.seq_compra          %type;
  v_seq_compra_new            pcpc_330.seq_compra          %type;
  v_endereco_old              pcpc_330.endereco            %type;
  v_endereco_new              pcpc_330.endereco            %type;
  v_seq_operacao_old          pcpc_330.seq_operacao        %type;
  v_seq_operacao_new          pcpc_330.seq_operacao        %type;
  v_executa_trigger_old       pcpc_330.executa_trigger     %type;
  v_executa_trigger_new       pcpc_330.executa_trigger     %type;

  v_operacao                  varchar(1);
  v_data_operacao             date;
  v_usuario_rede              varchar(20);
  v_maquina_rede              varchar(40);
  v_aplicativo                varchar(20);

  v_executa_trigger           number;

  v_sid                       number;
  v_usuario_systextil         varchar(250);
  v_empresa                   number;
  v_locale_usuario            varchar(20);
  v_nome_programa             varchar(20);
  
  v_ult_usuario               varchar(250);

begin
    -- grava a data/hora da inser??o do registro (log)
    v_data_operacao := sysdate();

    -- Dados do usu?rio logado
    inter_pr_dados_usuario (v_usuario_rede,         v_maquina_rede,    v_aplicativo,     v_sid,
                            v_usuario_systextil,    v_empresa,         v_locale_usuario);
                            
    if v_usuario_systextil is not null then 
       v_ult_usuario:= v_usuario_systextil;
    else
      v_ult_usuario:= v_usuario_rede; 
    end if;                            
                            
    --alimenta as vari?veis new caso seja insert ou update
    if inserting or updating
    then
       if inserting
       then
          v_operacao := 'i';
       else
          v_operacao := 'u';
       end if;

       v_periodo_producao    := :new.periodo_producao;
       v_ordem_producao       := :new.ordem_producao;
       v_ordem_confeccao      := :new.ordem_confeccao;
       v_sequencia             := :new.sequencia;
       v_estagio               := :new.estagio;
       v_nr_volume            := :new.nr_volume;
       v_nivel                 := :new.nivel;
       v_grupo                := :new.grupo;
       v_subgrupo             := :new.subgrupo;
       v_item                := :new.item;
       v_usuario_estq        := :new.usuario_estq;
       v_usuario_exped        := :new.usuario_exped;
       v_flag_controle        := :new.flag_controle;
       v_data_inclusao_new   := :new.data_inclusao;
       v_pedido_venda_new    := :new.pedido_venda;
       v_seq_item_pedido_new  := :new.seq_item_pedido;
       v_nota_inclusao_new    := :new.nota_inclusao;
       v_serie_nota_new      := :new.serie_nota;
       v_estoque_tag_new     := :new.estoque_tag;
       v_lote_new            := :new.lote;
       v_deposito_new        := :new.deposito;
       v_transacao_ent_new   := :new.transacao_ent;
       v_data_entrada_new    := :new.data_entrada;
       v_pre_romaneio_new    := :new.pre_romaneio;
       v_emp_saida_new       := :new.emp_saida;
       v_seq_saida_new       := :new.seq_saida;
       v_flag_inventario_new := :new.flag_inventario;
       v_forn9_entr_new      := :new.forn9_entr;
       v_forn4_entr_new      := :new.forn4_entr;
       v_forn2_entr_new      := :new.forn2_entr;
       v_nota_entr_new       := :new.nota_entr;
       v_serie_entr_new      := :new.serie_entr;
       v_seq_entr_new        := :new.seq_entr;
       v_nome_prog_050_new   := :new.nome_prog_050;
       v_pedido_compra_new   := :new.pedido_compra;
       v_seq_compra_new      := :new.seq_compra;
       v_endereco_new        := :new.endereco;
       v_nome_programa       := inter_fn_nome_programa(v_sid);
       v_seq_operacao_new    := :new.seq_operacao;
       v_executa_trigger_new := :new.executa_trigger;

    end if; --fim do if inserting or updating

    --alimenta as vari?veis old caso seja insert ou update
    if deleting or updating
    then
       if deleting
       then
          v_operacao      := 'd';
          v_periodo_producao     := :old.periodo_producao;
          v_ordem_producao       := :old.ordem_producao;
          v_ordem_confeccao      := :old.ordem_confeccao;
          v_sequencia             := :old.sequencia;
          v_nivel                := :old.nivel;
          v_grupo                := :old.grupo;
          v_subgrupo               := :old.subgrupo;
          v_item                   := :old.item;
       else
          v_operacao      := 'u';
       end if;

       v_estagio_old          := :old.estagio;
       v_nr_volume_old        := :old.nr_volume;
       v_data_inclusao_old   := :old.data_inclusao;
       v_pedido_venda_old    := :old.pedido_venda;
       v_seq_item_pedido_old  := :old.seq_item_pedido;
       v_nota_inclusao_old    := :old.nota_inclusao;
       v_serie_nota_old      := :old.serie_nota;
       v_estoque_tag_old     := :old.estoque_tag;
       v_lote_old            := :old.lote;
       v_deposito_old        := :old.deposito;
       v_transacao_ent_old   := :old.transacao_ent;
       v_data_entrada_old    := :old.data_entrada;
       v_pre_romaneio_old    := :old.pre_romaneio;
       v_emp_saida_old       := :old.emp_saida;
       v_seq_saida_old       := :old.seq_saida;
       v_flag_inventario_old := :old.flag_inventario;
       v_forn9_entr_old      := :old.forn9_entr;
       v_forn4_entr_old      := :old.forn4_entr;
       v_forn2_entr_old      := :old.forn2_entr;
       v_nota_entr_old       := :old.nota_entr;
       v_serie_entr_old      := :old.serie_entr;
       v_seq_entr_old        := :old.seq_entr;
       v_nome_prog_050_old   := :old.nome_prog_050;
       v_pedido_compra_old   := :old.pedido_compra;
       v_seq_compra_old      := :old.seq_compra;
       v_endereco_old        := :old.endereco;
       v_seq_operacao_old    := :old.seq_operacao;
       v_executa_trigger_old := :old.executa_trigger;
    

    end if; --fim do if inserting or updating

    --insere na pcpc_330_log o registro.
    insert into pcpc_330_log (
       periodo_producao,          ordem_producao,
       ordem_confeccao,          sequencia,
       estagio,                  nr_volume,
       nivel,                    grupo,
       subgrupo,                  item,
       usuario_estq,              usuario_exped,
       flag_controle,            pedido_venda_old,
       seq_item_pedido_old,      nota_inclusao_old,
       serie_nota_old,            data_inclusao_old,
       data_inclusao_new,         pedido_venda_new,
       seq_item_pedido_new,      nota_inclusao_new,
       serie_nota_new,            operacao,
       data_operacao,            usuario_rede,
       maquina_rede,             aplicativo,
       estoque_tag_old,          estoque_tag_new,
       lote_old,                 lote_new,
       deposito_old,             deposito_new,
       transacao_ent_old,        transacao_ent_new,
       data_entrada_new,         pre_romaneio_old,
       pre_romaneio_new,         emp_saida_old,
       emp_saida_new,            seq_saida_old,
       seq_saida_new,            flag_inventario_old,
       flag_inventario_new,      forn9_entr_old,
       forn9_entr_new,           forn4_entr_old,
       forn4_entr_new,           forn2_entr_old,
       forn2_entr_new,           nota_entr_old,
       nota_entr_new,            serie_entr_old,
       serie_entr_new,           seq_entr_old,
       seq_entr_new,             nome_prog_050_old,
       nome_prog_050_new,        estagio_old,
       nr_volume_old,            data_entrada_old,
       pedido_compra_old,         pedido_compra_new,
       seq_compra_old,            seq_compra_new,
       nome_programa,             endereco_old,
       endereco_new,              new_seq_operacao,
       old_seq_operacao,          executa_trigger_old,
       executa_trigger_new,       usuario_ult_movimento)
    values (
       v_periodo_producao,         v_ordem_producao,
       v_ordem_confeccao,         v_sequencia,
       v_estagio,                 v_nr_volume,
       v_nivel,                   v_grupo,
       v_subgrupo,                 v_item,
       v_usuario_estq,             v_usuario_exped,
       v_flag_controle,           v_pedido_venda_old,
       v_seq_item_pedido_old,     v_nota_inclusao_old,
       v_serie_nota_old,           v_data_inclusao_old,
       v_data_inclusao_new,       v_pedido_venda_new,
       v_seq_item_pedido_new,     v_nota_inclusao_new,
       v_serie_nota_new,           v_operacao,
       v_data_operacao,           v_usuario_rede,
       v_maquina_rede,            v_aplicativo,
       v_estoque_tag_old,         v_estoque_tag_new,
       v_lote_old,                v_lote_new,
       v_deposito_old,            v_deposito_new,
       v_transacao_ent_old,       v_transacao_ent_new,
       v_data_entrada_new,        v_pre_romaneio_old,
       v_pre_romaneio_new,        v_emp_saida_old,
       v_emp_saida_new,           v_seq_saida_old,
       v_seq_saida_new,           v_flag_inventario_old,
       v_flag_inventario_new,     v_forn9_entr_old,
       v_forn9_entr_new,          v_forn4_entr_old,
       v_forn4_entr_new,          v_forn2_entr_old,
       v_forn2_entr_new,          v_nota_entr_old,
       v_nota_entr_new,           v_serie_entr_old,
       v_serie_entr_new,          v_seq_entr_old,
       v_seq_entr_new,            v_nome_prog_050_old,
       v_nome_prog_050_new,       v_estagio_old,
       v_nr_volume_old,           v_data_entrada_old,
       v_pedido_compra_old,       v_pedido_compra_new,
       v_seq_compra_old,          v_seq_compra_new,
       v_nome_programa,           v_endereco_old,
       v_endereco_new,            v_seq_operacao_old,
       v_seq_operacao_new,        v_executa_trigger_old,
       v_executa_trigger_new,     v_ult_usuario);
              
           
end inter_tr_pcpc_330_log;

-- ALTER TRIGGER "INTER_TR_PCPC_330_LOG" ENABLE

-- ALTER TRIGGER "INTER_TR_PCPC_330_LOG" ENABLE
