CREATE OR REPLACE TRIGGER "INTER_TR_BASI_010_HIST" 
before insert or
       delete or
       update of item_ativo, narrativa , calc_disponibilidade, data_fim_disp, codigo_barras, classific_fiscal
on basi_010
for each row 
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_sit_item_old           varchar2(20);
   ws_sit_item_new           varchar2(20);

   ws_teve                   number;
   long_aux                  varchar2(4000);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   or updating
   then
      if :new.item_ativo = 0
      then
         ws_sit_item_new := inter_fn_buscar_tag('lb28086#ATIVO',ws_locale_usuario,ws_usuario_systextil);
      end if;

      if :new.item_ativo = 1
      then
         ws_sit_item_new := inter_fn_buscar_tag('lb30111#INATIVO',ws_locale_usuario,ws_usuario_systextil);
      end if;

      if :new.item_ativo = 2
      then
         ws_sit_item_new := inter_fn_buscar_tag('lb00751#LANCAMENTO',ws_locale_usuario,ws_usuario_systextil);
      end if;
   end if;

   if updating
   or deleting
   then
      if :old.item_ativo = 0
      then
         ws_sit_item_old := inter_fn_buscar_tag('lb28086#ATIVO',ws_locale_usuario,ws_usuario_systextil);
      end if;

      if :old.item_ativo = 1
      then
         ws_sit_item_old := inter_fn_buscar_tag('lb30111#INATIVO',ws_locale_usuario,ws_usuario_systextil);
      end if;

      if :old.item_ativo = 2
      then
         ws_sit_item_old := inter_fn_buscar_tag('lb00751#LANCAMENTO',ws_locale_usuario,ws_usuario_systextil);
      end if;
   end if;

   if inserting
   then
      begin
         insert into hist_999 (
            tabela,                          operacao,
            nivel_produto,                   grupo_produto,
            subgrupo_produto,                item_produto,
            long01
         ) values (
            'BASI_010',                      'I',
            :new.nivel_estrutura,            :new.grupo_estrutura,
            :new.subgru_estrutura,           :new.item_estrutura,
            '               '||
            inter_fn_buscar_tag('lb37298#CADASTRO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
            chr(10)               ||
            chr(10)               ||
            inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :new.nivel_estrutura   || '.'
                                  || :new.grupo_estrutura   || '.'
                                  || :new.subgru_estrutura  || '.'
                                  || :new.item_estrutura    || ' - '
                                  || :new.descricao_15      ||
            chr(10)               ||
            inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :new.item_ativo   || ' - '
                                  || ws_sit_item_new       ||
            chr(10)               ||
            inter_fn_buscar_tag('lb00785#NARRATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :new.narrativa    ||
            chr(10)               ||
            inter_fn_buscar_tag('lb46562#CALC DISPONIBILIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :new.calc_disponibilidade ||
            chr(10)               ||
            inter_fn_buscar_tag('lb46563#DATA FIM DISP:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :new.data_fim_disp||
            chr(10)               ||
            'CLASSIFICACAO FISCAL NCM: '|| :new.classific_fiscal
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_999(I)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   if updating
   then
      ws_teve := 0;

      long_aux := '               ' ||
                  inter_fn_buscar_tag('lb37298#CADASTRO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                  chr(10)               ||
                  chr(10)               ||
                  inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :new.nivel_estrutura   || '.'
                                        || :new.grupo_estrutura   || '.'
                                        || :new.subgru_estrutura  || '.'
                                        || :new.item_estrutura    || ' - '
                                        || :new.descricao_15      ||
                  chr(10);

      if :old.item_ativo <> :new.item_ativo
      then
         ws_teve := 1;

         long_aux := long_aux ||
                     inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.item_ativo   || ' - '
                                        || ws_sit_item_old   || ' -> '
                                        || :new.item_ativo   || ' - '
                                        || ws_sit_item_new   ||
                     chr(10);
      end if;

      if :old.narrativa <> :new.narrativa
      then
        ws_teve := 1;

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb00785#NARRATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.narrativa    || ' -> '
                                        || :new.narrativa    ||
                     chr(10);
      end if;
      
      if :old.calc_disponibilidade <> :new.calc_disponibilidade
      then
        ws_teve := 1;

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb46562#CALC DISPONIBILIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.calc_disponibilidade    || ' -> '
                                        || :new.calc_disponibilidade    ||
                     chr(10);
      end if;
      
      if :old.data_fim_disp is null or :old.data_fim_disp <> :new.data_fim_disp
      then
        ws_teve := 1;

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb46563#DATA FIM DISP:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.data_fim_disp    || ' -> '
                                        || :new.data_fim_disp    ||
                     chr(10);
      end if;
      
      if :old.codigo_barras is null or :old.codigo_barras <> :new.codigo_barras  or :new.codigo_barras is null
      then
        ws_teve := 1;

         long_aux := long_aux ||
            inter_fn_buscar_tag('lb04510#C�DIGO BARRAS:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                        || :old.codigo_barras    || ' -> '
                                        || :new.codigo_barras    ||
                     chr(10);
      end if;
      
      if :old.classific_fiscal is null or :old.classific_fiscal <> :new.classific_fiscal  or :new.classific_fiscal is null
      then
        ws_teve := 1;

         long_aux := long_aux ||
            'CLASSIFICACAO FISCAL NCM: ' || :old.classific_fiscal    || ' -> '
                                         || :new.classific_fiscal    || chr(10);
      end if;
      

      if ws_teve = 1
      then
         begin
            insert into hist_999 (
               tabela,                          operacao,
               nivel_produto,                   grupo_produto,
               subgrupo_produto,                item_produto,
               long01
            ) values (
               'BASI_010',                      'A',
               :new.nivel_estrutura,            :new.grupo_estrutura,
               :new.subgru_estrutura,           :new.item_estrutura,
               long_aux
            );
         exception when OTHERS then
            raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_999(A)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;

   if deleting
   then
      begin
         insert into hist_999 (
            tabela,                          operacao,
            nivel_produto,                   grupo_produto,
            subgrupo_produto,                item_produto,
            long01
         ) values (
            'BASI_010',                      'D',
            :old.nivel_estrutura,            :old.grupo_estrutura,
            :old.subgru_estrutura,           :old.item_estrutura,
            '               '||
            inter_fn_buscar_tag('lb37298#CADASTRO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
            chr(10)               ||
            chr(10)               ||
            inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :old.nivel_estrutura   || '.'
                                  || :old.grupo_estrutura   || '.'
                                  || :old.subgru_estrutura  || '.'
                                  || :old.item_estrutura    || ' - '
                                  || :old.descricao_15      ||
            chr(10)               ||
            inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :old.item_ativo   || ' - '
                                  || ws_sit_item_new   ||
            chr(10)               ||
            inter_fn_buscar_tag('lb00785#NARRATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :old.narrativa    ||
            chr(10)               ||
            inter_fn_buscar_tag('lb46562#CALC DISPONIBILIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :old.calc_disponibilidade    ||
            chr(10)               ||
            inter_fn_buscar_tag('lb46563#DATA FIM DISP:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :old.data_fim_disp||
            chr(10)               ||
            'CLASSIFICACAO FISCAL NCM: ' || :old.classific_fiscal
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_999(D)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;
end inter_tr_basi_010_hist;

-- ALTER TRIGGER "INTER_TR_BASI_010_HIST" ENABLE
/

exec inter_pr_recompile;

