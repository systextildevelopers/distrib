
  CREATE OR REPLACE PROCEDURE "INTER_PR_NEC_ORDEM_PLANEJ" 
    -- Recebe parametros para explosao da estrutura.
   (p_ordem_planejamento  in number,

    -- ORIGEM PARA EXPLODIR OS DADOS
    p_pedido_venda        in number,
    p_pedido_reserva      in number,

    -- DE QUAL PROJETO SE TRATA ESTA EXPLOSAO
    p_codigo_projeto      in varchar2,
    p_sequencia_projeto   in number,

   -- SE FOR UM PROJETO ESTES DADOS SAO OPCIONAIS DE UMA VENDA TEM QUE TER ESTES DADOS
    p_nivel_explode       in varchar2,
    p_grupo_explode       in varchar2,
    p_subgrupo_explode    in varchar2,
    p_item_explode        in varchar2,
    p_alternativa_explode in number,
    p_sequencia_explode   in number,

    p_qtde_programada     in number,
    p_qtde_perda          in number,
    p_qtde_vendida        in number,

    p_unid_res_planejada  in number)
is
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   -- ################# ESTRUTURA REAL
   -- Cria cursor para executar loop na basi_050 buscando todos
   -- os produtos da estrutura.
   cursor basi050 is
   select basi_050.nivel_comp,             basi_050.grupo_comp,
          basi_050.sub_comp,               basi_050.item_comp,
          basi_050.sequencia,              basi_050.consumo,
          basi_050.sub_item,               basi_050.item_item,
          basi_050.estagio,                basi_050.tipo_calculo,
          basi_050.alternativa_comp,       basi_050.relacao_banho,
          basi_050.cons_unid_med_generica, basi_050.tecido_principal
   from basi_050
   where  basi_050.nivel_item       = p_nivel_explode
     and  basi_050.grupo_item       = p_grupo_explode
     and (basi_050.sub_item         = p_subgrupo_explode or basi_050.sub_item  = '000')
     and (basi_050.item_item        = p_item_explode     or basi_050.item_item = '000000')
     and  basi_050.alternativa_item = p_alternativa_explode
   order by basi_050.sequencia;

   -- Declara as variaveis que serao utilizadas.  BASI_050
   p_nivel_comp050       basi_050.nivel_comp%type := '#';
   p_grupo_comp050       basi_050.grupo_comp%type := '#####';
   p_subgru_comp050      basi_050.sub_comp%type   := '###';
   p_item_comp050        basi_050.item_comp%type  := '######';
   p_sequencia050        basi_050.sequencia%type;
   p_consumo050          basi_050.consumo%type;
   p_sub_item050         basi_050.sub_item%type   := '###';
   p_item_item050        basi_050.item_item%type  := '######';
   p_estagio050          basi_050.estagio%type;
   p_tpcalculo050        basi_050.tipo_calculo%type;
   p_alternativa_comp    basi_050.alternativa_comp%type;

   v_codigo_projeto       basi_101.codigo_projeto%type;
   v_sequencia_projeto    basi_101.sequencia_projeto%type;
   v_tipo_produto         basi_101.codigo_tipo_produto%type;

   p_qtde_planejada       tmrp_625.qtde_reserva_planejada%type;

   v_total_625           number;

   p_relacao_banho_emp         empr_001.relacao_banho%type;
   p_relacao_banho_aux         number;
   p_relacao_banho             basi_050.relacao_banho%type;
   p_cons_unid_med_generica    number;

   p_unidades_planejadas       number;
   p_unidades_programadas      number;

   v_e_retilineo               number;
   v_qtde_programada           number;
   r_qtde_programada           number;

   v_aux                       number;
begin
   -- Dados do usuario logado





    inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- Busca parametro de empresa para calculo das quantidades que
   -- forem atraves do volume de banho.



   begin
      select empr_001.relacao_banho
      into p_relacao_banho_emp
      from empr_001;
   end;

   p_relacao_banho_aux       := 0;

   if p_nivel_explode <> '0'
   then
      if p_sequencia_explode = 0
      then
         begin
            select nvl(count(1),0)
            into v_total_625
            from tmrp_625
            where tmrp_625.ordem_planejamento         = p_ordem_planejamento
              and tmrp_625.pedido_venda               = p_pedido_venda
              and tmrp_625.pedido_reserva             = p_pedido_reserva
              and tmrp_625.seq_produto_origem         = p_sequencia_explode
              and tmrp_625.nivel_produto_origem       = '0'
              and tmrp_625.nivel_produto              = p_nivel_explode
              and tmrp_625.grupo_produto              = p_grupo_explode
              and tmrp_625.subgrupo_produto           = p_subgrupo_explode
              and tmrp_625.item_produto               = p_item_explode
              and tmrp_625.alternativa_produto        = p_alternativa_explode;
         exception
         when OTHERS then
            v_total_625 := 0;
         end;

         p_relacao_banho := 0;
         p_tpcalculo050  := 0;

         if v_total_625 = 0
         then
            -- Verifica se o produto esta devidamente cadastrado na basi_010
            begin
               select nvl(count(1),0)
               into v_aux
               from basi_010
               where basi_010.nivel_estrutura  = p_nivel_explode
                 and basi_010.grupo_estrutura  = p_grupo_explode
                 and basi_010.subgru_estrutura = p_subgrupo_explode
                 and basi_010.item_estrutura   = p_item_explode;
            exception when others then
               v_aux := 0;
            end;

            if v_aux = 0
            then
               -- ATENCAO! Produto nao cadastrado {0}.
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds25063', p_nivel_explode||'.'||p_grupo_explode||'.'||p_subgrupo_explode||'.'||p_item_explode , '', '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end if;

            begin
               insert into tmrp_625 (
                   ordem_planejamento,
                   pedido_venda,                             pedido_reserva,
                   nivel_produto,                            grupo_produto,
                   subgrupo_produto,                         item_produto,
                   alternativa_produto,                      seq_produto,
                   qtde_reserva_planejada,                   situacao,
                   data_requerida,                           qtde_vendida,
                   relacao_banho,                            tipo_calculo,
                   qtde_adicional,                           codigo_projeto
               ) values (
                   p_ordem_planejamento,
                   p_pedido_venda,                           p_pedido_reserva,
                   p_nivel_explode,                          p_grupo_explode,
                   p_subgrupo_explode,                       p_item_explode,
                   p_alternativa_explode,                    p_sequencia_explode,
                   p_qtde_programada + p_qtde_perda,         '2',
                   trunc(sysdate,'dd'),                      p_qtde_vendida,
                   p_relacao_banho,                          p_tpcalculo050,
                   p_qtde_perda,                             p_codigo_projeto
               );
            exception when others then
               -- ATENCAO! Nao inseriu {0}. Status = {1}.
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds21797', 'tmrp_625(20)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         else
            begin
               update tmrp_625
                  set tmrp_625.qtde_reserva_planejada     =
                      tmrp_625.qtde_reserva_planejada + (p_qtde_programada + p_qtde_perda),
                      tmrp_625.qtde_vendida               = tmrp_625.qtde_vendida + p_qtde_vendida
               where tmrp_625.ordem_planejamento          = p_ordem_planejamento
                 and tmrp_625.pedido_venda                = p_pedido_venda
                 and tmrp_625.pedido_reserva              = p_pedido_reserva
                 and tmrp_625.seq_produto_origem          = p_sequencia_explode
                 and tmrp_625.nivel_produto_origem        = '0'
                 and tmrp_625.nivel_produto               = p_nivel_explode
                 and tmrp_625.grupo_produto               = p_grupo_explode
                 and tmrp_625.subgrupo_produto            = p_subgrupo_explode
                 and tmrp_625.item_produto                = p_item_explode
                 and tmrp_625.alternativa_produto         = p_alternativa_explode;
            exception when others then
               -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_625(26)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end if;
      end if;

      -- Inicio do loop da explosao da estrutura.
      for reg_basi050 in basi050
      loop
      -- Carrega as variaveis com os valores do cursor.
         p_nivel_comp050    := reg_basi050.nivel_comp;
         p_grupo_comp050    := reg_basi050.grupo_comp;
         p_subgru_comp050   := reg_basi050.sub_comp;
         p_item_comp050     := reg_basi050.item_comp;
         p_sequencia050     := reg_basi050.sequencia;
         p_consumo050       := reg_basi050.consumo;

         p_sub_item050      := reg_basi050.sub_item;
         p_item_item050     := reg_basi050.item_item;

         p_estagio050       := reg_basi050.estagio;
         p_tpcalculo050     := reg_basi050.tipo_calculo;
         p_alternativa_comp := reg_basi050.alternativa_comp;
         p_cons_unid_med_generica := reg_basi050.cons_unid_med_generica;

         if p_subgru_comp050 = '000' or p_consumo050 = 0.0000000
         then
            begin
               select basi_040.sub_comp,   basi_040.consumo,   basi_040.cons_unid_med_generica
               into   p_subgru_comp050,    p_consumo050,       p_cons_unid_med_generica
               from basi_040
               where basi_040.nivel_item       = p_nivel_explode
                 and basi_040.grupo_item       = p_grupo_explode
                 and basi_040.sub_item         = p_subgrupo_explode
                 and basi_040.item_item        = p_item_item050
                 and basi_040.alternativa_item = p_alternativa_explode
                 and basi_040.sequencia        = p_sequencia050;
            exception when no_data_found then
               p_subgru_comp050         := '000';
               p_consumo050             := 0.0000000;
               p_cons_unid_med_generica := 0.0000000;
            end;
         end if;

         if p_item_comp050 = '000000'
         then
            begin
               select basi_040.item_comp
               into p_item_comp050
               from basi_040
               where basi_040.nivel_item       = p_nivel_explode
                 and basi_040.grupo_item       = p_grupo_explode
                 and basi_040.sub_item         = p_sub_item050
                 and basi_040.item_item        = p_item_explode
                 and basi_040.alternativa_item = p_alternativa_explode
                 and basi_040.sequencia        = p_sequencia050;
            exception when no_data_found then
                  p_item_comp050 := '000000';
            end;
         end if;

         if p_consumo050 <= 0.0000000
         then
            -- raise_application_error(-20000, 'ATENCAO! Problema na estrutura do produto. O consumo do item: ' || p_nivel_comp050 || '.' || p_grupo_comp050 || '.' || p_subgru_comp050 || p_item_comp050 || 'esta zerado (0.00000). Verifique a estrutura.');
           raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26671', p_nivel_comp050 || '.' || p_grupo_comp050 || '.' || p_subgru_comp050 || '.' ||p_item_comp050, '' , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end if;

         v_e_retilineo          := 0;

         p_unidades_programadas := 0;
         p_unidades_planejadas  := 0;

         if p_nivel_comp050 = '2'
         then
            begin
               select nvl(count(*),0)
               into v_e_retilineo
               from basi_030
               where basi_030.nivel_estrutura = p_nivel_comp050
                 and basi_030.referencia      = p_grupo_comp050
                 and basi_030.tipo_produto    in (3,4,6);
            exception when no_data_found then
               v_e_retilineo := 0;
            end;

            if v_e_retilineo >= 1
            then
               p_unidades_planejadas  := (p_qtde_programada + p_qtde_perda) * p_cons_unid_med_generica;
            else
               p_unidades_planejadas  := 0;
            end if;
         end if;

         if p_nivel_comp050 = '4'
         then
            p_unidades_planejadas  := p_unid_res_planejada;
         end if;

         if p_nivel_explode = '5' and p_relacao_banho_aux = 0.00
         then
            if reg_basi050.relacao_banho = 0.00
            then
               p_relacao_banho := p_relacao_banho_emp;
            else
               p_relacao_banho := reg_basi050.relacao_banho;
            end if;
         else
            p_relacao_banho := p_relacao_banho_aux;
         end if;

         if p_nivel_explode = '2'
         then
            p_relacao_banho := 0.00;
         end if;

         if p_tpcalculo050 = 2
         then
            if p_relacao_banho <= 0
            then
               p_relacao_banho := 1.000;
            end if;

            p_qtde_planejada := p_consumo050 * ((p_qtde_programada + p_qtde_perda) * p_relacao_banho);
         else
            p_qtde_planejada := ((p_qtde_programada + p_qtde_perda) * p_consumo050);
         end if;

         v_qtde_programada := 0.000000;

         if p_qtde_programada > 0
         then
            if p_relacao_banho <= 0
            then
               p_relacao_banho := 1.000;
            end if;

            if p_tpcalculo050 = 2
            then
               v_qtde_programada := p_consumo050 * (p_qtde_programada * p_relacao_banho);
            else
               v_qtde_programada := (p_qtde_programada * p_consumo050);
            end if;
         end if;

         select nvl(count(1),0)
         into v_total_625
         from tmrp_625
         where tmrp_625.ordem_planejamento         = p_ordem_planejamento
           and tmrp_625.pedido_venda               = p_pedido_venda
           and tmrp_625.pedido_reserva             = p_pedido_reserva
           and tmrp_625.seq_produto_origem         = p_sequencia_explode
           and tmrp_625.nivel_produto_origem       = p_nivel_explode
           and tmrp_625.grupo_produto_origem       = p_grupo_explode
           and tmrp_625.subgrupo_produto_origem    = p_subgrupo_explode
           and tmrp_625.item_produto_origem        = p_item_explode
           and tmrp_625.alternativa_produto_origem = p_alternativa_explode
           and tmrp_625.seq_produto                = p_sequencia050
           and tmrp_625.nivel_produto              = p_nivel_comp050
           and tmrp_625.grupo_produto              = p_grupo_comp050
           and tmrp_625.subgrupo_produto           = p_subgru_comp050
           and tmrp_625.item_produto               = p_item_comp050
           and tmrp_625.alternativa_produto        = p_alternativa_comp;

--if p_nivel_explode = '4'
--then raise_application_error(-20000, 'produto origem: '  || p_nivel_explode    || '.' || p_grupo_explode || '.' ||
--                                                            p_subgrupo_explode || '.' || p_item_explode  ||
--                                     ' produto : '       || p_nivel_comp050    || '.' || p_grupo_comp050 || '.' ||
--                                                            p_subgru_comp050   || '.' || p_item_comp050  ||
--                                     ' qtde planejada: ' || p_qtde_planejada ||
--                                     ' consumo: '        || p_consumo050 ||
--                                     ' relacao banho: '  || p_relacao_banho ||
--                                     ' parametro: '           || p_qtde_programada ||
--                                     ' parametro qtde prog: ' || v_qtde_prog_aux);
--end if;

         if v_total_625 = 0
         then
            v_codigo_projeto    := p_codigo_projeto;
            v_sequencia_projeto := p_sequencia_projeto;
            v_tipo_produto      := 'XXXXXX';

            if v_codigo_projeto is null or v_codigo_projeto = ' ' or v_sequencia_projeto = 0
            then
               v_codigo_projeto    := '000000';
               v_sequencia_projeto := 0;
               v_tipo_produto      := 'XXXXXX';
            end if;

            if v_codigo_projeto <> '000000' and v_sequencia_projeto <> 0
            then
               begin
                  select basi_101.codigo_tipo_produto
                  into   v_tipo_produto
                  from basi_101
                  where basi_101.codigo_projeto        = v_codigo_projeto
                    and basi_101.sequencia_projeto     = v_sequencia_projeto;
               exception when no_data_found then
                  v_tipo_produto  := 'XXXXXX';
               end;
            end if;

            -- Verifica se o componente esta devidamente cadastrado na basi_010
            begin
               select nvl(count(1),0)
               into v_aux
               from basi_010
               where basi_010.nivel_estrutura  = p_nivel_comp050
                 and basi_010.grupo_estrutura  = p_grupo_comp050
                 and basi_010.subgru_estrutura = p_subgru_comp050
                 and basi_010.item_estrutura   = p_item_comp050;
            exception when others then
               v_aux := 0;
            end;
            if v_aux = 0
            then
--               raise_application_error(-20000,'O componente '||p_nivel_comp050||'.'||p_grupo_comp050||'.'||p_subgru_comp050||'.'||p_item_comp050||' do produto '||p_nivel_explode||'.'||p_grupo_explode||'.'||p_subgrupo_explode||'.'||p_item_explode||' nao esta cadastrado.');

               -- ATENCAO! O componente {0} do produto {1} nao esta cadastrado.
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26657', p_nivel_comp050||'.'||p_grupo_comp050||'.'||p_subgru_comp050||'.'||p_item_comp050 ,
                                                                                      p_nivel_explode||'.'||p_grupo_explode||'.'||p_subgrupo_explode||'.'||p_item_explode, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end if;

            begin
               insert into tmrp_625 (
                   ordem_planejamento,
                   pedido_venda,                             pedido_reserva,

                   nivel_produto_origem,                     grupo_produto_origem,
                   subgrupo_produto_origem,                  item_produto_origem,
                   alternativa_produto_origem,               seq_produto_origem,

                   nivel_produto,                            grupo_produto,
                   subgrupo_produto,                         item_produto,
                   alternativa_produto,                      seq_produto,

                   estagio_producao,                         consumo,
                   qtde_reserva_planejada,                   situacao,

                   codigo_projeto,                           seq_projeto_origem,
                   tipo_produto_origem,                      data_requerida,
                   relacao_banho,                            tipo_calculo,
                   cons_unid_med_generica,
                   unidades_reserva_planejada,               tecido_principal
               ) values (
                   p_ordem_planejamento,
                   p_pedido_venda,                           p_pedido_reserva,

                   p_nivel_explode,                          p_grupo_explode,
                   p_subgrupo_explode,                       p_item_explode,
                   p_alternativa_explode,                    p_sequencia_explode,

                   p_nivel_comp050,                          p_grupo_comp050,
                   p_subgru_comp050,                         p_item_comp050,
                   p_alternativa_comp,                       p_sequencia050,

                   p_estagio050,                             p_consumo050,
                   p_qtde_planejada,                         '2',

                   p_codigo_projeto,                         p_sequencia_projeto,
                   v_tipo_produto,                           trunc(sysdate,'dd'),
                   p_relacao_banho,                          p_tpcalculo050,
                   p_cons_unid_med_generica,
                   p_unidades_planejadas,                   reg_basi050.tecido_principal
               );
            exception when others then
               -- ATENCAO! Nao inseriu {0}. Status = {1}.
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds21797', 'tmrp_625(21)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end if;

         r_qtde_programada := 0.000000;


         if v_total_625 > 0
         then
            begin
               update tmrp_625
                  set tmrp_625.qtde_reserva_planejada      = tmrp_625.qtde_reserva_planejada + p_qtde_planejada,
                      tmrp_625.unidades_reserva_planejada  = tmrp_625.unidades_reserva_planejada + p_unidades_planejadas,
                      tmrp_625.relacao_banho               = p_relacao_banho,
                      tmrp_625.tipo_calculo                = p_tpcalculo050,
                      tmrp_625.estagio_producao            = p_estagio050,
                      tmrp_625.consumo                     = p_consumo050,
                      tmrp_625.cons_unid_med_generica      = p_cons_unid_med_generica,
                      tmrp_625.tecido_principal            = reg_basi050.tecido_principal
               where tmrp_625.ordem_planejamento         = p_ordem_planejamento
                 and tmrp_625.pedido_venda               = p_pedido_venda
                 and tmrp_625.pedido_reserva             = p_pedido_reserva
                 and tmrp_625.seq_produto_origem         = p_sequencia_explode
                 and tmrp_625.nivel_produto_origem       = p_nivel_explode
                 and tmrp_625.grupo_produto_origem       = p_grupo_explode
                 and tmrp_625.subgrupo_produto_origem    = p_subgrupo_explode
                 and tmrp_625.item_produto_origem        = p_item_explode
                 and tmrp_625.alternativa_produto_origem = p_alternativa_explode
                 and tmrp_625.seq_produto                = p_sequencia050
                 and tmrp_625.nivel_produto              = p_nivel_comp050
                 and tmrp_625.grupo_produto              = p_grupo_comp050
                 and tmrp_625.subgrupo_produto           = p_subgru_comp050
                 and tmrp_625.item_produto               = p_item_comp050
                 and tmrp_625.alternativa_produto        = p_alternativa_comp;
               exception when others then
                  -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_625(22)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end if;

         INTER_PR_NEC_ORDEM_PLANEJ(p_ordem_planejamento,
                                   p_pedido_venda,
                                   p_pedido_reserva,
                                   p_codigo_projeto,
                                   p_sequencia_projeto,
                                   p_nivel_comp050,
                                   p_grupo_comp050,
                                   p_subgru_comp050,
                                   p_item_comp050,
                                   p_alternativa_comp,
                                   p_sequencia050,

                                   p_qtde_planejada,
                                   0,
                                   0,
                                   p_unidades_planejadas);
      end loop;
   end if;

end INTER_PR_NEC_ORDEM_PLANEJ;

 

/

exec inter_pr_recompile;

