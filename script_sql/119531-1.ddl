DECLARE
   cursor programa is select * from hdoc_033
                      where hdoc_033.programa IN ('pcpc_f160', 'pcpc_f070', 'pcpc_f090', 'pcpc_f045');
   v_usuario varchar2(15);
   v_rpt number;
begin

   v_rpt := 0;
   begin
      select count(1)
      into v_rpt
      from fatu_500
      where fatu_500.rpt_ordem_benef = 'pb_oopb151_aa';
   end;

  if v_rpt > 0 then
    v_rpt := 0;
  else
    v_rpt := 1;
  end if;

   for reg_programa in programa
   loop
      begin
         select oper_550.usuario
         into   v_usuario
         from oper_550
         where oper_550.usuario       = reg_programa.usu_prg_cdusu
           and oper_550.empresa       = reg_programa.usu_prg_empr_usu
           and oper_550.nome_programa = reg_programa.programa
           and oper_550.nome_field    = 'data_producao';
      exception when no_data_found then
         insert into oper_550 (
            usuario,                     empresa,
            nome_programa,               nome_subprograma,
            nome_field,                  requerido,
            acessivel,                   inicia_campo
         ) values (
            reg_programa.usu_prg_cdusu,  reg_programa.usu_prg_empr_usu,
            reg_programa.programa,       ' ',
            'data_producao',              v_rpt,
            v_rpt,                        0
         );
         commit;
      end;
   end loop;
end;

/

exec inter_pr_recompile;
