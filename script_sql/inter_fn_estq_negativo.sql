create or replace FUNCTION "INTER_FN_ESTQ_NEGATIVO"(fver_cod_dep in number, fver_nivel in varchar2)
	return boolean
is 
   a SYS_REFCURSOR;
   estqNegativo boolean;
   campo varchar(3000);
   sqlFormatado varchar(3000);
   v_value number;
begin
	
   estqNegativo := false;

   if(fver_nivel = null or fver_nivel = '') THEN
   	return false;
   end if;

   if (fver_nivel = '1') THEN
   	 campo := 'hdoc_001.campo_numerico01';
   elsif (fver_nivel = '2') THEN
   	 campo := 'hdoc_001.campo_numerico02';
   elsif (fver_nivel = '4') THEN
   	 campo := 'hdoc_001.campo_numerico03';
   elsif (fver_nivel = '7') THEN
   	 campo := 'hdoc_001.campo_numerico04';
   elsif (fver_nivel = '9') THEN
   	 campo := 'hdoc_001.campo_numerico05';
   else 
     return false;
   end if;

    
   estqNegativo := false;
   
   sqlFormatado  := 'select ' || campo ||
        			' from hdoc_001 '||
        			' where hdoc_001.tipo = 81 and hdoc_001.codigo in (0, '|| fver_cod_dep ||') ORDER BY hdoc_001.codigo DESC ';
   EXECUTE IMMEDIATE sqlFormatado;
   OPEN a FOR sqlFormatado;
   loop
       FETCH a INTO v_value;
       EXIT WHEN a%NOTFOUND;
       if(v_value > 0) then
       	  estqNegativo := true;
       else 
       	  estqNegativo := false;
       end if;
        
       EXIT;
   end loop;

   return(estqNegativo); 
end inter_fn_estq_negativo;
 

/

exec inter_pr_recompile;

