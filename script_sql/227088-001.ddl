CREATE TABLE empr_015 (
  param VARCHAR2(40) DEFAULT '' NOT NULL ,
  tipo_alerta NUMBER(1) DEFAULT 0 NOT NULL ,
  processo VARCHAR2(10) DEFAULT '' NOT NULL ,
  codigo_empresa NUMBER(3) DEFAULT 0 NOT NULL ,
  codigo_usuario NUMBER(5) DEFAULT 0 NOT NULL ,
  nome_usuario VARCHAR2(15) DEFAULT '' NOT NULL ,
  mensagem VARCHAR2(1000) DEFAULT '' NOT NULL,
  data_ocorrido DATE  DEFAULT SYSDATE NOT NULL
);

COMMENT ON TABLE empr_015 IS 'Tabela responsável por guardar o log de parâmetros empr_008, parametrizados na tela pcpb_fa14';
