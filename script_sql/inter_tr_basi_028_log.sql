
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_028_LOG" 
   after insert or delete or update
       of NIVEL_COMPONENTE,         GRUPO_COMPONENTE,
          SUBGRUPO_COMPONENTE,      ITEM_COMPONENTE,
          CNPJ_CLIENTE9,            CNPJ_CLIENTE4,
          CNPJ_CLIENTE2,            CODIGO_PROJETO,
          SEQUENCIA_PROJETO,        CODIGO_ATIVIDADE,
          SEQUENCIA_ATIVIDADE,      DATA_APROV_REPROV,
          HORA_APROV_REPROV,        SITUACAO_COMPONENTE,
          OBSERVACAO
   on basi_028
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_cod_tipo_produto_n     varchar2(60);
   ws_cod_tipo_produto_o     varchar2(60);

   ws_tipo_produto_o         varchar2(60);
   ws_tipo_produto_n         varchar2(60);

   ws_desc_atividade_o       varchar2(60);
   ws_desc_atividade_n       varchar2(60);

   ws_desc_projeto_o         varchar2(60);
   ws_desc_projeto_n         varchar2(60);

   ws_resp_projeto_o         varchar2(60);
   ws_resp_projeto_n         varchar2(60);

   ws_cod_responsavel_o      number(5);
   ws_cod_responsavel_n      number(5);

   ws_nome_responsavel_o     varchar2(60);
   ws_nome_responsavel_n     varchar2(60);

   ws_cod_cargo_o            number(4);
   ws_cod_cargo_n            number(4);

   ws_desc_cargo_o           varchar2(30);
   ws_desc_cargo_n           varchar2(30);

   ws_nome_cliente_o         varchar2(60);
   ws_nome_cliente_n         varchar2(60);

   ws_desc_comp_n            varchar2(120);
   ws_desc_grupo_n           varchar2(30);
   ws_desc_subgrupo_n        varchar2(30);
   ws_desc_item_n            varchar2(30);

   ws_desc_comb              varchar2(40);
   ws_alt_comb               number(2);
   ws_rot_comb               number(2);

   ws_sit_comp_o             varchar2(10);
   ws_sit_comp_n             varchar2(10);
   long_aux                  varchar2(4000);

   conta                     number;

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if updating and :new.tipo_aprovacao  = 1   --APROVACAO POR COMPONENTE
   then
      begin
         select basi_001.descricao_projeto, basi_001.codigo_responsavel
         into ws_desc_projeto_o, ws_resp_projeto_o
         from basi_001
         where basi_001.codigo_projeto = :old.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_o := '';
         ws_resp_projeto_o := 0;
      end;

      begin
         select basi_001.descricao_projeto, basi_001.codigo_responsavel
         into ws_desc_projeto_n, ws_resp_projeto_n
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_n := '';
         ws_resp_projeto_n := 0;
      end;

      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_o
         from basi_101
         where basi_101.codigo_projeto    = :old.codigo_projeto
           and basi_101.sequencia_projeto = :old.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_o := '';
      end;

      begin
         select basi_003.descricao
         into ws_tipo_produto_o
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_o;
      exception when no_data_found then
         ws_tipo_produto_o := '';
      end;

      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = :new.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_n := '';
      end;

      begin
         select basi_003.descricao
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n;
      exception when no_data_found then
         ws_tipo_produto_n := '';
      end;

      begin
         select basi_103.codigo_responsavel, basi_103.codigo_cargo
         into ws_cod_responsavel_o, ws_cod_cargo_o
         from basi_103
         where basi_103.codigo_projeto      = :old.codigo_projeto
           and basi_103.sequencia_projeto   = :old.sequencia_projeto
           and basi_103.codigo_atividade    = :old.codigo_atividade
           and basi_103.sequencia_atividade = :old.sequencia_atividade;
      exception when no_data_found then
         ws_cod_responsavel_o := 0;
         ws_cod_cargo_o       := 0;
      end;

      if ws_cod_responsavel_o = 0
      then ws_cod_responsavel_o := ws_resp_projeto_o;
      end if;

      begin
         select efic_050.nome
         into ws_nome_responsavel_o
         from efic_050
         where efic_050.cod_empresa     = ws_empresa
           and efic_050.cod_funcionario = ws_cod_responsavel_o;
      exception when no_data_found then
         ws_nome_responsavel_o := '';
      end;

      begin
         select mqop_120.descricao
         into ws_desc_cargo_o
         from mqop_120
         where mqop_120.codigo_cargo = ws_cod_cargo_o;
      exception when no_data_found then
         ws_desc_cargo_o := '';
      end;

      begin
         select basi_103.codigo_responsavel, basi_103.codigo_cargo
         into ws_cod_responsavel_n, ws_cod_cargo_n
         from basi_103
         where basi_103.codigo_projeto      = :new.codigo_projeto
           and basi_103.sequencia_projeto   = :new.sequencia_projeto
           and basi_103.codigo_atividade    = :new.codigo_atividade
           and basi_103.sequencia_atividade = :new.sequencia_atividade;
      exception when no_data_found then
         ws_cod_responsavel_n := 0;
         ws_cod_cargo_n       := 0;
      end;

      if ws_cod_responsavel_n = 0
      then ws_cod_responsavel_n := ws_resp_projeto_n;
      end if;

      begin
         select efic_050.nome
         into ws_nome_responsavel_n
         from efic_050
         where efic_050.cod_empresa     = ws_empresa
           and efic_050.cod_funcionario = ws_cod_responsavel_n;
      exception when no_data_found then
         ws_nome_responsavel_n := '';
      end;

      begin
         select mqop_120.descricao
         into ws_desc_cargo_n
         from mqop_120
         where mqop_120.codigo_cargo = ws_cod_cargo_n;
      exception when no_data_found then
         ws_desc_cargo_n := '';
      end;

      begin
         select basi_030.descr_referencia
         into ws_desc_grupo_n
         from basi_030
         where basi_030.nivel_estrutura = :new.nivel_componente
           and basi_030.referencia      = :new.grupo_componente;
      exception when no_data_found then
         ws_desc_grupo_n := '';
      end;

      begin
         select basi_020.descr_tam_refer
         into ws_desc_subgrupo_n
         from basi_020
         where basi_020.basi030_nivel030 = :new.nivel_componente
           and basi_020.basi030_referenc = :new.grupo_componente
           and basi_020.tamanho_ref      = :new.subgrupo_componente;
      exception when no_data_found then
         ws_desc_subgrupo_n := '';
      end;

      begin
         select basi_010.descricao_15
         into ws_desc_item_n
         from basi_010
         where basi_010.nivel_estrutura  = :new.nivel_componente
           and basi_010.grupo_estrutura  = :new.grupo_componente
           and basi_010.subgru_estrutura = :new.subgrupo_componente
           and basi_010.item_estrutura   = :new.item_componente;
      exception when no_data_found then
         ws_desc_item_n := '';
      end;

      ws_desc_comp_n := ws_desc_grupo_n || ' ' || ws_desc_subgrupo_n  || ' ' || ws_desc_item_n;

      begin
         select pedi_010.nome_cliente
         into ws_nome_cliente_n
         from pedi_010
         where pedi_010.cgc_9 = :new.cnpj_cliente9
           and pedi_010.cgc_4 = :new.cnpj_cliente4
           and pedi_010.cgc_2 = :new.cnpj_cliente2;
      exception when no_data_found then
         ws_nome_cliente_n := '';
      end;

      begin
         select pedi_010.nome_cliente
         into ws_nome_cliente_o
         from pedi_010
         where pedi_010.cgc_9 = :old.cnpj_cliente9
           and pedi_010.cgc_4 = :old.cnpj_cliente4
           and pedi_010.cgc_2 = :old.cnpj_cliente2;
      exception when no_data_found then
         ws_nome_cliente_o := '';
      end;

      begin
         select basi_002.descricao
         into ws_desc_atividade_o
         from basi_002
         where basi_002.codigo_atividade = :old.codigo_atividade;
      exception when no_data_found then
         ws_desc_atividade_o := '';
      end;

      begin
         select basi_002.descricao
         into ws_desc_atividade_n
         from basi_002
         where basi_002.codigo_atividade = :new.codigo_atividade;
      exception when no_data_found then
         ws_desc_atividade_n := '';
      end;

      long_aux := long_aux ||
      inter_fn_buscar_tag('lb34956#APROVACAO/REPROVACAO DE COMPONENTES DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
      chr(10)                    ||
      chr(10);

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb00523#COMPONENTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
         :new.nivel_componente     || '.' || :new.grupo_componente || '.' || :new.subgrupo_componente  || '.' || :new.item_componente ||
         ' - '                       || ws_desc_comp_n            ||
         chr(10);

      if :old.situacao_componente = 0 then ws_sit_comp_o := inter_fn_buscar_tag('lb31563#PENDENTE',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :old.situacao_componente = 1 then ws_sit_comp_o := inter_fn_buscar_tag('lb04901#ENVIADA',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :old.situacao_componente = 2 then ws_sit_comp_o := inter_fn_buscar_tag('lb23991#APROVADA',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :old.situacao_componente = 3 then ws_sit_comp_o := inter_fn_buscar_tag('lb34108#COM OBSERVACOES',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :old.situacao_componente = 4 then ws_sit_comp_o := inter_fn_buscar_tag('lb11294#REJEITADA',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :old.situacao_componente = 5 then ws_sit_comp_o := inter_fn_buscar_tag('lb08202#CANCELADA',ws_locale_usuario,ws_usuario_systextil);  end if;

      if :new.situacao_componente = 0 then ws_sit_comp_n := inter_fn_buscar_tag('lb31563#PENDENTE',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :new.situacao_componente = 1 then ws_sit_comp_n := inter_fn_buscar_tag('lb04901#ENVIADA',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :new.situacao_componente = 2 then ws_sit_comp_n := inter_fn_buscar_tag('lb23991#APROVADA',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :new.situacao_componente = 3 then ws_sit_comp_n := inter_fn_buscar_tag('lb34108#COM OBSERVACOES',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :new.situacao_componente = 4 then ws_sit_comp_n := inter_fn_buscar_tag('lb11294#REJEITADA',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :new.situacao_componente = 5 then ws_sit_comp_n := inter_fn_buscar_tag('lb08202#CANCELADA',ws_locale_usuario,ws_usuario_systextil);  end if;

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.situacao_componente,'0')     ||
           ' - '                       || ws_sit_comp_o               ||
           ' -> '                      || to_char(:new.situacao_componente,'0')     ||
           ' - '                       || ws_sit_comp_n               ||
           chr(10);

      if :new.nivel_componente = '2'
      then
         long_aux := long_aux ||
         inter_fn_buscar_tag('lb31254#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.cnpj_cliente9,'000000000')     || '/' ||
                                          to_char(:old.cnpj_cliente4,'0000')     || '-' ||
                                          to_char(:old.cnpj_cliente2,'00')       ||
           ' - '                       || ws_nome_cliente_o                      ||
           '-> '                       || to_char(:new.cnpj_cliente9,'000000000')     || '/' ||
                                           to_char(:new.cnpj_cliente4,'0000')     || '-' ||
                                           to_char(:new.cnpj_cliente2,'00')     ||
           ' - '                       || ws_nome_cliente_n                     ||
           chr(10);
      end if;

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.codigo_projeto    ||
           ' - '                       || ws_desc_projeto_o      ||
           chr(10)                                        ||
         inter_fn_buscar_tag('lb05455#SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.sequencia_projeto ||
           ' - '                       || ws_cod_tipo_produto_o  ||
           ' - '                       || ws_tipo_produto_o      ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.sequencia_atividade    ||
           ' - '                       || :old.codigo_atividade       ||
           ' - '                       || ws_desc_atividade_o         ||
           ' -> '                      ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb31305#RESPONSAVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_cod_responsavel_o
                                       || ' - ' || ws_nome_responsavel_o ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb12295#CARGO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_cod_cargo_o
                                       || ' - ' || ws_desc_cargo_o ||
           chr(10)                     ||
           chr(10);

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.codigo_projeto    ||
           ' - '                       || ws_desc_projeto_n      ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb05455#SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_projeto ||
           ' - '                       || ws_cod_tipo_produto_n  ||
           ' - '                       || ws_tipo_produto_n      ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_atividade    ||
           ' - '                       || :new.codigo_atividade       ||
           ' - '                       || ws_desc_atividade_n         ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb31305#RESPONSAVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_cod_responsavel_n
                                       || ' - ' || ws_nome_responsavel_n ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb12295#CARGO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_cod_cargo_n
                                       || ' - ' || ws_desc_cargo_n ||
           chr(10)                     ||
           chr(10);


      long_aux := long_aux ||
         inter_fn_buscar_tag('lb34958#DATA APROV/REPROV:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.data_aprov_reprov   ,'DD/MM/YYYY')  ||
           ' -> '                      || to_char(:new.data_aprov_reprov   ,'DD/MM/YYYY')
                                       || chr(10);

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb34959#HORA APROV/REPROV:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.hora_aprov_reprov,'HH24:MI')  ||
           ' -> '                      || to_char(:new.hora_aprov_reprov,'HH24:MI')
                                       || chr(10);

      conta := to_number((4000 - length(long_aux)) / 2);

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb00259#OBSERVACAO::',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || substr(:old.observacao,1,conta - 20)  ||
           ' -> '                      || substr(:new.observacao,1,conta - 20)
                                       || chr(10);
      INSERT INTO hist_100
         ( tabela,               operacao,
           data_ocorr,           aplicacao,
           usuario_rede,         maquina_rede,
           str01,                num05,
           long01,
           str07,                str08,
           str09,                str10,
           num07,                num08,
           num09,                num10
         )
      VALUES
         ( 'BASI_028',                'A',
           sysdate,                   ws_aplicativo,
           ws_usuario_rede,           ws_maquina_rede,
           :new.codigo_projeto,       :new.sequencia_projeto,
           long_aux,
           :new.nivel_componente,     :new.grupo_componente,
           :new.subgrupo_componente,  :new.item_componente,
           :new.cnpj_cliente9,        :new.cnpj_cliente4,
           :new.cnpj_cliente2,        :new.situacao_componente
         );
   end if;

   if updating and :new.tipo_aprovacao  = 2   --APROVACAO POR COMBINACAO
   then
      begin
         select basi_001.descricao_projeto, basi_001.codigo_responsavel
         into ws_desc_projeto_o, ws_resp_projeto_o
         from basi_001
         where basi_001.codigo_projeto = :old.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_o := '';
         ws_resp_projeto_o := 0;
      end;

      begin
         select basi_001.descricao_projeto, basi_001.codigo_responsavel
         into ws_desc_projeto_n, ws_resp_projeto_n
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_n := '';
         ws_resp_projeto_n := 0;
      end;

      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_o
         from basi_101
         where basi_101.codigo_projeto    = :old.codigo_projeto
           and basi_101.sequencia_projeto = :old.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_o := '';
      end;

      begin
         select basi_003.descricao
         into ws_tipo_produto_o
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_o;
      exception when no_data_found then
         ws_tipo_produto_o := '';
      end;

      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = :new.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_n := '';
      end;

      begin
         select basi_003.descricao
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n;
      exception when no_data_found then
         ws_tipo_produto_n := '';
      end;

      begin
         select basi_103.codigo_responsavel, basi_103.codigo_cargo
         into ws_cod_responsavel_o, ws_cod_cargo_o
         from basi_103
         where basi_103.codigo_projeto      = :old.codigo_projeto
           and basi_103.sequencia_projeto   = :old.sequencia_projeto
           and basi_103.codigo_atividade    = :old.codigo_atividade
           and basi_103.sequencia_atividade = :old.sequencia_atividade;
      exception when no_data_found then
         ws_cod_responsavel_o := 0;
         ws_cod_cargo_o       := 0;
      end;

      if ws_cod_responsavel_o = 0
      then ws_cod_responsavel_o := ws_resp_projeto_o;
      end if;

      begin
         select efic_050.nome
         into ws_nome_responsavel_o
         from efic_050
         where efic_050.cod_empresa     = ws_empresa
           and efic_050.cod_funcionario = ws_cod_responsavel_o;
      exception when no_data_found then
         ws_nome_responsavel_o := '';
      end;

      begin
         select mqop_120.descricao
         into ws_desc_cargo_o
         from mqop_120
         where mqop_120.codigo_cargo = ws_cod_cargo_o;
      exception when no_data_found then
         ws_desc_cargo_o := '';
      end;

      begin
         select basi_103.codigo_responsavel, basi_103.codigo_cargo
         into ws_cod_responsavel_n, ws_cod_cargo_n
         from basi_103
         where basi_103.codigo_projeto      = :new.codigo_projeto
           and basi_103.sequencia_projeto   = :new.sequencia_projeto
           and basi_103.codigo_atividade    = :new.codigo_atividade
           and basi_103.sequencia_atividade = :new.sequencia_atividade;
      exception when no_data_found then
         ws_cod_responsavel_n := 0;
         ws_cod_cargo_n       := 0;
      end;

      if ws_cod_responsavel_n = 0
      then ws_cod_responsavel_n := ws_resp_projeto_n;
      end if;

      begin
         select efic_050.nome
         into ws_nome_responsavel_n
         from efic_050
         where efic_050.cod_empresa     = ws_empresa
           and efic_050.cod_funcionario = ws_cod_responsavel_n;
      exception when no_data_found then
         ws_nome_responsavel_n := '';
      end;

      begin
         select mqop_120.descricao
         into ws_desc_cargo_n
         from mqop_120
         where mqop_120.codigo_cargo = ws_cod_cargo_n;
      exception when no_data_found then
         ws_desc_cargo_n := '';
      end;

      begin
         select pedi_010.nome_cliente
         into ws_nome_cliente_n
         from pedi_010
         where pedi_010.cgc_9 = :new.cnpj_cliente9
           and pedi_010.cgc_4 = :new.cnpj_cliente4
           and pedi_010.cgc_2 = :new.cnpj_cliente2;
      exception when no_data_found then
         ws_nome_cliente_n := '';
      end;

      begin
         select pedi_010.nome_cliente
         into ws_nome_cliente_o
         from pedi_010
         where pedi_010.cgc_9 = :old.cnpj_cliente9
           and pedi_010.cgc_4 = :old.cnpj_cliente4
           and pedi_010.cgc_2 = :old.cnpj_cliente2;
      exception when no_data_found then
         ws_nome_cliente_o := '';
      end;

      begin
         select basi_002.descricao
         into ws_desc_atividade_o
         from basi_002
         where basi_002.codigo_atividade = :old.codigo_atividade;
      exception when no_data_found then
         ws_desc_atividade_o := '';
      end;

      begin
         select basi_002.descricao
         into ws_desc_atividade_n
         from basi_002
         where basi_002.codigo_atividade = :new.codigo_atividade;
      exception when no_data_found then
         ws_desc_atividade_n := '';
      end;

      begin
         select basi_843.descr_combinacao, basi_843.alternativa_comb,
                basi_843.roteiro_comb
         into ws_desc_comb, ws_alt_comb,
              ws_rot_comb
         from basi_843
         where basi_843.codigo_projeto  = :new.codigo_projeto
           and basi_843.combinacao_item = :new.item_componente;
      exception when no_data_found then
         ws_desc_comb := '';
         ws_alt_comb  := 0;
         ws_rot_comb  := 0;
      end;

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb34960#APROVACAO/REPROVACAO DE COMBINACOES DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) ||
                  chr(10)                    ||
                  chr(10);

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb30204#COMBINACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.item_componente      ||
           ' - '                       || ws_desc_comb              ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb00442#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_alt_comb               ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb08191#ROTEIRO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_rot_comb               ||
           chr(10);

      if :old.situacao_componente = 0 then ws_sit_comp_o := inter_fn_buscar_tag('lb31563#PENDENTE',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :old.situacao_componente = 1 then ws_sit_comp_o := inter_fn_buscar_tag('lb04901#ENVIADA',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :old.situacao_componente = 2 then ws_sit_comp_o := inter_fn_buscar_tag('lb23991#APROVADA',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :old.situacao_componente = 3 then ws_sit_comp_o := inter_fn_buscar_tag('lb34108#COM OBSERVACOES',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :old.situacao_componente = 4 then ws_sit_comp_o := inter_fn_buscar_tag('lb11294#REJEITADA',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :old.situacao_componente = 5 then ws_sit_comp_o := inter_fn_buscar_tag('lb08202#CANCELADA',ws_locale_usuario,ws_usuario_systextil);  end if;

      if :new.situacao_componente = 0 then ws_sit_comp_n := inter_fn_buscar_tag('lb31563#PENDENTE',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :new.situacao_componente = 1 then ws_sit_comp_n := inter_fn_buscar_tag('lb04901#ENVIADA',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :new.situacao_componente = 2 then ws_sit_comp_n := inter_fn_buscar_tag('lb23991#APROVADA',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :new.situacao_componente = 3 then ws_sit_comp_n := inter_fn_buscar_tag('lb34108#COM OBSERVACOES',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :new.situacao_componente = 4 then ws_sit_comp_n := inter_fn_buscar_tag('lb11294#REJEITADA',ws_locale_usuario,ws_usuario_systextil);  end if;
      if :new.situacao_componente = 5 then ws_sit_comp_n := inter_fn_buscar_tag('lb08202#CANCELADA',ws_locale_usuario,ws_usuario_systextil);  end if;

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.situacao_componente,'0')     ||
           ' - '                       || ws_sit_comp_o               ||
           ' -> '                      || to_char(:new.situacao_componente,'0')     ||
           ' - '                       || ws_sit_comp_n               ||
           chr(10);

      if :new.nivel_componente = '2'
      then
         long_aux := long_aux ||
            inter_fn_buscar_tag('lb31254#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.cnpj_cliente9,'000000000')     || '/' ||
                                          to_char(:old.cnpj_cliente4,'0000')     || '-' ||
                                          to_char(:old.cnpj_cliente2,'00')       ||
           ' - '                       || ws_nome_cliente_o                      ||
           '-> '                       || to_char(:new.cnpj_cliente9,'000000000')     || '/' ||
                                           to_char(:new.cnpj_cliente4,'0000')     || '-' ||
                                           to_char(:new.cnpj_cliente2,'00')     ||
           ' - '                       || ws_nome_cliente_n                     ||
           chr(10);
      end if;

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.codigo_projeto    ||
           ' - '                       || ws_desc_projeto_o      ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb05455#SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.sequencia_projeto ||
           ' - '                       || ws_cod_tipo_produto_o  ||
           ' - '                       || ws_tipo_produto_o      ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :old.sequencia_atividade    ||
           ' - '                       || :old.codigo_atividade       ||
           ' - '                       || ws_desc_atividade_o         ||
           ' -> '                      ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb31305#RESPONSAVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_cod_responsavel_o
                                       || ' - ' || ws_nome_responsavel_o ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb12295#CARGO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_cod_cargo_o
                                       || ' - ' || ws_desc_cargo_o ||
           chr(10)                     ||
           chr(10);

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.codigo_projeto    ||
           ' - '                       || ws_desc_projeto_n      ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb05455#SEQUENCIA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_projeto ||
           ' - '                       || ws_cod_tipo_produto_n  ||
           ' - '                       || ws_tipo_produto_n      ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb04066#ATIVIDADE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.sequencia_atividade    ||
           ' - '                       || :new.codigo_atividade       ||
           ' - '                       || ws_desc_atividade_n         ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb31305#RESPONSAVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_cod_responsavel_n
                                       || ' - ' || ws_nome_responsavel_n ||
           chr(10)                     ||
         inter_fn_buscar_tag('lb12295#CARGO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_cod_cargo_n
                                       || ' - ' || ws_desc_cargo_n ||
           chr(10)                     ||
           chr(10);


      long_aux := long_aux ||
         inter_fn_buscar_tag('lb34958#DATA APROV/REPROV:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.data_aprov_reprov   ,'DD/MM/YYYY')  ||
           ' -> '                      || to_char(:new.data_aprov_reprov   ,'DD/MM/YYYY')
                                       || chr(10);

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb34959#HORA APROV/REPROV:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.hora_aprov_reprov,'HH24:MI')  ||
           ' -> '                      || to_char(:new.hora_aprov_reprov,'HH24:MI')
                                       || chr(10);

      conta := to_number((4000 - length(long_aux)) / 2);

      long_aux := long_aux ||
         inter_fn_buscar_tag('lb00259#OBSERVACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || substr(:old.observacao,1,conta - 20)  ||
           ' -> '                      || substr(:new.observacao,1,conta - 20)
                                       || chr(10);
      INSERT INTO hist_100
         ( tabela,               operacao,
           data_ocorr,           aplicacao,
           usuario_rede,         maquina_rede,
           str01,                num05,
           long01,
           str07,                str08,
           str09,                str10,
           num07,                num08,
           num09,                num10
         )
      VALUES
         ( 'BASI_843',                'A',
           sysdate,                   ws_aplicativo,
           ws_usuario_rede,           ws_maquina_rede,
           :new.codigo_projeto,       :new.sequencia_projeto,
           long_aux,
           :new.nivel_componente,     :new.grupo_componente,
           :new.subgrupo_componente,  :new.item_componente,
           :new.cnpj_cliente9,        :new.cnpj_cliente4,
           :new.cnpj_cliente2,        :new.situacao_componente
         );
   end if;

   if deleting  and :old.tipo_aprovacao  = 1   --APROVACAO POR COMPONENTE
   then
      INSERT INTO hist_100
         ( tabela,                    operacao,
           data_ocorr,                aplicacao,
           usuario_rede,              maquina_rede,

           str01,                     num05,
           str07,                     str08,
           str09,                     str10,

           num07,                     num08,
           num09,                     num10
         )
      VALUES
         ( 'BASI_028',                'D',
           sysdate,                   ws_aplicativo,
           ws_usuario_rede,           ws_maquina_rede,

           :old.codigo_projeto,       :old.sequencia_projeto,
           :old.nivel_componente,     :old.grupo_componente,
           :old.subgrupo_componente,  :old.item_componente,

           :old.cnpj_cliente9,        :old.cnpj_cliente4,
           :old.cnpj_cliente2,        :old.situacao_componente
        );
   end if;

end inter_tr_basi_028_log;

-- ALTER TRIGGER "INTER_TR_BASI_028_LOG" ENABLE
 

/

exec inter_pr_recompile;

