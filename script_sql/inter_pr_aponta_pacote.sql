
  CREATE OR REPLACE PROCEDURE "INTER_PR_APONTA_PACOTE" (
   p_empresa          in number,
   p_codigo_usuario   in number,
   p_ordem_producao   in number,
   p_ordem_confeccao  in number,
   p_codigo_estagio   in number,
   p_qualidade        in number,
   p_qtde_pecas       in number

)
is
   ws_usuario_rede                 varchar2(20);
   ws_maquina_rede                 varchar2(40);
   ws_aplicativo                   varchar2(20);
   ws_sid                          number(9);
   ws_empresa                      number(3);
   ws_usuario_systextil            varchar2(250);
   ws_locale_usuario               varchar2(5);

   v_periodo_producao              number;
   v_referencia_peca               varchar2(5);
   v_alternativa_peca              number;
   v_roteiro_peca                  number;
   v_ultimo_estagio                number;
   v_produziu_lote                 varchar2(1);
   v_sequencia_045                 number;
   v_qtde_produzida                number;
   v_qtde_pecas_2a                 number;
   v_qtde_conserto                 number;
   v_qtde_perdas                   number;
   v_turno_producao                number;
   v_sequencia                     number;
   v_tem_registro                  number;
   v_codigo_familia                number;

/*
   #################################################################################################
   Busca a proxima sequencia da pcpc_045
   #################################################################################################
*/
PROCEDURE f_get_pcpc_045_sequencia (
    p_periodo_ordem_seq045   in number,
    p_ordem_confeccao_seq045 in number,
    p_codigo_estagio_seq045  in number,
    r_sequencia_045          out number)
IS
BEGIN
   r_sequencia_045 := 0;

   select nvl(max(pcpc_045.sequencia),0) + 1
   into   r_sequencia_045
   from pcpc_045
   where pcpc_045.pcpc040_perconf = p_periodo_ordem_seq045
     and pcpc_045.pcpc040_ordconf = p_ordem_confeccao_seq045
     and pcpc_045.pcpc040_estconf = p_codigo_estagio_seq045;

end f_get_pcpc_045_sequencia;

/*
   #################################################################################################
   Busca o turno de produ�ao do centro de custo padrao(0)
   #################################################################################################
*/
PROCEDURE f_get_basi_185_turno (
    r_turno out number)
IS
   hinit1               date;
   hfimt1               date;
   hinit2               date;
   hfimt2               date;
   hinit3               date;
   hfimt3               date;
   hinit4               date;
   hfimt4               date;
BEGIN
   begin
      select basi_185.horaini_t1,  basi_185.horafim_t1,
             basi_185.horaini_t2,  basi_185.horafim_t2,
             basi_185.horaini_t3,  basi_185.horafim_t3,
             basi_185.horaini_t4,  basi_185.horafim_t4
      into   hinit1,               hfimt1,
             hinit2,               hfimt2,
             hinit3,               hfimt3,
             hinit4,               hfimt4
      from basi_185
      where basi_185.centro_custo = 0;
   end;

   if  to_CHAR(sysdate,'hh24:mi') >= to_CHAR(hinit4,'hh24:mi')
   and to_CHAR(sysdate,'hh24:mi') <= to_CHAR(hfimt4,'hh24:mi')
   then
      r_turno := 4;
   end if;

   if  to_CHAR(sysdate,'hh24:mi') >= to_CHAR(hinit3,'hh24:mi')
   and to_CHAR(sysdate,'hh24:mi') <= to_CHAR(hfimt3,'hh24:mi')
   then
      r_turno := 3;
   end if;

   if  to_CHAR(sysdate,'hh24:mi') >= to_CHAR(hinit2,'hh24:mi')
   and to_CHAR(sysdate,'hh24:mi') <= to_CHAR(hfimt2,'hh24:mi')
   then
      r_turno := 2;
   end if;

   if  to_CHAR(sysdate,'hh24:mi') >= to_CHAR(hinit1,'hh24:mi')
   and to_CHAR(sysdate,'hh24:mi') <= to_CHAR(hfimt1,'hh24:mi')
   then
      r_turno := 1;
   end if;

   if r_turno is null
   then
      r_turno := 0;
   end if;

end f_get_basi_185_turno;

/*
   #######################
   PROCESSAMENTO PRINCIPAL
   #######################
*/
begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   begin
      select   pcpc_020.periodo_producao,            pcpc_020.referencia_peca,
               pcpc_020.alternativa_peca,            pcpc_020.roteiro_peca,
               pcpc_020.ultimo_estagio
      into     v_periodo_producao,                   v_referencia_peca,
               v_alternativa_peca,                   v_roteiro_peca,
               v_ultimo_estagio
      from pcpc_020
      where pcpc_020.ordem_producao = p_ordem_producao;
   exception when others then
      raise_application_error(-20000,'ATEN��O! Ordem de produ��o n�o cadastrada.');
   end;

   for reg_apontar in (select pcpc_040.periodo_producao,
                              pcpc_040.ordem_confeccao,
                              pcpc_040.codigo_estagio,
                              pcpc_040.qtde_disponivel_baixa   as qtde_produzida,
                              pcpc_040.proconf_nivel99,
                              pcpc_040.proconf_grupo,
                              pcpc_040.proconf_subgrupo,
                              pcpc_040.proconf_item
                    from pcpc_040
                    where pcpc_040.ordem_producao   = p_ordem_producao
                      and pcpc_040.ordem_confeccao  = p_ordem_confeccao
                      and pcpc_040.codigo_estagio   = p_codigo_estagio)
   loop

      if reg_apontar.qtde_produzida > 0
      then
         v_produziu_lote := 's';

         begin
            update pcpc_020
            set   pcpc_020.situacao       = 4
            where pcpc_020.ordem_producao = p_ordem_producao;
         end;
      end if;

      begin
        select  mqop_050.codigo_familia
        into    v_codigo_familia
        from mqop_040, mqop_050
        where mqop_050.nivel_estrutura   = reg_apontar.proconf_nivel99
          and mqop_050.grupo_estrutura   = reg_apontar.proconf_grupo
          and (mqop_050.subgru_estrutura = reg_apontar.proconf_subgrupo or mqop_050.subgru_estrutura = '000')
          and (mqop_050.item_estrutura   = reg_apontar.proconf_item or mqop_050.item_estrutura = '000000')
          and mqop_050.numero_alternati  = v_alternativa_peca
          and mqop_050.numero_roteiro    = v_roteiro_peca
          and mqop_050.codigo_operacao   = mqop_040.codigo_operacao
          and mqop_050.codigo_estagio    = p_codigo_estagio
        order by mqop_050.seq_operacao      ASC,
                 mqop_050.sequencia_estagio ASC,
                 mqop_050.estagio_depende   ASC;
      exception when others then
        v_codigo_familia := 0;
      end;

      f_get_pcpc_045_sequencia (reg_apontar.periodo_producao,           reg_apontar.ordem_confeccao,
                                reg_apontar.codigo_estagio,   v_sequencia_045);

      v_sequencia := v_sequencia + 1;

      v_qtde_produzida := 0;
      v_qtde_pecas_2a  := 0;
      v_qtde_conserto  := 0;
      v_qtde_perdas    := 0;

      if p_qualidade = 1
      then
         v_qtde_produzida := p_qtde_pecas;
         v_qtde_pecas_2a  := 0;
         v_qtde_conserto  := 0;
         v_qtde_perdas    := 0;
      end if;

      if p_qualidade = 2
      then
         v_qtde_produzida := 0;
         v_qtde_pecas_2a  := p_qtde_pecas;
         v_qtde_conserto  := 0;
         v_qtde_perdas    := 0;
      end if;

      if p_qualidade = 3
      then
         v_qtde_produzida := 0;
         v_qtde_pecas_2a  := 0;
         v_qtde_conserto  := p_qtde_pecas;
         v_qtde_perdas    := 0;
      end if;

      if p_qualidade = 4
      then
         v_qtde_produzida := 0;
         v_qtde_pecas_2a  := 0;
         v_qtde_conserto  := 0;
         v_qtde_perdas    := p_qtde_pecas;
      end if;

      f_get_basi_185_turno (v_turno_producao);

      begin
         insert into pcpc_045 (
            pcpc040_ordconf,               pcpc040_perconf,
            pcpc040_estconf,               sequencia,
            data_producao,                 hora_producao,

            qtde_produzida,                qtde_pecas_2a,
            qtde_conserto,                 qtde_perdas,

            turno_producao,
            codigo_usuario,                codigo_familia,
            ordem_producao,                codigo_intervalo,
            executa_trigger
         ) values (
            reg_apontar.ordem_confeccao,   reg_apontar.periodo_producao,
            reg_apontar.codigo_estagio,    v_sequencia_045,
            sysdate,                       sysdate,

            v_qtde_produzida,              v_qtde_pecas_2a,
            v_qtde_conserto,               v_qtde_perdas,

            v_turno_producao,
            p_codigo_usuario,              v_codigo_familia,
            p_ordem_producao,              0,
            3
         );
      end;
   end loop;

   inter_pr_conf_parte_pc (p_ordem_producao, p_ordem_confeccao);

   inter_pr_avance_parte_pc (p_ordem_producao,   p_ordem_confeccao,
                             p_codigo_estagio);

   begin
      select nvl(1,0)
      into v_tem_registro
      from pcpc_040
      where pcpc_040.ordem_producao           = p_ordem_producao
        and pcpc_040.ordem_confeccao          = p_ordem_confeccao
        and (pcpc_040.qtde_em_producao_pacote < 0
        or   pcpc_040.qtde_a_produzir_pacote  < 0
        or   pcpc_040.qtde_disponivel_baixa   < 0);
   exception when others then
      v_tem_registro := 0;
   end;

   if v_tem_registro > 0
   then
      raise_application_error(-20000,inter_fn_buscar_tag('ds31041#ATEN��O! Quantidade em produ��o ou a produzir ou disponivel baixa na ordem de produ��o ficaria negativa, a opera��o ser� abortada.',ws_locale_usuario,ws_usuario_systextil));
   end if;

end inter_pr_aponta_pacote;

 

/

exec inter_pr_recompile;

