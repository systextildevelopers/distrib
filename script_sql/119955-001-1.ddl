alter table hdoc_030 
add (libera_ap_01 number(1) default 0,
     libera_ap_02 number(1) default 0,
     libera_ap_03 number(1) default 0,
     libera_ap_04 number(1) default 0);
   
comment on column hdoc_030.libera_ap_01 is 'Par�metro de permiss�o de Apontamento de coletor - 01';
comment on column hdoc_030.libera_ap_02 is 'Par�metro de permiss�o de Apontamento de coletor - 02';
comment on column hdoc_030.libera_ap_03 is 'Par�metro de permiss�o de Apontamento de coletor - 03';
comment on column hdoc_030.libera_ap_04 is 'Par�metro de permiss�o de Apontamento de coletor - 04';

/
