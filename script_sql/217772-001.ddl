insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('inte_p072', 'Pedidos para Coleta (Pronta Entrega)', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'inte_p072', 'menu_p001', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'inte_p072', 'menu_p001', 1, 0, 'S', 'S', 'S', 'S');


update hdoc_036
   set hdoc_036.descricao       = 'Órdenes de Cobro (Pronta Entrega)'
 where hdoc_036.codigo_programa = 'inte_p072'
   and hdoc_036.locale          = 'es_ES';
