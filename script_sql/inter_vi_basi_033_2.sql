create or replace view inter_vi_basi_033_2 as
select distinct basi_113.nivel_item,
       basi_113.grupo_item,
       basi_113.subgru_item,
       basi_113.item_item,
       basi_113.alternativa_produto alternativa_item,
       basi_113.sequencia_estrutura sequencia,
       basi_119.descricao_parte
 from basi_113, basi_119
where basi_113.nivel_item           = '1'
and basi_113.nivel_item = basi_119.nivel_item
and basi_113.grupo_item = basi_119.grupo_item
and basi_113.subgru_item = basi_119.subgru_item
and basi_113.item_item = basi_119.item_item
and basi_113.alternativa_produto = basi_119.alternativa_produto
and basi_113.sequencia_estrutura = basi_119.sequencia_estrutura
and basi_113.nivel_comp = '2';


/

exec inter_pr_recompile;
