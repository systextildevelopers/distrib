create or replace trigger inter_tr_obrf_089_1
     before insert
         or delete
         or update of situacao_item
     on obrf_089
     for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_periodo_producao            number;
   v_ordem_confeccao             number;
   v_codigo_estagio              number;
   v_valida_os_conserto_retorno  number;
   
begin
   /************************************************************************************************************/
   -- ANOTAÃ¿Ã¿ES PARA MANUTENÃ¿Ã¿O E ENTENDIMENTO DA TRIGGER PARA O CONSERTO.
   --
   -- Quando o valor da tabela no campo "tag_atualiza_conserto" for igual a Zero, 
   -- a trigger executa as atualizaÃ§Ãµes no pacote de produÃ§Ã£o, senÃ£o nÃ£o executa.
   --
   -- A trigger ATUALIZA O ESTÃ¿GIO QUE IDENTIFICOU O CONSERTO! O estÃ¡gio de envio do conserto
   -- nÃ£o Ã© atualizado por esta trigger.
   --
   /************************************************************************************************************/

   -- Dados do usuÃ¡rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_periodo_producao := 0;
   v_ordem_confeccao  := 0;
   v_codigo_estagio   := 0;

   
   
   /*********************************************************************************************************/
   -- INICIO - CARREGA OS VALORES DO CÃ¿DIGO DE BARRAS QUE ESTÃ¿ SENDO LIDO PARA CONSERTO.
   if  inserting
   or  updating
   then
      if length(:new.codigo_barras) = 28
      then
         v_periodo_producao := to_number(substr(:new.codigo_barras,16,4));
         v_ordem_confeccao  := to_number(substr(:new.codigo_barras,20,5));

         :new.periodo_producao    := to_number(substr(:new.codigo_barras,16,4));
         :new.ordem_confeccao     := to_number(substr(:new.codigo_barras,20,5));
         :new.seq_ordem_confeccao := to_number(substr(:new.codigo_barras,25,4));

         :new.nivel_estrutura     := substr(:new.codigo_barras,1,1);
         :new.grupo_estrutura     := substr(:new.codigo_barras,2,5);
         :new.subgrupo_estrutura  := substr(:new.codigo_barras,7,3);
         :new.item_estrutura      := substr(:new.codigo_barras,10,6);

         begin
            select pcpc_040.ordem_producao
            into :new.ordem_producao
            from pcpc_040
            where pcpc_040.periodo_producao = :new.periodo_producao
              and pcpc_040.ordem_confeccao  = :new.ordem_confeccao
              and pcpc_040.estagio_anterior = 0
              and rownum                    < 2;
         exception when others then
            :new.ordem_producao := 0;
         end;
      end if;

      if length(:new.codigo_barras) = 25
      then
         v_periodo_producao       := to_number(substr(:new.codigo_barras,13,4));
         v_ordem_confeccao        := to_number(substr(:new.codigo_barras,17,5));

         :new.periodo_producao    := to_number(substr(:new.codigo_barras,13,4));
         :new.ordem_confeccao     := to_number(substr(:new.codigo_barras,17,5));
         :new.seq_ordem_confeccao := to_number(substr(:new.codigo_barras,22,4));

         :new.nivel_estrutura     := substr(:new.codigo_barras,1,1);
         :new.grupo_estrutura     := substr(:new.codigo_barras,2,5);
         :new.subgrupo_estrutura  := substr(:new.codigo_barras,7,2);
         :new.item_estrutura      := substr(:new.codigo_barras,9,4);

         begin
            select pcpc_040.ordem_producao
            into :new.ordem_producao
            from pcpc_040
            where pcpc_040.periodo_producao = :new.periodo_producao
              and pcpc_040.ordem_confeccao  = :new.ordem_confeccao
              and pcpc_040.estagio_anterior = 0
              and rownum                    < 2;
         exception when others then
            :new.ordem_producao := 0;
         end;
      end if;

      if length(:new.codigo_barras) = 22
      then
         v_periodo_producao := to_number(substr(:new.codigo_barras,1,4));
         v_ordem_confeccao  := to_number(substr(:new.codigo_barras,14,5));

         :new.periodo_producao    := to_number(substr(:new.codigo_barras,1,4));
         :new.ordem_producao      := to_number(substr(:new.codigo_barras,5,9));
         :new.ordem_confeccao     := to_number(substr(:new.codigo_barras,14,5));
         :new.seq_ordem_confeccao := to_number(substr(:new.codigo_barras,19,4));

         begin
            select pcpc_040.proconf_nivel99,        pcpc_040.proconf_grupo,
                   pcpc_040.proconf_subgrupo,       pcpc_040.proconf_item
            into   :new.nivel_estrutura,            :new.grupo_estrutura,
                   :new.subgrupo_estrutura,         :new.item_estrutura
            from pcpc_040
            where pcpc_040.periodo_producao = :new.periodo_producao
              and pcpc_040.ordem_confeccao  = :new.ordem_confeccao
              and pcpc_040.estagio_anterior = 0
              and rownum                    < 2;
         exception when others then
            :new.ordem_producao := 0;
         end;
      end if;
   else
      if length(:old.codigo_barras) = 28
      then
         v_periodo_producao := to_number(substr(:old.codigo_barras,16,4));
         v_ordem_confeccao  := to_number(substr(:old.codigo_barras,20,5));
      end if;

      if length(:old.codigo_barras) = 25
      then
         v_periodo_producao := to_number(substr(:old.codigo_barras,13,4));
         v_ordem_confeccao  := to_number(substr(:old.codigo_barras,17,5));
      end if;

      if length(:old.codigo_barras) = 22
      then
         v_periodo_producao := to_number(substr(:old.codigo_barras,1,4));
         v_ordem_confeccao  := to_number(substr(:old.codigo_barras,14,5));
      end if;
   end if;
   
   -- FINAL - CARREGA OS VALORES DO CÃ¿DIGO DE BARRAS QUE ESTÃ¿ SENDO LIDO PARA CONSERTO.
   /*********************************************************************************************************/
   
   
   -- IncluÃ­ndo TAG, estamos enviando a peÃ§a para conserto...
   if  inserting
   and :new.numero_solicitacao    <> 0 -- Testa o numero da solicitacao de conserto.
   and :new.tag_atualiza_conserto =  0 -- Flag da tabela para executar a rotina.
   then
      -- Busca o estÃ¡gio que identificou o conserto para ESTORNAR da quantidade disponÃ­vel
      -- a peÃ§a que estÃ¡ sendo enviada para conserto.
      begin
         select pcpc_888.cod_est_ident_cons
         into   v_codigo_estagio
         from pcpc_888
         where pcpc_888.numero_solicitacao = :new.numero_solicitacao;
      exception when others then
         v_codigo_estagio := 0;
      end;

      begin
         update pcpc_040
         set   pcpc_040.executa_trigger         = 3, 
               pcpc_040.qtde_disponivel_baixa   = pcpc_040.qtde_disponivel_baixa - 1
         where pcpc_040.periodo_producao        = v_periodo_producao
           and pcpc_040.ordem_confeccao         = v_ordem_confeccao
           and pcpc_040.codigo_estagio          = v_codigo_estagio;
      exception when others then
         raise_application_error(-20000,'Erro update pcpc_040 (1): ' || sqlerrm);
      end;
   end if;

   -- Atualizando o TAG que foi enviado para o conserto.
   -- Estamos mudando a situaÃ§Ã£o do TAG de 0 - Enviado para 1 - Retornado do conserto.
   if  updating
   and :new.numero_solicitacao    <> 0
   and :new.situacao_item         <> :old.situacao_item
   and :new.situacao_item         =  1
   and :old.situacao_item         =  0
   and :new.tag_atualiza_conserto =  0 -- Flag da tabela para executar a rotina.
   then
      begin
         select pcpc_888.cod_est_ident_cons
         into   v_codigo_estagio
         from pcpc_888
         where pcpc_888.numero_solicitacao = :new.numero_solicitacao;
      exception when others then
         v_codigo_estagio := 0;
      end;

      -- Quando este TAG retorna, ou seja, passou a situaÃ§Ã£o do item 0 (zero) para
      -- 1 (um), o sistema devolve para o estÃ¡gio que identificou o conserto
      -- uma peÃ§a como disponÃ­vel para baixa.
      begin
         update pcpc_040
         set   pcpc_040.executa_trigger         = 3,
               pcpc_040.qtde_disponivel_baixa   = pcpc_040.qtde_disponivel_baixa + 1
         where pcpc_040.periodo_producao        = v_periodo_producao
           and pcpc_040.ordem_confeccao         = v_ordem_confeccao
           and pcpc_040.codigo_estagio          = v_codigo_estagio;
      exception when others then
         raise_application_error(-20000,'Erro update pcpc_040 (2): ' || sqlerrm);
      end;
   end if;

   if  updating
   and :new.numero_solicitacao    <> 0
   and :new.situacao_item         <> :old.situacao_item
   and :new.situacao_item         =  0
   and :old.situacao_item         =  1
   and :new.tag_atualiza_conserto =  0 -- Flag da tabela para executar a rotina.
   then
      if length(:new.codigo_barras) = 28
      then
         v_periodo_producao := to_number(substr(:new.codigo_barras,16,4));
         v_ordem_confeccao  := to_number(substr(:new.codigo_barras,20,5));
      end if;

      if length(:new.codigo_barras) = 25
      then
         v_periodo_producao := to_number(substr(:new.codigo_barras,13,4));
         v_ordem_confeccao  := to_number(substr(:new.codigo_barras,17,5));
      end if;

      if length(:new.codigo_barras) = 22
      then
         v_periodo_producao := to_number(substr(:new.codigo_barras,1,4));
         v_ordem_confeccao  := to_number(substr(:new.codigo_barras,14,5));
      end if;

      begin
         select pcpc_888.cod_est_ident_cons
         into   v_codigo_estagio
         from pcpc_888
         where pcpc_888.numero_solicitacao = :new.numero_solicitacao;
      exception when others then
         v_codigo_estagio := 0;
      end;

      begin
         update pcpc_040
         set   pcpc_040.executa_trigger         = 3,
               pcpc_040.qtde_disponivel_baixa   = pcpc_040.qtde_disponivel_baixa - 1
         where pcpc_040.periodo_producao        = v_periodo_producao
           and pcpc_040.ordem_confeccao         = v_ordem_confeccao
           and pcpc_040.codigo_estagio          = v_codigo_estagio;
      exception when others then
         raise_application_error(-20000,'Erro update pcpc_040 (3): ' || sqlerrm);
      end;
   end if;

   -- Eliminando o TAG que foi enviado para o terceiro.
   if  deleting
   and :old.numero_solicitacao <> 0
   then
      begin
         select pcpc_888.cod_est_ident_cons
         into   v_codigo_estagio
         from pcpc_888
         where pcpc_888.numero_solicitacao = :old.numero_solicitacao;
      exception when others then
         v_codigo_estagio := 0;
      end;

      begin
         update pcpc_040
         set   pcpc_040.executa_trigger         = 3,
               pcpc_040.qtde_disponivel_baixa   = pcpc_040.qtde_disponivel_baixa + 1
         where pcpc_040.periodo_producao        = v_periodo_producao
           and pcpc_040.ordem_confeccao         = v_ordem_confeccao
           and pcpc_040.codigo_estagio          = v_codigo_estagio;
      exception when others then
         raise_application_error(-20000,'Erro update pcpc_040 (4): ' || sqlerrm);
      end;
   end if;

   -- Carrega o parÃ¢metro de empresa para saber se vai atualizar a quantidade da OP
   -- ou nÃ£o, QTDE_EM_PRODUCAO_PACOTE.
   begin  
      select empr_002.valida_os_conserto
      into v_valida_os_conserto_retorno
      from empr_002;
   end;

   if  updating
   and :new.numero_solicitacao      <> 0
   and :new.situacao_item           <> :old.situacao_item
   and :new.situacao_item           =  1
   and :old.situacao_item           =  0
   and :new.tag_atualiza_conserto   =  0 -- Flag da tabela para executar a rotina.
   and v_valida_os_conserto_retorno =  1
   and (:new.cod_cancelamento is null or :new.cod_cancelamento = 0)-- Não executa quando estivermos cancelando a OS
   then
      begin
         select pcpc_888.cod_est_ident_cons
         into   v_codigo_estagio
         from pcpc_888
         where pcpc_888.numero_solicitacao = :new.numero_solicitacao;
      exception when others then
         v_codigo_estagio := 0;
      end;
             
      begin
         update pcpc_040
         set   pcpc_040.executa_trigger         = 3,
               pcpc_040.qtde_em_producao_pacote = pcpc_040.qtde_em_producao_pacote + 1
         where pcpc_040.periodo_producao        = v_periodo_producao
           and pcpc_040.ordem_confeccao         = v_ordem_confeccao
           and pcpc_040.codigo_estagio          = v_codigo_estagio;
         exception when others then
           raise_application_error(-20000,'Erro update pcpc_040 (5): ' || sqlerrm);
      end;
   end if;

   -- AtualizaÃ§Ã£o do TAG, mudando ele de confirmado / retornado para a confirmar.
   if  updating
   and :new.numero_solicitacao      <> 0
   and :new.situacao_item           <> :old.situacao_item
   and :new.situacao_item           =  0
   and :old.situacao_item           =  1
   and :new.tag_atualiza_conserto   =  0 -- Flag da tabela para executar a rotina.
   and v_valida_os_conserto_retorno =  1
   then
      begin
         select pcpc_888.cod_est_ident_cons
         into   v_codigo_estagio
         from pcpc_888
         where pcpc_888.numero_solicitacao = :new.numero_solicitacao;
      exception when others then
         v_codigo_estagio := 0;
      end;

      begin
         update pcpc_040
         set   pcpc_040.executa_trigger         = 3,
               pcpc_040.qtde_em_producao_pacote = pcpc_040.qtde_em_producao_pacote - 1
         where pcpc_040.periodo_producao        = v_periodo_producao
           and pcpc_040.ordem_confeccao         = v_ordem_confeccao
           and pcpc_040.codigo_estagio          = v_codigo_estagio;
      exception when others then
         raise_application_error(-20000,'Erro update pcpc_040 (6): ' || sqlerrm);
      end;
   end if;
   
end inter_tr_obrf_089_1;
