alter table "MANU_002" add
("SEQUENCIA" NUMBER(2,0)); 

alter table "MANU_002" add
("DEPENDENCIA" NUMBER(2,0));  

alter table "MANU_002" add
("PRIORIDADE" NUMBER(2,0));

alter table "MANU_002" add
("HORAS_PREVISTAS" VARCHAR2(45));

alter table "MANU_002" add
("AUTORIZADA" VARCHAR2(1));

alter table "MANU_002" add
("CONCLUIDA" VARCHAR2(1));

alter table "MANU_002" add
("ORIENTACAO_TAREFA" CLOB);

alter table "MANU_002" add
("COMENTARIOS_EXECUCAO" CLOB);

create sequence "MANU_002_SEQ";
