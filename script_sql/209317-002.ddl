create table cobr_002
(
  usuSolic             VARCHAR2(15) default '',
  dtaSolic             DATE  ,
  usuAnalise           VARCHAR2(15) default '',
  dtaAnalise           DATE  ,
  sitAnalise           NUMBER(1) default 0,  
  descMotivoCanc       VARCHAR2(120),
  selecao              NUMBER(1),
  tipo                 NUMBER(1) default 0,
  empresa              NUMBER(3) default 0,
  cliente_cgc_cli9     NUMBER(9) default 0,
  cliente_cgc_cli4     NUMBER(4) default 0,
  cliente_cgc_cli2     NUMBER(2) default 0,
  tipo_titulo          NUMBER(2) default 0,
  duplicata            NUMBER(9) default 0,
  parcela              NUMBER(2) default 0,
  instrucao_interna    NUMBER(3) default 0,
  instrucao            NUMBER(2) default 0,
  valor_instrucao      NUMBER(14,2) default 0.00,
  ins_tipo_data        DATE,
  instr_protesto       NUMBER(2) default 0,
  nr_mtv_prorrogacao   NUMBER(2) default 0,
  imprimir_comunicado  NUMBER(1) default 0,
  data_baixa           DATE,   
  portador_dupl        NUMBER(3) default 0,
  situacao_dupl        NUMBER(1) default 0,
  instr_baixa          NUMBER(2) default 0,  
  data_cred_baixa      DATE ,
  vlr_pago_baixa       NUMBER(15,2) default 0.00,
  vlr_desc_baixa       NUMBER(15,2) default 0.00,
  vlr_juro_baixa       NUMBER(15,2) default 0.00,
  mensagem             VARCHAR(120) default '',
  nr_dias              NUMBER(2),
  mensagem_canc        VARCHAR(120) default '',
  nr_bloq_dias         NUMBER(9) default 0,
  calc_perc            NUMBER(5,2) default 0.00
);

COMMENT ON COLUMN cobr_002.sitAnalise IS 'Digitado = 0 ,  Aprovado =1,  Rejeitar = 2,  Cancelar = 3';
COMMENT ON COLUMN cobr_002.selecao IS '0 - n�o selecionado, 1 - selecionado ou 3 - processado';
COMMENT ON COLUMN cobr_002.tipo IS '0 - carteira ou 1 - cobran�a';
