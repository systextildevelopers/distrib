create or replace function inter_fn_dados_produto_nf(p_ind_cons in number, p_id_ret in number,       p_cod_empresa in number,
													 p_num_nota in number, p_serie_nota in varchar2, p_nivel in varchar2,
													 p_grupo in varchar2,  p_subgrupo in varchar2,   p_item in varchar2, 
													 p_descr_item in varchar2)
return varchar2 is
 
  cursor b_400 is
    select basi_400.codigo_informacao
    from basi_400
    where basi_400.tipo_informacao = 50
      and basi_400.nivel           = p_nivel
      and basi_400.grupo           = p_grupo
      and basi_400.subgrupo        = p_subgrupo
      and basi_400.item            = p_item;

  cursor b_400_s_i is
   select basi_400.codigo_informacao
   from basi_400
   where basi_400.tipo_informacao = 50
     and basi_400.nivel           = p_nivel
     and basi_400.grupo           = p_grupo
     and basi_400.subgrupo        = p_subgrupo
     and basi_400.item            = '000000';

   cursor b_400_s_i_s is
    select basi_400.codigo_informacao
    from basi_400
    where basi_400.tipo_informacao = 50
      and basi_400.nivel           = p_nivel
      and basi_400.grupo           = p_grupo
      and basi_400.subgrupo        = '000'
      and basi_400.item            = '000000';

   Result varchar2(1000);

   gramatura_prod           varchar2(20);--basi_020.gramatura_1%type;
   largura_prod             basi_020.largura_1%type;
   descr_simbolo_prod_nota  varchar2(200);
   

   perc_comp1               basi_030.perc_composicao1%type;
   perc_comp2               basi_030.perc_composicao2%type;
   perc_comp3               basi_030.perc_composicao3%type;
   perc_comp4               basi_030.perc_composicao4%type;
   perc_comp5               basi_030.perc_composicao5%type;

   composicao_prod1         basi_030.composicao_01%type;
   composicao_prod2         basi_030.composicao_02%type;
   composicao_prod3         basi_030.composicao_03%type;
   composicao_prod4         basi_030.composicao_04%type;
   composicao_prod5         basi_030.composicao_05%type;

   prim_vez                 varchar2(10);
   entrou_simbolo           number;
   entrou_cadastro          number;
   descr_simbolo_nota       hdoc_001.descricao3%type;

   composicao1_aux          basi_030.composicao_01%type;
   composicao2_aux          basi_030.composicao_02%type;
   composicao3_aux          basi_030.composicao_03%type;
   composicao4_aux          basi_030.composicao_04%type;
   composicao5_aux          basi_030.composicao_05%type;

   descr_composicao1        varchar2(200);
   descr_composicao2        varchar2(200);
   descr_composicao3        varchar2(200);
   descr_composicao4        varchar2(200);
   descr_composicao5        varchar2(200);

   v_descr_nivel1           obrf_158.descr_nivel1%type;
   v_descr_nivel2           obrf_158.descr_nivel2%type;
   v_descr_nivel4           obrf_158.descr_nivel4%type;
   v_descr_nivel7           obrf_158.descr_nivel7%type;
   v_comp_nivel1            obrf_158.comp_nivel1%type;
   v_comp_nivel2            obrf_158.comp_nivel2%type;
   v_comp_nivel4            obrf_158.comp_nivel4%type;
   v_comp_nivel7            obrf_158.comp_nivel7%type;
   v_imprime_codigo_cliente obrf_158.imprime_codigo_cliente%type;
   v_imprime_cod_barras     obrf_158.imp_cod_barras_danfe%type;
   v_forma_ordenacao_danfe  obrf_158.forma_ordenacao_danfe%type;

   v_descr_produto          varchar2(1000);
   v_descr_composicao       varchar2(1000);
   v_descr_produto2         basi_010.narrativa2%type;
   v_codigo_cliente         basi_010.codigo_cliente%type;
   v_descr_referencia       basi_030.descr_referencia%type;
   v_descr_tam_refer        basi_020.descr_tam_refer%type;
   v_descricao_15           basi_010.descricao_15%type;
   v_cod_barras             basi_010.codigo_barras%type;
   v_cli_quebra_1           pedi_010.cli_quebra_peca%type;
   v_cli_quebra_2           pedi_010.cli_quebra_teci%type;
   v_cli_quebra_4           pedi_010.cli_quebra_pano%type;
   v_cli_quebra_7           pedi_010.cli_quebra_fios%type;
   v_emp_quebra_1           fatu_500.niv_quebra_peca%type;
   v_emp_quebra_2           fatu_500.niv_quebra_tecid%type;
   v_emp_quebra_4           fatu_500.niv_quebra_pano%type;
   v_emp_quebra_7           fatu_500.niv_quebra_fio%type;
   v_modo_impressao_emp     number(1);
   v_modo_impressao_cli     number(1);
   v_modo_impressao         varchar2(1);
   v_ref_fornec             varchar2(30);
   v_descr_ref_fornec       varchar2(60);
   v_descr_item_fornec      varchar2(90);

   v_narrativa_nfe          basi_031.narrativa_nfe%type;
   v_codigo_pais            basi_160.codigo_pais%type;
   v_marca                  basi_400.codigo_informacao%type;
   v_colecao                basi_030.colecao%type;
   v_retorno_msg            varchar2(4000);
   v_codigo_sql             number(5);
   v_local_resultado        number(1);

   b_acha_basi_020          boolean;

   v_imp_tam_danfe          obrf_158.imp_tam_danfe%type;
   v_natop_nf_est_oper      fatu_050.natop_nf_est_oper%type;
   v_tp_impressao_lg_gra    obrf_158.tp_impressao_lg_gra%type;
   v_observacao             fatu_060.observacao%type;
   v_desc_estagio           fatu_060.observacao%type;
   v_descr_param            varchar2(200);
   v_tipo_transacao         estq_005.tipo_transacao%type;

   v_codigo_inicial         fatu_504.posicoes_nr_pedido_cli_ini%type;
   v_codigo_final           fatu_504.posicoes_nr_pedido_cli_fim%type;
   v_descricao_produto      pedi_100.cod_ped_cliente%type;
   v_descricao_cliente      varchar2(1000);
   v_imprime_desc_cli_danfe estq_400.imprime_desc_cli_danfe%type;
begin
   if  instr(p_descr_item,'#$') is not null
   and instr(p_descr_item,'#$') > 0
   then
      v_observacao := nvl(substr(p_descr_item,instr(p_descr_item,'#$') + 2,length(p_descr_item)),' ');

      v_descr_param := nvl(substr(p_descr_item, 1, instr(p_descr_item,'#$') - 1),' ');
   end if;
   
   -- pega a parte enviada do programa para a descricao do estagio
   if  instr(p_descr_item,'!!') is not null
   and instr(p_descr_item,'!!') > 0
   then
      v_desc_estagio := nvl(substr(v_descr_param,instr(v_descr_param,'!!') + 2,length(v_descr_param)),' ');
   end if;

   -- le parametros da descricao do item (obrf_f830)
   begin
     select obrf_158.descr_nivel1,            obrf_158.comp_nivel1,
            obrf_158.descr_nivel2,            obrf_158.comp_nivel2,
            obrf_158.descr_nivel4,            obrf_158.comp_nivel4,
            obrf_158.descr_nivel7,            obrf_158.comp_nivel7,
            obrf_158.imprime_codigo_cliente , obrf_158.imp_cod_barras_danfe,
            obrf_158.imp_tam_danfe,           obrf_158.tp_impressao_lg_gra,
            obrf_158.forma_ordenacao_danfe
     into   v_descr_nivel1,                   v_comp_nivel1,
            v_descr_nivel2,                   v_comp_nivel2,
            v_descr_nivel4,                   v_comp_nivel4,
            v_descr_nivel7,                   v_comp_nivel7,
            v_imprime_codigo_cliente,         v_imprime_cod_barras,
            v_imp_tam_danfe,                  v_tp_impressao_lg_gra,
            v_forma_ordenacao_danfe
     from obrf_158
     where obrf_158.cod_empresa = p_cod_empresa;
   exception
     when no_data_found then
       v_descr_nivel1 := 0;
       v_descr_nivel2 := 0;
       v_descr_nivel4 := 0;
       v_descr_nivel7 := 0;
       v_comp_nivel1  := 0;
       v_comp_nivel2  := 0;
       v_comp_nivel4  := 0;
       v_comp_nivel7  := 0;
       v_imprime_codigo_cliente := 0;
       v_imprime_cod_barras := 'N';
       v_imp_tam_danfe := 0;
       v_tp_impressao_lg_gra := 0;
       v_forma_ordenacao_danfe :=0;
   end;

   -- le parametros de quebra do cliente
   begin
     select pedi_010.cli_quebra_peca, pedi_010.cli_quebra_teci,
            pedi_010.cli_quebra_pano, pedi_010.cli_quebra_fios,
            basi_160.codigo_pais, fatu_050.natop_nf_est_oper
     into   v_cli_quebra_1,           v_cli_quebra_2,
            v_cli_quebra_4,           v_cli_quebra_7,
            v_codigo_pais,            v_natop_nf_est_oper
     from pedi_010, fatu_050, basi_160
     where fatu_050.cgc_9           = pedi_010.cgc_9
       and fatu_050.cgc_4           = pedi_010.cgc_4
       and fatu_050.cgc_2           = pedi_010.cgc_2
       and pedi_010.cod_cidade      = basi_160.cod_cidade
       and fatu_050.codigo_empresa  = p_cod_empresa
       and fatu_050.num_nota_fiscal = p_num_nota
       and fatu_050.serie_nota_fisc = p_serie_nota;
   exception
     when no_data_found then
       v_cli_quebra_1 := 0;
       v_cli_quebra_2 := 0;
       v_cli_quebra_4 := 0;
       v_cli_quebra_7 := 0;
       v_natop_nf_est_oper := '';
   end;

   -- le parametros de quebra por empresa
   begin
     select fatu_500.niv_quebra_peca, fatu_500.niv_quebra_tecid,
            fatu_500.niv_quebra_pano, fatu_500.niv_quebra_fio
     into   v_emp_quebra_1,           v_emp_quebra_2,
            v_emp_quebra_4,           v_emp_quebra_7
     from fatu_500
     where fatu_500.codigo_empresa = p_cod_empresa;
   exception
     when no_data_found then
       v_emp_quebra_1 := 0;
       v_emp_quebra_2 := 0;
       v_emp_quebra_4 := 0;
       v_emp_quebra_7 := 0;
   end;

   /* ACHA FORMA DE QUEBRA CONFORME O NIVEL DO PRODUTO PARA PARAMETRO DE EMPRESA*/
   if    p_nivel = '1'
   then v_modo_impressao_emp := v_emp_quebra_1;
   elsif p_nivel = '2'
   then v_modo_impressao_emp := v_emp_quebra_2;
   elsif p_nivel = '4'
   then v_modo_impressao_emp := v_emp_quebra_4;
   elsif p_nivel = '7'
   then v_modo_impressao_emp := v_emp_quebra_7;
   end if;

   /* ACHA FORMA DE QUEBRA CONFORME O NIVEL DO PRODUTO PARA PARAMETRO DE CLIENTE*/
   if    p_nivel = '1'
   then v_modo_impressao_cli := v_cli_quebra_1;
   elsif p_nivel = '2'
   then v_modo_impressao_cli := v_cli_quebra_2;
   elsif p_nivel = '4'
   then v_modo_impressao_cli := v_cli_quebra_4;
   elsif p_nivel = '7'
   then v_modo_impressao_cli := v_cli_quebra_7;
   end if;

   if  p_nivel <> '1' and p_nivel <> '2' and p_nivel <> '4' and p_nivel <> '7'
   then
      v_modo_impressao_emp := 3;
      v_modo_impressao_cli := 3;
   end if;
   
   --le narrativa e narrativa2
   begin
     
     v_descr_produto := ' ';
     
     select basi_010.narrativa,       basi_010.narrativa2,
            basi_010.codigo_cliente,  trim(basi_010.codigo_barras)
     into   v_descr_produto,          v_descr_produto2,
            v_codigo_cliente,         v_cod_barras
     from basi_010
     where basi_010.nivel_estrutura  = p_nivel
       and basi_010.grupo_estrutura  = p_grupo
       and basi_010.subgru_estrutura = p_subgrupo
       and basi_010.item_estrutura   = p_item;
   exception
     when no_data_found then
       v_descr_produto  := ' ';
       v_descr_produto2 := ' ';
       v_codigo_cliente := ' ';
       v_descr_composicao := ' ';
       v_cod_barras := ' ';
   end;

   -- verifica se deve utilizar a narrativa ou a narrativa2
   if (p_nivel = '1' and v_descr_nivel1 = 1) or
      (p_nivel = '2' and v_descr_nivel2 = 1) or
      (p_nivel = '4' and v_descr_nivel4 = 1) or
      (p_nivel = '7' and v_descr_nivel7 = 1)
   then v_descr_produto := v_descr_produto2;
   end if;

   -- se a descricao estiver nula, busca a descricao do produto
   if trim(v_descr_produto) is null or (v_imp_tam_danfe = 0 and v_modo_impressao_emp = 1)
   then
       begin
         select basi_030.descr_referencia
         into   v_descr_referencia
         from basi_030
         where basi_030.nivel_estrutura = p_nivel
           and basi_030.referencia      = p_grupo;
       exception
         when no_data_found then
           v_descr_referencia := ' ';
       end;

       begin
         select basi_020.descr_tam_refer
         into   v_descr_tam_refer
         from basi_020
         where basi_020.basi030_nivel030 = p_nivel
           and basi_020.basi030_referenc = p_grupo
           and basi_020.tamanho_ref      = p_subgrupo;
       exception
         when no_data_found then
           v_descr_tam_refer := ' ';
       end;

       begin
         select basi_010.descricao_15
         into   v_descricao_15
         from basi_010
        where basi_010.nivel_estrutura  = p_nivel
          and basi_010.grupo_estrutura  = p_grupo
          and basi_010.subgru_estrutura = p_subgrupo
          and basi_010.item_estrutura   = p_item;
       exception
         when no_data_found then
           v_descricao_15 := ' ';
       end;

       if v_modo_impressao_cli > 0
       then
          if    v_modo_impressao_cli = 1 /* Por Grupo */
          then v_descr_produto := trim(v_descr_referencia);
          elsif v_modo_impressao_cli = 2 /* Por Grupo e Subgrupo */
          then v_descr_produto := trim(v_descr_referencia) || ' ' || trim(v_descr_tam_refer);
          elsif v_modo_impressao_cli = 3 or v_modo_impressao_cli = 4 or v_modo_impressao_cli = 5 /* Item completo */
          then v_descr_produto := trim(v_descr_referencia) || ' ' || trim(v_descr_tam_refer) || ' ' || trim(v_descricao_15);
          end if;
       else
          if    v_modo_impressao_emp = 1 /* Por Grupo */
          then v_descr_produto := trim(v_descr_referencia);
          elsif v_modo_impressao_emp = 2 /* Por Grupo e Subgrupo */
          then v_descr_produto := trim(v_descr_referencia) || ' ' || trim(v_descr_tam_refer);
          elsif v_modo_impressao_emp = 3 /* Item completo */
          then v_descr_produto := trim(v_descr_referencia) || ' ' || trim(v_descr_tam_refer) || ' ' || trim(v_descricao_15);
          end if;
       end if;
   end if;

begin
     select estq_005.tipo_transacao
     into   v_tipo_transacao
     from fatu_050, estq_005, pedi_080
    where fatu_050.codigo_empresa         = p_cod_empresa
      and fatu_050.num_nota_fiscal        = p_num_nota
      and fatu_050.serie_nota_fisc        = p_serie_nota
      and fatu_050.natop_nf_nat_oper      = pedi_080.natur_operacao
      and fatu_050.natop_nf_est_oper      = pedi_080.estado_natoper
      and pedi_080.codigo_transacao       = estq_005.codigo_transacao;
   exception
     when no_data_found then
       v_tipo_transacao := '';
   end;
   -- se a descricao estiver nula, pega a descricao da fatu_060
   if trim(v_descr_produto) is null or p_nivel = '0' or v_tipo_transacao = 'D'
   then v_descr_produto := v_descr_param;
   end if;

   if trim(v_descr_produto) is null
   then v_descr_produto := p_descr_item;
   end if;

   if v_imprime_codigo_cliente = 1
   then v_descr_produto := trim(v_descr_produto || ' ' || v_codigo_cliente);
   end if;



   -- se houver descricao por pa?s, deve utilizar esta
   begin
     select basi_031.narrativa_nfe
     into   v_narrativa_nfe
     from basi_031
     where basi_031.cod_pais         = v_codigo_pais
       and basi_031.nivel_estrutura  = p_nivel
       and basi_031.grupo_estrutura  = p_grupo
       and basi_031.subgru_estrutura = p_subgrupo
       and basi_031.item_estrutura   = p_item;
     exception
       when no_data_found then
         v_narrativa_nfe := ' ';
   end;

   if trim(v_narrativa_nfe) is not null
   then v_descr_produto := v_narrativa_nfe;
   end if;

   descr_composicao1 := ' ';
   descr_composicao2 := ' ';
   descr_composicao3 := ' ';
   descr_composicao4 := ' ';
   descr_composicao5 := ' ';
   gramatura_prod    := '990.000';
   largura_prod      := 0;
   descr_simbolo_prod_nota := ' ';
   b_acha_basi_020   := True;

   begin
     select basi_020.composicao_01,    basi_020.composicao_02,
            basi_020.composicao_03,    basi_020.composicao_04,
            basi_020.composicao_05,    basi_020.perc_composicao1,
            basi_020.perc_composicao2, basi_020.perc_composicao3,
            basi_020.perc_composicao4, basi_020.perc_composicao5,
            basi_020.largura_1,        to_char(basi_020.gramatura_1,'9999')
     into   composicao_prod1,          composicao_prod2,
            composicao_prod3,          composicao_prod4,
            composicao_prod5,          perc_comp1,
            perc_comp2,                perc_comp3,
            perc_comp4,                perc_comp5,
            largura_prod,              gramatura_prod
     from basi_020
     where   basi_020.basi030_nivel030 = p_nivel
       and   basi_020.basi030_referenc = p_grupo
       and   basi_020.tamanho_ref      = p_subgrupo
       and ((v_modo_impressao_emp      > 1 and v_modo_impressao_cli = 0)
        or   v_modo_impressao_cli      > 1);
   exception
     when no_data_found then
        begin
            b_acha_basi_020 := False;

            select basi_030.composicao_01,     basi_030.composicao_02,
                   basi_030.composicao_03,     basi_030.composicao_04,
                   basi_030.composicao_05,     basi_030.perc_composicao1,
                   basi_030.perc_composicao2,  basi_030.perc_composicao3,
                   basi_030.perc_composicao4,  basi_030.perc_composicao5
            into   composicao_prod1,          composicao_prod2,
                   composicao_prod3,          composicao_prod4,
                   composicao_prod5,          perc_comp1,
                   perc_comp2,                perc_comp3,
                   perc_comp4,                perc_comp5
            from basi_030
            where basi_030.nivel_estrutura = p_nivel
              and basi_030.referencia      = p_grupo;
       exception
         when no_data_found then
            composicao_prod1 := ' ';
            composicao_prod2 := ' ';
            composicao_prod3 := ' ';
            composicao_prod4 := ' ';
            composicao_prod5 := ' ';
            perc_comp1       := 0.00;
            perc_comp2       := 0.00;
            perc_comp3       := 0.00;
            perc_comp4       := 0.00;
            perc_comp5       := 0.00;
            largura_prod     := 0;
            gramatura_prod   := '990.000';
       end;
   end;

   v_descr_produto := v_descr_produto || ' ' || v_desc_estagio  || ' ' || v_observacao;

   if  trim(composicao_prod1) is null and trim(composicao_prod2) is null and trim(composicao_prod3) is null
   and trim(composicao_prod4) is null and trim(composicao_prod5)  is null and b_acha_basi_020
   then
     begin

          select basi_030.composicao_01,     basi_030.composicao_02,
                 basi_030.composicao_03,     basi_030.composicao_04,
                 basi_030.composicao_05,     basi_030.perc_composicao1,
                 basi_030.perc_composicao2,  basi_030.perc_composicao3,
                 basi_030.perc_composicao4,  basi_030.perc_composicao5
          into   composicao_prod1,          composicao_prod2,
                 composicao_prod3,          composicao_prod4,
                 composicao_prod5,          perc_comp1,
                 perc_comp2,                perc_comp3,
                 perc_comp4,                perc_comp5
          from basi_030
          where basi_030.nivel_estrutura = p_nivel
            and basi_030.referencia      = p_grupo;
     exception
       when no_data_found then
          composicao_prod1 := ' ';
          composicao_prod2 := ' ';
          composicao_prod3 := ' ';
          composicao_prod4 := ' ';
          composicao_prod5 := ' ';
          perc_comp1       := 0.00;
          perc_comp2       := 0.00;
          perc_comp3       := 0.00;
          perc_comp4       := 0.00;
          perc_comp5       := 0.00;
          largura_prod     := 0;
          gramatura_prod   := '990.000';
     end;
   end if;

   -- se for nota de devolucao e estiver parametrizado para imprimir descricao do fornecedor
   -- le a descricao do produto para determinado fornecedor -(supr_060)

   BEGIN
      select supr_060.referencia_forn, supr_060.desc_referencia
       into v_ref_fornec, v_descr_ref_fornec
       from supr_060, fatu_050, fatu_060, estq_005, fatu_503
       where supr_060.forn_060_forne9        = fatu_050.cgc_9
         and supr_060.forn_060_forne4        = fatu_050.cgc_4
         and supr_060.forn_060_forne2        = fatu_050.cgc_2
         and fatu_060.ch_it_nf_cd_empr       = fatu_050.codigo_empresa
         and fatu_060.ch_it_nf_num_nfis      = fatu_050.num_nota_fiscal
         and fatu_060.ch_it_nf_ser_nfis      = fatu_050.serie_nota_fisc
         and supr_060.item_060_nivel99       = p_nivel
         and supr_060.item_060_grupo         = p_grupo
         and supr_060.item_060_subgrupo      = p_subgrupo
         and supr_060.item_060_item          = p_item
         and fatu_050.codigo_empresa         = p_cod_empresa
         and fatu_050.num_nota_fiscal        = p_num_nota
         and fatu_050.serie_nota_fisc        = p_serie_nota
         and estq_005.tipo_transacao         = 'D'
         and estq_005.codigo_transacao       = fatu_060.transacao
         and fatu_503.codigo_empresa         = p_cod_empresa
         and fatu_503.lista_desc_item_fornec = 'S';
   EXCEPTION
       WHEN OTHERS THEN
           v_ref_fornec       := null;
           v_descr_ref_fornec := null;
   END;

   if v_ref_fornec is not null
   then
      v_descr_item_fornec := v_ref_fornec;
   end if;

   if v_ref_fornec is not null
   then
      if v_descr_item_fornec is null
      then
         v_descr_item_fornec := v_descr_ref_fornec;
      else
         v_descr_item_fornec := v_descr_item_fornec||' - '||v_descr_ref_fornec;
      end if;
   end if;

   composicao1_aux := (composicao_prod1);
   composicao2_aux := (composicao_prod2);
   composicao3_aux := (composicao_prod3);
   composicao4_aux := (composicao_prod4);
   composicao5_aux := (composicao_prod5);

   if (p_nivel = '1' and v_comp_nivel1 = 1) or
      (p_nivel = '2' and v_comp_nivel2 = 1) or
      (p_nivel = '4' and v_comp_nivel4 = 1) or
      (p_nivel = '7' and v_comp_nivel7 = 1)
   then
      if    composicao1_aux <> ' ' and composicao1_aux is not null
      then if perc_comp1 > 0
           then descr_composicao1 := ' ' || to_char(perc_comp1, 'FM990.00') ||'% ' || composicao1_aux;
           else descr_composicao1 := ' '|| composicao1_aux;
           end if;
      end if;

      if composicao2_aux <> ' ' and composicao2_aux is not null
      then if perc_comp2 > 0
           then descr_composicao2 := ' ' || to_char(perc_comp2, 'FM990.00') || '% ' || composicao2_aux;
           else descr_composicao2 := ' '|| composicao2_aux;
           end if;
      end if;

      if composicao3_aux <> ' ' and composicao3_aux is not null
      then if perc_comp3 > 0
           then descr_composicao3 := ' ' || to_char(perc_comp3, 'FM990.00') || '% ' || composicao3_aux;
           else descr_composicao3 := ' ' || composicao3_aux;
           end if;
      end if;

      if composicao4_aux <> ' ' and composicao4_aux is not null
      then if perc_comp4 > 0
           then descr_composicao4 := ' ' || to_char(perc_comp4, 'FM990.00') || '% ' || composicao4_aux;
           else descr_composicao4 := ' ' || composicao4_aux;
           end if;
      end if;

      if composicao5_aux <> ' ' and composicao5_aux is not null
      then if perc_comp5 > 0
           then descr_composicao5 := ' ' || to_char(perc_comp5, 'FM990.00') || '% ' || composicao5_aux;
           else descr_composicao5 := ' ' || composicao5_aux;
           end if;
      end if;

      if p_id_ret <> 6 and p_id_ret <> 7
      then
         if v_modo_impressao = 3 and v_natop_nf_est_oper = 'EX'
         then
            begin
                select descricao
                into v_descr_composicao
                from 
                (
                select a.descricao from obrf_022 a
                where a.nivel = p_nivel
                  and a.grupo = p_grupo
                  and a.subgrupo in (p_subgrupo, '0')
                  and a.item in (p_item, '0')
                  order by a.item desc, a.subgrupo desc
                ) 
                where rownum = 1 ;
            exception
            when no_data_found then
                v_descr_produto := trim(v_descr_produto) ||
                                descr_composicao1 ||
                                descr_composicao2 ||
                                descr_composicao3 ||
                                descr_composicao4 ||
                                descr_composicao5;
            end; 
            v_descr_produto := trim(v_descr_produto) ||v_descr_composicao;
         else
            v_descr_produto := trim(v_descr_produto) ||
                                descr_composicao1 ||
                                descr_composicao2 ||
                                descr_composicao3 ||
                                descr_composicao4 ||
                                descr_composicao5;
         end if;
      end if;

      if p_id_ret = 7
      then
         v_descr_composicao := v_observacao ||
            descr_composicao1 ||
            descr_composicao2 ||
            descr_composicao3 ||
            descr_composicao4 ||
            descr_composicao5;
      end if;
   end if;

   /* Adiciona o codigo de barras na descric?o do item.
      OBS: So por impress?o de item */
   if p_nivel = '1' 
   then 
      if v_cli_quebra_1 = 3 or (v_cli_quebra_1 = 0 and v_emp_quebra_1 = 3)
      then
         if v_imprime_cod_barras = 'S' and length(v_cod_barras) > 0 
         then v_descr_produto := trim(v_descr_produto || ' Codigo de barras: ' || trim(v_cod_barras));
         end if;
      end if;
   end if;
   
   if p_nivel = '2' 
   then 
      if v_cli_quebra_2 = 3 or (v_cli_quebra_2 = 0 and v_emp_quebra_2 = 3)
      then
         if v_imprime_cod_barras = 'S' and length(v_cod_barras) > 0 
         then v_descr_produto := trim(v_descr_produto || ' Codigo de barras: ' || trim(v_cod_barras));
         end if;
      end if;
   end if;
   
   if p_nivel = '4' 
   then 
      if v_cli_quebra_4 = 3 or (v_cli_quebra_4 = 0 and v_emp_quebra_4 = 3)
      then
         if v_imprime_cod_barras = 'S' and length(v_cod_barras) > 0 
         then v_descr_produto := trim(v_descr_produto || ' Codigo de barras: ' || trim(v_cod_barras));
         end if;
      end if;
   end if;
     
   if p_nivel = '7' 
   then 
      if v_cli_quebra_7 = 3 or (v_cli_quebra_7 = 0 and v_emp_quebra_7 = 3)
      then
         if v_imprime_cod_barras = 'S' and length(v_cod_barras) > 0 
         then v_descr_produto := trim(v_descr_produto || ' Codigo de barras: ' || trim(v_cod_barras));
         end if;
      end if;
   end if;

   select fatu_504.posicoes_nr_pedido_cli_ini, fatu_504.posicoes_nr_pedido_cli_fim
   into v_codigo_inicial, v_codigo_final
   from fatu_504
   where fatu_504.codigo_empresa = p_cod_empresa;

   if v_codigo_inicial > 0 and v_codigo_final > v_codigo_inicial
   then
     begin
      select substr(pedi_100.cod_ped_cliente,v_codigo_inicial,(v_codigo_final - v_codigo_inicial) + 1)
      into v_descricao_produto
      from fatu_060, obrf_082,pcpc_040,pcpc_020, pedi_100
      where fatu_060.ch_it_nf_cd_empr  = p_cod_empresa
        and fatu_060.ch_it_nf_num_nfis = p_num_nota
        and fatu_060.ch_it_nf_ser_nfis = p_serie_nota
        and fatu_060.nivel_estrutura   = p_nivel
        and fatu_060.grupo_estrutura   = p_grupo
        and fatu_060.subgru_estrutura  = p_subgrupo
        and fatu_060.item_estrutura    = p_item
        and obrf_082.numero_ordem      = fatu_060.num_ordem_serv
        and obrf_082.sequencia         = fatu_060.seq_item_ordem
        and pcpc_040.numero_ordem      = obrf_082.numero_ordem
        and pcpc_040.seq_ordem_serv    = obrf_082.sequencia
        and pcpc_040.ordem_producao    = pcpc_020.ordem_producao
        and pedi_100.pedido_venda      = pcpc_020.pedido_venda
        and  obrf_082.numero_ordem     <> 0
        and  pcpc_020.pedido_venda     > 0
        and   pedi_100.cod_ped_cliente not like (' ');
        exception
        when no_data_found then
          v_descricao_produto := ' ';
     end;
     if v_descricao_produto <> ' '
     then 
        v_descr_produto := v_descr_produto || ' ' || v_descricao_produto;
     end if;
   end if;
   
   v_descricao_cliente := '';
   
   begin
          select decode(estq_400.imprime_desc_cli_danfe,1, '(' || estq_400.ref_cliente || estq_400.cor_cliente || ' ' || estq_400.descr_cor_cliente || ')',
                                                       estq_400.ref_cliente || estq_400.tam_cliente || estq_400.cor_cliente || ' ' || estq_400.descr_ref_cliente || estq_400.descr_tam_cliente || estq_400.descr_cor_cliente),
               estq_400.imprime_desc_cli_danfe
     into v_descricao_cliente,
         v_imprime_desc_cli_danfe
     from estq_400, fatu_050
     where fatu_050.codigo_empresa = p_cod_empresa
     and fatu_050.num_nota_fiscal = p_num_nota
     and fatu_050.serie_nota_fisc = p_serie_nota
     and estq_400.cliente9 = fatu_050.cgc_9
     and estq_400.cliente4 = fatu_050.cgc_4
     and estq_400.cliente2 = fatu_050.cgc_2
     and estq_400.nivel_estrutura = p_nivel
     and estq_400.grupo_estrutura = p_grupo
     and estq_400.subgrupo_estrutura = p_subgrupo
     and estq_400.item_estrutura = p_item
     and estq_400.imprime_desc_cli_danfe in (1,2);
   exception
   when no_data_found then
       v_descricao_cliente := ' ';
   end;
   
   /* 1 - CONCATENA NA DESCRI??O DO PRODUTO, 
      2 - ENVIA EM INFORMA??ES ADICIONAIS DOS ITENS
   */
   
   if v_descricao_cliente <> ' ' and v_imprime_desc_cli_danfe = 1 
   then 
    v_descr_produto := v_descr_produto || ' ' || v_descricao_cliente;
   end if;   

   prim_vez := 's';
   entrou_simbolo := 0;

   for cb_400 in b_400
   LOOP
      entrou_simbolo := 1;
      begin
        select decode(trim(hdoc_001.descricao3),null,to_char(cb_400.codigo_informacao,'000'),trim(hdoc_001.descricao3))
        into   descr_simbolo_nota
        from hdoc_001
        where hdoc_001.tipo   = 50
          and hdoc_001.codigo = cb_400.codigo_informacao;
      exception
        when no_data_found then
          descr_simbolo_nota := ' ';
      end;
      if descr_simbolo_nota <> ' '
      then
         if prim_vez = 's'
         then descr_simbolo_prod_nota := descr_simbolo_nota;
         else descr_simbolo_prod_nota := descr_simbolo_prod_nota || ' ' || descr_simbolo_nota;
         end if;
         prim_vez := 'n';
      end if;
   END loop;

   if entrou_simbolo = 0
   then
      for cb_400_s_i in b_400_s_i
      LOOP
         entrou_simbolo := 1;
         begin
           select decode(trim(hdoc_001.descricao3),null,to_char(cb_400_s_i.codigo_informacao,'000'),trim(hdoc_001.descricao3))
           into   descr_simbolo_nota
           from hdoc_001
           where hdoc_001.tipo   = 50
             and hdoc_001.codigo = cb_400_s_i.codigo_informacao;
         exception
           when no_data_found then
             descr_simbolo_nota := ' ';
         end;

         if descr_simbolo_nota <> ' '
         then
           if prim_vez = 's'
           then descr_simbolo_prod_nota := descr_simbolo_nota;
           else descr_simbolo_prod_nota := descr_simbolo_prod_nota || ' ' || descr_simbolo_nota;
           end if;
           prim_vez := 'n';
         end if;
     END loop;
   end if;

   if entrou_simbolo = 0
   then
       for cb_400_s_i_s in b_400_s_i_s
       LOOP
          begin
            select decode(p_ind_cons,1,decode(trim(hdoc_001.descricao3),null,to_char(cb_400_s_i_s.codigo_informacao,'000'),trim(hdoc_001.descricao3)),2,trim(hdoc_001.descricao3),3,to_char(cb_400_s_i_s.codigo_informacao,'000'),null)
            into   descr_simbolo_nota
            from hdoc_001
            where hdoc_001.tipo   = 50
              and hdoc_001.codigo = cb_400_s_i_s.codigo_informacao;
          exception
            when no_data_found then
              descr_simbolo_nota := ' ';
          end;

          if descr_simbolo_nota <> ' '
          then
            if prim_vez = 's'
            then descr_simbolo_prod_nota := descr_simbolo_nota;
            else descr_simbolo_prod_nota := descr_simbolo_prod_nota || ' ' || descr_simbolo_nota;
            end if;
             prim_vez := 'n';
          end if;
      END loop;
   end if;

   if v_modo_impressao_cli > 0
   then v_modo_impressao := trim(to_char(v_modo_impressao_cli,'0'));
   else v_modo_impressao := trim(to_char(v_modo_impressao_emp,'0'));
   end if;

   if v_forma_ordenacao_danfe = 2 and v_modo_impressao = 3
   then v_modo_impressao := 4;
   end if;

   if v_modo_impressao = 3 and v_natop_nf_est_oper = 'EX'
   then
      begin
            select descricao
            into v_descr_composicao
            from 
            (
            select a.descricao from obrf_022 a
            where a.nivel = p_nivel
              and a.grupo = p_grupo
              and a.subgrupo in (p_subgrupo, '0')
              and a.item in (p_item, '0')
              order by a.item desc, a.subgrupo desc
            ) 
            where rownum = 1 ;
        exception
         when no_data_found then
            v_descr_composicao := v_descr_composicao;
        end; 
   end if;


   if p_id_ret = 1
   then
      if gramatura_prod is not null and p_nivel <> '0'
      then 
          if gramatura_prod < 0.999 
          then 
             result := 'gr/m2 ' || to_char(gramatura_prod*1000,'990');
          else result := 'gr/m2 ' || to_char(gramatura_prod,'990');
          end if;
      else result := NULL;
      end if;
   end if;

   if p_id_ret = 2
   then
     if largura_prod is not null  and p_nivel <> '0'
     then 
         if largura_prod > 0.000 and v_tp_impressao_lg_gra = 1
         then
            result := 'Larg ' || to_char(largura_prod,'990.00');
         else result := 'Larg ' || to_char(largura_prod,'990.000');
         end if;
     else result := NULL;
     end if;
   end if;

   if p_id_ret = 4 and v_descr_item_fornec is not null
   then
      select decode(v_descr_produto,null,''||v_descr_item_fornec,v_descr_produto||Chr(10)||v_descr_item_fornec)
      into v_descr_produto from dual;
   end if;

   if p_id_ret = 3 then result := descr_simbolo_prod_nota;
   elsif p_id_ret = 4 then result := v_descr_produto;
   elsif p_id_ret = 5 then result := v_modo_impressao;
   elsif p_id_ret = 6 then result := inter_fn_retira_acentuacao(v_descr_produto);
   elsif p_id_ret = 7 then result := inter_fn_retira_acentuacao(v_descr_composicao);
   elsif  p_id_ret = 9 then result :=  v_descricao_cliente;
   elsif p_id_ret = 8
   then
      if v_descr_item_fornec is null
      then
           result := '';
      else
           result := inter_fn_retira_acentuacao(v_descr_item_fornec);
      end if;
   end if;
   

   return(Result);

end inter_fn_dados_produto_nf;


/
