declare
tmpSql number;
begin
   begin
      select 1
      into tmpSql
      from pcpt_050
      where pcpt_050.cod_cancelamento = 0;
   exception when no_data_found then
      insert into pcpt_050 (cod_cancelamento, descricao) values (0,'.');
      commit;
   end;
end;

/
