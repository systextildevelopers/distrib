CREATE OR REPLACE TRIGGER INTER_TR_SUPR_113
BEFORE INSERT ON SUPR_113 FOR EACH ROW
DECLARE
    next_value number;
BEGIN
    if :new.COD_CONDICAO is null or :new.COD_CONDICAO = 0 then
        select ID_SUPR_113.nextval
        into next_value from dual;
        :new.COD_CONDICAO := next_value;
    end if;
END INTER_TR_SUPR_113;
