CREATE OR REPLACE PROCEDURE "INTER_PR_EFIC_100_INFO"
    (p_data_rejeicao DATE, p_nivel VARCHAR2, p_grupo VARCHAR2, p_subgrupo VARCHAR2,
    p_item VARCHAR2, p_motivo NUMBER, p_area_producao NUMBER, p_periodo NUMBER,
    p_numero_ordem NUMBER, p_classificacao NUMBER, p_quantidade NUMBER,
    p_numero_tubete NUMBER, p_estagio NUMBER, p_estagio_digitacao NUMBER,
    p_turno NUMBER, p_pedido_venda NUMBER, p_observacao VARCHAR2, p_nivel_mat_prima VARCHAR2,
    p_grupo_mat_prima VARCHAR2, p_sub_mat_prima VARCHAR2, p_item_mat_prima VARCHAR2, 
    p_parte_peca VARCHAR2, p_maq VARCHAR2, p_sub_maq VARCHAR2, p_num_maq NUMBER,
    p_codigo_operacao NUMBER, p_tipo_apont NUMBER, p_codigo_familia NUMBER, p_codigo_intervalo NUMBER
    /*p_usuario_digitacao VARCHAR2,
    p_deposito NUMBER*/)
is
    contador number;
begin

    begin

        if (p_numero_tubete = 0 and p_quantidade < 0)
        then

            SELECT count(*)
            INTO contador
            FROM efic_100_info
            WHERE data_rejeicao = p_data_rejeicao
            AND prod_rej_nivel99 = p_nivel
            AND prod_rej_grupo = p_grupo
            AND prod_rej_subgrupo = p_subgrupo
            AND prod_rej_item = p_item
            AND codigo_motivo = p_motivo
            AND numero_ordem = p_numero_ordem
            AND numero_tubete = 0
            AND codigo_estagio = p_estagio;
        else 
            SELECT count(*)
            INTO contador
            FROM efic_100_info
            WHERE data_rejeicao = p_data_rejeicao
            AND prod_rej_nivel99 = p_nivel
            AND prod_rej_grupo = p_grupo
            AND prod_rej_subgrupo = p_subgrupo
            AND prod_rej_item = p_item
            AND codigo_motivo = p_motivo
            AND numero_ordem = p_numero_ordem
            AND pedido_venda = p_pedido_venda
            AND (maq = p_maq or p_maq is null)
            AND (sub_maq = p_sub_maq or p_sub_maq is null)
            AND (num_maq = p_num_maq or p_num_maq is null)
            AND (codigo_operacao = p_codigo_operacao or p_codigo_operacao is null)
            AND codigo_familia = p_codigo_familia
            AND codigo_intervalo = p_codigo_intervalo
            AND numero_tubete = p_numero_tubete
            AND tipo_apont = p_tipo_apont;
        end if;

        if contador > 0
        then
            begin

                if (p_numero_tubete = 0 and p_quantidade < 0)
                then
                    begin
                    
                        DELETE FROM efic_100_info
                        WHERE data_rejeicao = p_data_rejeicao
                        AND prod_rej_nivel99 = p_nivel
                        AND prod_rej_grupo = p_grupo
                        AND prod_rej_subgrupo = p_subgrupo
                        AND prod_rej_item = p_item
                        AND numero_tubete = 0
                        AND numero_ordem = p_numero_ordem
                        AND codigo_motivo = p_motivo
                        AND codigo_estagio = p_estagio;
                        exception
                        when others then
                            raise_application_error(-20000, 'Erro ao deletar registros na tabela EFIC_100_INFO');
                    end;
                else
                    begin

                        UPDATE efic_100_info
                        SET quantidade = quantidade + p_quantidade
                        WHERE data_rejeicao = p_data_rejeicao
                        AND prod_rej_nivel99 = p_nivel
                        AND prod_rej_grupo = p_grupo
                        AND prod_rej_subgrupo = p_subgrupo
                        AND prod_rej_item = p_item
                        AND numero_tubete = p_numero_tubete
                        AND numero_ordem = p_numero_ordem
                        AND codigo_motivo = p_motivo
                        AND pedido_venda = p_pedido_venda
                        AND (maq = p_maq or p_maq is null)
                        AND (sub_maq = p_sub_maq or p_sub_maq is null)
                        AND (num_maq = p_num_maq or p_num_maq is null)
                        AND (codigo_operacao = p_codigo_operacao or p_codigo_operacao is null)
                        AND codigo_familia = p_codigo_familia
                        AND codigo_intervalo = p_codigo_intervalo;
                        exception
                        when others then
                            raise_application_error(-20000, 'Erro ao atualizar tabela EFIC_100_INFO');
                    end;
                end if;
            end;
        else
            if p_quantidade > 0
            then
                begin
                    INSERT INTO efic_100_info (
                        data_rejeicao,     		prod_rej_nivel99,
                        prod_rej_grupo, 		prod_rej_subgrupo,
                        prod_rej_item,    		codigo_motivo,
                        area_producao,  		periodo_producao,
                        numero_ordem, 		    classificacao,
                        quantidade,       		numero_tubete,
                        codigo_estagio, 		estagio_digitacao,
                        turno,            	 	pedido_venda,
                        observacao, 	 		nivel_mat_prima,
                        grupo_mat_prima, 	sub_mat_prima,
                        item_mat_prima,		parte_peca,
                        maq, 				sub_maq,
                        num_maq, 			codigo_operacao,
                        tipo_apont,         codigo_familia,
                        codigo_intervalo
                    ) VALUES (
                        p_data_rejeicao,     	p_nivel,
                        p_grupo, 		p_subgrupo,
                        p_item,    		p_motivo,
                        p_area_producao,  		p_periodo,
                        p_numero_ordem, 		    p_classificacao,
                        p_quantidade,       		p_numero_tubete,
                        p_estagio, 		p_estagio_digitacao,
                        p_turno,            	 	p_pedido_venda,
                        p_observacao, 	 		p_nivel_mat_prima,
                        p_grupo_mat_prima, 	p_sub_mat_prima,
                        p_item_mat_prima,		p_parte_peca,
                        p_maq, 				p_sub_maq,
                        p_num_maq, 			p_codigo_operacao,
                        p_tipo_apont,         p_codigo_familia,
                        p_codigo_intervalo
                    );
                    exception
                    when others then
                    raise_application_error(-20000,'Erro ao inserir tabela EFIC_100_INFO');
                end;
            end if;

        end if;
    end;
end INTER_PR_EFIC_100_INFO;


/

exec inter_pr_recompile;
