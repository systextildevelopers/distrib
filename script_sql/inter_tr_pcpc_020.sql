
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_020" 
   before update of cod_cancelamento, periodo_producao, ultimo_estagio, alternativa_peca
   on pcpc_020
   for each row
declare
   t_existe_041       number;
   t_existe_630       number;
   v_executa_trigger  number;
   qtde_programada    number;
   qtde_2a_ant        number;
   qtde_perdas_ant    number;
   qtde_conserto_ant  number;
   deposito_conserto  number;
   f_qtde_em_producao number;
   f_qtde_pecas_prod  number;
   f_qtde_conserto    number;
   qtde               number;
   f_qtde_pecas_prog  number;
   v_tem_reg_400      number;
begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)


   if v_executa_trigger = 0
   then
      t_existe_630 := 0;

      begin
         select nvl(count(*),0)
         into t_existe_630
         from tmrp_630
         where tmrp_630.ordem_prod_compra   = :new.ordem_producao
           and tmrp_630.alternativa_produto = :old.alternativa_peca
           and tmrp_630.area_producao     = 1;
      exception when OTHERS then
         t_existe_630 := 0;
      end;

      if  updating
      and :new.alternativa_peca <> :old.alternativa_peca
      and t_existe_630 > 0
      and :new.ordem_principal = 0
      then
         begin
            inter_pr_altera_alternativa (:new.ordem_producao,
                                         :new.alternativa_peca,
                                         :new.codigo_risco);
         end;
      end if;

      if updating
      then
         -- Executa o planejamento da producao.
         -- Quando o ultimo estagi e atualizado na capa da ordem de producao, significa que todos os
         -- pacotes de producao foram gerados para a ordem de producao, o sistema pega o ultimo estagio
         -- e atualiza na capa da ordem de producao.

         if  (:old.periodo_producao = :new.periodo_producao)
         and (:old.cod_cancelamento = :new.cod_cancelamento)
         and (:new.cod_cancelamento = 0)
         then
            begin
               select empr_001.deposito_conserto
               into deposito_conserto
               from empr_001;
            end;

            begin
               delete tmrp_041
               where tmrp_041.periodo_producao = :old.periodo_producao
                 and tmrp_041.area_producao    = 1
                 and tmrp_041.nr_pedido_ordem  = :old.ordem_producao;
            end;

            v_tem_reg_400 := nvl(inter_fn_executou_recalculo(),0);

            if v_tem_reg_400 = 0
            then
               for reg_estagio in (select pcpc_040.proconf_nivel99,    pcpc_040.proconf_grupo,
                                          pcpc_040.proconf_subgrupo,   pcpc_040.proconf_item,
                                          pcpc_040.codigo_estagio,     pcpc_040.qtde_pecas_prog,
                                          pcpc_040.seq_operacao,       pcpc_040.sequencia_estagio,
                                          pcpc_040.ordem_confeccao,    pcpc_040.qtde_pecas_prod,
                                          pcpc_040.qtde_pecas_2a,      pcpc_040.qtde_perdas,
                                          pcpc_040.qtde_conserto,      pcpc_040.estagio_depende,
                                          pcpc_040.estagio_anterior
                                   from pcpc_040
                                   where pcpc_040.ordem_producao = :new.ordem_producao
                                   order by pcpc_040.seq_operacao)
               loop
                  f_qtde_pecas_prod := reg_estagio.qtde_pecas_prod;
                  f_qtde_pecas_prog := reg_estagio.qtde_pecas_prog;
                  f_qtde_conserto   := reg_estagio.qtde_conserto;

                  if reg_estagio.codigo_estagio = :new.ultimo_estagio
                  then
                     qtde_programada := reg_estagio.qtde_pecas_prog;

                     if reg_estagio.estagio_anterior > 0
                     then
                        if reg_estagio.estagio_depende > 0
                        then
                           begin
                              select sum(pcpc_040.qtde_pecas_2a),     sum(pcpc_040.qtde_perdas),
                                     sum(pcpc_040.qtde_conserto)
                              into   qtde_2a_ant,                  qtde_perdas_ant,
   	                                qtde_conserto_ant
                              from pcpc_040
                              where pcpc_040.periodo_producao = :new.periodo_producao
                                and pcpc_040.ordem_confeccao  = reg_estagio.ordem_confeccao
                                and pcpc_040.codigo_estagio   = reg_estagio.estagio_depende;
                           exception when no_data_found then
                              qtde_2a_ant       := 0;
                              qtde_perdas_ant   := 0;
                              qtde_conserto_ant := 0;
                           end;

                           if qtde_conserto_ant > 0 and deposito_conserto > 0
                           then
                              qtde_conserto_ant := qtde_conserto_ant;
                           else
                              qtde_conserto_ant := 0;
                           end if;

                           qtde_programada := qtde_programada - (qtde_2a_ant + qtde_perdas_ant + qtde_conserto_ant);
                        else
                           begin
                              select sum(pcpc_040.qtde_pecas_2a),  sum(pcpc_040.qtde_perdas),
                                     sum(pcpc_040.qtde_conserto)
                              into   qtde_2a_ant,                  qtde_perdas_ant,
                                     qtde_conserto_ant
                              from pcpc_040
                              where pcpc_040.periodo_producao = :new.periodo_producao
                                and pcpc_040.ordem_confeccao  = reg_estagio.ordem_confeccao
                                and pcpc_040.codigo_estagio   = reg_estagio.estagio_anterior;
                           exception when no_data_found then
   	                         qtde_2a_ant       := 0;
   	                         qtde_perdas_ant   := 0;
   	                         qtde_conserto_ant := 0;
         	                end;

                           if  qtde_conserto_ant > 0
                           and deposito_conserto > 0
                           then
                              qtde_conserto_ant := qtde_conserto_ant;
                           else
                              qtde_conserto_ant := 0;
                           end if;

                           qtde_programada := qtde_programada - (qtde_2a_ant + qtde_perdas_ant + qtde_conserto_ant);
                        end if;
   	                else
                        qtde_programada := qtde_programada;
                     end if;

                     if  deposito_conserto = 0
                     and f_qtde_conserto > 0
                     then
                        f_qtde_conserto := 0.00;
                     end if;

                     f_qtde_em_producao := 0;
                     f_qtde_em_producao := qtde_programada - (reg_estagio.qtde_pecas_prod +
                                                              reg_estagio.qtde_pecas_2a +
                                                              reg_estagio.qtde_perdas +
                                                              f_qtde_conserto);

                     -- Verifica se o registro ja existe, se nao existir insere novo registro
                     -- senao atualiza a quantidade a receber do produto.
                     t_existe_041 := 0;

                     begin
                        select count(*)
                        into t_existe_041
                        from tmrp_041
                        where tmrp_041.periodo_producao    = :new.periodo_producao
                          and tmrp_041.area_producao       = 1
                          and tmrp_041.nr_pedido_ordem     = :new.ordem_producao
                          and tmrp_041.nivel_estrutura     = reg_estagio.proconf_nivel99
                          and tmrp_041.grupo_estrutura     = reg_estagio.proconf_grupo
                          and tmrp_041.subgru_estrutura    = reg_estagio.proconf_subgrupo
                          and tmrp_041.item_estrutura      = reg_estagio.proconf_item
                          and tmrp_041.codigo_estagio      = :new.ultimo_estagio
                          and tmrp_041.seq_pedido_ordem    = reg_estagio.ordem_confeccao;
                     end;

                     if t_existe_041 = 0
                     then
                        -- Insere produto para planejamento da producao.
                        insert into tmrp_041 (
                           periodo_producao,            area_producao,
                           nr_pedido_ordem,             seq_pedido_ordem,
                           codigo_estagio,              nivel_estrutura,
                           grupo_estrutura,             subgru_estrutura,
                           item_estrutura,              qtde_reservada,
                           qtde_areceber,               consumo
                        ) values (
                           :new.periodo_producao,        1,
                           :new.ordem_producao,          reg_estagio.ordem_confeccao,
                           :new.ultimo_estagio,          reg_estagio.proconf_nivel99,
                           reg_estagio.proconf_grupo,    reg_estagio.proconf_subgrupo,
                           reg_estagio.proconf_item,     0.00,
                           f_qtde_em_producao,           0.00
                        );
                     end if;
                  end if;

                  if deposito_conserto <> 0
                  and reg_estagio.codigo_estagio = :new.ultimo_estagio
                  then
                     f_qtde_pecas_prod := f_qtde_pecas_prod + f_qtde_conserto;
                  end if;

                  f_qtde_pecas_prod := f_qtde_pecas_prod + reg_estagio.qtde_pecas_2a + reg_estagio.qtde_perdas;
                  qtde              := f_qtde_pecas_prog - f_qtde_pecas_prod;

                  -- Chama a procedure que explode a estrutura gerando planejamento
                  -- a partir dos pacotes criados para producao.
                  ----
                  -- O planejamento do tecido principal e feito no insert acima.
                  -- PCPC_040.

                  if qtde > 0
                  then
                    inter_pr_explode_estrutura(reg_estagio.proconf_nivel99,
                                                reg_estagio.proconf_grupo,
                                                reg_estagio.proconf_subgrupo,
                                                reg_estagio.proconf_item,
                                                :new.alternativa_peca,
                                                :new.periodo_producao,
                                                qtde,
                                                :new.ordem_producao,
                                                :new.codigo_risco,
                                                1,
                                                reg_estagio.ordem_confeccao,
                                                reg_estagio.codigo_estagio,
                                                reg_estagio.codigo_estagio,
                                                0.00,
                                                qtde,
                                                'pcpc',1);
                  end if;
               end loop;
            else
               for reg_estagio in (select pcpc_040.proconf_nivel99,    pcpc_040.proconf_grupo,
                                          pcpc_040.proconf_subgrupo,   pcpc_040.proconf_item,
                                          pcpc_040.codigo_estagio,     pcpc_040.qtde_pecas_prog,
                                          pcpc_040.seq_operacao,       pcpc_040.sequencia_estagio,
                                          pcpc_040.ordem_confeccao,    pcpc_040.qtde_pecas_prod,
                                          pcpc_040.qtde_pecas_2a,      pcpc_040.qtde_perdas,
                                          pcpc_040.qtde_conserto,      pcpc_040.estagio_depende,
                                          pcpc_040.estagio_anterior,   pcpc_040.qtde_a_produzir_pacote
                                   from pcpc_040
                                   where pcpc_040.ordem_producao = :new.ordem_producao
                                   order by pcpc_040.seq_operacao)
               loop
                  if reg_estagio.codigo_estagio = :new.ultimo_estagio
                  then
                     -- Verifica se o registro ja existe, se nao existir insere novo registro
                     -- senao atualiza a quantidade a receber do produto.
                     t_existe_041 := 0;

                     begin
                        select count(*)
                        into t_existe_041
                        from tmrp_041
                        where tmrp_041.periodo_producao    = :new.periodo_producao
                          and tmrp_041.area_producao       = 1
                          and tmrp_041.nr_pedido_ordem     = :new.ordem_producao
                          and tmrp_041.nivel_estrutura     = reg_estagio.proconf_nivel99
                          and tmrp_041.grupo_estrutura     = reg_estagio.proconf_grupo
                          and tmrp_041.subgru_estrutura    = reg_estagio.proconf_subgrupo
                          and tmrp_041.item_estrutura      = reg_estagio.proconf_item
                          and tmrp_041.codigo_estagio      = :new.ultimo_estagio
                          and tmrp_041.seq_pedido_ordem    = reg_estagio.ordem_confeccao;
                     end;

                     if t_existe_041 = 0
                     then
                        -- Insere produto para planejamento da producao.
                        insert into tmrp_041 (
                           periodo_producao,                        area_producao,
                           nr_pedido_ordem,                         seq_pedido_ordem,
                           codigo_estagio,                          nivel_estrutura,
                           grupo_estrutura,                         subgru_estrutura,
                           item_estrutura,                          qtde_reservada,
                           qtde_areceber,                           consumo
                        ) values (
                           :new.periodo_producao,                   1,
                           :new.ordem_producao,                     reg_estagio.ordem_confeccao,
                           :new.ultimo_estagio,                     reg_estagio.proconf_nivel99,
                           reg_estagio.proconf_grupo,               reg_estagio.proconf_subgrupo,
                           reg_estagio.proconf_item,                0.00,
                           reg_estagio.qtde_a_produzir_pacote,      0.00
                        );
                     end if;
                  end if;

                  -- Chama a procedure que explode a estrutura gerando planejamento
                  -- a partir dos pacotes criados para producao.
                  ----
                  -- O planejamento do tecido principal e feito no insert acima.
                  -- PCPC_040.

                  if reg_estagio.qtde_a_produzir_pacote > 0
                  then
                     inter_pr_explode_estrutura(reg_estagio.proconf_nivel99,
                                                reg_estagio.proconf_grupo,
                                                reg_estagio.proconf_subgrupo,
                                                reg_estagio.proconf_item,
                                                :new.alternativa_peca,
                                                :new.periodo_producao,
                                                reg_estagio.qtde_a_produzir_pacote,
                                                :new.ordem_producao,
                                                :new.codigo_risco,
                                                1,
                                                reg_estagio.ordem_confeccao,
                                                reg_estagio.codigo_estagio,
                                                reg_estagio.codigo_estagio,
                                                0.00,
                                                reg_estagio.qtde_a_produzir_pacote,
                                                'pcpc',1);
                  end if;
               end loop;
            end if;
         end if;

         -- Se esta cancelando a OP ira eliminar os registros do planejamento.
         if :old.cod_cancelamento = 0 and :new.cod_cancelamento > 0
         then
            begin
               delete tmrp_041
               where tmrp_041.periodo_producao = :old.periodo_producao
                 and tmrp_041.area_producao    = 1
                 and tmrp_041.nr_pedido_ordem  = :old.ordem_producao;
            end;
         end if;

         -- Se alterar o periodo de producao da OP, altera o periodo de producao de
         -- todos os pacotes da OP.
         if :old.periodo_producao <> :new.periodo_producao
         then
            -- Altera periodo de producao do planejamento, se alterar o periodo de producao da OB
            begin
               update tmrp_041
               set tmrp_041.periodo_producao   = :new.periodo_producao
               where tmrp_041.periodo_producao = :old.periodo_producao
                 and tmrp_041.area_producao    = 1
                 and tmrp_041.nr_pedido_ordem  = :new.ordem_producao;
            end;
         end if;
      end if;

   end if;

end inter_tr_pcpc_020;

-- ALTER TRIGGER "INTER_TR_PCPC_020" ENABLE
 

/

exec inter_pr_recompile;

