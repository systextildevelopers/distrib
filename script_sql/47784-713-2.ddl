ALTER TABLE obrf_186
  ADD(
    PIS_CREDITAR 	NUMBER(15,2) default 0 null,
    COFINS_CREDITAR NUMBER(15,2) default 0 null,
    PERC_PIS_CREDITAR NUMBER(5,2) default 0 null,
    PERC_COFINS_CREDITAR NUMBER(5,2) default 0 null
  );

COMMIT;
EXEC inter_pr_recompile;
