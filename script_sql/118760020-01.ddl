create table rcnb_067_tmp (
	num_requisicao        	number(6) 		default 0,
	seq_item_req          	number(2) 		default 0,
	item_req_nivel99      	varchar2(1)     default '',
	item_req_grupo        	varchar2(5)     default '',
	item_req_subgrupo     	varchar2(3)     default '',
	item_req_item         	varchar2(6)     default '',
	qtde_requisitada      	number(15,3)    default 0.0,
	data_prev_entr        	date,
	flag_marcar				numeric (1)		default 0,
	usuario					varchar2(20)    default ''
);

alter table rcnb_067_tmp add constraint pk_supr_067_tmp primary key (num_requisicao, seq_item_req);

alter table rcnb_067_tmp add constraint supr_067_tmp_ck_flag_marcar check (flag_marcar in (0,1));
