
  CREATE OR REPLACE FUNCTION "INTER_FN_CONSUMO_QUEBRA_CORTE" (f_nivel_comp in varchar, f_grupo_comp in varchar,
                                                         f_sub_comp   in varchar, f_consumo    in number)

return number is
-- variavel a ser retornada no calculo da funcao
   v_consumo_final     number(15,5);
   v_perc_quebra_corte basi_023.perc_quebra_corte%type;

begin

   begin

      select basi_023.perc_quebra_corte
      into   v_perc_quebra_corte
	    from basi_023
    	where basi030_nivel030 = f_nivel_comp
	      and basi030_referenc = f_grupo_comp
    	  and tamanho_ref      = f_sub_comp;
    	exception
         when others then
            v_perc_quebra_corte := 0.00000;
   end;

   if v_perc_quebra_corte > 0
   then
      v_consumo_final := f_consumo + (f_consumo * (v_perc_quebra_corte/100));
   else
      v_consumo_final := f_consumo;
   end if;

   return(v_consumo_final);

end inter_fn_consumo_quebra_corte;

 

/

exec inter_pr_recompile;

