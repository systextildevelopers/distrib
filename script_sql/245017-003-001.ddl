ALTER TABLE fndc_005 ADD CODIGO_INSTRUCAO number(2) default 0;

ALTER TABLE fndc_005 ADD CODIGO_RECOMPRA number(3) default 0;

ALTER TABLE fndc_005
DROP CONSTRAINT PK_FNDC_005;

ALTER TABLE fndc_005
ADD CONSTRAINT PK_FNDC_005 PRIMARY KEY
(COD_EMPRESA, CODIGO_RECOMPRA);
