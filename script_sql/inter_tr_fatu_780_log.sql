  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_780_LOG" 
  before insert or delete or
  update of pedido, sequencia, nivel, grupo,
	subgrupo, item, deposito, lote,
	nr_solicitacao, tipo_registro, qtde_pedida, qtde_sugerida,
	qtde_disponivel, qtde_afaturar, nr_sugestao, nome_programa,
	ref_original, executa_trigger, flag_controle_wms,
	seq_conjunto,            qtde_coleta 
  on fatu_780
  for each row
declare
  -- local variables here
  v_pedido               				 fatu_780.pedido              %type;
  v_sequencia            				 fatu_780.sequencia           %type;
  v_nivel                				 fatu_780.nivel               %type;
  v_grupo                				 fatu_780.grupo               %type;
  v_subgrupo             				 fatu_780.subgrupo            %type;
  v_item                 				 fatu_780.item                %type;
  v_deposito             				 fatu_780.deposito            %type;
  v_lote                 				 fatu_780.lote                %type;
  v_nr_solicitacao_new       		 fatu_780.nr_solicitacao      %type;
  v_qtde_pedida_new      				 fatu_780.qtde_pedida         %type;
  v_qtde_sugerida_new    				 fatu_780.qtde_sugerida       %type;
  v_qtde_disponivel_new  				 fatu_780.qtde_disponivel     %type;
  v_nr_solicitacao_old       		 fatu_780.nr_solicitacao      %type;
  v_qtde_pedida_old      				 fatu_780.qtde_pedida         %type;
  v_qtde_sugerida_old    				 fatu_780.qtde_sugerida       %type;
  v_qtde_disponivel_old  				 fatu_780.qtde_disponivel     %type;
  v_nome_programa                fatu_780.nome_programa       %type;
  v_ref_original_new             fatu_780.ref_original        %type;
  v_ref_original_old             fatu_780.ref_original        %type;
  v_seq_conjunto_old             fatu_780.seq_conjunto%type;
  v_qtde_coleta_old              fatu_780.qtde_coleta%type;
  v_seq_conjunto_new             fatu_780.seq_conjunto%type;
  v_qtde_coleta_new              fatu_780.qtde_coleta%type;
  v_flag_controle_wms_old        fatu_780.flag_controle_wms   %type;
  v_flag_controle_wms_new        fatu_780.flag_controle_wms   %type;


  v_operacao            varchar(1);
  v_data_operacao       date;
  v_usuario_rede        varchar(20);
  v_maquina_rede        varchar(40);
  v_aplicativo          varchar(20);
  v_executa_trigger     number(1);

  v_sid                 number(9);
  v_empresa             number(3);
  v_usuario_systextil   varchar2(250);
  v_locale_usuario      varchar2(5);
            

begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      -- Grava a data/hora da inser��o do registro (log)
      v_data_operacao := sysdate();

      -- Encontra dados do usu�rio da rede, m�quina e aplicativo que esta atualizando a ficha
      inter_pr_dados_usuario (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                              v_usuario_systextil,   v_empresa,        v_locale_usuario  );

      v_nome_programa := inter_fn_nome_programa(v_sid);                         

      --alimenta as vari�veis new caso seja insert ou update
      if inserting or updating
      then
         if inserting
         then v_operacao := 'I';
         else v_operacao := 'U';
         end if;

         v_pedido               	:= :new.pedido;
         v_sequencia            	:= :new.sequencia;
         v_nivel                	:= :new.nivel;
         v_grupo                	:= :new.grupo;
         v_subgrupo             	:= :new.subgrupo;
         v_item                 	:= :new.item;
         v_deposito             	:= :new.deposito;
         v_lote                 	:= :new.lote;
         v_nr_solicitacao_new    := :new.nr_solicitacao;
         v_qtde_pedida_new      	:= :new.qtde_pedida;
         v_qtde_sugerida_new    	:= :new.qtde_sugerida;
         v_qtde_disponivel_new  	:= :new.qtde_disponivel;
         v_nome_programa         := :new.nome_programa;
         v_ref_original_new      := :new.ref_original;
         v_flag_controle_wms_new := :new.flag_controle_wms;
         v_seq_conjunto_new          := :new.seq_conjunto;            
         v_qtde_coleta_new           := :new.qtde_coleta;
      end if; --fim do if inserting or updating

      --alimenta as vari�veis old caso seja insert ou update
      if deleting or updating
      then
         if deleting
         then v_operacao      := 'D';
         else v_operacao      := 'U';
         end if;

         v_pedido               	:= :old.pedido;
         v_sequencia            	:= :old.sequencia;
         v_nivel                	:= :old.nivel;
         v_grupo                	:= :old.grupo;
         v_subgrupo             	:= :old.subgrupo;
         v_item                 	:= :old.item;
         v_deposito             	:= :old.deposito;
         v_lote                 	:= :old.lote;
         v_nr_solicitacao_old     := :old.nr_solicitacao;
         v_qtde_pedida_old      	:= :old.qtde_pedida;
         v_qtde_sugerida_old    	:= :old.qtde_sugerida;
         v_qtde_disponivel_old  	:= :old.qtde_disponivel;
         v_ref_original_old       := :old.ref_original;
         v_flag_controle_wms_old  := :old.flag_controle_wms;
         v_seq_conjunto_old          := :old.seq_conjunto;            
         v_qtde_coleta_old           := :old.qtde_coleta;

      end if; --fim do if inserting or updating

      --insere na FATU_780_log o registro.

      insert into fatu_780_log (
         pedido,                  sequencia,
         nivel,                   grupo,
         subgrupo,                item,
         deposito,                lote,
         nr_solicitacao_new,      qtde_pedida_new,
         qtde_sugerida_new,       qtde_disponivei_new,
         nr_solicitacao_old,      qtde_pedida_old,
         qtde_sugerida_old,       qtde_disponivel_old,
         operacao,                data_operacao,
         usuario_rede,            maquina_rede,
         aplicativo,              nome_programa,
         ref_original_new,        ref_original_old,
         flag_controle_wms_new,   flag_controle_wms_old,
         seq_conjunto_new,            qtde_coleta_new,
         seq_conjunto_old,            qtde_coleta_old)
      values (
         v_pedido,                v_sequencia,
         v_nivel,                 v_grupo,
         v_subgrupo,              v_item,
         v_deposito,              v_lote,
         v_nr_solicitacao_new,    v_qtde_pedida_new,
         v_qtde_sugerida_new,     v_qtde_disponivel_new,
         v_nr_solicitacao_old,    v_qtde_pedida_old,
         v_qtde_sugerida_old,     v_qtde_disponivel_old,
         v_operacao,              v_data_operacao,
         v_usuario_rede,          v_maquina_rede,
         v_aplicativo,            v_nome_programa,
         v_ref_original_new,      v_ref_original_old,
         v_flag_controle_wms_new, v_flag_controle_wms_old,
         v_seq_conjunto_new,          v_qtde_coleta_new,
         v_seq_conjunto_old,          v_qtde_coleta_old);

   end if;

end inter_tr_fatu_780_log;



-- ALTER TRIGGER "INTER_TR_FATU_780_LOG" ENABLE
 

/

exec inter_pr_recompile;

