CREATE OR REPLACE TRIGGER inter_tr_pedi_135_1
BEFORE INSERT or DELETE or UPDATE on pedi_135
for each row

declare
  v_con_serasa             number(01);
  v_qtde_aux               number(09);
  v_cnpj9                  number(09);
  v_cnpj4                  number(04);
  v_cnpj2                  number(02);    
  v_saldo_pedi             number(14,2);
  v_cod_empr               number(3);
  v_cod_rep                number(05);
  v_sub_regiao             number(04);
  v_cod_regiao             number(03);
  v_cod_cidade             number(05);  
  v_tipo_informacao        number(2);
  v_cod_informacao         number(9);
  v_analista               varchar2(10);
  v_senha_analista         varchar2(10);
  v_cod_emp_serasa         varchar2(08);
  v_sen_emp_serasa         varchar2(08);
  v_estado                 varchar2(02);
  v_fisica_juridica        number(1);  
  v_cond_pagto_venda       number(3);
  v_perc_vencimento        number(7,2);  
  v_vencimento             number(3);
  v_avista                 varchar2(1);
  v_nat_oper               number(3);
  v_nat_est                varchar(2);
  v_gera_dupl              number(1);
  v_sit_sera               number(1);
  v_executa_trigger        number(1);
  v_acum_cred_corporacao   number(1);
  v_grupo_economico        number(5);
  v_grupo_economico_ativo  number(1);
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      if inserting 
      then
         if :new.codigo_situacao <> 25 and :new.codigo_situacao <> 26 and 
            :new.codigo_situacao <> 45 and :new.codigo_situacao <> 38
         then 
            
            v_qtde_aux := 1;
             
            /*verifica se j� enviou*/
            select count(1) into v_qtde_aux
            from pedi_341
            where pedi_341.pedido_venda = :new.pedido_venda;
                     
            begin 
               select pedi_340.flag_retorno into v_sit_sera
               from pedi_340
               where pedi_340.pedido_venda = :new.pedido_venda;
               exception
                  when OTHERS then v_sit_sera := 1;
            end;            

            if  v_sit_sera > 0 and  v_sit_sera < 5 and v_qtde_aux = 0 
            then 
               /*Verifica os dados do pedido de venda*/
               select pedi_100.codigo_empresa,
                      pedi_100.cli_ped_cgc_cli9,
                      pedi_100.cli_ped_cgc_cli4,
                      pedi_100.cli_ped_cgc_cli2,
                      pedi_100.valor_saldo_pedi,
                      pedi_100.cod_rep_cliente,
                      pedi_100.cond_pgto_venda,
                      pedi_100.natop_pv_nat_oper,
                      pedi_100.natop_pv_est_oper
               into v_cod_empr, 
                    v_cnpj9,
                    v_cnpj4,
                    v_cnpj2,
                    v_saldo_pedi,
                    v_cod_rep,
                    v_cond_pagto_venda,
                    v_nat_oper,
                    v_nat_est
               from pedi_100
               where pedi_100.pedido_venda = :new.pedido_venda;
               
               /*verifica se possui analise por grupo*/
               begin
                 select fatu_500.acum_cred_corporacao
                 into v_acum_cred_corporacao
                 from fatu_500
                 where fatu_500.codigo_empresa = v_cod_empr;
                 exception
                    when OTHERS then v_acum_cred_corporacao := 0;
               end; 
               
               begin  
                 select pedi_010.grupo_economico
                 into   v_grupo_economico
                 from pedi_010
                 where pedi_010.cgc_9      = v_cnpj9
                 and pedi_010.cgc_4      = v_cnpj4
                 and pedi_010.cgc_2      = v_cnpj2;
                 exception
                    when OTHERS then v_grupo_economico := 0;
               end; 
                  
               begin
                 select pedi_220.ativo
                 into v_grupo_economico_ativo
                 from pedi_220
                 where pedi_220.codigo = v_grupo_economico
                 and   pedi_220.tipo   = 8;
                 exception
                    when OTHERS then v_grupo_economico_ativo := 0;
               end;
               
               if not (v_acum_cred_corporacao = 4 and v_grupo_economico > 0 and v_grupo_economico_ativo = 1)
               then
               
                   /* Verifica se o bloqueio analisa o Serasa*/ 
                   begin  
                      select hdoc_100.consulta_serasa into v_con_serasa
                      from hdoc_100
                      where hdoc_100.area_bloqueio      = 1
                       and  hdoc_100.sit_bloqueio       = :new.codigo_situacao
                       and  hdoc_100.usubloq_empr_usu   = v_cod_empr
                       and  hdoc_100.situacao_liberador = 0
                      group by  hdoc_100.consulta_serasa;
                      exception
                         when OTHERS then v_con_serasa := 0;                                 
                   end;
                   
                   /*Verifica se natureza emite duplicata*/
                   
                   begin
                      select pedi_080.emite_duplicata into v_gera_dupl
                      from pedi_080
                      where pedi_080.natur_operacao = v_nat_oper
                       and  pedi_080.estado_natoper = v_nat_est;
                      exception
                         when OTHERS then v_gera_dupl := 0;
                   end;
                   
                   if v_saldo_pedi = 0.00
                   then v_con_serasa := 0;
                   end if;
                   
                   /*somente se gera duplicata deve analisar o Serasa*/
                   if v_gera_dupl <> 1
                   then v_con_serasa := 0;
                   end if;
                   
                   if v_con_serasa <> 0
                   then 
                      /*Identifica��o da senha da empresa se n�o existir n�o far� o envio*/
                      begin 
                         select fatu_502.cod_emp_serasa,
                                fatu_502.senha_emp_serasa
                         into   v_cod_emp_serasa,
                                v_sen_emp_serasa
                         from fatu_502
                         where fatu_502.codigo_empresa = v_cod_empr;
                         exception
                            when OTHERS then 
                               raise_application_error (-20000, 'Par�metro empresa/senha do Serasa n�o foi informado nos par�metros de empresa. Cod. Emp.: '||v_cod_empr);
                      end;
                                     
                      if v_cod_emp_serasa is null or v_sen_emp_serasa is null
                      then 
                         raise_application_error (-20000, 'Par�metro empresa/senha do Serasa n�o foi informado nos par�metros de empresa.');
                      end if;
                   
                      /* Identifica��o do tipo de informacao analista*/
                      begin 
                         select MAX(inte_595.tipo_informacao) into v_tipo_informacao
                         from inte_595;
                         exception
                            when OTHERS then 
                              raise_application_error (-20000, 'Distribui��o de analistas do Serasa n�o cadastrada.');
                      end;
                      
                      /*Extra��o do tipo de informa��o*/
                      
                      /*REPRESENTANTE*/
                      if v_tipo_informacao = 1
                      then 
                        begin
                           v_cod_informacao := v_cod_rep;
                           v_vencimento := 999;
                        end;   
                      end if;
      
                      /*CLIENTE*/
                      if v_tipo_informacao = 2
                      then v_cod_informacao := v_cnpj9;
                      end if;
                      
                      /*VERIFICA O ESTADO E O TIPO DO CLIENTE*/
                        begin
                           select pedi_010.fisica_juridica,
                                  basi_160.estado
                           into   v_fisica_juridica,
                                  v_estado
                           from pedi_010,basi_160
                           where pedi_010.cgc_9      = v_cnpj9
                             and pedi_010.cgc_4      = v_cnpj4
                             and pedi_010.cgc_2      = v_cnpj2
                             and basi_160.cod_cidade = pedi_010.cod_cidade;
                             exception
                                when OTHERS then 
                                   v_estado := 'XX';
                                   v_fisica_juridica := 9;
                        end;               
                      
                      /*VERIFICA SE A CONDICAO DE PAGAMENTO � A VISTA*/
                                   
                         begin
                            select pedi_075.vencimento,
                                   pedi_075.perc_vencimento
                            into   v_vencimento,
                                   v_perc_vencimento
                            from pedi_075      
                            where pedi_075.condicao_pagto = v_cond_pagto_venda;
                            exception
                               when OTHERS then
                                  v_vencimento := 999;
                                  v_perc_vencimento := 0.00;
                         end;               
                  
                      /*SUB REGIAO, REGIAO,CIDADE*/
                      if v_tipo_informacao > 2
                      then 
                         begin 
                            select pedi_010.cod_cidade,
                                   pedi_010.sub_regiao                            
                            into   v_cod_cidade,
                                   v_sub_regiao                            
                            from pedi_010
                            where pedi_010.cgc_9 = v_cnpj9
                             and  pedi_010.cgc_4 = v_cnpj4
                             and  pedi_010.cgc_2 = v_cnpj2;
                            exception
                               when OTHERS then v_con_serasa := 0;
                         end;
                      
                         /*SUBREGIAO*/
                         if v_tipo_informacao = 3 and v_con_serasa = 1
                         then v_cod_informacao := v_sub_regiao;
                         end if;
                      
                         /*CIDADE*/
                         if v_tipo_informacao = 5 and v_con_serasa = 1
                         then v_cod_informacao := v_cod_cidade;
                         end if;
                      
                         /*REGIAO*/               
                         if v_tipo_informacao = 4 and v_con_serasa = 1
                         then 
                            begin
                               select pedi_043.codigo_regiao
                               into   v_cod_regiao
                               from pedi_043
                               where pedi_043.sub_regiao = v_sub_regiao;
                               exception
                                  when OTHERS then v_con_serasa := 0;
                            end;
                   
                            if v_con_serasa = 1 
                            then v_cod_informacao := v_cod_regiao;
                            end if;                  
                        end if;
                      end if;   /*tipo informacao > 2*/
                   
                      /*Seleciona o analista e a sua senha*/
                      begin
                         select inte_595.cod_analista,
                                inte_590.senha_analista
                         into   v_analista,
                                v_senha_analista
                         from   inte_590,inte_595
                         where  inte_590.cod_analista    = inte_595.cod_analista
                           and  inte_595.cod_informacao  = v_cod_informacao
                           and  inte_595.tipo_informacao = v_tipo_informacao;     
                         exception
                             when OTHERS then 
                             raise_application_error (-20000, 'Par�metro Analista/Senha n�o foi informado nos par�metros do Serasa. Cod. Inf.: '||v_cod_informacao||' / Tipo Inf.: '||v_tipo_informacao);
                      end;            
                      
                      /*Verifica se pedido � avista*/
                      v_avista := 'n';
                      if v_perc_vencimento = 100 and v_vencimento = 0
                      then  v_avista := 's';
                      end if;
                      
                      /* 
                        Se existe bloqueio grava a tabela auxiliar para o serasa
                        Conforme SS 83325/001 ao inves de gravar na pedi_340 ira gravar na tabela pedi_341
                      */
                                     
                      if     v_estado          <> 'EX'      
                         and v_fisica_juridica <>   1 
                         and  v_avista         <> 's'
                      then                  
                         if v_qtde_aux = 0
                         then 
                            begin 
                               INSERT INTO pedi_341
                                   (cnpj9,
                                    cnpj4,
                                    cnpj2,
                                    pedido_venda,
                                    valor_pedido,
                                    data_geracao,
                                    cod_emp_serasa,
                                    senha_emp_serasa,
                                    cod_usuario_serasa,
                                    senha_usuario_serasa)
                                 VALUES
                                   (v_cnpj9,
                                    v_cnpj4,
                                    v_cnpj2,
                                    :new.pedido_venda,
                                    trunc(v_saldo_pedi),
                                    trunc(sysdate,'DD'),
                                    v_cod_emp_serasa,
                                    v_sen_emp_serasa,                        
                                    v_analista,
                                    v_senha_analista);
                                    
                            /*
                               INSERT INTO pedi_340
                                  ( cnpj9,
                                    cnpj4,
                                    cnpj2,
                                    pedido_venda,
                                    valor_pedido, 
                                    data_consulta,
                                    cod_emp_serasa,
                                    senha_emp_serasa,
                                    cod_usuario_serasa,
                                    senha_usuario_serasa)
                                VALUES
                                   ( v_cnpj9,
                                     v_cnpj4,
                                     v_cnpj2,
                                     :new.pedido_venda,           
                                     trunc(v_saldo_pedi),
                                     trunc(sysdate,'DD'),
                                     v_cod_emp_serasa,
                                     v_sen_emp_serasa,                        
                                     v_analista,
                                     v_senha_analista);
                                */     
                               exception
                               when OTHERS then 
                                 raise_application_error (-20000, 'N�o atualizou a tabela pedi_341 do relacionamento com Serasa.');                      
                            end;
                         else 
                            begin
                              /*
                               update pedi_340
                                  set
                                    flag_retorno         = 0,
                                    cod_emp_serasa       = v_cod_emp_serasa,
                                    senha_emp_serasa     = v_sen_emp_serasa,
                                    cod_usuario_serasa   = v_analista,
                                    senha_usuario_serasa = v_senha_analista
                               where pedi_340.pedido_venda = :new.pedido_venda;
                               */
                               update pedi_341 set
                                      flag_geracao = 0
                                where pedi_341.pedido_venda = :new.pedido_venda;
                            exception
                              when OTHERS then 
                                raise_application_error (-20000, 'N�o alterou a tabela pedi_341 do relacionamento com Serasa.');
                            end; 
                         end if; /*qtde*/
                      end if; /*Insere ou n�o no serasa*/
                   end if; /*bloqueio serasa*/
                end if; /*analise por grupo*/
            end if; /*quantidade de registros*/
         end if; /*cod_bloqueio*/
      end if; /* Inserting */ 
      
      if updating
      then
         if :old.flag_liberacao  = 'N' and :old.flag_liberacao <> 'N'
         then 
            /*Verifica se pedido foi enviado e n�o processado*/  
            select count(1) into v_qtde_aux
            from pedi_341
            where pedi_341.pedido_venda = :new.pedido_venda
            and pedi_341.flag_geracao   = 0;
            
            select pedi_100.codigo_empresa into v_cod_empr
            from pedi_100
            where pedi_100.pedido_venda = :new.pedido_venda;            
                                    
            /*Verifica se � bloqueio do Serasa*/
            begin
               select  1  into v_con_serasa
               from hdoc_100
               where hdoc_100.area_bloqueio      = 1
                and  hdoc_100.sit_bloqueio       = :new.codigo_situacao
                and  hdoc_100.usubloq_empr_usu   = v_cod_empr
                and  hdoc_100.situacao_liberador = 0
                and  hdoc_100.consulta_serasa = 1 
                group by hdoc_100.sit_bloqueio;
            exception
              when OTHERS then 
                v_con_serasa := 0;
            end;
             
             if v_con_serasa >0 and v_qtde_aux > 0 
             then
                /*Se tem bloqueio e se j� passou elimina o pedido e se bloquaio foi liberado*/
               delete pedi_341
               where pedi_341.pedido_venda = :new.pedido_venda;  
            end if;       
         
         
         end if; /*altera flag*/
      
      
      end if ; /*updating*/
      
      if deleting 
      then
      
         /*Verifica os dados do pedido de venda*/
         select pedi_100.codigo_empresa  into v_cod_empr from pedi_100
         where pedi_100.pedido_venda = :old.pedido_venda;
      
         /* Verifica se o bloqueio analisa o Serasa*/ 
         begin  
            select hdoc_100.consulta_serasa into v_con_serasa
              from hdoc_100
             where hdoc_100.area_bloqueio      = 1
               and hdoc_100.sit_bloqueio       = :old.codigo_situacao
               and hdoc_100.usubloq_empr_usu   = v_cod_empr
               and hdoc_100.situacao_liberador = 0
             group by  hdoc_100.consulta_serasa;
         exception
           when OTHERS then 
             v_con_serasa := 0;
         end;
            
         if v_con_serasa = 1
         then      
            /*
            select count(pedi_340.pedido_venda) into v_qtde_aux
            from pedi_340
            where pedi_340.pedido_venda = :old.pedido_venda;
            */
            select count(pedi_341.pedido_venda) into v_qtde_aux
              from pedi_341
             where pedi_341.pedido_venda = :old.pedido_venda;

            if v_qtde_aux > 0
            then 
               DELETE from pedi_341
               where pedido_venda = :old.pedido_venda;
               /*
               DELETE  pedi_340
               where pedido_venda = :old.pedido_venda;         
               */
            end if;
         end if;
      end if;
   end if;

end inter_tr_pedi_135_1;

/

exec inter_pr_recompile;
