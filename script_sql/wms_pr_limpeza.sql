create or replace procedure wms_pr_limpeza is
BEGIN
   
   begin
      /*limpar as tabelas de retorno para o Syst�xtil*/
      delete from inte_wms_tags
      where inte_wms_tags.flag_integracao_st = 1;
   exception
      when others then
         raise_application_error(-20000, 'N�o excluiu da tabela INTE_WMS_TAGS.' || Chr(10) || SQLERRM);
   end;

   begin
      /*limpar as tabelas de retorno para o Syst�xtil*/
      delete from inte_wms_vol_itens
      where inte_wms_vol_itens.flag_integracao_st = 1;
   exception
      when others then
         raise_application_error(-20000, 'N�o excluiu da tabela INTE_WMS_VOL_ITENS.' || Chr(10) || SQLERRM);
   end;

   begin
      /*limpar as tabelas de retorno para o Syst�xtil*/
      delete from inte_wms_volumes
      where inte_wms_volumes.flag_integracao_st = 1;
   exception
      when others then
         raise_application_error(-20000, 'N�o excluiu da tabela INTE_WMS_VOLUMES.' || Chr(10) || SQLERRM);
   end;

   begin
      /*limpar as tabelas de retorno para o Syst�xtil*/
      delete from inte_wms_itens_ped
      where exists (select 1 from inte_wms_pedidos
                    where inte_wms_pedidos.numero_pedido = inte_wms_itens_ped.numero_pedido
                      and inte_wms_pedidos.flag_importacao_st = 1);
   exception
      when others then
         raise_application_error(-20000, 'N�o excluiu da tabela INTE_WMS_ITENS_PED.' || Chr(10) || SQLERRM);
   end;

   begin
      /*limpar as tabelas de retorno para o Syst�xtil*/
      delete from inte_wms_pedidos
      where inte_wms_pedidos.flag_importacao_st = 1;
   exception
      when others then
         raise_application_error(-20000, 'N�o excluiu da tabela INTE_WMS_PEDIDOS.' || Chr(10) || SQLERRM);
   end;

   commit WORK;

END wms_pr_limpeza;

/

exec inter_pr_recompile;

/* versao: 2 */


 exit;


 exit;
