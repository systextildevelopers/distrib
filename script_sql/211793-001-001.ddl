create table PCPB_421
(
  transacao_producao NUMBER(3) default 0,
  transacao_estorno  NUMBER(3) default 0,
  dep_primeira       NUMBER(3) default 0,
  dep_perda          NUMBER(3) default 0,
  dep_consumo_mp     NUMBER(3) default 0,
  layout             VARCHAR2(15) default '',
  grupo_maquina      VARCHAR2(4) default '',
  sub_maquina        VARCHAR2(3) default '',
  cod_empresa        NUMBER(3) default 0 not null,
  periodo_producao   NUMBER(4) default 0
);

/

exec inter_pr_recompile;
