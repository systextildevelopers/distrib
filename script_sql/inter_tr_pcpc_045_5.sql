CREATE OR REPLACE TRIGGER inter_tr_pcpc_045_5
BEFORE INSERT ON pcpc_045
FOR EACH ROW

DECLARE
  tmpInt    number := 0;

  v_nivel   pcpc_040.proconf_nivel99%type;
  v_ref     pcpc_040.proconf_grupo%type;
  v_tam     pcpc_040.proconf_subgrupo%type;
  v_cor     pcpc_040.proconf_item%type;
  v_alt     pcpc_020.alternativa_peca%type;
  v_rot     pcpc_020.roteiro_peca%type;
  v_tempo_unitario_new number;
  
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
BEGIN

-- Dados do usuario logado
inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                        ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

if inserting
then
   if :new.TURNO_PRODUCAO > 0 and :new.CODIGO_FAMILIA > 0
   then
      begin
         SELECT nvl(count(*),0)
         INTO tmpInt
         FROM pcpc_610
         WHERE pcpc_610.DATA_OPERADORAS = :new.DATA_PRODUCAO
           AND pcpc_610.TURNO_FAMILIA   = :new.TURNO_PRODUCAO
           AND pcpc_610.CODIGO_FAMILIA  = :new.CODIGO_FAMILIA;
      end;

      IF (tmpInt = 0)
      THEN
         begin
            INSERT INTO pcpc_610
               (DATA_OPERADORAS,
                TURNO_FAMILIA,
                CODIGO_FAMILIA,
                NUMERO_OPERADORAS)
            VALUES
               (:new.DATA_PRODUCAO,
                :new.TURNO_PRODUCAO,
                :new.CODIGO_FAMILIA,
                :new.NR_OPERADORES);
         end;
      END IF;
   END IF;

   begin
      select distinct
             pcpc_040.proconf_nivel99,
             pcpc_040.proconf_grupo,
             pcpc_040.proconf_subgrupo,
             pcpc_040.proconf_item
      into   v_nivel,
             v_ref,
             v_tam,
             v_cor
      from pcpc_040
      where pcpc_040.periodo_producao = :new.pcpc040_perconf
        and pcpc_040.ordem_confeccao  = :new.pcpc040_ordconf
        and pcpc_040.ordem_producao   = :new.ordem_producao
        and rownum = 1;
   exception when no_data_found then
         NULL;
   end;

   begin
      select pcpc_020.alternativa_peca,
             pcpc_020.roteiro_peca
      into   v_alt,
             v_rot
      from pcpc_020
      where pcpc_020.ordem_producao = :new.ordem_producao;
   exception when no_data_found then
         NULL;
   end;

	--Busca o tempo unitario do painel de confeccao pette
	v_tempo_unitario_new := inter_fn_get_novo_tempo_painel ( :new.ordem_producao,
															:new.pcpc040_perconf,
															:new.pcpc040_ordconf,
															:new.pcpc040_estconf);
   -- Neylor - Pettenati.
   -- Carrega os minutos unitários por peça no apontamento de produção.
   -- Informação para análise de eficiência real, pegando as peças minuto
   -- de quando foi apontada a produção e não o atual que está no roteiro
   -- porque o tempo que está no roteiro já pode ter sofrido alterações.
   if v_tempo_unitario_new is null or v_tempo_unitario_new = 0 
   then
	   :new.minutos_peca := inter_fn_get_tempo_estagio (v_nivel,
														v_ref,
														v_tam,
														v_cor,
														v_alt,
														v_rot,
														:new.pcpc040_estconf,
														0);
	else 
		:new.minutos_peca := v_tempo_unitario_new;
	end if;

end if;

declare
    controleProcessoBlocoK  number;
begin
    select count(*)
    into controleProcessoBlocoK
    from empr_007
    where param = 'obrf.controleProcessoBlocoK' 
    and   default_int = 1;
    
    if  controleProcessoBlocoK > 0 
    and inserting
    then
       declare
          v_cod_empresa_produtiva   pcpc_040.cod_empresa_produtiva%type;
          v_centro_custo            basi_180.centro_custo%type;
          v_local_entrega           basi_185.local_entrega%type;
          v_valEmpresaCelula        number; 
       begin
          begin
              select nvl(min(pcpc_040.cod_empresa_produtiva),0)
              into  v_cod_empresa_produtiva
              from pcpc_040
              where pcpc_040.periodo_producao = :new.pcpc040_perconf
                and pcpc_040.ordem_confeccao  = :new.pcpc040_ordconf
                and pcpc_040.codigo_estagio   = :new.pcpc040_estconf
                and pcpc_040.ordem_producao   = :new.ordem_producao;
          exception when OTHERS then
              v_cod_empresa_produtiva := 0;
          end;
          
          begin
              select count(*)
              into v_valEmpresaCelula
              from empr_008
              where empr_008.codigo_empresa = v_cod_empresa_produtiva
              and   empr_008.param          = 'pcpc.valEmpresaCelula'
              and   empr_008.val_int        = 1;
          exception when OTHERS then
              v_valEmpresaCelula := 0;
          end;
          
          if :new.codigo_familia > 0 
          and v_cod_empresa_produtiva > 0 
          and v_valEmpresaCelula > 0 then
              begin
                  select basi_180.centro_custo
                  into v_centro_custo
                  from basi_180
                  where basi_180.divisao_producao = :new.codigo_familia;
              exception when OTHERS then
                  v_centro_custo := 0;
              end;
              
              if (v_centro_custo > 0) then
                  begin
                      select basi_185.local_entrega
                      into v_local_entrega
                      from basi_185
                      where basi_185.centro_custo = v_centro_custo;
                  exception when OTHERS then
                      v_centro_custo := 0;
                  end;
              end if;
              
              if (v_cod_empresa_produtiva > 0
              and v_local_entrega <> v_cod_empresa_produtiva) then
                   raise_application_error(-20000,'ATENCAO! A empresa produtiva do estágio é diferente da empresa do centro de custo da célula.');
              end if;
          end if;
       exception when OTHERS then
          v_cod_empresa_produtiva := 0;
       end;
    end if;
exception when OTHERS then
    null;
end;

declare
    controleProcessoBlocoK  number;
begin
    select count(*)
    into controleProcessoBlocoK
    from empr_007
    where param = 'obrf.controleProcessoBlocoK' 
    and   default_int = 1;
    
    if  controleProcessoBlocoK > 0 
    and inserting
    then
       declare
          v_cod_empresa_produtiva   pcpc_040.cod_empresa_produtiva%type;
          v_centro_custo            basi_180.centro_custo%type;
          v_local_entrega           basi_185.local_entrega%type;
          v_valEmpresaCelula        number; 
       begin
          begin
              select nvl(min(pcpc_040.cod_empresa_produtiva),0)
              into  v_cod_empresa_produtiva
              from pcpc_040
              where pcpc_040.periodo_producao = :new.pcpc040_perconf
                and pcpc_040.ordem_confeccao  = :new.pcpc040_ordconf
                and pcpc_040.codigo_estagio   = :new.pcpc040_estconf
                and pcpc_040.ordem_producao   = :new.ordem_producao;
          exception when OTHERS then
              v_cod_empresa_produtiva := 0;
          end;
          
          begin
              select count(*)
              into v_valEmpresaCelula
              from empr_008
              where empr_008.codigo_empresa = v_cod_empresa_produtiva
              and   empr_008.param          = 'pcpc.valEmpresaCelula'
              and   empr_008.val_int        = 1;
          exception when OTHERS then
              v_valEmpresaCelula := 0;
          end;
          
          if :new.codigo_familia > 0 
          and v_cod_empresa_produtiva > 0 
          and v_valEmpresaCelula > 0 then
              begin
                  select basi_180.centro_custo
                  into v_centro_custo
                  from basi_180
                  where basi_180.divisao_producao = :new.codigo_familia;
              exception when OTHERS then
                  v_centro_custo := 0;
                  v_local_entrega := 0;
              end;
              
              if v_centro_custo = 0
              then
                   raise_application_error(-20000,'ATENCAO! O centro de custo da c�lula est� zerado.');
              end if;
              
              if (v_centro_custo > 0) then
                  begin
                      select basi_185.local_entrega
                      into v_local_entrega
                      from basi_185
                      where basi_185.centro_custo = v_centro_custo;
                  exception when OTHERS then
                      v_local_entrega := 0;
                  end;
              end if;
              
              if v_local_entrega = 0 
              then
                   raise_application_error(-20000,'ATENCAO! A empresa do centro de custo da c�lula est� zerada.');
              end if;
              
              if  v_local_entrega <> ws_empresa
              then
                   raise_application_error(-20000,'ATENCAO! A empresa logada � diferente da empresa do centro de custo da c�lula.');
              end if;
              
              update pcpc_040
              set   pcpc_040.cod_empresa_produtiva  = v_local_entrega
              where pcpc_040.periodo_producao       = :new.pcpc040_perconf
                and pcpc_040.ordem_confeccao        = :new.pcpc040_ordconf
                and pcpc_040.codigo_estagio         = :new.pcpc040_estconf
                and pcpc_040.ordem_producao         = :new.ordem_producao;
          end if;
       exception when OTHERS then
          v_cod_empresa_produtiva := 0;
       end;
    end if;
exception when OTHERS then
    null;
end;

END inter_tr_pcpc_045_5;

/
