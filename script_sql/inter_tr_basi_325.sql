
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_325" 
   after insert
      or delete
on basi_325
for each row

declare
   v_total_28    number;
   v_total_27    number;

   v_codigo_projeto_n basi_001.codigo_projeto%type;
   v_seq_projeto_n    basi_330.seq_projeto%type;

   v_codigo_projeto_o basi_001.codigo_projeto%type;
   v_seq_projeto_o    basi_330.seq_projeto%type;

   v_nivel_produto_n  basi_001.nivel_produto%type;
   v_grupo_produto_n  basi_001.grupo_produto%type;

   v_nivel_produto_o  basi_001.nivel_produto%type;
   v_grupo_produto_o  basi_001.grupo_produto%type;

   tem_projeto     varchar2(1);
   tem_solicitacao varchar2(1);

begin
   --Quando for iserida uma nova solicitacao de Desenvolvimento de Cor
   --sera inserida na tabela de situacao do componente*/
   tem_projeto     := 'S';
   tem_solicitacao := 'S';

   if inserting
   then
     begin
        select basi_330.codigo_projeto, basi_330.seq_projeto
        into   v_codigo_projeto_n,        v_seq_projeto_n
        from basi_330
        where basi_330.cod_solicitacao = :new.cod_solicitacao
          and rownum                   = 1;
     exception when OTHERS then
        tem_solicitacao := 'N';
     end;

     if tem_solicitacao = 'S'
     then
        begin
            select basi_001.nivel_produto,    basi_001.grupo_produto
            into   v_nivel_produto_n,         v_grupo_produto_n
            from basi_001
            where basi_001.codigo_projeto = v_codigo_projeto_n;
         exception when OTHERS then
            tem_projeto := 'N';
         end;

         if tem_projeto = 'S'
         then
            begin
               INSERT INTO basi_027 (
                  nivel_item,                   grupo_item,
                  subgrupo_item,                item_item,
                  alt_item,

                  nivel_componente,             grupo_componente,
                  subgrupo_componente,          item_componente,

                  cnpj_cliente9,                cnpj_cliente4,
                  cnpj_cliente2,
                  codigo_projeto,               sequencia_projeto,
                  cod_solicitacao,              cod_amostra

               ) VALUES (
                  v_nivel_produto_n,            v_grupo_produto_n,
                  '000',                        '000000',
                   0,

                  :new.nivel_receita,           :new.grupo_receita,
                  :new.subgrupo_receita,        :new.item_receita,

                  0,                            0,
                  0,
                  v_codigo_projeto_n,           v_seq_projeto_n,
                  :new.cod_solicitacao,         :new.cod_amostra);
            exception when OTHERS then
               raise_application_error (-20000, 'Nao incluiu na tabela (BASI_027, inter_tr_basi_325)');
            end;

            begin
               v_total_28 := 0;

               select count(1)
               into v_total_28
               from basi_028
               where basi_028.codigo_projeto     = v_codigo_projeto_n
                 and basi_028.sequencia_projeto  = v_seq_projeto_n
                 and basi_028.cod_solicitacao    = :new.cod_solicitacao
                 and basi_028.cod_amostra        = :new.cod_amostra
                 and basi_028.tipo_aprovacao     = 3; --APROVACAO POR COR;
            end;

            if v_total_28 = 0
            then
               begin
                  INSERT INTO basi_028(
                    nivel_componente,                        grupo_componente,
                    subgrupo_componente,                     item_componente,

                    situacao_componente,
                    codigo_projeto,                          sequencia_projeto,
                    cod_solicitacao,                         cod_amostra,
                    tipo_aprovacao
                   ) VALUES (
                    :new.nivel_receita,                     :new.grupo_receita,
                    :new.subgrupo_receita,                  :new.item_receita,

                    0,
                    v_codigo_projeto_n,                     v_seq_projeto_n,
                    :new.cod_solicitacao,                   :new.cod_amostra,
                    3);
               exception when OTHERS then
                  raise_application_error (-20000, 'Nao incluiu na tabela (BASI_028, inter_tr_basi_325)');
               end;
            end if;
         end if;
     end if;
   end if;

   if deleting
   then
     begin
        select basi_330.codigo_projeto, basi_330.seq_projeto
        into   v_codigo_projeto_o,        v_seq_projeto_o
        from basi_330
        where basi_330.cod_solicitacao = :old.cod_solicitacao;
     exception when OTHERS then
        tem_solicitacao := 'N';
     end;

     if tem_solicitacao = 'S'
     then
        begin
            select basi_001.nivel_produto,    basi_001.grupo_produto
            into   v_nivel_produto_o,         v_grupo_produto_o
            from basi_001
            where basi_001.codigo_projeto = v_codigo_projeto_o;
        exception when OTHERS then
            tem_projeto := 'N';
        end;

        if tem_projeto = 'S'
        then
           begin
              v_total_27 := 0;

              select count(1)
              into v_total_27
              from basi_027
              where basi_027.codigo_projeto      = v_codigo_projeto_o
                and basi_027.sequencia_projeto   = v_seq_projeto_o
                and basi_027.cod_solicitacao     = :old.cod_solicitacao
                and basi_027.cod_amostra         = :old.cod_amostra;
           end;

           if v_total_27 > 0
           then
              begin
                 delete from basi_027
                 where basi_027.codigo_projeto      = v_codigo_projeto_o
                   and basi_027.sequencia_projeto   = v_seq_projeto_o
                   and basi_027.cod_solicitacao     = :old.cod_solicitacao
                   and basi_027.cod_amostra         = :old.cod_amostra;
              exception when OTHERS then
                 raise_application_error (-20000, 'Nao excluiu da tabela (BASI_027, inter_tr_basi_325)');
              end;
           end if;

           begin
              v_total_28 := 0;

              select count(1)
              into v_total_28
              from basi_028
              where basi_028.codigo_projeto      = v_codigo_projeto_o
                and basi_028.sequencia_projeto   = v_seq_projeto_o
                and basi_028.cod_solicitacao     = :old.cod_solicitacao
                and basi_028.cod_amostra         = :old.cod_amostra
                and basi_028.tipo_aprovacao  = 3;   --APROVACAO POR COR;
           end;

           if v_total_28 > 0
           then
              begin
                 delete from basi_028
                 where basi_028.codigo_projeto      = v_codigo_projeto_o
                   and basi_028.sequencia_projeto   = v_seq_projeto_o
                   and basi_028.cod_solicitacao     = :old.cod_solicitacao
                   and basi_028.cod_amostra         = :old.cod_amostra
                   and basi_028.tipo_aprovacao  = 3;   --APROVACAO POR COR;
              exception when OTHERS then
                 raise_application_error (-20000, 'Nao excluiu da tabela (BASI_028, inter_tr_basi_330)');
              end;
           end if;
        end if;
     end if;
   end if;
end;

-- ALTER TRIGGER "INTER_TR_BASI_325" ENABLE
 

/

exec inter_pr_recompile;

