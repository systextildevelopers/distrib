declare
  i NUMBER := 0;

begin
   for nota in (select * from obrf_010 
                where obrf_010.nota_pendente is null)
   loop 
      update obrf_010 
        set nota_pendente = 0
      where obrf_010.documento     = nota.documento
        and obrf_010.serie         = nota.serie
        and obrf_010.cgc_cli_for_9 = nota.cgc_cli_for_9
        and obrf_010.cgc_cli_for_4 = nota.cgc_cli_for_4
        and obrf_010.cgc_cli_for_2 = nota.cgc_cli_for_2;
   
      i := i + 1;
      IF i > 1000 THEN
         COMMIT;
         i := 0;
      END IF;
   end loop;
end;
