alter table obrf_180 add cod_msg_exclui_icms_pc number(6) default 0;
alter table obrf_180 add seq_msg_exclui_icms_pc number(2) default 0;
alter table obrf_180 add loc_msg_exclui_icms_pc varchar2(1) default ' ';

exec inter_pr_recompile;
