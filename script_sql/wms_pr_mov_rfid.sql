create or replace procedure wms_pr_mov_rfid (p_cod_rfid         in  varchar2,
                                             p_operacao         in  varchar2,
                                             p_mensagem         in  varchar2) is
  v_tmp_int         number;
begin

   -- valida RFID se existe
   begin
      select 1
      into   v_tmp_int
      from inte_wms_rfid
      where inte_wms_rfid.rfid_caixa = p_cod_rfid
        and rownum                   <= 1;
   exception
      when no_data_found then
         raise_application_error(-20000, 'ATEN��O! O c�digo de RFID n�o existe na tabela INTE_WMS_RFID.');
   end;

   if p_operacao = 'A' --Libera��o para Armazenagem
   then

      begin
         update inte_wms_rfid
         set    inte_wms_rfid.flag                = 1, --Liberado para Armazenagem
                inte_wms_rfid.timestamp_liberacao = sysdate
         where  inte_wms_rfid.rfid_caixa          = p_cod_rfid;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);

      end;

   end if;

   if p_operacao = 'B' --Armazenamento
   then

      begin
         update inte_wms_rfid
         set    inte_wms_rfid.flag                  = 2, --Armazenado
                inte_wms_rfid.timestamp_armazenagem = sysdate
         where  inte_wms_rfid.rfid_caixa            = p_cod_rfid;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);

      end;

   end if;

   if p_operacao = 'C' --Rejei��o da Caixa
   then

      begin
         update inte_wms_rfid
         set    inte_wms_rfid.flag                  = 9, --Importa��o Rejeitada
                inte_wms_rfid.mensagem              = p_mensagem
         where  inte_wms_rfid.rfid_caixa            = p_cod_rfid;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);

      end;

   end if;

   if p_operacao = 'D' --Integra��o recebida, a conferir no check weight
   then

      begin
         update inte_wms_rfid
         set    inte_wms_rfid.flag                  = 8 --Aguardando resposta da integra��o
         where  inte_wms_rfid.rfid_caixa            = p_cod_rfid;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);

      end;

   end if;

   if p_operacao = 'E' --Livre para armazenar
   then

      begin
         update inte_wms_rfid
         set    inte_wms_rfid.flag                  = 0 --Livre para armazenar
         where  inte_wms_rfid.rfid_caixa            = p_cod_rfid;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela INTE_WMS_RFID.' || Chr(10) || SQLERRM);

      end;

   end if;

end wms_pr_mov_rfid;
/

exec inter_pr_recompile;

/* versao: 3 */


 exit;


 exit;

 exit;


 exit;

 exit;
