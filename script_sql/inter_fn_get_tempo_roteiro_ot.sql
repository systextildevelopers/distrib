
  CREATE OR REPLACE FUNCTION "INTER_FN_GET_TEMPO_ROTEIRO_OT" (p_ordem_tingimento   in number,
                                                          p_tipo_ordem         in varchar2,
                                                          p_depende_quantidade in number)
return number is
   -- p_depende_quantidade:
   -- Segundo Cadastro de Operacoes, coluna tipo_operacao,
   -- (1) Depende da quantidade,
   -- (2) Independe da quantidade.

   v_tempo_roteiro         number(15,4);
   v_estagio_tinturaria    number;
begin
   v_tempo_roteiro := 0.0000;

   begin
      select empr_001.estagio_tintur
        into v_estagio_tinturaria
        from empr_001;
      exception when no_data_found
      then v_estagio_tinturaria := 0;
   end;

   for reg_pcpb_100 in (select pcpb_020.pano_sbg_nivel99,  pcpb_020.pano_sbg_grupo,
                               pcpb_020.pano_sbg_subgrupo, pcpb_020.pano_sbg_item,
                               pcpb_020.alternativa_item,  pcpb_020.roteiro_opcional,
                               pcpb_020.qtde_quilos_prog,  pcpb_100.ordem_producao,
                               pcpb_010.grupo_maquina,     pcpb_010.subgrupo_maquina
                          from pcpb_100, pcpb_020, pcpb_010
                         where pcpb_100.ordem_agrupamento = p_ordem_tingimento
                           and pcpb_100.tipo_ordem        = p_tipo_ordem
                           and pcpb_100.ordem_producao    = pcpb_020.ordem_producao
                           and pcpb_020.ordem_producao    = pcpb_010.ordem_producao
                         order by pcpb_020.qtde_quilos_prog desc)
   loop
      v_tempo_roteiro := inter_fn_get_tempo_estagio (reg_pcpb_100.pano_sbg_nivel99,
                                                     reg_pcpb_100.pano_sbg_grupo,
                                                     reg_pcpb_100.pano_sbg_subgrupo,
                                                     reg_pcpb_100.pano_sbg_item,
                                                     reg_pcpb_100.alternativa_item,
                                                     reg_pcpb_100.roteiro_opcional,
                                                     v_estagio_tinturaria,
                                                     p_depende_quantidade);

      if p_depende_quantidade = 1
      then
         v_tempo_roteiro := v_tempo_roteiro * reg_pcpb_100.qtde_quilos_prog;
      end if;

      exit when v_tempo_roteiro > 0;
   end loop;

   return(v_tempo_roteiro);
end inter_fn_get_tempo_roteiro_ot;
 

/

exec inter_pr_recompile;

