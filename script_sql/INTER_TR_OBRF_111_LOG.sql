CREATE OR REPLACE TRIGGER inter_tr_OBRF_111_log
after insert or delete or update
on OBRF_111
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usu?rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);


 if inserting
 then
    begin

        insert into OBRF_111_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           PERIODO_FIM_OLD  /*8*/,
           PERIODO_FIM_NEW  /*9*/,
           COD_EMPRESA_OLD  /*10*/,
           COD_EMPRESA_NEW  /*11*/,
           SOMA_PARC_OLD    /*12*/,
           SOMA_PARC_NEW    /*13*/,
           ICMS_APROP_OLD   /*14*/,
           ICMS_APROP_NEW   /*15*/,
           SOM_ICMS_OC_OLD  /*16*/,
           SOM_ICMS_OC_NEW  /*17*/,
           INDICE_OLD       /*18*/,
           INDICE_NEW       /*19*/,
           SALDO_IN_ICMS_OLD/*20*/,
           SALDO_IN_ICMS_NEW/*21*/,
           PERIODO_INI_OLD  /*22*/,
           PERIODO_INI_NEW  /*23*/,
           VALOR_TOT_SAIDAS_OLD /*24*/,
           VALOR_TOT_SAIDAS_NEW /*25*/,
           SAIDAS_TRIB_OLD /*26*/,
           SAIDAS_TRIB_NEW /*27*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           null  /*8*/,
           :new.PERIODO_FIM  /*9*/,
           0  /*10*/,
           :new.COD_EMPRESA  /*11*/,
           0   /*12*/,
           :new.SOMA_PARC    /*13*/,
           0   /*14*/,
           :new.ICMS_APROP   /*15*/,
           0  /*16*/,
           :new.SOM_ICMS_OC /*17*/,
           0      /*18*/,
           :new.INDICE       /*19*/,
           0/*20*/,
           :new.SALDO_IN_ICMS/*21*/,
           null  /*22*/,
           :new.PERIODO_INI  /*23*/,
           0 /*24*/,
           :new.VALOR_TOT_SAIDAS/*25*/,
           0 /*26*/,
           :new.SAIDAS_TRIB /*27*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into OBRF_111_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           PERIODO_FIM_OLD  /*8*/,
           PERIODO_FIM_NEW  /*9*/,
           COD_EMPRESA_OLD  /*10*/,
           COD_EMPRESA_NEW  /*11*/,
           SOMA_PARC_OLD    /*12*/,
           SOMA_PARC_NEW    /*13*/,
           ICMS_APROP_OLD   /*14*/,
           ICMS_APROP_NEW   /*15*/,
           SOM_ICMS_OC_OLD  /*16*/,
           SOM_ICMS_OC_NEW  /*17*/,
           INDICE_OLD       /*18*/,
           INDICE_NEW       /*19*/,
           SALDO_IN_ICMS_OLD/*20*/,
           SALDO_IN_ICMS_NEW/*21*/,
           PERIODO_INI_OLD  /*22*/,
           PERIODO_INI_NEW  /*23*/,
           VALOR_TOT_SAIDAS_OLD /*24*/,
           VALOR_TOT_SAIDAS_NEW /*25*/,
           SAIDAS_TRIB_OLD /*26*/,
           SAIDAS_TRIB_NEW /*27*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.PERIODO_FIM  /*8*/,
           :new.PERIODO_FIM  /*9*/,
           :old.COD_EMPRESA  /*10*/,
           :new.COD_EMPRESA  /*11*/,
           :old.SOMA_PARC   /*12*/,
           :new.SOMA_PARC    /*13*/,
           :old.ICMS_APROP   /*14*/,
           :new.ICMS_APROP   /*15*/,
           :old.SOM_ICMS_OC  /*16*/,
           :new.SOM_ICMS_OC  /*17*/,
           :old.INDICE     /*18*/,
           :new.INDICE      /*19*/,
           :old.SALDO_IN_ICMS/*20*/,
           :new.SALDO_IN_ICMS/*21*/,
           :old.PERIODO_INI  /*22*/,
           :new.PERIODO_INI  /*23*/,
           :old.VALOR_TOT_SAIDAS /*24*/,
           :new.VALOR_TOT_SAIDAS /*25*/,
           :old.SAIDAS_TRIB /*26*/,
           :new.SAIDAS_TRIB /*27*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into OBRF_111_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           PERIODO_FIM_OLD  /*8*/,
           PERIODO_FIM_NEW  /*9*/,
           COD_EMPRESA_OLD  /*10*/,
           COD_EMPRESA_NEW  /*11*/,
           SOMA_PARC_OLD    /*12*/,
           SOMA_PARC_NEW    /*13*/,
           ICMS_APROP_OLD   /*14*/,
           ICMS_APROP_NEW   /*15*/,
           SOM_ICMS_OC_OLD  /*16*/,
           SOM_ICMS_OC_NEW  /*17*/,
           INDICE_OLD       /*18*/,
           INDICE_NEW       /*19*/,
           SALDO_IN_ICMS_OLD/*20*/,
           SALDO_IN_ICMS_NEW/*21*/,
           PERIODO_INI_OLD  /*22*/,
           PERIODO_INI_NEW  /*23*/,
           VALOR_TOT_SAIDAS_OLD /*24*/,
           VALOR_TOT_SAIDAS_NEW /*25*/,
           SAIDAS_TRIB_OLD /*26*/,
           SAIDAS_TRIB_NEW /*27*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.PERIODO_FIM  /*8*/,
           null  /*9*/,
           :old.COD_EMPRESA  /*10*/,
           0  /*11*/,
           :old.SOMA_PARC    /*12*/,
           0    /*13*/,
           :old.ICMS_APROP  /*14*/,
           0  /*15*/,
           :old.SOM_ICMS_OC  /*16*/,
           0  /*17*/,
           :old.INDICE       /*18*/,
           0      /*19*/,
           :old.SALDO_IN_ICMS/*20*/,
           0/*21*/,
           :old.PERIODO_INI  /*22*/,
           null  /*23*/,
           :old.VALOR_TOT_SAIDAS /*24*/,
           0 /*25*/,
           :old.SAIDAS_TRIB /*26*/,
           0 /*27*/
         );
    end;
 end if;
end inter_tr_OBRF_111_log;
/
