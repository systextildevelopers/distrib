create or replace trigger wms_tr_tags_log
before insert
or delete
or update of periodo_tag, ordem_prod_tag, ordem_conf_tag, sequencia_tag, 
	nivel_sku, grupo_sku, subgrupo_sku, item_sku, 
	nr_volume_st, nr_pedido, flag_integracao_st 
	on inte_wms_tags
for each row
declare

   v_periodo_tag                inte_wms_tags.periodo_tag           %type;
   v_ordem_prod_tag        	inte_wms_tags.ordem_prod_tag        %type;
   v_ordem_conf_tag        	inte_wms_tags.ordem_conf_tag        %type;
   v_sequencia_tag         	inte_wms_tags.sequencia_tag         %type;
   v_nivel_sku_new         	inte_wms_tags.nivel_sku      	    %type;
   v_nivel_sku_old         	inte_wms_tags.nivel_sku		    %type;
   v_grupo_sku_new         	inte_wms_tags.grupo_sku		    %type;
   v_grupo_sku_old         	inte_wms_tags.grupo_sku		    %type;
   v_subgrupo_sku_new      	inte_wms_tags.subgrupo_sku	    %type;
   v_subgrupo_sku_old      	inte_wms_tags.subgrupo_sku	    %type;
   v_item_sku_new          	inte_wms_tags.item_sku		    %type;
   v_item_sku_old          	inte_wms_tags.item_sku		    %type;
   v_nr_volume_st_new      	inte_wms_tags.nr_volume_st	    %type;
   v_nr_volume_st_old      	inte_wms_tags.nr_volume_st	    %type;
   v_nr_pedido_new         	inte_wms_tags.nr_pedido		    %type;
   v_nr_pedido_old         	inte_wms_tags.nr_pedido		    %type;
   v_flag_integracao_st_new	inte_wms_tags.flag_integracao_st    %type;
   v_flag_integracao_st_old	inte_wms_tags.flag_integracao_st    %type;
  								       
   v_sid                        number(9);			       
   v_empresa                    number(3);			       
   v_usuario_systextil          varchar2(250);		       
   v_locale_usuario             varchar2(5);		       
   v_nome_programa              varchar2(20);		       
   v_operacao                   varchar(1);
   v_data_operacao              date;
   v_usuario_rede               varchar(20);
   v_maquina_rede               varchar(40);
   v_aplicativo                 varchar(20);
begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();
   
   --alimenta as variaveis new caso seja insert ou update
   if inserting or updating
   then
      if inserting
      then v_operacao := 'i';
      else v_operacao := 'u';
      end if;
      
      v_periodo_tag               := :new.periodo_tag;       
      v_ordem_prod_tag        	  := :new.ordem_prod_tag;    
      v_ordem_conf_tag        	  := :new.ordem_conf_tag;    
      v_sequencia_tag         	  := :new.sequencia_tag;     
      v_nivel_sku_new         	  := :new.nivel_sku;	    
      v_grupo_sku_new         	  := :new.grupo_sku;         
      v_subgrupo_sku_new      	  := :new.subgrupo_sku;      
      v_item_sku_new          	  := :new.item_sku;          
      v_nr_volume_st_new      	  := :new.nr_volume_st;      
      v_nr_pedido_new         	  := :new.nr_pedido;         
      v_flag_integracao_st_new	  := :new.flag_integracao_st;
      
   end if; --fim do if inserting or updating
   
   --alimenta as variaveis old caso seja insert ou update
   if deleting or updating
   then
      if deleting
      then
         v_operacao      := 'd';
      else
         v_operacao      := 'u';
      end if;
      
      v_periodo_tag               := :old.periodo_tag;       
      v_ordem_prod_tag        	  := :old.ordem_prod_tag;    
      v_ordem_conf_tag        	  := :old.ordem_conf_tag;    
      v_sequencia_tag         	  := :old.sequencia_tag;     
      v_nivel_sku_old         	  := :old.nivel_sku;	    
      v_grupo_sku_old         	  := :old.grupo_sku;         
      v_subgrupo_sku_old      	  := :old.subgrupo_sku;      
      v_item_sku_old          	  := :old.item_sku;          
      v_nr_volume_st_old      	  := :old.nr_volume_st;      
      v_nr_pedido_old         	  := :old.nr_pedido;         
      v_flag_integracao_st_old	  := :old.flag_integracao_st;
                                                                
   end if; --fim do if deleting or updating
   
   
   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);
   
   
    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente
    
    -- insere as informa��es na tabela de log
    insert into inte_wms_tags_log (
       periodo_tag,                    ordem_prod_tag,         
       ordem_conf_tag,                 sequencia_tag,          
       nivel_sku_new,                  nivel_sku_old,
       grupo_sku_new,                  grupo_sku_old,
       subgrupo_sku_new,               subgrupo_sku_old,
       item_sku_new,                   item_sku_old,
       nr_volume_st_new,               nr_volume_st_old,
       nr_pedido_new,                  nr_pedido_old,
       flag_integracao_st_new,         flag_integracao_st_old,        
       operacao,                       data_operacao,             
       usuario_rede,                   maquina_rede,             
       aplicativo,                     nome_programa )
    values(
       v_periodo_tag,                  v_ordem_prod_tag,        
       v_ordem_conf_tag,               v_sequencia_tag,         
       v_nivel_sku_new,                v_nivel_sku_old,         
       v_grupo_sku_new,                v_grupo_sku_old,         
       v_subgrupo_sku_new,             v_subgrupo_sku_old,      
       v_item_sku_new,                 v_item_sku_old,          
       v_nr_volume_st_new,             v_nr_volume_st_old,      
       v_nr_pedido_new,                v_nr_pedido_old,           
       v_flag_integracao_st_new,       v_flag_integracao_st_old,
       v_operacao,                     v_data_operacao,             
       v_usuario_rede,                 v_maquina_rede,             
       v_aplicativo,                   v_nome_programa);
       
end wms_tr_tags_log;

/

execute inter_pr_recompile;

/* versao: 2 */


 exit;
