
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_013_LOG" 
   after insert or
         delete or
         update of  consumo_auxiliar,      consumo_componente,
                    codigo_estagio,        numbero_roteiro,
                    calcula_composicao,    percentual_perdas,
                    qtde_camadas,          tecido_principal,
                    seq_cor,               codigo_desenho,
                    tipo_destino
   on basi_013
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_cod_tipo_produto_n     varchar2(60);

   ws_tipo_produto_n         varchar2(60);

   ws_desc_projeto_n         varchar2(60);

   ws_desc_prod_n            varchar2(120);
   ws_desc_comp_o            varchar2(120);
   ws_desc_comp_n            varchar2(120);
   ws_narrativa              basi_010.narrativa%type;

   ws_cnpj_cliente9          basi_001.cnpj_cliente9%type;
   ws_cnpj_cliente4          basi_001.cnpj_cliente4%type;
   ws_cnpj_cliente2          basi_001.cnpj_cliente2%type;
   ws_situacao_componente    basi_028.situacao_componente%type;
   ws_teve                   varchar2(1);

   ws_tecido_principal       varchar2(30);

   long_aux                  varchar2(2000);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      -- ENCONTRA O TIPO_PRODUTO DA SEQUENCIA
      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = :new.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_n := '';
      end;

      -- DESCRICAO DO TIPO DE PRODUTO
      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n;
      exception when no_data_found then
         ws_tipo_produto_n := '';
      end;

     -- DESCRICAO DO PROJETO
      begin
         select basi_001.descricao_projeto,  basi_001.cnpj_cliente9,
                basi_001.cnpj_cliente4,      basi_001.cnpj_cliente2
         into ws_desc_projeto_n,             ws_cnpj_cliente9,
              ws_cnpj_cliente4,              ws_cnpj_cliente2
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_n := '';
         ws_cnpj_cliente9  := 0;
         ws_cnpj_cliente4  := 0;
         ws_cnpj_cliente2  := 0;
      end;

      -- DESCRICAO DO NOVO COMPONENTE
      inter_pr_desc_produto(:new.nivel_comp,
                            :new.grupo_comp,
                            :new.subgru_comp,
                            :new.item_comp,
                             ws_desc_comp_n,
                            ws_narrativa);

      -- DESCRICAO DO NOVO COMPONENTE - PRODUTO NIVEL 1
      inter_pr_desc_produto(:new.nivel_item,
                            :new.grupo_item,
                            :new.subgru_item,
                            :new.item_item,
                            ws_desc_prod_n,
                            ws_narrativa);

      -- Busca qual situacao do componente
        if :new.nivel_comp = '2'
      then
         begin
            select basi_028.situacao_componente
            into ws_situacao_componente
            from basi_028
            where basi_028.nivel_componente     = :new.nivel_comp
              and basi_028.grupo_componente     = :new.grupo_comp
              and basi_028.subgrupo_componente  = :new.subgru_comp
              and basi_028.item_componente      = :new.item_comp
              and basi_028.cnpj_cliente9        = ws_cnpj_cliente9
              and basi_028.cnpj_cliente4        = ws_cnpj_cliente4
              and basi_028.cnpj_cliente2        = ws_cnpj_cliente2
              and basi_028.tipo_aprovacao       = 1;   --APROVACAO POR COMPONENTE
         exception when no_data_found then
              ws_situacao_componente := 0;   -- 0 (Pendente), 1 (Enviada), 2 (Aprovada), 3 (Com Observacoes), 4 (Rejeitada), 5 (Cancelada).
         end;
      else
         begin
            select basi_028.situacao_componente
            into ws_situacao_componente
            from basi_028
            where basi_028.nivel_componente     = :new.nivel_comp
              and basi_028.grupo_componente     = :new.grupo_comp
              and basi_028.subgrupo_componente  = :new.subgru_comp
              and basi_028.item_componente      = :new.item_comp
              and basi_028.codigo_projeto       = :new.codigo_projeto
              and basi_028.sequencia_projeto    = :new.sequencia_subprojeto
              and basi_028.tipo_aprovacao       = 1;   --APROVACAO POR COMPONENTE
         exception when no_data_found then
              ws_situacao_componente := 0;   -- 0 (Pendente), 1 (Enviada), 2 (Aprovada), 3 (Com Observacoes), 4 (Rejeitada), 5 (Cancelada).
         end;
      end if;

      if :new.tecido_principal = 0
      then
         ws_tecido_principal := inter_fn_buscar_tag('lb34964#NAO SELECIONADO',ws_locale_usuario,ws_usuario_systextil);
      else
         ws_tecido_principal := inter_fn_buscar_tag('lb34102#SELECIONADO',ws_locale_usuario,ws_usuario_systextil);
      end if;

      begin
         INSERT INTO hist_100
            ( tabela,             operacao,
              data_ocorr,         aplicacao,
              usuario_rede,       maquina_rede,

              str01,              num05,
              num07,              num08,
              num09,

              num10,              num04,
              str07,              str08,
              str09,              str10,
              num06,
              long01
         ) VALUES (
              'BASI_013',               'I',
              sysdate,                  ws_aplicativo,
              ws_usuario_rede,          ws_maquina_rede,

              :new.codigo_projeto,      :new.sequencia_projeto,
              ws_cnpj_cliente9,
              ws_cnpj_cliente4,         ws_cnpj_cliente2,

              :new.sequencia_estrutura, :new.sequencia_variacao,
              :new.nivel_comp,          :new.grupo_comp,
              :new.subgru_comp,         :new.item_comp,
             ws_situacao_componente,
              inter_fn_buscar_tag('lb34894#ESTRUTURA DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) || '(013)' ||
              chr(10)                                               ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.codigo_projeto    ||
              ' - '                       || ws_desc_projeto_n      ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb08486#TIPO DE PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.sequencia_projeto ||
              ' - '                       || ws_cod_tipo_produto_n  ||
              ' - '                       || ws_tipo_produto_n      ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.nivel_item        ||
              '.'                         || :new.grupo_item        ||
              '.'                         || :new.subgru_item       ||
              '.'                         || :new.item_item         ||
              ' - '                       || ws_desc_prod_n         ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb00442#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || to_char(:new.alternativa_produto,'00')   ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb08191#ROTEIRO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || to_char(:new.numbero_roteiro,'00')       ||
              chr(10)                                               ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb00523#COMPONENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || :new.sequencia_estrutura ||
              '.'                         || :new.sequencia_variacao  ||
              ' '                         || :new.nivel_comp          ||
              '.'                         || :new.grupo_comp          ||
              '.'                         || :new.subgru_comp         ||
              '.'                         || :new.item_comp           ||
              ' - '                       || ws_desc_comp_n           ||
              chr(10)                                                 ||
              inter_fn_buscar_tag('lb34966#CONSUMO AUXILIAR:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.consumo_auxiliar  ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb00064#CONSUMO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.consumo_componente ||
              chr(10)                                                ||
              inter_fn_buscar_tag('lb00680#ESTAaGIO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.codigo_estagio     ||
              chr(10)                                                ||
              inter_fn_buscar_tag('lb32279#CALCULA',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              inter_fn_buscar_tag('lb00528#COMPOSICAO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.calcula_composicao ||
              chr(10)                                                ||
              inter_fn_buscar_tag('lb01280#% PERDA:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.percentual_perdas  ||
              chr(10)                                                ||
              inter_fn_buscar_tag('lb34968#QTDE CAMADAS:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.qtde_camadas       ||
              chr(10)                                                ||
              inter_fn_buscar_tag('lb34969#TECIDO PRINCIPAL:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || to_char( :new.tecido_principal ) ||
              '-'                         || ws_tecido_principal ||
              chr(10)                                                ||
              inter_fn_buscar_tag('lb35878#SEQ. DE COR:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.seq_cor ||
              chr(10)                                                ||
              inter_fn_buscar_tag('lb35879#CODIGO DESENHO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || :new.codigo_desenho ||
              chr(10)                                                ||
              inter_fn_buscar_tag('lb03230#DESTINO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                          || to_char( :new.tipo_destino ) ||
              chr(10)
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   if updating
   then
      -- ENCONTRA O TIPO_PRODUTO DA SEQUENCIA
      begin
         select basi_101.codigo_tipo_produto
         into ws_cod_tipo_produto_n
         from basi_101
         where basi_101.codigo_projeto    = :new.codigo_projeto
           and basi_101.sequencia_projeto = :new.sequencia_projeto;
      exception when no_data_found then
         ws_cod_tipo_produto_n := '';
      end;

      -- DESCRICAO DO TIPO DE PRODUTO
      begin
         select nvl(basi_003.descricao,' ')
         into ws_tipo_produto_n
         from basi_003
         where basi_003.codigo_tipo_produto = ws_cod_tipo_produto_n;
      exception when no_data_found then
         ws_tipo_produto_n := '';
      end;

     -- DESCRICAO DO PROJETO
      begin
         select basi_001.descricao_projeto,  basi_001.cnpj_cliente9,
                basi_001.cnpj_cliente4,      basi_001.cnpj_cliente2
         into ws_desc_projeto_n,             ws_cnpj_cliente9,
              ws_cnpj_cliente4,              ws_cnpj_cliente2
         from basi_001
         where basi_001.codigo_projeto = :new.codigo_projeto;
      exception when no_data_found then
         ws_desc_projeto_n := '';
         ws_cnpj_cliente9  := 0;
         ws_cnpj_cliente4  := 0;
         ws_cnpj_cliente2  := 0;
      end;

      -- DESCRICAO DO OLD COMPONENTE
      inter_pr_desc_produto(:old.nivel_comp,
                            :old.grupo_comp,
                            :old.subgru_comp,
                            :old.item_comp,
                            ws_desc_comp_o,
                            ws_narrativa);

      -- DESCRICAO DO NOVO COMPONENTE
      inter_pr_desc_produto(:new.nivel_comp,
                            :new.grupo_comp,
                            :new.subgru_comp,
                            :new.item_comp,
                            ws_desc_comp_n,
                            ws_narrativa);

      -- DESCRICAO DO NOVO COMPONENTE - PRODUTO NIVEL 1
      inter_pr_desc_produto(:new.nivel_item,
                            :new.grupo_item,
                            :new.subgru_item,
                            :new.item_item,
                            ws_desc_prod_n,
                            ws_narrativa);

      ws_teve := 'n';

      -- Busca qual situacao do componente
        if :new.nivel_comp = '2'
      then
         begin
            select basi_028.situacao_componente
            into ws_situacao_componente
            from basi_028
            where basi_028.nivel_componente     = :new.nivel_comp
              and basi_028.grupo_componente     = :new.grupo_comp
              and basi_028.subgrupo_componente  = :new.subgru_comp
              and basi_028.item_componente      = :new.item_comp
              and basi_028.cnpj_cliente9        = ws_cnpj_cliente9
              and basi_028.cnpj_cliente4        = ws_cnpj_cliente4
              and basi_028.cnpj_cliente2        = ws_cnpj_cliente2
              and basi_028.tipo_aprovacao       = 1;   --APROVACAO POR COMPONENTE
         exception when no_data_found then
              ws_situacao_componente := 0;   -- 0 - indefinido 1 - definido 2 - aprovado 3 - reprovado
         end;
      else
         begin
            select basi_028.situacao_componente
            into ws_situacao_componente
            from basi_028
            where basi_028.nivel_componente     = :new.nivel_comp
              and basi_028.grupo_componente     = :new.grupo_comp
              and basi_028.subgrupo_componente  = :new.subgru_comp
              and basi_028.item_componente      = :new.item_comp
              and basi_028.codigo_projeto       = :new.codigo_projeto
              and basi_028.sequencia_projeto    = :new.sequencia_subprojeto
              and basi_028.tipo_aprovacao       = 1;   --APROVACAO POR COMPONENTE
         exception when no_data_found then
              ws_situacao_componente := 0;   -- 0 - indefinido 1 - definido 2 - aprovado 3 - reprovado
         end;
      end if;

      ws_teve := 'n';

      long_aux := long_aux ||
           inter_fn_buscar_tag('lb34894#ESTRUTURA DO DESENVOLVIMENTO DE PRODUTO',ws_locale_usuario,ws_usuario_systextil) || '(013)' ||
           chr(10)                                               ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                       || :new.codigo_projeto    ||
           ' - '                       || ws_desc_projeto_n      ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb08486#TIPO DE PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                       || :new.sequencia_projeto ||
           ' - '                       || ws_cod_tipo_produto_n  ||
           ' - '                       || ws_tipo_produto_n      ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb15713#PRODUTO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                       || :new.nivel_item        ||
           '.'                         || :new.grupo_item        ||
           '.'                         || :new.subgru_item       ||
           '.'                         || :new.item_item         ||
           ' - '                       || ws_desc_prod_n         ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb00442#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                       || to_char(:old.alternativa_produto,'00')   ||
           '-> '                       || to_char(:new.alternativa_produto,'00')   ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb08191#ROTEIRO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                       || to_char(:old.numbero_roteiro,'00')       ||
           '-> '                       || to_char(:new.numbero_roteiro,'00')       ||
           chr(10)                                               ||
           chr(10);

      if (:old.nivel_comp  <> :new.nivel_comp)  or
         (:old.grupo_comp  <> :new.grupo_comp)  or
         (:old.subgru_comp <> :new.subgru_comp) or
         (:old.item_comp   <> :new.item_comp)
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00523#COMPONENTE:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                     || :old.sequencia_estrutura ||
         '.'                         || :old.sequencia_variacao  ||
         ' '                         || :old.nivel_comp        ||
         '.'                         || :old.grupo_comp        ||
         '.'                         || :old.subgru_comp       ||
         '.'                         || :old.item_comp         ||
         ' - '                       || ws_desc_comp_o         ||
         '-> '                       || :new.nivel_comp        ||
         '.'                         || :new.grupo_comp        ||
         '.'                         || :new.subgru_comp       ||
         '.'                         || :new.item_comp         ||
         ' - '                       || ws_desc_comp_n         ||
         chr(10);
      end if;

      if :old.consumo_auxiliar <> :new.consumo_auxiliar
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb34966#CONSUMO AUXILIAR:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                     || :old.consumo_auxiliar  ||
         ' -> '                      || :new.consumo_auxiliar  ||
         chr(10);
      end if;

      if :old.consumo_componente <> :new.consumo_componente
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00064#CONSUMO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                     || :old.consumo_componente  ||
         ' -> '                      || :new.consumo_componente  ||
         chr(10);
      end if;

      if :old.calcula_composicao <> :new.calcula_composicao
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb32279#CALCULA',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
         inter_fn_buscar_tag('lb00528#COMPOSICAO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                     || :old.calcula_composicao ||
         ' -> '                      || :new.calcula_composicao ||
         chr(10);
      end if;

      if :old.percentual_perdas <> :new.percentual_perdas
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb01280#% PERDA:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                     || :old.percentual_perdas  ||
         ' -> '                      || :new.percentual_perdas  ||
         chr(10);
      end if;

      if :old.qtde_camadas <> :new.qtde_camadas
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb34968#QTDE CAMADAS:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                     || :old.qtde_camadas       ||
         ' -> '                      || :new.qtde_camadas       ||
         chr(10);
      end if;

      if :old.codigo_estagio <> :new.codigo_estagio
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb00680#ESTAGIO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                     || :old.codigo_estagio     ||
         ' -> '                      || :new.codigo_estagio     ||
         chr(10);
      end if;


      if :old.tecido_principal <> :new.tecido_principal
      then
         ws_tecido_principal := ' ';

         if :new.tecido_principal = 0
         then
            ws_tecido_principal := inter_fn_buscar_tag('lb34964#NAO SELECIONADO',ws_locale_usuario,ws_usuario_systextil);
         else
            ws_tecido_principal := inter_fn_buscar_tag('lb34102#SELECIONADO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb34969TECIDO PRINCIPAL:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                     || to_char(:old.tecido_principal,'0')  ||
         ' -> '                      || to_char(:new.tecido_principal,'0')  ||
         '-'                         || ws_tecido_principal       ||
         chr(10);
      end if;

      if :old.seq_cor <> :new.seq_cor
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb35878#SEQ. DE COR:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                     || :old.seq_cor     ||
         ' -> '                      || :new.seq_cor     ||
         chr(10);
      end if;

      if :old.codigo_desenho <> :new.codigo_desenho
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb35879#CODIGO DESENHO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                     || :old.codigo_desenho     ||
         ' -> '                      || :new.codigo_desenho     ||
         chr(10);
      end if;

      if :old.tipo_destino <> :new.tipo_destino
      then
         ws_teve := 's';

         long_aux := long_aux ||
         inter_fn_buscar_tag('lb03230#DESTINO:',ws_locale_usuario,ws_usuario_systextil)  || ' '
                                     || :old.tipo_destino     ||
         ' -> '                      || :new.tipo_destino     ||
         chr(10);
      end if;

      if ws_teve = 's'
      then
         begin
            INSERT INTO hist_100
               ( tabela,              operacao,
                 data_ocorr,          aplicacao,
                 usuario_rede,        maquina_rede,

                 str01,               num05,
                 num07,               num08,
                 num09,

                 num10,
                 str07,               str08,
                 str09,               str10,
                 num06,
                 long01
            ) VALUES (
                 'BASI_013',          'A',
                 sysdate,             ws_aplicativo,
                 ws_usuario_rede,     ws_maquina_rede,

                 :new.codigo_projeto, :new.sequencia_projeto,
                 ws_cnpj_cliente9,    ws_cnpj_cliente4,
                 ws_cnpj_cliente2,

                 :new.sequencia_estrutura,
                 :new.nivel_comp,     :new.grupo_comp,
                 :new.subgru_comp,    :new.item_comp,
                 ws_situacao_componente,
                 long_aux
            );
         exception when OTHERS then
            raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(013-2)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;

   if deleting
   then
      begin
         INSERT INTO hist_100
            ( tabela,              operacao,
              data_ocorr,          aplicacao,
              usuario_rede,        maquina_rede,

              str01,               num05,
              num07,               num08,
              num09,

              num10,
              str07,               str08,
              str09,               str10
            )
         VALUES
            ( 'BASI_013',          'D',
              sysdate,             ws_aplicativo,
              ws_usuario_rede,     ws_maquina_rede,

              :old.codigo_projeto, :old.sequencia_projeto,
              ws_cnpj_cliente9,    ws_cnpj_cliente4,
              ws_cnpj_cliente2,

              :old.sequencia_estrutura,
              :old.nivel_comp,     :old.grupo_comp,
              :old.subgru_comp,    :old.item_comp
         );
      exception when OTHERS then
                     raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(013-3)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;
end inter_tr_basi_013_log;

-- ALTER TRIGGER "INTER_TR_BASI_013_LOG" ENABLE
 

/

exec inter_pr_recompile;

