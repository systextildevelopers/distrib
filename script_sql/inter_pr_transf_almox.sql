create or replace procedure inter_pr_transf_almox(nivel IN VARCHAR2,
                                                  grupo IN VARCHAR2,
                                                  subgrupo IN VARCHAR2,
                                                  item IN VARCHAR2,
                                                  depositoSaidaMP IN NUMBER,
                                                  flag IN NUMBER, -- 0: Estorno para depósito de almoxarifado
                                                                  -- 1: Saída do depósito de almoxarifado
                                                  ordem_servico IN NUMBER,
                                                  valor_movimento IN NUMBER,
                                                  quantidade IN NUMBER,
                                                  processo IN VARCHAR2 DEFAULT NULL)
is
  ws_usuario_rede           varchar2(20);
  ws_maquina_rede           varchar2(40);
  ws_aplicativo             varchar2(20);
  ws_sid                    number(9);
  ws_empresa                number(3);
  ws_usuario_systextil      varchar2(250);
  ws_locale_usuario         varchar2(5);

  deposito_almox number;
  transacao_transf_almox number;
  deposito_transf_almox number;
  transacao_entrada number;

  v_processo varchar2(100);
  v_qtde number;

  deposito_entrada number;
  deposito_saida number;
  quantidade_atual estq_040.qtde_estoque_atu%type;
begin
  BEGIN --Leitura dos dados do usuário logado.
     inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                             ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
  END;
  v_processo := null;
  v_qtde := quantidade;

  if ws_empresa = 0
  then
    ws_empresa := 500; --Lunender, execucao externa.
    v_processo := 'SQL';
  end if;

  BEGIN --Leitura das configurações de empresa.
    BEGIN
      select fatu_503.deposito_almoxarifado
      into deposito_almox
      from fatu_503
      where fatu_503.codigo_empresa = ws_empresa;
    EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, sqlerrm);
      deposito_almox := 0;
    END;

    BEGIN
      select fatu_504.transacao_transf_almox, fatu_504.deposito_transf_almox
      into transacao_transf_almox, deposito_transf_almox
      from fatu_504
      where fatu_504.codigo_empresa = ws_empresa;
    EXCEPTION
    WHEN OTHERS THEN
      transacao_transf_almox := 0;
      deposito_transf_almox := 0;
    END;

    BEGIN
      select estq_005.transac_entrada
      into transacao_entrada
      from estq_005
      where estq_005.codigo_transacao = transacao_transf_almox;
    EXCEPTION
    WHEN OTHERS THEN
      transacao_entrada := 0;
    END;

    IF deposito_almox = 0 or
       deposito_transf_almox = 0 or
       transacao_transf_almox = 0 or
       transacao_entrada = 0
    THEN
      if processo = 'ESTQ_220'
      then
        INSERT INTO estq_220_log (
          numero_ordem, nivel, grupo, subgrupo, item,
          data_ocorr, mensagem
        ) VALUES (
          ordem_servico, nivel, grupo, subgrupo, item,
          sysdate, 'Erro! Deposito Almox:' || deposito_almox ||
           ' deposito transferencia almoxarifado: ' || deposito_transf_almox ||
           ' transacao transferencia almoxarifado: ' || transacao_transf_almox ||
           ' transacao transferencia entrada: ' || transacao_entrada
        );
        RETURN;
      else
         raise_application_error(-20000, 'Erro! Deposito Almox:' || deposito_almox ||
           ' deposito transferencia almoxarifado: ' || deposito_transf_almox ||
           ' transacao transferencia almoxarifado: ' || transacao_transf_almox ||
           ' transacao transferencia entrada: ' || transacao_entrada);
      end if;
      return;
    END IF;
  END;

  BEGIN --Regras da transferência entre depósitos do almoxarifado
    IF depositoSaidaMP <> 0 THEN
      deposito_almox := depositoSaidaMP;
    END IF;

    IF flag = 0 THEN
      deposito_entrada := deposito_almox;
      deposito_saida := deposito_transf_almox;
    ELSE
      deposito_entrada := deposito_transf_almox;
      deposito_saida := deposito_almox;
    END IF;

    BEGIN
      select sum(estq_040.qtde_estoque_atu)
      into quantidade_atual
      from estq_040
      where estq_040.cditem_nivel99 = nivel
        and estq_040.cditem_grupo = grupo
        and estq_040.cditem_subgrupo = subgrupo
        and estq_040.cditem_item = item
        and estq_040.deposito = deposito_saida;
    EXCEPTION
    WHEN OTHERS THEN
      quantidade_atual := 0;
    END;

    IF quantidade_atual = 0 or (quantidade_atual < v_qtde and v_processo is null) THEN
      IF processo = 'ESTQ_220'
      THEN
        INSERT INTO estq_220_log (
          numero_ordem, nivel, grupo, subgrupo, item,
          data_ocorr, mensagem
        ) VALUES (
          ordem_servico, nivel, grupo, subgrupo, item,
          sysdate, inter_fn_buscar_tag('ds33524',ws_locale_usuario,ws_usuario_systextil)
        );
        RETURN;
      ELSE
        --ATENÇÃO! Saldo insuficiente para executar a transferência.
        RAISE_APPLICATION_ERROR(-20000, inter_fn_buscar_tag('ds33524',ws_locale_usuario,ws_usuario_systextil));
      END IF;
    ELSE
      IF quantidade_atual < v_qtde
      THEN v_qtde := quantidade_atual;
      END IF;
    END IF;
  END;

  IF (v_qtde <= 0)
  THEN
    IF processo = 'ESTQ_220'
    THEN
      INSERT INTO estq_220_log (
        numero_ordem, nivel, grupo, subgrupo, item,
        data_ocorr, mensagem
      ) VALUES (
        ordem_servico, nivel, grupo, subgrupo, item,
        sysdate, 'Quantidade negativa. ' || nivel || '.' || grupo || '.' || subgrupo || '.' || item || ' qtde: ' || v_qtde
      );
      RETURN;
    ELSE
      RAISE_APPLICATION_ERROR(-20000, nivel || '.' || grupo || '.' || subgrupo || '.' || item || ' qtde: ' || v_qtde);
    END IF;
  END IF;

  BEGIN --Insert para fazer a Saída do produto.
    INSERT INTO estq_300 (
      data_movimento,               codigo_transacao,
      numero_documento,             nivel_estrutura,
      grupo_estrutura,              subgrupo_estrutura,
      item_estrutura,               quantidade,
      valor_movimento_unitario,     valor_contabil_unitario,
      codigo_deposito,
      centro_custo,                 entrada_saida,
      tabela_origem,                usuario_systextil,
      processo_systextil
    ) VALUES (
      SYSDATE,                      transacao_transf_almox,
      ordem_servico,                nivel,
      grupo,                        subgrupo,
      item,                         v_qtde,
      valor_movimento,              valor_movimento,
      deposito_saida,
      0,                            'S',
      'ESTQ_300',                   ws_usuario_systextil,
      'PR_TRANSF_ALMOX'
    );
  EXCEPTION
  WHEN OTHERS THEN
    IF processo = 'ESTQ_220'
    THEN
      INSERT INTO estq_220_log (
        numero_ordem, nivel, grupo, subgrupo, item,
        data_ocorr, mensagem
      ) VALUES (
        ordem_servico, nivel, grupo, subgrupo, item,
        sysdate, ('1. - Nao atualizou o estoque. Verifique a transacao e a quantidade em estoque.')
      );
      RETURN;
    ELSE
      RAISE_APPLICATION_ERROR(-20000,'1. - ' || sqlerrm);
    END IF;
  END;

  BEGIN --Insert para fazer a Entrada do produto.
    INSERT INTO estq_300 (
      data_movimento,               codigo_transacao,
      numero_documento,             nivel_estrutura,
      grupo_estrutura,              subgrupo_estrutura,
      item_estrutura,               quantidade,
      valor_movimento_unitario,     valor_contabil_unitario,
      codigo_deposito,
      centro_custo,                 entrada_saida,
      tabela_origem,                usuario_systextil,
      processo_systextil
    ) VALUES (
      SYSDATE,                      transacao_entrada,
      ordem_servico,                nivel,
      grupo,                        subgrupo,
      item,                         v_qtde,
      valor_movimento,              valor_movimento,
      deposito_entrada,
      0,                            'E',
      'ESTQ_300',                   ws_usuario_systextil,
      'PR_TRANSF_ALMOX'
    );
  EXCEPTION
  WHEN OTHERS THEN
    IF processo = 'ESTQ_220'
    THEN
      INSERT INTO estq_220_log (
        numero_ordem, nivel, grupo, subgrupo, item,
        data_ocorr, mensagem
      ) VALUES (
        ordem_servico, nivel, grupo, subgrupo, item,
        sysdate, ('1. - Nao atualizou o estoque. Verifique a transacao e a quantidade em estoque.')
      );
      RETURN;
    ELSE
      RAISE_APPLICATION_ERROR(-20000,'2. - ' || sqlerrm);
    END IF;
  END;
end;
