CREATE OR REPLACE PROCEDURE "P_GERA_ACERTO_NFEC" (p_cod_empresa    in  number,
                                               p_cod_id         in  number,
                                               p_atualiza_just  in  number, -- Para notas canceladas com problema no e-mail n�o atualiza
                                               p_msg_erro       out varchar2 ) is
-- Created on6/2/2009 by ANDRE
begin
   declare cursor c_nfe is
      select * from obrf_150
      where obrf_150.cod_empresa = p_cod_empresa
        and obrf_150.cod_usuario = p_cod_id
        and obrf_150.tipo_documento <> 5;

   v_ERRO EXCEPTION;
   for9 supr_010.fornecedor9%type;
   for4 supr_010.fornecedor4%type;
   for2 supr_010.fornecedor2%type;

   begin

     for f_nfe in c_nfe
     LOOP

        if  f_nfe.cod_status is not null
        then

           if f_nfe.tipo_emissao = 1
           then

              BEGIN
                update fatu_050 set justificativa       = decode(p_atualiza_just,1,decode(f_nfe.cod_status,101,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                           420,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                           218,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                           563,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                           102,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                           990,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),null),fatu_050.justificativa),
                                    cod_status          = decode(f_nfe.cod_status,null,' ',
                                                                                 420,101,
                                                                                 135,101,
                                                                                 573,101,
                                                                                 420,101,
                                                                                 218,101,
                                                                                 304,304,
                                                                                 220,220,
                                                                                 221,221,
                                                                                 650,650,
                                                                                 690,690,
                                                                                 151,151,
                                                                                 219,219,
                                                                                 501,501,
                                                                                 240,240,
                                                                                 206,102,
                                                                                 563,102,f_nfe.cod_status),
                                    msg_status          = decode(f_nfe.msg_status,null,' ',f_nfe.msg_status),
                                    numero_danf_nfe     = decode(f_nfe.chave_acesso,null,' ',f_nfe.chave_acesso),
                                    cod_justificativa   = decode(f_nfe.cod_status,101,2,
                                                                                  102,1,
                                                                                  420,2,
                                                                                  218,2,
                                                                                  563,1,0), -- 2 Cancelado, 1 Inutilizado
                                    nr_protocolo        = decode(f_nfe.cod_status,102,f_nfe.nr_protocolo,
                                                                                  563,f_nfe.nr_protocolo,
                                                                                  null,' ',fatu_050.nr_protocolo),
                                    versao_systextilnfe = f_nfe.versao_systextilnfe
                where fatu_050.codigo_empresa = f_nfe.cod_empresa
                  --and fatu_050.cod_solicitacao_nfe = p_cod_id /*Retirado por John*/
                  /*Retirado por John*/
                  and fatu_050.num_nota_fiscal = f_nfe.numero
                  and fatu_050.serie_nota_fisc = to_char(f_nfe.serie);
              EXCEPTION
                 WHEN OTHERS THEN
                    p_msg_erro := 'N�o atualizou dados na tabela fatu_050' || Chr(10) || SQLERRM;
                 RAISE v_ERRO;
              END;

           else

              if (f_nfe.dest_sigla_uf = 'EX')
              then
                 select fornecedor9  , fornecedor4 , fornecedor2 into for9, for4, for2
                 from supr_010
                 where nome_fornecedor   = f_nfe.dest_nome
                   and supr_010.sit_fornecedor = 1
                   and rownum = 1;

                 BEGIN
                    update obrf_010 set justificativa       = decode(p_atualiza_just,1,decode(f_nfe.cod_status,101,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                               563,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                               420,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                               218,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                               102,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                               990,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),null),obrf_010.justificativa),
                                        cod_status          = decode(f_nfe.cod_status,null,' ',
                                                                                      420,101,
                                                                                      135,101,
                                                                                      573,101,
                                                                                      240,240,
                                                                                      420,101,
                                                                                      221,221,
                                                                                      304,304,
                                                                                      690,690,
                                                                                      220,220,
                                                                                      151,151,
                                                                                      219,219,
                                                                                      206,102,
                                                                                      650,650,
                                                                                      218,101,
                                                                                      501,501,
                                                                                      563,102,f_nfe.cod_status),
                                        msg_status          = decode(f_nfe.msg_status,null,' ',f_nfe.msg_status),
                                        numero_danf_nfe     = decode(f_nfe.chave_acesso,null,' ',f_nfe.chave_acesso),
                                        cod_justificativa   = decode(f_nfe.cod_status,101,2,
                                                                                      102,1,
                                                                                      563,1,
                                                                                      420,2,
                                                                                      206,102,
                                                                                      218,2,0), -- 2 Cancelado --1 Inutilizado
                                        nr_protocolo        = decode(f_nfe.cod_status,102,f_nfe.nr_protocolo,
                                                                                      563,f_nfe.nr_protocolo,
                                                                                      null,' ',obrf_010.nr_protocolo),
                                        versao_systextilnfe = f_nfe.versao_systextilnfe
                    where obrf_010.local_entrega           = p_cod_empresa
                      and obrf_010.documento               = f_nfe.numero
                      and obrf_010.serie                   = trim(to_char(f_nfe.serie))
                      and for9                             = obrf_010.cgc_cli_for_9
                      and for4                             = obrf_010.cgc_cli_for_4
                      and for2                             = obrf_010.cgc_cli_for_2;
                 EXCEPTION
                    WHEN OTHERS THEN
                       p_msg_erro := 'N�o atualizou dados na tabela obrf_010 (1)' || Chr(10) || SQLERRM;
                    RAISE v_ERRO;
                 END;

              else

                 BEGIN
                    update obrf_010 set justificativa       = decode(p_atualiza_just,1,decode(f_nfe.cod_status,101,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                               563,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                               420,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                               218,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                               102,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),
                                                                                                               990,(decode(f_nfe.justificativa,null,' ',f_nfe.justificativa)),null),obrf_010.justificativa),
                                        cod_status          = decode(f_nfe.cod_status,null,' ',
                                                                                      420,101,
                                                                                      135,101,
                                                                                      573,101,
                                                                                      420,101,
                                                                                      240,240,
                                                                                      304,304,
                                                                                      220,220,
                                                                                      221,221,
                                                                                      690,690,
                                                                                      219,219,
                                                                                      206,102,
                                                                                      650,650,
                                                                                      501,501,
                                                                                      218,101,
                                                                                      563,102,f_nfe.cod_status),
                                        msg_status          = decode(f_nfe.msg_status,null,' ',f_nfe.msg_status),
                                        numero_danf_nfe     = decode(f_nfe.chave_acesso,null,' ',f_nfe.chave_acesso),
                                        cod_justificativa   = decode(f_nfe.cod_status,101,2,
                                                                                      102,1,
                                                                                      563,1,
                                                                                      420,2,
                                                                                      206,102,
                                                                                      218,2,0), -- 2 Cancelado --1 Inutilizado
                                        nr_protocolo        = decode(f_nfe.cod_status,102,f_nfe.nr_protocolo,
                                                                                      563,f_nfe.nr_protocolo,
                                                                                      null,' ',obrf_010.nr_protocolo),
                                        versao_systextilnfe = f_nfe.versao_systextilnfe
                    where obrf_010.local_entrega           = p_cod_empresa
                      and obrf_010.documento               = f_nfe.numero
                      and obrf_010.serie                   = trim(to_char(f_nfe.serie))
                      and obrf_010.cgc_cli_for_9         = f_nfe.dest_cnpj9
                      and obrf_010.cgc_cli_for_4     = f_nfe.dest_cnpj4
                      and obrf_010.cgc_cli_for_2     = f_nfe.dest_cnpj2;
                      --and obrf_010.natoper_est_oper <> 'EX';

                 EXCEPTION
                    WHEN OTHERS THEN
                       p_msg_erro := 'N�o atualizou dados na tabela obrf_010 (1)' || Chr(10) || SQLERRM;
                    RAISE v_ERRO;
                 END;
              end if;
           end if;

           if  f_nfe.cod_status is not null
           then
              BEGIN
                insert into obrf_400
                   (obrf_400.codigo_empresa
                   ,obrf_400.num_nota_fiscal
                   ,obrf_400.serie_nota_fisc
                   ,obrf_400.cgc_9
                   ,obrf_400.cgc_4
                   ,obrf_400.cgc_2
                   ,obrf_400.cod_status
                   ,obrf_400.msg_status
                   ,obrf_400.nr_protocolo
                   ,obrf_400.data_inclusao)
                values
                   (f_nfe.cod_empresa
                   ,f_nfe.numero
                   ,trim(to_char(f_nfe.serie))
                   ,f_nfe.dest_cnpj9
                   ,f_nfe.dest_cnpj4
                   ,f_nfe.dest_cnpj2
                   ,decode(f_nfe.cod_status,null,' ',f_nfe.cod_status)
                   ,decode(f_nfe.msg_status,null,' ',f_nfe.msg_status)
                   ,decode(f_nfe.cod_status,null,' ',f_nfe.nr_protocolo)
                   ,sysdate
                );
              EXCEPTION
                WHEN OTHERS THEN
                  p_msg_erro := 'N�o atualizou dados na tabela obrf_f400' || Chr(10) || SQLERRM;
                RAISE v_ERRO;
              END;
           end if;
        end if;

        if f_nfe.cod_status <> '103' -- finalidade desse if � para nao excluir o registro do tipo 5 - verificacao de status do servico
        then
           BEGIN
              delete from obrf_150
              where cod_empresa = p_cod_empresa
                and cod_usuario = p_cod_id
                and id = f_nfe.id
                and obrf_150.cod_status <> '103' or obrf_150.cod_status is null;

           EXCEPTION
              WHEN OTHERS THEN
                p_msg_erro := 'N�o apagou os dados na tabela obrf_150' || Chr(10) || SQLERRM;
              RAISE v_erro;
           END;
        end if;
      END LOOP;
   end;
end;

 

/

exec inter_pr_recompile;

