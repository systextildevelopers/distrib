drop table pcpb_070_aa;
drop table pcpb_075_aa;

create table pcpb_070_aa(
    sequencia             number(3,0)
	,data_informacao      date
	,ordem_tingimento     number(9,0)
	,ordem_beneficiamento number(9,0)
	,artigo               varchar2(400)
	,processo             varchar2(400)
	,cor                  varchar2(400)
	,quilos               number(10,3)
);

comment on table pcpb_070_aa is 'Tabela criada para armazenar os dados (do lote) temporários do relatório de qualidade da tela lune_fa30 para criação e download do relatório jasper pc_relatorio_de_qualidade';

create table  pcpb_075_aa(
    ordem_tingimento      number(9,0)
	,ordem_beneficiamento number(9,0)
	,sequencia            number(3,0)
	,descricao            varchar2(400)
	,situacao             varchar2(400)
	,padrao               number(17,7)
	,observacao           varchar2(400)
	,resultado             number
);

comment on table pcpb_070_aa is 'Tabela criada para armazenar os dados (testes realizados no setor de laboratório) temporários do relatório de qualidade da tela lune_fa30 para criação e download do relatório jasper pc_relatorio_de_qualidade';
