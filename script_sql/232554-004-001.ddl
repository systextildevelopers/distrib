create table INTE_880
(
  ID_REG          NUMBER(30) not null,
  NOME_TABELA     VARCHAR2(20),
  NUM_LINHA       NUMBER(9),
  MSG_ERRO        CLOB,
  DATA_HORA_CARGA DATE default SYSDATE,
  LOTE_CARGA      NUMBER(9),
  LINHA_ERRO      CLOB
);
