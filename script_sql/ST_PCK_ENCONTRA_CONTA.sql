create or replace PACKAGE ST_PCK_ENCONTRA_CONTA AS
    
    function executar(  p_nome_form varchar,            p_cod_empresa number,  
                        p_data_lanc date,               p_tipo_contab number,  
                        p_cod_contabil number,          p_transacao number, 
                        p_c_custo number,               p_data_lcto_doc date,
                        p_mdi_empresa number,           p_mdi_usuario varchar2, 
                        p_mdi_codigo_usuario varchar2,  p_msg_erro in out varchar2) return number;

    function get_conta( p_nome_form varchar2,                                   p_cod_empresa number,
                        p_array_exercicio ST_PCK_EXERCICIO.t_dados_exercicio,   p_tipo_contab number, 
                        p_cod_contabil number,                                  p_transacao number,   
                        p_c_custo number,                                       p_mdi_empresa number,   
                        p_mdi_usuario varchar2,                                 p_mdi_codigo_usuario varchar2,
                        p_msg_erro in out varchar2) return number;

    function procurar_relac_contabil(p_tipo_contab number,  p_cod_contabil number,
                                     p_transacao number,    p_array_exercicio ST_PCK_EXERCICIO.t_dados_exercicio,
                                     p_msg_erro in out varchar) return number;
                                     
    function esta_ok(p_c_custo number, p_array_plano_contas_item ST_PCK_PLANO_CONTAS_ITEM.t_dados_cont_535) return boolean;

	function validar_tipo_natu_conta(   p_cod_empresa number, p_conta_contab number,
                                        p_c_custo number,     p_array_plano_contas_item ST_PCK_PLANO_CONTAS_ITEM.t_dados_cont_535,
                                        p_msg_erro in out varchar2 ) return boolean;

    function validar_pelo_plano_de_contas(  p_cod_empresa number,   p_conta_contab number, 
                                            p_c_custo number,       p_array_exercicio ST_PCK_EXERCICIO.t_dados_exercicio,
                                            p_msg_erro in out varchar) return number;

    function exibe_mensagens_erro(p_nome_form varchar2, p_tipo_contab number) return boolean;
    
END ST_PCK_ENCONTRA_CONTA;
