create or replace trigger inter_tr_pedi_187_log 
after insert or delete or update 
on pedi_187 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   ws_processo_systextil     hdoc_090.programa%type; 


begin
--- Dados do usu?rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 
                           
   ws_processo_systextil := inter_fn_nome_programa(ws_sid);                           
 
 
 if inserting 
 then 
    begin 
 
        insert into pedi_187_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           nivel_OLD,   /*8*/ 
           nivel_NEW,   /*9*/ 
           grupo_OLD,   /*10*/ 
           grupo_NEW,   /*11*/ 
           subgrupo_OLD,   /*12*/ 
           subgrupo_NEW,   /*13*/ 
           item_OLD,   /*14*/ 
           item_NEW,   /*15*/ 
           tipo_pessoa_OLD,   /*16*/ 
           tipo_pessoa_NEW,   /*17*/ 
           estado_OLD,   /*18*/ 
           estado_NEW,   /*19*/ 
           nat_operacao_OLD,   /*20*/ 
           nat_operacao_NEW,   /*21*/ 
           processo_OLD,   /*22*/ 
           processo_NEW,   /*23*/ 
           codigo_empresa_OLD,   /*24*/ 
           codigo_empresa_NEW,   /*25*/ 
           classific_fisc_OLD,   /*26*/ 
           classific_fisc_NEW,   /*27*/ 
           cnpj_cli9_OLD,   /*28*/ 
           cnpj_cli9_NEW,   /*29*/ 
           cnpj_cli4_OLD,   /*30*/ 
           cnpj_cli4_NEW,   /*31*/ 
           cnpj_cli2_OLD,   /*32*/ 
           cnpj_cli2_NEW,   /*33*/ 
           codigo_deposito_OLD,   /*34*/ 
           codigo_deposito_NEW,   /*35*/ 
           nat_op_origem_OLD,   /*36*/ 
           nat_op_origem_NEW,   /*37*/ 
           origem_prod_OLD,   /*38*/ 
           origem_prod_NEW,   /*39*/ 
           cod_atividade_economica_OLD,   /*40*/ 
           cod_atividade_economica_NEW,   /*41*/ 
           insc_est_OLD,   /*42*/ 
           insc_est_NEW,   /*43*/ 
           cod_artigo_produto_OLD,   /*44*/ 
           cod_artigo_produto_NEW,   /*45*/ 
           comprado_fabricado_OLD,   /*46*/ 
           comprado_fabricado_NEW,   /*47*/ 
           classificacao_pedido_OLD,   /*48*/ 
           classificacao_pedido_NEW,   /*49*/ 
           suframa_OLD,   /*50*/ 
           suframa_NEW    /*51*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            ws_processo_systextil, /*7*/
           '',/*8*/
           :new.nivel, /*9*/   
           '',/*10*/
           :new.grupo, /*11*/   
           '',/*12*/
           :new.subgrupo, /*13*/   
           '',/*14*/
           :new.item, /*15*/   
           0,/*16*/
           :new.tipo_pessoa, /*17*/   
           '',/*18*/
           :new.estado, /*19*/   
           0,/*20*/
           :new.nat_operacao, /*21*/   
           0,/*22*/
           :new.processo, /*23*/   
           0,/*24*/
           :new.codigo_empresa, /*25*/   
           '',/*26*/
           :new.classific_fisc, /*27*/   
           0,/*28*/
           :new.cnpj_cli9, /*29*/   
           0,/*30*/
           :new.cnpj_cli4, /*31*/   
           0,/*32*/
           :new.cnpj_cli2, /*33*/   
           0,/*34*/
           :new.codigo_deposito, /*35*/   
           0,/*36*/
           :new.nat_op_origem, /*37*/   
           0,/*38*/
           :new.origem_prod, /*39*/   
           0,/*40*/
           :new.cod_atividade_economica, /*41*/   
           0,/*42*/
           :new.insc_est, /*43*/   
           0,/*44*/
           :new.cod_artigo_produto, /*45*/   
           0,/*46*/
           :new.comprado_fabricado, /*47*/   
           0,/*48*/
           :new.classificacao_pedido, /*49*/   
           0,/*50*/
           :new.suframa /*51*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into pedi_187_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           nivel_OLD, /*8*/   
           nivel_NEW, /*9*/   
           grupo_OLD, /*10*/   
           grupo_NEW, /*11*/   
           subgrupo_OLD, /*12*/   
           subgrupo_NEW, /*13*/   
           item_OLD, /*14*/   
           item_NEW, /*15*/   
           tipo_pessoa_OLD, /*16*/   
           tipo_pessoa_NEW, /*17*/   
           estado_OLD, /*18*/   
           estado_NEW, /*19*/   
           nat_operacao_OLD, /*20*/   
           nat_operacao_NEW, /*21*/   
           processo_OLD, /*22*/   
           processo_NEW, /*23*/   
           codigo_empresa_OLD, /*24*/   
           codigo_empresa_NEW, /*25*/   
           classific_fisc_OLD, /*26*/   
           classific_fisc_NEW, /*27*/   
           cnpj_cli9_OLD, /*28*/   
           cnpj_cli9_NEW, /*29*/   
           cnpj_cli4_OLD, /*30*/   
           cnpj_cli4_NEW, /*31*/   
           cnpj_cli2_OLD, /*32*/   
           cnpj_cli2_NEW, /*33*/   
           codigo_deposito_OLD, /*34*/   
           codigo_deposito_NEW, /*35*/   
           nat_op_origem_OLD, /*36*/   
           nat_op_origem_NEW, /*37*/   
           origem_prod_OLD, /*38*/   
           origem_prod_NEW, /*39*/   
           cod_atividade_economica_OLD, /*40*/   
           cod_atividade_economica_NEW, /*41*/   
           insc_est_OLD, /*42*/   
           insc_est_NEW, /*43*/   
           cod_artigo_produto_OLD, /*44*/   
           cod_artigo_produto_NEW, /*45*/   
           comprado_fabricado_OLD, /*46*/   
           comprado_fabricado_NEW, /*47*/   
           classificacao_pedido_OLD, /*48*/   
           classificacao_pedido_NEW, /*49*/   
           suframa_OLD, /*50*/   
           suframa_NEW  /*51*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            ws_processo_systextil, /*7*/ 
           :old.nivel,  /*8*/  
           :new.nivel, /*9*/   
           :old.grupo,  /*10*/  
           :new.grupo, /*11*/   
           :old.subgrupo,  /*12*/  
           :new.subgrupo, /*13*/   
           :old.item,  /*14*/  
           :new.item, /*15*/   
           :old.tipo_pessoa,  /*16*/  
           :new.tipo_pessoa, /*17*/   
           :old.estado,  /*18*/  
           :new.estado, /*19*/   
           :old.nat_operacao,  /*20*/  
           :new.nat_operacao, /*21*/   
           :old.processo,  /*22*/  
           :new.processo, /*23*/   
           :old.codigo_empresa,  /*24*/  
           :new.codigo_empresa, /*25*/   
           :old.classific_fisc,  /*26*/  
           :new.classific_fisc, /*27*/   
           :old.cnpj_cli9,  /*28*/  
           :new.cnpj_cli9, /*29*/   
           :old.cnpj_cli4,  /*30*/  
           :new.cnpj_cli4, /*31*/   
           :old.cnpj_cli2,  /*32*/  
           :new.cnpj_cli2, /*33*/   
           :old.codigo_deposito,  /*34*/  
           :new.codigo_deposito, /*35*/   
           :old.nat_op_origem,  /*36*/  
           :new.nat_op_origem, /*37*/   
           :old.origem_prod,  /*38*/  
           :new.origem_prod, /*39*/   
           :old.cod_atividade_economica,  /*40*/  
           :new.cod_atividade_economica, /*41*/   
           :old.insc_est,  /*42*/  
           :new.insc_est, /*43*/   
           :old.cod_artigo_produto,  /*44*/  
           :new.cod_artigo_produto, /*45*/   
           :old.comprado_fabricado,  /*46*/  
           :new.comprado_fabricado, /*47*/   
           :old.classificacao_pedido,  /*48*/  
           :new.classificacao_pedido, /*49*/   
           :old.suframa,  /*50*/  
           :new.suframa  /*51*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into pedi_187_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           nivel_OLD, /*8*/   
           nivel_NEW, /*9*/   
           grupo_OLD, /*10*/   
           grupo_NEW, /*11*/   
           subgrupo_OLD, /*12*/   
           subgrupo_NEW, /*13*/   
           item_OLD, /*14*/   
           item_NEW, /*15*/   
           tipo_pessoa_OLD, /*16*/   
           tipo_pessoa_NEW, /*17*/   
           estado_OLD, /*18*/   
           estado_NEW, /*19*/   
           nat_operacao_OLD, /*20*/   
           nat_operacao_NEW, /*21*/   
           processo_OLD, /*22*/   
           processo_NEW, /*23*/   
           codigo_empresa_OLD, /*24*/   
           codigo_empresa_NEW, /*25*/   
           classific_fisc_OLD, /*26*/   
           classific_fisc_NEW, /*27*/   
           cnpj_cli9_OLD, /*28*/   
           cnpj_cli9_NEW, /*29*/   
           cnpj_cli4_OLD, /*30*/   
           cnpj_cli4_NEW, /*31*/   
           cnpj_cli2_OLD, /*32*/   
           cnpj_cli2_NEW, /*33*/   
           codigo_deposito_OLD, /*34*/   
           codigo_deposito_NEW, /*35*/   
           nat_op_origem_OLD, /*36*/   
           nat_op_origem_NEW, /*37*/   
           origem_prod_OLD, /*38*/   
           origem_prod_NEW, /*39*/   
           cod_atividade_economica_OLD, /*40*/   
           cod_atividade_economica_NEW, /*41*/   
           insc_est_OLD, /*42*/   
           insc_est_NEW, /*43*/   
           cod_artigo_produto_OLD, /*44*/   
           cod_artigo_produto_NEW, /*45*/   
           comprado_fabricado_OLD, /*46*/   
           comprado_fabricado_NEW, /*47*/   
           classificacao_pedido_OLD, /*48*/   
           classificacao_pedido_NEW, /*49*/   
           suframa_OLD, /*50*/   
           suframa_NEW /*51*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            ws_processo_systextil, /*7*/
           :old.nivel, /*8*/   
           '', /*9*/
           :old.grupo, /*10*/   
           '', /*11*/
           :old.subgrupo, /*12*/   
           '', /*13*/
           :old.item, /*14*/   
           '', /*15*/
           :old.tipo_pessoa, /*16*/   
           0, /*17*/
           :old.estado, /*18*/   
           '', /*19*/
           :old.nat_operacao, /*20*/   
           0, /*21*/
           :old.processo, /*22*/   
           0, /*23*/
           :old.codigo_empresa, /*24*/   
           0, /*25*/
           :old.classific_fisc, /*26*/   
           '', /*27*/
           :old.cnpj_cli9, /*28*/   
           0, /*29*/
           :old.cnpj_cli4, /*30*/   
           0, /*31*/
           :old.cnpj_cli2, /*32*/   
           0, /*33*/
           :old.codigo_deposito, /*34*/   
           0, /*35*/
           :old.nat_op_origem, /*36*/   
           0, /*37*/
           :old.origem_prod, /*38*/   
           0, /*39*/
           :old.cod_atividade_economica, /*40*/   
           0, /*41*/
           :old.insc_est, /*42*/   
           0, /*43*/
           :old.cod_artigo_produto, /*44*/   
           0, /*45*/
           :old.comprado_fabricado, /*46*/   
           0, /*47*/
           :old.classificacao_pedido, /*48*/   
           0, /*49*/
           :old.suframa, /*50*/   
           0 /*51*/
         );    
    end;    
 end if;    
end inter_tr_pedi_187_log;
/
exec inter_pr_recompile;
