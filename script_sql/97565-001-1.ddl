alter table pedi_080
add (perc_fcp_uf_dest    number(5,2) default 0,
     perc_icms_uf_dest     number(5,2) default 0);
     
comment on column pedi_080.perc_fcp_uf_dest is 'Percentual do ICMS relativo ao Fundo de Combate � Pobreza (FCP) na UF de destino';
comment on column pedi_080.perc_icms_uf_dest is 'Al�quota interna da UF do destinat�rio';

exec inter_pr_recompile;
/