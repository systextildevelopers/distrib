alter table fatu_070
add( 
  FND_TEM_ESPELHO        NUMBER(1) );

alter table fatu_070
modify FND_TEM_ESPELHO default 0;

comment on column fatu_070.FND_TEM_ESPELHO   is '0- NAO TEM OU 1 TEM';

exec inter_pr_recompile;
/
