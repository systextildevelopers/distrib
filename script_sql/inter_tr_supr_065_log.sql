
  CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_065_LOG" 
   before insert or
          delete or
          update of num_requisicao, data_requisicao, centro_custo, nome_requisit,
		    observacao_req, cod_cancelamento, codigo_deposito, codigo_empresa,
		    ccusto_aplicacao, codigo, solicitacao, executa_trigger,
	            codigo_comprador, usuario_digitacao
          on supr_065
for each row

declare
  V_NUM_REQUISICAO_OLD   supr_065.NUM_REQUISICAO%type;
  V_DATA_REQUISICAO_OLD  supr_065.DATA_REQUISICAO%type;
  V_CENTRO_CUSTO_OLD     supr_065.CENTRO_CUSTO%type;
  V_NOME_REQUISIT_OLD    supr_065.NOME_REQUISIT%type;
  V_OBSERVACAO_REQ_OLD   supr_065.OBSERVACAO_REQ%type;
  V_COD_CANCELAMENTO_OLD supr_065.COD_CANCELAMENTO%type;
  V_CODIGO_DEPOSITO_OLD  supr_065.CODIGO_DEPOSITO%type;
  V_CODIGO_EMPRESA_OLD   supr_065.CODIGO_EMPRESA%type;
  V_CCUSTO_APLICACAO_OLD supr_065.CCUSTO_APLICACAO%type;
  V_CODIGO_OLD           supr_065.CODIGO%type;
  V_SOLICITACAO_OLD      supr_065.SOLICITACAO%type;
  V_NUM_REQUISICAO_NEW   supr_065.NUM_REQUISICAO%type;
  V_DATA_REQUISICAO_NEW  supr_065.DATA_REQUISICAO%type;
  V_CENTRO_CUSTO_NEW     supr_065.CENTRO_CUSTO%type;
  V_NOME_REQUISIT_NEW    supr_065.NOME_REQUISIT%type;
  V_OBSERVACAO_REQ_NEW   supr_065.OBSERVACAO_REQ%type;
  V_COD_CANCELAMENTO_NEW supr_065.COD_CANCELAMENTO%type;
  V_CODIGO_DEPOSITO_NEW  supr_065.CODIGO_DEPOSITO%type;
  V_CODIGO_EMPRESA_NEW   supr_065.CODIGO_EMPRESA%type;
  V_CCUSTO_APLICACAO_NEW supr_065.CCUSTO_APLICACAO%type;
  V_CODIGO_NEW           supr_065.CODIGO%type;
  V_SOLICITACAO_NEW      supr_065.SOLICITACAO%type;
  V_OPERACAO             varchar(1);
  V_DATA_OPERACAO        date;

  ws_usuario_rede        varchar2(20);
  ws_maquina_rede        varchar2(40);
  ws_aplicativo          varchar2(20);
  ws_sid                 number(9);
  ws_empresa             number(3);
  ws_usuario_systextil   varchar2(250);
  ws_locale_usuario      varchar2(5);

  v_executa_trigger      number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      begin
         -- Grava a data/hora da insercao do registro (log)
         V_DATA_OPERACAO := sysdate;

         -- Dados do usuario logado
         inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                                 ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

         if ws_aplicativo <> 'vision.exe'
         then ws_usuario_systextil := 'PL/SQL';
         end if;

         --Alimenta as variaveis NEW caso seja insert ou update
         if  inserting or updating
         then
            if inserting
            then  V_OPERACAO := 'I';
            else  V_OPERACAO := 'U';
            end if;

            V_NUM_REQUISICAO_NEW   := :NEW.NUM_REQUISICAO;
            V_DATA_REQUISICAO_NEW  := :NEW.DATA_REQUISICAO;
            V_CENTRO_CUSTO_NEW     := :NEW.CENTRO_CUSTO;
            V_NOME_REQUISIT_NEW    := :NEW.NOME_REQUISIT;
            V_OBSERVACAO_REQ_NEW   := :NEW.OBSERVACAO_REQ;
            V_COD_CANCELAMENTO_NEW := :NEW.COD_CANCELAMENTO;
            V_CODIGO_DEPOSITO_NEW  := :NEW.CODIGO_DEPOSITO;
            V_CODIGO_EMPRESA_NEW   := :NEW.CODIGO_EMPRESA;
            V_CCUSTO_APLICACAO_NEW := :NEW.CCUSTO_APLICACAO;
            V_CODIGO_NEW           := :NEW.CODIGO;
            V_SOLICITACAO_NEW      := :NEW.SOLICITACAO;
         end if; -- Fim do inserting or updating

         -- Elimina as variaveis OLD caso seja insert ou update
         if deleting or updating
         then
            if deleting
            then
               V_OPERACAO := 'D';
            else
               V_OPERACAO := 'U';
            end if;

            V_NUM_REQUISICAO_OLD   := :OLD.NUM_REQUISICAO;
            V_DATA_REQUISICAO_OLD  := :OLD.DATA_REQUISICAO;
            V_CENTRO_CUSTO_OLD     := :OLD.CENTRO_CUSTO;
            V_NOME_REQUISIT_OLD    := :OLD.NOME_REQUISIT;
            V_OBSERVACAO_REQ_OLD   := :OLD.OBSERVACAO_REQ;
            V_COD_CANCELAMENTO_OLD := :OLD.COD_CANCELAMENTO;
            V_CODIGO_DEPOSITO_OLD  := :OLD.CODIGO_DEPOSITO;
            V_CODIGO_EMPRESA_OLD   := :OLD.CODIGO_EMPRESA;
            V_CCUSTO_APLICACAO_OLD := :OLD.CCUSTO_APLICACAO;
            V_CODIGO_OLD           := :OLD.CODIGO;
            V_SOLICITACAO_OLD      := :OLD.SOLICITACAO;
         end if; -- Fim do deleting or updating

         -- Insere o registro na supr_065_log
         insert into supr_065_log (
           NUM_REQUISICAO_OLD,
           DATA_REQUISICAO_OLD,
           CENTRO_CUSTO_OLD,
           NOME_REQUISIT_OLD,
           OBSERVACAO_REQ_OLD,
           COD_CANCELAMENTO_OLD,
           CODIGO_DEPOSITO_OLD,
           CODIGO_EMPRESA_OLD,
           CCUSTO_APLICACAO_OLD,
           CODIGO_OLD,
           SOLICITACAO_OLD,
           NUM_REQUISICAO_NEW,
           DATA_REQUISICAO_NEW,
           CENTRO_CUSTO_NEW,
           NOME_REQUISIT_NEW,
           OBSERVACAO_REQ_NEW,
           COD_CANCELAMENTO_NEW,
           CODIGO_DEPOSITO_NEW,
           CODIGO_EMPRESA_NEW,
           CCUSTO_APLICACAO_NEW,
           CODIGO_NEW,
           SOLICITACAO_NEW,
           OPERACAO,
           DATA_OPERACAO,
           USUARIO_REDE,
           MAQUINA_REDE,
           USUARIO_SYSTEXTIL,
           APLICATIVO)
        values (
           V_NUM_REQUISICAO_OLD,
           V_DATA_REQUISICAO_OLD,
           V_CENTRO_CUSTO_OLD,
           V_NOME_REQUISIT_OLD,
           V_OBSERVACAO_REQ_OLD,
           V_COD_CANCELAMENTO_OLD,
           V_CODIGO_DEPOSITO_OLD,
           V_CODIGO_EMPRESA_OLD,
           V_CCUSTO_APLICACAO_OLD,
           V_CODIGO_OLD,
           V_SOLICITACAO_OLD,
           V_NUM_REQUISICAO_NEW,
           V_DATA_REQUISICAO_NEW,
           V_CENTRO_CUSTO_NEW,
           V_NOME_REQUISIT_NEW,
           V_OBSERVACAO_REQ_NEW,
           V_COD_CANCELAMENTO_NEW,
           V_CODIGO_DEPOSITO_NEW,
           V_CODIGO_EMPRESA_NEW,
           V_CCUSTO_APLICACAO_NEW,
           V_CODIGO_NEW,
           V_SOLICITACAO_NEW,
           V_OPERACAO,
           V_DATA_OPERACAO,
           WS_USUARIO_REDE,
           WS_MAQUINA_REDE,
           WS_USUARIO_SYSTEXTIL,
           WS_APLICATIVO);
        exception
           when OTHERS
           then raise_application_error (-20000, 'Nao atualizou a tabela de log supr_065_log.');

      end;
   end if;
end inter_tr_supr_065_log;

-- ALTER TRIGGER "INTER_TR_SUPR_065_LOG" ENABLE
 

/

exec inter_pr_recompile;

