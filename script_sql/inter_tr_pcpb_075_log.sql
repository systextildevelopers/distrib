create or replace trigger inter_tr_PCPB_075_log 
after insert or delete or update 
on PCPB_075 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(20) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu?rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    begin 
       select hdoc_090.programa 
       into v_nome_programa 
       from hdoc_090 
       where hdoc_090.sid = ws_sid 
         and rownum       = 1 
         and hdoc_090.programa not like '%menu%' 
         and hdoc_090.programa not like '%_m%'; 
       exception 
         when no_data_found then            v_nome_programa := 'SQL'; 
    end; 
 
 
 
 if inserting 
 then 
    begin 
 
        insert into PCPB_075_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           TIPO_INFORMACAO_OLD,   /*8*/ 
           TIPO_INFORMACAO_NEW,   /*9*/ 
           ORDEM_PRODUCAO_OLD,   /*10*/ 
           ORDEM_PRODUCAO_NEW,   /*11*/ 
           NIVEL_ESTRUTURA_OLD,   /*12*/ 
           NIVEL_ESTRUTURA_NEW,   /*13*/ 
           GRUPO_ESTRUTURA_OLD,   /*14*/ 
           GRUPO_ESTRUTURA_NEW,   /*15*/ 
           SUBGRU_ESTRUTURA_OLD,   /*16*/ 
           SUBGRU_ESTRUTURA_NEW,   /*17*/ 
           ITEM_ESTRUTURA_OLD,   /*18*/ 
           ITEM_ESTRUTURA_NEW,   /*19*/ 
           SEQUENCIA_OLD,   /*20*/ 
           SEQUENCIA_NEW,   /*21*/ 
           CODIGO_INFORMACAO_OLD,   /*22*/ 
           CODIGO_INFORMACAO_NEW,   /*23*/ 
           OBSERVACAO_OLD,   /*24*/ 
           OBSERVACAO_NEW,   /*25*/ 
           VALOR_01_OLD,   /*26*/ 
           VALOR_01_NEW,   /*27*/ 
           VALOR_02_OLD,   /*28*/ 
           VALOR_02_NEW,   /*29*/ 
           SITUACAO_OLD,   /*30*/ 
           SITUACAO_NEW,   /*31*/ 
           CODIGO_ESTAGIO_OLD,   /*32*/ 
           CODIGO_ESTAGIO_NEW,   /*33*/ 
           DATA_INFORMACAO_OLD,   /*34*/ 
           DATA_INFORMACAO_NEW,   /*35*/ 
           DATA_ALT_OLD,   /*36*/ 
           DATA_ALT_NEW,   /*37*/ 
           HORA_ALT_OLD,   /*38*/ 
           HORA_ALT_NEW,   /*39*/ 
           VALOR_03_OLD,   /*40*/ 
           VALOR_03_NEW,   /*41*/ 
           VALOR_04_OLD,   /*42*/ 
           VALOR_04_NEW,   /*43*/ 
           VALOR_05_OLD,   /*44*/ 
           VALOR_05_NEW,   /*45*/ 
           VALOR_06_OLD,   /*46*/ 
           VALOR_06_NEW,   /*47*/ 
           VALOR_07_OLD,   /*48*/ 
           VALOR_07_NEW,   /*49*/ 
           VALOR_08_OLD,   /*50*/ 
           VALOR_08_NEW,   /*51*/ 
           VALOR_09_OLD,   /*52*/ 
           VALOR_09_NEW,   /*53*/ 
           VALOR_10_OLD,   /*54*/ 
           VALOR_10_NEW,   /*55*/ 
           VALOR_11_OLD,   /*56*/ 
           VALOR_11_NEW,   /*57*/ 
           EXIGE_BAIXA_OLD,   /*58*/ 
           EXIGE_BAIXA_NEW,   /*59*/ 
           LOTE_PRODUTO_OLD,   /*60*/ 
           LOTE_PRODUTO_NEW,   /*61*/ 
           CODIGO_MOTIVO_OLD,   /*62*/ 
           CODIGO_MOTIVO_NEW,   /*63*/ 
           CODIGO_INFORMANTE_OLD,   /*64*/ 
           CODIGO_INFORMANTE_NEW,   /*65*/ 
           HORA_INFORMACAO_OLD,   /*66*/ 
           HORA_INFORMACAO_NEW    /*67*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.TIPO_INFORMACAO, /*9*/   
           0,/*10*/
           :new.ORDEM_PRODUCAO, /*11*/   
           '',/*12*/
           :new.NIVEL_ESTRUTURA, /*13*/   
           '',/*14*/
           :new.GRUPO_ESTRUTURA, /*15*/   
           '',/*16*/
           :new.SUBGRU_ESTRUTURA, /*17*/   
           '',/*18*/
           :new.ITEM_ESTRUTURA, /*19*/   
           0,/*20*/
           :new.SEQUENCIA, /*21*/   
           0,/*22*/
           :new.CODIGO_INFORMACAO, /*23*/   
           '',/*24*/
           :new.OBSERVACAO, /*25*/   
           0,/*26*/
           :new.VALOR_01, /*27*/   
           0,/*28*/
           :new.VALOR_02, /*29*/   
           0,/*30*/
           :new.SITUACAO, /*31*/   
           0,/*32*/
           :new.CODIGO_ESTAGIO, /*33*/   
           null,/*34*/
           :new.DATA_INFORMACAO, /*35*/   
           null,/*36*/
           :new.DATA_ALT, /*37*/   
           null,/*38*/
           :new.HORA_ALT, /*39*/   
           0,/*40*/
           :new.VALOR_03, /*41*/   
           0,/*42*/
           :new.VALOR_04, /*43*/   
           0,/*44*/
           :new.VALOR_05, /*45*/   
           0,/*46*/
           :new.VALOR_06, /*47*/   
           0,/*48*/
           :new.VALOR_07, /*49*/   
           0,/*50*/
           :new.VALOR_08, /*51*/   
           0,/*52*/
           :new.VALOR_09, /*53*/   
           0,/*54*/
           :new.VALOR_10, /*55*/   
           0,/*56*/
           :new.VALOR_11, /*57*/   
           0,/*58*/
           :new.EXIGE_BAIXA, /*59*/   
           0,/*60*/
           :new.LOTE_PRODUTO, /*61*/   
           0,/*62*/
           :new.CODIGO_MOTIVO, /*63*/   
           0,/*64*/
           :new.CODIGO_INFORMANTE, /*65*/   
           null,/*66*/
           :new.HORA_INFORMACAO /*67*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into PCPB_075_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           TIPO_INFORMACAO_OLD, /*8*/   
           TIPO_INFORMACAO_NEW, /*9*/   
           ORDEM_PRODUCAO_OLD, /*10*/   
           ORDEM_PRODUCAO_NEW, /*11*/   
           NIVEL_ESTRUTURA_OLD, /*12*/   
           NIVEL_ESTRUTURA_NEW, /*13*/   
           GRUPO_ESTRUTURA_OLD, /*14*/   
           GRUPO_ESTRUTURA_NEW, /*15*/   
           SUBGRU_ESTRUTURA_OLD, /*16*/   
           SUBGRU_ESTRUTURA_NEW, /*17*/   
           ITEM_ESTRUTURA_OLD, /*18*/   
           ITEM_ESTRUTURA_NEW, /*19*/   
           SEQUENCIA_OLD, /*20*/   
           SEQUENCIA_NEW, /*21*/   
           CODIGO_INFORMACAO_OLD, /*22*/   
           CODIGO_INFORMACAO_NEW, /*23*/   
           OBSERVACAO_OLD, /*24*/   
           OBSERVACAO_NEW, /*25*/   
           VALOR_01_OLD, /*26*/   
           VALOR_01_NEW, /*27*/   
           VALOR_02_OLD, /*28*/   
           VALOR_02_NEW, /*29*/   
           SITUACAO_OLD, /*30*/   
           SITUACAO_NEW, /*31*/   
           CODIGO_ESTAGIO_OLD, /*32*/   
           CODIGO_ESTAGIO_NEW, /*33*/   
           DATA_INFORMACAO_OLD, /*34*/   
           DATA_INFORMACAO_NEW, /*35*/   
           DATA_ALT_OLD, /*36*/   
           DATA_ALT_NEW, /*37*/   
           HORA_ALT_OLD, /*38*/   
           HORA_ALT_NEW, /*39*/   
           VALOR_03_OLD, /*40*/   
           VALOR_03_NEW, /*41*/   
           VALOR_04_OLD, /*42*/   
           VALOR_04_NEW, /*43*/   
           VALOR_05_OLD, /*44*/   
           VALOR_05_NEW, /*45*/   
           VALOR_06_OLD, /*46*/   
           VALOR_06_NEW, /*47*/   
           VALOR_07_OLD, /*48*/   
           VALOR_07_NEW, /*49*/   
           VALOR_08_OLD, /*50*/   
           VALOR_08_NEW, /*51*/   
           VALOR_09_OLD, /*52*/   
           VALOR_09_NEW, /*53*/   
           VALOR_10_OLD, /*54*/   
           VALOR_10_NEW, /*55*/   
           VALOR_11_OLD, /*56*/   
           VALOR_11_NEW, /*57*/   
           EXIGE_BAIXA_OLD, /*58*/   
           EXIGE_BAIXA_NEW, /*59*/   
           LOTE_PRODUTO_OLD, /*60*/   
           LOTE_PRODUTO_NEW, /*61*/   
           CODIGO_MOTIVO_OLD, /*62*/   
           CODIGO_MOTIVO_NEW, /*63*/   
           CODIGO_INFORMANTE_OLD, /*64*/   
           CODIGO_INFORMANTE_NEW, /*65*/   
           HORA_INFORMACAO_OLD, /*66*/   
           HORA_INFORMACAO_NEW  /*67*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.TIPO_INFORMACAO,  /*8*/  
           :new.TIPO_INFORMACAO, /*9*/   
           :old.ORDEM_PRODUCAO,  /*10*/  
           :new.ORDEM_PRODUCAO, /*11*/   
           :old.NIVEL_ESTRUTURA,  /*12*/  
           :new.NIVEL_ESTRUTURA, /*13*/   
           :old.GRUPO_ESTRUTURA,  /*14*/  
           :new.GRUPO_ESTRUTURA, /*15*/   
           :old.SUBGRU_ESTRUTURA,  /*16*/  
           :new.SUBGRU_ESTRUTURA, /*17*/   
           :old.ITEM_ESTRUTURA,  /*18*/  
           :new.ITEM_ESTRUTURA, /*19*/   
           :old.SEQUENCIA,  /*20*/  
           :new.SEQUENCIA, /*21*/   
           :old.CODIGO_INFORMACAO,  /*22*/  
           :new.CODIGO_INFORMACAO, /*23*/   
           :old.OBSERVACAO,  /*24*/  
           :new.OBSERVACAO, /*25*/   
           :old.VALOR_01,  /*26*/  
           :new.VALOR_01, /*27*/   
           :old.VALOR_02,  /*28*/  
           :new.VALOR_02, /*29*/   
           :old.SITUACAO,  /*30*/  
           :new.SITUACAO, /*31*/   
           :old.CODIGO_ESTAGIO,  /*32*/  
           :new.CODIGO_ESTAGIO, /*33*/   
           :old.DATA_INFORMACAO,  /*34*/  
           :new.DATA_INFORMACAO, /*35*/   
           :old.DATA_ALT,  /*36*/  
           :new.DATA_ALT, /*37*/   
           :old.HORA_ALT,  /*38*/  
           :new.HORA_ALT, /*39*/   
           :old.VALOR_03,  /*40*/  
           :new.VALOR_03, /*41*/   
           :old.VALOR_04,  /*42*/  
           :new.VALOR_04, /*43*/   
           :old.VALOR_05,  /*44*/  
           :new.VALOR_05, /*45*/   
           :old.VALOR_06,  /*46*/  
           :new.VALOR_06, /*47*/   
           :old.VALOR_07,  /*48*/  
           :new.VALOR_07, /*49*/   
           :old.VALOR_08,  /*50*/  
           :new.VALOR_08, /*51*/   
           :old.VALOR_09,  /*52*/  
           :new.VALOR_09, /*53*/   
           :old.VALOR_10,  /*54*/  
           :new.VALOR_10, /*55*/   
           :old.VALOR_11,  /*56*/  
           :new.VALOR_11, /*57*/   
           :old.EXIGE_BAIXA,  /*58*/  
           :new.EXIGE_BAIXA, /*59*/   
           :old.LOTE_PRODUTO,  /*60*/  
           :new.LOTE_PRODUTO, /*61*/   
           :old.CODIGO_MOTIVO,  /*62*/  
           :new.CODIGO_MOTIVO, /*63*/   
           :old.CODIGO_INFORMANTE,  /*64*/  
           :new.CODIGO_INFORMANTE, /*65*/   
           :old.HORA_INFORMACAO,  /*66*/  
           :new.HORA_INFORMACAO  /*67*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into PCPB_075_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           TIPO_INFORMACAO_OLD, /*8*/   
           TIPO_INFORMACAO_NEW, /*9*/   
           ORDEM_PRODUCAO_OLD, /*10*/   
           ORDEM_PRODUCAO_NEW, /*11*/   
           NIVEL_ESTRUTURA_OLD, /*12*/   
           NIVEL_ESTRUTURA_NEW, /*13*/   
           GRUPO_ESTRUTURA_OLD, /*14*/   
           GRUPO_ESTRUTURA_NEW, /*15*/   
           SUBGRU_ESTRUTURA_OLD, /*16*/   
           SUBGRU_ESTRUTURA_NEW, /*17*/   
           ITEM_ESTRUTURA_OLD, /*18*/   
           ITEM_ESTRUTURA_NEW, /*19*/   
           SEQUENCIA_OLD, /*20*/   
           SEQUENCIA_NEW, /*21*/   
           CODIGO_INFORMACAO_OLD, /*22*/   
           CODIGO_INFORMACAO_NEW, /*23*/   
           OBSERVACAO_OLD, /*24*/   
           OBSERVACAO_NEW, /*25*/   
           VALOR_01_OLD, /*26*/   
           VALOR_01_NEW, /*27*/   
           VALOR_02_OLD, /*28*/   
           VALOR_02_NEW, /*29*/   
           SITUACAO_OLD, /*30*/   
           SITUACAO_NEW, /*31*/   
           CODIGO_ESTAGIO_OLD, /*32*/   
           CODIGO_ESTAGIO_NEW, /*33*/   
           DATA_INFORMACAO_OLD, /*34*/   
           DATA_INFORMACAO_NEW, /*35*/   
           DATA_ALT_OLD, /*36*/   
           DATA_ALT_NEW, /*37*/   
           HORA_ALT_OLD, /*38*/   
           HORA_ALT_NEW, /*39*/   
           VALOR_03_OLD, /*40*/   
           VALOR_03_NEW, /*41*/   
           VALOR_04_OLD, /*42*/   
           VALOR_04_NEW, /*43*/   
           VALOR_05_OLD, /*44*/   
           VALOR_05_NEW, /*45*/   
           VALOR_06_OLD, /*46*/   
           VALOR_06_NEW, /*47*/   
           VALOR_07_OLD, /*48*/   
           VALOR_07_NEW, /*49*/   
           VALOR_08_OLD, /*50*/   
           VALOR_08_NEW, /*51*/   
           VALOR_09_OLD, /*52*/   
           VALOR_09_NEW, /*53*/   
           VALOR_10_OLD, /*54*/   
           VALOR_10_NEW, /*55*/   
           VALOR_11_OLD, /*56*/   
           VALOR_11_NEW, /*57*/   
           EXIGE_BAIXA_OLD, /*58*/   
           EXIGE_BAIXA_NEW, /*59*/   
           LOTE_PRODUTO_OLD, /*60*/   
           LOTE_PRODUTO_NEW, /*61*/   
           CODIGO_MOTIVO_OLD, /*62*/   
           CODIGO_MOTIVO_NEW, /*63*/   
           CODIGO_INFORMANTE_OLD, /*64*/   
           CODIGO_INFORMANTE_NEW, /*65*/   
           HORA_INFORMACAO_OLD, /*66*/   
           HORA_INFORMACAO_NEW /*67*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.TIPO_INFORMACAO, /*8*/   
           0, /*9*/
           :old.ORDEM_PRODUCAO, /*10*/   
           0, /*11*/
           :old.NIVEL_ESTRUTURA, /*12*/   
           '', /*13*/
           :old.GRUPO_ESTRUTURA, /*14*/   
           '', /*15*/
           :old.SUBGRU_ESTRUTURA, /*16*/   
           '', /*17*/
           :old.ITEM_ESTRUTURA, /*18*/   
           '', /*19*/
           :old.SEQUENCIA, /*20*/   
           0, /*21*/
           :old.CODIGO_INFORMACAO, /*22*/   
           0, /*23*/
           :old.OBSERVACAO, /*24*/   
           '', /*25*/
           :old.VALOR_01, /*26*/   
           0, /*27*/
           :old.VALOR_02, /*28*/   
           0, /*29*/
           :old.SITUACAO, /*30*/   
           0, /*31*/
           :old.CODIGO_ESTAGIO, /*32*/   
           0, /*33*/
           :old.DATA_INFORMACAO, /*34*/   
           null, /*35*/
           :old.DATA_ALT, /*36*/   
           null, /*37*/
           :old.HORA_ALT, /*38*/   
           null, /*39*/
           :old.VALOR_03, /*40*/   
           0, /*41*/
           :old.VALOR_04, /*42*/   
           0, /*43*/
           :old.VALOR_05, /*44*/   
           0, /*45*/
           :old.VALOR_06, /*46*/   
           0, /*47*/
           :old.VALOR_07, /*48*/   
           0, /*49*/
           :old.VALOR_08, /*50*/   
           0, /*51*/
           :old.VALOR_09, /*52*/   
           0, /*53*/
           :old.VALOR_10, /*54*/   
           0, /*55*/
           :old.VALOR_11, /*56*/   
           0, /*57*/
           :old.EXIGE_BAIXA, /*58*/   
           0, /*59*/
           :old.LOTE_PRODUTO, /*60*/   
           0, /*61*/
           :old.CODIGO_MOTIVO, /*62*/   
           0, /*63*/
           :old.CODIGO_INFORMANTE, /*64*/   
           0, /*65*/
           :old.HORA_INFORMACAO, /*66*/   
           null /*67*/
         );    
    end;    
 end if;    
end inter_tr_PCPB_075_log;
