create table obrf_663
 (perc_reducao_icms number(10,5) default 0.0,
     perc_icms_nota    number(10,5) default 0.0);
     
alter table obrf_663 add constraint uniq_obrf_663 unique(perc_icms_nota);
 
comment on column obrf_663.perc_reducao_icms is 'Percentual de redução de ICMS do benefício fiscal';
comment on column obrf_663.perc_icms_nota is 'Percentual de ICMS da nota fiscal';
/
