
  CREATE OR REPLACE PROCEDURE "INTER_PR_LIMPA_TEMPORARIO" is
begin
   declare
      nr_dias_aux number;
   begin
      nr_dias_aux := 0;
      for tabelas in (select distinct hdoc_890.tabela
                      from hdoc_890)
      loop
         begin
            if tabelas.tabela = 'rcnb_060'
            then
                for reg in (select r.tipo_registro, h.nr_dias
                            from hdoc_890 h, rcnb_060 r
                            where not exists (select hdoc_890.tipo_registro
                                              from hdoc_890
                                              where hdoc_890.temporario = 0
                                                and hdoc_890.tipo_registro = h.tipo_registro)
                              and r.tipo_registro = h.tipo_registro
                              and h.tabela = 'rcnb_060'
                            group by r.tipo_registro, h.nr_dias)
                loop
                    begin
                       select hdoc_890.nr_dias into nr_dias_aux
                       from hdoc_890
                       where hdoc_890.tipo_registro = reg.tipo_registro
                         and rownum = 1
                       order by hdoc_890.nr_dias desc;
                    exception
                      when no_data_found then
                         nr_dias_aux := 0;
                    end;

                    if nr_dias_aux > reg.nr_dias
                    then
                       reg.nr_dias := nr_dias_aux;
                    end if;

                    begin
                       delete from
                       rcnb_060 c
                       where to_date(c.data_insercao, 'DD:MM:YYYY') + reg.nr_dias <= to_date(sysdate, 'DD:MM:YYYY')
                         and c.tipo_registro = reg.tipo_registro;
                    exception
                       when others then
                          null;
                    end;
                end loop;
             end if;
          end;
       end loop;
   exception
      when others then
         null;
   end;

   commit;
end inter_pr_limpa_temporario;

 

/

exec inter_pr_recompile;

