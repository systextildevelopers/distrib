create table pedi_265
( CNPJ9                   NUMBER(9) NOT NULL,
  CNPJ4                   NUMBER(4) NOT NULL,
  CNPJ2                   NUMBER(2) NOT NULL,
  COD_MSG_NOTA            NUMBER(9) NOT NULL);
alter table pedi_265
add constraint pk_pedi_265 primary key (CNPJ9, CNPJ4, CNPJ2,COD_MSG_NOTA);

alter table pedi_265
  add constraint pedi265_pedi010 foreign key (CNPJ9,CNPJ4,CNPJ2) references pedi_010 (cgc_9,cgc_4,cgc_2);
alter table pedi_265
  add constraint pedi265_obrf874 foreign key (COD_MSG_NOTA) references obrf_874 (COD_MENSAGEM);
  
create synonym systextilrpt.pedi_265 for pedi_265; 
