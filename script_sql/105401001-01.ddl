alter table obrf_157
	add (vfcpst 		number(15,2) default 0.00,
	     vfcp 			number(15,2) default 0.00,
		 vfcpstret 		number(15,2) default 0.00,
		 vbcfcpufdest 	number(15,2) default 0.00,
		 pfcp			number(15,2) default 0.00
	);
comment on column obrf_157.vfcpst 		is 'Valor da Base de Cálculo de Redução do FCP';
comment on column obrf_157.vfcp 		is 'Valor da Base de Cálculo do FCP retido por Substituição Tributária';
comment on column obrf_157.vfcpstret 	is 'Valor da Base de Cálculo do FCP retido anteriormente por ST';
comment on column obrf_157.vbcfcpufdest is 'Valor da Base de Cálculo FCP na UF de destino';


alter table obrf_150
	add (vfcpstret 	number(15,2) default 0.00,
		 vfcpst 	number(15,2) default 0.00,
		 vipidevol 	number(15,2) default 0.00,
		 vfcp		number(15,2) default 0.00
	);
comment on column obrf_150.vfcpstret 	is 'Valor Total do FCP retido anteriormente por Substituição Tributária';
comment on column obrf_150.vfcpst 		is 'Valor Total do FCP (Fundo de Combate à Pobreza) retido por substituição';
comment on column obrf_150.vipidevol 	is 'Valor Total do IPI devolvido';
comment on column obrf_150.vfcp 		is 'Valor do Fundo de Combate a Pobreza (FCP)';
/
exec inter_pr_recompile;
