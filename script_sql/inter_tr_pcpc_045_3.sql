
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_045_3" 
     before insert
         or delete
         or update of qtde_produzida, qtde_pecas_2a, qtde_conserto, qtde_perdas
     on pcpc_045
     for each row
declare
   v_executa_trigger number;
   v_tem_reg_400     number;
   v_verifica_nxj     number;
   v_periodo_op        pcpc_020.periodo_producao%type;
   
begin
   -- INICIO - L�gica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementa��o executada para limpeza da base de dados do cliente
   -- e replica��o dos dados para base hist�rico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :new.executa_trigger = 3
         then
             v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1
      or :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :new.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
             v_executa_trigger := 0;
         end if;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :old.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;
   -- FINAL - L�gica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger in (0,3)
   then
      if inserting or updating
      then
         inter_pr_verif_reg_em_producao (2,:new.executa_trigger);
		 
		 begin
            -- Encontra o periodo de produção da Capa da Ordem de Produção.
            select pcpc_020.periodo_producao
            into   v_periodo_op
            from pcpc_020
            where pcpc_020.ordem_producao = :new.ordem_producao;
            exception
            when others then
               v_periodo_op := 0;
         end;
		 
		 if v_periodo_op = 0
		 then
			v_periodo_op := :new.pcpc040_perconf;
		 end if;
		 
		 -- Validação do período aberto ou fechado deve ser feito do período da Capa da Ordem e não do Pacote.
         --inter_pr_verif_per_fechado(:new.pcpc040_perconf,1);
         inter_pr_verif_per_fechado(v_periodo_op,1);
      end if;

      if deleting
      then
         inter_pr_verif_reg_em_producao (2,:old.executa_trigger);

         begin
            -- Encontra o periodo de produção da Capa da Ordem de Produção.
            select pcpc_020.periodo_producao
            into   v_periodo_op
            from pcpc_020
            where pcpc_020.ordem_producao = :old.ordem_producao;
            exception
            when others then
               v_periodo_op := 0;
         end;
		 
		 if v_periodo_op = 0
		 then
			v_periodo_op := :old.pcpc040_perconf;
		 end if;
         
         -- Validação do período aberto ou fechado deve ser feito do período da Capa da Ordem e não do Pacote.
         --inter_pr_verif_per_fechado(:old.pcpc040_perconf,1);
         inter_pr_verif_per_fechado(v_periodo_op,1);
      end if;
   end if;

   v_tem_reg_400 := inter_fn_executou_recalculo();
   
   begin
      select 1 
      into v_verifica_nxj
      from fatu_500
      where fatu_500.rpt_impr_ftec = 'ft_oftec064_aa'
        and rownum  <= 1;
   exception when no_data_found then
      v_verifica_nxj := 0;
   end;

end inter_tr_pcpc_045_3;

/

exec inter_pr_recompile;

