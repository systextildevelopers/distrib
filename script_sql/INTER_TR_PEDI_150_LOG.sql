create or replace trigger inter_tr_pedi_150_log
   after insert or delete or update
   on pedi_150
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   long_aux                  varchar2(2000);
begin
   -- Dados do usuário logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

      if inserting
      then
         INSERT INTO hist_100
            ( tabela,            
              data_ocorr,       
              usuario_rede,      
              num01, 
              num02, 
              num03, 
              num04,  
              operacao,   
              aplicacao, 
              maquina_rede,       
              long01
            )
         VALUES
            ( 'PEDI_150',      
              sysdate,          
              ws_usuario_rede,  
              :new.CD_CLI_CGC_CLI9,
              :new.CD_CLI_CGC_CLI4,
              :new.CD_CLI_CGC_CLI2,
              :new.SEQ_ENDERECO,
              'I',
              ws_aplicativo,
              ws_maquina_rede,
              'CLIENTE: '
                                   || :new.CD_CLI_CGC_CLI9  || '/' ||
                                      :new.CD_CLI_CGC_CLI4  || '-' ||
                                      :new.CD_CLI_CGC_CLI2  ||
              chr(10)                                       ||
              'SEQUÊNCIA: ' 
                                    || :new.SEQ_ENDERECO    || 
                chr(10)                                     ||
              'TIPO ENDEREÇO: ' 
                                    || :new.TIPO_ENDERECO || 
                chr(10)                                   ||
              'ENDEREÇO: ' 
                                    || :new.END_ENTR_COBR || 
                chr(10)                                    ||
              'CNPJ: '
                                   || :new.CGC_ENTR_COBR9   || '/' ||
                                      :new.CGC_ENTR_COBR4   || '-' ||
                                      :new.CGC_ENTR_COBR2   ||
              chr(10)                                       ||
              'INSCRIÇÃO ESTADUAL:'
                                   || :new.IE_ENTR_COBR      ||
              chr(10)                                        ||
              'CEP: '
                                   || :new.CEP_ENTR_COBR    ||
              'BAIRRO: '
                                   || :new.BAIRRO_ENTR_COBR    ||
              chr(10)                                       ||
              'CIDADE: '
                                   || :new.CID_ENTR_COBR    ||
              chr(10)                                       ||
              'TELEFONE: '
                                   || :new.FONE_ENTR_COBR    ||
              chr(10)                                       ||
              'GERA NOTA ENTREGA: '
                                   || :new.GERA_NOTA_ENTREG    ||
              chr(10)                                       ||  
              'CIDADE CIF: '
                                   || :new.CIDADE_CIF    ||
              chr(10)                                       ||  
              'NOME COMPLEMENTO: '
                                   || :new.NOME_COMPLEMENTO ||
              chr(10)                                       ||  
              'FONE COMPLEMENTO: '
                                   || :new.FONE_COMPLEMENTO    ||
              chr(10)                                       ||
              'CEP COMPLEMENTO: '
                                   || :new.CEP_COMPLEMENTO    ||
              chr(10)                                       ||  
              'REDESPACHO: '
                                   || :new.TRANSP_REDESPACHO9   || '/' ||
                                      :new.TRANSP_REDESPACHO4   || '-' ||
                                      :new.TRANSP_REDESPACHO2   ||
              chr(10)                                       || 
              'NUMERO IMOVEL: '
                                   || :new.NUMERO_IMOVEL  ||
              chr(10)                                      || 
              'COMPLEMENTO ENDEREÇO: '
                                   || :new.COMPLEMENTO_ENDERECO    ||
              chr(10)                                       || 
              'FLAG EXPORTAÇÃO LOJA: '
                                   || :new.FLAG_EXPORTACAO_LOJA    ||
              chr(10)                                       || 
              'TIPO ENTREGA: '
                                   || :new.TIPO_ENTREGA ||
              chr(10)                                       ||  
              'DESTINATARIO ENTR COBR: '
                                   || :new.DESTINATARIO_ENTR_COBR   
           );
      end if;

        if updating 
            then
                long_aux := NULL;

                if :old.CD_CLI_CGC_CLI9 <> :new.CD_CLI_CGC_CLI9 or
                    :old.CD_CLI_CGC_CLI4 <> :new.CD_CLI_CGC_CLI4 or
                    :old.CD_CLI_CGC_CLI2 <> :new.CD_CLI_CGC_CLI2
                then
                    long_aux := long_aux ||
                    'CLIENTE: '
                                            || to_char(:old.CD_CLI_CGC_CLI9,'000000000')  || '/'
                                            || to_char(:old.CD_CLI_CGC_CLI4,'0000')       || '-'
                                            || to_char(:old.CD_CLI_CGC_CLI2,'00')         || ' -> ' ||
                                            chr(10);
                    long_aux := long_aux     ||
                        '                : ' || to_char(:new.CD_CLI_CGC_CLI9,'000000000')  || '/'
                                                || to_char(:new.CD_CLI_CGC_CLI4,'0000')       || '-'
                                                || to_char(:new.CD_CLI_CGC_CLI2,'00')         || 
                                                chr(10);
                end if;

                if  :old.SEQ_ENDERECO	 <> :new.SEQ_ENDERECO	
                then
                    long_aux := long_aux ||
                                            'SEQUÊNCIA: '
                                            || :old.SEQ_ENDERECO	   || ' ->'
                                            || :new.SEQ_ENDERECO	
                                            || chr(10);
                end if;

                if  :old.TIPO_ENDERECO <> :new.TIPO_ENDERECO 
                then
                        long_aux := long_aux ||
                        'TIPO ENDEREÇO: '     || :old.TIPO_ENDERECO
                                            ||  ' -> '
                                            || :new.TIPO_ENDERECO
                                            || chr(10);
                end if;

                if  :old.END_ENTR_COBR <> :new.END_ENTR_COBR 
                then
                        long_aux := long_aux ||
                        'ENDEREÇO: '     || :old.END_ENTR_COBR
                                            ||  ' -> '
                                            || :new.END_ENTR_COBR
                                            || chr(10);
                end if;
                
                if :old.CGC_ENTR_COBR9 <> :new.CGC_ENTR_COBR9 or
                    :old.CGC_ENTR_COBR4 <> :new.CGC_ENTR_COBR4 or
                    :old.CGC_ENTR_COBR2 <> :new.CGC_ENTR_COBR2
                then
                        long_aux := long_aux ||
                        'CNPJ: '     || to_char(:old.CGC_ENTR_COBR9,'000000000')  || '/'
                                            || to_char(:old.CGC_ENTR_COBR4,'0000')       || '-'
                                            || to_char(:old.CGC_ENTR_COBR2,'00')         || ' -> ' ||
                                            chr(10);
                    long_aux := long_aux     ||
                        '                : ' || to_char(:new.CGC_ENTR_COBR9,'000000000')  || '/'
                                                || to_char(:new.CGC_ENTR_COBR4,'0000')       || '-'
                                                || to_char(:new.CGC_ENTR_COBR2,'00')         || 
                                                chr(10);
                end if;

                if  :old.IE_ENTR_COBR <> :new.IE_ENTR_COBR 
                then
                        long_aux := long_aux ||
                        'INSCRIÇÃO ESTADUAL: '     || :old.IE_ENTR_COBR
                                            ||  ' -> '
                                            || :new.IE_ENTR_COBR
                                            || chr(10);
                end if;

                if  :old.CEP_ENTR_COBR <> :new.CEP_ENTR_COBR 
                then
                        long_aux := long_aux ||
                        'CEP: '     || :old.CEP_ENTR_COBR
                                            ||  ' -> '
                                            || :new.CEP_ENTR_COBR
                                            || chr(10);
                end if;

                if  :old.BAIRRO_ENTR_COBR <> :new.BAIRRO_ENTR_COBR 
                then
                        long_aux := long_aux ||
                        'BAIRRO: '     || :old.BAIRRO_ENTR_COBR
                                            ||  ' -> '
                                            || :new.BAIRRO_ENTR_COBR
                                            || chr(10);
                end if;

                if  :old.CID_ENTR_COBR <> :new.CID_ENTR_COBR 
                then
                        long_aux := long_aux ||
                        'CIDADE: '     || :old.CID_ENTR_COBR
                                            ||  ' -> '
                                            || :new.CID_ENTR_COBR
                                            || chr(10);
                end if;

                if  :old.FONE_ENTR_COBR <> :new.FONE_ENTR_COBR 
                then
                        long_aux := long_aux ||
                        'TELEFONE: '     || :old.FONE_ENTR_COBR
                                            ||  ' -> '
                                            || :new.FONE_ENTR_COBR
                                            || chr(10);
                end if;

                if  :old.GERA_NOTA_ENTREG <> :new.GERA_NOTA_ENTREG 
                then
                        long_aux := long_aux ||
                        'GERA NOTA ENTREGA: '     || :old.GERA_NOTA_ENTREG
                                            ||  ' -> '
                                            || :new.GERA_NOTA_ENTREG
                                            || chr(10);
                end if;

                if  :old.CIDADE_CIF <> :new.CIDADE_CIF 
                then
                        long_aux := long_aux ||
                        'CIDADE CIF: '     || :old.CIDADE_CIF
                                            ||  ' -> '
                                            || :new.CIDADE_CIF
                                            || chr(10);
                end if;

                if  :old.NOME_COMPLEMENTO <> :new.NOME_COMPLEMENTO 
                then
                        long_aux := long_aux ||
                        'NOME COMPLEMENTO: '     || :old.NOME_COMPLEMENTO
                                            ||  ' -> '
                                            || :new.NOME_COMPLEMENTO
                                            || chr(10);
                end if;

                if  :old.FONE_COMPLEMENTO <> :new.FONE_COMPLEMENTO 
                then
                        long_aux := long_aux ||
                        'FONE COMPLEMENTO: '     || :old.FONE_COMPLEMENTO
                                            ||  ' -> '
                                            || :new.FONE_COMPLEMENTO
                                            || chr(10);
                end if;

                if  :old.CEP_COMPLEMENTO <> :new.CEP_COMPLEMENTO 
                then
                        long_aux := long_aux ||
                        'CEP COMPLEMENTO: '     || :old.CEP_COMPLEMENTO
                                            ||  ' -> '
                                            || :new.CEP_COMPLEMENTO
                                            || chr(10);
                end if;

                if :old.TRANSP_REDESPACHO9 <> :new.TRANSP_REDESPACHO9 or
                    :old.TRANSP_REDESPACHO4 <> :new.TRANSP_REDESPACHO4 or
                    :old.TRANSP_REDESPACHO2 <> :new.TRANSP_REDESPACHO2
                then
                        long_aux := long_aux ||
                        'REDESPACHO: '     || to_char(:old.TRANSP_REDESPACHO9,'000000000')  || '/'
                                            || to_char(:old.TRANSP_REDESPACHO4,'0000')       || '-'
                                            || to_char(:old.TRANSP_REDESPACHO2,'00')         || ' -> ' ||
                                            chr(10);
                    long_aux := long_aux     ||
                        '                : ' || to_char(:new.TRANSP_REDESPACHO9,'000000000')  || '/'
                                                || to_char(:new.TRANSP_REDESPACHO4,'0000')       || '-'
                                                || to_char(:new.TRANSP_REDESPACHO2,'00')         || 
                                                chr(10);
                end if;

                if  :old.NUMERO_IMOVEL <> :new.NUMERO_IMOVEL 
                then
                        long_aux := long_aux ||
                        'NUMERO IMOVEL: '     || :old.NUMERO_IMOVEL
                                            ||  ' -> '
                                            || :new.NUMERO_IMOVEL
                                            || chr(10);
                end if;

                if  :old.COMPLEMENTO_ENDERECO <> :new.COMPLEMENTO_ENDERECO 
                then
                        long_aux := long_aux ||
                        'COMPLEMENTO ENDEREÇO: '     || :old.COMPLEMENTO_ENDERECO
                                            ||  ' -> '
                                            || :new.COMPLEMENTO_ENDERECO
                                            || chr(10);
                end if;

                if  :old.FLAG_EXPORTACAO_LOJA <> :new.FLAG_EXPORTACAO_LOJA 
                then
                        long_aux := long_aux ||
                        'FLAG EXPORTAÇÃO LOJA: '     || :old.FLAG_EXPORTACAO_LOJA
                                            ||  ' -> '
                                            || :new.FLAG_EXPORTACAO_LOJA
                                            || chr(10);
                end if;

                if  :old.TIPO_ENTREGA <> :new.TIPO_ENTREGA 
                then
                        long_aux := long_aux ||
                        'TIPO ENTREGA: '     || :old.TIPO_ENTREGA
                                            ||  ' -> '
                                            || :new.TIPO_ENTREGA
                                            || chr(10);
                end if;

                if  :old.DESTINATARIO_ENTR_COBR <> :new.DESTINATARIO_ENTR_COBR 
                then
                        long_aux := long_aux ||
                        'DESTINATARIO ENTR COBR: '     || :old.DESTINATARIO_ENTR_COBR
                                            ||  ' -> '
                                            || :new.DESTINATARIO_ENTR_COBR
                                            || chr(10);
                end if;
            
                INSERT INTO hist_100
                    ( tabela,            
                    data_ocorr,        
                    usuario_rede,      
                    num01, 
                    num02, 
                    num03, 
                    num04,     
                    operacao, 
                    aplicacao,   
                    maquina_rede,  
                    long01
                    )
                VALUES
                    ( 'PEDI_150',           
                    sysdate,              
                    ws_usuario_rede,      
                    :new.CD_CLI_CGC_CLI9,
                    :new.CD_CLI_CGC_CLI4,
                    :new.CD_CLI_CGC_CLI2,
                    :new.SEQ_ENDERECO,  
                    'A',
                    ws_aplicativo,
                    ws_maquina_rede,
                    long_aux
                );
            end if;

            if deleting
            then
                INSERT INTO hist_100
                    ( tabela,            
                    data_ocorr,        
                    usuario_rede,      
                    num01, 
                    num02, 
                    num03, 
                    num04,
                    operacao,
                    aplicacao,
                    maquina_rede
                    )
                VALUES
                    ( 'PEDI_150',        
                    sysdate,           
                    ws_usuario_rede,   
                    :old.CD_CLI_CGC_CLI9,
                    :old.CD_CLI_CGC_CLI4,
                    :old.CD_CLI_CGC_CLI2,
                    :old.SEQ_ENDERECO,
                    'D',
                    ws_aplicativo,
                    ws_maquina_rede
                );
        end if;

    
end inter_tr_pedi_150_log;

