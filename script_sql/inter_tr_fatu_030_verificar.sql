create or replace trigger "INTER_TR_FATU_030_VERIFICAR"
before delete
on fatu_030
for each row
declare

tem_nota_pendente number(1);

begin

  tem_nota_pendente := 0;
  begin
    select 1 into tem_nota_pendente
    from fatu_050
    where fatu_050.nr_solicitacao  = :old.nr_solicitacao
      and fatu_050.situacao_nfisc  = 3
      and fatu_050.num_nota_fiscal = :old.num_nota_fiscal
      and rownum                   = 1;
   exception when no_data_found
   then
     tem_nota_pendente := 0;
   end;

   if tem_nota_pendente = 1
   then
      raise_application_error(-20000,'Não é possível deletar a solicitação, pois existem notas pendentes para a mesma.');
   end if;

end inter_tr_fatu_030_verificar;

--ALTER TRIGGER "INTER_TR_FATU_030_VERIFICAR" DISABLE;
