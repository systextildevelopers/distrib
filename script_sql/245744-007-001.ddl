CREATE TABLE EXPT_060 (
    ENDERECO         VARCHAR2(40),
	DESCRICAO        VARCHAR2(40)
);

ALTER TABLE EXPT_060
ADD CONSTRAINT PK_EXPT_060 PRIMARY KEY (ENDERECO);
