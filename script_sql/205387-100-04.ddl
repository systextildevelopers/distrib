alter table tmrp_685 add familia_atributo_str varchar2(6);
comment on column tmrp_685.familia_atributo_str is 'Valor informado na geração da projeção (tmrp_f680)';

alter table tmrp_685 add codigo_grupo_atrib number(6);
comment on column tmrp_685.codigo_grupo_atrib is 'Valor informado na geração da projeção (tmrp_f680)';

alter table tmrp_685 add codigo_subgrupo_atrib number(6);
comment on column tmrp_685.codigo_subgrupo_atrib is 'Valor informado na geração da projeção (tmrp_f680)';

alter table tmrp_685 add codigo_atributo number(6);
comment on column tmrp_685.codigo_atributo is 'Valor informado na geração da projeção (tmrp_f680)';

exec inter_pr_recompile;
