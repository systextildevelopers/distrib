alter table obrf_050
add segreg_outr_cred_presumido_sai number(15,2) default 0.0;

comment on column obrf_050.segreg_outr_cred_presumido_sai is 'Quadro 09 - campo 38 - Segregação de outros créditos permitidos para compensar com o débito pela utilização do crédito presumido ';

alter table obrf_059
add credito_compen_deb_presumido number(15,2) default 0.0;

comment on column obrf_059.credito_compen_deb_presumido is 'Quadro 14 - campo 31 - Créditos Permitidos para Compensar com o Débito pela Utilização do Crédito Presumido ';

/
exec inter_pr_recompile;
