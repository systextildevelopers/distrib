DECLARE tmpInt number;
v_def empr_008.val_str%TYPE;
CURSOR parametro_c IS SELECT codigo_empresa FROM fatu_500;
BEGIN
   FOR reg_parametro IN parametro_c
   LOOP
      SELECT nvl(count(*),0)
      INTO tmpInt
      FROM fatu_500
      WHERE fatu_500.codigo_empresa = reg_parametro.codigo_empresa;
      IF(tmpInt > 0)
      THEN
        begin
           select val_str
           into v_def
           from empr_008
           where param = 'contab.agrupaLancamentoContabilChave'
           and codigo_empresa = reg_parametro.codigo_empresa; 
          exception when no_data_found then
            v_def := 'N';            
        end;
        BEGIN
           INSERT INTO empr_008 (CODIGO_EMPRESA, PARAM, VAL_STR)
           VALUES (reg_parametro.codigo_empresa, 'contab.agrupaLctoContabCrec', v_def);
        END;
      END IF;
    END LOOP;
   COMMIT;
END;
