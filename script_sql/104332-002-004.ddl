create table OBRF_823
( COD_EMPRESA        NUMBER(3)    default 0 not null,
  MES                NUMBER(2)    default 0 not null,
  ANO                NUMBER(4)    default 0 not null,
  ID                 NUMBER(9)    default 0 not null,
  COD_MENSAGEM       NUMBER(6)    default 0 not null,
  COD_AJUSTE53       VARCHAR2(10) default ' ' not null,
  NOTA               NUMBER(9)    default 0 not null,
  SERIE              VARCHAR2(3)  default ' ' not null,
  CNPJ9              NUMBER(9)    default 0 not null,
  CNPJ4              NUMBER(4)    default 0 not null,
  CNPJ2              NUMBER(2)    default 0 not null,
  NIVEL              VARCHAR2(1)  default ' ' not null,
  GRUPO              VARCHAR2(5)  default ' ' not null,
  SUBGRUPO           VARCHAR2(3)  default ' ' not null,
  ITEM               VARCHAR2(6)  default ' ' not null,
  DESC_PRODUTO       VARCHAR2(70) default ' ',
  VALOR_CONTABIL     NUMBER(13,2) default 0.00,
  ICMS_BASE          NUMBER(13,2) default 0.00,
  ICMS_PERC          NUMBER(5,2)  default 0.00,
  ICMS_VALOR         NUMBER(13,2) default 0.00,
  ICMS_OUTROS        NUMBER(13,2) default 0.00,
  DESCR_AJUSTE53     VARCHAR2(60) default ' ',
  DATA_VISTO         DATE,
  APURA_EST_JUSTIFICA   VARCHAR2(60) default ' ',
  APURA_EST_COD_MOTIVO  VARCHAR2(60) default ' ',
  APURA_EST_QUADRO      VARCHAR2(60) default ' ',
  IND_ENT_SAI           VARCHAR2(1) default ' ',
  SEQ_NF_TRANSFERENCIA  NUMBER(9) default 0,
  AUTO_INFRACAO         NUMBER(9) default 0);

alter table OBRF_823 add constraint PK_OBRF_823 primary key (COD_EMPRESA,MES,ANO,ID,COD_MENSAGEM,COD_AJUSTE53,NOTA,SERIE,CNPJ9,CNPJ4,CNPJ2,NIVEL,GRUPO,SUBGRUPO,ITEM);
  
create synonym systextilrpt.OBRF_823 for OBRF_823; 
