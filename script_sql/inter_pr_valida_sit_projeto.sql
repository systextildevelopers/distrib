
  CREATE OR REPLACE PROCEDURE "INTER_PR_VALIDA_SIT_PROJETO" (
   p_nivel_projeto       in varchar2,
   p_referencia_projeto  in varchar2,
   p_empresa_logada      in number,
   p_tipo_msg            in number DEFAULT 0
) is
   v_codigo_projeto            varchar2(6);
   v_valida_projeto_cancelado  varchar2(1);
   
begin
   v_valida_projeto_cancelado := 'N';

   begin
      select fatu_504.valida_projeto_cancelado
      into   v_valida_projeto_cancelado
      from fatu_504
      where fatu_504.codigo_empresa = p_empresa_logada;
   exception when OTHERS then
      v_valida_projeto_cancelado := 'N';
   end;

   if v_valida_projeto_cancelado = 'S'
   then
      v_codigo_projeto := '';

      begin
         select basi_001.codigo_projeto
         into   v_codigo_projeto
         from basi_001
         where basi_001.nivel_produto    = p_nivel_projeto
           and basi_001.grupo_produto    = p_referencia_projeto
           and basi_001.situacao_projeto = 3;
      exception when OTHERS then
         v_codigo_projeto := '';
      end;

      if sql%found
      then
         raise_application_error(-20000, 'ATEN��O! A refer�ncia da ordem de produ��o est� relacionada com o projeto ' || v_codigo_projeto ||
                                         ' e o mesmo est� reprovado. N�o ser� poss�vel digitar ordem de produ��o! Cancele a ordem de produ��o com o PCP!');
      end if;
   end if;

end inter_pr_valida_sit_projeto;

 

/

exec inter_pr_recompile;

