-- Atende a 105235-001 e 105236-001

ALTER TABLE OPER_001 ADD (
CLIENT_INFO VARCHAR2(64)
);
COMMENT ON COLUMN OPER_001.CLIENT_INFO IS 'Identifica��o da m�quina remota que agendou o processo';
  
EXEC inter_pr_recompile;
