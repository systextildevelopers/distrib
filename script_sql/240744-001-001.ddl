alter table cpag_015
add vlr_var_cambial number(15,2) default 0.00;

alter table fatu_075
add vlr_var_cambial number(15,2) default 0.00;

alter table fatu_075_hist
add vlr_var_cambial_old number(15,2) default 0.00;

alter table fatu_075_hist
add vlr_var_cambial_new number(15,2) default 0.00;
