
  CREATE OR REPLACE PROCEDURE "INTER_PR_SUGESTAO_FATUR" (p_pedido_venda     in number,     p_sequencia_pedido in number,
                                                     p_nivel_produto    in varchar2,   p_grupo_produto    in varchar2, 
                                                     p_subgrupo_produto in varchar2,   p_item_produto     in varchar2, 
                                                     p_deposito         in number,     p_lote             in number, 
                                                     p_qtde_sugerida    in number,     p_nr_sugestao      in number, 
                                                     p_nome_programa    in varchar2)
is
begin
   -- Atualizando a qtde sugerida da tabela de estoques
   update estq_040
   set qtde_sugerida     = qtde_sugerida + p_qtde_sugerida,
       nome_prog_040     = p_nome_programa
   where cditem_nivel99  = p_nivel_produto
     and cditem_grupo    = p_grupo_produto
     and cditem_subgrupo = p_subgrupo_produto
     and cditem_item     = p_item_produto
     and lote_acomp      = p_lote
     and deposito        = p_deposito;
     
   if SQL%notfound  -- Quando nao atualizar a tabela de estoques
   then
      raise_application_error (-20000, 'ATENCAO! Nao atualizou a qtde sugerida da tabela de estoques');
   end if; 

   -- Atualizando a qtde sugerida no item do pedido de venda
   update pedi_110
   set qtde_sugerida     = qtde_sugerida + p_qtde_sugerida,
       nr_sugestao       = p_nr_sugestao
   where pedido_venda    = p_pedido_venda
     and seq_item_pedido = p_sequencia_pedido;
     
   if SQL%notfound  -- Quando nao atualizar o item do pedido
   then
      raise_application_error (-20000, 'ATENCAO! Nao atualizou a qtde sugerida da tabela de itens de pedido');
   end if;
   
   -- Atualizando o nr da sugestao na capa do pedido
   update pedi_100
   set nr_sugestao = p_nr_sugestao
   where pedido_venda    = p_pedido_venda;
   
   if SQL%notfound  -- Quando nao atualizar a capa do pedido
   then
      raise_application_error (-20000, 'ATENCAO! Nao atualizou o nr sugestao na capa do pedido de venda');
   end if;
end inter_pr_sugestao_fatur;

 

/

exec inter_pr_recompile;

