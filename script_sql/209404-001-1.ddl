-- Create table 
create table PEDI_021_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(20) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(20) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  cod_repres_OLD    NUMBER(5) default 0 ,  
  cod_repres_NEW    NUMBER(5) default 0 , 
  tipo_comissao_OLD NUMBER(1) default 0 ,  
  tipo_comissao_NEW NUMBER(1) default 0 , 
  perc_comissao_OLD NUMBER(5,2) default 0.00, 
  perc_comissao_NEW NUMBER(5,2) default 0.00 
) ;
