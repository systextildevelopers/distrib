insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('basi_f056', 'Cadastro de Unidade de Medida Tributária', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'basi_f056', 'basi_menu', 1, 1, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Registro de Unidad de Medid Tributária'
 where hdoc_036.codigo_programa = 'basi_f056'
   and hdoc_036.locale          = 'es_ES';
commit;
/
