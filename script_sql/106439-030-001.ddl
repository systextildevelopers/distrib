alter table BASI_189
add (DIVISAO_PRODUCAO NUMBER(4) default 0);

comment on column BASI_189.DIVISAO_PRODUCAO
  is 'Informação da divisão de produção do centro de custo.';

alter table BASI_275
add (DIVISAO_PRODUCAO NUMBER(4) default 0);

comment on column BASI_275.DIVISAO_PRODUCAO
  is 'Informação da divisão de produção do centro de custo.';

alter table TMRP_660
add (DIVISAO_PRODUCAO NUMBER(4) default 0);

comment on column TMRP_660.DIVISAO_PRODUCAO
  is 'Informação da divisão de produção do centro de custo.';

/

exec inter_pr_recompile;
