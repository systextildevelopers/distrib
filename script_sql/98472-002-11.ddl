alter table fatu_060 
add (vICMSOp51   number(15,2));

alter table fatu_060 
add (pDIF51      number(9,2));

alter table fatu_060 
add (vICMSDif51  number(15,2));

comment on column fatu_060.vICMSOp51 
 is 'CST 51 - Valor como se nao tivesse diferimento';
comment on column fatu_060.pDIF51
is 'CST 51 - Percentual diferimento. No caso de diferimento total, informar 100 no percentual de diferimento';

comment on column fatu_060.vICMSDif51
is 'Valor do Icms Diferido';

alter table fatu_060 
modify ( vICMSOp51   default 0.00,
         pDIF51      default 0.00,
         vICMSDif51  default 0.00);


/

exec inter_pr_recompile;
