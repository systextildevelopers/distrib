create table pedi_016
 (id number(9) not null,
     cnpj9  number(9) default 0,
     cnpj4  number(4) default 0,
     cnpj2  number(2) default 0,
     atributo varchar2(100) default ' ',
     conteudo varchar2(400) default ' ');
     
comment on column pedi_016.cnpj9 is 'cnpj do cliente';     
comment on column pedi_016.cnpj4 is 'cnpj do cliente';
comment on column pedi_016.cnpj2 is 'cnpj do cliente';
comment on column pedi_016.atributo is 'Atributo do cliente';
comment on column pedi_016.atributo is 'Conteu do Atributo do cliente';

alter table PEDI_016
  add constraint PK_PEDI_016 primary key (ID);
  
alter table PEDI_016
  add constraint UNIQ_PEDI_016 unique (cnpj9, cnpj4,cnpj2,atributo);
  
CREATE SEQUENCE ID_PEDI_016 START WITH 1 INCREMENT BY 1;  

exec inter_pr_recompile;
/
