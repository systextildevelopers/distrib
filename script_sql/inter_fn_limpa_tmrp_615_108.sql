
  CREATE OR REPLACE FUNCTION "INTER_FN_LIMPA_TMRP_615_108" 
return number
is
   v_status_conexao     number;

begin
   for reg_tmrp_615_108 in (select tmrp_615_108.instancia, tmrp_615_108.sid_usuario
                        from tmrp_615_108
                        group by tmrp_615_108.instancia, tmrp_615_108.sid_usuario)
   loop
      v_status_conexao := 1;

      begin
         select decode(STATUS, 'KILLED', 0, 1)
         into v_status_conexao
         from sys.gv_$session
         where audsid > 0
           and osuser <> 'oracle'
           and sys.gv_$session.inst_id = reg_tmrp_615_108.instancia
           and sys.gv_$session.sid     = reg_tmrp_615_108.sid_usuario
           and rownum < 2;
      exception when others then
         v_status_conexao := 0;
      end;

      if v_status_conexao = 0
      then
         begin
            delete tmrp_615_108
            where tmrp_615_108.instancia   = reg_tmrp_615_108.instancia
              and tmrp_615_108.sid_usuario = reg_tmrp_615_108.sid_usuario;
         end;
      end if;
   end loop;

   return(v_status_conexao);

end inter_fn_limpa_tmrp_615_108;

 

/

exec inter_pr_recompile;

