
  CREATE OR REPLACE TRIGGER "INTER_TR_CONT_550_LOG" 
after insert or delete or update
on CONT_550
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
                           
   v_nome_programa := inter_fn_nome_programa(ws_sid);  

 if inserting
 then
    begin

        insert into CONT_550_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           COD_PLANO_CTA_OLD,   /*8*/
           COD_PLANO_CTA_NEW,   /*9*/
           TIPO_CONTABIL_OLD,   /*10*/
           TIPO_CONTABIL_NEW,   /*11*/
           CODIGO_CONTABIL_OLD,   /*12*/
           CODIGO_CONTABIL_NEW,   /*13*/
           TRANSACAO_OLD,   /*14*/
           TRANSACAO_NEW,   /*15*/
           CONTA_CONTABIL_OLD,   /*16*/
           CONTA_CONTABIL_NEW    /*17*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.COD_PLANO_CTA, /*9*/
           0,/*10*/
           :new.TIPO_CONTABIL, /*11*/
           0,/*12*/
           :new.CODIGO_CONTABIL, /*13*/
           0,/*14*/
           :new.TRANSACAO, /*15*/
           0,/*16*/
           :new.CONTA_CONTABIL /*17*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into CONT_550_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_PLANO_CTA_OLD, /*8*/
           COD_PLANO_CTA_NEW, /*9*/
           TIPO_CONTABIL_OLD, /*10*/
           TIPO_CONTABIL_NEW, /*11*/
           CODIGO_CONTABIL_OLD, /*12*/
           CODIGO_CONTABIL_NEW, /*13*/
           TRANSACAO_OLD, /*14*/
           TRANSACAO_NEW, /*15*/
           CONTA_CONTABIL_OLD, /*16*/
           CONTA_CONTABIL_NEW  /*17*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.COD_PLANO_CTA,  /*8*/
           :new.COD_PLANO_CTA, /*9*/
           :old.TIPO_CONTABIL,  /*10*/
           :new.TIPO_CONTABIL, /*11*/
           :old.CODIGO_CONTABIL,  /*12*/
           :new.CODIGO_CONTABIL, /*13*/
           :old.TRANSACAO,  /*14*/
           :new.TRANSACAO, /*15*/
           :old.CONTA_CONTABIL,  /*16*/
           :new.CONTA_CONTABIL  /*17*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into CONT_550_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_PLANO_CTA_OLD, /*8*/
           COD_PLANO_CTA_NEW, /*9*/
           TIPO_CONTABIL_OLD, /*10*/
           TIPO_CONTABIL_NEW, /*11*/
           CODIGO_CONTABIL_OLD, /*12*/
           CODIGO_CONTABIL_NEW, /*13*/
           TRANSACAO_OLD, /*14*/
           TRANSACAO_NEW, /*15*/
           CONTA_CONTABIL_OLD, /*16*/
           CONTA_CONTABIL_NEW /*17*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.COD_PLANO_CTA, /*8*/
           0, /*9*/
           :old.TIPO_CONTABIL, /*10*/
           0, /*11*/
           :old.CODIGO_CONTABIL, /*12*/
           0, /*13*/
           :old.TRANSACAO, /*14*/
           0, /*15*/
           :old.CONTA_CONTABIL, /*16*/
           0 /*17*/
         );
    end;
 end if;
end inter_tr_CONT_550_log;

-- ALTER TRIGGER "INTER_TR_CONT_550_LOG" ENABLE
 

/

exec inter_pr_recompile;

