
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_330_2" 
before
update of forn4_entr, forn2_entr, nota_entr, serie_entr,
	seq_entr, nome_prog_050, data_cardex, transacao_cardex,
	usuario_cardex, valor_movto_cardex, valor_contabil_cardex, tabela_origem_cardex,
	seq_operacao, pedido_compra, seq_compra, executa_trigger,
	cod_caixa_rfid, periodo_producao, ordem_producao, ordem_confeccao,
	sequencia, estagio, nr_volume, pedido_venda,
	seq_item_pedido, nivel, grupo, subgrupo,
	item, nota_inclusao, serie_nota, data_inclusao,
	usuario_estq, usuario_exped, flag_controle, estoque_tag,
	lote, deposito, transacao_ent, data_entrada,
	pre_romaneio, emp_saida, seq_saida, flag_inventario,
	forn9_entr
on pcpc_330
for each row
begin

   -- Nao permite alteracoes se a caixa esta no WMS
   if trim(:old.cod_caixa_rfid) is not null and
      trim(:new.cod_caixa_rfid) is not null and
      trim(:old.cod_caixa_rfid) = trim(:new.cod_caixa_rfid)
   then Raise_application_error(-20000, 'Esta TAG esta no estoque do WMS nao e permitido altera-la.');
   end if;

end inter_tr_pcpc_330_2;

-- ALTER TRIGGER "INTER_TR_PCPC_330_2" ENABLE
 

/

exec inter_pr_recompile;

