alter table oper_275
drop column cod_natur_operacao;

alter table oper_275 add nfe_repositorio_xml varchar2(100);

alter table oper_275 add leitura_automatica number(1);

exec inter_pr_recompile;
