
  CREATE OR REPLACE TRIGGER "LOJA_TR_PEDI_010" 
BEFORE  UPDATE
of cep_cliente,endereco_cliente,numero_imovel,bairro,cod_cidade,
   codigo_empresa,nome_cliente,insc_est_cliente,telefone_cliente,fax_cliente

on pedi_010
for each row

begin

  if updating
  then
     :new.flag_exportacao_loja := 0;
  end if;
end LOJA_TR_PEDI_010;
-- ALTER TRIGGER "LOJA_TR_PEDI_010" ENABLE
 

/

exec inter_pr_recompile;

