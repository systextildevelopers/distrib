alter table fatu_052
add (DESC_IND_ADICIONAIS          VARCHAR2(100),
     IND_LOCAL_MSG_DANFE          VARCHAR2(1));
     
alter table fatu_052
modify (DESC_IND_ADICIONAIS       default ' ',
        IND_LOCAL_MSG_DANFE       default ' ');

comment on column fatu_052.desc_ind_adicionais is 'Indica se a informação ser adicionais ou complementares no DANFE';
comment on column fatu_052.ind_local_msg_danfe  is 'Indica para o processo de emissão do DANFE, se uma mensagem de dados adicionais será continuado no corpo do DANFE';

exec inter_pr_recompile;

/
