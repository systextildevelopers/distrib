
  CREATE OR REPLACE PROCEDURE "INTER_PR_LEAD_TIME" 
is
   v_sub_ler             mqop_050.subgru_estrutura%type  := '###';
   v_item_ler            mqop_050.item_estrutura%type    := '######';

   v_nivel020         	 pcpb_020.pano_sbg_nivel99%type  := '#';
   v_grupo020         	 pcpb_020.pano_sbg_grupo%type    := '#####';
   v_sub020           	 pcpb_020.pano_sbg_subgrupo%type := '###';
   v_item020          	 pcpb_020.pano_sbg_item%type     := '######';

   v_nr_registro         number;

   v_data_embarque       date;

   v_dias_disp_final_ob  number;
   v_dias_disp_final_ot  number;

   v_alternativa_020     number;
   v_roteiro_020         number;

begin
   for pb_010 in (select pcpb_010.ordem_producao, pcpb_010.ordem_tingimento
		  from pcpb_010, pcpb_015
                  where pcpb_010.ordem_producao   = pcpb_015.ordem_producao
                    and pcpb_010.tipo_ordem  not in (4)
                    and pcpb_010.cod_cancelamento = 0
                    and pcpb_015.data_termino    is null
                  group by pcpb_010.ordem_producao, pcpb_010.ordem_tingimento
                  order by pcpb_010.ordem_producao, pcpb_010.ordem_tingimento)
	 loop
      begin
         select pcpb_020.alternativa_item,  pcpb_020.roteiro_opcional,
                pcpb_020.pano_sbg_nivel99,  pcpb_020.pano_sbg_grupo,
                pcpb_020.pano_sbg_subgrupo, pcpb_020.pano_sbg_item
         into   v_alternativa_020,          v_roteiro_020,
                v_nivel020,                 v_grupo020,
                v_sub020,                   v_item020
         from pcpb_020
         where pcpb_020.ordem_producao = pb_010.ordem_producao
				   and rownum									 = 1;
         exception
            when others then
                 v_alternativa_020  := 0;
                 v_roteiro_020      := 0;
	  	 v_nivel020         := '#';
                 v_grupo020         := '#####';
		 v_sub020           := '###';
		 v_item020          := '######';
      end;

      -- sempre antes de chamar a procedure para atualizar estagios e operacoes
      -- a logica abaixo deve ser executada para definicao do produto.
      begin
         v_sub_ler  := v_sub020;
         v_item_ler := v_item020;
      end;

      begin
         select count(*)
         into v_nr_registro
         from mqop_050
         where mqop_050.nivel_estrutura  = v_nivel020
           and mqop_050.grupo_estrutura  = v_grupo020
           and mqop_050.subgru_estrutura = v_sub_ler
           and mqop_050.item_estrutura   = v_item_ler
           and mqop_050.numero_alternati = v_alternativa_020
           and mqop_050.numero_roteiro   = v_roteiro_020;
         if v_nr_registro = 0
         then
            v_item_ler := '000000';

            begin
               select count(*)
               into v_nr_registro
               from mqop_050
               where mqop_050.nivel_estrutura  = v_nivel020
                 and mqop_050.grupo_estrutura  = v_grupo020
                 and mqop_050.subgru_estrutura = v_sub_ler
                 and mqop_050.item_estrutura   = v_item_ler
                 and mqop_050.numero_alternati = v_alternativa_020
                 and mqop_050.numero_roteiro   = v_roteiro_020;
               if v_nr_registro = 0
               then
                  v_sub_ler  := '000';
                  v_item_ler := v_item020;

                  begin
                     select count(*)
                     into v_nr_registro
                     from mqop_050
                     where mqop_050.nivel_estrutura  = v_nivel020
                       and mqop_050.grupo_estrutura  = v_grupo020
                       and mqop_050.subgru_estrutura = v_sub_ler
                       and mqop_050.item_estrutura   = v_item_ler
                       and mqop_050.numero_alternati = v_alternativa_020
                       and mqop_050.numero_roteiro   = v_roteiro_020;
                     if v_nr_registro = 0
                     then
                        v_item_ler := '000000';
                     end if;
                  end;
               end if;
            end;
         end if;
      end;

      begin
         -- busca a data de embarque/entrega do pedido de venda destinado a ob
         -- para calcular com o centro de custo os dias para producao.
         v_data_embarque       := null;

	 begin
            select min(pedi_100.data_entr_venda)
            into   v_data_embarque
            from pedi_100, pcpb_030
            where pedi_100.pedido_venda   = pcpb_030.nr_pedido_ordem
              and pcpb_030.ordem_producao = pb_010.ordem_producao
              and pcpb_030.pedido_corte   = 3;
            exception
               when others then
                   v_data_embarque := null;
         end;

         -- chama procedure para calcular a prioridade das operacoes.
         inter_pr_calcula_seq_ob(pb_010.ordem_producao,
                                 pb_010.ordem_tingimento,
                                 v_data_embarque,
                                 0,
                                 'pcpb_030',
                                 v_nivel020,
                                 v_grupo020,
                                 v_sub_ler,
                                 v_item_ler,
                                 v_alternativa_020,
                                 v_roteiro_020);
      end;
   end loop; --loop da pcpb_010, pcpb_015

   commit;
end inter_pr_lead_time;

 

/

exec inter_pr_recompile;

