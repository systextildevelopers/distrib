
  CREATE OR REPLACE PROCEDURE "LOJA_PR_EXP_PRODUTO" (p_nivel    varchar2,
                                                 p_grupo    varchar2,
                                                 p_subgrupo varchar2,
                                                 p_item     varchar2,
                                                 p_cod_empr number,
                                                 p_des_erro OUT varchar2) is
begin
  loja_pr_exp_produto_inte(p_nivel,
                           p_grupo,
                           p_subgrupo,
                           p_item,
                           p_cod_empr,
                           p_des_erro);
end loja_pr_exp_produto;
 

/

exec inter_pr_recompile;

