
  CREATE OR REPLACE TRIGGER "INTER_TR_CPAG_027_LOG" 
after insert or delete or update
on CPAG_027
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);                           

 if inserting
 then
    begin

        insert into CPAG_027_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           banco_OLD,   /*8*/
           banco_NEW,   /*9*/
           conta_corrente_OLD,   /*10*/
           conta_corrente_NEW,   /*11*/
           data_saldo_OLD,   /*12*/
           data_saldo_NEW,   /*13*/
           valor_saldo_OLD,   /*14*/
           valor_saldo_NEW    /*15*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.banco, /*9*/
           0,/*10*/
           :new.conta_corrente, /*11*/
           null,/*12*/
           :new.data_saldo, /*13*/
           0,/*14*/
           :new.valor_saldo /*15*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into CPAG_027_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           banco_OLD, /*8*/
           banco_NEW, /*9*/
           conta_corrente_OLD, /*10*/
           conta_corrente_NEW, /*11*/
           data_saldo_OLD, /*12*/
           data_saldo_NEW, /*13*/
           valor_saldo_OLD, /*14*/
           valor_saldo_NEW  /*15*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.banco,  /*8*/
           :new.banco, /*9*/
           :old.conta_corrente,  /*10*/
           :new.conta_corrente, /*11*/
           :old.data_saldo,  /*12*/
           :new.data_saldo, /*13*/
           :old.valor_saldo,  /*14*/
           :new.valor_saldo  /*15*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into CPAG_027_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           banco_OLD, /*8*/
           banco_NEW, /*9*/
           conta_corrente_OLD, /*10*/
           conta_corrente_NEW, /*11*/
           data_saldo_OLD, /*12*/
           data_saldo_NEW, /*13*/
           valor_saldo_OLD, /*14*/
           valor_saldo_NEW /*15*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.banco, /*8*/
           0, /*9*/
           :old.conta_corrente, /*10*/
           0, /*11*/
           :old.data_saldo, /*12*/
           null, /*13*/
           :old.valor_saldo, /*14*/
           0 /*15*/
         );
    end;
 end if;
end inter_tr_CPAG_027_log;

-- ALTER TRIGGER "INTER_TR_CPAG_027_LOG" ENABLE
 

/

exec inter_pr_recompile;

