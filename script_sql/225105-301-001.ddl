create table empr_020 (
    param       varchar2(40)    not null,
    value       varchar2(4000)  not null
);

alter table empr_020 add constraint empr_020_pk primary key (param, value);

comment on table empr_020               is 'Tabela de parâmetros mútiplos valores';

comment on column empr_020.param        is 'Nome do parâmetro. Ex.: obrf.retornoIndustrializacao.CFOPSaida';
comment on column empr_020.value        is 'Valor do parâmetro. Ex.: 5.101';
