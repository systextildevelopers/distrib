insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('basi_f046', 'Cadastro de Comprimento de Debrum DDP', 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'basi_f046', 'nenhum' ,0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Cadastro de Comprimento de Debrum DDP'
where hdoc_036.codigo_programa = 'basi_f046'
  and hdoc_036.locale          = 'es_ES';
  
commit;
/

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('basi_f301', 'Sele��o de partes para a localiza��o DDP', 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'basi_f301', 'nenhum' ,0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Sele��o de partes para a localiza��o DDP'
where hdoc_036.codigo_programa = 'basi_f301'
  and hdoc_036.locale          = 'es_ES';
  
commit;
/

alter table basi_012 
add sequencia NUMBER(5);
/

create table BASI_113
(
  nivel_item          VARCHAR2(1) default '' not null,
  grupo_item          VARCHAR2(5) default '' not null,
  subgru_item         VARCHAR2(3) default '' not null,
  item_item           VARCHAR2(6) default '' not null,
  alternativa_produto NUMBER(2) default 0 not null,
  numero_roteiro      NUMBER(2) default 0,
  sequencia_estrutura NUMBER(3) default 0 not null,
  sequencia           NUMBER(4) default 0 not null,
  tipo_produto        VARCHAR2(20) default '',
  combinacao_item     VARCHAR2(6) default '000000' not null,
  nivel_comp          VARCHAR2(1) default '',
  grupo_comp          VARCHAR2(5) default '',
  subgru_comp         VARCHAR2(3) default '',
  item_comp           VARCHAR2(6) default '',
  alternativa_comp    NUMBER(2) default 0,
  consumo_auxiliar    NUMBER(8,3) default 0.00,
  consumo_componente  NUMBER(13,6) default 0.00,
  calcula_composicao  NUMBER(1) default 0,
  percentual_perdas   NUMBER(7,3) default 0,
  qtde_camadas        NUMBER(3) default 0,
  codigo_estagio      NUMBER(2) default 0,
  centro_custo        NUMBER(6) default 0,
  variacao_tamanho    NUMBER(1) default 0,
  tecido_principal    NUMBER(1) default 0
);

alter table BASI_113
  add constraint PK_BASI_113 primary key (NIVEL_ITEM, GRUPO_ITEM, SUBGRU_ITEM, ITEM_ITEM, ALTERNATIVA_PRODUTO, SEQUENCIA_ESTRUTURA,SEQUENCIA);
/

create table BASI_121
(
  nivel_item          VARCHAR2(1) default '' not null,
  grupo_item          VARCHAR2(5) default '' not null,
  subgru_item         VARCHAR2(3) default '' not null,
  item_item           VARCHAR2(6) default '' not null,
  alternativa_produto NUMBER(2) default 0 not null,
  subgru_comp         VARCHAR2(3) default '',
  item_comp           VARCHAR2(6) default '',
  utiliza_componente  NUMBER(1) default 0,
  metros_componente   NUMBER(8,3) default 0.00,
  consumo_componente  NUMBER(13,6) default 0.0,
  sequencia           NUMBER(4) default 0 not null,
  sequencia_estrutura NUMBER(3) default 0 not null,
  alternativa_comp    NUMBER(2) default 0,
  ordem_tamanho       NUMBER(3) default 0
);

alter table BASI_121
  add constraint PK_BASI_121 primary key (NIVEL_ITEM, GRUPO_ITEM, SUBGRU_ITEM, ITEM_ITEM, ALTERNATIVA_PRODUTO, SEQUENCIA_ESTRUTURA,SEQUENCIA);
/

create table BASI_119
(
  sequencia             NUMBER(4) default 0 not null,
  alternativa_produto   NUMBER(2) default 0 not null,
  nivel_item            VARCHAR2(1) default '' not null,
  grupo_item            VARCHAR2(5) default '' not null,
  subgru_item           VARCHAR2(3) default '' not null,
  item_item             VARCHAR2(6) default '' not null,
  sequencia_estrutura   NUMBER(3) default 0 not null,
  descricao_parte       VARCHAR2(30) default '' not null,
  parte_conjunto        NUMBER(1) default 0,
  qtde_parte_peca       NUMBER(2) default 0,
  tipo_corte_peca       NUMBER(1) default 0,
  tipo_enfesto_1        NUMBER(1) default 0,
  tipo_enfesto_2        NUMBER(1) default 0,
  inf_corte_sentido     NUMBER(1) default 0,
  inf_corte_casado      NUMBER(1) default 0,
  inf_corte_desenho     NUMBER(1) default 0,
  exp_optiplan          NUMBER(1) default 1,
  comprimento_debrum    NUMBER(8,2) default 0.00,
  largura_debrum        NUMBER(8,3) default 0.00,
  numero_balancin_laser VARCHAR2(10) default '',
  observacao_parte      VARCHAR2(2000) default ''
);

alter table BASI_119
  add constraint PK_BASI_119 primary key (DESCRICAO_PARTE,NIVEL_ITEM, GRUPO_ITEM, SUBGRU_ITEM, ITEM_ITEM, ALTERNATIVA_PRODUTO, SEQUENCIA_ESTRUTURA,SEQUENCIA);
/

create table BASI_066
(
  nivel_estrutura     VARCHAR2(1) default '' not null,
  grupo_estrutura     VARCHAR2(5) default '' not null,
  subgru_estrutura    VARCHAR2(3) default '' not null,
  item_estrutura      VARCHAR2(6) default '' not null,
  alternativa_item    NUMBER(2) default 0 not null,
  sequencia           NUMBER(4) default 0 not null,
  sequencia_estrutura NUMBER(3) default 0 not null,
  descricao_parte     VARCHAR2(30) default '' not null,
  comprimento         NUMBER(8,2) default 0.00,
  sequencia_tamanho   NUMBER(3) default 0
);

alter table BASI_066
  add constraint PK_BASI_066 primary key (NIVEL_ESTRUTURA, GRUPO_ESTRUTURA, SUBGRU_ESTRUTURA, ITEM_ESTRUTURA, ALTERNATIVA_ITEM, SEQUENCIA,SEQUENCIA_ESTRUTURA, DESCRICAO_PARTE);
/
