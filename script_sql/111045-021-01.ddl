create table obrf_516 (
  cod_empresa	     		number(3)     default 0,
  uf_empresa	      		varchar2(2)   default ' ',
  perc_interno_fci    		number(8,5)   default 0.0,
  perc_interestadual_fci    number(8,5)   default 0.0);

alter table obrf_516 
	add constraint pk_obrf_516 primary key (cod_empresa,uf_empresa);
	
create synonym systextilrpt.obrf_516 for obrf_516; 

comment on table obrf_516
	is 'Tabela Para Criacao de Arquivo do FCI';
	
comment on column obrf_516.cod_empresa
	is 'Codigo da Empresa';
	
comment on column obrf_516.uf_empresa
	is 'Estado da empresa';
	
comment on column obrf_516.perc_interno_fci
	is 'Percentual de Participação ICMS Origem';
	
comment on column obrf_516.perc_interestadual_fci
	is 'Percentual de Participação ICMS Destino';

exec inter_pr_recompile;
