
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_060_SPED" 
BEFORE INSERT or
UPDATE of num_nota_ecf_ipi, observacao, pedido_venda, valor_iss,
	formulario_num, formulario_ser, formulario_seq, formulario_ori,
	um_faturamento_um, um_faturamento_qtde, um_faturamento_valor_unit, flag_guia_remissao,
	requisicao, seq_requisicao, executa_trigger, valor_pis,
	valor_cofins, perc_iss, cvf_pis, cvf_cofins,
	basi_pis_cofins, perc_pis, perc_cofins, cvf_ipi_saida,
	nota_ajuste, serie_ajuste, cod_csosn, valor_frete,
	valor_desc, valor_outros, valor_seguro, base_ipi,
	cvf_ipi, valor_icms, peso_liquido, descricao_item,
	centro_custo, unidade_medida, num_ordem_serv, seq_nota_orig,
	cgc9_origem, codigo_contabil, perc_iva_2, rateio_desc_propaganda,
	base_icms_difer, valor_icms_diferido, empresa_nota_reemissao, seq_nota_reemissao,
	ch_it_nf_cd_empr, ch_it_nf_num_nfis, ch_it_nf_ser_nfis, seq_item_nfisc,
	seq_item_pedido, nivel_estrutura, grupo_estrutura, subgru_estrutura,
	item_estrutura, lote_acomp, deposito, transacao,
	data_emissao, qtde_item_fatur, valor_unitario, valor_faturado,
	valor_contabil, desconto_item, natopeno_nat_oper, natopeno_est_oper,
	perc_ipi, valor_ipi, base_icms, perc_icms,
	cvf_icms, valor_icms_difer, cod_cancelamento, nr_solicitacao,
	atualizou_estq, classific_fiscal, classif_contabil, deposito_transf,
	cen_custo_transf, rateio_despesa, procedencia, seq_item_ordem,
	num_nota_orig, serie_nota_orig, motivo_devolucao, qtde_fatu_empe,
	cgc4_origem, cgc2_origem, flag_devolucao, obs_livro1,
	obs_livro2, perc_iva_1, valor_iva_1, valor_iva_2,
	atualizou_plano, sequencia_mao_obra, perc_mao_obra, valor_negociacao,
	perc_icms_diferido, usuario_cardex, nome_programa, data_canc_nfisc,
	transacao_canc_nfisc, numero_nota_reemissao, serie_nota_reemissao, nro_rolo_peca,
	rateio_despesas_ipi, rateio_descontos_ipi, processo_contabil_estoque
on fatu_060
for each row

declare
   v_simples   fatu_503.ind_empresa_simples%type;

begin
  begin
     --le parametro para saber se a empresa se enquadra no simples
     select fatu_503.ind_empresa_simples
     into v_simples
     from fatu_503
     where fatu_503.codigo_empresa  = :new.ch_it_nf_cd_empr;
  exception
     when no_data_found then
          v_simples := '3';
  end;

  if v_simples = '1'
  then
     :new.cvf_icms := 90;
  end if;

  if :new.cvf_ipi_saida = 50
  then
     :new.cvf_ipi := 1;
  end if;
  if :new.cvf_ipi_saida = 52
  then
     :new.cvf_ipi := 2;
  end if;
  if :new.cvf_ipi_saida = 51 or :new.cvf_ipi_saida = 53 or :new.cvf_ipi_saida = 54
  or :new.cvf_ipi_saida = 55 or :new.cvf_ipi_saida = 99
  then
     :new.cvf_ipi := 3;
  end if;

  if :new.cvf_ipi_saida = 0
  then
     if :new.cvf_ipi = 1
     then
        :new.cvf_ipi_saida := 50;
     end if;
     if :new.cvf_ipi = 2
     then
        :new.cvf_ipi_saida := 52;
     end if;
     if :new.cvf_ipi = 3
     then
        :new.cvf_ipi_saida := 99;
     end if;
  end if;

end inter_tr_fatu_060_sped;

-- ALTER TRIGGER "INTER_TR_FATU_060_SPED" ENABLE
 

/

exec inter_pr_recompile;

