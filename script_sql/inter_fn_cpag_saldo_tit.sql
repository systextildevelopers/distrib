
  CREATE OR REPLACE FUNCTION "INTER_FN_CPAG_SALDO_TIT" (pnr_duplicata number, pparcela     varchar2,
                                                    pcgc9         number, pcgc4        number,
                                                    pcgc2         number, ptipo_titulo number)
return number
is
   v_saldo_duplicata number;
   
begin

   select nvl(sum(cpag_010.valor_parcela - cpag_010.valor_irrf - cpag_010.valor_iss-
              cpag_010.valor_inss - nvl(pagtos.valor_pago,0)),0) VALOR_LIQUIDO
   into v_saldo_duplicata
   from cpag_010, (
   
      select cpag_015.dupl_for_nrduppag, cpag_015.dupl_for_no_parc,
             cpag_015.dupl_for_for_cli9, cpag_015.dupl_for_for_cli4,
             cpag_015.dupl_for_for_cli2, cpag_015.dupl_for_tipo_tit,
             sum(decode(cont_010.sinal_titulo, 1,
                       (cpag_015.valor_pago + cpag_015.valor_descontos + cpag_015.valor_abatimento - 
                        cpag_015.valor_juros - cpag_015.vlr_var_cambial ),
                       (cpag_015.valor_pago + cpag_015.valor_descontos + cpag_015.valor_abatimento - 
                        cpag_015.valor_juros - cpag_015.vlr_var_cambial) * (-1.0))) VALOR_PAGO
      from cpag_015, cont_010
      where cpag_015.dupl_for_nrduppag = pnr_duplicata
      and   cpag_015.dupl_for_no_parc  = pparcela
      and   cpag_015.dupl_for_for_cli9 = pcgc9
      and   cpag_015.dupl_for_for_cli4 = pcgc4
      and   cpag_015.dupl_for_for_cli2 = pcgc2
      and   cpag_015.dupl_for_tipo_tit = ptipo_titulo
      and   cpag_015.codigo_historico  = cont_010.codigo_historico
      and   cont_010.sinal_titulo     in (1,2)
      group by cpag_015.dupl_for_nrduppag, cpag_015.dupl_for_no_parc,
               cpag_015.dupl_for_for_cli9, cpag_015.dupl_for_for_cli4,
               cpag_015.dupl_for_for_cli2, cpag_015.dupl_for_tipo_tit) PAGTOS   

   where cpag_010.nr_duplicata = pnr_duplicata
   and   cpag_010.parcela      = pparcela
   and   cpag_010.cgc_9        = pcgc9
   and   cpag_010.cgc_4        = pcgc4
   and   cpag_010.cgc_2        = pcgc2
   and   cpag_010.tipo_titulo  = ptipo_titulo

   and   cpag_010.nr_duplicata = pagtos.dupl_for_nrduppag (+)
   and   cpag_010.parcela      = pagtos.dupl_for_no_parc  (+)
   and   cpag_010.cgc_9        = pagtos.dupl_for_for_cli9 (+)
   and   cpag_010.cgc_4        = pagtos.dupl_for_for_cli4 (+)
   and   cpag_010.cgc_2        = pagtos.dupl_for_for_cli2 (+)
   and   cpag_010.tipo_titulo  = pagtos.dupl_for_tipo_tit (+);
   
   return (v_saldo_duplicata);
end inter_fn_cpag_saldo_tit;

 

/

exec inter_pr_recompile;

