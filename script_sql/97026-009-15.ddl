alter table sped_c100 add(
val_fcp_uf_dest_nf number(17,2),
val_icms_uf_dest_nf   number(17,2),
val_icms_uf_remet_nf  number(17,2));


comment on column sped_c100.val_fcp_uf_dest_nf is 'Valor do ICMS relativo ao Fundo de Combate � Pobreza (FCP) da UF de destino.';
comment on column sped_c100.val_icms_uf_dest_nf is 'Valor do ICMS de partilha para a UF do destinat�rio';
comment on column sped_c100.val_icms_uf_remet_nf is 'Valor do ICMS de partilha para a UF do remetente.';

exec inter_pr_recompile;
