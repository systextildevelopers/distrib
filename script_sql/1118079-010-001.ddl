create table PCPC_042 (
	ordem_producao          NUMBER(9) default 0,
	sequencia_enfesto       NUMBER(3) default 0,
	seq_nuance              NUMBER(3) default 0,
	descricao_nuance        VARCHAR2(15) default ''
);


comment on table PCPC_042 is 'Cadastro de Nuance';
comment on column PCPC_042.ordem_producao is 'Ordem de Produ��o.';
comment on column PCPC_042.sequencia_enfesto is 'Sequencia do enfesto da nuance.';
comment on column PCPC_042.seq_nuance is 'Sequencia da nuance.';
comment on column PCPC_042.descricao_nuance is 'Sequencia da nuance.';

alter table PCPC_042 add constraint PK_PCPC_042 primary key (ordem_producao, sequencia_enfesto, seq_nuance);

alter table PCPC_042 add constraint REF_PCPC_042_PCPC_020 foreign key (ordem_producao) references pcpc_020 (ordem_producao);

exec inter_pr_recompile;
