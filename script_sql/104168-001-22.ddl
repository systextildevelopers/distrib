CREATE TABLE SUPP_010(
       NUMERO_NOTA             NUMBER(9) NOT NULL,
       SERIE                   NUMBER(2) NOT NULL,
       COD_EMPRESA             NUMBER(3) NOT NULL,
       CGC9                    NUMBER(9) NOT NULL,
       CGC4                    NUMBER(4) NOT NULL,
       CGC2                    NUMBER(2) NOT NULL,
       DATA_CRIACAO            DATE      NOT NULL,
       DATA_RETORNO            DATE,
       FLAG_RETORNO            VARCHAR2(1) DEFAULT 'N',
       VALOR_NOTA              NUMBER(14,2) DEFAULT 0.0
);

/
exec inter_pr_recompile;

