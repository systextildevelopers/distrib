
  CREATE OR REPLACE FUNCTION "TMRP_F002_CALC_TOTAIS" 
        (f_nome_programa    varchar2,
         f_codigo_usuario   number,
         f_nr_solicitacao   number,
         f_codigo_empresa   number,
         f_usuario          varchar2,
         f_tipo_registro    number,
         f_tipo_selecionado number,
         f_selecionado      number,
         f_variavel         number)
RETURN number
IS
   resultado  number;

BEGIN
   /*
      CALCULAR TOTAIS

      f_tipo_registro:
      O tipo de registro que tem que ser somado

      f_tipo_selecionado:
      1
         f_selecionado:
         0 = Nao selecionados
         1 = Selecionados
         2 = Selecionados
         3 = Selecionados
         4 = Selecionados
         9 = Nao selecionados e Selecionados
      2
         f_selecionado:
         0 = Nao selecionados
         1 = Selecionados
         9 = Nao selecionados e Selecionados

      f_variavel:
         1 = Soma a variavel (tmrp_615.qtde_maxima)
         2 = Soma a variavel (tmrp_615.qtde_metros)
         3 = Soma a variavel (tmrp_615.unidades_areceber_programada)
         4 = Soma a variavel (tmrp_615.qtde_programacao_kg)
         5 = Soma a variavel (tmrp_615.qtde_unidades)
         6 = Soma a variavel (tmrp_615.qtde_programacao)
   */
   begin
      select decode(f_variavel,1,nvl(sum(tmrp_615.qtde_maxima),0),
                               2,nvl(sum(tmrp_615.qtde_metros),0),
                               3,nvl(sum(decode(tmrp_615.qtde_metros,0,tmrp_615.unidades_areceber_programada * tmrp_615.cons_unid_med_generica,0)),0),
                               4,nvl(sum(tmrp_615.qtde_programacao_kg),0),
                               5,nvl(sum(tmrp_615.qtde_unidades),0),
                               6,nvl(sum(tmrp_615.qtde_programacao),0),
                               7,nvl(sum(tmrp_615.unidades_areceber_programada),0),
                               0)
      into resultado
      from tmrp_615
      where tmrp_615.nome_programa    = f_nome_programa
        and tmrp_615.codigo_usuario   = f_codigo_usuario
        and tmrp_615.nr_solicitacao   = f_nr_solicitacao
        and tmrp_615.codigo_empresa   = f_codigo_empresa
        and tmrp_615.usuario          = f_usuario
        and tmrp_615.situacao         = 'X'   /* NAO CALCULADO */
        and tmrp_615.tipo_registro    = f_tipo_registro
        and ((f_tipo_selecionado      = 2
        and   tmrp_615.selecionado2   in (decode(f_selecionado,0,f_selecionado,1,f_selecionado,0),
                                          decode(f_selecionado,0,f_selecionado,1,f_selecionado,1)))
         or  (f_tipo_selecionado      = 1
        and   tmrp_615.selecionado1   in (decode(f_selecionado,0,f_selecionado,1,f_selecionado,2,f_selecionado,3,f_selecionado,4,f_selecionado,1),
                                          decode(f_selecionado,0,f_selecionado,1,f_selecionado,2,f_selecionado,3,f_selecionado,4,f_selecionado,2),
                                          decode(f_selecionado,0,f_selecionado,1,f_selecionado,2,f_selecionado,3,f_selecionado,4,f_selecionado,3),
                                          decode(f_selecionado,0,f_selecionado,1,f_selecionado,2,f_selecionado,3,f_selecionado,4,f_selecionado,4)))
         or  (f_tipo_selecionado      = 9));

   end;

   return(resultado);

END tmrp_f002_calc_totais;

 

/

exec inter_pr_recompile;

