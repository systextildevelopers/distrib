
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_050_SPED_CTB" 
   before insert or update of cgc_9, cgc_4, cgc_2, natop_nf_nat_oper, natop_nf_est_oper
   on fatu_050
   for each row

declare
   v_cliente_fornec  cont_600.cliente_fornecedor_part%type;
   v_cnpj_9          cont_600.cnpj9_participante%type;
   v_cnpj_4          cont_600.cnpj4_participante%type;
   v_cnpj_2          cont_600.cnpj2_participante%type;
   v_trans           estq_005.codigo_transacao%type;
   v_natureza        fatu_050.natop_nf_nat_oper%type;
   v_estado          fatu_050.natop_nf_est_oper%type;
   v_tipo_transacao  estq_005.tipo_transacao%type;
   v_sid             cont_601.sid%type;
   v_instancia       cont_601.instancia%type;

begin

   if inserting
   then
      v_cnpj_9   := :new.cgc_9;
      v_cnpj_4   := :new.cgc_4;
      v_cnpj_2   := :new.cgc_2;
      v_natureza := :new.natop_nf_nat_oper;
      v_estado   := :new.natop_nf_est_oper;
   else
      v_cnpj_9   := :old.cgc_9;
      v_cnpj_4   := :old.cgc_4;
      v_cnpj_2   := :old.cgc_2;
      v_natureza := :old.natop_nf_nat_oper;
      v_estado   := :old.natop_nf_est_oper;
   end if;

   begin
      select codigo_transacao
      into   v_trans
      from pedi_080
      where natur_operacao = v_natureza
      and   estado_natoper = v_estado;

   exception
      when others then
         v_trans := 0;
   end;

   begin
      select tipo_transacao
      into   v_tipo_transacao
      from   estq_005
      where  codigo_transacao = v_trans;

   exception
      when others then
         v_tipo_transacao := 'N';
   end;

   if v_tipo_transacao <> 'D'
   then
      v_cliente_fornec := 1; -- cliente
   else
      v_cliente_fornec := 2; -- fornecedor
   end if;


   select sid,   inst_id
   into   v_sid, v_instancia
   from   v_lista_sessao_banco;

   insert into cont_601
      (sid,
       instancia,
       data_insercao,
       tabela_origem,
       cnpj_9,
       cnpj_4,
       cnpj_2,
       cliente_fornec)
   values
      (v_sid,
       v_instancia,
       sysdate,
       'FATU_050',
       v_cnpj_9,
       v_cnpj_4,
       v_cnpj_2,
       v_cliente_fornec);

   exception
      when others then
         null;
end inter_tr_fatu_050_sped_ctb;
-- ALTER TRIGGER "INTER_TR_FATU_050_SPED_CTB" ENABLE
 

/

exec inter_pr_recompile;

