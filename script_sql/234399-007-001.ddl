ALTER TABLE empr_090
DROP CONSTRAINT empr_090;

ALTER TABLE empr_090
MODIFY cnpj9 NUMBER(9) DEFAULT 0 NOT NULL;

ALTER TABLE empr_090
ADD CONSTRAINT pk_empr_090 PRIMARY KEY
(empresa, tipo_titulo, cnpj9);

EXEC inter_pr_recompile;
/
