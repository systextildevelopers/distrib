
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_650_ID" 
before insert on tmrp_650
  for each row

declare
   v_nr_registro number;

begin

   if (:new.numero_id is null or :new.numero_id <= 0)
   then
      select seq_tmrp_650.nextval into v_nr_registro from dual;
      :new.numero_id := v_nr_registro;
   end if;

end inter_tr_tmrp_650_id;
-- ALTER TRIGGER "INTER_TR_TMRP_650_ID" ENABLE
 

/

exec inter_pr_recompile;

