INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'obrf_f794',     1,
	1,			     'Parâmetros de Cadastro Nacional de Produtos');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'obrf_f794',   'obrf_menu', 
	1,             1, 
	'S',           'S', 
	'S',           'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Parâmetros de Cadastro Nacional de Produtos'
 WHERE hdoc_036.codigo_programa = 'obrf_f794'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;
