alter table basi_350
add (perc_cor number(5,2),
     faixa_etaria_custos number(3),
     qtde_marcacoes number(4,1),
     perc_faixa number(5,2),
     data_exe_processo date);

alter table basi_350
modify (faixa_etaria_custos default 0,
        perc_cor            default 0.0,
        qtde_marcacoes      default 0.0,
        perc_faixa          default 0.0,
        data_exe_processo   default sysdate);

/

declare
cursor basi_350_c is
   select f.rowid from basi_350 f where faixa_etaria_custos is null;
   
contaRegistro number;

begin
   contaRegistro := 0;
   
   for reg_basi_350_c in basi_350_c
   loop
      update basi_350
      set faixa_etaria_custos   = 0,
          perc_cor              = 0,
          perc_faixa            = 0,
          qtde_marcacoes        = 0
      where faixa_etaria_custos is null
        and basi_350.rowid = reg_basi_350_c.rowid;
      
      contaRegistro := contaRegistro + 1;
      
      if contaRegistro = 1000
      then
         contaRegistro := 0;
         commit;
      end if;
   end loop; 

   commit;
end;
/

alter table basi_350
  drop constraint PK_BASI_350;

drop index PK_BASI_350;

alter table basi_350
add  constraint PK_BASI_350
PRIMARY KEY (codigo_empresa, mes, ano, nivel_estrutura, grupo_estrutura, subgru_estrutura, item_estrutura,faixa_etaria_custos);

execute inter_pr_recompile;     
