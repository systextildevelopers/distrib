insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f746', 'Guias de ICMS recolhido ou a recolher',0,1);


insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'obrf_f746', 'menu_ad91' ,0, 99, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Guias de ICMS recolhido ou a recolher'
where hdoc_036.codigo_programa = 'obrf_f746'
  and hdoc_036.locale          = 'es_ES';

commit;
