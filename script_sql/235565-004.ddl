---------------------------------------------------------------------------------------------
-- Incusão de Campo para indicar qual será a url usada no processamento das transações SPH --
---------------------------------------------------------------------------------------------
ALTER TABLE FINA_203
ADD ATIVO_SANDBOX VARCHAR2(5);
ALTER TABLE FINA_203
ADD ATIVO_HOMOL   VARCHAR2(5);
ALTER TABLE FINA_203
ADD ATIVO_PRODUCAO  VARCHAR2(5);
ALTER TABLE FINA_203_bkp
ADD ATIVO_SANDBOX VARCHAR2(5);
ALTER TABLE FINA_203_bkp
ADD ATIVO_HOMOL   VARCHAR2(5);
ALTER TABLE FINA_203_bkp
ADD ATIVO_PRODUCAO  VARCHAR2(5);
/
