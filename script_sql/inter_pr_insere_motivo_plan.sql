
  CREATE OR REPLACE PROCEDURE "INTER_PR_INSERE_MOTIVO_PLAN" (
 p_ordem_planejamento    number,         p_pedido           number,
 p_nivel                 varchar2,       p_grupo            varchar2,
 p_subgrupo              varchar2,       p_item             varchar2,
 p_nivel_conj            varchar2,       p_grupo_conj       varchar2,
 p_situacao              number,         p_tipo_alteracao   number,
 p_msg_erro_princ        varchar2,       p_msg_erro         varchar2)
IS
v_numero_id_635  NUMBER;
v_nr_registro    number;

BEGIN

  begin
     select tmrp_635.numero_id
     into v_numero_id_635
     from tmrp_635
     where tmrp_635.ordem_planejamento = p_ordem_planejamento
       and tmrp_635.pedido_reserva     = p_pedido
       and tmrp_635.situacao           = p_situacao
       and tmrp_635.tipo_alteracao     = p_tipo_alteracao
       and tmrp_635.data_resolvido     is null
       and tmrp_635.nivel_produto      = p_nivel
       and tmrp_635.grupo_produto      = p_grupo
       and tmrp_635.subgrupo_produto   = p_subgrupo
       and tmrp_635.item_produto       = p_item
       and rownum                      = 1;
  exception when no_data_found then
     begin
        insert into tmrp_635 (
           ordem_planejamento,         pedido_reserva,
           nivel_produto,              grupo_produto,
           subgrupo_produto,           item_produto,
           situacao,                   tipo_alteracao,
           data_cadastro,              motivo,
           nivel_conjunto,             grupo_conjunto
        ) values (
           p_ordem_planejamento,       p_pedido,
           p_nivel,                    p_grupo,
           p_subgrupo,                 p_item,
           p_situacao,                 p_tipo_alteracao,
           sysdate,                    p_msg_erro_princ,
           p_nivel_conj,               p_grupo_conj
        );
     end;
  end;

  begin
     select tmrp_635.numero_id
     into v_numero_id_635
     from tmrp_635
     where tmrp_635.ordem_planejamento = p_ordem_planejamento
       and tmrp_635.pedido_reserva     = p_pedido
       and tmrp_635.situacao           = p_situacao
       and tmrp_635.tipo_alteracao     = p_tipo_alteracao
       and tmrp_635.nivel_produto      = p_nivel
       and tmrp_635.grupo_produto      = p_grupo
       and tmrp_635.subgrupo_produto   = p_subgrupo
       and tmrp_635.item_produto       = p_item
       and tmrp_635.data_resolvido     is null
       and rownum                      = 1;
  end;

  begin
     insert into tmrp_636 (
        numero_id_635,                motivo
     ) values (
        v_numero_id_635,              p_msg_erro
     );
  end;

end inter_pr_insere_motivo_plan;

 

/

exec inter_pr_recompile;

