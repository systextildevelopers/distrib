create table sped_0210
(periodo_sped  number(6) default 0);

drop table sped_0210;


CREATE TABLE SPED_0210 (
  COD_EMPRESA     NUMBER(3)     DEFAULT 0,
  COD_NIVEL               VARCHAR2(1)   DEFAULT ' ' NOT NULL,
  COD_GRUPO               VARCHAR2(5)   DEFAULT ' ' NOT NULL,
  COD_SUBGRUPO            VARCHAR2(3)   DEFAULT ' ' NOT NULL,
  COD_ITEM                VARCHAR2(6)   DEFAULT ' ' NOT NULL,
  CONSUMO         NUMBER (13,3) DEFAULT 0,
  PERC_PERDA      NUMBER (7,3)  DEFAULT 0
);

ALTER TABLE SPED_0210 ADD CONSTRAINT PK_SPED_0210 PRIMARY KEY(COD_EMPRESA, COD_NIVEL, COD_GRUPO, COD_SUBGRUPO, COD_ITEM);

COMMENT ON COLUMN SPED_0210.COD_EMPRESA       IS 'Codigo da empresa que gera o arquivo';
COMMENT ON COLUMN SPED_0210.CONSUMO           IS 'Qtde consumo da estrutura';
COMMENT ON COLUMN SPED_0210.PERC_PERDA        IS 'Percentual de perda';

/
exec inter_pr_recompile;
