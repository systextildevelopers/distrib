create or replace trigger inter_tr_estq_038_log 
after insert or delete or update 
on ESTQ_038 
for each row 
declare 
   ws_usuario_rede           varchar2(250) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    -- begin 
    --    select hdoc_090.programa 
    --    into v_nome_programa 
    --    from hdoc_090 
    --    where hdoc_090.sid = ws_sid 
    --      and rownum       = 1 
    --      and hdoc_090.programa not like '%menu%' 
    --      and hdoc_090.programa not like '%_m%'; 
    --    exception 
    --      when no_data_found then            v_nome_programa := 'SQL'; 
    -- end; 
 
    v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 
 if inserting 
 then 
    begin 
 
        insert into ESTQ_038_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           codigo_empresa_OLD,   /*8*/ 
           codigo_empresa_NEW,   /*9*/ 
           mes_OLD,   /*10*/ 
           mes_NEW,   /*11*/ 
           ano_OLD,   /*12*/ 
           ano_NEW,   /*13*/ 
           nivel_produto_OLD,   /*14*/ 
           nivel_produto_NEW,   /*15*/ 
           grupo_produto_OLD,   /*16*/ 
           grupo_produto_NEW,   /*17*/ 
           subgru_produto_OLD,   /*18*/ 
           subgru_produto_NEW,   /*19*/ 
           item_produto_OLD,   /*20*/ 
           item_produto_NEW,   /*21*/ 
           codigo_estagio_OLD,   /*22*/ 
           codigo_estagio_NEW,   /*23*/ 
           ordem_producao_OLD,   /*24*/ 
           ordem_producao_NEW,   /*25*/ 
           qtde_em_elaboracao_OLD,   /*26*/ 
           qtde_em_elaboracao_NEW,   /*27*/ 
           valor_total_mp_OLD,   /*28*/ 
           valor_total_mp_NEW,   /*29*/ 
           valor_total_mo_OLD,   /*30*/ 
           valor_total_mo_NEW,   /*31*/ 
           valor_total_cp_OLD,   /*32*/ 
           valor_total_cp_NEW,   /*33*/ 
           valor_total_cd_OLD,   /*34*/ 
           valor_total_cd_NEW,   /*35*/ 
           registro_importado_OLD,   /*36*/ 
           registro_importado_NEW,   /*37*/ 
           tipo_valorizacao_OLD,   /*38*/ 
           tipo_valorizacao_NEW,   /*39*/ 
           cod_processo_OLD,   /*40*/ 
           cod_processo_NEW    /*41*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.codigo_empresa, /*9*/   
           0,/*10*/
           :new.mes, /*11*/   
           0,/*12*/
           :new.ano, /*13*/   
           '',/*14*/
           :new.nivel_produto, /*15*/   
           '',/*16*/
           :new.grupo_produto, /*17*/   
           '',/*18*/
           :new.subgru_produto, /*19*/   
           '',/*20*/
           :new.item_produto, /*21*/   
           0,/*22*/
           :new.codigo_estagio, /*23*/   
           0,/*24*/
           :new.ordem_producao, /*25*/   
           0,/*26*/
           :new.qtde_em_elaboracao, /*27*/   
           0,/*28*/
           :new.valor_total_mp, /*29*/   
           0,/*30*/
           :new.valor_total_mo, /*31*/   
           0,/*32*/
           :new.valor_total_cp, /*33*/   
           0,/*34*/
           :new.valor_total_cd, /*35*/   
           0,/*36*/
           :new.registro_importado, /*37*/   
           0,/*38*/
           :new.tipo_valorizacao, /*39*/   
           0,/*40*/
           :new.cod_processo /*41*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into ESTQ_038_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           codigo_empresa_OLD, /*8*/   
           codigo_empresa_NEW, /*9*/   
           mes_OLD, /*10*/   
           mes_NEW, /*11*/   
           ano_OLD, /*12*/   
           ano_NEW, /*13*/   
           nivel_produto_OLD, /*14*/   
           nivel_produto_NEW, /*15*/   
           grupo_produto_OLD, /*16*/   
           grupo_produto_NEW, /*17*/   
           subgru_produto_OLD, /*18*/   
           subgru_produto_NEW, /*19*/   
           item_produto_OLD, /*20*/   
           item_produto_NEW, /*21*/   
           codigo_estagio_OLD, /*22*/   
           codigo_estagio_NEW, /*23*/   
           ordem_producao_OLD, /*24*/   
           ordem_producao_NEW, /*25*/   
           qtde_em_elaboracao_OLD, /*26*/   
           qtde_em_elaboracao_NEW, /*27*/   
           valor_total_mp_OLD, /*28*/   
           valor_total_mp_NEW, /*29*/   
           valor_total_mo_OLD, /*30*/   
           valor_total_mo_NEW, /*31*/   
           valor_total_cp_OLD, /*32*/   
           valor_total_cp_NEW, /*33*/   
           valor_total_cd_OLD, /*34*/   
           valor_total_cd_NEW, /*35*/   
           registro_importado_OLD, /*36*/   
           registro_importado_NEW, /*37*/   
           tipo_valorizacao_OLD, /*38*/   
           tipo_valorizacao_NEW, /*39*/   
           cod_processo_OLD, /*40*/   
           cod_processo_NEW  /*41*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.codigo_empresa,  /*8*/  
           :new.codigo_empresa, /*9*/   
           :old.mes,  /*10*/  
           :new.mes, /*11*/   
           :old.ano,  /*12*/  
           :new.ano, /*13*/   
           :old.nivel_produto,  /*14*/  
           :new.nivel_produto, /*15*/   
           :old.grupo_produto,  /*16*/  
           :new.grupo_produto, /*17*/   
           :old.subgru_produto,  /*18*/  
           :new.subgru_produto, /*19*/   
           :old.item_produto,  /*20*/  
           :new.item_produto, /*21*/   
           :old.codigo_estagio,  /*22*/  
           :new.codigo_estagio, /*23*/   
           :old.ordem_producao,  /*24*/  
           :new.ordem_producao, /*25*/   
           :old.qtde_em_elaboracao,  /*26*/  
           :new.qtde_em_elaboracao, /*27*/   
           :old.valor_total_mp,  /*28*/  
           :new.valor_total_mp, /*29*/   
           :old.valor_total_mo,  /*30*/  
           :new.valor_total_mo, /*31*/   
           :old.valor_total_cp,  /*32*/  
           :new.valor_total_cp, /*33*/   
           :old.valor_total_cd,  /*34*/  
           :new.valor_total_cd, /*35*/   
           :old.registro_importado,  /*36*/  
           :new.registro_importado, /*37*/   
           :old.tipo_valorizacao,  /*38*/  
           :new.tipo_valorizacao, /*39*/   
           :old.cod_processo,  /*40*/  
           :new.cod_processo  /*41*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into ESTQ_038_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           codigo_empresa_OLD, /*8*/   
           codigo_empresa_NEW, /*9*/   
           mes_OLD, /*10*/   
           mes_NEW, /*11*/   
           ano_OLD, /*12*/   
           ano_NEW, /*13*/   
           nivel_produto_OLD, /*14*/   
           nivel_produto_NEW, /*15*/   
           grupo_produto_OLD, /*16*/   
           grupo_produto_NEW, /*17*/   
           subgru_produto_OLD, /*18*/   
           subgru_produto_NEW, /*19*/   
           item_produto_OLD, /*20*/   
           item_produto_NEW, /*21*/   
           codigo_estagio_OLD, /*22*/   
           codigo_estagio_NEW, /*23*/   
           ordem_producao_OLD, /*24*/   
           ordem_producao_NEW, /*25*/   
           qtde_em_elaboracao_OLD, /*26*/   
           qtde_em_elaboracao_NEW, /*27*/   
           valor_total_mp_OLD, /*28*/   
           valor_total_mp_NEW, /*29*/   
           valor_total_mo_OLD, /*30*/   
           valor_total_mo_NEW, /*31*/   
           valor_total_cp_OLD, /*32*/   
           valor_total_cp_NEW, /*33*/   
           valor_total_cd_OLD, /*34*/   
           valor_total_cd_NEW, /*35*/   
           registro_importado_OLD, /*36*/   
           registro_importado_NEW, /*37*/   
           tipo_valorizacao_OLD, /*38*/   
           tipo_valorizacao_NEW, /*39*/   
           cod_processo_OLD, /*40*/   
           cod_processo_NEW /*41*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.codigo_empresa, /*8*/   
           0, /*9*/
           :old.mes, /*10*/   
           0, /*11*/
           :old.ano, /*12*/   
           0, /*13*/
           :old.nivel_produto, /*14*/   
           '', /*15*/
           :old.grupo_produto, /*16*/   
           '', /*17*/
           :old.subgru_produto, /*18*/   
           '', /*19*/
           :old.item_produto, /*20*/   
           '', /*21*/
           :old.codigo_estagio, /*22*/   
           0, /*23*/
           :old.ordem_producao, /*24*/   
           0, /*25*/
           :old.qtde_em_elaboracao, /*26*/   
           0, /*27*/
           :old.valor_total_mp, /*28*/   
           0, /*29*/
           :old.valor_total_mo, /*30*/   
           0, /*31*/
           :old.valor_total_cp, /*32*/   
           0, /*33*/
           :old.valor_total_cd, /*34*/   
           0, /*35*/
           :old.registro_importado, /*36*/   
           0, /*37*/
           :old.tipo_valorizacao, /*38*/   
           0, /*39*/
           :old.cod_processo, /*40*/   
           0 /*41*/
         );    
    end;    
 end if;    
end inter_tr_ESTQ_038_log;
