create or replace trigger inter_tr_fatu_075_2
   after insert or delete or update on fatu_075

begin
   -- se foi atualizado os campos data_pagamento ou data_credito,
   -- atualiza o titulo com a maior data
   if inter_pc_crec_variaveis.codigo_empresa is not null
   then
      -- chama a procedure para atualizar a duplicata com a data do ultimo
      -- pagamento e do ultimo credito
      inter_pr_saldo_fatu_070_tr(inter_pc_crec_variaveis.codigo_empresa,
                                 inter_pc_crec_variaveis.cli_dup_cgc_cli9,
                                 inter_pc_crec_variaveis.cli_dup_cgc_cli4,
                                 inter_pc_crec_variaveis.cli_dup_cgc_cli2,
                                 inter_pc_crec_variaveis.tipo_titulo,
                                 inter_pc_crec_variaveis.num_duplicata,
                                 inter_pc_crec_variaveis.seq_duplicatas);

      -- seta null para as variaveis compartilhada, isto � pera o controle se
      -- a procedure deve ou nao ser executada (apenas quando se altera a 
      -- data_pagamento ou de credito
      inter_pc_crec_variaveis.codigo_empresa   := null;
      inter_pc_crec_variaveis.cli_dup_cgc_cli9 := null;
      inter_pc_crec_variaveis.cli_dup_cgc_cli4 := null;
      inter_pc_crec_variaveis.cli_dup_cgc_cli2 := null;
      inter_pc_crec_variaveis.tipo_titulo      := null;
      inter_pc_crec_variaveis.num_duplicata    := null;
      inter_pc_crec_variaveis.seq_duplicatas   := null;
   end if;
end inter_tr_fatu_075_2;
/
/* versao: 1 */
