CREATE OR REPLACE TRIGGER INTER_TR_ESTQ_308_HIST
AFTER INSERT OR UPDATE OR DELETE ON ESTQ_308
FOR EACH ROW
DECLARE
   v_usuario_rede          varchar(20);
   v_maquina_rede          varchar(40);
   v_aplicativo            varchar(20);
   v_nome_programa         varchar(20);
BEGIN
   
   select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
   into v_usuario_rede, v_maquina_rede, v_aplicativo
   from sys.gv_$session
   where audsid  = userenv('SESSIONID')
     and inst_id = userenv('INSTANCE')
     and rownum < 2;
     
   begin
     select hdoc_090.programa
     into v_nome_programa
     from hdoc_090
     where hdoc_090.sid = userenv('SESSIONID')
       and rownum       = 1
       and hdoc_090.programa not like '%menu%'
       and hdoc_090.programa not like '%_m%';
     exception
       when no_data_found then v_nome_programa := 'SQL';
   end;

    IF INSERTING THEN

        INSERT INTO ESTQ_308_HIST (
            COD_EMPRESA,
            NIVEL_ESTRUTURA,
            GRUPO_ESTRUTURA,
            SUBGRU_ESTRUTURA,
            ITEM_ESTRUTURA,
            SALDO_QTDE_NEW,
            DATA_OCORR,
            TIPO_OCORR,
            NOME_PROGRAMA,
            USUARIO_REDE,
            MAQUINA_REDE,
            APLICACAO
        ) VALUES (
            :NEW.COD_EMPRESA,
            :NEW.NIVEL_ESTRUTURA,
            :NEW.GRUPO_ESTRUTURA,
            :NEW.SUBGRU_ESTRUTURA,
            :NEW.ITEM_ESTRUTURA,
            :NEW.SALDO_QTDE,
            SYSDATE,
            'I',
            v_nome_programa,
            v_usuario_rede,
            v_maquina_rede,
            v_aplicativo
        );
    ELSIF UPDATING THEN

        INSERT INTO ESTQ_308_HIST (
            COD_EMPRESA,
            NIVEL_ESTRUTURA,
            GRUPO_ESTRUTURA,
            SUBGRU_ESTRUTURA,
            ITEM_ESTRUTURA,
            SALDO_QTDE_OLD,
            SALDO_QTDE_NEW,
            QTDE_EMPENHADA_OLD,
            QTDE_EMPENHADA_NEW,
            DATA_OCORR,
            TIPO_OCORR,
            NOME_PROGRAMA,
            USUARIO_REDE,
            MAQUINA_REDE,
            APLICACAO
        ) VALUES (
            :NEW.COD_EMPRESA,
            :NEW.NIVEL_ESTRUTURA,
            :NEW.GRUPO_ESTRUTURA,
            :NEW.SUBGRU_ESTRUTURA,
            :NEW.ITEM_ESTRUTURA,
            :OLD.SALDO_QTDE,
            :NEW.SALDO_QTDE,
            :OLD.QTDE_EMPENHADA,
            :NEW.QTDE_EMPENHADA,
            SYSDATE,
            'U',
            v_nome_programa, 
            v_usuario_rede,
            v_maquina_rede,
            v_aplicativo
        );
    ELSIF DELETING THEN
        INSERT INTO ESTQ_308_HIST (
            COD_EMPRESA,
            NIVEL_ESTRUTURA,
            GRUPO_ESTRUTURA,
            SUBGRU_ESTRUTURA,
            ITEM_ESTRUTURA,
            SALDO_QTDE_OLD,
            SALDO_QTDE_NEW,
            QTDE_EMPENHADA_OLD,
            QTDE_EMPENHADA_NEW,
            DATA_OCORR,
            TIPO_OCORR,
            NOME_PROGRAMA,
            USUARIO_REDE,
            MAQUINA_REDE,
            APLICACAO
        ) VALUES (
            :OLD.COD_EMPRESA,
            :OLD.NIVEL_ESTRUTURA,
            :OLD.GRUPO_ESTRUTURA,
            :OLD.SUBGRU_ESTRUTURA,
            :OLD.ITEM_ESTRUTURA,
            :OLD.SALDO_QTDE,
            NULL,
            :OLD.QTDE_EMPENHADA,
            NULL,
            SYSDATE,
            'D',
            v_nome_programa,
            v_usuario_rede,
            v_maquina_rede,
            v_aplicativo
        );
    END IF;
END INTER_TR_ESTQ_308_HIST;
/
