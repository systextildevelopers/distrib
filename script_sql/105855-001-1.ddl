ALTER TABLE ESTQ_400 ADD IMPRIME_DESC_CLI_DANFE NUMERIC(1) DEFAULT 0;

COMMENT ON COLUMN ESTQ_400.IMPRIME_DESC_CLI_DANFE IS 'Indica se campos de descricao do produto cliente serao impressos na danfe juntamente a descricao do produto';

EXEC INTER_PR_RECOMPILE;
/
