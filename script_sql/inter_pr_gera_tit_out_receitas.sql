create or replace PROCEDURE inter_pr_gera_tit_out_receitas(p_numero_adiantam in number, p_programa in varchar2, p_usuario in varchar2
) is

   v_codigo_contabil number;
   v_tipo_titulo number;
   v_origem_titulo number;
   v_codigo_transacao number;
   v_codigo_historico_cont number;
   v_posicao_titulo number;
   v_codigo_contabil_cc number;
   v_gera_contabil_cc number;


   v_codigo_historico_pgto number;
   v_cod_portador number;
   v_cod_portador_fatu_070 number;
   v_conta_corrente number;

   r_num_lcto number;
   r_des_erro varchar2(4000);
   v_sequencia_insert number;
   ok BOOLEAN;

BEGIN
    -- ler tabela de parametros oper_026 - Parâmetro Baixa Por Receita
    BEGIN
        select tipo_titulo, origem_titulo, codigo_contabil, codigo_transacao, codigo_historico_cont, posicao_titulo, cod_portador
            into v_tipo_titulo, v_origem_titulo, v_codigo_contabil, v_codigo_transacao, v_codigo_historico_cont, v_posicao_titulo, v_cod_portador_fatu_070
        from oper_026;

        EXCEPTION
            WHEN no_data_found THEN
                raise_application_error(-20001, r_des_erro || ' Erro ao buscar parametros - cpag_013' || Chr(10) || SQLERRM);
    END;


   FOR adto in (
      select a.codigo_empresa, a.numero_adiantam,
             a.cgc_9, a.cgc_4, a.cgc_2, c.nome_cliente,
             a.cod_moeda, m.descricao desc_moenda,
             a.valor_saldo saldo_moeda,
             a.cod_portador, p.nome_banco, a.origem_adiantam, c.codigo_contabil cod_contabil_cli, c.cdrepres_cliente 
      from cpag_200 a, pedi_010 c, basi_265 m, pedi_050 p
      where a.numero_adiantam = p_numero_adiantam
      and a.tipo_adiantam = 2
      and a.valor_saldo > 0
      and a.cgc_9 = c.cgc_9
      and a.cgc_4 = c.cgc_4
      and a.cgc_2 = c.cgc_2
      and a.cod_moeda = m.codigo_moeda
      and a.cod_portador = p.cod_portador

   )
    LOOP

    -- ler tabela de parametros oper_027 para baixa e titulos a receber - Conta corrente

    BEGIN
        select codigo_historico_pgto, cod_portador, conta_corrente
            into v_codigo_historico_pgto, v_cod_portador, v_conta_corrente
            from oper_027
            where cod_empresa = adto.codigo_empresa; 

        EXCEPTION
            WHEN no_data_found THEN
                raise_application_error(-20001, r_des_erro || ' Erro ao buscar parametros - cpag_014' || Chr(10) || SQLERRM);
    END;
    
        BEGIN
        select c.codigo_contabil, c.gera_contabil
            into v_codigo_contabil_cc, v_gera_contabil_cc
            from cpag_020 c
            where c.portador = v_cod_portador
            and c.conta_corrente = v_conta_corrente;

        EXCEPTION
            WHEN no_data_found THEN
            raise_application_error(-20001, r_des_erro || ' Erro ao buscar codigo_contabil e gera_contabil - cpag_020' || Chr(10) || SQLERRM);
    END;

        r_num_lcto := 0;
        v_sequencia_insert := 1;
        ok := false;

        inter_pr_contabilizacao_prg_02(adto.codigo_empresa,
                            0, --p_c_custo
                            adto.cgc_9,
                            adto.cgc_4,
                            adto.cgc_2,
                            v_codigo_transacao,
                            v_codigo_historico_cont,
                            v_conta_corrente,
                            adto.cod_contabil_cli,
                            v_codigo_contabil,
                            adto.numero_adiantam,
                            to_date(sysdate),
                            adto.saldo_moeda,
                            3, -- p_origem      titulo a receber                       
                            1, -- p_tp_cta_deb       IN NUMBER
                            5, -- p_tp_cta_cre       IN NUMBER
                            p_programa,
                            p_usuario,
                            adto.numero_adiantam || '-' || adto.nome_cliente,
                            v_tipo_titulo,
                            r_num_lcto,
                            r_des_erro);

        if r_des_erro is not null then
           rollback;
           raise_application_error(-20001, r_des_erro || Chr(10) || SQLERRM);
        end if;

     
        while ok = false 
        LOOP
            begin                        
                inter_pr_insere_fatu_070 (                                    
                  adto.codigo_empresa,                           
                  adto.cgc_9,                                    
                  adto.cgc_4,                                    
                  adto.cgc_2,                                    
                  v_tipo_titulo,                                 
                  adto.numero_adiantam,                          
                  v_sequencia_insert,                            
                  to_date(sysdate),                                       
                  adto.saldo_moeda,                              
                  adto.cdrepres_cliente,                         
                  to_date(sysdate),                                      
                  adto.numero_adiantam,                          
                  v_sequencia_insert,                            
                  v_codigo_historico_cont,                       
                  adto.codigo_empresa,                           
                  0, --in fatu_070.port_anterior%type            
                  v_cod_portador_fatu_070, --v_cod_portador,                                
                  v_posicao_titulo,                              
                  to_date(sysdate),                                      
                  '0', --in fatu_070.tecido_peca%type,           
                  2,   --fatu_070.seq_end_cobranca%type          
                  0,   --in fatu_070.comissao_lancada%type,      
                  0,   --in fatu_070.tipo_tit_origem%type,       
                  0,   --in fatu_070.num_dup_origem%type,        
                  0,   --in fatu_070.seq_dup_origem%type,        
                  adto.cgc_9,                                    
                  adto.cgc_4,                                    
                  adto.cgc_2,                                    
                  0,   --in fatu_070.origem_pedido%type          
                  v_codigo_transacao,                            
                  v_codigo_contabil,                             
                  adto.saldo_moeda,                              
                  0, --in fatu_070.nr_identificacao%type,        
                  v_conta_corrente,                              
                  r_num_lcto,                                    
                  0, -- in fatu_070.duplic_emitida%type,         
                  0,  -- in fatu_070.pedido_venda%type 
                  'BAIXA DE ADIANTAMENTOS POR OUTRAS RECEITAS', --p_complemento_hist
                  r_des_erro           
                );
                ok := true;
           exception 
                when others then 
                   ok := false;
                   v_sequencia_insert := v_sequencia_insert + 1;
           end;           
        end loop;
        
        if r_des_erro is not null then
             rollback;
             raise_application_error(-20001, r_des_erro || Chr(10) || SQLERRM);                      
        end if;  

        
        r_num_lcto := 0;
        r_des_erro := NULL;
        
        
        if v_gera_contabil_cc = 1 
        then      
          inter_pr_contabilizacao_prg_02(adto.codigo_empresa,
                             0, -- p_c_custo
                             adto.cgc_9,
                             adto.cgc_4,
                             adto.cgc_2,
                             v_codigo_transacao,
                             v_codigo_historico_pgto,
                             v_conta_corrente,
                             v_codigo_contabil_cc,
                             adto.cod_contabil_cli,
                             adto.numero_adiantam,
                             to_date(sysdate),
                             adto.saldo_moeda,
                             4, --p_origem    baixa titulos,
                             20, -- p_tp_cta_deb       IN NUMBER,
                             1, --p_tp_cta_cre       IN NUMBER,
                             p_programa,
                             p_usuario,
                             adto.numero_adiantam || '-' || adto.nome_cliente,
                             v_tipo_titulo,
                             r_num_lcto,
                             r_des_erro);
                             
             if r_des_erro is not null then
                      rollback;
                      raise_application_error(-20001, r_des_erro || Chr(10) || SQLERRM);
                      
             end if;
         end if;
     
     BEGIN  
     
     INTER_PR_INSERE_CPAG_026
     ( v_conta_corrente, v_cod_portador,  to_date(sysdate),
     'C', v_codigo_historico_pgto,  adto.saldo_moeda,
     2,  1,  'BAIXA OUTRAS RECEITAS', to_date(sysdate), r_num_lcto);
     
     EXCEPTION WHEN OTHERS THEN
       rollback;
       raise_application_error(-20001, r_des_erro || ' Erro ao inserir conta corrente - cpag_026' || Chr(10) || SQLERRM);
     END;
     
     BEGIN
     INTER_PR_GERA_FATU_075(
      adto.cgc_9,
      adto.cgc_4,
      adto.cgc_2,
      v_tipo_titulo, 
      adto.numero_adiantam,
      adto.codigo_empresa,
      v_sequencia_insert,
      to_date(sysdate),
      v_codigo_historico_pgto,
      v_cod_portador,  
      v_conta_corrente,  
      to_date(sysdate),
      1, --p_pago_adiantamento
      adto.numero_adiantam,
      r_num_lcto,
      adto.saldo_moeda, 
      0, --p_tipo_lanc_comissao
      r_des_erro);
  
      EXCEPTION WHEN OTHERS THEN
        rollback;
        raise_application_error(-20001, r_des_erro || ' Erro ao inserir conta corrente - cpag_026' || Chr(10) || SQLERRM);
     END;
     
     BEGIN
      update cpag_200 
      set valor_saldo = 0
      where tipo_adiantam = 2 
      and numero_adiantam = adto.numero_adiantam;
     EXCEPTION WHEN OTHERS THEN
         rollback;
        raise_application_error(-20001, r_des_erro || ' Erro ao atualziar conta corrente - cpag_200' || Chr(10) || SQLERRM);
     END;
      
   END LOOP;
   
   if r_des_erro is not null then
      rollback;
      raise_application_error(-20001, r_des_erro || Chr(10) || SQLERRM);
   else
      commit;
   end if;
   

END inter_pr_gera_tit_out_receitas;
