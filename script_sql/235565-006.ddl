--------------------------------------------------------------------
-- REMOVIDAS TODOS AS TABELAS QUE FORAM SUBSTIUIDAS PELO FINA_211 --
--------------------------------------------------------------------
DROP TABLE FINA_203_LOG;
DROP TABLE FINA_203_BKP;
DROP TABLE FINA_205_BKP;
DROP TABLE FINA_206_BKP;
DROP TABLE FINA_207_BKP;
DROP TABLE FINA_208_BKP;

------------------------------------------------------------------
-- Tabela de Objetos Alterados/Inseridos na configuração do SPH --
------------------------------------------------------------------
CREATE TABLE FINA_211
   (	
    id number(9),
	data_movimento date,
	nome_processo varchar2(255),
	tabela varchar2(60),
	tipo_processo varchar2(60),
    usuario varchar(60),
    empresa number,
	dados clob,
	CONSTRAINT FINA_211_PK PRIMARY KEY (ID) USING INDEX  ENABLE 
   );
      
	CREATE SEQUENCE FINA_211_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 10;

	CREATE OR REPLACE TRIGGER INTER_TR_FINA_211
	  before insert on FINA_211
	  for each row  
	begin   
	  if :NEW.ID is null then 
		select FINA_211_SEQ.nextval into :NEW.ID from sys.dual; 
	  end if; 
	end;  
/
