create or replace package inter_pc_cpag_variaveis
is

   -- Author  : Neylor Zimmermann de Carvalho
   -- Created : 24/10/05
   -- Purpose : Package do modulo de contas a pagar

   -- Public variable declarations
   codigo_empresa    cpag_010.codigo_empresa%type;
   cgc_9             cpag_010.cgc_9%type;
   cgc_4             cpag_010.cgc_4%type;
   cgc_2             cpag_010.cgc_2%type;
   tipo_titulo       cpag_010.tipo_titulo%type;
   nr_duplicata      cpag_010.nr_duplicata%type;
   parcela           cpag_010.parcela%type;

end inter_pc_cpag_variaveis;

/
/* versao: 1 */
