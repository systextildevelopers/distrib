create or replace procedure wms_pr_atu_pedidos (p_pedido       in  number,
                                                p_tipo_atu     in  varchar2,
                                                p_mensagem     in  varchar2) is
                                                
  v_tmp_int         number;
begin
   --ver se existe o pedido
   begin
      select 1
      into   v_tmp_int
      from inte_wms_pedidos
      where inte_wms_pedidos.numero_pedido  = p_pedido;
   exception
      when no_data_found then
         raise_application_error(-20000, 'ATEN��O! O c�digo de pedido n�o existe na tabela INTE_WMS_PEDIDOS.');
   end;

   if p_tipo_atu = 'A' --Atualizado pelo Systextil
   then

      begin
         update inte_wms_pedidos
         set    inte_wms_pedidos.flag_importacao_wms = 0 -- A importar
         where  inte_wms_pedidos.numero_pedido       = p_pedido;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela inte_wms_pedidos.' || Chr(10) || SQLERRM);

      end;

   end if;

   if p_tipo_atu = 'I' --Importado pelo WMS
   then

      begin
         update inte_wms_pedidos
         set    inte_wms_pedidos.flag_importacao_wms = 1 --Importado
         where  inte_wms_pedidos.numero_pedido       = p_pedido;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela inte_wms_pedidos.' || Chr(10) || SQLERRM);

      end;

   end if;

   if p_tipo_atu = 'MI' --Montagem iniciada no WMS
   then

      begin
         update inte_wms_pedidos
         set    inte_wms_pedidos.flag_importacao_wms = 2, --Montagem iniciada
                inte_wms_pedidos.timestamp_ini_mont  = sysdate
         where  inte_wms_pedidos.numero_pedido       = p_pedido;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela inte_wms_pedidos.' || Chr(10) || SQLERRM);

      end;

   end if;
   
   if p_tipo_atu = 'MF' --Montagem finalizada no WMS
   then

      begin
         update inte_wms_pedidos
         set    inte_wms_pedidos.flag_importacao_wms = 3, --Montagem finalizada
                inte_wms_pedidos.timestamp_fim_mont  = sysdate
         where  inte_wms_pedidos.numero_pedido       = p_pedido;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;      
         exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela inte_wms_pedidos.' || Chr(10) || SQLERRM);

      end;

   end if;
   
   if p_tipo_atu = 'E' --Pedido expedido no WMS
   then

      begin
         update inte_wms_pedidos
         set    inte_wms_pedidos.flag_importacao_wms = 4 --Expedido
         where  inte_wms_pedidos.numero_pedido       = p_pedido;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela inte_wms_pedidos.' || Chr(10) || SQLERRM);

      end;

   end if;
   
   if p_tipo_atu = 'C' --Coleta cancelada no WMS
   then

      begin
         update inte_wms_pedidos
         set    inte_wms_pedidos.flag_importacao_wms = 5 --Coleta Cancelada
         where  inte_wms_pedidos.numero_pedido       = p_pedido;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela inte_wms_pedidos.' || Chr(10) || SQLERRM);

      end;

   end if;

   if p_tipo_atu = 'R' --Importa��o rejeitada pelo WMS
   then

      begin
         update inte_wms_pedidos
         set    inte_wms_pedidos.flag_importacao_wms = 9, --Importa��o rejeitada
                inte_wms_pedidos.mensagem            = p_mensagem
         where  inte_wms_pedidos.numero_pedido       = p_pedido;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela inte_wms_pedidos.' || Chr(10) || SQLERRM);

      end;

   end if;
   
   if p_tipo_atu = 'S' --Integra��o de retorno realizada pelo Systextil
   then

      begin
         update inte_wms_pedidos
         set    inte_wms_pedidos.flag_importacao_st  = 1 --Integra��o de retorno realizada
         where  inte_wms_pedidos.numero_pedido       = p_pedido;
         -- comentado conforme solicita��o de Jean /Renato SCI ss 65853/036
         -- commit;
      exception
          when others then
             raise_application_error(-20000, 'N�o atualizou tabela inte_wms_pedidos.' || Chr(10) || SQLERRM);

      end;

   end if;
   
end wms_pr_atu_pedidos;

/

exec inter_pr_recompile;
/* versao: 2 */


 exit;


 exit;

 exit;
