create table pedi_038 (
	
	cod_portador number(3),
	instrucao number(2)
);

alter table pedi_038 add
   constraint pk_pedi_038 primary key (cod_portador, instrucao);

exec inter_pr_recompile;
