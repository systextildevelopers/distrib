alter table oper_023
add cod_empresa number(3) default 0;

alter table oper_023
add constraint pk_oper_023 primary key(cod_empresa, cod_portador, conta_corrente, codigo_historico_pgto);

/
exec inter_pr_recompile;           
