create or replace trigger INTER_TR_OBRF_010_POSTHAUS
before insert on obrf_010
for each row

declare

  eh_nota_posthaus number(1);

begin

  begin
    select 1 into eh_nota_posthaus
    from haus_007 
    where haus_007.numero_danf_nfe = :new.numero_danf_nfe
      and rownum = 1;
  exception
    when no_data_found then
         eh_nota_posthaus := 0;
  end;
  
  if eh_nota_posthaus = 1
  then
    begin
      update haus_007 
      set situacao = 1, --PROCESSADO
          observacao = 'PROCESSADO'
      where haus_007.numero_danf_nfe = :new.numero_danf_nfe;
    end;
  end if;

end inter_tr_obrf_010_posthaus;
