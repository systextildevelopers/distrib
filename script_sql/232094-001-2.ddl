insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('supr_f014', 'Cadastro de dependentes', 0, 0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'supr_f014', 'favoritos', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Cadastro de dependentes'
where hdoc_036.codigo_programa = 'supr_f014'
  and hdoc_036.locale          = 'es_ES';

commit;
