create or replace FUNCTION validar_matriz_pedi_155(p_de IN NUMBER, p_ate IN NUMBER)
  RETURN BOOLEAN
IS
  v_count NUMBER;
BEGIN
  
  SELECT COUNT(*)
  INTO v_count
  FROM pedi_155
  WHERE (METROS_DE <= p_de  AND METROS_ATE >= p_de)
     OR (METROS_DE <= p_ate AND METROS_ATE >= p_ate)
     OR (METROS_DE = p_de  AND METROS_ATE = p_ate);

  RETURN (v_count = 0);
END;
