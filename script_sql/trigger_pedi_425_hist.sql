
  CREATE OR REPLACE TRIGGER "TRIGGER_PEDI_425_HIST" 
BEFORE UPDATE of pedido_venda, tipo_homologacao, sequencia, data_envio,
	etiquetas_previstas, etiquetas_faturadas, etiquetas_enviadas, volume,
	origem_etiquetas, resposta_pergunta
OR DELETE
OR INSERT
on pedi_425
for each row

declare
   ws_pedido_venda         number(6);
   ws_tipo_homologacao     number(3);
   ws_sequencia            number(3);
   ws_data_envio_old       date;
   ws_data_envio_atu       date;
   ws_etq_previstas_old    number(6);
   ws_etq_previstas_atu    number(6);
   ws_etq_faturadas_old    number(6);
   ws_etq_faturadas_atu    number(6);
   ws_etq_enviadas_old     number(6);
   ws_etq_enviadas_atu     number(6);
   ws_volume_old           number(2);
   ws_volume_atu           number(2);
   ws_tipo_ocorr           varchar2(1);
   ws_usuario_rede         varchar2(20);
   ws_maquina_rede         varchar2(40);
   ws_aplicacao            varchar2(20);
begin
   if INSERTING then
      ws_pedido_venda         := :new.pedido_venda;
      ws_tipo_homologacao     := :new.tipo_homologacao;
      ws_sequencia            := :new.sequencia;
      ws_data_envio_old       := null;
      ws_data_envio_atu       := :new.data_envio;
      ws_etq_previstas_old    := 0;
      ws_etq_previstas_atu    := :new.etiquetas_previstas;
      ws_etq_faturadas_old    := 0;
      ws_etq_faturadas_atu    := :new.etiquetas_faturadas;
      ws_etq_enviadas_old     := 0;
      ws_etq_enviadas_atu     := :new.etiquetas_enviadas;
      ws_volume_old           := 0;
      ws_volume_atu           := :new.volume;
      ws_tipo_ocorr           := 'I';
   elsif UPDATING then
         ws_pedido_venda         := :old.pedido_venda;
         ws_tipo_homologacao     := :old.tipo_homologacao;
         ws_sequencia            := :old.sequencia;
         ws_data_envio_old       := :old.data_envio;
         ws_data_envio_atu       := :new.data_envio;
         ws_etq_previstas_old    := :old.etiquetas_previstas;
         ws_etq_previstas_atu    := :new.etiquetas_previstas;
         ws_etq_faturadas_old    := :old.etiquetas_faturadas;
         ws_etq_faturadas_atu    := :new.etiquetas_faturadas;
         ws_etq_enviadas_old     := :old.etiquetas_enviadas;
         ws_etq_enviadas_atu     := :new.etiquetas_enviadas;
         ws_volume_old           := :old.volume;
         ws_volume_atu           := :new.volume;
         ws_tipo_ocorr           := 'A';
      elsif DELETING then
            ws_pedido_venda         := :old.pedido_venda;
            ws_tipo_homologacao     := :old.tipo_homologacao;
            ws_sequencia            := :old.sequencia;
            ws_data_envio_old       := :old.data_envio;
            ws_data_envio_atu       := null;
            ws_etq_previstas_old    := :old.etiquetas_previstas;
            ws_etq_previstas_atu    := 0;
            ws_etq_faturadas_old    := :old.etiquetas_faturadas;
            ws_etq_faturadas_atu    := 0;
            ws_etq_enviadas_old     := :old.etiquetas_enviadas;
            ws_etq_enviadas_atu     := 0;
            ws_volume_old           := :old.volume;
            ws_volume_atu           := 0;
            ws_tipo_ocorr           := 'D';
   end if;

   select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
   into ws_usuario_rede, ws_maquina_rede, ws_aplicacao
   from sys.gv_$session
   where audsid  = userenv('SESSIONID')
     and inst_id = userenv('INSTANCE')
     and rownum < 2;


   INSERT INTO pedi_425_hist
     (pedido_venda,         tipo_homologacao,
      sequencia,            data_envio_old,
      data_envio_atu,       etq_previstas_old,
      etq_previstas_atu,    etq_faturadas_old,
      etq_faturadas_atu,    etq_enviadas_old,
      etq_enviadas_atu,     volume_old,
      volume_atu,           tipo_ocorr,
      data_ocorr,           usuario_rede,
      maquina_rede,         aplicacao)
   VALUES
     (ws_pedido_venda,         ws_tipo_homologacao,
      ws_sequencia,            ws_data_envio_old,
      ws_data_envio_atu,       ws_etq_previstas_old,
      ws_etq_previstas_atu,    ws_etq_faturadas_old,
      ws_etq_faturadas_atu,    ws_etq_enviadas_old,
      ws_etq_enviadas_atu,     ws_volume_old,
      ws_volume_atu,           ws_tipo_ocorr,
      sysdate,                 ws_usuario_rede,
      ws_maquina_rede,         ws_aplicacao);

end trigger_pedi_425_hist;

-- ALTER TRIGGER "TRIGGER_PEDI_425_HIST" ENABLE
 

/

exec inter_pr_recompile;

