
  CREATE OR REPLACE TRIGGER "INTER_TR_LOJA_075_LOG" 
after insert or delete or update
on LOJA_075
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid); 

 if inserting
 then
    begin

        insert into LOJA_075_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           DOCUMENTO_OLD,   /*8*/
           DOCUMENTO_NEW,   /*9*/
           CONDICAO_PAGTO_OLD,   /*10*/
           CONDICAO_PAGTO_NEW,   /*11*/
           SEQUENCIA_OLD,   /*12*/
           SEQUENCIA_NEW,   /*13*/
           VALOR_PARC_OLD,   /*14*/
           VALOR_PARC_NEW,   /*15*/
           PERC_VENCIMENTO_OLD,   /*16*/
           PERC_VENCIMENTO_NEW,   /*17*/
           DATA_VENCTO_OLD,   /*18*/
           DATA_VENCTO_NEW,   /*19*/
           COD_EMPRESA_OLD,   /*20*/
           COD_EMPRESA_NEW,   /*21*/
           FORMA_PGTO_OLD,   /*22*/
           FORMA_PGTO_NEW,   /*23*/
           DEVOLUCAO_OLD,   /*24*/
           DEVOLUCAO_NEW    /*25*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.DOCUMENTO, /*9*/
           0,/*10*/
           :new.CONDICAO_PAGTO, /*11*/
           0,/*12*/
           :new.SEQUENCIA, /*13*/
           0,/*14*/
           :new.VALOR_PARC, /*15*/
           0,/*16*/
           :new.PERC_VENCIMENTO, /*17*/
           null,/*18*/
           :new.DATA_VENCTO, /*19*/
           0,/*20*/
           :new.COD_EMPRESA, /*21*/
           0,/*22*/
           :new.FORMA_PGTO, /*23*/
           0,/*24*/
           :new.DEVOLUCAO /*25*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into LOJA_075_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           DOCUMENTO_OLD, /*8*/
           DOCUMENTO_NEW, /*9*/
           CONDICAO_PAGTO_OLD, /*10*/
           CONDICAO_PAGTO_NEW, /*11*/
           SEQUENCIA_OLD, /*12*/
           SEQUENCIA_NEW, /*13*/
           VALOR_PARC_OLD, /*14*/
           VALOR_PARC_NEW, /*15*/
           PERC_VENCIMENTO_OLD, /*16*/
           PERC_VENCIMENTO_NEW, /*17*/
           DATA_VENCTO_OLD, /*18*/
           DATA_VENCTO_NEW, /*19*/
           COD_EMPRESA_OLD, /*20*/
           COD_EMPRESA_NEW, /*21*/
           FORMA_PGTO_OLD, /*22*/
           FORMA_PGTO_NEW, /*23*/
           DEVOLUCAO_OLD, /*24*/
           DEVOLUCAO_NEW  /*25*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.DOCUMENTO,  /*8*/
           :new.DOCUMENTO, /*9*/
           :old.CONDICAO_PAGTO,  /*10*/
           :new.CONDICAO_PAGTO, /*11*/
           :old.SEQUENCIA,  /*12*/
           :new.SEQUENCIA, /*13*/
           :old.VALOR_PARC,  /*14*/
           :new.VALOR_PARC, /*15*/
           :old.PERC_VENCIMENTO,  /*16*/
           :new.PERC_VENCIMENTO, /*17*/
           :old.DATA_VENCTO,  /*18*/
           :new.DATA_VENCTO, /*19*/
           :old.COD_EMPRESA,  /*20*/
           :new.COD_EMPRESA, /*21*/
           :old.FORMA_PGTO,  /*22*/
           :new.FORMA_PGTO, /*23*/
           :old.DEVOLUCAO,  /*24*/
           :new.DEVOLUCAO  /*25*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into LOJA_075_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           DOCUMENTO_OLD, /*8*/
           DOCUMENTO_NEW, /*9*/
           CONDICAO_PAGTO_OLD, /*10*/
           CONDICAO_PAGTO_NEW, /*11*/
           SEQUENCIA_OLD, /*12*/
           SEQUENCIA_NEW, /*13*/
           VALOR_PARC_OLD, /*14*/
           VALOR_PARC_NEW, /*15*/
           PERC_VENCIMENTO_OLD, /*16*/
           PERC_VENCIMENTO_NEW, /*17*/
           DATA_VENCTO_OLD, /*18*/
           DATA_VENCTO_NEW, /*19*/
           COD_EMPRESA_OLD, /*20*/
           COD_EMPRESA_NEW, /*21*/
           FORMA_PGTO_OLD, /*22*/
           FORMA_PGTO_NEW, /*23*/
           DEVOLUCAO_OLD, /*24*/
           DEVOLUCAO_NEW /*25*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.DOCUMENTO, /*8*/
           0, /*9*/
           :old.CONDICAO_PAGTO, /*10*/
           0, /*11*/
           :old.SEQUENCIA, /*12*/
           0, /*13*/
           :old.VALOR_PARC, /*14*/
           0, /*15*/
           :old.PERC_VENCIMENTO, /*16*/
           0, /*17*/
           :old.DATA_VENCTO, /*18*/
           null, /*19*/
           :old.COD_EMPRESA, /*20*/
           0, /*21*/
           :old.FORMA_PGTO, /*22*/
           0, /*23*/
           :old.DEVOLUCAO, /*24*/
           0 /*25*/
         );
    end;
 end if;
end inter_tr_LOJA_075_log;
-- ALTER TRIGGER "INTER_TR_LOJA_075_LOG" ENABLE
 

/

exec inter_pr_recompile;

