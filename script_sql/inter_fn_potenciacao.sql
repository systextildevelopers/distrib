
  CREATE OR REPLACE FUNCTION "INTER_FN_POTENCIACAO" (base in number, expoente number)
return number
is
   Result number;
begin
   result := power(base, expoente);

   return(Result);
end inter_fn_potenciacao;
 

/

exec inter_pr_recompile;

