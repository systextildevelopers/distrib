CREATE TABLE HIST_382
(
   NUMERO_VOLUME    NUMBER(9)           DEFAULT 0,
   CODIGO_BARRAS    VARCHAR2(3)   		DEFAULT ' ',
   DATA_OCORRENCIA  DATE                DEFAULT sysdate
);
