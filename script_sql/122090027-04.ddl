create table cons_003
( Natureza                       NUMBER(3) default 0 not null,  
  tipo_retorno                   NUMBER(1) default 0  );  
  
COMMENT ON COLUMN cons_003.tipo_retorno   IS 'Identifica as naturezas para retorno  0 devolução, 1 retorno';
alter table cons_003 add constraint pk_cons_003 primary key ( natureza);
