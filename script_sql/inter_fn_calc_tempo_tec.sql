
  CREATE OR REPLACE FUNCTION "INTER_FN_CALC_TEMPO_TEC" (p_nivel_tec         in varchar2, p_grupo_tec       in varchar2,
                                                    p_subgrupo_tec      in varchar2, p_item_tec        in varchar2,
                                                    p_alt_tec           in number,   p_rot_tec         in number,
                                                    p_grupo_maquina_tec in varchar2, p_sub_maquina_tec in varchar2,
                                                    p_qtde_quilos_tec   in number)
RETURN number is
   -- Variavel a ser retornada no calculo da funcao
   v_tempo_total_aux       number (15,4);

   -- Variaveis a serem utilizadas no calculo do tempo
   v_tempo_total_aux_princ number;
   v_tempo_total_aux_5     number;

   v_numero_grafico_aux    number;
   v_numero_grafico_aux_5  number;

   v_sub_comp_aux          varchar2(3);
   v_item_comp_aux         varchar2(6);

   v_tempo_unitario        number;

   v_numero_grafico        number;

   -- Estagio de tinturaria para substituir a MDI
   v_estagio_tinturaria    number;

begin
   v_tempo_total_aux      := 0.0000;
   v_tempo_total_aux_5    := 0.0000;
   v_numero_grafico_aux   := 0;
   v_numero_grafico_aux_5 := 0;

   begin
      select empr_001.estagio_tintur into v_estagio_tinturaria
      from empr_001;
   end;

   for reg_basi_050 in (select basi_050.nivel_comp,      basi_050.grupo_comp,
                               basi_050.sub_comp,        basi_050.item_comp,
                               basi_050.sequencia,       basi_050.sub_item,
                               basi_050.item_item,       basi_050.numero_grafico,
                               basi_050.alternativa_comp
                        from basi_050
                        where  basi_050.nivel_item       = p_nivel_tec
                          and  basi_050.grupo_item       = p_grupo_tec
                          and (basi_050.sub_item         = p_subgrupo_tec or basi_050.sub_item  = '000')
                          and (basi_050.item_item        = p_item_tec     or basi_050.item_item = '000000')
                          and  basi_050.alternativa_item = p_alt_tec
                          and  basi_050.nivel_comp       = '5'
                        order by basi_050.sequencia, basi_050.numero_grafico)
   loop
      if v_numero_grafico_aux <> reg_basi_050.numero_grafico or v_numero_grafico_aux = 0
      then
         if reg_basi_050.sub_comp = '000'
         then
            begin
               select basi_040.sub_comp
               into v_sub_comp_aux
               from basi_040
               where basi_040.nivel_item       = p_nivel_tec
                 and basi_040.grupo_item       = p_grupo_tec
                 and basi_040.sub_item         = p_subgrupo_tec
                 and basi_040.item_item        = reg_basi_050.item_item
                 and basi_040.sequencia        = reg_basi_050.sequencia
                 and basi_040.alternativa_item = p_alt_tec;
               exception when no_data_found
               then v_sub_comp_aux := '000';
            end;
         else
            v_sub_comp_aux := reg_basi_050.sub_comp;
         end if;

         if reg_basi_050.item_comp = '000000'
         then
            begin
               select basi_040.item_comp
               into v_item_comp_aux
               from basi_040
               where basi_040.nivel_item       = p_nivel_tec
                 and basi_040.grupo_item       = p_grupo_tec
                 and basi_040.sub_item         = reg_basi_050.sub_item
                 and basi_040.item_item        = p_item_tec
                 and basi_040.sequencia        = reg_basi_050.sequencia
                 and basi_040.alternativa_item = p_alt_tec;
               exception when no_data_found
               then v_item_comp_aux := '000000';
            end;
         else
            v_item_comp_aux := reg_basi_050.item_comp;
         end if;

         if reg_basi_050.numero_grafico = 0
         then
            begin
               select basi_010.numero_grafico
               into   v_numero_grafico
               from basi_010
               where basi_010.nivel_estrutura  = reg_basi_050.nivel_comp
                 and basi_010.grupo_estrutura  = reg_basi_050.grupo_comp
                 and basi_010.subgru_estrutura = v_sub_comp_aux
                 and basi_010.item_estrutura   = v_item_comp_aux;
               exception when others
               then v_numero_grafico := 0;
            end;
         else
            v_numero_grafico := reg_basi_050.numero_grafico;
         end if;

         if v_numero_grafico > 0
         then
            v_tempo_total_aux_princ := inter_fn_get_tempo_mqop_150 (v_numero_grafico,
                                                                    p_grupo_maquina_tec,
                                                                    p_sub_maquina_tec);
         end if;

         if v_tempo_total_aux_princ is not null and v_numero_grafico <> 0
         then
            v_tempo_total_aux := v_tempo_total_aux + v_tempo_total_aux_princ;
         end if;

         for reg_basi_050_5 in (select basi_050.nivel_comp, basi_050.grupo_comp,
                                       basi_050.sub_comp,   basi_050.item_comp,
                                       basi_050.sequencia,  basi_050.sub_item,
                                       basi_050.item_item,  basi_050.numero_grafico
                                from basi_050
                                where  basi_050.nivel_item       = reg_basi_050.nivel_comp
                                  and  basi_050.grupo_item       = reg_basi_050.grupo_comp
                                  and (basi_050.sub_item         = v_sub_comp_aux  or basi_050.sub_item  = '000')
                                  and (basi_050.item_item        = v_item_comp_aux or basi_050.item_item = '000000')
                                  and  basi_050.alternativa_item = reg_basi_050.alternativa_comp
                                  and  basi_050.nivel_comp       = '5'
                                order by basi_050.sequencia, basi_050.numero_grafico)
         loop
            if v_numero_grafico_aux_5 <> reg_basi_050_5.numero_grafico or v_numero_grafico_aux_5 = 0
            then
               if reg_basi_050_5.numero_grafico > 0
               then
                  v_tempo_total_aux_5 := inter_fn_get_tempo_mqop_150 (reg_basi_050_5.numero_grafico,
                                                                      p_grupo_maquina_tec,
                                                                      p_sub_maquina_tec);
               end if;

               if v_tempo_total_aux_5 is not null and reg_basi_050_5.numero_grafico <> 0
               then
                  v_tempo_total_aux := v_tempo_total_aux + v_tempo_total_aux_5;
               end if;

               v_numero_grafico_aux_5 := reg_basi_050_5.numero_grafico;
            end if;
         end loop;

         v_numero_grafico_aux := reg_basi_050.numero_grafico;

      end if;
   end loop;

   -- Se nao encontrar pelo grafico, carrega pelo roteiro.
   if v_tempo_total_aux = 0.00
   then
      v_tempo_unitario := inter_fn_get_tempo_estagio (p_nivel_tec,
                                                      p_grupo_tec,
                                                      p_subgrupo_tec,
                                                      p_item_tec,
                                                      p_alt_tec,
                                                      p_rot_tec,
                                                      v_estagio_tinturaria,
                                                      0);

      v_tempo_total_aux := v_tempo_unitario * p_qtde_quilos_tec;
   end if;

   return(v_tempo_total_aux);

end inter_fn_calc_tempo_tec;
 

/

exec inter_pr_recompile;

