create table efic_306(  
cod_nao_conf number(6),
cod_acao number(4),
data date,
usuario varchar2(30),
cod_setor number(3),
detalhes long,
constraint efic_306_pk primary key (cod_nao_conf, cod_acao),
constraint fk_efic_306_efic_305	foreign key (cod_nao_conf) references efic_305 (id),
constraint fk_efic_306_efic_301	foreign key (cod_acao) references efic_301 (id)
);

comment on column efic_306.cod_nao_conf is 'Identificador da não conformidade';
comment on column efic_306.cod_acao is 'Identificador da ação da não conformidade';
comment on column efic_306.data is 'Data de registro da ação';
comment on column efic_306.usuario is 'Usuario de registro da ação';
comment on column efic_306.cod_setor is 'Código do setor do usuario de registro da ação';
comment on column efic_306.detalhes is 'Detalhes da ação da não conformidade';
comment on table efic_306 is 'Tabela de cadastro das ações da não conformidade produtiva';

/	
	
