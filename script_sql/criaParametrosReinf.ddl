/* Caminho dos arquivos XML */
INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('reinf.caminhoArquivos', 0, 'lb37345', 'fy27102', '', null, null, null);

/* Identificação do ambiente */
INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('reinf.ambiente', 1, 'lb34395', '', null, 3, null, null);

/* Classificação tributária do contribuinte */
INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('reinf.classTrib', 1, 'lb46103', 'fy44883', null, 0, null, null);

/*Obrigatoriedade ECD */
INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('reinf.entregaEcd', 1, 'lb46102', 'fy44884', null, 0, null, null);

/* Desoneração da folha */
INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('reinf.desoneraFolha', 1, 'lb46104', '', null, 0, null, null);

/* Acordo internacional de para isenção de multa */
INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('reinf.acordoIsentMulta', 1, 'lb46105', '', null, 0, null, null);

/* Situação da pessoa juridica */
INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('reinf.sitPessoaJurid', 1, 'lb46106', '', null, 0, null, null);

/* CNPJ empresa prestadora de software */
INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('reinf.cnpjPrestadora', 0, 'lb46107', 'fy44885', ' ', null, null, null);

/* Nome empresa prestadora de software */
INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('reinf.nomePrestadora', 0, 'lb46108', 'fy44886', '', null, null, null);

/* Nome do contato na prestadora de software */
INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('reinf.nomeContatoPrestadora', 0, 'lb46109', 'fy44887', '', null, null, null);

/* Número do telefone da prestadora de software */
INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('reinf.numTelPrestadora', 0, 'lb46111', 'fy44888', '', null, null, null);

/* Email da prestadora de software */
INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('reinf.emailPrestadora', 0, 'lb46112', 'fy44889', '', null, null, null);

COMMIT;
