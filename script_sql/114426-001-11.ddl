create table OBRF_794
(
  numero_solicitacao  NUMBER(9) default 0 not null,
  numero_documento    NUMBER(9) default 0 not null,
  periodo_producao040 NUMBER(4) default 0 not null,
  ordem_confeccao040  NUMBER(5) default 0 not null,
  codigo_estagio      NUMBER(2) default 0 not null,
  ordem_producao      NUMBER(9) default 0 not null,
  nivel               VARCHAR2(1) default ' ' not null,
  grupo               VARCHAR2(5) default ' ' not null,
  subgrupo            VARCHAR2(3) default ' ' not null,
  item                VARCHAR2(6) default ' ' not null,
  quantidade          NUMBER(15,3) default 0.000 not null,
  flag_marcado        NUMBER(1) default 0 not null,
  lote_acomp          NUMBER(6) default 0 not null
);

-- Add comments to the columns 
comment on column OBRF_794.NUMERO_SOLICITACAO
  is 'Número de solicitação da execução do processo';
comment on column OBRF_794.PERIODO_PRODUCAO040
  is 'Período de Produção da execução do processo neste passo';
comment on column OBRF_794.ORDEM_CONFECCAO040
  is 'Ordem de Confecção da execução do processo neste passo';
comment on column OBRF_794.CODIGO_ESTAGIO
  is 'Código do Estágio da execução do processo neste passo';
comment on column OBRF_794.ORDEM_PRODUCAO
  is 'Sequencial do produto a ser gerado na execução do processo neste passo';
comment on column OBRF_794.NIVEL
  is 'Nível do produto a ser gerado na execução do processo neste passo';
comment on column OBRF_794.GRUPO
  is 'Grupo do produto a ser gerado na execução do processo neste passo';
comment on column OBRF_794.SUBGRUPO
  is 'Subgrupo do produto a ser gerado na execução do processo neste passo';
comment on column OBRF_794.ITEM
  is 'Item do produto a ser gerado na execução do processo neste passo';
comment on column OBRF_794.QUANTIDADE
  is 'Quantidade do item a ser gerado na execução do processo neste passo';
comment on column OBRF_794.FLAG_MARCADO
  is 'Flag para indicar se o item foi considerado (1) ou não (0) a ser gerado na execução do processo neste passo';


exec inter_pr_recompile;
/
