alter table inte_100
add (perc_comissao_fatu_repres number(5,2)  DEFAULT NULL,
     perc_comissao_fatu_adm number(5,2)  DEFAULT NULL);

comment on column inte_100.perc_comissao_fatu_repres is 'Percentual utilizado para comissao no faturamento caso a empresa utilizar por prazo medio';
comment on column inte_100.perc_comissao_fatu_adm is 'Percentual utilizado para comissao no faturamento caso a empresa utilizar por prazo medio';

exec inter_pr_recompile;
