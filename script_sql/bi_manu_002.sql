create or replace TRIGGER "BI_MANU_002" 
  before insert on "MANU_002"               
  for each row  
begin   
  if :NEW."SOLICITACAO" is null then 
    select "MANU_002_SEQ".nextval into :NEW."SOLICITACAO" from dual;
     
  end if; 
end; 
/
