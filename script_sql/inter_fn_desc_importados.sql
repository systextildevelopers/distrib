
  CREATE OR REPLACE FUNCTION "INTER_FN_DESC_IMPORTADOS" (p_estado       in varchar2,
                                                    p_data_emissao in date,
                                                    p_cod_empresa  in number,
                                                    p_nivel        in varchar2,
                                                    p_grupo        in varchar2,
                                                    p_subgrupo     in varchar2,
                                                    p_item         in varchar2)

return number is

   v_perc_desc_icms number;
   v_not_found      varchar2(1);
   v_periodo_max    date;

/*PROCEDURE INTERNA PARA RETORNAR OS DADOS DA obrf_658*/
procedure p_busca_dados_obrf_658(p_data_emissao   in date,
                                 p_cod_emp_int    in number,
                                 p_nivel          in varchar2,
                                 p_grupo          in varchar2,
                                 p_subgrupo       in varchar2,
                                 p_item           in varchar2,
                                 p_estado         in varchar2,
                                 p_perc_desc_icms in out number,
                                 p_not_found      in out varchar2
) is

begin

   begin
      select obrf_658.perc_desc_icms
      into   p_perc_desc_icms
      from obrf_658
      where to_date('01'||trim(to_char(obrf_658.mes,'00'))||trim(to_char(obrf_658.ano,'0000')), 'DDMMYYYY') = p_data_emissao
        and obrf_658.cod_empresa   = p_cod_emp_int
        and obrf_658.nivel         = p_nivel
        and obrf_658.grupo         = p_grupo
        and obrf_658.subgrupo      = p_subgrupo
        and obrf_658.item          = p_item
        and obrf_658.estado        = p_estado;
      exception
         when others then
            p_not_found      := 'n';
            p_perc_desc_icms := 0.00;

   end;

   if sql%found
   then p_not_found := 's';
   else p_not_found := 'n';
   end if;

end;
/*FIM- Procedure interna*/

begin

   v_not_found      := 'n';
   v_perc_desc_icms := 0.00;

   begin
      select max(to_date('01'||trim(to_char(obrf_658.mes,'00'))||trim(to_char(obrf_658.ano,'0000')), 'DDMMYYYY'))
      into   v_periodo_max
      from obrf_658
      where  to_date('01'||trim(to_char(obrf_658.mes,'00'))||trim(to_char(obrf_658.ano,'0000')), 'DDMMYYYY') <= p_data_emissao
        and  obrf_658.cod_empresa = p_cod_empresa
        and  obrf_658.nivel       = p_nivel
        and  obrf_658.grupo       = p_grupo
        and (obrf_658.subgrupo   = p_subgrupo or obrf_658.subgrupo = 'XXX')
        and (obrf_658.item       = p_item     or obrf_658.item     = 'XXXXXX')
        and  obrf_658.estado      = p_estado;
      exception
         when others then
            v_periodo_max := to_date(null, '');

   end;

   if v_periodo_max is not null
   then

      p_busca_dados_obrf_658(v_periodo_max,
                             p_cod_empresa,
                             p_nivel,
                             p_grupo,
                             'XXX',
                             'XXXXXX',
                             p_estado,
                             v_perc_desc_icms,
                             v_not_found);

      if v_not_found = 'n'
      then
         p_busca_dados_obrf_658(v_periodo_max,
                                p_cod_empresa,
                                p_nivel,
                                p_grupo,
                                'XXX',
                                p_item,
                                p_estado,
                                v_perc_desc_icms,
                                v_not_found);

         if v_not_found = 'n'
         then
            p_busca_dados_obrf_658(v_periodo_max,
                                   p_cod_empresa,
                                   p_nivel,
                                   p_grupo,
                                   p_subgrupo,
                                   'XXXXXX',
                                   p_estado,
                                   v_perc_desc_icms,
                                   v_not_found);

            if v_not_found = 'n'
            then
               p_busca_dados_obrf_658(v_periodo_max,
                                      p_cod_empresa,
                                      p_nivel,
                                      p_grupo,
                                      p_subgrupo,
                                      p_item,
                                      p_estado,
                                      v_perc_desc_icms,
                                      v_not_found);
            end if;
         end if;
      end if;

   else

      begin
         select max(to_date('01'||trim(to_char(obrf_658.mes,'00'))||trim(to_char(obrf_658.ano,'0000')), 'DDMMYYYY'))
         into   v_periodo_max
         from obrf_658
         where  to_date('01'||trim(to_char(obrf_658.mes,'00'))||trim(to_char(obrf_658.ano,'0000')), 'DDMMYYYY') <= p_data_emissao
           and  obrf_658.cod_empresa = 0
           and  obrf_658.nivel       = p_nivel
           and  obrf_658.grupo       = p_grupo
           and (obrf_658.subgrupo    = p_subgrupo or obrf_658.subgrupo = 'XXX')
           and (obrf_658.item        = p_item     or obrf_658.item     = 'XXXXXX')
           and  obrf_658.estado      = p_estado;
         exception
            when others then
               v_periodo_max := to_date(null, '');
      end;

      if v_periodo_max is not null
      then

         p_busca_dados_obrf_658(v_periodo_max,
                                0,
                                p_nivel,
                                p_grupo,
                                'XXX',
                                'XXXXXX',
                                p_estado,
                                v_perc_desc_icms,
                                v_not_found);

         if v_not_found = 'n'
         then
            p_busca_dados_obrf_658(v_periodo_max,
                                   0,
                                   p_nivel,
                                   p_grupo,
                                   'XXX',
                                   p_item,
                                   p_estado,
                                   v_perc_desc_icms,
                                   v_not_found);

            if v_not_found = 'n'
            then
               p_busca_dados_obrf_658(v_periodo_max,
                                      0,
                                      p_nivel,
                                      p_grupo,
                                      p_subgrupo,
                                      'XXXXXX',
                                      p_estado,
                                      v_perc_desc_icms,
                                      v_not_found);

               if v_not_found = 'n'
               then
                  p_busca_dados_obrf_658(v_periodo_max,
                                         0,
                                         p_nivel,
                                         p_grupo,
                                         p_subgrupo,
                                         p_item,
                                         p_estado,
                                         v_perc_desc_icms,
                                         v_not_found);
               end if;
            end if;
         end if;
      end if;
   end if;

   if v_not_found = 's'
   then return(v_perc_desc_icms);
   else return(0.00);
   end if;

end inter_fn_desc_importados;
 

/

exec inter_pr_recompile;

