INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'pedi_f038',     0,
	1,			     'Cadastro de Exceção de Ocorrência de Cobrança Escritural');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'pedi_f038',   'pedi_menu', 
	0,             0, 
	'S',           'S', 
	'S',           'S');
	
UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Cadastro de Exceção de Ocorrência de Cobrança Escritural'
 WHERE hdoc_036.codigo_programa = 'pedi_f038'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;
