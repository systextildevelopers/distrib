INSERT INTO EMPR_007 ( PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('faturamento.diasNovoVencTitulo', 1, 'lb48458', 'fy00010', null, 0, null, null);
COMMIT;

DECLARE tmpInt number;
CURSOR parametro_c IS SELECT codigo_empresa FROM fatu_500;
BEGIN
  FOR reg_parametro IN parametro_c
  LOOP
    SELECT nvl(count(*),0)
    INTO tmpInt
    FROM fatu_500
    WHERE fatu_500.codigo_empresa = reg_parametro.codigo_empresa;

    IF(tmpInt > 0)
    THEN
      BEGIN
        INSERT INTO empr_008 (CODIGO_EMPRESA, PARAM, VAL_INT)
            VALUES (reg_parametro.codigo_empresa, 'faturamento.diasNovoVencTitulo', 0);
      END;
    END IF;
  END LOOP;
  COMMIT;
END;
/
