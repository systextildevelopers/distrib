alter table obrf_158 add (
compacta_arquivo_xml number(1) default 1 null);

comment on column obrf_158.compacta_arquivo_xml 
is 'Compactar arquivo XML.';

exec inter_pr_recompile;
/
