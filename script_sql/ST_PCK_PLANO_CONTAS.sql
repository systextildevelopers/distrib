create or replace package ST_PCK_PLANO_CONTAS as
	TYPE t_cont_530 IS TABLE OF cont_530%rowtype;
	
	function get( p_cod_plano_cta NUMBER, p_msg_erro in out varchar2) return t_cont_530;

	function exists_plano_conta(p_cod_plano_cta number) return boolean;
		
    procedure insere_plano_contas (	p_cod_plano_cta number, p_descricao varchar2, p_mascara varchar2, p_msg_erro in out varchar2);
end ST_PCK_PLANO_CONTAS;
