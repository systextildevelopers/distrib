CREATE OR REPLACE VIEW obrf_889_ABERTA_OP
(
  ORDEM_SERVICO,
  SITUACAO_ITEM, 
  NUMERO_SOLICITACAO,
  TIPO_SOLICITACAO_CONSERTO,
  TAG_ATUALIZA_CONSERTO,
  NIVEL_ESTRUTURA,
  GRUPO_ESTRUTURA,
  SUBGRUPO_ESTRUTURA,
  ITEM_ESTRUTURA,
  ORDEM_PRODUCAO,
  QTDE_TAG
)
AS 
select obrf_889.ordem_servico,
       obrf_889.situacao_item, 
       obrf_889.numero_solicitacao,
       obrf_889.tipo_solicitacao_conserto,
       obrf_889.tag_atualiza_conserto,
       obrf_889.nivel_estrutura,
       obrf_889.grupo_estrutura,
       obrf_889.subgrupo_estrutura,
       obrf_889.item_estrutura,
       obrf_889.ordem_producao,
       count(*)   as qtde_tag
from obrf_889
group by obrf_889.ordem_servico,
         obrf_889.situacao_item, 
         obrf_889.numero_solicitacao,
         obrf_889.tipo_solicitacao_conserto,
         obrf_889.tag_atualiza_conserto,
         obrf_889.nivel_estrutura,
         obrf_889.grupo_estrutura,
         obrf_889.subgrupo_estrutura,
         obrf_889.item_estrutura,
         obrf_889.ordem_producao;
/
