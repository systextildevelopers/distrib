-- Create table 
drop table estq_250_log;

create table ESTQ_250_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(20) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(20) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  id_OLD         NUMBER(9) ,  
  id_NEW         NUMBER(9) , 
  endereco_OLD   VARCHAR2(25),  
  endereco_NEW   VARCHAR2(25), 
  empresa_OLD    NUMBER(3),  
  empresa_NEW    NUMBER(3), 
  criado_por_OLD VARCHAR2(250),  
  criado_por_NEW VARCHAR2(250), 
  criado_em_OLD  TIMESTAMP(6) default current_timestamp, 
  criado_em_NEW  TIMESTAMP(6) default current_timestamp 
);
/
