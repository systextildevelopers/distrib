create or replace trigger inter_tr_basi_013_2
   after update of seq_cor, codigo_desenho
on basi_013 -- Estrutura do produto de Projeto
for each row

declare
   Pragma Autonomous_Transaction;

   v_nro_reg      number;

   v_pode_excluir number;
begin
   v_nro_reg := 0;

   begin
      commit work;
   end;

   if updating
   then
      if :new.seq_cor <> :old.seq_cor and :old.seq_cor <>  ' ' and :old.seq_cor is not null
      then
         v_pode_excluir := 0;

         --
         -- Verifica se a SEQ. COR n�o esta em outro componente ou em outra estrutura do projeto
         --
         for reg_basi_013 in (select basi_013.seq_cor
                              from basi_013
                              where basi_013.codigo_projeto      =  :new.codigo_projeto
                                and basi_013.seq_cor             <> ' '
                                and basi_013.sequencia_estrutura <> :new.sequencia_estrutura
                              group by basi_013.seq_cor)
         loop
            if reg_basi_013.seq_cor = :old.seq_cor
            then
               v_pode_excluir := 1;
            end if;
         end loop;

         --
         -- Verifica se a SEQ. COR n�o esta em algum desenho
         --
         for reg_basi_013 in (select basi_842.seq_cor
                              from basi_013, basi_842
                              where basi_013.codigo_projeto       =  :new.codigo_projeto
                                and basi_013.sequencia_estrutura  <> :new.sequencia_estrutura
                                and basi_842.seq_cor              <> ' '
                                and basi_013.codigo_desenho       = basi_842.codigo_desenho
                              group by basi_842.seq_cor)
         loop
            if reg_basi_013.seq_cor = :old.seq_cor
            then
               v_pode_excluir := 1;
            end if;
         end loop;

         if v_pode_excluir = 0
         then
            --
            --  BUSCAR TODAS AS COMBINA��ES DO PROJETO
            --
            for reg_basi_847 in (select basi_843.combinacao_item
                                 from basi_843
                                 where basi_843.codigo_projeto  = :new.codigo_projeto)
            loop
               --
               -- VERIFICAR SE EXISTE A SEQ DE COR PARA A COMBINA��O
               --
               begin
                  select count(1)
                  into v_nro_reg
                  from basi_847
                  where basi_847.codigo_projeto  = :new.codigo_projeto
                    and basi_847.combinacao_item = reg_basi_847.combinacao_item
                    and basi_847.seq_cor         = :old.seq_cor;
               end;

               if v_nro_reg = 1
               then
                  begin
                     delete from basi_847
                     where basi_847.codigo_projeto   = :new.codigo_projeto
                       and basi_847.combinacao_item  = reg_basi_847.combinacao_item
                       and basi_847.seq_cor          = :old.seq_cor;
                  end;

                  begin
                     commit work;
                  end;
               end if;
            end loop;
         end if;
      end if;

   end if;
  
end inter_tr_basi_013_2;

/

execute inter_pr_recompile;

/* versao: 3 */
