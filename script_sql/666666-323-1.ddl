alter table cpag_020 
add (
    cnpj9_portador number(9) default 0,
    cnpj4_portador number(4) default 0,
    cnpj2_portador number(2) default 0,
    tipo_sped number(1) default 0
);
