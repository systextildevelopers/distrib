alter table estq_300 add pacote_op 	number(5);
alter table estq_310 add pacote_op 	number(5);

comment on column estq_300.pacote_op 	is 'Numero do pacote (ordem de confeccao) da OP origem da movimentacao';
comment on column estq_310.pacote_op 	is 'Numero do pacote (ordem de confeccao) da OP origem da movimentacao';

exec inter_pr_recompile;
