create or replace procedure inter_pr_gera_sped_0150_pc(p_cod_empresa     IN NUMBER,
                                                       p_cod_matriz      IN  NUMBER,
                                                       p_cnpj9_empresa   IN  NUMBER,
                                                       p_cnpj4_empresa   IN  NUMBER,
                                                       p_cnpj2_empresa   IN  NUMBER,
                                                       p_dat_inicial     IN  DATE,
                                                       p_des_erro    out varchar2) is
--
-- Finalidade: Gerar a tabela sped_0150 - Clientes e Fornecedores
-- Autor.....: Edson Pio
-- Data......: 23/02/11
--
-- Hist�ricos
--
-- Data    Autor    Observa��es
--
w_estado                basi_160.estado%TYPE;
w_ddd                   basi_160.ddd%TYPE;
w_codigo_cidade_ibge    number;
w_codigo_pais           basi_160.codigo_pais%TYPE;
w_cidade                basi_160.cidade%TYPE;
w_codigo_fiscal_uf      basi_167.codigo_fiscal_uf%TYPE;
w_codigo_pais_sisc      basi_165.codigo_fiscal%TYPE;
w_cod_cid_z_franca      basi_160.cod_cidade_zona_franca%TYPE;
w_tp_pessoa_fisica      pedi_010.fisica_juridica%TYPE;
w_ind_achou             varchar2(1);
w_tip_contribuinte_icms varchar2(1);
w_erro                  EXCEPTION;
w_fornec_cliente        number;
indConsolDetalhe        number;

-- INCLUSO UNION PARA PEGAR PARTICIPANTES DAS NOTAS REFERENCIADAS
CURSOR u_sped_pc_c100 (p_cod_empresa     NUMBER,
                       p_cod_matriz      NUMBER,
                       indConsolDetalhe  NUMBER) IS
   SELECT distinct tip_entrada_saida,
          num_cnpj_9,
          num_cnpj_4,
          num_cnpj_2
   FROM   ((select distinct  sped_pc_c100.tip_entrada_saida, sped_pc_c100.num_cnpj_9, sped_pc_c100.num_cnpj_4, sped_pc_c100.num_cnpj_2
           from sped_pc_c100
           WHERE   sped_pc_c100.cod_empresa     = p_cod_empresa
             and   sped_pc_c100.cod_situacao_nota in (0,6)
             and  (not  (sped_pc_c100.cod_modelo in ('55', '65', '59')  and sped_pc_c100.tip_emissao_prop = 0) or indConsolDetalhe = 2)
             and  sped_pc_c100.cod_serie_nota not in ('ECF','CF'))
   union all
           (select distinct 'E', sped_pc_c100.num_cnpj_9_transp, sped_pc_c100.num_cnpj_4_transp, sped_pc_c100.num_cnpj_2_transp
            from sped_pc_c100
            WHERE sped_pc_c100.cod_empresa        = p_cod_empresa
              and sped_pc_c100.cod_situacao_nota in (0,6)
              and (sped_pc_c100.cod_modelo        not in ('55', '65','59') or indConsolDetalhe = 2)
              and sped_pc_c100.tip_entrada_saida <> 'E'
              and sped_pc_c100.cod_serie_nota not in ('ECF','CF')
              and sped_pc_c100.num_cnpj_9_transp  > 0)
   union all
          (select distinct 'E', sped_pc_c170.num_cnpj9_refer , sped_pc_c170.num_cnpj4_refer, sped_pc_c170.num_cnpj2_refer
           from sped_pc_c170,
                sped_pc_c100
           WHERE   sped_pc_c170.cod_empresa     = p_cod_empresa
             and   sped_pc_c170.num_cnpj9_refer > 0
             and   sped_pc_c170.cod_empresa     = sped_pc_c100.cod_empresa
             and   sped_pc_c170.num_nota_fiscal = sped_pc_c100.num_nota_fiscal
             and   sped_pc_c170.cod_serie_nota  = sped_pc_c100.cod_serie_nota
             and   sped_pc_c170.num_cnpj_9      = sped_pc_c100.num_cnpj_9
             and   sped_pc_c170.num_cnpj_4      = sped_pc_c100.num_cnpj_4
             and   sped_pc_c170.num_cnpj_2      = sped_pc_c100.num_cnpj_2
             and   sped_pc_c100.cod_modelo      not in ('55', '65') 
             and   sped_pc_c100.cod_situacao_nota in (0,6))
   union all -- F100
         (select decode(obrf_700.ind_oper, 0, 'E','S') tip_entrada_saida, obrf_700.cod_part9, obrf_700.cod_part4, obrf_700.cod_part2
          from obrf_700
          where obrf_700.cod_empresa = p_cod_empresa
            and obrf_700.mes_apur    = to_char(p_dat_inicial,'MM')
            and obrf_700.ano_apur    = to_char(p_dat_inicial,'YYYY')
            and obrf_700.cod_part9   > 0)
    union all -- 1100 e 1500
         (select 'E' tip_entrada_saida, obrf_711.cod_part_9, obrf_711.cod_part_4, obrf_711.cod_part_2
          from obrf_711
          where obrf_711.cod_empresa = p_cod_empresa
            and obrf_711.per_apu_cred_mes = to_char(p_dat_inicial,'MM')
            and obrf_711.per_apu_cred_ano = to_char(p_dat_inicial,'YYYY')
            and obrf_711.cod_part_9       > 0)
            -- 1200 e 1600
     union all
         (select 'S' tip_entrada_saida, obrf_721.cod_part9, obrf_721.cod_part4, obrf_721.cod_part2
          from obrf_721
          where obrf_721.cod_empresa = p_cod_empresa
            and obrf_721.mes_apur = to_char(p_dat_inicial,'MM')
            and obrf_721.ano_apur = to_char(p_dat_inicial,'YYYY')
            and obrf_721.cnpj9    > 0)
   )
   ORDER BY tip_entrada_saida DESC;


CURSOR u_pedi_010 (p_num_cnpj_9         number,
                   p_num_cnpj_4         NUMBER,
                   p_num_cnpj_2         NUMBER) IS
   SELECT *
   FROM   pedi_010
   WHERE  cgc_9   = p_num_cnpj_9
   AND    cgc_4   = p_num_cnpj_4
   AND    cgc_2   = p_num_cnpj_2;


CURSOR u_supr_010 (p_num_cnpj_9         number,
                   p_num_cnpj_4         NUMBER,
                   p_num_cnpj_2         NUMBER) IS
   SELECT *
   FROM   supr_010
   WHERE  fornecedor9   = p_num_cnpj_9
   AND    fornecedor4   = p_num_cnpj_4
   AND    fornecedor2   = p_num_cnpj_2;

-- A100
-- C100
-- F100 - obrf_700
-- 1101
-- 1210
-- 1501
-- 1610
--

BEGIN
   p_des_erro := NULL;
   
   indConsolDetalhe := inter_fn_get_param_int(p_cod_matriz, 'sped.indivConsol');

   FOR sped_pc_c100 IN u_sped_pc_c100 (p_cod_empresa,p_cod_matriz,indConsolDetalhe)
   LOOP
       w_ind_achou := 'N';

       -- o documento � de saida (clientes)
       IF sped_pc_c100.tip_entrada_saida = 'S'
       THEN

          -- le as informa��es do cliente
          FOR pedi_010 IN u_pedi_010 (sped_pc_c100.num_cnpj_9, sped_pc_c100.num_cnpj_4, sped_pc_c100.num_cnpj_2)
          LOOP

              w_ind_achou := 'S';

              -- le os dados da cidade, estado e pais do cliente
              BEGIN
                 SELECT basi_160.estado,                 basi_160.ddd,                to_number(to_char(basi_160.codigo_fiscal,'00000')),
                        basi_160.codigo_pais,            basi_160.cidade,             basi_167.codigo_fiscal_uf,
                        basi_165.codigo_fiscal,          basi_160.cod_cidade_zona_franca
                 INTO   w_estado,                        w_ddd,                       w_codigo_cidade_ibge,
                        w_codigo_pais,                   w_cidade,                    w_codigo_fiscal_uf,
                        w_codigo_pais_sisc,              w_cod_cid_z_franca
                 from   basi_160,
                        basi_167,
                        basi_165
                 where pedi_010.cod_cidade  = basi_160.cod_cidade
                 and   basi_160.estado      = basi_167.estado
                 and   basi_160.codigo_pais = basi_167.codigo_pais
                 and   basi_160.codigo_pais = basi_165.codigo_pais;

              EXCEPTION
                 WHEN OTHERS THEN
                    w_estado             := NULL;
                    w_ddd                := NULL;
                    w_codigo_pais_sisc   := NULL;
                    w_codigo_pais        := NULL;
                    w_cidade             := NULL;
                    w_codigo_fiscal_uf   := NULL;
                    w_codigo_cidade_ibge := NULL;
              END;

              -- se o cliente for ISENTO, seta as variaveis como n�o contribuionte e anula o campo da inscricao
              BEGIN

                 if trim(pedi_010.insc_est_cliente) = 'ISENTO'
                 then
                    w_tip_contribuinte_icms    := 'N';
                    pedi_010.insc_est_cliente  := null;
                 else
                    w_tip_contribuinte_icms := 'S';
                 end if;

                 if w_estado = 'EX'
                 then
                    w_codigo_cidade_ibge := null;
                 end if;

                 -- insere o registro do cliente
                 INSERT INTO sped_pc_0150
                    (cod_empresa
                    ,cod_matriz
                    ,num_cnpj9_empr
                    ,num_cnpj4_empr
                    ,num_cnpj2_empr
                    ,num_cnpj9
                    ,num_cnpj4
                    ,num_cnpj2
                    ,nom_participante
                    ,cod_pais_particip
                    ,num_inscri_estadual
                    ,cod_cidade_ibge
                    ,num_suframa
                    ,des_endereco
                    ,num_endereco
                    ,des_complemento
                    ,nom_bairro
                    ,sig_estado_particip
                    ,cod_estado_ibge
                    ) values
                    (p_cod_empresa                                       -- cod_empresa
                    ,p_cod_matriz
                    ,p_cnpj9_empresa
                    ,p_cnpj4_empresa
                    ,p_cnpj2_empresa
                    ,sped_pc_c100.num_cnpj_9                                        -- num_cnpj9
                    ,sped_pc_c100.num_cnpj_4                                        -- num_cnpj4
                    ,sped_pc_c100.num_cnpj_2                                        -- num_cnpj2
                    ,trim(pedi_010.nome_cliente)                                   -- nom_participante
                    ,w_codigo_pais_sisc                                          -- cod_pais_particip
                    ,REPLACE(REPLACE(REPLACE(REPLACE(pedi_010.insc_est_cliente,' '),'-'),'.'),'/')  -- num_inscri_estadual
                    ,w_codigo_cidade_ibge                                        -- cod_cidade_ibge
                    ,trim(pedi_010.nr_suframa_cli)                               -- num_suframa
                    ,trim(pedi_010.endereco_cliente)                                   -- des_endereco
                    ,trim(pedi_010.numero_imovel)                                      -- num_endereco
                    ,trim(pedi_010.complemento)                                        -- des_complemento
                    ,trim(pedi_010.bairro)                                             -- nom_bairro
                    ,trim(w_estado)                                                    --estado
                    ,w_codigo_fiscal_uf
                    );

              EXCEPTION
                 WHEN OTHERS THEN
                    p_des_erro := 'Erro na inclus�o da tabela sped_pc_0150 ' || Chr(10) ||
                                  'Cliente: ' || sped_pc_c100.num_cnpj_9 || '/' || sped_pc_c100.num_cnpj_4 || '-' || sped_pc_c100.num_cnpj_2 ||
                                  Chr(10) || SQLERRM;
                    RAISE W_ERRO;
              END;

              COMMIT;

          END LOOP;
       ELSE

          -- le os dados do fornecedor, pois o documento � de entrada
          FOR supr_010 IN u_supr_010 (sped_pc_c100.num_cnpj_9, sped_pc_c100.num_cnpj_4, sped_pc_c100.num_cnpj_2)
          LOOP

              w_ind_achou := 'S';

              -- le os dados da cidade, estado e pais do fornecedor
              BEGIN
                 SELECT basi_160.estado,                 basi_160.ddd,                to_char(basi_160.codigo_fiscal,'00000'),
                        basi_160.codigo_pais,            basi_160.cidade,             basi_167.codigo_fiscal_uf,
                        basi_165.codigo_fiscal,          basi_160.cod_cidade_zona_franca
                 INTO   w_estado,                        w_ddd,                       w_codigo_cidade_ibge,
                        w_codigo_pais,                   w_cidade,                    w_codigo_fiscal_uf,
                        w_codigo_pais_sisc,              w_cod_cid_z_franca
                 from   basi_160,
                        basi_167,
                        basi_165
                 where supr_010.cod_cidade  = basi_160.cod_cidade
                 and   basi_160.estado      = basi_167.estado
                 and   basi_160.codigo_pais = basi_167.codigo_pais
                 and   basi_160.codigo_pais = basi_165.codigo_pais;

              EXCEPTION
                 WHEN OTHERS THEN
                    w_estado             := NULL;
                    w_ddd                := NULL;
                    w_codigo_pais_sisc   := NULL;
                    w_codigo_pais        := NULL;
                    w_cidade             := NULL;
                    w_codigo_fiscal_uf   := NULL;
                    w_codigo_cidade_ibge := NULL;

              END;

              begin
                 select count(*)
                 into w_fornec_cliente
                 from sped_pc_0150
                 where cod_empresa = p_cod_empresa
                   and num_cnpj9 = supr_010.fornecedor9
                   and num_cnpj4 = supr_010.fornecedor4
                   and num_cnpj2 = supr_010.fornecedor2;
              end;

              if w_fornec_cliente = 0
              THEN

                BEGIN
                   -- verifica se o fornecedor � pessoa fisica ou juridica (pelo CNPJ)
                   if sped_pc_c100.num_cnpj_4 = 0 and (sped_pc_c100.num_cnpj_9 > 0 or sped_pc_c100.num_cnpj_2 > 0)
                   then
                      w_tp_pessoa_fisica := '1';
                   else
                      w_tp_pessoa_fisica := '2';
                   end if;

                   -- se o fornecedor for ISENTO, seta as variaveis como n�o contribuinte e anula o campo da inscricao
                   if trim(supr_010.inscr_est_forne) = 'ISENTO'
                   then
                      w_tip_contribuinte_icms  := 'N';
                      supr_010.inscr_est_forne := null;
                   else
                      w_tip_contribuinte_icms := 'S';
                   end if;

                   if w_estado = 'EX'
                   then
                      w_codigo_cidade_ibge := null;
                   end if;

                   -- insere o registro do fornecedor
                   INSERT INTO sped_pc_0150
                      (cod_empresa
                      ,cod_matriz
                      ,num_cnpj9_empr
                      ,num_cnpj4_empr
                      ,num_cnpj2_empr
                      ,num_cnpj9
                      ,num_cnpj4
                      ,num_cnpj2
                      ,nom_participante
                      ,cod_pais_particip
                      ,num_inscri_estadual
                      ,cod_cidade_ibge
                      ,num_suframa
                      ,des_endereco
                      ,num_endereco
                      ,des_complemento
                      ,nom_bairro
                      ,sig_estado_particip
                      ,cod_estado_ibge
                      ) values
                      (p_cod_empresa                                       -- cod_empresa
                      ,p_cod_matriz
                      ,p_cnpj9_empresa
                      ,p_cnpj4_empresa
                      ,p_cnpj2_empresa
                      ,sped_pc_c100.num_cnpj_9                                        -- num_cnpj9
                      ,sped_pc_c100.num_cnpj_4                                        -- num_cnpj4
                      ,sped_pc_c100.num_cnpj_2                                        -- num_cnpj2
                      ,supr_010.nome_fornecedor                                    -- nom_participante
                      ,w_codigo_pais_sisc                                          -- cod_pais_particip
                      ,ltrim(REPLACE(REPLACE(REPLACE(supr_010.inscr_est_forne,'.'),'-'),'/'),' ') -- num_inscri_estadual
                      ,w_codigo_cidade_ibge                                        -- cod_cidade_ibge
                      ,' '                                                         -- num_suframa
                      ,trim(supr_010.endereco_forne)                                     -- des_endereco
                      ,trim(supr_010.numero_imovel)                                      -- num_endereco
                      ,trim(supr_010.complemento)                                        -- des_complemento
                      ,trim(supr_010.bairro)                                             -- nom_bairro
                      ,trim(w_estado)                                                    -- estado
                      ,w_codigo_fiscal_uf
                      );

                EXCEPTION
                   WHEN OTHERS THEN
                      p_des_erro := 'Erro na inclus�o da tabela sped_pc_0150 ' || Chr(10) ||
                                    'Cliente: ' || sped_pc_c100.num_cnpj_9 || '/' || sped_pc_c100.num_cnpj_4 || '-' || sped_pc_c100.num_cnpj_2 ||
                                    Chr(10) || SQLERRM;
                      RAISE W_ERRO;
                END;
              END IF;
              COMMIT;
          END LOOP;
       END IF;
   END LOOP;
   COMMIT;

EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_pc_0150 ' || Chr(10) || p_des_erro;

END inter_pr_gera_sped_0150_pc;
/
exec inter_pr_recompile;
