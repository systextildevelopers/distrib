
CREATE OR REPLACE TRIGGER "INTER_TR_PLAN_MOV_PEDI_100" 
   after delete or update
       of DATA_ENTR_VENDA , COD_CANCELAMENTO , QTDE_TOTAL_PEDI
   on pedi_100
   for each row

declare
   v_executa_trigger         number(1);
   v_nivel                   varchar2(1);
   v_grupo                   varchar2(5);
   v_subgrupo                varchar2(3);
   v_item                    varchar2(6);
   v_ordem_planej            number(9);

   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);

begin
   v_executa_trigger := 1;

   begin
      select tmrp_625.ordem_planejamento , tmrp_625.nivel_produto    ,
             tmrp_625.grupo_produto      , tmrp_625.subgrupo_produto ,
             tmrp_625.item_produto
      into   v_ordem_planej              , v_nivel                   ,
             v_grupo                     , v_subgrupo                ,
             v_item
      from tmrp_625, tmrp_610
      where   tmrp_625.pedido_venda             = :old.pedido_venda
        and   tmrp_625.ordem_planejamento       = tmrp_610.ordem_planejamento
        and   tmrp_610.situacao_ordem           < 9
        and   tmrp_625.nivel_produto_origem     = '0'
        and   tmrp_625.grupo_produto_origem     = '00000'
        and   tmrp_625.subgrupo_produto_origem  = '000'
        and   tmrp_625.item_produto_origem      = '000000'
        and ((tmrp_625.qtde_areceber_programada > 0 and exists (select 1 from tmrp_041, pcpc_020, tmrp_630
                                                                where tmrp_041.periodo_producao   = pcpc_020.periodo_producao
                                                                  and tmrp_041.area_producao      = 1
                                                                  and tmrp_041.nr_pedido_ordem    = tmrp_630.ordem_prod_compra
                                                                  and tmrp_041.codigo_estagio     = pcpc_020.ultimo_estagio
                                                                  and tmrp_041.nivel_estrutura    = tmrp_625.nivel_produto
                                                                  and tmrp_041.grupo_estrutura    = tmrp_625.grupo_produto
                                                                  and tmrp_041.subgru_estrutura   = tmrp_625.subgrupo_produto
                                                                  and tmrp_041.item_estrutura     = tmrp_625.item_produto
                                                                  and tmrp_041.qtde_areceber      > 0
                                                                  and pcpc_020.ordem_producao     = tmrp_630.ordem_prod_compra
                                                                  and pcpc_020.cod_cancelamento   = 0
                                                                  and tmrp_630.ordem_planejamento = tmrp_610.ordem_planejamento
                                                                  and tmrp_630.area_producao      = 1)) or
              tmrp_625.qtde_areceber_programada = 0)
        and rownum = 1;
      exception when no_data_found then
            v_executa_trigger := 0;
   end;

   if v_executa_trigger = 1
   then
      -- DADOS DO LOGIN DO USUARIO

      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

      -- Alteracao na data de entrega, codigo de cancelamento e quantidade total vendida.
      if updating
      then
         -- Cancelamento do pedido
         if  :old.cod_cancelamento <> :new.cod_cancelamento
         and :old.cod_cancelamento = 0
         then
            inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         :old.pedido_venda,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE VENDA ALTERADO
                  inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  -- Pedido de venda numero {0} cancelado.
                  inter_fn_buscar_tag_composta('lb33046', to_char(:old.pedido_venda,'000000000'),
                                                            '','','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
         end if;

         -- Alteracao da quantidade pedida
         if :old.qtde_total_pedi <> :new.qtde_total_pedi
         then
            inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         :old.pedido_venda,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE VENDA ALTERADO
                  inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  -- Quantidade pedida do pedido de venda alterada.
                  inter_fn_buscar_tag('lb33047', ws_locale_usuario,ws_usuario_systextil));
         end if;

         if :old.data_entr_venda <>  :new.data_entr_venda
         then
            inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         :old.pedido_venda,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE VENDA ALTERADO
                  inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  -- Data embarque do pedido de venda alterada de {0} para {1}.
                  inter_fn_buscar_tag_composta('lb33044', to_char(:old.data_entr_venda,'MM/DD/YYYY'),
                                                          to_char(:new.data_entr_venda,'MM/DD/YYYY'),
                                                          '','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
         end if;
      end if;

      -- Eliminacao de um pedido de venda.
      if deleting
      then
            inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         :old.pedido_venda,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE VENDA ALTERADO
                  inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                 -- Pedido de venda numero {0} eliminado.
                 inter_fn_buscar_tag_composta('lb34233', to_char(:old.pedido_venda,'000000000'),
                                                         '','','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
      end if;
   end if;

end inter_tr_plan_mov_pedi_100;

-- ALTER TRIGGER "INTER_TR_PLAN_MOV_PEDI_100" ENABLE
 

/

exec inter_pr_recompile;

