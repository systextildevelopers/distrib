create table estq_710 (
	documento				number(9)		not null,
	data_montagem			date,
	nivel_entrada			varchar2(1),
	grupo_entrada			varchar2(5),
	subgrupo_entrada		varchar2(3),
	item_entrada			varchar2(6),
	quantidade				number(12,3),
	deposito_entrada		number(3),
	transacao_entrada		number(3),
	status					number(1)	
);

comment on table estq_710 						is 'Tabela para Montagem de Produtos (Entrada)';

comment on column estq_710.documento 			is 'Número do documento da montagem';
comment on column estq_710.data_montagem 		is 'Data da montagem';
comment on column estq_710.nivel_entrada 		is 'Nivel do produto montado (entrada)';
comment on column estq_710.grupo_entrada 		is 'Grupo do produto montado (entrada)';
comment on column estq_710.subgrupo_entrada 	is 'Subgrupo do produto montado (entrada)';
comment on column estq_710.item_entrada 		is 'Item do produto montado (entrada)';
comment on column estq_710.quantidade 			is 'Quantidade montada (entrada)';
comment on column estq_710.deposito_entrada		is 'Deposito de entrada do produto montado';
comment on column estq_710.transacao_entrada 	is 'Transacao de entrada do produto montado';
comment on column estq_710.status 				is 'Status da montagem: 0-Nao montado ainda, 1-Montagem Realizada, 2-Montagem Cancelada/Desfeita';

alter table estq_710 add constraint estq_710_pk primary key (documento);

create sequence seq_estq_710 
	minvalue 1 
	maxvalue 999999999
	increment by 1 
	start with 1 
	cache 20 
	nocycle
;
/
