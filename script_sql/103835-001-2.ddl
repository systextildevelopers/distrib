alter table pedi_050 add ind_canhoto_cheque varchar2(1) default 'N';

comment on column pedi_050.ind_canhoto_cheque is 'S - Imprime canhoto no cheque ou N - Nao imprime';

exec inter_pr_recompile;
