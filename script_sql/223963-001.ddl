alter table fatu_504 add conc_def_saldos varchar2(1) default 'N';

comment on column fatu_504.conc_def_saldos is 'Considerar defeitos nos saldos dos Rolos (empr_f833)';
