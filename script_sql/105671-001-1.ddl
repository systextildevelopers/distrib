create table pcpc_700 (
id                   number(9) default 0,
ordem_producao       number(9) default 0,
periodo_producao     number(9) default 0,
ordem_confeccao      number(9) default 0,
codigo_estagio       number(9) default 0,
seq_operacao         number(9) default 0,
dt_incial            date,
dt_final             date,
dt_producao          date,
ind_estagio_gargalo  number(1) default 0);

alter table pcpc_700 add constraint pk_pcpc_700 primary key (id);

alter table pcpc_700 add constraint uniq_pcpc_700 unique(ordem_producao, periodo_producao, ordem_confeccao,codigo_estagio);

comment on column pcpc_700.id is 'Codigo chave sequencial';
comment on column pcpc_700.ordem_producao is 'Numero da ordem de producao';
comment on column pcpc_700.periodo_producao is 'Codigo do periodo de producao';
comment on column pcpc_700.ordem_confeccao is 'Ordem de confeccao ou pacote';
comment on column pcpc_700.codigo_estagio is 'Codigo do estagio de producao';
comment on column pcpc_700.dt_incial is 'Data incial da ordem';
comment on column pcpc_700.dt_final is 'Data final da ordem';
comment on column pcpc_700.dt_producao is 'Data em que o estagio sera produzido';
comment on column pcpc_700.ind_estagio_gargalo is '1- Indica se o estagio sera estagio gargalo ou 0 - N�o';
comment on column pcpc_700.seq_operacao is 'Indica sequencia de operacao do roteiro';


create sequence id_pcpc_700 start with 1 increment by 1;

exec inter_pr_recompile ;
