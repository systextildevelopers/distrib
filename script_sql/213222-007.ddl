alter table manu_001 add (cod_parte number(4) default 0 not null, cod_comp number(4) default 0 not null);

ALTER TABLE manu_001
DROP CONSTRAINT PK_MANU_001;
create index PK_MANU_001 on manu_001 (solicitacao,cod_empresa,grupo_maquina,subgru_maquina,numero_maquina);

drop index PK_MANU_001;

ALTER TABLE manu_001
ADD CONSTRAINT PK_MANU_001 PRIMARY KEY
(solicitacao,cod_empresa,grupo_maquina,subgru_maquina,numero_maquina,cod_parte,cod_comp);
commit;

alter table manu_004 add (cod_parte number(4) default 0 not null, cod_comp number(4) default 0 not null);

ALTER TABLE manu_004
DROP CONSTRAINT PK_MANU_004;
create index PK_MANU_004 on manu_004 (grupo_maquina,subgru_maquina,numero_maquina,c_custo_resp,tipo_problema);

drop index PK_MANU_004;

ALTER TABLE manu_004
ADD CONSTRAINT PK_MANU_004 PRIMARY KEY
(grupo_maquina,subgru_maquina,numero_maquina,c_custo_resp,tipo_problema,cod_parte,cod_comp);

commit;
