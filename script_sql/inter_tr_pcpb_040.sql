
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_040" 
   before delete or
          insert or
          update of data_inicio, hora_inicio, data_termino, hora_termino
   on pcpb_040
   for each row

declare
   t_periodo_producao        pcpb_010.periodo_producao%type;
   t_ordem_tingimento        pcpb_010.ordem_tingimento%type;
   t_ultimo_estagio          pcpb_010.ultimo_estagio%type;
   v_bloq_est_tinturaria     empr_002.bloq_est_tinturaria%type;
   v_estagio_tintur          empr_001.estagio_tintur%type;
   v_nr_quimicos             number;
   v_ordem_ting              number;
   v_estagio_prepara         number;
   v_seq_operacao_preparacao number;
   v_tem_registro            number;
   v_codigo_empresa          number;
   v_tipo_ordem              varchar(1);
begin
   if inserting and :new.codigo_estagio = 0
   then
      raise_application_error(-20000,'Estagio de Producao da Ordem de Beneficiamento nao pode ser zero, esta operacao sera cancelada.');
   end if;
   
   if updating
   then
      -- Busca o periodo e tipo da ordem de producao da OB.
      begin
         select pcpb_010.periodo_producao, pcpb_010.ordem_tingimento,
                pcpb_010.ultimo_estagio
         into   t_periodo_producao,        t_ordem_tingimento,
                t_ultimo_estagio
         from pcpb_010
         where pcpb_010.ordem_producao = :old.ordem_producao;
         exception
         when others then
            t_periodo_producao := 0;
            raise_application_error(-20000,'Capa da Ordem de Beneficiamento nao cadastrada (PCPB_010).');
      end;

      -- Se for a ultima sequencia do estagio e se for informada a data de termino
      -- atualiza a quantidade reservada para zero, ou seja nao existe mais reserva
      -- para este estagio.
      if  :old.seq_estagio   = 9
      and :old.data_termino is null
      and :new.data_termino is not null
      then
         update tmrp_041
         set tmrp_041.qtde_reservada = 0.00
         where tmrp_041.periodo_producao    = t_periodo_producao
           and tmrp_041.area_producao       = 2
           and tmrp_041.nr_pedido_ordem     = :old.ordem_producao
           and tmrp_041.codigo_estagio      = :old.codigo_estagio
           and tmrp_041.sequencia_estrutura > 0;

         update pcpt_040
         set pcpt_040.situacao = 1
         where pcpt_040.ordem_beneficiamento = :old.ordem_producao;
      end if;

      -- Se for a ultima sequencia do ultimo estagio estagio e se for informada a data de termino
      -- atualiza a quantidade reservada para zero, ou seja nao existe mais reserva
      -- para a ordem. CASO NECESSARIO ALTERAR FALAR COM FABIO K.
      if :old.codigo_estagio = t_ultimo_estagio and
         :old.seq_estagio    = 9                 and
         :old.data_termino is null              and
         :new.data_termino is not null
      then
         update tmrp_141
            set tmrp_141.qtde_reservada = 0.00
         where tmrp_141.ordem_producao = :old.ordem_producao;
      end if;

      -- Se for a ultima sequencia do estagio e se for desfeita a baixa do estagio
      -- retorna a quantidade reservada que existia anteriormente.
      if  :old.seq_estagio   = 9
      and :old.data_termino is not null
      and :new.data_termino is null
      then
         -- Inicio do loop para reatualizacao da quantidade reservada.
         for reg_pcpb_020 in (select sequencia, qtde_quilos_prog
                              from pcpb_020
                              where pcpb_020.ordem_producao = :old.ordem_producao)
         loop
            update tmrp_041
            set tmrp_041.qtde_reservada = tmrp_041.qtde_reservada + (reg_pcpb_020.qtde_quilos_prog * tmrp_041.consumo)
            where tmrp_041.periodo_producao    = t_periodo_producao
              and tmrp_041.area_producao       = 2
              and tmrp_041.nr_pedido_ordem     = :old.ordem_producao
              and tmrp_041.seq_pedido_ordem    = reg_pcpb_020.sequencia
              and tmrp_041.codigo_estagio      = :old.codigo_estagio
              and tmrp_041.sequencia_estrutura > 0;
         end loop;
      end if;
   end if;

   -- Logica para gravar a hora temino e hora inicio com uma data valida.
   -- A data definida '16/11/1989', foi definida com base da data de gravacao padrao do Vision.
   if (updating  and :new.hora_inicio is not null)
   or (inserting and :new.hora_inicio is not null)
   then
      :new.hora_inicio := to_date('16/11/1989 '||to_char(:new.hora_inicio,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if (updating  and :new.hora_termino is not null)
   or (inserting and :new.hora_termino is not null)
   then
      :new.hora_termino := to_date('16/11/1989 '||to_char(:new.hora_termino,'HH24:MI'),'DD/MM/YYYY HH24:MI');
   end if;

   if updating
   then
      begin
         select empr_001.estagio_prepara
         into v_estagio_prepara
         from empr_001;
      end;

      begin
         select nvl(max(pcpb_015.seq_operacao),0)
         into  v_seq_operacao_preparacao
         from pcpb_015
         where pcpb_015.ordem_producao   = :new.ordem_producao
           and pcpb_015.codigo_estagio   = v_estagio_prepara;
      exception when OTHERS then
         v_seq_operacao_preparacao := 0;
      end;

      v_tem_registro := 0;

      begin
         select nvl(count(*),0)
         into  v_tem_registro
         from pcpb_015
         where pcpb_015.ordem_producao   = :new.ordem_producao
           and pcpb_015.codigo_estagio   = :new.codigo_estagio
           and pcpb_015.seq_operacao     < v_seq_operacao_preparacao;
      exception when OTHERS then
         v_tem_registro := 0;
      end;

      if v_tem_registro > 0
      then
         for itens_ordem_020 in (select pcpb_020.pano_sbg_nivel99,   pcpb_020.pano_sbg_grupo,
                                        pcpb_010.periodo_producao
                                 from  pcpb_020, pcpb_010
                                 where pcpb_010.ordem_producao = :old.ordem_producao
                                   and pcpb_020.ordem_producao = pcpb_010.ordem_producao
                                 group by pcpb_020.pano_sbg_nivel99,   pcpb_020.pano_sbg_grupo,
                                        pcpb_010.periodo_producao)
         loop
            v_codigo_empresa := 0;

            begin
               select pcpc_010.codigo_empresa
               into   v_codigo_empresa
               from pcpc_010
               where pcpc_010.area_periodo     = 2
                 and pcpc_010.periodo_producao = itens_ordem_020.periodo_producao;
            exception when others then
               v_codigo_empresa := 0;
            end;

            inter_pr_valida_sit_projeto(itens_ordem_020.pano_sbg_nivel99,
                                        itens_ordem_020.pano_sbg_grupo,
                                        v_codigo_empresa);
         end loop;
      end if;
   end if;

   -- Consistencia da baixa dos produtos quimicos pesados manualmente
   if updating and :new.data_inicio is not null
   then
      begin
         select empr_002.bloq_est_tinturaria
         into v_bloq_est_tinturaria
         from empr_002;
      end;

      begin
        select empr_001.estagio_tintur
        into v_estagio_tintur
        from empr_001;
      end;

      begin
         select pcpb_010.ordem_tingimento
         into v_ordem_ting
         from pcpb_010
         where pcpb_010.ordem_producao = :new.ordem_producao;
      end;

      if v_bloq_est_tinturaria = 1 and v_estagio_tintur = :new.codigo_estagio
      then
         begin
             select pcpb_100.tipo_ordem
             into   v_tipo_ordem
             from pcpb_100
             where pcpb_100.ordem_agrupamento = v_ordem_ting
             and   pcpb_100.ordem_producao    = :new.ordem_producao;
         exception when others then
            v_tipo_ordem := '';
         end;
         
        --Alterando por conta que no tipo 4 a ordem de agrupamento � a ordem de reprocesso TICKET 247327
         begin
            select count(*)
            into v_nr_quimicos
            from pcpb_080
            where (case
                     when v_tipo_ordem = 4 then
                     pcpb_080.ordem_reprocesso
                     else
                     pcpb_080.ordem_producao
                  end) = v_ordem_ting
               and pcpb_080.pesar_produto = 1
               and pcpb_080.tipo_ordem = v_tipo_ordem
               and pcpb_080.data_pesagem is null;
         end;

         if v_nr_quimicos > 0
         then
            raise_application_error(-20000,chr(10)||chr(10)||'Existem produtos configurados para pesagem via balanca na baixa da tinturaria, que ainda nao foram pesados, estao sem data de pesagem informada.'||chr(10)||'Execute a pesagem dos produtos faltantes e tente apontar o estagio novamente.'||chr(10)||chr(10));
         end if;
      end if;
   end if;

   if updating and :new.data_termino is not null
   then
   	  begin
         select empr_002.bloq_est_tinturaria
         into v_bloq_est_tinturaria
         from empr_002;
      end;

      begin
        select empr_001.estagio_tintur
        into v_estagio_tintur
        from empr_001;
      end;

      begin
         select pcpb_010.ordem_tingimento
         into v_ordem_ting
         from pcpb_010
         where pcpb_010.ordem_producao = :new.ordem_producao;
      end;

      if v_bloq_est_tinturaria = 2 and v_estagio_tintur = :new.codigo_estagio
      then
         begin
             select pcpb_100.tipo_ordem
             into   v_tipo_ordem
             from pcpb_100
             where pcpb_100.ordem_agrupamento = v_ordem_ting
             and   pcpb_100.ordem_producao    = :new.ordem_producao;
         exception when others then
            v_tipo_ordem := '';
         end;
         
         begin
            select count(*)
            into v_nr_quimicos
            from pcpb_080
            where pcpb_080.ordem_producao = v_ordem_ting
              and pcpb_080.pesar_produto  = 1
              and pcpb_080.tipo_ordem     = v_tipo_ordem
              and pcpb_080.data_pesagem is null;
         end;

         if v_nr_quimicos > 0
         then
            raise_application_error(-20000,chr(10)||chr(10)||'Existem produtos configurados para pesagem via balanca na baixa da tinturaria, que ainda nao foram pesados, estao sem data de pesagem informada.'||chr(10)||'Execute a pesagem dos produtos faltantes e tente apontar o estagio novamente.'||chr(10)||chr(10));
         end if;
      end if;
      
      -- Atualiza EXEC_015
      begin
         for itens_ordem_020 in (select pcpb_020.pano_sbg_nivel99,   pcpb_020.pano_sbg_grupo,
                                        pcpb_020.pano_sbg_subgrupo,  sum(pcpb_020.qtde_quilos_real) qtde_preparada
                                 from  pcpb_020
                                 where pcpb_020.ordem_producao = :new.ordem_producao
                                 group by pcpb_020.pano_sbg_nivel99,
                                          pcpb_020.pano_sbg_grupo,
                                          pcpb_020.pano_sbg_subgrupo)
         loop
            inter_pr_alimenta_exec_015(:new.data_termino,              0,
                                       0,                              itens_ordem_020.pano_sbg_nivel99,
                                       itens_ordem_020.pano_sbg_grupo, itens_ordem_020.pano_sbg_subgrupo,
                                       :new.codigo_estagio,            :new.turno_producao,
                                       0.000,                          itens_ordem_020.qtde_preparada);
         end loop;
      end;
   end if;

   if (deleting and :old.data_termino is not null) or
      (updating and :old.data_termino is not null and :new.data_termino is null)
   then
      -- Atualiza EXEC_015 (Estorna)
      begin
         for itens_ordem_020 in (select pcpb_020.pano_sbg_nivel99,   pcpb_020.pano_sbg_grupo,
                                        pcpb_020.pano_sbg_subgrupo,  sum(pcpb_020.qtde_quilos_real) qtde_preparada
                                 from  pcpb_020
                                 where pcpb_020.ordem_producao = :old.ordem_producao
                                 group by pcpb_020.pano_sbg_nivel99,
                                          pcpb_020.pano_sbg_grupo,
                                          pcpb_020.pano_sbg_subgrupo)
         loop
            inter_pr_alimenta_exec_015(:old.data_termino,              0,
                                       0,                              itens_ordem_020.pano_sbg_nivel99,
                                       itens_ordem_020.pano_sbg_grupo, itens_ordem_020.pano_sbg_subgrupo,
                                       :old.codigo_estagio,            :old.turno_producao,
                                       0.000,                          (itens_ordem_020.qtde_preparada * -1));

         end loop;
      end;
   end if;

end inter_tr_pcpb_040;

-- ALTER TRIGGER "INTER_TR_PCPB_040" ENABLE
 

/

exec inter_pr_recompile;

