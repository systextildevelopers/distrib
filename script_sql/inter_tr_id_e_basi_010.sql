
  CREATE OR REPLACE TRIGGER "INTER_TR_ID_E_BASI_010" 
before insert on E_BASI_010
for each row

declare
  v_id_registro number;
begin
   select SEQ_EXPORTACAO.nextval into v_id_registro from dual;
   :new.id_registro     := v_id_registro;
end;
-- ALTER TRIGGER "INTER_TR_ID_E_BASI_010" DISABLE
 

/

exec inter_pr_recompile;

