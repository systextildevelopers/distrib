declare 
cursor parametro_c is select codigo_empresa from fatu_500;
tmpInt number;

begin
  for reg_parametro in parametro_c
  loop
    select count(*) into tmpInt
    from empr_008
    where empr_008.codigo_empresa = reg_parametro.codigo_empresa
      and empr_008.param = 'obrf.gerarTitulosDataProibitiva' ;
    if(tmpInt = 0)
    then
      begin
        insert into empr_008 (
          codigo_empresa, param, val_int
        ) values (
          reg_parametro.codigo_empresa, 'obrf.gerarTitulosDataProibitiva', 2);
      end;
     end if; 
  end loop;
  commit;
end;
/

exec inter_pr_recompile;
