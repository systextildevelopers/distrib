
  CREATE OR REPLACE PROCEDURE "INTER_PR_SEQUENCIAMENTO_OT" 
  (p_tipo_alocacao   in  number,
   p_tipo_recurso    in  number,
   p_nivel_produto   in  varchar2,
   p_data_inicial    in  date,
   p_data_final      in  date,
   p_codigo_estagio  in  number )
is
   v_data_producao_aux  date;
   v_codigo_recurso_aux varchar2(20);
   v_prioridade         number;
   v_ordem              number;
begin

   v_data_producao_aux := null ;
   v_codigo_recurso_aux := null ;
   v_prioridade := 0 ;
   v_ordem := 0 ;

   for reg_tmrp_650 in (select tmrp_650.codigo_recurso, tmrp_650.data_inicio,tmrp_650.ordem_trabalho
                        from tmrp_650
                        where tmrp_650.tipo_alocacao = p_tipo_alocacao
                          and tmrp_650.tipo_recurso = p_tipo_recurso
                          and tmrp_650.nivel_produto = p_nivel_produto
                          and (tmrp_650.codigo_recurso is not null and
                               trim(tmrp_650.codigo_recurso) is not null )
                          and tmrp_650.codigo_estagio = p_codigo_estagio
                          and tmrp_650.data_inicio between p_data_inicial and p_data_final
                        order by tmrp_650.codigo_recurso,tmrp_650.data_inicio,tmrp_650.hora_inicio )
   loop

     if v_data_producao_aux is null or v_data_producao_aux  <> reg_tmrp_650.data_inicio or
        v_codigo_recurso_aux is null or v_codigo_recurso_aux <> reg_tmrp_650.codigo_recurso
     then
        v_data_producao_aux := reg_tmrp_650.data_inicio;
        v_codigo_recurso_aux := reg_tmrp_650.codigo_recurso;
        v_prioridade := 0 ;
     end if;

     v_prioridade := v_prioridade + 1 ;
     v_ordem := reg_tmrp_650.ordem_trabalho ;

     update pcpb_100
        set prioridade_producao = v_prioridade
     where pcpb_100.ordem_agrupamento = reg_tmrp_650.ordem_trabalho ;

   end loop; --- tmrp_650

end inter_pr_sequenciamento_ot;

 

/

exec inter_pr_recompile;

