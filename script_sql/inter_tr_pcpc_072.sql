
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_072" 
   before insert on pcpc_072
   for each row

declare
   v_tipo_ordem integer;

begin
   if inserting
   then
      begin
         if :new.ordem_producao > 0
         then
            begin

               begin
                  select pcpc_070.tipo_ordem into v_tipo_ordem
                  from pcpc_070
                  where pcpc_070.ordem_agrupamento = :new.ordem_agrupamento;
                  exception
                    when others then
                      v_tipo_ordem := 4;
               end;

               if v_tipo_ordem = 4
               then
                  begin
                     update pcpc_020
                        set pcpc_020.ordem_agrup_corte = :new.ordem_agrupamento
                      where pcpc_020.ordem_producao    = :new.ordem_producao;
                      exception
                        when others then
                          null;
                  end;
               end if;

            end;
         end if;
      end;
   end if;
end inter_tr_pcpc_072;
-- ALTER TRIGGER "INTER_TR_PCPC_072" ENABLE
 

/

exec inter_pr_recompile;

