alter table pedi_114
modify  pedido_venda  number(9);

alter table pedi_119
modify  pedido_venda  number(9);

alter table fatu_780
modify  pedido  number(9);

alter table pedi_250
modify  pedido_venda  number(9);

alter table pedi_999
modify  pedido  number(9);

alter table  rcnb_910
modify  pedido_venda  number(9);

alter table  tmrp_622
modify  pedido_venda  number(9);

alter table  tmrp_625
modify  pedido_venda  number(9);

alter table  tmrp_630
modify  pedido_venda  number(9);

alter table  tmrp_650
modify  pedido_venda  number(9);

alter table pcpb_200
modify  pedido  number(9);

/
exec inter_pr_recompile;
