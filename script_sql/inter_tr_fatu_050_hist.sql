CREATE OR REPLACE TRIGGER "INTER_TR_FATU_050_HIST" 
   after insert or
         delete or
         update of  natop_nf_nat_oper,
                    natop_nf_est_oper,
                    cond_pgto_venda,
                    data_emissao,
                    data_base_fatur,
                    seq_end_entr,
                    cod_rep_cliente,
                    perc_repres,
                    seq_end_cobr,
                    cod_vendedor,
                    perc_vendedor,
                    transpor_forne9,
                    transpor_forne4,
                    transpor_forne2,
                    transp_redespacho9,
                    transp_redespacho4,
                    transp_redespacho2,
                    codigo_embalagem,
                    qtde_embalagens,
                    col_tabela,
                    mes_tabela,
                    seq_tabela,
                    peso_liquido,
                    peso_bruto,
                    valor_despesas,
                    valor_frete_nfis,
                    valor_seguro,
                    valor_iss,
                    base_icms,
                    valor_icms,
                    valor_ipi,
                    situacao_nfisc,
                    desconto1,
                    vlr_desc_especial,
                    encargos,
                    historico_cont,
                    num_contabil,
                    numero_danf_nfe,
                    cod_status,
                    msg_status,
                    cod_solicitacao_nfe,
                    nr_recibo,
                    nr_protocolo,
                    chave_contingencia,
                    cod_canc_nfisc,
                    qtde_itens,
                    quantidade,
                    desconto2,
                    desconto3,
                    valor_itens_nfis,
                    valor_encar_nota,
                    valor_desc_nota,
                    valor_desconto,
                    valor_icms_frete,
                    base_icms_sub,
                    valor_icms_sub,
                    valor_suframa,
                    flag_devolucao,
                    flag_contabil,
                    nr_cupom,
                    situacao_edi,
                    status_frete,
                    perc_iss,
                    num_serie_ecf,
                    num_intervencao,
                    ano_inutilizacao,
                    nr_final_inut,
                    status_impressao_danfe,
                    nfe_contigencia

   on fatu_050
   for each row



-- Finalidade: Registrar altera��es feitas na tabela fatu_050 - notas fiscais de sa�da
-- Autor.....: Aline Blanski
-- Data......: 07/06/10
--
-- Hist�ricos de altera��es na trigger
--
-- Data      Autor           SS          Observa��es
-- 12/07/10  Aline Blanski   58329/001   Adicionado campos na verifica��o da trigger.
-- 30/05/11  Vanderlei       59924/001   Realizado altera��es conforme doc da SS.

declare
   ws_tipo_ocorr               varchar2 (1);
   ws_usuario_rede             varchar2(20);
   ws_maquina_rede             varchar2(40);
   ws_aplicativo               varchar2(20);
   ws_sid                      number(9);
   ws_empresa                  number(3);
   ws_usuario_systextil        varchar2(250);
   ws_locale_usuario           varchar2(5);
   ws_nome_programa            varchar2(20);
   ws_codigo_empresa_new       NUMBER   (3) default 0;
   ws_num_nota_fiscal_new      NUMBER   (9) default 0;
   ws_serie_nota_fisc_new      VARCHAR2 (3) default '';
   ws_especie_docto_new        VARCHAR2 (5) default '';
   ws_natop_nf_nat_oper_old    NUMBER   (3) default 0;
   ws_natop_nf_nat_oper_new    NUMBER   (3) default 0;
   ws_natop_nf_est_oper_old    VARCHAR2 (2) default '';
   ws_natop_nf_est_oper_new    VARCHAR2 (2) default '';
   ws_cond_pgto_venda_old      NUMBER   (3) default 0;
   ws_cond_pgto_venda_new      NUMBER   (3) default 0;
   ws_data_emissao_old         DATE;
   ws_data_emissao_new         DATE;
   ws_data_saida_new           DATE;
   ws_hora_saida_new           DATE;
   ws_data_base_fatur_old      DATE;
   ws_data_base_fatur_new      DATE;
   ws_cgc_9_new                NUMBER   (9) default 0;
   ws_cgc_4_new                NUMBER   (4) default 0;
   ws_cgc_2_new                NUMBER   (2) default 0;
   ws_seq_end_entr_old         NUMBER   (3) default 0;
   ws_seq_end_entr_new         NUMBER   (3) default 0;
   ws_cod_rep_cliente_old      NUMBER   (5) default 0;
   ws_cod_rep_cliente_new      NUMBER   (5) default 0;
   ws_perc_repres_old          NUMBER   (6,2)default 0.00;
   ws_perc_repres_new          NUMBER   (6,2)default 0.00;
   ws_seq_end_cobr_old         NUMBER   (3) default 0;
   ws_seq_end_cobr_new         NUMBER   (3) default 0;
   ws_cod_vendedor_old         NUMBER   (5) default 0;
   ws_cod_vendedor_new         NUMBER   (5) default 0;
   ws_perc_vendedor_old        NUMBER   (6,2)default 0.00;
   ws_perc_vendedor_new        NUMBER   (6,2)default 0.00;
   ws_transpor_forne9_old      NUMBER   (9) default 0;
   ws_transpor_forne9_new      NUMBER   (9) default 0;
   ws_transpor_forne4_old      NUMBER   (4) default 0;
   ws_transpor_forne4_new      NUMBER   (4) default 0;
   ws_transpor_forne2_old      NUMBER   (2) default 0;
   ws_transpor_forne2_new      NUMBER   (2) default 0;
   ws_transp_redespacho9_old   NUMBER   (9) default 0;
   ws_transp_redespacho9_new   NUMBER   (9) default 0;
   ws_transp_redespacho4_old   NUMBER   (4) default 0;
   ws_transp_redespacho4_new   NUMBER   (4) default 0;
   ws_transp_redespacho2_old   NUMBER   (2) default 0;
   ws_transp_redespacho2_new   NUMBER   (2) default 0;
   ws_codigo_embalagem_old     NUMBER   (3) default 0;
   ws_codigo_embalagem_new     NUMBER   (3) default 0;
   ws_qtde_embalagens_old      NUMBER   (4) default 0;
   ws_qtde_embalagens_new      NUMBER   (4) default 0;
   ws_col_tabela_old           NUMBER   (2) default 0;
   ws_col_tabela_new           NUMBER   (2) default 0;
   ws_mes_tabela_old           NUMBER   (2) default 0;
   ws_mes_tabela_new           NUMBER   (2) default 0;
   ws_seq_tabela_old           NUMBER   (2) default 0;
   ws_seq_tabela_new           NUMBER   (2) default 0;
   ws_peso_liquido_old         NUMBER   (12,3)default 0.0;
   ws_peso_liquido_new         NUMBER   (12,3)default 0.0;
   ws_peso_bruto_old           NUMBER   (12,3)default 0.0;
   ws_peso_bruto_new           NUMBER   (12,3)default 0.0;
   ws_valor_despesas_old       NUMBER   (15,2)default 0.0;
   ws_valor_despesas_new       NUMBER   (15,2)default 0.0;
   ws_valor_frete_nfis_old     NUMBER   (15,2)default 0.0;
   ws_valor_frete_nfis_new     NUMBER   (15,2)default 0.0;
   ws_valor_seguro_old         NUMBER   (15,2)default 0.0;
   ws_valor_seguro_new         NUMBER   (15,2)default 0.0;
   ws_valor_iss_old            NUMBER   (15,2)default 0.0;
   ws_valor_iss_new            NUMBER   (15,2)default 0.0;
   ws_base_icmm_old            NUMBER   (15,2)default 0.0;
   ws_base_icmm_new            NUMBER   (15,2)default 0.0;
   ws_valor_icms_old           NUMBER   (15,2)default 0.0;
   ws_valor_icms_new           NUMBER   (15,2)default 0.0;
   ws_valor_ipi_old            NUMBER   (15,2)default 0.0;
   ws_valor_ipi_new            NUMBER   (15,2)default 0.0;
   ws_situacao_nfisc_old       NUMBER   (1) default 0;
   ws_situacao_nfisc_new       NUMBER   (1) default 0;
   ws_desconto1_old            NUMBER   (6,2) default 0.0;
   ws_desconto1_new            NUMBER   (6,2) default 0.0;
   ws_vlr_desc_especial_old    NUMBER   (15,2)default 0.0;
   ws_vlr_desc_especial_new    NUMBER   (15,2)default 0.0;
   ws_encargos_old             NUMBER   (9,5) default 0.0;
   ws_encargos_new             NUMBER   (9,5) default 0.0;
   ws_historico_cont_old       NUMBER   (4) default 0;
   ws_historico_cont_new       NUMBER   (4) default 0;
   ws_num_contabil_old         NUMBER   (9) default 0;
   ws_num_contabil_new         NUMBER   (9) default 0;
   ws_numero_danf_nfe_old      VARCHAR2 (44)default '';
   ws_numero_danf_nfe_new      VARCHAR2 (44)default '';
   ws_cod_status_new           VARCHAR2(5)  default ' ';
   ws_cod_status_old           VARCHAR2(5)  default ' ';
   ws_msg_status_new           VARCHAR2(4000)default' ';
   ws_msg_status_old           VARCHAR2(4000)default' ';
   ws_cod_solicitacao_nfe_new  NUMBER(9)     default 0 ;
   ws_cod_solicitacao_nfe_old  NUMBER(9)     default 0 ;
   ws_nr_recibo_new            VARCHAR2(20)  default ' ';
   ws_nr_recibo_old            VARCHAR2(20)  default ' ';
   ws_nr_protocolo_new         VARCHAR2(20)  default ' ';
   ws_nr_protocolo_old         VARCHAR2(20)  default ' ';
   ws_chave_contingencia_new   VARCHAR2(36)  default '';
   ws_chave_contingencia_old   VARCHAR2(36)  default '';
   ws_cod_canc_nfisc_new          NUMBER(2)     default 0;
   ws_cod_canc_nfisc_old          NUMBER(2)     default 0;
   ws_qtde_itens_new              NUMBER(5)     default 0;
   ws_qtde_itens_old              NUMBER(5)     default 0;
   ws_quantidade_new              NUMBER(15,3)  default 0.0;
   ws_quantidade_old              NUMBER(15,3)  default 0.0;
   ws_desconto2_new               NUMBER(6,2)   default 0.0;
   ws_desconto2_old               NUMBER(6,2)   default 0.0;
   ws_desconto3_new               NUMBER(6,2)   default 0.0;
   ws_desconto3_old               NUMBER(6,2)   default 0.0;
   ws_valor_itens_nfis_new        NUMBER(13,2)  default 0.0;
   ws_valor_itens_nfis_old        NUMBER(13,2)  default 0.0;
   ws_valor_encar_nota_new        NUMBER(15,2)  default 0.0;
   ws_valor_encar_nota_old        NUMBER(15,2)  default 0.0;
   ws_valor_desc_nota_new         NUMBER(15,2)  default 0.0;
   ws_valor_desc_nota_old         NUMBER(15,2)  default 0.0;
   ws_valor_desconto_new          NUMBER(15,2)  default 0.0;
   ws_valor_desconto_old          NUMBER(15,2)  default 0.0;
   ws_valor_icms_frete_new        NUMBER(15,2)  default 0.0;
   ws_valor_icms_frete_old        NUMBER(15,2)  default 0.0;
   ws_base_icms_sub_new           NUMBER(15,2)  default 0.0;
   ws_base_icms_sub_old           NUMBER(15,2)  default 0.0;
   ws_valor_icms_sub_new          NUMBER(15,2)  default 0.0;
   ws_valor_icms_sub_old          NUMBER(15,2)  default 0.0;
   ws_valor_suframa_new           NUMBER(15,2)  default 0.0;
   ws_valor_suframa_old           NUMBER(15,2)  default 0.0;
   ws_flag_devolucao_new          NUMBER(1)     default 0;
   ws_flag_devolucao_old          NUMBER(1)     default 0;
   ws_flag_contabil_new           NUMBER(1)     default 0;
   ws_flag_contabil_old           NUMBER(1)     default 0;
   ws_nr_cupom_new                NUMBER(9)     default 0;
   ws_nr_cupom_old                NUMBER(9)     default 0;
   ws_situacao_edi_new            NUMBER(1)     default 0;
   ws_situacao_edi_old            NUMBER(1)     default 0;
   ws_status_frete_new            NUMBER(1)     default 0;
   ws_status_frete_old            NUMBER(1)     default 0;
   ws_perc_iss_new                NUMBER(6,2)   default 0.0;
   ws_perc_iss_old                NUMBER(6,2)   default 0.0;
   ws_num_serie_ecf_new           VARCHAR2(20)  default '';
   ws_num_serie_ecf_old           VARCHAR2(20)  default '';
   ws_num_intervencao_new         NUMBER(4)     default 0;
   ws_num_intervencao_old         NUMBER(4)     default 0;
   ws_ano_inutilizacao_new        VARCHAR2(2)   default '';
   ws_ano_inutilizacao_old        VARCHAR2(2)   default '';
   ws_nr_final_inut_new           VARCHAR2(9)   default '';
   ws_nr_final_inut_old           VARCHAR2(9)   default '';
   ws_status_impressao_danfe_new  NUMBER(1)     default 0;
   ws_status_impressao_danfe_old  NUMBER(1)     default 0;
   ws_nfe_contigencia_new         NUMBER(1)     default 0;
   ws_nfe_contigencia_old         NUMBER(1)     default 0;

begin
   if INSERTING then
      ws_tipo_ocorr                := 'I';
      ws_codigo_empresa_new        := :new.codigo_empresa;
      ws_num_nota_fiscal_new       := :new.num_nota_fiscal;
      ws_serie_nota_fisc_new       := :new.serie_nota_fisc;
      ws_especie_docto_new         := :new.especie_docto;
      ws_natop_nf_nat_oper_old     := 0;
      ws_natop_nf_nat_oper_new     := :new.natop_nf_nat_oper;
      ws_natop_nf_est_oper_old     := null;
      ws_natop_nf_est_oper_new     := :new.natop_nf_est_oper;
      ws_cond_pgto_venda_old       := null;
      ws_cond_pgto_venda_new       := :new.cond_pgto_venda;
      ws_data_emissao_old          := null;
      ws_data_emissao_new          := :new.data_emissao;
      ws_data_saida_new            := :new.data_saida;
      ws_hora_saida_new            := :new.hora_saida;
      ws_data_base_fatur_old       := null;
      ws_data_base_fatur_new       := :new.data_base_fatur;
      ws_cgc_9_new                 := :new.cgc_9;
      ws_cgc_4_new                 := :new.cgc_4;
      ws_cgc_2_new                 := :new.cgc_2;
      ws_seq_end_entr_old          := 0;
      ws_seq_end_entr_new          := :new.seq_end_entr;
      ws_cod_rep_cliente_old       := 0;
      ws_cod_rep_cliente_new       := :new.cod_rep_cliente;
      ws_perc_repres_old           := 0.00;
      ws_perc_repres_new           := :new.perc_repres;
      ws_seq_end_cobr_old          := 0;
      ws_seq_end_cobr_new          := :new.seq_end_cobr;
      ws_cod_vendedor_old          := 0;
      ws_cod_vendedor_new          := :new.cod_vendedor;
      ws_perc_vendedor_old         := 0.00;
      ws_perc_vendedor_new         := :new.perc_vendedor;
      ws_transpor_forne9_old       := 0;
      ws_transpor_forne9_new       := :new.transpor_forne9;
      ws_transpor_forne4_old       := 0;
      ws_transpor_forne4_new       := :new.transpor_forne4;
      ws_transpor_forne2_old       := 0;
      ws_transpor_forne2_new       := :new.transpor_forne2;
      ws_transp_redespacho9_old    := 0;
      ws_transp_redespacho9_new    := :new.transp_redespacho9;
      ws_transp_redespacho4_old    := 0;
      ws_transp_redespacho4_new    := :new.transp_redespacho4;
      ws_transp_redespacho2_old    := 0;
      ws_transp_redespacho2_new    := :new.transp_redespacho2;
      ws_codigo_embalagem_old      := 0;
      ws_codigo_embalagem_new      := :new.codigo_embalagem;
      ws_qtde_embalagens_old       := 0;
      ws_qtde_embalagens_new       := :new.qtde_embalagens;
      ws_col_tabela_old            := 0;
      ws_col_tabela_new            := :new.col_tabela;
      ws_mes_tabela_old            := 0;
      ws_mes_tabela_new            := :new.mes_tabela;
      ws_seq_tabela_old            := 0;
      ws_seq_tabela_new            := :new.seq_tabela;
      ws_peso_liquido_old          := 0;
      ws_peso_liquido_new          := :new.peso_liquido;
      ws_peso_bruto_old            := 0;
      ws_peso_bruto_new            := :new.peso_bruto;
      ws_valor_despesas_old        := 0;
      ws_valor_despesas_new        := :new.valor_despesas;
      ws_valor_frete_nfis_old      := 0;
      ws_valor_frete_nfis_new      := :new.valor_frete_nfis;
      ws_valor_seguro_old          := 0;
      ws_valor_seguro_new          := :new.valor_seguro;
      ws_valor_iss_old             := 0;
      ws_valor_iss_new             := :new.valor_iss;
      ws_base_icmm_old             := 0;
      ws_base_icmm_new             := :new.base_icms;
      ws_valor_icms_old            := 0;
      ws_valor_icms_new            := :new.valor_icms;
      ws_valor_ipi_old             := 0;
      ws_valor_ipi_new             := :new.valor_ipi;
      ws_situacao_nfisc_old        := 0;
      ws_situacao_nfisc_new        := :new.situacao_nfisc;
      ws_desconto1_old             := 0;
      ws_desconto1_new             := :new.desconto1;
      ws_vlr_desc_especial_old     := 0;
      ws_vlr_desc_especial_new     := :new.vlr_desc_especial;
      ws_encargos_old              := 0;
      ws_encargos_new              := :new.encargos;
      ws_historico_cont_old        := 0;
      ws_historico_cont_new        := :new.historico_cont;
      ws_num_contabil_old          := 0;
      ws_num_contabil_new          := :new.num_contabil;
      ws_numero_danf_nfe_old       := null;
      ws_numero_danf_nfe_new       := :new.numero_danf_nfe;
      ws_cod_status_old            := null;
      ws_cod_status_new            := :new.cod_status;
      ws_msg_status_old            := null;
      ws_msg_status_new            := :new.msg_status;
      ws_cod_solicitacao_nfe_old   := 0;
      ws_cod_solicitacao_nfe_new   := :new.cod_solicitacao_nfe;
      ws_nr_recibo_old             := null;
      ws_nr_recibo_new             := :new.nr_recibo;
      ws_nr_protocolo_old          := null;
      ws_nr_protocolo_new          := :new.nr_protocolo;
      ws_chave_contingencia_old    := null;
      ws_chave_contingencia_new    := :new.chave_contingencia;
      ws_cod_canc_nfisc_new         := :new.cod_canc_nfisc;
      ws_cod_canc_nfisc_old         := 0;
      ws_qtde_itens_new             := :new.qtde_itens;
      ws_qtde_itens_old             := 0;
      ws_quantidade_new             := :new.quantidade;
      ws_quantidade_old             := 0.0;
      ws_desconto2_new              := :new.desconto2;
      ws_desconto2_old              := 0.0;
      ws_desconto3_new              := :new.desconto3;
      ws_desconto3_old              := 0.0;
      ws_valor_itens_nfis_new       := :new.valor_itens_nfis;
      ws_valor_itens_nfis_old       := 0.0;
      ws_valor_encar_nota_new       := :new.valor_encar_nota;
      ws_valor_encar_nota_old       := 0.0;
      ws_valor_desc_nota_new        := :new.valor_desc_nota;
      ws_valor_desc_nota_old        := 0.0;
      ws_valor_desconto_new         := :new.valor_desconto;
      ws_valor_desconto_old         := 0.0;
      ws_valor_icms_frete_new       := :new.valor_icms_frete;
      ws_valor_icms_frete_old       := 0.0;
      ws_base_icms_sub_new          := :new.base_icms_sub;
      ws_base_icms_sub_old          := 0.0;
      ws_valor_icms_sub_new         := :new.valor_icms_sub;
      ws_valor_icms_sub_old         := 0.0;
      ws_valor_suframa_new          := :new.valor_suframa;
      ws_valor_suframa_old          := 0.0;
      ws_flag_devolucao_new         := :new.flag_devolucao;
      ws_flag_devolucao_old         := 0;
      ws_flag_contabil_new          := :new.flag_contabil;
      ws_flag_contabil_old          := 0;
      ws_nr_cupom_new               := :new.nr_cupom;
      ws_nr_cupom_old               := 0;
      ws_situacao_edi_new           := :new.situacao_edi;
      ws_situacao_edi_old           := 0;
      ws_status_frete_new           := :new.status_frete;
      ws_status_frete_old           := 0;
      ws_perc_iss_new               := :new.perc_iss;
      ws_perc_iss_old               := 0.0;
      ws_num_serie_ecf_new          := :new.num_serie_ecf;
      ws_num_serie_ecf_old          := '';
      ws_num_intervencao_new        := :new.num_intervencao;
      ws_num_intervencao_old        := 0;
      ws_ano_inutilizacao_new       := :new.ano_inutilizacao;
      ws_ano_inutilizacao_old       := '';
      ws_nr_final_inut_new          := :new.nr_final_inut;
      ws_nr_final_inut_old          := '';
      ws_status_impressao_danfe_new := :new.status_impressao_danfe;
      ws_status_impressao_danfe_old := 0;
      ws_nfe_contigencia_new        := :new.nfe_contigencia;
      ws_nfe_contigencia_old        := 0;

   elsif UPDATING then

      ws_tipo_ocorr                := 'A';
      ws_codigo_empresa_new        := :new.codigo_empresa;
      ws_num_nota_fiscal_new       := :new.num_nota_fiscal;
      ws_serie_nota_fisc_new       := :new.serie_nota_fisc;
      ws_especie_docto_new         := :new.especie_docto;
      ws_natop_nf_nat_oper_old     := :old.natop_nf_nat_oper;
      ws_natop_nf_nat_oper_new     := :new.natop_nf_nat_oper;
      ws_natop_nf_est_oper_old     := :old.natop_nf_est_oper;
      ws_natop_nf_est_oper_new     := :new.natop_nf_est_oper;
      ws_cond_pgto_venda_old       := :old.cond_pgto_venda;
      ws_cond_pgto_venda_new       := :new.cond_pgto_venda;
      ws_data_emissao_old          := :old.data_emissao;
      ws_data_emissao_new          := :new.data_emissao;
      ws_data_saida_new            := :new.data_saida;
      ws_hora_saida_new            := :new.hora_saida;
      ws_data_base_fatur_old       := :old.data_base_fatur;
      ws_data_base_fatur_new       := :new.data_base_fatur;
      ws_cgc_9_new                 := :new.cgc_9;
      ws_cgc_4_new                 := :new.cgc_4;
      ws_cgc_2_new                 := :new.cgc_2;
      ws_seq_end_entr_old          := :old.seq_end_entr;
      ws_seq_end_entr_new          := :new.seq_end_entr;
      ws_cod_rep_cliente_old       := :old.cod_rep_cliente;
      ws_cod_rep_cliente_new       := :new.cod_rep_cliente;
      ws_perc_repres_old           := :old.perc_repres;
      ws_perc_repres_new           := :new.perc_repres;
      ws_seq_end_cobr_old          := :old.seq_end_cobr;
      ws_seq_end_cobr_new          := :new.seq_end_cobr;
      ws_cod_vendedor_old          := :old.cod_vendedor;
      ws_cod_vendedor_new          := :new.cod_vendedor;
      ws_perc_vendedor_old         := :old.perc_vendedor;
      ws_perc_vendedor_new         := :new.perc_vendedor;
      ws_transpor_forne9_old       := :old.transpor_forne9;
      ws_transpor_forne9_new       := :new.transpor_forne9;
      ws_transpor_forne4_old       := :old.transpor_forne4;
      ws_transpor_forne4_new       := :new.transpor_forne4;
      ws_transpor_forne2_old       := :old.transpor_forne2;
      ws_transpor_forne2_new       := :new.transpor_forne2;
      ws_transp_redespacho9_old    := :old.transp_redespacho9;
      ws_transp_redespacho9_new    := :new.transp_redespacho9;
      ws_transp_redespacho4_old    := :old.transp_redespacho4;
      ws_transp_redespacho4_new    := :new.transp_redespacho4;
      ws_transp_redespacho2_old    := :old.transp_redespacho2;
      ws_transp_redespacho2_new    := :new.transp_redespacho2;
      ws_codigo_embalagem_old      := :old.codigo_embalagem;
      ws_codigo_embalagem_new      := :new.codigo_embalagem;
      ws_qtde_embalagens_old       := :old.qtde_embalagens;
      ws_qtde_embalagens_new       := :new.qtde_embalagens;
      ws_col_tabela_old            := :old.col_tabela;
      ws_col_tabela_new            := :new.col_tabela;
      ws_mes_tabela_old            := :old.mes_tabela;
      ws_mes_tabela_new            := :new.mes_tabela;
      ws_seq_tabela_old            := :old.seq_tabela;
      ws_seq_tabela_new            := :new.seq_tabela;
      ws_peso_liquido_old          := :old.peso_liquido;
      ws_peso_liquido_new          := :new.peso_liquido;
      ws_peso_bruto_old            := :old.peso_bruto;
      ws_peso_bruto_new            := :new.peso_bruto;
      ws_valor_despesas_old        := :old.valor_despesas;
      ws_valor_despesas_new        := :new.valor_despesas;
      ws_valor_frete_nfis_old      := :old.valor_frete_nfis;
      ws_valor_frete_nfis_new      := :new.valor_frete_nfis;
      ws_valor_seguro_old          := :old.valor_seguro;
      ws_valor_seguro_new          := :new.valor_seguro;
      ws_valor_iss_old             := :old.valor_iss;
      ws_valor_iss_new             := :new.valor_iss;
      ws_base_icmm_old             := :old.base_icms;
      ws_base_icmm_new             := :new.base_icms;
      ws_valor_icms_old            := :old.valor_icms;
      ws_valor_icms_new            := :new.valor_icms;
      ws_valor_ipi_old             := :old.valor_ipi;
      ws_valor_ipi_new             := :new.valor_ipi;
      ws_situacao_nfisc_old        := :old.situacao_nfisc;
      ws_situacao_nfisc_new        := :new.situacao_nfisc;
      ws_desconto1_old             := :old.desconto1;
      ws_desconto1_new             := :new.desconto1;
      ws_vlr_desc_especial_old     := :old.vlr_desc_especial;
      ws_vlr_desc_especial_new     := :new.vlr_desc_especial;
      ws_encargos_old              := :old.encargos;
      ws_encargos_new              := :new.encargos;
      ws_historico_cont_old        := :old.historico_cont;
      ws_historico_cont_new        := :new.historico_cont;
      ws_num_contabil_old          := :old.num_contabil;
      ws_num_contabil_new          := :new.num_contabil;
      ws_numero_danf_nfe_old       := :old.numero_danf_nfe;
      ws_numero_danf_nfe_new       := :new.numero_danf_nfe;
      ws_cod_status_old            := :old.cod_status;
      ws_cod_status_new            := :new.cod_status;
      ws_msg_status_old            := :old.msg_status;
      ws_msg_status_new            := :new.msg_status;
      ws_cod_solicitacao_nfe_old   := :old.cod_solicitacao_nfe;
      ws_cod_solicitacao_nfe_new   := :new.cod_solicitacao_nfe;
      ws_nr_recibo_old             := :old.nr_recibo;
      ws_nr_recibo_new             := :new.nr_recibo;
      ws_nr_protocolo_old          := :old.nr_protocolo;
      ws_nr_protocolo_new          := :new.nr_protocolo;
      ws_chave_contingencia_old    := :old.chave_contingencia;
      ws_chave_contingencia_new    := :new.chave_contingencia;
      ws_cod_canc_nfisc_new         := :new.cod_canc_nfisc;
      ws_cod_canc_nfisc_old         := :old.cod_canc_nfisc;
      ws_qtde_itens_new             := :new.qtde_itens;
      ws_qtde_itens_old             := :old.qtde_itens;
      ws_quantidade_new             := :new.quantidade;
      ws_quantidade_old             := :old.quantidade;
      ws_desconto2_new              := :new.desconto2;
      ws_desconto2_old              := :old.desconto2;
      ws_desconto3_new              := :new.desconto3;
      ws_desconto3_old              := :old.desconto3;
      ws_valor_itens_nfis_new       := :new.valor_itens_nfis;
      ws_valor_itens_nfis_old       := :old.valor_itens_nfis;
      ws_valor_encar_nota_new       := :new.valor_encar_nota;
      ws_valor_encar_nota_old       := :old.valor_encar_nota;
      ws_valor_desc_nota_new        := :new.valor_desc_nota;
      ws_valor_desc_nota_old        := :old.valor_desc_nota;
      ws_valor_desconto_new         := :new.valor_desconto;
      ws_valor_desconto_old         := :old.valor_desconto;
      ws_valor_icms_frete_new       := :new.valor_icms_frete;
      ws_valor_icms_frete_old       := :old.valor_icms_frete;
      ws_base_icms_sub_new          := :new.base_icms_sub;
      ws_base_icms_sub_old          := :old.base_icms_sub;
      ws_valor_icms_sub_new         := :new.valor_icms_sub;
      ws_valor_icms_sub_old         := :old.valor_icms_sub;
      ws_valor_suframa_new          := :new.valor_suframa;
      ws_valor_suframa_old          := :old.valor_suframa;
      ws_flag_devolucao_new         := :new.flag_devolucao;
      ws_flag_devolucao_old         := :old.flag_devolucao;
      ws_flag_contabil_new          := :new.flag_contabil;
      ws_flag_contabil_old          := :old.flag_contabil;
      ws_nr_cupom_new               := :new.nr_cupom;
      ws_nr_cupom_old               := :old.nr_cupom;
      ws_situacao_edi_new           := :new.situacao_edi;
      ws_situacao_edi_old           := :old.situacao_edi;
      ws_status_frete_new           := :new.status_frete;
      ws_status_frete_old           := :old.status_frete;
      ws_perc_iss_new               := :new.perc_iss;
      ws_perc_iss_old               := :old.perc_iss;
      ws_num_serie_ecf_new          := :new.num_serie_ecf;
      ws_num_serie_ecf_old          := :old.num_serie_ecf;
      ws_num_intervencao_new        := :new.num_intervencao;
      ws_num_intervencao_old        := :old.num_intervencao;
      ws_ano_inutilizacao_new       := :new.ano_inutilizacao;
      ws_ano_inutilizacao_old       := :old.ano_inutilizacao;
      ws_nr_final_inut_new          := :new.nr_final_inut;
      ws_nr_final_inut_old          := :old.nr_final_inut;
      ws_status_impressao_danfe_new := :new.status_impressao_danfe;
      ws_status_impressao_danfe_old := :old.status_impressao_danfe;
      ws_nfe_contigencia_new        := :new.nfe_contigencia;
      ws_nfe_contigencia_old        := :old.nfe_contigencia;


   elsif DELETING then
      ws_tipo_ocorr                := 'D';
      ws_codigo_empresa_new        := 0;
      ws_num_nota_fiscal_new       := :old.num_nota_fiscal;
      ws_serie_nota_fisc_new       := :old.serie_nota_fisc;
      ws_especie_docto_new         := :old.especie_docto;
      ws_natop_nf_nat_oper_old     := :old.natop_nf_nat_oper;
      ws_natop_nf_nat_oper_new     := 0;
      ws_natop_nf_est_oper_old     := :old.natop_nf_est_oper;
      ws_natop_nf_est_oper_new     := 0;
      ws_cond_pgto_venda_old       := :old.cond_pgto_venda;
      ws_cond_pgto_venda_new       := 0;
      ws_data_emissao_old          := :old.data_emissao;
      ws_data_emissao_new          := null;
      ws_data_saida_new            := null;
      ws_hora_saida_new            := null;
      ws_data_base_fatur_old       := :old.data_base_fatur;
      ws_data_base_fatur_new       := null;
      ws_cgc_9_new                 := 0;
      ws_cgc_4_new                 := 0;
      ws_cgc_2_new                 := 0;
      ws_seq_end_entr_old          := :old.seq_end_entr;
      ws_seq_end_entr_new          := 0;
      ws_cod_rep_cliente_old       := :old.cod_rep_cliente;
      ws_cod_rep_cliente_new       := 0;
      ws_perc_repres_old           := :old.perc_repres;
      ws_perc_repres_new           := 0;
      ws_seq_end_cobr_old          := :old.seq_end_cobr;
      ws_seq_end_cobr_new          := 0;
      ws_cod_vendedor_old          := :old.cod_vendedor;
      ws_cod_vendedor_new          := 0;
      ws_perc_vendedor_old         := :old.perc_vendedor;
      ws_perc_vendedor_new         := 0;
      ws_transpor_forne9_old       := :old.transpor_forne9;
      ws_transpor_forne9_new       := 0;
      ws_transpor_forne4_old       := :old.transpor_forne4;
      ws_transpor_forne4_new       := 0;
      ws_transpor_forne2_old       := :old.transpor_forne2;
      ws_transpor_forne2_new       := 0;
      ws_transp_redespacho9_old    := :old.transp_redespacho9;
      ws_transp_redespacho9_new    := 0;
      ws_transp_redespacho4_old    := :old.transp_redespacho4;
      ws_transp_redespacho4_new    := 0;
      ws_transp_redespacho2_old    := :old.transp_redespacho2;
      ws_transp_redespacho2_new    := 0;
      ws_codigo_embalagem_old      := :old.codigo_embalagem;
      ws_codigo_embalagem_new      := 0;
      ws_qtde_embalagens_old       := :old.qtde_embalagens;
      ws_qtde_embalagens_new       := 0;
      ws_col_tabela_old            := :old.col_tabela;
      ws_col_tabela_new            := 0;
      ws_mes_tabela_old            := :old.mes_tabela;
      ws_mes_tabela_new            := 0;
      ws_seq_tabela_old            := :old.seq_tabela;
      ws_seq_tabela_new            := 0;
      ws_peso_liquido_old          := :old.peso_liquido;
      ws_peso_liquido_new          := 0;
      ws_peso_bruto_old            := :old.peso_bruto;
      ws_peso_bruto_new            := 0;
      ws_valor_despesas_old        := :old.valor_despesas;
      ws_valor_despesas_new        := 0;
      ws_valor_frete_nfis_old      := :old.valor_frete_nfis;
      ws_valor_frete_nfis_new      := 0;
      ws_valor_seguro_old          := :old.valor_seguro;
      ws_valor_seguro_new          := 0;
      ws_valor_iss_old             := :old.valor_iss;
      ws_valor_iss_new             := 0;
      ws_base_icmm_old             := :old.base_icms;
      ws_base_icmm_new             := 0;
      ws_valor_icms_old            := :old.valor_icms;
      ws_valor_icms_new            := 0;
      ws_valor_ipi_old             := :old.valor_ipi;
      ws_valor_ipi_new             := 0;
      ws_situacao_nfisc_old        := :old.situacao_nfisc;
      ws_situacao_nfisc_new        := 0;
      ws_desconto1_old             := :old.desconto1;
      ws_desconto1_new             := 0;
      ws_vlr_desc_especial_old     := :old.vlr_desc_especial;
      ws_vlr_desc_especial_new     := 0;
      ws_encargos_old              := :old.encargos;
      ws_encargos_new              := 0;
      ws_historico_cont_old        := :old.historico_cont;
      ws_historico_cont_new        := 0;
      ws_num_contabil_old          := :old.num_contabil;
      ws_num_contabil_new          := 0;
      ws_numero_danf_nfe_old       := :old.numero_danf_nfe;
      ws_numero_danf_nfe_new       := null;
      ws_cod_status_old            := :old.cod_status;
      ws_cod_status_new            := null;
      ws_msg_status_old            := :old.msg_status;
      ws_msg_status_new            := null;
      ws_cod_solicitacao_nfe_old   := :old.cod_solicitacao_nfe;
      ws_cod_solicitacao_nfe_new   := 0;
      ws_nr_recibo_old             := :old.nr_recibo;
      ws_nr_recibo_new             := null;
      ws_nr_protocolo_old          := :old.nr_protocolo;
      ws_nr_protocolo_new          := null;
      ws_chave_contingencia_old    := :old.chave_contingencia;
      ws_chave_contingencia_new    := null;
      ws_cod_canc_nfisc_new         := null;
      ws_cod_canc_nfisc_old         := :old.cod_canc_nfisc;
      ws_qtde_itens_new             := null;
      ws_qtde_itens_old             := :old.qtde_itens;
      ws_quantidade_new             := null;
      ws_quantidade_old             := :old.quantidade;
      ws_desconto2_new              := null;
      ws_desconto2_old              := :old.desconto2;
      ws_desconto3_new              := null;
      ws_desconto3_old              := :old.desconto3;
      ws_valor_itens_nfis_new       := null;
      ws_valor_itens_nfis_old       := :old.valor_itens_nfis;
      ws_valor_encar_nota_new       := null;
      ws_valor_encar_nota_old       := :old.valor_encar_nota;
      ws_valor_desc_nota_new        := null;
      ws_valor_desc_nota_old        := :old.valor_desc_nota;
      ws_valor_desconto_new         := null;
      ws_valor_desconto_old         := :old.valor_desconto;
      ws_valor_icms_frete_new       := null;
      ws_valor_icms_frete_old       := :old.valor_icms_frete;
      ws_base_icms_sub_new          := null;
      ws_base_icms_sub_old          := :old.base_icms_sub;
      ws_valor_icms_sub_new         := null;
      ws_valor_icms_sub_old         := :old.valor_icms_sub;
      ws_valor_suframa_new          := null;
      ws_valor_suframa_old          := :old.valor_suframa;
      ws_flag_devolucao_new         := null;
      ws_flag_devolucao_old         := :old.flag_devolucao;
      ws_flag_contabil_new          := null;
      ws_flag_contabil_old          := :old.flag_contabil;
      ws_nr_cupom_new               := null;
      ws_nr_cupom_old               := :old.nr_cupom;
      ws_situacao_edi_new           := null;
      ws_situacao_edi_old           := :old.situacao_edi;
      ws_status_frete_new           := null;
      ws_status_frete_old           := :old.status_frete;
      ws_perc_iss_new               := null;
      ws_perc_iss_old               := :old.perc_iss;
      ws_num_serie_ecf_new          := null;
      ws_num_serie_ecf_old          := :old.num_serie_ecf;
      ws_num_intervencao_new        := null;
      ws_num_intervencao_old        := :old.num_intervencao;
      ws_ano_inutilizacao_new       := null;
      ws_ano_inutilizacao_old       := :old.ano_inutilizacao;
      ws_nr_final_inut_new          := null;
      ws_nr_final_inut_old          := :old.nr_final_inut;
      ws_status_impressao_danfe_new := null;
      ws_status_impressao_danfe_old := :old.status_impressao_danfe;
      ws_nfe_contigencia_new        := null;
      ws_nfe_contigencia_old        := :old.nfe_contigencia;
   end if;

   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ws_nome_programa := inter_fn_nome_programa(ws_sid); 
   
   if ws_natop_nf_nat_oper_old     <>    ws_natop_nf_nat_oper_new or
      ws_natop_nf_est_oper_old     <>     ws_natop_nf_est_oper_new  or
      ws_cond_pgto_venda_old       <>       ws_cond_pgto_venda_new  or
      ws_data_emissao_old          <>       ws_data_emissao_new     or
      ws_data_base_fatur_old       <>       ws_data_base_fatur_new       or
      ws_seq_end_entr_old          <>       ws_seq_end_entr_new          or
      ws_cod_rep_cliente_old       <>       ws_cod_rep_cliente_new       or
      ws_perc_repres_old           <>       ws_perc_repres_new           or
      ws_seq_end_cobr_old          <>       ws_seq_end_cobr_new          or
      ws_cod_vendedor_old          <>       ws_cod_vendedor_new          or
      ws_perc_vendedor_old         <>       ws_perc_vendedor_new         or
      ws_transpor_forne9_old       <>       ws_transpor_forne9_new       or
      ws_transpor_forne4_old       <>       ws_transpor_forne4_new       or
      ws_transpor_forne2_old       <>       ws_transpor_forne2_new       or
      ws_transp_redespacho9_old    <>       ws_transp_redespacho9_new    or
      ws_transp_redespacho4_old    <>       ws_transp_redespacho4_new    or
      ws_transp_redespacho2_old    <>       ws_transp_redespacho2_new    or
      ws_codigo_embalagem_old      <>       ws_codigo_embalagem_new      or
      ws_qtde_embalagens_old       <>       ws_qtde_embalagens_new       or
      ws_col_tabela_old            <>       ws_col_tabela_new            or
      ws_mes_tabela_old            <>       ws_mes_tabela_new            or
      ws_seq_tabela_old            <>       ws_seq_tabela_new            or
      ws_peso_liquido_old          <>       ws_peso_liquido_new          or
      ws_peso_bruto_old            <>       ws_peso_bruto_new            or
      ws_valor_despesas_old        <>       ws_valor_despesas_new        or
      ws_valor_frete_nfis_old      <>       ws_valor_frete_nfis_new      or
      ws_valor_seguro_old          <>       ws_valor_seguro_new          or
      ws_valor_iss_old             <>       ws_valor_iss_new             or
      ws_base_icmm_old             <>       ws_base_icmm_new             or
      ws_valor_icms_old            <>       ws_valor_icms_new            or
      ws_valor_ipi_old             <>       ws_valor_ipi_new             or
      ws_situacao_nfisc_old        <>       ws_situacao_nfisc_new        or
      ws_desconto1_old             <>       ws_desconto1_new             or
      ws_vlr_desc_especial_old     <>       ws_vlr_desc_especial_new     or
      ws_encargos_old              <>       ws_encargos_new              or
      ws_historico_cont_old        <>       ws_historico_cont_new        or
      ws_num_contabil_old          <>       ws_num_contabil_new          or
      ws_numero_danf_nfe_old       <>       ws_numero_danf_nfe_new       or
      ws_cod_status_old            <>       ws_cod_status_new            or
      ws_msg_status_old            <>       ws_msg_status_new            or
      ws_cod_solicitacao_nfe_old   <>       ws_cod_solicitacao_nfe_new   or
      ws_nr_recibo_old             <>       ws_nr_recibo_new             or
      ws_nr_protocolo_old          <>       ws_nr_protocolo_new          or
      ws_chave_contingencia_old    <>       ws_chave_contingencia_new    or
      ws_cod_canc_nfisc_new         <>       ws_cod_canc_nfisc_old         or
      ws_qtde_itens_new             <>       ws_qtde_itens_old             or
      ws_quantidade_new             <>       ws_quantidade_old             or
      ws_desconto2_new              <>       ws_desconto2_old              or
      ws_desconto3_new              <>       ws_desconto3_old              or
      ws_valor_itens_nfis_new       <>       ws_valor_itens_nfis_old       or
      ws_valor_encar_nota_new       <>       ws_valor_encar_nota_old       or
      ws_valor_desc_nota_new        <>       ws_valor_desc_nota_old        or
      ws_valor_desconto_new         <>       ws_valor_desconto_old         or
      ws_valor_icms_frete_new       <>       ws_valor_icms_frete_old       or
      ws_base_icms_sub_new          <>       ws_base_icms_sub_old          or
      ws_valor_icms_sub_new         <>       ws_valor_icms_sub_old         or
      ws_valor_suframa_new          <>       ws_valor_suframa_old          or
      ws_flag_devolucao_new         <>       ws_flag_devolucao_old         or
      ws_flag_contabil_new          <>       ws_flag_contabil_old          or
      ws_nr_cupom_new               <>       ws_nr_cupom_old               or
      ws_situacao_edi_new           <>       ws_situacao_edi_old           or
      ws_status_frete_new           <>       ws_status_frete_old           or
      ws_perc_iss_new               <>       ws_perc_iss_old               or
      ws_num_serie_ecf_new          <>       ws_num_serie_ecf_old          or
      ws_num_intervencao_new        <>       ws_num_intervencao_old        or
      ws_ano_inutilizacao_new       <>       ws_ano_inutilizacao_old       or
      ws_nr_final_inut_new          <>       ws_nr_final_inut_old          or
      ws_status_impressao_danfe_new <>       ws_status_impressao_danfe_old or
      ws_nfe_contigencia_new        <>       ws_nfe_contigencia_old
   then


	   INSERT INTO fatu_050_hist
	      ( aplicacao,                      tipo_ocorr,
		data_ocorr,
		usuario_rede,                   maquina_rede,
		nome_programa,                  usuario_systextil,
		codigo_empresa_new,
		num_nota_fiscal_new,
		serie_nota_fisc_new,
		especie_docto_new,
		natop_nf_nat_oper_old,          natop_nf_nat_oper_new,
		natop_nf_est_oper_old,          natop_nf_est_oper_new,
		cond_pgto_venda_old,            cond_pgto_venda_new,
		data_emissao_old,               data_emissao_new,
		data_saida_new,
		hora_saida_new,
		data_base_fatur_old,            data_base_fatur_new,
		cgc_9_new,
		cgc_4_new,
		cgc_2_new,
		seq_end_entr_old,               seq_end_entr_new,
		cod_rep_cliente_old,            cod_rep_cliente_new,
		perc_repres_old,                perc_repres_new,
		seq_end_cobr_old,               seq_end_cobr_new,
		cod_vendedor_old,               cod_vendedor_new,
		perc_vendedor_old,              perc_vendedor_new,
		transpor_forne9_old,            transpor_forne9_new,
		transpor_forne4_old,            transpor_forne4_new,
		transpor_forne2_old,            transpor_forne2_new,
		transp_redespacho9_old,         transp_redespacho9_new,
		transp_redespacho4_old,         transp_redespacho4_new,
		transp_redespacho2_old,         transp_redespacho2_new,
		codigo_embalagem_old,           codigo_embalagem_new,
		qtde_embalagens_old,            qtde_embalagens_new,
		col_tabela_old,                 col_tabela_new,
		mes_tabela_old,                 mes_tabela_new,
		seq_tabela_old,                 seq_tabela_new,
		peso_liquido_old,               peso_liquido_new,
		peso_bruto_old,                 peso_bruto_new,
		valor_despesas_old,             valor_despesas_new,
		valor_frete_nfis_old,           valor_frete_nfis_new,
		valor_seguro_old,               valor_seguro_new,
		valor_iss_old,                  valor_iss_new,
		base_icms_old,                  base_icms_new,
		valor_icms_old,                 valor_icms_new,
		valor_ipi_old,                  valor_ipi_new,
		situacao_nfisc_old,             situacao_nfisc_new,
		desconto1_old,                  desconto1_new,
		vlr_desc_especial_old,          vlr_desc_especial_new,
		encargos_old,                   encargos_new,
		historico_cont_old,             historico_cont_new,
		num_contabil_old,               num_contabil_new,
		numero_danf_nfe_old,            numero_danf_nfe_new,
		cod_status_old,                 cod_status_new,
		msg_status_old,                 msg_status_new,
		cod_solicitacao_nfe_old,        cod_solicitacao_nfe_new,
		nr_recibo_old,                  nr_recibo_new,
		nr_protocolo_old,               nr_protocolo_new,
		chave_contingencia_old,         chave_contingencia_new,
		cod_canc_nfisc_new,           cod_canc_nfisc_old,
		qtde_itens_new,             qtde_itens_old,
		quantidade_new,                 quantidade_old,
		 desconto2_new,              desconto2_old,
		desconto3_new,                desconto3_old,
		valor_itens_nfis_new,         valor_itens_nfis_old,
		valor_encar_nota_new,         valor_encar_nota_old,
		valor_desc_nota_new,          valor_desc_nota_old,
		valor_desconto_new,           valor_desconto_old,
		valor_icms_frete_new,         valor_icms_frete_old,
		base_icms_sub_new,            base_icms_sub_old,
		valor_icms_sub_new,           valor_icms_sub_old,
		valor_suframa_new,            valor_suframa_old,
		flag_devolucao_new,           flag_devolucao_old,
		flag_contabil_new,            flag_contabil_old,
		nr_cupom_new,                 nr_cupom_old,
		situacao_edi_new,             situacao_edi_old,
		status_frete_new,             status_frete_old,
		perc_iss_new,                 perc_iss_old,
		num_serie_ecf_new,            num_serie_ecf_old,
		num_intervencao_new,          num_intervencao_old,
		ano_inutilizacao_new,         ano_inutilizacao_old,
		nr_final_inut_new,            nr_final_inut_old,
		status_impressao_danfe_new,   status_impressao_danfe_old,
		nfe_contigencia_new,          nfe_contigencia_old


		 )
	   VALUES
	      ( ws_aplicativo,                  ws_tipo_ocorr,
		sysdate,
		ws_usuario_rede,                ws_maquina_rede,
		ws_nome_programa,               ws_usuario_systextil,
		ws_codigo_empresa_new,
		ws_num_nota_fiscal_new,
		ws_serie_nota_fisc_new,
		ws_especie_docto_new,
		ws_natop_nf_nat_oper_old,       ws_natop_nf_nat_oper_new,
		ws_natop_nf_est_oper_old,       ws_natop_nf_est_oper_new,
		ws_cond_pgto_venda_old,         ws_cond_pgto_venda_new,
		ws_data_emissao_old,            ws_data_emissao_new,
		ws_data_saida_new,
		ws_hora_saida_new,
		ws_data_base_fatur_old,         ws_data_base_fatur_new,
		ws_cgc_9_new,
		ws_cgc_4_new,
		ws_cgc_2_new,
		ws_seq_end_entr_old,            ws_seq_end_entr_new,
		ws_cod_rep_cliente_old,         ws_cod_rep_cliente_new,
		ws_perc_repres_old,             ws_perc_repres_new,
		ws_seq_end_cobr_old,            ws_seq_end_cobr_new,
		ws_cod_vendedor_old,            ws_cod_vendedor_new,
		ws_perc_vendedor_old,           ws_perc_vendedor_new,
		ws_transpor_forne9_old,         ws_transpor_forne9_new,
		ws_transpor_forne4_old,         ws_transpor_forne4_new,
		ws_transpor_forne2_old,         ws_transpor_forne2_new,
		ws_transp_redespacho9_old,      ws_transp_redespacho9_new,
		ws_transp_redespacho4_old,      ws_transp_redespacho4_new,
		ws_transp_redespacho2_old,      ws_transp_redespacho2_new,
		ws_codigo_embalagem_old,        ws_codigo_embalagem_new,
		ws_qtde_embalagens_old,         ws_qtde_embalagens_new,
		ws_col_tabela_old,              ws_col_tabela_new,
		ws_mes_tabela_old,              ws_mes_tabela_new,
		ws_seq_tabela_old,              ws_seq_tabela_new,
		ws_peso_liquido_old,            ws_peso_liquido_new,
		ws_peso_bruto_old,              ws_peso_bruto_new,
		ws_valor_despesas_old,          ws_valor_despesas_new,
		ws_valor_frete_nfis_old,        ws_valor_frete_nfis_new,
		ws_valor_seguro_old,            ws_valor_seguro_new,
		ws_valor_iss_old,               ws_valor_iss_new,
		ws_base_icmm_old,               ws_base_icmm_new,
		ws_valor_icms_old,              ws_valor_icms_new,
		ws_valor_ipi_old,               ws_valor_ipi_new,
		ws_situacao_nfisc_old,          ws_situacao_nfisc_new,
		ws_desconto1_old,               ws_desconto1_new,
		ws_vlr_desc_especial_old,       ws_vlr_desc_especial_new,
		ws_encargos_old,                ws_encargos_new,
		ws_historico_cont_old,          ws_historico_cont_new,
		ws_num_contabil_old,            ws_num_contabil_new,
		ws_numero_danf_nfe_old,         ws_numero_danf_nfe_new,
		ws_cod_status_old,              ws_cod_status_new,
		ws_msg_status_old,              ws_msg_status_new,
		ws_cod_solicitacao_nfe_old,     ws_cod_solicitacao_nfe_new,
		ws_nr_recibo_old,               ws_nr_recibo_new,
		ws_nr_protocolo_old,            ws_nr_protocolo_new,
		ws_chave_contingencia_old,      ws_chave_contingencia_new,

		ws_cod_canc_nfisc_new,          ws_cod_canc_nfisc_old,
		ws_qtde_itens_new,              ws_qtde_itens_old,
		ws_quantidade_new,              ws_quantidade_old,
		ws_desconto2_new,               ws_desconto2_old,
		ws_desconto3_new,               ws_desconto3_old,
		ws_valor_itens_nfis_new,        ws_valor_itens_nfis_old,
		ws_valor_encar_nota_new,        ws_valor_encar_nota_old,
		ws_valor_desc_nota_new,         ws_valor_desc_nota_old,
		ws_valor_desconto_new,          ws_valor_desconto_old,
		ws_valor_icms_frete_new,        ws_valor_icms_frete_old,
		ws_base_icms_sub_new,           ws_base_icms_sub_old,
		ws_valor_icms_sub_new,          ws_valor_icms_sub_old,
		ws_valor_suframa_new,           ws_valor_suframa_old,
		ws_flag_devolucao_new,          ws_flag_devolucao_old,
		ws_flag_contabil_new,           ws_flag_contabil_old,
		ws_nr_cupom_new,                ws_nr_cupom_old,
		ws_situacao_edi_new,            ws_situacao_edi_old,
		ws_status_frete_new,            ws_status_frete_old,
		ws_perc_iss_new,                ws_perc_iss_old,
		ws_num_serie_ecf_new,           ws_num_serie_ecf_old,
		ws_num_intervencao_new,         ws_num_intervencao_old,
		ws_ano_inutilizacao_new,        ws_ano_inutilizacao_old,
		ws_nr_final_inut_new,           ws_nr_final_inut_old,
		ws_status_impressao_danfe_new,  ws_status_impressao_danfe_old,
		ws_nfe_contigencia_new,         ws_nfe_contigencia_old);

	end if;

end inter_tr_fatu_050_hist;


-- ALTER TRIGGER "INTER_TR_FATU_050_HIST" ENABLE
 

/

exec inter_pr_recompile;

