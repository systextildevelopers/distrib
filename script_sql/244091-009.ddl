-- Create table
create table PEDI_223
(
  codigo_ocorrencia NUMBER(4) default 0 not null,
  descricao         VARCHAR2(60) default ''
);
comment on table PEDI_223 is 'Cadastro de Ocorrencias para Pedido de Venda';
comment on column PEDI_223.codigo_ocorrencia  is 'Codigo de ocorrencia.';
comment on column PEDI_223.descricao  is 'Descricao da ocorrencia.';
alter table PEDI_223 add constraint PK_PEDI_223 primary key (CODIGO_OCORRENCIA)
;
