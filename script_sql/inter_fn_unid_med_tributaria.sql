create or replace function inter_fn_unid_med_tributaria(p_natureza_oper     in number,
                                                        p_estado            in varchar2,
                                                        p_unidade_med_venda in varchar2,
                                                        p_ncm               in varchar2,
                                                        p_nivel             in varchar2,
                                                        p_grupo             in varchar2,
                                                        p_subGrupo          in varchar2,
                                                        p_item              in varchar2)
  return varchar2 is

  v_unid_med_trib_ncm basi_240.unid_med_trib%type;
  v_unid_med_trib_cad basi_240.unid_med_trib%type;
  v_ind_cfop_exportacao pedi_080.ind_cfop_exportacao%type;
  v_fator_umed_trib     basi_010.fator_umed_trib%type;
  v_valida_tecido_kg    boolean;

begin

   if p_estado <> 'EX'
   then
      begin
        select pedi_080.ind_cfop_exportacao into v_ind_cfop_exportacao from pedi_080
        where pedi_080.natur_operacao = p_natureza_oper
          and pedi_080.estado_natoper = p_estado;
      exception
         when no_data_found then
            v_ind_cfop_exportacao := 'N';
      end;
   else
      v_ind_cfop_exportacao := 'N';
   end if;

   if p_estado = 'EX' or v_ind_cfop_exportacao = 'S'
   then

      begin
        select nvl(basi_240.unid_med_trib,' ') into v_unid_med_trib_ncm from basi_240
        where basi_240.classific_fiscal = p_ncm
          and rownum = 1;
      exception
         when no_data_found then
            v_unid_med_trib_ncm := ' ';
      end;

      if v_unid_med_trib_ncm is null
      then
        v_unid_med_trib_ncm := ' ';
      end if;

      if v_unid_med_trib_ncm <> p_unidade_med_venda
      then
         begin
            select nvl(basi_010.fator_umed_trib,0) fator_umed_trib into v_fator_umed_trib
            from basi_010
            where basi_010.nivel_estrutura  = p_nivel
              and basi_010.grupo_estrutura  = p_grupo
              and basi_010.subgru_estrutura = p_subGrupo
              and basi_010.item_estrutura   = p_item;
         exception
            when no_data_found then
               v_fator_umed_trib := 0.0;
         end;
      end if;

      if trim(v_unid_med_trib_ncm) is null
      then
         raise_application_error(-20000,'Unidade de medida tributaria invalida. Verifique o cadastro de classificacao fiscal.'  || Chr(10) ||
         ' NCM: ' || p_ncm || Chr(10) ||
         ' Produto: ' || p_nivel || '.' || p_grupo  || '.' || p_subGrupo  || '.' || p_item || Chr(10) ||
         ' Fator: ' || v_fator_umed_trib);
      end if;
      
      if p_nivel = '2' and trim(v_unid_med_trib_ncm) = 'KG'
      then
         v_valida_tecido_kg := false;
      else
         v_valida_tecido_kg := true;
      end if;
      
      if trim(v_unid_med_trib_ncm) <> trim(p_unidade_med_venda) and v_fator_umed_trib = 0 and v_valida_tecido_kg = true
      then
         raise_application_error(-20000,'Un. Med. comercial DIFERENTE da Un. Med. Tributaria. Fator tributcrio nao informado. Informe-o no cadastro do produto tributario. ' || Chr(10) ||
         ' Produto: ' || p_nivel || '.' || p_grupo  || '.' || p_subGrupo  || '.' || p_item || Chr(10) ||
         ' Un. Med. Produto: ' || p_unidade_med_venda || Chr(10) ||
         ' Unid. Med. NCM: ' || v_unid_med_trib_ncm || Chr(10) ||
         ' NCM: ' || p_ncm);
      end if;
   end if;

   return(v_unid_med_trib_ncm);

end inter_fn_unid_med_tributaria;
