create or replace procedure inter_pr_gera_ctb_sped_i200(p_cod_empresa   IN NUMBER,
                                                        p_exercicio     IN NUMBER,
                                                        p_cod_plano_cta IN NUMBER,
                                                        p_per_inicial   IN NUMBER,
                                                        p_per_final     IN NUMBER,
                                                        p_layout_arq    IN NUMBER,
                                                        p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar a tabela sped_cta_i200 - Lancamentos contabeis
   --
   -- Hist�ricos
   --
   -- Data    Autor    Observa��es
   --

   cursor cont600(p_cod_empresa NUMBER, p_exercicio NUMBER, p_cod_plano_cta NUMBER) IS
      select cont_600.numero_lanc,             cont_600.seq_lanc,
             cont_600.origem,                  cont_600.lote,
             periodo,                          cont_600.centro_custo,
             cont_600.debito_credito,          cont_600.hist_contabil,
             replace(replace(replace(cont_600.compl_histor1, chr(13), ' '),chr(10),' '),'|',' ') compl_histor1,           cont_600.data_lancto,
             cont_600.valor_lancto,            cont_600.filial_lancto,
             cont_600.datainsercao,            cont_600.cnpj9_participante,
             cont_600.cnpj4_participante,      cont_600.cnpj2_participante,
             cont_600.cliente_fornecedor_part,
             cont_535.conta_contabil,          cont_535.subconta,
             cont_535.tipo_conta,              cont_535.patr_result,
             cont_535.exige_subconta,          cont_535.cod_reduzido,
             cont_600.tip_zeramento_fcont,     cont_050.ajuste
      from   cont_600, cont_535, cont_050
      where  cont_600.cod_empresa    = p_cod_empresa
      and    cont_600.exercicio      = p_exercicio
      and    cont_600.periodo        between p_per_inicial and p_per_final
      and    cont_535.cod_plano_cta  = p_cod_plano_cta
   --   and    cont_535.conta_contabil = cont_600.conta_contabil
      and    cont_535.cod_reduzido = cont_600.conta_reduzida
      and    cont_535.subconta       = cont_600.subconta
      and    cont_600.origem         = cont_050.origem
      and    (p_layout_arq <> 2 -- 2 FCONT, 0 - CONTABIL, 1 - SOFTEAM
              or (p_layout_arq = 2
                  and (cont_050.ajuste = 1 -- pega as contas de ajustes
                      or (cont_600.origem = 20 and cont_600.tip_zeramento_fcont = 1)
                      )  -- nulo ou zero - Lan�amento de fechamento normal, 1 - Lan�amento de fechamento de ajuste
                  )
             )
      order  by cont_600.data_lancto, cont_600.numero_lanc, cont_600.lote, cont_600.origem;

   v_erro                  EXCEPTION;

   v_conta_contabil        varchar2(30);
   v_origem_anterior       cont_600.origem%type;
   v_ajuste_anterior       cont_050.ajuste%type;
   v_numero_lanc           varchar2(19);
   v_numero_lanc_anterior  varchar2(19);

   v_indicador_lcto        varchar2(2);
   v_centro_custo          varchar2(6);
   v_valor_chave           number(15,2);
   v_data_lancto           date;
   v_controle              number(1);
   v_debito_credito        cont_600.debito_credito%type;
   v_tip_zeramento_fcont_ant   cont_600.tip_zeramento_fcont%type;

begin
   p_des_erro             := NULL;

   v_numero_lanc_anterior := '##############';
   v_origem_anterior      := 0;
   v_valor_chave          := 0.00;
   v_controle             := 1;

   for reg_cont600 in cont600(p_cod_empresa, p_exercicio, p_cod_plano_cta)
   loop
      -- concatenea o numero da chave com o n�mero do lote, que � a chave
      -- do lan�amento para o SPED
      v_numero_lanc := rtrim(ltrim(to_char(reg_cont600.numero_lanc, '000000000'))) || rtrim(ltrim(to_char(reg_cont600.lote, '00000')));

      v_debito_credito := reg_cont600.debito_credito;


      -- Quando for origem 20 e o for marcado como ajuste, deve ser encontrado o lan�amento de expurgo e este deve ser invertido d�bito e cr�ido
      -- o zeramento no FCont deve ser o mesmo tipo de lan�amento do exporgo.
      if reg_cont600.tip_zeramento_fcont = 1 -- deve ser a origem do lan�amento atual.
      then
         if p_layout_arq = 2 -- 2 FCONT, 0 - CONTABIL, 1 - SOFTEAM
         then
            if reg_cont600.debito_credito = 'D'
            then
               v_debito_credito := 'C';
            else
               if reg_cont600.debito_credito = 'C'
               then
                  v_debito_credito := 'D';
               end if;
            end if;
         end if;
      end if;

      if v_controle = 1 -- Quando for o primeiro registro lido
      then
         v_numero_lanc_anterior := v_numero_lanc;
         v_origem_anterior      := reg_cont600.origem;
         v_ajuste_anterior      := reg_cont600.ajuste;
         v_tip_zeramento_fcont_ant  := reg_cont600.tip_zeramento_fcont;
         v_controle             := 2;

         -- guarda a data do lancamento
         v_data_lancto  := reg_cont600.data_lancto;

      end if;

      -- monta a conta contabil acrescendo a subconta, quando necessario
      -- se for uma conta de resultado ou se a conta N�O exige subconta ou se a conta for sint�tica
      -- a conta ser� a propria conta. Caso contrario, concatenea o codigo da conta e o codigo da
      -- subconta

      if reg_cont600.patr_result = 2 or reg_cont600.exige_subconta = 2
      or reg_cont600.tipo_conta  = 2
      then
         v_conta_contabil := reg_cont600.conta_contabil;
      else
         v_conta_contabil := rtrim(reg_cont600.conta_contabil) ||
                             ltrim(to_char(reg_cont600.subconta, '0000'));
      end if;

      -- se houver centro de custo, transforma em caracter
      if reg_cont600.centro_custo > 0
      then
         v_centro_custo := reg_cont600.centro_custo;
      else
         v_centro_custo := null;
      end if;

      begin
         insert into sped_ctb_i250
           (cod_empresa,                         exercicio,
            num_lcto,                            cod_conta,
            cod_centro_custo,                    valor_partida,
            indicador_partida,                   cod_hist_padrao,
            complem_historico,                   cod_participante9,
            cod_participante4,                   cod_participante2,
            cliente_fornecedor_part,             codigo_reduzido,
            cod_origem)
         values
           (p_cod_empresa,                       p_exercicio,
            v_numero_lanc,                       v_conta_contabil,
            v_centro_custo,                      reg_cont600.valor_lancto,
            v_debito_credito,          ltrim(to_char(reg_cont600.hist_contabil, '0000')),
            reg_cont600.compl_histor1,           reg_cont600.cnpj9_participante,
            reg_cont600.cnpj4_participante,      reg_cont600.cnpj2_participante,
            reg_cont600.cliente_fornecedor_part, reg_cont600.cod_reduzido,
            reg_cont600.origem);

      exception
         when others then
            p_des_erro := 'Erro na inclus�o da tabela sped_ctb_i250 ' || Chr(10) || SQLERRM;
            RAISE V_ERRO;
      end;

      -- se mudar a chave/lote, grava o capa do lancamento
      if v_numero_lanc <> v_numero_lanc_anterior
      then

         -- guarda o indicador do lancamento
         if v_origem_anterior = 20
         then
            if p_layout_arq = 2 and v_tip_zeramento_fcont_ant = 1 -- 2 FCONT, 0 - CONTABIL, 1 - SOFTEAM
            then
               v_indicador_lcto := 'EF';
            else
               v_indicador_lcto := 'E'; -- CONTABIL
            end if;
         else
            if p_layout_arq <> 2
            then
               v_indicador_lcto := 'N';
            else
               if v_ajuste_anterior = 1
               then
                  v_indicador_lcto := 'X';
               end if;
            end if;
         end if;

         begin
            insert into sped_ctb_i200
              (cod_empresa,             exercicio,
               num_lcto,                data_lcto,
               valor_lcto,              indicador_lcto,
               cod_origem)
            values
              (p_cod_empresa,           p_exercicio,
               v_numero_lanc_anterior,  v_data_lancto,
               v_valor_chave,           v_indicador_lcto,
               v_origem_anterior);

         exception
            when others then
            p_des_erro := 'Erro na inclus�o da tabela sped_ctb_i200 (1) ' || Chr(10) ||
            'Lan�amento: ' || v_numero_lanc_anterior || Chr(10) ||
            'Data Lanc.: ' || to_char(v_data_lancto, 'dd/mm/yyyy') || Chr(10) || SQLERRM;
            RAISE V_ERRO;
         end;

         -- acumula o valor da chave contabil (do SPED) para gravar na tabela da capa do lancamento
         if reg_cont600.debito_credito = 'D'
         then
            v_valor_chave := reg_cont600.valor_lancto;
         else
            v_valor_chave := 0.00;
         end if;

         -- guarda a data do lancamento
         v_data_lancto  := reg_cont600.data_lancto;

      else -- � a mesma chave do sped (numero do lancto + lote)
         if reg_cont600.debito_credito = 'D'
         then
            v_valor_chave := v_valor_chave + reg_cont600.valor_lancto;
         end if;

      end if;

      v_numero_lanc_anterior := v_numero_lanc;
      v_origem_anterior      := reg_cont600.origem;
      v_ajuste_anterior      := reg_cont600.ajuste;
      v_tip_zeramento_fcont_ant  := reg_cont600.tip_zeramento_fcont;

   end loop;

   -- grava a capa dos lancamentos do ultimo lancamento

   if v_origem_anterior = 20
   then
      if p_layout_arq = 2 and v_tip_zeramento_fcont_ant = 1 -- 2 FCONT, 0 - CONTABIL, 1 - SOFTEAM
      then
         v_indicador_lcto := 'EF';
      else
         v_indicador_lcto := 'E'; -- CONTABIL
      end if;
   else
      if p_layout_arq <> 2
      then
         v_indicador_lcto := 'N';
      else
         if v_ajuste_anterior = 1
         then
            v_indicador_lcto := 'X';
         end if;
      end if;
   end if;
   begin
      insert into sped_ctb_i200
        (cod_empresa,             exercicio,
         num_lcto,                data_lcto,
         valor_lcto,              indicador_lcto,
         cod_origem)
      values
        (p_cod_empresa,           p_exercicio,
         v_numero_lanc_anterior,  v_data_lancto,
         v_valor_chave,           v_indicador_lcto,
         v_origem_anterior);

   exception
      when others then
      p_des_erro := 'Erro na inclus�o da tabela sped_ctb_i200 (1) ' || Chr(10) ||
      'Lan�amento: ' || v_numero_lanc_anterior || Chr(10) ||
      'Data Lanc.: ' || to_char(v_data_lancto, 'dd/mm/yyyy') || Chr(10) || SQLERRM;
         RAISE V_ERRO;
   end;


   commit;

exception
   when V_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_gera_ctb_sped_i200 ' || Chr(10) || p_des_erro;

   when others then
      p_des_erro := 'Outros erros na procedure inter_pr_gera_ctb_sped_i200 ' || Chr(10) || SQLERRM;
end inter_pr_gera_ctb_sped_i200;


/
exec inter_pr_recompile;
