alter table obrf_070
add (
    tipo_geracao_pedido number(1) default 1,
    perc_valor_pedido number(15,2) default 0.00,
    materia_prima_nivel varchar2(1) default '0',
    materia_prima_grupo varchar2(5) default '00000',
    materia_prima_subgrupo varchar2(3) default '000',
    materia_prima_item varchar2(6) default '000000'
)
