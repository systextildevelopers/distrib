CREATE OR REPLACE VIEW obrf_889_ABERTA
(
  ORDEM_SERVICO,
  SEQ_ORDEM_SERVICO,
  SITUACAO_ITEM, 
  NUMERO_SOLICITACAO,
  TIPO_SOLICITACAO_CONSERTO,
  TAG_ATUALIZA_CONSERTO,
  NIVEL_ESTRUTURA,
  GRUPO_ESTRUTURA,
  SUBGRUPO_ESTRUTURA,
  ITEM_ESTRUTURA,
  PERIODO_PRODUCAO,
  ORDEM_PRODUCAO,
  ORDEM_CONFECCAO,
  SEQ_ORDEM_CONFECCAO,
  CODIGO_BARRAS
)
AS 
select obrf_889.ordem_servico                         as ordem_servico,
       obrf_889.sequencia                             as seq_ordem_servico,
       obrf_889.situacao_item                         as situacao_item, 
       obrf_889.numero_solicitacao                    as numero_solicitacao,
       obrf_889.tipo_solicitacao_conserto             as tipo_solicitacao_conserto,
       obrf_889.tag_atualiza_conserto                 as tag_atualiza_conserto,
       obrf_889.nivel_estrutura                       as nivel_estrutura,
       obrf_889.grupo_estrutura                       as grupo_estrutura,
       obrf_889.subgrupo_estrutura                    as subgrupo_estrutura,
       obrf_889.item_estrutura                        as item_estrutura,
       obrf_889.periodo_producao                      as periodo_producao,
       obrf_889.ordem_producao                        as ordem_producao,
       obrf_889.ordem_confeccao                       as ordem_confeccao,
       obrf_889.seq_ordem_confeccao                   as seq_ordem_confeccao,
       obrf_889.codigo_barras                         as codigo_barras
 from obrf_889;
/
