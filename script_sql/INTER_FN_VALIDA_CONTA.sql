create or replace FUNCTION INTER_FN_VALIDA_CONTA (p_cod_empresa          in number,
                                                   p_conta_contab        in number,
                                                   p_data_movimento      in date)
                                                   return number   is v_conta_contab_ret  number;

v_empresa_matriz             fatu_500.codigo_matriz%type;
v_plano_conta_doc            cont_500.cod_plano_cta%type;
v_conta_contab               cont_550.conta_contabil%type;
v_cont                       number(1);

begin
   v_conta_contab_ret := 0;
   v_conta_contab     := -99;

  begin
   select fatu_500.codigo_matriz
   into v_empresa_matriz
   from fatu_500
   where fatu_500.codigo_empresa = p_cod_empresa;
  exception
    when no_data_found then
     v_empresa_matriz := 0;
  end;

  
     v_plano_conta_doc := 0;
     begin
      select cont_500.cod_plano_cta
      into   v_plano_conta_doc
      from cont_500
      where cont_500.cod_empresa  = v_empresa_matriz
        and p_data_movimento between cont_500.per_inicial and cont_500.per_final
        and cont_500.situacao     = 0;
     exception
       when no_data_found then
        v_plano_conta_doc := -99;
     end;

     if v_plano_conta_doc <> -99
     then

    
          begin
           select cont_535.cod_reduzido
           into   v_conta_contab
           from cont_535
           where cont_535.cod_plano_cta = v_plano_conta_doc
             and cont_535.cod_reduzido  = p_conta_contab;
          exception
            when no_data_found then
             v_conta_contab := -99;
          end;
         
     else
      v_conta_contab := -99;
     end if; --if v_plano_conta_doc <> -99
 


   v_conta_contab_ret := v_conta_contab;

   return(v_conta_contab_ret);
end INTER_FN_VALIDA_CONTA;
