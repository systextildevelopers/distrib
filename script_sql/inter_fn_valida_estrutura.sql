
  CREATE OR REPLACE FUNCTION "INTER_FN_VALIDA_ESTRUTURA" (
   p_nivel_produto     in varchar2,
   p_grupo_produto     in varchar2,
   p_subgrupo_produto  in varchar2,
   p_item_produto      in varchar2,
   p_alt_produto       in number,
   
   p_nivel_comparar    in varchar2,
   p_grupo_comparar    in varchar2,
   p_subgrupo_comparar in varchar2,
   p_item_comparar     in varchar2,
   p_alt_comparar      in number)

return varchar2 is
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_alternativa_produto       number;
   v_alternativa_comparar      number;
   v_nro_nivel_1_produto       number;
   v_nro_nivel_1_comparar      number;
   v_consumo_comp_aux          number;
   v_consumo_comp_aux_comparar number;
   
   v_tem_mesma_seq             number;
   r_msg_erro                  varchar2(255);

/*
   Funcao para encontrar a alternativa do produto
*/   
FUNCTION f_alternativa_produto (
   p_nivel     in varchar2,
   p_grupo     in varchar2,
   p_subgrupo  in varchar2,
   p_item      in varchar2)
return number is
   r_alternativa  number;
BEGIN
   r_alternativa := 0;
   
   begin
      select basi_010.alternativa_acabado
      into   r_alternativa
      from basi_010
      where basi_010.nivel_estrutura  = p_nivel
        and basi_010.grupo_estrutura  = p_grupo
        and basi_010.subgru_estrutura = p_subgrupo
        and basi_010.item_estrutura   = p_item;
   exception when others then
      r_alternativa := 0;
   end;

   return (r_alternativa);

END;

/*
   Funcao para encontrar quantos nivel 1 tem dentro do produto
*/   
FUNCTION f_nro_de_itens_estrutura (
   p_nivel       in varchar2,
   p_grupo       in varchar2,
   p_subgrupo    in varchar2,
   p_item        in varchar2,
   p_alternativa in number)
return number is
   r_nro_registros  number;
BEGIN
   r_nro_registros := 0;
   
   begin
      select nvl(count(*),0)
      into r_nro_registros
      from basi_050
      where basi_050.nivel_item        = p_nivel
        and basi_050.grupo_item        = p_grupo
        and (basi_050.sub_item         = p_subgrupo
        or   basi_050.sub_item         = '000')
        and (basi_050.item_item        = p_item
        or   basi_050.item_item        = '000000')
        and basi_050.alternativa_item  = p_alternativa
        and basi_050.nivel_comp        = '1';
   end;
   
   return (r_nro_registros);
   
END;


begin
   r_msg_erro := '';
   
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   
   /*
      Busca a alternativa padrao dos produtos
   */
   if p_alt_produto = 0
   then 
      v_alternativa_produto := f_alternativa_produto(p_nivel_produto,
                                                     p_grupo_produto,
                                                     p_subgrupo_produto,
                                                     p_item_produto);
   else
      v_alternativa_produto := p_alt_produto;
   end if;
   
   if p_alt_comparar = 0
   then
      v_alternativa_comparar := f_alternativa_produto(p_nivel_comparar,
                                                      p_grupo_comparar,
                                                      p_subgrupo_comparar,
                                                      p_item_comparar);
   else
      v_alternativa_comparar := p_alt_comparar;
   end if;
   
   /*
      Busca quantos componente do nivel 1 tem estes produtos
   */
   v_nro_nivel_1_produto  := f_nro_de_itens_estrutura (p_nivel_produto,
                                                       p_grupo_produto,
                                                       p_subgrupo_produto,
                                                       p_item_produto,
                                                       v_alternativa_produto);
   
   v_nro_nivel_1_comparar := f_nro_de_itens_estrutura (p_nivel_comparar,
                                                       p_grupo_comparar,
                                                       p_subgrupo_comparar,
                                                       p_item_comparar,
                                                       v_alternativa_produto);
   
   if v_nro_nivel_1_produto = v_nro_nivel_1_comparar
   then
      /*
         Verificar o consumo dos niveis 1 se sao iguais
      */
      for reg_basi_050 in (select basi_050.sub_comp,         basi_050.item_comp,
                                  basi_050.sequencia,        basi_050.sub_item,
                                  basi_050.item_item,        basi_050.consumo
                           from basi_050
                           where basi_050.nivel_item        = p_nivel_produto
                             and basi_050.grupo_item        = p_grupo_produto
                             and (basi_050.sub_item         = p_subgrupo_produto 
                             or   basi_050.sub_item         = '000')
                             and (basi_050.item_item        = p_item_produto
                             or   basi_050.item_item        = '000000')
                             and basi_050.alternativa_item  = v_alternativa_produto
                             and basi_050.nivel_comp        = '1'
                           order by basi_050.sequencia)
      loop
         v_tem_mesma_seq := 0;
         
         begin
            select 1
            into   v_tem_mesma_seq
            from basi_050
            where basi_050.nivel_item        = p_nivel_comparar
              and basi_050.grupo_item        = p_grupo_comparar
              and (basi_050.sub_item         = p_subgrupo_comparar
              or   basi_050.sub_item         = '000')
              and (basi_050.item_item        = p_item_comparar
              or   basi_050.item_item        = '000000')
              and basi_050.alternativa_item  = v_alternativa_comparar
              and basi_050.nivel_comp        = '1'
              and basi_050.sequencia         = reg_basi_050.sequencia;
         exception when no_data_found then
            v_tem_mesma_seq := 0;
         end;
         
         if v_tem_mesma_seq = 0
         then
            r_msg_erro := 'ATENCAO! O produto do pedido de reserva 9.99999.999.9999 nao tem sequencia de estrutura 999 do pedido de venda' || to_char(reg_basi_050.sequencia);
            exit;
         end if;
         
         if reg_basi_050.sub_comp = '000'
         then
            begin
               select basi_040.consumo
               into   v_consumo_comp_aux
               from basi_040
               where basi_040.nivel_item       = p_nivel_produto
                 and basi_040.grupo_item       = p_grupo_produto
                 and basi_040.sub_item         = p_subgrupo_produto
                 and basi_040.item_item        = reg_basi_050.item_item
                 and basi_040.sequencia        = reg_basi_050.sequencia
                 and basi_040.alternativa_item = v_alternativa_produto;
            exception when no_data_found then
               v_consumo_comp_aux := 0.000000;
            end;
         else
            v_consumo_comp_aux  := reg_basi_050.consumo;
         end if;
         
         for reg_basi_050c in (select basi_050.sequencia,        basi_050.item_item,
                                      basi_050.consumo,          basi_050.sub_comp
                               from basi_050
                               where basi_050.nivel_item        = p_nivel_comparar
                                 and basi_050.grupo_item        = p_grupo_comparar
                                 and (basi_050.sub_item         = p_subgrupo_comparar
                                 or   basi_050.sub_item         = '000')
                                 and (basi_050.item_item        = p_item_comparar
                                 or   basi_050.item_item        = '000000')
                                 and basi_050.alternativa_item  = v_alternativa_comparar
                                 and basi_050.sequencia         = reg_basi_050.sequencia)
         loop
            if reg_basi_050c.sub_comp = '000'
            then
               begin
                  select basi_040.consumo
                  into   v_consumo_comp_aux_comparar
                  from basi_040
                  where basi_040.nivel_item       = p_nivel_comparar
                    and basi_040.grupo_item       = p_grupo_comparar
                    and basi_040.sub_item         = p_subgrupo_comparar
                    and basi_040.item_item        = reg_basi_050c.item_item
                    and basi_040.sequencia        = reg_basi_050c.sequencia
                    and basi_040.alternativa_item = v_alternativa_comparar;
               exception when no_data_found then
                  v_consumo_comp_aux_comparar := 0.000000;
               end;
            else
               v_consumo_comp_aux_comparar  := reg_basi_050c.consumo;
            end if;
            
            if v_consumo_comp_aux_comparar <> v_consumo_comp_aux
            then
               r_msg_erro := 'ATENCAO! O consumo do produto 9.9999.99.999 do pedido de reserva esta diferente na sequencia de estrutura 999 do produto do pedido de venda';
               exit;
            end if;
         end loop;
         
         if v_consumo_comp_aux_comparar <> v_consumo_comp_aux
         then
            exit;
         end if;
         
      end loop;
   else
      r_msg_erro := 'ATENCAO! O produto do pedido de reserva nao tem o mesmo nro de componente de nivel 1 para o produto informando no pedido de venda';
      r_msg_erro := inter_fn_buscar_tag('lb05455#SEQUeNCIA:',ws_locale_usuario,ws_usuario_systextil);
   end if;   
   
   return(r_msg_erro);

end inter_fn_valida_estrutura;
--raise_application_error(-20000,'Erro ao atualizar tabela EXEC_015.');



 

/

exec inter_pr_recompile;

