alter table basi_023
add (faixa_etaria_custos number(3));

comment on column basi_023.faixa_etaria_custos
is 'Faixa etária de custo que o tamanho grande.';

alter table basi_023
modify faixa_etaria_custos default 0;

execute inter_pr_recompile;     
/

declare
cursor basi_023_c is
   select f.rowid from basi_023 f where faixa_etaria_custos is null;
   
contaRegistro number;

begin
   contaRegistro := 0;
   
   for reg_basi_023_c in basi_023_c
   loop
      update basi_023
      set faixa_etaria_custos   = 0
      where faixa_etaria_custos is null
        and basi_023.rowid = reg_basi_023_c.rowid;
      
      contaRegistro := contaRegistro + 1;
      
      if contaRegistro = 1000
      then
         contaRegistro := 0;
         commit;
      end if;
   end loop; 

   commit;
end;
/
