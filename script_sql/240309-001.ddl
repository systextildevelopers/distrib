create table obrf_202 (
  EMPRESA    NUMBER(3) default 0 not null,
  MARCA      NUMBER(9) default 0 not null,
  DESCRICAO  VARCHAR2(60) default ''
);


insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f202', 'Relacionamento de empresa com marca para entradas fiscais',1,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f202', ' ' ,1, 1, 'S', 'S', 'S', 'S');
