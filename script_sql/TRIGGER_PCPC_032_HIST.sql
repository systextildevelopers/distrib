create or replace trigger "TRIGGER_PCPC_032_HIST"
before update of PCPC0302_ORPROCOR,
                 PCPC0302_SEQORCOR,
                 PCPC0302_SEQUENOR,
                 SORTIMENTO_PECA,
                 TECORDCO_NIVEL99,
                 TECORDCO_GRUPO,
                 TECORDCO_SUBGRUPO,
                 TECORDCO_ITEM,
                 CODIGO_DEPOSITO,
                 QTDE_KG_PROG,
                 QTDE_KG_REAL,
                 SORTIMENTO_PANO,
                 QTDE_SOBRA_FALTA,
                 QTDE_ENFE_PROG,
                 QTDE_ENFE_REAL,
                 PESO_PECA,
                 RESIDUO,
                 ENCAIXE,
                 SOBRAS,
                 ORDEM_PRODUCAO,
                 TIPO_CORTE,
                 QTDE_METROS,
                 QTDE_KG_COLETADOS,
                 CORREDOR,
                 BOX,
                 PERC_EFICIENCIA,
                 PERC_REJEICAO,
                 PESO_POR_CAMADA,
                 COMPRIMENTO_ENFESTO,
                 QTDE_DEBRUM,
                 QTDE_TAPETE,
                 QTDE_VIVO,
                 QTDE_PONTAS,
                 QTDE_EMPALME,
                 QTDE_RECUPERACAO,
                 QTDE_ENTRE_CORTE,
                 TOTAL_DEVOLUCAO_PRIMEIRA,
                 TOTAL_DEVOLUCAO_SEGUNDA,
                 TOTAL_REJEICAO,
                 TOTAL_DEPURADO,
                 NUMERO_CAD,
                 QTDE_METROS_OPTIPLAN,
                 LARG_MINIMA,
                 CENTRO_CUSTO,
                 QTDE_METROS_POR_PECA,
                 OBSERVACAO_APROV_CORTE
       or delete or
       insert on pcpc_032
for each row
declare

   ws_sid                     number;
   ws_usuario_systextil       hdoc_030.usuario%type;
   ws_nome_programa           pcpt_020_hist.nome_programa%type;
   ws_aplicativo              varchar2(20);
   ws_gravar_log              varchar2(20);

   ws_empresa                 number(3);
   ws_locale_usuario          varchar2(5);
   ws_usuario_rede            pcpc_032_hist.usuario_rede%type;
   ws_maquina_rede            pcpc_032_hist.maquina_rede%type;
   ws_tipo_ocorr              pcpc_032_hist.tipo_ocorr%type;

   ws_PCPC0302_ORPROCOR             pcpc_032.PCPC0302_ORPROCOR%type;
   ws_PCPC0302_SEQORCOR             pcpc_032.PCPC0302_SEQORCOR%type;
   ws_PCPC0302_SEQUENOR             pcpc_032.PCPC0302_SEQUENOR%type;
   ws_SORTIMENTO_PECA               pcpc_032.SORTIMENTO_PECA%type;
   ws_TECORDCO_NIVEL99_NEW          pcpc_032.TECORDCO_NIVEL99%type;

   ws_TECORDCO_NIVEL99_OLD          pcpc_032.TECORDCO_NIVEL99%type;
   ws_TECORDCO_GRUPO_NEW            pcpc_032.TECORDCO_GRUPO%type;
   ws_TECORDCO_GRUPO_OLD            pcpc_032.TECORDCO_GRUPO%type;
   ws_TECORDCO_SUBGRUPO             pcpc_032.TECORDCO_SUBGRUPO%type;
   ws_TECORDCO_ITEM_NEW             pcpc_032.TECORDCO_ITEM%type;
   ws_TECORDCO_ITEM_OLD             pcpc_032.TECORDCO_ITEM%type;
   ws_CODIGO_DEPOSITO_NEW           pcpc_032.CODIGO_DEPOSITO%type;
   ws_CODIGO_DEPOSITO_OLD           pcpc_032.CODIGO_DEPOSITO%type;
   ws_QTDE_KG_PROG_NEW              pcpc_032.QTDE_KG_PROG%type;
   ws_QTDE_KG_PROG_OLD              pcpc_032.QTDE_KG_PROG%type;
   ws_QTDE_KG_REAL_NEW              pcpc_032.QTDE_KG_REAL%type;
   ws_QTDE_KG_REAL_OLD              pcpc_032.QTDE_KG_REAL%type;
   ws_SORTIMENTO_PANO_NEW           pcpc_032.SORTIMENTO_PANO%type;
   ws_SORTIMENTO_PANO_OLD           pcpc_032.SORTIMENTO_PANO%type;
   ws_QTDE_SOBRA_FALTA_NEW          pcpc_032.QTDE_SOBRA_FALTA%type;
   ws_QTDE_SOBRA_FALTA_OLD          pcpc_032.QTDE_SOBRA_FALTA%type;
   ws_QTDE_ENFE_PROG_NEW            pcpc_032.QTDE_ENFE_PROG%type;
   ws_QTDE_ENFE_PROG_OLD            pcpc_032.QTDE_ENFE_PROG%type;
   ws_QTDE_ENFE_REAL_NEW            pcpc_032.QTDE_ENFE_REAL%type;
   ws_QTDE_ENFE_REAL_OLD            pcpc_032.QTDE_ENFE_REAL%type;
   ws_PESO_PECA_NEW                 pcpc_032.PESO_PECA%type;
   ws_PESO_PECA_OLD                 pcpc_032.PESO_PECA%type;
   ws_RESIDUO_NEW                   pcpc_032.RESIDUO%type;
   ws_RESIDUO_OLD                   pcpc_032.RESIDUO%type;
   ws_ENCAIXE_NEW                   pcpc_032.ENCAIXE%type;
   ws_ENCAIXE_OLD                   pcpc_032.ENCAIXE%type;
   ws_SOBRAS_NEW                    pcpc_032.SOBRAS%type;
   ws_SOBRAS_OLD                    pcpc_032.SOBRAS%type;
   ws_ORDEM_PRODUCAO_NEW            pcpc_032.ORDEM_PRODUCAO%type;
   ws_ORDEM_PRODUCAO_OLD            pcpc_032.ORDEM_PRODUCAO%type;
   ws_TIPO_CORTE_NEW                pcpc_032.TIPO_CORTE%type;
   ws_TIPO_CORTE_OLD                pcpc_032.TIPO_CORTE%type;
   ws_QTDE_METROS_NEW               pcpc_032.QTDE_METROS%type;
   ws_QTDE_METROS_OLD               pcpc_032.QTDE_METROS%type;
   ws_QTDE_KG_COLETADOS_NEW         pcpc_032.QTDE_KG_COLETADOS%type;
   ws_QTDE_KG_COLETADOS_OLD         pcpc_032.QTDE_KG_COLETADOS%type;
   ws_CORREDOR_NEW                  pcpc_032.CORREDOR%type;
   ws_CORREDOR_OLD                  pcpc_032.CORREDOR%type;
   ws_BOX_NEW                       pcpc_032.BOX%type;
   ws_BOX_OLD                       pcpc_032.BOX%type;
   ws_PERC_EFICIENCIA_NEW           pcpc_032.PERC_EFICIENCIA%type;
   ws_PERC_EFICIENCIA_OLD           pcpc_032.PERC_EFICIENCIA%type;
   ws_PERC_REJEICAO_NEW             pcpc_032.PERC_REJEICAO%type;
   ws_PERC_REJEICAO_OLD             pcpc_032.PERC_REJEICAO%type;
   ws_PESO_POR_CAMADA_NEW           pcpc_032.PESO_POR_CAMADA%type;
   ws_PESO_POR_CAMADA_OLD           pcpc_032.PESO_POR_CAMADA%type;
   ws_COMPRIMENTO_ENFESTO_NEW       pcpc_032.COMPRIMENTO_ENFESTO%type;
   ws_COMPRIMENTO_ENFESTO_OLD       pcpc_032.COMPRIMENTO_ENFESTO%type;
   ws_QTDE_DEBRUM_NEW               pcpc_032.QTDE_DEBRUM%type;
   ws_QTDE_DEBRUM_OLD               pcpc_032.QTDE_DEBRUM%type;
   ws_QTDE_TAPETE_NEW               pcpc_032.QTDE_TAPETE%type;
   ws_QTDE_TAPETE_OLD               pcpc_032.QTDE_TAPETE%type;
   ws_QTDE_VIVO_NEW                 pcpc_032.QTDE_VIVO%type;
   ws_QTDE_VIVO_OLD                 pcpc_032.QTDE_VIVO%type;
   ws_QTDE_PONTAS_NEW               pcpc_032.QTDE_PONTAS%type;
   ws_QTDE_PONTAS_OLD               pcpc_032.QTDE_PONTAS%type;
   ws_QTDE_EMPALME_NEW              pcpc_032.QTDE_EMPALME%type;
   ws_QTDE_EMPALME_OLD              pcpc_032.QTDE_EMPALME%type;
   ws_QTDE_RECUPERACAO_NEW          pcpc_032.QTDE_RECUPERACAO%type;
   ws_QTDE_RECUPERACAO_OLD          pcpc_032.QTDE_RECUPERACAO%type;
   ws_QTDE_ENTRE_CORTE_NEW          pcpc_032.QTDE_ENTRE_CORTE%type;
   ws_QTDE_ENTRE_CORTE_OLD          pcpc_032.QTDE_ENTRE_CORTE%type;
   ws_TOTAL_DEV_PRIMEIRA_NEW        pcpc_032.TOTAL_DEVOLUCAO_PRIMEIRA%type;
   ws_TOTAL_DEV_PRIMEIRA_OLD        pcpc_032.TOTAL_DEVOLUCAO_PRIMEIRA%type;
   ws_TOTAL_DEV_SEGUNDA_NEW         pcpc_032.TOTAL_DEVOLUCAO_SEGUNDA%type;
   ws_TOTAL_DEV_SEGUNDA_OLD         pcpc_032.TOTAL_DEVOLUCAO_SEGUNDA%type;
   ws_TOTAL_REJEICAO_NEW            pcpc_032.TOTAL_REJEICAO%type;
   ws_TOTAL_REJEICAO_OLD            pcpc_032.TOTAL_REJEICAO%type;
   ws_TOTAL_DEPURADO_NEW            pcpc_032.TOTAL_DEPURADO%type;
   ws_TOTAL_DEPURADO_OLD            pcpc_032.TOTAL_DEPURADO%type;
   ws_NUMERO_CAD_NEW                pcpc_032.NUMERO_CAD%type;
   ws_NUMERO_CAD_OLD                pcpc_032.NUMERO_CAD%type;
   ws_QTDE_METROS_OPTIPLAN_NEW      pcpc_032.QTDE_METROS_OPTIPLAN%type;
   ws_QTDE_METROS_OPTIPLAN_OLD      pcpc_032.QTDE_METROS_OPTIPLAN%type;
   ws_LARG_MINIMA_NEW               pcpc_032.LARG_MINIMA%type;
   ws_LARG_MINIMA_OLD               pcpc_032.LARG_MINIMA%type;
   ws_CENTRO_CUSTO_NEW              pcpc_032.CENTRO_CUSTO%type;
   ws_CENTRO_CUSTO_OLD              pcpc_032.CENTRO_CUSTO%type;

begin

      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                              ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
     
     ws_nome_programa := inter_fn_nome_programa(ws_sid);
     
     ws_gravar_log := 'N';
     
     begin
        select val_str
        into ws_gravar_log
        from empr_008
        where codigo_empresa = ws_empresa
        and   param          = 'pcpc.logTecidosOP';
      exception when others
         then ws_gravar_log := 'N';
      end;
      
      if (ws_gravar_log = 'S') then
      
         if INSERTING then
             ws_tipo_ocorr               := 'I';

             ws_PCPC0302_ORPROCOR                   := :new.PCPC0302_ORPROCOR;
             ws_PCPC0302_SEQORCOR                   := :new.PCPC0302_SEQORCOR;
             ws_PCPC0302_SEQUENOR                   := :new.PCPC0302_SEQUENOR;
             ws_SORTIMENTO_PECA                     := :new.SORTIMENTO_PECA;
             ws_TECORDCO_NIVEL99_NEW                := :new.TECORDCO_NIVEL99;
             ws_TECORDCO_NIVEL99_OLD                := null;
             ws_TECORDCO_GRUPO_NEW                  := :new.TECORDCO_GRUPO;
             ws_TECORDCO_GRUPO_OLD                  := null;
             ws_TECORDCO_SUBGRUPO                   := :new.TECORDCO_SUBGRUPO;
             ws_TECORDCO_ITEM_NEW                   := :new.TECORDCO_ITEM;
             ws_TECORDCO_ITEM_OLD                   := null;
             ws_CODIGO_DEPOSITO_NEW                 := :new.CODIGO_DEPOSITO;
             ws_CODIGO_DEPOSITO_OLD                 := 0;
             ws_QTDE_KG_PROG_NEW                    := :new.QTDE_KG_PROG;
             ws_QTDE_KG_PROG_OLD                    := 0;
             ws_QTDE_KG_REAL_NEW                    := :new.QTDE_KG_REAL;
             ws_QTDE_KG_REAL_OLD                    := 0;
             ws_SORTIMENTO_PANO_NEW                 := :new.SORTIMENTO_PANO;
             ws_SORTIMENTO_PANO_OLD                 := null;
             ws_QTDE_SOBRA_FALTA_NEW                := :new.QTDE_SOBRA_FALTA;
             ws_QTDE_SOBRA_FALTA_OLD                := 0;
             ws_QTDE_ENFE_PROG_NEW                  := :new.QTDE_ENFE_PROG;
             ws_QTDE_ENFE_PROG_OLD                  := 0;
             ws_QTDE_ENFE_REAL_NEW                  := :new.QTDE_ENFE_REAL;
             ws_QTDE_ENFE_REAL_OLD                  := 0;
             ws_PESO_PECA_NEW                       := :new.PESO_PECA;
             ws_PESO_PECA_OLD                       := 0;
             ws_RESIDUO_NEW                         := :new.RESIDUO;
             ws_RESIDUO_OLD                         := 0;
             ws_ENCAIXE_NEW                         := :new.ENCAIXE;
             ws_ENCAIXE_OLD                         := 0;
             ws_SOBRAS_NEW                          := :new.SOBRAS;
             ws_SOBRAS_OLD                          := 0;
             ws_ORDEM_PRODUCAO_NEW                  := :new.ORDEM_PRODUCAO;
             ws_ORDEM_PRODUCAO_OLD                  := 0;
             ws_TIPO_CORTE_NEW                      := :new.TIPO_CORTE;
             ws_TIPO_CORTE_OLD                      := 0;
             ws_QTDE_METROS_NEW                     := :new.QTDE_METROS;
             ws_QTDE_METROS_OLD                     := 0;
             ws_QTDE_KG_COLETADOS_NEW               := :new.QTDE_KG_COLETADOS;
             ws_QTDE_KG_COLETADOS_OLD               := 0;
             ws_CORREDOR_NEW                        := :new.CORREDOR;
             ws_CORREDOR_OLD                        := 0;
             ws_BOX_NEW                             := :new.BOX;
             ws_BOX_OLD                             := 0;
             ws_PERC_EFICIENCIA_NEW                 := :new.PERC_EFICIENCIA;
             ws_PERC_EFICIENCIA_OLD                 := 0;
             ws_PERC_REJEICAO_NEW                   := :new.PERC_REJEICAO;
             ws_PERC_REJEICAO_OLD                   := 0;
             ws_PESO_POR_CAMADA_NEW                 := :new.PESO_POR_CAMADA;
             ws_PESO_POR_CAMADA_OLD                 := 0;
             ws_COMPRIMENTO_ENFESTO_NEW             := :new.COMPRIMENTO_ENFESTO;
             ws_COMPRIMENTO_ENFESTO_OLD             := 0;
             ws_QTDE_DEBRUM_NEW                     := :new.QTDE_DEBRUM;
             ws_QTDE_DEBRUM_OLD                     := 0;
             ws_QTDE_TAPETE_NEW                     := :new.QTDE_TAPETE;
             ws_QTDE_TAPETE_OLD                     := 0;
             ws_QTDE_VIVO_NEW                       := :new.QTDE_VIVO;
             ws_QTDE_VIVO_OLD                       := 0;
             ws_QTDE_PONTAS_NEW                     := :new.QTDE_PONTAS;
             ws_QTDE_PONTAS_OLD                     := 0;
             ws_QTDE_EMPALME_NEW                    := :new.QTDE_EMPALME;
             ws_QTDE_EMPALME_OLD                    := 0;
             ws_QTDE_RECUPERACAO_NEW                := :new.QTDE_RECUPERACAO;
             ws_QTDE_RECUPERACAO_OLD                := 0;
             ws_QTDE_ENTRE_CORTE_NEW                := :new.QTDE_ENTRE_CORTE;
             ws_QTDE_ENTRE_CORTE_OLD                := 0;
             ws_TOTAL_DEV_PRIMEIRA_NEW              := :new.TOTAL_DEVOLUCAO_PRIMEIRA;
             ws_TOTAL_DEV_PRIMEIRA_OLD              := 0;
             ws_TOTAL_DEV_SEGUNDA_NEW               := :new.TOTAL_DEVOLUCAO_SEGUNDA;
             ws_TOTAL_DEV_SEGUNDA_OLD               := 0;
             ws_TOTAL_REJEICAO_NEW                  := :new.TOTAL_REJEICAO;
             ws_TOTAL_REJEICAO_OLD                  := 0;
             ws_TOTAL_DEPURADO_NEW                  := :new.TOTAL_DEPURADO;
             ws_TOTAL_DEPURADO_OLD                  := 0;
             ws_NUMERO_CAD_NEW                      := :new.NUMERO_CAD;
             ws_NUMERO_CAD_OLD                      := null;
             ws_QTDE_METROS_OPTIPLAN_NEW            := :new.QTDE_METROS_OPTIPLAN;
             ws_QTDE_METROS_OPTIPLAN_OLD            := 0;
             ws_LARG_MINIMA_NEW                     := :new.LARG_MINIMA;
             ws_LARG_MINIMA_OLD                     := 0;
             ws_CENTRO_CUSTO_NEW                    := :new.CENTRO_CUSTO;
             ws_CENTRO_CUSTO_OLD                    := 0;
         elsif UPDATING then
            ws_tipo_ocorr               := 'A';

            ws_PCPC0302_ORPROCOR                   := :new.PCPC0302_ORPROCOR;
            ws_PCPC0302_SEQORCOR                   := :new.PCPC0302_SEQORCOR;
            ws_PCPC0302_SEQUENOR                   := :new.PCPC0302_SEQUENOR;
            ws_SORTIMENTO_PECA                     := :new.SORTIMENTO_PECA;
            ws_TECORDCO_NIVEL99_NEW                := :new.TECORDCO_NIVEL99;
            ws_TECORDCO_NIVEL99_OLD                := :old.TECORDCO_NIVEL99;
            ws_TECORDCO_GRUPO_NEW                  := :new.TECORDCO_GRUPO;
            ws_TECORDCO_GRUPO_OLD                  := :old.TECORDCO_GRUPO;
            ws_TECORDCO_SUBGRUPO                   := :new.TECORDCO_SUBGRUPO;
            ws_TECORDCO_ITEM_NEW                   := :new.TECORDCO_ITEM;
            ws_TECORDCO_ITEM_OLD                   := :old.TECORDCO_ITEM;
            ws_CODIGO_DEPOSITO_NEW                 := :new.CODIGO_DEPOSITO;
            ws_CODIGO_DEPOSITO_OLD                 := :old.CODIGO_DEPOSITO;
            ws_QTDE_KG_PROG_NEW                    := :new.QTDE_KG_PROG;
            ws_QTDE_KG_PROG_OLD                    := :old.QTDE_KG_PROG;
            ws_QTDE_KG_REAL_NEW                    := :new.QTDE_KG_REAL;
            ws_QTDE_KG_REAL_OLD                    := :old.QTDE_KG_REAL;
            ws_SORTIMENTO_PANO_NEW                 := :new.SORTIMENTO_PANO;
            ws_SORTIMENTO_PANO_OLD                 := :old.SORTIMENTO_PANO;
            ws_QTDE_SOBRA_FALTA_NEW                := :new.QTDE_SOBRA_FALTA;
            ws_QTDE_SOBRA_FALTA_OLD                := :old.QTDE_SOBRA_FALTA;
            ws_QTDE_ENFE_PROG_NEW                  := :new.QTDE_ENFE_PROG;
            ws_QTDE_ENFE_PROG_OLD                  := :old.QTDE_ENFE_PROG;
            ws_QTDE_ENFE_REAL_NEW                  := :new.QTDE_ENFE_REAL;
            ws_QTDE_ENFE_REAL_OLD                  := :old.QTDE_ENFE_REAL;
            ws_PESO_PECA_NEW                       := :new.PESO_PECA;
            ws_PESO_PECA_OLD                       := :old.PESO_PECA;
            ws_RESIDUO_NEW                         := :new.RESIDUO;
            ws_RESIDUO_OLD                         := :old.RESIDUO;
            ws_ENCAIXE_NEW                         := :new.ENCAIXE;
            ws_ENCAIXE_OLD                         := :old.ENCAIXE;
            ws_SOBRAS_NEW                          := :new.SOBRAS;
            ws_SOBRAS_OLD                          := :old.SOBRAS;
            ws_ORDEM_PRODUCAO_NEW                  := :new.ORDEM_PRODUCAO;
            ws_ORDEM_PRODUCAO_OLD                  := :old.ORDEM_PRODUCAO;
            ws_TIPO_CORTE_NEW                      := :new.TIPO_CORTE;
            ws_TIPO_CORTE_OLD                      := :old.TIPO_CORTE;
            ws_QTDE_METROS_NEW                     := :new.QTDE_METROS;
            ws_QTDE_METROS_OLD                     := :old.QTDE_METROS;
            ws_QTDE_KG_COLETADOS_NEW               := :new.QTDE_KG_COLETADOS;
            ws_QTDE_KG_COLETADOS_OLD               := :old.QTDE_KG_COLETADOS;
            ws_CORREDOR_NEW                        := :new.CORREDOR;
            ws_CORREDOR_OLD                        := :old.CORREDOR;
            ws_BOX_NEW                             := :new.BOX;
            ws_BOX_OLD                             := :old.BOX;
            ws_PERC_EFICIENCIA_NEW                 := :new.PERC_EFICIENCIA;
            ws_PERC_EFICIENCIA_OLD                 := :old.PERC_EFICIENCIA;
            ws_PERC_REJEICAO_NEW                   := :new.PERC_REJEICAO;
            ws_PERC_REJEICAO_OLD                   := :old.PERC_REJEICAO;
            ws_PESO_POR_CAMADA_NEW                 := :new.PESO_POR_CAMADA;
            ws_PESO_POR_CAMADA_OLD                 := :old.PESO_POR_CAMADA;
            ws_COMPRIMENTO_ENFESTO_NEW             := :new.COMPRIMENTO_ENFESTO;
            ws_COMPRIMENTO_ENFESTO_OLD             := :old.COMPRIMENTO_ENFESTO;
            ws_QTDE_DEBRUM_NEW                     := :new.QTDE_DEBRUM;
            ws_QTDE_DEBRUM_OLD                     := :old.QTDE_DEBRUM;
            ws_QTDE_TAPETE_NEW                     := :new.QTDE_TAPETE;
            ws_QTDE_TAPETE_OLD                     := :old.QTDE_TAPETE;
            ws_QTDE_VIVO_NEW                       := :new.QTDE_VIVO;
            ws_QTDE_VIVO_OLD                       := :old.QTDE_VIVO;
            ws_QTDE_PONTAS_NEW                     := :new.QTDE_PONTAS;
            ws_QTDE_PONTAS_OLD                     := :old.QTDE_PONTAS;
            ws_QTDE_EMPALME_NEW                    := :new.QTDE_EMPALME;
            ws_QTDE_EMPALME_OLD                    := :old.QTDE_EMPALME;
            ws_QTDE_RECUPERACAO_NEW                := :new.QTDE_RECUPERACAO;
            ws_QTDE_RECUPERACAO_OLD                := :old.QTDE_RECUPERACAO;
            ws_QTDE_ENTRE_CORTE_NEW                := :new.QTDE_ENTRE_CORTE;
            ws_QTDE_ENTRE_CORTE_OLD                := :old.QTDE_ENTRE_CORTE;
            ws_TOTAL_DEV_PRIMEIRA_NEW              := :new.TOTAL_DEVOLUCAO_PRIMEIRA;
            ws_TOTAL_DEV_PRIMEIRA_OLD              := :old.TOTAL_DEVOLUCAO_PRIMEIRA;
            ws_TOTAL_DEV_SEGUNDA_NEW               := :new.TOTAL_DEVOLUCAO_SEGUNDA;
            ws_TOTAL_DEV_SEGUNDA_OLD               := :old.TOTAL_DEVOLUCAO_SEGUNDA;
            ws_TOTAL_REJEICAO_NEW                  := :new.TOTAL_REJEICAO;
            ws_TOTAL_REJEICAO_OLD                  := :old.TOTAL_REJEICAO;
            ws_TOTAL_DEPURADO_NEW                  := :new.TOTAL_DEPURADO;
            ws_TOTAL_DEPURADO_OLD                  := :old.TOTAL_DEPURADO;
            ws_NUMERO_CAD_NEW                      := :new.NUMERO_CAD;
            ws_NUMERO_CAD_OLD                      := :old.NUMERO_CAD;
            ws_QTDE_METROS_OPTIPLAN_NEW            := :new.QTDE_METROS_OPTIPLAN;
            ws_QTDE_METROS_OPTIPLAN_OLD            := :old.QTDE_METROS_OPTIPLAN;
            ws_LARG_MINIMA_NEW                     := :new.LARG_MINIMA;
            ws_LARG_MINIMA_OLD                     := :old.LARG_MINIMA;
            ws_CENTRO_CUSTO_NEW                    := :new.CENTRO_CUSTO;
            ws_CENTRO_CUSTO_OLD                    := :old.CENTRO_CUSTO;

         elsif DELETING then
               ws_tipo_ocorr               := 'D';

               ws_PCPC0302_ORPROCOR                   := :old.PCPC0302_ORPROCOR;
               ws_PCPC0302_SEQORCOR                   := :old.PCPC0302_SEQORCOR;
               ws_PCPC0302_SEQUENOR                   := :old.PCPC0302_SEQUENOR;
               ws_SORTIMENTO_PECA                     := :old.SORTIMENTO_PECA;
               ws_TECORDCO_NIVEL99_NEW                := null;
               ws_TECORDCO_NIVEL99_OLD                := :old.TECORDCO_NIVEL99;
               ws_TECORDCO_GRUPO_NEW                  := null;
               ws_TECORDCO_GRUPO_OLD                  := :old.TECORDCO_GRUPO;
               ws_TECORDCO_SUBGRUPO                   := :old.TECORDCO_SUBGRUPO;
               ws_TECORDCO_ITEM_NEW                   :=  null;
               ws_TECORDCO_ITEM_OLD                   := :old.TECORDCO_ITEM;
               ws_CODIGO_DEPOSITO_NEW                 := 0;
               ws_CODIGO_DEPOSITO_OLD                 := :old.CODIGO_DEPOSITO;
               ws_QTDE_KG_PROG_NEW                    := 0;
               ws_QTDE_KG_PROG_OLD                    := :old.QTDE_KG_PROG;
               ws_QTDE_KG_REAL_NEW                    := 0;
               ws_QTDE_KG_REAL_OLD                    := :old.QTDE_KG_REAL;
               ws_SORTIMENTO_PANO_NEW                 := null;
               ws_SORTIMENTO_PANO_OLD                 := :old.SORTIMENTO_PANO;
               ws_QTDE_SOBRA_FALTA_NEW                := 0;
               ws_QTDE_SOBRA_FALTA_OLD                := :old.QTDE_SOBRA_FALTA;
               ws_QTDE_ENFE_PROG_NEW                  := 0;
               ws_QTDE_ENFE_PROG_OLD                  := :old.QTDE_ENFE_PROG;
               ws_QTDE_ENFE_REAL_NEW                  := 0;
               ws_QTDE_ENFE_REAL_OLD                  := :old.QTDE_ENFE_REAL;
               ws_PESO_PECA_NEW                       := 0;
               ws_PESO_PECA_OLD                       := :old.PESO_PECA;
               ws_RESIDUO_NEW                         := 0;
               ws_RESIDUO_OLD                         := :old.RESIDUO;
               ws_ENCAIXE_NEW                         := 0;
               ws_ENCAIXE_OLD                         := :old.ENCAIXE;
               ws_SOBRAS_NEW                          := 0;
               ws_SOBRAS_OLD                          := :old.SOBRAS;
               ws_ORDEM_PRODUCAO_NEW                  := 0;
               ws_ORDEM_PRODUCAO_OLD                  := :old.ORDEM_PRODUCAO;
               ws_TIPO_CORTE_NEW                      := 0;
               ws_TIPO_CORTE_OLD                      := :old.TIPO_CORTE;
               ws_QTDE_METROS_NEW                     := 0;
               ws_QTDE_METROS_OLD                     := :old.QTDE_METROS;
               ws_QTDE_KG_COLETADOS_NEW               := 0;
               ws_QTDE_KG_COLETADOS_OLD               := :old.QTDE_KG_COLETADOS;
               ws_CORREDOR_NEW                        := 0;
               ws_CORREDOR_OLD                        := :old.CORREDOR;
               ws_BOX_NEW                             := 0;
               ws_BOX_OLD                             := :old.BOX;
               ws_PERC_EFICIENCIA_NEW                 := 0;
               ws_PERC_EFICIENCIA_OLD                 := :old.PERC_EFICIENCIA;
               ws_PERC_REJEICAO_NEW                   := 0;
               ws_PERC_REJEICAO_OLD                   := :old.PERC_REJEICAO;
               ws_PESO_POR_CAMADA_NEW                 := 0;
               ws_PESO_POR_CAMADA_OLD                 := :old.PESO_POR_CAMADA;
               ws_COMPRIMENTO_ENFESTO_NEW             := 0;
               ws_COMPRIMENTO_ENFESTO_OLD             := :old.COMPRIMENTO_ENFESTO;
               ws_QTDE_DEBRUM_NEW                     := 0;
               ws_QTDE_DEBRUM_OLD                     := :old.QTDE_DEBRUM;
               ws_QTDE_TAPETE_NEW                     := 0;
               ws_QTDE_TAPETE_OLD                     := :old.QTDE_TAPETE;
               ws_QTDE_VIVO_NEW                       := 0;
               ws_QTDE_VIVO_OLD                       := :old.QTDE_VIVO;
               ws_QTDE_PONTAS_NEW                     := 0;
               ws_QTDE_PONTAS_OLD                     := :old.QTDE_PONTAS;
               ws_QTDE_EMPALME_NEW                    := 0;
               ws_QTDE_EMPALME_OLD                    := :old.QTDE_EMPALME;
               ws_QTDE_RECUPERACAO_NEW                := 0;
               ws_QTDE_RECUPERACAO_OLD                := :old.QTDE_RECUPERACAO;
               ws_QTDE_ENTRE_CORTE_NEW                := 0;
               ws_QTDE_ENTRE_CORTE_OLD                := :old.QTDE_ENTRE_CORTE;
               ws_TOTAL_DEV_PRIMEIRA_NEW              := 0;
               ws_TOTAL_DEV_PRIMEIRA_OLD              := :old.TOTAL_DEVOLUCAO_PRIMEIRA;
               ws_TOTAL_DEV_SEGUNDA_NEW               := 0;
               ws_TOTAL_DEV_SEGUNDA_OLD               := :old.TOTAL_DEVOLUCAO_SEGUNDA;
               ws_TOTAL_REJEICAO_NEW                  := 0;
               ws_TOTAL_REJEICAO_OLD                  := :old.TOTAL_REJEICAO;
               ws_TOTAL_DEPURADO_NEW                  := 0;
               ws_TOTAL_DEPURADO_OLD                  := :old.TOTAL_DEPURADO;
               ws_NUMERO_CAD_NEW                      := null;
               ws_NUMERO_CAD_OLD                      := :old.NUMERO_CAD;
               ws_QTDE_METROS_OPTIPLAN_NEW            := 0;
               ws_QTDE_METROS_OPTIPLAN_OLD            := :old.QTDE_METROS_OPTIPLAN;
               ws_LARG_MINIMA_NEW                     := 0;
               ws_LARG_MINIMA_OLD                     := :old.LARG_MINIMA;
               ws_CENTRO_CUSTO_NEW                    := 0;
               ws_CENTRO_CUSTO_OLD                    := :old.CENTRO_CUSTO;
         end if;

         INSERT INTO pcpc_032_hist (
         PCPC0302_ORPROCOR,                 PCPC0302_SEQORCOR,                     PCPC0302_SEQUENOR,
         SORTIMENTO_PECA,                   DATA_OCORR,                            TIPO_OCORR,
         NOME_PROGRAMA,                     APLICACAO,                             TECORDCO_NIVEL99_OLD,
         TECORDCO_NIVEL99_NEW,              TECORDCO_GRUPO_OLD,                    TECORDCO_GRUPO_NEW,
         TECORDCO_SUBGRUPO,                 TECORDCO_ITEM_OLD,                     TECORDCO_ITEM_NEW,
         CODIGO_DEPOSITO_OLD,               CODIGO_DEPOSITO_NEW,                   QTDE_KG_PROG_OLD,
         QTDE_KG_PROG_NEW,                  QTDE_KG_REAL_OLD,                      QTDE_KG_REAL_NEW,
         SORTIMENTO_PANO_OLD,               SORTIMENTO_PANO_NEW,                   QTDE_SOBRA_FALTA_OLD,
         QTDE_SOBRA_FALTA_NEW,              QTDE_ENFE_PROG_OLD,                    QTDE_ENFE_PROG_NEW,
         QTDE_ENFE_REAL_OLD,                QTDE_ENFE_REAL_NEW,                    PESO_PECA_OLD,
         PESO_PECA_NEW,                     RESIDUO_OLD,                           RESIDUO_NEW,
         ENCAIXE_OLD,                       ENCAIXE_NEW,                           SOBRAS_OLD,
         SOBRAS_NEW,                        ORDEM_PRODUCAO_OLD,                    ORDEM_PRODUCAO_NEW,
         TIPO_CORTE_OLD,                    TIPO_CORTE_NEW,                        QTDE_METROS_OLD,
         QTDE_METROS_NEW,                   QTDE_KG_COLETADOS_OLD,                 QTDE_KG_COLETADOS_NEW,
         CORREDOR_OLD,                      CORREDOR_NEW,                          BOX_OLD,
         BOX_NEW,                           PERC_EFICIENCIA_OLD,                   PERC_EFICIENCIA_NEW,
         PERC_REJEICAO_OLD,                 PERC_REJEICAO_NEW,                     PESO_POR_CAMADA_OLD,
         PESO_POR_CAMADA_NEW,               COMPRIMENTO_ENFESTO_OLD,               COMPRIMENTO_ENFESTO_NEW,
         QTDE_DEBRUM_OLD,                   QTDE_DEBRUM_NEW,                       QTDE_TAPETE_OLD,
         QTDE_TAPETE_NEW,                   QTDE_VIVO_OLD,                         QTDE_VIVO_NEW,
         QTDE_PONTAS_OLD,                   QTDE_PONTAS_NEW,                       QTDE_EMPALME_OLD,
         QTDE_EMPALME_NEW,                  QTDE_RECUPERACAO_OLD,                  QTDE_RECUPERACAO_NEW,
         QTDE_ENTRE_CORTE_OLD,              QTDE_ENTRE_CORTE_NEW,                  TOTAL_DEVOLUCAO_PRIMEIRA_OLD,
         TOTAL_DEVOLUCAO_PRIMEIRA_NEW,      TOTAL_DEVOLUCAO_SEGUNDA_OLD,           TOTAL_DEVOLUCAO_SEGUNDA_NEW,
         TOTAL_REJEICAO_OLD,                TOTAL_REJEICAO_NEW,                    TOTAL_DEPURADO_OLD,
         TOTAL_DEPURADO_NEW,                NUMERO_CAD_OLD,                        NUMERO_CAD_NEW,
         QTDE_METROS_OPTIPLAN_OLD,          QTDE_METROS_OPTIPLAN_NEW,              LARG_MINIMA_OLD,
         LARG_MINIMA_NEW,                   CENTRO_CUSTO_OLD,                      CENTRO_CUSTO_NEW,
         OBSERVACAO_CAD_OLD,                OBSERVACAO_CAD_NEW,                    RESPONSAVEL_CAD_OLD,
         RESPONSAVEL_CAD_NEW,               RESPONSAVEL_OPTIPLAN_OLD,              RESPONSAVEL_OPTIPLAN_NEW,
         QTDE_METROS_POR_PECA_OLD,          QTDE_METROS_POR_PECA_NEW,              OBSERVACAO_APROV_CORTE_OLD,
         OBSERVACAO_APROV_CORTE_NEW,
         usuario_rede,                      maquina_rede      
         ) VALUES  (
         WS_PCPC0302_ORPROCOR,              WS_PCPC0302_SEQORCOR,                  WS_PCPC0302_SEQUENOR,
         WS_SORTIMENTO_PECA,                sysdate,                               WS_TIPO_OCORR,
         WS_NOME_PROGRAMA,                  ws_aplicativo,                          WS_TECORDCO_NIVEL99_OLD,
         WS_TECORDCO_NIVEL99_NEW,           WS_TECORDCO_GRUPO_OLD,                 WS_TECORDCO_GRUPO_NEW,
         WS_TECORDCO_SUBGRUPO,              WS_TECORDCO_ITEM_OLD,                  WS_TECORDCO_ITEM_NEW,
         WS_CODIGO_DEPOSITO_OLD,            WS_CODIGO_DEPOSITO_NEW,                WS_QTDE_KG_PROG_OLD,
         WS_QTDE_KG_PROG_NEW,               WS_QTDE_KG_REAL_OLD,                   WS_QTDE_KG_REAL_NEW,
         WS_SORTIMENTO_PANO_OLD,            WS_SORTIMENTO_PANO_NEW,                WS_QTDE_SOBRA_FALTA_OLD,
         WS_QTDE_SOBRA_FALTA_NEW,           WS_QTDE_ENFE_PROG_OLD,                 WS_QTDE_ENFE_PROG_NEW,
         WS_QTDE_ENFE_REAL_OLD,             WS_QTDE_ENFE_REAL_NEW,                 WS_PESO_PECA_OLD,
         WS_PESO_PECA_NEW,                  WS_RESIDUO_OLD,                        WS_RESIDUO_NEW,
         WS_ENCAIXE_OLD,                    WS_ENCAIXE_NEW,                        WS_SOBRAS_OLD,
         WS_SOBRAS_NEW,                     WS_ORDEM_PRODUCAO_OLD,                 WS_ORDEM_PRODUCAO_NEW,
         WS_TIPO_CORTE_OLD,                 WS_TIPO_CORTE_NEW,                     WS_QTDE_METROS_OLD,
         WS_QTDE_METROS_NEW,                WS_QTDE_KG_COLETADOS_OLD,              WS_QTDE_KG_COLETADOS_NEW,
         WS_CORREDOR_OLD,                   WS_CORREDOR_NEW,                       WS_BOX_OLD,
         WS_BOX_NEW,                        WS_PERC_EFICIENCIA_OLD,                WS_PERC_EFICIENCIA_NEW,
         WS_PERC_REJEICAO_OLD,              WS_PERC_REJEICAO_NEW,                  WS_PESO_POR_CAMADA_OLD,
         WS_PESO_POR_CAMADA_NEW,            WS_COMPRIMENTO_ENFESTO_OLD,            WS_COMPRIMENTO_ENFESTO_NEW,
         WS_QTDE_DEBRUM_OLD,                WS_QTDE_DEBRUM_NEW,                    WS_QTDE_TAPETE_OLD,
         WS_QTDE_TAPETE_NEW,                WS_QTDE_VIVO_OLD,                      WS_QTDE_VIVO_NEW,
         WS_QTDE_PONTAS_OLD,                WS_QTDE_PONTAS_NEW,                    WS_QTDE_EMPALME_OLD,
         WS_QTDE_EMPALME_NEW,               WS_QTDE_RECUPERACAO_OLD,               WS_QTDE_RECUPERACAO_NEW,
         WS_QTDE_ENTRE_CORTE_OLD,           WS_QTDE_ENTRE_CORTE_NEW,               ws_TOTAL_DEV_PRIMEIRA_OLD,
         ws_TOTAL_DEV_PRIMEIRA_NEW,         ws_TOTAL_DEV_SEGUNDA_OLD,              ws_TOTAL_DEV_SEGUNDA_NEW,
         WS_TOTAL_REJEICAO_OLD,             WS_TOTAL_REJEICAO_NEW,                 WS_TOTAL_DEPURADO_OLD,
         WS_TOTAL_DEPURADO_NEW,             WS_NUMERO_CAD_OLD,                     WS_NUMERO_CAD_NEW,
         WS_QTDE_METROS_OPTIPLAN_OLD,       WS_QTDE_METROS_OPTIPLAN_NEW,           WS_LARG_MINIMA_OLD,
         WS_LARG_MINIMA_NEW,                WS_CENTRO_CUSTO_OLD,                   WS_CENTRO_CUSTO_NEW,
         '',                                '',                                    '',
         '',                                '',                                    '',
         0,                                 0,                                     '',
         '',
         ws_usuario_rede,                   ws_maquina_rede
         );
   end if;
   
end TRIGGER_PCPC_032_HIST;
