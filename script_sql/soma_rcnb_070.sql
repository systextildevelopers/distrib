
  CREATE OR REPLACE FUNCTION "SOMA_RCNB_070" (empresa in number,
 colecao in number,
 linha   in number,
 artigo  in number)

RETURN number
IS v_soma_rcnb_070 number;


BEGIN
   select sum(consumo) into v_soma_rcnb_070 from rcnb_070
   where  rcnb_070.codigo_empresa = empresa
     and (rcnb_070.colecao        = colecao or colecao = 0)
     and (rcnb_070.linha          = linha   or linha   = 0)
     and (rcnb_070.artigo         = artigo  or artigo  = 0)
     and (rcnb_070.estado         = estado  or estado  = '')
   order by rcnb_070.colecao ASC,
            rcnb_070.linha   ASC,
            rcnb_070.artigo  ASC;
   return(v_soma_rcnb_070);

end soma_rcnb_070;

 

/

exec inter_pr_recompile;

