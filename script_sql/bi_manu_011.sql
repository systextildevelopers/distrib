CREATE OR REPLACE trigger "BI_MANU_011"  
  before insert on "MANU_011"              
  for each row 
begin  
  if :NEW."ID" is null then
    select "MANU_011_SEQ".nextval into :NEW."ID" from dual;
  end if;
end;
/
