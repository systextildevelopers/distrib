
  CREATE OR REPLACE PROCEDURE "INTER_PR_CALCULA_SEQ_OB" 
    -- Recebe parametros para criacao dos estagios e operacoes.
   (v_ordem_producao   in number,       v_ordem_tingimento  in number,
    v_data_embarque    in date,         v_sequencia         in number,
    v_origem_chamada   varchar2,        v_pano_sbg_nivel99  varchar2,
    v_pano_sbg_grupo   varchar2,        v_pano_sbg_subgrupo varchar2,
    v_pano_sbg_item    varchar2,        v_alternativa_item  in number,
    v_roteiro_opcional in number)
is
   v_centro_custo              basi_185.centro_custo%type;
   v_existe_estagio_aberto     varchar2(1);

   v_codigo_estagio            number;
   v_codigo_estagio_old        number;

   v_data_corrente             date;
   v_data_maior                date;
   v_data_menor                date;

   v_dias_disp                 number;
   v_dias_disp_oficial         number;
   v_dias_disp_oficial_old     number;

   v_lead_time                 number;
	 v_tot_lead_time             number;

   v_dias_nao_uteis            number;
	 v_dias_nao_uteis_old        number;
   v_dias_disp_final_ob        number;
   v_dias_disp_final_ot        number;

   v_sub_ler                   mqop_050.subgru_estrutura%type := '###';
   v_item_ler                  mqop_050.item_estrutura%type   := '######';

   v_nr_registro               number;

begin
   -- Chamadas: inter_tr_pcpb_010
   --           inter_tr_pcpb_020
   --           inter_tr_pcpb_030
   --           inter_tr_pedi_100_3

   -- Sempre antes de chamar a procedure para atualizar estagios e operacoes
   -- a logica abaixo deve ser executada para definicao do produto.
   begin
      v_sub_ler  := v_pano_sbg_subgrupo;
      v_item_ler := v_pano_sbg_item;
   end;

   begin
      select count(*)
      into v_nr_registro
      from mqop_050
      where mqop_050.nivel_estrutura  = v_pano_sbg_nivel99
        and mqop_050.grupo_estrutura  = v_pano_sbg_grupo
        and mqop_050.subgru_estrutura = v_sub_ler
        and mqop_050.item_estrutura   = v_item_ler
        and mqop_050.numero_alternati = v_alternativa_item
        and mqop_050.numero_roteiro   = v_roteiro_opcional;
      if v_nr_registro = 0
      then
         v_item_ler := '000000';
         begin
            select count(*)
            into v_nr_registro
            from mqop_050
            where mqop_050.nivel_estrutura  = v_pano_sbg_nivel99
              and mqop_050.grupo_estrutura  = v_pano_sbg_grupo
              and mqop_050.subgru_estrutura = v_sub_ler
              and mqop_050.item_estrutura   = v_item_ler
              and mqop_050.numero_alternati = v_alternativa_item
              and mqop_050.numero_roteiro   = v_roteiro_opcional;
            if v_nr_registro = 0
            then
               v_sub_ler  := '000';
               v_item_ler := v_pano_sbg_item;
               begin
                  select count(*)
                  into v_nr_registro
                  from mqop_050
                  where mqop_050.nivel_estrutura  = v_pano_sbg_nivel99
                    and mqop_050.grupo_estrutura  = v_pano_sbg_grupo
                    and mqop_050.subgru_estrutura = v_sub_ler
                    and mqop_050.item_estrutura   = v_item_ler
                    and mqop_050.numero_alternati = v_alternativa_item
                    and mqop_050.numero_roteiro   = v_roteiro_opcional;
                  if v_nr_registro = 0
                  then
                     v_item_ler := '000000';
                  end if;
               end;
            end if;
         end;
      end if;
   end;

   v_tot_lead_time         := 0.00;
   v_dias_nao_uteis_old    := 0;
   v_codigo_estagio_old    := 0;
   v_dias_disp_oficial_old := 0;

   for reg_mqop_050 in (select codigo_operacao,  seq_operacao
                        from mqop_050
                        where mqop_050.nivel_estrutura  = v_pano_sbg_nivel99
                          and mqop_050.grupo_estrutura  = v_pano_sbg_grupo
                          and mqop_050.subgru_estrutura = v_sub_ler
                          and mqop_050.item_estrutura   = v_item_ler
                          and mqop_050.numero_alternati = v_alternativa_item
                          and mqop_050.numero_roteiro   = v_roteiro_opcional
                        order by mqop_050.codigo_estagio desc)
   loop
      -- Carrega a data corrente.
      v_data_corrente := trunc(sysdate,'DD');

      -- Executa loop no pcpb_015 para calcular o tempo de cada operacao
      -- para todas as operacoes / maquinas.
      -- Este loop e para que execute o calculo somente dos itens que tem destino na OB.
      for reg_pcpb_015 in (select pcpb_015.grupo_maquina, pcpb_015.subgrupo_maquina
                           from pcpb_015
                           where pcpb_015.ordem_producao  = v_ordem_producao
                             and pcpb_015.seq_operacao    = reg_mqop_050.seq_operacao
                             and pcpb_015.codigo_operacao = reg_mqop_050.codigo_operacao)
      loop
         -- Busca o centro de custo da maquina para verificar dias uteis para producao.
         begin
            v_centro_custo := 0;

            select mqop_030.centro_custo
            into v_centro_custo
            from mqop_030
            where mqop_030.maq_sub_grupo_mq  = reg_pcpb_015.grupo_maquina
              and mqop_030.maq_sub_sbgr_maq  = reg_pcpb_015.subgrupo_maquina
              and mqop_030.maquina_ativa    <> 2
            group by mqop_030.centro_custo;
            exception
            when others then
               v_centro_custo := 0;
         end;

         begin
            -- Calcula somente para estagios em aberto para ordem / maquina.
            select pcpb_015.codigo_estagio
            into v_codigo_estagio
            from pcpb_015
            where pcpb_015.ordem_producao   = v_ordem_producao
              and pcpb_015.data_termino     is null
              and pcpb_015.hora_termino     is null
              and pcpb_015.seq_operacao     = reg_mqop_050.seq_operacao
              and pcpb_015.codigo_operacao  = reg_mqop_050.codigo_operacao;
            exception
            when others then
               v_codigo_estagio := 0;
         end;

         begin
            -- Busca o tempo para cada estagio.
            select mqop_005.leed_time
            into v_lead_time
            from mqop_005
            where mqop_005.codigo_estagio = v_codigo_estagio;
            exception
            when others then
               v_lead_time := 0.000;
         end;

         v_tot_lead_time := v_tot_lead_time + v_lead_time;

         -- Rotina para calcular os dias para producao da OB.
         if  v_codigo_estagio  > 0
         then
            -- Salva o estagio anterior para verificar se calcula ou nao os dias.
            v_dias_disp          := 0;
            v_dias_disp_final_ob := 0;
            v_dias_disp_oficial  := 0;
            v_dias_nao_uteis     := 0;

            if v_data_embarque is not null
            then

               v_dias_disp := v_data_embarque - v_data_corrente;

               if v_data_embarque > v_data_corrente
               then
                  v_data_maior := v_data_embarque;
                  v_data_menor := v_data_corrente;
               else
                  if v_data_embarque <= v_data_corrente
                  then
                     v_data_maior := v_data_corrente;
                     v_data_menor := v_data_embarque;
                  end if;
               end if;

               v_dias_nao_uteis := 0;

               for reg_basi_275 in (select basi_275.data_nao_util from basi_275
                                    where basi_275.centro_custo   = v_centro_custo
                                      and basi_275.data_nao_util >= v_data_menor
                                      and basi_275.data_nao_util <= v_data_maior
                                    group by basi_275.data_nao_util)
               loop
                  v_dias_nao_uteis := v_dias_nao_uteis + 1;
               end loop;

               if v_dias_nao_uteis <> 0
               then
                  v_dias_nao_uteis_old := v_dias_nao_uteis;
               end if;

               if v_dias_nao_uteis = 0
               then
                  v_dias_nao_uteis := v_dias_nao_uteis_old;
               end if;

               v_dias_disp_oficial := v_dias_disp - v_dias_nao_uteis;
               v_dias_disp_oficial := v_dias_disp_oficial - v_tot_lead_time;

               if v_dias_disp_oficial > 99
               then
                  v_dias_disp_oficial := 99;
               else
                  if v_dias_disp_oficial < -99
                  then
                     v_dias_disp_oficial := -99;
                  end if;
               end if;
            end if;

            if v_dias_disp_oficial is null
            then
               v_dias_disp_oficial := 99;
            end if;

            if v_codigo_estagio_old = v_codigo_estagio
            then
               v_dias_disp_oficial := v_dias_disp_oficial_old;
            end if;

            -- Atualiza as operacoes com os dias para operacao para ordem/maquina operacao.
            update pcpb_015
            set prioridade_producao = v_dias_disp_oficial
            where pcpb_015.ordem_producao  = v_ordem_producao
              and pcpb_015.seq_operacao    = reg_mqop_050.seq_operacao
              and pcpb_015.codigo_operacao = reg_mqop_050.codigo_operacao;

            v_codigo_estagio_old    := v_codigo_estagio;
            v_dias_disp_oficial_old := v_dias_disp_oficial;

         end if;

      end loop;

      if v_origem_chamada <> 'PCPB_010'
      then
         -- Atualiza a prioridade de producao na capa da OB e da OT (Agrupamento).
         begin
            select min(pcpb_015.prioridade_producao)
            into v_dias_disp_final_ob
            from pcpb_015
            where pcpb_015.ordem_producao = v_ordem_producao;
         end;

         if v_dias_disp_final_ob is null
         then
            v_dias_disp_final_ob := 0;
         end if;

         update pcpb_010
         set ordem_preparacao = v_dias_disp_final_ob
         where pcpb_010.ordem_producao = v_ordem_producao;

         if v_ordem_tingimento > 0
         then
            begin
               select min(pcpb_010.ordem_preparacao)
               into v_dias_disp_final_ot
               from pcpb_010
               where pcpb_010.ordem_tingimento = v_ordem_tingimento;
            end;

            update pcpb_100
            set prioridade_producao = v_dias_disp_final_ot
            where pcpb_100.ordem_agrupamento = v_ordem_tingimento
              and pcpb_100.tipo_ordem        = '1';

            update pcpb_110
            set prioridade_producao = v_dias_disp_final_ot
            where pcpb_110.ordem_agrupamento = v_ordem_tingimento
              and pcpb_110.tipo_ordem        = '1';

         end if;
      end if;
   end loop;
end inter_pr_calcula_seq_ob;
 

/

exec inter_pr_recompile;

