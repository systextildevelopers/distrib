
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_030_PLAN" 
   before insert or
          delete or
          update of pedido_corte, nr_pedido_ordem, qtde_quilos_prog, alternativa
   on pcpb_030
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_qtde_update             number(15,5);
   v_saldo_prog              number(15,5);
   v_total_necessario        number(15,5);
   v_alternativa             number(2);
   v_count_reg               number(7);

   v_nivel_produto           varchar2(1);
   v_grupo_produto           varchar2(5);
   v_subgrupo_produto        varchar2(3);
   v_item_produto            varchar2(6);
   v_destino                 number(1);
   v_numero_destino          number(9);
   v_executa_trigger         number;

   PRAGMA AUTONOMOUS_TRANSACTION;
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting or updating
   then
      v_nivel_produto    := :new.pano_nivel99;
      v_grupo_produto    := :new.pano_grupo;
      v_subgrupo_produto := :new.pano_subgrupo;
      v_item_produto     := :new.pano_item;
      v_destino          := :new.pedido_corte;
      v_numero_destino   := :new.nr_pedido_ordem;

      v_executa_trigger  := :new.executa_trigger;

      if inserting
      then
         v_saldo_prog := :new.qtde_quilos_prog;
      else
         if :old.alternativa = :new.alternativa
         then
            v_saldo_prog := :new.qtde_quilos_prog - :old.qtde_quilos_prog;
         else
            v_saldo_prog := :new.qtde_quilos_prog;
         end if;
      end if;

      if v_executa_trigger <> 1
      then
         if :new.alternativa = 0
         then
            begin
               select pcpb_020.alternativa_item
               into   v_alternativa
               from pcpb_020
               where pcpb_020.ordem_producao    = :new.ordem_producao
                 and pcpb_020.sequencia         = :new.sequencia
                 and pcpb_020.pano_sbg_nivel99  = v_nivel_produto
                 and pcpb_020.pano_sbg_grupo    = v_grupo_produto
                 and pcpb_020.pano_sbg_subgrupo = v_subgrupo_produto
                 and pcpb_020.pano_sbg_item     = v_item_produto
                 and rownum                     = 1;
               exception when no_data_found
               then v_alternativa := 0;
            end;
         else
            v_alternativa := :new.alternativa;
         end if;

         if (v_destino = 7 and v_nivel_produto = '7')
         then
            for reg_tmrp_630 in (select tmrp_630.ordem_planejamento,          tmrp_630.pedido_venda,
                                        tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                        tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                        tmrp_625.alternativa_produto_origem,
                                        tmrp_630.area_producao,               tmrp_630.ordem_prod_compra,
                                        tmrp_625.seq_produto,                 tmrp_625.seq_produto_origem
                                 from tmrp_630, tmrp_625
                                 where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
                                   and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
                                   and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
                                   and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
                                   and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
                                   and tmrp_625.item_produto_origem        = tmrp_630.item_produto
                                   and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto
                                   and tmrp_625.nivel_produto              = v_nivel_produto
                                   and tmrp_625.grupo_produto              = v_grupo_produto
                                   and tmrp_625.subgrupo_produto           = v_subgrupo_produto
                                   and tmrp_625.item_produto               = v_item_produto
                                   and tmrp_625.alternativa_produto        = v_alternativa
                                   and tmrp_630.area_producao              = decode(v_nivel_produto,'2',1,4)
                                   and tmrp_630.ordem_prod_compra          = v_numero_destino
                                 group by  tmrp_630.ordem_planejamento,          tmrp_630.pedido_venda,
                                           tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                           tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                           tmrp_625.alternativa_produto_origem,
                                           tmrp_630.area_producao,               tmrp_630.ordem_prod_compra,
                                           tmrp_625.seq_produto,                 tmrp_625.seq_produto_origem)
            loop
               begin
                  insert into tmrp_615
                    (ordem_prod,                               area_producao,
                     nivel,                                    grupo,
                     subgrupo,                                 item,
                     alternativa,                              ordem_planejamento,
                     pedido_venda,                             nome_programa,
                     nr_solicitacao,                           tipo_registro,
                     cgc_cliente9,

                     nivel_prod,                               grupo_prod,
                     subgrupo_prod,                            item_prod,
                     alternativa_prod,
                     seq_registro,                             sequencia)
                  values
                    (reg_tmrp_630.ordem_prod_compra,           reg_tmrp_630.area_producao,
                     reg_tmrp_630.nivel_produto_origem,        reg_tmrp_630.grupo_produto_origem,
                     reg_tmrp_630.subgrupo_produto_origem,     reg_tmrp_630.item_produto_origem,
                     reg_tmrp_630.alternativa_produto_origem,  reg_tmrp_630.ordem_planejamento,
                     reg_tmrp_630.pedido_venda,                'trigger_planejamento',
                     888,                                      888,
                     ws_sid,

                     v_nivel_produto,                          v_grupo_produto,
                     v_subgrupo_produto,                       v_item_produto,
                     v_alternativa,
                     reg_tmrp_630.seq_produto,                 reg_tmrp_630.seq_produto_origem);
                  exception when others
                  then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            end loop;

            begin
               select nvl(sum(tmrp_625.qtde_reserva_planejada),0)
               into v_total_necessario
               from tmrp_630, tmrp_625
               where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
                 and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
                 and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
                 and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
                 and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
                 and tmrp_625.item_produto_origem        = tmrp_630.item_produto
                 and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto
                 and tmrp_625.nivel_produto              = v_nivel_produto
                 and tmrp_625.grupo_produto              = v_grupo_produto
                 and tmrp_625.subgrupo_produto           = v_subgrupo_produto
                 and tmrp_625.item_produto               = v_item_produto
                 and tmrp_625.alternativa_produto        = v_alternativa
                 and tmrp_630.area_producao              = decode(v_nivel_produto,'2',1,4)
                 and tmrp_630.ordem_prod_compra          = v_numero_destino;
               exception when others
               then v_total_necessario := 0.00;
            end;

            for reg_ordens_planej in (select tmrp_630.ordem_planejamento,
                                             tmrp_630.pedido_venda,
                                             nvl(sum(tmrp_625.qtde_reserva_planejada),0) qtde_necessaria
                                      from tmrp_630, tmrp_625
                                      where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
                                        and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
                                        and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
                                        and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
                                        and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
                                        and tmrp_625.item_produto_origem        = tmrp_630.item_produto
                                        and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto
                                        and tmrp_625.nivel_produto              = v_nivel_produto
                                        and tmrp_625.grupo_produto              = v_grupo_produto
                                        and tmrp_625.subgrupo_produto           = v_subgrupo_produto
                                        and tmrp_625.item_produto               = v_item_produto
                                        and tmrp_625.alternativa_produto        = v_alternativa
                                        and tmrp_630.ordem_prod_compra          = v_numero_destino
                                        and tmrp_630.area_producao              = decode(v_nivel_produto,'2',1,4)
                                      group by tmrp_630.ordem_planejamento, tmrp_630.pedido_venda)
            loop
               if v_total_necessario > 0
               then
                  v_qtde_update := (((reg_ordens_planej.qtde_necessaria * 100) / v_total_necessario) * v_saldo_prog) / 100;
               end if;

               begin
                  select count(1)
                  into   v_count_reg
                  from tmrp_630
                  where tmrp_630.ordem_planejamento   = reg_ordens_planej.ordem_planejamento
                    and tmrp_630.pedido_venda         = reg_ordens_planej.pedido_venda
                    and tmrp_630.ordem_prod_compra    = :new.ordem_producao
                    and tmrp_630.area_producao        = decode(v_nivel_produto,'2',2,7)
                    and tmrp_630.nivel_produto        = v_nivel_produto
                    and tmrp_630.grupo_produto        = v_grupo_produto
                    and tmrp_630.subgrupo_produto     = v_subgrupo_produto
                    and tmrp_630.item_produto         = v_item_produto
                    and tmrp_630.alternativa_produto  = v_alternativa;
                  exception when others
                  then v_count_reg := 0;
               end;

               if v_count_reg > 0
               then
                  begin
                     update tmrp_630
                        set tmrp_630.programado_comprado = tmrp_630.programado_comprado + v_qtde_update
                     where tmrp_630.ordem_planejamento   = reg_ordens_planej.ordem_planejamento
                       and tmrp_630.pedido_venda         = reg_ordens_planej.pedido_venda
                       and tmrp_630.ordem_prod_compra    = :new.ordem_producao
                       and tmrp_630.area_producao        = decode(v_nivel_produto,'2',2,7)
                       and tmrp_630.nivel_produto        = v_nivel_produto
                       and tmrp_630.grupo_produto        = v_grupo_produto
                       and tmrp_630.subgrupo_produto     = v_subgrupo_produto
                       and tmrp_630.item_produto         = v_item_produto
                       and tmrp_630.alternativa_produto  = v_alternativa;
                     exception when others
                     then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               else
                  begin
                     insert into tmrp_630
                       (ordem_planejamento,                   area_producao,                   ordem_prod_compra,
                        nivel_produto,                        grupo_produto,                   subgrupo_produto,
                        item_produto,                         pedido_venda,                    programado_comprado,
                        alternativa_produto)
                     values
                       (reg_ordens_planej.ordem_planejamento, decode(v_nivel_produto,'2',2,7), :new.ordem_producao,
                        v_nivel_produto,                      v_grupo_produto,                 v_subgrupo_produto,
                        v_item_produto,                       reg_ordens_planej.pedido_venda,  v_qtde_update,
                        v_alternativa);
                     exception when others
                     then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               end if;
            end loop;
         end if;


         /*

            ORDEM DE PLANEJAMENTO


         */

         if (v_destino = 8 and v_nivel_produto = '7')
         then
            for reg_tmrp_630 in (select tmrp_625.ordem_planejamento,          tmrp_625.pedido_venda,
                                        tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                        tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                        tmrp_625.alternativa_produto_origem,
                                        tmrp_625.seq_produto,                 tmrp_625.seq_produto_origem
                                 from tmrp_625
                                 where tmrp_625.ordem_planejamento         = v_numero_destino
                                   and tmrp_625.nivel_produto              = v_nivel_produto
                                   and tmrp_625.grupo_produto              = v_grupo_produto
                                   and tmrp_625.subgrupo_produto           = v_subgrupo_produto
                                   and tmrp_625.item_produto               = v_item_produto
                                   and tmrp_625.alternativa_produto        = v_alternativa
                                 group by  tmrp_625.ordem_planejamento,          tmrp_625.pedido_venda,
                                           tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                           tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                           tmrp_625.alternativa_produto_origem,
                                           tmrp_625.seq_produto,                 tmrp_625.seq_produto_origem)
            loop
               begin
                  insert into tmrp_615
                    (ordem_prod,                               area_producao,
                     nivel,                                    grupo,
                     subgrupo,                                 item,
                     alternativa,                              ordem_planejamento,
                     pedido_venda,                             nome_programa,
                     nr_solicitacao,                           tipo_registro,
                     cgc_cliente9,

                     nivel_prod,                               grupo_prod,
                     subgrupo_prod,                            item_prod,
                     alternativa_prod,
                     seq_registro,                             sequencia
                  ) values (
                     0,                                        4,
                     reg_tmrp_630.nivel_produto_origem,        reg_tmrp_630.grupo_produto_origem,
                     reg_tmrp_630.subgrupo_produto_origem,     reg_tmrp_630.item_produto_origem,
                     reg_tmrp_630.alternativa_produto_origem,  reg_tmrp_630.ordem_planejamento,
                     reg_tmrp_630.pedido_venda,                'trigger_planejamento',
                     888,                                      888,
                     ws_sid,

                     v_nivel_produto,                          v_grupo_produto,
                     v_subgrupo_produto,                       v_item_produto,
                     v_alternativa,
                     reg_tmrp_630.seq_produto,                 reg_tmrp_630.seq_produto_origem);
                  exception when others
                  then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            end loop;

            begin
--               select nvl(sum(tmrp_625.qtde_reserva_programada),0) - nvl(sum(tmrp_625.qtde_areceber_programada),0)
               select nvl(sum(tmrp_625.qtde_reserva_planejada),0)
               into v_total_necessario
               from tmrp_625
               where tmrp_625.ordem_planejamento         = v_numero_destino
                 and tmrp_625.nivel_produto              = v_nivel_produto
                 and tmrp_625.grupo_produto              = v_grupo_produto
                 and tmrp_625.subgrupo_produto           = v_subgrupo_produto
                 and tmrp_625.item_produto               = v_item_produto
                 and tmrp_625.alternativa_produto        = v_alternativa;
--                 and tmrp_625.qtde_reserva_programada - tmrp_625.qtde_areceber_programada > 0;
               exception when others
               then v_total_necessario := 0.00;
            end;

            for reg_ordens_planej in (select tmrp_625.ordem_planejamento,
                                             tmrp_625.pedido_venda,
                                             nvl(sum(tmrp_625.qtde_reserva_planejada),0) qtde_necessaria
--                                             nvl(sum(tmrp_625.qtde_reserva_programada),0) - nvl(sum(tmrp_625.qtde_areceber_programada),0) qtde_necessaria
                                      from tmrp_625
                                      where tmrp_625.ordem_planejamento         = v_numero_destino
                                        and tmrp_625.nivel_produto              = v_nivel_produto
                                        and tmrp_625.grupo_produto              = v_grupo_produto
                                        and tmrp_625.subgrupo_produto           = v_subgrupo_produto
                                        and tmrp_625.item_produto               = v_item_produto
                                        and tmrp_625.alternativa_produto        = v_alternativa
--                                        and tmrp_625.qtde_reserva_programada - tmrp_625.qtde_areceber_programada > 0
                                      group by tmrp_625.ordem_planejamento, tmrp_625.pedido_venda)
            loop
               -- Esclarecimentos:
               --   Toma o total necessario pelo planejamento e obtem o percentual relativo de cada item.
               --   Depois extrai eset percentual da quantidade sendo programada.
               -- Ex.:
               --   Tenho uma necessidade de 200, dividida em 10 itens de 20 (10% para cada um).
               --   Ao programar 300, sei que tenho que atualizar o primeiro item com 30 (10% de 300).
               if v_total_necessario > 0
               then
                  v_qtde_update := (((reg_ordens_planej.qtde_necessaria * 100) / v_total_necessario) * v_saldo_prog) / 100;
               end if;

               begin
                  select count(1)
                  into   v_count_reg
                  from tmrp_630
                  where tmrp_630.ordem_planejamento   = reg_ordens_planej.ordem_planejamento
                    and tmrp_630.pedido_venda         = reg_ordens_planej.pedido_venda
                    and tmrp_630.ordem_prod_compra    = :new.ordem_producao
                    and tmrp_630.area_producao        = decode(v_nivel_produto,'2',2,7)
                    and tmrp_630.nivel_produto        = v_nivel_produto
                    and tmrp_630.grupo_produto        = v_grupo_produto
                    and tmrp_630.subgrupo_produto     = v_subgrupo_produto
                    and tmrp_630.item_produto         = v_item_produto
                    and tmrp_630.alternativa_produto  = v_alternativa;
                  exception when others
                  then v_count_reg := 0;
               end;

               if v_count_reg > 0
               then
                  begin
                     update tmrp_630
                        set tmrp_630.programado_comprado = tmrp_630.programado_comprado + v_qtde_update
                     where tmrp_630.ordem_planejamento   = reg_ordens_planej.ordem_planejamento
                       and tmrp_630.pedido_venda         = reg_ordens_planej.pedido_venda
                       and tmrp_630.ordem_prod_compra    = :new.ordem_producao
                       and tmrp_630.area_producao        = decode(v_nivel_produto,'2',2,7)
                       and tmrp_630.nivel_produto        = v_nivel_produto
                       and tmrp_630.grupo_produto        = v_grupo_produto
                       and tmrp_630.subgrupo_produto     = v_subgrupo_produto
                       and tmrp_630.item_produto         = v_item_produto
                       and tmrp_630.alternativa_produto  = v_alternativa;
                     exception when others
                     then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               else
                  begin
                     insert into tmrp_630
                       (ordem_planejamento,                   area_producao,                   ordem_prod_compra,
                        nivel_produto,                        grupo_produto,                   subgrupo_produto,
                        item_produto,                         pedido_venda,                    programado_comprado,
                        alternativa_produto
                     ) values (
                        reg_ordens_planej.ordem_planejamento, decode(v_nivel_produto,'2',2,7), :new.ordem_producao,
                        v_nivel_produto,                      v_grupo_produto,                 v_subgrupo_produto,
                        v_item_produto,                       reg_ordens_planej.pedido_venda,  v_qtde_update,
                        v_alternativa
                     );
                     exception when others
                     then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               end if;
            end loop;
         end if;


         /*

            ORDEM DE PLANEJAMENTO


         */

         if (v_destino = 8 and v_nivel_produto = '7')
         then
            for reg_tmrp_630 in (select tmrp_625.ordem_planejamento,          tmrp_625.pedido_venda,
                                        tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                        tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                        tmrp_625.alternativa_produto_origem,
                                        tmrp_625.seq_produto,                 tmrp_625.seq_produto_origem
                                 from tmrp_625
                                 where tmrp_625.ordem_planejamento         = v_numero_destino
                                   and tmrp_625.nivel_produto              = v_nivel_produto
                                   and tmrp_625.grupo_produto              = v_grupo_produto
                                   and tmrp_625.subgrupo_produto           = v_subgrupo_produto
                                   and tmrp_625.item_produto               = v_item_produto
                                   and tmrp_625.alternativa_produto        = v_alternativa
                                 group by  tmrp_625.ordem_planejamento,          tmrp_625.pedido_venda,
                                           tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                           tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                           tmrp_625.alternativa_produto_origem,
                                           tmrp_625.seq_produto,                 tmrp_625.seq_produto_origem)
            loop
               begin
                  insert into tmrp_615
                    (ordem_prod,                               area_producao,
                     nivel,                                    grupo,
                     subgrupo,                                 item,
                     alternativa,                              ordem_planejamento,
                     pedido_venda,                             nome_programa,
                     nr_solicitacao,                           tipo_registro,
                     cgc_cliente9,

                     nivel_prod,                               grupo_prod,
                     subgrupo_prod,                            item_prod,
                     alternativa_prod,
                     seq_registro,                             sequencia
                  ) values (
                     0,                                        4,
                     reg_tmrp_630.nivel_produto_origem,        reg_tmrp_630.grupo_produto_origem,
                     reg_tmrp_630.subgrupo_produto_origem,     reg_tmrp_630.item_produto_origem,
                     reg_tmrp_630.alternativa_produto_origem,  reg_tmrp_630.ordem_planejamento,
                     reg_tmrp_630.pedido_venda,                'trigger_planejamento',
                     888,                                      888,
                     ws_sid,

                     v_nivel_produto,                          v_grupo_produto,
                     v_subgrupo_produto,                       v_item_produto,
                     v_alternativa,
                     reg_tmrp_630.seq_produto,                 reg_tmrp_630.seq_produto_origem);
                  exception when others
                  then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            end loop;

            begin
--               select nvl(sum(tmrp_625.qtde_reserva_programada),0) - nvl(sum(tmrp_625.qtde_areceber_programada),0)
               select nvl(sum(tmrp_625.qtde_reserva_planejada),0)
               into v_total_necessario
               from tmrp_625
               where tmrp_625.ordem_planejamento         = v_numero_destino
                 and tmrp_625.nivel_produto              = v_nivel_produto
                 and tmrp_625.grupo_produto              = v_grupo_produto
                 and tmrp_625.subgrupo_produto           = v_subgrupo_produto
                 and tmrp_625.item_produto               = v_item_produto
                 and tmrp_625.alternativa_produto        = v_alternativa;
--                 and tmrp_625.qtde_reserva_programada - tmrp_625.qtde_areceber_programada > 0;
               exception when others
               then v_total_necessario := 0.00;
            end;

            for reg_ordens_planej in (select tmrp_625.ordem_planejamento,
                                             tmrp_625.pedido_venda,
                                             nvl(sum(tmrp_625.qtde_reserva_planejada),0) qtde_necessaria
--                                             nvl(sum(tmrp_625.qtde_reserva_programada),0) - nvl(sum(tmrp_625.qtde_areceber_programada),0) qtde_necessaria
                                      from tmrp_625
                                      where tmrp_625.ordem_planejamento         = v_numero_destino
                                        and tmrp_625.nivel_produto              = v_nivel_produto
                                        and tmrp_625.grupo_produto              = v_grupo_produto
                                        and tmrp_625.subgrupo_produto           = v_subgrupo_produto
                                        and tmrp_625.item_produto               = v_item_produto
                                        and tmrp_625.alternativa_produto        = v_alternativa
--                                        and tmrp_625.qtde_reserva_programada - tmrp_625.qtde_areceber_programada > 0
                                      group by tmrp_625.ordem_planejamento, tmrp_625.pedido_venda)
            loop
               -- Esclarecimentos:
               --   Toma o total necessario pelo planejamento e obtem o percentual relativo de cada item.
               --   Depois extrai eset percentual da quantidade sendo programada.
               -- Ex.:
               --   Tenho uma necessidade de 200, dividida em 10 itens de 20 (10% para cada um).
               --   Ao programar 300, sei que tenho que atualizar o primeiro item com 30 (10% de 300).
               if v_total_necessario > 0
               then
                  v_qtde_update := (((reg_ordens_planej.qtde_necessaria * 100) / v_total_necessario) * v_saldo_prog) / 100;
               end if;

               begin
                  select count(1)
                  into   v_count_reg
                  from tmrp_630
                  where tmrp_630.ordem_planejamento   = reg_ordens_planej.ordem_planejamento
                    and tmrp_630.pedido_venda         = reg_ordens_planej.pedido_venda
                    and tmrp_630.ordem_prod_compra    = :new.ordem_producao
                    and tmrp_630.area_producao        = decode(v_nivel_produto,'2',2,7)
                    and tmrp_630.nivel_produto        = v_nivel_produto
                    and tmrp_630.grupo_produto        = v_grupo_produto
                    and tmrp_630.subgrupo_produto     = v_subgrupo_produto
                    and tmrp_630.item_produto         = v_item_produto
                    and tmrp_630.alternativa_produto  = v_alternativa;
                  exception when others
                  then v_count_reg := 0;
               end;

               if v_count_reg > 0
               then
                  begin
                     update tmrp_630
                        set tmrp_630.programado_comprado = tmrp_630.programado_comprado + v_qtde_update
                     where tmrp_630.ordem_planejamento   = reg_ordens_planej.ordem_planejamento
                       and tmrp_630.pedido_venda         = reg_ordens_planej.pedido_venda
                       and tmrp_630.ordem_prod_compra    = :new.ordem_producao
                       and tmrp_630.area_producao        = decode(v_nivel_produto,'2',2,7)
                       and tmrp_630.nivel_produto        = v_nivel_produto
                       and tmrp_630.grupo_produto        = v_grupo_produto
                       and tmrp_630.subgrupo_produto     = v_subgrupo_produto
                       and tmrp_630.item_produto         = v_item_produto
                       and tmrp_630.alternativa_produto  = v_alternativa;
                     exception when others
                     then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               else
                  begin
                     insert into tmrp_630
                       (ordem_planejamento,                   area_producao,                   ordem_prod_compra,
                        nivel_produto,                        grupo_produto,                   subgrupo_produto,
                        item_produto,                         pedido_venda,                    programado_comprado,
                        alternativa_produto
                     ) values (
                        reg_ordens_planej.ordem_planejamento, decode(v_nivel_produto,'2',2,7), :new.ordem_producao,
                        v_nivel_produto,                      v_grupo_produto,                 v_subgrupo_produto,
                        v_item_produto,                       reg_ordens_planej.pedido_venda,  v_qtde_update,
                        v_alternativa
                     );
                     exception when others
                     then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               end if;
            end loop;
         end if;

         if (v_destino = 8 and v_nivel_produto = '2')
         then

            inter_pr_destinos_planejamento(:new.nr_pedido_ordem,
                                           :new.ordem_producao,
                                           v_saldo_prog,
                                           :new.pano_nivel99,
                                           :new.pano_grupo,
                                           :new.pano_subgrupo,
                                           :new.pano_item,
                                           :new.alternativa,
                                           'I');
         end if;
      else
         :new.executa_trigger := 0;
      end if;
   else
      v_nivel_produto    := :old.pano_nivel99;
      v_grupo_produto    := :old.pano_grupo;
      v_subgrupo_produto := :old.pano_subgrupo;
      v_item_produto     := :old.pano_item;
      v_destino          := :old.pedido_corte;
      v_numero_destino   := :old.nr_pedido_ordem;

      begin
         select pcpb_020.alternativa_item
         into   v_alternativa
         from pcpb_020
         where pcpb_020.ordem_producao = :old.ordem_producao
           and pcpb_020.sequencia      = :old.sequencia
           and pcpb_020.pano_sbg_nivel99  = v_nivel_produto
           and pcpb_020.pano_sbg_grupo    = v_grupo_produto
           and pcpb_020.pano_sbg_subgrupo = v_subgrupo_produto
           and pcpb_020.pano_sbg_item     = v_item_produto
           and rownum                     = 1;
         exception when no_data_found
         then v_alternativa := 0;
      end;

      if v_destino = 4
      then
         for reg_tmrp_630 in (select tmrp_630.ordem_planejamento,  tmrp_630.pedido_venda,
                                     tmrp_630.nivel_produto,       tmrp_630.grupo_produto,
                                     tmrp_630.subgrupo_produto,    tmrp_630.item_produto,
                                     tmrp_630.alternativa_produto,
                                     tmrp_630.area_producao,       tmrp_630.ordem_prod_compra,
                                     tmrp_625.seq_produto,         tmrp_625.seq_produto_origem
                              from tmrp_630, tmrp_625
                              where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
                                and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
                                and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
                                and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
                                and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
                                and tmrp_625.item_produto_origem        = tmrp_630.item_produto
                                and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto
                                and tmrp_625.nivel_produto              = v_nivel_produto
                                and tmrp_625.grupo_produto              = v_grupo_produto
                                and tmrp_625.subgrupo_produto           = v_subgrupo_produto
                                and tmrp_625.item_produto               = v_item_produto
                                and tmrp_625.alternativa_produto        = v_alternativa
                                and tmrp_630.area_producao              = decode(v_nivel_produto,'2',1,4)
                                and tmrp_630.ordem_prod_compra          = v_numero_destino
                              group by  tmrp_630.ordem_planejamento,  tmrp_630.pedido_venda,
                                        tmrp_630.nivel_produto,       tmrp_630.grupo_produto,
                                        tmrp_630.subgrupo_produto,    tmrp_630.item_produto,
                                        tmrp_630.alternativa_produto,
                                        tmrp_630.area_producao,       tmrp_630.ordem_prod_compra,
                                     tmrp_625.seq_produto,         tmrp_625.seq_produto_origem)
         loop
            begin
               insert into tmrp_615
                 (ordem_prod,                        area_producao,
                  nivel,                             grupo,
                  subgrupo,                          item,
                  alternativa,                       ordem_planejamento,
                  pedido_venda,                      nome_programa,
                  nr_solicitacao,                    tipo_registro,
                  cgc_cliente9,

                  nivel_prod,                        grupo_prod,
                  subgrupo_prod,                     item_prod,
                  alternativa_prod,
                  seq_registro,                      sequencia)
               values
                 (reg_tmrp_630.ordem_prod_compra,    reg_tmrp_630.area_producao,
                  reg_tmrp_630.nivel_produto,        reg_tmrp_630.grupo_produto,
                  reg_tmrp_630.subgrupo_produto,     reg_tmrp_630.item_produto,
                  reg_tmrp_630.alternativa_produto,  reg_tmrp_630.ordem_planejamento,
                  reg_tmrp_630.pedido_venda,         'trigger_planejamento',
                  888,                               888,
                  ws_sid,

                  v_nivel_produto,                   v_grupo_produto,
                  v_subgrupo_produto,                v_item_produto,
                  v_alternativa,
                  reg_tmrp_630.seq_produto,          reg_tmrp_630.seq_produto_origem);
               exception when others
               then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;
         end loop;

         begin
            delete tmrp_630
            where tmrp_630.ordem_prod_compra    = :old.ordem_producao
              and tmrp_630.area_producao        = decode(v_nivel_produto,'2',2,7)
              and tmrp_630.nivel_produto        = v_nivel_produto
              and tmrp_630.grupo_produto        = v_grupo_produto
              and tmrp_630.subgrupo_produto     = v_subgrupo_produto
              and tmrp_630.item_produto         = v_item_produto
              and tmrp_630.alternativa_produto  = v_alternativa;
            exception when others
            then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;

      if v_destino = 8
      then
         inter_pr_destinos_planejamento(:old.nr_pedido_ordem,
                                        :old.ordem_producao,
                                        :old.qtde_quilos_prog * (-1),
                                        v_nivel_produto,
                                        v_grupo_produto,
                                        v_subgrupo_produto,
                                        v_item_produto,
                                        v_alternativa,
                                        'D');
      end if;

   end if;
end inter_tr_pcpb_030_plan;

-- ALTER TRIGGER "INTER_TR_PCPB_030_PLAN" ENABLE
 

/

exec inter_pr_recompile;

