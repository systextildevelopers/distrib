CREATE OR REPLACE PROCEDURE inter_pr_valida_nf_confirm AS

v_nota_entrada number;
v_nota_existente number;
v_cod_empresa_ent number;
v_cgc_9 number;
v_cgc_4 number;
v_cgc_2 number;

BEGIN

  -- 1. Selecionar notas de saida que atendem aos criterios especificados.
	
  FOR nota IN (select fatu_050.num_nota_fiscal, fatu_050.serie_nota_fisc, 
	                    fatu_050.situacao_nfisc,  fatu_050.cgc_9, 
						          fatu_050.cgc_4,           fatu_050.cgc_2,
						          fatu_050.codigo_empresa
				       from fatu_050, pedi_080
				       where fatu_050.situacao_nfisc in (1, 4)
				         and fatu_050.natop_nf_nat_oper = pedi_080.natur_operacao
				         and fatu_050.natop_nf_est_oper = pedi_080.estado_natoper
				         and pedi_080.cod_natureza || substr(trim(to_char(pedi_080.divisao_natur, '00')), 2, 1) in ('5.152', '6.152', '5.209', '6.209')
                 and fatu_050.data_emissao >= (fatu_050.data_emissao - 30))

  LOOP
    
    -- 2. Procurar a nota de entrada correspondente.
            
    
    BEGIN    
        select fatu_500.codigo_empresa
        into v_cod_empresa_ent
        from fatu_500
        where fatu_500.cgc_9 = nota.cgc_9
          and fatu_500.cgc_4 = nota.cgc_4
          and fatu_500.cgc_2 = nota.cgc_2;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      CONTINUE;
    END;
    
    BEGIN    
        select fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2
        into v_cgc_9, v_cgc_4, v_cgc_2
        from fatu_500
        where fatu_500.codigo_empresa = nota.codigo_empresa;
    EXCEPTION
   WHEN NO_DATA_FOUND THEN
      CONTINUE;
    END;
    
    
    BEGIN    
        select 1
        into v_nota_entrada
        from obrf_010
        where documento = nota.num_nota_fiscal
          and serie = nota.serie_nota_fisc
          and local_entrega = v_cod_empresa_ent
          and obrf_010.cgc_cli_for_9 =  v_cgc_9
          and obrf_010.cgc_cli_for_4 =  v_cgc_4
          and obrf_010.cgc_cli_for_2 =  v_cgc_2;
        
    EXCEPTION
      -- 3. Se n?o encontrar a nota fiscal, verificar se ja existe na tabela obrf_201.
      WHEN NO_DATA_FOUND THEN
      BEGIN
        select 1
        into v_nota_existente
        from obrf_201
        where codigo_empresa  = nota.codigo_empresa
          and num_nota_fiscal = nota.num_nota_fiscal
          and cgc_9           = nota.cgc_9
          and cgc_4           = nota.cgc_4
          and cgc_2           = nota.cgc_2;
              
      EXCEPTION
        -- 4. Se n?o encontrar a nota fiscal, inserir na tabela obrf_201.
        WHEN NO_DATA_FOUND THEN
          INSERT INTO obrf_201 (codigo_empresa, num_nota_fiscal, serie_nota_fisc, cgc_9, cgc_4, cgc_2, data_emissao)
          VALUES (nota.codigo_empresa, nota.num_nota_fiscal, nota.serie_nota_fisc, nota.cgc_9, nota.cgc_4, nota.cgc_2, SYSDATE);
      END;
    END;
            
    -- 5. Atualizar situacao_fisc para 4 se situac?o for 1.
    IF v_nota_entrada = 1 AND nota.situacao_nfisc = 1 THEN

      update fatu_050
      set situacao_nfisc = 4
      where num_nota_fiscal = nota.num_nota_fiscal
        and serie_nota_fisc = nota.serie_nota_fisc
        and cgc_9 = nota.cgc_9
        and cgc_4 = nota.cgc_4
        and cgc_2 = nota.cgc_2;
        
    -- 6. Deleta os registros que est?o na obrf_201 que ja est?o na situac?o 4.
    ELSIF v_nota_entrada = 1 AND nota.situacao_nfisc = 4 THEN

      delete obrf_201
      where codigo_empresa = nota.codigo_empresa
        and num_nota_fiscal = nota.num_nota_fiscal
        and serie_nota_fisc = nota.serie_nota_fisc
        and cgc_9 = nota.cgc_9
        and cgc_4 = nota.cgc_4
        and cgc_2 = nota.cgc_2;
    END IF;
        
  END LOOP;
  
  COMMIT;
END inter_pr_valida_nf_confirm;


/
