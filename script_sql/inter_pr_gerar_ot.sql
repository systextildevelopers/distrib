CREATE OR REPLACE procedure inter_pr_gerar_ot(
        p_id                in   number,
        p_tipo_ordem        in   number,
        p_lista_ob          in   varchar2,
        p_observacao        in   varchar2,
        p_ot_gerada         out  number
        )

 is
   v_ordem_agrupamento       number;
   v_primeira_ob             number;   
   v_grupo_maquina           varchar2(04);
   v_subgrupo_maquina        varchar2(03);
   v_numero_maquina          number;
   v_mdi_cod_ordem_impressa  number;
   v_periodo_producao        number;
   v_codigo_empresa          number;
   v_relacao_banho           number(9,3);
   v_produto_nivel           varchar2(01); 
   v_produto_grupo           varchar2(05);
   v_produto_subgrupo        varchar2(03);
   v_produto_item            varchar2(06);
   v_msg_erro                varchar2(255);
   v_temporario              number;
   v_ob_existentes           varchar2(255);

begin

   v_msg_erro := '';
   p_ot_gerada := 0;
   
   if p_tipo_ordem < 1 or p_tipo_ordem > 2
   then
   
      v_msg_erro := v_msg_erro || 'Tipo de Ordem deve ser 1 ou 2. ';
      
   end if;

   if trim(v_msg_erro) is null
   then
      begin
         select to_number(REGEXP_SUBSTR(trim(p_lista_ob), '[^;]+', 1, 1))
         INTO v_primeira_ob
         from dual;
      end;
   
      begin
         select pcpb_010.grupo_maquina,     pcpb_010.subgrupo_maquina,
                pcpb_010.numero_maquina,    pcpb_010.periodo_producao,
                pcpb_020.pano_sbg_nivel99,  pcpb_020.pano_sbg_grupo,
                pcpb_020.pano_sbg_subgrupo, pcpb_020.pano_sbg_item
         INTO   v_grupo_maquina,            v_subgrupo_maquina,
                v_numero_maquina,           v_periodo_producao,
                v_produto_nivel,            v_produto_grupo,
                v_produto_subgrupo,         v_produto_item
         from   pcpb_010, pcpb_020, pcpb_030
         where pcpb_010.ordem_producao    = v_primeira_ob
         and   pcpb_020.ordem_producao    = v_primeira_ob
         and   pcpb_030.ordem_producao    = v_primeira_ob
         and   pcpb_030.pano_nivel99      = pcpb_020.pano_sbg_nivel99
         and   pcpb_030.pano_grupo        = pcpb_020.pano_sbg_grupo
         and   pcpb_030.pano_subgrupo     = pcpb_020.pano_sbg_subgrupo
         and   pcpb_030.pano_item         = pcpb_020.pano_sbg_item;
         exception when others then
            v_msg_erro := v_msg_erro || 'ATENÇÃO! não encontrou tecido e/ou máquina com a OB informada. ';
      end;
   end if;

   if trim(v_msg_erro) is null
   then

      begin
         select nvl(pcpc_010.codigo_empresa, 1)
         INTO   v_codigo_empresa
         from   pcpc_010
         where pcpc_010.area_periodo     = 2
         and   pcpc_010.periodo_producao = v_periodo_producao;
      end;
   
      begin
         select fatu_502.cod_ordem_impressa
         INTO   v_mdi_cod_ordem_impressa
         from   fatu_502
         where fatu_502.codigo_empresa = v_codigo_empresa;
      end;
      
      begin
         select mqop_060.relacao_banho
         INTO   v_relacao_banho
         from   mqop_060
         where mqop_060.maq_sbgr_grupo_mq = v_grupo_maquina
         and   mqop_060.maq_sbgr_sbgr_maq = v_subgrupo_maquina
         and   mqop_060.nivel_estrutura   = v_produto_nivel
         and   mqop_060.grupo_estrutura   = v_produto_grupo
         and  (mqop_060.subgru_estrutura  = v_produto_subgrupo or mqop_060.subgru_estrutura = '000')
         and  (mqop_060.item_estrutura    = v_produto_item or mqop_060.item_estrutura ='000000');
      end;

      v_temporario := 0;
      v_ob_existentes := '';
      
      -- Verifica se alguma OB na lista já foi inserida na pcpb_100
      for v_ob_existe in
         (select to_number(REGEXP_SUBSTR(str, exp, 1, level)) OB
          from  (select trim(p_lista_ob) str, '[^;]+' exp 
                 from dual)
          connect by REGEXP_SUBSTR(str, exp, 1, level) is not null)
      loop

      dbms_output.put_line('v_ob_existe.OB: ' || to_char(v_ob_existe.OB));
         begin
            select count(1)
            INTO v_temporario
            from pcpb_100
            where pcpb_100.ordem_producao = v_ob_existe.OB;
         end;
         
         if v_temporario > 0
         then
            if v_ob_existentes IS NULL
            then 
               v_ob_existentes :=  'OB s ja associadas a OT: ' || to_char(v_ob_existe.OB)  ;
            else     v_ob_existentes :=   v_ob_existentes || ';'|| to_char(v_ob_existe.OB)    ;
            end if;
            v_temporario := 0;
         end if;
         
      end loop;
      
      dbms_output.put_line('v_ob_existentes: ' || to_char(v_ob_existentes));
      -- Faz insert da lista de OB na pcpb_100 caso não exista nenhuma OB já inserida
      if v_ob_existentes IS NULL
      then

        if p_tipo_ordem = 1
        then
      
           begin
            select seq_pcpb_100_1.nextval 
            INTO   v_ordem_agrupamento
            from   dual;
          end;
      
         end if;
   
         if p_tipo_ordem = 2
         then
      
            begin
               select seq_pcpb_100_2.nextval 
               INTO   v_ordem_agrupamento
               from   dual;
            end;
         
         end if;
        
         p_ot_gerada := v_ordem_agrupamento;
      
         for v_ob_aux in
            (select to_number(REGEXP_SUBSTR(str, exp, 1, level)) OB
             from  (select trim(p_lista_ob) str, '[^;]+' exp 
                    from dual)
             connect by REGEXP_SUBSTR(str, exp, 1, level) is not null)
         loop
         
            if p_tipo_ordem = 1
            then
            
               begin
                  insert into pcpb_100 (tipo_ordem,                ordem_agrupamento, 
                                        ordem_producao,            data_digitacao, 
                                        data_prev_termino,         grupo_maquina,
                                        subgrupo_maquina,          numero_maquina,
                                        relacao_banho,             nivel_estrutura,
                                        data_prevista_maquina,     sequencia_entrada_maquina,
                                        cod_ordem_impressa,        observacao,
                                        lote,                      data_prevista_ini,
                                        data_prevista_fim,         hora_prevista_ini,
                                        hora_prevista_fim) 
                                values (p_tipo_ordem,              p_ot_gerada,
                                        v_ob_aux.OB,               sysdate,
                                        sysdate,                   v_grupo_maquina,
                                        v_subgrupo_maquina,        v_numero_maquina,
                                        v_relacao_banho,           '2',
                                        sysdate,                   99,
                                        v_mdi_cod_ordem_impressa,  p_observacao,
                                        0,                         sysdate,
                                        sysdate,                   sysdate,
                                        sysdate
                                );
                  exception when others then
                     p_ot_gerada := 0;
                     v_msg_erro := v_msg_erro || 'ATENÇÃO! não inseriu na PCPB_100 com Tipo de Ordem 1. ';
                     EXIT;
               end;
               
            end if;
            
            if p_tipo_ordem = 2
            then
      
               begin
                  insert into pcpb_100 (tipo_ordem,                ordem_agrupamento, 
                                        ordem_producao,            data_digitacao, 
                                        data_prev_termino,         grupo_maquina,
                                        subgrupo_maquina,          numero_maquina,
                                        relacao_banho,             nivel_estrutura,
                                        data_prevista_maquina,     sequencia_entrada_maquina,
                                        cod_ordem_impressa,        observacao,
                                        lote,                      data_prevista_ini,
                                        data_prevista_fim,         hora_prevista_ini,
                                        hora_prevista_fim) 
                                values (p_tipo_ordem,              p_ot_gerada,
                                        v_ob_aux.OB,               sysdate,
                                        sysdate,                   v_grupo_maquina,
                                        v_subgrupo_maquina,        v_numero_maquina,
                                        1,                         '2',
                                        sysdate,                   99,
                                        v_mdi_cod_ordem_impressa,  p_observacao,
                                        0,                         sysdate,
                                        sysdate,                   sysdate,
                                        sysdate
                                );
               exception when others then
                  p_ot_gerada := 0;
                  v_msg_erro := v_msg_erro || 'ATENÇÃO! não inseriu na PCPB_100 com Tipo de Ordem 2. ';
                  EXIT;
               end;
               
            end if;
      
         end loop;         
      
      else 
         v_msg_erro := v_msg_erro || ' ' || v_ob_existentes;      
      end if;
      
   end if;

   if trim(v_msg_erro) IS NOT NULL
   then
   
      begin
         insert into pcpb_812 (
            pcpb_812.id,             pcpb_812.mensagem, 
            pcpb_812.data_insercao,  pcpb_812.tipo_processo
         ) values (
            p_id,      v_msg_erro, 
            sysdate,   'OT'
         );
      end;
      
   end if;
   
end inter_pr_gerar_ot;
/
