
  CREATE OR REPLACE PROCEDURE "INTER_PR_INSERE_ITENS_DRAW" 
   (p_empresa  in numeric,   p_documento in numeric, p_serie in varchar2,
    p_cnpj9    in numeric,   p_cnpj4    in numeric,  p_cnpj2 in numeric,
    p_nr_draw  in numeric)
is
   cursor obrf015 is
      select obrf_015.coditem_nivel99 pnivel, obrf_015.coditem_grupo pgrupo,
             obrf_015.coditem_subgrupo psub , obrf_015.coditem_item pitem,
             obrf_015.quantidade pqtde,       obrf_015.valor_unitario pvlr_uni,
             obrf_015.codigo_deposito pdep
      from obrf_015
      where obrf_015.capa_ent_nrdoc   = p_documento
        and obrf_015.capa_ent_serie   = p_serie
        and obrf_015.capa_ent_forcli9 = p_cnpj9
        and obrf_015.capa_ent_forcli4 = p_cnpj4
        and obrf_015.capa_ent_forcli2 = p_cnpj2;

   v_ctrl_draw      basi_015.controla_draw_back%type;
   v_seq            number;
begin
   v_seq := 0;
   for reg_obrf015 in obrf015
   loop

     begin
        /*Verifica se o produto e controlado por draw back*/
         select basi_015.controla_draw_back into  v_ctrl_draw
         from basi_015
            where basi_015.codigo_empresa    = p_empresa
              and  basi_015.nivel_estrutura   = reg_obrf015.pnivel
              and basi_015.grupo_estrutura    = reg_obrf015.pgrupo
              and basi_015.subgru_estrutura   = reg_obrf015.psub
              and basi_015.item_estrutura     = reg_obrf015.pitem
              and basi_015.controla_draw_back = 1;
         exception
            when others then
               v_ctrl_draw := 0;
      end;


      if v_ctrl_draw = 1
      then

         v_seq := v_seq + 1;

         insert into obrf_018
         (empresa,
          draw_back_entrada,
          sequencia_entrada,
          nivel_insumo,
          grupo_insumo,
          subgrupo_insumo,
          item_insumo,
          quantidade_entrada,
          valor_entrada,
          situacao
          )
      VALUES (
          p_empresa,
          p_nr_draw,
          v_seq,
          reg_obrf015.pnivel,
          reg_obrf015.pgrupo,
          reg_obrf015.psub,
          reg_obrf015.pitem,
          reg_obrf015.pqtde,
          reg_obrf015.pvlr_uni*reg_obrf015.pqtde,
          0);

      end if;
   end loop;
end inter_pr_insere_itens_draw;

 

/

exec inter_pr_recompile;

