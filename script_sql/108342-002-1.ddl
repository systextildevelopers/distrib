alter table mqop_030 add (volume_decimal NUMBER(6,2) default 0);
comment on column mqop_030.volume_decimal is 'Volume de banho controlado pelo cadastro de campos acessíveis e requeridos (oper_f006).';

commit;
