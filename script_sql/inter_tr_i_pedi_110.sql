
  CREATE OR REPLACE TRIGGER "INTER_TR_I_PEDI_110" 
before insert on i_pedi_110
for each row

declare
  v_id_registro number;
begin
   if :new.id_registro is null or
      :new.id_registro = 0
   then
      select SEQ_IMPORTACAO.nextval into v_id_registro from dual;
      :new.id_registro     := v_id_registro;
      :new.data_exportacao := sysdate;
   end if;
end inter_tr_i_pedi_110;

-- ALTER TRIGGER "INTER_TR_I_PEDI_110" ENABLE
 

/

exec inter_pr_recompile;

