
  CREATE OR REPLACE PROCEDURE "INTER_PR_ATU_REVES_COMP" (p_ordem_planejamento        in number,
                                                    p_pedido_venda              in number,
                                                    p_nivel_produto_org         in varchar2,
                                                    p_grupo_produto_org         in varchar2,
                                                    p_subgrupo_produto_org      in varchar2,
                                                    p_item_produto_org          in varchar2,
                                                    p_seq_produto_org           in number,
                                                    p_area_prod                 in number,
                                                    p_ordem_producao            in number,
                                                    p_qtde_prog_ordem           in number)
is

   v_qtde_prod_pedido number;

begin

   for reg_atu_comp in (select tmrp_625.nivel_produto,    tmrp_625.grupo_produto,
                               tmrp_625.subgrupo_produto, tmrp_625.item_produto,
                               tmrp_625.seq_produto,      tmrp_625.alternativa_produto,
                               tmrp_625.data_requerida,   tmrp_625.seq_produto_origem,
                               tmrp_625.consumo
                        from tmrp_625
                        where tmrp_625.ordem_planejamento         = p_ordem_planejamento
                          and (tmrp_625.pedido_venda              = p_pedido_venda
                            or tmrp_625.pedido_reserva            = p_pedido_venda)
                          and tmrp_625.nivel_produto_origem       = p_nivel_produto_org
                          and tmrp_625.grupo_produto_origem       = p_grupo_produto_org
                          and tmrp_625.subgrupo_produto_origem    = p_subgrupo_produto_org
                          and tmrp_625.item_produto_origem        = p_item_produto_org
                          and tmrp_625.seq_produto_origem         = p_seq_produto_org
                          order by tmrp_625.nivel_produto,    tmrp_625.grupo_produto,
                                   tmrp_625.subgrupo_produto, tmrp_625.item_produto)
   loop
      -- calculando a quantidade pelo consumo da tmrp_625
      v_qtde_prod_pedido := p_qtde_prog_ordem * reg_atu_comp.consumo;

      -- atualizando a quatidade dos componentes
      begin
         update tmrp_625
         set qtde_reserva_programada = tmrp_625.qtde_reserva_programada - v_qtde_prod_pedido
         where tmrp_625.ordem_planejamento          = p_ordem_planejamento
           and (tmrp_625.pedido_venda               = p_pedido_venda
             or tmrp_625.pedido_reserva             = p_pedido_venda)
           and tmrp_625.nivel_produto_origem        = p_nivel_produto_org
           and tmrp_625.grupo_produto_origem        = p_grupo_produto_org
           and tmrp_625.subgrupo_produto_origem     = p_subgrupo_produto_org
           and tmrp_625.item_produto_origem         = p_item_produto_org
           and tmrp_625.seq_produto_origem          = reg_atu_comp.seq_produto_origem
           and tmrp_625.nivel_produto               = reg_atu_comp.nivel_produto
           and tmrp_625.grupo_produto               = reg_atu_comp.grupo_produto
           and tmrp_625.subgrupo_produto            = reg_atu_comp.subgrupo_produto
           and tmrp_625.item_produto                = reg_atu_comp.item_produto
           and tmrp_625.seq_produto                 = reg_atu_comp.seq_produto;
      exception
      when OTHERS then
         raise_application_error(-20000,'ATENCAO! Nao atualizou TMRP_625 (QTDE A RECEBER - QTDE RESERVADA)');
      end;

      if (reg_atu_comp.nivel_produto = '3' or reg_atu_comp.nivel_produto = '5') and p_area_prod <> 4
      then
         inter_pr_atu_reves_comp(p_ordem_planejamento,                p_pedido_venda,
                                 reg_atu_comp.nivel_produto,          reg_atu_comp.grupo_produto,
                                 reg_atu_comp.subgrupo_produto,       reg_atu_comp.item_produto,
                                 0,                                   p_area_prod,
                                 p_ordem_producao,                    p_qtde_prog_ordem);
      end if;
   end loop;
end inter_pr_atu_reves_comp;

 

/

exec inter_pr_recompile;

