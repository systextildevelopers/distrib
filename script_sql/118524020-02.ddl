INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'basi_l266',     0,
	1,			     'Configuração de Tipo de Título por Moeda');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'basi_l266',   'basi_menu', 
	0,             0, 
	'S',           'S', 
	'S',           'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Configuração de Tipo de Título por Moeda'
 WHERE hdoc_036.codigo_programa = 'basi_l266'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;

EXEC inter_pr_recompile;
