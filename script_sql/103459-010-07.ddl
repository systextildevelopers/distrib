ALTER TABLE RCNB_760
ADD valor_custo_com_icms number(17,10);

ALTER TABLE RCNB_760
ADD  valor_custo_sem_icms number(17,10);


ALTER TABLE RCNB_760
ADD     perc_icms            number(5,2) ;

ALTER TABLE RCNB_760
ADD  valor_custo_mp number(17,10);



ALTER TABLE RCNB_760
modify (valor_custo_com_icms  default 0.00000,
        valor_custo_sem_icms  default 0.00000,
        perc_icms             default 0.00000,
        valor_custo_mp        default 0.00000);


comment on column RCNB_760.valor_custo_com_icms is 'Mostra o preco de custo sem mao de obra, com icms';
comment on column RCNB_760.valor_custo_sem_icms is 'Mostra o preco de custo sem mao de obra, sem icms';
comment on column RCNB_760.perc_icms is 'percentual de icms';

exec inter_pr_recompile;
/
