alter table tmrp_690 add inc_exc_estagios_cost number(1) default 0;
comment on column tmrp_690.inc_exc_estagios_cost is 'Inclusão (1) ou exclusão (2) de estágios costura para exibição do tempo de costura na projeção (tela tmrp_f690) conforme o que foi informado na geração (tmrp_f680)';

alter table tmrp_690 add lista_estagios_cost varchar2(4000) default '';
comment on column tmrp_690.lista_estagios_cost is 'Lista dos estágios informada no tmrp_f680 para geração da projeção, para exibir o tempo de costura na projeção (tela tmrp_f690).';

exec inter_pr_recompile;
