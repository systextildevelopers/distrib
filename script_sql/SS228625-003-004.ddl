create table oper_024
(
COD_PORTADOR     NUMBER(3)  default 0,
CODIGO_CONTABIL  NUMBER(6)  default 0,
CODIGO_TRANSACAO NUMBER(3)  default 0,
CODIGO_HISTORICO_CONT NUMBER(4)  default 0,
POSICAO_TITULO NUMBER(9) default 0,
NR_DIAS_UTEIS NUMBER(6) default 0
);


exec inter_pr_recompile;
