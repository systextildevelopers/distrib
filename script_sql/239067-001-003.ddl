CREATE TABLE BLOCOK_280 (
    COD_EMPRESA            NUMBER(3,0),
	ANO_PERIODO_APUR       NUMBER(4,0),
	MES_PERIODO_APUR       NUMBER(2,0),
	COD_ITEM               VARCHAR2(60),
	REGISTRO               VARCHAR2(4BYTE) DEFAULT 'K280',
    DT_EST                 DATE,
    COD_ITEM_NIVEL         VARCHAR2(1)   DEFAULT ' ' NOT NULL,
	COD_ITEM_GRUPO         VARCHAR2(5)   DEFAULT ' ' NOT NULL,
	COD_ITEM_SUBGRUPO      VARCHAR2(3)   DEFAULT ' ' NOT NULL,
	COD_ITEM_ITEM          VARCHAR2(6)   DEFAULT ' ' NOT NULL,
    QTDE_COR_POS           NUMBER(13,5),
    QTDE_COR_NEG           NUMBER(13,5),
    IND_EST                NUMBER(1) DEFAULT 0,
    CNPJ_9                 NUMBER(9) DEFAULT 0,
    CNPJ_4                 NUMBER(4) DEFAULT 0,
    CNPJ_2                 NUMBER(2) DEFAULT 0
);

COMMENT ON COLUMN BLOCOK_280.DT_EST           IS 'Data do Estoque a corrigir.';
COMMENT ON COLUMN BLOCOK_280.CNPJ_9           IS 'Cnpj de propriedade do produto';
COMMENT ON COLUMN BLOCOK_280.CNPJ_4           IS 'Cnpj de propriedade do produto';
COMMENT ON COLUMN BLOCOK_280.CNPJ_2           IS 'Cnpj de propriedade do produto';

/
exec inter_pr_recompile;
