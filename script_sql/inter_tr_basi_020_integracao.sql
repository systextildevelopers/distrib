
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_020_INTEGRACAO" 
before insert or
       delete or
       update of basi030_referenc,    tamanho_ref,       descr_tam_refer,
                 tipo_produto,        grupo_agrupador,   sub_agrupador,
                 desc_tam_ficha,      gramatura_1,       peso_rolo,
                 composicao_01,       composicao_02,     composicao_03,
                 composicao_04,       composicao_05,     perc_composicao1,
                 perc_composicao2,    perc_composicao3,  perc_composicao4,
                 perc_composicao5,    rendimento,        largura_1,
                 largura_2,           cod_processo,      tubular_aberto
       on basi_020
for each row

declare

                                                                            v_sol_friccao_seco_ini            	  e_basi_020.sol_friccao_seco_ini%type;
v_basi030_nivel030           e_basi_020.basi030_nivel030%type;              v_sol_friccao_seco_fim            	  e_basi_020.sol_friccao_seco_fim%type;
v_basi030_referenc           e_basi_020.basi030_referenc%type;              v_solidez_friccao_umido           	  e_basi_020.solidez_friccao_umido%type;
v_tamanho_ref                e_basi_020.tamanho_ref%type;                   v_sol_friccao_umido_ini           	  e_basi_020.sol_friccao_umido_ini%type;
v_descr_tam_refer            e_basi_020.descr_tam_refer%type;               v_sol_friccao_umido_fim           	  e_basi_020.sol_friccao_umido_fim%type;
v_tipo_produto               e_basi_020.tipo_produto%type;                  v_metros_lineares                 	  e_basi_020.metros_lineares%type;
v_artigo_cotas               e_basi_020.artigo_cotas%type;                  v_metros_cubicos                  	  e_basi_020.metros_cubicos%type;
v_largura_1                  e_basi_020.largura_1%type;                     v_agulhas_falhadas                	  e_basi_020.agulhas_falhadas%type;
v_gramatura_1                e_basi_020.gramatura_1%type;                   v_num_agulhas_falhadas            	  e_basi_020.num_agulhas_falhadas%type;
v_peso_rolo                  e_basi_020.peso_rolo%type;                     v_numero_pontos                   	  e_basi_020.numero_pontos%type;
v_data_importacao            e_basi_020.data_importacao%type;               v_polegadas_agulha                	  e_basi_020.polegadas_agulha%type;
v_peso_mini_rolo             e_basi_020.peso_mini_rolo%type;
v_compos_comer01             e_basi_020.compos_comer01%type;
v_lote_fabr_pecas            e_basi_020.lote_fabr_pecas%type;          	    v_compos_comer02           		  e_basi_020.compos_comer02%type;
v_peso_liquido               e_basi_020.peso_liquido%type;             	    v_compos_comer03           		  e_basi_020.compos_comer03%type;
v_qtde_max_enfesto           e_basi_020.qtde_max_enfesto%type;         	    v_compos_comer04           		  e_basi_020.compos_comer04%type;
v_comp_max_enfesto           e_basi_020.comp_max_enfesto%type;         	    v_descr_ing                		  e_basi_020.descr_ing%type;
v_composicao_01              e_basi_020.composicao_01%type;            	    v_descr_ing2               		  e_basi_020.descr_ing2%type;
v_composicao_02              e_basi_020.composicao_02%type;            	    v_descr_esp                		  e_basi_020.descr_esp%type;
v_composicao_03              e_basi_020.composicao_03%type;            	    v_compos_ing1              		  e_basi_020.compos_ing1%type;
v_composicao_04              e_basi_020.composicao_04%type;            	    v_compos_ing2              		  e_basi_020.compos_ing2%type;
v_composicao_05              e_basi_020.composicao_05%type;            	    v_compos_ing3              		  e_basi_020.compos_ing3%type;
v_largura_2                  e_basi_020.largura_2%type;                	    v_compos_ing4              		  e_basi_020.compos_ing4%type;
v_gramatura_2                e_basi_020.gramatura_2%type;              	    v_compos_esp1              		  e_basi_020.compos_esp1%type;
v_gramatura_3                e_basi_020.gramatura_3%type;              	    v_compos_esp2              		  e_basi_020.compos_esp2%type;
v_avaliacao_toque            e_basi_020.avaliacao_toque%type;          	    v_compos_esp3              		  e_basi_020.compos_esp3%type;
v_classe_fio                 e_basi_020.classe_fio%type;               	    v_compos_esp4              		  e_basi_020.compos_esp4%type;
v_cursos                     e_basi_020.cursos%type;                   	    v_observacao               		  e_basi_020.observacao%type;
v_elasticidade_aca           e_basi_020.elasticidade_aca%type;         	    v_obs_ing                  		  e_basi_020.obs_ing%type;
v_elasticidade_cru           e_basi_020.elasticidade_cru%type;         	    v_obs_esp                  		  e_basi_020.obs_esp%type;
v_enco_comprimento           e_basi_020.enco_comprimento%type;         	    v_gramatura_pre_estampado  		  e_basi_020.gramatura_pre_estampado%type;
v_enco_largura               e_basi_020.enco_largura%type;             	    v_peso_rolo_pre_estampado  		  e_basi_020.peso_rolo_pre_estampado%type;
v_fator_cobertura            e_basi_020.fator_cobertura%type;          	    v_largura_pre_estampado    		  e_basi_020.largura_pre_estampado%type;
v_grau_compactacao           e_basi_020.grau_compactacao%type;         	    v_comprim_pre_estampado    		  e_basi_020.comprim_pre_estampado%type;
v_kit_elastano               e_basi_020.kit_elastano%type;             	    v_perdas_pre_estampado     		  e_basi_020.perdas_pre_estampado%type;
v_mercerizado                e_basi_020.mercerizado%type;              	    v_limite_falhas_rolo       		  e_basi_020.limite_falhas_rolo%type;
v_nr_alimentadores           e_basi_020.nr_alimentadores%type;         	    v_limite_falhas_mini       		  e_basi_020.limite_falhas_mini%type;
v_numero_agulhas             e_basi_020.numero_agulhas%type;           	    v_tato                     		  e_basi_020.tato%type;
v_perc_composicao1           e_basi_020.perc_composicao1%type;         	    v_referencia_origem        		  e_basi_020.referencia_origem%type;
v_perc_composicao2           e_basi_020.perc_composicao2%type;         	    v_consumo_pasta_normal     		  e_basi_020.consumo_pasta_normal%type;
v_perc_composicao3           e_basi_020.perc_composicao3%type;         	    v_largura_cilindro         		  e_basi_020.largura_cilindro%type;
v_perc_composicao4           e_basi_020.perc_composicao4%type;         	    v_largura_estampa          		  e_basi_020.largura_estampa%type;
v_perc_composicao5           e_basi_020.perc_composicao5%type;         	    v_compos_ing5              		  e_basi_020.compos_ing5%type;
v_perc_perdas                e_basi_020.perc_perdas%type;              	    v_compos_esp5              		  e_basi_020.compos_esp5%type;
v_perc_umidade               e_basi_020.perc_umidade%type;             	    v_compos_comer05           		  e_basi_020.compos_comer05%type;
v_relach_comprimen           e_basi_020.relach_comprimen%type;         	    v_cod_fluxo                		  e_basi_020.cod_fluxo%type;
v_relach_largura             e_basi_020.relach_largura%type;           	    v_perdas_antes_tingimento  		  e_basi_020.perdas_antes_tingimento%type;
v_rendimento                 e_basi_020.rendimento%type;               	    v_peso_multiplo            		  e_basi_020.peso_multiplo%type;
v_temperatura_estp           e_basi_020.temperatura_estp%type;         	    v_tem_dep_rolo             		  e_basi_020.tem_dep_rolo%type;
v_temperatura_fixa           e_basi_020.temperatura_fixa%type;         	    v_tem_dep_mini_rolo        		  e_basi_020.tem_dep_mini_rolo%type;
v_tempo_fixacao              e_basi_020.tempo_fixacao%type;            	    v_tem_dep_estamparia       		  e_basi_020.tem_dep_estamparia%type;
v_tempo_termofixar           e_basi_020.tempo_termofixar%type;         	    v_composicao_06            		  e_basi_020.composicao_06%type;
v_termofixar                 e_basi_020.termofixar%type;               	    v_perc_composicao6         		  e_basi_020.perc_composicao6%type;
v_titulo_fio                 e_basi_020.titulo_fio%type;              	    v_diametro                 		  e_basi_020.diametro%type;
v_torcao_acabado             e_basi_020.torcao_acabado%type;           	    v_grupo_encolhimento       		  e_basi_020.grupo_encolhimento%type;
v_torcao_cru                 e_basi_020.torcao_cru%type;               	    v_homologacao_fornec       		  e_basi_020.homologacao_fornec%type;
v_tubular_aberto             e_basi_020.tubular_aberto%type;           	    v_litros_absorcao          		  e_basi_020.litros_absorcao%type;
v_variacao_fio               e_basi_020.variacao_fio%type;             	    v_nr_rolos_cubagem         		  e_basi_020.nr_rolos_cubagem%type;
v_fator                      e_basi_020.fator%type;                    	    v_descr_marca_reg          		  e_basi_020.descr_marca_reg%type;
v_ind_mt_minutos             e_basi_020.ind_mt_minutos%type;           	    v_codigo_embalagem         		  e_basi_020.codigo_embalagem%type;
v_ind_kg_maq_dia             e_basi_020.ind_kg_maq_dia%type;           	    v_desc_tam_ficha           		  e_basi_020.desc_tam_ficha%type;
v_ind_eficiencia             e_basi_020.ind_eficiencia%type;          	    v_comprimento_mini         		  e_basi_020.comprimento_mini%type;
v_ne_titulo                  e_basi_020.ne_titulo%type;                	    v_caract                   		  e_basi_020.caract%type;
v_processo_unico_tec         e_basi_020.processo_unico_tec%type;       	    v_caract_ing               		  e_basi_020.caract_ing%type;
v_gramatura_vaporizado       e_basi_020.gramatura_vaporizado%type;     	    v_caract_esp               		  e_basi_020.caract_esp%type;
v_cod_processo               e_basi_020.cod_processo%type;             	    v_perc_difer_peso          		  e_basi_020.perc_difer_peso%type;
v_grade_distribuicao         e_basi_020.grade_distribuicao%type;       	    v_sequencia_tamanho        		  e_basi_020.sequencia_tamanho%type;
v_largura_saida_maquina      e_basi_020.largura_saida_maquina%type;    	    v_raport                   		  e_basi_020.raport%type;
v_gramatura_saida_maquina    e_basi_020.gramatura_saida_maquina%type;  	    v_num_desenvolvimento      		  e_basi_020.num_desenvolvimento%type;
v_cursos_saida_maquina       e_basi_020.cursos_saida_maquina%type;     	    v_alternativa_padrao       		  e_basi_020.alternativa_padrao%type;
v_colunas_saida_maquina      e_basi_020.colunas_saida_maquina%type;    	    v_roteiro_padrao           		  e_basi_020.roteiro_padrao%type;
v_largura_apos_repouso       e_basi_020.largura_apos_repouso%type;     	    v_peso_quebra              		  e_basi_020.peso_quebra%type;
v_gramatura_apos_repouso     e_basi_020.gramatura_apos_repouso%type;   	    v_grupo_tecido_costurar    		  e_basi_020.grupo_tecido_costurar%type;
v_cursos_apos_repouso        e_basi_020.cursos_apos_repouso%type;      	    v_batidas_polegadas        		  e_basi_020.batidas_polegadas%type;
v_colunas_apos_repouso       e_basi_020.colunas_apos_repouso%type;    	    v_ordem_compra             		  e_basi_020.ordem_compra%type;
v_perc_variacao_gramatura    e_basi_020.perc_variacao_gramatura%type;  	    v_tipo_produto_global      		  e_basi_020.tipo_produto_global%type;
v_tam_ponto_1                e_basi_020.tam_ponto_1%type;              	    v_batidas_metro            		  e_basi_020.batidas_metro%type;
v_tam_ponto_2                e_basi_020.tam_ponto_2%type;              	    v_codigo_pente             		  e_basi_020.codigo_pente%type;
v_tam_ponto_3                e_basi_020.tam_ponto_3%type;              	    v_densidade_pente          		  e_basi_020.densidade_pente%type;
v_tam_ponto_4                e_basi_020.tam_ponto_4%type;              	    v_qtde_fios                		  e_basi_020.qtde_fios%type;
v_ne_titulo_2                e_basi_020.ne_titulo_2%type;              	    v_qtde_fios_fundo          		  e_basi_020.qtde_fios_fundo%type;
v_ne_titulo_3                e_basi_020.ne_titulo_3%type;              	    v_qtde_fios_ourela         		  e_basi_020.qtde_fios_ourela%type;
v_ne_titulo_4                e_basi_020.ne_titulo_4%type;              	    v_perc_encolhimento        		  e_basi_020.perc_encolhimento%type;
v_lfa_1                      e_basi_020.lfa_1%type;                    	    v_passamento_fundo         		  e_basi_020.passamento_fundo%type;
v_lfa_2                      e_basi_020.lfa_2%type;                    	    v_passamento_ourela        		  e_basi_020.passamento_ourela%type;
v_lfa_3                      e_basi_020.lfa_3%type;                    	    v_puas_fundo               		  e_basi_020.puas_fundo%type;
v_lfa_4                      e_basi_020.lfa_4%type;                    	    v_puas_ourela              		  e_basi_020.puas_ourela%type;
v_tear_finura                e_basi_020.tear_finura%type;              	    v_largura_pente            		  e_basi_020.largura_pente%type;
v_tear_diametro              e_basi_020.tear_diametro%type;            	    v_contracao_urdume         		  e_basi_020.contracao_urdume%type;
v_perc_alt_dim_comp          e_basi_020.perc_alt_dim_comp%type;        	    v_contracao_trama          		  e_basi_020.contracao_trama%type;
v_perc_alt_dim_larg          e_basi_020.perc_alt_dim_larg%type;        	    v_oz_yd2                   		  e_basi_020.oz_yd2%type;
v_largura_proj               e_basi_020.largura_proj%type;             	    v_cor_etiqueta             		  e_basi_020.cor_etiqueta%type;
v_gramatura_proj             e_basi_020.gramatura_proj%type;           	    v_linha_producao           		  e_basi_020.linha_producao%type;
v_larg_var_ini               e_basi_020.larg_var_ini%type;             	    v_titulo_fibra             		  e_basi_020.titulo_fibra%type;
v_larg_var_fim               e_basi_020.larg_var_fim%type;             	    v_um_titulo_fibra          		  e_basi_020.um_titulo_fibra%type;
v_gar_var_ini                e_basi_020.gar_var_ini%type;              	    v_simbolo_1                		  e_basi_020.simbolo_1%type;
v_gar_var_fim                e_basi_020.gar_var_fim%type;              	    v_simbolo_2                		  e_basi_020.simbolo_2%type;
v_cursos_var_ini             e_basi_020.cursos_var_ini%type;           	    v_simbolo_3                		  e_basi_020.simbolo_3%type;
v_cursos_var_fim             e_basi_020.cursos_var_fim%type;           	    v_simbolo_4                		  e_basi_020.simbolo_4%type;
v_colunas_var_ini            e_basi_020.colunas_var_ini%type;          	    v_simbolo_5                		  e_basi_020.simbolo_5%type;
v_colunas_var_fim            e_basi_020.colunas_var_fim%type;          	    v_cod_toler_rendim         		  e_basi_020.cod_toler_rendim%type;
v_torcao_aca_var_ini         e_basi_020.torcao_aca_var_ini%type;       	    v_solidez_agua_mar         		  e_basi_020.solidez_agua_mar%type;
v_torcao_aca_var_fim         e_basi_020.torcao_aca_var_fim%type;       	    v_sol_agua_mar_ini         		  e_basi_020.sol_agua_mar_ini%type;
v_tipo_retorno               e_basi_020.tipo_retorno%type;             	    v_perc_retracao            		  e_basi_020.perc_retracao%type;
v_perc_var_largura           e_basi_020.perc_var_largura%type;         	    v_altura_1                 		  e_basi_020.altura_1%type;
v_perc_var_compr             e_basi_020.perc_var_compr%type;           	    v_altura_2                 		  e_basi_020.altura_2%type;
v_ne_titulo_1                e_basi_020.ne_titulo_1%type;              	    v_cursos_centimetros       		  e_basi_020.cursos_centimetros%type;
v_alongamento                e_basi_020.alongamento%type;              	    v_tamanho_retilinea        		  e_basi_020.tamanho_retilinea%type;
v_grupo_agrupador            e_basi_020.grupo_agrupador%type;          	    v_dias_armazenamento       		  e_basi_020.dias_armazenamento%type;
v_sub_agrupador              e_basi_020.sub_agrupador%type;            	    v_peso_medio_cone          		  e_basi_020.peso_medio_cone%type;
v_tipo_homologacao           e_basi_020.tipo_homologacao%type;         	    v_un_med_titulo_fio        		  e_basi_020.un_med_titulo_fio%type;
v_nro_etiqs_homologacao      e_basi_020.nro_etiqs_homologacao%type;    	    v_grupo_tecido_cortar      		  e_basi_020.grupo_tecido_cortar%type;
v_solidez_lavacao            e_basi_020.solidez_lavacao%type;          	    v_permite_misturar_maquina 		  e_basi_020.permite_misturar_maquina%type;
v_sol_lavacao_ini            e_basi_020.sol_lavacao_ini%type;          	    v_colunas_ercru            		  e_basi_020.colunas_ercru%type;
v_sol_lavacao_fim            e_basi_020.sol_lavacao_fim%type;          	    v_cursos_ercru             		  e_basi_020.cursos_ercru%type;
v_solidez_suor               e_basi_020.solidez_suor%type;             	    v_gramatura_ercru          		  e_basi_020.gramatura_ercru%type;
v_sol_suor_ini               e_basi_020.sol_suor_ini%type;             	    v_largura_ercru            		  e_basi_020.largura_ercru%type;
v_sol_suor_fim               e_basi_020.sol_suor_fim%type;             	    v_largura_pf_1             		  e_basi_020.largura_pf_1%type;
v_solidez_agua_clorada       e_basi_020.solidez_agua_clorada%type;     	    v_largura_pf_2             		  e_basi_020.largura_pf_2%type;
v_sol_agua_clorada_ini       e_basi_020.sol_agua_clorada_ini%type;     	    v_altura_pf_1              		  e_basi_020.altura_pf_1%type;
v_sol_agua_clorada_fim       e_basi_020.sol_agua_clorada_fim%type;     	    v_altura_pf_2              		  e_basi_020.altura_pf_2%type;
v_sol_agua_mar_fim           e_basi_020.sol_agua_mar_fim%type;         	    v_compri_plotagem  			  e_basi_020.compri_plotagem%type;
v_solidez_friccao_seco       e_basi_020.solidez_friccao_seco%type;

v_tipo_operacao              e_basi_020.tipo_operacao%type;
ws_usuario_systextil         e_basi_020.usuario_operacao%type;

ws_usuario_rede              varchar2(20);
ws_maquina_rede              varchar2(40);
ws_aplicativo                varchar2(20);
ws_sid                       number(9);
ws_empresa                   number(3);
ws_locale_usuario            varchar2(5);

begin

inter_pr_dados_usuario (ws_usuario_rede, ws_maquina_rede, ws_aplicativo, ws_sid, ws_usuario_systextil, ws_empresa, ws_locale_usuario);

v_sol_friccao_seco_ini      	:= :new.sol_friccao_seco_ini;
v_basi030_nivel030           := :new.basi030_nivel030;         		    v_sol_friccao_seco_fim      	:= :new.sol_friccao_seco_fim;
v_basi030_referenc           := :new.basi030_referenc;         		    v_solidez_friccao_umido     	:= :new.solidez_friccao_umido;
v_tamanho_ref                := :new.tamanho_ref;              		    v_sol_friccao_umido_ini     	:= :new.sol_friccao_umido_ini;
v_descr_tam_refer            := :new.descr_tam_refer;          		    v_sol_friccao_umido_fim     	:= :new.sol_friccao_umido_fim;
v_tipo_produto               := :new.tipo_produto;             		    v_metros_lineares           	:= :new.metros_lineares;
v_artigo_cotas               := 0;                            		    v_metros_cubicos            	:= :new.metros_cubicos;
v_largura_1                  := :new.largura_1;                		    v_agulhas_falhadas          	:= :new.agulhas_falhadas;
v_gramatura_1                := :new.gramatura_1;              		    v_num_agulhas_falhadas      	:= :new.num_agulhas_falhadas;
v_peso_rolo                  := :new.peso_rolo;                		    v_numero_pontos             	:= :new.numero_pontos;
v_data_importacao            := sysdate();          		              v_polegadas_agulha          	:= :new.polegadas_agulha;
v_peso_mini_rolo             := :new.peso_mini_rolo;
v_compos_comer01           	 := :new.compos_comer01;
v_lote_fabr_pecas            := :new.lote_fabr_pecas;          		    v_compos_comer02           		:= :new.compos_comer02;
v_peso_liquido               := :new.peso_liquido;          		      v_compos_comer03           		:= :new.compos_comer03;
v_qtde_max_enfesto           := :new.qtde_max_enfesto;         		    v_compos_comer04           		:= :new.compos_comer04;
v_comp_max_enfesto           := :new.comp_max_enfesto;         		    v_descr_ing                		:= :new.descr_ing;
v_composicao_01              := :new.composicao_01;            		    v_descr_ing2               		:= :new.descr_ing2;
v_composicao_02              := :new.composicao_02;            		    v_descr_esp                		:= :new.descr_esp;
v_composicao_03              := :new.composicao_03;            		    v_compos_ing1              		:= :new.compos_ing1;
v_composicao_04              := :new.composicao_04;           		    v_compos_ing2              		:= :new.compos_ing2;
v_composicao_05              := :new.composicao_05;            		    v_compos_ing3              		:= :new.compos_ing3;
v_largura_2                  := :new.largura_2;                		    v_compos_ing4              		:= :new.compos_ing4;
v_gramatura_2                := :new.gramatura_2;              		    v_compos_esp1              		:= :new.compos_esp1;
v_gramatura_3                := :new.gramatura_3;              		    v_compos_esp2              		:= :new.compos_esp2;
v_avaliacao_toque            := :new.avaliacao_toque;         		    v_compos_esp3              		:= :new.compos_esp3;
v_classe_fio                 := :new.classe_fio;               		    v_compos_esp4              		:= :new.compos_esp4;
v_cursos                     := :new.cursos;                   		    v_observacao               		:= :new.observacao;
v_elasticidade_aca           := :new.elasticidade_aca;         		    v_obs_ing                  		:= :new.obs_ing;
v_elasticidade_cru           := :new.elasticidade_cru;         		    v_obs_esp                  		:= :new.obs_esp;
v_enco_comprimento           := :new.enco_comprimento;         		    v_gramatura_pre_estampado  		:= :new.gramatura_pre_estampado;
v_enco_largura               := :new.enco_largura;             		    v_peso_rolo_pre_estampado  		:= :new.peso_rolo_pre_estampado;
v_fator_cobertura            := :new.fator_cobertura;          		    v_largura_pre_estampado    		:= :new.largura_pre_estampado;
v_grau_compactacao           := :new.grau_compactacao;         		    v_comprim_pre_estampado    		:= :new.comprim_pre_estampado;
v_kit_elastano               := :new.kit_elastano;             		    v_perdas_pre_estampado     		:= :new.perdas_pre_estampado;
v_mercerizado                := :new.mercerizado;              		    v_limite_falhas_rolo       		:= :new.limite_falhas_rolo;
v_nr_alimentadores           := :new.nr_alimentadores;         		    v_limite_falhas_mini       		:= :new.limite_falhas_mini;
v_numero_agulhas             := :new.numero_agulhas;           		    v_tato                     		:= :new.tato;
v_perc_composicao1           := :new.perc_composicao1;         		    v_referencia_origem        		:= :new.referencia_origem;
v_perc_composicao2           := :new.perc_composicao2;         		    v_consumo_pasta_normal     		:= :new.consumo_pasta_normal;
v_perc_composicao3           := :new.perc_composicao3;         		    v_largura_cilindro         		:= :new.largura_cilindro;
v_perc_composicao4           := :new.perc_composicao4;         		    v_largura_estampa          		:= :new.largura_estampa;
v_perc_composicao5           := :new.perc_composicao5;         		    v_compos_ing5              		:= :new.compos_ing5;
v_perc_perdas                := :new.perc_perdas;              		    v_compos_esp5              		:= :new.compos_esp5;
v_perc_umidade               := :new.perc_umidade;             		    v_compos_comer05           		:= :new.compos_comer05;
v_relach_comprimen           := :new.relach_comprimen;         		    v_cod_fluxo                		:= :new.cod_fluxo;
v_relach_largura             := :new.relach_largura;           		    v_perdas_antes_tingimento  		:= :new.perdas_antes_tingimento;
v_rendimento                 := :new.rendimento;               		    v_peso_multiplo            		:= :new.peso_multiplo;
v_temperatura_estp           := :new.temperatura_estp;         		    v_tem_dep_rolo             		:= :new.tem_dep_rolo;
v_temperatura_fixa           := :new.temperatura_fixa;         		    v_tem_dep_mini_rolo        		:= :new.tem_dep_mini_rolo;
v_tempo_fixacao              := :new.tempo_fixacao;            		    v_tem_dep_estamparia       		:= :new.tem_dep_estamparia;
v_tempo_termofixar           := :new.tempo_termofixar;         		    v_composicao_06            		:= :new.composicao_06;
v_termofixar                 := :new.termofixar;               		    v_perc_composicao6         		:= :new.perc_composicao6;
v_titulo_fio                 := :new.titulo_fio;               		    v_diametro                 		:= :new.diametro;
v_torcao_acabado             := :new.torcao_acabado;           		    v_grupo_encolhimento       		:= :new.grupo_encolhimento;
v_torcao_cru                 := :new.torcao_cru;              		    v_homologacao_fornec       		:= :new.homologacao_fornec;
v_tubular_aberto             := :new.tubular_aberto;          		    v_litros_absorcao          		:= :new.litros_absorcao;
v_variacao_fio               := :new.variacao_fio;             		    v_nr_rolos_cubagem         		:= :new.nr_rolos_cubagem;
v_fator                      := :new.fator;                    		    v_descr_marca_reg          		:= :new.descr_marca_reg;
v_ind_mt_minutos             := :new.ind_mt_minutos;           		    v_codigo_embalagem         		:= :new.codigo_embalagem;
v_ind_kg_maq_dia             := :new.ind_kg_maq_dia;           		    v_desc_tam_ficha           		:= :new.desc_tam_ficha;
v_ind_eficiencia             := :new.ind_eficiencia;           		    v_comprimento_mini         		:= :new.comprimento_mini;
v_ne_titulo                  := :new.ne_titulo;                		    v_caract                   		:= :new.caract;
v_processo_unico_tec         := :new.processo_unico_tec;      		    v_caract_ing               		:= :new.caract_ing;
v_gramatura_vaporizado       := :new.gramatura_vaporizado;     		    v_caract_esp               		:= :new.caract_esp;
v_cod_processo               := :new.cod_processo;             		    v_perc_difer_peso          		:= :new.perc_difer_peso;
v_grade_distribuicao         := :new.grade_distribuicao;       		    v_sequencia_tamanho        		:= :new.sequencia_tamanho;
v_largura_saida_maquina      := :new.largura_saida_maquina;    		    v_raport                   		:= :new.raport;
v_gramatura_saida_maquina    := :new.gramatura_saida_maquina;  		    v_num_desenvolvimento      		:= :new.num_desenvolvimento;
v_cursos_saida_maquina       := :new.cursos_saida_maquina;     		    v_alternativa_padrao       		:= :new.alternativa_padrao;
v_colunas_saida_maquina      := :new.colunas_saida_maquina;    		    v_roteiro_padrao           		:= :new.roteiro_padrao;
v_largura_apos_repouso       := :new.largura_apos_repouso;     		    v_peso_quebra              		:= :new.peso_quebra;
v_gramatura_apos_repouso     := :new.gramatura_apos_repouso;   		    v_grupo_tecido_costurar    		:= :new.grupo_tecido_costurar;
v_cursos_apos_repouso        := :new.cursos_apos_repouso;     		    v_batidas_polegadas        		:= :new.batidas_polegadas;
v_colunas_apos_repouso       := :new.colunas_apos_repouso;     		    v_ordem_compra             		:= :new.ordem_compra;
v_perc_variacao_gramatura    := :new.perc_variacao_gramatura;  		    v_tipo_produto_global      		:= :new.tipo_produto_global;
v_tam_ponto_1                := :new.tam_ponto_1;              		    v_batidas_metro            		:= :new.batidas_metro;
v_tam_ponto_2                := :new.tam_ponto_2;              		    v_codigo_pente             		:= :new.codigo_pente;
v_tam_ponto_3                := :new.tam_ponto_3;              		    v_densidade_pente          		:= :new.densidade_pente;
v_tam_ponto_4                := :new.tam_ponto_4;              		    v_qtde_fios                		:= :new.qtde_fios;
v_ne_titulo_2                := :new.ne_titulo_2;              		    v_qtde_fios_fundo          		:= :new.qtde_fios_fundo;
v_ne_titulo_3                := :new.ne_titulo_3;             		    v_qtde_fios_ourela         		:= :new.qtde_fios_ourela;
v_ne_titulo_4                := :new.ne_titulo_4;              		    v_perc_encolhimento        		:= :new.perc_encolhimento;
v_lfa_1                      := :new.lfa_1;                    		    v_passamento_fundo         		:= :new.passamento_fundo;
v_lfa_2                      := :new.lfa_2;                    		    v_passamento_ourela        		:= :new.passamento_ourela;
v_lfa_3                      := :new.lfa_3;                    		    v_puas_fundo               		:= :new.puas_fundo;
v_lfa_4                      := :new.lfa_4;                    		    v_puas_ourela              		:= :new.puas_ourela;
v_tear_finura                := :new.tear_finura;              		    v_largura_pente            		:= :new.largura_pente;
v_tear_diametro              := :new.tear_diametro;            		    v_contracao_urdume         		:= :new.contracao_urdume;
v_perc_alt_dim_comp          := :new.perc_alt_dim_comp;        		    v_contracao_trama          		:= :new.contracao_trama;
v_perc_alt_dim_larg          := :new.perc_alt_dim_larg;        		    v_oz_yd2                   		:= :new.oz_yd2;
v_largura_proj               := :new.largura_proj;             		    v_cor_etiqueta             		:= :new.cor_etiqueta;
v_gramatura_proj             := :new.gramatura_proj;           		    v_linha_producao           		:= :new.linha_producao;
v_larg_var_ini               := :new.larg_var_ini;             		    v_titulo_fibra             		:= :new.titulo_fibra;
v_larg_var_fim               := :new.larg_var_fim;             		    v_um_titulo_fibra          		:= :new.um_titulo_fibra;
v_gar_var_ini                := :new.gar_var_ini;              		    v_simbolo_1                		:= :new.simbolo_1;
v_gar_var_fim                := :new.gar_var_fim;              		    v_simbolo_2                		:= :new.simbolo_2;
v_cursos_var_ini             := :new.cursos_var_ini;           		    v_simbolo_3                		:= :new.simbolo_3;
v_cursos_var_fim             := :new.cursos_var_fim;           		    v_simbolo_4                		:= :new.simbolo_4;
v_colunas_var_ini            := :new.colunas_var_ini;          		    v_simbolo_5                		:= :new.simbolo_5;
v_colunas_var_fim            := :new.colunas_var_fim;          		    v_cod_toler_rendim         		:= :new.cod_toler_rendim;
v_torcao_aca_var_ini         := :new.torcao_aca_var_ini;       		    v_solidez_agua_mar         		:= :new.solidez_agua_mar;
v_torcao_aca_var_fim         := :new.torcao_aca_var_fim;      		    v_sol_agua_mar_ini         		:= :new.sol_agua_mar_ini;
v_tipo_retorno               := :new.tipo_retorno;             		    v_perc_retracao            		:= :new.perc_retracao;
v_perc_var_largura           := :new.perc_var_largura;         		    v_altura_1                 		:= :new.altura_1;
v_perc_var_compr             := :new.perc_var_compr;           		    v_altura_2                 		:= :new.altura_2;
v_ne_titulo_1                := :new.ne_titulo_1;              		    v_cursos_centimetros       		:= :new.cursos_centimetros;
v_alongamento                := :new.alongamento;              		    v_tamanho_retilinea        		:= :new.tamanho_retilinea;
v_grupo_agrupador            := :new.grupo_agrupador;          		    v_dias_armazenamento       		:= :new.dias_armazenamento;
v_sub_agrupador              := :new.sub_agrupador;            		    v_peso_medio_cone          		:= :new.peso_medio_cone;
v_tipo_homologacao           := :new.tipo_homologacao;         		    v_un_med_titulo_fio        		:= :new.un_med_titulo_fio;
v_nro_etiqs_homologacao      := :new.nro_etiqs_homologacao;    		    v_grupo_tecido_cortar      		:= :new.grupo_tecido_cortar;
v_solidez_lavacao            := :new.solidez_lavacao;         		    v_permite_misturar_maquina 		:= :new.permite_misturar_maquina;
v_sol_lavacao_ini            := :new.sol_lavacao_ini;          		    v_colunas_ercru            		:= :new.colunas_ercru;
v_sol_lavacao_fim            := :new.sol_lavacao_fim;          		    v_cursos_ercru             		:= :new.cursos_ercru;
v_solidez_suor               := :new.solidez_suor;             		    v_gramatura_ercru          		:= :new.gramatura_ercru;
v_sol_suor_ini               := :new.sol_suor_ini;             		    v_largura_ercru            		:= :new.largura_ercru;
v_sol_suor_fim               := :new.sol_suor_fim;             		    v_largura_pf_1             		:= :new.largura_pf_1;
v_solidez_agua_clorada       := :new.solidez_agua_clorada;     		    v_largura_pf_2             		:= :new.largura_pf_2;
v_sol_agua_clorada_ini       := :new.sol_agua_clorada_ini;     		    v_altura_pf_1              		:= :new.altura_pf_1;
v_sol_agua_clorada_fim       := :new.sol_agua_clorada_fim;     		    v_altura_pf_2              		:= :new.altura_pf_2;
v_sol_agua_mar_fim           := :new.sol_agua_mar_fim;         		    v_compri_plotagem  			:= :new.compri_plotagem;
v_solidez_friccao_seco       := :new.solidez_friccao_seco;


if inserting or updating
then
   if inserting
   then v_tipo_operacao := 'I';
   else v_tipo_operacao := 'A';
   end if;

   if v_basi030_nivel030  = '2'  and
       ((v_tipo_operacao  = 'I') or
        ( v_tipo_operacao = 'A'  and
          (:old.basi030_referenc <> :new.basi030_referenc or
           :old.tamanho_ref      <> :new.tamanho_ref      or
           :old.descr_tam_refer  <> :new.descr_tam_refer  or
           :old.tipo_produto     <> :new.tipo_produto     or
           :old.grupo_agrupador  <> :new.grupo_agrupador  or
           :old.sub_agrupador    <> :new.sub_agrupador    or
           :old.desc_tam_ficha   <> :new.desc_tam_ficha   or
           :old.gramatura_1      <> :new.gramatura_1      or
           :old.peso_rolo        <> :new.peso_rolo        or
           :old.composicao_01    <> :new.composicao_01    or
           :old.composicao_02    <> :new.composicao_02    or
           :old.composicao_03    <> :new.composicao_03    or
           :old.composicao_04    <> :new.composicao_04    or
           :old.composicao_05    <> :new.composicao_05    or
           :old.perc_composicao1 <> :new.perc_composicao1 or
           :old.perc_composicao2 <> :new.perc_composicao2 or
           :old.perc_composicao3 <> :new.perc_composicao3 or
           :old.perc_composicao4 <> :new.perc_composicao4 or
           :old.perc_composicao5 <> :new.perc_composicao5 or
           :old.rendimento       <> :new.rendimento       or
           :old.largura_1        <> :new.largura_1        or
           :old.largura_2        <> :new.largura_2        or
           :old.cod_processo     <> :new.cod_processo     or
           :old.tubular_aberto   <> :new.tubular_aberto)))
      then

      begin

      insert into e_basi_020
      (                     	     sol_friccao_seco_ini,
       basi030_nivel030,             sol_friccao_seco_fim,
       basi030_referenc,             solidez_friccao_umido,
       tamanho_ref,                  sol_friccao_umido_ini,
       descr_tam_refer,              sol_friccao_umido_fim,
       tipo_produto,                 metros_lineares,
       artigo_cotas,                 metros_cubicos,
       largura_1,                    agulhas_falhadas,
       gramatura_1,                  num_agulhas_falhadas,
       peso_rolo,                    numero_pontos,
       data_importacao,              polegadas_agulha,
                        	     peso_mini_rolo,
      	   	                     compos_comer01,
       lote_fabr_pecas,              compos_comer02,
       peso_liquido,                 compos_comer03,
       qtde_max_enfesto,             compos_comer04,
       comp_max_enfesto,             descr_ing,
       composicao_01,                descr_ing2,
       composicao_02,                descr_esp,
       composicao_03,                compos_ing1,
       composicao_04,                compos_ing2,
       composicao_05,                compos_ing3,
       largura_2,                    compos_ing4,
       gramatura_2,                  compos_esp1,
       gramatura_3,                  compos_esp2,
       avaliacao_toque,              compos_esp3,
       classe_fio,                   compos_esp4,
       cursos,                       observacao,
       elasticidade_aca,             obs_ing,
       elasticidade_cru,             obs_esp,
       enco_comprimento,             gramatura_pre_estampado,
       enco_largura,                 peso_rolo_pre_estampado,
       fator_cobertura,              largura_pre_estampado,
       grau_compactacao,             comprim_pre_estampado,
       kit_elastano,                 perdas_pre_estampado,
       mercerizado,                  limite_falhas_rolo,
       nr_alimentadores,             limite_falhas_mini,
       numero_agulhas,               tato,
       perc_composicao1,             referencia_origem,
       perc_composicao2,             consumo_pasta_normal,
       perc_composicao3,             largura_cilindro,
       perc_composicao4,             largura_estampa,
       perc_composicao5,             compos_ing5,
       perc_perdas,                  compos_esp5,
       perc_umidade,                 compos_comer05,
       relach_comprimen,             cod_fluxo,
       relach_largura,               perdas_antes_tingimento,
       rendimento,                   peso_multiplo,
       temperatura_estp,             tem_dep_rolo,
       temperatura_fixa,             tem_dep_mini_rolo,
       tempo_fixacao,                tem_dep_estamparia,
       tempo_termofixar,             composicao_06,
       termofixar,                   perc_composicao6,
       titulo_fio,                   diametro,
       torcao_acabado,               grupo_encolhimento,
       torcao_cru,                   homologacao_fornec,
       tubular_aberto,               litros_absorcao,
       variacao_fio,                 nr_rolos_cubagem,
       fator,                        descr_marca_reg,
       ind_mt_minutos,               codigo_embalagem,
       ind_kg_maq_dia,               desc_tam_ficha,
       ind_eficiencia,               comprimento_mini,
       ne_titulo,                    caract,
       processo_unico_tec,           caract_ing,
       gramatura_vaporizado,         caract_esp,
       cod_processo,                 perc_difer_peso,
       grade_distribuicao,           sequencia_tamanho,
       largura_saida_maquina,        raport,
       gramatura_saida_maquina,      num_desenvolvimento,
       cursos_saida_maquina,         alternativa_padrao,
       colunas_saida_maquina,        roteiro_padrao,
       largura_apos_repouso,         peso_quebra,
       gramatura_apos_repouso,       grupo_tecido_costurar,
       cursos_apos_repouso,          batidas_polegadas,
       colunas_apos_repouso,         ordem_compra,
       perc_variacao_gramatura,      tipo_produto_global,
       tam_ponto_1,                  batidas_metro,
       tam_ponto_2,                  codigo_pente,
       tam_ponto_3,                  densidade_pente,
       tam_ponto_4,                  qtde_fios,
       ne_titulo_2,                  qtde_fios_fundo,
       ne_titulo_3,                  qtde_fios_ourela,
       ne_titulo_4,                  perc_encolhimento,
       lfa_1,                        passamento_fundo,
       lfa_2,                        passamento_ourela,
       lfa_3,                        puas_fundo,
       lfa_4,                        puas_ourela,
       tear_finura,                  largura_pente,
       tear_diametro,                contracao_urdume,
       perc_alt_dim_comp,            contracao_trama,
       perc_alt_dim_larg,            oz_yd2,
       largura_proj,                 cor_etiqueta,
       gramatura_proj,               linha_producao,
       larg_var_ini,                 titulo_fibra,
       larg_var_fim,                 um_titulo_fibra,
       gar_var_ini,                  simbolo_1,
       gar_var_fim,                  simbolo_2,
       cursos_var_ini,               simbolo_3,
       cursos_var_fim,               simbolo_4,
       colunas_var_ini,              simbolo_5,
       colunas_var_fim,              cod_toler_rendim,
       torcao_aca_var_ini,           solidez_agua_mar,
       torcao_aca_var_fim,           sol_agua_mar_ini,
       tipo_retorno,                 perc_retracao,
       perc_var_largura,             altura_1,
       perc_var_compr,               altura_2,
       ne_titulo_1,                  cursos_centimetros,
       alongamento,                  tamanho_retilinea,
       grupo_agrupador,              dias_armazenamento,
       sub_agrupador,                peso_medio_cone,
       tipo_homologacao,             un_med_titulo_fio,
       nro_etiqs_homologacao,        grupo_tecido_cortar,
       solidez_lavacao,              permite_misturar_maquina,
       sol_lavacao_ini,              colunas_ercru,
       sol_lavacao_fim,              cursos_ercru,
       solidez_suor,                 gramatura_ercru,
       sol_suor_ini,                 largura_ercru,
       sol_suor_fim,                 largura_pf_1,
       solidez_agua_clorada,         largura_pf_2,
       sol_agua_clorada_ini,         altura_pf_1,
       sol_agua_clorada_fim,         altura_pf_2,
       sol_agua_mar_fim,             compri_plotagem,
       solidez_friccao_seco,         tipo_operacao,
       usuario_operacao,             flag_integracao)
      VALUES
      (                    	     v_sol_friccao_seco_ini,
       v_basi030_nivel030,           v_sol_friccao_seco_fim,
       v_basi030_referenc,           v_solidez_friccao_umido,
       v_tamanho_ref,		     v_sol_friccao_umido_ini,
       v_descr_tam_refer,            v_sol_friccao_umido_fim,
       v_tipo_produto,               v_metros_lineares,
       v_artigo_cotas,               v_metros_cubicos,
       v_largura_1,           	     v_agulhas_falhadas,
       v_gramatura_1,                v_num_agulhas_falhadas,
       v_peso_rolo,                  v_numero_pontos,
       v_data_importacao,            v_polegadas_agulha,
                         	     v_peso_mini_rolo,
       	                             v_compos_comer01,
       v_lote_fabr_pecas,            v_compos_comer02,
       v_peso_liquido,        	     v_compos_comer03,
       v_qtde_max_enfesto,           v_compos_comer04,
       v_comp_max_enfesto,           v_descr_ing,
       v_composicao_01,              v_descr_ing2,
       v_composicao_02,              v_descr_esp,
       v_composicao_03,              v_compos_ing1,
       v_composicao_04,              v_compos_ing2,
       v_composicao_05,              v_compos_ing3,
       v_largura_2,                  v_compos_ing4,
       v_gramatura_2,                v_compos_esp1,
       v_gramatura_3,                v_compos_esp2,
       v_avaliacao_toque,            v_compos_esp3,
       v_classe_fio,          	     v_compos_esp4,
       v_cursos,               	     v_observacao,
       v_elasticidade_aca,     	     v_obs_ing,
       v_elasticidade_cru,    	     v_obs_esp,
       v_enco_comprimento,    	     v_gramatura_pre_estampado,
       v_enco_largura,         	     v_peso_rolo_pre_estampado,
       v_fator_cobertura,     	     v_largura_pre_estampado,
       v_grau_compactacao,    	     v_comprim_pre_estampado,
       v_kit_elastano,         	     v_perdas_pre_estampado,
       v_mercerizado,          	     v_limite_falhas_rolo,
       v_nr_alimentadores,     	     v_limite_falhas_mini,
       v_numero_agulhas,       	     v_tato,
       v_perc_composicao1,     	     v_referencia_origem,
       v_perc_composicao2,    	     v_consumo_pasta_normal,
       v_perc_composicao3,     	     v_largura_cilindro,
       v_perc_composicao4,     	     v_largura_estampa,
       v_perc_composicao5,     	     v_compos_ing5,
       v_perc_perdas,          	     v_compos_esp5,
       v_perc_umidade,         	     v_compos_comer05,
       v_relach_comprimen,     	     v_cod_fluxo,
       v_relach_largura,       	     v_perdas_antes_tingimento,
       v_rendimento,           	     v_peso_multiplo,
       v_temperatura_estp,     	     v_tem_dep_rolo,
       v_temperatura_fixa,     	     v_tem_dep_mini_rolo,
       v_tempo_fixacao,        	     v_tem_dep_estamparia,
       v_tempo_termofixar,     	     v_composicao_06,
       v_termofixar,           	     v_perc_composicao6,
       v_titulo_fio,           	     v_diametro,
       v_torcao_acabado,       	     v_grupo_encolhimento,
       v_torcao_cru,           	     v_homologacao_fornec,
       v_tubular_aberto,       	     v_litros_absorcao,
       v_variacao_fio,         	     v_nr_rolos_cubagem,
       v_fator,                	     v_descr_marca_reg,
       v_ind_mt_minutos,       	     v_codigo_embalagem,
       v_ind_kg_maq_dia,       	     v_desc_tam_ficha,
       v_ind_eficiencia,             v_comprimento_mini,
       v_ne_titulo,                  v_caract,
       v_processo_unico_tec,         v_caract_ing,
       v_gramatura_vaporizado, 	     v_caract_esp,
       v_cod_processo,         	     v_perc_difer_peso,
       v_grade_distribuicao,   	     v_sequencia_tamanho,
       v_largura_saida_maquina,	     v_raport,
       v_gramatura_saida_maquina,    v_num_desenvolvimento,
       v_cursos_saida_maquina,       v_alternativa_padrao,
       v_colunas_saida_maquina,      v_roteiro_padrao,
       v_largura_apos_repouso,       v_peso_quebra,
       v_gramatura_apos_repouso,     v_grupo_tecido_costurar,
       v_cursos_apos_repouso,        v_batidas_polegadas,
       v_colunas_apos_repouso,       v_ordem_compra,
       v_perc_variacao_gramatura,    v_tipo_produto_global,
       v_tam_ponto_1,                v_batidas_metro,
       v_tam_ponto_2,                v_codigo_pente,
       v_tam_ponto_3,                v_densidade_pente,
       v_tam_ponto_4,                v_qtde_fios,
       v_ne_titulo_2,                v_qtde_fios_fundo,
       v_ne_titulo_3,                v_qtde_fios_ourela,
       v_ne_titulo_4,                v_perc_encolhimento,
       v_lfa_1,                      v_passamento_fundo,
       v_lfa_2,                      v_passamento_ourela,
       v_lfa_3,                      v_puas_fundo,
       v_lfa_4,                      v_puas_ourela,
       v_tear_finura,                v_largura_pente,
       v_tear_diametro,              v_contracao_urdume,
       v_perc_alt_dim_comp,          v_contracao_trama,
       v_perc_alt_dim_larg,          v_oz_yd2,
       v_largura_proj,               v_cor_etiqueta,
       v_gramatura_proj,             v_linha_producao,
       v_larg_var_ini,               v_titulo_fibra,
       v_larg_var_fim,               v_um_titulo_fibra,
       v_gar_var_ini,                v_simbolo_1,
       v_gar_var_fim,                v_simbolo_2,
       v_cursos_var_ini,             v_simbolo_3,
       v_cursos_var_fim,             v_simbolo_4,
       v_colunas_var_ini,            v_simbolo_5,
       v_colunas_var_fim,            v_cod_toler_rendim,
       v_torcao_aca_var_ini,         v_solidez_agua_mar,
       v_torcao_aca_var_fim,         v_sol_agua_mar_ini,
       v_tipo_retorno,               v_perc_retracao,
       v_perc_var_largura,           v_altura_1,
       v_perc_var_compr,             v_altura_2,
       v_ne_titulo_1,                v_cursos_centimetros,
       v_alongamento,                v_tamanho_retilinea,
       v_grupo_agrupador,            v_dias_armazenamento,
       v_sub_agrupador,              v_peso_medio_cone,
       v_tipo_homologacao,           v_un_med_titulo_fio,
       v_nro_etiqs_homologacao,      v_grupo_tecido_cortar,
       v_solidez_lavacao,            v_permite_misturar_maquina,
       v_sol_lavacao_ini,            v_colunas_ercru,
       v_sol_lavacao_fim,            v_cursos_ercru,
       v_solidez_suor,               v_gramatura_ercru,
       v_sol_suor_ini,               v_largura_ercru,
       v_sol_suor_fim,               v_largura_pf_1,
       v_solidez_agua_clorada,       v_largura_pf_2,
       v_sol_agua_clorada_ini,       v_altura_pf_1,
       v_sol_agua_clorada_fim,       v_altura_pf_2,
       v_sol_agua_mar_fim,           v_compri_plotagem,
       v_solidez_friccao_seco,	     v_tipo_operacao,
       ws_usuario_systextil,         1);

    exception
    when OTHERS
       then raise_application_error (-20000, 'N�o atualizou a tabela e_basi_020' || Chr(10) || SQLERRM);
    end;


    if v_tipo_operacao       = 'A'                   and
       :old.basi030_nivel030 <> :new.basi030_nivel030 or
       :old.basi030_referenc <> :new.basi030_referenc or
       :old.tamanho_ref      <> :new.tamanho_ref
    then


       v_tipo_operacao := 'E';

                                                                 		    v_sol_friccao_seco_ini      	:= :old.sol_friccao_seco_ini;
       v_basi030_nivel030           := :old.basi030_nivel030;         		    v_sol_friccao_seco_fim      	:= :old.sol_friccao_seco_fim;
       v_basi030_referenc           := :old.basi030_referenc;         		    v_solidez_friccao_umido     	:= :old.solidez_friccao_umido;
       v_tamanho_ref                := :old.tamanho_ref;              		    v_sol_friccao_umido_ini     	:= :old.sol_friccao_umido_ini;
       v_descr_tam_refer            := :old.descr_tam_refer;          		    v_sol_friccao_umido_fim     	:= :old.sol_friccao_umido_fim;
       v_tipo_produto               := :old.tipo_produto;             		    v_metros_lineares           	:= :old.metros_lineares;
       v_artigo_cotas               := :old.artigo_cotas;             		    v_metros_cubicos            	:= :old.metros_cubicos;
       v_largura_1                  := :old.largura_1;                		    v_agulhas_falhadas          	:= :old.agulhas_falhadas;
       v_gramatura_1                := :old.gramatura_1;              		    v_num_agulhas_falhadas      	:= :old.num_agulhas_falhadas;
       v_peso_rolo                  := :old.peso_rolo;                		    v_numero_pontos             	:= :old.numero_pontos;
       v_data_importacao            := sysdate();      		                    v_polegadas_agulha          	:= :old.polegadas_agulha;
                                                                   		    v_peso_mini_rolo            	:= :old.peso_mini_rolo;
  	   		                                                            v_compos_comer01           		:= :old.compos_comer01;
       v_lote_fabr_pecas            := :old.lote_fabr_pecas;          		    v_compos_comer02           		:= :old.compos_comer02;
       v_peso_liquido               := :old.peso_liquido;     		            v_compos_comer03           		:= :old.compos_comer03;
       v_qtde_max_enfesto           := :old.qtde_max_enfesto;         		    v_compos_comer04           		:= :old.compos_comer04;
       v_comp_max_enfesto           := :old.comp_max_enfesto;         		    v_descr_ing                		:= :old.descr_ing;
       v_composicao_01              := :old.composicao_01;            		    v_descr_ing2               		:= :old.descr_ing2;
       v_composicao_02              := :old.composicao_02;            		    v_descr_esp                		:= :old.descr_esp;
       v_composicao_03              := :old.composicao_03;            		    v_compos_ing1              		:= :old.compos_ing1;
       v_composicao_04              := :old.composicao_04;           		    v_compos_ing2              		:= :old.compos_ing2;
       v_composicao_05              := :old.composicao_05;            		    v_compos_ing3              		:= :old.compos_ing3;
       v_largura_2                  := :old.largura_2;                		    v_compos_ing4              		:= :old.compos_ing4;
       v_gramatura_2                := :old.gramatura_2;              		    v_compos_esp1              		:= :old.compos_esp1;
       v_gramatura_3                := :old.gramatura_3;              		    v_compos_esp2              		:= :old.compos_esp2;
       v_avaliacao_toque            := :old.avaliacao_toque;         		    v_compos_esp3              		:= :old.compos_esp3;
       v_classe_fio                 := :old.classe_fio;               		    v_compos_esp4              		:= :old.compos_esp4;
       v_cursos                     := :old.cursos;                   		    v_observacao               		:= :old.observacao;
       v_elasticidade_aca           := :old.elasticidade_aca;         		    v_obs_ing                  		:= :old.obs_ing;
       v_elasticidade_cru           := :old.elasticidade_cru;         		    v_obs_esp                  		:= :old.obs_esp;
       v_enco_comprimento           := :old.enco_comprimento;         		    v_gramatura_pre_estampado  		:= :old.gramatura_pre_estampado;
       v_enco_largura               := :old.enco_largura;             		    v_peso_rolo_pre_estampado  		:= :old.peso_rolo_pre_estampado;
       v_fator_cobertura            := :old.fator_cobertura;          		    v_largura_pre_estampado    		:= :old.largura_pre_estampado;
       v_grau_compactacao           := :old.grau_compactacao;         		    v_comprim_pre_estampado    		:= :old.comprim_pre_estampado;
       v_kit_elastano               := :old.kit_elastano;             		    v_perdas_pre_estampado     		:= :old.perdas_pre_estampado;
       v_mercerizado                := :old.mercerizado;              		    v_limite_falhas_rolo       		:= :old.limite_falhas_rolo;
       v_nr_alimentadores           := :old.nr_alimentadores;         		    v_limite_falhas_mini       		:= :old.limite_falhas_mini;
       v_numero_agulhas             := :old.numero_agulhas;           		    v_tato                     		:= :old.tato;
       v_perc_composicao1           := :old.perc_composicao1;         		    v_referencia_origem        		:= :old.referencia_origem;
       v_perc_composicao2           := :old.perc_composicao2;         		    v_consumo_pasta_normal     		:= :old.consumo_pasta_normal;
       v_perc_composicao3           := :old.perc_composicao3;         		    v_largura_cilindro         		:= :old.largura_cilindro;
       v_perc_composicao4           := :old.perc_composicao4;         		    v_largura_estampa          		:= :old.largura_estampa;
       v_perc_composicao5           := :old.perc_composicao5;         		    v_compos_ing5              		:= :old.compos_ing5;
       v_perc_perdas                := :old.perc_perdas;              		    v_compos_esp5              		:= :old.compos_esp5;
       v_perc_umidade               := :old.perc_umidade;             		    v_compos_comer05           		:= :old.compos_comer05;
       v_relach_comprimen           := :old.relach_comprimen;         		    v_cod_fluxo                		:= :old.cod_fluxo;
       v_relach_largura             := :old.relach_largura;           		    v_perdas_antes_tingimento  		:= :old.perdas_antes_tingimento;
       v_rendimento                 := :old.rendimento;               		    v_peso_multiplo            		:= :old.peso_multiplo;
       v_temperatura_estp           := :old.temperatura_estp;         		    v_tem_dep_rolo             		:= :old.tem_dep_rolo;
       v_temperatura_fixa           := :old.temperatura_fixa;         		    v_tem_dep_mini_rolo        		:= :old.tem_dep_mini_rolo;
       v_tempo_fixacao              := :old.tempo_fixacao;            		    v_tem_dep_estamparia       		:= :old.tem_dep_estamparia;
       v_tempo_termofixar           := :old.tempo_termofixar;         		    v_composicao_06            		:= :old.composicao_06;
       v_termofixar                 := :old.termofixar;               		    v_perc_composicao6         		:= :old.perc_composicao6;
       v_titulo_fio                 := :old.titulo_fio;               		    v_diametro                 		:= :old.diametro;
       v_torcao_acabado             := :old.torcao_acabado;           		    v_grupo_encolhimento       		:= :old.grupo_encolhimento;
       v_torcao_cru                 := :old.torcao_cru;              		    v_homologacao_fornec       		:= :old.homologacao_fornec;
       v_tubular_aberto             := :old.tubular_aberto;          		    v_litros_absorcao          		:= :old.litros_absorcao;
       v_variacao_fio               := :old.variacao_fio;             		    v_nr_rolos_cubagem         		:= :old.nr_rolos_cubagem;
       v_fator                      := :old.fator;                    		    v_descr_marca_reg          		:= :old.descr_marca_reg;
       v_ind_mt_minutos             := :old.ind_mt_minutos;           		    v_codigo_embalagem         		:= :old.codigo_embalagem;
       v_ind_kg_maq_dia             := :old.ind_kg_maq_dia;           		    v_desc_tam_ficha           		:= :old.desc_tam_ficha;
       v_ind_eficiencia             := :old.ind_eficiencia;           		    v_comprimento_mini         		:= :old.comprimento_mini;
       v_ne_titulo                  := :old.ne_titulo;                		    v_caract                   		:= :old.caract;
       v_processo_unico_tec         := :old.processo_unico_tec;      		    v_caract_ing               		:= :old.caract_ing;
       v_gramatura_vaporizado       := :old.gramatura_vaporizado;     		    v_caract_esp               		:= :old.caract_esp;
       v_cod_processo               := :old.cod_processo;             		    v_perc_difer_peso          		:= :old.perc_difer_peso;
       v_grade_distribuicao         := :old.grade_distribuicao;       		    v_sequencia_tamanho        		:= :old.sequencia_tamanho;
       v_largura_saida_maquina      := :old.largura_saida_maquina;    		    v_raport                   		:= :old.raport;
       v_gramatura_saida_maquina    := :old.gramatura_saida_maquina;  		    v_num_desenvolvimento      		:= :old.num_desenvolvimento;
       v_cursos_saida_maquina       := :old.cursos_saida_maquina;     		    v_alternativa_padrao       		:= :old.alternativa_padrao;
       v_colunas_saida_maquina      := :old.colunas_saida_maquina;    		    v_roteiro_padrao           		:= :old.roteiro_padrao;
       v_largura_apos_repouso       := :old.largura_apos_repouso;     		    v_peso_quebra              		:= :old.peso_quebra;
       v_gramatura_apos_repouso     := :old.gramatura_apos_repouso;   		    v_grupo_tecido_costurar    		:= :old.grupo_tecido_costurar;
       v_cursos_apos_repouso        := :old.cursos_apos_repouso;     		    v_batidas_polegadas        		:= :old.batidas_polegadas;
       v_colunas_apos_repouso       := :old.colunas_apos_repouso;     		    v_ordem_compra             		:= :old.ordem_compra;
       v_perc_variacao_gramatura    := :old.perc_variacao_gramatura;  		    v_tipo_produto_global      		:= :old.tipo_produto_global;
       v_tam_ponto_1                := :old.tam_ponto_1;              		    v_batidas_metro            		:= :old.batidas_metro;
       v_tam_ponto_2                := :old.tam_ponto_2;              		    v_codigo_pente             		:= :old.codigo_pente;
       v_tam_ponto_3                := :old.tam_ponto_3;              		    v_densidade_pente          		:= :old.densidade_pente;
       v_tam_ponto_4                := :old.tam_ponto_4;              		    v_qtde_fios                		:= :old.qtde_fios;
       v_ne_titulo_2                := :old.ne_titulo_2;              		    v_qtde_fios_fundo          		:= :old.qtde_fios_fundo;
       v_ne_titulo_3                := :old.ne_titulo_3;             		    v_qtde_fios_ourela         		:= :old.qtde_fios_ourela;
       v_ne_titulo_4                := :old.ne_titulo_4;              		    v_perc_encolhimento        		:= :old.perc_encolhimento;
       v_lfa_1                      := :old.lfa_1;                    		    v_passamento_fundo         		:= :old.passamento_fundo;
       v_lfa_2                      := :old.lfa_2;                    		    v_passamento_ourela        		:= :old.passamento_ourela;
       v_lfa_3                      := :old.lfa_3;                    		    v_puas_fundo               		:= :old.puas_fundo;
       v_lfa_4                      := :old.lfa_4;                    		    v_puas_ourela              		:= :old.puas_ourela;
       v_tear_finura                := :old.tear_finura;              		    v_largura_pente            		:= :old.largura_pente;
       v_tear_diametro              := :old.tear_diametro;            		    v_contracao_urdume         		:= :old.contracao_urdume;
       v_perc_alt_dim_comp          := :old.perc_alt_dim_comp;        		    v_contracao_trama          		:= :old.contracao_trama;
       v_perc_alt_dim_larg          := :old.perc_alt_dim_larg;        		    v_oz_yd2                   		:= :old.oz_yd2;
       v_largura_proj               := :old.largura_proj;             		    v_cor_etiqueta             		:= :old.cor_etiqueta;
       v_gramatura_proj             := :old.gramatura_proj;           		    v_linha_producao           		:= :old.linha_producao;
       v_larg_var_ini               := :old.larg_var_ini;             		    v_titulo_fibra             		:= :old.titulo_fibra;
       v_larg_var_fim               := :old.larg_var_fim;             		    v_um_titulo_fibra          		:= :old.um_titulo_fibra;
       v_gar_var_ini                := :old.gar_var_ini;              		    v_simbolo_1                		:= :old.simbolo_1;
       v_gar_var_fim                := :old.gar_var_fim;              		    v_simbolo_2                		:= :old.simbolo_2;
       v_cursos_var_ini             := :old.cursos_var_ini;           		    v_simbolo_3                		:= :old.simbolo_3;
       v_cursos_var_fim             := :old.cursos_var_fim;           		    v_simbolo_4                		:= :old.simbolo_4;
       v_colunas_var_ini            := :old.colunas_var_ini;          		    v_simbolo_5                		:= :old.simbolo_5;
       v_colunas_var_fim            := :old.colunas_var_fim;          		    v_cod_toler_rendim         		:= :old.cod_toler_rendim;
       v_torcao_aca_var_ini         := :old.torcao_aca_var_ini;       		    v_solidez_agua_mar         		:= :old.solidez_agua_mar;
       v_torcao_aca_var_fim         := :old.torcao_aca_var_fim;      		    v_sol_agua_mar_ini         		:= :old.sol_agua_mar_ini;
       v_tipo_retorno               := :old.tipo_retorno;             		    v_perc_retracao            		:= :old.perc_retracao;
       v_perc_var_largura           := :old.perc_var_largura;         		    v_altura_1                 		:= :old.altura_1;
       v_perc_var_compr             := :old.perc_var_compr;           		    v_altura_2                 		:= :old.altura_2;
       v_ne_titulo_1                := :old.ne_titulo_1;              		    v_cursos_centimetros       		:= :old.cursos_centimetros;
       v_alongamento                := :old.alongamento;              		    v_tamanho_retilinea        		:= :old.tamanho_retilinea;
       v_grupo_agrupador            := :old.grupo_agrupador;          		    v_dias_armazenamento       		:= :old.dias_armazenamento;
       v_sub_agrupador              := :old.sub_agrupador;            		    v_peso_medio_cone          		:= :old.peso_medio_cone;
       v_tipo_homologacao           := :old.tipo_homologacao;         		    v_un_med_titulo_fio        		:= :old.un_med_titulo_fio;
       v_nro_etiqs_homologacao      := :old.nro_etiqs_homologacao;    		    v_grupo_tecido_cortar      		:= :old.grupo_tecido_cortar;
       v_solidez_lavacao            := :old.solidez_lavacao;         		    v_permite_misturar_maquina 		:= :old.permite_misturar_maquina;
       v_sol_lavacao_ini            := :old.sol_lavacao_ini;          		    v_colunas_ercru            		:= :old.colunas_ercru;
       v_sol_lavacao_fim            := :old.sol_lavacao_fim;          		    v_cursos_ercru             		:= :old.cursos_ercru;
       v_solidez_suor               := :old.solidez_suor;             		    v_gramatura_ercru          		:= :old.gramatura_ercru;
       v_sol_suor_ini               := :old.sol_suor_ini;             		    v_largura_ercru            		:= :old.largura_ercru;
       v_sol_suor_fim               := :old.sol_suor_fim;             		    v_largura_pf_1             		:= :old.largura_pf_1;
       v_solidez_agua_clorada       := :old.solidez_agua_clorada;     		    v_largura_pf_2             		:= :old.largura_pf_2;
       v_sol_agua_clorada_ini       := :old.sol_agua_clorada_ini;     		    v_altura_pf_1              		:= :old.altura_pf_1;
       v_sol_agua_clorada_fim       := :old.sol_agua_clorada_fim;     		    v_altura_pf_2              		:= :old.altura_pf_2;
       v_sol_agua_mar_fim           := :old.sol_agua_mar_fim;         		    v_compri_plotagem  			:= :old.compri_plotagem;
       v_solidez_friccao_seco       := :old.solidez_friccao_seco;

    begin

       insert into e_basi_020
       (                           	     sol_friccao_seco_ini,
        basi030_nivel030,         	     sol_friccao_seco_fim,
        basi030_referenc,         	     solidez_friccao_umido,
        tamanho_ref,              	     sol_friccao_umido_ini,
        descr_tam_refer,          	     sol_friccao_umido_fim,
        tipo_produto,            	     metros_lineares,
        artigo_cotas,             	     metros_cubicos,
        largura_1,                	     agulhas_falhadas,
        gramatura_1,              	     num_agulhas_falhadas,
        peso_rolo,               	     numero_pontos,
        data_importacao,          	     polegadas_agulha,
                                    	     peso_mini_rolo,
          	   	                     compos_comer01,
        lote_fabr_pecas,         	     compos_comer02,
        peso_liquido,             	     compos_comer03,
        qtde_max_enfesto,         	     compos_comer04,
        comp_max_enfesto,         	     descr_ing,
        composicao_01,            	     descr_ing2,
        composicao_02,            	     descr_esp,
        composicao_03,            	     compos_ing1,
        composicao_04,            	     compos_ing2,
        composicao_05,            	     compos_ing3,
        largura_2,                	     compos_ing4,
        gramatura_2,              	     compos_esp1,
        gramatura_3,              	     compos_esp2,
        avaliacao_toque,          	     compos_esp3,
        classe_fio,               	     compos_esp4,
        cursos,                   	     observacao,
        elasticidade_aca,        	     obs_ing,
        elasticidade_cru,         	     obs_esp,
        enco_comprimento,         	     gramatura_pre_estampado,
        enco_largura,             	     peso_rolo_pre_estampado,
        fator_cobertura,          	     largura_pre_estampado,
        grau_compactacao,         	     comprim_pre_estampado,
        kit_elastano,             	     perdas_pre_estampado,
        mercerizado,              	     limite_falhas_rolo,
        nr_alimentadores,         	     limite_falhas_mini,
        numero_agulhas,           	     tato,
        perc_composicao1,         	     referencia_origem,
        perc_composicao2,         	     consumo_pasta_normal,
        perc_composicao3,        	     largura_cilindro,
        perc_composicao4,         	     largura_estampa,
        perc_composicao5,         	     compos_ing5,
        perc_perdas,              	     compos_esp5,
        perc_umidade,            	     compos_comer05,
        relach_comprimen,         	     cod_fluxo,
        relach_largura,           	     perdas_antes_tingimento,
        rendimento,               	     peso_multiplo,
        temperatura_estp,         	     tem_dep_rolo,
        temperatura_fixa,         	     tem_dep_mini_rolo,
        tempo_fixacao,            	     tem_dep_estamparia,
        tempo_termofixar,         	     composicao_06,
        termofixar,               	     perc_composicao6,
        titulo_fio,               	     diametro,
        torcao_acabado,           	     grupo_encolhimento,
        torcao_cru,               	     homologacao_fornec,
        tubular_aberto,           	     litros_absorcao,
        variacao_fio,             	     nr_rolos_cubagem,
        fator,                    	     descr_marca_reg,
        ind_mt_minutos,           	     codigo_embalagem,
        ind_kg_maq_dia,           	     desc_tam_ficha,
        ind_eficiencia,           	     comprimento_mini,
        ne_titulo,                	     caract,
        processo_unico_tec,       	     caract_ing,
        gramatura_vaporizado,     	     caract_esp,
        cod_processo,             	     perc_difer_peso,
        grade_distribuicao,       	     sequencia_tamanho,
        largura_saida_maquina,    	     raport,
        gramatura_saida_maquina,  	     num_desenvolvimento,
        cursos_saida_maquina,     	     alternativa_padrao,
        colunas_saida_maquina,    	     roteiro_padrao,
        largura_apos_repouso,     	     peso_quebra,
        gramatura_apos_repouso,   	     grupo_tecido_costurar,
        cursos_apos_repouso,      	     batidas_polegadas,
        colunas_apos_repouso,     	     ordem_compra,
        perc_variacao_gramatura,  	     tipo_produto_global,
        tam_ponto_1,              	     batidas_metro,
        tam_ponto_2,              	     codigo_pente,
        tam_ponto_3,              	     densidade_pente,
        tam_ponto_4,              	     qtde_fios,
        ne_titulo_2,              	     qtde_fios_fundo,
        ne_titulo_3,              	     qtde_fios_ourela,
        ne_titulo_4,              	     perc_encolhimento,
        lfa_1,                    	     passamento_fundo,
        lfa_2,                    	     passamento_ourela,
        lfa_3,                    	     puas_fundo,
        lfa_4,                    	     puas_ourela,
        tear_finura,              	     largura_pente,
        tear_diametro,            	     contracao_urdume,
        perc_alt_dim_comp,       	     contracao_trama,
        perc_alt_dim_larg,        	     oz_yd2,
        largura_proj,             	     cor_etiqueta,
        gramatura_proj,          	     linha_producao,
        larg_var_ini,             	     titulo_fibra,
        larg_var_fim,             	     um_titulo_fibra,
        gar_var_ini,              	     simbolo_1,
        gar_var_fim,              	     simbolo_2,
        cursos_var_ini,           	     simbolo_3,
        cursos_var_fim,           	     simbolo_4,
        colunas_var_ini,          	     simbolo_5,
        colunas_var_fim,          	     cod_toler_rendim,
        torcao_aca_var_ini,       	     solidez_agua_mar,
        torcao_aca_var_fim,       	     sol_agua_mar_ini,
        tipo_retorno,             	     perc_retracao,
        perc_var_largura,         	     altura_1,
        perc_var_compr,           	     altura_2,
        ne_titulo_1,              	     cursos_centimetros,
        alongamento,             	     tamanho_retilinea,
        grupo_agrupador,          	     dias_armazenamento,
        sub_agrupador,            	     peso_medio_cone,
        tipo_homologacao,         	     un_med_titulo_fio,
        nro_etiqs_homologacao,    	     grupo_tecido_cortar,
        solidez_lavacao,          	     permite_misturar_maquina,
        sol_lavacao_ini,          	     colunas_ercru,
        sol_lavacao_fim,          	     cursos_ercru,
        solidez_suor,            	     gramatura_ercru,
        sol_suor_ini,             	     largura_ercru,
        sol_suor_fim,             	     largura_pf_1,
        solidez_agua_clorada,     	     largura_pf_2,
        sol_agua_clorada_ini,     	     altura_pf_1,
        sol_agua_clorada_fim,     	     altura_pf_2,
        sol_agua_mar_fim,         	     compri_plotagem,
        solidez_friccao_seco,     	     tipo_operacao,
        usuario_operacao,                    flag_integracao)
       VALUES
       (                     	             v_sol_friccao_seco_ini,
        v_basi030_nivel030,       	     v_sol_friccao_seco_fim,
        v_basi030_referenc,       	     v_solidez_friccao_umido,
        v_tamanho_ref,		             v_sol_friccao_umido_ini,
        v_descr_tam_refer,        	     v_sol_friccao_umido_fim,
        v_tipo_produto,           	     v_metros_lineares,
        v_artigo_cotas,           	     v_metros_cubicos,
        v_largura_1,           	             v_agulhas_falhadas,
        v_gramatura_1,            	     v_num_agulhas_falhadas,
        v_peso_rolo,              	     v_numero_pontos,
        v_data_importacao,        	     v_polegadas_agulha,
                                 	     v_peso_mini_rolo,
          	                             v_compos_comer01,
        v_lote_fabr_pecas,        	     v_compos_comer02,
        v_peso_liquido,        	             v_compos_comer03,
        v_qtde_max_enfesto,       	     v_compos_comer04,
        v_comp_max_enfesto,       	     v_descr_ing,
        v_composicao_01,          	     v_descr_ing2,
        v_composicao_02,          	     v_descr_esp,
        v_composicao_03,          	     v_compos_ing1,
        v_composicao_04,          	     v_compos_ing2,
        v_composicao_05,          	     v_compos_ing3,
        v_largura_2,              	     v_compos_ing4,
        v_gramatura_2,            	     v_compos_esp1,
        v_gramatura_3,            	     v_compos_esp2,
        v_avaliacao_toque,        	     v_compos_esp3,
        v_classe_fio,            	     v_compos_esp4,
        v_cursos,                 	     v_observacao,
        v_elasticidade_aca,       	     v_obs_ing,
        v_elasticidade_cru,       	     v_obs_esp,
        v_enco_comprimento,       	     v_gramatura_pre_estampado,
        v_enco_largura,           	     v_peso_rolo_pre_estampado,
        v_fator_cobertura,        	     v_largura_pre_estampado,
        v_grau_compactacao,      	     v_comprim_pre_estampado,
        v_kit_elastano,           	     v_perdas_pre_estampado,
        v_mercerizado,            	     v_limite_falhas_rolo,
        v_nr_alimentadores,       	     v_limite_falhas_mini,
        v_numero_agulhas,         	     v_tato,
        v_perc_composicao1,       	     v_referencia_origem,
        v_perc_composicao2,       	     v_consumo_pasta_normal,
        v_perc_composicao3,       	     v_largura_cilindro,
        v_perc_composicao4,       	     v_largura_estampa,
        v_perc_composicao5,       	     v_compos_ing5,
        v_perc_perdas,            	     v_compos_esp5,
        v_perc_umidade,           	     v_compos_comer05,
        v_relach_comprimen,       	     v_cod_fluxo,
        v_relach_largura,         	     v_perdas_antes_tingimento,
        v_rendimento,             	     v_peso_multiplo,
        v_temperatura_estp,       	     v_tem_dep_rolo,
        v_temperatura_fixa,       	     v_tem_dep_mini_rolo,
        v_tempo_fixacao,          	     v_tem_dep_estamparia,
        v_tempo_termofixar,       	     v_composicao_06,
        v_termofixar,             	     v_perc_composicao6,
        v_titulo_fio,             	     v_diametro,
        v_torcao_acabado,         	     v_grupo_encolhimento,
        v_torcao_cru,             	     v_homologacao_fornec,
        v_tubular_aberto,         	     v_litros_absorcao,
        v_variacao_fio,           	     v_nr_rolos_cubagem,
        v_fator,                  	     v_descr_marca_reg,
        v_ind_mt_minutos,         	     v_codigo_embalagem,
        v_ind_kg_maq_dia,         	     v_desc_tam_ficha,
        v_ind_eficiencia,         	     v_comprimento_mini,
        v_ne_titulo,              	     v_caract,
        v_processo_unico_tec,     	     v_caract_ing,
        v_gramatura_vaporizado,   	     v_caract_esp,
        v_cod_processo,           	     v_perc_difer_peso,
        v_grade_distribuicao,     	     v_sequencia_tamanho,
        v_largura_saida_maquina,  	     v_raport,
        v_gramatura_saida_maquina,	     v_num_desenvolvimento,
        v_cursos_saida_maquina,   	     v_alternativa_padrao,
        v_colunas_saida_maquina,  	     v_roteiro_padrao,
        v_largura_apos_repouso,   	     v_peso_quebra,
        v_gramatura_apos_repouso, 	     v_grupo_tecido_costurar,
        v_cursos_apos_repouso,    	     v_batidas_polegadas,
        v_colunas_apos_repouso,   	     v_ordem_compra,
        v_perc_variacao_gramatura,	     v_tipo_produto_global,
        v_tam_ponto_1,            	     v_batidas_metro,
        v_tam_ponto_2,            	     v_codigo_pente,
        v_tam_ponto_3,            	     v_densidade_pente,
        v_tam_ponto_4,            	     v_qtde_fios,
        v_ne_titulo_2,            	     v_qtde_fios_fundo,
        v_ne_titulo_3,            	     v_qtde_fios_ourela,
        v_ne_titulo_4,           	     v_perc_encolhimento,
        v_lfa_1,                  	     v_passamento_fundo,
        v_lfa_2,                  	     v_passamento_ourela,
        v_lfa_3,                  	     v_puas_fundo,
        v_lfa_4,                  	     v_puas_ourela,
        v_tear_finura,            	     v_largura_pente,
        v_tear_diametro,          	     v_contracao_urdume,
        v_perc_alt_dim_comp,      	     v_contracao_trama,
        v_perc_alt_dim_larg,      	     v_oz_yd2,
        v_largura_proj,           	     v_cor_etiqueta,
        v_gramatura_proj,        	     v_linha_producao,
        v_larg_var_ini,           	     v_titulo_fibra,
        v_larg_var_fim,           	     v_um_titulo_fibra,
        v_gar_var_ini,            	     v_simbolo_1,
        v_gar_var_fim,            	     v_simbolo_2,
        v_cursos_var_ini,         	     v_simbolo_3,
        v_cursos_var_fim,         	     v_simbolo_4,
        v_colunas_var_ini,        	     v_simbolo_5,
        v_colunas_var_fim,        	     v_cod_toler_rendim,
        v_torcao_aca_var_ini,     	     v_solidez_agua_mar,
        v_torcao_aca_var_fim,     	     v_sol_agua_mar_ini,
        v_tipo_retorno,           	     v_perc_retracao,
        v_perc_var_largura,       	     v_altura_1,
        v_perc_var_compr,         	     v_altura_2,
        v_ne_titulo_1,            	     v_cursos_centimetros,
        v_alongamento,            	     v_tamanho_retilinea,
        v_grupo_agrupador,        	     v_dias_armazenamento,
        v_sub_agrupador,          	     v_peso_medio_cone,
        v_tipo_homologacao,       	     v_un_med_titulo_fio,
        v_nro_etiqs_homologacao,  	     v_grupo_tecido_cortar,
        v_solidez_lavacao,        	     v_permite_misturar_maquina,
        v_sol_lavacao_ini,        	     v_colunas_ercru,
        v_sol_lavacao_fim,        	     v_cursos_ercru,
        v_solidez_suor,           	     v_gramatura_ercru,
        v_sol_suor_ini,           	     v_largura_ercru,
        v_sol_suor_fim,           	     v_largura_pf_1,
        v_solidez_agua_clorada,   	     v_largura_pf_2,
        v_sol_agua_clorada_ini,   	     v_altura_pf_1,
        v_sol_agua_clorada_fim,   	     v_altura_pf_2,
        v_sol_agua_mar_fim,      	     v_compri_plotagem,
        v_solidez_friccao_seco,	             v_tipo_operacao,
        ws_usuario_systextil,                1);

     exception
     when OTHERS
        then raise_application_error (-20000, 'N�o atualizou a tabela e_basi_020' || Chr(10) || SQLERRM);
     end;

    end if;
  end if;
end if;


if deleting
then

  v_tipo_operacao := 'E';

                                                                 		    v_sol_friccao_seco_ini      	:= :old.sol_friccao_seco_ini;
  v_basi030_nivel030           := :old.basi030_nivel030;         		    v_sol_friccao_seco_fim      	:= :old.sol_friccao_seco_fim;
  v_basi030_referenc           := :old.basi030_referenc;         		    v_solidez_friccao_umido     	:= :old.solidez_friccao_umido;
  v_tamanho_ref                := :old.tamanho_ref;              		    v_sol_friccao_umido_ini     	:= :old.sol_friccao_umido_ini;
  v_descr_tam_refer            := :old.descr_tam_refer;          		    v_sol_friccao_umido_fim     	:= :old.sol_friccao_umido_fim;
  v_tipo_produto               := :old.tipo_produto;             		    v_metros_lineares           	:= :old.metros_lineares;
  v_artigo_cotas               := :old.artigo_cotas;             		    v_metros_cubicos            	:= :old.metros_cubicos;
  v_largura_1                  := :old.largura_1;                		    v_agulhas_falhadas          	:= :old.agulhas_falhadas;
  v_gramatura_1                := :old.gramatura_1;              		    v_num_agulhas_falhadas      	:= :old.num_agulhas_falhadas;
  v_peso_rolo                  := :old.peso_rolo;                		    v_numero_pontos             	:= :old.numero_pontos;
  v_data_importacao            := sysdate();          		                    v_polegadas_agulha          	:= :old.polegadas_agulha;
                                                                		    v_peso_mini_rolo            	:= :old.peso_mini_rolo;
  	   		                                                            v_compos_comer01           		:= :old.compos_comer01;
  v_lote_fabr_pecas            := :old.lote_fabr_pecas;          		    v_compos_comer02           		:= :old.compos_comer02;
  v_peso_liquido               := :old.peso_liquido;          		            v_compos_comer03           		:= :old.compos_comer03;
  v_qtde_max_enfesto           := :old.qtde_max_enfesto;         		    v_compos_comer04           		:= :old.compos_comer04;
  v_comp_max_enfesto           := :old.comp_max_enfesto;         		    v_descr_ing                		:= :old.descr_ing;
  v_composicao_01              := :old.composicao_01;            		    v_descr_ing2               		:= :old.descr_ing2;
  v_composicao_02              := :old.composicao_02;            		    v_descr_esp                		:= :old.descr_esp;
  v_composicao_03              := :old.composicao_03;            		    v_compos_ing1              		:= :old.compos_ing1;
  v_composicao_04              := :old.composicao_04;           		    v_compos_ing2              		:= :old.compos_ing2;
  v_composicao_05              := :old.composicao_05;            		    v_compos_ing3              		:= :old.compos_ing3;
  v_largura_2                  := :old.largura_2;                		    v_compos_ing4              		:= :old.compos_ing4;
  v_gramatura_2                := :old.gramatura_2;              		    v_compos_esp1              		:= :old.compos_esp1;
  v_gramatura_3                := :old.gramatura_3;              		    v_compos_esp2              		:= :old.compos_esp2;
  v_avaliacao_toque            := :old.avaliacao_toque;         		    v_compos_esp3              		:= :old.compos_esp3;
  v_classe_fio                 := :old.classe_fio;               		    v_compos_esp4              		:= :old.compos_esp4;
  v_cursos                     := :old.cursos;                   		    v_observacao               		:= :old.observacao;
  v_elasticidade_aca           := :old.elasticidade_aca;         		    v_obs_ing                  		:= :old.obs_ing;
  v_elasticidade_cru           := :old.elasticidade_cru;         		    v_obs_esp                  		:= :old.obs_esp;
  v_enco_comprimento           := :old.enco_comprimento;         		    v_gramatura_pre_estampado  		:= :old.gramatura_pre_estampado;
  v_enco_largura               := :old.enco_largura;             		    v_peso_rolo_pre_estampado  		:= :old.peso_rolo_pre_estampado;
  v_fator_cobertura            := :old.fator_cobertura;          		    v_largura_pre_estampado    		:= :old.largura_pre_estampado;
  v_grau_compactacao           := :old.grau_compactacao;         		    v_comprim_pre_estampado    		:= :old.comprim_pre_estampado;
  v_kit_elastano               := :old.kit_elastano;             		    v_perdas_pre_estampado     		:= :old.perdas_pre_estampado;
  v_mercerizado                := :old.mercerizado;              		    v_limite_falhas_rolo       		:= :old.limite_falhas_rolo;
  v_nr_alimentadores           := :old.nr_alimentadores;         		    v_limite_falhas_mini       		:= :old.limite_falhas_mini;
  v_numero_agulhas             := :old.numero_agulhas;           		    v_tato                     		:= :old.tato;
  v_perc_composicao1           := :old.perc_composicao1;         		    v_referencia_origem        		:= :old.referencia_origem;
  v_perc_composicao2           := :old.perc_composicao2;         		    v_consumo_pasta_normal     		:= :old.consumo_pasta_normal;
  v_perc_composicao3           := :old.perc_composicao3;         		    v_largura_cilindro         		:= :old.largura_cilindro;
  v_perc_composicao4           := :old.perc_composicao4;         		    v_largura_estampa          		:= :old.largura_estampa;
  v_perc_composicao5           := :old.perc_composicao5;         		    v_compos_ing5              		:= :old.compos_ing5;
  v_perc_perdas                := :old.perc_perdas;              		    v_compos_esp5              		:= :old.compos_esp5;
  v_perc_umidade               := :old.perc_umidade;             		    v_compos_comer05           		:= :old.compos_comer05;
  v_relach_comprimen           := :old.relach_comprimen;         		    v_cod_fluxo                		:= :old.cod_fluxo;
  v_relach_largura             := :old.relach_largura;           		    v_perdas_antes_tingimento  		:= :old.perdas_antes_tingimento;
  v_rendimento                 := :old.rendimento;               		    v_peso_multiplo            		:= :old.peso_multiplo;
  v_temperatura_estp           := :old.temperatura_estp;         		    v_tem_dep_rolo             		:= :old.tem_dep_rolo;
  v_temperatura_fixa           := :old.temperatura_fixa;         		    v_tem_dep_mini_rolo        		:= :old.tem_dep_mini_rolo;
  v_tempo_fixacao              := :old.tempo_fixacao;            		    v_tem_dep_estamparia       		:= :old.tem_dep_estamparia;
  v_tempo_termofixar           := :old.tempo_termofixar;         		    v_composicao_06            		:= :old.composicao_06;
  v_termofixar                 := :old.termofixar;               		    v_perc_composicao6         		:= :old.perc_composicao6;
  v_titulo_fio                 := :old.titulo_fio;               		    v_diametro                 		:= :old.diametro;
  v_torcao_acabado             := :old.torcao_acabado;           		    v_grupo_encolhimento       		:= :old.grupo_encolhimento;
  v_torcao_cru                 := :old.torcao_cru;              		    v_homologacao_fornec       		:= :old.homologacao_fornec;
  v_tubular_aberto             := :old.tubular_aberto;          		    v_litros_absorcao          		:= :old.litros_absorcao;
  v_variacao_fio               := :old.variacao_fio;             		    v_nr_rolos_cubagem         		:= :old.nr_rolos_cubagem;
  v_fator                      := :old.fator;                    		    v_descr_marca_reg          		:= :old.descr_marca_reg;
  v_ind_mt_minutos             := :old.ind_mt_minutos;           		    v_codigo_embalagem         		:= :old.codigo_embalagem;
  v_ind_kg_maq_dia             := :old.ind_kg_maq_dia;           		    v_desc_tam_ficha           		:= :old.desc_tam_ficha;
  v_ind_eficiencia             := :old.ind_eficiencia;           		    v_comprimento_mini         		:= :old.comprimento_mini;
  v_ne_titulo                  := :old.ne_titulo;                		    v_caract                   		:= :old.caract;
  v_processo_unico_tec         := :old.processo_unico_tec;      		    v_caract_ing               		:= :old.caract_ing;
  v_gramatura_vaporizado       := :old.gramatura_vaporizado;     		    v_caract_esp               		:= :old.caract_esp;
  v_cod_processo               := :old.cod_processo;             		    v_perc_difer_peso          		:= :old.perc_difer_peso;
  v_grade_distribuicao         := :old.grade_distribuicao;       		    v_sequencia_tamanho        		:= :old.sequencia_tamanho;
  v_largura_saida_maquina      := :old.largura_saida_maquina;    		    v_raport                   		:= :old.raport;
  v_gramatura_saida_maquina    := :old.gramatura_saida_maquina;  		    v_num_desenvolvimento      		:= :old.num_desenvolvimento;
  v_cursos_saida_maquina       := :old.cursos_saida_maquina;     		    v_alternativa_padrao       		:= :old.alternativa_padrao;
  v_colunas_saida_maquina      := :old.colunas_saida_maquina;    		    v_roteiro_padrao           		:= :old.roteiro_padrao;
  v_largura_apos_repouso       := :old.largura_apos_repouso;     		    v_peso_quebra              		:= :old.peso_quebra;
  v_gramatura_apos_repouso     := :old.gramatura_apos_repouso;   		    v_grupo_tecido_costurar    		:= :old.grupo_tecido_costurar;
  v_cursos_apos_repouso        := :old.cursos_apos_repouso;     		    v_batidas_polegadas        		:= :old.batidas_polegadas;
  v_colunas_apos_repouso       := :old.colunas_apos_repouso;     		    v_ordem_compra             		:= :old.ordem_compra;
  v_perc_variacao_gramatura    := :old.perc_variacao_gramatura;  		    v_tipo_produto_global      		:= :old.tipo_produto_global;
  v_tam_ponto_1                := :old.tam_ponto_1;              		    v_batidas_metro            		:= :old.batidas_metro;
  v_tam_ponto_2                := :old.tam_ponto_2;              		    v_codigo_pente             		:= :old.codigo_pente;
  v_tam_ponto_3                := :old.tam_ponto_3;              		    v_densidade_pente          		:= :old.densidade_pente;
  v_tam_ponto_4                := :old.tam_ponto_4;              		    v_qtde_fios                		:= :old.qtde_fios;
  v_ne_titulo_2                := :old.ne_titulo_2;              		    v_qtde_fios_fundo          		:= :old.qtde_fios_fundo;
  v_ne_titulo_3                := :old.ne_titulo_3;             		    v_qtde_fios_ourela         		:= :old.qtde_fios_ourela;
  v_ne_titulo_4                := :old.ne_titulo_4;              		    v_perc_encolhimento        		:= :old.perc_encolhimento;
  v_lfa_1                      := :old.lfa_1;                    		    v_passamento_fundo         		:= :old.passamento_fundo;
  v_lfa_2                      := :old.lfa_2;                    		    v_passamento_ourela        		:= :old.passamento_ourela;
  v_lfa_3                      := :old.lfa_3;                    		    v_puas_fundo               		:= :old.puas_fundo;
  v_lfa_4                      := :old.lfa_4;                    		    v_puas_ourela              		:= :old.puas_ourela;
  v_tear_finura                := :old.tear_finura;              		    v_largura_pente            		:= :old.largura_pente;
  v_tear_diametro              := :old.tear_diametro;            		    v_contracao_urdume         		:= :old.contracao_urdume;
  v_perc_alt_dim_comp          := :old.perc_alt_dim_comp;        		    v_contracao_trama          		:= :old.contracao_trama;
  v_perc_alt_dim_larg          := :old.perc_alt_dim_larg;        		    v_oz_yd2                   		:= :old.oz_yd2;
  v_largura_proj               := :old.largura_proj;             		    v_cor_etiqueta             		:= :old.cor_etiqueta;
  v_gramatura_proj             := :old.gramatura_proj;           		    v_linha_producao           		:= :old.linha_producao;
  v_larg_var_ini               := :old.larg_var_ini;             		    v_titulo_fibra             		:= :old.titulo_fibra;
  v_larg_var_fim               := :old.larg_var_fim;             		    v_um_titulo_fibra          		:= :old.um_titulo_fibra;
  v_gar_var_ini                := :old.gar_var_ini;              		    v_simbolo_1                		:= :old.simbolo_1;
  v_gar_var_fim                := :old.gar_var_fim;              		    v_simbolo_2                		:= :old.simbolo_2;
  v_cursos_var_ini             := :old.cursos_var_ini;           		    v_simbolo_3                		:= :old.simbolo_3;
  v_cursos_var_fim             := :old.cursos_var_fim;           		    v_simbolo_4                		:= :old.simbolo_4;
  v_colunas_var_ini            := :old.colunas_var_ini;          		    v_simbolo_5                		:= :old.simbolo_5;
  v_colunas_var_fim            := :old.colunas_var_fim;          		    v_cod_toler_rendim         		:= :old.cod_toler_rendim;
  v_torcao_aca_var_ini         := :old.torcao_aca_var_ini;       		    v_solidez_agua_mar         		:= :old.solidez_agua_mar;
  v_torcao_aca_var_fim         := :old.torcao_aca_var_fim;      		    v_sol_agua_mar_ini         		:= :old.sol_agua_mar_ini;
  v_tipo_retorno               := :old.tipo_retorno;             		    v_perc_retracao            		:= :old.perc_retracao;
  v_perc_var_largura           := :old.perc_var_largura;         		    v_altura_1                 		:= :old.altura_1;
  v_perc_var_compr             := :old.perc_var_compr;           		    v_altura_2                 		:= :old.altura_2;
  v_ne_titulo_1                := :old.ne_titulo_1;              		    v_cursos_centimetros       		:= :old.cursos_centimetros;
  v_alongamento                := :old.alongamento;              		    v_tamanho_retilinea        		:= :old.tamanho_retilinea;
  v_grupo_agrupador            := :old.grupo_agrupador;          		    v_dias_armazenamento       		:= :old.dias_armazenamento;
  v_sub_agrupador              := :old.sub_agrupador;            		    v_peso_medio_cone          		:= :old.peso_medio_cone;
  v_tipo_homologacao           := :old.tipo_homologacao;         		    v_un_med_titulo_fio        		:= :old.un_med_titulo_fio;
  v_nro_etiqs_homologacao      := :old.nro_etiqs_homologacao;    		    v_grupo_tecido_cortar      		:= :old.grupo_tecido_cortar;
  v_solidez_lavacao            := :old.solidez_lavacao;         		    v_permite_misturar_maquina 		:= :old.permite_misturar_maquina;
  v_sol_lavacao_ini            := :old.sol_lavacao_ini;          		    v_colunas_ercru            		:= :old.colunas_ercru;
  v_sol_lavacao_fim            := :old.sol_lavacao_fim;          		    v_cursos_ercru             		:= :old.cursos_ercru;
  v_solidez_suor               := :old.solidez_suor;             		    v_gramatura_ercru          		:= :old.gramatura_ercru;
  v_sol_suor_ini               := :old.sol_suor_ini;             		    v_largura_ercru            		:= :old.largura_ercru;
  v_sol_suor_fim               := :old.sol_suor_fim;             		    v_largura_pf_1             		:= :old.largura_pf_1;
  v_solidez_agua_clorada       := :old.solidez_agua_clorada;     		    v_largura_pf_2             		:= :old.largura_pf_2;
  v_sol_agua_clorada_ini       := :old.sol_agua_clorada_ini;     		    v_altura_pf_1              		:= :old.altura_pf_1;
  v_sol_agua_clorada_fim       := :old.sol_agua_clorada_fim;     		    v_altura_pf_2              		:= :old.altura_pf_2;
  v_sol_agua_mar_fim           := :old.sol_agua_mar_fim;         		    v_compri_plotagem  			:= :old.compri_plotagem;
  v_solidez_friccao_seco       := :old.solidez_friccao_seco;

  if v_basi030_nivel030 = '2'
  then

  begin

  insert into e_basi_020
  (                      	     sol_friccao_seco_ini,
   basi030_nivel030,         	     sol_friccao_seco_fim,
   basi030_referenc,         	     solidez_friccao_umido,
   tamanho_ref,              	     sol_friccao_umido_ini,
   descr_tam_refer,          	     sol_friccao_umido_fim,
   tipo_produto,            	     metros_lineares,
   artigo_cotas,             	     metros_cubicos,
   largura_1,                	     agulhas_falhadas,
   gramatura_1,              	     num_agulhas_falhadas,
   peso_rolo,               	     numero_pontos,
   data_importacao,          	     polegadas_agulha,
                               	     peso_mini_rolo,
     	   	                     compos_comer01,
   lote_fabr_pecas,         	     compos_comer02,
   peso_liquido,             	     compos_comer03,
   qtde_max_enfesto,         	     compos_comer04,
   comp_max_enfesto,         	     descr_ing,
   composicao_01,            	     descr_ing2,
   composicao_02,            	     descr_esp,
   composicao_03,            	     compos_ing1,
   composicao_04,            	     compos_ing2,
   composicao_05,            	     compos_ing3,
   largura_2,                	     compos_ing4,
   gramatura_2,              	     compos_esp1,
   gramatura_3,              	     compos_esp2,
   avaliacao_toque,          	     compos_esp3,
   classe_fio,               	     compos_esp4,
   cursos,                   	     observacao,
   elasticidade_aca,        	     obs_ing,
   elasticidade_cru,         	     obs_esp,
   enco_comprimento,         	     gramatura_pre_estampado,
   enco_largura,             	     peso_rolo_pre_estampado,
   fator_cobertura,          	     largura_pre_estampado,
   grau_compactacao,         	     comprim_pre_estampado,
   kit_elastano,             	     perdas_pre_estampado,
   mercerizado,              	     limite_falhas_rolo,
   nr_alimentadores,         	     limite_falhas_mini,
   numero_agulhas,           	     tato,
   perc_composicao1,         	     referencia_origem,
   perc_composicao2,         	     consumo_pasta_normal,
   perc_composicao3,        	     largura_cilindro,
   perc_composicao4,         	     largura_estampa,
   perc_composicao5,         	     compos_ing5,
   perc_perdas,              	     compos_esp5,
   perc_umidade,            	     compos_comer05,
   relach_comprimen,         	     cod_fluxo,
   relach_largura,           	     perdas_antes_tingimento,
   rendimento,               	     peso_multiplo,
   temperatura_estp,         	     tem_dep_rolo,
   temperatura_fixa,         	     tem_dep_mini_rolo,
   tempo_fixacao,            	     tem_dep_estamparia,
   tempo_termofixar,         	     composicao_06,
   termofixar,               	     perc_composicao6,
   titulo_fio,               	     diametro,
   torcao_acabado,           	     grupo_encolhimento,
   torcao_cru,               	     homologacao_fornec,
   tubular_aberto,           	     litros_absorcao,
   variacao_fio,             	     nr_rolos_cubagem,
   fator,                    	     descr_marca_reg,
   ind_mt_minutos,           	     codigo_embalagem,
   ind_kg_maq_dia,           	     desc_tam_ficha,
   ind_eficiencia,           	     comprimento_mini,
   ne_titulo,                	     caract,
   processo_unico_tec,       	     caract_ing,
   gramatura_vaporizado,     	     caract_esp,
   cod_processo,             	     perc_difer_peso,
   grade_distribuicao,       	     sequencia_tamanho,
   largura_saida_maquina,    	     raport,
   gramatura_saida_maquina,  	     num_desenvolvimento,
   cursos_saida_maquina,     	     alternativa_padrao,
   colunas_saida_maquina,    	     roteiro_padrao,
   largura_apos_repouso,     	     peso_quebra,
   gramatura_apos_repouso,   	     grupo_tecido_costurar,
   cursos_apos_repouso,      	     batidas_polegadas,
   colunas_apos_repouso,     	     ordem_compra,
   perc_variacao_gramatura,  	     tipo_produto_global,
   tam_ponto_1,              	     batidas_metro,
   tam_ponto_2,              	     codigo_pente,
   tam_ponto_3,              	     densidade_pente,
   tam_ponto_4,              	     qtde_fios,
   ne_titulo_2,              	     qtde_fios_fundo,
   ne_titulo_3,              	     qtde_fios_ourela,
   ne_titulo_4,              	     perc_encolhimento,
   lfa_1,                    	     passamento_fundo,
   lfa_2,                    	     passamento_ourela,
   lfa_3,                    	     puas_fundo,
   lfa_4,                    	     puas_ourela,
   tear_finura,              	     largura_pente,
   tear_diametro,            	     contracao_urdume,
   perc_alt_dim_comp,       	     contracao_trama,
   perc_alt_dim_larg,        	     oz_yd2,
   largura_proj,             	     cor_etiqueta,
   gramatura_proj,          	     linha_producao,
   larg_var_ini,             	     titulo_fibra,
   larg_var_fim,             	     um_titulo_fibra,
   gar_var_ini,              	     simbolo_1,
   gar_var_fim,              	     simbolo_2,
   cursos_var_ini,           	     simbolo_3,
   cursos_var_fim,           	     simbolo_4,
   colunas_var_ini,          	     simbolo_5,
   colunas_var_fim,          	     cod_toler_rendim,
   torcao_aca_var_ini,       	     solidez_agua_mar,
   torcao_aca_var_fim,       	     sol_agua_mar_ini,
   tipo_retorno,             	     perc_retracao,
   perc_var_largura,         	     altura_1,
   perc_var_compr,           	     altura_2,
   ne_titulo_1,              	     cursos_centimetros,
   alongamento,             	     tamanho_retilinea,
   grupo_agrupador,          	     dias_armazenamento,
   sub_agrupador,            	     peso_medio_cone,
   tipo_homologacao,         	     un_med_titulo_fio,
   nro_etiqs_homologacao,    	     grupo_tecido_cortar,
   solidez_lavacao,          	     permite_misturar_maquina,
   sol_lavacao_ini,          	     colunas_ercru,
   sol_lavacao_fim,          	     cursos_ercru,
   solidez_suor,            	     gramatura_ercru,
   sol_suor_ini,             	     largura_ercru,
   sol_suor_fim,             	     largura_pf_1,
   solidez_agua_clorada,     	     largura_pf_2,
   sol_agua_clorada_ini,     	     altura_pf_1,
   sol_agua_clorada_fim,     	     altura_pf_2,
   sol_agua_mar_fim,         	     compri_plotagem,
   solidez_friccao_seco,     	     tipo_operacao,
   usuario_operacao,                 flag_integracao)
   VALUES
   (                     	     v_sol_friccao_seco_ini,
    v_basi030_nivel030,       	     v_sol_friccao_seco_fim,
    v_basi030_referenc,       	     v_solidez_friccao_umido,
    v_tamanho_ref,		     v_sol_friccao_umido_ini,
    v_descr_tam_refer,        	     v_sol_friccao_umido_fim,
    v_tipo_produto,           	     v_metros_lineares,
    v_artigo_cotas,           	     v_metros_cubicos,
    v_largura_1,           	     v_agulhas_falhadas,
    v_gramatura_1,            	     v_num_agulhas_falhadas,
    v_peso_rolo,              	     v_numero_pontos,
    v_data_importacao,        	     v_polegadas_agulha,
                         	     v_peso_mini_rolo,
      	                             v_compos_comer01,
    v_lote_fabr_pecas,        	     v_compos_comer02,
    v_peso_liquido,        	     v_compos_comer03,
    v_qtde_max_enfesto,       	     v_compos_comer04,
    v_comp_max_enfesto,       	     v_descr_ing,
    v_composicao_01,          	     v_descr_ing2,
    v_composicao_02,          	     v_descr_esp,
    v_composicao_03,          	     v_compos_ing1,
    v_composicao_04,          	     v_compos_ing2,
    v_composicao_05,          	     v_compos_ing3,
    v_largura_2,              	     v_compos_ing4,
    v_gramatura_2,            	     v_compos_esp1,
    v_gramatura_3,            	     v_compos_esp2,
    v_avaliacao_toque,        	     v_compos_esp3,
    v_classe_fio,            	     v_compos_esp4,
    v_cursos,                 	     v_observacao,
    v_elasticidade_aca,       	     v_obs_ing,
    v_elasticidade_cru,       	     v_obs_esp,
    v_enco_comprimento,       	     v_gramatura_pre_estampado,
    v_enco_largura,           	     v_peso_rolo_pre_estampado,
    v_fator_cobertura,        	     v_largura_pre_estampado,
    v_grau_compactacao,      	     v_comprim_pre_estampado,
    v_kit_elastano,           	     v_perdas_pre_estampado,
    v_mercerizado,            	     v_limite_falhas_rolo,
    v_nr_alimentadores,       	     v_limite_falhas_mini,
    v_numero_agulhas,         	     v_tato,
    v_perc_composicao1,       	     v_referencia_origem,
    v_perc_composicao2,       	     v_consumo_pasta_normal,
    v_perc_composicao3,       	     v_largura_cilindro,
    v_perc_composicao4,       	     v_largura_estampa,
    v_perc_composicao5,       	     v_compos_ing5,
    v_perc_perdas,            	     v_compos_esp5,
    v_perc_umidade,           	     v_compos_comer05,
    v_relach_comprimen,       	     v_cod_fluxo,
    v_relach_largura,         	     v_perdas_antes_tingimento,
    v_rendimento,             	     v_peso_multiplo,
    v_temperatura_estp,       	     v_tem_dep_rolo,
    v_temperatura_fixa,       	     v_tem_dep_mini_rolo,
    v_tempo_fixacao,          	     v_tem_dep_estamparia,
    v_tempo_termofixar,       	     v_composicao_06,
    v_termofixar,             	     v_perc_composicao6,
    v_titulo_fio,             	     v_diametro,
    v_torcao_acabado,         	     v_grupo_encolhimento,
    v_torcao_cru,             	     v_homologacao_fornec,
    v_tubular_aberto,         	     v_litros_absorcao,
    v_variacao_fio,           	     v_nr_rolos_cubagem,
    v_fator,                  	     v_descr_marca_reg,
    v_ind_mt_minutos,         	     v_codigo_embalagem,
    v_ind_kg_maq_dia,         	     v_desc_tam_ficha,
    v_ind_eficiencia,         	     v_comprimento_mini,
    v_ne_titulo,              	     v_caract,
    v_processo_unico_tec,     	     v_caract_ing,
    v_gramatura_vaporizado,   	     v_caract_esp,
    v_cod_processo,           	     v_perc_difer_peso,
    v_grade_distribuicao,     	     v_sequencia_tamanho,
    v_largura_saida_maquina,  	     v_raport,
    v_gramatura_saida_maquina,	     v_num_desenvolvimento,
    v_cursos_saida_maquina,   	     v_alternativa_padrao,
    v_colunas_saida_maquina,  	     v_roteiro_padrao,
    v_largura_apos_repouso,   	     v_peso_quebra,
    v_gramatura_apos_repouso, 	     v_grupo_tecido_costurar,
    v_cursos_apos_repouso,    	     v_batidas_polegadas,
    v_colunas_apos_repouso,   	     v_ordem_compra,
    v_perc_variacao_gramatura,	     v_tipo_produto_global,
    v_tam_ponto_1,            	     v_batidas_metro,
    v_tam_ponto_2,            	     v_codigo_pente,
    v_tam_ponto_3,            	     v_densidade_pente,
    v_tam_ponto_4,            	     v_qtde_fios,
    v_ne_titulo_2,            	     v_qtde_fios_fundo,
    v_ne_titulo_3,            	     v_qtde_fios_ourela,
    v_ne_titulo_4,           	     v_perc_encolhimento,
    v_lfa_1,                  	     v_passamento_fundo,
    v_lfa_2,                  	     v_passamento_ourela,
    v_lfa_3,                  	     v_puas_fundo,
    v_lfa_4,                  	     v_puas_ourela,
    v_tear_finura,            	     v_largura_pente,
    v_tear_diametro,          	     v_contracao_urdume,
    v_perc_alt_dim_comp,      	     v_contracao_trama,
    v_perc_alt_dim_larg,      	     v_oz_yd2,
    v_largura_proj,           	     v_cor_etiqueta,
    v_gramatura_proj,        	     v_linha_producao,
    v_larg_var_ini,           	     v_titulo_fibra,
    v_larg_var_fim,           	     v_um_titulo_fibra,
    v_gar_var_ini,            	     v_simbolo_1,
    v_gar_var_fim,            	     v_simbolo_2,
    v_cursos_var_ini,         	     v_simbolo_3,
    v_cursos_var_fim,         	     v_simbolo_4,
    v_colunas_var_ini,        	     v_simbolo_5,
    v_colunas_var_fim,        	     v_cod_toler_rendim,
    v_torcao_aca_var_ini,     	     v_solidez_agua_mar,
    v_torcao_aca_var_fim,     	     v_sol_agua_mar_ini,
    v_tipo_retorno,           	     v_perc_retracao,
    v_perc_var_largura,       	     v_altura_1,
    v_perc_var_compr,         	     v_altura_2,
    v_ne_titulo_1,            	     v_cursos_centimetros,
    v_alongamento,            	     v_tamanho_retilinea,
    v_grupo_agrupador,        	     v_dias_armazenamento,
    v_sub_agrupador,          	     v_peso_medio_cone,
    v_tipo_homologacao,       	     v_un_med_titulo_fio,
    v_nro_etiqs_homologacao,  	     v_grupo_tecido_cortar,
    v_solidez_lavacao,        	     v_permite_misturar_maquina,
    v_sol_lavacao_ini,        	     v_colunas_ercru,
    v_sol_lavacao_fim,        	     v_cursos_ercru,
    v_solidez_suor,           	     v_gramatura_ercru,
    v_sol_suor_ini,           	     v_largura_ercru,
    v_sol_suor_fim,           	     v_largura_pf_1,
    v_solidez_agua_clorada,   	     v_largura_pf_2,
    v_sol_agua_clorada_ini,   	     v_altura_pf_1,
    v_sol_agua_clorada_fim,   	     v_altura_pf_2,
    v_sol_agua_mar_fim,      	     v_compri_plotagem,
    v_solidez_friccao_seco,	     v_tipo_operacao,
    ws_usuario_systextil,            1);

    exception
    when OTHERS
       then raise_application_error (-20000, 'N�o atualizou a tabela e_basi_020' || Chr(10) || SQLERRM);
    end;
  end if;
  end if;
end inter_tr_basi_020_integracao;
-- ALTER TRIGGER "INTER_TR_BASI_020_INTEGRACAO" DISABLE
 

/

exec inter_pr_recompile;

