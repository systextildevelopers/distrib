CREATE OR REPLACE FORCE VIEW inter_vi_pedidos_copiados 
as 
select destino.pedido_venda pedido_destino, 
       destino.pedido_origem,
       destino.cli_ped_cgc_cli9,
       destino.cli_ped_cgc_cli4,
       destino.cli_ped_cgc_cli2,
       pedi_010.nome_cliente,
       origem.codigo_empresa codigo_empresa_origem,
       destino.codigo_empresa codigo_empresa_destino,
       origem.data_emis_venda data_emis_venda_origem,
       destino.data_emis_venda data_emis_venda_destino,
       destino.data_hora_copia
from pedi_100 destino, pedi_010, pedi_100 origem
where destino.cli_ped_cgc_cli9 = pedi_010.cgc_9
  and destino.cli_ped_cgc_cli4 = pedi_010.cgc_4
  and destino.cli_ped_cgc_cli2 = pedi_010.cgc_2
  and destino.pedido_origem    = origem.pedido_venda
;
/

exec inter_pr_recompile;
