ALTER TABLE SPED_0150
ADD CONSTRAINT PK_SPED_0150
PRIMARY KEY (TIP_CONTABIL_FISCAL, COD_EMPRESA, NUM_CNPJ9, NUM_CNPJ4, NUM_CNPJ2, TIP_PARTICIPANTE, SEQ_END_ENTREGA);

ALTER TABLE SPED_0150
ENABLE CONSTRAINT PK_SPED_0150;

ALTER TABLE SPED_C100
MODIFY SEQ_END_ENTREGA DEFAULT 0 NOT NULL;

ALTER TABLE SPED_0150 
MODIFY SEQ_END_ENTREGA DEFAULT 0 NOT NULL;

