INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'obrf_f276',     1,
	1,			     'Parâmetros de Importação de XML de Nota Fiscal');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    1, 
	'obrf_f276',   'menu_mp05', 
	1,             1, 
	'S',           'S', 
	'S',           'S');
	
INSERT INTO hdoc_033
(	usu_prg_cdusu,  usu_prg_empr_usu, 
	programa,       nome_menu, 
	item_menu,      ordem_menu, 
	incluir,        modificar, 
	excluir,        procurar)
VALUES
(	'TREINAMENTO',  1, 
	'obrf_f276',    'menu_mp05', 
	1,              1, 
	'S',            'S', 
	'S',            'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Parâmetros de Importação de XML de Nota Fiscal'
 WHERE hdoc_036.codigo_programa = 'obrf_f276'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;

/
EXEC inter_pr_recompile;
