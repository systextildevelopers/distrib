-- Create table
create table OBRF_797
(
  NUMERO_SOLICITACAO  NUMBER(9) default 0 not null,
  SEQ_PASSO           NUMBER(4) default 0 not null,
  NUMERO_DOCUMENTO    NUMBER(9) default 0 not null,
  PERIODO_PRODUCAO040 NUMBER(4) default 0 not null,
  ORDEM_CONFECCAO040  NUMBER(5) default 0 not null,
  CODIGO_ESTAGIO      NUMBER(2) default 0 not null,
  ORDEM_PRODUCAO      NUMBER(9) default 0 not null,
  SEQ_PRODUTO         NUMBER(5) default 0 not null,
  NIVEL               VARCHAR2(1) default ' ' not null,
  GRUPO               VARCHAR2(5) default ' ' not null,
  SUBGRUPO            VARCHAR2(3) default ' ' not null,
  ITEM                VARCHAR2(6) default ' ' not null,
  LOTE_ACOMP          NUMBER(6) default 0 not null,
  COD_DEPOSITO        NUMBER(3) default 0 not null,
  VALOR_UNITARIO      NUMBER(15,5) default 0.00000 not null,
  QUANTIDADE          NUMBER(15,3) default 0.000 not null,
  FLAG_MARCADO        NUMBER(1) default 0 not null
);

-- Add comments to the columns 
comment on column OBRF_797.NUMERO_SOLICITACAO
  is 'Número de solicitação da execução do processo';
comment on column OBRF_797.SEQ_PASSO
  is 'Sequência do passo da execução do processo';
comment on column OBRF_797.PERIODO_PRODUCAO040
  is 'Período de Produção da execução do processo neste passo';
comment on column OBRF_797.ORDEM_CONFECCAO040
  is 'Ordem de Confecção da execução do processo neste passo';
comment on column OBRF_797.CODIGO_ESTAGIO
  is 'Código do Estágio da execução do processo neste passo';
comment on column OBRF_797.SEQ_PRODUTO
  is 'Sequencial do produto a ser gerado na execução do processo neste passo';
comment on column OBRF_797.NIVEL
  is 'Nível do produto a ser gerado na execução do processo neste passo';
comment on column OBRF_797.GRUPO
  is 'Grupo do produto a ser gerado na execução do processo neste passo';
comment on column OBRF_797.SUBGRUPO
  is 'Subgrupo do produto a ser gerado na execução do processo neste passo';
comment on column OBRF_797.ITEM
  is 'Item do produto a ser gerado na execução do processo neste passo';
comment on column OBRF_797.LOTE_ACOMP
  is 'Lote do produto a ser gerado na execução do processo neste passo';
comment on column OBRF_797.COD_DEPOSITO
  is 'Depósito do item a ser gerado na execução do processo neste passo';
comment on column OBRF_797.VALOR_UNITARIO
  is 'Valor unitário do item a ser gerado na execução do processo neste passo';
comment on column OBRF_797.QUANTIDADE
  is 'Quantidade do item a ser gerado na execução do processo neste passo';
comment on column OBRF_797.FLAG_MARCADO
  is 'Flag para indicar se o item foi considerado (1) ou não (0) a ser gerado na execução do processo neste passo';
-- Create/Recreate primary, unique and foreign key constraints 
alter table OBRF_797
  add constraint PK_OBRF_797 primary key (NUMERO_SOLICITACAO, SEQ_PASSO, NIVEL, GRUPO, SUBGRUPO, ITEM, LOTE_ACOMP);


exec inter_pr_recompile;
/
