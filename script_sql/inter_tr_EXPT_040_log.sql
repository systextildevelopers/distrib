create or replace trigger inter_tr_EXPT_040_log 
after insert or delete or update 
on EXPT_040 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(20) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 
 
 if inserting 
 then 
    begin 
 
        insert into EXPT_040_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           documento_OLD,   /*8*/ 
           documento_NEW,   /*9*/ 
           serie_OLD,   /*10*/ 
           serie_NEW,   /*11*/ 
           cgc9_OLD,   /*12*/ 
           cgc9_NEW,   /*13*/ 
           cgc4_OLD,   /*14*/ 
           cgc4_NEW,   /*15*/ 
           cgc2_OLD,   /*16*/ 
           cgc2_NEW,   /*17*/ 
           atualizado_em_OLD,   /*18*/ 
           atualizado_em_NEW,   /*19*/ 
           atualizado_por_OLD,   /*20*/ 
           atualizado_por_NEW,   /*21*/ 
           situacao_OLD,   /*22*/ 
           situacao_NEW    /*23*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.documento, /*9*/   
           '',/*10*/
           :new.serie, /*11*/   
           0,/*12*/
           :new.cgc9, /*13*/   
           0,/*14*/
           :new.cgc4, /*15*/   
           0,/*16*/
           :new.cgc2, /*17*/   
           null,/*18*/
           :new.atualizado_em, /*19*/   
           '',/*20*/
           :new.atualizado_por, /*21*/   
           0,/*22*/
           :new.situacao /*23*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into EXPT_040_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           documento_OLD, /*8*/   
           documento_NEW, /*9*/   
           serie_OLD, /*10*/   
           serie_NEW, /*11*/   
           cgc9_OLD, /*12*/   
           cgc9_NEW, /*13*/   
           cgc4_OLD, /*14*/   
           cgc4_NEW, /*15*/   
           cgc2_OLD, /*16*/   
           cgc2_NEW, /*17*/   
           atualizado_em_OLD, /*18*/   
           atualizado_em_NEW, /*19*/   
           atualizado_por_OLD, /*20*/   
           atualizado_por_NEW, /*21*/   
           situacao_OLD, /*22*/   
           situacao_NEW  /*23*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.documento,  /*8*/  
           :new.documento, /*9*/   
           :old.serie,  /*10*/  
           :new.serie, /*11*/   
           :old.cgc9,  /*12*/  
           :new.cgc9, /*13*/   
           :old.cgc4,  /*14*/  
           :new.cgc4, /*15*/   
           :old.cgc2,  /*16*/  
           :new.cgc2, /*17*/   
           :old.atualizado_em,  /*18*/  
           :new.atualizado_em, /*19*/   
           :old.atualizado_por,  /*20*/  
           :new.atualizado_por, /*21*/   
           :old.situacao,  /*22*/  
           :new.situacao  /*23*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into EXPT_040_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           documento_OLD, /*8*/   
           documento_NEW, /*9*/   
           serie_OLD, /*10*/   
           serie_NEW, /*11*/   
           cgc9_OLD, /*12*/   
           cgc9_NEW, /*13*/   
           cgc4_OLD, /*14*/   
           cgc4_NEW, /*15*/   
           cgc2_OLD, /*16*/   
           cgc2_NEW, /*17*/   
           atualizado_em_OLD, /*18*/   
           atualizado_em_NEW, /*19*/   
           atualizado_por_OLD, /*20*/   
           atualizado_por_NEW, /*21*/   
           situacao_OLD, /*22*/   
           situacao_NEW /*23*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.documento, /*8*/   
           0, /*9*/
           :old.serie, /*10*/   
           '', /*11*/
           :old.cgc9, /*12*/   
           0, /*13*/
           :old.cgc4, /*14*/   
           0, /*15*/
           :old.cgc2, /*16*/   
           0, /*17*/
           :old.atualizado_em, /*18*/   
           null, /*19*/
           :old.atualizado_por, /*20*/   
           '', /*21*/
           :old.situacao, /*22*/   
           0 /*23*/
         );    
    end;    
 end if;    
end inter_tr_EXPT_040_log;
