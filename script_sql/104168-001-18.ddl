alter table FATU_504 add (FORMA_DISTR_TAM_COR NUMERIC(1) DEFAULT 0);
COMMENT ON COLUMN fatu_504.FORMA_DISTR_TAM_COR IS '0-Distribui tamanho e cor pela grade do produto completo ou 1-Distribui tamanho e cor pela grade da referencia';

/
exec inter_pr_recompile;
