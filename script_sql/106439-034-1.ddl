INSERT INTO EMPR_007 (PARAM, TIPO, LABEL,  FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT) 
VALUES ('supr.sugereObservacao', 1, 'lb47656', 'fy45528', null, 0, null, null);
commit;
/

declare tmpInt number;
cursor parametro_c is select codigo_empresa from fatu_500;
begin
  for reg_parametro in parametro_c
  loop
    select nvl(count(*),0) into tmpInt
    from fatu_500
    where fatu_500.codigo_empresa = reg_parametro.codigo_empresa;
    if(tmpInt > 0)
    then
      begin
        insert into empr_008 ( codigo_empresa, param, val_int)
            values (reg_parametro.codigo_empresa, 'supr.sugereObservacao', 0);
      end;
    end if;
  end loop;
  commit;
end;
/

