alter table obrf_180
add (cod_mensagem_cest     number(6) default 0,
     cod_sequencia_cest    number(2) default 0,
     cod_local_cest        varchar2(1) default 'C',
     cod_mensagem_SubTrib  number(6) default 0,
     cod_sequencia_SubTrib number(2) default 0,
     cod_local_SubTrib     varchar2(1) default 'C');
exec inter_pr_recompile;
