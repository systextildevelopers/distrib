
  CREATE OR REPLACE PROCEDURE "INTER_PR_ORDEM_PLANEJAMENTO" 
    -- Recebe parametros para explosao da estrutura.
   (p_origem              in varchar2,
    p_ordem_planejamento  in number,
    p_nivel_explode       in varchar2,
    p_grupo_explode       in varchar2,
    p_subgrupo_explode    in varchar2,
    p_item_explode        in varchar2,
    p_alternativa_explode in number,
    p_qtde_quilos_prog    in number)
is
   -- Cria cursor para executar loop na basi_050 buscando todos
   -- os produtos da estrutura.
   cursor basi050 is
   select nivel_comp,       grupo_comp,
          sub_comp,         item_comp,
          sequencia,        consumo,
          sub_item,         item_item,
          estagio,          tipo_calculo,
          alternativa_comp
   from basi_050
   where  basi_050.nivel_item       = p_nivel_explode
     and  basi_050.grupo_item       = p_grupo_explode
     and (basi_050.sub_item         = p_subgrupo_explode or basi_050.sub_item  = '000')
     and (basi_050.item_item        = p_item_explode     or basi_050.item_item = '000000')
     and  basi_050.alternativa_item = p_alternativa_explode
   order by basi_050.sequencia;

   -- Declara as variaveis que serao utilizadas.
   p_nivel_comp050       basi_050.nivel_comp%type := '#';
   p_grupo_comp050       basi_050.grupo_comp%type := '#####';
   p_subgru_comp050      basi_050.sub_comp%type   := '###';
   p_item_comp050        basi_050.item_comp%type  := '######';
   p_sequencia050        basi_050.sequencia%type;
   p_consumo050          basi_050.consumo%type;
   p_sub_item050         basi_050.sub_item%type   := '###';
   p_item_item050        basi_050.item_item%type  := '######';
   p_estagio050          basi_050.estagio%type;
   p_tpcalculo050        basi_050.tipo_calculo%type;
   p_alternativa_comp    basi_050.alternativa_comp%type;

begin
    -- Inicio do loop da explosao da estrutura.
   for reg_basi050 in basi050
   loop
      -- Carrega as variaveis com os valores do cursor.
      p_nivel_comp050    := reg_basi050.nivel_comp;
      p_grupo_comp050    := reg_basi050.grupo_comp;
      p_subgru_comp050   := reg_basi050.sub_comp;
      p_item_comp050     := reg_basi050.item_comp;
      p_sequencia050     := reg_basi050.sequencia;
      p_consumo050       := reg_basi050.consumo;

      p_sub_item050      := reg_basi050.sub_item;
      p_item_item050     := reg_basi050.item_item;

      p_estagio050       := reg_basi050.estagio;
      p_tpcalculo050     := reg_basi050.tipo_calculo;
      p_alternativa_comp := reg_basi050.alternativa_comp;

      if p_subgru_comp050 = '000' or p_consumo050 = 0.0000000
      then
         begin
            select basi_040.sub_comp, basi_040.consumo
            into   p_subgru_comp050,  p_consumo050
            from basi_040
            where basi_040.nivel_item       = p_nivel_explode
              and basi_040.grupo_item       = p_grupo_explode
              and basi_040.sub_item         = p_subgrupo_explode
              and basi_040.item_item        = p_item_item050
              and basi_040.alternativa_item = p_alternativa_explode
              and basi_040.sequencia        = p_sequencia050;
            exception
            when others then
               p_subgru_comp050 := '000';
               p_consumo050     := 0.0000000;
         end;
      end if;

      if p_item_comp050 = '000000'
      then
         begin
            select basi_040.item_comp
            into p_item_comp050
            from basi_040
            where basi_040.nivel_item       = p_nivel_explode
              and basi_040.grupo_item       = p_grupo_explode
              and basi_040.sub_item         = p_sub_item050
              and basi_040.item_item        = p_item_explode
              and basi_040.alternativa_item = p_alternativa_explode
              and basi_040.sequencia        = p_sequencia050;
            exception
            when others then
               p_item_comp050 := '000000';
         end;
      end if;

      update tmrp_625
         set tmrp_625.qtde_reserva_programada   = p_qtde_quilos_prog * p_consumo050
      where tmrp_625.ordem_planejamento         = p_ordem_planejamento

        and tmrp_625.nivel_produto_origem       = p_nivel_explode
        and tmrp_625.grupo_produto_origem       = p_grupo_explode
        and tmrp_625.subgrupo_produto_origem    = p_subgrupo_explode
        and tmrp_625.item_produto_origem        = p_item_explode

        and tmrp_625.nivel_produto              = p_nivel_comp050
        and tmrp_625.grupo_produto              = p_grupo_comp050
        and tmrp_625.subgrupo_produto           = p_subgru_comp050
        and tmrp_625.item_produto               = p_item_comp050;

      if p_origem = 'pcpb'
      then
          -- Se a origem chamada for obrf, atualiza a quantidade a enviar da ordem de servico.
          if  p_nivel_comp050 = '5' or p_nivel_comp050 = '4'
          then
            -- Chama procedure novamente para explosao da estrutura dos componentes
            -- da receita.
            inter_pr_ordem_planejamento('pcpb',
                                        p_ordem_planejamento,
                                        p_nivel_comp050,
                                        p_grupo_comp050,
                                        p_subgru_comp050,
                                        p_item_comp050,
                                        p_alternativa_comp,
                                        p_qtde_quilos_prog * p_consumo050);
          end if;
      end if;

      if p_origem = 'pcpt'
      then
          -- Se a origem chamada for obrf, atualiza a quantidade a enviar da ordem de servico.
          if  p_nivel_comp050 = '7'  or  p_nivel_comp050 = '4' or p_nivel_comp050 = '2'
          then
            -- Chama procedure novamente para explosao da estrutura dos componentes
            -- da receita.
            inter_pr_ordem_planejamento('pcpt',
                                        p_ordem_planejamento,
                                        p_nivel_comp050,
                                        p_grupo_comp050,
                                        p_subgru_comp050,
                                        p_item_comp050,
                                        p_alternativa_comp,
                                        p_qtde_quilos_prog * p_consumo050);
          end if;
      end if;
   end loop;
end INTER_PR_ORDEM_PLANEJAMENTO;


/*
         insert into tmrp_625
               (ordem_planejamento,                       tipo_produto_origem,
                pedido_venda,                             seq_item_pedido,

                seq_produto_origem,
                nivel_produto_origem,                     grupo_produto_origem,
                subgrupo_produto_origem,                  item_produto_origem,

                alternativa_produto_origem,               tipo_produto,
                nivel_produto,                            grupo_produto,
                subgrupo_produto,                         item_produto,

                alternativa_produto,                      seq_produto,
                estagio_producao,                         tipo_calculo,
                consumo,                                  qtde_reserva_planejada,

                qtde_reserva_programada,                  situacao)
            values
               (p_ordem_planejamento,                     ' ',
                800,                                      10,

                p_sequencia_explode,
                p_nivel_explode,                          p_grupo_explode,
                p_subgrupo_explode,                       p_item_explode,

                p_alternativa_explode,                    ' ',
                p_nivel_comp050,                          p_grupo_comp050,
                p_subgru_comp050,                         p_item_comp050,

                p_alternativa_comp,                       p_sequencia050,
                p_estagio050,                             p_tpcalculo050,
                p_consumo050,                             p_qtde_quilos_prog,

                p_qtde_quilos_prog,                       '2' );

*/

 

/

exec inter_pr_recompile;

