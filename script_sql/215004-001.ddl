insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('menu_ap00', 'Menu Apex', 1, 1, null);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'menu_ap00', 'menu_mp00', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'menu_ap00', 'menu_mp00', 1, 0, 'S', 'S', 'S', 'S');

commit;

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('menu_ap01', 'Carga de Dados', 1, 1, null);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'menu_ap01', 'menu_ap00', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'menu_ap01', 'menu_ap00', 1, 0, 'S', 'S', 'S', 'S');

commit;

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('cdst_fa01', 'Carga de Dados Systêxtil', 0, 1, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'cdst_fa01', 'menu_ap01', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'cdst_fa01', 'menu_ap01', 1, 0, 'S', 'S', 'S', 'S');

commit;

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('cdst_fa02', 'Carga de Dados Systêxtil - Log', 0, 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'cdst_fa02', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'cdst_fa02', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

commit;
