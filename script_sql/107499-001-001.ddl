alter table oper_001
add execution_delay number(10);

COMMENT ON COLUMN oper_001.execution_delay is 'Tempo em minutos para execu��o do proximo agendamento apos execu��o do atual';

exec inter_pr_recompile;
