
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_020_2" 
  -- TRIGGER PARA GERAR REGISTRO DE CONSULTA DE FATURAMENTO DE ROLO
  before delete or
         insert or
         update of nota_fiscal,
                   serie_fiscal_sai,
                   cod_empresa_nota, seq_nota_fiscal on pcpt_020
  for each row

declare
   v_hora_faturamento    date;

   v_executa_trigger      number;

begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      if updating
      then
         -- Se o novo valor da nota fiscal e diferene do valor antigo
         -- da nota, insere um registro na tabela de consulta-historico
         -- de faturamento do rolo .
         -- Para isso, testamos os campos nota fiscal e serie da nota fiscal
         if :new.nota_fiscal > 0
         then
            if (:new.nota_fiscal       <> :old.nota_fiscal      or
                :new.serie_fiscal_sai  <> :old.serie_fiscal_sai or
                :new.cod_empresa_nota  <> :old.cod_empresa_nota or
                :new.seq_nota_fiscal   <> :old.seq_nota_fiscal
                )
            or (:old.nota_fiscal is null                        or
                :old.serie_fiscal_sai is null                   or
                :old.cod_empresa_nota is null                   or
                :old.seq_nota_fiscal is null
                )
            then
                v_hora_faturamento := to_date('16/11/1989 '||to_char(sysdate,'HH24:MI'),'DD/MM/YYYY HH24:MI');

                insert into pcpt_060 (
                   codigo_rolo,           codigo_empresa,
                   num_nota_fiscal,       serie_nota_fisc,
                   peso_bruto,            data_faturamento,
                   hora_faturamento,      pedido_venda,
                   seq_pedido,            qtde_quilos_acab,
                   seq_nota_fiscal
                ) values (
                   :new.codigo_rolo,      :new.cod_empresa_nota,
                   :new.nota_fiscal,      :new.serie_fiscal_sai,
                   :new.peso_bruto,       trunc(sysdate),
                   v_hora_faturamento,    :new.pedido_venda,
                   :new.seq_item_pedido,  :new.qtde_quilos_acab,
                   :new.seq_nota_fiscal
                );
            end if;
         end if;
      end if;
   end if;
end;

-- ALTER TRIGGER "INTER_TR_PCPT_020_2" ENABLE
 

/

exec inter_pr_recompile;
