CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_425"
BEFORE INSERT on pedi_425
for each row

begin

    :new.etiq_previstas_orig := :new.etiquetas_previstas;

end INTER_TR_PEDI_425;

-- ALTER TRIGGER "INTER_TR_PEDI_425" ENABLE
