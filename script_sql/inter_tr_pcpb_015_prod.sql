
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_015_PROD" 
   before update of data_inicio, data_termino, hora_inicio, hora_termino
   on pcpb_015
   for each row
declare
   cursor pcpb100 is
   select pcpb_100.tipo_ordem, pcpb_100.ordem_agrupamento
   from pcpb_100
   where pcpb_100.ordem_producao = :new.ordem_producao;
begin
   for reg_pcpb100 in pcpb100
   loop
      -- A atualizacao da data inicio, data termino, devem ser separadas, porque sao independentes.
      -- pela logica, a data inicio normalmente e apontada antes da data termino.

      -- Se mudou a data inicio...
      if  :old.data_inicio <> :new.data_inicio
      and :new.data_inicio is not null
      then
         begin
            update tmrp_650
            set tmrp_650.inicio_producao  = to_date(to_char(:new.data_inicio,'DD/MM/YYYY') || ' ' || to_char(:new.hora_inicio,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
            where tmrp_650.tipo_alocacao          = 2
              and tmrp_650.tipo_recurso           = 1
              and tmrp_650.ordem_trabalho         = reg_pcpb100.ordem_agrupamento
              and tmrp_650.tipo_ordem_agrupamento = reg_pcpb100.tipo_ordem
              and tmrp_650.codigo_estagio         = :new.codigo_estagio
              and tmrp_650.seq_operacao           = :new.seq_operacao;
         exception when others then
            raise_application_error(-20000, 'Nao atualizou TMRP_650, data inicio de producao. ' || sqlerrm);
         end;
      end if;

      -- Se mudou a data termino...
      if  :old.data_termino <> :new.data_termino
      and :new.data_termino is not null
      then
         begin
            update tmrp_650
            set tmrp_650.termino_producao = to_date(to_char(:new.data_termino,'DD/MM/YYYY') || ' ' || to_char(:new.hora_termino,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
            where tmrp_650.tipo_alocacao          = 2
              and tmrp_650.tipo_recurso           = 1
              and tmrp_650.ordem_trabalho         = reg_pcpb100.ordem_agrupamento
              and tmrp_650.tipo_ordem_agrupamento = reg_pcpb100.tipo_ordem
              and tmrp_650.codigo_estagio         = :new.codigo_estagio
              and tmrp_650.seq_operacao           = :new.seq_operacao;
         exception when others then
            raise_application_error(-20000, 'Nao atualizou TMRP_650, data inicio de producao. ' || sqlerrm);
         end;
      end if;
   end loop;
end inter_tr_pcpb_015_prod;
-- ALTER TRIGGER "INTER_TR_PCPB_015_PROD" ENABLE
 

/

exec inter_pr_recompile;

