
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_060_CCE" 
before update of natopeno_nat_oper,            natopeno_est_oper,
                 classific_fiscal,             unidade_medida,
                 cvf_ipi,                      cvf_ipi_saida,
                 procedencia,                  cvf_icms,
                 cod_csosn,                    cvf_pis,
                 cvf_cofins

on fatu_060
for each row

declare
v_st_nota_fiscal_aux  number (1);
v_cod_status_aux      varchar2(5);

begin
   begin
      select situacao_nfisc    , cod_status
      into v_st_nota_fiscal_aux, v_cod_status_aux
      from fatu_050
      where fatu_050.codigo_empresa   =  :new.ch_it_nf_cd_empr
        and fatu_050.num_nota_fiscal  =  :new.ch_it_nf_num_nfis
        and fatu_050.serie_nota_fisc  =  :new.ch_it_nf_ser_nfis;
   exception
     when others then
     v_st_nota_fiscal_aux := 0;
	 v_cod_status_aux := ' ';
   end;

   if  v_st_nota_fiscal_aux = 1
   and (v_cod_status_aux = '100' or v_cod_status_aux = '990')
   then
      begin
         update fatu_050
         set fatu_050.st_flag_cce = 1
         where fatu_050.codigo_empresa   =  :new.ch_it_nf_cd_empr
           and fatu_050.num_nota_fiscal  =  :new.ch_it_nf_num_nfis
           and fatu_050.serie_nota_fisc  =  :new.ch_it_nf_ser_nfis;
      end;
   end if;

end inter_tr_fatu_060_cce;
-- ALTER TRIGGER "INTER_TR_FATU_060_CCE" ENABLE
 

/

exec inter_pr_recompile;

