
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_010_HIST" 
AFTER INSERT OR
      DELETE OR
      UPDATE of periodo_producao,      cd_pano_nivel99,      cd_pano_grupo,
                cd_pano_subgrupo,      cd_pano_item,         qtde_quilos_prog,
                qtde_quilos_prod,      qtde_rolos_prog,      qtde_rolos_prod,
                grupo_maquina,         subgru_maquina,       cod_cancelamento,
                roteiro_opcional,      alternativa_item,     nivel_acab,
                grupo_acab,            sub_acab,             item_acab,
                situacao_ordem,        ordem_tecelagem,      numero_maquina,
                peso_rolo,             quilos_prog_acab,     rolos_prog_acab,
                quilos_prod_acab,      rolos_prod_acab,      dep_entrada_tec,
                ordem_servico,         seq_ordem_serv,       numero_lote,
                tran_baixa_insumo,     tipo_ordem,           numero_agrupamento
      on pcpt_010
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_area_producao          number(1);
   ws_ordem_producao         number(9);
   ws_periodo_producao       number(4);
   ws_ordem_confeccao        number(2);
   ws_tipo_historico         number(1);

   ws_descricao_historico    varchar2(4000);
   ws_tipo_ocorr             varchar2(1);


begin

   -- Dados do usuario logado
  inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                          ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

  if INSERTING then
     ws_area_producao         := 4;
     ws_ordem_producao        := :new.ordem_tecelagem;
     ws_periodo_producao      := :new.periodo_producao;
     ws_ordem_confeccao       := 0;
     ws_tipo_historico        := 1;

     ws_descricao_historico   := 'INCLUSAO ORDEM TECELAGEM (PCPT_010)'
                               ||chr(10)
                               || 'ORDEM TECELAGEM: ' || :new.ordem_tecelagem
                               ||chr(10)
                               || 'PERIODO PRODUCAO: ' || :new.periodo_producao
                               ||chr(10)
                               || 'TIPO ORDEM: ' || :new.tipo_ordem
                               ||chr(10)
                               || 'SIT.ORDEM PROD: ' || :new.situacao_reposicao
                               ||chr(10)
                               || 'CODIGO TECIDO: '  || :new.cd_pano_nivel99
                               || '.'
                               || :new.cd_pano_grupo
                               || '.'
                               || :new.cd_pano_subgrupo
                               || '.'
                               || :new.cd_pano_item
                               ||chr(10)
                               ||'TECIDO DIRECIONADO: ' || :new.nivel_acab
                               || '.'
                               || :new.grupo_acab
                               || '.'
                               || :new.sub_acab
                               || '.'
                               || :new.item_acab
                               ||chr(10)
                               || 'ALTERNATIVA: ' || :new.alternativa_item
                               ||chr(10)
                               || 'ROTEIRO: ' || :new.roteiro_opcional
                               ||chr(10)
                               || 'LOTE: ' || :new.numero_lote
                               ||chr(10)
                               || 'DEPOSITO: ' || :new.dep_entrada_tec
                               ||chr(10)
                               || 'CODIGO MAQUINA: ' || :new.grupo_maquina
                               || '.'
                               || :new.subgru_maquina
                               || '.'
                               || :new.numero_maquina
                               ||chr(10)
                               || 'NUMERO AGRUPAMENTO: ' || :new.numero_agrupamento
                               ||chr(10)
                               || 'PESO ROLO: ' || :new.peso_rolo
                               ||chr(10)
                               || 'PESO PROGRAMADO: ' || :new.qtde_quilos_prog
                               ||chr(10)
                               || 'QTDE PROGRAMADA: ' || :new.qtde_rolos_prog
                               ||chr(10)
                               || 'PESO PRODUZIDO: ' || :new.qtde_quilos_prod
                               ||chr(10)
                               || 'QTDE PRODUZIDA: ' || :new.qtde_rolos_prod
                               ||chr(10)
                               || 'COD.CANCELAMENTO: ' || :new.cod_cancelamento
                               ||chr(10)
                               || 'TRANSACAO DE BAIXA: ' || :new.tran_baixa_insumo
                               ||chr(10)
                               || 'ORDEM SERVICO: ' || :new.ordem_servico
                               ||chr(10)
                               || 'SEQ ORDEM SERVICO: ' || :new.seq_ordem_serv;

     ws_tipo_ocorr            := 'I';

  elsif DELETING then
     ws_area_producao         := 4;
     ws_ordem_producao        := :old.ordem_tecelagem;
     ws_periodo_producao      := :old.periodo_producao;
     ws_ordem_confeccao       := 0;
     ws_tipo_historico        := 1;

     ws_descricao_historico   := 'EXCLUSAO ORDEM TECELAGEM (PCPT_010)'
                               ||chr(10)
                               || 'ORDEM TECELAGEM: ' || :old.ordem_tecelagem
                               ||chr(10)
                               || 'PERIODO PRODUCAO: ' || :old.periodo_producao
                               ||chr(10)
                               || 'TIPO ORDEM: ' || :old.tipo_ordem
                               ||chr(10)
                               || 'SIT.ORDEM PROD: ' || :old.situacao_reposicao
                               ||chr(10)
                               || 'CODIGO TECIDO: '  || :old.cd_pano_nivel99
                               || '.'
                               || :old.cd_pano_grupo
                               || '.'
                               || :old.cd_pano_subgrupo
                               || '.'
                               || :old.cd_pano_item
                               ||chr(10)
                               ||'TECIDO DIRECIONADO: ' || :old.nivel_acab
                               || '.'
                               || :old.grupo_acab
                               || '.'
                               || :old.sub_acab
                               || '.'
                               || :old.item_acab
                               ||chr(10)
                               || 'ALTERNATIVA: ' || :old.alternativa_item
                               ||chr(10)
                               || 'ROTEIRO: ' || :old.roteiro_opcional
                               ||chr(10)
                               || 'LOTE: ' || :old.numero_lote
                               ||chr(10)
                               || 'DEPOSITO: ' || :old.dep_entrada_tec
                               ||chr(10)
                               || 'CODIGO MAQUINA: ' || :old.grupo_maquina
                               || '.'
                               || :old.subgru_maquina
                               || '.'
                               || :old.numero_maquina
                               ||chr(10)
                               || 'NUMERO AGRUPAMENTO: ' || :old.numero_agrupamento
                               ||chr(10)
                               || 'PESO ROLO: ' || :old.peso_rolo
                               ||chr(10)
                               || 'PESO PROGRAMADO: ' || :old.qtde_quilos_prog
                               ||chr(10)
                               || 'QTDE PROGRAMADA: ' || :old.qtde_rolos_prog
                               ||chr(10)
                               || 'PESO PRODUZIDO: ' || :old.qtde_quilos_prod
                               ||chr(10)
                               || 'QTDE PRODUZIDA: ' || :old.qtde_rolos_prod
                               ||chr(10)
                               || 'COD.CANCELAMENTO: ' || :old.cod_cancelamento
                               ||chr(10)
                               || 'TRANSACAO DE BAIXA: ' || :old.tran_baixa_insumo
                               ||chr(10)
                               || 'ORDEM SERVICO: ' || :old.ordem_servico
                               ||chr(10)
                               || 'SEQ ORDEM SERVICO: ' || :old.seq_ordem_serv;

     ws_tipo_ocorr            := 'D';

  elsif UPDATING then
     ws_area_producao         := 4;
     ws_ordem_producao        := :old.ordem_tecelagem;
     ws_periodo_producao      := :old.periodo_producao;
     ws_ordem_confeccao       := 0;
     ws_tipo_historico        := 1;

     ws_descricao_historico   := 'ATUALIZACAO ORDEM TECELAGEM (PCPT_010)'
                               ||chr(10)
                               || 'ORDEM TECELAGEM: ' || :old.ordem_tecelagem || ' -> ' || :new.ordem_tecelagem
                               ||chr(10)
                               || 'PERIODO PRODUCAO: ' || :old.periodo_producao || ' -> ' || :new.periodo_producao
                               ||chr(10)
                               || 'TIPO ORDEM: ' || :old.tipo_ordem || ' -> ' || :new.tipo_ordem
                               ||chr(10)
                               || 'SIT.ORDEM PROD: ' || :old.situacao_reposicao || ' -> ' || :new.situacao_reposicao
                               ||chr(10)
                               || 'CODIGO TECIDO: '  || :old.cd_pano_nivel99
                               || '.'
                               || :old.cd_pano_grupo
                               || '.'
                               || :old.cd_pano_subgrupo
                               || '.'
                               || :old.cd_pano_item || ' -> ' || :new.cd_pano_nivel99
                               || '.'
                               || :new.cd_pano_grupo
                               || '.'
                               || :new.cd_pano_subgrupo
                               || '.'
                               || :new.cd_pano_item
                               ||chr(10)
                               ||'TECIDO DIRECIONADO: ' || :old.nivel_acab
                               || '.'
                               || :old.grupo_acab
                               || '.'
                               || :old.sub_acab
                               || '.'
                               || :old.item_acab  || ' -> '|| :new.nivel_acab
                               || '.'
                               || :new.grupo_acab
                               || '.'
                               || :new.sub_acab
                               || '.'
                               || :new.item_acab
                               ||chr(10)
                               || 'ALTERNATIVA: ' || :old.alternativa_item || ' -> ' || :new.alternativa_item
                               ||chr(10)
                               || 'ROTEIRO: ' || :old.roteiro_opcional || ' -> ' || :new.roteiro_opcional
                               ||chr(10)
                               || 'LOTE: ' || :old.numero_lote || ' -> ' || :new.numero_lote
                               ||chr(10)
                               || 'DEPOSITO: ' || :old.dep_entrada_tec || ' -> ' || :new.dep_entrada_tec
                               ||chr(10)
                               || 'CODIGO MAQUINA: ' || :old.grupo_maquina
                               || '.'
                               || :old.subgru_maquina
                               || '.'
                               || :old.numero_maquina || ' -> ' || :new.grupo_maquina
                               || '.'
                               || :new.subgru_maquina
                               || '.'
                               || :new.numero_maquina
                               ||chr(10)
                               || 'NUMERO AGRUPAMENTO: ' || :old.numero_agrupamento || ' -> ' || :new.numero_agrupamento
                               ||chr(10)
                               || 'PESO ROLO: ' || :old.peso_rolo || ' -> ' || :new.peso_rolo
                               ||chr(10)
                               || 'PESO PROGRAMADO: ' || :old.qtde_quilos_prog || ' -> ' || :new.qtde_quilos_prog
                               ||chr(10)
                               || 'QTDE PROGRAMADA: ' || :old.qtde_rolos_prog || ' -> ' || :new.qtde_rolos_prog
                               ||chr(10)
                               || 'PESO PRODUZIDO: ' || :old.qtde_quilos_prod || ' -> ' || :new.qtde_quilos_prod
                               ||chr(10)
                               || 'QTDE PRODUZIDA: ' || :old.qtde_rolos_prod || ' -> ' || :new.qtde_rolos_prod
                               ||chr(10)
                               || 'COD.CANCELAMENTO: ' || :old.cod_cancelamento || ' -> ' || :new.cod_cancelamento
                               ||chr(10)
                               || 'TRANSACAO DE BAIXA: ' || :old.tran_baixa_insumo || ' -> ' || :new.tran_baixa_insumo
                               ||chr(10)
                               || 'ORDEM SERVICO: ' || :old.ordem_servico || ' -> ' || :new.ordem_servico
                               ||chr(10)
                               || 'SEQ ORDEM SERVICO: ' || :old.seq_ordem_serv || ' -> ' || :new.seq_ordem_serv ;
     ws_tipo_ocorr            := 'A';
  end if;

  INSERT INTO hist_010
    (area_producao,           ordem_producao,          periodo_producao,
     ordem_confeccao,         tipo_historico,          descricao_historico,
     tipo_ocorr,              data_ocorr,              hora_ocorr,
     usuario_rede,            maquina_rede,            aplicacao,
     usuario_sistema)
  VALUES
    (ws_area_producao,        ws_ordem_producao,       ws_periodo_producao,
     ws_ordem_confeccao,      ws_tipo_historico,       ws_descricao_historico,
     ws_tipo_ocorr,           sysdate,                 sysdate,
     ws_usuario_rede,         ws_maquina_rede,         ws_aplicativo,
     ws_usuario_systextil);

end inter_tr_pcpt_010_hist;

-- ALTER TRIGGER "INTER_TR_PCPT_010_HIST" ENABLE
 

/

exec inter_pr_recompile;

