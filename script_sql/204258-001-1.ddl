declare
    id_grid    number;
    id_profile number;
    id_column  number;

    procedure insert_column(p_id number,
                            p_id_grid number,
                            p_key varchar2,
                            p_name varchar2,
                            p_type varchar2,
                            p_formatter_index number,
                            p_scale number) is
    begin
        insert into conf_grid_column (id, id_grid, key, name, type, formatter_index, scale)
        values (p_id, p_id_grid, p_key, p_name, p_type, p_formatter_index, p_scale);
    end;

    procedure insert_column_profile(p_id_grid_perfil number,
                                    p_id_grid_column number,
                                    p_position number) is
    begin
        insert into conf_grid_perfil_column (id_grid_perfil, id_grid_column, position)
        values (p_id_grid_perfil, p_id_grid_column, p_position);
    end;
begin
    id_grid := id_conf_grid.nextval;

    insert into conf_grid (id, nome) values (id_grid, 'CaixasNotaFiscal');

    id_profile := id_conf_grid_perfil.nextval;

    insert into conf_grid_perfil (id, id_grid, usuario_gerenciador,
                                  empresa_usuario_gerenciador, nome,
                                  publico, padrao, padrao_usuario)
    values (id_profile, id_grid, 'INTERSYS', 1, 'Padr�o', 1, 1, 1);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'numero_caixa', 'Numero Caixa',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 1);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'turno', 'Turno',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 2);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'codigo_deposito', 'Codigo Deposito',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 3);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'lote', 'Lote',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 4);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'prodcai_nivel99', 'Prodcai Nivel99',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 5);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'prodcai_grupo', 'Prodcai Grupo',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 6);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'prodcai_subgrupo', 'Prodcai Subgrupo',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 7);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'prodcai_item', 'Prodcai Item',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 8);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'data_producao', 'Data Producao',
                  'DATE', 2, 0);
    insert_column_profile(id_profile, id_column, 9);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'peso_liquido', 'Peso Liquido',
                  'NUMBER', 1, 3);
    insert_column_profile(id_profile, id_column, 10);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'peso_bruto', 'Peso Bruto',
                  'NUMBER', 1, 3);
    insert_column_profile(id_profile, id_column, 11);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'peso_embalagem', 'Peso Embalagem',
                  'NUMBER', 1, 4);
    insert_column_profile(id_profile, id_column, 12);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'preco_medio', 'Preco Medio',
                  'NUMBER', 1, 3);
    insert_column_profile(id_profile, id_column, 13);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'endereco_caixa', 'Endereco Caixa',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 14);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'numero_pedido', 'Numero Pedido',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 15);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'sequencia_pedido', 'Sequencia Pedido',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 16);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'nr_solicitacao', 'Nr Solicitacao',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 17);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'status_caixa', 'Status Caixa',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 18);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'nota_fiscal', 'Nota Fiscal',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 19);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'serie_nota', 'Serie Nota',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 20);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'transacao_ent', 'Transacao Ent',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 21);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'seq_nota_fiscal', 'Seq Nota Fiscal',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 22);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'data_entrada', 'Data Entrada',
                  'DATE', 2, 0);
    insert_column_profile(id_profile, id_column, 23);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'codigo_embalagem', 'Codigo Embalagem',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 24);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'proforma', 'Proforma',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 25);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'valor_entrada', 'Valor Entrada',
                  'NUMBER', 1, 2);
    insert_column_profile(id_profile, id_column, 26);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'valor_despesas', 'Valor Despesas',
                  'NUMBER', 1, 2);
    insert_column_profile(id_profile, id_column, 27);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'qtde_pecas_emb', 'Qtde Pecas Emb',
                  'NUMBER', 1, 3);
    insert_column_profile(id_profile, id_column, 28);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'data_transacao', 'Data Transacao',
                  'DATE', 2, 0);
    insert_column_profile(id_profile, id_column, 29);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'torcao', 'Torcao',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 30);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'nota_fisc_ent', 'Nota Fisc Ent',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 31);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'seri_fisc_ent', 'Seri Fisc Ent',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 32);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'sequ_fisc_ent', 'Sequ Fisc Ent',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 33);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'nome_prog', 'Nome Prog',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 34);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'periodo_producao', 'Periodo Producao',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 35);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'qualidade_fio', 'Qualidade Fio',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 36);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'pre_romaneio', 'Pre Romaneio',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 37);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'grupo_maquina', 'Grupo Maquina',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 38);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'sub_maquina', 'Sub Maquina',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 39);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'numero_maquina', 'Numero Maquina',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 40);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'cnpj_fornecedor9', 'Cnpj Fornecedor9',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 41);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'cnpj_fornecedor4', 'Cnpj Fornecedor4',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 42);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'cnpj_fornecedor2', 'Cnpj Fornecedor2',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 43);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'data_insercao', 'Data Insercao',
                  'DATE', 2, 0);
    insert_column_profile(id_profile, id_column, 44);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'data_cardex', 'Data Cardex',
                  'DATE', 2, 0);
    insert_column_profile(id_profile, id_column, 45);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'transacao_cardex', 'Transacao Cardex',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 46);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'usuario_cardex', 'Usuario Cardex',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 47);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'valor_movto_cardex', 'Valor Movto Cardex',
                  'NUMBER', 1, 5);
    insert_column_profile(id_profile, id_column, 48);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'valor_contabil_cardex', 'Valor Contabil Cardex',
                  'NUMBER', 1, 5);
    insert_column_profile(id_profile, id_column, 49);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'tabela_origem_cardex', 'Tabela Origem Cardex',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 50);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'ordem_agrupamento', 'Ordem Agrupamento',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 51);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'caixa_original', 'Caixa Original',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 52);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'turno_original', 'Turno Original',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 53);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'codigo_pesador', 'Codigo Pesador',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 54);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'divisao_producao', 'Divisao Producao',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 55);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'ordem_producao', 'Ordem Producao',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 56);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'peso_adicional', 'Peso Adicional',
                  'NUMBER', 1, 4);
    insert_column_profile(id_profile, id_column, 57);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'arreada', 'Arreada',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 58);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'restricao', 'Restricao',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 59);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'deposito_producao', 'Deposito Producao',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 60);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'transacao_producao', 'Transacao Producao',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 61);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'sequencia_plano', 'Sequencia Plano',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 62);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'cod_empresa_sai', 'Cod Empresa Sai',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 63);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'sigla_inmetro', 'Sigla Inmetro',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 64);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'centro_custo_cardex', 'Centro Custo Cardex',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 65);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'qtde_cones_cx', 'Qtde Cones Cx',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 66);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'lote_old', 'Lote Old',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 67);


end;

/
