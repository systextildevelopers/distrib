CREATE TABLE basi_711 (
        fam_pente           VARCHAR2(10) NOT NULL ENABLE,
        num_pente           VARCHAR2(10) NOT NULL ENABLE,
        descricao_sub_fam   VARCHAR2(60) NOT NULL ENABLE,
        situacao            NUMBER(5) NOT NULL ENABLE,
        num_patrimonio      VARCHAR2(20) NOT NULL ENABLE,
    CONSTRAINT "PK_BASI_711" PRIMARY KEY (fam_pente,num_pente) ENABLE
);

COMMENT ON COLUMN basi_711.situacao IS '0 - livre 1 - ocupado';
